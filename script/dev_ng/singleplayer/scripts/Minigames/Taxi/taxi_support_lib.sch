/*
Change Log 
10/4/11 - Removed fail for deadline time
7/7/11 - Added TAXI_CAM_PICKUP_INTERP_IN & TAXI_CAM_PICKUP_INTERP_OUT to use along with SETUP_TAXI_QUICK_CUT which I use when picking up the passenger in TAXI_HANDLE_IV_PICKUP
7/5/11  - Added TAXI_MONEY_BONUS_MIN_BAG_RETURN & TAXI_MONEY_BONUS_MAX_BAG_RETURN for assigning a Bonus $$$ amt when Passenger forgets his bag in TAXI_HANDLE_FORGOTTEN_BAG
4/15/11 - Added in IS_TAXI_POINT_IN_FRONT_OF_PLAYER() for enhancing the pickup/dropoff points when spawning a passenger. Works pretty well. Ah it felt good brushing up on Vector Math. 
*/

USING "chase_hint_cam.sch"		CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "commands_shapetest.sch"
USING "CompletionPercentage_public.sch"
USING "controller_taxi_lib.sch"
USING "cutscene_public.sch"
USING "properties_public.sch"

USING "Taxi_Leaderboards_lib.sch"
USING "Taxi_Stats_lib.sch"
USING "Taxi_Dialogue_Lib.sch"
USING "Taxi_Radio_Control_Lib.sch"
USING "Taxi_Tips.sch"
USING "taxi_functions.sch"

USING "lineactivation.sch"

USING "oddjob_aggro.sch"

USING "script_oddjob_funcs.sch"
USING "shared_hud_displays.sch"

USING "rich_presence_public.sch"
USING "achievement_public.sch"

ENUM TAXI_PED_RUN_STATE
    TPRS_INIT,
    TPRS_GETTING_OUT,
    TPRS_RUNNING,
    TPRS_TOSS_MONEY,
    TPRS_WAIT_FOR_MONEY_PICKUP,
    TPRS_CLEANUP
ENDENUM

ENUM TAXI_PED_ENTER_CUT
    TAXI_PED_ENTER_CUT_STOP,
    TAXI_PED_ENTER_CUT_FIRST,
	TAXI_PED_ENTER_CUT_SECOND,
    TAXI_PED_ENTER_CUT_INTERP_WAIT,
    TAXI_PED_ENTER_CUT_INTERP,
    TAXI_PED_ENTER_CUT_WAIT,
    TAXI_PED_ENTER_CUT_ENTER,
    TAXI_PED_ENTER_CUT_SKIP,
    TAXI_PED_ENTER_CUT_FADEIN,
    TAXI_PED_ENTER_CUT_END, 
	TAXI_PED_NO_CUTSCENE
ENDENUM

TAXI_PED_ENTER_CUT taxiEnterCut = TAXI_PED_ENTER_CUT_STOP

ENUM TAXI_PED_DRIVE_UP
    TAXI_PED_DRIVE_UP_INIT,
	TAXI_PED_TRIGGER_SHAPE_TEST,
    TAXI_PED_DRIVE_UP_DIALOUGE,
    TAXI_PED_DRIVE_UP_END
ENDENUM

 TAXI_PED_DRIVE_UP taxiDriveUp = TAXI_PED_DRIVE_UP_INIT

ENUM TXM_DEBUG_SKIP_STATES
	TXM_DSS_CHECK_FOR_BUTTON_PRESS = 0,
	TXM_FIND_WHAT_POINT_TO_WARP_TO,
	TXM_DSS_CHECK_DISTANCE,
	TXM_DSS_FADE_DOWN,
	TXM_DSS_FADING,
	TXM_DSS_TELEPORT,
	TXM_DSS_WAIT_FOR_DATA,
	TXM_DSS_FADE_UP
ENDENUM

ENUM TAXI_VEHICLE_TYPES
	TAXITYPE_LOW,
	TAXITYPE_REG,
	TAXITYPE_HIGH,
	TAXITYPE_NUM
ENDENUM

STRUCT TAXI_MONEY_STRUCT
    INT             iTaxiMoneyAmount
    INT             iMoneyPlacementFlags
    VECTOR          vTaxiMoneyPos
    BLIP_INDEX      blipMoney
    PICKUP_INDEX    pickupMoney
ENDSTRUCT

// Keep this here for the above tweakables.

INT             iAllowSkipCutsceneTime = 0
INT				iEnterDelayTimer

structTimer		timerTotal

BOOL			bMileageDebugGate
BOOL			bNoEnterCutscene

//structTimer	timerFailDelay

//INT 			iSlowDownTimer

//B*454232 - Removing Vanity plates on the taxi
PROC ASSIGN_TAXI_OJ_LICENSE_PLATE(TaxiStruct &myTaxiData)
	IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
		IF GET_RANDOM_BOOL()
			//SET_VEHICLE_NUMBER_PLATE_TEXT(myTaxiData.viTaxi,"MAHALO")
	    ELSE
			//SET_VEHICLE_NUMBER_PLATE_TEXT(myTaxiData.viTaxi,"ALOHA")
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC TAXI_SAFE_ASSERT(STRING sError,BOOL bDebug = FALSE)
    IF bDebug
        SCRIPT_ASSERT(sError)
    ENDIF
ENDPROC

#ENDIF

FUNC FLOAT GET_INVERSE_HEADING(FLOAT fHeading)
	IF fHeading > 180 
		CDEBUG1LN(DEBUG_OJ_TAXI," Inverse Heading for ", fHeading, " = ", (fHeading - 180))
		RETURN (fHeading - 180)
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI," Inverse Heading for ", fHeading, " = ", (fHeading + 180))
		RETURN (fHeading + 180)
	ENDIF
ENDFUNC
PROC SET_PLAYER_REENTERED_TAXI_OJ(TaxiStruct &myTaxiData)

	TAXI_CANCEL_TIMERS(myTaxiData, TT_NOTINCAB)
	TAXI_CANCEL_TIMERS(myTaxiData, TT_DIFFERENTCAR)
	TOGGLE_TAXI_OJ_RADIO_SOUNDS_ON()
	
	IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
		PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
	ENDIF
	
ENDPROC

PROC SET_PLAYER_LEFT_TAXI_OJ(TaxiStruct &myTaxiData)
	
	TAXI_RESET_TIMERS(myTaxiData, TT_NOTINCAB)
	TOGGLE_TAXI_OJ_RADIO_SOUNDS_OFF()
	
ENDPROC

PROC TAXI_PREP_TURN_OFF_VEHICLE_GENS(VECTOR vPoint, BOOL bEnable = FALSE)
    IF NOT ARE_VECTORS_EQUAL(vPoint,NULL_VECTOR())
        VECTOR vMin = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vPoint,1,<<-TAXI_AREA_SAFE_AROUND_PASSENGER,-TAXI_AREA_SAFE_AROUND_PASSENGER,-10>>) 
        VECTOR vMax = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vPoint,1,<<TAXI_AREA_SAFE_AROUND_PASSENGER,TAXI_AREA_SAFE_AROUND_PASSENGER,10>>)
        
        SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMin, vMax,bEnable)
        CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi - Toggled generator")
    ENDIF
ENDPROC

FUNC TAXI_VEHICLE_TYPES GET_TAXI_VEHICLE_TYPE(TaxiStruct &myTaxiData)
	
	IF DOES_ENTITY_EXIST(myTaxiData.viTaxi)
	AND IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF GET_ENTITY_MODEL(myTaxiData.viTaxi) = Granger //GET_VEHICLE_LAYOUT_HASH = VAN
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = rebel
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = rebel2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = sandking
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = sandking2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Patriot
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Surfer
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Surfer2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = mesa
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = rancherxl
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Tiptruck2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = mixer
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = mixer2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = rubble
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = scrap
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = tiptruck
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Armytanker
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Barracks
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = barracks2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Pranger
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = ambulance
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = firetruk
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = policet
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = riot
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = sheriff2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = stockade
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = stockade3
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Biff
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Hauler
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Packer
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Trash
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = benson
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = phantom
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = pounder
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Utillitruck
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Utillitruck2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Utillitruck3
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = dloader
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = flatbed
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = tourbus
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = towtruck
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = towtruck2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Camper
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Taco
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = boxville
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = boxville2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = burrito
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = burrito2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = burrito3
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = burrito4
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = gburrito
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = journey
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = mule
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = mule2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = pony
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = rumpo
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = speedo
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = speedo2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = youga
			
			RETURN TAXITYPE_HIGH
			//strVomitVariation = "vomit_van"
			
		ELIF GET_ENTITY_MODEL(myTaxiData.viTaxi) = Phoenix //GET_VEHICLE_LAYOUT_HASH = LOW
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = coquette
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = dune
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Cheetah
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Cogcabrio
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = JB700
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Monroe
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Ninef
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Ninef2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = RapidGT
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = RapidGT2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Stinger
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = StingerGT
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = Ztype
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = banshee
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = bullet
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = carbonizzare
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = comet2
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = ENTITYXF
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = f620
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = infernus
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = surano
		OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = voltic
			
			RETURN TAXITYPE_LOW
			//strVomitVariation = "vomit_low"
			
		ELSE
			RETURN TAXITYPE_REG
			//strVomitVariation = "vomit_outside"
		ENDIF
	ENDIF
	
	RETURN TAXITYPE_NUM
ENDFUNC

FUNC STATSENUM GET_TAXI_DRIVING_STAT_FOR_PLAYER()
	STATSENUM sStatToReturn
	enumCharacterList playerEnum
	playerEnum = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	
	IF playerEnum = CHAR_MICHAEL
		sStatToReturn =  SP0_DIST_DRIVING_CAR
		CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_DRIVING_STAT_FOR_PLAYER: Player stat = SP0_DIST_DRIVING_CAR")
	
	ELIF playerEnum = CHAR_TREVOR
		sStatToReturn =  SP2_DIST_DRIVING_CAR
		CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_DRIVING_STAT_FOR_PLAYER: Player stat = SP2_DIST_DRIVING_CAR")

	ELIF playerEnum = CHAR_FRANKLIN
		sStatToReturn =  SP1_DIST_DRIVING_CAR
		CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_DRIVING_STAT_FOR_PLAYER: Player stat = SP1_DIST_DRIVING_CAR")
	
	ELSE
		sStatToReturn =  SP0_DIST_DRIVING_CAR
	ENDIF
	
	RETURN sStatToReturn
ENDFUNC

PROC SET_TAXI_FARE_OFF_MILEAGE(TaxiStruct &myTaxiData)
	FLOAT fDistanceDriven
	 
	fDistanceDriven = myTaxiData.fTaxiOJ_CurrentMileage
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_MILEAGE: Player drove ", fDistanceDriven, " meters.")
	fDistanceDriven = CAST_METRES_TO_MILES(fDistanceDriven )
	
	iTaxiSCStats[TAXISC_DISTANCE] = CEIL(fDistanceDriven)
	iTaxiSCStats[TAXISC_LONGEST_FARE] = CEIL(fDistanceDriven)
	TAXI_STATS_UPDATE(TAXI_STAT_DIST_LONGEST, CEIL(fDistanceDriven))
	TAXI_STATS_UPDATE(TAXI_STAT_DIST_TOTAL_W_PASSENGER, CEIL(fDistanceDriven))
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_MILEAGE: ", "Player drove ", fDistanceDriven, " MILES.")
	myTaxiData.iTaxiOJ_CashFare =  CEIL(fDistanceDriven * TAXI_BASE_FARE)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"$$$$$$$$$$$$$$$$$$$$$		SET_TX_FARE_OFF_MILEAGE: $", "", myTaxiData.iTaxiOJ_CashFare)
	
ENDPROC

//// PURPOSE: Deactivates your special ability if it's turned on. Used for cutscenes and any time control is removed
 ///    
PROC TAXI_DEACTIVATE_SPECIAL_ABILITY_FOR_DROPOFF()
	IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
		SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
	ENDIF
ENDPROC
PROC SET_TAXI_OJ_CURRENT_OBJECTIVE(TaxiStruct &myTaxiData,TAXI_DIALOGUE_INDEX tNewObjective )
	
	IF myTaxiData.tTaxiOJ_ObjectiveCurrent <> tNewObjective
		myTaxiData.tTaxiOJ_ObjectiveCurrent = tNewObjective
		CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ - Current Objective is set to ",tNewObjective, " with DX Index = ", GET_STRING_OJ_TAXI_DIALOGUE_INDEX(myTaxiData))
	ENDIF
ENDPROC

FUNC BOOL IS_TAXI_APP_INTERNET_RUNNING()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appInternet")) > 0
		CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ - Internet app is running. Waiting til it's closed")
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_TAXI_APP_CAMERA_RUNNING()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera")) > 0
		CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ - Camera app is running. Waiting til it's closed")
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_OJ_TAXI_PLAYER_CONTROL_OFF()
	
	IF NOT IS_TAXI_APP_INTERNET_RUNNING()
	AND NOT IS_TAXI_APP_CAMERA_RUNNING()
		IF NOT IS_PLAYER_SCRIPT_CONTROL_ON(GET_PLAYER_INDEX())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
PROC STOP_PHONE_AND_APPS_FOR_TAXI_CUTSCENE()
	
	IF IS_TAXI_APP_INTERNET_RUNNING()
		TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("appInternet")
		CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi APPS - Internet app is being quit for cutscene.")

	ENDIF
	
	IF IS_TAXI_APP_CAMERA_RUNNING()
		TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("appCamera")
		CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi APPS - Camera app is being quit for cutscene.")
    ENDIF
	
	IF IS_PHONE_ONSCREEN(TRUE)     
    	HANG_UP_AND_PUT_AWAY_PHONE()	
		CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi APPS - Phone is onscreen, putting it away and hanging up")
	ENDIF
	
ENDPROC
FUNC BOOL IS_TAXI_FULLY_STOPPED(TaxiStruct &myTaxiData, BOOL bLeaveCameraControlOn = TRUE, FLOAT fStopDistance = 5.0)

    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
        IF IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi) 
            
			IF IS_PHONE_ONSCREEN(TRUE)
				//HIDE_ACTIVE_PHONE(TRUE)
				HANG_UP_AND_PUT_AWAY_PHONE()
				CDEBUG1LN(DEBUG_OJ_TAXI,"Phone put away in IS_TAXI_FULLY_STOPPED")
			ENDIF
			
			IF IS_OJ_TAXI_PLAYER_CONTROL_OFF()
				TAXI_DEACTIVATE_SPECIAL_ABILITY_FOR_DROPOFF()
                RETURN TRUE
            ELSE
//                IF (GET_GAME_TIMER() - iSlowDownTimer) > 500
//					BRING_VEHICLE_TO_HALT(myTaxiData.viTaxi, 5.0, 2000)
//					CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi being halted")
//				ENDIF
//				
//				iSlowDownTimer = GET_GAME_TIMER()
				
//				IF (GET_GAME_TIMER() % 1000) < 50
//					CDEBUG1LN(DEBUG_OJ_TAXI, "stop distance = ", fStopDistance)
//				ENDIF
				
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(myTaxiData.viTaxi, fStopDistance) //GET_ENTITY_SPEED(myTaxiData.viTaxi) <= 0.5
	                
					IF bLeaveCameraControlOn
	                    SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
	                ELSE
	                    SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE)
	                ENDIF
				ENDIF
            ENDIF
            SET_VEHICLE_BRAKE_LIGHTS(myTaxiData.viTaxi,TRUE)
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

PROC TAXI_DEBUG_SET_IN_TAXI(Taxistruct &taxiData, BOOL bDebugOnly)
    IF bDebugOnly
        IF NOT HAS_MODEL_LOADED(TAXI)
            REQUEST_MODEL(TAXI)
        ELIF NOT IS_PLAYER_IN_ANY_TAXI()
            taxiData.viTaxi = CREATE_VEHICLE(TAXI_GET_TAXI_MODEL_NAME(),GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(),<<3.0,1.0,10.0>>))
        ENDIF
        
        IF IS_VEHICLE_DRIVEABLE(taxiData.viTaxi)
            SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),taxiData.viTaxi)
        ENDIF
    ENDIF
    
ENDPROC

PROC TOGGLE_ROAD_NODES_IN_AREA_FOR_TAXI_OJ(VECTOR vRoadMidPt,FLOAT fRadius = TAXI_AREA_ROAD_DISABLE_RADIUS , BOOL bTurnOff = TRUE)
	FLOAT fNodeHeading
	VECTOR vNodeCoords
	
	//Grab a road node near the midpoint
	GET_CLOSEST_VEHICLE_NODE_WITH_HEADING(vRoadMidPt,vNodeCoords, fNodeHeading, NF_INCLUDE_SWITCHED_OFF_NODES)
	CDEBUG1LN(DEBUG_OJ_TAXI, "Vector at: ",vRoadMidPt, " Grabbed a node at: ", vNodeCoords, " with a heading of: ", fNodeHeading)
	VECTOR vMaxRoadPt = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vNodeCoords,GET_INVERSE_HEADING(fNodeHeading), <<fRadius,fRadius,5.0>>)
	VECTOR vMinRoadPt = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vNodeCoords,fNodeHeading, <<fRadius,fRadius,-5.0>>)
	
	vMinRoadPt.z-= 22.0
    vMaxRoadPt.z+= 22.0
	
	CDEBUG1LN(DEBUG_OJ_TAXI, " MinPt = ", vMinRoadPt, " MaxPt = ", vMaxRoadPt)
	
	 IF bTurnOff
        SET_ROADS_IN_AREA(vMinRoadPt,vMaxRoadPt,FALSE)
		CDEBUG1LN(DEBUG_OJ_TAXI, " Road nodes disabled" )
    ELSE
        SET_ROADS_BACK_TO_ORIGINAL(vMinRoadPt, vMaxRoadPt)
		CDEBUG1LN(DEBUG_OJ_TAXI, " Road nodes set back to original" )
    ENDIF
	
ENDPROC

PROC TOGGLE_ROAD_NODES_ALONG_WAYPOINT_RECORDING_PATH(STRING sVehicleRecordingName )
	INT iNumWayPointPoints, iWayPointIterator
	VECTOR vecWayPointCoords
	IF GET_IS_WAYPOINT_RECORDING_LOADED(sVehicleRecordingName)
		WAYPOINT_RECORDING_GET_NUM_POINTS(sVehicleRecordingName,iNumWayPointPoints)
		
		REPEAT iNumWayPointPoints iWayPointIterator
			WAYPOINT_RECORDING_GET_COORD(sVehicleRecordingName,iWayPointIterator,vecWayPointCoords)
			CDEBUG1LN(DEBUG_OJ_TAXI, "Iterating thru waypoint # ", iWayPointIterator)
			TOGGLE_ROAD_NODES_IN_AREA_FOR_TAXI_OJ(vecWayPointCoords,5.0,TRUE)
		ENDREPEAT
		
	ENDIF
	
ENDPROC

PROC TOGGLE_OFF_ROAD_NODES_ALONG_WAYPOINT_FROM_COORD(STRING sVehicleRecordingName , INT iStartWayPt, INT numPtsToDisable)
	INT iNumWayPointPoints, iWayPointIterator, iWayPtIteratorEndPt
	VECTOR vecWayPointCoords
	
	IF GET_IS_WAYPOINT_RECORDING_LOADED(sVehicleRecordingName)
		WAYPOINT_RECORDING_GET_NUM_POINTS(sVehicleRecordingName,iNumWayPointPoints)
		
		//Handle the endPt
		IF iStartWayPt + numPtsToDisable <= iNumWayPointPoints
			iWayPtIteratorEndPt = iStartWayPt + numPtsToDisable
		ELSE
			iWayPtIteratorEndPt = iNumWayPointPoints
		ENDIF
		CDEBUG1LN(DEBUG_OJ_TAXI, "****************** TOGGLE_OFF_ROAD_NODES_ALONG_WAYPOINT_FROM_COORD ******************")
		CDEBUG1LN(DEBUG_OJ_TAXI, "****************** Start pt = ", iStartWayPt, " End Pt = ",iWayPtIteratorEndPt, " ******************" )
		
		FOR iWayPointIterator = iStartWayPt to iWayPtIteratorEndPt -1
		
			WAYPOINT_RECORDING_GET_COORD(sVehicleRecordingName,iWayPointIterator,vecWayPointCoords)
			//CDEBUG1LN(DEBUG_OJ_TAXI, "Toggling off thru waypoint for # ", iWayPointIterator)
			TOGGLE_ROAD_NODES_IN_AREA_FOR_TAXI_OJ(vecWayPointCoords,4.0,TRUE)
			IF iWayPointIterator = iWayPtIteratorEndPt -1
			OR iWayPointIterator = iWayPtIteratorEndPt/2
				CLEAR_AREA_OF_VEHICLES(vecWayPointCoords,50,FALSE,TRUE,TRUE,TRUE)
				CDEBUG1LN(DEBUG_OJ_TAXI, "Cleared area of vehicles at = ", vecWayPointCoords)
			ENDIF
		ENDFOR
	ENDIF
	
	//
	
ENDPROC

PROC TOGGLE_ON_ROAD_NODES_ALONG_WAYPOINT_BEHIND_VEHICLE(STRING sVehicleRecordingName , INT iStartWayPt, INT numPtsToEnable)
	INT iNumWayPointPoints, iWayPointIterator, iWayPtIteratorEndPt
	VECTOR vecWayPointCoords
	
	IF GET_IS_WAYPOINT_RECORDING_LOADED(sVehicleRecordingName)
		WAYPOINT_RECORDING_GET_NUM_POINTS(sVehicleRecordingName,iNumWayPointPoints)
		
		//Handle the first section
		iWayPtIteratorEndPt = iStartWayPt
		IF iStartWayPt < numPtsToEnable
			iStartWayPt = 0
		ELSE
			iStartWayPt = iWayPtIteratorEndPt - numPtsToEnable
		ENDIF
		CDEBUG1LN(DEBUG_OJ_TAXI, "****************** TOGGLE_ON_ROAD_NODES_ALONG_WAYPOINT_BEHIND_VEHICLE ******************")
		CDEBUG1LN(DEBUG_OJ_TAXI, "****************** Start pt = ", iStartWayPt, " End Pt = ",iWayPtIteratorEndPt, " ******************" )
		
		FOR iWayPointIterator = iStartWayPt to iWayPtIteratorEndPt -1
		
			WAYPOINT_RECORDING_GET_COORD(sVehicleRecordingName,iWayPointIterator,vecWayPointCoords)
			//CDEBUG1LN(DEBUG_OJ_TAXI, "Iterating thru waypoint # ", iWayPointIterator)
			TOGGLE_ROAD_NODES_IN_AREA_FOR_TAXI_OJ(vecWayPointCoords,4.0,FALSE)
		ENDFOR
	ENDIF
	
ENDPROC


PROC TOGGLE_NODES_ALONG_WAYPOINT_RECORDING_PATH_ON_PROGRESS(TaxiStruct &myTaxiData, STRING sVehicleRecordingName, VEHICLE_INDEX vehEntity, INT &iState, INT &iNumWayPts, INT &iWayPtProgress, INT numPtsToLookAhead, FLOAT fTimeInterval = 5.0)
	
	SWITCH iState
		//Get Total Num Of Way Pts
		CASE 0 
			IF GET_IS_WAYPOINT_RECORDING_LOADED(sVehicleRecordingName)
				WAYPOINT_RECORDING_GET_NUM_POINTS(sVehicleRecordingName,iNumWayPts)
				CDEBUG1LN(DEBUG_OJ_TAXI, "Total Num Waypoints  =  ", iNumWayPts , " iState = ", iState)
				iState++
				
			ENDIF
		BREAK
		
		//See The Progress Along the Path
		CASE 1
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FOLLOW_CAR_AI)  > fTimeInterval
				IF GET_IS_WAYPOINT_RECORDING_LOADED(sVehicleRecordingName)
					IF IS_VEHICLE_DRIVEABLE(vehEntity)
						IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEntity)
							iWayPtProgress = GET_VEHICLE_WAYPOINT_PROGRESS(vehEntity)
							CDEBUG1LN(DEBUG_OJ_TAXI, " Vehicle Waypoint Progress   =  ", iWayPtProgress , " iState = ", iState)
							iState++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//Toggle Nodes Ahead off based on progress along path and turn back nodes behind the player
		CASE 2
			IF GET_IS_WAYPOINT_RECORDING_LOADED(sVehicleRecordingName)
				TOGGLE_OFF_ROAD_NODES_ALONG_WAYPOINT_FROM_COORD(sVehicleRecordingName,iWayPtProgress,numPtsToLookAhead)
				IF iWayPtProgress > 56
					TOGGLE_ON_ROAD_NODES_ALONG_WAYPOINT_BEHIND_VEHICLE(sVehicleRecordingName,iWayPtProgress,numPtsToLookAhead)
				ENDIF
				TAXI_RESET_TIMERS(myTaxiData, TT_FOLLOW_CAR_AI)
				iState = 1
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(VECTOR vRoadMidPt,BOOL bEnable = FALSE,FLOAT fRadius = TAXI_AREA_ROAD_DISABLE_RADIUS)

    VECTOR vMinRoadPt = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vRoadMidPt,1.0, <<-fRadius,-fRadius,-fRadius>>)
    VECTOR vMaxRoadPt = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vRoadMidPt,1.0, <<fRadius,fRadius,fRadius>>)
    
    vMinRoadPt.z-= 22.0
    vMaxRoadPt.z+= 22.0
    
    IF NOT bEnable
        SET_ROADS_IN_AREA(vMinRoadPt,vMaxRoadPt,FALSE)
    ELSE
        SET_ROADS_BACK_TO_ORIGINAL(vMinRoadPt, vMaxRoadPt)
        CLEAR_PED_NON_CREATION_AREA()
    ENDIF
    
    CDEBUG1LN(DEBUG_OJ_TAXI,"VMin To Clear ", "", vMinRoadPt, "", "VMax to Clear ", "", vMaxRoadPt)
ENDPROC
PROC TAXI_OJ_CLEAR_AREA_OF_ALL_PEDS_AND_SCENARIOS(SCENARIO_BLOCKING_INDEX &sbIndex, VECTOR vCenterPt, FLOAT fRadius, BOOL bCleanup = FALSE)
    
    VECTOR vMinRoadPt = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vCenterPt,1.0, <<-fRadius,-fRadius,-22.0>>)
    VECTOR vMaxRoadPt = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vCenterPt,1.0, <<fRadius,fRadius,44.0>>)
    
    //For Some reason getting an offset doesn't act on the Z value. So we hard alter them here.
    vMinRoadPt.z-= 22.0
    vMaxRoadPt.z+= 22.0
    
    IF NOT bCleanup
        
        SET_PED_NON_CREATION_AREA(vMinRoadPt, vMaxRoadPt)
        
        sbIndex = ADD_SCENARIO_BLOCKING_AREA(vMinRoadPt, vMaxRoadPt)
        
        CLEAR_AREA_OF_PEDS(vCenterPt,fRadius)
        
        
        CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_CLEAR_AREA_OF_ALL_PEDS_AND_SCENARIOS - Peds cleared")
    
    ELSE
        CLEAR_PED_NON_CREATION_AREA()
        
        REMOVE_SCENARIO_BLOCKING_AREA(sbIndex)
        
        CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_CLEAR_AREA_OF_ALL_PEDS_AND_SCENARIOS - Is uncleard for mission end.")
        
    ENDIF
    
    CDEBUG1LN(DEBUG_OJ_TAXI,"VMin To Clear ", "", vMinRoadPt, "", "VMax to Clear ", "", vMaxRoadPt)
ENDPROC
PROC SET_PED_AS_TAXI_OJ_PASSENGER(TaxiStruct &myTaxiData, PED_INDEX &ped)
    IF NOT IS_PED_INJURED(ped)
        myTaxiData.piTaxiPassenger = ped
        CDEBUG1LN(DEBUG_OJ_TAXI,"SET_PED_AS_TAXI_OJ_PASSENGER - New Taxi Passenger Set Up")
    ELSE
        SCRIPT_ASSERT("SET_PED_AS_TAXI_OJ_PASSENGER - New Taxi Passenger FAILED")
    ENDIF
    
ENDPROC
/// PURPOSE: Accessor for the mission type
///    
/// PARAMS:
///    myTaxiData - global taxi data
/// RETURNS:
///    
FUNC TAXI_MISSION_TYPES GET_TAXI_MISSION_TYPE(TaxiStruct & myTaxiData)
    RETURN myTaxiData.tTaxiOJ_MissionType
ENDFUNC


PROC TAXI_OJ_CLEAR_ALL_BLIPS(TaxiStruct & myTaxiData)
    
    //Cleanup all blips just in case
    IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
        REMOVE_BLIP(myTaxiData.blipTaxiPassenger)
        CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ passenger blip cleared")
    ENDIF
    
    IF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
        REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
        CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ dropoff blip cleared")
    ENDIF
    
    IF DOES_BLIP_EXIST(myTaxiData.blipTaxiVehicle)
        REMOVE_BLIP(myTaxiData.blipTaxiVehicle)
        CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ Taxi blip cleared")
    ENDIF
    
    
    
ENDPROC

PROC TAXI_OJ_UPDATE_BLIP_TAXI(TaxiStruct & myTaxiData,BOOL bClearAll = FALSE, BOOL bPrintObj = FALSE)
    
    //Remove all blips
    IF bClearAll
        TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
    ENDIF
    
    //Blip Taxi
    IF NOT DOES_BLIP_EXIST(myTaxiData.blipTaxiVehicle)
        myTaxiData.blipTaxiVehicle = CREATE_BLIP_ON_ENTITY(myTaxiData.viTaxi)
        
        SET_BLIP_NAME_FROM_TEXT_FILE(myTaxiData.blipTaxiVehicle,"TAXI_BLIP_CAR")
        SET_BLIP_ROUTE(myTaxiData.blipTaxiVehicle, TRUE)
        
        SET_PLAYER_LEFT_TAXI_OJ(myTaxiData)
        
        IF bPrintObj
            CLEAR_PRINTS()
            SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_RETURN,TRUE)
        ENDIF
    ENDIF

    
ENDPROC

PROC TAXI_OJ_UPDATE_BLIP_PED(TaxiStruct & myTaxiData, PED_INDEX & ped, STRING sBlipName, BOOL bClearAll = FALSE)
    
    //Remove all blips
    IF bClearAll
        TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
    ENDIF
    
    //Blip Destination
    IF NOT DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
        myTaxiData.blipTaxiPassenger = CREATE_BLIP_ON_ENTITY(ped,FALSE)
    ENDIF
    
    //Name the blip something unique
    IF NOT IS_STRING_NULL(sBlipName)
        SET_BLIP_NAME_FROM_TEXT_FILE(myTaxiData.blipTaxiPassenger,sBlipName)
    ENDIF
    
ENDPROC

/// PURPOSE:
///    Setup the exact tasks for the passenger to do when being dropped off.
///    Keeps it universal and centralized, and add variant task seqs if I chose to down the line.
PROC TAXI_OJ_TASK_PASSENGER_DROPOFF( TaxiStruct &myTaxiData)

    IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
        
		IF ARE_VECTORS_EQUAL(NULL_VECTOR(),myTaxiData.vTaxiOJ_PassengerGoToPt)
            IF myTaxiData.tTaxiOJ_MissionType = TXM_03_DEADLINE
                SET_ENTITY_HEADING(myTaxiData.piTaxiPassenger, 84.9058)
                SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger, TRUE)
            ELIF myTaxiData.tTaxiOJ_MissionType = TXM_05_TAKETOBEST
                SET_ENTITY_HEADING(myTaxiData.piTaxiPassenger, 68.3138)
                SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger, TRUE)         
            ELSE
                TASK_WANDER_STANDARD(myTaxiData.piTaxiPassenger)
            ENDIF
            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_TASK_PASSENGER_DROPOFF - Passenger is WANDERING")
        ELSE
            SEQUENCE_INDEX tempSequenceIndex
			CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
            CLEAR_SEQUENCE_TASK(tempSequenceIndex)
            OPEN_SEQUENCE_TASK(tempSequenceIndex)
				TASK_LEAVE_ANY_VEHICLE(NULL)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
                TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,myTaxiData.vTaxiOJ_PassengerGoToPt,PEDMOVEBLENDRATIO_WALK,-1)
                IF myTaxiData.tTaxiOJ_MissionType = TXM_03_DEADLINE
                    TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_AA_SMOKE")
     			ELIF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
					TASK_CLIMB_LADDER(NULL,TRUE)
                    TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 813.9421, 1172.6809, 329.7988 >>,PEDMOVEBLENDRATIO_WALK,-1)
					TASK_ACHIEVE_HEADING(NULL, 151.7794)
					TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING_POT")
					//TASK_PLAY_ANIM(NULL, "gestures@m@standing@casual", "gesture_pleased")
				ELIF myTaxiData.tTaxiOJ_MissionType = TXM_05_TAKETOBEST
					TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, myTaxiData.vTaxiOJ_PassengerGoToPt, 15)
				ELIF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
					TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, myTaxiData.vTaxiOJ_PassengerGoToPt, 15, 20000)
					TASK_WANDER_STANDARD(NULL)
				ELSE    
  					TASK_PLAY_ANIM(NULL, "oddjobs@taxi@", "base")
					TASK_PLAY_ANIM(NULL, "gestures@m@standing@casual", "gesture_pleased")
                ENDIF
				IF DOES_SCENARIO_EXIST_IN_AREA(myTaxiData.vTaxiOJ_PassengerGoToPt, 15, TRUE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, myTaxiData.vTaxiOJ_PassengerGoToPt, 15)
				ELSE
					TASK_WANDER_STANDARD(NULL)
				ENDIF
            CLOSE_SEQUENCE_TASK(tempSequenceIndex)
            TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, tempSequenceIndex)
            CLEAR_SEQUENCE_TASK(tempSequenceIndex)
            
            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_TASK_PASSENGER_DROPOFF - Passenger is HEADING TO HANGOUT PT")
        ENDIF

        SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger,TRUE)
    ENDIF
    
    
ENDPROC


PROC TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER( TaxiStruct &myTaxiData, ENTER_EXIT_VEHICLE_FLAGS eFlags = 0,/*BOOL bPedNeeded = FALSE,*/BOOL bIgnoreDropPt = FALSE)
    
    IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
        
        //Set attributes
        SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiData.piTaxiPassenger,FALSE)
        
        CLEAR_ENTITY_LAST_DAMAGE_ENTITY(myTaxiData.piTaxiPassenger)
        
        SET_PED_FLEE_ATTRIBUTES(myTaxiData.piTaxiPassenger, FA_USE_COVER | FA_USE_VEHICLE, FALSE)
        
        SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_ALWAYS_FLEE, TRUE)
        
		CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
		
		IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.piTaxiPassenger,myTaxiData.vTaxiOJ_PassengerGoToPt) <= TAXI_OJ_DIST_TO_WALK_TO_DEST
		AND NOT IS_VECTOR_ZERO(myTaxiData.vTaxiOJ_PassengerGoToPt)
		AND NOT bIgnoreDropPt
			TAXI_OJ_TASK_PASSENGER_DROPOFF(myTaxiData)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER USed to send passenger to go to pt")
		ELSE
	        //Task
	        SEQUENCE_INDEX tempAIseqID
	    	
			SET_PED_FLEE_ATTRIBUTES(myTaxiData.piTaxiPassenger, FA_DISABLE_COWER, TRUE)
			SET_PED_FLEE_ATTRIBUTES(myTaxiData.piTaxiPassenger, FA_DISABLE_HESITATE_IN_VEHICLE, TRUE)
			
	        OPEN_SEQUENCE_TASK(tempAIseqID)
	            TASK_LEAVE_ANY_VEHICLE(NULL,0,eFlags)
				IF myTaxiData.tTaxiOJ_FailType = TFS_ABANDONED_PASSENGER
					TASK_WANDER_STANDARD(NULL)
	            ELSE
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000.0, -1)
				ENDIF
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
	        CLOSE_SEQUENCE_TASK(tempAIseqID)
	        TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, tempAIseqID)
	        CLEAR_SEQUENCE_TASK(tempAIseqID)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER Called In Here")
		ENDIF
		
        SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger, TRUE)
        
//		IF NOT bPedNeeded
//            SET_PED_AS_NO_LONGER_NEEDED(myTaxiData.piTaxiPassenger)
//        ENDIF
    ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER - ped can't flee because of injury")
    ENDIF
ENDPROC
FUNC enumCompletionPercentageEntries GET_TAXI_OJ_COMPLETION_ENUM(TAXI_MISSION_TYPES eMissionType)
    enumCompletionPercentageEntries eCompletionEntryToReturn
    
    SWITCH eMissionType
        

        
        // REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_TAX1 )
        CASE TXM_01_NEEDEXCITEMENT
            eCompletionEntryToReturn = CP_OJ_TAX1
        BREAK
        
		// REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_TAX2 )
        CASE TXM_02_TAKEITEASY
            eCompletionEntryToReturn = CP_OJ_TAX2
        BREAK
		
        // REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_TAX3 )
        CASE TXM_03_DEADLINE
            eCompletionEntryToReturn = CP_OJ_TAX3
        BREAK

        // REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_TAX4 )
        CASE TXM_04_GOTYOURBACK
            eCompletionEntryToReturn = CP_OJ_TAX4
        BREAK
        
        // REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_TAX5 )
        CASE TXM_05_TAKETOBEST
            eCompletionEntryToReturn = CP_OJ_TAX5
        BREAK
        
        // REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_TAX6 )
//        CASE TXM_06_IKNOWWAY
//            eCompletionEntryToReturn = CP_OJ_TAX6
//        BREAK
        
        // REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_TAX7 )
        CASE TXM_07_CUTYOUIN
            eCompletionEntryToReturn = CP_OJ_TAX7
        BREAK
        
        // REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_TAX8 )
        CASE TXM_08_GOTYOUNOW
            eCompletionEntryToReturn = CP_OJ_TAX8
        BREAK
        
        // REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_TAX9 )
        CASE TXM_09_CLOWNCAR
            eCompletionEntryToReturn = CP_OJ_TAX9
        BREAK
		   
        // REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_TAX10 )
        CASE TXM_10_FOLLOWTHATCAR
            eCompletionEntryToReturn = CP_OJ_TAX10
        BREAK
		
		CASE TXM_PROCEDURAL
            //eCompletionEntryToReturn = CP_OJ_TAX10
        BREAK


        DEFAULT
            SCRIPT_ASSERT("GET_TAXI_OJ_COMPLETION_ENUM - Somehow Taxi OJ Completion Mission was invalid. Please bug this for John R.Diaz")
            eCompletionEntryToReturn = CP_OJ_TAX1
        BREAK
    ENDSWITCH
    
    RETURN eCompletionEntryToReturn
    
ENDFUNC

PROC ADD_TAXI_OJ_SPEED_REDUCTION_VOLUMES(TaxiStruct &myTaxiData,VECTOR vPos)
    
    myTaxiData.iTaxiOJ_SpeedReducVols[0] = ADD_ROAD_NODE_SPEED_ZONE(vPos, 20, 5)
    //myTaxiData.iTaxiOJ_SpeedReducVols[1] = ADD_ROAD_NODE_SPEED_ZONE(vPos, 50, 10)
    //myTaxiData.iTaxiOJ_SpeedReducVols[2] = ADD_ROAD_NODE_SPEED_ZONE(vPos, 10, 1)

    CDEBUG1LN(DEBUG_OJ_TAXI,"Speed Zone created for Taxi OJ")
    
ENDPROC


PROC REMOVE_TAXI_OJ_SPEED_REDUCTION_VOLUMES(TaxiStruct &myTaxiData)

    REMOVE_ROAD_NODE_SPEED_ZONE(myTaxiData.iTaxiOJ_SpeedReducVols[0])
    //REMOVE_ROAD_NODE_SPEED_ZONE(myTaxiData.iTaxiOJ_SpeedReducVols[1])
    //REMOVE_ROAD_NODE_SPEED_ZONE(myTaxiData.iTaxiOJ_SpeedReducVols[2])
    
    CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Oddjob - Road Node Speed Zones cleaned up")
ENDPROC

FUNC BOOL IS_TAXI_ODDJOB_CLOSE_TO_COORD(TaxiStruct &myTaxiData, VECTOR vDest, FLOAT fDistance)
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi,vDest) < fDistance
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEANUP_TAXI_OJ_PASSENGER(TaxiStruct &myTaxiData)
	IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPassenger)
	    IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
	        
			IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiData.piTaxiPassenger)
				RESET_PED_LAST_VEHICLE(myTaxiData.piTaxiPassenger)
			ENDIF
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiData.piTaxiPassenger,FALSE)
	        SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, myTaxiData.relPassenger, RELGROUPHASH_PLAYER)
			
			//Stop the peds from pointing at you
			IF IS_ENTITY_PLAYING_ANIM(myTaxiData.piTaxiPassenger, "oddjobs@towingcome_here", "come_here_idle_a")
				STOP_ANIM_TASK(myTaxiData.piTaxiPassenger,"oddjobs@towingcome_here", "come_here_idle_a")
			ELIF IS_ENTITY_PLAYING_ANIM(myTaxiData.piTaxiPassenger, "oddjobs@towingcome_here", "come_here_idle_c")
				STOP_ANIM_TASK(myTaxiData.piTaxiPassenger,"oddjobs@towingcome_here", "come_here_idle_c")
			ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(myTaxiData.piTaxiPassenger,"gestures@m@standing@casual", "gesture_nod_yes_hard")
				STOP_ANIM_TASK(myTaxiData.piTaxiPassenger,"gestures@m@standing@casual", "gesture_nod_yes_hard")
			ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(myTaxiData.piTaxiPassenger,"gestures@m@standing@casual", "gesture_you_hard")
				STOP_ANIM_TASK(myTaxiData.piTaxiPassenger,"gestures@m@standing@casual", "gesture_you_hard")
			ENDIF
			
			SET_PED_AS_NO_LONGER_NEEDED(myTaxiData.piTaxiPassenger)
			
	    ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_TAXI_ODDJOB(TaxiStruct &myTaxiData, BOOL bCleanupPassenger = TRUE)
	TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
    
    //Kill any conversations and prints
    IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
        KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
    ENDIF
    
	Set_Leave_Area_Flag_For_All_Blipped_Missions()
	
    CLEAR_PRINTS()
    CLEAR_HELP()
	HANG_UP_AND_PUT_AWAY_PHONE()
  
	 //Clean up Taxi Here-----------------------------
    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
        //ENTITY_INDEX tempTaxiEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(myTaxiData.viTaxi)
		SET_VEHICLE_HAS_STRONG_AXLES(myTaxiData.viTaxi, FALSE)
        REMOVE_VEHICLE_STUCK_CHECK(myTaxiData.viTaxi)
        REMOVE_VEHICLE_UPSIDEDOWN_CHECK(myTaxiData.viTaxi)
        //SET_ENTITY_AS_NO_LONGER_NEEDED(tempTaxiEntity)
    ENDIF
    
    //Turn the generators back on if need be
    TAXI_PREP_TURN_OFF_VEHICLE_GENS(myTaxiData.vTaxiOJPickup,TRUE)

    //Setting the world back to normal
    TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(myTaxiData.vTaxiOJPickup,TRUE)
    
    //Remove passenger from the dialogue struct
    REMOVE_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,3)
    
    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_WillFlyThroughWindscreen,TRUE)
    
	IF IS_OJ_TAXI_PLAYER_CONTROL_OFF()
		CDEBUG1LN(DEBUG_OJ_TAXI,"CLEANUP TAXI ODDJOB - Player control off, setting back on")
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	// do these just in case player quits during a cutscene.
	SET_PAD_CAN_SHAKE_DURING_CUTSCENE(TRUE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	IF DOES_CAM_EXIST(myTaxiData.camTaxi)
		DESTROY_CAM(myTaxiData.camTaxi)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	ENDIF
	
    IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
        CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
        SET_VEHICLE_MODEL_IS_SUPPRESSED(TAXI_GET_TAXI_MODEL_NAME(), FALSE)
    ENDIF
    
    //Passenger cleanup
	IF bCleanupPassenger
		CLEANUP_TAXI_OJ_PASSENGER(myTaxiData)
	ENDIF
    
    REMOVE_TAXI_OJ_SPEED_REDUCTION_VOLUMES(myTaxiData)
	
	REMOVE_ANIM_DICT("gestures@m@standing@casual") 
	REMOVE_ANIM_DICT("oddjobs@taxi@")
	REMOVE_ANIM_DICT("oddjobs@towingcome_here")
	
	IF NETWORK_IS_SIGNED_ONLINE()
		CDEBUG1LN(DEBUG_OJ_TAXI, "online, writing to social club leaderboard")
		WRITE_TAXI_SCLB_DATA(myTaxiData.tTaxiOJ_MissionType)
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())							
		SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
	ENDIF
	
	PLAYSTATS_ODDJOB_DONE(ROUND(GET_TIMER_IN_SECONDS_SAFE(timerTotal)*1000), ENUM_TO_INT(MINIGAME_TAXI))
	PRINTLN("Calling PLAYSTATS_ODDJOB_DONE(", ROUND(GET_TIMER_IN_SECONDS_SAFE(timerTotal))*1000, ")")
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"CLEANUP_TAXI_ODDJOB - SUCCESS")
ENDPROC

PROC SET_TAXI_ODDJOB_PASSED(TaxiStruct &myTaxiData)
	TAXI_STATS_UPDATE(TAXI_STAT_FARES_COMPLETED)
	
	IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_SUCCESS)
	    AWARD_ACHIEVEMENT(ACH12)
	ENDIF
	
	SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_PASSED)

    //Set the completion percentage in the case that the controller isn't running but you have passed this mission
    IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_CONTROLLER_RUNNING)
        REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (GET_TAXI_OJ_COMPLETION_ENUM(GET_TAXI_MISSION_TYPE(myTaxiData)))
    ENDIF               
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"SET_TAXI_ODDJOB_PASSED - SUCCESS")
ENDPROC

/// PURPOSE:
///    Tells the controller what the final score percent was for a successful taxi mission.
/// PARAMS:
///    bPassToo - Also sets the flag telling the controller that the mission is passed. The controller does nothing until it sees this flag is passed.
PROC TAXI_MISSION_END(BOOL bPass, TaxiStruct &myTaxiData, BOOL bCleanupPassenger = TRUE)

    IF bPass
        SET_TAXI_ODDJOB_PASSED(  myTaxiData ) 
		
		IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
			SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger,PCF_CanSayFollowedByPlayerAudio,TRUE)
		ENDIF
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"(:)(:)(:)(:)(:)(:)(:)(:)(:)(:)(:)(:) TAXI_MISSION_END - SUCCESS (:)(:)(:) (:)(:)(:) (:)(:)(:) (:)(:)(:)")
    ELSE
        TAXI_STATS_UPDATE(TAXI_STAT_FARES_FAILED)
		CDEBUG1LN(DEBUG_OJ_TAXI,"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx TAXI_MISSION_END - FAILURE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
    ENDIF
    
	CLEANUP_TAXI_ODDJOB(myTaxiData, bCleanupPassenger)
ENDPROC

PROC TAXI_SCRIPT_FAILED(TaxiStruct &myTaxiData)
    
    TAXI_MISSION_END(FALSE,myTaxiData)
	
	IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
	ENDIF
    
//    IF  myTaxiData.tTaxiOJ_FailType = TFS_PASSENGER_DIED
//        PRINT_NOW("TAXI_OBJ_F_DEAD",DEFAULT_GOD_TEXT_TIME,1)
//		
//	ELIF IS_STRING_NULL_OR_EMPTY(myTaxiData.sTaxiOJ_FailGodLabel)
//		PRINT_NOW("TAXI_OBJ_FAIL", DEFAULT_GOD_TEXT_TIME, 1)
//    
//	ELSE
//        PRINT_NOW(myTaxiData.sTaxiOJ_FailGodLabel, DEFAULT_GOD_TEXT_TIME, 1)
//    ENDIF
    
    CDEBUG1LN(DEBUG_OJ_TAXI,"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  Fail During State: "," ", myTaxiData.sTaxiOJ_RideState, " ", myTaxiData.sTaxiOJ_Reason4Fail, " xxxxxxxxxxxxxxxxxxxxxxx" )
    
ENDPROC

/// PURPOSE:
///    Helper function that initializes the BONUS_FIELD struct
/// PARAMS:
///    iBonusInfo - The BONUS_FIELD struct to be initialized
///    description - The string ID for the description of the bonus
///    cash - The cash award for the bonus.
PROC TAXI_INITIALIZE_BONUS_FIELD( BONUS_FIELD & iBonusInfo, STRING description, INT cash )
    iBonusInfo.status = 0
    iBonusInfo.cash = cash
    iBonusInfo.description = description
ENDPROC

/// PURPOSE:
///    Helper function that fully initializes the bonus array and inserts into the main taxi struct
/// PARAMS:
///    myTaxiData - The main taxi struct
///    iBonusInfo - The bonus array to be inserted.
PROC TAXI_INITIALIZE_BONUS_INFO(TaxiStruct & myTaxiData, BONUS_FIELD & iBonusInfo[])

    IF COUNT_OF(iBonusInfo) <= MAX_BONUS_INFO
            
        INT iIter = 0
        
        FOR iIter = 0 TO MAX_BONUS_INFO - 1
            CLEAR_BIT( myTaxiData.iTaxiOJ_CashBonusInfo[iIter].status, BONUS_STATUS_INITIALIZED )
        ENDFOR
        
        iIter = 0
        
        REPEAT COUNT_OF(iBonusInfo) iIter
            myTaxiData.iTaxiOJ_CashBonusInfo[iIter] = iBonusInfo[iIter]
            SET_BIT( myTaxiData.iTaxiOJ_CashBonusInfo[iIter].status, BONUS_STATUS_INITIALIZED )
            CLEAR_BIT( myTaxiData.iTaxiOJ_CashBonusInfo[iIter].status, BONUS_STATUS_AWARDED )
        ENDREPEAT
    ELSE
        SCRIPT_ASSERT("TAXI_INITIALIZE_BONUS_BITMASK - trying to pass in too much bonus info.  Bad scripter.  Bad!")
    ENDIF
    
ENDPROC

/// PURPOSE:
///    Marks a bonus as being awarded to be processed and rewarded later on at mission completion.
/// PARAMS:
///    myTaxiData - The main taxi struct
///    bonusIndex - The index which defines which bonus is to be awarded.
PROC TAXI_SET_BONUS_AWARD(TaxiStruct &myTaxiData, INT bonusIndex)
    IF bonusIndex < MAX_BONUS_INFO
        SET_BIT( myTaxiData.iTaxiOJ_CashBonusInfo[bonusIndex].status, BONUS_STATUS_AWARDED )
        myTaxiData.iTaxiOJ_SpecialCash += myTaxiData.iTaxiOJ_CashBonusInfo[bonusIndex].cash
    ENDIF   
ENDPROC
PROC TAXI_OVERRIDE_BONUS_AWARD(TaxiStruct &myTaxiData, INT &thisBonusField, INT bonusIndex, INT iBonusAmount)
    IF bonusIndex < MAX_BONUS_INFO
        thisBonusField = iBonusAmount
        myTaxiData.iTaxiOJ_CashBonusInfo[bonusIndex].cash = iBonusAmount
    ENDIF
ENDPROC


/// PURPOSE:
///    Clears a bonus from being awarded.
/// PARAMS:
///    myTaxiData - The main taxi struct
///    bonusIndex - The index which defines which bonus is to be cleared.
PROC TAXI_CLEAR_BONUS_AWARD(TaxiStruct &myTaxiData, INT bonusIndex)

    IF bonusIndex < MAX_BONUS_INFO
        CLEAR_BIT( myTaxiData.iTaxiOJ_CashBonusInfo[bonusIndex].status, BONUS_STATUS_AWARDED )
    ENDIF
    
ENDPROC

/// PURPOSE:
///    Clears all the bonuses in this mission.  Should be used on just about any failure condition.
/// PARAMS:
///    myTaxiData - The main taxi struct
PROC TAXI_CLEAR_BONUS_AWARD_ALL(TaxiStruct &myTaxiData)

    INT iIter = 0
    REPEAT COUNT_OF(myTaxiData.iTaxiOJ_CashBonusInfo) iIter
        CLEAR_BIT( myTaxiData.iTaxiOJ_CashBonusInfo[iIter].status, BONUS_STATUS_AWARDED )
    ENDREPEAT
    
ENDPROC

/// PURPOSE: Used to advance states during a taxi oddjob and fill appropriate info.
///    
/// PARAMS:
///    myTaxiData - Global taxi data
///    trState -    the state to advance to
PROC TAXI_JOB_SET_NEXT_STATE(TaxiStruct &myTaxiData, TAXI_RIDE_STATE trState)
    
    SWITCH trState
        
        CASE TRS_STREAMING						myTaxiData.sTaxiOJ_RideState = "TRS_STREAMING"						BREAK
        
        CASE TRS_FINDING_LOCATION 				myTaxiData.sTaxiOJ_RideState = " TRS_FINDING_LOCATION "				BREAK
        
        CASE TRS_SPAWNING 						myTaxiData.sTaxiOJ_RideState = " TRS_SPAWNING "						BREAK
        
        CASE TRS_SPAWN_CAR						myTaxiData.sTaxiOJ_RideState = " TRS_SPAWN_CAR " 					BREAK
        
        CASE TRS_MANAGE_PICKUP					myTaxiData.sTaxiOJ_RideState = " TRS_MANAGE_PICKUP "				BREAK
        
		CASE TRS_PASSENGER_ENTER				myTaxiData.sTaxiOJ_RideState = " TRS_PASSENGER_ENTER "				BREAK
		
        CASE TRS_WAIT_FOR_TIME					myTaxiData.sTaxiOJ_RideState = " TRS_WAIT_FOR_TIME "				BREAK
        
        CASE TRS_WAIT_FOR_TAIL					myTaxiData.sTaxiOJ_RideState = " TRS_WAIT_FOR_TAIL "				BREAK
        
        CASE TRS_DRIVING_PASSENGER				myTaxiData.sTaxiOJ_RideState = " TRS_DRIVING_PASSENGER "			BREAK
        
        CASE TRS_WAIT_FOR_FULL_STOP				myTaxiData.sTaxiOJ_RideState = " TRS_WAIT_FOR_FULL_STOP "			BREAK
        
		CASE TRS_PRE_CUTSCENE					myTaxiData.sTaxiOJ_RideState = " TRS_PRE_CUTSCENE "					BREAK
		
        CASE TRS_SWITCH_JOB						myTaxiData.sTaxiOJ_RideState = " TRS_SWITCH_JOB "					BREAK
        
        CASE TRS_CUTSCENE						myTaxiData.sTaxiOJ_RideState = " TRS_CUTSCENE "						BREAK
		
		CASE TRS_CUTSCENE_02       				myTaxiData.sTaxiOJ_RideState = " TRS_CUTSCENE_02 "					BREAK
        
		CASE TRS_CHASE_DRIVER      				myTaxiData.sTaxiOJ_RideState = " TRS_CHASE_DRIVER "					BREAK
        
		CASE TRS_SAVE_DAMSEL       				myTaxiData.sTaxiOJ_RideState = " TRS_SAVE_DAMSEL "					BREAK
        
		CASE TRS_REVEAL_DESTINATION 			myTaxiData.sTaxiOJ_RideState = " TRS_REVEAL_DESTINATION "			BREAK
		
        CASE TRS_WAIT_PARK          			myTaxiData.sTaxiOJ_RideState = " TRS_WAIT_PARK "					BREAK
        
        CASE TRS_SEND_TO_STORE      			myTaxiData.sTaxiOJ_RideState = " TRS_SEND_TO_STORE "				BREAK
        
        CASE TRS_WAIT_1ST_STOP      			myTaxiData.sTaxiOJ_RideState = " TRS_WAIT_1ST_STOP "				BREAK
    
        CASE TRS_DROPPING_OFF       			myTaxiData.sTaxiOJ_RideState = " TRS_DROPPING_OFF "        			BREAK
        
        CASE TRS_ESCAPE_POLICE      			myTaxiData.sTaxiOJ_RideState = " TRS_ESCAPE_POLICE "				BREAK
        
        CASE TRS_POLICE_ESCAPED     			myTaxiData.sTaxiOJ_RideState = " TRS_POLICE_ESCAPED "				BREAK
        
        CASE TRS_TIE_UP_LOSE_ENDS   			myTaxiData.sTaxiOJ_RideState = " TRS_TIE_UP_LOSE_ENDS "				BREAK
        
        CASE TRS_REGULAR_PAYMENT    			myTaxiData.sTaxiOJ_RideState = " TRS_REGULAR_PAYMENT "				BREAK
        
        CASE TRS_ELIMATE_ALL_ENEMIES 			myTaxiData.sTaxiOJ_RideState = " TRS_ELIMATE_ALL_ENEMIES "			BREAK
		
        CASE TRS_SPECIAL_ENDING	     			myTaxiData.sTaxiOJ_RideState = " TRS_SPECIAL_ENDING "				BREAK
        
        CASE TRS_SCORECARD_GRADE     			myTaxiData.sTaxiOJ_RideState = " TRS_SCORECARD_GRADE "				BREAK
        
        CASE TRS_CLEANUP	         			myTaxiData.sTaxiOJ_RideState = " TRS_CLEANUP "						BREAK
        
        CASE TRS_WAIT_FOR_PASSENGER  			myTaxiData.sTaxiOJ_RideState = " TRS_WAIT_FOR_PASSENGER "			BREAK
		
		DEFAULT				   
			myTaxiData.sTaxiOJ_RideState = " TRS IS UNIDENTIFIED - FIX THIS "
        BREAK

	ENDSWITCH
    
    myTaxiData.tTaxiOJ_RideState = trState
    
    CDEBUG1LN(DEBUG_OJ_TAXI," >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  [TAXI_RIDE_STATE] = ", myTaxiData.sTaxiOJ_RideState," <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" )
        
ENDPROC
FUNC BOOL CAN_TAXI_PASSENGER_BE_HEARD(TaxiStruct &myTaxiData)
    IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
        IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),myTaxiData.piTaxiPassenger) < TAXI_DIALOGUE_MAX_DISTANCE
		AND NOT IS_ENTITY_OCCLUDED(myTaxiData.piTaxiPassenger)
            RETURN TRUE
        ENDIF
	
		CDEBUG1LN(DEBUG_OJ_TAXI," Passenger could not be heard because he/she was too far out of range at :  ", GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),myTaxiData.piTaxiPassenger), " OR occlusion value is : " , IS_ENTITY_OCCLUDED(myTaxiData.piTaxiPassenger) )    
   
    ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for entering, not being in a vehicle.  true when ped puts hand on the door handle
/// PARAMS:
///    myTaxi - 
/// RETURNS:
///    
FUNC BOOL IS_PASSENGER_ENTERING_TAXI( TaxiStruct &myTaxi)//PED_INDEX ped, VEHICLE_INDEX vehicle)
    //INT iThrottle = 2
    IF IS_VEHICLE_DRIVEABLE(myTaxi.viTaxi)
        IF NOT IS_PED_INJURED(myTaxi.piTaxiPassenger)
			IF (IS_PED_GETTING_INTO_A_VEHICLE(myTaxi.piTaxiPassenger)
				AND (GET_GAME_TIMER() - iEnterDelayTimer) > 500)
			OR IS_PED_IN_VEHICLE(myTaxi.piTaxiPassenger,myTaxi.viTaxi, TRUE)
                RETURN TRUE
            ENDIF
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

/// PURPOSE: Safe check for a passenger being in the taxi
///    
/// PARAMS:
///    ped - the ped to check
///    vehicle - the vehicle to check for the ped
/// RETURNS:
///    
FUNC BOOL IS_PASSENGER_IN_TAXI( TaxiStruct &myTaxi)//PED_INDEX ped, VEHICLE_INDEX vehicle)
    //INT iThrottle = 2
    IF IS_VEHICLE_DRIVEABLE(myTaxi.viTaxi)
        IF NOT IS_PED_INJURED(myTaxi.piTaxiPassenger)
			IF IS_PED_IN_VEHICLE(myTaxi.piTaxiPassenger,myTaxi.viTaxi, TRUE)
            //IF IS_PED_SITTING_IN_VEHICLE(myTaxi.piTaxiPassenger,myTaxi.viTaxi)
                RETURN TRUE
            ENDIF
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC
FUNC BOOL IS_PASSENGER_IN_TAXI_SEAT(TaxiStruct &myTaxi, VEHICLE_SEAT vSeat = VS_BACK_LEFT)
    IF IS_VEHICLE_DRIVEABLE(myTaxi.viTaxi)
        IF NOT IS_PED_INJURED(myTaxi.piTaxiPassenger)
            IF IS_PED_IN_VEHICLE(myTaxi.piTaxiPassenger,myTaxi.viTaxi)
                IF NOT IS_VEHICLE_SEAT_FREE(myTaxi.viTaxi,vSeat)
                    RETURN TRUE
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC 

FUNC BOOL TAXI_SHUFFLE_PASSENGER_SEAT(TaxiStruct &myTaxi,VEHICLE_SEAT vSeat = VS_BACK_LEFT)
    IF NOT IS_PED_INJURED(myTaxi.piTaxiPassenger)
        IF IS_PED_IN_VEHICLE(myTaxi.piTaxiPassenger,myTaxi.viTaxi)
            IF IS_VEHICLE_SEAT_FREE(myTaxi.viTaxi,vSeat)
                IF GET_SCRIPT_TASK_STATUS(myTaxi.piTaxiPassenger,SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) <> PERFORMING_TASK
                    TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(myTaxi.piTaxiPassenger,myTaxi.viTaxi)
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SHUFFLE_PASSENGER_SEAT")
					RETURN TRUE
                ENDIF
            ENDIF
        ENDIF
    ENDIF
	
	RETURN FALSE
ENDFUNC


PROC PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(TaxiStruct &myTaxiData,STRING sRootLabel)
    IF CAN_TAXI_PASSENGER_BE_HEARD(myTaxiData)
		CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID,sRootLabel,CONV_PRIORITY_VERY_HIGH)
		CDEBUG1LN(DEBUG_OJ_TAXI,"Playing Conversation if player is in range: ", sRootLabel)
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"Player is not in range so not playing: ", sRootLabel)
    ENDIF
	
	
ENDPROC
/// PURPOSE: Handles the end of a taxi mission gracefully and with output
///    
/// PARAMS:
///    myTaxiData - our taxi object that has our Timer, wants a failstring, and will hold our FAIL flag
///    theFailString - Our specific Fail string
PROC TAXI_SET_FAIL(TaxiStruct &myTaxiData, STRING theFailString, TAXI_FAIL_STRING tfsString = TFS_TIME_EXPIRED)
    
	KILL_ANY_CONVERSATION()
	
	SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
	
	TEXT_LABEL_23 tempFailText = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	
	IF NOT IS_STRING_NULL_OR_EMPTY( tempFailText )	
	AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND NOT ARE_STRINGS_EQUAL( tempFailText , "NULL")
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"xxxxxxxxxxxxxxxxxx NOT SETTING Taxi Oddjob FAIL Because Conversation is still going xxxxxxxxxxxxxxxxxx " )
		CDEBUG1LN(DEBUG_OJ_TAXI,"xxxxxxxxxxxxxxxxxx conversation is = ", tempFailText )
	
	
	ELSE
		
		CLEAR_PRINTS()
    	CLEAR_HELP()
		
		tempFailText = myTaxiData.sTaxiOJ_DXMissionID
		
		//Handle pickup case
		IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
			IF NOT IS_PASSENGER_IN_TAXI(myTaxiData)
				IF IS_ENTITY_PLAYING_ANIM(myTaxiData.piTaxiPassenger, "oddjobs@towingcome_here", "come_here_idle_a")
					STOP_ANIM_TASK(myTaxiData.piTaxiPassenger,"oddjobs@towingcome_here", "come_here_idle_a")
				ELIF IS_ENTITY_PLAYING_ANIM(myTaxiData.piTaxiPassenger, "oddjobs@towingcome_here", "come_here_idle_c")
					STOP_ANIM_TASK(myTaxiData.piTaxiPassenger,"oddjobs@towingcome_here", "come_here_idle_c")
				ELIF IS_ENTITY_PLAYING_ANIM(myTaxiData.piTaxiPassenger, "gestures@m@standing@casual", "gesture_nod_yes_hard")
					STOP_ANIM_TASK(myTaxiData.piTaxiPassenger,"gestures@m@standing@casual", "gesture_nod_yes_hard")
				ENDIF
			ENDIF
		ENDIF
		
        myTaxiData.bTaxiOJ_Failed = TRUE
        myTaxiData.sTaxiOJ_Reason4Fail = theFailString
        myTaxiData.tTaxiOJ_FailType = tfsString
        TAXI_RESET_TIMERS(myTaxiData, TT_FAILDELAY)
        
        IF tfsString = TFS_ABANDONED_PASSENGER
        OR tfsString = TFS_TAXI_GARBAGE_COLLECTED
           	
			IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
				 IF myTaxiData.bSecondPassenger
					tempFailText += "_aband2"
				ELSE
					tempFailText += "_aband1"
				ENDIF
			
            	PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            ELSE
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "TAXI_FAIL", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
				ENDIF
			ENDIF
			
			myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_FAIL"
            
        ELIF tfsString = TFS_AGGROD
            IF myTaxiData.bSecondPassenger
				tempFailText += "_aggro2"
			ELSE
	            tempFailText += "_aggro"
    		ENDIF  
			
            PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_SPK"
			
        ELIF  myTaxiData.tTaxiOJ_FailType = TFS_PASSENGER_DIED
			myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_DEAD"
			
		ELIF tfsString = TFS_LOST_CAR
            tempFailText += "_lost1"
            PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_FAIL"
			
        ELIF tfsString = TFS_SPOTTED
            tempFailText += "_spotd1"
            
            PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_SPT"
        
        ELIF tfsString = TFS_WANTED
            tempFailText += "_wntd1"
            
            PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_SPT"
            
        ELIF tfsString = TFS_SPOOKED
            tempFailText += "_spook"
            PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_SPK"
			
			IF IS_PASSENGER_IN_TAXI(myTaxiData)
				IF IS_VEHICLE_STOPPED(myTaxiData.viTaxi)
        			TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER(myTaxiData,ECF_JUMP_OUT)
				ELSE
					TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER(myTaxiData)
				ENDIF
			ENDIF
        
		ELIF tfsString = TFS_HIT2
            tempFailText += "_hit2"
            
			PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_SPK"
		
		ELIF tfsString = TFS_JACKED
            tempFailText += "_jak"
            PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_SPT"
            
        ELIF tfsString = TFS_PASSENGER_SHOT
			IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
				IF myTaxiData.bSecondPassenger
					tempFailText += "_shot2"
				ELSE
		            tempFailText += "_shot1"
	    		ENDIF        
	            PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
			ELSE
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "CALL_COPS_COMMIT", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
				ENDIF
			ENDIF
            SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),1)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_SHT"
        
		ELIF tfsString = TFS_CHASEE_SHOT
            tempFailText += "_shot1"
            
            PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),1)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_SPT"
			
		ELIF tfsString = TFS_CHASEE_KILLED
            tempFailText += "_shot1"
            
            PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),1)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_CK"
			
	   
		ELIF tfsstring = TFS_UNDER_WATER
			TAXI_STATS_UPDATE(TAXI_STAT_FUN_NUM_WRECKED)
			iTaxiSCStats[TAXISC_CARS_WRECKED] = 1
			
			IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
				IF myTaxiData.bSecondPassenger
					tempFailText += "_noDri2"
				ELSE
					tempFailText += "_noDri1"
				ENDIF
			
            	PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            ELSE
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "GENERIC_INSULT_MED", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
				ENDIF
			ENDIF
			
			TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER(myTaxiData)
			
            PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_WAT"
			
        ELIF tfsString = TFS_TAXI_DISABLED
            TAXI_STATS_UPDATE(TAXI_STAT_FUN_NUM_WRECKED)
			iTaxiSCStats[TAXISC_CARS_WRECKED] = 1
			
			IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
				IF myTaxiData.bSecondPassenger
					tempFailText += "_noDri2"
				ELSE
					tempFailText += "_noDri1"
				ENDIF
			
            	PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            ELSE
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "TAXI_FAIL", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
				ENDIF
			ENDIF
			
            PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_DR"
		
		ELIF tfsString = TFS_TAXI_ENGINE_FIRE
            TAXI_STATS_UPDATE(TAXI_STAT_FUN_NUM_WRECKED)
			iTaxiSCStats[TAXISC_CARS_WRECKED] = 1
			
			IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
				IF myTaxiData.bSecondPassenger
					tempFailText += "_noDri2"
				ELSE
					tempFailText += "_noDri1"
				ENDIF
			
            	PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
            ELSE
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "TAXI_FAIL", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
				ENDIF
			ENDIF
			
			
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_F_FIRE"
			
		ELIF tfsString = TFS_TAXI_CANCELLED
		
			tempFailText = "OJTX_QUIT_"
			TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER_PROCEDURAL(tempFailText)
		
			CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, "OJTXAUD", tempFailText, CONV_PRIORITY_MEDIUM)
		
			myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_CNCL"
    	
		ELIF tfsString = TFS_TAXI_STOPPED
			
			IF myTaxiData.bSecondPassenger
				tempFailText += "_aband2"
			ELSE
				tempFailText += "_fail1"
			ENDIF
			
			PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData, tempFailText)
		
			myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_FAIL"
			
		ELSE
            
			IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
	            IF (tfsString <> TFS_DEBUG_FAIL)
	                tempFailText += "_Fail1"
	            ELSE
	                tempFailText += "_aband1"
	            ENDIF
	            
	            IF myTaxiData.tTaxiOJ_RideState > TRS_MANAGE_PICKUP
	                PLAY_CONVERSATION_IF_PASSENGER_IS_IN_RANGE(myTaxiData,tempFailText)
	            ENDIF
			ELSE
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "TAXI_FAIL", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
				ENDIF
			ENDIF
			
            myTaxiData.sTaxiOJ_FailGodLabel = "TAXI_OBJ_FAIL"
			
			TAXI_STATS_UPDATE(TAXI_STAT_FARES_EXPIRED)
        ENDIF
	       
		TAXI_RESET_TIMERS(myTaxiData,TT_FAILDELAY,0,TRUE)
		CDEBUG1LN(DEBUG_OJ_TAXI,"XXXXXXXXXXxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx TAXI ODDJOB FAIL xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxXXXXXXXXXXXXXXX ")
        CDEBUG1LN(DEBUG_OJ_TAXI,"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx Fail Reason:  ", myTaxiData.sTaxiOJ_Reason4Fail, " xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx " )
	
		
	ENDIF
	
ENDPROC
PROC SET_TAXI_EMERGENCY_FAIL(TaxiStruct &myTaxiData, TAXI_FAIL_STRING tfsString = TFS_TIME_EXPIRED)
    myTaxiData.bEmergencyDlgStop = TRUE
    myTaxiData.tfsEmergencyFailString = tfsString
ENDPROC
FUNC BOOL IS_TAXI_EMERGENCY_FAIL_SET(TaxiStruct &myTaxiData)
    RETURN myTaxiData.bEmergencyDlgStop
ENDFUNC
FUNC TAXI_FAIL_STRING GET_TAXI_EMERGENCY_FAIL_STRING(TaxiStruct &myTaxiData)
    RETURN myTaxiData.tfsEmergencyFailString
ENDFUNC
/// PURPOSE: Do all your taxi specific requests and loading here. Also a good place to ninja in any additional inits
///    Loads our model, string table, and anims and whatever we need for taxi oddjobs
///    
PROC TAXI_INIT_SHARED_STREAMS(BOOL bIsMalePassenger = TRUE)
    REQUEST_MODEL(TAXI_GET_TAXI_MODEL_NAME())
    IF bIsMalePassenger
	    REQUEST_ANIM_DICT("gestures@m@standing@casual") 
    ENDIF
	
	REQUEST_ANIM_DICT("oddjobs@taxi@")
	REQUEST_ANIM_DICT("oddjobs@towingcome_here")
	REQUEST_ANIM_DICT("misscommon@response")
	
	
    //Load text and UI
    REQUEST_ADDITIONAL_TEXT("TAXI",ODDJOB_TEXT_SLOT)
    
    IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_MISSION_RUNNING)
        SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_MISSION_RUNNING)
        CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_INIT_SHARED_STREAMS: Mission IS running")
    ENDIF
    
    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_INIT_SHARED_STREAMS - Anims & Text Requested ----SUCCESS")
ENDPROC
/// PURPOSE: Used to make sure all of our data is loaded and ready to be used
///    
/// RETURNS: TRUE once everything we requested has loaded in successfully
///    
FUNC BOOL TAXI_SHARED_ASSETS_STREAMED(INT &iThrottle, BOOL bIsMalePassenger = TRUE)

    IF NOT HAS_MODEL_LOADED(TAXI_GET_TAXI_MODEL_NAME())
        CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_SHARED_ASSETS_STREAMED - TAXI MODEL Loading...",iThrottle)
        RETURN FALSE
    ENDIF
    IF bIsMalePassenger
		IF NOT HAS_ANIM_DICT_LOADED("gestures@m@standing@casual")
	        CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_SHARED_ASSETS_STREAMED - Anim Dicts gestures@m@standing@casual Loading...",iThrottle)
	        RETURN FALSE
	    ENDIF
    ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("oddjobs@taxi@")
        CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_SHARED_ASSETS_STREAMED - Anim Dicts oddjobs@taxi@ Loading...",iThrottle)
        RETURN FALSE
    ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("oddjobs@towingcome_here")
        CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_SHARED_ASSETS_STREAMED - Anim Dicts oddjobs@towingcome_here Loading...",iThrottle)
        RETURN FALSE
    ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("misscommon@response")
        CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_SHARED_ASSETS_STREAMED - Anim Dicts misscommon@ Loading...",iThrottle)
        RETURN FALSE
    ENDIF
	
	IF NOT HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
        CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_SHARED_ASSETS_STREAMED - Mission Text Loading...",iThrottle)
        RETURN FALSE
    ENDIF
	
    RETURN TRUE
ENDFUNC

PROC MONITOR_TAXI_PASSENGER_OK(TaxiStruct &myTaxi, PED_INDEX taxiPassenger)
    IF DOES_ENTITY_EXIST(taxiPassenger)
        //Check if he's been injured than fail
        IF NOT IS_PED_INJURED(taxiPassenger)    
            
            IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
                IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(taxiPassenger, WEAPONTYPE_STUNGUN,GENERALWEAPON_TYPE_INVALID)
                OR HAS_PED_BEEN_DAMAGED_BY_WEAPON(taxiPassenger, WEAPONTYPE_INVALID,GENERALWEAPON_TYPE_ANYWEAPON)
                OR HAS_PED_BEEN_DAMAGED_BY_WEAPON(taxiPassenger, WEAPONTYPE_INVALID,GENERALWEAPON_TYPE_ANYMELEE)
                    TAXI_SET_FAIL(myTaxi, "Passenger injured by player with weapon.", TFS_PASSENGER_SHOT)
                ENDIF
                CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
            ENDIF
        ENDIF
        
        
    ENDIF
    //---------------------------------------------------------------------------------
ENDPROC

//Handle the global gates for the taxi mission here
PROC TAXI_OJ_MAINTAIN_GLOBAL_GATES(Taxistruct &myTaxiData)

    //Gate 1 - TAXI_GATE_CLEAR_DROPOFF - Clear all vehicles around the dropoff point once player is close enough
    IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_Bits_Gates,TAXI_GATE_CLEAR_DROPOFF)
        IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
            IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff) < TAXI_OJ_DIST_CLEAR_VEHICLE_DROPOFF
                CLEAR_AREA_OF_VEHICLES(myTaxiData.vTaxiOJDropoff,TAXI_OJ_RADIUS_CLEAR_VEHICLE)
                SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_Bits_Gates,TAXI_GATE_CLEAR_DROPOFF)
                CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_GATE_CLEAR_DROPOFF - Dropoff pt cleared of vehicles")
            ENDIF
        ENDIF
    ENDIF
    //-----------------------------------
    
ENDPROC
/// PURPOSE: Mutator proc for setting the TAXI_FOLLOW_POSITION
///    
/// PARAMS:
///    myTaxiData - our global taxi data
///    tFollowPos - TAXI_FOLLOW_POSITION 
///    bDebugOn - flag to show debug spew or not

PROC TAXI_SET_CHASE_POSITION(TaxiStruct &myTaxiData, TAXI_FOLLOW_POSITION tFollowPos, BOOL bDebugOn = TRUE)
    
    myTaxiData.tTaxiOJ_FollowPosition = tFollowPos
    
    IF bDebugOn 
        CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Chase Position Set to : ", "", ENUM_TO_INT(myTaxiData.tTaxiOJ_FollowPosition))
    ENDIF
ENDPROC
PROC TAXI_OJ_REMOVE_PASSENGER_BLIP(TaxiStruct &myTaxiData)
    IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
        SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger, FALSE)
        REMOVE_BLIP(myTaxiData.blipTaxiPassenger)
        CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SET_DROPOFF_BLIP_ON - Passenger blips removed")
    ENDIF
ENDPROC
/// PURPOSE: Disable the passenger blip, enable the dropoff blip and set gps
///         Used primarily in the TAXI_HANDLE_STANDARD_PICKUP function below
///    
/// PARAMS:
///    myTaxiData - Our taxi data
///    bShouldCreateBlip - whether or not to create a dropoff blip, usually true 
PROC TAXI_SET_DROPOFF_BLIP_ON(TaxiStruct &myTaxiData, BOOL bShouldCreateBlip)
    TAXI_OJ_REMOVE_PASSENGER_BLIP(myTaxiData)
    
    IF bShouldCreateBlip
        IF NOT DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
            myTaxiData.blipTaxiDropOff = CREATE_BLIP_FOR_COORD(myTaxiData.vTaxiOJDropoff,TRUE)
            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SET_DROPOFF_BLIP_ON - Destination Blip set")
            
        ELIF GET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff) = 0
            SET_GPS_FLAGS(GPS_FLAG_IGNORE_ONE_WAY)
			SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,255)
            SET_BLIP_COORDS(myTaxiData.blipTaxiDropOff,myTaxiData.vTaxiOJDropoff)
			SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, TRUE)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SET_DROPOFF_BLIP_ON - Destination blip is now visible")
        ENDIF
    ENDIF

ENDPROC
/// PURPOSE:
///    Handles all the blips in the oddjob if the player abandons the taxi during an active mission.
/// PARAMS:
///    myTaxiData - global taxi data
///    bOn - TRUE if he's left the taxi FALSE if he's returning
PROC SET_TAXI_ABANDON_BLIP_ON_OFF(TaxiStruct &myTaxiData, BOOL bOn = TRUE)
    
    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
        IF bON
            //Picking Up
            IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
                SET_BLIP_ALPHA(myTaxiData.blipTaxiPassenger,0)
                SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger, FALSE)
                
                //No passenger, just give god text
                //This should give the player his last objective.- GATED
                SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_RETURN_TO_TAXI,TAXI_OBJ_RETURN)
            
            //Dropping Off
            ELIF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
                SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,0)
                SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, FALSE)
                
				CLEAR_PRINTS()
				
                //Passenger yells at you
                //This should give the player his last objective.- GATED
                IF IS_PASSENGER_IN_TAXI(myTaxiData)
                    SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_LEFT_CAR,TRUE)
                ELSE
                    SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_RETURN_TO_TAXI,TAXI_OBJ_RETURN)
                ENDIF
                //SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_RETURN_TO_TAXI,TAXI_OBJ_LEFT_CAR)
            ENDIF
            
            //Blip Taxi--------
            TAXI_OJ_UPDATE_BLIP_TAXI(myTaxiData)
        ELSE
            //*Sigh* I have to turn off all the mission blips whatever they may be....
            //Leave this for a later day.
            IF DOES_BLIP_EXIST(myTaxiData.blipTaxiVehicle)
                REMOVE_BLIP(myTaxiData.blipTaxiVehicle)

                //Picking Up
                IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
                    SET_BLIP_ALPHA(myTaxiData.blipTaxiPassenger,255)
                    SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger, TRUE)
                    
                    //Give Obj to Pickup Passenger
                //  SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_REGIVE_OBJ,TAXI_OBJ_PICKUP)                   
                    
                //Dropping Off
                ELIF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
                    
                    //HACK Special case for CutYouIN*****************************************
                    IF myTaxiData.tTaxiOJ_MissionType = TXM_07_CUTYOUIN
                    AND myTaxiData.tTaxiOJ_RideState = TRS_WAIT_1ST_STOP
                        SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,0)
                        SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, FALSE)
                        
                        
                    ELSE
                        //Don't want the blip to draw if you're in TTB
                        IF myTaxiData.tTaxiOJ_MissionType <> TXM_05_TAKETOBEST
                            SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,255)
                            SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, TRUE)
                        ENDIF
                    ENDIF
                    //This should give the player his last objective. - GATED
                    //SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_REGIVE_OBJ,myTaxiData.tTaxiOJ_ObjectiveCurrent)                   
                ENDIF
				
				CLEAR_PRINTS()
				
            ENDIF
        ENDIF
    ENDIF
ENDPROC

/// PURPOSE: Reminds the player return to the taxi. If more than TAXI_TWEAK_TIME_FAIL_ABANDONED_CAR seconds has passed, FAIL the mission. 
///    
/// PARAMS:
///    myTaxiData - Our Taxi data
PROC TAXI_YELL_PLAYER_TO_RETURN(TaxiStruct &myTaxiData,BOOL bFailForLeavingOn = TRUE)

    //Check for the player doing anything to the passenger when he's out of the vehicle
    MONITOR_TAXI_PASSENGER_OK(myTaxiData,myTaxiData.piTaxiPassenger)
    
    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
        
        IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
            
            IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
                IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				ENDIF
                TAXI_SET_CHASE_POSITION(myTaxiData,TAXI_LEFT_CAR)
                SET_TAXI_ABANDON_BLIP_ON_OFF(myTaxiData)
                TAXI_RESET_TIMERS(myTaxiData, TT_NOTINCAB,0,TRUE)
				CDEBUG1LN(DEBUG_OJ_TAXI," %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		[DRIVE STATUS] - Player stepped out of TAXI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
				
				//IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
				IF myTaxiData.tTaxiOJ_RideState < TRS_WAIT_PARK
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_YELL_PLAYER_TO_RETURN, clearing ped tasks")
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_YELL_PLAYER_TO_RETURN, no longer clearing ped tasks because the passenger was getting out of the car")
					//CLEAR_PED_TASKS_IMMEDIATELY(myTaxiData.piTaxiPassenger)
				ENDIF
                
            ELSE
				IF bFailForLeavingOn
                    //Get yelled at before fail text if you're close enough to the taxi, otherwise just get god text
					IF NOT IS_PASSENGER_IN_TAXI(myTaxiData)
						IF (GET_GAME_TIMER() % 1000) < 50
							CDEBUG1LN(DEBUG_OJ_TAXI, "GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_NOTINCAB) = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_NOTINCAB))
						ENDIF
						IF IS_PED_IN_ANY_VEHICLE(myTaxiData.piTaxiPlayer)
							IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIFFERENTCAR) > 5
								CDEBUG1LN(DEBUG_OJ_TAXI,"Player got in a different vehicle, setting TT_DIFFERENTCAR timer")
								TAXI_RESET_TIMERS(myTaxiData, TT_DIFFERENTCAR,0,TRUE)
							ENDIF
						ENDIF
						
						IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_NOTINCAB) > TAXI_TWEAK_TIME_FAIL_ABANDONED_CAR_NO_PASSENGER
						
							CDEBUG1LN(DEBUG_OJ_TAXI,"Setting Player not in taxi for over 20 seconds")
							TAXI_SET_FAIL(myTaxiData, "Player not in taxi. - YELL RETURN", TFS_ABANDONED_PASSENGER)
						ENDIF
                    ELSE
						IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_NOTINCAB) > TAXI_TWEAK_TIME_FAIL_ABANDONED_CAR
	                        IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.viTaxi) > TAXI_DIALOGUE_MAX_DISTANCE
	                            CDEBUG1LN(DEBUG_OJ_TAXI,"Setting Player not in taxi with passenger fail > max distance")
								TAXI_SET_FAIL(myTaxiData, "Player not in taxi.", TFS_DEBUG_FAIL) 
	                        ELSE
	                            CDEBUG1LN(DEBUG_OJ_TAXI,"Setting Player not in taxi with passenger < max distance")
								TAXI_SET_FAIL(myTaxiData, "Player not in taxi. - YELL RETURN 2", TFS_ABANDONED_PASSENGER) 
	                        ENDIF
	                    ENDIF
					ENDIF
                ENDIF
            ENDIF           
        ENDIF
    
    ENDIF
    
ENDPROC

FUNC BOOL IS_PLAYER_DRIVING_TAXI_OJ(TaxiStruct & myTaxiData)
    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
        IF GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi,VS_DRIVER) = PLAYER_PED_ID()
            RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC BOOL PROCESS_TAXI_DEBUG_SKIP(TaxiStruct &myTaxiData,TXM_DEBUG_SKIP_STATES &tDebugState)
	
	IF IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
	
		SWITCH tDebugState
			CASE TXM_DSS_CHECK_FOR_BUTTON_PRESS
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					//Make sure we're in a skippable state
					IF myTaxiData.tTaxiOJ_RideState = TRS_MANAGE_PICKUP
					OR myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
					OR (GET_TAXI_MISSION_TYPE(myTaxiData) = TXM_08_GOTYOUNOW AND myTaxiData.tTaxiOJ_RideState = TRS_DROPPING_OFF)
					OR (GET_TAXI_MISSION_TYPE(myTaxiData) = TXM_07_CUTYOUIN AND myTaxiData.tTaxiOJ_RideState = TRS_DROPPING_OFF)
					OR (GET_TAXI_MISSION_TYPE(myTaxiData) = TXM_10_FOLLOWTHATCAR AND (myTaxiData.tTaxiOJ_RideState = TRS_WAIT_1ST_STOP OR myTaxiData.tTaxiOJ_RideState = TRS_CHASE_DRIVER))
						
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						
						ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_TAXI_DBG_SKIP = Key pressed.")
						tDebugState = TXM_FIND_WHAT_POINT_TO_WARP_TO
					ENDIF
				ENDIF
			BREAK
			
			
			
			CASE TXM_FIND_WHAT_POINT_TO_WARP_TO
				
				IF myTaxiData.tTaxiOJ_RideState = TRS_MANAGE_PICKUP
				
					myTaxiData.vTaxiOJ_WarpPt = myTaxiData.vTaxiOJ_WarpPtPickup
					myTaxiData.fTaxiOJ_WarpPtHeading = myTaxiData.fTaxiOJ_WarpPtHeadingPickup
				
				ELIF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
				 	
					myTaxiData.vTaxiOJ_WarpPt = myTaxiData.vTaxiOJ_WarpPtDropoff
					myTaxiData.fTaxiOJ_WarpPtHeading = myTaxiData.fTaxiOJ_WarpPtHeadingDropoff
					
					// edge case hack for needs excitement
					IF GET_TAXI_MISSION_TYPE(myTaxiData) = TXM_01_NEEDEXCITEMENT
						myTaxiData.iTaxiOJ_CashTip = 7
						myTaxiData.iTaxiOJ_CashFare = 50
						CDEBUG1LN(DEBUG_OJ_TAXI,"Tip set to cut off average for needs excitement")
					ENDIF
				//All Edge cases
				ELSE
					//Got You Now
					IF GET_TAXI_MISSION_TYPE(myTaxiData) = TXM_08_GOTYOUNOW AND myTaxiData.tTaxiOJ_RideState = TRS_DROPPING_OFF
						myTaxiData.vTaxiOJ_WarpPt = << -627.9326, -790.7941, 24.3267 >>
						myTaxiData.fTaxiOJ_WarpPtHeading = 11
					
					//Cut You In
					ELIF GET_TAXI_MISSION_TYPE(myTaxiData) = TXM_07_CUTYOUIN AND myTaxiData.tTaxiOJ_RideState = TRS_DROPPING_OFF
						myTaxiData.vTaxiOJ_WarpPt = myTaxiData.vTaxiOJ_WarpPtDropoff
						myTaxiData.fTaxiOJ_WarpPtHeading = myTaxiData.fTaxiOJ_WarpPtHeadingDropoff
					
					
					//Follow Car
					ELIF GET_TAXI_MISSION_TYPE(myTaxiData) = TXM_10_FOLLOWTHATCAR 
						IF myTaxiData.tTaxiOJ_RideState = TRS_WAIT_1ST_STOP
							myTaxiData.vTaxiOJ_WarpPt = << 505.2828, -1421.6077, 28.2163 >>
							myTaxiData.fTaxiOJ_WarpPtHeading = 85
							
						ELIF myTaxiData.tTaxiOJ_RideState = TRS_CHASE_DRIVER
							myTaxiData.vTaxiOJ_WarpPt = <<-662.2928, -1036.4415, 16.5224>> 
							myTaxiData.fTaxiOJ_WarpPtHeading = 167.6095 
						ENDIF
					
					ELSE
						SCRIPT_ASSERT("Taxi Debug Skip has run into an unhandled case. Please bug John Diaz.")
						RETURN FALSE
					ENDIF
				ENDIF
			 	
				tDebugState = TXM_DSS_CHECK_DISTANCE
			 BREAK
			 
			
			CASE TXM_DSS_CHECK_DISTANCE
			
				
        		IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJ_WarpPt) > CONST_TAXI_OJ_DISTANCE_AWAY_TO_WARP
	             	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
          				NEW_LOAD_SCENE_START(myTaxiData.vTaxiOJ_WarpPt, GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()), 30.0)
					ENDIF
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_TAXI_DBG_SKIP = Taxi is being warped.")
					myTaxiData.bIsTaxiDebugSkipping = TRUE
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE)
					tDebugState = TXM_DSS_FADE_DOWN
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_TAXI_DBG_SKIP = Taxi not far enough away from point.")
					tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS
        		ENDIF
			
			
			BREAK
			
			CASE TXM_DSS_FADE_DOWN
				IF NOT IS_SCREEN_FADED_OUT() 
					IF NOT IS_SCREEN_FADING_OUT()
						FADE_DOWN()
						tDebugState = TXM_DSS_FADING
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_TAXI_DBG_SKIP = Screen is fading down.")
				ENDIF
				
			BREAK
			
			
			CASE TXM_DSS_FADING
				IF IS_SCREEN_FADED_OUT()
					tDebugState = TXM_DSS_TELEPORT
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_TAXI_DBG_SKIP = Screen is fading down.")
				ENDIF
			BREAK
		
			CASE TXM_DSS_TELEPORT
				
				TAXI_RESET_TIMERS(myTaxiData, TT_GENERIC)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING() 
	            SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
                SET_ENTITY_COORDS(myTaxiData.viTaxi,myTaxiData.vTaxiOJ_WarpPt)
                SET_ENTITY_HEADING(myTaxiData.viTaxi,myTaxiData.fTaxiOJ_WarpPtHeading)
				
				tDebugState = TXM_DSS_WAIT_FOR_DATA
				CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_TAXI_DBG_SKIP = Player teleported.")
			BREAK
					
			CASE TXM_DSS_WAIT_FOR_DATA
				IF IS_NEW_LOAD_SCENE_LOADED()
				OR GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > TAXI_CONST_DEBUG_FADE_FAILSAFE
					NEW_LOAD_SCENE_STOP()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					tDebugState = TXM_DSS_FADE_UP
				ELSE
					CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("PROCESS_TAXI_DBG_SKIP = Scene is loading.",myTaxiData.iTaxiOJ_DebugThrottle)
				ENDIF
			
			BREAK
			CASE TXM_DSS_FADE_UP
				IF IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
					
					myTaxiData.bIsTaxiDebugSkipping = FALSE
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					IF IS_PASSENGER_IN_TAXI(myTaxiData)
						TAXI_RESET_TIMERS(myTaxiData, TT_STOPPED)
					ENDIF
					tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					
					RETURN TRUE
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_TAXI_DBG_SKIP = Scene is fading in.")
				ENDIF
				
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF


PROC TAXI_ODDJOB_HANDLE_TAXI_BLIPPING(TaxiStruct &myTaxiData)
    //If player is not in TAXI remind to get back in or suffer consequences
    IF NOT IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)    
        IF (GET_GAME_TIMER() % 1000) < 50
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_YELL_PLAYER_TO_RETURN called from TAXI_ODDJOB_HANDLE_TAXI_BLIPPING")
		ENDIF
		TAXI_YELL_PLAYER_TO_RETURN(myTaxiData)
    
    //Else player is in taxi
    ELSE
        //Make sure to cancel the "I'm outside the cab" timer as above check proves it wrong
        IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
            CDEBUG1LN(DEBUG_OJ_TAXI,"SET_PLAYER_REENTERED_TAXI_OJ called in 1749")
			SET_PLAYER_REENTERED_TAXI_OJ(myTaxiData)
            SET_TAXI_ABANDON_BLIP_ON_OFF(myTaxiData,FALSE)
        ENDIF
    ENDIF
ENDPROC

/// PURPOSE: Felt like I needed to consolidate checking for the player in taxi seat with the standard IS_VEHICLE_DRIVEABLE check
///    
/// RETURNS: TRUE if Taxi is being driven by player, FALSE otherwise
///    
FUNC BOOL IS_TAXI_DRIVEN_BY_PLAYER(TaxiStruct &myTaxiData)
    
    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
        IF GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi) = myTaxiData.piTaxiPlayer
            
            IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
                CDEBUG1LN(DEBUG_OJ_TAXI,"SET_PLAYER_REENTERED_TAXI_OJ called in 1789")
				SET_PLAYER_REENTERED_TAXI_OJ(myTaxiData)
            ENDIF
            
            SET_TAXI_ABANDON_BLIP_ON_OFF(myTaxiData,FALSE)

            RETURN TRUE
    
    //DEBUG IN CASE SOMETHING WENT WRONG/////////   
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC
FUNC BOOL IS_PED_AND_VEHICLE_VALID(PED_INDEX piPassenger,VEHICLE_INDEX viRide)
    IF IS_VEHICLE_DRIVEABLE(viRide)
        IF NOT IS_PED_INJURED(piPassenger)
            RETURN TRUE
        ENDIF
    ENDIF
    
    CDEBUG1LN(DEBUG_OJ_TAXI,"IS_PED_AND_VEHICLE_VALID - Return FALSE")
    RETURN FALSE
ENDFUNC
FUNC BOOL IS_PED_IN_VEHICLE_SAFE(PED_INDEX piPassenger,VEHICLE_INDEX viRide)
    
    IF IS_PED_AND_VEHICLE_VALID(piPassenger, viRide)
        IF IS_PED_IN_VEHICLE(piPassenger,viRide)
            RETURN TRUE
        ENDIF   
    ENDIF
    
    CDEBUG1LN(DEBUG_OJ_TAXI,"IS_PED_IN_VEHICLE_SAFE - Return FALSE")
    RETURN FALSE
ENDFUNC
/// PURPOSE: This is an ALL - IN - One Check for a driveable taxi that has the player and the passenger
///         Also handles reminding the player to get back in the taxi if he's left.
/// PARAMS:
///    myTaxiData - Our global Taxi data
/// RETURNS:
///    
FUNC BOOL IS_TAXI_RIDE_ALL_READY(TaxiStruct &myTaxiData)
    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)      
        IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
            
            IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPassenger, myTaxiData.viTaxi, TRUE)
            AND NOT IS_PED_GETTING_INTO_A_VEHICLE(myTaxiData.piTaxiPassenger)
                TAXI_SET_FAIL(myTaxiData, "Passenger left car.")
            ELSE
                IF IS_TAXI_DRIVEN_BY_PLAYER(myTaxiData)
                    IF IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_PICKUP")
						CLEAR_THIS_PRINT("TAXI_OBJ_PICKUP")
					ENDIF
					
					IF IS_SCREEN_FADED_IN()
	                    RETURN TRUE
					ENDIF
                ELSE
                    IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_YELL_PLAYER_TO_RETURN called from IS_TAXI_RIDE_ALL_READY")
					ENDIF
					TAXI_YELL_PLAYER_TO_RETURN(myTaxiData)
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

/// PURPOSE: Checks that everything is ready to go with the taxi and the player, otherwise fails and cancels the taxi mission
///    I generally use this at the start of the taxi mission scripts
FUNC BOOL TAXI_INIT_READY(TaxiStruct &myTaxiData)
    //goes w/o saying that we need to check to see if the player is in a taxi
    //I guess this was setup in an off chance that IS_PED_IN_ANY_TAXI doesn't do it's job correctly
    IF IS_PED_IN_ANY_VEHICLE(myTaxiData.piTaxiPlayer)
        
        //Store off the taxi info, this is very handy info, and than you can use it to see it's ENTITY_MODEL
        myTaxiData.viTaxi = GET_VEHICLE_PED_IS_IN(myTaxiData.piTaxiPlayer)
        
        //99.98% success rate checking against the ENTITY_MODEL OR IS_IN_TAXI
        IF IS_PED_IN_ANY_TAXI(myTaxiData.piTaxiPlayer)
        OR GET_ENTITY_MODEL(myTaxiData.viTaxi) = TAXI_GET_TAXI_MODEL_NAME()
            
            //This is LAW. ALWAYS CHECK FOR IS_DRIVEABLE before doing anything else on the vehicle
            IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
            
                //This seems redundant cause we're already checking this same object, is this in case he gets pulled out???
                IF GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi) = myTaxiData.piTaxiPlayer
                    
                    ADD_VEHICLE_UPSIDEDOWN_CHECK(myTaxiData.viTaxi)
					
					ASSIGN_TAXI_OJ_LICENSE_PLATE(myTaxiData)
					
					TAXI_STATS_UPDATE(TAXI_STAT_FARES_ACCEPTED)
					
					bNoEnterCutscene = TRUE
					
					SET_RICH_PRESENCE_FOR_SP_MINIGAME(MINIGAME_TAXI)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"starting total timer now")
					START_TIMER_NOW(timerTotal)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_INIT_READY - SUCCESS ")
                    RETURN TRUE
        ////////FAIL TAXI MISSION IF ANY OF THESE DON'T HOLD TRUE /////////////////////////
                ELSE
                    TAXI_SET_FAIL(myTaxiData, "No Taxi", TFS_DEBUG_FAIL)
                    PRINT_HELP("TAXI_DBG_NTAXI")
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_INIT_READY - FAILED - GET_PED_VEHICLE_SEAT")
                ENDIF
            ELSE
                TAXI_SET_FAIL(myTaxiData, "Taxi is not drivable", TFS_TAXI_DISABLED)
                PRINT_HELP("TAXI_DBG_NTAXI")
                CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_INIT_READY - FAILED - IS_VEHICLE_DRIVEABLE")
            ENDIF
        ELSE
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FAILDELAY) > 2.0
	            TAXI_SET_FAIL(myTaxiData, "No Taxi", TFS_DEBUG_FAIL)
	            PRINT_HELP("TAXI_DBG_NTAXI")
	            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_INIT_READY - FAILED - IS_PED_IN_ANY_TAXI")
			ENDIF
        ENDIF
    ENDIF
    /////////////////////////////////////////////////////////////////////////////END FAILS
    RETURN FALSE
ENDFUNC

PROC SETUP_TAXI_PASSENGER_WAIT_FOR_PICKUP_TIME(TaxiStruct &myTaxiData)

	//DEBUG TIME OF DAY DEADLINES
	IF NOT Is_TIMEOFDAY_Valid(myTaxiData.eTODToPickUpPassenger)
		myTaxiData.eTODToPickUpPassenger = GET_CURRENT_TIMEOFDAY()
		ADD_TIME_TO_TIMEOFDAY(myTaxiData.eTODToPickUpPassenger , 0, 0,GET_RANDOM_INT_IN_RANGE(CONST_iTaxiPassengerWaitTimeMin, CONST_iTaxiPassengerWaitTimeMax) )
		CDEBUG1LN(DEBUG_OJ_TAXI,"[TOD] Passenger Wait Time Set up")

	
	ELSE
		//Debug Print the TOD that the call will end if the player takes too long
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sDebugTOD
					
				sDebugTOD = TIMEOFDAY_TO_TEXT_LABEL(myTaxiData.eTODToPickUpPassenger)
				sDebugTOD += "Passenger expires:"
				DRAW_DEBUG_TEXT_2D( sDebugTOD , <<0.1, 0.12, 0.0>>)
				
		#ENDIF
		
		IF IS_NOW_AFTER_TIMEOFDAY(myTaxiData.eTODToPickUpPassenger)			
			TAXI_SET_FAIL(myTaxiData, "Player took too long to make pickup", TFS_TIME_EXPIRED)
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_IN_ANY_VIP_PROPERTY_VEHICLE(TaxiStruct &myTaxiData , INT iMinPassengerSeats = 1)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				MODEL_NAMES vehModel = GET_ENTITY_MODEL( tempVeh )
				
				// Make sure the car has at least 2/4 tires, and some doors.
				BOOL bDMGGood = IS_TAXI_VEHICLE_DAMAGE_OK(tempVeh)
				
				// Run the model check.
				BOOL bVehicleModelGood = TRUE
				IF (vehModel = RHINO) OR (vehModel = RATLOADER) OR (vehModel = SURFER) OR (vehModel = SURFER2) 
						OR (vehModel = ARMYTANKER) OR (vehModel = BARRACKS) OR (vehModel = BARRACKS2) OR (vehModel = CRUSADER)
						OR (vehModel = UTILLITRUCK) OR (vehModel = UTILLITRUCK2) OR (vehModel = UTILLITRUCK3) OR (vehModel = AIRTUG)
						OR (vehModel = CADDY)  OR (vehModel = CADDY2)  OR (vehModel = DLOADER) OR (vehModel = DOCKTUG)
						OR (vehModel = FLATBED) OR (vehModel = RIPLEY) OR (vehModel = ROMERO)
						OR (vehModel = TOWTRUCK) OR (vehModel = TOWTRUCK2) OR (vehModel = AIRBUS) OR (vehModel = BUS) OR (vehModel = COACH)
						OR (vehModel = RENTALBUS) OR (vehModel = TOURBUS) OR (vehModel = RIOT) OR (vehModel = TRASH) OR (vehModel = BENSON)
						OR (vehModel = BIFF) OR (vehModel = HAULER) OR (vehModel = PACKER) OR (vehModel = PHANTOM) OR (vehModel = POUNDER)
						OR (vehModel = BULLDOZER) OR (vehModel = HANDLER) OR (vehModel = TIPTRUCK2) OR (vehModel = CUTTER)
						OR (vehModel = DUMP) OR (vehModel = MIXER) OR (vehModel = MIXER2) OR (vehModel = RUBBLE) OR (vehModel = SCRAP)
						OR (vehModel = TIPTRUCK) OR (vehModel = CAMPER) OR (vehModel = TACO) OR (vehModel = BOXVILLE) OR (vehModel = BOXVILLE2)
						OR (vehModel = BOXVILLE3) OR (vehModel = BURRITO) OR (vehModel = BURRITO2) OR (vehModel = BURRITO3) OR (vehModel = BURRITO4)
						OR (vehModel = GBURRITO) OR (vehModel = JOURNEY) OR (vehModel = MULE) OR (vehModel = MULE2) OR (vehModel = PONY)
						OR (vehModel = RUMPO) OR (vehModel = RUMPO2) OR (vehModel = SPEEDO) OR (vehModel = SPEEDO2) OR (vehModel = YOUGA)
						OR (vehModel = MOWER) OR (vehModel = TRACTOR) OR (vehModel = TRACTOR2)
						OR (vehModel = FBI) OR (vehModel = FBI2) OR (vehModel = PRANGER)
						OR (vehModel = AMBULANCE) OR (vehModel = DILETTANTE2) OR (vehModel = FIRETRUK)
						OR (vehModel = LGUARD) OR (vehModel = DUNE)
						OR (vehModel = PBUS) OR (vehModel = POLICE) OR (vehModel = POLICE2) OR (vehModel = POLICE3) OR (vehModel = POLICE4)
						OR (vehModel = POLICEB) OR (vehModel = POLICET) OR (vehModel = SHERIFF) OR (vehModel = SHERIFF2) OR (vehModel = STOCKADE)
					bVehicleModelGood = FALSE
				ENDIF
				
				// just an extra check for clown car - the bodhi is the only 2 seater vehicle that allows 4 passengers, blocking this
				IF myTaxiData.tTaxiOJ_MissionType = TXM_09_CLOWNCAR
					IF vehModel = BODHI2
						bVehicleModelGood = FALSE
					ENDIF
				ENDIF
				
				// Check vehicle capability
				BOOL bVehCapGood = FALSE
				IF IS_THIS_MODEL_A_CAR(vehModel)
					IF (GET_PED_IN_VEHICLE_SEAT(tempVeh) = PLAYER_PED_ID())
						bVehCapGood = TRUE
					ENDIF
				ENDIF
				
				BOOL bVehSizeGood = TRUE
				IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempVeh) < iMinPassengerSeats
					bVehSizeGood = FALSE
				ENDIF
				
				
				// OKAY! Print what's wrong.
				IF NOT bDMGGood
					IF NOT IS_BITMASK_AS_ENUM_SET( myTaxiData.iTaxiJobBits, TAXI_OJ_JB_CAR_DMGED )
						IF HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
							PRINT_HELP("TX_VIP_DMGD")
							
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TX_VIP_DMGD")
								SET_BITMASK_AS_ENUM( myTaxiData.iTaxiJobBits, TAXI_OJ_JB_CAR_DMGED  )
							ENDIF
							CDEBUG1LN(DEBUG_OJ_TAXI,"Player in vehicle that is too dmged.")
						ENDIF
					ENDIF
					RETURN FALSE
				
				ELIF NOT bVehCapGood OR NOT bVehicleModelGood
					IF NOT IS_BITMASK_AS_ENUM_SET( myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GET_CAR_HELP )
						IF HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
							PRINT_HELP("TX_VIP_CAR") // "Get a car for the customer."
							
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TX_VIP_CAR")
								SET_BITMASK_AS_ENUM( myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GET_CAR_HELP  )
							ENDIF
							CDEBUG1LN(DEBUG_OJ_TAXI,"Player in vehicle that isn't a car")
						ENDIF
					ENDIF
					RETURN FALSE
				
				ELIF NOT bVehSizeGood
					IF NOT IS_BITMASK_AS_ENUM_SET( myTaxiData.iTaxiJobBits, TAXI_OJ_JB_SMALL_CAR_HELP )
						IF HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
							PRINT_HELP("TX_VIP_SMALL")
							
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TX_VIP_SMALL")
								SET_BITMASK_AS_ENUM( myTaxiData.iTaxiJobBits, TAXI_OJ_JB_SMALL_CAR_HELP  )
							ENDIF
							CDEBUG1LN(DEBUG_OJ_TAXI,"Player in vehicle that doesn't meet VIP Vehicle needs")
						ENDIF
					ENDIF
					RETURN FALSE
				ENDIF
				
				
				// All good!
				RETURN TRUE
			ENDIF
		ELSE
			
			CLEAR_BITMASK_AS_ENUM( myTaxiData.iTaxiJobBits, TAXI_OJ_JB_SMALL_CAR_HELP  )
			CLEAR_BITMASK_AS_ENUM( myTaxiData.iTaxiJobBits, TAXI_OJ_JB_CAR_DMGED  )
			CLEAR_BITMASK_AS_ENUM( myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GET_CAR_HELP  )
		
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Checks that everything is ready to go with the vehicle for the property mission and the player, otherwise fails and cancels the taxi mission
///    I generally use this at the start of the taxi mission scripts
FUNC BOOL PROPERTY_VIP_INIT_READY(TaxiStruct &myTaxiData, INT nMinNumSeats = 1)
	 
	
	IF NOT HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
        CDEBUG1LN(DEBUG_OJ_TAXI,"Loading Taxi Text ")
        RETURN FALSE
    ENDIF
	
	SET_RICH_PRESENCE_FOR_SP_MINIGAME(MINIGAME_TAXI)
	
	IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GET_VIP)
		IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_CAR_JACKED)
			IF IS_PED_JACKING(PLAYER_PED_ID())
			//AND GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) < CONST_iDistanceToBlipPassenger
			AND NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_CAR_JACKING)
				CDEBUG1LN(DEBUG_OJ_TAXI,"passenger saw player jacking, will comment on it momentarily")
				SET_BITMASK_AS_ENUM(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_CAR_JACKING)
			ENDIF
			IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_CAR_JACKING)
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_JACKED_CAR, TRUE, FALSE, TRUE)
				SET_BITMASK_AS_ENUM(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_CAR_JACKED)
			ENDIF
		ENDIF
	ENDIF
	
	//Tell player to come with a proper vehicle
	IF NOT IS_PLAYER_IN_ANY_VIP_PROPERTY_VEHICLE(myTaxiData, nMinNumSeats)
		//PRINT_NOW_ONCE("TX_VIP",DEFAULT_GOD_TEXT_TIME,0,myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GET_VIP)
		IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) < CONST_iDistanceToBlipPassenger
			IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GET_VIP)
				TASK_TURN_PED_TO_FACE_ENTITY(myTaxiData.piTaxiPassenger, PLAYER_PED_ID())
				SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_GET_CAR, TRUE, FALSE, TRUE)
				SET_BITMASK_AS_ENUM(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GET_VIP)
			ENDIF
		ENDIF
		
		IF IS_PED_INJURED(myTaxiData.piTaxiPassenger)
		OR IS_PED_FLEEING(myTaxiData.piTaxiPassenger)
		OR GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) > 400.0
			TAXI_SET_FAIL(myTaxiData, "ped is fleeing or injured", TFS_SPOOKED)
		ENDIF
	ELSE
		
		myTaxiData.viTaxi = GET_VEHICLE_PED_IS_IN(myTaxiData.piTaxiPlayer)
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"taxi set")
		CDEBUG1LN(DEBUG_OJ_TAXI,"taxi model is ", GET_ENTITY_MODEL(myTaxiData.viTaxi))
		
		SET_VEHICLE_HAS_STRONG_AXLES(myTaxiData.viTaxi, TRUE)
		
        //This seems redundant cause we're already checking this same object, is this in case he gets pulled out???
        IF GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi) = myTaxiData.piTaxiPlayer
            
            ADD_VEHICLE_UPSIDEDOWN_CHECK(myTaxiData.viTaxi)
			
			ASSIGN_TAXI_OJ_LICENSE_PLATE(myTaxiData)
			
			TAXI_STATS_UPDATE(TAXI_STAT_FARES_ACCEPTED)
			
			bNoEnterCutscene = TRUE
			
			CDEBUG1LN(DEBUG_OJ_TAXI,"starting total timer now")
			START_TIMER_NOW(timerTotal)
			
			CDEBUG1LN(DEBUG_OJ_TAXI,"PROPERTY_VIP_INIT_READY - SUCCESS ")
            RETURN TRUE
        ////////FAIL TAXI MISSION IF ANY OF THESE DON'T HOLD TRUE /////////////////////////
        ELSE
            TAXI_SET_FAIL(myTaxiData, "No Taxi", TFS_DEBUG_FAIL)
           	PRINT_HELP("TAXI_DBG_NTAXI")
	        CDEBUG1LN(DEBUG_OJ_TAXI,"PROPERTY_VIP_INIT_READY - FAILED - IS_PED_IN_ANY_TAXI")          
        ENDIF
    ENDIF
    /////////////////////////////////////////////////////////////////////////////END FAILS
    RETURN FALSE
ENDFUNC


/// PURPOSE: Run this when relaunching a taxi mission to reinitialize the data correctly.
///    
/// PARAMS:
///    myTaxiData - our taxi info
PROC RESET_TAXI_STRUCT_TO_DEFAULTS(TaxiStruct &myTaxiData)
    myTaxiData.piTaxiPlayer = PLAYER_PED_ID()
    myTaxiData.piTaxiPassenger = NULL
    myTaxiData.viTaxi = NULL
    myTaxiData.vTaxiOJPickup = NULL_VECTOR()
    myTaxiData.vTaxiOJDropoff = NULL_VECTOR()
    myTaxiData.fTaxiOJ_TimeDeadline = 0.0
    myTaxiData.iTaxiOJ_PassengerExcitement = 0
    myTaxiData.iTaxiOJ_StatesGenIndex = 0 
    myTaxiData.bTaxiOJ_Failed = FALSE
    //myTaxiData.bTaxiOJ_IsPickupValid = FALSE
    //myTaxiData.bTaxiOJ_IsDropoffValid = FALSE
    myTaxiData.fTaxiOJ_BanterDelay = 0//GET_RANDOM_FLOAT_IN_RANGE(TAXI_BANTER_MIN,TAXI_BANTER_MAX)
    myTaxiData.tTaxiOJ_RideState = TRS_FINDING_LOCATION
    myTaxiData.sTaxiOJ_RideState = "TRS_FINDING_LOCATION"
	
	TAXI_CANCEL_TIMERS(myTaxiData)
ENDPROC
/// PURPOSE: Set all our String IDS
///    
/// PARAMS:
///    myTaxiData - 
///    sIDPrefix - The unique mission id 
///    DialogueMissionID - Used for Dialogue Star
///    DialogueSubtitleGroupID - Used for Dialogue Star
PROC TAXI_OJ_INIT_IDS(TaxiStruct & myTaxiData, String sIDPrefix, String DialogueMissionID, String DialogueSubtitleGroupID)
    
    myTaxiData.sTaxiOJ_IDPrefix = sIDPrefix
    myTaxiData.sTaxiOJ_DXMissionID = DialogueMissionID
    myTaxiData.sTaxiOJ_DXSubtitleGroupID = DialogueSubtitleGroupID

    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_INIT_IDS - All IDs init for : ","",myTaxiData.sTaxiOJ_IDPrefix )
ENDPROC

/// PURPOSE: This is where I init all specific info, cause when you have 10+ missions, going in individually into each file is a huge time sink.
///    This way I just add it to here.
///    
/// PARAMS:
///    myTaxiData - 
PROC SET_TAXI_ODDJOB_SPECIFICS(TaxiStruct & myTaxiData)
    
    SWITCH GET_TAXI_MISSION_TYPE(myTaxiData)
        
		CASE TXM_01_NEEDEXCITEMENT
            TAXI_OJ_INIT_IDS(myTaxiData, "EXC","Txm2","Txm2aud")
            myTaxiData.iTaxiOJ_NumDXVariations = 2
        BREAK
        
        CASE TXM_02_TAKEITEASY
            TAXI_OJ_INIT_IDS(myTaxiData, "TIE","Txm1","txm1aud")
            myTaxiData.iTaxiOJ_NumDXVariations = 1
        BREAK
        
        CASE TXM_03_DEADLINE
            TAXI_OJ_INIT_IDS(myTaxiData, "DED","Txm3","Txm3aud")
            myTaxiData.iTaxiOJ_NumDXVariations = 1
        BREAK
        
		CASE TXM_04_GOTYOURBACK
            TAXI_OJ_INIT_IDS(myTaxiData, "GYB","Txm12","Txm12au")
            myTaxiData.iTaxiOJ_NumDXVariations = 2
        BREAK
		
        CASE TXM_05_TAKETOBEST
            TAXI_OJ_INIT_IDS(myTaxiData, "TTB","Txm6","Txm6aud")
            myTaxiData.iTaxiOJ_NumDXVariations = 2
        BREAK               
        
        CASE TXM_07_CUTYOUIN
            TAXI_OJ_INIT_IDS(myTaxiData, "CUI","Txm8","Txm8aud")
            myTaxiData.iTaxiOJ_NumDXVariations = 1
        BREAK       
        
        CASE TXM_08_GOTYOUNOW
            TAXI_OJ_INIT_IDS(myTaxiData, "GYN","Txm9","Txm9aud")
            myTaxiData.iTaxiOJ_NumDXVariations = 1
        BREAK
		
        CASE TXM_09_CLOWNCAR
            TAXI_OJ_INIT_IDS(myTaxiData, "TCC","Txm10","Txm10au")
            myTaxiData.iTaxiOJ_NumDXVariations = 2
        BREAK       
        
        CASE TXM_10_FOLLOWTHATCAR
            TAXI_OJ_INIT_IDS(myTaxiData, "TFC","Txm4","Txm4aud")
            myTaxiData.iTaxiOJ_NumDXVariations = 1
        BREAK
        
		CASE TXM_PROCEDURAL
			TAXI_OJ_INIT_IDS(myTaxiData, "PRO","txmP","TxmPaud")
            myTaxiData.iTaxiOJ_NumDXVariations = 1
		BREAK
		
    ENDSWITCH
    
ENDPROC

/// PURPOSE: One place to init all taxi data instead of doing it in every single file.
///    
/// PARAMS:
///    myTaxiData - our global taxi data
///    thisMissionType - TAXI_MISSION_TYPES - Specific mission type 
PROC TAXI_ODDJOB_GLOBAL_SETUP(TaxiStruct &myTaxiData, TAXI_MISSION_TYPES thisMissionType)

	
    //Set up the player ped for dialogue if the controller isn't running to avoid asserts
    //Despite the fact that you will never be able to launch a taxi oddjob w/o the controller running in the final build
//  IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_CONTROLLER_RUNNING)
    DISABLE_TAXI_HAILING()
	
	TOGGLE_TAXI_OJ_RADIO_SOUNDS_ON()
	
    //***1/12/12 - Updated this to remove the is controller running check so that this always reset the player when the mission is started. 
    TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo)
//  ENDIF
    RESET_TAXI_STRUCT_TO_DEFAULTS(myTaxiData)
    myTaxiData.tTaxiOJ_MissionType = thisMissionType
   	
    IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
        SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
//        SET_VEHICLE_MODEL_IS_SUPPRESSED(TAXI_GET_TAXI_MODEL_NAME(), TRUE)
    ENDIF
    
    //Init Radio Prefs
    INIT_TAXI_OJ_RADIO_STATION_PREFERENCES_PER_MISSION(myTaxiData)
    
    //TAXI_CONTROLLER_UpdateNumRuns(thisMissionType)
    
    SET_TAXI_ODDJOB_SPECIFICS(myTaxiData)
    
    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_WillFlyThroughWindscreen,FALSE)
    myTaxiData.iTaxiOJ_GetRunModCount = TAXI_CONTROLLER_GetNumRuns(myTaxiData.tTaxiOJ_MissionType) % myTaxiData.iTaxiOJ_NumDXVariations
    myTaxiData.iTaxiOJ_BanterCount = 0
	
	myTaxiData.sDrivingStat = GET_TAXI_DRIVING_STAT_FOR_PLAYER()
	
	REQUEST_ADDITIONAL_TEXT("TAXI",ODDJOB_TEXT_SLOT)
	
    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_ODDJOB_GLOBAL_SETUP - SUCCESS")
ENDPROC


FUNC BOOL EVALUATE_PROGRESS(TaxiStruct &myTaxiData, TAXI_DIALOGUE_INDEX tdPositiveLine, TAXI_DIALOGUE_INDEX tdNegativeLine, FLOAt fTriggerDist, FLOAT fGoodTime)
    
    IF GET_PLAYER_DISTANCE_FROM_LOCATION(myTaxiData.vTaxiOJDropoff, FALSE) < fTriggerDist
        IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_RIDETODEST)
            IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST) <= fGoodTime
                SET_NEXT_TAXI_SPEECH(myTaxiData,tdPositiveLine,TRUE)
                RETURN TRUE
            ELSE
                SET_NEXT_TAXI_SPEECH(myTaxiData,tdNegativeLine,TRUE)
                RETURN TRUE
            ENDIF
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

FUNC BOOL EVALUATE_OVERALL_TIME(TaxiStruct &myTaxiData, TAXI_DIALOGUE_INDEX tdNegativeLine, FLOAT fMaxTime)

	IF myTaxiData.tTaxiOJ_RideState < TRS_SCORECARD_GRADE	//Don't evaluate time if you're getting your scorecard
	    IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_RIDETODEST)
	        TAXI_START_TIMER(myTaxiData, TT_TIP_CHECK)
	        
	        IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST) > fMaxTime AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_TIP_CHECK) > CONST_TAXI_TIME_UPDATE
	            TAXI_RESET_TIMERS(myTaxiData, TT_TIP_CHECK)
	            SET_NEXT_TAXI_SPEECH(myTaxiData,tdNegativeLine,TRUE)
	            RETURN TRUE
	        ENDIF
	    ENDIF
	ENDIF
    RETURN FALSE
ENDFUNC

// DELETE?
/// PURPOSE: Sets up a cam shot, if you pass it an interp time it will interp back to the game cam. Otherwise it will just cut.
///    
/// PARAMS:
///    taxiData - 
///    vOffset - 
///    iInterpTime - 
PROC SETUP_TAXI_QUICK_CUT(TaxiStruct& taxiData, VECTOR vOffset, INT iInterpTime = 0)
    DESTROY_CAM(taxiData.camTaxi)
    RENDER_SCRIPT_CAMS(FALSE, FALSE)
    
    IF NOT DOES_CAM_EXIST(taxiData.camTaxi)
        taxiData.camTaxi = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), 50, FALSE) // TODO : Change 1st param to as of CAMTYPE_SCRIPTED after Thomas French's email
    ENDIF
    //cargoArgs.iCutGameTimeStart = GET_GAME_TIMER()
    
    //SET_CAM_ROT(taxiData.camTaxi, <<-90,0,0>>)
    IF IS_VEHICLE_DRIVEABLE(taxiData.viTaxi)
    AND NOT IS_ENTITY_DEAD(taxiData.piTaxiPassenger)
        
        ATTACH_CAM_TO_ENTITY(taxiData.camTaxi, taxiData.viTaxi, vOffset)
        POINT_CAM_AT_ENTITY(taxiData.camTaxi, taxiData.piTaxiPassenger, <<0,0,0>>)
    ENDIF
    
    SET_CAM_ACTIVE(taxiData.camTaxi, TRUE)
    IF iInterpTime > 0
        RENDER_SCRIPT_CAMS(TRUE,TRUE,iInterpTime)
    ELSE
        RENDER_SCRIPT_CAMS(TRUE,FALSE)
    ENDIF
ENDPROC

PROC CLEANUP_TAXI_QUICK_CAM(TaxiStruct& taxiData, INT iInterpTime = 0)
    IF DOES_CAM_EXIST(taxiData.camTaxi)
        DESTROY_CAM(taxiData.camTaxi)
    ENDIF
    IF iInterpTime > 0
        RENDER_SCRIPT_CAMS(FALSE,TRUE,iInterpTime)
    ELSE
        RENDER_SCRIPT_CAMS(FALSE,FALSE)
    ENDIF
ENDPROC

PROC CLEANUP_TAXI_PICKUP_STOP(TaxiStruct& taxiData)
    //This should resolve problems with the HIT_PED dialogue firing off cause the passenger ran into your car
    CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
    
    //CleanUp Pickup Lock & POI
    SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
    CLEANUP_TAXI_QUICK_CAM(taxiData, TAXI_CAM_PICKUP_INTERP_OUT)
	
ENDPROC

PROC BLIP_TAXI_OJ_PASSENGER(TaxiStruct &myTaxiData)
	IF NOT myTaxiData.bBlipPassenger
		IF myTaxiData.bPassengerObjPrinted
			//CLEAR_PRINTS()
			
			//Remove Yellow Pickup Blip
			IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
				REMOVE_BLIP(myTaxiData.blipTaxiPassenger)
			ENDIF
			
			//Create Blue Passenger Blip
			myTaxiData.blipTaxiPassenger = CREATE_BLIP_FOR_ENTITY(myTaxiData.piTaxiPassenger)
		    
		   	SET_BLIP_NAME_FROM_TEXT_FILE(myTaxiData.blipTaxiPassenger,"TAXI_BLIP_PASS")
		   	SET_GPS_FLAGS(GPS_FLAG_IGNORE_ONE_WAY)
			SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger,TRUE)
			
			myTaxiData.bBlipPassenger = TRUE
		ENDIF
	ENDIF
ENDPROC
FUNC STRING GET_TAXI_OJ_BLIP_NAME(TAXI_MISSION_TYPES tMissionType)
	
	STRING sBlipName = NULL_STRING()
	
	SWITCH tMissionType
		CASE TXM_01_NEEDEXCITEMENT		sBlipName = "TX_BLIP_NEX_PU"	BREAK
		CASE TXM_02_TAKEITEASY			sBlipName = "TX_BLIP_TIE_PU"	BREAK
		CASE TXM_03_DEADLINE			sBlipName = "TX_BLIP_DL_PU"		BREAK
		CASE TXM_04_GOTYOURBACK			sBlipName = "TX_BLIP_GYB_PU"	BREAK
		CASE TXM_05_TAKETOBEST			sBlipName = "TX_BLIP_TTB_PU"	BREAK
		CASE TXM_07_CUTYOUIN			sBlipName = "TX_BLIP_CYI_PU"	BREAK
		CASE TXM_08_GOTYOUNOW			sBlipName = "TX_BLIP_GYN_PU"	BREAK
		CASE TXM_09_CLOWNCAR			sBlipName = "TX_BLIP_CC_PU"		BREAK
		CASE TXM_10_FOLLOWTHATCAR		sBlipName = "TX_BLIP_FC_PU"		BREAK
		
		DEFAULT
			sBlipName = "TAXI_BLIP_PASS"
		BREAK
		
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi_JD Blip Named----: ", "", sBlipName)
	RETURN sBlipName
ENDFUNC

/// PURPOSE: Creates a passenger for the taxi to pickup
///    
/// PARAMS:
///    myTaxiData - our taxi info
///    locatesData - 
/// RETURNS:
///    
FUNC BOOL TAXI_SPAWN_PASSENGER(TaxiStruct &myTaxiData, VECTOR passengerSpawnPt, VECTOR passengerPickupPt, STRING sDialogueVoiceID, MODEL_NAMES mName = DUMMY_MODEL_FOR_SCRIPT, FLOAT fHeading = 0.0,FLOAT fRadiusRoadNodeDisable = TAXI_AREA_ROAD_DISABLE_RADIUS)
    
    IF NOT DOES_ENTITY_EXIST(myTaxiData.piTaxiPassenger)
        
        ADD_TAXI_OJ_SPEED_REDUCTION_VOLUMES(myTaxiData,myTaxiData.vTaxiOJPickup)
        
        //Initialize spawn and pickup locations
        myTaxiData.vTaxiOJSpawn = passengerSpawnPt
        myTaxiData.vTaxiOJPickup = passengerPickupPt
        
        //Stops cars from spawning in the area around the passenger
        TAXI_PREP_TURN_OFF_VEHICLE_GENS(myTaxiData.vTaxiOJPickup)
        
        //Clears the current vehicles in the area
        //CLEAR_AREA_OF_VEHICLES(myTaxiData.vTaxiOJPickup, TAXI_OJ_RADIUS_CLEAR_VEHICLE)
        
		CLEAR_AREA_OF_PEDS(myTaxiData.vTaxiOJPickup,TAXI_OJ_RADIUS_CLEAR_PED)
        
        TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(myTaxiData.vTaxiOJPickup, FALSE,  fRadiusRoadNodeDisable)

        IF mName = DUMMY_MODEL_FOR_SCRIPT
            myTaxiData.piTaxiPassenger = CREATE_RANDOM_PED(myTaxiData.vTaxiOJSpawn)
        ELSE
            IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1])
				myTaxiData.piTaxiPassenger = g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1]
				SET_ENTITY_AS_MISSION_ENTITY(myTaxiData.piTaxiPassenger, TRUE, TRUE)
			ELSE
				myTaxiData.piTaxiPassenger = CREATE_PED(PEDTYPE_MISSION, mName, myTaxiData.vTaxiOJSpawn,fHeading)
			ENDIF
        ENDIF
        
        //Dialogue Setup-----------------
        ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,3,myTaxiData.piTaxiPassenger,sDialogueVoiceID)
        SET_AMBIENT_VOICE_NAME(myTaxiData.piTaxiPassenger, sDialogueVoiceID)
		
        CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SPAWN_PASSENGER - Passenger spawned at these coords", "", myTaxiData.vTaxiOJSpawn)
		
		SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_DisableSeeThroughChecksWhenTargeting, TRUE)
		
        IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
            
			SET_ENTITY_LOD_DIST(myTaxiData.piTaxiPassenger, 250)
			
            //SET_PED_CAN_BE_TARGETTED(myTaxiData.piTaxiPassenger,FALSE)
			SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger,PCF_WillFlyThroughWindscreen,FALSE)
			SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger,PCF_OpenDoorArmIK,TRUE)
			SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger,PCF_PedsJackingMeDontGetIn,TRUE)
    		SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger,PCF_GetOutBurningVehicle,FALSE)
			
//			myTaxiData.blipTaxiPassenger = CREATE_BLIP_FOR_ENTITY(myTaxiData.piTaxiPassenger)
//			SET_GPS_FLAGS(GPS_FLAG_IGNORE_ONE_WAY)
//			SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger,TRUE)
			
			//myTaxiData.blipTaxiPassenger = CREATE_BLIP_FOR_COORD(myTaxiData.vTaxiOJSpawn,TRUE)
			//SET_BLIP_NAME_FROM_TEXT_FILE(myTaxiData.blipTaxiPassenger,GET_TAXI_OJ_BLIP_NAME(GET_TAXI_MISSION_TYPE(myTaxiData)))
            
            ADD_RELATIONSHIP_GROUP("TAXI_Passenger", myTaxiData.relPassenger)
            SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, myTaxiData.relPassenger, RELGROUPHASH_PLAYER)
            SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, myTaxiData.relPassenger, RELGROUPHASH_COP)
    
            SET_PED_RELATIONSHIP_GROUP_HASH(myTaxiData.piTaxiPassenger,myTaxiData.relPassenger)
//    		TASK_LOOK_AT_ENTITY(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,-1,SLF_DEFAULT)
            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SPAWN_PASSENGER - SUCCESS")
            RETURN TRUE
        ENDIF
    ENDIF
    
    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SPAWN_PASSENGER - FAILURE- Usual culprit is commandline -nopeds ")
    RETURN FALSE
ENDFUNC

FUNC BOOL TAXI_INIT_PASSENGER_BLIP(TaxiStruct &myTaxiData)
	IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
		myTaxiData.blipTaxiPassenger = CREATE_BLIP_FOR_ENTITY(myTaxiData.piTaxiPassenger)
		SET_GPS_FLAGS(GPS_FLAG_IGNORE_ONE_WAY)
		SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger,TRUE)
		SET_BLIP_NAME_FROM_TEXT_FILE(myTaxiData.blipTaxiPassenger,"TAXI_BLIP_PASS")
		TASK_LOOK_AT_ENTITY(myTaxiData.piTaxiPassenger, PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
			
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC VEHICLE_SEAT TAXI_GET_PROPER_SEAT(VEHICLE_INDEX viTaxi,PED_INDEX piPassenger)
    VECTOR tempVector
    VEHICLE_SEAT vsReturn
	INT iNumSeats
    
	iNumSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(viTaxi)
	tempVector = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(viTaxi, GET_ENTITY_COORDS(piPassenger))
    
	IF GET_ENTITY_MODEL(viTaxi) != VACCA
		CDEBUG1LN(DEBUG_OJ_TAXI,"vehicle is not a vacca")
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"vehicle is indeed a vacca")
	ENDIF
	
	IF GET_ENTITY_MODEL(viTaxi) = SENTINEL2
		CDEBUG1LN(DEBUG_OJ_TAXI,"vehicle is sentinel2")
		iNumSeats = 1
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"vehicle is not a sentinel2")
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"number of seats in the car is ", iNumSeats)
	
	IF iNumSeats = 1
		vsReturn = VS_FRONT_RIGHT
		CDEBUG1LN(DEBUG_OJ_TAXI,"passenger going to the front right door")
	ELSE
	    IF tempVector.x > 0 //Passenger coming in from RIGHT
	        IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(piPassenger, viTaxi, VS_BACK_RIGHT)
	            vsReturn = VS_BACK_RIGHT
				CDEBUG1LN(DEBUG_OJ_TAXI,"passenger going to the back right door")
	        ELIF IS_ENTRY_POINT_FOR_SEAT_CLEAR(piPassenger, viTaxi, VS_BACK_LEFT)
	            vsReturn = VS_BACK_LEFT
				CDEBUG1LN(DEBUG_OJ_TAXI,"passenger going to the back left door")
	        ELSE
	            CDEBUG1LN(DEBUG_OJ_TAXI,"no doors!  damn")
				//SCRIPT_ASSERT("No unblocked door")
	            vsReturn = VS_ANY_PASSENGER
	        ENDIF
	    ELSE //Passenger coming in from the left
	        IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(piPassenger, viTaxi, VS_BACK_LEFT)
	            vsReturn = VS_BACK_LEFT
				CDEBUG1LN(DEBUG_OJ_TAXI,"passenger going to the back left door")
	        ELIF IS_ENTRY_POINT_FOR_SEAT_CLEAR(piPassenger, viTaxi, VS_BACK_RIGHT) //If left door is blocked go the right door
	            vsReturn = VS_BACK_RIGHT
				CDEBUG1LN(DEBUG_OJ_TAXI,"passenger going to the back right door")
	        ELSE
	            CDEBUG1LN(DEBUG_OJ_TAXI,"no doors!  damn")
				//SCRIPT_ASSERT("No unblocked door")
	            vsReturn = VS_ANY_PASSENGER
	        ENDIF
	    ENDIF
	ENDIF
    
    RETURN vsReturn
ENDFUNC


PROC CHECK_TAXI_SWAP_CAR(TaxiStruct& myTaxiData)
    
    IF myTaxiData.tTaxiOJ_RideState < TRS_WAIT_FOR_TIME AND NOT myTaxiData.bPedEntering
        IF (myTaxiData.tTaxiOJ_MissionType = TXM_PROCEDURAL AND IS_PLAYER_IN_ANY_TAXI() AND NOT IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData))
        OR (myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL AND IS_PLAYER_IN_ANY_VIP_PROPERTY_VEHICLE(myTaxiData) AND NOT IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData))
			ENTITY_INDEX tempTaxiEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(myTaxiData.viTaxi)
            
            //Release the old taxi
            SET_ENTITY_AS_NO_LONGER_NEEDED(tempTaxiEntity)
            
            //Assign the new one
            myTaxiData.viTaxi = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
            SET_ENTITY_AS_MISSION_ENTITY(myTaxiData.viTaxi)
            
			CDEBUG1LN(DEBUG_OJ_TAXI,"SET_PLAYER_REENTERED_TAXI_OJ called in 2416")
			SET_PLAYER_REENTERED_TAXI_OJ(myTaxiData)
			SET_TAXI_ABANDON_BLIP_ON_OFF(myTaxiData,FALSE)
			
		ENDIF
    ENDIF

ENDPROC

FUNC BOOL SHOULD_TAXI_FAIL_FOR_AGGRO(TaxiStruct& myTaxiData,BOOL bForceFail = FALSE, EAggro eAggroType = EAggro_Aiming)
    
    CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ Aggro Count is now ", "", myTaxiData.iTaxiOJ_AggroCount, "", " Fail Limit is = ","",TAXI_OJ_AGGRO_TIMES_BEFORE_FAIL)
    
    IF (myTaxiData.iTaxiOJ_AggroCount >= TAXI_OJ_AGGRO_TIMES_BEFORE_FAIL
    AND myTaxiData.tTaxiOJ_RideState < TRS_SCORECARD_GRADE)
	
	OR (myTaxiData.tTaxiOJ_RideState <= TRS_MANAGE_PICKUP)
	
    OR (bForceFail AND NOT myTaxiData.bIsTaxiOJInCombat)
        RETURN TRUE
    
	ELSE
		IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
			IF NOT IS_ENTITY_OCCLUDED(myTaxiData.piTaxiPassenger)
				
				IF eAggroType = EAggro_ShotNear
	        		SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_SHOOTING,TRUE,FALSE,TRUE)
				ELSE
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_AGGRO,TRUE,FALSE,TRUE)
				ENDIF
				
			ENDIF
		ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

PROC TAXI_OJ_HANDLE_AGGRO(TaxiStruct& myTaxiData,EAggro &eAggroType, AGGRO_ARGS &aggroArgs)
    
    SWITCH eAggroType
        
        CASE EAggro_Aiming//----------------------------------------------
            IF SHOULD_TAXI_FAIL_FOR_AGGRO(myTaxiData)
                TAXI_SET_FAIL(myTaxiData,"Aggro Aiming", TFS_SPOOKED)
            ENDIF
        BREAK
        
        CASE EAggro_ShotNear//--------------------------------------------
            IF SHOULD_TAXI_FAIL_FOR_AGGRO(myTaxiData, FALSE, EAggro_ShotNear)
                TAXI_SET_FAIL(myTaxiData,"Aggro Shot Near", TFS_ABANDONED_PASSENGER)
            ENDIF
        BREAK
        
        CASE EAggro_HeardShot//--------------------------------------------
            IF SHOULD_TAXI_FAIL_FOR_AGGRO(myTaxiData, FALSE, EAggro_HeardShot)
			
                TAXI_SET_FAIL(myTaxiData,"Aggro Heard Shot", TFS_ABANDONED_PASSENGER)
            ENDIF
        BREAK
        
        //The one case where they should fail on the first time
        CASE EAggro_Attacked//--------------------------------------------
            IF SHOULD_TAXI_FAIL_FOR_AGGRO(myTaxiData,TRUE)
                TAXI_SET_FAIL(myTaxiData,"Aggro Attacked", TFS_PASSENGER_SHOT)
            ENDIF
        BREAK
        
        CASE EAggro_MinorAttacked//--------------------------------------------
            IF SHOULD_TAXI_FAIL_FOR_AGGRO(myTaxiData)
                TAXI_SET_FAIL(myTaxiData,"Aggro Minor Attacked", TFS_ABANDONED_PASSENGER)
            ENDIF
        BREAK
		
		CASE EAggro_Wanted//----------------------------------------------
			IF NOT IS_BITMASK_AS_ENUM_SET (aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
			AND IS_PASSENGER_IN_TAXI(myTaxiData)
 	           TAXI_SET_FAIL(myTaxiData,"Aggro Went Wanted", TFS_SPOOKED)
			ENDIF
        BREAK
		
    ENDSWITCH
ENDPROC

FUNC BOOL RUN_GLOBAL_TAXI_UPDATES(TaxiStruct& myTaxiData,AGGRO_ARGS &aggroArgs)
    IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_AGGRO)
		TAXI_START_TIMER(myTaxiData, TT_AGGRO)
		aggroArgs.fAimTime = CONST_TaxiVIP_Aggro_Aim_Delay
		aggroArgs.fAimRange = CONST_TaxiVIP_Aggro_Aim_Range
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI AGGRO Timer Started")
	ENDIF
	
	IF NOT myTaxiData.bTaxiOJ_Failed
	AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_AGGRO) > 5
        EAggro aggroReason
        
        IF DO_AGGRO_CHECK(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,aggroArgs, aggroReason)  
            TAXI_RESET_TIMERS(myTaxiData, TT_AGGRO)
			TAXI_RESET_TIMERS(myTaxiData, TT_BORING)  
			TAXI_OJ_HANDLE_AGGRO(myTaxiData,aggroReason,aggroArgs)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI AGGRO Timer Reset, Aggro triggered")
            //TAXI_SET_FAIL(myTaxiData,"Player aggro'd the taxi.",TFS_ABANDONED_PASSENGER)
        ENDIF
		
        CHECK_TAXI_SWAP_CAR(myTaxiData)
    ENDIF
    
	//Time Of Day Fail
	IF myTaxiData.tTaxiOJ_RideState >= TRS_SPAWNING
	AND myTaxiData.tTaxiOJ_RideState <= TRS_MANAGE_PICKUP
		SETUP_TAXI_PASSENGER_WAIT_FOR_PICKUP_TIME(myTaxiData)
	ENDIF
	
	// player control failsafe
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
	AND (DOES_CAM_EXIST(myTaxiData.camTaxi) AND NOT IS_CAM_ACTIVE(myTaxiData.camTaxi))
	AND (DOES_CAM_EXIST(myTaxiData.camInterp) AND NOT IS_CAM_ACTIVE(myTaxiData.camInterp))
	AND NOT IS_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	
		IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CONTROL_CHECK) > 10
			TAXI_CANCEL_TIMERS(myTaxiData, TT_CONTROL_CHECK)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TT_CONTROL_CHECK > 10, PLAYER CONTROL RESTORED!")
		ENDIF
	ELSE
		IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_CONTROL_CHECK)
			TAXI_CANCEL_TIMERS(myTaxiData, TT_CONTROL_CHECK)
		ENDIF
	ENDIF
	
    RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if Line of Sight is clear between two coords.  This is asynchronous, so it must be called
///    frame by frame, and the result may not be returned until a few frames later
/// PARAMS:
///    tShapeTest - 
///    vStart, vEnd - if you pass in an entity's coord's, it's best to use an offset coord from the entity
///    				otherwise the entity itself may say that LOS is blocked
/// RETURNS:
///    3 different INT values.  0 = clear, 1 = not clear, -1 = not ready.
FUNC INT IS_LOS_SHAPETEST_CLEAR_BETWEEN_COORDS(SHAPETEST_INDEX & tShapeTest, VECTOR vStart, VECTOR vEnd, BOOL bShowDebugInfo = FALSE)
	
	VECTOR vLOSProbePos, vLOSProbeNormal
	INT bLOSProbeHit
	ENTITY_INDEX hitEntity
	SHAPETEST_STATUS eLOSProbeStatus
	
	IF tShapeTest = NULL
		tShapeTest = START_SHAPE_TEST_LOS_PROBE(vStart, vEnd, SCRIPT_INCLUDE_ALL)
		
		IF bShowDebugInfo
			CDEBUG1LN(DEBUG_OJ_TAXI,"GRABBING NEW tShapeTest")
		ENDIF
	
	ELSE 
		CDEBUG1LN(DEBUG_OJ_TAXI,"NEW tShapeTest, WAITING FOR IT TO BE READY")
		eLOSProbeStatus = GET_SHAPE_TEST_RESULT(tShapeTest, bLOSProbeHit, vLOSProbePos, vLOSProbeNormal, hitEntity)
		
		#IF IS_DEBUG_BUILD
		IF bShowDebugInfo
			CDEBUG1LN(DEBUG_OJ_TAXI,"vStart = ", vStart)
			CDEBUG1LN(DEBUG_OJ_TAXI,"vEnd = ", vEnd)
			CDEBUG1LN(DEBUG_OJ_TAXI,"bLOSProbeHit = ", bLOSProbeHit)
			CDEBUG1LN(DEBUG_OJ_TAXI,"vLOSProbePos = ", vLOSProbePos)
			CDEBUG1LN(DEBUG_OJ_TAXI,"vLOSProbeNormal = ", vLOSProbeNormal)
		ENDIF
		#ENDIF
		
		IF eLOSProbeStatus = SHAPETEST_STATUS_RESULTS_READY
			IF bShowDebugInfo
				CDEBUG1LN(DEBUG_OJ_TAXI,"eLOSProbeStatus = SHAPETEST_STATUS_RESULTS_READY")
			ENDIF
			
			IF bLOSProbeHit = 0
				CDEBUG1LN(DEBUG_OJ_TAXI,"bLOSProbeHit = TRUE, LOS is clear")
				
				tShapeTest = NULL
				RETURN bLOSProbeHit
				
			ELSE
				IF bShowDebugInfo
					CDEBUG1LN(DEBUG_OJ_TAXI,"bLOSProbeHit = FALSE")
				ENDIF
				tShapeTest = NULL
				RETURN bLOSProbeHit
				
			ENDIF
			
		ELIF eLOSProbeStatus = SHAPETEST_STATUS_NONEXISTENT
			IF bShowDebugInfo
				CDEBUG1LN(DEBUG_OJ_TAXI,"eLOSProbeStatus = SHAPETEST_STATUS_NONEXISTENT")
			ENDIF
			tShapeTest = NULL
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL UPDATE_PED_DRIVE_UP(TaxiStruct &myTaxiData, FLOAT fPedLookDistance)
    
    VECTOR vPlayerTemp, vPassengerTemp, vPlayerOffset
	VECTOR vLOSProbePos, vLOSProbeNormal
	INT bLOSProbeHit
	ENTITY_INDEX hitEntity
	SHAPETEST_STATUS eLOSProbeStatus
	
//	SWITCH GET_TAXI_VEHICLE_TYPE(myTaxiData)
//    	CASE TAXITYPE_LOW 	vPlayerTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << 0, 1.5, 0.75>>) BREAK
//		CASE TAXITYPE_REG  	vPlayerTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << 0, 1.5, 0.75>>) BREAK
//		CASE TAXITYPE_HIGH 	vPlayerTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << 0, 3.0, 1.5>>) BREAK
//    ENDSWITCH
	
	INT iBoneWindscreen
	iBoneWindscreen = GET_ENTITY_BONE_INDEX_BY_NAME(myTaxiData.viTaxi, "windscreen")
	CDEBUG1LN(DEBUG_OJ_TAXI,"windscreen bone index = ", iBoneWindscreen)
	
	IF iBoneWindscreen = -1
		iBoneWindscreen = GET_ENTITY_BONE_INDEX_BY_NAME(myTaxiData.viTaxi, "windscreen_f")
		CDEBUG1LN(DEBUG_OJ_TAXI,"windscreen_f bone index = ", iBoneWindscreen)
	ENDIF
	
	IF iBoneWindscreen != -1
		CDEBUG1LN(DEBUG_OJ_TAXI,"windscreen bone is valid, using it!")
		vPlayerOffset = GET_WORLD_POSITION_OF_ENTITY_BONE(myTaxiData.viTaxi, iBoneWindscreen)
		vPlayerOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(myTaxiData.viTaxi, vPlayerOffset)
		vPlayerOffset.y += 1.0
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"windscreen bone is invalid, using standard offset")
		vPlayerOffset = << 0, 1.0, 1.0 >>
	ENDIF
	
	
	vPlayerTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, vPlayerOffset)
	vPassengerTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.piTaxiPassenger, << 0, 0.25, 0.9>>)
    
	DRAW_DEBUG_LINE(vPlayerTemp, vPassengerTemp)
	
    SWITCH taxiDriveUp
        CASE TAXI_PED_DRIVE_UP_INIT
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiData.piTaxiPassenger, TRUE)
			taxiDriveUp = TAXI_PED_TRIGGER_SHAPE_TEST
			#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_2D("taxiDriveUp = TAXI_PED_DRIVE_UP_INIT", << 0.7, 0.5, 0.0>>)
			#ENDIF
        BREAK
		CASE TAXI_PED_TRIGGER_SHAPE_TEST
			#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_2D("taxiDriveUp = TAXI_PED_TRIGGER_SHAPE_TEST", << 0.7, 0.5, 0.0>>)
			#ENDIF
			IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,myTaxiData.piTaxiPassenger,FALSE)  <=  fPedLookDistance
			AND NOT myTaxiData.bIsTaxiDebugSkipping
            AND ABSF(vPlayerTemp.z - vPassengerTemp.z) < 5.0
            	#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_2D("Within range, grabbing shape tests", << 0.7, 0.55, 0.0>>)
				#ENDIF
				
				IF myTaxiData.tShapeTest = NULL //NEW FUNCTION GOES HERE  HAS_ENTITY_CLEAR_LOS_TO_ENTITY(myTaxiData.piTaxiPassenger, myTaxiData.viTaxi)
					myTaxiData.tShapeTest = START_SHAPE_TEST_LOS_PROBE(vPlayerTemp, vPassengerTemp, SCRIPT_INCLUDE_ALL) //SCRIPT_INCLUDE_VEHICLE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"GRABBING NEW myTaxiData.tShapeTest")
				ELSE 
					CDEBUG1LN(DEBUG_OJ_TAXI,"NEW myTaxiData.tShapeTest, WAITING FOR IT TO BE READY")
					eLOSProbeStatus = GET_SHAPE_TEST_RESULT(myTaxiData.tShapeTest, bLOSProbeHit, vLOSProbePos, vLOSProbeNormal, hitEntity)
					
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_OJ_TAXI,"vPlayerTemp = ", vPlayerTemp)
					CDEBUG1LN(DEBUG_OJ_TAXI,"vPassengerTemp = ", vPassengerTemp)
					CDEBUG1LN(DEBUG_OJ_TAXI,"bLOSProbeHit = ", bLOSProbeHit)
					CDEBUG1LN(DEBUG_OJ_TAXI,"vLOSProbePos = ", vLOSProbePos)
					CDEBUG1LN(DEBUG_OJ_TAXI,"vLOSProbeNormal = ", vLOSProbeNormal)
					#ENDIF
					
					IF eLOSProbeStatus = SHAPETEST_STATUS_RESULTS_READY
						CDEBUG1LN(DEBUG_OJ_TAXI,"eLOSProbeStatus = SHAPETEST_STATUS_RESULTS_READY")
						
						IF bLOSProbeHit = 0
							CDEBUG1LN(DEBUG_OJ_TAXI,"bLOSProbeHit = TRUE, going to TAXI_PED_DRIVE_UP_DIALOUGE")
							taxiDriveUp = TAXI_PED_DRIVE_UP_DIALOUGE
						ELSE
							CDEBUG1LN(DEBUG_OJ_TAXI,"bLOSProbeHit = FALSE")
						ENDIF
						
						myTaxiData.tShapeTest = NULL
					
					ELIF eLOSProbeStatus = SHAPETEST_STATUS_NONEXISTENT
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"eLOSProbeStatus = SHAPETEST_STATUS_NONEXISTENT")
						
						myTaxiData.tShapeTest = NULL
					ENDIF
				ENDIF
			ELSE
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"passenger distance from taxi = ", GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,myTaxiData.piTaxiPassenger,FALSE))
				CDEBUG1LN(DEBUG_OJ_TAXI,"z offset = ", vPlayerTemp.z - vPassengerTemp.z)
				CDEBUG1LN(DEBUG_OJ_TAXI,"vPlayerTemp = ", vPlayerTemp)
				CDEBUG1LN(DEBUG_OJ_TAXI,"vPassengerTemp = ", vPassengerTemp)
				
				IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK 
				AND GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
					CDEBUG1LN(DEBUG_OJ_TAXI,"passenger tasked with anim in UPDATE_PED_DRIVE_UP->TAXI_PED_TRIGGER_SHAPE_TEST")
					CLEAR_SEQUENCE_TASK(myTaxiData.siSeqIndex)
            		OPEN_SEQUENCE_TASK(myTaxiData.siSeqIndex)
						TASK_PLAY_ANIM(NULL, "oddjobs@taxi@", "idle_a")//, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0.556)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
						SET_SEQUENCE_TO_REPEAT(myTaxiData.siSeqIndex, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(myTaxiData.siSeqIndex)
            		TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, myTaxiData.siSeqIndex)
				ENDIF
			ENDIF
		BREAK
        CASE TAXI_PED_DRIVE_UP_DIALOUGE
			
			CLEAR_PRINTS()
			
			IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
				IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_CAR_JACKED)
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_HAIL,TRUE,FALSE,TRUE)  
				ENDIF
            ELSE
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "TAXI_HAIL", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
			ENDIF
			
			CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
            
			TASK_LOOK_AT_ENTITY(myTaxiData.piTaxiPassenger, myTaxiData.viTaxi, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
			
			CLEAR_SEQUENCE_TASK(myTaxiData.siSeqIndex)
            OPEN_SEQUENCE_TASK(myTaxiData.siSeqIndex)
				IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
					TASK_PLAY_ANIM(NULL, "gestures@m@standing@casual", "gesture_nod_yes_hard",NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY) //| AF_LOOPING)
				ELSE
					TASK_PLAY_ANIM(NULL, "oddjobs@towingcome_here", "come_here_idle_c", NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY )//| AF_LOOPING)
				ENDIF
				TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
                //SET_SEQUENCE_TO_REPEAT(myTaxiData.siSeqIndex, REPEAT_FOREVER)
            CLOSE_SEQUENCE_TASK(myTaxiData.siSeqIndex)
            TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, myTaxiData.siSeqIndex)
            CLEAR_SEQUENCE_TASK(myTaxiData.siSeqIndex)
			
			CDEBUG1LN(DEBUG_OJ_TAXI,"passenger tasked with come here anim")
			
			taxiDriveUp = TAXI_PED_DRIVE_UP_END
        BREAK
        CASE TAXI_PED_DRIVE_UP_END
            taxiDriveUp = TAXI_PED_DRIVE_UP_INIT
			
			IF IS_ENTITY_PLAYING_ANIM(myTaxiData.piTaxiPassenger,"gestures@m@standing@casual", "gesture_you_hard")
				SET_ENTITY_ANIM_SPEED(myTaxiData.piTaxiPassenger,"gestures@m@standing@casual", "gesture_you_hard",0.8)
			ENDIF
				
			RETURN TRUE
        BREAK
    ENDSWITCH
    
    RETURN FALSE
    
ENDFUNC 
FUNC SC_DOOR_LIST TAXI_OJ_CONVERT_VEHICLE_SEAT_TO_DOOR_LIST(VEHICLE_SEAT vSeat)
	SWITCH vSeat
		CASE VS_BACK_LEFT	RETURN SC_DOOR_REAR_LEFT	
		CASE VS_BACK_RIGHT	RETURN SC_DOOR_REAR_RIGHT	
		CASE VS_FRONT_RIGHT	RETURN SC_DOOR_FRONT_RIGHT	
		CASE VS_DRIVER		RETURN SC_DOOR_FRONT_LEFT	
	ENDSWITCH
	
	RETURN SC_DOOR_REAR_LEFT
ENDFUNC

FUNC FLOAT GET_PASSENGER_HEADING_TO_ENTRY_POINT(TaxiStruct &taxiData)

	FLOAT fHeadingToDoorHandle = 0
	VECTOR vDoorHandlePt = NULL_VECTOR()
	
	IF taxiData.vsEnterSeat = VS_BACK_RIGHT
		vDoorHandlePt = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(taxiData.viTaxi,<<0.773, -0.646, -0.6422>>)			
	ELSE
	
		vDoorHandlePt = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(taxiData.viTaxi,<<-0.773, -0.646, -0.6422>>)
	ENDIF
	
	fHeadingToDoorHandle = GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(taxiData.piTaxiPassenger),vDoorHandlePt)	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Ped is facing: ",GET_ENTITY_HEADING(taxiData.piTaxiPassenger)," Heading to door is: ",fHeadingToDoorHandle)
	
	RETURN fHeadingToDoorHandle
	
ENDFUNC

FUNC BOOL IS_TAXI_CLEAR_OF_DEAD_BODIES(TaxiStruct &myTaxiData, BOOL bIsDebug = TRUE)

	IF IS_VEHICLE_DRIVEABLE(myTaxiData.vitaxi)
		PED_INDEX piDeadBody[3]
		INT iDeadBodyIter
		
		piDeadBody[0] = GET_PED_IN_VEHICLE_SEAT(myTaxiData.vitaxi,VS_BACK_LEFT)
		piDeadBody[1] = GET_PED_IN_VEHICLE_SEAT(myTaxiData.vitaxi,VS_BACK_RIGHT)
		piDeadBody[2] = GET_PED_IN_VEHICLE_SEAT(myTaxiData.vitaxi,VS_FRONT_RIGHT)
		
		REPEAT 	3 iDeadBodyIter
			IF DOES_ENTITY_EXIST(piDeadBody[iDeadBodyIter])
				IF IS_ENTITY_DEAD(piDeadBody[iDeadBodyIter])
					IF iDeadBodyIter = 0
						myTaxiData.vsEnterSeat = VS_BACK_LEFT
						IF bIsDebug
							CDEBUG1LN(DEBUG_OJ_TAXI,"Player seat to clean is VS_BACK_LEFT which = ", myTaxiData.vsEnterSeat )
						ENDIF				
					ELIF iDeadBodyIter = 1
						myTaxiData.vsEnterSeat = VS_BACK_RIGHT
						IF bIsDebug
							CDEBUG1LN(DEBUG_OJ_TAXI,"Player seat to clean is VS_BACK_RIGHT which =", myTaxiData.vsEnterSeat )
						ENDIF
					ELSE
						myTaxiData.vsEnterSeat = VS_FRONT_RIGHT
						IF bIsDebug
							CDEBUG1LN(DEBUG_OJ_TAXI,"Player seat to clean is VS_FRONT_RIGHT which =", myTaxiData.vsEnterSeat )
						ENDIF
					ENDIF
					
					IF bIsDebug
						CDEBUG1LN(DEBUG_OJ_TAXI,"Found a DEAD ped in passenger seat: ",iDeadBodyIter )
					ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF (GET_GAME_TIMER() % 1000) < 50
			CDEBUG1LN(DEBUG_OJ_TAXI,"No dead bodies in the taxi. Congratulations :) " )
		ENDIF
		
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC PED_INDEX GET_LEFT_OVER_PASSENGER_IN_TAXI(TaxiStruct &myTaxiData, VEHICLE_SEAT &vsCurrentSeat)
	PED_INDEX piLeftOverPassenger
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		piLeftOverPassenger = GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi,VS_BACK_LEFT)
		IF DOES_ENTITY_EXIST(piLeftOverPassenger)
			CDEBUG1LN(DEBUG_OJ_TAXI,"Found Left Over Passenger in the VS_BACK_LEFT")
			vsCurrentSeat = VS_BACK_LEFT
			RETURN piLeftOverPassenger
			
		//Check next seat
		ELSE
			piLeftOverPassenger = GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi,VS_BACK_RIGHT)
		
			IF DOES_ENTITY_EXIST(piLeftOverPassenger)
				CDEBUG1LN(DEBUG_OJ_TAXI,"Found Left Over Passenger in the VS_BACK_RIGHT")
				vsCurrentSeat = VS_BACK_RIGHT
				RETURN piLeftOverPassenger
			ELSE
				//Check next seat
				piLeftOverPassenger = GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi,VS_FRONT_RIGHT)
				
				IF DOES_ENTITY_EXIST(piLeftOverPassenger)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Found Left Over Passenger in the VS_FRONT_RIGHT")
					vsCurrentSeat = VS_FRONT_RIGHT
					RETURN piLeftOverPassenger
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"NO LEFT OVER PASSENGER FOUND. Well Done")
	RETURN NULL
ENDFUNC

PROC CREATE_TAXI_OJ_CAM(CAMERA_INDEX &camTaxi, VECTOR vPos , VECTOR vRot, FLOAT fov = 50.0)

	IF NOT DOES_CAM_EXIST(camTaxi)
		camTaxi = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vPos, vRot, fov, FALSE)
		CDEBUG1LN(DEBUG_OJ_TAXI,"~`~`~`~`~`~ [TAXICAMS] - New CamTaxi created at Pos: ", vPos, " Rot = ", vRot, " FOV = ", fov)	
	ENDIF
			
ENDPROC

FUNC BOOL UPDATE_PASSENGER_ENTER_CUTSCENE(TaxiStruct &myTaxiData)
    CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("UPDATE_PASSENGER_ENTER_CUTSCENE",myTaxiData.iTaxiOJ_DebugThrottle)
    VECTOR vOffset, vLookAt, vWarpPos
	PED_INDEX piLeftOverPassenger
	VEHICLE_SEAT vsLeftOverPassengersSeat
	ENTER_EXIT_VEHICLE_FLAGS ecf_exitFlag
	INT iLOSTest = -1
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL sLOSTest
	#ENDIF
	
    IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger) AND NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
        //OH NO!!
      	 STOP_PHONE_AND_APPS_FOR_TAXI_CUTSCENE()
    ENDIF
    
	IF bNoEnterCutscene
		taxiEnterCut = TAXI_PED_NO_CUTSCENE
	ENDIF
	
	//Handle SKIP--------------------
    IF taxiEnterCut < TAXI_PED_ENTER_CUT_SKIP 
	AND taxiEnterCut > TAXI_PED_ENTER_CUT_STOP
	AND NOT bNoEnterCutscene
        IF HANDLE_SKIP_CUTSCENE(iAllowSkipCutsceneTime) 
            taxiEnterCut =  TAXI_PED_ENTER_CUT_SKIP
        ENDIF
    ENDIF
    
    SWITCH taxiEnterCut
        CASE TAXI_PED_ENTER_CUT_STOP
            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_PED_ENTER_CUT_STOP")
			
			IF IS_TAXI_FULLY_STOPPED(myTaxiData,FALSE) AND CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)
				IF IS_TAXI_CLEAR_OF_DEAD_BODIES(myTaxiData)
	                iAllowSkipCutsceneTime = GET_GAME_TIMER()
	                SETTIMERA(0)
	                taxiEnterCut = TAXI_PED_ENTER_CUT_FIRST
				ELSE
					TAXI_SET_FAIL(myTaxiData,"You had a dead body in your back seat.",TFS_SPOOKED)
				ENDIF
            ENDIF
        BREAK
		
        CASE TAXI_PED_ENTER_CUT_FIRST
			//B*432820 - Make this instant
            IF TIMERA() > 500
	            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_PED_ENTER_CUT_FIRST")
			
				myTaxiData.vsEnterSeat = TAXI_GET_PROPER_SEAT(myTaxiData.viTaxi, myTaxiData.piTaxiPassenger)
				piLeftOverPassenger = GET_LEFT_OVER_PASSENGER_IN_TAXI(myTaxiData,vsLeftOverPassengersSeat)
				
				//Handles telling a left over passenger to GTFO B*513153
				
				IF NOT IS_ENTITY_DEAD(piLeftOverPassenger)
					//If the seat he's in is the same as the one the passenger is trying to enter have him exit out the other side.
					IF vsLeftOverPassengersSeat = myTaxiData.vsEnterSeat
						IF myTaxiData.vsEnterSeat = VS_BACK_LEFT
							ecf_exitFlag = ECF_USE_RIGHT_ENTRY
							CDEBUG1LN(DEBUG_OJ_TAXI,"Found Left Over Passenger leaving out of the  ECF_USE_RIGHT_ENTRY")
						ELSE
							ecf_exitFlag = ECF_USE_LEFT_ENTRY
							CDEBUG1LN(DEBUG_OJ_TAXI,"Found Left Over Passenger leaving out of the  ECF_USE_LEFT_ENTRY")
						ENDIF
						TASK_LEAVE_ANY_VEHICLE(piLeftOverPassenger,0,ecf_exitFlag)
					ELSE
					
						TASK_LEAVE_ANY_VEHICLE(piLeftOverPassenger)
					ENDIF
				ENDIF
				
				//Needs excitement wants adrenaline seeking passenger in the front seat SO FOR NOW WE JUST OVERWRITE IT
				IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
					IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,VS_FRONT_RIGHT)
						myTaxiData.vsEnterSeat = VS_FRONT_RIGHT
						IF NOT IS_ENTITY_DEAD(piLeftOverPassenger)
							TASK_LEAVE_ANY_VEHICLE(piLeftOverPassenger,0,ECF_JUMP_OUT)
							CDEBUG1LN(DEBUG_OJ_TAXI,"Found Left Over Passenger leaving out of the  VS_FRONT_RIGHT")
						ENDIF
					ENDIF
				ENDIF
						
				taxiEnterCut = TAXI_PED_ENTER_CUT_SECOND
				
            ENDIF
        BREAK
		
		CASE TAXI_PED_ENTER_CUT_SECOND
			
			#IF IS_DEBUG_BUILD
			sLOSTest = "sLOSTest = "
			sLOSTest += TAXI_UTILS_GET_STRING_FROM_INT_SP(iLOSTest)
			CDEBUG_MESSAGE_OJ_TAXI_PERIODIC(sLOSTest, myTaxiData.iTaxiOJ_DebugThrottle)
			#ENDIF
			
			//Front Right <<0.773, -0.646, 0.43>>
			IF myTaxiData.vsEnterSeat = VS_FRONT_RIGHT
				vWarpPos = <<1.5, 0.0, -0.6422>>//<<2
				vOffset = <<-1.4309, 3.9580, 3.5037>>
				vLookAt = <<0.1017, 1.3354, -0.0925>>
				
            ELIF myTaxiData.vsEnterSeat = VS_BACK_RIGHT
                vWarpPos = <<1.5, -1.0, -0.6422>>//<<2.773, -0.646, -0.6422>>//<< 1.7, -0.75, -0.6422>> //0.357628 >>
                vOffset = <<1.4309, 3.9580, 0.5037>>
                vLookAt = <<0.1017, 1.3354, -0.0925>>
            ELSE
               	vWarpPos = <<-1.5, -1.0, -0.6422>> //<<-2.774,-0.546, -0.6206>>//<< -1.7, -0.75, -0.6206 >>
                vOffset = <<-1.4823, 4.2333, 0.5939>>//<<-2.0871, 0.6131, 0.7690>>
                vLookAt = <<-0.4718, 1.4282, 0.3619>>
            ENDIF
			
			iLOSTest = IS_LOS_SHAPETEST_CLEAR_BETWEEN_COORDS(myTaxiData.tShapeTest, 
															//GET_ENTITY_COORDS(myTaxiData.viTaxi, FALSE),
															GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << 0, 2.2, 0.275>>),
															GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, vOffset),
															TRUE)
			
			// if LOS is blocked, raise the camera
			IF iLOSTest = 1
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"iLOSTest = 1, raising the offset.z by 3")
				
				vOffset.z += 1.5
				
				IF myTaxiData.vsEnterSeat = VS_BACK_RIGHT
					vLookAt = <<0.7632, 1.4884, 0.4369>>
				ENDIF
			ENDIF
			
			IF iLOSTest != -1
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"READY TO ENTER TAXI ENTRY CUTSCENE")
				
				//Cutscene prep - Turn off hud , clear everything...
	            ODDJOB_ENTER_CUTSCENE()
				
				CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(myTaxiData.viTaxi),3.0)
	            //CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(myTaxiData.viTaxi), TAXI_OJ_RADIUS_CLEAR_VEHICLE)
	            CLEAR_AREA_OF_OBJECTS(GET_ENTITY_COORDS(myTaxiData.viTaxi), TAXI_OJ_RADIUS_CLEAR_VEHICLE)
				
	            CLEAR_HELP()
	            CLEAR_PRINTS()
	            KILL_FACE_TO_FACE_CONVERSATION()
	            ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				
				//Teleport Passenger
				VECTOR vTemp
				vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, vWarpPos)
	            SET_ENTITY_COORDS(myTaxiData.piTaxiPassenger, vTemp, TRUE)
	          	SET_ENTITY_HEADING(myTaxiData.piTaxiPassenger, GET_PASSENGER_HEADING_TO_ENTRY_POINT(myTaxiData))
				
				//Get camera set up	
				CREATE_TAXI_OJ_CAM(myTaxiData.camTaxi,<<1,1,1>>, <<0,0,0>>)
				
				//ATTACH_CAM_TO_ENTITY(myTaxiData.camTaxi, myTaxiData.viTaxi, vOffset)
	            SET_CAM_COORD(myTaxiData.camTaxi, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, vOffset))
				POINT_CAM_AT_ENTITY(myTaxiData.camTaxi, myTaxiData.viTaxi, vLookAt)
				//SET_CAM_FOV(myTaxiData.camTaxi, 60)
	            SET_CAM_ACTIVE(myTaxiData.camTaxi,TRUE)
	            RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(myTaxiData.piTaxiPassenger)
	            TASK_ENTER_VEHICLE(myTaxiData.piTaxiPassenger, myTaxiData.viTaxi, DEFAULT_TIME_BEFORE_WARP, myTaxiData.vsEnterSeat , myTaxiData.fTaxiEnterSpeed,ECF_WARP_ENTRY_POINT | ECF_RESUME_IF_INTERRUPTED)
	            
	            taxiEnterCut = TAXI_PED_ENTER_CUT_WAIT
			ENDIF
		BREAK
		
        CASE TAXI_PED_ENTER_CUT_WAIT
			CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_PED_ENTER_CUT_WAIT",myTaxiData.iTaxiOJ_DebugThrottle)
			
            IF TIMERA() > 3500 //OR IS_PED_SITTING_IN_ANY_VEHICLE(myTaxiData.piTaxiPassenger)
                taxiEnterCut =  TAXI_PED_ENTER_CUT_END
            ENDIF
        
        BREAK
        CASE TAXI_PED_ENTER_CUT_ENTER

        BREAK
        CASE TAXI_PED_ENTER_CUT_SKIP
            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_PED_ENTER_CUT_SKIP")
            IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger) AND NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
                IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiData.piTaxiPassenger)
                    SET_PED_INTO_VEHICLE(myTaxiData.piTaxiPassenger, myTaxiData.viTaxi, myTaxiData.vsEnterSeat)
					SET_VEHICLE_DOOR_SHUT(myTaxiData.viTaxi,TAXI_OJ_CONVERT_VEHICLE_SEAT_TO_DOOR_LIST( myTaxiData.vsEnterSeat))
                ENDIF   
                RENDER_SCRIPT_CAMS(FALSE,FALSE)
                DESTROY_CAM(myTaxiData.camTaxi)
                DESTROY_CAM(myTaxiData.camInterp)
                SET_GAMEPLAY_CAM_RELATIVE_HEADING()
                SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				CLEAR_HELP()
				CLEAR_PRINTS()
				KILL_FACE_TO_FACE_CONVERSATION()
				ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
			
                taxiEnterCut = TAXI_PED_ENTER_CUT_FADEIN
            ENDIF
        BREAK
        CASE TAXI_PED_ENTER_CUT_FADEIN
           
            IF IS_SCREEN_FADED_IN()
            AND NOT IS_SCREEN_FADING_IN()
				ODDJOB_EXIT_CUTSCENE()
				CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_PED_ENTER_CUT_FADEIN")

	            RETURN TRUE
			ENDIF
        BREAK
        CASE TAXI_PED_ENTER_CUT_END
            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_PED_ENTER_CUT_SKIP")
            RENDER_SCRIPT_CAMS(FALSE,FALSE)
            DESTROY_CAM(myTaxiData.camTaxi)
            DESTROY_CAM(myTaxiData.camInterp)
            SET_GAMEPLAY_CAM_RELATIVE_HEADING()
            SET_GAMEPLAY_CAM_RELATIVE_PITCH()
            ODDJOB_EXIT_CUTSCENE()
            RETURN TRUE
        BREAK
		CASE TAXI_PED_NO_CUTSCENE
			IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger) AND NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
                
				//IF IS_PED_IN_ANY_VEHICLE(myTaxiData.piTaxiPassenger, TRUE)
				IF IS_PED_GETTING_INTO_A_VEHICLE(myTaxiData.piTaxiPassenger)
				OR IS_PED_IN_ANY_VEHICLE(myTaxiData.piTaxiPassenger, TRUE)
					RETURN TRUE
				ELSE
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK
					//AND NOT GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) <> FINISHED_TASK
						TASK_ENTER_VEHICLE(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi, DEFAULT_TIME_BEFORE_WARP,myTaxiData.vsEnterSeat,myTaxiData.fTaxiEnterSpeed)
					ENDIF
				ENDIF
			ENDIF
		BREAK
    ENDSWITCH
    RETURN FALSE
ENDFUNC

/// PURPOSE: Detects if a vehicle has fucked itself.  If it's on it's back, it'll just start a very rapid deterioration.
/// PARAMS:
///    VehicleID - vehicle to check
/// RETURNS:  TRUE if vehicle is fucked,  FALSE otherwise.  Will also destroy vehicles that have been flipped.
///    
FUNC BOOL IS_TAXI_OJ_VEHICLE_FUCKED(VEHICLE_INDEX VehicleID)

    IF NOT IS_VEHICLE_DRIVEABLE(VehicleID)
		CDEBUG1LN(DEBUG_OJ_TAXI,"IS_VEHICLE_DRIVEABLE messed it up")
		RETURN TRUE
	ENDIF

    IF GET_VEHICLE_ENGINE_HEALTH(VehicleID) = 0
        IF NOT IS_VEHICLE_DRIVEABLE(VehicleID)
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_VEHICLE_ENGINE_HEALTH(VehicleID = ", GET_VEHICLE_ENGINE_HEALTH(VehicleID))
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_ENTITY_HEALTH(VehicleID) = ", GET_ENTITY_HEALTH(VehicleID))			
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_VEHICLE_ENGINE_HEALTH messed it up")
		ENDIF
		
		RETURN(TRUE)
    ELSE        
        
        IF IS_VEHICLE_STUCK_TIMER_UP( VehicleID, VEH_STUCK_ON_ROOF, SIDE_TIME)//ROOF_TIME)
        OR IS_VEHICLE_STUCK_TIMER_UP( VehicleID, VEH_STUCK_ON_SIDE, SIDE_TIME)
            RETURN TRUE
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

/// PURPOSE: Detects if a vehicle has fucked itself.  If it's on it's back, it'll just start a very rapid deterioration.
/// PARAMS:
///    VehicleID - vehicle to check
/// RETURNS:  TRUE if vehicle is fucked,  FALSE otherwise.  Will also destroy vehicles that have been flipped.
///    
FUNC BOOL IS_TAXI_OJ_VEHICLE_ENGINE_FUCKED(VEHICLE_INDEX VehicleID)

	IF NOT IS_VEHICLE_DRIVEABLE(VehicleID)
		RETURN TRUE
	ENDIF

    IF GET_VEHICLE_ENGINE_HEALTH(VehicleID) < -100
//        IF NOT IS_VEHICLE_DRIVEABLE(VehicleID)
//			CDEBUG1LN(DEBUG_OJ_TAXI,"IS_VEHICLE_DRIVEABLE messed it up")
//		ELSE
//			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_VEHICLE_ENGINE_HEALTH messed it up")
//		ENDIF
		
		RETURN(TRUE)
    ELSE        
        
        IF IS_VEHICLE_STUCK_TIMER_UP( VehicleID, VEH_STUCK_ON_ROOF, SIDE_TIME)//ROOF_TIME)
        OR IS_VEHICLE_STUCK_TIMER_UP( VehicleID, VEH_STUCK_ON_SIDE, SIDE_TIME)
            RETURN TRUE
        ENDIF
    ENDIF

    RETURN(FALSE)
ENDFUNC

PROC TAXI_HANDLE_WANTED_PRE_PICKUP(TaxiStruct &myTaxiData)
    MONITOR_TAXI_PASSENGER_OK(myTaxiData, myTaxiData.piTaxiPassenger)
	
	
	//B*518789 - Taxi blip wasn't getting removed if got it during wanted level
	IF IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
		IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
		   	CDEBUG1LN(DEBUG_OJ_TAXI,"SET_PLAYER_REENTERED_TAXI_OJ called in 3116")
			SET_PLAYER_REENTERED_TAXI_OJ(myTaxiData)
		    IF DOES_BLIP_EXIST(myTaxiData.blipTaxiVehicle)
                REMOVE_BLIP(myTaxiData.blipTaxiVehicle)
			ENDIF
		ENDIF
	ENDIF
	
    IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_POLICE)
        
        IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
            SET_BLIP_ALPHA(myTaxiData.blipTaxiPassenger,0)
            SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger, FALSE)
        ENDIF
        
        TAXI_RESET_TIMERS(myTaxiData, TT_POLICE)
        
        PRINT_NOW("TAXI_OBJ_POL", DEFAULT_GOD_TEXT_TIME, 1)
    ENDIF
ENDPROC

FUNC FLOAT GET_TAXI_DISTANCE_FROM_ENTITY(TaxiStruct &myTaxiData, ENTITY_INDEX eiEntity)
    IF NOT IS_ENTITY_DEAD(eiEntity)
        IF IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
            RETURN GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,eiEntity)
        ENDIF
    ENDIF
    RETURN 0.0
ENDFUNC

FUNC BOOL SHOULD_TRIGGER_TAXI_ENTITY_OPT(TaxiStruct &myTaxiData, INT iTimeTilAction = 5, FLOAT minDist = 35.0, FLOAT fOverRideDist = 8.0, FLOAT minSpeed = 4.0)
    IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
            
        IF NOT IS_ENTITY_OCCLUDED(myTaxiData.piTaxiPassenger) AND GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) < minDist
            IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_PASSENGER_ACTION) 
                TAXI_RESET_TIMERS(myTaxiData, TT_PASSENGER_ACTION)
                CDEBUG1LN(DEBUG_OJ_TAXI,"SHOULD_TRIGGER_TX_ENTITY_OPT - Timer to walk over is started")
            ENDIF
        ELSE
            IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_PASSENGER_ACTION)
                TAXI_CANCEL_TIMERS(myTaxiData, TT_PASSENGER_ACTION)
            ENDIF
        ENDIF
        
        IF (GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_PASSENGER_ACTION) > iTimeTilAction AND GET_ENTITY_SPEED(myTaxiData.viTaxi) < minSpeed AND NOT IS_ENTITY_OCCLUDED(myTaxiData.piTaxiPassenger))
        OR GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) <= fOverRideDist 
          
            RETURN TRUE
        ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC
FUNC BOOL IS_PLAYER_FUCKING_WITH_PICKING_UP_PASSENGER(TaxiStruct &myTaxiData)
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 sTimeDickingWPassenger
	
		sTimeDickingWPassenger = "Wait Til Leave = "
		sTimeDickingWPassenger += ROUND(GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_PASSENGER_ACTION))
		sTimeDickingWPassenger += "s | "
		sTimeDickingWPassenger += ROUND(TAXI_CONST_FAIL_FOR_NOT_PICKING_UP)
		DRAW_DEBUG_TEXT_2D(sTimeDickingWPassenger,<<0.8,0.32,0.0>>)
	#ENDIF
			
	IF (GET_ENTITY_SPEED(myTaxiData.viTaxi) > 3.0//NOT IS_VEHICLE_STOPPED(myTaxiData.viTaxi)
		AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_PASSENGER_ACTION) > TAXI_CONST_FAIL_FOR_NOT_PICKING_UP)
	OR IS_ENTITY_UPSIDEDOWN(myTaxiData.viTaxi)

		CDEBUG1LN(DEBUG_OJ_TAXI,"IS_PLAYER_FUCKING_WITH_PICKING_UP_PASSENGER - Player abandoned the pickup.")
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
ENDFUNC
//Handles the passenger being tasked to go the player's taxi but the player drives away
PROC TAXI_PASSENGER_ENTER_CAB_FAILSAFE_IF_PLAYER_LEAVES(TaxiStruct &myTaxiData)
    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
        IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 sDistanceToPassenger
			
				sDistanceToPassenger = "Passenger = "
				sDistanceToPassenger += ROUND(GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger))
				sDistanceToPassenger += "m | "
				sDistanceToPassenger += ROUND(CONST_TAXI_PASSENGER_PICKUP_DIST_FAIL)
				DRAW_DEBUG_TEXT_2D(sDistanceToPassenger,<<0.8,0.3,0.0>>)
			#ENDIF
			
            IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) > CONST_TAXI_PASSENGER_PICKUP_DIST_FAIL
			OR IS_PLAYER_FUCKING_WITH_PICKING_UP_PASSENGER(myTaxiData)
			
                IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
                OR GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
                    
					SEQUENCE_INDEX tempSequenceIndex
		            CLEAR_SEQUENCE_TASK(tempSequenceIndex)
		            OPEN_SEQUENCE_TASK(tempSequenceIndex)
						
						
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiData.viTaxi)
						TASK_PLAY_ANIM(NULL, "misscommon@response","screw_you")
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, myTaxiData.vTaxiOJSpawn, myTaxiData.fTaxiEnterSpeed)
					
					CLOSE_SEQUENCE_TASK(tempSequenceIndex)
					TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, tempSequenceIndex)
					
                    TAXI_SET_FAIL(myTaxiData,"Player abandoned passenger on pickup.",TFS_ABANDONED_PASSENGER)
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_PASSENGER_ENTER_CAB_FAILSAFE_IF_PLAYER_LEAVES - Player abandoned the pickup.")
                    
                ENDIF
            ENDIF
        ENDIF
    ENDIF
ENDPROC
FUNC VECTOR GET_NEAREST_TAXI_DOOR_POS(TaxiStruct &myTaxiData)

    myTaxiData.vsEnterSeat = TAXI_GET_PROPER_SEAT(myTaxiData.viTaxi, myTaxiData.piTaxiPassenger)

    VECTOR vOffset        
    IF myTaxiData.vsEnterSeat = VS_BACK_RIGHT
        vOffset = <<2.5, -1.0, -0.6422>>
    ELSE
        vOffset = <<-2.5, -1.0, -0.6422>>
    ENDIF
    
	RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, vOffset)

ENDFUNC

PROC UPDATE_OJ_TAXI_MILEAGE(TaxiStruct &myTaxiData, FLOAT &fTempMileage)
	
	STAT_GET_FLOAT(myTaxiData.sDrivingStat,fTempMileage)
			
	myTaxiData.fTaxiOJ_CurrentMileage = fTempMileage - myTaxiData.fTaxiOJ_InitialMileage
	//CDEBUG1LN(DEBUG_OJ_TAXI,"Current Mileage = ", fTempMileage, " - Initial Mileage ", myTaxiData.fTaxiOJ_InitialMileage, " = ",  myTaxiData.fTaxiOJ_CurrentMileage)
									
ENDPROC
/// PURPOSE: New Pickup Func that is set up to play like IV's Roman taxi pickup missions
///    1)Once taxi is near passenger, give help text to stop or honk near passenger
///    2)Once in the area - autostop cab & remove player control
///    3)Engage POI cam as passenger runs to the cab
/// PARAMS:
///    myTaxiData - taxi data
///    theMainMeter - hud meter
/// RETURNS: TRUE once passenger is in cab?
FUNC BOOL TAXI_HANDLE_IV_PICKUP_NO_METER(TaxiStruct &myTaxiData, BOOL bDebug= FALSE, FLOAT fPedLookDistance = TAXI_DIALOGUE_MAX_DISTANCE)
	
	PED_INDEX piLeftOverPassenger
	VEHICLE_SEAT vsLeftOverPassengersSeat
	ENTER_EXIT_VEHICLE_FLAGS ecf_exitFlag
	
	//Always have to check if taxi and player are alive and well
    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)        
        //If player is not in TAXI remind to get back in or suffer consequences---------------------------------------------------------
        IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
            IF (GET_GAME_TIMER() % 1000) < 50
				CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_YELL_PLAYER_TO_RETURN called from TAXI_HANDLE_IV_PICKUP_NO_METER")
			ENDIF
			TAXI_YELL_PLAYER_TO_RETURN(myTaxiData)
            IF IS_OJ_TAXI_PLAYER_CONTROL_OFF()//LM if player dives just before a pickup, their controls might be off
                SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
            ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
			OR GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = WAITING_TO_START_TASK
				CLEAR_PED_TASKS_IMMEDIATELY(myTaxiData.piTaxiPassenger)
				
				myTaxiData.iTaxiOJ_StatesPickup = 0
				taxiDriveUp = TAXI_PED_DRIVE_UP_INIT
				
				TASK_TURN_PED_TO_FACE_ENTITY(myTaxiData.piTaxiPassenger, PLAYER_PED_ID())
			ENDIF
		
		//Handle Going WANTED
        ELIF IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
            TAXI_HANDLE_WANTED_PRE_PICKUP(myTaxiData)
       		IF myTaxiData.iTaxiOJ_StatesPickup > 1
				CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_HANDLE_IV_PICKUP_NO_METER passenger tasked to walk back to spawn point")
				SEQUENCE_INDEX tempSequenceIndex
	            CLEAR_SEQUENCE_TASK(tempSequenceIndex)
	            OPEN_SEQUENCE_TASK(tempSequenceIndex)
					
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, myTaxiData.vTaxiOJSpawn, myTaxiData.fTaxiEnterSpeed)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiData.viTaxi)
				
				CLOSE_SEQUENCE_TASK(tempSequenceIndex)
				TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, tempSequenceIndex)
				myTaxiData.iTaxiOJ_StatesPickup = 0
			ENDIF
			
		ELSE //Else player is in taxi-----------------------------------------------------------------------------------------------------
					
			//Make sure to cancel the "I'm outside the cab" timer as above check proves it wrong
            IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
                CDEBUG1LN(DEBUG_OJ_TAXI,"SET_PLAYER_REENTERED_TAXI_OJ called in 3298")
				SET_PLAYER_REENTERED_TAXI_OJ(myTaxiData)
                SET_TAXI_ABANDON_BLIP_ON_OFF(myTaxiData,FALSE)
            ENDIF
            
            IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_POLICE)
                TAXI_CANCEL_TIMERS(myTaxiData, TT_POLICE)
                CLEAR_PRINTS()
                IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
                    SET_BLIP_ALPHA(myTaxiData.blipTaxiPassenger,255)
                    SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger, TRUE)
				
                ENDIF
            ENDIF
            
            //Always check PED is fine before anything else
            IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
				
				//BLIP_TAXI_OJ_PASSENGER(myTaxiData)
				FLOAT fDistaForPickup = (GET_ENTITY_SPEED(myTaxiData.viTaxi)/TAXI_TWEAKS_SPEED_FACTOR) + TAXI_TWEAKS_PICKUP_PAD
				
				IF myTaxiData.bPassengerObjPrinted
				AND GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData, myTaxiData.piTaxiPassenger) > 300
					TAXI_SET_FAIL(myTaxiData, "Left Passenger", TFS_ABANDONED_PASSENGER)
				ENDIF
				
                SWITCH myTaxiData.iTaxiOJ_StatesPickup
                    
                    //Wait til player is near the passenger to stop
                    CASE 0      
                        IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK  //TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
						OR (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), myTaxiData.piTaxiPassenger) < 20 //10
							AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.piTaxiPassenger, myTaxiData.vTaxiOJSpawn) <= CONST_TAXI_PASSENGER_PICKUP_DIST
							AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJSpawn) <= CONST_TAXI_PASSENGER_PICKUP_DIST)
							
								IF UPDATE_PED_DRIVE_UP(myTaxiData, fPedLookDistance)
		                            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_HANDLE_IV_PICKUP_NO_METER: UPDATE_PED_DRIVE_UP returned true, going to Case 1")
									
									myTaxiData.iTaxiOJ_StatesPickup++
		                        ENDIF
						ELSE
							
						ENDIF 
                    BREAK
                

                    CASE 1
                        //Sets the PED to stand and wave at the player until he pulls up close enough to him/her.
                        IF SHOULD_TRIGGER_TAXI_ENTITY_OPT(myTaxiData, 0, CONST_TAXI_PASSENGER_PICKUP_DIST, fDistaForPickup, 4)
							//myTaxiData.vTxOJ_EntryDoorPos = GET_NEAREST_TAXI_DOOR_POS(myTaxiData)
							myTaxiData.bPedEntering = TRUE
							
							//CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
							
							IF bNoEnterCutscene
								
								myTaxiData.vsEnterSeat = TAXI_GET_PROPER_SEAT(myTaxiData.viTaxi, myTaxiData.piTaxiPassenger)
								piLeftOverPassenger = GET_LEFT_OVER_PASSENGER_IN_TAXI(myTaxiData,vsLeftOverPassengersSeat)
								
								//Handles telling a left over passenger to GTFO B*513153
								
								IF NOT IS_ENTITY_DEAD(piLeftOverPassenger)
									//If the seat he's in is the same as the one the passenger is trying to enter have him exit out the other side.
									IF vsLeftOverPassengersSeat = myTaxiData.vsEnterSeat
										IF myTaxiData.vsEnterSeat = VS_BACK_LEFT
											ecf_exitFlag = ECF_USE_RIGHT_ENTRY
											CDEBUG1LN(DEBUG_OJ_TAXI,"Found Left Over Passenger leaving out of the  ECF_USE_RIGHT_ENTRY")
										ELIF myTaxiData.vsEnterSeat = VS_BACK_RIGHT
											ecf_exitFlag = ECF_USE_LEFT_ENTRY
											CDEBUG1LN(DEBUG_OJ_TAXI,"Found Left Over Passenger leaving out of the  ECF_USE_LEFT_ENTRY")
										ELSE
											CDEBUG1LN(DEBUG_OJ_TAXI,"Found Left Over Passenger in front seat, no flag set")
										ENDIF
										TASK_LEAVE_ANY_VEHICLE(piLeftOverPassenger,0,ecf_exitFlag)
									ELSE
										TASK_LEAVE_ANY_VEHICLE(piLeftOverPassenger)
									ENDIF
								ENDIF
								
								//Needs excitement wants adrenaline seeking passenger in the front seat SO FOR NOW WE JUST OVERWRITE IT
								IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
									IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,VS_FRONT_RIGHT)
										IF myTaxiData.vsEnterSeat = VS_BACK_RIGHT
											CDEBUG1LN(DEBUG_OJ_TAXI,"this is Needs Excitement and the passenger is coming from the right, sending to the front seat.")
											
											myTaxiData.vsEnterSeat = VS_FRONT_RIGHT
											IF NOT IS_ENTITY_DEAD(piLeftOverPassenger)
												TASK_LEAVE_ANY_VEHICLE(piLeftOverPassenger,0,ECF_JUMP_OUT)
												CDEBUG1LN(DEBUG_OJ_TAXI,"Found Left Over Passenger leaving out of the  VS_FRONT_RIGHT")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//TASK_GO_STRAIGHT_TO_COORD(myTaxiData.piTaxiPassenger, myTaxiData.vTxOJ_EntryDoorPos, myTaxiData.fTaxiEnterSpeed)
							CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
                            TASK_ENTER_VEHICLE(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi, DEFAULT_TIME_BEFORE_WARP,myTaxiData.vsEnterSeat,myTaxiData.fTaxiEnterSpeed)
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_HANDLE_IV_PICKUP_NO_METER: SHOULD_TRIGGER_TAXI_ENTITY_OPT returned true, going to Case 2")
							
                            myTaxiData.iTaxiOJ_StatesPickup++
                        ELSE
							IF bDebug
								DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(myTaxiData.piTaxiPassenger),fDistaForPickup,0,0,255,TAXI_TWEAKS_SPEED_ALPHA)
							ENDIF
							
							IF NOT IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
								CDEBUG1LN(DEBUG_OJ_TAXI,"NOT IS_PLAYER_DRIVING_TAXI_OJ, clearing ped tasks")
								//IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
									CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
								//ENDIF
							ELSE
								
								IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
								AND GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PERFORM_SEQUENCE) <> FINISHED_TASK
								AND GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData, myTaxiData.piTaxiPassenger) > 8
									CDEBUG1LN(DEBUG_OJ_TAXI,"re-tasking player anim")
									CDEBUG1LN(DEBUG_OJ_TAXI,"script task status = ", GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PERFORM_SEQUENCE))
									CDEBUG1LN(DEBUG_OJ_TAXI,"distance = ", GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData, myTaxiData.piTaxiPassenger))
									
									CLEAR_SEQUENCE_TASK(myTaxiData.siSeqIndex)
						            OPEN_SEQUENCE_TASK(myTaxiData.siSeqIndex)
						                TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
										IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
											TASK_PLAY_ANIM(NULL, "gestures@m@standing@casual", "gesture_nod_yes_hard",NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY) //| AF_LOOPING)
										ELSE
											TASK_PLAY_ANIM(NULL, "oddjobs@towingcome_here", "come_here_idle_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY )//| AF_LOOPING)
										ENDIF
						            CLOSE_SEQUENCE_TASK(myTaxiData.siSeqIndex)
						            TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, myTaxiData.siSeqIndex)
								ENDIF
							ENDIF
							
							IF IS_ENTITY_PLAYING_ANIM(myTaxiData.piTaxiPassenger,"oddjobs@towingcome_here", "come_here_idle_c")
								SET_ENTITY_ANIM_SPEED(myTaxiData.piTaxiPassenger,"oddjobs@towingcome_here", "come_here_idle_c", 1.75)
							ENDIF
							
                        ENDIF       
                    BREAK
                    
                    CASE 2
                        
						IF bNoEnterCutscene
							IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi, myTaxiData.piTaxiPassenger) < 3
								IF NOT IS_TAXI_CLEAR_OF_DEAD_BODIES(myTaxiData)
									CLEAR_PED_TASKS_IMMEDIATELY(myTaxiData.piTaxiPassenger)
									TAXI_SET_FAIL(myTaxiData,"You had a dead body in your back seat.",TFS_SPOOKED)
								ELIF NOT IS_TAXI_VEHICLE_DAMAGE_OK(myTaxiData.viTaxi)
									CLEAR_PED_TASKS_IMMEDIATELY(myTaxiData.piTaxiPassenger)
									TAXI_SET_FAIL(myTaxiData,"You had a dead body in your back seat.", TFS_TAXI_DISABLED)
								ENDIF
				            ENDIF
						ENDIF
						
						TAXI_PASSENGER_ENTER_CAB_FAILSAFE_IF_PLAYER_LEAVES(myTaxiData)
                        
						//Once passenger is in range OR for some reason control was pulled away from player, TRIGGER ENTER CUTSCENE
                        IF GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData,myTaxiData.piTaxiPassenger) < fDistaForPickup
                        OR IS_OJ_TAXI_PLAYER_CONTROL_OFF()
                        
                            IF IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi) //LM extra check just in case player dives
                                IF UPDATE_PASSENGER_ENTER_CUTSCENE(myTaxiData)
									TAXI_OJ_REMOVE_PASSENGER_BLIP(myTaxiData)
									
									iEnterDelayTimer = GET_GAME_TIMER()
									
									SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger,PCF_DontAllowToBeDraggedOutOfVehicle,TRUE)

									TAXI_CANCEL_TIMERS(myTaxiData, TT_PASSENGER_ACTION)
									
									CLEAR_GPS_FLAGS()
									
									STAT_GET_FLOAT(myTaxiData.sDrivingStat,myTaxiData.fTaxiOJ_InitialMileage)
									CDEBUG1LN(DEBUG_OJ_TAXI,"Initial Taxi Mileage = ", myTaxiData.fTaxiOJ_InitialMileage)

									CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_ENTER_CUTSCENE returning true")
									
									SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(myTaxiData.piTaxiPassenger, FALSE)
									
                                    RETURN TRUE
                                ENDIF
                            ENDIF
                        ELSE
                            IF bDebug
                                CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Speed is: ", "", GET_ENTITY_SPEED(myTaxiData.viTaxi),"", " Dist = ","", GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData,myTaxiData.piTaxiPassenger), " waiting for distance: ","",fDistaForPickup)
                                DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(myTaxiData.piTaxiPassenger),fDistaForPickup,0,0,255,TAXI_TWEAKS_SPEED_ALPHA)
                            ENDIF  
							
                        ENDIF
						
						//if player drives away from the passenger while they try to walk up, reset the pickup states----------------------------------
						//Commenting this out for B*1311050
//						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.piTaxiPassenger, myTaxiData.vTaxiOJSpawn) > CONST_TAXI_PASSENGER_PICKUP_DIST+5
//						OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJSpawn) > CONST_TAXI_PASSENGER_PICKUP_DIST+5
//						OR GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData,myTaxiData.piTaxiPassenger) > CONST_TAXI_PASSENGER_PICKUP_DIST
//							
//							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_HANDLE_IV_PICKUP_NO_METER passenger tasked to walk back to spawn point")
//							SEQUENCE_INDEX tempSequenceIndex
//				            CLEAR_SEQUENCE_TASK(tempSequenceIndex)
//				            OPEN_SEQUENCE_TASK(tempSequenceIndex)
//								
//								
//								TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiData.viTaxi)
//								TASK_PLAY_ANIM(NULL, "misscommon@response","screw_you")
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, myTaxiData.vTaxiOJSpawn, myTaxiData.fTaxiEnterSpeed)
//							
//							CLOSE_SEQUENCE_TASK(tempSequenceIndex)
//							TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, tempSequenceIndex)
//							myTaxiData.iTaxiOJ_StatesPickup = 0
//							taxiDriveUp = TAXI_PED_DRIVE_UP_INIT
//						
//						ENDIF
						
                    BREAK
                
                ENDSWITCH
            ENDIF
        ENDIF
	//ELSE
	//	IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi, myTaxiData.piTaxiPassenger) < 3
	//		CLEAR_PED_TASKS_IMMEDIATELY(myTaxiData.piTaxiPassenger)
	//		TAXI_SET_FAIL(myTaxiData,"You had a dead body in your back seat.", TFS_TAXI_DISABLED)
    //    ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC

/// PURPOSE: Let's you know when the player has hit the dropoff point, 
///    and has slowed/come to a stop before the taxiTimer has ticked down passed the deadline
///    
/// PARAMS:
///    myTaxiData - Our global Taxi data
/// RETURNS:
///    TRUE when you've reached the dropoff point before the timer expires
FUNC BOOL HAS_TAXI_OJ_REACHED_DROPOFF(TaxiStruct &myTaxiData, FLOAT fRadius = TAXI_OJ_CONST_GATEWAY_SIZE_SMALL)
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE(myTaxiData.vTaxiOJDropoff,fRadius+1,0,0,255,80)
	#ENDIF
	
	// creating a special case for deadline since we have to ensure that the passenger here takes a short path.
	// the deadline passenger has to get in the casino and lock the door before giving control back tot he player
	IF GET_TAXI_MISSION_TYPE(myTaxiData) = TXM_03_DEADLINE
		// Drawing the corona
		IF IS_ENTITY_AT_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJDropoff, << fRadius+1, fRadius+1, LOCATE_SIZE_HEIGHT >>, !(myTaxiData.bIsCurrentlyWanted))
		ENDIF
		
		// Actual dropoff check
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff) <= fRadius+1// IS_ENTITY_IN_ANGLED_AREA(myTaxiData.viTaxi, <<928.292358,66.219345,78.729050>>, <<909.166321,35.528076,83.289467>>, 16.250000)
		AND GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
	    AND CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)
	    AND IS_VEHICLE_ON_ALL_WHEELS(myTaxiData.viTaxi)
	        RETURN IS_TAXI_FULLY_STOPPED(myTaxiData, TRUE, fRadius)
	    ENDIF
	ELSE
		IF IS_ENTITY_AT_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJDropoff, << fRadius+1, fRadius+1, LOCATE_SIZE_HEIGHT >>, !(myTaxiData.bIsCurrentlyWanted)) //TRUE)
		AND GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
	    AND CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)
	    AND IS_VEHICLE_ON_ALL_WHEELS(myTaxiData.viTaxi)
	        RETURN IS_TAXI_FULLY_STOPPED(myTaxiData, TRUE, fRadius)
	    ENDIF
	ENDIF
	
    RETURN FALSE
ENDFUNC

PROC TAXI_OJ_MONITOR_PLAYER_HORN_HONK(TaxiStruct &myTaxiData)
    //Keep track how much the player is using the horn
    IF IS_PLAYER_PRESSING_HORN(GET_PLAYER_INDEX())
	AND NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_HORN_HONKED)
        SET_BITMASK_AS_ENUM(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_HORN_HONKED)
		
	ELIF NOT IS_PLAYER_PRESSING_HORN(GET_PLAYER_INDEX())
	AND IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_HORN_HONKED)
		
		CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_HORN_HONKED)
		
		myTaxiData.iTaxiOJ_HornHonkCount++
        TAXI_STATS_UPDATE(TAXI_STAT_NUM_HORN_HONKS)
        CDEBUG1LN(DEBUG_OJ_TAXI,"Player has honked horn for:", "", myTaxiData.iTaxiOJ_HornHonkCount, "", " times.")
    ENDIF
    
    //Bitch if he's honked one too many times than reset the counter
    IF myTaxiData.iTaxiOJ_HornHonkCount >  TAXI_TWEAKS_NUM_HONKS_BEFORE_BITCH
        SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_HORN_HONK,TRUE)
        myTaxiData.iTaxiOJ_HornHonkCount = 0
        myTaxiData.iTaxiOJ_PassengerExcitement += CEIL(TAXI_TWEAKS_VALUE_HORN_EXCITEMENT)
    ENDIF
ENDPROC
/// PURPOSE: Mutator for our taxi's internal wanted tracker
///    
/// PARAMS:
///    myTaxiData - global taxi data
///    iWantedLevel - new level to set
PROC SET_TAXI_OJ_WANTED_LEVEL(TaxiStruct &myTaxiData, INT iWantedLevel)
    IF myTaxiData.iTaxiOJ_WantedLevel <> iWantedLevel
        myTaxiData.iTaxiOJ_WantedLevel = iWantedLevel
    ENDIF
    
ENDPROC

/// PURPOSE: Accessor for Taxi's Wanted Level tracker
///    
/// PARAMS:
///    myTaxiData - our global taxi data
/// RETURNS:
///    
FUNC INT GET_TAXI_OJ_WANTED_LEVEL(TaxiStruct &myTaxiData)
    
    RETURN myTaxiData.iTaxiOJ_WantedLevel
    
ENDFUNC
/// PURPOSE: Monitors when the player goes wanted and taking care of everything that needs it like: dialogue/objectives
///
///    
/// PARAMS:
///    myTaxiData - our global taxi data
PROC TAXI_OJ_MONITOR_WANTED_LEVEL(TaxiStruct &myTaxiData/*, TAXI_OJ_DIALOGUE_Q_DATA &tTaxiDQ_data*/)
    
    IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits,TAXI_OJ_JB_DISABLE_WANTED)
        
        SWITCH myTaxiData.tWantedStateIndex
        
            CASE TWS_CHECK_IF_WANTED//--------------------------------------------------------------
        
                IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) >= 1
                    IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > TAXI_TWEAKS_TIME_DX_DELAY_POLICE
                        
                        //Preserve the wanted_level for the stats
                        SET_TAXI_OJ_WANTED_LEVEL(myTaxiData,GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()))
                        
                        //"Hey lose the cops"
                        IF myTaxiData.tTaxiOJ_RideState != TRS_DROPPING_OFF
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_WENT_WANTED,TRUE,TRUE)
                        ENDIF
						
                        TAXI_RESET_TIMERS(myTaxiData, TT_POLICE)
                        
                        //Hide old gps & blips
                        IF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)          
                            SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,0)
                            SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, FALSE)
                        ENDIF
                        
						myTaxiData.bIsCurrentlyWanted = TRUE
						
                        myTaxiData.tWantedStateIndex = TWS_PRINT_OBJ_TO_LOSE_POLICE
                        CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_PRINT_OBJ_TO_LOSE_POLICE")
                    ENDIF
                    
                ENDIF
            BREAK
            
            CASE TWS_PRINT_OBJ_TO_LOSE_POLICE//--------------------------------------------------------------
                
                IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > 4.0
                    
                    SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_POLICE,TRUE)   
                    myTaxiData.tWantedStateIndex = TWS_CONFIRM_OBJ_LOSE_POLICE_PRINTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CONFIRM_OBJ_LOSE_POLICE_PRINTED")
                ENDIF
            BREAK
            
            CASE TWS_CONFIRM_OBJ_LOSE_POLICE_PRINTED//--------------------------------------------------------------
                
                IF IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_POL")
                    IF myTaxiData.tTaxiOJ_RideState != TRS_DROPPING_OFF
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_ESCAPE_POLICE)
					ENDIF
                    myTaxiData.tWantedStateIndex = TWS_CHECK_IF_PLAYER_LOST_WANTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CHECK_IF_PLAYER_LOST_WANTED")
				ELIF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
					myTaxiData.tWantedStateIndex = TWS_CHECK_IF_PLAYER_LOST_WANTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CHECK_IF_PLAYER_LOST_WANTED - objective not printed")
                ENDIF
            BREAK
            
            
            CASE TWS_CHECK_IF_PLAYER_LOST_WANTED//--------------------------------------------------------------
                
                //If it's grown since last time make sure we update stats and the tracker
                IF IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(),GET_TAXI_OJ_WANTED_LEVEL(myTaxiData))
                    SET_TAXI_OJ_WANTED_LEVEL(myTaxiData,GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()))
                    TAXI_STATS_UPDATE(TAXI_STAT_WANTED_GAINED)
                ENDIF
                
                //Toggle escape police dialogue every x seconds
                IF NOT IS_TAXI_SPEECH_ENABLED(myTaxiData)// NOT myTaxiData.bTaxiOJ_CanSpeak
                    IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
                        IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE) > TAXI_OJ_CONST_TIME_BETWEEN_POLICE_REACTIONS
                            IF myTaxiData.tTaxiOJ_RideState != TRS_DROPPING_OFF
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_ESCAPE_POLICE,TRUE)
							ENDIF
                        ENDIF
                    ENDIF
                ENDIF
                
                //Wait til he's lost it
                IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
					
					IF IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_POL")
						CLEAR_PRINTS()
					ENDIF
					
                    //Show Blip & GPS again
                    IF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
						CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - putting the drop off blip back on")
						SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,255)
						SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, TRUE)
                    ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - blip does not exist, can't bring it back!")
					ENDIF
                    
					myTaxiData.bIsCurrentlyWanted = FALSE
					
                    myTaxiData.tWantedStateIndex = TWS_CONGRATULATE_PLAYER_ON_LOSING_WANTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CONGRATULATE_PLAYER_ON_LOSING_WANTED")
                ENDIF
                    
            BREAK
            
            CASE TWS_CONGRATULATE_PLAYER_ON_LOSING_WANTED
                IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                
                    //"Nice You Lost the Pigs "
                    IF myTaxiData.tTaxiOJ_RideState != TRS_DROPPING_OFF
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_POLICE_LOST,TRUE,FALSE,TRUE)
        			ENDIF
					
                    //Award Bonus
                    //TAXI_SET_BONUS(myTaxiData,TAXI_BONUS_LOST_POLICE, 100 )
                        
                    //Update stats
                    TAXI_STATS_UPDATE(TAXI_STAT_WANTED_LOST,GET_TAXI_OJ_WANTED_LEVEL(myTaxiData))
                        
                    //Set tracker back to zero
                    SET_TAXI_OJ_WANTED_LEVEL(myTaxiData,0)
                    
                    myTaxiData.tWantedStateIndex = TWS_CLEANUP
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_MONITOR_WANTED_LEVEL - Police lost")
                ENDIF
                
            BREAK
        
            CASE TWS_CLEANUP
                IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                    //OPEN_DIALOGUE_QUEUE(tTaxiDQ_data,-1,TDQ_ADD_DELAY_BEFORE_RESUME)
					
					TAXI_CANCEL_TIMERS(myTaxiData, TT_POLICE)
					TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
					
                    myTaxiData.tWantedStateIndex = TWS_CHECK_IF_WANTED
                ENDIF
            BREAK
        ENDSWITCH
    ENDIF
ENDPROC

PROC TAXI_OJ_MONITOR_STOPPED(TaxiStruct &myTaxiData, FLOAT fStopLong = CONST_TAXI_TIME_STOPPED_TOO_LONG, FLOAT fStopFail = CONST_TAXI_TIME_STOPPED_TOO_LONG_FAIL)
    
    IF IS_PASSENGER_IN_TAXI(myTaxiData)
	AND IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GREETING_DONE)
	
        IF (IS_VEHICLE_STOPPED(myTaxiData.viTaxi) OR GET_ENTITY_SPEED(myTaxiData.viTaxi) < 3.0)
        AND IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
            IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_STOPPED)
                TAXI_START_TIMER(myTaxiData, TT_STOPPED)
                CDEBUG1LN(DEBUG_OJ_TAXI,"Stopped Timer is at ", "", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_STOPPED))
            //Timer has been started let's see how long it's been going
            ELSE
                IF IS_PASSENGER_IN_TAXI(myTaxiData)
                    IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_STOPPED) > fStopLong
                    AND NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
						
						//Add internet comments here
						IF IS_TAXI_APP_INTERNET_RUNNING()
	                        SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_COMPLETE_STOP,TRUE)
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_COMPLETE_STOP,TRUE)
						ENDIF
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"Saying stopped whine.  Stopped Timer is at ", "", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_STOPPED))
						
                        TAXI_RESET_TIMERS(myTaxiData, TT_STOPPED)
                        IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_BORING)
                            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_MONITOR_STOPPED: restarting TT_BORING timer")
                            TAXI_RESET_TIMERS(myTaxiData, TT_BORING)    
                        ENDIF
                    ENDIF       
                    IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_FAILDELAY)
                        CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_MONITOR_STOPPED: restarting TT_FAILDELAY timer")
                        TAXI_RESET_TIMERS(myTaxiData, TT_FAILDELAY) 
                    ELIF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FAILDELAY) >= fStopFail
                        CDEBUG1LN(DEBUG_OJ_TAXI,"Failing for stopped.  TT_FAILDELAY Timer is at ", "", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FAILDELAY))
						TAXI_CANCEL_TIMERS(myTaxiData, TT_FAILDELAY)
                        TAXI_SET_FAIL(myTaxiData,"Car not moving", TFS_TAXI_STOPPED)
                    ENDIF
                ENDIF
            ENDIF
        //Player is on the move again
        ELSE
            IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_STOPPED)
                CDEBUG1LN(DEBUG_OJ_TAXI,"STOPPED Timer was at ", "", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_STOPPED))
                TAXI_CANCEL_TIMERS(myTaxiData, TT_STOPPED)
            ENDIF
            IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_FAILDELAY)
                CDEBUG1LN(DEBUG_OJ_TAXI,"FAILDELAY Timer was at ", "", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FAILDELAY))
                TAXI_CANCEL_TIMERS(myTaxiData, TT_FAILDELAY)
            ENDIF           
        ENDIF
    ENDIF
ENDPROC


FLOAT fTaxiDistanceCheckLast
INT iFartherCounter
INT iWrongWayTimer

PROC TAXI_OJ_MONITOR_WRONG_WAY(TaxiStruct &myTaxiData)
	IF IS_PASSENGER_IN_TAXI(myTaxiData)	
	AND IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_BANTER)
	AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
	
		// if it's farther twice in a row, play a line
		IF (GET_GAME_TIMER() - iWrongWayTimer) >= 10000
			FLOAT fTaxiDistanceCheck
			fTaxiDistanceCheck = GET_PLAYER_DISTANCE_FROM_LOCATION(myTaxiData.vTaxiOJDropoff)
			IF fTaxiDistanceCheck > fTaxiDistanceCheckLast
				CDEBUG1LN(DEBUG_OJ_TAXI,"player is getting farther from the destination")
				iFartherCounter += 1
			ELSE
				iFartherCounter = 0
				CDEBUG1LN(DEBUG_OJ_TAXI,"player is getting closer to the destination")
			ENDIF
			fTaxiDistanceCheckLast = fTaxiDistanceCheck
			iWrongWayTimer = GET_GAME_TIMER()
		ENDIF
		
		IF iFartherCounter >= 2
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			// SAY wrong speech
			SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_WRONG_WAY, TRUE, FALSE, TRUE)
			iFartherCounter = 0
			CDEBUG1LN(DEBUG_OJ_TAXI,"player has been getting farther for a while, say something about it")
		ENDIF
		
		IF (GET_GAME_TIMER() % 1000) < 50
			CDEBUG1LN(DEBUG_OJ_TAXI,"hit 21")
		ENDIF
	ELSE
		IF (GET_GAME_TIMER() % 4000) < 50
			IF NOT IS_PASSENGER_IN_TAXI(myTaxiData)	
				CDEBUG1LN(DEBUG_OJ_TAXI,"passenger not in taxi")
			ENDIF
			IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_BANTER)
				CDEBUG1LN(DEBUG_OJ_TAXI,"banter flag not set")
			ENDIF
		ENDIF
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF iFartherCounter > 0
				CDEBUG1LN(DEBUG_OJ_TAXI,"resetting farther count becaue wanted")
				iFartherCounter = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL TAXI_HANDLE_DRIVING(TaxiStruct &myTaxiData,/*TAXI_OJ_DIALOGUE_Q_DATA &tTaxiDQ_data, */FLOAT fStopRadius = TAXI_OJ_CONST_GATEWAY_SIZE_SMALL, 
								FLOAT fStopLong = CONST_TAXI_TIME_STOPPED_TOO_LONG, FLOAT fStopFail = CONST_TAXI_TIME_STOPPED_TOO_LONG_FAIL)
	
	
	
    IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN() AND NOT IS_SCREEN_FADING_OUT() AND NOT IS_SCREEN_FADED_OUT() AND NOT myTaxiData.bIsTaxiDebugSkipping
	    IF IS_TAXI_RIDE_ALL_READY(myTaxiData)
	        
			UPDATE_OJ_TAXI_MILEAGE(myTaxiData,myTaxiData.fTaxiOJ_TempFloat)
			
	        TAXI_OJ_MONITOR_WANTED_LEVEL(myTaxiData/*,tTaxiDQ_data*/)
	        
	        TAXI_OJ_MONITOR_PLAYER_HORN_HONK(myTaxiData)
	        
	        RUN_TAXIRADIO_UPDATE_WHEN_ENABLED(myTaxiData)
	    
	        TAXI_OJ_MONITOR_STOPPED(myTaxiData, fStopLong, fStopFail)
	        
			TAXI_OJ_MONITOR_WRONG_WAY(myTaxiData)
			
			#IF DEBUG_iShowDrivingFare
				IF (ROUND(GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST)) % 5) = 0 
				AND NOT bMileageDebugGate
					CDEBUG1LN(DEBUG_OJ_TAXI,"###########################")
					CDEBUG1LN(DEBUG_OJ_TAXI,"Time driving passenger    = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST))
					CDEBUG1LN(DEBUG_OJ_TAXI,"Current Mileage           = ", myTaxiData.fTaxiOJ_CurrentMileage)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Distance from Destination = ", GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff))
					CDEBUG1LN(DEBUG_OJ_TAXI,"###########################")
					bMileageDebugGate = TRUE
				ENDIF
				
				IF (ROUND(GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST)) % 5) = 2 
				AND bMileageDebugGate
					bMileageDebugGate = FALSE
				ENDIF
			#ENDIF
			
			RETURN HAS_TAXI_OJ_REACHED_DROPOFF(myTaxiData, fStopRadius)
	        
	    ENDIF
    ENDIF
    RETURN FALSE

ENDFUNC

FUNC BOOL TAXI_HANDLE_EXCITEMENT_DRIVING_DEUX(TaxiStruct &myTaxiData/*,TAXI_OJ_DIALOGUE_Q_DATA &tTaxiDQ_data*/, FLOAT fStopRadius = TAXI_OJ_CONST_GATEWAY_SIZE_SMALL, FLOAT fStopLong = CONST_TAXI_TIME_STOPPED_TOO_LONG)
    IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN() AND NOT IS_SCREEN_FADING_OUT() AND NOT IS_SCREEN_FADED_OUT() AND NOT myTaxiData.bIsTaxiDebugSkipping
	    IF IS_TAXI_RIDE_ALL_READY(myTaxiData)
	        
			UPDATE_OJ_TAXI_MILEAGE(myTaxiData,myTaxiData.fTaxiOJ_TempFloat)

	        RUN_TAXIRADIO_UPDATE_WHEN_ENABLED(myTaxiData)
	        
			TAXI_OJ_MONITOR_WANTED_LEVEL(myTaxiData/*,tTaxiDQ_data*/)
			
			TAXI_OJ_MONITOR_STOPPED(myTaxiData, fStopLong)
			
	        TAXI_OJ_MONITOR_PLAYER_HORN_HONK(myTaxiData)
			
			TAXI_OJ_MONITOR_WRONG_WAY(myTaxiData)
			
			IF (ROUND(GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST)) % 5) = 0 
			AND NOT bMileageDebugGate
				CDEBUG1LN(DEBUG_OJ_TAXI,"###########################")
				CDEBUG1LN(DEBUG_OJ_TAXI,"Time driving passenger    = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST))
				CDEBUG1LN(DEBUG_OJ_TAXI,"Current Mileage           = ", myTaxiData.fTaxiOJ_CurrentMileage)
				CDEBUG1LN(DEBUG_OJ_TAXI,"Distance from Destination = ", GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff))
				CDEBUG1LN(DEBUG_OJ_TAXI,"###########################")
				bMileageDebugGate = TRUE
			ENDIF
			
			IF (ROUND(GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST)) % 5) = 2 
			AND bMileageDebugGate
				bMileageDebugGate = FALSE
			ENDIF
			
	        RETURN HAS_TAXI_OJ_REACHED_DROPOFF(myTaxiData, fStopRadius)
	        
	    ENDIF
    ENDIF
    RETURN FALSE

ENDFUNC

FUNC BOOL TAXI_HAS_PASSENGER_ESCAPED(PED_INDEX Player, PED_INDEX Passenger)
    IF NOT IS_PED_INJURED(Passenger)
        IF NOT IS_ENTITY_AT_ENTITY(Player, Passenger, <<100.0, 100.0, 50.0>>)
            IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(Passenger), 15.0)
                CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_HAS_PASSENGER_ESCAPED = TRUE")
                RETURN TRUE
            ENDIF
        ENDIF
    ENDIF
    //CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_HAS_PASSENGER_ESCAPED = FALSE")
    RETURN FALSE
ENDFUNC 

/// PURPOSE: This function handles all the initialization for a money drop.
///    
/// PARAMS:
///    ped - The ped whose dropping the money
///    money - the money struct
///    bBlipOn - whether or not to blip the money pickup
PROC TAXI_SETUP_MONEY_DROP_ON_PED(PED_INDEX ped, TAXI_MONEY_STRUCT &money, BOOL bBlipOn = FALSE, INT iDollarAmountDropped = -1)
    
    //Init the base values
    money.iMoneyPlacementFlags = 0
    
    //Set the amount of money and where to drop it
    IF NOT IS_PED_INJURED(ped)
        
//        IF iDollarAmountDropped <= 0
//            money.iTaxiMoneyAmount = GET_PED_MONEY(ped)
//			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_PED_MONEY(ped) = ", money.iTaxiMoneyAmount)
//        ELSE
//            money.iTaxiMoneyAmount = iDollarAmountDropped
//			CDEBUG1LN(DEBUG_OJ_TAXI,"money.iTaxiMoneyAmount = ", money.iTaxiMoneyAmount)
//        ENDIF
        money.vTaxiMoneyPos = GET_ENTITY_COORDS(ped)
        
        SET_PED_MONEY(ped, 0)
        
		CDEBUG1LN(DEBUG_OJ_TAXI,"setting money.iTaxiMoneyAmount to 0 to match the fare")
		money.iTaxiMoneyAmount = 0
        CDEBUG1LN(DEBUG_OJ_TAXI,"Set up money - for not injured ped")
    //In case he's dead, drop money anyway
    ELSE
        
        IF iDollarAmountDropped <= 0
            money.iTaxiMoneyAmount = GET_RANDOM_INT_IN_RANGE(80,200)
        ELSE
            money.iTaxiMoneyAmount = iDollarAmountDropped
        ENDIF
        
        money.vTaxiMoneyPos = GET_DEAD_PED_PICKUP_COORDS(ped)
        
        CDEBUG1LN(DEBUG_OJ_TAXI,"Set up money - for INJURED ped")

    ENDIF
    
    //Set these self-explanatory BIT FLAGS on
    SET_BIT(money.iMoneyPlacementFlags,ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
    SET_BIT(money.iMoneyPlacementFlags,ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
    
	VECTOR vSafePickup
	vSafePickup = GET_SAFE_PICKUP_COORDS(money.vTaxiMoneyPos)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"money pos = ", money.vTaxiMoneyPos)
	CDEBUG1LN(DEBUG_OJ_TAXI,"SAFE pos  = ", vSafePickup)
	
    //Finally...Create the $$$$ pickup
    money.pickupMoney = CREATE_PICKUP(PICKUP_MONEY_VARIABLE, vSafePickup, money.iMoneyPlacementFlags, money.iTaxiMoneyAmount)
    
    //Do we want to blip or not?
    IF bBlipOn
		IF NOT DOES_BLIP_EXIST(money.blipMoney)
       		money.blipMoney = CREATE_BLIP_FOR_PICKUP(money.pickupMoney)
			SET_BLIP_NAME_FROM_TEXT_FILE(money.blipMoney, "TAXI_BLIP_MONE")
		ELSE
			SET_BLIP_NAME_FROM_TEXT_FILE(money.blipMoney, "TAXI_BLIP_MONE")
		ENDIF
       
    ENDIF
            
ENDPROC



/// PURPOSE: STATE MACHINE FOR Passenger bailing on fare or fleeing from any vehicle with blip 
///    
/// PARAMS:
///    runState - ped run data
///    myTaxiData - global taxi sata
/// RETURNS:
///    
FUNC BOOL TAXI_SET_PED_RUNNING(TAXI_PED_RUN_STATE &runState, TaxiStruct &myTaxiData,  PED_INDEX &ped, TAXI_MONEY_STRUCT &money,BOOL bMoneyOwed = FALSE)
    
    SWITCH runState
        CASE TPRS_INIT
            IF NOT IS_ENTITY_DEAD(ped)
                TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER(myTaxiData,ECF_DONT_CLOSE_DOOR | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP,bMoneyOwed)
                
                SET_PED_MONEY(ped,GET_RANDOM_INT_IN_RANGE(100,300))
                
                //Add Blip
                TAXI_OJ_UPDATE_BLIP_PED(myTaxiData,ped,"TAXI_BLIP_PASS",TRUE)
                
				//Update Stats
                TAXI_STATS_UPDATE(TAXI_STAT_PASS_RUN)
                
                runState = TPRS_GETTING_OUT
                CDEBUG1LN(DEBUG_OJ_TAXI,"runState = TPRS_GETTING_OUT")
            ELSE
                runState = TPRS_CLEANUP
                CDEBUG1LN(DEBUG_OJ_TAXI,"runState = TPRS_INIT - TPRS_WAIT_FOR_MONEY_PICKUP")
            ENDIF
        BREAK
        
        //Ped has left vehicle - Return player control----------------------
        CASE TPRS_GETTING_OUT
            IF NOT IS_ENTITY_DEAD(ped)
                IF NOT IS_PED_IN_ANY_VEHICLE(ped)
            
                    SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
                    
                    runState = TPRS_RUNNING
                    CDEBUG1LN(DEBUG_OJ_TAXI,"runState = TPRS_RUNNING")
                ENDIF
            //Ped was killed
            ELSE
                runState = TPRS_RUNNING
                CDEBUG1LN(DEBUG_OJ_TAXI,"runState = TPRS_RUNNING")
            ENDIF
        BREAK
        
        //Ped is on the run - check if the player ever injures him physically on in car-------
        CASE TPRS_RUNNING
            IF NOT IS_ENTITY_DEAD(ped)
                CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, ped)
				//CONTROL_PED_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, ped)
				
                IF NOT TAXI_HAS_PASSENGER_ESCAPED(myTaxiData.piTaxiPlayer, ped)
                    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
                        IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped, myTaxiData.viTaxi)
                            runState = TPRS_TOSS_MONEY
                        ENDIF
                    ENDIF
                    IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped, myTaxiData.piTaxiPlayer)
                        runState = TPRS_TOSS_MONEY
                    ENDIF
                    
                    IF runState <> TPRS_TOSS_MONEY
                        //Not in cab
                        IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiData.piTaxiPlayer)
                            //Keep within 20 units
                            IF IS_ENTITY_AT_ENTITY(myTaxiData.piTaxiPlayer, ped, <<20.0, 20.0, 10.0>>)
                                //If I shoot
                                IF IS_PED_SHOOTING(myTaxiData.piTaxiPlayer)
                                    runState = TPRS_TOSS_MONEY
                                ELSE
                                    IF IS_ENTITY_AT_ENTITY(myTaxiData.piTaxiPlayer, ped, <<2.0, 2.0, 10.0>>)
										runState = TPRS_TOSS_MONEY
									ENDIF
									
									//If I'm aiming at him and he can see me
                                    IF IS_PED_ARMED(myTaxiData.piTaxiPlayer, WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
                                        IF IS_AIM_CAM_ACTIVE()
                                        //    IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(myTaxiData.piTaxiPlayer, ped)
                                        //    AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(ped,myTaxiData.piTaxiPlayer)
                                                runState = TPRS_TOSS_MONEY
                                        //    ENDIF
                                        ENDIF
                                    ENDIF
                                ENDIF
                            //If I'm shooting and he can see me within 50 units
                            ELSE
                                IF IS_ENTITY_AT_ENTITY(myTaxiData.piTaxiPlayer, ped, <<50.0, 50.0, 10.0>>)
                                    IF IS_PED_SHOOTING(myTaxiData.piTaxiPlayer)
                                    //    IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(ped, myTaxiData.piTaxiPlayer)
                                            runState = TPRS_TOSS_MONEY
                                    //    ENDIF
                                    ENDIF
                                ENDIF
                            ENDIF
                        //Outside of cab
                        ELSE
                            IF IS_ENTITY_AT_ENTITY(myTaxiData.piTaxiPlayer, ped, <<50.0, 50.0, 25.0>>)
                                IF IS_PED_SHOOTING(myTaxiData.piTaxiPlayer)
                                    runState = TPRS_TOSS_MONEY
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                ELSE
                    KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					//Set Bonus
                    //TAXI_SET_BONUS(myTaxiData, TAXI_BONUS_LOST_PAY)
                    myTaxiData.bTaxiOJ_Failed = TRUE
                    runState = TPRS_CLEANUP
                    CDEBUG1LN(DEBUG_OJ_TAXI,"runState = TPRS_CLEANUP")
                ENDIF
            ELSE
                KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				runState = TPRS_CLEANUP
                CDEBUG1LN(DEBUG_OJ_TAXI,"runState = TPRS_CLEANUP")
            ENDIF
        BREAK
        
        //Ped got got make him drop money----------------------------------------
        CASE TPRS_TOSS_MONEY
            KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			
			IF NOT IS_ENTITY_DEAD(ped)
                
                IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
                    REMOVE_BLIP(myTaxiData.blipTaxiPassenger)
                ENDIF
                
                SEQUENCE_INDEX seqID
				
                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
                
                OPEN_SEQUENCE_TASK(seqID)
                    TASK_COWER(NULL, 5000)
                    TASK_SMART_FLEE_PED(NULL, myTaxiData.piTaxiPlayer, 1000.0, -1)
                    TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
                CLOSE_SEQUENCE_TASK(seqID)
                TASK_PERFORM_SEQUENCE(ped, seqID)
                CLEAR_SEQUENCE_TASK(seqID)

                SET_PED_KEEP_TASK(ped, TRUE)
                
                SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_MAKE_PAY,TRUE)
                
                runState = TPRS_WAIT_FOR_MONEY_PICKUP
                CLEAR_PRINTS()
                CDEBUG1LN(DEBUG_OJ_TAXI,"runState = TPRS_WAIT_FOR_MONEY_PICKUP")
            ELSE
                runState = TPRS_WAIT_FOR_MONEY_PICKUP
                CLEAR_PRINTS()
                CDEBUG1LN(DEBUG_OJ_TAXI,"runState =TPRS_TOSS_MONEY - INJURED TPRS_WAIT_FOR_MONEY_PICKUP")
            ENDIF
            
            IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
                REMOVE_BLIP(myTaxiData.blipTaxiPassenger)
            ENDIF
                
            //Set Money Properties  
            TAXI_SETUP_MONEY_DROP_ON_PED(ped,money,TRUE)
            
        BREAK
        
        //Blip And tell to go get money and then wait
        CASE TPRS_WAIT_FOR_MONEY_PICKUP
            IF HAS_PICKUP_BEEN_COLLECTED(money.pickupMoney) 
                IF DOES_BLIP_EXIST(money.blipMoney)
                    REMOVE_BLIP(money.blipMoney)
                ENDIF
                
                //Fare = TO_FLOAT(money.iTaxiMoneyAmount/TAXI_BASE_FARE))
                SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)

				//Set Bonus
                //TAXI_SET_BONUS(myTaxiData, TAXI_BONUS_MADE_PAY, money.iTaxiMoneyAmount)
                
                TAXI_STATS_UPDATE(TAXI_STAT_PASS_PAY)
				
				myTaxiData.iTaxiOJ_CashTip = 0
                
                runState = TPRS_CLEANUP
                CDEBUG1LN(DEBUG_OJ_TAXI,"runState = TPRS_WAIT_FOR_MONEY_PICKUP ->TPRS_CLEANUP")
			ELSE
				//CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, money.vTaxiMoneyPos )
            ENDIF
            
        BREAK

        CASE TPRS_CLEANUP           
            
            IF NOT IS_ENTITY_DEAD(ped)
                SET_PED_AS_NO_LONGER_NEEDED(ped)
                IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
                    REMOVE_BLIP(myTaxiData.blipTaxiPassenger)
                ENDIF
                
            ELSE
                SET_PLAYER_WANTED_LEVEL_NO_DROP(GET_PLAYER_INDEX(), 2)
                SET_PLAYER_WANTED_LEVEL_NOW(GET_PLAYER_INDEX())
            ENDIF
        
            RETURN TRUE
        BREAK
        
    ENDSWITCH
    RETURN FALSE
ENDFUNC

 //See which door is closer to the dropoff point
PROC LEAVE_TAXI_OJ_FROM_CLOSEST_DOOR_TO_POINT(TaxiStruct &myTaxiData,VECTOR vPtToCompare)
    
	VECTOR vTempTaxiDoorPosLeft
    VECTOR vTempTaxiDoorPosRight
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
        IF Not IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
		AND IS_PED_SITTING_IN_VEHICLE(myTaxiData.piTaxiPassenger, myTaxiData.viTaxi)
			vTempTaxiDoorPosLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << -1.78774, -1.62399, -0.6206 >>)
		    vTempTaxiDoorPosRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << 1.78498, -1.24105, -0.6422>>)
		 	
			//check if they're in front seat first
			IF (GET_PED_VEHICLE_SEAT(myTaxiData.piTaxiPassenger, myTaxiData.viTaxi) = VS_FRONT_RIGHT)
				TASK_LEAVE_VEHICLE(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,ECF_WARP_IF_DOOR_IS_BLOCKED)
		        CDEBUG1LN(DEBUG_OJ_TAXI,"Passenger is coming out of the front seat, even if he has to warp, since that's the only way out")
				
			//Left Door is closer and not blocked
		    ELIF GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vPtToCompare,FALSE) < GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vPtToCompare,FALSE)
		    AND IS_ENTRY_POINT_FOR_SEAT_CLEAR(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,VS_BACK_LEFT)

		        TASK_LEAVE_VEHICLE(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,ECF_USE_LEFT_ENTRY)
		        CDEBUG1LN(DEBUG_OJ_TAXI,"Passenger is exiting out of LEFT DOOR because it's closer ", GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vPtToCompare,FALSE), " than the right side door which is: ", GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vPtToCompare,FALSE))
		    
		    //Right door is closer and not blocked
		    ELIF GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vPtToCompare,FALSE) >= GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vPtToCompare,FALSE)
		    AND IS_ENTRY_POINT_FOR_SEAT_CLEAR(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,VS_BACK_RIGHT)

		        TASK_LEAVE_VEHICLE(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,ECF_USE_RIGHT_ENTRY)
		        CDEBUG1LN(DEBUG_OJ_TAXI,"Passenger is exiting out of RIGHT DOOR because it's closer than ", GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vPtToCompare,FALSE) , " where as this distance is: ",  GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vPtToCompare,FALSE))

		    //All doors are shitty, just get out by any means.
		    ELSE
		        TASK_LEAVE_VEHICLE(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi)
		        CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXIT_CLOSEST_TO_STREET is EXITING Passenger out of ANY DOOR")   
		    
		    ENDIF
		ENDIF
	ENDIF
ENDPROC

 //See which door is closer to the dropoff point for use in a task sequence
PROC LEAVE_TAXI_OJ_FROM_CLOSEST_DOOR_TO_POINT_AS_SEQUENCE(TaxiStruct &myTaxiData,VECTOR vPtToCompare)
    
	VECTOR vTempTaxiDoorPosLeft
    VECTOR vTempTaxiDoorPosRight
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
        IF Not IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
		
			vTempTaxiDoorPosLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << -1.78774, -1.62399, -0.6206 >>)
		    vTempTaxiDoorPosRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << 1.78498, -1.24105, -0.6422>>)
		 	
			//Left Door is closer and not blocked
		    IF GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vPtToCompare,FALSE) < GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vPtToCompare,FALSE)
		    AND IS_ENTRY_POINT_FOR_SEAT_CLEAR(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,VS_BACK_LEFT)

		        TASK_LEAVE_VEHICLE(NULL,myTaxiData.viTaxi,ECF_USE_LEFT_ENTRY|ECF_WARP_IF_DOOR_IS_BLOCKED)
		        CDEBUG1LN(DEBUG_OJ_TAXI,"Passenger is exiting out of LEFT DOOR because it's closer ","", GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vPtToCompare,FALSE), "" , " than the right side door which is: ","",  GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vPtToCompare,FALSE))
		    
		    //Right door is closer and not blocked
		    ELIF GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vPtToCompare,FALSE) >= GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vPtToCompare,FALSE)
		    AND IS_ENTRY_POINT_FOR_SEAT_CLEAR(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,VS_BACK_RIGHT)

		        TASK_LEAVE_VEHICLE(NULL,myTaxiData.viTaxi,ECF_USE_RIGHT_ENTRY|ECF_WARP_IF_DOOR_IS_BLOCKED)
		        CDEBUG1LN(DEBUG_OJ_TAXI,"Passenger is exiting out of RIGHT DOOR because it's closer than ","", GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vPtToCompare,FALSE), "" , " where as this distance is: ","",  GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vPtToCompare,FALSE))

		    //All doors are shitty, just get out by any means.
		    ELSE
		        TASK_LEAVE_VEHICLE(NULL,myTaxiData.viTaxi, ECF_WARP_IF_DOOR_IS_BLOCKED)
		        CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXIT_CLOSEST_TO_STREET is EXITING Passenger out of ANY DOOR")   
		    
		    ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TAXI_OJ_EXIT_CLOSEST_TO_STREET(TaxiStruct &myTaxiData)

    VECTOR vPtToCompare
   
    //in the off chance that a mission doesn't have this defined, default to the dropoff vector
    IF IS_VECTOR_ZERO(myTaxiData.vTaxiOJ_PassengerGoToPt)
        vPtToCompare = myTaxiData.vTaxiOJDropoff
    ELSE
        vPtToCompare = myTaxiData.vTaxiOJ_PassengerGoToPt
    ENDIF
    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXIT_CLOSEST_TO_STREET is using this point to test on exit:","",vPtToCompare) 
    
    //See which door is closer to the dropoff point
    LEAVE_TAXI_OJ_FROM_CLOSEST_DOOR_TO_POINT(myTaxiData,vPtToCompare)       
       
ENDPROC

/// PURPOSE: Handles appropriately tasking the passenger to exit the taxi on the side closest to the sidewalk
///    
/// PARAMS:
///    myTaxiData - global taxi data
/// RETURNS: TRUE when the passenger has been tasked to LEAVE_ANY_VEHICLE
//    
FUNC BOOL HAS_TAXI_OJ_PASSENGER_BEEN_DROPPED_OFF(TaxiStruct &myTaxiData,BOOL bWaitToFinishSpeaking = TRUE)
    IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
		//Wait til passenger is done speaking/paying than tell him to leave vehicle
	    IF IS_PASSENGER_IN_TAXI(myTaxiData)
	    AND IS_TAXI_FULLY_STOPPED(myTaxiData)
	        IF bWaitToFinishSpeaking
				IF IS_TAXI_DIALOGUE_DONE(myTaxiData,TAXI_DXF_THANKS)
					TAXI_OJ_EXIT_CLOSEST_TO_STREET(myTaxiData)
				ENDIF
			ELSE
				TAXI_OJ_EXIT_CLOSEST_TO_STREET(myTaxiData)
	        ENDIF
			
	            
	    //Wait til passenger leaves taxi and is done speaking to move onto next stage
	    ELIF NOT IS_PASSENGER_IN_TAXI(myTaxiData)
	   		IF bWaitToFinishSpeaking
				IF IS_TAXI_DIALOGUE_DONE(myTaxiData,TAXI_DXF_THANKS)
	        
			        //IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_LEAVE_ANY_VEHICLE) = FINISHED_TASK    
			        IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_LEAVE_VEHICLE) = FINISHED_TASK
			            RETURN TRUE
			        ENDIF
				ENDIF
			ELSE
				//IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_LEAVE_ANY_VEHICLE) = FINISHED_TASK    
		        IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_LEAVE_VEHICLE) = FINISHED_TASK
		            RETURN TRUE
		        ENDIF
			ENDIF
	    ENDIF       
	ENDIF
    RETURN FALSE
ENDFUNC

/// PURPOSE: Call this to clean up the passengers tasks etc.
///    
/// PARAMS:
///    myTaxiData - Our global all u can eat taxi data 
/// RETURNS: TRUE when the fail is done being executed
///    
FUNC BOOL TAXI_HANDLE_FAIL(TaxiStruct &myTaxiData, BOOL bKeepTask = FALSE)
    IF bKeepTask
        RETURN TRUE
    ENDIF
    
    IF (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FAILDELAY) > 1.0)
    
	//BYPASSES...
	OR myTaxiData.tTaxiOJ_FailType = TFS_TAXI_GARBAGE_COLLECTED
	OR myTaxiData.tTaxiOJ_FailType = TFS_PASSENGER_SHOT
	
	OR myTaxiData.tTaxiOJ_FailType = TFS_PASSENGER_DIED
	//OR myTaxiData.tTaxiOJ_FailType = TFS_UNDER_WATER
	
        //KILL_FACE_TO_FACE_CONVERSATION()
        IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)      
            IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
			
            	//IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_PASSENGER_ACTION)
	        
					IF IS_PED_IN_VEHICLE(myTaxiData.piTaxiPassenger, myTaxiData.viTaxi)        
	                   
						IF GET_TAXI_MISSION_TYPE(myTaxiData) = TXM_01_NEEDEXCITEMENT
	                    OR IS_BITMASK_SET(myTaxiData.iTaxiOJ_DXBitsPolice,TAXI_OJ_FAIL_BIT_POLICE)
	                		
							IF NOT IS_VEHICLE_STOPPED(myTaxiData.viTaxi)
								TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER(myTaxiData, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_JUMP_OUT)
							ELSE
								TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER(myTaxiData)
							ENDIF
							
							//TAXI_RESET_TIMERS(myTaxiData, TT_PASSENGER_ACTION)
							RETURN TRUE
	                    ELSE
							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER Called - MISSION IS NOT NEX")


	                        TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER(myTaxiData)
							
							//TAXI_RESET_TIMERS(myTaxiData, TT_PASSENGER_ACTION)
							RETURN TRUE
	                    ENDIF
	                
					ELSE                      

                        TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER(myTaxiData)						
						//TAXI_RESET_TIMERS(myTaxiData, TT_PASSENGER_ACTION)
						RETURN TRUE
	                ENDIF
			//	ELSE
			//		IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_PASSENGER_ACTION) > 0.5
			//			RETURN TRUE
			//		ELSE
			//			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_HANDLE_FAIL: GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_PASSENGER_ACTION) = ",GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_PASSENGER_ACTION))
			//		ENDIF
				
			//	ENDIF
			
            ELSE
                TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER(myTaxiData)
				CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_TASK_PASSENGER_FLEE_PLAYER Called - TAXI NOT DRIVEABLE")
                RETURN TRUE
            ENDIF
        
        ELSE
            RETURN TRUE
        ENDIF   
    ELSE    
        IF (GET_GAME_TIMER() % 1000) < 50
			CDEBUG1LN(DEBUG_OJ_TAXI,"Waiting for HANDLE FAIL TO BE READY")
		ENDIF
    ENDIF

	RETURN FALSE
ENDFUNC

PROC TAXI_HANDLE_PLAYER_DIVE(TaxiStruct & myTaxiData)
    //reset cam
    CLEANUP_TAXI_QUICK_CAM(myTaxiData, TAXI_CAM_PICKUP_INTERP_OUT)
    
    // cleaer the passengers 'get in cab' task
    IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	
        CLEAR_PED_TASKS_IMMEDIATELY(myTaxiData.piTaxiPassenger)
		
		TASK_TURN_PED_TO_FACE_ENTITY(myTaxiData.piTaxiPassenger, PLAYER_PED_ID())
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"Tasked passenger to face the player")
    ENDIF
    
    KILL_ANY_CONVERSATION()
    
    // cancel the taxi not_in_cab timer so that it can be reset
    CDEBUG1LN(DEBUG_OJ_TAXI,"SET_PLAYER_REENTERED_TAXI_OJ called in 4721")
	SET_PLAYER_REENTERED_TAXI_OJ(myTaxiData)
ENDPROC

FUNC BOOL HAS_TAXI_DAMAGED_ENTITY(TaxiStruct &myTaxiData,ENTITY_INDEX eEntityToCheckForDamage,BOOL bCheckPlayerPedAlso = TRUE)
    
    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
        IF DOES_ENTITY_EXIST(eEntityToCheckForDamage)
            IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(eEntityToCheckForDamage, myTaxiData.viTaxi)
                RETURN TRUE
            ENDIF
        ENDIF
    ENDIF
    
    IF bCheckPlayerPedAlso
        IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
            IF DOES_ENTITY_EXIST(eEntityToCheckForDamage)
                IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(eEntityToCheckForDamage, PLAYER_PED_ID())
                    RETURN TRUE
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_TAXI_JOB_IN_FAIL_STATE(TaxiStruct &myTaxiData)
    IF myTaxiData.bTaxiOJ_Failed
    AND myTaxiData.tTaxiOJ_RideState < TRS_SCORECARD_GRADE
	//AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC
/// PURPOSE: in the random case that the player ref is lost, this func reassigns everything properly
///    
/// PARAMS:
///    myTaxiData - global taxi data
PROC REASSIGN_TAXI_OJ_DRIVER(TaxiStruct &myTaxiData)
    
    myTaxiData.piTaxiPlayer = PLAYER_PED_ID()
    TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo)

	//Track current mileage to zero when in new taxi
	myTaxiData.sDrivingStat = GET_TAXI_DRIVING_STAT_FOR_PLAYER()

    CDEBUG1LN(DEBUG_OJ_TAXI,"REASSIGN_TX_OJ_DRIVER- PLayer had to get regrabbed")
ENDPROC
#IF IS_DEBUG_BUILD
PROC TAXI_ODDJOB_DEBUG_SKIP_TO_SCORECARD(TaxiStruct &myTaxiData)
	
	SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)
	
	TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)		
	
ENDPROC
#ENDIF

FUNC BOOL IS_PED_PERFORMING_TASK(PED_INDEX &ped, SCRIPT_TASK_NAME sTask)
	IF NOT IS_ENTITY_DEAD(ped)
		IF GET_SCRIPT_TASK_STATUS(ped, sTask) = PERFORMING_TASK
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC TAXI_DEBUG_FAIL_TRIGGERED(TaxiStruct &myTaxiData)
	
	myTaxiData.sTaxiOJ_Reason4Fail = "Debug Fail Key Pressed"

	IF IS_PASSENGER_IN_TAXI(myTaxiData)
		IF IS_VEHICLE_STOPPED(myTaxiData.viTaxi)
			TASK_LEAVE_ANY_VEHICLE(myTaxiData.piTaxiPassenger,0,ECF_DONT_CLOSE_DOOR )
		ELSE
			TASK_LEAVE_ANY_VEHICLE(myTaxiData.piTaxiPassenger,0,ECF_JUMP_OUT)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DEBUG_FAIL_TRIGGERED- Passenger Dove Out!!!!!!!!!!!!!!!!")
		ENDIF
	ENDIF

	
ENDPROC
//EOF






