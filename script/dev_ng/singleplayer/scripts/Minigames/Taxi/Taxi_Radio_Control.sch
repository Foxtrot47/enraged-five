//Taxi_Radio_Control.sch
//Author: John R. Diaz
//Handles radio control when a passenger enters the taxi and either likes or dislikes the current radio station.

ENUM TAXI_RADIO_STATIONS_INDEX
	TRS_0_CLASS_ROCK = 0,
	TRS_1_POP,
	TRS_2_HIPHOP_NEW,
	TRS_3_PUNK,
	TRS_4_TALK_01,
	TRS_5_COUNTRY,
	TRS_6_DANCE_01,
	TRS_7_MEXICAN,
	TRS_8_HIPHOP_OLD,
	TRS_9_SURF,
	TRS_10_TALK_02,
	TRS_11_REGGAE,
	TRS_12_JAZZ,
	TRS_13_DANCE_02,
	TRS_14_MOTOWN,
	TRS_15_SILVERLAKE,
	TRS_NUM_STATIONS
ENDENUM

ENUM TAXI_RADIO_CATEGORY
	TRC_RAP = 1,
	TRC_ROCK,
	TRC_TALK,
	TRC_DANCE,
	TRC_REGGAE,
	TRC_SOUL,
	TRC_MEX
ENDENUM

/* 
RADIO_01_CLASS_ROCK
RADIO_02_POP
RADIO_03_HIPHOP_NEW
RADIO_04_PUNK
RADIO_05_TALK_01
RADIO_06_COUNTRY
RADIO_07_DANCE_01
RADIO_08_MEXICAN
RADIO_09_HIPHOP_OLD
RADIO_10_SURF
RADIO_11_TALK_02
RADIO_12_REGGAE
RADIO_13_JAZZ
RADIO_14_DANCE_02
RADIO_15_MOTOWN
RADIO_16_SILVERLAKE
*/

STRUCT TAXI_RADIO_STRUCT
	
	INT iCurrStation	 	= -1		//Stores the taxi's current radio station
	INT iDislikedStation 	= -1		//Stores the passenger's least favorite station
	INT iLikedStation 		= -1		//Stores the passenger's favorite station
	INT iCategory 			= -1		//Stores what hint to use for the procedural taxi
	INT iSwitch 			= 0			//Used to control states
	BOOL bHitLeastFav		= FALSE		//Whether the player has hit the least favorite station already
	BOOL bHitFav			= FALSE
	BOOL bReactNeutral		= FALSE
	BOOL bGiveHint			= FALSE
ENDSTRUCT

//EOF
