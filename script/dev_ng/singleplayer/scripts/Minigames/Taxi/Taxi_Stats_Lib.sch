//Taxi_Stats_Lib.sch
//Author: John R. Diaz
//Purpose - Store all taxi stats related enums, procs, and functions in this lib for easy tweaking

USING "Taxi_Stats.sch"
USING "minigames_helpers.sch"

CONST_FLOAT		TAXI_REWARD_MIN			10.0
CONST_FLOAT		TAXI_REWARD_MAX			40.0

//SCREEN POSITIONS
CONST_FLOAT TAXI_UI_SM_HEAD_SCALE		1.0				// UI choose race heading scale (x/y).
CONST_FLOAT TAXI_UI_SM_HEAD_POS_X		0.4				// UI choose race heading pos (x).
CONST_FLOAT TAXI_UI_SM_HEAD_POS_Y		0.15			// UI choose race heading pos (y).
CONST_FLOAT TAXI_UI_SM_INFO_POS_X		0.325			// UI choose race info pos (x).
CONST_FLOAT TAXI_UI_SM_STAT_POS_X		0.6			// UI choose race info pos (x).
CONST_FLOAT TAXI_UI_SM_CURRPAGE_POS_X	0.42			// UI choose race info pos (x).
CONST_FLOAT TAXI_UI_SM_NUMPAGE_POS_X	0.44			// UI choose race info pos (x).

CONST_FLOAT TAXI_UI_SM_INFO_SCALE		0.4				// UI choose race info scale (x/y).
CONST_FLOAT TAXI_UI_SM_INFO_POS_Y		0.025			// UI choose race info pos (y).

CONST_FLOAT TAXI_UI_SM_SEL_POS_X		0.32			// UI choose race select pos (x).
CONST_FLOAT TAXI_UI_SM_SEL_POS_Y		0.81			// UI choose race select pos (y).

CONST_FLOAT TAXI_UI_SM_BUT_SCALE		0.625			// UI choose race select scale (x/y).

CONST_FLOAT TAXI_UI_SM_EXIT_POS_X		0.614			// UI choose race exit pos (x).
CONST_FLOAT TAXI_UI_SM_EXIT_POS_Y		0.81			// UI choose race exit pos (y).

CONST_FLOAT TAXI_UI_SM_RECT_POS_X		0.5				// UI choose race rectangle pos (x).
CONST_FLOAT TAXI_UI_SM_RECT_POS_Y		0.5				// UI choose race rectangle pos (y).
CONST_FLOAT TAXI_UI_SM_RECT_WIDTH		0.375			// UI choose race rectange width.
CONST_FLOAT TAXI_UI_SM_RECT_HEIGHT		0.750			// UI choose race rectangle height.
CONST_INT 	TAXI_UI_SM_RECT_ALPHA		191				// UI choose race rectange alpha.

CONST_INT STATS_NUM_PER_PAGE	20
CONST_INT STATS_NUM_PAGES		2

PROC TAXI_STATS_DEBUG(String sStatus, INT iNum)
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_STATS_DEBUG - Status = ","", sStatus, "", " INT ", "",iNum )
ENDPROC
/// PURPOSE: Wipe the whole tree of stats
///    
PROC TAXI_CLEAR_ALL_STATS()
	INT i

	REPEAT  TAXI_STATS_NUM_STATS i
		g_savedGlobals.sTaxiData.iTaxiStats[i] = 0
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CLEAR_ALL_STATS - Stats Cleared")
ENDPROC

PROC  TAXI_STATS_UPDATE(TAXI_STATS_INFO whichStat, INT iNewStatValue = 0)
	
	SWITCH whichStat


		CASE TAXI_STAT_FARES_COMPLETED
			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_COMPLETED]++
			TAXI_STATS_DEBUG("Fares Completed ++ = ",g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_COMPLETED])
		BREAK

		CASE TAXI_STAT_FARES_FAILED		
		
			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_FAILED]++
			TAXI_STATS_DEBUG("Fares Failed ++ = ",g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_FAILED])
		BREAK
		
		CASE TAXI_STAT_FARES_ACCEPTED	
		
			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_ACCEPTED]++
			TAXI_STATS_DEBUG("Fares Accepted ++ ",g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_ACCEPTED])
		BREAK
		
		CASE TAXI_STAT_FARES_EXPIRED
		
			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_EXPIRED]++
			TAXI_STATS_DEBUG("Fares Expired ++ ",g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_EXPIRED])
			
		BREAK
		
//		CASE TAXI_STAT_FARES_FLAWLESS		
//		
//			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_FLAWLESS]++
//			TAXI_STATS_DEBUG("Flawless Fares ++ ",g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_FLAWLESS])
//		BREAK
		
		//Get Paid the Hard Way
		CASE TAXI_STAT_PASS_RUN		
		
			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_PASS_RUN]++
			TAXI_STATS_DEBUG("Passengers run ++ = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_PASS_RUN] )	
		BREAK

		CASE TAXI_STAT_PASS_PAY		
			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_PASS_PAY]++
			TAXI_STATS_DEBUG( "Passenger Forced to Pay ++ = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_PASS_PAY] )	
									
		BREAK
		
		//Lost & Found Items
//		CASE TAXI_STAT_ITEMS_KEPT	
//			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_ITEMS_KEPT]++
//			TAXI_STATS_DEBUG( "Items Kept ++ ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_ITEMS_KEPT] )	
//		BREAK
	
//		CASE TAXI_STAT_ITEMS_RTURNED	
//		
//			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_ITEMS_RTURNED]++
//			TAXI_STATS_DEBUG( "Items Returned ++ ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_ITEMS_RTURNED] )	
//		BREAK
					
		
		//Distance
//		CASE TAXI_STAT_DIST_TOTAL			
//			
//			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_TOTAL]+= iNewStatValue
//			TAXI_STATS_DEBUG( "Total Distance traveled is now ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_TOTAL] )	
//		BREAK
		
//		CASE TAXI_STAT_DIST_SHORTEST
//			IF iNewStatValue <> 0
//				IF iNewStatValue < g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_SHORTEST]
//					g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_SHORTEST] = iNewStatValue
//					
//					TAXI_STATS_DEBUG( "This distance " , iNewStatValue )
//					TAXI_STATS_DEBUG( "is shorter than current best", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_SHORTEST])		
//				ELSE
//					TAXI_STATS_DEBUG( "Shortest Distance Not Beat ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_SHORTEST] )
//				ENDIF
//			ENDIF
//		BREAK
		
		CASE TAXI_STAT_DIST_LONGEST	
			IF iNewStatValue <> 0
				IF iNewStatValue > g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_LONGEST]
					g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_LONGEST] = iNewStatValue
					
					TAXI_STATS_DEBUG( "This distance " , iNewStatValue )
					TAXI_STATS_DEBUG( " is longer than current best", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_LONGEST])
				ELSE
					TAXI_STATS_DEBUG( "Longest Distance Not Beat ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_LONGEST] )
				ENDIF
			ENDIF
		BREAK
		
		CASE TAXI_STAT_DIST_TOTAL_W_PASSENGER
			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_TOTAL_W_PASSENGER]+= iNewStatValue
			TAXI_STATS_DEBUG( "Total Distance w/ Passenger = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_TOTAL_W_PASSENGER] )	
		BREAK
		
//		CASE TAXI_STAT_DIST_AVG	
//			IF g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_ACCEPTED] > 0
//				g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_AVG] = g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_TOTAL] / g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_ACCEPTED]
//				
//				TAXI_STATS_DEBUG( "Avg Distance = Total Distance ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_TOTAL] ) 
//				TAXI_STATS_DEBUG("DIVIDED BY Fares Accepted", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_ACCEPTED])
//				TAXI_STATS_DEBUG(" EQUALS ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_DIST_AVG] )	
//			ENDIF
//		BREAK
											
		//Wanted Levels
		CASE TAXI_STAT_WANTED_GAINED	
			IF iNewStatValue = 0
				g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_WANTED_GAINED]++
			ELSE
				g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_WANTED_GAINED]+= iNewStatValue
			ENDIF
			TAXI_STATS_DEBUG( "Wanted Levels ++ = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_WANTED_GAINED] )	
		BREAK
	
		CASE TAXI_STAT_WANTED_LOST		
			IF iNewStatValue > 0
				g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_WANTED_LOST]+= iNewStatValue
			ELSE
				g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_WANTED_LOST]++
			ENDIF
			TAXI_STATS_DEBUG( "Wanted Levels Lost = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_WANTED_LOST] )	
		BREAK
											
		//Violence/Carelessness/Fun
		CASE TAXI_STAT_FUN_NUM_WRECKED		
			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FUN_NUM_WRECKED]++
			TAXI_STATS_DEBUG(  "Taxis wrecked ++ = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FUN_NUM_WRECKED] )	
		BREAK
	
//		CASE TAXI_STAT_FUN_NUM_PEDS_KILLED	
//			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FUN_NUM_PEDS_KILLED]++
//			TAXI_STATS_DEBUG( "Peds killed ++ = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FUN_NUM_PEDS_KILLED] )	
//		BREAK
					
		CASE TAXI_STAT_NUM_HORN_HONKS	
			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_NUM_HORN_HONKS]++
			TAXI_STATS_DEBUG( "Horn Honked ++ = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_NUM_HORN_HONKS] )	
		BREAK
		//	//Money
		CASE TAXI_STAT_MONEY_TOTAL		
			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_TOTAL] += iNewStatValue
			TAXI_STATS_DEBUG( "Total Money Earned = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_TOTAL] )	
		BREAK
		
		CASE TAXI_STAT_MONEY_TIPS
			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_TIPS] += iNewStatValue
			TAXI_STATS_DEBUG( "Total Tips Earned = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_TIPS] )	
		BREAK
		
		CASE TAXI_STAT_MONEY_HI_TIP		
			IF iNewStatValue > g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_HI_TIP]
				g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_HI_TIP] = iNewStatValue
				TAXI_STATS_DEBUG( "New Highest Tip = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_HI_TIP] )	
			ELSE
			
				TAXI_STATS_DEBUG( "Highest Tip Not Reached = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_HI_TIP] )	
			ENDIF
		BREAK
	
//		CASE TAXI_STAT_MONEY_LO_TIP	
//		
//			IF iNewStatValue < g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_LO_TIP]
//			OR g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_LO_TIP] = 0
//				g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_LO_TIP] = iNewStatValue
//				TAXI_STATS_DEBUG(  "New Lowest Tip = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_LO_TIP] )	
//			ELSE
//			
//				TAXI_STATS_DEBUG( "Lowest Tip Not Reached = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_MONEY_LO_TIP] )	
//			ENDIF
//			
//
//		BREAK
											
		//	//Time
		
//		CASE TAXI_STAT_TIME_AVG_FARE	
//			IF g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_ACCEPTED] > 0
//				g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_TIME_AVG_FARE] = g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_TIME_TOTAL] / g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_ACCEPTED]
//				
//				TAXI_STATS_DEBUG( "Avg Time = Total Time ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_TIME_TOTAL])
//				TAXI_STATS_DEBUG( "DIVIDED BY Fares Accepted", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_FARES_ACCEPTED])
//				TAXI_STATS_DEBUG(" EQUALS ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_TIME_AVG_FARE] )	
//			ENDIF
//		BREAK
		
//		CASE TAXI_STAT_TIME_TOTAL		
//			
//			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_TIME_TOTAL]+= iNewStatValue
//			TAXI_STATS_DEBUG( "Total Time Spent on Taxi Oddjobs = ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_TIME_TOTAL] )	
//		BREAK
	
//		CASE TAXI_STAT_TIME_PICKUP	
//		
//			g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_TIME_PICKUP]+= iNewStatValue
//			TAXI_STATS_DEBUG( "Total Time Spent Picking up= ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_STAT_TIME_PICKUP] )	
//		BREAK
	
	ENDSWITCH
ENDPROC
FUNC STRING TAXI_STATS_GET_STAT_STRING(INT iStatValue)
	TAXI_STATS_INFO eStat = INT_TO_ENUM(TAXI_STATS_INFO,iStatValue)
	STRING sString
	
	SWITCH eStat
		//Fares
		CASE TAXI_STAT_FARES_COMPLETED
			sString = "TAXI_ST_FCOMP"
		BREAK
		
		CASE TAXI_STAT_FARES_FAILED
			sString = "TAXI_ST_FFAIL"
		BREAK
		CASE TAXI_STAT_FARES_ACCEPTED
			sString = "TAXI_ST_FACC"
		BREAK
		
		CASE TAXI_STAT_FARES_EXPIRED
			sString = "TAXI_ST_FEXP"
		BREAK
//		CASE TAXI_STAT_FARES_FLAWLESS
//			sString = "TAXI_ST_FFLW"
//		BREAK
		
		//Distance
//		CASE TAXI_STAT_DIST_SHORTEST
//			sString = "TAXI_ST_DSHOR"
//		BREAK
		CASE TAXI_STAT_DIST_LONGEST
			sString = "TAXI_ST_DLONG"
		BREAK
		
//		CASE TAXI_STAT_DIST_AVG
//			sString = "TAXI_ST_DAVG"
//		BREAK
		CASE TAXI_STAT_DIST_TOTAL_W_PASSENGER
			sString = "TAXI_ST_DPASS"
		BREAK
		
//		CASE TAXI_STAT_DIST_TOTAL
//			sString = "TAXI_ST_DCAB"
//		BREAK
		
		//Wanted
		CASE TAXI_STAT_WANTED_GAINED
			sString = "TAXI_ST_WANTG"
		BREAK
		CASE TAXI_STAT_WANTED_LOST
			sString = "TAXI_ST_WANTL"
		BREAK
		
		//Violence
		CASE TAXI_STAT_FUN_NUM_WRECKED
			sString = "TAXI_ST_WRECK"
		BREAK
//		CASE TAXI_STAT_FUN_NUM_PEDS_KILLED
//			sString = "TAXI_ST_PKILLS"
//		BREAK
		
		//General
		CASE TAXI_STAT_NUM_HORN_HONKS
			sString = "TAXI_ST_HONKS"
		BREAK
		
		//Money
		CASE TAXI_STAT_MONEY_TOTAL
			sString = "TAXI_ST_MONE"
		BREAK
		
		CASE TAXI_STAT_MONEY_TIPS
			sString = "TAXI_ST_TIPS"
		BREAK
		
		CASE TAXI_STAT_MONEY_HI_TIP
			sString = "TAXI_ST_TIPHI"
		BREAK
		
//		CASE TAXI_STAT_MONEY_LO_TIP
//			sString = "TAXI_ST_TIPLO"
//		BREAK
		
		//Time
//		CASE TAXI_STAT_TIME_TOTAL
//			sString = "TAXI_ST_TDROP"
//		BREAK
		
//		CASE TAXI_STAT_TIME_PICKUP
//			sString = "TAXI_ST_TPICK"
//		BREAK
		
//		CASE TAXI_STAT_TIME_AVG_FARE
//			sString = "TAXI_ST_DOAVG"
//		BREAK
		
//		CASE TAXI_STAT_TIME_AVG_PICKUP
//			sString = "TAXI_ST_POAVG"
//		BREAK
		
		//Lost & Found 
		
//		CASE TAXI_STAT_ITEMS_KEPT
//			sString = "TAXI_ST_IKEPT"
//		BREAK
		
//		CASE TAXI_STAT_ITEMS_RTURNED
//			sString = "TAXI_ST_IRET"
//		BREAK
		
		//Get Paid the Hard Way
		CASE TAXI_STAT_PASS_RUN
			sString = "TAXI_ST_PGOT"
		BREAK
		
		CASE TAXI_STAT_PASS_PAY
			sString = "TAXI_ST_PRAN"
		BREAK
		
	ENDSWITCH
	
	RETURN sString
ENDFUNC

/// PURPOSE: Handles the selection & drawing of the cheats menu.
///    
/// PARAMS:
///    iNumCheats - I use this to control the number of cheats presented. Currently outside of a mission it depends on TAXI_CO_NUM_NONMISSION_CHEATS
///    So if you have cheats that you want to add make sure to add them after TAXI_CO_RANK but before the mission specific TAXI_CO_SKIP and increment TAXI_CO_NUM_NONMISSION_CHEATS accordingly
/// RETURNS:
///    
///    
/*
FUNC BOOL Taxi_Stats_Menu(INT &iCurrentStatsPage)

	FLOAT fInfoPosY
		
	// Scroll & Select Cheats.

	// R1 - Page ++
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RB)
		//1 -> 2 -> 3
		IF (iCurrentStatsPage < (STATS_NUM_PAGES - 1))
			++iCurrentStatsPage
		//Last page -> Scroll around
		ELSE
			iCurrentStatsPage = 0
		ENDIF
	// L1 - Page --
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LB)
		IF (iCurrentStatsPage > 0)
			--iCurrentStatsPage
		ELSE
			iCurrentStatsPage = STATS_NUM_PAGES - 1
		ENDIF
		
	// B/O Button - Exit menu.
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
		iCurrentStatsPage = 0
		RETURN FALSE
	ENDIF
	
	// Display Choose Race heading.
	SET_TEXT_SCALE(TAXI_UI_SM_HEAD_SCALE, TAXI_UI_SM_HEAD_SCALE)
	DISPLAY_TEXT(TAXI_UI_SM_HEAD_POS_X, TAXI_UI_SM_HEAD_POS_Y, "TC_STATS_B1")
	
	// Display Choose Race info (name/rank/time).
	// TODO: Need some sort of display line limit and scrolling.
	INT i
	INT iStat
	
	REPEAT STATS_NUM_PER_PAGE i
		iStat = i + (iCurrentStatsPage * STATS_NUM_PER_PAGE)
		IF iStat < ENUM_TO_INT(TAXI_STATS_NUM_STATS)
			// Cache Choose Race info position-y.
			fInfoPosY = 0.2 + TAXI_UI_SM_INFO_POS_Y + (i * TAXI_UI_SM_INFO_POS_Y)
			SET_TEXT_SCALE(TAXI_UI_SM_INFO_SCALE, TAXI_UI_SM_INFO_SCALE)
			DISPLAY_TEXT(TAXI_UI_SM_INFO_POS_X, fInfoPosY, TAXI_STATS_GET_STAT_STRING(iStat))
			SET_TEXT_SCALE(TAXI_UI_SM_INFO_SCALE, TAXI_UI_SM_INFO_SCALE)
			DISPLAY_TEXT_WITH_NUMBER(TAXI_UI_SM_STAT_POS_X, fInfoPosY, "NUMBER", g_savedGlobals.sTaxiData.iTaxiStats[iStat])

		ENDIF
	ENDREPEAT
	
	// Display Choose Race controls (select/enter/stats/exit).
//	SET_TEXT_SCALE(TAXI_UI_SM_BUT_SCALE, TAXI_UI_SM_BUT_SCALE)
//	DISPLAY_TEXT(TAXI_UI_SM_SEL_POS_X, TAXI_UI_SM_SEL_POS_Y, "TC_UI_PAGE")
//	//Page Number
//	SET_TEXT_SCALE(TAXI_UI_SM_BUT_SCALE, TAXI_UI_SM_BUT_SCALE)
//	DISPLAY_TEXT_WITH_NUMBER(TAXI_UI_SM_CURRPAGE_POS_X, TAXI_UI_SM_SEL_POS_Y, "NUMBER",(iCurrentStatsPage + 1))
//	
//	SET_TEXT_SCALE(TAXI_UI_SM_BUT_SCALE, TAXI_UI_SM_BUT_SCALE)
//	DISPLAY_TEXT_WITH_NUMBER(TAXI_UI_SM_NUMPAGE_POS_X, TAXI_UI_SM_SEL_POS_Y, "NUMBER",STATS_NUM_PAGES)

	SET_TEXT_SCALE(TAXI_UI_SM_BUT_SCALE, TAXI_UI_SM_BUT_SCALE)
	DISPLAY_TEXT(TAXI_UI_SM_EXIT_POS_X, TAXI_UI_SM_EXIT_POS_Y, "TC_UI_EXIT")
	
	// Display Choose Race rectangle.
	DRAW_RECT(TAXI_UI_SM_RECT_POS_X, TAXI_UI_SM_RECT_POS_Y, TAXI_UI_SM_RECT_WIDTH, TAXI_UI_SM_RECT_HEIGHT, 0, 0, 0, TAXI_UI_SM_RECT_ALPHA)
		
	// Choose SPR Race still running.
	RETURN TRUE
ENDFUNC
*/

//EOF
