// Taxi_Widgets_lib.sch
USING "Taxi_Debug_Lib.sch"
USING "Taxi_Point_Driving.sch"

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID	taxiWidgets, taxiWidgets_Gameplay, taxiWidgets_HUD, taxiWidgets_Cams, taxiWidgets_Dialogue
	
	BOOL bOTXWTurnOnDebugDraw	
	BOOL bOTXW_DrawHudOverlay
	BOOL bOTXW_DrawTaxiTimers
	BOOL bOTXW_DrawTaxiTips
	BOOL bOTXW_DrawTaxiVelocity
	BOOL bOTXW_DrawTaxiWanted
	BOOL bOTXW_StartOutputCam
	
	
	BOOL bOTXW_DialogueSTATEDebug 
	BOOL bOTXW_DialogueDebug 
	BOOL bOTXW_DialogueQueueNumDebug 
	BOOL bOTXW_DialogueQueueState
	BOOL bOTXW_DialogueSpeechGate 
	BOOL bOTXW_ShowTaxiHealth
	BOOL bOTXW_ShowTaxiOdometer
	TEXT_LABEL_63				sConvLabel
	
	PROC ENABLE_ALL_DIALOGUE_DEBUG()
	
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
		bOTXW_DialogueSTATEDebug = TRUE
		bOTXW_DialogueDebug = TRUE
		bOTXW_DialogueQueueNumDebug = TRUE
		bOTXW_DialogueSpeechGate = TRUE
		bOTXW_DialogueQueueState = TRUE
	
	ENDPROC
	
	PROC INIT_OJ_TAXI_WIDGETS_CAMERA()
		
		taxiWidgets_Cams = START_WIDGET_GROUP("Cameras")
		
			ADD_WIDGET_BOOL("Turn Out Output Cam", bOTXW_StartOutputCam)
		
		STOP_WIDGET_GROUP()
		
		
					/*
	//CAMERA WIDGET
	vOffSetCam = <<1.11,2.59, 1.5>>
	vOffSetPlayer = <<-0.2332,0.2521, 1.1115>>
		
	ADD_WIDGET_STRING("Camera Codes")
		ADD_WIDGET_BOOL("Show Camera Cut", bDebugCam)
		ADD_WIDGET_BOOL("Clean Up  Camera Cut", bDebugCamClean)
		ADD_WIDGET_INT_SLIDER("Pickup State",myTaxiData.iTaxiOJ_StatesPickup,0,2,1)
		ADD_WIDGET_FLOAT_SLIDER("Cam X = ", vOffSetCam.x, -4.0, 5.0,0.01)
		ADD_WIDGET_FLOAT_SLIDER("Cam Y = ", vOffSetCam.y, -4.0, 5.0,0.01)
		ADD_WIDGET_FLOAT_SLIDER("Cam Z = ", vOffSetCam.z, -4.0, 5.0,0.01)
		
		ADD_WIDGET_FLOAT_SLIDER("Player Cam X = ", vOffSetPlayer.x, -4.0, 5.0,0.01)
		ADD_WIDGET_FLOAT_SLIDER("Player Cam Y = ", vOffSetPlayer.y, -4.0, 5.0,0.01)
		ADD_WIDGET_FLOAT_SLIDER("Player Cam Z = ", vOffSetPlayer.z, -4.0, 5.0,0.01)
*/
		
		
	ENDPROC

	PROC INIT_OJ_TAXI_WIDGETS_DIALOGUE()
		taxiWidgets_Dialogue = START_WIDGET_GROUP("Dialogue")
			
			
			ADD_WIDGET_BOOL("Display Dialogue State", bOTXW_DialogueSTATEDebug)
			ADD_WIDGET_BOOL("Enable Current Conv Debug", bOTXW_DialogueDebug)
			ADD_WIDGET_BOOL("Show Dialogue Q Value", bOTXW_DialogueQueueNumDebug)
			ADD_WIDGET_BOOL("Show DQ State", bOTXW_DialogueQueueState)
			ADD_WIDGET_BOOL("Show if Taxi Speech is enabled", bOTXW_DialogueSpeechGate)
		STOP_WIDGET_GROUP()
		
	ENDPROC

	PROC INIT_OJ_TAXI_WIDGETS_HUD()
		taxiWidgets_HUD = START_WIDGET_GROUP("HUD")
	
			ADD_WIDGET_BOOL("Draw HUD Overlay", bOTXW_DrawHudOverlay)
			
			ADD_WIDGET_BOOL("Draw MPH", bOTXW_DrawTaxiVelocity)
			
			ADD_WIDGET_BOOL("Show Taxi Health",bOTXW_ShowTaxiHealth)
			
			ADD_WIDGET_BOOL("Show Taxi Odometer",bOTXW_ShowTaxiOdometer)
			
			ADD_WIDGET_STRING("Taxi Timers")
				ADD_WIDGET_BOOL("Turn on Taxi Timers Debug",bOTXW_DrawTaxiTimers)
	
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	PROC INIT_OJ_TAXI_WIDGETS_GAMEPLAY()
		taxiWidgets_Gameplay = START_WIDGET_GROUP("Gameplay")
		
			ADD_WIDGET_BOOL("Turn on Tip Debug", bOTXW_DrawTaxiTips)
			
			ADD_WIDGET_BOOL("Wanted Level Debugging",bOTXW_DrawTaxiWanted)
		
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC INIT_TAXI_WIDGETS(WIDGET_GROUP_ID & otherWidgetGroup)
		
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bOTXWTurnOnDebugDraw)
		
		// Setup the widgets
		SET_CURRENT_WIDGET_GROUP(otherWidgetGroup)
		taxiWidgets = START_WIDGET_GROUP("Taxi Widgets")
		
		ADD_WIDGET_STRING("General Ride Settings:")
			ADD_WIDGET_FLOAT_SLIDER("Time between wanted level complaints : ", TAXI_TWEAKS_TIME_DX_DELAY_POLICE, 1.0,60.0,1.0)
			ADD_WIDGET_INT_SLIDER("# of horn honks to complain:", TAXI_TWEAKS_NUM_HONKS_BEFORE_BITCH, 1,50,1)
			ADD_WIDGET_FLOAT_SLIDER("Time before failing for leaving taxi:", TAXI_TWEAK_TIME_FAIL_ABANDONED_CAR, 10.0,60.0,1.0)
			
		ADD_WIDGET_STRING("Pickup Distance:")

			ADD_WIDGET_FLOAT_SLIDER("Ratio of Speed: ", TAXI_TWEAKS_SPEED_FACTOR, 1.0,20.0,1.0)
			ADD_WIDGET_INT_SLIDER("Alpha of Sphere: ", TAXI_TWEAKS_SPEED_ALPHA, 1,255,2)
			ADD_WIDGET_FLOAT_SLIDER("Padding: ", TAXI_TWEAKS_PICKUP_PAD, 1.0,10.0,1.0)
			
			
		ADD_WIDGET_STRING("All the Excitement Tweaks Times:")
		
		ADD_WIDGET_STRING("EX- Gen Settings:")//------------------------------------------------------		
			ADD_WIDGET_FLOAT_SLIDER("Boring Timer:", TAXI_TWEAKS_TIME_BORING, 10.0,120.0,1.0)
			ADD_WIDGET_FLOAT_SLIDER("Relaxing Timer:", TAXI_TWEAKS_TIME_RELAXING, 10.0,120.0,1.0)
			ADD_WIDGET_FLOAT_SLIDER("Ran Red Cap", TAXI_TWEAKS_EX_RAN_RED_CAP, 100.0, 10000.0, 100.0)
			ADD_WIDGET_FLOAT_SLIDER("Delay in between rolls", TAXI_TWEAKS_EX_DELAY_FOR_ROLL_CHECK, 1.0, 100.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Min roll in degrees", TAXI_TWEAKS_ROLL_NEG, -180.0, -1.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Max roll in degrees", TAXI_TWEAKS_ROLL_POS, 1.0, 180.0, 1.0)
			
		ADD_WIDGET_STRING("EX- Air:")//----------------------------------------------------------
			ADD_WIDGET_FLOAT_SLIDER("Delay BTW Checks ", TAXI_TWEAKS_EX_DELAY_FOR_AIR_CHECK, 1.0, 100.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Min Time", TAXI_TWEAKS_TIME_AIR, 0.1, 10.0, 0.1)
			
		ADD_WIDGET_STRING("EX- Speeding:")//----------------------------------------------------------
			
			ADD_WIDGET_FLOAT_SLIDER("MPH for Speeding ", TAXI_TWEAKS_THRESHOLD_SPEED, 10.0, 50.0, 5.0)
			ADD_WIDGET_FLOAT_SLIDER("Min Time", TAXI_TWEAKS_TIME_SPEEDIN, 1.0, 100.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Delay in between Speed checks", TAXI_TWEAKS_EX_DELAY_FOR_SPEEDING_CHECK, 1.0, 60.0, 1.0)
		
		ADD_WIDGET_STRING("EX- Powersliding:")//------------------------------------------------------
			ADD_WIDGET_FLOAT_SLIDER("X Velocity Threshold", TAXI_TWEAKS_THRESHOLD_DRIFT, 0.1, 100.0, 0.025)
			ADD_WIDGET_FLOAT_SLIDER("Seconds to hold", TAXI_TWEAKS_TIME_POWERSLIDING, 1.0, 10.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Delay in between checks", TAXI_TWEAKS_EX_DELAY_FOR_DRIFT_CHECK, 1.0, 100.0, 1.0)
		
		ADD_WIDGET_STRING("EX- On Coming Traffic:")//------------------------------------------------------

			ADD_WIDGET_FLOAT_SLIDER("Min Time", TAXI_TWEAKS_TIME_ONCOMING_TRAFFIC, 1.0, 100.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Delay in between checks", TAXI_TWEAKS_EX_DELAY_FOR_ONCOMING_CHECK, 1.0, 100.0, 1.0)
			
		ADD_WIDGET_STRING("EX- Sidewalkin:")//------------------------------------------------------
			ADD_WIDGET_FLOAT_SLIDER("Min Time", TAXI_TWEAKS_TIME_SIDEWALKIN, 1.0, 100.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Delay btw checks", TAXI_TWEAKS_EX_DELAY_FOR_SIDEWALK_CHECK, 1.0, 100.0, 1.0)
			
		ADD_WIDGET_STRING("EX- Quick Stop:")//------------------------------------------------------
			
			ADD_WIDGET_FLOAT_SLIDER("Speed Delta", TAXI_TWEAKS_SPEED_CHANGE_DIFF, 1.0, 15.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Min Delta", TAXI_TWEAKS_SPEED_CHANGE_MIN, 1.0, 20.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Speed Time To Check", TAXI_TWEAK_TIME_DRAMATIC_SPEED_CHANGE, 0.1, 3.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Delay btw checks", TAXI_TWEAKS_EX_DELAY_FOR_QSTOP_CHECK, 1.0, 100.0, 1.0)
		
		ADD_WIDGET_STRING("EX- Swerving:")//------------------------------------------------------
			ADD_WIDGET_FLOAT_SLIDER("Swerve Delta", TAXI_TWEAKS_SWERVE_INIT_SPEED, 1.0, 5.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Delay btw checks", TAXI_TWEAKS_EX_DELAY_FOR_SWERVING_CHECK, 1.0, 100.0, 1.0) 
		
		ADD_WIDGET_STRING("EX- Reverse:")//------------------------------------------------------
			ADD_WIDGET_FLOAT_SLIDER("Delay btw checks", TAXI_TWEAKS_EX_DELAY_FOR_REVERSE_CHECK, 1.0, 100.0, 1.0)
			
		ADD_WIDGET_STRING("EX- Near Miss:")//------------------------------------------------------
			ADD_WIDGET_FLOAT_SLIDER("Delay btw checks", TAXI_TWEAKS_EX_DELAY_FOR_NEARMISS_CHECK, 1.0, 100.0, 1.0)
			
		ADD_WIDGET_STRING("Hit Tweaks:")
			
			ADD_WIDGET_FLOAT_SLIDER("Amount of excitement for horn honking: ", TAXI_TWEAKS_VALUE_HORN_EXCITEMENT, 1.0,50.0,1.0)
			ADD_WIDGET_FLOAT_SLIDER("Small Hit Min Damage ", TAXI_TWEAKS_SMALL_HIT, 10.0, 200.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Big Hit Min Damage", TAXI_TWEAKS_BIG_HIT, 40.0, 200.0, 1.0)
			
			ADD_WIDGET_FLOAT_SLIDER("Took Damage Delay", TAXI_TWEAKS_EX_DELAY_FOR_TOOK_DAMAGE, 1.0, 100.0, 1.0)
			
			ADD_WIDGET_FLOAT_SLIDER("Hit Ped Delay", TAXI_TWEAKS_EX_DELAY_FOR_HIT_PED, 1.0, 100.0, 1.0)
			
			ADD_WIDGET_FLOAT_SLIDER("Hit Car Delay", TAXI_TWEAKS_EX_DELAY_FOR_HIT_VEHICLE, 1.0, 100.0, 1.0)
			
			ADD_WIDGET_FLOAT_SLIDER("Hit Building Delay", TAXI_TWEAKS_EX_DELAY_FOR_HIT_BUILDING, 1.0, 100.0, 1.0)
			
			ADD_WIDGET_FLOAT_SLIDER("Hit Object Delay", TAXI_TWEAKS_EX_DELAY_FOR_HIT_OBJECT, 1.0, 100.0, 1.0)

			
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(otherWidgetGroup)
	ENDPROC
	
	PROC INIT_ODDJOB_TAXI_WIDGETS(BOOL bGameplay = TRUE, BOOL bDialogue = TRUE, BOOL bCamera = TRUE, BOOL bHUD = TRUE, BOOL bVolumes = TRUE)
		IF bHUD
			INIT_OJ_TAXI_WIDGETS_HUD()
		ENDIF
		
		IF bGameplay
			INIT_OJ_TAXI_WIDGETS_GAMEPLAY()
		ENDIF
		
		IF bCamera
			INIT_OJ_TAXI_WIDGETS_CAMERA()
		ENDIF	
		
		IF bDialogue
			INIT_OJ_TAXI_WIDGETS_DIALOGUE()
		ENDIF
	
		IF bVolumes
			INIT_ZVOLUME_WIDGETS()
		ENDIF
		
	ENDPROC
	
	PROC UPDATE_TAXI_WIDGETS(TaxiStruct &myTaxiData,TAXI_OJ_DIALOGUE_Q_DATA	&tTaxiOJ_DQ_Data)
		// Any update needed?
		//HUD Widget Update
		TAXI_DEBUG_HUD_POS(bOTXW_DrawHudOverlay)
		
		UPDATE_ZVOLUME_WIDGETS()
		IF bOTXW_ShowTaxiOdometer
			TAXI_DEBUG_SHOW_Odometer(myTaxiData)
		ENDIF
		
		IF bOTXW_ShowTaxiHealth
			TAXI_DEBUG_SHOW_HEALTH_ON_SCREEN(myTaxiData)
		ENDIF
		
		IF bOTXW_DrawTaxiWanted
			TAXI_DEBUG_SHOW_WANTED(myTaxiData)
		ENDIF
		
		IF bOTXW_DrawTaxiTimers
			TAXI_DEBUG_TIMERS(myTaxiData)
		ENDIF
		
		IF bOTXW_DrawTaxiTips
			TAXI_DEBUG_SHOW_TIPS(myTaxiData)
		ENDIF
		
		IF bOTXW_DrawTaxiVelocity
			TAXI_DEBUG_SPEED_TEXT_LABEL(myTaxiData)
		ENDIF
		
		IF bOTXW_StartOutputCam
			//Grab cam coords func - Usually forget what this is
			OUTPUT_DEBUG_CAM_RELATIVE_TO_ENTITY(myTaxiData.viTaxi)
			
			bOTXW_StartOutputCam = FALSE
		ENDIF
		
		IF bOTXW_DialogueSTATEDebug 
			TAXI_DEBUG_SHOW_CURRENT_DIALOGUE_INDEX(myTaxiData)
		ENDIF
		
		IF bOTXW_DialogueDebug
			TAXI_DEBUG_SHOW_CURRENT_PLAYING_LABEL(sConvLabel)
		ENDIF
		
		IF bOTXW_DialogueQueueNumDebug
			TAXI_DEBUG_SHOW_CURRENT_D_QUEUE_NUM(tTaxiOJ_DQ_Data )
		ENDIF
		
		IF bOTXW_DialogueSpeechGate
			TAXI_DEBUG_SHOW_SPEECH_GATE_STATUS(myTaxiData)
		ENDIF
		
		IF bOTXW_DialogueQueueState
			TAXI_DEBUG_SHOW_DQ_STATE(tTaxiOJ_DQ_Data)
		ENDIF
		/*Cam Updates
			IF bDebugCam
		IF NOT ARE_VECTORS_EQUAL(vOffSetCam, vRealOffsetCam)
		OR NOT ARE_VECTORS_EQUAL(vOffSetPlayer, vRealOffSetPlayer)
			
			vRealOffsetCam = vOffSetCam
			vRealOffSetPlayer = vOffSetPlayer 
			
			DESTROY_CAM(myTaxiData.camTaxi)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
							
			IF NOT DOES_CAM_EXIST(myTaxiData.camTaxi)
				myTaxiData.camTaxi = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, 50, FALSE) // TODO : Change 1st param to as of CAMTYPE_SCRIPTED after Thomas French's email
			ENDIF
							
			SET_CAM_COORD(myTaxiData.camTaxi,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.piTaxiPlayer,vOffSetCam))
			POINT_CAM_AT_COORD(myTaxiData.camTaxi,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.piTaxiPlayer, vOffSetPlayer))
			
			SET_CAM_ACTIVE(myTaxiData.camTaxi, TRUE)		
			RENDER_SCRIPT_CAMS(TRUE,FALSE)
			
		ENDIF
		
	ENDIF
	
	IF bDebugCamClean
		CLEANUP_TAXI_QUICK_CAM(myTaxiData)
		bDebugCamClean = FALSE
	ENDIF
	*/
	
	ENDPROC
	
	PROC CLEANUP_TAXI_WIDGETS()
		// Clean all of the above.
		CLEANUP_ZVOLUME_WIDGETS()
		
		IF DOES_WIDGET_GROUP_EXIST(taxiWidgets_Dialogue)
			DELETE_WIDGET_GROUP(taxiWidgets_Dialogue)
		ENDIF
		
		IF DOES_WIDGET_GROUP_EXIST(taxiWidgets_Cams)
			DELETE_WIDGET_GROUP(taxiWidgets_Cams)
		ENDIF
		
		IF DOES_WIDGET_GROUP_EXIST(taxiWidgets)
			DELETE_WIDGET_GROUP(taxiWidgets)
		ENDIF
		
		IF DOES_WIDGET_GROUP_EXIST(taxiWidgets_Gameplay)
			DELETE_WIDGET_GROUP(taxiWidgets_Gameplay)
		ENDIF
		
		IF DOES_WIDGET_GROUP_EXIST(taxiWidgets_HUD)
			DELETE_WIDGET_GROUP(taxiWidgets_HUD)
		ENDIF
		
		
		
	ENDPROC
#ENDIF



