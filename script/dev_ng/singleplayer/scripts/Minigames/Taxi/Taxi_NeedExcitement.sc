//=======================================================================================================================================
// Taxi_NeedExcitement.sc
// Dev : John R. Diaz
/*
	•	“Show me some excitement.” The passenger requests excitement.  Bonus tips for air time, close calls with other cars, high speeds, and long drifts. 
		No penalties for collisions.
*/

//CHANGELOG==========================================================================================================
//9/27/11 - Reformatting/prettifying all taxi oj scripts
//===================================================================================================================


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//Includes------------------------------------------------------
USING "Taxi_Includes.sch"

//Local Variables-----------------------------------------------
ENUM TAXIOJ_NEX_BONUS
	TNEX_BONUS_EXTREME = 0,
	TNEX_BONUS_TOTAL
				
ENDENUM

TaxiStruct 						myTaxiData

TAXI_PED_RUN_STATE 				myRunState = TPRS_INIT

TAXI_MONEY_STRUCT 				taxiMoney

BONUS_FIELD						bonusFieldNeedExcitement[TNEX_BONUS_TOTAL]

MODEL_NAMES 					mPassengerModel = A_M_Y_SKATER_02

//Vectors
VECTOR 							vPassengerPt = << -496.0739, -336.6628, 33.5017 >>	//Fanny Howe Medical Center
VECTOR 							vPassengerPickupPt = << -484.6879, -330.1646, 33.3534 >>
VECTOR 							vDropOff = << 725.3476, 1204.5260, 324.9057 >> //<< 742.5978, 1200.3478, 325.3470 >> //Vinewood Sign
//VECTOR							vOffRoadLookAt = << 503.3788, 750.9902, 198.4714 >>

//Ints
INT 							iDebugThrottle = 1
INT								iTipIndex = 0


//Bools
BOOL 							bDropOffFound
BOOL							bOffroadChallenged = FALSE
//BOOL							bOffroadHelpShown = FALSE

//DialogueQ Info
BOOL							g_bDebug = FALSE
TAXI_OJ_DIALOGUE_Q_DATA			tTaxiOJ_DQ_Data								
TAXI_OJ_DQ_CONVERSATION_LINE	tDialogueLine[CONST_TAXIOJ_SIZE_Q]

CONST_INT						CONST_TAXI_NEX_AMAZING_CUTOFF 30
CONST_INT						TAXI_CONST_BONUS_CASH_DAREDEVIL		200

AGGRO_ARGS 						aggroArgs 

structTimer						shoutDelay

SCRIPT_SHARD_BIG_MESSAGE TaxiMidSize
//Debug---------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	//HUD_2D_DEADLINE 			hudDeadline
	WIDGET_GROUP_ID 			taxiRideWidgets,ojTaxiWidgets_Excitement
	BOOL 						bDebugTurnOnFreeRide = FALSE
	BOOL						bDebugShowExcitement
	TEXT_LABEL_63				sDebugString[2]	
	BOOL						bDebugBonusDaredevil
	BOOL						bFreeRideisOn
	TXM_DEBUG_SKIP_STATES 		tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS
#ENDIF


//FUNCTIONS------------------------------------------------------------------------------------------------------------

PROC Script_Cleanup()
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(taxiRideWidgets)
			DELETE_WIDGET_GROUP(taxiRideWidgets)
		ENDIF
		
		IF DOES_WIDGET_GROUP_EXIST(ojTaxiWidgets_Excitement)
			DELETE_WIDGET_GROUP(ojTaxiWidgets_Excitement)
		ENDIF
		
		CLEANUP_TAXI_WIDGETS()
	#ENDIF

	SET_MODEL_AS_NO_LONGER_NEEDED(mPassengerModel)
	REMOVE_ANIM_DICT("gestures@m@standing@casual")
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	TERMINATE_THIS_THREAD()
ENDPROC

// Perform any special commands if the script fails
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Failed()
	TAXI_SCRIPT_FAILED(myTaxiData)
	Script_Cleanup()
ENDPROC

PROC INITIALIZE_SCRIPT_VARIABLES(TAXI_MISSION_TYPES thisMissionType = TXM_01_NEEDEXCITEMENT)

	TAXI_ODDJOB_GLOBAL_SETUP(myTaxiData,thisMissionType)
		
	// Set our initial state up. VIP missions don't start at TRS_FINDING_LOCATION.
	myTaxiData.tTaxiOJ_RideState = TRS_INIT_STREAM
	
	SET_TAXI_TIP_CUTOFFS(myTaxiData, 7, 14)//CONST_TAXI_OJ_TIP_AVERAGE_CUTOFF,4)


	myTaxiData.vTaxiOJ_WarpPtPickup = << -492.4436, -290.3657, 34.4860 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingPickup = 202.0759
	
	myTaxiData.vTaxiOJ_WarpPtDropoff = << 856.3766, 1288.9180, 357.7924 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 162.3
	
	myTaxiData.fTaxiEnterSpeed = PEDMOVEBLENDRATIO_WALK //PEDMOVE_RUN
	//TODO : Set the POI
	myTaxiData.vTaxiOJ_PassengerGoToPt = << 810.3334, 1172.4128, 321.8036 >>
	
	#IF IS_DEBUG_BUILD
		#IF DEBUG_iTurnOnAllDXDebug
			ENABLE_ALL_DIALOGUE_DEBUG()
		#ENDIF
		
		taxiRideWidgets = START_WIDGET_GROUP("Taxi Ride - Need Excitement")
			
			INIT_ODDJOB_TAXI_WIDGETS()
			
			//Excitement Widget
			ojTaxiWidgets_Excitement = START_WIDGET_GROUP("Excitement")
			
				ADD_WIDGET_STRING("Excitement Ride Test")
					ADD_WIDGET_BOOL("Turn On Free Ride", bDebugTurnOnFreeRide)
					ADD_WIDGET_BOOL("Show Excitement",bDebugShowExcitement)
					
			
				ADD_WIDGET_STRING("Debug Taxi Bonus")
					ADD_WIDGET_BOOL(" Daredevil",bDebugBonusDaredevil)
			
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		INIT_TAXI_WIDGETS(taxiRideWidgets)
	#ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~~~~~~~~~~~~ Oddjobs | Taxi | Needs Excitement ~~~~~~~~~~~~~----------")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")
	
	
ENDPROC
#IF IS_DEBUG_BUILD
PROC PROCESS_WIDGETS()

	UPDATE_TAXI_WIDGETS(myTaxiData,tTaxiOJ_DQ_Data)
	
	IF bDebugShowExcitement
		sDebugString[0] = "Excitement = "
		sDebugString[0] += TAXI_UTILS_GET_STRING_FROM_INT_SP(myTaxiData.iTaxiOJ_PassengerExcitement)
		sDebugString[0] += " /100"
		DRAW_DEBUG_TEXT_2D(sDebugString[0], <<0.07, 0.46,0.0>>)
		
	ENDIF
	IF bDebugBonusDaredevil
		IF myTaxiData.iTaxiOJ_PassengerExcitement < TAXI_CONST_EXCITEMENT_LEVEL_HI
			sDebugString[1]= "Daredevil Bonus LOCKED "
			DRAW_DEBUG_TEXT_2D(sDebugString[1],<<0.07,0.77,0.0>>,255,0,0)
			
		ELSE
			sDebugString[1]= "Daredevil Bonus UnLOCKED "
			DRAW_DEBUG_TEXT_2D(sDebugString[1],<<0.07,0.77,0.0>>)
		ENDIF
	ENDIF
ENDPROC
#ENDIF
/// PURPOSE: Request all our mission specific assets here
///  
PROC INIT_TAXI_STREAMS()
	//Load text and UI
	REQUEST_MODEL(mPassengerModel)
	REQUEST_ANIM_DICT("gestures@m@standing@casual")
	TAXI_INIT_SHARED_STREAMS()
	TaxiMidSize.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	CDEBUG1LN(DEBUG_OJ_TAXI,"INIT_TAXI_STREAMS - SUCCESS")
ENDPROC

//// PURPOSE: Waits for our assets to load
 ///    
 /// RETURNS: TRUE if everything has loaded correctly FALSE if there was a problem
 ///    

FUNC BOOL TAXI_ASSETS_STREAMED()
	
	IF NOT HAS_MODEL_LOADED(mPassengerModel)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading A_M_M_Skater_01",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("gestures@m@standing@casual")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading gestures@m@standing@casual",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(TaxiMidSize.siMovie)
		#IF IS_DEBUG_BUILD
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TaxiMidSize.siMovie",iDebugThrottle)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	IF NOT TAXI_SHARED_ASSETS_STREAMED(iDebugThrottle)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading shared assets",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_ASSETS_STREAMED - SUCCESS")
	RETURN TRUE		
ENDFUNC

PROC TAXI_OJ_NEX_SET_TIP_AND_EXCITEMENT_TO_CHECK()
	
	//Tip Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POS_DELIVERY_TIME)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_HIT_PED_POS)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_TOOK_DAMAGE_POS)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ROLL_CAR_POS)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LEFT_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_STOPPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_LIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_DISLIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LOST_POLICE)
	
	//Tips for Excitement
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_BORED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_SPEEDING)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_AIR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POWERSLIDING)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ONCOMING)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_SIDEWALK)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_OFFROAD)
	
	//This one is important for THIS mission
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POS_EXCITEMENT)
	
	//EXCITEMENT BITS
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_SPEEDING)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_TOOKDAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_WRONGLANE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_SIDEWALK)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_ROLL)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_HITPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_AIR)
	//SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_OFFROAD) // this is now set after the challenge is issued
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_REVERSE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_POWERSLIDE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_NEAR_MISS)
	
	//Turn off aggro bits
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
	
	//Start the mission timer here
	TAXI_START_TIMER(myTaxiData, TT_RIDETODEST)				
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DL_SET_TIPS_TO_CHECK set tips to check on")
	
ENDPROC

PROC TRIGGER_TAXI_QUEUE_NE_LINES()
	
	//SET_TAXI_OJ_INTERRUPT_TIMER_OFF(myTaxiData)
	
	//Trigger lines
	IF IS_BANTER_SAFE_TO_PLAY(myTaxiData,tTaxiOJ_DQ_Data)
		
		SWITCH tTaxiOJ_DQ_Data.iCurrentDQLine
			CASE 0
				IF myTaxiData.tTaxiOJ_RideState =  TRS_DRIVING_PASSENGER
					IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
					AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER,TRUE)
						TAXI_RESET_TIMERS(myTaxiData, TT_BORING)
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter 1")
						ENDIF
						
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 15
					IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
					AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_3
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_BANTER)
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_3,TRUE)
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter 3")
						ENDIF
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_OJ_TAXI,"banter timer = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER))
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 30
					IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
					AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_2 
						
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_2,TRUE) 
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter 2")
						ENDIF
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_OJ_TAXI,"banter timer = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER))
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 30
					IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
					AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_4 
						
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_4,TRUE) 
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter 4")
						ENDIF
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_OJ_TAXI,"banter timer = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER))
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 30
					IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
					AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_5 
						
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_5,TRUE) 
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter 5")
						ENDIF
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_OJ_TAXI,"banter timer = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER))
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
	//PROCESS_IMPORTANT_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, g_bDebug)
	PROCESS_MANUAL_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, 1, g_bDebug)
ENDPROC

PROC Main_Taxi_OJ_NeedsExcitement()

	//Handles Fail Or No Taxi-------------------------------------------------------------------------------
	IF IS_TAXI_JOB_IN_FAIL_STATE(myTaxiData)
		TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
		
		IF myTaxiData.tTaxiOJ_RideState > TRS_INIT_STREAM
			IF TAXI_HANDLE_FAIL(myTaxiData)
				Script_Failed()
			ENDIF
		ELSE
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CLEANUP) > 5.0
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"Fail During State: "," ", myTaxiData.sTaxiOJ_RideState, " ", myTaxiData.sTaxiOJ_Reason4Fail)
					
				SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
				Script_Cleanup()
			
			ENDIF
		ENDIF
	
	//Proceed
	ELSE
		//Run Throughtout the Entire Mission--------------------------------------------------------------
		TAXI_OJ_MAINTAIN_GLOBAL_GATES(myTaxiData)
		
		IF myTaxiData.tTaxiOJ_RideState <= TRS_DRIVING_PASSENGER
			//Gobal taxi updates that all taxi missions run every frame
			RUN_GLOBAL_TAXI_UPDATES(myTaxiData,aggroArgs)
		
			PROCESS_TAXI_EXCEPTIONS(myTaxiData)
		ENDIF
		
		//Tips
		UPDATE_TAXI_OJ_TIP(myTaxiData,iTipIndex)
		
		//Dialogue & Objectives
		IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION
					
			IF NOT IS_TAXI_EMERGENCY_FAIL_SET(myTaxiData)
				TRIGGER_TAXI_QUEUE_NE_LINES()
			ELSE
				TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",GET_TAXI_EMERGENCY_FAIL_STRING(myTaxiData))
			ENDIF			

		ENDIF
		
				
		IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
			IF myTaxiData.bObjPrinted
				//once the player gets near the mountain, add leeway
				IF ( NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				AND (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff)) > 450 )
					HANDLE_TAXI_EXCITEMENT(myTaxiData,FALSE)
				ELSE
					HANDLE_TAXI_EXCITEMENT(myTaxiData,FALSE,TRUE)
				ENDIF
			ENDIF
#IF IS_DEBUG_BUILD
		ELIF bDebugTurnOnFreeRide
			HANDLE_TAXI_EXCITEMENT(myTaxiData,TRUE)
			IF NOT bFreeRideisOn
				TAXI_OJ_NEX_SET_TIP_AND_EXCITEMENT_TO_CHECK()
				bFreeRideisOn = TRUE
			ENDIF
#ENDIF
		ENDIF
		

		//================================================================================================
	
		
		SWITCH myTaxiData.tTaxiOJ_RideState
			
/*PRESTREAM and Check that the player and taxi are good to go----------------------------------------
						
ooooo oooo   oooo ooooo ooooooooooo 
 888   8888o  88   888  88  888  88 
 888   88 888o88   888      888     
 888   88   8888   888      888     
o888o o88o    88  o888o    o888o    
*/
			CASE TRS_INIT_STREAM
				//Initialize bonuses
				TAXI_INITIALIZE_BONUS_FIELD(bonusFieldNeedExcitement[TNEX_BONUS_EXTREME], "TAXI_SC_BN_02",TAXI_CONST_BONUS_CASH_DAREDEVIL)
				TAXI_INITIALIZE_BONUS_INFO(myTaxiData, bonusFieldNeedExcitement)
				
				//Request of our assets
				INIT_TAXI_STREAMS()
				
				TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
				
				TAXI_SPAWN_PASSENGER(myTaxiData, vPassengerPt, vPassengerPickupPt,"TaxiFelipe",mPassengerModel,256,30.0)
				TAXI_INIT_PASSENGER_BLIP(myTaxiData)
					
				//Move on to the next stage
				TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_STREAMING)
				
			BREAK
			
			//Wait for streaming to finish--------------------------------------------------------------
			CASE TRS_STREAMING

				IF TAXI_ASSETS_STREAMED()
					
					INIT_ALL_TAXI_EXCITEMENT_VALUES()
					
					INITIALIZE_GENERIC_TAXI_EXCEPTIONS()
					
					CLEAR_ALL_TAXI_PASSENGER_REACT_BITS(myTaxiData)
					
					myTaxiData.vTaxiOJPickup = vPassengerPt
					
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPAWNING)

				ENDIF
	
			BREAK
			
			//Spawn Passenger---------------------------------------------------------------------------
			CASE TRS_SPAWNING
				IF PROPERTY_VIP_INIT_READY(myTaxiData)
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
						SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger, PCF_GetOutUndriveableVehicle, FALSE)
						
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_TORSO, 1, 1)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_LEG, 1, 2)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_HEAD, 0,1)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_JBIB, 1,0)	//Remove Headphones per B*1460010
					ENDIF
					//Give player obj to go pickup Passenger					
					ENABLE_TAXI_SPEECH(myTaxiData)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
				ENDIF
			BREAK
			
			//Waits for passenger to head to cab---------------------------------------------------------
			CASE TRS_MANAGE_PICKUP
				IF NOT IS_ENTITY_IN_ANGLED_AREA( myTaxiData.viTaxi, <<-484.921753,-370.123108,33.408054>>, <<-517.908936,-362.789948,37.313259>>, 28.75)
				AND NOT IS_ENTITY_IN_ANGLED_AREA( myTaxiData.viTaxi, <<-472.058838,-321.036041,33.353264>>, <<-485.478973,-285.421692,37.492317>>, 28.75)
					IF TAXI_HANDLE_IV_PICKUP_NO_METER(myTaxiData, FALSE, 30)//, FALSE, 18)
						
						//Move on to the next stage
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_PARK)
					ENDIF
				ENDIF
			BREAK
			
			CASE TRS_WAIT_PARK
				IF IS_PASSENGER_ENTERING_TAXI(myTaxiData)
					
					myTaxiData.vTaxiOJDropoff = vDropOff
					bDropOffFound = TRUE

					//Greet the player
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GREET,TRUE)
			
					//CleanUp Pickup Lock & POI
					CLEANUP_TAXI_PICKUP_STOP(myTaxiData)
					
					//Set Vehicle Health
					myTaxiData.iOldVehicleHealth =  GET_ENTITY_HEALTH(myTaxiData.viTaxi)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_FOR_TIME)
					
				ENDIF
				
				// check to see if player dived, if so send back to prev state
				IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
					IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
						TAXI_HANDLE_PLAYER_DIVE(myTaxiData)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
					ENDIF
				ENDIF
			BREAK
			//This state simply waits for the obj to be given before drawing the countdown timer.
			CASE TRS_WAIT_FOR_TIME
				IF bDropOffFound

					TAXI_OJ_NEX_SET_TIP_AND_EXCITEMENT_TO_CHECK()
					
				//IF bStartTiming
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DRIVING_PASSENGER)
				ENDIF
			BREAK			
			
			CASE TRS_DRIVING_PASSENGER
				
				IF (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff)) < 875
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_OFFROAD_CHALLENGE
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT bOffroadChallenged
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI-NE] offroad challenge issued")
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_OFFROAD)
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_OFFROAD_CHALLENGE, TRUE, FALSE,TRUE)
					SETTIMERA(0)
					bOffroadChallenged = TRUE
				ENDIF
				
				//Set Radio Station Check
				IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_BANTER)
					IF NOT GET_TAXI_RADIO_CHECK_FLAG(myTaxiData)	
						//TAXI_RADIO_STATION_TURN_ON(myTaxiData)
						//SCRIPT_ASSERT("The Taxi Radio Should Be Updating")
					ENDIF
				ENDIF
				
				IF TAXI_HANDLE_EXCITEMENT_DRIVING_DEUX(myTaxiData/*,tTaxiOJ_DQ_Data*/,9,7.5)					
					//Remove destination blip
					REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
					
					KILL_ANY_CONVERSATION()
					CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data, default, TRUE)
					
					// Tip is now decided using the bonus item system.  No more tiers... for now.
					IF myTaxiData.iTaxiOJ_PassengerExcitement >= TAXI_CONST_EXCITEMENT_LEVEL_HI
						TAXI_SET_BONUS_AWARD(myTaxiData, ENUM_TO_INT(TNEX_BONUS_EXTREME))
					ENDIF
					
					//CASH $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
					SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)
					TAXI_OJ_RATE_OVERALL_TIP_LEVEL(myTaxiData)
					
					//If you sucked he's gonna run on you
				
					//IF myTaxiData.iTaxiOJ_PassengerExcitement < TAXI_CONST_EXCITEMENT_LEVEL_MED
					IF myTaxiData.iTaxiOJ_CashTip < myTaxiData.iTaxiOJ_CashTipAvg
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPECIAL_ENDING)
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI-NE] look task given")
						TASK_LOOK_AT_ENTITY(myTaxiData.piTaxiPassenger, PLAYER_PED_ID(), -1, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), myTaxiData.piTaxiPassenger, -1, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
					ENDIF
					
					CONVERT_TAXI_TIP_TO_CASH(myTaxiData)
				ENDIF
			BREAK
			
			//Regular Payment---------------------------------------------------------------------------
			CASE TRS_REGULAR_PAYMENT
				//Wait til passenger is done speaking/paying than tell him to leave vehicle
				IF HAS_TAXI_OJ_PASSENGER_BEEN_DROPPED_OFF(myTaxiData)
					
					TASK_CLEAR_LOOK_AT(myTaxiData.piTaxiPassenger)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					
					TAXI_OJ_TASK_PASSENGER_DROPOFF(myTaxiData)
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)				
		
				ENDIF			

			BREAK
			
			//Passenger attempts to run out-------------------------------------------------------------
			CASE TRS_SPECIAL_ENDING
				IF myRunState = TPRS_RUNNING
				AND NOT TAXI_HAS_PASSENGER_ESCAPED(myTaxiData.piTaxiPlayer, myTaxiData.piTaxiPassenger)
					IF NOT IS_TIMER_STARTED(shoutDelay)
						RESTART_TIMER_NOW(shoutDelay)
					ELIF GET_TIMER_IN_SECONDS(shoutDelay) > 5.0
						SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_RUNOFF_SHOUTS, TRUE, FALSE, TRUE)
						RESTART_TIMER_NOW(shoutDelay)
					ENDIF
				ENDIF
				
				IF TAXI_SET_PED_RUNNING(myRunState, myTaxiData, myTaxiData.piTaxiPassenger, taxiMoney,TRUE)	
					IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
						//SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_MAKE_PAY_END, TRUE, FALSE, TRUE)
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, "txm2_runoff", CONV_PRIORITY_VERY_HIGH)
					ENDIF
					
					//State++
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)
				ENDIF
			BREAK
			
			//Pop up the scorecard----------------------------------------------------------------------
			CASE TRS_SCORECARD_GRADE
				IF TAXI_CALC_SCORECARD(myTaxiData,TaxiMidSize)
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						TAXI_MISSION_END(TRUE,myTaxiData)
						
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CLEANUP)
					ENDIF
					
				ENDIF
			
			BREAK
			
			//Clean everything up
			CASE TRS_CLEANUP
				Script_Cleanup()
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC
SCRIPT

	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		Script_Cleanup()
	ENDIF
	
	// Any initialisation (generally, only mission scripts should set
	// the mission flag to TRUE)
	SET_MISSION_FLAG(TRUE)
	
	INITIALIZE_SCRIPT_VARIABLES()
	
	// The script loop
	WHILE (TRUE)
		// Maintain the script – perform per-frame functionality

	//All skips and debug shortcuts-------------------------------
	#IF IS_DEBUG_BUILD	
		PROCESS_TAXI_DEBUG_SKIP(myTaxiData,tDebugState)
		
		// Debug Key: Check for Pass (not for Minigmes)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			TAXI_ODDJOB_DEBUG_SKIP_TO_SCORECARD(myTaxiData)
		ENDIF

		// Debug Key: Check for Fail (not for Minigames)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			TAXI_DEBUG_FAIL_TRIGGERED(myTaxiData)
			Script_Failed()
		ENDIF
		
		// Debug Key: set vehicle health to 0.0
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
			IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				SET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi, 0.0)
			ENDIF
		ENDIF		
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Q))
			IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				SET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi, 50.0)
			ENDIF
		ENDIF		
		
		PROCESS_WIDGETS()

	#ENDIF
	//END DEBUG----------------------------------------------------
		
		IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPlayer)
			Main_Taxi_OJ_NeedsExcitement()
		ELSE
			REASSIGN_TAXI_OJ_DRIVER(myTaxiData)
		ENDIF
		
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

