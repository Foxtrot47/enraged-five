//=======================================================================================================================================

// Taxi_TakeItEasy.sc
// Dev : John R. Diaz
/*
	•	“Take it easy.” Sick or hung over passenger requests a stable ride without too many bumps. (However still as fast as possible.)
*/

//CHANGELOG==========================================================================================================
//9/27/11 - Reformatting/prettifying all taxi oj scripts
//===================================================================================================================


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.



//Includes------------------------------------------------------
USING "Taxi_Includes.sch"

//Enums---------------------------------------------------------

ENUM TAXI_RIDER_STATUS
	TAXI_RIDERSTATUS_NORMAL,
	TAXI_RIDERSTATUS_PUKE
ENDENUM

//Local Variables-----------------------------------------------

TAXI_PED_RUN_STATE 				myRunState = TPRS_INIT

TaxiStruct 						myTaxiData

TAXI_MONEY_STRUCT 				taxiMoney

TAXI_RIDER_STATUS				eTaxiRiderStatus = TAXI_RIDERSTATUS_NORMAL

ENUM TAXIOJ_TIE_BONUS
	TTIE_BONUS_NO_PUKE = 0,
	TTIE_BONUS_TOTAL
				
ENDENUM

CONST_INT						TAXI_CONST_BONUS_CASH_NO_PUKE		100
CONST_INT						TAXI_CONST_TIE_BAD_TIME				155

BONUS_FIELD						bonusFieldTakeItEasy[TTIE_BONUS_TOTAL]

MODEL_NAMES 					mPassengerModel = A_M_Y_SouCent_04

PTFX_ID							ptSpew

//Vectors
//Willie's Pharmacy
VECTOR 							vDropOff = <<-1230.55042, -896.78137, 11.11059>> //<< -1231.7030, -896.4080, 11.1530 >>
VECTOR 							vPassengerPt = << 58.8213, 293.8480, 109.6124 >>//ATOM BURGER//<< -173.7200, -360.1367, 32.0002 >>
VECTOR 							vPassengerPickupPt = << 62.7742, 307.4984, 109.9810 >>//<< -171.5603, -362.0354, 31.8624 >>

//Puking vectors
VECTOR							vPukePos = <<0, 0, 0>> //<<0.0, 0.13, 0.03>>
VECTOR							vPukeRot = <<0, 0, 0>>

//Ints
INT 							iDebugThrottle = 1
INT 							iSickStage = 0
INT								iTipIndex = 0
INT								iExcitementState = 0
//Floats
FLOAT 							fRelentlessTaxiTime = 0.0
FLOAT 							fVomitStopBonus = 12.0// the amount of time player must slow down while passenger is puking
FLOAT 							fVomitStopTime
//Bools
BOOL 							bVomiting, bVomiting1, bVomiting2
BOOL 							bVomitDone = FALSE
BOOL							bStoppedForVomit
BOOL							bTriggerPlayerReactionLine
BOOL							bSeatShuffled

STRING							strPukeClip
STRING							strPukeDict = "random@drunk_driver_1" //"oddjobs@taxi@tie"

//DialogueQ Info
BOOL							g_bDebug = FALSE
TAXI_OJ_DIALOGUE_Q_DATA			tTaxiOJ_DQ_Data								
TAXI_OJ_DQ_CONVERSATION_LINE	tDialogueLine[CONST_TAXIOJ_SIZE_Q]

AGGRO_ARGS 						aggroArgs 

SCRIPT_SHARD_BIG_MESSAGE TaxiMidSize


CONST_INT						NUM_PUKE_WARNINGS 2
CONST_INT						NUM_FAIL_WARNINGS 5
CONST_INT						NUM_FAIL_WARNINGS_CLOSE_TO_DROPOFF	7
CONST_INT						NO_VOMIT_CASH_TIP 18
//Debug---------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD


	TEXT_LABEL_63	sDebugString[5]
	WIDGET_GROUP_ID 			taxiRideWidgets, ojTaxiWidgets_Excitement
	BOOL bDebugTurnOnFreeRide = FALSE
	BOOL bMakePassengerPuke
	BOOL bDrawVehicleHealth
	TXM_DEBUG_SKIP_STATES 		tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS
		
#ENDIF


//FUNCTIONS------------------------------------------------------------------------------------------------------------

PROC Script_Cleanup()
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(taxiRideWidgets)
			DELETE_WIDGET_GROUP(taxiRideWidgets)
		ENDIF
		
		IF DOES_WIDGET_GROUP_EXIST(ojTaxiWidgets_Excitement)
			DELETE_WIDGET_GROUP(ojTaxiWidgets_Excitement)
		ENDIF
		
		CLEANUP_TAXI_WIDGETS()
	#ENDIF
	
	//LM gotta dump those anim dicts and PTFX now
	IF HAS_ANIM_DICT_LOADED(strPukeDict)
		REMOVE_ANIM_DICT(strPukeDict)
	ENDIF
	
	IF (ptSpew <> NULL)
	    STOP_PARTICLE_FX_LOOPED(ptSpew)
	    ptSpew = NULL
	ENDIF
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	TERMINATE_THIS_THREAD()
ENDPROC

// Perform any special commands if the script fails
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Failed()
	TAXI_SCRIPT_FAILED(myTaxiData)
	Script_Cleanup()
ENDPROC

PROC INITIALIZE_SCRIPT_VARIABLES()
	
	TAXI_ODDJOB_GLOBAL_SETUP(myTaxiData,TXM_02_TAKEITEASY)
		
	// Set our initial state up. VIP missions don't start at TRS_FINDING_LOCATION.
	myTaxiData.tTaxiOJ_RideState = TRS_INIT_STREAM
	
	SET_TAXI_TIP_CUTOFFS(myTaxiData,2,4)
	
	myTaxiData.vTaxiOJ_WarpPtPickup = << 31.3927, 229.9863, 108.4500 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingPickup = 340.5
	
	myTaxiData.vTaxiOJ_WarpPtDropoff = << -1239.9924, -902.0005, 10.8342 >>//<< -1144.8926, -1380.4265, 3.9352 >>//<< -1165.6697, -1594.5378, 3.3168 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 305.2155//27.9
	
	//TODO : Set the POI
	myTaxiData.vTaxiOJ_PassengerGoToPt = << -1224.0927, -907.3411, 11.3263 >>
	
	#IF IS_DEBUG_BUILD
		#IF DEBUG_iTurnOnAllDXDebug
			ENABLE_ALL_DIALOGUE_DEBUG()
		#ENDIF
		
		taxiRideWidgets = START_WIDGET_GROUP("Taxi Ride - Take it easy")
			INIT_ODDJOB_TAXI_WIDGETS()
			
			//Excitement Widget
			ojTaxiWidgets_Excitement = START_WIDGET_GROUP("Excitement")
				ADD_WIDGET_STRING("Excitement Ride Test")
					ADD_WIDGET_BOOL("Turn On Free Ride", bDebugTurnOnFreeRide)
					ADD_WIDGET_BOOL("Turn On Vehicle Health",bDrawVehicleHealth)
					
				ADD_WIDGET_STRING("Puking Pos")
					ADD_WIDGET_FLOAT_SLIDER("Puke Pos X", vPukePos.x, -2000.0, 2000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Puke Pos Y", vPukePos.y, -2000.0, 2000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Puke Pos Z", vPukePos.z, -2000.0, 2000.0, 0.001)
				ADD_WIDGET_STRING("Puking Rot")	
					ADD_WIDGET_FLOAT_SLIDER("Puke Rot X", vPukeRot.x, -2000.0, 2000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Puke Rot Y", vPukeRot.y, -2000.0, 2000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Puke Rot Z", vPukeRot.z, -2000.0, 2000.0, 0.001)
					
				ADD_WIDGET_STRING("bMakePassengerPuke")
					ADD_WIDGET_BOOL("bMakePassengerPuke", bMakePassengerPuke)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		INIT_TAXI_WIDGETS(taxiRideWidgets)
	#ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~~~~~~~~~~~~ Oddjobs | Taxi | Take It Easy ~~~~~~~~~~~~~~~~~----------")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")
	
ENDPROC
#IF IS_DEBUG_BUILD
PROC PROCESS_WIDGETS()

	UPDATE_TAXI_WIDGETS(myTaxiData,tTaxiOJ_DQ_Data)
			
	IF bDrawVehicleHealth
		INT iRed 
		INT iBlue
		INT iGreen
		
		sDebugString[0] = "Health = "
		sDebugString[0] += TAXI_UTILS_GET_STRING_FROM_INT_SP(GET_ENTITY_HEALTH(myTaxiData.viTaxi))
		DRAW_DEBUG_TEXT_2D(sDebugString[0], <<0.8, 0.4,0.0>>)
		
		IF myTaxiData.iTaxiOJ_PassengerExcitement <= TAXI_CONST_EXCITEMENT_LEVEL_LO
		  iRed= 0
		  iBlue = 255
		ELIF myTaxiData.iTaxiOJ_PassengerExcitement > TAXI_CONST_EXCITEMENT_LEVEL_LO
		AND myTaxiData.iTaxiOJ_PassengerExcitement <= TAXI_CONST_EXCITEMENT_LEVEL_MED
			  iRed= 0
			  iGreen = 255
			  iBlue = 0
		ELSE
			iRed= 255
			iGreen = 0
			iBlue = 0
		ENDIF
		sDebugString[1] = "ExciteMNT = "
		sDebugString[1] += TAXI_UTILS_GET_STRING_FROM_INT_SP(myTaxiData.iTaxiOJ_PassengerExcitement)
		DRAW_DEBUG_TEXT_2D(sDebugString[1], <<0.8, 0.42,0.0>>,iRed,iGreen,iBlue)
		
		sDebugString[3] = "NumWarnings = "
		sDebugString[3] += TAXI_UTILS_GET_STRING_FROM_INT_SP(GET_TAXI_TIE_NUM_WARNINGS(myTaxiData))
		DRAW_DEBUG_TEXT_2D(sDebugString[3], <<0.8, 0.44,0.0>>,iRed,iGreen,iBlue)
		
		sDebugString[2] = "Horn Honks = "
		sDebugString[2] += TAXI_UTILS_GET_STRING_FROM_INT_SP(myTaxiData.iTaxiOJ_HornHonkCount)
		DRAW_DEBUG_TEXT_2D(sDebugString[2], <<0.8, 0.46,0.0>>)
	ENDIF
	
ENDPROC
#ENDIF



/// PURPOSE: Request all our mission specific assets here
///  
PROC INIT_TAXI_STREAMS()
	//Load text and UI
	REQUEST_MODEL(mPassengerModel)
	REQUEST_ANIM_DICT(strPukeDict) //("oddjobs@taxi@tie")
	REQUEST_SCRIPT_AUDIO_BANK("Taxi_Vomit")
	TaxiMidSize.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	REQUEST_PTFX_ASSET()
	
	TAXI_INIT_SHARED_STREAMS()
	CDEBUG1LN(DEBUG_OJ_TAXI,"INIT_TAXI_STREAMS - SUCCESS")
ENDPROC

//// PURPOSE: Waits for our assets to load
 ///    
 /// RETURNS: TRUE if everything has loaded correctly FALSE if there was a problem
 ///    

FUNC BOOL TAXI_ASSETS_STREAMED()
	
	IF NOT HAS_MODEL_LOADED(mPassengerModel)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading A_M_Y_SouCent_01",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	/*
	//5/8/2012 JD - Taking a risk here at things not being loaded when you need them, but I figure from when 
	//the mission launches to when you pick up the passenger this should have enough time to load.
	
	IF NOT HAS_ANIM_DICT_LOADED("oddjobs@taxi@tie")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading oddjobs@taxi@tie",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_PTFX_ASSET_LOADED()
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading PTFX",iDebugThrottle)
		RETURN FALSE
	ENDIF
	*/
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(TaxiMidSize.siMovie)
		#IF IS_DEBUG_BUILD
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TaxiMidSize.siMovie",iDebugThrottle)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	IF NOT TAXI_SHARED_ASSETS_STREAMED(iDebugThrottle)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading shared assets",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_ASSETS_STREAMED - SUCCESS")
	RETURN TRUE		
ENDFUNC

FUNC BOOL TAXI_MAKE_PASSENGER_PUKE(PED_INDEX & piSpewer)
	FLOAT fSpewStartTime
	CONST_FLOAT fSTART_VOMITING_TIME0	0.243	
	CONST_FLOAT fSTOP_VOMITING_TIME0	0.280
	CONST_FLOAT fSTART_VOMITING_TIME1	0.295	
	CONST_FLOAT fSTOP_VOMITING_TIME1	0.370//0.3239
	CONST_FLOAT fSTART_VOMITING_TIME2	0.487857	
	CONST_FLOAT fSTOP_VOMITING_TIME2	0.55//0.494326	
	
	
	
	IF NOT IS_PED_INJURED(piSpewer)
		SWITCH iSickStage
			CASE 0
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, "txm1aud", "txm1_Puke1", CONV_PRIORITY_HIGH)
						TASK_PLAY_ANIM(piSpewer, strPukeDict, strPukeClip, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
						IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							SWITCH GET_SEAT_PED_IS_IN(piSpewer)
								CASE VS_FRONT_RIGHT
									REMOVE_VEHICLE_WINDOW(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), SC_WINDOW_FRONT_RIGHT)
									CDEBUG1LN(DEBUG_OJ_TAXI,"removed front right window")
								BREAK
								CASE VS_BACK_RIGHT
									REMOVE_VEHICLE_WINDOW(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), SC_WINDOW_REAR_RIGHT)
									CDEBUG1LN(DEBUG_OJ_TAXI,"removed rear right window")
								BREAK
								DEFAULT
									REMOVE_VEHICLE_WINDOW(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), SC_WINDOW_REAR_RIGHT)
									CDEBUG1LN(DEBUG_OJ_TAXI,"DIDN'T GET BACK RIGHT OR FRONT RIGHT SEAT. just removing rear right window")
								BREAK
							ENDSWITCH
						ENDIF
						IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_BORING)
							TAXI_CANCEL_TIMERS(myTaxiData, TT_BORING)
							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_MAKE_PASSENGER_PUKE: cancel TT_BORING timer before puking")
						ENDIF						
						CDEBUG1LN(DEBUG_OJ_TAXI,"Passenger is about to puke")
						iSickStage ++
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"Puke conversation not creating")
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_OJ_TAXI,"There is some conversation playing")
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF IS_ENTITY_PLAYING_ANIM(piSpewer, strPukeDict, strPukeClip)     
					fSpewStartTime = GET_ENTITY_ANIM_CURRENT_TIME(piSpewer,strPukeDict, strPukeClip)
					IF g_bDebug
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_MAKE_PASSENGER_PUKE: fSpewStartTime = ", fSpewStartTime)
					ENDIF
					
					IF ((fSpewStartTime > fSTART_VOMITING_TIME0) AND (fSpewStartTime < fSTOP_VOMITING_TIME0))
						IF NOT bVomiting
							START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_puke_in_car", piSpewer, vPukePos, vPukeRot, BONETAG_HEAD)
							bVomiting = TRUE
						ENDIF
					ELIF ((fSpewStartTime > fSTART_VOMITING_TIME1) AND (fSpewStartTime < fSTOP_VOMITING_TIME1))
						IF NOT bVomiting1
							/*ptSpray = */START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_puke_in_car", piSpewer, vPukePos, vPukeRot, BONETAG_HEAD)
							bVomiting1 = TRUE
						ENDIF
						
						
					ELIF ((fSpewStartTime > fSTART_VOMITING_TIME2) AND (fSpewStartTime < fSTOP_VOMITING_TIME2))
						IF NOT bVomiting2
							START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_puke_in_car", piSpewer, vPukePos, vPukeRot, BONETAG_HEAD)
							bVomiting2 = TRUE
							
							IF NOT bTriggerPlayerReactionLine
								
								//There is a reason the dialogue manager is not on during the puke, so I have to explicity create_conversation for now
								
								//SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_PUKE_PLAYER_REACT,TRUE)
								//TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
								
								TEXT_LABEL_23 tlPukeReaction
								tlPukeReaction = "txm1_pukeR1"
								TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(tlPukeReaction)

								CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, "txm1aud", tlPukeReaction, CONV_PRIORITY_HIGH)
								bTriggerPlayerReactionLine = TRUE
								CDEBUG1LN(DEBUG_OJ_TAXI,"Puke line should fire")
							ENDIF
						ENDIF
					ENDIF
					
					IF GET_ENTITY_ANIM_CURRENT_TIME(piSpewer,strPukeDict, strPukeClip) > 0.9
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"CASE 1 GETTING EXITED")
						iSickStage ++
					ENDIF
				ELSE
					
//					IF (ptSpew <> NULL)
//						STOP_PARTICLE_FX_LOOPED(ptSpew)
//						
//						ptSpew = NULL
//					ENDIF
//					
//					CDEBUG1LN(DEBUG_OJ_TAXI,"ANIM WAS INTERRUPTED, going to iSickStage 2")
//					iSickStage ++
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_ENTITY_PLAYING_ANIM(piSpewer, strPukeDict, strPukeClip)
//					IF (ptSpew <> NULL)
//						STOP_PARTICLE_FX_LOOPED(ptSpew)
//						ptSpew = NULL
//			    	ENDIF
				ENDIF
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"CASE 2 GOT HIT")
				
				TASK_PLAY_ANIM(piSpewer, strPukeDict, "drunk_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				bVomiting = FALSE
				iSickStage ++
			BREAK

			CASE 3
				bVomitDone = TRUE
				IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_BORING)
					TAXI_RESET_TIMERS(myTaxiData, TT_BORING)
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_MAKE_PASSENGER_PUKE: Start TT_BORING timer after puking")
				ENDIF	
				RETURN TRUE
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

// monitors taxi speed while the passenger is puking
// if the passenger is stopped for long enough, the player will get a bonus
FUNC BOOL TAXI_MONITOR_PUKE_BONUS()
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
	
		IF bVomiting
		AND GET_ENTITY_SPEED(myTaxiData.viTaxi) < 5.0
			IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_PUKING)
				CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi stopped.  Resetting timer TT_PUKING")
				TAXI_RESET_TIMERS(myTaxiData, TT_PUKING)
			ENDIF
		ENDIF
		
		IF GET_ENTITY_SPEED(myTaxiData.viTaxi) >= 5.0
		AND IS_TAXI_TIMER_STARTED(myTaxiData, TT_PUKING)
			CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi not stopped.  Cancelling timer TT_PUKING")
			TAXI_CANCEL_TIMERS(myTaxiData, TT_PUKING)
		ENDIF
	
	ENDIF
	
	if bVomitDone
		TAXI_PAUSE_TIMER(myTaxiData, TT_PUKING)
		fVomitStopTime = GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_PUKING)
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"*************  fVomitStopTime = ", fVomitStopTime)
		
		IF fVomitStopTime >= fVomitStopBonus
			// give bonus
			// set flag for conversation
			
			//TAXI_SET_BONUS(myTaxiData, TAXI_BONUS_SAVED_LIFE, 20)		//removing bonus, B* 290232 --jsripan
			
			bStoppedForVomit = TRUE
			
			RETURN TRUE
		ELSE
			bStoppedForVomit = FALSE
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC TAXI_OJ_TIE_SET_TIPS_AND_EXCITMENT_TO_CHECK()
	
	//Tip Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POS_DELIVERY_TIME)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_HIT_PED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_TOOK_DAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ROLL_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LEFT_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_STOPPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_LIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_DISLIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_NOPUKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_PUKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_PUKE_NO_STOP)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_PUKE_STOP)
	
	//Tips for Excitement
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_BORED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_SPEEDING)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_AIR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_QUICKSTOP)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POWERSLIDING)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ONCOMING)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_SIDEWALK)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_OFFROAD)
	
	//This one is important for THIS mission
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_NEG_EXCITEMENT)
	
	//Excitement Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_SPEEDING)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_TOOKDAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_WRONGLANE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_SIDEWALK)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_ROLL)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_HITPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_AIR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_SWERVE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits,TAXI_DF_REVERSE)

	//Turn off aggro bits
	CLEAR_BIT(aggroArgs.iBitFieldDontCheck, ENUM_TO_INT(EAggro_Wanted))
	
	//Start the mission timer here
	TAXI_START_TIMER(myTaxiData, TT_RIDETODEST)
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DL_SET_TIPS_TO_CHECK set tips to check on")
ENDPROC

FUNC BOOL TAXI_HANDLE_RELENTLESS_EXCITEMENT_DEUX()
	FLOAT fCurrentTaxiTime
	INT iCurrentNumWarnings
	
	fCurrentTaxiTime = TO_FLOAT(GET_GAME_TIMER()) 
	
	iCurrentNumWarnings = GET_TAXI_TIE_NUM_WARNINGS(myTaxiData)
	
	// check every 15 seconds
	IF ( fCurrentTaxiTime - fRelentlessTaxiTime) > 5.0 //15.0
		//CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI-TIE] Current number of warnings = ", iCurrentNumWarnings)
		
		// reset the values
		fRelentlessTaxiTime = fCurrentTaxiTime	
		
		IF iExcitementState = 0
			//If your within TAXI_DISTANCE_TO_IGNORE_EXCITEMENT to dropoff change the fail criteria
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff) < TAXI_DISTANCE_TO_IGNORE_EXCITEMENT
				iExcitementState++
			ENDIF			
			
			IF iCurrentNumWarnings >= NUM_FAIL_WARNINGS
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI-TIE] ******************* Relentlessness caused the fail*******************")
					RETURN TRUE
				ELSE
					KILL_ANY_CONVERSATION()
					fRelentlessTaxiTime = 0	
				ENDIF
			ENDIF
		ELSE
			IF iCurrentNumWarnings >= NUM_FAIL_WARNINGS_CLOSE_TO_DROPOFF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI-TIE] ******************* Relentlessness caused the fail near the dropoff *******************")
					RETURN TRUE
				ELSE
					KILL_ANY_CONVERSATION()
					fRelentlessTaxiTime = 0
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_PROGRESS()
	IF EVALUATE_OVERALL_TIME(myTaxiData, TAXI_DI_TIME_BAD, TAXI_CONST_TIE_BAD_TIME)
		CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PROGRESS: ride time > overall time!")
	ENDIF
ENDPROC
PROC TAXI_TIE_OJ_RATE_OVERALL_TIP_LEVEL(BOOL bNoPuke = FALSE)
	
	IF myTaxiData.iTaxiOJ_CashTip < myTaxiData.iTaxiOJ_CashTipAvg
		SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_RATE_TIP_ASS,TRUE)
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_RATE_OVERALL_TIP_LEVEL: myTaxiData.iTaxiOJ_CashTip = ",myTaxiData.iTaxiOJ_CashTip, "myTaxiData.iTaxiOJ_CashTipAvg = ", myTaxiData.iTaxiOJ_CashTipAvg)
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_RATE_OVERALL_TIP_LEVEL: TAXI_DI_RATE_TIP_ASS")
	ELIF myTaxiData.iTaxiOJ_CashTip >= myTaxiData.iTaxiOJ_CashTipAmazing
		IF NOT bNoPuke
			SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_RATE_TIP_AMAZING,TRUE)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_RATE_OVERALL_TIP_LEVEL: TAXI_DI_RATE_TIP_AMAZING")
		ELSE
			SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_NOPUKE2)
			SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_DIDNT_PUKE,TRUE)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_RATE_OVERALL_TIP_LEVEL: TAXI_DI_RATE_TIP_AMAZING, NO PUKE")
		ENDIF
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_RATE_OVERALL_TIP_LEVEL: myTaxiData.iTaxiOJ_CashTip = ",myTaxiData.iTaxiOJ_CashTip, "myTaxiData.iTaxiOJ_CashTipAvg = ", myTaxiData.iTaxiOJ_CashTipAvg)
	ELSE
		IF NOT bNoPuke
			SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_RATE_TIP_AVERAGE,TRUE)
		ELSE
			SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_NOPUKE1)
			SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_DIDNT_PUKE,TRUE)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_RATE_OVERALL_TIP_LEVEL: TAXI_DI_RATE_TIP_AVERAGE, NO PUKE")
		ENDIF
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_RATE_OVERALL_TIP_LEVEL: myTaxiData.iTaxiOJ_CashTip = ",myTaxiData.iTaxiOJ_CashTip, "myTaxiData.iTaxiOJ_CashTipAvg = ", myTaxiData.iTaxiOJ_CashTipAvg)
	ENDIF
	
	
ENDPROC

/*
ooooooooo  ooooo      o      ooooo         ooooooo     ooooooo8  
 888    88o 888      888      888        o888   888o o888    88  
 888    888 888     8  88     888        888     888 888    oooo 
 888    888 888    8oooo88    888      o 888o   o888 888o    88  
o888ooo88  o888o o88o  o888o o888ooooo88   88ooo88    888ooo888  
*/
PROC TRIGGER_TAXI_QUEUE_TIE_LINES()

	//SET_TAXI_OJ_INTERRUPT_TIMER_OFF(myTaxiData)

	//Trigger lines
	IF IS_BANTER_SAFE_TO_PLAY(myTaxiData,tTaxiOJ_DQ_Data)
		
		SWITCH tTaxiOJ_DQ_Data.iCurrentDQLine
			//Make sure if player interrupts the initial line, that the objective prints anyway.
			CASE 0
				IF myTaxiData.tTaxiOJ_RideState =  TRS_DRIVING_PASSENGER
					
					IF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
					
						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_TIE_DO")
						OR DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
							CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO J Store has been  assigned")
							tTaxiOJ_DQ_Data.iCurrentDQLine++
							
						ELSE
							IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_OBJ_GIVE_MAIN
							
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE,FALSE,TRUE)
								CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO J Store has been REassigned")

							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 1
				IF NOT bVomitDone
					IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
					AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER,TRUE)
						TAXI_RESET_TIMERS(myTaxiData, TT_BORING)
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter 1")
						ENDIF
						
					ENDIF
				ELSE
					tTaxiOJ_DQ_Data.iCurrentDQLine++
				ENDIF
			BREAK
		
			CASE 2
				IF bVomitDone
				AND eTaxiRiderStatus = TAXI_RIDERSTATUS_NORMAL
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
					AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_2
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_2,TRUE)
						TAXI_RESET_TIMERS(myTaxiData, TT_BORING)
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter 1")
						ENDIF
						
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
//				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > GET_RANDOM_FLOAT_IN_RANGE(8.0,14.0)
//					//Set Radio Station Check
//					IF NOT GET_TAXI_RADIO_CHECK_FLAG(myTaxiData)	
//						TAXI_RADIO_STATION_TURN_ON(myTaxiData)
//						tTaxiOJ_DQ_Data.iCurrentDQLine++
//						IF g_bDebug
//							SCRIPT_ASSERT("The Taxi Radio Should Be Updating")
//						ENDIF
//					ENDIF
//				ENDIF
			BREAK
		ENDSWITCH				
	ENDIF				
	
	//PROCESS_IMPORTANT_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, g_bDebug)
	PROCESS_MANUAL_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, 4, g_bDebug)
ENDPROC


PROC Main_Taxi_OJ_TakeItEasy()
		
	//Handles Fail Or No Taxi-------------------------------------------------------------------------------
	IF IS_TAXI_JOB_IN_FAIL_STATE(myTaxiData)
		TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
		
		IF myTaxiData.tTaxiOJ_RideState > TRS_INIT_STREAM
			IF (GET_GAME_TIMER() % 1000) < 50
				CDEBUG1LN(DEBUG_OJ_TAXI,"Main_Taxi_OJ_TakeItEasy: (Fail state) myTaxiData.tTaxiOJ_RideState > TRS_INIT_STREAM")
			ENDIF
			
			IF TAXI_HANDLE_FAIL(myTaxiData)
				Script_Failed()
			ENDIF
		ELSE
			
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CLEANUP) > 5.0
				SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
				CDEBUG1LN(DEBUG_OJ_TAXI,"Fail During State: "," ", myTaxiData.sTaxiOJ_RideState, " ", myTaxiData.sTaxiOJ_Reason4Fail)
				Script_Cleanup()
			
			ENDIF
		ENDIF
	
	//Proceed
	ELSE
		//Run Throughtout the Entire Mission--------------------------------------------------------------
		TAXI_OJ_MAINTAIN_GLOBAL_GATES(myTaxiData)
		
		//Gobal taxi updates that all taxi missions run every frame
		RUN_GLOBAL_TAXI_UPDATES(myTaxiData,aggroArgs)
		
		UPDATE_TAXI_OJ_TIP(myTaxiData,iTipIndex)
		
		PROCESS_TAXI_EXCEPTIONS(myTaxiData)
		
		//Dialogue & Objectives
		IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION
		AND eTaxiRiderStatus = TAXI_RIDERSTATUS_NORMAL
			//TAXI_DIALOGUE_OBJ_MANAGER(myTaxiData,locatesData)			
			
			IF NOT IS_TAXI_EMERGENCY_FAIL_SET(myTaxiData)
				TRIGGER_TAXI_QUEUE_TIE_LINES()
			ELSE
				TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",GET_TAXI_EMERGENCY_FAIL_STRING(myTaxiData))
			ENDIF
			
		ENDIF
		
		//Process excitement		
		IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
		AND eTaxiRiderStatus = TAXI_RIDERSTATUS_NORMAL
			HANDLE_TAXI_EXCITEMENT(myTaxiData,FALSE)
			
			//4/3/12 - Moving this here because of B*453195
			UPDATE_PROGRESS()
			//LM added this check for B* 278551
			IF bVomitDone
				
				IF TAXI_HANDLE_RELENTLESS_EXCITEMENT_DEUX()
					TAXI_SET_FAIL(myTaxiData, "Rider is too damn uncomfortable.")
				ENDIF
			
			ENDIF

#IF IS_DEBUG_BUILD
		ELIF bDebugTurnOnFreeRide
			HANDLE_TAXI_EXCITEMENT(myTaxiData,TRUE)
#ENDIF

		ENDIF
		
		//================================================================================================
	
		SWITCH myTaxiData.tTaxiOJ_RideState
			
			
			
/*PRESTREAM and Check that the player and taxi are good to go----------------------------------------
						
ooooo oooo   oooo ooooo ooooooooooo 
 888   8888o  88   888  88  888  88 
 888   88 888o88   888      888     
 888   88   8888   888      888     
o888o o88o    88  o888o    o888o    
*/
			
			CASE TRS_INIT_STREAM
				
				//Initialize bonus //LM TODo: i'm guessing the bonus indices should be enums?
				TAXI_INITIALIZE_BONUS_FIELD(bonusFieldTakeItEasy[TTIE_BONUS_NO_PUKE], "TAXI_SC_BN_01", TAXI_CONST_BONUS_CASH_NO_PUKE)
				
				TAXI_INITIALIZE_BONUS_INFO(myTaxiData, bonusFieldTakeItEasy)
				
				//Request of our assets
				INIT_TAXI_STREAMS()
				
				TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
				
				TAXI_SPAWN_PASSENGER(myTaxiData, vPassengerPt, vPassengerPickupPt,  "TaxiOtis", mPassengerModel,135.3, 40.0)
				TAXI_INIT_PASSENGER_BLIP(myTaxiData)
					
				//Move on to the next stage
				TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_STREAMING)
				
			BREAK
			
			//Wait for streaming to finish
			CASE TRS_STREAMING

				IF TAXI_ASSETS_STREAMED()
					
					INIT_ALL_TAXI_EXCITEMENT_VALUES()
					
					INITIALIZE_GENERIC_TAXI_EXCEPTIONS()
					
					CLEAR_ALL_TAXI_PASSENGER_REACT_BITS(myTaxiData)
					
					myTaxiData.vTaxiOJPickup = vPassengerPt	
					
					
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPAWNING)
				ENDIF
	
			BREAK
			
			CASE TRS_SPAWNING
				IF PROPERTY_VIP_INIT_READY(myTaxiData)
					
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
					
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_TORSO, 1, 0)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_LEG, 1, 0)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_HEAD, 1, 0)
						
						SET_PED_FLEE_ATTRIBUTES(myTaxiData.piTaxiPassenger, FA_DISABLE_COWER, TRUE)
						SET_PED_FLEE_ATTRIBUTES(myTaxiData.piTaxiPassenger, FA_DISABLE_HESITATE_IN_VEHICLE, TRUE)
						
					ENDIF
	
					//Give player obj to go pickup Passenger
					ENABLE_TAXI_SPEECH(myTaxiData)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
				ENDIF
			BREAK
			
			CASE TRS_MANAGE_PICKUP
				IF TAXI_HANDLE_IV_PICKUP_NO_METER(myTaxiData)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_PARK)
				ENDIF
			BREAK
			
			CASE TRS_WAIT_PARK
				IF IS_PASSENGER_ENTERING_TAXI(myTaxiData)
					myTaxiData.vTaxiOJDropoff = vDropOff
					
					//Greet the player
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GREET,TRUE)
					
					//CleanUp Pickup Lock & POI
					CLEANUP_TAXI_PICKUP_STOP(myTaxiData)
					
					//Set Vehicle Health
					myTaxiData.iOldVehicleHealth =  GET_ENTITY_HEALTH(myTaxiData.viTaxi)
					
					TAXI_OJ_TIE_SET_TIPS_AND_EXCITMENT_TO_CHECK()
					
					SWITCH GET_TAXI_VEHICLE_TYPE(myTaxiData)
						CASE TAXITYPE_HIGH 		strPukeClip = "vomit_van" 	BREAK
						CASE TAXITYPE_LOW 		strPukeClip = "vomit_low" 	BREAK
						CASE TAXITYPE_REG 		strPukeClip = "vomit_outside" 	BREAK
					ENDSWITCH
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"strPukeClip = ", strPukeClip)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DRIVING_PASSENGER)
					
				ENDIF
				
				// check to see if player dived, if so send back to prev state
				IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
					IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
						TAXI_HANDLE_PLAYER_DIVE(myTaxiData)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
					ENDIF
				ENDIF
			BREAK		
						
			CASE TRS_DRIVING_PASSENGER
				
				IF NOT bSeatShuffled
					//Shuffle to the next seat
					IF IS_PASSENGER_IN_TAXI_SEAT(myTaxiData, VS_FRONT_RIGHT)
					OR IS_PASSENGER_IN_TAXI_SEAT(myTaxiData, VS_BACK_RIGHT)
					OR TAXI_SHUFFLE_PASSENGER_SEAT(myTaxiData,VS_BACK_RIGHT)
						bSeatShuffled = TRUE
					ENDIF
				ENDIF
				
				//Set Radio Station Check
//				IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_BANTER)
//					IF NOT GET_TAXI_RADIO_CHECK_FLAG(myTaxiData)	
//						TAXI_RADIO_STATION_TURN_ON(myTaxiData)
//					ENDIF
//				ENDIF
				//stop puking if already at destination
				IF HAS_TAXI_OJ_REACHED_DROPOFF(myTaxiData)
				AND eTaxiRiderStatus = TAXI_RIDERSTATUS_PUKE
					IF (ptSpew <> NULL)
						STOP_PARTICLE_FX_LOOPED(ptSpew)
						ptSpew = NULL
					ENDIF	
					CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
					bVomitDone = TRUE
				ENDIF
				
				SWITCH eTaxiRiderStatus
					
					CASE TAXI_RIDERSTATUS_NORMAL
						IF TAXI_HANDLE_EXCITEMENT_DRIVING_DEUX(myTaxiData/*,tTaxiOJ_DQ_Data*/)
							
							//Remove destination blip
							CLEAR_DIALOGUE_QUEUE(tDialogueLine)
							CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,3)
							
							REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
							
							SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)
							
							CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, "txm1_arrive", CONV_PRIORITY_VERY_HIGH)
							
							TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DROPPING_OFF)
						ENDIF
						
						
						//LM handling when to make the passenger get his puke on
						// after excitement level hits 50 (125 is the max before fail)
					
						//IF  myTaxiData.iTaxiOJ_PassengerExcitement > iPukeExcitementLevel
						IF GET_TAXI_TIE_NUM_WARNINGS(myTaxiData) >= NUM_PUKE_WARNINGS
						AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
						AND NOT IS_ENTITY_UPSIDEDOWN(myTaxiData.viTaxi)
						AND NOT (GET_ENTITY_ROLL(myTaxiData.viTaxi) <= TAXI_TWEAKS_ROLL_NEG
								OR GET_ENTITY_ROLL(myTaxiData.viTaxi) >= TAXI_TWEAKS_ROLL_POS)
						AND NOT bVomitDone
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"GET_ENTITY_ROLL  = ", GET_ENTITY_ROLL(myTaxiData.viTaxi))
							
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_ANY_CONVERSATION()
								SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_PUKE, TRUE)
							ENDIF
							//RESET EXITEMENT AFTER PUKING SO EXITEMENT PTS BUILD UP FROM 0 TO FAIL MISSION
							myTaxiData.iTaxiOJ_PassengerExcitement = 0
							eTaxiRiderStatus = TAXI_RIDERSTATUS_PUKE
						ENDIF
						
						IF DOES_CAM_EXIST(myTaxiData.camTaxi)
							DESTROY_CAM(myTaxiData.camTaxi)
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
						ENDIF
					BREAK
					
					CASE TAXI_RIDERSTATUS_PUKE
						
						
						IF NOT bVomitDone
							TAXI_MAKE_PASSENGER_PUKE(myTaxiData.piTaxiPassenger)
						ENDIF
						
						// monitor for puke bonus
						IF TAXI_MONITOR_PUKE_BONUS()
							IF bStoppedForVomit
								CDEBUG1LN(DEBUG_OJ_TAXI,"YOU GOT THE GOOD REACT.  fVomitStopTime = ", fVomitStopTime)
								
								SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_PUKE_REACT_GOOD, TRUE)
								
								eTaxiRiderStatus = TAXI_RIDERSTATUS_NORMAL
							ELSE
								CDEBUG1LN(DEBUG_OJ_TAXI,"YOU GOT THE BAD REACT.  fVomitStopTime = ", fVomitStopTime)
								
								SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_PUKE_REACT_BAD, TRUE)
								
								eTaxiRiderStatus = TAXI_RIDERSTATUS_NORMAL
							ENDIF
							
							SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
						ENDIF
						
						IF NOT IS_TAXI_DRIVEN_BY_PLAYER(myTaxiData)
							TAXI_YELL_PLAYER_TO_RETURN(myTaxiData)
							
							IF NOT IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
								
								SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
							ENDIF
						
						// if player reaches destination, set control off
						ELIF IS_ENTITY_AT_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJDropoff, <<TAXI_DROPOFF_X_COORD, TAXI_DROPOFF_Y_COORD,TAXI_DROPOFF_Z_COORD>>)
						
							SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE)
						ENDIF
						
						
						
					BREAK
				ENDSWITCH
			BREAK
			
			CASE TRS_DROPPING_OFF
//				//If you sucked he's gonna run on you
//				IF myTaxiData.iTaxiOJ_PassengerExcitement > TAXI_CONST_EXCITEMENT_LEVEL_HI
//					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPECIAL_ENDING)
//				ELSE
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
						IF bVomitDone
							
							TAXI_TIE_OJ_RATE_OVERALL_TIP_LEVEL(FALSE)
							
						//Set TIP & BONUS Here------------------------------------
						ELSE
							TAXI_TIE_OJ_RATE_OVERALL_TIP_LEVEL(TRUE)
							TAXI_SET_BONUS_AWARD(myTaxiData,ENUM_TO_INT(TTIE_BONUS_NO_PUKE))
						ENDIF
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
					ENDIF
//				ENDIF
			BREAK
			
			//Regular Payment
			CASE TRS_REGULAR_PAYMENT
				//Wait til passenger is done speaking/paying than tell him to leave vehicle
				IF HAS_TAXI_OJ_PASSENGER_BEEN_DROPPED_OFF(myTaxiData)

					TAXI_OJ_TASK_PASSENGER_DROPOFF(myTaxiData)
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_JOKE,TRUE,TRUE)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)				
		
				ENDIF			

			BREAK
			
			//Passenger attempts to run out
			CASE TRS_SPECIAL_ENDING
				IF TAXI_SET_PED_RUNNING(myRunState, myTaxiData, myTaxiData.piTaxiPassenger, taxiMoney, TRUE)	
	
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_JOKE,TRUE,TRUE)
					//State++
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)
				ENDIF
			BREAK
			
			//Pop up the scorecard
			CASE TRS_SCORECARD_GRADE
				IF TAXI_CALC_SCORECARD(myTaxiData,TaxiMidSize)//,bonusFieldTakeItEasy[TTIE_BONUS_NO_PUKE].description)
					
					TAXI_MISSION_END(TRUE,myTaxiData)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CLEANUP)
					
				ENDIF
				
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.piTaxiPassenger, <<-1226.25, -902.82, 11.33>>) < 3.0
						SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_SearchForClosestDoor, TRUE)
						#IF IS_DEBUG_BUILD
							DRAW_DEBUG_TEXT_2D("PRF_SearchForClosestDoor Flag Set", <<0.5, 0.2, 0.0>>)
						#ENDIF
					ENDIF
				ENDIF
			BREAK
			
			//Clean everything up
			CASE TRS_CLEANUP
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.piTaxiPassenger, <<-1226.25, -902.82, 11.33>>) < 3.0
						SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_SearchForClosestDoor, TRUE)
						
						#IF IS_DEBUG_BUILD
							DRAW_DEBUG_TEXT_2D("PRF_SearchForClosestDoor Flag Set", <<0.5, 0.2, 0.0>>)
						#ENDIF
					ENDIF
					
					IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.piTaxiPassenger, myTaxiData.vTaxiOJ_PassengerGoToPt) < 1.0
						Script_Cleanup()	
					ENDIF
				ELSE
					Script_Cleanup()
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC
SCRIPT

	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		Script_Cleanup()
	ENDIF
	
	// Any initialisation (generally, only mission scripts should set
	// the mission flag to TRUE)
	SET_MISSION_FLAG(TRUE)
	
	INITIALIZE_SCRIPT_VARIABLES()
	
	// The script loop
	WHILE (TRUE)
		// Maintain the script – perform per-frame functionality
		
	//All skips and debug shortcuts-------------------------------
	#IF IS_DEBUG_BUILD
	
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			IF GET_TAXI_TIE_NUM_WARNINGS(myTaxiData) < NUM_PUKE_WARNINGS
			AND IS_PASSENGER_IN_TAXI(myTaxiData)
				SET_TAXI_TIP_TO_AMAZING(myTaxiData)
			ENDIF
		ENDIF
		
		PROCESS_TAXI_DEBUG_SKIP(myTaxiData,tDebugState)
		
		// Debug Key: Check for Pass (not for Minigmes)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			TAXI_ODDJOB_DEBUG_SKIP_TO_SCORECARD(myTaxiData)
		ENDIF

		// Debug Key: Check for Fail (not for Minigames)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			TAXI_DEBUG_FAIL_TRIGGERED(myTaxiData)
			Script_Failed()
		ENDIF
		
		// Debug Key: set vehicle health to 0.0
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
			IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				SET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi, 0.0)
			ENDIF
		ENDIF		
		
		IF bMakePassengerPuke
			TAXI_MAKE_PASSENGER_PUKE(myTaxiData.piTaxiPassenger)
		ENDIF

		PROCESS_WIDGETS()
	#ENDIF
	//END DEBUG----------------------------------------------------
		
		
		IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPlayer)
			Main_Taxi_OJ_TakeItEasy()
		ELSE
			REASSIGN_TAXI_OJ_DRIVER(myTaxiData)
		ENDIF
		
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
