USING "commands_pad.sch"
USING "commands_hud.sch"
USING "commands_path.sch"
USING "minigames_helpers.sch"
USING "taxi_shared_Consts.sch"
USING "Taxi_lib_timer.sch"
USING "z_volumes.sch"

#IF IS_DEBUG_BUILD

PROC TAXI_DEBUG_SHOW_Odometer(TaxiStruct &myTaxiData)
	FLOAT fDistanceDriven, fMilesDriven
	TEXT_LABEL_63 sDebugTaxiOdometer[4]
	
	fDistanceDriven = myTaxiData.fTaxiOJ_CurrentMileage
	
	sDebugTaxiOdometer[0] = "Taxi Odometer:"
	DRAW_DEBUG_TEXT_2D(sDebugTaxiOdometer[0],<<0.4,0.28,0.0>>)

	sDebugTaxiOdometer[1] = "Meters: "
	sDebugTaxiOdometer[1] += GET_STRING_FROM_FLOAT(fDistanceDriven,2)
	DRAW_DEBUG_TEXT_2D(sDebugTaxiOdometer[1],<<0.4,0.30,0.0>>)
	
	fMilesDriven = CAST_METRES_TO_MILES(fDistanceDriven )
	sDebugTaxiOdometer[2] = "Miles: "
	sDebugTaxiOdometer[2] += GET_STRING_FROM_FLOAT(fMilesDriven,2)
	DRAW_DEBUG_TEXT_2D(sDebugTaxiOdometer[2],<<0.4,0.32,0.0>>)
	
	sDebugTaxiOdometer[3] = "Fare: $"
	sDebugTaxiOdometer[3] += GET_STRING_FROM_FLOAT(fMilesDriven * TAXI_BASE_FARE,2)
	DRAW_DEBUG_TEXT_2D(sDebugTaxiOdometer[3],<<0.4,0.34,0.0>>)
	
	
ENDPROC
PROC TAXI_DEBUG_SHOW_HEALTH_ON_SCREEN(TaxiStruct &myTaxiData)
	
	IF DOES_ENTITY_EXIST(myTaxiData.viTaxi)
		IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
			TEXT_LABEL_63 sDebugTaxiHealth[3]

			//Show Tip Amount
			sDebugTaxiHealth[0] = "Taxi H:"
			sDebugTaxiHealth[0] += TAXI_UTILS_GET_STRING_FROM_INT_SP(GET_ENTITY_HEALTH(myTaxiData.viTaxi))
			DRAW_DEBUG_TEXT_2D(sDebugTaxiHealth[0],<<0.4,0.5,0.0>>)
			
			sDebugTaxiHealth[1] = "Engine H:"
			sDebugTaxiHealth[1] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi))
			DRAW_DEBUG_TEXT_2D(sDebugTaxiHealth[1],<<0.4,0.52,0.0>>)
			
			sDebugTaxiHealth[2] = "Gas H:"
			sDebugTaxiHealth[2] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_VEHICLE_PETROL_TANK_HEALTH(myTaxiData.viTaxi))
			DRAW_DEBUG_TEXT_2D(sDebugTaxiHealth[2],<<0.4,0.54,0.0>>)
		ENDIF
	ENDIF
ENDPROC
PROC TAXI_DEBUG_SPEED(TaxiStruct &myTaxiData, HUD_2D_INT &h2i_dist1,HUD_2D_INT &h2i_dist2)
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),myTaxiData.viTaxi)	
			SET_HUD_INT_VALUE(h2i_dist1,FLOOR(GET_ENTITY_SPEED(myTaxiData.viTaxi)),TAXI_UI_TAXI_SPEED_DEBUG_X,TAXI_UI_TAXI_SPEED_DEBUG_Y,TAXI_UI_DIST_TO_ESCAPEE_DEBUG_S,255,255,255)
			SET_HUD_INT_VALUE(h2i_dist2,FLOOR(GET_VEHICLE_ESTIMATED_MAX_SPEED(myTaxiData.viTaxi)),TAXI_UI_TAXI_MAX_SPEED_DEBUG_X,TAXI_UI_TAXI_MAX_SPEED_DEBUG_Y,TAXI_UI_DIST_TO_ESCAPEE_DEBUG_S,0,255,0)
			HUD_2D_NUM_DRAW(h2i_dist2)
			HUD_2D_NUM_DRAW(h2i_dist1)
		ENDIF
	ENDIF
	
ENDPROC
PROC TAXI_DEBUG_SPEED_TEXT_LABEL(TaxiStruct &myTaxiData)

	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),myTaxiData.viTaxi)	
			TEXT_LABEL_63 sDebugTaxiHealth[3]
			
			sDebugTaxiHealth[0] = "Speed :"
			sDebugTaxiHealth[0] += TAXI_UTILS_GET_STRING_FROM_INT_SP(FLOOR(GET_ENTITY_SPEED(myTaxiData.viTaxi)))
			sDebugTaxiHealth[0] += " / "
			sDebugTaxiHealth[0] += TAXI_UTILS_GET_STRING_FROM_INT_SP(FLOOR(GET_VEHICLE_ESTIMATED_MAX_SPEED(myTaxiData.viTaxi)))
			DRAW_DEBUG_TEXT_2D(sDebugTaxiHealth[0],<<TAXI_UI_TAXI_MAX_SPEED_DEBUG_X,TAXI_UI_TAXI_MAX_SPEED_DEBUG_Y,0.0>>)
			
		ENDIF
	ENDIF
	
ENDPROC

PROC TAXI_DEBUG_SHOW_CURRENT_PLAYING_LABEL(TEXT_LABEL_63 &sConvLabel)
	
	TEXT_LABEL_63 sDebugConvLabel[1]
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	
		sConvLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
		sDebugConvLabel[0] = "Current Conv Label : "
		sDebugConvLabel[0] +=sConvLabel
		DRAW_DEBUG_TEXT_2D(sDebugConvLabel[0],<<0.1,0.5,0.0>>)
	
	ENDIF
		
ENDPROC


PROC TAXI_DEBUG_SHOW_CURRENT_DIALOGUE_INDEX(TaxiStruct &myTaxiData)
	
	TEXT_LABEL_63 sDebugConvLabel[1]
	
	sDebugConvLabel[0] = "D Index : "
	sDebugConvLabel[0] += GET_STRING_OJ_TAXI_DIALOGUE_INDEX(myTaxiData)
	DRAW_DEBUG_TEXT_2D(sDebugConvLabel[0],<<0.1,0.48,0.0>>)
		
ENDPROC

PROC TAXI_DEBUG_SHOW_CURRENT_D_QUEUE_NUM(TAXI_OJ_DIALOGUE_Q_DATA &tTaxiOJ_DQ_Data)
	
	TEXT_LABEL_63 sDialogueQDebug[1]
	
	
	sDialogueQDebug[0] = "Taxi Dialogue Q is : "
	sDialogueQDebug[0] += TAXI_UTILS_GET_STRING_FROM_INT_SP(tTaxiOJ_DQ_Data.iCurrentDQLine)
	DRAW_DEBUG_TEXT_2D(sDialogueQDebug[0],<<0.1,0.52,0.0>>)
		
ENDPROC

PROC TAXI_DEBUG_SHOW_SPEECH_GATE_STATUS(TaxiStruct &myTaxiData)
	TEXT_LABEL_63 sDialogueGateDebug[1]
	
	sDialogueGateDebug[0] = "Speech Gate is : "
	IF myTaxiData.bTaxiOJ_CanSpeak
		sDialogueGateDebug[0] += " OPEN"
	ELSE
		sDialogueGateDebug[0] += " CLOSED"
	ENDIF
	

	DRAW_DEBUG_TEXT_2D(sDialogueGateDebug[0],<<0.1,0.54,0.0>>)
ENDPROC

PROC TAXI_DEBUG_SHOW_DQ_STATE(TAXI_OJ_DIALOGUE_Q_DATA &tTaxiOJ_DQ_Data)
	
	TEXT_LABEL_63 sDialogueQState[1]
	sDialogueQState[0] = "State : "
	
	SWITCH tTaxiOJ_DQ_Data.tDialogueQStates
	
		CASE TDQ_STOPPED
			sDialogueQState[0] += "TDQ_STOPPED"
		BREAK
		
		
		CASE TDQ_CLOSED
			sDialogueQState[0] += "TDQ_CLOSED"
		BREAK
		
		CASE TDQ_PLAY_LINE
			sDialogueQState[0] += "TDQ_PLAY_LINE"
		BREAK
		
		CASE TDQ_CONFIRM_LINE_IS_PLAYING
			sDialogueQState[0] += "TDQ_CONFIRM_LINE_IS_PLAYING"
		BREAK
		
		CASE TDQ_WAIT_FOR_INTERRUPTION
			sDialogueQState[0] += "TDQ_WAIT_FOR_INTERRUPTION"
		BREAK
		
		CASE TDQ_ADD_DELAY_BEFORE_RESUME
			sDialogueQState[0] += "TDQ_ADD_DELAY_BEFORE_RESUME"
		BREAK
		
		CASE TDQ_RESUME_FROM_INTERUPTION
			sDialogueQState[0] += "TDQ_RESUME_FROM_INTERUPTION"
		BREAK
		
		CASE TDQ_RESUME_CONVERSATION
			sDialogueQState[0] +="TDQ_RESUME_CONVERSATION"
		BREAK
		
		CASE TDQ_SPECIAL_CASE_FOR_LAST_LINE_IN_CONVERSATION_INTERRUPTION
			sDialogueQState[0] +="TDQ_SPECIAL_CASE_FOR_LAST"
		BREAK
	
	ENDSWITCH
	
	DRAW_DEBUG_TEXT_2D(sDialogueQState[0],<<0.1,0.58,0.0>>)
ENDPROC

PROC TAXI_DEBUG_TIMERS(TaxiStruct &myTaxiData)
	TEXT_LABEL_31 sDebugTimers[TT_NUM_TIMERS]
	INT i
	REPEAT TT_NUM_TIMERS i
		IF IS_TAXI_TIMER_STARTED(myTaxiData, INT_TO_ENUM(TAXI_TIMERS,i))
			sDebugTimers[i] = GET_TAXI_TIMER_NAME(INT_TO_ENUM(TAXI_TIMERS,i))
			sDebugTimers[i] += "  "
			sDebugTimers[i] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_TAXI_TIMER_IN_SECONDS(myTaxiData, INT_TO_ENUM(TAXI_TIMERS,i)))
			IF i > 0 AND i <= 3
				DRAW_DEBUG_TEXT_2D(sDebugTimers[i],<<0.8,0.2 + (i*0.02),0.0>>,255,0,0)
			ELSE
				DRAW_DEBUG_TEXT_2D(sDebugTimers[i],<<0.8,0.2 + (i*0.02),0.0>>)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
PROC TAXI_DEBUG_SHOW_TIPS(TaxiStruct & myTaxiData)

	TEXT_LABEL_63 sDebugTips[8]
	
	//Show total mission time
	IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_RIDETODEST)
		sDebugTips[0] = "Total Ride Time: "
		sDebugTips[0] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST))
		DRAW_DEBUG_TEXT_2D(sDebugTips[0],<<0.1,0.18,0.0>>)
	ENDIF
	//Show Tip Amount
	sDebugTips[1] = "Current Tip Points:"
	sDebugTips[1] += TAXI_UTILS_GET_STRING_FROM_INT_SP(myTaxiData.iTaxiOJ_CashTip)
	DRAW_DEBUG_TEXT_2D(sDebugTips[1],<<0.1,0.2,0.0>>)
	
		
	//Show Tip Level Legend
	sDebugTips[2] = "Tips Levels: ASS < "
	sDebugTips[2] += TAXI_UTILS_GET_STRING_FROM_INT_SP(myTaxiData.iTaxiOJ_CashTipAvg)
	sDebugTips[2] += " AVERAGE > "
	sDebugTips[2] += TAXI_UTILS_GET_STRING_FROM_INT_SP(myTaxiData.iTaxiOJ_CashTipAmazing)
	sDebugTips[2] += " AMAZING "
	DRAW_DEBUG_TEXT_2D(sDebugTips[2],<<0.1,0.22,0.0>>)
	
	
	//Show Currentn Tip Level
	sDebugTips[3] = "Current tip level is: "
	IF myTaxiData.iTaxiOJ_CashTip > myTaxiData.iTaxiOJ_CashTipAmazing
		sDebugTips[3] += "AMAZING" 
	ELIF myTaxiData.iTaxiOJ_CashTip < myTaxiData.iTaxiOJ_CashTipAvg
		sDebugTips[3] += "ASS" 
	ELSE
		sDebugTips[3] += "AVERAGE" 
	ENDIF
	DRAW_DEBUG_TEXT_2D(sDebugTips[3],<<0.1,0.24,0.0>>)


	//Show countdown til next check
	IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
		IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_TIP_CHECK)
			IF (myTaxiData.fTaxiOJ_TimeGood - GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_TIP_CHECK)) >= 0
			
				sDebugTips[4] = "Time left to hit Good Time: "
				sDebugTips[4] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(myTaxiData.fTaxiOJ_TimeGood - GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_TIP_CHECK))
				DRAW_DEBUG_TEXT_2D(sDebugTips[4],<<0.1,0.26,0.0>>)
			ENDIF
		ENDIF
	ENDIF
	
	//Show distance from check
	IF myTaxiData.fTaxiOJ_DistanceGood > 0
		sDebugTips[5] = "Distance away from goal: "
		sDebugTips[5] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(myTaxiData.fTaxiOJ_DistanceGood)
		DRAW_DEBUG_TEXT_2D(sDebugTips[5],<<0.1,0.28,0.0>>)
	ENDIF
	
	//Show line playing and amount that line is going to give
	IF NOT IS_STRING_NULL_OR_EMPTY(myTaxiData.sTaxiOJ_TipLine)
		sDebugTips[6] = "Tip line playing is: "
		sDebugTips[6] += myTaxiData.sTaxiOJ_TipLine
		DRAW_DEBUG_TEXT_2D(sDebugTips[6],<<0.1,0.3,0.0>>)
		
		sDebugTips[7] = "Amount of tip to be added: "
		sDebugTips[7] += TAXI_UTILS_GET_STRING_FROM_INT_SP(myTaxiData.iTaxiOJ_CashTipToAdd)
		DRAW_DEBUG_TEXT_2D(sDebugTips[7],<<0.1,0.32,0.0>>)
	
	ENDIF
	
	

ENDPROC

PROC TAXI_DEBUG_SHOW_WANTED(TaxiStruct &myTaxiData)
	TEXT_LABEL_63 sDebugWanted[2]
	
	sDebugWanted[0] = "Num Wanted # "
	sDebugWanted[0] += TAXI_UTILS_GET_STRING_FROM_INT_SP(myTaxiData.iTaxiOJ_WantedLevel)
	DRAW_DEBUG_TEXT_2D(sDebugWanted[0],<<0.8,0.2,0.0>>)
	
	
	sDebugWanted[1] = "Time "
	sDebugWanted[1] += TAXI_UTILS_GET_STRING_FROM_INT_SP(GET_WANTED_LEVEL_TIME_TO_ESCAPE())
	DRAW_DEBUG_TEXT_2D(sDebugWanted[1],<<0.8,0.22,0.0>>)

ENDPROC
PROC TAXI_DEBUG_HUD_POS(BOOL bEnable = TRUE)
	IF bEnable
		draw_debug_screen_grid()
	ENDIF
ENDPROC
/// PURPOSE:	Used for debugging Taxi Oddjobs STOPCAR & FOLLOWCAR
///    
/// PARAMS:
///    bDroppOff - 
///    TaxiStruct - 
///    blip - 			ref for the point to blip
///    viChasee - 		the chaser/chasee vehicle
///    h2i_dist1 - 
///    h2i_dist2 - 
PROC TAXI_DEBUG_DRAW_DISTANCE_CHASE(TaxiStruct &myTaxiData, VEHICLE_INDEX &viChasee, VECTOR vDestination, TEXT_LABEL &sDebugText[])
			
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
	AND IS_VEHICLE_DRIVEABLE(viChasee)
		
		sDebugText[0] = "2 Fiance: "
		sDebugText[0] += TAXI_UTILS_GET_STRING_FROM_INT_SP(CEIL(GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,viChasee)))
		DRAW_DEBUG_TEXT_2D(sDebugText[0],<<TAXI_UI_DESC_TO_DEST_DEBUG_X,TAXI_UI_DIST_FROM_ESCAPEE_DEBUG_Y,0.0>>)
		
		sDebugText[1] = "2 Whore: "
		sDebugText[1] += TAXI_UTILS_GET_STRING_FROM_INT_SP(CEIL(GET_ENTITY_DISTANCE_FROM_LOCATION(viChasee,vDestination)))
		DRAW_DEBUG_TEXT_2D(sDebugText[1],<<TAXI_UI_DESC_TO_DEST_DEBUG_X,TAXI_UI_LOWEST_DEBUG_Y_ABOVE_MINI_MAP,0.0>>)
	
	ENDIF
	
ENDPROC

PROC TAXI_DEBUG_DRAW_DISTANCE(BOOL bDropOff, TaxiStruct &myTaxiData, HUD_2D_INT &h2i_dist, BOOL bDrawDebugSphere = FALSE)
	
	IF myTaxiData.bTaxiOJ_CheatDrawDists
		//No dropoff point only need distance to passenger.
		IF NOT bDropOff
			//At this point a valid pickup has been located
			IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION
				//Distance from Player to Passenger
				SET_HUD_INT_VALUE(h2i_dist,CEIL(GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJPickup)),TAXI_UI_DIST_TO_DEST_DEBUG_X,TAXI_UI_LOWEST_DEBUG_Y_ABOVE_MINI_MAP,TAXI_UI_DIST_TO_ESCAPEE_DEBUG_S,255,215,0)
				HUD_2D_NUM_DRAW(h2i_dist)
			ENDIF
		//Dropoff point has been found
		ELSE
			DRAW_DEBUG_TEXT("Destination",myTaxiData.vTaxiOJDropoff,0,255,0)
			IF bDrawDebugSphere
				DRAW_DEBUG_SPHERE(myTaxiData.vTaxiOJDropoff,3,0,255,125)
				
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
				
				//Distance from Escapee to Destination
				SET_HUD_INT_VALUE(h2i_dist,CEIL(GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff)),TAXI_UI_DIST_TO_DEST_DEBUG_X,TAXI_UI_LOWEST_DEBUG_Y_ABOVE_MINI_MAP,TAXI_UI_DIST_TO_ESCAPEE_DEBUG_S,255,215,0)
				HUD_2D_NUM_DRAW(h2i_dist)
			
			ENDIF
							
		ENDIF
	ENDIF
	
ENDPROC

PROC TAXI_DEBUG_DRAW_DISTANCE_GROUP(BOOL bDropOff, TaxiStruct &myTaxiData, VECTOR vTaxiGroupDropoff, HUD_2D_INT &h2i_dist, BOOL bDrawDebugSphere = FALSE)
	
	IF myTaxiData.bTaxiOJ_CheatDrawDists
		//No dropoff point only need distance to passenger.
		IF NOT bDropOff
			//At this point a valid pickup has been located
			IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION
				//Distance from Player to Passenger
				SET_HUD_INT_VALUE(h2i_dist,CEIL(GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJPickup)),TAXI_UI_DIST_TO_DEST_DEBUG_X,TAXI_UI_LOWEST_DEBUG_Y_ABOVE_MINI_MAP,TAXI_UI_DIST_TO_ESCAPEE_DEBUG_S,255,215,0)
				HUD_2D_NUM_DRAW(h2i_dist)
			ENDIF
		//Dropoff point has been found
		ELSE
			DRAW_DEBUG_TEXT("Destination",myTaxiData.vTaxiOJDropoff,0,255,0)
			IF bDrawDebugSphere
				DRAW_DEBUG_SPHERE(myTaxiData.vTaxiOJDropoff,3,0,255,125)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
				
				//Distance from Escapee to Destination
				SET_HUD_INT_VALUE(h2i_dist,CEIL(GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, vTaxiGroupDropoff)),TAXI_UI_DIST_TO_DEST_DEBUG_X,TAXI_UI_LOWEST_DEBUG_Y_ABOVE_MINI_MAP,TAXI_UI_DIST_TO_ESCAPEE_DEBUG_S,255,215,0)
				HUD_2D_NUM_DRAW(h2i_dist)
			
			ENDIF
							
		ENDIF
	ENDIF
ENDPROC
#ENDIF

//EOF
