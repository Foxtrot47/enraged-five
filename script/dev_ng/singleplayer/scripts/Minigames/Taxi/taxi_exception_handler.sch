///Taxi_exception_handler.sch
///Author : John Sripan
///
///    This libary runs and manages all the exception handlers you're using for your mission.  Order of operations are as follows
///    
///    1)  Write exceptions.  This will generally just return true on some condition.  False otherwise.
///    2)  Write exception handlers.  This will generally just call into the dialogue manager to say something.  Handlers should return true if you've handled the exception.  False otherwise.
///    3)  Push your exceptions and exception handlers on to the exception stack on intialization of your taxi script thread.
///    4)  Run the main process function below on update at the top of your main loop.
///    5)  Enjoy.
///    

//----------- Taxi exception handler stuff --------------------

TYPEDEF FUNC BOOL TAXI_EXCEPTION_CALLBACK(TaxiStruct & taxiData)
TYPEDEF FUNC BOOL TAXI_EXCEPTION_HANDLER_CALLBACK(TaxiStruct & taxiData, INT currentExceptionTimeStamp)

CONST_INT MAX_EXCEPTIONS	16

CONST_INT TAXI_EXCEPTION_BIT_INITIALIZED		BIT0
CONST_INT TAXI_EXCEPTION_BIT_ENABLED			BIT1
CONST_INT TAXI_EXCEPTION_BIT_CAUGHT				BIT2
CONST_INT TAXI_EXCEPTION_BIT_HANDLED			BIT3

ENUM TAXI_GENERIC_EXCEPTIONS
	TAXI_GENERIC_EXCEPTION_PASSENGER_NOT_OK			=	0,						
	TAXI_GENERIC_EXCEPTION_TAXI_NOT_DRIVEABLE,							
	TAXI_GENERIC_EXCEPTION_TAXI_STOPPED,									
	TAXI_GENERIC_EXCEPTION_TAXI_FLIPPED,									
	TAXI_GENERIC_EXCEPTION_TAXI_WATER,									
	TAXI_GENERIC_EXCEPTION_PLAYER_WANTED,								
	TAXI_GENERIC_EXCEPTION_PLAYER_NOT_IN_TAXI,							
	TAXI_GENERIC_EXCEPTION_DRIVER_SIDE_BLOCKED							
ENDENUM

STRUCT TAXI_EXCEPTION_FIELD
	INT									status
	INT									timeStamp
	TAXI_EXCEPTION_CALLBACK				exceptionCallback
	TAXI_EXCEPTION_HANDLER_CALLBACK		exceptionHandlerCallback
ENDSTRUCT

STRUCT TAXI_EXCEPTION_STACK
	INT						exceptionCount
	TAXI_EXCEPTION_FIELD	exceptions[MAX_EXCEPTIONS]
ENDSTRUCT

TAXI_EXCEPTION_STACK		g_TaxiExceptionStack

//-----------   End Taxi exception handler stuff --------------------


//--------------------------------- GENERIC TAXI EXCEPTIONS ---------------------------------
USING "taxi_support_lib.sch"
/// PURPOSE:
///    Returns true if the taxi exists and the taxi isn't driveable.
/// PARAMS:
///    myTaxiData - The main taxi struct
FUNC BOOL TAXI_EXCEPTION_TAXI_NOT_DRIVEABLE( TaxiStruct & myTaxiData )
	
	IF DOES_ENTITY_EXIST(myTaxiData.viTaxi)		
		IF IS_TAXI_OJ_VEHICLE_FUCKED(myTaxiData.viTaxi)
			SET_TAXI_EMERGENCY_FAIL(myTaxiData, TFS_TAXI_DISABLED)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXCEPTION_TAXI_NOT_DRIVEABLE: TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Generic exception handler - This shouldn't be used for anything
/// PARAMS:
///    myTaxiData - The main taxi struct
///    currentExceptionTimeStamp - Time stamp of the caught exception
FUNC BOOL TAXI_EXCEPTION_HANDLER_TAXI_NOT_DRIVEABLE( TaxiStruct & myTaxiData, INT currentExceptionTimeStamp )
	//do something
	UNUSED_PARAMETER(myTaxiData)
	currentExceptionTimeStamp = currentExceptionTimeStamp
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXCEPTION_HANDLER_TAXI_NOT_DRIVEABLE: TAXI_SET_FAIL" )
	TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",TFS_TAXI_DISABLED)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns true if the taxi exists and the taxi engine is on fire
/// PARAMS:
///    myTaxiData - The main taxi struct
FUNC BOOL TAXI_EXCEPTION_TAXI_ENGINE_ON_FIRE( TaxiStruct & myTaxiData )
	
	IF DOES_ENTITY_EXIST(myTaxiData.viTaxi)		
		IF IS_TAXI_OJ_VEHICLE_ENGINE_FUCKED(myTaxiData.viTaxi)
			SET_TAXI_EMERGENCY_FAIL(myTaxiData, TFS_TAXI_ENGINE_FIRE)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXCEPTION_TAXI_NOT_DRIVEABLE: TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Generic exception handler - This shouldn't be used for anything
/// PARAMS:
///    myTaxiData - The main taxi struct
///    currentExceptionTimeStamp - Time stamp of the caught exception
FUNC BOOL TAXI_EXCEPTION_HANDLER_TAXI_ENGINE_ON_FIRE( TaxiStruct & myTaxiData, INT currentExceptionTimeStamp )
	//do something
	UNUSED_PARAMETER(myTaxiData)
	currentExceptionTimeStamp = currentExceptionTimeStamp
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXCEPTION_HANDLER_TAXI_NOT_DRIVEABLE: TAXI_SET_FAIL" )
	TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",TFS_TAXI_ENGINE_FIRE)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns true if the player is not in the taxi.  Taxi must exist and be driveable.
/// PARAMS:
///    myTaxiData - The main taxi struct
FUNC BOOL TAXI_EXCEPTION_PLAYER_NOT_IN_TAXI( TaxiStruct & myTaxiData )
	
	IF DOES_ENTITY_EXIST(myTaxiData.viTaxi)
		IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
			IF GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi,VS_DRIVER) != PLAYER_PED_ID()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Generic exception handler - This shouldn't be used for anything
/// PARAMS:
///    myTaxiData - The main taxi struct
///    currentExceptionTimeStamp - Time stamp of the caught exception
FUNC BOOL TAXI_EXCEPTION_HANDLER_PLAYER_NOT_IN_TAXI( TaxiStruct & myTaxiData, INT currentExceptionTimeStamp )
	//do something
	UNUSED_PARAMETER(myTaxiData)
	currentExceptionTimeStamp = currentExceptionTimeStamp
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXCEPTION_HANDLER_PLAYER_NOT_IN_TAXI - Exception handled" )
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns true if the passenger is not ok.  Passenger must exist.
/// PARAMS:
///    myTaxiData - The main taxi struct
FUNC BOOL TAXI_EXCEPTION_PASSENGER_NOT_OK( TaxiStruct & myTaxiData )
		
	IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPassenger)
		IF IS_PED_INJURED(myTaxiData.piTaxiPassenger) OR IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger) OR IS_ENTITY_ON_FIRE(myTaxiData.piTaxiPassenger)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Generic exception handler - This shouldn't be used for anything
/// PARAMS:
///    myTaxiData - The main taxi struct
///    currentExceptionTimeStamp - Time stamp of the caught exception
FUNC BOOL TAXI_EXCEPTION_HANDLER_PASSENGER_NOT_OK( TaxiStruct & myTaxiData, INT currentExceptionTimeStamp )
	//do something
	UNUSED_PARAMETER(myTaxiData)
	currentExceptionTimeStamp = currentExceptionTimeStamp
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXCEPTION_HANDLER_PASSENGER_NOT_OK - Exception handled" )
	TAXI_SET_FAIL(myTaxiData, "Passenger injured.",TFS_PASSENGER_DIED)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns true if the taxi is in the water.  Taxi must exist and be driveable.
/// PARAMS:
///    myTaxiData - The main taxi struct
FUNC BOOL TAXI_EXCEPTION_TAXI_WATER( TaxiStruct & myTaxiData )
		
	IF DOES_ENTITY_EXIST(myTaxiData.viTaxi)
		IF IS_ENTITY_IN_WATER(myTaxiData.viTaxi) AND NOT IS_VEHICLE_ON_ALL_WHEELS(myTaxiData.viTaxi)
			IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_UNDER_WATER)
				TAXI_RESET_TIMERS(myTaxiData, TT_UNDER_WATER)
			ELSE
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_UNDER_WATER) > TAXI_CONST_TIME_FOR_UNDER_WATER_FAIL
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_UNDER_WATER)
				TAXI_CANCEL_TIMERS(myTaxiData, TT_UNDER_WATER)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Generic exception handler - This shouldn't be used for anything
/// PARAMS:
///    myTaxiData - The main taxi struct
///    currentExceptionTimeStamp - Time stamp of the caught exception
FUNC BOOL TAXI_EXCEPTION_HANDLER_TAXI_WATER( TaxiStruct & myTaxiData, INT currentExceptionTimeStamp )
	//do something
	UNUSED_PARAMETER(myTaxiData)
	currentExceptionTimeStamp = currentExceptionTimeStamp
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXCEPTION_TAXI_WATER - Exception handled" )
	//TAXI_SET_FAIL(myTaxiData,"Taxi Is Under Water",TFS_UNDER_WATER)
	SET_TAXI_EMERGENCY_FAIL(myTaxiData, TFS_UNDER_WATER)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns true if the taxi is flipped over.  Taxi must exist and be driveable.
/// PARAMS:
///    myTaxiData - The main taxi struct
FUNC BOOL TAXI_EXCEPTION_TAXI_FLIPPED( TaxiStruct & myTaxiData )
		
	IF NOT IS_ENTITY_DEAD( myTaxiData.viTaxi )
		IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(myTaxiData.viTaxi)
			//IF IS_VEHICLE_STUCK_ON_ROOF( myTaxiData.viTaxi )
			IF IS_VEHICLE_STUCK_TIMER_UP( myTaxiData.viTaxi, VEH_STUCK_ON_ROOF, SIDE_TIME )//ROOF_TIME )
	    	OR IS_VEHICLE_STUCK_TIMER_UP( myTaxiData.viTaxi, VEH_STUCK_ON_SIDE, SIDE_TIME )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Generic exception handler - This shouldn't be used for anything
/// PARAMS:
///    myTaxiData - The main taxi struct
///    currentExceptionTimeStamp - Time stamp of the caught exception
FUNC BOOL TAXI_EXCEPTION_HANDLER_TAXI_FLIPPED( TaxiStruct & myTaxiData, INT currentExceptionTimeStamp )
	//do something
	UNUSED_PARAMETER(myTaxiData)
	currentExceptionTimeStamp = currentExceptionTimeStamp
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXCEPTION_HANDLER_TAXI_FLIPPED - Exception handled" )
	TAXI_SET_FAIL(myTaxiData,"Taxi Is Flipped",TFS_TAXI_DISABLED)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns true if the taxi is stopped.  Taxi must exist and be driveable.
/// PARAMS:
///    myTaxiData - The main taxi struct
FUNC BOOL TAXI_EXCEPTION_TAXI_STOPPED( TaxiStruct & myTaxiData )
		
	IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
		IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
			IF IS_VEHICLE_STOPPED(myTaxiData.viTaxi)
				IF IS_BITMASK_SET(myTaxiData.iTaxiOJ_DXBitsStopped,  TAXI_OJ_FAIL_BIT_STOPPED)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Generic exception handler - This shouldn't be used for anything
/// PARAMS:
///    myTaxiData - The main taxi struct
///    currentExceptionTimeStamp - Time stamp of the caught exception
FUNC BOOL TAXI_EXCEPTION_HANDLER_TAXI_STOPPED( TaxiStruct & myTaxiData, INT currentExceptionTimeStamp )
	//do something
	UNUSED_PARAMETER(myTaxiData)
	currentExceptionTimeStamp = currentExceptionTimeStamp
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXCEPTION_HANDLER_TAXI_STOPPED - Exception handled" )
	TAXI_SET_FAIL(myTaxiData, "Stayed stopped for too long.")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns true is the player is wanted.
/// PARAMS:
///    myTaxiData - The main taxi struct
FUNC BOOL TAXI_EXCEPTION_PLAYER_WANTED( TaxiStruct & myTaxiData )
	
	UNUSED_PARAMETER(myTaxiData)
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		IF IS_BITMASK_SET(myTaxiData.iTaxiOJ_DXBitsPolice,  TAXI_OJ_FAIL_BIT_POLICE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Generic exception handler - This shouldn't be used for anything
/// PARAMS:
///    myTaxiData - The main taxi struct
///    currentExceptionTimeStamp - Time stamp of the caught exception
FUNC BOOL TAXI_EXCEPTION_HANDLER_PLAYER_WANTED( TaxiStruct & myTaxiData, INT currentExceptionTimeStamp )
	//do something
	UNUSED_PARAMETER(myTaxiData)
	currentExceptionTimeStamp = currentExceptionTimeStamp
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXCEPTION_HANDLER_PLAYER_WANTED - Exception handled" )
	TAXI_SET_FAIL(myTaxiData, "Didn't lose police in time.")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns true if the taxi driver side door is blocked.  Taxi must exist and be driveable.
/// PARAMS:
///    myTaxiData - The main taxi struct
FUNC BOOL TAXI_EXCEPTION_DRIVER_SIDE_BLOCKED( TaxiStruct & myTaxiData )
	
	IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)	
		IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
			IF GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi,VS_DRIVER) != PLAYER_PED_ID()
				IF NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR( PLAYER_PED_ID(), myTaxiData.viTaxi, VS_DRIVER )
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Generic exception handler - This shouldn't be used for anything.
/// PARAMS:
///    myTaxiData - The main taxi struct
///    currentExceptionTimeStamp - Time stamp of the caught exception
FUNC BOOL TAXI_EXCEPTION_HANDLER_DRIVER_SIDE_BLOCKED( TaxiStruct & myTaxiData, INT currentExceptionTimeStamp )
	//do something
	UNUSED_PARAMETER(myTaxiData)
	currentExceptionTimeStamp = currentExceptionTimeStamp
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXCEPTION_HANDLER_DRIVER_SIDE_BLOCKED - Exception handled" )
	RETURN TRUE
ENDFUNC

//--------------------------------- END GENERIC TAXI EXCEPTIONS ---------------------------------

/// PURPOSE:
///    Pushes a new exception on to the exception stack
/// PARAMS:
///    fp_Callback - Function pointer to the exception callback
///    fp_HandlerCallback - Function pointer to the exception handler callback
///    enable - Most exceptions will default to true.  If for some reason it needs to be false, toggle this.
PROC TAXI_EXCEPTION_PUSH_EXCEPTION( TAXI_EXCEPTION_CALLBACK fp_Callback, TAXI_EXCEPTION_HANDLER_CALLBACK fp_HandlerCallback, BOOL enable = TRUE )
	
	IF g_TaxiExceptionStack.exceptionCount >= MAX_EXCEPTIONS
		g_TaxiExceptionStack.exceptionCount = MAX_EXCEPTIONS
		SCRIPT_ASSERT("TAXI_EXCEPTION_ADD_EXCEPTION - Nope.  You've reached max exception count.  Go to taxi_globals and increase the limit.")
		EXIT
	ENDIF
	
	g_TaxiExceptionStack.exceptions[g_TaxiExceptionStack.exceptionCount].status = 0	
	SET_BITMASK( g_TaxiExceptionStack.exceptions[g_TaxiExceptionStack.exceptionCount].status, TAXI_EXCEPTION_BIT_INITIALIZED )
	IF enable
		SET_BITMASK( g_TaxiExceptionStack.exceptions[g_TaxiExceptionStack.exceptionCount].status, TAXI_EXCEPTION_BIT_ENABLED )
	ENDIF
	
	g_TaxiExceptionStack.exceptions[g_TaxiExceptionStack.exceptionCount].exceptionCallback = fp_Callback
	g_TaxiExceptionStack.exceptions[g_TaxiExceptionStack.exceptionCount].exceptionHandlerCallback = fp_HandlerCallback
	g_TaxiExceptionStack.exceptionCount++
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Exception is being pushed on- Count  = ","",g_TaxiExceptionStack.exceptionCount)
ENDPROC

/// PURPOSE:
///    Pops an exception off the exception stack.
PROC TAXI_EXCEPTION_POP_EXCEPTION()
	
	g_TaxiExceptionStack.exceptionCount--
	
	IF g_TaxiExceptionStack.exceptionCount < 0
		g_TaxiExceptionStack.exceptionCount = 0
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets or clears the enabled bit on the exception at the given index
/// PARAMS:
///    index - index in the exception stock
///    value - value you want to set for the enable bit on the exception call stack
PROC TAXI_EXCEPTION_SET_ENABLE_BIT( INT index, BOOL value )
	IF index < g_TaxiExceptionStack.exceptionCount AND index >= 0
		IF value
			SET_BITMASK( g_TaxiExceptionStack.exceptions[ index ].status, TAXI_EXCEPTION_BIT_ENABLED )
		ELSE
			CLEAR_BITMASK( g_TaxiExceptionStack.exceptions[ index ].status, TAXI_EXCEPTION_BIT_ENABLED )
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the status of the initialized bit of the exception
/// PARAMS:
///    index - index in the exception stock
FUNC BOOL TAXI_EXCEPTION_GET_INITIALIZED_BIT( INT index )
	IF index < g_TaxiExceptionStack.exceptionCount AND index >= 0
		RETURN IS_BITMASK_SET( g_TaxiExceptionStack.exceptions[ index ].status, TAXI_EXCEPTION_BIT_INITIALIZED )
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the status of the enable bit of the exception
/// PARAMS:
///    index - index in the exception stock
FUNC BOOL TAXI_EXCEPTION_GET_ENABLE_BIT( INT index )
	IF index < g_TaxiExceptionStack.exceptionCount AND index >= 0
		RETURN IS_BITMASK_SET( g_TaxiExceptionStack.exceptions[ index ].status, TAXI_EXCEPTION_BIT_ENABLED )
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the status of the caught bit of the exception
/// PARAMS:
///    index - index in the exception stock
FUNC BOOL TAXI_EXCEPTION_GET_CAUGHT_BIT( INT index )
	IF index < g_TaxiExceptionStack.exceptionCount AND index >= 0
		RETURN IS_BITMASK_SET( g_TaxiExceptionStack.exceptions[ index ].status, TAXI_EXCEPTION_BIT_CAUGHT )
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the status of the handled bit of the exception
/// PARAMS:
///    index - index in the exception stock
FUNC BOOL TAXI_EXCEPTION_GET_HANDLED_BIT( INT index )
	IF index < g_TaxiExceptionStack.exceptionCount AND index >= 0
		RETURN IS_BITMASK_SET( g_TaxiExceptionStack.exceptions[ index ].status, TAXI_EXCEPTION_BIT_HANDLED )
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Initializes the exception stack with generic data.  YOU SHOULD NOT BE CALLING THIS IN GENERAL.
PROC INITIALIZE_GENERIC_TAXI_EXCEPTIONS()
	g_TaxiExceptionStack.exceptionCount = 0
	TAXI_EXCEPTION_PUSH_EXCEPTION( &TAXI_EXCEPTION_PASSENGER_NOT_OK, 			&TAXI_EXCEPTION_HANDLER_PASSENGER_NOT_OK )			//Exception 0
	TAXI_EXCEPTION_PUSH_EXCEPTION( &TAXI_EXCEPTION_TAXI_NOT_DRIVEABLE, 			&TAXI_EXCEPTION_HANDLER_TAXI_NOT_DRIVEABLE )		//Exception 1
	TAXI_EXCEPTION_PUSH_EXCEPTION( &TAXI_EXCEPTION_TAXI_STOPPED, 				&TAXI_EXCEPTION_HANDLER_TAXI_STOPPED )				//Exception 2
	TAXI_EXCEPTION_PUSH_EXCEPTION( &TAXI_EXCEPTION_TAXI_FLIPPED, 				&TAXI_EXCEPTION_HANDLER_TAXI_FLIPPED )
	TAXI_EXCEPTION_PUSH_EXCEPTION( &TAXI_EXCEPTION_TAXI_WATER, 					&TAXI_EXCEPTION_HANDLER_TAXI_WATER )
	TAXI_EXCEPTION_PUSH_EXCEPTION( &TAXI_EXCEPTION_PLAYER_WANTED, 				&TAXI_EXCEPTION_HANDLER_PLAYER_WANTED )
	TAXI_EXCEPTION_PUSH_EXCEPTION( &TAXI_EXCEPTION_TAXI_ENGINE_ON_FIRE, 		&TAXI_EXCEPTION_HANDLER_TAXI_ENGINE_ON_FIRE )
	
	//TAXI_EXCEPTION_PUSH_EXCEPTION( &TAXI_EXCEPTION_PLAYER_NOT_IN_TAXI, 			&TAXI_EXCEPTION_HANDLER_PLAYER_NOT_IN_TAXI )
	//TAXI_EXCEPTION_PUSH_EXCEPTION( &TAXI_EXCEPTION_DRIVER_SIDE_BLOCKED, 		&TAXI_EXCEPTION_HANDLER_DRIVER_SIDE_BLOCKED )
ENDPROC

/// PURPOSE:
///    Processes all the exceptions in the exception stack.  This needs to be called in your main loop.
/// PARAMS:
///    myTaxiData - The main taxi struct
PROC PROCESS_TAXI_EXCEPTIONS( TaxiStruct & myTaxiData )
	
	IF myTaxiData.tTaxiOJ_RideState >=TRS_MANAGE_PICKUP

		IF DOES_ENTITY_EXIST(myTaxiData.viTaxi)
		
			IF g_TaxiExceptionStack.exceptionCount > 0
			AND NOT IS_BITMASK_SET( g_TaxiExceptionStack.exceptions[4].status,TAXI_EXCEPTION_BIT_CAUGHT ) // doing a hack for underwater
				
				INT i = 0
				
				FOR i = 0 TO ( g_TaxiExceptionStack.exceptionCount - 1 )
					IF IS_BITMASK_SET( g_TaxiExceptionStack.exceptions[i].status,  TAXI_EXCEPTION_BIT_ENABLED )
					
						// PROCESS EXCEPTION
						IF CALL g_TaxiExceptionStack.exceptions[i].exceptionCallback( myTaxiData )
							IF NOT IS_BITMASK_SET( g_TaxiExceptionStack.exceptions[i].status,TAXI_EXCEPTION_BIT_CAUGHT )
								SET_BITMASK( g_TaxiExceptionStack.exceptions[i].status, TAXI_EXCEPTION_BIT_CAUGHT )
								CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_TAXI_EXCEPTIONS - Exception caught | index = ","", i)
								CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_TAXI_EXCEPTIONS - Exception caught | RideState = ","", myTaxiData.sTaxiOJ_RideState)
								g_TaxiExceptionStack.exceptions[i].timeStamp = GET_GAME_TIMER()	
							ENDIF
						ELSE
							CLEAR_BITMASK( g_TaxiExceptionStack.exceptions[i].status, TAXI_EXCEPTION_BIT_CAUGHT | TAXI_EXCEPTION_BIT_HANDLED )
						ENDIF
						
						// PROCESS EXCEPTION HANDLER
						IF IS_BITMASK_SET( g_TaxiExceptionStack.exceptions[i].status,TAXI_EXCEPTION_BIT_CAUGHT ) AND NOT IS_BITMASK_SET( g_TaxiExceptionStack.exceptions[i].status,TAXI_EXCEPTION_BIT_HANDLED )			
							IF CALL g_TaxiExceptionStack.exceptions[i].exceptionHandlerCallback( myTaxiData, g_TaxiExceptionStack.exceptions[i].timeStamp )
								SET_BITMASK( g_TaxiExceptionStack.exceptions[i].status, TAXI_EXCEPTION_BIT_HANDLED )	
							ENDIF
						ENDIF
						
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"Exceptions not being handled - Taxi Garbage Collected")
			TAXI_SET_FAIL(myTaxiData,"Taxi Garbage Collected",TFS_TAXI_GARBAGE_COLLECTED)
		ENDIF
	ENDIF
ENDPROC



//eof

