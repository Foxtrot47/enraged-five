//Setting up a world regions header to better breakdown the world for the purposes of having most granular data for Taxi Oddjobs
//The goal is to be able to use these regions as ways to make use of specific dialogue or having easier ways of sending a player somewhere

//Author : John Diaz 4/19/11

USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_vehicle.sch"
USING "commands_player.sch"
USING "Commands_debug.sch"
USING "commands_entity.sch"
USING "model_enums.sch"
USING "commands_HUD.sch"
USING "commands_Misc.sch"
USING "script_drawing.sch"
USING "minigames_helpers.sch"
USING "Taxi_Lib_Timer.sch"

//Consts representing the number of locations for each region... aka how big the array is
CONST_INT NUM_DEST_TR_0_LOS_PUERTA 			8
CONST_INT NUM_DEST_TR_1_SOUTH_LOS_SANTOS   12
CONST_INT NUM_DEST_TR_2_VINEWOOD_HILLS		6
CONST_INT NUM_DEST_TR_3_DEL_PERRO			5
CONST_INT NUM_DEST_TR_4_LITTLE_SEOUL	   20
CONST_INT NUM_DEST_TR_5_DOWNTOWN		   11  //LM Change 12/5/11
CONST_INT NUM_DEST_TR_6_SOUTHGATE			3
CONST_INT NUM_DEST_TR_7_CITYHILLS			3 //1
CONST_INT NUM_DEST_TR_8_COUTRYHILLS			4 //2
CONST_INT NUM_DEST_TR_9_BEL_AIR				7
CONST_INT NUM_DEST_TR_10_W_HOLLYWOOD		3 //2
CONST_INT NUM_DEST_TR_11_ECHO_PARK			4 //1
CONST_INT NUM_DEST_TR_12_N_CITYHILLS		2 //1
CONST_INT NUM_DEST_TR_13_VINEWOOD			4
CONST_INT NUM_DEST_TR_14_MOUNTAINS			7 //5 //3
CONST_INT NUM_DEST_TR_15_LAKES				7 //2
CONST_INT NUM_DEST_TR_16_SANDY_SHORES		8
CONST_INT NUM_DEST_TR_17_SS_HILLS			5 //8
CONST_INT NUM_DEST_TR_18_NW_COUNTRY			5 //4
CONST_INT NUM_DEST_TR_19_N_COUNTRY			6
CONST_INT NUM_DEST_TR_20_E_COUNTRY			6 //3
CONST_INT NUM_DEST_TR_21_COUNTRYSIDE		6 //4
CONST_INT NUM_DEST_TR_22_SPECIAL_POIS	   10

//Tweakable Consts
CONST_FLOAT VISIBILITY_DISTANCE				40.0
CONST_FLOAT TAXI_REGION_MIN_RIDE_DISTANCE	800.0	//Min distance for a dropoff location to be from where the player is
//Taxi Regions
ENUM TAXI_REGIONS
	TR_0_LOS_PUERTA = 0,
	TR_1_SOUTH_LOS_SANTOS,
	TR_2_VINEHOOD_HILLS,
	TR_3_DEL_PERRO,
	TR_4_LITTLE_SEOUL,
	TR_5_DOWNTOWN,
	TR_6_SOUTHGATE,
	TR_7_CITYHILLS,
	TR_8_COUTRYHILLS,
	TR_9_BEL_AIR,
	TR_10_W_HOLLYWOOD,
	TR_11_ECHO_PARK,
	TR_12_N_CITYHILLS,
	TR_13_VINEWOOD,
	TR_14_MOUNTAINS,
	TR_15_LAKES,
	TR_16_SANDY_SHORES,
	TR_17_SS_HILLS,
	TR_18_NW_COUNTRY,
	TR_19_N_COUNTRY,
	TR_20_E_COUNTRY,
	TR_21_COUNTRYSIDE,
	TR_22_SPECIAL_POIS,
	TR_NUM_REGIONS,
	TR_CURRENT_REGION				//ENUM USED FOR ERROR CHECKING
ENDENUM	

STRUCT TAXI_REG_INFO
	
	STRING 			sRegName				//String Debug Info
	INT				iNumDestinations		//Number of destinations in the region
	VECTOR			vTopLeft				//NW most point of the region
	VECTOR			vBottomRight			//SE most point of the region
	VECTOR			vNeighborRegions		//Since I can't keep an array inside of here, I'll use a vector to represent 3 distinct neighboring regions
ENDSTRUCT

//EOF

