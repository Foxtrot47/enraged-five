USING "Taxi_Regions.sch"
USING "Controller_Taxi_Lib.sch"

VECTOR taxiDest_0_LosPuerta[NUM_DEST_TR_0_LOS_PUERTA]
VECTOR taxiDest_1_SouthLosSantos[NUM_DEST_TR_1_SOUTH_LOS_SANTOS]
VECTOR taxiDest_2_VinewoodHills[NUM_DEST_TR_2_VINEWOOD_HILLS]
VECTOR taxiDest_3_DelPerro[NUM_DEST_TR_3_DEL_PERRO]
VECTOR taxiDest_4_LittleSeoul[NUM_DEST_TR_4_LITTLE_SEOUL]
VECTOR taxiDest_5_Downtown[NUM_DEST_TR_5_DOWNTOWN]
VECTOR taxiDest_6_Southgate[NUM_DEST_TR_6_SOUTHGATE]
VECTOR taxiDest_7_CityHills[NUM_DEST_TR_7_CITYHILLS]
VECTOR taxiDest_8_CountryHills[NUM_DEST_TR_8_COUTRYHILLS]
VECTOR taxiDest_9_BelAir[NUM_DEST_TR_9_BEL_AIR]
VECTOR taxiDest_10_WHollywood[NUM_DEST_TR_10_W_HOLLYWOOD]
VECTOR taxiDest_11_EchoPark[NUM_DEST_TR_11_ECHO_PARK]
VECTOR taxiDest_12_NCityHills[NUM_DEST_TR_12_N_CITYHILLS]
VECTOR taxiDest_13_Vinewood[NUM_DEST_TR_13_VINEWOOD]
VECTOR taxiDest_14_Mountains[NUM_DEST_TR_14_MOUNTAINS]
VECTOR taxiDest_15_Lakes[NUM_DEST_TR_15_LAKES]
VECTOR taxiDest_16_SandyShores[NUM_DEST_TR_16_SANDY_SHORES]
VECTOR taxiDest_17_SSHills[NUM_DEST_TR_17_SS_HILLS]
VECTOR taxiDest_18_NWCountry[NUM_DEST_TR_18_NW_COUNTRY]
VECTOR taxiDest_19_NCountry[NUM_DEST_TR_19_N_COUNTRY]
VECTOR taxiDest_20_ECountry[NUM_DEST_TR_20_E_COUNTRY]
VECTOR taxiDest_21_CountrySide[NUM_DEST_TR_21_COUNTRYSIDE]
//VECTOR taxiDest_8_SpecialPOIs[NUM_DEST_TR_8_SPECIAL_POIS]

#IF IS_DEBUG_BUILD

FLOAT FMINdistance = 1000
INT iDebugSwitchCounter = 0
INT iTempIter
BLIP_INDEX blipTemp[NUM_DEST_TR_4_LITTLE_SEOUL]
BLIP_INDEX blipClosest, blipPickup
TAXI_REGIONS debugCurrentRegion
VECTOR vClosestPoint = <<0,0,0>>
VECTOR vTestPickup
BOOL bNewPointFound
#ENDIF

//Fills the array of locations.
//Make sure to increment CONST_INTS NUM_DEST_TR_#_ at the top of the header if you add/delete points from here
PROC INIT_TAXI_LOCATIONS()

//	Region 0 Los Puerta 		MIN - << -1902,	-3533>>		MAX - << 765, 	-1280>>
	taxiDest_0_LosPuerta[0] = << -939.6133, -1523.3700, 4.1764 >> 
	taxiDest_0_LosPuerta[1] = << -1161.5302, -1492.4709, 3.3139 >> 
	taxiDest_0_LosPuerta[2] =  <<-1037.1715, -2730.9226, 19.1697>> //<< -1035.7612, -2731.2888, 25 >>		//Airport
	taxiDest_0_LosPuerta[3] = << -1204.5907, -1377.7397, 3.1844 >>	//Marina Del Rey Liquor Store ** Problem Spot
	taxiDest_0_LosPuerta[4] = << -939.6133, -1523.3700, 4.1764 >>  
	taxiDest_0_LosPuerta[5] = << -91.6341, -1328.1545, 28.3373 >>
	taxiDest_0_LosPuerta[6] = <<-900.3735, -2690.0142, 12.9447>>
	taxiDest_0_LosPuerta[7] = <<-1012.7864, -2468.6018, 12.8384>>

//	Region 1 South Los Santos	MIN - << -373,  -3533>>		MAX - << 708, 	-1280>>
	taxiDest_1_SouthLosSantos[0] = << 87.0014, -1402.4484, 28.2549 >>
	taxiDest_1_SouthLosSantos[1] = << 146.5773, -1426.8824, 28.2915 >>  	// Baldwin Hill - Chili House 
	taxiDest_1_SouthLosSantos[2] = << 435.1445, -1462.7794, 28.2915 >> 
	taxiDest_1_SouthLosSantos[3] = << 135.1209, -1302.6038, 28.1936 >>  	//Vanilla Unicorn
	taxiDest_1_SouthLosSantos[4] = << 112.2743, -1947.2144, 19.6826 >> 	//Neighborhood Cul De Sac
	taxiDest_1_SouthLosSantos[5] = << 65.3804, -1568.9438, 28.6028 >>		//Dollar Pills
	taxiDest_1_SouthLosSantos[6] = << 137.8282, -1724.2274, 28.2079 >> 	//Sex Shop			//Problem spot - Middle of the Road
	taxiDest_1_SouthLosSantos[7] = << 132.9427, -2009.9111, 17.2055 >> 	//Repair Shop
	taxiDest_1_SouthLosSantos[8] = << -184.5559, -1438.0306, 30.3049 >>	//Small Shops Cove
	taxiDest_1_SouthLosSantos[9] = << 153.5036, -1451.8922, 28.1418 >>
	taxiDest_1_SouthLosSantos[10] = << 50.2007, -1379.7288, 32 >>		//Car Wash
	taxiDest_1_SouthLosSantos[11] = << -297.4216, -1848.1876, 24.7217 >>
	
//	Region 2 Vinewood Hills		MIN - << 708, 	-3533>>		MAX - << 1952,	-1280>> 
	taxiDest_2_VinewoodHills[0] = << 768.2622, -2371.7468, 21.6783 >> 	//Industrial
	taxiDest_2_VinewoodHills[1] = << 1188.8407, -3249.5051, 5.0276 >> 
	taxiDest_2_VinewoodHills[2] = << 770.0794, -2991.9758, 5.0203 >> 
	taxiDest_2_VinewoodHills[3] = << 946.5765, -2453.4355, 27.5512 >>
	taxiDest_2_VinewoodHills[4] = << 957.6567, -2097.3511, 29.6646 >>
	taxiDest_2_VinewoodHills[5] = << 926.1091, -1749.8364, 29.9898 >> 
	
//	Region 3 Del Perro 			MIN - << -4613, -1280>>		MAX - << -1902,	315>> 
	taxiDest_3_DelPerro[0] = << -2075.6023, -330.3965, 12.3162 >> 
	taxiDest_3_DelPerro[1] = << -1994.4458, -545.3368, 10.7213 >>	//on beach
	taxiDest_3_DelPerro[2] = << -3171.3213, 918.0819, 13.4239 >> 
	taxiDest_3_DelPerro[3] = << -1918.1833, -370.5667, 48.1919 >> //near pier
	taxiDest_3_DelPerro[4] = << -1938.7660, -441.2331, 18.7351 >>
	
//	Region 4 Little Seoul		MIN - << -1902, -1280>>		MAX - << -373, 	315>> 
	taxiDest_4_LittleSeoul[0] = << -1392.6733, -179.0173, 46.3472 >> 
	taxiDest_4_LittleSeoul[1] = << -1428.5615, -195.8090, 46.4109 >> 
	taxiDest_4_LittleSeoul[2] = << -385.7066, -403.3985, 30.5014 >> 	
	taxiDest_4_LittleSeoul[3] = << -486.7284, -389.6814, 33.2888 >>
	taxiDest_4_LittleSeoul[4] = << -583.0761, -446.2154, 33.2990 >> 
	taxiDest_4_LittleSeoul[5] = << -496.0706, 236.2110, 82.0245 >>  //theBandiera
	taxiDest_4_LittleSeoul[6] = << -536.2413, -679.3174, 32.2419 >> //korea Town
	taxiDest_4_LittleSeoul[7] = << -1156.0463, -1418.8805, 3.7969 >>
	taxiDest_4_LittleSeoul[8] = << -856.6715, -1148.8550, 5.2178 >> 
	taxiDest_4_LittleSeoul[9] = << -696.4865, -823.4500, 22.8254 >> 
	taxiDest_4_LittleSeoul[10] = << -1142.8555, -933.4650, 1.6658 >> 		//Venice Streets
	taxiDest_4_LittleSeoul[11] = << -1281.3527, -882.9135, 10.3152 >> 		//Venice Coffee Shops
	taxiDest_4_LittleSeoul[12] = << -1099.7911, -1702.4319, 3.3700 >>		//Beach
	taxiDest_4_LittleSeoul[13] = << -1270.7485, -1159.3470, 5.2547 >>		//Venice Shopping Complex
	taxiDest_4_LittleSeoul[14] = << -1130.5422, -1195.1903, 1.2757 >>		//Venice across bridge
	taxiDest_4_LittleSeoul[15] = << -1076.0032, -1011.8756, 1.1503 >>		//Marina Del Rey Liquor Store
	taxiDest_4_LittleSeoul[16] = << -1035.1838, -1252.2964, 5.1970 >>		//Venice Strip
	taxiDest_4_LittleSeoul[17] = << -1006.7727, -1106.0552, 1.1484 >> 		//Venice Orange House
	taxiDest_4_LittleSeoul[18] = << -1847.1997, -602.4301, 10.4091 >>
	taxiDest_4_LittleSeoul[19] = << -597.1105, -304.0766, 33.9632 >>			// Jewelry Store
	
//	Region 5 Downtown		MIN - << -373, 	-1280>> 	MAX - << 708, 	315>> 
	taxiDest_5_Downtown[0] = << -272.7684, 248.4368, 89.1359 >> //Viper Room
	taxiDest_5_Downtown[1] = << 56.3248, 288.1890, 109.2810 >>
	taxiDest_5_Downtown[2] = << 415.2359, 103.9472, 99.8247 >>	//Hollywood Blvd
	taxiDest_5_Downtown[3] = << 394.9993, -200.0683, 58.3083 >>
	taxiDest_5_Downtown[4] = << 253.8570, -378.6688, 43.6252 >>
	taxiDest_5_Downtown[5] = << 223.6173, -50.8911, 68.2267 >> 
	taxiDest_5_Downtown[6] = << 544.0235, 202.5245, 100.6100 >> //Nice Apartment
	taxiDest_5_Downtown[7] = << -264.7876, -1062.8395, 24.8463 >> 
	taxiDest_5_Downtown[8] = << -234.3063, -732.1405, 32.5016 >>  
	taxiDest_5_Downtown[9] = << -84.3683, 241.2898, 99.4509 >>		//Eclipse Lounge
	taxiDest_5_Downtown[10] = << 346.4130, -960.6713, 28.4297 >>		//Jewelry District  //LM Change 12/5/11
	
	
//	Region 6 SouthGate		MIN - << 708, 	-1280>> 	MAX - << 1952, 	315>> 
	taxiDest_6_Southgate[0] = << 898.7972, 56.0888, 78.0298 >>  //Highway
	taxiDest_6_Southgate[1] = << 832.3909, -191.5166, 71.6695 >> //Streets
	taxiDest_6_Southgate[2] = << 790.4477, -734.7306, 26.5794 >> 		//PROBLEM POINT - LEADS TO SPAWNING IN MIDDLE OF ROAD #189051
	//<< 785.387, -751.008, 0 >>

//	Region 7 CityHills		MIN - << 1952, 	-1280>> 	MAX - << 4603, 	315>>
	taxiDest_7_CityHills[0] = << 2590.3372, -293.4442, 92.0786 >> 	//security prison
	taxiDest_7_CityHills[1] = << 2531.4043, -600.4139, 63.2007 >>
	taxiDest_7_CityHills[2] = << 2604.2168, 375.1005, 107.4720 >>

//	Region 8 CountryHills	MIN - << -4613, 315>> 		MAX - << -1902,	2550>>
	taxiDest_8_CountryHills[0] = << -3233.2627, 953.5733, 12.2187 >>
	taxiDest_8_CountryHills[1] = << -3078.3464, 675.6660, 12.9159 >>
	taxiDest_8_CountryHills[2] = << -2647.8303, 1504.8210, 117.9680 >>
	taxiDest_8_CountryHills[3] = << -2564.2141, 2318.0300, 32.2153 >>
	
//	Region 9 BelAir			MIN - << -1902, 315>> 		MAX - << 2780, 	2550>>
	taxiDest_9_BelAir[0] = << -1314.0194, 2483.4399, 22.7740 >>
	taxiDest_9_BelAir[1] = << -409.0888, 1172.8971, 324.6412 >>
	taxiDest_9_BelAir[2] = <<-1506.1864, 1473.5696, 116.8286>> 	//<< -1564.3497, 1479.2860, 87.5668 >> 
	taxiDest_9_BelAir[3] = << -518.6162, 1995.3928, 205.0397 >> 
	taxiDest_9_BelAir[4] = << -742.0004, 2320.3760, 70.6610 >>
	taxiDest_9_BelAir[5] = << -1851.3719, -602.5151, 10.4741 >> 
	taxiDest_9_BelAir[6] = << -1666.4133, -538.9609, 34.2561 >> 
	
//	Region 10 WHollywood	MIN - << -373, 	315>> 		MAX - << 708, 	2550 >>
	taxiDest_10_WHollywood[0] = << -139.8096, 1872.3776, 197.1660 >>
	taxiDest_10_WHollywood[1] = << 695.1385, 2297.1184, 49.9406 >>
	taxiDest_10_WHollywood[2] = << 167.7863, 782.6042, 208.0805 >>	
	
//	Region 11 EchoPark		MIN - << 708, 	315>> 		MAX - << 1952, 	2550>>
	taxiDest_11_EchoPark[0] = << 1498.5458, 774.8596, 76.4493 >> //near prison
	taxiDest_11_EchoPark[1] = << 1309.9080, 1090.1334, 104.5610 >>
	taxiDest_11_EchoPark[2] = << 1510.7048, 1712.5287, 109.2625 >>
	taxiDest_11_EchoPark[3] = << 1070.3932, 2038.0148, 52.5529 >>

//	Region 12 N_CityHills	MIN - << 1952, 	315>> 		MAX - << 4603, 	2550>>
	taxiDest_12_NCityHills[0] = << 2509.3093, 1601.6224, 30.0754 >>
	taxiDest_12_NCityHills[1] = << 2546.5505, 1971.3053, 19.0362 >>
	
//	Region 13 Vinewood 		MIN - << -4613, 2550>> 		MAX - << -1902,	4435>>
	taxiDest_13_Vinewood[0] = << -2339.0210, 3424.6162, 28.7394 >>
	taxiDest_13_Vinewood[1] = << -2494.4658, 3644.7483, 12.9216 >> 
	taxiDest_13_Vinewood[2] = << -2223.4434, 4335.2515, 48.4857 >> 
	taxiDest_13_Vinewood[3] = << -2369.6516, 4033.3623, 27.9193 >> 

//	Region 14 Mountains		MIN - << -1902, 2550>> 		MAX - << 2780, 	4435 >>
	taxiDest_14_Mountains[0] = << -1578.5250, 3005.0249, 32.3629 >>
	taxiDest_14_Mountains[1] = << -1441.7157, 4224.6069, 49.0816 >>
	taxiDest_14_Mountains[2] = << -637.3727, 4014.4500, 124.2407 >> 
	taxiDest_14_Mountains[3] = << -1092.5985, 2696.0288, 18.9957 >> 
	taxiDest_14_Mountains[4] = << -482.8450, 2853.0522, 32.7909 >> 
	taxiDest_14_Mountains[5] = << -1280.54346, 2543.43970, 17.30960 >>
	taxiDest_14_Mountains[6] = << -435.94427, 3039.76611, 27.85744 >>
	
//	Region 15 Lakes			MIN - << -373, 	2550>> 		MAX - << 708, 	4435 >>
	taxiDest_15_Lakes[0] = << 322.4791, 2624.6846, 43.4903 >> 
	taxiDest_15_Lakes[1] = << -118.0555, 2831.1055, 49.9236 >>
	taxiDest_15_Lakes[2] = << 8.32642, 3036.62500, 39.93201 >>
	taxiDest_15_Lakes[3] = << 486.82001, 3094.72314, 39.68279 >>
	taxiDest_15_Lakes[4] = << 355.66324, 3465.56396, 34.46225 >>
	taxiDest_15_Lakes[5] = << 73.93981, 3597.89917, 38.72102 >>
	taxiDest_15_Lakes[6] = << -223.53360, 3914.40942, 36.47960 >>
	
//	Region 16 SandyShores 	MIN - << 708, 	2550>> 		MAX - << 1952, 	4435>>
	taxiDest_16_SandyShores[0] = << 914.0446, 3641.2002, 31.4356 >>
	taxiDest_16_SandyShores[1] = << 1713.8070, 3786.2986, 33.6656 >>
	taxiDest_16_SandyShores[2] = << 1798.2664, 3773.4963, 32.6142 >>
	taxiDest_16_SandyShores[3] = << 2008.2037, 3771.9539, 31.1457 >> 
	taxiDest_16_SandyShores[4] = << 1860.1516, 3675.6560, 32.6690 >>
	taxiDest_16_SandyShores[5] = << 1974.8621, 3855.3694, 31.1620 >> 
	taxiDest_16_SandyShores[6] = << 1841.7070, 3799.6541, 32.2190 >>// replace
	taxiDest_16_SandyShores[7] = << 1775.6593, 3335.4927, 40.2247 >>
	
//	Region 17 SS_Hills 		MIN - << 1952, 	2550>> 		MAX - << 4603, 	4435>>
	taxiDest_17_SSHills[0] = << 2506.6829, 4104.0376, 37.3903 >> //CS_4 Auto Repair
	taxiDest_17_SSHills[1] = << 2902.4426, 4428.4380, 47.2892 >> 
	taxiDest_17_SSHills[2] = << 2338.3906, 3146.7617, 47.1288 >> 
	taxiDest_17_SSHills[3] = << 2756.3962, 3396.3950, 55.5796 >> 
	taxiDest_17_SSHills[4] = << 2560.3132, 2619.5896, 36.7532 >> 
	
//	Region 18 NW_Country 	MIN - << -1902, 4435>> 		MAX - << 2780, 	7693>>
	taxiDest_18_NWCountry[0] = << -1499.3542, 4980.8174, 61.8513 >>  //THIS PICKUP IS PROBLEMATIC
	taxiDest_18_NWCountry[1] = << -686.7758, 5841.2407, 15.7987 >>
	taxiDest_18_NWCountry[2] = << -547.5048, 5405.4707, 64.2441 >> 
	taxiDest_18_NWCountry[3] = << -411.6347, 6050.6509, 30.4916 >>  //something is wrong with this one
	taxiDest_18_NWCountry[4] = << -860.8704, 5421.5054, 33.9464 >> 

//	Region 19 N_Country 	MIN - << -373, 	4435>> 		MAX - << 708, 	7693 >>
	taxiDest_19_NCountry[0] = << 331.2503, 6586.8931, 27.8962 >>
	taxiDest_19_NCountry[1] = << -37.7542, 6319.7397, 30.3761 >> 
	taxiDest_19_NCountry[2] = << -115.5213, 6456.3770, 30.4546 >>
	taxiDest_19_NCountry[3] = << -145.1629, 6442.3926, 30.4404 >>
	taxiDest_19_NCountry[4] = << -46.1561, 6475.1499, 30.4923 >> 
	taxiDest_19_NCountry[5] = << 533.2067, 6537.7798, 26.6048 >> 

//	Region 20 E_Country 	MIN - << 708, 	4435>> 		MAX - << 1952, 	7693>>
	taxiDest_20_ECountry[0] = << 1592.8146, 6427.9238, 24.2329 >>   // The edge lighthouse
	taxiDest_20_ECountry[1] = << 1664.3406, 4845.4668, 41.0121 >> //CS_2 Shops
	taxiDest_20_ECountry[2] = << 1705.2246, 4713.1606, 41.4366 >> //CS_2 Feed Shop
	taxiDest_20_ECountry[3] = << 1731.8092, 6387.0054, 33.5953 >>
	taxiDest_20_ECountry[4] = << 1082.4832, 6503.5376, 20.0265 >>
	taxiDest_20_ECountry[5] = << 1996.3209, 5148.7158, 44.2356 >>
	
//	Region 21 CountrySide 	MIN - << 1952, 	4435>> 		MAX - << 4603, 	7693>>

	taxiDest_21_CountrySide[0] = << 2172.5203, 4759.7949, 40.0044 >>
	taxiDest_21_CountrySide[1] = << 2451.0212, 4953.2886, 43.9633 >> 
	taxiDest_21_CountrySide[2] = << 2245.1553, 5562.8618, 51.4367 >>
	taxiDest_21_CountrySide[3] = << 2456.1868, 4596.7407, 35.8278 >>
	taxiDest_21_CountrySide[4] = << 3336.8147, 5464.4482, 18.5259 >>
	taxiDest_21_CountrySide[5] = << 3491.2090, 4707.0747, 50.1144 >>
	//taxiDest_21_CountrySide[2] = << 3333.0630, 5470.4395, 18.6786 >> // The edge lighthouse

	CDEBUG1LN(DEBUG_OJ_TAXI,"INIT_TAXI_LOCATIONS - SUCCESS")
ENDPROC

//Fill in our Taxi Region Data Info
//Make sure to call this with txRegionsArray or you will geto a compile error
PROC INIT_TAXI_REGIONS(TAXI_REG_INFO &taxiRegionsArray[])
	
	INIT_TAXI_LOCATIONS()
	
	//Region 0 - Los Puerta////////////////////////////////////////////////////////
	taxiRegionsArray[TR_0_LOS_PUERTA].sRegName = "Region 0 - Los Puerta"
	taxiRegionsArray[TR_0_LOS_PUERTA].vTopLeft = <<-1902,-1280,20 >>  
	taxiRegionsArray[TR_0_LOS_PUERTA].vBottomRight = << -373, -3533, -20 >>
	taxiRegionsArray[TR_0_LOS_PUERTA].iNumDestinations = NUM_DEST_TR_0_LOS_PUERTA
	taxiRegionsArray[TR_0_LOS_PUERTA].vNeighborRegions = <<ENUM_TO_INT(TR_1_SOUTH_LOS_SANTOS),ENUM_TO_INT(TR_4_LITTLE_SEOUL),ENUM_TO_INT(TR_5_DOWNTOWN)>>
	
	//Region 1 - South Los Santos////////////////////////////////////////////////////////
	taxiRegionsArray[TR_1_SOUTH_LOS_SANTOS].sRegName = "Region 1 - South Los Santos"
	taxiRegionsArray[TR_1_SOUTH_LOS_SANTOS].vTopLeft = << -373,  -1280, 20 >>
	taxiRegionsArray[TR_1_SOUTH_LOS_SANTOS].vBottomRight = << 708, -3533, -20 >>
	taxiRegionsArray[TR_1_SOUTH_LOS_SANTOS].iNumDestinations = NUM_DEST_TR_1_SOUTH_LOS_SANTOS
	taxiRegionsArray[TR_1_SOUTH_LOS_SANTOS].vNeighborRegions = <<ENUM_TO_INT(TR_0_LOS_PUERTA),ENUM_TO_INT(TR_1_SOUTH_LOS_SANTOS),ENUM_TO_INT(TR_5_DOWNTOWN)>>
	
	//Region 2 - Vinewood Hills////////////////////////////////////////////////////////
	taxiRegionsArray[TR_2_VINEHOOD_HILLS].sRegName = "Region 2 - Vinewood Hills"
	taxiRegionsArray[TR_2_VINEHOOD_HILLS].vTopLeft = <<  708, -1280, 20 >>
	taxiRegionsArray[TR_2_VINEHOOD_HILLS].vBottomRight = << 1952, -3533,  -20 >> 
	taxiRegionsArray[TR_2_VINEHOOD_HILLS].iNumDestinations = NUM_DEST_TR_2_VINEWOOD_HILLS
	taxiRegionsArray[TR_2_VINEHOOD_HILLS].vNeighborRegions = <<ENUM_TO_INT(TR_0_LOS_PUERTA),ENUM_TO_INT(TR_4_LITTLE_SEOUL),ENUM_TO_INT(TR_5_DOWNTOWN)>>
	
	//Region 3 - Del Perro////////////////////////////////////////////////////////
	taxiRegionsArray[TR_3_DEL_PERRO].sRegName = "Region 3 - Del Perro"
	taxiRegionsArray[TR_3_DEL_PERRO].vTopLeft = << -4613, 315, 20 >>
	taxiRegionsArray[TR_3_DEL_PERRO].vBottomRight = << -1902, -1280, -20 >>  
	taxiRegionsArray[TR_3_DEL_PERRO].iNumDestinations = NUM_DEST_TR_3_DEL_PERRO
	taxiRegionsArray[TR_3_DEL_PERRO].vNeighborRegions = <<ENUM_TO_INT(TR_0_LOS_PUERTA),ENUM_TO_INT(TR_4_LITTLE_SEOUL),ENUM_TO_INT(TR_5_DOWNTOWN)>>
	
	//Region 4 - Little Seoul////////////////////////////////////////////////////////
	taxiRegionsArray[TR_4_LITTLE_SEOUL].sRegName = "Region 4 - Little Seoul"
	taxiRegionsArray[TR_4_LITTLE_SEOUL].vTopLeft = <<  -1902, 315, 20 >>
	taxiRegionsArray[TR_4_LITTLE_SEOUL].vBottomRight = << -373, -1280, 33 >> 
	taxiRegionsArray[TR_4_LITTLE_SEOUL].iNumDestinations = NUM_DEST_TR_4_LITTLE_SEOUL
	taxiRegionsArray[TR_4_LITTLE_SEOUL].vNeighborRegions = <<ENUM_TO_INT(TR_5_DOWNTOWN),ENUM_TO_INT(TR_0_LOS_PUERTA),ENUM_TO_INT(TR_1_SOUTH_LOS_SANTOS)>>
	
	//Region 5 - Downtown////////////////////////////////////////////////////////
	taxiRegionsArray[TR_5_DOWNTOWN].sRegName = "Region 5 -  Downtown"
	taxiRegionsArray[TR_5_DOWNTOWN].vTopLeft = << -373, 315, 20>> 
	taxiRegionsArray[TR_5_DOWNTOWN].vBottomRight = << 708, -1280, -17>> 
	taxiRegionsArray[TR_5_DOWNTOWN].iNumDestinations = NUM_DEST_TR_5_DOWNTOWN
	taxiRegionsArray[TR_5_DOWNTOWN].vNeighborRegions = <<ENUM_TO_INT(TR_0_LOS_PUERTA),ENUM_TO_INT(TR_4_LITTLE_SEOUL),ENUM_TO_INT(TR_2_VINEHOOD_HILLS)>>
	
	//Region 6 - SouthGate////////////////////////////////////////////////////////
	taxiRegionsArray[TR_6_SOUTHGATE].sRegName = "Region 6 - SouthGate"
	taxiRegionsArray[TR_6_SOUTHGATE].vTopLeft = <<  708, 315, 20.0 >>
	taxiRegionsArray[TR_6_SOUTHGATE].vBottomRight = << 1952, -1280, -20.0 >> 
	taxiRegionsArray[TR_6_SOUTHGATE].iNumDestinations = NUM_DEST_TR_6_SOUTHGATE
	taxiRegionsArray[TR_6_SOUTHGATE].vNeighborRegions = <<ENUM_TO_INT(TR_4_LITTLE_SEOUL),ENUM_TO_INT(TR_5_DOWNTOWN),ENUM_TO_INT(TR_2_VINEHOOD_HILLS)>>
	
	//Region 7 - City Hills////////////////////////////////////////////////////////
	taxiRegionsArray[TR_7_CITYHILLS].sRegName = "Region 7 - City Hills"
	taxiRegionsArray[TR_7_CITYHILLS].vTopLeft = << 1952, 315,  35 >> 
	taxiRegionsArray[TR_7_CITYHILLS].vBottomRight = << 4603, -1280, -20.0 >>
	taxiRegionsArray[TR_7_CITYHILLS].iNumDestinations = NUM_DEST_TR_7_CITYHILLS
	taxiRegionsArray[TR_7_CITYHILLS].vNeighborRegions = <<ENUM_TO_INT(TR_5_DOWNTOWN),ENUM_TO_INT(TR_2_VINEHOOD_HILLS),ENUM_TO_INT(TR_6_SOUTHGATE)>>

	//Region 8 - Country Hills////////////////////////////////////////////////////////
	taxiRegionsArray[TR_8_COUTRYHILLS].sRegName = "Region 8 - Country Hills"
	taxiRegionsArray[TR_8_COUTRYHILLS].vTopLeft = <<  -4613, 2550,  35 >> 
	taxiRegionsArray[TR_8_COUTRYHILLS].vBottomRight = << -1902, 315, -20.0 >>
	taxiRegionsArray[TR_8_COUTRYHILLS].iNumDestinations = NUM_DEST_TR_8_COUTRYHILLS
	taxiRegionsArray[TR_8_COUTRYHILLS].vNeighborRegions = <<ENUM_TO_INT(TR_9_BEL_AIR),ENUM_TO_INT(TR_4_LITTLE_SEOUL),ENUM_TO_INT(TR_0_LOS_PUERTA)>>
	
	//Region 9 - Bel Air////////////////////////////////////////////////////////
	taxiRegionsArray[TR_9_BEL_AIR].sRegName = "Region 9 - Bel Air"
	taxiRegionsArray[TR_9_BEL_AIR].vTopLeft = << -1902, 2550,  35 >> 
	taxiRegionsArray[TR_9_BEL_AIR].vBottomRight = << -373, 315, -20.0 >>
	taxiRegionsArray[TR_9_BEL_AIR].iNumDestinations = NUM_DEST_TR_9_BEL_AIR
	taxiRegionsArray[TR_9_BEL_AIR].vNeighborRegions = <<ENUM_TO_INT(TR_4_LITTLE_SEOUL),ENUM_TO_INT(TR_5_DOWNTOWN),ENUM_TO_INT(TR_10_W_HOLLYWOOD)>>
	
	//Region 10 - West Hollywood////////////////////////////////////////////////////////
	taxiRegionsArray[TR_10_W_HOLLYWOOD].sRegName = "Region 10 - West Hollywood"
	taxiRegionsArray[TR_10_W_HOLLYWOOD].vTopLeft = << -373, 2550,  35 >>
	taxiRegionsArray[TR_10_W_HOLLYWOOD].vBottomRight = << 708, 315, -20 >> 
	taxiRegionsArray[TR_10_W_HOLLYWOOD].iNumDestinations = NUM_DEST_TR_10_W_HOLLYWOOD
	taxiRegionsArray[TR_10_W_HOLLYWOOD].vNeighborRegions = <<ENUM_TO_INT(TR_4_LITTLE_SEOUL),ENUM_TO_INT(TR_5_DOWNTOWN),ENUM_TO_INT(TR_6_SOUTHGATE)>>
	
	//Region 11 - Echo Park ////////////////////////////////////////////////////////
	taxiRegionsArray[TR_11_ECHO_PARK].sRegName = "Region 11 - Echo Park"
	taxiRegionsArray[TR_11_ECHO_PARK].vTopLeft = 		<< 708, 2550,  35 >> 
	taxiRegionsArray[TR_11_ECHO_PARK].vBottomRight = 	<< 1952, 315, -20 >> 
	taxiRegionsArray[TR_11_ECHO_PARK].iNumDestinations = NUM_DEST_TR_11_ECHO_PARK
	taxiRegionsArray[TR_11_ECHO_PARK].vNeighborRegions = <<ENUM_TO_INT(TR_10_W_HOLLYWOOD),ENUM_TO_INT(TR_5_DOWNTOWN),ENUM_TO_INT(TR_6_SOUTHGATE)>>
	
	//Region 12 - North City Hills////////////////////////////////////////////////////////
	taxiRegionsArray[TR_12_N_CITYHILLS].sRegName = "Region 12 - North City Hills"
	taxiRegionsArray[TR_12_N_CITYHILLS].vTopLeft = 		<< 1952, 2550,  35 >>
	taxiRegionsArray[TR_12_N_CITYHILLS].vBottomRight = 	<< 4603, 315, -20 >> 
	taxiRegionsArray[TR_12_N_CITYHILLS].iNumDestinations = NUM_DEST_TR_12_N_CITYHILLS
	taxiRegionsArray[TR_12_N_CITYHILLS].vNeighborRegions = <<ENUM_TO_INT(TR_11_ECHO_PARK),ENUM_TO_INT(TR_6_SOUTHGATE),ENUM_TO_INT(TR_6_SOUTHGATE)>>
	
	//Region 13 - Vinewood////////////////////////////////////////////////////////
	taxiRegionsArray[TR_13_VINEWOOD].sRegName = "Region 13 - Vinewood"
	taxiRegionsArray[TR_13_VINEWOOD].vTopLeft = 	<<  -4613, 4435,  35 >>  
	taxiRegionsArray[TR_13_VINEWOOD].vBottomRight = << -1902, 2550, -20.0 >>
	taxiRegionsArray[TR_13_VINEWOOD].iNumDestinations = NUM_DEST_TR_13_VINEWOOD
	taxiRegionsArray[TR_13_VINEWOOD].vNeighborRegions = <<ENUM_TO_INT(TR_9_BEL_AIR),ENUM_TO_INT(TR_8_COUTRYHILLS),ENUM_TO_INT(TR_9_BEL_AIR)>>
	
	//Region 14 - Mountains////////////////////////////////////////////////////////
	taxiRegionsArray[TR_14_MOUNTAINS].sRegName = "Region 14 - Mountains"
	taxiRegionsArray[TR_14_MOUNTAINS].vTopLeft = 	<< -1902, 4435,  35 >> 
	taxiRegionsArray[TR_14_MOUNTAINS].vBottomRight =<< -373, 2550, -20 >>
	taxiRegionsArray[TR_14_MOUNTAINS].iNumDestinations = NUM_DEST_TR_14_MOUNTAINS
	taxiRegionsArray[TR_14_MOUNTAINS].vNeighborRegions = <<ENUM_TO_INT(TR_8_COUTRYHILLS),ENUM_TO_INT(TR_9_BEL_AIR),ENUM_TO_INT(TR_10_W_HOLLYWOOD)>>
	
	//Region 15 - Lakes////////////////////////////////////////////////////////
	taxiRegionsArray[TR_15_LAKES].sRegName = "Region 15 - Lakes"
	taxiRegionsArray[TR_15_LAKES].vTopLeft = 		<< -373, 4435,  35 >> 
	taxiRegionsArray[TR_15_LAKES].vBottomRight = 	<< 708, 2550, 1.0 >>
	taxiRegionsArray[TR_15_LAKES].iNumDestinations = NUM_DEST_TR_15_LAKES
	taxiRegionsArray[TR_15_LAKES].vNeighborRegions = <<ENUM_TO_INT(TR_9_BEL_AIR),ENUM_TO_INT(TR_10_W_HOLLYWOOD),ENUM_TO_INT(TR_11_ECHO_PARK)>>
	
	//Region 16 - Sandy Shores////////////////////////////////////////////////////////
	taxiRegionsArray[TR_16_SANDY_SHORES].sRegName = "Region 16 - Sandy Shores"
	taxiRegionsArray[TR_16_SANDY_SHORES].vTopLeft = 	<< 708, 4435,  35 >> 
	taxiRegionsArray[TR_16_SANDY_SHORES].vBottomRight = << 1952, 2550, -20 >> 
	taxiRegionsArray[TR_16_SANDY_SHORES].iNumDestinations = NUM_DEST_TR_16_SANDY_SHORES
	taxiRegionsArray[TR_16_SANDY_SHORES].vNeighborRegions = <<ENUM_TO_INT(TR_10_W_HOLLYWOOD),ENUM_TO_INT(TR_11_ECHO_PARK),ENUM_TO_INT(TR_15_LAKES)>>
	
	//Region 17 - SS Hills////////////////////////////////////////////////////////
	taxiRegionsArray[TR_17_SS_HILLS].sRegName = "Region 17 - SS Hills"
	taxiRegionsArray[TR_17_SS_HILLS].vTopLeft = 	<< 1952, 4435,  35 >>  
	taxiRegionsArray[TR_17_SS_HILLS].vBottomRight = << 4603, 2550, -20 >> 
	taxiRegionsArray[TR_17_SS_HILLS].iNumDestinations = NUM_DEST_TR_17_SS_HILLS
	taxiRegionsArray[TR_17_SS_HILLS].vNeighborRegions = <<ENUM_TO_INT(TR_11_ECHO_PARK),ENUM_TO_INT(TR_15_LAKES),ENUM_TO_INT(TR_12_N_CITYHILLS)>>
	
	//Region 18 - NW Country////////////////////////////////////////////////////////
	taxiRegionsArray[TR_18_NW_COUNTRY].sRegName = "Region 18 - NW Country"
	taxiRegionsArray[TR_18_NW_COUNTRY].vTopLeft = 		<< -1902, 7693,  35 >>
	taxiRegionsArray[TR_18_NW_COUNTRY].vBottomRight = 	<< -373, 4435, -20 >> 
	taxiRegionsArray[TR_18_NW_COUNTRY].iNumDestinations = NUM_DEST_TR_18_NW_COUNTRY
	taxiRegionsArray[TR_18_NW_COUNTRY].vNeighborRegions = <<ENUM_TO_INT(TR_13_VINEWOOD),ENUM_TO_INT(TR_14_MOUNTAINS),ENUM_TO_INT(TR_15_LAKES)>>

	//Region 19 - N Country////////////////////////////////////////////////////////
	taxiRegionsArray[TR_19_N_COUNTRY].sRegName = "Region 19 - N Country"
	taxiRegionsArray[TR_19_N_COUNTRY].vTopLeft =	 << -373, 7693,  35 >>  
	taxiRegionsArray[TR_19_N_COUNTRY].vBottomRight = << 708, 4435, -20 >>
	taxiRegionsArray[TR_19_N_COUNTRY].iNumDestinations = NUM_DEST_TR_19_N_COUNTRY
	taxiRegionsArray[TR_19_N_COUNTRY].vNeighborRegions = <<ENUM_TO_INT(TR_18_NW_COUNTRY),ENUM_TO_INT(TR_20_E_COUNTRY),ENUM_TO_INT(TR_15_LAKES)>>

	//Region 20 - E Country////////////////////////////////////////////////////////
	taxiRegionsArray[TR_20_E_COUNTRY].sRegName = "Region 20 - E Country"
	taxiRegionsArray[TR_20_E_COUNTRY].vTopLeft = 	<< 708, 7693,  35 >>  
	taxiRegionsArray[TR_20_E_COUNTRY].vBottomRight = << 1952, 4435, -20 >>
	taxiRegionsArray[TR_20_E_COUNTRY].iNumDestinations = NUM_DEST_TR_20_E_COUNTRY
	taxiRegionsArray[TR_20_E_COUNTRY].vNeighborRegions = <<ENUM_TO_INT(TR_19_N_COUNTRY),ENUM_TO_INT(TR_21_COUNTRYSIDE),ENUM_TO_INT(TR_16_SANDY_SHORES)>>

	//Region 21 - CountrySide////////////////////////////////////////////////////////
	taxiRegionsArray[TR_21_COUNTRYSIDE].sRegName = "Region 21 - CountrySide"
	taxiRegionsArray[TR_21_COUNTRYSIDE].vTopLeft = 		<< 1952, 7693,  35 >> 
	taxiRegionsArray[TR_21_COUNTRYSIDE].vBottomRight = << 4603, 4435, 1.0 >>
	taxiRegionsArray[TR_21_COUNTRYSIDE].iNumDestinations = NUM_DEST_TR_21_COUNTRYSIDE
	taxiRegionsArray[TR_21_COUNTRYSIDE].vNeighborRegions = <<ENUM_TO_INT(TR_17_SS_HILLS),ENUM_TO_INT(TR_20_E_COUNTRY),ENUM_TO_INT(TR_16_SANDY_SHORES)>>
	
	//Region 8 - Special POIs////////////////////////////////////////////////////////
	//The min/max vectors are the NW & SE tip of the entire world.	POIs can be anywhere in the world. Must figure out how to optomizek
	taxiRegionsArray[TR_22_SPECIAL_POIS].sRegName = "Region 8 - Special POIs"
	taxiRegionsArray[TR_22_SPECIAL_POIS].vTopLeft =  << -4613, 7693, 20 >>					
	taxiRegionsArray[TR_22_SPECIAL_POIS].vBottomRight = << 4603, -3533, -20 >>				
	taxiRegionsArray[TR_22_SPECIAL_POIS].iNumDestinations = NUM_DEST_TR_22_SPECIAL_POIS
	taxiRegionsArray[TR_22_SPECIAL_POIS].vNeighborRegions = <<ENUM_TO_INT(TR_1_SOUTH_LOS_SANTOS),ENUM_TO_INT(TR_2_VINEHOOD_HILLS),ENUM_TO_INT(TR_3_DEL_PERRO)>>
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"INIT_TAXI_REGIONS - SUCCESS")
ENDPROC
//// PURPOSE: Pass it a point in the world and it will use the look at and dot product to figure out if it's in front of the player or not
 ///    
 /// PARAMS:
 ///    vTestVector - the point to test
 ///    fScaleForward - how much to scale the players forward vector by when considering what's in front
 /// RETURNS: TRUE if the point is in front of the player, false otherwise
 ///    

FUNC BOOL IS_POINT_IN_FRONT_OF_PLAYER(VECTOR vTestVector, FLOAT fScaleForward = 5.0)
	
	VECTOR vPlayerForward, vPlayerForwardNormalized, vPlayerPos, vPlayerToTarget, vPlayerToTargetNormalized, vScaledPlayerForward
	FLOAT fPlyrFwdDotPlyrToTarget
	
	//Get player forward
	vPlayerForward = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
	
	//ZERO out Z for now cause we don't care about height in our checks 
	vPlayerForward.z = 0
	
	//Project it forward a bit so that we get points at least x ahead of the player to avoid points directly to the side.
	vScaledPlayerForward.x = vPlayerForward.x * fScaleForward 
	vScaledPlayerForward.y = vPlayerForward.y * fScaleForward
	
	//Normalize
	vPlayerForwardNormalized =  NORMALISE_VECTOR(vPlayerForward)
	
	//Get Player To Target Vector
	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	vPlayerPos += vScaledPlayerForward
	
	vPlayerToTarget = vTestVector - vPlayerPos
	vPlayerToTarget.z = 0
	
	vPlayerToTargetNormalized = NORMALISE_VECTOR(vPlayerToTarget)
	
	//Dot Product of Player Fwd w/ Player To Target
	fPlyrFwdDotPlyrToTarget = DOT_PRODUCT(vPlayerForwardNormalized, vPlayerToTargetNormalized)
	
	//Don't want anything with a dot product < 0.2 because it would be directly to the side or 
	IF fPlyrFwdDotPlyrToTarget > 0.1
		CDEBUG1LN(DEBUG_OJ_TAXI,"IS_TAXI_POINT_IN_FRONT_OF_PLAYER - Return TRUE")
		RETURN TRUE
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"IS_TAXI_POINT_IN_FRONT_OF_PLAYER - Return FALSE")
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE: 
///    
/// PARAMS:
///    whichRegion - enum value of the region you're checking for
/// RETURNS: TRUE if the player is 
///    
FUNC BOOL IS_PLAYER_IN_TAXI_REGION(TAXI_REG_INFO &txRegArry[], TAXI_REGIONS whichRegion)
	
	IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(),txRegArry[whichRegion].vTopLeft, txRegArry[whichRegion].vBottomRight,FALSE,FALSE)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC
/// PURPOSE: Find out what region the player is in
///    
/// RETURNS: Returns the region the player is in
///    
FUNC TAXI_REGIONS GET_PLAYER_TAXI_REGION(TAXI_REG_INFO &txRegArry[])
	INT iTemp 
	REPEAT TR_NUM_REGIONS iTemp 
		IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(),txRegArry[iTemp].vTopLeft, txRegArry[iTemp].vBottomRight,FALSE,FALSE)
			
			CDEBUG1LN(DEBUG_OJ_TAXI,"Player is in ","", txRegArry[iTemp].sRegName)
			
			RETURN INT_TO_ENUM(TAXI_REGIONS, iTemp)
		ENDIF
	ENDREPEAT
	//Default
	RETURN TR_22_SPECIAL_POIS
ENDFUNC
/// PURPOSE: Rolls a die 3 and picks a TAXI_REGIONS from the vNeighborRegions 
///    
/// PARAMS:
///    iDist - Represents how far away you want to jump regions. For now 1 means same region (use default to find a good pickup point), > 1 means pull a good DROPOFF Point from the vNeighborRegions
/// RETURNS:
///    
FUNC TAXI_REGIONS GET_TAXI_REGION_FROM_DISTANCE(TAXI_REG_INFO &txRegArry[], INT iDist = 1)
	
	TAXI_REGIONS currentPlayerRegion = GET_PLAYER_TAXI_REGION(txRegArry)
	
	INT iTemp, iRegionID
	
	IF iDist > 1
		iTemp  = GET_RANDOM_INT_IN_RANGE() % 3
		
		IF iTemp = 0 
			//Have to use FLOOR because vector coords are FLOATS, and INT_TO_ENUM only likes INTs
			iRegionID = FLOOR(txRegArry[currentPlayerRegion].vNeighborRegions.x)
		ELIF iTemp = 1 
			iRegionID = FLOOR(txRegArry[currentPlayerRegion].vNeighborRegions.y)
		ELSE
			iRegionID = FLOOR(txRegArry[currentPlayerRegion].vNeighborRegions.z)
		ENDIF
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_REGION_FROM_DISTANCE -  ","", txRegArry[iRegionID].sRegName)

		RETURN INT_TO_ENUM(TAXI_REGIONS,iRegionID)
	
	//If there is any garbage values passed or 1 is passed return current region
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_REGION_FROM_DISTANCE -  ","", txRegArry[currentPlayerRegion].sRegName)
		
		RETURN currentPlayerRegion
	ENDIF
	
ENDFUNC
/// PURPOSE: Returns the num of destinations for the region the player is currently in
///    
/// PARAMS:
///    txRegArr - TAXI_REG_INFO regions data array
/// RETURNS:
///    
FUNC INT GET_CURRENT_TAXI_REGION_NUM_LOCATIONS(TAXI_REG_INFO &txRegArr[])
	RETURN txRegArr[GET_PLAYER_TAXI_REGION(txRegArr)].iNumDestinations
ENDFUNC
/// PURPOSE: Returns the num of destinations for the passed in region
///    
/// PARAMS:
///    txRegArr - TAXI_REG_INFO regions data array
///    whichRegion - whichRegion to get the info for, TR_CURRENT_REGION if you want the current
/// RETURNS:
///    
FUNC INT GET_TAXI_REGION_NUM_LOCATIONS(TAXI_REG_INFO &txRegArr[], TAXI_REGIONS whichRegion = TR_CURRENT_REGION)
	IF whichRegion = TR_CURRENT_REGION
		RETURN GET_CURRENT_TAXI_REGION_NUM_LOCATIONS(txRegArr)
	ELSE
		RETURN txRegArr[GET_PLAYER_TAXI_REGION(txRegArr)].iNumDestinations
	ENDIF
ENDFUNC

/// PURPOSE: returns a random point from within a specified region
///    
/// PARAMS:
///    txRegArr - TAXI_REG_INFO regions data array
///    whichRegion - the TAXI_REGIONS you want the point from
/// RETURNS:
///    
FUNC VECTOR GET_TAXI_LOCATION_FROM_REGION(TAXI_REG_INFO &txRegArr[], TAXI_REGIONS whichRegion, INT i = -1)
	
	//INT i 
	VECTOR vReturnVector = NULL_VECTOR()
	
	//Get a random index into the array
	IF (i = -1)
		i  = GET_RANDOM_INT_IN_RANGE() % txRegArr[whichRegion].iNumDestinations
	ENDIF

	SWITCH whichRegion
		CASE TR_0_LOS_PUERTA
			 vReturnVector = taxiDest_0_LosPuerta[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_0_LOS_PUERTA- i = ", i)
		BREAK
		
		CASE TR_1_SOUTH_LOS_SANTOS
			vReturnVector = taxiDest_1_SouthLosSantos[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_1_SOUTH_LOS_SANTOS - i = ", i)

		BREAK
		
		CASE TR_2_VINEHOOD_HILLS
			vReturnVector = taxiDest_2_VinewoodHills[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_2_VINEHOOD_HILLS - i = ", i)

		BREAK
		
		CASE TR_3_DEL_PERRO
			vReturnVector = taxiDest_3_DelPerro[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_3_DEL_PERRO - i = ", i)

		BREAK
		
		CASE TR_4_LITTLE_SEOUL
			vReturnVector = taxiDest_4_LittleSeoul[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_4_LITTLE_SEOUL - i = ", i)

		BREAK
		
		CASE TR_5_DOWNTOWN
			vReturnVector = taxiDest_5_Downtown[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_5_DOWNTOWN- i = ", i)

		BREAK
		
		CASE TR_6_SOUTHGATE
			vReturnVector = taxiDest_6_Southgate[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_6_SOUTHGATE- i = ", i)

		BREAK
		
		CASE TR_7_CITYHILLS
			vReturnVector = taxiDest_7_CityHills[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_7_CITYHILLS- i = ", i)

		BREAK
	
		CASE TR_8_COUTRYHILLS
			 vReturnVector = taxiDest_8_CountryHills[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_8_COUTRYHILLS- i = ", i)
		BREAK
		
		CASE TR_9_BEL_AIR
			vReturnVector = taxiDest_9_BelAir[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_9_BEL_AIR - i =", i)

		BREAK
		
		CASE TR_10_W_HOLLYWOOD
			vReturnVector = taxiDest_10_WHollywood[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_10_W_HOLLYWOOD - i = ", i)

		BREAK
		
		CASE TR_11_ECHO_PARK
			vReturnVector = taxiDest_11_EchoPark[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_11_ECHO_PARK - i = ", i)

		BREAK
		
		CASE TR_12_N_CITYHILLS
			vReturnVector = taxiDest_12_NCityHills[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_12_N_CITYHILLS - i = ", i)

		BREAK
		
		CASE TR_13_VINEWOOD
			vReturnVector = taxiDest_13_Vinewood[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_13_VINEWOOD- i = ", i)

		BREAK
		
		CASE TR_14_MOUNTAINS
			vReturnVector = taxiDest_14_Mountains[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_14_MOUNTAINS- i = ", i)

		BREAK
		
		CASE TR_15_LAKES
			vReturnVector = taxiDest_15_Lakes[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_15_LAKES- i = ", i)

		BREAK
		
		CASE TR_16_SANDY_SHORES
			 vReturnVector = taxiDest_16_SandyShores[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_16_SANDY_SHORES- i = ", i)
		BREAK
		
		CASE TR_17_SS_HILLS
			vReturnVector = taxiDest_17_SSHills[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_17_SS_HILLS - i = ", i)

		BREAK
		
		CASE TR_18_NW_COUNTRY
			vReturnVector = taxiDest_18_NWCountry[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_18_NW_COUNTRY - i = ", i)

		BREAK
		
		CASE TR_19_N_COUNTRY
			vReturnVector = taxiDest_19_NCountry[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_19_N_COUNTRY - i = ", i)

		BREAK
		
		CASE TR_20_E_COUNTRY
			vReturnVector = taxiDest_20_ECountry[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_20_E_COUNTRY - i = ", i)

		BREAK
		
		CASE TR_21_COUNTRYSIDE
			vReturnVector = taxiDest_21_CountrySide[i]
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_LOCATION_FROM_REGION - Region  = TR_21_COUNTRYSIDE- i = ", i)

		BREAK
	
	
	ENDSWITCH
	
	RETURN vReturnVector
	
ENDFUNC

//INFO: This wraps the function GET_POSITION_BY_SIDE_OF_ROAD with GET_SAFE_COORD_FOR_PED into a gated function based on a timer since it's a relatively slow function
//	It then makes sure that the position is safe for a ped to stand with GET_SAFE_COORD_FOR_PED
//	If the input node position is unsuitable, it will search 30m for a valid node.
//PARAM NOTES:	vInputNodePosition	- should be the position of a road node, 
//				iDirection			- should be a direction (0=forwards, 1=back, -1=doesn't matter)
//				vOutPositionByRoad	- the roadside position is passed back in this vector
//				tDelayTimer 		- a timer to throttle the function
//RETURNS: A boolean to indicate whether a position was found or not.

FUNC BOOL GET_SAFE_POSITION_BY_SIDE_OF_ROAD_PERIODIC(TaxiStruct & myTaxiData, VECTOR vInputNodePosition, VECTOR &vOutPositionByRoad)
	
	//VECTOR vTempSafePt = NULL_VECTOR()
	
	//IF GET_POSITION_BY_SIDE_OF_ROAD(vInputNodePosition, iDirection, vOutPositionByRoad)
	
		//This is here because GET_SAFE_COORD_FOR_PED is slow and should not be called every frame if it fails
		IF TAXI_TIMER_DO_WHEN_READY(myTaxiData, TT_REGION_FIND_POINT,0.05)
			
			//vTempSafePt = vOutPositionByRoad
			
			//IF GET_SAFE_COORD_FOR_PED(vTempSafePt,FALSE,vOutPositionByRoad)
			IF GET_SAFE_COORD_FOR_PED(vInputNodePosition,FALSE,vOutPositionByRoad)				
				CDEBUG1LN(DEBUG_OJ_TAXI,"GET_SAFE_POSITION_BY_SIDE_OF_ROAD_PERIODIC - Found Good Point for Ped - ", "",vOutPositionByRoad , " \nThat is offset by diff of ","",GET_DISTANCE_BETWEEN_COORDS(vInputNodePosition,vOutPositionByRoad))
				
				TAXI_CANCEL_TIMERS(myTaxiData, TT_REGION_FIND_POINT)
				RETURN TRUE
			
			ELSE
			
				TAXI_RESET_TIMERS(myTaxiData, TT_REGION_FIND_POINT)
				CDEBUG1LN(DEBUG_OJ_TAXI,"GET_SAFE_POSITION_BY_SIDE_OF_ROAD_PERIODIC - Couldn't find a safe coord for the ped....waiting a tick")

			ENDIF
	
		ENDIF
		
	//ENDIF	
	
	RETURN FALSE
ENDFUNC
 

/// PURPOSE:Returns a pickup location. If DEFAULT param of 1 is passed, it'll pick from your current region, otherwise it will pick from a neighbor region
///    
/// PARAMS: 
///    vPickupLocation - The pickup location vector to fill in
///    iCounter - 		A counter for the switch statement (was having issues with locally declared ones so I ask for a ref)
///    iDistance - 		1 - Current Region / > 1 means one of your neighboring regions
/// RETURNS:	True when it finds a good one
///    
FUNC BOOL GET_TAXI_PICKUP_LOCATION_FROM_DISTANCE(TaxiStruct & myTaxiData, TAXI_REG_INFO &txRegArry[], VECTOR &vPickupLocation, INT &iCounter, INT iDistance = 1)
	
	VECTOR vImprovedPickupLoc
	FLOAT fGroundZ
	
	SWITCH iCounter
		//Get a location
		CASE 0
			vPickupLocation = GET_TAXI_LOCATION_FROM_REGION(txRegArry, GET_TAXI_REGION_FROM_DISTANCE(txRegArry, iDistance))
			//vPickupLocation = taxiDest_6_Southgate[2]
			
			TAXI_START_TIMER(TT_REGION_TIME_POINT)
			iCounter++
			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_PICKUP_LOCATION_FROM_DISTANCE - Pickup Found - ", "",vPickupLocation )
		BREAK
		
		//Make sure it's in front of the player and not visible otherwise grab another one.
		CASE 1
			IF IS_POINT_IN_FRONT_OF_PLAYER(vPickupLocation)
			AND WOULD_ENTITY_BE_OCCLUDED(TAXI_GET_TAXI_MODEL_NAME(),vPickupLocation)	
			
				TAXI_RESET_TIMERS(myTaxiData, TT_REGION_FIND_POINT)
				TAXI_RESET_TIMERS(myTaxiData, TT_REGION_FIND_FAIL)
		
				iCounter++
				CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_PICKUP_LOCATION_FROM_DISTANCE - Point is not visible & in front")
				
			ELIF TAXI_TIMER_DO_WHEN_READY(myTaxiData, TT_REGION_FIND_POINT,1.5)
			AND WOULD_ENTITY_BE_OCCLUDED(TAXI_GET_TAXI_MODEL_NAME(),vPickupLocation)	
				TAXI_RESET_TIMERS(myTaxiData, TT_REGION_FIND_POINT)
				iCounter++
				CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_PICKUP_LOCATION_FROM_DISTANCE - Don't care that it's not in front of player")
			
			ELSE 
				iCounter = 0
				CDEBUG1LN(DEBUG_OJ_TAXI,"Sending to grab another point cause point is visible or not in front")
			ENDIF
		BREAK
		
		//Make it safe for the ped
		CASE 2
			IF GET_SAFE_POSITION_BY_SIDE_OF_ROAD_PERIODIC(vPickupLocation,vImprovedPickupLoc)
				
				//This is a failsafe as I've seen this fall thru
				REQUEST_COLLISION_AT_COORD(vImprovedPickupLoc)
				GET_GROUND_Z_FOR_3D_COORD(vImprovedPickupLoc,fGroundZ)
				vPickupLocation = 	vImprovedPickupLoc 
				vPickupLocation.z = fGroundZ
				iCounter = 0
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"Pickup Location Found :", "",vPickupLocation, "" ," Time Taken : ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_REGION_TIME_POINT))
				
				RETURN TRUE
			ELIF TAXI_TIMER_DO_WHEN_READY(myTaxiData, TT_REGION_FIND_FAIL,1.5)
				
				iCounter = 0
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"Timed out Pickup Location Found But Not Guaranteed Safe: ", "",vPickupLocation, "", " Time Taken : ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_REGION_TIME_POINT))
			
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_OJ_TAXI,"Trying to find safe pos by side of road")
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//FUNC BOOL GET_TAXI_PROCEDURAL_PICKUP(TaxiStruct & myTaxiData, TAXI_REG_INFO &txRegArry[] , VECTOR &vDropoffLocation, INT &iCounter, INT iDistance = 2)//, BOOL bCheckForward = FALSE)
//	INT iTemp
//	
//	TAXI_REGIONS eTaxiRegion
//	
//	SWITCH iCounter
//		//Get a location
//		CASE 0
//			
//			
//			eTaxiRegion = GET_TAXI_REGION_FROM_DISTANCE(txRegArry, iDistance)
//			
//			REPEAT txRegArry[eTaxiRegion].iNumDestinations iTemp
//				vDropoffLocation = GET_TAXI_LOCATION_FROM_REGION(txRegArry, eTaxiRegion, iTemp)
//				IF GET_PLAYER_DISTANCE_FROM_LOCATION(vDropoffLocation) <= 500
//				AND GET_PLAYER_DISTANCE_FROM_LOCATION(vDropoffLocation) > 50 
//					
//				ELIF GET_PLAYER_DISTANCE_FROM_LOCATION(vDropoffLocation) > 50 
//					
//				ENDIF
//			ENDREPEAT 
//			
//			IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_REGION_FIND_POINT)
//				TAXI_RESET_TIMERS(myTaxiData, TT_REGION_FIND_POINT,0,TRUE)
//			ENDIF
//			iCounter++
//			CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_DROPOFF_LOCATION_FROM_DISTANCE - Dropoff Found From Regions  @ ", "",vDropoffLocation )
//		BREAK
//		
//		CASE 1
//			//If too much time has passed and a point isn't satisfied , just use the current one.
//			IF TAXI_TIMER_DO_WHEN_READY(myTaxiData, TT_REGION_FIND_POINT,3.0)	
//			AND GET_PLAYER_DISTANCE_FROM_LOCATION(vDropoffLocation) >= 50
//				TAXI_RESET_TIMERS(myTaxiData, TT_REGION_FIND_POINT,0)
//				iCounter++
//				CDEBUG1LN(DEBUG_OJ_TAXI,"Destination Timed Out, Proceeding with a poor point")
//			
//			ELIF GET_PLAYER_DISTANCE_FROM_LOCATION(vDropoffLocation) <= 500	//TAXI_REGION_MIN_RIDE_DISTANCE
//			AND GET_PLAYER_DISTANCE_FROM_LOCATION(vDropoffLocation) > 50 	//100 
//		
//				TAXI_RESET_TIMERS(myTaxiData, TT_REGION_FIND_POINT,0,TRUE)
//				iCounter++
//				CDEBUG1LN(DEBUG_OJ_TAXI,"Point is in front of player & not visible")
//				
//			ELSE 
//				IF NOT IS_POINT_IN_FRONT_OF_PLAYER(vDropoffLocation)
//					CDEBUG1LN(DEBUG_OJ_TAXI,"Point is not in front of player")
//				ENDIF
//				
//				CDEBUG1LN(DEBUG_OJ_TAXI,"Distance is ", GET_PLAYER_DISTANCE_FROM_LOCATION(vDropoffLocation))
//				
//				iCounter = 0
//				CDEBUG1LN(DEBUG_OJ_TAXI,"Sending to grab another point")
//			ENDIF
//		BREAK
//		
//		
//		
//	ENDSWITCH
//ENDFUNC

PROC CHECK_SPECIFIC_REGION_FOR_PICKUP(TAXI_REG_INFO &txRegArry[], TAXI_REGIONS eRegionCheck, VECTOR &vDropoffLocation, BOOL & bRegionFound, INT & iWhichLocation)
	INT idx
	VECTOR vTempVector
	
	REPEAT txRegArry[eRegionCheck].iNumDestinations idx
		vTempVector = GET_TAXI_LOCATION_FROM_REGION(txRegArry, eRegionCheck, idx)
		IF GET_GPS_DISTANCE(vTempVector) < GET_GPS_DISTANCE(vDropoffLocation)
			IF g_bTaxiFirstProcedural
			AND ( (ENUM_TO_INT(eRegionCheck) = g_iLastTaxiPickupRegion[0] AND idx = g_iLastTaxiPickupLocation[0])
				OR (ENUM_TO_INT(eRegionCheck) = g_iLastTaxiPickupRegion[1] AND idx = g_iLastTaxiPickupLocation[1])
				OR (ENUM_TO_INT(eRegionCheck) = g_iLastTaxiPickupRegion[2] AND idx = g_iLastTaxiPickupLocation[2])
				OR (ENUM_TO_INT(eRegionCheck) = g_iLastTaxiPickupRegion[3] AND idx = g_iLastTaxiPickupLocation[3]) )
			
				CDEBUG1LN(DEBUG_OJ_TAXI,"this location was used too recently, disqualifying. region: ", eRegionCheck, ". location: ", idx)
			ELSE
				IF GET_PLAYER_DISTANCE_FROM_LOCATION(vTempVector) > 40
					vDropoffLocation = vTempVector
					iWhichLocation = idx
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"idx = ", idx)
					CDEBUG1LN(DEBUG_OJ_TAXI,"NEW possible pickup at ", vDropoffLocation)
					CDEBUG1LN(DEBUG_OJ_TAXI,"GPS distance is = ", GET_GPS_DISTANCE(vDropoffLocation))
					CDEBUG1LN(DEBUG_OJ_TAXI,"crow distance is = ", GET_PLAYER_DISTANCE_FROM_LOCATION(vDropoffLocation))
					IF GET_GPS_DISTANCE(vDropoffLocation) <= 0.6
						iWhichLocation = idx
						CDEBUG1LN(DEBUG_OJ_TAXI,"distance is also < 500, looks good to go ")
						bRegionFound = TRUE
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"location < 40, too close: ", idx)
				ENDIF
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"this location was not closer ", idx)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE: Returns a pickup location. If DEFAULT param of 1 is passed, it'll pick from your current region, otherwise it will pick from a neighbor region
///    
/// PARAMS:
///    iDistance - 1 - Current Region / > 1 means one of your neighboring regions
/// RETURNS: A valid pickup point
///    
FUNC BOOL GET_TAXI_DROPOFF_LOCATION_FROM_DISTANCE(TAXI_REG_INFO &txRegArry[] , VECTOR &vDropoffLocation, INT iDistance = 2)
	
	TAXI_REGIONS eRegionCheck, eRegionCurrent
	BOOL bRegionFound, bLeft, bRight, bTop, bBottom
	INT iWhichLocation
	
	eRegionCurrent = GET_TAXI_REGION_FROM_DISTANCE(txRegArry, iDistance)
	eRegionCheck = eRegionCurrent
	
	//check your region first
	CHECK_SPECIFIC_REGION_FOR_PICKUP(txRegArry, eRegionCheck, vDropoffLocation, bRegionFound, iWhichLocation)
	
	// still no point, check left
	IF NOT bRegionFound
		IF ENUM_TO_INT(eRegionCurrent) != 0 AND ENUM_TO_INT(eRegionCurrent) != 3 AND ENUM_TO_INT(eRegionCurrent) != 8 AND ENUM_TO_INT(eRegionCurrent) != 13 AND ENUM_TO_INT(eRegionCurrent) != 18
			CDEBUG1LN(DEBUG_OJ_TAXI,"let's check the left")
			eRegionCheck = INT_TO_ENUM(TAXI_REGIONS, ENUM_TO_INT(eRegionCurrent) - 1)
			bLeft = TRUE
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"this is a left most region, not checking left")
		ENDIF
		IF bLeft
			CHECK_SPECIFIC_REGION_FOR_PICKUP(txRegArry, eRegionCheck, vDropoffLocation, bRegionFound, iWhichLocation)
		ENDIF
	ENDIF
	
	// still no point, check right
	IF NOT bRegionFound
		IF ENUM_TO_INT(eRegionCurrent) != 2 AND ENUM_TO_INT(eRegionCurrent) != 7 AND ENUM_TO_INT(eRegionCurrent) != 12 AND ENUM_TO_INT(eRegionCurrent) != 17 AND ENUM_TO_INT(eRegionCurrent) != 21
			CDEBUG1LN(DEBUG_OJ_TAXI,"let's check the right")
			eRegionCheck = INT_TO_ENUM(TAXI_REGIONS, ENUM_TO_INT(eRegionCurrent) + 1)
			bRight = TRUE
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"this is a right most region, not checking right")
		ENDIF
		IF bRight
			CHECK_SPECIFIC_REGION_FOR_PICKUP(txRegArry, eRegionCheck, vDropoffLocation, bRegionFound, iWhichLocation)
		ENDIF
	ENDIF
	
	// still no point, check top
	IF NOT bRegionFound
		IF ENUM_TO_INT(eRegionCurrent) != 18 AND ENUM_TO_INT(eRegionCurrent) != 19 AND ENUM_TO_INT(eRegionCurrent) != 20 AND ENUM_TO_INT(eRegionCurrent) != 21 AND ENUM_TO_INT(eRegionCurrent) != 13
			CDEBUG1LN(DEBUG_OJ_TAXI,"let's check the top")
			IF (ENUM_TO_INT(eRegionCurrent) >= 14 AND ENUM_TO_INT(eRegionCurrent) <=16)
			OR (ENUM_TO_INT(eRegionCurrent) >= 0 AND ENUM_TO_INT(eRegionCurrent) <=2)
				eRegionCheck = INT_TO_ENUM(TAXI_REGIONS, ENUM_TO_INT(eRegionCurrent) + 4)
			ELSE
				eRegionCheck = INT_TO_ENUM(TAXI_REGIONS, ENUM_TO_INT(eRegionCurrent) + 5)
			ENDIF
			bTop = TRUE
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"this is a top most region, not checking top")
		ENDIF
		IF bTop
			CHECK_SPECIFIC_REGION_FOR_PICKUP(txRegArry, eRegionCheck, vDropoffLocation, bRegionFound, iWhichLocation)
		ENDIF
	ENDIF
	
	// last chance, check bottom
	IF NOT bRegionFound
		IF ENUM_TO_INT(eRegionCurrent) != 3 AND ENUM_TO_INT(eRegionCurrent) != 0 AND ENUM_TO_INT(eRegionCurrent) != 1 AND ENUM_TO_INT(eRegionCurrent) != 2 AND ENUM_TO_INT(eRegionCurrent) != 7
			CDEBUG1LN(DEBUG_OJ_TAXI,"let's check the Bottom")
			IF (ENUM_TO_INT(eRegionCurrent) >= 18 AND ENUM_TO_INT(eRegionCurrent) <= 21)
			OR (ENUM_TO_INT(eRegionCurrent) >= 4 AND ENUM_TO_INT(eRegionCurrent) <= 6)
				eRegionCheck = INT_TO_ENUM(TAXI_REGIONS, ENUM_TO_INT(eRegionCurrent) - 4)
			ELSE
				eRegionCheck = INT_TO_ENUM(TAXI_REGIONS, ENUM_TO_INT(eRegionCurrent) - 5)
			ENDIF
			bBottom = TRUE
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"this is a Bottom most region, not checking Bottom")
		ENDIF
		IF bBottom
			CHECK_SPECIFIC_REGION_FOR_PICKUP(txRegArry, eRegionCheck, vDropoffLocation, bRegionFound, iWhichLocation)
		ENDIF
	ENDIF
	
	IF bRegionFound
		CDEBUG1LN(DEBUG_OJ_TAXI,"proper pickup was found")
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"no proper pickup found, consider adding pickups to this location")
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"official pickup is at ", vDropoffLocation)
	
	g_iLastTaxiPickupRegion[3] = g_iLastTaxiPickupRegion[2]
	g_iLastTaxiPickupRegion[2] = g_iLastTaxiPickupRegion[1]
	g_iLastTaxiPickupRegion[1] = g_iLastTaxiPickupRegion[0]
	g_iLastTaxiPickupRegion[0] = ENUM_TO_INT(eRegionCheck)
	
	g_iLastTaxiPickupLocation[3] = g_iLastTaxiPickupLocation[2]
	g_iLastTaxiPickupLocation[2] = g_iLastTaxiPickupLocation[1]
	g_iLastTaxiPickupLocation[1] = g_iLastTaxiPickupLocation[0]
	g_iLastTaxiPickupLocation[0] = iWhichLocation
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"g_iLastTaxiPickupLocation = ", g_iLastTaxiPickupLocation[0])
	CDEBUG1LN(DEBUG_OJ_TAXI,"g_iLastTaxiPickupRegion = ", g_iLastTaxiPickupRegion[0])
	
	IF NOT g_bTaxiFirstProcedural
		CDEBUG1LN(DEBUG_OJ_TAXI,"first procedural was not set, setting")
		g_bTaxiFirstProcedural = TRUE
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"first procedural was already set")
	ENDIF
	
	RETURN TRUE
			
ENDFUNC

//DEBUG REGION FUNCTIONS
#IF IS_DEBUG_BUILD


PROC TAXI_DEBUG_REGION_POINT(TAXI_REG_INFO &txRegArry[], BOOL bIsDropOff = FALSE)//,FLOAT &fDist1, FLOAT &fDist2, debugTaxiColors dtc_temp1, debugTaxiColors dtc_temp2)
	//Keep CurrentRegion up to date
	IF iDebugSwitchCounter > 0
		IF NOT IS_PLAYER_IN_TAXI_REGION(txRegArry,debugCurrentRegion)
			iDebugSwitchCounter = 0
			CDEBUG1LN(DEBUG_OJ_TAXI,"Player left region : ", "", txRegArry[debugCurrentRegion].sRegName)
			SCRIPT_ASSERT("Player left region." )
		
		ENDIF
	ENDIF
	
	SWITCH iDebugSwitchCounter
		//First find what region the player located in.
		CASE 0
			debugCurrentRegion = GET_PLAYER_TAXI_REGION(txRegArry)
			
			//Clean Blips if they exist
			REPEAT NUM_DEST_TR_4_LITTLE_SEOUL iTempIter
				IF DOES_BLIP_EXIST(blipTemp[iTempIter])
					REMOVE_BLIP(blipTemp[iTempIter])
				ENDIF
				IF DOES_BLIP_EXIST(blipClosest)
					REMOVE_BLIP(blipClosest)
				ENDIF
				
				IF DOES_BLIP_EXIST(blipPickup)
					REMOVE_BLIP(blipPickup)
				ENDIF
			ENDREPEAT
			iDebugSwitchCounter++
		BREAK
		
		//Than blip all points in that region
		CASE 1
			SWITCH debugCurrentRegion
				CASE TR_0_LOS_PUERTA
					REPEAT NUM_DEST_TR_0_LOS_PUERTA iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_0_LosPuerta[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_1_SOUTH_LOS_SANTOS
					REPEAT NUM_DEST_TR_1_SOUTH_LOS_SANTOS iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_1_SouthLosSantos[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_2_VINEHOOD_HILLS
					REPEAT NUM_DEST_TR_2_VINEWOOD_HILLS iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_2_VinewoodHills[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_3_DEL_PERRO
					REPEAT NUM_DEST_TR_3_DEL_PERRO iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_3_DelPerro[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK

				
				CASE TR_4_LITTLE_SEOUL
					REPEAT NUM_DEST_TR_4_LITTLE_SEOUL iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_4_LittleSeoul[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_5_DOWNTOWN
					REPEAT NUM_DEST_TR_5_DOWNTOWN iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_5_Downtown[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_6_SOUTHGATE
					REPEAT NUM_DEST_TR_6_SOUTHGATE iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_6_Southgate[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_7_CITYHILLS
					REPEAT NUM_DEST_TR_7_CITYHILLS iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_7_CityHills[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				CASE TR_8_COUTRYHILLS
					REPEAT NUM_DEST_TR_8_COUTRYHILLS iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_8_CountryHills[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_9_BEL_AIR
					REPEAT NUM_DEST_TR_9_BEL_AIR iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_9_BelAir[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_10_W_HOLLYWOOD
					REPEAT NUM_DEST_TR_10_W_HOLLYWOOD iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_10_WHollywood[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_11_ECHO_PARK
					REPEAT NUM_DEST_TR_11_ECHO_PARK iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_11_EchoPark[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK

				CASE TR_12_N_CITYHILLS
					REPEAT NUM_DEST_TR_12_N_CITYHILLS iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_12_NCityHills[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_13_VINEWOOD
					REPEAT NUM_DEST_TR_13_VINEWOOD iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_13_Vinewood[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_14_MOUNTAINS
					REPEAT NUM_DEST_TR_14_MOUNTAINS iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_14_Mountains[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_15_LAKES
					REPEAT NUM_DEST_TR_15_LAKES iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_15_Lakes[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_16_SANDY_SHORES
					REPEAT NUM_DEST_TR_16_SANDY_SHORES iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_16_SandyShores[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_17_SS_HILLS
					REPEAT NUM_DEST_TR_17_SS_HILLS iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_17_SSHills[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_18_NW_COUNTRY
					REPEAT NUM_DEST_TR_18_NW_COUNTRY iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_18_NWCountry[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_19_N_COUNTRY
					REPEAT NUM_DEST_TR_19_N_COUNTRY iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_19_NCountry[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK

				CASE TR_20_E_COUNTRY
					REPEAT NUM_DEST_TR_20_E_COUNTRY iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_20_ECountry[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
				CASE TR_21_COUNTRYSIDE
					REPEAT NUM_DEST_TR_21_COUNTRYSIDE iTempIter
						blipTemp[iTempIter] = ADD_BLIP_FOR_COORD(taxiDest_21_CountrySide[iTempIter])
						SET_BLIP_COLOUR(blipTemp[iTempIter],BLIP_COLOUR_WHITE)
					ENDREPEAT
				BREAK
				
			ENDSWITCH
			iDebugSwitchCounter++
		BREAK
		//Figure out initial closest blip if it hasn't been defined yet
		CASE 2
			REPEAT NUM_DEST_TR_4_LITTLE_SEOUL iTempIter
				IF DOES_BLIP_EXIST(blipTemp[iTempIter])
					IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(),GET_BLIP_COORDS(blipTemp[iTempIter])) < FMINdistance
						FMINdistance = GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(),GET_BLIP_COORDS(blipTemp[iTempIter]))
						
						//If one was present before set it back to normal
						IF DOES_BLIP_EXIST(blipClosest)
							REMOVE_BLIP(blipClosest)
						ENDIF
						
						IF DOES_BLIP_EXIST(blipPickup)
							REMOVE_BLIP(blipPickup)
						ENDIF
					
						blipClosest = blipTemp[iTempIter]	
						
						bNewPointFound = TRUE
						
					ENDIF
				ENDIF
		
			ENDREPEAT
			iDebugSwitchCounter++
		BREAK
		//Update closest blip... polls every 10000 millisecs
		CASE 3
			IF TIMERA() > 10000
				REPEAT NUM_DEST_TR_4_LITTLE_SEOUL iTempIter
					IF DOES_BLIP_EXIST(blipTemp[iTempIter])
						IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(),GET_BLIP_COORDS(blipTemp[iTempIter])) < GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(),GET_BLIP_COORDS(blipClosest))
							
							//If one was present before set it back to normal
							IF DOES_BLIP_EXIST(blipClosest)
								REMOVE_BLIP(blipClosest)
							ENDIF
							
							IF DOES_BLIP_EXIST(blipPickup)
								REMOVE_BLIP(blipPickup)
							ENDIF
						
							blipClosest = ADD_BLIP_FOR_COORD(GET_BLIP_COORDS(blipTemp[iTempIter]))
							
							bNewPointFound = TRUE
							CDEBUG1LN(DEBUG_OJ_TAXI,"Grabbed new point")
							
						ENDIF
					ENDIF
			
				ENDREPEAT
				SETTIMERA(0)
				CDEBUG1LN(DEBUG_OJ_TAXI,"Rechecking closest blip")
			ENDIF
			
			
			IF bNewPointFound
				IF NOT DOES_BLIP_EXIST(blipPickup)
					IF DOES_BLIP_EXIST(blipClosest)
						vClosestPoint = GET_BLIP_COORDS(blipClosest)
					ENDIF
					
					//Once closest point is determined, throw it into side of road function
					IF NOT bIsDropOff
						IF GET_SAFE_POSITION_BY_SIDE_OF_ROAD_PERIODIC(vClosestPoint,vTestPickup)
							//If it doesn't exist make it
							IF NOT DOES_BLIP_EXIST(blipClosest)
								blipClosest = ADD_BLIP_FOR_COORD(vClosestPoint)
							ENDIF
							SET_BLIP_FLASHES(blipClosest,TRUE)
							SET_BLIP_COLOUR(blipClosest,BLIP_COLOUR_GREEN)
							SET_BLIP_NAME_FROM_ASCII(blipClosest,"Closest RegionPt")
				
							blipPickup = ADD_BLIP_FOR_COORD(vTestPickup)
							SET_BLIP_COLOUR(blipPickup,BLIP_COLOUR_RED)
							SET_BLIP_NAME_FROM_ASCII(blipClosest,"Pickup Pt")
							bNewPointFound = FALSE
							CDEBUG1LN(DEBUG_OJ_TAXI,"New Pickup Point found")
						ENDIF
					//If it's a Dropoff point throw get closest vehicle node than side of the road	
					ELSE
						IF GET_CLOSEST_VEHICLE_NODE(vClosestPoint,vTestPickup)
							IF GET_POSITION_BY_SIDE_OF_ROAD(vTestPickup,0,vClosestPoint)
								//If it doesn't exist make it
								IF NOT DOES_BLIP_EXIST(blipClosest)
									blipClosest = ADD_BLIP_FOR_COORD(vClosestPoint)
								ENDIF
								SET_BLIP_FLASHES(blipClosest,TRUE)
								SET_BLIP_COLOUR(blipClosest,BLIP_COLOUR_GREEN)
								SET_BLIP_NAME_FROM_ASCII(blipClosest,"Closest RegionPt")
					
								blipPickup = ADD_BLIP_FOR_COORD(vTestPickup)
								SET_BLIP_COLOUR(blipPickup,BLIP_COLOUR_RED)
								SET_BLIP_NAME_FROM_ASCII(blipClosest,"Pickup Pt")
								bNewPointFound = FALSE
								CDEBUG1LN(DEBUG_OJ_TAXI,"New Pickup Point found")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//GOTCHA: TO SEE THESE RENDERED IN THE WORLD HAVE TO BE IN DEBUG AND RAG->SCRIPT->DRAW DEBUG LINES & SPHERES MUST BE CHECKED
			
			//Draw a GREEN sphere around the pickup point OR Position by Side of Road
			IF DOES_BLIP_EXIST(blipClosest)
				DRAW_DEBUG_SPHERE(GET_BLIP_COORDS(blipClosest),2.4,0,200,0,222)
				IF bIsDropOff
					DRAW_DEBUG_TEXT("Side of Road",GET_BLIP_COORDS(blipClosest))
				ELSE
					DRAW_DEBUG_TEXT("PickUp Point",GET_BLIP_COORDS(blipClosest))
				ENDIF
			ENDIF
			//Draw a RED sphere around the new point generated by GET_SAFE_COORD_FOR_PED OR Closest Vehicle Node
			IF DOES_BLIP_EXIST(blipPickup)
				DRAW_DEBUG_SPHERE(GET_BLIP_COORDS(blipPickup),2.4,200,0,0,77)
				IF bIsDropOff
					DRAW_DEBUG_TEXT("Closest V Node",GET_BLIP_COORDS(blipPickup))
				ELSE
					DRAW_DEBUG_TEXT("Safe Coord 4 Ped",GET_BLIP_COORDS(blipPickup))
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
ENDPROC
#ENDIF
//EOF
