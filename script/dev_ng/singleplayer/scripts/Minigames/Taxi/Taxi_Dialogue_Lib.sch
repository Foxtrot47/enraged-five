///Taxi_Dialogue_Lib.sch
///Author : John R. Diaz
///Moving all dialogue into this library for sanity sake, since for the most part it'll be the same. At least for the moment, I might be shooting myself in the foot here, but let's see what happens.
///    
USING "controller_Taxi_lib.sch"
USING "Taxi_Dialogue.sch"
USING "dialogue_public.sch"
USING "hud_text.sch"
USING "taxi_point_driving.sch"
USING "taxi_shared_consts.sch"
USING "taxi_tips.sch"

//Dialogue
CONST_FLOAT		TAXI_DX_DIRECTION_DELAY			5.0					//How long before allowed to give another direction
CONST_FLOAT		TAXI_DX_DELAY					4.0						//How long before being allowed to speak again.
CONST_FLOAT		TAXI_DX_GETOOFF_HWY_DELAY		15.0
CONST_FLOAT		TAXI_DIALOGUE_DELAY				9.0						//How long before the mission barks being allowed to trigger again.
CONST_FLOAT		TAXI_DIALOGUE_MAX_DISTANCE			40.0					//Max Distance that peds can speak to you
CONST_FLOAT		TAXI_DIALOGUE_MIN_DISTANCE			5.0					//Min Distance that peds can hail the cab from 

CONST_INT		TAXI_DIALOGUE_KTW_NUM_CONV_LINES_STR		3
CONST_INT		TAXI_DIALOGUE_KTW_NUM_CONV_LINES_RIGHT		3
CONST_INT		TAXI_DIALOGUE_KTW_NUM_CONV_LINES_LEFT		3
CONST_INT		TAXI_DIALOGUE_KTW_NUM_CONV_LINES_MISS		4
CONST_INT		TAXI_DIALOGUE_KTW_NUM_CONV_LINES_R_WAY		4
CONST_INT		TAXI_DIALOGUE_KTW_NUM_CONV_LINES_W_WAY		4
CONST_INT		TAXI_DIALOGUE_KTW_NUM_CONV_LINES_GETOFF_HWY	2

CONST_INT		TAXI_DIALOGUE_NUM_MULTI_LINE		5
CONST_INT		TAXI_NUM_MAIN_PLAYERS				3

// follow car hacks
BOOL			bSayFCRandomBanter
BOOL			bSayFCBanter3
BOOL			bSayFCBanter2

ENUM TAXI_KTW_DIRECTIONS
	TAXI_DIR_STRAIGHT = 0,
	TAXI_DIR_KEEP_DRIVING,
	TAXI_DIR_LEFT,
	TAXI_DIR_RIGHT,
	TAXI_DIR_KEEP_LEFT,
	TAXI_DIR_KEEP_RIGHT,
	TAXI_DIR_MISS,
	TAXI_DIR_R_WAY,
	TAXI_DIR_W_WAY,
	TAXI_DIR_GETOFF_HIGHWAY,
	TAXI_NUM_DIRECTIONS
ENDENUM

ENUM TAXI_OJ_DIALOGUE_BLOCKS
	TXOJ_DX_BOX_OBJ_MAIN,
	TXOJ_DX_BOX_OBJ_REMINDER,
	TXOJ_DX_BOX_DIALOGUE_IMPORTANT,
	TXOJ_DX_BOX_DIALOGUE_ONE_OFFS,
	TXOJ_DX_BOX_TOTAL_NUM
ENDENUM

FUNC TAXI_DIALOGUE_INDEX GET_TAXI_SPEECH_INDEX(TaxiStruct &myTaxiData)
	RETURN myTaxiData.tTaxiOJ_DXIndex
ENDFUNC

FUNC STRING GET_STRING_OJ_TAXI_DIALOGUE_INDEX(TaxiStruct &myTaxiData)

	SWITCH GET_TAXI_SPEECH_INDEX(myTaxiData)
		CASE	TAXI_DI_OBJ							RETURN "TAXI_DI_OBJ"											BREAK
		CASE 	TAXI_DI_HAIL						RETURN "TAXI_DI_HAIL"											BREAK
		CASE 	TAXI_DI_RATE_PICKUP					RETURN "TAXI_DI_RATE_PICKUP"									BREAK
		CASE 	TAXI_DI_TTB_SHOP					RETURN "TAXI_DI_TTB_SHOP"										BREAK
		CASE 	TAXI_DI_GREET						RETURN	"TAXI_DI_GREET"											BREAK
		CASE 	TAXI_DI_FOLLOW_CAR_BARKS			RETURN	"TAXI_DI_FOLLOW_CAR_BARKS"								BREAK
		CASE 	TAXI_DI_VARIABLE_BANTER				RETURN	"TAXI_DI_VARIABLE_BANTER"								BREAK
		CASE 	TAXI_DI_BANTER						RETURN	"TAXI_DI_BANTER"										BREAK
		CASE 	TAXI_DI_BANTER_2					RETURN	"TAXI_DI_BANTER_2"										BREAK
		CASE 	TAXI_DI_BANTER_3					RETURN	"TAXI_DI_BANTER_3"										BREAK
		CASE 	TAXI_DI_BANTER_4					RETURN	"TAXI_DI_BANTER_4"										BREAK
		CASE 	TAXI_DI_BANTER_5					RETURN	"TAXI_DI_BANTER_5"										BREAK
		CASE 	TAXI_DI_TTB_BANTER					RETURN	"TAXI_DI_TTB_BANTER"									BREAK
		CASE 	TAXI_DI_SICK_WHOA					RETURN	"TAXI_DI_SICK_WHOA"										BREAK
		CASE 	TAXI_DI_DIDNT_PUKE					RETURN	"TAXI_DI_DIDNT_PUKE"									BREAK
		CASE 	TAXI_DI_IKW_SEEPT					RETURN	"TAXI_DI_IKW_SEEPT"										BREAK
		CASE 	TAXI_DI_GET_CLOSER					RETURN	"TAXI_DI_GET_CLOSER"									BREAK
		CASE 	TAXI_DI_TIME_FAST					RETURN	"TAXI_DI_TIME_FAST"										BREAK
		CASE 	TAXI_DI_TIME_BAD					RETURN	"TAXI_DI_TIME_BAD"										BREAK
		CASE 	TAXI_DI_1ST_STOP					RETURN	"TAXI_DI_1ST_STOP"										BREAK
		CASE 	TAXI_DI_TO_AIRPORT_AMAZING			RETURN	"TAXI_DI_TO_AIRPORT_AMAZING"							BREAK
		CASE 	TAXI_DI_TO_AIRPORT_AVG				RETURN	"TAXI_DI_TO_AIRPORT_AVG	"								BREAK
		CASE 	TAXI_DI_TO_AIRPORT_ASS				RETURN	"TAXI_DI_TO_AIRPORT_ASS"								BREAK
		CASE	TAXI_DI_AIRPORT_BANTER				RETURN	"TAXI_DI_AIRPORT_BANTER"								BREAK
		CASE 	TAXI_DI_SEEN_DESTINATION			RETURN	"TAXI_DI_SEEN_DESTINATION"								BREAK
		CASE 	TAXI_DI_SEEN_DESTINATION_2			RETURN	"TAXI_DI_SEEN_DESTINATION_2"							BREAK
		CASE 	TAXI_DI_DROP_OFF					RETURN	"TAXI_DI_DROP_OFF"										BREAK
		CASE	TAXI_DI_GROUP_DROPOFF				RETURN	"TAXI_DI_GROUP_DROPOFF"									BREAK
		CASE	TAXI_DI_GROUP_DROPOFF2				RETURN	"TAXI_DI_GROUP_DROPOFF2"								BREAK
		CASE	TAXI_DI_GROUP_DROPOFF3				RETURN	"TAXI_DI_GROUP_DROPOFF3"								BREAK
		CASE	TAXI_DI_HORN_HONK					RETURN	"TAXI_DI_HORN_HONK"										BREAK
		CASE	TAXI_DI_COMPLETE_STOP				RETURN	"TAXI_DI_COMPLETE_STOP"									BREAK
		CASE	TAXI_LOSE_JEWELRY_POL				RETURN	"TAXI_LOSE_JEWELRY_POL"									BREAK
		CASE	TAXI_DI_WENT_WANTED					RETURN	"TAXI_DI_WENT_WANTED"									BREAK
		CASE	TAXI_DI_ESCAPE_POLICE				RETURN	"TAXI_DI_ESCAPE_POLICE"									BREAK
		CASE	TAXI_DI_POLICE_LOST					RETURN	"TAXI_DI_POLICE_LOST"									BREAK
		CASE	TAXI_DI_POLICE_ARREST				RETURN	"TAXI_DI_POLICE_ARREST"									BREAK
		CASE	TAXI_DI_CYI_GET_BACK_TO_CAR			RETURN	"TAXI_DI_CYI_GET_BACK_TO_CAR"							BREAK
		CASE	TAXI_DI_CYI_DONT_FOLLOW_ME			RETURN	"TAXI_DI_CYI_DONT_FOLLOW_ME	"							BREAK
		CASE 	TAXI_DI_ENEMY_KILL					RETURN	"TAXI_DI_ENEMY_KILL	"									BREAK
		CASE 	TAXI_DI_ENEMY_EVADED				RETURN	"TAXI_DI_ENEMY_EVADED"									BREAK
		CASE	TAXI_DI_PICKUP_MONEY				RETURN	"TAXI_DI_PICKUP_MONEY"									BREAK
		CASE	TAXI_DI_PUKE						RETURN	"TAXI_DI_PUKE"											BREAK
		CASE	TAXI_DI_PUKE_REACT_GOOD				RETURN	"TAXI_DI_PUKE_REACT_GOOD"								BREAK
		CASE	TAXI_DI_PUKE_REACT_BAD				RETURN	"TAXI_DI_PUKE_REACT_BAD"								BREAK
		CASE	TAXI_DI_PUKE_PLAYER_REACT			RETURN	"TAXI_DI_PUKE_PLAYER_REACT"								BREAK
		CASE	TAXI_DI_AGGRO						RETURN	"TAXI_DI_AGGRO"											BREAK
		CASE	TAXI_DI_OFFROAD						RETURN	"TAXI_DI_OFFROAD"										BREAK
		CASE	TAXI_DI_OFFROAD_CHALLENGE			RETURN	"TAXI_DI_OFFROAD_CHALLENGE"								BREAK
		CASE	TAXI_DI_REVERSE						RETURN	"TAXI_DI_REVERSE"										BREAK
		CASE	TAXI_DI_SWERVE						RETURN	"TAXI_DI_SWERVE"										BREAK
		CASE	TAXI_DI_POWERSLIDING				RETURN	"TAXI_DI_POWERSLIDING"									BREAK
		CASE	TAXI_DI_COLLISION					RETURN	"TAXI_DI_COLLISION"										BREAK
		CASE	TAXI_DI_COLLISION_BIG				RETURN	"TAXI_DI_COLLISION_BIG"									BREAK
		CASE	TAXI_DI_ROLLED						RETURN	"TAXI_DI_ROLLED"										BREAK
		CASE	TAXI_DI_NEARMISS					RETURN	"TAXI_DI_NEARMISS"										BREAK
		CASE	TAXI_DI_AIRBORN						RETURN	"TAXI_DI_AIRBORN"										BREAK
		CASE	TAXI_DI_QUICKSTOP					RETURN	"TAXI_DI_QUICKSTOP"										BREAK
		CASE	TAXI_DI_SPEEDING					RETURN	"TAXI_DI_SPEEDING"										BREAK
		CASE	TAXI_DI_SIDEWALKIN					RETURN	"TAXI_DI_SIDEWALKIN"									BREAK
		CASE	TAXI_DI_OPPLANE						RETURN	"TAXI_DI_OPPLANE"										BREAK
		CASE	TAXI_DI_RANRED						RETURN	"TAXI_DI_RANRED	"										BREAK
		CASE	TAXI_DI_HITNRUN						RETURN	"TAXI_DI_HITNRUN"										BREAK
		CASE	TAXI_DI_BORING						RETURN	"TAXI_DI_BORING"										BREAK
		CASE	TAXI_DI_RADIO_CHANGE				RETURN	"TAXI_DI_RADIO_CHANGE"									BREAK
		CASE	TAXI_DI_RADIO_LIKE					RETURN	"TAXI_DI_RADIO_LIKE"									BREAK
		CASE	TAXI_DI_FLEE						RETURN	"TAXI_DI_FLEE"											BREAK
		CASE	TAXI_DI_AMBUSHED					RETURN	"TAXI_DI_AMBUSHED"										BREAK
		CASE	TAXI_DI_AMBUSHED_TALK				RETURN	"TAXI_DI_AMBUSHED_TALK"									BREAK
		CASE	TAXI_DI_EXCITED_TO_BORED			RETURN	"TAXI_DI_EXCITED_TO_BORED"								BREAK
		CASE	TAXI_DI_BORED_TO_EXCITED			RETURN	"TAXI_DI_BORED_TO_EXCITED"								BREAK
		CASE	TAXI_DI_BUILD_HOTBOX				RETURN	"TAXI_DI_BUILD_HOTBOX"									BREAK
		CASE	TAXI_DI_BUILD_HOTBOX_ONE_LINE		RETURN	"TAXI_DI_BUILD_HOTBOX_ONE_LINE	"						BREAK
		CASE	TAXI_DI_INIT_HOTBOX					RETURN	"TAXI_DI_INIT_HOTBOX"									BREAK
		CASE	TAXI_DI_CC_BANTER					RETURN	"TAXI_DI_CC_BANTER"										BREAK
		CASE	TAXI_DI_CC_BANTER2					RETURN	"TAXI_DI_CC_BANTER2"									BREAK
		CASE	TAXI_DI_CC_CLOSE					RETURN	"TAXI_DI_CC_CLOSE"										BREAK
		CASE	TAXI_DI_RATE_TIP_AMAZING			RETURN	"TAXI_DI_RATE_TIP_AMAZING"								BREAK
		CASE	TAXI_DI_RATE_TIP_AVERAGE			RETURN	"TAXI_DI_RATE_TIP_AVERAGE"								BREAK
		CASE	TAXI_DI_RATE_TIP_ASS				RETURN	"TAXI_DI_RATE_TIP_ASS"									BREAK
		CASE	TAXI_DI_JOKE						RETURN	"TAXI_DI_JOKE"											BREAK
		CASE	TAXI_DI_RUNOFF						RETURN	"TAXI_DI_RUNOFF"										BREAK
		CASE	TAXI_DI_KILLED_THIEF				RETURN	"TAXI_DI_KILLED_THIEF"									BREAK
		CASE	TAXI_DI_MAKE_PAY					RETURN	"TAXI_DI_MAKE_PAY"										BREAK
		CASE	TAXI_DI_FORGOT						RETURN	"TAXI_DI_FORGOT"										BREAK
		CASE	TAXI_DI_CUTSCENE					RETURN	"TAXI_DI_CUTSCENE"										BREAK
		CASE	TAXI_DI_POST_CUTSCENE				RETURN	"TAXI_DI_POST_CUTSCENE"									BREAK
		CASE	TAXI_DI_ENEMY_EGG_ON				RETURN	"TAXI_DI_ENEMY_EGG_ON"									BREAK
		CASE 	TAXI_DI_ENEMY_EGG_ON_2				RETURN	"TAXI_DI_ENEMY_EGG_ON_2"								BREAK
		CASE 	TAXI_DI_ENEMY_FIRES					RETURN	"TAXI_DI_ENEMY_FIRES"									BREAK
		CASE 	TAXI_DI_PLAYER_RUNS					RETURN	"TAXI_DI_PLAYER_RUNS"									BREAK
		CASE 	TAXI_DI_PLAYER_WINS_FAIR_FIGHT		RETURN	"TAXI_DI_PLAYER_WINS_FAIR_FIGHT"						BREAK
		CASE 	TAXI_DI_PUT_GUN_AWAY				RETURN	"TAXI_DI_PUT_GUN_AWAY"									BREAK
		CASE 	TAXI_DI_GIRL_NOTICE					RETURN	"TAXI_DI_GIRL_NOTICE"									BREAK
		CASE 	TAXI_DI_START_FIGHT					RETURN	"TAXI_DI_START_FIGHT"									BREAK
		CASE 	TAXI_DI_GIRL_EGGON_PLAYER_NO_HIT	RETURN	"TAXI_DI_GIRL_EGGON_PLAYER_NO_HIT"						BREAK
		CASE 	TAXI_DI_GIRL_EGGON_BF				RETURN	"TAXI_DI_GIRL_EGGON_BF"									BREAK
		CASE 	TAXI_DI_GIRL_EGGON_PLAYER_WINNING	RETURN	"TAXI_DI_GIRL_EGGON_PLAYER_WINNING"						BREAK
		CASE 	TAXI_DI_GIRL_LEAVE					RETURN	"TAXI_DI_GIRL_LEAVE"									BREAK
		CASE 	TAXI_DI_GIRL_BF_KILLED				RETURN	"TAXI_DI_GIRL_BF_KILLED"								BREAK
		CASE 	TAXI_DI_GIRL_KOD					RETURN	"TAXI_DI_GIRL_KOD"										BREAK
		CASE 	TAXI_DI_GIRL_HELP					RETURN	"TAXI_DI_GIRL_HELP"										BREAK
		CASE 	TAXI_DI_GIRL_DEST					RETURN	"TAXI_DI_GIRL_DEST"										BREAK
		CASE 	TAXI_DI_GIRL_BANTER					RETURN	"TAXI_DI_GIRL_BANTER"									BREAK
		CASE 	TAXI_DI_GIRL_THANKS					RETURN	"TAXI_DI_GIRL_THANKS"									BREAK
		CASE	TAXI_DI_FOLLOW						RETURN	"TAXI_DI_FOLLOW"										BREAK
		CASE	TAXI_DI_RETURN						RETURN	"TAXI_DI_RETURN"										BREAK
		CASE	TAXI_DI_EXCEPTION_DISABLED			RETURN	"TAXI_DI_EXCEPTION_DISABLED"							BREAK
		CASE	TAXI_DI_EMPTY						RETURN	"TAXI_DI_EMPTY"											BREAK
		CASE	TAXI_DI_GET_CAR						RETURN	"TAXI_DI_GET_CAR"										BREAK
		CASE 	TAXI_DI_JACKED_CAR					RETURN	"TAXI_DI_JACKED_CAR"									BREAK
		
		//Taxi Objectives
		CASE	TAXI_OBJ_PICKUP						RETURN	"TAXI_OBJ_PICKUP"										BREAK
		CASE	TAXI_OBJ_LEFT_CAR					RETURN	"TAXI_OBJ_LEFT_CAR"										BREAK
		CASE	TAXI_OBJ_RETURN						RETURN	"TAXI_OBJ_RETURN"										BREAK		
		CASE	TAXI_OBJ_PU_PASSENGER				RETURN	"TAXI_OBJ_PU_PASSENGER"									BREAK
		CASE	TAXI_OBJ_GIVE_MAIN					RETURN	"TAXI_OBJ_GIVE_MAIN"									BREAK
		CASE	TAXI_OBJ_GYN						RETURN	"TAXI_OBJ_GYN"											BREAK
		CASE	TAXI_OBJ_GYB						RETURN	"TAXI_OBJ_GYB"											BREAK
		CASE	TAXI_OBJ_IKW_DROPOFF				RETURN	"TAXI_OBJ_IKW_DROPOFF"									BREAK
		CASE	TAXI_OBJ_WAIT_PASS					RETURN	"TAXI_OBJ_WAIT_PASS"									BREAK
		CASE	TAXI_OBJ_GO_AIRPORT					RETURN	"TAXI_OBJ_GO_AIRPORT"									BREAK
		CASE	TAXI_OBJ_CC_2						RETURN	"TAXI_OBJ_CC_2"											BREAK
		CASE	TAXI_OBJ_CC_3						RETURN	"TAXI_OBJ_CC_3"											BREAK
		CASE	TAXI_OBJ_DROP						RETURN	"TAXI_OBJ_DROP"											BREAK
		CASE	TAXI_OBJ_POLICE						RETURN	"TAXI_OBJ_POLICE"										BREAK
		CASE	TAXI_OBJ_CYI_RETURN_WAIT			RETURN	"TAXI_OBJ_CYI_RETURN_WAIT"								BREAK
		CASE	TAXI_OBJ_RETURN_TO_PED				RETURN	"TAXI_OBJ_RETURN_TO_PED"								BREAK
		CASE	TAXI_OBJ_RUNOFF						RETURN	"TAXI_OBJ_RUNOFF"										BREAK
		CASE	TAXI_OBJ_GET_RUNAWAY_MONEY			RETURN	"TAXI_OBJ_GET_RUNAWAY_MONEY"							BREAK
		CASE	TAXI_OBJ_FORGOT						RETURN	"TAXI_OBJ_FORGOT"										BREAK
		CASE	TAXI_OBJ_FTC2						RETURN	"TAXI_OBJ_FTC2"											BREAK
		CASE	TAXI_OBJ_GET_CAR					RETURN	"TAXI_OBJ_GET_CAR"										BREAK
		
		
		
		
	ENDSWITCH
		
		
	RETURN "TAXI_OBJ Playing"	
ENDFUNC																									
																										
FUNC  BOOL IS_TAXI_DIALOGUE_DONE(TaxiStruct &myTaxiData, TAXI_DIALOGUE_FLAG_BITS dFlag = TAXI_DXF_TOTAL_CLEAR)
	IF dFlag <> TAXI_DXF_TOTAL_CLEAR
		RETURN (IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,dFlag) AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
	ENDIF
	
	RETURN IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
ENDFUNC

//// PURPOSE: Our mutator for the bSpeakOn flag that is contained within our taxi data.
 ///    
 /// PARAMS:
 ///    myTaxiData - our global taxi data
 ///    bTalk - whether to open / close the talk gate
PROC ENABLE_TAXI_SPEECH(TaxiStruct &myTaxiData, BOOL bTalk = TRUE, BOOL bForceTalk = FALSE)
	myTaxiData.bTaxiOJ_CanSpeak = bTalk
	
	IF bForceTalk
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
		TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
		CLEAR_PRINTS()
	ENDIF
	
ENDPROC

FUNC BOOL IS_TAXI_SPEECH_ENABLED(TaxiStruct &myTaxiData)
	RETURN myTaxiData.bTaxiOJ_CanSpeak
ENDFUNC

PROC TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER_PROCEDURAL(TEXT_LABEL_23 &sBaseLine)
	SWITCH GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
			
		CASE CHAR_MICHAEL	
			sBaseLine += "M"
		BREAK

		CASE CHAR_TREVOR
			sBaseLine += "T"
		BREAK
		
		CASE CHAR_FRANKLIN
			sBaseLine += "F"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Setting dialogue line to Michael since for some reason player ped enum came back out of bounds.")
			sBaseLine += "M"
			
		BREAK
	ENDSWITCH
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER_PROCEDURAL - Line to play is ", "", sBaseLine)
ENDPROC


//Remenants of a time when you could do Taxi with all 3 characters
PROC TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(TEXT_LABEL_23 &sBaseLine)

	sBaseLine += "F"
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TX_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER - Line to play is ", "", sBaseLine)
ENDPROC


FUNC BOOL DID_TAXI_OJ_INTRO_OBJECTIVE_LINE_PLAY(TaxiStruct &myTaxiData)
	TEXT_LABEL_23 textLine, sCurrentPlayingConvRoot
	textLine = myTaxiData.sTaxiOJ_DXMissionID
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	
		sCurrentPlayingConvRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sCurrentPlayingConvRoot)
			//Only Deadline Uses this method for now.
			textLine += "_obj1"
			
			IF ARE_STRINGS_EQUAL(sCurrentPlayingConvRoot,textLine)
				CDEBUG1LN(DEBUG_OJ_TAXI," [ - TX DIALOGUE -] DID_TX_OJ_INTRO_OBJECTIVE_LINE_PLAY Matched To Obj--- ", textLine)
				RETURN TRUE
			ENDIF
			
			//All other missions use this for their intro line
			textLine = myTaxiData.sTaxiOJ_DXMissionID
			textLine += "_gret1"
			TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)	
			
			IF ARE_STRINGS_EQUAL(sCurrentPlayingConvRoot,textLine)
				CDEBUG1LN(DEBUG_OJ_TAXI," [ - TX DIALOGUE -] DID_TX_OJ_INTRO_OBJECTIVE_LINE_PLAY Matched To Greeting--- ", textLine)
				RETURN TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Sets the next line of dialogue to play in the state machine.
///    
/// PARAMS:
///    myTaxiData - 
///    thisNextLine - 
///    bOpenSpeechGate - To play immediately set this to TRUE
///    bForceSpeech - This clears all prints and kills any current conversation to force this to play right away. NOTE**** DO NOT USE when the dialogue QUEUE is RUNNING Will F SHIT UP!
PROC SET_NEXT_TAXI_SPEECH(TaxiStruct &myTaxiData,TAXI_DIALOGUE_INDEX thisNextLine, BOOL bOpenSpeechGate = FALSE, BOOL bForceSpeech = FALSE,BOOL bSpeakNowButDontForce = FALSE)//,BOOL bMissionSpecific = FALSE)
	
	myTaxiData.tTaxiOJ_DXIndex = thisNextLine
	
	IF bSpeakNowButDontForce
		TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
		
		IF DID_TAXI_OJ_INTRO_OBJECTIVE_LINE_PLAY(myTaxiData)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			CDEBUG1LN(DEBUG_OJ_TAXI,"[ INTERRUPTION ] - ", GET_STRING_OJ_TAXI_DIALOGUE_INDEX(myTaxiData), " Interrupted Initial Objective Line ")
		ENDIF
	ENDIF
	
	ENABLE_TAXI_SPEECH(myTaxiData,bOpenSpeechGate,bForceSpeech)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] -SET_NEXT_TX_SPEECH Dialogue is - ",GET_STRING_OJ_TAXI_DIALOGUE_INDEX(myTaxiData))
ENDPROC


//
//PROC SET_NEXT_TAXI_SPEECH_NEW(TaxiStruct &myTaxiData,TAXI_DIALOGUE_INDEX thisNextLine, FLOAT fDelay = 0.0, BOOL bOpenSpeechGate = FALSE, BOOL bForceSpeech = FALSE)
//	
//	myTaxiData.tTaxiOJ_DXIndex = thisNextLine
//	ENABLE_TAXI_SPEECH(myTaxiData,bOpenSpeechGate,bForceSpeech)
//	
//	CDEBUG1LN(DEBUG_OJ_TAXI,"SET_NEXT_TAXI_SPEECH - Dialogue is - ","", thisNextLine)
//ENDPROC

/// PURPOSE: A wrapper for playing dialogue/objective and checking if it's already played
///    
/// PARAMS:
///    myTaxiData - Global taxi data
///    iGateBits - 		the bitfield to check against
///    eWhichTaxiGate - which bit to check
///    diObj - 			he objective to play if it hasn't already played once
///    
PROC SPEAK_IN_TAXI_OJ_IF_NOT_GATED(TaxiStruct &myTaxiData, INT &iGateBits,TAXI_GATE_BITS eWhichTaxiGateMask, TAXI_DIALOGUE_INDEX diObj )
	
	IF NOT IS_BITMASK_AS_ENUM_SET(iGateBits,eWhichTaxiGateMask)
		
		//This should give the player his last objective, but ONLY ONCE
		CLEAR_PRINTS()
	
		SET_NEXT_TAXI_SPEECH(myTaxiData,diObj,TRUE)
		
		SET_BITMASK_AS_ENUM(iGateBits,eWhichTaxiGateMask)
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"SPEAK_IN_TAXI_OJ_IF_NOT_GATED - With Dialogue Index of ", "", diObj)
	ENDIF
	
ENDPROC

FUNC BOOL CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(TaxiStruct &myTaxiData, STRING sRootLabel, BOOL bIsThereAnotherLineToPlay = FALSE, BOOL bIsThisAnInterruptLine = FALSE, enumConversationPriority eConvPriority = CONV_PRIORITY_HIGH)
	//Append the RootSuffix to the string.
	//EX: 	sRootLabel = txm4
	//		sSpecificRootSuffix = _spook
	// 		sRootLabel += sSpecificRootSuffix = "txm4_spook"
	
	//sRootLabel += sSpecificRootSuffix
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ~~~~~~ TXCONVO ~~~~~~ RootLabel = ", sRootLabel, " ~~~STATE =  ", GET_STRING_OJ_TAXI_DIALOGUE_INDEX(myTaxiData))
	
	//Make sure to reset your dialogue timer to space out chatter
	TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
	
	IF bIsThisAnInterruptLine
		TAXI_RESET_TIMERS(myTaxiData,TT_INTERUPT,0,TRUE)
	ENDIF

	//Close the dialogue gate behind you
	ENABLE_TAXI_SPEECH(myTaxiData, bIsThereAnotherLineToPlay)
	
	//Play the line
	RETURN CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, sRootLabel, eConvPriority)

ENDFUNC


//// PURPOSE:
 ///    
 /// 0: No Direction
 /// 1: Turn right
//	 2: Turn left
//	 3: Straight on
 ///     
 ///     
 ///     

PROC TAXI_PLAY_SINGLE_LINE_DIRECTION(TaxiStruct &myTaxiData, INT &speakCounters[], TAXI_KTW_DIRECTIONS iDirection)
	
	TEXT_LABEL_15 sTempRootLabel = myTaxiData.sTaxiOJ_DXMissionID
	TEXT_LABEL_15 sTempFileLabel
	TEXT_LABEL_23 sTempFileLabel23
	INT iVariationToPlay, iNumOfLinesPerDirection
	
	SWITCH iDirection
	
		//STRAIGHT
		CASE TAXI_DIR_KEEP_DRIVING
		DEFAULT
			sTempRootLabel += "_dirS1"
			speakCounters[TAXI_DIR_KEEP_DRIVING]++
			
			iNumOfLinesPerDirection = TAXI_DIALOGUE_KTW_NUM_CONV_LINES_STR

		BREAK
		
		//LEFT 
		CASE TAXI_DIR_LEFT	
			sTempRootLabel += "_dirL1"
			speakCounters[TAXI_DIR_LEFT]++
			
			iNumOfLinesPerDirection = TAXI_DIALOGUE_KTW_NUM_CONV_LINES_LEFT
		BREAK
		
		//RIGHT
		CASE TAXI_DIR_RIGHT	
			sTempRootLabel += "_dirR1"
			speakCounters[TAXI_DIR_RIGHT]++
			
			iNumOfLinesPerDirection = TAXI_DIALOGUE_KTW_NUM_CONV_LINES_RIGHT
		BREAK
		
		//MISS
		CASE TAXI_DIR_MISS	
			sTempRootLabel += "_miss1"
			speakCounters[TAXI_DIR_MISS]++
			
			iNumOfLinesPerDirection = TAXI_DIALOGUE_KTW_NUM_CONV_LINES_MISS
		BREAK

		//Going Right Way
		CASE TAXI_DIR_R_WAY	
			sTempRootLabel += "_rWay1"
			speakCounters[TAXI_DIR_R_WAY]++
			
			iNumOfLinesPerDirection = TAXI_DIALOGUE_KTW_NUM_CONV_LINES_R_WAY
		BREAK

		//Wrong Way
		CASE TAXI_DIR_W_WAY	
			sTempRootLabel += "_wWay1"
			speakCounters[TAXI_DIR_W_WAY]++
			
			iNumOfLinesPerDirection = TAXI_DIALOGUE_KTW_NUM_CONV_LINES_W_WAY			
		BREAK
		
		//KEEP LEFT
		CASE TAXI_DIR_KEEP_LEFT
			sTempRootLabel += "_dirKL1"
			speakCounters[TAXI_DIR_KEEP_LEFT]++
			
			iNumOfLinesPerDirection = TAXI_DIALOGUE_KTW_NUM_CONV_LINES_LEFT
		BREAK

		//KEEP RIGHT
		CASE TAXI_DIR_KEEP_RIGHT
			sTempRootLabel += "_dirKR1"
			speakCounters[TAXI_DIR_KEEP_RIGHT]++
			
			iNumOfLinesPerDirection = TAXI_DIALOGUE_KTW_NUM_CONV_LINES_RIGHT
		BREAK
		
		//STRAIGHT THROUGH JUNC
		CASE TAXI_DIR_STRAIGHT	
			sTempRootLabel += "_dirSt1"
			speakCounters[TAXI_DIR_STRAIGHT]++
			
			iNumOfLinesPerDirection = TAXI_DIALOGUE_KTW_NUM_CONV_LINES_R_WAY
		BREAK	
		
		CASE TAXI_DIR_GETOFF_HIGHWAY
			sTempRootLabel += "_offHwy"
			speakCounters[TAXI_DIR_GETOFF_HIGHWAY]++
			
			iNumOfLinesPerDirection = TAXI_DIALOGUE_KTW_NUM_CONV_LINES_GETOFF_HWY
		BREAK
	ENDSWITCH
	
	//Get which line to play by modding the number of times it's been triggered by total num variants
	iVariationToPlay = speakCounters[iDirection] % iNumOfLinesPerDirection
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI IKNOWWAY Variation in direction: ","",iDirection, "", " = ", iVariationToPlay)
	
	//Terminal case of ZERO
	IF iVariationToPlay = 0
		iVariationToPlay = iNumOfLinesPerDirection
	ENDIF
	
	//sTempFileLabel = myTaxiData.sTaxiOJ_DXMissionID
	stempFileLabel = sTempRootLabel
	stempFileLabel += "_"
	stempFileLabel += iVariationToPlay
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI IKNOWWAY Direction what sTempFileLabel value is after ","",sTempFileLabel)
	
	//Play one of the lines in the conversation
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID,sTempRootLabel,sTempFileLabel,CONV_PRIORITY_HIGH)
	ELSE
		PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID,sTempRootLabel,sTempFileLabel,CONV_PRIORITY_MEDIUM)
	ENDIF
	
	IF iDirection = TAXI_DIR_MISS
	OR iDirection = TAXI_DIR_W_WAY
		sTempFileLabel23 = sTempFileLabel
		ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_IKNOWWAY_NEG_MISSED_TURN,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,sTempFileLabel23, FALSE)
	ENDIF
	//CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID,sTempRootLabel,CONV_PRIORITY_HIGH)

ENDPROC
/// PURPOSE:
///    Called to print our strings from the table
/// PARAMS:
///    myTaxiData - our Taxi data
///    lineString - The string from the String table
PROC TAXI_PRINT_TAXI_DIALOG(TaxiStruct &myTaxiData, STRING lineString = NULL)
	TEXT_LABEL_63 newText
	newText = "TAXI_"
	newtext += myTaxiData.sTaxiOJ_IDPrefix
	newtext += lineString
	PRINT_NOW(newtext, DEFAULT_GOD_TEXT_TIME, 1)
ENDPROC
/// PURPOSE: Wrapper for playing conversations
///    
/// PARAMS:
///    myTaxiData - 
///    eBit - 
///    sBaseLine - 
///    sVariation - 
PROC TAXI_PLAY_CONVERSATION(TaxiStruct &myTaxiData,TAXI_DIALOGUE_FLAG_BITS eBit,TEXT_LABEL_23 sBaseLine, String sVariation,enumConversationPriority convPriority = CONV_PRIORITY_HIGH)
	
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,eBit)
	TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
	
	sBaseLine += sVariation
						
	CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, sBaseLine, convPriority)
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TX_PLAY_CONVERSATION ------------ LINE IS  ","", sBaseLine)

ENDPROC

/// PURPOSE: used for filling in the multi-line conversation array sDialogueLines with the appropriate lines based off how many lines the NPC has and which player your currently controlling.
///    Can easily be modified to support additional lines by passing in a higher number for iNumConversations, and modifying the lines for the player to add on more than one line and probably increase
///    the size of TAXI_DIALOGUE_NUM_MULTI_LINE currently set to 5 - leaves up to 4 npc lines and one player response line
///    
/// PARAMS:
///    sTextIDLines - the array that will get filled in with the conversation line IDs
///    textLine - the root for the conversation
///    iNumConversationLines - the number of lines the NPC has, before the player responds
PROC TAXI_OJ_DX_GET_LINE_FOR_MULTI_CONVERSATION(TEXT_LABEL_23 &sTextIDLines[],TEXT_LABEL_23 textLine, INT iNumNPCLines, BOOL bPlayerSpeaksFirst = FALSE)
	INT iIter
	
	//GEt total number of lines = # of NPC lines + # of player lines (for now HARDCODING TO 1)
	INT iNumPlayerLines = 1
	INT iNumTotalLines = iNumPlayerLines + iNumNPCLines
	
	//Set up entire conversation array
	REPEAT iNumTotalLines iIter
		sTextIDLines[iIter] = textLine
		sTextIDLines[iIter] += "_"
	ENDREPEAT
	
	IF NOT bPlayerSpeaksFirst
		
		//Setup ONLY NPC lines
		REPEAT iNumNPCLines iIter
			sTextIDLines[iIter] += iIter+1
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DX_GET_LINE_FOR_PLAYER - Line ID Loop = ", "", sTextIDLines[iIter] )
		ENDREPEAT
		
		//Setup last line
		sTextIDLines[iNumNPCLines] = textLine
		sTextIDLines[iNumNPCLines] += "_"
		
		SWITCH GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
			
			CASE CHAR_MICHAEL	
				sTextIDLines[iNumNPCLines] += iNumNPCLines + 1
			BREAK

			CASE CHAR_TREVOR
				sTextIDLines[iNumNPCLines] += (iNumNPCLines + 2)
			BREAK
			
			CASE CHAR_FRANKLIN
				sTextIDLines[iNumNPCLines] += (iNumNPCLines + 3)
			BREAK
			DEFAULT
				sTextIDLines[iNumNPCLines] += iNumNPCLines
			BREAK
		ENDSWITCH
		
	//Order is reversed player is speaking first
	ELSE
		
		SWITCH GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
			
			CASE CHAR_MICHAEL	
				sTextIDLines[0] += 1
			BREAK

			CASE CHAR_TREVOR
				sTextIDLines[0] += 2
			BREAK
			
			CASE CHAR_FRANKLIN
				sTextIDLines[0] += 3
			BREAK
			DEFAULT
				sTextIDLines[0] += 1
			BREAK
		ENDSWITCH
		
		IF iNumNPCLines > 0
			INT iNumNPCStartLine
				
			REPEAT iNumNPCLines iIter
				iNumNPCStartLine = iIter + iNumPlayerLines
				sTextIDLines[iNumNPCStartLine] += iIter + iNumPlayerLines + TAXI_NUM_MAIN_PLAYERS
				CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DX_GET_LINE_FOR_PLAYER - Line ID Loop = ", "", sTextIDLines[iNumNPCStartLine] )
			ENDREPEAT
		ENDIF
		
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DX_GET_LINE_FOR_PLAYER - Line ID = ", "", sTextIDLines[iNumNPCLines] )

ENDPROC



/// PURPOSE: Wrapper for CREATE_MULTIPART_CONVERSATION_WITH_#_LINES
///    
/// PARAMS:
///    myTaxiData - global taxi data
///    sBaseLine - 	the root for the line
///    sTextIDLines - the string array for the lines
///    iNumLines - number of npc lines
///    convPriority - 
PROC TAXI_OJ_CREATE_MULTIPART_CONVERSATION_WITH_NUM_LINES(TaxiStruct &myTaxiData,TEXT_LABEL_23 sBaseLine, TEXT_LABEL_23 &sTextIDLines[], INT iNumLines, BOOL bDoesPlayerSpeakFirst = FALSE, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)

	TAXI_OJ_DX_GET_LINE_FOR_MULTI_CONVERSATION(sTextIDLines,sBaseLine, iNumLines, bDoesPlayerSpeakFirst)
	
	IF iNumLines = 0
		PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID,sBaseLine,sTextIDLines[0],convPriority)
	ELIF iNumLines = 1
		CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID,sBaseLine,sTextIDLines[0],sBaseLine,sTextIDLines[1],convPriority)
	ELIF iNumLines = 2
		CREATE_MULTIPART_CONVERSATION_WITH_3_LINES(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID,sBaseLine,sTextIDLines[0],sBaseLine,sTextIDLines[1],sBaseLine, sTextIDLines[2],convPriority)
	ELIF iNumLines = 3
		CREATE_MULTIPART_CONVERSATION_WITH_4_LINES(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID,sBaseLine,sTextIDLines[0],sBaseLine,sTextIDLines[1],sBaseLine, sTextIDLines[2],sBaseLine, sTextIDLines[3], convPriority)
	ELIF iNumLines = 4
		CREATE_MULTIPART_CONVERSATION_WITH_5_LINES(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID,sBaseLine,sTextIDLines[0],sBaseLine,sTextIDLines[1],sBaseLine, sTextIDLines[2],sBaseLine, sTextIDLines[3], sBaseLine, sTextIDLines[4], convPriority)
	ELSE
		SCRIPT_ASSERT("Triggering conversation that had an invalid # of lines in TAXI ODDJOBS")
	ENDIF
	
ENDPROC
FUNC STRING GET_TAXI_PROP_MISSION_PASSENGER_NAME(TaxiStruct &myTaxiData)
	
	STRING sDialogueStarCharacterName
	SWITCH myTaxiData.tTaxiOJ_MissionType
		
		
		CASE TXM_01_NEEDEXCITEMENT
			sDialogueStarCharacterName = "TaxiFelipe"
		BREAK
		
		CASE TXM_02_TAKEITEASY
			sDialogueStarCharacterName = "TaxiOtis"
		BREAK
		
		CASE TXM_03_DEADLINE
			sDialogueStarCharacterName = "TaxiKwak"
		BREAK
		
		CASE TXM_04_GOTYOURBACK
			sDialogueStarCharacterName = "TaxiWalter"
		BREAK
		
		CASE TXM_05_TAKETOBEST
			sDialogueStarCharacterName = "TaxiMiranda"
		BREAK	
		
		CASE TXM_07_CUTYOUIN
			sDialogueStarCharacterName = "TaxiDerrick"
		BREAK
		
		CASE TXM_08_GOTYOUNOW
			sDialogueStarCharacterName = "TaxiAlonzo"
		BREAK
		
		CASE TXM_09_CLOWNCAR
			sDialogueStarCharacterName = "TaxiDarren"
		BREAK
		
		CASE TXM_10_FOLLOWTHATCAR
			sDialogueStarCharacterName = "TaxiKeyla"
		BREAK
		
	ENDSWITCH
	
	RETURN sDialogueStarCharacterName
ENDFUNC

FUNC STRING GET_TAXI_INTERRUPT_STRING(TaxiStruct & myTaxiData, TAXI_DIALOGUE_INDEX eWhichInterrupt)
	SWITCH eWhichInterrupt
		CASE TAXI_DI_COLLISION
			SWITCH myTaxiData.tTaxiOJ_MissionType
				CASE TXM_01_NEEDEXCITEMENT		RETURN "_ACRH"
				CASE TXM_02_TAKEITEASY			RETURN "_ACRH"
				CASE TXM_03_DEADLINE			RETURN "_ACAA"
				CASE TXM_04_GOTYOURBACK			RETURN "_ACRH"
				CASE TXM_05_TAKETOBEST			RETURN "_AEAA"
				CASE TXM_07_CUTYOUIN			RETURN "_DKAA" //"_ACRH" // This is actually missing, bugged gemma about it
				CASE TXM_08_GOTYOUNOW			RETURN "_ACAA"
				CASE TXM_09_CLOWNCAR			
					// add a condition here to check for smoke
					//RETURN "_ADAA"
					RETURN "_ACAA"
				CASE TXM_10_FOLLOWTHATCAR		RETURN "_ACRH"
				DEFAULT							RETURN "_ACRH"
			ENDSWITCH
		BREAK
		
		CASE TAXI_DI_HITNRUN
			SWITCH myTaxiData.tTaxiOJ_MissionType
				CASE TXM_01_NEEDEXCITEMENT		RETURN "_AHIT"
				CASE TXM_02_TAKEITEASY			RETURN "_AHIT"
				CASE TXM_03_DEADLINE			RETURN "_AEAA"
				CASE TXM_04_GOTYOURBACK			RETURN "_ADAA"
				CASE TXM_05_TAKETOBEST			RETURN "_AFAA"
				CASE TXM_07_CUTYOUIN			RETURN "_AFAA"
				CASE TXM_08_GOTYOUNOW			RETURN "_AEAA"
				CASE TXM_09_CLOWNCAR			RETURN "_AHAA"
				CASE TXM_10_FOLLOWTHATCAR		RETURN "_AEAA"
				DEFAULT							RETURN "_AHIT"
			ENDSWITCH
		BREAK
		
		CASE TAXI_DI_ROLLED
			SWITCH myTaxiData.tTaxiOJ_MissionType
				CASE TXM_01_NEEDEXCITEMENT		RETURN "_AROL"
				CASE TXM_02_TAKEITEASY			RETURN "_AROL"
				CASE TXM_03_DEADLINE			RETURN "_AHAA"
				CASE TXM_04_GOTYOURBACK			RETURN "_AROL"
				CASE TXM_05_TAKETOBEST			RETURN "_AGAA"
				CASE TXM_07_CUTYOUIN			RETURN "_AROL"
				CASE TXM_08_GOTYOUNOW			RETURN "_AROL"
				CASE TXM_09_CLOWNCAR			RETURN "_AROL"
				CASE TXM_10_FOLLOWTHATCAR		RETURN "_AROL"
				DEFAULT							RETURN "_AROL"
			ENDSWITCH
		BREAK
		
		CASE TAXI_DI_AGGRO
			SWITCH myTaxiData.tTaxiOJ_MissionType
				CASE TXM_01_NEEDEXCITEMENT		RETURN "_ABAA"
				CASE TXM_02_TAKEITEASY			RETURN "_FUAA"
				CASE TXM_03_DEADLINE			RETURN "_AFAA"
				CASE TXM_04_GOTYOURBACK			RETURN "_DBAA"
				CASE TXM_05_TAKETOBEST			RETURN "_DLAA"
				CASE TXM_07_CUTYOUIN			RETURN "_DKAA"
				CASE TXM_08_GOTYOUNOW			
					IF myTaxiData.bSecondPassenger
						RETURN "_EEAA"
					ELSE
						RETURN "_EFAA"
					ENDIF
				BREAK
				CASE TXM_09_CLOWNCAR			
					//maybe add support for the other two passengers here
					RETURN "_DBAA"
				CASE TXM_10_FOLLOWTHATCAR		RETURN "_EFAA"
				DEFAULT							RETURN "_AROL"
			ENDSWITCH
		BREAK
		
		CASE TAXI_DI_SHOOTING
			SWITCH myTaxiData.tTaxiOJ_MissionType
				CASE TXM_01_NEEDEXCITEMENT		RETURN "_AEAA"
				CASE TXM_02_TAKEITEASY			RETURN "_ACAA"
				CASE TXM_03_DEADLINE			RETURN "_AJAA"
				CASE TXM_04_GOTYOURBACK			RETURN "_AHAA"
				CASE TXM_05_TAKETOBEST			RETURN "_AHAA"
				CASE TXM_07_CUTYOUIN			RETURN "_AIAA"
				CASE TXM_08_GOTYOUNOW			
					IF myTaxiData.bSecondPassenger
						RETURN "_AJAA"
					ELSE
						RETURN "_AGAA"
					ENDIF
				BREAK
				CASE TXM_09_CLOWNCAR			RETURN "_AUAA"
				CASE TXM_10_FOLLOWTHATCAR		RETURN "_AHAA"
				DEFAULT							RETURN "_AROL"
			ENDSWITCH
		BREAK
		
		CASE TAXI_OBJ_LEFT_CAR
			SWITCH myTaxiData.tTaxiOJ_MissionType
				CASE TXM_01_NEEDEXCITEMENT		RETURN "_AAAA"
				CASE TXM_02_TAKEITEASY			RETURN "_AAAA"
				CASE TXM_03_DEADLINE			RETURN "_ADAA"
				CASE TXM_04_GOTYOURBACK			RETURN "_ABAA"
				CASE TXM_05_TAKETOBEST			RETURN "_AAAA"
				CASE TXM_07_CUTYOUIN			RETURN "_ABAA"
				CASE TXM_08_GOTYOUNOW			
					IF myTaxiData.bSecondPassenger
						RETURN "_AHAA"
					ELSE
						RETURN "_ABAA"
					ENDIF
				BREAK
				CASE TXM_09_CLOWNCAR			RETURN "_BUAA"
				CASE TXM_10_FOLLOWTHATCAR		RETURN "_ADAA"
				DEFAULT							RETURN "_AAAA"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN "_ACRH"
ENDFUNC

PROC PLAY_TAXI_PROP_MISSION_DIALOGUE_INTERRUPT(TaxiStruct &myTaxiData,TEXT_LABEL_23 sBaseLine, String sVariation)
	
	TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
	TAXI_RESET_TIMERS(myTaxiData,TT_INTERUPT,0,TRUE)
	
	sBaseLine += sVariation
	
	IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
		INTERRUPT_CONVERSATION(myTaxiData.piTaxiPassenger,sBaseLine, GET_TAXI_PROP_MISSION_PASSENGER_NAME(myTaxiData))
		CDEBUG1LN(DEBUG_OJ_TAXI,"[ INTERRUPT ] PLAY_TAXI_PROP_MISSION_DIALOGUE_INTERRUPT ------------ LINE IS  ","", sBaseLine)
	ENDIF
	
ENDPROC

FUNC BOOL HAS_THIS_TAXI_PROP_MISSION_BEEN_UPDATED_FOR_NEW_INTERRUPTS(TaxiStruct &myTaxiData)
	
	IF myTaxiData.tTaxiOJ_MissionType = TXM_03_DEADLINE
	OR myTaxiData.tTaxiOJ_MissionType = TXM_04_GOTYOURBACK
	OR myTaxiData.tTaxiOJ_MissionType = TXM_05_TAKETOBEST
	OR myTaxiData.tTaxiOJ_MissionType = TXM_07_CUTYOUIN
	OR myTaxiData.tTaxiOJ_MissionType = TXM_08_GOTYOUNOW
	OR myTaxiData.tTaxiOJ_MissionType = TXM_09_CLOWNCAR
	OR myTaxiData.tTaxiOJ_MissionType = TXM_10_FOLLOWTHATCAR

		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(TaxiStruct &myTaxiData,TAXI_DIALOGUE_FLAG_BITS eBit,TEXT_LABEL_23 sBaseLine, String sVariation, BOOL bSupportsTrevNFrank = TRUE, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)
	
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,eBit)
	TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
	
	sBaseLine += sVariation
						
	//Play one way conversation
	IF bSupportsTrevNFrank
		TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(sBaseLine)
	ENDIF
	
	
	CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, sBaseLine, convPriority)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TX_PLAY_CONVERSATION_W_PLAYER_RESPONSE ------------ LINE IS  ","", sBaseLine)

ENDPROC
PROC TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE_USING_EXTRA_DLG_BITS(TaxiStruct &myTaxiData,TAXI_DIALOGUE_FLAG_BITS_2 eBit,TEXT_LABEL_23 sBaseLine, String sVariation, BOOL bSupportsTrevNFrank = TRUE, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)
	
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,eBit)
	TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
	
	sBaseLine += sVariation
						
	//Play one way conversation
	IF bSupportsTrevNFrank
		TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(sBaseLine)
	ENDIF
	
	
	CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, sBaseLine, convPriority)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TX_PLAY_CONVERSATION_W_PLAYER_RESPONSE_USING_XTRA_DLG_BITS ------------ LINE IS  ","", sBaseLine)

ENDPROC

//// PURPOSE: Neeeded another one of these as I ran out of bits
 ///    
 /// PARAMS:
 ///    myTaxiData - 
 ///    eBit - 
 ///    sBaseLine - 
 ///    sVariation - 
 ///    convPriority - 
PROC TAXI_PLAY_CONVERSATION_EX(TaxiStruct &myTaxiData,TAXI_DIALOGUE_FLAG_BITS_2 eBit,TEXT_LABEL_23 sBaseLine, String sVariation,enumConversationPriority convPriority = CONV_PRIORITY_HIGH, BOOL bSupportsTrevNFrank = FALSE)
	
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,eBit)
	TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
	
	sBaseLine += sVariation
	
	//Play one way conversation
	IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
	OR myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
	OR myTaxiData.tTaxiOJ_MissionType = TXM_07_CUTYOUIN
	OR myTaxiData.tTaxiOJ_MissionType = TXM_10_FOLLOWTHATCAR
	OR myTaxiData.tTaxiOJ_MissionType = TXM_03_DEADLINE
		IF bSupportsTrevNFrank
			TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(sBaseLine)
		ENDIF
	ENDIF
	
	CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, sBaseLine, convPriority)
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TX_PLAY_CONVERSATION_EX ------------ LINE IS  ", sBaseLine)

ENDPROC
//TODO: Finish this so it plays a generic reaction when all audio is exhausted
/*
PROC TAXI_PLAY_GENERIC_REACTION(TaxiStruct &myTaxiData)
	
ENDPROC
*/

PROC TAXI_PLAY_CONVERSATION_ROLLED(TaxiStruct &myTaxiData,TEXT_LABEL_23 sBaseLine,enumConversationPriority convPriority = CONV_PRIORITY_HIGH)
	TEXT_LABEL_23 sLineID = sBaseLine
	
	IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DRB_ROLLED1)
		
		SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DRB_ROLLED1)
		sLineID += "_1"
	ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DRB_ROLLED2)
		
		SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DRB_ROLLED2)
		sLineID += "_2"

	ELSE
		sLineID += "_"
		sLineID += GET_RANDOM_INT_IN_RANGE(1,3)
		//TAXI_PLAY_GENERIC_REACTION
	ENDIF
	

	PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,sBaseLine, sLineID,convPriority)
	TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TX_PLAY_CONVERSATION_ROLLED - ", sBaseLine, " with variation : ",  sLineID)
	


ENDPROC
/// PURPOSE: This function loops plays a unique line seqeuentially until it reaches the end, than loops back around.
///    
/// PARAMS:
///    iBitField - specific  bitfield for the line you want to say
///    iNumLines - number of lines for that variation - should be a const found in Taxi_Shared_Consts.sch
///    sTheSpecificLine - the string to write the filename to
///    bLoop - If false, lines will not loop once exhausted, also important to pass FALSE when there are FAIL CASES waiting on a line to play like TAXI_OJ_FAIL_BIT_POLICE &  TAXI_OJ_FAIL_BIT_STOPPED
///    bSingleLine - If I'm cuing a single line from conversation than I need an '_' otherwise I don't
PROC GET_TAXI_OJ_LINE_TO_SAY(INT &iBitField, INT iNumLines, TEXT_LABEL_23 & sTheSpecificLine,BOOL bLoop = FALSE, BOOL bSingleLine = TRUE, BOOL bSupportTrevNFranklin = FALSE)

	FLOAT fExponent
	INT iBitMask
	INT iIterator

	REPEAT iNumLines iIterator 
		//iter = 0 //iNumLines = 8
		//2^0 = 1
		//2^1 = 2
		fExponent = TO_FLOAT(iIterator)
		iBitMask = ROUND(POW(2,fExponent)) 
		
	
		IF NOT IS_BITMASK_SET(iBitField,iBitMask ) 	
			SET_BITMASK(iBitField,iBitMask)
			
			IF bSupportTrevNFranklin 
				//appends # & M,F,or T ex : 1M or 2F etc.
				IF NOT bSingleLine
					sTheSpecificLine += (iIterator + 1)
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(sTheSpecificLine)	
			
				//appends ONLY the letter M,F,T	
				ELSE
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(sTheSpecificLine)
				ENDIF
			//Not appending letters, simply numbers and _ if your only playing a single line in the conversation.	
			ELSE
				IF bSingleLine
					sTheSpecificLine += "_"
				ENDIF
			
				sTheSpecificLine += (iIterator + 1)
			ENDIF		
			

			CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] GET_TAXI_OJ_LINE_TO_SAY returning i = ","", iIterator, " and line is ", "", sTheSpecificLine)
			
			//All lines have been said if it gets to this point, reset
			IF bLoop
				IF iIterator = (iNumLines - 1)
					iBitField = 0
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] GET_TAXI_OJ_LINE_TO_SAY - All lines used up, resetting - iBitField = ", "",iBitField)
					
				ENDIF
			ENDIF
			
			EXIT
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI," [TAXI_DIALOGUE_LIB] GET_TAXI_OJ_LINE_TO_SAY CHECKED BIT i = ","", iIterator) 
		ENDIF
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] GET_TAXI_OJ_LINE_TO_SAY returning - END OF ROAD - i = ","", iIterator, " and line is ", "", sTheSpecificLine)
ENDPROC


//****
/// PURPOSE: This function loops plays a unique line seqeuentially until it reaches the end, than loops back around.
///    
/// PARAMS:
///    iBitField - specific  bitfield for the line you want to say
///    iNumLines - number of lines for that variation - should be a const found in Taxi_Shared_Consts.sch
///    sTheSpecificLine - the string to write the filename to
///    bLoop - If false, lines will not loop once exhausted, also important to pass FALSE when there are FAIL CASES waiting on a line to play like TAXI_OJ_FAIL_BIT_POLICE &  TAXI_OJ_FAIL_BIT_STOPPED
///    bSingleLine - If I'm cuing a single line from conversation than I need an '_' otherwise I don't
PROC GET_TAXI_OJ_LINE_TO_SAY_FOR_MULTIPLE_VARIATIIONS_WITH_MFT(INT &iBitField, INT iNumLines, TEXT_LABEL_23 & sTheSpecificLine, BOOL &bSupportTrevNFranklin[], BOOL bLoop = FALSE, BOOL bSingleLine = TRUE)

	FLOAT fExponent
	INT iBitMask
	INT iIterator

	REPEAT iNumLines iIterator 
		//iter = 0 //iNumLines = 8
		//2^0 = 1
		//2^1 = 2
		fExponent = TO_FLOAT(iIterator)
		iBitMask = ROUND(POW(2,fExponent)) 
		
	
		IF NOT IS_BITMASK_SET(iBitField,iBitMask ) 	
			SET_BITMASK(iBitField,iBitMask)
			
			//If playing single line need this '_' in between the base string and the num otherwise it's a conversation and don't need it.
 			IF bSingleLine
				sTheSpecificLine += "_"
			ENDIF
			sTheSpecificLine += (iIterator + 1)
			
			//Appends M, T, or F before picking the line variation			
			IF bSupportTrevNFranklin[iIterator]
				TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(sTheSpecificLine)	
			ENDIF		
			
			CDEBUG1LN(DEBUG_OJ_TAXI," [TAXI_DIALOGUE_LIB]  GET_TAXI_OJ_LINE_TO_SAY returning i = ","", iIterator, " and line is ", "", sTheSpecificLine)
			
			//All lines have been said if it gets to this point, reset
			IF bLoop
				IF iIterator = (iNumLines - 1)
					iBitField = 0
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  GET_TAXI_OJ_LINE_TO_SAY - All lines used up, resetting - iBitField = ", "",iBitField)
					
				ENDIF
			ENDIF
			
			EXIT
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI," [TAXI_DIALOGUE_LIB]  GET_TAXI_OJ_LINE_TO_SAY CHECKED BIT i = ","", iIterator) 
		ENDIF
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_OJ_TAXI," [TAXI_DIALOGUE_LIB]  GET_TAXI_OJ_LINE_TO_SAY returning - END OF ROAD - i = ","", iIterator, " and line is ", "", sTheSpecificLine)
ENDPROC

FUNC STRING TAXI_GET_VEHICLE_DIRECTION_IN_STRING(VEHICLE_PATH_DIRECTIONS eNextDirection)
	STRING sDir
	
	SWITCH eNextDirection	
		CASE DIRECTIONS_UNKNOWN
			sDir = "NO DIRECTION"
		BREAK		
		CASE DIRECTIONS_LEFT_AT_JUNCTION
			sDir = "LEFT AT JUNCTION"
		BREAK			
		CASE DIRECTIONS_RIGHT_AT_JUNCTION
			sDir = "RIGHT AT JUNCTION"
		BREAK
		CASE DIRECTIONS_KEEP_LEFT
			sDir = "KEEP LEFT"
		BREAK			
		CASE DIRECTIONS_KEEP_RIGHT
			sDir = "KEEP RIGHT"
		BREAK
		CASE DIRECTIONS_KEEP_DRIVING
			sDir = "KEEP DRIVING"
		BREAK		
		CASE DIRECTIONS_STRAIGHT_THROUGH_JUNCTION
			sDir = "STRAIGHT AT JUNCTION"
		BREAK
		CASE DIRECTIONS_WRONG_WAY
			sDir = "TURN AROUND"
		BREAK	
	ENDSWITCH
	
	RETURN sDir
ENDFUNC

PROC TAXI_DEBUG_PRINT_DIRECTIONS_TO_NEXT_INTERSECTION(VEHICLE_PATH_DIRECTIONS eNextDirection, FLOAT fDistance, BOOL bOffset = FALSE)
	STRING sDir, sDistance
	sDistance = sDistance
	fDistance = fDistance
	
	boffset = boffset
	sDir = sDir
	
	sDir = TAXI_GET_VEHICLE_DIRECTION_IN_STRING(eNextDirection)
	
	IF GET_IS_PLAYER_DRIVING_ON_HIGHWAY(GET_PLAYER_INDEX())
		sDir = "TURN AROUND"
	ENDIF
	#IF IS_DEBUG_BUILD
	sDistance = GET_STRING_FROM_FLOAT(fDistance)
	IF NOT bOffset
		DEBUG_DISPLAY_LITERAL_TEXT(sDir, 0.5, 0.1, 0.6, 0.6, 0.0, 1.0, INT_COLOUR(255,255,255,255))
		DEBUG_DISPLAY_LITERAL_TEXT(sDistance, 0.5, 0.15, 0.4, 0.4, 0.0, 1.0, INT_COLOUR(255,255,255,255))
	ELSE
		DEBUG_DISPLAY_LITERAL_TEXT(sDir, 0.5, 0.3, 0.6, 0.6, 0.0, 1.0, INT_COLOUR(255,150,255,255))
		DEBUG_DISPLAY_LITERAL_TEXT(sDistance, 0.5, 0.35, 0.4, 0.4, 0.0, 1.0, INT_COLOUR(255,150,255,255))
	ENDIF
	#ENDIF
	//sDir = sDir
ENDPROC

FUNC BOOL TAXI_IKW_HAS_FIRST_DIRECTION_GIVEN(INT &iNumVars[])
	INT i, k  
	REPEAT TAXI_NUM_DIRECTIONS i
		IF iNumVars[i] > 0
			k++
		ENDIF
	ENDREPEAT
	RETURN k > 0
ENDFUNC

INT iSteadyStart, iLastStart
VEHICLE_PATH_DIRECTIONS	eLastDirection
VEHICLE_PATH_DIRECTIONS	eTwoDirectionsAgo
BOOL bSteady, bSteadyTry, bWrongTurn

/// PURPOSE: This one uses dialogue from dialogue star
///    
/// PARAMS:
///    myTaxiData - 
PROC TAXI_DIALOGUE_UPDATE_PASSENGER_DIRECTIONS_EX(TaxiStruct &myTaxiData, VEHICLE_PATH_DIRECTIONS &ePrevDirection , INT &iNumVars[], VEHICLE_PATH_DIRECTIONS &eDebugNextDirection, VECTOR &vNextIntersection, BOOL & bResetWrongTimer, BOOL bDisplayDirections = FALSE)
	INT StreetNameHash
	FLOAT fDistance
	VECTOR vLocalIntersection
	
	VEHICLE_PATH_DIRECTIONS	eNextDirection
	
	IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_IKW_DIRECTIONS)
		CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: TT_IKW_DIRECTIONS timer not started, resetting")
		TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
		ePrevDirection = DIRECTIONS_UNKNOWN
	ELSE		
		//GENERATE_DIRECTIONS_TO_COORD(myTaxiData.vTaxiOJDropoff, 0, eNextDirection ,StreetNameHash, fDistance)
		vLocalIntersection = GENERATE_DIRECTIONS_TO_COORD_OUTPOS(myTaxiData.vTaxiOJDropoff, ENUM_TO_INT(GPS_FLAG_AVOID_HIGHWAY), eNextDirection,
											StreetNameHash, fDistance)
		
		// Direction has been consistent for a length of time, set bSteady to TRUE
		IF eNextDirection = eLastDirection
		AND (GET_GAME_TIMER() - iSteadyStart) >  PICK_INT((eNextDirection = DIRECTIONS_WRONG_WAY), 3000, 5000)
		AND NOT bSteady
		AND NOT bSteadyTry
			bSteady = TRUE
			CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: bSteady set to TRUE.  eLastDirection = ", 
						TAXI_GET_VEHICLE_DIRECTION_IN_STRING(eLastDirection))
			
		// Direction has been consistent for more then a moment, set bsteadtTry to false to make the above condition valid
		ELIF eNextDirection = eLastDirection
		AND (GET_GAME_TIMER() - iSteadyStart) > 2000
		AND bSteadyTry
			bSteadyTry = FALSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: bSteadyTry cleared to FALSE after 2 seconds.  eLastDirection is officially = ", 
						TAXI_GET_VEHICLE_DIRECTION_IN_STRING(eLastDirection))
			
		// Direction has reset to two directions ago, meaning there was just a momentary lapse in the return
		ELIF eNextDirection = eTwoDirectionsAgo
		AND bSteadyTry
			eLastDirection  = eTwoDirectionsAgo
			iSteadyStart = iLastStart
			bSteadyTry = FALSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: eNextDirection reset after momentary lapse. eLastDirection = ", 
						TAXI_GET_VEHICLE_DIRECTION_IN_STRING(eLastDirection))
		
		// Direction is different, check it
		ELIF eNextDirection <> eLastDirection
			
			bSteadyTry = TRUE
			eTwoDirectionsAgo = eLastDirection
			eLastDirection = eNextDirection 
			
			IF eLastDirection = DIRECTIONS_WRONG_WAY
			AND eTwoDirectionsAgo = DIRECTIONS_UNKNOWN
				bWrongTurn = TRUE
			ENDIF
			
			iLastStart = iSteadyStart
			iSteadyStart = GET_GAME_TIMER()
			//bSteady = FALSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: bSteady cleared to FALSE, iSteadyStart = ", iSteadyStart,
						". eLastDirection = ", TAXI_GET_VEHICLE_DIRECTION_IN_STRING(eLastDirection), 
						". eTwoDirectionsAgo = ", TAXI_GET_VEHICLE_DIRECTION_IN_STRING(eTwoDirectionsAgo))
		ENDIF
		
		fDistance = fDistance
		
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT GET_IS_PLAYER_DRIVING_ON_HIGHWAY(GET_PLAYER_INDEX())
				IF (fDistance <= 65.0 AND fDistance >= 20.0)
				AND  GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 3  //TAXI_DX_DIRECTION_DELAY
					//TURN LEFT------------------------------------------------------------------
					IF eNextDirection = DIRECTIONS_LEFT_AT_JUNCTION
					OR eNextDirection = DIRECTIONS_KEEP_LEFT
						IF bDisplayDirections
							eDebugNextDirection = eNextDirection
						ENDIF			
						
						vNextIntersection = vLocalIntersection
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_LEFT_AT_JUNCTION timer = ", 
												GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
												" iNumVars[TAXI_DIR_LEFT] = ", iNumVars[TAXI_DIR_LEFT])
						
						TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_LEFT)
						TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
						ePrevDirection = eNextDirection
					//TURN RIGHT------------------------------------------------------------------
					ELIF eNextDirection = DIRECTIONS_RIGHT_AT_JUNCTION
					OR eNextDirection = DIRECTIONS_KEEP_RIGHT
						IF bDisplayDirections
							eDebugNextDirection = eNextDirection
						ENDIF
						
						vNextIntersection = vLocalIntersection
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_RIGHT_AT_JUNCTION timer = ", 
												GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
												" iNumVars[TAXI_DIR_RIGHT] = ", iNumVars[TAXI_DIR_RIGHT])
						
						TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_RIGHT)
						TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
						ePrevDirection = eNextDirection
					ENDIF
					//GO STRAIGHT AT JUNCTION------------------------------------------------------------				
				ELIF ( ((GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 5) AND (fDistance <= 65.0 AND fDistance >= 15.0)) 
				 	OR ((GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 8) AND bSteady) )
				AND eNextDirection = DIRECTIONS_STRAIGHT_THROUGH_JUNCTION
				
					//IF ePrevDirection = eNextDirection
					//IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 10.0
					//OR iNumVars[TAXI_DIR_STRAIGHT] = 0
						IF bDisplayDirections
							eDebugNextDirection = eNextDirection
						ENDIF							
						
						vNextIntersection = vLocalIntersection
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_STRAIGHT_THROUGH_JUNCTION timer = ", 
												GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
												" iNumVars[TAXI_DIR_STRAIGHT] = ", iNumVars[TAXI_DIR_STRAIGHT])
						
						TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars, TAXI_DIR_STRAIGHT)//TAXI_DIR_STRAIGHT)
						TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
						ePrevDirection = eNextDirection
					//ENDIF
					
				ELIF fDistance = 0.0
					IF (eNextDirection = DIRECTIONS_WRONG_WAY
						OR eNextDirection = DIRECTIONS_UNKNOWN)
					//AND TAXI_IKW_HAS_FIRST_DIRECTION_GIVEN(iNumVars)
					// sometimes fDistance gets set to 0 if the player is completely going the opposite way
					// set a time to see if it's at 0 for more then a few seconds, then just tell the player to turn around
						IF NOT bResetWrongTimer
							bResetWrongTimer = TRUE
							CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: Reset timer for WRONG WAY")
							TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
						ELIF( GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 3.0
							AND ePrevDirection <> DIRECTIONS_WRONG_WAY )
						OR GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 7.0
							bResetWrongTimer = FALSE
							
							IF bDisplayDirections
								eDebugNextDirection = eNextDirection
							ENDIF
							
							IF bWrongTurn
							// let's possibly base the response by what previous turn you made
								bWrongTurn = FALSE
								CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_WRONG_WAY timer = ", 
														GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
														" iNumVars[TAXI_DIR_MISS] = ", iNumVars[TAXI_DIR_MISS])
								
								TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_MISS)
							ELSE
								CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_WRONG_WAY timer = ", 
														GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
														" iNumVars[TAXI_DIR_W_WAY] = ", iNumVars[TAXI_DIR_W_WAY])
								
								TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_W_WAY)
							ENDIF
							TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
							ePrevDirection = eNextDirection
						ENDIF
					ELIF eNextDirection = DIRECTIONS_KEEP_DRIVING
						IF NOT bResetWrongTimer
							bResetWrongTimer = TRUE
							CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: Reset timer for RIGHT WAY")
							TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
						ELIF ( GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 4.0
							AND ePrevDirection <> DIRECTIONS_KEEP_DRIVING )
						OR GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 7.0
							bResetWrongTimer = FALSE
							
							IF bDisplayDirections
								eDebugNextDirection = eNextDirection
							ENDIF
							
							// let's possibly base the response by what previous turn you made
							CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_KEEP_DRIVING timer = ", 
													GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
													" iNumVars[TAXI_DIR_KEEP_DRIVING] = ", iNumVars[TAXI_DIR_KEEP_DRIVING])
							
							TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars, TAXI_DIR_KEEP_DRIVING) //TAXI_DIR_R_WAY)
							TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
							ePrevDirection = eNextDirection
						ENDIF
					ENDIF
				ELIF bResetWrongTimer
					CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_PASSENGER_DIRECTIONS_EX: Reset wrong timer to FALSE")
					bResetWrongTimer = FALSE
				ENDIF
			
			ELIF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > TAXI_DX_GETOOFF_HWY_DELAY
				TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_GETOFF_HIGHWAY)
				TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
			ENDIF
		ENDIF
		
//		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//		AND ( (ePrevDirection <> DIRECTIONS_WRONG_WAY AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > TAXI_DX_DIRECTION_DELAY) 
//			OR (ePrevDirection = DIRECTIONS_WRONG_WAY AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 1.5 * TAXI_DX_DIRECTION_DELAY) )
//		
//			IF NOT GET_IS_PLAYER_DRIVING_ON_HIGHWAY(GET_PLAYER_INDEX())
//				IF (eNextDirection <> DIRECTIONS_UNKNOWN)
//				AND ( (fDistance <= 65.0 AND fDistance >= 15.0 AND TAXI_IKW_HAS_FIRST_DIRECTION_GIVEN(iNumVars))
//					OR NOT TAXI_IKW_HAS_FIRST_DIRECTION_GIVEN(iNumVars) )
//					//GOING RIGHT WAY-------------------------------------------------------------
//					IF ePrevDirection = DIRECTIONS_KEEP_DRIVING
//					AND eNextDirection = ePrevDirection
//						IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 4.0
//							IF bDisplayDirections
//								eDebugNextDirection = eNextDirection
//							ENDIF
//							
//							vNextIntersection = vLocalIntersection
//							
//							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DIALOGUE_UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_KEEP_DRIVING timer = ", 
//													GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
//													" iNumVars[TAXI_DIR_R_WAY] = ", iNumVars[TAXI_DIR_R_WAY])
//							
//							TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_R_WAY)
//							TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
//							ePrevDirection = DIRECTIONS_UNKNOWN
//						ENDIF
//						
//					//If you haven't been told to turn around and u haven't gotten an update than you're going the right way.
//					ELIF ePrevDirection = DIRECTIONS_WRONG_WAY
//					AND eNextDirection = ePrevDirection
//						IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 15.0
//							IF bDisplayDirections
//								eDebugNextDirection = eNextDirection
//							ENDIF
//							
//							vNextIntersection = vLocalIntersection
//							
//							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DIALOGUE_UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_WRONG_WAY timer = ", 
//													GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
//													" iNumVars[TAXI_DIR_W_WAY] = ", iNumVars[TAXI_DIR_W_WAY])
//							
//							TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_W_WAY)
//							TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
//							ePrevDirection = DIRECTIONS_UNKNOWN
//						ENDIF
//					
//					//ELIF ePrevDirection <> eNextDirection		//So it doesn't keep repeating to go straight at every intersection
//					ELSE	
//						//TURN LEFT------------------------------------------------------------------
//						//In actuality, these are backwards from the function description.
//						IF eNextDirection = DIRECTIONS_LEFT_AT_JUNCTION
//						OR eNextDirection = DIRECTIONS_KEEP_LEFT
//							IF bDisplayDirections
//								eDebugNextDirection = eNextDirection
//							ENDIF						
//							
//							vNextIntersection = vLocalIntersection
//							
//							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DIALOGUE_UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_LEFT_AT_JUNCTION timer = ", 
//													GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
//													" iNumVars[TAXI_DIR_LEFT] = ", iNumVars[TAXI_DIR_LEFT])
//							
//							TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_LEFT)
//							TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
//							ePrevDirection = eNextDirection
//						//TURN RIGHT------------------------------------------------------------------
//						ELIF eNextDirection = DIRECTIONS_RIGHT_AT_JUNCTION
//						OR eNextDirection = DIRECTIONS_KEEP_RIGHT
//							IF bDisplayDirections
//								eDebugNextDirection = eNextDirection
//							ENDIF
//							
//							vNextIntersection = vLocalIntersection
//							
//							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DIALOGUE_UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_RIGHT_AT_JUNCTION timer = ", 
//													GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
//													" iNumVars[TAXI_DIR_RIGHT] = ", iNumVars[TAXI_DIR_RIGHT])
//							
//							TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_RIGHT)
//							TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
//							ePrevDirection = eNextDirection
//						//GO STRAIGHT AT JUNCTION------------------------------------------------------------				
//						ELIF eNextDirection = DIRECTIONS_STRAIGHT_THROUGH_JUNCTION
//							//IF ePrevDirection = eNextDirection
//							IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 10.0
//							OR iNumVars[TAXI_DIR_STRAIGHT] = 0
//								IF bDisplayDirections
//									eDebugNextDirection = eNextDirection
//								ENDIF							
//								
//								vNextIntersection = vLocalIntersection
//								
//								CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DIALOGUE_UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_STRAIGHT_THROUGH_JUNCTION timer = ", 
//														GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
//														" iNumVars[TAXI_DIR_STRAIGHT] = ", iNumVars[TAXI_DIR_STRAIGHT])
//								
//								TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_STRAIGHT)
//								TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
//								ePrevDirection = eNextDirection
//							ENDIF
//						//GO STRAIGHT-----------------------------------------------------------------					
//						ELIF eNextDirection = DIRECTIONS_KEEP_DRIVING
//							IF bDisplayDirections
//								eDebugNextDirection = eNextDirection
//							ENDIF
//							
//							vNextIntersection = vLocalIntersection
//							
//							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DIALOGUE_UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_KEEP_DRIVING timer = ", 
//													GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS))
//							
//							TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
//							ePrevDirection = eNextDirection
//						//TURN AROUND------------------------------------------------------------------
//						ELIF eNextDirection = DIRECTIONS_WRONG_WAY
//						AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 10.0
//							IF bDisplayDirections
//								eDebugNextDirection = eNextDirection
//							ENDIF
//							
//							vNextIntersection = vLocalIntersection
//							
//							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DIALOGUE_UPDATE_PASSENGER_DIRECTIONS_EX: DIRECTIONS_WRONG_WAY timer = ", 
//													GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS), 
//													" iNumVars[TAXI_DIR_MISS] = ", iNumVars[TAXI_DIR_MISS])
//							
//							TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_MISS)
//							TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)
//							ePrevDirection = eNextDirection
//						ENDIF					
//						
////					ELIF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > 2.0
////						ePrevDirection = DIRECTIONS_UNKNOWN
//					ENDIF
//				ENDIF
//			ELIF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS) > TAXI_DX_GETOOFF_HWY_DELAY
//				TAXI_PLAY_SINGLE_LINE_DIRECTION(myTaxiData,iNumVars,TAXI_DIR_GETOFF_HIGHWAY)
//				TAXI_RESET_TIMERS(myTaxiData, TT_IKW_DIRECTIONS)			
//			ENDIF
//		ENDIF
	ENDIF
	
	IF bDisplayDirections
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_31 sStraightTime
		TEXT_LABEL_31 sDirectionTime
		sStraightTime = "Steady Timer: "
		sStraightTime += GET_STRING_FROM_INT((GET_GAME_TIMER() - iSteadyStart)/1000)
		sDirectionTime = "I Know a Way Timer: "
		sDirectionTime += GET_STRING_FROM_FLOAT(GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_IKW_DIRECTIONS))
		TAXI_DEBUG_PRINT_DIRECTIONS_TO_NEXT_INTERSECTION(eDebugNextDirection, fDistance)
		TAXI_DEBUG_PRINT_DIRECTIONS_TO_NEXT_INTERSECTION(eNextDirection, fDistance, TRUE)
		DRAW_DEBUG_TEXT_2D(sStraightTime, <<0.65, 0.2, 0.0>>)
		DRAW_DEBUG_TEXT_2D(sDirectionTime, <<0.65, 0.25, 0.0>>)
		DRAW_DEBUG_SPHERE(vNextIntersection, 4.0, 0, 0, 255, 150)
		DRAW_DEBUG_SPHERE(vLocalIntersection, 2.0, 0, 100, 100, 150)
	#ENDIF
	ENDIF
	
ENDPROC
/*

       oooo  ooooooo    ooooo ooooo            o888                                                 
  ooooo888 o888   888o   888   888  ooooooooo8  888 ooooooooo    ooooooooo8 oo oooooo    oooooooo8  
888    888 888     888   888ooo888 888oooooo8   888  888    888 888oooooo8   888    888 888ooooooo  
888    888 888o  8o888   888   888 888          888  888    888 888          888                888 
  88ooo888o  88ooo88    o888o o888o  88oooo888 o888o 888ooo88     88oooo888 o888o       88oooooo88  
                  88o8                              o888                                            

*/

PROC OPEN_DIALOGUE_QUEUE(TAXI_OJ_DIALOGUE_Q_DATA &tTaxiDQ_data, INT iLineToSkipTo = -1,TAXIOJ_DIALOGUE_Q_STATES eDQState = TDQ_STOPPED)
	
	
	tTaxiDQ_data.tDialogueQStates = eDQState
	
	//Usually when closing the queue, we want to make sure to advance the conversation
	IF iLineToSkipTo > -1
		tTaxiDQ_data.iCurrentDQLine = iLineToSkipTo
	ENDIF
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] OPEN_DIALOGUE_QUEUE - DQ Opened")
ENDPROC
PROC CLOSE_DIALOGUE_QUEUE(TAXI_OJ_DIALOGUE_Q_DATA &tTaxiDQ_data, INT iLineToSkipTo = -1,BOOL bKeepClosed = FALSE)
	
	IF bKeepClosed
		tTaxiDQ_data.tDialogueQStates = TDQ_CLOSED
	ELSE
		tTaxiDQ_data.tDialogueQStates = TDQ_STOPPED
	ENDIF
	
	//Usually when closing the queue, we want to make sure to advance the conversation
	IF iLineToSkipTo > -1
		tTaxiDQ_data.iCurrentDQLine = iLineToSkipTo
	ENDIF
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  CLOSE_DIALOGUE_QUEUE - DQ Closed")
ENDPROC

PROC CLEAR_DIALOGUE_QUEUE(TAXI_OJ_DQ_CONVERSATION_LINE &sImportantDX[])

	INT iIterator
	
	REPEAT CONST_TAXIOJ_SIZE_Q iIterator
	
		sImportantDX[iIterator].tTimeStamp = 0
		sImportantDX[iIterator].iDQFlagBits = 0
		sImportantDX[iIterator].sLineToSay = NULL_STRING()
	ENDREPEAT

	
	CLEAR_PRINTS()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  CLEAR_DIALOGUE_QUEUE - DQ Cleared out")

ENDPROC
/// PURPOSE: Really base function to check if a line hasn't played, then return it's index into the queue.
///    
/// PARAMS:
///    sImportantDX - our Dialogue Q
/// RETURNS:
///    
FUNC INT FIND_NEXT_FREE_SLOT_IN_DIALOGUE_QUEUE(TAXI_OJ_DQ_CONVERSATION_LINE &sImportantDX[])
	INT iIterator
	
	REPEAT CONST_TAXIOJ_SIZE_Q iIterator
		IF IS_STRING_NULL_OR_EMPTY(sImportantDX[iIterator].sLineToSay)
			RETURN iIterator
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC
FUNC TEXT_LABEL_23 FIND_NEXT_VALID_LINE_IN_DIALOGUE_QUEUE(TAXI_OJ_DQ_CONVERSATION_LINE &sImportantDX[])
	INT iIterator
	TEXT_LABEL_23 sReturnLineToSay	
	
	REPEAT CONST_TAXIOJ_SIZE_Q iIterator
		IF IS_BITMASK_AS_ENUM_SET(sImportantDX[iIterator].iDQFlagBits,TAXI_DQL_VALID)
			sReturnLineToSay = sImportantDX[iIterator].sLineToSay
			SET_BITMASK_AS_ENUM(sImportantDX[iIterator].iDQFlagBits,TAXI_DQL_CURRENTLY_PLAYING)
		ENDIF
	ENDREPEAT
	

	RETURN sReturnLineToSay
ENDFUNC

FUNC BOOL DOES_DIALOGUE_QUEUE_HAVE_VALID_LINE(TAXI_OJ_DQ_CONVERSATION_LINE &sImportantDX[])
	INT iIterator
	
	REPEAT CONST_TAXIOJ_SIZE_Q iIterator
		IF IS_BITMASK_AS_ENUM_SET(sImportantDX[iIterator].iDQFlagBits,TAXI_DQL_VALID)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC INT FIND_CURRENT_SLOT_PLAYING_IN_DIALOGUE_QUEUE(TAXI_OJ_DQ_CONVERSATION_LINE &sImportantDX[])
	INT iIterator
	
	REPEAT CONST_TAXIOJ_SIZE_Q iIterator
		IF 	IS_BITMASK_AS_ENUM_SET(sImportantDX[iIterator].iDQFlagBits,TAXI_DQL_CURRENTLY_PLAYING)
			RETURN iIterator
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC
PROC REGISTER_NEXT_LINE_IN_QUEUE(TEXT_LABEL_23 sLineToSay, TAXI_OJ_DQ_CONVERSATION_LINE &sImportantDX[])
	INT iOpenIndexInQueue = FIND_NEXT_FREE_SLOT_IN_DIALOGUE_QUEUE(sImportantDX)
	CDEBUG1LN(DEBUG_OJ_TAXI,"HIT 2 ")
	IF iOpenIndexInQueue > -1
		CDEBUG1LN(DEBUG_OJ_TAXI,"HIT 3 ")
		sImportantDX[iOpenIndexInQueue].sLineToSay = sLineToSay
		SET_BITMASK_AS_ENUM(sImportantDX[iOpenIndexInQueue].iDQFlagBits,TAXI_DQL_VALID)
	ENDIF
ENDPROC
PROC SET_CURRENT_LINE_PLAYING_IN_QUEUE_FINISHED(TAXI_OJ_DQ_CONVERSATION_LINE &sImportantDX[])
	INT iSlotCurrentlyPlaying = FIND_CURRENT_SLOT_PLAYING_IN_DIALOGUE_QUEUE(sImportantDX)
	
	IF iSlotCurrentlyPlaying > -1
		CLEAR_BITMASK_AS_ENUM(sImportantDX[iSlotCurrentlyPlaying].iDQFlagBits,TAXI_DQL_VALID)
		CLEAR_BITMASK_AS_ENUM(sImportantDX[iSlotCurrentlyPlaying].iDQFlagBits,TAXI_DQL_CURRENTLY_PLAYING)
		SET_BITMASK_AS_ENUM(sImportantDX[iSlotCurrentlyPlaying].iDQFlagBits,TAXI_DQL_DONE_PLAYING)
	
		sImportantDX[iSlotCurrentlyPlaying].sLineToSay = NULL_STRING()
	ENDIF
ENDPROC

FUNC BOOL IS_DIALOGUE_Q_CLOSED(TAXI_OJ_DIALOGUE_Q_DATA &tTaxiOJ_DQ_Data)
	RETURN tTaxiOJ_DQ_Data.tDialogueQStates = TDQ_CLOSED
ENDFUNC
/// PURPOSE: Use this to trigger a specific banter line when you know you've closed it. (usually when going into a cutscene or something)
///    
/// PARAMS:
///    eiEntity - the entity to check
///    vPosition - the point to check
///    fDist - 	the distance away to trigger from
///    tTaxiOJ_DQ_Data - 
///    iNumBanterLine - which banter conversation
PROC TRIGGER_TAXIOJ_BANTER_WHEN_VEHICLE_NEAR_COORD(VEHICLE_INDEX eVehicle, VECTOR vPosition, FLOAT fDist,TAXI_OJ_DIALOGUE_Q_DATA &tTaxiOJ_DQ_Data, INT iNumBanterLine	)
	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		
		IF IS_VEHICLE_DRIVEABLE(eVehicle)
			//|
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(eVehicle,vPosition) < fDist
				IF IS_DIALOGUE_Q_CLOSED(tTaxiOJ_DQ_Data	)
					OPEN_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,iNumBanterLine)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  Taxi Dialogue is playing banter # ", "",iNumBanterLine)
					
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC
/// PURPOSE: Put all the special resume conversation roots in this place. Does a simple compare to return true/false
///    
/// PARAMS:
///    tTaxiDQ_data - 
/// RETURNS:
///    
FUNC BOOL DOES_TAXI_LINE_HAVE_SPECIAL_RESUME(TAXI_OJ_DIALOGUE_Q_DATA &tTaxiDQ_data, TEXT_LABEL_23 &sResumeLine)
	IF ARE_STRINGS_EQUAL(tTaxiDQ_data.sCurrentConvoPlaying,"txm3_bant1")
	OR ARE_STRINGS_EQUAL(tTaxiDQ_data.sCurrentConvoPlaying,"txm9_bant2")
		sResumeLine +="_resBn1"
		
		RETURN TRUE
	ENDIF
	
	//Take to Best Specific	
	IF ARE_STRINGS_EQUAL(tTaxiDQ_data.sCurrentConvoPlaying,"txm6_bant3M")
	OR ARE_STRINGS_EQUAL(tTaxiDQ_data.sCurrentConvoPlaying,"txm6_bant3T")
	OR ARE_STRINGS_EQUAL(tTaxiDQ_data.sCurrentConvoPlaying,"txm6_bant3F")
		SET_BITMASK_AS_ENUM(tTaxiDQ_data.iSpecialMissionBits ,TAXI_SQB_TTB_BUMP_DROPPED)		
	
		sResumeLine +="_resBn1"
		
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] DOES_TX_LINE_HAVE_SPECIAL_RESUME Current Convo Playing is ", "", tTaxiDQ_data.sCurrentConvoPlaying)

	RETURN FALSE
ENDFUNC
//// PURPOSE: Play the appropriate feedback line based on tip accumulated on the ride
 ///    NOTE: MUST CALL BEFORE CONVERT_TAXI_TIP_TO_CASH***
 /// PARAMS:
 ///    myTaxiData - 
PROC TAXI_OJ_RATE_OVERALL_TIP_LEVEL(TaxiStruct &myTaxiData)
	
	KILL_FACE_TO_FACE_CONVERSATION()
	CLEAR_PRINTS()
	
	IF myTaxiData.iTaxiOJ_CashTip < myTaxiData.iTaxiOJ_CashTipAvg
		IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
			SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_RUNOFF,TRUE)
		ELSE
			SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_RATE_TIP_ASS,TRUE)
		ENDIF
		CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TAXI_RATE_OVERALL_TIP_LEVEL: TAXI_DI_RATE_TIP_ASS iTaxiOJ_CashTip = ",myTaxiData.iTaxiOJ_CashTip, " which is less than iTaxiOJ_CashTipAvg = ", myTaxiData.iTaxiOJ_CashTipAvg)
		
		OVERRIDE_PROPERTY_MISSION_PASS_REASON(PROPERTY_MISSION_PASS_REASON_OVERRIDE_NOT_GREAT)
		
	ELIF myTaxiData.iTaxiOJ_CashTip >= myTaxiData.iTaxiOJ_CashTipAmazing
		SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_RATE_TIP_AMAZING,TRUE)
		CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TAXI_RATE_OVERALL_TIP_LEVEL: TAXI_DI_RATE_TIP_AMAZING iTaxiOJ_CashTip = ",myTaxiData.iTaxiOJ_CashTip, " which is greater than iTaxiOJ_CashTipAmazing = ", myTaxiData.iTaxiOJ_CashTipAmazing)
	
	ELSE
		SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_RATE_TIP_AVERAGE,TRUE)
		CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TAXI_RATE_OVERALL_TIP_LEVEL: TAXI_DI_RATE_TIP_AVERAGE iTaxiOJ_CashTip = ",myTaxiData.iTaxiOJ_CashTip, "iTaxiOJ_CashTipAvg = ", myTaxiData.iTaxiOJ_CashTipAvg)
	ENDIF
	
	TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
	
ENDPROC

PROC PRINT_WITH_TIME_STRING(STRING pTextLabel, STRING sTime, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sTime)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

FUNC BOOL IS_THIS_PRINT_WITH_TIME_BEING_DISPLAYED(STRING pTextLabel, STRING sTime)
	BEGIN_TEXT_COMMAND_IS_MESSAGE_DISPLAYED(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sTime)
	RETURN END_TEXT_COMMAND_IS_MESSAGE_DISPLAYED()
ENDFUNC

PROC PRINT_TAXI_ODDJOB_OBJ(TaxiStruct &myTaxiData,BOOL bIsPickUpObj = FALSE)
	SWITCH myTaxiData.tTaxiOJ_MissionType
		CASE TXM_01_NEEDEXCITEMENT//--------------------------------------------------------------------------------------------------------
			IF bIsPickUpObj
				PRINT_NOW("TX_OBJ_NEX_PU", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to ~y~Mount Zonah Medical Center~s~ off Dorset Drive. ~s~
			ELSE
				PRINT_NOW("TX_OBJ_NEX_DO", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to the ~y~Vinewood sign~s~ without letting the passenger get too bored. ~s~			
			ENDIF
		BREAK
		
		CASE TXM_02_TAKEITEASY //--------------------------------------------------------------------------------------------------------
			IF bIsPickUpObj
				PRINT_NOW("TX_OBJ_TIE_PU", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to ~y~Up-n-Atom Burger~s~ on Las Lagunas Blvd in Vinewood.~s~
			ELSE
				PRINT_NOW("TX_OBJ_TIE_DO", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to ~y~Rob's~s~ in Vespucci.~s~			
			ENDIF
		BREAK
		
		CASE TXM_03_DEADLINE //--------------------------------------------------------------------------------------------------------
			IF bIsPickUpObj
				PRINT_NOW("TX_OBJ_DL_PU", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to ~y~Seagrass Herbals~s~ on Magellan Ave in Del Perro. ~s~
			ELSE
				PRINT_NOW("TX_OBJ_DL_DO", DEFAULT_GOD_TEXT_TIME, 1)
				//PRINT_NOW("TX_OBJ_DL_DO", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to the ~y~Casino~s~ next to the Vinewood Racetrack.~s~		
			ENDIF
		BREAK
		
		CASE TXM_04_GOTYOURBACK //--------------------------------------------------------------------------------------------------------
			IF bIsPickUpObj
				PRINT_NOW("TX_OBJ_GYB_PU", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to ~y~Ammunation~s~ on Adam's Apple Blvd in Downtown. ~s~
			ELSE
				PRINT_NOW("TX_OBJ_GYB_DO", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to the ~y~shipyard~s~ off Signal St in the Port. ~s~			
			ENDIF
		BREAK
		
		CASE TXM_05_TAKETOBEST //--------------------------------------------------------------------------------------------------------
			IF bIsPickUpObj
				PRINT_NOW("TX_OBJ_TTB_PU", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to the ~y~Galileo Observatory~s~ off Galileo Ave in Vinewood Hills. ~s~
			ELSE
				PRINT_NOW("TX_OBJ_TTB_DO", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to the best ~y~clothing store.~s~		
			ENDIF
		BREAK
		
		CASE TXM_07_CUTYOUIN //--------------------------------------------------------------------------------------------------------
			IF bIsPickUpObj
				PRINT_NOW("TX_OBJ_CYI_PU", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to ~y~Los Santos International Airport~s~ off New Empire Way.~s~
			ELSE
				PRINT_NOW("TX_OBJ_CYI_DO", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Go to ~y~Little Gems Jewelry~s~ off Atlee St in Downtown.		
			ENDIF
		BREAK
		
		CASE TXM_08_GOTYOUNOW //--------------------------------------------------------------------------------------------------------
			IF bIsPickUpObj
				PRINT_NOW("TX_OBJ_GYN_PU", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to ~y~ULSA~s~ off Picture Perfect Drive in Richman.~s~
			ELSE
				PRINT_NOW("TX_OBJ_GYN_DO", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to the ~y~construction site~s~ in Downtown. .~s~		
			ENDIF
		BREAK
		
		CASE TXM_09_CLOWNCAR //--------------------------------------------------------------------------------------------------------
			IF bIsPickUpObj
				PRINT_NOW("TX_OBJ_CC_PU", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to ~y~Golfing Society~s~ off W Eclipse Blvd in Richman.~s~
			ELSE
				PRINT_NOW("TAXI_OBJ_CC1", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to ~y~Maze bank~s~ off Alta St in Downtown.~s~	
			ENDIF
		BREAK
		
		CASE TXM_10_FOLLOWTHATCAR //--------------------------------------------------------------------------------------------------------
			IF bIsPickUpObj
				PRINT_NOW("TX_OBJ_FC_PU", DEFAULT_GOD_TEXT_TIME, 1)	//~s~Drive to this ~y~house~s~ on the corner of Sustancia Rd and El Rancho Blvd in Murrieta Heights.~s~
			ELSE
				PRINT_NOW("TAXI_OBJ_FTC1", DEFAULT_GOD_TEXT_TIME, 1) //~s~Drive to the ~y~Central Los Santos Medical Center~s~ off Capital Blvd in South Los Santos.			
			ENDIF
		BREAK
		
		CASE TXM_PROCEDURAL
			IF bIsPickUpObj
				PRINT_NOW("TX_OBJ_PRO_PU", DEFAULT_GOD_TEXT_TIME, 1) //~s~Drive to the ~y~nearby location~s~ to pick up the passenger. ~s~
			ELSE
				PRINT_NOW("TX_OBJ_PRO_DO", DEFAULT_GOD_TEXT_TIME, 1) //~s~Drive to the ~y~destination. ~s~
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF NOT bIsPickUpObj
//		//These 3 variations don't have standard dropoff blips.
//		IF myTaxiData.tTaxiOJ_MissionType <> TXM_09_CLOWNCAR
//		AND myTaxiData.tTaxiOJ_MissionType <> TXM_05_TAKETOBEST
//			//Passenger is in cab, do cleanup and add new blip for destination.
//			 IF NOT DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
//	            myTaxiData.blipTaxiDropOff = CREATE_BLIP_FOR_COORD(myTaxiData.vTaxiOJDropoff,TRUE)
//	            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SET_DROPOFF_BLIP_ON - Destination Blip set")
//	            
//	        ELIF GET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff) = 0
//	            SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,255)
//	            SET_BLIP_COORDS(myTaxiData.blipTaxiDropOff,myTaxiData.vTaxiOJDropoff)
//	            SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, TRUE)
//	        ENDIF	
//		ENDIF
		myTaxiData.bObjPrinted = TRUE
	ENDIF
	
ENDPROC
FUNC BOOL IS_VIP_TAXI_OBJ_BEING_DISPLAYED(TaxiStruct &myTaxiData)
	
	SWITCH myTaxiData.tTaxiOJ_MissionType
	
		CASE TXM_01_NEEDEXCITEMENT
			IF IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_NEX_DO") 
				RETURN TRUE
			ENDIF
		BREAK
			
		CASE TXM_02_TAKEITEASY
			IF IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_TIE_DO")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TXM_03_DEADLINE
			IF IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_DL_DO")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TXM_04_GOTYOURBACK
			IF IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_GYB_DO")
			OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_GYB")
			OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_GYB_2")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TXM_05_TAKETOBEST
			IF IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_TTB_DO")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TXM_07_CUTYOUIN
			IF IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_CYI_DO")
			OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_CYI_01")
			OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_CYI_02")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TXM_08_GOTYOUNOW
			IF IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_GYN_DO")
			OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_GYN")
			OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_GYN_TG")
			OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_GYN_GF")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TXM_09_CLOWNCAR
			IF IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_CC1")
			OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_CC2")
			OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_CC3")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TXM_10_FOLLOWTHATCAR
			IF IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_FTC1")
			OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_FTC2")
			OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_FTC3")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TXM_PROCEDURAL
			IF IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_PRO_DO")
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
		
	RETURN ( IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_GETRUN")
		OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_DRIVE")
		OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_RETURN")
		OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_POL")
		OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_RUNOUT")
		OR IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_POL")
		)
	
ENDFUNC
/*

ooooooooo   o88              o888                                                 oooo     oooo                                                                      
 888    88o oooo   ooooooo    888   ooooooo     oooooooo8 oooo  oooo  ooooooooo8   8888o   888   ooooooo   oo oooooo    ooooooo     oooooooo8 ooooooooo8 oo oooooo   
 888    888  888   ooooo888   888 888     888 888    88o   888   888 888oooooo8    88 888o8 88   ooooo888   888   888   ooooo888  888    88o 888oooooo8   888    888 
 888    888  888 888    888   888 888     888  888oo888o   888   888 888           88  888  88 888    888   888   888 888    888   888oo888o 888          888        
o888ooo88   o888o 88ooo88 8o o888o  88ooo88   888     888   888o88 8o  88oooo888  o88o  8  o88o 88ooo88 8o o888o o888o 88ooo88 8o 888     888  88oooo888 o888o       
                                               888ooo888                                                                           888ooo888                         

*/
/// PURPOSE:
///    Handles the dialogue and objectives for the mission. As these type of things usually go hand in hand.
///    This is gated by the DXTimer, that makes it wait for TAXI_DIALOGUE_DELAY
PROC TAXI_DIALOGUE_OBJ_MANAGER(TaxiStruct &myTaxiData, TAXI_OJ_DQ_CONVERSATION_LINE &sDialogueQ[])
	//CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DIALOGUE_OBJ_MANAGER: ")
	TEXT_LABEL_23 textLine
	TEXT_LABEL_23 sSpecificLine

	INT iIterator, iNumlines, iAppendee
	BOOL bMFTEnabled[CONST_TAXI_OJ_NUM_DX_LINES_TIME_GOOD]
	BOOL bSupportsTrevNFranklin //LM - i've been using this to sort out the M T F addends. TODO:  come up with a universal system for this
	
	
	IF IS_VIP_TAXI_OBJ_BEING_DISPLAYED(myTaxiData)
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_2D("OBJ ON SCREEN ",<<0.1,0.56,0.0>>)
		#ENDIF
		
		//CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DIALOGUE_OBJ_MANAGER: IS_ANY_TEXT_BEING_DISPLAYED")
	ELSE
	
/*

                 __                  _    
  ___ __ _ _ __ / _\_ __   ___  __ _| | __
 / __/ _` | '_ \\ \| '_ \ / _ \/ _` | |/ /
| (_| (_| | | | |\ \ |_) |  __/ (_| |   < 
 \___\__,_|_| |_\__/ .__/ \___|\__,_|_|\_\
                   |_|
*/
		IF NOT myTaxiData.bTaxiOJ_CanSpeak
			//CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("bSpeakOn - FALSE ", iDebugThrottle)
			//CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DIALOGUE_OBJ_MANAGER: myTaxiData.bTaxiOJ_CanSpeak = FALSE!")	
		//Player stepped out of car-------------------------------------------
		ELIF IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
			textLine = myTaxiData.sTaxiOJ_DXMissionID
			
			SWITCH GET_TAXI_SPEECH_INDEX(myTaxiData)
				//"Hey where are you going?"-----------------------------------
				
				CASE TAXI_DI_CYI_GET_BACK_TO_CAR
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TAXI_DIALOGUE_OBJ_MANAGER: TAXI_DI_CYI_GET_BACK_TO_CAR")
					textLine += "_lvMe1"
					sSpecificLine = textLine
					
					GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsGenericMission,CONST_TAXI_OJ_NUM_DX_LINES_CYI_RETURN_TO_CAR,sSpecificLine, FALSE, TRUE, FALSE)
					
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,textLine, sSpecificLine,CONV_PRIORITY_HIGH)
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_LEFT_CAR,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine, textLine)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_OBJ_LEFT_CAR
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF myTaxiData.bSecondPassenger
							textLine += "_retrn2"
						ELSE
							textLine += "_retrn1"
						ENDIF
						
						IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
							CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
						ENDIF
						
					ELSE
						PLAY_TAXI_PROP_MISSION_DIALOGUE_INTERRUPT(myTaxiData, textLine, GET_TAXI_INTERRUPT_STRING(myTaxiData, TAXI_OBJ_LEFT_CAR))
					ENDIF
					
					IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_Bits_Gates,TAXI_GATE_RETURN_TO_TAXI)
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					ENDIF
					
					SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_RETURN_TO_TAXI,TAXI_OBJ_RETURN)
					
				BREAK
				
				//Obj : ~s~Return to the taxi.~s~------------------------------
				CASE TAXI_OBJ_RETURN
					
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE) > 1.0
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						
						IF myTaxiData.tTaxiOJ_MissionType = TXM_PROCEDURAL
							PRINT_NOW("TAXI_OBJ_RETURN", DEFAULT_GOD_TEXT_TIME, 1)
						ELSE
							PRINT_NOW("TAXI_VIP_RETURN", DEFAULT_GOD_TEXT_TIME, 1)
						ENDIF
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - TAXI_OBJ_RETURN")
						
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						
						ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					ENDIF
				BREAK
				
				CASE TAXI_DI_AGGRO
					IF myTaxiData.bSecondPassenger
						textLine += "_aggro2"
					ELSE
			            textLine += "_aggro"
		    		ENDIF  
					
					myTaxiData.iTaxiOJ_AggroCount++				
					
					IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine,FALSE,TRUE)
					ELSE
						IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "TAXI_WHAT_THE_HELL", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
						ENDIF
					ENDIF
					//ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_FOLLOW_CAR_NEG_LOOSING_CAR,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine, FALSE)
				
				BREAK
				
			ENDSWITCH
			
		ELIF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE) > PICK_FLOAT((myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT), 1, TAXI_DX_DELAY)
		
		AND NOT myTaxiData.bIsTaxiDebugSkipping
		
		AND ( ((myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT OR myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY) 
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
			OR  (myTaxiData.tTaxiOJ_MissionType != TXM_01_NEEDEXCITEMENT AND myTaxiData.tTaxiOJ_MissionType != TXM_02_TAKEITEASY))
//			#IF IS_DEBUG_BUILD
//			DRAW_DEBUG_TEXT_2D("READY FOR CONVERSATION", << 0.7, 0.52, 0.0>>)
//			#ENDIF
			
			textLine = myTaxiData.sTaxiOJ_DXMissionID
			
			SWITCH GET_TAXI_SPEECH_INDEX(myTaxiData)
				//EXCEPTION DIALOGUE GETS EVALUATED FIRST
				CASE TAXI_DI_EXCEPTION_DISABLED
					IF myTaxiData.bSecondPassenger
						textLine += "_noDri2"
					ELSE
						textLine += "_noDri1"
					ENDIF
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine,TRUE)
				BREAK
				
				CASE TAXI_DI_CYI_DONT_FOLLOW_ME
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]: TAXI_DI_CYI_DONT_FOLLOW_ME")
					textLine += "_lvMe2"
					sSpecificLine = textLine
					
					GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsGenericMission,CONST_TAXI_OJ_NUM_DX_LINES_CYI_RETURN_TO_CAR,sSpecificLine, FALSE, TRUE, FALSE)
					
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,textLine, sSpecificLine,CONV_PRIORITY_HIGH)
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_LEFT_CAR,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine, textLine)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK				
				
				CASE TAXI_DISPATCH_DIRECTIONS
					// Print the convo line.
					IF myTaxiData.tTaxiOJ_MissionType = TXM_PROCEDURAL
//						textLine = TAXI_CONTROLLER_GET_DIALOGUE_ROOT(myTaxiData.tTaxiOJ_MissionType)
//						sSpecificLine = textLine
//						
//						//Acceptance line
//						sSpecificLine += "_5"
//						PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, "OJTXAUD", textLine, sSpecificLine, CONV_PRIORITY_HIGH)
						
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, "OJTXAUD", "OJTX_PRO_CON", CONV_PRIORITY_HIGH)
					ENDIF
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_PICKUP,TRUE)
				BREAK
				
				//Obj - Go pickup passenger------------------------------------------------
				CASE TAXI_OBJ_PICKUP
					//PRINT_TAXI_ODDJOB_OBJ(myTaxiData,TRUE)

					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - OBJ = TAXI_OBJ_PICKUP")
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_HAIL)
					
				BREAK
				
				//Want it to loop here til driver gets close-------------------------------
				CASE TAXI_DI_HAIL
					
					textLine += "_hail1"
					sSpecificLine = textLine
					
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_CAR_JACKED)
						IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						OR myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
						OR myTaxiData.tTaxiOJ_MissionType = TXM_05_TAKETOBEST
							GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsHail, 4, sSpecificLine, FALSE, TRUE, FALSE)
							PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, sSpecificLine, CONV_PRIORITY_HIGH)
						ELSE
							CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
						ENDIF
					ENDIF
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - OBJ = TAXI_DI_HAIL")
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
											
					//SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_PU_PASSENGER,TRUE)
					
					myTaxiData.bPassengerObjPrinted = TRUE
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				
				BREAK
				
				CASE TAXI_OBJ_PU_PASSENGER
					IF myTaxiData.tTaxiOJ_MissionType = TXM_09_CLOWNCAR
						PRINT_NOW("TAXI_OBJ_GPCKUP", DEFAULT_GOD_TEXT_TIME, 1)	//Passengers
					ELSE
						PRINT_NOW("TAXI_OBJ_PICKUP", DEFAULT_GOD_TEXT_TIME, 1)	//Passenger
					ENDIF
					myTaxiData.bPassengerObjPrinted = TRUE
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - OBJ = TAXI_OBJ_PU_PASSENGER -> TAXI_OBJ_PICKUP")
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_OBJ//--------------------------------------------------------------------------------------------
				//Take me to "__________"
					textLine += "_obj1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					//These 3 variations don't have standard dropoff blips.
					IF myTaxiData.tTaxiOJ_MissionType <> TXM_09_CLOWNCAR
					AND myTaxiData.tTaxiOJ_MissionType <> TXM_05_TAKETOBEST
						//Passenger is in cab, do cleanup and add new blip for destination.
						 IF NOT DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
				            myTaxiData.blipTaxiDropOff = CREATE_BLIP_FOR_COORD(myTaxiData.vTaxiOJDropoff,TRUE)
				            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SET_DROPOFF_BLIP_ON - Destination Blip set")
				            
				        ELIF GET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff) = 0
				            SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,255)
				            SET_BLIP_COORDS(myTaxiData.blipTaxiDropOff,myTaxiData.vTaxiOJDropoff)
				            SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, TRUE)
				        ENDIF	
					ENDIF
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE)
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_GIVE_MAIN
					
				BREAK
				
				
				//Special Greeting currently used only in Follow Car where it picks one of 3 variations based on attempt count
				CASE TAXI_DI_GREET//------------------------------------------------------------------------------------------
					
					
					//Figure out which variation to play
					IF myTaxiData.iTaxiOJ_NumDXVariations = 1
					OR myTaxiData.tTaxiOJ_MissionType = TXM_03_DEADLINE
					OR myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						textLine += "_gret1"
					ELSE
						SWITCH myTaxiData.iTaxiOJ_GetRunModCount
							CASE 1
								textLine += "_gret1"
							BREAK
							CASE 0 	//LM the answer is because we use modCount rather then straight iteration count
								textLine += "_gret2"
							BREAK
							CASE 2
								textLine += "_gret3"
							BREAK
							//If some have more than 2 vars send them here for now
							DEFAULT
								textLine += "_gret4"
							BREAK
						ENDSWITCH
					ENDIF
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)					
					
					IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					ELSE
						//PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GENERIC_HI", SPEECH_PARAMS_FORCE_FRONTEND)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "TAXI_START", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
					ENDIF
					//TAXI_RESET_TIMERS(myTaxiData, TT_STOPPED)
					
					
					//These 3 variations don't have standard dropoff blips.
					IF myTaxiData.tTaxiOJ_MissionType <> TXM_09_CLOWNCAR
					AND myTaxiData.tTaxiOJ_MissionType <> TXM_05_TAKETOBEST
						//Passenger is in cab, do cleanup and add new blip for destination.
						 IF NOT DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
				            myTaxiData.blipTaxiDropOff = CREATE_BLIP_FOR_COORD(myTaxiData.vTaxiOJDropoff,TRUE)
				            CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SET_DROPOFF_BLIP_ON - Destination Blip set")
				            
				        ELIF GET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff) = 0
				            SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,255)
				            SET_BLIP_COORDS(myTaxiData.blipTaxiDropOff,myTaxiData.vTaxiOJDropoff)
				            SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, TRUE)
				        ENDIF	
					ENDIF
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE)
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_GIVE_MAIN
				BREAK
				//----------------------------------------------------------------------------------------------------------------
				
				CASE TAXI_DI_TTB_SHOP//------------------------------------------------------------------------------------------
					
					textLine += "_dest2"
						
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TAXI_DI_TTB_SHOP - Playing Line - ","", textLine)
					
					TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_RUNOFF,textLine, "")
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE)
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_GIVE_MAIN
					
				BREAK
				//----------------------------------------------------------------------------------------------------------------
			
				//Give Mission Specific obj
				/*

                 _         ___ _     _ 
 _ __ ___   __ _(_)_ __   /___\ |__ (_)
| '_ ` _ \ / _` | | '_ \ //  // '_ \| |
| | | | | | (_| | | | | / \_//| |_) | |
|_| |_| |_|\__,_|_|_| |_\___/ |_.__// |
                                  |__/ 				

				*/
				CASE TAXI_OBJ_GIVE_MAIN
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINT_TAXI_ODDJOB_OBJ(myTaxiData)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - Greeting done, setting bit")
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GREETING_DONE)
						
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)					
				
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_FOLLOW_CAR_BARKS,FALSE)	
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - TAXI_OBJ_GIVE_MAIN")
					ENDIF
				BREAK
				//----------------------------------------------------------------------------------------------------------------
				CASE TAXI_DI_VARIABLE_BANTER
					//Pick a variation based on how many times you've played the mission
					SWITCH myTaxiData.iTaxiOJ_GetRunModCount
						CASE 0
							textLine += "_bant2"
						BREAK
						
						CASE 1
							textLine += "_bant1"
						BREAK
						
						CASE 2
							textLine += "_bant3"
						BREAK
						//If some have more than 2 vars send them here for now
						DEFAULT
							textLine += "_bant1"
						BREAK
					ENDSWITCH
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
							
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - TAXI_DI_VARIABLE_BANTER - ","", textLine)
				BREAK
				
				CASE TAXI_DI_BANTER//---------------------------------------------------------------------------------------------
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) > myTaxiData.fTaxiOJ_BanterDelay
						
						IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
							textLine += "_ban1"
						ELSE
							textLine += "_bant1"
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
						ENDIF
						
						REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_BANTER)
						
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
						
						ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - TAXI_DI_BANTER - ","", textLine)
						
						
					ENDIF
				BREAK
				
				CASE TAXI_DI_BANTER_2
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) > myTaxiData.fTaxiOJ_BanterDelay
					
						IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
							textLine += "_ban2"
						ELIF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
							textLine += "_banter"
						ELSE
							textLine += "_bant2"
							IF myTaxiData.tTaxiOJ_MissionType <> TXM_08_GOTYOUNOW
								TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
							ENDIF
						ENDIF
						
						REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
														
						TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
						
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - TAXI_DI_BANTER_2 ")
						ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
						
					ENDIF
				BREAK
				
				CASE TAXI_DI_BANTER_3
					
					IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						textLine += "_ban3"
					ELSE
						textLine += "_bant3"
						TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					ENDIF
					
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
							
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - TAXI_DI_BANTER_3 ")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)	
				BREAK
				CASE TAXI_DI_BANTER_4
					IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						textLine += "_BAN4"
					ELSE
						textLine += "_bant4"
					ENDIF
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
							
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - TAXI_DI_BANTER_4 ")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)	
				BREAK
				
				CASE TAXI_DI_BANTER_5
					IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						textLine += "_BAN5"
					ELSE
						textLine += "_bant5"
					ENDIF
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
							
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - TAXI_DI_BANTER_5 ")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)	
				BREAK
				//====================================================================================================================================
				
				CASE TAXI_DI_TTB_BANTER
					
					textLine += "_dest2b"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
						
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_TTB_BANTER)
					
					myTaxiData.fTaxiOJ_BanterDelay = 20.0
				BREAK
				//----------------------------------------------------------------------------------------------------------------
				CASE TAXI_DI_IKW_SEEPT
					
					textLine += "_seePt1"
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_IKW_DROPOFF,TRUE)
					
				BREAK
				
				CASE TAXI_OBJ_IKW_DROPOFF
					//YP I dont think we need this since im printing the objective in the script it self.
					//PRINT_NOW("TAXI_OBJ_KTW2", DEFAULT_GOD_TEXT_TIME, 1)
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_IKW_DROPOFF
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - TAXI_OBJ_KTW2")
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
				BREAK
				//----------------------------------------------------------------------------------------------------------------
				CASE TAXI_DI_AMBUSHED_TALK
					textLine += "_talk"
					textLine += 1
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine, TRUE)
					
					//SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_AMBUSH_EXTRA_BANTER, TRUE)
				BREAK
				//----------------------------------------------------------------------------------------------------------------
				CASE TAXI_DI_AMBUSHED

					SWITCH myTaxiData.iTaxiOJ_GetRunModCount
						CASE 0
							textLine += "_deal1" //"_deal2"
						BREAK
						
						CASE 1
							textLine += "_deal1"
						BREAK
						
						//If some have more than 2 vars send them here for now
						DEFAULT
							textLine += "_deal1"
						BREAK
					ENDSWITCH
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
										
				BREAK
				
				//----------------------------------------------------------------------------------------------------------------
				
				CASE TAXI_DI_AMBUSH_EXTRA_BANTER
					textLine += "_ig1c"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
				BREAK
				
				CASE TAXI_DI_TIME_BAD
					textLine += "_bTime"
					
					IF myTaxiData.tTaxiOJ_MissionType = TXM_03_DEADLINE
						iNumlines = CONST_TAXI_OJ_NUM_DX_LINES_TIME_GOOD
					ELSE
						iNumlines = CONST_TAXI_OJ_NUM_DX_LINES_TIME_MISC_GOOD
					ENDIF
						
					//Fill this array with TRUE for the num lines
					REPEAT iNumlines iIterator
						SWITCH myTaxiData.tTaxiOJ_MissionType
							CASE TXM_03_DEADLINE
								SWITCH iIterator
									CASE 3
									CASE 5
									CASE 6
									CASE 7
										bMFTEnabled[iIterator] = FALSE
									BREAK
									DEFAULT
										bMFTEnabled[iIterator] = TRUE
									BREAK
								ENDSWITCH
							BREAK
							
							DEFAULT 
								bMFTEnabled[iIterator] = TRUE
							BREAK
							
						ENDSWITCH	
					ENDREPEAT

					IF myTaxiData.tTaxiOJ_MissionType = TXM_03_DEADLINE
						GET_TAXI_OJ_LINE_TO_SAY_FOR_MULTIPLE_VARIATIIONS_WITH_MFT(myTaxiData.iTaxiOJ_DXBitsBTime,CONST_TAXI_OJ_NUM_DX_LINES_TIME_BAD,textLine,bMFTEnabled,TRUE,FALSE)
					ELSE
						GET_TAXI_OJ_LINE_TO_SAY_FOR_MULTIPLE_VARIATIIONS_WITH_MFT(myTaxiData.iTaxiOJ_DXBitsBTime,CONST_TAXI_OJ_NUM_DX_LINES_TIME_BAD_IKW,textLine,bMFTEnabled,TRUE,FALSE)
						
					ENDIF
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_DELIVERY_TIME,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine, FALSE)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					TAXI_RESET_TIMERS(myTaxiData, TT_DROPOFF,0,TRUE)
		
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
					
				BREAK
				
				//----------------------------------------------------------------------------------------------------------------
				
				CASE TAXI_DI_TIME_FAST
					textLine += "_gTime"
					
					//Add your mission to this IF, if it requires MFT - YP
					REPEAT CONST_TAXI_OJ_NUM_DX_LINES_TIME_GOOD iIterator
						SWITCH myTaxiData.tTaxiOJ_MissionType
							CASE TXM_03_DEADLINE
								SWITCH iIterator
									CASE 3
									CASE 4
									CASE 5
									CASE 7
										bMFTEnabled[iIterator] = FALSE
									BREAK
									DEFAULT
										bMFTEnabled[iIterator] = TRUE
									BREAK
								ENDSWITCH
							BREAK						
							
							DEFAULT 
								bMFTEnabled[iIterator] = TRUE
							BREAK
						ENDSWITCH	
					ENDREPEAT
					
					IF myTaxiData.tTaxiOJ_MissionType = TXM_03_DEADLINE
						GET_TAXI_OJ_LINE_TO_SAY_FOR_MULTIPLE_VARIATIIONS_WITH_MFT(myTaxiData.iTaxiOJ_DXBitsGTime,CONST_TAXI_OJ_NUM_DX_LINES_TIME_GOOD,textLine,bMFTEnabled,TRUE,FALSE)
					ELSE
						GET_TAXI_OJ_LINE_TO_SAY_FOR_MULTIPLE_VARIATIIONS_WITH_MFT(myTaxiData.iTaxiOJ_DXBitsGTime,CONST_TAXI_OJ_NUM_DX_LINES_TIME_GOOD_IKW,textLine,bMFTEnabled,TRUE,FALSE)
						
					ENDIF
					
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_DELIVERY_TIME,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine,TRUE)
				
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					TAXI_RESET_TIMERS(myTaxiData, TT_DROPOFF,0,TRUE)
		
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//----------------------------------------------------------------------------------------------------------------
				
				CASE TAXI_OBJ_GYB
					PRINT_NOW("TAXI_OBJ_GYB", 3500, 1) //DEFAULT_GOD_TEXT_TIME, 1)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_GYB")
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//----------------------------------------------------------------------------------------------------------------
				
				CASE TAXI_OBJ_GYN
					IF myTaxiData.tTaxiOJ_RideState = TRS_SAVE_DAMSEL
					OR myTaxiData.tTaxiOJ_RideState =TRS_PRE_CUTSCENE
						PRINT_NOW("TAXI_OBJ_GYN_TG", DEFAULT_GOD_TEXT_TIME, 1)
						
					ELIF myTaxiData.tTaxiOJ_RideState < TRS_WAIT_FOR_PASSENGER
						PRINT_NOW("TAXI_OBJ_GYN", DEFAULT_GOD_TEXT_TIME, 1)
					ELSE
						PRINT_NOW("TAXI_OBJ_GYN_GF", DEFAULT_GOD_TEXT_TIME, 1)
					ENDIF
					
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_GYN
					myTaxiData.bPassengerObjPrinted = TRUE
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_GYN")
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//----------------------------------------------------------------------------------------------------------------
				
				CASE TAXI_DI_1ST_STOP
					SWITCH myTaxiData.iTaxiOJ_GetRunModCount
						CASE 0
							textLine += "_dest1A"
						BREAK
						CASE 1
							textLine += "_dest1B"
						BREAK	
						DEFAULT
							textLine += "_dest1A"
						BREAK
					ENDSWITCH
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_WAIT_PASS,TRUE)
					
				BREAK
				
				CASE TAXI_OBJ_WAIT_PASS
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_Bits_Gates,TAXI_GATE_CYI_WAIT_PASS)
						PRINT_NOW("TAXI_OBJ_CYI_01", DEFAULT_GOD_TEXT_TIME, 1)
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_Bits_Gates,TAXI_GATE_CYI_WAIT_PASS)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_WAIT_PASS")
					ENDIF
					
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_WAIT_PASS
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)	
	
				BREAK
				
				CASE TAXI_DI_TO_AIRPORT_AMAZING
				
					textLine += "_rCar1"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)		
					
					//Todo: Clean this up and put into taxi_tips
					myTaxiData.sTaxiOJ_TipLine = textLine
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_TO_AIRPORT_AMAZING ")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)	
					
				BREAK
				
				CASE TAXI_DI_TO_AIRPORT_AVG
				
					textLine += "_rCar2"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)		
					
					//Todo: Clean this up and put into taxi_tips
					myTaxiData.sTaxiOJ_TipLine = textLine
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_TO_AIRPORT_AVG ")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)	
					
				BREAK
				
				CASE TAXI_DI_TO_AIRPORT_ASS
				
					textLine += "_rCar3"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)		
					
					//Todo: Clean this up and put into taxi_tips
					myTaxiData.sTaxiOJ_TipLine = textLine
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_TO_AIRPORT_ASS ")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)	
					
				BREAK
				CASE TAXI_OBJ_GO_AIRPORT
	
					PRINT_NOW("TAXI_OBJ_CYI_02", DEFAULT_GOD_TEXT_TIME, 1)
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_GO_AIRPORT
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_GO_AIRPORT")
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)	
					//SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_AIRPORT_BANTER,FALSE)
				BREAK
				
				CASE TAXI_DI_AIRPORT_BANTER
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_DEST_BANT)
						IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) > myTaxiData.fTaxiOJ_BanterDelay
							
							textLine += "_airBr1"
							
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
						
					
							IF myTaxiData.tTaxiOJ_MissionType = TXM_07_CUTYOUIN
								REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
							ELSE
								CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
							ENDIF
													
							SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_DEST_BANT)
							
							TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
							TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
							
							ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
						ENDIF
					ENDIF
				BREAK
				
				CASE TAXI_DI_SEEN_DESTINATION
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_SEEN_DEST)
						
						textLine += "_seeD1"
						
						TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_SEEN_DEST)
						
					ENDIF
				BREAK
				
								
				CASE TAXI_DI_SEEN_DESTINATION_2
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_SEEN_DEST_2)
						
						textLine += "_seeD2"
						
						TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_SEEN_DEST_2)
						
					ENDIF
				BREAK
				
				
				//Give obj
				CASE TAXI_DI_DROP_OFF
					textLine += "_done1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_DROP,TRUE)
				BREAK
				
				//Group Dropoff Single
				CASE TAXI_DI_FOLLOW
				
					//Figure out which variation to play
					IF myTaxiData.iTaxiOJ_NumDXVariations = 1
						textLine += "_ftc1"
					ELSE
						SWITCH myTaxiData.iTaxiOJ_GetRunModCount
							CASE 1
								textLine += "_ftc1"
							BREAK
							CASE 0
								textLine += "_ftc2"
							BREAK
							CASE 2
								textLine += "_ftc3"
							BREAK
							//If some have more than 2 vars send them here for now
							DEFAULT
								textLine += "_ftc3"
							BREAK
						ENDSWITCH
					ENDIF
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)				
				
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_FTC2,TRUE)
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_FTC2
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
										
				BREAK
				
				CASE TAXI_OBJ_FTC2
				
					PRINT_NOW("TAXI_OBJ_FTC2", DEFAULT_GOD_TEXT_TIME, 1)
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_DI_FOLLOW_CAR_BARKS	
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_FTC2 = TAXI_OBJ_FTC2")
									
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_FOLLOW_CAR_BARKS)
					
				BREAK
				
				//Group Dropoff
				CASE TAXI_DI_GROUP_DROPOFF
					textLine += "_dOff1"
					sSpecificLine = textLine
					
					iAppendee = GET_RANDOM_INT_IN_RANGE(0, 120)
					
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_DROPOFF)
						IF iAppendee > 80
							sSpecificLine += "_1"
						ELIF iAppendee > 40
							sSpecificLine += "_2"
						ELSE
							sSpecificLine += "_3"
						ENDIF
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_DROPOFF)
					ELSE
						IF iAppendee > 80
							sSpecificLine += "_4"
						ELIF iAppendee > 40
							sSpecificLine += "_5"
						ELSE
							sSpecificLine += "_6"
						ENDIF
					ENDIF
					
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, sSpecificLine, CONV_PRIORITY_HIGH)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ~~ TXCONVO ~~ specific label = ", sSpecificLine, " ~~~STATE =  ", GET_STRING_OJ_TAXI_DIALOGUE_INDEX(myTaxiData))
					
					//Make sure to reset your dialogue timer to space out chatter
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					//Close the dialogue gate behind you
					ENABLE_TAXI_SPEECH(myTaxiData, FALSE)
					
				BREAK
				
				CASE TAXI_DI_GROUP_DROPOFF_ASS
					textLine += "_bdOff1"
					sSpecificLine = textLine
					
					iAppendee = GET_RANDOM_INT_IN_RANGE(0, 100)
					
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_DROPOFF)
						IF iAppendee < 50
							sSpecificLine += "_1"
						ELSE
							sSpecificLine += "_2"
						ENDIF
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_DROPOFF)
					ELSE
						IF iAppendee < 50
							sSpecificLine += "_3"
						ELSE
							sSpecificLine += "_4"
						ENDIF
					ENDIF
					
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, sSpecificLine, CONV_PRIORITY_HIGH)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ~~ TXCONVO ~~ specific label = ", sSpecificLine, " ~~~STATE =  ", GET_STRING_OJ_TAXI_DIALOGUE_INDEX(myTaxiData))
					
					//Make sure to reset your dialogue timer to space out chatter
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					//Close the dialogue gate behind you
					ENABLE_TAXI_SPEECH(myTaxiData, FALSE)
					
					//CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
				BREAK
				
				CASE TAXI_DI_GROUP_DROPOFF2_GOOD
					textLine += "_dr2P"
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					//Make sure to reset your dialogue timer to space out chatter
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					//Close the dialogue gate behind you
					ENABLE_TAXI_SPEECH(myTaxiData, FALSE)
					
				BREAK
				
				CASE TAXI_DI_GROUP_DROPOFF2_BAD
					textLine += "_dr2N"
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					//Make sure to reset your dialogue timer to space out chatter
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					//Close the dialogue gate behind you
					ENABLE_TAXI_SPEECH(myTaxiData, FALSE)
				BREAK
				
				CASE TAXI_DI_GROUP_DROPOFF2
					textLine += "_dOff2"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_CC_2,TRUE)
				BREAK
				
				CASE TAXI_OBJ_CC_2
					PRINT_NOW("TAXI_OBJ_CC2", DEFAULT_GOD_TEXT_TIME, 1)
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_CC_2
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_CC_2 = TAXI_OBJ_CC2")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_CC_BANTER, TRUE)
				BREAK
				
				CASE TAXI_DI_GROUP_DROPOFF3
					textLine += "_dOff3"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_CC_3,TRUE)
				BREAK
				
				CASE TAXI_OBJ_CC_3
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINT_NOW("TAXI_OBJ_CC3", DEFAULT_GOD_TEXT_TIME, 1)
						myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_CC_3
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_CC_3 = TAXI_OBJ_CC3")
						ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
						
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_CC_BANTER2,TRUE)
					ENDIF
				BREAK
				
				CASE TAXI_OBJ_DROP
					PRINT_NOW("TAXI_OBJ_FTC3", DEFAULT_GOD_TEXT_TIME, 1)
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_DROP
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_DROP = TAXI_OBJ_FTC3")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//TAKE IT EASY 
				//N
				//NEEDS EXCITEMENT
				
				CASE TAXI_DI_SICK_WHOA
					//Comment on low comfort var 1
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_SICK1)
					
						TAXI_PLAY_CONVERSATION(myTaxiData,TAXI_DXF_SICK1,textLine, "_sick1")
						CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_SICK2)
						
					//Comment on low comfort var 2 only if var 1 has already played
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_SICK2)
						
						TAXI_PLAY_CONVERSATION(myTaxiData,TAXI_DXF_SICK2,textLine, "_sick2")
						CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_SICK1)
						
					ENDIF
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_QUICKSTOP,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
				BREAK
				//TAKE IT EASY 				
				CASE TAXI_DI_DIDNT_PUKE
					//Comment on low comfort var 1
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_NOPUKE1)
						textLine += "_nopke1"
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_NOPUKE2)					
						textLine += "_nopke2"
					ENDIF
					
					//TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue, TAXI_DXF_THANKS)
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Dialogue = TAXI_DI_DIDNT_PUKE where textline = ","",textline)

					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_NOPUKE,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine,TRUE)				
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_PUKE
					textLine += "_Puke1"
									
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_PUKE,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
										
				BREAK
				
				CASE TAXI_DI_PUKE_REACT_BAD
					textLine += "_PkStp2"
									
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_PUKE_NO_STOP,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
						
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_PUKE_REACT_BAD")
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_PUKE_REACT_GOOD
					textLine += "_PkStp1"
									
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_PUKE_STOP,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
						
				BREAK
				
				CASE TAXI_DI_PUKE_PLAYER_REACT
					textLine += "_PukeR1"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				//Drifting
				CASE TAXI_DI_POWERSLIDING
					//Comment on power sliding var 1
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_TURNS1)
					
						TAXI_PLAY_CONVERSATION(myTaxiData,TAXI_DXF_TURNS1,textLine, "_turns1")
					
					//Comment on powersliding var 2 only if var 1 has already played
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_TURNS2)
						
						TAXI_PLAY_CONVERSATION(myTaxiData,TAXI_DXF_TURNS2,textLine, "_turns2")
					ELSE
						TAXI_PLAY_CONVERSATION(myTaxiData,TAXI_DXF_TURNS2,textLine, "_turns3")
						CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_TURNS1)
						CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_TURNS2)
					ENDIF
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_POWERSLIDING,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
				BREAK
				
				//Collisions
				CASE TAXI_DI_COLLISION				

					
					//New Interrupts
					IF HAS_THIS_TAXI_PROP_MISSION_BEEN_UPDATED_FOR_NEW_INTERRUPTS(myTaxiData)
						PLAY_TAXI_PROP_MISSION_DIALOGUE_INTERRUPT(myTaxiData, textLine, GET_TAXI_INTERRUPT_STRING(myTaxiData, TAXI_DI_COLLISION))
						
					ELIF myTaxiData.tTaxiOJ_MissionType = TXM_PROCEDURAL
						
						IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "CRASH_GENERIC", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
						ENDIF
						
					//Take it Easy plays full conversations instead of single lines-----------------------------
					ELIF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
						
						textLine += "_carHt"
						sSpecificLine = textLine
					
						GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsCollision,CONST_TAXI_OJ_NUM_DX_LINES_COLLIDE_TIE,sSpecificLine, TRUE, FALSE,TRUE)	
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID, sSpecificLine,CONV_PRIORITY_HIGH)
										
					//The other variations play single lines
					ELSE
						textLine += "_carHt1"
						sSpecificLine = textLine
						//Needs Excitement has more variations------------------------------
						IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
							GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsCollision,CONST_TAXI_OJ_NUM_DX_LINES_COLLIDE_NEX,sSpecificLine, TRUE, TRUE, FALSE)
						
						ELSE
							GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsCollision,CONST_TAXI_OJ_NUM_DX_LINES_COLLIDE,sSpecificLine, TRUE, TRUE, FALSE)
						ENDIF
					
						PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,textLine, sSpecificLine,CONV_PRIORITY_HIGH)
					ENDIF
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_TOOK_DAMAGE,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,sSpecificLine)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_COLLISION_BIG // DIALOGUE DELETE?
					textLine += "_genPnHi"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
				BREAK
				//Collision
				CASE TAXI_DI_NEARMISS
					//Comment on near miss var 1
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_NEARMISS1)
					
						//TAXI_PLAY_CONVERSATION(myTaxiData,TAXI_DXF_NEARMISS1,textLine, "_nMiss1")
						
						textLine += "_nMiss1"
						
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ------------ LINE IS  ","", textLine)
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_NEARMISS1)
						CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_NEARMISS2)
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					//Comment on near miss var 2 only if var 1 has already played
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_NEARMISS2)
					
						//TAXI_PLAY_CONVERSATION(myTaxiData,TAXI_DXF_NEARMISS2,textLine, "_nMiss2")
						
						textLine += "_nMiss2"
						
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ------------ LINE IS  ","", textLine)
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_NEARMISS2)
						CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_NEARMISS3)
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_NEARMISS3)
					
						//TAXI_PLAY_CONVERSATION(myTaxiData,TAXI_DXF_NEARMISS2,textLine, "_nMiss3")
						
						textLine += "_nMiss3"
						
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ------------ LINE IS  ","", textLine)
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_NEARMISS3)
						CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_NEARMISS1)
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						
					ENDIF
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				
				//Airborn
				CASE TAXI_DI_AIRBORN
					IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
					OR myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						bSupportsTrevNFranklin = TRUE
					ENDIF
				
					//Comment on airborn var 1
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_AIR1)
					
						//TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_AIR1,textLine, "_air1", bSupportsTrevNFranklin)
						
						textLine += "_air1"
											
						//Play one way conversation
						IF bSupportsTrevNFranklin
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
						ENDIF
						
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ------------ LINE IS  ","", textLine)
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_AIR1)
						CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_AIR2)
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					//Comment on airborn var 2 only if var 1 has already played
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_AIR2)
						
						//TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_AIR2,textLine, "_air2", bSupportsTrevNFranklin)
						
						textLine += "_air2"
											
						//Play one way conversation
						IF bSupportsTrevNFranklin
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
						ENDIF
						
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ------------ LINE IS  ","", textLine)
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_AIR2)
						IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
							CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_AIR3)
						ELSE
							CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_AIR1)
						ENDIF
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_AIR3)
						
						textLine += "_air3"
											
						//Play one way conversation
						IF bSupportsTrevNFranklin
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
						ENDIF
						
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ------------ LINE IS  ","", textLine)
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_AIR3)
						CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_AIR1)
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						
					ENDIF
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_AIR,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//Driving on the sidewalk
				CASE TAXI_DI_SIDEWALKIN
					IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
					OR myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						bSupportsTrevNFranklin = TRUE
					ENDIF
				
					//Comment on sidewalk driving var 1
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_SIDEWALK1)
					
						TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_SIDEWALK1,textLine, "_sideW1", bSupportsTrevNFranklin)
					
					//Comment on sidewalk driving var 2 only if var 1 has already played
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_SIDEWALK2)
					
						TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_SIDEWALK2,textLine, "_sideW2", bSupportsTrevNFranklin)

					ENDIF
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_SIDEWALK,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//Driving into oncoming traffic
				CASE TAXI_DI_OPPLANE
					IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
					OR myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						bSupportsTrevNFranklin = TRUE
					ENDIF
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_DI_OPPLANE should be playing here ********")
					
					
					//Comment on Driving into oncoming traffic var 1
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_LANE1)
					
						TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_LANE1,textLine, "_opLne1", bSupportsTrevNFranklin)
					//Comment on Driving into oncoming traffic var 2 only if var 1 has already played
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_LANE2)
					
						TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_LANE2,textLine, "_opLne2", bSupportsTrevNFranklin)


					ENDIF
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_ONCOMING,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_BORING
				
					IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						
						IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_BORED1)
						
							TAXI_PLAY_CONVERSATION_EX(myTaxiData,TAXI_DXF_BORED1,textLine, "_bored1")	
						
						ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_BORED2)
						
							TAXI_PLAY_CONVERSATION_EX(myTaxiData,TAXI_DXF_BORED2,textLine, "_bored2")	
						
						ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_BORED3)
						
							TAXI_PLAY_CONVERSATION_EX(myTaxiData,TAXI_DXF_BORED3,textLine, "_bored3")
							
						ENDIF
					
						ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_BORED,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
						ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
						
					ENDIF
					
					IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
					//AND	myTaxiData.iTaxiOJ_PassengerExcitement < 15 
						textLine += "_good1"
						sSpecificLine = textLine
						
						GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsGenericMission, CONST_TAXI_OJ_NUM_DX_LINES_GOOD_TIE, sSpecificLine, FALSE, TRUE, FALSE)
						PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, sSpecificLine, CONV_PRIORITY_HIGH)
						
						ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_BORED,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
			
						ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					ENDIF
				
				BREAK
				
				CASE TAXI_DI_EXCITED_TO_BORED
					textLine += "_EtoB1"
					sSpecificLine = textLine
					
					GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsExcitedToBored, CONST_TAXI_OJ_NUM_DX_LINES_BG_TRANS, sSpecificLine, FALSE, TRUE, FALSE)
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, sSpecificLine, CONV_PRIORITY_HIGH)
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_BORED,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_BORED_TO_EXCITED
					textLine += "_BtoE1"
					sSpecificLine = textLine
					
					GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsBoredToExcited, CONST_TAXI_OJ_NUM_DX_LINES_BG_TRANS, sSpecificLine, FALSE, TRUE, FALSE)
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, sSpecificLine, CONV_PRIORITY_HIGH)
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_QUICKSTOP,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//Running a red light
				CASE TAXI_DI_RANRED
					//Comment on running the light var 1
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_LIGHT1)
					
						TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_LIGHT1,textLine, "_runLit1")
					
					//Comment on running the light var 2 only if var 1 has already played
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_LIGHT2)
					
						TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_LIGHT2,textLine, "_runLit2")

					ENDIF
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//Hit N Run
				CASE TAXI_DI_HITNRUN
					//New Interrupts
					IF HAS_THIS_TAXI_PROP_MISSION_BEEN_UPDATED_FOR_NEW_INTERRUPTS(myTaxiData)
						PLAY_TAXI_PROP_MISSION_DIALOGUE_INTERRUPT(myTaxiData, textLine, GET_TAXI_INTERRUPT_STRING(myTaxiData, TAXI_DI_HITNRUN))
					ELIF myTaxiData.tTaxiOJ_MissionType = TXM_PROCEDURAL
						IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "CAR_HIT_PED_DRIVEN", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
						ENDIF
					ELSE
						//Comment on hit n run  var 1
						IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR1)
						
							//TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_HITNR1,textLine, "_hitR1")
							
							textLine += "_hitR1"
							
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
							
							CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
							CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ------------ LINE IS  ","", textLine)
							
							SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR1)
							CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR2)
							TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
							
						//Comment on hit n run var 2 only if var 1 has already played
						ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR2)
							//TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_HITNR2,textLine, "_hitR2")
							
							textLine += "_hitR2"
							
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
							
							CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
							CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ------------ LINE IS  ","", textLine)
							
							SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR2)
							CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR3)
							TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
							
						ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR3)
							//TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_HITNR2,textLine, "_hitR2")
							
							textLine += "_hitR3"
							
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
							
							CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
							CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ------------ LINE IS  ","", textLine)
							
							SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR3)
							
							IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
								CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR4)
							ELSE
								CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR1)
							ENDIF
							TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						
						ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR4)
							
							//TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_HITNR2,textLine, "_hitR2")
							
							textLine += "_hitR4"
							
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
							
							CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
							CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] ------------ LINE IS  ","", textLine)
							
							SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR4)
							CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_HITNR1)
							TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
							
						ENDIF
					ENDIF
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_HIT_PED,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_AGGRO
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF myTaxiData.bSecondPassenger
							textLine += "_aggro2"
						ELSE
				            textLine += "_aggro"
			    		ENDIF
						
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine,FALSE,TRUE)
					ELSE
						PLAY_TAXI_PROP_MISSION_DIALOGUE_INTERRUPT(myTaxiData, textLine, GET_TAXI_INTERRUPT_STRING(myTaxiData, TAXI_DI_AGGRO))
						ENABLE_TAXI_SPEECH(myTaxiData, FALSE)
					ENDIF
					//ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_FOLLOW_CAR_NEG_LOOSING_CAR,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine, FALSE)
					myTaxiData.iTaxiOJ_AggroCount++
				
				BREAK
				
				CASE TAXI_DI_SHOOTING
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF myTaxiData.tTaxiOJ_MissionType = TXM_05_TAKETOBEST
							textline = "tm6_shoot"
						ELIF myTaxiData.bSecondPassenger
							textline += "_shootlz"
						ELSE
							textline += "_shoot"
						ENDIF
						
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine,FALSE,TRUE)
					ELSE
						PLAY_TAXI_PROP_MISSION_DIALOGUE_INTERRUPT(myTaxiData, textLine, GET_TAXI_INTERRUPT_STRING(myTaxiData, TAXI_DI_SHOOTING))
						ENABLE_TAXI_SPEECH(myTaxiData, FALSE)
					ENDIF
					myTaxiData.iTaxiOJ_AggroCount++
				BREAK
				
				//FOLLOW CAR//////////////////////////////////////////////////
				CASE TAXI_DI_FOLLOW_CAR_BARKS
					IF myTaxiData.tTaxiOJ_MissionType = TXM_10_FOLLOWTHATCAR
					//IF TAXI_TIMER_DO_WHEN_READY(myTaxiData, TT_DIALOGUE,TAXI_DIALOGUE_DELAY)
						IF NOT myTaxiData.bTaxiOJ_Failed
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							SWITCH myTaxiData.tTaxiOJ_FollowPosition
								
								CASE TAXI_TOO_CLOSE_WARNING
									textLine += "_warnC1"
						
									CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
									
									CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_TOO_CLOSE_WARNING")
								
								BREAK
								
								CASE TAXI_TOO_FAR_WARNING
									textLine += "_warnF1"
									
									CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
									
									CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_TOO_FAR_WARNING")
									
								BREAK
								
								CASE TAXI_LOSING_CHASE
									textLine += "_far1"
									
									CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
									ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_FOLLOW_CAR_NEG_LOOSING_CAR,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine, FALSE)
					
									CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_LOSING_CHASE")
									
								BREAK
							
								CASE TAXI_TOO_CLOSE_TARGET
									textLine += "_close1"
									
									CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
									ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_FOLLOW_CAR_NEG_TOO_CLOSE_TO_CAR,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine, FALSE)
					
									CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_TOO_CLOSE_TARGET")

								BREAK
								
								CASE TAXI_HIT_CAR
									textLine += "_hit1"
									
									CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
								
									CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_HIT_CAR")
								BREAK
								
								CASE TAXI_SWEET_SPOT
									textLine += "_good1"
									sSpecificLine = textLine
								
									GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsGenericMission,CONST_TAXI_OJ_NUM_DX_LINES_SWEETSPOT_FTC,sSpecificLine, TRUE, TRUE, FALSE)
									
									PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,textLine, sSpecificLine,CONV_PRIORITY_HIGH)
									ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_FOLLOW_CAR_POS_SWEET_SPOT,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,sSpecificLine, FALSE)
					
									CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_SWEET_SPOT")
									TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
									ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
									
								BREAK
								
								CASE TAXI_SWEET_SPOT_BANTER
									IF NOT bSayFCRandomBanter
										textLine += "_sBant1"
										sSpecificLine = textLine
									
//										GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsFC_SS_Banter,8,sSpecificLine, FALSE, TRUE, FALSE)
//										
//										PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,textLine, sSpecificLine,CONV_PRIORITY_HIGH)
										
										CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
										
										CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_SWEET_SPOT_BANTER")
										TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
										TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
										ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
										
										IF NOT bSayFCBanter2
											bSayFCRandomBanter = TRUE
										ENDIF
										
									ELSE
										IF NOT bSayFCBanter3
											textLine += "_bant3"
											bSayFCBanter3 = TRUE
										ELSE
											textLine += "_bant2"
											bSayFCBanter2 = TRUE
										ENDIF
										
										TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
										CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData, textLine)
												
										TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
										TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
										CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - ", textLine)
										ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
										
										bSayFCRandomBanter = FALSE
									ENDIF

								BREAK

								CASE TAXI_ARRIVE_WALK
									textLine += "_done1"
									
									CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
								
									CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_ARRIVE_WALK")
								BREAK
								
							ENDSWITCH
							
							SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_EMPTY)
						ENDIF
					//ENDIF
					ENDIF
				BREAK
				
				//SPEEDING------------------------------------------------------------------
				CASE TAXI_DI_SPEEDING 
					textLine += "_speed1"
					sSpecificLine = textLine
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"SPEEDING CONVERSATION SHOULD PLAY HERE ********")
					
					GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsSpeed, CONST_TAXI_OJ_NUM_DX_LINES_SPEED, sSpecificLine, TRUE, TRUE, FALSE)
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, sSpecificLine,CONV_PRIORITY_HIGH)
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_SPEEDING,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
		
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				//--------------------------------------------------------------------------
				
				CASE TAXI_DI_GET_CLOSER
					
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF2_STOP_CLOSE1)
						TAXI_PLAY_CONVERSATION_EX(myTaxiData,TAXI_DXF2_STOP_CLOSE1,textLine, "_close1")
						
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF2_STOP_CLOSE2)
						TAXI_PLAY_CONVERSATION_EX(myTaxiData,TAXI_DXF2_STOP_CLOSE2,textLine, "_close2")
						
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF2_STOP_CLOSE3)
						TAXI_PLAY_CONVERSATION_EX(myTaxiData,TAXI_DXF2_STOP_CLOSE3,textLine, "_close3")
					ENDIF
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)

				BREAK
				
			
				//TAXI COMPLETELY STOPPED------------------------------------------------------------------------------------
				//TODO this doesn't work with different players
				CASE TAXI_DI_COMPLETE_STOP
					IF myTaxiData.bSecondPassenger
						textLine += "_stop2"
					ELSE
						textLine += "_stop1"
					ENDIF
					sSpecificLine = textLine
					
					GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsStopped,CONST_TAXI_OJ_NUM_DX_LINES_STOPPED,sSpecificLine, FALSE, TRUE, FALSE)
					
					IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
						PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,textLine, sSpecificLine,CONV_PRIORITY_HIGH)
					ELSE
						IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "TAXI_STOPPED", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
						ENDIF
					ENDIF
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_STOPPED,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//ROLLED TAXI------------------------------------------------------------------------------------------------
				CASE TAXI_DI_ROLLED
					
					//New Interrupts
					IF HAS_THIS_TAXI_PROP_MISSION_BEEN_UPDATED_FOR_NEW_INTERRUPTS(myTaxiData)
						PLAY_TAXI_PROP_MISSION_DIALOGUE_INTERRUPT(myTaxiData, textLine, GET_TAXI_INTERRUPT_STRING(myTaxiData, TAXI_DI_ROLLED))
					ELIF myTaxiData.tTaxiOJ_MissionType = TXM_PROCEDURAL
						IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "VEHICLE_FLIPPED", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
						ENDIF
					ELSE
						textLine += "_roll1"
						TAXI_PLAY_CONVERSATION_ROLLED(myTaxiData,textLine)
					ENDIF
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_ROLL_CAR,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_ROLLED")
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//SWERVE TAXI------------------------------------------------------------------------------------------------
				CASE TAXI_DI_SWERVE
					//Comment on swerve var 1
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue, TAXI_DXF_SWRV1)
					
						//TAXI_PLAY_CONVERSATION_EX(myTaxiData,TAXI_DXF_SWRV1,textLine, "_swrv1", CONV_PRIORITY_HIGH, TRUE)
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue, TAXI_DXF_SWRV1)
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						
						textLine += "_swrv1"
						TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
						
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TX_PLAY_CONVERSATION_EX ------------ LINE IS  ", textLine)
						
					//Comment on swerve var 2 only if var 1 has already played
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_SWRV2)
						
						//TAXI_PLAY_CONVERSATION_EX(myTaxiData,TAXI_DXF_SWRV2,textLine, "_swrv2", CONV_PRIORITY_HIGH, TRUE)
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue, TAXI_DXF_SWRV2)
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						
						textLine += "_swrv2"
						TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
						
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TX_PLAY_CONVERSATION_EX ------------ LINE IS  ", textLine)
						
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ReactBitsDialogue,TAXI_DXF_SWRV3)
						
						//TAXI_PLAY_CONVERSATION_EX(myTaxiData,TAXI_DXF_SWRV2,textLine, "_swrv2", CONV_PRIORITY_HIGH, TRUE)
						// let's only play the three variations and not repeat them
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ReactBitsDialogue, TAXI_DXF_SWRV3)
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
						
						textLine += "_swrv3"
						TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
						
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_HIGH)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TX_PLAY_CONVERSATION_EX ------------ LINE IS  ", textLine)
						
					ENDIF
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
				BREAK
				
				//REVERSE TAXI------------------------------------------------------------------------------------------------
				CASE TAXI_DI_REVERSE
					//Comment on low comfort var 1
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue, TAXI_DXF_RVRS1)
					
						TAXI_PLAY_CONVERSATION_EX(myTaxiData,TAXI_DXF_RVRS1,textLine, "_rvrs1", CONV_PRIORITY_HIGH, TRUE)
						CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_RVRS2)
						
					//Comment on low comfort var 2 only if var 1 has already played
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_RVRS2)
						
						TAXI_PLAY_CONVERSATION_EX(myTaxiData,TAXI_DXF_RVRS2,textLine, "_rvrs2", CONV_PRIORITY_HIGH, TRUE)
						// let's only play the two variations and not repeat them
						//CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_RVRS1)
						
					ENDIF
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
				BREAK
				
				//OFFROAD TAXI------------------------------------------------------------------------------------------------
				CASE TAXI_DI_OFFROAD
					textLine += "_ofrd1"
					sSpecificLine = textLine
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"OFFROAD CONVERSATION SHOULD PLAY HERE ********")
					
					GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsOffroad, CONST_TAXI_OJ_NUM_DX_LINES_OFFROAD, sSpecificLine, FALSE, TRUE, FALSE)
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, sSpecificLine,CONV_PRIORITY_HIGH)
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_OFFROAD,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,sSpecificLine)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
		
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_OFFROAD_CHALLENGE
					
					textLine += "_ofrdch1"

					CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_VERY_HIGH)
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
				BREAK
				
				CASE TAXI_LOSE_JEWELRY_POL
					textLine += "_losPol1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_POLICE,TRUE)	
				BREAK
				
				CASE TAXI_DI_WENT_WANTED
					IF myTaxiData.bSecondPassenger
						textLine += "_lsPo2"
					ELSE
						textLine += "_lsPo1"
					ENDIF
					
					IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					ELSE
						IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "POLICE_PURSUIT_DRIVEN", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
						ENDIF
					ENDIF
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)	
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_WENT_WANTED = _lsPo1")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//GOING WANTED------------------------------------------------------------------------------------------------
				CASE TAXI_DI_ESCAPE_POLICE
					
					IF HAS_THIS_TAXI_PROP_MISSION_BEEN_UPDATED_FOR_NEW_INTERRUPTS(myTaxiData)
						
						//Deadline you can lose if you don't lose em fast enough.
						IF myTaxiData.tTaxiOJ_MissionType = TXM_03_DEADLINE
							textLine += "_copBa1"
							sSpecificLine = textLine
							
							sSpecificLine = textLine
							CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
							
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_POLICE,TRUE)	
							TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)	
							//ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
						ELSE
							IF myTaxiData.tTaxiOJ_MissionType = TXM_07_CUTYOUIN
							AND myTaxiData.tTaxiOJ_RideState > TRS_DRIVING_PASSENGER
								textLine += "_copBa2"
							ELSE
								textLine += "_copBa1"
							ENDIF
							
							sSpecificLine = textLine
							CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
							
							TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)	
							//ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
						ENDIF
					ELSE	
						textLine += "_copBa1"
						sSpecificLine = textLine

						GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsPolice,CONST_TAXI_OJ_NUM_DX_LINES_POLICE,sSpecificLine, TRUE, TRUE, FALSE)
						PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,textLine, sSpecificLine,CONV_PRIORITY_HIGH)
						TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)	
						ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					ENDIF
				BREAK
				
				CASE TAXI_DI_POLICE_LOST

					textLine += "_evdeP1"
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_LOST_POLICE,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_BORING,0,TRUE)
				BREAK
				
				//Obj - Lose the police
				CASE TAXI_OBJ_POLICE
					IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) >= 1
						//hack hack hack --johnsripan
						PRINT_NOW("TAXI_OBJ_POL", DEFAULT_GOD_TEXT_TIME, 1)
						myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_POLICE
					ENDIF
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_POLICE = TAXI_OBJ_POL")
				BREAK
				
				CASE TAXI_DI_POLICE_ARREST
					textLine += "_cpFz1"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
				BREAK
				
				//-------------------------------------------------------------------------------------------------------------
				//Radio Comments///
				//Tells player to change the station
				CASE TAXI_DI_RADIO_CHANGE

					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_RADIO_CHANGE)
						TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_RADIO_CHANGE,textLine, "_rdCh1")
			
					
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_RADIO_CHANGE2)
						IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						OR myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
							TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_RADIO_CHANGE2,textLine, "_rdCh2")	
						ELSE
							TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_RADIO_CHANGE2,textLine, "_rdCh2",FALSE)	
						ENDIF
					ENDIF
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_RADIO_STATION_DISLIKE,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//Tells the player they like the station
				CASE TAXI_DI_RADIO_LIKE
					
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_RADIO_LIKE)
						IF myTaxiData.tTaxiOJ_Radio.bHitFav
							TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE_USING_EXTRA_DLG_BITS(myTaxiData,TAXI_DXF_RADIO_LIKE,textLine, "_rdLk1", TRUE,  CONV_PRIORITY_MEDIUM)
						ENDIF
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_RADIO_LIKE2)
						
						TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE_USING_EXTRA_DLG_BITS(myTaxiData,TAXI_DXF_RADIO_LIKE2,textLine, "_rdFv1", FALSE,  CONV_PRIORITY_MEDIUM)
						
					ENDIF
					
					ADD_TAXI_OJ_TIP(TAXI_TIP_TYPE_RADIO_STATION_LIKE,myTaxiData.iTaxiOJ_CashTipToAdd,myTaxiData.iTaxiOJ_TipsBitMask,myTaxiData.sTaxiOJ_TipLine,textLine)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_RADIO_CHANGE_NEUTRAL
					textLine += "_rdneu1"
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				CASE TAXI_DI_RADIO_HINT
					textLine += "_rdtip1"
					sSpecificLine = textLine
					sSpecificLine += "_"
					sSpecificLine += myTaxiData.tTaxiOJ_Radio.iCategory
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_RADIO_HINT, sSpecificLine = ", sSpecificLine)
					
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, sSpecificLine,CONV_PRIORITY_HIGH)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_OBJ_RETURN_TO_PED
					
					PRINT_NOW("TAXI_OBJ_GYB_2", DEFAULT_GOD_TEXT_TIME, 1)
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_RETURN_TO_PED
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_RETURN_TO_PED = TAXI_OBJ_GYB_2")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//Currently only in Take It Easy
				CASE TAXI_DI_HORN_HONK
					IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
						textLine += "_horn"											
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					ENDIF
					
				BREAK
				
				//Tip Reactions-------------------------------------------------------------------------------------
				CASE TAXI_DI_RATE_TIP_AMAZING
					textLine += "_thank1"
					
					IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
					OR myTaxiData.tTaxiOJ_MissionType = TXM_07_CUTYOUIN
					OR myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					ENDIF
					
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue, TAXI_DXF_THANKS)
					
					IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					ENDIF
				BREAK
				
				CASE TAXI_DI_RATE_TIP_AVERAGE
					textLine += "_thank2"
					
					IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
					OR myTaxiData.tTaxiOJ_MissionType = TXM_07_CUTYOUIN
					OR myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					ENDIF
					
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue, TAXI_DXF_THANKS)
					
					IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					ENDIF
				BREAK
			
				CASE TAXI_DI_RATE_TIP_ASS					
					IF myTaxiData.tTaxiOJ_MissionType = TXM_05_TAKETOBEST
						textLine += "_pissed1"
					ELSE 
						textLine += "_thank3"
					ENDIF
					IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
					OR myTaxiData.tTaxiOJ_MissionType = TXM_07_CUTYOUIN
					OR myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
					//OR myTaxiData.tTaxiOJ_MissionType = TXM_05_TAKETOBEST
						TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					ENDIF
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue, TAXI_DXF_THANKS)
					
					IF myTaxiData.tTaxiOJ_MissionType != TXM_PROCEDURAL
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					ENDIF
					
				BREAK
				
				//LM - Clown Car banter in between destinations ----------------------------------------------------
				CASE TAXI_DI_CC_BANTER
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_BANTER)
						IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) > myTaxiData.fTaxiOJ_BanterDelay
							
							SWITCH myTaxiData.iTaxiOJ_GetRunModCount
								CASE 0
									textLine += "_ccba2"
								BREAK
								
								CASE 1
									textLine += "_ccba1"
								BREAK
								
								//If some have more than 2 vars send them here for now
								DEFAULT
									textLine += "_ccba1"
								BREAK
							ENDSWITCH
							
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
							
							REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
							
							SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_BANTER)
							
							TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
							TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
							
							ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
							CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_CC_BANTER - ","", textLine)
							
						ENDIF
					ENDIF
				BREAK
				
				CASE TAXI_DI_CC_BANTER2
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_BANTER2)
						IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) > myTaxiData.fTaxiOJ_BanterDelay
							
//							SWITCH myTaxiData.iTaxiOJ_GetRunModCount
//								CASE 0
//									textLine += "_ccbb2"
//								BREAK
//								
//								CASE 1
//									textLine += "_ccbb1"
//								BREAK
//								
//								//If some have more than 2 vars send them here for now
//								DEFAULT
//									textLine += "_ccbb1"
//								BREAK
//							ENDSWITCH
							
							textLine += "_ccbb1"
							
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
							REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)

							SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_BANTER2)
							
							TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
							TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
							
							ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
							CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_CC_BANTER2 - ","", textLine)
							
						ENDIF
					ENDIF
				BREAK
				
				CASE TAXI_DI_CC_CLOSE
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_CLOSE1)
						textLine += "_close1"
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_CLOSE1)
						
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_CLOSE2)
						textLine += "_close2"
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_CLOSE2)
						
					ELIF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_CLOSE3)
						textLine += "_close3"
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_CC_CLOSE3)
					ENDIF
					
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_CC_CLOSE - ","", textLine)
					
				BREAK
				
				//-----------------------------------------------------------------------------------------------
				
				
				//Get Outta Here - PLAYER kills pursuers
				CASE TAXI_DI_ENEMY_KILL
					IF myTaxiData.tTaxiOJ_MissionType = TXM_08_GOTYOUNOW
					
					
						textLine += "_kill1"
						sSpecificLine = textLine
					
						GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsGenericMission,CONST_TAXI_OJ_NUM_DX_LINES_KILLED_GYN,sSpecificLine, TRUE, TRUE, TRUE)
						
						TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
						PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,textLine, sSpecificLine,CONV_PRIORITY_HIGH)
						
					ELIF myTaxiData.tTaxiOJ_MissionType = TXM_10_FOLLOWTHATCAR
						textLine += "_cheat1"
						//textLine += "_kill1"
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					ELSE
						textLine += "_kill1"
						CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					ENDIF	
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
				BREAK
				
				CASE TAXI_DI_BUILD_HOTBOX_ONE_LINE
				
					textLine += "_wndw2"
					// hotbox buildup dialogue one liner
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
						
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_BUILD_HOTBOX_ONE_LINE - ","", textLine)
				BREAK
				
				CASE TAXI_DI_BUILD_HOTBOX
				
					textLine += "_wndw0"
					// hotbox buildup dialogue
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
						
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_BUILD_HOTBOX - ","", textLine)
				BREAK
				
				CASE TAXI_DI_INIT_HOTBOX
				
					textLine += "_wndw1"
					// crack a window dialogue
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
						
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_INIT_HOTBOX - ","", textLine)
				BREAK
				
				
				CASE TAXI_DI_CUTSCENE  // DIALOGUE DELETE?
					textLine += "_csite1"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				CASE TAXI_DI_PLAYER_WINS_FAIR_FIGHT  // DIALOGUE DELETE?
					textLine += "_fair1"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
				
				BREAK
				
				//Get Outta Here - PLAYER kills pursuers
				CASE TAXI_DI_ENEMY_EGG_ON
					textLine += "_AlCk1"
					sSpecificLine = textLine
				
					GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsEnemyBanter,CONST_TAXI_OJ_NUM_DX_LINES_EGG_ON_GYN,sSpecificLine, TRUE, TRUE, FALSE)
				
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,textLine, sSpecificLine,CONV_PRIORITY_HIGH)
		
		
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				
				BREAK
				
				CASE TAXI_DI_ENEMY_EGG_ON_2
					textLine += "_eggG1"
					sSpecificLine = textLine
				
					GET_TAXI_OJ_LINE_TO_SAY(myTaxiData.iTaxiOJ_DXBitsEnemyBanter2,CONST_TAXI_OJ_NUM_DX_LINES_EGG_ON_2_GYN,sSpecificLine, TRUE, TRUE, FALSE)
				
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,textLine, sSpecificLine,CONV_PRIORITY_HIGH)
					
		
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
				BREAK
				
				CASE TAXI_DI_POST_CUTSCENE
					
					textLine += "_goons1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				//Get Outta Here - PLAYER  outruns pursuers
				CASE TAXI_DI_ENEMY_EVADED  // DIALOGUE DELETE?
					textLine += "_oRun1"
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
				
				BREAK
				
				CASE TAXI_DI_ENEMY_FIRES
					textLine += "_gotG1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
										
				BREAK
				
				CASE TAXI_DI_PLAYER_RUNS
					textLine += "_getA1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				CASE TAXI_DI_PUT_GUN_AWAY  // DIALOGUE DELETE?
					textLine += "_gnawy1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
										
				BREAK
				
				// dialogue of player noticing the passenger's girlfriend
				CASE TAXI_DI_GIRL_NOTICE
					textLine += "_grl1"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
										
				BREAK
				
				// dialogue of player noticing the passenger's girlfriend
				CASE TAXI_DI_START_FIGHT
					textLine += "_figt1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
										
				BREAK
				
				CASE TAXI_DI_GIRL_EGGON_PLAYER_NO_HIT
					textLine += "_gEgg1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				CASE TAXI_DI_GIRL_EGGON_BF
					textLine += "_gEgg3"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				CASE TAXI_DI_GIRL_EGGON_PLAYER_WINNING
					textLine += "_gEgg2"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				CASE TAXI_DI_GIRL_LEAVE
					textLine += "_gLeav1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				CASE TAXI_DI_GIRL_BF_KILLED
					textLine += "_aKill1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				
				CASE TAXI_DI_GIRL_HELP
					textLine += "_gHelp1"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
										
				BREAK
				
				CASE TAXI_DI_GIRL_DEST
					textLine += "_gDest1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GYN,TRUE)	
				BREAK
				
				CASE TAXI_DI_GIRL_KOD
					textLine += "_gKO1"
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				CASE TAXI_DI_GIRL_THANKS
					
					textLine += "_gThank1"
					
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue, TAXI_DXF_THANKS)
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				CASE TAXI_DI_GIRL_BANTER
				
					textLine += "_gDriv1"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					
					REGISTER_NEXT_LINE_IN_QUEUE(textLine,sDialogueQ)
					
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_BANTER)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] - TAXI_DI_BANTER - ","", textLine)
					
					//CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				//Picked up some MONEY-----------------------------------------
				CASE TAXI_DI_PICKUP_MONEY  // DIALOGUE DELETE?
				
					textLine += "_cash1"

					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
				
				BREAK
				//Passenger runs out on driver
				CASE TAXI_DI_RUNOFF
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_RUNOFF)
					AND myTaxiData.tTaxiOJ_MissionType <> TXM_PROCEDURAL
						TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_RUNOFF,textLine, "_noPay1")
					ELIF myTaxiData.tTaxiOJ_MissionType = TXM_PROCEDURAL
						IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "TAXI_NO_PAY", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
						ENDIF
						//textLine += "_noPay1"
						//CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData, textLine, TRUE)
					ENDIF
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_RUNOFF,TRUE)
				BREAK
				
				CASE TAXI_DI_RUNOFF_SHOUTS
					
					textLine += "_foot"
					
					CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_VERY_HIGH)
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					
				BREAK
				
				CASE TAXI_DI_KILLED_THIEF
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_KILLEDTHIEF)
						TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_KILLEDTHIEF,textLine, "_kPay1")
					ENDIF							
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_POLICE,TRUE)	
				BREAK
				
				//Passenger runs out on driver
				CASE TAXI_OBJ_RUNOFF

					PRINT_NOW("TAXI_OBJ_RUNOUT", DEFAULT_GOD_TEXT_TIME, 1)		
		
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_RUNOFF
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_RUNOFF = TAXI_OBJ_RUNOUT")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				//Passenger pleads for mercy as player has caught up to hit.
				CASE TAXI_DI_MAKE_PAY
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_MAKEPAY)
						IF myTaxiData.tTaxiOJ_MissionType = TXM_03_DEADLINE
						OR myTaxiData.tTaxiOJ_MissionType = TXM_08_GOTYOUNOW
							TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_MAKEPAY,textLine, "_mPay1", FALSE)
						ELIF myTaxiData.tTaxiOJ_MissionType = TXM_PROCEDURAL
							IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "APOLOGY_NO_TROUBLE", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
							ENDIF
							//textLine += "_mPay1"
							//CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData, textLine, TRUE)
						ELSE 
							TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_MAKEPAY,textLine, "_mPay1")
						ENDIF	
					ENDIF							
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GET_RUNAWAY_MONEY,TRUE)
				BREAK
				
				//Passenger pleads for mercy as player has caught up to hit.
				CASE TAXI_OBJ_GET_RUNAWAY_MONEY
				
					PRINT_NOW("TAXI_OBJ_GETRUN", DEFAULT_GOD_TEXT_TIME, 1)		
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_GET_RUNAWAY_MONEY
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_GET_RUNAWAY_MONEY = TAXI_OBJ_GETRUN")
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_MAKE_PAY_END
					textLine += "_runoff"
					
					CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, textLine, CONV_PRIORITY_VERY_HIGH)
					TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE)
					
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_RETURN
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_RETURN)
						TAXI_PLAY_CONVERSATION_WITH_PLAYER_RESPONSE(myTaxiData,TAXI_DXF_RETURN,textLine, "_ret1")
					ENDIF							
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_FLEE
					PRINT_NOW("TAXI_TIEFLEE", DEFAULT_GOD_TEXT_TIME, 1)						
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_DI_FLEE = TAXI_TIEFLEE")
				BREAK
				
				CASE TAXI_OBJ_CYI_RETURN_WAIT
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_Bits_Gates,TAXI_GATE_CYI_RETURN_RENDEVOUS)
						PRINT_NOW("TAXI_OBJ_CYI_1B", DEFAULT_GOD_TEXT_TIME,1)
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_Bits_Gates,TAXI_GATE_CYI_RETURN_RENDEVOUS)
						CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_CYI_RETURN_WAIT = TAXI_OBJ_CYI_1B")
				
					ENDIF
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_CYI_RETURN_WAIT	
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_JOKE
					textLine += "_joke1"
					
					TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(textLine)
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine)
					
				BREAK
				
				CASE TAXI_DI_WRONG_WAY
					
					IF myTaxiData.bSecondPassenger
						textline += "_wronglz"
					ELIF myTaxiData.bSecondLeg
						textline += "_wrong2"
					ELSE
						textline += "_wrong"
					ENDIF
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine, FALSE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_SHOOT
					
					textline += "_shout"
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine, FALSE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_JACKED_CAR
					IF myTaxiData.tTaxiOJ_MissionType = TXM_05_TAKETOBEST
						textline = "tm6_jack"
					ELSE
						textline += "_jack"
					ENDIF
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine, FALSE)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
				
				CASE TAXI_DI_GET_CAR
					textline += "_getCar"
					
					CREATE_CONVERSATION_WITH_OJ_TAXI_OUTPUT(myTaxiData,textLine, TRUE)
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GET_CAR,TRUE)
				BREAK
				
				CASE TAXI_OBJ_GET_CAR
					PRINT_NOW("TX_VIP",DEFAULT_GOD_TEXT_TIME,0)
					CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB]  - TAXI_OBJ_GET_CAR printing")
					
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_GET_CAR	
					
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DISPATCH_DIRECTIONS)
					ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				BREAK
			ENDSWITCH

		ENDIF
	ENDIF
	
	
ENDPROC



/*

     _   ____                       
  __| | /___ \_   _  ___ _   _  ___ 
 / _` |//  / / | | |/ _ \ | | |/ _ \
| (_| / \_/ /| |_| |  __/ |_| |  __/
 \__,_\___,_\ \__,_|\___|\__,_|\___|
                                    
*/
/// PURPOSE:Call this every frame to make sure all important dialogue plays
///    
/// PARAMS:
///    myTaxiData - global taxi info
///    lData - 		global locates data
///    sImportantDX - array of all the important dialogue ID lines that this will process
///    tTaxiDQ_data - look up all the info in Taxi_Dialogue.sch
///    iNumInterruptLines- number of different resume dialogue lines (USUALLY A CONST_INT)
///    bDebug	- turn this on to get useful log debug messages 
/// RETURNS:
///    TRUE when the important line is finished playing
FUNC BOOL PROCESS_IMPORTANT_DIALOGUE_Q(TaxiStruct &myTaxiData, 
		TAXI_OJ_DQ_CONVERSATION_LINE &sImportantDX[],TAXI_OJ_DIALOGUE_Q_DATA &tTaxiDQ_data, /*INT iNumInterruptLines,*/BOOL bDebug = FALSE)
		
	//Dialogue Manager 
	TAXI_DIALOGUE_OBJ_MANAGER(myTaxiData, sImportantDX)
	
	IF tTaxiDQ_data.tDialogueQStates = TDQ_STOPPED
	AND myTaxiData.tTaxiOJ_RideState < TRS_SCORECARD_GRADE	//don't worry about getting interupted when you're already getting a scorecard. JD 
	AND NOT myTaxiData.bTaxiOJ_Failed
	AND NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_STOPPED)
		
		IF DOES_DIALOGUE_QUEUE_HAVE_VALID_LINE(sImportantDX)
			CDEBUG1LN(DEBUG_OJ_TAXI,"HIT 4")
			tTaxiDQ_data.tDialogueQStates = TDQ_PLAY_LINE
			IF bDebug
				SCRIPT_ASSERT("Queue Open")
			ENDIF
		ELSE
			IF (GET_GAME_TIMER() % 2000) < 50
				CDEBUG1LN(DEBUG_OJ_TAXI,"HIT 6")
			ENDIF
		ENDIF
//	ELSE 	//if dialogue isn't playing, uncomment this to see possible reasons why.
//		IF (GET_GAME_TIMER() % 2000) < 50
//			CDEBUG1LN(DEBUG_OJ_TAXI,"HIT 5")
//			IF tTaxiDQ_data.tDialogueQStates = TDQ_STOPPED
//				CDEBUG1LN(DEBUG_OJ_TAXI,"tTaxiDQ_data.tDialogueQStates = TDQ_STOPPED")
//			ENDIF
//			IF myTaxiData.tTaxiOJ_RideState < TRS_SCORECARD_GRADE
//				CDEBUG1LN(DEBUG_OJ_TAXI,"myTaxiData.tTaxiOJ_RideState < TRS_SCORECARD_GRADE")
//			ENDIF
//			IF NOT myTaxiData.bTaxiOJ_Failed
//				CDEBUG1LN(DEBUG_OJ_TAXI,"NOT myTaxiData.bTaxiOJ_Failed")
//			ENDIF
//			IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_STOPPED)
//				CDEBUG1LN(DEBUG_OJ_TAXI,"NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_STOPPED)")
//			ENDIF
//		ENDIF
	
	ENDIF
	IF NOT myTaxiData.bTaxiOJ_Failed
		SWITCH tTaxiDQ_data.tDialogueQStates
			//Trigger line to play
			CASE TDQ_PLAY_LINE
				IF NOT IS_VIP_TAXI_OBJ_BEING_DISPLAYED(myTaxiData)	
					tTaxiDQ_data.sTexLabelCurrentlyPlaying = FIND_NEXT_VALID_LINE_IN_DIALOGUE_QUEUE(sImportantDX)	//this line is a string that already comes in with the current data
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"sTexLabelCurrentlyPlaying = ", tTaxiDQ_data.sTexLabelCurrentlyPlaying)
					
					CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID,tTaxiDQ_data.sTexLabelCurrentlyPlaying,CONV_PRIORITY_HIGH)
					
					tTaxiDQ_data.tDialogueQStates = TDQ_CONFIRM_LINE_IS_PLAYING
					IF bDebug
						SCRIPT_ASSERT("Taxi DQ - Queuing Line")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"Dialogue is playing/queued--- Waiting til that clears")
				ENDIF
			BREAK
			
			//Check that line is playing, if not go back
			CASE TDQ_CONFIRM_LINE_IS_PLAYING
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					tTaxiDQ_data.sCurrentConvoPlaying = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				
					IF ARE_STRINGS_EQUAL(tTaxiDQ_data.sCurrentConvoPlaying,tTaxiDQ_data.sTexLabelCurrentlyPlaying)
					
						//tTaxiDQ_data.tDialogueQStates = TDQ_CLOSED//TDQ_WAIT_FOR_INTERRUPTION
						IF bDebug
							SCRIPT_ASSERT("Taxi DQ - Line is verified")
						ENDIF
					//Line didn't trigger correctly go back and retrigger
					ELSE
						//tTaxiDQ_data.tDialogueQStates = TDQ_PLAY_LINE
						//Clear our string
						CDEBUG1LN(DEBUG_OJ_TAXI,"Conversation = ", "", tTaxiDQ_data.sCurrentConvoPlaying, "", " and ", "", tTaxiDQ_data.sTexLabelCurrentlyPlaying)
					ENDIF
					
				ELSE
					//Conversation is finished, update to next line,set the bitmask for it, and close Queue til we need to reopen it
					SET_BITMASK(tTaxiDQ_data.iDialogueQBITS,ROUND(POW(2,TO_FLOAT(tTaxiDQ_data.iCurrentDQLine))))
					
					//advance the queue
					tTaxiDQ_data.iCurrentDQLine++
					CDEBUG1LN(DEBUG_OJ_TAXI,"[********************DIALOGUE Q************************] Line Num = ", tTaxiDQ_data.iCurrentDQLine , " Conversation has finished.")
					SET_CURRENT_LINE_PLAYING_IN_QUEUE_FINISHED(sImportantDX)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DQUEUE)
					tTaxiDQ_data.tDialogueQStates = TDQ_STOPPED
					
					IF bDebug
						SCRIPT_ASSERT("Taxi DQ - Conversation has finished")
					ENDIF
					RETURN TRUE
				ENDIF
			BREAK
			
			//Line is playing - check for/process interuption
			CASE TDQ_WAIT_FOR_INTERRUPTION
				//Has something from the dialogue manager attempted to speak
				IF IS_TAXI_SPEECH_ENABLED(myTaxiData)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
						
//						//If player was talking we know he was interrupted, than we can queue an "I'm sorry I interrupted you line"
//						IF IS_ANY_SPEECH_PLAYING(PLAYER_PED_ID())
//							tTaxiDQ_data.bPlayerWasInterupted = TRUE
//							IF bDebug
//								SCRIPT_ASSERT("Taxi DQ - Line has been interupted")
//							ENDIF
//						//Otherwise passenger interrupted himself
//						ELSE
//							tTaxiDQ_data.bPlayerWasInterupted = FALSE
//							IF bDebug
//								SCRIPT_ASSERT("Taxi DQ - Passenger interrupted himself")
//							ENDIF
//						ENDIF
						
						TAXI_RESET_TIMERS(myTaxiData,TT_INTERUPT,0,TRUE)
						
						tTaxiDQ_data.sResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						
						//If wanted we're going to close the queue, TAXI_OJ_MONITOR_WANTED_LEVEL handles reopening it again.
						IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) >= 1
							tTaxiDQ_data.tDialogueQStates = TDQ_CLOSED
						ELSE
							
							tTaxiDQ_data.tDialogueQStates = TDQ_ADD_DELAY_BEFORE_RESUME
						ENDIF
						
					ENDIF
				//Make sure entire conversation has played out
				ELSE
					
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//Grab the currently playing line to later compare against the resumeLabel to see if it's the last line of the conversation or not.
						tTaxiDQ_data.sLabelNowPlaying =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
					ELSE
						//Conversation is finished, update to next line,set the bitmask for it, and close Queue til we need to reopen it
						SET_BITMASK(tTaxiDQ_data.iDialogueQBITS,ROUND(POW(2,TO_FLOAT(tTaxiDQ_data.iCurrentDQLine))))
						
						//advance the queue
						tTaxiDQ_data.iCurrentDQLine++
						CDEBUG1LN(DEBUG_OJ_TAXI,"[********************DIALOGUE Q************************] Line Num = ", tTaxiDQ_data.iCurrentDQLine , " Conversation has finished.")
						SET_CURRENT_LINE_PLAYING_IN_QUEUE_FINISHED(sImportantDX)
						
						TAXI_RESET_TIMERS(myTaxiData, TT_DQUEUE)
						tTaxiDQ_data.tDialogueQStates = TDQ_STOPPED
						
						IF bDebug
							SCRIPT_ASSERT("Taxi DQ - Conversation has finished")
						ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			
			//Conversation was interupted, trigger timer 
			CASE TDQ_ADD_DELAY_BEFORE_RESUME
				IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() //AND NO OTHER EXCEPTIONS
				AND NOT IS_TAXI_SPEECH_ENABLED(myTaxiData)
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 1.0
					//UNUSED_PARAMETER(iNumInterruptLines)
					TAXI_RESET_TIMERS(myTaxiData, TT_DQUEUE)
					tTaxiDQ_data.tDialogueQStates = TDQ_RESUME_CONVERSATION
					
					IF bDebug
						SCRIPT_ASSERT("Taxi DQ - Starting delay timer")
					ENDIF
				ENDIF
				
			BREAK
			//Play reminder line after delay- "What were we talking about?" 
			//When the System is in this state, make sure that no other dialogue can trigger
//			CASE TDQ_RESUME_FROM_INTERUPTION
//				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > TAXIOJ_DIALOGUE_Q_DELAY_BEFORE_RESUME_LINE
//				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				AND NOT IS_TAXI_SPEECH_ENABLED(myTaxiData)	
//		
//					TEXT_LABEL_23 sResumeLine
//					TEXT_LABEL_23 sResumeRoot
//					sResumeLine = myTaxiData.sTaxiOJ_DXMissionID
//					
//					IF DOES_TAXI_LINE_HAVE_SPECIAL_RESUME(tTaxiDQ_data, sResumeLine)
//						
//						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, sResumeLine, CONV_PRIORITY_HIGH)
//						
//						TAXI_RESET_TIMERS(myTaxiData, TT_DQUEUE)
//
//						tTaxiDQ_data.tDialogueQStates = TDQ_SPECIAL_CASE_FOR_LAST_LINE_IN_CONVERSATION_INTERRUPTION
//					ELSE
//						IF myTaxiData.bSecondPassenger
//							IF tTaxiDQ_data.bPlayerWasInterupted
//								sResumeLine +="_resB2"
//							ELSE
//								sResumeLine +="_resA2"
//							ENDIF
//						ELSE
//							IF tTaxiDQ_data.bPlayerWasInterupted
//								sResumeLine +="_resB"
//							ELSE
//								sResumeLine +="_resA"
//							ENDIF
//						ENDIF
//						
//						sResumeRoot = sResumeLine
//						
//						GET_TAXI_OJ_LINE_TO_SAY(tTaxiDQ_data.iResumeDXBitMask,iNumInterruptLines,sResumeLine, TRUE, TRUE, FALSE)	
//						PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,sResumeRoot, sResumeLine,CONV_PRIORITY_HIGH)
//					
//						TAXI_RESET_TIMERS(myTaxiData, TT_DQUEUE)
//						
//						tTaxiDQ_data.tDialogueQStates = TDQ_RESUME_CONVERSATION
//					ENDIF
//				ENDIF
//			BREAK
			
			//Conversation is now ready to resume from where it left off.
			CASE TDQ_RESUME_CONVERSATION
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 1.0
//						//If these 2 strings are equal , this means you are in the last line of a conversation, and to prevent the line from repeating we handle it specially here.
//						IF ARE_STRINGS_EQUAL(tTaxiDQ_data.sLabelNowPlaying, tTaxiDQ_data.sResumeLabel)
//							
//							tTaxiDQ_data.sResumeLabel = myTaxiData.sTaxiOJ_DXMissionID
//							tTaxiDQ_data.sResumeLabel += "_end1"
//							
//							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(tTaxiDQ_data.sResumeLabel)
//							CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, tTaxiDQ_data.sResumeLabel, CONV_PRIORITY_HIGH)
//							
//							//Kill the timer we use to track whether we've been interupted.
//							TAXI_CANCEL_TIMERS(myTaxiData,TT_INTERUPT,TRUE)
//							
//							CDEBUG1LN(DEBUG_OJ_TAXI,"")
//							tTaxiDQ_data.tDialogueQStates = TDQ_SPECIAL_CASE_FOR_LAST_LINE_IN_CONVERSATION_INTERRUPTION
//						ELSE	
							IF NOT IS_STRING_NULL_OR_EMPTY(tTaxiDQ_data.sResumeLabel)
								CREATE_CONVERSATION_FROM_SPECIFIC_LINE(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, tTaxiDQ_data.sCurrentConvoPlaying, tTaxiDQ_data.sResumeLabel, CONV_PRIORITY_HIGH)
								
								IF bDebug
									SCRIPT_ASSERT("Taxi DQ - Conversation resuming")
								ENDIF
							
							ENDIF
//						ENDIF
						//2/2/12 - Moving this out here. In case the resume label is NULL, this will allow it to continue forward and check if the conversation is done and fall thru correctly.
						tTaxiDQ_data.tDialogueQStates = TDQ_WAIT_FOR_INTERRUPTION
					ENDIF
				ENDIF
			BREAK
			
			//This is a special case that only plays when you've interrupted a conversation at the last line of it.
			//It just waits for the conversation to be over and moves the queue forward
			CASE TDQ_SPECIAL_CASE_FOR_LAST_LINE_IN_CONVERSATION_INTERRUPTION
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//Conversation is finished, update to next line,set the bitmask for it, and close Queue til we need to reopen it
					SET_BITMASK(tTaxiDQ_data.iDialogueQBITS,ROUND(POW(2,TO_FLOAT(tTaxiDQ_data.iCurrentDQLine))))
					
					//advance the queue
					tTaxiDQ_data.iCurrentDQLine++
					CDEBUG1LN(DEBUG_OJ_TAXI,"[********************DIALOGUE Q************************] Line Num = ", tTaxiDQ_data.iCurrentDQLine , " Conversation has finished with special end case.")
					SET_CURRENT_LINE_PLAYING_IN_QUEUE_FINISHED(sImportantDX)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DQUEUE)
					tTaxiDQ_data.tDialogueQStates = TDQ_STOPPED
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    manual interruption process for NE and TIE
/// PARAMS:
///    myTaxiData - 
///    sImportantDX - 
///    tTaxiDQ_data - 
///    iNumInterruptLines - 
///    bDebug - 
/// RETURNS:
///    
FUNC BOOL PROCESS_MANUAL_DIALOGUE_Q(TaxiStruct &myTaxiData, TAXI_OJ_DQ_CONVERSATION_LINE &sImportantDX[],TAXI_OJ_DIALOGUE_Q_DATA &tTaxiDQ_data, INT iNumInterruptLines,BOOL bDebug = FALSE)
		
	//Dialogue Manager 
	TAXI_DIALOGUE_OBJ_MANAGER(myTaxiData, sImportantDX)
	
	IF tTaxiDQ_data.tDialogueQStates = TDQ_STOPPED
	AND myTaxiData.tTaxiOJ_RideState < TRS_SCORECARD_GRADE	//don't worry about getting interupted when you're already getting a scorecard. JD 
	AND NOT myTaxiData.bTaxiOJ_Failed
		IF DOES_DIALOGUE_QUEUE_HAVE_VALID_LINE(sImportantDX)
			tTaxiDQ_data.tDialogueQStates = TDQ_PLAY_LINE
			IF bDebug
				SCRIPT_ASSERT("Queue Open")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT myTaxiData.bTaxiOJ_Failed
		SWITCH tTaxiDQ_data.tDialogueQStates
			//Trigger line to play
			CASE TDQ_PLAY_LINE
				IF NOT IS_VIP_TAXI_OBJ_BEING_DISPLAYED(myTaxiData)
					tTaxiDQ_data.sTexLabelCurrentlyPlaying = FIND_NEXT_VALID_LINE_IN_DIALOGUE_QUEUE(sImportantDX)	//this line is a string that already comes in with the current data
					
					CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo,myTaxiData.sTaxiOJ_DXSubtitleGroupID,tTaxiDQ_data.sTexLabelCurrentlyPlaying,CONV_PRIORITY_HIGH)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - playing line ", tTaxiDQ_data.sTexLabelCurrentlyPlaying )
					
					tTaxiDQ_data.tDialogueQStates = TDQ_CONFIRM_LINE_IS_PLAYING
					IF bDebug
						SCRIPT_ASSERT("Taxi DQ - Queuing Line")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"Dialogue is playing/queued--- Waiting til that clears")
				ENDIF
			BREAK
			
			//Check that line is playing, if not go back
			CASE TDQ_CONFIRM_LINE_IS_PLAYING
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					tTaxiDQ_data.sCurrentConvoPlaying = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				
					IF ARE_STRINGS_EQUAL(tTaxiDQ_data.sCurrentConvoPlaying,tTaxiDQ_data.sTexLabelCurrentlyPlaying)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - strings are equal, going to wait for interruption ")
						
						tTaxiDQ_data.tDialogueQStates = TDQ_WAIT_FOR_INTERRUPTION
						IF bDebug
							SCRIPT_ASSERT("Taxi DQ - Line is verified")
						ENDIF
					//Line didn't trigger correctly go back and retrigger
					ELSE
						tTaxiDQ_data.tDialogueQStates = TDQ_PLAY_LINE
						//Clear our string
						CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - Conversation = ", "", tTaxiDQ_data.sCurrentConvoPlaying, "", " and ", "", tTaxiDQ_data.sTexLabelCurrentlyPlaying)
					ENDIF
					
				ENDIF
			BREAK
			
			//Line is playing - check for/process interuption
			CASE TDQ_WAIT_FOR_INTERRUPTION
				//Has something from the dialogue manager attempted to speak
				IF IS_TAXI_SPEECH_ENABLED(myTaxiData)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
						
						//If player was talking we know he was interrupted, than we can queue an "I'm sorry I interrupted you line"
						IF IS_ANY_SPEECH_PLAYING(PLAYER_PED_ID())
							tTaxiDQ_data.bPlayerWasInterupted = TRUE
							CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - player was interrupted")
							
						//Otherwise passenger interrupted himself
						ELSE
							tTaxiDQ_data.bPlayerWasInterupted = FALSE
							CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - passenger was interrupted")
						ENDIF
						
						TAXI_RESET_TIMERS(myTaxiData,TT_INTERUPT,0,TRUE)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - interruption happened, grabbing a resume label ")
						
						IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
							tTaxiDQ_data.sResumeLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ELSE
							tTaxiDQ_data.sResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
							KILL_ANY_CONVERSATION()
						ENDIF
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - resume label = ", tTaxiDQ_data.sResumeLabel)
						
						//If wanted we're going to close the queue, TAXI_OJ_MONITOR_WANTED_LEVEL handles reopening it again.
						IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) >= 1
							tTaxiDQ_data.tDialogueQStates = TDQ_CLOSED
						ELSE
							IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
								tTaxiDQ_data.tDialogueQStates = TDQ_ADD_DELAY_BEFORE_RESUME
							ELSE
								tTaxiDQ_data.tDialogueQStates = TDQ_RESUME_CONVERSATION
							ENDIF
						ENDIF
						
					ENDIF
				//Make sure entire conversation has played out
				ELSE
					
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//Grab the currently playing line to later compare against the resumeLabel to see if it's the last line of the conversation or not.
						tTaxiDQ_data.sLabelNowPlaying =  GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() //GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
					ELSE
						//Conversation is finished, update to next line,set the bitmask for it, and close Queue til we need to reopen it
						SET_BITMASK(tTaxiDQ_data.iDialogueQBITS,ROUND(POW(2,TO_FLOAT(tTaxiDQ_data.iCurrentDQLine))))
						
						//advance the queue
						tTaxiDQ_data.iCurrentDQLine++
						CDEBUG1LN(DEBUG_OJ_TAXI,"[{{{{{{{{manual dialogue q - ] Line Num = ", tTaxiDQ_data.iCurrentDQLine , " Conversation has finished.")
						SET_CURRENT_LINE_PLAYING_IN_QUEUE_FINISHED(sImportantDX)
						
						TAXI_RESET_TIMERS(myTaxiData, TT_DQUEUE)
						tTaxiDQ_data.tDialogueQStates = TDQ_STOPPED
						
						IF bDebug
							SCRIPT_ASSERT("Taxi DQ - Conversation has finished")
						ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			
			//Conversation was interupted, trigger timer 
			CASE TDQ_ADD_DELAY_BEFORE_RESUME
				IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() //AND NO OTHER EXCEPTIONS
				AND NOT IS_TAXI_SPEECH_ENABLED(myTaxiData)
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 1.0

					TAXI_RESET_TIMERS(myTaxiData, TT_DQUEUE)
					tTaxiDQ_data.tDialogueQStates = TDQ_RESUME_FROM_INTERUPTION
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - going to resume from interruption")
					
					IF bDebug
						SCRIPT_ASSERT("Taxi DQ - Starting delay timer")
					ENDIF
				ENDIF
				
			BREAK
			//Play reminder line after delay- "What were we talking about?" 
			//When the System is in this state, make sure that no other dialogue can trigger
			CASE TDQ_RESUME_FROM_INTERUPTION
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > TAXIOJ_DIALOGUE_Q_DELAY_BEFORE_RESUME_LINE
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_TAXI_SPEECH_ENABLED(myTaxiData)	
		
					TEXT_LABEL_23 sResumeLine
					TEXT_LABEL_23 sResumeRoot
					sResumeLine = myTaxiData.sTaxiOJ_DXMissionID
					
					IF DOES_TAXI_LINE_HAVE_SPECIAL_RESUME(tTaxiDQ_data, sResumeLine)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - playing a special resume line, this shouldn't be happening")
						
						CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, sResumeLine, CONV_PRIORITY_HIGH)
						
						TAXI_RESET_TIMERS(myTaxiData, TT_DQUEUE)

						tTaxiDQ_data.tDialogueQStates = TDQ_SPECIAL_CASE_FOR_LAST_LINE_IN_CONVERSATION_INTERRUPTION
					ELSE
						IF tTaxiDQ_data.bPlayerWasInterupted
							sResumeLine +="_resB"
						ELSE
							sResumeLine +="_resA"
						ENDIF
						
						sResumeRoot = sResumeLine
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - playing a normal resume line")
						
						GET_TAXI_OJ_LINE_TO_SAY(tTaxiDQ_data.iResumeDXBitMask,iNumInterruptLines,sResumeLine, TRUE, TRUE, FALSE)	
						PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID,sResumeRoot, sResumeLine,CONV_PRIORITY_HIGH)
					
						TAXI_RESET_TIMERS(myTaxiData, TT_DQUEUE)
						
						tTaxiDQ_data.tDialogueQStates = TDQ_RESUME_CONVERSATION
					ENDIF
				ENDIF
			BREAK
			
			//Conversation is now ready to resume from where it left off.
			CASE TDQ_RESUME_CONVERSATION
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_INTERUPT) > 1.0
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 0.5
					OR myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
						//If these 2 strings are equal , this means you are in the last line of a conversation, and to prevent the line from repeating we handle it specially here.
						IF ARE_STRINGS_EQUAL(tTaxiDQ_data.sLabelNowPlaying, tTaxiDQ_data.sResumeLabel)
						AND myTaxiData.tTaxiOJ_MissionType != TXM_01_NEEDEXCITEMENT
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - ending the conversation after resuming")
							
							tTaxiDQ_data.sResumeLabel = myTaxiData.sTaxiOJ_DXMissionID
							tTaxiDQ_data.sResumeLabel += "_end1"
							
							TAXI_OJ_DX_PLAY_CONVO_FOR_SPECIFIC_PLAYER(tTaxiDQ_data.sResumeLabel)
							CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, tTaxiDQ_data.sResumeLabel, CONV_PRIORITY_HIGH)
							
							//Kill the timer we use to track whether we've been interupted.
							TAXI_CANCEL_TIMERS(myTaxiData,TT_INTERUPT,TRUE)
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"")
							tTaxiDQ_data.tDialogueQStates = TDQ_SPECIAL_CASE_FOR_LAST_LINE_IN_CONVERSATION_INTERRUPTION
						ELSE	
							IF NOT IS_STRING_NULL_OR_EMPTY(tTaxiDQ_data.sResumeLabel)
								CREATE_CONVERSATION_FROM_SPECIFIC_LINE(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, tTaxiDQ_data.sCurrentConvoPlaying, tTaxiDQ_data.sResumeLabel, CONV_PRIORITY_HIGH)
								
								CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - resuming the normal conversation")
								
								IF bDebug
									SCRIPT_ASSERT("Taxi DQ - Conversation resuming")
								ENDIF
							
							ENDIF
						ENDIF
						//2/2/12 - Moving this out here. In case the resume label is NULL, this will allow it to continue forward and check if the conversation is done and fall thru correctly.
						tTaxiDQ_data.tDialogueQStates = TDQ_WAIT_FOR_INTERRUPTION
						CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - going back to wait for interruptions")
					ENDIF
				ENDIF
			BREAK
			
			//This is a special case that only plays when you've interrupted a conversation at the last line of it.
			//It just waits for the conversation to be over and moves the queue forward
			CASE TDQ_SPECIAL_CASE_FOR_LAST_LINE_IN_CONVERSATION_INTERRUPTION
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//Conversation is finished, update to next line,set the bitmask for it, and close Queue til we need to reopen it
					SET_BITMASK(tTaxiDQ_data.iDialogueQBITS,ROUND(POW(2,TO_FLOAT(tTaxiDQ_data.iCurrentDQLine))))
					
					//advance the queue
					tTaxiDQ_data.iCurrentDQLine++
					CDEBUG1LN(DEBUG_OJ_TAXI,"{{{{{{{{manual dialogue q - Line Num = ", tTaxiDQ_data.iCurrentDQLine , " Conversation has finished with special end case.")
					SET_CURRENT_LINE_PLAYING_IN_QUEUE_FINISHED(sImportantDX)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DQUEUE)
					tTaxiDQ_data.tDialogueQStates = TDQ_STOPPED
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(TaxiStruct &myTaxiData)
	
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		RETURN TRUE
	
	ENDIF
	
	//Is interrupt timer running - signifies Interuption is currently playing
	IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_INTERUPT)
		RETURN TRUE
	ENDIF
	
	//Is player in the cab - signifies passenger is yelling at him to return
	IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
		RETURN TRUE
	ENDIF
		
	IF IS_TAXI_SPEECH_ENABLED(myTaxiData)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BANTER_SAFE_TO_PLAY(TaxiStruct &myTaxiData, TAXI_OJ_DIALOGUE_Q_DATA &tTaxiDQ_data)
	RETURN ( tTaxiDQ_data.tDialogueQStates = TDQ_STOPPED
			AND myTaxiData.tWantedStateIndex = TWS_CHECK_IF_WANTED
			AND NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_POLICE) )
ENDFUNC

PROC SET_TAXI_OJ_INTERRUPT_TIMER_OFF(TaxiStruct &myTaxiData)
	IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_INTERUPT)
		IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
			IF NOT IS_TAXI_SPEECH_ENABLED(myTaxiData)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TAXI_CANCEL_TIMERS(myTaxiData, TT_INTERUPT,TRUE)
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
//EOF
