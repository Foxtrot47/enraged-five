
//=======================================================================================================================================
// Taxi_FollowCar.sc
// Dev : John R. Diaz
/*
	•	“Follow that car!” This is a high speed pursuit of a fleeing automobile. Pretenses range from a cop chasing down a suspect to 
	a woman chasing a purse snatcher. As long as you pursue the target and never shoot/kill a pedestrian, these passengers will never 
	bail. Eventually, the target car stops and the driver bails. If you stop nearby, the passenger tosses you the fare when they get out.
*/



//CHANGELOG==========================================================================================================
//Seen Al do this, so I think I'll be doing the same
//
//4/11/11 - Added a new state for the player to earn a wanted level if he kills the driver of the car he's following.
//9/27/11 - Damn, so much for keeping a changelog. Alright trying this again. Today I'm reformatting/pretItifying my scripts
//===================================================================================================================



//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.



//Includes------------------------------------------------------
USING "Taxi_Includes.sch"

USING "Taxi_AI_Car_lib.sch"

//Consts-------------------------------------------------------
CONST_FLOAT 					TAXI_CONST_CHASEE_CRUISE_SPEED	35.0
CONST_FLOAT						TAXI_OJ_CONST_DIST_TO_SEE_FC_CLUB	150.0
CONST_INT						TAXI_CONST_CASH_BONUS_PRIVATE_EYE	100
CONST_FLOAT						TAXI_CONST_FC_ROAD_DISABLE_RADIUS	50.0
CONST_FLOAT						TAXI_CONST_FC_WAIT_TIME_BEFORE_AVOIDING_CARS 120.0
CONST_INT						TAXI_CONST_VR_FIANCE_EXIT_HOSPITAL	120
CONST_FLOAT						TAXI_CONST_FC_ROAD_NODES_RADIUS		15.0
//CONST_INT						TAXI_FC_CHASE_CAM_HLP		0

//Local Variables-----------------------------------------------

//Taxi Bonus Data-------------------------------------------------------------------
ENUM TAXIOJ_FC_BONUS
	TFC_BONUS_P_EYE = 0,
	TFC_BONUS_TOTAL				
ENDENUM
BONUS_FIELD						bonusFieldFollowCar[TFC_BONUS_TOTAL]

ENUM TAXIOJ_FC_DROPZONE
	TFC_DROPZONE1,
	TFC_DROPZONE2
ENDENUM
TAXIOJ_FC_DROPZONE				eFCDropZone

//Native Data
SEQUENCE_INDEX 					siEscapeeSequenceID
SEQUENCE_INDEX 					siWhoreSequenceID
SEQUENCE_INDEX 					siPassengerSequenceID
//SEQUENCE_INDEX					wayPtSeq

VEHICLE_INDEX					viHospitalCars[3]
VEHICLE_INDEX 					viAmbulance

CAMERA_INDEX					ciTransCam
//Custom Data
TaxiStruct 						myTaxiData

escapeStruct 					myEscapeeStruct

MODEL_NAMES 					mPassengerModel = A_F_Y_GENHOT_01 // A_F_Y_FITNESS_02
MODEL_NAMES 					mWhoreModel = A_F_Y_BEVHILLS_01
MODEL_NAMES						mHospitalCar1 = AMBULANCE
MODEL_NAMES						mHospitalCar2 = bison //pyxis Changed to fix build error

PED_INDEX						piWhore
PED_INDEX 						piMedic

REL_GROUP_HASH					rghPassenger
REL_GROUP_HASH					rghWhore

SCENARIO_BLOCKING_INDEX			tempScenarioBlockingIndex

INCIDENT_INDEX 					nurseIncident

//Ints
INT 							iDebugThrottle = 1
INT 							iAllowSkipCutsceneTimeLocal = 1000
INT								iTipIndex = 0
INT								iOutWayPoint
INT								iFinalFailDelay
//INT 							iShootStartTime

//Road Node Machine Vars

//INT								iRoadNodeMachineIndex = 0
//INT								iNumRoadNodes
//INT								iWayPtProgress

//
//INT								iFCHelpBitField
//INT								iChaseCamTime = 0

//Bools

BOOL							bHusbnadDrvSet = FALSE
BOOL							bPlayerDisabled = FALSE
BOOL							bTransititonToWaypointRec = FALSE
BOOL							bTaxiOJ_GYN_IsPlayerTooClose
BOOL							bTaxiOJ_GYN_IsFianceDead
BOOL							bTaxiOJ_GYN_IsPassengerAtConfrontationPt
BOOL							bTaxiSetVehicleMultiplier
BOOL							bFianceReacted
BOOL							bCheckPlayerEndInterruption
BOOL 							bAmbulanceFound, bDispatchSuccessful
BOOL 							bStartCar
BOOL							bKillFinalConversation
BOOL							bPulseOut1
BOOL							bPulseOut2

//Vectors
VECTOR 							vPassengerPt = << 1358.8215, -1547.3961, 53.7793 >>
VECTOR 							vPassengerPickupPt = << 1358.8215, -1547.3961, 53.7793 >>
VECTOR 							vEnemyDestination = <<-694.2758, -1119.4468, 13.5250>> //<< -878.1066, -19.5377, 41.1525 >>
VECTOR 							vDropOff = <<-683.1272, -1102.1846, 13.5257>> //<< -866.5179, -3.2549, 42.2436 >> //<< -870.4353, -9.8660, 41.8970 >> 
VECTOR 							vHospitalMin = << 410.2629, -1399.1598, 28.4017 >>
VECTOR 							vHospitalMax = << 371.3834, -1482.9553, 28.3418 >>

VECTOR							vEnemyPedSpawnPt = << 404.8026, -1416.2942, 28.4350 >>
VECTOR							vEnemyCarSpawnPt = <<406.612000,-1419.936890,29.003754>> //<<406.600494,-1419.958008,28.987108>> //<< 407.0237, -1420.0444, 28.4585 >>

VECTOR 							vWhoreSpawn = <<-682.5392, -1109.0822, 13.6729>>//<<-689.522827,-1116.601929,13.524973>> //<<-687.552002,-1114.860474,14.524975>>
VECTOR							vWhoreGoToPos = << -688.67273, -1117.51196, 13.52498 >>
VECTOR							vHouseMin = <<-667.1360, -1046.0604, 15.9174>> //<<-663.4819, -1064.0221, 15.5359>> //<< -872.3717, 1.9813, 42.8129 >>
VECTOR							vHouseMax = <<-703.1228, -1142.4324, 9.8127>> //<< -895.1227, -26.2457, 40.9286 >>

VECTOR 							vOutWayPointCoord

//New Cutscene 02 - Fiance Reaches His Side Chick - Coords
VECTOR							vTaxiOJ_FC_CS02_FianceWarpPt1 = <<-701.2533, -1080.2850, 12.2884>> //<< -820.3372, -4.8864, 39.7083 >> //<< -815.7826, -10.8013, 38.7909 >>
//VECTOR							vTaxiOJ_FC_C02_WarpPt_Player1 = <<-662.2928, -1036.4415, 16.5224>> //<< -793.3931, -50.7208, 36.8511 >>
VECTOR							vTaxiOJ_FC_RoadNodePts_End1 = <<-687.8794, -1108.0731, 13.5257>> //<< -868.58, 4.25, 42.71>>
VECTOR							vTaxiOJ_FC_RoadNodePts_End2 = <<-703.0013, -1084.0730, 12.1105>> //<<-939.73, 57.97, 49.24 >>

VECTOR							vPathNodeMin
VECTOR							vPathNodeMax

//Floats
FLOAT							fWhoreSpawnHeading = 25.0227
//FLOAT							fTaxiOJ_FC_C02_Heading_Taxi1 = 167.6095 //28
FLOAT							fTaxiOJ_FC_C02_Heading_Fiance1 = 226.3085 //120.7013 //29
FLOAT 							fEscapeeSpawnHeading = -128.232941 //232.3856 //231.0608
FLOAT							fEscapeeHeading = 238.4969
FLOAT							fEnemyDestHeading = 212.7682 //163.3336

INT								iTimeOfEscapeeStart = 0

TEXT_LABEL_15					sNurseCarRecording = "taxi_oj_fc_2" //"taxi_oj_fc"

TEXT_LABEL_63					sNurseChatAnim = "amb@world_human_hang_out_street@male_c@idle_a" // use idle_b
TEXT_LABEL_63					sNurseChatExit = "amb@world_human_hang_out_street@male_c@exit"
TEXT_LABEL_63					sMistressChatAnim = "amb@world_human_hang_out_street@female_hold_arm@idle_a" // use idle_a 
TEXT_LABEL_63					sMistressChatExit = "amb@world_human_hang_out_street@female_hold_arm@exit"

VECTOR 							vLocate_fc_final_cs_triggerPos1 = <<-727.111023,-1046.356812,11.439257>> //<<-683.490173,-1027.691528,15.216422>>
VECTOR 							vLocate_fc_final_cs_triggerPos2 = <<-642.609741,-1085.538086,28.429213>> //<<-624.758972,-1057.963501,22.049709>> 
FLOAT 							fLocate_fc_final_cs_triggerWidth = 76.750 //10.0 //5.000000


//Dialogue Queue Info
BOOL							g_bDebug = FALSE
TAXI_OJ_DIALOGUE_Q_DATA			tTaxiOJ_DQ_Data								
TAXI_OJ_DQ_CONVERSATION_LINE	tDialogueLine[CONST_TAXIOJ_SIZE_Q]

DRIVINGMODE						tFianceDrivingMode = DF_SwerveAroundAllCars|DF_SteerAroundObjects|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions
//DRIVINGMODE						tFianceDrivingMode = DF_SwerveAroundAllCars|DF_SteerAroundPeds|DF_SteerAroundObjects|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions
DRIVINGMODE						tFianceDrivingMode2 = DF_StopForCars //DF_SteerAroundStationaryCars | DF_ChangeLanesAroundObstructions | DF_PreferNavmeshRoute | DF_SwerveAroundAllCars
AGGRO_ARGS 						aggroArgs 

SCRIPT_SHARD_BIG_MESSAGE TaxiMidSize
//Debug---------------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 			taxiRideWidgets,ojTaxiWidgets_Chase
	TEXT_LABEL 					sDebugNumClose,sDebugNumHits, sDebugNumTooFar, sDebugNumWarn,sDebugChaseVals[2]
	TEXT_LABEL					sDebugPlayerDistance
	BOOL 						bDebugDrawDistance, bDrawHudHitCount
	BOOL 						bDebugTurnOnFreeRide = FALSE
	BOOL						bDebugDrawZones
	TXM_DEBUG_SKIP_STATES 		tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS
#ENDIF

//FUNCTIONS------------------------------------------------------------------------------------------------------------


ENUM FINAL_SCENE
	FINAL_SCENE_ARRIVE = 0,
	FINAL_SCENE_TAKE_PLACES,
	FINAL_SCENE_PASSENGER_POUNCE,
	FINAL_SCENE_WAIT_FOR_DIALOGUE,
	FINAL_SCENE_KILL_WOMAN,
	FINAL_SCENE_WAIT,
	FINAL_SCENE_KILL_MAN,
	FINAL_SCENE_FLEE
ENDENUM

FINAL_SCENE finalState =  FINAL_SCENE_ARRIVE

FUNC VECTOR GET_PASSENGER_WALK_TO_POINT(TAXIOJ_FC_DROPZONE eThisDropZone)
	SWITCH eThisDropZone
		CASE TFC_DROPZONE1
			RETURN <<-691.48, -1113.31, 13.53>> //<< -876.9708, -10.3602, 41.7911 >>
		CASE TFC_DROPZONE2
			RETURN <<-691.48, -1113.31, 13.53>> //<< -885.94, -10.91, 42.19 >>
	ENDSWITCH
	
	RETURN <<-691.48, -1113.31, 13.53>> //<< -876.9708, -10.3602, 41.7911 >>
ENDFUNC

/// PURPOSE: Request all our mission specific assets here
///    
PROC REQUEST_TAXI_ODDJOB_FC_STREAMS_STAGE_01()

	REQUEST_MODEL(mPassengerModel)
	TAXI_INIT_SHARED_STREAMS(FALSE)
	TaxiMidSize.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	REQUEST_ANIM_DICT("gestures@m@standing@casual")

	CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ Follow Car Stage01 Assets Requested")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_DL_STREAMS_STAGE_01()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mPassengerModel)
	
	REMOVE_ANIM_DICT("gestures@m@standing@casual")
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"[CLEANUP] Taxi OJ Follow Car Stage01 Assets Released")
	
ENDPROC
PROC REQUEST_TAXI_ODDJOB_FC_STREAMS_STAGE_02()
	//Load text and UI
	REQUEST_MODEL(myEscapeeStruct.escapeeModel)
	REQUEST_MODEL(myEscapeeStruct.escapeeCarModel)
	
	REQUEST_MODEL(mHospitalCar1)
	REQUEST_MODEL(mHospitalCar2)
	
	REQUEST_VEHICLE_RECORDING(TAXI_CONST_VR_FIANCE_EXIT_HOSPITAL,"txm_fc_h1_")
	REQUEST_VEHICLE_RECORDING(0,"taxi_oj_fc3")
	
	REQUEST_WAYPOINT_RECORDING(sNurseCarRecording)
	
	REQUEST_ANIM_DICT("veh@truck@ds@base")
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ Follow Car Stage02 - Assets Requested")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_FC_STREAMS_STAGE_02()

	//Load text and UI
	SET_MODEL_AS_NO_LONGER_NEEDED(myEscapeeStruct.escapeeModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(myEscapeeStruct.escapeeCarModel)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mHospitalCar1)
	SET_MODEL_AS_NO_LONGER_NEEDED(mHospitalCar2)
	
	REMOVE_WAYPOINT_RECORDING(sNurseCarRecording)
	
	REMOVE_ANIM_DICT("veh@truck@ds@base")
	
	CDEBUG1LN(DEBUG_OJ_TAXI," [CLEANUP] INIT_TAXI_STREAMS - SUCCESS")
	
ENDPROC

PROC REQUEST_TAXI_ODDJOB_FC_STREAMS_STAGE_END()
	
	REQUEST_MODEL(mWhoreModel)
	REQUEST_ANIM_DICT("MOVE_DUCK_FOR_COVER")
	REQUEST_ANIM_DICT(sNurseChatAnim)
	REQUEST_ANIM_DICT(sNurseChatExit)
	REQUEST_ANIM_DICT(sMistressChatAnim)
	REQUEST_ANIM_DICT(sMistressChatExit)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ Follow Car End Assets Requested")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_FC_STREAMS_STAGE_END()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mWhoreModel)
	REMOVE_ANIM_DICT("MOVE_DUCK_FOR_COVER")
	REMOVE_ANIM_DICT(sNurseChatAnim) 
	REMOVE_ANIM_DICT(sNurseChatExit)
	REMOVE_ANIM_DICT(sMistressChatAnim)
	REMOVE_ANIM_DICT(sMistressChatExit)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"[CLEANUP]Taxi OJ Follow Car End Assets Requested")
ENDPROC

FUNC BOOL HAVE_TAXI_OJ_FC_STAGE_END_ASSETS_LOADED()

	IF NOT HAS_MODEL_LOADED(mWhoreModel)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("MOVE_DUCK_FOR_COVER")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading MOVE_DUCK_FOR_COVER",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"[ASSETS] Taxi OJ FC End Assets Have All Loaded")
	RETURN TRUE
	
ENDFUNC


FUNC BOOL HAVE_TAXI_OJ_FC_STAGE_02_ASSETS_LOADED()
	//Need this after passenger is picked up
	
	IF NOT HAS_MODEL_LOADED(myEscapeestruct.escapeeModel)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_FC_STAGE_02 - Loading escape ped",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(myEscapeestruct.escapeeCarModel)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_FC_STAGE_02 - Loading escape car",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(mHospitalCar1)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_FC_STAGE_02 - Loading ambulance",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(mHospitalCar2)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_FC_STAGE_02 - Loading hospital car 2",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(TAXI_CONST_VR_FIANCE_EXIT_HOSPITAL, "txm_fc_h1_")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_FC_STAGE_02 - Vehicle Recording Loading txm_fc_h1_...",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(0, "taxi_oj_fc3")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_FC_STAGE_02 - Vehicle Recording Loading taxi_oj_fc3...",iDebugThrottle)
		RETURN FALSE
	ENDIF

	IF NOT GET_IS_WAYPOINT_RECORDING_LOADED(sNurseCarRecording)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_FC_STAGE_02 - Way point Recording Loading taxi_oj_fc...",iDebugThrottle)
		RETURN FALSE
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_ASSET_STREAMED - sNurseCarRecording loaded up just fine.")
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("veh@truck@ds@base")
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_ASSET_STREAMED - Loading anim dict veh@truck@ds@base")
		RETURN FALSE
	ENDIF

	CDEBUG1LN(DEBUG_OJ_TAXI,"[ASSETS] Taxi OJ FC S02 Assets Have All Loaded - [S_M_M_DOCTOR_01, BISON, AMBULANCE, 1x VEHICLE RECORDINGS AND 1 WAYPOINT RECORDING]")
	RETURN TRUE
ENDFUNC


//// PURPOSE: Waits for our assets to load
 ///    
 /// RETURNS: TRUE if everything has loaded correctly FALSE if there was a problem
 ///    

FUNC BOOL HAVE_TAXI_OJ_FC_STAGE_01_ASSETS_LOADED()
	
	IF NOT HAS_MODEL_LOADED(mPassengerModel)
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_ASSET_STREAMED - Loading ", "",GET_MODEL_NAME_FOR_DEBUG(mPassengerModel) )
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("gestures@m@standing@casual")
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_ASSET_STREAMED - Loading anim dict gestures@m@standing@casual")
		RETURN FALSE
	ENDIF
	
	IF NOT TAXI_SHARED_ASSETS_STREAMED(iDebugThrottle,FALSE)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading shared assets",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"[ASSETS] TAXI OJ FC STAGE01 Assets Loaded~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")	
	RETURN TRUE		
ENDFUNC

/// PURPOSE: Call just before terminating this script
///   
PROC Script_Cleanup()
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(taxiRideWidgets)
			DELETE_WIDGET_GROUP(taxiRideWidgets)
		ENDIF
		
		IF DOES_WIDGET_GROUP_EXIST(ojTaxiWidgets_Chase)
			DELETE_WIDGET_GROUP(ojTaxiWidgets_Chase)
		ENDIF
		
		CLEANUP_TAXI_WIDGETS()
		
	#ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vHospitalMin, vHospitalMax, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vHouseMin, vHouseMax, TRUE)
	TOGGLE_ROAD_NODES_IN_AREA_FOR_TAXI_OJ(vTaxiOJ_FC_RoadNodePts_End1, TAXI_AREA_ROAD_DISABLE_RADIUS, FALSE)
	TOGGLE_ROAD_NODES_IN_AREA_FOR_TAXI_OJ(vTaxiOJ_FC_RoadNodePts_End2, TAXI_AREA_ROAD_DISABLE_RADIUS, FALSE)
	
	CLEAR_PED_NON_CREATION_AREA()
	REMOVE_SCENARIO_BLOCKING_AREA(tempScenarioBlockingIndex)
	
	REMOVE_RELATIONSHIP_GROUP( rghWhore )
	REMOVE_RELATIONSHIP_GROUP( rghPassenger )
	REMOVE_WAYPOINT_RECORDING(sNurseCarRecording)
	
	TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(<< 385.1685, -1372.7186, 29.8554 >>,TRUE,TAXI_CONST_FC_ROAD_DISABLE_RADIUS)
	
	RELEASE_TAXI_ODDJOB_DL_STREAMS_STAGE_01()
	RELEASE_TAXI_ODDJOB_FC_STREAMS_STAGE_02()
	RELEASE_TAXI_ODDJOB_FC_STREAMS_STAGE_END()
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(myEscapeeStruct.escapeeCarModel, FALSE)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Oddjob Follow Car_Cleanup - SUCCESS")
	TERMINATE_THIS_THREAD()
ENDPROC

// Perform any special commands if the script passes
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Passed()
	SET_TAXI_ODDJOB_PASSED(myTaxiData)
	
	CLEANUP_TAXI_ODDJOB(myTaxiData)
	Script_Cleanup()
ENDPROC

// Perform any special commands if the script fails
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Failed()
	TAXI_SCRIPT_FAILED(myTaxiData)
	SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
	Script_Cleanup()
ENDPROC

PROC INITIALIZE_SCRIPT_VARIABLES()
	
	TAXI_ODDJOB_GLOBAL_SETUP(myTaxiData,TXM_10_FOLLOWTHATCAR)	
		
	// Set our initial state up. VIP missions don't start at TRS_FINDING_LOCATION.
	myTaxiData.tTaxiOJ_RideState = TRS_INIT_STREAM
	
	SET_TAXI_TIP_CUTOFFS(myTaxiData,CONST_TAXI_OJ_TIP_AVERAGE_CUTOFF,CONST_TAXI_OJ_TIP_AMAZING_CUTOFF)
	
	//Init j skip points------
	myTaxiData.vTaxiOJ_WarpPtPickup = << 1412.3242, -1518.3154, 58.4644 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingPickup = 114.1
	
	myTaxiData.vTaxiOJ_WarpPtDropoff = vDropOff
	myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 95.93
	
	INITIALIZE_ESCAPEE_STRUCT(myTaxiData,myEscapeeStruct)
	myEscapeeStruct.escapeeModel 	=  S_M_M_DOCTOR_01
	myEscapeeStruct.escapeeCarModel = BISON
	
	#IF IS_DEBUG_BUILD
		#IF DEBUG_iTurnOnAllDXDebug
			ENABLE_ALL_DIALOGUE_DEBUG()
		#ENDIF
		//Initial position for the distance between the taxi and the pursue-ee
		taxiRideWidgets = START_WIDGET_GROUP("Taxi Ride - Follow That Car")
			
			INIT_ODDJOB_TAXI_WIDGETS()
			
			//Gameplay Widget
			ojTaxiWidgets_Chase = START_WIDGET_GROUP("Chase")
	
				ADD_WIDGET_STRING("Follow Lengths")
					ADD_WIDGET_FLOAT_SLIDER("Too Far Warn Dist: ", TWEAK_TAXI_FOLLOW_TOO_FAR_DIST_WARN,100.0,500.0,20.0)
					ADD_WIDGET_FLOAT_SLIDER("Too Far Dist: ", TWEAK_TAXI_FOLLOW_TOO_FAR_DIST,100.0,500.0,20.0)
					
		
				ADD_WIDGET_BOOL("Draw Chase Hit/Warn Count", bDrawHudHitCount)

				ADD_WIDGET_BOOL("Draw Follow Zones", bDebugDrawZones)
				
				ADD_WIDGET_BOOL("Draw Distance to Chasee", bDebugDrawDistance)
				
					
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()//Main Widget Group
		
		
	#ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~~~~~~~~~~~~ Oddjobs | Taxi | Follow Car ~~~~~~~~~~~~~~~~~~~----------")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")
	
ENDPROC
#IF IS_DEBUG_BUILD
PROC PROCESS_WIDGETS()
	
	UPDATE_TAXI_WIDGETS(myTaxiData,tTaxiOJ_DQ_Data)
	
	IF bDebugDrawDistance
		TAXI_DEBUG_DRAW_DISTANCE_CHASE(myTaxiData,myEscapeeStruct.viCar,vTaxiOJ_FC_CS02_FianceWarpPt1,sDebugChaseVals)
	ENDIF

	IF bDrawHudHitCount
		IF IS_VEHICLE_DRIVEABLE(myEscapeeStruct.viCar)

			sDebugNumWarn = "Warned # "
			sDebugNumWarn += TAXI_UTILS_GET_STRING_FROM_INT_SP(myEscapeeStruct.iNumTimesWarned)
			DRAW_DEBUG_TEXT_2D(sDebugNumWarn,<<0.8,0.28,0.0>>)
			
			sDebugNumHits = "Hits # "
			sDebugNumHits += TAXI_UTILS_GET_STRING_FROM_INT_SP(myEscapeeStruct.iNumTimesHitByPlayer)
			DRAW_DEBUG_TEXT_2D(sDebugNumHits,<<0.8,0.3,0.0>>)
			
			sDebugNumClose = "Close # "
			sDebugNumClose +=TAXI_UTILS_GET_STRING_FROM_INT_SP(myEscapeeStruct.iNumTimesTooClose)
			DRAW_DEBUG_TEXT_2D(sDebugNumClose,<<0.80,0.32,0.0>>)

			sDebugNumTooFar = "Far # "
			sDebugNumTooFar +=TAXI_UTILS_GET_STRING_FROM_INT_SP(myEscapeeStruct.iNumTimesOutofSight)
			DRAW_DEBUG_TEXT_2D(sDebugNumTooFar,<<0.8,0.34,0.0>>)
			
			
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL SPAWN_ESCAPEE_VEHICLE(escapeStruct &theEscapeStruct, VECTOR pos, FLOAT heading )
	theEscapestruct.vCarSpawnPt = pos
	theEscapeStruct.fCarheading = heading
	theEscapestruct.viCar = CREATE_VEHICLE(theEscapestruct.escapeeCarModel, theEscapestruct.vCarSpawnPt, theEscapeStruct.fCarheading)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(theEscapestruct.escapeeCarModel, TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(theEscapeStruct.escapeeCarModel)
	
	// placing ambient hospital vehicles
	viHospitalCars[0] = CREATE_VEHICLE(mHospitalCar1, << 404.9912, -1423.9735, 28.4638 >>, 227.5235)//<< 401.3416, -1425.8754, 28.4548 >>, 227.6349)
	viHospitalCars[1] = CREATE_VEHICLE(mHospitalCar1, << 402.2958, -1427.0176, 28.4632 >>, 226.6982)//<< 409.2538, -1437.2434, 28.4066 >>, 34.1472)
	viHospitalCars[2] = CREATE_VEHICLE(mHospitalCar2, << 398.4150, -1428.1665, 28.4504 >>, 226.4081)
	
	SET_VEHICLE_DOORS_LOCKED(viHospitalCars[0], VEHICLELOCK_LOCKED )
	SET_VEHICLE_DOORS_LOCKED(viHospitalCars[1], VEHICLELOCK_LOCKED )
	SET_VEHICLE_DOORS_LOCKED(viHospitalCars[2], VEHICLELOCK_LOCKED )
	
	
	SET_VEHICLE_NUMBER_PLATE_TEXT(viHospitalCars[0],"BRAVEST")
	SET_VEHICLE_NUMBER_PLATE_TEXT(viHospitalCars[1],"BOLDEST")
	SET_VEHICLE_NUMBER_PLATE_TEXT(viHospitalCars[2],"BADDEST")
	
	//Store current health
	SET_ENTITY_HEALTH(theEscapeStruct.viCar,TAXI_CONST_STOP_CAR_START_HEALTH)
	SET_VEHICLE_NUMBER_PLATE_TEXT(theEscapeStruct.viCar,"PUSSYWAG")
	SET_VEHICLE_DISABLE_TOWING(theEscapestruct.viCar, TRUE)
	SET_VEHICLE_ALARM(theEscapestruct.viCar,TRUE)
	
	theEscapeStruct.fCurrentEngHealth = GET_VEHICLE_ENGINE_HEALTH(theEscapeStruct.viCar) 
	theEscapestruct.tesState = TES_INIT			
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(theEscapeStruct.viCar,TRUE)
	
	TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(myTaxiData.vTaxiOJPickup,TRUE)
	TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(vTaxiOJ_FC_RoadNodePts_End1,TRUE,TAXI_CONST_FC_ROAD_NODES_RADIUS)
	TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(vTaxiOJ_FC_RoadNodePts_End2,TRUE,TAXI_CONST_FC_ROAD_NODES_RADIUS)
				
				
	SET_VEHICLE_DOORS_LOCKED(theEscapeStruct.viCar, VEHICLELOCK_LOCKED )
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Spawning Fiance's BISON.")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SPAWN_ESCAPEE(escapeStruct &theEscapeStruct, VECTOR pos, FLOAT heading )
	//Create driver and set him up
	
	IF NOT DOES_ENTITY_EXIST(theEscapestruct.piPed)
		theEscapestruct.piPed = CREATE_PED( PEDTYPE_MISSION, theEscapestruct.escapeeModel, pos, heading )
	ENDIF	

	//Add to dialogue struct.
	ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,4,theEscapestruct.piPed,"TaxiJames")
	SET_AMBIENT_VOICE_NAME(theEscapestruct.piPed, "TaxiJames")
	
	//SET_PED_COMPONENT_VARIATION(theEscapestruct.piPed, PED_COMP_HEAD, 1, 0)
	//SET_PED_COMPONENT_VARIATION(theEscapestruct.piPed, PED_COMP_LEG, 0, 0)
//	SET_PED_COMPONENT_VARIATION(theEscapestruct.piPed, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
//	SET_PED_COMPONENT_VARIATION(theEscapestruct.piPed, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
//	SET_PED_COMPONENT_VARIATION(theEscapestruct.piPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
//	SET_PED_COMPONENT_VARIATION(theEscapestruct.piPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
	
	IF NOT IS_ENTITY_DEAD(piWhore)
		SET_PED_CONFIG_FLAG(piWhore, PCF_AllowMedicsToAttend, TRUE)
	ENDIF	
	
	IF NOT IS_ENTITY_DEAD(theEscapestruct.piPed)
		SET_PED_COMPONENT_VARIATION(theEscapestruct.piPed, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(theEscapestruct.piPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(theEscapestruct.piPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(theEscapestruct.piPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
		SET_PED_COMPONENT_VARIATION(theEscapestruct.piPed, INT_TO_ENUM(PED_COMPONENT,11), 0, 1, 0) //(jbib)
		
		SET_PED_RELATIONSHIP_GROUP_HASH(theEscapestruct.piPed, theEscapestruct.relEscapee)
		SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed, CA_USE_VEHICLE,TRUE)
		SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed, CA_FLEE_WHILST_IN_VEHICLE,TRUE)
		SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed, CA_ALWAYS_FLEE,TRUE)
		SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_SEEING_RANGE(theEscapestruct.piPed, 300.0)
		SET_PED_HEARING_RANGE(theEscapestruct.piPed, 300.0)
		SET_PED_ID_RANGE(theEscapestruct.piPed, 300.0)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(theEscapestruct.piPed, TRUE)	
	ENDIF
	RETURN TRUE
ENDFUNC

PROC SPAWN_WHORE()
	SEQUENCE_INDEX seq
	piWhore = CREATE_PED(PEDTYPE_MISSION, mWhoreModel, vWhoreSpawn, fWhoreSpawnHeading)
	SET_PED_MAX_HEALTH( piWhore, 5 )
	
	SET_PED_COMPONENT_VARIATION(piWhore, PED_COMP_TORSO, 1, 2)
	SET_PED_COMPONENT_VARIATION(piWhore, PED_COMP_LEG, 0, 1)
	SET_PED_COMPONENT_VARIATION(piWhore, PED_COMP_HAIR, 1, 2)
	
	SET_PED_CONFIG_FLAG(piWhore, PCF_AllowMedicsToAttend, TRUE)
	
	ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,5,piWhore,"TaxiCarrie")
	
	IF NOT IS_ENTITY_DEAD(piWhore)
		SET_ENTITY_HEADING(piWhore, fWhoreSpawnHeading)
		CLEAR_SEQUENCE_TASK(seq)
		OPEN_SEQUENCE_TASK(seq)
			TASK_STAND_STILL(NULL, 2500)
			TASK_PLAY_ANIM(NULL, "oddjobs@towingcome_here", "come_here_idle_a", NORMAL_BLEND_IN, WALK_BLEND_OUT, 2000)
			TASK_GO_TO_COORD_ANY_MEANS(NULL, vWhoreGoToPos, PEDMOVE_WALK, NULL)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(piWhore, seq)
		CLEAR_SEQUENCE_TASK(seq)	
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Spawning Whore")

ENDPROC

PROC MONITOR_TAXI_ESCAPEE_OK(TaxiStruct &myTaxi, PED_INDEX target, BOOL bCheckJacking = TRUE, BOOL bIsTarget = TRUE)
	IF DOES_ENTITY_EXIST(target)
		IF bCheckJacking
			IF NOT IS_ENTITY_DEAD(target)
				IF IS_PED_BEING_JACKED(target)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Ped is being jacked")
					IF IS_PED_JACKING(PLAYER_PED_ID())
						IF myTaxiData.bTaxiOJ_Failed = FALSE
							TAXI_SET_FAIL(myTaxiData,"Driver jacked the fiance", TFS_JACKED)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		//Check if he's been injured than fail
		IF IS_PED_INJURED(target)
			IF bIsTarget
				IF NOT IS_ENTITY_DEAD(piWhore)
					CLEAR_PED_TASKS(piWhore)
					TASK_SMART_FLEE_PED(piWhore, PLAYER_PED_ID(), 100, 20000)
					SET_PED_KEEP_TASK(piWhore, TRUE)
				ENDIF
				
				TAXI_SET_FAIL(myTaxi, "Target injured.", TFS_CHASEE_KILLED)
			ELSE
				IF NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed)
					CLEAR_PED_TASKS(myEscapeeStruct.piPed)
					TASK_SMART_FLEE_PED(myEscapeeStruct.piPed, PLAYER_PED_ID(), 100, 20000)
					SET_PED_KEEP_TASK(myEscapeeStruct.piPed, TRUE)
				ENDIF
				
				TAXI_SET_FAIL(myTaxi, "Mistress injured.", TFS_PASSENGER_SHOT)
			ENDIF
		ELSE
			IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(target, WEAPONTYPE_STUNGUN,GENERALWEAPON_TYPE_INVALID)
				OR HAS_PED_BEEN_DAMAGED_BY_WEAPON(target, WEAPONTYPE_INVALID,GENERALWEAPON_TYPE_ANYWEAPON)
				OR HAS_PED_BEEN_DAMAGED_BY_WEAPON(target, WEAPONTYPE_INVALID,GENERALWEAPON_TYPE_ANYMELEE)
					IF NOT IS_ENTITY_DEAD(piWhore)
						CLEAR_PED_TASKS(piWhore)
						TASK_SMART_FLEE_PED(piWhore, PLAYER_PED_ID(), 100, 20000)
						SET_PED_KEEP_TASK(piWhore, TRUE)
					ENDIF
					IF NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed)
						CLEAR_PED_TASKS(myEscapeeStruct.piPed)
						TASK_SMART_FLEE_PED(myEscapeeStruct.piPed, PLAYER_PED_ID(), 100, 20000)
						SET_PED_KEEP_TASK(myEscapeeStruct.piPed, TRUE)
					ENDIF
					
					TAXI_SET_FAIL(myTaxi, "Passenger injured by player with weapon.", TFS_PASSENGER_SHOT)
				ENDIF
				IF NOT bIsTarget
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				ENDIF
			ENDIF
		ENDIF
		
		
	ENDIF
	//---------------------------------------------------------------------------------
ENDPROC

PROC CHECK_NURSE_OR_HIS_VEHICLE_BEEN_DAMAGED(TaxiStruct &myTaxi, escapeStruct &target)
	
	MONITOR_TAXI_ESCAPEE_OK(myTaxi,target.piPed )
	
	IF IS_VEHICLE_DRIVEABLE(target.viCar)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(target.viCar,PLAYER_PED_ID())
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				TAXI_SET_FAIL(myTaxiData,"Target's vehicle has been damaged by the player", TFS_SPOTTED)
			ELSE
				CDEBUG1LN(DEBUG_OJ_TAXI,"can't fail player yet for damaging the target, killing conversation first")
				KILL_ANY_CONVERSATION()
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			TAXI_SET_FAIL(myTaxiData,"Target's vehicle isn't driveable", TFS_CHASEE_KILLED)
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"can't fail player yet for killing the target, killing conversation first")
			KILL_ANY_CONVERSATION()
		ENDIF
	ENDIF
	//---------------------------------------------------------------------------------
ENDPROC



FUNC VECTOR GET_TAXI_OJ_GYN_WHORE_REVEAL_CS_CAM_POS(INT index)
	VECTOR vCamPos
	
	SWITCH index
		CASE 0
			vCamPos = <<-686.0368, -1073.2203, 16.1762>> //<<-697.9617, -1073.7943, 18.1988>>
		BREAK
	ENDSWITCH
	
	RETURN vCamPos
ENDFUNC

FUNC VECTOR GET_TAXI_OJ_GYN_WHORE_REVEAL_CS_CAM_ROT(INT index)
	VECTOR vCamPos
	
	SWITCH index
		CASE 0
			vCamPos = <<1.9027, -0.0000, 170.5764>> //<<-13.7034, -0.0000, -152.3847>>
		BREAK
	ENDSWITCH
	
	RETURN vCamPos
	
ENDFUNC
ENUM TX_OJ_CS_02_STATES
	FC_CS_02_INIT = 0,
	FC_CS_02_SHOT_1,
	FS_CS_02_SHOT_2,
	FC_CS_02_SKIP,
	FC_CS_02_END,
	FC_CS_02_CLEANUP
ENDENUM
TX_OJ_CS_02_STATES thisCS_02_States = FC_CS_02_INIT

FUNC BOOL TAXI_OJ_CS_WHORE_REVEAL()
	
	STOP_PHONE_AND_APPS_FOR_TAXI_CUTSCENE()
	
	//SKIP CHECK
	IF thisCS_02_States < FC_CS_02_SKIP AND thisCS_02_States > FC_CS_02_INIT
		IF HANDLE_SKIP_CUTSCENE(iAllowSkipCutsceneTimeLocal)
			thisCS_02_States = FC_CS_02_SKIP
			CDEBUG1LN(DEBUG_OJ_TAXI,"CUTSCENE FC_CS_02_SKIP")
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(piWhore)
		IF NOT IS_PED_INJURED(piWhore)
			VECTOR vPos
			vPos = GET_ENTITY_COORDS(piWhore)
			CDEBUG1LN(DEBUG_OJ_TAXI,"vPos = ", vPos)
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"whore is dead")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"whore does not exist")
	ENDIF
	
	SWITCH thisCS_02_States
		CASE FC_CS_02_INIT
			IF CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)
			AND NOT myTaxiData.bIsTaxiDebugSkipping
				iAllowSkipCutsceneTimeLocal = GET_GAME_TIMER()
				
				//Clear & Close the Dialogue Q
				CLEAR_DIALOGUE_QUEUE(tDialogueLine)
				CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,-1,TRUE)
				
				TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(vTaxiOJ_FC_RoadNodePts_End1,FALSE,TAXI_CONST_FC_ROAD_NODES_RADIUS)
				TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(vTaxiOJ_FC_RoadNodePts_End2,FALSE,TAXI_CONST_FC_ROAD_NODES_RADIUS)
				
				
				//Cleanup,Create & init cams and make it look at the first shot
				CREATE_TAXI_OJ_CAM(myTaxiData.camTaxi,<<0,0,0>>, <<0,0,0>>)				
				
				SPAWN_WHORE()
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"thisCS_02_States = FC_CS_02_SHOT_1")
				thisCS_02_States = FC_CS_02_SHOT_1
			ELSE
				CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TX_FC CAN_PLAYER_START_CUTSCENE returns false",myTaxiData.iTaxiOJ_DebugThrottle)
			ENDIF
				
		BREAK
		
		CASE FC_CS_02_SHOT_1
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 0.5

				ODDJOB_ENTER_CUTSCENE()
				
				CLEAR_AREA_OF_VEHICLES(vTaxiOJ_FC_RoadNodePts_End1,30.0)
				CLEAR_AREA_OF_VEHICLES(vTaxiOJ_FC_RoadNodePts_End2,30.0)
				CLEAR_AREA_OF_PEDS(vTaxiOJ_FC_RoadNodePts_End1,30.0)
				CLEAR_AREA_OF_PEDS(vTaxiOJ_FC_RoadNodePts_End2,30.0)
				
				TOGGLE_ROAD_NODES_IN_AREA_FOR_TAXI_OJ(vTaxiOJ_FC_RoadNodePts_End1)
				TOGGLE_ROAD_NODES_IN_AREA_FOR_TAXI_OJ(vTaxiOJ_FC_RoadNodePts_End2)
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vHouseMin, vHouseMax, FALSE)
				
				//"He better not be going where I think he is"
				SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_SEEN_DESTINATION,TRUE,TRUE)
						
				SET_CAM_COORD(myTaxiData.camTaxi, GET_TAXI_OJ_GYN_WHORE_REVEAL_CS_CAM_POS(0))
				SET_CAM_ROT(myTaxiData.camTaxi, GET_TAXI_OJ_GYN_WHORE_REVEAL_CS_CAM_ROT(0))
				SET_CAM_FOV(myTaxiData.camTaxi, 44.5167)
				SHAKE_CAM(myTaxiData.camTaxi, "HAND_SHAKE", 0.1)
				SET_CAM_ACTIVE(myTaxiData.camTaxi,TRUE)
				
				RENDER_SCRIPT_CAMS(TRUE,FALSE)	
				
//				//Port Player
//				IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)			
//					SET_ENTITY_COORDS(myTaxiData.viTaxi,vTaxiOJ_FC_C02_WarpPt_Player1)
//					SET_ENTITY_HEADING(myTaxiData.viTaxi,fTaxiOJ_FC_C02_Heading_Taxi1)
//				ENDIF
				
				//Port Fiance car
				IF IS_VEHICLE_DRIVEABLE(myEscapeeStruct.viCar)
					//IF GET_ENTITY_DISTANCE_FROM_LOCATION(myEscapeeStruct.viCar,vTaxiOJ_FC_CS02_FianceWarpPt1) > 10.0
						SET_ENTITY_COORDS(myEscapeeStruct.viCar,vTaxiOJ_FC_CS02_FianceWarpPt1)
						SET_ENTITY_HEADING(myEscapeeStruct.viCar,fTaxiOJ_FC_C02_Heading_Fiance1 )
						
						//Task Fiance Car in Case he's not correctly following his point
						IF WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(sNurseCarRecording, vTaxiOJ_FC_CS02_FianceWarpPt1, iOutWayPoint)
							CDEBUG1LN(DEBUG_OJ_TAXI,"closest waypoint grabbed")
						ELSE
							CDEBUG1LN(DEBUG_OJ_TAXI,"closest waypoint NOT grabbed")
						ENDIF
						
						IF WAYPOINT_RECORDING_GET_COORD(sNurseCarRecording, iOutWayPoint, vOutWayPointCoord)
							CDEBUG1LN(DEBUG_OJ_TAXI,"closest coord grabbed")
						ELSE
							CDEBUG1LN(DEBUG_OJ_TAXI,"closest coord NOT grabbed")
						ENDIF
						
						//Retask him at this point
						IF NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed)
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(myEscapeeStruct.piPed, myEscapeeStruct.viCar, sNurseCarRecording, tFianceDrivingMode2, iOutWayPoint + 4)
						ENDIF
					//ENDIF
				ENDIF
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)			
				CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = FS_CS_02_SHOT_2")
				thisCS_02_States = FS_CS_02_SHOT_2
					
			ENDIF
		BREAK
		
		CASE FS_CS_02_SHOT_2
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 5
				TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)			
				CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = FC_CS_02_END")
				thisCS_02_States = FC_CS_02_END
			ELIF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 4.7
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					IF NOT bPulseOut2
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bPulseOut2 = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE FC_CS_02_SKIP				
			IF IS_VEHICLE_DRIVEABLE(myEscapeeStruct.viCar)
				SET_ENTITY_COORDS(myEscapeeStruct.viCar,vEnemyDestination)
				SET_ENTITY_HEADING(myEscapeeStruct.viCar,fEnemyDestHeading )
				STOP_PLAYBACK_RECORDED_VEHICLE(myEscapeeStruct.viCar)				

			ENDIF
			TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)	
			thisCS_02_States = FC_CS_02_END
			CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = FC_CS_02_END")
			
		BREAK
		CASE FC_CS_02_END
			
			IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)

				myTaxiData.bPassengerObjPrinted = FALSE
				
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				IF IS_VEHICLE_DRIVEABLE(myEscapeeStruct.viCar)
					
					SET_ENTITY_COORDS(myEscapeeStruct.viCar,vEnemyDestination)
					SET_ENTITY_HEADING(myEscapeeStruct.viCar,fEnemyDestHeading )
					STOP_PLAYBACK_RECORDED_VEHICLE(myEscapeeStruct.viCar)
					
					IF NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed)
						CLEAR_PED_TASKS_IMMEDIATELY(myEscapeeStruct.piPed)
						SET_ENTITY_COORDS(myEscapeeStruct.piPed, << -691.15710, -1117.73206, 13.52498 >>)//<<-690.617859,-1117.427246,13.524973>>)
						SET_ENTITY_HEADING(myEscapeeStruct.piPed, -49.274364 )
					ENDIF
				ENDIF
				
				DESTROY_CAM(myTaxiData.camTaxi)
				DESTROY_CAM(ciTransCam)
				ODDJOB_EXIT_CUTSCENE()
				
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
				
				WHILE NOT IS_SCREEN_FADED_IN()
					WAIT(0)
				ENDWHILE
				
				TAXI_CANCEL_TIMERS(myTaxiData, TT_CUTSCENE)			
				thisCS_02_States = FC_CS_02_CLEANUP
				CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = GYN_CS_CLEANUP")
						
				RETURN TRUE
					
			ENDIF
		BREAK
			
			
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
// PURPOSE: Call this to get the different camera positions for the csite cutscene
///    
/// PARAMS:
///    index - 
/// RETURNS:
///    
FUNC VECTOR GET_TAXI_OJ_FTC_HOSPITAL_CS_CAM_POS(INT index)
	VECTOR vCamPos
	SWITCH index
		
		//Shot of the car & taxi
		CASE 0
			 vCamPos =  << 495.4, -1416.8, 30.2>> //<< 408.8071, -1426.0117, 30.2467 >>
		BREAK
		
		//Shot from side of taxi
		CASE 1
			 vCamPos =  << 429.6911, -1385.4146, 29.7266 >>
		BREAK
		
		//Vista shot of the entire scene
		CASE 2
			 vCamPos =  <<417.9, -1419.4, 30.6>> //<< 448.2621, -1413.8420, 34.4501 >>
		BREAK
		
		//Facing the cab
		CASE 3
			 vCamPos =  << 426.7540, -1386.5096, 29.6201 >>
		BREAK
		
		//Transition cams----------------------------------------------
		CASE 4
			 vCamPos =  <<493.7, -1416.9, 30.2>> //<< 409.5267, -1426.0370, 30.2467 >>
		BREAK
		
		//Vista shot of the entire scene
		CASE 5
			 vCamPos =  << 450.0146, -1414.5232, 34.4507 >>
		BREAK
		
		//Facing the cab
		CASE 6
			 vCamPos =  << 448.2621, -1413.8420, 34.4501 >>
		BREAK
	ENDSWITCH
	
	RETURN vCamPos
ENDFUNC
/// PURPOSE: Call this to get the different camera directions for the csite custscene
///    
/// PARAMS:
///    index - 
/// RETURNS:
///    
FUNC VECTOR GET_TAXI_OJ_FTC_HOSPITAL_CS_CAM_ROT(INT index)
	VECTOR vCamDir
	SWITCH index
		
		//Shot of the car & taxi
		CASE 0
			 vCamDir = <<-0.7535, -0.0475, 91.9877>>// << -5.4284, 0.0000, -3.6285 >>
		BREAK
		
		//Shot from side of taxi
		CASE 1
			 vCamDir =  << 4.2328, 0.0000, 165.0072 >>
		BREAK
		
		//Vista shot of the entire scene
		CASE 2
			 vCamDir =  << -0.3221, 0.0000, 68.7635 >>
		BREAK
		
		//Facing the cab
		CASE 3
			 vCamDir =  << -0.3614, 0.0000, -145.1533 >>
		BREAK
		//Transition cams----------------------------------------------
		CASE 4
			 vCamDir =  << -5.4284, 0.0000, 1.8065 >>
		BREAK
		
		//Vista shot of the entire scene
		CASE 5
			 vCamDir =  << 1.2038, -0.0000, 68.7635 >>
		BREAK
		
		//Facing the cab
		CASE 6
			 vCamDir =  << -2.1877, 0.0000, 71.1648 >>
		BREAK
	ENDSWITCH
	
	RETURN vCamDir
ENDFUNC

ENUM HOSPITAL_CUTSCENE_STATES
	HOSP_CS_INIT = 0,
	HOSP_CS_CUT_01,
	HOSP_CS_TRIGGER_INTERP_01,
	HOSP_CS_CUT_02,
	HOSP_CS_TRIGGER_INTERP_02,
	HOSP_CS_CUT_03,
	HOSP_CS_CUT_04,
	HOSP_CS_CUT_05,
	HOSP_CS_CUT_06,
	HOSP_CS_CUT_SKIP,
	HOSP_CS_CUT_FADEIN,
	HOSP_CS_CUT_END,
	HOSP_CS_CUT_CLEANUP
	
ENDENUM
HOSPITAL_CUTSCENE_STATES	m_HospCSState = HOSP_CS_INIT

PROC CONFIGURE_DRIVE_OFF()
	//SEQUENCE_INDEX tempIndex
	bHusbnadDrvSet = TRUE
	//Task the driver to enter the car, than task them to drive to a point
	IF NOT IS_PED_INJURED(myEscapeeStruct.piPed) 
		IF IS_VEHICLE_DRIVEABLE(myEscapeeStruct.viCar)
			IF IS_PED_IN_VEHICLE(myEscapeeStruct.piPed, myEscapeeStruct.viCar)
				
				SET_ENTITY_COORDS (myEscapeeStruct.viCar, <<406.612000,-1419.936890,29.003754>>)
				
				SET_PED_RELATIONSHIP_GROUP_HASH(myEscapeeStruct.piPed, myEscapeeStruct.relEscapee)
				SET_PED_COMBAT_ATTRIBUTES(myEscapeeStruct.piPed, CA_USE_VEHICLE ,TRUE)
				SET_PED_COMBAT_ATTRIBUTES(myEscapeeStruct.piPed, CA_FLEE_WHILST_IN_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(myEscapeeStruct.piPed, CA_ALWAYS_FLEE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(myEscapeeStruct.piPed, CA_LEAVE_VEHICLES, FALSE)
				SET_PED_SEEING_RANGE(myEscapeeStruct.piPed, 500.0)
				SET_PED_HEARING_RANGE(myEscapeeStruct.piPed, 500.0)
				SET_PED_ID_RANGE(myEscapeeStruct.piPed, 500.0)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myEscapeeStruct.piPed, TRUE)
				
				SET_ENTITY_LOD_DIST( myEscapeeStruct.viCar, 500 )
				SET_ENTITY_LOD_DIST( myEscapeeStruct.piPed, 500 )
				SET_ENTITY_LOAD_COLLISION_FLAG( myEscapeeStruct.viCar, TRUE )
				SET_ENTITY_LOAD_COLLISION_FLAG( myEscapeeStruct.piPed, TRUE )
													
				//Blip Chasee once objective has been given
				IF NOT DOES_BLIP_EXIST(myEscapeeStruct.bBlip)
					myEscapeeStruct.bBlip = CREATE_BLIP_FOR_ENTITY(myEscapeeStruct.viCar)
				ENDIF
														
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myEscapeeStruct.piPed, TRUE)
										
				START_PLAYBACK_RECORDED_VEHICLE(myEscapeeStruct.viCar,TAXI_CONST_VR_FIANCE_EXIT_HOSPITAL,"txm_fc_h1_")
				SET_PLAYBACK_SPEED(myEscapeeStruct.viCar,0.8)
			
				
				
				iTimeOfEscapeeStart = GET_GAME_TIMER()
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TAXI_OJ_FC_SET_TIP_AND_EXCITEMENT_TO_CHECK()
	
	//Tip Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POS_DELIVERY_TIME)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_HIT_PED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_TOOK_DAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ROLL_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LEFT_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_STOPPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_LIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_DISLIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LOST_POLICE)
	
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_FOLLOW_TOO_FAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_FOLLOW_JUST_RIGHT)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_FOLLOW_TOO_CLOSE)
	
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_FREEBIE)
	
	//EXCITEMENT BITS
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_HITPED)					
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_TOOKDAMAGE)					
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_ROLL)	
	
	//Turn off aggro bits
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
					
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DL_SET_TIPS_TO_CHECK set tips to check on")
	
ENDPROC

/// PURPOSE: Monitors important peds during the taxi mission. If they are ever harmed/killed set the player to wanted level, 
///     don't let the mission end until the wanted level has been lost.
///    
/// PARAMS:
///    theEscapeStruct - our followee data
/// RETURNS:
///    
///    
FUNC BOOL IS_TAXI_POLICE_READY(PED_INDEX ped, BLIP_INDEX blip)
    
    IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
        IF IS_PED_INJURED(ped)
        
            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped,PLAYER_PED_ID())
                    SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(),1)
                    SET_PLAYER_WANTED_LEVEL_NOW(GET_PLAYER_INDEX())
                    
                    IF DOES_BLIP_EXIST(blip)
                        REMOVE_BLIP(blip)
                    ENDIF
                    
                    CDEBUG1LN(DEBUG_OJ_TAXI,"IS_TAXI_POLICE_READY - Player is now wanted")
                                
                    RETURN TRUE
                ENDIF
            ENDIF
        ENDIF

    ENDIF
    
    RETURN FALSE
ENDFUNC

//Plays the ambush in construction site cutscene
FUNC BOOL TAXI_OJ_FTC_CUTSCENE_HOSPITAL()
	
	STOP_PHONE_AND_APPS_FOR_TAXI_CUTSCENE()
	
	IF m_HospCSState < HOSP_CS_CUT_SKIP AND m_HospCSState > HOSP_CS_TRIGGER_INTERP_01 //HOSP_CS_INIT
		IF HANDLE_SKIP_CUTSCENE(iAllowSkipCutsceneTimeLocal)
			m_HospCSState = HOSP_CS_CUT_SKIP
			CDEBUG1LN(DEBUG_OJ_TAXI,"HOSP_CS_CUT_SKIP")
		ENDIF
	ENDIF
	//PED_INDEX pedCSDummy
	SWITCH m_HospCSState
	
		//Init everying and put everything in it's place
		CASE HOSP_CS_INIT
		
			IF CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)
				iAllowSkipCutsceneTimeLocal = GET_GAME_TIMER()

				ODDJOB_ENTER_CUTSCENE(SPC_LEAVE_CAMERA_CONTROL_ON)
				
				//Clear & Close the Dialogue Q
				CLEAR_DIALOGUE_QUEUE(tDialogueLine)
				CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data, 2, FALSE)
				
				
				IF SPAWN_ESCAPEE(myEscapeeStruct, vEnemyPedSpawnPt, fEscapeeHeading )
						
					SET_ROADS_IN_AREA(<< 1346.90, -1606.52, 31.16 >>, << 1457.25, -1508.19, 83.05 >>, TRUE)
					
					//Cleanup,Create & init cams and make it look at the first shot
					CREATE_TAXI_OJ_CAM(myTaxiData.camTaxi,<<0,0,0>>, <<0,0,0>>)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_FOLLOW,TRUE)
					
					m_HospCSState = HOSP_CS_CUT_01
					CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = HOSP_CS_CUT_01")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_OJ_TAXI,"CAN_PLAYER_START_CUTSCENE returns false")
			ENDIF
		BREAK
		
		//1st shot, looks at nurse, driver
		CASE HOSP_CS_CUT_01
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 3.0
				//Husband
				IF NOT IS_PED_INJURED( myEscapeeStruct.piPed )
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myEscapeeStruct.piPed, TRUE)
					
					CLEAR_PED_TASKS(myEscapeeStruct.piPed)
				
				
					IF IS_VEHICLE_DRIVEABLE(myEscapeeStruct.viCar)
						
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						OPEN_SEQUENCE_TASK(siEscapeeSequenceID)
							TASK_ENTER_VEHICLE( NULL, myEscapeeStruct.viCar, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK )
							TASK_PLAY_ANIM(NULL, "veh@truck@ds@base", "start_engine")
						CLOSE_SEQUENCE_TASK(siEscapeeSequenceID)
						TASK_PERFORM_SEQUENCE(myEscapeeStruct.piPed, siEscapeeSequenceID)
						
						//SET_VEHICLE_ENGINE_ON( myEscapeeStruct.viCar, FALSE, TRUE )		
						SET_VEHICLE_DOORS_LOCKED(myEscapeeStruct.viCar, VEHICLELOCK_UNLOCKED )
						
					ENDIF
				ENDIF
				
	
				//Warp Taxi To Make Sure It's Where It Needs To Be
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(),myTaxiData.vTaxiOJDropoff)//<< 430.6897, -1389.0588, 28.3235 >>)
				
				SET_CAM_COORD(myTaxiData.camTaxi, GET_TAXI_OJ_FTC_HOSPITAL_CS_CAM_POS(0))
				SET_CAM_ROT(myTaxiData.camTaxi, GET_TAXI_OJ_FTC_HOSPITAL_CS_CAM_ROT(0))
				//POINT_CAM_AT_ENTITY(myTaxiData.camTaxi, myEscapeeStruct.viCar, <<0,0,0>>)
				SET_CAM_FOV(myTaxiData.camTaxi, 17.2)
				SHAKE_CAM(myTaxiData.camTaxi, "HAND_SHAKE", 0.1)
				SET_CAM_ACTIVE(myTaxiData.camTaxi,TRUE)
				
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				

				TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)			
				m_HospCSState = HOSP_CS_TRIGGER_INTERP_01
				CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = HOSP_CS_TRIGGER_INTERP_01")
			ENDIF
		BREAK
		CASE HOSP_CS_TRIGGER_INTERP_01
		
			//pedCSDummy = CREATE_PED(PEDTYPE_CIVMALE,mCasualty,<< 403.4721, -1404.2327, 28.4902 >>)				
			
			//IF NOT IS_ENTITY_DEAD(pedCSDummy)
				//TASK_FOLLOW_NAV_MESH_TO_COORD(pedCSDummy,<< 426.5035, -1422.9502, 28.2920 >>,PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT)
			//ENDIF
			
			//Make it and turn it on
			ciTransCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>)
			
			SET_CAM_COORD(ciTransCam, GET_TAXI_OJ_FTC_HOSPITAL_CS_CAM_POS(4))
			SET_CAM_ROT(ciTransCam, GET_TAXI_OJ_FTC_HOSPITAL_CS_CAM_ROT(0))
			//POINT_CAM_AT_ENTITY(ciTransCam, myEscapeeStruct.viCar, <<0,0,0>>)
			SET_CAM_FOV(ciTransCam, 17.2)
			SHAKE_CAM(ciTransCam, "HAND_SHAKE", 0.1)
			SET_CAM_ACTIVE_WITH_INTERP(ciTransCam, myTaxiData.camTaxi , 6000)
			
			TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)			
			m_HospCSState = HOSP_CS_CUT_02
			CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = HOSP_CS_CUT_02")
		BREAK
		
		CASE HOSP_CS_CUT_02
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 7.85
			AND NOT bStartCar
				//Setup Dropoff Blip at the right time which is when the god text plays
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
					CONFIGURE_DRIVE_OFF()
				ELSE						
					//SET_VEHICLE_ENGINE_ON( myEscapeeStruct.viCar, FALSE, TRUE )
				ENDIF
				
				bStartCar = TRUE
			ENDIF
			
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 8.0
			AND bStartCar
				
				SET_CAM_COORD(myTaxiData.camTaxi, GET_TAXI_OJ_FTC_HOSPITAL_CS_CAM_POS(2))
				//SET_CAM_ROT(myTaxiData.camTaxi, GET_TAXI_OJ_FTC_HOSPITAL_CS_CAM_ROT(2))
				POINT_CAM_AT_ENTITY(myTaxiData.camTaxi, myEscapeeStruct.viCar, <<0,0,0>>)
				SET_CAM_FOV(myTaxiData.camTaxi, 35.0)
				SHAKE_CAM(myTaxiData.camTaxi, "HAND_SHAKE", 0.2)
				SET_CAM_ACTIVE(myTaxiData.camTaxi,TRUE)
				
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)			
				m_HospCSState = HOSP_CS_TRIGGER_INTERP_02
				CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = HOSP_CS_TRIGGER_INTERP_02")
			ENDIF
		BREAK
		
		CASE HOSP_CS_TRIGGER_INTERP_02
			//IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 0.25
				SET_CAM_COORD(ciTransCam,GET_TAXI_OJ_FTC_HOSPITAL_CS_CAM_POS(2))
				//SET_CAM_ROT(ciTransCam,GET_TAXI_OJ_FTC_HOSPITAL_CS_CAM_ROT(5))
				POINT_CAM_AT_ENTITY(ciTransCam, myEscapeeStruct.viCar, <<0,0,0>>)
				SET_CAM_FOV(ciTransCam, 45.0)
				SHAKE_CAM(ciTransCam, "HAND_SHAKE", 0.2)
				SET_CAM_ACTIVE_WITH_INTERP(ciTransCam, myTaxiData.camTaxi, 10000)
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)			
				m_HospCSState = HOSP_CS_CUT_END
				CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = HOSP_CS_CUT_END")
			//ENDIF
		BREAK
		
		CASE HOSP_CS_CUT_SKIP

			IF NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed) AND NOT IS_ENTITY_DEAD(myEscapeeStruct.viCar)
				
				IF NOT IS_PED_IN_ANY_VEHICLE(myEscapeeStruct.piPed)
					SET_PED_INTO_VEHICLE(myEscapeeStruct.piPed, myEscapeeStruct.viCar)
				ENDIF			
						
				SET_VEHICLE_FORWARD_SPEED(myEscapeeStruct.viCar, 10)
			
				RESTART_TIMER_NOW(myEscapeeStruct.closeTimer)
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE,2.0)	
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"HOSP_CS_CUT_END")
				m_HospCSState = HOSP_CS_CUT_END
				
				IF NOT bHusbnadDrvSet
					CONFIGURE_DRIVE_OFF()
				ENDIF
				
			ENDIF
		BREAK
		
		CASE HOSP_CS_CUT_END
			
			IF (GET_GAME_TIMER() % 1000) < 50
				CDEBUG1LN(DEBUG_OJ_TAXI,"Opening gate")
			ENDIF
			
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<413.80, -1416.19, 29.26>>, 5.0, PROP_SEC_BARRIER_LD_02A)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_SEC_BARRIER_LD_02A, <<413.80, -1416.19, 29.26>>, FALSE, 0.85)
			ENDIF
			
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 4.0
				
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				//Give player obj
				//SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_FTC2,TRUE)
				
				RESTART_TIMER_NOW(myEscapeeStruct.closeTimer)
				
				DESTROY_CAM(myTaxiData.camTaxi)
				DESTROY_CAM(ciTransCam)
				ODDJOB_EXIT_CUTSCENE()
				
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
				
				TAXI_CANCEL_TIMERS(myTaxiData, TT_CUTSCENE)			
				m_HospCSState = HOSP_CS_CUT_CLEANUP
				CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = HOSP_CS_CUT_CLEANUP")
			
			ELIF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 3.7
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					IF NOT bPulseOut1
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bPulseOut1 = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE HOSP_CS_CUT_CLEANUP
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC LOAD_PATH_NODES_FOR_NURSE_CAR_THIS_FRAME()
	
	VECTOR vStartNode, vEndNode
	
	vStartNode = << 402.42, -1457.19, 28.82 >>  // coord provided by assert in 1300405
	vEndNode  = << -701.25, -1080.29, 12.29 >>  // coord provided by assert in 1300405
	
	// Set the area's X values
	IF vEndNode.X <= vStartNode.X
		vPathNodeMin.X = vEndNode.X
		vPathNodeMax.X = vStartNode.X
	ELSE
		vPathNodeMin.X = vStartNode.X
		vPathNodeMax.X = vEndNode.X
	ENDIF
	
	// Set the area's X values
	IF vEndNode.Y <= vStartNode.Y
		vPathNodeMin.Y = vEndNode.Y
		vPathNodeMax.Y = vStartNode.Y
	ELSE
		vPathNodeMin.Y = vStartNode.Y
		vPathNodeMax.Y = vEndNode.Y
	ENDIF
	
	// Extra leeway around the positions
	vPathNodeMin -= << 20, 20, 20 >>
	vPathNodeMax += << 20, 20, 20 >>
	
	//CDEBUG1LN(DEBUG_OJ_TAXI, " LOAD_PATH_NODES_IN_AREA - requested for - vMin : ", vPathNodeMin, " vMax : ", vPathNodeMax)
	
	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPathNodeMin.x, vPathNodeMin.y, vPathNodeMax.x, vPathNodeMax.y)
	
ENDPROC

/*
ooooooooo  ooooo      o      ooooo         ooooooo     ooooooo8  
 888    88o 888      888      888        o888   888o o888    88  
 888    888 888     8  88     888        888     888 888    oooo 
 888    888 888    8oooo88    888      o 888o   o888 888o    88  
o888ooo88  o888o o88o  o888o o888ooooo88   88ooo88    888ooo888  
*/
PROC TRIGGER_TAXI_QUEUE_FC_LINES()
	
	SET_TAXI_OJ_INTERRUPT_TIMER_OFF(myTaxiData)
	
	//Trigger lines
	IF IS_BANTER_SAFE_TO_PLAY(myTaxiData,tTaxiOJ_DQ_Data)
		
		SWITCH tTaxiOJ_DQ_Data.iCurrentDQLine
			
			//Make sure if player interrupts the initial line, that the objective prints anyway.
			CASE 0
				IF myTaxiData.tTaxiOJ_RideState =  TRS_WAIT_1ST_STOP
					
					IF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_FTC1")
						OR DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
							CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO Hospital has been  assigned. Dialogue Q = ",tTaxiOJ_DQ_Data.iCurrentDQLine )
							tTaxiOJ_DQ_Data.iCurrentDQLine++
							
						ELSE
							IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_OBJ_GIVE_MAIN
							
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE,FALSE,TRUE)
								CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO Hospital has been REassigned")

							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			BREAK
			
			//Trigger Line #1 -------------------------------------------------
			
			CASE 1
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER
				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
				
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER,TRUE)
					
					//Start the mission timer here
					TAXI_START_TIMER(myTaxiData, TT_RIDETODEST)
				ENDIF
			BREAK
			
			//Trigger Line #2--------------------------------------------------
			
//			CASE 2
//				IF myTaxiData.tTaxiOJ_RideState = TRS_CHASE_DRIVER
//					IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_FOLLOW_CAR_BARKS
//					AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_2
//					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
//						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_2,TRUE)
//						IF g_bDebug
//							SCRIPT_ASSERT("Triggering Banter 2")
//						ENDIF
//						
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 3
//				IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_FOLLOW_CAR_BARKS
//				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_3
//				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					IF IS_VEHICLE_DRIVEABLE(myEscapeeStruct.viCar)
//						//IF GET_ENTITY_DISTANCE_FROM_LOCATION(myEscapeeStruct.viCar, << 173.3356, -1018.9367, 28.3714 >>) < 10
//						IF GET_ENTITY_DISTANCE_FROM_LOCATION(myEscapeeStruct.viCar, <<-97.918594,-1287.412354,28.338017>>) < 10
//						OR GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 20.0 //40.0
//				
//							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_3,TRUE)
//							IF g_bDebug
//								SCRIPT_ASSERT("Triggering Banter 3")
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//		
//			CASE 4
//				IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_FOLLOW_CAR_BARKS
//				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_5  // switching the banters up since the route is shorter
//				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					IF IS_VEHICLE_DRIVEABLE(myEscapeeStruct.viCar)
//						IF GET_ENTITY_DISTANCE_FROM_LOCATION(myEscapeeStruct.viCar, <<-466.446320,-1093.321655,26.419662>>) < 10
//						OR GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 20.0 //35.0	
//							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_5,TRUE)
//							CDEBUG1LN(DEBUG_OJ_TAXI,"Queue Open for Follow Car Banter # 5")
//							
//							IF g_bDebug
//								SCRIPT_ASSERT("Triggering Banter 5")
//							ENDIF
//						ENDIF
//					ENDIF	
//				ENDIF
//			BREAK
//			
//			CASE 5
//				IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_FOLLOW_CAR_BARKS
//				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_4
//					IF IS_VEHICLE_DRIVEABLE(myEscapeeStruct.viCar)
//						IF GET_ENTITY_DISTANCE_FROM_LOCATION(myEscapeeStruct.viCar, << -628.6014, -459.8645, 33.7821 >>) < 10
//							
//							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_4,TRUE)
//							CDEBUG1LN(DEBUG_OJ_TAXI,"Queue Open for Follow Car Banter # 4")
//							
//							IF g_bDebug
//								SCRIPT_ASSERT("Triggering Banter 4")
//							ENDIF
//						ENDIF
//					ENDIF	
//				ENDIF
//			BREAK
//			
//			CASE 6
//				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > GET_RANDOM_FLOAT_IN_RANGE(4.0,10.0)
//					//Set Radio Station Check
//					IF NOT GET_TAXI_RADIO_CHECK_FLAG(myTaxiData)	
//						TAXI_RADIO_STATION_TURN_ON(myTaxiData)
//						IF g_bDebug
//							SCRIPT_ASSERT("The Taxi Radio Should Be Updating")
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
		ENDSWITCH				
	ENDIF				
	
	PROCESS_IMPORTANT_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, g_bDebug)

ENDPROC


/*			
 oooooooo8 ooooo ooooo  ooooooo  oooo     oooo ooooooooo     ooooooo  oooo     oooo oooo   oooo 
888         888   888 o888   888o 88   88  88   888    88o o888   888o 88   88  88   8888o  88  
 888oooooo  888ooo888 888     888  88 888 88    888    888 888     888  88 888 88    88 888o88  
        888 888   888 888o   o888   888 888     888    888 888o   o888   888 888     88   8888  
o88oooo888 o888o o888o  88ooo88      8   8     o888ooo88     88ooo88      8   8     o88o    88  
*/
			
PROC UPDATE_FINAL_SCENE()
	
	CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("UPDATE_FINAL_SCENE()", iDebugThrottle)
	
	IF myTaxiData.tTaxiOJ_RideState >= TRS_SPECIAL_ENDING 
	AND NOT bTaxiOJ_GYN_IsFianceDead
	AND (  HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myTaxiData.piTaxiPassenger, PLAYER_PED_ID()) 
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piWhore, PLAYER_PED_ID()) 
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myEscapeeStruct.piPed, PLAYER_PED_ID())) 		
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_FINAL_SCENE: PLAYER HAS DISRUPTED THE SCENE BY DAMAGING ONE OF THE PEDS")
		
		bTaxiOJ_GYN_IsFianceDead = TRUE
	ELIF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
	
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myEscapeeStruct.piPed, myTaxiData.piTaxiPassenger) 
		AND finalState < FINAL_SCENE_KILL_MAN
			CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_FINAL_SCENE: Passenger killed fiance")
			
			bTaxiOJ_GYN_IsFianceDead = TRUE		
		ENDIF
	ENDIF
	
	IF finalState >= FINAL_SCENE_KILL_WOMAN
		#IF IS_DEBUG_BUILD
			sDebugPlayerDistance = "Distance = "
			sDebugPlayerDistance += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger))
			DRAW_DEBUG_TEXT_2D(sDebugPlayerDistance, <<0.75, 0.75, 0.0>>)
		#ENDIF
		
		IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) < 3.0 //5.0
		AND NOT bTaxiOJ_GYN_IsPlayerTooClose
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CDEBUG1LN(DEBUG_OJ_TAXI,"PLAYER HAS DISRUPTED THE SCENE BY GETTING TOO CLOSE TO THE PASSENGER")
				//SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_AGGRO,TRUE)
				
				// setting this here since this would be the last line anyway
				CREATE_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, "txm4_aggro", CONV_PRIORITY_HIGH)
				
				TASK_AIM_GUN_AT_ENTITY(myTaxiData.piTaxiPassenger, PLAYER_PED_ID(), -1 )
				
				iFinalFailDelay = GET_GAME_TIMER()
				
				bTaxiOJ_GYN_IsPlayerTooClose = TRUE
			ELSE
				IF NOT bKillFinalConversation
					KILL_ANY_CONVERSATION()
					bKillFinalConversation = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH finalState
		CASE FINAL_SCENE_ARRIVE
			CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_FINAL_SCENE - FINAL_SCENE_ARRIVES")
			IF NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed)
			AND NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger) 
			AND NOT IS_ENTITY_DEAD(piWhore) 
				ADD_RELATIONSHIP_GROUP("TAXI_Whore", rghWhore)
				rghPassenger = GET_PED_RELATIONSHIP_GROUP_HASH( myTaxiData.piTaxiPassenger )
				
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, myEscapeeStruct.relEscapee, rghWhore )
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, myEscapeeStruct.relEscapee, rghPassenger )
				
				SET_PED_RELATIONSHIP_GROUP_HASH(myEscapeeStruct.piPed, myEscapeeStruct.relEscapee)
				SET_PED_RELATIONSHIP_GROUP_HASH(piWhore, rghWhore)
				
				//Husband
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myEscapeeStruct.piPed, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(myEscapeeStruct.piPed, CA_ALWAYS_FLEE, TRUE)
				SET_ENTITY_HEALTH(myEscapeeStruct.piPed, 130)
				SET_PED_SUFFERS_CRITICAL_HITS( myEscapeeStruct.piPed, TRUE)
				SET_PED_DIES_WHEN_INJURED( myEscapeeStruct.piPed, TRUE)
				CLEAR_PED_TASKS(myEscapeeStruct.piPed)
				
				//Whore
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piWhore, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(piWhore, CA_ALWAYS_FLEE, TRUE)
				SET_ENTITY_HEALTH(piWhore, 130)
				SET_PED_SUFFERS_CRITICAL_HITS( piWhore, TRUE)
				SET_PED_DIES_WHEN_INJURED( piWhore, TRUE)
				CLEAR_PED_TASKS(piWhore)

				finalState = FINAL_SCENE_TAKE_PLACES
			ENDIF
			
		BREAK
		
		CASE FINAL_SCENE_TAKE_PLACES
			CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_FINAL_SCENE - FINAL_SCENE_TAKE_PLACES")
			IF NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed) AND NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger) 
			AND NOT IS_ENTITY_DEAD(piWhore) AND NOT IS_ENTITY_DEAD(myEscapeeStruct.viCar)
				
				//Husband
				CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
				OPEN_SEQUENCE_TASK(siEscapeeSequenceID)
					//TASK_LEAVE_VEHICLE(NULL,myEscapeeStruct.viCar)
					//TASK_GO_STRAIGHT_TO_COORD(NULL, << -878.8551, -14.6230, 41.6086 >>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP)
					//TASK_GO_TO_ENTITY(NULL, piWhore, DEFAULT_TIME_BEFORE_WARP, 1.0, PEDMOVEBLENDRATIO_WALK)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, piWhore)
					//TASK_CHAT_TO_PED(NULL, piWhore, CF_AUTO_CHAT, << -880.3378, -14.1614, 41.7429 >>, 87.8475, -1)
					TASK_PLAY_ANIM(NULL, sNurseChatAnim, "idle_b")//, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					TASK_PLAY_ANIM(NULL, sNurseChatExit, "exit")
				CLOSE_SEQUENCE_TASK(siEscapeeSequenceID)
				TASK_PERFORM_SEQUENCE(myEscapeeStruct.piPed, siEscapeeSequenceID)
				CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)

				//Whore
				CLEAR_SEQUENCE_TASK(siWhoreSequenceID)
				OPEN_SEQUENCE_TASK(siWhoreSequenceID)
					//TASK_GO_STRAIGHT_TO_COORD(NULL,  << -879.7616, -13.6659, 41.7242 >>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, 271.4075  ) 
					//TASK_GO_TO_ENTITY(NULL, myEscapeeStruct.piPed, DEFAULT_TIME_BEFORE_WARP, 1.0, PEDMOVEBLENDRATIO_WALK)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, myEscapeeStruct.piPed)
					//TASK_CHAT_TO_PED(NULL, myEscapeeStruct.piPed, CF_AUTO_CHAT, << -880.9371, -13.3114, 41.7583 >>, 267.8475, -1)
					TASK_PLAY_ANIM(NULL, sMistressChatAnim, "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					//TASK_STAND_STILL(NULL, -1)
				CLOSE_SEQUENCE_TASK(siWhoreSequenceID)
				TASK_PERFORM_SEQUENCE(piWhore, siWhoreSequenceID)
				CLEAR_SEQUENCE_TASK(siWhoreSequenceID)
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_FINAL_SCENE - Moving to FINAL_SCENE_PASSENGER_POUNCE")
				finalState = FINAL_SCENE_PASSENGER_POUNCE
			ENDIF
		BREAK
		
		
		//Hang out here til the player arrives at the destination
		CASE FINAL_SCENE_PASSENGER_POUNCE
			IF bTaxiOJ_GYN_IsPassengerAtConfrontationPt
				//Passenger
				IF NOT IS_ENTITY_DEAD(piWhore) AND NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
//				AND ((GET_SCRIPT_TASK_STATUS(myEscapeeStruct.piPed, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK AND GET_SEQUENCE_PROGRESS(myEscapeeStruct.piPed) > 1)
//					OR GET_SCRIPT_TASK_STATUS(myEscapeeStruct.piPed, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK)
					
					SET_PED_RELATIONSHIP_GROUP_HASH(myTaxiData.piTaxiPassenger, rghPassenger)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiData.piTaxiPassenger, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_AGGRESSIVE, TRUE)
					SET_PED_SHOOT_RATE( myTaxiData.piTaxiPassenger, 100 )
					SET_PED_ACCURACY(myTaxiData.piTaxiPassenger, 100 )
					SET_ENTITY_HEALTH(myTaxiData.piTaxiPassenger, 105)
					CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
					
					
					CLEAR_SEQUENCE_TASK(siPassengerSequenceID)
					OPEN_SEQUENCE_TASK(siPassengerSequenceID)
						TASK_STAND_STILL(NULL, 5000)
						LEAVE_TAXI_OJ_FROM_CLOSEST_DOOR_TO_POINT_AS_SEQUENCE(myTaxiData,<<-691.48, -1113.31, 13.53>>)//<< -874.0767, -4.8782, 42.0797 >>)
						//TASK_GO_STRAIGHT_TO_COORD(NULL, GET_PASSENGER_WALK_TO_POINT(eFCDropZone), PEDMOVEBLENDRATIO_SPRINT)
						//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_PASSENGER_WALK_TO_POINT(eFCDropZone), PEDMOVEBLENDRATIO_SPRINT)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, GET_PASSENGER_WALK_TO_POINT(eFCDropZone), piWhore, PEDMOVEBLENDRATIO_SPRINT, TRUE)
						TASK_SHOOT_AT_ENTITY(NULL, piWhore, -1, FIRING_TYPE_CONTINUOUS )
						//TASK_AIM_GUN_AT_ENTITY(NULL,piWhore,-1)
					CLOSE_SEQUENCE_TASK(siPassengerSequenceID)
					TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siPassengerSequenceID)
					CLEAR_SEQUENCE_TASK(siPassengerSequenceID)
					
//					IF NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed)
//						TASK_PLAY_ANIM(myEscapeeStruct.piPed, sNurseChatExit, "exit")
//					ENDIF
					
					bPlayerDisabled = TRUE
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					
					GIVE_WEAPON_TO_PED(myTaxiData.piTaxiPassenger, WEAPONTYPE_PISTOL, 100, TRUE, TRUE)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_FINAL_SCENE - Moving to FINAL_SCENE_WAIT_FOR_DIALOGUE")
					finalState = FINAL_SCENE_WAIT_FOR_DIALOGUE
				ENDIF
			ELSE
				IF NOT bCheckPlayerEndInterruption
					IF NOT IS_PED_INJURED(myEscapeeStruct.piPed)
					AND NOT IS_PED_INJURED(piWhore)
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(myEscapeeStruct.piPed) < 2.0
						OR GET_PLAYER_DISTANCE_FROM_ENTITY(piWhore) < 2.0
							CLEAR_PED_TASKS(myEscapeeStruct.piPed)
							CLEAR_PED_TASKS(piWhore)
							
							TASK_LOOK_AT_ENTITY(myEscapeeStruct.piPed, PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
		
							CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
							OPEN_SEQUENCE_TASK(siEscapeeSequenceID)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(siEscapeeSequenceID)
							TASK_PERFORM_SEQUENCE(myEscapeeStruct.piPed, siEscapeeSequenceID)
							
							TASK_LOOK_AT_ENTITY(piWhore, PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
		
							CLEAR_SEQUENCE_TASK(siWhoreSequenceID)
							OPEN_SEQUENCE_TASK(siWhoreSequenceID)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(siWhoreSequenceID)
							TASK_PERFORM_SEQUENCE(piWhore, siWhoreSequenceID)
							
							bCheckPlayerEndInterruption = TRUE
						ENDIF
					ELSE
						IF IS_PED_INJURED(myEscapeeStruct.piPed)
							CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_FINAL_SCENE - nurse was injured")
						ELSE
							CLEAR_PED_TASKS(myEscapeeStruct.piPed)
							TASK_SMART_FLEE_PED(myEscapeeStruct.piPed, PLAYER_PED_ID(), 100, 20000)
						ENDIF
						
						IF IS_PED_INJURED(piWhore)
							CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_FINAL_SCENE - mistress was injured")
						ELSE
							CLEAR_PED_TASKS(piWhore)
							TASK_SMART_FLEE_PED(piWhore, PLAYER_PED_ID(), 100, 20000)
						ENDIF
						
						// Fail here is taken care of by MONITOR_TAXI_ESCAPEE
					ENDIF
				ENDIF
			ENDIF
				
		BREAK
		
		
		CASE FINAL_SCENE_WAIT_FOR_DIALOGUE
			CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("UPDATE_FINAL_SCENE - FINAL_SCENE_WAIT_FOR_DIALOGUE", iDebugThrottle)
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
				IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
					IF GET_SEQUENCE_PROGRESS(myTaxiData.piTaxiPassenger) > 0 
					AND bPlayerDisabled 
						
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						bPlayerDisabled = FALSE
					ENDIF
				ENDIF
			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			
//			IF (NOT IS_PED_INJURED(piWhore) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(piWhore))
//			AND iShootStartTime = 0
//				iShootStartTime = GET_GAME_TIMER()
//			ENDIF
			
			//IF (iShootStartTime > 0 AND (GET_GAME_TIMER() - iShootStartTime) > 1000)
			IF (NOT IS_PED_INJURED(piWhore) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(piWhore))
			AND NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed)
			AND NOT bFianceReacted
				//Task fiance to face
				TASK_LOOK_AT_ENTITY(myEscapeeStruct.piPed, myTaxiData.piTaxiPassenger, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
		
		
				CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
				OPEN_SEQUENCE_TASK(siEscapeeSequenceID)
					//TASK_PLAY_ANIM(NULL, sNurseChatExit, "exit")
					TASK_STAND_STILL(NULL, 500)
					TASK_PLAY_ANIM(NULL, "MOVE_DUCK_FOR_COVER", "enter")
					TASK_PLAY_ANIM(NULL, "MOVE_DUCK_FOR_COVER", "loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiData.piTaxiPassenger)
				CLOSE_SEQUENCE_TASK(siEscapeeSequenceID)
				TASK_PERFORM_SEQUENCE(myEscapeeStruct.piPed, siEscapeeSequenceID)
				
				bFianceReacted = TRUE
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
			AND NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed)
				IF GET_SEQUENCE_PROGRESS(myTaxiData.piTaxiPassenger) > 2
				//AND NOT IS_ENTITY_DEAD(piWhore)
					
					//Task whore to face
//						CLEAR_SEQUENCE_TASK(siWhoreSequenceID)
//						OPEN_SEQUENCE_TASK(siWhoreSequenceID)
//							TASK_PLAY_ANIM(NULL, sMistressChatExit, "exit")
//							TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiData.piTaxiPassenger)
//						CLOSE_SEQUENCE_TASK(siWhoreSequenceID)
//						TASK_PERFORM_SEQUENCE(piWhore, siWhoreSequenceID)
					
					//play fuck you dialouge
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_ENEMY_KILL,TRUE)
					
					//Task passenger
					CLEAR_SEQUENCE_TASK(siPassengerSequenceID)
					OPEN_SEQUENCE_TASK(siPassengerSequenceID)
						TASK_CLEAR_LOOK_AT(NULL)
						TASK_AIM_GUN_AT_ENTITY(NULL,myEscapeeStruct.piPed,2000)
						//TASK_SHOOT_AT_ENTITY(NULL, piWhore, -1, FIRING_TYPE_CONTINUOUS )
					CLOSE_SEQUENCE_TASK(siPassengerSequenceID)
					TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siPassengerSequenceID)
					
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_FINAL_SCENE - Moving to FINAL_SCENE_KILL_WOMAN")
					
					finalState = FINAL_SCENE_KILL_WOMAN
				ENDIF
			ENDIF
		BREAK
		
		CASE FINAL_SCENE_KILL_WOMAN
			CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("FINAL_SCENE_KILL_WOMAN", iDebugThrottle)
			
			IF IS_ENTITY_DEAD(piWhore)
				IF NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed)
					
					//Task passenger to move toward fiance
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
						CLEAR_SEQUENCE_TASK(siWhoreSequenceID)
						OPEN_SEQUENCE_TASK(siWhoreSequenceID)
						
							//TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL,<< -877.9511, -12.1408, 41.7063 >>,myEscapeeStruct.piPed,PEDMOVEBLENDRATIO_WALK,FALSE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL,<<-691.48, -1113.31, 13.53>>,myEscapeeStruct.piPed,PEDMOVEBLENDRATIO_WALK,FALSE)
							TASK_AIM_GUN_AT_ENTITY(NULL,myEscapeeStruct.piPed, -1,TRUE)
							
						CLOSE_SEQUENCE_TASK(siWhoreSequenceID)
						TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siWhoreSequenceID)
						
					ENDIF
				ENDIF
				CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_FINAL_SCENE - Moving to FINAL_SCENE_KILL_MAN")
				finalState = FINAL_SCENE_KILL_MAN
			ENDIF
			
		BREAK
		
		CASE FINAL_SCENE_KILL_MAN
			CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("FINAL_SCENE_KILL_MAN", iDebugThrottle)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
				AND NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed)
				
					TASK_SHOOT_AT_ENTITY(myTaxiData.piTaxiPassenger, myEscapeeStruct.piPed, -1, FIRING_TYPE_CONTINUOUS )
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_FINAL_SCENE - Moving to FINAL_SCENE_FLEE")
					finalState = FINAL_SCENE_FLEE
				ENDIF
			ENDIF
		BREAK
		CASE FINAL_SCENE_FLEE
			CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("FINAL_SCENE_FLEE", iDebugThrottle)
			
			IF IS_ENTITY_DEAD(myEscapeeStruct.piPed) AND NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
				
				VECTOR vDeadHusband
				vDeadHusband = GET_ENTITY_COORDS(myEscapeeStruct.piPed, FALSE)
				
				OPEN_SEQUENCE_TASK(siPassengerSequenceID)
					TASK_AIM_GUN_AT_COORD(NULL, vDeadHusband, 1000)
					TASK_SWAP_WEAPON(NULL, FALSE)
					TASK_STAND_STILL(NULL, 500)
					//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-705.40240, -1120.21875, 13.52498>>, PEDMOVEBLENDRATIO_SPRINT)
					TASK_SMART_FLEE_COORD(NULL, <<-691.48, -1113.31, 13.53>>, 1000, -1)
				CLOSE_SEQUENCE_TASK(siPassengerSequenceID)
				TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siPassengerSequenceID)
				CLEAR_SEQUENCE_TASK(siPassengerSequenceID)
				
				SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger, TRUE)
				bTaxiOJ_GYN_IsFianceDead = TRUE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/*

ooooooooooo ooooo      o      oooo   oooo  oooooooo8 ooooooooooo  		ooooooooooo oooooooooo ooooo  oooo  oooooooo8 oooo   oooo 		      o      ooooo 
 888         888      888      8888o  88 o888     88  888    88   		88  888  88  888    888 888    88 o888     88  888  o88   		     888      888  
 888ooo8     888     8  88     88 888o88 888          888ooo8     		    888      888oooo88  888    88 888          888888     		    8  88     888  
 888         888    8oooo88    88   8888 888o     oo  888    oo   		    888      888  88o   888    88 888o     oo  888  88o   		   8oooo88    888  
o888o       o888o o88o  o888o o88o    88  888oooo88  o888ooo8888  		   o888o    o888o  88o8  888oo88   888oooo88  o888o o888o 		 o88o  o888o o888o 
   
*/
///// PURPOSE: This checks the fiance's truck for all types of aggro and fail cases along with handling his tasks to follow waypoints and when he's stuck behind a vehicle
  ///    
FUNC BOOL HANDLE_TAXIOJ_AI_FIANCE_TRUCK_DRIVING()

	//Give some time for obj to print
	IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DELAY) > 2.0
		//Monitors your follow/chase status
		IF NOT myTaxiData.bIsTaxiDebugSkipping
			#IF IS_DEBUG_BUILD
				TAXI_AI_WATCH_FOLLOW_STATUS(myTaxiData,myEscapeeStruct, ( (GET_GAME_TIMER() - iTimeOfEscapeeStart) > 15000 ),bDebugDrawZones )
			#ENDIF
			
			#IF IS_FINAL_BUILD
				TAXI_AI_WATCH_FOLLOW_STATUS(myTaxiData,myEscapeeStruct, ( (GET_GAME_TIMER() - iTimeOfEscapeeStart) > 15000 ))
			#ENDIF
		ENDIF
	ENDIF				
		
	IF IS_VEHICLE_DRIVEABLE(myEscapeeStruct.viCar)

		IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)		
			IF IS_ENTITY_IN_ANGLED_AREA(myTaxiData.viTaxi, vLocate_fc_final_cs_triggerPos1, vLocate_fc_final_cs_triggerPos2, fLocate_fc_final_cs_triggerWidth)
				BRING_VEHICLE_TO_HALT(myTaxiData.viTaxi, 5.0, 1)					
				RETURN TRUE
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_SPHERE(vTaxiOJ_FC_CS02_FianceWarpPt1, 5.0, 0, 0, 255, 120)
			//DRAW_DEBUG_SPHERE(vTaxiOJ_FC_C02_WarpPt_Player1, 5.0, 255, 0, 0, 120)
		#ENDIF
				
		//TOGGLE_NODES_ALONG_WAYPOINT_RECORDING_PATH_ON_PROGRESS(myTaxiData,sNurseCarRecording,myEscapeeStruct.viCar,iRoadNodeMachineIndex,iNumRoadNodes, iWayPtProgress,50,15)
																																  
		//Override the cinematic cam
		CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, myEscapeeStruct.viCar)
				
		UPDATE_OJ_TAXI_MILEAGE(myTaxiData,myTaxiData.fTaxiOJ_TempFloat)
		
		//FIANCE LEAVES HOSPITAL
		//Task to waypoint recording once vehicle recording to exit hospital is done-----------------------------------------
		IF NOT bTransititonToWaypointRec	
			IF NOT IS_ENTITY_DEAD(myEscapeeStruct.piPed)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(myEscapeeStruct.viCar)
				AND (GET_GAME_TIMER() - iTimeOfEscapeeStart) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(TAXI_CONST_VR_FIANCE_EXIT_HOSPITAL,"txm_fc_h1_")
					CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXIOJ_AI_FIANCE_TRUCK_DRIVING: bTransititonToWaypointRec ?")

					TASK_VEHICLE_DRIVE_TO_COORD(myEscapeeStruct.piPed, myEscapeeStruct.viCar, vTaxiOJ_FC_CS02_FianceWarpPt1, 28.0, DRIVINGSTYLE_NORMAL, myEscapeeStruct.escapeeCarModel, tFianceDrivingMode, 13.75, -1.0)
										
					bTransititonToWaypointRec = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
/*

oooo     oooo      o      ooooo oooo   oooo 
 8888o   888      888      888   8888o  88  
 88 888o8 88     8  88     888   88 888o88  
 88  888  88    8oooo88    888   88   8888  
o88o  8  o88o o88o  o888o o888o o88o    88  
                                           
*/
/// PURPOSE: The almighty assembly line. This is where the magic happens
///    
PROC Main_Taxi_OJ_FollowCar()
	
		
	//Handles Fail Or No Taxi-------------------------------------------------------------------------------
	IF IS_TAXI_JOB_IN_FAIL_STATE(myTaxiData)
		TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
		
		//Cleanup any straggler blips
		IF DOES_BLIP_EXIST(myEscapeeStruct.bBlip)
			REMOVE_BLIP(myEscapeeStruct.bBlip)
		ENDIF
		
		IF TAXI_HANDLE_FAIL(myTaxiData)
			Script_Failed()
		ENDIF
	
	//Proceed
	ELSE
		//Run Throughtout the Entire Mission--------------------------------------------------------------
		TAXI_OJ_MAINTAIN_GLOBAL_GATES(myTaxiData)
		
		//In Follow Car we fake the mission complete a little early, so only run these before TRS_CLEANUP
		if myTaxiData.tTaxiOJ_RideState < TRS_SPECIAL_ENDING
			RUN_GLOBAL_TAXI_UPDATES(myTaxiData,aggroArgs)
			
			PROCESS_TAXI_EXCEPTIONS(myTaxiData)
			
			UPDATE_TAXI_OJ_TIP(myTaxiData,iTipIndex)
		ENDIF
		
		//Dialogue & Objectives
		IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION			
			IF NOT IS_TAXI_EMERGENCY_FAIL_SET(myTaxiData)
				TRIGGER_TAXI_QUEUE_FC_LINES()
			ELSE
				TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",GET_TAXI_EMERGENCY_FAIL_STRING(myTaxiData))
			ENDIF			

		ENDIF
		
		//Update Final Scene-----------------------------------------
		IF myTaxiData.tTaxiOJ_RideState >= TRS_WAIT_FOR_PASSENGER
		AND NOT bTaxiOJ_GYN_IsFianceDead
			UPDATE_FINAL_SCENE() 
		ENDIF
		//--------------------
		
		IF bTaxiSetVehicleMultiplier
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.8)//9)
			//CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("vehicle density set to 0.9 right now", iDebugThrottle)
		ENDIF
		
		//Track Driver progress
		IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
		OR myTaxiData.tTaxiOJ_RideState = TRS_WAIT_1ST_STOP
			
			HANDLE_TAXI_EXCITEMENT(myTaxiData,FALSE)

#IF IS_DEBUG_BUILD
		IF bDebugTurnOnFreeRide
			HANDLE_TAXI_EXCITEMENT(myTaxiData,TRUE,TRUE)
		ENDIF	
#ENDIF
		ENDIF			
				
		//================================================================================================
	
		SWITCH myTaxiData.tTaxiOJ_RideState
/*PRESTREAM and Check that the player and taxi are good to go----------------------------------------
						
ooooo oooo   oooo ooooo ooooooooooo 
 888   8888o  88   888  88  888  88 
 888   88 888o88   888      888     
 888   88   8888   888      888     
o888o o88o    88  o888o    o888o    
*/
			CASE TRS_INIT_STREAM
				
				//Request of our assets
				REQUEST_TAXI_ODDJOB_FC_STREAMS_STAGE_01()
				
				TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
				
				TAXI_SPAWN_PASSENGER(myTaxiData, vPassengerPt, vPassengerPickupPt, "TaxiKeyla",mPassengerModel,41.1334,15.0)
				TAXI_INIT_PASSENGER_BLIP(myTaxiData)
					
					
				//Move on to the next stage
				TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_STREAMING)
				
			BREAK
			
			//Wait for all of assets that we just requested, to finish streaming---------------------------------------------
			CASE TRS_STREAMING

				IF HAVE_TAXI_OJ_FC_STAGE_01_ASSETS_LOADED()
					
					INIT_ALL_TAXI_EXCITEMENT_VALUES()
					
					INITIALIZE_GENERIC_TAXI_EXCEPTIONS()
					
					TAXI_INITIALIZE_BONUS_FIELD(bonusFieldFollowCar[TFC_BONUS_P_EYE], "TAXI_SC_BN_04", TAXI_CONST_CASH_BONUS_PRIVATE_EYE)
					TAXI_INITIALIZE_BONUS_INFO(myTaxiData, bonusFieldFollowCar)
				
					//SET REACT BITS
					CLEAR_ALL_TAXI_PASSENGER_REACT_BITS(myTaxiData)
					
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vHospitalMin, vHospitalMax, FALSE)
					
					myTaxiData.vTaxiOJPickup = vPassengerPt
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPAWNING)
				ENDIF
	
			BREAK
			
			//Once we have a good point, spawn our passenger------------------------------------------------------------------
			CASE TRS_SPAWNING
			
				IF PROPERTY_VIP_INIT_READY(myTaxiData)
					
					
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,2), 1, 2, 0) //(hair)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,4), 1, 3, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
						
//						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_TORSO, 1, 5)
//						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_LEG, 1, 5)
//						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_HAIR, 1, 0)
					ENDIF
					
					//Give player obj to go pickup Passenger
					ENABLE_TAXI_SPEECH(myTaxiData)

					//Clear our all potential relationships
					ADD_RELATIONSHIP_GROUP("TAXI_Escapee", myEscapeeStruct.relEscapee)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, myEscapeeStruct.relEscapee, myTaxiData.relPassenger)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, myEscapeeStruct.relEscapee, RELGROUPHASH_PLAYER)
					
					SET_ROADS_IN_AREA(<< 1346.90, -1606.52, 31.16 >>, << 1457.25, -1508.19, 83.05 >>, FALSE)
															
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
				ENDIF
			BREAK
			
			/*
			LM - Had to change the state machine flow from TRS_MANAGE_PICKUP -> TRS_WAIT_1ST_STOP in order to 
			accomodate for players diving out of the car during pickup.  This fixes B* 323041, because when players
			dove during Following Car, they wouldn't lose control, but the mortuary would get blipped.  I used the
			pre-existing states TRS_SWITCH_JOB and TRS_SEND_TO_STORE to control this flow a little more.
			*/
			
			//Once near the passenger make him get in and assign a dropoff destination-----------------------------------------
			CASE TRS_MANAGE_PICKUP
				IF TAXI_HANDLE_IV_PICKUP_NO_METER(myTaxiData)
				
					REQUEST_TAXI_ODDJOB_FC_STREAMS_STAGE_02()
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_PASSENGER_ENTER)
				ENDIF
				
			BREAK
			
			CASE TRS_PASSENGER_ENTER //placeholder - see above explanation
								
				IF IS_PASSENGER_ENTERING_TAXI(myTaxiData)
					//Greet the player
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GREET,TRUE)					
					
					CLEANUP_TAXI_PICKUP_STOP(myTaxiData)
					
					myTaxiData.vTaxiOJDropoff = << 485.2039, -1418.0643, 28.2112 >> //<< 452.6766, -1403.7572, 28.1917  >>//<< 430.1324, -1389.1927, 28.3329 >>
					
					TAXI_OJ_FC_SET_TIP_AND_EXCITEMENT_TO_CHECK()
					
					TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(<< 392.8545, -1379.5774, 29.2837 >>,FALSE,TAXI_CONST_FC_ROAD_DISABLE_RADIUS)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_1ST_STOP)
				ENDIF
				
				// check to see if player dived, if so send back to prev state
				IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
					IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
						TAXI_HANDLE_PLAYER_DIVE(myTaxiData)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
					ENDIF
				ENDIF
			BREAK

			//This is where you wait for them to arrive at the hospital. This is where i should add the blip check
			CASE TRS_WAIT_1ST_STOP
				TAXI_AI_HANDLE_BLIPS(myTaxiData,myTaxiData.blipTaxiDropOff)
				
				//Handle damageing the husbands car
				IF NOT DOES_ENTITY_EXIST(myEscapeeStruct.viCar)
					IF HAVE_TAXI_OJ_FC_STAGE_02_ASSETS_LOADED()
						SPAWN_ESCAPEE_VEHICLE(myEscapeeStruct, vEnemyCarSpawnPt, fEscapeeSpawnHeading )
					ENDIF
				ELSE
					CHECK_NURSE_OR_HIS_VEHICLE_BEEN_DAMAGED(myTaxiData,myEscapeeStruct)
				ENDIF
				
				
				IF TAXI_HANDLE_DRIVING(myTaxiData,/* tTaxiOJ_DQ_Data,*/ 9)
					IF DOES_BLIP_HAVE_GPS_ROUTE(myTaxiData.blipTaxiDropOff)
						SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, FALSE)
						CDEBUG1LN(DEBUG_OJ_TAXI,"myTaxiData.blipTaxiDropOff - route turned off")
					ENDIF
					
					REMOVE_BLIP( myTaxiData.blipTaxiDropOff )
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CUTSCENE)
				ENDIF
			BREAK
/*Cutscene - At the hospital

  oooooooo8  oooooooo8    oooooooo8 ooooooooooo oooo   oooo ooooooooooo 
o888     88 888         o888     88  888    88   8888o  88   888    88  
888          888oooooo  888          888ooo8     88 888o88   888ooo8    
888o     oo         888 888o     oo  888    oo   88   8888   888    oo  
 888oooo88  o88oooo888   888oooo88  o888ooo8888 o88o    88  o888ooo8888 
			*/
			CASE TRS_CUTSCENE
				IF TAXI_OJ_FTC_CUTSCENE_HOSPITAL()	
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_FTC2,TRUE)
						bTaxiSetVehicleMultiplier = TRUE
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DRIVING_PASSENGER)
					ENDIF
				ENDIF
			BREAK
			
			//Waits for the passenger to get the taxi to then spawn the followee car--------------------------------------------
			CASE TRS_DRIVING_PASSENGER
//				IF NOT IS_BIT_SET(iFCHelpBitField, TAXI_FC_CHASE_CAM_HLP)
//					PRINT_HELP("TX_FC_CHS_CAM")
//					SET_BIT(iFCHelpBitField, TAXI_FC_CHASE_CAM_HLP)
//				ENDIF
	
				CHECK_NURSE_OR_HIS_VEHICLE_BEEN_DAMAGED(myTaxiData,myEscapeeStruct)
				
				LOAD_PATH_NODES_FOR_NURSE_CAR_THIS_FRAME()
				
				IF IS_PED_AND_VEHICLE_VALID(myEscapeeStruct.piPed,myEscapeeStruct.viCar)
				//AND ARE_NODES_LOADED_FOR_AREA(vPathNodeMin.x, vPathNodeMin.y, vPathNodeMax.x, vPathNodeMax.y)
					
					TAXI_CANCEL_TIMERS(myTaxiData, TT_STOPPED)
					
					SET_PED_CAN_BE_SHOT_IN_VEHICLE(myEscapeeStruct.piPed,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(myEscapeeStruct.piPed, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(myEscapeeStruct.piPed, CA_ALWAYS_FLEE, TRUE)
					//Tell the player what to do.
					ENABLE_TAXI_SPEECH(myTaxiData)
					
					myEscapeeStruct.iStartHealth= GET_ENTITY_HEALTH(myEscapeeStruct.viCar)
					myEscapeeStruct.fCurrentEngHealth = GET_VEHICLE_ENGINE_HEALTH(myEscapeeStruct.viCar)
					myEscapeeStruct.fPetrolHealth = GET_VEHICLE_PETROL_TANK_HEALTH(myEscapeeStruct.viCar)
					
					TAXI_RESET_TIMERS(myTaxiData,  TT_COLLIDE,0)
					TAXI_RESET_TIMERS(myTaxiData,  TT_DELAY,0)
					
					SET_PED_NON_CREATION_AREA(vHouseMin, vHouseMax)
					tempScenarioBlockingIndex = ADD_SCENARIO_BLOCKING_AREA(vHouseMin, vHouseMax)
					SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CHASE_DRIVER)
	
				ENDIF
			BREAK

				
/* FOLLOWING CAR

  oooooooo8 ooooo ooooo      o       oooooooo8 ooooooooooo 
o888     88  888   888      888     888         888    88  
888          888ooo888     8  88     888oooooo  888ooo8    
888o     oo  888   888    8oooo88           888 888    oo  
 888oooo88  o888o o888o o88o  o888o o88oooo888 o888ooo8888 	*/

//Follow the car and monitor everything| Check if the player attacks the car also--------------------------------------------

			CASE TRS_CHASE_DRIVER
				//These run even if the player is not in the taxi------------------------------------------------
				
				LOAD_PATH_NODES_FOR_NURSE_CAR_THIS_FRAME()
				
				//Handle player harming the fiance
				CHECK_NURSE_OR_HIS_VEHICLE_BEEN_DAMAGED(myTaxiData,myEscapeeStruct)
				
				//Blip Management
				TAXI_AI_HANDLE_BLIPS(myTaxiData,myEscapeeStruct.bBlip)
				
				//New location: <<-675.1650, -1054.1213, 15.2092>>, 120.9886
				
				
				IF IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
					
					//Handle Going Wanted during this Section--------------------------------------
					IF IS_TAXI_POLICE_READY(myEscapeeStruct.piPed, myEscapeeStruct.bBlip)
						//Set dialogue stuff up
						SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_ESCAPE_POLICE,TRUE)
						//Debug to the next stage
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_ESCAPE_POLICE)
					ENDIF
					
					
					//Run Radio Dialogue
					RUN_TAXIRADIO_UPDATE_WHEN_ENABLED(myTaxiData)
					
					
					//Banter # 1
					TRIGGER_TAXIOJ_BANTER_WHEN_VEHICLE_NEAR_COORD(myEscapeeStruct.viCar,<< 291.0313, -1476.4460, 28.2945 >>,15,tTaxiOJ_DQ_Data,2)
										
					
					//Truck driving handled here
					IF HANDLE_TAXIOJ_AI_FIANCE_TRUCK_DRIVING()
						
						IF IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(myEscapeeStruct.viCar)	
							CDEBUG1LN(DEBUG_OJ_TAXI,"stopping playback")
							STOP_PLAYBACK_RECORDED_VEHICLE(myEscapeeStruct.viCar)
							REMOVE_VEHICLE_RECORDING(0, "taxi_oj_fc3")
						ENDIF
						
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						
						//Fiance hit the cutscene trigger B*486284
						REQUEST_TAXI_ODDJOB_FC_STREAMS_STAGE_END()
						
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CUTSCENE_02)
					ENDIF
					
//					IF IsChaseHintCamButtonPressedInVehicle()
//					AND IS_BIT_SET(iFCHelpBitField, TAXI_FC_CHASE_CAM_HLP)
//					AND iChaseCamTime = 0
//						iChaseCamTime = GET_GAME_TIMER()
//						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TX_FC_CHS_CAM")
//							CLEAR_HELP()
//						ENDIF
//					ENDIF
				ENDIF
			BREAK
			//END OF CHASE CASE-------------------------------------------------------------------------------------------
			
			
			
			//Play cutscene
			CASE TRS_CUTSCENE_02
				IF HAVE_TAXI_OJ_FC_STAGE_END_ASSETS_LOADED()
					IF TAXI_OJ_CS_WHORE_REVEAL()
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REVEAL_DESTINATION)
					ENDIF
				ENDIF
			BREAK
			
			
			
			//Wait for player to drop off passenger, should be a huge volume around the entire area.
			
			CASE TRS_REVEAL_DESTINATION
				UPDATE_OJ_TAXI_MILEAGE(myTaxiData,myTaxiData.fTaxiOJ_TempFloat)
				
				//Your followee made it to his destination.
				IF IS_VEHICLE_DRIVEABLE(myEscapeeStruct.viCar)
					IF ( IS_ENTITY_AT_COORD(myEscapeeStruct.viCar,vEnemyDestination,<<20.0,20.0,60.0>>, TRUE)
					AND GET_ENTITY_SPEED(myEscapeeStruct.viCar) < 5.0 )
					OR myEscapeeStruct.tesState > TES_DRIVING
						IF NOT IS_ENTITY_DEAD(piWhore)
							CLEAR_PED_TASKS(piWhore)
							SET_ENTITY_COORDS(piWhore, vWhoreGoToPos)
						ENDIF
		
						SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_DROP_OFF,TRUE)

						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_FOR_PASSENGER)
						
					ENDIF
				ENDIF
			BREAK
			
			CASE TRS_WAIT_FOR_PASSENGER
				UPDATE_OJ_TAXI_MILEAGE(myTaxiData,myTaxiData.fTaxiOJ_TempFloat)
				
				MONITOR_TAXI_ESCAPEE_OK( myTaxiData, myEscapeeStruct.piPed )
				MONITOR_TAXI_ESCAPEE_OK( myTaxiData, piWhore, FALSE, FALSE )
				
				TAXI_AI_HANDLE_BLIPS(myTaxiData, myTaxiData.blipTaxiDropOff)
				
				IF DOES_BLIP_EXIST(myEscapeeStruct.bBlip)
					REMOVE_BLIP(myEscapeeStruct.bBlip)
					myTaxiData.vTaxiOJDropoff = vDropOff
	
					CDEBUG1LN(DEBUG_OJ_TAXI,"TRS_WAIT_FOR_PASSENGER - Removed blip for enemy")
			
				ELIF NOT DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
		
					myTaxiData.blipTaxiDropOff =  CREATE_BLIP_FOR_COORD(myTaxiData.vTaxiOJDropoff,TRUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"TRS_WAIT_FOR_PASSENGER - Added blip for dest")
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
					IF IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
//						IF IS_ENTITY_IN_ANGLED_AREA(myTaxiData.viTaxi, <<-861.4246,-4.3583,42.3135>>, <<-878.7411,2.9062,46.6091>>, 3.00)
//						OR IS_ENTITY_IN_ANGLED_AREA(myTaxiData.viTaxi, <<-894.5720,-3.8196,42.6009>>, <<-880.9864,4.0184,46.3550>>, 1.75)
						
						//placing here to fake a corona
						IF IS_ENTITY_AT_COORD(myTaxiData.viTaxi, <<-684.52600, -1105.75964, 13.52571>>, << 1.0, 1.0, LOCATE_SIZE_HEIGHT>>, !(myTaxiData.bIsCurrentlyWanted))
						ENDIF
						
						IF IS_ENTITY_IN_ANGLED_AREA(myTaxiData.viTaxi, <<-685.0081,-1101.2966,13.5270>>, <<-678.6770,-1110.6399, 15.5871>>, 2.25)
						OR IS_ENTITY_IN_ANGLED_AREA(myTaxiData.viTaxi, <<-704.1705,-1115.2262,13.5250>>, <<-700.2585,-1121.2924,15.4336>>, 2.25)
							//IF IS_TAXI_FULLY_STOPPED(myTaxiData)
								
								//IF IS_ENTITY_IN_ANGLED_AREA(myTaxiData.viTaxi, <<-861.4246,-4.3583,42.3135>>, <<-878.7411,2.9062,46.6091>>, 3.00)
								IF IS_ENTITY_IN_ANGLED_AREA(myTaxiData.viTaxi, <<-685.0081,-1101.2966,13.5270>>, <<-678.6770,-1110.6399, 15.5871>>, 2.25)
									eFCDropZone = TFC_DROPZONE1
								ELSE
									eFCDropZone = TFC_DROPZONE2
								ENDIF
								
								myTaxiData.fTaxiOJ_TimeDeadline = 60.0
								
								myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_DI_EMPTY
								myTaxiData.tTaxiOJ_DXIndex = TAXI_DI_EMPTY
								
								bTaxiSetVehicleMultiplier = FALSE
								
								TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DROPPING_OFF)
							//ENDIF
						
						ENDIF
					ENDIF
				ENDIF
			BREAK

			CASE TRS_DROPPING_OFF
				MONITOR_TAXI_ESCAPEE_OK( myTaxiData, myEscapeeStruct.piPed, FALSE )
				MONITOR_TAXI_ESCAPEE_OK( myTaxiData, piWhore, FALSE, FALSE )
				
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
						IF myEscapeeStruct.tesState <> TES_DEAD
							
							//IF TAXI_HANDLE_DRIVING(myTaxiData,tTaxiOJ_DQ_Data,TRUE)
							IF IS_TAXI_FULLY_STOPPED(myTaxiData, TRUE, 3)
									
								bTaxiOJ_GYN_IsPassengerAtConfrontationPt = TRUE
								
								REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
							
								TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
							ENDIF	
						ELSE
							IF NOT DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
								myTaxiData.blipTaxiDropOff = CREATE_BLIP_FOR_COORD(myTaxiData.vTaxiOJDropoff,TRUE)
							ENDIF
							IF TAXI_HANDLE_DRIVING(myTaxiData,/*tTaxiOJ_DQ_Data,*/8)
							OR IS_TAXI_FULLY_STOPPED(myTaxiData)
								REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
								
								TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			//This case is only hit if the play kills the driver of the car he's following
			//Waits for the player to clear his wanted level.
			CASE TRS_ESCAPE_POLICE
				MONITOR_TAXI_ESCAPEE_OK( myTaxiData, myEscapeeStruct.piPed ,FALSE)
				MONITOR_TAXI_ESCAPEE_OK( myTaxiData, piWhore, FALSE, FALSE )
				
				IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_DROP_OFF,TRUE)		
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
					
				ENDIF
			BREAK

			CASE TRS_REGULAR_PAYMENT
				MONITOR_TAXI_ESCAPEE_OK( myTaxiData, myEscapeeStruct.piPed ,FALSE)
				MONITOR_TAXI_ESCAPEE_OK( myTaxiData, piWhore, FALSE, FALSE )
				
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)

					IF DOES_BLIP_EXIST(myEscapeeStruct.bBlip)
						REMOVE_BLIP( myEscapeeStruct.bBlip )
					ENDIF
						
					CLEAR_THIS_PRINT("TAXI_OBJ_DRIVE") 
					
					
					//This variable represents our delta of positive / negative feedback.  Follow car is a special case
					IF myEscapeeStruct.iNumTimesWarned > 2 //more than 2 warnings.  Ass tip
						CDEBUG1LN(DEBUG_OJ_TAXI,"more than 2 warnings.  Ass tip")
						myTaxiData.iTaxiOJ_CashTip = -10
					
					ELIF myEscapeeStruct.iNumTimesWarned = 0 //Amazing tip!  No warnings AND UNLOCK BONUS
						CDEBUG1LN(DEBUG_OJ_TAXI,"Amazing tip!  No warnings")
						myTaxiData.iTaxiOJ_CashTip = 7
						
						TAXI_SET_BONUS_AWARD(myTaxiData,ENUM_TO_INT(TFC_BONUS_P_EYE))
						
					ELSE //1-2 warnings.. ok tip
						CDEBUG1LN(DEBUG_OJ_TAXI,"1-2 warnings.. ok tip")
						myTaxiData.iTaxiOJ_CashTip = 4
					ENDIF
					
					SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)
					TAXI_OJ_RATE_OVERALL_TIP_LEVEL(myTaxiData)
					CONVERT_TAXI_TIP_TO_CASH(myTaxiData)
					
					
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)
				ENDIF
			BREAK
			
			//Pop up the scorecard
			CASE TRS_SCORECARD_GRADE
				
				CLEAR_THIS_PRINT("TAXI_OBJ_DRIVE") 
				
				IF TAXI_CALC_SCORECARD(myTaxiData,TaxiMidSize)
					
					SET_TAXI_ODDJOB_PASSED(myTaxiData)
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPECIAL_ENDING)
					
				ENDIF

			BREAK		
			
			CASE TRS_SPECIAL_ENDING
				
				IF bTaxiOJ_GYN_IsFianceDead
				OR bTaxiOJ_GYN_IsPlayerTooClose
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CLEANUP)
				ENDIF

			BREAK
			
			CASE TRS_CLEANUP
			
				CLEAR_THIS_PRINT("TAXI_OBJ_DRIVE")
				
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
				AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piWhore, myTaxiData.piTaxiPassenger) 
				AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myEscapeeStruct.piPed, myTaxiData.piTaxiPassenger)
					IF bAmbulanceFound
					OR TIMERA() > 20000
						//IF TIMERA() > 5000	
							
							TAXI_MISSION_END(TRUE, myTaxiData)
							Script_Cleanup()
						//ENDIF
					ELSE
						viAmbulance = GET_RANDOM_VEHICLE_IN_SPHERE(vWhoreSpawn, 10, AMBULANCE, 0)
						IF IS_VEHICLE_DRIVEABLE(viAmbulance)
							//SETTIMERA(0)
							bAmbulanceFound = TRUE
						ENDIF
						piMedic = GET_RANDOM_PED_AT_COORD(vWhoreSpawn, <<5,5,5>>)
						IF NOT IS_PED_INJURED(piMedic)
							//SETTIMERA(5000)
							bAmbulanceFound = TRUE
						ENDIF
						IF NOT bDispatchSuccessful
							IF CREATE_INCIDENT(DT_AMBULANCE_DEPARTMENT, vWhoreSpawn, 1, 0.0, nurseIncident)
								CDEBUG1LN(DEBUG_OJ_TAXI,"incident created")
								SETTIMERA(0)
								bDispatchSuccessful = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_PLAYER_DISTANCE_FROM_LOCATION(vWhoreSpawn) > 100
				OR GET_DISTANCE_BETWEEN_PEDS( myTaxiData.piTaxiPassenger, PLAYER_PED_ID() ) > 100			// player far awar from passenger
				 
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"Player is far away.  Cleaning up")
					
					TAXI_MISSION_END(TRUE, myTaxiData)
					Script_Cleanup()
					
				ELIF ( IS_PED_INJURED(myTaxiData.piTaxiPassenger) 											// any of the peds get injured 
						//OR IS_PED_INJURED(piWhore) 
						//OR IS_PED_INJURED(myEscapeeStruct.piPed) )
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piWhore, PLAYER_PED_ID()) 
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myEscapeeStruct.piPed, PLAYER_PED_ID()) )
						
					IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiData.piTaxiPassenger, TRUE)
						CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
						
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						OPEN_SEQUENCE_TASK(siEscapeeSequenceID)
							TASK_COWER(NULL, 2000)
							TASK_SMART_FLEE_COORD(NULL, myTaxiData.vTaxiOJDropoff, 1000, -1)
						CLOSE_SEQUENCE_TASK(siEscapeeSequenceID)
						TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siEscapeeSequenceID)
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						
						SET_PED_KEEP_TASK( myTaxiData.piTaxiPassenger, TRUE )
						
					ENDIF
					
					IF NOT IS_PED_INJURED(piWhore)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piWhore, TRUE)
						CLEAR_PED_TASKS(piWhore)
						
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						OPEN_SEQUENCE_TASK(siEscapeeSequenceID)
							TASK_COWER(NULL, 1000)
							TASK_SMART_FLEE_COORD(NULL, myTaxiData.vTaxiOJDropoff, 1000, -1)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -900.3293, 6.8557, 43.2381 >>, PEDMOVEBLENDRATIO_SPRINT)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -917.1956, -9.5316, 42.6199 >>, PEDMOVEBLENDRATIO_SPRINT)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -910.8076, -18.8469, 42.5941 >>, PEDMOVEBLENDRATIO_SPRINT) //coord behind house
//							TASK_COWER(NULL)
						CLOSE_SEQUENCE_TASK(siEscapeeSequenceID)
						TASK_PERFORM_SEQUENCE(piWhore, siEscapeeSequenceID)
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						
						SET_PED_KEEP_TASK( piWhore, TRUE )
						
					ENDIF
					
					IF NOT IS_PED_INJURED(myEscapeeStruct.piPed)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myEscapeeStruct.piPed, TRUE)
						//CLEAR_PED_TASKS(myEscapeeStruct.piPed)
						
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						OPEN_SEQUENCE_TASK(siEscapeeSequenceID)
							IF bFianceReacted
								TASK_PLAY_ANIM(NULL, "MOVE_DUCK_FOR_COVER", "exit")
							ENDIF
							TASK_STAND_STILL(NULL, 500)
							TASK_SMART_FLEE_COORD(NULL, myTaxiData.vTaxiOJDropoff, 1000, -1)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -900.3293, 6.8557, 43.2381 >>, PEDMOVEBLENDRATIO_SPRINT)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -917.1956, -9.5316, 42.6199 >>, PEDMOVEBLENDRATIO_SPRINT)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -910.8076, -18.8469, 42.5941 >>, PEDMOVEBLENDRATIO_SPRINT) //coord behind house
//							TASK_COWER(NULL)
						CLOSE_SEQUENCE_TASK(siEscapeeSequenceID)
						TASK_PERFORM_SEQUENCE(myEscapeeStruct.piPed, siEscapeeSequenceID)
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						
						SET_PED_KEEP_TASK( myEscapeeStruct.piPed, TRUE )
						
					ENDIF
					
					TAXI_MISSION_END(TRUE, myTaxiData)
					Script_Cleanup()
				
				ELIF bTaxiOJ_GYN_IsPlayerTooClose
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (GET_GAME_TIMER() - iFinalFailDelay) > 500
					
					IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
						
						
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiData.piTaxiPassenger, TRUE)
						CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
						
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						OPEN_SEQUENCE_TASK(siEscapeeSequenceID)
							TASK_SHOOT_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, FIRING_TYPE_CONTINUOUS )
						CLOSE_SEQUENCE_TASK(siEscapeeSequenceID)
						TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siEscapeeSequenceID)
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						
						SET_PED_KEEP_TASK( myTaxiData.piTaxiPassenger, TRUE )
						
					ENDIF
					
					IF NOT IS_PED_INJURED(piWhore)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piWhore, TRUE)
						CLEAR_PED_TASKS(piWhore)
						
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						OPEN_SEQUENCE_TASK(siEscapeeSequenceID)
							TASK_COWER(NULL, 1000)
							TASK_SMART_FLEE_COORD(NULL, myTaxiData.vTaxiOJDropoff, 1000, -1)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -900.3293, 6.8557, 43.2381 >>, PEDMOVEBLENDRATIO_SPRINT)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -917.1956, -9.5316, 42.6199 >>, PEDMOVEBLENDRATIO_SPRINT)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -910.8076, -18.8469, 42.5941 >>, PEDMOVEBLENDRATIO_SPRINT) //coord behind house
//							TASK_COWER(NULL)
						CLOSE_SEQUENCE_TASK(siEscapeeSequenceID)
						TASK_PERFORM_SEQUENCE(piWhore, siEscapeeSequenceID)
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						
						SET_PED_KEEP_TASK( piWhore, TRUE )
						
					ENDIF
					
					IF NOT IS_PED_INJURED(myEscapeeStruct.piPed)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myEscapeeStruct.piPed, TRUE)
						CLEAR_PED_TASKS(myEscapeeStruct.piPed)
						
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						OPEN_SEQUENCE_TASK(siEscapeeSequenceID)
							TASK_STAND_STILL(NULL, 500)
							TASK_SMART_FLEE_COORD(NULL, myTaxiData.vTaxiOJDropoff, 1000, -1)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -900.3293, 6.8557, 43.2381 >>, PEDMOVEBLENDRATIO_SPRINT)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -917.1956, -9.5316, 42.6199 >>, PEDMOVEBLENDRATIO_SPRINT)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -910.8076, -18.8469, 42.5941 >>, PEDMOVEBLENDRATIO_SPRINT) //coord behind house
//							TASK_COWER(NULL)
						CLOSE_SEQUENCE_TASK(siEscapeeSequenceID)
						TASK_PERFORM_SEQUENCE(myEscapeeStruct.piPed, siEscapeeSequenceID)
						CLEAR_SEQUENCE_TASK(siEscapeeSequenceID)
						
						SET_PED_KEEP_TASK( myEscapeeStruct.piPed, TRUE )
						
					ENDIF
					
					CLEANUP_TAXI_ODDJOB(myTaxiData)
					Script_Cleanup()
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC

SCRIPT

	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		Script_Cleanup()
	ENDIF
	
	// Any initialisation (generally, only mission scripts should set
	// the mission flag to TRUE)
	SET_MISSION_FLAG(TRUE)
	
	INITIALIZE_SCRIPT_VARIABLES()
	
	// The script loop
	WHILE (TRUE)
		// Maintain the script – perform per-frame functionality

	//All skips and debug shortcuts-------------------------------
	#IF IS_DEBUG_BUILD			
		
		PROCESS_TAXI_DEBUG_SKIP(myTaxiData,tDebugState)
		
		// Debug Key: Check for Pass (not for Minigmes)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			Script_Passed()
		ENDIF

		// Debug Key: Check for Fail (not for Minigames)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			TAXI_DEBUG_FAIL_TRIGGERED(myTaxiData)
			Script_Failed()
		ENDIF
		
		// Debug Key: set vehicle health to 0.0
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
			IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				SET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi, 0.0)
			ENDIF
		ENDIF			

		PROCESS_WIDGETS()

	#ENDIF
	//END DEBUG----------------------------------------------------

		IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPlayer)
			Main_Taxi_OJ_FollowCar()
		ELSE
			REASSIGN_TAXI_OJ_DRIVER(myTaxiData)
		ENDIF
		
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

