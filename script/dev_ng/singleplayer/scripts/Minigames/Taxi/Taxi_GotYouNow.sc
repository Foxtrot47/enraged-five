//=======================================================================================================================================
// Taxi_GotYouNow.sc
// Dev : John R. Diaz
/*
	•	“We’ve got you this time, pal!” This is the only scenario that launches without immediately notifying the player. If the player 
	has angered a specific gang or has committed enough crimes to anger the police, this scenario will launch and mimic a normal passenger. 
	However, the drop off for this passenger will be someplace unusual. (Such as behind a warehouse or at an abandoned peer.) When you arrive, 
	the passenger runs from the car and takes cover. Baddies and cars pop out of hiding and ambush you. “You shouldn’t fuck with ___!” they 
	shout before opening fire.  The player can flee, but if they opt to fight the ambushers should have ammo and contraband such that the 
	player feels that they did not waste their time by picking up cab fares.
*/



//CHANGELOG==========================================================================================================
//9/27/11 - Reformatting/prettifying all taxi oj scripts
//12/20/11 - End of mission cleaned up to allow for better sequence of events.  General script cleanup.  --johnsripan
//4/27/12	- Removed all refs to previous Got You Now which took place in the construction site. :( 
//===================================================================================================================



//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.



//Includes------------------------------------------------------
USING "Taxi_Includes.sch"

USING "stripclub_public.sch"		//Add girl to cellphone


//Local Variables-----------------------------------------------

TaxiStruct 						myTaxiData

MODEL_NAMES 					mPassengerModel = G_M_Y_MexGoon_03


ENUM TAXIOJ_GYN_BONUS
	TGYN_BONUS_FLAWLESS_VICTORY = 0,
	TGYN_BONUS_TOTAL
				
ENDENUM

BONUS_FIELD						bonusFieldGotYouNow[TGYN_BONUS_TOTAL]

//Construction Site Setup/Custscene states
ENUM CSITE_STATES
	CSITE_INTRO_DIALOGUE = 0,
	CSITE_S01_CLEAR_LAND,
	CSITE_S01_CLEANUP
	
ENDENUM
CSITE_STATES					m_CSiteState = CSITE_INTRO_DIALOGUE

ENUM MUG_SCENE
	MUGSCENE_INIT,
	MUGSCENE_AGGRO,
	MUGSCENE_PULSE,
	MUGSCENE_FIRST_CAM,
	MUGSCENE_SECOND_CAM,
	MUGSCENE_GEN_END,
	MUGSCENE_GEN_END2,
	MUGSCENE_SKIP,
	MUGSCENE_SKIP_SETUP,
	MUGSCENE_SKIP_FADE_IN,
	MUGSCENE_SKIP_END
ENDENUM
MUG_SCENE						m_mugScene = MUGSCENE_INIT

ENUM TAXI_DYNAMIC_REQUESTS
	TAXIDR_REQUEST,
	TAXIDR_WAIT_STREAM,
	TAXIDR_CREATE,
	TAXIDR_CLEANUP
ENDENUM
TAXI_DYNAMIC_REQUESTS eTaxiRequestState = TAXIDR_REQUEST

CONST_FLOAT						TAXI_OJ_CONST_DIST_TO_SEE_CSITE					85.0
CONST_FLOAT						TAXI_OJ_CONST_DIST_TO_ESCAPE_CSITE				300.0
CONST_FLOAT						TAXI_CONST_CSITE_SPEED_PLAYBACK		1.0
CONST_FLOAT						TAXI_CONST_GOON_EGG_ON_DELAY		10.0
CONST_FLOAT 					fCarDistance 5.0
CONST_FLOAT						fPedDistance 4.0
CONST_INT						TAXI_CONST_BONUS_CASH_FLAWLESS_VICTORY	100 //500 //1000

//Native Data
SEQUENCE_INDEX 					siTemp

WEAPON_TYPE 					currentWeapon

//Entitys
VEHICLE_INDEX					viGauntlet
PED_INDEX						piChickInGauntlet
//Ints
INT 							iDebugThrottle = 1
INT								iUnderBridgeIndex = -1
INT								iLocalBitSet = 0
INT								iTipIndex = 0
INT								iPlayerHealth
INT								iPassengerHealth
INT								iSoundID_RingTone = -1

//Bools
BOOL 							bFairFight = TRUE
BOOL 							bTaxiGYNOneWarning = FALSE
BOOL 							bWarningLeeway = FALSE
#IF IS_DEBUG_BUILD
BOOL 							bFlawlessVictory = TRUE //Made debug only by bobby wright - 30/04/12
#ENDIF
BOOL							bHealthBonusGiven = FALSE
BOOL 							bTaxiEarlyAggro
BOOL 							bPhoneRang
BOOL							bSeatShuffled
BOOL							bPhoneAnswered
BOOL							bPulseTriggered

//Vectors
VECTOR 							vPassengerPt = << -1612.2349, 189.1934, 58.9435 >>	//UCLS Campus
VECTOR 							vPassengerPickupPt = << -1612.4570, 184.5166, 58.7712 >>
VECTOR							vNewDropoff_2_Under_Bridge = << 32.60266, -1243.83740, 28.45076 >>//<< 27.1498, -1246.5232, 28.4013 >> 
VECTOR							vDropOffUnderBridge =  << 33.2419, -1234.3851, 28.2953 >>
VECTOR							vDropOffChickHouse = << -628.1456, -760.3765, 25.1060 >>//<< -1044.6719, -889.0599, 3.9594 >>
VECTOR							vChickPtToWalkTo = << -584.7739, -779.0701, 24.0178 >>//<< -1025.6396, -919.9550, 1.2780 >>
VECTOR 							vFightAreaPos1 = <<47.567871,-1234.684692,28.297070>>
VECTOR 							vFightAreaPos2 = <<-21.325907,-1235.549194,32.475967>> 
VECTOR 							vScrapLocation = <<37.51834, -1239.02063, 28.40267>>
//VECTOR 							vAlonzoRunPt

FLOAT 							fFightAreaWidth = 33.750000

TEXT_LABEL_23					sGetInCabLabel
TEXT_LABEL_23					sCurrentLine

TEXT_LABEL_63					sMaleChatEnter = "amb@world_human_hang_out_street@male_a@enter"	//use enter
TEXT_LABEL_63					sMaleChatBase = "amb@world_human_hang_out_street@male_a@base" 	//use base
TEXT_LABEL_63					sMaleChatIdle = "amb@world_human_hang_out_street@male_a@idle_a"	//use idle_a or idle_d
TEXT_LABEL_63					sMaleChatExit = "amb@world_human_hang_out_street@male_a@exit"	//use exit 

TEXT_LABEL_63					sFemaleChatEnter = "amb@world_human_hang_out_street@female_arm_side@enter"	//use enter
TEXT_LABEL_63					sFemaleChatBase = "amb@world_human_hang_out_street@female_arm_side@base"	//use base
TEXT_LABEL_63					sFemaleChatIdle = "amb@world_human_hang_out_street@female_arm_side@idle_a"	//use idle_b
TEXT_LABEL_63					sFemaleChatExit = "amb@world_human_hang_out_street@female_arm_side@exit"	//use exit

//Const
CONST_INT						kPedLowAccuracy  		1
CONST_INT						kPedNotSoLowAccuracy  	1
CONST_INT						k_bDropOffFound			0
CONST_INT						k_bPassengerTalking		1
CONST_INT						k_bPassengerVulnerable	2

SCENARIO_BLOCKING_INDEX 		sbIndex_TaxiOJ

CAMERA_INDEX					ciTransCam
//Dialogue Queue Info
BOOL							g_bDebug = FALSE
TAXI_OJ_DIALOGUE_Q_DATA			tTaxiOJ_DQ_Data								
TAXI_OJ_DQ_CONVERSATION_LINE	tDialogueLine[CONST_TAXIOJ_SIZE_Q]

BLIP_INDEX						blipZo

AGGRO_ARGS 						aggroArgs 

SCRIPT_SHARD_BIG_MESSAGE TaxiMidSize
//Debug---------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 			taxiRideWidgets
	BOOL 						bForceReset, bResetting
	INT 						iForcedLocation// = 2
	BOOL 						bDebugTurnOnFreeRide = FALSE	
	FLOAT						fCar2PBSpeed = 1.0
	BOOL						bDebugBonusFlawlessVictory	
	TEXT_LABEL_31				sDebugString[3]
	TXM_DEBUG_SKIP_STATES 		tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS
#ENDIF

//FUNCTIONS------------------------------------------------------------------------------------------------------------


ENUM GYN_MODELS
	GYN_MODEL_GAUNTLET	=	0,
	GYN_MODEL_LADY,
	
	GYN_NUM_MODELS
ENDENUM

FUNC MODEL_NAMES GET_GYN_MISSION_MODELS(GYN_MODELS whichModel)
	MODEL_NAMES mnToReturn
	SWITCH whichModel
		CASE GYN_MODEL_GAUNTLET
			mnToReturn = GAUNTLET
		BREAK
		
		CASE GYN_MODEL_LADY
			mnToReturn = A_F_Y_EASTSA_03
		BREAK
	ENDSWITCH
	
	RETURN mnToReturn
ENDFUNC


/// PURPOSE: Make all of our mission specific requests here
///    
PROC REQUEST_TAXI_ODDJOB_GYN_STREAMS_STAGE_01()
	//Load text and UI
	REQUEST_MODEL(mPassengerModel)
		//Load text and UI
	TAXI_INIT_SHARED_STREAMS()
	TaxiMidSize.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	//REQUEST_AMBIENT_AUDIO_BANK("TREVOR_2_BIKER_RINGTONE")
	REQUEST_AMBIENT_AUDIO_BANK("SCRIPT\\TREVOR_2_BIKER_RINGTONE")
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM GYN - Stage 01 Assets Requested")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_GYN_STREAMS_STAGE_01()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mPassengerModel)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ Follow Car Stage01 Assets Released")
	
ENDPROC

PROC REQUEST_TAXI_ODDJOB_GYN_STREAMS_STAGE_02()

	REQUEST_MODEL(GET_GYN_MISSION_MODELS(GYN_MODEL_GAUNTLET))
	REQUEST_MODEL(GET_GYN_MISSION_MODELS(GYN_MODEL_LADY))
	
	REQUEST_ANIM_SET("move_strafe_melee_unarmed")
	
	REQUEST_ANIM_DICT("oddjobs@taxi@gyn@")
	REQUEST_ANIM_DICT("misscommon@response")
	
	REQUEST_ANIM_DICT("melee@unarmed@streamed_taunts")
	REQUEST_ANIM_DICT(sMaleChatEnter)
	REQUEST_ANIM_DICT(sMaleChatBase)
	REQUEST_ANIM_DICT(sMaleChatIdle)
	REQUEST_ANIM_DICT(sMaleChatExit)
	REQUEST_ANIM_DICT(sFemaleChatEnter)
	REQUEST_ANIM_DICT(sFemaleChatBase)
	REQUEST_ANIM_DICT(sFemaleChatIdle)
	REQUEST_ANIM_DICT(sFemaleChatExit)
	
	REQUEST_WAYPOINT_RECORDING("taxi_oj_gyn")
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM GYN - Stage 02 Assets Requested")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_GYN_STREAMS_STAGE_02()

	SET_MODEL_AS_NO_LONGER_NEEDED(GET_GYN_MISSION_MODELS(GYN_MODEL_GAUNTLET))
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_GYN_MISSION_MODELS(GYN_MODEL_LADY))
	
	REMOVE_ANIM_SET("move_strafe_melee_unarmed")
	
	REMOVE_ANIM_DICT("oddjobs@taxi@gyn@")
	REMOVE_ANIM_DICT("melee@unarmed@streamed_taunts")
	REMOVE_ANIM_DICT("misscommon@response")
	REMOVE_ANIM_DICT(sMaleChatEnter)
	REMOVE_ANIM_DICT(sMaleChatBase)
	REMOVE_ANIM_DICT(sMaleChatIdle)
	REMOVE_ANIM_DICT(sMaleChatExit)
	REMOVE_ANIM_DICT(sFemaleChatEnter)
	REMOVE_ANIM_DICT(sFemaleChatBase)
	REMOVE_ANIM_DICT(sFemaleChatIdle)
	REMOVE_ANIM_DICT(sFemaleChatExit)
	
	REMOVE_WAYPOINT_RECORDING("taxi_oj_gyn")
	
	RELEASE_AMBIENT_AUDIO_BANK()
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM GYN - Stage 02 Assets Released")
ENDPROC


//// PURPOSE: Waits for our assets to load
 ///    
 /// RETURNS: TRUE if everything has loaded correctly FALSE if there was a problem
 ///    
 ///     
 ///     
FUNC BOOL HAVE_TAXI_OJ_GYN_STAGE_01_ASSETS_LOADED()
	
	IF NOT HAS_MODEL_LOADED(mPassengerModel)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading A_M_M_Farmer_01",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(TaxiMidSize.siMovie)
		#IF IS_DEBUG_BUILD
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TaxiMidSize.siMovie",iDebugThrottle)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	IF NOT TAXI_SHARED_ASSETS_STREAMED(iDebugThrottle)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading shared assets",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXMOJ - GYN All Stage 01 Assets Have Loaded")
	RETURN TRUE		
ENDFUNC

FUNC BOOL HAVE_TAXI_OJ_GYN_STAGE_02_ASSETS_LOADED()
		
	IF NOT HAS_MODEL_LOADED(GET_GYN_MISSION_MODELS(GYN_MODEL_GAUNTLET))
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading Gaunlet ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(GET_GYN_MISSION_MODELS(GYN_MODEL_LADY))
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading A_F_Y_EastSA_03",iDebugThrottle)
		RETURN FALSE
	ENDIF

	
	IF NOT HAS_ANIM_SET_LOADED("move_strafe_melee_unarmed")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading move_strafe_melee_unarmed ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("misscommon@response")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading misscommon@response ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	
	IF NOT HAS_ANIM_DICT_LOADED(sMaleChatEnter)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading sMaleChatEnter ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(sMaleChatBase)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading sMaleChatBase ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(sMaleChatExit)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading sMaleChatExit ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(sMaleChatIdle)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading sMaleChatIdle ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(sFemaleChatBase)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading sFemaleChatBase ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(sFemaleChatEnter)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading sFemaleChatEnter ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(sFemaleChatExit)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading sFemaleChatExit ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(sFemaleChatIdle)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading sFemaleChatIdle ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("taxi_oj_gyn")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMING - Way point Recording Loading taxi_oj_gyn...",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE		
ENDFUNC

PROC Script_Cleanup()
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(taxiRideWidgets)
			DELETE_WIDGET_GROUP(taxiRideWidgets)
		ENDIF
		
		CLEANUP_TAXI_WIDGETS()
		
	#ENDIF
	
	TAXI_OJ_CLEAR_AREA_OF_ALL_PEDS_AND_SCENARIOS(sbIndex_TaxiOJ, vNewDropoff_2_Under_Bridge, 100.0,TRUE)
		
	
	REMOVE_WAYPOINT_RECORDING("taxi_oj_gyn")
	SET_CREATE_RANDOM_COPS(TRUE)
	
	RELEASE_TAXI_ODDJOB_GYN_STREAMS_STAGE_01()
	RELEASE_TAXI_ODDJOB_GYN_STREAMS_STAGE_02()
	
	REMOVE_MODEL_HIDE(vScrapLocation, 5.0, PROP_SKID_CHAIR_02)
	REMOVE_MODEL_HIDE(vScrapLocation, 5.0, PROP_RUB_COUCH03)
	
	STOP_AUDIO_SCENE("TAXI_GOT_U_NOW")
	
	TERMINATE_THIS_THREAD()
ENDPROC

// Perform any special commands if the script fails
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Failed()
	TAXI_SCRIPT_FAILED(myTaxiData)
	Script_Cleanup()
ENDPROC




PROC SETUP_AMBUSH_SCENE_UNDER_BRIDGE()

	SWITCH iUnderBridgeIndex
		//Create Vehicle
		CASE -1
			IF NOT DOES_ENTITY_EXIST(viGauntlet)
				viGauntlet = CREATE_VEHICLE(GET_GYN_MISSION_MODELS(GYN_MODEL_GAUNTLET), << 27.1498, -1246.5232, 28.4013 >>, 297.6290)//<< 27.22, -1239.09, 28.95 >>, 310.50) // << 27.2828, -1232.9750, 28.3352 >>, 311.8042) //
				
				
				iUnderBridgeIndex++
				CDEBUG1LN(DEBUG_OJ_TAXI,"Camaro Created")
				
			ENDIF
		BREAK
		
		//Create Chick & Setup Vehicle
		CASE 0
			IF NOT IS_ENTITY_DEAD(viGauntlet)
			
				SET_VEHICLE_RADIO_LOUD(viGauntlet,TRUE)
				SET_VEHICLE_RADIO_ENABLED(viGauntlet,TRUE)
				SET_VEHICLE_NUMBER_PLATE_TEXT(viGauntlet,"SNAKEYES")
				SET_VEHICLE_INTERIORLIGHT(viGauntlet,TRUE)
				//SET_VEHICLE_DOOR_OPEN( viGauntlet, SC_DOOR_BOOT )
				SET_VEHICLE_DOORS_LOCKED(viGauntlet, VEHICLELOCK_CANNOT_ENTER)
				
				VECTOR vTemp
				vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viGauntlet, <<0.0,3.0,0.0>>)
				piChickInGauntlet =  CREATE_PED(PEDTYPE_MISSION,GET_GYN_MISSION_MODELS(GYN_MODEL_LADY), vTemp, (GET_ENTITY_HEADING(viGauntlet) - 180)) //<< 46.4830, -1230.2010, 28.2881 >>,250.6)
				
				iUnderBridgeIndex++
				CDEBUG1LN(DEBUG_OJ_TAXI,"Chick Created Created")
				
			ENDIF
		BREAK
		
		//Ped is created and car is in place, now task ped
		CASE 1
			IF NOT IS_ENTITY_DEAD(piChickInGauntlet)
			AND NOT IS_ENTITY_DEAD(viGauntlet)
				ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,5,piChickInGauntlet,"TaxiLiz")
				SET_AMBIENT_VOICE_NAME(piChickInGauntlet, "TaxiLiz")
				
				SET_PED_CONFIG_FLAG(piChickInGauntlet,PCF_CanSayFollowedByPlayerAudio,TRUE)
				
				SET_PED_COMPONENT_VARIATION(piChickInGauntlet, PED_COMP_TORSO, 2, 0)
				SET_PED_COMPONENT_VARIATION(piChickInGauntlet, PED_COMP_LEG, 0, 1)
				SET_PED_COMPONENT_VARIATION(piChickInGauntlet, PED_COMP_HAIR, 0, 0)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piChickInGauntlet, TRUE)
				SET_PED_CAN_BE_TARGETTED(piChickInGauntlet,FALSE)
				PED_HAS_SEXINESS_FLAG_SET(piChickInGauntlet,SF_JEER_AT_HOT_PED)
								
				TASK_LOOK_AT_ENTITY(piChickInGauntlet, viGauntlet, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
		
				OPEN_SEQUENCE_TASK(siTemp)
					//TASK_GO_STRAIGHT_TO_COORD_RELATIVE_TO_ENTITY(NULL,viGauntlet,<<0.0,-3.0,0.0>>,PEDMOVEBLENDRATIO_WALK)
                    //TASK_TURN_PED_TO_FACE_ENTITY(NULL, viGauntlet)
                    TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@", "idle_b_ped",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)    
                CLOSE_SEQUENCE_TASK(siTemp)
                TASK_PERFORM_SEQUENCE(piChickInGauntlet, siTemp)
                CLEAR_SEQUENCE_TASK(siTemp)  		
				
				iUnderBridgeIndex++
				CDEBUG1LN(DEBUG_OJ_TAXI,"Chick Tasked")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Call this to get the different camera positions for the csite cutscene
///    
/// PARAMS:
///    index - 
/// RETURNS:
///    
FUNC VECTOR GET_TAXI_OJ_GYN_CSITE_CS_CAM_POS(INT index)
	VECTOR vCamPos
	SWITCH index
		
		//Revised End shot interp
		CASE 8
			 vCamPos = << -137.4260, -1086.8722, 26.0049 >> //<< -138.6420, -1089.5889, 25.3735 >>
		BREAK
		
		//Revised end shot2
		CASE 9
			 vCamPos = << -118.4414, -1079.5820, 27.9648 >> //<< -129.8217, -1101.1809, 24.6919 >>
		BREAK
		
		//Revised End shot interp2
		CASE 10
			 vCamPos = << -124.0122, -1077.5231, 27.9648 >> //<< -132.0994, -1100.2334, 24.6919 >>
		BREAK
		
		
	ENDSWITCH
	
	RETURN vCamPos
ENDFUNC
/// PURPOSE: Call this to get the different camera directions for the csite custscene
///    
/// PARAMS:
///    index - 
/// RETURNS:
///    
FUNC VECTOR GET_TAXI_OJ_GYN_CSITE_CS_CAM_ROT(INT index)
	VECTOR vCamDir
	SWITCH index
		
		//Revised End shot interp
		CASE 8
			 vCamDir = << -10.5394, -0.0000, -112.7087 >> //<< -14.2245, -0.0000, -90.7686 >>
		BREAK
		
		//Revised end shot
		CASE 9
			 vCamDir = << -26.9453, -0.0000, 153.6124 >> //<< -13.3622, -0.0000, -22.5860 >>
		BREAK
		
		//Revised End shot interp
		CASE 10
			 vCamDir = << -26.9453, -0.0000, 166.5540 >> //<< -13.3622, -0.0000, -22.5860 >>
		BREAK
		
	ENDSWITCH
	
	RETURN vCamDir
ENDFUNC


PROC CLEANUP_TAXIOJ_GYN()

	IF DOES_CAM_EXIST(myTaxiData.camTaxi)
		DESTROY_CAM(myTaxiData.camTaxi)
	ENDIF		

	DISPLAY_RADAR(TRUE)
	SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
	
ENDPROC

/// PURPOSE:
///    Initializes script variables
PROC INITIALIZE_SCRIPT_VARIABLES()
	
	//Init j skip points------
	myTaxiData.vTaxiOJ_WarpPtPickup = << -1583.5905, 169.2662, 57.6205 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingPickup = 116

	myTaxiData.vTaxiOJ_WarpPtDropoff = << 49.0898, -1178.9226, 28.2091 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 185.975	
	
	TAXI_ODDJOB_GLOBAL_SETUP(myTaxiData,TXM_08_GOTYOUNOW)
		
	// Set our initial state up. VIP missions don't start at TRS_FINDING_LOCATION.
	myTaxiData.tTaxiOJ_RideState = TRS_INIT_STREAM
	
	SET_TAXI_TIP_CUTOFFS(myTaxiData,CONST_TAXI_OJ_TIP_AVERAGE_CUTOFF,CONST_TAXI_OJ_TIP_AMAZING_CUTOFF)
	
	enumCharacterList playerEnum
	playerEnum = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())

	IF (playerEnum = CHAR_MICHAEL)
		sGetInCabLabel = "txm9_gHelp1M_4"
	ELIF (playerEnum = CHAR_TREVOR)
		sGetInCabLabel = "txm9_gHelp1T_4"
	ELIF (playerEnum = CHAR_FRANKLIN)
		sGetInCabLabel = "txm9_gHelp1F_7"
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"sGetInCabLabel = ", sGetInCabLabel)
	
	#IF IS_DEBUG_BUILD
		#IF DEBUG_iTurnOnAllDXDebug
			ENABLE_ALL_DIALOGUE_DEBUG()
		#ENDIF
		taxiRideWidgets = START_WIDGET_GROUP("Taxi Ride - Got You Now")
			
			INIT_ODDJOB_TAXI_WIDGETS()
			
			ADD_WIDGET_STRING("Debug Taxi Bonus")
				ADD_WIDGET_BOOL("Flawless Victory ",bDebugBonusFlawlessVictory)
			
				START_NEW_WIDGET_COMBO()
				//REPEAT COUNT_OF(CSITE_CUTSCENE_STATES) i
					ADD_TO_WIDGET_COMBO("CSITE_CS_TRIGGER_CARS")
				//ENDREPEAT
			STOP_WIDGET_COMBO( "Mission To Run Next", iForcedLocation)
			ADD_WIDGET_FLOAT_SLIDER("Car 2 Speed", fCar2PBSpeed,1.0,3.0,0.1)
			ADD_WIDGET_BOOL("Restart Taxi Mission", bForceReset)
		STOP_WIDGET_GROUP()
	#ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~~~~~~~~~~~~ Oddjobs | Taxi | Got You Now ~~~~~~~~~~~~~~~~~~----------")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_WIDGETS()

	UPDATE_TAXI_WIDGETS(myTaxiData,tTaxiOJ_DQ_Data)
	
	IF bForceReset
		
		CLEANUP_TAXIOJ_GYN()
		bForceReset = FALSE
		bResetting = TRUE
	ENDIF
	
	IF bDebugBonusFlawlessVictory
		//Show if bonus has been unlocked or not
		IF NOT bFlawlessVictory
			sDebugString[0]= "Flawless V Bonus LOCKED "
			DRAW_DEBUG_TEXT_2D(sDebugString[0],<<0.07,0.77,0.0>>,255,0,0)
			
		ELSE
			sDebugString[0]= "Flawless Bonus UnLOCKED "
			DRAW_DEBUG_TEXT_2D(sDebugString[0],<<0.07,0.77,0.0>>)
		ENDIF
	ENDIF
	
	IF bResetting
		IF HAVE_TAXI_OJ_GYN_STAGE_01_ASSETS_LOADED()
			bResetting = FALSE
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

PROC TASK_PED_FIGHT_STANCE(PED_INDEX &Fighter)
	
	IF NOT IS_PED_INJURED(Fighter)
		//SET_PED_STRAFE_CLIPSET(Fighter, "move_strafe_melee_unarmed")
		IF ( GET_SCRIPT_TASK_STATUS(Fighter, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
			OR GET_SCRIPT_TASK_STATUS(Fighter, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK	)
			
			TASK_PLAY_ANIM(Fighter, "move_strafe_melee_unarmed", "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:  Sets variables for the passenger to go aggressive.  Also tasks him into combat.
///    
PROC SET_TAXI_OJ_PED_IN_COMBAT(PED_INDEX & pedInCombat)
	IF DOES_ENTITY_EXIST(pedInCombat)
		IF NOT IS_PED_INJURED(pedInCombat)
			
			SET_PED_COMBAT_ATTRIBUTES(pedInCombat, CA_AGGRESSIVE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pedInCombat, CA_USE_VEHICLE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pedInCombat, CA_LEAVE_VEHICLES, TRUE)
			SET_PED_SEEING_RANGE(pedInCombat, 300.0)
			SET_PED_HEARING_RANGE(pedInCombat, 300.0)
			SET_PED_ID_RANGE(pedInCombat, 300.0)
			
			SET_PED_COMBAT_MOVEMENT(pedInCombat, CM_WILLADVANCE)
			SET_PED_COMBAT_ABILITY(pedIncombat, CAL_PROFESSIONAL)
			
			SET_PED_ACCURACY(pedInCombat, 75)
			SET_PED_COMBAT_RANGE(pedInCombat, CR_MEDIUM)
			
			SET_COMBAT_FLOAT(pedInCombat, CCF_FIGHT_PROFICIENCY, 1.0)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(pedInCombat,myTaxiData.relPassenger)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, myTaxiData.relPassenger, RELGROUPHASH_PLAYER)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED( pedInCombat,100.0)
			
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL MUGGING_CAMS()
	VECTOR vMugCamPosition, vMugCamRot, vWarpTemp2
	
	STOP_PHONE_AND_APPS_FOR_TAXI_CUTSCENE()	
	
	IF m_mugScene < MUGSCENE_FIRST_CAM //MUGSCENE_SKIP
	AND m_mugScene > MUGSCENE_INIT
		IF HANDLE_SKIP_CUTSCENE(iAllowSkipCutsceneTime)
			m_mugScene = MUGSCENE_SKIP
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			DRAW_DEBUG_TEXT_2D("Player INSIDE of vehicle", << 0.5, 0.2, 0.0 >>)
		ELSE
			DRAW_DEBUG_TEXT_2D("Player OUTSIDE of vehicle", << 0.5, 0.2, 0.0 >>)
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			DRAW_DEBUG_TEXT_2D("Player INSIDE of vehicle adjusted", << 0.5, 0.25, 0.0 >>)
		ELSE
			DRAW_DEBUG_TEXT_2D("Player OUTSIDE of vehicle adjusted", << 0.5, 0.25, 0.0 >>)
		ENDIF
	ENDIF
	
	SWITCH m_mugScene
		CASE MUGSCENE_INIT
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ODDJOB_ENTER_CUTSCENE()
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				
				CREATE_TAXI_OJ_CAM(myTaxiData.camTaxi,<<0,0,0>>, <<0,0,0>>)
			
				//vMugCamPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viGauntlet, << -1.5, 6.0, -0.2 >>)
				vMugCamPosition = <<30.8062, -1242.6118, 29.1026>>
				vMugCamRot = <<7.5623, -0.0714, 149.3929>>
				SET_CAM_COORD(myTaxiData.camTaxi, vMugCamPosition)
				SET_CAM_ROT(myTaxiData.camTaxi, vMugCamRot)
				SET_CAM_FOV(myTaxiData.camTaxi, 35.0)
				SHAKE_CAM(myTaxiData.camTaxi, "HAND_SHAKE", 0.3)
				//POINT_CAM_AT_ENTITY(myTaxiData.camTaxi, viGauntlet, <<1.0, 1.0, 1.0>>)
				SET_CAM_ACTIVE(myTaxiData.camTaxi,TRUE)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
				AND NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
	//				VECTOR vWarpTemp
					
					SET_ENTITY_COORDS(myTaxiData.viTaxi, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viGauntlet, << 0, 8.25, 0.0>>)) //<< 33.2618, -1227.6528, 28.3351 >>)
					SET_ENTITY_HEADING(myTaxiData.viTaxi, 114.629) //131.7553)
					
	//				vWarpTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << -2.0, -0.75, 0.0 >>)
	//				SET_ENTITY_COORDS(myTaxiData.piTaxiPassenger, vWarpTemp)
	//				SET_ENTITY_HEADING(myTaxiData.piTaxiPassenger, (GET_ENTITY_HEADING(myTaxiData.viTaxi) - 45))
					
					//TASK_LEAVE_ANY_VEHICLE(myTaxiData.piTaxiPassenger)
					TASK_LOOK_AT_ENTITY(myTaxiData.piTaxiPassenger, PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
			
					//TASK_LEAVE_ANY_VEHICLE(myTaxiData.piTaxiPassenger, 0)
				ENDIF
				
				SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_GIRL_NOTICE, TRUE, TRUE)
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"Combat timer started")
				
				TAXI_RESET_TIMERS(myTaxiData, TT_GENERIC)
				
				m_mugScene = MUGSCENE_AGGRO
			ELSE
				KILL_ANY_CONVERSATION()
				
			ENDIF
		BREAK
		
		CASE MUGSCENE_AGGRO
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > 2.0
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Combat initiated")
					
					CLEAR_SEQUENCE_TASK(siTemp)
					OPEN_SEQUENCE_TASK(siTemp)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_ENTER_VEHICLE(NULL, myTaxiData.viTaxi, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, ECF_JUST_PULL_PED_OUT | ECF_JACK_ANYONE)// | ECF_WARP_ENTRY_POINT)
					CLOSE_SEQUENCE_TASK(siTemp)
					TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siTemp)
					
					PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, "txm9_figt1", "txm9_figt1_1",CONV_PRIORITY_HIGH)
				ENDIF
				TAXI_RESET_TIMERS(myTaxiData, TT_GENERIC)
				
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					IF NOT bPulseTriggered
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bPulseTriggered = TRUE
					ENDIF
				ENDIF
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"going to mug pulse")
				m_mugScene = MUGSCENE_PULSE
			
			ENDIF
		BREAK
		
		CASE MUGSCENE_PULSE
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > 0.3
				CDEBUG1LN(DEBUG_OJ_TAXI,"going to mug first cam")
				m_mugScene = MUGSCENE_FIRST_CAM
			ENDIF
		BREAK
		
		CASE MUGSCENE_FIRST_CAM
//			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > 0.75
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_GENERIC)
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"going to mug second cam")
				m_mugScene = MUGSCENE_SECOND_CAM
				
//			ENDIF
			
			
		BREAK
		
		CASE MUGSCENE_SECOND_CAM
			IF NOT IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
			AND NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
				
				//KILL_ANY_CONVERSATION()
				
				SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger, PCF_ForceIgnoreMeleeActiveCombatant, TRUE)
				//SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_ForcePedToStrafe, TRUE)
				SET_TAXI_OJ_PED_IN_COMBAT(myTaxiData.piTaxiPassenger)
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_GENERIC)
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"going to mug gen end")
				m_mugScene = MUGSCENE_GEN_END
			ELSE
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						CDEBUG1LN(DEBUG_OJ_TAXI,"passenger was broke out of his task, re-tasking")
						CLEAR_SEQUENCE_TASK(siTemp)
						OPEN_SEQUENCE_TASK(siTemp)
							TASK_LEAVE_ANY_VEHICLE(NULL)
							TASK_ENTER_VEHICLE(NULL, myTaxiData.viTaxi, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, ECF_JUST_PULL_PED_OUT | ECF_JACK_ANYONE)// | ECF_WARP_ENTRY_POINT)
						CLOSE_SEQUENCE_TASK(siTemp)
						TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siTemp)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"ped is injured, big problem")
				ENDIF
			ENDIF
		BREAK
		
		CASE MUGSCENE_GEN_END
//			IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
//				SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_ForcePedToStrafe, TRUE)
//			ENDIF
			
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > 0.5
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, "txm9_figt1", "txm9_figt1_2",CONV_PRIORITY_VERY_HIGH)
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_GENERIC)
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"going to gen end 2")
				
				m_mugScene = MUGSCENE_GEN_END2
			ENDIF
		BREAK
		
		CASE MUGSCENE_GEN_END2
			IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
				SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_ForcePedToStrafe, TRUE)
			ENDIF
			
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > 1.0
			//AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_OBJ_GYN, TRUE)
				
				ODDJOB_EXIT_CUTSCENE()
				
				IF DOES_CAM_EXIST(myTaxiData.camTaxi)
					DESTROY_CAM(myTaxiData.camTaxi)
				ENDIF
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_GENERIC)
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE MUGSCENE_SKIP
			CDEBUG1LN(DEBUG_OJ_TAXI,"Mug scene skipped")
						
			IF IS_SCREEN_FADED_OUT()
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
				AND IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
					
					CLEAR_PED_TASKS_IMMEDIATELY(myTaxiData.piTaxiPassenger)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					
				ENDIF
				
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_ANY_CONVERSATION()
				ENDIF
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_GENERIC)
				m_mugScene = MUGSCENE_SKIP_SETUP
			ENDIF
		BREAK
		
		CASE MUGSCENE_SKIP_SETUP
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > 0.3
				VECTOR vWarpTemp
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
				AND IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
					
					CLEAR_PED_TASKS_IMMEDIATELY(myTaxiData.piTaxiPassenger)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					
					SET_VEHICLE_DOOR_SHUT(myTaxiData.viTaxi, SC_DOOR_FRONT_LEFT)
					
					vWarpTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << -1.6, 0.0, 0.0 >>)
					vWarpTemp2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << -4.0, 2.0, 0.0 >>)
					
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpTemp)
					SET_ENTITY_COORDS(myTaxiData.piTaxiPassenger, vWarpTemp2)
					
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 
										GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(myTaxiData.piTaxiPassenger)))
					//SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 2000, 3000, TASK_NM_SCRIPT)
					
					SET_ENTITY_HEADING(myTaxiData.piTaxiPassenger, 
										GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(myTaxiData.piTaxiPassenger), GET_ENTITY_COORDS(PLAYER_PED_ID())))
					
					//TASK_PLAY_ANIM(myTaxiData.piTaxiPassenger, "move_strafe_melee_unarmed", "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					SET_TAXI_OJ_PED_IN_COMBAT(myTaxiData.piTaxiPassenger)
				ENDIF
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				IF DOES_CAM_EXIST(myTaxiData.camTaxi)
					DESTROY_CAM(myTaxiData.camTaxi)
				ENDIF
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_GENERIC)
				m_mugScene = MUGSCENE_SKIP_FADE_IN
			ENDIF
		BREAK
		
		CASE MUGSCENE_SKIP_FADE_IN
			CDEBUG1LN(DEBUG_OJ_TAXI,"Mug scene skip fade in")
			
			IF NOT IS_SCREEN_FADING_IN()
			AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > 1.0
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
			
				m_mugScene = MUGSCENE_SKIP_END
			ENDIF
		BREAK
		
		CASE MUGSCENE_SKIP_END
			IF NOT IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
				ODDJOB_EXIT_CUTSCENE()
			
				SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GYN, TRUE)
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_GENERIC)
				
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
					

	RETURN FALSE
ENDFUNC

PROC TAXI_OJ_S01_SETUP_CSITE()

	SWITCH m_CSiteState
		//Say something when the construction site is near
		CASE CSITE_INTRO_DIALOGUE
		
			IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_SEEN_DEST)
			AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_SEEN_DESTINATION
				IF IS_PASSENGER_IN_TAXI(myTaxiData)
					IF IS_TAXI_DRIVEN_BY_PLAYER(myTaxiData)
						IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff) <= TAXI_OJ_CONST_DIST_TO_SEE_CSITE
						AND iUnderBridgeIndex > 1
							
							myTaxiData.vTaxiOJDropoff = vNewDropoff_2_Under_Bridge
							SET_BLIP_COORDS(myTaxiData.blipTaxiDropOff, vNewDropoff_2_Under_Bridge)
							
							//Clear & Close the Dialogue Q
							CLEAR_DIALOGUE_QUEUE(tDialogueLine)
							CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,3)
							
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_SEEN_DESTINATION_2,TRUE)
							
							IF IS_VEHICLE_DRIVEABLE(viGauntlet)
								SET_VEHICLE_DOOR_OPEN( viGauntlet, SC_DOOR_BONNET )
								SET_VEHICLE_COLOUR_COMBINATION(viGauntlet, 0 )
								//SET_VEHICLE_DOOR_BROKEN( viGauntlet, SC_DOOR_BONNET, FALSE)
								SET_VEHICLE_ENGINE_HEALTH(viGauntlet, 150.0)
								SET_VEHICLE_DAMAGE(viGauntlet, <<0.0, 0.0, 0.0>>, 1000.0, 0.0, TRUE)
								//GIVE_VEHICLE_MINIMUM_HEALTH(viGauntlet)
							ENDIF
							
							CREATE_MODEL_HIDE(vScrapLocation, 5.0, PROP_SKID_CHAIR_02, TRUE)
							CREATE_MODEL_HIDE(vScrapLocation, 5.0, PROP_RUB_COUCH03, TRUE)
							
							m_CSiteState = CSITE_S01_CLEAR_LAND
							CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteState = CSITE_S01_CLEAR_LAND")
							IF g_bDebug
								SCRIPT_ASSERT("Seeing the construction site")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CSITE_S01_CLEAR_LAND

			TAXI_OJ_CLEAR_AREA_OF_ALL_PEDS_AND_SCENARIOS(sbIndex_TaxiOJ, vNewDropoff_2_Under_Bridge, 30.0)
			
			m_CSiteState = CSITE_S01_CLEANUP
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_S01_SETUP_CSITE = CSITE_S01_CLEANUP")
			
		BREAK
		
		CASE CSITE_S01_CLEANUP
		BREAK
	ENDSWITCH	
ENDPROC

FUNC BOOL CHECK_PLAYER_ESCAPED_CSITE()
	IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.piTaxiPlayer,myTaxiData.vTaxiOJDropoff) >= TAXI_OJ_CONST_DIST_TO_ESCAPE_CSITE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC TAXI_OJ_GYN_SET_TIP_AND_EXCITEMENT_TO_CHECK()
	
	//Tip Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POS_DELIVERY_TIME)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_HIT_PED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_TOOK_DAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ROLL_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LEFT_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_STOPPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_LIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_DISLIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_FREEBIE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LOST_POLICE)
	
	
	//EXCITEMENT BITS
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_HITPED)					
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_TOOKDAMAGE)					
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_ROLL)	
	
					
	//Turn off aggro bits
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
					
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DL_SET_TIPS_TO_CHECK set tips to check on")
	
ENDPROC

ENUM GYN_CS_STATES
	GYN_CS_01_INIT = 0,
	GYN_CS_SHOT_02,
	GYN_CS_SHOT_INTERP_02,
	GYN_CS_SHOT_03,
	GYN_CS_SHOT_INTERP_03,
	GYN_CS_SHOT_04,
	GYN_CS_SHOT_05,

	GYN_CS_SKIP,
	GYN_CS_END,
	GYN_CS_CLEANUP,
	GYN_CS_NUM_SCENES
	
ENDENUM
GYN_CS_STATES m_ChickCSState = GYN_CS_01_INIT

/// PURPOSE: plays the cutscene when the lady goes up to the player and asks him for ride home
///    
/// RETURNS: TRUE when it's done and ready to move on
///    
FUNC BOOL TAXI_OJ_GYN_CUTSCENE_SAVE_CHICK()
	
	STOP_PHONE_AND_APPS_FOR_TAXI_CUTSCENE()
	
	IF NOT IS_ENTITY_DEAD(piChickInGauntlet)
	AND IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
	
		SWITCH m_ChickCSState
		
			//Init everying and put everything in it's place
			CASE GYN_CS_01_INIT
			
				IF CAN_PLAYER_START_CUTSCENE(TRUE, FALSE)
					
					TASK_LOOK_AT_ENTITY(piChickInGauntlet, PLAYER_PED_ID(), -1, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), piChickInGauntlet, -1, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
					
					//Clear & Close the Dialogue Q
					CLEAR_DIALOGUE_QUEUE(tDialogueLine)
					CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,-1,TRUE)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = GYN_CS_SHOT_02")
					m_ChickCSState = GYN_CS_SHOT_02
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"CAN_PLAYER_START_CUTSCENE returns false")
				ENDIF
			BREAK
			
			//1st shot, looks at nurse, driver
			CASE GYN_CS_SHOT_02
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 2.0
				
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GIRL_HELP,TRUE,FALSE,TRUE)
					
					TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)			
					CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = GYN_CS_SHOT_INTERP_04")
					m_ChickCSState = GYN_CS_SHOT_04
				ENDIF
			BREAK
			
			//Cut to them getting in taxi
			CASE GYN_CS_SHOT_04
				
				sCurrentLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
				
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 15.0
				OR ARE_STRINGS_EQUAL(sGetInCabLabel, sCurrentLine)
					
					INT iNumSeats
					VEHICLE_SEAT vsChickSeat
					iNumSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(myTaxiData.viTaxi)
					
					IF GET_ENTITY_MODEL(myTaxiData.viTaxi) = SENTINEL2
						CDEBUG1LN(DEBUG_OJ_TAXI,"vehicle is sentinel2")
						iNumSeats = 1
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"vehicle is not a sentinel2")
					ENDIF
					
					IF iNumSeats > 1
						vsChickSeat = VS_BACK_RIGHT
					ELSE
						vsChickSeat = VS_FRONT_RIGHT
					ENDIF
					
					TASK_ENTER_VEHICLE(piChickInGauntlet, myTaxiData.viTaxi, 40000, vsChickSeat, PEDMOVEBLENDRATIO_WALK)
					
					TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)			
					m_ChickCSState = GYN_CS_SHOT_05
					CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = GYN_CS_SHOT_05")
				ELSE
					IF (GET_GAME_TIMER() % 1500) < 50
						TASK_TURN_PED_TO_FACE_ENTITY(piChickInGauntlet, PLAYER_PED_ID())
					ENDIF
				ENDIF
			BREAK
			
			CASE GYN_CS_SHOT_05
				
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 20
					IF NOT IS_ENTITY_DEAD(piChickInGauntlet)
						CLEAR_PED_TASKS(piChickInGauntlet)
						TASK_WANDER_STANDARD(piChickInGauntlet)
					ENDIF
					TAXI_SET_FAIL(myTaxiData, "Player not letting the chick in the car", TFS_TAXI_STOPPED)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
					IF NOT IS_ENTITY_DEAD(piChickInGauntlet)
						IF IS_PED_IN_ANY_VEHICLE(piChickInGauntlet)
							m_ChickCSState = GYN_CS_END
							CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = GYN_CS_END")
						ELSE
							IF (GET_GAME_TIMER() % 2500) < 50
								CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = GYN_CS_SHOT_05 - Gfriend is not in a vehicle")
							ENDIF
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = GYN_CS_SHOT_05 - Gfriend is not dead")
					ENDIF
				
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = GYN_CS_SHOT_05 - Taxi is not driveable")
					
				ENDIF
			BREAK
			
			CASE GYN_CS_SKIP				
				IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
					IF NOT IS_ENTITY_DEAD(piChickInGauntlet)
						IF NOT IS_PED_IN_ANY_VEHICLE(piChickInGauntlet)
							SET_PED_INTO_VEHICLE(piChickInGauntlet, myTaxiData.viTaxi,VS_BACK_RIGHT)
						ENDIF
					ENDIF
					
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myTaxiData.viTaxi)
					ENDIF
				ENDIF
				TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)	
				m_ChickCSState = GYN_CS_END
				CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = GYN_CS_END")
				
			BREAK
			
			CASE GYN_CS_END
				DRAW_DEBUG_TEXT_2D("GYN_CS_END", << 0.75, 0.75, 0.0 >>)
				
				myTaxiData.bPassengerObjPrinted = FALSE
				
				IF DOES_CAM_EXIST(myTaxiData.camTaxi)
					DESTROY_CAM(myTaxiData.camTaxi)
				ENDIF
				
				IF DOES_CAM_EXIST(myTaxiData.camInterp)
					DESTROY_CAM(myTaxiData.camInterp)
				ENDIF
				
				IF DOES_CAM_EXIST(ciTransCam)
					DESTROY_CAM(ciTransCam)
				ENDIF
				
//				ODDJOB_EXIT_CUTSCENE()
				
//				RENDER_SCRIPT_CAMS(FALSE,FALSE)
//				
//				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				TAXI_CANCEL_TIMERS(myTaxiData, TT_CUTSCENE)		
				
				m_ChickCSState = GYN_CS_CLEANUP
				CDEBUG1LN(DEBUG_OJ_TAXI,"m_CSiteCSState = GYN_CS_CLEANUP")
				
				RETURN TRUE
			BREAK
			
		ENDSWITCH
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"Cutscene can't go")
	ENDIF
	RETURN FALSE
ENDFUNC

PROC UPDATE_DYNAMIC_TAXI_REQUESTS()
	SWITCH eTaxiRequestState
		CASE TAXIDR_REQUEST
			IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
			AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJDropoff) < 300
				REQUEST_TAXI_ODDJOB_GYN_STREAMS_STAGE_02()
					
				eTaxiRequestState = TAXIDR_WAIT_STREAM
			ENDIF
		BREAK
		CASE TAXIDR_WAIT_STREAM
			IF HAVE_TAXI_OJ_GYN_STAGE_02_ASSETS_LOADED()
				eTaxiRequestState = TAXIDR_CREATE
			ENDIF
		BREAK
		CASE TAXIDR_CREATE
			
			SETUP_AMBUSH_SCENE_UNDER_BRIDGE()
			
			IF iUnderBridgeIndex > 1
				eTaxiRequestState = TAXIDR_CLEANUP
			ENDIF
		BREAK
		CASE TAXIDR_CLEANUP	
			IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
			AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJDropoff) > 350
				RELEASE_TAXI_ODDJOB_GYN_STREAMS_STAGE_02()
				iUnderBridgeIndex = -1
				eTaxiRequestState = TAXIDR_REQUEST
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/*
ooooooooo  ooooo      o      ooooo         ooooooo     ooooooo8  
 888    88o 888      888      888        o888   888o o888    88  
 888    888 888     8  88     888        888     888 888    oooo 
 888    888 888    8oooo88    888      o 888o   o888 888o    88  
o888ooo88  o888o o88o  o888o o888ooooo88   88ooo88    888ooo888  
*/
PROC TRIGGER_TAXI_QUEUE_GYN_LINES()

	SET_TAXI_OJ_INTERRUPT_TIMER_OFF(myTaxiData)

	//Trigger lines
	IF IS_BANTER_SAFE_TO_PLAY(myTaxiData,tTaxiOJ_DQ_Data)
		
		SWITCH tTaxiOJ_DQ_Data.iCurrentDQLine
			//Make sure if player interrupts the initial line, that the objective prints anyway.
			CASE 0
				IF myTaxiData.tTaxiOJ_RideState =  TRS_DRIVING_PASSENGER
					
					IF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_GYN_DO")
						OR DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
							CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO J Store has been  assigned")
							tTaxiOJ_DQ_Data.iCurrentDQLine++
							
						ELSE
							IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_OBJ_GIVE_MAIN
							
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE,FALSE,TRUE)
								CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO J Store has been REassigned")

							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 1
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER
				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER,TRUE)
					IF g_bDebug
						SCRIPT_ASSERT("Triggering Banter 1")
					ENDIF
					
					//Start the mission timer here
					TAXI_START_TIMER(myTaxiData, TT_RIDETODEST)
				ENDIF
			BREAK
		
			CASE 2
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 4.0
				AND NOT bPhoneRang
					iSoundID_RingTone = GET_SOUND_ID()
					PLAY_SOUND_FROM_ENTITY(iSoundID_RingTone, "Biker_Ring_Tone", myTaxiData.viTaxi, "TREVOR_2_SOUNDS")
					//PLAY_SOUND_FRONTEND(iSoundID_RingTone, "Biker_Ring_Tone", "TREVOR_2_SOUNDS")
					CDEBUG1LN(DEBUG_OJ_TAXI,"Rang dat phone. iSoundID_RingTone = ", iSoundID_RingTone)
					bPhoneRang = TRUE
				
				ELIF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 7.0
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_2
				AND bPhoneRang
				AND NOT bPhoneAnswered
					ODDJOB_STOP_SOUND(iSoundID_RingTone)
					
					IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
						TASK_USE_MOBILE_PHONE(myTaxiData.piTaxiPassenger, TRUE)
					ENDIF
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_2,TRUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"TX DX CALL TAXI_DI_BANTER_2")
					IF g_bDebug
						SCRIPT_ASSERT("Triggering Banter 1")
					ENDIF
					
					bPhoneAnswered = TRUE
				ENDIF
			BREAK
			
			CASE 3
				// Get the guy off the phone.
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					TASK_USE_MOBILE_PHONE(myTaxiData.piTaxiPassenger, FALSE)
				ENDIF
				
				tTaxiOJ_DQ_Data.iCurrentDQLine++	
				
//				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > GET_RANDOM_FLOAT_IN_RANGE(3.0,6.0)
//					//Set Radio Station Check
//					IF NOT GET_TAXI_RADIO_CHECK_FLAG(myTaxiData)	
//						TAXI_RADIO_STATION_TURN_ON(myTaxiData)
//						IF g_bDebug
//							SCRIPT_ASSERT("The Taxi Radio Should Be Updating")
//						ENDIF
//					ENDIF
//				ENDIF
			BREAK
			
			
			//Liz's Banter
			CASE 5	
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 2.0
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_GIRL_BANTER
				
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GIRL_BANTER,TRUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"TX DX CALL TAXI_DI_GIRL_BANTER")
					
					IF g_bDebug
						SCRIPT_ASSERT("Triggering TAXI_DI_GIRL_BANTER")
					ENDIF
					tTaxiOJ_DQ_Data.iCurrentDQLine++	
				ENDIF
				
			BREAK
		ENDSWITCH				
	ENDIF				
	
	PROCESS_IMPORTANT_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, g_bDebug)

ENDPROC
PROC CALCULATE_TAXI_GYN_JOB_REWARDS()
	//CALC HEALTH CASH VALUE----------------------------------------------------
	//Taxi Health will be written into CashFare
	IF DOES_ENTITY_EXIST(myTaxiData.viTaxi)
		IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
			//Check a min payout for taxi damage, if not good enough, predefine a value
			IF GET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi) < 500
				myTaxiData.iTaxiOJ_CashFare = 250 //1000 //2000
			ELSE
				myTaxiData.iTaxiOJ_CashFare = 100 //500 //1000
			ENDIF
		ENDIF
	ENDIF
	
	//Player health
	myTaxiData.iTaxiOJ_CashTip = GET_PED_MAX_HEALTH(PLAYER_PED_ID()) - GET_ENTITY_HEALTH(PLAYER_PED_ID())
	
	IF myTaxiData.iTaxiOJ_CashTip <= 150 //100
		myTaxiData.iTaxiOJ_CashTip = 250 //1000 //2000
	ELSE
		myTaxiData.iTaxiOJ_CashTip = 100 //500 //1000
	ENDIF
	
	//PRICE FOR KO
	//myTaxiData.iTaxiOJ_SpecialCash = 1000
ENDPROC
PROC Main_Taxi_OJ_GotYouNow()
	
	//Handles Fail Or No Taxi-------------------------------------------------------------------------------
	IF IS_TAXI_JOB_IN_FAIL_STATE(myTaxiData)
		
		TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
		
		IF TAXI_HANDLE_FAIL(myTaxiData)
			Script_Failed()
		ENDIF
	
	//Proceed
	ELSE
		//Run Throughtout the Entire Mission--------------------------------------------------------------
		TAXI_OJ_MAINTAIN_GLOBAL_GATES(myTaxiData)
		
		IF (myTaxiData.tTaxiOJ_RideState >= TRS_MANAGE_PICKUP
			AND myTaxiData.tTaxiOJ_RideState <= TRS_DRIVING_PASSENGER)
		OR myTaxiData.tTaxiOJ_RideState >= TRS_DROPPING_OFF
			//Gobal taxi updates that all taxi missions run every frame
			RUN_GLOBAL_TAXI_UPDATES(myTaxiData,aggroArgs)
		
			PROCESS_TAXI_EXCEPTIONS(myTaxiData)
		ENDIF
		
		UPDATE_TAXI_OJ_TIP(myTaxiData,iTipIndex)
		
		//Find Dropoff Point
		IF myTaxiData.tTaxiOJ_RideState > TRS_MANAGE_PICKUP
		AND NOT IS_BIT_SET( iLocalBitSet, k_bDropOffFound )
		
			myTaxiData.vTaxiOJDropoff = vDropOffUnderBridge //vNewDropoff_2_Under_Bridge
			
			SET_BIT( iLocalBitSet, k_bDropOffFound )
		ENDIF
	

		IF myTaxiData.tTaxiOJ_RideState >= TRS_MANAGE_PICKUP
		AND myTaxiData.tTaxiOJ_RideState < TRS_WAIT_FOR_FULL_STOP
			TAXI_ODDJOB_HANDLE_TAXI_BLIPPING(myTaxiData)
		ENDIF
		
		//Dialogue & Objectives
		IF myTaxiData.tTaxiOJ_RideState >= TRS_FINDING_LOCATION			
			IF NOT IS_TAXI_EMERGENCY_FAIL_SET(myTaxiData)
				TRIGGER_TAXI_QUEUE_GYN_LINES()
			ELSE
				TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",GET_TAXI_EMERGENCY_FAIL_STRING(myTaxiData))
			ENDIF				

		ENDIF
		
		IF myTaxiData.tTaxiOJ_RideState >= TRS_SAVE_DAMSEL
		AND myTaxiData.tTaxiOJ_RideState < TRS_REGULAR_PAYMENT
			//Protect Girlfriend
			//Fail Case for new booty call----------------------------------------------	
			IF DOES_ENTITY_EXIST(piChickInGauntlet)
				IF IS_ENTITY_DEAD(piChickInGauntlet)
					TAXI_SET_FAIL(myTaxiData,"Player killed his new booty call")
				ENDIF
			ENDIF
			
			IF HAS_TAXI_DAMAGED_ENTITY(myTaxiData, viGauntlet)
				IF NOT IS_ENTITY_DEAD(viGauntlet)
					EXPLODE_VEHICLE(viGauntlet)
					TAXI_SET_FAIL(myTaxiData,"Player exploded the broke down car")
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(piChickInGauntlet)
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(piChickInGauntlet) > 50
					TAXI_SET_FAIL(myTaxiData,"Player abandoned the girl")
				ENDIF
			ENDIF
		ENDIF
		//---------------------------------------------------------------------------
		
		
		IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
			HANDLE_TAXI_EXCITEMENT(myTaxiData,FALSE,TRUE)

#IF IS_DEBUG_BUILD
		ELIF bDebugTurnOnFreeRide
			HANDLE_TAXI_EXCITEMENT(myTaxiData,TRUE,TRUE)
#ENDIF
		ENDIF			
		
		
		SWITCH myTaxiData.tTaxiOJ_RideState
/*PRESTREAM and Check that the player and taxi are good to go----------------------------------------
			
ooooo oooo   oooo ooooo ooooooooooo 
 888   8888o  88   888  88  888  88 
 888   88 888o88   888      888     
 888   88   8888   888      888     
o888o o88o    88  o888o    o888o   	
*/
			CASE TRS_INIT_STREAM
				
				//Request of our assets
				REQUEST_TAXI_ODDJOB_GYN_STREAMS_STAGE_01()
				
				TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
				
				TAXI_SPAWN_PASSENGER(myTaxiData, vPassengerPt, vPassengerPickupPt, "TaxiAlonzo",mPassengerModel,196.3547,15.0)
				TAXI_INIT_PASSENGER_BLIP(myTaxiData)
					
					
				//Move on to the next stage
				TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_STREAMING)
			BREAK
			
			//Wait for streaming to finish
			CASE TRS_STREAMING

				IF HAVE_TAXI_OJ_GYN_STAGE_01_ASSETS_LOADED()

					INIT_ALL_TAXI_EXCITEMENT_VALUES()

					//Init Bonus----------------------------------------			
					TAXI_INITIALIZE_BONUS_FIELD(bonusFieldGotYouNow[TGYN_BONUS_FLAWLESS_VICTORY], "TAXI_SC_KO", TAXI_CONST_BONUS_CASH_FLAWLESS_VICTORY)
					TAXI_INITIALIZE_BONUS_INFO(myTaxiData, bonusFieldGotYouNow)
					
					//SET REACT BITS
					CLEAR_ALL_TAXI_PASSENGER_REACT_BITS(myTaxiData)
					
					INITIALIZE_GENERIC_TAXI_EXCEPTIONS()
					
					//Set Pickup
					myTaxiData.vTaxiOJPickup  = vPassengerPt	
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPAWNING)	
				ENDIF
	
			BREAK
			/*SPAWNING---------------------------------------------------------------------------------------------------------
		
 oooooooo8 oooooooooo   o  oooo     oooo oooo   oooo ooooo oooo   oooo  
888         888    888 888  88   88  88   8888o  88   888   8888o  88  
 888oooooo  888oooo88 8  88  88 888 88    88 888o88   888   88 888o88  
        888 888      8oooo88  888 888     88   8888   888   88   8888  
o88oooo888 o888o   o88o  o888o 8   8     o88o    88  o888o o88o    88 
		*/
			CASE TRS_SPAWNING
				IF PROPERTY_VIP_INIT_READY(myTaxiData)
					
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
					
						SET_PED_MONEY(myTaxiData.piTaxiPassenger,200)
					
//						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_TORSO, 1, 0)
//						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_LEG, 1, 0)
//						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_SPECIAL2, 0, 0)
//						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_HAIR, 0, 0)
						
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)
					ENDIF
					
					//Give player obj to go pickup Passenger
					ENABLE_TAXI_SPEECH(myTaxiData)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
				ENDIF
			BREAK
			
			CASE TRS_MANAGE_PICKUP

				IF TAXI_HANDLE_IV_PICKUP_NO_METER(myTaxiData)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_PARK)	
				ENDIF
			BREAK
//Waits for passenger to head to cab
/*			
oooooooooo ooooo  oooooooo8 oooo   oooo ooooo  oooo oooooooooo  
 888    888 888 o888     88  888  o88    888    88   888    888 
 888oooo88  888 888          888888      888    88   888oooo88  
 888        888 888o     oo  888  88o    888    88   888        
o888o      o888o 888oooo88  o888o o888o   888oo88   o888o       
                                                                
																*/			
			CASE TRS_WAIT_PARK
				IF IS_PASSENGER_ENTERING_TAXI(myTaxiData)
					RELEASE_TAXI_ODDJOB_GYN_STREAMS_STAGE_01()
					
					//Greet the player
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GREET,TRUE)
					
					//CleanUp Pickup Lock & POI
					CLEANUP_TAXI_PICKUP_STOP(myTaxiData)
					
					TAXI_OJ_GYN_SET_TIP_AND_EXCITEMENT_TO_CHECK()
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DRIVING_PASSENGER)
					
				ENDIF
				
				// check to see if player dived, if so send back to prev state
				IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
					IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
						TAXI_HANDLE_PLAYER_DIVE(myTaxiData)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DRIVING_PASSENGER)
					ENDIF
				ENDIF
			BREAK
			
/* DRIVING PASSENGER
	
ooooooooo  oooooooooo  ooooo ooooo  oooo ooooo oooo   oooo  ooooooo8  
 888    88o 888    888  888   888    88   888   8888o  88 o888    88  
 888    888 888oooo88   888    888  88    888   88 888o88 888    oooo 
 888    888 888  88o    888     88888     888   88   8888 888o    88  
o888ooo88  o888o  88o8 o888o     888     o888o o88o    88  888ooo888  
*/
			//Once you get to destination wait----------------------------------------------------------------------------------------------------------
			CASE TRS_DRIVING_PASSENGER
				
				IF NOT bSeatShuffled
					//Shuffle to the next seat
					IF IS_PASSENGER_IN_TAXI_SEAT(myTaxiData, VS_FRONT_RIGHT)
					OR IS_PASSENGER_IN_TAXI_SEAT(myTaxiData)
					OR TAXI_SHUFFLE_PASSENGER_SEAT(myTaxiData)
						bSeatShuffled = TRUE
					ENDIF
				ENDIF
				
				//Construction arrival line
				//SetUp the CSite
				TAXI_OJ_S01_SETUP_CSITE()
				
				UPDATE_DYNAMIC_TAXI_REQUESTS()
				
				IF TAXI_HANDLE_DRIVING(myTaxiData,/*tTaxiOJ_DQ_Data,*/ 9)
			//	OR ( ( ((NOT IS_ENTITY_DEAD(piChickInGauntlet)) AND (GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,piChickInGauntlet) < fPedDistance))
			//			OR ((NOT IS_ENTITY_DEAD(viGauntlet)) AND (GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,viGauntlet) < fCarDistance)) )
			//		AND  IS_TAXI_RIDE_ALL_READY(myTaxiData) )
					
					REMOVE_BLIP(myTaxiData.blipTaxiDropOff)					
					
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE)
					
					//Reduce chance of player going wanted right here
					SET_CREATE_RANDOM_COPS(FALSE)
					CLEAR_AREA_OF_COPS(GET_PLAYER_COORDS(PLAYER_ID()), 50.0)
					SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger, PCF_DontInfluenceWantedLevel,TRUE)
			
			
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_FOR_FULL_STOP)
				ENDIF
				
//				IF NOT IS_ENTITY_DEAD(piChickInGauntlet)
//				AND NOT IS_ENTITY_DEAD(viGauntlet)
//					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(piChickInGauntlet), fPedDistance, 0, 0, 255, 100)
//					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(viGauntlet), fCarDistance, 0, 255, 255, 100)
//				ENDIF
				
				IF iUnderBridgeIndex >=2
					IF NOT bTaxiEarlyAggro
					AND (  IS_PED_INJURED(piChickInGauntlet)
						OR IS_ENTITY_DEAD(viGauntlet)
						OR ((NOT IS_ENTITY_DEAD(piChickInGauntlet)) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(piChickInGauntlet))
						OR ((NOT IS_ENTITY_DEAD(viGauntlet)) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(viGauntlet))
						OR ((NOT IS_ENTITY_DEAD(piChickInGauntlet)) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(piChickInGauntlet, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON))
						OR ((NOT IS_ENTITY_DEAD(viGauntlet)) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(viGauntlet, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)) 
						OR ((NOT IS_ENTITY_DEAD(piChickInGauntlet)) AND (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())) AND (GET_PLAYER_DISTANCE_FROM_ENTITY(piChickInGauntlet) < 1.0)) )
						
						IF ((NOT IS_ENTITY_DEAD(viGauntlet)) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(viGauntlet))
						OR ((NOT IS_ENTITY_DEAD(viGauntlet)) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(viGauntlet, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON))
							EXPLODE_VEHICLE(viGauntlet)
						ENDIF
						
						IF NOT IS_PED_INJURED(piChickInGauntlet)
							TASK_SMART_FLEE_PED(piChickInGauntlet, PLAYER_PED_ID(), 500, -1)
							SET_PED_KEEP_TASK(piChickInGauntlet, TRUE)
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(piChickInGauntlet)
						AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						AND (GET_PLAYER_DISTANCE_FROM_ENTITY(piChickInGauntlet) < 1.0)
							CLEAR_SEQUENCE_TASK(siTemp)
							OPEN_SEQUENCE_TASK(siTemp)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
							CLOSE_SEQUENCE_TASK(siTemp)
							TASK_PERFORM_SEQUENCE(piChickInGauntlet, siTemp)
							SET_PED_KEEP_TASK(piChickInGauntlet, TRUE)
						ENDIF
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"Early aggro triggered")
						
						TAXI_SET_FAIL(myTaxiData,"Aggro Heard Shot", TFS_ABANDONED_PASSENGER)
						
						bTaxiEarlyAggro = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE TRS_WAIT_FOR_FULL_STOP
				
				IF MUGGING_CAMS()
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
					
					blipZo = CREATE_BLIP_ON_ENTITY(myTaxiData.piTaxiPassenger ,FALSE)
					
					TAXI_RESET_TIMERS(myTaxiData,  TT_GENERIC)
					
					IF NOT IS_ENTITY_DEAD(piChickInGauntlet)
						CLEAR_PED_TASKS_IMMEDIATELY(piChickInGauntlet)
						TASK_TURN_PED_TO_FACE_ENTITY(piChickInGauntlet, PLAYER_PED_ID())
						SET_ENTITY_HEALTH(piChickInGauntlet, 115)
					ENDIF
					
					iPlayerHealth = GET_ENTITY_HEALTH(PLAYER_PED_ID())
					iPassengerHealth = GET_ENTITY_HEALTH(myTaxiData.piTaxiPassenger)
					
					START_AUDIO_SCENE("TAXI_GOT_U_NOW")
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SWITCH_JOB)
				ENDIF
				
				
			BREAK
			
			CASE TRS_SWITCH_JOB
				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
						IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger )
							SET_ENTITY_HEALTH(myTaxiData.piTaxiPassenger,2)
						ENDIF
					ENDIF
				#ENDIF
				
				SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_HIT_PED)
				
				IF IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger )
				OR ((NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger))
					AND IS_PED_FLEEING(myTaxiData.piTaxiPassenger) 
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.piTaxiPassenger, vDropOffUnderBridge) > 25.0)
					
					//Clear objective to knock him out.
					CLEAR_THIS_PRINT("TAXI_OBJ_GYN")
					
					//Clean up all blips------------------------------------
					IF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
						REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
					ENDIF
					
					IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger )
						REMOVE_BLIP(myTaxiData.blipTaxiPassenger )
					ENDIF
					
					//remove alonzo blip
					IF DOES_BLIP_EXIST(blipZo)
						REMOVE_BLIP(blipZo)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(piChickInGauntlet)
						IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
							
							STOP_AUDIO_SCENE("TAXI_GOT_U_NOW")
							
							//Here the player has won the fight by hand
							GET_CURRENT_PED_WEAPON(myTaxiData.piTaxiPlayer, currentWeapon)
							
							IF IS_WEAPON_NON_LETHAL(currentWeapon)
							OR (NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger))
								
								bFairFight = TRUE
								
								CLEAR_SEQUENCE_TASK(siTemp)
								OPEN_SEQUENCE_TASK(siTemp)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(siTemp)
								TASK_PERFORM_SEQUENCE(piChickInGauntlet, siTemp)
								
								//TASK_STAND_STILL(piChickInGauntlet,-1)
								
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GIRL_LEAVE,TRUE)//,FALSE,TRUE)
								
								myTaxiData.vTaxiOJDropoff = vDropOffChickHouse
								TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)
								
								TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SAVE_DAMSEL)
							
							//Here the player has killed the ped cause he's dead, and the player is not unarmed.
							ELSE
								
								bFairFight = FALSE
								
								TASK_SMART_FLEE_PED(piChickInGauntlet, PLAYER_PED_ID(), 500, -1)
								SET_PED_KEEP_TASK(piChickInGauntlet,TRUE)
								
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GIRL_BF_KILLED,TRUE,FALSE,TRUE)
								
								TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
							ENDIF
						ENDIF
					
					//Player killed chick
					ELSE
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
					ENDIF
					
				ELSE
					
					// make sure passenger stays in fight area
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vFightAreaPos1, vFightAreaPos2, fFightAreaWidth)
					
						IF (GET_ENTITY_HEALTH(myTaxiData.piTaxiPassenger) = GET_PED_MAX_HEALTH(myTaxiData.piTaxiPassenger))
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT bWarningLeeway
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GIRL_EGGON_PLAYER_NO_HIT,TRUE)
							bWarningLeeway = TRUE
						ENDIF
						
						IF NOT bTaxiGYNOneWarning
						AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > 1.5
						//AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
							SET_TAXI_OJ_PED_IN_COMBAT(myTaxiData.piTaxiPassenger)
							SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger, PCF_ForceIgnoreMeleeActiveCombatant, FALSE)
							bTaxiGYNOneWarning = TRUE
							CDEBUG1LN(DEBUG_OJ_TAXI,"passengered was tasked to combat")
						#IF IS_DEBUG_BUILD
						ELSE
							DRAW_DEBUG_TEXT_2D("passenger should be fighting", << 0.5, 0.25, 0 >>)	
						#ENDIF
						ENDIF
						
						
						// task the girls idles during the fight
						IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > TAXI_CONST_GOON_EGG_ON_DELAY
							CDEBUG1LN(DEBUG_OJ_TAXI,"hit egg on")
							IF NOT IS_ENTITY_DEAD(piChickInGauntlet)
								
								TASK_LOOK_AT_ENTITY(piChickInGauntlet, PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
								
								CLEAR_SEQUENCE_TASK(siTemp)
								OPEN_SEQUENCE_TASK(siTemp)
									
									TASK_PLAY_ANIM(NULL, "misscommon@response", "give_me_a_break")
									TASK_PLAY_ANIM(NULL, "misscommon@response", "damn")
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
							
								CLOSE_SEQUENCE_TASK(siTemp)
								TASK_PERFORM_SEQUENCE(piChickInGauntlet, siTemp)
								
								// we need to track who's winning at this point
								// let's try polling who's taken the most damage within the 8 second time
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									
									iPlayerHealth -= GET_ENTITY_HEALTH(PLAYER_PED_ID())
									iPassengerHealth -= GET_ENTITY_HEALTH(myTaxiData.piTaxiPassenger)
									
									CDEBUG1LN(DEBUG_OJ_TAXI,"hit egg on dialogue")
									
									IF iPlayerHealth < iPassengerHealth
										SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GIRL_EGGON_PLAYER_WINNING,TRUE)
									ELSE
										SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GIRL_EGGON_BF,TRUE)
									ENDIF
									
									// reset the current health values
									iPlayerHealth = GET_ENTITY_HEALTH(PLAYER_PED_ID())
									iPassengerHealth = GET_ENTITY_HEALTH(myTaxiData.piTaxiPassenger)
								ENDIF
								
							ELSE
								IF NOT bHealthBonusGiven
									KILL_ANY_CONVERSATION()
									// If girl dies, reset the passenger's health to account for his aggravation
									SET_ENTITY_HEALTH(myTaxiData.piTaxiPassenger, GET_PED_MAX_HEALTH(myTaxiData.piTaxiPassenger))
									CDEBUG1LN(DEBUG_OJ_TAXI,"hit KO dialogue")
									SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_GIRL_KOD, TRUE)
									bHealthBonusGiven = TRUE
									bFairFight = FALSE
								ENDIF
							ENDIF
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"piChickInGauntlet was tasked")
							TAXI_RESET_TIMERS(myTaxiData,  TT_GENERIC)
						ELSE
							IF (GET_GAME_TIMER() % 1000) < 50
								CDEBUG1LN(DEBUG_OJ_TAXI,"hit timer not ready")
							ENDIF
						ENDIF
					
					//if passenger follows player out of fight area, task him to go back to his chick
					// ideally it will look like they're arguing
					ELIF NOT IS_PED_FLEEING(myTaxiData.piTaxiPassenger)
						IF bTaxiGYNOneWarning
							
							CLEAR_PED_TASKS_IMMEDIATELY(myTaxiData.piTaxiPassenger)
							
							CLEAR_SEQUENCE_TASK(siTemp)
							OPEN_SEQUENCE_TASK(siTemp)
								IF IS_PED_INJURED(piChickInGauntlet)
									TASK_GO_STRAIGHT_TO_COORD(NULL, GET_ENTITY_COORDS(piChickInGauntlet, FALSE), PEDMOVEBLENDRATIO_SPRINT)
									TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(piChickInGauntlet, FALSE))
								ELSE
									TASK_GO_TO_ENTITY(NULL, piChickInGauntlet, DEFAULT_TIME_BEFORE_WARP, 3.5, PEDMOVEBLENDRATIO_WALK)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, piChickInGauntlet)
								//	TASK_CHAT_TO_PED(NULL, piChickInGauntlet, CF_AUTO_CHAT, << 0,0,0 >>, 0, 0)
								ENDIF
							CLOSE_SEQUENCE_TASK(siTemp)
							TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siTemp)
							
							IF NOT IS_PED_INJURED(piChickInGauntlet)
							//	CLEAR_SEQUENCE_TASK(siTemp)
							//	OPEN_SEQUENCE_TASK(siTemp)
									TASK_TURN_PED_TO_FACE_ENTITY(piChickInGauntlet, myTaxiData.piTaxiPassenger)
							//		TASK_CHAT_TO_PED(NULL, myTaxiData.piTaxiPassenger, CF_AUTO_CHAT, << 0,0,0 >>, 0, 0)
							//	CLOSE_SEQUENCE_TASK(siTemp)
							//	TASK_PERFORM_SEQUENCE(piChickInGauntlet, siTemp)
							ENDIF
							
							SETTIMERA(0)
							
							bTaxiGYNOneWarning = FALSE
						ENDIF
						
						IF NOT IS_PED_INJURED(piChickInGauntlet)
							IF VDIST2(GET_ENTITY_COORDS(myTaxiData.piTaxiPassenger), GET_ENTITY_COORDS(piChickInGauntlet, FALSE)) < 25.0
							AND (GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_CHAT_TO_PED) <> PERFORMING_TASK
								OR GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_CHAT_TO_PED) <> WAITING_TO_START_TASK)
								TASK_CHAT_TO_PED(piChickInGauntlet, myTaxiData.piTaxiPassenger, CF_AUTO_CHAT, << 0,0,0 >>, 0, 0)
								TASK_CHAT_TO_PED(myTaxiData.piTaxiPassenger, piChickInGauntlet, CF_AUTO_CHAT, << 0,0,0 >>, 0, 0)
							ENDIF
						ENDIF
						
						// give the player 30 seconds outside of the zone before failing him
						IF TIMERA() > 30000
							CDEBUG1LN(DEBUG_OJ_TAXI,"IS THAT HIT??")
							TAXI_SET_FAIL(myTaxiData, "Player abandoned passenger", TFS_ABANDONED_PASSENGER)
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_PLAYER_DISTANCE_FROM_LOCATION(myTaxiData.vTaxiOJDropoff) > 250 //500
				AND DOES_BLIP_EXIST(blipZo)	//that makes it so you only fail while fight is active
					CDEBUG1LN(DEBUG_OJ_TAXI,"IS THIS HIT??")
					TAXI_SET_FAIL(myTaxiData, "Player abandoned passenger", TFS_ABANDONED_PASSENGER)
				ENDIF
			BREAK
			
			
			
			//------------------------------------NEW STUFF-------------------------------------------
			CASE TRS_SAVE_DAMSEL
//				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),piChickInGauntlet) < 5.5 //4.0
//				AND IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
//					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), FALSE)
//				ENDIF
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 2.0
					
					myTaxiData.blipTaxiPassenger = CREATE_BLIP_FOR_ENTITY(piChickInGauntlet)
					SET_BLIP_NAME_FROM_TEXT_FILE(myTaxiData.blipTaxiPassenger,"TX_BLIP_GYN_TG")
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GYN,TRUE)
					
					SET_CREATE_RANDOM_COPS(TRUE)
					
					TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_PRE_CUTSCENE)
				ENDIF
			BREAK
			
			CASE TRS_PRE_CUTSCENE
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),piChickInGauntlet) < 3.0
				AND NOT IS_PLAYER_DRIVING_ANY_VEHICLE()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					
					myTaxiData.bSecondPassenger = TRUE
					CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_BANTER)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CUTSCENE)
				ELSE
					IF NOT IS_PED_INJURED(piChickInGauntlet)
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_TURN_PED_TO_FACE_ENTITY(piChickInGauntlet, PLAYER_PED_ID())
					ENDIF
				ENDIF
			BREAK

			
			CASE TRS_CUTSCENE
				IF TAXI_OJ_GYN_CUTSCENE_SAVE_CHICK()
					//Remove passenger blip
					IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger )
						REMOVE_BLIP(myTaxiData.blipTaxiPassenger )
					ENDIF
					
					SET_PED_AS_NO_LONGER_NEEDED(myTaxiData.piTaxiPassenger)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_FOR_PASSENGER)
				ENDIF
			BREAK 
			
			CASE TRS_WAIT_FOR_PASSENGER
				IF NOT myTaxiData.bPassengerObjPrinted
				AND NOT ARE_VECTORS_EQUAL(myTaxiData.vTaxiOJ_PassengerGoToPt, vChickPtToWalkTo)
				
					IF IS_PED_IN_ANY_VEHICLE(piChickInGauntlet)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
						myTaxiData.piTaxiPassenger = piChickInGauntlet
						myTaxiData.vTaxiOJ_PassengerGoToPt = vChickPtToWalkTo
						myTaxiData.vTaxiOJDropoff = vDropOffChickHouse
						
						// since it's a new passenger, reset the gate for these bits if they've been set
						IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_RETURN_TO_TAXI)
							CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_RETURN_TO_TAXI)
						ENDIF
						IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_REGIVE_OBJ)
							CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_REGIVE_OBJ)
						ENDIF
						
						myTaxiData.iTaxiOJ_DXBitsStopped = 0
						
						
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GIRL_DEST,TRUE)
					ENDIF
				ELIF myTaxiData.bPassengerObjPrinted
						
					myTaxiData.blipTaxiDropOff = CREATE_BLIP_FOR_COORD(vDropOffChickHouse,TRUE)
					SET_BLIP_NAME_FROM_TEXT_FILE(myTaxiData.blipTaxiDropOff,"TX_BLIP_GYN_GF")
					
					RELEASE_TAXI_ODDJOB_GYN_STREAMS_STAGE_02()
					
					TAXI_CANCEL_TIMERS(myTaxiData, TT_STOPPED)
					
					OPEN_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,5)
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DROPPING_OFF)
				ENDIF
			BREAK
			
			CASE TRS_DROPPING_OFF
				
				IF TAXI_HANDLE_DRIVING(myTaxiData/*,tTaxiOJ_DQ_Data*/)
					REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
					
					CLEAR_DIALOGUE_QUEUE(tDialogueLine)
					CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,-1,TRUE)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GIRL_THANKS,TRUE)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
				ENDIF
			BREAK
			/*	REGULAR PAYMENT------------------------------------------------------------------------------------------------
	
oooooooooo   o   ooooo  oooo oooo     oooo ooooooooooo oooo   oooo ooooooooooo 
 888    888 888    888  88    8888o   888   888    88   8888o  88  88  888  88 
 888oooo88 8  88     888      88 888o8 88   888ooo8     88 888o88      888     
 888      8oooo88    888      88  888  88   888    oo   88   8888      888     
o888o   o88o  o888o o888o    o88o  8  o88o o888ooo8888 o88o    88     o888o    
      */
			CASE TRS_REGULAR_PAYMENT
				IF HAS_TAXI_OJ_PASSENGER_BEEN_DROPPED_OFF(myTaxiData,TRUE)
				OR IS_PED_PERFORMING_TASK(piChickInGauntlet, SCRIPT_TASK_SMART_FLEE_PED)
				OR IS_ENTITY_DEAD(piChickInGauntlet)
					
				//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//TAXI_OJ_TASK_PASSENGER_DROPOFF(myTaxiData)
					
					IF bFairFight
						CLEAR_SEQUENCE_TASK(siTemp)
						OPEN_SEQUENCE_TASK(siTemp)
							//TASK_GO_STRAIGHT_TO_COORD(NULL, << -617.7251, -763.0566, 24.9895 >>, PEDMOVEBLENDRATIO_WALK)
							//TASK_GO_STRAIGHT_TO_COORD(NULL, << -615.6333, -778.8123, 24.1921 >>, PEDMOVEBLENDRATIO_WALK)
							//TASK_GO_STRAIGHT_TO_COORD(NULL, myTaxiData.vTaxiOJ_PassengerGoToPt, PEDMOVEBLENDRATIO_WALK)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-612.6458, -777.7148, 24.2700>>, PEDMOVEBLENDRATIO_WALK)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-608.3197, -775.0622, 24.0547>>, PEDMOVEBLENDRATIO_WALK)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-589.0419, -775.1888, 24.0172>>, PEDMOVEBLENDRATIO_WALK)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, myTaxiData.vTaxiOJ_PassengerGoToPt, PEDMOVEBLENDRATIO_WALK)//, DEFAULT_TIME_BEFORE_WARP)
							//TASK_WANDER_STANDARD(NULL)
							TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
						CLOSE_SEQUENCE_TASK(siTemp)
						TASK_PERFORM_SEQUENCE(piChickInGauntlet, siTemp)	
						
						SET_PED_KEEP_TASK(piChickInGauntlet, TRUE)
						
						TAXI_SET_BONUS_AWARD(myTaxiData,ENUM_TO_INT(TGYN_BONUS_FLAWLESS_VICTORY))
						
						SET_BOOTY_CALL_ID_ACTIVATED(BC_TAXI_LIZ, TRUE)
					ENDIF
					
					SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)
					
					CALCULATE_TAXI_GYN_JOB_REWARDS()
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)
						
				ENDIF
			BREAK
			
			/*Pop up the scorecard---------------------------------------------------------------------------------------

 oooooooo8    oooooooo8   ooooooo  oooooooooo  ooooooooooo  oooooooo8 
888         o888     88 o888   888o 888    888  888    88 o888     88 
 888oooooo  888         888     888 888oooo88   888ooo8   888         
        888 888o     oo 888o   o888 888  88o    888    oo 888o     oo 
o88oooo888   888oooo88    88ooo88  o888o  88o8 o888ooo8888 888oooo88  
                                                                      */
			CASE TRS_SCORECARD_GRADE
				IF TAXI_CALC_SCORECARD(myTaxiData,TaxiMidSize)
					TAXI_MISSION_END(TRUE, myTaxiData)	
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CLEANUP)
				ENDIF
			
			BREAK
			
			CASE TRS_CLEANUP
				Script_Cleanup()
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC
SCRIPT
	//FIX FOR BUILD
	IF ARE_VECTORS_EQUAL(vDropOffUnderBridge,<<0,0,0>>)
		
	ENDIF


	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		Script_Cleanup()
	ENDIF
	
	// Any initialisation (generally, only mission scripts should set
	// the mission flag to TRUE)
	SET_MISSION_FLAG(TRUE)
	
	INITIALIZE_SCRIPT_VARIABLES()
	
	// The script loop
	WHILE (TRUE)
		// Maintain the script – perform per-frame functionality
	//All skips and debug shortcuts-------------------------------
	#IF IS_DEBUG_BUILD
	
		PROCESS_TAXI_DEBUG_SKIP(myTaxiData,tDebugState)
	
		// Debug Key: Check for Pass (not for Minigmes)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			TAXI_ODDJOB_DEBUG_SKIP_TO_SCORECARD(myTaxiData)
		ENDIF

		// Debug Key: Check for Fail (not for Minigames)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			TAXI_DEBUG_FAIL_TRIGGERED(myTaxiData)
			Script_Failed()
		ENDIF
		
		// Debug Key: set vehicle health to 0.0
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
			IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				SET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi, 50.0)
				SET_ENTITY_HEALTH(myTaxiData.viTaxi, 50)
			ENDIF
		ENDIF			
		
		PROCESS_WIDGETS()
		
	#ENDIF
	//END DEBUG----------------------------------------------------
		
		IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPlayer)
			Main_Taxi_OJ_GotYouNow()
		ELSE
			REASSIGN_TAXI_OJ_DRIVER(myTaxiData)
		ENDIF
	
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
