USING "script_maths.sch"

ENUM TAXI_JOB_BITS
	TAXI_OJ_JB_DISABLE_WANTED 	= BIT0,
	TAXI_OJ_JB_HORN_HONKED		= BIT1,
	TAXI_OJ_JB_GREETING_DONE	= BIT2,
	TAXI_OJ_JB_GET_VIP			= BIT3,
	TAXI_OJ_JB_SMALL_CAR_HELP	= BIT4,
	TAXI_OJ_JB_GET_CAR_HELP		= BIT5,
	TAXI_OJ_JB_CAR_DMGED		= BIT6,
	TAXI_OJ_JB_CAR_JACKED		= BIT7,
	TAXI_OJ_JB_CAR_JACKING		= BIT8,
	TAXI_OJ_JB_TOTAL
ENDENUM

//Debug Consts

CONST_INT		DEBUG_iShowDrivingFare					0
CONST_INT		DEBUG_iTurnOnAllDXDebug					0

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



CONST_FLOAT		CONST_iDistanceToBlipPassenger			35.0
CONST_INT		CONST_iTimeBetweenGodTextAndDialogue	10 //ms
CONST_INT		CONST_iTaxiPassengerWaitTimeMin			4
CONST_INT		CONST_iTaxiPassengerWaitTimeMax			7
CONST_FLOAT     CONST_TAXI_PASSENGER_PICKUP_DIST        28.0
CONST_FLOAT     CONST_TAXI_PASSENGER_PICKUP_DIST_FAIL   30.0
CONST_FLOAT     TAXI_CONST_DELAY_FAIL                   4.0

CONST_FLOAT     TAXI_CONST_FAIL_FOR_NOT_PICKING_UP      15.0	//This is the amount of time you have to pick up the passenger

CONST_FLOAT		TAXI_CONST_DEBUG_FADE_FAILSAFE			10.0

CONST_FLOAT     TAXI_TWEAK_DEADLINE                     0.275       //What is multipled to the distance to get a deadline time eta. Lower means harder
CONST_FLOAT     TAXI_TWEAK_GROUP_DEADLINE               0.5         //What is multipled to the distance to get a deadline time eta. Lower means harder
CONST_INT       TAXI_MONEY_BONUS_MIN_BAG_RETURN         5   //Min reward for returning bag
CONST_INT       TAXI_MONEY_BONUS_MAX_BAG_RETURN         20  //Max reward for returning bag
CONST_INT       TAXI_MONEY_BONUS_MIN_BAG_KEEP           20  //Min reward for returning bag
CONST_INT       TAXI_MONEY_BONUS_MAX_BAG_KEEP           100 //Max reward for returning bag
CONST_FLOAT     TAXI_CONST_IKW_BLIP_DISTANCE            140.0   //The distance before the player has gotten close enough to blip the location for I KNOW WAY.
CONST_FLOAT     TAXI_FAILSAFE_NO_CONTROL_TIME           4.0
CONST_FLOAT     CONST_TAXI_BAG_CS1_TIME                 5.0
CONST_FLOAT     CONST_TAXI_BAG_CS2_TIME                 5.0

CONST_FLOAT     CONST_TAXI_RADIO_DISLIKE_TIME           5.0

CONST_FLOAT     TAXI_AREA_SAFE_AROUND_PASSENGER         30.0    //Radius around the passenger to turn off the car spawn generatos
CONST_FLOAT     TAXI_AREA_ROAD_DISABLE_RADIUS           60.0    //Radius around the passenger to turn off the car spawn generatos
CONST_FLOAT     TAXI_AREA_CSITE_DISABLE_RADIUS          100.0   //Radius around the passenger to turn off the car spawn generatos

CONST_FLOAT     TAXI_TWEAK_ETA                          0.05    //The pickup timer tweaker -> Lesser means harder
CONST_INT       TAXI_LOOK_AT_TIME                       3

CONST_INT       TAXI_CAM_PICKUP_INTERP_IN               1000
CONST_INT       TAXI_CAM_PICKUP_INTERP_OUT              1000

CONST_FLOAT     TAXI_BANTER_MIN                         12.0
CONST_FLOAT     TAXI_BANTER_MAX                         25.0

//Timers
CONST_FLOAT     TAXI_FAIL_LOST_CAR                      1.0
CONST_FLOAT     TAXI_ANIM_WAVE_FREQ                     6.0
CONST_FLOAT     CONST_TAXI_TIME_UPDATE                  35.0
CONST_FLOAT		TAXI_CONST_TIME_FOR_UNDER_WATER_FAIL	2.0 //4.0

//Excitement
CONST_FLOAT		TAXI_DISTANCE_TO_IGNORE_EXCITEMENT		200.0	//When this close to dropoff don't allow failing for excitement
CONST_FLOAT     TAXI_EXCITEMENT_TOO_SLOW_SPEED          15.0

CONST_FLOAT     HUD_2D_WAIT_FOR_MORE_TEXT               2.0
CONST_FLOAT     HUD_TEXT_SCROLL_SPEED                   1.2
CONST_FLOAT     TAXI_TAKEITEASY_DEGRADE                 3.0
CONST_FLOAT     TAXI_COMFORT_MULT                       2.0
CONST_INT       TAXI_EASY_AIR_BONUS                     5
CONST_INT       TAXI_EXCITEMENT_AIR                     2
CONST_INT       TAXI_HIT_PED_PENALTY                    50
CONST_INT       TAXI_PENALTY_COUNTER                    100 //How many milliseconds to check for when dealing a penalty

CONST_INT       TAXI_DEFAULT_TIME_BEFORE_WARP           20000

CONST_FLOAT		TAXI_OJ_DIST_TO_WALK_TO_DEST			200.0		//Distance from dropoff pt to tell passenger to walk to dest if failed
CONST_FLOAT     TAXI_OJ_DIST_SC_FAIL                    400.0
CONST_FLOAT     TAXI_OJ_DIST_SC_FAIL_CYI                250.0
CONST_FLOAT     TAXI_OJ_DIST_CLEAR_VEHICLE_DROPOFF      180.0
CONST_FLOAT     TAXI_OJ_RADIUS_CLEAR_VEHICLE            25.0
CONST_FLOAT     TAXI_OJ_RADIUS_CLEAR_PED	            2.0
CONST_FLOAT     TAXI_OJ_RADIUS_VISIBLE_MULT             5.0
CONST_FLOAT     TAXI_STOP_VEHICLE_DISTANCE              5.0

CONST_FLOAT     TAXI_DROPOFF_X_COORD                    2.0
CONST_FLOAT     TAXI_DROPOFF_Y_COORD                    2.0 
CONST_FLOAT     TAXI_DROPOFF_Z_COORD                    20.0

CONST_FLOAT     TAXI_DROPOFF_X_LARGE                    8.0
CONST_FLOAT     TAXI_DROPOFF_Y_LARGE                    5.0 
CONST_FLOAT     TAXI_OJ_LOS_LENGTH                      85.0    //Distance away from passenger before checking if he can see/hail the taxi

CONST_FLOAT     CONST_TAXI_OJ_DISTANCE_AWAY_TO_WARP     20.0    //Minimum radius taxi has to be from point to trigger J Skip

CONST_FLOAT     CONST_TAXI_TIME_STOPPED_TOO_LONG 		15.0
CONST_FLOAT     CONST_TAXI_TIME_STOPPED_TOO_LONG_FAIL 	80.0

CONST_INT       TAXI_OJ_AGGRO_TIMES_BEFORE_FAIL         2
CONST_INT		CONST_TaxiVIP_Aggro_Aim_Delay			250
CONST_INT		CONST_TaxiVIP_Aggro_Aim_Range			40

//Fares & Tips----------------------------------------------
CONST_INT		TAXI_BASE_FARE				100

CONST_FLOAT		TAXI_FARE_DIST_SHORT		2.0
CONST_FLOAT		TAXI_FARE_DIST_MED			3.0
CONST_FLOAT		TAXI_FARE_DIST_LONG			4.0


//--------------------------------------------------------------------------
CONST_FLOAT		TAXI_OJ_CONST_GATEWAY_SIZE_SMALL				6.0
CONST_FLOAT		TAXI_OJ_CONST_GATEWAY_SIZE_LARGE				8.0
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_STOPPED		4					//Should match the number of lines in dialogue star sheet!
CONST_FLOAT		TAXI_OJ_CONST_TIME_BETWEEN_POLICE_REACTIONS			10.0 //16.0
CONST_INT		TAXI_OJ_FAIL_BIT_STOPPED 				BIT3	//Should be equal BIT # where # is CONST_TAXI_OJ_NUM_DX_LINES_STOPPED -1
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_SWEETSPOT_FTC		6					//Should match the number of lines in dialogue star sheet!
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_KILLED_GYN		6					//Should match the number of lines in dialogue star sheet!
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_EGG_ON_GYN		5					//Should match the number of lines in dialogue star sheet!
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_EGG_ON_2_GYN		5					//Should match the number of lines in dialogue star sheet!
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_COLLIDE			5					//Should match the number of lines in dialogue star sheet!
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_COLLIDE_NEX		8					//Should match the number of lines in dialogue star sheet!
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_COLLIDE_TIE		5					//Should match the number of lines in dialogue star sheet!

CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_CYI_RETURN_TO_CAR		3					//Should match the number of lines in dialogue star sheet!
CONST_INT		TAXI_OJ_FAIL_BIT_CYI_RETURN_TO_CAR 		BIT2	//Should be equal BIT # where # is CONST_TAXI_OJ_NUM_DX_LINES_STOPPED -1

CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_SPEED		6
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_OFFROAD		4
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_TIME_GOOD	8
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_TIME_BAD		8
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_TIME_MISC_GOOD 3

CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_GOOD_TIE		6
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_BG_TRANS		2

CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_TIME_GOOD_IKW	3
CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_TIME_BAD_IKW		3

CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_ROLL			2

CONST_INT		CONST_TAXI_OJ_NUM_DX_LINES_POLICE		6
CONST_INT		TAXI_OJ_FAIL_BIT_POLICE 				BIT5	//Should be equal BIT # where # is CONST_TAXI_OJ_NUM_DX_LINES_POLICE -1

CONST_FLOAT		CONST_TAXI_OJ_TIME_AFTER_BANTER			25.0
CONST_FLOAT		CONST_TAXI_OJ_MIN_TAXI_HEALTH_TO_BAIL	80.0


//Taxi Cheats-----------------------------------------------
CONST_FLOAT		TAXI_CHEATS_ADD_TIME		60.0	//How much time the cheat gives us

//UI Consts-------------------------------------------------
CONST_FLOAT TAXI_UI_CR_HEAD_SCALE				1.0				// UI choose race heading scale (x/y).
CONST_FLOAT TAXI_UI_CR_HEAD_POS_X				0.4				// UI choose race heading pos (x).
CONST_FLOAT TAXI_UI_CR_HEAD_POS_Y				0.15			// UI choose race heading pos (y).
CONST_FLOAT TAXI_UI_CR_INFO_POS_Y				0.25			// UI choose race info pos (y).
CONST_FLOAT TAXI_UI_CR_INFO_SPC_Y				0.052			// UI choose race info space (y).
CONST_FLOAT TAXI_UI_CR_INFO_SCALE				0.5				// UI choose race info scale (x/y).
CONST_FLOAT TAXI_UI_CR_INFO_POS_X				0.325			// UI choose race info pos (x).
CONST_FLOAT TAXI_UI_CR_SEL_SCALE				0.625			// UI choose race select scale (x/y).
CONST_FLOAT TAXI_UI_CR_SEL_POS_X				0.32			// UI choose race select pos (x).
CONST_FLOAT TAXI_UI_CR_SEL_POS_Y				0.81			// UI choose race select pos (y).
CONST_FLOAT TAXI_UI_CR_ENT_SCALE				0.625			// UI choose race enter scale (x/y).
CONST_FLOAT TAXI_UI_CR_ENT_POS_X				0.435			// UI choose race enter pos (x).
CONST_FLOAT TAXI_UI_CR_ENT_POS_Y				0.81			// UI choose race enter pos (y).
CONST_FLOAT TAXI_UI_CR_EXIT_SCALE				0.625			// UI choose race exit scale (x/y).
CONST_FLOAT TAXI_UI_CR_EXIT_POS_X				0.614			// UI choose race exit pos (x).
CONST_FLOAT TAXI_UI_CR_EXIT_POS_Y				0.81			// UI choose race exit pos (y).
CONST_FLOAT TAXI_UI_CR_RECT_POS_X				0.5				// UI choose race rectangle pos (x).
CONST_FLOAT TAXI_UI_CR_RECT_POS_Y				0.5				// UI choose race rectangle pos (y).
CONST_FLOAT TAXI_UI_CR_RECT_WIDTH				0.375			// UI choose race rectange width.
CONST_FLOAT TAXI_UI_CR_RECT_HEIGHT				0.750			// UI choose race rectangle height.
CONST_INT 	TAXI_UI_CR_RECT_ALPHA				191				// UI choose race rectange alpha.
CONST_FLOAT	TAXI_UI_DIST_TO_DEST_DEBUG_X		0.05			//Used for the x-screen coord of the distance from Taxi to Escapee
CONST_FLOAT	TAXI_UI_DESC_TO_DEST_DEBUG_X		0.05			//Used for the x-screen coord of the distance from Taxi to Escapee
CONST_FLOAT	TAXI_UI_LOWEST_DEBUG_Y_ABOVE_MINI_MAP		0.72			//Used for the y-screen coord of the distance from Taxi to Escapee
CONST_FLOAT	TAXI_UI_DIST_FROM_ESCAPEE_DEBUG_Y	0.70			//Used for the y-screen coord of the distance from Taxi to Escapee
CONST_FLOAT	TAXI_UI_DIST_TO_ESCAPEE_DEBUG_S		0.395			//Used for the scale of the distance from Taxi to Escapee

CONST_FLOAT	TAXI_UI_TAXI_SPEED_DEBUG_X			0.155			//Used for the x-screen coord of the distance from Taxi to Escapee
CONST_FLOAT	TAXI_UI_TAXI_SPEED_DEBUG_Y			0.890			//Used for the x-screen coord of the distance from Taxi to Escapee
CONST_FLOAT	TAXI_UI_TAXI_MAX_SPEED_DEBUG_X		0.05			//Used for the y-screen coord of the distance from Taxi to Escapee
CONST_FLOAT	TAXI_UI_TAXI_MAX_SPEED_DEBUG_Y		0.74			//Used for the scale of the distance from Taxi to Escapee

//Regions---------------------------------------------------
CONST_INT		TAXI_LOCATIONS 				107




//*****Probably good candidate for deletion
//Trip Times-----------------------------------------------
CONST_INT		TAXI_LONG_TRIP_TIME		480
CONST_INT		TAXI_MED_TRIP_TIME		240
CONST_INT		TAXI_SHORT_TRIP_TIME	120

//Tweakables

//How many times before triggerring------------------------
TWEAK_INT       TAXI_TWEAKS_NUM_HONKS_BEFORE_BITCH      30

//Excitement Values----------------------------------------
TWEAK_FLOAT     TAXI_TWEAKS_VALUE_HORN_EXCITEMENT       5.0     //How much excitement from honking the horn

TWEAK_FLOAT     TAXI_TWEAK_TIME_FAIL_ABANDONED_CAR_NO_PASSENGER      20.0    //Min time before failing for not returning to the taxi when not with a  passenger
TWEAK_FLOAT     TAXI_TWEAK_TIME_FAIL_ABANDONED_CAR      20.0   				 //Min time before failing for not returning to the taxi when with a passenger

TWEAK_FLOAT     TAXI_TWEAKS_SPEED_FACTOR                5.0     //z radius to pickup a passenger
TWEAK_INT       TAXI_TWEAKS_SPEED_ALPHA                 150
TWEAK_FLOAT     TAXI_TWEAKS_PICKUP_PAD                  4.0

//Time Delays-----------------------------------------------
TWEAK_FLOAT     TAXI_TWEAKS_TIME_DX_DELAY_POLICE        1.0 //time delay before barking about wanted level 

//EOF
