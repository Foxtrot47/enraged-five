// controller_Taxi.sch

ENUM TAXI_CONTROL_STATE
	TXC_INIT_CONTROLLER,
	TXC_INIT_TAXI_RANK,
	TXC_WAIT_FOR_ACCEPT,
	TXC_COMMENT_ON_DISTANCE,
	TXC_TAXI_RANK_WAIT,
	TXC_DISPATCH_ASK,
	TXC_DISPATCH_CONFIRM,
	TXC_PLAYER_ACCEPT,
	TXC_ACTIVATE,
	TXC_LAUNCH_TAXI_RANK,
	TXC_TAXI_RANK_UPDATE,
	TXC_CLEANUP_TAXI_RANK,
	TXC_CLEANUP_CONTROLLER
ENDENUM

ENUM TAXI_NO_MISSION_REASON
	TAXI_CAN_RUN_MISSION,
	TAXINOMISSION_NOTAXI,
	TAXINOMISSION_HEALTHGONE,
	TAXINOMISSION_WANTED
ENDENUM

ENUM TAXI_MISSION_TYPES
	TXM_00_INVALID = -1,
	
	// Regular set
	TXM_01_NEEDEXCITEMENT,		// Drive erratically, barely missing cars, going fast, hitting jumps
	TXM_02_TAKEITEASY,			// Drive carefully to destination
	TXM_03_DEADLINE,			// Get to a destination fast
	TXM_04_GOTYOURBACK,			// Player has to keep passenger safe from a gang shoot out.
	TXM_05_TAKETOBEST,			// "Take me to the best ____ in town." (club, bar, store, beach, etc.)
	//TXM_06_IKNOWWAY,			// Guy gives verbal directions, as he doesn't know the exact name of place.
	TXM_07_CUTYOUIN,			// Take a guy around, he gives you a cut of what he does.
	TXM_08_GOTYOUNOW,			// Player is attacked in the cab.
	TXM_09_CLOWNCAR,			// Pick up a large group and have multiple dropoffs under the deadline
	TXM_10_FOLLOWTHATCAR,		// Follow another car to it's destination
	
	TXM_PROCEDURAL,
	
	TXM_NUM_TYPES
	
ENDENUM

ENUM TAXI_OFFER_STATE
	TXO_WAIT_PRINT_OFFER = 0,
	TXO_WAIT_OFFER_DISP,
	TXO_WAIT_ACCEPT,
	TXO_WAIT_REOFFER
ENDENUM

CONST_INT TAXI_MAX_PICKUP 1500 //2500
CONST_INT TAXI_MIN_PICKUP 100

