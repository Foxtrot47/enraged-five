//=======================================================================================================================================

// Taxi_TakeToBest.sc
// Dev : John R. Diaz
/*
	•	“Take me to the best…” This passenger does not give a specific destination but rather asks the player to take them to a type of location.
		This could be a strip club, a restaurant, or a clothing store. The passenger will talk about what kind of place they are looking for and 
		provide the player clues to their preferences. The player will receive bonus tips based on how much the passenger likes the place they 
		are dropped at or no tip if it’s a terrible choice. (For example: the conservative straight passenger heading to hit the clubs will not 
		pay the driver that drops them at the gay bar.)
*/

//CHANGELOG==========================================================================================================
//11/29/11 - Removed Blip scaling on shop blips on radar per #290240
//9/27/11 - Reformatting/prettifying all taxi oj scripts
//===================================================================================================================


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.



//Includes------------------------------------------------------
USING "Taxi_Includes.sch"

USING "shop_public.sch"


//Consts
CONST_INT						TAXI_SHOPS_MAX_NUM_SHOPS	3	
CONST_INT						TAXI_TTB_TIP_HIGH			30
CONST_INT						TAXI_TTB_TIP_MED			10
//Local Variables-----------------------------------------------
CONST_INT						TAXI_CONST_BONUS_CASH_BEST_STORE		500

TaxiStruct 						myTaxiData

SHOP_NAME_ENUM 					targetShopName, nearestShopName

MODEL_NAMES 					mPassengerModel = A_F_Y_BEVHILLS_03

ENUM TAXIOJ_DL_BONUS
	TTTB_BONUS_GENERIC,
	TTTB_BONUS_TOTAL				
ENDENUM

BONUS_FIELD						bonusTakeToBest[TTTB_BONUS_TOTAL]

//Native Data
BLIP_INDEX 						blipTaxiShop[TAXI_SHOPS_MAX_NUM_SHOPS]

SEQUENCE_INDEX					siTemp

//Vectors
VECTOR 							vPassengerPt = << -412.0875, 1171.3588, 324.8176 >>	//Los Santos Obeservatory
VECTOR 							vPassengerPickupPt = << -411.0378, 1175.7334, 324.6417 >>
VECTOR 							vTaxiDropoffShopPos[NUMBER_OF_SHOPS]				//Droppoints
VECTOR 							vTaxiDropoffPOIPos[NUMBER_OF_SHOPS]				//Passenger POI Walk Pts
VECTOR 							vTaxiDropoffWarpPos[NUMBER_OF_SHOPS]			//Taxi Warp Pts
VECTOR							vTaxiInStorePos[NUMBER_OF_SHOPS]
VECTOR							vTaxiCurrentWalkInStorePos

//Bools
BOOL 							bNotCheckInModeOn = FALSE //FALSE TRUE
BOOL 							bLeftTaxi
BOOL 							bWentWanted
//BOOL 							bShowDebug = TRUE // TRUE FALSE 

//Ints
INT 							iDebugThrottle = 1

#IF IS_DEBUG_BUILD
INT 							iBestShopIndex = 0
#ENDIF	//	IS_DEBUG_BUILD
INT								iTipIndex = 0


BOOL 							bCheckMidComplete = FALSE
BOOL 							bCheckEndComplete = FALSE
BOOL							bFirstDropoffBlipEnabled = FALSE

//Floats
FLOAT 							fTaxiDropoffWarpHeading[NUMBER_OF_SHOPS]

//Dialogue Queue Info
BOOL							g_bDebug = FALSE
TAXI_OJ_DIALOGUE_Q_DATA			tTaxiOJ_DQ_Data								
TAXI_OJ_DQ_CONVERSATION_LINE	tDialogueLine[CONST_TAXIOJ_SIZE_Q]

AGGRO_ARGS 						aggroArgs 

SCRIPT_SHARD_BIG_MESSAGE TaxiMidSize
//Debug---------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 			taxiRideWidgets,ojTaxiWidgets_Shops
	BOOL 						bShowDropoffPt, bShowBestChoice
	BOOL						bDebugTurnOnFreeRide
	TXM_DEBUG_SKIP_STATES 		tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS
#ENDIF


//FUNCTIONS------------------------------------------------------------------------------------------------------------

/*
ooooooooo  ooooo      o      ooooo         ooooooo     ooooooo8  
 888    88o 888      888      888        o888   888o o888    88  
 888    888 888     8  88     888        888     888 888    oooo 
 888    888 888    8oooo88    888      o 888o   o888 888o    88  
o888ooo88  o888o o88o  o888o o888ooooo88   88ooo88    888ooo888  
*/
PROC TRIGGER_TAXI_QUEUE_TTB_LINES()

	SET_TAXI_OJ_INTERRUPT_TIMER_OFF(myTaxiData)
	
	//Trigger lines
	IF IS_BANTER_SAFE_TO_PLAY(myTaxiData,tTaxiOJ_DQ_Data)
		
		SWITCH tTaxiOJ_DQ_Data.iCurrentDQLine
			//Make sure if player interrupts the initial line, that the objective prints anyway.
			CASE 0
				IF myTaxiData.tTaxiOJ_RideState =  TRS_DRIVING_PASSENGER
					
					IF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_TTB_DO")
						OR DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
							CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO J Store has been  assigned")
							tTaxiOJ_DQ_Data.iCurrentDQLine++
							
						ELSE
							IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_OBJ_GIVE_MAIN
							
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE,FALSE,TRUE)
								CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO J Store has been REassigned")

							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 1
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_TTB_BANTER
				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"Current speech index:", GET_TAXI_SPEECH_INDEX(myTaxiData))
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_TTB_BANTER,TRUE)
					IF g_bDebug
						SCRIPT_ASSERT("Triggering TTB Banter ")
					ENDIF
				ENDIF			
			BREAK
			
			CASE 2
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > GET_RANDOM_FLOAT_IN_RANGE(2.0,10.0)
					IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_3
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_3,TRUE)
						
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter 3")
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > GET_RANDOM_FLOAT_IN_RANGE(2.0,10.0)
					IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_4
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_4,TRUE)
						
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter 4")
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 30.0//GET_RANDOM_FLOAT_IN_RANGE(2.0,10.0)
					IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_2
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_2,TRUE)
						
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter 2")
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 5
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > GET_RANDOM_FLOAT_IN_RANGE(8.0,16.0)
					//Set Radio Station Check
//					IF NOT GET_TAXI_RADIO_CHECK_FLAG(myTaxiData)	
//						TAXI_RADIO_STATION_TURN_ON(myTaxiData)
//						IF g_bDebug
//							SCRIPT_ASSERT("The Taxi Radio Should Be Updating")
//						ENDIF
//					ENDIF
				ENDIF
			BREAK
		ENDSWITCH				
	ENDIF				
	
	PROCESS_IMPORTANT_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, g_bDebug)

ENDPROC
PROC TAXI_SHOP_SETUP_BLIPS(BLIP_INDEX &blipShop, SHOP_NAME_ENUM eShopName, BLIP_SPRITE eBlipSprite, INT iBlipColor = BLIP_COLOUR_YELLOW )
	
	//Make my own
	IF DOES_BLIP_EXIST(blipShop)
		REMOVE_BLIP(blipShop)
	ENDIF
	
	blipShop = ADD_BLIP_FOR_COORD(GET_SHOP_COORDS(eShopName))
	
	SET_BLIP_SPRITE(blipShop, eBlipSprite)
	SET_BLIP_COLOUR(blipShop,iBlipColor)
	// B* 926655 Taxi Take To Best: The map refers to all blipped clothing stores as "Discount Store". - have to hard code clothes store for now
	SET_BLIP_NAME_FROM_TEXT_FILE(blipShop,GET_SHOP_NAME(eShopName))
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SHOP_SETUP_BLIPS - Shops name is ", GET_SHOP_NAME(eShopName))
	
	//SET_BLIP_NAME_FROM_TEXT_FILE(blipShop,"BLIP_73")
	
	//SET_BLIP_AS_MINIMAL_ON_EDGE(blipShop,TRUE)
	SET_BLIP_SCALE(blipShop, BLIP_SIZE_COORD)
	
	SET_BLIP_AS_MISSION_CREATOR_BLIP(blipShop, TRUE)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SHOP_SETUP_BLIPS - Shops blipped")
ENDPROC

PROC TAXI_SHOP_DELETE_ALL_BLIPS(BLIP_INDEX &blipArray[])
	INT i
	REPEAT 3 i
		IF DOES_BLIP_EXIST(blipArray[i])
			REMOVE_BLIP(blipArray[i])
		ENDIF
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SHOP_DELETE_ALL_BLIPS - Blips cleaned up")
ENDPROC

PROC UPDATE_PROGRESS()
	IF EVALUATE_OVERALL_TIME(myTaxiData, TAXI_DI_TIME_BAD, 168)
	
	ELIF NOT bCheckMidComplete
		bCheckMidComplete = EVALUATE_PROGRESS(myTaxiData, TAXI_DI_TIME_FAST, TAXI_DI_TIME_BAD, 1000, 73)
	ELIF NOT bCheckEndComplete
		bCheckEndComplete = EVALUATE_PROGRESS(myTaxiData, TAXI_DI_TIME_FAST, TAXI_DI_TIME_BAD, 28, 148)
	ENDIF
ENDPROC

PROC TAXI_OJ_SET_SHOP_DROPOFFS()

	vTaxiDropoffShopPos[HAIRDO_SHOP_01_BH] = 	<< -829.8521, -192.3817, 36.3936 >>
	vTaxiDropoffShopPos[HAIRDO_SHOP_02_SC] = 	<< 129.8964, -1716.6322, 28.0725 >>
	vTaxiDropoffShopPos[CLOTHES_SHOP_L_01_SC] = << 88.1425, -1391.7910, 28.1999 >>
	vTaxiDropoffShopPos[CLOTHES_SHOP_M_01_SM] = << -1210.9993, -785.9365, 16.0056 >>
	vTaxiDropoffShopPos[CLOTHES_SHOP_H_01_BH] = << -723.82043, -160.79854, 35.92944 >> //<< -723.9122, -161.8416, 35.9609 >>
	vTaxiDropoffShopPos[TATTOO_PARLOUR_01_HW] = << 339.2214, 158.9484, 102.1509 >>
	vTaxiDropoffShopPos[GUN_SHOP_01_DT] = 		<< 15.1447, -1127.1610, 27.7764 >>
	
	//Set POI points (point where passenger is going to walk toward
	//If these are not working, I think it's because shop scripts aren't all active or waiting to be discovered
	vTaxiDropoffPOIPos[HAIRDO_SHOP_01_BH] = 	<< -815.6819, -183.7194, 36.5695 >>
	vTaxiDropoffPOIPos[HAIRDO_SHOP_02_SC] = 	<< 136.1871, -1712.0688, 28.2916 >>
	vTaxiDropoffPOIPos[CLOTHES_SHOP_L_01_SC] = << 84.2585, -1379.5623, 28.2919 >>
	vTaxiDropoffPOIPos[CLOTHES_SHOP_M_01_SM] = << -1206.15601, -777.45782, 16.32830 >>
	vTaxiDropoffPOIPos[CLOTHES_SHOP_H_01_BH] = << -716.05505, -160.17415, 35.98816 >>
	vTaxiDropoffPOIPos[TATTOO_PARLOUR_01_HW] = << 342.1741, 172.9035, 102.1543 >>
	vTaxiDropoffPOIPos[GUN_SHOP_01_DT] = 		<< 22.1268, -1109.8680, 28.7970 >>
	
	vTaxiInStorePos[CLOTHES_SHOP_L_01_SC] = << 76.3885, -1397.6653, 28.3719 >>
	vTaxiInStorePos[CLOTHES_SHOP_M_01_SM] = << -1193.4290, -769.2299, 16.3289 >>
	vTaxiInStorePos[CLOTHES_SHOP_H_01_BH] = << -710.8855, -152.4612, 36.4106 >>
	
	vTaxiDropoffWarpPos[HAIRDO_SHOP_01_BH] = 	<< -821.3112, -207.0323, 36.3333 >>
	vTaxiDropoffWarpPos[HAIRDO_SHOP_02_SC] = 	<< 138.4111, -1726.9070, 28.0569 >>
	vTaxiDropoffWarpPos[CLOTHES_SHOP_L_01_SC] = << 96.1429, -1407.3462, 28.1563 >>
	vTaxiDropoffWarpPos[CLOTHES_SHOP_M_01_SM] = << -1202.8093, -795.3967, 15.2069 >>
	vTaxiDropoffWarpPos[CLOTHES_SHOP_H_01_BH] = << -710.7402, -178.9253, 35.8931 >>
	vTaxiDropoffWarpPos[TATTOO_PARLOUR_01_HW] = << 348.4406, 156.2108, 101.9483 >>
	vTaxiDropoffWarpPos[GUN_SHOP_01_DT] = 		<< 40.1115, -1113.9235, 28.1427 >>
	
	fTaxiDropoffWarpHeading[HAIRDO_SHOP_01_BH] = 	33.19
	fTaxiDropoffWarpHeading[HAIRDO_SHOP_02_SC] = 	50.6
	fTaxiDropoffWarpHeading[CLOTHES_SHOP_L_01_SC] = 48.99
	fTaxiDropoffWarpHeading[CLOTHES_SHOP_M_01_SM] = 37.21
	fTaxiDropoffWarpHeading[CLOTHES_SHOP_H_01_BH] = 28.39
	fTaxiDropoffWarpHeading[TATTOO_PARLOUR_01_HW] = 71.56
	fTaxiDropoffWarpHeading[GUN_SHOP_01_DT] = 		148.91
	
	
	/*
	SWITCH eShopType
		CASE SHOP_TYPE_HAIRDO
			
			vTaxiDropoffShopPos[0] = << -829.8521, -192.3817, 36.3936 >>
			vTaxiDropoffShopPos[1] = << 129.8964, -1716.6322, 28.0725 >>
		
		BREAK
		
		CASE SHOP_TYPE_CLOTHES
		
			vTaxiDropoffShopPos[0] = << 88.1425, -1391.7910, 28.1999 >>
			vTaxiDropoffShopPos[1] = << -1210.9993, -785.9365, 16.0056 >>
			vTaxiDropoffShopPos[2] = << -723.9122, -161.8416, 35.9609 >>
		
		BREAK
		
		CASE SHOP_TYPE_TATTOO
			
			vTaxiDropoffShopPos[0] = << 339.2214, 158.9484, 102.1509 >>
		BREAK
		
		CASE SHOP_TYPE_GUN
			
			vTaxiDropoffShopPos[0] = << 15.1447, -1127.1610, 27.7764 >>
		BREAK
	ENDSWITCH
	*/
ENDPROC

PROC TAXI_SHOP_HIDE_BLIPS()
	
	INT iTemp
	REPEAT TAXI_SHOPS_MAX_NUM_SHOPS iTemp
		IF DOES_BLIP_EXIST(blipTaxiShop[iTemp])
			SET_BLIP_ALPHA(blipTaxiShop[iTemp],0)
		ENDIF
	ENDREPEAT
ENDPROC


PROC TAXI_SHOP_SHOW_BLIPS()
	
	INT iTemp
	REPEAT TAXI_SHOPS_MAX_NUM_SHOPS iTemp
		IF DOES_BLIP_EXIST(blipTaxiShop[iTemp])
			SET_BLIP_ALPHA(blipTaxiShop[iTemp],255)
		ENDIF
	ENDREPEAT
ENDPROC



PROC TAXI_SHOP_ENABLE_BLIP_FOR_DESTINATION(SHOP_TYPE_ENUM eShopType)
#IF IS_DEBUG_BUILD
	INT iTemp
#ENDIF	//	IS_DEBUG_BUILD

	TAXI_OJ_SET_SHOP_DROPOFFS()
	
	SWITCH eShopType
		//"Clothing Shop"
		CASE SHOP_TYPE_HAIRDO
			
		BREAK
		
		CASE SHOP_TYPE_CLOTHES
			//Turn off the one on it
			SET_STATIC_BLIP_CATEGORY_VISIBILITY(STATIC_BLIP_CATEGORY_SHOP, FALSE)
			
			// clears out local blips
			TAXI_SHOP_DELETE_ALL_BLIPS(blipTaxiShop)
			
			//Make my own
			TAXI_SHOP_SETUP_BLIPS(blipTaxiShop[0],CLOTHES_SHOP_L_01_SC,RADAR_TRACE_CLOTHES_STORE)
			TAXI_SHOP_SETUP_BLIPS(blipTaxiShop[1],CLOTHES_SHOP_M_01_SM,RADAR_TRACE_CLOTHES_STORE)
			TAXI_SHOP_SETUP_BLIPS(blipTaxiShop[2],CLOTHES_SHOP_H_01_BH,RADAR_TRACE_CLOTHES_STORE)
		BREAK
		
		//"Tattoo "
		CASE SHOP_TYPE_TATTOO
			
		BREAK
	
		//"Gunshop"
		CASE SHOP_TYPE_GUN
			
		BREAK
	ENDSWITCH

#IF IS_DEBUG_BUILD
	REPEAT TAXI_SHOPS_MAX_NUM_SHOPS iTemp
		IF DOES_BLIP_EXIST(blipTaxiShop[iTemp])
			IF ARE_VECTORS_EQUAL(GET_BLIP_COORDS(blipTaxiShop[iTemp]),GET_SHOP_COORDS(targetShopName))
				iBestShopIndex = iTemp
				CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Shops Best one is ", iBestShopIndex)
			ENDIF
		ENDIF
	ENDREPEAT
#ENDIF	//	IS_DEBUG_BUILD
ENDPROC

PROC ENABLE_INCORRECT_SHOP_BLIPS(BOOL bEnable)
	
	IF bEnable
		SET_STATIC_BLIP_CATEGORY_VISIBILITY(STATIC_BLIP_CATEGORY_SHOP, TRUE)
		CDEBUG1LN(DEBUG_OJ_TAXI,"Enabled Shop Blips.")
	ELSE
		SET_STATIC_BLIP_CATEGORY_VISIBILITY(STATIC_BLIP_CATEGORY_SHOP, FALSE)
		CDEBUG1LN(DEBUG_OJ_TAXI,"Disabled Shop Blip.")
	ENDIF
	
ENDPROC

/// PURPOSE: Request all our mission specific assets here
///  
PROC REQUEST_TAXI_ODDJOB_TTB_STREAMS_STAGE_01()
	//Load text and UI
	REQUEST_MODEL(mPassengerModel)
	TaxiMidSize.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	TAXI_INIT_SHARED_STREAMS(FALSE)
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM - TTB Stage 01 Assets Requested")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_TTB_STREAMS_STAGE_01()

	SET_MODEL_AS_NO_LONGER_NEEDED(mPassengerModel)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ Follow Car Stage01 Assets Released")
	
ENDPROC

/// PURPOSE: Waits for our assets to load
///    
/// RETURNS: TRUE if everything has loaded correctly FALSE if there was a problem
///    
FUNC BOOL HAVE_TAXI_OJ_FC_STAGE_01_ASSETS_LOADED()
	
	IF NOT HAS_MODEL_LOADED(mPassengerModel)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading A_F_Y_BevHills_03",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(TaxiMidSize.siMovie)
		#IF IS_DEBUG_BUILD
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TaxiMidSize.siMovie",iDebugThrottle)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	IF NOT TAXI_SHARED_ASSETS_STREAMED(iDebugThrottle,FALSE)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading shared assets",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE		
ENDFUNC

PROC Script_Cleanup()
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(taxiRideWidgets)
			DELETE_WIDGET_GROUP(taxiRideWidgets)
		ENDIF
		
		IF DOES_WIDGET_GROUP_EXIST(ojTaxiWidgets_Shops)
			DELETE_WIDGET_GROUP(ojTaxiWidgets_Shops)
		ENDIF
		
		CLEANUP_TAXI_WIDGETS()
		
	#ENDIF
	ENABLE_INCORRECT_SHOP_BLIPS(TRUE)
	
	TAXI_SHOP_DELETE_ALL_BLIPS(blipTaxiShop)
	
	RELEASE_TAXI_ODDJOB_TTB_STREAMS_STAGE_01()
	
	TERMINATE_THIS_THREAD()
ENDPROC

// Perform any special commands if the script fails
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Failed()
	TAXI_SCRIPT_FAILED(myTaxiData)
	Script_Cleanup()
ENDPROC

PROC INITIALIZE_SCRIPT_VARIABLES()
	
	TAXI_ODDJOB_GLOBAL_SETUP(myTaxiData,TXM_05_TAKETOBEST)
		
	// Set our initial state up. VIP missions don't start at TRS_FINDING_LOCATION.
	myTaxiData.tTaxiOJ_RideState = TRS_INIT_STREAM
	
//	iDebugShop = iPrevDebugShop
	myTaxiData.fTaxiOJ_BanterDelay = 20.0
	myTaxiData.vTaxiOJ_WarpPtPickup = << -422.4797, 1181.7690, 324.6417 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingPickup = 225.6436
	
	myTaxiData.vTaxiOJ_WarpPtDropoff = << 171.1731, -3288.1711, 4.7828 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 184.17
	
	SET_TAXI_TIP_CUTOFFS(myTaxiData, 20, 40)
	
	#IF IS_DEBUG_BUILD
		#IF DEBUG_iTurnOnAllDXDebug
			ENABLE_ALL_DIALOGUE_DEBUG()
		#ENDIF
		
		taxiRideWidgets = START_WIDGET_GROUP("Taxi Ride - Take To Best")
			INIT_ODDJOB_TAXI_WIDGETS()
			
			//Excitement Widget
			ojTaxiWidgets_Shops = START_WIDGET_GROUP("Shops")
			
				ADD_WIDGET_STRING("Locations Debugging")
					ADD_WIDGET_BOOL("Show Drop Pts ",bShowDropoffPt)
					ADD_WIDGET_BOOL("Toggle Best Shop GREEN",bShowBestChoice)
				
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	#ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~~~~~~~~~~~~ Oddjobs | Taxi | Take To Best ~~~~~~~~~~~~~~~~~----------")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")
	
	
ENDPROC
#IF IS_DEBUG_BUILD
PROC PROCESS_WIDGETS()

	UPDATE_TAXI_WIDGETS(myTaxiData,tTaxiOJ_DQ_Data)
	
	IF bShowDropoffPt	
		DRAW_DEBUG_SPHERE(myTaxiData.vTaxiOJDropoff,3.0,255,215,0,150)
		DRAW_DEBUG_TEXT("Closest Shop Dropoff Pt Here", myTaxiData.vTaxiOJDropoff)
	ENDIF
	
	IF bShowBestChoice
		IF GET_BLIP_COLOUR(blipTaxiShop[iBestShopIndex]) = BLIP_COLOUR_YELLOW
			SET_BLIP_COLOUR(blipTaxiShop[iBestShopIndex], BLIP_COLOUR_GREEN)
			
		ELIF GET_BLIP_COLOUR(blipTaxiShop[iBestShopIndex]) = BLIP_COLOUR_GREEN
			SET_BLIP_COLOUR(blipTaxiShop[iBestShopIndex], BLIP_COLOUR_YELLOW)
		ENDIF
		bShowBestChoice = FALSE
	ENDIF
		
ENDPROC
#ENDIF

FUNC SHOP_TYPE_ENUM CHOOSE_RANDOM_SHOP_TYPE()
	INT NumOptions = 4
	INT tempInt
	SHOP_TYPE_ENUM returnEnum = SHOP_TYPE_EMPTY
	STRING sShoptype = "SHOP_TYPE_EMPTY"
	
	IF NOT bNotCheckInModeOn
		tempInt = GET_RANDOM_INT_IN_RANGE() % NumOptions
	ELSE
//		tempInt = iDebugShop
	ENDIF
	
	SWITCH tempInt
		CASE 0
			returnEnum = SHOP_TYPE_HAIRDO
			sShoptype = "SHOP_TYPE_HAIRDO"
		BREAK
		
		CASE 1
			returnEnum = SHOP_TYPE_CLOTHES
			sShoptype = "SHOP_TYPE_CLOTHES"
		BREAK
		
		CASE 2
			returnEnum = SHOP_TYPE_TATTOO
			sShoptype = "SHOP_TYPE_TATTOO"
		BREAK
		
		CASE 3
			returnEnum = SHOP_TYPE_GUN
			sShoptype = "SHOP_TYPE_GUN"
		BREAK
	ENDSWITCH
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI - Looking for shop type: ","",sShoptype)
	RETURN returnEnum
ENDFUNC
FUNC SHOP_NAME_ENUM CHOOSE_RANDOM_SHOP_NAME_OF_TYPE(SHOP_TYPE_ENUM inputType, BOOL bPickRandomStore = TRUE)
	INT i, startingInt, endingInt, countInt, randInt
	SHOP_NAME_ENUM returnEnum = EMPTY_SHOP
	startingInt = -1
	endingInt = -1
	//CLOTHES_SHOP_H_01_BH
	
	REPEAT COUNT_OF(SHOP_NAME_ENUM) i
		IF GET_SHOP_TYPE_ENUM((INT_TO_ENUM(SHOP_NAME_ENUM, i))) = inputType
			countInt++
			IF startingInt = -1
				startingInt = i
			ELSE
				endingInt = i
			ENDIF
		ENDIF
	ENDREPEAT 
	IF countInt > 0
		IF endingInt <> -1
			IF bPickRandomStore
				randInt = GET_RANDOM_INT_IN_RANGE() % countInt
				returnEnum = INT_TO_ENUM(SHOP_NAME_ENUM, (randInt + startingInt))	
			ELSE
				returnEnum = CLOTHES_SHOP_H_01_BH
			ENDIF
		ELSE
			returnEnum = INT_TO_ENUM(SHOP_NAME_ENUM, startingInt)	
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI - Looking for shop named: ", Get_Shop_Display_String_From_Shop_ID(returnEnum), " Enum Int = ", ENUM_TO_INT(returnEnum) )
	#ENDIF
	RETURN returnEnum
ENDFUNC
PROC BEST_CALCULATE_FARE_VALUES()
	//No longer doing Random shops, hardcoding to HAIR SALON 
	//myTaxiData.targetShopType = CHOOSE_RANDOM_SHOP_TYPE()
	
	// this just hardcodes it to CLOTHES_SHOP_H_01_BH
	targetShopName = CHOOSE_RANDOM_SHOP_NAME_OF_TYPE(myTaxiData.targetShopType, FALSE)
	
	TAXI_SHOP_ENABLE_BLIP_FOR_DESTINATION(myTaxiData.targetShopType)
	
	IF targetShopName <> EMPTY_SHOP
		FLOAT fDist
		fDist = GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), GET_SHOP_COORDS(targetShopName))
		myTaxiData.fTaxiOJ_TimeDeadline = (fDist * 0.3)
	ELSE
		myTaxiData.fTaxiOJ_TimeDeadline = 120.0
	ENDIF

	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		myTaxiData.iOldVehicleHealth = GET_ENTITY_HEALTH(myTaxiData.viTaxi)
	ENDIF
ENDPROC
PROC Update_Destination_shop()

	//Handle hiding/showing shop blips when player enters/exits taxi
	IF NOT bLeftTaxi
		IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
			TAXI_SHOP_HIDE_BLIPS()
			SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_LEFT_CAR,TRUE)
			bLeftTaxi = TRUE
		ENDIF
	//Has Left Taxi
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
				SET_PLAYER_LEFT_TAXI_OJ(myTaxiData)
				TAXI_SHOP_SHOW_BLIPS()
				bLeftTaxi = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF myTaxiData.bIsCurrentlyWanted
		TAXI_SHOP_HIDE_BLIPS()
		bWentWanted = TRUE
	ELSE
		IF bWentWanted
			TAXI_SHOP_SHOW_BLIPS()
			bWentWanted = FALSE
		ENDIF
	ENDIF
	
	nearestShopName = GET_CLOSEST_SHOP_OF_TYPE(GET_ENTITY_COORDS(PLAYER_PED_ID()), myTaxiData.targetShopType)
	IF nearestShopName <> EMPTY_SHOP
		
		SHOP_NAME_ENUM enumClosestShop = GET_CLOSEST_SHOP_OF_TYPE(GET_ENTITY_COORDS(PLAYER_PED_ID()), myTaxiData.targetShopType)
	
		IF NOT ARE_VECTORS_EQUAL(myTaxiData.vTaxiOJDropoff, vTaxiDropoffShopPos[enumClosestShop])
			
			myTaxiData.vTaxiOJDropoff = vTaxiDropoffShopPos[enumClosestShop]
			myTaxiData.vTaxiOJ_PassengerGoToPt = vTaxiDropoffPOIPos[enumClosestShop]
			myTaxiData.vTaxiOJ_WarpPtDropoff = vTaxiDropoffWarpPos[enumClosestShop]
			myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = fTaxiDropoffWarpHeading[enumClosestShop]
			vTaxiCurrentWalkInStorePos = vTaxiInStorePos[enumClosestShop]
		ENDIF

	ENDIF
ENDPROC
PROC TAXI_OJ_TTB_SET_TIPS_AND_EXCITEMENT_TO_CHECK()
	
	//Tip Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POS_DELIVERY_TIME)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_HIT_PED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_TOOK_DAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ROLL_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LEFT_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_STOPPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_LIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_DISLIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_FREEBIE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LOST_POLICE)
	
	//Excitement Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_HITPED)					
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_ROLL)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_TOOKDAMAGE)
	
	
	//Turn off aggro bits
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
	
	
	//Start the mission timer here
	TAXI_START_TIMER(myTaxiData, TT_RIDETODEST)
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DL_SET_TIPS_TO_CHECK set tips to check on")
ENDPROC

/*

oooo     oooo      o      ooooo oooo   oooo 
 8888o   888      888      888   8888o  88  
 88 888o8 88     8  88     888   88 888o88  
 88  888  88    8oooo88    888   88   8888  
o88o  8  o88o o88o  o888o o888o o88o    88  
                                           
*/

PROC Main_Taxi_OJ_TakeToBest()

	//Handles Fail Or No Taxi-------------------------------------------------------------------------------
	IF IS_TAXI_JOB_IN_FAIL_STATE(myTaxiData)
		
		TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
		
		IF myTaxiData.tTaxiOJ_RideState > TRS_INIT_STREAM
			IF TAXI_HANDLE_FAIL(myTaxiData)
				Script_Failed()
			ENDIF
		ELSE
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CLEANUP) > 5.0
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"Fail During State: "," ", myTaxiData.sTaxiOJ_RideState, " ", myTaxiData.sTaxiOJ_Reason4Fail)
				
				SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
				Script_Cleanup()
			
			ENDIF
		ENDIF
	
	//Proceed
	ELSE
		//Run Throughtout the Entire Mission--------------------------------------------------------------
		TAXI_OJ_MAINTAIN_GLOBAL_GATES(myTaxiData)
		
		//Gobal taxi updates that all taxi missions run every frame
		RUN_GLOBAL_TAXI_UPDATES(myTaxiData,aggroArgs)
		
		UPDATE_TAXI_OJ_TIP(myTaxiData,iTipIndex)
		
		PROCESS_TAXI_EXCEPTIONS(myTaxiData)
		
	
		//Dialogue & Objectives
		IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION
			
			TAXI_ODDJOB_HANDLE_TAXI_BLIPPING(myTaxiData)
								
			IF NOT IS_TAXI_EMERGENCY_FAIL_SET(myTaxiData)
				TRIGGER_TAXI_QUEUE_TTB_LINES()
			ELSE
				TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",GET_TAXI_EMERGENCY_FAIL_STRING(myTaxiData))
			ENDIF			
			
		ENDIF
		//Track Driver progress
		IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
			HANDLE_TAXI_EXCITEMENT(myTaxiData,FALSE,TRUE)

#IF IS_DEBUG_BUILD
		ELIF bDebugTurnOnFreeRide
			HANDLE_TAXI_EXCITEMENT(myTaxiData,TRUE,TRUE)
#ENDIF
		ENDIF	
		//================================================================================================
	
	
		SWITCH myTaxiData.tTaxiOJ_RideState
		
			//PRESTREAM and Check that the player and taxi are good to go
			CASE TRS_INIT_STREAM
				
				//Request of our assets
				REQUEST_TAXI_ODDJOB_TTB_STREAMS_STAGE_01()
				
				TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
				
				TAXI_SPAWN_PASSENGER(myTaxiData, vPassengerPt, vPassengerPickupPt, "TaxiMiranda",mPassengerModel,0,10.0)
				TAXI_INIT_PASSENGER_BLIP(myTaxiData)
					
				//Move on to the next stage
				TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_STREAMING)
				
			BREAK
			
			//Wait for streaming to finish
			CASE TRS_STREAMING

				IF HAVE_TAXI_OJ_FC_STAGE_01_ASSETS_LOADED()
					
					INIT_ALL_TAXI_EXCITEMENT_VALUES()
				
					INITIALIZE_GENERIC_TAXI_EXCEPTIONS()
					
					TAXI_INITIALIZE_BONUS_FIELD(bonusTakeToBest[TTTB_BONUS_GENERIC], "TAXI_SC_BN_08", TAXI_CONST_BONUS_CASH_BEST_STORE)
					TAXI_INITIALIZE_BONUS_INFO(myTaxiData, bonusTakeToBest)					
					
					myTaxiData.vTaxiOJPickup =  vPassengerPt	
					
					//SET REACT BITS******************
					CLEAR_ALL_TAXI_PASSENGER_REACT_BITS(myTaxiData)
					
					
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPAWNING)
				ENDIF

			BREAK
			
			//Spawn passenger & designate the shop
			CASE TRS_SPAWNING
				IF PROPERTY_VIP_INIT_READY(myTaxiData)
					
					
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_TORSO, 0, 2)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_HAIR, 1, 0)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_LEG, 1,0)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_HEAD, 0,1)	
					
						SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger, PCF_GetOutUndriveableVehicle, FALSE)
					ENDIF
					
					//Setting the shop type to clothes---------------------------------------
					myTaxiData.targetShopType = SHOP_TYPE_CLOTHES
					
					//Give player obj to go pickup Passenger
					ENABLE_TAXI_SPEECH(myTaxiData)
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
					
				ENDIF
			BREAK
			
			CASE TRS_MANAGE_PICKUP
				IF TAXI_HANDLE_IV_PICKUP_NO_METER(myTaxiData)
	
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_PARK)
				ENDIF
			BREAK
				
			CASE TRS_WAIT_PARK
				IF IS_PASSENGER_ENTERING_TAXI(myTaxiData)
					//Greet Driver as soon as they sit down & blip
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_TTB_SHOP,TRUE)	
					
					//CleanUp Pickup Lock & POI
					CLEANUP_TAXI_PICKUP_STOP(myTaxiData)
				
					TAXI_OJ_TTB_SET_TIPS_AND_EXCITEMENT_TO_CHECK()
				
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DRIVING_PASSENGER)
					
				ENDIF
				
				// check to see if player dived, if so send back to prev state
				IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
					IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
						TAXI_HANDLE_PLAYER_DIVE(myTaxiData)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
					ENDIF
				ENDIF
			BREAK		
						
			CASE TRS_DRIVING_PASSENGER
				
				IF NOT bFirstDropoffBlipEnabled
					IF myTaxiData.bObjPrinted					
						BEST_CALCULATE_FARE_VALUES()
						ENABLE_INCORRECT_SHOP_BLIPS(FALSE)
						bFirstDropoffBlipEnabled = TRUE
					ENDIF
				ENDIF
				
				Update_Destination_shop()
				
				IF TAXI_HANDLE_DRIVING(myTaxiData/*,tTaxiOJ_DQ_Data*/, 5)
					
					CLEAR_DIALOGUE_QUEUE(tDialogueLine)
					CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,CONST_TAXI_OJ_LINE_NUM_TO_CLOSE_DIALOGUE_Q)	
					
					IF nearestShopName = targetShopName
						//TAXI_OVERRIDE_BONUS_AWARD(myTaxiData,bonusTakeToBest[TTTB_BONUS_TIP].cash,ENUM_TO_INT(TTTB_BONUS_TIP),TAXI_TTB_TIP_HIGH)
						TASK_LOOK_AT_COORD(myTaxiData.piTaxiPassenger, <<-713.44891, -153.40938, 37.14410>>, 10000, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						TAXI_SET_BONUS_AWARD(myTaxiData,ENUM_TO_INT(TTTB_BONUS_GENERIC))
						myTaxiData.iTaxiOJ_CashTip = 50
						myTaxiData.bTaxiOJ_DoSayNoThanks = FALSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"Main_Taxi_OJ_TakeToBest: at CLOTHES_SHOP_H_01_BH") 
					ELIF nearestShopName = CLOTHES_SHOP_M_01_SM
						//TAXI_OVERRIDE_BONUS_AWARD(myTaxiData,bonusTakeToBest[TTTB_BONUS_TIP].cash,ENUM_TO_INT(TTTB_BONUS_TIP),TAXI_TTB_TIP_MED)
						//TAXI_SET_BONUS_AWARD(myTaxiData,ENUM_TO_INT(TTTB_BONUS_TIP))
						TASK_LOOK_AT_COORD(myTaxiData.piTaxiPassenger, <<-1196.37183, -772.46075, 17.30166>>, 10000, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						myTaxiData.iTaxiOJ_CashTip = 25
						myTaxiData.bTaxiOJ_DoSayNoThanks = FALSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"Main_Taxi_OJ_TakeToBest: at CLOTHES_SHOP_M_01_SM") 
					ELSE
						TASK_LOOK_AT_COORD(myTaxiData.piTaxiPassenger, <<72.87311, -1392.59741, 29.76223>>, 10000, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						myTaxiData.iTaxiOJ_CashTip = 0
						myTaxiData.bTaxiOJ_DoSayNoThanks = TRUE
						CDEBUG1LN(DEBUG_OJ_TAXI,"Main_Taxi_OJ_TakeToBest: at CLOTHES_SHOP CRAP!") 
					ENDIF
					
					
					//CASH $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
					SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)	
					TAXI_OJ_RATE_OVERALL_TIP_LEVEL(myTaxiData)
					CONVERT_TAXI_TIP_TO_CASH(myTaxiData)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
				
				ENDIF
			BREAK
			
			//Regular Payment
			CASE TRS_REGULAR_PAYMENT
				//Wait til passenger is done speaking/paying than tell him to leave vehicle
				IF HAS_TAXI_OJ_PASSENGER_BEEN_DROPPED_OFF(myTaxiData)
					
					CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
		            CLEAR_SEQUENCE_TASK(siTemp)
		            OPEN_SEQUENCE_TASK(siTemp)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,myTaxiData.vTaxiOJ_PassengerGoToPt,PEDMOVEBLENDRATIO_WALK,-1)
						TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, myTaxiData.vTaxiOJ_PassengerGoToPt, 15, 5000)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,vTaxiCurrentWalkInStorePos,PEDMOVEBLENDRATIO_WALK,-1)
						TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, vTaxiCurrentWalkInStorePos, 15)
		            CLOSE_SEQUENCE_TASK(siTemp)
		            TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siTemp)
					
					SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger, TRUE)
					
					//TAXI_OJ_TASK_PASSENGER_DROPOFF(myTaxiData)
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)				
		
				ENDIF			

			BREAK
			
			//Pop up the scorecard
			CASE TRS_SCORECARD_GRADE
				
				//SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_SearchForClosestDoor, TRUE)
				
				IF TAXI_CALC_SCORECARD(myTaxiData,TaxiMidSize)
					TAXI_MISSION_END(TRUE, myTaxiData,FALSE)	
					TAXI_RESET_TIMERS(myTaxiData,TT_CLEANUP)
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CLEANUP)
				ENDIF
			
			BREAK
		
			CASE TRS_CLEANUP
			
				//SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_SearchForClosestDoor, TRUE)
				
			  
//				IF IS_ENTITY_AT_COORD(myTaxiData.piTaxiPassenger, <<80.6663, -1391.2383, 28.3876>>, <<25.0,25.0, LOCATE_SIZE_HEIGHT>>, FALSE, TRUE, TM_ON_FOOT)		//Low End Clothing
//				OR IS_ENTITY_AT_COORD(myTaxiData.piTaxiPassenger, <<-1198.8291, -775.9587, 16.3238>>, <<2.0, 2.0, LOCATE_SIZE_HEIGHT>>, FALSE, TRUE, TM_ON_FOOT)	//SubUrban
//				OR IS_ENTITY_AT_COORD(myTaxiData.piTaxiPassenger, <<-714.7526, -155.4902, 36.4152>>, <<2.0, 2.0, LOCATE_SIZE_HEIGHT>>, FALSE, TRUE, TM_ON_FOOT)		//Ponsonbys
//				OR GET_TAXI_TIMER_IN_SECONDS(myTaxiData,TT_CLEANUP) > 15
					CLEANUP_TAXI_OJ_PASSENGER(myTaxiData)
					Script_Cleanup()
//				ENDIF
			BREAK
			
		ENDSWITCH
	
	ENDIF
ENDPROC

SCRIPT

	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		Script_Cleanup()
	ENDIF
	
	// Any initialisation (generally, only mission scripts should set
	// the mission flag to TRUE)
	SET_MISSION_FLAG(TRUE)
	
	INITIALIZE_SCRIPT_VARIABLES()
	
	// The script loop
	WHILE (TRUE)
		// Maintain the script – perform per-frame functionality

	//All skips and debug shortcuts-------------------------------
	#IF IS_DEBUG_BUILD
	
		PROCESS_TAXI_DEBUG_SKIP(myTaxiData,tDebugState)
		
		// Debug Key: Check for Pass (not for Minigmes)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			TAXI_ODDJOB_DEBUG_SKIP_TO_SCORECARD(myTaxiData)
		ENDIF

		// Debug Key: Check for Fail (not for Minigames)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			TAXI_DEBUG_FAIL_TRIGGERED(myTaxiData)
			Script_Failed()
		ENDIF
		
		// Debug Key: set vehicle health to 0.0
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
			IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				SET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi, 0.0)
			ENDIF
		ENDIF			

		PROCESS_WIDGETS()

	#ENDIF
	//END DEBUG----------------------------------------------------
		
		IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPlayer)
			Main_Taxi_OJ_TakeToBest()
		ELSE
			REASSIGN_TAXI_OJ_DRIVER(myTaxiData)
		ENDIF
		
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

