//Changelog-
/*
6/30/11 - Removing the draw call for displaying the Dropoff Grade in TAXI_CALC_SCORECARD for Bug # 137695, so I have to move all the y coords positions by one

*/
USING "Taxi_Scorecard.sch"
USING "Taxi_Support_Lib.sch"
USING "Minigame_Stats_tracker_Helpers.sch"
USING "minigame_midsized_message.sch"

TAXI_SCORECARD_STATES tscIndex = TSC_INIT
TAXI_SCORECARD ts_Scorecard

/// PURPOSE: This converts the tip values into strings that the scorecard then displays
///    
/// PARAMS:
///    myTaxiData - our taxi data
PROC TAXI_PROCESS_TIPS(TaxiStruct &myTaxiData)

	// Set Fare
	ts_Scorecard.fFare = TO_FLOAT(myTaxiData.iTaxiOJ_CashFare)
	CDEBUG1LN(DEBUG_OJ_TAXI,"Setting the fare to: ", myTaxiData.iTaxiOJ_CashFare)
	
	// Set Tip
	ts_Scorecard.fTips = TO_FLOAT(myTaxiData.iTaxiOJ_CashTip)
	CDEBUG1LN(DEBUG_OJ_TAXI,"Setting the tip to: ", myTaxiData.iTaxiOJ_CashTip)	
	
	TAXI_STATS_UPDATE(TAXI_STAT_MONEY_TIPS, myTaxiData.iTaxiOJ_CashTip)
	TAXI_STATS_UPDATE(TAXI_STAT_MONEY_HI_TIP, myTaxiData.iTaxiOJ_CashTip)
	iTaxiSCStats[TAXISC_TIPS_EARNED] = myTaxiData.iTaxiOJ_CashTip
	iTaxiSCStats[TAXISC_HIGHEST_TIP] = myTaxiData.iTaxiOJ_CashTip
	
	
	// Set Bonus Amt
	INT iIter = 0
	REPEAT COUNT_OF(myTaxiData.iTaxiOJ_CashBonusInfo) iIter
		ts_Scorecard.iCashBonusInfo[iIter] = myTaxiData.iTaxiOJ_CashBonusInfo[iIter]
		IF IS_BIT_SET( ts_Scorecard.iCashBonusInfo[iIter].status, BONUS_STATUS_AWARDED )
			ts_Scorecard.fSpecialCash += myTaxiData.iTaxiOJ_CashBonusInfo[iIter].cash
			CDEBUG1LN(DEBUG_OJ_TAXI,"Setting the special cash to: ", ts_Scorecard.fSpecialCash)
		ENDIF
	ENDREPEAT
	
	ts_Scorecard.eMissionType = myTaxiData.tTaxiOJ_MissionType
	
	// Set Total Money
	ts_Scorecard.fTotalMoneys = ts_Scorecard.fFare + ts_Scorecard.fTips + ts_Scorecard.fSpecialCash
ENDPROC


PROC TAXI_REWARD_DRIVER(TaxiStruct &myTaxiData)
    INT iTaxiTotalCashAward = myTaxiData.iTaxiOJ_CashFare + myTaxiData.iTaxiOJ_CashTip + myTaxiData.iTaxiOJ_SpecialCash
    
    //iReward = iReward
    IF iTaxiTotalCashAward > 0
        CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_TAXI, iTaxiTotalCashAward)
        TAXI_STATS_UPDATE(TAXI_STAT_MONEY_TOTAL,iTaxiTotalCashAward)
		iTaxiSCStats[TAXISC_SCORE] = iTaxiTotalCashAward
        CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_REWARD_DRIVER - Drivers Bank Account was just credited : ", "",iTaxiTotalCashAward)
    ELSE
        CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_REWARD_DRIVER - BANK ACCT not credited because no money was earned.")
    ENDIF
ENDPROC

FUNC STRING GET_SCALEFORM_SHARD_METHOD()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		RETURN "SHOW_COND_SHARD_MESSAGE"
	ENDIF
	
	RETURN "SHOW_SHARD_MIDSIZED_MESSAGE"
ENDFUNC

/// PURPOSE:
///    Set big message with a number token
/// PARAMS:
///    scaleformStruct - 
///    label - splash text string
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
///    eMessageType - Tells us what scaleform method we want to call
PROC SET_TAXI_SCALEFORM_SHARD_MESSAGE_WITH_THREE_NUMBERS_IN_STRAPLINE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, INT iFare, INT iTips, INT iTotalCash, INT iBonusAmount, STRING sBonusName,
																	STRING strapLine, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_WHITE)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, GET_SCALEFORM_SHARD_METHOD())
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapline)
			//Fare
			ADD_TEXT_COMPONENT_INTEGER(iFare)
			//Tips
			ADD_TEXT_COMPONENT_INTEGER(iTips)
			//Bonus Name
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sBonusName)
			//Bonus
			ADD_TEXT_COMPONENT_INTEGER(iBonusAmount)
			//Total
			ADD_TEXT_COMPONENT_INTEGER(iTotalCash)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
	scaleformStruct.eEndFlash = HUD_COLOUR_WHITE
ENDPROC

FUNC STRING GET_TAXI_MISSION_BONUS_NAME(TaxiStruct &myTaxiData)

	STRING sBonusName
	SWITCH myTaxiData.tTaxiOJ_MissionType
		
		
		CASE TXM_01_NEEDEXCITEMENT
			sBonusName = "TAXI_SC_BN_02"
		BREAK
		
		CASE TXM_02_TAKEITEASY
			sBonusName = "TAXI_SC_BN_01"
		BREAK
		
		CASE TXM_03_DEADLINE
			sBonusName = "TAXI_SC_BN_03"
		BREAK
		
		CASE TXM_04_GOTYOURBACK
			sBonusName = "TAXI_SC_BN_12"
		BREAK
		
		CASE TXM_05_TAKETOBEST
			sBonusName = "TAXI_SC_BN_08"
		BREAK	
		
		CASE TXM_07_CUTYOUIN
			sBonusName = "TAXI_SC_BN_07"
		BREAK
		
		CASE TXM_08_GOTYOUNOW
			sBonusName = "TAXI_SC_KO"
		BREAK
		
		CASE TXM_09_CLOWNCAR
			sBonusName = "TAXI_SC_BN_10"
		BREAK
		
		CASE TXM_10_FOLLOWTHATCAR
			sBonusName = "TAXI_SC_BN_04"
		BREAK
		
	ENDSWITCH
	
	RETURN sBonusName

ENDFUNC

FUNC BOOL TAXI_CALC_SCORECARD(TaxiStruct &myTaxiData, SCRIPT_SHARD_BIG_MESSAGE &TaxiMidSize)//, STRING sBonusName)
	SWITCH tscIndex
		CASE TSC_INIT
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > 1.0	
				
				IF IS_OJ_TAXI_PLAYER_CONTROL_OFF()
					CDEBUG1LN(DEBUG_OJ_TAXI,"scorecard - Player control off, setting back on")
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
				
				TAXI_PROCESS_TIPS(myTaxiData)
				
				//Clear the bit before right before drawing the scorecard.
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SCORECARD_IS_DRAWING)
				
				//Draw midmessage - https://devstar.rockstargames.com/wiki/index.php/CENTRAL_MESSAGES
				SET_TAXI_SCALEFORM_SHARD_MESSAGE_WITH_THREE_NUMBERS_IN_STRAPLINE(TaxiMidSize, "TAXI_FARE_TITLE", myTaxiData.iTaxiOJ_CashFare, myTaxiData.iTaxiOJ_CashTip, 
																						(myTaxiData.iTaxiOJ_CashFare+myTaxiData.iTaxiOJ_CashTip + myTaxiData.iTaxiOJ_SpecialCash), 
																						myTaxiData.iTaxiOJ_SpecialCash, GET_TAXI_MISSION_BONUS_NAME(myTaxiData),"TAXI_FARE_MID")
						
				
				TaxiMidSize.iDuration = 4000
				
				PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_UNDER_THE_BRIDGE", "HUD_MINI_GAME_SOUNDSET", FALSE)
				
				SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
				
				tscIndex = TSC_TALLY
				CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CALC_SCORECARD - TSC_TALLY")
			ENDIF
		BREAK
		
		//After pressing X to continue & close the menu
		CASE TSC_TALLY
			IF NOT UPDATE_SHARD_BIG_MESSAGE(TaxiMidSize)//, TRUE)
				TAXI_REWARD_DRIVER(myTaxiData)
				
				TAXI_RESET_TIMERS(myTaxiData, TT_GENERIC)
				
				SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
				
				tscIndex = TSC_FINISH
			//	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CALC_SCORECARD - TSC_FINISH")
				CDEBUG1LN(DEBUG_OJ_TAXI,"** TAXI_CALC_SCORECARD - SCORECARD DONE! **")
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//EOF
