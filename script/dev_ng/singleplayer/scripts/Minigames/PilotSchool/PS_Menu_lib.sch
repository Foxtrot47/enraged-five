USING "minigame_uiinputs.sch"
USING "hud_drawing.sch"
USING "screens_header.sch"
USING "PS_Menu.sch" 
USING "Pilot_School_Data.sch"
USING "UIUtil.sch"
USING "script_oddjob_funcs.sch"
USING "menu_cursor_public.sch"

BOOL bIsWidescreen

#IF IS_DEBUG_BUILD
//	WIDGET_GROUP_ID PS_Widget
	INT iSpriteSelection, iTextSelection, iRectSelection, dbgCnt
	INT iLastSpriteSelection, iLastTextSelection, iLastRectSelection
	FLOAT spriteX, spriteY, textX, textY, rectX, rectY
	
	FUNC STRING PS_GET_LABEL_FROM_RECT_BY_ID(INT thisID)
		SWITCH INT_TO_ENUM(PS_SCREEN_RECT, thisID)
			CASE PS_SELECTION_BACKGROUND
				RETURN "PS_SELECTION_BACKGROUND"
			CASE PS_DESCRIPTION_BACKGROUND
				RETURN "PS_DESCRIPTION_BACKGROUND"
			CASE PS_AWARDS_BACKGROUND
				RETURN "PS_AWARDS_BACKGROUND"
			CASE PS_SELECTION_EDGING
				RETURN "PS_SELECTION_EDGING"
			CASE PS_DESCRIPTION_EDGING
				RETURN "PS_DESCRIPTION_EDGING"
			CASE PS_AWARDS_EDGING
				RETURN "PS_AWARDS_EDGING"
			CASE PS_SELECTION_IMAGE_BACKGROUD
				RETURN "PS_SELECTION_IMAGE_BACKGROUD"
			CASE PS_DESCRIPTION_IMAGE_BACKGROUND
				RETURN "PS_DESCRIPTION_IMAGE_BACKGROUND"
			CASE PS_AWARDS_IMAGE_BACKGROUND
				RETURN "PS_AWARDS_IMAGE_BACKGROUND"
			CASE PS_SELECTION_ITEM_BG_1
				RETURN "PS_SELECTION_ITEM_BG_1"
			CASE PS_SELECTION_ITEM_BG_2
				RETURN "PS_SELECTION_ITEM_BG_2"
			CASE PS_SELECTION_ITEM_BG_3
				RETURN "PS_SELECTION_ITEM_BG_3"
			CASE PS_SELECTION_ITEM_BG_4
				RETURN "PS_SELECTION_ITEM_BG_4"
			CASE PS_SELECTION_ITEM_BG_5
				RETURN "PS_SELECTION_ITEM_BG_5"
			CASE PS_SELECTION_ITEM_BG_6
				RETURN "PS_SELECTION_ITEM_BG_6"
			CASE PS_SELECTION_ITEM_BG_7
				RETURN "PS_SELECTION_ITEM_BG_7"
			CASE PS_SELECTION_ITEM_BG_8
				RETURN "PS_SELECTION_ITEM_BG_8"
			CASE PS_SELECTION_ITEM_BG_9
				RETURN "PS_SELECTION_ITEM_BG_9"
			CASE PS_SELECTION_ITEM_BG_10
				RETURN "PS_SELECTION_ITEM_BG_10"
			CASE PS_SELECTION_ITEM_BG_11
				RETURN "PS_SELECTION_ITEM_BG_11"
			CASE PS_SELECTION_ITEM_BG_12
				RETURN "PS_SELECTION_ITEM_BG_12"
			CASE PS_AWARDS_ITEM_BG_1
				RETURN "PS_AWARDS_ITEM_BG_1"
			CASE PS_AWARDS_ITEM_BG_2
				RETURN "PS_AWARDS_ITEM_BG_2"
			CASE PS_AWARDS_MEDAL_BG_1
				RETURN "PS_AWARDS_MEDAL_BG_1"
			CASE PS_AWARDS_MEDAL_BG_2
				RETURN "PS_AWARDS_MEDAL_BG_2"
			CASE PS_AWARDS_MEDAL_BG_3
				RETURN "PS_AWARDS_MEDAL_BG_3"
			CASE PS_AWARDS_MEDAL_SUB_1
				RETURN "PS_AWARDS_MEDAL_SUB_1"
			CASE PS_AWARDS_MEDAL_SUB_2
				RETURN "PS_AWARDS_MEDAL_SUB_2"			
			CASE PS_AWARDS_MEDAL_SUB_3
				RETURN "PS_AWARDS_MEDAL_SUB_3"
			CASE PS_DESCRIPTION_1_INFO_BG
				RETURN "PS_DESCRIPTION_1_INFO_BG"
		ENDSWITCH
		RETURN "NOTHING"
	ENDFUNC
	
	FUNC STRING PS_GET_LABEL_FROM_SPRITE_BY_ID(INT thisID)
		SWITCH INT_TO_ENUM(PS_SCREEN_SPRITE, thisID)
			CASE PS_SECONDARY_BACKGROUND RETURN "PS_SECONDARY_BACKGROUND"
			CASE PS_SELECTION_ITEM_SPRITE_1 RETURN "PS_SELECTION_ITEM_SPRITE_1"
			CASE PS_SELECTION_ITEM_SPRITE_2 RETURN "PS_SELECTION_ITEM_SPRITE_2"
			CASE PS_SELECTION_ITEM_SPRITE_3 RETURN "PS_SELECTION_ITEM_SPRITE_3"
			CASE PS_SELECTION_ITEM_SPRITE_4 RETURN "PS_SELECTION_ITEM_SPRITE_4"
			CASE PS_SELECTION_ITEM_SPRITE_5 RETURN "PS_SELECTION_ITEM_SPRITE_5"
			CASE PS_SELECTION_ITEM_SPRITE_6 RETURN "PS_SELECTION_ITEM_SPRITE_6"
			CASE PS_SELECTION_ITEM_SPRITE_7 RETURN "PS_SELECTION_ITEM_SPRITE_7"
			CASE PS_SELECTION_ITEM_SPRITE_8 RETURN "PS_SELECTION_ITEM_SPRITE_8"
			CASE PS_SELECTION_ITEM_SPRITE_9 RETURN "PS_SELECTION_ITEM_SPRITE_9"
			CASE PS_SELECTION_ITEM_SPRITE_10 RETURN "PS_SELECTION_ITEM_SPRITE_10"
			CASE PS_SELECTION_ITEM_SPRITE_11 RETURN "PS_SELECTION_ITEM_SPRITE_11"
			CASE PS_SELECTION_ITEM_SPRITE_12 RETURN "PS_SELECTION_ITEM_SPRITE_12"
			CASE PS_DESCRIPTION_IMAGE_SPRITE RETURN "PS_DESCRIPTION_IMAGE_SPRITE"
			CASE PS_DESCRIPTION_INFO_SPRITE RETURN "PS_DESCRIPTION_INFO_SPRITE"
			CASE PS_AWARDS_IMAGE_SPRITE RETURN "PS_AWARDS_IMAGE_SPRITE"
			CASE PS_AWARDS_MEDAL_SPRITE_1 RETURN "PS_AWARDS_MEDAL_SPRITE_1"
			CASE PS_AWARDS_MEDAL_SPRITE_2 RETURN "PS_AWARDS_MEDAL_SPRITE_2"
			CASE PS_AWARDS_MEDAL_SPRITE_3 RETURN "PS_AWARDS_MEDAL_SPRITE_3"
		ENDSWITCH
		RETURN "NOTHING"
	ENDFUNC
	
	FUNC STRING PS_GET_LABEL_FROM_TEXT_BY_ID(INT thisID)
		SWITCH INT_TO_ENUM(PS_SCREEN_TEXT, thisID)
			CASE PS_MENU_MAIN_TITLE RETURN "PS_MENU_MAIN_TITLE"
			CASE PS_SELECTION_TITLE RETURN "PS_SELECTION_TITLE"
			CASE PS_DESCRIPTION_TITLE RETURN "PS_DESCRIPTION_TITLE"
			CASE PS_AWARDS_TITLE RETURN "PS_AWARDS_TITLE"
			CASE PS_AWARDS_SUBTITLE RETURN "PS_AWARDS_SUBTITLE"
			CASE PS_AWARDS_VALUE RETURN "PS_AWARDS_VALUE"
			CASE PS_BEST_SUBTITLE RETURN "PS_BEST_SUBTITLE"
			CASE PS_BEST_TEXT RETURN "PS_BEST_TEXT"
			CASE PS_MEDAL_SUBTITLE_1 RETURN "PS_MEDAL_SUBTITLE_1"
			CASE PS_MEDAL_SUBTITLE_2 RETURN "PS_MEDAL_SUBTITLE_2"
			CASE PS_MEDAL_SUBTITLE_3 RETURN "PS_MEDAL_SUBTITLE_3"
			CASE PS_MEDAL_VALUE_1 RETURN "PS_MEDAL_VALUE_1"
			CASE PS_MEDAL_VALUE_2 RETURN "PS_MEDAL_VALUE_2"
			CASE PS_MEDAL_VALUE_3 RETURN "PS_MEDAL_VALUE_3"
			CASE PS_DESCRIPTION_INFO_TEXT RETURN "PS_DESCRIPTION_INFO_TEXT"
			CASE PS_SELECTION_ITEM_TITLE_1 RETURN "PS_SELECTION_ITEM_TITLE_1"
			CASE PS_SELECTION_ITEM_TITLE_2 RETURN "PS_SELECTION_ITEM_TITLE_2"
			CASE PS_SELECTION_ITEM_TITLE_3 RETURN "PS_SELECTION_ITEM_TITLE_3"
			CASE PS_SELECTION_ITEM_TITLE_4 RETURN "PS_SELECTION_ITEM_TITLE_4"
			CASE PS_SELECTION_ITEM_TITLE_5 RETURN "PS_SELECTION_ITEM_TITLE_5"
			CASE PS_SELECTION_ITEM_TITLE_6 RETURN "PS_SELECTION_ITEM_TITLE_6"
			CASE PS_SELECTION_ITEM_TITLE_7 RETURN "PS_SELECTION_ITEM_TITLE_7"
			CASE PS_SELECTION_ITEM_TITLE_8 RETURN "PS_SELECTION_ITEM_TITLE_8"
			CASE PS_SELECTION_ITEM_TITLE_9 RETURN "PS_SELECTION_ITEM_TITLE_9"
			CASE PS_SELECTION_ITEM_TITLE_10 RETURN "PS_SELECTION_ITEM_TITLE_10"
			CASE PS_SELECTION_ITEM_TITLE_11 RETURN "PS_SELECTION_ITEM_TITLE_11"
			CASE PS_SELECTION_ITEM_TITLE_12 RETURN "PS_SELECTION_ITEM_TITLE_12"
		ENDSWITCH
		RETURN "NOTHING"
	ENDFUNC
	
	PROC PS_INIT_PLACEMENT_WIDGET(MEGA_PLACEMENT_TOOLS &thisPlacement)
		PS_Widget = PS_Widget
		spriteX = FLOAT_X_TO_PIXEL(thisPlacement.SpritePlacement[0].x, 1280, TRUE) 
		spriteY = FLOAT_Y_TO_PIXEL(thisPlacement.SpritePlacement[0].y, 720, TRUE)
		
		textX = FLOAT_X_TO_PIXEL(thisPlacement.TextPlacement[0].x, 1280, TRUE) 
		textY = FLOAT_Y_TO_PIXEL(thisPlacement.TextPlacement[0].y, 720, TRUE)
		
		rectX = FLOAT_X_TO_PIXEL(thisPlacement.RectPlacement[0].x, 1280, TRUE) 
		rectY = FLOAT_Y_TO_PIXEL(thisPlacement.RectPlacement[0].y, 720, TRUE)
		
		iRectSelection = 0
		iSpriteSelection = 0
		iTextSelection = 0
		
		PS_Widget = START_WIDGET_GROUP("Flight School Menu Placements")
			START_NEW_WIDGET_COMBO()
				//add all the Rects
				dbgCnt = 0
				REPEAT ENUM_TO_INT(PS_DESCRIPTION_1_INFO_BG)+1 dbgCnt
					ADD_TO_WIDGET_COMBO(PS_GET_LABEL_FROM_RECT_BY_ID(dbgCnt))
				ENDREPEAT
			STOP_WIDGET_COMBO("Rect Selector", iRectSelection)
			ADD_WIDGET_FLOAT_SLIDER("x pos:", rectX, 0, 5000, 1)
			ADD_WIDGET_FLOAT_SLIDER("y pos:", rectY, 0, 5000, 1)
			START_NEW_WIDGET_COMBO()
				//add all the sprites
				dbgCnt = 0
				REPEAT ENUM_TO_INT(PS_AWARDS_MEDAL_SPRITE_3)+1 dbgCnt
					ADD_TO_WIDGET_COMBO(PS_GET_LABEL_FROM_SPRITE_BY_ID(dbgCnt))
				ENDREPEAT
			STOP_WIDGET_COMBO("Sprite Selector", iSpriteSelection)
			ADD_WIDGET_FLOAT_SLIDER("x pos:", spriteX, 0, 5000, 1)
			ADD_WIDGET_FLOAT_SLIDER("y pos:", spriteY, 0, 5000, 1)
			START_NEW_WIDGET_COMBO()
				//add all the Text
				dbgCnt = 0
				REPEAT ENUM_TO_INT(PS_SELECTION_ITEM_TITLE_12)+1 dbgCnt
					ADD_TO_WIDGET_COMBO(PS_GET_LABEL_FROM_TEXT_BY_ID(dbgCnt))
				ENDREPEAT
			STOP_WIDGET_COMBO("Text Selector", iTextSelection)
			ADD_WIDGET_FLOAT_SLIDER("x pos:", textX, 0, 5000, 1)
			ADD_WIDGET_FLOAT_SLIDER("y pos:", textY, 0, 5000, 1)
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC PS_UPDATE_PLACEMENT_WIDGET(MEGA_PLACEMENT_TOOLS &thisPlacement)
		
//		PRINTLN("updating")
		IF iSpriteSelection != iLastSpriteSelection
//			PRINTLN("iSpriteSelection selection changed!")
			spriteX = FLOAT_X_TO_PIXEL(thisPlacement.SpritePlacement[iSpriteSelection].x, 1280, TRUE) 
			spriteY = FLOAT_Y_TO_PIXEL(thisPlacement.SpritePlacement[iSpriteSelection].y, 720, TRUE)
			iLastSpriteSelection = iSpriteSelection
//		ELSE
//			PRINTLN("iSpriteSelection is....", iSpriteSelection, " and iLastSpriteSelection is...", iLastSpriteSelection)
		ENDIF
		
		IF iRectSelection != iLastRectSelection
//			PRINTLN("iRectSelection selection changed!")
			rectX = FLOAT_X_TO_PIXEL(thisPlacement.RectPlacement[iRectSelection].x, 1280, TRUE) 
			rectY = FLOAT_Y_TO_PIXEL(thisPlacement.RectPlacement[iRectSelection].y, 720, TRUE)
			iLastRectSelection = iRectSelection
//		ELSE
//			PRINTLN("iRectSelection is....", iRectSelection, " and iLastRectSelection is...", iLastRectSelection)
		ENDIF
		
		IF iTextSelection != iLastTextSelection
//			PRINTLN("iTextSelection selection changed!")
			textX = FLOAT_X_TO_PIXEL(thisPlacement.TextPlacement[iTextSelection].x, 1280, TRUE) 
			textY = FLOAT_Y_TO_PIXEL(thisPlacement.TextPlacement[iTextSelection].y, 720, TRUE)
			iLastTextSelection = iTextSelection
//		ELSE
//			PRINTLN("iTextSelection is....", iTextSelection, " and iLastTextSelection is...", iLastTextSelection)
		ENDIF
		
		IF thisPlacement.SpritePlacement[iSpriteSelection].x != PIXEL_X_TO_FLOAT(spriteX, 1280)
			thisPlacement.SpritePlacement[iSpriteSelection].x = PIXEL_X_TO_FLOAT(spriteX, 1280)
		ENDIF
		IF thisPlacement.SpritePlacement[iSpriteSelection].y != PIXEL_Y_TO_FLOAT(spriteY, 720)
			thisPlacement.SpritePlacement[iSpriteSelection].y = PIXEL_Y_TO_FLOAT(spriteY, 720)
		ENDIF
		
		IF thisPlacement.RectPlacement[iRectSelection].x != PIXEL_X_TO_FLOAT(rectX, 1280)
			thisPlacement.RectPlacement[iRectSelection].x = PIXEL_X_TO_FLOAT(rectX, 1280)
		ENDIF
		IF thisPlacement.RectPlacement[iRectSelection].y != PIXEL_Y_TO_FLOAT(rectY, 720)
			thisPlacement.RectPlacement[iRectSelection].y = PIXEL_Y_TO_FLOAT(rectY, 720)
		ENDIF
		
		IF thisPlacement.TextPlacement[iTextSelection].x != PIXEL_X_TO_FLOAT(textX, 1280)
			thisPlacement.TextPlacement[iTextSelection].x = PIXEL_X_TO_FLOAT(textX, 1280)
		ENDIF
		IF thisPlacement.TextPlacement[iTextSelection].y != PIXEL_Y_TO_FLOAT(textY, 720)
			thisPlacement.TextPlacement[iTextSelection].y = PIXEL_Y_TO_FLOAT(textY, 720)
		ENDIF
	ENDPROC
	
#ENDIF

PROC INIT_PS_MENU_BUTTONS()	
	CLEANUP_SIMPLE_USE_CONTEXT(menuInstructions)
	IF (PS_Challenges[g_current_selected_PilotSchool_class].LockStatus = PSS_LOCKED)				
		
		INIT_SIMPLE_USE_CONTEXT(menuInstructions, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "IB_QUIT", FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF NOT IS_PLAYER_ONLINE()
			ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "HUD_INPUT68", FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = TRUE
		ELIF IS_PLAYER_ONLINE() AND NOT PS_IS_LEADERBOARD_WRITER_ACTIVE()
			ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "HUD_INPUT68", FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = FALSE
		ELSE //player is online and writer is active
			bShowingOfflineLBButton = FALSE
		ENDIF
		
	ELSE		
		INIT_SIMPLE_USE_CONTEXT(menuInstructions, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "FE_HLP4", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "IB_QUIT", FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF NOT IS_PLAYER_ONLINE()
			ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "HUD_INPUT68", FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = TRUE
		ELIF IS_PLAYER_ONLINE() AND NOT PS_IS_LEADERBOARD_WRITER_ACTIVE()
			ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "HUD_INPUT68", FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = FALSE
		ELSE //player is online and writer is active
			bShowingOfflineLBButton = FALSE
		ENDIF
	ENDIF
	
	SET_SIMPLE_USE_CONTEXT_FULLSCREEN(menuInstructions, TRUE)
ENDPROC


PROC INIT_PS_LB_BUTTONS(BOOL bViewProfile = FALSE)
	CLEANUP_SIMPLE_USE_CONTEXT(menuInstructions)
	IF bViewProfile
		SOCIAL_CLUB_CLEAR_CONTROL_STRUCT(psLBControl)	
		INIT_SIMPLE_USE_CONTEXT(menuInstructions, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "FE_HLP3",		FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "SCLB_PROFILE",	FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
	ELSE
		SOCIAL_CLUB_CLEAR_CONTROL_STRUCT(psLBControl)	
		INIT_SIMPLE_USE_CONTEXT(menuInstructions, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "FE_HLP3",		FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	ENDIF
	
	SET_SIMPLE_USE_CONTEXT_FULLSCREEN(menuInstructions, TRUE)
ENDPROC



PROC INIT_PS_MENU(MEGA_PLACEMENT_TOOLS &thisPlacement)
	RESET_ALL_SPRITE_PLACEMENT_VALUES(thisPlacement.SpritePlacement)
	SET_STANDARD_INGAME_TEXT_DETAILS(thisPlacement.aStyle)
	
	DISABLE_CELLPHONE(TRUE)
		
	INIT_PS_MENU_BUTTONS()
	
	bIsWidescreen = GET_IS_WIDESCREEN()
	INIT_SCREEN_PS_MENU(thisPlacement)
	
	#IF IS_DEBUG_BUILD
//		PS_INIT_PLACEMENT_WIDGET(thisPlacement)
	#ENDIF
ENDPROC


#IF IS_DEBUG_BUILD 
PROC PS_MENU_DRAW_DEBUG_OPTIONS()
	

	CONST_FLOAT fDiffForPrint	0.04		//0.03		//0.05
	FLOAT xpos, ypos
	xpos = 0.060//0.3660
	ypos = 0.7500
	
	//RectPieceNum: 0
	DRAW_RECT(xpos + 0.14, ypos + 0.08 , 0.3000, 0.1600, 128, 128, 0, 255)
	
	//TextPieceNum: 0
	SET_TEXT_SCALE(0.5000, 0.5000)
	SET_TEXT_COLOUR(0, 0, 128, 255)
	SET_TEXT_DROP_SHADOW()
	DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*0.0), "STRING", "'W' to unlock all challenges")

	//TextPieceNum: 1
	SET_TEXT_SCALE(0.5000, 0.5000)
	SET_TEXT_COLOUR(0, 0, 128, 255)
	SET_TEXT_DROP_SHADOW()
	DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*1.0), "STRING", "'Shift-W' to reset challenges")

	//TextPieceNum: 2
	SET_TEXT_SCALE(0.5000, 0.5000)
	SET_TEXT_COLOUR(0, 0, 128, 255)
	SET_TEXT_DROP_SHADOW()
	
	INT tempidx = ENUM_TO_INT(g_current_selected_PilotSchool_class)
	IF PS_Challenges[tempidx].LockStatus = PSS_LOCKED
		//to unlock a challenge, the challenge before must have some sort of score
		IF tempidx > 0 AND PS_Challenges[(tempidx - 1)].LockStatus = PSS_UNLOCKED
			DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*2.0), "STRING", "'P' to unlock this challenge")
		ELSE
			DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*2.0), "STRING", "Pass previous challenge first")
		ENDIF
	ELIF PS_Challenges[tempidx].LockStatus = PSS_NEW
		DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*2.0), "STRING", "'P' to pass this challenge")
	ELIF PS_Challenges[tempidx].LockStatus = PSS_UNLOCKED
		IF tempidx > 0
			DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*2.0), "STRING", "'P' to lock this challenge")
		ELSE
			DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*2.0), "STRING", "'P' to generate new score")
		ENDIF
	ENDIF

	//TextPieceNum: 3
	SET_TEXT_SCALE(0.5000, 0.5000)
	SET_TEXT_COLOUR(0, 0, 128, 255)
	SET_TEXT_DROP_SHADOW()
	IF bDebugLogDataFile
		DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*3.0), "STRING", "'C' to turn stat logging off")
	ELSE
		DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*3.0), "STRING", "'C' to turn stat logging on")
	ENDIF

ENDPROC
#ENDIF



PROC PROCESS_SCREEN_PS_MENU(MEGA_PLACEMENT_TOOLS &thisPlacement)
//	#IF IS_DEBUG_BUILD
//		PS_UPDATE_PLACEMENT_WIDGET(thisPlacement)
//		SPRITE_PLACEMENT pauseGuidePlacement
//		PIXEL_POSITION_AND_SIZE_SPRITE(pauseGuidePlacement, 0, 0, 1280, 720, TRUE)
//		SPRITE_COLOR(pauseGuidePlacement, 255, 255, 255, 184 )
//		DRAW_2D_SPRITE("pilotSchool", "pauseguide", pauseGuidePlacement, FALSE)
//	#ENDIF

	
//	SPRITE_PLACEMENT tempoverlay
//	PIXEL_POSITION_AND_SIZE_SPRITE(tempoverlay, 0, 0, 1280, 720, TRUE)
//	SET_SPRITE_WHITE(tempoverlay)
//	tempoverlay.a = 128
//	DRAW_2D_SPRITE("PS_Menu", "menu_test", tempoverlay, FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)	
	
	//pad instructions
	UPDATE_SIMPLE_USE_CONTEXT(menuInstructions)
	
	IF NOT bIsWidescreen
		IF NOT IS_PC_VERSION()
			SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_CENTRE)
		ENDIF
	ENDIF

	//background
	DRAW_2D_SPRITE("Shared", "BGGradient_32x1024", thisPlacement.SpritePlacement[PS_SECONDARY_BACKGROUND], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_BEFORE_HUD)

	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_SELECTION_BACKGROUND])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_DESCRIPTION_BACKGROUND])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_BACKGROUND])

	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_SELECTION_EDGING])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_DESCRIPTION_EDGING])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_EDGING])

	
	//main title
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TITLE)
	DRAW_TEXT(thisPlacement.TextPlacement[PS_MENU_MAIN_TITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "PS_TITLE")	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	//subtitles n such


	SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_DESCRIPTION_IMAGE_BACKGROUND])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_IMAGE_BACKGROUND])

	SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, thisPlacement.RectPlacement[PS_SELECTION_BACKGROUND], 0, 0)
		DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[PS_SELECTION_TITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "PSM_SELECT", TRUE, FALSE)
	CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, thisPlacement.RectPlacement[PS_DESCRIPTION_BACKGROUND], 0, 0)
		DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[PS_DESCRIPTION_TITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "PSM_INFO", TRUE, FALSE)
	CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, thisPlacement.RectPlacement[PS_AWARDS_BACKGROUND], 0, 0)
		DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[PS_AWARDS_TITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "PSM_AWARD_TITLE", TRUE, FALSE)
	CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	//reset text
	SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)

	
	
	//keep track of selection
	INT iCurIdx = 0
	HIGHLIGHT_OPTION eCurHighlightOption = HIGHLIGHT_OPTION_NORMAL
	PS_SCREEN_RECT eCurRectIdx = PS_SELECTION_ITEM_BG_1//keeps track of what rect we're drawing
	PS_SCREEN_TEXT eCurTitleIdx = PS_SELECTION_ITEM_TITLE_1//keeps track of what text position we're using for challenge titles
//	PS_SCREEN_TEXT eCurNewTextIdx = PS_SELECTION_ITEM_NEW_TEXT_1//keeps track of what text position we're using for "new" challenge
	PS_SCREEN_SPRITE eCurSpriteIdx = PS_SELECTION_ITEM_SPRITE_1//keeps track of what sprite position we're using for mini medals
	STRING sCurTxtDict = "Shared"
	STRING sCurMedalSprite = "Locked_Icon_32"
	SPRITE_PLACEMENT tempSprPlacement
	TEXT_PLACEMENT tempTxtPlacement
	
	//draw selections
	REPEAT NUMBER_OF_PILOT_SCHOOL_CLASSES iCurIdx
		
		//draw selection
		IF iCurIdx = ENUM_TO_INT(g_current_selected_PilotSchool_class)
			
//			//highlight last medal.
//			IF PS_PlayerData[g_current_selected_PilotSchool_class].eLastMedal > PS_NONE
//				IF PS_PlayerData[g_current_selected_PilotSchool_class].eLastMedal = PS_GOLD
//					SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_1], HUD_COLOUR_GOLD, TRUE)
//					SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_2], HUD_COLOUR_PAUSE_BG, TRUE)
//					SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_3], HUD_COLOUR_PAUSE_BG, TRUE)
//				ELIF PS_PlayerData[g_current_selected_PilotSchool_class].eLastMedal = PS_SILVER
//					SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_1], HUD_COLOUR_PAUSE_BG, TRUE)
//					SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_2], HUD_COLOUR_SILVER, TRUE)
//					SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_3], HUD_COLOUR_PAUSE_BG, TRUE)
//				ELIF PS_PlayerData[g_current_selected_PilotSchool_class].eLastMedal = PS_BRONZE
//					SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_1], HUD_COLOUR_PAUSE_BG, TRUE)
//					SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_2], HUD_COLOUR_PAUSE_BG, TRUE)
//					SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_3], HUD_COLOUR_BRONZE, TRUE)
//				ENDIF
//			ELSE
//				SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_1], HUD_COLOUR_PAUSE_BG, TRUE)
//				SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_2], HUD_COLOUR_PAUSE_BG, TRUE)
//				SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_3], HUD_COLOUR_PAUSE_BG, TRUE)
//			ENDIF
			//display "best" score
			//initially set text to white
			SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Challenges[g_current_selected_PilotSchool_class], FSG_0_timeTaken)
				//last medal time
				IF PS_PlayerData[g_current_selected_PilotSchool_class].LastElapsedTime >= 0
					DRAW_TEXT(thisPlacement.TextPlacement[PS_RECENT_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_RTIME")//most recent time
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_2], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT_TIMER(thisPlacement.TextPlacement[PS_RECENT_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, FLOOR(PS_PlayerData[g_current_selected_PilotSchool_class].LastElapsedTime * 1000.0),TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", FALSE, TRUE )
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					//highlight medal
//					HIGHLIGHT_OPTION_SELECTED_WHITE
				ELSE
					IF PS_Challenges[iCurIdx].LockStatus = PSS_LOCKED
						SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					ENDIF
					DRAW_TEXT(thisPlacement.TextPlacement[PS_RECENT_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_RTIME")//most recent time
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_2], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[PS_RECENT_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SC_LB_EMPTY", FALSE, TRUE)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				ENDIF
				//best saved time
				IF PS_PlayerData[g_current_selected_PilotSchool_class].ElapsedTime >= 0
					DRAW_TEXT(thisPlacement.TextPlacement[PS_BEST_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_BTIME")//best time
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_3], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT_TIMER(thisPlacement.TextPlacement[PS_BEST_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, FLOOR(PS_PlayerData[g_current_selected_PilotSchool_class].ElapsedTime * 1000.0),TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", FALSE, TRUE )
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				ELSE
					IF PS_Challenges[iCurIdx].LockStatus = PSS_LOCKED
						SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					ENDIF
					DRAW_TEXT(thisPlacement.TextPlacement[PS_BEST_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_BTIME")//best time
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_3], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
//					DRAW_TEXT(thisPlacement.TextPlacement[PS_BEST_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SC_LB_EMPTY")
					DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[PS_BEST_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SC_LB_EMPTY", FALSE, TRUE)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				ENDIF
				
			ELIF Pilot_School_Data_Is_Goal_Bit_Set(PS_Challenges[g_current_selected_PilotSchool_class], FSG_2_distanceFromTarget)
				//last medal dist
				IF PS_PlayerData[g_current_selected_PilotSchool_class].LastLandingDistance >= 0
					DRAW_TEXT(thisPlacement.TextPlacement[PS_RECENT_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_RDIST")//most recent dist
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_2], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[PS_RECENT_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_PlayerData[g_current_selected_PilotSchool_class].LastLandingDistance), 2, FONT_RIGHT)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				ELSE
					IF PS_Challenges[iCurIdx].LockStatus = PSS_LOCKED
						SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					ENDIF
					DRAW_TEXT(thisPlacement.TextPlacement[PS_RECENT_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_RDIST")//most recent dist
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_2], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[PS_RECENT_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SC_LB_EMPTY", FALSE, TRUE)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				ENDIF
				//best medal dist
				IF PS_PlayerData[g_current_selected_PilotSchool_class].LandingDistance >= 0
					DRAW_TEXT(thisPlacement.TextPlacement[PS_BEST_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_BDIST")//best dist
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_3], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[PS_BEST_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_PlayerData[g_current_selected_PilotSchool_class].LandingDistance), 2, FONT_RIGHT)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)					
				ELSE
					IF PS_Challenges[iCurIdx].LockStatus = PSS_LOCKED
						SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					ENDIF
					DRAW_TEXT(thisPlacement.TextPlacement[PS_BEST_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_BDIST")//best dist
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_3], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[PS_BEST_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SC_LB_EMPTY", FALSE, TRUE)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				ENDIF
			ENDIF
			//highlight rect and text
			eCurHighlightOption = HIGHLIGHT_OPTION_SELECTED_WHITE
			SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			
						//draw score/awards info
			DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_1])
			DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_2])
			DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_3])
			DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_1])
			DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_2])
			DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_BG_3])
			
			//set colors for medals/award
			IF iChallengeScores[iCurIdx] >= iSCORE_FOR_GOLD
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				DRAW_TEXT(thisPlacement.TextPlacement[PS_AWARDS_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_AWARDS_SUB")
				SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
				SET_SPRITE_WHITE(thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE])
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Gold_128", thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_1])
					SET_TEXT_GOLD(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					SET_TEXT_RIGHT_JUSTIFY(TRUE)
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_1], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT(thisPlacement.TextPlacement[PS_AWARDS_VALUE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PS_sGOLD")
					SET_TEXT_RIGHT_JUSTIFY(FALSE)
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_1].a = ROUND(255)
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_2].a = ROUND(255)
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_3].a = ROUND(255)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Gold_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_1], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Silver_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_2], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Bronze_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_3], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
			ELIF iChallengeScores[iCurIdx] >= iSCORE_FOR_SILVER
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				DRAW_TEXT(thisPlacement.TextPlacement[PS_AWARDS_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_AWARDS_SUB")
				SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
				SET_SPRITE_WHITE(thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE])
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Silver_128", thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_1])
					SET_TEXT_SILVER(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					SET_TEXT_RIGHT_JUSTIFY(TRUE)
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_1], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT(thisPlacement.TextPlacement[PS_AWARDS_VALUE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PS_sSILVER")
					SET_TEXT_RIGHT_JUSTIFY(FALSE)
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				// Gold medal should be at 30%
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_1].a = ROUND(255 * 0.30)
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_2].a = ROUND(255)
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_3].a = ROUND(255)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Gold_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_1], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Silver_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_2], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Bronze_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_3], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
			ELIF iChallengeScores[iCurIdx] >= iSCORE_FOR_BRONZE
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				DRAW_TEXT(thisPlacement.TextPlacement[PS_AWARDS_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_AWARDS_SUB")
				SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
				SET_SPRITE_WHITE(thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE])
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Bronze_128", thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_1])
					SET_TEXT_BRONZE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					SET_TEXT_RIGHT_JUSTIFY(TRUE)
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_1], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT(thisPlacement.TextPlacement[PS_AWARDS_VALUE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PS_sBRONZE")
					SET_TEXT_RIGHT_JUSTIFY(FALSE)
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				// Gold and silver medal should be at 30%
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_1].a = ROUND(255 * 0.30)
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_2].a = ROUND(255 * 0.30)
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_3].a = ROUND(255)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Gold_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_1], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Silver_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_2], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Bronze_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_3], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
			ELIF PS_Challenges[iCurIdx].LockStatus = PSS_LOCKED
				//if there is no medal and lesson is locked
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				DRAW_TEXT(thisPlacement.TextPlacement[PS_AWARDS_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_AWARDS_SUB")
				SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_1])
					SET_TEXT_RIGHT_JUSTIFY(TRUE)
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_1], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT(thisPlacement.TextPlacement[PS_AWARDS_VALUE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SC_LB_EMPTY")
					SET_TEXT_RIGHT_JUSTIFY(FALSE)	
				CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				
				SET_SPRITE_WHITE(thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE])
				thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE].a = ROUND(255 * 0.30)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Bronze_128", thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_BEFORE_HUD)
				// All medals should be at 30%
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_1].a = ROUND(255 * 0.30)
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_2].a = ROUND(255 * 0.30)
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_3].a = ROUND(255 * 0.30)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Gold_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_1], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Silver_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_2], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Bronze_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_3], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				
				//reset text
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			ELSE
				//if there is no medal but lesson is unlocked
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				DRAW_TEXT(thisPlacement.TextPlacement[PS_AWARDS_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSM_AWARDS_SUB")
				SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_1])
					SET_TEXT_RIGHT_JUSTIFY(TRUE)
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PS_AWARDS_ITEM_BG_1], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					DRAW_TEXT(thisPlacement.TextPlacement[PS_AWARDS_VALUE], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SC_LB_EMPTY")
					SET_TEXT_RIGHT_JUSTIFY(FALSE)	
				CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				SET_SPRITE_WHITE(thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE])
				thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE].a = ROUND(255 * 0.30)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Bronze_128", thisPlacement.SpritePlacement[PS_AWARDS_IMAGE_SPRITE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_BEFORE_HUD)
				// All medals should be at 30%
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_1].a = ROUND(255 * 0.30)
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_2].a = ROUND(255 * 0.30)
				thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_3].a = ROUND(255 * 0.30)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Gold_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_1], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Silver_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_2], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Bronze_128", thisPlacement.SpritePlacement[PS_AWARDS_MEDAL_SPRITE_3], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
//				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
			ENDIF
		ELSE
			//dont highlight rect or text
			eCurHighlightOption = HIGHLIGHT_OPTION_NORMAL
		ENDIF		
		
		//now draw challenge selection rect with the name on top and an icon to the side
		IF (eCurHighlightOption = HIGHLIGHT_OPTION_SELECTED_WHITE)			
			SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[eCurRectIdx], HUD_COLOUR_WHITE, TRUE)
		ELSE
			SET_RECT_UNDIMMED(thisPlacement.RectPlacement[eCurRectIdx])
		ENDIF
		DRAW_RECTANGLE(thisPlacement.RectPlacement[eCurRectIdx])

//		IF PS_Challenges[iCurIdx].LockStatus = PSS_LOCKED
//			IF iCurIdx = ENUM_TO_INT(g_current_selected_PilotSchool_class)
//				SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)				
//				SET_SPRITE_HUD_COLOUR(thisPlacement.SpritePlacement[eCurSpriteIdx], HUD_COLOUR_BLACK)
//			ELSE
//				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)				
//				SET_SPRITE_HUD_COLOUR(thisPlacement.SpritePlacement[eCurSpriteIdx], HUD_COLOUR_WHITE)
//			ENDIF
//			
//		ELSE
//			IF iCurIdx = ENUM_TO_INT(g_current_selected_PilotSchool_class)
//				SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
//			ENDIF
//			DRAW_TEXT(thisPlacement.TextPlacement[eCurTitleIdx], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, PS_Challenges[iCurIdx].Title)
//			SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)			
//			SET_SPRITE_HUD_COLOUR(thisPlacement.SpritePlacement[eCurSpriteIdx], HUD_COLOUR_WHITE)
//		ENDIF

		IF PS_Challenges[iCurIdx].LockStatus = PSS_NEW
			//draw text "new" where sprite is
			IF iCurIdx = ENUM_TO_INT(g_current_selected_PilotSchool_class)
				SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				SET_SPRITE_HUD_COLOUR(thisPlacement.SpritePlacement[eCurSpriteIdx], HUD_COLOUR_BLACK)
			ELSE
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				SET_SPRITE_HUD_COLOUR(thisPlacement.SpritePlacement[eCurSpriteIdx], HUD_COLOUR_WHITE)
			ENDIF
			
			//draw new star
			tempTxtPlacement = thisPlacement.TextPlacement[eCurTitleIdx]
			
			PIXEL_POSITION_AND_SIZE_SPRITE(tempSprPlacement, 269.0000, 0.0, 32.0000, 32.0000)
			SET_SPRITE_WHITE(tempSprPlacement)		
			tempSprPlacement.y = thisPlacement.TextPlacement[eCurTitleIdx].y + NEW_STAR_OFFSET_Y
			tempTxtPlacement.x = tempSprPlacement.x + NEW_STAR_OFFSET_X
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(tempSprPlacement)
			ENDIF

			DRAW_2D_SPRITE("Shared", "NewStar_32", tempSprPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
			
			// Only fixup if we're shifting the text.
			IF NOT bIsWidescreen
				FIXUP_TEXT_FOR_NON_WIDESCREEN(tempTxtPlacement, thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			ENDIF
			
			//draw title
			CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			IF IS_PC_VERSION()
				TEXT_STYLE nonWidescreenTextStyle = thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY
//				IF NOT GET_IS_WIDESCREEN()
//					nonWidescreenTextStyle.XScale = 0.265
//					nonWidescreenTextStyle.YScale = 0.265
//				ENDIF
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_RUSSIAN
					nonWidescreenTextStyle.XScale = 0.325
					nonWidescreenTextStyle.YScale = 0.325
				ENDIF
				DRAW_TEXT(tempTxtPlacement, nonWidescreenTextStyle, PS_Challenges[iCurIdx].Title)
			ELSE
				DRAW_TEXT(tempTxtPlacement, thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, PS_Challenges[iCurIdx].Title)
			ENDIF
		ELSE
			//draw title
			IF iCurIdx = ENUM_TO_INT(g_current_selected_PilotSchool_class)
				SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			ELSE
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)	
			ENDIF

			CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			IF IS_PC_VERSION()
				TEXT_STYLE nonWidescreenTextStyle = thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY
//				IF NOT GET_IS_WIDESCREEN()
//					nonWidescreenTextStyle.XScale = 0.265
//					nonWidescreenTextStyle.YScale = 0.265
//				ENDIF
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_RUSSIAN
					nonWidescreenTextStyle.XScale = 0.325
					nonWidescreenTextStyle.YScale = 0.325
				ENDIF
				DRAW_TEXT(thisPlacement.TextPlacement[eCurTitleIdx], nonWidescreenTextStyle, PS_Challenges[iCurIdx].Title)
			ELSE
				DRAW_TEXT(thisPlacement.TextPlacement[eCurTitleIdx], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, PS_Challenges[iCurIdx].Title)
			ENDIF
			
			//reset colors
			SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)			
			SET_SPRITE_HUD_COLOUR(thisPlacement.SpritePlacement[eCurSpriteIdx], HUD_COLOUR_WHITE)
			
			//draw medal or lock icon
			//set mini-medal sprites 
			
			IF iChallengeScores[iCurIdx] >= iSCORE_FOR_BRONZE
				sCurTxtDict = "PS_Menu"
				sCurMedalSprite = "Common_Medal"
				IF iChallengeScores[iCurIdx] >= iSCORE_FOR_GOLD
//					PRINTLN("SETTING MINI MEDAL GOLD")
					SET_SPRITE_GOLD(thisPlacement.SpritePlacement[eCurSpriteIdx + INT_TO_ENUM(PS_SCREEN_SPRITE, 12)]) //ad 12 to use the sprite thats 16x16
				ELIF iChallengeScores[iCurIdx] >= iSCORE_FOR_SILVER
//					PRINTLN("SETTING MINI MEDAL SILVER")
					SET_SPRITE_SILVER(thisPlacement.SpritePlacement[eCurSpriteIdx + INT_TO_ENUM(PS_SCREEN_SPRITE, 12)])
				ELIF iChallengeScores[iCurIdx] >= iSCORE_FOR_BRONZE
//					PRINTLN("SETTING MINI MEDAL BRONZE")
					SET_SPRITE_BRONZE(thisPlacement.SpritePlacement[eCurSpriteIdx + INT_TO_ENUM(PS_SCREEN_SPRITE, 12)])
				ENDIF
				DRAW_2D_SPRITE(sCurTxtDict, sCurMedalSprite, thisPlacement.SpritePlacement[eCurSpriteIdx + INT_TO_ENUM(PS_SCREEN_SPRITE, 12)], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
			ELSE
				sCurTxtDict = "Shared"
				sCurMedalSprite = "Locked_Icon_32"
				IF iCurIdx = ENUM_TO_INT(g_current_selected_PilotSchool_class)
					SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					SET_SPRITE_BLACK(thisPlacement.SpritePlacement[eCurSpriteIdx])
				ELSE
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
				ENDIF
				DRAW_2D_SPRITE(sCurTxtDict, sCurMedalSprite, thisPlacement.SpritePlacement[eCurSpriteIdx], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
			ENDIF
			
		ENDIF
		SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		
		//increment rect, text and sprite indexes
		eCurRectIdx += INT_TO_ENUM(PS_SCREEN_RECT, 1)
		eCurTitleIdx += INT_TO_ENUM(PS_SCREEN_TEXT, 1)
		eCurSpriteIdx += INT_TO_ENUM(PS_SCREEN_SPRITE, 1)
//		eCurNewTextIdx += INT_TO_ENUM(PS_SCREEN_TEXT, 1)
	ENDREPEAT

	//figure out what description image to display
	STRING sDescriptionImageName = "Locked_Icon_32"
	SWITCH g_current_selected_PilotSchool_class
		CASE PSC_Takeoff
			sDescriptionImageName = "FlightSchool_1a_RunwayTakeOff"
			BREAK
		CASE PSC_Landing
			sDescriptionImageName = "FlightSchool_2a_RunwayLanding"
			BREAK
		CASE PSC_Inverted
			sDescriptionImageName = "FlightSchool_1_Invert"
			BREAK
		CASE PSC_Knifing
			sDescriptionImageName = "FlightSchool_2_Knife"
			BREAK
		CASE PSC_loopTheLoop
			sDescriptionImageName = "FlightSchool_3_Loop"
			BREAK
		CASE PSC_FlyLow
			sDescriptionImageName = "FlightSchool_4_LowFly"
			BREAK
		CASE PSC_DaringLanding
			sDescriptionImageName = "FlightSchool_5_Land"
			BREAK
		CASE PSC_heliCourse
			sDescriptionImageName = "FlightSchool_7_HeliObstacle"
			BREAK
		CASE PSC_heliSpeedRun
			sDescriptionImageName = "FlightSchool_8_HeliSpeed"
			BREAK
		CASE PSC_parachuteOntoTarget
			sDescriptionImageName = "FlightSchool_9_SkyDive"
			BREAK
		CASE PSC_chuteOntoMovingTarg
			sDescriptionImageName = "FlightSchool_10_SkyDiveMoving"
			BREAK
		CASE PSC_planeCourse
			sDescriptionImageName = "FlightSchool_6_Obstacle"
			BREAK
	ENDSWITCH
	
	//draw description image
	DRAW_2D_SPRITE("PS_Menu", sDescriptionImageName, thisPlacement.SpritePlacement[PS_DESCRIPTION_IMAGE_SPRITE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
	
	//figure out and draw drescription string
	TEXT_LABEL sDescriptionLabel = "PS_DESC_"
	sDescriptionLabel += ENUM_TO_INT(g_current_selected_PilotSchool_class)+1
	IF PS_Challenges[g_current_selected_PilotSchool_class].LockStatus = PSS_LOCKED
		SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
		SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)	
	ELSE	
		SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
		SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)	
	ENDIF
	SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[PS_DESCRIPTION_1_INFO_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
	SET_TEXT_LEADING(1)
	
	TEXT_PLACEMENT txtTemp = thisPlacement.TextPlacement[PS_DESCRIPTION_INFO_TEXT]
	TEXT_STYLE txtStyleTemp = thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY
	IF NOT bIsWidescreen
		txtStyleTemp.WrapEndX -= 0.116
		FIXUP_TEXT_FOR_NON_WIDESCREEN(txtTemp, txtStyleTemp)
	ENDIF

	IF bIsWidescreen
		SET_RECT_DYNAMIC_TO_STRING(thisPlacement.RectPlacement[PS_DESCRIPTION_1_INFO_BG], sDescriptionLabel, txtTemp, txtStyleTemp, 27.0, 2.0, 5, 159.0)
	ELSE
		SET_RECT_DYNAMIC_TO_STRING(thisPlacement.RectPlacement[PS_DESCRIPTION_1_INFO_BG], sDescriptionLabel, txtTemp, txtStyleTemp, 18.0, 1.55, 5, 106.0)
	ENDIF
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_DESCRIPTION_1_INFO_BG])

	DRAW_TEXT(thisPlacement.TextPlacement[PS_DESCRIPTION_INFO_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, sDescriptionLabel)
	CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)

	//set text black for medal goal times
	SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_1])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_2])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_3])
	
	//fix for B*2149607
	//apply local text style for drawing medals and medal goals, that is by default
	//a copy of thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, but scales down text when on PC in non widescreen formats
	TEXT_STYLE LocalTextStyle = thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY
	//no longer need this here as text scaling is fixed for 2240510 and 2279960
//	IF IS_PC_VERSION()
//		IF NOT GET_IS_WIDESCREEN()
//			LocalTextStyle.XScale = 0.265
//			LocalTextStyle.YScale = 0.265
//		ENDIF
//	ENDIF
	
	//draw medals and medal goal times
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Challenges[g_current_selected_PilotSchool_class], FSG_0_timeTaken)
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_1])
			DRAW_TEXT_TIMER(thisPlacement.TextPlacement[PS_MEDAL_VALUE_1], LocalTextStyle, FLOOR(PS_Challenges[g_current_selected_PilotSchool_class].GoldTime * 1000.0),TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE, FALSE )
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
		
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_1])
			DRAW_TEXT_TIMER(thisPlacement.TextPlacement[PS_MEDAL_VALUE_2], LocalTextStyle, FLOOR(PS_Challenges[g_current_selected_PilotSchool_class].SilverTime * 1000.0),TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE, FALSE )		
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
		
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_1])
			IF PS_Challenges[g_current_selected_PilotSchool_class].BronzeTime > 0
				DRAW_TEXT_TIMER(thisPlacement.TextPlacement[PS_MEDAL_VALUE_3], LocalTextStyle, FLOOR(PS_Challenges[g_current_selected_PilotSchool_class].BronzeTime * 1000.0),TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE, FALSE )
			ELSE	
				DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[PS_MEDAL_VALUE_3], LocalTextStyle, "PS_sFINISH", TRUE, FALSE)
	//			DRAW_TEXT(thisPlacement.TextPlacement[PS_MEDAL_VALUE_3], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SC_LB_EMPTY")
			ENDIF
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)	
	ELIF Pilot_School_Data_Is_Goal_Bit_Set(PS_Challenges[g_current_selected_PilotSchool_class], FSG_2_distanceFromTarget)
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_1])
			DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[PS_MEDAL_VALUE_1], LocalTextStyle, "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_Challenges[g_current_selected_PilotSchool_class].GoldDistance), 2, FONT_CENTRE)
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
		
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_1])
		DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[PS_MEDAL_VALUE_2], LocalTextStyle, "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_Challenges[g_current_selected_PilotSchool_class].SilverDistance), 2, FONT_CENTRE)
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
		
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_1])
			IF PS_Challenges[g_current_selected_PilotSchool_class].BronzeDistance > 0
				DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[PS_MEDAL_VALUE_3], LocalTextStyle, "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_Challenges[g_current_selected_PilotSchool_class].BronzeDistance), 2,FONT_CENTRE)
			ELSE	
				DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[PS_MEDAL_VALUE_3], LocalTextStyle, "PS_sFINISH", TRUE, FALSE)
	//			DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[PS_MEDAL_VALUE_3], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_Challenges[g_current_selected_PilotSchool_class].BronzeDistance), 2,FONT_CENTRE)
			ENDIF
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)	
	ELIF Pilot_School_Data_Is_Goal_Bit_Set(PS_Challenges[g_current_selected_PilotSchool_class], FSG_4_formationCompletion)
	
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_1])
			DRAW_TEXT_WITH_NUMBER(thisPlacement.TextPlacement[PS_MEDAL_VALUE_1], LocalTextStyle, "PERCENTAGE", 90, FONT_CENTRE)
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)	
		
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_2])
			DRAW_TEXT_WITH_NUMBER(thisPlacement.TextPlacement[PS_MEDAL_VALUE_2], LocalTextStyle, "PERCENTAGE", 80, FONT_CENTRE)
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)	
		
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[PS_AWARDS_MEDAL_SUB_3])
			DRAW_TEXT_WITH_NUMBER(thisPlacement.TextPlacement[PS_MEDAL_VALUE_3], LocalTextStyle, "PERCENTAGE", 70, FONT_CENTRE)
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)	
	ENDIF
	
	//reset text color
	SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
//	#IF IS_DEBUG_BUILD
//		PS_MENU_DRAW_DEBUG_OPTIONS()
//	#ENDIF
	
ENDPROC

PROC PS_MENU_SETUP_CAMERA()	
	
	IF DOES_CAM_EXIST(camMainMenu)
		DESTROY_CAM(camMainMenu)
	ENDIF
	CAMERA_INDEX camToDestroy
	
	SETTIMERA(0)
	NEW_LOAD_SCENE_START(PS_MENU_LOAD_SCENE_COORDS, PS_CONVERT_ROTATION_TO_DIRECTION(PS_MENU_CAM_ROT), 1000)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		PRINTLN("SETTING PLAYER TO NEW MENU COORDS!")
		SET_ENTITY_COORDS(PLAYER_PED_ID(), PS_MENU_PLAYER_COORD)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), PS_MENU_PLAYER_HEADING)
	ENDIF
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND TIMERA() < 5000
			PS_HUD_HIDE_GAMEHUD_ELEMENTS(TRUE)
			WAIT(0)
		ENDWHILE
		PS_HUD_HIDE_GAMEHUD_ELEMENTS(TRUE)
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	IF ARE_VECTORS_EQUAL(vPS_MenuCamCoords, <<0, 0, 0>>)// OR !bDontFadeToMenu
		PRINTLN("Setting up menu cam with the airport shot.")
		camToDestroy = GET_RENDERING_CAM()
		camMainMenu = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, PS_MENU_CAM_COORDS, PS_MENU_CAM_ROT, PS_MENU_CAM_FOV, TRUE)
		vPS_MenuCamCoords = PS_MENU_CAM_COORDS
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ELSE
		camToDestroy = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<PS_MENU_CAM_COORDS.x, PS_MENU_CAM_COORDS.y, PS_MENU_CAM_COORDS.z + 1000>>, PS_MENU_CAM_ROT, PS_MENU_CAM_FOV, TRUE)
		IF DOES_CAM_EXIST(camToDestroy)
			SET_CAM_ACTIVE(camToDestroy, TRUE)
		ENDIF
		IF IS_CAM_ACTIVE(camToDestroy)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		ENDIF
		camMainMenu = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, PS_MENU_CAM_COORDS, PS_MENU_CAM_ROT, PS_MENU_CAM_FOV)
		ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
		SET_CAM_ACTIVE_WITH_INTERP(camMainMenu, camToDestroy, 500)
		PS_PLAY_WOOSH_SOUND()
		PRINTLN("New load scene finished ")
		ANIMPOSTFX_STOP("MinigameTransitionOut")
		ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
	ENDIF	
	
	IF DOES_CAM_EXIST(PS_EndCutCam)
		DESTROY_CAM(PS_EndCutCam)
	ENDIF
	
	IF DOES_CAM_EXIST(camToDestroy)
		SET_CAM_ACTIVE(camToDestroy, FALSE)
		DESTROY_CAM(camToDestroy, TRUE)
	ENDIF
	
	
ENDPROC


/// PURPOSE:
///    Resets all of the scores
PROC PS_MENU_RESET_SCORES()

	INT iNodeCount = 0
	REPEAT NUMBER_OF_PILOT_SCHOOL_CLASSES iNodeCount
			PS_Challenges[iNodeCount].LockStatus		= PSS_LOCKED
			PS_PlayerData[iNodeCount].ElapsedTime		= -1
			PS_PlayerData[iNodeCount].eMedal			= PS_NONE
			iChallengeScores[iNodeCount] 				= -1
	ENDREPEAT
	
	//always keep first challenge unlocked
	PS_Challenges[PSC_Inverted].LockStatus		= PSS_NEW
	

ENDPROC


//PURPOSE:	Initializes all the variables used within the scope of the script
PROC PS_MENU_LOAD_VARIABLES()                                

	//load up lesson data
	PS_Load_Pilot_School_Data() 
	
	//init player score data
	PS_MENU_RESET_SCORES()

	//first set everything to unlocked
	INT k
	REPEAT NUMBER_OF_PILOT_SCHOOL_CLASSES k
		PS_Challenges[k].LockStatus = PSS_UNLOCKED
	ENDREPEAT
	INT iSlot = ENUM_TO_INT(GET_PLAYER_PED_ENUM(PLAYER_PED_ID()))
	//Stop array over run in MP (temp)
	#IF IS_DEBUG_BUILD
		IF NETWORK_IS_GAME_IN_PROGRESS()
			iSlot = 0
		ENDIF
	#ENDIF
	//unlock other challenges if they have saved scores
	REPEAT NUMBER_OF_PILOT_SCHOOL_CLASSES k
		//get saved data
		PS_PlayerData[k]  = g_savedGlobals.sFlightSchoolData[iSlot].PlayerData[k]
		//calculate score
		IF PS_PlayerData[k].HasPassedLesson
			iChallengeScores[k] = Pilot_School_Data_Get_Score(PS_Challenges[k], PS_PlayerData[k], TRUE)
		ELSE
			iChallengeScores[k] = -1
		ENDIF
		#IF IS_DEBUG_BUILD
			DEBUG_MESSAGE("******Calculating scores for menu******")
			DEBUG_MESSAGE(PS_Challenges[k].Title, " is ", GET_STRING_FROM_INT(iChallengeScores[k]))
			PRINTLN("Medal for this challenge is... ", PS_PlayerData[k].eMedal)
		#ENDIF
		IF iChallengeScores[k] >= iSCORE_FOR_BRONZE
			PS_Challenges[k].HasBeenPreviewed = TRUE
			PS_Challenges[k].LockStatus = PSS_UNLOCKED
			PRINTLN("Status for this challenge is unlocked!")
		ELSE 
			PS_Challenges[k].LockStatus = PSS_LOCKED
			IF k > 0 //make sure we don't check a previous index unless we're at least at the 2nd index
				IF PS_Challenges[k-1].LockStatus = PSS_UNLOCKED
					PS_Challenges[k].LockStatus = PSS_NEW
					PRINTLN("Status for this challenge NEW!")
				ELSE
					PRINTLN("Status for this challenge is LOCKED!")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//first challenge always unlocked
	IF PS_Challenges[0].LockStatus = PSS_LOCKED
		PS_Challenges[0].LockStatus = PSS_NEW
		//this will overwrite our save data (but there shouldnt be any if its locked after loading data)
		PS_PlayerData[0].ElapsedTime = -1
		iChallengeScores[0] = -1
	ENDIF
	
	IF iChallengeScores[PSC_planeCourse] <> -1
		bUnlockFlightSchoolReward = TRUE
	ENDIF
	
ENDPROC


PROC PS_MENU_SFX_PLAY_NAV_UP()
	PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
ENDPROC

PROC PS_MENU_SFX_PLAY_NAV_DOWN()
	PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
ENDPROC

PROC PS_MENU_SFX_PLAY_NAV_SELECT()
	PLAY_SOUND_FRONTEND(-1, "YES", "HUD_FRONTEND_DEFAULT_SOUNDSET")
ENDPROC

PROC PS_MENU_SFX_PLAY_NAV_BACK()
	PLAY_SOUND_FRONTEND(-1, "NO", "HUD_FRONTEND_DEFAULT_SOUNDSET")
ENDPROC


//Returns true as long as we're waiting on or receiving valid input, returns false when launching challenge or exiting
FUNC BOOL PS_MENU_GET_INPUT(BOOL &bIsLaunching)

	//if we were waiting for the trans to finish in order to quit, do it now
//	IF bIsWaitingToQuit
//		RETURN FALSE
//	ENDIF
	
//	IF bIsQuitTrans = TRUE
//		//skip input if we're transitioning to quit menu
//		RETURN TRUE
//	ENDIF

	BOOL bCursorAccept = FALSE

	
	IF bShowQuitMenu
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
		SET_WARNING_MESSAGE_WITH_HEADER("PS_QTITLE", "PS_QUIT", FE_WARNING_YES | FE_WARNING_NO)
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)  //IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
//			QUITUI_TRANSITION_OUT(quitUI, PS_QUIT_UI_TRANS_TIME)
//			bIsQuitTrans = TRUE
//			bIsWaitingToQuit = TRUE
			bIsLaunching = FALSE
			eGameMode = PS_GAME_MODE_QUIT_FROM_MENU
			DO_SCREEN_FADE_OUT(0)
			eGameSubState = PS_SUBSTATE_ENTER
//			bShowQuitInstructions = FALSE
			PS_MENU_SFX_PLAY_NAV_SELECT()
			IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
				ANIMPOSTFX_STOP("MinigameTransitionIn")
			ENDIF
			RETURN TRUE
		ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) //IS_BUTTON_JUST_PRESSED(PAD1, CIRCLE)
//			QUITUI_TRANSITION_OUT(quitUI, PS_QUIT_UI_TRANS_TIME)
//			bIsQuitTrans = TRUE
//			bShowQuitInstructions = FALSE
			bShowQuitMenu = FALSE
			PS_MENU_SFX_PLAY_NAV_BACK()						
		ENDIF
		RETURN TRUE
	ELIF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL))//IS_BUTTON_JUST_PRESSED(PAD1, TRIANGLE)
		HIDE_HUD_AND_RADAR_THIS_FRAME()
//		INIT_SIMPLE_USE_CONTEXT(quitInstructions, FALSE, FALSE, TRUE, TRUE)
//		ADD_SIMPLE_USE_CONTEXT_INPUT(quitInstructions, "FE_HLP32",	FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//		ADD_SIMPLE_USE_CONTEXT_INPUT(quitInstructions, "FE_HLP3",	FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//		
		DEBUG_MESSAGE("EXIT BUTTON PRESSED")
		//we're exiting the menu without launching
//		QUITUI_TRANSITION_IN(quitUI, PS_QUIT_UI_TRANS_TIME)
//		bShowQuitInstructions = TRUE
//		bIsQuitTrans = TRUE
		bShowQuitMenu = TRUE
		PS_MENU_SFX_PLAY_NAV_BACK()
		CLEAR_HELP()
		RETURN TRUE
	ENDIF

	#IF IS_DEBUG_BUILD
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			DEBUG_MESSAGE("LAUNCHING STUNT TUNER")
			bIsLaunching = TRUE
			bIsLaunchingStuntTuner = TRUE
			
			RETURN FALSE
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
			
			bDebugLogDataFile = !bDebugLogDataFile
//			STRING sFilePath = "X:/gta5/titleupdate/dev_ng/common/data/script/xml"
//			STRING sFileName = "local_PilotSchoolRecords.xml"
//			
//			OPEN_NAMED_DEBUG_FILE(sFilePath , sFileName)
//				SAVE_STRING_TO_DEBUG_FILE("<PilotSchoolRecords_xml>")SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("	<PilotSchool>")SAVE_NEWLINE_TO_DEBUG_FILE()
//				
//				INT iSave
//				REPEAT NUMBER_OF_PILOT_SCHOOL_CLASSES iSave
//					SAVE_PILOT_SCHOOL_STRUCT_TO_NAMED_DEBUG_FILE(PS_Challenges[iSave], PS_PlayerData[iSave], sFilePath, sFileName)
//				ENDREPEAT
//				
//				SAVE_STRING_TO_DEBUG_FILE("	</PilotSchool>")SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("</PilotSchoolRecords_xml>")SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_NEWLINE_TO_DEBUG_FILE()
//			CLOSE_DEBUG_FILE()
//			
//			TEXT_LABEL_63 str = "saved "
//			str += sFileName
//			PRINT_STRING_WITH_LITERAL_STRING("STRING", str, DEFAULT_GOD_TEXT_TIME, 1)
//			SAVE_STRING_TO_DEBUG_FILE("saved debug info in \"")SAVE_STRING_TO_DEBUG_FILE(sFilePath)SAVE_STRING_TO_DEBUG_FILE(sFileName)SAVE_STRING_TO_DEBUG_FILE("\"")SAVE_NEWLINE_TO_DEBUG_FILE()
			
		ENDIF
		
		INT iRepeat
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_W, KEYBOARD_MODIFIER_SHIFT, "reset scores") //IS_KEYBOARD_KEY_PRESSED(KEY_LSHIFT) OR IS_KEYBOARD_KEY_PRESSED(KEY_RSHIFT)
			REPEAT NUMBER_OF_PILOT_SCHOOL_CLASSES iRepeat
				IF iRepeat > 0
					PS_PlayerData[iRepeat].ElapsedTime = -1.0
					PS_PlayerData[iRepeat].LandingDistance = -1.0
					PS_PlayerData[iRepeat].CheckpointCount = -1
					PS_PlayerData[iRepeat].Multiplier = 1.0
					PS_PlayerData[iRepeat].HasPassedLesson = FALSE
					PS_Challenges[iRepeat].LockStatus = PSS_LOCKED
					iChallengeScores[iRepeat] = -1
				ENDIF
			ENDREPEAT
			//set stats
				STAT_SET_INT(SP0_FLYING_ABILITY, 0)
				STAT_SET_INT(SP1_FLYING_ABILITY, 0)
				STAT_SET_INT(SP2_FLYING_ABILITY, 70)
				PRINTLN("Set flying ability to 'defaults' (0,0,70) for all characters")
//			PS_Challenges[PSC_Inverted].LockStatus = PSS_NEW
			
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_W, KEYBOARD_MODIFIER_NONE, "unlock all lessons") OR IS_DEBUG_KEY_JUST_PRESSED(KEY_Z, KEYBOARD_MODIFIER_NONE, "unlock all lessons")
			//unlock all schools
			REPEAT NUMBER_OF_PILOT_SCHOOL_CLASSES iRepeat
				IF PS_Challenges[iRepeat].LockStatus = PSS_LOCKED
					PS_Challenges[iRepeat].LockStatus = PSS_NEW
				ENDIF
			ENDREPEAT
			//set stats
			STAT_SET_INT(SP0_FLYING_ABILITY, 100)
			STAT_SET_INT(SP1_FLYING_ABILITY, 100)
			STAT_SET_INT(SP2_FLYING_ABILITY, 100)
			PRINTLN("Set flying ability to 100 for all characters")
		ENDIF
	
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_P, KEYBOARD_MODIFIER_NONE, "unlock lesson")
			IF PS_Challenges[g_current_selected_PilotSchool_class].LockStatus = PSS_LOCKED
				IF ENUM_TO_INT(g_current_selected_PilotSchool_class) > 0 AND PS_Challenges[g_current_selected_PilotSchool_class - INT_TO_ENUM(PILOT_SCHOOL_CLASSES_ENUM, 1)].LockStatus = PSS_UNLOCKED
					PS_Challenges[g_current_selected_PilotSchool_class].LockStatus = PSS_NEW
					PS_PlayerData[g_current_selected_PilotSchool_class].ElapsedTime = -1.0
					iChallengeScores[g_current_selected_PilotSchool_class] = -1
				ENDIF
			ELIF PS_Challenges[g_current_selected_PilotSchool_class].LockStatus = PSS_UNLOCKED AND g_current_selected_PilotSchool_class > INT_TO_ENUM(PILOT_SCHOOL_CLASSES_ENUM, 0) AND PS_PlayerData[g_current_selected_PilotSchool_class].ElapsedTime > 0
				PS_Challenges[g_current_selected_PilotSchool_class].LockStatus = PSS_LOCKED
				PS_PlayerData[g_current_selected_PilotSchool_class].ElapsedTime = -1.0
				iChallengeScores[g_current_selected_PilotSchool_class] = -1
			ELSE
				PS_Challenges[g_current_selected_PilotSchool_class].LockStatus = PSS_UNLOCKED
				Set_Random_Values_For_PilotSchoolClass(PS_Challenges[g_current_selected_PilotSchool_class], PS_PlayerData[g_current_selected_PilotSchool_class])
				iChallengeScores[g_current_selected_PilotSchool_class] = Pilot_School_Data_Get_Score(PS_Challenges[g_current_selected_PilotSchool_class], PS_PlayerData[g_current_selected_PilotSchool_class], TRUE)
				PS_Challenges[g_current_selected_PilotSchool_class].HasBeenPreviewed = TRUE
			ENDIF
			INT k 
			REPEAT NUMBER_OF_PILOT_SCHOOL_CLASSES k
				g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[k] = PS_PlayerData[k]
			ENDREPEAT
			DEBUG_MESSAGE(PS_Challenges[g_current_selected_PilotSchool_class].Title, " SCORE IS ", GET_STRING_FROM_INT(Pilot_School_Data_Get_Score(PS_Challenges[g_current_selected_PilotSchool_class], PS_PlayerData[g_current_selected_PilotSchool_class], TRUE)))
		ENDIF
	#ENDIF
	
	
	// Cursor selection for PC
	IF NOT bShowQuitMenu
	AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
	
		INT iNumItems = 12
		
		// Get the highlighted menu item
		g_iMenuCursorItem = GET_CURSOR_HIGHLIGHTED_ITEM_IN_CUSTOM_MENU( 0.201, 0.222, 0.198, 0.0375, 0.034, iNumItems)
		HANDLE_MENU_CURSOR(FALSE, ENUM_TO_INT(g_current_selected_PilotSchool_class) )
		
		// Get the menu item index that the mouse is over.
		// -1 means the mouse is outside the menu area.
		
		// We know the mouse is inside the menu now...
		IF IS_MENU_CURSOR_ACCEPT_PRESSED()
							
			// Selecting again the currently selected item = buy
			IF INT_TO_ENUM( PILOT_SCHOOL_CLASSES_ENUM, g_iMenuCursorItem ) = g_current_selected_PilotSchool_class
				bCursorAccept = TRUE
			ELSE
				PS_MENU_SFX_PLAY_NAV_DOWN()
				g_current_selected_PilotSchool_class = INT_TO_ENUM(PILOT_SCHOOL_CLASSES_ENUM, g_iMenuCursorItem)
				INIT_PS_MENU_BUTTONS()
			ENDIF
					
		ENDIF
		
	ENDIF

	IF NOT bShowQuitMenu 
	AND (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR bCursorAccept )//IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
		DEBUG_MESSAGE("LAUNCH BUTTON PRESSED")
		//make sure we tell the menu to launch a challenge
		IF NOT (PS_Challenges[g_current_selected_PilotSchool_class].LockStatus = PSS_LOCKED)
			bIsLaunching = TRUE
			PS_MENU_SFX_PLAY_NAV_SELECT()			
			RETURN FALSE
		ELSE
			PS_MENU_SFX_PLAY_NAV_BACK()
		ENDIF
	ENDIF

	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
	OR IS_LEFT_ANALOG_AS_DPAD(LEFT_ANALOG_AS_DPAD_UP, PS_uiInput)
#IF IS_DEBUG_BUILD
	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
#ENDIF	//	IS_DEBUG_BUILD	
		INT iPrevSelectedClass
		iPrevSelectedClass = ENUM_TO_INT(g_current_selected_PilotSchool_class) - 1
		IF iPrevSelectedClass < 0
			iPrevSelectedClass = ENUM_TO_INT(NUMBER_OF_PILOT_SCHOOL_CLASSES) - 1
		ENDIF
		PS_MENU_SFX_PLAY_NAV_UP()
		g_current_selected_PilotSchool_class = INT_TO_ENUM(PILOT_SCHOOL_CLASSES_ENUM, iPrevSelectedClass)
		INIT_PS_MENU_BUTTONS()
	ENDIF
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)	 //IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN) OR IS_LEFT_ANALOG_AS_DPAD(PAD1, DPADDOWN, uiInput)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
	OR IS_LEFT_ANALOG_AS_DPAD(LEFT_ANALOG_AS_DPAD_DOWN, PS_uiInput)
#IF IS_DEBUG_BUILD
	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
#ENDIF	//	IS_DEBUG_BUILD
		INT iNextSelectedClass
		iNextSelectedClass = ENUM_TO_INT(g_current_selected_PilotSchool_class) + 1
		IF iNextSelectedClass >= ENUM_TO_INT(NUMBER_OF_PILOT_SCHOOL_CLASSES)
			iNextSelectedClass = 0
		ENDIF
		PS_MENU_SFX_PLAY_NAV_DOWN()
		g_current_selected_PilotSchool_class = INT_TO_ENUM(PILOT_SCHOOL_CLASSES_ENUM, iNextSelectedClass)
		INIT_PS_MENU_BUTTONS()
	ENDIF

	RETURN TRUE
	
ENDFUNC

PROC TEST_TEXTURES()
//	DRAW_RECT(0.25, 0.5, 0.5, 1, 255, 255, 255, 255)
//	DRAW_RECT(0.75, 0.5, 0.5, 1, 0, 0, 0, 255)

	SPRITE_PLACEMENT test
	test.r = 255
	test.g = 255
	test.b = 255
	test.a = 255
	
	// WHITE
	test.x = 0.125
	test.y = 0.2
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 2, 2)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.53
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 1.5, 1.5)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.76
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 1, 1)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.9
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 0.5, 0.5)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	
	// WHITE PRE-MULTI
	test.x = 0.375
	test.y = 0.2
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 2, 2)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.53
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 1.5, 1.5)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.76
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 1, 1)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.9
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 0.5, 0.5)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	
	// BLACK
	test.x = 0.625
	test.y = 0.2
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 2, 2)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.53
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 1.5, 1.5)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.76
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 1, 1)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.9
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 0.5, 0.5)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	
	// BLACK PRE-MULTI
	test.x = 0.875
	test.y = 0.2
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 2, 2)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.53
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 1.5, 1.5)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.76
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 1, 1)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	test.y = 0.9
	PIXEL_SCALE_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test, 0.5, 0.5)
	DRAW_2D_SPRITE("PS_Menu", "Test_FlightSchool_Silver", test)
	
	PIXEL_SCALE_SPRITE("pilotSchool", "FlightSchool_Bronze", test, 2, 2)
ENDPROC

