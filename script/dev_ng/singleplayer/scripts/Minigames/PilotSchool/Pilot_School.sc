	//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			6/24/11
//	Description:	Minigame shell logic for pilot school. Contains main loop to deal with menus, 
//					preview cut-scenes and launching/updating challenges.
//
//	ALL PILOT SCHOOL SCRIPT LOCATIONS:
//	X:\gta5\script\dev\singleplayer\include\private\Pilot_School
//	X:\gta5\script\dev\singleplayer\scripts\Minigames\PilotSchool
//****************************************************************************************************



//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.



CONST_INT						PS_FORMATION_DEBUG			0
#IF IS_DEBUG_BUILD
	CONST_INT						PS_PRINT_STATS				1
	CONST_INT						PS_DRAW_DEBUG_LINES_AND_SPHERES				0
#ENDIF
#IF NOT IS_DEBUG_BUILD
 	CONST_INT						PS_DRAW_DEBUG_LINES_AND_SPHERES				0 //for compile, functions won't work anyways
#ENDIF
 
#IF IS_DEBUG_BUILD
	USING "PS_Stunt_Tuner.sch"
#ENDIF
USING "Pilot_School_Menu.sch"

USING "vehicle_gen_private.sch"
USING "emergency_call.sch"
USING "rgeneral_include.sch"
USING "code_control_public.sch"
USING "minigames_helpers.sch"
USING "script_oddjob_funcs.sch"
USING "PS_Objective_lib.sch"
USING "Pilot_School_Challenge_Helpers.sch"

USING "PS_Takeoff.sch"
USING "PS_Landing.sch"
USING "PS_Inverted.sch"
USING "PS_Knifing.sch"
USING "PS_Looping.sch"
USING "PS_Fly_Low.sch"
USING "PS_Daring_Landing.sch"
USING "PS_Plane_Course.sch"
USING "PS_Heli_Course.sch"
USING "PS_Heli_Speed.sch"
USING "PS_Parachute.sch"
USING "PS_Moving_Chute.sch"

#IF IS_DEBUG_BUILD
	USING "pilot_school_debug_lib.sch"
#ENDIF

PROC PS_CLEANUP()
	CLEAR_HELP()
	CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT, TRUE)
	
	CLEAR_RANK_REDICTION_DETAILS() 
	
	SC_LEADERBOARD_CACHE_CLEAR_ALL()

	DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, TRUE)
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	
	TRIGGER_SCREENBLUR_FADE_OUT(0)
	
	CLEAR_TIMECYCLE_MODIFIER()
	
	STOP_AUDIO_SCENES()
	
	SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(FALSE)
	
	BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_PISCO)
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	REQUEST_IPL("MG-Flight School 5")
	
	CANCEL_MUSIC_EVENT("MGPS_START")
	CANCEL_MUSIC_EVENT("MGPS_STOP")
	CANCEL_MUSIC_EVENT("MGPS_FAIL")
	
	//make sure the camera views for the helicopter and stunt plane are set to the views before flight school had launched.
	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI, cvmHelicopter)
	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT, cvmStuntPlane)
	
	IF DOES_ENTITY_EXIST(PS_Main.playerVehicle) AND IS_VEHICLE_DRIVEABLE(PS_Main.playerVehicle)
		SET_PLAYERS_LAST_VEHICLE(PS_Main.playerVehicle)
		FREEZE_ENTITY_POSITION(PS_Main.playerVehicle, FALSE)
		ACTIVATE_PHYSICS(PS_Main.playerVehicle)
	ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_MAX_WANTED_LEVEL(5)		
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	ENABLE_ALL_DISPATCH_SERVICES(TRUE)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_GOLF_COURSE)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_BASE)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_PRISON)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_BIOTECH)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO)
		
	SET_MINIGAME_SPLASH_SHOWING(FALSE)
	
	SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
	
	STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
	//make sure the parachute pack is turned off
	IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
	ENDIF
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
		ENDIF
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
		RESTORE_PLAYER_PED_VARIATIONS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		SET_PED_VARIATIONS(PLAYER_PED_ID(), structPlayerProps)
				
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF
	
	RELEASE_PILOT_SCHOOL_ASSETS()
	#IF IS_DEBUG_BUILD
		#IF PS_PRINT_STATS
			PS_CLEANUP_DATA_FILE_WIDGET()
		#ENDIF
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	#ENDIF
	IF DOES_CAM_EXIST(camMainMenu)
		DESTROY_CAM(camMainMenu)
	ENDIF
	
	RESET_SCENARIO_TYPES_ENABLED()
	SET_GAME_PAUSED(FALSE)
	
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
	ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENABLE_SELECTOR()
	
	PRINTLN("enabling selector!!")
	DISABLE_CELLPHONE(FALSE)
	KILL_FACE_TO_FACE_CONVERSATION()
		
	Set_Leave_Area_Flag_For_All_Blipped_Missions()	
	
	
	#IF IS_DEBUG_BUILD
	IF NOT bPlayerUsedDebugSkip	
	#ENDIF
		// if player gets to cleanup and the writer is still ctive, do a normal write
		// this will be rare since the predictor kicks off well before the the menu appears, but should be a failsafe against really slow connections
		IF IS_PLAYER_ONLINE() AND PS_IS_LEADERBOARD_WRITER_ACTIVE()
			IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Challenges[eLBLessonToWrite], FSG_0_timeTaken)
				PS_WRITE_DATA_TO_LEADERBOARD(eLBLessonToWrite, Pilot_School_Data_Get_Score(PS_Main.myChallengeData, PS_Main.myPlayerData), FLOOR(PS_Main.myPlayerData.elapsedTime * 1000.0))
			ELSE
				PS_WRITE_DATA_TO_LEADERBOARD(eLBLessonToWrite, Pilot_School_Data_Get_Score(PS_Main.myChallengeData, PS_Main.myPlayerData), DEFAULT, PS_Main.myPlayerData.LandingDistance)
			ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	
	
	DEBUG_MESSAGE("******************** END SCRIPT: Pilot_School.sc ********************")
ENDPROC

PROC PS_INIT()
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_REPEAT_PLAY|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP)
		PRINTSTRING("PILOT SCHOOL FORCE CLEANUP!!")PRINTNL()
   		PS_CLEANUP()
	ENDIF
	
	DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	REMOVE_IPL("MG-Flight School 5")
	
	//store the player's camera views of the heli and plane immediately, so they can be restored when flight school terminates.
	cvmHelicopter = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI)
	cvmStuntPlane = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT)
		
	SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
	
	MINIGAME_DISPLAY_MISSION_TITLE(ENUM_TO_INT(MINIGAME_PILOT_SCHOOL))
	SET_PLAYERPAD_SHAKES_WHEN_CONTROLLER_DISABLED(FALSE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
//	DISPLAY_HUD(FALSE)
	DISABLE_SELECTOR()

	ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
	CLEAR_HELP()
	IF NOT GET_MISSION_FLAG()
		SET_MISSION_FLAG(TRUE)
	ENDIF
	
	SET_MAX_WANTED_LEVEL(0)
	SET_WANTED_LEVEL_MULTIPLIER(0)
	SUPPRESS_EMERGENCY_CALLS()
	ENABLE_ALL_DISPATCH_SERVICES(FALSE)
	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE)
	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_GOLF_COURSE)
	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_BASE)
	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_PRISON)
	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_BIOTECH)
	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS)
	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO)
	
	PS_Main.playerVehicle = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(PS_Main.playerVehicle) AND IS_VEHICLE_DRIVEABLE(PS_Main.playerVehicle)
		SET_ENTITY_AS_MISSION_ENTITY(PS_Main.playerVehicle, TRUE, TRUE)
		GET_VEHICLE_TRAILER_VEHICLE(PS_Main.playerVehicle, PS_Main.playerTrailer)
		IF DOES_ENTITY_EXIST(PS_Main.playerTrailer)
			SET_ENTITY_AS_MISSION_ENTITY(PS_Main.playerTrailer, TRUE, TRUE)
		ENDIF
		FREEZE_ENTITY_POSITION(PS_Main.playerVehicle, TRUE)
	ENDIF
	
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
	ENDIF
	STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())	
	GET_PED_VARIATIONS(PLAYER_PED_ID(), structPlayerProps)
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		PRINTLN("Giving FRANKLIN a headset")
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HEAD, 008, 0)//franklin
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		PRINTLN("Giving TREVOR a headset")
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HEAD, 012, 0)//trevor
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		PRINTLN("Giving MICHAEL a headset")
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HEAD, 021, 0)//michael
	ENDIF	
	DISABLE_CELLPHONE(TRUE)
	KILL_ANY_CONVERSATION()
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	#ENDIF
	DEBUG_MESSAGE("******************** INIT SCRIPT: Pilot_School.sc ********************")
ENDPROC

PROC PS_INCREMENT_MAIN_SUBSTATE()
	SWITCH eGameSubState
		CASE PS_SUBSTATE_ENTER
			eGameSubState = PS_SUBSTATE_UPDATE
			BREAK
		CASE PS_SUBSTATE_UPDATE
			eGameSubState = PS_SUBSTATE_EXIT
			BREAK
		CASE PS_SUBSTATE_EXIT
			eGameSubState = PS_SUBSTATE_ENTER
			BREAK
	ENDSWITCH
ENDPROC

PROC PS_SETUP_FUNCTION_POINTERS()
	#IF IS_DEBUG_BUILD
		IF bIsLaunchingStuntTuner
			PSC_InitProc = &PS_Tuner_Initialise
			PSC_PreviewFunc = &PS_Tuner_Preview
			PSC_SetupProc = &PS_Tuner_MainSetup
			PSC_UpdateFunc = &PS_Tuner_MainUpdate
			PSC_CleanupProc = &PS_Tuner_MainCleanup
			PSC_DBG_FORCE_FAIL = &PS_TUNER_FORCE_FAIL
			PSC_DBG_FORCE_PASS = &PS_TUNER_FORCE_PASS
			EXIT
		ENDIF
	#ENDIF
	
	SWITCH(g_current_selected_PilotSchool_class)
		CASE PSC_Takeoff
			PSC_InitProc = &PS_Takeoff_Initialise
			PSC_PreviewFunc = &PS_Takeoff_Preview
			PSC_SetupProc = &PS_Takeoff_MainSetup
			PSC_UpdateFunc = &PS_Takeoff_MainUpdate
			PSC_CleanupProc = &PS_Takeoff_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_TAKEOFF_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_TAKEOFF_FORCE_PASS
			#ENDIF
			BREAK
		CASE PSC_Landing
			PSC_InitProc = &PS_Landing_Initialise
			PSC_PreviewFunc = &PS_Landing_Preview
			PSC_SetupProc = &PS_Landing_MainSetup
			PSC_UpdateFunc = &PS_Landing_MainUpdate
			PSC_CleanupProc = &PS_Landing_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_LANDING_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_LANDING_FORCE_PASS
			#ENDIF
			BREAK
		CASE PSC_Inverted
			PSC_InitProc = &PS_Inverted_Initialise
			PSC_PreviewFunc = &PS_Inverted_Preview
			PSC_SetupProc = &PS_Inverted_MainSetup
			PSC_UpdateFunc = &PS_Inverted_MainUpdate
			PSC_CleanupProc = &PS_Inverted_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_INVERTED_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_INVERTED_FORCE_PASS
			#ENDIF
			BREAK
		CASE PSC_Knifing
			PSC_InitProc = &PS_Knifing_Initialise
			PSC_PreviewFunc = &PS_Knifing_Preview
			PSC_SetupProc = &PS_Knifing_MainSetup
			PSC_UpdateFunc = &PS_Knifing_MainUpdate
			PSC_CleanupProc = &PS_Knifing_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_KNIFING_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_KNIFING_FORCE_PASS
			#ENDIF
			BREAK
		CASE PSC_loopTheLoop
			PSC_InitProc = &PS_Looping_Initialise
			PSC_PreviewFunc = &PS_Looping_Preview
			PSC_SetupProc = &PS_Looping_MainSetup
			PSC_UpdateFunc = &PS_Looping_MainUpdate
			PSC_CleanupProc = &PS_Looping_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_LOOPING_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_LOOPING_FORCE_PASS
			#ENDIF
			BREAK
		CASE PSC_FlyLow
			PSC_InitProc = &PS_Fly_Low_Initialise
			PSC_PreviewFunc = &PS_Fly_Low_Preview
			PSC_SetupProc = &PS_Fly_Low_MainSetup
			PSC_UpdateFunc = &PS_Fly_Low_MainUpdate
			PSC_CleanupProc = &PS_Fly_Low_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_FLY_LOW_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_FLY_LOW_FORCE_PASS
			#ENDIF
			BREAK
		CASE PSC_DaringLanding
			PSC_InitProc = &PS_Daring_Initialise
			PSC_PreviewFunc = &PS_Daring_Preview
			PSC_SetupProc = &PS_Daring_MainSetup
			PSC_UpdateFunc = &PS_Daring_MainUpdate
			PSC_CleanupProc = &PS_Daring_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_DARING_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_DARING_FORCE_PASS
			#ENDIF
			BREAK
		CASE PSC_planeCourse
			PSC_InitProc = &PS_Plane_Course_Initialise
			PSC_PreviewFunc = &PS_Plane_Course_Preview
			PSC_SetupProc = &PS_Plane_Course_MainSetup
			PSC_UpdateFunc = &PS_Plane_Course_MainUpdate
			PSC_CleanupProc = &PS_Plane_Course_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_PLANE_COURSE_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_PLANE_COURSE_FORCE_PASS
			#ENDIF
			BREAK
		CASE PSC_heliCourse
			PSC_InitProc = &PS_Heli_Course_Initialise
			PSC_PreviewFunc = &PS_Heli_Course_Preview
			PSC_SetupProc = &PS_Heli_Course_MainSetup
			PSC_UpdateFunc = &PS_Heli_Course_MainUpdate
			PSC_CleanupProc = &PS_Heli_Course_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_HELI_COURSE_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_HELI_COURSE_FORCE_PASS
			#ENDIF
			BREAK
		CASE PSC_heliSpeedRun
			PSC_InitProc = &PS_Heli_Speed_Initialise
			PSC_PreviewFunc = &PS_Heli_Speed_Preview
			PSC_SetupProc = &PS_Heli_Speed_MainSetup
			PSC_UpdateFunc = &PS_Heli_Speed_MainUpdate
			PSC_CleanupProc = &PS_Heli_Speed_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_HELI_SPEED_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_HELI_SPEED_FORCE_PASS
			#ENDIF
			BREAK
		CASE PSC_parachuteOntoTarget
			PSC_InitProc = &PS_Parachute_Initialise
			PSC_PreviewFunc = &PS_Parachute_Preview
			PSC_SetupProc = &PS_Parachute_MainSetup
			PSC_UpdateFunc = &PS_Parachute_MainUpdate
			PSC_CleanupProc = &PS_Parachute_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_PARACHUTE_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_PARACHUTE_FORCE_PASS
			#ENDIF
			BREAK
		CASE PSC_chuteOntoMovingTarg
			PSC_InitProc = &PS_Moving_Chute_Initialise
			PSC_PreviewFunc = &PS_Moving_Chute_Preview
			PSC_SetupProc = &PS_Moving_Chute_MainSetup
			PSC_UpdateFunc = &PS_Moving_Chute_MainUpdate
			PSC_CleanupProc = &PS_Moving_Chute_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_MOVING_CHUTE_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_MOVING_CHUTE_FORCE_PASS
			#ENDIF
			BREAK
		DEFAULT
			SCRIPT_ASSERT("selected challenge does not exist for some reason")
			BREAK
	ENDSWITCH
ENDPROC

SCRIPT (PS_LAUNCH_ARGS psArgs)
	vPilotSchoolSceneCoords = vPilotSchoolSceneCoords
	
	PS_INIT()
	
	//VECTOR vGameCamPos
	
	#IF IS_DEBUG_BUILD
		#IF PS_PRINT_STATS
			PS_INIT_DATA_FILE_WIDGET()
		#ENDIF
	#ENDIF
	
	//main loop	
	WHILE bGameIsRunning
		
		#IF IS_DEBUG_BUILD
			#IF PS_PRINT_STATS
				IF eGameMode = PS_GAME_MODE_MENU
					PS_UPDATE_DATA_FILE_WIDGET()
				ENDIF
			#ENDIF
		#ENDIF
		
		//global HUD stuff... running during menu, preview and challenges
		PS_HUD_UPDATE_FADE()
		
		//Hide hud stuff
		BOOL bHideTheRadar
		HIDE_STREET_AND_CAR_NAMES_THIS_FRAME()
		
		IF eGameMode = PS_GAME_MODE_CHALLENGE //AND eGameSubState = PS_SUBSTATE_UPDATE
			bHideTheRadar = FALSE
		ELSE
			bHideTheRadar = TRUE //hide the radar when not in a challenge
		ENDIF
		
		PS_HUD_HIDE_GAMEHUD_ELEMENTS(bHideTheRadar)
		
		PS_UPDATE_LEADERBOARD_WRITER()
		
		//states: a.load menu b.menu c.course init d.load preview e. preview f.load challenge g.challenge h.quit to menu
		SWITCH(eGameMode)
			CASE PS_GAME_MODE_INTRO_SCENE
				SWITCH(eGameSubState)
					CASE PS_SUBSTATE_ENTER
//						IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_OUT, 1000)
//							eGameMode = PS_GAME_MODE_LOAD_MENU //bypass intro (leaving this state here in case we add it back)
//						ENDIF 
						DEBUG_MESSAGE("############################################################# starting intro scene")
						//cut scene setup stuff
						PS_INIT_INTRO_SCENE(psArgs)
						PS_INCREMENT_MAIN_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//do cut scene here
						IF NOT PS_UPDATE_INTRO_SCENE(psArgs)
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						eGameMode = PS_GAME_MODE_LOAD_MENU
						PS_INCREMENT_MAIN_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			CASE PS_GAME_MODE_LOAD_MENU
				IF IS_PC_VERSION()
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				ENDIF
				SWITCH(eGameSubState)
					CASE PS_SUBSTATE_ENTER
						DEBUG_MESSAGE("############################################################# loading menu")
//						IF bDontFadeToMenu OR PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_OUT, 1000)
							PS_CLEANUP_PLAYER_AND_VEHICLES()
							PS_MENU_SETUP()
							PS_INCREMENT_MAIN_SUBSTATE()
//						ENDIF
						
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//wait while shit is loading
						DEBUG_MESSAGE("waiting for shit to load...")
						PS_INCREMENT_MAIN_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						eGameMode = PS_GAME_MODE_MENU
						PS_INCREMENT_MAIN_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_GAME_MODE_MENU
				//draw menu
				IF IS_PC_VERSION()
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				ENDIF
				SWITCH(eGameSubState)
					CASE PS_SUBSTATE_ENTER
						IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_IN, 1000)
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF 
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_MENU_UPDATE() //change this to just grab the input
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_OUT, 1000)
							DEBUG_MESSAGE("############################################################# running menu cleanup")
							PS_MENU_CLEANUP()
							eGameMode = PS_GAME_MODE_COURSE_INIT
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF 
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_GAME_MODE_COURSE_INIT
				SWITCH(eGameSubState)
					CASE PS_SUBSTATE_ENTER
						IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_OUT, 1000)
							PS_CLEANUP_PLAYER_AND_VEHICLES()
							DEBUG_MESSAGE("############################################################# running course init")
							TRIGGER_SCREENBLUR_FADE_OUT(0)
							PS_SETUP_FUNCTION_POINTERS()
							PS_CLEANUP_PLAYER()
							PS_INIT_CHALLENGE_VARIABLES()
							PS_SETUP_DISPATCHER()
							
							//setup for escaping death
							SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(TRUE)
							
							CALL PSC_InitProc()
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_MAIN_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
							ANIMPOSTFX_STOP("MinigameTransitionIn")
						ENDIF
						IF PS_Main.myChallengeData.HasBeenPreviewed  OR bRetryChallenge
							//we want to skip preview on retry or if the lesson has been passed
							DEBUG_MESSAGE("############################################################# skipping preview")
							IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
								FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
							ENDIF
							eGameMode = PS_GAME_MODE_PREVIEW
						ELSE
							eGameMode = PS_GAME_MODE_LOAD_PREVIEW
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF
						BREAK
				ENDSWITCH	
				BREAK
			
			CASE PS_GAME_MODE_LOAD_PREVIEW
				SWITCH(eGameSubState)
					CASE PS_SUBSTATE_ENTER
						//we're already faded out here so just start loading
						DEBUG_MESSAGE("############################################################# loading preview")
						//LOAD_SCENE(vPilotSchoolPreviewCoords)
						IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
							SET_ENTITY_HEALTH(PS_Main.myVehicle, 1500)
							
							// Ensure the vehicle is at the start of the preview to improve the scene loading.
							SET_ENTITY_COORDS(PS_Main.myVehicle, vPilotSchoolPreviewCoords)
							FREEZE_ENTITY_POSITION(PS_Main.myVehicle, TRUE)
						ENDIF
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						
						SET_OVERRIDE_WEATHER("EXTRASUNNY")
						iSavedTimeOfDay = GET_CLOCK_HOURS()
						SET_CLOCK_TIME(12, GET_CLOCK_MINUTES(), GET_CLOCK_SECONDS())
						sfTelevisionBorder = REQUEST_SCALEFORM_MOVIE("TV_FRAME")
						START_TIMER_NOW_SAFE(streamingTimer)
						
						//this is what the script is currently using.
						//streamVolume = STREAMVOL_CREATE_FRUSTUM(vPilotSchoolPreviewCoords, PS_CONVERT_ROTATION_TO_DIRECTION(vPilotSchoolPreviewRot), 800, FLAG_MAPDATA | FLAG_COLLISIONS_MOVER)
						
						//attempted to use spheres.  They seem to work, but can't be resized to a large degree without killing the game.
						//streamVolume = STREAMVOL_CREATE_SPHERE(vPilotSchoolPreviewCoords, 400, FLAG_MAPDATA | FLAG_COLLISIONS_MOVER)
						//vPilotSchoolPreviewRot = vPilotSchoolPreviewRot
						
						//vGameCamPos = GET_GAMEPLAY_CAM_COORD()
						
						//testing new load scene.
						//IF NEW_LOAD_SCENE_START(vGameCamPos, PS_CONVERT_ROTATION_TO_DIRECTION(vPilotSchoolPreviewRot), 800.0)
						IF NEW_LOAD_SCENE_START_SPHERE(vPilotSchoolPreviewCoords, 1200.0)
							vPilotSchoolPreviewRot = vPilotSchoolPreviewRot
							//Set multihead blinders on early
							SET_MULTIHEAD_SAFE(TRUE,TRUE)
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF
						
						//IF STREAMVOL_IS_VALID(streamVolume)
							//PS_INCREMENT_MAIN_SUBSTATE()
						//ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF HAS_SCALEFORM_MOVIE_LOADED(sfTelevisionBorder)
							
							//draw early to account for delay
							DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfTelevisionBorder, 255, 255, 255, 255)
							
							//IF (STREAMVOL_HAS_LOADED(streamVolume) AND GET_TIMER_IN_SECONDS(streamingTimer) > 3.0) OR TIMER_DO_ONCE_WHEN_READY(streamingTimer, 10.0)
							//	PS_INCREMENT_MAIN_SUBSTATE()
							//ENDIF
							
							//ensure the new scene is active and loaded.  
							IF IS_NEW_LOAD_SCENE_ACTIVE() OR TIMER_DO_WHEN_READY(streamingTimer, 12.0)
								IF IS_NEW_LOAD_SCENE_LOADED() OR TIMER_DO_WHEN_READY(streamingTimer, 12.0)
									CANCEL_TIMER(streamingTimer)
									NEW_LOAD_SCENE_STOP()
									PS_INCREMENT_MAIN_SUBSTATE()
								ENDIF
							ENDIF
								
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						//draw early to account for delay
						IF HAS_SCALEFORM_MOVIE_LOADED(sfTelevisionBorder)
							DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfTelevisionBorder, 255, 255, 255, 255)
						ENDIF
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
						ELSE
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						ENDIF
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							// Unfreeze the vehicle before the recording begins.
							SET_ENTITY_INVINCIBLE(PS_Main.myVehicle, TRUE)
							FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
						ENDIF
						
						
						eGameMode = PS_GAME_MODE_PREVIEW
						PS_INCREMENT_MAIN_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_GAME_MODE_PREVIEW
				//playe preview cutscene
				CALL PSC_PreviewFunc()
				DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
				SWITCH(eGameSubState)
					CASE PS_SUBSTATE_ENTER
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
						ENDIF
						IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_IN, 1000)
							
//							IF IS_NEW_LOAD_SCENE_ACTIVE()
//								NEW_LOAD_SCENE_STOP()
//							ENDIF
							DEBUG_MESSAGE("############################################################# running preview start")
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF 
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_PREVIEW_CHECK_FOR_SKIP()
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_OUT, 1000)
							CLEAR_OVERRIDE_WEATHER()
							SET_CLOCK_TIME(iSavedTimeOfDay, GET_CLOCK_MINUTES(), GET_CLOCK_SECONDS())
							IF IS_STREAMVOL_ACTIVE()
								STREAMVOL_DELETE(streamVolume)
							ENDIF
							KILL_ANY_CONVERSATION()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()//sometimes can hear dialogue playing after the skipped preview, so kill it and don't finish last line
							
							DEBUG_MESSAGE("############################################################# cleaning up preview")
							PS_PREVIEW_CLEANUP_CUTSCENE()
							//resetting var here so we can skip preview. 
							bRetryChallenge = FALSE
							//Set multihead blinders off
							SET_MULTIHEAD_SAFE(FALSE)
							
							eGameMode = PS_GAME_MODE_LOAD_CHALLENGE
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF 
						BREAK
				ENDSWITCH			
				BREAK

			CASE PS_GAME_MODE_LOAD_CHALLENGE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
				SWITCH(eGameSubState)
					CASE PS_SUBSTATE_ENTER
						//we're already faded out here
						DEBUG_MESSAGE("############################################################# loading challenge")
						CALL PSC_SetupProc()
						
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							CLEAR_PED_WETNESS(PLAYER_PED_ID())
							CLEAR_PED_ENV_DIRT(PLAYER_PED_ID())
							CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
							RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
						ENDIF
						
						//if the helicopter view is set to overhead when retrying, ensure it's recent to behind the helicopter.
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_THIRD_PERSON_FAR
							SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
						ENDIF
						
						START_TIMER_NOW_SAFE(streamingTimer)
						PS_INCREMENT_MAIN_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF (IS_NEW_LOAD_SCENE_ACTIVE() AND IS_NEW_LOAD_SCENE_LOADED()) OR TIMER_DO_ONCE_WHEN_READY(streamingTimer, 10.0)
							NEW_LOAD_SCENE_STOP()
							IF GET_ENTITY_MODEL(PS_Main.myVehicle) <> MAVERICK
								FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
							ENDIF
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							SET_ENTITY_INVINCIBLE(PS_Main.myVehicle, FALSE)
						ENDIF
						eGameMode = PS_GAME_MODE_CHALLENGE
						PS_INCREMENT_MAIN_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_GAME_MODE_CHALLENGE
				SWITCH(eGameSubState)
					CASE PS_SUBSTATE_ENTER
						PS_HUD_START_FADE(PS_SCREEN_FADE_IN, 1000)
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							NEW_LOAD_SCENE_STOP()
						ENDIF
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
						DEBUG_MESSAGE("############################################################# starting challenge")
						PS_INCREMENT_MAIN_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT CALL PSC_UpdateFunc()
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						DEBUG_MESSAGE("############################################################# running game cleanup")
						#IF IS_DEBUG_BUILD
							#IF PS_PRINT_STATS
								PS_PRINT_LESSON_RECORD()
							#ENDIF
						#ENDIF
						CALL PSC_CleanupProc()
						IF bEndScreenQuitAlert
							eGameMode = PS_GAME_MODE_QUIT_FROM_MENU
						ELIF bRetryChallenge	
							eGameMode = PS_GAME_MODE_COURSE_INIT
						ELSE
							IF IS_PLAYER_PLAYING(PLAYER_ID())
								eGameMode = PS_GAME_MODE_LOAD_MENU
							ELSE
								eGameMode = PS_GAME_MODE_LOAD_MENU
							ENDIF
						ENDIF
						PS_INCREMENT_MAIN_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

			CASE PS_GAME_MODE_QUIT_FROM_MENU
				SWITCH(eGameSubState)
					CASE PS_SUBSTATE_ENTER
						IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_OUT, 1000)
							PS_CLEANUP_PLAYER_AND_VEHICLES()
							DEBUG_MESSAGE("############################################################# quitting from menu")
							#IF IS_DEBUG_BUILD
								#IF PS_PRINT_STATS
									PS_PRINT_FLIGHT_SCHOOL_STATS()
									PS_END_FLIGHT_SCHOOL_DATA_FILE()
								#ENDIF
							#ENDIF
							
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								CLEAR_PED_WETNESS(PLAYER_PED_ID())
								CLEAR_PED_ENV_DIRT(PLAYER_PED_ID())
								CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
								RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
							ENDIF
							
							SET_ENTITY_COORDS(PLAYER_PED_ID(), PS_MENU_PLAYER_COORD)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), PS_MENU_PLAYER_HEADING)
							NEW_LOAD_SCENE_START_SPHERE(PS_MENU_PLAYER_COORD, 50)
							START_TIMER_NOW_SAFE(streamingTimer)
							WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND GET_TIMER_IN_SECONDS_SAFE(streamingTimer) < 10000
								WAIT(0)
							ENDWHILE
							NEW_LOAD_SCENE_STOP()							
							PS_CLEANUP()
							PS_INCREMENT_MAIN_SUBSTATE()
						ENDIF 
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//wait while shit is loading
						DEBUG_MESSAGE("waiting for shit to load...")
						PS_INCREMENT_MAIN_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_IN, 1000)
							bGameIsRunning = FALSE
						ENDIF 
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		WAIT(0)
	ENDWHILE
	
	//If the vehicle generator flag hasnt been set yet and the player has unlocked rewards.
	PRINTLN("Checking to see if we're ready to unlock the duster.")
	bUnlockFlightSchoolReward = g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[PSC_planeCourse].eMedal <> PS_NONE 
	
	IF bUnlockFlightSchoolReward AND NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_DUSTER, VEHGEN_S_FLAG_AVAILABLE)
		PRINTLN("setting VEHGEN_DUSTER available")
		
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_DUSTER, TRUE)
	ELSE
		PRINTLN("Didn't unlock.")
		PRINTLN("bUnlockFlightSchoolReward is (1 for true) ", bUnlockFlightSchoolReward)
		PRINTLN("GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_DUSTER, VEHGEN_S_FLAG_AVAILABLE) is ", bUnlockFlightSchoolReward)
	ENDIF
	
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	ENABLE_SELECTOR()
ENDSCRIPT
