//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/11/11
//	Description:	"Knife Flight" Challenge for Pilot School. The player is taught to fly with their wings
//					oriented in a verticle position and must perform the stunt a couple times to pass the challenge.
//	
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

//CONSTS
CONST_INT			MAX_KNIFING_CHECKPOINTS 					1

INT iKnifingInstructionCounter =1
//See which side the player is knifing to make sure he does it on both sides.
//BOOL bIsKnifingLeft 
//PS_OBJECTIVE_TYPE_ENUM eKnifeSide
//structTimer tKnifingLevelOutTimer

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Inverted Flight
//****************************************************************************************************
//****************************************************************************************************

PROC  PS_Challenge_Knifing_Init_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1286.3641, -2124.3770, 62>>)
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Challenge_Knifing_Fail_Check()
	SWITCH(PS_Knifing_State)
		CASE PS_KNIFING_INIT
			//No fail check to do
			RETURN FALSE
			
		CASE PS_KNIFING_COUNTDOWN
			//No fail check to do
			RETURN FALSE
			
		CASE PS_KNIFING_TAKEOFF
			//Do runway check and default checks
			IF PS_UPDATE_FAIL_CHECKS(PS_Main, TRUE, FALSE)
				RETURN TRUE
			ENDIF
			BREAK
		
		DEFAULT
			//Do default checks
			IF PS_UPDATE_FAIL_CHECKS(PS_Main)
				RETURN TRUE
			ENDIF
			BREAK
	ENDSWITCH
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    When it returns TRUE, the main loop will increment the challenge state/stage
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Challenge_Knifing_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_Knifing_State)
			CASE PS_KNIFING_INIT
				RETURN FALSE
				BREAK

			CASE PS_KNIFING_COUNTDOWN
				RETURN FALSE
				BREAK
				
			CASE PS_KNIFING_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1 OR PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_KNIFING_STAGE_ONE_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_KNIFING_STAGE_ONE_START
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//Make the player do a knife 
						IF PS_IS_PLANE_KNIFING_LEFT()
							PS_HUD_SET_OBJECTIVE_TYPE(PS_OBJECTIVE_TYPE_KNIFE_L)
							eCurrentKnifingObjective = PS_OBJECTIVE_TYPE_KNIFE_L
							PRINTLN("PS_KNIFING_STAGE_ONE_START - SETTING OBJECTIVE METER TO KNIFE LEFT!!!")
							RETURN TRUE
						ELIF PS_IS_PLANE_KNIFING_RIGHT()
							PS_HUD_SET_OBJECTIVE_TYPE(PS_OBJECTIVE_TYPE_KNIFE_R)
							eCurrentKnifingObjective = PS_OBJECTIVE_TYPE_KNIFE_R
							PRINTLN("PS_KNIFING_STAGE_ONE_START - SETTING OBJECTIVE METER TO KNIFE RIGHT!!!")
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_KNIFING_STAGE_ONE_HOLD
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//Are we checking for the knife left or right?
						IF PS_IS_KNIFE_METER_FULL(5.0)
							PRINTLN("PS_KNIFING_STAGE_ONE_HOLD - KNIFE METER REACHED 5 SECONDS!")
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_KNIFING_STAGE_ONE_LEVEL_OUT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//Make sure the player levels out for a second
						IF PS_IS_LEVEL_OUT_COMPLETE(PS_DEFAULT_LEVEL_OUT_HOLD_TIME)
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_KNIFING_STAGE_TWO_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
				BREAK	
				
			CASE PS_KNIFING_STAGE_TWO_START
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//Are we checking for the knife left or right?
						IF Pilot_School_HUD_Get_Obj_Meter_Type() = PS_OBJECTIVE_TYPE_KNIFE_L
							PRINTLN("PS_KNIFING_STAGE_TWO_START - KNIFING LEFT! PROGRESSING TO PS_KNIFING_STAGE_TWO_HOLD")
							//See if the player is knifing left
							IF PS_IS_KNIFE_METER_FULL(0.5)
								RETURN TRUE
							ENDIF
						ELIF Pilot_School_HUD_Get_Obj_Meter_Type() = PS_OBJECTIVE_TYPE_KNIFE_R
							PRINTLN("PS_KNIFING_STAGE_TWO_START - KNIFING RIGHT! PROGRESSING TO PS_KNIFING_STAGE_TWO_HOLD")
							//See if the player is knifing right
							IF PS_IS_KNIFE_METER_FULL(0.5)
								RETURN TRUE
							ENDIF
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
					
			CASE PS_KNIFING_STAGE_TWO_HOLD
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//Are we checking for the knife left or right?
						IF Pilot_School_HUD_Get_Obj_Meter_Type() = PS_OBJECTIVE_TYPE_KNIFE_L
							PRINTLN("PS_KNIFING_STAGE_TWO_HOLD - KNIFING LEFT!")
							//See if the player is knifing left
							IF PS_IS_KNIFE_METER_FULL(5.0)
								PRINTLN("PS_KNIFING_STAGE_ONE_HOLD - KNIFE METER REACHED 5 SECONDS!")
								RETURN TRUE
							ENDIF
						ELIF Pilot_School_HUD_Get_Obj_Meter_Type() = PS_OBJECTIVE_TYPE_KNIFE_R
							PRINTLN("PS_KNIFING_STAGE_TWO_HOLD - KNIFING RIGHT!")
							//See if the player is knifing right
							IF PS_IS_KNIFE_METER_FULL(5.0)
								PRINTLN("PS_KNIFING_STAGE_ONE_HOLD - KNIFE METER REACHED 5 SECONDS!")
								RETURN TRUE
							ENDIF
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_KNIFING_STAGE_TWO_LEVEL_OUT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//Make sure the player levels out for a second
						PRINTLN("checking for level out!")
						IF PS_IS_LEVEL_OUT_COMPLETE(PS_DEFAULT_LEVEL_OUT_HOLD_TIME)
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE		
ENDFUNC

PROC PS_KNIFING_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_Knifing_State = PS_KNIFING_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_KNIFING_FORCE_PASS
	PS_Knifing_State = PS_KNIFING_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_KNIFING_J_SKIP()
		PS_KNIFING_FLIGHT_CHALLENGE nextState = PS_Knifing_State
		IF PS_Knifing_State > PS_KNIFING_TAKEOFF
			IF PS_Knifing_State < PS_KNIFING_STAGE_ONE_PREP
				nextState = PS_KNIFING_STAGE_ONE_PREP			
			ELIF PS_Knifing_State < PS_KNIFING_STAGE_TWO_PREP
				nextState = PS_KNIFING_STAGE_TWO_PREP
			ELIF PS_Knifing_State < PS_KNIFING_STAGE_TWO_LEVEL_OUT
				nextState = PS_KNIFING_STAGE_TWO_LEVEL_OUT
			ENDIF
			IF nextState != PS_Knifing_State AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				//send plane to the takeoff checkpoint
				SET_ENTITY_COORDS(PS_Main.myVehicle, PS_GET_CHECKPOINT_FROM_ID(1))
				SET_VEHICLE_FORWARD_SPEED(PS_Main.myVehicle, 60.0)
				PS_Knifing_State = nextState
				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
				RETURN TRUE
			ENDIF	
		ENDIF
		RETURN FALSE
	ENDFUNC
#ENDIF								

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Knifing_Initialise()
	PS_Challenge_Knifing_Init_Checkpoints()
	PS_Knifing_State = PS_KNIFING_INIT
	
	//Start pos and heading are hard-coded since it different for each challenge.
	vStartPosition								= <<-1627.5929, -2715.2949, 12.9445>>
	vStartRotation 								= <<10.5441, 0.0000, 330.0000>>
	fStartHeading 								= 330.0000
	iFailDialogue								= 1
	bLessonCompleted							= FALSE
	bLesonFinishedLineTriggered					= FALSE
	
	//Player vehicle model
	VehicleToUse = STUNT
	
	PS_SETUP_CHALLENGE()	
	PS_REQUEST_SHARED_ASSETS()
	
	PS_SET_GROUND_OFFSET(1.5)
	iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_KNIFING
	REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		WAIT(0)
	ENDWHILE

	fPreviewVehicleSkipTime = 28000
	fPreviewTime = 11	
	
	vPilotSchoolSceneCoords = vStartPosition
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
		vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
	ENDIF
	
	//Spawn player vehicle			
	PS_CREATE_VEHICLE()
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Knifing.sc******************")
	#ENDIF
ENDPROC

PROC PS_Knifing_MainSetup()
	PS_Challenge_Knifing_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	
	NEW_LOAD_SCENE_START(<<-1637.2950, -2732.1042, 17.4674>>, NORMALISE_VECTOR(<<-1634.5145, -2727.2866, 16.9657>> - <<-1637.2950, -2732.1042, 17.4674>>), 5000.0)
ENDPROC

FUNC BOOL PS_Knifing_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()			
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT, iPreviewCheckpointIdx, TRUE)	
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV2", "PS_PREV2_1", "PS_PREV2_2", "PS_PREV2_3")
				//"This lesson will help you build your stick and rudder skills."
				//"You'll have to keep your plane steady while rolled over on either side."
				//"Perfecting this trick will really come in handy later on. "
				BREAK
			CASE PS_PREVIEW_PLAYING
				//let vehicle recording play
				BREAK
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC
					
FUNC BOOL PS_Knifing_MainUpdate()
	IF TempDebugInput() AND NOT bFinishedChallenge
		
		#IF IS_DEBUG_BUILD
			IF PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
				IF PS_KNIFING_J_SKIP()
					RETURN TRUE //if we do a j skip, then skip an update
				ENDIF
			ENDIF
		#ENDIF
	
		PS_LESSON_UDPATE()
		
		SWITCH(PS_Knifing_State)
		//Spawn player and plane on the runway
			CASE PS_KNIFING_INIT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Knifing_State = PS_KNIFING_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Play countdown
			CASE PS_KNIFING_COUNTDOWN
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Knifing_State = PS_KNIFING_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving off
			CASE PS_KNIFING_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//Setup objective, give the player controls and manage any new help text
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT)
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_PLAY_DISPATCHER_INSTRUCTION( "PS_KNIFE", "PS_KNIFE_1")//"Take off and head for the checkpoint marked at the runway. Then I'll talk you through it."
						PS_HUD_RESET_TIMER()
						PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Knifing_Fail_Check()
							PS_Knifing_State = PS_KNIFING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Knifing_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND iKnifingInstructionCounter = 1
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_KNIFE", "PS_KNIFE_2") // "A knife is one of the harder moves. You have to keep your wings steady and use your rudders to maintain altitude."
							iKnifingInstructionCounter++
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND iKnifingInstructionCounter = 2
							PRINT("PSKNIFING_A", DEFAULT_GOD_TEXT_TIME, 1) // "~s~Head for the first ~y~checkpoint.~s~"
							iKnifingInstructionCounter++
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_KILL_HINT_CAM()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)	
						PS_SET_HINT_CAM_ACTIVE(FALSE)
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_KNIFE", "PS_KNIFE_3")
						//"When I give the signal, I want you to try to hold a knife edge with your plane."
						iKnifingInstructionCounter = 1
						PS_Knifing_State = PS_KNIFING_STAGE_ONE_PREP
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_KNIFING_STAGE_ONE_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
	 				    PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Knifing_Fail_Check()
							PS_Knifing_State = PS_KNIFING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Knifing_Progress_Check()
							PS_INCREMENT_SUBSTATE()				
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						CLEAR_HELP()
						KILL_ANY_CONVERSATION()						
						PS_Knifing_State = PS_KNIFING_STAGE_ONE_START
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		
			CASE PS_KNIFING_STAGE_ONE_START
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//set knifing progress meter, then turn it on	
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_KNIFEOBJ", "PS_KNIFEOBJ_1") // "Perform a knife edge."
						PS_INCREMENT_SUBSTATE()
						PS_SET_KNIFE_METER_ACTIVE(TRUE)
						Pilot_School_HUD_Reset_Obj_Meter()
						PS_HUD_SET_OBJECTIVE_TYPE(PS_OBJECTIVE_TYPE_KNIFE)
						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(TRUE)
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Knifing_Fail_Check()
							PS_Knifing_State = PS_KNIFING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND PS_Challenge_Knifing_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						KILL_ANY_CONVERSATION()
						iKnifingInstructionCounter = 1
						PS_Knifing_State = PS_KNIFING_STAGE_ONE_HOLD
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

		//Track how long the player is holding the knife
			CASE PS_KNIFING_STAGE_ONE_HOLD
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINT_HELP("PSKNIFING_D") // "~s~Use ~PAD_LB~ or ~PAD_RB~ to control your rudders."
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_KNIFE", "PS_KNIFE_5", "PS_KNIFE", "PS_KNIFE_6") 
						//"Perfect, now hold that position."
						//"Don't forget to use your rudders to maintain altitude."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Knifing_Fail_Check()
							PS_Knifing_State = PS_KNIFING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Knifing_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						PS_SET_KNIFE_METER_ACTIVE(FALSE)
						Pilot_School_HUD_Reset_Obj_Meter()
						PS_Knifing_State = PS_KNIFING_STAGE_ONE_LEVEL_OUT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_KNIFING_STAGE_ONE_LEVEL_OUT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_KNIFEOBJ", "PS_KNIFEOBJ_4") //"Level out the plane."
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Knifing_Fail_Check()
							PS_Knifing_State = PS_KNIFING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND PS_Challenge_Knifing_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						PS_Knifing_State = PS_KNIFING_STAGE_TWO_PREP
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_KNIFING_STAGE_TWO_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_KNIFE", "PS_KNIFE_7","PS_KNIFE", "PS_KNIFE_8") 
						//"Good! Let's see if you can do the same thing on the other side."	
 				    	//"On my mark, roll your plane over to the opposite side and hold the position."
						
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Knifing_Fail_Check()
							PS_Knifing_State = PS_KNIFING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Knifing_Progress_Check()
							PS_INCREMENT_SUBSTATE()				
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						CLEAR_HELP()
						KILL_ANY_CONVERSATION()						
						PS_Knifing_State = PS_KNIFING_STAGE_TWO_START
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_KNIFING_STAGE_TWO_START
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
							IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
								//this needs to be checking the correct side:
								PS_PLAY_DISPATCHER_INSTRUCTION("PS_KNIFEOBJ", "PS_KNIFEOBJ_3") // "Perform a knife edge on the other side."
								Pilot_School_HUD_Reset_Obj_Meter()
								PS_SET_KNIFE_METER_ACTIVE(TRUE)
								IF eCurrentKnifingObjective = PS_OBJECTIVE_TYPE_KNIFE_R
									PRINTLN("SETTING OBJECTIVE METER TO KNIFE LEFT!!!")
									PS_HUD_SET_OBJECTIVE_TYPE(PS_OBJECTIVE_TYPE_KNIFE_L)
								ELSE
									PRINTLN("SETTING OBJECTIVE METER TO KNIFE RIGHT!!!")
									PS_HUD_SET_OBJECTIVE_TYPE(PS_OBJECTIVE_TYPE_KNIFE_R)
								ENDIF
								PS_HUD_SET_OBJECTIVE_METER_VISIBLE(TRUE)
						    	PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							IF PS_Challenge_Knifing_Fail_Check()
								PS_Knifing_State = PS_KNIFING_FAIL
							ENDIF
						ENDIF
						BREAK
						
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Knifing_Fail_Check()
							PS_Knifing_State = PS_KNIFING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Knifing_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_HELP()
						iKnifingInstructionCounter = 1
						PS_Knifing_State = PS_KNIFING_STAGE_TWO_HOLD
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_KNIFING_STAGE_TWO_HOLD
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Knifing_Fail_Check()
							PS_Knifing_State = PS_KNIFING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Knifing_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELIF NOT IS_MESSAGE_BEING_DISPLAYED() AND iKnifingInstructionCounter = 1 //AND meter is < somenumber
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_KNIFEOBJ", "PS_KNIFEOBJ_9") // "Nice! Hold that position for a few seconds."
//							PRINT("PSKNIFING_C", DEFAULT_GOD_TEXT_TIME, 1)//"~s~Hold the plane steady until the meter fills"
							iKnifingInstructionCounter++
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						CLEAR_HELP()
						PS_SET_KNIFE_METER_ACTIVE(FALSE)
						Pilot_School_HUD_Reset_Obj_Meter()
						PS_Knifing_State = PS_KNIFING_STAGE_TWO_LEVEL_OUT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_KNIFING_STAGE_TWO_LEVEL_OUT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_KNIFEOBJ", "PS_KNIFEOBJ_4") //"Level out the plane."
						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Knifing_Fail_Check()
							PS_Knifing_State = PS_KNIFING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND PS_Challenge_Knifing_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						CLEAR_HELP()
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_Knifing_State = PS_KNIFING_SUCCESS
						ELSE
							PS_Knifing_State = PS_KNIFING_FAIL
						ENDIF
						bLessonCompleted = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_KNIFING_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							PS_UPDATE_RECORDS(VECTOR_ZERO)
							iFailDialogue = 1
							PS_PLAY_LESSON_FINISHED_LINE("PS_KNIFE", "PS_KNIFE_10", "PS_KNIFE_11", "PS_KNIFE_12", "PS_KNIFE_13")
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
							PS_LESSON_END(TRUE)
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_KNIFING_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
							ELSE
								PS_UPDATE_RECORDS(VECTOR_ZERO)
								IF ( bLessonCompleted = TRUE )
									PS_PLAY_LESSON_FINISHED_LINE("PS_KNIFE", "PS_KNIFE_10", "PS_KNIFE_11", "PS_KNIFE_12", "PS_KNIFE_13")
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_KNIFE", "PS_KNIFE_10", "PS_KNIFE_11", "PS_KNIFE_12", "")
								ENDIF
								iEndDialogueTimer = GET_GAME_TIMER()
								bLesonFinishedLineTriggered = TRUE
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_Knifing_MainCleanup()
//take care of hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	
//take care of objects	
	PS_SEND_PLAYER_BACK_TO_MENU()
	
	PS_RESET_CHECKPOINT_MGR()
	
//loaded assets	
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Knifing.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************
