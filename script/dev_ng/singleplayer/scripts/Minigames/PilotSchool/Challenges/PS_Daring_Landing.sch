//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/12/11
//	Description:	"Daring Landing" challenge for Pilot School. Player starts out in a plane that is already airborne,
//					pointed towards a bridge. The goal of the challenge is to land the plane on the bridge in the middle
//					of the street, while trying to stop the plane as close to the landing target as possible.
//	
//****************************************************************************************************


USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

//Consts
CONST_INT					MAX_DARING_CHECKPOINTS	2
CONST_INT					LOCATE_SIZE_DARING_LANDING	100
//BOOL 						bWentNearLandingCoord 		= FALSE

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Inverted Flight
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Challenge_Daring_Init_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1546.1194, 4970.3354, 115.6974>>)//0
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_LANDING, <<-1980.9310, 4538.2231, 59.1218>>)//1
ENDPROC

PROC PS_DARING_CHECK_FOR_LOW_LANDING()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		VECTOR vCheckPt = PS_GET_CURRENT_CHECKPOINT()
		VECTOR vPlayerPt = GET_ENTITY_COORDS(PS_Main.myVehicle)
		IF vPlayerPt.z < (vCheckPt.z - 5) //5m cushion
			DEBUG_MESSAGE("FAILED BC PLAYER WENT UNDER THE LANDING POINT")
			ePSFailReason = PS_FAIL_SPECIAL_REASON
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_SPEEDING_UP_ON_ROAD()		
	FLOAT tempSpeed = GET_ENTITY_SPEED(PS_Main.myVehicle)
	IF tempSpeed < 30
		
		IF fCurPlaneSpeed = -1
			fCurPlaneSpeed = tempSpeed
		ELSE
			IF fCurPlaneSpeed < tempSpeed + 1// 1m/s slack
				fCurPlaneSpeed = -1
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		fCurPlaneSpeed = -1
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determines what fail conditions we need to check dependending on the state/stage of the challenge we're in
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Daring_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_Daring_State)
			CASE PS_DARING_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DARING_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DARING_TAKEOFF
				//Do default checks, minus runway
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE)
					RETURN TRUE
				ENDIF
				// This has been removed due to url:bugstar:2149994. Landing is being checked in PS_UPDATE_FAIL_CHECKS above
//				IF GET_ENTITY_SPEED(PS_Main.myVehicle) < 5.0
//					IF NOT IS_ENTITY_IN_ANGLED_AREA(PS_Main.myVehicle, <<-1777.394897,4741.145020,55.200260>>, <<-2029.554199,4489.504883,58.195431>>, 20)
//						DEBUG_MESSAGE("FAILED BC PLAYER LANDED VEHICLE")
//						ePSFailReason = PS_FAIL_LANDED_VEHICLE
//						RETURN TRUE
//					ENDIF
//				ENDIF
				BREAK
			
			CASE PS_DARING_LANDING
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, TRUE, TRUE, FALSE)
					RETURN TRUE
				ENDIF
				IF GET_ENTITY_SPEED(PS_Main.myVehicle) < 5.0
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PS_Main.myVehicle, <<-1777.394897,4741.145020,55.200260>>, <<-2029.554199,4489.504883,58.195431>>, 20)
						DEBUG_MESSAGE("FAILED BC PLAYER LANDED VEHICLE")
						ePSFailReason = PS_FAIL_TOO_FAR
						RETURN TRUE
					ENDIF
				ENDIF
				BREAK
		
			DEFAULT
				//Do default checks, minu stuck check
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE, TRUE, TRUE, FALSE)
					RETURN TRUE
				ENDIF
				
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    When it returns TRUE, the main loop will increment the challenge state/stage
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Daring_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_Daring_State)
			CASE PS_DARING_INIT
				RETURN FALSE
				BREAK

			CASE PS_DARING_COUNTDOWN
				RETURN FALSE
				BREAK
				
			CASE PS_DARING_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1 OR PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF						
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DARING_LANDING		
				IF GET_ENTITY_SPEED(PS_Main.myVehicle) < 5.0
					IF IS_ENTITY_IN_ANGLED_AREA(PS_Main.myVehicle, <<-1777.394897,4741.145020,55.200260>>, <<-2029.554199,4489.504883,58.195431>>, 20)
						RETURN TRUE
					ENDIF
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE
ENDFUNC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Daring_Initialise()
	PS_Challenge_Daring_Init_Checkpoints()	
	vStartPosition = <<-824.4, 5680.4, 395.1864>>//<< -609.1918, 5842.0552, 200.8225 >>//<< -2125, -1095, 199.68>>
	fStartHeading = 134.5
	vStartRotation = <<-16.0000, 0.0000, fStartHeading>>
	iFailDialogue = 1
	bLessonCompleted = FALSE
	bLesonFinishedLineTriggered	= FALSE
	
	//Bool used to keep the loop going for the entire challenge
	bFinishedChallenge = FALSE
	iInstructionCounter = 1
	
	PS_Special_Fail_Reason = "PS_FAIL_3A"
	
	//Player vehicle model
	VehicleToUse = STUNT
	PS_SETUP_CHALLENGE()
	PS_REQUEST_SHARED_ASSETS()
	
	SET_ROADS_IN_AREA(<<-1506.4900, 5000.0000, 62.0109>>, <<-2177.8604, 4408.8848, 59.1266>>, FALSE)
	CLEAR_AREA(<<-1506.4900, 5000.0000, 62.0109>> + (<<-2177.8604, 4408.8848, 59.1266>> - <<-1506.4900, 5000.0000, 62.0109>>), 500, TRUE)
	
	//offset used for checking if player is on the ground or in the air
	PS_SET_GROUND_OFFSET(1.5)
	iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_DARING
	REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		WAIT(0)
	ENDWHILE	
	IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
		SET_VEHICLE_ACTIVE_DURING_PLAYBACK(PS_Main.myVehicle, TRUE) 
	ENDIF
	fCurPlaneSpeed = -1
	
	fPreviewVehicleSkipTime = 17000
	fPreviewTime = 15
	
	vPilotSchoolSceneCoords = vStartPosition//<<-628.692, 5842.0552, 200.8225 >>
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName) //<< -1831.4043, 4688.6953, 69.2119 >>
		vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
	ENDIF
	
	//Spawn player vehicle
	PS_CREATE_VEHICLE()

	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	SET_ENTITY_MAX_SPEED(PS_Main.myVehicle, 70.001)

	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Daring_Landing.sc******************")
	#ENDIF
ENDPROC

PROC PS_Daring_MainSetup()
	PS_Challenge_Daring_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		SET_ENTITY_ROTATION(PS_Main.myVehicle, vStartRotation)
		PS_AUTO_PILOT(PS_Main, 70.0, FALSE, vStartRotation.x, vStartRotation.y)
	ENDIF
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	NEW_LOAD_SCENE_START(<<-828.8492, 5675.9849, 395.3450>>, NORMALISE_VECTOR(<<-855.9717, 5652.1563, 384.8416>> - <<-828.8492, 5675.9849, 395.3450>>), 5000.0)
ENDPROC

FUNC BOOL PS_Daring_Preview()
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE, iPreviewCheckpointIdx, TRUE)
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV5", "PS_PREV5_1", "PS_PREV5_2", "PS_PREV5_3")
				//"You're going to learn how to perform an emergency landing in this lesson."
				//"Remember to begin decelerating while you are still airborne for a smoother landing."
				//"Then, once you touch asphalt, the brakes should slow you down real quick."
				BREAK
			CASE PS_PREVIEW_PLAYING
				//let vehicle recording play
				BREAK
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PS_DARING_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_Daring_State = PS_DARING_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_DARING_FORCE_PASS
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		SET_ENTITY_COORDS(PS_Main.myVehicle, PS_GET_LAST_CHECKPOINT())
		BRING_VEHICLE_TO_HALT(PS_Main.myVehicle, 5.0, 1)
	ENDIF
	PS_Daring_State = PS_DARING_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

FUNC BOOL PS_Daring_MainUpdate()

	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

	IF TempDebugInput() AND NOT bFinishedChallenge

		PS_LESSON_UDPATE()
		
		IF PS_Daring_State = PS_DARING_INIT OR PS_Daring_State = PS_DARING_COUNTDOWN
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LEFT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_RIGHT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UP_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_DOWN_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
		ENDIF
		
		SWITCH(PS_Daring_State)
		
		//Spawn player and plane in the air, aimed towards the bridge/landing target, with a forward velocity
			CASE PS_DARING_INIT
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 70.0, FALSE, vStartRotation.x, vStartRotation.y)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Daring_State = PS_DARING_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

		//Play countdown
			CASE PS_DARING_COUNTDOWN
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 70.0, FALSE, vStartRotation.x, vStartRotation.y)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_HUD_UPDATE_COUNTDOWN()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Daring_State = PS_DARING_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Give player control and tell him to head for the bridge checkpoint
			CASE PS_DARING_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						//Setup objective, give the player controls and manage any new help text
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_LANDING)
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_LAND", "PS_LAND_1", "PS_LAND", "PS_LAND_2")
						//"Okay, do you see the checkpoint marked on the radar?"
						//"You're going to practice an emergency landing on that bridge straight ahead."
//						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
//						PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Daring_Fail_Check()
							PS_Daring_State = PS_DARING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Daring_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND iInstructionCounter = 1
							PRINT("PLANDING_A", DEFAULT_GOD_TEXT_TIME, 1)//~s~Head for the first ~y~checkpoint.~s~
							iInstructionCounter++
						ENDIF
						BREAK
	
					CASE PS_SUBSTATE_EXIT
						iInstructionCounter = 1
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_Daring_State = PS_DARING_LANDING
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			//Tell player to aim for the landing target and check for the plane landing
			CASE PS_DARING_LANDING
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						bHasPlaneSpedUp = FALSE
						QUEUE_OBJHELPALOGUE_HELP("PLANDING_B", DEFAULT_GOD_TEXT_TIME, 1.0) // "~s~Hold ~PAD_LT~ to lower your speed until the plane slowly descends."
						PLAY_OBJHELPALOGUE()
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_LAND", "PS_LAND_3", "PS_LAND", "PS_LAND_4")
						//"Slow your approach as you descend onto the road. "		
						//"If you touch down smoothly your plane is going to stop pretty fast."
						
 				    	PS_SET_DIST_HUD_ACTIVE(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
						
					CASE PS_SUBSTATE_UPDATE
						IF PS_Daring_Fail_Check()
							PS_Daring_State = PS_DARING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Daring_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_OBJHELPALOGUE_RUNNING() AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED() AND iInstructionCounter = 1
								PS_PLAY_DISPATCHER_INSTRUCTION("PS_LANDOBJ", "PS_LANDOBJ_1")//"Bring the plane down onto the landing target."						
//								PRINT("PLANDING_C", DEFAULT_GOD_TEXT_TIME, 1)//~s~Bring the plane down onto the landing target.
//								QUEUE_OBJHELPALOGUE_HELP("PLANDING_D", -1, 1.5) // "~s~Land closer to the target for a better score."
								PLAY_OBJHELPALOGUE()
								iInstructionCounter++
							ENDIF
						ENDIF
						
						IF bHasPlaneSpedUp
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
						ELIF IS_PLAYER_SPEEDING_UP_ON_ROAD()
							bHasPlaneSpedUp = TRUE
						ENDIF
						
						BREAK
						
					CASE PS_SUBSTATE_EXIT
						IF IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							ePSFailReason = PS_FAIL_DAMAGED_VEHICLE
							PRINTLN("Vehicle is destroyed setting ePSFailReason to PS_FAIL_DAMAGED_VEHICLE.")
						ENDIF
						PS_DARING_CHECK_FOR_LOW_LANDING()
						PS_UPDATE_RECORDS(PS_GET_CURRENT_CHECKPOINT())
						PS_SET_DIST_HUD_ACTIVE(FALSE)
						IF 	NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
						AND	Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							SET_ENTITY_INVINCIBLE(PS_Main.myVehicle, TRUE)
							BRING_VEHICLE_TO_HALT(PS_Main.myVehicle, 3.0, 1)
							PS_Daring_State = PS_DARING_SUCCESS
						ELSE
							ePSFailReason 	= PS_FAIL_DAMAGED_VEHICLE
							PS_Daring_State = PS_DARING_FAIL
							PRINTLN("Vehicle is destroyed setting ePSFailReason to PS_FAIL_DAMAGED_VEHICLE and PS_Daring_State to PS_DARING_FAIL.")
						ENDIF
						IF ePSFailReason = PS_FAIL_TOO_FAR OR ePSFailReason = PS_FAIL_LANDED_VEHICLE
							PRINTLN("Plane was too far. Checking to see if it's still on the bridge")
							IF IS_ENTITY_IN_ANGLED_AREA(PS_Main.myVehicle, <<-1777.394897,4741.145020,55.200260>>, <<-2029.554199,4489.504883,58.195431>>, 20)
								PRINTLN("Plane is still on the bridge! SUCCESS!")
//								SET_ENTITY_INVINCIBLE(PS_Main.myVehicle, TRUE)
								BRING_VEHICLE_TO_HALT(PS_Main.myVehicle, 3.0, 1)
								PS_Main.myChallengeData.bBronzeOverride = TRUE
								ePSFailReason = PS_FAIL_NO_REASON
								PS_Daring_State = PS_DARING_SUCCESS
							ELSE
								PRINTLN("Plane is not detected on the bridge! FAIL!")
							ENDIF
						ENDIF
						SET_ENTITY_INVINCIBLE(PS_Main.myVehicle, TRUE)
						IF PS_Daring_State = PS_DARING_SUCCESS
							PS_INCREMENT_CHECKPOINT()
						ELSE
							PS_HIDE_CHECKPOINT(PS_GET_TOTAL_CHECKPOINTS())
						ENDIF
						bLessonCompleted = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
												
			CASE PS_DARING_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("Entering PS_DARING_SUCCESS")
						IF ( bLesonFinishedLineTriggered = FALSE )
							PS_UPDATE_RECORDS(PS_GET_LAST_CHECKPOINT())
							IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle) AND GET_ENTITY_SPEED(PS_Main.myVehicle) < 5.0
								SET_VEHICLE_UNDRIVEABLE(PS_Main.myVehicle, TRUE)
								SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, FALSE, TRUE)
							ENDIF
							IF PS_IS_COORD_IN_FRONT_OF_PLAYER(PS_GET_LAST_CHECKPOINT())
								PS_PLAY_LESSON_FINISHED_LINE("PS_LAND", "PS_LAND_5", "PS_LAND_6", "PS_LAND_7", "PS_LAND_8")			
							ELSE
								PS_PLAY_LESSON_FINISHED_LINE("PS_LAND", "PS_LAND_5", "PS_LAND_6", "PS_LAND_7", "PS_LAND_9")
							ENDIF
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF 	NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
						AND NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							PS_LESSON_END(TRUE)
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								IF IS_VEHICLE_FUCKED(PS_Main.myVehicle)
									//reset bPlayerDataHasBeenUpdated to FALSE
									//call update records again to get new failed data
									//reset iCurrentChallengeScore to 0
									PRINTLN("Vehicle is destroyed 1.")
								ENDIF
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
	
			CASE PS_DARING_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("Entering PS_DARING_FAIL")
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
							ELSE
								IF IS_VEHICLE_FUCKED(PS_Main.myVehicle)
									//reset bPlayerDataHasBeenUpdated to FALSE
									//call update records again to get new failed data
									//reset iCurrentChallengeScore to 0
									
									ePSFailReason = PS_FAIL_DAMAGED_VEHICLE
									
									PRINTLN("Vehicle is destroyed 1.")
								ENDIF
								PS_UPDATE_RECORDS(PS_GET_LAST_CHECKPOINT())
								IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle) AND GET_ENTITY_SPEED(PS_Main.myVehicle) < 5.0
									SET_VEHICLE_UNDRIVEABLE(PS_Main.myVehicle, TRUE)
									SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, FALSE, TRUE)
								ENDIF
								IF ( bLessonCompleted = TRUE )
									IF PS_IS_COORD_IN_FRONT_OF_PLAYER(PS_GET_LAST_CHECKPOINT())
										PS_PLAY_LESSON_FINISHED_LINE("PS_LAND", "PS_LAND_5", "PS_LAND_6", "PS_LAND_7", "PS_LAND_8")			
									ELSE
										PS_PLAY_LESSON_FINISHED_LINE("PS_LAND", "PS_LAND_5", "PS_LAND_6", "PS_LAND_7", "PS_LAND_9")	
									ENDIF
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_LAND", "PS_LAND_5", "PS_LAND_6", "PS_LAND_7", "")
								ENDIF
								iEndDialogueTimer = GET_GAME_TIMER()
								bLesonFinishedLineTriggered = TRUE
								
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC

PROC PS_Daring_MainCleanup()
	//TODO: do we need this?
	PS_Special_Fail_Reason = ""
	
//hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	PS_RESET_CHECKPOINT_MGR()
	
	//put cars/traffic back on the roads
	SET_ROADS_BACK_TO_ORIGINAL(<<-1506.4900, 5000.0000, 62.0109>>, <<-2177.8604, 4408.8848, 59.1266>>)

//player/objects
	PS_SEND_PLAYER_BACK_TO_MENU()	
//assets
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Daring_Landing.sc******************")
	#ENDIF
ENDPROC


// EOF
//****************************************************************************************************
//****************************************************************************************************
