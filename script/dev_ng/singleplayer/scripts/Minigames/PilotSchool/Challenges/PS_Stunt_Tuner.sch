 //****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/12/11
//	Description:	"Loop the Loop" Challenge for Pilot School. The player is required to do two loop-de-loops
//					and then an immelman to pass the challenge.
//****************************************************************************************************
//Plane 1a Offset: <<-32.3339,-1.7793,-0.0107>>
//Plane 1b Offset: <<-15.0338,9.0205,-0.0107>>
//Plane 2a Offset: <<-15.6339,-10.9795,-0.0107>>


#IF IS_DEBUG_BUILD

USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

CONST_INT PS_MAX_DEBUG_FORMATION_CHECKPOINTS 20

//widget stuffs
WIDGET_GROUP_ID	wPS_Widget_Tools
//widget stuffs

	
STRING sStuntTunerFilePath	= "X:/gta5/build/dev"
STRING sStuntTunerFileName	= "temp_stunt_tuner.csv"


INT iFrameCount = 0
INT iNumPathNodes = 6
INT iEndNodeFactor = 1
FLOAT fCurPlaneHeading = 0
FLOAT fCurPlaneRoll = 0
FLOAT fCurPlanePitch = 0
FLOAT fPathDistance = 10 //def
FLOAT fOffsetHeadings[8]
VECTOR vCurPlaneCoords = VECTOR_ZERO
VECTOR vLastPlaneCoords = VECTOR_ZERO
VECTOR vCenterVector = << -1335.8392, -3047.3298, 30.0000 >>
VECTOR vOffsetVectors[8]
FLOAT fDbgLastPlaneHeading = 0
FLOAT fDbgPlaneHeading = 0


//widget switches
BOOL bDbgToggleUsePathDistance = FALSE
BOOL bDbgTogglePrintVectors = FALSE
BOOL bDbgToggleFreezePlane = FALSE
BOOL bDbgTogglePlayerControl = FALSE
BOOL bDbgToggleStartRecording = FALSE
BOOL bDbgToggleClearData = FALSE


BOOL bDbgToggleDrawPathNodes = FALSE

//formation tool
//checkpoints
VEHICLE_INDEX vehPlane1a, vehPlane1b, vehPlane2a
FLOAT fDbgCheckpointSpacing = 250.0
FLOAT fDbgCheckpointHeight = 60.0
BOOL bDbgActivateCheckpointMgr = FALSE
BOOL bDbgLoadHardCodedCheckpoints = FALSE
BOOL bDbgToggleShowCheckpoints = FALSE
BOOL bDbgAreCheckpointsShowing = FALSE
BOOL bDbgGenerateCheckpoints = FALSE
BOOL bDbgSaveCheckpointsToFile = FALSE
BOOL bDbgPrintFormationOffsets = FALSE
INT iDbgCheckpointTypes[PS_MAX_DEBUG_FORMATION_CHECKPOINTS + 1]
VECTOR vDbgCheckpoints[PS_MAX_DEBUG_FORMATION_CHECKPOINTS + 1]

//formation planes
VECTOR vDbgPlane1a = VECTOR_ZERO
VECTOR vDbgPlane1b = VECTOR_ZERO
VECTOR vDbgPlane2a = VECTOR_ZERO
VECTOR vDbgLastPlane1a = VECTOR_ZERO
VECTOR vDbgLastPlane1b = VECTOR_ZERO
VECTOR vDbgLastPlane2a = VECTOR_ZERO
BOOL bDbgSpawnPlane1a = FALSE
BOOL bDbgSpawnPlane1b = FALSE
BOOL bDbgSpawnPlane2a = FALSE

//
//

//tracking bools
BOOL bIsPlaneFrozen = FALSE
BOOL bIsPlayerControlled = FALSE
BOOL bIsRecording = FALSE


PROC TempUnreferencedVariableHack()
	wPS_Widget_Tools = wPS_Widget_Tools
ENDPROC

 /// PURPOSE:
///    Cleans up the script for this specific challenge
/// PARAMS:
///    PS_Main - Struct that contains all the challenge vars/objectptrs
PROC PS_Stunt_Tuner_Cleanup()

	//TODO: Save new unlocks/records	

	//TODO: Destroy scripted cameras

	//TODO: Destroy any texture dicts streamed in
	DELETE_WIDGET_GROUP(wPS_Widget_Tools)
	PS_SEND_PLAYER_BACK_TO_MENU()
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
		DELETE_VEHICLE(PS_Main.myVehicle)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	CLEAR_PRINTS()
	CLEAR_HELP()
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Stunt_Tuner.sch******************")
	#ENDIF
	
	
ENDPROC

PROC PS_Tuner_Initialise_Vars()
	INT i = 0 //for counting
	
	//stunt tuner
	
	
	//skydiving plane
	iNumPathNodes = 6
	bDbgToggleDrawPathNodes = FALSE
	
	
	iFrameCount = 0
	fCurPlaneHeading = 0
	fCurPlaneRoll = 0
	fCurPlanePitch = 0
	fPathDistance = 10

	vCenterVector = << -1335.8392, -3047.3298, 30.0000 >>
	
	//widget toggle bools
	bDbgToggleUsePathDistance = FALSE
	bDbgToggleFreezePlane = FALSE
	bDbgTogglePlayerControl = FALSE
	bDbgToggleStartRecording = FALSE

	
	//formation tool bools
	//checkpoint bools
	
	REPEAT COUNT_OF(vDbgCheckpoints) i
		iDbgCheckpointTypes[i] = 0
		vDbgCheckpoints[i] = VECTOR_ZERO
	ENDREPEAT
	fDbgCheckpointSpacing = 250.0
	fDbgCheckpointHeight = 60.0
	bDbgActivateCheckpointMgr = FALSE
	bDbgLoadHardCodedCheckpoints = FALSE
	bDbgToggleShowCheckpoints = FALSE
	bDbgAreCheckpointsShowing = FALSE
	bDbgGenerateCheckpoints = FALSE
	bDbgSaveCheckpointsToFile = FALSE
	bDbgPrintFormationOffsets = FALSE
	
	//formation planes
	vDbgPlane1a = VECTOR_ZERO
	vDbgPlane1b = VECTOR_ZERO
	vDbgPlane2a = VECTOR_ZERO

	bDbgSpawnPlane1a = FALSE
	bDbgSpawnPlane1b = FALSE
	bDbgSpawnPlane2a = FALSE
	
	//tracking bools
	bIsPlaneFrozen = FALSE
	bIsPlayerControlled = FALSE
	bIsRecording = FALSE
	
	//vectors
	vLastPlaneCoords = VECTOR_ZERO
	vCurPlaneCoords = VECTOR_ZERO
	
ENDPROC

FUNC INT PS_Tuner_Get_Checkpoint_Type_Int(PS_CHECKPOINT_ENUM thisType)
	SWITCH thisType
		CASE PS_CHECKPOINT_OBSTACLE_PLANE
			RETURN 0
			BREAK
		
		CASE PS_CHECKPOINT_STUNT_INVERTED
			RETURN 1
			BREAK
		
		CASE PS_CHECKPOINT_STUNT_LEFT
			RETURN 2
			BREAK
			
		CASE PS_CHECKPOINT_STUNT_RIGHT
			RETURN 3
			BREAK
	ENDSWITCH
	SCRIPT_ASSERT("couldnt get checkpoint type!")
	RETURN 0
ENDFUNC

PROC PS_Tuner_Init_Formation_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()

//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-2684.4258, -1260.5272, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-2434.4946, -1254.6599, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_LEFT, <<-2184.5635, -1248.7926, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1934.6323, -1242.9253, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_RIGHT, <<-1684.7012, -1237.0580, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1434.7700, -1231.1907, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1184.8389, -1225.3234, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-934.9077, -1219.4561, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-684.9766, -1213.5887, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-435.0454, -1207.7214, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_RIGHT, <<-185.1143, -1201.8541, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<64.8169, -1195.9868, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_LEFT, <<314.7480, -1190.1195, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<564.6791, -1184.2522, 70.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<814.6103, -1178.3849, 70.0000>>)
	
//	//CHECKPOINTS OVER THE MOUNTAINS/CITY
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<2254.8638, -1145.4482, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<2004.9564, -1152.2542, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_LEFT, <<1755.0491, -1159.0601, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1505.1417, -1165.8660, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_RIGHT, <<1255.2344, -1172.6719, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1005.3270, -1179.4778, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<755.4197, -1186.2837, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<505.5123, -1193.0896, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<255.6050, -1199.8955, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<5.6976, -1206.7014, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-244.2097, -1213.5073, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_RIGHT, <<-494.1171, -1220.3132, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-744.0244, -1227.1191, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_LEFT, <<-993.9318, -1233.9250, 265.4754>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1243.8391, -1240.7310, 265.4754>>)

//	//Adjusted Z Values over mountains
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<2254.8638, -1145.4482, 250.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<2004.9564, -1152.2542, 240.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_LEFT, <<1755.0491, -1159.0601, 220.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1505.1417, -1165.8660, 200.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_RIGHT, <<1255.2344, -1172.6719, 180.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1005.3270, -1179.4778, 140.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<755.4197, -1186.2837, 110.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<505.5123, -1193.0896, 110.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<255.6050, -1199.8955, 110.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<5.6976, -1206.7014, 120.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-244.2097, -1213.5073, 130.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_RIGHT, <<-494.1171, -1220.3132, 130.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-744.0244, -1227.1191, 130.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_LEFT, <<-993.9318, -1233.9250, 130.0000>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1243.8391, -1240.7310, 130.0000>>)

	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<4003.6877, -1152.3419, 95.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<3703.7231, -1156.9493, 95.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_LEFT, <<3403.7585, -1161.5568, 95.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<3103.7939, -1166.1642, 110.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_RIGHT, <<2803.8293, -1170.7716, 140.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<2503.8647, -1175.3790, 170.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<2203.9001, -1179.9865, 220.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1903.9355, -1184.5939, 200.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1603.9709, -1189.2013, 175.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1304.0063, -1193.8087, 150.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1004.0417, -1198.4161, 110.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<704.0771, -1203.0236, 100.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<404.1125, -1207.6310, 75.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<104.1479, -1212.2384, 75.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-195.8167, -1216.8458, 75.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-495.7813, -1221.4532, 100.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_RIGHT, <<-795.7458, -1226.0607, 100.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1095.7104, -1230.6681, 100.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_LEFT, <<-1395.6750, -1235.2755, 100.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1695.6396, -1239.8829, 100.0000>>)

	
	INT i = 0
	REPEAT PS_MAX_DEBUG_FORMATION_CHECKPOINTS + 1 i
		IF i > 0
			vDbgCheckpoints[i] = PS_Main.myCheckpointz[i].position
			iDbgCheckpointTypes[i] = PS_Tuner_Get_Checkpoint_Type_Int(PS_Main.myCheckpointz[i].type)
		ENDIF
	ENDREPEAT
	
ENDPROC


PROC PS_Tuner_Initialise()

	DEBUG_MESSAGE("looping script started")
	
	//Start pos and heading are hard-coded since it different for each challenge.
	vStartPosition = <<-1601.3109, -2989.9836, 14.2137>>
	vStartRotation = <<10.5441,0.0155,-120.2854>>
	fStartHeading = -120.25
	fDbgPlaneHeading =  -120.25
	fDbgLastPlaneHeading = -120.25
	
	//Player vehicle model
	VehicleToUse = STUNT
	
	PS_SETUP_CHALLENGE()	
	PS_SET_GROUND_OFFSET(1.5)

	PS_Main.myChallengeData.HasBeenPreviewed = TRUE	
	
	//Spawn player vehicle			
	PS_CREATE_VEHICLE()
	SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle)
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Initialising PS_Stunt_Tuner.sch******************")
	#ENDIF
ENDPROC

PROC PS_Stunt_Tuner_Create_Widget()
	INT i = 0 //for counting
	
	wPS_Widget_Tools = START_WIDGET_GROUP("Pilot School - Tools")
		START_WIDGET_GROUP("Stunt Tuner")
			START_WIDGET_GROUP("Plane Data")
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					vCurPlaneCoords = GET_ENTITY_COORDS(PS_Main.myVehicle, TRUE)
				ENDIF
				ADD_WIDGET_VECTOR_SLIDER("Current Plane Coords:", vCurPlaneCoords, -7500, 7500, 0.5)
				ADD_WIDGET_FLOAT_READ_ONLY("Heading/Yaw:", fCurPlaneHeading)
				ADD_WIDGET_FLOAT_READ_ONLY("Pitch:", fCurPlanePitch)
				ADD_WIDGET_FLOAT_READ_ONLY("Roll:", fCurPlaneRoll)
				ADD_WIDGET_BOOL("READ ONLY IsRecording ", bIsRecording)
				ADD_WIDGET_BOOL("Toggle Manual Control", bDbgTogglePlayerControl)		
				ADD_WIDGET_BOOL("Toggle Freeze Plane", bDbgToggleFreezePlane)
				ADD_WIDGET_BOOL("Start/Stop Recording Data", bDbgToggleStartRecording)
				ADD_WIDGET_BOOL("CLEAR Recording Data", bDbgToggleClearData)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Skydiving Plane")
			START_WIDGET_GROUP("Offset Nodes")
				ADD_WIDGET_BOOL("Show Pathnodes", bDbgToggleDrawPathNodes)
				ADD_WIDGET_BOOL("Recalculate Path Nodes", bDbgToggleUsePathDistance)
				ADD_WIDGET_VECTOR_SLIDER("Center Vector", vCenterVector, -10000, 10000, 1)
				ADD_WIDGET_FLOAT_SLIDER("Pathnode Distance", fPathDistance, 10, 10000, 1)
				ADD_WIDGET_INT_SLIDER("6 or 8 Path Nodes", iNumPathNodes, 6, 8, 2)
				ADD_WIDGET_INT_SLIDER("end node factor", iEndNodeFactor, 1, 20, 1)
				ADD_WIDGET_BOOL("Print vectors", bDbgTogglePrintVectors)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Formation Tool")
			START_WIDGET_GROUP("Player Plane Controls")
				ADD_WIDGET_VECTOR_SLIDER("Current Plane Coords:", vCurPlaneCoords, -7500, 7500, 0.5)
				ADD_WIDGET_BOOL("Toggle Manual Control", bDbgTogglePlayerControl)		
				ADD_WIDGET_BOOL("Toggle Freeze Plane", bDbgToggleFreezePlane)
				ADD_WIDGET_FLOAT_SLIDER("Plane Heading", fDbgPlaneHeading, -360, 360, 0.1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Checkpoint Generator")
				ADD_WIDGET_BOOL("Activate checkpoints manager", bDbgActivateCheckpointMgr)
				ADD_WIDGET_BOOL("Load hardcoded checkpoints", bDbgLoadHardCodedCheckpoints)
				ADD_WIDGET_BOOL("Show Checkpoints", bDbgToggleShowCheckpoints)
				ADD_WIDGET_BOOL("Save checkpoints to debug text file", bDbgSaveCheckpointsToFile)
				ADD_WIDGET_BOOL("Generate Checkpoints", bDbgGenerateCheckpoints)
				ADD_WIDGET_FLOAT_SLIDER("Height of checkpoints", fDbgCheckpointHeight, 0, 200, 1)
				ADD_WIDGET_FLOAT_SLIDER("Spacing between checkpoints", fDbgCheckpointSpacing, 0, 500, 1)
				TEXT_LABEL sCurDbgString, sCheckpoint = "Checkpoint "
				REPEAT PS_MAX_DEBUG_FORMATION_CHECKPOINTS + 1 i
					IF i > 0
						START_NEW_WIDGET_COMBO()
							ADD_TO_WIDGET_COMBO("Normal")
							ADD_TO_WIDGET_COMBO("Inverted")
							ADD_TO_WIDGET_COMBO("Left Knife")
							ADD_TO_WIDGET_COMBO("Right Knife")
						STOP_WIDGET_COMBO("Checkpoint Type", iDbgCheckpointTypes[i])
						sCurDbgString = sCheckpoint
						sCurDbgString += GET_STRING_FROM_INT(i)
						ADD_WIDGET_VECTOR_SLIDER(sCurDbgString, vDbgCheckpoints[i], -7500, 7500, 0.5)
					ENDIF
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Formation Planes")
//				ADD_WIDGET_BOOL("Move Plane1a near player", bDbgMovePlane1aNearPlayer)
				ADD_WIDGET_BOOL("Spawn Plane1a", bDbgSpawnPlane1a)
				ADD_WIDGET_VECTOR_SLIDER("Plane1a Coords", vDbgPlane1a, -7500, 7500, 0.1)
//				ADD_WIDGET_BOOL("Move Plane1a near player", bDbgMovePlane1bNearPlayer)
				ADD_WIDGET_BOOL("Spawn Plane1b", bDbgSpawnPlane1b)
				ADD_WIDGET_VECTOR_SLIDER("Plane1b Coords", vDbgPlane1b, -7500, 7500, 0.1)
//				ADD_WIDGET_BOOL("Move Plane1a near player", bDbgMovePlane2aNearPlayer)
				ADD_WIDGET_BOOL("Spawn Plane2a", bDbgSpawnPlane2a)
				ADD_WIDGET_VECTOR_SLIDER("Plane2a Coords", vDbgPlane2a, -7500, 7500, 0.1)
				ADD_WIDGET_BOOL("Print Offsets", bDbgPrintFormationOffsets)
			STOP_WIDGET_GROUP()	
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

PROC PS_Tuner_Print_Path_Nodes()
                                                              
	INT i = 0
	DEBUG_MESSAGE("------------------------------------------")
	REPEAT iNumPathNodes i 
		DEBUG_MESSAGE("offset vector ", GET_STRING_FROM_INT(i), "-")
		DEBUG_MESSAGE(GET_STRING_FROM_VECTOR(vOffsetVectors[i]))
	ENDREPEAT
	DEBUG_MESSAGE("------------------------------------------")
ENDPROC


PROC PS_Tuner_Calculate_Path_Nodes()

	FLOAT fCurAngle
	INT i = 0
	
	IF iNumPathNodes = 6
		fCurAngle = 60
	ELSE
		fCurAngle = 45
	ENDIF
	REPEAT iNumPathNodes i 
		fOffsetHeadings[i] = fCurAngle*i
		IF (iNumPathNodes = 6 AND (i = 1 OR i = (iNumPathNodes/2 + 1))) OR (iNumPathNodes = 8 AND (i = 1 OR i = 2 OR i = (iNumPathNodes/2 + 1) OR i = (iNumPathNodes/2 + 2)))
			vOffsetVectors[i] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCenterVector, fOffsetHeadings[i], <<0, iEndNodeFactor*fPathDistance, 0>>)
//		ELIF iNumPathNodes = 8 AND (i = 1 OR i = 2 OR i = (iNumPathNodes/2 + 1) OR i = (iNumPathNodes/2 + 2))
//			vOffsetVectors[i] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCenterVector, fOffsetHeadings[i], <<0, iEndNodeFactor*fPathDistance, 0>>)
		ELSE
			vOffsetVectors[i] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCenterVector, fOffsetHeadings[i], <<0, fPathDistance, 0>>)
		ENDIF
		
	ENDREPEAT
	
	
ENDPROC

PROC PS_Tuner_MainSetup()
	
	PS_Tuner_Initialise_Vars()
	PS_Stunt_Tuner_Create_Widget()
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	PS_Tuner_Calculate_Path_Nodes()

ENDPROC

FUNC BOOL PS_Tuner_Preview()

	IF PS_PREVIEW_UPDATE_CUTSCENE()
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC


PROC PS_Tuner_Write_Heading_To_File()
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("frame, pitch, roll, heading", sStuntTunerFilePath, sStuntTunerFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sStuntTunerFilePath, sStuntTunerFileName)
ENDPROC

PROC PS_Tuner_Clear_Recording()

	CLEAR_NAMED_DEBUG_FILE(sStuntTunerFilePath, sStuntTunerFileName)

ENDPROC

PROC PS_Tuner_Fill_Data()
	iFrameCount++
	fCurPlanePitch = GET_ENTITY_PITCH(PS_Main.myVehicle)
	fCurPlaneRoll = GET_ENTITY_ROLL(PS_Main.myVehicle)
	fCurPlaneHeading = GET_ENTITY_HEADING(PS_Main.myVehicle)
ENDPROC

PROC PS_Tuner_Update_Recording()
	SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_INT(iFrameCount), sStuntTunerFilePath, sStuntTunerFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", sStuntTunerFilePath, sStuntTunerFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_FLOAT(fCurPlanePitch), sStuntTunerFilePath, sStuntTunerFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", sStuntTunerFilePath, sStuntTunerFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_FLOAT(fCurPlaneRoll), sStuntTunerFilePath, sStuntTunerFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", sStuntTunerFilePath, sStuntTunerFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_FLOAT(fCurPlaneHeading), sStuntTunerFilePath, sStuntTunerFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sStuntTunerFilePath, sStuntTunerFileName)
ENDPROC

PROC PS_Tuner_Watch_Recording_Toggle()	
	IF bDbgToggleStartRecording
		IF NOT bIsRecording 
			IF iFrameCount = 0
				PS_Tuner_Write_Heading_To_File()
			ENDIF
			bIsRecording = TRUE
		ELSE
			bIsRecording = FALSE
		ENDIF		
	ENDIF
ENDPROC

FUNC PS_CHECKPOINT_ENUM PS_Tuner_Get_Checkpoint_Type(INT thisType)
	SWITCH thisType
		CASE 0
			RETURN PS_CHECKPOINT_OBSTACLE_PLANE
			BREAK
		
		CASE 1
			RETURN PS_CHECKPOINT_STUNT_INVERTED
			BREAK
		
		CASE 2
			RETURN PS_CHECKPOINT_STUNT_LEFT
			BREAK
			
		CASE 3
			RETURN PS_CHECKPOINT_STUNT_RIGHT
			BREAK
	ENDSWITCH
	SCRIPT_ASSERT("couldnt get checkpoint type!")
	RETURN PS_CHECKPOINT_OBSTACLE_PLANE
ENDFUNC

FUNC STRING PS_Tuner_Get_String_For_Checkpoint_Type(PS_CHECKPOINT_ENUM thisType)
	SWITCH thisType
		CASE PS_CHECKPOINT_OBSTACLE_PLANE
			RETURN "PS_CHECKPOINT_OBSTACLE_PLANE"
			BREAK
		
		CASE PS_CHECKPOINT_STUNT_INVERTED
			RETURN "PS_CHECKPOINT_STUNT_INVERTED"
			BREAK
		
		CASE PS_CHECKPOINT_STUNT_LEFT
			RETURN "PS_CHECKPOINT_STUNT_LEFT"
			BREAK
			
		CASE PS_CHECKPOINT_STUNT_RIGHT
			RETURN "PS_CHECKPOINT_STUNT_RIGHT"
			BREAK
	ENDSWITCH
	SCRIPT_ASSERT("couldnt get string for checkpoint type!")
	RETURN "PS_CHECKPOINT_OBSTACLE_PLANE"
	
	
ENDFUNC

PROC PS_Tuner_Calculate_New_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()
	
	INT i = 0
	VECTOR tempVector, vPlayerDirection, vPlayerPosition, vCurCheckpoint			
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
		GET_ENTITY_MATRIX(PLAYER_PED_ID(), vPlayerDirection, tempVector, tempVector, vPlayerPosition)
	ENDIF
	fDbgCheckpointHeight = vPlayerPosition.z
	vPlayerDirection.z = 0.0
	DEBUG_MESSAGE("playerdirection is ", GET_STRING_FROM_VECTOR(vPlayerDirection))
	vPlayerPosition.z = 0.0
	
	vCurCheckpoint = vPlayerPosition + vPlayerDirection * fDbgCheckpointSpacing
	vCurCheckpoint.z = fDbgCheckpointHeight//all checkpoints will be 60m height
	vDbgCheckpoints[0] = VECTOR_ZERO
	vDbgCheckpoints[1] = vCurCheckpoint
	PS_REGISTER_CHECKPOINT(PS_Tuner_Get_Checkpoint_Type(iDbgCheckpointTypes[1]), vCurCheckpoint)
	
	REPEAT PS_MAX_DEBUG_FORMATION_CHECKPOINTS + 1 i
		IF i > 1
			vCurCheckpoint = vDbgCheckpoints[i-1] + vPlayerDirection * fDbgCheckpointSpacing
			PS_REGISTER_CHECKPOINT(PS_Tuner_Get_Checkpoint_Type(iDbgCheckpointTypes[i]), vCurCheckpoint)
			vDbgCheckpoints[i] = vCurCheckpoint
		ENDIF
	ENDREPEAT

ENDPROC

PROC PS_Tuner_Save_Checkpoints_To_File
	string sFilePath	= "X:/gta5/build/dev"
	string sFileName	= "temp_debug_checkpoints.txt"
	CLEAR_NAMED_DEBUG_FILE(sFilePath, sFileName)
	INT i = 0
	REPEAT PS_MAX_DEBUG_FORMATION_CHECKPOINTS + 1 i
		IF i > 0	
			SAVE_STRING_TO_NAMED_DEBUG_FILE("PS_REGISTER_CHECKPOINT(", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(PS_Tuner_Get_String_For_Checkpoint_Type(PS_Main.myCheckpointz[i].type), sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("<<", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(PS_Main.myCheckpointz[i].position.x, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(PS_Main.myCheckpointz[i].position.y, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(PS_Main.myCheckpointz[i].position.z, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(">>)", sFilePath, sFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		ENDIF
	ENDREPEAT

ENDPROC

PROC PS_Update_Checkpoint_Adjustments()
	INT i = 0
	REPEAT PS_MAX_DEBUG_FORMATION_CHECKPOINTS + 1 i
		IF i > 0
			IF NOT ARE_VECTORS_EQUAL(vDbgCheckpoints[i], PS_Main.myCheckpointz[i].position)
				PS_Main.myCheckpointz[i].position = vDbgCheckpoints[i]
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC PS_Tuner_Print_Formation_Offsets()
	VECTOR Offset1, Offset2, Offset3
	
	Offset1 = vCurPlaneCoords - vDbgPlane1a
	
	Offset2 = vCurPlaneCoords - vDbgPlane1b
	
	Offset3 = vCurPlaneCoords - vDbgPlane2a
	
	DEBUG_MESSAGE("Plane 1a Offset: ", GET_STRING_FROM_VECTOR(Offset1))
	DEBUG_MESSAGE("Plane 1b Offset: ", GET_STRING_FROM_VECTOR(Offset2))
	DEBUG_MESSAGE("Plane 2a Offset: ", GET_STRING_FROM_VECTOR(Offset3))

	
ENDPROC

PROC PS_Tuner_Update_Formation_Widget()

	IF bDbgPrintFormationOffsets
		bDbgPrintFormationOffsets = FALSE
		PS_Tuner_Print_Formation_Offsets()
	ENDIF

//fDbgCheckpointSpacing 
	IF bDbgToggleShowCheckpoints
		IF bDbgAreCheckpointsShowing
			PS_Update_Checkpoint_Adjustments()
			//do nothing
		ELSE
			PS_SHOW_ALL_CHECKPOINTS()
			bDbgAreCheckpointsShowing = TRUE
		ENDIF
	ELIF bDbgAreCheckpointsShowing
		PS_HIDE_ALL_CHECKPOINTS()
		bDbgAreCheckpointsShowing = FALSE
	ELSE
		//do nothing
	ENDIF
	
	IF bDbgLoadHardCodedCheckpoints 
		bDbgLoadHardCodedCheckpoints = FALSE
		PS_Tuner_Init_Formation_Checkpoints()
	ENDIF
	
	IF bDbgActivateCheckpointMgr
		bDbgActivateCheckpointMgr = FALSE
		PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)
	ENDIF
	
	IF bDbgGenerateCheckpoints
		bDbgGenerateCheckpoints = FALSE
		PS_RESET_ALL_CHECKPOINTS()
		PS_Tuner_Calculate_New_Checkpoints()
		IF bDbgAreCheckpointsShowing
			PS_HIDE_ALL_CHECKPOINTS()
			PS_SHOW_ALL_CHECKPOINTS()
		ENDIF
	ENDIF
	
	IF bDbgSaveCheckpointsToFile 
		bDbgSaveCheckpointsToFile = FALSE
		PS_Tuner_Save_Checkpoints_To_File()
	ENDIF

	//formation planes
	//vDbgPlane1a 
	//vDbgPlane1b 
	//vDbgPlane2a
	
	IF bDbgSpawnPlane1a
		bDbgSpawnPlane1a = FALSE
		IF DOES_ENTITY_EXIST(vehPlane1a)
			DELETE_VEHICLE(vehPlane1a)
		ENDIF
		IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		vDbgPlane1a = GET_ENTITY_COORDS(PS_Main.myVehicle)
		vDbgLastPlane1a = vDbgPlane1a
		vehPlane1a = CREATE_VEHICLE(STUNT, vDbgPlane2a, GET_ENTITY_HEADING(PS_Main.myVehicle))
		SET_ENTITY_COLLISION(vehPlane1a, FALSE)
		FREEZE_ENTITY_POSITION(vehPlane1a, TRUE)
	ELSE
		
	ENDIF

	IF bDbgSpawnPlane1b
		bDbgSpawnPlane1b = FALSE
		IF DOES_ENTITY_EXIST(vehPlane1b)
			DELETE_VEHICLE(vehPlane1b)
		ENDIF
		IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		vDbgPlane1b = GET_ENTITY_COORDS(PS_Main.myVehicle)
		vDbgLastPlane1b = vDbgPlane1b
		vehPlane1b = CREATE_VEHICLE(STUNT, vDbgPlane1b, GET_ENTITY_HEADING(PS_Main.myVehicle))
		SET_ENTITY_COLLISION(vehPlane1b, FALSE)
		FREEZE_ENTITY_POSITION(vehPlane1b, TRUE)
	ENDIF
	
	
	IF bDbgSpawnPlane2a
		bDbgSpawnPlane2a = FALSE
		IF DOES_ENTITY_EXIST(vehPlane2a)
			DELETE_VEHICLE(vehPlane2a)
		ENDIF
		IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		vDbgPlane2a = GET_ENTITY_COORDS(PS_Main.myVehicle)
		vDbgLastPlane2a = vDbgPlane2a
		vehPlane2a = CREATE_VEHICLE(STUNT, vDbgPlane2a, GET_ENTITY_HEADING(PS_Main.myVehicle))
		SET_ENTITY_COLLISION(vehPlane2a, FALSE)
		FREEZE_ENTITY_POSITION(vehPlane2a, TRUE)
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vDbgPlane1a)
		IF NOT ARE_VECTORS_EQUAL(vDbgPlane1a, vDbgLastPlane1a)
			vDbgLastPlane1a = vDbgPlane1a
			IS_VEHICLE_FUCKED(vehPlane1b)
			SET_ENTITY_COORDS(vehPlane1a, vDbgPlane1a)
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vDbgPlane1b)
		IF NOT ARE_VECTORS_EQUAL(vDbgPlane1b, vDbgLastPlane1b)
			vDbgLastPlane1b = vDbgPlane1b
			IS_VEHICLE_FUCKED(vehPlane1b)
			SET_ENTITY_COORDS(vehPlane1b, vDbgPlane1b)
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vDbgPlane2a)
		IF NOT ARE_VECTORS_EQUAL(vDbgPlane2a, vDbgLastPlane2a)
			vDbgLastPlane2a = vDbgPlane2a
			IS_VEHICLE_FUCKED(vehPlane2a)
			SET_ENTITY_COORDS(vehPlane2a, vDbgPlane2a)
		ENDIF
	ENDIF
//vDbgLastPlane1a
//vDbgLastPlane1b 
//vDbgLastPlane2a 
ENDPROC

PROC PS_Tuner_Update_Player_Widget()
	
	//if we changed the planes position vector, then update the plane
	IF NOT ARE_VECTORS_EQUAL(vLastPlaneCoords, vCurPlaneCoords)
		IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
			SET_ENTITY_COORDS(PS_Main.myVehicle, vCurPlaneCoords)
			vLastPlaneCoords = vCurPlaneCoords
			IF bDbgAreCheckpointsShowing
				bDbgGenerateCheckpoints = TRUE //regenerate checkpoints on the fly
			ENDIF
		
		ENDIF
		
	ENDIF
	
	//if we changed the planes heading, then update the plane
	IF fDbgPlaneHeading <> fDbgLastPlaneHeading
		IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
			SET_ENTITY_HEADING(PS_Main.myVehicle, fDbgPlaneHeading)
			fDbgLastPlaneHeading = fDbgPlaneHeading
			IF bDbgAreCheckpointsShowing
				bDbgGenerateCheckpoints = TRUE //regenerate checkpoints on the fly
			ENDIF
		ENDIF
		
	ENDIF
	
	//did we toggle freeze plane?
	IF bDbgToggleFreezePlane
		IF bIsPlaneFrozen
			//do nothing
		ELSE
			//freeze plane
			IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				FREEZE_ENTITY_POSITION(PS_Main.myVehicle, TRUE)
				bIsPlaneFrozen = TRUE
			ENDIF
		ENDIF
	ELSE
		IF bIsPlaneFrozen
			//unfreeze plane
			IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
				bIsPlaneFrozen = FALSE
			ENDIF
		ELSE
			//do nothing
		ENDIF
	ENDIF
	
	//did we toggle player control?
	IF bDbgTogglePlayerControl
		IF bIsPlayerControlled
			//do nothing
		ELSE
			//turn on player control
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				bIsPlayerControlled = TRUE
			ENDIF
		ENDIF
	ELSE
		IF bIsPlayerControlled
			//turn off player control
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				bIsPlayerControlled = FALSE
			ENDIF
		ELSE
			//do nothing
		ENDIF
	ENDIF
ENDPROC

PROC PS_Tuner_Update_Stunt_Widget()
	PS_Tuner_Watch_Recording_Toggle()
	
	IF bDbgToggleUsePathDistance
		PS_Tuner_Calculate_Path_Nodes()
		bDbgToggleUsePathDistance = FALSE
	ENDIF
		
	IF bDbgTogglePrintVectors
		PS_Tuner_Print_Path_Nodes()
		bDbgTogglePrintVectors = FALSE
	ENDIF
	
	

	IF bDbgToggleClearData
		bDbgToggleClearData = FALSE
		CLEAR_NAMED_DEBUG_FILE(sStuntTunerFilePath, sStuntTunerFileName)
		DEBUG_MESSAGE("********************CLEARING STUNT TUNER DATA*************************")
	ENDIF
ENDPROC

PROC PS_Tuner_Draw_Path_Nodes()
	INT i = 0
	REPEAT iNumPathNodes i 
		DRAW_DEBUG_CROSS(vOffsetVectors[i], 5, 255, 0, 0)
		IF i < iNumPathNodes - 1
			DRAW_DEBUG_LINE(vOffsetVectors[i], vOffsetVectors[i+1])
		ELIF i = iNumPathNodes - 1
			DRAW_DEBUG_LINE(vOffsetVectors[i], vOffsetVectors[0])
		ENDIF
	ENDREPEAT
ENDPROC

PROC PS_Tuner_Update_Skydiving_Widget()
	IF bDbgToggleDrawPathNodes
		PS_Tuner_Calculate_Path_Nodes()
		PS_Tuner_Draw_Path_Nodes()
	ENDIF
ENDPROC

PROC PS_Tuner_Update_Widget()
	
	//updates widget shit to do with the player & plane
	PS_Tuner_Update_Player_Widget()
	
	//updates widget shit for stunts (get readouts of barrel roll, inside loop, etc)
	PS_Tuner_Update_Stunt_Widget()
	
	//updates widget shit for formation course (setup checkpoints, adjust formation spacing)
	PS_Tuner_Update_Formation_Widget()
	
	//updates widget shit for skydiving plane (create cargoplane autopilot path)
	PS_Tuner_Update_Skydiving_Widget()
	

ENDPROC




FUNC BOOL PS_Tuner_MainUpdate()
	

	IF TempDebugInput() AND NOT bFinishedChallenge
		IF bIsRecording
			PS_Tuner_Watch_Recording_Toggle()
			PS_Tuner_Fill_Data()
			PS_Tuner_Update_Recording()
		ELSE
			PS_UPDATE_CALCULATIONS()
			PS_HUD_UPDATE()
			PS_Tuner_Update_Widget()
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

	
ENDFUNC

PROC PS_TUNER_FORCE_FAIL()
	bFinishedChallenge = TRUE
ENDPROC

PROC PS_TUNER_FORCE_PASS()
	bFinishedChallenge = TRUE
ENDPROC


PROC PS_Tuner_MainCleanup()

	PS_Stunt_Tuner_Cleanup()

ENDPROC


#ENDIF
//debug build wrap
