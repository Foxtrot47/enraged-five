//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/13/11
//	Description:	"Helicopter Speed Course" Challenge for Pilot School.
//	
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

//CONSTS
CONST_INT MAX_HELISPEEDRUN_CHECKPOINTS  17
CONST_INT SOME_CHECKPOINT				4
CONST_INT SOME_OTHER_CHECKPOINT				7
CONST_INT PS_HELISPEED_LANDING_CHECKPOINT 16
CONST_INT PS_HELISPEED_NOCOMMENT_1		(PS_HELISPEED_LANDING_CHECKPOINT - 1)
CONST_INT PS_HELISPEED_NOCOMMENT_2		(PS_HELISPEED_LANDING_CHECKPOINT - 2)
CONST_INT PS_HELISPEED_NOCOMMENT_3		(PS_HELISPEED_LANDING_CHECKPOINT - 3)

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Inverted Flight
//****************************************************************************************************
//****************************************************************************************************

PROC  PS_Heli_Speed_Init_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<2587.4009, 574.9205, 220.0000>>)//1
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<2575.4661, 184.5000, 180.0000>>)//2
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<2373.0669, -286.2998, 140.0000>>)//3
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<2004.5393, -676.9011, 140.0000>>)//4
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<1389.7465, -1088.1499, 120.0000>>)//5
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<1060.7111, -1434.2180, 120.0000>>)//6
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<1201.0780, -1944.7361, 120.0000>>)//7
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<1319.4810, -2430.7410, 120.0000>>)//8
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<800.9724, -2624.5081, 100.0000>>)//9
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<361.7378, -2668.9270, 80.0000>>)//10
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-85.8033, -2554.7871, 80.0000>>)//11
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-267.8112, -2428.5950, 97.0000>>)//12
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-547.2916, -2230.8440, 97.0000>>)//13
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-794.8572, -2211.2461, 75.0000>>)//14
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-1061.4908, -2581.5049, 75.0000>>)//15
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FINISH, <<-1145.2367, -2864.8611, 15.0426>>)//16
ENDPROC

PROC PS_Heli_Speed_Update_Dialogue()
	IF PS_IS_CHECKPOINT_MGR_ACTIVE() AND PS_IS_WAITING_FOR_CHECKPOINT_FEEDBACK()
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			SWITCH(PS_GET_CHECKPOINT_PROGRESS())
				CASE 0
					//do nothing
					BREAK
				CASE 1
					//do nothing
					BREAK
				CASE SOME_CHECKPOINT
					//if the player missed the check point, we don't want to congratulate him.
					IF PS_GET_LAST_CHECKPOINT_SCORE() <> PS_CHECKPOINT_MISS
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_HSR", "PS_HSR_5")//"Good work so far. Keep her at full speed!"
					ENDIF
					BREAK
				CASE SOME_OTHER_CHECKPOINT
					//if the player missed the check point, we don't want to congratulate him.
					IF PS_GET_LAST_CHECKPOINT_SCORE() <> PS_CHECKPOINT_MISS
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_HSR", "PS_HSR_6")//"That's it! You are blazing it now.  "
					ENDIF
					BREAK
				CASE PS_HELISPEED_NOCOMMENT_3
				CASE PS_HELISPEED_NOCOMMENT_2
				CASE PS_HELISPEED_NOCOMMENT_1
					//do nothing
					BREAK
				CASE PS_HELISPEED_LANDING_CHECKPOINT
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_HSR", "PS_HSR_7")//"Bring the helicopter down gently onto the helipad."
					BREAK
				DEFAULT
					IF PS_GET_RANDOM_CHANCE()
						IF PS_GET_LAST_CHECKPOINT_SCORE() = PS_CHECKPOINT_MISS
							PS_PLAY_RANDOM_FEEDBACK_DIALOGUE(PS_GET_LAST_CHECKPOINT_SCORE())
						ELSE
							PS_PLAY_RANDOM_FEEDBACK_DIALOGUE(PS_CHECKPOINT_AWESOME) //since we dont have stunts, we always want rewarding feedback for passing these checkpoints
						ENDIF
					ENDIF
					PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
					BREAK
			ENDSWITCH
		ELSE
			PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Heli_Speed_Fail_Check()
	
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_Heli_Speed_State)
			CASE PS_HELI_SPEED_INIT
				RETURN FALSE
				
			CASE PS_HELI_SPEED_COUNTDOWN
				RETURN FALSE
				
			CASE PS_HELI_SPEED_TAKEOFF
//				IF PS_HUD_GET_HOURGLASS_TIMER_VALUE() <= 0
//					ePSFailReason = PS_FAIL_TIME_UP
//					RETURN TRUE
//				ENDIF
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
					RETURN TRUE
				ENDIF
				BREAK
				
			DEFAULT
				IF PS_HUD_GET_HOURGLASS_TIMER_VALUE() <= 0
					ePSFailReason = PS_FAIL_TIME_UP
					RETURN TRUE
				ENDIF
				//Do default checks
				IF PS_GET_CHECKPOINT_PROGRESS() = PS_GET_TOTAL_CHECKPOINTS()
					//make sure we don't fail the player for landing
					IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE)
						RETURN TRUE
					ENDIF
				ELSE
					IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    When it returns TRUE, the main loop will increment the challenge state/stage
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Heli_Speed_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_Heli_Speed_State)
			CASE PS_HELI_SPEED_INIT
				RETURN FALSE
				BREAK

			CASE PS_HELI_SPEED_COUNTDOWN
				RETURN FALSE
				BREAK
				
			CASE PS_HELI_SPEED_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_HELI_SPEED_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK		
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE	
ENDFUNC

PROC PS_HELI_SPEED_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_Heli_Speed_State = PS_HELI_SPEED_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_HELI_SPEED_FORCE_PASS
	PS_Heli_Speed_State = PS_HELI_SPEED_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Heli_Speed_Initialise()
	//Start pos and heading are hard-coded since it different for each challenge.
	vStartPosition =<< 2544.7241, 884.7112, 230.0 >>// << 2833.1814, 1042.1832, 361.8937 >>
	vStartRotation = <<0, 0, 180>>
	fStartHeading = 220
	iFailDialogue = 1
	bLessonCompleted = FALSE
	bLesonFinishedLineTriggered	= FALSE
	
	//vPreviewCamCoords = <<1342.4, -2385.5, 152.2>>
	//vPreviewCamDirection = NORMALISE_VECTOR(<<1255.5, -2554.0, 48.0>> - vPreviewCamCoords)
	
	iInstructionCounter = 1
	bFinishedChallenge = FALSE
	VehicleToUse = FROGGER
	
	PS_Heli_Speed_Init_Checkpoints()

	PS_SETUP_CHALLENGE()
	PS_REQUEST_SHARED_ASSETS()
	
	iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_HELI_SPEED
	REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		WAIT(0)
	ENDWHILE	
	
	fPreviewVehicleSkipTime = 76000
	fPreviewTime = 17
	
	vPilotSchoolSceneCoords = vStartPosition
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
		vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
		
		//hardcoding where to place the frustum in this lesson, since the bridge isn't immediately in the camera view at the start.
		//vPilotSchoolPreviewCoords = << 1387.3, -2502.8, 182.0 >>
		//vPilotSchoolPreviewRot = << -25.6102, 23.0560, 104.3196 >>
	ENDIF
	
	scenBlockHelipad = ADD_SCENARIO_BLOCKING_AREA( <<-1177.8660, -2802.9944, 11.3277>>, <<-1066.8466, -2953.8477, 16.1229>>)
	CLEAR_AREA(<<-1143.9893, -2865.0439, 12.9484>>, 25.0, TRUE, TRUE)
	
	//Offset used to check if player is in the air or on the ground
	PS_SET_GROUND_OFFSET(1.0)
	//Spawn player vehicle				
	PS_CREATE_VEHICLE()

	SET_VEHICLE_RADIO_ENABLED(PS_Main.myVehicle, FALSE)
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Heli_Speed.sc******************")
	#ENDIF
ENDPROC

PROC PS_Heli_Speed_MainSetup()
	PS_Heli_Speed_Init_Checkpoints()
	IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
		SET_ENTITY_COORDS(PS_Main.myVehicle, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, 1500, "PilotSchool"))
		SET_ENTITY_ROTATION(PS_Main.myVehicle, GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, 1500, "PilotSchool"))
		FREEZE_ENTITY_POSITION(PS_Main.myVehicle, TRUE)
	ENDIF
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	
	NEW_LOAD_SCENE_START(<<2538.5901, 889.7548, 262.7056>>, NORMALISE_VECTOR(<<2544.7478, 882.0132, 254.3920>> - <<2538.5901, 889.7548, 262.7056>>), 5000.0)
ENDPROC

FUNC BOOL PS_Heli_Speed_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				//LOAD_SCENE(vPilotSchoolPreviewCoords)
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_HELI, iPreviewCheckpointIdx + 1, TRUE)
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV8", "PS_PREV8_1", "PS_PREV8_2", "PS_PREV8_3")
				//"This lesson utilizes the helicopter, with an emphasis on speed."
				//"Finish the obstacle course around the city before the timer runs out and try not to crash."
				//"At the very end of the course, you'll be required to safely land your helicopter."				
				BREAK
			CASE PS_PREVIEW_PLAYING
				//let vehicle recording play
				BREAK
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC

FUNC BOOL PS_Heli_Speed_MainUpdate()
	IF TempDebugInput() AND NOT bFinishedChallenge
		
		PS_LESSON_UDPATE()
		
		SWITCH(PS_Heli_Speed_State)
		
		//Spawn player and helicopter on the runway
			CASE PS_HELI_SPEED_INIT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//Setup vars and vehicle
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle) AND NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(PS_Main.myVehicle, VS_DRIVER))
							SET_ENTITY_COORDS(PS_Main.myVehicle, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, 1500, "PilotSchool"))
							SET_ENTITY_ROTATION(PS_Main.myVehicle, GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, 1500, "PilotSchool"))
							TASK_VEHICLE_MISSION_COORS_TARGET(GET_PED_IN_VEHICLE_SEAT(PS_Main.myVehicle, VS_DRIVER), PS_Main.myVehicle, PS_GET_CHECKPOINT_FROM_ID(1), MISSION_GOTO, 70.0, DRIVINGMODE_PLOUGHTHROUGH, 0, 100)
						ENDIF
						PS_Heli_Speed_State = PS_HELI_SPEED_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

		//Play countdown
			CASE PS_HELI_SPEED_COUNTDOWN
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//Update countdown
						IF	PS_HUD_UPDATE_COUNTDOWN()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
						PS_Heli_Speed_State = PS_HELI_SPEED_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Give player control and the first instruction to take off
			CASE PS_HELI_SPEED_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//Setup objective, give the player controls and manage any new help text
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_HELI)
						PS_PLAY_DISPATCHER_INSTRUCTION_3_LINES("PS_HSR", "PS_HSR_1", "PS_HSR", "PS_HSR_2", "PS_HSR", "PS_HSR_3") 
						//"Remember, this course is all about speed!"
						//"To maximize acceleration, keep the nose of the helicopter tipped."
						//"Just bear in mind that you're also losing altitude when the nose is pointed down. "
						
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Heli_Speed_Fail_Check()
							PS_Heli_Speed_State = PS_HELI_SPEED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Heli_Speed_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_SET_CHECKPOINT_COUNTER_ACTIVE(TRUE)
						PS_HUD_RESET_TIMER() //keep this for our hourglass
						PS_START_HOURGLASS_TIMER(FLOOR(PS_Challenges[PSC_heliSpeedRun].BronzeTime * 1000))
						PS_SET_TIMER_ACTIVE(TRUE)
						PS_Heli_Speed_State = PS_HELI_SPEED_OBSTACLE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				
				BREAK
		
	
		//Obstacle course loop
			CASE PS_HELI_SPEED_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_KILL_HINT_CAM()
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Heli_Speed_Fail_Check()
							PS_Heli_Speed_State = PS_HELI_SPEED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Heli_Speed_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELSE
							PS_Heli_Speed_Update_Dialogue()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_UPDATE_RECORDS(PS_GET_LAST_CHECKPOINT())
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_Heli_Speed_State = PS_HELI_SPEED_SUCCESS
						ELSE
							PS_Heli_Speed_State = PS_HELI_SPEED_FAIL
						ENDIF
						bLessonCompleted = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
												
			CASE PS_HELI_SPEED_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							PS_UPDATE_RECORDS(PS_GET_LAST_CHECKPOINT())
							PS_PLAY_LESSON_FINISHED_LINE("PS_HSR", "PS_HSR_8", "PS_HSR_9", "PS_HSR_10", "PS_HSR_11")
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
							PS_LESSON_END(TRUE)
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_HELI_SPEED_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
							ELSE
								PS_UPDATE_RECORDS(PS_GET_LAST_CHECKPOINT())
								IF ( bLessonCompleted = TRUE )
									PS_PLAY_LESSON_FINISHED_LINE("PS_HSR", "PS_HSR_8", "PS_HSR_9", "PS_HSR_10", "PS_HSR_11")
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_HSR", "PS_HSR_8", "PS_HSR_9", "PS_HSR_10", "")
								ENDIF
								iEndDialogueTimer = GET_GAME_TIMER()
								bLesonFinishedLineTriggered = TRUE
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		ENDSWITCH
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


PROC PS_Heli_Speed_MainCleanup()
	PS_SEND_PLAYER_BACK_TO_MENU()
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	CLEAR_PRINTS()
	CLEAR_HELP()
	
	IF (scenBlockHelipad != NULL)
		REMOVE_SCENARIO_BLOCKING_AREA(scenBlockHelipad)
		scenBlockHelipad = NULL
	ENDIF
	
	//cleanup checkpoints
	PS_RESET_CHECKPOINT_MGR()
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Heli_Speed.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************
