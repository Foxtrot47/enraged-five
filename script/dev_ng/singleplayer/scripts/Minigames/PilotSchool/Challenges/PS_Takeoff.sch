//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/08/11
//	Description:	"Upside-Down Flight" challenge for Pilot School. The player is told to do a couple stunts,
//					including barrel rolls and holding the plane upside down for a short duration.
//	
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Takeoff Flight
//****************************************************************************************************
//****************************************************************************************************

PROC  PS_Challenge_Takeoff_Init_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1286.3641, -2124.3770, 62>>)	
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FINISH, <<-1065.9170, -1742.3917, 70>>)
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Challenge_Takeoff_Fail_Check()
	LANDING_GEAR_STATE lgstate
	
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_Takeoff_State)
			CASE PS_TAKEOFF_INIT
				//No fail check to do
				RETURN FALSE
				BREAK
				
			CASE PS_TAKEOFF_THROTTLE
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, TRUE, FALSE)
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_TAKEOFF_LIFT_OFF
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, TRUE, FALSE)
					RETURN TRUE
				ENDIF
				BREAK
			
			CASE PS_TAKEOFF_RETRACT_GEAR
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE)
					RETURN TRUE
				ENDIF
				IF PS_GET_CLEARED_CHECKPOINTS() > 0 // if we cleared at least one checkpoint.
					lgstate = GET_LANDING_GEAR_STATE(PS_Main.myVehicle)
					IF lgstate = LGS_LOCKED_DOWN OR lgstate = LGS_DEPLOYING OR lgstate = LGS_BROKEN
						DEBUG_MESSAGE("FAILED BC PLAYER DIDNT RETRACT LANDING GEAR")
						ePSFailReason = PS_FAIL_LANDING_GEAR
						RETURN TRUE
					ENDIF
				ENDIF
				BREAK
				
			CASE PS_TAKEOFF_FLY_OUT
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main)
					RETURN TRUE
				ENDIF
				lgstate = GET_LANDING_GEAR_STATE(PS_Main.myVehicle)
				IF lgstate = LGS_LOCKED_DOWN OR lgstate = LGS_DEPLOYING OR lgstate = LGS_BROKEN
					DEBUG_MESSAGE("FAILED BC PLAYER DIDNT RETRACT LANDING GEAR")
					ePSFailReason = PS_FAIL_LANDING_GEAR
					RETURN TRUE
				ENDIF
				BREAK
			
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main)
					RETURN TRUE
				ENDIF
				lgstate = GET_LANDING_GEAR_STATE(PS_Main.myVehicle)
				IF lgstate = LGS_LOCKED_DOWN OR lgstate = LGS_DEPLOYING OR lgstate = LGS_BROKEN
					DEBUG_MESSAGE("FAILED BC PLAYER DIDNT RETRACT LANDING GEAR")
					ePSFailReason = PS_FAIL_LANDING_GEAR
					RETURN TRUE
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    When it returns TRUE, the main loop will increment the challenge state/stage
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Challenge_Takeoff_Progress_Check()
	LANDING_GEAR_STATE lgstate
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_Takeoff_State)
			CASE PS_TAKEOFF_THROTTLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							//is the player up to speed?
							IF GET_ENTITY_SPEED(PS_Main.myVehicle) > 20
								RETURN TRUE
							ENDIF
						ELSE
							PRINTLN("Waiting for IS_ANY_CONVERSATION_ONGOING_OR_QUEUED to return false")
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
								
			CASE PS_TAKEOFF_LIFT_OFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							//is the player lifting off?
							IF IS_ENTITY_IN_AIR(PS_Main.myVehicle)
								RETURN TRUE
							ENDIF
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_TAKEOFF_RETRACT_GEAR
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							//is the player lifting off?
							lgstate = GET_LANDING_GEAR_STATE(PS_Main.myVehicle)
							IF lgstate = LGS_LOCKED_UP
								RETURN TRUE
							ENDIF
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_TAKEOFF_FLY_OUT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//is the player at the last checkpoint?
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
						
		ENDSWITCH
	ENDIF
	//only true when we're progressing
	RETURN FALSE
ENDFUNC

PROC PS_TAKEOFF_FORCE_FAIL
	ePSFailReason 		= PS_FAIL_DEBUG
	PS_Takeoff_State 	= PS_TAKEOFF_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_TAKEOFF_FORCE_PASS
	PS_Takeoff_State 	= PS_TAKEOFF_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_TAKEOFF_J_SKIP()
		PS_TAKEOFF_FLIGHT_CHALLENGE nextState = PS_Takeoff_State
		IF PS_Takeoff_State > PS_TAKEOFF_COUNTDOWN
			nextState = PS_TAKEOFF_FLY_OUT			
			
			IF nextState != PS_Takeoff_State AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				//send plane to the takeoff checkpoint
				CONTROL_LANDING_GEAR(PS_Main.myVehicle, LGC_RETRACT_INSTANT)
				SET_ENTITY_COORDS(PS_Main.myVehicle, PS_GET_CHECKPOINT_FROM_ID(1))
				SET_VEHICLE_FORWARD_SPEED(PS_Main.myVehicle, 60.0)
				PS_Takeoff_State = nextState
				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
				RETURN TRUE
			ENDIF	
		ENDIF
		RETURN FALSE
	ENDFUNC
#ENDIF

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Takeoff_Initialise()
	PS_Challenge_Takeoff_Init_Checkpoints()
	
	//init vars
	PS_Takeoff_State 							= PS_TAKEOFF_INIT
	iPS_PreviewVehicleRecordingID 				= iPS_PEVIEW_VEH_REC_ID_TAKEOFF
	vStartPosition								= <<-1627.5929, -2715.2949, 12.9445>>
	vStartRotation 								= <<8.804, 0.0, 330.0000>>
	fStartHeading 								= 330.00
	iPSDialogueCounter							= 1
	iPSHelpTextCounter							= 1
	iPSObjectiveTextCounter						= 1
	iFailDialogue								= 1
	bLessonCompleted							= FALSE
	bLesonFinishedLineTriggered					= FALSE
	
	//player vehicle model
	VehicleToUse = CUBAN800
	PS_SET_GROUND_OFFSET(1.5)
	
	PS_SETUP_CHALLENGE()	
	
	PS_REQUEST_SHARED_ASSETS()
	
	//load assets
	REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		PRINTLN("Flight School - Waiting on assets to load")
		WAIT(0)
	ENDWHILE
	
	//preview stuff
	fPreviewVehicleSkipTime 					= 5000
	fPreviewTime								= 12
	vPilotSchoolSceneCoords = vStartPosition
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
		vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
	ENDIF
	
	//Spawn player vehicle			
	PS_CREATE_VEHICLE()
	IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SET_VEHICLE_COLOUR_COMBINATION(PS_Main.myVehicle, 2)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Takeoff.sc******************")
	#ENDIF
	
ENDPROC

FUNC BOOL PS_Takeoff_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				IF DOES_CAM_EXIST(camMainMenu)
//					DESTROY_CAM(camMainMenu)
//				ENDIF
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT, iPreviewCheckpointIdx, TRUE)	
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV01", "PS_PREV01_1", "PS_PREV01_2")
				//"Welcome to Flight School. My name's Jackson with a J and I'll be your instructor."
				//"First things first, let's see if we can get you into the air without killing yourself or someone else."
				BREAK
			CASE PS_PREVIEW_PLAYING
//				PS_WATCH_LANDING_GEAR_STATE()
				IF IS_ENTITY_IN_AIR(PS_Main.myVehicle) AND GET_LANDING_GEAR_STATE(PS_Main.myVehicle) = LGS_LOCKED_DOWN
					CONTROL_LANDING_GEAR(PS_Main.myVehicle, LGC_RETRACT)
				ENDIF
				//let vehicle recording play
				BREAK
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PS_Takeoff_MainSetup()
	PRINTLN("Setting initial flying ability stat")
	INT iStatValue
	IF STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), iStatValue)
		IF iStatValue < (PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON() - 10)
			STAT_SET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), (PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()-10))
		ENDIF
	ENDIF
	PS_Challenge_Takeoff_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	
	NEW_LOAD_SCENE_START(<<-1637.5970, -2732.6311, 22.5273>>, NORMALISE_VECTOR(<<-1634.7294, -2727.6628, 20.9882>> - <<-1637.5970, -2732.6311, 22.5273>>), 5000.0)
ENDPROC

FUNC BOOL PS_Takeoff_MainUpdate()
	IF TempDebugInput() AND NOT bFinishedChallenge
		
		#IF IS_DEBUG_BUILD
			IF PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
				IF PS_TAKEOFF_J_SKIP()
					RETURN TRUE //if we do a j skip, then skip an update
				ENDIF
			ENDIF
		#ENDIF
		
		PS_LESSON_UDPATE()
		
		SWITCH(PS_Takeoff_State)
			CASE PS_TAKEOFF_INIT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Takeoff_State = PS_TAKEOFF_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		
			CASE PS_TAKEOFF_COUNTDOWN
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Takeoff_State = PS_TAKEOFF_THROTTLE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_TAKEOFF_THROTTLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_TAKEOFF_THROTTLE state")
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT)
						QUEUE_OBJHELPALOGUE_HELP("PS_TAKEOFF_A", -1, 0.5)
						PLAY_OBJHELPALOGUE()
						//PS_PLAY_DISPATCHER_INSTRUCTION("PS_TAKEOFF", "PS_TAKEOFF_1")	//play 3 lines of dialogue to tell player what to do, see B*1965219
						PS_PLAY_DISPATCHER_INSTRUCTION_3_LINES("PS_TAKEOFF", "PS_TAKEOFF_1", "PS_TAKEOFF", "PS_TAKEOFF_2", "PS_TAKEOFF", "PS_TAKEOFF_3")
						//"We're going to start with a basic takeoff."
						
						
						//hud stuff
						PS_HUD_RESET_TIMER()
						PS_SET_TIMER_ACTIVE(TRUE)
						
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Takeoff_Fail_Check()
							PS_Takeoff_State = PS_TAKEOFF_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Takeoff_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PRINTLN("[Flight School] >>>>>>>>>>> Exiting PS_TAKEOFF_THROTTLE state")
						CLEAR_HELP(TRUE)
						PS_Takeoff_State = PS_TAKEOFF_LIFT_OFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_TAKEOFF_LIFT_OFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_TAKEOFF_LIFT_OFF state")
						//IF IS_PC_VERSION()
							CLEAR_HELP(TRUE)
							PRINT_HELP("PS_TAKEOFF_B")
						//ELSE
						//	QUEUE_OBJHELPALOGUE_HELP("PS_TAKEOFF_B", -1, 1.0)
						//	PLAY_OBJHELPALOGUE()
						//ENDIF
						//PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_TAKEOFF", "PS_TAKEOFF_2", "PS_TAKEOFF", "PS_TAKEOFF_3")
						//"Hold the throttle all the way down to pick up speed."
						//"And pull back the flight stick to take off."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Takeoff_Fail_Check()
							PS_Takeoff_State = PS_TAKEOFF_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Takeoff_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PRINTLN("[Flight School] >>>>>>>>>>> Exiting PS_TAKEOFF_LIFT_OFF state")
						CLEAR_HELP(FALSE)
						PS_Takeoff_State = PS_TAKEOFF_RETRACT_GEAR
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_TAKEOFF_RETRACT_GEAR
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_TAKEOFF_RETRACT_GEAR state")
						IF PS_IS_LANDING_GEAR_DOWN()
							QUEUE_OBJHELPALOGUE_HELP("PS_TAKEOFF_C", -1, 0.5)
							PLAY_OBJHELPALOGUE()
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_TAKEOFF", "PS_TAKEOFF_4")
							//"You can retract your landing gear once you're a safe distance above the ground."
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Takeoff_Fail_Check()
							PS_Takeoff_State = PS_TAKEOFF_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Takeoff_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PRINTLN("[Flight School] >>>>>>>>>>> Exiting PS_TAKEOFF_RETRACT_GEAR state")
						CLEAR_HELP(FALSE)
						PS_Takeoff_State = PS_TAKEOFF_FLY_OUT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_TAKEOFF_FLY_OUT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_TAKEOFF_FLY_OUT state")
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_TAKEOFF", "PS_TAKEOFF_5","PS_TAKEOFF", "PS_TAKEOFF_7")
						//Congratulations, pilot! You are now airborne!
						//Now navigate through those checkpoints marked on your radar and we can move on to the next lesson.
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Takeoff_Fail_Check()
							PS_Takeoff_State = PS_TAKEOFF_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Takeoff_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PRINTLN("[Flight School] >>>>>>>>>>> Exiting PS_TAKEOFF_FLY_OUT state")
						PS_Takeoff_State = PS_TAKEOFF_SUCCESS
						bLessonCompleted = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_TAKEOFF_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_TAKEOFF_SUCCESS state")
						IF ( bLesonFinishedLineTriggered = FALSE )
							PS_UPDATE_RECORDS(VECTOR_ZERO)
							iFailDialogue = GET_RANDOM_INT_IN_RANGE(1, 4)
							PS_PLAY_LESSON_FINISHED_LINE("PS_TAKEOFF", "PS_TAKEOFF_8", "PS_TAKEOFF_8", "PS_TAKEOFF_8", "PS_TAKEOFF_9", "PS_TAKEOFF_10", "PS_TAKEOFF_11")
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
							PS_LESSON_END(TRUE)
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_TAKEOFF_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_TAKEOFF_FAIL state")
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
							ELSE
								PS_UPDATE_RECORDS(VECTOR_ZERO)
								IF ( bLessonCompleted = TRUE )
									PS_PLAY_LESSON_FINISHED_LINE("PS_TAKEOFF", "PS_TAKEOFF_8", "PS_TAKEOFF_8", "PS_TAKEOFF_8", "PS_TAKEOFF_9", "PS_TAKEOFF_10", "PS_TAKEOFF_11")
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_TAKEOFF", "PS_TAKEOFF_8", "PS_TAKEOFF_8", "PS_TAKEOFF_8", "")
								ENDIF
								iEndDialogueTimer = GET_GAME_TIMER()
								bLesonFinishedLineTriggered = TRUE
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_Takeoff_MainCleanup() 
//take care of hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	
//take care of objects
	//player first
	PS_SEND_PLAYER_BACK_TO_MENU()
	//then vehicle
	
	//then checkpoints
	PS_RESET_CHECKPOINT_MGR()

//loaded assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(STUNT)	
	
//done cleaning up
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************PS_Takeoff: done cleaning up lesson ******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************
