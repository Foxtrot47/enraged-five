//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/13/11
//	Description:	"Fly Low" challenge for Pilot School. This challenges requires the player to complete an
//					obstacle course while maintaining a low altitude. The player can fail by falling in the water,
//					flying too high, or running out of time before getting to the last checkpoint.
//	
//****************************************************************************************************


USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

//CONSTS
CONST_INT						MAX_FLYLOW_CHECKPOINTS 			20
CONST_FLOAT						PS_FLY_LOW_MAX_ALTITUDE			50.0
VECTOR 							vHeightCheck
structTimer 					Fail_Timer_Altitude

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Inverted Flight
//****************************************************************************************************
//****************************************************************************************************

PROC  PS_Fly_Low_Init_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-957.4382, -3362.3669, 32.6549>>)//0
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-723.2173, -3497.1792, 30.4632>>)//1
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-452.3100, -3558.2610, 15.0000>>)//2
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-89.7333, -3585.2371, 15.0000>>)//3
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<296.5815, -3582.7891, 15.0000>>)//4
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<654.1344, -3565.4080, 15.0000>>)//5
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1050.1160, -3546.3569, 15.0000>>)//6
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1380.2960, -3464.2419, 15.0000>>)//7
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1588.4780, -3221.4009, 15.0000>>)//8
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1555.9900, -2950.9570, 15.0000>>)//9
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1363.2560, -2782.5410, 15.0000>>)//10
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1072.9810, -2704.2080, 15.0000>>)//11
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<851.7050, -2774.2720, 15.0000>>)//12
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<689.5747, -2910.5229, 30.9228>>)//13
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<475.6135, -3006.5000, 35.0000>>)//14
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<383.1139, -3130.4751, 15.0000>>)//15
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<318.2879, -3475.4753, 15.0000>>)//16
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<14.5475, -3788.1760, 15.0000>>)//17
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-424.7075, -3672.2520, 38.4119>>)//18
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FINISH, <<-963.2404, -3358.9646, 45.6356>>)//19//<<-1899.6328, -2707.1082, 40>>
ENDPROC

PROC PS_Fly_Low_Update_Dialogue()
	IF PS_IS_CHECKPOINT_MGR_ACTIVE() AND PS_IS_WAITING_FOR_CHECKPOINT_FEEDBACK() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		SWITCH(PS_GET_CHECKPOINT_PROGRESS())
			CASE 0
				//do nothing
				BREAK
			CASE 1
				//do nothing
				BREAK
			DEFAULT
				IF PS_GET_RANDOM_CHANCE()
					IF PS_GET_LAST_CHECKPOINT_SCORE() = PS_CHECKPOINT_MISS
						PS_PLAY_RANDOM_FEEDBACK_DIALOGUE(PS_GET_LAST_CHECKPOINT_SCORE())
					ELSE
						PS_PLAY_RANDOM_FEEDBACK_DIALOGUE(PS_CHECKPOINT_AWESOME) //since we dont have stunts, we always want rewarding feedback for passing these checkpoints
					ENDIF
				ENDIF
				PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
				BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the player is above PS_FLY_LOW_MAX_ALTITUDE. If he is, return true and start the fail timer
/// RETURNS:
///    TRUE if the player is above the altitude, otherwise false
FUNC BOOL PS_Fly_Low_Player_Is_Too_High() 
	IF PS_HUD_GET_ALTIMETER() > PS_FLY_LOW_MAX_ALTITUDE
		
		PRINTLN("Altimeter is reporting...", PS_HUD_GET_ALTIMETER(), "; player height is...", GET_ENTITY_HEIGHT(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE, TRUE), "; and player height from ground is...", GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID()))
		IF NOT IS_TIMER_STARTED(Fail_Timer_Altitude)
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_5", TRUE) //"You are flying too high!"
			START_TIMER_NOW(Fail_Timer_Altitude)
			RETURN TRUE
		ENDIF
		RETURN TRUE			
	ENDIF
	IF IS_TIMER_STARTED(Fail_Timer_Altitude)
		CANCEL_TIMER(Fail_Timer_Altitude)
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Watches the fail timer for the player being above PS_FLY_LOW_MAX_ALTITUDE
/// RETURNS:
///    TRUE once the timer is up, otherwise false
FUNC BOOL PS_Fly_Low_Is_Altitude_Timer_Up()
	IF IS_TIMER_STARTED(Fail_Timer_Altitude)
		IF TIMER_DO_ONCE_WHEN_READY(Fail_Timer_Altitude, 5)
			RETURN TRUE			
		ENDIF
	ELSE
		SCRIPT_ASSERT("Altitude fail timer is not running when it should be!")
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_FLY_LOW_UPDATE_HELP_MESSAGES()

//	IF TIMERA() > 1000
//		//do nothing... delay before hep text is shown
//	ELIF TIMERA() > 1000 AND TIMERA() < 6000	
//		SET_FLOATING_HELP_TEXT_SCREEN_POSITION(FLOATING_HELP_TEXT_ID_1, 0.15, 0.75)
//		SET_FLOATING_HELP_TEXT_STYLE(FLOATING_HELP_TEXT_ID_1, HELP_MESSAGE_STYLE_TAGGABLE, HUD_COLOUR_BLACK, 255, HELP_TEXT_SOUTH)
//		DISPLAY_FLOATING_HELP_TEXT(FLOATING_HELP_TEXT_ID_1, "PFLYLOW_B")
//	ELIF TIMERA() > 6000 AND TIMERA() < 13500
//		SET_FLOATING_HELP_TEXT_SCREEN_POSITION(FLOATING_HELP_TEXT_ID_1, 0.15, 0.75)
//		SET_FLOATING_HELP_TEXT_STYLE(FLOATING_HELP_TEXT_ID_1, HELP_MESSAGE_STYLE_TAGGABLE, HUD_COLOUR_BLACK, 255, HELP_TEXT_SOUTH)
//		DISPLAY_FLOATING_HELP_TEXT(FLOATING_HELP_TEXT_ID_1, "PFLYLOW_B1")
//	ELIF TIMERA() > 13500
		IF iPSHelpTextCounter = 1 AND TIMERA() > 1000
			IF PS_PlayerData[g_current_selected_PilotSchool_class].eMedal <> PS_NONE
				iPSHelpTextCounter = 7
			ELSE
				iPSHelpTextCounter++
			ENDIF
		ELIF iPSHelpTextCounter = 2 AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			PRINT_HELP("PFLYLOW_F")//~s~Missing a checkpoint will result in a time penalty.
			iPSHelpTextCounter++
		ELIF iPSHelpTextCounter = 3 AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()	
			PRINT_HELP("PFLYLOW_B")//~s~The altitude indicator tracks height above sea level.		
			iPSHelpTextCounter++
		ELIF iPSHelpTextCounter = 4 AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			PRINT_HELP("PFLYLOW_B1")//~s~The vertical line going across represents the plane, and the red shaded area represents the maximum altitude.
			iPSHelpTextCounter++
		ELIF iPSHelpTextCounter = 5 AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			PRINT_HELP("PFLYLOW_D")//"~s~In order to make sharp turns, use ~PAD_LB~ or ~PAD_RB~ for control over your rudder."
			iPSHelpTextCounter++
		ELIF iPSHelpTextCounter = 6 AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//			PRINT_HELP("PFLYLOW_E")//"~s~If a checkpoint goes out of view, an arrow indicator pointing to it will appear in your peripheral view."
			iPSHelpTextCounter++
		ELIF iPSHelpTextCounter = 7 AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			PS_Fly_Low_Update_Dialogue()
		ENDIF
//	ENDIF
ENDPROC

/// PURPOSE:
///   Determines what fail conditions we need to check dependending on the state/stage of the challenge we're in
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Fly_Low_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_Fly_Low_State)
			CASE PS_FLY_LOW_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_FLY_LOW_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_FLY_LOW_TAKEOFF
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, TRUE, FALSE)
					RETURN TRUE
				ENDIF
				BREAK
			
			CASE PS_FLY_LOW_OBSTACLE_COURSE
				IF PS_Fly_Low_Player_Is_Too_High()
					IF PS_Fly_Low_Is_Altitude_Timer_Up()	
						DEBUG_MESSAGE("PLAYER FAILED BECAUSE ALTITUDE TIMER IS UP")
//						iFailDialogue = 2
						ePSFailReason = PS_FAIL_TOO_HIGH
						RETURN TRUE
					ENDIF
				ENDIF
				IF PS_UPDATE_FAIL_CHECKS(PS_Main)
					RETURN TRUE
				ENDIF
				BREAK
				
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main)
					RETURN TRUE
				ENDIF
				BREAK		
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    When it returns TRUE, the main loop will increment the challenge state/stage
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Fly_Low_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_Fly_Low_State)
			CASE PS_FLY_LOW_INIT
				RETURN FALSE
				BREAK

			CASE PS_FLY_LOW_COUNTDOWN
				RETURN FALSE
				BREAK
				
			CASE PS_FLY_LOW_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to first the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_FLY_LOW_OBSTACLE_COURSE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the last checkpoint yet
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK		
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE
ENDFUNC

PROC PS_FLY_LOW_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_Fly_Low_State = PS_FLY_LOW_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_FLY_LOW_FORCE_PASS
	PS_Fly_Low_State = PS_FLY_LOW_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Fly_Low_Initialise()
	//Start pos and heading are hard-coded since it different for each challenge.
	vStartPosition = <<-1601.2122, -2990.6001, 12.9693>> 
	vStartRotation = <<0.0000, 0.0000, 240.0000>>
	fStartHeading = 240.0000
	iFailDialogue = 1
	bLessonCompleted = FALSE
	bLesonFinishedLineTriggered	= FALSE
	
	//Bool used to keep the loop going for the entire challenge
	bFinishedChallenge 				= FALSE
	VehicleToUse 					= STUNT

	PS_Fly_Low_Init_Checkpoints()
	
	PS_SETUP_CHALLENGE()
	PS_REQUEST_SHARED_ASSETS()
	
	iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_FLY_LOW
	REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		WAIT(0)
	ENDWHILE	
	
	fPreviewVehicleSkipTime = 65000
	fPreviewTime = 14
	
	vPilotSchoolSceneCoords = vStartPosition
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
		vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
	ENDIF
	
	iPSDialogueCounter = 1
	iPSObjectiveTextCounter = 1
	iPSHelpTextCounter = 1
	//Offset to help us check whether we're in the air or on the ground
	PS_SET_GROUND_OFFSET(1.5)
	PS_HUD_SET_MAX_ALTITUDE(PS_FLY_LOW_MAX_ALTITUDE)

	//Spawn player vehicle				
	PS_CREATE_VEHICLE()
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Fly_Low.sc******************")
	#ENDIF
ENDPROC

PROC PS_Fly_Low_MainSetup()
	PS_Fly_Low_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	
	NEW_LOAD_SCENE_START(<<-1618.1404, -2980.8284, 17.4172>>, NORMALISE_VECTOR(<<-1611.9894, -2984.3794, 16.7974>> - <<-1618.1404, -2980.8284, 17.4172>>), 5000.0)
ENDPROC

FUNC BOOL PS_Fly_Low_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF							
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE, iPreviewCheckpointIdx, TRUE)
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV4", "PS_PREV4_1", "PS_PREV4_2", "PS_PREV4_3")
				//"This lesson will teach you how to get better control over your turns."
				//"The obstacle course goes around the harbor and you have to stay low."
				//"You'll need to utilize your rudders to help you make some of the sharp turns."
				BREAK
			CASE PS_PREVIEW_PLAYING
				//let recording play.
				BREAK
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC

FUNC BOOL PS_Fly_Low_MainUpdate()
	IF TempDebugInput() AND NOT bFinishedChallenge
		
		PS_LESSON_UDPATE()
		
		SWITCH(PS_Fly_Low_State)
		//Spawn player and plane on the runway
			CASE PS_FLY_LOW_INIT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//Setup vars and vehicle
						SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(PS_FLY_LOW_MAX_ALTITUDE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Fly_Low_State = PS_FLY_LOW_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

		//Play countdown
			CASE PS_FLY_LOW_COUNTDOWN
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//Update countdown
						IF	PS_HUD_UPDATE_COUNTDOWN()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Fly_Low_State = PS_FLY_LOW_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving off
			CASE PS_FLY_LOW_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//Setup objective, give the player controls, turn on HUD elements, and manage any new help text
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_FLYLOW", "PS_FLYLOW_1", "PS_FLYLOW", "PS_FLYLOW_2")
						//"I want you to use everything you learned so far to navigate this course as fast as you can."
						//"Watch your altitude, though. This is really a test of how low you can go and how well you can maneuver the plane."
						PS_HUD_RESET_TIMER()
						//hacky, but resets the altimeter
						vHeightCheck = GET_ENTITY_COORDS(PLAYER_PED_ID())
						PS_HUD_SET_ALTIMETER(vHeightCheck.z - GroundOffset)
						PS_SET_CHECKPOINT_COUNTER_ACTIVE(TRUE)
						PS_SET_ALTIMETER_ACTIVE(TRUE)
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
						
					CASE PS_SUBSTATE_UPDATE
						IF PS_Fly_Low_Fail_Check()
							PS_Fly_Low_State = PS_FLY_LOW_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Fly_Low_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
						
					CASE PS_SUBSTATE_EXIT
						PS_Fly_Low_State = PS_FLY_LOW_OBSTACLE_COURSE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Obstacle course loop
			CASE PS_FLY_LOW_OBSTACLE_COURSE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//Reset the chase cam
 				    	PS_INCREMENT_SUBSTATE()
						iPSHelpTextCounter = 1
						SETTIMERA (0)
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Fly_Low_Fail_Check()
							PS_Fly_Low_State = PS_FLY_LOW_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Fly_Low_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELIF TIMERA() > 0
							PS_FLY_LOW_UPDATE_HELP_MESSAGES()
						ENDIF
						BREAK
						
					CASE PS_SUBSTATE_EXIT
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_Fly_Low_State = PS_FLY_LOW_SUCCESS
						ELSE
							PS_Fly_Low_State = PS_FLY_LOW_FAIL
						ENDIF
						bLessonCompleted = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
												
			CASE PS_FLY_LOW_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							PS_UPDATE_RECORDS(VECTOR_ZERO)
							iFailDialogue = GET_RANDOM_INT_IN_RANGE(1,3)
							PS_PLAY_LESSON_FINISHED_LINE("PS_FLYLOW", "PS_FLYLOW_4", "PS_FLYLOW_5", "PS_FLYLOW_6", "PS_FLYLOW_7", "PS_FLYLOW_8")
							PS_SET_ALTIMETER_ACTIVE(FALSE)
							PS_HUD_STOP_ALTIMETER_ALARM()
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
							PS_LESSON_END(TRUE)
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_FLY_LOW_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
							ELSE
								PS_UPDATE_RECORDS(VECTOR_ZERO)
								IF ( bLessonCompleted = TRUE )
									PS_PLAY_LESSON_FINISHED_LINE("PS_FLYLOW", "PS_FLYLOW_4", "PS_FLYLOW_5", "PS_FLYLOW_6", "PS_FLYLOW_7", "PS_FLYLOW_8")
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_FLYLOW", "PS_FLYLOW_4", "PS_FLYLOW_5", "PS_FLYLOW_6", "")
								ENDIF
								PS_SET_ALTIMETER_ACTIVE(FALSE)
								PS_HUD_STOP_ALTIMETER_ALARM()
								iEndDialogueTimer = GET_GAME_TIMER()
								bLesonFinishedLineTriggered = TRUE
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_Fly_Low_MainCleanup()
	SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
	PS_HUD_STOP_ALTIMETER_ALARM()
//hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_SET_ALTIMETER_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_CHECKPOINT_COUNTER_ACTIVE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	//cleanup checkpoints
	PS_RESET_CHECKPOINT_MGR()
	
//player/objects	
	PS_SEND_PLAYER_BACK_TO_MENU()
//assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Fly_Low.sc******************")
	#ENDIF	
ENDPROC


// EOF
//****************************************************************************************************
//****************************************************************************************************

