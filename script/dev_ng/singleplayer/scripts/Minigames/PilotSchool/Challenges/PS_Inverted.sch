//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/08/11
//	Description:	"Upside-Down Flight" challenge for Pilot School. The player is told to do a couple stunts,
//					including barrel rolls and holding the plane upside down for a short duration.
//	
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Inverted Flight
//****************************************************************************************************
//****************************************************************************************************

PROC  PS_Challenge_Inverted_Init_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1286.3641, -2124.3770,62>>)	
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Challenge_Inverted_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_Inverted_State)
			CASE PS_INVERTED_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_INVERTED_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_INVERTED_TAKEOFF
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, TRUE, FALSE)
					RETURN TRUE
				ENDIF
				BREAK
			
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main)
					RETURN TRUE
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    When it returns TRUE, the main loop will increment the challenge state/stage
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Challenge_Inverted_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_Inverted_State)
			CASE PS_INVERTED_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1 OR PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
								
			CASE PS_INVERTED_SINGLE_BARREL_PERFORM
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player has completed exactly 1 roll
						IF(PS_GET_OBJECTIVE_CURRENT_ORIENT_COUNT() > 0 )
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
						
			CASE PS_INVERTED_LEVEL_OUT_ONE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//Make sure the player levels out for a second
						IF PS_IS_LEVEL_OUT_COMPLETE(PS_DEFAULT_LEVEL_OUT_HOLD_TIME)
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_INVERTED_MULTI_BARREL_PERFORM
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//check for 2+ more barrel rolls
						IF(PS_GET_OBJECTIVE_CURRENT_ORIENT_COUNT() + PS_GET_OBJECTIVE_TOTAL_ORIENT_COUNT()) > 2
							RETURN TRUE
						ELSE
							RETURN FALSE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_INVERTED_LEVEL_OUT_TWO
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						IF PS_IS_LEVEL_OUT_COMPLETE(PS_DEFAULT_LEVEL_OUT_HOLD_TIME)
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_UPSIDE_DOWN_PERFORM
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//Track how long the player is upside-down
						IF PS_IS_INVERTED_STUNT_COMPLETE(PS_INVERTED_HOLD_TIME)
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_LEVEL_OUT_THREE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						IF PS_IS_LEVEL_OUT_COMPLETE(PS_DEFAULT_LEVEL_OUT_HOLD_TIME)
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
	ENDIF
	//only true when we're progressing
	RETURN FALSE
ENDFUNC

PROC PS_INVERTED_FORCE_FAIL
	ePSFailReason 		= PS_FAIL_DEBUG
	PS_Inverted_State 	= PS_INVERTED_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_INVERTED_FORCE_PASS
	PS_Inverted_State 	= PS_INVERTED_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_INVERTED_J_SKIP()
		PS_INVERTED_FLIGHT_CHALLENGE nextState = PS_Inverted_State
		IF PS_Inverted_State > PS_INVERTED_TAKEOFF
			IF PS_Inverted_State < PS_INVERTED_SINGLE_BARREL_PREP
				nextState = PS_INVERTED_SINGLE_BARREL_PREP			
			ELIF PS_Inverted_State < PS_INVERTED_MULTI_BARREL_PREP
				nextState = PS_INVERTED_MULTI_BARREL_PREP
			ELIF PS_Inverted_State < PS_INVERTED_UPSIDE_DOWN_PREP
				nextState = PS_INVERTED_UPSIDE_DOWN_PREP
			ELIF PS_Inverted_State < PS_INVERTED_LEVEL_OUT_THREE
				nextState = PS_INVERTED_LEVEL_OUT_THREE
			ENDIF
			IF nextState != PS_Inverted_State AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				//send plane to the takeoff checkpoint
				SET_ENTITY_COORDS(PS_Main.myVehicle, PS_GET_CHECKPOINT_FROM_ID(1))
				SET_VEHICLE_FORWARD_SPEED(PS_Main.myVehicle, 60.0)
				PS_Inverted_State = nextState
				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
				RETURN TRUE
			ENDIF	
		ENDIF
		RETURN FALSE
	ENDFUNC
#ENDIF

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Inverted_Initialise()
	PS_Challenge_Inverted_Init_Checkpoints()
	
	//init vars
	PS_Inverted_State 							= PS_INVERTED_INIT
	iPS_PreviewVehicleRecordingID 				= iPS_PEVIEW_VEH_REC_ID_INVERTED
	vStartPosition								= <<-1627.5929, -2715.2949, 12.9445>>
	vStartRotation 								= <<10.5441, 0.0000, 330.0000>>
	fStartHeading 								= 330.0000
	iPSDialogueCounter							= 1
	iPSHelpTextCounter							= 1
	iPSObjectiveTextCounter						= 1
	iFailDialogue								= 1
	bLessonCompleted							= FALSE
	bLesonFinishedLineTriggered					= FALSE
	
	//player vehicle model
	VehicleToUse = STUNT
	PS_SET_GROUND_OFFSET(1.5)
	
	PS_SETUP_CHALLENGE()	
	PS_REQUEST_SHARED_ASSETS()
	
	//load assets
	REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		PRINTLN("Flight School - Waiting on assets to load")
		WAIT(0)
	ENDWHILE
	
	//preview stuff
	fPreviewVehicleSkipTime 					= 38000
	fPreviewTime								= 13
	vPilotSchoolSceneCoords = vStartPosition
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
		vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
	ENDIF

	//Spawn player vehicle			
	PS_CREATE_VEHICLE()
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Inverted.sc******************")
	#ENDIF
	
ENDPROC

FUNC BOOL PS_Inverted_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				IF DOES_CAM_EXIST(camMainMenu)
//					DESTROY_CAM(camMainMenu)
//				ENDIF
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT, iPreviewCheckpointIdx, TRUE)	
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV1", "PS_PREV1_1", "PS_PREV1_2", "PS_PREV1_3")
				//"For this lesson you will be performing a couple different stunts."
				//"We'll start you out with a few barrel rolls."
				//"If you make it through those, you can take a whack at flying upside-down."
				BREAK
			CASE PS_PREVIEW_PLAYING
				//let vehicle recording play
				BREAK
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PS_Inverted_MainSetup()
	PS_Challenge_Inverted_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	
	NEW_LOAD_SCENE_START(<<-1637.2950, -2732.1042, 17.4674>>, NORMALISE_VECTOR(<<-1635.0789, -2728.2649, 17.0676>> - <<-1637.2950, -2732.1042, 17.4674>>), 5000.0)
ENDPROC

FUNC BOOL PS_Inverted_MainUpdate()
	VECTOR vFront, vSide, vUp, vPos
	
	IF TempDebugInput() AND NOT bFinishedChallenge
		
		#IF IS_DEBUG_BUILD
			IF PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
				IF PS_INVERTED_J_SKIP()
					RETURN TRUE //if we do a j skip, then skip an update
				ENDIF
			ENDIF
		#ENDIF
		
		IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
			GET_ENTITY_MATRIX(PS_Main.myVehicle, vFront, vSide, vUp, vPos)
		ENDIF
		
		PS_LESSON_UDPATE()
		
		SWITCH(PS_Inverted_State)
			CASE PS_INVERTED_INIT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Inverted_State = PS_INVERTED_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		
			CASE PS_INVERTED_COUNTDOWN
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Inverted_State = PS_INVERTED_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT)
						QUEUE_OBJHELPALOGUE_HELP("PSINVERT_B", -1, 0.5)
						PLAY_OBJHELPALOGUE()
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_INVERT", "PS_INVERT_1") //"Alright, pilot, it's time for us to get a little crazy."
						iInstructionCounter = -1
						//hud stuff
						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Inverted_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ENDIF
						IF iInstructionCounter = -1 AND GET_ENTITY_SPEED(PS_Main.myVehicle) > 25
							CLEAR_HELP()
							iInstructionCounter++
						ELIF iInstructionCounter = 0 AND GET_ENTITY_SPEED(PS_Main.myVehicle) > 25
							PRINT_HELP("PSINVERT_C")
							iInstructionCounter++
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED() AND iInstructionCounter = 1
							PS_PLAY_DISPATCHER_INSTRUCTION( "PS_INVERT", "PS_INVERT_2") //"First I want you to get some altitude and head for that checkpoint marked at the end of the runway."
							iInstructionCounter++
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED() AND iInstructionCounter = 2
							PRINT("PSINVERT_A", DEFAULT_GOD_TEXT_TIME, 1)//"~s~Head for the first ~y~checkpoint~s~."
							iInstructionCounter++	
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
						PS_SET_HINT_CAM_ACTIVE(FALSE)
						iInstructionCounter = 1
						iPSHelpTextCounter = 3
						PS_Inverted_State = PS_INVERTED_TAKEOFF_REACT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_TAKEOFF_REACT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_INVERT", "PS_INVERT_3")//"Good. Okay, let's see what you got! "
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Inverted_State = PS_INVERTED_SINGLE_BARREL_PREP
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_INVERTED_SINGLE_BARREL_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_INVERT", "PS_INVERT_4")//"On my mark, perform a barrel roll by pulling your flight stick to the left or right."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Inverted_State = PS_INVERTED_SINGLE_BARREL_PERFORM
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_SINGLE_BARREL_PERFORM
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_INVERTOBJ", "PS_INVERTOBJ_1")//"Perform a barrel roll."
//						PRINT("PSINVERT_D", DEFAULT_GOD_TEXT_TIME, 1)//"~s~Perform a barrel roll."
						iInstructionCounter++
						QUEUE_OBJHELPALOGUE_HELP("PSINVERT_E", -1, 1.5)
						QUEUE_OBJHELPALOGUE_HELP("PSINVERT_C1", -1, 4.5)
						PLAY_OBJHELPALOGUE()
						//reset the roll progress meter, then turn it on
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_ROLL_METER_ACTIVE(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Inverted_Progress_Check()
							PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
//						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
						PS_RESET_OBJECTIVE_DATA()
						CLEAR_OBJHELPALOGUE()
//						PS_Main.myObjectiveData.iTotalOrientCount = -1
						CLEAR_PRINTS()
//						CLEAR_HELP()
						PS_Inverted_State = PS_INVERTED_SINGLE_BARREL_REACT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_SINGLE_BARREL_REACT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//positive feedback, now level out the plane.
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_INVERT", "PS_INVERT_5", "PS_INVERT", "PS_INVERT_6")//"Stones of steel! I like it. Now let's try a few of those in a row."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Inverted_State = PS_INVERTED_LEVEL_OUT_ONE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_INVERTED_LEVEL_OUT_ONE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							IF NOT PS_IS_PLANE_CURRENTLY_LEVEL()
								QUEUE_OBJHELPALOGUE_OBJECTIVE("PSBARREL_E", -1, 0.5) // "~s~Level out the plane."
								QUEUE_OBJHELPALOGUE_HELP("PSINVERT_G", -1, 2.0) // "~s~Center the ~PAD_LSTICK_NONE~ to level out."
								PLAY_OBJHELPALOGUE()
							ELSE
								PS_INCREMENT_SUBSTATE() //double increment! coupled with the other increment, this should make us exit this state next frame
							ENDIF
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//check for plane leveling out
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Inverted_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_OBJHELPALOGUE()
						CLEAR_PRINTS()
						CLEAR_HELP()
						iInstructionCounter = 1
						PS_Inverted_State = PS_INVERTED_MULTI_BARREL_PREP
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_MULTI_BARREL_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_INVERT", "PS_INVERT_7")//"I want to see you do three barrel rolls in a row on my signal. Watch your altitude on this one."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Inverted_State = PS_INVERTED_MULTI_BARREL_PERFORM
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_MULTI_BARREL_PERFORM
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_INVERTOBJ", "PS_INVERTOBJ_2")//"Perform three barrel rolls in a row."
//						PRINT_NOW("PSINVERT_H", DEFAULT_GOD_TEXT_TIME, 1)	//"~s~Complete three barrel rolls."
						PS_RESET_OBJECTIVE_DATA()
						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Inverted_Progress_Check()
							PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Inverted_State = PS_INVERTED_MULTI_BARREL_REACT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_MULTI_BARREL_REACT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_INVERT", "PS_INVERT_8")//"Very nice! Are you getting dizzy in there yet? Go ahead and level out the plane again."
//						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
						PS_RESET_OBJECTIVE_DATA()
//						PS_Main.myObjectiveData.iTotalOrientCount = -1
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Inverted_State = PS_INVERTED_LEVEL_OUT_TWO
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_LEVEL_OUT_TWO
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							IF NOT PS_IS_PLANE_CURRENTLY_LEVEL()
								PRINT("PSBARREL_E", DEFAULT_GOD_TEXT_TIME, 1) //"~s~Level out the plane."
							ELSE
								PS_INCREMENT_SUBSTATE() //double increment! coupled with the other increment, this should make us exit this state next frame
							ENDIF
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//check for plane leveling out
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Inverted_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_OBJHELPALOGUE()
						CLEAR_PRINTS()
						CLEAR_HELP()
						iInstructionCounter = 1
						PS_Inverted_State = PS_INVERTED_UPSIDE_DOWN_PREP
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_UPSIDE_DOWN_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_INVERT", "PS_INVERT_9", "PS_INVERT", "PS_INVERT_10")
						//"Now let's see how you do upside down!"
						//"On my word, roll over the plane and hold it steady."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						CLEAR_HELP()
						iInstructionCounter = 1
						PS_Inverted_State = PS_INVERTED_UPSIDE_DOWN_PERFORM
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_UPSIDE_DOWN_PERFORM
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF vUp.z <= STUNT_PLANE_INVERT_THRESHOLD
							//then we're inverted
							PRINTLN("We're inverted so skipping dispatcher instruction")
						ELSE
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_INVERTOBJ", "PS_INVERTOBJ_3")//"Roll the plane upside down."
//							PRINT_NOW("PSINVERT_I", DEFAULT_GOD_TEXT_TIME, 1)//"~s~Roll the plane upside down."
						ENDIF
						iInstructionCounter++
						Pilot_School_HUD_Reset_Obj_Meter()
						RESTART_TIMER_AT(PS_Main.tPulse, 0.0)
						PAUSE_TIMER(PS_Main.tPulse)
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_INVERTED_METER_ACTIVE(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//check for plane leveling out
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Inverted_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND iInstructionCounter = 2
								IF vUp.z <= STUNT_PLANE_INVERT_THRESHOLD
									PS_PLAY_DISPATCHER_INSTRUCTION("PS_INVERTOBJ", "PS_INVERTOBJ_4")//"Hold the plane upside down for several seconds."
	//								PRINT_NOW("PSINVERT_J", DEFAULT_GOD_TEXT_TIME, 1)//"~s~Hold the plane upside-down until the meter fills."
									iInstructionCounter++
								ENDIF
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						CLEAR_HELP()
						PS_Inverted_State = PS_INVERTED_UPSIDE_DOWN_REACT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_UPSIDE_DOWN_REACT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//"Gettin sick yet?! You aren't half bad at this pilot thing, kid. Good thing you have an excellent teacher!"
						//"Go ahead and level out. That went well for your first lesson. Let's take a breather for now."
						PS_SET_INVERTED_METER_ACTIVE(FALSE)
						PS_RESET_OBJECTIVE_DATA()
//						PS_Main.myObjectiveData.iTotalOrientCount = -1
						//give feedback and then say level out
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Inverted_State = PS_INVERTED_LEVEL_OUT_THREE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_LEVEL_OUT_THREE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							IF NOT PS_IS_PLANE_CURRENTLY_LEVEL()
								PS_PLAY_DISPATCHER_INSTRUCTION("PS_INVERTOBJ", "PS_INVERTOBJ_5")//"Roll the plane right side up."
//								PRINT_NOW("PSINVERT_K", DEFAULT_GOD_TEXT_TIME, 1) //"~s~Roll the plane right side up."
							ELSE
								PS_INCREMENT_SUBSTATE() //double increment! coupled with the other increment, this should make us exit this state next frame
							ENDIF
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Inverted_Fail_Check()
							PS_Inverted_State = PS_INVERTED_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Inverted_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_Inverted_State = PS_INVERTED_SUCCESS
						ELSE
							PS_Inverted_State = PS_INVERTED_FAIL
						ENDIF
						bLessonCompleted = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_INVERTED_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							PS_UPDATE_RECORDS(VECTOR_ZERO)
							iFailDialogue = 1
							PS_PLAY_LESSON_FINISHED_LINE("PS_INVERT", "PS_INVERT_11", "PS_INVERT_12", "PS_INVERT_13", "PS_INVERT_14")
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
							PS_LESSON_END(TRUE)
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_INVERTED_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
							ELSE
								PS_UPDATE_RECORDS(VECTOR_ZERO)
								IF ( bLessonCompleted = TRUE )
									PS_PLAY_LESSON_FINISHED_LINE("PS_INVERT", "PS_INVERT_11", "PS_INVERT_12", "PS_INVERT_13", "PS_INVERT_14")
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_INVERT", "PS_INVERT_11", "PS_INVERT_12", "PS_INVERT_13", "")
								ENDIF
								iEndDialogueTimer = GET_GAME_TIMER()
								bLesonFinishedLineTriggered = TRUE
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_Inverted_MainCleanup() 
//take care of hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	
//take care of objects
	//player first
	PS_SEND_PLAYER_BACK_TO_MENU()
	//then vehicle
	
	//then checkpoints
	PS_RESET_CHECKPOINT_MGR()

//loaded assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(STUNT)	
	
//done cleaning up
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************PS_Inverted: done cleaning up lesson ******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************
