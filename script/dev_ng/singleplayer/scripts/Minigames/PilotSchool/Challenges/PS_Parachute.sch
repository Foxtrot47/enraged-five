//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/14/11
//	Description:	"Parachute onto a Target" Challenge for Pilot School. The player must skydive
//					from a piloted plane and land as close as possible to a static target on the ground.
//	
////****************************************************************************************************
//
//Jump Cam position 1
//ATTACH_CAM_TO_ENTITY(cam, entity, <<-12.6533, -49.3726, -16.0138>>)
//POINT_CAM_AT_ENTITY(cam, entity, <<-10.9340, -47.6023, -14.3079>>)
//SET_CAM_FOV(cam, 32.4589)
//Camera world coords: <<-1159.7648, -3297.0969, 1152.3550>>  Camera rotation: <<38.3069, 0.0000, 58.9977>>
//Entity world coords: <<-1209.1200, -3298.0085, 1172.7861>>  Entity rotation: <<5.2102, 0.0002, 105.9105>>

//Jump Cam position 2
//ATTACH_CAM_TO_ENTITY(cam, entity, <<-5.0812, -36.9571, -0.4403>>)
//POINT_CAM_AT_ENTITY(cam, entity, <<-3.2242, -38.1790, -2.4549>>)
//SET_CAM_FOV(cam, 70.0000)
//Camera world coords: <<-1172.3711, -3292.8167, 1168.9916>>  Camera rotation: <<-44.8897, 0.0000, -13.1970>>
//Entity world coords: <<-1209.1200, -3298.0085, 1172.7861>>  Entity rotation: <<5.2102, 0.0002, 105.9105>>

USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Inverted Flight
//****************************************************************************************************
//****************************************************************************************************


PROC  PS_Challenge_Parachute_Init_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_LANDING, <<-1016.4353, -2600.2429, 40.1061>>)//<< -1145.7977, -2864.1245, 12.9452 >>)//1
ENDPROC

//FUNC BOOL PS_Parachute_Skip_Cutscene()
//	SWITCH(PS_CutsceneSkipState)
//		CASE PS_STARTING_CUTSCENE_STATE_01
//			DO_SCREEN_FADE_OUT(500)
//			PS_CutsceneSkipState = PS_STARTING_CUTSCENE_STATE_02
//			BREAK
//		CASE PS_STARTING_CUTSCENE_STATE_02
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//				// TODO: Player setup here
//				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
//				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle, VS_FRONT_RIGHT )
//				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1)
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				PS_Parachute_State = PS_PARACHUTE_PREJUMP
//				eSubState = PS_SUBSTATE_ENTER
//			ENDIF
//			BREAK			
//	ENDSWITCH
//	RETURN FALSE
//ENDFUNC

/// PURPOSE:
///    See if the player has landed near the target, and how close.
FUNC PS_PARACHUTE_FLIGHT_CHALLENGE PS_PARACHUTE_LANDING_CHECK()
	VECTOR vTargetToPlayer
	FLOAT fDist
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		ePSFailReason = PS_FAIL_PLAYER_DEAD
		RETURN PS_PARACHUTE_FAIL
	ENDIF
	
	IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		ePSFailReason = PS_FAIL_SUBMERGED
		RETURN PS_PARACHUTE_FAIL
	ENDIF
	
	vTargetToPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) - PS_GET_LAST_CHECKPOINT()
	
	IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
		IF vTargetToPlayer.z > 17.0
			ePSFailReason = PS_FAIL_REMOVED_PARACHUTE
			RETURN PS_PARACHUTE_FAIL
		ELSE
			RETURN PS_PARACHUTE_PROCESS_DATA
		ENDIF
	ENDIF
	
	IF vTargetToPlayer.z < -2.0 OR vTargetToPlayer.z > 5.0
		//VECTOR vFuckOurLanguage = PS_GET_LAST_CHECKPOINT()
		ePSFailReason = PS_FAIL_TOO_FAR
		RETURN PS_PARACHUTE_FAIL
	ENDIF
	
	fDist = vTargetToPlayer.x * vTargetToPlayer.x + vTargetToPlayer.y * vTargetToPlayer.y
	IF fDist > PS_Challenges[PSC_parachuteOntoTarget].BronzeDistance * PS_Challenges[PSC_parachuteOntoTarget].BronzeDistance
		//VECTOR vInTheFace = PS_GET_LAST_CHECKPOINT()
		ePSFailReason = PS_FAIL_TOO_FAR
		RETURN PS_PARACHUTE_FAIL
	ENDIF
	
	PS_Main.myPlayerData.LandingDistance = SQRT(fDist)
	
	IF PS_Main.myPlayerData.LandingDistance > PS_Challenges[PSC_parachuteOntoTarget].SilverDistance
		iChallengeScores[g_current_selected_PilotSchool_class] = iSCORE_FOR_BRONZE
	ELIF PS_Main.myPlayerData.LandingDistance > PS_Challenges[PSC_parachuteOntoTarget].GoldDistance
		iChallengeScores[g_current_selected_PilotSchool_class] = iSCORE_FOR_SILVER
	ELSE
		iChallengeScores[g_current_selected_PilotSchool_class] = iSCORE_FOR_GOLD
	ENDIF
	
	RETURN PS_PARACHUTE_SUCCESS
ENDFUNC

PROC PS_Parachute_Updating_Preview()
	SWITCH (eParachutePreview)
		CASE PS_PARACHUTE_INIT
			//put player in the air and 
			fPreviewTime = 17
			RESTART_TIMER_NOW(tParachutePreviewTimer)
			eParachutePreview = PS_PARACHUTE_SKYDIVING
			BREAK

		CASE PS_PARACHUTE_JUMPING
			//wait for a while then pull yor chute?
			IF TIMER_DO_WHEN_READY(tParachutePreviewTimer, 5.0)
				IF NOT IS_ENTITY_DEAD(PS_Main.prevPed)
					TASK_PARACHUTE_TO_TARGET(PS_Main.prevPed, PS_GET_CURRENT_CHECKPOINT())
				ENDIF
				eParachutePreview = PS_PARACHUTE_SKYDIVING
			ENDIF
			BREAK
			
		CASE PS_PARACHUTE_SKYDIVING
			IF TIMER_DO_WHEN_READY(tParachutePreviewTimer, 7.0)
				eParachutePreview = PS_PARACHUTE_WAIT
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(PS_Main.prevPed)				
					SET_PED_KEEP_TASK(PLAYER_PED_ID(), TRUE)
					SET_PARACHUTE_TASK_TARGET(PS_Main.prevPed, PS_GET_CURRENT_CHECKPOINT())
				ENDIF
			ENDIF
			//wait for a while then fade out
			BREAK
	
		CASE PS_PARACHUTE_WAIT
			//wait for a while then fade out
			BREAK
	ENDSWITCH
ENDPROC

PROC PS_PARACHUTE_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_Parachute_State = PS_PARACHUTE_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_PARACHUTE_FORCE_PASS
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), PS_GET_LAST_CHECKPOINT())
	ENDIF
	PS_Parachute_State = PS_PARACHUTE_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_PARACHUTE_J_SKIP()
/*		PS_PARACHUTE_FLIGHT_CHALLENGE nextState = PS_Parachute_State
		IF PS_Parachute_State > PS_PARACHUTE_COUNTDOWN
			IF PS_Parachute_State < PS_PARACHUTE_SKYDIVING
				nextState = PS_PARACHUTE_SKYDIVING
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					VECTOR tempVec = GET_ENTITY_COORDS(PLAYER_PED_ID())
					tempVec.z -= 50
					SET_ENTITY_COORDS(PLAYER_PED_ID(), tempVec)
				ENDIF
			ELIF PS_Parachute_State < PS_PARACHUTE_FLOATING
				nextState = PS_PARACHUTE_FLOATING
				FORCE_PED_TO_OPEN_PARACHUTE(PLAYER_PED_ID())
			ELIF PS_Parachute_State = PS_PARACHUTE_FLOATING //if we're already floatin
				nextState = PS_PARACHUTE_SUCCESS //keep it floating and just
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), PS_GET_LAST_CHECKPOINT())
			ENDIF
			IF nextState != PS_Parachute_State AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				PS_Parachute_State = nextState
				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
				RETURN TRUE
			ENDIF	
		ENDIF
*/		RETURN FALSE
	ENDFUNC
#ENDIF

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Parachute_Initialise()
	bFinishedChallenge 							= FALSE
	VehicleToUse 								= MAVERICK
	iInstructionCounter 						= 1
	iPSDialogueCounter							= 1
	iPSHelpTextCounter							= 1
	iPSObjectiveTextCounter						= 1
	ppsPlayerParachuteState						= PPS_INVALID
	eParachutePreview							= PS_PARACHUTE_INIT
	iFailDialogue								= 1
	bLessonCompleted							= FALSE
	bLesonFinishedLineTriggered					= FALSE
	
	PS_Challenge_Parachute_Init_Checkpoints()
	
	//setup autopilot path nodes
//	vCheckpoint[0] = cargoStartCoords//<<-1985.3583,-2672.3298,1250.0000>>
//	vCheckpoint[1] = cargoEndCoords//<<-686.3201,-3422.3298,1250.0000>>
	vStartPosition = <<-2064.2603, -2995.9041, 1105.6519>>
	fStartHeading = 14.6945
	
	vPreviewCamCoords = <<-1087.4, -3330.0, 604.6>>
	vPreviewCamDirection = NORMALISE_VECTOR(<<-1090.4, -3326.1, 599.7>> - vPreviewCamCoords)
	
	//TODO: do we still need this for the parachute?
	//Offset to calculate whether player is in the air or on the ground
	PS_SET_GROUND_OFFSET(1.5)	

	PS_SETUP_CHALLENGE()
	
//	ePS_CamViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT)
//	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT, CAM_VIEW_MODE_THIRD_PERSON_FAR)
	
	vPilotSchoolSceneCoords = vStartPosition
	vPilotSchoolPreviewCoords = PS_MENU_CAM_COORDS
	
	MODEL_NAMES previewModel = GET_NPC_PED_MODEL(CHAR_DOM)
	
	REQUEST_ANIM_DICT("veh@helicopter@rps@base")
	REQUEST_ANIM_DICT("oddjobs@basejump@")
	
	PS_REQUEST_SHARED_ASSETS()
	REQUEST_MODEL(previewModel)
	REQUEST_MODEL(PilotModel)
	REQUEST_MODEL(PROP_PARAPACK_01)
	REQUEST_PTFX_ASSET()
	
	WHILE NOT HAS_MODEL_LOADED(previewModel)
		OR NOT HAS_ANIM_DICT_LOADED("veh@helicopter@rps@base")
		OR NOT HAS_ANIM_DICT_LOADED("oddjobs@basejump@")
		OR NOT HAS_MODEL_LOADED(PilotModel)
		OR NOT HAS_MODEL_LOADED(PROP_PARAPACK_01)
		OR NOT HAS_PTFX_ASSET_LOADED()
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		WAIT(0)
	ENDWHILE
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1366.7184, -1773.0824, 0.00 >>, << -1526.6874, -3390.1799, 1500.00 >>, FALSE)
	
	//put player in the air and try to force him into freefalling
	
	SETTIMERA(0)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 2, TRUE)
		
//		WHILE NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
//			WAIT(0)
//		ENDWHILE
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			SET_PED_COMP_ITEM_CURRENT_SP( PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BLUE_BOILER_SUIT, FALSE )
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 5, 0) 		
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			SET_PED_COMP_ITEM_CURRENT_SP( PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_SKYDIVING, FALSE )
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 1, 0)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			SET_PED_COMP_ITEM_CURRENT_SP( PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_BLUE_BOILER_SUIT, FALSE )
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0) 
		ENDIF
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		AND NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//			SPECIAL_FUNCTION_DO_NOT_USE(PLAYER_PED_ID(), FALSE)
//		ENDIF
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), << -1090.4011, -3326.0393, 700.0000 >>)//<< -1090.4011, -3326.0393, 1030.2483 >>)
//		SET_PED_GRAVITY(PLAYER_PED_ID(), TRUE)
//		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
//		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
//		SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	PS_Main.prevPed = CREATE_PED(PEDTYPE_MISSION, previewModel, << -1090.4011, -3326.0393, 650.0000 >>)
	IF NOT IS_ENTITY_DEAD(PS_Main.prevPed)
		GIVE_WEAPON_TO_PED(PS_Main.prevPed , GADGETTYPE_PARACHUTE, 1, TRUE)		
		WHILE NOT HAS_PED_GOT_WEAPON(PS_Main.prevPed, GADGETTYPE_PARACHUTE)
			WAIT(0)
		ENDWHILE
		SET_PED_GADGET(PS_Main.prevPed, GADGETTYPE_PARACHUTE, TRUE)		
		SET_PED_GRAVITY(PS_Main.prevPed, TRUE)		
		CLEAR_PED_TASKS_IMMEDIATELY(PS_Main.prevPed)
		TASK_SKY_DIVE(PS_Main.prevPed)
	ENDIF
	
	PS_Main.previewDummy = CREATE_PED(PEDTYPE_MISSION, previewModel, << -1090.4011, -3326.0393, 650.0000 >>)
	IF NOT IS_ENTITY_DEAD(PS_Main.previewDummy) 
		GIVE_WEAPON_TO_PED(PS_Main.previewDummy , GADGETTYPE_PARACHUTE, 1, TRUE)
		WHILE NOT HAS_PED_GOT_WEAPON(PS_Main.previewDummy, GADGETTYPE_PARACHUTE)
			WAIT(0)
		ENDWHILE
		SET_PED_GADGET(PS_Main.previewDummy, GADGETTYPE_PARACHUTE, TRUE)
		SET_PED_GRAVITY(PS_Main.previewDummy, TRUE)
		CLEAR_PED_TASKS_IMMEDIATELY(PS_Main.previewDummy)
		TASK_SKY_DIVE(PS_Main.previewDummy)
		
		SET_ENTITY_COLLISION(PS_Main.previewDummy, FALSE)
		SET_ENTITY_VISIBLE(PS_Main.previewDummy, FALSE)
	ENDIF

	IF NOT IS_ENTITY_DEAD(PS_Main.prevPed)
		WHILE NOT IS_ENTITY_DEAD(PS_Main.prevPed) AND GET_PED_PARACHUTE_STATE(PS_Main.prevPed) <> PPS_SKYDIVING AND TIMERA() < 3000
//			FORCE_PED_MOTION_STATE(PS_Main.prevPed, MS_PARACHUTING)
			WAIT(0)
		ENDWHILE
	ENDIF
		
	//setup ptfx
//	PS_PARACHUTE_START_TARGET_FLARE(PS_GET_CHECKPOINT_FROM_ID(1))
ENDPROC 

PROC PS_Parachute_MainSetup()
	VECTOR vBaseRotCopy
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF
	//REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)	
	PS_PREVIEW_CLEANUP_CUTSCENE()
	
	IF NOT IS_ENTITY_DEAD(PS_Main.prevPed)
		DELETE_PED(PS_Main.prevPed)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PS_Main.previewDummy)
		DELETE_PED(PS_Main.previewDummy)
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_DOM))
	
	CLEAR_AREA(vStartPosition, 2000, TRUE)
	SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
	
	//Spawn heli and turn it on
	IF NOT DOES_ENTITY_EXIST(PS_Main.myVehicle)
		PS_Main.myVehicle = CREATE_VEHICLE(VehicleToUse, vStartPosition, fStartHeading)
	ELSE
		SET_VEHICLE_FIXED(PS_Main.myVehicle)
		SET_ENTITY_COORDS(PS_Main.myVehicle, vStartPosition)
		SET_ENTITY_HEADING(PS_Main.myVehicle, fStartHeading)
	ENDIF
	
	SET_VEHICLE_USED_FOR_PILOT_SCHOOL(PS_Main.myVehicle, TRUE)
	SET_VEHICLE_HAS_STRONG_AXLES(PS_Main.myVehicle, TRUE)
	SET_ENTITY_AS_MISSION_ENTITY(PS_Main.myVehicle)
	SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, TRUE, TRUE)
	FREEZE_ENTITY_POSITION(PS_Main.myVehicle, TRUE)
	
	IF NOT DOES_ENTITY_EXIST(PS_Main.myPilotPed)
		PS_Main.myPilotPed = CREATE_PED_INSIDE_VEHICLE(PS_Main.myVehicle, PEDTYPE_MISSION, PilotModel, VS_DRIVER)
	ENDIF
	
//	SET_ENTITY_INVINCIBLE(PS_Main.myPilotPed, TRUE)
	
	SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1)
		
		IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
			iSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
			ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneID, PS_Main.myVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(PS_Main.myVehicle, "Chassis"))
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneID, "oddjobs@basejump@", "Heli_door_loop", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS)
			SET_SYNCHRONIZED_SCENE_LOOPED(iSceneID, TRUE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		// Change the player's clothing.
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 5, 0) 		
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 1, 0)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0) 
		ENDIF
	ENDIF
	
	PS_Challenge_Parachute_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	
	PS_Main.myJumpCam.vLookCameraRot = <<10.2986, 0, 8.9090>>
	PS_Main.myJumpCam.vFocalOffset = 1.2 * <<SIN(-97.4239 + fStartHeading), -COS(-97.4239 + fStartHeading), 0.0>>
	PS_Main.myJumpCam.vFocalPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle, <<1.12046, -0.317773, 1.3385>>)
	VECTOR vCurFocalOffset = ROTATE_VECTOR_ABOUT_Z(PS_Main.myJumpCam.vFocalOffset, PS_Main.myJumpCam.vLookCameraRot.z)
	PS_Main.myJumpCam.vBaseLookCamRot = <<-33.0000 + PS_Main.myJumpCam.vLookCameraRot.x, 0.0, -88.515 + fStartHeading>>
	PS_Main.myJumpCam.fBaseLookCamFOV = 26.0
	
	vBaseRotCopy = PS_Main.myJumpCam.vBaseLookCamRot
	vBaseRotCopy.z += PS_Main.myJumpCam.vLookCameraRot.z
	
	IF DOES_CAM_EXIST(PS_Main.previewCam1)
		DESTROY_CAM(PS_Main.previewCam1)
	ENDIF
	
	NEW_LOAD_SCENE_START(PS_Main.myJumpCam.vFocalPoint + PS_Main.myJumpCam.vFocalOffset, PS_Main.myJumpCam.vFocalOffset, 7000.0)
	PS_Main.previewCam1 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,
		PS_Main.myJumpCam.vFocalPoint + vCurFocalOffset, vBaseRotCopy, PS_Main.myJumpCam.fBaseLookCamFOV, TRUE)
	SET_CAM_NEAR_CLIP(PS_Main.previewCam1, PS_GET_NEAR_CLIP())
	SHAKE_SCRIPT_GLOBAL("SKY_DIVING_SHAKE", 0.15)
	
	PS_Main.myJumpCam.vCameraVelocity = <<0,0,0>>
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Parachute.sc******************")
	#ENDIF
ENDPROC

FUNC BOOL PS_Parachute_Preview()
	SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(PS_Main.prevPed)
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV9", "PS_PREV9_1", "PS_PREV9_2", "PS_PREV9_3")
				//"This will be a lesson in skydiving so I hope you aren't afraid of heights!"
				//"Aim for the yellow smoking flare and pull your chute when you get close to the ground."
				//"The parachute is difficult to control so slow your approach to give yourself extra time to correct."
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_SKYDIVING)
				PS_Main.previewCam1 = CREATE_CAMERA()
//				previewCam2 = CREATE_CAMERA()
				SHAKE_CAM(PS_Main.previewCam1, "VIBRATE_SHAKE" )
				IF NOT IS_ENTITY_DEAD(PS_Main.prevPed) AND NOT IS_ENTITY_DEAD(PS_Main.previewDummy)
					SET_ENTITY_COORDS(PS_Main.previewDummy, GET_ENTITY_COORDS(PS_Main.prevPed))
					SET_ENTITY_VELOCITY(PS_Main.previewDummy, GET_ENTITY_VELOCITY(PS_Main.prevPed))
				ENDIF
				ATTACH_CAM_TO_ENTITY(PS_Main.previewCam1, PS_Main.previewDummy, <<3, -4, 1>>, TRUE)
				POINT_CAM_AT_ENTITY(PS_Main.previewCam1, PS_Main.prevPed, <<0, 0, 0>>)
				SET_CAM_ACTIVE(PS_Main.previewCam1, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				BREAK
			CASE PS_PREVIEW_PLAYING
				//do preview cutscene here
				PS_Parachute_Updating_Preview()
				IF IS_TIMER_STARTED(tParachutePreviewTimer)
					IF GET_TIMER_IN_SECONDS(tParachutePreviewTimer) > 7.0
						FORCE_CINEMATIC_RENDERING_THIS_UPDATE(TRUE)
					ENDIF
				ENDIF
				BREAK
			CASE PS_PREVIEW_CLEANUP
				
				//cleanup if we reach the end of the preview before skipping
				BREAK	
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


PROC PS_PARACHUTE_UPDATE_MUSIC()
//PS_PARACHUTE_SUCCESS,
//	PS_PARACHUTE_FAIL,
//	PS_PARACHUTE_INIT,
//	PS_PARACHUTE_COUNTDOWN,
//	PS_PARACHUTE_PREJUMP,
//	PS_PARACHUTE_JUMPING,
//	PS_PARACHUTE_SKYDIVING,
//	PS_PARACHUTE_DEPLOYING,
//	PS_PARACHUTE_FLOATING,
//	PS_PARACHUTE_WAIT
	
//	PS_MUSIC_EVENT_PREPARE_START,
//	PS_MUSIC_EVENT_TRIGGER_START,
//	PS_MUSIC_EVENT_PREPARE_STOP,
//	PS_MUSIC_EVENT_TRIGGER_STOP,
//	PS_MUSIC_EVENT_PREPARE_FAIL,
//	PS_MUSIC_EVENT_TRIGGER_FAIL
//
//	MGPS_START
//	MGPS_FAIL
//	MGPS_STOP
//	
	//if the prepared flag is true, or if we're in the right state.
	IF NOT IS_BIT_SET(iPSMusicBits, ENUM_TO_INT(PS_MUSIC_EVENT_TRIGGER_START))
		IF PS_Parachute_State >= PS_PARACHUTE_JUMPING
			IF MG_PREPARE_MUSIC_EVENT("MGPS_START", iPSMusicBits, PS_MUSIC_EVENT_PREPARE_START)
				PRINTLN("Attempting to trigger music event: MGPS_START")
				MG_TRIGGER_MUSIC_EVENT("MGPS_START", iPSMusicBits, PS_MUSIC_EVENT_TRIGGER_START)
			ENDIF
		ENDIF
	ELIF NOT IS_BIT_SET(iPSMusicBits, ENUM_TO_INT(PS_MUSIC_EVENT_TRIGGER_STOP)) AND NOT IS_BIT_SET(iPSMusicBits, ENUM_TO_INT(PS_MUSIC_EVENT_TRIGGER_FAIL))
		IF PS_Parachute_State = PS_PARACHUTE_SUCCESS OR PS_Parachute_State = PS_PARACHUTE_FAIL
			PRINTLN("Attempting to prepare music event: MGPS_STOP")
			IF MG_PREPARE_MUSIC_EVENT("MGPS_STOP", iPSMusicBits, PS_MUSIC_EVENT_PREPARE_STOP)
				PRINTLN("Attempting to trigger music event: MGPS_STOP")
				MG_TRIGGER_MUSIC_EVENT("MGPS_STOP", iPSMusicBits, PS_MUSIC_EVENT_TRIGGER_STOP)
			ENDIF
		ELSE
//			PRINTLN("PS_Parachute_State is in some odd state... ", PS_Parachute_State)
		ENDIF
	ELSE
		PRINTLN("PS_MUSIC_EVENT_TRIGGER_START, PS_MUSIC_EVENT_TRIGGER_STOP, and PS_MUSIC_EVENT_TRIGGER_FAIL are somehow all set")
	ENDIF

ENDPROC

FUNC BOOL PS_Parachute_MainUpdate()
	SEQUENCE_INDEX seqJump
	VECTOR vPlayerCoords, vCurFocalOffset
	
	IF TempDebugInput() AND NOT bFinishedChallenge
		
		#IF IS_DEBUG_BUILD
			IF PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
				IF PS_PARACHUTE_J_SKIP()
					RETURN TRUE //if we do a j skip, then skip an update
				ENDIF
			ENDIF
		#ENDIF

		PS_LESSON_UDPATE()
		PS_PARACHUTE_UPDATE_MUSIC()
		
		//Disable special ability b*2248190
		IF IS_SPECIAL_ABILITY_ENABLED(PLAYER_ID())
			ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
		ENDIF
		
		SWITCH(PS_Parachute_State)
		//Spawn plane and pilot and setup player with parachute in the plane
			CASE PS_PARACHUTE_INIT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_SKYDIVING)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						DISPLAY_RADAR(FALSE)
						DISPLAY_HUD(FALSE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Parachute_State = PS_PARACHUTE_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

			CASE PS_PARACHUTE_COUNTDOWN
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_UPDATE_LOOK_CAM()
						IF PS_HUD_UPDATE_COUNTDOWN(FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_UPDATE_LOOK_CAM()
						PS_Parachute_State = PS_PARACHUTE_LOOKING
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_PARACHUTE_LOOKING
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_UPDATE_LOOK_CAM()
						PRINT_HELP("PSCHUTE_1")
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_POT", "PS_POT_1") //"Hope you aren't afraid of heights!"
						iPSDialogueCounter++
						START_TIMER_NOW(PS_Main.myJumpCam.camTimer)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 0.08
						AND IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
							IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
								SET_ENTITY_COORDS(PS_Main.myVehicle, vStartPosition)
								SET_ENTITY_HEADING(PS_Main.myVehicle, fStartHeading)
								iSceneId = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneId, PS_Main.myVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(PS_Main.myVehicle, "Chassis"))
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneId, "oddjobs@basejump@", "Heli_jump", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT)
								SET_SYNCHRONIZED_SCENE_PHASE(iSceneID, 0.6)
							ENDIF
							
							CLEAR_HELP()
							PS_SETUP_JUMP_CAM()
							RESTART_TIMER_NOW(PS_Main.myJumpCam.camTimer)
							
							PS_Parachute_State = PS_PARACHUTE_JUMPING
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELSE
							PS_UPDATE_LOOK_CAM()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						// We should never get here
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_PARACHUTE_JUMPING
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) > 0.92
					OPEN_SEQUENCE_TASK(seqJump)				
						TASK_FORCE_MOTION_STATE(NULL, ENUM_TO_INT(MS_PARACHUTING))
						TASK_PARACHUTE(NULL, TRUE)
					CLOSE_SEQUENCE_TASK(seqJump)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqJump)
					CLEAR_SEQUENCE_TASK(seqJump)
				ENDIF
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
				    	IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 0.0
							PS_Main.myJumpCam.vLookCameraRot.z = CLAMP(PS_Main.myJumpCam.vLookCameraRot.z, -8.9090, 8.9090)
							PS_Main.myJumpCam.vBaseLookCamRot.z += PS_Main.myJumpCam.vLookCameraRot.z
							
							vCurFocalOffset = ROTATE_VECTOR_ABOUT_Z(PS_Main.myJumpCam.vFocalOffset, PS_Main.myJumpCam.vLookCameraRot.z)
							
							SET_CAM_PARAMS(PS_Main.previewCam1, PS_Main.myJumpCam.vFocalPoint + vCurFocalOffset, PS_Main.myJumpCam.vBaseLookCamRot, PS_Main.myJumpCam.fBaseLookCamFOV, 300)
							SET_CAM_ACTIVE_WITH_INTERP(PS_Main.previewCam2, PS_Main.previewCam1, FLOOR(1000.0 * (1.1 - 0.0)), GRAPH_TYPE_ACCEL)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 0.0
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							RENDER_SCRIPT_CAMS(FALSE, TRUE, FLOOR(1000.0 * (1.1 - 0.0)), FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 1.1
							SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), PS_Main.myVehicle, FALSE)
							IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "oddjobs@basejump@", "Heli_jump")
								DISPLAY_RADAR(TRUE)
								DISPLAY_HUD(TRUE)
								PS_HUD_RESET_TIMER()
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//								SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
								PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
								//QUEUE_OBJHELPALOGUE_HELP("PSCHUTE_2", -2, 1.5)	//"Use ~PAD_LSTICK_ALL~ to direct your free fall~n~
																				//Press ~PAD_A~ to deploy parachute~n~"
								IF IS_SCRIPT_GLOBAL_SHAKING()
									STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
								ENDIF
								
								CANCEL_TIMER(PS_Main.myJumpCam.camTimer)
								PS_Parachute_State = PS_PARACHUTE_SKYDIVING
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
		
			CASE PS_PARACHUTE_SKYDIVING
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINT_HELP("PSCHUTE_2")
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_POT", "PS_POT_2")//"Aim for the target and deploy your parachute when you start getting closer to the ground."
						iPSDialogueCounter++
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF IS_ENTITY_IN_WATER(PLAYER_PED_ID()) OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_LANDING
						OR (GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID) //AND (NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())))
							CLEAR_PRINTS()
							CLEAR_HELP()
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
							REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
							START_TIMER_NOW(PS_Main.tmrEndDelay)
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID
							OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_LANDING
								CLEAR_HELP()
							
							ELIF NOT PS_Main.myChallengeData.bPrintedChuteHelp
								IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_DEPLOYING
								OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
									CLEAR_HELP()
									PRINT_HELP("PSCHUTE_3")
									PS_Main.myChallengeData.bPrintedChuteHelp = TRUE
								ENDIF
							
							ELIF NOT PS_Main.myChallengeData.bPrintedLandingHelp
								vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
								IF vPlayerCoords.z <= 110.0
									CLEAR_HELP()
									PS_Main.myCheckpointz[0].position.z -= 2.0 //fix for building LOD messing with the marker drawing									
									IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
										PRINT_HELP("PSCHUTE_4_KM", DEFAULT_HELP_TEXT_TIME)
									ELSE
										PRINT_HELP("PSCHUTE_4", DEFAULT_HELP_TEXT_TIME)
									ENDIF	
									PS_Main.myChallengeData.bPrintedLandingHelp = TRUE
								ENDIF
							ENDIF
							
							IF PS_IS_PLAYER_OUT_OF_RANGE() AND PS_IS_RANGE_TIMER_UP()
								CLEAR_PRINTS()
								CLEAR_HELP()
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
								REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
								START_TIMER_NOW(PS_Main.tmrEndDelay)
								PS_INCREMENT_SUBSTATE()
								PRINTLN("FAILED BC PLAYER IS OUT OF RANGE")
								ePSFailReason = PS_FAIL_OUT_OF_RANGE
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Main.myChallengeData.bPrintedChuteHelp = FALSE
						PS_Main.myChallengeData.bPrintedLandingHelp = FALSE
						PS_Parachute_State = PS_PARACHUTE_PROCESS_DATA
						iInstructionCounter = 1
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_PARACHUTE_PROCESS_DATA
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						CERRORLN(DEBUG_MISSION, "PS_PARACHUTE_PROCESS_DATA -> ENTER")
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE	
						CERRORLN(DEBUG_MISSION, "PS_PARACHUTE_PROCESS_DATA -> UPDATE")
						IF GET_TIMER_IN_SECONDS(PS_Main.tmrEndDelay) > 0.25
							CANCEL_TIMER(PS_Main.tmrEndDelay)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CERRORLN(DEBUG_MISSION, "PS_PARACHUTE_PROCESS_DATA -> EXIT")
						PS_Parachute_State = PS_PARACHUTE_LANDING_CHECK()
						IF PS_Parachute_State <> PS_PARACHUTE_PROCESS_DATA
							iInstructionCounter = 1
							iPSDialogueCounter = 4 //should be 4
							//vAlphaPosition = PS_GET_CURRENT_CHECKPOINT()
							IF PS_Parachute_State = PS_PARACHUTE_SUCCESS
								PS_INITIALISE_CHECKPOINT_MARKER_FLASH(PS_GET_CURRENT_CHECKPOINT())
								PS_PLAY_CHECKPOINT_CHIME(PS_CHECKPOINT_OK)
							ENDIF
							bLessonCompleted = TRUE
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
												
			CASE PS_PARACHUTE_SUCCESS
				IF IS_PED_RAGDOLL(PLAYER_PED_ID())
					PRINTLN("PLAYER PED IS RAGDOLLING ON LANDING!!")
				ENDIF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						CERRORLN(DEBUG_MISSION, "PS_PARACHUTE_SUCCESS -> ENTER")
						IF ( bLesonFinishedLineTriggered = FALSE )
							PS_UPDATE_RECORDS(VECTOR_ZERO, TRUE)
							PS_RESET_CHECKPOINT_MGR(FALSE)
							PS_PLAY_LESSON_FINISHED_LINE("PS_POT", "PS_POT_5", "PS_POT_6", "PS_POT_7", "PS_POT_8")
							iPSHelpTextCounter++
							RESTART_TIMER_NOW(tmrLandingAlpha)
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
							IF GET_GAME_TIMER() - iEndDialogueTimer > 500
								PS_LESSON_END(TRUE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//PS_PARACHUTE_UPDATE_TARGET_WITH_ALPHA(vAlphaPosition, tmrLandingAlpha)
						CERRORLN(DEBUG_MISSION, "PS_PARACHUTE_SUCCESS -> UPDATE")
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						//PS_PARACHUTE_UPDATE_TARGET_WITH_ALPHA(vAlphaPosition, tmrLandingAlpha)
						CERRORLN(DEBUG_MISSION, "PS_PARACHUTE_SUCCESS -> EXIT")
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_PARACHUTE_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						CERRORLN(DEBUG_MISSION, "PS_PARACHUTE_FAIL -> ENTER")
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
							ELSE
								PS_UPDATE_RECORDS(VECTOR_ZERO, TRUE)
								IF ( bLessonCompleted = TRUE )
									PS_PLAY_LESSON_FINISHED_LINE("PS_POT", "PS_POT_5", "PS_POT_6", "PS_POT_7", "PS_POT_8")
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_POT", "PS_POT_5", "PS_POT_6", "PS_POT_7", "")
								ENDIF
								iPSHelpTextCounter++
								iEndDialogueTimer = GET_GAME_TIMER()
								RESTART_TIMER_NOW(tmrLandingAlpha)
								bLesonFinishedLineTriggered = TRUE
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//PS_PARACHUTE_UPDATE_TARGET_WITH_ALPHA(vAlphaPosition, tmrLandingAlpha)
						CERRORLN(DEBUG_MISSION, "PS_PARACHUTE_FAIL -> UPDATE")
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						//PS_PARACHUTE_UPDATE_TARGET_WITH_ALPHA(vAlphaPosition, tmrLandingAlpha)
						CERRORLN(DEBUG_MISSION, "PS_PARACHUTE_FAIL -> EXIT")
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
	
PROC PS_Parachute_MainCleanup()
	
	CANCEL_MUSIC_EVENT("MGPS_START")
	CANCEL_MUSIC_EVENT("MGPS_STOP")
	CANCEL_MUSIC_EVENT("MGPS_FAIL")
	
	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT, ePS_CamViewMode)
	
	IF IS_SCRIPT_GLOBAL_SHAKING()
		STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
	ENDIF
	
	IF NOT IS_SPECIAL_ABILITY_ENABLED(PLAYER_ID())
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
	ENDIF

//hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)

	SET_ALL_VEHICLE_GENERATORS_ACTIVE()

//player/objects	
	PS_PARACHUTE_KILL_TARGET_FLARE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
	ENDIF
	RESTORE_PLAYER_PED_VARIATIONS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
	SET_PED_VARIATIONS(PLAYER_PED_ID(), structPlayerProps)
	PS_SEND_PLAYER_BACK_TO_MENU(/*FALSE*/)
//cleanup checkpoints
	PS_RESET_CHECKPOINT_MGR()
	
//assets
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	SET_MODEL_AS_NO_LONGER_NEEDED(PilotModel)	
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Parachute.sc******************")
	#ENDIF	
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************
