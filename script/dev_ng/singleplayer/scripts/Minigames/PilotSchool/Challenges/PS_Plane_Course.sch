//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/13/11
//	Description:	"Plane Obstacle Course" Challenge for Pilot School. The player has to go through a series
//					of checkpoints placed precariously through the area around the airport. The player needs
//					avoid obstacles while also performing stunts through the coronas
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Plane Course
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Plane_Course_Init_Checkpoints()  
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-948.3597, -3367.6340, 69.5180>>)//0
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-240.2299, -3641.9639, 52.8490>>)//1
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_SIDEWAYS_L, <<504.4131, -3523.5945, 48.6284>>)//2
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<766.7775, -2990.6262, 68.1908>>)//3
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_INVERTED, <<794.2898, -2620.9307, 104.2130>>)//4
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<834.3015, -2324.5811, 88.7130>>)//5
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_SIDEWAYS_R, <<940.7796, -1969.1132, 80.2920>>)//6
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<982.0824, -1673.1564, 90.7920>>)//7
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<980.3978, -1393.1320, 105.0000>>)//8
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_SIDEWAYS_L, <<912.2797, -1168.7510, 105.0000>>)//9
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<650.5259, -1018.4594, 70.2440>>)//10
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<339.9143, -1047.7452, 67.0030>>)//11
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<34.0000, -971.0000, 64.7536>>)//12
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_SIDEWAYS_L, <<-309.0000, -865.0000, 70.3340>>)//13
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-530.2971, -964.9720, 74.7470>>)//14
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_SIDEWAYS_R, <<-829.1100, -1306.8101, 68.4979>>)//15
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_INVERTED, <<-1339.7749, -1638.0120, 40.0540>>)//16
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_SIDEWAYS_L, <<-1743.3560, -2152.1340, 64.2060>>)//17
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FINISH, <<-1698.6770, -2939.1848, 44.7581>>)//18	
ENDPROC

PROC PS_Plane_Course_Update_Dialogue()
	IF PS_IS_CHECKPOINT_MGR_ACTIVE() AND PS_IS_WAITING_FOR_CHECKPOINT_FEEDBACK() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		SWITCH(PS_GET_CHECKPOINT_PROGRESS())
			CASE 0
				BREAK
			CASE 1
				BREAK
			CASE 3
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_POC", "PS_POC_3")//"Get ready for the next gate. Since it's green you'll have to perform a knife through it."
				PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
				BREAK
				
			CASE 5		
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_POC", "PS_POC_4")//"This next gate is blue, so you're going to have to fly upside down through it."
				PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
				BREAK
			DEFAULT
				IF PS_GET_PREV_CHECKPOINT_TYPE() != PS_CHECKPOINT_OBSTACLE_PLANE AND PS_GET_PREV_CHECKPOINT_TYPE() != PS_CHECKPOINT_FINISH
					PS_PLAY_RANDOM_FEEDBACK_DIALOGUE(PS_GET_LAST_CHECKPOINT_SCORE())
					PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
FUNC BOOL PS_Plane_Course_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_Plane_Course_State)
			CASE PS_PLANE_COURSE_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_PLANE_COURSE_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_PLANE_COURSE_TAKEOFF
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, TRUE, FALSE, TRUE)
					RETURN TRUE
				ENDIF
				BREAK
			
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE, TRUE)
					RETURN TRUE
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    Also updates ChallengeState when we complete an objective 
FUNC BOOL PS_Plane_Course_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_Plane_Course_State)
			CASE PS_PLANE_COURSE_INIT
				RETURN FALSE
				BREAK

			CASE PS_PLANE_COURSE_COUNTDOWN
				RETURN FALSE
				BREAK
				
			CASE PS_PLANE_COURSE_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_PLANE_COURSE_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE	
ENDFUNC

PROC PS_PLANE_COURSE_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_Plane_Course_State = PS_PLANE_COURSE_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_PLANE_COURSE_FORCE_PASS
	PS_Plane_Course_State = PS_PLANE_COURSE_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Plane_Course_Initialise()
	//Start pos and heading are hard-coded since it different for each challenge.
	vStartPosition = <<-1601.2122, -2990.6001, 12.9693>> 
	vStartRotation = <<0.0000, 0.0000, 240.0000>>
	fStartHeading = 240.0000
	iFailDialogue = 1
	bLessonCompleted = FALSE
	bLesonFinishedLineTriggered	= FALSE
	
	bFinishedChallenge = FALSE
	VehicleToUse = STUNT

	PS_Plane_Course_Init_Checkpoints()

	PS_SETUP_CHALLENGE()
	PS_REQUEST_SHARED_ASSETS()
	
	iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_PLANE_COURSE
	REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, "PilotSchool")
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		WAIT(0)
	ENDWHILE
	
	fPreviewVehicleSkipTime = 45000
	fPreviewTime = 16
	
	vPilotSchoolSceneCoords = vStartPosition
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
		vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
	ENDIF
	
	//offset for checking whether player is in the air or on the ground
	PS_SET_GROUND_OFFSET(1.5)
	
	PS_CREATE_VEHICLE()
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Plane_Course.sc******************")
	#ENDIF
ENDPROC

PROC PS_Plane_Course_MainSetup()
	PS_Plane_Course_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	
	// Preventing planes from being generated in the two boarding locations, where planes normally pop into view as the challenge starts.
	VECTOR vGenBlockMin = <<-1514.927490,-3203.546387,26.694818>> - << 200.0, 200.0, 20.0 >>
	VECTOR VGenBlockMax = <<-1514.927490,-3203.546387,26.694818>> + << 200.0, 200.0, 20.0 >>
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, FALSE)
	
	NEW_LOAD_SCENE_START(<<-1618.1404, -2980.8284, 17.4172>>, NORMALISE_VECTOR(<<-1614.2157, -2983.0940, 17.0217>> - <<-1618.1404, -2980.8284, 17.4172>>), 5000.0)
ENDPROC

FUNC BOOL PS_Plane_Course_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE, iPreviewCheckpointIdx + 1, TRUE)
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV6", "PS_PREV6_1", "PS_PREV6_2", "PS_PREV6_3", "PS_PREV6_4")
				//"Now we're going to combine some of the stunts we learned with an obstacle course."
				//"For your sake, I hope you've been practicing knifing and flying upside-down. "
				//"Now, let's see you pull off extremely dangerous stunts around highly populated city areas!"
				BREAK
				
			CASE PS_PREVIEW_PLAYING
				//let vehicle recording play
				BREAK
				
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


PROC PS_PLANE_COURSE_UPDATE_MUSIC()
//	PS_PLANE_COURSE_INIT,
//	PS_PLANE_COURSE_CUTSCENE_INIT,
//	PS_PLANE_COURSE_CUTSCENE,
//	PS_PLANE_COURSE_COUNTDOWN,
//	PS_PLANE_COURSE_TAKEOFF,
//	PS_PLANE_COURSE_SUCCESS,
//	PS_PLANE_COURSE_FAIL,
//	PS_PLANE_COURSE_OBSTACLE,
//	PS_PLANE_COURSE_STAGE_TWO,
//	PS_PLANE_COURSE_STAGE_THREE
	
//	PS_MUSIC_EVENT_PREPARE_START,
//	PS_MUSIC_EVENT_TRIGGER_START,
//	PS_MUSIC_EVENT_PREPARE_STOP,
//	PS_MUSIC_EVENT_TRIGGER_STOP,
//	PS_MUSIC_EVENT_PREPARE_FAIL,
//	PS_MUSIC_EVENT_TRIGGER_FAIL
//FS_OBSTACLE_START
//FS_OBSTACLE_FAIL
//FS_OBSTACLE_STOP
//	
	//if the prepared flag is true, or if we're in the right state.
	IF NOT IS_BIT_SET(iPSMusicBits, ENUM_TO_INT(PS_MUSIC_EVENT_TRIGGER_START))
		IF PS_Plane_Course_State < PS_PLANE_COURSE_COUNTDOWN //make sure this triggers since INIT is only 3 frames long.
			IF MG_PREPARE_MUSIC_EVENT("FS_OBSTACLE_START", iPSMusicBits, PS_MUSIC_EVENT_PREPARE_START)
				PRINTLN("Attempting to trigger music event: FS_OBSTACLE_START")
				MG_TRIGGER_MUSIC_EVENT("FS_OBSTACLE_START", iPSMusicBits, PS_MUSIC_EVENT_TRIGGER_START)
			ENDIF
		ENDIF
	ELIF NOT IS_BIT_SET(iPSMusicBits, ENUM_TO_INT(PS_MUSIC_EVENT_TRIGGER_STOP)) AND NOT IS_BIT_SET(iPSMusicBits, ENUM_TO_INT(PS_MUSIC_EVENT_TRIGGER_FAIL))
		IF PS_Plane_Course_State = PS_PLANE_COURSE_SUCCESS
			IF MG_PREPARE_MUSIC_EVENT("FS_OBSTACLE_STOP", iPSMusicBits, PS_MUSIC_EVENT_PREPARE_STOP)
				PRINTLN("Attempting to trigger music event: FS_OBSTACLE_STOP")
				MG_TRIGGER_MUSIC_EVENT("FS_OBSTACLE_STOP", iPSMusicBits, PS_MUSIC_EVENT_TRIGGER_STOP)
			ENDIF
		ELIF PS_Plane_Course_State = PS_PLANE_COURSE_FAIL
			IF MG_PREPARE_MUSIC_EVENT("FS_OBSTACLE_FAIL", iPSMusicBits, PS_MUSIC_EVENT_PREPARE_FAIL)
				PRINTLN("Attempting to trigger music event: FS_OBSTACLE_FAIL")
				MG_TRIGGER_MUSIC_EVENT("FS_OBSTACLE_FAIL", iPSMusicBits, PS_MUSIC_EVENT_TRIGGER_FAIL)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL PS_Plane_Course_MainUpdate()
	IF TempDebugInput() AND NOT bFinishedChallenge

		PS_LESSON_UDPATE()
		PS_PLANE_COURSE_UPDATE_MUSIC()

		SWITCH(PS_Plane_Course_State)
		//Spawn player and plane on the runway
			CASE PS_PLANE_COURSE_INIT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Plane_Course_State = PS_PLANE_COURSE_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

			CASE PS_PLANE_COURSE_COUNTDOWN
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Plane_Course_State = PS_PLANE_COURSE_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving off
			CASE PS_PLANE_COURSE_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_SET_CHECKPOINT_COUNTER_ACTIVE(TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_POC", "PS_POC_1", "PS_POC", "PS_POC_2") //"Take your best shot at this obstacle course. There are going to be special gates that are going to require you to fly upside down or knife through them."
						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Plane_Course_Fail_Check()
							PS_Plane_Course_State = PS_PLANE_COURSE_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Plane_Course_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Plane_Course_State = PS_PLANE_COURSE_OBSTACLE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
	
		//Obstacle course loop
			CASE PS_PLANE_COURSE_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_KILL_HINT_CAM()
						PRINT_HELP("PS_HELP_GATE")	//"~g~green gate ~s~- Knife flight~n~"
													//"~b~blue gate ~s~- Inverted flight"
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Plane_Course_Fail_Check()
							PS_Plane_Course_State = PS_PLANE_COURSE_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Plane_Course_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELSE
							PS_Plane_Course_Update_Dialogue()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_Plane_Course_State = PS_PLANE_COURSE_SUCCESS
						ELSE
							PS_Plane_Course_State = PS_PLANE_COURSE_FAIL
						ENDIF
						bLessonCompleted = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
												
			CASE PS_PLANE_COURSE_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							PS_UPDATE_RECORDS(VECTOR_ZERO)
							PS_PLAY_LESSON_FINISHED_LINE("PS_POC", "PS_POC_5", "PS_POC_6", "PS_POC_7", "PS_POC_8")
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
							PS_LESSON_END(TRUE)
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_PLANE_COURSE_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
							ELSE
								PS_UPDATE_RECORDS(VECTOR_ZERO)
								IF ( bLessonCompleted = TRUE )
									PS_PLAY_LESSON_FINISHED_LINE("PS_POC", "PS_POC_5", "PS_POC_6", "PS_POC_7", "PS_POC_8")
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_POC", "PS_POC_5", "PS_POC_6", "PS_POC_7", "")
								ENDIF
								iEndDialogueTimer = GET_GAME_TIMER()
								bLesonFinishedLineTriggered = TRUE
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_Plane_Course_MainCleanup()
//hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()

//player/objects
	PS_SEND_PLAYER_BACK_TO_MENU()

//cleanup checkpoints
	PS_RESET_CHECKPOINT_MGR()
	
//assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	
	// Allow the blocked planes from being generated in the two boarding locations.
	VECTOR vGenBlockMin = <<-1514.927490,-3203.546387,26.694818>> - << 200.0, 200.0, 20.0 >>
	VECTOR VGenBlockMax = <<-1514.927490,-3203.546387,26.694818>> + << 200.0, 200.0, 20.0 >>
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, TRUE)
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Plane_Course.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************
