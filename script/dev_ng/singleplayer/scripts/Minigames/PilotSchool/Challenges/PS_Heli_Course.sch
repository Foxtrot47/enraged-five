//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/13/11
//	Description:	"Helicopter Obstacle Course" Challenge for Pilot School. 
//	
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

ENUM PS_HELI_COURSE_TUTORIAL
	PS_HELI_TUTORIAL_MSG_ONE,
	PS_HELI_TUTORIAL_MSG_TWO,
	PS_HELI_TUTORIAL_MSG_THREE,
	PS_HELI_TUTORIAL_DONE
ENDENUM

//CONSTS
CONST_INT MAX_HELICOURSE_CHECKPOINTS 	18
CONST_INT BRIDGE_CHECKPOINT 			5
CONST_INT CITY_CHECKPOINT 				11
CONST_INT PS_HELICOURSE_LANDING_CHECKPOINT 20
CONST_INT PS_HELICOURSE_NOCOMMENT_1 (PS_HELICOURSE_LANDING_CHECKPOINT - 1)
CONST_INT PS_HELICOURSE_NOCOMMENT_2 (PS_HELICOURSE_LANDING_CHECKPOINT - 2)
CONST_INT PS_HELICOURSE_NOCOMMENT_3 (PS_HELICOURSE_LANDING_CHECKPOINT - 3)

//Helicopter tutorial
BOOL bHeliTutorialInProgress = TRUE
structTimer	tHeliTutorialTimer
PS_HELI_COURSE_TUTORIAL eHelicopterTutorial

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Inverted Flight
//****************************************************************************************************
//****************************************************************************************************

PROC  PS_Heli_Course_Init_Checkpoints()
//	PS_RESET_CHECKPOINT_MGR()
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-1066.6219, -2785.1580, 80.0000>>)//0
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-712.5000, -2568.0000, 65.0000>>)//1
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-349.4300, -2355.2109, 30.0000>>)//2
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-75.4834, -2323.6079, 26.5000>>)//3
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<202.0167, -2323.5723, 46.4372>>)//4
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<352.7669, -2319.3196, 46.5509>>)//5
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<619.9371, -2232.8669, 38.5773>>)//6
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<672.0545, -1889.3560, 30.0000>>)//7
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<678.6499, -1584.5229, 30.0000>>)//8
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<541.2670, -1260.1210, 52.5070>>)//9
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<340.0385, -1049.9189, 70.4692>>)//10
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<30.0475, -970.9750, 95.0000>>)//11
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-175.3369, -898.9467, 125.0000>>)//12
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-271.1930, -951.4211, 150.9396>>)//13
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-564.6730, -1676.8342, 55.2971>>)//14
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-981.2974, -2634.3062, 105.1028>>)//15
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-1138.1230, -2858.7930, 74.8648>>)//16
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-1138.1230, -2858.7930, 100>>)//17
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-1138.1230, -2858.7930, 125>>)//18
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-1138.1230, -2858.7930, 150>>)//19
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FINISH, <<-1146.4918, -2864.2168, 13.0000>>)//20

	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-1066.6219, -2785.1580, 80.0000>>) //0
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-712.5000, -2568.0000, 65.0000>>) //1
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-349.4300, -2355.2109, 30.0000>>) //2
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-75.4834, -2323.6079, 26.5000>>) //3
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<202.0167, -2323.5723, 46.4372>>) //4
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<352.7669, -2319.3196, 46.5509>>) //5 
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<619.9371, -2232.8669, 38.5773>>) //6 
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<672.0545, -1889.3560, 30.0000>>) //7
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<678.6499, -1584.5229, 30.0000>>) //8
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<541.2670, -1260.1210, 52.5070>>) //9
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<340.0385, -1049.9189, 70.4692>>) //10
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<30.0475, -970.9750, 95.0000>>) //11
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-175.3369, -898.9467, 125.0000>>) //12
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-271.1930, -951.4211, 150.9396>>) //13
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-564.6730, -1676.8342, 55.2971>>) //14
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-981.2974, -2634.3062, 105.1028>>) //15
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-1114.8230, -2631.1001, 80.0000>>) //16
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-1226.4000, -2679.5000, 75.1000>>) //17
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-1266.9000, -2799.0000, 60.0000>>) //18
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_HELI, <<-1165.1230, -2860.7930, 40.7000>>) //19
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FINISH, <<-1146.4918, -2864.2168, 13.0000>>) //20
ENDPROC

/// PURPOSE:
///    Prints text for the helicopter intro tutorial
PROC Pilot_School_Update_Heli_Tutorial()
	SWITCH(eHelicopterTutorial)
		CASE PS_HELI_TUTORIAL_MSG_ONE
			CLEAR_HELP()
			DEBUG_MESSAGE("PSHELITUT_A")
			QUEUE_OBJHELPALOGUE_HELP("PSHELITUT_A", -1, 1.5) // "~s~Use ~PAD_RT~ to ascend and ~PAD_LT~ to descend."
			PLAY_OBJHELPALOGUE()
			CANCEL_TIMER(tHeliTutorialTimer)
			START_TIMER_NOW_SAFE(tHeliTutorialTimer)
			eHelicopterTutorial = PS_HELI_TUTORIAL_MSG_TWO
			BREAK
		CASE PS_HELI_TUTORIAL_MSG_TWO
			IF PS_GET_CHECKPOINT_PROGRESS() = 2 OR TIMER_DO_ONCE_WHEN_READY(tHeliTutorialTimer, 7.5)
				CLEAR_HELP()
				DEBUG_MESSAGE("PSHELITUT_B")
				PRINT_HELP("PSHELITUT_B")//"~s~Use ~PAD_LSTICK_ALL~ to control the nose of your vehicle."
				RESTART_TIMER_NOW(tHeliTutorialTimer)
				eHelicopterTutorial = PS_HELI_TUTORIAL_MSG_THREE
			ENDIF
			BREAK
		CASE PS_HELI_TUTORIAL_MSG_THREE
			IF PS_GET_CHECKPOINT_PROGRESS() >= 2 AND TIMER_DO_WHEN_READY(tHeliTutorialTimer, 5.0)
				CLEAR_HELP()
				DEBUG_MESSAGE("PSHELITUT_C")
				PRINT_HELP("PSHELITUT_C")//"~s~Use ~PAD_LB~ or ~PAD_RB~ to rotate left or right."
				RESTART_TIMER_NOW(tHeliTutorialTimer)
				eHelicopterTutorial = PS_HELI_TUTORIAL_DONE
			ENDIF
			BREAK
		CASE PS_HELI_TUTORIAL_DONE
			IF TIMER_DO_ONCE_WHEN_READY(tHeliTutorialTimer, 5.0)
				CLEAR_HELP()
				CANCEL_TIMER(tHeliTutorialTimer)
				bHeliTutorialInProgress = FALSE
			ENDIF
			BREAK
	ENDSWITCH
ENDPROC

PROC PS_Heli_Course_Update_Dialogue()
	IF PS_IS_CHECKPOINT_MGR_ACTIVE() AND PS_IS_WAITING_FOR_CHECKPOINT_FEEDBACK()
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			SWITCH(PS_GET_CHECKPOINT_PROGRESS())
				CASE 0
					//do nothing
					BREAK
				CASE 1
					//do nothing
					BREAK
				CASE BRIDGE_CHECKPOINT
					IF PS_GET_LAST_CHECKPOINT_SCORE() <> PS_CHECKPOINT_MISS
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_HOC", "PS_HOC_3")//"Nicely done! The bridge didn't seem to slow you down at all."
					ENDIF
					BREAK
				CASE CITY_CHECKPOINT
					PS_PLAY_DISPATCHER_INSTRUCTION("PS_HOC", "PS_HOC_4")//"You are heading into the city now, watch for street lights and buildings!"
					BREAK
				CASE PS_HELICOURSE_NOCOMMENT_3
				CASE PS_HELICOURSE_NOCOMMENT_2
				CASE PS_HELICOURSE_NOCOMMENT_1
					//do nothing
					BREAK
				CASE PS_HELICOURSE_LANDING_CHECKPOINT
					PS_PLAY_DISPATCHER_INSTRUCTION("PS_HOC", "PS_HOC_5")//"There's the landing pad. Go ahead and set 'er down easy."
					BREAK
				DEFAULT
					IF PS_GET_RANDOM_CHANCE()
						IF PS_GET_LAST_CHECKPOINT_SCORE() = PS_CHECKPOINT_MISS
							PS_PLAY_RANDOM_FEEDBACK_DIALOGUE(PS_GET_LAST_CHECKPOINT_SCORE())
						ELSE
							PS_PLAY_RANDOM_FEEDBACK_DIALOGUE(PS_CHECKPOINT_AWESOME) //since we dont have stunts, we always want rewarding feedback for passing these checkpoints
						ENDIF
					ENDIF
					PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
					BREAK
			ENDSWITCH
		ELSE
			PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Heli_Course_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_Heli_Course_State)
			CASE PS_HELI_COURSE_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_HELI_COURSE_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_HELI_COURSE_TAKEOFF
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE)
					RETURN TRUE
				ENDIF
				BREAK
			
			DEFAULT
				//Do default checks
				IF PS_GET_CHECKPOINT_PROGRESS() = PS_GET_TOTAL_CHECKPOINTS()
					//make sure we don't fail the player for landing
					IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE)
						RETURN TRUE
					ENDIF
				ELSE
					IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
				BREAK		
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    Also updates ChallengeState when we complete an objective 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Heli_Course_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_Heli_Course_State)
			CASE PS_HELI_COURSE_INIT
				RETURN FALSE
				BREAK

			CASE PS_HELI_COURSE_COUNTDOWN
				RETURN FALSE
				BREAK
	
			CASE PS_HELI_COURSE_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_HELI_COURSE_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK	
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE
ENDFUNC

PROC PS_HELI_COURSE_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_Heli_Course_State = PS_HELI_COURSE_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_HELI_COURSE_FORCE_PASS
	PS_Heli_Course_State = PS_HELI_COURSE_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_Heli_Course_Initialise()
	//Start pos and heading are hard-coded since it different for each challenge.
	vStartPosition = <<-1145.8069, -2864.4500, 13.6562>>
	vStartRotation = <<0.0000, 0.0000, 330.0000>>
	fStartHeading = 330.0
	iInstructionCounter = 1
	bFinishedChallenge = FALSE
	VehicleToUse = FROGGER
	iFailDialogue = 1
	bLessonCompleted = FALSE
	bLesonFinishedLineTriggered	= FALSE
	PS_Heli_Course_Init_Checkpoints()
	
	eHelicopterTutorial = PS_HELI_TUTORIAL_MSG_ONE
	
	PS_SETUP_CHALLENGE()
	PS_REQUEST_SHARED_ASSETS()
	
	iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_HELI_COURSE
	REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		WAIT(0)
	ENDWHILE	

	fPreviewVehicleSkipTime = 30000
	fPreviewTime = 18
	
	vPilotSchoolSceneCoords = vStartPosition
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
		vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
	ENDIF
	
	scenBlockHelipad = ADD_SCENARIO_BLOCKING_AREA( <<-1177.8660, -2802.9944, 11.3277>>, <<-1066.8466, -2953.8477, 16.1229>>)
	CLEAR_AREA(<<-1143.9893, -2865.0439, 12.9484>>, 25.0, TRUE, TRUE)
	
	PS_SET_GROUND_OFFSET(1.0)
	
	//Spawn player vehicle
	PS_CREATE_VEHICLE()
	
	SET_VEHICLE_RADIO_ENABLED(PS_Main.myVehicle, FALSE)
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Heli_Course.sc******************")
	#ENDIF
ENDPROC

PROC PS_Heli_Course_MainSetup()
	PS_Heli_Course_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
//	#IF IS_DEBUG_BUILD
//		PS_OBSTACLE_COURSE_CREATE_WIDGET(PS_GET_TOTAL_CHECKPOINTS()+1)
//	#ENDIF
	
	NEW_LOAD_SCENE_START(<<-1154.2679, -2879.1084, 17.7100>>, NORMALISE_VECTOR(<<-1152.3270, -2875.7458, 17.1644>> - <<-1154.2679, -2879.1084, 17.7100>>), 5000.0)
ENDPROC

FUNC BOOL PS_Heli_Course_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_HELI, iPreviewCheckpointIdx + 1, TRUE)
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV7", "PS_PREV7_1", "PS_PREV7_2", "PS_PREV7_3")
				//"For a change of pace we're going to put you inside a helicopter for this lesson."
				//"There are a few more controls than the stunt plane, but it's a little more forgiving."
				//"Try to finish the obstacle course without running into something or killing anyone."
				BREAK
			CASE PS_PREVIEW_PLAYING
				//let vehicle recording play
				BREAK
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL PS_Heli_Course_MainUpdate()	
	IF TempDebugInput() AND NOT bFinishedChallenge
	
		PS_LESSON_UDPATE()
		
//		#IF IS_DEBUG_BUILD
//			IF PS_Heli_Course_State	= PS_HELI_COURSE_OBSTACLE OR PS_Heli_Course_State = PS_HELI_COURSE_TAKEOFF 
//				PS_OBSTACLE_COURSE_UPDATE_WIDGET()
//			ENDIF
//		#ENDIF
		
		SWITCH(PS_Heli_Course_State)
		//Spawn player and helicopter
			CASE PS_HELI_COURSE_INIT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)						
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Heli_Course_State = PS_HELI_COURSE_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Play countdown
			CASE PS_HELI_COURSE_COUNTDOWN
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Heli_Course_State = PS_HELI_COURSE_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Give player control and start the helicopter intro tutorial
			CASE PS_HELI_COURSE_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//Setup objective, give the player controls and manage any new help text
						PS_SET_CHECKPOINT_COUNTER_ACTIVE(TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_HELI)
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_HOC", "PS_HOC_1", "PS_HOC", "PS_HOC_2") //"Let's go on a short tour of the city and see how well you handle the chopper."
						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Heli_Course_Fail_Check()
							PS_Heli_Course_State = PS_HELI_COURSE_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Heli_Course_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF bHeliTutorialInProgress
								Pilot_School_Update_Heli_Tutorial()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Heli_Course_State = PS_HELI_COURSE_OBSTACLE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Obstacle course loops
			CASE PS_HELI_COURSE_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_KILL_HINT_CAM()
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Heli_Course_Fail_Check()
							PS_Heli_Course_State = PS_HELI_COURSE_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Heli_Course_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF bHeliTutorialInProgress
								Pilot_School_Update_Heli_Tutorial()
							ELSE
								PS_Heli_Course_Update_Dialogue()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_UPDATE_RECORDS(PS_GET_CURRENT_CHECKPOINT())
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_Heli_Course_State = PS_HELI_COURSE_SUCCESS
						ELSE
							PS_Heli_Course_State = PS_HELI_COURSE_FAIL
						ENDIF
						bLessonCompleted = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
												
			CASE PS_HELI_COURSE_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							PS_UPDATE_RECORDS(PS_GET_LAST_CHECKPOINT())
							PS_PLAY_LESSON_FINISHED_LINE("PS_HOC", "PS_HOC_6", "PS_HOC_7", "PS_HOC_8", "PS_HOC_9")
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
							PS_LESSON_END(TRUE)
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_HELI_COURSE_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ELSE
								PS_UPDATE_RECORDS(PS_GET_LAST_CHECKPOINT())
								IF ( bLessonCompleted = TRUE )
									PS_PLAY_LESSON_FINISHED_LINE("PS_HOC", "PS_HOC_6", "PS_HOC_7", "PS_HOC_8", "PS_HOC_9")
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_HOC", "PS_HOC_6", "PS_HOC_7", "PS_HOC_8", "")
								ENDIF
								iEndDialogueTimer = GET_GAME_TIMER()
								bLesonFinishedLineTriggered = TRUE
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_Heli_Course_MainCleanup()
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()

	PS_SEND_PLAYER_BACK_TO_MENU()
	
	//cleanup checkpoints
	PS_RESET_CHECKPOINT_MGR()

	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	
	IF (scenBlockHelipad != NULL)
		REMOVE_SCENARIO_BLOCKING_AREA(scenBlockHelipad)
		scenBlockHelipad = NULL
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		PS_OBSTACLE_COURSE_DELETE_WIDGET()
//	#ENDIF
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Heli_Course.sc******************")
	#ENDIF	
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************

