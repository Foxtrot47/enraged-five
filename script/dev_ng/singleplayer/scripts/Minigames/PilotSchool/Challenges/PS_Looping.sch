 //****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/12/11
//	Description:	"Loop the Loop" Challenge for Pilot School. The player is required to do two loop-de-loops
//					and then an immelman to pass the challenge.
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Inverted Flight
//****************************************************************************************************
//****************************************************************************************************

PROC  PS_Challenge_Looping_Init_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1286.3641, -2124.3770, 62>>)
ENDPROC

//TODO: need to do use a better method for this stuff
PROC PS_Challenge_Looping_Tutorial_Text_Immelman()
	SWITCH iTutorialCounter
		CASE 1
			IF ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > LOOP_PREPARE_IMMELMAN_ANGLE
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_LOOP", "PS_LOOP_12")//"Once the plane's upside down, roll it over so that it's right side up."
				iTutorialCounter++
			ENDIF
			BREAK
	ENDSWITCH
ENDPROC

PROC PS_Challenge_Looping_Tutorial_Text_Loop()
	SWITCH iTutorialCounter
		CASE 1
			IF ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > LOOP_PREPARE_LEVEL_OUT_ANGLE
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_LOOP", "PS_LOOP_5")//"Get ready to level out the plane when you're near the end of the loop."
				iTutorialCounter++
			ENDIF
			BREAK
		CASE 2
			IF ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > LOOP_LEVEL_OUT_ANGLE
				PRINT_HELP("PSLOOP_F") //~s~Let go of ~PAD_LSTICK_NONE~ near the end of the loop
				iTutorialCounter++
			ENDIF
			BREAK
	ENDSWITCH
ENDPROC

PROC PS_LOOPING_PLAY_RETRY_DIALOGUE()
	IF IS_TIMER_STARTED(tRetryInstructionTimer) AND GET_TIMER_IN_SECONDS(tRetryInstructionTimer) > 10.0 AND TIMERA() > 15
		PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_7")	//"That wasn't quite right. Level out and retry the stunt from the beginning."
		RESTART_TIMER_NOW(tRetryInstructionTimer)
	ELIF IS_TIMER_STARTED(tRetryInstructionTimer)
		IF GET_TIMER_IN_SECONDS(tRetryInstructionTimer) > 10.0
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_7")	//"That wasn't quite right. Level out and retry the stunt from the beginning."
			RESTART_TIMER_NOW(tRetryInstructionTimer)
		ENDIF
	ELSE
		PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_7")	//"That wasn't quite right. Level out and retry the stunt from the beginning."
		RESTART_TIMER_NOW(tRetryInstructionTimer)
	ENDIF
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Challenge_Looping_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_Looping_State)
			CASE PS_LOOPING_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_LOOPING_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_LOOPING_TAKEOFF
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, TRUE, FALSE)
					RETURN TRUE
				ENDIF
				BREAK
			
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main)
					RETURN TRUE
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    Also updates ChallengeState when we complete an objective 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Challenge_Looping_Progress_Check()
	SWITCH(PS_Looping_State)
			CASE PS_LOOPING_INIT
				RETURN FALSE
				BREAK

			CASE PS_LOOPING_COUNTDOWN
				RETURN FALSE
				BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving offroad
			CASE PS_LOOPING_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1 OR PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LOOPING_TAKEOFF_REACT
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
				BREAK	
			
			CASE PS_LOOPING_LOOP_ONE_PREP
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_LOOPING_LOOP_ONE_START
				IF PS_IS_PLANE_CURRENTLY_LEVEL()
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_LOOPING_LOOP_ONE_PERFORM
				IF PS_GET_OBJECTIVE_CURRENT_ORIENT_COUNT() > 0
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_LOOPING_LOOP_ONE_REACT
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
				BREAK
			
			CASE PS_LOOPING_LOOP_TWO_PREP
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_LOOPING_LOOP_TWO_START
				IF PS_IS_PLANE_CURRENTLY_LEVEL()
					RETURN TRUE
				ENDIF
				BREAK
			
			CASE PS_LOOPING_LOOP_TWO_PERFORM
				IF PS_GET_OBJECTIVE_CURRENT_ORIENT_COUNT() > 0
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_LOOPING_LOOP_TWO_REACT
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
				BREAK
			
			CASE PS_LOOPING_IMMELMAN_PREP
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_LOOPING_IMMELMAN_START
				IF PS_IS_PLANE_CURRENTLY_LEVEL()
					RETURN TRUE
				ENDIF
				BREAK
			
			CASE PS_LOOPING_IMMELMAN_PERFORM_STEP_ONE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is upside down
						IF ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > LOOP_IMMELMAN_ANGLE AND (GET_ENTITY_ROLL(PS_Main.myVehicle) < STUNT_PLANE_INVERT_MIN OR GET_ENTITY_ROLL(PS_Main.myVehicle) > STUNT_PLANE_INVERT_MAX)
							//save heading
							RETURN TRUE
//						ELIF TIMERA() > 15
//							PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
		
			CASE PS_LOOPING_IMMELMAN_PERFORM_STEP_TWO
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is right side up
						IF (GET_ENTITY_PITCH(PS_Main.myVehicle) < -45 AND GET_ENTITY_PITCH(PS_Main.myVehicle) > -135) 
//						IF ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > LOOP_IMMELMAN_ANGLE_TOO_FAR
							CLEAR_HELP()
							CLEAR_PRINTS()
							PS_Looping_State = PS_LOOPING_IMMELMAN_PERFORM_STEP_ONE
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_IS_LEVEL_OUT_COMPLETE(PS_DEFAULT_LEVEL_OUT_HOLD_TIME)
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LOOPING_IMMELMAN_REACT
				IF PS_IS_LEVEL_OUT_COMPLETE(1.0)
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_LOOPING_LEVEL_OUT_THREE
					//do nothing
				BREAK	
		ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC PS_LOOPING_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_Looping_State = PS_LOOPING_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_LOOPING_FORCE_PASS
	PS_Looping_State = PS_LOOPING_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Looping_Initialise()
	PS_Challenge_Looping_Init_Checkpoints()

	//init vars
	PS_Looping_State = PS_LOOPING_INIT
	iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_LOOPING
	vStartPosition								= <<-1627.5929, -2715.2949, 12.9445>>
	vStartRotation 								= <<10.5441, 0.0000, 330.0000>>
	fStartHeading 								= 330.0000
	iPSDialogueCounter							= 1
	iPSHelpTextCounter							= 1
	iPSObjectiveTextCounter						= 1
	iFailDialogue								= 1
	bLessonCompleted							= FALSE
	bLesonFinishedLineTriggered					= FALSE
		
	//Player vehicle model
	VehicleToUse = STUNT
	PS_SET_GROUND_OFFSET(1.5)
	
	PS_SETUP_CHALLENGE()
	PS_REQUEST_SHARED_ASSETS()
	
	//load assets
	REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		WAIT(0)
	ENDWHILE
	
	fPreviewVehicleSkipTime = 44000
	fPreviewTime = 15
	
	vPilotSchoolSceneCoords = vStartPosition
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
		vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
	ENDIF
	
	//Spawn player vehicle			
	PS_CREATE_VEHICLE()
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Looping.sc******************")
	#ENDIF
ENDPROC

FUNC BOOL PS_Looping_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT, iPreviewCheckpointIdx, TRUE)
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV3", "PS_PREV3_1", "PS_PREV3_2", "PS_PREV3_3")
				//"The loop the loop is a little disorienting, so we'll see how you do under pressure."
				//"Make sure you start this stunt from a completely level position."
				//"It's also important to remember to pull your stick back as straight as you can so you don't get thrown into a spiral."
				BREAK
			CASE PS_PREVIEW_PLAYING
				//let vehicle recording play
				BREAK
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PS_Looping_MainSetup()
	PS_Challenge_Looping_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	
	NEW_LOAD_SCENE_START(<<-1637.2950, -2732.1042, 17.4674>>, NORMALISE_VECTOR(<<-1635.4385, -2728.8879, 17.1325>> - <<-1637.2950, -2732.1042, 17.4674>>), 5000.0)
ENDPROC

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_LOOPING_J_SKIP()
		PS_LOOPING_FLIGHT_CHALLENGE nextState = PS_Looping_State
		IF PS_Looping_State > PS_LOOPING_TAKEOFF
			IF PS_Looping_State < PS_LOOPING_LOOP_ONE_PREP
				nextState = PS_LOOPING_LOOP_ONE_PREP			
			ELIF PS_Looping_State < PS_LOOPING_LOOP_TWO_PREP
				nextState = PS_LOOPING_LOOP_TWO_PREP
			ELIF PS_Looping_State < PS_LOOPING_IMMELMAN_PREP
				nextState = PS_LOOPING_IMMELMAN_PREP
			ELIF PS_Looping_State < PS_LOOPING_LEVEL_OUT_THREE
				nextState = PS_LOOPING_LEVEL_OUT_THREE
			ENDIF
			IF nextState != PS_Looping_State AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				//send plane to the takeoff checkpoint
				SET_ENTITY_COORDS(PS_Main.myVehicle, PS_GET_CHECKPOINT_FROM_ID(1))
				SET_VEHICLE_FORWARD_SPEED(PS_Main.myVehicle, 60.0)
				PS_Looping_State = nextState
				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
				RETURN TRUE
			ENDIF	
		ENDIF
		RETURN FALSE
	ENDFUNC
#ENDIF	

FUNC BOOL PS_Looping_MainUpdate()			
	IF TempDebugInput() AND NOT bFinishedChallenge
		
		#IF IS_DEBUG_BUILD
			IF PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
				IF PS_LOOPING_J_SKIP()
					RETURN TRUE //if we do a j skip, then skip an update
				ENDIF
			ENDIF
		#ENDIF
		
		PS_LESSON_UDPATE()
		
		SWITCH(PS_Looping_State)
		
			CASE PS_LOOPING_INIT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Looping_State = PS_LOOPING_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

			CASE PS_LOOPING_COUNTDOWN
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Looping_State = PS_LOOPING_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving offroad
			CASE PS_LOOPING_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//Setup objective, give the player controls and manage any new help text
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT)
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_LOOP", "PS_LOOP_1", "PS_LOOP", "PS_LOOP_2")
						//"Take off and head for the marker at the end of the runway."
						//"Some pilots find these moves particularly tricky so it might take you a few tries."
						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Looping_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ELSE							
							IF iPSObjectiveTextCounter = 1 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
								PRINT("PSLOOP_A", DEFAULT_GOD_TEXT_TIME, 1)	//"~s~Head for the first ~y~checkpoint~s~."
							ENDIF
						ENDIF
						
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
						PS_SET_HINT_CAM_ACTIVE(FALSE)
						PS_KILL_HINT_CAM()
						PS_Looping_State = PS_LOOPING_TAKEOFF_REACT
						iInstructionCounter = 1
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LOOPING_TAKEOFF_REACT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						CLEAR_HELP()
						CLEAR_PRINTS()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Looping_State = PS_LOOPING_LOOP_ONE_PREP
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK	
			
			CASE PS_LOOPING_LOOP_ONE_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_LOOP", "PS_LOOP_3", "PS_LOOP", "PS_LOOP_4")
						//"Always make sure your plane is straightened out before you attempt an inside loop."
						//"On my mark, pull back hard on the flight stick and hold it."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Looping_State = PS_LOOPING_LOOP_ONE_START
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LOOPING_LOOP_ONE_START
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Looping_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						IF NOT PS_IS_PLANE_CURRENTLY_LEVEL()
							PRINT_NOW("PSBARREL_E", DEFAULT_GOD_TEXT_TIME, 1) //level out
						ENDIF	
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						PS_Looping_State = PS_LOOPING_LOOP_ONE_PERFORM
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LOOPING_LOOP_ONE_PERFORM
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_LOOPOBJ", "PS_LOOPOBJ_1") //"Perform an inside loop."		
//						PRINT("PSLOOP_B", 3000, 1)//"~s~Perform an inside loop."
						QUEUE_OBJHELPALOGUE_HELP("PSLOOP_C", -1, 1.5) //~s~Use ~PAD_LSTICK_DOWN~ to perform an inside loop.
						PLAY_OBJHELPALOGUE()
						PS_RESET_OBJECTIVE_DATA()
						SETTIMERA(0)
						PS_SET_LOOP_METER_ACTIVE(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Looping_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > LOOP_PREPARE_LEVEL_OUT_ANGLE AND iInstructionCounter = 1
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_LOOP", "PS_LOOP_5")	//"Get ready to level out the plane when you get near the end of the loop."
							iInstructionCounter++
						ELSE
							IF PS_GET_OBJECTIVE_RETRY_FLAG()
							AND (ABSF(fPlaneLastOrient) / PS_UI_ObjectiveMeter.fObjBarDemoninator) >= 0.33
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PS_SET_OBJECTIVE_RETRY_FLAG(FALSE)
								PS_Main.myObjectiveData.fPlaneTotalOrient = 0
								PS_LOOPING_PLAY_RETRY_DIALOGUE()
								SETTIMERA(0)
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_HELP()
						CLEAR_PRINTS()
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_LOOP_METER_ACTIVE(FALSE)
						iInstructionCounter = 1
						PS_Looping_State = PS_LOOPING_LOOP_ONE_REACT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LOOPING_LOOP_ONE_REACT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_LOOP", "PS_LOOP_6") //"That wasn't too bad for a rookie! Let's try that again."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Looping_State = PS_LOOPING_LOOP_TWO_PREP
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_LOOPING_LOOP_TWO_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_LOOP", "PS_LOOP_7")//"Just like before, steady the plane first, wait for my signal, and then pull back hard on the flight stick."
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_LOOP_METER_ACTIVE(TRUE)
						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Looping_State = PS_LOOPING_LOOP_TWO_START
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_LOOPING_LOOP_TWO_START
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Looping_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						IF NOT PS_IS_PLANE_CURRENTLY_LEVEL()
							PRINT_NOW("PSBARREL_E", DEFAULT_GOD_TEXT_TIME, 1) //level out
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_LOOP_METER_ACTIVE(TRUE)
						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
						iInstructionCounter = 1
						PS_Looping_State = PS_LOOPING_LOOP_TWO_PERFORM
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_LOOPING_LOOP_TWO_PERFORM
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_LOOPOBJ", "PS_LOOPOBJ_1") //"Perform an inside loop."
//						PRINT("PSLOOP_B", 3000, 1)//"~s~Perform an inside loop."
						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Looping_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > LOOP_PREPARE_LEVEL_OUT_ANGLE AND iInstructionCounter = 1
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_LOOP", "PS_LOOP_5")	//"Get ready to level out the plane when you get near the end of the loop."
							iInstructionCounter++
						ELSE
							IF PS_GET_OBJECTIVE_RETRY_FLAG()
							AND (ABSF(fPlaneLastOrient) / PS_UI_ObjectiveMeter.fObjBarDemoninator) >= 0.33
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PS_SET_OBJECTIVE_RETRY_FLAG(FALSE)
								PS_LOOPING_PLAY_RETRY_DIALOGUE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						CLEAR_HELP()
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_LOOP_METER_ACTIVE(FALSE)
						PS_Looping_State = PS_LOOPING_LOOP_TWO_REACT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LOOPING_LOOP_TWO_REACT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_LOOP", "PS_LOOP_8", "PS_LOOP", "PS_LOOP_9")
						//"Now we're talking! You're doing good so far let's squeeze in one more stunt."
						//"We'll combine the inside loop with the barrel roll to make an immelman."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						iInstructionCounter = 1
						PS_Looping_State = PS_LOOPING_IMMELMAN_PREP
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_LOOPING_IMMELMAN_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_LOOP", "PS_LOOP_10", "PS_LOOP", "PS_LOOP_11")
						//"Start out with an inside loop but only go halfway. When you're fully inverted then roll the plane over."
						//"Now when I give the go ahead, yank that flight stick back as hard as you can."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Looping_State = PS_LOOPING_IMMELMAN_START
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_LOOPING_IMMELMAN_START
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Looping_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						IF NOT PS_IS_PLANE_CURRENTLY_LEVEL()
							PRINT_NOW("PSBARREL_E", DEFAULT_GOD_TEXT_TIME, 1) //level out
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						iInstructionCounter = 1
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_LOOP_METER_ACTIVE(FALSE)
						PS_Looping_State = PS_LOOPING_IMMELMAN_PERFORM_STEP_ONE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_LOOPING_IMMELMAN_PERFORM_STEP_ONE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_LOOPOBJ", "PS_LOOPOBJ_2") //"Perform an inside loop."		
//						PRINT("PSLOOP_G", DEFAULT_GOD_TEXT_TIME, 1)//"~s~Complete an Immelman."
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_IMMELMAN_METER_ACTIVE(TRUE, PS_OBJECTIVE_TYPE_IMMELMAN_TURN)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Looping_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELSE
						IF PS_GET_OBJECTIVE_RETRY_FLAG()
						AND (ABSF(fPlaneLastOrient) / PS_UI_ObjectiveMeter.fObjBarDemoninator) >= 0.33
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_SET_OBJECTIVE_RETRY_FLAG(FALSE)
							PS_LOOPING_PLAY_RETRY_DIALOGUE()
						ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						iInstructionCounter = 1
						PS_SET_IMMELMAN_METER_ACTIVE(FALSE)
						PS_Looping_State = PS_LOOPING_IMMELMAN_PERFORM_STEP_TWO
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		
			CASE PS_LOOPING_IMMELMAN_PERFORM_STEP_TWO
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_LOOPOBJ", "PS_LOOPOBJ_4") //"roll the plane right side up"
//						PRINT_NOW("PSINVERT_K", DEFAULT_GOD_TEXT_TIME, 1) //"roll the plane right side up"
						PS_RESET_OBJECTIVE_DATA()
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Looping_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELSE
//							IF PS_GET_OBJECTIVE_RETRY_FLAG() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								PS_SET_OBJECTIVE_RETRY_FLAG(FALSE)
//								PS_Looping_State =  PS_LOOPING_IMMELMAN_PERFORM_STEP_ONE
//								PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
//							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Looping_State = PS_LOOPING_IMMELMAN_REACT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LOOPING_IMMELMAN_REACT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Looping_State = PS_LOOPING_LEVEL_OUT_THREE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LOOPING_LEVEL_OUT_THREE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						RESTART_TIMER_NOW(tLevelOutTimer)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Looping_Fail_Check()
							PS_Looping_State = PS_LOOPING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF TIMER_DO_ONCE_WHEN_READY(tLevelOutTimer, 1.0)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						CLEAR_HELP()
						PS_UPDATE_RECORDS(VECTOR_ZERO) //this was commented out, but without it the lesson will fail as the below command checks data that needs updated with the update_records command.
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_Looping_State = PS_LOOPING_SUCCESS
						ELSE
							PS_Looping_State = PS_LOOPING_FAIL
						ENDIF
						bLessonCompleted = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK	
									
			CASE PS_LOOPING_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							PS_UPDATE_RECORDS(VECTOR_ZERO)
							PS_PLAY_LESSON_FINISHED_LINE("PS_LOOP", "PS_LOOP_13", "PS_LOOP_14", "PS_LOOP_15", "PS_LOOP_16")
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
							PS_LESSON_END(TRUE)
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LOOPING_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
							ELSE
								PS_UPDATE_RECORDS(VECTOR_ZERO)
								IF ( bLessonCompleted = TRUE )
									PS_PLAY_LESSON_FINISHED_LINE("PS_LOOP", "PS_LOOP_13", "PS_LOOP_14", "PS_LOOP_15", "PS_LOOP_16")
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_LOOP", "PS_LOOP_13", "PS_LOOP_14", "PS_LOOP_15", "")
								ENDIF
								iEndDialogueTimer = GET_GAME_TIMER()
								bLesonFinishedLineTriggered = TRUE
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_Looping_MainCleanup()
//take care of hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()

//take care of objects
	//player first
	PS_SEND_PLAYER_BACK_TO_MENU()
	//then vehicle
	
	//then checkpoints
	PS_RESET_CHECKPOINT_MGR()

//loaded assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	
//dont cleaning up
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Looping.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************
