//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/08/11
//	Description:	"Upside-Down Flight" challenge for Pilot School. The player is told to do a couple stunts,
//					including barrel rolls and holding the plane upside down for a short duration.
//	
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Landing Flight
//****************************************************************************************************
//****************************************************************************************************

PROC  PS_Challenge_Landing_Init_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1617.3740, -2698.5027, 14.5794>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_LANDING, <<-1605.7271, -2793.0781, 14.5808>>)
	//<<-1605.7271, -2793.0781, 12.9444>>
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Challenge_Landing_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_Landing_State)
			CASE PS_LANDING_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_LANDING_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_LANDING_APPROACH_ALT
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE)
					RETURN TRUE
				ENDIF
				BREAK
			
			CASE PS_LANDING_APPROACH_SPEED
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE)
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_LANDING_RUNWAY
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, TRUE, TRUE, TRUE)
					RETURN TRUE
				ENDIF
				BREAK
			
			CASE PS_LANDING_TAXI
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, TRUE, TRUE, TRUE)
					RETURN TRUE
				ENDIF
				//if the player is farther from the taxi spot than the landing spot, then we've gone out of range. 
				IF NOT IS_ENTITY_IN_ANGLED_AREA( PS_Main.myVehicle, <<-1454.559814,-2802.258301,10>>, <<-1635.761841,-2698.190186, 17>>, 160)
					ePSFailReason = PS_FAIL_OUT_OF_RANGE
					RETURN TRUE
				ENDIF
				BREAK
			
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main)
					RETURN TRUE
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    When it returns TRUE, the main loop will increment the challenge state/stage
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_Challenge_Landing_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_Landing_State)
			CASE PS_LANDING_APPROACH_ALT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//Trigger the next state when the player gets close to the runway
						IF IS_ENTITY_AT_COORD(PS_Main.myVehicle, vDialogueMarker1, <<30, 30, 30>>, FALSE, FALSE)
							RETURN TRUE
						ELSE
							IF VDIST2(GET_ENTITY_COORDS(PS_Main.myVehicle), PS_GET_CHECKPOINT_FROM_ID(PS_GET_CHECKPOINT_PROGRESS())) < VDIST2(vDialogueMarker1, PS_GET_CHECKPOINT_FROM_ID(PS_GET_CHECKPOINT_PROGRESS()))
								RETURN TRUE
							ENDIF
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
								
			CASE PS_LANDING_APPROACH_SPEED
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//Trigger the next state when the player gets close to the runway
						IF IS_ENTITY_AT_COORD(PS_Main.myVehicle, vDialogueMarker2, <<30, 30, 30>>, FALSE, FALSE)
							RETURN TRUE
						ELSE
							IF VDIST2(GET_ENTITY_COORDS(PS_Main.myVehicle), PS_GET_CHECKPOINT_FROM_ID(PS_GET_CHECKPOINT_PROGRESS())) < VDIST2(vDialogueMarker2, PS_GET_CHECKPOINT_FROM_ID(PS_GET_CHECKPOINT_PROGRESS()))
								RETURN TRUE
							ENDIF
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_LANDING_RUNWAY
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player has finished landing
						IF IS_ENTITY_AT_COORD(PS_Main.myVehicle, vLandingSpot, <<30, 30, 30>>, FALSE, FALSE)
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_LANDING_TAXI
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player has parked the plane in the designated area
						IF IS_ENTITY_AT_COORD(PS_Main.myVehicle, PS_GET_CURRENT_CHECKPOINT(), <<15, 15, 15>>) 
							IF GET_ENTITY_SPEED(PS_Main.myVehicle) < 1
								//DELETE_CHECKPOINT(PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].checkpoint)
								PS_INCREMENT_CHECKPOINT()
								PS_Main.myCheckpointMgr.savedFinishTime = GET_TIMER_IN_SECONDS_SAFE(PS_Main.tRaceTimer)
								RETURN TRUE
							ENDIF
							
							IF (PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].checkpoint != NULL)
								IF IS_ENTITY_AT_COORD(PS_Main.myVehicle, PS_GET_CURRENT_CHECKPOINT(), <<10, 10, 10>>) 									
									//DELETE_CHECKPOINT(PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].checkpoint)
									//PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].checkpoint = NULL
									PS_Main.myCheckpointMgr.savedFinishTime = GET_TIMER_IN_SECONDS_SAFE(PS_Main.tRaceTimer)
									PRINTLN("[Flight School] >>>>>>>>>>> Plane is near final checkpoint and waiting to stop, turning off checkpoint manager")
								ENDIF													
							ENDIF
						ELIF (PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].checkpoint = NULL)
							PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
	ENDIF
	//only true when we're progressing
	RETURN FALSE
ENDFUNC

PROC PS_LANDING_FORCE_FAIL
	ePSFailReason 		= PS_FAIL_DEBUG
	PS_Landing_State 	= PS_LANDING_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_LANDING_FORCE_PASS
	PS_Landing_State 	= PS_LANDING_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_LANDING_J_SKIP()
		PS_LANDING_FLIGHT_CHALLENGE nextState = PS_Landing_State
		IF PS_Landing_State > PS_LANDING_APPROACH_ALT
			IF PS_Landing_State < PS_LANDING_APPROACH_SPEED
				nextState = PS_LANDING_APPROACH_SPEED
			ELIF PS_Landing_State < PS_LANDING_RUNWAY
				nextState = PS_LANDING_RUNWAY
			ELIF PS_Landing_State < PS_LANDING_TAXI
				nextState = PS_LANDING_TAXI
			ENDIF
			IF nextState != PS_Landing_State AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				//send plane to the takeoff checkpoint
//				SET_ENTITY_COORDS(PS_Main.myVehicle, PS_GET_CHECKPOINT_FROM_ID(1))
//				SET_VEHICLE_FORWARD_SPEED(PS_Main.myVehicle, 60.0)
//				PS_Landing_State = nextState
//				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
//				RETURN TRUE
			ENDIF	
		ENDIF
		RETURN FALSE
	ENDFUNC
#ENDIF

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Landing_Initialise()
	PS_Challenge_Landing_Init_Checkpoints()
	
	//init vars
	PS_Landing_State 							= PS_LANDING_INIT
	iPS_PreviewVehicleRecordingID 				= iPS_PEVIEW_VEH_REC_ID_LANDING
	vStartPosition								= <<-889.9432, -1437.7682, 125>> //<<-881.9342, -1380.8066, 125>>//<<-876.7917, -1402.2606, 125>>
	vStartRotation 								= <<-2.0, 0.0, 150.0>>
	fStartHeading 								= 150.0
	vPreviewCamCoords							= <<-1312.8, -2168.9, 67.7>>
	vPreviewCamDirection						= NORMALISE_VECTOR(<<-1318.1, -2173.6, 65.0>> - vPreviewCamCoords)
	vDialogueMarker1							= <<-1375.5485, -2278.7483, 12.9444>>//<<-1164.5254, -1886.3885, 100>>
	vDialogueMarker2							= <<-1531.9657, -2549.5588, 12.9444>>//<<-1595.1257, -2658.3257, 12.9444>>//<<-1429.6575, -2371.6216, 12.9444>>
	vLandingSpot								= <<-1617.3740, -2698.5027, 14.5794>>
	iPSDialogueCounter							= 1
	iPSHelpTextCounter							= 1
	iPSObjectiveTextCounter						= 1
	iFailDialogue								= 1
	bLessonCompleted							= FALSE
	bLesonFinishedLineTriggered					= FALSE
	
	//player vehicle model
	VehicleToUse = CUBAN800
	PS_SET_GROUND_OFFSET(1.5)
	
	PS_SETUP_CHALLENGE()	
	
	PS_REQUEST_SHARED_ASSETS()
	
	PS_Special_Fail_Reason = "PS_FAIL_7A"
	
	//load assets
	REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		PRINTLN("Flight School - Waiting on assets to load")
		WAIT(0)
	ENDWHILE
	
	//preview stuff
	fPreviewVehicleSkipTime 					= 11000
	fPreviewTime								= 10
	vPilotSchoolSceneCoords = vStartPosition
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
		vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
	ENDIF
	
	//Spawn player vehicle			
	PS_CREATE_VEHICLE()
	IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SET_VEHICLE_COLOUR_COMBINATION(PS_Main.myVehicle, 2)
		CONTROL_LANDING_GEAR(PS_Main.myVehicle, LGC_RETRACT_INSTANT)
		SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
	ENDIF
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Landing.sc******************")
	#ENDIF
	
ENDPROC
BOOL bdonestreamin = false
FUNC BOOL PS_Landing_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				IF DOES_CAM_EXIST(camMainMenu)
//					DESTROY_CAM(camMainMenu)
//				ENDIF
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				PS_Main.previewCam1 = CREATE_CAMERA()
				PS_Main.previewCam2 = CREATE_CAMERA()
				ATTACH_CAM_TO_ENTITY(PS_Main.previewCam1, PS_Main.myVehicle, <<-2, -6, 1>>, TRUE)
				POINT_CAM_AT_ENTITY(PS_Main.previewCam1, PS_Main.myVehicle, <<0, 0, 0>>)
//				ATTACH_CAM_TO_ENTITY(PS_Main.previewCam2, PS_Main.myVehicle, <<-3, -10, 3>>, TRUE)
//				POINT_CAM_AT_ENTITY(PS_Main.previewCam2, PS_Main.myVehicle, <<0, 0, 0>>)
				
//				SET_CAM_ACTIVE_WITH_INTERP(PS_Main.previewCam1, PS_Main.previewCam2, 5000, GRAPH_TYPE_LINEAR)
				SET_CAM_ACTIVE(PS_Main.previewCam1, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_LANDING, iPreviewCheckpointIdx, TRUE)	
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV02", "PS_PREV02_1", "PS_PREV02_2")
				//"Now I'm going to have you attempt a safe landing on the runway."
				//"You don't have too many chances so make sure you get this right and don't crash!"
				BREAK
			CASE PS_PREVIEW_PLAYING
				//let vehicle recording play
				SWITCH (PS_Main.customPreviewState)
					CASE PS_PREVIEW_CUSTOM_STATE_1
						IF GET_TIMER_IN_SECONDS(tPS_PreviewTimer) > 4.5
							DETACH_CAM(PS_Main.previewCam1)
							STOP_CAM_POINTING(PS_Main.previewCam1)
							SET_CAM_COORD(PS_Main.previewCam1, <<-1457.3, -2450.3, 15.3>>)
							SET_CAM_ROT(PS_Main.previewCam1, <<2.5, 0, -8.2>>)
							CONTROL_LANDING_GEAR(PS_Main.myVehicle, LGC_DEPLOY)
							bdonestreamin = FALSE
							PS_Main.customPreviewState = PS_PREVIEW_CUSTOM_STATE_2
						ELIF GET_TIMER_IN_SECONDS(tPS_PreviewTimer) = 3
							IF NOT bdonestreamin
								streamVolume = STREAMVOL_CREATE_FRUSTUM(<<-1457.3, -2450.3, 15.3>>, PS_CONVERT_ROTATION_TO_DIRECTION(<<2.5, 0, 138.0>>), 5000, FLAG_MAPDATA)
								bdonestreamin = TRUE
							ENDIF
						ENDIF
						BREAK
					CASE PS_PREVIEW_CUSTOM_STATE_2
						IF GET_TIMER_IN_SECONDS(tPS_PreviewTimer) > 7.5
							SET_CAM_COORD(PS_Main.previewCam1, <<-1457.3, -2450.3, 15.3>>)
							SET_CAM_ROT(PS_Main.previewCam1, <<2.5, 0, 138.0>>)
							PS_Main.customPreviewState = PS_PREVIEW_CUSTOM_STATE_3
						ENDIF
						BREAK
				ENDSWITCH
				
				BREAK
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PS_Landing_MainSetup()
	PS_Challenge_Landing_Init_Checkpoints()
	IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
		CONTROL_LANDING_GEAR(PS_Main.myVehicle, LGC_RETRACT_INSTANT)
		SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
		SET_ENTITY_COORDS_NO_OFFSET(PS_Main.myVehicle, vStartPosition)
		SET_ENTITY_ROTATION(PS_Main.myVehicle, vStartRotation)
	ENDIF
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	
	NEW_LOAD_SCENE_START(<<-934.1227, -1514.2896, 128.6280>>, NORMALISE_VECTOR(<<-947.9191, -1538.1844, 126.1967>> - <<-934.1227, -1514.2896, 128.6280>>), 5000.0)
ENDPROC

FUNC BOOL PS_Landing_MainUpdate()
	IF TempDebugInput() AND NOT bFinishedChallenge
		
		#IF IS_DEBUG_BUILD
			IF PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
				IF PS_LANDING_J_SKIP()
					RETURN TRUE //if we do a j skip, then skip an update
				ENDIF
			ENDIF
		#ENDIF
		
		PS_LESSON_UDPATE()
		
		IF PS_Landing_State = PS_LANDING_INIT OR PS_Landing_State = PS_LANDING_COUNTDOWN
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LEFT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_RIGHT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UP_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_DOWN_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
		ENDIF
		
		//for lining the plane up with the runway
//		IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle) 
//			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PS_Main.myVehicle), <<-1617.3740, -2698.5027, 14.5794>>)
//		ENDIF
		
		SWITCH(PS_Landing_State)
			CASE PS_LANDING_INIT
				PS_AUTO_PILOT(PS_Main, 70.0, FALSE)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						SET_ENTITY_COORDS_NO_OFFSET(PS_Main.myVehicle, vStartPosition)
						SET_ENTITY_ROTATION(PS_Main.myVehicle, vStartRotation)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Landing_State = PS_LANDING_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		
			CASE PS_LANDING_COUNTDOWN
				PS_AUTO_PILOT(PS_Main, 70.0, FALSE)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Landing_State = PS_LANDING_APPROACH_ALT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LANDING_APPROACH_ALT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_LANDING_APPROACH_ALT state")
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_LANDING)
						QUEUE_OBJHELPALOGUE_HELP("PS_TAXI_A", -1, 0.5)
						QUEUE_OBJHELPALOGUE_HELP("PS_TAXI_C", -1, 5.5) //deploy your gear
						PLAY_OBJHELPALOGUE()
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_TAXI", "PS_TAXI_1", "PS_TAXI", "PS_TAXI_2")
						//"Start reducing the plane's altitude gradually so you're just above the runway as you approach."
						//"Be sure to deploy the landing gear before you reach the runway."
						
						//hud stuff
						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Landing_Fail_Check()
							PS_Landing_State = PS_LANDING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Landing_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ENDIF
						
						IF PS_IS_LANDING_GEAR_DOWN()
							REMOVE_OBJHELPALOGUE_ITEM_FROM_QUEUE("PS_TAXI_C")
						ENDIF
						
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_HELP(FALSE)
						PRINTLN("[Flight School] >>>>>>>>>>> Exited PS_LANDING_APPROACH_ALT state")
						PS_Landing_State = PS_LANDING_APPROACH_SPEED
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LANDING_APPROACH_SPEED
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_LANDING_APPROACH_SPEED state")
						IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle) AND IS_ENTITY_IN_AIR(PS_Main.myVehicle)
							QUEUE_OBJHELPALOGUE_HELP("PS_TAXI_B", -1, 0.5)
							PLAY_OBJHELPALOGUE()
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_TAXI", "PS_TAXI_3")
							//"Start to decelerate while you're still in the air, but be careful not to stall the engines."
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Landing_Fail_Check()
							PS_Landing_State = PS_LANDING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Landing_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						
						IF PS_IS_LANDING_GEAR_DOWN()
							REMOVE_OBJHELPALOGUE_ITEM_FROM_QUEUE("PS_TAXI_C")
						ENDIF
						
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_HELP(FALSE)
						PRINTLN("[Flight School] >>>>>>>>>>> Exited PS_LANDING_APPROACH_SPEED state")
						PS_Landing_State = PS_LANDING_RUNWAY
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_LANDING_RUNWAY
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_LANDING_RUNWAY state")
						PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
						PS_SET_HINT_CAM_ACTIVE(FALSE)
						IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle) AND IS_ENTITY_IN_AIR(PS_Main.myVehicle)
							PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_TAXI", "PS_TAXI_4", "PS_TAXI", "PS_TAXI_5")
							//"When you touch down, continue to apply the brakes carefully until you come to a complete stop."
							//"Try to land as close as you can to the checkpoint marked on the runway."					
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Landing_Fail_Check()
							PS_Landing_State = PS_LANDING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Landing_Progress_Check()
							PS_INCREMENT_CHECKPOINT() 		//is this called twice? once here and once in checkpoint mgr?
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_HELP(FALSE)
						PRINTLN("[Flight School] >>>>>>>>>>> Exited PS_LANDING_RUNWAY state")
						PS_Landing_State = PS_LANDING_TAXI
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_LANDING_TAXI
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_LANDING_TAXI state")
						QUEUE_OBJHELPALOGUE_HELP("PS_TAXI_D", -1, 0.5)
						PLAY_OBJHELPALOGUE()
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_TAXI", "PS_TAXI_6", "PS_TAXI", "PS_TAXI_7")
						//"Executed like a pro! Now let's get the plane off the runway.  "
						//"Taxi over to the marked area and come to a complete stop."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_Challenge_Landing_Fail_Check()
							PS_Landing_State = PS_LANDING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_Challenge_Landing_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						
						IF DOES_ENTITY_EXIST(PS_Main.myVehicle) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)	
							IF IS_ENTITY_AT_COORD(PS_Main.myVehicle, PS_GET_CURRENT_CHECKPOINT(), <<40, 40, 10>>)
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PS_TAXI_E") AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PS_TAXI_D")
									PRINT_HELP_FOREVER("PS_TAXI_E")				
								ENDIF
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_HELP(FALSE)
						PRINTLN("[Flight School] >>>>>>>>>>> Exited PS_LANDING_TAXI state")
						PS_Landing_State = PS_LANDING_SUCCESS
						bLessonCompleted = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_LANDING_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_LANDING_SUCCESS state")
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF DOES_ENTITY_EXIST(PS_Main.myVehicle) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)	
								BRING_VEHICLE_TO_HALT(PS_Main.myVehicle, 1, 10)
							ENDIF
							PS_UPDATE_RECORDS(VECTOR_ZERO)
							iFailDialogue = 1
							PS_PLAY_LESSON_FINISHED_LINE("PS_LAND", "PS_LAND_6", "PS_LAND_7", "PS_LAND_7", "PS_LAND_9")
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
							PS_LESSON_END(TRUE)
							PS_INCREMENT_SUBSTATE()
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_LANDING_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("[Flight School] >>>>>>>>>>> Entered PS_LANDING_FAIL state")
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
							ELSE
								PS_UPDATE_RECORDS(VECTOR_ZERO)
								IF ( bLessonCompleted = TRUE )
									PS_PLAY_LESSON_FINISHED_LINE("PS_TAXI", "PS_TAXI_8", "PS_TAXI_8", "PS_TAXI_8", "PS_TAXI_9")
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_TAXI", "PS_TAXI_8", "PS_TAXI_8", "PS_TAXI_8", "")
								ENDIF
								iEndDialogueTimer = GET_GAME_TIMER()
								bLesonFinishedLineTriggered = TRUE
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_Landing_MainCleanup() 
//take care of hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	
//take care of objects
	//player first
	PS_SEND_PLAYER_BACK_TO_MENU()
	//then vehicle
	
	//then checkpoints
	PS_RESET_CHECKPOINT_MGR()

//loaded assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(STUNT)	
	
//done cleaning up
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************PS_Landing: done cleaning up lesson ******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************
