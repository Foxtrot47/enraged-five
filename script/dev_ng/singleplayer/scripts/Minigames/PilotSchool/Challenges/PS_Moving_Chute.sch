//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/14/11
//	Description:	"Parachute onto a Moving Target" Challenge for Pilot School. The player must skydive
//					from a piloted plane and land as close as possible to a dynamic target on the ground.
//	
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_Challenge_Helpers.sch"

CONST_FLOAT				PS_MOVING_TARGET_LOCATE_RANGE_X						3.0
CONST_FLOAT				PS_MOVING_TARGET_LOCATE_RANGE_Y						3.0
CONST_FLOAT				PS_MOVING_TARGET_LOCATE_RANGE_Z						3.0

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID ptfxwidget
	FLOAT tempfloat1 = PS_MOVE_TRUCK_TARGET_PTFX_SCALE
	FLOAT oldtempfloat1 = 0
	FLOAT tempfloat2 = 0
	FLOAT oldtempfloat2 = 0
	FLOAT tempfloat3 = 0
	FLOAT oldtempfloat3 = 0
#ENDIF




BLIP_INDEX				bPS_MovingTargetBlip
//VECTOR					vAirportCenter
//BOOL 					bPS_DidPlayerHitTruckSphere					= FALSE
//BOOL					bPS_WasHeadingStraight						= TRUE
	
//****************************************************************************************************
//****************************************************************************************************
//	Private functions
//****************************************************************************************************
//****************************************************************************************************	
	
PROC  PS_Challenge_Moving_Chute_Init_Checkpoints()
	vPS_MovingTarget = << -1048.5212, -3211.0261, 12.9443 >>
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, << -1048.5212, -3211.0261, 12.9443 >>)	//registering checkpoint for the out of range check
	//intentionally not activating checkpoint mgr
ENDPROC

/// PURPOSE:
///    See if the player has landed near the target, and how close.
FUNC PS_MOVING_CHUTE_FLIGHT_CHALLENGE PS_MOVING_CHUTE_LANDING_CHECK()
	VECTOR vPlayerOffset
	
	CERRORLN(DEBUG_MISSION, "Moving chute landing check...")
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		ePSFailReason = PS_FAIL_PLAYER_DEAD
		RETURN PS_MOVING_CHUTE_FAIL
	ENDIF
	
	IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		ePSFailReason = PS_FAIL_SUBMERGED
		RETURN PS_MOVING_CHUTE_FAIL
	ENDIF
	
	IF IS_ENTITY_DEAD(PS_FlatBedTrailer)
		ePSFailReason = PS_FAIL_DAMAGED_VEHICLE
		RETURN PS_MOVING_CHUTE_FAIL
	ENDIF
	
	vPlayerOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PS_FlatBedTrailer, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
	IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
		IF vPlayerOffset.z > 17.0
			ePSFailReason = PS_FAIL_REMOVED_PARACHUTE
			RETURN PS_MOVING_CHUTE_FAIL
		ELSE
			IF IS_PED_RAGDOLL(PLAYER_PED_ID()) AND NOT bRagdollingOnTruck
				bRagdollingOnTruck = TRUE
				SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 500, 1500, TASK_NM_SCRIPT) //set max ragdoll time
				SET_PED_RAGDOLL_ON_COLLISION(PLAYER_PED_ID(), FALSE)
			ENDIF	
			CERRORLN(DEBUG_MISSION, "Waiting a frame, the player is within 17m of the target...")
			RETURN PS_MOVING_CHUTE_PROCESS_DATA
				
		ENDIF
	ENDIF
	
	// Fail if we're too far above or below the flatbed.
	IF vPlayerOffset.z < -2.5 OR vPlayerOffset.z > 4.5
		ePSFailReason = PS_FAIL_TOO_FAR
		RETURN PS_MOVING_CHUTE_FAIL
	ENDIF
	
	// Give a gold with a distance of 0.0 if we're standing on the flatbed.
	IF NOT IS_ENTITY_DEAD(PS_FlatBedTrailer)
		IF IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), PS_FlatBedTrailer)
			PS_Main.myPlayerData.LandingDistance = 0.0
			iChallengeScores[g_current_selected_PilotSchool_class] = iSCORE_FOR_GOLD
			RETURN PS_MOVING_CHUTE_SUCCESS
		ENDIF
	ENDIF
	
	vPlayerOffset.x = FMAX(ABSF(vPlayerOffset.x) - 1.305, 0.0) // Half-width of flatbed
	IF vPlayerOffset.y >= 0.0
		vPlayerOffset.y = FMAX(vPlayerOffset.y - 5.98, 0.0) // Center to front
	ELSE
		vPlayerOffset.y = FMAX(-vPlayerOffset.y - 6.21, 0.0) // Center to back
	ENDIF
	
	// Give a gold if the player is directly above or below the flatbed.
	IF vPlayerOffset.x = 0.0 AND vPlayerOffset.y = 0.0
		PS_Main.myPlayerData.LandingDistance = 0.0
		iChallengeScores[g_current_selected_PilotSchool_class] = iSCORE_FOR_GOLD
		RETURN PS_MOVING_CHUTE_SUCCESS
	ENDIF
	
	// Measure distance if the player is directly to the left or right of the trailer.
	IF vPlayerOffset.y = 0.0
		IF vPlayerOffset.x > PS_Challenges[PSC_chuteOntoMovingTarg].BronzeDistance
			ePSFailReason = PS_FAIL_TOO_FAR
			RETURN PS_MOVING_CHUTE_FAIL
		ELSE
			PS_Main.myPlayerData.LandingDistance = vPlayerOffset.x
			IF PS_Main.myPlayerData.LandingDistance > PS_Challenges[PSC_chuteOntoMovingTarg].SilverDistance
				iChallengeScores[g_current_selected_PilotSchool_class] = iSCORE_FOR_BRONZE
			ELSE
				iChallengeScores[g_current_selected_PilotSchool_class] = iSCORE_FOR_SILVER
			ENDIF
			RETURN PS_MOVING_CHUTE_SUCCESS
		ENDIF
	ENDIF
	
	// Measure distance if the player is directly in front of or behind the trailer.
	IF vPlayerOffset.x = 0.0
		IF vPlayerOffset.y > PS_Challenges[PSC_chuteOntoMovingTarg].BronzeDistance
			ePSFailReason = PS_FAIL_TOO_FAR
			RETURN PS_MOVING_CHUTE_FAIL
		ELSE
			PS_Main.myPlayerData.LandingDistance = vPlayerOffset.y
			IF PS_Main.myPlayerData.LandingDistance > PS_Challenges[PSC_chuteOntoMovingTarg].SilverDistance
				iChallengeScores[g_current_selected_PilotSchool_class] = iSCORE_FOR_BRONZE
			ELSE
				iChallengeScores[g_current_selected_PilotSchool_class] = iSCORE_FOR_SILVER
			ENDIF
			RETURN PS_MOVING_CHUTE_SUCCESS
		ENDIF
	ENDIF
	
	// Get the distance from the nearest corner of the trailer.
	vPlayerOffset.x = vPlayerOffset.x * vPlayerOffset.x + vPlayerOffset.y * vPlayerOffset.y
	IF vPlayerOffset.x > PS_Challenges[PSC_chuteOntoMovingTarg].BronzeDistance * PS_Challenges[PSC_chuteOntoMovingTarg].BronzeDistance
		ePSFailReason = PS_FAIL_TOO_FAR
		RETURN PS_MOVING_CHUTE_FAIL
	ENDIF
	
	PS_Main.myPlayerData.LandingDistance = SQRT(vPlayerOffset.x)
	IF PS_Main.myPlayerData.LandingDistance > PS_Challenges[PSC_chuteOntoMovingTarg].SilverDistance
		iChallengeScores[g_current_selected_PilotSchool_class] = iSCORE_FOR_BRONZE
	ELSE
		iChallengeScores[g_current_selected_PilotSchool_class] = iSCORE_FOR_SILVER
	ENDIF
	RETURN PS_MOVING_CHUTE_SUCCESS
ENDFUNC

PROC PS_MOVING_CHUTE_FORCE_FAIL
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vPS_MovingTarget + <<0,0,10>>)
	ENDIF
	ePSFailReason = PS_FAIL_DEBUG
	PS_Moving_Chute_State = PS_MOVING_CHUTE_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC 

PROC PS_MOVING_CHUTE_FORCE_PASS
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vPS_MovingTarget)
	ENDIF
	PS_Moving_Chute_State = PS_MOVING_CHUTE_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC 


#IF IS_DEBUG_BUILD

	PROC DBG_INIT_PTFX_WIDGET()
	//	SET_CURRENT_WIDGET_GROUP(ptfxwidget)
		ptfxwidget = START_WIDGET_GROUP("ptfx tweaks (flight school)")
			ADD_WIDGET_FLOAT_SLIDER("Scale:", tempfloat1, -100, 100, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Emitter Size:", tempfloat2, -100, 100, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Alpha:", tempfloat3, 0, 1, 0.05)
		STOP_WIDGET_GROUP()
		SET_CURRENT_WIDGET_GROUP(ptfxwidget)
	ENDPROC


	PROC DBG_UPDATE_PTFX_WIDGET()
		IF oldtempfloat1 != tempfloat1
			oldtempfloat1 = tempfloat1
			SET_PARTICLE_FX_LOOPED_SCALE(PTFX_SkydivingTargetFlare, tempfloat1)
		ENDIF
		IF oldtempfloat2 != tempfloat2
			oldtempfloat2 = tempfloat2
			SET_PARTICLE_FX_LOOPED_EMITTER_SIZE(PTFX_SkydivingTargetFlare, TRUE, <<tempfloat2, tempfloat2, tempfloat2>>)
		ENDIF
		IF oldtempfloat3 != tempfloat3
			oldtempfloat3 = tempfloat3
			SET_PARTICLE_FX_LOOPED_ALPHA(PTFX_SkydivingTargetFlare, tempfloat3)
		ENDIF
		 
		
	ENDPROC
	
#ENDIF

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_MOVING_CHUTE_J_SKIP()
//		PS_MOVING_CHUTE_FLIGHT_CHALLENGE nextState = PS_Moving_Chute_State
//		IF PS_Moving_Chute_State > PS_MOVING_CHUTE_COUNTDOWN
//			IF PS_Moving_Chute_State < PS_MOVING_CHUTE_FREE_FALL
//				nextState = PS_MOVING_CHUTE_FREE_FALL
//				VECTOR tempVec = GET_ENTITY_COORDS(PLAYER_PED_ID())
//				tempVec.z -= 50
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), tempVec)
//			ELIF PS_Moving_Chute_State < PS_MOVING_CHUTE_FLOATING
//				nextState = PS_MOVING_CHUTE_FLOATING
//				FORCE_PED_TO_OPEN_PARACHUTE(PLAYER_PED_ID())
//			ELIF PS_Moving_Chute_State = PS_MOVING_CHUTE_FLOATING //if we're already floatin
//				nextState = PS_MOVING_CHUTE_LANDING //keep it floating and just
//			ENDIF
//			IF nextState != PS_Moving_Chute_State AND NOT IS_PED_INJURED(PLAYER_PED_ID())
//				PS_Moving_Chute_State = nextState
//				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
//				RETURN TRUE
//			ENDIF	
//		ENDIF
		RETURN FALSE
	ENDFUNC
#ENDIF	

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Moving_Chute_Initialise()
//	bPS_DidPlayerHitTruckSphere					= FALSE
	bFinishedChallenge							= FALSE
	VehicleToUse								= MAVERICK
	iPSHelpTextCounter							= 1
	iPSObjectiveTextCounter						= 1
	iPSDialogueCounter							= 1
	ppsPlayerParachuteState						= PPS_INVALID
	eParachutePreview							= PS_PARACHUTE_INIT
	iFailDialogue								= 1
	bLessonCompleted							= FALSE
	bLesonFinishedLineTriggered					= FALSE

	PS_Challenge_Moving_Chute_Init_Checkpoints()

	//setup autopilot path nodes
	vStartPosition = <<-846.7234, -3697.9106, 1248.2856>>
	fStartHeading = 102.2189
	
	vPreviewCamCoords = <<-1929.2, -2791.4, 616.3>>
	vPreviewCamDirection = NORMALISE_VECTOR(<<-1925.6, -2793.5, 614.9>> - vPreviewCamCoords)
	
	//TODO: do we still need this for the parachute?
	//Offset to calculate whether player is in the air or on the ground
	PS_SET_GROUND_OFFSET(1.5)	
	
	PS_SETUP_CHALLENGE()
	
	vPilotSchoolPreviewCoords = PS_MENU_CAM_COORDS
	
	MODEL_NAMES previewModel = GET_NPC_PED_MODEL(CHAR_DOM)
	
	REQUEST_WAYPOINT_RECORDING("ps_trucktarget_cw")
	REQUEST_ANIM_DICT("veh@helicopter@rps@base")
	REQUEST_ANIM_DICT("oddjobs@basejump@")
	PS_REQUEST_SHARED_ASSETS()
	
	REQUEST_MODEL(PHANTOM)
	REQUEST_MODEL(TRFLAT)
	REQUEST_MODEL(PilotModel)
	REQUEST_PTFX_ASSET()
	REQUEST_MODEL(previewModel)
	REQUEST_MODEL(PROP_PARAPACK_01)
	
	WHILE NOT PS_ARE_SHARED_ASSETS_LOADED()
		OR NOT HAS_MODEL_LOADED(previewModel)
		OR NOT HAS_ANIM_DICT_LOADED("veh@helicopter@rps@base")
		OR NOT HAS_ANIM_DICT_LOADED("oddjobs@basejump@")
		OR NOT HAS_MODEL_LOADED(PHANTOM)
		OR NOT HAS_MODEL_LOADED(TRFLAT)
		OR NOT HAS_MODEL_LOADED(PilotModel)
		OR NOT HAS_MODEL_LOADED(PROP_PARAPACK_01)
		OR NOT HAS_PTFX_ASSET_LOADED()
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("ps_trucktarget_cw")
		OR NOT CAN_CREATE_RANDOM_DRIVER()
		WAIT(0)
	ENDWHILE
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1366.7184, -1773.0824, -3.5000 >>, << -1526.6874, -3390.1799, 1.9358 >>, FALSE)
	
	//put player in the air and try to force him into freefalling
	
	//SET_ENTITY_HEADING(PLAYER_PED_ID(), 239.6148)
	SETTIMERA(0)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			SET_PED_COMP_ITEM_CURRENT_SP( PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BLUE_BOILER_SUIT, FALSE )
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 5, 0) 		
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			SET_PED_COMP_ITEM_CURRENT_SP( PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_SKYDIVING, FALSE )
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 1, 0)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			SET_PED_COMP_ITEM_CURRENT_SP( PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_BLUE_BOILER_SUIT, FALSE )
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0) 
		ENDIF
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 2, TRUE)
		
//		WHILE NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
//			WAIT(0)
//		ENDWHILE
		
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), << -1925.6293, -2793.5227, 700.0000 >>)//<< -1090.4011, -3326.0393, 1030.2483 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 239.6148)
		//SET_PED_GRAVITY(PLAYER_PED_ID(), TRUE)
		//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		//SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		//SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	PS_Main.prevPed = CREATE_PED(PEDTYPE_MISSION, previewModel, << -1925.6293, -2793.5227, 650.0000 >>)
	IF NOT IS_PED_INJURED(PS_Main.prevPed )
		SET_ENTITY_HEADING(PS_Main.prevPed, 239.6148)
		GIVE_WEAPON_TO_PED(PS_Main.prevPed , GADGETTYPE_PARACHUTE, 1, TRUE)

		WHILE NOT HAS_PED_GOT_WEAPON(PS_Main.prevPed, GADGETTYPE_PARACHUTE)
			WAIT(0)
		ENDWHILE
		SET_PED_GADGET(PS_Main.prevPed, GADGETTYPE_PARACHUTE, TRUE)
		
		SET_PED_GRAVITY(PS_Main.prevPed, TRUE)
		CLEAR_PED_TASKS_IMMEDIATELY(PS_Main.prevPed)
		TASK_SKY_DIVE(PS_Main.prevPed)
		WHILE (NOT IS_ENTITY_DEAD(PS_Main.prevPed)) AND GET_PED_PARACHUTE_STATE(PS_Main.prevPed) <> PPS_SKYDIVING AND TIMERA() < 3000
//			FORCE_PED_MOTION_STATE(PS_Main.prevPed, MS_PARACHUTING)
			WAIT(0)
		ENDWHILE
	ENDIF
	
ENDPROC

PROC PS_Moving_Chute_MainSetup()

//#IF IS_DEBUG_BUILD
//	DBG_INIT_PTFX_WIDGET()
//#ENDIF
	VECTOR vBaseRotCopy
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF
	
	//REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)	
	PS_PREVIEW_CLEANUP_CUTSCENE()
	
	CLEAR_AREA(vStartPosition, 1600, TRUE)
	
	//create truck target so we can use the player hack	
	PS_MOVING_CHUTE_CREATE_TARGET_TRUCK()
	
	IF NOT IS_ENTITY_DEAD(PS_Main.prevPed)
		DELETE_PED(PS_Main.prevPed)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PS_Main.previewDummy)
		DELETE_PED(PS_Main.previewDummy)
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_DOM))
	
	//Spawn heli and turn it on
	IF NOT DOES_ENTITY_EXIST(PS_Main.myVehicle)
		PS_Main.myVehicle = CREATE_VEHICLE(VehicleToUse, vStartPosition, fStartHeading)
	ELSE
		SET_VEHICLE_FIXED(PS_Main.myVehicle)
		SET_ENTITY_COORDS(PS_Main.myVehicle, vStartPosition)
		SET_ENTITY_HEADING(PS_Main.myVehicle, fStartHeading)
	ENDIF
	
	SET_VEHICLE_USED_FOR_PILOT_SCHOOL(PS_Main.myVehicle, TRUE)
	SET_VEHICLE_HAS_STRONG_AXLES(PS_Main.myVehicle, TRUE)
	SET_ENTITY_AS_MISSION_ENTITY(PS_Main.myVehicle)
	SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, TRUE, TRUE)
	FREEZE_ENTITY_POSITION(PS_Main.myVehicle, TRUE)
	
	IF NOT DOES_ENTITY_EXIST(PS_Main.myPilotPed)
		PS_Main.myPilotPed = CREATE_PED_INSIDE_VEHICLE(PS_Main.myVehicle, PEDTYPE_MISSION, PilotModel, VS_DRIVER)
	ENDIF
	
//	SET_ENTITY_INVINCIBLE(PS_Main.myPilotPed, TRUE)
	
	SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1)
		
		IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
			iSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
			ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneID, PS_Main.myVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(PS_Main.myVehicle, "Chassis"))
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneID, "oddjobs@basejump@", "Heli_door_loop", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS)
			SET_SYNCHRONIZED_SCENE_LOOPED(iSceneID, TRUE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		// Change the player's clothing.
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 5, 0) 		
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 1, 0)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0) 
		ENDIF
	ENDIF
	
	PS_MOVING_CHUTE_CREATE_TARGET_TRUCK()
	
	PS_Challenge_Moving_Chute_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	
	PS_Main.myJumpCam.vLookCameraRot = <<10.2986, 0, 8.9090>>
	PS_Main.myJumpCam.vFocalOffset = 1.2 * <<SIN(-97.4239 + fStartHeading), -COS(-97.4239 + fStartHeading), 0.0>>
	PS_Main.myJumpCam.vFocalPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle, <<1.12046, -0.317773, 1.3385>>)
	VECTOR vCurFocalOffset = ROTATE_VECTOR_ABOUT_Z(PS_Main.myJumpCam.vFocalOffset, PS_Main.myJumpCam.vLookCameraRot.z)
	PS_Main.myJumpCam.vBaseLookCamRot = <<-33.0000 + PS_Main.myJumpCam.vLookCameraRot.x, 0.0, -88.515 + fStartHeading>>
	PS_Main.myJumpCam.fBaseLookCamFOV = 26.0
	
	vBaseRotCopy = PS_Main.myJumpCam.vBaseLookCamRot
	vBaseRotCopy.z += PS_Main.myJumpCam.vLookCameraRot.z
	
	IF DOES_CAM_EXIST(PS_Main.previewCam1)
		DESTROY_CAM(PS_Main.previewCam1)
	ENDIF
	
	NEW_LOAD_SCENE_START(PS_Main.myJumpCam.vFocalPoint + PS_Main.myJumpCam.vFocalOffset, PS_Main.myJumpCam.vFocalOffset, 7000.0)
	PS_Main.previewCam1 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,
		PS_Main.myJumpCam.vFocalPoint + vCurFocalOffset, vBaseRotCopy, PS_Main.myJumpCam.fBaseLookCamFOV, TRUE)
	SET_CAM_NEAR_CLIP(PS_Main.previewCam1, PS_GET_NEAR_CLIP())
	SHAKE_SCRIPT_GLOBAL("SKY_DIVING_SHAKE", 0.15)
	
	PS_Main.myJumpCam.vCameraVelocity = <<0,0,0>>
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_Moving_Chute.sc******************")
	#ENDIF
ENDPROC

PROC PS_Moving_Chute_Updating_Preview()
	SWITCH (eParachutePreview)
		CASE PS_PARACHUTE_INIT
			//put player in the air and 
			fPreviewTime = 20
			RESTART_TIMER_NOW(tParachutePreviewTimer)
			IF NOT IS_VEHICLE_FUCKED(PS_FlatBedTruck)
				vPS_MovingTarget = GET_ENTITY_COORDS(PS_FlatBedTruck)
			ENDIF
			eParachutePreview = PS_PARACHUTE_JUMPING
			BREAK

		CASE PS_PARACHUTE_JUMPING
			//wait for a while then pull yor chute?
			IF TIMER_DO_WHEN_READY(tParachutePreviewTimer, 5.0)
				IF NOT IS_ENTITY_DEAD(PS_Main.prevPed)
					TASK_PARACHUTE_TO_TARGET(PS_Main.prevPed, vPS_MovingTarget)
				ENDIF
				eParachutePreview = PS_PARACHUTE_SKYDIVING
			ENDIF
			BREAK
		
		CASE PS_PARACHUTE_SKYDIVING
			IF TIMER_DO_WHEN_READY(tParachutePreviewTimer, 7.0)
				IF NOT IS_ENTITY_DEAD(PS_Main.prevPed)
					eParachutePreview = PS_PARACHUTE_WAIT
					SET_PED_KEEP_TASK(PS_Main.prevPed, TRUE)
					SET_PARACHUTE_TASK_TARGET(PS_Main.prevPed, vPS_MovingTarget)
				ENDIF
			ENDIF
			BREAK
	
		CASE PS_PARACHUTE_WAIT
			//do nothing
			BREAK
	
	ENDSWITCH
	
ENDPROC

FUNC BOOL PS_Moving_Chute_Preview()
	SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(PS_Main.prevPed)
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				//PS_PARACHUTE_START_TRUCK_TARGET(bPS_MovingTargetBlip)
				PS_PREVIEW_DIALOGUE_PLAY("PS_PREV10", "PS_PREV10_1", "PS_PREV10_2", "PS_PREV10_3", "PS_PREV10_4", "PS_PREV10_5")
				//"This lesson will involve skydiving onto a moving target,"
				//"so I hope you brought a change of underwear."
				//"The target will be circling around the runway, so watch it carefully and pay attention to the route!"
				//"It may take several tries, but once you get this down"
				//"you'll be terrorizing drivers all over Los Santos."

				BREAK
			CASE PS_PREVIEW_PLAYING
				//do preview cutscene here
				PS_Moving_Chute_Updating_Preview()
				BREAK
			CASE PS_PREVIEW_FADE_END
				//Setup vars and vehicle
				BREAK
			CASE PS_PREVIEW_CLEANUP
				//cleanup if we reach the end of the preview before skipping
				BREAK	
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//PROC DBG_UPDATE_MOVING_TARGET()
//
//	IF IS_BUTTON_JUST_PRESSED(PAD1, DPADUP)
//		tempfloat++
//		SET_PARTICLE_FX_LOOPED_SCALE(PTFX_SkydivingTargetFlare, tempfloat1)
//		PRINTLN("new value is ... ", tempfloat1)
////								SET_PARTICLE_FX_LOOPED_EVOLUTION(PTFX_SkydivingTargetFlare, tempevolution)
////								SET_PARTICLE_FX_LOOPED_ALPHA
////								SET_PARTICLE_FX_LOOPED_SCALE
////								SET_PARTICLE_FX_LOOPED_EMITTER_SIZE
//	ELIF IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
//		tempfloat--
//		SET_PARTICLE_FX_LOOPED_SCALE(PTFX_SkydivingTargetFlare, tempfloat1)
//		PRINTLN("new value is ... ", tempfloat1)
//	ELIF IS_BUTTON_JUST_PRESSED(PAD1, DPADRIGHT)
//		tempfloat += 0.1
//		SET_PARTICLE_FX_LOOPED_SCALE(PTFX_SkydivingTargetFlare, tempfloat1)
//		PRINTLN("new value is ... ", tempfloat1)
//	ELIF IS_BUTTON_JUST_PRESSED(PAD1, DPADLEFT)
//		tempfloat -= 0.1
//		SET_PARTICLE_FX_LOOPED_SCALE(PTFX_SkydivingTargetFlare, tempfloat1)
//		PRINTLN("new value is ... ", tempfloat1)
//	ENDIF
//ENDPROC

PROC PS_MOVING_CHUTE_UPDATE_MUSIC()
//	PS_MOVING_CHUTE_SUCCESS,
//	PS_MOVING_CHUTE_FAIL,
//	PS_MOVING_CHUTE_INIT,
//	PS_MOVING_CHUTE_COUNTDOWN,
//	PS_MOVING_CHUTE_PREJUMP,
//	PS_MOVING_CHUTE_JUMPING,
//	PS_MOVING_CHUTE_FREE_FALL,
//	PS_MOVING_CHUTE_DEPLOYING,
//	PS_MOVING_CHUTE_FLOATING,
//	PS_MOVING_CHUTE_LANDING,
//	PS_MOVING_CHUTE_CRASHING
	
//	PS_MUSIC_EVENT_PREPARE_START,
//	PS_MUSIC_EVENT_TRIGGER_START,
//	PS_MUSIC_EVENT_PREPARE_STOP,
//	PS_MUSIC_EVENT_TRIGGER_STOP,
//	PS_MUSIC_EVENT_PREPARE_FAIL,
//	PS_MUSIC_EVENT_TRIGGER_FAIL
//
//	MGPS_START
//	MGPS_FAIL
//	MGPS_STOP
//	
	//if the prepared flag is true, or if we're in the right state.
	IF NOT IS_BIT_SET(iPSMusicBits, ENUM_TO_INT(PS_MUSIC_EVENT_TRIGGER_START))
		IF PS_Moving_Chute_State >= PS_MOVING_CHUTE_JUMPING
			IF MG_PREPARE_MUSIC_EVENT("MGPS_START", iPSMusicBits, PS_MUSIC_EVENT_PREPARE_START)
				PRINTLN("Attempting to trigger music event: MGPS_START")
				MG_TRIGGER_MUSIC_EVENT("MGPS_START", iPSMusicBits, PS_MUSIC_EVENT_TRIGGER_START)
			ENDIF
		ENDIF
	ELIF NOT IS_BIT_SET(iPSMusicBits, ENUM_TO_INT(PS_MUSIC_EVENT_TRIGGER_STOP)) AND NOT IS_BIT_SET(iPSMusicBits, ENUM_TO_INT(PS_MUSIC_EVENT_TRIGGER_FAIL))
		IF PS_Moving_Chute_State = PS_MOVING_CHUTE_SUCCESS
			IF MG_PREPARE_MUSIC_EVENT("MGPS_STOP", iPSMusicBits, PS_MUSIC_EVENT_PREPARE_STOP)
				PRINTLN("Attempting to trigger music event: MGPS_STOP")
				MG_TRIGGER_MUSIC_EVENT("MGPS_STOP", iPSMusicBits, PS_MUSIC_EVENT_TRIGGER_STOP)
			ENDIF
		ELIF PS_Moving_Chute_State = PS_MOVING_CHUTE_FAIL
			IF MG_PREPARE_MUSIC_EVENT("MGPS_STOP", iPSMusicBits, PS_MUSIC_EVENT_PREPARE_FAIL)
				PRINTLN("Attempting to trigger music event: MGPS_STOP")
				MG_TRIGGER_MUSIC_EVENT("MGPS_STOP", iPSMusicBits, PS_MUSIC_EVENT_TRIGGER_FAIL)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL PS_IS_PLAYER_ON_TARGET_VEHICLE(VEHICLE_INDEX vehTarget)
	VEHICLE_INDEX tempTrailer
	VECTOR vTargetVehiclePos
	VECTOR vPlayerPos
	
	IF (NOT IS_PED_INJURED(PLAYER_PED_ID())) AND DOES_ENTITY_EXIST(vehTarget) AND IS_VEHICLE_DRIVEABLE(vehTarget)
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		PRINTLN("Checking if player is touching vehicle")
		IF DOES_ENTITY_EXIST(vehTarget)
			GET_VEHICLE_TRAILER_VEHICLE(vehTarget, tempTrailer)
			IF IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), vehTarget)
			OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), vehTarget) 
				IF vTargetVehiclePos.z < vPlayerPos.z
					RETURN TRUE
				ELSE
					PRINTLN("Player z pos is lower than vehicle z pos!")
				ENDIF
			ELSE
				PRINTLN("Player not touching vehicle entity!")
			ENDIF
		ELSE
			PRINTLN("Vehicle is dead!")
		ENDIF
		
		PRINTLN("Checking if player is touching trailer")
		IF DOES_ENTITY_EXIST(tempTrailer) 
			IF NOT IS_ENTITY_DEAD(tempTrailer) 
				IF IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), tempTrailer)
				OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), tempTrailer)
					IF vTargetVehiclePos.z < vPlayerPos.z
						RETURN TRUE
						
					ELSE
						PRINTLN("Player z pos is lower than Trailer z pos!")
					ENDIF
				ELSE
					PRINTLN("Player not touching Trailer entity!")
				ENDIF
			ELSE
				PRINTLN("Trailer is dead!")
			ENDIF
		ELSE
			PRINTLN("Trailer doesn't exist!")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_Moving_Chute_MainUpdate()	
	SEQUENCE_INDEX seqJump
	VECTOR vPlayerCoords, vCurFocalOffset
	
	IF TempDebugInput() AND NOT bFinishedChallenge

		#IF IS_DEBUG_BUILD
			IF PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
				IF PS_MOVING_CHUTE_J_SKIP()
					RETURN TRUE //if we do a j skip, then skip an update
				ENDIF
			ENDIF
		#ENDIF

		PS_LESSON_UDPATE()
		PS_MOVING_CHUTE_UPDATE_MUSIC()
		
		//Disable special ability b*2248190
		IF IS_SPECIAL_ABILITY_ENABLED(PLAYER_ID())
			ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
		ENDIF
			
		IF (PS_Moving_Chute_State = PS_MOVING_CHUTE_PROCESS_DATA) 
		OR (PS_Moving_Chute_State = PS_MOVING_CHUTE_SUCCESS) 
		OR (PS_Moving_Chute_State = PS_MOVING_CHUTE_FAIL) 
			IF NOT IS_ENTITY_DEAD(PS_FlatBedTruck)
				vPS_MovingTarget = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTruck, <<0, -5, 1>>)
				IF 	(PS_Moving_Chute_State != PS_MOVING_CHUTE_SUCCESS) 
				AND	(PS_Moving_Chute_State != PS_MOVING_CHUTE_FAIL) 
					PS_PARACHUTE_UPDATE_TARGET_WITH_ALPHA(vPS_MovingTarget, tmrLandingAlpha)
				ENDIF
				PS_Main.vCheckpointMarkerFlashPosition = vPS_MovingTarget
			ENDIF
		ELIF (PS_Moving_Chute_State >= PS_MOVING_CHUTE_JUMPING) //AND NOT (PS_Moving_Chute_State = PS_MOVING_CHUTE_FAIL) AND NOT (PS_Moving_Chute_State = PS_MOVING_CHUTE_SUCCESS)
			PS_PARACHUTE_UPDATE_TRUCK_TARGET(vPS_MovingTarget, bPS_MovingTargetBlip)
		ENDIF
		
		SWITCH(PS_Moving_Chute_State)
		//Spawn plane with player in the passenger seat
			CASE PS_MOVING_CHUTE_INIT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("on the new version!")
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						DISPLAY_RADAR(FALSE)
						DISPLAY_HUD(FALSE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Moving_Chute_State = PS_MOVING_CHUTE_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

		//Play countdown
			CASE PS_MOVING_CHUTE_COUNTDOWN
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_UPDATE_LOOK_CAM()
						IF	PS_HUD_UPDATE_COUNTDOWN(FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_UPDATE_LOOK_CAM()
						PS_Moving_Chute_State = PS_MOVING_CHUTE_LOOKING
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Give player control and the first instruction to jump
			CASE PS_MOVING_CHUTE_LOOKING
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_UPDATE_LOOK_CAM()
						PRINT_HELP("PSCHUTE_1")
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_POMT", "PS_POMT_1", "PS_POMT", "PS_POMT_2")
						//"The drop zone is on the back of a moving truck, so take your time maneuvering with your parachute."
						//"On the drop, keep a close eye on where the target is headed. "
						iPSDialogueCounter++
						START_TIMER_NOW(PS_Main.myJumpCam.camTimer)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 0.08
						AND IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
							IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
								SET_ENTITY_COORDS(PS_Main.myVehicle, vStartPosition)
								SET_ENTITY_HEADING(PS_Main.myVehicle, fStartHeading)
								iSceneId = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneId, PS_Main.myVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(PS_Main.myVehicle, "Chassis"))
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneId, "oddjobs@basejump@", "Heli_jump", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT)
								SET_SYNCHRONIZED_SCENE_PHASE(iSceneID, 0.6)
							ENDIF
							
							PS_MOVING_CHUTE_START_TARGET_TRUCK(bPS_MovingTargetBlip)
							
							CLEAR_HELP()
							PS_SETUP_JUMP_CAM()
							RESTART_TIMER_NOW(PS_Main.myJumpCam.camTimer)
							PS_Moving_Chute_State = PS_MOVING_CHUTE_JUMPING
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELSE
							PS_UPDATE_LOOK_CAM()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						// We should never get here
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_MOVING_CHUTE_JUMPING
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) > 0.92
					OPEN_SEQUENCE_TASK(seqJump)				
						TASK_FORCE_MOTION_STATE(NULL, ENUM_TO_INT(MS_PARACHUTING))
						TASK_PARACHUTE(NULL, TRUE)
					CLOSE_SEQUENCE_TASK(seqJump)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqJump)
					CLEAR_SEQUENCE_TASK(seqJump)
				ENDIF
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
				    	IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 0.0
							PS_Main.myJumpCam.vLookCameraRot.z = CLAMP(PS_Main.myJumpCam.vLookCameraRot.z, -8.9090, 8.9090)
							PS_Main.myJumpCam.vBaseLookCamRot.z += PS_Main.myJumpCam.vLookCameraRot.z
							
							vCurFocalOffset = ROTATE_VECTOR_ABOUT_Z(PS_Main.myJumpCam.vFocalOffset, PS_Main.myJumpCam.vLookCameraRot.z)
							
							SET_CAM_PARAMS(PS_Main.previewCam1, PS_Main.myJumpCam.vFocalPoint + vCurFocalOffset, PS_Main.myJumpCam.vBaseLookCamRot, PS_Main.myJumpCam.fBaseLookCamFOV, 300)
							SET_CAM_ACTIVE_WITH_INTERP(PS_Main.previewCam2, PS_Main.previewCam1, FLOOR(1000.0 * (1.1 - 0.0)), GRAPH_TYPE_ACCEL)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE		
						IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 0.0
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							RENDER_SCRIPT_CAMS(FALSE, TRUE, FLOOR(1000.0 * (1.1 - 0.0)), FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF					
						BREAK
					CASE PS_SUBSTATE_EXIT
						IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 1.1
							SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), PS_Main.myVehicle, FALSE)
							IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "oddjobs@basejump@", "Heli_jump")
								DISPLAY_RADAR(TRUE)
								DISPLAY_HUD(TRUE)
								PS_HUD_RESET_TIMER()
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//								SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
								PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
								//QUEUE_OBJHELPALOGUE_HELP("PSCHUTE_2", -2, 1.5)	//"Use ~PAD_LSTICK_ALL~ to direct your free fall~n~
																				//Press ~PAD_A~ to deploy parachute~n~"
								IF IS_SCRIPT_GLOBAL_SHAKING()
									STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
								ENDIF
								
								CANCEL_TIMER(PS_Main.myJumpCam.camTimer)
								PS_Moving_Chute_State = PS_MOVING_CHUTE_SKYDIVING
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_MOVING_CHUTE_SKYDIVING
				IF NOT IS_ENTITY_DEAD(PS_FlatBedTruck)
					vPS_MovingTarget = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTruck, <<0, -5, 1>>)
				ENDIF
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINT_HELP("PSCHUTE_2")
						PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_POMT", "PS_POMT_4", "PS_POMT", "PS_POMT_5")
						//"It's better to pull your chute early than late. "
						//"It will give you more time to track the target and try to match its speed."
						iPSDialogueCounter++
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF IS_ENTITY_IN_WATER(PLAYER_PED_ID()) OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_LANDING
						OR (GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID) //AND (NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())))
							CLEAR_PRINTS()
							CLEAR_HELP()
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
							REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
							START_TIMER_NOW(PS_Main.tmrEndDelay)
							PS_INCREMENT_SUBSTATE()
						
						ELSE
							IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID
							OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_LANDING
								CLEAR_HELP()
							
							ELIF NOT PS_Main.myChallengeData.bPrintedChuteHelp
								IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_DEPLOYING
								OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
									CLEAR_HELP()
									PRINT_HELP("PSCHUTE_3")
									PS_Main.myChallengeData.bPrintedChuteHelp = TRUE
								ENDIF
							
							ELIF NOT PS_Main.myChallengeData.bPrintedLandingHelp
								vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
								IF vPlayerCoords.z <= 100.0
									CLEAR_HELP()
									IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
										PRINT_HELP("PSCHUTE_4_KM", DEFAULT_HELP_TEXT_TIME)
									ELSE
										PRINT_HELP("PSCHUTE_4", DEFAULT_HELP_TEXT_TIME)
									ENDIF	
									PS_Main.myChallengeData.bPrintedLandingHelp = TRUE
								ENDIF
							ENDIF
							IF PS_IS_PLAYER_OUT_OF_RANGE() AND PS_IS_RANGE_TIMER_UP()
								CLEAR_PRINTS()
								CLEAR_HELP()
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
								REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
								START_TIMER_NOW(PS_Main.tmrEndDelay)
								PS_INCREMENT_SUBSTATE()
								PRINTLN("FAILED BC PLAYER IS OUT OF RANGE")
								ePSFailReason = PS_FAIL_OUT_OF_RANGE
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Main.myChallengeData.bPrintedChuteHelp = FALSE
						PS_Main.myChallengeData.bPrintedLandingHelp = FALSE
						PS_Moving_Chute_State = PS_MOVING_CHUTE_PROCESS_DATA
						iInstructionCounter = 1
						RESTART_TIMER_NOW(tmrLandingAlpha)
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_MOVING_CHUTE_PROCESS_DATA
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE	
						IF GET_TIMER_IN_SECONDS(PS_Main.tmrEndDelay) > 0.25
							CANCEL_TIMER(PS_Main.tmrEndDelay)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Moving_Chute_State = PS_MOVING_CHUTE_LANDING_CHECK()
						IF PS_Moving_Chute_State <> PS_MOVING_CHUTE_PROCESS_DATA
							CERRORLN(DEBUG_MISSION, "Moving to success/fail state...")
							iInstructionCounter = 1
							iPSDialogueCounter = 4 //should be 4
							IF PS_Moving_Chute_State = PS_MOVING_CHUTE_SUCCESS
								PS_INITIALISE_CHECKPOINT_MARKER_FLASH(vPS_MovingTarget)
								PS_PLAY_CHECKPOINT_CHIME(PS_CHECKPOINT_OK)
							ENDIF
							bLessonCompleted = TRUE
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_MOVING_CHUTE_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							PS_UPDATE_RECORDS(VECTOR_ZERO, TRUE)
							PS_RESET_CHECKPOINT_MGR(FALSE)
							PS_PLAY_LESSON_FINISHED_LINE("PS_POMT", "PS_POMT_6", "PS_POMT_7", "PS_POMT_8", "PS_POMT_9")
							iPSObjectiveTextCounter++
							iEndDialogueTimer = GET_GAME_TIMER()
							bLesonFinishedLineTriggered = TRUE
						ENDIF
						IF NOT PILOT_SCHOOL_DATA_HAS_FAIL_REASON()			//lesson is being passed
							IF GET_GAME_TIMER() - iEndDialogueTimer > 500
								PS_LESSON_END(TRUE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//lesson is being failed
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_MOVING_CHUTE_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF ( bLesonFinishedLineTriggered = FALSE )
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
							ELSE
								PS_UPDATE_RECORDS(VECTOR_ZERO, TRUE)
								IF ( bLessonCompleted = TRUE )
		 							PS_PLAY_LESSON_FINISHED_LINE("PS_POMT", "PS_POMT_6", "PS_POMT_7", "PS_POMT_8", "PS_POMT_9")
								ELSE
									PS_PLAY_LESSON_FINISHED_LINE("PS_POMT", "PS_POMT_6", "PS_POMT_7", "PS_POMT_8", "")
								ENDIF
								iPSObjectiveTextCounter++
								iEndDialogueTimer = GET_GAME_TIMER()
								bLesonFinishedLineTriggered = TRUE
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_GAME_TIMER() - iEndDialogueTimer > PS_END_DIALOGUE_TIME
								PS_LESSON_END(FALSE)
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							CERRORLN(DEBUG_MISSION, "Moving to PS_MOVING_CHUTE_FAIL -> PS_SUBSTATE_EXIT")
							PS_INCREMENT_SUBSTATE()
						ELSE
							CERRORLN(DEBUG_MISSION, "Waiting for scorecard to become active...")
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK		
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
	
PROC PS_Moving_Chute_MainCleanup()	
	PED_INDEX targetDriver
	
	CANCEL_MUSIC_EVENT("MGPS_START")
	CANCEL_MUSIC_EVENT("MGPS_STOP")
	CANCEL_MUSIC_EVENT("MGPS_FAIL")
	
	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT, ePS_CamViewMode)
	PS_MOVING_CHUTE_STOP_TARGET_TRUCK(vPS_MovingTarget)
//hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	REMOVE_BLIP(bPS_MovingTargetBlip)
	CLEAR_PRINTS()
	CLEAR_HELP()
	
	IF NOT IS_SPECIAL_ABILITY_ENABLED(PLAYER_ID())
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE()	
	
//player/objects		
	PS_PARACHUTE_CLEANUP_TRUCK_TARGET()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)			
	ENDIF
	RESTORE_PLAYER_PED_VARIATIONS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
	SET_PED_VARIATIONS(PLAYER_PED_ID(), structPlayerProps)
	PS_SEND_PLAYER_BACK_TO_MENU(/*FALSE*/)

//assets

	IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
		DELETE_VEHICLE(PS_Main.myVehicle)
	ENDIF
	
//	IF DOES_ENTITY_EXIST(PS_FlatBedTrailer)
//		DELETE_VEHICLE(PS_FlatBedTrailer)
//	ENDIF
	
	IF DOES_ENTITY_EXIST(PS_FlatBedTruck)
		targetDriver = GET_PED_IN_VEHICLE_SEAT(PS_FlatBedTruck, VS_DRIVER)
		IF DOES_ENTITY_EXIST(targetDriver)
			DELETE_PED(targetDriver)
		ENDIF
		
		DELETE_VEHICLE(PS_FlatBedTruck)
	ENDIF

	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	SET_MODEL_AS_NO_LONGER_NEEDED(PilotModel)
	REMOVE_WAYPOINT_RECORDING("ps_trucktarget_cw")

	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Moving_Chute.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************
