//|=======================================================================================|
//|                 Author:  Lukasz Bogaj	 				Date: 09/02/2012              |
//|=======================================================================================|
//|                                  		YOGA.sc	 	                             	  |
//|                            		SINGLEPLAYER YOGA MINIGAME	                       	  |
//|                             									                      |
//|=======================================================================================|


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

//Commands headers
USING "commands_ped.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "commands_camera.sch"
USING "commands_object.sch"
USING "commands_interiors.sch"
USING "commands_streaming.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_path.sch"
USING "commands_audio.sch"
USING "commands_entity.sch"

//Script headers
USING "script_player.sch"
USING "script_ped.sch"
USING "script_buttons.sch"
USING "script_maths.sch"
USING "script_blips.sch"
//USING "script_oddjob_funcs.sch"

//Public files
USING "flow_public_core_override.sch"
USING "cellphone_public.sch"
USING "selector_public.sch"
USING "player_ped_public.sch"
USING "locates_public.sch"
USING "building_control_public.sch"
USING "dialogue_public.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "cutscene_public.sch"
USING "clearmissionarea.sch"
USING "mission_stat_public.sch"
USING "yoga.sch"
USING "mp_scaleform_functions.sch"
USING "fake_cellphone_public.sch"

// Includes for Quit UI.
USING "minigame_uiinputs.sch"
USING "hud_drawing.sch"
USING "screens_header.sch"
USING "UIUtil.sch"
USING "script_usecontext.sch"

//Debug headers
#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

//===================================== ENUMS & STRUCTS ==================================|

/// PURPOSE: Specifies location of the minigame
ENUM MINIGAME_LOCATIONS
	MINIGAME_LOCATION_MICHAELS_HOUSE	= 0,
	MINIGAME_LOCATION_NORTH_COAST		= 1
ENDENUM

/// PURPOSE: Specifies stages of the minigame.
ENUM MINIGAME_STAGES
	MINIGAME_STAGE_SETUP 				= 0,		//Setup
	MINIGAME_STAGE_CUTSCENE_INTRO		= 1,		//Intro cutscene
	MINIGAME_STAGE_YOGA_WARRIOR			= 2,		//First yoga move - Warrior
	MINIGAME_STAGE_YOGA_TRIANGLE		= 3,		//Second yoga move - Triangle
	MINIGAME_STAGE_YOGA_SUNSALUTATION	= 4,		//Third yoga move - Sun Salutation
	MINIGAME_STAGE_PASSED_CUTSCENE_END	= 5,		//Passed end cutscene
	MINIGAME_STAGE_FAILED_CUTSCENE_END	= 6,		//Passed end cutscene
	MINIGAME_STAGE_PASSED,							//Minigame passed
	MINIGAME_STAGE_FAILED							//Minigame failed
ENDENUM

/// PURPOSE: Specifies minigame fail reasons.
ENUM MINIGAME_FAILS
	MINIGAME_FAIL_YOGA_NOT_FAILED		= 0,
	MINIGAME_FAIL_YOGA_FAILED			= 1,		//Player failed yoga minigame
	MINIGAME_FAIL_FORCE_FAIL			= 2,		//Debug forced fail
	
	MAX_MINIGAME_FAILS
ENDENUM

/// PURPOSE: Specifies outfits for the minigame.
ENUM MINIGAME_OUTFITS
	MINIGAME_OUTFIT_DEFAULT				= 0, 		//Default outfit in which player starts the minigame
	MINIGAME_OUTFIT_YOGA				= 1			//Outfit for yoga
ENDENUM

/// PURPOSE: Specifies details of minigame specific objects.
STRUCT OBJECT_STRUCT
	OBJECT_INDEX			ObjectIndex				//Object index
	MODEL_NAMES				ModelName				//Object model
	VECTOR					vPosition				//Object postion
	VECTOR					vRotation				//Object rotation
ENDSTRUCT

/// PURPOSE: Contains player postion and heading.
STRUCT LOCATION_STRUCT
	VECTOR 					vPosition				//Position
	FLOAT 					fHeading				//Heading
ENDSTRUCT

/// PURPOSE: Stores player position, heading and got coords for the end of the game.
STRUCT PLAYER_ENDGAME_STRUCT
	VECTOR 					vWarpPosition			// Player warp position
	FLOAT 					fWarpHeading			// Player warp heading
	VECTOR					vGotoPosition			// Player goto position
	FLOAT					fGotoHeading			// Player final heading
ENDSTRUCT

/// PURPOSE: Stores player vehicle safe position and heading
STRUCT PLAYER_VEHICLE_STRUCT
	VECTOR 					vPosition				//Position
	FLOAT 					fHeading				//Heading
	VECTOR					vAngledAreaPos1			// Vector for angled area check
	VECTOR					vAngledAreaPos2			// Vector for angled area check
	FLOAT					fAngledAreaWidth		// Width for angled area check.
ENDSTRUCT
	
	

//|======================================= CONSTANTS =====================================|

CONST_INT					END_CUTSCENE_SUCCESS	10
CONST_INT					END_CUTSCENE_FAILURE	20
CONST_INT					END_CUTSCENE_QUIT		30
CONST_INT					END_CUTSCENE_FINISH		40
			
//======================================== VARIABLES =====================================|

//VECTOR						vMinigameTriggerPosition

MINIGAME_STAGES				eMinigameStage
MINIGAME_FAILS				eMinigameFail
MINIGAME_LOCATIONS			eMinigameLocation

LOCATION_STRUCT				sMinigamePosition
YOGASTRUCT 					sYogaPlayer
OBJECT_STRUCT				sYogaMat
OBJECT_STRUCT				sDock
OBJECT_STRUCT				sCandle04
OBJECT_STRUCT				sCandle05
OBJECT_STRUCT				sCandle06
OBJECT_STRUCT				sCellphone
PLAYER_ENDGAME_STRUCT		sPlayerEndgame
PLAYER_VEHICLE_STRUCT		sPlayerVehicleSafe

FAKE_CELLPHONE_DATA			sFakeCellphoneData

INT 						iStageSetupProgress
INT							iMinigameStageProgress
BOOL						bLoadingFlag
BOOL						bSkipFlag
BOOL						bReplayFlag

//INT							iTimer
//INT							iCutsceneDelayTimer

BOOL 						bOutfitYoga
BOOL						bMusicEventPositionCompleteTriggered
BOOL						bHasQuit

FLOAT						fKharma

CAMERA_INDEX				ScriptedCamera, AnimatedCamera

INT							iMinigameIntroSceneID	= -1
INT							iMinigameOutroScene1ID	= -1
INT							iMinigameOutroScene2ID	= -1

// UI Quit vars & consts
BOOL bIsQuitMenuActive = FALSE

VECTOR						vIntroScenePosition, vIntroSceneRotation
VECTOR						vOutro1ScenePosition, vOutro1SceneRotation
VECTOR						vOutro2ScenePosition, vOutro2SceneRotation
VECTOR						vLoadSceneCamPosition, vLoadSceneCamDirection

SCALEFORM_INSTRUCTIONAL_BUTTONS instructionalButtons
SPRITE_PLACEMENT aSpriteYogaControls
SCALEFORM_INDEX scaleformYogaControls
BOOL bUpdateYogaControls = FALSE


//|========================================= ARRAYS ======================================|

//fail flags
BOOL						FailFlags[MAX_MINIGAME_FAILS]

//text printing
INT 						iTriggeredTextHashes[20]


//|================================ MISC PROCEDURES & FUNCTIONS ==========================|


/// PURPOSE: Display the Yoga quit button
///    
PROC DISPLAY_INSTRUCTIONAL_BUTTONS()

	IF HAS_SCALEFORM_MOVIE_LOADED(scaleformYogaControls)
	
		IF bUpdateYogaControls
			REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(instructionalButtons)					
			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CONTEXT), "H_YOGA_QUIT", instructionalButtons)
		ENDIF
		
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(scaleformYogaControls, aSpriteYogaControls, instructionalButtons, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(instructionalButtons))
	
	ELSE
		scaleformYogaControls = REQUEST_SCALEFORM_MOVIE("instructional_buttons")
		bUpdateYogaControls = TRUE
	ENDIF

ENDPROC

PROC RESET_MINIGAME_FLAGS()

	//reset setup progress to 1, so that iSetupProgress = 0 only runs once, on initial minigame load.
	iStageSetupProgress 			= 1
	
	iMinigameStageProgress 			= 0
	
	bMusicEventPositionCompleteTriggered = FALSE
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Minigame flags and progress counters are reset.")
	#ENDIF

ENDPROC


PROC YOGA_MANAGE_QUIT_UI()
	
	IF IS_SCREEN_FADED_IN() AND NOT IS_PAUSE_MENU_ACTIVE()
	
		IF bIsQuitMenuActive = FALSE
		
			IF eMinigameStage = MINIGAME_STAGE_CUTSCENE_INTRO
			OR eMinigameStage = MINIGAME_STAGE_YOGA_WARRIOR
			OR eMinigameStage = MINIGAME_STAGE_YOGA_TRIANGLE
			OR eMinigameStage = MINIGAME_STAGE_YOGA_SUNSALUTATION
			
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					
					SET_GAME_PAUSED(TRUE)
					
					bIsQuitMenuActive = TRUE

				ENDIF
				
			DISPLAY_INSTRUCTIONAL_BUTTONS()
				
			ENDIF
			
		ELSE
				
			SET_WARNING_MESSAGE_WITH_HEADER("YOGA_QUIT", "YOGA_QUIT_INST", (FE_WARNING_YES | FE_WARNING_NO))
			
			IF IS_MESSAGE_BEING_DISPLAYED()
				CLEAR_PRINTS()
			ENDIF
			
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
						
							
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				
				DO_SCREEN_FADE_OUT(0)
				
				bIsQuitMenuActive = FALSE
				SET_GAME_PAUSED(FALSE)
				
				CLEAR_PRINTS()
				CLEAR_HELP()
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								
				eMinigameStage = MINIGAME_STAGE_FAILED_CUTSCENE_END
				
				bHasQuit = TRUE
								
				RESET_MINIGAME_FLAGS()
		
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			
				bIsQuitMenuActive = FALSE
				SET_GAME_PAUSED(FALSE)
				
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC






/// PURPOSE:
///    Sets all fail flags to specified boolean value.
/// PARAMS:
///    bNewBool - Boolean value to set all flags to TRUE or FALSE.
PROC SET_FAIL_FLAGS(BOOL bNewBool)

	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(FailFlags) - 1 )

		FailFlags[i] = bNewBool

	ENDFOR

ENDPROC

/// PURPOSE:
///    Gets next minigame stage for specified minigame stage
/// PARAMS:
///    eStage - Minigame stage to get next minigame stage for
/// RETURNS:
///    Next stage for the specified minigame stage.
FUNC MINIGAME_STAGES GET_NEXT_MINIGAME_STAGE(MINIGAME_STAGES eStage)

	IF eStage = MINIGAME_STAGE_PASSED_CUTSCENE_END
		RETURN MINIGAME_STAGE_PASSED_CUTSCENE_END
	ENDIF
	
	IF eStage = MINIGAME_STAGE_FAILED_CUTSCENE_END
		RETURN MINIGAME_STAGE_FAILED_CUTSCENE_END
	ENDIF
	
	MINIGAME_STAGES eNextStage
	
	INT iNextStage = ENUM_TO_INT(eStage) + 1
	
	eNextStage = INT_TO_ENUM(MINIGAME_STAGES, iNextStage)
	
	RETURN eNextStage

ENDFUNC

/// PURPOSE:
///    Performs a screen fade in with a wait for a specified time.
/// PARAMS:
///    iTime - Duration of the fade in miliseconds.
PROC DO_SAFE_SCREEN_FADE_IN(INT iTime)

	IF NOT IS_SCREEN_FADED_IN()
	
		DO_SCREEN_FADE_IN(iTime)
		
		WHILE NOT IS_SCREEN_FADED_IN()
			WAIT(0)
		ENDWHILE
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Performs a screen fade out with a wait for a specified time.
/// PARAMS:
///    iTime - Duration of the fade in miliseconds.
PROC DO_SAFE_SCREEN_FADE_OUT(INT iTime)

	IF NOT IS_SCREEN_FADED_OUT()
	
		DO_SCREEN_FADE_OUT(iTime)
		
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
		
	ENDIF

ENDPROC

FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING strLabel)
	INT iHash = GET_HASH_KEY(strLabel)
	INT iNumHashes = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT iNumHashes i
		IF iTriggeredTextHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_LABEL_AS_TRIGGERED(STRING strLabel, BOOL bIsTriggered)
	INT iHash = GET_HASH_KEY(strLabel)
	INT iNumHashes = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	BOOL bQuitLoop = FALSE
	
	WHILE i < iNumHashes AND NOT bQuitLoop
		IF bIsTriggered
			IF iTriggeredTextHashes[i] = 0
				iTriggeredTextHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iTriggeredTextHashes[i] = iHash
				iTriggeredTextHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT iNumHashes = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT iNumHashes i
		iTriggeredTextHashes[i] = 0
	ENDREPEAT
ENDPROC

PROC PRINT_GOD_TEXT_ADVANCED(STRING sLabel, INT iDuration = DEFAULT_GOD_TEXT_TIME, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)
		PRINT(sLabel, iDuration, 1)
		SET_LABEL_AS_TRIGGERED(sLabel, bOnce)
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Printing god text advanced ", sLabel, ".")
		#ENDIF
	ENDIF
ENDPROC

PROC SET_PLAYER_PED_OUTFIT(MINIGAME_OUTFITS eOutfit)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())

		SWITCH eOutfit
		
			CASE MINIGAME_OUTFIT_DEFAULT
			
				RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Restoring default player ped variation.")
				#ENDIF
				
			BREAK
			
			CASE MINIGAME_OUTFIT_YOGA
			
				SET_PLAYER_PED_VARIATIONS_FOR_YOGA(PLAYER_PED_ID())
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting player ped variation to yoga outfit.")
				#ENDIF
				
			BREAK
			
		ENDSWITCH
	
	ENDIF

ENDPROC

/// PURPOSE:
///    Warps specified ped to new coordinates and sets specified ped's new heading.
/// PARAMS:
///    ped - PED_INDEX to warp.
///    vNewPosition - VECTOR coordinates specifying new ped's position.
///    fNewHeading - FLOAT specifying new ped's heading.
///    bKeepVehicle - Specifies if ped should keep vehicle they are currently in.
///    bResetGameplayCamera - Specifies if gameplay camera should be positioned behind player's back. Works only for PLAYER_PED_ID().
///    bLoadScene - Specifies if scene at new provided coordinates should be loaded. Works only for PLAYER_PED_ID().
PROC WARP_PED(PED_INDEX ped, VECTOR vNewPosition, FLOAT fNewHeading, BOOL bKeepVehicle, BOOL bResetGameplayCamera, BOOL bLoadScene)

	IF NOT IS_PED_INJURED(ped)
	
		IF ( bKeepVehicle = TRUE )
			SET_PED_COORDS_KEEP_VEHICLE(ped, vNewPosition)
		ELIF ( bKeepVehicle = FALSE ) 
			SET_ENTITY_COORDS(ped, vNewPosition)
		ENDIF
		
		SET_ENTITY_HEADING(ped, fNewHeading)
		
		IF ( ped = PLAYER_PED_ID())
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping player ped to coordinates ", vNewPosition, " and heading ", fNewHeading, ".")
			#ENDIF
		
			IF ( bResetGameplayCamera = TRUE)
			
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)	
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				
			ENDIF
			
			IF ( bLoadScene = TRUE )
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling LOAD_SCENE() at coordinates ", vNewPosition, ". This might take a while to load.")
				#ENDIF
			
				LOAD_SCENE(vNewPosition)
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Creates an object and returns TRUE if such object was created successfully.
/// PARAMS:
///    osObject - OBJECT_STRUCT containing model, position and rotation of the object.
///    bFreezeObject - Specify if this object's position should be frozen.
/// RETURNS:
///    TRUE if object was created, FALSE if otherwise.
FUNC BOOL HAS_MINIGAME_OBJECT_BEEN_CREATED(OBJECT_STRUCT &osObject, BOOL bFreezeObject = FALSE)

	IF NOT DOES_ENTITY_EXIST(osObject.ObjectIndex)
		
		REQUEST_MODEL(osObject.ModelName)
		
		IF HAS_MODEL_LOADED(osObject.ModelName)
		
			osObject.ObjectIndex = CREATE_OBJECT(osObject.ModelName, osObject.vPosition)
			SET_ENTITY_COORDS_NO_OFFSET(osObject.ObjectIndex, osObject.vPosition)
			SET_ENTITY_ROTATION(osObject.ObjectIndex, osObject.vRotation)
			FREEZE_ENTITY_POSITION(osObject.ObjectIndex, bFreezeObject)

			SET_ENTITY_INVINCIBLE(osObject.ObjectIndex, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(osObject.ModelName)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object with model ", GET_MODEL_NAME_FOR_DEBUG(osObject.ModelName), " at coordinates ", osObject.vPosition, ".")
			#ENDIF
		
		ENDIF
	
	ENDIF
	
	IF DOES_ENTITY_EXIST(osObject.ObjectIndex)

		RETURN TRUE
	
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks for various minigame fail conditions. Redirects minigame flow to MINIGAME_STAGE_FAILED when one of fail conditions is true.
/// PARAMS:
///    eStage - Current minigame stage.
///    eFail - Minigame fail reason.
PROC RUN_FAIL_CHECKS(MINIGAME_STAGES &eStage, MINIGAME_FAILS &eFail)

	IF eStage <> MINIGAME_STAGE_PASSED
		IF eStage <> MINIGAME_STAGE_FAILED
		
			IF eStage = MINIGAME_STAGE_YOGA_WARRIOR
			OR eStage = MINIGAME_STAGE_YOGA_TRIANGLE
			OR eStage = MINIGAME_STAGE_YOGA_SUNSALUTATION

				IF ( FailFlags[MINIGAME_FAIL_YOGA_FAILED] = TRUE )
				
					IF ( sYogaPlayer.iFailCounter = 3 )
				
						IF sYogaPlayer.eYogaMinigameStage = YOGAMINIGAMESTAGE_FAILRESET

							IF IS_PED_AT_LAST_STAGE_OF_SEQUENCE(sYogaPlayer.PedIndex, 2)
							
								RESET_MINIGAME_FLAGS()
							
								eFail = MINIGAME_FAIL_YOGA_FAILED
								eStage = MINIGAME_STAGE_FAILED_CUTSCENE_END
							
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF

			ENDIF

		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Reduces Kharma based on the number of fails. Kharma value is used to replenish the special ability meter.
PROC CHECK_DECREASE_KHARMA()

	IF sYogaPlayer.iFailCounter > 0
		
		fKharma = fKharma - (0.125 * TO_FLOAT(sYogaPlayer.iFailCounter))
		
		IF fKharma < 0.0
			fKharma = 0.0
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Yoga Kharma decreased to ", fKharma)
		#ENDIF
		
	ENDIF

ENDPROC

//|============================== END MISC PROCEDURES & FUNCTIONS ========================|

//|============================== CLEANUP PROCEDURES & FUNCTIONS =========================|

PROC CLEANUP_OBJECT(OBJECT_INDEX &ObjectIndex, BOOL bDelete = FALSE)

	IF DOES_ENTITY_EXIST(ObjectIndex)
	
		IF IS_ENTITY_ATTACHED(ObjectIndex)
			DETACH_ENTITY(ObjectIndex)
		ENDIF

		IF ( bDelete = TRUE )
			DELETE_OBJECT(ObjectIndex)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(ObjectIndex)
		ENDIF
		
	ENDIF
	
ENDPROC

PROC RUN_MINIGAME_STAGE_CLEANUP()
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting minigame stage cleanup.")
	#ENDIF
	
	IF DOES_CAM_EXIST(ScriptedCamera)
		SET_CAM_ACTIVE(ScriptedCamera, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(ScriptedCamera)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF
	
	RESET_YOGA_STRUCT(sYogaPlayer, TRUE, TRUE)
	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()

	//turn off the audio scenes
	IF IS_AUDIO_SCENE_ACTIVE("YOGA_MINIGAME_TRANQUIL")
		STOP_AUDIO_SCENE("YOGA_MINIGAME_TRANQUIL")
	ENDIF
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	CLEAR_TRIGGERED_LABELS()
	
	CLEANUP_OBJECT(sDock.ObjectIndex, TRUE)
	CLEANUP_OBJECT(sYogaMat.ObjectIndex, TRUE)
	CLEANUP_OBJECT(sCandle04.ObjectIndex, TRUE)
	CLEANUP_OBJECT(sCandle05.ObjectIndex, TRUE)
	CLEANUP_OBJECT(sCandle06.ObjectIndex, TRUE)
	CLEANUP_OBJECT(sCellphone.ObjectIndex, TRUE)
	
	IF TRIGGER_MUSIC_EVENT("MGYG_END")
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggered music event MGYG_END.")
		#ENDIF
	ENDIF
	
	CLEAR_TIMECYCLE_MODIFIER()
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished minigame stage cleanup.")
	#ENDIF
	
ENDPROC

PROC MINIGAME_CLEANUP()
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting minigame cleanup.")
	#ENDIF
	
	IF DOES_CAM_EXIST(ScriptedCamera)
		SET_CAM_ACTIVE(ScriptedCamera, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(ScriptedCamera)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
		CLEAR_PED_DAMAGE_DECAL_BY_ZONE(PLAYER_PED_ID(), PDZ_HEAD, "blushing")	//clear blushing decal
	ENDIF
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	CLEANUP_OBJECT(sDock.ObjectIndex, TRUE)
	CLEANUP_OBJECT(sYogaMat.ObjectIndex, TRUE)
	CLEANUP_OBJECT(sCandle04.ObjectIndex, TRUE)
	CLEANUP_OBJECT(sCandle05.ObjectIndex, TRUE)
	CLEANUP_OBJECT(sCandle06.ObjectIndex, TRUE)
	CLEANUP_OBJECT(sCellphone.ObjectIndex, TRUE)
	
	RELEASE_FAKE_CELLPHONE_MOVIE(sFakeCellphoneData)
	//RELEASE_NPC_PHONE_RENDERTARGET()

	KillAllConversations()
	
	SET_MAX_WANTED_LEVEL(5) //set max wanted level back up
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	//enable the phone 
	DISABLE_CELLPHONE(FALSE)
	ENABLE_SELECTOR()
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_MOUNTAIN_LION_WANDER",TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(A_C_MTLION,FALSE)
	
	IF IS_AUDIO_SCENE_ACTIVE("YOGA_MINIGAME_TRANQUIL")
		STOP_AUDIO_SCENE("YOGA_MINIGAME_TRANQUIL")
	ENDIF
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	IF ( bOutfitYoga = TRUE )
		SET_PLAYER_PED_OUTFIT(MINIGAME_OUTFIT_DEFAULT)
		bOutfitYoga = FALSE
	ENDIF
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scaleformYogaControls)
	
	IF TRIGGER_MUSIC_EVENT("MGYG_END")
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggered music event MGYG_END.")
		#ENDIF
	ENDIF
	
	PAUSE_CLOCK(FALSE)
	CLEAR_TIMECYCLE_MODIFIER()
	
	SHUTDOWN_PC_SCRIPTED_CONTROLS()
		
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished minigame cleanup.")
	#ENDIF
		
ENDPROC

//|============================ END CLEANUP PROCEDURES & FUNCTIONS =======================|

//|========================== DEBUG VARIABLES, PROCEDURES & FUNCTIONS ====================|

#IF IS_DEBUG_BUILD

	//mission skip menu variables
	MissionStageMenuTextStruct	SkipMenu[6]
	INT 			iReturnStage
	BOOL			bStageResetFlag
	
	//debug widgets variables
	WIDGET_GROUP_ID YogaWidgets
	BOOL 			bPositionMat

	/// PURPOSE:
	///    Returns string name of the specified minigame stage.
	/// PARAMS:
	///    eStage - Minigame stage to get string name for.
	/// RETURNS:
	///    String name of the specified minigame stage.
	FUNC STRING GET_MINIGAME_STAGE_NAME_FOR_DEBUG(MINIGAME_STAGES eStage)
	
		SWITCH eStage
		
			CASE MINIGAME_STAGE_SETUP
				RETURN "MINIGAME_STAGE_SETUP"
			BREAK
			CASE MINIGAME_STAGE_CUTSCENE_INTRO
				RETURN "MINIGAME_STAGE_CUTSCENE_INTRO"
			BREAK
			CASE MINIGAME_STAGE_YOGA_WARRIOR
				RETURN "MINIGAME_STAGE_YOGA_WARRIOR"
			BREAK
			CASE MINIGAME_STAGE_YOGA_TRIANGLE
				RETURN "MINIGAME_STAGE_YOGA_TRIANGLE"
			BREAK
			CASE MINIGAME_STAGE_YOGA_SUNSALUTATION
				RETURN "MINIGAME_STAGE_YOGA_SUNSALUTATION"
			BREAK
			CASE MINIGAME_STAGE_PASSED_CUTSCENE_END
				RETURN "MINIGAME_STAGE_PASSED_CUTSCENE_END"
			BREAK
			CASE MINIGAME_STAGE_FAILED_CUTSCENE_END
				RETURN "MINIGAME_STAGE_FAILED_CUTSCENE_END"
			BREAK
			
		ENDSWITCH
		
		RETURN "INCORRECT_MINIGAME_STAGE"
	
	ENDFUNC

	PROC CREATE_DEBUG_WIDGETS()
	
		YogaWidgets = START_WIDGET_GROUP("Mission: Yoga Minigame")

			ADD_WIDGET_BOOL("Can fail yoga?", FailFlags[MINIGAME_FAIL_YOGA_FAILED])

			START_WIDGET_GROUP("Yoga mat position")

				ADD_WIDGET_BOOL("Enable positioning", bPositionMat)
				ADD_WIDGET_STRING("Yoga Mat 01")
				ADD_WIDGET_VECTOR_SLIDER("Position: ", sYogaMat.vPosition, -7000.0, 7000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation: ", sYogaMat.vRotation, -7000.0, 7000.0, 0.01)
				
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	
	ENDPROC
	
	PROC DELETE_DEBUG_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(YogaWidgets)
			DELETE_WIDGET_GROUP(YogaWidgets)
		ENDIF
	ENDPROC
	
	PROC CREATE_DEBUG_MINIGAME_STAGE_MENU()
	
		SkipMenu[0].sTxtLabel		= "SETUP"
		SkipMenu[0].bSelectable		= FALSE
		SkipMenu[1].sTxtLabel		= "CUTSCENE - INTRO"
		SkipMenu[2].sTxtLabel	 	= "YOGA WARRIOR"
		SkipMenu[3].sTxtLabel	 	= "YOGA TRIANGLE"
		SkipMenu[4].sTxtLabel	 	= "YOGA SUN SALUTATION"
		SkipMenu[5].sTxtLabel		= "CUTSCENE - END"
	
	ENDPROC

	PROC RUN_DEBUG_PASS_AND_FAIL_CHECK(MINIGAME_STAGES &eStage, MINIGAME_FAILS &eFailReason)
	
		IF IS_KEYBOARD_KEY_PRESSED (KEY_S)
			eStage = MINIGAME_STAGE_PASSED
	    ENDIF
		
	    IF IS_KEYBOARD_KEY_PRESSED (KEY_F)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			eStage = MINIGAME_STAGE_FAILED
			eFailReason = MINIGAME_FAIL_FORCE_FAIL
	    ENDIF
	
	ENDPROC
	
	PROC RUN_DEBUG_MINIGAME_STAGE_MENU(INT &iSelectedStage, MINIGAME_STAGES &eStage, BOOL &bSkipActive, BOOL &bResetStage)
	
		IF LAUNCH_MISSION_STAGE_MENU(SkipMenu, iSelectedStage, ENUM_TO_INT(eStage), FALSE, "YOGA MINIGAME")
		
			DO_SAFE_SCREEN_FADE_OUT(500)

			CLEAR_PRINTS()
			
			CLEAR_HELP()
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			
			RUN_MINIGAME_STAGE_CLEANUP()
			
			eStage = INT_TO_ENUM(MINIGAME_STAGES, iSelectedStage)
			
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current minigame stage changed to ", GET_MINIGAME_STAGE_NAME_FOR_DEBUG(eStage), " via mission stage menu.")
			
			//reset the debug stage reset flag
			bResetStage	= FALSE
			
			bSkipActive = TRUE
			
			RESET_MINIGAME_FLAGS()
		
		ENDIF
	
	ENDPROC
	
	PROC RUN_DEBUG_SKIPS(MINIGAME_STAGES &eStage, BOOL &bSkipActive, BOOL &bResetStage)
	
		INT iCurrentStage = ENUM_TO_INT(eStage)
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		
			IF 	eStage <> MINIGAME_STAGE_PASSED_CUTSCENE_END
			AND eStage <> MINIGAME_STAGE_FAILED_CUTSCENE_END
		
				DO_SAFE_SCREEN_FADE_OUT(500)
				
				CLEAR_PRINTS()
			
				CLEAR_HELP()
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				RUN_MINIGAME_STAGE_CLEANUP()
				
				iCurrentStage++
				
				eStage = INT_TO_ENUM(MINIGAME_STAGES, iCurrentStage)
								
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current minigame stage changed to ", GET_MINIGAME_STAGE_NAME_FOR_DEBUG(eStage), " via J skip.")
				
				//reset the debug stage reset flag
				bResetStage	= FALSE
				
				bSkipActive = TRUE
				
				RESET_MINIGAME_FLAGS()

			ENDIF
			
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		
			IF eStage <> MINIGAME_STAGE_CUTSCENE_INTRO
		
				DO_SAFE_SCREEN_FADE_OUT(500)
				
				CLEAR_PRINTS()
			
				CLEAR_HELP()
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				RUN_MINIGAME_STAGE_CLEANUP()
				
				IF ( bResetStage = FALSE )
				
					iCurrentStage--
					
					eStage = INT_TO_ENUM(MINIGAME_STAGES, iCurrentStage)			
					
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current minigame stage changed to ", GET_MINIGAME_STAGE_NAME_FOR_DEBUG(eStage), " via P skip.")
				
				ELIF ( bResetStage = TRUE )
				
					eStage = INT_TO_ENUM(MINIGAME_STAGES, iCurrentStage)			
					
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current minigame stage ", GET_MINIGAME_STAGE_NAME_FOR_DEBUG(eStage), " reset via P skip.")
				
				ENDIF
				
				//reset the debug stage reset flag
				bResetStage	= FALSE
				
				bSkipActive = TRUE
				
				RESET_MINIGAME_FLAGS()
				
			ENDIF
			
		ENDIF
	
	ENDPROC
	
	PROC RUN_DEBUG_WIDGETS()
	
		IF ( bPositionMat = TRUE ) 
			
			IF DOES_ENTITY_EXIST(sYogaMat.ObjectIndex)
				IF NOT IS_ENTITY_DEAD(sYogaMat.ObjectIndex)
					SET_ENTITY_COORDS(sYogaMat.ObjectIndex, sYogaMat.vPosition)
					SET_ENTITY_ROTATION(sYogaMat.ObjectIndex, sYogaMat.vRotation)
				ENDIF
			ENDIF
			
		ENDIF

	ENDPROC
	
#ENDIF

//|===================== END DEBUG VARIABLES, PROCEDURES & FUNCTIONS =====================|

//|=============================== MINIGAME STAGES FUNCTIONS =============================|


/// PURPOSE: Checks the player position and sets the player's location
///    
PROC CHECK_MINIGAME_LOCATION(MINIGAME_STAGES eStage)

	VECTOR vPlayerPos
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()))
	ENDIF
	
	IF ARE_VECTORS_ALMOST_EQUAL(vPlayerPos, <<-790.906, 186.293, 71.835>>, 15.0)
		
		eMinigameLocation = MINIGAME_LOCATION_MICHAELS_HOUSE
		
		//vMinigameTriggerPosition 					= << -790.906, 186.293, 71.8351 >>
		sPlayerVehicleSafe.vPosition				= <<-786.077393,193.253357,55.168709>>
		sPlayerVehicleSafe.fHeading					= 88.5632
		
		sPlayerVehicleSafe.vAngledAreaPos1 			=  <<-783.845642,173.658768,75.979942>>
		sPlayerVehicleSafe.vAngledAreaPos2 			=  << -793.906, 189.293, 71.8351 >>
		sPlayerVehicleSafe.fAngledAreaWidth 		= 29.500000
		
		sPlayerEndgame.vWarpPosition				= << -791.5933, 188.4993, 71.8352 >>
		sPlayerEndgame.fWarpHeading					= 223.3349
		
		sPlayerEndgame.vGotoPosition				=  << -788.8267, 185.6219, 71.8352 >>
		sPlayerEndgame.fGotoHeading					=  223.7161
		
		sMinigamePosition.vPosition 				= << -790.906, 186.293, 71.8351 >>
		sMinigamePosition.fHeading  				= 275.074219
		
		vIntroScenePosition							= << -790.271, 185.760, 71.842 >>
		vIntroSceneRotation							= << -0.000, 0.000, -65.000 >>
		
		vOutro1ScenePosition						= << -790.271, 185.760, 71.842 >>
		vOutro1SceneRotation						= << -0.000, 0.000, -65.000 >>
		
		vOutro2ScenePosition						= << -793.750, 172.200, 71.555 >>
		vOutro2SceneRotation						= << 0.000, 0.000, -162.240 >>
		
		SWITCH eStage
		
			CASE MINIGAME_STAGE_SETUP
			CASE MINIGAME_STAGE_YOGA_WARRIOR
			CASE MINIGAME_STAGE_YOGA_TRIANGLE
			
				sYogaMat.vPosition					= <<-791.0036, 186.3552, 71.8295>> 
				sYogaMat.vRotation					= << 0.0, 0.0, -87.1403>>
				sYogaMat.ModelName					= PROP_YOGA_MAT_03
				
			BREAK
			
			CASE MINIGAME_STAGE_YOGA_SUNSALUTATION
			CASE MINIGAME_STAGE_PASSED_CUTSCENE_END
			
				sYogaMat.vPosition					= << -791.480, 186.325, 71.835 >>
				sYogaMat.vRotation					= << 0.0, 0.0, 0.0 >> 
				sYogaMat.ModelName					= PROP_YOGA_MAT_03
			
			BREAK
		
		ENDSWITCH
	
		sCandle04.vPosition							= <<-789.4000, 187.8659, 71.8349>>
		sCandle04.vRotation							= <<0.0000, 0.0000, 105.4800>>
		sCandle04.ModelName							= PROP_MEM_CANDLE_04
		
		sCandle05.vPosition							= <<-789.4700, 187.5759, 71.8349>>
		sCandle05.vRotation							= <<-0.0000, 0.0000, -37.4400>>
		sCandle05.ModelName							= PROP_MEM_CANDLE_05
		
		sCandle06.vPosition							= <<-789.7500, 187.7759, 71.8349>>
		sCandle06.vRotation							= <<0.0000, 0.0000, 52.2000>>
		sCandle06.ModelName							= PROP_MEM_CANDLE_06
		
		sDock.vPosition								= <<-790.6470, 188.6160, 71.9080>>
		sDock.vRotation								= <<0.0000, 0.0000, -17.0000>>
		sDock.ModelName								= PROP_MP3_DOCK
		
		sCellphone.vPosition						= <<-790.6667, 188.5546, 71.9111>>
		sCellphone.vRotation						= <<0.3273, -0.4297, -18.2587>>
		sCellphone.ModelName						= PROP_PHONE_ING
		
		vLoadSceneCamPosition						= << -793.52, 174.39, 73.20 >>
		vLoadSceneCamDirection						= << 0.26, -0.97, 0.03 >> 
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting up Yoga at Michael's house.")
		#ENDIF
		
	ELIF ARE_VECTORS_ALMOST_EQUAL(vPlayerPos, <<2862.15, 5945.49, 357.11>>, 15.0)
		
		eMinigameLocation = MINIGAME_LOCATION_NORTH_COAST
		
		//vMinigameTriggerPosition 					= <<2861.4500, 5945.9048, 357.0988>>
		sPlayerVehicleSafe.vPosition				= <<2840.0188, 5955.4077, 353.0921>> 
		sPlayerVehicleSafe.fHeading					= 225.4881
		
		sPlayerVehicleSafe.vAngledAreaPos1 			=  <<2893.812500,5906.262695,368.200012>>//<<2882.842041,5959.318848,361.058990>>
		sPlayerVehicleSafe.vAngledAreaPos2 			=  <<2833.079102,5967.083984,346.331116>>//<<2839.791992,5952.963867,336.083954>>
		sPlayerVehicleSafe.fAngledAreaWidth 		= 63.500000//35.500000
		
		sPlayerEndgame.vWarpPosition				= <<2861.3835, 5945.8013, 357.0977>>
		sPlayerEndgame.fWarpHeading					= 75.9740
		
		sPlayerEndgame.vGotoPosition				=  <<2857.6204, 5946.8945, 356.6815>>
		sPlayerEndgame.fGotoHeading					=  74.0123
		
		sMinigamePosition.vPosition 				= <<2861.4485, 5945.8584, 357.0606>>	//<<2861.4500, 5945.9048, 357.0988>>
		sMinigamePosition.fHeading  				= 255.0873								//74.0086
		
		vIntroScenePosition							= << 2862.325, 5944.692, 357.070 >>
		vIntroSceneRotation							= << -0.000, 0.000, -85.000 >>
		
		vOutro1ScenePosition						= << 2862.325, 5944.692, 357.070 >>
		vOutro1SceneRotation						= << -0.000, 0.000, -85.000 >>
		
		vOutro2ScenePosition						= << 2859.625, 5946.542, 357.050 >>
		vOutro2SceneRotation						= << -0.000, 0.000, 82.800 >>
				
		SWITCH eStage
		
			CASE MINIGAME_STAGE_SETUP
			CASE MINIGAME_STAGE_YOGA_WARRIOR
			CASE MINIGAME_STAGE_YOGA_TRIANGLE
			
				sYogaMat.vPosition					= << 2861.47, 5945.90, 357.06 >> 
				sYogaMat.vRotation					= << 0.0, -0.5, 70.0 >>
				sYogaMat.ModelName					= PROP_YOGA_MAT_03
				
			BREAK
			
			CASE MINIGAME_STAGE_YOGA_SUNSALUTATION
			CASE MINIGAME_STAGE_PASSED_CUTSCENE_END
			
				sYogaMat.vPosition					= << 2860.900, 5946.040, 357.060 >>
				sYogaMat.vRotation					= << 0.0, -0.5, -20.00 >> 
				sYogaMat.ModelName					= PROP_YOGA_MAT_03
			
			BREAK
		
		ENDSWITCH
		
		sCandle04.vPosition							= <<2864.1760, 5943.6089, 357.0612>>
		sCandle04.vRotation							= <<-0.0000, 0.0000, 0.0000>>
		sCandle04.ModelName							= PROP_MEM_CANDLE_04
		
		sCandle05.vPosition							= <<2863.9309, 5943.6650, 357.0606>>
		sCandle05.vRotation							= <<-0.0000, 0.0000, 142.9200>>
		sCandle05.ModelName							= PROP_MEM_CANDLE_05
		
		sCandle06.vPosition							= <<2864.0840, 5943.8691, 357.0550>>
		sCandle06.vRotation							= <<-0.0000, 0.0000, -95.7600>>
		sCandle06.ModelName							= PROP_MEM_CANDLE_06
		
		sDock.vPosition								= <<2862.946, 5947.504, 357.1310>>
		sDock.vRotation								= <<-0.0000, 0.0000, -38.5000>>
		sDock.ModelName								= PROP_MP3_DOCK
		
		sCellphone.vPosition						= <<2862.9082, 5947.4536, 357.1394>>
		sCellphone.vRotation						= <<0.2939, -0.4187, -38.2439>>
		sCellphone.ModelName						= PROP_PHONE_ING
		
		vLoadSceneCamPosition						= << 2861.51, 5945.42, 358.69 >>
		vLoadSceneCamDirection						= << -0.98, 0.17, 0.03 >> 
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting up Yoga at North Coast.")
		#ENDIF
	ELSE
		SCRIPT_ASSERT("YOGA: Invalid location!")
	ENDIF

ENDPROC

FUNC BOOL IS_MINIGAME_STAGE_LOADED(MINIGAME_STAGES eStage, INT &iSetupProgress, BOOL &bStageLoaded, BOOL &bStageSkippedTo, BOOL &bStageReplayed)

	//handle initial minigame setup
	//only run this IF block once, when the minigame first loads
	IF ( iSetupProgress = 0 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting initial minigame loading.")
		#ENDIF
		
		//clear the screen of any text displayed before minigame started
		CLEAR_PRINTS()
		CLEAR_HELP()
				
		REGISTER_SCRIPT_WITH_AUDIO(TRUE)

		IF IS_PLAYER_PLAYING(PLAYER_ID())
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),0)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
		ENDIF
		
		PAUSE_CLOCK(TRUE)
		CLEAR_TIMECYCLE_MODIFIER()
		
		CHECK_MINIGAME_LOCATION(eStage)
		RESET_YOGA_STRUCT(sYogaPlayer, TRUE)
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished initial minigame loading.")
		#ENDIF
		
		iSetupProgress++
	
	ENDIF
	
	//run this each time new minigame stage needs to be loaded
	IF ( iSetupProgress = 1 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Started minigame stage loading for ", GET_MINIGAME_STAGE_NAME_FOR_DEBUG(eStage), ".")
		#ENDIF
		
		bStageLoaded = FALSE
		
		CHECK_MINIGAME_LOCATION(eStage)
		
		//clear triggered text labels
		CLEAR_TRIGGERED_LABELS()
		
		iSetupProgress++
	
	ENDIF
		
	//handle player positioning when current stage is being skipped to or played normally
	//fade in the screen if it was faded out due to skipping
	IF ( iSetupProgress = 2 )
	
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE )
		OR ( bStageReplayed	= TRUE )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping player ped to new position for minigame stage being loaded due to skipping or replay.")
			#ENDIF

			WARP_PED(PLAYER_PED_ID(), sMinigamePosition.vPosition, sMinigamePosition.fHeading, FALSE, TRUE, FALSE)
			
		ENDIF
		
		iSetupProgress++
	
	ENDIF
	
	
	//handle creating vehicles/peds and setting up fail flags
	//run this each time new minigame stage needs to be loaded
	//THIS SETUP SECTION ASSUMES THAT ALL REQUIRED ASSETS HAVE BEEN ALREADY LOADED TO MEMORY IN PREVIOUS SETUP SECTIONS
	IF ( iSetupProgress = 3 )
	
		SET_FAIL_FLAGS(FALSE)
	
		SWITCH eStage
				
			CASE MINIGAME_STAGE_YOGA_WARRIOR
			CASE MINIGAME_STAGE_YOGA_TRIANGLE
			CASE MINIGAME_STAGE_YOGA_SUNSALUTATION
			CASE MINIGAME_STAGE_PASSED_CUTSCENE_END
			CASE MINIGAME_STAGE_FAILED_CUTSCENE_END
			
				IF NOT IS_AUDIO_SCENE_ACTIVE("YOGA_MINIGAME_TRANQUIL")
				
					START_AUDIO_SCENE("YOGA_MINIGAME_TRANQUIL")
					
					IF TRIGGER_MUSIC_EVENT("MGYG_START")
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggered music event MGYG_START.")
						#ENDIF
					ENDIF
					
				ENDIF
			
				IF ( bOutfitYoga = FALSE )
					SET_PLAYER_PED_OUTFIT(MINIGAME_OUTFIT_YOGA)
					bOutfitYoga = TRUE
				ENDIF
			
				IF 	HAS_MINIGAME_OBJECT_BEEN_CREATED(sYogaMat)
				AND HAS_MINIGAME_OBJECT_BEEN_CREATED(sCandle04, TRUE)
				AND HAS_MINIGAME_OBJECT_BEEN_CREATED(sCandle05, TRUE)
				AND HAS_MINIGAME_OBJECT_BEEN_CREATED(sCandle06, TRUE)
				
					SET_ENTITY_COORDS_NO_OFFSET(sYogaMat.ObjectIndex, sYogaMat.vPosition)
					SET_ENTITY_ROTATION(sYogaMat.ObjectIndex, sYogaMat.vRotation)
					FREEZE_ENTITY_POSITION(sYogaMat.ObjectIndex, TRUE)
				
					FailFlags[MINIGAME_FAIL_YOGA_FAILED] = TRUE
				
					iSetupProgress++
				ENDIF
				
				
			BREAK

			DEFAULT
				iSetupProgress++	
			BREAK
		
		ENDSWITCH
		
	ENDIF
	
	//handle player positioning when current stage is being skipped to or played normally
	//fade in the screen if it was faded out due to skipping
	IF ( iSetupProgress = 4 )
	
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE )
		OR ( bStageReplayed	= TRUE )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling LOAD_SCENE() at coordinates ", sMinigamePosition.vPosition, " for minigame replay or stage skip. This might take a while to load.")
			#ENDIF
		
			LOAD_SCENE(sMinigamePosition.vPosition)
			
			CLEAR_AREA(sMinigamePosition.vPosition, 15.0, TRUE)
			CLEAR_AREA_OF_PEDS(sMinigamePosition.vPosition, 20.0, TRUE)
			CLEAR_AREA_OF_VEHICLES(sMinigamePosition.vPosition, 15.0)
			REMOVE_PARTICLE_FX_IN_RANGE(sMinigamePosition.vPosition, 20.0)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			
			bStageSkippedTo = FALSE
			bStageReplayed = FALSE
			
			IF IS_SCREEN_FADED_OUT()
				DO_SAFE_SCREEN_FADE_IN(500)
			ENDIF
				
		ENDIF
		
		iSetupProgress++
	
	ENDIF

	//print the message only once per minigame stage
	//handle fades
	IF ( iSetupProgress = 5 )
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished minigame stage loading for ", GET_MINIGAME_STAGE_NAME_FOR_DEBUG(eStage), ".")
		#ENDIF
		
		bStageLoaded = TRUE
		
		iSetupProgress++
		
	ENDIF
	
	IF ( iSetupProgress = 6 )

		RETURN TRUE
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MINIGAME_STAGE_CUTSCENE_INTRO_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0	//request animations and models
		
			REQUEST_ANIM_DICT("mini@yoga")
			REQUEST_MODEL(sDock.ModelName)
			REQUEST_MODEL(sCellphone.ModelName)
			REQUEST_FAKE_CELLPHONE_MOVIE(sFakeCellphoneData)
			
			IF 	HAS_ANIM_DICT_LOADED("mini@yoga")
			AND HAS_MODEL_LOADED(sDock.ModelName)
			AND HAS_MODEL_LOADED(sCellphone.ModelName)
			AND HAS_FAKE_CELLPHONE_MOVIE_LOADED(sFakeCellphoneData)
			
				iStageProgress++
			
			ENDIF
		
		BREAK
		
		CASE 1
		
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iMinigameIntroSceneID)
			
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(sPlayerVehicleSafe.vAngledAreaPos1,
																	sPlayerVehicleSafe.vAngledAreaPos2,
																	sPlayerVehicleSafe.fAngledAreaWidth,
																	sPlayerVehicleSafe.vPosition,
																	sPlayerVehicleSafe.fHeading,
																	FALSE, FALSE, TRUE)
																	
				CLEAR_AREA(sMinigamePosition.vPosition, 15.0, TRUE)
				CLEAR_AREA_OF_PEDS(sMinigamePosition.vPosition, 20.0)
				CLEAR_AREA_OF_OBJECTS(sMinigamePosition.vPosition, 20.0)
				CLEAR_AREA_OF_PROJECTILES(sMinigamePosition.vPosition, 20.0)
				
				STOP_FIRE_IN_RANGE(sMinigamePosition.vPosition, 20.0)
				REMOVE_DECALS_IN_RANGE(sMinigamePosition.vPosition, 20.0)
				REMOVE_PARTICLE_FX_IN_RANGE(sMinigamePosition.vPosition, 20.0)
				
				
				iMinigameIntroSceneID = CREATE_SYNCHRONIZED_SCENE(vIntroScenePosition, vIntroSceneRotation)
			
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iMinigameIntroSceneID, TRUE)
				
				IF HAS_MINIGAME_OBJECT_BEEN_CREATED(sDock, TRUE)
					//
				ENDIF
				
				IF HAS_MINIGAME_OBJECT_BEEN_CREATED(sCellphone)
					IF DOES_ENTITY_EXIST(sCellphone.ObjectIndex) AND NOT IS_ENTITY_DEAD(sCellphone.ObjectIndex)
						DRAW_FAKE_CELLPHONE_SCREEN(sFakeCellphoneData, TRUE, FAKE_CELLPHONE_SCREEN_GENERIC_MENU)
						ATTACH_ENTITY_TO_ENTITY(sCellphone.ObjectIndex, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND),
											    << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
					ENDIF
				ENDIF
				
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
				
				SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iMinigameIntroSceneID, "mini@yoga", "intro", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				
				DESTROY_ALL_CAMS()
					
				AnimatedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
				PLAY_SYNCHRONIZED_CAM_ANIM(AnimatedCamera, iMinigameIntroSceneID, "intro_cam", "mini@yoga")
				
				CLEAR_HELP()
				CLEAR_PRINTS()
				
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)			
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				CLEAR_TIMECYCLE_MODIFIER()
				
				SET_MODEL_AS_NO_LONGER_NEEDED(sDock.ModelName)
				SET_MODEL_AS_NO_LONGER_NEEDED(sCellphone.ModelName)
				
				iStageProgress++
			
			ENDIF
			
		BREAK
		
		CASE 2
				
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iMinigameIntroSceneID)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			
				IF NOT IS_AUDIO_SCENE_ACTIVE("YOGA_MINIGAME_TRANQUIL")
					TRIGGER_MUSIC_EVENT("MGYG_START")
					START_AUDIO_SCENE("YOGA_MINIGAME_TRANQUIL")					
				ENDIF
					
				IF DOES_ENTITY_EXIST(sCellphone.ObjectIndex) AND NOT IS_ENTITY_DEAD(sCellphone.ObjectIndex)
					IF IS_ENTITY_ATTACHED_TO_ANY_PED(sCellphone.ObjectIndex)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iMinigameIntroSceneID) >= 0.395
							DETACH_ENTITY(sCellphone.ObjectIndex, FALSE)
							SET_ENTITY_COORDS_NO_OFFSET(sCellphone.ObjectIndex, sCellphone.vPosition)
							SET_ENTITY_ROTATION(sCellphone.ObjectIndex, sCellphone.vRotation)
							FREEZE_ENTITY_POSITION(sCellphone.ObjectIndex, TRUE)
						ENDIF
					ENDIF
				ENDIF

				IF ( bOutfitYoga = FALSE )
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMinigameIntroSceneID) >= 0.80
						SET_PLAYER_PED_OUTFIT(MINIGAME_OUTFIT_YOGA)
						bOutfitYoga = TRUE
					ENDIF
				ENDIF
			
				IF 	HAS_MINIGAME_OBJECT_BEEN_CREATED(sYogaMat, TRUE)
				AND HAS_MINIGAME_OBJECT_BEEN_CREATED(sCandle04, TRUE)
				AND HAS_MINIGAME_OBJECT_BEEN_CREATED(sCandle05, TRUE)
				AND HAS_MINIGAME_OBJECT_BEEN_CREATED(sCandle06, TRUE)
				
					IF ( bOutfitYoga = TRUE )
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
								iStageProgress++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
		
		BREAK
	
		CASE 3
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iMinigameIntroSceneID)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iMinigameIntroSceneID) >= 1.0
				
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
					ENDIF
				
					WARP_PED(PLAYER_PED_ID(), sMinigamePosition.vPosition, sMinigamePosition.fHeading, FALSE, FALSE, FALSE)
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfam5_yoga", "start_pose", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0, FALSE, AIK_DISABLE_LEG_IK)

					iStageProgress++
					
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 4
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iMinigameIntroSceneID)
		
				IF eMinigameLocation = MINIGAME_LOCATION_MICHAELS_HOUSE

					ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					SET_CAM_PARAMS(ScriptedCamera, << -782.802368, 187.4389, 73.326614>>, << -3.540541, 0.000000, 99.799942>>, 39.355549)
								   
				ELIF eMinigameLocation = MINIGAME_LOCATION_NORTH_COAST
				
					ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					SET_CAM_PARAMS(ScriptedCamera, <<2870.826172,5943.484863,356.912140>>, <<11.166323,0.034383,76.034073>>, 39.355549)
								   
				ENDIF
				
				DESTROY_CAM(AnimatedCamera)
				
				REMOVE_ANIM_DICT("mini@yoga")
								
				RETURN TRUE
				
			ENDIF
		
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MINIGAME_STAGE_YOGA_WARRIOR_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0

			IF NOT DOES_CAM_EXIST(ScriptedCamera)
			
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				IF eMinigameLocation = MINIGAME_LOCATION_MICHAELS_HOUSE
					
					SET_CAM_PARAMS(ScriptedCamera, << -782.802368, 187.4389, 73.326614>>, << -3.540541, 0.000000, 99.799942>>, 39.355549)
					SET_CAM_PARAMS(ScriptedCamera, << -787.032959, 186.882339, 73.063408 >>, << -3.376375, 0.000000, 97.665771 >>, 39.355549,
								   25000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
				
				ELIF eMinigameLocation = MINIGAME_LOCATION_NORTH_COAST
				
					SET_CAM_PARAMS(ScriptedCamera, <<2870.826172,5943.484863,356.912140>>, <<11.166323,0.034383,76.034073>>, 39.355549) // CAM 2
					SET_CAM_PARAMS(ScriptedCamera, <<2867.668213,5944.266113,357.554688>>,<<9.696003,0.034384,75.404343>>,39.355549,
								   25000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL) // CAM3
								   
				ENDIF
				
				SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.25)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera.")
				#ENDIF
				
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				
				SET_CAM_FAR_CLIP(ScriptedCamera, 500)
				
			ENDIF
			
			IF DOES_CAM_EXIST(ScriptedCamera)
			
				IF eMinigameLocation = MINIGAME_LOCATION_MICHAELS_HOUSE
			
					SET_CAM_PARAMS(ScriptedCamera, << -782.802368, 187.4389, 73.326614>>, << -3.540541, 0.000000, 99.799942>>, 39.355549)
					SET_CAM_PARAMS(ScriptedCamera, << -787.032959, 186.882339, 73.063408 >>, << -3.376375, 0.000000, 97.665771 >>, 39.355549,
								   25000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
				
				ELIF eMinigameLocation = MINIGAME_LOCATION_NORTH_COAST
				
					SET_CAM_PARAMS(ScriptedCamera, <<2870.826172,5943.484863,356.912140>>, <<11.166323,0.034383,76.034073>>, 39.355549) // CAM 2
					SET_CAM_PARAMS(ScriptedCamera, <<2867.668213,5944.266113,357.554688>>,<<9.696003,0.034384,75.404343>>,39.355549,
								   25000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL) // CAM3
								   
				ENDIF
				
				SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.25)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			ENDIF
			
			iStageProgress++

		BREAK
	
		CASE 1
		
			//trigger music event for completed pose
			IF ( bMusicEventPositionCompleteTriggered = FALSE )
				IF ( sYogaPlayer.eYogaMinigameStage = YOGAMINIGAMESTAGE_DOMOVE )				//when player is doing yoga moves and when
					IF ( sYogaPlayer.eYogaMoveStage = YOGAMOVESTAGE_PLAY_ENDPOSE_ANIMATION )	//player has completed pose is playing end pose animation
						IF PREPARE_MUSIC_EVENT("MGYG_POSITION_COMPLETE")
							IF TRIGGER_MUSIC_EVENT("MGYG_POSITION_COMPLETE")
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggered music event MGYG_POSITION_COMPLETE.")
								#ENDIF
								bMusicEventPositionCompleteTriggered = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bIsQuitMenuActive = FALSE
			
				RUN_YOGA_CUSTOM_CAMERA(sYogaPlayer)
				UPDATE_YOGA_TIMECYCLE_MODIFIER(sYogaPlayer, TRUE)
			
				IF HAS_PLAYER_COMPLETED_YOGA_MOVE(sYogaPlayer, YOGAMOVE_WARRIOR, TRUE, TRUE, FALSE, DEFAULT, ENUM_TO_INT(eMinigameLocation), TRUE)
				
					CHECK_DECREASE_KHARMA()
					RESET_YOGA_STRUCT(sYogaPlayer, FALSE)
					
					bMusicEventPositionCompleteTriggered = FALSE
					
					iStageProgress++
				
				ENDIF

			ENDIF
		
		BREAK
		
		CASE 2
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MINIGAME_STAGE_YOGA_TRIANGLE_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0
			
			IF NOT DOES_CAM_EXIST(ScriptedCamera)
			
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				IF eMinigameLocation = MINIGAME_LOCATION_MICHAELS_HOUSE
				
					SET_CAM_PARAMS(ScriptedCamera, << -782.802368, 187.4389, 73.326614>>, << -3.540541, 0.000000, 99.799942>>, 39.355549)
					SET_CAM_PARAMS(ScriptedCamera, << -787.032959, 186.882339, 73.063408 >>, << -3.376375, 0.000000, 97.665771 >>, 39.355549,
								   25000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
					
				ELIF eMinigameLocation = MINIGAME_LOCATION_NORTH_COAST
				
					SET_CAM_PARAMS(ScriptedCamera, <<2870.826172,5943.484863,356.912140>>, <<11.166323,0.034383,76.034073>>, 39.355549) // CAM 2
					SET_CAM_PARAMS(ScriptedCamera, <<2867.668213,5944.266113,357.554688>>,<<9.696003,0.034384,75.404343>>,39.355549,
								   25000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL) // CAM3
				
				ENDIF
				
				SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.25)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera.")
				#ENDIF
				
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_CAM_FAR_CLIP(ScriptedCamera, 500)
				
			ENDIF
			
			iStageProgress++

		BREAK
	
		CASE 1
		
			//trigger music event for completed pose
			IF ( bMusicEventPositionCompleteTriggered = FALSE )
				IF ( sYogaPlayer.eYogaMinigameStage = YOGAMINIGAMESTAGE_DOMOVE )				//when player is doing yoga moves and when
					IF ( sYogaPlayer.eYogaMoveStage = YOGAMOVESTAGE_PLAY_ENDPOSE_ANIMATION )	//player has completed pose is playing end pose animation
						IF PREPARE_MUSIC_EVENT("MGYG_POSITION_COMPLETE")
							IF TRIGGER_MUSIC_EVENT("MGYG_POSITION_COMPLETE")
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggered music event MGYG_POSITION_COMPLETE.")
								#ENDIF
								bMusicEventPositionCompleteTriggered = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF bIsQuitMenuActive = FALSE
			
				RUN_YOGA_CUSTOM_CAMERA(sYogaPlayer)
				UPDATE_YOGA_TIMECYCLE_MODIFIER(sYogaPlayer, TRUE)
				
				IF HAS_PLAYER_COMPLETED_YOGA_MOVE(sYogaPlayer, YOGAMOVE_TRIANGLE, TRUE, FALSE, FALSE, DEFAULT, ENUM_TO_INT(eMinigameLocation), TRUE)
				
					CHECK_DECREASE_KHARMA()
					RESET_YOGA_STRUCT(sYogaPlayer, FALSE)
					
					bMusicEventPositionCompleteTriggered = FALSE
				
					iStageProgress++
				
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 2
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MINIGAME_STAGE_YOGA_SUNSALUTATION_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0

			IF NOT DOES_CAM_EXIST(ScriptedCamera)
			
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				IF eMinigameLocation = MINIGAME_LOCATION_MICHAELS_HOUSE
				
					SET_CAM_PARAMS(ScriptedCamera, << -782.802368, 187.4389, 73.326614>>, << -3.540541, 0.000000, 99.799942>>, 39.355549)
					SET_CAM_PARAMS(ScriptedCamera, << -787.032959, 186.882339, 73.063408 >>, << -3.376375, 0.000000, 97.665771 >>, 39.355549,
								   25000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
								   
				ELIF eMinigameLocation = MINIGAME_LOCATION_NORTH_COAST
				
					SET_CAM_PARAMS(ScriptedCamera, <<2870.826172,5943.484863,356.912140>>, <<11.166323,0.034383,76.034073>>, 39.355549) // CAM 2
					SET_CAM_PARAMS(ScriptedCamera, <<2867.668213,5944.266113,357.554688>>,<<9.696003,0.034384,75.404343>>,39.355549,
								   25000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL) // CAM3
					
				ENDIF
				
				SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.25)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera.")
				#ENDIF
				
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_CAM_FAR_CLIP(ScriptedCamera, 500)

				
			ENDIF
			
			iStageProgress++

		BREAK
	
		CASE 1
		
			//trigger music event for completed pose
			IF ( bMusicEventPositionCompleteTriggered = FALSE )
				IF ( sYogaPlayer.eYogaMinigameStage = YOGAMINIGAMESTAGE_DOMOVE )				//when player is doing yoga moves and when
					IF ( sYogaPlayer.eYogaMoveStage = YOGAMOVESTAGE_PLAY_ENDPOSE_ANIMATION )	//player has completed pose is playing end pose animation
						IF PREPARE_MUSIC_EVENT("MGYG_POSITION_COMPLETE")
							IF TRIGGER_MUSIC_EVENT("MGYG_POSITION_COMPLETE")
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggered music event MGYG_POSITION_COMPLETE.")
								#ENDIF
								bMusicEventPositionCompleteTriggered = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF bIsQuitMenuActive = FALSE
			
				RUN_YOGA_CUSTOM_CAMERA(sYogaPlayer)
				UPDATE_YOGA_TIMECYCLE_MODIFIER(sYogaPlayer, TRUE)
			
				IF HAS_PLAYER_COMPLETED_YOGA_MOVE(sYogaPlayer, YOGAMOVE_SUNSALUTATION, TRUE, FALSE, FALSE, DEFAULT, ENUM_TO_INT(eMinigameLocation), TRUE)
				
					CHECK_DECREASE_KHARMA()
					RESET_YOGA_STRUCT(sYogaPlayer, FALSE)
					
					bMusicEventPositionCompleteTriggered = FALSE
				
					iStageProgress++
				
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 2
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MINIGAME_STAGE_CUTSCENE_END_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			iStageProgress++
					
		BREAK
	
		CASE 1	
		
			IF NOT bHasQuit
		
				REQUEST_ANIM_DICT("mini@yoga")
				REQUEST_MODEL(sDock.ModelName)
				REQUEST_MODEL(sCellphone.ModelName)
				REQUEST_ANIM_DICT("move_p_m_zero_idles@generic")
				REQUEST_FAKE_CELLPHONE_MOVIE(sFakeCellphoneData)
				
				IF 	HAS_ANIM_DICT_LOADED("mini@yoga")
				AND HAS_MODEL_LOADED(sDock.ModelName)
				AND HAS_MODEL_LOADED(sCellphone.ModelName)
				AND HAS_ANIM_DICT_LOADED("move_p_m_zero_idles@generic")
				AND HAS_FAKE_CELLPHONE_MOVIE_LOADED(sFakeCellphoneData)
				
					iStageProgress++
				
				ENDIF
				
			ELSE
				
				IF IS_SCREEN_FADED_IN()
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				ENDIF
				
				IF IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
				
					iStageProgress = END_CUTSCENE_QUIT
					
				ENDIF
			
			ENDIF
			
		BREAK
		
		CASE 2
		
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iMinigameOutroScene1ID)
		
				iMinigameOutroScene1ID = CREATE_SYNCHRONIZED_SCENE(vOutro1ScenePosition, vOutro1SceneRotation)
		
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iMinigameOutroScene1ID, TRUE)
				
				
				IF HAS_MINIGAME_OBJECT_BEEN_CREATED(sDock, TRUE)
					//
				ENDIF
				
				IF HAS_MINIGAME_OBJECT_BEEN_CREATED(sCellphone, TRUE)
					DRAW_FAKE_CELLPHONE_SCREEN(sFakeCellphoneData, TRUE, FAKE_CELLPHONE_SCREEN_GENERIC_MENU)
				ENDIF
				
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
				
				SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iMinigameOutroScene1ID, "mini@yoga", "outro_1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
				AnimatedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
				PLAY_SYNCHRONIZED_CAM_ANIM(AnimatedCamera, iMinigameOutroScene1ID, "outro_1_cam", "mini@yoga")
				
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)			
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				IF eMinigameFail = MINIGAME_FAIL_YOGA_FAILED
					IF TRIGGER_MUSIC_EVENT("MGYG_END")
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggered music event MGYG_END.")
						#ENDIF
					ENDIF
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(sDock.ModelName)
				SET_MODEL_AS_NO_LONGER_NEEDED(sCellphone.ModelName)
				
				NEW_LOAD_SCENE_START(vLoadSceneCamPosition, vLoadSceneCamDirection, 200.0)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_START().")
				#ENDIF
				
				iStageProgress++
		
			ENDIF
		
		BREAK
		
		CASE 3
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iMinigameOutroScene1ID)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				IF DOES_ENTITY_EXIST(sCellphone.ObjectIndex) AND NOT IS_ENTITY_DEAD(sCellphone.ObjectIndex)
					IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(sCellphone.ObjectIndex)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iMinigameOutroScene1ID) >= 0.305
							FREEZE_ENTITY_POSITION(sCellphone.ObjectIndex, FALSE)
							ATTACH_ENTITY_TO_ENTITY(sCellphone.ObjectIndex, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND),
											    	<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iMinigameOutroScene1ID) >= 1.0
				
					IF eMinigameFail = MINIGAME_FAIL_YOGA_FAILED
					
						SET_PLAYER_PED_OUTFIT(MINIGAME_OUTFIT_DEFAULT)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
					
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), sPlayerEndgame.vGotoPosition)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), sPlayerEndgame.fGotoHeading)
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "move_p_m_zero_idles@generic", "fidget_impatient",
										   INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, AF_HOLD_LAST_FRAME, 0.445)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							
							DESTROY_ALL_CAMS()
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
							
							CLEANUP_OBJECT(sCellphone.ObjectIndex, TRUE)
					
							iStageProgress = END_CUTSCENE_FAILURE
						
						ENDIF

					ELSE
				
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iMinigameOutroScene2ID)
			
							iMinigameOutroScene2ID = CREATE_SYNCHRONIZED_SCENE(vOutro2ScenePosition, vOutro2SceneRotation)

							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iMinigameOutroScene2ID, "mini@yoga", "outro_2", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								
							PLAY_SYNCHRONIZED_CAM_ANIM(AnimatedCamera, iMinigameOutroScene2ID, "outro_2_cam", "mini@yoga")

							IF TRIGGER_MUSIC_EVENT("MGYG_END")
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggered music event MGYG_END.")
								#ENDIF
							ENDIF

							iStageProgress = END_CUTSCENE_SUCCESS
							
						ENDIF
					
					ENDIF
					
				ENDIF
			ENDIF
		
		BREAK
		
		CASE END_CUTSCENE_SUCCESS
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iMinigameOutroScene2ID)
			
				IF ( bOutfitYoga = TRUE )
					SET_PLAYER_PED_OUTFIT(MINIGAME_OUTFIT_DEFAULT)
					bOutfitYoga = FALSE
				ENDIF
				
				CLEANUP_OBJECT(sDock.ObjectIndex, TRUE)
				CLEANUP_OBJECT(sCellphone.ObjectIndex, TRUE)
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iMinigameOutroScene2ID) >= 0.95
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iMinigameOutroScene2ID) >= 1.0
				
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
					DESTROY_ALL_CAMS()
					
					NEW_LOAD_SCENE_STOP()

					REMOVE_ANIM_DICT("mini@yoga")
					
					iStageProgress = END_CUTSCENE_FINISH
				
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE END_CUTSCENE_FAILURE
		
			CLEANUP_OBJECT(sDock.ObjectIndex, TRUE)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "move_p_m_zero_idles@generic", "fidget_impatient")
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "move_p_m_zero_idles@generic", "fidget_impatient") >= 0.99 
					
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						
						iStageProgress = END_CUTSCENE_FINISH
						
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE END_CUTSCENE_QUIT
			
			IF bHasQuit = TRUE
		
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), sPlayerEndgame.vGotoPosition)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sPlayerEndgame.fGotoHeading)
				ENDIF
				
				CLEAR_TIMECYCLE_MODIFIER()
				
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				
				CLEANUP_OBJECT(sDock.ObjectIndex, TRUE)
				CLEANUP_OBJECT(sCellphone.ObjectIndex, TRUE)
				
				IF ( bOutfitYoga = TRUE )
					SET_PLAYER_PED_OUTFIT(MINIGAME_OUTFIT_DEFAULT)
					bOutfitYoga = FALSE
				ENDIF
			
				iStageProgress = END_CUTSCENE_FINISH
				
			ENDIF
		
		BREAK
		
		CASE END_CUTSCENE_FINISH
		
			IF IS_SCREEN_FADED_OUT()
				IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
			
			IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
			
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
			
				NEW_LOAD_SCENE_STOP()
				CLEAR_TIMECYCLE_MODIFIER()
				REMOVE_ANIM_DICT("mini@yoga")
			
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				
				RETURN TRUE
				
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iMinigameOutroScene1ID)
	OR IS_SYNCHRONIZED_SCENE_RUNNING(iMinigameOutroScene2ID)
		UPDATE_YOGA_TIMECYCLE_MODIFIER(sYogaPlayer, FALSE, 0.0, 0.0005)
	ENDIF

	RETURN FALSE

ENDFUNC

//|============================= END MINIGAME STAGES FUNCTIONS ===========================|

//|==================================== MAIN SCRIPT LOOP =================================|

SCRIPT
	
	SET_MINIGAME_IN_PROGRESS(TRUE)
		
	DISABLE_CELLPHONE(TRUE)
	
	DISABLE_SELECTOR()
	
	INIT_PC_SCRIPTED_CONTROLS("Yoga")
	bSouthpaw = FALSE
		
	//check for death or arrest
    IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY) 
	
        MINIGAME_CLEANUP()
		
		TERMINATE_THIS_THREAD()
		
    ENDIF
	
	SET_MAX_WANTED_LEVEL(0) //disable wanted levels for this mini game.
	
	STOP_SMOKE_GRENADE_EXPLOSION_SOUNDS()
	
	// Turn off player control immediately to stop the player doing silly things like detonating stickybombs underneath themselves.
	IF NOT IS_PED_INJURED( PLAYER_PED_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
		
	#IF IS_DEBUG_BUILD
		CREATE_DEBUG_MINIGAME_STAGE_MENU()
		CREATE_DEBUG_WIDGETS()
	#ENDIF
	
	//load the minigame text scaleform UI and other assets
	//scaleformQuitUI = REQUEST_QUIT_UI()
	WHILE NOT ARE_YOGA_REQUESTS_LOADED()
		WAIT(0)
	ENDWHILE
	
	//WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(scaleformQuitUI)
	//	WAIT(0)
	//ENDWHILE
		
	bHasQuit = FALSE
	fKharma = 1.0
	
	WHILE (TRUE)

		//check each frame if minigame is failed due to fail conditions
		//check only if minigame stage loading has finished
		IF ( bLoadingFlag = TRUE )
			RUN_FAIL_CHECKS(eMinigameStage, eMinigameFail)
		ENDIF
		
		// This is a kludge to force player control off for the duration of the mission as something keeps turning it back on again.
		//IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE )
		//ENDIF
		
		YOGA_MANAGE_QUIT_UI()
		
		SET_SCENARIO_TYPE_ENABLED("WORLD_MOUNTAIN_LION_WANDER",FALSE)
		SET_PED_MODEL_IS_SUPPRESSED(A_C_MTLION,TRUE)
		
		SWITCH eMinigameStage
		
			CASE MINIGAME_STAGE_SETUP	
				
				IF IS_MINIGAME_STAGE_LOADED(eMinigameStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
				
					RESET_MINIGAME_FLAGS()
				
					eMinigameStage = MINIGAME_STAGE_CUTSCENE_INTRO
					
				ENDIF

			BREAK
			
			CASE MINIGAME_STAGE_CUTSCENE_INTRO
			
				IF IS_MINIGAME_STAGE_LOADED(eMinigameStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)

					IF IS_MINIGAME_STAGE_CUTSCENE_INTRO_COMPLETED(iMinigameStageProgress)
					
						RESET_MINIGAME_FLAGS()

						eMinigameStage = MINIGAME_STAGE_YOGA_WARRIOR
						
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE MINIGAME_STAGE_YOGA_WARRIOR

				IF IS_MINIGAME_STAGE_LOADED(eMinigameStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)

					IF IS_MINIGAME_STAGE_YOGA_WARRIOR_COMPLETED(iMinigameStageProgress)
								
						RESET_MINIGAME_FLAGS()

						eMinigameStage = MINIGAME_STAGE_YOGA_TRIANGLE
						
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE MINIGAME_STAGE_YOGA_TRIANGLE
			
				IF IS_MINIGAME_STAGE_LOADED(eMinigameStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)

					IF IS_MINIGAME_STAGE_YOGA_TRIANGLE_COMPLETED(iMinigameStageProgress)
										
						RESET_MINIGAME_FLAGS()

						eMinigameStage = MINIGAME_STAGE_YOGA_SUNSALUTATION
						
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE MINIGAME_STAGE_YOGA_SUNSALUTATION

				IF IS_MINIGAME_STAGE_LOADED(eMinigameStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)

					IF IS_MINIGAME_STAGE_YOGA_SUNSALUTATION_COMPLETED(iMinigameStageProgress)
									
						RESET_MINIGAME_FLAGS()

						eMinigameStage = MINIGAME_STAGE_PASSED_CUTSCENE_END
						
					ENDIF
				
				ENDIF
				
			BREAK
			
			CASE MINIGAME_STAGE_PASSED_CUTSCENE_END
			CASE MINIGAME_STAGE_FAILED_CUTSCENE_END

				IF IS_MINIGAME_STAGE_LOADED(eMinigameStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)

					IF IS_MINIGAME_STAGE_CUTSCENE_END_COMPLETED(iMinigameStageProgress)
					
						RESET_MINIGAME_FLAGS()

						SWITCH eMinigameStage
						
							CASE MINIGAME_STAGE_PASSED_CUTSCENE_END
								eMinigameStage = MINIGAME_STAGE_PASSED
							BREAK
							
							CASE MINIGAME_STAGE_FAILED_CUTSCENE_END
								eMinigameStage = MINIGAME_STAGE_FAILED
							BREAK
							
						ENDSWITCH
						
					ENDIF
					
				ENDIF
				
			BREAK
			
			
			CASE MINIGAME_STAGE_PASSED
				
				//IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//	RESTORE_PLAYER_PED_HEALTH(PLAYER_PED_ID())		//restore player's health
				//ENDIF
				
				//IF IS_PLAYER_PLAYING(PLAYER_ID())
				//	SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), TRUE)	//restore player's special ability
				//ENDIF
				
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), fKharma, TRUE) // Special ability bar refilled depending on Kharma
				ENDIF
				
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_YOGA)
				
				MAKE_AUTOSAVE_REQUEST()
			
				MINIGAME_CLEANUP()
				
				TERMINATE_THIS_THREAD()
				
			BREAK
			
			CASE MINIGAME_STAGE_FAILED
								
				MINIGAME_CLEANUP()
				
				TERMINATE_THIS_THREAD()
				
			BREAK
			
		ENDSWITCH
		
		//handle debug functionality
		#IF IS_DEBUG_BUILD
		
			//handle key_f and key_s presses
			RUN_DEBUG_PASS_AND_FAIL_CHECK(eMinigameStage, eMinigameFail)
			
			//handle z-skip menu
			RUN_DEBUG_MINIGAME_STAGE_MENU(iReturnStage, eMinigameStage, bSkipFlag, bStageResetFlag)
			
			//handle j-skip and p-skip
			RUN_DEBUG_SKIPS(eMinigameStage, bSkipFlag, bStageResetFlag)
			
			//handle rag widgets
			RUN_DEBUG_WIDGETS()
		
		#ENDIF
	
		WAIT(0)
		
	ENDWHILE

ENDSCRIPT

//|================================= END MAIN SCRIPT LOOP ================================|
