// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	TRI_Triathlon_Roads.sch
//		AUTHOR			:	Carlos Mijares (CM)
//		DESCRIPTION		:	Manages Triathlon race roads.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************





// ============================================================
// 	FILE INCLUDES
// ============================================================

// -----------------------------------
// 	GENERAL INCLUDES
// -----------------------------------

USING "minigames_helpers.sch"
USING "minigame_uiinputs.sch"
USING "commands_xml.sch"


// -----------------------------------
// SPR FILE INCLUDES
// -----------------------------------

USING "tri_helpers_triathlon.sch"

// ============================================================
// 	E N D  FILE INCLUDES
// ============================================================










// ============================================================
//  ROADS VARIABLES
// ============================================================

// -------------------------------------------
//	RACE BARRICADES VARIABLES
// -------------------------------------------

// Race barricades.
CONST_INT iNumobjTriRaceBarricades				101
OBJECT_INDEX objTriRaceBarricades[iNumobjTriRaceBarricades]

CONST_INT iNumTriRaceBarricadesPerGroup			20
OBJECT_INDEX objTriRaceBarricades_Group1[iNumTriRaceBarricadesPerGroup]
OBJECT_INDEX objTriRaceBarricades_Group2[iNumTriRaceBarricadesPerGroup]
OBJECT_INDEX objTriRaceBarricades_Group3[iNumTriRaceBarricadesPerGroup]
OBJECT_INDEX objTriRaceBarricades_Group4[iNumTriRaceBarricadesPerGroup]

// Update race barricades at this gate.  A value of -1 means it won't update.
INT iPlayerPreviousBarricadeGate

OBJECT_INDEX objRaceBanners[2]





// -------------------------------------------
//	RACE SPEED ZONES VARIABLES
// -------------------------------------------

// Track the road indeces that slow down traffic to prevent their interference in race.
BOOL bAreNonIronSpeedZonesSet

// Tri 1 and Tri 2 are short enough where we can have speed zones active simultaneously for all intersections in the race road.
CONST_INT iNumOfSpeedDampenerRoadNodes	32
INT iTriSpeedDampenerRoadNodes[iNumOfSpeedDampenerRoadNodes]

// Because of the great length of Ironman, we manage what speed zones are active based on the player's current gate.
CONST_INT iNumOfIronSpeedDampenerRoadNodesPerGroup	8
INT iTriIronSpeedDampenerRoadNodes_Group1[iNumOfIronSpeedDampenerRoadNodesPerGroup]
INT iTriIronSpeedDampenerRoadNodes_Group2[iNumOfIronSpeedDampenerRoadNodesPerGroup]
INT iTriIronSpeedDampenerRoadNodes_Group3[iNumOfIronSpeedDampenerRoadNodesPerGroup]
INT iTriIronSpeedDampenerRoadNodes_Group4[iNumOfIronSpeedDampenerRoadNodesPerGroup]

// Update speed zone nodes at this gate.  A value of -1 means it won't update.
INT iPlayerPreviousSpeedZoneGate

// ============================================================
//  E N D  ROADS VARIABLES
// ============================================================










// ===================================================
// 	RACE ROADS PROCEDURES AND FUNCTIONS
// ===================================================

/// PURPOSE:
///    Wrapper for ADD_SCENARIO_BLOCKING_AREA, adds a blocking area at vCenter with a (vTriScenarioOffset * fAreaScalar) offset for vMin and vMax
/// PARAMS:
///    vCenter - 
///    fAreaScalar - 
PROC ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(VECTOR vCenter, FLOAT fAreaScalar = 1.0)
	CPRINTLN(DEBUG_TRIATHLON, "ADD_TRIATHLON_SCENARIO_BLOCKING_AREA added at ", vCenter, ", with fAreaScalar of ", fAreaScalar)
	VECTOR vTriScenarioOffset = <<3,3,3>>
	ADD_SCENARIO_BLOCKING_AREA(vCenter - vTriScenarioOffset * fAreaScalar, vCenter + vTriScenarioOffset * fAreaScalar)
ENDPROC

PROC CREATE_TRI_RACE_BANNERS(INT iRaceCur)
	CPRINTLN(DEBUG_TRIATHLON, "[triathlon\tri_roads.sch] CREATE_TRI_RACE_BANNERS called")
	
	UNUSED_PARAMETER(iRaceCur)
	#IF IS_DEBUG_BUILD
		IF DOES_ENTITY_EXIST(objRaceBanners[1])
	//	TEST_VECTOR_1 = <<1579.837, 3835.127, 31.298>>
		TEST_VECTOR_2 = GET_ENTITY_COORDS(objRaceBanners[1])
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    come back to this, it is temporary and for test
PROC CREATE_TRI_SWIM_LEG_BUOYS(INT iRaceCur, TRI_RACE_STRUCT &race)
	CPRINTLN(DEBUG_TRIATHLON, "[tri_roads.sch::CREATE_TRI_SWIM_LEG_BUOYS] called")
	IF iRaceCur = ENUM_TO_INT(TRIATHLON_RACE_ALAMO_SEA)
		race.objSwimLegBuoys[0] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<2347.9053, 4268.9404, 30.5569>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[0], TRUE )
		race.objSwimLegBuoys[1] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<2221.4478, 4203.9600, 30.3139>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[1], TRUE )
		race.objSwimLegBuoys[2] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<2208.5334, 4032.4700, 30.1761>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[2], TRUE )
	ELIF iRaceCur = ENUM_TO_INT(TRIATHLON_RACE_VESPUCCI)
		race.objSwimLegBuoys[0] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<-1282.6902, -2014.7354, 0.2113>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[0], TRUE )
		race.objSwimLegBuoys[1] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<-1321.1080, -1934.6177, 0.3026>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[1], TRUE )
		race.objSwimLegBuoys[2] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<-1323.6047, -1829.1255, 0.3970>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[2], TRUE )
	ELIF iRaceCur = ENUM_TO_INT(TRIATHLON_RACE_IRONMAN)
		race.objSwimLegBuoys[0] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<1541.7368, 3857.7822, 30.1022>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[0], TRUE )
		race.objSwimLegBuoys[1] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<1400.9979, 3899.6733, 30.3399>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[1], TRUE )
		race.objSwimLegBuoys[2] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<1138.2854, 3967.3750, 30.3145>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[2], TRUE )
		race.objSwimLegBuoys[3] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<718.0370, 4075.2139, 30.1365>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[3], TRUE )
		race.objSwimLegBuoys[4] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<256.9774, 4198.9800, 30.1401>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[4], TRUE )
		race.objSwimLegBuoys[5] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<-39.6365, 4202.0918, 29.9779>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[5], TRUE )
		race.objSwimLegBuoys[6] = CREATE_OBJECT(PROP_DOCK_BOUY_3, <<-138.2176, 4133.3237, 30.1959>>)
		SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY( race.objSwimLegBuoys[6], TRUE )
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC RESET_TRI_BANNERS()
	IF DOES_ENTITY_EXIST(objRaceBanners[0]) AND IS_CHEATING_ENABLED
		SET_ENTITY_COORDS(objRaceBanners[0], TEST_VECTOR_1)
		SET_ENTITY_HEADING(objRaceBanners[0], TEST_HEADING)
	ENDIF
	IF DOES_ENTITY_EXIST(objRaceBanners[1]) AND IS_CHEATING_ENABLED
		SET_ENTITY_COORDS(objRaceBanners[1], TEST_VECTOR_2)
		SET_ENTITY_HEADING(objRaceBanners[1], TEST_HEADING)
	ENDIF
ENDPROC
#ENDIF

PROC CLEANUP_TRI_RACE_BANNERS()
	SET_OBJECT_AS_NO_LONGER_NEEDED(objRaceBanners[0])
	IF IS_TRI_CONTROL_FLAG_SET(TCF_FINISHED_RACE)
		DELETE_OBJECT(objRaceBanners[1])
	ELSE
		SET_OBJECT_AS_NO_LONGER_NEEDED(objRaceBanners[1])
	ENDIF
ENDPROC

PROC CLEANUP_TRI_SWIM_LEG_BUOYS(TRI_RACE_STRUCT &race)
	SET_OBJECT_AS_NO_LONGER_NEEDED(race.objSwimLegBuoys[0])
	SET_OBJECT_AS_NO_LONGER_NEEDED(race.objSwimLegBuoys[1])
	SET_OBJECT_AS_NO_LONGER_NEEDED(race.objSwimLegBuoys[2])
	SET_OBJECT_AS_NO_LONGER_NEEDED(race.objSwimLegBuoys[3])
	SET_OBJECT_AS_NO_LONGER_NEEDED(race.objSwimLegBuoys[4])
	SET_OBJECT_AS_NO_LONGER_NEEDED(race.objSwimLegBuoys[5])
	SET_OBJECT_AS_NO_LONGER_NEEDED(race.objSwimLegBuoys[6])
	
	SET_OBJECT_AS_NO_LONGER_NEEDED(TRI_Master.oPencil)
ENDPROC

/// PURPOSE:
///    Manage the creation and deletion of barricades throughout the race based
///    on the player's current gate.  The barricades are an additional player
///    direction element to let the player know what roads are part of the race.
///    
///    NOTE:	This is fugly, but because of the different layouts of the race, 
///    			as well as memory limits, spawning and despawning barricades throughout 
///    			the race needs to be handled manually.
///    
///    NOTE:	Ironman barricades are unfinished.  They have been created up until
///    			the beginning of the mountain descent, a little after the player
///    			crosses the .
PROC UPDATE_TRI_RACE_ROAD_BARRICADES(TRI_RACE_STRUCT& Race)
	IF iPlayerPreviousBarricadeGate <> Race.Racer[0].iGateCur
		INT iCurrentPlayerGate 	= Race.Racer[0].iGateCur
		INT iBarricadeCounter

		SWITCH eCurrentTriRace
			CASE TRIATHLON_RACE_ALAMO_SEA
				SWITCH iCurrentPlayerGate
				
					// Tri 1 Gate: Race start.
					CASE 0	
						// First right blockade.
						objTriRaceBarricades[0] = CREATE_OBJECT(Prop_Barrier_Work06A, << 2004.35, 3820.26, 31.29 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[0], << 0.00, 0.00, 26.93 >>)
						
						objTriRaceBarricades[1] = CREATE_OBJECT(Prop_Barrier_Work06A, << 2008.53, 3822.45, 31.27 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[1], << 0.00, 0.00, 30.94 >>)
		
						
						// First left blockade.
						objTriRaceBarricades[2] = CREATE_OBJECT(Prop_Barrier_Work06A, << 2014.15, 3811.08, 31.28 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[2], << 0.00, 0.00, 29.79 >>)
						
						objTriRaceBarricades[3] = CREATE_OBJECT(Prop_Barrier_Work06A, << 2009.98, 3808.24, 31.29 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[3], << 0.00, 0.00, 32.09 >>)
						
						
						// Second right blockade.
						objTriRaceBarricades[4] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1926.76, 3779.54, 31.30 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[4], << 0.00, 0.00, 0.00 >>)
						
						objTriRaceBarricades[5] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1922.26, 3777.00, 31.33 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[5], << 0.00, 0.00, 32.66 >>)
						

						// First front blockade.
						objTriRaceBarricades[6] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1920.64, 3762.73, 31.34 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[6], << 0.00, 0.00, -59.59 >>)
						
						objTriRaceBarricades[7] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1917.69, 3767.16, 31.31 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[7], << 0.00, 0.00, -52.71 >>)
						
						
						// Second left blockade.
						objTriRaceBarricades[8] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1959.77, 3728.88, 31.36 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[8], << 0.00, 0.00, -60.16 >>)
						
						objTriRaceBarricades[9] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1961.34, 3722.45, 31.39 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[9], << 0.00, 0.00, -108.29 >>)
						
						
						// Second front blockade.
						objTriRaceBarricades[10] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1958.50, 3717.41, 31.33 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[10], << 0.00, 0.00, 29.22 >>)
						
						objTriRaceBarricades[11] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1954.45, 3715.06, 31.35 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[11], << 0.00, 0.00, 28.07 >>)
						
						
						// Update the previous barricade gate so we don't enter this case again in the next frame. 
						iPlayerPreviousBarricadeGate = Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_RACE_ROAD_BARRICADES] Created barricades in gate 0 of Triathlon 1.")
					BREAK
					
					// Tri 1 Gate: Swim->Bike transition.
					CASE 4
						// Third front blockade.
						objTriRaceBarricades[12] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1807.02, 3632.48, 33.33 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[12], << 0.00, 0.00, -63.60 >>)
						
						objTriRaceBarricades[13] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1804.81, 3636.01, 33.33 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[13], << 0.00, 0.00, -61.88 >>)
						

						// Third right blockade.
						objTriRaceBarricades[14] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1794.72, 3686.27, 33.22 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[14], << 0.00, 0.00, -61.88 >>)
						
						objTriRaceBarricades[15] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1791.90, 3690.92, 33.25 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[15], << 0.00, 0.00, -54.43 >>)
						
						
						// Fourth front blockade.
						objTriRaceBarricades[16] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1784.15, 3694.50, 33.24 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[16], << 0.00, 0.00, 26.93 >>)
						
						objTriRaceBarricades[17] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1780.40, 3692.03, 33.26 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[17], << 0.00, 0.00, 34.38 >>)
						
						
						// Fifth front blockade.
						objTriRaceBarricades[18] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1710.76, 3640.67, 34.06 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[18], << 0.00, 0.00, -55.58 >>)
						
						objTriRaceBarricades[19] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1708.49, 3644.04, 34.07 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[19], << 0.00, 0.00, -47.56 >>)

						
						// Fourth right blockade.
						objTriRaceBarricades[20] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1683.38, 3713.10, 33.01 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[20], << 0.00, 0.00, -61.31 >>)
				
						objTriRaceBarricades[21] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1677.64, 3720.92, 33.01 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[21], << 0.00, 0.00, -28.07 >>)
					
						
						// Third left blockade.
						objTriRaceBarricades[22] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1612.28, 3668.28, 33.50 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[22], << 0.00, 0.00, -4.58 >>)
						
						objTriRaceBarricades[23] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1606.54, 3665.35, 33.50 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[23], << 0.00, 0.00, 27.50 >>)
					
						
						// Sixth front blockade.
						objTriRaceBarricades[24] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1598.00, 3667.36, 33.50 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[24], << 0.00, 0.00, -65.32 >>)
						
						objTriRaceBarricades[25] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1595.84, 3671.44, 33.52 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[25], << 0.00, 0.00, -62.45 >>)

						
						// Fifth right blockade.
						objTriRaceBarricades[26] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1578.86, 3720.93, 33.55 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[26], << 0.00, 0.00, -55.00 >>)
						
						objTriRaceBarricades[27] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1581.19, 3717.19, 33.55 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[27], << 0.00, 0.00, -56.72 >>)
						
						
						// Update the previous barricade gate so we don't enter this case again in the next frame. 
						iPlayerPreviousBarricadeGate = Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_RACE_ROAD_BARRICADES] Created barricades in gate 4 of Triathlon 1.")
					BREAK
					
					// Tri 1 Gate: Bike->First right to return to the village area.
					CASE 9
						// Delete first set of barricades.
						REPEAT (12) iBarricadeCounter
							IF DOES_ENTITY_EXIST(objTriRaceBarricades[iBarricadeCounter])
								DELETE_OBJECT(objTriRaceBarricades[iBarricadeCounter])
							ENDIF
						ENDREPEAT
						
						
						// Create peds and barricades in the final bike streets and mound.
						
						// BARRICADES: Fourth left blockade, in front of building.
						objTriRaceBarricades[0] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1538.91, 3749.03, 33.52 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[0], << 0.00, 0.00, -50.42 >>)
						
						objTriRaceBarricades[1] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1535.98, 3752.04, 33.53 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[1], << 0.00, 0.00, -42.97 >>)
						
						
						// BARRICADES: Sixth right blockade.
						objTriRaceBarricades[2] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1666.49, 3852.70, 33.91 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[2], << 0.00, 0.00, 44.12 >>)
						
						objTriRaceBarricades[3] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1670.53, 3856.54, 33.91 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[3], << 0.00, 0.00, 45.26 >>)		
				
						
						// BARRICADES: Seventh right blockade, on the opposite side of the final gate.
						objTriRaceBarricades[4] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1745.85, 3912.58, 33.93 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[4], << 0.00, 0.00, 25.78 >>)
						
						objTriRaceBarricades[5] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1749.87, 3914.58, 33.93 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[5], << 0.00, 0.00, 28.07 >>)
						

						// BARRICADES: Eigtht right blockade, across from mound.
						objTriRaceBarricades[6] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1854.29, 3938.16, 32.06 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[6], << 0.00, 0.00, 0.00 >>)
						
						objTriRaceBarricades[7] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1859.38, 3937.73, 32.06 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[7], << 0.00, 0.00, 0.00 >>)
			
						
						// Update the previous barricade gate so we don't enter this case again in the next frame. 
						iPlayerPreviousBarricadeGate = Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_RACE_ROAD_BARRICADES] Created barricades in gate 9 of Triathlon 1.")
					BREAK
					
					// Tri 1 Gate: Bike->Straight shot towards little mound.
					CASE 15
						FOR iBarricadeCounter = 12 TO (iNumobjTriRaceBarricades - 1)
							IF DOES_ENTITY_EXIST(objTriRaceBarricades[iBarricadeCounter])
								DELETE_OBJECT(objTriRaceBarricades[iBarricadeCounter])
							ENDIF
						ENDFOR
						
						
						// Create peds and barricades in the running transition area and near.
						
						// BARRICADES: First running right blockade.
						objTriRaceBarricades[8] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1973.36, 3891.21, 31.33 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[8], << 0.00, 0.00, -60.16 >>)
						
						objTriRaceBarricades[9] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1976.07, 3886.16, 31.34 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[9], << 0.00, 0.00, -67.61 >>)
				
						
						// BARRICADES: First running left blockade.
						objTriRaceBarricades[10] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1973.92, 3877.91, 31.35 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[10], << 0.00, 0.00, 26.93>>)
						
						objTriRaceBarricades[11] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1969.69, 3875.41, 31.33 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[11], << 0.00, 0.00, 25.78 >>)
						
						// BARRICADES: Second running left blockade.
						objTriRaceBarricades[12] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1896.09, 3831.12, 31.40 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[12], << 0.00, 0.00, 37.24 >>)
						
						objTriRaceBarricades[13] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1892.08, 3828.75, 31.43 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[13], << 0.00, 0.00, 32.09 >>)

						
						// BARRICADES: First running front blockade.
						objTriRaceBarricades[14] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1884.60, 3830.70, 31.41 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[14], << 0.00, 0.00, -59.01 >>)
						
						objTriRaceBarricades[15] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1882.81, 3834.90, 31.41 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[15], << 0.00, 0.00, -60.16 >>)
						
						objTriRaceBarricades[16] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1880.13, 3838.67, 31.41 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[16], << 0.00, 0.00, -60.16 >>)
						
						
						// Update the previous barricade gate so we don't enter this case again in the next frame. 
						iPlayerPreviousBarricadeGate = Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_RACE_ROAD_BARRICADES] Created barricades in gate 15 of Triathlon 1.")
					BREAK
					
					// Tri 1 Gate: Run->First right in run portion.
					CASE 19	
						REPEAT (8) iBarricadeCounter
							IF DOES_ENTITY_EXIST(objTriRaceBarricades[iBarricadeCounter])
								DELETE_OBJECT(objTriRaceBarricades[iBarricadeCounter])
							ENDIF
						ENDREPEAT
						
						// Create peds in the end running area.
						
						// BARRICADES: Last front blockade.
						objTriRaceBarricades[0] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1773.32, 3855.45, 33.40 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[0], << 0.00, 0.00, -61.88 >>)
						
						objTriRaceBarricades[1] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1770.90, 3859.71, 33.39 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[1], <<0.00, 0.00, -59.01 >>)

						
						// BARRICADES: Last left blockade.
						objTriRaceBarricades[2] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1785.65, 3854.89, 33.39 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[2], << 0.00, 0.00, 22.92 >>)
						
						objTriRaceBarricades[3] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1781.05, 3852.64, 33.43 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[3], << 0.00, 0.00, 21.20 >>)
						
						objTriRaceBarricades[4] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1776.57, 3852.54, 33.39 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[4], << 0.00, 0.00, -18.91 >>)
						
						
						// BARRICADES: Front blockade last intersection.
						objTriRaceBarricades[5] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1868.55, 3905.49, 32.08 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[5], << 0.00, 0.00, 0.00 >>)
						
						objTriRaceBarricades[6] = CREATE_OBJECT(Prop_Barrier_Work06A, << 1864.04, 3904.22, 32.08 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades[6], << 0.00, 0.00, 17.19 >>)
						
						
						// Update the previous barricade gate so we don't enter this case again in the next frame. 
						iPlayerPreviousBarricadeGate = Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_RACE_ROAD_BARRICADES] Created barricades in gate 19 of Triathlon 1.")
					BREAK
				
				ENDSWITCH
			BREAK
			
			
			CASE TRIATHLON_RACE_VESPUCCI
				SWITCH iCurrentPlayerGate

					// Tri 2 Gate: Start of race.
					CASE 0
						// GROUP 1
						
						// 1st set: Behind and by right side of swim->bike transition.
						objTriRaceBarricades_Group1[0] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1251.88, -1708.03, 3.47 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[0], << 0.00, 0.00, 35.52 >>)
						
						objTriRaceBarricades_Group1[1] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1254.57, -1698.15, 3.24 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[1], << 0.00, 0.00, -53.86 >>)
						
						
						// 2nd set: By right side, right after the swim->bike transition.
						objTriRaceBarricades_Group1[2] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1291.87, -1645.98, 3.39 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[2], << 0.00, 0.00, -56.15 >>)
						
						objTriRaceBarricades_Group1[3] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1293.65, -1643.15, 3.38 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[3], << 0.00, 0.00, -56.15 >>)
						
						
						// 3rd set: By right side, by the Vespucci Beach Recreation Centre.
						objTriRaceBarricades_Group1[4] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1303.26, -1627.35, 3.35 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[4], << 0.00, 0.00, -56.15 >>)
						
						objTriRaceBarricades_Group1[5] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1305.43, -1624.25, 3.36 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[5], << 0.00, 0.00, -56.15 >>)
						
						
						// 4th set: By right side, on basketball court.
						objTriRaceBarricades_Group1[6] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1312.92, -1612.41, 3.23 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[6], << 0.00, 0.00, -63.60 >>)
						
						objTriRaceBarricades_Group1[7] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1315.34, -1608.29, 3.23 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[7], << 0.00, 0.00, -57.30 >>)	
						
						objTriRaceBarricades_Group1[8] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1317.18, -1604.61, 3.23 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[8], << 0.00, 0.00, -61.88 >>)	
						
						objTriRaceBarricades_Group1[9] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1319.44, -1600.76, 3.23 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[9], << 0.00, 0.00, -57.30 >>)	
						
						objTriRaceBarricades_Group1[10] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1321.94, -1596.94, 3.23 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[10], << 0.00, 0.00, -61.31 >>)	

						objTriRaceBarricades_Group1[11] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1324.12, -1593.29, 3.23 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[11], << 0.00, 0.00, -61.31 >>)
						
						objTriRaceBarricades_Group1[12] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1326.12, -1589.81, 3.23 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[12], << 0.00, 0.00, -63.60 >>)
						
						objTriRaceBarricades_Group1[13] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1328.02, -1586.82, 3.36 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[13], << 0.00, 0.00, -58.44 >>)
						
						
						// 5th set: By right side, first right turn to a narrow path surrounded by thick trees.
						objTriRaceBarricades_Group1[14] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1339.30, -1554.03, 3.44 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[14], << 0.00, 0.00, 85.94 >>)
						
						objTriRaceBarricades_Group1[15] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1339.71, -1550.58, 3.44 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[15], << 0.00, 0.00, -85.37 >>)
							

						// 6th set: By right side, in front of skate park.
						objTriRaceBarricades_Group1[16] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1380.21, -1438.74, 2.86 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[16], << 0.00, 0.00, -71.62 >>)
						
						objTriRaceBarricades_Group1[17] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1381.51, -1434.00, 2.77 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[17], << 0.00, 0.00, -75.06 >>)
						
						objTriRaceBarricades_Group1[18] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1382.52, -1429.76, 2.69 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[18], << 0.00, 0.00, -73.34 >>)
						
						objTriRaceBarricades_Group1[19] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1384.17, -1425.30, 2.63 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[19], << 0.00, 0.00, -64.17 >>)
						
						
						
						// GROUP 2
						
						// 7th set: In front of narrow trail by the beach, redirecting race to the right, away from the beach.
						objTriRaceBarricades_Group2[0] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1357.89, -1335.12, 3.12 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[0], << 0.00, 0.00, 84.22 >>)		
						
						objTriRaceBarricades_Group2[1] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1357.00, -1331.86, 3.20 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[1], << 0.00, 0.00, 81.93 >>)
						
						objTriRaceBarricades_Group2[2] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1354.14, -1328.52, 3.48 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[2], << 0.00, 0.00, 16.04 >>)		
						
						objTriRaceBarricades_Group2[3] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1350.95, -1328.05, 3.48 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[3], << 0.00, 0.00, 6.30 >>)
						
						
						// 8th set: Right side of the beachwalk, right after exiting the narrow beach path.
						objTriRaceBarricades_Group2[4] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1330.99, -1337.14, 3.71 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[4], << 0.00, 0.00, 29.22 >>)		
						
						objTriRaceBarricades_Group2[5] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1327.36, -1334.40, 3.68 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[5], << 0.00, 0.00, 38.96 >>)
						
						objTriRaceBarricades_Group2[6] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1322.91, -1330.92, 3.71 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[6], << 0.00, 0.00, 50.42 >>)		
						
						objTriRaceBarricades_Group2[7] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1320.47, -1326.51, 3.73 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[7], << 0.00, 0.00, 68.18 >>)
						
						
						// 9th set: Beach side of the beachwalk, shortly after entering beachwalk.
						objTriRaceBarricades_Group2[8] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1358.19, -1256.65, 3.90 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[8], << 0.00, 0.00, -74.48 >>)		
						
						objTriRaceBarricades_Group2[9] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1360.16, -1251.53, 3.90 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[9], << 0.00, 0.00, -69.33 >>)
						
						objTriRaceBarricades_Group2[10] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1361.65, -1246.33, 3.89 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[10], << 0.00, 0.00, -76.20 >>)
						
						
						// 10th set: Beach side of the beachwalk, second left.
						objTriRaceBarricades_Group2[11] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1370.84, -1178.08, 3.45 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[11], << 0.00, 0.00, -85.94 >>)
						
						objTriRaceBarricades_Group2[12] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1370.87, -1173.21, 3.55 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[12], << 0.00, 0.00, 82.51 >>)
						
						
						// 11th set: Front of beachwalk, redirecting race to streets.
						objTriRaceBarricades_Group2[13] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1367.38, -1110.23, 3.43 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[13], << 0.00, 0.00, 14.90 >>)
						
						objTriRaceBarricades_Group2[14] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1362.66, -1109.99, 3.33 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[14], << 0.00, 0.00, 5.73 >>)
						
						objTriRaceBarricades_Group2[15] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1358.07, -1109.26, 3.25 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[15], << 0.00, 0.00, 7.45 >>)
						
						
						
						// GROUP 3
						
						// 12th set: Left and front by first right turn in streets.
						objTriRaceBarricades_Group3[0] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1316.74, -1083.18, 5.80 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[0], << 0.00, 0.00, 22.35 >>)
						
						objTriRaceBarricades_Group3[1] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1312.69, -1081.74, 5.95 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[1], << 0.00, 0.00, 0.0 >>)
						
						objTriRaceBarricades_Group3[2] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1307.10, -1081.34, 5.90 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[2], << 0.00, 0.00, -14.90 >>)
						
						objTriRaceBarricades_Group3[3] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1302.01, -1082.50, 5.88 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[3], << 0.00, 0.00, -37.82 >>)
						
						objTriRaceBarricades_Group3[4] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1299.40, -1086.14, 5.97 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[4], << 0.00, 0.00, -70.47 >>)						
						
						objTriRaceBarricades_Group3[5] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1298.20, -1090.00, 6.06 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[5], << 0.00, 0.00, -72.19 >>)
						
						
						// 13th set: Left of road blocking parking lot by Bean Machine.
						objTriRaceBarricades_Group3[6] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1293.63, -1133.60, 4.99 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[6], << 0.00, 0.00, -94.54 >>)
						
						objTriRaceBarricades_Group3[7] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1293.29, -1138.75, 4.81 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[7], << 0.00, 0.00, 77.35 >>)
						
						
						// 14th set: First left turn in streets.
						objTriRaceBarricades_Group3[8] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1307.44, -1191.57, 3.77 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[8], << 0.00, 0.00, -67.61 >>)
						
						objTriRaceBarricades_Group3[9] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1305.01, -1196.22, 3.92 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[9], << 0.00, 0.00, -50.42 >>)
						
						objTriRaceBarricades_Group3[10] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1300.14, -1200.32, 3.83 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[10], << 0.00, 0.00, -16.62 >>)
						
						objTriRaceBarricades_Group3[11] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1295.12, -1200.71, 3.89 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[11], << 0.00, 0.00, 13.18 >>)
						
						objTriRaceBarricades_Group3[12] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1290.70, -1199.32, 3.91 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[12], << 0.00, 0.00, 26.93 >>)
						
						objTriRaceBarricades_Group3[13] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1287.01, -1197.73, 3.77 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[13], << 0.00, 0.00, 23.49 >>)
						
						
						// 15th set: Left of road, other side of parking lot by Bean Machine.
						objTriRaceBarricades_Group3[14] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1280.56, -1174.64, 4.19 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[14], << 0.00, 0.00, 25.78 >>)
						
						objTriRaceBarricades_Group3[15] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1277.29, -1172.75, 4.44 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[15], << 0.00, 0.00, 25.78 >>)
						
						
						// GROUP 4
						
						// 16th set: Right of road, to back alley behind health food restaurant.
						objTriRaceBarricades_Group4[0] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1257.79, -1188.19, 5.20 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[0], << 0.00, 0.00, 17.19 >>)
						
						objTriRaceBarricades_Group4[1] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1254.92, -1186.69, 5.51 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[1], << 0.00, 0.00, 20.05 >>)

					
						// 17th set: Left turn to long road.
						objTriRaceBarricades_Group4[2] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1221.57, -1169.95, 6.55 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[2], << 0.00, 0.00, 9.17 >>)
						
						objTriRaceBarricades_Group4[3] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1217.41, -1168.71, 6.72 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[3], << 0.00, 0.00, 24.64 >>)
						
						objTriRaceBarricades_Group4[4] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1213.66, -1166.08, 6.74 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[4], << 0.00, 0.00, 45.84 >>)
						
						objTriRaceBarricades_Group4[5] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1210.98, -1161.85, 6.70 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[5], << 0.00, 0.00, 85.37 >>)
						
						
						// 18th set: First right turn in long road.
						objTriRaceBarricades_Group4[6] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1216.02, -1115.82, 6.81 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[6], << 0.00, 0.00, -72.77 >>)
						
						objTriRaceBarricades_Group4[7] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1217.10, -1112.19, 6.85 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[7], << 0.00, 0.00, -72.77 >>)
						
						
						// 19th set: First left turn in long road.
						objTriRaceBarricades_Group4[8] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1259.74, -1071.68, 7.34 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[8], << 0.00, 0.00, -63.60 >>)
						
						objTriRaceBarricades_Group4[9] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1261.22, -1068.47, 7.46 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[9], << 0.00, 0.00, -68.18 >>)
						
						objTriRaceBarricades_Group4[10] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1262.85, -1065.11, 7.34 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[10], << 0.00, 0.00, -71.62 >>)
						
						
						// 20th set: Second right turn in long road, into a back parking lot.
						objTriRaceBarricades_Group4[11] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1244.95, -1038.79, 7.51 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[11], << 0.00, 0.00, -73.91 >>)
						
						objTriRaceBarricades_Group4[12] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1246.41, -1034.65, 7.62 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[12], << 0.00, 0.00, -73.91 >>)
						
						
						// 21st set: Left and front at the end of the long road.
						objTriRaceBarricades_Group4[13] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1301.45, -917.82, 10.19 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[13], << 0.00, 0.00, 101.99 >>)

						objTriRaceBarricades_Group4[14] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1302.30, -912.27, 10.22 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[14], << 0.00, 0.00, 81.93 >>)
						
						objTriRaceBarricades_Group4[15] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1300.89, -907.33, 10.35 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[15], << 0.00, 0.00, 56.15 >>)
						
						objTriRaceBarricades_Group4[16] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1296.50, -903.95, 10.39 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[16], << 0.00, 0.00, 19.48 >>)

						objTriRaceBarricades_Group4[17] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1292.15, -902.31, 10.39 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[17], << 0.00, 0.00, 18.91 >>)
						
						objTriRaceBarricades_Group4[18] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1287.36, -900.65, 10.39 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[18], << 0.00, 0.00, 14.90 >>)
						
						objTriRaceBarricades_Group4[19] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1282.91, -899.03, 10.32 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group4[19], << 0.00, 0.00, 19.48 >>)
							
							
						// Update the previous barricade gate so we don't enter this case again in the next frame.
						iPlayerPreviousBarricadeGate = Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_RACE_ROAD_BARRICADES] Created barricades in gate 0 of Triathlon 2.")
					BREAK
					
					
					// Tri 2 Gate: By first right turn after exiting streets.
					CASE 15
						// Delete GROUP 1 of barricades.
						REPEAT (iNumTriRaceBarricadesPerGroup) iBarricadeCounter
							IF DOES_ENTITY_EXIST(objTriRaceBarricades_Group1[iBarricadeCounter])
								DELETE_OBJECT(objTriRaceBarricades_Group1[iBarricadeCounter])
							ENDIF
						ENDREPEAT
						
						
						// GROUP 1
						
						// 22nd set: In front of Burger Shot.
						objTriRaceBarricades_Group1[0] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1185.65, -872.02, 12.85 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[0], << 0.00, 0.00, 31.51 >>)
						
						objTriRaceBarricades_Group1[1] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1182.13, -869.96, 12.97 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[1], << 0.00, 0.00, 25.21 >>)
						
						
						// 23rd set: Left and front at the end of the long road.
						objTriRaceBarricades_Group1[2] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1187.03, -837.13, 13.19 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[2], << 0.00, 0.00, 33.23 >>)
						
						objTriRaceBarricades_Group1[3] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1183.18, -835.56, 13.30 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[3], << 0.00, 0.00, 0.00 >>)
						
						objTriRaceBarricades_Group1[4] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1179.36, -836.58, 13.25 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[4], << 0.00, 0.00, -19.48 >>)
						
						objTriRaceBarricades_Group1[5] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1175.65, -838.19, 13.30 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[5], << 0.00, 0.00, -21.20 >>)
						
						objTriRaceBarricades_Group1[6] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1172.39, -839.67, 13.28 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[6], << 0.00, 0.00, -26.36 >>)
						
						objTriRaceBarricades_Group1[6] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1168.99, -841.31, 13.23 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[6], << 0.00, 0.00, -32.09 >>)
						
						objTriRaceBarricades_Group1[7] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1165.80, -843.07, 13.22 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[7], << 0.00, 0.00, -28.65 >>)
						
						objTriRaceBarricades_Group1[8] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1162.99, -844.95, 13.21 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[8], << 0.00, 0.00, -53.86 >>)
						
						
						// 24th set: Left and front barricades to direct run course to the right.
						objTriRaceBarricades_Group1[9] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1113.33, -918.55, 1.53 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[9], << 0.00, 0.00, -60.16 >>)
						
						objTriRaceBarricades_Group1[10] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1112.77, -923.04, 1.64 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[10], << 0.00, 0.00, -91.10 >>)
						
						objTriRaceBarricades_Group1[11] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1114.17, -928.01, 1.63 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[11], << 0.00, 0.00, 60.73 >>)
						
						objTriRaceBarricades_Group1[12] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1117.41, -932.60, 1.59 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[12], << 0.00, 0.00, 45.84 >>)
						
						objTriRaceBarricades_Group1[13] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1121.38, -936.31, 1.51 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[13], << 0.00, 0.00, 21.20 >>)
						
						
						// Update the previous barricade gate so we don't enter this case again in the next frame.
						iPlayerPreviousBarricadeGate = Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_RACE_ROAD_BARRICADES] Created barricades in gate 15 of Triathlon 2.")
					BREAK
					
						
					// Tri 2 Gate: Bike->Run transition gate.
					CASE 19
						// Delete GROUP 2 of barricades.
						REPEAT (iNumTriRaceBarricadesPerGroup) iBarricadeCounter
							IF DOES_ENTITY_EXIST(objTriRaceBarricades_Group2[iBarricadeCounter])
								DELETE_OBJECT(objTriRaceBarricades_Group2[iBarricadeCounter])
							ENDIF
						ENDREPEAT
						
						
						// GROUP 2
						
						// By last intersection, across from the finish line.
						objTriRaceBarricades_Group2[0] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1360.99, -984.68, 7.21 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[0], << 0.00, 0.00, 24.64 >>)
						
						objTriRaceBarricades_Group2[1] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1363.99, -987.39, 7.34 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[1], << 0.00, 0.00, 59.01 >>)
						
						objTriRaceBarricades_Group2[2] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1365.15, -991.50, 7.31 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[2], << 0.00, 0.00, -88.81 >>)
						
						objTriRaceBarricades_Group2[3] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1364.81, -996.02, 7.28 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[3], << 0.00, 0.00, -76.78 >>)
						
						objTriRaceBarricades_Group2[4] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1363.77, -1000.48, 7.23 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[4], << 0.00, 0.00, -73.34 >>)
						
						objTriRaceBarricades_Group2[5] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1362.08, -1004.64, 7.22 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[5], << 0.00, 0.00, -57.87 >>)
						
						objTriRaceBarricades_Group2[6] = CREATE_OBJECT(PROP_BARRIER_WORK06A, << -1360.13, -1008.18, 7.21 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[6], << 0.00, 0.00, -55.00 >>)
						
						
						// Update the previous barricade gate so we don't enter this case again in the next frame.
						iPlayerPreviousBarricadeGate = Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_RACE_ROAD_BARRICADES] Created barricades in gate 19 of Triathlon 2.")
					BREAK
				ENDSWITCH
			BREAK
			
			
			CASE TRIATHLON_RACE_IRONMAN
				SWITCH iCurrentPlayerGate
				
					// Tri 3 Gate: Race start.
					CASE 0
						// GROUP 1
						
						// 1st set: Behind bikes blockade.
						objTriRaceBarricades_Group1[0] = CREATE_OBJECT(Prop_Barrier_Work06A, << -213.07, 4183.79, 42.60 >>)
						SET_ENTITY_HEADING(objTriRaceBarricades_Group1[0], 162.72)


						// 2nd set: Blockade to dirt road by third birdge.
						objTriRaceBarricades_Group1[1] = CREATE_OBJECT(Prop_Barrier_Work06A, << 104.62, 3423.08, 38.40 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[1], << 0.00, 0.00, 22.92 >>)					
						
						
						// 3rd set: Blockade on the left side of first road fork.
						objTriRaceBarricades_Group1[2] = CREATE_OBJECT(Prop_Barrier_Work06A, << 213.53, 3395.94, 37.39 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[2], << 0.00, 0.00, -81.93 >>)
						
						objTriRaceBarricades_Group1[3] = CREATE_OBJECT(Prop_Barrier_Work06A, << 213.91, 3392.51, 37.43 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[3], << 0.00, 0.00, -81.93 >>)
						
						objTriRaceBarricades_Group1[4] = CREATE_OBJECT(Prop_Barrier_Work06A, << 214.55, 3388.98, 37.43 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[4], << 0.00, 0.00, 95.68 >>)
						
						
						// 4th set: Blockade ON first right after overpass.
						objTriRaceBarricades_Group1[5] = CREATE_OBJECT(Prop_Barrier_Work06A, << 206.06, 3259.66, 40.64 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[5], << 0.00, 0.00, -83.65 >>)
						
						objTriRaceBarricades_Group1[6] = CREATE_OBJECT(Prop_Barrier_Work06A, << 205.55, 3255.18, 40.75 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[6], << 0.00, 0.00, -85.94 >>)
						
						
						// 5th set: Blockade on first left after overpass.
						objTriRaceBarricades_Group1[7] = CREATE_OBJECT(Prop_Barrier_Work06A, << 231.66, 3137.83, 41.25 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[7], << 0.00, 0.00, -88.24 >>)
						
						objTriRaceBarricades_Group1[8] = CREATE_OBJECT(Prop_Barrier_Work06A, << 231.26, 3132.59, 41.28 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[8], << 0.00, 0.00, 85.94 >>)
						
						
						// 6th set: Blockade across from first left, on second right after overpass.
						objTriRaceBarricades_Group1[9] = CREATE_OBJECT(Prop_Barrier_Work06A, << 202.56, 3111.83, 41.15 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[9], << 0.00, 0.00, -75.06 >>)
						
						objTriRaceBarricades_Group1[10] = CREATE_OBJECT(Prop_Barrier_Work06A, << 202.98, 3108.23, 41.17 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[10], << 0.00, 0.00, -88.24 >>)
						
						
						// 7th set: Blockade across from first left, on second right after overpass.
						objTriRaceBarricades_Group1[11] = CREATE_OBJECT(Prop_Barrier_Work06A, << 224.14, 2968.36, 41.73 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[11], << 0.00, 0.00, 56.72 >>)
						
						objTriRaceBarricades_Group1[12] = CREATE_OBJECT(Prop_Barrier_Work06A, << 226.86, 2972.91, 41.71 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[12], << 0.00, 0.00, 62.45 >>)
						
						objTriRaceBarricades_Group1[13] = CREATE_OBJECT(Prop_Barrier_Work06A, << 229.18, 2977.87, 41.68 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[13], << 0.00, 0.00, 69.33 >>)
						
						objTriRaceBarricades_Group1[14] = CREATE_OBJECT(Prop_Barrier_Work06A, << 231.56, 2983.72, 41.60 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[14], << 0.00, 0.00, -106.57 >>)
						
						
						// 8th set: Blockade on right by sharp left turn.
						objTriRaceBarricades_Group1[15] = CREATE_OBJECT(Prop_Barrier_Work06A, << -80.66, 2831.72, 51.89 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[15], << 0.00, 0.00, 71.62 >>)
						
						objTriRaceBarricades_Group1[16] = CREATE_OBJECT(Prop_Barrier_Work06A, << -80.53, 2825.88, 52.06 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[16], << 0.00, 0.00, -87.09 >>)
						
						objTriRaceBarricades_Group1[17] = CREATE_OBJECT(Prop_Barrier_Work06A, << -78.46, 2819.61, 52.32 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group1[17], << 0.00, 0.00, -76.20 >>)
						
						
						
						// GROUP 2
						
						// 9th set: Blockade on left side after sharp left turn, before dirt road.
						objTriRaceBarricades_Group2[0] = CREATE_OBJECT(Prop_Barrier_Work06A, << -44.36, 2789.68, 55.32 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[0], << 0.00, 0.00, 0.00 >>)
						
						objTriRaceBarricades_Group2[1] = CREATE_OBJECT(Prop_Barrier_Work06A, << -38.90, 2787.60, 55.51 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[1], << 0.00, 0.00, -30.37 >>)
						
						objTriRaceBarricades_Group2[2] = CREATE_OBJECT(Prop_Barrier_Work06A, << -34.55, 2784.30, 55.75 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[2], << 0.00, 0.00, -38.39 >>)

					
						// 10th set: Left blockade after old dirt road, before right turn into long road.
						objTriRaceBarricades_Group2[3] = CREATE_OBJECT(Prop_Barrier_Work06A, << 280.01, 2640.67, 43.64 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[3], << 0.00, 0.00, 15.47 >>)
						
						objTriRaceBarricades_Group2[4] = CREATE_OBJECT(Prop_Barrier_Work06A, << 286.10, 2643.15, 43.65 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[4], << 0.00, 0.00, 0.00 >>)
						
						
						// 11th set: Front blocakde after old dirt road, before right turn into long road.
						objTriRaceBarricades_Group2[5] = CREATE_OBJECT(Prop_Barrier_Work06A, << 293.96, 2640.35, 43.66 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[5], << 0.00, 0.00, -71.05 >>)
						
						objTriRaceBarricades_Group2[6] = CREATE_OBJECT(Prop_Barrier_Work06A, << 295.83, 2635.74, 43.65 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[6], << 0.00, 0.00, -76.78 >>)

						
						// 12th set: Front blockade before left turn between the two long roads.
						objTriRaceBarricades_Group2[7] = CREATE_OBJECT(Prop_Barrier_Work06A, << 714.05, 2280.47, 49.49 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[7], << 0.00, 0.00, 120.32 >>)
						
						objTriRaceBarricades_Group2[8] = CREATE_OBJECT(Prop_Barrier_Work06A, << 716.39, 2274.91, 49.38 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[8], << 0.00, 0.00, -68.75 >>)
						
						
						// 13th set: In between turning from long road, to uphill second long road.
						objTriRaceBarricades_Group2[9] = CREATE_OBJECT(Prop_Barrier_Work06A, << 724.59, 2205.82, 56.32 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[9], << 0.00, 0.00, 68.18 >>)
						
						objTriRaceBarricades_Group2[10] = CREATE_OBJECT(Prop_Barrier_Work06A, << 719.51, 2199.87, 57.14 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[10], << 0.00, 0.00, 64.74 >>)
						
						objTriRaceBarricades_Group2[11] = CREATE_OBJECT(Prop_Barrier_Work06A, << 715.40, 2194.01, 57.66 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[11], << 0.00, 0.00, 58.44 >>)
						
						objTriRaceBarricades_Group2[12] = CREATE_OBJECT(Prop_Barrier_Work06A, << 709.77, 2188.46, 58.30 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[12], << 0.00, 0.00, 52.14 >>)
						
						
						// 14th set: Near apex of long road uphill, across from farm entrance, to the left.
						objTriRaceBarricades_Group2[13] = CREATE_OBJECT(Prop_Barrier_Work06A, << -82.68, 2002.30, 180.02 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[13], << 0.00, 0.00, 21.20 >>)
						
						
						// 15th set: Second left road in long, uphill road.
						objTriRaceBarricades_Group2[14] = CREATE_OBJECT(Prop_Barrier_Work06A, << 643.92, 2175.31, 63.60 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[14], << 0.00, 0.00, -42.97 >>)
						
						
						// Second right after dirt road.
						objTriRaceBarricades_Group2[15] = CREATE_OBJECT(Prop_Barrier_Work06A, << -182.31, 1911.07, 196.30 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[15], << 0.00, 0.00, 94.72 >>)
						
						objTriRaceBarricades_Group2[16] = CREATE_OBJECT(Prop_Barrier_Work06A, << -183.33, 1901.75, 195.87 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[16], << 0.00, 0.00, 80.21 >>)
						
						
						// Second right after dirt road.
						objTriRaceBarricades_Group2[17] = CREATE_OBJECT(Prop_Barrier_Work06A, << -172.88, 1884.43, 197.20 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[17], << 0.00, 0.00, -61.88 >>)
						
						objTriRaceBarricades_Group2[18] = CREATE_OBJECT(Prop_Barrier_Work06A, << -170.16, 1880.49, 197.21 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group2[18], << 0.00, 0.00, -61.31 >>)
						
						
						
						// GROUP 3
						
						// Left dirt exit after farm area.
						objTriRaceBarricades_Group3[0] = CREATE_OBJECT(Prop_Barrier_Work06A, << -4.54, 1832.29, 205.84 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[0], << 0.00, 0.00, -55.58 >>)
						
						objTriRaceBarricades_Group3[1] = CREATE_OBJECT(Prop_Barrier_Work06A, << -2.66, 1828.43, 206.37 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[1], << 0.00, 0.00, -40.68 >>)
						
						objTriRaceBarricades_Group3[2] = CREATE_OBJECT(Prop_Barrier_Work06A, << -0.68, 1824.21, 206.99 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[2], << 0.00, 0.00, -49.85 >>)
						
						
						// Left exit before valley.
						objTriRaceBarricades_Group3[3] = CREATE_OBJECT(Prop_Barrier_Work06A, << 146.99, 1654.85, 227.99 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[3], << 0.00, 0.00, -57.87 >>)
						
						objTriRaceBarricades_Group3[4] = CREATE_OBJECT(Prop_Barrier_Work06A, << 149.00, 1650.24, 228.06 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[4], << 0.00, 0.00, -63.60 >>)
						
						
						// Left before the theater.
						objTriRaceBarricades_Group3[5] = CREATE_OBJECT(Prop_Barrier_Work06A, << 229.63, 1346.25, 237.73 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[5], << 0.00, 0.00, 84.22 >>)
						
						objTriRaceBarricades_Group3[6] = CREATE_OBJECT(Prop_Barrier_Work06A, << 228.83, 1343.00, 237.63 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[6], << 0.00, 0.00, 83.08 >>)
						
						objTriRaceBarricades_Group3[7] = CREATE_OBJECT(Prop_Barrier_Work06A, << 228.56, 1338.54, 237.45 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[7], <<0.00, 0.00, 89.38 >>)

						
						// Left side by the finish line.
						objTriRaceBarricades_Group3[8] = CREATE_OBJECT(Prop_Barrier_Work06A, << 331.83, 1007.23, 209.50 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[8], << 0.00, 0.00, -90.53 >>)
						
						objTriRaceBarricades_Group3[9] = CREATE_OBJECT(Prop_Barrier_Work06A, << 332.03, 1003.95, 209.57 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[9], << 0.00, 0.00, -93.97 >>)
						
						objTriRaceBarricades_Group3[10] = CREATE_OBJECT(Prop_Barrier_Work06A, << 332.22, 1000.55, 209.52 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[10], << 0.00, 0.00, 78.50 >>)
						
						
						// Right side by the finish line.
						objTriRaceBarricades_Group3[11] = CREATE_OBJECT(Prop_Barrier_Work06A, << 302.02, 1006.53, 209.52 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[11], << 0.00, 0.00, -88.24 >>)
						
						objTriRaceBarricades_Group3[12] = CREATE_OBJECT(Prop_Barrier_Work06A, << 302.27, 1003.18, 209.56 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[12], << 0.00, 0.00, -89.957 >>)
						
						objTriRaceBarricades_Group3[13] = CREATE_OBJECT(Prop_Barrier_Work06A, << 301.90, 999.64, 209.52 >>)
						SET_ENTITY_ROTATION(objTriRaceBarricades_Group3[13], << 0.00, 0.00, -93.97 >>)
							
							
						// Update the previous barricade gate so we don't enter this case again in the next frame.
						iPlayerPreviousBarricadeGate = Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_RACE_ROAD_BARRICADES] Created barricades in gate 0 of Triathlon 3.")
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Update which road node speed zones are active whenever the player's current
///    gate changes.
///    
///    These are used to prevent vehicles spawning outside race roads from entering 
///    the race roads. We do this by setting the speed of the intersection between a 
///    race and non-race road to ZERO, which stops the vehicle before entering the race.
///    
///    As of 5/17/2012, JMart has upgraded the number of these zones that can be active
///    at the same time from 16 to 32.
///    
///    For Ironman, I've created 4 arrays, each storing 8 speed zone indeces.  Depending 
///    on the player's current gate, the indeces in these arrays change, so only the closest 
///    to the player are active.
///    
///    For Tri 1 and Tri 2, a single array is enough to account for all race road intersections.
///    
///    Directions like "right" and "left" are from the perspective of the player during the race.
///    
///    You can view the road speed zones in the world through RAG:
///    		Vehicle AI and Nodes->Road Speed Zones->Enable Debug Draw
PROC UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE(TRI_RACE_STRUCT& Race)
	IF iPlayerPreviousSpeedZoneGate <> Race.Racer[0].iGateCur
		INT iCurrentPlayerGate = Race.Racer[0].iGateCur
		INT iNodeCounter
	
		SWITCH eCurrentTriRace
			CASE TRIATHLON_RACE_ALAMO_SEA
				IF NOT bAreNonIronSpeedZonesSet
					iTriSpeedDampenerRoadNodes[0] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 2184.52, 3888.70, 30.83 >>, 470, 0.0, FALSE)	// Water during swim leg.
					iTriSpeedDampenerRoadNodes[1] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 2184.52, 3888.70, 30.83 >>, 37.0, 0.0, FALSE)	// Dirt road behind bikes.
					iTriSpeedDampenerRoadNodes[2] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 2008.49, 3815.51, 31.30 >>, 11.0, 0.0, FALSE)	// First intersection after bikes.
					iTriSpeedDampenerRoadNodes[3] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1928.68, 3769.48, 31.34 >>, 10.0, 0.0, FALSE)	// Second intersection after bikes.
					iTriSpeedDampenerRoadNodes[4] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1955.29, 3722.61, 31.37 >>, 13.0, 0.0, FALSE)	// T-intersection after left turn from second intersection.
					iTriSpeedDampenerRoadNodes[5] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1813.46, 3639.46, 33.32 >>, 14.0, 0.0, FALSE) // Right race turn back to town.
					iTriSpeedDampenerRoadNodes[6] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1786.53, 3685.61, 33.26 >>, 10.0, 0.0, FALSE) // First intersection after right race turn back to town.
					iTriSpeedDampenerRoadNodes[7] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1715.90, 3647.45, 34.05 >>, 13.0, 0.0, FALSE) // Right race turn after intersection.
					iTriSpeedDampenerRoadNodes[8] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1675.58, 3714.10, 33.06 >>, 11.0, 0.0, FALSE) // First t-intersection after returning to town.
					iTriSpeedDampenerRoadNodes[9] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1603.86, 3672.74, 33.51 >>, 11.0, 0.0, FALSE) // Intersection before right turn to long road.
					iTriSpeedDampenerRoadNodes[10] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1575.37, 3715.84, 33.51 >>, 11.0, 0.0, FALSE) // Narrow left turn before entering long road.
					iTriSpeedDampenerRoadNodes[11] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1547.26, 3754.18, 33.52 >>, 13.0, 0.0, FALSE)	// T-intersection to long road.
					iTriSpeedDampenerRoadNodes[12] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1664.17, 3857.17, 33.89 >>, 11.0, 0.0, FALSE)	// First block in long road after left race turn.
					iTriSpeedDampenerRoadNodes[13] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1743.48, 3920.48, 33.94 >>, 12.0, 0.0, FALSE)	// Second block in long road.
					iTriSpeedDampenerRoadNodes[14] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1856.24, 3947.12, 32.07 >>, 14.0, 0.0, FALSE)	// Third block in long road.
					iTriSpeedDampenerRoadNodes[15] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1967.46, 3885.17, 31.34 >>, 16.0, 0.0, FALSE)	// Bike->run transition intersection.
					iTriSpeedDampenerRoadNodes[16] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1950.00, 3868.20, 31.26 >>, 12.0, 0.0, FALSE)	// Left entrance to liquor store parking lot.
					iTriSpeedDampenerRoadNodes[17] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1888.61, 3838.33, 31.44 >>, 14.0, 0.0, FALSE)	// First intersection in run leg.
					iTriSpeedDampenerRoadNodes[18] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1868.84, 3895.22, 32.09 >>, 14.0, 0.0, FALSE)	// Second intersection in run leg.
					iTriSpeedDampenerRoadNodes[19] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 1778.97, 3859.56, 33.42 >>, 13.0, 0.0, FALSE)	// Final intersection before finish line.
					
					bAreNonIronSpeedZonesSet = TRUE
					CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created 0-based road speed zones in Triathlon 1.")
				ENDIF
			BREAK
			
			CASE TRIATHLON_RACE_VESPUCCI
				IF NOT bAreNonIronSpeedZonesSet
					iTriSpeedDampenerRoadNodes[0] 	= ADD_ROAD_NODE_SPEED_ZONE(<< -1240.68, -1926.18, 0.03 >>, 37.0, 0.0, FALSE)	// Left canal in water by start area.
					iTriSpeedDampenerRoadNodes[1]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1327.15, -1963.89, 1.07 >>, 65.0, 0.0, FALSE)	// Three-way water intersection on swim leg.
					iTriSpeedDampenerRoadNodes[2]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1251.58, -1708.55, 3.47 >>, 4.0, 0.0, FALSE)		// Behind bikes.
					iTriSpeedDampenerRoadNodes[3]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1324.31, -1601.92, 3.47 >>, 7.0, 0.0, FALSE)		// Road nodes across from basketball court towards water.
					iTriSpeedDampenerRoadNodes[4]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1351.33, -1334.59, 3.48 >>, 11.0, 0.0, FALSE)	// Intersection between beach road and beach stores.
					iTriSpeedDampenerRoadNodes[5]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1323.55, -1335.86, 3.65 >>, 9.0, 0.0, FALSE)		// Right turn after entering beach store area.
					iTriSpeedDampenerRoadNodes[6]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1336.74, -1298.39, 3.84 >>, 16.0, 0.0, FALSE)	// First right turn after board stores road.
					iTriSpeedDampenerRoadNodes[7]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1360.95, -1205.19, 3.45 >>, 12.0, 0.0, FALSE)	// Second right turn after board stores road.
					iTriSpeedDampenerRoadNodes[8]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1360.87, -1118.91, 3.26 >>, 17.0, 0.0, FALSE)	// Race right turn to streets.
					iTriSpeedDampenerRoadNodes[9]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1308.61, -1090.68, 5.98 >>, 12.0, 0.0, FALSE)	// Intersection after entering streets.
					iTriSpeedDampenerRoadNodes[10]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1295.97, -1192.29, 3.92 >>, 13.0, 0.0, FALSE)	// Intersection before uphill road towards long road.
					iTriSpeedDampenerRoadNodes[11]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1218.39, -1158.88, 6.74 >>, 19.0, 0.0, FALSE)	// Intersection at start of long road.
					iTriSpeedDampenerRoadNodes[12]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1222.77, -1115.26, 6.95 >>, 9.0, 0.0, FALSE)		// First right in long road.
					iTriSpeedDampenerRoadNodes[13]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1250.04, -1062.88, 7.45 >>, 15.0, 0.0, FALSE)	// First left in long road.
					iTriSpeedDampenerRoadNodes[14]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1291.22, -906.45, 10.40 >>, 24.0, 0.0, FALSE)	// End of long road, before bike->run transition.
					iTriSpeedDampenerRoadNodes[15]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1175.67, -843.70, 13.34 >>, 23.0, 0.0, FALSE)	// Bike->run transition intersection.
					iTriSpeedDampenerRoadNodes[16]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1119.27, -928.70, 1.68 >>, 16.0, 0.0, FALSE)		// First intersection after bike->run transition.
					iTriSpeedDampenerRoadNodes[17]	= ADD_ROAD_NODE_SPEED_ZONE(<< -1357.12, -995.60, 7.30 >>, 18.0, 0.0, FALSE)		// Last intersection before finish line.
					
					bAreNonIronSpeedZonesSet = TRUE
					CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created 0-based road speed zones in Triathlon 2.")
				ENDIF
			BREAK
			
			CASE TRIATHLON_RACE_IRONMAN
				SWITCH iCurrentPlayerGate
					// Gate: First gate, on the water.
					CASE 0
						// First set of roads: From bridge behind bike start area, to right before first overpass.
						iTriIronSpeedDampenerRoadNodes_Group1[0] 	= ADD_ROAD_NODE_SPEED_ZONE(<< -202.08, 4215.12, 43.73 >>, 12.0, 0.0, FALSE)		// Intersection in front of bridge, behind the area where bikes spawn.
						iTriIronSpeedDampenerRoadNodes_Group1[1] 	= ADD_ROAD_NODE_SPEED_ZONE(<< -230.27, 3902.66, 36.35 >>, 18.0, 0.0, FALSE)		// Right dirt-road intersection after bike area.
						iTriIronSpeedDampenerRoadNodes_Group1[2] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 18.86, 3603.60, 38.92 >>, 9.0, 0.0, FALSE)		// First left dirt-road to trailer park, before sharp right turn.
						iTriIronSpeedDampenerRoadNodes_Group1[3] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 78.44, 3587.27, 38.80 >>, 9.0, 0.0, FALSE)		// Second left dirt-road to trailer park, before sharp right turn.
						iTriIronSpeedDampenerRoadNodes_Group1[4] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 102.67, 3426.61, 38.34 >>, 7.0, 0.0, FALSE)		// To right dirt road by third mini-bridge.
						iTriIronSpeedDampenerRoadNodes_Group1[5] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 209.87, 3392.95, 37.43 >>, 12.0, 0.0, FALSE)		// Left on fork after third mini-bridge.
						iTriIronSpeedDampenerRoadNodes_Group1[6] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 242.46, 3340.55, 38.58 >>, 7.0, 0.0, FALSE)		// Opposite road from overpass.
						iTriIronSpeedDampenerRoadNodes_Group1[7] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 246.30, 3327.84, 38.69 >>, 10.0, 0.0, FALSE)		// Dirt road, left side by overpass.
						
						// Second set of roads: From right before first overpass, to intersection before long road by mountain.
						iTriIronSpeedDampenerRoadNodes_Group2[0] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 209.00, 3257.40, 40.69 >>, 8.00, 0.0, FALSE)		// First right dirt road after first overpass.
						iTriIronSpeedDampenerRoadNodes_Group2[1] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 224.43, 3134.18, 41.24 >>, 9.00, 0.0, FALSE)		// First left dirt road after first overpass.
						iTriIronSpeedDampenerRoadNodes_Group2[2]	= ADD_ROAD_NODE_SPEED_ZONE(<< 209.88, 3110.79, 41.12 >>, 10.0, 0.0, FALSE)		// Second right dirt road after first overpass.
						iTriIronSpeedDampenerRoadNodes_Group2[3]	= ADD_ROAD_NODE_SPEED_ZONE(<< 221.83, 2978.32, 41.57 >>, 13.0, 0.0, FALSE)		// By first right turn after overpass.
						iTriIronSpeedDampenerRoadNodes_Group2[4]	= ADD_ROAD_NODE_SPEED_ZONE(<< 169.24, 2909.25, 45.74 >>, 14.0, 0.0, FALSE)		// First dirt road after right turn after overpass.
						iTriIronSpeedDampenerRoadNodes_Group2[5]	= ADD_ROAD_NODE_SPEED_ZONE(<< -74.33, 2824.69, 52.42 >>, 14.0, 0.0, FALSE)		// By U-turn after first right after overpass.
						iTriIronSpeedDampenerRoadNodes_Group2[6]	= ADD_ROAD_NODE_SPEED_ZONE(<< -29.47, 2795.60, 55.71 >>, 18.0, 0.0, FALSE)		// First right dirt road after u-turn.
						iTriIronSpeedDampenerRoadNodes_Group2[7]	= ADD_ROAD_NODE_SPEED_ZONE(<< 189.50, 2628.79, 46.94 >>, 14.0, 0.0, FALSE)		// Second right dirt road after u-turn.
						
						// Third set of roads: From u-turn to mountain ascent to first set of dirt roads during ascent.
						iTriIronSpeedDampenerRoadNodes_Group3[0]	= ADD_ROAD_NODE_SPEED_ZONE(<< 285.46, 2636.48, 43.66 >>, 12.0, 0.0, FALSE)		// Intersection to long road by mountain.
						iTriIronSpeedDampenerRoadNodes_Group3[1]	= ADD_ROAD_NODE_SPEED_ZONE(<< 311.18, 2578.59, 43.13 >>, 16.0, 0.0, FALSE)		// First set of dirt roads across long road.
						iTriIronSpeedDampenerRoadNodes_Group3[2]	= ADD_ROAD_NODE_SPEED_ZONE(<< 417.47, 2459.65, 45.04 >>, 40.0, 0.0, FALSE)		// Second set of dirt roads across long road.
						iTriIronSpeedDampenerRoadNodes_Group3[3]	= ADD_ROAD_NODE_SPEED_ZONE(<< 672.48, 2297.02, 50.13 >>, 18.0, 0.0, FALSE)		// Left dirt road before long road U-turn.
						iTriIronSpeedDampenerRoadNodes_Group3[4]	= ADD_ROAD_NODE_SPEED_ZONE(<< 719.38, 2227.78, 54.52 >>, 44.0, 0.0, FALSE)		// U-turn.
						iTriIronSpeedDampenerRoadNodes_Group3[5]	= ADD_ROAD_NODE_SPEED_ZONE(<< 632.87, 2182.69, 64.70 >>, 16.0, 0.0, FALSE)		// First dirt road after U-turn.
						iTriIronSpeedDampenerRoadNodes_Group3[6]	= ADD_ROAD_NODE_SPEED_ZONE(<< 222.72, 2103.20, 117.56 >>, 22.0, 0.0, FALSE)		// Second dirt road after U-turn.
						iTriIronSpeedDampenerRoadNodes_Group3[7]	= ADD_ROAD_NODE_SPEED_ZONE(<< 57.63, 2074.84, 151.87 >>, 30.0, 0.0, FALSE)		// Third set of dirt roads, across from each other.
						
						// Fourth set of roads: From first set of dirt roads during ascent to intersection after theater at the top of mountain.
						iTriIronSpeedDampenerRoadNodes_Group4[0]	= ADD_ROAD_NODE_SPEED_ZONE(<< -80.22, 1995.61, 180.18 >>, 28.0, 0.0, FALSE)		// Right dirt road after first set of dirt roads, and path to ranch.
						iTriIronSpeedDampenerRoadNodes_Group4[1]	= ADD_ROAD_NODE_SPEED_ZONE(<< -160.90, 1900.78, 197.21 >>, 28.0, 0.0, FALSE)	// Right dirt turn, and right road turn right after it, across farm.
						iTriIronSpeedDampenerRoadNodes_Group4[2]	= ADD_ROAD_NODE_SPEED_ZONE(<< -75.78, 1852.73, 198.95 >>, 10.0, 0.0, FALSE)		// Left dirt turn to farm.
						iTriIronSpeedDampenerRoadNodes_Group4[3]	= ADD_ROAD_NODE_SPEED_ZONE(<< -13.57, 1823.64, 206.26 >>, 15.0, 0.0, FALSE)		// Left dirt road after farm.
						iTriIronSpeedDampenerRoadNodes_Group4[4]	= ADD_ROAD_NODE_SPEED_ZONE(<< 140.56, 1649.13, 227.99 >>, 11.0, 0.0, FALSE)		// Left road after farm, at top of mountain.
						iTriIronSpeedDampenerRoadNodes_Group4[5]	= ADD_ROAD_NODE_SPEED_ZONE(<< 239.81, 1343.35, 237.50 >>, 13.0, 0.0, FALSE)		// Right turn before theater.
						iTriIronSpeedDampenerRoadNodes_Group4[6]	= ADD_ROAD_NODE_SPEED_ZONE(<< 260.10, 1181.67, 224.82 >>, 23.0, 0.0, FALSE)		// Entrance to theater.
						iTriIronSpeedDampenerRoadNodes_Group4[7]	= ADD_ROAD_NODE_SPEED_ZONE(<< 315.99, 1003.40, 209.56 >>, 16.0, 0.0, FALSE)		// Intersection after theater.
						
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 0 using all groups in Triathlon 3.")
					BREAK
					
					// Gate: Intersection to long road by mountain.  Race flows to the right here, after making a tough u-turn a bit after the first overpass. (Start of third set of roads).
					CASE 17
						// Release road node indeces from bridge behind bike start area, to right before first overpass (first set under CASE 0).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group1[iNodeCounter])
						ENDREPEAT
					
						// Fifth set of roads: From after intersection after theater at top of mountain, to right before town descent.
						iTriIronSpeedDampenerRoadNodes_Group1[0]	= ADD_ROAD_NODE_SPEED_ZONE(<< 283.15, 831.88, 191.10 >>, 12.0, 0.0, FALSE)	// First little intersection after top of mountain intersection.
						iTriIronSpeedDampenerRoadNodes_Group1[1] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 292.98, 759.14, 183.18 >>, 12.0, 0.0, FALSE)	// Right turn after little intersection.
						iTriIronSpeedDampenerRoadNodes_Group1[2] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 272.28, 613.01, 153.58 >>, 11.0, 0.0, FALSE)	// Left turn in race after right turn.
						iTriIronSpeedDampenerRoadNodes_Group1[3] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 668.79, 340.23, 109.78 >>, 14.0, 0.0, FALSE)	// U-turn after first mountain descent, towards town.
						iTriIronSpeedDampenerRoadNodes_Group1[4] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 621.84, 309.11, 105.94 >>, 10.0, 0.0, FALSE) 	// Left entrance to gas station after u-turn.
						iTriIronSpeedDampenerRoadNodes_Group1[5] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 571.93, 247.74, 102.14 >>, 20.0, 0.0, FALSE)	// Intersection turn after gas station.
						iTriIronSpeedDampenerRoadNodes_Group1[6] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 422.43, 296.28, 102.06 >>, 19.0, 0.0, FALSE)	// Intersection before orienting towards town descent.
						iTriIronSpeedDampenerRoadNodes_Group1[7] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 361.30, 137.65, 102.09 >>, 20.0, 0.0, FALSE)	// Intersection before town descent.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 17 using group 1 in Triathlon 3.")
					BREAK
					
					// Gate: During mountain ascent, right dirt road after first set of dirt roads, and across to first path to ranch, to the left. (Start of fourth set of roads)
					CASE 21
						// Release road node indeces from right before first overpass, to intersection before long road by mountain (second set under CASE 0)
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group2[iNodeCounter])
						ENDREPEAT
					
						// Sixth set of roads: From start of town descent to intersection before first bridge around industrial area.
						iTriIronSpeedDampenerRoadNodes_Group2[0] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 281.11, -86.82, 69.13 >>, 28.00, 0.0, FALSE)	// First intersection during town descent.
						iTriIronSpeedDampenerRoadNodes_Group2[1] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 230.99, -219.45, 53.03 >>, 28.00, 0.0, FALSE)	// Second intersection during town descent.
						iTriIronSpeedDampenerRoadNodes_Group2[2] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 187.22, -341.36, 43.06 >>, 22.00, 0.0, FALSE)	// Intersection at end of town descent.
						iTriIronSpeedDampenerRoadNodes_Group2[3] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 103.84, -573.50, 42.70 >>, 21.00, 0.0, FALSE) // Second intersection after town descent, after crossing above highway.
						iTriIronSpeedDampenerRoadNodes_Group2[4] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 243.16, -627.07, 39.94 >>, 22.00, 0.0, FALSE)	// Intersection after turning left from previous intersection.
						iTriIronSpeedDampenerRoadNodes_Group2[5] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 171.92, -819.01, 30.19 >>, 23.00, 0.0, FALSE) // Intersection after crossing below the bridge.
						iTriIronSpeedDampenerRoadNodes_Group2[6] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 288.97, -855.26, 28.21 >>, 28.00, 0.0, FALSE)	// Intersection after making left at previous intersection.
						iTriIronSpeedDampenerRoadNodes_Group2[7] 	= ADD_ROAD_NODE_SPEED_ZONE(<< 404.98, -853.74, 28.34 >>, 24.00, 0.0, FALSE)	// Intersection in front of bridge.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 21 using group 2 in Triathlon 3.")
					BREAK
					
					// Gate: Shortly after start of mountain descent, before sharp left turn, right after a little intersection (Start of fifth set of roads under CASE 17)
					CASE 27
						// Release road node indeces from u-turn to mountain ascent to first set of dirt roads during ascent. (Third set under CASE 0).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group3[iNodeCounter])
						ENDREPEAT
						
						// Seventh set of roads: From intersection after crossing industrial bridge to before right turn that's before water tower to the left.
						iTriIronSpeedDampenerRoadNodes_Group3[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< 925.77, -840.64, 42.71 >>, 27.00, 0.0, FALSE)	// Intersection after crossing industrial bridge.
						iTriIronSpeedDampenerRoadNodes_Group3[1]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 988.19, -994.31, 40.96 >>, 12.00, 0.0, FALSE)	// Intersection after right turn at previous intersection, towards narrow bridge.
						iTriIronSpeedDampenerRoadNodes_Group3[2]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 1155.63, -949.62, 47.34 >>, 17.00, 0.0, FALSE)	// Intersection after narrow bridge.
						iTriIronSpeedDampenerRoadNodes_Group3[3]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 1240.27, -1289.72, 34.10 >>, 10.00, 0.0, FALSE)	// Right turn to warehouse entrance.
						iTriIronSpeedDampenerRoadNodes_Group3[4]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 1245.71, -1426.14, 34.04 >>, 31.00, 0.0, FALSE)	// First big left after narrow bridge left turn.
						iTriIronSpeedDampenerRoadNodes_Group3[5]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 1299.87, -1559.39, 48.90 >>, 22.00, 0.0, FALSE) // Entrance to small residential roads across from each other.
						iTriIronSpeedDampenerRoadNodes_Group3[6]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 1333.49, -1612.43, 51.52 >>, 26.00, 0.0, FALSE) // First real intersection after left turn from narrow bridge.
						iTriIronSpeedDampenerRoadNodes_Group3[7]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 1387.92, -1693.63, 61.74 >>, 13.00, 0.0, FALSE)	// First narrow left turn after intersection.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 27 using group 3 in Triathlon 3.")
					BREAK
					
					// Gate: First intersection after player begins town descent. (Start of sixth set of roads under CASE 21)
					CASE 32
						// Release road node indeces from first set of dirt roads during ascent to intersection after theater at the top of mountain.  (Fourth set under CASE 0).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group4[iNodeCounter])
						ENDREPEAT
						
						// Eight set of roads: From intersection after crossing industrial bridge to epic intersection before crossing another industrial bridge over a canal.
						iTriIronSpeedDampenerRoadNodes_Group4[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< 1405.90, -1759.06, 64.87 >>, 13.00, 0.0, FALSE)	// Right turn before water tower.
						iTriIronSpeedDampenerRoadNodes_Group4[1]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 1356.95, -2008.41, 50.77 >>, 10.00, 0.0, FALSE)	// Left turn before major intersection.
						iTriIronSpeedDampenerRoadNodes_Group4[2]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 1244.08, -2053.19, 43.36 >>, 28.00, 0.0, FALSE) // Major intersection after water tower.
						iTriIronSpeedDampenerRoadNodes_Group4[3]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 1065.21, -2089.11, 32.17 >>, 16.00, 0.0, FALSE)	// Major left turn after intersection.
						iTriIronSpeedDampenerRoadNodes_Group4[4]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 1015.58, -2081.86, 30.11 >>, 20.00, 0.0, FALSE)	// Entrances to industrial complexes.
						iTriIronSpeedDampenerRoadNodes_Group4[5]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 934.02, -2079.15, 29.81 >>, 20.00, 0.0, FALSE)	// First intersection after major intersection.
						iTriIronSpeedDampenerRoadNodes_Group4[6]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 871.07, -2076.44, 29.44 >>, 19.00, 0.0, FALSE)	// Entrance ton industrial complex on right, alley on left.
						iTriIronSpeedDampenerRoadNodes_Group4[7]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< 774.16, -2064.31, 28.37 >>, 26.00, 0.0, FALSE)	// Giant intersection before bridge.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 32 using group 4 in Triathlon 3.")
					BREAK
					
					// Gate: At intersection right after first industrial bridge. (Start of seventh set of roads under CASE 26)
					CASE 40
						// Release road node indeces from after intersection after theater at top of mountain, to right before town descent. (fifth set under CASE 17).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group1[iNodeCounter])
						ENDREPEAT
						
						// Ninth set of roads: From second major industrial bridge (big dome theater can be seen in the distance), to just after left turn from metro stop.
						iTriIronSpeedDampenerRoadNodes_Group1[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< 485.08, -2057.32, 24.07 >>, 28.00, 0.0, FALSE)	// Right turn from second industrial bridge.
						iTriIronSpeedDampenerRoadNodes_Group1[1]	=	ADD_ROAD_NODE_SPEED_ZONE(<< 441.52, -2010.76, 22.32 >>, 23.00, 0.0, FALSE)	// After right turn from bridge, right to entrance of complex, left to alley roar.
						iTriIronSpeedDampenerRoadNodes_Group1[2]	=	ADD_ROAD_NODE_SPEED_ZONE(<< 398.45, -1957.69, 23.22 >>, 15.00, 0.0, FALSE)	// After right turn from bridge, first proper right turn.
						iTriIronSpeedDampenerRoadNodes_Group1[3]	=	ADD_ROAD_NODE_SPEED_ZONE(<< 361.43, -1931.09, 23.61 >>, 23.00, 0.0, FALSE)	// First intersection after right turn from bridge.
						iTriIronSpeedDampenerRoadNodes_Group1[4]	=	ADD_ROAD_NODE_SPEED_ZONE(<< 273.97, -1857.93, 25.86 >>, 30.00, 0.0, FALSE)	// Big intersection with train tracks running across it.
						iTriIronSpeedDampenerRoadNodes_Group1[5]	=	ADD_ROAD_NODE_SPEED_ZONE(<< 170.51, -1771.53, 28.09 >>, 21.00, 0.0, FALSE)	// Intersection after big intersection, with train tracks runnin in parallel.
						iTriIronSpeedDampenerRoadNodes_Group1[6]	=	ADD_ROAD_NODE_SPEED_ZONE(<< 53.04, -1671.93, 28.30 >>, 37.00, 0.0, FALSE)	// Intersection after metro stop.
						iTriIronSpeedDampenerRoadNodes_Group1[7]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -16.91, -1713.40, 28.16 >>, 13.00, 0.0, FALSE)	// Left entrance to parking lot after metro turn.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 40 using group 1 in Triathlon 3.")
					BREAK
					
					// Gate: By the water tower a while after crossing the first industrial bridge. (Start of eight set of roads under CASE 32)
					CASE 47
						// Release road node indeces from start of town descent to intersection before first bridge around industrial area. (sixth set under CASE 21).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group2[iNodeCounter])
						ENDREPEAT
						
						// Tenth set of roads: Shortly after left turn from metro stop to recycling facility after left turn away from dome theater.
						iTriIronSpeedDampenerRoadNodes_Group2[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -51.24, -1709.98, 28.33 >>, 11.00, 0.0, FALSE)	// Right u-turn after metro stop left turn.
						iTriIronSpeedDampenerRoadNodes_Group2[1]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< -72.38, -1726.48, 28.34 >>, 25.00, 0.0, FALSE)	// Small left road by gas station, and gas station entrance.
						iTriIronSpeedDampenerRoadNodes_Group2[2]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< -124.44, -1738.59, 29.15 >>, 33.00, 0.0, FALSE)	// Multi-lane intersection in front of dome theater.
						iTriIronSpeedDampenerRoadNodes_Group2[3]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< -245.71, -1832.28, 28.26 >>, 13.00, 0.0, FALSE)	// Left turn in front of dome theater entrance.
						iTriIronSpeedDampenerRoadNodes_Group2[4]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< -390.27, -1831.57, 20.46 >>, 22.00, 0.0, FALSE)	// Front lane before right turn, by dome theater.
						iTriIronSpeedDampenerRoadNodes_Group2[5]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< -408.28, -1765.96, 19.72 >>, 20.00, 0.0, FALSE)	// Left turn after turning away from dome.
						iTriIronSpeedDampenerRoadNodes_Group2[6]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< -400.42, -1719.66, 18.17 >>, 14.00, 0.0, FALSE)	// Entrance to junkyard on the left.
						iTriIronSpeedDampenerRoadNodes_Group2[7]	= 	ADD_ROAD_NODE_SPEED_ZONE(<< -351.67, -1617.67, 18.69 >>, 12.00, 0.0, FALSE)	// Ramp to recycling facility on the left.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 47 using group 2 in Triathlon 3.")
					BREAK
					
					// Gate: Right turn from second industrial bridge. (Start of ninth set of roads under CASE 40)
					CASE 50
						// Release road node indeces from intersection after crossing industrial bridge to before right turn that's before water tower to the left. (seventh set under CASE 27).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group3[iNodeCounter])
						ENDREPEAT
						
						// Eleventh set of roads: After first intersection after turning right away from the dome theater to left turn on third intersection after left turn on third intersection after dome (ugh...)
						iTriIronSpeedDampenerRoadNodes_Group3[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -351.67, -1617.67, 18.69 >>, 12.00, 0.0, FALSE)	// Main entrance to recycling facility, and to gas station on the left.
						iTriIronSpeedDampenerRoadNodes_Group3[1]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -275.18, -1428.71, 30.35 >>, 23.00, 0.0, FALSE)	// First intersection after dome turn.
						iTriIronSpeedDampenerRoadNodes_Group3[2]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -272.27, -1142.25, 22.09 >>, 25.00, 0.0, FALSE)	// Second intersection after dome turn.
						iTriIronSpeedDampenerRoadNodes_Group3[3]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -184.63, -894.89, 28.34 >>, 22.00, 0.0, FALSE)	// Third intersection after dome turn.
						iTriIronSpeedDampenerRoadNodes_Group3[4]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -285.27, -849.23, 30.75 >>, 13.00, 0.0, FALSE)	// Right road after left turn on third intersection.
						iTriIronSpeedDampenerRoadNodes_Group3[5]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -500.02, -836.83, 29.49 >>, 25.00, 0.0, FALSE)	// First intersection after left turn on third intersection after dome turn.
						iTriIronSpeedDampenerRoadNodes_Group3[6]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -637.93, -835.77, 23.96 >>, 25.00, 0.0, FALSE)	// Second intersection after left turn on third intersection after dome turn.
						iTriIronSpeedDampenerRoadNodes_Group3[7]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -745.51, -835.46, 21.68 >>, 21.00, 0.0, FALSE)	// Third intersection after left turn on third intersection after dome turn.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 50 using group 3 in Triathlon 3.")
					BREAK
					
					// Gate: In front of dome theater. (Start of tenth set of roads under CASE 47)
					CASE 53
						// Release road node indeces from intersection after crossing industrial bridge to epic intersection before crossing another industrial bridge over a canal. (eight set under CASE 32).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group4[iNodeCounter])
						ENDREPEAT
						
						// Twelfth set of roads: At left road race turn into a curved road leaving to a road that, after two lefts, leads to the Venice canals; to before the third intersection in the canals road.
						iTriIronSpeedDampenerRoadNodes_Group4[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -351.67, -1617.67, 18.69 >>, 18.00, 0.0, FALSE)	// Left entrance to gas station during curve.
						iTriIronSpeedDampenerRoadNodes_Group4[1]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -758.69, -962.37, 15.37 >>, 18.00, 0.0, FALSE)	// Merging fork after gas station, before curve ends.
						iTriIronSpeedDampenerRoadNodes_Group4[2]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -823.59, -1014.37, 12.36 >>, 17.00, 0.0, FALSE)	// T-intersection at end of curve.  Race flows to the right.
						iTriIronSpeedDampenerRoadNodes_Group4[3]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -770.83, -1116.07, 9.70 >>, 22.00, 0.0, FALSE)	// First intersection after right turn on t-intersection.
						iTriIronSpeedDampenerRoadNodes_Group4[4]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -860.82, -1180.68, 4.25 >>, 18.00, 0.0, FALSE)	// Left entrance to Hilton-like hotel.
						iTriIronSpeedDampenerRoadNodes_Group4[5]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -951.85, -1222.91, 4.30 >>, 19.00, 0.0, FALSE)	// Left turn to long, narrow canals road/bridges.
						iTriIronSpeedDampenerRoadNodes_Group4[6]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1005.71, -1120.32, 1.17 >>, 10.00, 0.0, FALSE)	// First intersection in canals road.
						iTriIronSpeedDampenerRoadNodes_Group4[7]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1058.15, -1029.12, 1.18 >>, 11.00, 0.0, FALSE)	// Second intersection in canals road.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 53 using group 4 in Triathlon 3.")
					BREAK
					
					// Gate: After first intersection after turning right away from the dome theater. (Start of eleventh set of roads under CASE 50)
					CASE 55
						// Release road node indeces from second major industrial bridge (big dome theater can be seen in the distance), to just after left turn from metro stop  (ninth set under CASE 40).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group1[iNodeCounter])
						ENDREPEAT
						
						// Thirteenth set of roads: From third intersection in canals to before fork in road parallel to beach, before beachwalk.
						iTriIronSpeedDampenerRoadNodes_Group1[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1118.13, -928.10, 1.67 >>, 13.00, 0.0, FALSE)	// Third intersection in canals road.
						iTriIronSpeedDampenerRoadNodes_Group1[1]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1177.15, -844.47, 13.30 >>, 15.00, 0.0, FALSE)	// Intersection after exiting canals road, race flows to left from here.
						iTriIronSpeedDampenerRoadNodes_Group1[2]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1288.86, -910.85, 10.40 >>, 21.00, 0.0, FALSE)	// Intersection from left turn after exiting canals, race flows left from here.
						iTriIronSpeedDampenerRoadNodes_Group1[3]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1253.74, -1064.55, 7.46 >>, 9.00, 0.0, FALSE)	// First right turn after intersection.
						iTriIronSpeedDampenerRoadNodes_Group1[4]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1223.99, -1115.16, 6.96 >>, 10.00, 0.0, FALSE)	// First left turn after intersection.
						iTriIronSpeedDampenerRoadNodes_Group1[5]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1219.36, -1156.96, 6.72 >>, 15.00, 0.0, FALSE)	// Second right turn after intersection.
						iTriIronSpeedDampenerRoadNodes_Group1[6]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1202.47, -1212.51, 6.71 >>, 11.00, 0.0, FALSE)	// Second left turn after intersection.
						iTriIronSpeedDampenerRoadNodes_Group1[7]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1204.98, -1248.69, 5.99 >>, 11.00, 0.0, FALSE)	// Third right turn after intersection.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 55 using group 1 in Triathlon 3.")
					BREAK
					
					// Gate: At left road race turn into a curved road leaving to a road that, after two lefts, leads to the Venice canals (Start of twelfth set of roads under CASE 53).
					CASE 59
						// Release road node indeces from shortly after left turn from metro stop to recycling facility after left turn away from dome theater (tenth set under CASE 47).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group2[iNodeCounter])
						ENDREPEAT
						
						// Fourteenth set of roads: From fork before left turn towards beach, to before third intersection after exiting highway.
						iTriIronSpeedDampenerRoadNodes_Group2[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1118.13, -928.10, 1.67 >>, 13.00, 0.0, FALSE)	// Left turn at the fork, away from beach.
						iTriIronSpeedDampenerRoadNodes_Group2[1]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1157.70, -1341.64, 4.09 >>, 14.00, 0.0, FALSE)	// Road race turn to face beach.
						iTriIronSpeedDampenerRoadNodes_Group2[2]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1224.28, -1372.30, 3.14 >>, 15.00, 0.0, FALSE)	// Last intersection before beach.
						iTriIronSpeedDampenerRoadNodes_Group2[3]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1294.65, -1399.80, 3.56 >>, 13.00, 0.0, FALSE)	// Road into beach area.
						iTriIronSpeedDampenerRoadNodes_Group2[4]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1340.54, -1333.20, 3.71 >>, 21.00, 0.0, FALSE)	// Road to beach.
						iTriIronSpeedDampenerRoadNodes_Group2[5]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -2048.68, -404.17, 10.16 >>, 32.00, 0.0, FALSE)	// Intersection after exiting beach road on highway.
						iTriIronSpeedDampenerRoadNodes_Group2[6]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1709.74, -537.02, 36.44 >>, 16.00, 0.0, FALSE)	// Intersection on road after exiting highway.  Race flows to right from here.
						iTriIronSpeedDampenerRoadNodes_Group2[7]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1649.81, -574.30, 32.69 >>, 22.00, 0.0, FALSE)	// Second intersection after exiting highway.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 59 using group 2 in Triathlon 3.")
					BREAK
					
					// Gate: Right before uphill road to exit canals, after third intersection in canals. (Start of thirteenth set of roads under CASE 55)
					CASE 63
						// Release road node indeces from first intersection after turning right away from the dome theater to left turn on third intersection after left turn on third intersection after dome (eleventh set under CASE 50).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group3[iNodeCounter])
						ENDREPEAT
						
						// Fifteenth set of roads: From intersection where race flows left opposite beach to t-intersection after driving away from beach.
						iTriIronSpeedDampenerRoadNodes_Group3[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1535.24, -673.29, 27.91 >>, 20.00, 0.0, FALSE)	// Left turn opposite beach.
						iTriIronSpeedDampenerRoadNodes_Group3[1]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1379.03, -560.17, 29.23 >>, 22.00, 0.0, FALSE)	// First intersection while driving opposite beach.
						iTriIronSpeedDampenerRoadNodes_Group3[2]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1300.02, -503.12, 32.16 >>, 25.00, 0.0, FALSE)	// Second intersection while driving opposite beach.
						iTriIronSpeedDampenerRoadNodes_Group3[3]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1242.43, -444.37, 32.60 >>, 20.00, 0.0, FALSE)	// Left turn after second intersection.
						iTriIronSpeedDampenerRoadNodes_Group3[4]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1184.77, -411.61, 33.51 >>, 12.00, 0.0, FALSE)	// Left turn to narrow road.
						iTriIronSpeedDampenerRoadNodes_Group3[5]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1093.27, -402.18, 35.62 >>, 11.00, 0.0, FALSE)	// Right turn to narrow road.
						iTriIronSpeedDampenerRoadNodes_Group3[6]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1007.64, -349.69, 36.90 >>, 23.00, 0.0, FALSE)	// Third intersection while driving opposite beach.
						iTriIronSpeedDampenerRoadNodes_Group3[7]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1053.09, -267.57, 36.69 >>, 15.00, 0.0, FALSE)	// T-intersection.  Race flows right from here.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 63 using group 3 in Triathlon 3.")
					BREAK
					
					// Gate: Fork before left turn towards beach (Start of fourteenth set of roads under CASE 59)
					CASE 66
						// Release road node indeces from left road race turn into a curved road leaving to a road that, after two lefts, leads to the Venice canals; to before the third intersection in the canals road. (twelfth set under CASE 53).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group4[iNodeCounter])
						ENDREPEAT
						
						// Sixteenth set of roads: From after t-intersection after driving opposite beach, to intersection before uphill road, close to end of bike leg.
						iTriIronSpeedDampenerRoadNodes_Group4[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1007.53, -222.78, 36.76 >>, 9.00, 0.0, FALSE)	// First left of first intersection.
						iTriIronSpeedDampenerRoadNodes_Group4[1]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -989.60, -210.20, 36.84 >>, 10.00, 0.0, FALSE)	// Second left of first intersection.
						iTriIronSpeedDampenerRoadNodes_Group4[2]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -978.94, -220.53, 36.86 >>, 22.00, 0.0, FALSE)	// Right of first intersection.
						iTriIronSpeedDampenerRoadNodes_Group4[3]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -941.54, -203.08, 36.92 >>, 8.00, 0.0, FALSE)	// Merge lane after first intersection.
						iTriIronSpeedDampenerRoadNodes_Group4[4]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -859.58, -158.47, 36.94 >>, 15.00, 0.0, FALSE)	// Second intersection.
						iTriIronSpeedDampenerRoadNodes_Group4[5]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -767.29, -110.03, 36.88 >>, 15.00, 0.0, FALSE)	// Third intersection.
						iTriIronSpeedDampenerRoadNodes_Group4[6]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -688.16, -69.20, 36.71 >>, 14.00, 0.0, FALSE)	// Fourth intersection.  Race flows left from here.
						iTriIronSpeedDampenerRoadNodes_Group4[7]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -707.95, -27.05, 36.88 >>, 18.00, 0.0, FALSE)	// Intersection before uphill road.
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 66 using group 4 in Triathlon 3.")
					BREAK
					
					// Gate: At intersection where race roads turns left opposite beach (Start of fifteenth set of roads under CASE 63)
					CASE 76
						// Release road node indeces from third intersection in canals to before fork in road parallel to beach, before beachwalk (thirteenth set under CASE 55).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group1[iNodeCounter])
						ENDREPEAT
						
						// Seventeenth set of roads: From first intersection during uphill road to last intersection before end of bike leg.
						iTriIronSpeedDampenerRoadNodes_Group1[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -747.08, 101.02, 54.72 >>, 13.00, 0.0, FALSE)	// First intersection in uphil road.
						iTriIronSpeedDampenerRoadNodes_Group1[1]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -750.53, 124.43, 56.00 >>, 9.00, 0.0, FALSE)	// Left turn after first intersection.
						iTriIronSpeedDampenerRoadNodes_Group1[2]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -759.68, 217.31, 74.73 >>, 20.00, 0.0, FALSE)	// Intersection at end of uphill road.
						iTriIronSpeedDampenerRoadNodes_Group1[3]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -786.39, 223.57, 75.02 >>, 13.00, 0.0, FALSE)	// First right turn after intersection.
						iTriIronSpeedDampenerRoadNodes_Group1[4]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -856.61, 211.81, 72.81 >>, 11.00, 0.0, FALSE)	// First left turn after intersection.
						iTriIronSpeedDampenerRoadNodes_Group1[5]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -920.61, 253.85, 69.40 >>, 16.00, 0.0, FALSE)	// Second right turn after intersection.
						iTriIronSpeedDampenerRoadNodes_Group1[6]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -978.06, 254.87, 67.63 >>, 12.00, 0.0, FALSE)	// Second left turn after intersection.
						iTriIronSpeedDampenerRoadNodes_Group1[7]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1078.55, 261.68, 62.95 >>, 25.00, 0.0, FALSE)	// First big intersection after uphill road.		
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 76 using group 1 in Triathlon 3.")
					BREAK
					
					// Gate: T-intersection towards road before left turn towards uphill road.  (Start of sixteenth set of roads under CASE 66)
					CASE 79
						// Release road node indeces from fork before left turn towards beach, to before third intersection after exiting highway. (fourteenth set under CASE 59).
						REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
							REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group2[iNodeCounter])
						ENDREPEAT
						
						// Eighteenth set of roads: From after last intersection before end of bike leg, to ...
						iTriIronSpeedDampenerRoadNodes_Group2[0]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1138.99, 268.05, 65.26 >>, 14.00, 0.0, FALSE)	// First right turn after intersection.
						iTriIronSpeedDampenerRoadNodes_Group2[1]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1234.10, 234.02, 64.45 >>, 21.00, 0.0, FALSE)	// Right turn to end of bike leg.
						iTriIronSpeedDampenerRoadNodes_Group2[2]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1388.48, 200.07, 57.71 >>, 24.00, 0.0, FALSE)	// First right turn in right leg.
						iTriIronSpeedDampenerRoadNodes_Group2[3]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1441.30, 271.45, 60.08 >>, 16.00, 0.0, FALSE)	// T-intersection after turn.  Race flows right here.
						iTriIronSpeedDampenerRoadNodes_Group2[4]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1492.58, 227.93, 59.60 >>, 10.00, 0.0, FALSE)	// University road.
						iTriIronSpeedDampenerRoadNodes_Group2[5]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1836.11, 121.58, 76.14 >>, 18.71, 0.0, FALSE)	// End of university road.
						iTriIronSpeedDampenerRoadNodes_Group2[6]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -1800.50, 91.66, 71.08 >>, 24.00, 0.0, FALSE)	// Start of final winding road.
						iTriIronSpeedDampenerRoadNodes_Group2[7]	=	ADD_ROAD_NODE_SPEED_ZONE(<< -2306.00, 442.53, 173.47 >>, 10.00, 0.0, FALSE)	// End of race (Finally!  Oh god this took so long...)
					
						iPlayerPreviousSpeedZoneGate 	= Race.Racer[0].iGateCur
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_ACTIVE_ROAD_NODES_SPEED_ZONES_ON_NEW_PLAYER_CURRENT_GATE] Created new 0-based road speed zones in gate 79 using group 2 in Triathlon 3.")
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes any traffic (peds, vehicles and props) from interfering in the race.
PROC REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS()
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Procedure started.")
	
	// Prevent dispatching vehicles from spawning in the road.
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, FALSE)
	SET_CREATE_RANDOM_COPS(FALSE)
	
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Dispatch services disabled for race.")
	
	
	// Lower the amount of ambient peds spawned.
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Decreasing ped density for race.")
	
	
	SWITCH eCurrentTriRace
		CASE TRIATHLON_RACE_ALAMO_SEA
		
			// -----------------------------
			//	TODO: PED TRAFFIC
			// -----------------------------
			
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Race path, creation and scenario areas turned OFF in Triathlon 1.")
			
			
			
			// -----------------------------
			//	VEHICLE TRAFFIC
			// -----------------------------
			
			// Clear swim and rest of race area of vehicles.
			CLEAR_AREA_OF_VEHICLES(<< 2110.01, 4268.56, 8.53 >>, 460.00)
			CLEAR_AREA_OF_VEHICLES(<< 1847.2754, 3827.2170, 31.6549 >>, 330.00)
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Cleared race area from vehicles in Triathlon 1.")
			
			// Zero road set: Water in swim leg.
			SET_ROADS_IN_ANGLED_AREA(<< 2289.135, 4321.770, 40.505 >>, 	<< 2253.114, 3878.230, 18.505 >>, 265.000, FALSE, FALSE)	// From start of race to end of swim leg.
			
			// First road set: From behind bikes location by dirt road, to before first race left turn.
			SET_ROADS_IN_ANGLED_AREA(<< 2191.210, 3896.372, 23.924 >>, 	<< 2190.456, 3875.386, 39.924 >>, 70.000, FALSE, FALSE)		// Behind bikes location.
			SET_ROADS_IN_ANGLED_AREA(<< 2025.350, 3831.476, 25.926 >>, 	<< 2030.913, 3821.987, 41.926 >>, 280.000, FALSE, FALSE)	// From bikes location to first race left turn.
			
			
			// Second road set: From first left race turn to start of long roaf towards bike->run transition.
			SET_ROADS_IN_ANGLED_AREA(<< 1932.811, 3739.470, 24.371 >>, 	<< 1950.988, 3749.985, 40.371 >>, 70.000, FALSE, FALSE)		// From first left race turn to first racr right turn.
			SET_ROADS_IN_ANGLED_AREA(<< 1877.811, 3688.361, 25.498 >>, 	<< 1888.060, 3670.032, 41.498 >>, 190.000, FALSE, FALSE)	// From first right race turn to right turn back to town.
			SET_ROADS_IN_ANGLED_AREA(<< 1807.241, 3669.025, 25.904 >>, 	<< 1789.286, 3658.134, 41.904 >>, 70.000, FALSE, FALSE)		// From right back to town to first left.
			SET_ROADS_IN_ANGLED_AREA(<< 1743.463, 3674.442, 26.263 >>, 	<< 1754.072, 3656.319, 42.263 >>, 110.000, FALSE, FALSE)	// From first left after returning to town, to following right.
			SET_ROADS_IN_ANGLED_AREA(<< 1703.885, 3686.683, 26.358 >>, 	<< 1685.745, 3676.104, 42.358 >>, 110.000, FALSE, FALSE)	// From right race turn after returning to town, to first t-intersection.
			SET_ROADS_IN_ANGLED_AREA(<< 1635.022, 3703.902, 26.221 >>, 	<< 1645.492, 3685.698, 42.221 >>, 110.000, FALSE, FALSE)	// From first t-intersection to start of right turn towards lake view.
			SET_ROADS_IN_ANGLED_AREA(<< 1583.338, 3720.986, 26.221 >>, 	<< 1565.558, 3709.811, 42.221 >>, 130.000, FALSE, FALSE)	// From facing lake view, to right turn to long road.
		
		
			// Third road set: From start of long road to end of long road/ bike->run transition gate.
			SET_ROADS_IN_ANGLED_AREA(<< 1617.290, 3786.476, 26.221 >>, 	<< 1592.070, 3818.802, 42.221 >>, 190.000, FALSE, FALSE)	// From start of long road to end of first block.
			SET_ROADS_IN_ANGLED_AREA(<< 1715.433, 3885.008, 26.221 >>, 	<< 1696.223, 3909.338, 42.221 >>, 130.000, FALSE, FALSE)	// From end of first block to end of second block in long road.
			SET_ROADS_IN_ANGLED_AREA(<< 1801.187, 3925.098, 26.221 >>, 	<< 1794.713, 3955.414, 42.221 >>, 130.000, FALSE, FALSE)	// From end of second block to end of third block in long road.
			SET_ROADS_IN_ANGLED_AREA(<< 1903.961, 3912.521, 24.221 >>, 	<< 1923.415, 3959.665, 40.221 >>, 170.000, FALSE, FALSE)	// From end of third block to bike->run transition gate.
			
			
			// Fourth road set: From start of bike->run transition to finish line.
			SET_ROADS_IN_ANGLED_AREA(<< 1921.498, 3874.776, 24.221 >>, 	<< 1936.966, 3847.910, 40.221 >>, 110.000, FALSE, FALSE)	// From bike->run transition to first right on run leg.
			SET_ROADS_IN_ANGLED_AREA(<< 1891.288, 3871.255, 24.221 >>, 	<< 1861.130, 3864.079, 40.221 >>, 80.000, FALSE, FALSE)		// From first run leg turn to first left turn.
			SET_ROADS_IN_ANGLED_AREA(<< 1816.318, 3896.067, 25.221 >>, 	<< 1828.275, 3867.466, 41.221 >>, 120.000, FALSE, FALSE)	// From first left turn to right turn towards finish line.
			SET_ROADS_IN_ANGLED_AREA(<< 1771.863, 3892.823, 26.221 >>, 	<< 1753.740, 3882.215, 42.221 >>, 90.000, FALSE, FALSE)		// Road towards finish line.
			
			// Blocking that boat from spawning
			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<2290.9141, 4067.8401, 30.5179>>)
			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<2220.3108, 4436.5332, 30.1107>>)
			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<2049.6865, 4352.8394, 30.1496>>)
			
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Race road nodes turned OFF in Triathlon 1.")
		BREAK
		
		CASE TRIATHLON_RACE_VESPUCCI
		
			// Remove objects in the way between the swim and bike transition gate.
			SET_BUILDING_STATE(BUILDINGNAME_IPL_VB_PROPS, BUILDINGSTATE_DESTROYED)
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Cleared beach area from props in Triathlon 2.")
			
			
			
			// -----------------------------
			//	PED TRAFFIC
			// -----------------------------
			
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)	// TODO: Keep for now until we can prove we're successfully blocking peds from race roads.
			
			// First road set: From swim->bike transition to major right turn towards beach shops.
			SET_PED_PATHS_IN_AREA(<< -1329.54, -1783.29, -20.99 >>, << -1278.47, -1666.56, 23.43 >>, FALSE)	// Beach from swim-to-bike.
			
			SET_PED_NON_CREATION_AREA(<< -1336.26, 	-1786.84, -2.00 >>, << -1277.37, -1663.03, 9.00 >>)
			
			SET_PED_PATHS_IN_AREA(<< -1347.77, -1705.95, -23.03 >>, << -1247.42, -1572.45, 23.22 >>, FALSE) // Bike start to first curve by aerobics area.
			SET_PED_NON_CREATION_AREA(<< -1347.77, -1705.95, -23.03 >>, << -1247.42, -1572.45, 23.22 >>)
			
			SET_PED_PATHS_IN_AREA(<< -1360.70, -1589.52, -3.87 >>, << -1340.51, -1500.11, 6.09 >>, FALSE) 	// Bike start, after first curve to before metal art.
			SET_PED_NON_CREATION_AREA(<< -1360.70, -1589.52, -3.87 >>, << -1340.51, -1500.11, 6.09 >>)
			
			SET_PED_PATHS_IN_AREA(<< -1416.93, -1500.80, -5.90 >>, << -1349.71, -1385.46, 8.37 >>, FALSE) 	// From before metal art to before major right turn.
			SET_PED_NON_CREATION_AREA(<< -1416.93, -1500.80, -5.90 >>, << -1349.71, -1385.46, 8.37 >>)
			
			SET_PED_PATHS_IN_AREA(<< -1391.68,  -1409.03, -3.95 >>, << -1353.17, -1300.24, 7.60 >>, FALSE) 	// Major right turn towards beach shops.
			SET_PED_NON_CREATION_AREA(<< -1391.68,  -1409.03, -3.95 >>, << -1353.17, -1300.24, 7.60 >>)

//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1213.3916, -1802.0354, 3.1399>>)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1301.0900, -1751.3831, 1.1172>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1264.7830, -1688.6780, 3.3740>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1305.6350, -1630.6801, 3.4400>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1328.6890, -1593.9310, 3.3736>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1343.4990, -1562.8051, 3.3357>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1368.2760, -1515.9590, 3.3244>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1400.4550, -1401.9922, 2.5794>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1376.3430, -1362.7770, 2.4835>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1328.7706, -1317.4766, 3.7066>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1360.4821, -1206.1150, 3.3541>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1357.5861, -1115.6140, 3.1734>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1374.6362, -1460.4220, 3.8966>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1352.2245, -1260.5876, 4.3456>>, TRI_VENICE_SCENARIO_MULT)
//			ADD_TRIATHLON_SCENARIO_BLOCKING_AREA(<<-1364.7084, -1160.0864, 4.0897>>, TRI_VENICE_SCENARIO_MULT)
			
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Race path, creation and scenario areas turned OFF in Triathlon 2.")
			
			// -----------------------------
			//	VEHICLE TRAFFIC
			// -----------------------------
		
			// Clear race areas of vehicles.
			CLEAR_AREA_OF_VEHICLES(<< -1365.2754, -1451.6089, 4.3495 >>, 675.00)
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Cleared race area from vehicles in Triathlon 2.")
			
			// Clear parked jetski near the last cleared gate.
			CLEAR_AREA_OF_VEHICLES(<< -1317.59, -1763.79, 0.92 >>, 6.00)
			
			// First road set: Water road nodes in swim leg.
			SET_ROADS_IN_ANGLED_AREA(<< -1278.97, -1932.01, -10.00 >>, 	<< 1191.09, -2014.33, 22.47 >>, 1200.00, TRUE, FALSE) 			// Right canal.
			SET_ROADS_IN_ANGLED_AREA(<< -1703.19, -2046.52, -37.28 >>, 	<< -1279.62, -1753.35, 21.26 >>, 500.00, TRUE, FALSE) 			// Beach.
			
			// First road set: From bike area to end of beach stores.
			SET_ROADS_IN_ANGLED_AREA(<< -1275.526, -1669.598, -2.474 >>, << -1280.479, -1672.984, 10.474 >>, 125.000, FALSE, FALSE)		// From bike area to start of basketball court.
			SET_ROADS_IN_ANGLED_AREA(<< -1328.572, -1588.628, -2.453 >>, << -1333.682, -1591.772, 10.453 >>, 75.000, FALSE, FALSE)		// From start of basketball court to athletic park.
			SET_ROADS_IN_ANGLED_AREA(<< -1346.266, -1533.706, -2.449 >>, << -1360.745, -1540.515, 10.449 >>, 105.000, FALSE, FALSE) 	// From middle of atheltic park to before metal art thing.
			SET_ROADS_IN_ANGLED_AREA(<< -1376.607, -1433.510, -2.034 >>, << -1400.276, -1444.268, 10.034 >>, 125.000, FALSE, FALSE) 	// From metal art thing to major right turn.
			SET_ROADS_IN_ANGLED_AREA(<< -1368.123, -1379.432, -3.892 >>, << -1390.870, -1317.475, 9.892 >>, 135.000, FALSE, FALSE)		// From major right turn to beah stores area.
			SET_ROADS_IN_ANGLED_AREA(<< -1331.716, -1266.083, -2.900 >>, << -1356.625, -1273.536, 10.900 >>, 145.000, FALSE, FALSE)		// From start of beach stores area through start of second block.
			SET_ROADS_IN_ANGLED_AREA(<< -1346.508, -1157.917, -2.378 >>, << -1372.508, -1157.917, 10.378 >>, 115.000, FALSE, FALSE)		// From start of second block to right-race turn to streets.
			
			
			// Second road set: From end of beach stores to bike->run transition.
			SET_ROADS_IN_ANGLED_AREA(<< -1320.172, -1107.892, -4.977 >>, << -1329.571, -1090.238, 16.977 >>, 65.000, FALSE, FALSE)		// From end of beach road to end of uphill road to streets.
			SET_ROADS_IN_ANGLED_AREA(<< -1312.138, -1138.175, -2.019 >>, << -1292.138, -1138.329, 12.019 >>, 95.000, FALSE, FALSE)		// From right turn after uphill to first left turn.
			SET_ROADS_IN_ANGLED_AREA(<< -1253.094, -1186.270, -1.682 >>, << -1265.201, -1163.260, 13.682 >>, 135.000, FALSE, FALSE)		// From first left turn to left turn at t-intersection towards long road.
			SET_ROADS_IN_ANGLED_AREA(<< -1238.610, -1030.409, -8.791 >>, << -1273.366, -1039.790, 18.791 >>, 295.000, FALSE, FALSE)		// From start of long road to end of long road.
			SET_ROADS_IN_ANGLED_AREA(<< -1214.013, -907.341, -8.548 >>, << -1246.902, -862.017, 18.548 >>, 160.000, FALSE, FALSE)		// From right turn from long road to bike->run transition.
			
			
			// Third road set: From bike->run transition to finish line.
			SET_ROADS_IN_ANGLED_AREA(<< -1155.810, -891.950, -10.801 >>, << -1133.376, -878.807, 20.801 >>, 120.000, FALSE, FALSE)		// From bike->run transition to first right turn from downhill road.
			SET_ROADS_IN_ANGLED_AREA(<< -1173.853, -943.301, -3.183 >>, << -1160.572, -965.653, 9.183 >>, 130.000, FALSE, FALSE)		// From right turn after downhill to before bridge.
			SET_ROADS_IN_ANGLED_AREA(<< -1286.419, -978.159, -4.489 >>, << -1286.617, -994.158, 14.489 >>, 130.000, FALSE, FALSE)		// From before bridge to right turn towards finish line.
			SET_ROADS_IN_ANGLED_AREA(<< -1346.018, -1046.348, -1.711 >>, << -1323.297, -1033.709, 15.711 >>, 140.000, FALSE, FALSE)		// From right turn to finish line.
			
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Race road nodes turned OFF in Triathlon 2.")
			
			// ----------------------------
			// Stationary Vehicles
			// ----------------------------
			VECTOR vMin, vMax 
			vMin = << -1302.005, -1779.373, -2.155 >>
			vMax = << -1331.091, -1749.1610, 2.789 >>
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMin, vMax, FALSE)
		BREAK
		
		
		CASE TRIATHLON_RACE_IRONMAN
		
			// -----------------------------
			//	TODO: PED TRAFFIC
			// -----------------------------
			
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Race path, creation and scenario areas turned OFF in Triathlon 1.")
			
			CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 675.00)
		
		
		
			// -----------------------------
			//	VEHICLE TRAFFIC
			// -----------------------------
			
			// First road set: From bridge behind bike start area, to right before first overpass.
			SET_ROADS_IN_ANGLED_AREA(<< -230.567, 4148.788, 33.628 >>, 	<< -219.182, 4144.995, 49.628 >>, 145.00, FALSE, FALSE)		// Where bikes are located.
			SET_ROADS_IN_ANGLED_AREA(<< -242.724, 4028.156, 28.249 >>, 	<< -222.463, 4036.729, 44.249 >>, 95.00, FALSE, FALSE)		// Zig-zag turns after bike area.
			SET_ROADS_IN_ANGLED_AREA(<< -236.160, 3937.613, 28.477 >>, 	<< -214.342, 3934.785, 44.477 >>, 125.00, FALSE, FALSE)		// Road before first mini-bridge.
			SET_ROADS_IN_ANGLED_AREA(<< -218.068, 3772.185, 32.976 >>, 	<< -187.264, 3780.850, 48.976 >>, 225.000, FALSE, FALSE)	// From first mini-bridge to second mini-bridge.
			SET_ROADS_IN_ANGLED_AREA(<< -149.866, 3628.335, 38.828 >>, 	<< -130.144, 3653.535, 53.828 >>, 225.000, FALSE, FALSE)	// From second mini-bridge to before slight left.
			SET_ROADS_IN_ANGLED_AREA(<< 7.534, 3586.556, 32.396 >>,		<< 11.175, 3618.348, 48.396 >>, 225.000, FALSE, FALSE)		// From slight-left to before sharp-right turn.
			SET_ROADS_IN_ANGLED_AREA(<< 83.410, 3519.563, 34.793 >>,	<< 118.236, 3516.078, 44.793 >>, 205.000, FALSE, FALSE)		// From sharp-right turn before third mini-bridge.
			SET_ROADS_IN_ANGLED_AREA(<< 161.356, 3384.402, 35.287 >>,	<< 176.253, 3416.074, 45.287 >>, 205.000, FALSE, FALSE)		// From third mini-bridge to fork before first overpass.
			
			
			// Second road set: From right before first overpass to intersection to long road by mountain.
			SET_ROADS_IN_ANGLED_AREA(<< 211.200, 3305.038, 35.644 >>,	<< 245.022, 3296.038, 45.644 >>, 205.000, FALSE, FALSE)		// From before first overpass to after first-right dirt road.
			SET_ROADS_IN_ANGLED_AREA(<< 201.909, 3073.113, 37.313 >>,	<< 236.800, 3075.870, 47.313 >>, 295.000, FALSE, FALSE)		// From after first overpass to first race road right turn.
			SET_ROADS_IN_ANGLED_AREA(<< 172.832, 2936.860, 40.329 >>,	<< 198.827, 2913.424, 50.329 >>, 155.000, FALSE, FALSE)		// First half after right turn after first overpass.
			SET_ROADS_IN_ANGLED_AREA(<< 24.765, 2856.754, 48.433 >>,	<< 35.331, 2823.387, 62.433 >>, 195.000, FALSE, FALSE)		// Second half after right turn after first overpass.
			SET_ROADS_IN_ANGLED_AREA(<< 4.779, 2750.103, 51.233 >>,		<< 31.367, 2798.250, 65.233 >>, 235.000, FALSE, FALSE)		// Around U-turn.
			SET_ROADS_IN_ANGLED_AREA(<< 145.456, 2635.881, 35.443 >>,	<< 201.325, 2712.716, 59.443 >>, 235.000, FALSE, FALSE)		// After U-turn, before right turn to long road by mountain.
			
			
			// Third road set: From start of road by mountain to U-turn.
			SET_ROADS_IN_ANGLED_AREA(<< 283.407, 2611.802, 39.679 >>,	<< 302.296, 2618.374, 49.679 >>, 75.000, FALSE, FALSE)		// From intersection to start of long road.
			SET_ROADS_IN_ANGLED_AREA(<< 353.296, 2510.228, 39.798 >>,	<< 371.948, 2526.875, 49.798 >>, 185.000, FALSE, FALSE)		// From start of long road to second set of dirt roads.
			SET_ROADS_IN_ANGLED_AREA(<< 542.405, 2339.161, 43.571 >>,	<< 568.678, 2381.702, 48.571 >>, 425.000, FALSE, FALSE)		// From second set of dirt roads to U-turn towards mountain.
			SET_ROADS_IN_ANGLED_AREA(<< 705.415, 2263.484, 44.566 >>,	<< 716.676, 2296.623, 56.566 >>, 295.000, FALSE, FALSE)		// U-turn.
			SET_ROADS_IN_ANGLED_AREA(<< 695.232, 2232.163, 49.846 >>,	<< 745.230, 2232.558, 59.846 >>, 105.000, FALSE, FALSE)		// From start of U-turn to end of U-turn towards mountain.
			
			
			// Fourth road set: From start of ascending mountain road to top of mountain.
			SET_ROADS_IN_ANGLED_AREA(<< 613.376, 2192.395, 47.678 >>,	<< 614.517, 2172.428, 87.678 >>, 185.000, FALSE, FALSE)		// From after u-turn to first part of mountain.
			SET_ROADS_IN_ANGLED_AREA(<< 430.020, 2148.145, 69.481 >>,	<< 444.714, 2121.990, 109.481 >>, 195.000, FALSE, FALSE)	// From first part of mountain to second part.
			SET_ROADS_IN_ANGLED_AREA(<< 219.651, 2115.604, 85.485 >>,	<< 217.790, 2085.662, 149.485 >>, 285.000, FALSE, FALSE)	// From second part of mountain to third part.
			SET_ROADS_IN_ANGLED_AREA(<< -31.857, 2038.572, 137.936 >>,	<< -16.549, 2012.771, 203.936 >>, 295.000, FALSE, FALSE)	// From third part of mountain to fourth part.
			SET_ROADS_IN_ANGLED_AREA(<< -42.146, 1737.544, 175.64 >>,	<< 23.141, 1813.290, 241.640 >>, 475.000, FALSE, FALSE)		// From fourth part of mountain to top.
			
			
			// Fifth road set: From top of mountain to sharp left turn during mountain descent.
			SET_ROADS_IN_ANGLED_AREA(<< 133.180, 1570.556, 219.501 >>,	<< 158.180, 1570.551, 241.501 >>, 175.000, FALSE, FALSE)	// From top of mountain to start of first left curve.
			SET_ROADS_IN_ANGLED_AREA(<< 172.870, 1441.773, 228.915 >>,	<< 193.180, 1456.351, 250.915 >>, 175.000, FALSE, FALSE)	// From start of first left curve to end of curve.
			SET_ROADS_IN_ANGLED_AREA(<< 234.385, 1239.455, 211.588 >>,	<< 268.710, 1246.295, 251.588 >>, 355.000, FALSE, FALSE)	// From end of curve to first part of descent by theater.
			SET_ROADS_IN_ANGLED_AREA(<< 286.787, 1073.197, 194.623 >>,	<< 326.009, 1081.046, 234.623 >>, 175.000, FALSE, FALSE)	// From theater to first intersection in descent.
			SET_ROADS_IN_ANGLED_AREA(<< 288.456, 909.196, 217.008 >>,	<< 323.109, 904.281, 189.008 >>, 185.000, FALSE, FALSE)		// From first intersection in descent second intersection.
			SET_ROADS_IN_ANGLED_AREA(<< 284.596, 762.277, 170.424 >>,	<< 299.393, 764.733, 198.424 >>, 185.000, FALSE, FALSE)		// From second intersection descent to right curve.
			SET_ROADS_IN_ANGLED_AREA(<< 265.143, 676.300, 145.153 >>,	<< 309.172, 667.003, 185.153 >>, 145.000, FALSE, FALSE)		// From right curve after second intersection to sharp left turn.
			
			
			// Sixth road set: From sharp left turn on mountain descent to u-turn towards town at end of mountain descent.
			SET_ROADS_IN_ANGLED_AREA(<< 302.641, 577.480, 149.325 >>,	<< 313.087, 588.245, 159.325 >>, 95.000, FALSE, FALSE)		// From sharp left turn to start of right curve.
			SET_ROADS_IN_ANGLED_AREA(<< 325.258, 523.143, 148.179 >>,	<< 340.223, 522.120, 158.179 >>, 95.000, FALSE, FALSE)		// From start of right curve after sharp left turn to start of left curve.
			SET_ROADS_IN_ANGLED_AREA(<< 336.271, 456.256, 142.987 >>,	<< 349.631, 463.076, 152.987 >>, 55.000, FALSE, FALSE)		// From start of left turn to end of left turn.
			SET_ROADS_IN_ANGLED_AREA(<< 395.685, 422.968, 137.985 >>,	<< 398.965, 447.752, 147.985 >>, 95.000, FALSE, FALSE)		// From end of left turn to start of right turn.
			SET_ROADS_IN_ANGLED_AREA(<< 447.861, 372.691, 138.028 >>,	<< 501.875, 383.059, 138.028 >>, 115.000, FALSE, FALSE)		// From start of right turn to end of right turn.
			SET_ROADS_IN_ANGLED_AREA(<< 592.660, 322.263, 117.328 >>,	<< 583.714, 376.530, 135.328 >>, 165.000, FALSE, FALSE)		// From end of right turn to u-turn towards town.
			
			
			// Seventh road: From u-turn towads town at the end of the mountain descent towards to end of bridge crossing.
			SET_ROADS_IN_ANGLED_AREA(<< 597.491, 315.620, 98.659 >>,	<< 629.928, 265.144, 114.659 >>, 165.000, FALSE, FALSE)		// U-turn towards town to right turn.
			SET_ROADS_IN_ANGLED_AREA(<< 502.540, 276.640, 98.108 >>,	<< 496.197, 259.794, 108.108 >>, 195.000, FALSE, FALSE)		// From right turn into town to left turn facing epic descent ahead.
			SET_ROADS_IN_ANGLED_AREA(<< 375.999, 212.344, 93.103 >>,	<< 398.405, 203.744, 113.103 >>, 205.00, FALSE, FALSE)		// From left turn facing epic descent, to start of epic descent.
			SET_ROADS_IN_ANGLED_AREA(<< 314.004, 42.642, 67.914 >>,		<< 336.455, 34.157, 109.914 >>, 295.000, FALSE, FALSE)		// From start of epic descent, to start of second descent.
			SET_ROADS_IN_ANGLED_AREA(<< 239.999, -144.154, 42.440 >>,	<< 272.028, -155.560, 84.440 >>, 205.000, FALSE, FALSE)		// From start of second descent, to start of final descent.
			SET_ROADS_IN_ANGLED_AREA(<< 189.844, -282.718, 26.418 >>,	<< 221.345, -295.514, 68.418 >>, 205.000, FALSE, FALSE)		// From start of final descent, to highway cross-bridge road.
			
			
			// Eight road set: From end of crossing bridge to end of bridge after main crossing around industrial area.
			SET_ROADS_IN_ANGLED_AREA(<< 119.513, -476.605, 37.141 >>,	<< 151.697, -487.569, 49.141 >>, 265.000, FALSE, FALSE)		// From highway cross-bridge road to left turn.
			SET_ROADS_IN_ANGLED_AREA(<< 177.935, -617.074, 37.079 >>,	<< 189.995, -585.285, 49.079 >>, 165.000, FALSE, FALSE)		// From left turn from highway to right turn.
			SET_ROADS_IN_ANGLED_AREA(<< 187.797, -721.593, 25.341 >>,	<< 219.790, -733.100, 43.341 >>, 245.000, FALSE, FALSE)		// From right turn under bridge to left turn towards another bridge.
			SET_ROADS_IN_ANGLED_AREA(<< 223.003, -856.936, 21.201 >>,	<< 234.487, -824.934, 39.201 >>, 165.000, FALSE, FALSE)		// From left turn after crossing under bridge to first intersection.
			SET_ROADS_IN_ANGLED_AREA(<< 650.029, -877.933, 23.751 >>,	<< 650.731, -823.937, 47.751 >>, 690.000, FALSE, FALSE)		// From first intersection after left turn all the way to narrow right turn after bridge.
			SET_ROADS_IN_ANGLED_AREA(<< 944.824, -934.767, 32.724 >>,	<< 995.498, -916.107, 52.724 >>, 180.000, FALSE, FALSE)		// From right turn after bridge, to left turn to narrower bridge.
			SET_ROADS_IN_ANGLED_AREA(<< 1070.454, -981.505, 38.962 >>,	<< 1063.625, -958.497, 52.962 >>, 230.000, FALSE, FALSE)	// From start of narrow bridge to end of bridge.
			
			SET_ROADS_IN_ANGLED_AREA(<< 1145.897, -1098.933, 20.561 >>,	<< 1227.198, -1077.810, 80.561 >>, 1000.000, FALSE, FALSE)	// From end of narrow bridge to just after crossing below highway bridge.
			SET_ROADS_IN_ANGLED_AREA(<< 1145.897, -1098.933, 20.561 >>,	<< 1227.198, -1077.810, 80.561 >>, 1000.000, FALSE, FALSE)	// From right turn after end of narrow bridge to first full intersection ahead.
			
			SET_ROADS_IN_ANGLED_AREA(<< 1324.901, -1776.794, 45.925 >>,	<< 1456.276, -1750.400, 85.925 >>, 460.000, FALSE, FALSE)	// From right before first intersection to curve before main intersection, past water tower.
			SET_ROADS_IN_ANGLED_AREA(<< 1221.789, -2006.202, 24.364 >>,	<< 1262.837, -2101.758, 64.364 >>, 460.000, FALSE, FALSE)	// From curve before main intersection, to before first non-race left turn after intersection.
			SET_ROADS_IN_ANGLED_AREA(<< 806.686, -2015.736, 9.354 >>,	<< 795.051, -2119.083, 49.354 >>, 790.000, FALSE, FALSE)	// From after crossing main intersection to slight right.
		
		
			// Ninth road set: From slight right towards metro stop to start of beach roads.
			SET_ROADS_IN_ANGLED_AREA(<< 323.806, -1832.678, 6.795 >>,	<< 257.289, -1912.625, 46.795 >>, 710.000, FALSE, FALSE)	// From slight right towards metro stop to left turn after metro stop.
			SET_ROADS_IN_ANGLED_AREA(<< -239.763, -1729.849, 9.875 >>,	<< -164.735, -1864.336, 49.875 >>, 530.000, FALSE, FALSE)	// From left turn from metro to slight right opposite stadium. 
			SET_ROADS_IN_ANGLED_AREA(<< -258.619, -1518.138, 4.395 >>,	<< -356.781, -1483.784, 44.395 >>, 690.000, FALSE, FALSE)	// After slight right/u-turn by stadium, all the way until under bridge.
			SET_ROADS_IN_ANGLED_AREA(<< -160.670, -1123.358, 3.124 >>,	<< -374.574, -1116.961, 43.124 >>, 500.000, FALSE, FALSE)	// From under the bridge to left turn.
			SET_ROADS_IN_ANGLED_AREA(<< -525.382, -796.191, 18.881 >>,	<< -535.791, -879.543, 40.881 >>, 630.000, FALSE, FALSE)	// From left turn to left turn in race.
			SET_ROADS_IN_ANGLED_AREA(<< -799.832, -913.989, 7.844 >>,	<< -740.698, -938.465, 29.844 >>, 630.000, FALSE, FALSE)	// From left turn as it curves into another road.
			SET_ROADS_IN_ANGLED_AREA(<< -812.214, -1072.057, 0.973 >>,	<< -782.054, -1056.361, 22.973 >>, 150.000, FALSE, FALSE)	// From road to first right.
			SET_ROADS_IN_ANGLED_AREA(<< -870.227, -1154.799, -6.444 >>,	<< -852.817, -1184.003, 16.444 >>, 230.000, FALSE, FALSE)	// From first right to right turn towards Venice canals area.
			
			SET_ROADS_IN_ANGLED_AREA(<< -1044.686, -1018.576, -18.191 >>,	<< -1074.690, -1036.597, 22.191 >>, 475.000, FALSE, FALSE)	// From start of venice canals to end of canals, before right turn.
			
			SET_ROADS_IN_ANGLED_AREA(<< -1239.659, -867.453, -5.612 >>,	<< -1220.347, -895.436, 29.612 >>, 150.000, FALSE, FALSE)	// From right turn after canals, to first right.
			SET_ROADS_IN_ANGLED_AREA(<< -1252.833, -1131.194, 1.989 >>,	<< -1202.302, -1112.154, 13.989 >>, 490.000, FALSE, FALSE)	// From first right parallel to beach, to right turn towards beach in race.
			SET_ROADS_IN_ANGLED_AREA(<< -1240.484, -1350.328, -2.141 >>, << -1216.541, -1398.730, 10.141 >>, 190.000, FALSE, FALSE)	// From right turn towards beach to beach walk.
			
			
			// Tenth road set: All the beach roads.
			SET_ROADS_IN_ANGLED_AREA(<< -1399.979, -1146.362, -4.538 >>,	<< -1534.428, -1221.458, 8.538 >>, 560.000, FALSE, FALSE)	// From beach walk to after tunnel on beach.
			SET_ROADS_IN_ANGLED_AREA(<< -1600.241, -937.126, 2.651 >>,	<< -1724.148, -845.677, 14.538 >>, 150.000, FALSE, FALSE)	// Around parking lot after tunnel.
			SET_ROADS_IN_ANGLED_AREA(<< -1873.631, -615.433, 5.385 >>,	<< -1889.230, -633.673, 17.385 >>, 650.000, FALSE, FALSE)	// Long bike road from parking lot before sharp right turn.
			SET_ROADS_IN_ANGLED_AREA(<< -2025.051, -434.340, 5.345 >>,	<< -2066.297, -399.486, 17.345 >>, 120.000, FALSE, FALSE)	// From bike road end to highway exit road.
			
			
			// Eleventh road set: After beach until bike->run transition.
			SET_ROADS_IN_ANGLED_AREA(<< -1912.926, -482.324, 1.968 >>,	<< -1883.548, -437.015, 43.968 >>, 430.000, FALSE, FALSE)	// From highway exit road start to end.
			SET_ROADS_IN_ANGLED_AREA(<< -1637.753, -610.882, 25.238 >>,	<< -1619.315, -587.217, 41.238 >>, 255.000, FALSE, FALSE)	// From end of highway ramp to second right.
			SET_ROADS_IN_ANGLED_AREA(<< -1387.339, -584.663, 22.275 >>,	<< -1404.017, -559.726, 38.275 >>, 315.000, FALSE, FALSE)	// From second right to first big intersection.
			SET_ROADS_IN_ANGLED_AREA(<< -1140.504, -438.163, 28.024 >>,	<< -1166.388, -384.033, 44.024 >>, 355.000, FALSE, FALSE)	// From big intersection to left race road turn.
			SET_ROADS_IN_ANGLED_AREA(<< -1012.658, -293.632, 29.915 >>,	<< -1048.317, -311.756, 45.915 >>, 120.000, FALSE, FALSE)	// From left race road turn to right turn into long race road.
			SET_ROADS_IN_ANGLED_AREA(<< -855.566, -172.588, 29.989 >>,	<< -869.863, -146.214, 45.989 >>, 480.000, FALSE, FALSE)	// From start of long road to end of long race road left turn.
			SET_ROADS_IN_ANGLED_AREA(<< -689.471, -37.670, 29.809 >>,	<< -715.916, -51.835, 45.809 >>, 100.000, FALSE, FALSE)		// From end of long race road to start of last bike ascent.
			SET_ROADS_IN_ANGLED_AREA(<< -721.977, 106.795, 26.744 >>,	<< -771.364, 98.991, 84.744 >>, 270.000, FALSE, FALSE)		// From start of last bike ascent to end of last bike ascent.
			SET_ROADS_IN_ANGLED_AREA(<< -1018.792, 295.542, 54.800 >>,	<< -1024.618, 185.696, 76.800 >>, 490.000, FALSE, FALSE)	// From end of ascent to road by bike->run transition area.
		
		
			// Twelveth road set: From bike->run tranisition to gate.
			SET_ROADS_IN_ANGLED_AREA(<< -1330.464, 309.088, 51.596 >>,	<< -1305.142, 212.347, 73.596 >>, 160.000, FALSE, FALSE)	// Bike->run transition area.
			SET_ROADS_IN_ANGLED_AREA(<< -1407.836, 246.958, 48.861 >>,	<< -1432.525, 229.915, 70.861 >>, 140.000, FALSE, FALSE)	// From bike->run exit to end after right turn.
			SET_ROADS_IN_ANGLED_AREA(<< -1477.162, 261.072, 56.639 >>,	<< -1462.686, 247.272, 66.639 >>, 90.000, FALSE, FALSE)		// From end of right turn to start of left turn towards university.
			SET_ROADS_IN_ANGLED_AREA(<< -1552.774, 281.729, 50.794 >>,	<< -1575.409, 248.749, 66.794 >>, 190.000, FALSE, FALSE)	// From start of uni road to end of zig-zag.
			SET_ROADS_IN_ANGLED_AREA(<< -1774.927, 321.237, 48.227 >>,	<< -1705.271, 188.391, 82.227 >>, 260.000, FALSE, FALSE)	// From end of zig-zag road to end of uni road.
			SET_ROADS_IN_ANGLED_AREA(<< -1827.498, 88.766, 63.301 >>,	<< -1802.620, 120.088, 85.301 >>, 90.000, FALSE, FALSE)		// From end of uni road to right turn towards last run ascent.
			SET_ROADS_IN_ANGLED_AREA(<< -2025.797, 173.784, 57.746 >>,	<< -1857.736, 16.762, 121.746 >>, 140.000, FALSE, FALSE)	// From start of run ascent to end of small curve.
			SET_ROADS_IN_ANGLED_AREA(<< -2159.272, 628.243, 89.682 >>,	<< -2082.667, 124.029, 177.682 >>, 130.000, FALSE, FALSE)	// From end of small curve to start of u-turn ascent.
			SET_ROADS_IN_ANGLED_AREA(<< -2361.799, 453.168, 128.538 >>,	<< -2190.908, 509.703, 216.538 >>, 200.000, FALSE, FALSE)	// From u-turn ascent to end of race.
		
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_ANY_TRAFFIC_FROM_TRI_RACE_ROADS] Race road nodes turned OFF in Triathlon 3.")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Remove 0-based road node speed zones.
///    
///    This is called when the game is restarted or script is terminated.
PROC REMOVE_TRI_RACE_ROAD_NODE_SPEED_ZONES()
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_TRI_RACE_ROAD_NODE_SPEED_ZONES] Procedure started.")
	
	INT iNodeCounter
	REPEAT iNumOfSpeedDampenerRoadNodes iNodeCounter
		REMOVE_ROAD_NODE_SPEED_ZONE(iTriSpeedDampenerRoadNodes[iNodeCounter])
	ENDREPEAT
	
	REPEAT iNumOfIronSpeedDampenerRoadNodesPerGroup iNodeCounter
		REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group1[iNodeCounter])
		REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group2[iNodeCounter])
		REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group3[iNodeCounter])
		REMOVE_ROAD_NODE_SPEED_ZONE(iTriIronSpeedDampenerRoadNodes_Group4[iNodeCounter])
	ENDREPEAT
	
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REMOVE_TRI_RACE_ROAD_NODE_SPEED_ZONES] Remove Triathlon race road speed nodes.")
ENDPROC

/// PURPOSE:
///    Set race roads back to their original state.
///    
///    This is called only when the script terminates.
PROC SET_TRI_RACE_ROADS_BACK_TO_ORIGINAL_STATE()
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SET_TRI_RACE_ROADS_BACK_TO_ORIGINAL_STATE] Procedure started.")
	
	// Reset any traffic-related data shared across all Tri races.
	SET_VEHICLE_POPULATION_BUDGET(3)
	CLEAR_PED_NON_CREATION_AREA()
	REMOVE_SCENARIO_BLOCKING_AREAS()
	
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SET_TRI_RACE_ROADS_BACK_TO_ORIGINAL_STATE] Reset vehicle and ped density and population to original state.")
	
	
	// Re-enable dispatching.
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, TRUE)
	SET_CREATE_RANDOM_COPS(TRUE)
	
	
	//Reenable vehicle generators
	VECTOR vMin, vMax 
	vMin = << -1302.005, -1779.373, -2.155 >>
	vMax = << -1331.091, -1749.1610, 2.789 >>
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMin, vMax, TRUE)
	
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SET_TRI_RACE_ROADS_BACK_TO_ORIGINAL_STATE] Re-enabling dispatching services.")
ENDPROC

// ===================================================
// 	E N D  RACE ROADS PROCEDURES AND FUNCTIONS
// ===================================================





// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
// 		END OF FILE - DO NOT ADD ANYTHING BELOW THIS BLOCK!
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************









