// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	TRI_Triathlon_Cutscenes.sch
//		AUTHOR			:	Carlos Mijares (CM)
//		DESCRIPTION		:	Functions and procedures that handle cutscenes in every 
//								Triathlon race.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************



// ===================================
// 	FILE INCLUDES
// ===================================

USING "TRI_Head.sch"
USING "tri_helpers.sch"

// ===================================
// 	E N D  FILE INCLUDES
// ===================================





// =====================================
// 	TRIATHLON CUTSCENE VARIABLES
// =====================================

CAMERA_INDEX camScene_End
CAMERA_INDEX camIntroSkyview_Start
CAMERA_INDEX camIntroSkyview_End
CAMERA_INDEX camIntroWater_Start
CAMERA_INDEX camIntroWater_End
CAMERA_INDEX camIntroRacer_Start
CAMERA_INDEX camIntroRacer_End
CAMERA_INDEX camIntroIECam
CAMERA_INDEX camIntroCatchupCam1, camIntroCatchupCam2


VECTOR vIntensityPosStart
VECTOR vIntensityPosEnd
VECTOR vIntensityRotStart
VECTOR vIntensityRotEnd

VECTOR vEnergyPosStart
VECTOR vEnergyPosEnd
VECTOR vEnergyRotStart
VECTOR vEnergyRotEnd

INT iChatterCount = 0
FLOAT fDelay = GET_RANDOM_FLOAT_IN_RANGE(7.0, 9.0)
FLOAT fCheeerDelay = GET_RANDOM_FLOAT_IN_RANGE(1.0, 3.0)
PED_INDEX pedChattering 

structTimer timerCutsceneCamera
structTimer diagTimer
structTimer cheerTimer

ENUM TRI_CUTSCENE_STAGE
	TRI_CUTSCENE_STAGE_SETUP,				//0
	TRI_CUTSCENE_STAGE_SIGN_IN,
	TRI_CUTSCENE_STAGE_SIGN_IN_WAIT,
	TRI_CUTSCENE_STAGE_HELI_CREATE,
	TRI_CUTSCENE_STAGE_HELI_WAIT,
	TRI_CUTSCENE_STAGE_HELI_HOVER,
	TRI_CUTSCENE_STAGE_SKYVIEW,
	TRI_CUTSCENE_STAGE_SKYVIEW_HELP,
	TRI_CUTSCENE_STAGE_SKYVIEW_WAIT,
	TRI_CUTSCENE_STAGE_WATER,
	TRI_CUTSCENE_STAGE_WATER_WAIT,
	TRI_CUTSCENE_STAGE_WATER_WAIT_HELI,
	TRI_CUTSCENE_STAGE_BIKE,
	TRI_CUTSCENE_STAGE_BIKE_WAIT,
	TRI_CUTSCENE_STAGE_FINISH_LINE,
	TRI_CUTSCENE_STAGE_FINISH_LINE_WAIT,
	TRI_CUTSCENE_STAGE_RACER,
	TRI_CUTSCENE_STAGE_RACER_WAIT,
	TRI_CUTSCENE_STAGE_INTENSITY,
	TRI_CUTSCENE_STAGE_INTENSITY_WAIT,
	TRI_CUTSCENE_STAGE_ENERGY,
	TRI_CUTSCENE_STAGE_ENERGY_WAIT,
	TRI_CUTSCENE_STAGE_HP_LOSS,
	TRI_CUTSCENE_STAGE_HP_LOSS_WAIT,
	TRI_CUTSCENE_STAGE_GAME_CAM,
	TRI_CUTSCENE_STAGE_GAME_CAM_WAIT,
	TRI_CUTSCENE_STAGE_CLEANUP
ENDENUM

STRUCT TRI_OUTRO_SCENE_DATA
	TEXT_LABEL_31 texOutroDict
	INT iOutroSceneID
	OBJECT_INDEX oBottle
ENDSTRUCT
TRI_OUTRO_SCENE_DATA sOutroSceneData

TRI_CUTSCENE_STAGE eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_SETUP

// =====================================
// 	E N D  TRIATHLON CUTSCENE VARIABLES
// =====================================





// ====================================================
// 	TRIATHLON CUTSCENE FUNCTIONS AND PROCEDURES
// ====================================================

PROC TRI_PLAY_CAM_SNAP_DOWN_FROM_BOARD(TRIATHLON_RACE_INDEX &eTriRace)
	INT iDuration = 600
	VECTOR vCamPos, vCamRot
	
	SET_CAM_ACTIVE(camIntroIECam, TRUE)
	
	enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
	
	// Pick where we're setting the cam to now.
	IF (eTriRace = TRIATHLON_RACE_VESPUCCI)
		IF (ePed = CHAR_FRANKLIN)
			vCamPos = <<-1340.1471, -1015.2643, 23.0>>
			vCamRot = <<80.0, 0.0, -160.0018>>
		ELIF (ePed = CHAR_MICHAEL)
			vCamPos = <<-1341.5535, -1016.5138, 23.0>>
			vCamRot = <<80.0, 0.0, -66.7842>>
		ELIF (ePed = CHAR_TREVOR)
			vCamPos = <<-1340.8750, -1015.0278, 23.0>>
			vCamRot = <<80.0, 0.0854, -133.9209>>
		ENDIF
	ELIF (eTriRace = TRIATHLON_RACE_ALAMO_SEA)
		IF (ePed = CHAR_FRANKLIN)
			vCamPos = <<1760.1621, 3904.5063, 50.0>>
			vCamRot = <<80.0, 0.0, -121.7289>>
		ELIF (ePed = CHAR_MICHAEL)
			vCamPos = <<1761.2694, 3901.9807, 50.0>>
			vCamRot = <<80.0, 0.0, -28.4583>>
		ELIF (ePed = CHAR_TREVOR)
			vCamPos = <<1760.7273, 3903.4209, 50.0>>
			vCamRot = <<80.0, 0.1610, -95.6354>>
		ENDIF
	ELSE		
		IF (ePed = CHAR_FRANKLIN)
			vCamPos = <<-2279.9561, 414.1333, 190.0>>
			vCamRot = <<80.0, 0.0, 164.7893>>
		ELIF (ePed = CHAR_MICHAEL)
			vCamPos = <<-2282.2585, 412.3413, 190.0>>
			vCamRot = <<80.0, 0.0, -101.9460>>
		ELIF (ePed = CHAR_TREVOR)
			vCamPos = <<-2280.8669, 413.3051, 190.0>>
			vCamRot = <<80.0, 0.1686, -175.5375>>
		ENDIF
	ENDIF
	SET_CAM_COORD(camIntroIECam, vCamPos)
	SET_CAM_ROT(camIntroIECam, vCamRot)
	TRIGGER_SCREENBLUR_FADE_OUT(TO_FLOAT(iDuration))
	RENDER_SCRIPT_CAMS(TRUE, FALSE)

	CPRINTLN(DEBUG_TRIATHLON, "TRI_PLAY_CAM_SNAP_DOWN_FROM_BOARD :: vCamPos=", vCamPos, ", vCamRot=", vCamRot)
ENDPROC

PROC PLAY_TRI_CAM_SNAP_UPWARD_AFTER_OUTRO()
	INT iDuration = 700
	
	// Put the camera we're going to interp to where the cam is now.
	VECTOR vCamPos, vCamRot
	IF DOES_CAM_EXIST(camScene_End)
		vCamPos = GET_CAM_COORD(camScene_End)
		vCamRot = GET_CAM_ROT(camScene_End)
	ENDIF
	
	camIntroIECam = CREATE_CAMERA()
	SET_CAM_COORD(camIntroIECam, vCamPos)
	SET_CAM_ROT(camIntroIECam, vCamRot)
	SET_CAM_FOV(camIntroIECam, 30.0)
	SET_CAM_ACTIVE(camIntroIECam, TRUE)
	
	// Destroy the cam after, to prevent pops.
	IF DOES_CAM_EXIST(camScene_End)
		DESTROY_CAM(camScene_End)
	ENDIF
	
	// Move cam up, and rotate it up.
	VECTOR vEndPos, vEndRot
	vEndPos = vCamPos + <<0,0,20.0>>
	vEndRot = vCamRot + <<70.0,0,0>>

	SET_CAM_PARAMS(camIntroIECam, vEndPos, vEndRot, 30, iDuration)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	TRIGGER_SCREENBLUR_FADE_IN(TO_FLOAT(iDuration))
	
	PLAY_SOUND_FRONTEND(-1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
	
	CPRINTLN(DEBUG_TRIATHLON, "PLAY_TRI_CAM_SNAP_UPWARD_AFTER_OUTRO :: vCamPos=", vCamPos, ", vCamRot=", vCamRot)
ENDPROC

FUNC FLOAT TRI_GET_RACER_FINISH_TELEPORT_HEADING(TRIATHLON_RACE_INDEX eTriRace, INT iRacer)
	FLOAT fRetVal
	
	SWITCH eTriRace
		CASE TRIATHLON_RACE_VESPUCCI
			SWITCH iRacer
				CASE 1
					fRetVal = 214.1666
				BREAK
				CASE 2
					fRetVal = 136.9917 
				BREAK
			ENDSWITCH
		BREAK
		CASE TRIATHLON_RACE_ALAMO_SEA
			SWITCH iRacer
				CASE 1
					fRetVal = 20.6779
				BREAK
				CASE 2
					fRetVal = 335.9650
				BREAK
			ENDSWITCH
		BREAK
		CASE TRIATHLON_RACE_IRONMAN
			SWITCH iRacer
				CASE 1
					fRetVal = 192.4683
				BREAK
				CASE 2
					fRetVal = 190.4380
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN fRetVal
ENDFUNC

FUNC VECTOR TRI_GET_RACER_FINISH_TELEPORT_POS(TRIATHLON_RACE_INDEX eTriRace, INT iRacer)
	VECTOR vRetVal = <<0,0,0>>
	SWITCH eTriRace
		CASE TRIATHLON_RACE_VESPUCCI
			SWITCH iRacer
				CASE 1
					vRetVal = <<-1333.5430, -1034.3549, 6.6195>>
				BREAK
				CASE 2
					vRetVal = <<-1328.6180, -1045.0208, 6.5493>>
				BREAK
			ENDSWITCH
		BREAK
		CASE TRIATHLON_RACE_ALAMO_SEA
			SWITCH iRacer
				CASE 1
					vRetVal = <<1755.5608, 3891.0405, 33.7649>>
				BREAK
				CASE 2
					vRetVal = <<1759.7052, 3889.2129, 33.7466>>
				BREAK
			ENDSWITCH
		BREAK
		CASE TRIATHLON_RACE_IRONMAN
			SWITCH iRacer
				CASE 1
					vRetVal = <<-2299.4753, 465.9030, 173.4305>>
				BREAK
				CASE 2
					vRetVal = <<-2300.4429, 462.9010, 173.4523>>
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN vRetVal
ENDFUNC

PROC TRI_CLEAR_RACERS_FROM_OUTRO_CUTSCENE(TRI_RACE_STRUCT &thisRace, TRIATHLON_RACE_INDEX &eTriRace)
	
	VECTOR vScenePos
	IF eTriRace = TRIATHLON_RACE_VESPUCCI
		vScenePos = <<-1329.3944, -1051.3582, 6.5187>>
	ELIF eTriRace = TRIATHLON_RACE_ALAMO_SEA
		vScenePos = <<1757.5648, 3897.9070, 33.8581>>
	ELSE
		vScenePos = <<-2306.3442, 454.1608, 173.4667>>
	ENDIF
	
	VECTOR vMoveDirection = vScenePos - GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE )
	FLOAT fMagnitude = VMAG( vMoveDirection )
	#IF IS_DEBUG_BUILD
	DEBUG_RECORD_ENTITY_FLOAT( thisRace.Racer[0].Driver, "fMagnitude", fMagnitude )
	#ENDIF
	
	INT i=1
	VECTOR vRacerPos
	FLOAT fDistance
	FOR i = 1 TO 7
		vRacerPos = GET_ENTITY_COORDS( thisRace.Racer[i].Driver, FALSE )
		fDistance = VDIST( vRacerPos, vScenePos )
		#IF IS_DEBUG_BUILD
		DEBUG_RECORD_ENTITY_FLOAT( thisRace.Racer[i].Driver, "vDistance", fDistance )
		#ENDIF
		IF fDistance < fMagnitude
			CPRINTLN( DEBUG_TRIATHLON, "moving racer[", i, "], too close to the outro scene position." )
			SET_ENTITY_COORDS( thisRace.Racer[i].Driver, vScenePos + vMoveDirection, FALSE, TRUE )
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	DEBUG_RECORD_LINE( "vMoveDirection", vScenePos, vScenePos + vMoveDirection, (<<255,0,0>>), (<<0,0,255>>) )
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisTOSData - 
///    thisRace - 
///    eTriRace - 
///    bWonRace - 
///    uses camIntroIECam camera index.
PROC PLAY_TRI_OUTRO_SYNC_SCENE_WINLOSS(TRI_OUTRO_SCENE_DATA &thisTOSData, TRI_RACE_STRUCT &thisRace, TRIATHLON_RACE_INDEX &eTriRace, BOOL bWonRace)
	FLOAT fHeading
	VECTOR vScenePos
	IF eTriRace = TRIATHLON_RACE_VESPUCCI
		fHeading = 201.6718
		vScenePos = <<-1332.92249, -1043.14160, 6.65>>
	ELIF eTriRace = TRIATHLON_RACE_ALAMO_SEA
		fHeading = 25.4602
		vScenePos = <<1759.43518, 3894.69409, 33.789>>
	ELSE
		fHeading = 167.8617
		vScenePos = <<-2304.44312, 462.66916, 173.4493>>
	ENDIF
	
	enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
	STRING sPlayer
	IF (ePed = CHAR_MICHAEL)
		sPlayer = "michael"
		fHeading -= 180.0			// Michael's anims are backwards.
	ELIF (ePed = CHAR_FRANKLIN)
		sPlayer = "franklin"
		fHeading -= 180.0			// Franklin's anims are backwards.
	ELIF (ePed = CHAR_TREVOR)
		sPlayer = "trevor"
		fHeading -= 180.0			// Trevor's anims are backwards.
	ENDIF

	thisTOSData.iOutroSceneID = CREATE_SYNCHRONIZED_SCENE(vScenePos, (<<0,0,fHeading>>))
	
	TEXT_LABEL_31 texAnim = PICK_STRING(bWonRace, "win", "lose")
	texAnim += "_race_"
	texAnim += sPlayer
	
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	CLEAR_PED_TASKS_IMMEDIATELY(thisRace.Racer[0].Driver)
	CLEAR_PED_PROP(thisRace.Racer[0].Driver, ANCHOR_EYES)
	CLEAR_PED_PROP(thisRace.Racer[0].Driver, ANCHOR_HEAD)
	CLEAR_AREA_OF_PEDS(vScenePos, 3.0)
	DISABLE_NAVMESH_IN_AREA(<< vScenePos.x - 3.0, vScenePos.y - 3.0, vScenePos.z - 3.0 >>, << vScenePos.x + 3.0, vScenePos.y + 3.0, vScenePos.z + 3.0 >>, TRUE)
	
	
	TASK_SYNCHRONIZED_SCENE(thisRace.Racer[0].Driver, thisTOSData.iOutroSceneID, thisTOSData.texOutroDict, texAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
	CPRINTLN(DEBUG_TRIATHLON, "PLAY_TRI_OUTRO_SYNC_SCENE_WINLOSS, thisTOSData.iOutroSceneID=", thisTOSData.iOutroSceneID, ", thisTOSData.texOutroDict=", thisTOSData.texOutroDict, ", texAnim=", texAnim)
	texAnim += "_cam"
	IF DOES_CAM_EXIST(camScene_End)
		DESTROY_CAM(camScene_End)
	ENDIF
	camScene_End = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
	SET_CAM_ACTIVE(camScene_End, TRUE)
	PLAY_SYNCHRONIZED_CAM_ANIM(camScene_End, thisTOSData.iOutroSceneID, texAnim, thisTOSData.texOutroDict)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	TRI_CLEAR_RACERS_FROM_OUTRO_CUTSCENE( thisRace, eTriRace )
	//SET_CAM_ACTIVE_WITH_INTERP(camScene_End, camIntroIECam, 600)
	
	INT iFinishedCount = TRI_GET_NUM_RACERS_FINISHED(thisRace)
	INT iIndex, iRacerNum
	iRacerNum = 1
	CPRINTLN( DEBUG_TRIATHLON, "Finished count is ", iFinishedCount )
	IF iFinishedCount >= 4
		CPRINTLN( DEBUG_TRIATHLON, "4 racers have finished..." )
		// Teleport 2 racers
		REPEAT COUNT_OF(thisRace.Racer) iIndex 
			IF thisRace.Racer[iIndex].Driver <> PLAYER_PED_ID()
				IF NOT IS_ENTITY_DEAD(thisRace.Racer[iIndex].Driver)
					IF HAS_TRI_RACER_FINISHED_RACE(thisRace, thisRace.Racer[iIndex])
						CPRINTLN( DEBUG_TRIATHLON, "Racer ", iIndex, " has finished the race." )
						SET_ENTITY_COORDS(thisRace.Racer[iIndex].Driver, TRI_GET_RACER_FINISH_TELEPORT_POS(eTriRace, iRacerNum))
						SET_ENTITY_HEADING(thisRace.Racer[iIndex].Driver, TRI_GET_RACER_FINISH_TELEPORT_HEADING(eTriRace, iRacerNum))
						
						IF (thisRace.Racer[iIndex].iRank = 1)
							STRING szTempAnim = GET_RANDOM_ANIM_CLIP_FROM_TRI_ANIM_DICTIONARY(szTriAnimDicts[0])
							TASK_PLAY_ANIM(thisRace.Racer[iIndex].Driver, szTriAnimDicts[0], szTempAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0))
						ELSE
							STRING szTempAnim = GET_RANDOM_ANIM_CLIP_FROM_TRI_ANIM_DICTIONARY(szTriAnimDicts[1])
							TASK_PLAY_ANIM(thisRace.Racer[iIndex].Driver, szTriAnimDicts[1], szTempAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0))
						ENDIF	
						++iRacerNum
					ENDIF
				ENDIF
				
				IF iRacerNum > 2
					iIndex = thisRace.iRacerCnt
				ENDIF
			ENDIF
			
		ENDREPEAT
	ELSE
		REPEAT COUNT_OF(thisRace.Racer) iIndex 
			IF thisRace.Racer[iIndex].Driver <> PLAYER_PED_ID()
				IF NOT IS_ENTITY_DEAD(thisRace.Racer[iIndex].Driver)
					IF HAS_TRI_RACER_FINISHED_RACE(thisRace, thisRace.Racer[iIndex])
						SET_ENTITY_COORDS(thisRace.Racer[iIndex].Driver, TRI_GET_RACER_FINISH_TELEPORT_POS(eTriRace, iRacerNum))
						SET_ENTITY_HEADING(thisRace.Racer[iIndex].Driver, TRI_GET_RACER_FINISH_TELEPORT_HEADING(eTriRace, iRacerNum))
						
						IF (thisRace.Racer[iIndex].iRank = 1)
							STRING szTempAnim = GET_RANDOM_ANIM_CLIP_FROM_TRI_ANIM_DICTIONARY(szTriAnimDicts[0])
							TASK_PLAY_ANIM(thisRace.Racer[iIndex].Driver, szTriAnimDicts[0], szTempAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0))
						ELSE
							STRING szTempAnim = GET_RANDOM_ANIM_CLIP_FROM_TRI_ANIM_DICTIONARY(szTriAnimDicts[1])
							TASK_PLAY_ANIM(thisRace.Racer[iIndex].Driver, szTriAnimDicts[1], szTempAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0))
						ENDIF	
						iIndex = thisRace.iRacerCnt
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	CPRINTLN(DEBUG_TRIATHLON, "PLAY_TRI_OUTRO_SYNC_SCENE_WINLOSS, Cam texAnim=", texAnim)
ENDPROC

PROC PLAY_TRI_OUTRO_SYNC_SCENE_WATER_BOTTLE(TRI_OUTRO_SCENE_DATA &thisTOSData, TRI_RACE_STRUCT &thisRace, VECTOR vScenePos, FLOAT fHeading)
	thisTOSData.iOutroSceneID = CREATE_SYNCHRONIZED_SCENE(vScenePos, (<<0,0,fHeading>>))
	
	SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(thisTOSData.iOutroSceneID, FALSE)
	enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
	TEXT_LABEL_31 texAnim = "triathlon_outro_"
	IF ePed = CHAR_MICHAEL
		texAnim += "michael"
	ELIF ePed = CHAR_FRANKLIN
		texAnim += "franklin"
	ELIF ePed = CHAR_TREVOR
		texAnim += "trevor"
	ENDIF
	CLEAR_PED_TASKS_IMMEDIATELY(thisRace.Racer[0].Driver)
	TASK_SYNCHRONIZED_SCENE(thisRace.Racer[0].Driver, thisTOSData.iOutroSceneID, thisTOSData.texOutroDict, texAnim, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT)
	CPRINTLN(DEBUG_TRIATHLON, "PLAY_TRI_OUTRO_SYNC_SCENE_WATER_BOTTLE, thisTOSData.iOutroSceneID=", thisTOSData.iOutroSceneID, ", thisTOSData.texOutroDict=", thisTOSData.texOutroDict, ", texAnim=", texAnim)
	
	FADE_UP_PED_LIGHT()
	TEXT_LABEL_31 texCamAnim = texAnim
	texCamAnim += "_cam"
	IF DOES_CAM_EXIST(camScene_End)
		DESTROY_CAM(camScene_End)
	ENDIF
	camScene_End = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
	SET_CAM_ACTIVE(camScene_End, TRUE)
	PLAY_SYNCHRONIZED_CAM_ANIM(camScene_End, thisTOSData.iOutroSceneID, texCamAnim, thisTOSData.texOutroDict)
	//RENDER_SCRIPT_CAMS(TRUE, FALSE)
	SET_CAM_ACTIVE_WITH_INTERP(camScene_End, camIntroIECam, 600)
	PLAY_SOUND_FRONTEND(-1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
	CPRINTLN(DEBUG_TRIATHLON, "PLAY_TRI_OUTRO_SYNC_SCENE_WATER_BOTTLE, Cam texCamAnim=", texCamAnim)
	
	TEXT_LABEL_31 texBottleAnim = texAnim
	texBottleAnim += "_bottle"
	thisTOSData.oBottle = CREATE_OBJECT(PROP_ENERGY_DRINK, vScenePos)
	PLAY_SYNCHRONIZED_ENTITY_ANIM(thisTOSData.oBottle, thisTOSData.iOutroSceneID, texBottleAnim, thisTOSData.texOutroDict, INSTANT_BLEND_IN)
	CPRINTLN(DEBUG_TRIATHLON, "PLAY_TRI_OUTRO_SYNC_SCENE_WATER_BOTTLE, bottle texBottleAnim=", texBottleAnim)
ENDPROC

PROC UPDATE_TRI_OUTRO_ASSETS(TRI_RACE_STRUCT &thisRace, TRI_OUTRO_SCENE_DATA &thisTOSData)
	IF thisRace.Racer[0].iGateCur >= thisRace.iGateCnt - 3 AND NOT IS_TRI_CONTROL_FLAG_SET(TCF_OUTRO_ASSETS_REQUESTED)
		SET_TRI_CONTROL_FLAG(TCF_OUTRO_ASSETS_REQUESTED)
		thisTOSData.texOutroDict = "mini@triathlon"
		enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
		IF ePed = CHAR_MICHAEL
			thisTOSData.texOutroDict += "michael"
		ELIF ePed = CHAR_FRANKLIN
			thisTOSData.texOutroDict += "franklin"
		ELIF ePed = CHAR_TREVOR
			thisTOSData.texOutroDict += "trevor"
		ENDIF
		REQUEST_ANIM_DICT(thisTOSData.texOutroDict)
		CPRINTLN(DEBUG_TRIATHLON, "UPDATE_TRI_OUTRO_ASSETS :: requested ", thisTOSData.texOutroDict)
		REQUEST_MODEL(PROP_ENERGY_DRINK)
	ENDIF
ENDPROC

FUNC BOOL HAVE_TRI_OUTRO_ASSETS_LOADED(TRI_OUTRO_SCENE_DATA &thisTOSData)
	IF NOT HAS_ANIM_DICT_LOADED(thisTOSData.texOutroDict)
		CPRINTLN(DEBUG_TRIATHLON, "HAVE_TRI_OUTRO_ASSETS_LOADED waiting on ", thisTOSData.texOutroDict)
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(PROP_ENERGY_DRINK)
		CPRINTLN(DEBUG_TRIATHLON, "HAVE_TRI_OUTRO_ASSETS_LOADED waiting on PROP_ENERGY_DRINK")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Create the cutscene cameras for the current race.
PROC Tri_Cutscenes_Create_Cameras()

	DEBUG_MESSAGE("[TRI_Triathlon_Cutscenes->Tri_Cutscenes_Create_Cameras] Started procedure.")

	SWITCH eCurrentTriRace
	
		// Triathlon 1
		CASE TRIATHLON_RACE_ALAMO_SEA
			// Sky view cameras.	
			
			IF NOT DOES_CAM_EXIST(camIntroSkyview_Start)
				camIntroSkyview_Start 		= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2405.7759, 4282.3945, 35.6522>>, <<-1.3334, 0.0000, 95.4363>>, 30.30)
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroSkyview_End)
				camIntroSkyview_End			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2405.5820, 4280.5117, 33.9315>>, <<5.2742, -0.0000, 91.9419>>, 50.0)
			ENDIF
			
			// Water cameras.
			IF NOT DOES_CAM_EXIST(camIntroWater_Start)
				camIntroWater_Start 		= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2386.6360, 4295.4775, 32.2856>>, <<-1.1846, 0.6029, -150.5774>>, 24.0182)
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroWater_End)
				camIntroWater_End			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2386.8499, 4295.0957, 32.2766>>, <<-1.1846, 0.6029, -150.5774>>, 24.0182)
			ENDIF
			
			// Racer and spectator cameras.
			IF NOT DOES_CAM_EXIST(camIntroRacer_Start)
				camIntroRacer_Start	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 2403.0247, 4281.1650, 33.5233 >>, << -5.6822, 0.0000, 95.0976 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroRacer_End)
				camIntroRacer_End = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 2402.9746, 4281.7417, 33.5233 >>, << -5.6822, 0.0000, 95.0976 >>, 50.0)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(camIntroCatchupCam1)
				camIntroCatchupCam1	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2398.0886, 4282.6377, 33.2085>>, <<-6.7472, 0.0000, 99.2434>>, 40.0100 )
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroCatchupCam2)
				camIntroCatchupCam2	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2397.4504, 4282.5605, 33.1346>>, <<-5.6170, -0.0000, 98.3052>>, 40.0100 )
			ENDIF
			
			vIntensityPosStart			= <<2389.7273, 4251.5620, 40.2787>>//<<2390.8015, 4288.4790, 32.5432>>
			vIntensityRotStart			= <<-20.5590, 0.6028, -3.2381>>//<<-7.2814, -0.0240, -133.1707>>
			vIntensityPosEnd			= <<2405.3708, 4256.7573, 40.7256>>//<<2391.5134, 4284.8232, 32.5181>>
			vIntensityRotEnd			= <<-14.5943, 0.6248, 41.4011>>//<<-2.6823, -0.0240, -133.1707>>
			
			// cam 4
			vEnergyPosStart				= <<2396.3472, 4271.7251, 32.4817>>//<<2396.8118, 4271.3403, 33.2084>>//<< 2400.2520, 4283.3398, 33.7358 >>
			vEnergyRotStart				= <<2.6718, -0.0000, 3.2264>>//<<-5.2101, 0.0000, 4.2778>>//<< -7.4578, -0.0240, 151.5870 >>
			vEnergyPosEnd				= <<2396.2920, 4272.7051, 32.5122>>//<<2396.5664, 4272.7051, 33.1121>>//<< 2399.2461, 4283.8843, 33.7362 >>
			vEnergyRotEnd				= <<2.0511, 0.0000, 3.2264>>//<<-3.9804, 0.0000, 4.2778>>//<< -7.4578, -0.0240, 151.5870 >>
			
			IF NOT DOES_CAM_EXIST(camIntroIECam)
				camIntroIECam				= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vIntensityPosStart, vIntensityRotStart, 50.0)
			ENDIF
			
			DEBUG_MESSAGE("[TRI_Triathlon_Cutscenes->Tri_Cutscenes_Create_Cameras] Cutscene cameras made in Triathlon 1.")
		BREAK
		
		// Triathlon 2
		CASE TRIATHLON_RACE_VESPUCCI
			// Sky view cameras.
			IF NOT DOES_CAM_EXIST(camIntroSkyview_Start)
				camIntroSkyview_Start 		= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1260.5, -2048.5, 15.4>>, <<16.4, -0.0, 45.2>>, 30.30)
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroSkyview_End)
				camIntroSkyview_End			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1261.4497, -2047.3550, 7.0256>>, <<-2.7528, 0.0163, 45.8717>>, 50.0)
			ENDIF
			
			// Water cameras.
			IF NOT DOES_CAM_EXIST(camIntroWater_Start)
				camIntroWater_Start 		= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1261.4224, -2022.1317, 3.0495>>, <<5.1381, -0.0000, 147.5967>>, 33.6646)
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroWater_End)
				camIntroWater_End			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1261.7815, -2022.6979, 3.1098>>, <<5.1381, -0.0000, 147.5967>>, 33.6646)	
			ENDIF

			// Racer and spectator cameras.
			IF NOT DOES_CAM_EXIST(camIntroRacer_Start)
				camIntroRacer_Start			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1266.98, -2046.60, 4.84 >>, << -5.35, 0.00, 10.12 >>, 25.11)
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroRacer_End)
				camIntroRacer_End			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1266.49, -2046.57, 4.84 >>, << -5.35, 0.00, 12.21 >>, 25.11)
			ENDIF
		
			IF NOT DOES_CAM_EXIST(camIntroCatchupCam1)
				camIntroCatchupCam1			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1265.7010, -2041.5682, 4.6619>>, <<-5.0125, -0.0000, 50.3428>>, 40.0349)
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroCatchupCam2)
				camIntroCatchupCam2			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1266.1606, -2041.1896, 4.6097>>, <<-5.0125, -0.0000, 50.3428>>, 40.0349)
			ENDIF
		
			vIntensityPosStart			= <<-1300.0, -2059.4, 12.3>>//<<-1265.2870, -2041.0748, 5.0353>>
			vIntensityRotStart			= <<-16.4,- 0.0, -52.8>>//<<-15.4951, -0.0000, 94.5429>>
			vIntensityPosEnd			= <<-1287.3, -2066.5, 10.7>>//<<-1265.2870, -2041.0748, 5.0353>>
			vIntensityRotEnd			= <<-10.0,- 0.0, -15.2>>//<<-15.4951, -0.0000, 71.3706>>
			
			vEnergyPosStart				= <<-1277.9, -2047.9, 4.3>>//<< -1270.3761, -2045.4255, 4.4269 >>
			vEnergyRotStart				= <<-2.3,- 0.0, -56.3>>//<< -1.7076, -0.0000, -21.4116 >>
			vEnergyPosEnd				= <<-1276.8, -2046.7, 4.3>>//<< -1271.6577, -2044.9227, 4.4269 >>
			vEnergyRotEnd				= <<-2.3,- 0.0, -56.3>>//<< -1.7076, -0.0000, -21.4116 >>
			
			
			IF NOT DOES_CAM_EXIST(camIntroIECam)
				camIntroIECam				= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vIntensityPosStart, vIntensityRotStart, 50.0)
			ENDIF

			DEBUG_MESSAGE("[TRI_Triathlon_Cutscenes->Tri_Cutscenes_Create_Cameras] Flythrough cameras made in Triathlon 2.")
		BREAK
		
		// Triathlon 3
		CASE TRIATHLON_RACE_IRONMAN
			IF NOT DOES_CAM_EXIST(camIntroSkyview_Start)
				camIntroSkyview_Start 		= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1589.0675, 3834.8213, 54.2202 >>, 	<< 5.3341, -0.0003, 71.4501 >>, 	50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroSkyview_End)
				camIntroSkyview_End			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1590.1229, 3820.2156, 34.9406>>, <<5.1632, -0.0000, 36.9737>>, 50.0)
			ENDIF
			
			// Water cameras.
			IF NOT DOES_CAM_EXIST(camIntroWater_Start)
				camIntroWater_Start 		= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1566.3325, 3823.0542, 32.7110>>, <<-1.0620, 0.0000, -63.9708>>, 32.5918 )
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroWater_End)
				camIntroWater_End			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1567.1281, 3823.4412, 32.6946>>, <<-1.0620, 0.0000, -63.9708>>, 32.5918 )
			ENDIF
			
			// Racer and spectator cameras.
			IF NOT DOES_CAM_EXIST(camIntroRacer_Start)
				camIntroRacer_Start			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1585.4115, 3832.3213, 33.2760>>, <<6.1917, -0.0000, -152.8082>>, 28.6127 )
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroRacer_End)
				camIntroRacer_End			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1585.7034, 3831.7537, 33.3460>>, <<6.7344, -0.0000, -152.8082>>, 28.6127 )
			ENDIF
			
			IF NOT DOES_CAM_EXIST(camIntroCatchupCam1)
				camIntroCatchupCam1			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1589.5148, 3827.8237, 34.0188>>, <<-5.4725, 0.0000, 51.0998>>, 40.0333 )
			ENDIF
			IF NOT DOES_CAM_EXIST(camIntroCatchupCam2)
				camIntroCatchupCam2			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1588.8525, 3828.3591, 33.9373>>, <<-5.4725, 0.0000, 51.0998>>, 40.0333 )
			ENDIF
			
			DEBUG_MESSAGE("[TRI_Triathlon_Cutscenes->Tri_Cutscenes_Create_Cameras] Flythrough cameras made in Triathlon 3.")
		BREAK		
	ENDSWITCH	
ENDPROC


/// PURPOSE:
///    Request cutscene assets for the current race.
PROC Tri_Cutscenes_Request_Assets()

	DEBUG_MESSAGE("[TRI_Triathlon_Cutscenes->Tri_Cutscenes_Request_Assets] Started procedure.")
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
ENDPROC


/// PURPOSE:
///    Load cutscene assets for the current race.
FUNC BOOL Tri_Cutscenes_Load_Assets()

	DEBUG_MESSAGE("[TRI_Triathlon_Cutscenes->Tri_Cutscenes_Load_Assets] Started procedure.")
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	RETURN FALSE

ENDFUNC


/*
	[2] amb@busstop@char2@idle_a		idle_a		// Looks like he's waiting.  Not ideal, but best one found yet.

	[3] amb@queue@impatient@idle_a  	idle_a		// Looking ahead, shaking head in impatience.  Fits ok.
										idle_b		// Swinging his arms.  This looks good.
										idle_c		// Looks at his watch, scratches his head.  Might be acceptable for now.
								
	[4] amb@queue@impatient@idle_b		idle_e		// He looks behind him a couple of times, to both sides, then looks up.  Looks good.
*/


/// PURPOSE:
///    Play racer animations during the intro.
///    TODO: 	Find a way to randomly choose which animation is set to who, so the same
///    			racers aren't always playing the same animation at every race start.
///    			Do this only once we have all animation variations.
PROC Tri_Cutscenes_Play_Racer_Intro_Anims(TRI_RACE_STRUCT& thisRace)
	INT iRacerCounter//, iAnim
//	STRING sAnim
	FLOAT fStartPhase
	REPEAT thisRace.iRacerCnt iRacerCounter
		//iAnim = GET_RANDOM_INT_IN_RANGE(0, 6)
		fStartPhase = GET_RANDOM_FLOAT_IN_RANGE()
		IF NOT IS_ENTITY_DEAD(thisRace.Racer[iRacerCounter].Driver)
//			sAnim = PICK_STRING(iAnim = 0, "idle_a", 
//					PICK_STRING(iAnim = 1, "idle_b", 
//					PICK_STRING(iAnim = 2, "idle_c", 
//					PICK_STRING(iAnim = 3, "idle_d", 
//					PICK_STRING(iAnim = 4, "idle_e", "idle_f")))))
			IF iRacerCounter <> 0
				TASK_PLAY_ANIM(thisRace.Racer[iRacerCounter].Driver, szTriAnimDicts[2], "idle_f", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, fStartPhase)
			ELSE
				TASK_PLAY_ANIM(thisRace.Racer[iRacerCounter].Driver, szTriAnimDicts[2], "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, fStartPhase)		
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SET_TRI_INTRO_ANIM_SPEEDS(TRI_RACE_STRUCT& thisRace)
	IF NOT IS_TRI_CONTROL_FLAG_SET(TCF_INTRO_ANIM_SPEEDS_SET)
		INT iRacerCounter
		FLOAT fSpeed
		STRING sDict = "mini@triathlon"
		REPEAT thisRace.iRacerCnt iRacerCounter
			PED_INDEX ped = thisRace.Racer[iRacerCounter].Driver
			// Make the player playback at 1.0 and the other racers at a reasonable value other than 1.0
			IF iRacerCounter = 0
				fSpeed = 1.0
			ELSE
				fSpeed = PICK_FLOAT(GET_RANDOM_FLOAT_IN_RANGE() <= TRI_ANIM_PLAYBACK_LOW_PROB, GET_RANDOM_FLOAT_IN_RANGE(TRI_ANIM_PLAYBACK_LOW_LOW, TRI_ANIM_PLAYBACK_LOW_HI), GET_RANDOM_FLOAT_IN_RANGE(TRI_ANIM_PLAYBACK_HI_LOW, TRI_ANIM_PLAYBACK_HI_HI))
			ENDIF
			IF NOT IS_ENTITY_DEAD(ped)
				IF IS_ENTITY_PLAYING_ANIM(ped, sDict, "idle_a")
					SET_ENTITY_ANIM_SPEED(ped, sDict, "idle_a", fSpeed)
					CPRINTLN(DEBUG_TRIATHLON, "[tri_cutscenes.sch::SET_TRI_INTRO_ANIM_SPEEDS] iRacer[", iRacerCounter, "] set with fSpeed=", fSpeed)
				ELIF IS_ENTITY_PLAYING_ANIM(ped, sDict, "idle_b")
					SET_ENTITY_ANIM_SPEED(ped, sDict, "idle_b", fSpeed)
					CPRINTLN(DEBUG_TRIATHLON, "[tri_cutscenes.sch::SET_TRI_INTRO_ANIM_SPEEDS] iRacer[", iRacerCounter, "] set with fSpeed=", fSpeed)
				ELIF IS_ENTITY_PLAYING_ANIM(ped, sDict, "idle_c")
					SET_ENTITY_ANIM_SPEED(ped, sDict, "idle_c", fSpeed)
					CPRINTLN(DEBUG_TRIATHLON, "[tri_cutscenes.sch::SET_TRI_INTRO_ANIM_SPEEDS] iRacer[", iRacerCounter, "] set with fSpeed=", fSpeed)
				ELIF IS_ENTITY_PLAYING_ANIM(ped, sDict, "idle_d")
					SET_ENTITY_ANIM_SPEED(ped, sDict, "idle_d", fSpeed)
					CPRINTLN(DEBUG_TRIATHLON, "[tri_cutscenes.sch::SET_TRI_INTRO_ANIM_SPEEDS] iRacer[", iRacerCounter, "] set with fSpeed=", fSpeed)
				ELIF IS_ENTITY_PLAYING_ANIM(ped, sDict, "idle_e")
					SET_ENTITY_ANIM_SPEED(ped, sDict, "idle_e", fSpeed)
					CPRINTLN(DEBUG_TRIATHLON, "[tri_cutscenes.sch::SET_TRI_INTRO_ANIM_SPEEDS] iRacer[", iRacerCounter, "] set with fSpeed=", fSpeed)
				ELIF IS_ENTITY_PLAYING_ANIM(ped, sDict, "idle_f")
					SET_ENTITY_ANIM_SPEED(ped, sDict, "idle_f", fSpeed)
					CPRINTLN(DEBUG_TRIATHLON, "[tri_cutscenes.sch::SET_TRI_INTRO_ANIM_SPEEDS] iRacer[", iRacerCounter, "] set with fSpeed=", fSpeed)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	SET_TRI_CONTROL_FLAG(TCF_INTRO_ANIM_SPEEDS_SET)
ENDPROC


/// PURPOSE:
///    Stop playing racer animations in intro.
PROC Tri_Cutscenes_Setup_Racer_Intro_Anims(TRI_RACE_STRUCT& Race)
	CPRINTLN(DEBUG_TRIATHLON, "Tri_Cutscenes_Setup_Racer_Intro_Anims called")
//	INT iRacerCounter
//	FLOAT fWait
//	
//	REPEAT Race.iRacerCnt iRacerCounter
//		fWait = GET_RANDOM_FLOAT_IN_RANGE(0, ANIM_WAIT_THRESHOLD)
//		Race.Racer[iRacerCounter].fAnimWait = fWait
//		CPRINTLN(DEBUG_TRIATHLON, "iRacer[", iRacerCounter, "] has an fWait of ", fWait)
//	ENDREPEAT

/// PURPOSE:
///    Does the waiting around while the player animates during the countdown.
/// PARAMS:
///    Race - 
	INT p
	REPEAT Race.iRacerCnt p
		IF NOT IS_ENTITY_DEAD(Race.Racer[p].Driver)
			INT iAnim = GET_RANDOM_INT_IN_RANGE(1, 14)
			STRING sAnim
			IF p <> 0
				sAnim = PICK_STRING(iAnim = 1, "ig_2_gen_warmup_01", 
					PICK_STRING(iAnim = 2, "ig_2_gen_warmup_02",
					PICK_STRING(iAnim = 3, "ig_2_gen_warmup_03",
					PICK_STRING(iAnim = 4, "ig_2_gen_warmup_04",
					PICK_STRING(iAnim = 5, "ig_2_gen_warmup_05",
					PICK_STRING(iAnim = 6, "ig_2_gen_warmup_06",
					PICK_STRING(iAnim = 7, "ig_2_gen_warmup_07",
					PICK_STRING(iAnim = 8, "ig_2_gen_warmup_08",
					PICK_STRING(iAnim = 9, "ig_2_gen_warmup_09",
					PICK_STRING(iAnim = 10, "ig_2_gen_warmup_10",
					PICK_STRING(iAnim = 11, "ig_2_gen_warmup_11",
					PICK_STRING(iAnim = 12, "ig_2_gen_warmup_12", 
					"ig_2_gen_warmup_13"))))))))))))
					
				TASK_PLAY_ANIM(Race.Racer[p].Driver, "mini@triathlon", sAnim, INSTANT_BLEND_IN, default, default, AF_ABORT_ON_PED_MOVEMENT | AF_EXIT_AFTER_INTERRUPTED, 0.02)
			ELSE
				sAnim = "ig_2_gen_warmup_01"
				CLEAR_PED_TASKS_IMMEDIATELY(Race.Racer[p].Driver)
				IF IS_TRI_CONTROL_FLAG_SET(TCF_TRI_SKIPPED_CUTSCENE)
					TASK_PLAY_ANIM(Race.Racer[p].Driver, "mini@triathlon", sAnim, INSTANT_BLEND_IN, default, default, AF_ABORT_ON_PED_MOVEMENT | AF_EXIT_AFTER_INTERRUPTED, 0.15)
				ELSE
					TASK_PLAY_ANIM(Race.Racer[p].Driver, "mini@triathlon", sAnim, INSTANT_BLEND_IN, default, default, AF_ABORT_ON_PED_MOVEMENT | AF_EXIT_AFTER_INTERRUPTED, 0.25)
				ENDIF
			ENDIF
			FORCE_PED_AI_AND_ANIMATION_UPDATE(Race.Racer[p].Driver)
			CPRINTLN(DEBUG_TRIATHLON, "[tri_helpers.sch::HANDLE_TRI_COUNTDOWN_ANIMS] Setting iRacer[", p, "] with ", sAnim)
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Advance to the next stage in the cutscene.
PROC Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE eNewCutsceneStage)
	RESTART_TIMER_NOW(timerCutsceneCamera)
	eCurrentCutsceneStage = eNewCutsceneStage
ENDPROC


/// PURPOSE:
///    Destroy intro cutscene cameras.
PROC DESTROY_TRI_INTRO_CUTSCENE_CAMERAS()
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_INTRO_CUTSCENE_CAMERAS] Procedure started.")

	IF DOES_CAM_EXIST(camIntroSkyview_Start)
		DESTROY_CAM(camIntroSkyview_Start)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_INTRO_CUTSCENE_CAMERAS] Destroying camera: camIntroSkyview_Start.")
	ENDIF
	
	IF DOES_CAM_EXIST(camIntroSkyview_End)
		DESTROY_CAM(camIntroSkyview_End)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_INTRO_CUTSCENE_CAMERAS] Destroying camera: camIntroSkyview_End.")
	ENDIF
	
	IF DOES_CAM_EXIST(camIntroWater_Start)
		DESTROY_CAM(camIntroWater_Start)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_INTRO_CUTSCENE_CAMERAS] Destroying camera: camIntroWater_Start.")
	ENDIF
	
	IF DOES_CAM_EXIST(camIntroWater_End)
		DESTROY_CAM(camIntroWater_End)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_INTRO_CUTSCENE_CAMERAS] Destroying camera: camIntroWater_End.")
	ENDIF
	
	IF DOES_CAM_EXIST(camIntroRacer_Start)
		DESTROY_CAM(camIntroRacer_Start)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_INTRO_CUTSCENE_CAMERAS] Destroying camera: camIntroRacer_Start.")
	ENDIF
	
	IF DOES_CAM_EXIST(camIntroRacer_End)
		DESTROY_CAM(camIntroRacer_End)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_INTRO_CUTSCENE_CAMERAS] Destroying camera: camIntroRacer_End.")
	ENDIF
	
//	IF DOES_CAM_EXIST(camIntroIECam)
//		DESTROY_CAM(camIntroIECam)
//		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_INTRO_CUTSCENE_CAMERAS] Destroying camera: camIntroIECam.")
//	ENDIF
ENDPROC

PROC UPDATE_TRI_CHEERING_PEDS(TRI_RACE_STRUCT &thisRace, FLOAT minDelay = 1.0, FLOAT fMaxDelay = 3.0)
	IF NOT IS_TIMER_STARTED(cheerTimer)
		RESTART_TIMER_NOW(cheerTimer)
	ENDIF
	
	
	
	IF (IS_PED_INJURED(pedChattering) OR NOT IS_ANY_SPEECH_PLAYING(pedChattering)) AND TIMER_DO_WHEN_READY(cheerTimer, fCheeerDelay)
		// Find the range from the player we're going to grab a cheering ped from
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		VECTOR vCheeringRange = (<<1,1,1>>) * TRI_CHEERING_RANGE

		SET_SCENARIO_PEDS_TO_BE_RETURNED_BY_NEXT_COMMAND(TRUE)
		PED_INDEX pedCheering = GET_RANDOM_PED_AT_COORD(vPlayerPos, vCheeringRange, PEDTYPE_LAST_PEDTYPE)
		// Make sure the cheering ped isn't a racer
		IF DOES_ENTITY_EXIST(pedCheering) AND NOT IS_PED_INJURED(pedCheering) AND GET_ENTITY_MODEL(pedCheering) <> A_M_Y_RoadCyc_01
			STRING sVoice = PICK_STRING(IS_PED_MALE(pedCheering), "WAVELOAD_PAIN_MALE", "WAVELOAD_PAIN_FEMALE")
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedCheering, "WHOOP", sVoice, SPEECH_PARAMS_FORCE_NORMAL)
			fCheeerDelay = GET_RANDOM_FLOAT_IN_RANGE(minDelay, fMaxDelay)
			RESTART_TIMER_NOW(cheerTimer)
		ENDIF
	ENDIF
	
	IF VDIST2(GET_ENTITY_COORDS(thisRace.Racer[0].Driver), thisRace.sGate[thisRace.iGateCnt - 1].vPos) < 1000 AND NOT IS_TRI_CONTROL_FLAG_SET(TCF_BIG_CHEERS_PLAYED)
		PLAY_TRI_BIG_CHEERS(thisRace)
		SET_TRI_CONTROL_FLAG(TCF_BIG_CHEERS_PLAYED)
	ENDIF
ENDPROC

PROC HANDLE_TRI_INTRO_AUDIO(TRI_RACE_STRUCT &thisRace)
	IF NOT IS_TIMER_STARTED(diagTimer)
		RESTART_TIMER_NOW(diagTimer)
	ENDIF
	
	IF NOT IS_TIMER_STARTED(cheerTimer)
		RESTART_TIMER_NOW(cheerTimer)
	ENDIF
	
	//We want some level of cheering peds during countdown if we can
 	UPDATE_TRI_CHEERING_PEDS(thisRace, 4.0, 6.0)

	IF TIMER_DO_WHEN_READY(diagTimer, fDelay)
		STRING sVoice
		pedChattering = thisRace.Racer[GET_RANDOM_INT_IN_RANGE(1, 8)].Driver
		
		IF iChatterCount % 2 = 0
			IF GET_RANDOM_BOOL()
				sVoice = "A_M_Y_TRIATHLON_01_MINI_01"
			ELSE
				sVoice = "A_M_Y_TRIATHLON_01_MINI_02"
			ENDIF
		ELSE
			IF GET_RANDOM_BOOL()
				sVoice = "A_M_Y_TRIATHLON_01_MINI_03"
			ELSE
				sVoice = "A_M_Y_TRIATHLON_01_MINI_04"
			ENDIF
		ENDIF

		IF DOES_ENTITY_EXIST(pedChattering) AND NOT IS_PED_INJURED(pedChattering)
			iChatterCount++
			fDelay = GET_RANDOM_FLOAT_IN_RANGE(7.0, 10.0)
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedChattering, "TRIATHLON_WARMUP", sVoice, SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
			RESTART_TIMER_NOW(diagTimer)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
/// Goggles for Michael are 7
/// Goggles for Franklin are 2
/// Goggles for Trevor are 5, cap is 22(?), hair is 3 for cap.
PROC SETUP_TRI_PED_PROPS()
	
	
	CPRINTLN(DEBUG_TRIATHLON, "SETUP_TRI_PED_PROPS called")
	enumCharacterList eChar = GET_CURRENT_PLAYER_PED_ENUM()
	
	IF eChar = CHAR_MICHAEL
		SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_EYES, 7)
		SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, 22)
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 1, 0)
	ELIF eChar = CHAR_FRANKLIN
		SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_EYES, 2)
	ELIF eChar = CHAR_TREVOR
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 1, 0)
		SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, 22)
		SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_EYES, 5)
	ELSE
		CPRINTLN(DEBUG_TENNIS, "Player other than the main 3 is selected. No components set.")
	ENDIF
ENDPROC

/// Purpose:
///    Change the player's outfit to his Triathlon outfit.
PROC SET_PLAYER_TRI_OUTFIT()
	CPRINTLN(DEBUG_TRIATHLON, "[TriathlonSP.sc->SET_PLAYER_TRI_OUTFIT] Procedure started.")
	
	//Save all current props
	GET_PED_VARIATIONS(PLAYER_PED_ID(), Tri_Master.sPlayerVariation)
	STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), FALSE)
	Tri_Master.bVariationRestored = FALSE

	//B*1475521 - Clear Blood and Damage
	CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())	
	SET_ENTITY_HEALTH(PLAYER_PED_ID(),GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID()))
		
	// Store for Franklin, he keeps his hair.
	INT iHairDrawable, iHairTexture, iBerdDrawable, iBerdTexture
	iHairDrawable = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR)
	iHairTexture = GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR)
	iBerdDrawable = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD)
	iBerdTexture = GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD)
	
	// If the player came here with a helmet, store the hat, if any, underneath it.
//	GET_PED_HELMET_STORED_HAT_PROP_INDEX(PLAYER_PED_ID())
//	GET_PED_HELMET_STORED_HAT_TEX_INDEX(PLAYER_PED_ID())
	
	#IF IS_DEBUG_BUILD
		PRINT_PED_VARIATIONS(PLAYER_PED_ID())
	#ENDIF
	
	SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			KNOCK_OFF_PED_PROP(PLAYER_PED_ID(),TRUE,TRUE,TRUE,TRUE)
			CLEAR_PED_STORED_HAT_PROP(PLAYER_PED_ID())
			
			CPRINTLN(DEBUG_TRIATHLON, "[TriathlonSP.sc->SET_PLAYER_TRI_OUTFIT] Removing all player head props, if he has one on.")
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO,	13, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG,		12, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET,		8, 	0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 	iBerdDrawable,	iBerdTexture)	//(berd)
				
				CPRINTLN(DEBUG_TRIATHLON, "[TriathlonSP.sc->SET_PLAYER_TRI_OUTFIT] Triathlon outfit has been set for Michael.")
				CPRINTLN(DEBUG_TRIATHLON, "[TriathlonSP.sc->SET_PLAYER_TRI_OUTFIT] iBerdDrawable=", iBerdDrawable, ", iBerdTexture=", iBerdTexture)
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO,	10, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 		10, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 	4, 	0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 	iBerdDrawable,	iBerdTexture)	//(berd)
				
				CPRINTLN(DEBUG_TRIATHLON, "[TriathlonSP.sc->SET_PLAYER_TRI_OUTFIT] Triathlon outfit has been set for Trevor.")
				CPRINTLN(DEBUG_TRIATHLON, "[TriathlonSP.sc->SET_PLAYER_TRI_OUTFIT] iBerdDrawable=", iBerdDrawable, ", iBerdTexture=", iBerdTexture)
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO,	5, 	0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 		5, 	0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 	3, 	0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 	iHairDrawable, iHairTexture)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 	iBerdDrawable,	iBerdTexture)	//(berd)
				
				CPRINTLN(DEBUG_TRIATHLON, "[TriathlonSP.sc->SET_PLAYER_TRI_OUTFIT] Triathlon outfit has been set for Franklin.")
				CPRINTLN(DEBUG_TRIATHLON, "[TriathlonSP.sc->SET_PLAYER_TRI_OUTFIT] iHairDrawable=", iHairDrawable, ", iHairTexture=", iHairTexture)
				CPRINTLN(DEBUG_TRIATHLON, "[TriathlonSP.sc->SET_PLAYER_TRI_OUTFIT] iBerdDrawable=", iBerdDrawable, ", iBerdTexture=", iBerdTexture)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_WEARING_TRI_OUTFIT()
	IF IS_ENTITY_DEAD( PLAYER_PED_ID() )
		RETURN FALSE
	ENDIF
	
	PED_VARIATION_STRUCT sPlayerVariation
	GET_PED_VARIATIONS( PLAYER_PED_ID(), sPlayerVariation )
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			IF sPlayerVariation.iDrawableVariation[ENUM_TO_INT( PED_COMP_TORSO )] = 13
			AND sPlayerVariation.iDrawableVariation[ENUM_TO_INT( PED_COMP_LEG )] = 12
			AND sPlayerVariation.iDrawableVariation[ENUM_TO_INT( PED_COMP_FEET )] = 8
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CHAR_TREVOR
			IF sPlayerVariation.iDrawableVariation[ENUM_TO_INT( PED_COMP_TORSO )] = 10
			AND sPlayerVariation.iDrawableVariation[ENUM_TO_INT( PED_COMP_LEG )] = 10
			AND sPlayerVariation.iDrawableVariation[ENUM_TO_INT( PED_COMP_FEET )] = 4
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CHAR_FRANKLIN
			IF sPlayerVariation.iDrawableVariation[ENUM_TO_INT( PED_COMP_TORSO )] = 5
			AND sPlayerVariation.iDrawableVariation[ENUM_TO_INT( PED_COMP_LEG )] = 5
			AND sPlayerVariation.iDrawableVariation[ENUM_TO_INT( PED_COMP_FEET )] = 3
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC TASK_TRI_HELI(PED_INDEX &pedPilot, VEHICLE_INDEX &vehChopper)
	IF NOT IS_PED_INJURED(pedPilot) AND NOT IS_ENTITY_DEAD(vehChopper) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
//		TASK_HELI_CHASE(pedPilot, PLAYER_PED_ID(), (<<50, 50, 35>>))
//		TASK_HELI_MISSION(pedPilot, vehChopper, NULL, PLAYER_PED_ID(), (<<0.0,0.0,0.0>>), MISSION_PROTECT, 30.0, 25, -1.0, CEIL(35), CEIL(35))
		TASK_VEHICLE_HELI_PROTECT(pedPilot, vehChopper, PLAYER_PED_ID(), 30, DF_SteerAroundObjects, 25, 35)
		CPRINTLN(DEBUG_TRIATHLON, "TASK_TRI_HELI :: FLY_CHASE_HELI_FLY!")
	ENDIF
ENDPROC

/// PURPOSE:
///    Play the intro cutscene for the current race.
PROC Tri_Cutscenes_Play_Intro(VECTOR vScriptStartPos, TRI_RACE_STRUCT& Race, FLOAT fPlayerHeading, CAMERA_INDEX &camTriSky, VEHICLE_INDEX &vehChopper, PED_INDEX &pedPilot)

	SET_TRI_CONTROL_FLAG(TCF_TRI_CUTSCENE_PLAYING)
	UNUSED_PARAMETER(fPlayerHeading)
	HANDLE_TRI_INTRO_AUDIO(Race)
	FLOAT fCutTime
	
	VECTOR vHPLossStartPos, vHPLossEndPos, vHPLossStartRot, vHPLossEndRot
	
	// Cam 5
	IF eCurrentTriRace = TRIATHLON_RACE_ALAMO_SEA
		vHPLossStartPos	= <<2391.1677, 4283.4873, 32.2649>>//<<2391.0835, 4283.2949, 32.2649>>//<<2392.4084, 4282.7051, 33.8151>>
		vHPLossStartRot	= <<7.3105, 0.0009, -116.2334>>//<<8.0232, 0.0009, -109.3500>>//<<-14.6657, -0.0240, -100.1658>>
		vHPLossEndPos	= <<2391.8623, 4283.1465, 32.3712>>//<<2391.0835, 4283.2949, 32.2649>>//<<2390.7314, 4283.0068, 34.2611>>
		vHPLossEndRot	= <<7.7310, 0.0009, -115.5986>>//<<10.4965, 0.0009, -109.3500>>//<<-14.6657, -0.0240, -100.1658>>
	ELIF eCurrentTriRace = TRIATHLON_RACE_VESPUCCI
		vHPLossStartPos	= <<-1269.5, -2036.2, 3.1>> //<< -1273.2681, -2038.3073, 3.4913 >>
		vHPLossStartRot	= <<16.2, 0.0, -167.1 >>//<< 5.5019, -0.0000, -108.7474 >>
		vHPLossEndPos	= <<-1269.5, -2036.2, 3.1>>//<< -1277.3439, -2036.9229, 3.0768 >>
		vHPLossEndRot	= <<17.8, 0.0, -165.5>>//<< 5.5019, -0.0000, -108.7474 >>
	ENDIF

	// Check if the player has skipped the cutscene, and if not, check for input to skip.
	IF NOT IS_TRI_CONTROL_FLAG_SET(TCF_TRI_SKIPPED_CUTSCENE) AND eCurrentCutsceneStage > TRI_CUTSCENE_STAGE_SETUP
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			IF eCurrentCutsceneStage > TRI_CUTSCENE_STAGE_SKYVIEW_WAIT
				DEBUG_MESSAGE("[TRI_Triathlon_Cutscenes.sch->Tri_Cutscenes_Play_Intro] Player wants to skip cutscene.  Return to game camera.")
				SET_TRI_CONTROL_FLAG(TCF_TRI_SKIPPED_CUTSCENE)
				DEBUG_MESSAGE("[TRI_Triathlon_Cutscenes.sch->Tri_Cutscenes_Play_Intro] Cutscene skipped.")
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_CLEANUP
				
				IF DOES_ENTITY_EXIST(TRI_Master.oPencil)
					IF IS_ENTITY_ATTACHED(TRI_Master.oPencil)
						DETACH_ENTITY(TRI_Master.oPencil)
					ENDIF
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ELSE
				DO_SCREEN_FADE_OUT( 0 )
				DEBUG_MESSAGE("[TRI_Triathlon_Cutscenes.sch->Tri_Cutscenes_Play_Intro] Player wants to skip cutscene. Return to game camera." )
				SET_TRI_CONTROL_FLAG(TCF_TRI_SKIPPED_CUTSCENE)
				
				IF DOES_ENTITY_EXIST(TRI_Master.oPencil)
					DETACH_ENTITY(TRI_Master.oPencil)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_HELI_CREATE
			ENDIF
		ENDIF
	ENDIF

	SWITCH eCurrentCutsceneStage
	
		// Cutscene set up.
		CASE TRI_CUTSCENE_STAGE_SETUP
			// Set cameras early enough so they don't pop at the start.
			Tri_Cutscenes_Create_Cameras()
			
			IF IS_SCREEN_FADED_OUT() AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vScriptStartPos) > 1000
				// Load script start area.
				LOAD_SCENE(vScriptStartPos)
				VECTOR vPlayerPos
				vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				CPRINTLN(DEBUG_TRIATHLON, "LOAD_SCENE :: TRI_CUTSCENE_STAGE_SETUP, vScriptStartPos=", vScriptStartPos, ", playerPos=", vPlayerPos)
			ENDIF
		
			CLEAR_PRINTS()
			
			IF NOT IS_TIMER_STARTED(timerCutsceneCamera)
				START_TIMER_NOW(timerCutsceneCamera)
			ELSE
				RESTART_TIMER_NOW(timerCutsceneCamera)
			ENDIF
			
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->READY_PLAYER_CHARACTER_FOR_TRIATHLON] Unequipping possible player weapon.")
			
			//Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_HELI_CREATE)
			Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_SIGN_IN)
			
		BREAK	
		
		CASE TRI_CUTSCENE_STAGE_SIGN_IN
			IF GET_SYNCHRONIZED_SCENE_PHASE( iSynchSceneIntro ) >= 0.75 //0.74
				Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_SIGN_IN_WAIT)
				VECTOR vDest, vStart, vHeading
				FLOAT fHeading
				IF eCurrentTriRace = TRIATHLON_RACE_ALAMO_SEA
					vStart = <<2384.3933, 4317.6313, 49.1586>>//<<2338.2388, 4378.1123, 120.2957>>
					vDest = <<2383.0474, 4244.2207, 46.9973>>//<<2388.5806, 4256.2310, 31.5247>>
					vHeading = vDest - vStart
					fHeading = GET_HEADING_FROM_VECTOR_2D(vHeading.x, vHeading.y)
				ELIF eCurrentTriRace = TRIATHLON_RACE_VESPUCCI
					vStart = <<-1254.4238, -2008.6199, 20.4530>>//<<-1273.9961, -2022.5023, 30.7509>>//<<-1241.9432, -1941.1986, 84.1085>>//<<-1261.2136, -1971.7616, 57.7592>>//
					vDest = <<-1307.8572, -2062.9504, 14.3542>>//<<-1302.4484, -2058.4885, 16.6435>>//<<-1298.9327, -2063.5742, 5.5249>>//<<-1287.3, -2066.5, 10.7>> //
					fHeading = 148.1232
				ELIF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
					vStart = <<1612.5658, 3863.6152, 48.4078>>//<<1483.1023, 3768.6030, 138.9427>>
					vDest = <<1551.9393, 3818.1523, 49.4007>>//<<1593.7911, 3847.5664, 32.1028>>
					vHeading = vDest - vStart
					fHeading = GET_HEADING_FROM_VECTOR_2D(vHeading.x, vHeading.y)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedPilot) AND NOT IS_ENTITY_DEAD(vehChopper)
					SET_ENTITY_COORDS(vehChopper, vStart)
					SET_ENTITY_HEADING(vehChopper, fHeading)
					TASK_HELI_MISSION(pedPilot, vehChopper, NULL, NULL, vDest, MISSION_GOTO, 30.0, 3.0, fHeading, 1, 1)
					CPRINTLN(DEBUG_TRIATHLON, "vehChopper tasked in intro")
				ENDIF
			ENDIF
			//eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_SIGN_IN_WAIT
		BREAK
		
		CASE TRI_CUTSCENE_STAGE_SIGN_IN_WAIT
			IF GET_SYNCHRONIZED_SCENE_PHASE( iSynchSceneIntro ) >= 1.0 //0.74
				Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_HELI_CREATE)
			
			ENDIF
			
		BREAK
	
		CASE TRI_CUTSCENE_STAGE_HELI_CREATE

				
			IF NOT IS_ENTITY_DEAD(Tri_Master.pedTableGuy)
				TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(Tri_Master.pedTableGuy, GET_ENTITY_COORDS(Tri_Master.pedTableGuy), 2.0)
			ENDIF
			IF NOT TRI_Master.bOutfitChanged
			
				// Set the racer outfits.
				SET_PLAYER_TRI_OUTFIT()
			
				// Set up goggles and swim caps for the main player characters
				SETUP_TRI_PED_PROPS()
				TRI_Master.bOutfitChanged = TRUE
			ENDIF
			
			IF NOT IS_TRI_CONTROL_FLAG_SET( TCF_TRI_SKIPPED_CUTSCENE )
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_HELI_WAIT
			ELSE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), Race.Racer[0].vStartPos )
					SET_ENTITY_HEADING( PLAYER_PED_ID(), Race.Racer[0].fStartHead )
				ENDIF
				Tri_Cutscenes_Play_Racer_Intro_Anims(Race)
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_CLEANUP
			ENDIF
		BREAK
		
		CASE TRI_CUTSCENE_STAGE_HELI_WAIT
			FLOAT fTime
			VECTOR vPos, vLook
			//INT iDuration
			
			IF eCurrentTriRace = TRIATHLON_RACE_ALAMO_SEA
				fTime = 0.0//7.98
				vPos = <<2407.5278, 4280.5791, 51.4604>>//<<2429.2104, 4284.1646, 45.1134>>
				vLook = <<10.5419, 0.0000, 91.9416>>//<<-11.8552, -0.0000, 115.8075>>
				//iDuration = 0//3215//5000
			ELIF eCurrentTriRace = TRIATHLON_RACE_VESPUCCI
				fTime = 0.0//6.43
				vPos = <<-1261.8286, -2046.9830, 18.0698>>//<<-1260.5, -2048.5, 15.4>> //<<-1260.4017, -2048.6079, 16.1657>>
				vLook = <<9.3682, 0.0163, 45.8717>>//<<16.4, -0.0, 45.2>>//<<-14.0170, -0.0000, 72.6713>>
				//iDuration = 0//3215//5000
			ELIF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
				fTime = 0.0
				vPos = <<1590.8025, 3819.3125, 45.5862>>//<<1605.2433, 3822.2114, 53.5891>>
				vLook = <<14.8126, -0.0000, 36.9737>>//<<-31.9753, 0.0000, 57.0484>>
				//iDuration = 0//4000
			ENDIF
			
			IF eCurrentTriRace <> TRIATHLON_RACE_IRONMAN
				PRINT_HELP("TRI_INTRO_GOAL")
					//CLEAR_THIS_PRINT("TRI_INTRO_GOAL")
				//PRINT_HELP("TRI_INTRO_LEGS")
			ENDIF
			
			IF GET_TIMER_IN_SECONDS(timerCutsceneCamera) > fTime
				// Play racer animations during the intro.
				IF DOES_CAM_EXIST(camIntroIECam)
					DESTROY_CAM(camIntroIECam)
				ENDIF
				camIntroIECam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vPos, vLook, 50.0)
				SET_CAM_ACTIVE( camIntroIECam, TRUE )
				IF DOES_ENTITY_EXIST(TRI_Master.oPencil)
					DETACH_ENTITY(TRI_Master.oPencil)
				ENDIF
				//SET_CAM_PARAMS(camTriSky, vPos, vLook, 50, iDuration)
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_HELI_HOVER
			ELIF IS_TRI_CONTROL_FLAG_SET(TCF_TRI_SKIPPED_CUTSCENE)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), Race.Racer[0].vStartPos )
					SET_ENTITY_HEADING( PLAYER_PED_ID(), Race.Racer[0].fStartHead )
				ENDIF
				Tri_Cutscenes_Play_Racer_Intro_Anims(Race)
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_CLEANUP
			ENDIF
		BREAK
		
		CASE TRI_CUTSCENE_STAGE_HELI_HOVER			
			IF eCurrentTriRace = TRIATHLON_RACE_ALAMO_SEA
				fTime = 0.0//12.65
			ELIF eCurrentTriRace = TRIATHLON_RACE_VESPUCCI
				fTime = 0.0//12.61
			ELIF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
				fTime = 0.0//14.12
			ENDIF
			IF GET_TIMER_IN_SECONDS(timerCutsceneCamera) > fTime
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), Race.Racer[0].vStartPos )
					SET_ENTITY_HEADING( PLAYER_PED_ID(), Race.Racer[0].fStartHead )
				ENDIF
				Tri_Cutscenes_Play_Racer_Intro_Anims(Race)
				
				IF NOT IS_PED_INJURED(pedPilot) AND NOT IS_ENTITY_DEAD(vehChopper)
//					IF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
//						TASK_HELI_MISSION(pedPilot, vehChopper, NULL, NULL, GET_ENTITY_COORDS(vehChopper), MISSION_GOTO, 30.0, 3.0, GET_ENTITY_HEADING(PLAYER_PED_ID()), 1, 1)
//					ENDIF

					Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_SKYVIEW)
					CPRINTLN(DEBUG_TRIATHLON, "vehChopper tasked to hover")
				ENDIF
			ELIF IS_TRI_CONTROL_FLAG_SET(TCF_TRI_SKIPPED_CUTSCENE)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), Race.Racer[0].vStartPos )
					SET_ENTITY_HEADING( PLAYER_PED_ID(), Race.Racer[0].fStartHead )
				ENDIF
				Tri_Cutscenes_Play_Racer_Intro_Anims(Race)
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_CLEANUP
			ENDIF
		BREAK		
		
		// Show skyview of race.
		CASE TRI_CUTSCENE_STAGE_SKYVIEW
			IF DOES_CAM_EXIST(camTriSky)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				IF IS_TRI_CONTROL_FLAG_SET(TCF_TRI_SKIPPED_CUTSCENE)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), Race.Racer[0].vStartPos )
						SET_ENTITY_HEADING( PLAYER_PED_ID(), Race.Racer[0].fStartHead )
					ENDIF
					Tri_Cutscenes_Play_Racer_Intro_Anims(Race)
					eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_CLEANUP
				ELSE
					IF eCurrentTriRace <> TRIATHLON_RACE_IRONMAN
						SET_CAM_ACTIVE_WITH_INTERP(camIntroSkyview_End, camIntroIECam, 6215)
					ELSE
						SET_CAM_ACTIVE_WITH_INTERP(camIntroSkyview_End, camIntroIECam, 8125)
					ENDIF
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_SKYVIEW_HELP
				ENDIF
	
			ELSE
				CLEAR_TRI_CONTROL_FLAG(TCF_TRI_CUTSCENE_PLAYING)
			ENDIF
		BREAK
		
		CASE TRI_CUTSCENE_STAGE_SKYVIEW_HELP
			SET_TRI_INTRO_ANIM_SPEEDS(Race)
			IF eCurrentTriRace <> TRIATHLON_RACE_IRONMAN
				IF (GET_TIMER_IN_SECONDS(timerCutsceneCamera) > 6.215)//9.0 )
					Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_WATER)
				ENDIF
			ELSE
				IF GET_TIMER_IN_SECONDS(timerCutsceneCamera) > 8.125//9.0
					Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_WATER)
				ENDIF
			ENDIF
			IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
				IF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
					PRINT_HELP("TRI_INTRO_IM1")
				ENDIF
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_SKYVIEW_WAIT
			ENDIF
			IF IS_TRI_CONTROL_FLAG_SET(TCF_TRI_SKIPPED_CUTSCENE)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), Race.Racer[0].vStartPos )
					SET_ENTITY_HEADING( PLAYER_PED_ID(), Race.Racer[0].fStartHead )
				ENDIF
				CLEAR_HELP()
				Tri_Cutscenes_Play_Racer_Intro_Anims(Race)
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_CLEANUP
			ENDIF
		BREAK
		
		// Wait for skyview to complete.
		CASE TRI_CUTSCENE_STAGE_SKYVIEW_WAIT
			IF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
				fCutTime = 8.125//9.0
			ELSE
				fCutTime = 6.125
			ENDIF
			IF (GET_TIMER_IN_SECONDS(timerCutsceneCamera) > fCutTime )
				Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_WATER)
			ENDIF
			IF IS_TRI_CONTROL_FLAG_SET(TCF_TRI_SKIPPED_CUTSCENE)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), Race.Racer[0].vStartPos )
					SET_ENTITY_HEADING( PLAYER_PED_ID(), Race.Racer[0].fStartHead )
				ENDIF
				CLEAR_HELP()
				Tri_Cutscenes_Play_Racer_Intro_Anims(Race)
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_CLEANUP
			ENDIF
		BREAK
		
		
		// Show water.
		CASE TRI_CUTSCENE_STAGE_WATER
			IF DOES_CAM_EXIST(camIntroWater_Start)
				
				//Cam 2
				IF eCurrentTriRace = TRIATHLON_RACE_ALAMO_SEA
					SET_CAM_ACTIVE_WITH_INTERP(camIntroWater_End, camIntroWater_Start, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				ELIF eCurrentTriRace = TRIATHLON_RACE_VESPUCCI
					SET_CAM_ACTIVE_WITH_INTERP(camIntroWater_End, camIntroWater_Start, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				ELSE 
					SET_CAM_ACTIVE_WITH_INTERP(camIntroWater_End, camIntroWater_Start, 6000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehChopper)
					IF eCurrentTriRace = TRIATHLON_RACE_ALAMO_SEA
						SET_ENTITY_COORDS(vehChopper, <<2386.2920, 4232.5552, 36.9120>>)
					ELIF eCurrentTriRace = TRIATHLON_RACE_VESPUCCI
						SET_ENTITY_COORDS(vehChopper, <<-1306.7081, -2067.3142, 13.6222>>)
//					ELIF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
//						SET_ENTITY_COORDS(vehChopper, <<-1269.0126, -2041.7416, 3.0650>>)
					ENDIF				
				ENDIF
				
				IF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
					CLEAR_THIS_PRINT("TRI_INTRO_IM2")
					PRINT_HELP("TRI_INTRO_IM2")
				ELSE
					CLEAR_THIS_PRINT("TRI_INTRO_GOAL")
					IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						PRINT_HELP("TRI_INTNS_KM")
					ELSE
						PRINT_HELP("TRI_INTNS")
					ENDIF
				ENDIF
				
				IF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
					eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_WATER_WAIT
				ELSE
					eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_WATER_WAIT_HELI
				ENDIF
				
			ELSE
				CLEAR_TRI_CONTROL_FLAG(TCF_TRI_CUTSCENE_PLAYING)
			ENDIF
		BREAK
		
		CASE TRI_CUTSCENE_STAGE_WATER_WAIT_HELI
			IF GET_TIMER_IN_SECONDS(timerCutsceneCamera) > 1.5
				FLOAT fHeliHeading
				VECTOR vChopperPos
				IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(vehChopper)
					vChopperPos = GET_ENTITY_COORDS(vehChopper)
					fHeliHeading = GET_HEADING_BETWEEN_VECTORS(vChopperPos, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				ENDIF
				IF NOT IS_PED_INJURED(pedPilot) AND NOT IS_ENTITY_DEAD(vehChopper)
					TASK_HELI_MISSION(pedPilot, vehChopper, NULL, NULL, vChopperPos, MISSION_GOTO, 30.0, 3.0, fHeliHeading, 1, 1)
				ENDIF
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_WATER_WAIT
			ENDIF
		BREAK
		
		// Wait for water cam to complete.
		CASE TRI_CUTSCENE_STAGE_WATER_WAIT
			IF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
				fCutTime = 6.0
			ELSE
				fCutTime = 5.0
			ENDIF
			IF (GET_TIMER_IN_SECONDS(timerCutsceneCamera) > fCutTime )
				IF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
					IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
						TASK_PLAY_ANIM(PLAYER_PED_ID(), szTriAnimDicts[2], "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0.5)
					ENDIF
					Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_RACER)
				ELSE
					IF DOES_CAM_EXIST(camIntroSkyview_Start)
						DESTROY_CAM(camIntroSkyview_Start)
					ENDIF
					IF DOES_CAM_EXIST(camIntroIECam)
						DESTROY_CAM(camIntroIECam)
					ENDIF
					// Cam 3 heli cam
					SHAKE_SCRIPT_GLOBAL( "WOBBLY_SHAKE", 0.0002 )
					camIntroSkyview_Start = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vIntensityPosStart, vIntensityRotStart, 35.0)
					camIntroIECam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vIntensityPosEnd, vIntensityRotEnd, 35.0)
					Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_INTENSITY)
				ENDIF
			ENDIF
		BREAK
		
		
		//Show Intensiy help and set the cam to lerp sideways a little
		CASE TRI_CUTSCENE_STAGE_INTENSITY
			IF DOES_CAM_EXIST(camIntroIECam) AND DOES_CAM_EXIST( camIntroSkyview_Start )
				SET_CAM_ACTIVE_WITH_INTERP(camIntroIECam, camIntroSkyview_Start, 6100, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				
				CLEAR_THIS_PRINT("TRI_INTNS")
				IF IS_PC_VERSION()
					IF IS_THIS_PRINT_BEING_DISPLAYED( "TRI_INTNS_KM")
						CLEAR_THIS_PRINT("TRI_INTNS_KM")
					ENDIF
				ENDIF
				PRINT_HELP("TRI_NRG_INTRO")
				
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_INTENSITY_WAIT
				Race.Racer[0].iRank = 1
				//TRI_Race_Draw_Hud(Race)
			ENDIF
		BREAK
		
		//Wait for Intensity cam to complete
		CASE TRI_CUTSCENE_STAGE_INTENSITY_WAIT
			IF (GET_TIMER_IN_SECONDS(timerCutsceneCamera) > 6.0 )
				IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
					TASK_PLAY_ANIM(PLAYER_PED_ID(), szTriAnimDicts[2], "idle_d", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0.05)
					
				ENDIF
				
				IF DOES_CAM_EXIST(camIntroSkyview_Start)
					DESTROY_CAM(camIntroSkyview_Start)
				ENDIF
				IF DOES_CAM_EXIST(camIntroSkyview_End)
					DESTROY_CAM(camIntroSkyview_End)
				ENDIF
				// Cam 4
				camIntroSkyview_End = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vEnergyPosEnd, vEnergyRotEnd, 20.7)
				camIntroSkyview_Start = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vEnergyPosStart, vEnergyRotStart, 20.7)
				Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_ENERGY)
			ENDIF
			//TRI_Race_Draw_Hud(Race)
		BREAK
		
		
		//Show Energy help and set the cam to lerp backwards a little
		CASE TRI_CUTSCENE_STAGE_ENERGY
			IF DOES_CAM_EXIST(camIntroIECam)
				SET_CAM_ACTIVE_WITH_INTERP(camIntroSkyview_End, camIntroSkyview_Start, 4100, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR )
				
				//SET_CAM_PARAMS(camIntroIECam, vEnergyPosEnd, vEnergyRotEnd, 50.0, 9000)
				CLEAR_THIS_PRINT("TRI_NRG_INTRO")
				PRINT_HELP("TRI_HP_LOSS")
				
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_ENERGY_WAIT
				TRI_Race_Draw_Hud(Race)
			ENDIF
		BREAK
		
		//Wait for Energy cam to complete
		CASE TRI_CUTSCENE_STAGE_ENERGY_WAIT
			STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
		
			IF (GET_TIMER_IN_SECONDS(timerCutsceneCamera) > 4.1 )
				IF DOES_CAM_EXIST(camIntroSkyview_Start)
					DESTROY_CAM(camIntroSkyview_Start)
				ENDIF
				// Cam 5
				camIntroSkyview_Start = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vHPLossStartPos, vHPLossStartRot, 20.7)
				Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_HP_LOSS)
			ENDIF
			TRI_Race_Draw_Hud(Race)
		BREAK
		
		CASE TRI_CUTSCENE_STAGE_HP_LOSS
			IF DOES_CAM_EXIST(camIntroIECam)
				CPRINTLN(DEBUG_TRIATHLON, "vStartPos ", vHPLossStartPos, ", vStartRot ", vHPLossStartRot)
				CPRINTLN(DEBUG_TRIATHLON, "vEndPos ", vHPLossEndPos, ", vEndRot ", vHPLossEndRot)
				SET_CAM_PARAMS(camIntroIECam, vHPLossEndPos, vHPLossEndRot, 20.7)
				SET_CAM_ACTIVE_WITH_INTERP(camIntroIECam, camIntroSkyview_Start, 4000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR )			
				CLEAR_THIS_PRINT("TRI_HP_LOSS")
	
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_HP_LOSS_WAIT
			ENDIF
			TRI_Race_Draw_Hud(Race)			
		BREAK
		
		CASE TRI_CUTSCENE_STAGE_HP_LOSS_WAIT
			IF (GET_TIMER_IN_SECONDS(timerCutsceneCamera) > 4.0 )
				Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_CLEANUP)
			ENDIF
			TRI_Race_Draw_Hud(Race)
		BREAK
		
		
		// Show finish line.
		CASE TRI_CUTSCENE_STAGE_RACER
			IF DOES_CAM_EXIST(camIntroRacer_Start)
			
				IF eCurrentTriRace = TRIATHLON_RACE_ALAMO_SEA
					SET_CAM_ACTIVE_WITH_INTERP(camIntroRacer_End, camIntroRacer_Start, 4000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				ELIF eCurrentTriRace = TRIATHLON_RACE_VESPUCCI
					SET_CAM_ACTIVE_WITH_INTERP(camIntroRacer_End, camIntroRacer_Start, 4000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				ELSE 
					SET_CAM_ACTIVE_WITH_INTERP(camIntroRacer_End, camIntroRacer_Start, 6000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				ENDIF
				
				IF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
					CLEAR_THIS_PRINT("TRI_INTRO_IM2")
					PRINT_HELP("TRI_INTRO_IM3")

				ENDIF
				
				eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_RACER_WAIT
				
			ELSE
				CLEAR_TRI_CONTROL_FLAG(TCF_TRI_CUTSCENE_PLAYING)
			ENDIF
		BREAK
		
		// Wait for finish line cam to complete.
		CASE TRI_CUTSCENE_STAGE_RACER_WAIT
			IF (GET_TIMER_IN_SECONDS(timerCutsceneCamera) > 6.0 )
				Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_CLEANUP)
//				SET_CAM_ACTIVE(camIntroRacer_Start, TRUE)
//				IF eCurrentTriRace = TRIATHLON_RACE_ALAMO_SEA
//					SET_CAM_PARAMS(camIntroRacer_Start, (<<2396.6938, 4290.7183, 32.7631>>), (<<-4.6996, -0.0000, 162.4670>>), 50, 0)
//					SET_CAM_PARAMS(camIntroRacer_Start, (<<2397.2419, 4290.5449, 32.7631>>), (<<-4.6996, -0.0000, 162.4670>>), 50, 4000)
//					CPRINTLN(DEBUG_TRIATHLON, "TRIATHLON_RACE_ALAMO_SEA, cam params for final")
//				ELIF eCurrentTriRace = TRIATHLON_RACE_VESPUCCI
//					SET_CAM_PARAMS(camIntroRacer_Start, (<<-1261.9166, -2035.0470, 4.5939>>), (<<-10.4041, 0.0000, 106.0351>>), 50, 0)
//					SET_CAM_PARAMS(camIntroRacer_Start, (<<-1261.6249, -2036.0605, 4.5939>>), (<<-10.4041, 0.0000, 106.0351>>), 50, 4000)
//					CPRINTLN(DEBUG_TRIATHLON, "TRIATHLON_RACE_VESPUCCI, cam params for final")
//				ELIF eCurrentTriRace = TRIATHLON_RACE_IRONMAN
//					SET_CAM_PARAMS(camIntroRacer_Start, (<<1583.9034, 3822.2495, 35.5762>>), (<<-19.7925, 0.0000, 12.9743>>), 50, 0)
//					SET_CAM_PARAMS(camIntroRacer_Start, (<<1585.5526, 3824.0723, 35.5680>>), (<<-19.7925, 0.0000, 12.9743>>), 50, 4000)
//					CPRINTLN(DEBUG_TRIATHLON, "TRIATHLON_RACE_IRONMAN, cam params for final")
//				ENDIF
			ENDIF
		BREAK
		
		
		// Return to game camera.
		CASE TRI_CUTSCENE_STAGE_GAME_CAM
//			RENDER_SCRIPT_CAMS(FALSE, TRUE)

			eCurrentCutsceneStage = TRI_CUTSCENE_STAGE_GAME_CAM_WAIT
		BREAK
		
		// Wait for game camera to finish setting.
		CASE TRI_CUTSCENE_STAGE_GAME_CAM_WAIT
			IF (GET_TIMER_IN_SECONDS(timerCutsceneCamera) > 4.0 )
				Tri_Cutscenes_Update_Current_Stage_And_Restart_Timer(TRI_CUTSCENE_STAGE_CLEANUP)
			ENDIF
		BREAK
		
		
		// Clean up cutscene cameras.
		CASE TRI_CUTSCENE_STAGE_CLEANUP
			IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID()) AND IS_PLAYER_WEARING_TRI_OUTFIT()
				CLEAR_HELP()
				DISPLAY_RADAR(TRUE)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(900)
				ENDIF
				
				//RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_CAM_ACTIVE_WITH_INTERP( camIntroCatchupCam2, camIntroCatchupCam1, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR )
					
				// Stop playing racer animations.
				Tri_Cutscenes_Setup_Racer_Intro_Anims(Race)
				
				TASK_TRI_HELI(pedPilot, vehChopper)
				
				// The player's startup animation may change his heading, so ensure he's facing the race course once the cutscene ends.
	//			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
	//				IF GET_ENTITY_HEADING(Race.Racer[0].Driver) <> fPlayerHeading
	//					SET_ENTITY_HEADING(Race.Racer[0].Driver, fPlayerHeading)
	//					CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->Tri_Cutscenes_Play_Intro] Player heading differs from original heading.  Setting player heading to: ", fPlayerHeading)
	//				ELSE
	//					CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->Tri_Cutscenes_Play_Intro] Player heading is the same as original heading: ", fPlayerHeading)
	//				ENDIF
	//			ENDIF
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				
				CLEAR_TRI_CONTROL_FLAG(TCF_TRI_SKIPPED_CUTSCENE)
				CLEAR_TRI_CONTROL_FLAG(TCF_TRI_CUTSCENE_PLAYING)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_TRIATHLON, "GET_TIMER_IN_SECONDS(", GET_TIMER_IN_SECONDS(timerCutsceneCamera), ")")
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN) OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
			CPRINTLN(DEBUG_TRIATHLON, "camera tag")
		ENDIF
	#ENDIF

ENDPROC

// ====================================================
// 	E N D  TRIATHLON CUTSCENE FUNCTIONS AND PROCEDURES
// ====================================================

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
// 		END OF FILE - DO NOT ADD ANYTHING BELOW THIS BLOCK!
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************









