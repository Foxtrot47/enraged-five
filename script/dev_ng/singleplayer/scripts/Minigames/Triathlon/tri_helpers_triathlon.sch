// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	TRI_Triathlon_Helpers.sch
//		AUTHOR			:	Carlos Mijares (CM)
//		DESCRIPTION		:	General support functionality for Triathlon races.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************





// =====================================
// 	FILE INCLUDES
// =====================================

// -------------------------------------
// GENERAL INCLUDES
// -------------------------------------
USING "globals.sch"
USING "minigames_helpers.sch"
USING "minigame_uiinputs.sch"
USING "commands_xml.sch"
USING "commands_water.sch"
USING "chase_hint_cam.sch"
USING "lineactivation.sch"
USING "building_control_public.sch"
USING "shared_hud_displays.sch"
USING "stats_private.sch"
USING "Overlay_Effects.sch"
USING "tri_head.sch"

// =====================================
// 	E N D  FILE INCLUDES
// =====================================


FLOAT fOnTribikeTimer
INT iCurrentText = 0
CONST_FLOAT	ON_TRIBIKE_THRESHOLD	5.0
CONST_FLOAT	MIN_GLARING_DISTANCE	5.0

BOOL bCatchupDone = FALSE
BOOL bDidCatchupAlready = FALSE









// ===================================
// 	TRIATHLON HELPER VARIABLES
// ===================================

ENUM TRI_PLAYED_MUSIC_CUES_FLAGS
	TPMC_LAST_SWIM 		= BIT0,
	TPMC_LAST_CYCLE		= BIT1,
	TPMC_LAST_FOOT		= BIT3
ENDENUM

// Current Triathlon race.
TRIATHLON_RACE_INDEX eCurrentTriRace
// Keep the last vehicle the player was on when he started the race.
VEHICLE_INDEX vehPlayerDroveBeforeRace

ENUM TRI_TRI_RACE_LEG
	TRI_TRI_RACE_LEG_SWIM,
	TRI_TRI_RACE_LEG_BIKE,
	TRI_TRI_RACE_LEG_RUN,
	TRI_TRI_RACE_LEG_ERROR
ENDENUM

//// Tracks when leg-specific blip sprites are set.
BOOL bIsSwimBlipSpriteSet
BOOL bIsBikeBlipSpriteSet
BOOL bIsRunBlipSpriteSet

// -------------------------------------
//  BIKE VARIABLES
// -------------------------------------

// Blip variables
BLIP_INDEX blipBikes
BLIP_INDEX blipBikesEdge

// Tracks the number of racers that have already mounted bikes.
INT iNumOfRacersThatHaveMountedBikes

// Determine when the player will begin to slowdown for a smooth bike->to-run transition.
VECTOR vSlowDownPlayerBikeAtThisPos
BOOL bIsPlayerBikeSpeedSlowedForTransitionSet

// -------------------------------------
//  PLAYER RANK VARIABLES
// -------------------------------------

// Track the player's current and previous ranks.
INT iPlayerPreviousRank
INT iPlayerCurrentRank
INT iPlayerInPlaceRank


// -------------------------------------
//  PLAYER STAMINA VARIABLES
// -------------------------------------

// Stamina enums.
ENUM PLAYER_STAMINA_RESET_ENUM
	PLAYER_STAMINA_WAIT_FOR_SWIM_BIKE_GATE,
	PLAYER_STAMINA_RESET_STAMINA_AT_SWIM_BIKE_GATE,
	PLAYER_STAMINA_WAIT_FOR_BIKE_RUN_GATE,
	PLAYER_STAMINA_RESET_STAMINA_AT_BIKE_RUN_GATE,
	PLAYER_STAMINA_WAIT_FOR_LAST_SPURT_GATE,
	PLAYER_STAMINA_RESET_STAMINA_AT_LAST_SPURT_GATE,
	PLAYER_STAMINA_RESET_DONE
ENDENUM

PLAYER_STAMINA_RESET_ENUM ePlayerStaminaResetState


// -------------------------------------
//  PLAYER SPEED AND ENERGY VARIABLES
// -------------------------------------

STRUCT TRI_PLAYER_STAMINA_STRUCT
	INT iCurrentEnergy
	INT iMaxEnergy
	HUD_COLOURS hcolorEnergy
	
//	INT iCurrentIntensity
//	INT iMaxIntensity
//	HUD_COLOURS hColorIntensity
	
	structTimer timerSprintButtonLastPressed
	structTimer timerBeforeIntensityMeterCooldown
	structTimer timerBeforeReplenishingEmptyEnergyMeter
	
	BOOL bIsFastMovementDisabled
ENDSTRUCT

TRI_PLAYER_STAMINA_STRUCT Tri_Player_Stamina

// Player swim state enum.
ENUM TRI_PLAYER_MOVE_SWIM_STATE
	TRI_PLAYER_MOVE_SWIM_STATE_STILL,
	TRI_PLAYER_MOVE_SWIM_STATE_SLOW,
	TRI_PLAYER_MOVE_SWIM_STATE_NORMAL,
	TRI_PLAYER_MOVE_SWIM_STATE_FAST,
	TRI_PLAYER_MOVE_SWIM_STATE_NOT_IN_WATER
ENDENUM

// Player bike state enum.
ENUM TRI_PLAYER_MOVE_BIKE_STATE
	TRI_PLAYER_MOVE_BIKE_STATE_NOT_FAST,	// Naming this NOT_FAST because it accounts for any other state besides fast: slow, still, etc.
	TRI_PLAYER_MOVE_BIKE_STATE_FAST,
	TRI_PLAYER_MOVE_BIKE_STATE_NOT_IN_BIKE
ENDENUM

// Speed meter state.
ENUM TRI_PLAYER_INTENSITY_METER_STATE
	TRI_PLAYER_INTENSITY_METER_STATE_EMPTY,
	TRI_PLAYER_INTENSITY_METER_STATE_GREEN,
	TRI_PLAYER_INTENSITY_METER_STATE_YELLOW,
	TRI_PLAYER_INTENSITY_METER_STATE_RED,
	TRI_PLAYER_INTENSITY_METER_STATE_FULL
ENDENUM

// Energy meter state.
ENUM TRI_PLAYER_ENERGY_METER_STATE
	TRI_PLAYER_ENERGY_METER_STATE_FULL,
	TRI_PLAYER_ENERGY_METER_STATE_DRAIN,
	TRI_PLAYER_ENERGY_METER_STATE_EMPTY,
	TRI_PLAYER_ENERGY_METER_STATE_REPLENISH
ENDENUM

//TRI_PLAYER_ENERGY_METER_STATE eCurrentPlayerEnergyMeterState





// -------------------------------------
//  PLAYER EVENT VARIABLES
// -------------------------------------

// Timers to track how long the player has remained in a race rank.
structTimer timerPlayerRemainingInPlace
structTimer timerPlayerRemainingInFirstPlace
structTimer timerPlayerRemainingInSecondPlace
structTimer timerPlayerRemainingInLastPlace





// -------------------------------------
//  TUTORIAL VARIABLES
// -------------------------------------

INT iShowSwimHelpAtThisGate
INT iShowBikeHelpAtThisGate
INT iStopShowBikeHelpAtThisGate
INT iShowRunHelpAtThisGate
INT iShowTipHelpAtThisGate
INT iShowCamHelpAtThisGate

BOOL bHasSwimHintPlayed
BOOL bHasBikeHintPlayed
BOOL bHasRunHintPlayed
BOOL bHasTipHintPlayed_0
BOOL bHasTipHintPlayed_1
BOOL bHasCamHintPlayed

BOOL bIsTVCamHintSetToBeCleared

structTimer timerBeforeShowingHint
structTimer timerBeforeRemovingHint





// -------------------------------------
//  CAMERA VARIABLES
// -------------------------------------

// Custom cams.
CAMERA_INDEX camTV_Top_Medium
//CAMERA_INDEX camTV_Top_Far
//CAMERA_INDEX camTV_Behind_Close
CAMERA_INDEX camPlayerFinishedRace
CAMERA_INDEX camEndOfRace_Start
CAMERA_INDEX camEndOfRace_End


// TV cam enums.
//ENUM TV_CAM_ANGLE_ENUM
//	TV_CAM_ANGLE_TOP_MEDIUM_RANGE,
//	TV_CAM_ANGLE_TOP_FAR_RANGE,
//	TV_CAM_ANGLE_BEHIND_FAR_RANGE
//ENDENUM

//TV_CAM_ANGLE_ENUM eCurrentTVCamAngle

// Cam timers.
structTimer timerShowPlayerFinishedRace

// Cam bools.
BOOL bTVCam_Has_TV_Cam_Stopped
BOOL bFinishedLookingAtPlayer

// TV cam HUD stuff.
SCALEFORM_INDEX scformTVCam

STRING szTVNews_Title 		= "TRI_NEWS_00"
STRING szTVNews_Headline1 	= "TRI_NEWS_01"
STRING szTVNews_Headline2 	= "TRI_NEWS_02"
STRING szTVNews_Headline3 	= "TRI_NEWS_03"
STRING szTVNews_Headline4 	= "TRI_NEWS_04"
STRING szTVNews_Headline5 	= "TRI_NEWS_05"
STRING szTVNews_Headline6 	= "TRI_NEWS_06"
STRING szTVNews_Headline7 	= "TRI_NEWS_07"

// ===================================
// 	E N D  TRIATHLON HELPER VARIABLES
// ===================================










// ==================================================
// 	TRIATHLON HELPER FUNCTIONS AND PROCEDURES
// ==================================================

/// PURPOSE:
///    
/// PARAMS:
///    race - 
/// RETURNS:
///    TRUE when glaring at a racer, false otherwise
FUNC BOOL UPDATE_TRI_PLAYER_GLARING_AT_OTHER_RACERS(TRI_RACE_STRUCT &race)
	IF race.sGate[race.Racer[0].iGateCur].eChkpntType <> TRI_CHKPT_TRI_BIKE
	OR GET_SCRIPT_TASK_STATUS(race.Racer[0].Driver, SCRIPT_TASK_ANY) = PERFORMING_TASK
//		CPRINTLN(DEBUG_TRIATHLON, "[tri_helpers_triathlon.sch::UPDATE_TRI_PLAYER_GLARING_AT_OTHER_RACERS] returning false, player ped is tasked OR we're not biking yet")
		RETURN FALSE
	ENDIF
	
	// If we're in the biking section and not already glaring
	VECTOR vPlayerPos = GET_ENTITY_COORDS(race.Racer[0].Driver)
	FLOAT fDist = MIN_GLARING_DISTANCE * MIN_GLARING_DISTANCE
	INT iGlaredRacer
	INT p = 1
	BOOL bGlaring = FALSE
	WHILE p < race.iRacerCnt
		FLOAT fDistBetweenRacers = VDIST2(vPlayerPos, GET_ENTITY_COORDS(race.Racer[p].Driver))
		// If they're close enough and have are behind me in the race
		IF fDistBetweenRacers < fDist AND race.Racer[p].iRank > race.Racer[0].iRank
			fDist = fDistBetweenRacers
			iGlaredRacer = p
			bGlaring = TRUE
//			CPRINTLN(DEBUG_TRIATHLON, "[tri_helpers_triathlon.sch::UPDATE_TRI_PLAYER_GLARING_AT_OTHER_RACERS] found someone to glare at, ", p)
		ENDIF
		p++
	ENDWHILE
	
	IF bGlaring
		// Glare at racer
		TASK_LOOK_AT_ENTITY(race.Racer[0].Driver, race.Racer[iGlaredRacer].Driver, 3000)
//		CPRINTLN(DEBUG_TRIATHLON, "[tri_helpers_triathlon.sch::UPDATE_TRI_PLAYER_GLARING_AT_OTHER_RACERS] returning TRUE, glaring at racer ", iGlaredRacer)
		RETURN TRUE
	ELSE
		// Don't glare at all
//		CPRINTLN(DEBUG_TRIATHLON, "[tri_helpers_triathlon.sch::UPDATE_TRI_PLAYER_GLARING_AT_OTHER_RACERS] returning FALSE, no one to glare at")
		RETURN FALSE
	ENDIF
ENDFUNC

/// Purpose:
///    Randomize an array of INTs.
PROC RANDOMIZE_ARRAY_OF_INTS(INT& nArray[])
	INT iLoopCounter
	REPEAT COUNT_OF(nArray) iLoopCounter
		INT iRandomNum = GET_RANDOM_INT_IN_RANGE( iLoopCounter, COUNT_OF(nArray) )
		INT iTempNum
		
		iTempNum				= nArray[iLoopCounter]
		nArray[iLoopCounter] 	= nArray[iRandomNum]
		nArray[iRandomNum]		= iTempNum
		
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Randomize_Array_Of_INTs] Array element ", iLoopCounter, " is ", nArray[iLoopCounter])
	ENDREPEAT
ENDPROC

/// Purpose:
///    Find the midpoint position vector between two points.
FUNC VECTOR FIND_MIDPOINT_OF_TWO_POINTS(VECTOR vPointA, VECTOR vPointB)
	VECTOR vMidPoint
	
	vMidPoint.x = 	(vPointA.x + vPointB.x) / 2
	vMidPoint.y =	(vPointA.y + vPointB.y) / 2
	vMidPoint.z =	(vPointA.z + vPointB.z) / 2
	
	RETURN vMidPoint
ENDFUNC

// ==================================================
// 	E N D  TRIATHLON HELPER FUNCTIONS AND PROCEDURES
// ==================================================










// ===================================================
// 	GATE ACCESSORS
// ===================================================

/// PURPOSE:
///    Get the current race leg (swim, bike, run) the racer is on:
///    		TRI_TRI_RACE_LEG_SWIM
///    		TRI_TRI_RACE_LEG_BIKE
///    		TRI_TRI_RACE_LEG_RUN
FUNC TRI_TRI_RACE_LEG Tri_Get_Racer_Current_Race_Leg(TRI_RACE_STRUCT& Race, TRI_RACER_STRUCT& Racer)
	IF NOT IS_ENTITY_DEAD(Racer.Driver)
		IF (Racer.iGateCur < Race.iGateCnt)
			// Check which gate the racer is on, and determine what leg he's on from there.
			IF (Race.sGate[Racer.iGateCur].eChkpntType = TRI_CHKPT_TRI_SWIM) OR (Race.sGate[Racer.iGateCur].eChkpntType = TRI_CHKPT_TRI_FIRST_TRANS)
				RETURN TRI_TRI_RACE_LEG_SWIM
			ELIF (Race.sGate[Racer.iGateCur].eChkpntType = TRI_CHKPT_TRI_RUN) OR (Race.sGate[Racer.iGateCur].eChkpntType = TRI_CHKPT_TRI_FINISH)
				RETURN TRI_TRI_RACE_LEG_RUN
			ELIF (Race.sGate[Racer.iGateCur].eChkpntType = TRI_CHKPT_TRI_BIKE) OR (Race.sGate[Racer.iGateCur].eChkpntType = TRI_CHKPT_TRI_SECOND_TRANS)
				RETURN TRI_TRI_RACE_LEG_BIKE
			ENDIF
		ENDIF
	ENDIF
	
	// It should only get here if racer reached the end.
	RETURN TRI_TRI_RACE_LEG_RUN
ENDFUNC

/// Purpose:
///   Return the SWIM to BIKE transition gate index.
FUNC INT Tri_Get_Swim_To_Bike_Transition_Gate()
	SWITCH eCurrentTriRace
		CASE TRIATHLON_RACE_VESPUCCI
			RETURN 3
		BREAK
		CASE TRIATHLON_RACE_ALAMO_SEA
			RETURN 3
		BREAK
		CASE TRIATHLON_RACE_IRONMAN
			RETURN 7
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC


/// Purpose:
///   Return the BIKE to RUN transition gate index.
FUNC INT Tri_Get_Bike_To_Run_Transition_Gate()
	SWITCH eCurrentTriRace
		CASE TRIATHLON_RACE_VESPUCCI
			RETURN 18
		BREAK
		CASE TRIATHLON_RACE_ALAMO_SEA
			RETURN 19
		BREAK
		CASE TRIATHLON_RACE_IRONMAN
			RETURN 86
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC


///// Purpose:
/////   Return the number of swim gates in the race.
//FUNC INT Tri_Get_Number_Of_Swim_Gates_In_Race( TRI_RACE_STRUCT& Race )
//	INT iNumOfSwimGatesInRace = 0
//	
//	INT iGateCounter
//	REPEAT COUNT_OF(Race.sGate) iGateCounter
//		IF (Race.sGate[iGateCounter].eChkpntType = TRI_CHKPT_TRI_SWIM) OR (Race.sGate[iGateCounter].eChkpntType = TRI_CHKPT_TRI_FIRST_TRANS)
//			iNumOfSwimGatesInRace++
//		ENDIF
//	ENDREPEAT
//	
//	//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Get_Number_Of_Swim_Gates_In_Race] There are ", iNumOfSwimGatesInRace, " swim gates in current race.")
//	RETURN iNumOfSwimGatesInRace
//ENDFUNC


///// Purpose:
/////   Return the number of bike gates in the race.
//FUNC INT Tri_Get_Number_Of_Bike_Gates_In_Race( TRI_RACE_STRUCT& Race )
//	INT iNumOfBikeGatesInRace = 0
//	
//	INT iGateCounter
//	REPEAT COUNT_OF(Race.sGate) iGateCounter
//		IF (Race.sGate[iGateCounter].eChkpntType = TRI_CHKPT_TRI_BIKE) OR (Race.sGate[iGateCounter].eChkpntType = TRI_CHKPT_TRI_SECOND_TRANS)
//			iNumOfBikeGatesInRace++
//		ENDIF
//	ENDREPEAT
//	
//	//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Get_Number_Of_Bike_Gates_In_Race] There are ", iNumOfBikeGatesInRace, " bike gates in current race.")
//	RETURN iNumOfBikeGatesInRace
//ENDFUNC
//
//
///// Purpose:
/////   Return the number of run gates in the race.
//FUNC INT Tri_Get_Number_Of_Run_Gates_In_Race()
//	
//	// Leaving +1 as a reminder that the finish line is included
//	SWITCH eCurrentTriRace
//		CASE TRIATHLON_RACE_VESPUCCI
//			RETURN 3+1
//		BREAK
//		CASE TRIATHLON_RACE_ALAMO_SEA
//			RETURN 4+1
//		BREAK
//		CASE TRIATHLON_RACE_IRONMAN
//			RETURN 33+1
//		BREAK
//	ENDSWITCH
//	
//	RETURN -1
//ENDFUNC

// This is the checkpoint that won't go away
FUNC INT TRI_GET_FIRST_BAKE_GATE()
	SWITCH eCurrentTriRace
		CASE TRIATHLON_RACE_VESPUCCI
			RETURN 4
		BREAK
		CASE TRIATHLON_RACE_ALAMO_SEA
			RETURN 4
		BREAK
		CASE TRIATHLON_RACE_IRONMAN
			RETURN 8
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("TRI_GET_FIRST_BAKE_GATE: Getting first bike gate on an invdalid race")
	RETURN -1
ENDFUNC


/// Purpose:
///   Return middle gate of the swim leg.
///   References
///    		triathlon_traditional_01.sch
///    		triathlon_traditional_02.sch
///    		triathlon_traditional_03.sch
///    - Counts the number of checkpoints tagged as TRI_CHKPT_TRI_SWIM or TRI_CHKPT_TRI_FIRST_TRANS
///    - then divides that number by 2
FUNC INT Tri_Get_Middle_Swim_Gate()
	SWITCH eCurrentTriRace
		CASE TRIATHLON_RACE_VESPUCCI
			RETURN 2
		BREAK
		CASE TRIATHLON_RACE_ALAMO_SEA
			RETURN 2
		BREAK
		CASE TRIATHLON_RACE_IRONMAN
			RETURN 8
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Tri_Get_Middle_Swim_Gate: Getting middle swim gate on an invdalid race")
	RETURN -1
	//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Get_Middle_Swim_Gate] The middle swim gate is, ", Tri_Get_Number_Of_Swim_Gates_In_Race(Race) / 2)
	//RETURN Tri_Get_Number_Of_Swim_Gates_In_Race(Race) / 2
ENDFUNC


/// Purpose:
///   Return middle gate of the bike leg.
///   References
///    		triathlon_traditional_01.sch
///    		triathlon_traditional_02.sch
///    		triathlon_traditional_03.sch
///    - Counts the number of checkpoints tagged as TRI_CHKPT_TRI_BIKE or TRI_CHKPT_TRI_SECOND_TRANS
///    - then divides that number by 2
FUNC INT Tri_Get_Middle_Bike_Gate()
	SWITCH eCurrentTriRace
		CASE TRIATHLON_RACE_VESPUCCI
			RETURN 15/2		// This will most likely FLOOR
		BREAK
		CASE TRIATHLON_RACE_ALAMO_SEA
			RETURN 8
		BREAK
		CASE TRIATHLON_RACE_IRONMAN
			RETURN 79/2		// This will most likely FLOOR
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Tri_Get_Middle_Bike_Gate: Getting middle bike gate on an invdalid race")
	RETURN -1
	//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Get_Middle_Bike_Gate] The middle bike gate is, ", Tri_Get_Number_Of_Bike_Gates_In_Race(Race) / 2)
	//RETURN Tri_Get_Number_Of_Bike_Gates_In_Race(Race) / 2
ENDFUNC


/// Purpose:
///   Return middle gate of the run leg
///   References
///    		triathlon_traditional_01.sch
///    		triathlon_traditional_02.sch
///    		triathlon_traditional_03.sch
///    - Counts the number of checkpoints tagged as TRI_CHKPT_TRI_RUN or TRI_CHKPT_TRI_FINISH
///    - then divides that number by 2   
FUNC INT Tri_Get_Middle_Run_Gate()
	SWITCH eCurrentTriRace
		CASE TRIATHLON_RACE_VESPUCCI
			RETURN 2
		BREAK
		CASE TRIATHLON_RACE_ALAMO_SEA
			RETURN 5/2
		BREAK
		CASE TRIATHLON_RACE_IRONMAN
			RETURN 17
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Tri_Get_Middle_Run_Gate: Getting middle run gate on an invdalid race")
	RETURN -1
	//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Get_Middle_Run_Gate] The middle run gate is, ", Tri_Get_Number_Of_Run_Gates_In_Race(Race) / 2)
	//RETURN Tri_Get_Number_Of_Run_Gates_In_Race(Race) / 2
ENDFUNC


/// Purpose:
///   Return TRUE if racer is at second half of the swim leg.
FUNC BOOL Tri_Is_Racer_In_Second_Half_Of_Swim_Leg(TRI_RACE_STRUCT& Race, INT iRacerIndex)
	// Ensure the racer is still in the swim leg.
	IF (Tri_Get_Racer_Current_Race_Leg(Race, Race.Racer[iRacerIndex]) = TRI_TRI_RACE_LEG_SWIM)
		IF Race.Racer[iRacerIndex].iGateCur >= Tri_Get_Middle_Swim_Gate()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// Purpose:
///   Return TRUE if racer is at second half of the bike leg.
FUNC BOOL Tri_Is_Racer_In_Second_Half_Of_Bike_Leg(TRI_RACE_STRUCT& Race, INT iRacerIndex)
	// Ensure the racer is still in the bike leg.
	IF (Tri_Get_Racer_Current_Race_Leg(Race, Race.Racer[iRacerIndex]) = TRI_TRI_RACE_LEG_BIKE)
		IF Race.Racer[iRacerIndex].iGateCur >= Tri_Get_Middle_Bike_Gate()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// Purpose:
///   Return TRUE if racer is at second half of the run leg.
FUNC BOOL Tri_Is_Racer_In_Second_Half_Of_Run_Leg(TRI_RACE_STRUCT& Race, INT iRacerIndex)
	// Ensure the racer is still in the run leg.
	IF (Tri_Get_Racer_Current_Race_Leg(Race, Race.Racer[iRacerIndex]) = TRI_TRI_RACE_LEG_RUN)
		IF Race.Racer[iRacerIndex].iGateCur >= Tri_Get_Middle_Run_Gate()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the Racer's current gate is the last bike gate.
FUNC BOOL Is_Racer_Current_Gate_The_Last_Bike_Gate(TRI_RACER_STRUCT& Racer)
	SWITCH eCurrentTriRace
		CASE TRIATHLON_RACE_VESPUCCI
			RETURN Racer.iGateCur = 18
		BREAK
		CASE TRIATHLON_RACE_ALAMO_SEA
			RETURN Racer.iGateCur = 19
		BREAK
		CASE TRIATHLON_RACE_IRONMAN
			RETURN Racer.iGateCur = 86
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC CLEANUP_TRI_PED_PROPS()
	CPRINTLN(DEBUG_TRIATHLON, "CLEANUP_TRI_PED_PROPS called")
	CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EYES)
	CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
ENDPROC

PROC UPDATE_TRI_PLAYER_CAP_AND_GOGGLES(TRI_RACE_STRUCT &thisRace)
//	VECTOR vForward = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
//	VECTOR vCamPos = GET_GAMEPLAY_CAM_COORD()
//	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
//	FLOAT fDot = DOT_PRODUCT(vForward, vCamPos - vPlayerPos)
	IF NOT IS_TRI_CONTROL_FLAG_SET(TCF_REMOVED_SWIM_GEAR)
		INT iStartGate = Tri_Get_Swim_To_Bike_Transition_Gate() + 1
		INT iGateCur = thisRace.Racer[0].iGateCur
		
//		CPRINTLN(DEBUG_TRIATHLON, "iGateCur=", iGateCur, ", Tri_Get_Swim_To_Bike_Transition_Gate(thisRace)=", Tri_Get_Swim_To_Bike_Transition_Gate(thisRace))
		
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfbi4", "takeoff_mask")	
			FLOAT fCurAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missfbi4", "takeoff_mask")
			IF fCurAnimTime >= 0.457
				KNOCK_OFF_PED_PROP(PLAYER_PED_ID(), FALSE, TRUE, TRUE, TRUE)
				SET_TRI_CONTROL_FLAG(TCF_REMOVED_SWIM_GEAR)
				CPRINTLN(DEBUG_TRIATHLON, "KNOCK_OFF_PED_PROP(PLAYER_PED_ID(), FALSE, TRUE, TRUE, TRUE)")
			ENDIF
		ENDIF
		
		IF iGateCur >= iStartGate AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfbi4", "takeoff_mask") AND NOT IS_PED_SWIMMING(PLAYER_PED_ID())
			TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfbi4", "takeoff_mask", default, default, default, AF_UPPERBODY | AF_SECONDARY)
			CPRINTLN(DEBUG_TRIATHLON, "TASK_PLAY_ANIM(PLAYER_PED_ID(), missfbi4, takeoff_mask")
		ENDIF
	ENDIF
ENDPROC


PROC UPDATE_TRI_AI_BIKE_HELMETS(TRI_RACE_STRUCT &thisRace)

	
	INT iRacerIterator
	REPEAT TRI_RACER_MAX iRacerIterator
		//No need to check the player
		IF iRacerIterator > 0
			IF NOT IS_BITMASK_AS_ENUM_SET(thisRace.Racer[iRacerIterator].iAIBitFlags, TACF_REQUEST_HELMET_ASSETS)
				
				INT iGateCur = thisRace.Racer[iRacerIterator].iGateCur
				
				IF iGateCur >= (Tri_Get_Swim_To_Bike_Transition_Gate() + 1)
					INT iHelmetTextID = GET_RANDOM_INT_IN_RANGE(0,3)
				//	IF IS_PED_ON_ANY_BIKE(thisRace.Racer[iRacerIterator].Driver)
					//	SCRIPT_ASSERT("Setting helmet for racer")
						CDEBUG1LN(DEBUG_TRIATHLON, "UPDATE_TRI_AI_BIKE_HELMETS - assigning racer #",iRacerIterator, " Helmet Prop ID # 0 and Texture ID# ",iHelmetTextID )
						
						SET_PED_HELMET_PROP_INDEX(thisRace.Racer[iRacerIterator].Driver,0)
						SET_PED_HELMET_TEXTURE_INDEX(thisRace.Racer[iRacerIterator].Driver,iHelmetTextID)
						
						SET_PED_HELMET(thisRace.Racer[iRacerIterator].Driver, TRUE)
					//	GIVE_PED_HELMET(thisRace.Racer[iRacerIterator].Driver, TRUE,PV_FLAG_BIKE_ONLY,iHelmetTextID)
						
						SET_BITMASK_AS_ENUM(thisRace.Racer[iRacerIterator].iAIBitFlags, TACF_REQUEST_HELMET_ASSETS)
				//	ENDIF

				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

// ===================================================
// 	E N D  GATE ACCESSORS
// ===================================================



///////////////
/// NEWS CAM DIALOGUE HELPERS
////////////////

PROC RESET_TRI_NEWS_EVENTS()
	INT iIndex
	
	REPEAT TRI_EVENT_QUEUE_COUNT iIndex
		TRI_Master.sEventData.sEvents[iIndex].eNews = TRNE_INVALID
	ENDREPEAT
	
	IF IS_TIMER_STARTED(TRI_Master.sEventData.sRankTimer)
		CANCEL_TIMER(TRI_Master.sEventData.sRankTimer)
	ENDIF
	
	IF IS_TIMER_STARTED(TRI_Master.sEventData.sLastCommentaryTimer)
		CANCEL_TIMER(TRI_Master.sEventData.sLastCommentaryTimer)
	ENDIF
	
	IF IS_TIMER_STARTED(TRI_Master.sEventData.sLongRankTimer)
		CANCEL_TIMER(TRI_Master.sEventData.sLongRankTimer)
	ENDIF
	
	IF IS_TIMER_STARTED(TRI_Master.sEventData.sRallyFallbackTimer)
		CANCEL_TIMER(TRI_Master.sEventData.sRallyFallbackTimer)
	ENDIF
	
	TRI_Master.sEventData.eLastEvent = TRNE_INVALID
	TRI_Master.sEventData.iCurRank = 0
	TRI_Master.sEventData.iLastRank = 0
	TRI_Master.sEventData.iRallyFallbackRank = 0
	TRI_Master.sEventData.eFlags = TREDF_EMPTY
ENDPROC

FUNC INT GET_TRI_EVENT_PRIORITY(TRI_RACE_NEWS_EVENT eEvent)
	SWITCH eEvent
		CASE TRNE_PLAYER_STAMINA
			RETURN 8
		BREAK
		CASE TRNE_PLAYER_CRASH_NPC
		CASE TRNE_PLAYER_CRASH
			RETURN 7
		BREAK
		CASE TRNE_PLAYER_IN_LAST
		CASE TRNE_PLAYER_IN_LAST_LONGTERM
			RETURN 6
		BREAK
		CASE TRNE_PLAYER_IN_SECOND
			RETURN 5
		BREAK
		CASE TRNE_PLAYER_RALLIED
		CASE TRNE_PLAYER_FALLBACK
			RETURN 4
		BREAK

		CASE TRNE_PLAYER_IN_FIRST
		CASE TRNE_PLAYER_IN_FIRST_LONGTERM
			RETURN 2
		BREAK
		CASE TRNE_PLAYER_MISSED_CHECKPT
			RETURN 3
		BREAK
		CASE TRNE_NPC_WON_SWIM
		CASE TRNE_NPC_WON_BIKE
		CASE TRNE_NPC_WON_RACE
			RETURN 1
		BREAK
		CASE TRNE_RACE_STARTED
			RETURN 0
		BREAK
	ENDSWITCH
	
	RETURN 10
ENDFUNC

FUNC BOOL GET_TRI_NEWS_EVENT_VALIDATION_NECESSARY(TRI_RACE_NEWS_EVENT eEvent)
	SWITCH eEvent
		CASE TRNE_PLAYER_IN_LAST
		CASE TRNE_PLAYER_IN_SECOND
		CASE TRNE_PLAYER_IN_FIRST
		CASE TRNE_PLAYER_IN_FIRST_LONGTERM
		CASE TRNE_PLAYER_IN_LAST_LONGTERM
		CASE TRNE_PLAYER_FALLBACK
			RETURN TRUE
		BREAK
		CASE TRNE_NPC_WON_SWIM
		CASE TRNE_NPC_WON_BIKE
		CASE TRNE_NPC_WON_RACE
		CASE TRNE_PLAYER_CRASH_NPC
		CASE TRNE_PLAYER_CRASH
		CASE TRNE_PLAYER_STAMINA
		CASE TRNE_RACE_STARTED
		CASE TRNE_PLAYER_MISSED_CHECKPT
		CASE TRNE_PLAYER_RALLIED
			RETURN FALSE
		BREAK
	ENDSWITCH	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ADD_TRI_NEWS_EVENT(TRI_RACE_NEWS_EVENT eEvent)
	INT iIndex
	REPEAT TRI_EVENT_QUEUE_COUNT iIndex
		IF TRI_Master.sEventData.sEvents[iIndex].eNews = TRNE_INVALID
			TRI_Master.sEventData.sEvents[iIndex].eNews = eEvent
			TRI_Master.sEventData.sEvents[iIndex].iTimeStamp = GET_GAME_TIMER()
			TRI_Master.sEventData.sEvents[iIndex].iPriority = GET_TRI_EVENT_PRIORITY(eEvent)
			
			IF GET_TRI_NEWS_EVENT_VALIDATION_NECESSARY( eEvent )
				SET_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.sEvents[iIndex].eFlags, TREF_VALIDATE)
			ENDIF
			
			CPRINTLN(DEBUG_TRIATHLON, "Adding news event ", eEvent )
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_AND_SAMPLE_TRI_RANK(TRI_RACE_STRUCT &thisRace)
	IF NOT IS_TIMER_STARTED(TRI_Master.sEventData.sRankTimer)
		START_TIMER_NOW(TRI_Master.sEventData.sRankTimer)
	ENDIF
	
	IF GET_TIMER_IN_SECONDS(TRI_Master.sEventData.sRankTimer) >= TRI_RANK_SAMPLE_TIME
		TRI_Master.sEventData.iLastRank = TRI_Master.sEventData.iCurRank
		TRI_Master.sEventData.iCurRank = thisRace.Racer[0].iRank
		
		IF IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.eFlags, TREDF_RALLYING)
			IF GET_TIMER_IN_SECONDS(TRI_Master.sEventData.sRallyFallbackTimer) <= 60.0
				IF TRI_Master.sEventData.iRallyFallbackRank - TRI_Master.sEventData.iCurRank >= 3
					ADD_TRI_NEWS_EVENT(TRNE_PLAYER_RALLIED)
					CANCEL_TIMER(TRI_master.sEventData.sRallyFallbackTimer)
					CLEAR_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_RALLYING)
				ENDIF
			ELSE
				CANCEL_TIMER(TRI_master.sEventData.sRallyFallbackTimer)
				CLEAR_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_RALLYING)				
			ENDIF
		ELIF IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.eFlags, TREDF_FALLINGBACK)
			IF GET_TIMER_IN_SECONDS(TRI_Master.sEventData.sRallyFallbackTimer) <= 60.0
				IF TRI_Master.sEventData.iCurRank - TRI_Master.sEventData.iRallyFallbackRank >= 3
					ADD_TRI_NEWS_EVENT(TRNE_PLAYER_FALLBACK)
					CANCEL_TIMER(TRI_master.sEventData.sRallyFallbackTimer)
					CLEAR_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_FALLINGBACK)
				ENDIF
			ELSE
				CANCEL_TIMER(TRI_master.sEventData.sRallyFallbackTimer)
				CLEAR_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_FALLINGBACK)			
			ENDIF			
		ENDIF
		
		IF TRI_Master.sEventData.iCurRank < TRI_Master.sEventData.iLastRank
			IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.eFlags, TREDF_RALLYING)
				RESTART_TIMER_NOW(TRI_Master.sEventData.sRallyFallbackTimer)
				SET_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_RALLYING)
				CLEAR_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_FALLINGBACK)
				TRI_Master.sEventData.iRallyFallbackRank = TRI_Master.sEventData.iLastRank
			ENDIF
		ELIF TRI_Master.sEventData.iCurRank > TRI_Master.sEventData.iLastRank
			IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.eFlags, TREDF_FALLINGBACK)
				RESTART_TIMER_NOW(TRI_Master.sEventData.sRallyFallbackTimer)
				SET_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_FALLINGBACK)
				CLEAR_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_RALLYING)
				TRI_Master.sEventData.iRallyFallbackRank = TRI_Master.sEventData.iLastRank
			ENDIF			
		ENDIF
		
		IF thisRace.Racer[0].iRank = 2
			AND TRI_Master.sEventData.iLastRank <> 2
			ADD_TRI_NEWS_EVENT(TRNE_PLAYER_IN_SECOND)
			
			IF IS_TIMER_STARTED(TRI_Master.sEventData.sLongRankTimer)
				CANCEL_TIMER(TRI_Master.sEventData.sLongRankTimer)
			ENDIF
		ELIF thisRace.Racer[0].iRank = 1
			IF TRI_Master.sEventData.iLastRank <> 1
				ADD_TRI_NEWS_EVENT(TRNE_PLAYER_IN_FIRST)
				
				IF IS_TIMER_STARTED(TRI_Master.sEventData.sLongRankTimer)
					CANCEL_TIMER(TRI_Master.sEventData.sLongRankTimer)
				ENDIF
			ELSE
				IF NOT IS_TIMER_STARTED(TRI_Master.sEventData.sLongRankTimer)
					START_TIMER_NOW(TRI_Master.sEventData.sLongRankTimer)
				ELIF GET_TIMER_IN_SECONDS(TRI_Master.sEventData.sLongRankTimer) > 60.0
					CANCEL_TIMER(TRI_Master.sEventData.sLongRankTimer)
					ADD_TRI_NEWS_EVENT(TRNE_PLAYER_IN_FIRST_LONGTERM)
				ENDIF
			ENDIF
		ELIF thisRace.Racer[0].iRank = thisRace.iRacerCnt
			IF TRI_Master.sEventData.iLastRank <> thisRace.iRacerCnt
				ADD_TRI_NEWS_EVENT(TRNE_PLAYER_IN_LAST)
				
				IF IS_TIMER_STARTED(TRI_Master.sEventData.sLongRankTimer)
					CANCEL_TIMER(TRI_Master.sEventData.sLongRankTimer)
				ENDIF
			ELSE
				IF NOT IS_TIMER_STARTED(TRI_Master.sEventData.sLongRankTimer)
					START_TIMER_NOW(TRI_Master.sEventData.sLongRankTimer)
				ELIF GET_TIMER_IN_SECONDS(TRI_Master.sEventData.sLongRankTimer) > 60.0
					CANCEL_TIMER(TRI_Master.sEventData.sLongRankTimer)
					ADD_TRI_NEWS_EVENT(TRNE_PLAYER_IN_LAST_LONGTERM)
				ENDIF
			ENDIF
		ENDIF
		RESTART_TIMER_NOW(TRI_Master.sEventData.sRankTimer)
	ENDIF
ENDPROC

PROC UPDATE_STAMINA_NEWS_EVENT()
	IF Tri_Player_Stamina.iCurrentEnergy <= 5
		IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.eFlags, TREDF_STAMINA_REGISTERED)
			SET_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_STAMINA_REGISTERED)
			
			ADD_TRI_NEWS_EVENT(TRNE_PLAYER_STAMINA)
		ENDIF
	ELSE
		IF IS_BITMASK_ENUM_AS_ENUM_SET(Tri_Master.sEventData.eFlags, TREDF_STAMINA_REGISTERED)
			CLEAR_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_STAMINA_REGISTERED)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_PLAYER_CRASH_NEWS_EVENT(TRI_RACE_STRUCT& thisRace)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(thisRace.Racer[0].Vehicle)
		IF IS_PED_IN_VEHICLE(thisRace.Racer[0].Driver, thisRace.Racer[0].Vehicle)
			INT iIndex
			BOOL bCrashed = FALSE
			REPEAT thisRace.iRacerCnt iIndex
				IF iIndex <> 0
					IF NOT IS_ENTITY_DEAD(thisRace.Racer[iIndex].Vehicle)
						IF IS_ENTITY_TOUCHING_ENTITY(thisRace.Racer[iIndex].Vehicle, thisRace.Racer[0].Vehicle)
							ADD_TRI_NEWS_EVENT(TRNE_PLAYER_CRASH_NPC)
							iIndex = thisRace.iRacerCnt
							bCrashed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT bCrashed
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(thisRace.Racer[0].Vehicle)
					ADD_TRI_NEWS_EVENT(TRNE_PLAYER_CRASH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL VALIDATE_TRI_EVENT( TRI_RACE_NEWS_EVENT eEvent, TRI_RACE_STRUCT& thisRace )
	SWITCH eEvent
		CASE TRNE_PLAYER_IN_FIRST
		CASE TRNE_PLAYER_IN_FIRST_LONGTERM
			IF thisRace.Racer[0].iRank = 1
				RETURN TRUE
			ENDIF
		BREAK
		CASE TRNE_PLAYER_IN_SECOND
			IF thisRace.Racer[0].iRank = 2
				RETURN TRUE
			ENDIF
		BREAK
		CASE TRNE_PLAYER_FALLBACK
			IF thisRace.Racer[0].iGateCur > 1
				RETURN TRUE
			ENDIF
		BREAK
		CASE TRNE_PLAYER_IN_LAST
		CASE TRNE_PLAYER_IN_LAST_LONGTERM
			IF thisRace.Racer[0].iRank = thisRace.iRacerCnt
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_AND_CLEAN_QUEUE()
	INT iIndex
	
	REPEAT TRI_EVENT_QUEUE_COUNT iIndex
		IF GET_GAME_TIMER() - TRI_Master.sEventData.sEvents[iIndex].iTimeStamp >= TRI_QUEUE_CLEAN_TIME // Clean this event
			TRI_Master.sEventData.sEvents[iIndex].eNews = TRNE_INVALID
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT TRI_GET_HIGHEST_PRIORITY_NEWS_EVENT(TRI_RACE_STRUCT& thisRace)
	INT iIndex, iRetVal
	INT iCurPriority = 12
	
	iRetVal = -1
	REPEAT TRI_EVENT_QUEUE_COUNT iIndex
		IF TRI_Master.sEventData.sEvents[iIndex].eNews <> TRNE_INVALID
			IF TRI_Master.sEventData.sEvents[iIndex].iPriority < iCurPriority
				AND TRI_Master.sEventData.eLastEvent <> TRI_Master.sEventData.sEvents[iIndex].eNews
				
				IF IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.sEvents[iIndex].eFlags, TREF_VALIDATE)
					IF VALIDATE_TRI_EVENT(TRI_Master.sEventData.sEvents[iIndex].eNews, thisRace)
						IF TRI_Master.sEventData.sEvents[iIndex].iPriority = 0
							RETURN iIndex
						ELSE
							iCurPriority = TRI_Master.sEventData.sEvents[iIndex].iPriority
							iRetVal = iIndex
						ENDIF					
					ENDIF
				ELSE
					IF TRI_Master.sEventData.sEvents[iIndex].iPriority = 0
						RETURN iIndex
					ELSE
						iCurPriority = TRI_Master.sEventData.sEvents[iIndex].iPriority
						iRetVal = iIndex
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iRetVal
ENDFUNC

PROC TRI_PLAY_EVENT_DIALOGUE(TRI_RACE_NEWS_EVENT event)
	SWITCH event
		CASE TRNE_PLAYER_IN_FIRST
			CPRINTLN( DEBUG_TRIATHLON, "PLAYER PLAYER IN FIRST NEWS DLG" )
			SWITCH GET_PLAYER_PED_ENUM( PLAYER_PED_ID() )
				CASE CHAR_MICHAEL
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_FIRSTM", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_FRANKLIN
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_FIRSTF", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_TREVOR
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_FIRSTT", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
			ENDSWITCH
		BREAK
		CASE TRNE_PLAYER_IN_FIRST_LONGTERM
			CPRINTLN( DEBUG_TRIATHLON, "PLAYER PLAYER IN FIRST LONGTERM NEWS DLG" )
			SWITCH GET_PLAYER_PED_ENUM( PLAYER_PED_ID() )
				CASE CHAR_MICHAEL
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_FIRST2M", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_FRANKLIN
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_FIRST2F", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_TREVOR
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_FIRST2T", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
			ENDSWITCH
		BREAK
		CASE TRNE_PLAYER_IN_SECOND
			CPRINTLN( DEBUG_TRIATHLON, "PLAYER PLAYER IN SECOND NEWS DLG" )
			SWITCH GET_PLAYER_PED_ENUM( PLAYER_PED_ID() )
				CASE CHAR_MICHAEL
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_SECM", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_FRANKLIN
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_SECF", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_TREVOR
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_SECT", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
			ENDSWITCH			
		BREAK
		CASE TRNE_PLAYER_IN_LAST
			CPRINTLN( DEBUG_TRIATHLON, "PLAYER PLAYER IN LAST NEWS DLG" )
			SWITCH GET_PLAYER_PED_ENUM( PLAYER_PED_ID() )
				CASE CHAR_MICHAEL
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_LASTM", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_FRANKLIN
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_LASTF", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_TREVOR
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_LASTT", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
			ENDSWITCH				
		BREAK
		CASE TRNE_PLAYER_IN_LAST_LONGTERM
			CPRINTLN( DEBUG_TRIATHLON, "PLAYER PLAYER IN LAST LONGTERM NEWS DLG" )
			SWITCH GET_PLAYER_PED_ENUM( PLAYER_PED_ID() )
				CASE CHAR_MICHAEL
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_LAST2M", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_FRANKLIN
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_LAST2F", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_TREVOR
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_LAST2T", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
			ENDSWITCH				
		BREAK
		CASE TRNE_RACE_STARTED
			CPRINTLN( DEBUG_TRIATHLON, "PLAYING RACE STARTED DLG" )
			CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_START", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
		BREAK
		CASE TRNE_NPC_WON_SWIM
			CPRINTLN( DEBUG_TRIATHLON, "PLAYING WON SWIM NEWS DLG" )
			CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_NPCSW", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
		BREAK
		CASE TRNE_NPC_WON_BIKE
			CPRINTLN( DEBUG_TRIATHLON, "PLAYING WON BIKE NEWS DLG" )
			CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_NPCBI", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
		BREAK
		CASE TRNE_NPC_WON_RACE
			CPRINTLN( DEBUG_TRIATHLON, "PLAYING WON RACE NEWS DLG" )
			CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_NPCTR", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
		BREAK
		CASE TRNE_PLAYER_STAMINA
			CPRINTLN( DEBUG_TRIATHLON, "PLAYING PLAYER STAMINA NEWS DLG" )
			SWITCH GET_PLAYER_PED_ENUM( PLAYER_PED_ID() )
				CASE CHAR_MICHAEL
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_STAMM", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_FRANKLIN
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_STAMF", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_TREVOR
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_STAMT", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
			ENDSWITCH				
		BREAK
		CASE TRNE_PLAYER_RALLIED
			CPRINTLN( DEBUG_TRIATHLON, "PLAYING PLAYER RALLIED NEWS DLG" )
			SWITCH GET_PLAYER_PED_ENUM( PLAYER_PED_ID() )
				CASE CHAR_MICHAEL
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_GAINM", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_FRANKLIN
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_GAINF", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_TREVOR
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_GAINT", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
			ENDSWITCH			
		BREAK
		CASE TRNE_PLAYER_FALLBACK
			CPRINTLN( DEBUG_TRIATHLON, "PLAYING PLAYER FALLBACK NEWS DLG" )
			SWITCH GET_PLAYER_PED_ENUM( PLAYER_PED_ID() )
				CASE CHAR_MICHAEL
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_BACKM", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_FRANKLIN
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_BACKF", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_TREVOR
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_BACKT", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
			ENDSWITCH			
		BREAK
		CASE TRNE_PLAYER_MISSED_CHECKPT
			CPRINTLN( DEBUG_TRIATHLON, "PLAYING MISSED CHECKPT NEWS DLG" )
			SWITCH GET_PLAYER_PED_ENUM( PLAYER_PED_ID() )
				CASE CHAR_MICHAEL
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_MISSM", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_FRANKLIN
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_MISSF", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_TREVOR
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_MISST", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
			ENDSWITCH			
		BREAK
		CASE TRNE_PLAYER_CRASH
			CPRINTLN( DEBUG_TRIATHLON, "PLAYING CRASH NEWS DLG" )
			SWITCH GET_PLAYER_PED_ENUM( PLAYER_PED_ID() )
				CASE CHAR_MICHAEL
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_CRASH2M", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_FRANKLIN
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_CRASH2F", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_TREVOR
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_CRASH2T", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
			ENDSWITCH			
		BREAK
		CASE TRNE_PLAYER_CRASH_NPC
			CPRINTLN( DEBUG_TRIATHLON, "PLAYING CRASH NPC NEWS DLG" )
			SWITCH GET_PLAYER_PED_ENUM( PLAYER_PED_ID() )
				CASE CHAR_MICHAEL
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_CRASHM", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_FRANKLIN
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_CRASHF", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
				CASE CHAR_TREVOR
					CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_CRASHT", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
				BREAK
			ENDSWITCH			
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL TRI_SHOULD_USE_IRONMAN_DLG()
	IF TRI_Master.iRaceCur = ENUM_TO_INT(TRIATHLON_RACE_IRONMAN)
		INT iRandom = GET_RANDOM_INT_IN_RANGE()
		
		IF iRandom%100 > 30
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC UPDATE_TRI_NEWS_DIALOGUE(TRI_RACE_STRUCT &thisRace)
	IF TRI_Master.bQuitting
		EXIT
	ENDIF
	
	IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.eFlags, TREDF_PED_ADDED)
		ADD_PED_FOR_DIALOGUE(TRI_Master.sEventData.convoStruct, 8, NULL, "TRINEWS")
		SET_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_PED_ADDED)
	ENDIF
	
	IF IS_TRI_CONTROL_FLAG_SET(TCF_NEWS_CAM_ACTIVE)
		IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.eFlags, TREDF_LINE_SPOKEN)
			INT iNewsIndex
			SWITCH Tri_Get_Racer_Current_Race_Leg( thisRace, thisRace.Racer[0] )
				CASE TRI_TRI_RACE_LEG_SWIM
					iNewsIndex = TRI_GET_HIGHEST_PRIORITY_NEWS_EVENT(thisRace)
					
					IF iNewsIndex <> -1
						IF TRI_Master.sEventData.sEvents[iNewsIndex].eNews = TRI_Master.sEventData.eLastEvent
							TRI_Master.sEventData.eLastEvent = TRNE_INVALID
							
							IF NOT TRI_SHOULD_USE_IRONMAN_DLG()
								CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_SWIM", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
							ELSE
								CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_IRON", CONV_PRIORITY_HIGH)
							ENDIF
						ELSE
							IF IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.sEvents[iNewsIndex].eFlags, TREF_VALIDATE)
								IF VALIDATE_TRI_EVENT(TRI_Master.sEventData.sEvents[iNewsIndex].eNews, thisRace)
									TRI_PLAY_EVENT_DIALOGUE(TRI_Master.sEventData.sEvents[iNewsIndex].eNews)
									TRI_Master.sEventData.eLastEvent = TRI_Master.sEventData.sEvents[iNewsIndex].eNews
									
									TRI_Master.sEventData.sEvents[iNewsIndex].eNews = TRNE_INVALID	
								ELSE
									TRI_Master.sEventData.eLastEvent = TRNE_INVALID
									IF NOT TRI_SHOULD_USE_IRONMAN_DLG()
										CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_SWIM", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
									ELSE
										CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_IRON", CONV_PRIORITY_HIGH)
									ENDIF
								ENDIF
							ELSE
								TRI_PLAY_EVENT_DIALOGUE(TRI_Master.sEventData.sEvents[iNewsIndex].eNews)
								TRI_Master.sEventData.eLastEvent = TRI_Master.sEventData.sEvents[iNewsIndex].eNews
								TRI_Master.sEventData.sEvents[iNewsIndex].eNews = TRNE_INVALID					
							ENDIF
						ENDIF
					ELSE
						TRI_Master.sEventData.eLastEvent = TRNE_INVALID
						IF NOT TRI_SHOULD_USE_IRONMAN_DLG()
							CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_SWIM", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
						ELSE
							CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_IRON", CONV_PRIORITY_HIGH)
						ENDIF
					ENDIF
					
					SET_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_LINE_SPOKEN)
				BREAK
				CASE TRI_TRI_RACE_LEG_BIKE
					iNewsIndex = TRI_GET_HIGHEST_PRIORITY_NEWS_EVENT(thisRace)
					
					IF iNewsIndex <> -1
						IF TRI_Master.sEventData.sEvents[iNewsIndex].eNews = TRI_Master.sEventData.eLastEvent
							TRI_Master.sEventData.eLastEvent = TRNE_INVALID
							IF NOT TRI_SHOULD_USE_IRONMAN_DLG()
								CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_BIKE", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
							ELSE
								CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_IRON", CONV_PRIORITY_HIGH)
							ENDIF
						ELSE
							IF IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.sEvents[iNewsIndex].eFlags, TREF_VALIDATE)
								IF VALIDATE_TRI_EVENT(TRI_Master.sEventData.sEvents[iNewsIndex].eNews, thisRace)
									TRI_PLAY_EVENT_DIALOGUE(TRI_Master.sEventData.sEvents[iNewsIndex].eNews)
									TRI_Master.sEventData.eLastEvent = TRI_Master.sEventData.sEvents[iNewsIndex].eNews
									TRI_Master.sEventData.sEvents[iNewsIndex].eNews = TRNE_INVALID										
								ELSE
									TRI_Master.sEventData.eLastEvent = TRNE_INVALID
									IF NOT TRI_SHOULD_USE_IRONMAN_DLG()
										CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_BIKE", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
									ELSE
										CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_IRON", CONV_PRIORITY_HIGH)
									ENDIF
								ENDIF
							ELSE
								TRI_PLAY_EVENT_DIALOGUE(TRI_Master.sEventData.sEvents[iNewsIndex].eNews)
								TRI_Master.sEventData.eLastEvent = TRI_Master.sEventData.sEvents[iNewsIndex].eNews
								TRI_Master.sEventData.sEvents[iNewsIndex].eNews = TRNE_INVALID					
							ENDIF
						ENDIF
					ELSE
						TRI_Master.sEventData.eLastEvent = TRNE_INVALID
						IF NOT TRI_SHOULD_USE_IRONMAN_DLG()
							CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_BIKE", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
						ELSE
							CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_IRON", CONV_PRIORITY_HIGH)
						ENDIF
					ENDIF
					
					SET_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_LINE_SPOKEN)				
				BREAK
				CASE TRI_TRI_RACE_LEG_RUN
					iNewsIndex = TRI_GET_HIGHEST_PRIORITY_NEWS_EVENT(thisRace)
					
					IF iNewsIndex <> -1
						IF TRI_Master.sEventData.sEvents[iNewsIndex].eNews = TRI_Master.sEventData.eLastEvent
							TRI_Master.sEventData.eLastEvent = TRNE_INVALID
							IF NOT TRI_SHOULD_USE_IRONMAN_DLG()
								CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_RUN", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
							ELSE
								CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_IRON", CONV_PRIORITY_HIGH)
							ENDIF
						ELSE
							IF IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.sEvents[iNewsIndex].eFlags, TREF_VALIDATE)
								IF VALIDATE_TRI_EVENT(TRI_Master.sEventData.sEvents[iNewsIndex].eNews, thisRace)
									TRI_PLAY_EVENT_DIALOGUE(TRI_Master.sEventData.sEvents[iNewsIndex].eNews)
									TRI_Master.sEventData.eLastEvent = TRI_Master.sEventData.sEvents[iNewsIndex].eNews
									TRI_Master.sEventData.sEvents[iNewsIndex].eNews = TRNE_INVALID										
								ELSE
									TRI_Master.sEventData.eLastEvent = TRNE_INVALID
									IF NOT TRI_SHOULD_USE_IRONMAN_DLG()
										CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_RUN", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
									ELSE
										CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_IRON", CONV_PRIORITY_HIGH)
									ENDIF
								ENDIF
							ELSE
								TRI_PLAY_EVENT_DIALOGUE(TRI_Master.sEventData.sEvents[iNewsIndex].eNews)
								TRI_Master.sEventData.eLastEvent = TRI_Master.sEventData.sEvents[iNewsIndex].eNews
								TRI_Master.sEventData.sEvents[iNewsIndex].eNews = TRNE_INVALID						
							ENDIF
						ENDIF
					ELSE
						TRI_Master.sEventData.eLastEvent = TRNE_INVALID
						IF NOT TRI_SHOULD_USE_IRONMAN_DLG()
							CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_RUN", CONV_PRIORITY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
						ELSE
							CREATE_CONVERSATION(TRI_Master.sEventData.convoStruct, "MGTRAUD", "MGTR_IRON", CONV_PRIORITY_HIGH)
						ENDIF
					ENDIF
					
					SET_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_LINE_SPOKEN)				
				BREAK
			ENDSWITCH
		ELSE
			IF NOT IS_TIMER_STARTED(TRI_Master.sEventData.sLastCommentaryTimer)
				START_TIMER_NOW(TRI_Master.sEventData.sLastCommentaryTimer)
			ELIF GET_TIMER_IN_SECONDS(TRI_Master.sEventData.sLastCommentaryTimer) >= 15.0
				CLEAR_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_LINE_SPOKEN)
				CANCEL_TIMER(TRI_Master.sEventData.sLastCommentaryTimer)
			ENDIF
		ENDIF
	ELSE
		IF IS_BITMASK_ENUM_AS_ENUM_SET(TRI_Master.sEventData.eFlags, TREDF_LINE_SPOKEN)
			IF NOT IS_TIMER_STARTED(TRI_Master.sEventData.sLastCommentaryTimer)
				START_TIMER_NOW(TRI_Master.sEventData.sLastCommentaryTimer)
			ELIF GET_TIMER_IN_SECONDS(TRI_Master.sEventData.sLastCommentaryTimer) >= 10.0
				CLEAR_BITMASK_ENUM_AS_ENUM(TRI_Master.sEventData.eFlags, TREDF_LINE_SPOKEN)
				CANCEL_TIMER(TRI_Master.sEventData.sLastCommentaryTimer)
			ENDIF
			
			IF IS_SCRIPTED_CONVERSATION_ONGOING()
				STOP_SCRIPTED_CONVERSATION(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	UPDATE_AND_CLEAN_QUEUE()
	UPDATE_AND_SAMPLE_TRI_RANK(thisRace)
	UPDATE_STAMINA_NEWS_EVENT()
	UPDATE_PLAYER_CRASH_NEWS_EVENT(thisRace)
ENDPROC

///////////////
///    END NEWS CAM DIALOGUE HELPERS
//////////////






// ===================================================
// 	RACER STATUS PROCEDURES AND FUNCTIONS
// ===================================================

/// PURPOSE:
///    Returns TRUE if commands can be called using a Tri ped and his vehicle, 
///    including player's.
FUNC BOOL IS_TRI_AI_RACER_OR_PLAYER_VALID(TRI_RACE_STRUCT& Race, INT iRacerIndex,  BOOL bCheckIfBikeIsDead)
	IF IS_ENTITY_DEAD(Race.Racer[iRacerIndex].Driver)
		RETURN FALSE
	ENDIF
	
	IF bCheckIfBikeIsDead
		IF IS_ENTITY_DEAD(Race.Racer[iRacerIndex].Vehicle)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns TRUE if commands can be called using an AI Tri ped and his vehicle.
FUNC BOOL IS_TRI_AI_RACER_VALID(TRI_RACE_STRUCT& Race, INT iRacerIndex, BOOL bCheckIfBikeIsDead)
	IF iRacerIndex < 1
		RETURN FALSE
	ENDIF 
	
	RETURN IS_TRI_AI_RACER_OR_PLAYER_VALID(Race, iRacerIndex, bCheckIfBikeIsDead)
ENDFUNC

/// PURPOSE:
///    Get closest racer to the player.
FUNC PED_INDEX Tri_Get_Closest_Racer_To_Player(TRI_RACE_STRUCT& Race)
	VECTOR vPlayerPos
	VECTOR vRacerPos
	INT iClosestPedIndex
	FLOAT fCurrentDistance
	FLOAT fClosestDistance
	PED_INDEX closestPedToPlayerIndex

	INT iRacerCounter
	REPEAT Race.iRacerCnt iRacerCounter
		IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerCounter].Driver)
			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
				IF NOT (Race.Racer[iRacerCounter].Driver = Race.Racer[0].Driver)
					IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerCounter].Vehicle)
						IF IS_PED_IN_VEHICLE(Race.Racer[iRacerCounter].Driver, Race.Racer[iRacerCounter].Vehicle)
							vRacerPos = GET_ENTITY_COORDS(Race.Racer[iRacerCounter].Vehicle)
						ELSE
							vRacerPos = GET_ENTITY_COORDS(Race.Racer[iRacerCounter].Driver)
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
						IF IS_PED_IN_VEHICLE(Race.Racer[iRacerCounter].Driver, Race.Racer[0].Vehicle)
							vPlayerPos = GET_ENTITY_COORDS(Race.Racer[0].Vehicle)
						ELSE
							vPlayerPos = GET_ENTITY_COORDS(Race.Racer[0].Driver)
						ENDIF
					ENDIF
					
					IF iRacerCounter = 1
						fClosestDistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vRacerPos)
						iClosestPedIndex = iRacerCounter
					ELSE
						fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vRacerPos)
						
						IF fCurrentDistance < fClosestDistance
							fClosestDistance = fCurrentDistance
							iClosestPedIndex = iRacerCounter
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	closestPedToPlayerIndex = Race.Racer[iClosestPedIndex].Driver
	
	RETURN closestPedToPlayerIndex
ENDFUNC


/// PURPOSE:
///    Get closest racer to the player.
FUNC BOOL Tri_Is_A_Racer_Close_And_On_Screen( TRI_RACE_STRUCT& Race )
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Is_A_Racer_Close_And_On_Screen] Function called.")

	FLOAT fDistanceBetweenPlayerAndRacer
	INT iRacerCounter
	REPEAT Race.iRacerCnt iRacerCounter
		IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerCounter].Driver)
			IF NOT (Race.Racer[iRacerCounter].Driver = Race.Racer[0].Driver)
				fDistanceBetweenPlayerAndRacer = GET_DISTANCE_BETWEEN_ENTITIES(Race.Racer[iRacerCounter].Driver, Race.Racer[0].Driver)
				IF NOT IS_ENTITY_OCCLUDED(Race.Racer[iRacerCounter].Driver)
					IF fDistanceBetweenPlayerAndRacer <= 11.00
						//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Is_A_Racer_Close_And_On_Screen] Racer #", iRacerCounter," is less than or equal to 11 units away from player.")
						RETURN TRUE
					ELSE
						//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Is_A_Racer_Close_And_On_Screen] Racer #", iRacerCounter," is more than 11 units away from player.")
					ENDIF
				ELSE
					//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Is_A_Racer_Close_And_On_Screen] Racer #", iRacerCounter," is occluded.")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

// ===================================================
// 	E N D  RACER STATUS PROCEDURES AND FUNCTIONS
// ===================================================










// ===================================================
// 	PLAYER PROCEDURES AND FUNCTIONS
// ===================================================

/// PURPOSE:
///    Update the player's bike speed.
///    
///    NOTE: Currently, this is only used for slowing down the 
///    			player's maximum possible speed as he approaches
///    			the bike->run transition gate, so player getting
///    			off the bike doesn't look abrupt if he arrives
///    			at the gate at top speed.
PROC UPDATE_TRI_PLAYER_BIKE_SPEED(TRI_RACE_STRUCT& Race, FLOAT fDefaultTribikeSpeed)

	IF NOT bIsPlayerBikeSpeedSlowedForTransitionSet
		IF IS_TRI_AI_RACER_OR_PLAYER_VALID(Race, 0, TRUE)
			IF IS_ENTITY_AT_COORD(Race.Racer[0].Vehicle, vSlowDownPlayerBikeAtThisPos, << 25.0, 25.0, 10.0 >>)
				IF IS_PED_ON_ANY_BIKE(Race.Racer[0].Driver)
					SET_DRIVE_TASK_CRUISE_SPEED(Race.Racer[0].Driver, fDefaultTribikeSpeed)
				ENDIF
//				MODIFY_VEHICLE_TOP_SPEED(Race.Racer[0].Vehicle, -25.0)
				bIsPlayerBikeSpeedSlowedForTransitionSet = TRUE
				CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_PLAYER_BIKE_SPEED] :: bIsPlayerBikeSpeedSlowedForTransitionSet = TRUE")
			ENDIF
		ENDIF
	ENDIF
ENDPROC





// ---------------------------------------------------
// 	PLAYER STAMINA FUNCTIONS
// ---------------------------------------------------

/// Purpose:
///    Replenish part of the player's energy meter at certain gates.
///    
///    bRefillHalfOfMaxEnergy: Setting this to TRUE will replenish the
///    							player's energy to half of the maximuym, 
///    							only if player has less than half the max energy.
///    
///    			       	       If FALSE, the player will have its entire
///    							energy replenished.
PROC REPLENISH_PLAYER_ENERGY_AT_GATE(BOOL bRefillPartial)
	IF bRefillPartial
		IF Tri_Player_Stamina.iCurrentEnergy < (Tri_Player_Stamina.iMaxEnergy/2)
//			Tri_Player_Stamina.iCurrentEnergy += Tri_Player_Stamina.iMaxEnergy/3
			RESTORE_PLAYER_STAMINA(PLAYER_ID(), STAMINA_RESTORE_PERCENTAGE)
			
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REPLENISH_PLAYER_STAMINA_AT_GATE] Replenished player energy by 1/3 of max energy.")
		ELSE
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REPLENISH_PLAYER_STAMINA_AT_GATE] Did NOT replenish player energy by 1/3 of max energy, and it's already equal or above 1/3.")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->REPLENISH_PLAYER_STAMINA_AT_GATE] FULLY replenished player energy.")
		Tri_Player_Stamina.iCurrentEnergy = Tri_Player_Stamina.iMaxEnergy
	ENDIF
ENDPROC

/// Purpose:
///    Update when the player's energy is replenished.
PROC UPDATE_TRI_PLAYER_ENERGY_REPLENISHMENT(TRI_RACE_STRUCT& Race)
	IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
		INT iCurrentPlayerGate = Race.Racer[0].iGateCur
	
		SWITCH (ePlayerStaminaResetState)
			CASE PLAYER_STAMINA_WAIT_FOR_SWIM_BIKE_GATE
				IF iCurrentPlayerGate = Tri_Get_Swim_To_Bike_Transition_Gate() + 1	// We add 1 because the func we're using is giving us the next gate, not the one just passed.
					CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->UPDATE_TRI_PLAYER_ENERGY_REPLENISHMENT] Stamina: Jumping from PLAYER_STAMINA_WAIT_FOR_SWIM_BIKE_GATE to PLAYER_STAMINA_RESET_STAMINA_AT_SWIM_BIKE_GATE")
					ePlayerStaminaResetState = PLAYER_STAMINA_RESET_STAMINA_AT_SWIM_BIKE_GATE
				ENDIF
			BREAK
			
			CASE PLAYER_STAMINA_RESET_STAMINA_AT_SWIM_BIKE_GATE
				REPLENISH_PLAYER_ENERGY_AT_GATE(TRUE)
				CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->UPDATE_TRI_PLAYER_ENERGY_REPLENISHMENT] Stamina: Jumping from PLAYER_STAMINA_RESET_STAMINA_AT_SWIM_BIKE_GATE to PLAYER_STAMINA_WAIT_FOR_BIKE_RUN_GATE")
				ePlayerStaminaResetState = PLAYER_STAMINA_WAIT_FOR_BIKE_RUN_GATE
			BREAK
			
			CASE PLAYER_STAMINA_WAIT_FOR_BIKE_RUN_GATE
				IF iCurrentPlayerGate = Tri_Get_Bike_To_Run_Transition_Gate() + 1	// We add 1 because the func we're using is giving us the next gate, not the one just passed.
					CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->UPDATE_TRI_PLAYER_ENERGY_REPLENISHMENT] Stamina: Jumping from PLAYER_STAMINA_WAIT_FOR_BIKE_RUN_GATE to PLAYER_STAMINA_RESET_STAMINA_AT_BIKE_RUN_GATE")
					ePlayerStaminaResetState = PLAYER_STAMINA_RESET_STAMINA_AT_BIKE_RUN_GATE
				ENDIF
			BREAK
			
			CASE PLAYER_STAMINA_RESET_STAMINA_AT_BIKE_RUN_GATE
				REPLENISH_PLAYER_ENERGY_AT_GATE(TRUE)
				CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->UPDATE_TRI_PLAYER_ENERGY_REPLENISHMENT] Stamina: Jumping from PLAYER_STAMINA_RESET_STAMINA_AT_BIKE_RUN_GATE to PLAYER_STAMINA_WAIT_FOR_LAST_SPURT_GATE")
				ePlayerStaminaResetState = PLAYER_STAMINA_RESET_DONE	// Skipping giving player energy at the last few gates, for now.			
			BREAK
			
			CASE PLAYER_STAMINA_WAIT_FOR_LAST_SPURT_GATE
				IF iCurrentPlayerGate = (Race.iGateCnt - 1)
					CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->UPDATE_TRI_PLAYER_ENERGY_REPLENISHMENT] Stamina: Jumping from PLAYER_STAMINA_WAIT_FOR_LAST_SPURT_GATE to PLAYER_STAMINA_RESET_STAMINA_AT_LAST_SPURT_GATE")
					ePlayerStaminaResetState = PLAYER_STAMINA_RESET_STAMINA_AT_LAST_SPURT_GATE
				ENDIF
			BREAK
			
			CASE PLAYER_STAMINA_RESET_STAMINA_AT_LAST_SPURT_GATE
				REPLENISH_PLAYER_ENERGY_AT_GATE(TRUE)
				CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRI_PLAYER_ENERGY_REPLENISHMENT] Stamina: Jumping from PLAYER_STAMINA_RESET_STAMINA_AT_LAST_SPURT_GATE to PLAYER_STAMINA_RESET_DONE")
				ePlayerStaminaResetState = PLAYER_STAMINA_RESET_DONE		
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// Purpose:
///    Update when the player's energy is replenished.
PROC UPDATE_TRI_MUSIC_CHECKPOINT_CUES(TRI_RACE_STRUCT& Race, INT &iMusicFlags)
	INT iCurrentPlayerGate = Race.Racer[0].iGateCur
//	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->UPDATE_TRI_MUSIC_CUES] :: Running")
	
	IF iCurrentPlayerGate = Tri_Get_Swim_To_Bike_Transition_Gate() AND NOT IS_BITMASK_AS_ENUM_SET(iMusicFlags, TPMC_LAST_SWIM)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->UPDATE_TRI_MUSIC_CUES] MGTR_LAST_SWIM triggered")
		SET_BITMASK_AS_ENUM(iMusicFlags, TPMC_LAST_SWIM)
		TRIGGER_MUSIC_EVENT("MGTR_LAST_SWIM")
	ELIF iCurrentPlayerGate = Tri_Get_Bike_To_Run_Transition_Gate() AND NOT IS_BITMASK_AS_ENUM_SET(iMusicFlags, TPMC_LAST_CYCLE)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->UPDATE_TRI_MUSIC_CUES] Stamina: MGTR_LAST_CYCLE triggered")
		SET_BITMASK_AS_ENUM(iMusicFlags, TPMC_LAST_CYCLE)
		TRIGGER_MUSIC_EVENT("MGTR_LAST_CYCLE")
	ELIF iCurrentPlayerGate = (Race.iGateCnt - 2) AND NOT IS_BITMASK_AS_ENUM_SET(iMusicFlags, TPMC_LAST_FOOT)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->UPDATE_TRI_MUSIC_CUES] Stamina: MGTR_LAST_FOOT triggered")
		SET_BITMASK_AS_ENUM(iMusicFlags, TPMC_LAST_FOOT)
		TRIGGER_MUSIC_EVENT("MGTR_LAST_FOOT")
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->UPDATE_TRI_MUSIC_CUES] Stamina: MGTR_COMPLETE prepared")
		PREPARE_MUSIC_EVENT("MGTR_COMPLETE")
	ENDIF
ENDPROC

/// Purpose:
///    Ensure the intensity and energy meters do not go over their respective limits.
//PROC CHECK_INTENSITY_AND_ENERY_METERS_STAY_IN_BOUNDS()
//	IF Tri_Player_Stamina.iCurrentIntensity < 0
//		Tri_Player_Stamina.iCurrentIntensity = 0
//	ENDIF
	
//	IF Tri_Player_Stamina.iCurrentIntensity > Tri_Player_Stamina.iMaxIntensity
//		Tri_Player_Stamina.iCurrentIntensity = Tri_Player_Stamina.iMaxIntensity
//	ENDIF
	
//	IF Tri_Player_Stamina.iCurrentEnergy < 0
//		Tri_Player_Stamina.iCurrentEnergy = 0
//		eCurrentPlayerEnergyMeterState = TRI_PLAYER_ENERGY_METER_STATE_EMPTY
//	ENDIF
//	
//	IF Tri_Player_Stamina.iCurrentEnergy > Tri_Player_Stamina.iMaxEnergy
//		Tri_Player_Stamina.iCurrentEnergy = Tri_Player_Stamina.iMaxEnergy
//		eCurrentPlayerEnergyMeterState = TRI_PLAYER_ENERGY_METER_STATE_FULL
//	ENDIF
//ENDPROC

/// Purpose:
///    Get the first third of the Tri intensity meter, where it transitions from green to yellow
//FUNC FLOAT GET_FIRST_TRANSITION_IN_TRI_SPEED_METER()
//	RETURN Tri_Player_Stamina.iMaxIntensity * GREEN_TO_YELLOW_SHIFT
//ENDFUNC

/// Purpose:
///    Get the last third of the Tri intensity meter, where it transitions from yellow to red.
//FUNC FLOAT GET_LAST_TRANSITION_IN_TRI_SPEED_METER()
//	RETURN Tri_Player_Stamina.iMaxIntensity * YELLOW_TO_RED_SHIFT
//ENDFUNC

/// Purpose:
///   	Get the player's current movement state in the swim leg:
///    		TRI_PLAYER_MOVE_SWIM_STATE_STILL
///    		TRI_PLAYER_MOVE_SWIM_STATE_SLOW
///    		TRI_PLAYER_MOVE_SWIM_STATE_NORMAL
///    		TRI_PLAYER_MOVE_SWIM_STATE_FAST
FUNC TRI_PLAYER_MOVE_SWIM_STATE GET_TRI_PLAYER_SWIM_MOVE_STATE(TRI_RACE_STRUCT& Race)
	IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
		IF GET_PED_DESIRED_MOVE_BLEND_RATIO(Race.Racer[0].Driver) > 2.0
			RETURN TRI_PLAYER_MOVE_SWIM_STATE_FAST
		ELIF GET_PED_DESIRED_MOVE_BLEND_RATIO(Race.Racer[0].Driver) > 1.0
			RETURN TRI_PLAYER_MOVE_SWIM_STATE_NORMAL
		ELIF GET_PED_DESIRED_MOVE_BLEND_RATIO(Race.Racer[0].Driver) > 0.0
			RETURN TRI_PLAYER_MOVE_SWIM_STATE_SLOW
		ELSE
			RETURN TRI_PLAYER_MOVE_SWIM_STATE_STILL
		ENDIF
	ENDIF
	
	RETURN TRI_PLAYER_MOVE_SWIM_STATE_STILL
ENDFUNC

/// Purpose:
///   	Get the player's current movement state in the bike leg:
///    		TRI_PLAYER_MOVE_BIKE_STATE_NOT_FAST
///    		TRI_PLAYER_MOVE_BIKE_STATE_FAST
///    
///    NOTE: Unlike the swim/run leg's use of move blend ratio to determine the speed state,
///    			the speed of the bike is an unreliable factor for determining the intensity state
///    			of the bike.  Therefore, we need to use player input to establish bike state.
//FUNC TRI_PLAYER_MOVE_BIKE_STATE GET_TRI_PLAYER_BIKE_SPEED_STATE(TRI_RACE_STRUCT& Race)
//	IF IS_TRI_AI_RACER_OR_PLAYER_VALID(Race, 0, TRUE)
//		IF IS_PED_IN_VEHICLE(Race.Racer[0].Driver, Race.Racer[0].Vehicle)
//			IF NOT IS_PED_RAGDOLL(Race.Racer[0].Driver)
//				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
//				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
//					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
//						// Start tracking how long since the last time the player pressed the button.
//						IF NOT IS_TIMER_STARTED(Tri_Player_Stamina.timerSprintButtonLastPressed)
//							START_TIMER_NOW(Tri_Player_Stamina.timerSprintButtonLastPressed)
//						ELSE
//							RESTART_TIMER_NOW(Tri_Player_Stamina.timerSprintButtonLastPressed)
//						ENDIF
////						CPRINTLN(DEBUG_TRIATHLON, "Returning from TRUE 1")
//						RETURN TRI_PLAYER_MOVE_BIKE_STATE_FAST
//					ELSE
//						IF IS_TIMER_STARTED(Tri_Player_Stamina.timerSprintButtonLastPressed)
//							IF TIMER_DO_WHEN_READY(Tri_Player_Stamina.timerSprintButtonLastPressed, BIKE_WAIT_BEFORE_COOLDOWN)
//								CANCEL_TIMER(Tri_Player_Stamina.timerSprintButtonLastPressed)
////								CPRINTLN(DEBUG_TRIATHLON, "Returning from not fast 1")
//								RETURN TRI_PLAYER_MOVE_BIKE_STATE_NOT_FAST
//							ELSE
////								CPRINTLN(DEBUG_TRIATHLON, "Returning from TRUE 2")
//								RETURN TRI_PLAYER_MOVE_BIKE_STATE_FAST
//							ENDIF
//						ELSE
////							CPRINTLN(DEBUG_TRIATHLON, "Returning from not fast 2")
//							RETURN TRI_PLAYER_MOVE_BIKE_STATE_NOT_FAST
//						ENDIF
//					ENDIF
//				ELSE
//					IF IS_TIMER_STARTED(Tri_Player_Stamina.timerSprintButtonLastPressed)
//						IF TIMER_DO_WHEN_READY(Tri_Player_Stamina.timerSprintButtonLastPressed, BIKE_WAIT_BEFORE_COOLDOWN)
//							CANCEL_TIMER(Tri_Player_Stamina.timerSprintButtonLastPressed)
////							CPRINTLN(DEBUG_TRIATHLON, "Returning from not fast 5")
//							RETURN TRI_PLAYER_MOVE_BIKE_STATE_NOT_FAST
//						ELSE
////							CPRINTLN(DEBUG_TRIATHLON, "Returning from TRUE 3")
//							RETURN TRI_PLAYER_MOVE_BIKE_STATE_FAST
//						ENDIF
//					ELSE
////						CPRINTLN(DEBUG_TRIATHLON, "Returning from not fast 3")
//						RETURN TRI_PLAYER_MOVE_BIKE_STATE_NOT_FAST
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
//			//check gate, check controls, do intensity stuff.
//			INT iGate = Tri_Get_Swim_To_Bike_Transition_Gate(Race) + 1	// Gets the last swim gate, we want the first bike gate.
//			IF iGate = Race.Racer[0].iGateCur AND GET_TRI_PLAYER_SWIM_MOVE_STATE(Race) = TRI_PLAYER_MOVE_SWIM_STATE_FAST
////				CPRINTLN(DEBUG_TRIATHLON, "Returning from TRUE 3")
//				RETURN TRI_PLAYER_MOVE_BIKE_STATE_FAST
//			ENDIF
//		ENDIF
//	ENDIF
////	CPRINTLN(DEBUG_TRIATHLON, "Returning from not fast 4")
//	RETURN TRI_PLAYER_MOVE_BIKE_STATE_NOT_FAST
//ENDFUNC

/// Purpose:
///   	Get the player's intensity meter state:
///    		TRI_PLAYER_INTENSITY_METER_STATE_EMPTY
///    		TRI_PLAYER_INTENSITY_METER_STATE_GREEN
///    		TRI_PLAYER_INTENSITY_METER_STATE_YELLOW
///    		TRI_PLAYER_INTENSITY_METER_STATE_RED
///    		TRI_PLAYER_INTENSITY_METER_STATE_FULL
//FUNC TRI_PLAYER_INTENSITY_METER_STATE GET_TRI_PLAYER_INTENSITY_METER_STATE()
//		IF Tri_Player_Stamina.iCurrentIntensity = Tri_Player_Stamina.iMaxIntensity
//			RETURN TRI_PLAYER_INTENSITY_METER_STATE_FULL
//		ELIF Tri_Player_Stamina.iCurrentIntensity > GET_LAST_TRANSITION_IN_TRI_SPEED_METER()
//			RETURN TRI_PLAYER_INTENSITY_METER_STATE_RED
//		ELIF Tri_Player_Stamina.iCurrentIntensity > GET_FIRST_TRANSITION_IN_TRI_SPEED_METER()
//			RETURN TRI_PLAYER_INTENSITY_METER_STATE_YELLOW
//		ELIF Tri_Player_Stamina.iCurrentIntensity > 0
//			RETURN TRI_PLAYER_INTENSITY_METER_STATE_GREEN
//		ELSE
//			RETURN TRI_PLAYER_INTENSITY_METER_STATE_EMPTY
//		ENDIF
//	
//	RETURN TRI_PLAYER_INTENSITY_METER_STATE_EMPTY
//ENDFUNC

/// Purpose:
///    Setup the player's intensity and energy meters.
PROC SETUP_TRI_PLAYER_ENERGY_AND_INTENSITY()
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SETUP_TRI_PLAYER_ENERGY_AND_INTENSITY] Procedure started.")
	enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
	INT iStamina = GET_INITIAL_STAT_VALUE(ePed, PS_STAMINA)
	FLOAT fAlpha = TO_FLOAT(iStamina)/100
	FLOAT fStaminaBoost = LERP_FLOAT(1.0, MAX_STAMINA_BOOST, fAlpha)
//	FLOAT fIntensityBoost = LERP_FLOAT(1.0, MAX_INTENSITY_BOOST, fAlpha)
	INT iMaxRaceEnergy	//, iMaxRaceIntensity
	
	RESET_PLAYER_STAMINA(PLAYER_ID())
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SETUP_TRI_PLAYER_ENERGY_AND_INTENSITY] :: RESET_PLAYER_STAMINA(PLAYER_ID())")

	IF 	eCurrentTriRace <> TRIATHLON_RACE_IRONMAN
		iMaxRaceEnergy = FLOOR(NORMAL_RACE_ENERGY * fStaminaBoost)
		Tri_Player_Stamina.iCurrentEnergy 	= iMaxRaceEnergy
		Tri_Player_Stamina.iMaxEnergy		= iMaxRaceEnergy
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SETUP_TRI_PLAYER_ENERGY_AND_INTENSITY] Current player energy set to ", Tri_Player_Stamina.iCurrentEnergy,
					", and current maximum energy set to ", Tri_Player_Stamina.iMaxEnergy, ", fStaminaBoost = ", fStaminaBoost)
	ELSE
		iMaxRaceEnergy = FLOOR(IRONMAN_RACE_ENERGY * fStaminaBoost)
		Tri_Player_Stamina.iCurrentEnergy 	= iMaxRaceEnergy
		Tri_Player_Stamina.iMaxEnergy		= iMaxRaceEnergy
		
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SETUP_TRI_PLAYER_ENERGY_AND_INTENSITY] Current player energy set to ", Tri_Player_Stamina.iCurrentEnergy,
					", and current maximum energy set to ", Tri_Player_Stamina.iMaxEnergy, " in Ironman. fStaminaBoost = ", fStaminaBoost)	
	ENDIF
	
//	iMaxRaceIntensity = FLOOR(MAX_RACE_INTENSITY * fIntensityBoost)
//	Tri_Player_Stamina.iCurrentIntensity 	= 0
//	Tri_Player_Stamina.iMaxIntensity 		= iMaxRaceIntensity
	
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SETUP_TRI_PLAYER_ENERGY_AND_INTENSITY] Current player speed set to ", Tri_Player_Stamina.iCurrentEnergy,
			", and current maximum spped set to ", Tri_Player_Stamina.iMaxEnergy, " in Ironman.")		
	
	Tri_Player_Stamina.hcolorEnergy 	= HUD_COLOUR_BLUE
//	Tri_Player_Stamina.hColorIntensity	= HUD_COLOUR_GREEN
	
//	eCurrentPlayerEnergyMeterState = TRI_PLAYER_ENERGY_METER_STATE_FULL
	Tri_Player_Stamina.bIsFastMovementDisabled = FALSE
ENDPROC

/// Purpose:
///    Update the player's intensity and energy meters in the swim leg.
//PROC UPDATE_PLAYER_TRI_INTENSITY_IN_SWIM_LEG(TRI_RACE_STRUCT& Race)
//	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPRINT) OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
//		IF NOT IS_PED_RAGDOLL(Race.Racer[0].Driver)
//			// Stop timing intensity meter cooldown, since the player is inputting the sprint button.
//			CANCEL_TIMER(Tri_Player_Stamina.timerBeforeIntensityMeterCooldown)
//			
//			TRI_PLAYER_MOVE_SWIM_STATE eCurrentPlayerSwimMoveState 				= GET_TRI_PLAYER_SWIM_MOVE_STATE(Race)
//			TRI_PLAYER_INTENSITY_METER_STATE eCurrentPlayerIntensityMeterState 	= GET_TRI_PLAYER_INTENSITY_METER_STATE()
//			
//			SWITCH (eCurrentPlayerSwimMoveState)
//				CASE TRI_PLAYER_MOVE_SWIM_STATE_NORMAL
//					SWITCH (eCurrentPlayerIntensityMeterState)
//						CASE TRI_PLAYER_INTENSITY_METER_STATE_EMPTY
//						CASE TRI_PLAYER_INTENSITY_METER_STATE_GREEN
//							Tri_Player_Stamina.iCurrentIntensity += FLOOR(SWIM_INTENSITY_INCREASE_EMPTY * GET_FRAME_TIME())
//							
//							// Ensure we stay in green.
//							IF Tri_Player_Stamina.iCurrentIntensity > GET_FIRST_TRANSITION_IN_TRI_SPEED_METER()
//								Tri_Player_Stamina.iCurrentIntensity -= FLOOR(SWIM_INTENSITY_DECREASE_GREEN * GET_FRAME_TIME())
////								CPRINTLN(DEBUG_TRIATHLON, "Decreasing while in green/empty, greater than first transition")
//							ENDIF
//						BREAK
//						
//						CASE TRI_PLAYER_INTENSITY_METER_STATE_YELLOW
//						CASE TRI_PLAYER_INTENSITY_METER_STATE_RED
//						CASE TRI_PLAYER_INTENSITY_METER_STATE_FULL
//							Tri_Player_Stamina.iCurrentIntensity -= FLOOR(SWIM_INTENSITY_DECREASE_YRF * GET_FRAME_TIME())
////							CPRINTLN(DEBUG_TRIATHLON, "Decreasing to green from YRF")
//						BREAK
//					ENDSWITCH					
//				BREAK
//				
//				CASE TRI_PLAYER_MOVE_SWIM_STATE_FAST
//					SWITCH (eCurrentPlayerIntensityMeterState)
//						CASE TRI_PLAYER_INTENSITY_METER_STATE_EMPTY														
//						CASE TRI_PLAYER_INTENSITY_METER_STATE_GREEN
//							IF Tri_Player_Stamina.iCurrentIntensity < CEIL(GET_FIRST_TRANSITION_IN_TRI_SPEED_METER()) + 1
//								Tri_Player_Stamina.iCurrentIntensity += FLOOR(SWIM_INTENSITY_INCREASE_GREEN * GET_FRAME_TIME())
//							ELSE
//								Tri_Player_Stamina.iCurrentIntensity = CEIL(GET_FIRST_TRANSITION_IN_TRI_SPEED_METER()) + 1
//							ENDIF
//						BREAK
//						
//						CASE TRI_PLAYER_INTENSITY_METER_STATE_YELLOW
//						CASE TRI_PLAYER_INTENSITY_METER_STATE_RED
//							Tri_Player_Stamina.iCurrentIntensity += FLOOR(SWIM_INTENSITY_INCREASE_YRF * GET_FRAME_TIME())			
//						BREAK
//						
//						CASE TRI_PLAYER_INTENSITY_METER_STATE_FULL
//							Tri_Player_Stamina.iCurrentIntensity = Tri_Player_Stamina.iMaxIntensity
//						BREAK
//					ENDSWITCH
//				BREAK
//				
//				CASE TRI_PLAYER_MOVE_SWIM_STATE_SLOW
//				CASE TRI_PLAYER_MOVE_SWIM_STATE_STILL
//					Tri_Player_Stamina.iCurrentIntensity -= FLOOR(SWIM_INTENSITY_DECREASE_YRF * GET_FRAME_TIME())
////					CPRINTLN(DEBUG_TRIATHLON, "decreasing due to stillness")
//				BREAK
//			ENDSWITCH
//		ENDIF
//	ELSE		
//		// Start timer before allowing intensity meter cooldown, as long as there's intensity meter left.
//		IF Tri_Player_Stamina.iCurrentIntensity > 0
//			IF NOT IS_TIMER_STARTED(Tri_Player_Stamina.timerBeforeIntensityMeterCooldown)
//				START_TIMER_NOW(Tri_Player_Stamina.timerBeforeIntensityMeterCooldown)
//			ENDIF
//			
//			// Start cooling down when timer is met.
//			IF TIMER_DO_WHEN_READY(Tri_Player_Stamina.timerBeforeIntensityMeterCooldown, 1.0)
//				TRI_PLAYER_INTENSITY_METER_STATE eCurrentPlayerIntensityMeterState = GET_TRI_PLAYER_INTENSITY_METER_STATE()
////				CPRINTLN(DEBUG_TRIATHLON, "Decreasing in ELSE bracket")
//				SWITCH eCurrentPlayerIntensityMeterState
//					// We may want to decrease the meter at different intensity states, so keep logic below until then.
//					CASE TRI_PLAYER_INTENSITY_METER_STATE_GREEN
//						Tri_Player_Stamina.iCurrentIntensity -= FLOOR(SWIM_INTENSITY_DECREASE_YRF * GET_FRAME_TIME())
//					BREAK
//					
//					CASE TRI_PLAYER_INTENSITY_METER_STATE_YELLOW
//						Tri_Player_Stamina.iCurrentIntensity -= FLOOR(SWIM_INTENSITY_DECREASE_YRF * GET_FRAME_TIME())
//					BREAK
//					
//					CASE TRI_PLAYER_INTENSITY_METER_STATE_RED
//					CASE TRI_PLAYER_INTENSITY_METER_STATE_FULL
//						Tri_Player_Stamina.iCurrentIntensity -= FLOOR(SWIM_INTENSITY_DECREASE_YRF * GET_FRAME_TIME())
//					BREAK
//				ENDSWITCH
//			ENDIF
//		ELSE
//			Tri_Player_Stamina.iCurrentIntensity = 0
//		ENDIF
//	ENDIF
//ENDPROC

/// Purpose:
///    Update the player's intensity and energy meters in the bike leg.
//PROC UPDATE_PLAYER_TRI_INTENSITY_IN_BIKE_LEG(TRI_RACE_STRUCT& Race)
//	TRI_PLAYER_MOVE_BIKE_STATE eCurrentPlayerBikeSpeedState 			= GET_TRI_PLAYER_BIKE_SPEED_STATE(Race)
//	TRI_PLAYER_INTENSITY_METER_STATE eCurrentPlayerIntensityMeterState 	= GET_TRI_PLAYER_INTENSITY_METER_STATE()
//	
//	INT iIntensityIncrease = FLOOR(BIKE_INTENSITY_INCREASE_RATE * GET_FRAME_TIME())
//	INT iIntensityDecrease = FLOOR(BIKE_INTENSITY_DECREASE_RATE * GET_FRAME_TIME())
//	
//	SWITCH (eCurrentPlayerBikeSpeedState)
//		CASE TRI_PLAYER_MOVE_BIKE_STATE_NOT_FAST
//			Tri_Player_Stamina.iCurrentIntensity -= iIntensityDecrease
////			CPRINTLN(DEBUG_TRIATHLON, "TRI_PLAYER_MOVE_BIKE_STATE_NOT_FAST: Decreasing bike intensity")
//		BREAK
//		
//		CASE TRI_PLAYER_MOVE_BIKE_STATE_FAST
//			// We may want to increase the player's intensity faster based on the intensity meter state, so leave this for now.
////			SWITCH eCurrentPlayerIntensityMeterState
//				CASE TRI_PLAYER_INTENSITY_METER_STATE_EMPTY
//				CASE TRI_PLAYER_INTENSITY_METER_STATE_GREEN
//					Tri_Player_Stamina.iCurrentIntensity += iIntensityIncrease
////					CPRINTLN(DEBUG_TRIATHLON, PICK_STRING(eCurrentPlayerIntensityMeterState = TRI_PLAYER_INTENSITY_METER_STATE_EMPTY, "TRI_PLAYER_INTENSITY_METER_STATE_EMPTY", "TRI_PLAYER_INTENSITY_METER_STATE_GREEN"), ": Increasing bike intensity")
//				BREAK
//				
//				CASE TRI_PLAYER_INTENSITY_METER_STATE_YELLOW
//					Tri_Player_Stamina.iCurrentIntensity += iIntensityIncrease	
////					CPRINTLN(DEBUG_TRIATHLON, "TRI_PLAYER_INTENSITY_METER_STATE_YELLOW: Increasing bike intensity")
//				BREAK
//				
//				CASE TRI_PLAYER_INTENSITY_METER_STATE_RED
//					Tri_Player_Stamina.iCurrentIntensity += iIntensityIncrease
////					CPRINTLN(DEBUG_TRIATHLON, "TRI_PLAYER_INTENSITY_METER_STATE_RED: Increasing bike intensity")
//				BREAK
//				
//				CASE TRI_PLAYER_INTENSITY_METER_STATE_FULL
//					Tri_Player_Stamina.iCurrentIntensity = Tri_Player_Stamina.iMaxIntensity
////					CPRINTLN(DEBUG_TRIATHLON, "TRI_PLAYER_INTENSITY_METER_STATE_FULL: Increasing bike intensity")
//				BREAK
//				
//				DEFAULT
////					CPRINTLN(DEBUG_TRIATHLON, "Hitting Default case for TRI_PLAYER_MOVE_BIKE_STATE_FAST Bike Intensity")
//				BREAK
//			ENDSWITCH
//		BREAK
//		
//		DEFAULT
////			CPRINTLN(DEBUG_TRIATHLON, "Hitting Default case for eCurrentPlayerBikeSpeedState Bike Intensity")
//		BREAK
//	ENDSWITCH
//ENDPROC

/// Purpose:
///    Update the player's intensity and energy meters in the run leg.
//PROC UPDATE_PLAYER_TRI_INTENSITY_IN_RUN_LEG(TRI_RACE_STRUCT& Race)
//	// Swimming and running currently have the share the same move values and states, so let's use the same logic.
//	UPDATE_PLAYER_TRI_INTENSITY_IN_SWIM_LEG(Race)
//ENDPROC
/*
PROC UPDATE_TRI_PLAYER_WHEEZING(TRI_RACE_STRUCT &thisRace, FLOAT fAlpha)
	//AUD_DAMAGE_REASON_CLIMB_LARGE
	//AUD_DAMAGE_REASON_CLIMB_SMALL
	//AUD_DAMAGE_REASON_JUMP
	//AUD_DAMAGE_REASON_DEFAULT	1.1
	//AUD_DAMAGE_REASON_DEFAULT 28.0
	//AUD_DAMAGE_REASON_DEFAULT	32.0
	//AUD_DAMAGE_REASON_WHEEZE
//	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP) OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
//		CPRINTLN(DEBUG_TRIATHLON, "PLAY_PAIN(PLAYER_PED_ID(), CLIMB_LARGE)")
//		PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_CLIMB_LARGE)
////		PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_DEFAULT, 1.1)
//	ENDIF
//	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN) OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
//		CPRINTLN(DEBUG_TRIATHLON, "PLAY_PAIN(PLAYER_PED_ID(), CLIMB_SMALL)")
//		PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_CLIMB_SMALL)
////		PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_DEFAULT, 28)
//	ENDIF
//	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT) OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
//		CPRINTLN(DEBUG_TRIATHLON, "PLAY_PAIN(PLAYER_PED_ID(), JUMP)")
//		PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_JUMP)
////		PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_DEFAULT, 32)
//	ENDIF
//	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT) OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
//		CPRINTLN(DEBUG_TRIATHLON, "PLAY_PAIN(PLAYER_PED_ID(), PAIN_WHEEZE)")
//		PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_WHEEZE)
//	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPRINT) OR GET_TRI_WHEEZE_TIMER(thisRace) > TRI_WHEEZE_COUNTDOWN
		FLOAT fRandMax = 10000.0
		FLOAT fFreq = GET_RANDOM_FLOAT_IN_RANGE(0, fRandMax)
		fFreq = fFreq / fRandMax
		
		IF fAlpha < TRI_WHEEZE_HARD AND fFreq < TRI_WHEEZE_ODDS_HARD
			CPRINTLN(DEBUG_TRIATHLON, "[tri_helpers_triathlon.sch :: UPDATE_TRI_PLAYER_WHEEZING] Wheezing AUD_DAMAGE_REASON_DEFAULT, 32")
			PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_DEFAULT, 32)
		ELIF fAlpha < TRI_WHEEZE_MED AND fFreq < TRI_WHEEZE_ODDS_MED
			CPRINTLN(DEBUG_TRIATHLON, "[tri_helpers_triathlon.sch :: UPDATE_TRI_PLAYER_WHEEZING] Wheezing AUD_DAMAGE_REASON_DEFAULT, 28")
			PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_DEFAULT, 28)
		ELIF fAlpha < TRI_WHEEZE_EASY AND fFreq < TRI_WHEEZE_ODDS_EASY
			CPRINTLN(DEBUG_TRIATHLON, "[tri_helpers_triathlon.sch :: UPDATE_TRI_PLAYER_WHEEZING] Wheezing AUD_DAMAGE_REASON_DEFAULT, 1.1")
			PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_DEFAULT, 1.1)
		ELIF fFreq < (TRI_WHEEZE_ODDS_EASY / 2)
			CPRINTLN(DEBUG_TRIATHLON, "[tri_helpers_triathlon.sch :: UPDATE_TRI_PLAYER_WHEEZING] Wheezing AUD_DAMAGE_REASON_WHEEZE")
			PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_WHEEZE)
		ENDIF
		RESET_TRI_WHEEZE_TIMER(thisRace)
	ENDIF
	INCREASE_TRI_WHEEZE_TIMER(thisRace)
ENDPROC
*/

///	PURPOSE:
///    Update the player's energy meter.
///    
///    Current design: 
///    	GREEN	- 	Energy drains slowly.
///    	YELLOW	-	Energy drains moderately.
///    	RED		-	Energy drains fastest.
///    	FULL	-	Burning rubber! 
PROC UPDATE_PLAYER_TRI_ENERGY()
//	TRI_PLAYER_INTENSITY_METER_STATE eCurrentPlayerIntensityMeterState = GET_TRI_PLAYER_INTENSITY_METER_STATE()
	FLOAT fMaxSprint = 100	//GET_PLAYER_MAX_SPRINT_STAMINA(PLAYER_ID())
	FLOAT fRemainingSprint = GET_PLAYER_SPRINT_STAMINA_REMAINING(PLAYER_ID())
	FLOAT fAlpha = 1 - (fRemainingSprint/fMaxSprint)
	fAlpha = CLAMP(fAlpha, 0, 1)
	
	Tri_Player_Stamina.iCurrentEnergy = ROUND(TO_FLOAT(Tri_Player_Stamina.iMaxEnergy) * fAlpha)
	
//	CPRINTLN(DEBUG_TRIATHLON, "fMaxSprint=", fMaxSprint, ", fRemainingSprint=", fRemainingSprint, ", fAlpha=", fAlpha, ", iCurrentEnergy=", Tri_Player_Stamina.iCurrentEnergy)
	
//	TRI_TRI_RACE_LEG eLeg = Tri_Get_Racer_Current_Race_Leg(thisRace, thisRace.Racer[0])
//	IF eLeg <> TRI_TRI_RACE_LEG_SWIM	// TODO: Remove all wheezing, B*1286136
//		UPDATE_TRI_PLAYER_WHEEZING(thisRace, fAlpha)
//	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_CHEATING_ENABLED
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP) OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
				RESTORE_PLAYER_STAMINA(PLAYER_ID(), STAMINA_RESTORE_PERCENTAGE)
				CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_PLAYER_TRI_ENERGY] :: Energy increased by STAMINA_RESTORE_PERCENTAGE via debug")
			ENDIF
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN) OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
				RESET_PLAYER_STAMINA(PLAYER_ID())
				CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_PLAYER_TRI_ENERGY] :: RESET_PLAYER_STAMINA(PLAYER_ID()) thru debug")
			ENDIF
		ENDIF
	#ENDIF
ENDPROC

/// Purpose:
///    Update the player's intensity and energy meters.
PROC UPDATE_PLAYER_TRI_INTENSITY_AND_ENERGY()
//	TRI_TRI_RACE_LEG eCurrentPlayerRaceLeg = Tri_Get_Racer_Current_Race_Leg(Race, Race.Racer[0])
	
	// Check which leg of the race the player is in.
//	SWITCH (eCurrentPlayerRaceLeg)
//		CASE TRI_TRI_RACE_LEG_SWIM
//			UPDATE_PLAYER_TRI_INTENSITY_IN_SWIM_LEG(Race)
//		BREAK
//		
//		CASE TRI_TRI_RACE_LEG_BIKE
//			UPDATE_PLAYER_TRI_INTENSITY_IN_BIKE_LEG(Race)
//		BREAK
//		
//		CASE TRI_TRI_RACE_LEG_RUN
//			UPDATE_PLAYER_TRI_INTENSITY_IN_RUN_LEG(Race)
//		BREAK
//	ENDSWITCH

	// Update the player's energy meter.
	UPDATE_PLAYER_TRI_ENERGY()
	
	// Ensure the intensity and energy meters do not go over their respective limits.
//	CHECK_INTENSITY_AND_ENERY_METERS_STAY_IN_BOUNDS()
ENDPROC

// ---------------------------------------------------
// 	E N D  PLAYER STAMINA FUNCTIONS
// ---------------------------------------------------










// ---------------------------------------------------
// 	PLAYER EVENT FUNCTIONS
// ---------------------------------------------------

/// PURPOSE:
///    Check if player has just pushed someone.
FUNC BOOL Tri_Has_Player_Pushed_A_Racer(TRI_RACE_STRUCT& Race)
	INT iLoopCounter
	REPEAT (Race.iRacerCnt) iLoopCounter
		IF (Race.Racer[iLoopCounter].Driver <> Race.Racer[0].Driver)
			IF NOT IS_ENTITY_DEAD(Race.Racer[iLoopCounter].Driver)
				IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
					IF IS_ENTITY_TOUCHING_ENTITY(Race.Racer[0].Driver, Race.Racer[iLoopCounter].Driver)
						CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Pushed_A_Racer] Player has touched another racer.")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check if player has lost his current position in the race.
FUNC BOOL Tri_Has_Player_Dropped_Rank(TRI_RACE_STRUCT& Race)
	IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
		IF iPlayerPreviousRank <> -1
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Dropped_Rank] Set current rank ", iPlayerCurrentRank, "  to player's rank ", Race.Racer[0].iRank)
			iPlayerCurrentRank = Race.Racer[0].iRank
		
			IF iPlayerCurrentRank > iPlayerPreviousRank
				//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Dropped_Rank] Current rank ", iPlayerCurrentRank, "  is now greater than previous rank ", iPlayerPreviousRank)
				//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Dropped_Rank] Setting previous rank to current rank:", iPlayerCurrentRank, " and returning TRUE")
				iPlayerPreviousRank = iPlayerCurrentRank
				//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Has_Player_Improved_Rank] Player dropped rank.")
				RETURN TRUE
			ELIF iPlayerCurrentRank = iPlayerPreviousRank
				//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Dropped_Rank] Current rank ", iPlayerCurrentRank, "  is equal to previous rank ", iPlayerPreviousRank)
				RETURN FALSE
			ELSE
				//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Dropped_Rank] Current rank ", iPlayerCurrentRank, "  is less than previous rank ", iPlayerPreviousRank)
				RETURN FALSE
			ENDIF
		ELSE
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Dropped_Rank] Previous rank is -1, so set previous rank to player's rank ", Race.Racer[0].iRank)
			iPlayerPreviousRank = Race.Racer[0].iRank
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Script should not reach here while the race is ongoing.
	//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Dropped_Rank] Script should only reach here is player is dead.  Is he dead?")
	iPlayerPreviousRank = iPlayerCurrentRank
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check if player has improved his current position in the race.
FUNC BOOL Tri_Has_Player_Improved_Rank(TRI_RACE_STRUCT& Race)
	IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
		IF iPlayerPreviousRank <> -1
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Improved_Rank] Set current rank ", iPlayerCurrentRank, "  to player's rank ", Race.Racer[0].iRank)
			iPlayerCurrentRank = Race.Racer[0].iRank
		
			IF iPlayerCurrentRank < iPlayerPreviousRank
				//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Improved_Rank] Current rank ", iPlayerCurrentRank, "  is now less than previous rank ", iPlayerPreviousRank)
				//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Improved_Rank] Setting previous rank to current rank:", iPlayerCurrentRank, " and returning TRUE")
				iPlayerPreviousRank = iPlayerCurrentRank
				//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Has_Player_Improved_Rank] Player improved rank.")
				RETURN TRUE
			ELIF iPlayerCurrentRank = iPlayerPreviousRank
				//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Improved_Rank] Current rank ", iPlayerCurrentRank, "  is equal to previous rank ", iPlayerPreviousRank)
				RETURN FALSE
			ELSE
				//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Improved_Rank] Current rank ", iPlayerCurrentRank, "  is greater than previous rank ", iPlayerPreviousRank)
				RETURN FALSE
			ENDIF
		ELSE
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Improved_Rank] Previous rank is -1, so set previous rank to player's rank ", Race.Racer[0].iRank)
			iPlayerPreviousRank = Race.Racer[0].iRank
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Script should not reach here while the race is ongoing.
	//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers->Tri_Has_Player_Improved_Rank] Script should only reach here is player is dead.  Is he dead?")
	iPlayerPreviousRank = iPlayerCurrentRank
	RETURN FALSE
ENDFUNC

// ---------------------------------------------------
// 	E N D  PLAYER EVENT FUNCTIONS
// ---------------------------------------------------










// ---------------------------------------------------
// 	PLAYER RANK FUNCTIONS 
// ---------------------------------------------------

/// PURPOSE:
///    Check if player is in last place.
FUNC BOOL Tri_Is_Player_In_Last_Place(TRI_RACE_STRUCT& Race)
	IF Race.Racer[0].iRank = Race.iRacerCnt
		IF NOT IS_TIMER_STARTED(timerPlayerRemainingInLastPlace)
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Is_Player_In_Last_Place] Started timer on player remaining in last place.")
			START_TIMER_NOW(timerPlayerRemainingInLastPlace)
		ENDIF
		RETURN TRUE
	ELSE
		IF IS_TIMER_STARTED(timerPlayerRemainingInLastPlace)
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Is_Player_In_Last_Place] Stopped timer on player remaining in last place.")
			CANCEL_TIMER(timerPlayerRemainingInLastPlace)
		ENDIF
		RETURN FALSE
	ENDIF
ENDFUNC


/// PURPOSE:
///    Check if player is in second place.
FUNC BOOL Tri_Is_Player_In_Second_Place(TRI_RACE_STRUCT& Race)
	IF Race.Racer[0].iRank = 2
		IF NOT IS_TIMER_STARTED(timerPlayerRemainingInSecondPlace)
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Is_Player_In_Second_Place] Started timer on player remaining in second place.")
			START_TIMER_NOW(timerPlayerRemainingInSecondPlace)
		ENDIF
		RETURN TRUE
	ELSE
		IF IS_TIMER_STARTED(timerPlayerRemainingInSecondPlace)
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Is_Player_In_Second_Place] Stopped timer on player remaining in second place.")
			CANCEL_TIMER(timerPlayerRemainingInSecondPlace)
		ENDIF
		RETURN FALSE
	ENDIF
ENDFUNC


/// PURPOSE:
///    Check if player is in first place.
FUNC BOOL Tri_Is_Player_In_First_Place(TRI_RACE_STRUCT& Race)
	IF Race.Racer[0].iRank = 1
		IF NOT IS_TIMER_STARTED(timerPlayerRemainingInFirstPlace)
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Is_Player_In_First_Place] Started timer on player remaining in first place.")
			START_TIMER_NOW(timerPlayerRemainingInFirstPlace)
		ENDIF
		RETURN TRUE
	ELSE
		IF IS_TIMER_STARTED(timerPlayerRemainingInFirstPlace)
			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Is_Player_In_First_Place] Stopped timer on player remaining in first place.")
			CANCEL_TIMER(timerPlayerRemainingInFirstPlace)
		ENDIF
		RETURN FALSE
	ENDIF
ENDFUNC


/// PURPOSE:
///    Check if player is in a place other than first, second, or last.
FUNC BOOL Tri_Is_Player_In_One_Place(TRI_RACE_STRUCT& Race)
	IF NOT ( (Race.Racer[0].iRank = 1)  OR  (Race.Racer[0].iRank = 2)  OR  (Race.Racer[0].iRank = Race.iRacerCnt) )
		IF iPlayerInPlaceRank = Race.Racer[0].iRank
			IF NOT IS_TIMER_STARTED(timerPlayerRemainingInPlace)
				//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Is_Player_In_One_Place] Started timer on player remaining in one place.")
				START_TIMER_NOW(timerPlayerRemainingInPlace)
			ENDIF
		ELSE
			iPlayerInPlaceRank = Race.Racer[0].iRank
			CANCEL_TIMER(timerPlayerRemainingInPlace)
		ENDIF
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

// ---------------------------------------------------
// 	E N D  PLAYER RANK FUNCTIONS
// ---------------------------------------------------

// ===================================================
// 	E N D  PLAYER PROCEDURES AND FUNCTIONS
// ===================================================










// ===================================================
// 	BIKE PROCEDURES AND FUNCTIONS
// ===================================================

/// PURPOSE:
///    Returns if a racer is getting on a TRIBIKE, or is already in a TRIBIKE.
/// PARAMS:
///    Race - 
///    iRacerIndex - 
///    bCheckMounting - Used when you don't want to check if the player is mounting the bike
/// RETURNS:
///    
FUNC BOOL IS_TRI_RACER_GETTING_ON_OR_IS_HE_ALREADY_ON_A_TRIBIKE(TRI_RACE_STRUCT& Race, INT iRacerIndex, BOOL bCheckMounting = TRUE)
	IF IS_PED_IN_ANY_VEHICLE(Race.Racer[iRacerIndex].Driver) OR (IS_PED_GETTING_INTO_A_VEHICLE(Race.Racer[iRacerIndex].Driver) AND bCheckMounting)
		VEHICLE_INDEX vehRacerIsOn = GET_VEHICLE_PED_IS_USING(Race.Racer[iRacerIndex].Driver)
		
		IF (IS_VEHICLE_MODEL( vehRacerIsOn, TRIBIKE) 
		OR IS_VEHICLE_MODEL( vehRacerIsOn, TRIBIKE2)
		OR IS_VEHICLE_MODEL( vehRacerIsOn, TRIBIKE3))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Return the center position of all racer bikes.
FUNC VECTOR Tri_Get_Bikes_Center_Position(TRI_RACE_STRUCT& Race)
	INT iGateLoopCounter = 0
	WHILE iGateLoopCounter < Race.iGateCnt
		IF Race.sGate[iGateLoopCounter].eChkpntType = TRI_CHKPT_TRI_BIKE
			RETURN Race.sGate[iGateLoopCounter].vPos
		ENDIF
		iGateLoopCounter++
	ENDWHILE
	RETURN Race.sGate[Race.Racer[0].iGateCur].vPos
ENDFUNC

/// PURPOSE:
///    Create blip for player vehicle.
PROC CREATE_PLAYER_TRI_BIKE_BLIP(TRI_RACE_STRUCT &Race)
	IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
		IF NOT DOES_BLIP_EXIST(blipPlayerVehicle)
			blipPlayerVehicle = CREATE_BLIP_FOR_VEHICLE(Race.Racer[0].Vehicle)
		ELSE
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->CREATE_PLAYER_TRI_BIKE_BLIP] Player bike blip already exists.")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->CREATE_PLAYER_TRI_BIKE_BLIP] ERROR: Can't create blip.  Player bike is DEAD!")
	ENDIF
ENDPROC

/// PURPOSE:
///    Create the radar blip for the bikes.
PROC CREATE_TRI_BIKES_RADAR_BLIP(TRI_RACE_STRUCT &Race)
	IF NOT DOES_BLIP_EXIST(blipBikes)
		blipBikes = ADD_BLIP_FOR_RADIUS(Tri_Get_Bikes_Center_Position(Race), 15.0)
		SET_BLIP_COLOUR(blipBikes, BLIP_COLOUR_BLUE)
		SET_BLIP_ALPHA(blipBikes, 220)
		SET_BLIP_AS_MINIMAL_ON_EDGE(blipBikes, TRUE)
		
		// Show and prioritize the bike blip.
		SET_BLIP_PRIORITY(blipBikes, 	BLIPPRIORITY_HIGHEST)
		SET_BLIP_DISPLAY(blipBikes, 	DISPLAY_BOTH)
	ENDIF
ENDPROC

/// PURPOSE:
///    Create the radar blip for the bikes.
PROC CREATE_TRI_BIKES_EDGE_RADAR_BLIP(TRI_RACE_STRUCT &Race)
	IF NOT DOES_BLIP_EXIST(blipBikesEdge)
		blipBikesEdge = ADD_BLIP_FOR_COORD(Tri_Get_Bikes_Center_Position(Race))
		SET_BLIP_COLOUR(blipBikesEdge, BLIP_COLOUR_BLUE)
		SET_BLIP_ALPHA(blipBikesEdge, 220)
		SET_BLIP_AS_MINIMAL_ON_EDGE(blipBikesEdge, TRUE)
		
		// Show and prioritize the bike blip.
		SET_BLIP_PRIORITY(blipBikesEdge, 	BLIPPRIORITY_HIGHEST)
		SET_BLIP_DISPLAY(blipBikesEdge, 	DISPLAY_BOTH)
	ENDIF
ENDPROC

/// PURPOSE:
///    Ensure the player can only get on his chosen bike.
PROC SET_TRI_PLAYER_CAN_ONLY_RIDE_BIKE_HE_IS_ON(TRI_RACE_STRUCT &Race)
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->Tri_Set_Player_Can_Only_Ride_Player_Bike] Started procedure.")

	INT iRacerCounter
	REPEAT (Race.iRacerCnt) iRacerCounter
		IF NOT (Race.Racer[iRacerCounter].Vehicle = Race.Racer[0].Vehicle)
			IF DOES_ENTITY_EXIST(Race.Racer[iRacerCounter].Vehicle)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(Race.Racer[iRacerCounter].Vehicle, FALSE)
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(Race.Racer[iRacerCounter].Vehicle)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(Race.Racer[iRacerCounter].Vehicle, TRUE)
			ENDIF
		ENDIF	
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Checks if a vehicle is empty.  This is an altered copy of IS_VEHICLE_EMPTY under net_include.sch.
///    
/// PARAMS:
///    vehBike - Check if this bike is empty.
FUNC BOOL IS_RACER_TRIBIKE_EMPTY(VEHICLE_INDEX vehBike)
	INT iLoopCounter
	INT iMaxSeats
	iMaxSeats = (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehBike) + 1) // Add 1 for the driver
	
	REPEAT iMaxSeats iLoopCounter
		IF NOT IS_VEHICLE_SEAT_FREE(vehBike, INT_TO_ENUM(VEHICLE_SEAT, iLoopCounter-1))			
			RETURN FALSE		
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE
///    Updates whether or not a bike has been mounted by a racer.
///    
///    If bike was mounted by an AI racer, sets bike unavailable to player.
///    
///    NOTE:	The reason we're using bubble-sort, is because at this time
///    			in the race, there's a likelihood a racer will enter
///    			a bike that isn't assigned to him, so to account for that issue
/// 			we're decoupling the driver index with the bike index.
PROC UPDATE_TRIBIKE_MOUNTING_STATUS(TRI_RACE_STRUCT &Race)
	IF iNumOfRacersThatHaveMountedBikes < Race.iRacerCnt
		INT iBikeCounter
		REPEAT Race.iRacerCnt iBikeCounter
			IF NOT IS_ENTITY_DEAD(Race.Racer[iBikeCounter].Vehicle)
				IF NOT IS_RACER_TRIBIKE_EMPTY(Race.Racer[iBikeCounter].Vehicle) AND NOT IS_TRI_AI_CONTROL_FLAG_SET(Race.Racer[iBikeCounter],TACF_BHASBEENONABIKE)
					SET_TRI_AI_CONTROL_FLAG(Race.Racer[iBikeCounter],TACF_BHASBEENONABIKE)
					SET_PED_CONFIG_FLAG(Race.Racer[iBikeCounter].Driver,PCF_DontTakeOffHelmet,TRUE)
					iNumOfRacersThatHaveMountedBikes++
//					SET_DRIVE_TASK_DRIVING_STYLE(Race.Racer[iBikeCounter].Driver, ENUM_TO_INT(DRIVINGSTYLE_RACING))
					CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRIBIKE_MOUNTING_STATUS] Bike #", iBikeCounter," has been mounted on.")
					//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->UPDATE_TRIBIKE_MOUNTING_STATUS] Number of mounted racers updated to ", iNumOfRacersThatHaveMountedBikes)
					
					IF iBikeCounter <> 0
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(Race.Racer[iBikeCounter].Vehicle, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	INT iRacerCounter
	REPEAT Race.iRacerCnt iRacerCounter
		// Set AI racer bike to be inaccessible to player.
		IF iRacerCounter <> 0
			IF IS_TRI_RACER_GETTING_ON_OR_IS_HE_ALREADY_ON_A_TRIBIKE(Race, iRacerCounter)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(Race.Racer[iRacerCounter].Vehicle, FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

// ===================================================
// 	E N D  BIKE PROCEDURES AND FUNCTIONS
// ===================================================


















// ===================================================
//  PRINT HELP PROCEDURES AND FUNCTIONS
// ===================================================

/// PURPOSE:
///    Manage printing help text in the race.
PROC UPDATE_PRINT_HELP_TEXT(TRI_RACE_STRUCT& Race)
	IF (eCurrentTriRace = TRIATHLON_RACE_VESPUCCI)
		INT iPlayerCurrentGate 	= Race.Racer[0].iGateCur
		IF (iPlayerCurrentGate = iShowSwimHelpAtThisGate)
			// Gate: After first water gate.
			IF NOT bHasSwimHintPlayed
//				PRINT_HELP("TRI_HELP_SWIM")
				bHasSwimHintPlayed = TRUE
			ENDIF
		// Gate: After first BIKE gate.
		ELIF (iPlayerCurrentGate >= iShowBikeHelpAtThisGate)  AND  (iPlayerCurrentGate <= iStopShowBikeHelpAtThisGate)
			IF NOT bHasBikeHintPlayed
				IF IS_PED_IN_VEHICLE(Race.Racer[0].Driver, Race.Racer[0].Vehicle)
					IF NOT IS_TIMER_STARTED(timerBeforeShowingHint)
						START_TIMER_NOW_SAFE(timerBeforeShowingHint)
					ELIF TIMER_DO_WHEN_READY(timerBeforeShowingHint, 3.0)
						IF IS_USING_KEYBOARD_AND_MOUSE(Player_control)
							PRINT_HELP("TRI_HELP_BIKE_KM")
						ELSE
	//						PRINT_HELP("TRI_HELP_BIKE")
						ENDIF
						bHasBikeHintPlayed = TRUE
						CANCEL_TIMER(timerBeforeShowingHint)
					ENDIF
				ENDIF
			ENDIF
		// Gate: After first run gate.
		ELIF (iPlayerCurrentGate = iShowRunHelpAtThisGate)
			IF NOT bHasRunHintPlayed
				IF NOT IS_TIMER_STARTED(timerBeforeShowingHint)
					START_TIMER_NOW_SAFE(timerBeforeShowingHint)
				ELIF TIMER_DO_WHEN_READY(timerBeforeShowingHint, 5.0)
					IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						PRINT_HELP("TRI_HELP_RUN_KM")
					ELSE
						PRINT_HELP("TRI_HELP_RUN")
					ENDIF
					bHasRunHintPlayed = TRUE
					CANCEL_TIMER(timerBeforeShowingHint)
				ENDIF
			ENDIF
		ENDIF
		
		// Gate: After second swim gate.
		IF (iPlayerCurrentGate = iShowTipHelpAtThisGate)
			AND GET_SP_PLAYER_PED_STAT_VALUE( GET_CURRENT_PLAYER_PED_ENUM(), PS_STAMINA ) < 99
			IF NOT bHasTipHintPlayed_0
				PRINT_HELP("TRI_HELP_TIP_1")
				bHasTipHintPlayed_0 = TRUE
			ELIF NOT bHasTipHintPlayed_1
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRI_HELP_TIP_1")
					PRINT_HELP("TRI_HELP_TIP_2")
					bHasTipHintPlayed_1 = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		// Gate: Around middle of water leg.
		IF NOT (eCurrentTriRace = TRIATHLON_RACE_VESPUCCI)
			IF (iPlayerCurrentGate = iShowCamHelpAtThisGate)
				IF NOT bHasCamHintPlayed AND NOT IS_TRI_CONTROL_FLAG_SET(TCF_NEWS_CAM_SHOWN)
					PRINT_HELP("TRI_HELP_TV")
					bHasCamHintPlayed = TRUE
				ENDIF
			ENDIF
		// In Tri 2 the swim leg is short, so we need to spread when hints are displayed.
		ELSE
			IF (iPlayerCurrentGate = iShowCamHelpAtThisGate)
				FLOAT fDistanceBetweenPlayerAndCurrentGate = 0.0
				IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
					fDistanceBetweenPlayerAndCurrentGate = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(Race.Racer[0].Driver), Race.sGate[iShowCamHelpAtThisGate].vPos)
					IF (fDistanceBetweenPlayerAndCurrentGate < 60.0)
						IF NOT bHasCamHintPlayed AND NOT IS_TRI_CONTROL_FLAG_SET(TCF_NEWS_CAM_SHOWN)
							PRINT_HELP("TRI_HELP_TV")
							bHasCamHintPlayed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
//		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//			PRINT_HELP_FOREVER("TRI_Q_PROMPT")
//		ENDIF
		
		// Remove bike tips in display if player gets off bike.
//		IF NOT IS_PED_IN_VEHICLE(Race.Racer[0].Driver, Race.Racer[0].Vehicle)
//			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRI_HELP_BIKE")
//				CLEAR_HELP(TRUE)
//			ENDIF
//		ENDIF
	ELSE
//		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//			PRINT_HELP_FOREVER("TRI_Q_PROMPT")
//		ENDIF
	ENDIF
ENDPROC

// ===================================================
//  E N D  PRINT HELP PROCEDURES AND FUNCTIONS
// ===================================================










// ===================================================
//  RACE HUD PROCEDURES AND FUNCTIONS
// ===================================================

/// Purpose:
///    Draw the player's intensity and energy bars in the race HUD.
PROC DRAW_PLAYER_TRI_INTENSITY_AND_ENERGY_METERS()

	// Determine what color the speed should draw in.
//	IF Tri_Player_Stamina.iCurrentIntensity > GET_LAST_TRANSITION_IN_TRI_SPEED_METER()
//		Tri_Player_Stamina.hColorIntensity		= HUD_COLOUR_RED
//	ELIF Tri_Player_Stamina.iCurrentIntensity > GET_FIRST_TRANSITION_IN_TRI_SPEED_METER()
//		Tri_Player_Stamina.hColorIntensity		= HUD_COLOUR_YELLOW
//	ELIF Tri_Player_Stamina.iCurrentIntensity > 0
//		Tri_Player_Stamina.hColorIntensity		= HUD_COLOUR_GREEN
//	ELSE
//		Tri_Player_Stamina.hColorIntensity		= HUD_COLOUR_BLACK
//	ENDIF
	
	Tri_Player_Stamina.hcolorEnergy			= HUD_COLOUR_BLUE
	
	// Determine what color the energy should draw in.
//	IF Tri_Player_Stamina.iCurrentEnergy <= 0
//		Tri_Player_Stamina.hcolorEnergy		= HUD_COLOUR_BLACK
//	ENDIF
	
	// Draw the energy meter.
	DRAW_GENERIC_METER(Tri_Player_Stamina.iCurrentEnergy, Tri_Player_Stamina.iMaxEnergy, "TRI_ENERGY", Tri_Player_Stamina.hcolorEnergy, -1, HUDORDER_EIGHTHBOTTOM, -1, -1,
							FALSE, TRUE)

	// Draw the intensity meter.
//	DRAW_GENERIC_METER(Tri_Player_Stamina.iCurrentIntensity, Tri_Player_Stamina.iMaxIntensity, "TRI_INTENSE", Tri_Player_Stamina.hColorIntensity, -1, HUDORDER_NINETHBOTTOM, -1, -1,
//							FALSE, TRUE)
ENDPROC

PROC DRAW_TRI_INTRO_HUD()
	
ENDPROC

/// PURPOSE:
///    Draw race HUD information on screen.
///    
///    NOTE: Don't change the name of this command.
///    			The name is shared across other
///    			SPR races, so the SPR system
///    			automatically calls the correct
///    			command based on which script is running.
///    			(Or it's magic).
PROC TRI_Race_Draw_Hud(TRI_RACE_STRUCT& Race)
	// Check whether or not to display best time.
	STRING szBestTriTimeLabel
	INT iBesTriTime
	
	IF (g_savedGlobals.sTriathlonData.fBestTime[TRI_Master.iRaceCur] > 0.0)
		szBestTriTimeLabel 	= "" 
		iBesTriTime			= CEIL(g_savedGlobals.sTriathlonData.fBestTime[TRI_Master.iRaceCur]*1000)
	ELSE
		szBestTriTimeLabel 	= "-1"
		iBesTriTime			= -1
	ENDIF
	
	IF NOT IS_TRI_CONTROL_FLAG_SET(TCF_NEWS_CAM_ACTIVE)	//IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)

		DRAW_CHECKPOINT_COUNTDOWN_RACE_HUD(FLOOR(Race.Racer[0].fClockTime*1000), // RaceTime - The timer
			"TRI_HUD_TIME",						 // TimerTitle - The title of the timer. Defaults to TIME with "" passed in
			-1, 								 // LapNumber - Number of laps
			-1, 								 // LapMaxNumber - Max number of laps
			"", 								 // LapTitle - The title of the lap meter. Defaults to LAP with "" Passed in
			Race.Racer[0].iRank, 				 // PositionNum - The position Number
			Race.iRacerCnt, 					 // PositionMaxNumber - The position maximum number
			"", 								 // PositionTitle - The title of the position number. Defaults to POSITION with "" Passed in 
			CEIL(Race.Racer[0].fPlsMnsTot*1000), // ExtraTimeGiven - If any extra time is given to the timer, pass this in to display +xs -xs
			HUD_COLOUR_WHITE,				 	 // PlacementColour - The position numbers can change colour
			/*iGateCur*/-1, 					 // CheckpointNumber - if you have a checkpoint bar the current number
			/*Race.iGateCnt*/-1, 				 // CheckpointMaxNum - if you have a checkpoint bar, the maximum number
		    "SPR_GATES", 						 // CheckpointTitle - The title of the checkpoint bar, defaults to CHECKPOINT with "" passed in
			HUD_COLOUR_YELLOW, 					 // CheckpointColour - the colour the bar should be
			-1, 								 // MeterNumber - if you want a meter displayed pass in the current value
			-1, 								 // MeterMaxNum - If you want a meter displayed pass in the max value
			"", 								 // MeterTitle - the meters title. Defaults to DAMAGE with "" Passed in
			HUD_COLOUR_YELLOW, 					 // MeterColour - The meter colour
			iBesTriTime,						 // BestTime - If you want to show a best time then pass in a millisecond value
			// CEIL(TRI_Global_BestTime_Get(TRI_Master.iRaceCur)*1000),
			szBestTriTimeLabel,					 // BestTimeTitle - Title of the best time timer. Defaults to BEST TIME with "" passed in			
			PODIUMPOS_NONE, 					 // MedalDisplay - If you want to show a medal postion next to the Best timer then pass in a position. Used for a target times.
			TRUE, 								 // DisplayMilliseconds - True if you want the main timer to display milliseconds
			-1,									 // FlashingTime - How long you want the whole hud to flash for.
			"",									 // Float title - NULL, default
			-1,									 // Float value - -1, default
			HUD_COLOUR_WHITE,					 // Float color - default
			HUD_COLOUR_WHITE,					 // abesttimecolor - default
			TRUE)								 // bDispTimeString - Display "BEST TIME" string instead of "BEST LAP"
		
		// Draw the player's intensity and energy bars in the race HUD.
		DRAW_PLAYER_TRI_INTENSITY_AND_ENERGY_METERS()
	ELSE
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	ENDIF
ENDPROC

// ===================================================
//  E N D  RACE HUD PROCEDURES AND FUNCTIONS
// ===================================================










// ===================================================
//  CAMERA PROCEDURES AND FUNCTIONS
// ===================================================

/// PURPOSE:
///    Clear TV cam help a few seconds after TV news camera activates.
PROC CLEAR_TV_NEWS_CAM_HELP_PRINT_WHEN_TV_NEWS_CAM_IS_ACTIVE()
	IF NOT IS_TIMER_STARTED(timerBeforeRemovingHint)
		START_TIMER_NOW(timerBeforeRemovingHint)
	ENDIF
	
	IF TIMER_DO_WHEN_READY(timerBeforeRemovingHint, 2.5)
		CLEAR_HELP(TRUE)
		bIsTVCamHintSetToBeCleared = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    Set which TV cam to activate.
///    
///    NOTE:	Tried shaking TV news cam, but never saw any shaking.
///    			Here are some of the enums for shaking, just in case:
///    				SMALL_EXPLOSION_SHAKE
///    				MEDIUM_EXPLOSION_SHAKE
///    				LARGE_EXPLOSION_SHAKE
///    				HAND_SHAKE
///    				JOLT_SHAKE
///    				VIBRATE_SHAKE
///    				WOBBLY_SHAKE
///    				DRUNK_SHAKE
PROC SET_TV_NEWS_CAM_ACTIVE(BOOL bIsTVCamActive)
	SET_CAM_ACTIVE(camTV_Top_Medium, bIsTVCamActive)
//	SWITCH (eCurrentTVCamAngle)
//		CASE TV_CAM_ANGLE_TOP_MEDIUM_RANGE
//			SET_CAM_ACTIVE(camTV_Top_Medium, bIsTVCamActive)
//			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SET_TV_NEWS_CAM_ACTIVE] Camera set to TV_CAM_ANGLE_TOP_MEDIUM_RANGE.")
//			
//			IF NOT bIsTVCamActive
//				eCurrentTVCamAngle = TV_CAM_ANGLE_TOP_FAR_RANGE
//			ENDIF 
//		BREAK
//		
//		CASE TV_CAM_ANGLE_TOP_FAR_RANGE
//			SET_CAM_ACTIVE(camTV_Top_Far, bIsTVCamActive)
//			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SET_TV_NEWS_CAM_ACTIVE] Camera set to TV_CAM_ANGLE_TOP_FAR_RANGE.")
//
//			IF NOT bIsTVCamActive
//				eCurrentTVCamAngle = TV_CAM_ANGLE_BEHIND_FAR_RANGE
//			ENDIF 
//		BREAK
//		
//		CASE TV_CAM_ANGLE_BEHIND_FAR_RANGE
//			SET_CAM_ACTIVE(camTV_Behind_Close, bIsTVCamActive)
//			//CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SET_TV_NEWS_CAM_ACTIVE] Camera set to TV_CAM_ANGLE_BEHIND_FAR_RANGE.")
//			
//			IF NOT bIsTVCamActive
//				eCurrentTVCamAngle = TV_CAM_ANGLE_TOP_MEDIUM_RANGE
//			ENDIF 
//		BREAK
//	ENDSWITCH
ENDPROC

PROC SHOW_TRI_NEWS_CAM_STATIC(BOOL bShowStatic)
	CPRINTLN(DEBUG_TRIATHLON, "SHOW_TRI_NEWS_CAM_STATIC called :: bShowStatic=", PICK_STRING(bShowStatic, "TRUE", "FALSE"))
	INT iStatic = PICK_INT(bShowStatic, 1, -1)
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "SHOW_STATIC")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStatic)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC CHANGE_TRI_NEWS_HEADLINE()
	iCurrentText++
	IF iCurrentText > 6		iCurrentText = 0		ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "DISPLAY_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentText)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Update the TV news camera based on player input.
PROC UPDATE_TRI_TV_NEWS_CAM_PLAYER_INPUT(TRI_RACE_STRUCT& Race)
	
	BOOL bFirstPerson = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
	
	IF IS_TRI_CONTROL_FLAG_SET(TCF_NEWS_CAM_ACTIVE)
		IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
			SET_PED_RESET_FLAG( PLAYER_PED_ID(), PRF_ForcePedToUseScripCamHeading, TRUE )
		ENDIF
		
		IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM) 
		AND (GET_TRI_CIN_CAM_TIMER(Race) >= (CIN_CAM_TIMER_THRESHOLD_LONG))
			CLEAR_TRI_CONTROL_FLAG(TCF_NEWS_CAM_ACTIVE)
			RESET_TRI_CIN_CAM_TIMER(Race)
			//Set multihead blinders off
			SET_MULTIHEAD_SAFE(FALSE,TRUE)
		ENDIF
	ENDIF
	
	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		INCREASE_TRI_CIN_CAM_TIMER(Race)
		
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		
			BOOL bCanPress = TRUE
			
			IF bFirstPerson
				bCanPress = GET_GAME_TIMER() > iCinCamTimer
			ENDIF
			
			IF bCanPress
				iCinCamTimer = GET_GAME_TIMER() + TRI_CAM_BUTTON_COOLDOWN
				IF IS_TRI_CONTROL_FLAG_SET(TCF_NEWS_CAM_ACTIVE)
					CLEAR_TRI_CONTROL_FLAG(TCF_NEWS_CAM_ACTIVE)
					RESET_TRI_CIN_CAM_TIMER(Race)
					//Set multihead blinders off
					SET_MULTIHEAD_SAFE(FALSE,TRUE)
				ELSE
					SET_TRI_CONTROL_FLAG(TCF_NEWS_CAM_ACTIVE)
					//Set multihead blinders on
					SET_MULTIHEAD_SAFE(TRUE,TRUE)
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
		
//	CPRINTLN(DEBUG_TRIATHLON, "GET_TRI_CIN_CAM_TIMER(", GET_TRI_CIN_CAM_TIMER(Race), ")")
	
	// Checking the button press ensures the overhead default cam isn't activated.
	IF IS_TRI_CONTROL_FLAG_SET(TCF_NEWS_CAM_ACTIVE)
		IF IS_TRI_CONTROL_FLAG_SET(TCF_FAIL_CHECKING)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRI_HELP_TV")
				IF NOT bIsTVCamHintSetToBeCleared
					bIsTVCamHintSetToBeCleared = TRUE
				ENDIF
			ENDIF
			
			IF IS_TRI_CONTROL_FLAG_SET(TCF_NEWS_CAST_INTERRUPTED) AND GET_GAME_TIMER() > Race.iNewsInterruptStamp
				CLEAR_TRI_CONTROL_FLAG(TCF_NEWS_CAST_INTERRUPTED)
				SHOW_TRI_NEWS_CAM_STATIC(FALSE)
			ENDIF
		
			SET_TV_NEWS_CAM_ACTIVE(TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			
//			OVERLAY_SCAN_LINES(TRUE, TRI_NEWS_SCAN_LINES)
			OVERLAY_SET_SECURITY_CAM(TRUE, FALSE, 30, 5, TRUE)
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(scformTVCam, 255,255,255,0)
			
			IF NOT IS_RADAR_HIDDEN()
				DISPLAY_RADAR(FALSE)
			ENDIF
			
			IF GET_GAME_TIMER() > Race.iNewsTickerTime
				CHANGE_TRI_NEWS_HEADLINE()
				Race.iNewsTickerTime = GET_GAME_TIMER() + 5000
			ENDIF
			
			bTVCam_Has_TV_Cam_Stopped = TRUE
			bDidCatchupAlready = FALSE
		ENDIF
	ELSE
		IF bCatchupDone
		AND NOT bDidCatchupAlready
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			bDidCatchupAlready = TRUE
		ENDIF
		
		IF bTVCam_Has_TV_Cam_Stopped
		
			SET_TV_NEWS_CAM_ACTIVE(FALSE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			bTVCam_Has_TV_Cam_Stopped 	= FALSE
		ENDIF
		
		IF IS_RADAR_HIDDEN()
			DISPLAY_RADAR(TRUE)
		ENDIF
	ENDIF

	// Clear TV cam help a few seconds after TV news camera activates.
	IF bIsTVCamHintSetToBeCleared
		CLEAR_TV_NEWS_CAM_HELP_PRINT_WHEN_TV_NEWS_CAM_IS_ACTIVE()
	ENDIF
ENDPROC

/// PURPOSE:
///    Create the TV news camera and scaleform objects.
///    
///    NOTE: Currently, every time the player presses the FOCUS
///    			(melee) button, the player will scroll through 
///    			the next cinematic camera angle (three total).
///    
///    NOTE: It would be awesome to somehow use the default
///    			cinematic camera behavior when holding the
///    			FOCUS button, and overlay the news scaleform
///    			on top of it, but I couldn't get this to work.
PROC CREATE_TRI_TV_NEWS_CAM(VEHICLE_INDEX &vehNewsHeli)
	// Set the starting state of the TV news camera.
//	eCurrentTVCamAngle = TV_CAM_ANGLE_TOP_MEDIUM_RANGE

	VECTOR vCamTV_TopMediumOffset 		= << -20.0, -15.0, 20.0 >>
//	VECTOR vCamTV_TopFarOffset 			= << 10.0, -30.0, 50.0 >>
//	VECTOR vCamTV_BehindMediumOffset 	= << 50.0, -15.0, 15.0 >>
	VECTOR vCamTV_UnderHeliOffset 		= << 0.0, 0.0, -2.0 >>

	VECTOR vCamTVPosition
	VECTOR vCamTVRotation = << 0.0, 0.0, 0.0 >>


	// Set up the TV top, medium-height cam.
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vCamTVPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 
							GET_ENTITY_HEADING(PLAYER_PED_ID()), vCamTV_TopMediumOffset)
	ENDIF
	
	IF NOT DOES_CAM_EXIST(camTV_Top_Medium)
		camTV_Top_Medium = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, 	vCamTVPosition, vCamTVRotation, 30, FALSE)
		ATTACH_CAM_TO_ENTITY(camTV_Top_Medium, 	vehNewsHeli, vCamTV_UnderHeliOffset, 		FALSE)
		POINT_CAM_AT_ENTITY(camTV_Top_Medium,	PLAYER_PED_ID(), << 0.0, 0.0, 0.0 >>,		TRUE)
	ENDIF
	
	
//	// Set up the TV top, far-height cam.
//	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//		vCamTVPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 
//							GET_ENTITY_HEADING(PLAYER_PED_ID()), vCamTV_TopFarOffset) 
//	ENDIF
//	
//	IF NOT DOES_CAM_EXIST(camTV_Top_Far)
//		camTV_Top_Far = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamTVPosition, vCamTVRotation, 30, FALSE)
//		ATTACH_CAM_TO_ENTITY(camTV_Top_Far, PLAYER_PED_ID(), vCamTV_TopFarOffset, 	FALSE)
//		POINT_CAM_AT_ENTITY(camTV_Top_Far, 	PLAYER_PED_ID(), << 0.0, 0.0, 0.0 >>, 	TRUE)
//	ENDIF
//	
//	
//	// Set up the TV behind, close cam.
//	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//		vCamTVPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 
//							GET_ENTITY_HEADING(PLAYER_PED_ID()), vCamTV_BehindMediumOffset) 
//	ENDIF
//	
//	IF NOT DOES_CAM_EXIST(camTV_Behind_Close)
//		camTV_Behind_Close = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamTVPosition, vCamTVRotation, 30, FALSE)
//		ATTACH_CAM_TO_ENTITY(camTV_Behind_Close, 	PLAYER_PED_ID(), vCamTV_BehindMediumOffset, 	FALSE)
//		POINT_CAM_AT_ENTITY(camTV_Behind_Close, 	PLAYER_PED_ID(), << 0.0, 0.0, 0.0 >>, 			TRUE)
//	ENDIF


	// Set up the breaking news title: 
	//		TRIATHLON COVERAGE
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "SET_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(szTVNews_Title)
	END_SCALEFORM_MOVIE_METHOD()
	
	STRING sSubTitle = PICK_STRING(eCurrentTriRace = TRIATHLON_RACE_ALAMO_SEA, "TRI_BASIC_1", PICK_STRING(eCurrentTriRace = TRIATHLON_RACE_VESPUCCI, "TRI_BASIC_2", "TRI_BASIC_3"))
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sSubTitle)
	END_SCALEFORM_MOVIE_METHOD()
	
	// Set up the breaking news bottom ticker of scrolling text: 
	//		TRIATHLON VIEWERS IN LOS SANTOS TOO LAZY TO LEAVE THEIR HOMES TO SUPPORT RACES IN PERSON.
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(szTVNews_Headline2)
	END_SCALEFORM_MOVIE_METHOD()
	
	//		LOS SANTOS POLICE REPORTS LOCAL CRIMINALS OFTEN PARTICIPATE IN SPORTING EVENTS WITH CASH REWARDS FOR SUPPLEMENTAL INCOME.
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(szTVNews_Headline3)
	END_SCALEFORM_MOVIE_METHOD()
	
	//		BURGER SHOT JOINS TRIATHLON SPONSORSHIP WITH NEW FOUR-DECKER-PATTY HEALTHY CHOICE BURGER.
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(szTVNews_Headline4)
	END_SCALEFORM_MOVIE_METHOD()	
	
	//		A LOS STANTOS RADIO COURTROOM SHOW INSPIRED BY 'JUST OR UNJUST' IS RUMORED TO BE IN THE WORKS.
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(szTVNews_Headline5)
	END_SCALEFORM_MOVIE_METHOD()

	//		INCIDENTS OF DOMESTIC VIOLENCE HAVE RISEN SHARPLY OVER THE PAST MONTH.
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(szTVNews_Headline6)
	END_SCALEFORM_MOVIE_METHOD()
	
	//		TEEN PREGNANCY RISES IN YOUTHS' SUPPORT OF KEEPING WEAKENING REALITY SHOW "TEENAGE MOMMA" ON THE AIR.
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(szTVNews_Headline7)
	END_SCALEFORM_MOVIE_METHOD()
	
	// Set up the breaking news top ticker of scrolling text: 
	//		RECENT TRIATHLON EVENTS PULL UNPRECEDENTED RATINGS AS TRADITIONAL PROFESSIONAL SPORTS REMAIN ON STRIKE. 
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(6)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(szTVNews_Headline1)
	END_SCALEFORM_MOVIE_METHOD()
	
	
	// Set up to display both top and bottom of scrolling text.
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "DISPLAY_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scformTVCam, "DISPLAY_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_RANDOM_INT_IN_RANGE(0,7))
	END_SCALEFORM_MOVIE_METHOD()
	
	
ENDPROC

/// PURPOSE:
///    Create the end-of-race scorecard cameras.
PROC CREATE_TRI_END_OF_RACE_SCORECARD_CAMERAS()
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->CREATE_TRI_END_OF_RACE_SCORECARD_CAMERAS] Procedure started.")

	SWITCH eCurrentTriRace
		CASE TRIATHLON_RACE_ALAMO_SEA
			IF NOT DOES_CAM_EXIST(camEndOfRace_Start)
				camEndOfRace_Start 	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1764.2440, 3888.2188, 80.1004 >>, << 6.3875, 0.0000, 46.1461 >>, 50.0, FALSE)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(camEndOfRace_End)
				camEndOfRace_End 	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1764.2440, 3888.2188, 80.1004 >>, << 6.3875, 0.0000, -16.5562 >>, 50.0, FALSE)
			ENDIF	
		BREAK
		
		CASE TRIATHLON_RACE_VESPUCCI
			IF NOT DOES_CAM_EXIST(camEndOfRace_Start)
				camEndOfRace_Start 	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1334.5372, -1034.4276, 37.5560 >>, << 8.4473, -0.0000, 99.2059 >>, 50.0, FALSE)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(camEndOfRace_End)
				camEndOfRace_End 	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1334.5372, -1034.4276, 37.5560 >>, << 8.4473, 0.0000, 49.8388 >>, 50.0, FALSE)
			ENDIF
		BREAK
		
		CASE TRIATHLON_RACE_IRONMAN
			IF NOT DOES_CAM_EXIST(camEndOfRace_Start)
				camEndOfRace_Start 	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -2301.4180, 495.3127, 224.8627 >>, << 6.5320, 0.0000, -132.4508 >>, 50.0, FALSE)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(camEndOfRace_End)
				camEndOfRace_End 	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -2301.4180, 495.3127, 224.8627 >>, << 6.5320, 0.0000, 169.8684 >>, 50.0, FALSE)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Create the camera for the player just completing the race.
PROC CREATE_TRI_PLAYER_FINISHING_RACE_CAMERA()
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->CREATE_TRI_PLAYER_FINISHING_RACE_CAMERA] Procedure started.")

	VECTOR vCamPlayerFinished_Pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, -15.0, 15.0 >>)
	VECTOR vCamPlayerFinished_Rot = << 0.0, 0.0, 0.0 >>
	
	IF NOT DOES_CAM_EXIST(camPlayerFinishedRace)
		camPlayerFinishedRace = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPlayerFinished_Pos, vCamPlayerFinished_Rot, 50, FALSE)
		ATTACH_CAM_TO_ENTITY(camPlayerFinishedRace, PLAYER_PED_ID(),  	<< -2.4646, 2.4270, 1.3 >>, 	TRUE)
		POINT_CAM_AT_ENTITY(camPlayerFinishedRace, 	PLAYER_PED_ID(),	<< 0.0606, -0.0400, 0.0745 >>, 		FALSE)
		SET_CAM_FOV(camPlayerFinishedRace, 50.0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Create Triathlon cameras.
PROC CREATE_TRI_CAMERAS(VEHICLE_INDEX &vehNewsHeli)
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->CREATE_TRI_CAMERAS] Procedure started.")

	// Create the camera for the player just completing the race.
	CREATE_TRI_PLAYER_FINISHING_RACE_CAMERA()
	
	// Create the end-of-race scorecard cameras.
	CREATE_TRI_END_OF_RACE_SCORECARD_CAMERAS()
	
	// Create the TV news camera and scaleform objects.
	CREATE_TRI_TV_NEWS_CAM(vehNewsHeli)
ENDPROC

/// PURPOSE:
///    Return true when if the camera is still showing the player.
FUNC BOOL Tri_Showing_Player_Finished_Race(INT iOutroSceneID)
	IF NOT bFinishedLookingAtPlayer
		IF GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneID) > 0.90
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers_AI.sch->Tri_Showing_Player_Finished_Race] Timer at ", GET_TIMER_IN_SECONDS(timerShowPlayerFinishedRace)," is ready.  Finish looking at the player.  Return FALSE")
//			SET_CAM_ACTIVE(camPlayerFinishedRace, FALSE)
//			RENDER_SCRIPT_CAMS(FALSE, TRUE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			bFinishedLookingAtPlayer = TRUE
			DISPLAY_RADAR(TRUE)
			RETURN FALSE
		ELIF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers_AI.sch->Tri_Showing_Player_Finished_Race] Timer at ", GET_TIMER_IN_SECONDS(timerShowPlayerFinishedRace),"is NOT ready  Keep showing the player.  Return TRUE")
			SET_TV_NEWS_CAM_ACTIVE(FALSE)
//			SET_CAM_ACTIVE(camPlayerFinishedRace, TRUE)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			DISPLAY_RADAR(FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Activate or deactivate end-of-race cutscene camera at the end of the race.
PROC SET_CUTSCENE_CAMERA_AT_END_OF_RACE(BOOL bActivateCam, BOOL bInterpToGameCam = TRUE)
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->SET_CUTSCENE_CAMERA_AT_END_OF_RACE] Procedure started.")

	IF bActivateCam
		SET_CAM_ACTIVE_WITH_INTERP(camEndOfRace_End, camEndOfRace_Start, 30000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
		RENDER_SCRIPT_CAMS(TRUE, TRUE, 7000)
	ELSE
		IF DOES_CAM_EXIST(camEndOfRace_Start)
			IF IS_CAM_ACTIVE(camEndOfRace_Start)
				SET_CAM_ACTIVE(camEndOfRace_Start, FALSE)
			ENDIF
		ENDIF
			
		IF DOES_CAM_EXIST(camEndOfRace_End)
			IF IS_CAM_ACTIVE(camEndOfRace_End)
				SET_CAM_ACTIVE(camEndOfRace_End, FALSE)
			ENDIF
		ENDIF
		RENDER_SCRIPT_CAMS(FALSE, (NOT IS_PED_INJURED(PLAYER_PED_ID())) AND bInterpToGameCam)
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Destroy TV news cameras.
PROC DESTROY_TRI_TV_NEWS_CAMERAS()
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_TV_CAMERAS] Procedure started.")

//	IF DOES_CAM_EXIST(camTV_Top_Far)
//		DESTROY_CAM(camTV_Top_Far)
//		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_TV_CAMERAS] Destroying camera: camTV_Top_Far.")
//	ENDIF
	
	IF DOES_CAM_EXIST(camTV_Top_Medium)
		DESTROY_CAM(camTV_Top_Medium)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_TV_CAMERAS] Destroying camera: camTV_Top_Medium.")
	ENDIF
	
//	IF DOES_CAM_EXIST(camTV_Behind_Close)
//		DESTROY_CAM(camTV_Behind_Close)
//		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_TV_CAMERAS] Destroying camera: camTV_Behind_Close.")
//	ENDIF
ENDPROC

/// PURPOSE:
///    Destroy TV news cameras.
PROC DESTROY_TRI_FINISHED_RACE_CAMERAS()
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_FINISHED_RACE_CAMERAS] Procedure started.")

	IF DOES_CAM_EXIST(camPlayerFinishedRace)
		DESTROY_CAM(camPlayerFinishedRace)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_FINISHED_RACE_CAMERAS] Destroying camera: camPlayerFinishedRace.")
	ENDIF
	
	IF DOES_CAM_EXIST(camEndOfRace_Start)
		DESTROY_CAM(camEndOfRace_Start)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_FINISHED_RACE_CAMERAS] Destroying camera: camEndOfRace_Start.")
	ENDIF
	
	IF DOES_CAM_EXIST(camEndOfRace_End)
		DESTROY_CAM(camEndOfRace_End)
		CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Cutscenes.sch->DESTROY_TRI_FINISHED_RACE_CAMERAS] Destroying camera: camEndOfRace_End.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Destroy Triathlon cameras.
PROC CLEANUP_TRI_CAMERAS(BOOL bInterpToGameCam = TRUE)
	CPRINTLN(DEBUG_TRIATHLON, "[TRI_Triathlon_Helpers.sch->DESTROY_TRI_CAMERAS] Procedure started.")
	IF IS_TRI_CONTROL_FLAG_SET(TCF_FINISHED_RACE)
		STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE)
	ENDIF
	//SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_FAR)
	
	DESTROY_TRI_TV_NEWS_CAMERAS()
	DESTROY_TRI_FINISHED_RACE_CAMERAS()
		
	// Deactivate end-of-race cutscene camera at the end of the race.
	SET_CUTSCENE_CAMERA_AT_END_OF_RACE(FALSE, bInterpToGameCam)
ENDPROC

// ===================================================
// 	E N D  CAMERA PROCEDURES AND FUNCTIONS
// ===================================================





// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
// 		END OF FILE - DO NOT ADD ANYTHING BELOW THIS BLOCK!
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************









