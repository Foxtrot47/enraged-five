// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	Triathlon_Race_Scoreboard.sch
//		AUTHOR			:	Troy Schram (TS), Carlos Mijares (CM)
//		DESCRIPTION		:	Race Complete scoreboards functionality.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************



// =====================================
// 	FILE INCLUDES
// =====================================

// -------------------------------------
// GENERAL INCLUDES
// -------------------------------------

USING "screens_header.sch"
USING "hud_drawing.sch"
USING "socialclub_leaderboard.sch"
USING "screen_placements.sch"
USING "screen_placements_export.sch"
USING "tri_race_scorecard.sch"
USING "tri_sc_leaderboard_lib.sch"
USING "tri_head.sch"
USING "minigame_big_message.sch"
USING "end_screen.sch"

// =====================================
// 	E N D  FILE INCLUDES
// =====================================


// End Screen Struct
END_SCREEN_DATASET thisEsd


// =====================================================
// 	TRIATHLON SCORECARD FUNCTIONS AND PROCEDURES
// =====================================================

/// PURPOSE:
///    Sets up the race's scorecard
PROC TRI_Init_Scorecard(TRI_RACE_STRUCT &Race)
	CPRINTLN(DEBUG_TRIATHLON, "[Triathlon_Race_Scorecard_lib.sch->TRI_Init_Scorecard]")
	RESET_ALL_SPRITE_PLACEMENT_VALUES(Race.uiScorecard.SpritePlacement)
	SET_STANDARD_INGAME_TEXT_DETAILS(Race.uiScorecard.aStyle)
	
	thisEsd.bHoldOnEnd = TRUE	
	
	INIT_SCREEN_TRIATHLON_MENU(Race.uiScorecard)
ENDPROC

PROC RELEASE_TRI_END_SCREEN()
	ENDSCREEN_SHUTDOWN(thisEsd)
ENDPROC

FUNC BOOL IS_TRI_END_SCREEN_READY()
	IF IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, TRI_ENDSCREEN_READY)
		RETURN TRUE
	ENDIF

	IF ENDSCREEN_PREPARE(thisEsd, FALSE)
		SET_BITMASK_AS_ENUM(iSPRGeneralBits, TRI_ENDSCREEN_READY)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DISPLAY_TRI_END_SCREEN()
	RETURN RENDER_ENDSCREEN(thisEsd)
ENDFUNC


/// PURPOSE:
///    populates the scorecard for the Triathlon Race
///    Uses https://devstar.rockstargames.com/wiki/index.php/MISSION_BOX#Dataset
PROC TRI_Draw_Scorecard(TRI_RACE_STRUCT &thisRace, INT iPlayerIndex)
//	CPRINTLN(DEBUG_TRIATHLON, "TRI_Draw_Scorecard :: Called")
	
	IF g_bResultScreenDisplaying
		SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	ENDIF
	
	IF NOT IS_TRI_CONTROL_FLAG_SET(TCF_END_MENU_CREATED)
		SET_TRI_CONTROL_FLAG(TCF_END_MENU_CREATED)
		
		// Big Message text
		STRING sRank
		SWITCH thisRace.Racer[iPlayerIndex].iRank
			CASE 1	sRank = "SPR_1stpl"		BREAK
			CASE 2	sRank = "SPR_2ndpl"		BREAK
			CASE 3	sRank = "SPR_3rdpl"		BREAK
			CASE 4	sRank = "SPR_4thpl"		BREAK
			CASE 5 	sRank = "SPR_5thpl"		BREAK
			CASE 6 	sRank = "SPR_6thpl"		BREAK
			CASE 7 	sRank = "SPR_7thpl"		BREAK
			CASE 8 	sRank = "SPR_8thpl"		BREAK
		ENDSWITCH
		SET_ENDSCREEN_DATASET_HEADER(thisEsd, sRank, TRI_Master.szRaceName[TRI_Master.iRaceCur])
		
		//ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(thisEsd, ESEF_RAW_STRING, "SPR_RACENAME", TRI_Master.szRaceName[TRI_Master.iRaceCur], 0, 1, ESCM_NO_MARK)
		//ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(thisEsd, ESEF_FRACTION, "SPR_POSITION", "", thisRace.Racer[iPlayerIndex].iRank, thisRace.iRacerCnt, ESCM_NO_MARK)
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(thisEsd, ESEF_TIME_M_S_MS_WITH_PERIOD, "SPR_TIME", "", ROUND(thisRace.Racer[iPlayerIndex].fClockTime * 1000), 0, ESCM_NO_MARK)
		
		STRING sMedal = "SPR_RESULT"
//		SWITCH thisRace.Racer[iPlayerIndex].iRank
//			CASE 1	sMedal = "SPR_GOLD_MED"		BREAK
//			CASE 2	sMedal = "SPR_SILVER_MED"	BREAK
//			CASE 3	sMedal = "SPR_BRONZE_MED"	BREAK
//			DEFAULT	sMedal = "SPR_NONE"			BREAK
//		ENDSWITCH
		IF (thisRace.Racer[iPlayerIndex].iRank >= 1)
		AND (thisRace.Racer[iPlayerIndex].iRank <= 3)
			END_SCREEN_MEDAL_STATUS eMedal
			SWITCH thisRace.Racer[iPlayerIndex].iRank
				CASE 1	eMedal = ESMS_GOLD			BREAK
				CASE 2	eMedal = ESMS_SILVER		BREAK
				CASE 3	eMedal = ESMS_BRONZE		BREAK
				//DEFAULT	eMedal = ESMS_NO_MEDAL		BREAK
			ENDSWITCH
			SET_ENDSCREEN_COMPLETION_LINE_STATE(thisEsd, TRUE, sMedal, thisRace.Racer[iPlayerIndex].iRank, thisRace.iRacerCnt, ESC_FRACTION_COMPLETION, eMedal)
		ENDIF
		
		IS_TRI_END_SCREEN_READY()
		
//		ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "TRI_CONT")	
//		ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_LEADERBOARD, "SPR_SCLB")
	ENDIF
	
//	DRAW_MENU(TRUE, -1, TRUE)
ENDPROC

// =====================================================
// 	E N D  TRIATHLON SCORECARD FUNCTIONS AND PROCEDURES
// =====================================================

// =====================================================
// 	TRIATHLON LEADERBOARD FUNCTIONS AND PROCEDURES
// =====================================================
//


PROC TRI_Draw_Leaderboard(TRI_RACE_STRUCT &Race, TRIATHLON_RACE_INDEX eRace)
	TRI_DISPLAY_SOCIAL_CLUB_LEADERBOARD(Race.uiLeaderboard, eRace)
	
	IF NOT g_bResultScreenDisplaying
		SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
	ENDIF
ENDPROC

PROC TRI_Cleanup_Leaderboard(TRI_RACE_STRUCT &Race)
	TRI_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
	CLEANUP_SC_LEADERBOARD_UI(Race.uiLeaderboard)
ENDPROC



///   CHANGELOG
///    
///    09/17/2012
///    	- [AmD] Added placeholder leaderboard funcs
///    
///    10/26/2011
///    	- [CM] Copied over TRIATHLON_race_scorecard_lib.sch.
///     - [CM] Replaced TRIATHLON_race_scorecard.sch for Triathlon_Race_scorecard_lib.sch.






// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
// 		END OF FILE - DO NOT ADD ANYTHING BELOW THIS BLOCK!
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
