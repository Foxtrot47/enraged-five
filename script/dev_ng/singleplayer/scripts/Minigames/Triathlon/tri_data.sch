//****************************************************************************************************
//
//	Author: 		Asa Dang, modified by Troy Schram
//	Date: 			6/24/11
//	Description:	Contains shared funcs and procs for data management in the Stunt Plane Races, including: 
//					load/save player data and retrieving any stored data (strings, scores, times, etc)
//	
//****************************************************************************************************






USING "globals.sch"
USING "rage_builtins.sch"




//INFO: 
//PARAM NOTES: The index (BitToCheckIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Checks if the integer (IntToCheck) passed has the bit set at the index (BitToCheckIndex) passed.More info..
FUNC BOOL TRI_Data_Is_Goal_Bit_Set(TRI_STRUCT &Variable, TRI_GOAL_BITS BitIndex)
	RETURN IS_BIT_SET(Variable.goals, ENUM_TO_INT(BitIndex))
ENDFUNC

//INFO: 
//PARAM NOTES:The index (BitToSetIndex) passed must be between 0 and 31 or the command will ASSERT.
//PURPOSE: Sets the bit at the index (BitToSetIndex) of the integer (IntToModify) passed. More info..
PROC TRI_Data_Set_Goal_Bit(TRI_STRUCT &Variable, TRI_GOAL_BITS BitIndex)
	SET_BIT(Variable.goals, ENUM_TO_INT(BitIndex))
ENDPROC

//INFO: 
//PARAM NOTES:The index (BitToClearIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Clear the bit at the index (BitToClearIndex) of the integer (IntToModify) passed. More info..
PROC TRI_Data_Clear_Goal_Bit(TRI_STRUCT &Variable, TRI_GOAL_BITS BitIndex)
	CLEAR_BIT(Variable.goals, ENUM_TO_INT(BitIndex))
ENDPROC

//INFO: 
//PARAM NOTES: The index (BitToCheckIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Checks if the integer (IntToCheck) passed has the bit set at the index (BitToCheckIndex) passed.More info..
FUNC BOOL TRI_Data_Is_PreReq_Bit_Set(TRI_STRUCT &Variable, TRI_Races_ENUM BitIndex)
	RETURN IS_BIT_SET(Variable.preReq, ENUM_TO_INT(BitIndex))
ENDFUNC

//INFO: 
//PARAM NOTES:The index (BitToSetIndex) passed must be between 0 and 31 or the command will ASSERT.
//PURPOSE: Sets the bit at the index (BitToSetIndex) of the integer (IntToModify) passed. More info..
PROC TRI_Data_Set_PreReq_Bit(TRI_STRUCT &Variable, TRI_Races_ENUM BitIndex)
	SET_BIT(Variable.preReq, ENUM_TO_INT(BitIndex))
ENDPROC
//INFO: 

//PARAM NOTES:The index (BitToClearIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Clear the bit at the index (BitToClearIndex) of the integer (IntToModify) passed. More info..
PROC TRI_Data_Clear_PreReq_Bit(TRI_STRUCT &Variable, TRI_Races_ENUM BitIndex)
	CLEAR_BIT(Variable.preReq, ENUM_TO_INT(BitIndex))
ENDPROC

#IF IS_DEBUG_BUILD
//
	PROC SAVE_TRI_STRUCT_TO_NAMED_DEBUG_FILE(TRI_STRUCT &sprStruct, string sFilePath, string sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		<RecordNode name = \"", sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(sprStruct.name, sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" status = \"", sFilePath, sFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(ENUM_TO_INT(sprStruct.status), sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" goals = \"", sFilePath, sFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(sprStruct.goals, sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" preReq = \"", sFilePath, sFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(sprStruct.preReq, sFilePath, sFileName)
				
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"  />		<!--  ", sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(sprStruct.name), sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" -->", sFilePath, sFileName)
		
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		
	ENDPROC
	PROC SAVE_TRI_STRUCT_TO_DEBUG_FILE(TRI_STRUCT &sprStruct)
		string sFilePath	= "X:/gta5/build/dev"
		string sFileName	= "temp_debug.txt"
		
		SAVE_TRI_STRUCT_TO_NAMED_DEBUG_FILE(sprStruct, sFilePath, sFileName)
	ENDPROC
#ENDIF


