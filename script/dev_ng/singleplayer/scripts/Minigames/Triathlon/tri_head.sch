// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	TRI_Head.sch
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Single Player Races - Head global data file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "minigames_helpers.sch"
//USING "minigame_uiinputs.sch"
USING "script_usecontext.sch"
USING "commands_xml.sch"
USING "commands_water.sch"
USING "screen_placements.sch"
USING "minigame_big_message.sch"
USING "timer_globals.sch"

// Variables
INT iControlFlags
INT iCinCamTimer
CONST_INT	TRI_CAM_BUTTON_COOLDOWN			500
BLIP_INDEX blipPlayerVehicle

TWEAK_INT	TRI_BLOCK_QUIT_INPUT_DURATION	1000
CONST_INT	TRI_ENABLE_WIDGETS				0				// 1 to use widgets


#IF NOT DEFINED(TRI_RACE_IS_TRIATHLON)
	CONST_INT TRI_RACE_IS_TRIATHLON			0	
#ENDIF


// -----------------------------------
// CONSTANTS
// -----------------------------------

#IF IS_DEBUG_BUILD
CONST_INT	TRI_DEBUG_DISPLAY_TEXT					0
CONST_FLOAT TRI_GATE_CHKPNT_OFFROAD_ARROW_SCL		3.0			// Gate checkpoint size.
CONST_FLOAT TRI_GATE_CHKPNT_OFFROAD_CYLINDER_SCL	8.0			// Gate next checkpoint size.
CONST_FLOAT TRI_GATE_CHKPNT_OFFROAD_FLAG_SCL		4.0			// Gate next checkpoint size.

CONST_FLOAT TRI_GATE_CHKPNT_STUNT_ARROW_SCL		3.0			// Gate checkpoint size.
CONST_FLOAT TRI_GATE_CHKPNT_STUNT_PLANE_SCL		5.0			// Gate checkpoint size.
CONST_FLOAT TRI_GATE_CHKPNT_STUNT_RING_SCL		8.0			// Gate next checkpoint size.
CONST_FLOAT TRI_GATE_CHKPNT_STUNT_FLAG_SCL		4.0			// Gate next checkpoint size.

CONST_FLOAT TRI_GATE_CHKPNT_TRI_ARROW_SCL		3.0			// Gate checkpoint size.
CONST_FLOAT TRI_GATE_CHKPNT_TRI_CYLINDER_SCL	8.0			// Gate next checkpoint size.
CONST_FLOAT TRI_GATE_CHKPNT_TRI_FLAG_SCL		4.0			// Gate next checkpoint size.
#ENDIF

#IF NOT DEFINED(TRI_GATE_MAX)
CONST_INT 	TRI_GATE_MAX 					33				// Max race gates.
#ENDIF
CONST_INT	TRI_RACER_MAX					8				// Max race racers.
CONST_INT	TRI_FADE_LONG_TIME				5000			// Fade time (long) (ms).
CONST_INT	TRI_FADE_NORM_TIME				1000			// Fade time (normal) (ms).
CONST_INT	TRI_FADE_QUICK_TIME				500				// Fade time (quick) (ms).
CONST_FLOAT	TRI_CLEAR_AREA_RADIUS			30.0			// Clear area radius (m).
CONST_FLOAT	TRI_ON_GROUND_DIST				10.0			// On ground distance (m).
CONST_FLOAT TRI_GATE_CHKPNT_SCL				10.25			// Gate checkpoint size.
CONST_FLOAT TRI_GATE_CHKPNTNXT_SCL			7.0				// Gate next checkpoint size.
CONST_FLOAT TRI_GATE_BLIPCUR_SCL			1.0				// Gate current blip scale.
CONST_FLOAT TRI_GATE_BLIPNXT_SCL			0.75			// Gate next blip scale.
CONST_FLOAT TRI_GATE_MISS_DIST_SCL			5.0				// Gate miss distance scale.
CONST_FLOAT TRI_GATE_MISS_PENALTY			5.0				// Gate miss penalty (s).
CONST_FLOAT TRI_GATE_INNER_PASS_BONUS		1.0				// Gate inner pass bonus (s).
CONST_FLOAT TRI_GATE_OUTTER_PASS_BONUS		0.0				// Gate Outter pass bonus (s).
CONST_FLOAT TRI_GATE_OUTTER_KNIFE_BONUS		1.5				// Gate Outter Knife bonus (s).
CONST_FLOAT TRI_GATE_OUTTER_INVERT_BONUS	2.0				// Gate Outter Invert bonus (s).
CONST_FLOAT TRI_GATE_INNER_KNIFE_BONUS		2.5				// Gate Inner Knife bonus (s).
CONST_FLOAT TRI_GATE_INNER_INVERT_BONUS		3.0				// Gate Outter pass bonus (s).
CONST_FLOAT	TRI_GATE_STUNT_BONUS			4.0				// Stunt Gate hit bonus
CONST_FLOAT	TRI_GATE_STUNT_HIT				1.5				// Stunt Gate failure to do stunt
CONST_FLOAT TRI_GATE_WRONG_PENALTY			2.5				// Gate miss penalty (s).
CONST_INT	TRI_GATE_MISS_DISP_TIME			2500 			// Gate miss display time (ms).
CONST_FLOAT TRI_GATE_HEIGHT_CHECK			10.0			// Gate height check - to fix Bug # 666112 - DS
CONST_FLOAT TRI_WATER_RESET_OFFSET			-1.0			// Z offset for resetting player on a swimming checkpoint (m) -AsD (B* 1184990)

CONST_FLOAT TRI_STUNT_INVERT_MIN	-140.0			// The min amount the player needs to get the invert stunt bonus
CONST_FLOAT TRI_STUNT_INVERT_MAX	140.0			// The max amount the player can be inverted to get the invert stunt bonus

CONST_INT	TRI_VEH_STUCK_PLANE_ROOF_TIME	1000	// Vehicle stuck time (roof) (ms).
CONST_INT	TRI_VEH_STUCK_PLANE_SIDE_TIME	1000	// Vehicle stuck time (side) (ms).
CONST_INT	TRI_VEH_STUCK_PLANE_HUNG_TIME	2000	// Vehicle stuck time (hung) (ms).
CONST_INT	TRI_VEH_STUCK_PLANE_JAM_TIME	2000	// Vehicle stuck time (jam) (ms).

CONST_INT	TRI_VEH_STUCK_BIKE_ROOF_TIME	5000	// Vehicle stuck time (roof) (ms).
CONST_INT	TRI_VEH_STUCK_BIKE_SIDE_TIME	20000	// Vehicle stuck time (side) (ms).
CONST_INT	TRI_VEH_STUCK_BIKE_HUNG_TIME	30000	// Vehicle stuck time (hung) (ms).
CONST_INT	TRI_VEH_STUCK_BIKE_JAM_TIME		60000	// Vehicle stuck time (jam) (ms).

CONST_INT 	TRI_XML_HASH_MASTER		-232937294		// XML hash key for "Master" string.
CONST_INT 	TRI_XML_HASH_RACES		993425358		// XML hash key for "Races" string.
CONST_INT 	TRI_XML_HASH_RACE		-1423748980		// XML hash key for "Race" string.
CONST_INT 	TRI_XML_HASH_FILENAME	1087019066		// XML hash key for "FileName" string.
CONST_INT 	TRI_XML_HASH_NAME		-421429484		// XML hash key for "Name" string.
CONST_INT 	TRI_XML_HASH_TIME		258444835		// XML hash key for "Time" string.
CONST_INT 	TRI_XML_HASH_GATES		-1448335268		// XML hash key for "Gates" string.
CONST_INT 	TRI_XML_HASH_GATE		145736838		// XML hash key for "Gate" string.
CONST_INT 	TRI_XML_HASH_POS		18243940		// XML hash key for "Position" string.
CONST_INT	TRI_XML_HASH_CHKPNT		302512246		// XML hash key for "checkpoint" string
CONST_INT 	TRI_XML_HASH_RACERS		-1335331260		// XML hash key for "Racers" string.
CONST_INT 	TRI_XML_HASH_RACER		361770114		// XML hash key for "Racer" string.
CONST_INT 	TRI_XML_HASH_LOC		-71226926		// XML hash key for "Location" string.
CONST_INT 	TRI_XML_HASH_DRIVER		2096477759		// XML hash key for "Driver" string.
CONST_INT 	TRI_XML_HASH_VEHICLE	-584819812		// XML hash key for "Vehicle" string.
CONST_INT 	TRI_XML_HASH_X			-1828477467		// XML hash key for "X" string.
CONST_INT 	TRI_XML_HASH_Y			-2137718520		// XML hash key for "Y" string
CONST_INT 	TRI_XML_HASH_Z			-1235194722		// XML hash key for "Z" string.
CONST_INT 	TRI_XML_HASH_H			1879302122		// XML hash key for "Z" string.
CONST_INT	TRI_XML_HASH_RADIUS		1337695475		// XML hash key for "Radius" string.
CONST_INT 	TRI_XML_HASH_TYPE		828747869		// XML hash key for "Type" string.
CONST_INT 	TRI_XML_HASH_STUNT		-2122757008		// XML hash key for "Stunt" string.
CONST_INT 	TRI_XML_HASH_MODEL		1816327236		// XML hash key for "Model" string.

CONST_FLOAT	TRI_HUD_NUM_WIDTH		0.0165			// HUD number width for offsetting.
CONST_FLOAT	TRI_HUD_CLN_WIDTH		0.0265			// HUD colon width for offsetting.
CONST_FLOAT	TRI_HUD_PLS_WIDTH		0.0175			// HUD plus width for offsetting.
CONST_FLOAT	TRI_HUD_MNS_WIDTH		0.0095			// HUD minus width for offsetting.
CONST_FLOAT	TRI_HUD_RADAR_POS_X		0.109 			// HUD radar overlay pos (x).
CONST_FLOAT	TRI_HUD_RADAR_POS_Y		0.850 			// HUD radar overlay pos (y).
CONST_FLOAT	TRI_HUD_RADAR_WIDTH		0.110			// HUD radar overlay width.
CONST_FLOAT	TRI_HUD_RADAR_HEIGHT	0.200 			// HUD radar overlay height.
CONST_FLOAT	TRI_HUD_RADAR_ROT_SCL	-1.0 			// HUD radar overlay rot scale.

CONST_FLOAT TRI_UI_CR_HEAD_POS_X	0.4				// UI choose race heading pos (x).
CONST_FLOAT TRI_UI_CR_HEAD_POS_Y	0.05			// UI choose race heading pos (y). (formally .15)
CONST_FLOAT TRI_UI_CR_HEAD_SCALE	0.8				// UI choose race heading scale (x/y). (formally 1.0)
CONST_FLOAT TRI_UI_CR_INFO_POS_X	0.325			// UI choose race info pos (x).
CONST_FLOAT TRI_UI_CR_INFO_POS_Y	0.11			// UI choose race info pos (y). (formally .25)
CONST_FLOAT TRI_UI_CR_INFO_SCALE	0.5				// UI choose race info scale (x/y).
CONST_FLOAT TRI_UI_CR_INFO_SPC_X1	0.23			// UI choose race info space (x1).
CONST_FLOAT TRI_UI_CR_INFO_SPC_X2	0.282			// UI choose race info space (x2).
CONST_FLOAT TRI_UI_CR_INFO_SPC_Y	0.052			// UI choose race info space (y).
CONST_FLOAT TRI_UI_CR_SEL_POS_X		0.32			// UI choose race select pos (x).
CONST_FLOAT TRI_UI_CR_SEL_POS_Y		0.895			// UI choose race select pos (y). (formally .81)
CONST_FLOAT TRI_UI_CR_SEL_SCALE		0.625			// UI choose race select scale (x/y).
CONST_FLOAT TRI_UI_CR_ENT_POS_X		0.435			// UI choose race enter pos (x).
CONST_FLOAT TRI_UI_CR_ENT_POS_Y		0.895			// UI choose race enter pos (y). (formally .81)
CONST_FLOAT TRI_UI_CR_ENT_SCALE		0.625			// UI choose race enter scale (x/y).
CONST_FLOAT TRI_UI_CR_EXIT_POS_X	0.614			// UI choose race exit pos (x).
CONST_FLOAT TRI_UI_CR_EXIT_POS_Y	0.895			// UI choose race exit pos (y). (formally .81)
CONST_FLOAT TRI_UI_CR_EXIT_SCALE	0.625			// UI choose race exit scale (x/y).
CONST_FLOAT TRI_UI_CR_RECT_POS_X	0.5				// UI choose race rectangle pos (x).
CONST_FLOAT TRI_UI_CR_RECT_POS_Y	0.3				// UI choose race rectangle pos (y). (formally .5)
CONST_FLOAT TRI_UI_CR_RECT_WIDTH	0.385			// UI choose race rectange width.
CONST_FLOAT TRI_UI_CR_RECT_HEIGHT	1.30			// UI choose race rectangle height.
CONST_INT 	TRI_UI_CR_RECT_ALPHA	191				// UI choose race rectange alpha.

CONST_FLOAT TRI_UI_CD_NUM_SCALE		2.0				// UI countdown number scale for both x/y.
CONST_FLOAT TRI_UI_CD_NUM_POS_X		0.48			// UI countdown number start position (x).
CONST_FLOAT TRI_UI_CD_NUM_POS_Y		0.4				// UI countdown number start position (y).
CONST_FLOAT TRI_UI_CD_NUM_TIME		1.25			// UI countdown number display for time (s).
CONST_FLOAT TRI_UI_CD_GO_SCALE		2.0				// UI countdown "GO!" scale for both x/y.
CONST_FLOAT TRI_UI_CD_GO_POS_X		0.44			// UI countdown "GO!" start position (x).
CONST_FLOAT TRI_UI_CD_GO_POS_Y		0.4				// UI countdown "GO!" start position (y).
CONST_FLOAT TRI_UI_CD_GO_TIME		1.0				// UI countdown "GO!" display for time (s).

CONST_FLOAT TRI_UI_FINISH_SCALE		2.0				// UI finish scale for both x/y.
CONST_FLOAT TRI_UI_FINISH_POS_X		0.4				// UI finish start position (x).
CONST_FLOAT TRI_UI_FINISH_POS_Y		0.4				// UI finish start position (y).
CONST_FLOAT TRI_UI_FINISH_TIME		3.0				// UI finish display for time (s).

CONST_FLOAT TRI_UI_LB_HEAD_POS_X	0.4				// UI leaderboard heading pos (x).
CONST_FLOAT TRI_UI_LB_HEAD_POS_Y	0.15			// UI leaderboard heading pos (y).
CONST_FLOAT TRI_UI_LB_HEAD_SCALE	1.0				// UI leaderboard heading scale (x/y).
CONST_FLOAT TRI_UI_LB_INFO_POS_X	0.325			// UI leaderboard info pos (x).
CONST_FLOAT TRI_UI_LB_INFO_POS_Y	0.21			// UI leaderboard info pos (y).
CONST_FLOAT TRI_UI_LB_INFO_SCALE	0.5				// UI leaderboard info scale (x/y).
CONST_FLOAT TRI_UI_LB_INFO_SPC_X1	0.23			// UI leaderboard info space (x1).
CONST_FLOAT TRI_UI_LB_INFO_SPC_X2	0.282			// UI leaderboard info space (x2).
CONST_FLOAT TRI_UI_LB_INFO_SPC_Y	0.052			// UI leaderboard info space (y).
CONST_FLOAT TRI_UI_LB_CONT_POS_X	0.32			// UI leaderboard select pos (x).
CONST_FLOAT TRI_UI_LB_CONT_POS_Y	0.81			// UI leaderboard select pos (y).
CONST_FLOAT TRI_UI_LB_CONT_SCALE	0.625			// UI leaderboard select scale (x/y).
CONST_FLOAT TRI_UI_LB_EXIT_POS_X	0.614			// UI leaderboard exit pos (x).
CONST_FLOAT TRI_UI_LB_EXIT_POS_Y	0.81			// UI leaderboard exit pos (y).
CONST_FLOAT TRI_UI_LB_EXIT_SCALE	0.625			// UI leaderboard exit scale (x/y).
CONST_FLOAT TRI_UI_LB_RECT_POS_X	0.5				// UI leaderboard rectangle pos (x).
CONST_FLOAT TRI_UI_LB_RECT_POS_Y	0.5				// UI leaderboard rectangle pos (y).
CONST_FLOAT TRI_UI_LB_RECT_WIDTH	0.375			// UI leaderboard rectange width.
CONST_FLOAT TRI_UI_LB_RECT_HEIGHT	0.750			// UI leaderboard rectangle height.
CONST_INT 	TRI_UI_LB_RECT_ALPHA	191				// UI leaderboard rectange alpha.
CONST_INT 	TRI_UI_LB_RACER_LIMIT	10				// UI leaderboard racer limit.

CONST_INT	TRI_UI_TIME_THROTTLE	10				// UI throttle for updating the race time

CONST_INT	TRI_INPUT_STICK_LIMIT	128				// INPUT stick limit for both x/y.
CONST_INT	TRI_INPUT_STICK_DZ_X	32				// INPUT stick dead zone (x).
CONST_INT	TRI_INPUT_STICK_DZ_Y	32				// INPUT stick dead zone (x).

CONST_FLOAT TRI_CAM_INTERP_SPEED	100.0			// CAM interp speed (m/s).
CONST_INT 	TRI_CAM_INTERP_TIME_MIN	2000			// CAM interp time max (ms).
CONST_INT 	TRI_CAM_INTERP_TIME_MAX	6000			// CAM interp time max (ms).
CONST_FLOAT	TRI_CAM_ROTATE_INC		2.0				// CAM rotate increment (deg).
CONST_FLOAT	TRI_CAM_ROTATE_LIMIT	89.0			// CAM rotate limit (deg).

CONST_FLOAT	TRI_CAM_ZOOM_INC		0.1				// CAM zoom increment (scalar).
CONST_FLOAT	TRI_CAM_ZOOM_LIMIT_MIN	0.5				// CAM zoom min limit (scalar).
CONST_FLOAT	TRI_CAM_ZOOM_LIMIT_MAX	5.0				// CAM zoom max limit (scalar).
CONST_FLOAT TRI_CAM_GATE_FOCUS_DIST 50.0			// CAM gate focus distance (m).
CONST_FLOAT TRI_CAM_RACER_FOCUS_DIST 10.0			// CAM racer focus distance (m).

CONST_FLOAT	TRI_PLANE_SPD_MAX		60.0			// Plane speed max(m/s).
CONST_FLOAT	TRI_TRICK_ROT_MIN		0.0				// Trick rotation min (deg).
CONST_FLOAT	TRI_TRICK_ROT_MID		180.0			// Trick rotation mid (deg).
CONST_FLOAT	TRI_TRICK_ROT_MAX		360.0			// Trick rotation max (deg).
CONST_FLOAT	TRI_TRICK_ROT_JUMP		90.0			// Trick rotation jump (deg).
CONST_FLOAT	TRI_TRICK_ROT_BUFF		30.0			// Trick rotation buffer (deg).
CONST_FLOAT	TRI_TRICK_LOOP_MIN		1.0				// Trick loop bonus min (s).
CONST_FLOAT	TRI_TRICK_LOOP_MAX		15.0			// Trick loop bonus max (s).
CONST_FLOAT	TRI_TRICK_ROLL_MIN		0.2				// Trick roll bonus min (s).
CONST_FLOAT	TRI_TRICK_ROLL_MAX		3.0				// Trick roll bonus max (s).
CONST_FLOAT	TRI_TRICK_INVT_TIME		10.0			// Trick invert time (s).
CONST_FLOAT	TRI_TRICK_INVT_MIN		0.4				// Trick invert bonus min (s).
CONST_FLOAT	TRI_TRICK_INVT_MAX		6.0				// Trick invert bonus max (s).
CONST_FLOAT	TRI_TRICK_DEGRADE		2.0				// Trick bonus degrade (^x).
CONST_INT	TRI_TRICK_DISP_TIME		2500 			// Trick display time (ms).

CONST_FLOAT	CIN_CAM_TIMER_THRESHOLD			0.200		// Threshold for going into cinematic cam
CONST_FLOAT	CIN_CAM_TIMER_THRESHOLD_LONG	1.000		// Threshold for going back from cinematic cam, like Alwyn's stuff does

CONST_INT TRI_EVENT_QUEUE_COUNT		15				// Number of events we can hold in the queue
CONST_INT TRI_QUEUE_CLEAN_TIME		20000			// 20 seconds til we clean the queue
CONST_FLOAT TRI_RANK_SAMPLE_TIME	2.0



// -----------------------------------
// ENUMERATIONS
// -----------------------------------

// SPR Main Run.
ENUM TRI_MAIN_RUN_ENUM
	TRI_MAIN_RUN_SETUP,
	TRI_MAIN_RUN_UPDATE,
	TRI_MAIN_RUN_CLEANUP
ENDENUM

// SPR Main Setup.
ENUM TRI_MAIN_SETUP_ENUM
	TRI_MAIN_SETUP_INIT,
	TRI_MAIN_SETUP_FADE_OUT,
	TRI_MAIN_SETUP_LOAD_INIT,
	TRI_MAIN_SETUP_LOAD_WAIT,
	TRI_MAIN_SETUP_CREATE_INIT,
	TRI_MAIN_SETUP_CREATE_WAIT,
	TRI_MAIN_SETUP_PLACE_INIT,
	TRI_MAIN_SETUP_PLACE_WAIT,
	TRI_MAIN_SETUP_FADE_IN,
	TRI_MAIN_SETUP_WAIT,
	TRI_MAIN_SETUP_CLEANUP
ENDENUM

// SPR Main Udpate.
ENUM TRI_MAIN_UPDATE_ENUM
	TRI_MAIN_UPDATE_INIT,
	TRI_MAIN_UPDATE_GET_IN_INIT,
	TRI_MAIN_UPDATE_GET_IN_WAIT,
	TRI_MAIN_UPDATE_CHOOSE_RACE,
	TRI_MAIN_RESETTING_LOAD,
	TRI_MAIN_UPDATE_ORCUT_LOAD,
	TRI_MAIN_QUIT_FADE_TELEPORT,
	TRI_MAIN_UPDATE_FADE_OUT,
	TRI_MAIN_UPDATE_SETUP_RACE_INIT,
	TRI_MAIN_UPDATE_SETUP_RACE_WAIT,
	TRI_MAIN_UPDATE_FADE_IN,
	TRI_MAIN_UPDATE_WAIT,
	TRI_MAIN_UPDATE_PRECLEANUP_DELAY,
	TRI_MAIN_RESETTING_LOAD_FADE,
	TRI_MAIN_UPDATE_CLEANUP
ENDENUM

// SPR Camera Mode.
ENUM TRI_CAMERA_MODE_ENUM
	TRI_CAMERA_MODE_GAME,
	TRI_CAMERA_MODE_FREE,
	TRI_CAMERA_MODE_FLY
ENDENUM

// SPR Camera Focus.
ENUM TRI_CAMERA_FOCUS_ENUM
	TRI_CAMERA_FOCUS_GATES,
	TRI_CAMERA_FOCUS_RACERS
ENDENUM

// SPR Move Mode.
ENUM TRI_MOVE_MODE_ENUM
	TRI_MOVE_MODE_CAMERA,
	TRI_MOVE_MODE_GROUND,
	TRI_MOVE_MODE_PREV,
	TRI_MOVE_MODE_NEXT,
	TRI_MOVE_MODE_SELECT
ENDENUM

// SPR Racer Reset.
ENUM TRI_RACER_RESET_ENUM
	TRI_RACER_RESET_FAIL_OVER,
	TRI_RACER_RESET_FAIL_SELECT_RETRY,
	TRI_RACER_RESET_FAIL_HINT,
	TRI_RACER_RESET_INIT,
	TRI_RACER_RESET_CHOOSE,
	TRI_RACER_RESET_FADE_OUT,
	TRI_RACER_QUIT_FADE_OUT,
	TRI_RACER_RESET_CREATE,
	TRI_RACER_RESET_PLACE,
	TRI_RACER_QUIT_PLACE,
	TRI_RACER_RESET_FADE_IN,
	TRI_RACER_QUIT_FADE_IN,
	TRI_RACER_RESET_WAIT,
	TRI_RACER_QUIT_EXIT
ENDENUM

// SPR Race Type.
ENUM TRI_RACE_TYPE_ENUM
	TRI_RACE_TYPE_PLANE,
	TRI_RACE_TYPE_OFFROAD,
	TRI_RACE_TYPE_TRIATHLON
ENDENUM

// SPR Race Setup.
ENUM TRI_RACE_SETUP_ENUM
	TRI_RACE_SETUP_INIT,
	TRI_RACE_SETUP_LOAD_INIT,
	TRI_RACE_SETUP_LOAD_WAIT,
	TRI_RACE_SETUP_CREATE_INIT,
	TRI_RACE_SETUP_CREATE_WAIT,
	TRI_RACE_SETUP_WAIT,
	TRI_RACE_SETUP_CLEANUP
ENDENUM

// SPR Race Update.
ENUM TRI_RACE_UPDATE_ENUM
	TRI_RACE_UPDATE_INIT,
	TRI_RACE_UPDATE_COUNTDOWN,
	TRI_RACE_UPDATE_FINISH,
	TRI_RACE_UPDATE_LEADERBOARD,
	TRI_RACE_UPDATE_WAIT_ANIM_OUT,
	TRI_RACE_UPDATE_FADE_OUT_BEFORE_FINISH,
	TRI_RACE_UPDATE_FADE_IN_BEFORE_FINISH,
	TRI_RACE_UPDATE_WAIT,
	TRI_RACE_UPDATE_CLEANUP
ENDENUM

// SPR Race Gate stunt types
ENUM TRI_RACE_STUNT_GATE_ENUM
	TRI_RACE_STUNT_GATE_NORMAL,
	TRI_RACE_STUNT_GATE_INVERTED, 
	TRI_RACE_STUNT_GATE_SIDE_LEFT,
	TRI_RACE_STUNT_GATE_SIDE_RIGHT
ENDENUM

//SPR Gate check types
ENUM TRI_RACE_GATE_STATUS
	TRI_RACE_GATE_STATUS_INVALID = -1,
	TRI_RACE_GATE_STATUS_PASS
ENDENUM

//SPR Gate check types
ENUM TRI_RACE_CHECKPOINT_TYPE
	TRI_CHKPT_OFFROAD_DEFAULT = 0,
	TRI_CHKPT_OFFROAD_FINISH,
	TRI_CHKPT_STUNT_DEFAULT,
	TRI_CHKPT_STUNT_STUNT,
	TRI_CHKPT_STUNT_FINISH,
	TRI_CHKPT_TRI_SWIM,
	TRI_CHKPT_TRI_BIKE,
	TRI_CHKPT_TRI_RUN,
	TRI_CHKPT_TRI_FINISH,
	TRI_CHKPT_TRI_FIRST_TRANS,
	TRI_CHKPT_TRI_SECOND_TRANS
ENDENUM

ENUM TRI_RACE_GATE_FLAGS
	TRI_RACE_GATE_FLAG_AP_IGNORE_GROUND 		= BIT0,
	TRI_RACE_GATE_FLAG_CHECK_VEHICLE 			= BIT1,
	TRI_RACE_GATE_FLAG_DO_NOT_SNAP_TO_GROUND 	= BIT2,
	TRI_RACE_GATE_FLAG_PLAY_SOUND				= BIT3
ENDENUM

#IF TRI_RACE_IS_TRIATHLON
// Determines how a Tri racer competes.
ENUM TRI_RACER_COMPETE_MODE
	TRI_RACER_COMPETE_MODE_DEFAULT,		
	TRI_RACER_COMPETE_MODE_TIRED,		
	TRI_RACER_COMPETE_MODE_AGGRESSIVE,	
	TRI_RACER_COMPETE_MODE_FORCED_TIRED			
ENDENUM
#ENDIF

ENUM TRI_SC_SCREEN_TEXT
	TRI_SC_MAIN_TITLE,
	TRI_SC_SCORE_TITLE,
	TRI_SC_AWARD_TXT,
	TRI_SC_SCORE_TXT,
	TRI_SC_HISCORE_TXT,
	TRI_SC_TOTAL_TXT,
	TRI_SC_WEAPON_TXT,
	TRI_SC_FIRED_TXT,
	TRI_SC_HITS_TXT,
	TRI_SC_ACC_TXT,
	TRI_SC_TIMEREM_TXT,
	TRI_SC_AWARDVAL_TXT,
	TRI_SC_SCOREVAL_TXT,
	TRI_SC_HISCOREVAL_TXT,
	TRI_SC_TOTALVAL_TXT,
	TRI_SC_WEAPONVAL_TXT,
	TRI_SC_BRONZE_GOAL_TXT,
	TRI_SC_SILVER_GOAL_TXT,
	TRI_SC_GOLD_GOAL_TXT
ENDENUM

ENUM TRI_SC_SCREEN_SPRITE
	TRI_SCREEN_MAIN_BACKGROUND,
	TRI_SC_SOCIALCLUB_IMG,
	TRI_SC_HITS_IMG,
	TRI_SC_MEDAL_AWARD_IMG,
	TRI_SC_BRONZE_IMG,
	TRI_SC_SILVER_IMG,
	TRI_SC_GOLD_IMG
ENDENUM

ENUM TRI_SC_SCREEN_RECT
	TRI_SC_SCORE_BG,
	TRI_SC_SCORE_EDGE,
	TRI_SC_HIT_BG,
	TRI_SC_INFO1_BG,
	TRI_SC_INFO2_BG,
	TRI_SC_INFO3_BG,
	TRI_SC_INFO4_BG,
	TRI_SC_INFO5_BG,
	TRI_SC_INFO6_BG,
	TRI_SC_INFO7_BG,
	TRI_SC_INFO8_BG,
	TRI_SC_BRONZE_BG,
	TRI_SC_SILVER_BG,
	TRI_SC_GOLD_BG,
	TRI_SC_BRONZE_OVERLAY,
	TRI_SC_SILVER_OVERLAY,
	TRI_SC_GOLD_OVERLAY
ENDENUM

ENUM TRI_PLAYER_RESPAWN_STATE
	TRI_RESPAWN_SET,
	TRI_RESPAWN_INIT,
	TRI_RESPAWN_CHECK,
	TRI_RESPAWN_ACTIVE,
	TRI_RESPAWN_WAIT,
	TRI_RESPAWN_INVALID
ENDENUM

ENUM TRI_RACE_NEWS_EVENT
	TRNE_INVALID = -1,
	TRNE_NPC_WON_SWIM,
	TRNE_NPC_WON_BIKE,
	TRNE_NPC_WON_RACE,
	TRNE_PLAYER_CRASH_NPC,
	TRNE_PLAYER_CRASH,
	TRNE_PLAYER_STAMINA,
	TRNE_PLAYER_FALLBACK,
	TRNE_PLAYER_RALLIED,
	TRNE_PLAYER_MISSED_CHECKPT,
	TRNE_PLAYER_IN_FIRST,
	TRNE_PLAYER_IN_FIRST_LONGTERM,
	TRNE_PLAYER_IN_LAST,
	TRNE_PLAYER_IN_LAST_LONGTERM,
	TRNE_PLAYER_IN_SECOND,
	TRNE_RACE_STARTED
ENDENUM

ENUM TRI_RACE_EVENT_FLAGS
	TREF_VALIDATE = BIT0,
	TREF_TIMESTAMP = BIT1
ENDENUM

ENUM TRI_RACE_EVENT_DATA_FLAGS
	TREDF_EMPTY = 0,
	TREDF_LINE_SPOKEN = BIT0,
	TREDF_PED_ADDED = BIT1,
	TREDF_STAMINA_REGISTERED = BIT2,
	TREDF_RALLYING = BIT3,
	TREDF_FALLINGBACK = BIT4
ENDENUM

// -----------------------------------
// STRUCTURES
// -----------------------------------

// SPR Main.
STRUCT TRI_MAIN_STRUCT
	INT iTRIMode
	TRI_MAIN_RUN_ENUM eRun
	StructTimer tSetup
	TRI_MAIN_SETUP_ENUM eSetup
	TRI_MAIN_UPDATE_ENUM eUpdate
	CAM_VIEW_MODE eStartCam
ENDSTRUCT

// SPR Display.
STRUCT TRI_DISPLAY_STRUCT	
	StructTimer tCnt
ENDSTRUCT

// SPR Input.
STRUCT TRI_INPUT_STRUCT
	INT iLS_X, iLS_Y
	INT iRS_X, iRS_Y
ENDSTRUCT

// SPR Camera.
STRUCT TRI_CAMERA_STRUCT
	CAMERA_INDEX Lst, Cur
	OBJECT_INDEX DummyCam
	FLOAT fZoom
	TRI_CAMERA_MODE_ENUM eMode
	TRI_CAMERA_FOCUS_ENUM eFocus
ENDSTRUCT

STRUCT TRI_HINT_CAM_STRUCT
	BOOL bActive
//	ENTITY_INDEX Entity
	VECTOR Coord
//	PS_HINT_CAM_ENUM HintType
	CAMERA_INDEX CustomCam
ENDSTRUCT

// SPR Gate.
STRUCT TRI_GATE_STRUCT
	VECTOR vHeading
	VECTOR vPos
	FLOAT fRadius
	FLOAT fHeightCheck
	INT iHUDAlphaAfterPass
	BLIP_INDEX Blip
	CHECKPOINT_INDEX Chkpnt
	TRI_RACE_CHECKPOINT_TYPE eChkpntType
	INT iGateFlags
	BOOL bIsFading
ENDSTRUCT

/// Everything about the player's vehicle that he drove to the race up in.
STRUCT TRI_PLAYER_VEHICLE_STRUCT
	BOOL bRecreateSavedVeh = FALSE
	BOOL bProperlyInit = FALSE
	INT iVehicleColor
	MODEL_NAMES modelName = DUMMY_MODEL_FOR_SCRIPT
	VEHICLE_SETUP_STRUCT sVehicleSetup
ENDSTRUCT

FUNC STRING GET_TRI_GATE_FLAG_NAME(TRI_RACE_GATE_FLAGS eFlag)
	SWITCH eFlag
		CASE TRI_RACE_GATE_FLAG_AP_IGNORE_GROUND		RETURN "TRI_RACE_GATE_FLAG_AP_IGNORE_GROUND"
		CASE TRI_RACE_GATE_FLAG_CHECK_VEHICLE			RETURN "TRI_RACE_GATE_FLAG_CHECK_VEHICLE"
		CASE TRI_RACE_GATE_FLAG_DO_NOT_SNAP_TO_GROUND	RETURN "TRI_RACE_GATE_FLAG_DO_NOT_SNAP_TO_GROUND"
		CASE TRI_RACE_GATE_FLAG_PLAY_SOUND				RETURN "TRI_RACE_GATE_FLAG_PLAY_SOUND"
	ENDSWITCH
	RETURN "Unknown TRI_RACE_GATE_FLAGS"
ENDFUNC

FUNC BOOL IS_TRI_GATE_FLAG_SET(TRI_GATE_STRUCT &tGateStruct, TRI_RACE_GATE_FLAGS eFlagToCheck)
	RETURN IS_BITMASK_AS_ENUM_SET(tGateStruct.iGateFlags, eFlagToCheck)
ENDFUNC
PROC SET_TRI_GATE_FLAG(TRI_GATE_STRUCT &tGateStruct, TRI_RACE_GATE_FLAGS eFlagToSet)
	IF NOT IS_TRI_GATE_FLAG_SET(tGateStruct, eFlagToSet)
		SET_BITMASK_AS_ENUM(tGateStruct.iGateFlags, eFlagToSet)
		CPRINTLN(DEBUG_TRIATHLON, "[Triathlon.sc->SET_TRI_CONTROL_FLAG] :: ", GET_TRI_GATE_FLAG_NAME(eFlagToSet))
	ENDIF
ENDPROC
PROC CLEAR_TRI_GATE_FLAG(TRI_GATE_STRUCT &tGateStruct, TRI_RACE_GATE_FLAGS eFlagToClear)
	IF IS_TRI_GATE_FLAG_SET(tGateStruct, eFlagToClear)
		CLEAR_BITMASK_AS_ENUM(tGateStruct.iGateFlags, eFlagToClear)
		CPRINTLN(DEBUG_TRIATHLON, "[Triathlon.sc->CLEAR_TRI_CONTROL_FLAG] :: ", GET_TRI_GATE_FLAG_NAME(eFlagToClear))
	ENDIF
ENDPROC

// SPR Racer.
STRUCT TRI_RACER_STRUCT
	TEXT_LABEL_31 szName
	PED_INDEX Driver
	VEHICLE_INDEX Vehicle
	BLIP_INDEX Blip
	INT iGateCur, iRank
	FLOAT fClockTime	
	FLOAT fPlsMnsLst
	FLOAT fPlsMnsTot
	VECTOR vStartPos
	FLOAT fStartHead
	FLOAT fAnimWait		// Used only during the countdown
	PED_TYPE eDriverType
	MODEL_NAMES eDriverModel
	MODEL_NAMES eVehicleModel
	TRI_RACER_RESET_ENUM eReset
	structPedsForConversation pedConvo
	
	#IF TRI_RACE_IS_TRIATHLON
		FLOAT fMinMoveBlendRatioOnFoot, 	fMaxMoveBlendRatioOnFoot		
		FLOAT fMinMoveBlendRatioInWater, 	fMaxMoveBlendRatioInWater
		FLOAT fMinBikeSpeed, 				fMaxBikeSpeed
		
		INT iSkillPlacement
		//Added this for any ai needs for use with TRI_AI_CONTROL_FLAGS primarily used for B*1516859
		INT iAIBitFlags
		
		TRI_RACER_COMPETE_MODE eCompeteMode
		
		structTimer timerInCurrentCompeteMode
		structTimer timerRacerVehicleIdle
			
		STRING szCurrentBikeRecordingName	// Necessary for Ironman, to switch a racer's waypoint vehicle recoring.
	#ENDIF
	
ENDSTRUCT
ENUM TRI_AI_CONTROL_FLAGS

	TACF_REQUEST_HELMET_ASSETS	= BIT0,
	TACF_BANIMSPED				= BIT1,
	TACF_BSETSPEED 				= BIT2,
	TACF_BHASBEENONABIKE		= BIT3

ENDENUM

FUNC STRING GET_TRI_AI_CONTROL_FLAG_NAME(TRI_AI_CONTROL_FLAGS eFlag)
	SWITCH eFlag
		CASE TACF_REQUEST_HELMET_ASSETS		RETURN "TACF_REQUEST_HELMET_ASSETS"
		CASE TACF_BANIMSPED					RETURN "TACF_BANIMSPED"
		CASE TACF_BSETSPEED					RETURN "TACF_BSETSPEED"
		CASE TACF_BHASBEENONABIKE			RETURN "TACF_BHASBEENONABIKE"
	ENDSWITCH
	RETURN "Unknown TRI_AI_CONTROL_FLAGS"
ENDFUNC

FUNC BOOL IS_TRI_AI_CONTROL_FLAG_SET(TRI_RACER_STRUCT &tRacerStruct, TRI_AI_CONTROL_FLAGS eFlagToCheck)
	RETURN IS_BITMASK_AS_ENUM_SET(tRacerStruct.iAIBitFlags, eFlagToCheck)
ENDFUNC
PROC SET_TRI_AI_CONTROL_FLAG(TRI_RACER_STRUCT &tRacerStruct, TRI_AI_CONTROL_FLAGS eFlagToSet)
	IF NOT IS_TRI_AI_CONTROL_FLAG_SET(tRacerStruct, eFlagToSet)
		SET_BITMASK_AS_ENUM(tRacerStruct.iAIBitFlags, eFlagToSet)
		CPRINTLN(DEBUG_TRIATHLON, "[Triathlon.sc->SET_TRI_AI_CONTROL_FLAG] :: ", GET_TRI_AI_CONTROL_FLAG_NAME(eFlagToSet))
	ENDIF
ENDPROC
PROC CLEAR_TRI_AI_CONTROL_FLAG(TRI_RACER_STRUCT &tRacerStruct, TRI_AI_CONTROL_FLAGS eFlagToClear)
	IF IS_TRI_AI_CONTROL_FLAG_SET(tRacerStruct,eFlagToClear)
		CLEAR_BITMASK_AS_ENUM(tRacerStruct.iAIBitFlags, eFlagToClear)
		CPRINTLN(DEBUG_TRIATHLON, "[Triathlon.sc->CLEAR_TRI_AI_CONTROL_FLAG] :: ", GET_TRI_AI_CONTROL_FLAG_NAME(eFlagToClear))
	ENDIF
ENDPROC

ENUM TRI_UI_FLAGS
	TUIF_SCLB_PROFILE_BUTTON_SHOWN		= BIT0,
	TUIF_REPLACE_ME_FLAG				= BIT1	// replace this, it's unused and just for space in the switch statement below
ENDENUM
FUNC STRING GET_STRING_FROM_TRI_UI_FLAG(TRI_UI_FLAGS eFlag)
	SWITCH eFlag
		CASE TUIF_SCLB_PROFILE_BUTTON_SHOWN		RETURN "TUIF_SCLB_PROFILE_BUTTON_SHOWN"
		CASE TUIF_REPLACE_ME_FLAG				RETURN "TUIF_REPLACE_ME_FLAG"
	ENDSWITCH
	RETURN "Unknown TRI_UI_FLAGS flag"
ENDFUNC

// SPR Race.
STRUCT TRI_RACE_STRUCT
	StructTimer tClock
	FLOAT fBestClockTime
	FLOAT fBestSplitTime
	FLOAT fCinCamTimer
	FLOAT fCheeringTimer
	INT	iBlockQuitInputTimer
	INT iNewsTickerTime, iNewsInterruptStamp
	INT iGateCheck
	INT iGateCnt, iRacerCnt	
	INT iCountdownStamp
	INT iCheerMasterSound, iWallaSound
	INT iFrameCount
	STRING FailString
	TRI_DISPLAY_STRUCT Display
	StructTimer tUpdate
	TRI_RACE_SETUP_ENUM eSetup
	TRI_RACE_UPDATE_ENUM eUpdate	
	TRI_GATE_STRUCT sGate[TRI_GATE_MAX]
	TRI_RACER_STRUCT Racer[TRI_RACER_MAX]
	MEGA_PLACEMENT_TOOLS uiScorecard
	SCALEFORM_INDEX uiLeaderboard
	SCRIPT_SCALEFORM_BIG_MESSAGE bigMessageUI
	OBJECT_INDEX objSwimLegBuoys[7]
	OBJECT_INDEX objErrantBuoy1
	OBJECT_INDEX objErrantBuoy2
	VECTOR vCheeringAreas[2]
	SCENARIO_BLOCKING_INDEX sbiStart
	TRI_PLAYER_RESPAWN_STATE ePlayerRespawnState
	BOOL bPlayerRespawning = FALSE
	BOOL bDidWeRestart, bRestarting
	TRI_UI_FLAGS eUIFlags
	
	//Latest additions are going under here since I don't see any particular order
	TRI_PLAYER_VEHICLE_STRUCT sPlayerVehicleData

ENDSTRUCT

/// PURPOSE:
///    Accessor. Sets how long, in ms, the system will block Accept input on the quit screen
PROC TRI_SET_BLOCK_QUIT_INPUT_TIMER( TRI_RACE_STRUCT & race )
	race.iBlockQuitInputTimer = GET_GAME_TIMER() + TRI_BLOCK_QUIT_INPUT_DURATION
	CDEBUG2LN( DEBUG_TRIATHLON, "TRI_SET_BLOCK_QUIT_INPUT_TIMER, TRI_BLOCK_QUIT_INPUT_DURATION = ", TRI_BLOCK_QUIT_INPUT_DURATION, ", race.iBlockQuitInputTimer = ", race.iBlockQuitInputTimer )
ENDPROC

/// PURPOSE:
///    Accessor. Denotes when we can accept accept input on the quit screen, check against game time
FUNC INT TRI_GET_BLOCK_QUIT_INPUT_TIMER( TRI_RACE_STRUCT & race )
	RETURN race.iBlockQuitInputTimer
ENDFUNC

/// PURPOSE:
///    Needs to be called during quit screen to update the timer. This is because GET_GAME_TIMER() doesn't update during the pause.
PROC TRI_UPDATE_BLOCK_QUIT_INPUT_TIMER( TRI_RACE_STRUCT & race )
	FLOAT fFrameTime = GET_FRAME_TIME()
	race.iBlockQuitInputTimer -= ROUND( fFrameTime * 1000 )
	CDEBUG2LN( DEBUG_TRIATHLON, "TRI_SET_BLOCK_QUIT_INPUT_TIMER, fFrameTime = ", fFrameTime, ", race.iBlockQuitInputTimer = ", race.iBlockQuitInputTimer )
ENDPROC

PROC SET_TRI_RACE_UI_FLAG(TRI_RACE_STRUCT &race, TRI_UI_FLAGS eFlag)
	CDEBUG2LN(DEBUG_TRIATHLON, "SET_TRI_RACE_UI_FLAG :: setting ", GET_STRING_FROM_TRI_UI_FLAG(eFlag))
	race.eUIFlags = race.eUIFlags | eFlag
ENDPROC

PROC CLEAR_TRI_RACE_UI_FLAG(TRI_RACE_STRUCT &race, TRI_UI_FLAGS eFlag)
	CDEBUG2LN(DEBUG_TRIATHLON, "CLEAR_TRI_RACE_UI_FLAG :: clearing ", GET_STRING_FROM_TRI_UI_FLAG(eFlag))
	race.eUIFlags -= race.eUIFlags & eFlag
ENDPROC

FUNC BOOL IS_TRI_RACE_UI_FLAG_SET(TRI_RACE_STRUCT &race, TRI_UI_FLAGS eFlag)
	RETURN (ENUM_TO_INT(race.eUIFlags) & ENUM_TO_INT(eFlag)) <> 0
ENDFUNC

#IF IS_DEBUG_BUILD

	// SPR Widget toggle.
	STRUCT TRI_WIDGET_TOGGLE
		BOOL bCur, bLst
	ENDSTRUCT

	// SPR Widget button.
	STRUCT TRI_WIDGET_BUTTON
		BOOL bCur
	ENDSTRUCT

	// SPR Widget int.
	STRUCT TRI_WIDGET_INT
		INT iCur, iLst
	ENDSTRUCT

	// SPR Widget float.
	STRUCT TRI_WIDGET_FLOAT
		FLOAT fCur, fLst
	ENDSTRUCT

	// SPR Widget vector.
	STRUCT TRI_WIDGET_VECTOR
		VECTOR vCur, vLst
	ENDSTRUCT

	// SPR Widget Textbox.
	STRUCT TRI_WIDGET_TEXTBOX
		TEXT_WIDGET_ID WidgetID
		TEXT_LABEL_31 szCur, szLst
	ENDSTRUCT

	// SPR Widget Listbox.
	STRUCT TRI_WIDGET_LISTBOX
		INT iCur, iLst
	ENDSTRUCT

	// SPR Widget Gates.
	STRUCT TRI_WIDGET_GATES
		WIDGET_GROUP_ID GroupID
		TRI_WIDGET_BUTTON Create
		TRI_WIDGET_BUTTON Delete
		TRI_WIDGET_TOGGLE ShowAll
		TRI_WIDGET_BUTTON SnapSelected
		TRI_WIDGET_INT Selection
		TRI_WIDGET_VECTOR Position
		TRI_WIDGET_FLOAT Radius
		TRI_WIDGET_LISTBOX ChkpntType
		TRI_WIDGET_LISTBOX StuntType
		TRI_WIDGET_LISTBOX RelMode
		TRI_WIDGET_TOGGLE RelAbs
		TRI_WIDGET_VECTOR RelMove
	ENDSTRUCT

	// SPR Widget Racers.
	STRUCT TRI_WIDGET_RACERS
		WIDGET_GROUP_ID GroupID
		TRI_WIDGET_BUTTON Create
		TRI_WIDGET_BUTTON Delete
		TRI_WIDGET_TOGGLE ShowAll
		TRI_WIDGET_INT Selection
		TRI_WIDGET_TEXTBOX Name
		TRI_WIDGET_VECTOR Position
		TRI_WIDGET_FLOAT Heading
		TRI_WIDGET_LISTBOX DriverType
		TRI_WIDGET_LISTBOX DriverModel
		TRI_WIDGET_LISTBOX VehicleModel
		TRI_WIDGET_LISTBOX RelMode
		TRI_WIDGET_TOGGLE RelAbs
		TRI_WIDGET_VECTOR RelMove
		BOOL bRacerQueue[TRI_RACER_MAX]
	ENDSTRUCT

	// SPR Widget Race.
	STRUCT TRI_WIDGET_RACE
		WIDGET_GROUP_ID GroupID
		TRI_WIDGET_TEXTBOX Error
		//TRI_WIDGET_FLOAT Time
		TRI_WIDGET_TEXTBOX Name
		TRI_WIDGET_TEXTBOX FileName
		TRI_WIDGET_BUTTON Create
		TRI_WIDGET_LISTBOX Pick
		TRI_WIDGET_BUTTON Load
		TRI_WIDGET_BUTTON Save
		TRI_WIDGET_BUTTON Export
		TRI_WIDGET_GATES Gates
		TRI_WIDGET_RACERS Racers
		TRI_RACE_STRUCT Race
	ENDSTRUCT

	// SPR Widget Race Mode.
	STRUCT TRI_WIDGET_RACEMODE
		WIDGET_GROUP_ID GroupID
	ENDSTRUCT

	// SPR Widget Edit Mode.
	STRUCT TRI_WIDGET_EDITMODE
		WIDGET_GROUP_ID GroupID
		TRI_WIDGET_LISTBOX CamMode
		TRI_WIDGET_LISTBOX CamFocus
		TRI_WIDGET_RACE Race
		TRI_INPUT_STRUCT Input
		TRI_CAMERA_STRUCT Camera
	ENDSTRUCT

	// SPR Widget Main.
	STRUCT TRI_WIDGET_MAIN
		WIDGET_GROUP_ID GroupID
		TRI_WIDGET_LISTBOX TRIMode
		TRI_WIDGET_RACEMODE RaceMode
		TRI_WIDGET_EDITMODE EditMode
	ENDSTRUCT

#ENDIF	//	IS_DEBUG_BUILD

STRUCT TRI_NEWS_EVENT_NODE
	TRI_RACE_NEWS_EVENT eNews
	TRI_RACE_EVENT_FLAGS eFlags
	INT					iTimeStamp
	INT					iPriority
ENDSTRUCT

STRUCT TRI_NEWS_EVENT_DATA
	TRI_RACE_NEWS_EVENT eLastEvent = TRNE_INVALID
	
	TRI_NEWS_EVENT_NODE sEvents[TRI_EVENT_QUEUE_COUNT]
	
	INT iCurRank
	INT iLastRank
	INT iRallyFallbackRank
	
	structTimer	sRankTimer
	structTimer	sLastCommentaryTimer
	structTimer sLongRankTimer
	structTimer	sRallyFallbackTimer
	
	
	TRI_RACE_EVENT_DATA_FLAGS	eFlags
	structPedsForConversation convoStruct
ENDSTRUCT

// SPR Master.
STRUCT TRI_MASTER_STRUCT
	TRI_RACE_TYPE_ENUM eRaceType
	VEHICLE_INDEX PlayerVeh	
	TRI_RACE_CHECKPOINT_TYPE eDefChkPntType
	VECTOR vDefRcrPos
	FLOAT fDefRcrHead
	PED_TYPE eDefDrvType
	MODEL_NAMES eDefDrvModel
	MODEL_NAMES eDefVehModel
	TEXT_LABEL_63 szXMLPath
	TEXT_LABEL_63 szSCHPath
	TEXT_LABEL_15 szTRIPath
	TEXT_LABEL_31 szMainPath
	TEXT_LABEL_15 szRacesPath
	INT iRaceCur, iRaceCnt
	FLOAT fRaceTime[TRI_RACE_MAX]
	TEXT_LABEL_31 szRaceName[TRI_RACE_MAX]
	TEXT_LABEL_31 szRaceFileName[TRI_RACE_MAX]
	FLOAT fTimeGold[TRI_RACE_MAX]
	FLOAT fTimeBronze[TRI_RACE_MAX]
	INT iBS		// Added for Conor's offline SCLB warning message
	
	OBJECT_INDEX oTable
	OBJECT_INDEX oClipboard
	OBJECT_INDEX oPencil
	PED_INDEX pedTableGuy
	PED_VARIATION_STRUCT sPlayerVariation
	BOOL 				bVariationRestored = FALSE
	
	BOOL				bOutfitChanged = FALSE
	BOOL				bQuitting = FALSE
	SIMPLE_USE_CONTEXT	uiInput
	
	// Once done with testing, convert to hardcoded
	INT iNavBlockAreas[5]
	
	TRI_NEWS_EVENT_DATA sEventData
//	FLOAT fNavBlockHeadings[5]
//	VECTOR vNavBlockScales[5]
//	VECTOR vNavBlockPositions[5]
	//SCRIPT_SCALEFORM_UI uiInput
ENDSTRUCT

PROC SETUP_NAVMESH_BLOCKING_VALUES_HARDCODED(TRIATHLON_RACE_INDEX eCurrentTriRace, INT iNavIndex, VECTOR& vPos, VECTOR& vScale, FLOAT& fHeading)
	SWITCH eCurrentTriRace
		CASE TRIATHLON_RACE_VESPUCCI
			SWITCH iNavIndex
				CASE 0
					vPos = << -1285.28811, -2039.94321, 1.60045 >>
					vScale = << 4.0, 10.2, 10.0>>
					fHeading = 2.3168
				BREAK
				CASE 1
					vPos = << -1268.66729, -2024.77961, 1.56780 >>
					vScale = << 4.0, 10.2, 10.0>>
					fHeading = 2.4
				BREAK
				CASE 2
					vPos = << -1227.08472, -2053.45801, 12.98837 >>
					vScale = << 5.0, 14.0, 10.0 >>
					fHeading = 2.6
				BREAK
				CASE 3
					vPos = << -1210.86499, -2052.28882, 13.0 >>
					vScale = << 1.96, 3.0, 10.0 >>
					fHeading = 2.865
				BREAK
				CASE 4
					vPos = << -1215.45227, -2065.8811, 13.0 >>
					vScale = << 1.63, 2.9, 10.0 >>
					fHeading = 2.685
				BREAK
			ENDSWITCH
		BREAK
		CASE TRIATHLON_RACE_ALAMO_SEA
			SWITCH iNavIndex
				CASE 0
					vPos = << 2384.31685, 4268.87617, 30.44363 >>
					vScale = << 4.0, 10.0, 10.0>>
					fHeading = 6.047
				BREAK
				CASE 1
					vPos = << 2384.97534, 4289.71924, 30.32816 >>
					vScale = << 4.0, 10.0, 10.0>>
					fHeading = 3.238
				BREAK
				CASE 2
					vPos = << 2436.90698, 4282.84961, 35.58720 >>
					vScale = << 4.5, 13.0, 10.0 >>
					fHeading = 3.019
				BREAK
				CASE 3
					vPos = << 2411.25024, 4298.00635, 34.98311 >>
					vScale = << 2.1, 3.2, 10.0 >>
					fHeading = 1.19
				BREAK
				CASE 4
					vPos = << 0.0, 0.0, 0.0 >>
					vScale = << 1.63, 2.9, 0.0 >>
					fHeading = 2.685
				BREAK
			ENDSWITCH
		BREAK
		CASE TRIATHLON_RACE_IRONMAN
			SWITCH iNavIndex
				CASE 0
					vPos = << 1568.879, 3829.782, 30.95098 >>
					vScale = << 4.0, 9.9, 10.0>>
					fHeading = 2.107
				BREAK
				CASE 1
					vPos = << 1586.192, 3842.765, 30.538 >>
					vScale = << 4.0, 9.9, 10.0>>
					fHeading = 2.295
				BREAK
				CASE 2
					vPos = << 1594.00220, 3810.71753, 33.55904 >>
					vScale = << 4.0, 12.0, 10.0 >>
					fHeading = 2.251
				BREAK
				CASE 3
					vPos = << 1604.67896, 3828.30786, 33.82485 >>
					vScale = << 1.96, 3.0, 10.0 >>
					fHeading = 0.876
				BREAK
				CASE 4
					vPos = << 1607.13513, 3824.38794, 33.06039 >>
					vScale = << 1.63, 2.9, 0.0 >>
					fHeading = 0.0
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

// Debug only. Only used for adjusting positions if needed. If adjusting, remember to update the hardcoded versions
//PROC SETUP_NAVMESH_BLOCKING_VALUES(TRI_MASTER_STRUCT& triMaster, TRIATHLON_RACE_INDEX eCurrentTriRace)
//	SWITCH eCurrentTriRace
//		CASE TRIATHLON_RACE_VESPUCCI
//			// 0 - Gate left
//			// 1 - Gate right
//			// 2 - Registration tent
//			// 3 - Food truck 1
//			// 4 - Food truck 2
//			triMaster.vNavBlockPositions[0] = << -1285.28811, -2039.94321, 1.60045 >>
//			triMaster.vNavBlockPositions[1] = << -1268.66729, -2024.77961, 1.56780 >>
//			triMaster.vNavBlockPositions[2] = << -1227.08472, -2053.45801, 12.98837 >>
//			triMaster.vNavBlockPositions[3] = << -1210.86499, -2052.28882, 13.0 >>
//			triMaster.vNavBlockPositions[4] = << -1215.45227, -2065.8811, 13.0 >>
//			
//			triMaster.fNavBlockHeadings[0] = 2.3168
//			triMaster.fNavBlockHeadings[1] = 2.4
//			triMaster.fNavBlockHeadings[2] = 2.6
//			triMaster.fNavBlockHeadings[3] = 2.865
//			triMaster.fNavBlockHeadings[4] = 2.685
//			
//			triMaster.vNavBlockScales[0] = << 4.0, 10.2, 10.0>>
//			triMaster.vNavBlockScales[1] = << 4.0, 10.2, 10.0>>
//			triMaster.vNavBlockScales[2] = << 5.0, 14.0, 10.0 >>
//			triMaster.vNavBlockScales[3] = << 1.96, 3.0, 10.0 >>
//			triMaster.vNavBlockScales[4] = << 1.63, 2.9, 10.0 >>
//			
//			CPRINTLN(DEBUG_TRIATHLON, "BLOCK_NAV_AROUND_TRIATHLON_PROPS: Blocking props at TRIATHLON_RACE_VESPUCCI location")	
//		BREAK
//		CASE TRIATHLON_RACE_ALAMO_SEA
//			// 0 - Gate left
//			// 1 - Gate right
//			// 2 - Registration tent
//			// 3 - Food truck 1
//			triMaster.vNavBlockPositions[0] = << 2384.31685, 4268.87617, 30.44363 >>
//			triMaster.vNavBlockPositions[1] = << 2384.97534, 4289.71924, 30.32816 >>
//			triMaster.vNavBlockPositions[2] = << 2436.90698, 4282.84961, 35.58720 >>
//			triMaster.vNavBlockPositions[3] = << 2411.25024, 4298.00635, 34.98311 >>
//			triMaster.vNavBlockPositions[4] = << 0,0,0 >>
//			
//			triMaster.vNavBlockScales[0] = << 4.0, 10.0, 10.0>>
//			triMaster.vNavBlockScales[1] = << 4.0, 10.0, 10.0>>
//			triMaster.vNavBlockScales[2] = << 4.5, 13.0, 10.0 >>
//			triMaster.vNavBlockScales[3] = << 2.1, 3.2, 10.0 >>
//			triMaster.vNavBlockScales[4] = << 1.63, 2.9, 0.0 >>
//			
//			triMaster.fNavBlockHeadings[0] = 6.047
//			triMaster.fNavBlockHeadings[1] = 3.238
//			triMaster.fNavBlockHeadings[2] = 3.019
//			triMaster.fNavBlockHeadings[3] = 1.19
//			triMaster.fNavBlockHeadings[4] = 0.0
//						
//			CPRINTLN(DEBUG_TRIATHLON, "BLOCK_NAV_AROUND_TRIATHLON_PROPS: Blocking props at TRIATHLON_RACE_ALAMO_SEA location")	
//		BREAK
//		CASE TRIATHLON_RACE_IRONMAN
//			// 0 - Gate left
//			// 1 - Gate right
//			// 2 - Registration tent
//			// 3 - Food truck 1
//			// 4 - Food truck 2
//			triMaster.vNavBlockPositions[0] = << 1568.879, 3829.782, 30.95098 >>
//			triMaster.vNavBlockPositions[1] = << 1586.192, 3842.765, 30.538 >>
//			triMaster.vNavBlockPositions[2] = << 1594.00220, 3810.71753, 33.55904 >>
//			triMaster.vNavBlockPositions[3] = << 1604.67896, 3828.30786, 33.82485 >>
//			triMaster.vNavBlockPositions[4] = << 1607.13513, 3824.38794, 33.06039 >>
//			
//			triMaster.fNavBlockHeadings[0] = 2.107
//			triMaster.fNavBlockHeadings[1] = 2.295
//			triMaster.fNavBlockHeadings[2] = 2.251
//			triMaster.fNavBlockHeadings[3] = 0.876
//			triMaster.fNavBlockHeadings[4] = 0.0
//			
//			triMaster.vNavBlockScales[0] = << 4.0, 9.9, 10.0>>
//			triMaster.vNavBlockScales[1] = << 4.0, 9.9, 10.0>>
//			triMaster.vNavBlockScales[2] = << 4.0, 12.0, 10.0 >>
//			triMaster.vNavBlockScales[3] = << 1.96, 3.0, 10.0 >>
//			triMaster.vNavBlockScales[4] = << 1.63, 2.9, 0.0 >>
//			
//			CPRINTLN(DEBUG_TRIATHLON, "BLOCK_NAV_AROUND_TRIATHLON_PROPS: Blocking props at TRIATHLON_RACE_IRONMAN location")	
//		BREAK
//	ENDSWITCH
//ENDPROC


// =====================================
// BEGIN Bitflags for Tri
// =====================================

// Check if the player has been warned about failing the race
/// PURPOSE: Control flags and functions (below) for controlling tri fail reasons and other stuff
ENUM TRI_CONTROL_FLAGS
	TCF_TOLD_HOW_TO_QUIT  		= BIT0,
	TCF_WARNED_AND_LEFT_VEHICLE	= BIT1,
	TCF_CAN_FORFEIT_ON_TRIBIKE	= BIT2,
	TCF_SHOW_CHECKPOINTS		= BIT3,
	TCF_NEWS_CAM_SHOWN			= BIT4,
	TCF_NEWS_CAM_ACTIVE			= BIT5,
	TCF_FINISHED_RACE			= BIT6,
	TCF_SKIP_INTRO_ON_RESTART	= BIT7,
	TCF_HAS_BEEN_ON_BIKE		= BIT8,
	TCF_RACE_RESTARTING			= BIT9,
	TCF_FAIL_CHECKING			= BIT10,
	TCF_TRI_MOUNTED				= BIT11,
	TCF_TRI_CUTSCENE_PLAYING	= BIT12,
	TCF_TRI_SKIPPED_CUTSCENE	= BIT13,
	TCF_INTRO_ANIM_SPEEDS_SET	= BIT14,
	TCF_BIG_CHEERS_PLAYED		= BIT15,
	TCF_END_MENU_CREATED		= BIT16,
	TCF_RACER_WAYPOINTED_1		= BIT17,	// These may be refactored into the racer struct if more TCFs are needed
	TCF_RACER_WAYPOINTED_2		= BIT18,	//
	TCF_RACER_WAYPOINTED_3		= BIT19,	//
	TCF_RACER_WAYPOINTED_4		= BIT20,	//
	TCF_RACER_WAYPOINTED_5		= BIT21,	//
	TCF_RACER_WAYPOINTED_6		= BIT22,	//
	TCF_RACER_WAYPOINTED_7		= BIT23,	//
	TCF_REMOVED_SWIM_GEAR		= BIT24,
	TCF_UNPATCH_CORONA			= BIT25,
	TCF_SHOW_SCLB				= BIT26,
	TCF_NEWS_CAST_INTERRUPTED	= BIT27,
	TCF_OUTRO_ASSETS_REQUESTED	= BIT28,
	TCF_QUIT_SCREEN_ON			= BIT29,
	TCF_QUITTING_RACE			= BIT30
ENDENUM
FUNC STRING GET_TRI_CONTROL_FLAG_NAME(TRI_CONTROL_FLAGS eFlag)
	SWITCH eFlag
		CASE TCF_OUTRO_ASSETS_REQUESTED		RETURN "TCF_OUTRO_ASSETS_REQUESTED"
		CASE TCF_NEWS_CAST_INTERRUPTED		RETURN "TCF_NEWS_CAST_INTERRUPTED"
		CASE TCF_SHOW_SCLB					RETURN "TCF_SHOW_SCLB"
		CASE TCF_UNPATCH_CORONA				RETURN "TCF_UNPATCH_CORONA"
		CASE TCF_END_MENU_CREATED			RETURN "TCF_END_MENU_CREATED"
		CASE TCF_BIG_CHEERS_PLAYED			RETURN "TCF_BIG_CHEERS_PLAYED"
		CASE TCF_RACER_WAYPOINTED_7			RETURN "TCF_RACER_WAYPOINTED_7"
		CASE TCF_RACER_WAYPOINTED_6			RETURN "TCF_RACER_WAYPOINTED_6"
		CASE TCF_RACER_WAYPOINTED_5			RETURN "TCF_RACER_WAYPOINTED_5"
		CASE TCF_RACER_WAYPOINTED_4			RETURN "TCF_RACER_WAYPOINTED_4"
		CASE TCF_RACER_WAYPOINTED_3			RETURN "TCF_RACER_WAYPOINTED_3"
		CASE TCF_RACER_WAYPOINTED_2			RETURN "TCF_RACER_WAYPOINTED_2"
		CASE TCF_RACER_WAYPOINTED_1			RETURN "TCF_RACER_WAYPOINTED_1"
		CASE TCF_REMOVED_SWIM_GEAR			RETURN "TCF_REMOVED_SWIM_GEAR"
		CASE TCF_INTRO_ANIM_SPEEDS_SET		RETURN "TCF_INTRO_ANIM_SPEEDS_SET"
		CASE TCF_TRI_SKIPPED_CUTSCENE		RETURN "TCF_TRI_SKIPPED_CUTSCENE"
		CASE TCF_TRI_CUTSCENE_PLAYING		RETURN "TCF_TRI_CUTSCENE_PLAYING"
		CASE TCF_TRI_MOUNTED				RETURN "TCF_TRI_MOUNTED"
		CASE TCF_FAIL_CHECKING				RETURN "TCF_FAIL_CHECKING"
		CASE TCF_RACE_RESTARTING			RETURN "TCF_RACE_RESTARTING"
		CASE TCF_HAS_BEEN_ON_BIKE			RETURN "TCF_HAS_BEEN_ON_BIKE"
		CASE TCF_SKIP_INTRO_ON_RESTART		RETURN "TCF_SKIP_INTRO_ON_RESTART"
		CASE TCF_FINISHED_RACE				RETURN "TCF_FINISHED_RACE"
		CASE TCF_NEWS_CAM_ACTIVE			RETURN "TCF_NEWS_CAM_ACTIVE"
		CASE TCF_NEWS_CAM_SHOWN				RETURN "TCF_NEWS_CAM_SHOWN"
		CASE TCF_SHOW_CHECKPOINTS			RETURN "TCF_SHOW_CHECKPOINTS"
		CASE TCF_CAN_FORFEIT_ON_TRIBIKE		RETURN "TCF_CAN_FORFEIT_ON_TRIBIKE"
		CASE TCF_WARNED_AND_LEFT_VEHICLE	RETURN "TCF_WARNED_AND_LEFT_VEHICLE"
		CASE TCF_TOLD_HOW_TO_QUIT 			RETURN "TCF_TOLD_HOW_TO_QUIT "
		CASE TCF_QUIT_SCREEN_ON				RETURN "TCF_QUIT_SCREEN_ON"
		CASE TCF_QUITTING_RACE				RETURN "TCF_QUITTING_RACE"
	ENDSWITCH
	RETURN "Unknown TRI_CONTROL_FLAG"
ENDFUNC
FUNC BOOL IS_TRI_CONTROL_FLAG_SET(TRI_CONTROL_FLAGS eFlagToCheck)
	RETURN IS_BITMASK_AS_ENUM_SET(iControlFlags, eFlagToCheck)
ENDFUNC
PROC SET_TRI_CONTROL_FLAG(TRI_CONTROL_FLAGS eFlagToSet)
	IF NOT IS_TRI_CONTROL_FLAG_SET(eFlagToSet)
		SET_BITMASK_AS_ENUM(iControlFlags, eFlagToSet)
		CPRINTLN(DEBUG_TRIATHLON, "[Triathlon.sc->SET_TRI_CONTROL_FLAG] :: ", GET_TRI_CONTROL_FLAG_NAME(eFlagToSet))
	ENDIF
ENDPROC
PROC CLEAR_TRI_CONTROL_FLAG(TRI_CONTROL_FLAGS eFlagToClear)
	IF IS_TRI_CONTROL_FLAG_SET(eFlagToClear)
		CLEAR_BITMASK_AS_ENUM(iControlFlags, eFlagToClear)
		CPRINTLN(DEBUG_TRIATHLON, "[Triathlon.sc->CLEAR_TRI_CONTROL_FLAG] :: ", GET_TRI_CONTROL_FLAG_NAME(eFlagToClear))
	ENDIF
ENDPROC

// =====================================
// END Bitflags for Tri
// =====================================


// -----------------------------------
// VARIABLES
// -----------------------------------

// SPR Master Data (DO NOT MOVE).
TRI_MASTER_STRUCT TRI_Master


// -----------------------------------
// PROCS/FUNCTIONS
// -----------------------------------
PROC RESET_TRI_CIN_CAM_TIMER(TRI_RACE_STRUCT &thisRace)
	thisRace.fCinCamTimer = 0
ENDPROC

PROC INCREASE_TRI_CIN_CAM_TIMER(TRI_RACE_STRUCT &thisRace)
	thisRace.fCinCamTimer += GET_FRAME_TIME()
ENDPROC

FUNC FLOAT GET_TRI_CIN_CAM_TIMER(TRI_RACE_STRUCT &thisRace)
	RETURN thisRace.fCinCamTimer
ENDFUNC

PROC RESET_TRI_CHEERING_TIMER(TRI_RACE_STRUCT &thisRace)
	thisRace.fCheeringTimer = 0
ENDPROC

PROC INCREASE_TRI_CHEERING_TIMER(TRI_RACE_STRUCT &thisRace)
	thisRace.fCheeringTimer += GET_FRAME_TIME()
ENDPROC

FUNC FLOAT GET_TRI_CHEERING_TIMER(TRI_RACE_STRUCT &thisRace)
	RETURN thisRace.fCheeringTimer
ENDFUNC

/// PURPOSE:
///    co-opts the cheering timer, they aren't used at the same times.
/// PARAMS:
///    thisRace - 
PROC RESET_TRI_INTRO_CHATTER_TIMER(TRI_RACE_STRUCT &thisRace)
	thisRace.fCheeringTimer = 0
ENDPROC

/// PURPOSE:
///    co-opts the cheering timer, they aren't used at the same times.
/// PARAMS:
///    thisRace - 
PROC INCREASE_TRI_INTRO_CHATTER_TIMER(TRI_RACE_STRUCT &thisRace)
	thisRace.fCheeringTimer += GET_FRAME_TIME()
ENDPROC

/// PURPOSE:
///    co-opts the cheering timer, they aren't used at the same times.
/// PARAMS:
///    thisRace - 
/// RETURNS:
///    
FUNC FLOAT GET_TRI_INTRO_CHATTER_TIMER(TRI_RACE_STRUCT &thisRace)
	RETURN thisRace.fCheeringTimer
ENDFUNC

FUNC STRING GET_STRING_FROM_PCF_FLAG( PED_CONFIG_FLAGS eFlag )
	SWITCH eFlag
		CASE PCF_DisableDeepSurfaceAnims		RETURN "PCF_DisableDeepSurfaceAnims"
		CASE PCF_ForceDeepSurfaceCheck			RETURN "PCF_ForceDeepSurfaceCheck"
	ENDSWITCH
	RETURN "Unknown PED_CONFIG_FLAGS enum"
ENDFUNC

/// PURPOSE:
///    Turns use of PCF_DisableDeepSurfaceAnims on and off.
PROC TRI_SET_PED_CONFIG_FLAG(PED_INDEX ped, PED_CONFIG_FLAGS pcf_flag, BOOL bUse)
	CPRINTLN( DEBUG_TRIATHLON, "TRI_SET_PED_CONFIG_FLAG flag_", GET_STRING_FROM_PCF_FLAG( pcf_flag ), " set on ped=", NATIVE_TO_INT( ped ), ", bUse=", PICK_STRING( bUse, "TRUE", "FALSE" ) )
	SET_PED_CONFIG_FLAG( ped, pcf_flag, bUse )
ENDPROC
