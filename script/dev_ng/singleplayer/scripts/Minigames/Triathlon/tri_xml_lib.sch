
#IF IS_DEBUG_BUILD	
//	USING "tri_data.sch"

	#IF iDEBUG_USE_XML

	// HASH KEYS

	CONST_INT XML_HASH_ALL 				1166234150	// "all" - passed into commands

	// "SplineNode"
	CONST_INT XML_HASH_RECORDNODE		686443152	// "RecordNode"

	CONST_INT XML_HASH_NAME				-421429484	// "name"
	CONST_INT XML_HASH_STATUS			1711913059	// "status"
	CONST_INT XML_HASH_GOALS			-763179266	// "goals"
	CONST_INT XML_HASH_PRE_REQ			-2069083032	// "preReq"

	//CONST_INT XML_HASH_GOAL_TIME		398356723	// "goalTime"
	CONST_INT XML_HASH_GOAL_timeGold	-724149506	// "timeGold"
	CONST_INT XML_HASH_GOAL_timeBronze	68426182	// "timeBronze"

	//CONST_INT XML_HASH_GOAL_DIST		-1817767987	// "goalDist"
	CONST_INT XML_HASH_GOAL_distGold	2042985856	// "distGold"
	CONST_INT XML_HASH_GOAL_distBronze 	-1007341200	// "distBronze"
	//CONST_INT XML_HASH_GOAL_fake_distBronze 	1688763194	// "fake_distBronze"

	CONST_INT XML_HASH_GOAL_DAMAGE		-290043632	// "goalDamage"

	//CONST_INT XML_HASH_GOAL_CHECK		-134112714	// "goalCheck"
	CONST_INT XML_HASH_GOAL_checkTotal	2118892389	// "checkTotal"

	CONST_INT XML_HASH_GOAL_PLACE		-854551281	// "goalPlace"
	
	#ENDIF
#ENDIF
	
	
/*
	STRUCT XML_TRI_DATA
		TEXT_LABEL					name
		INT							status
		INT							goals
		INT							preReq
				
		BOOL	active		// bool storing if struct in the right section of the xml
		INT		totalNodes	// int counter for total nodes
		INT		currentNode	// int counter for current node
		BOOL	loaded		// bool saying if xml was loaded ok
	ENDSTRUCT



	FUNC BOOL getValuesFromSPRDataXml(XML_TRI_DATA &xmlSPRData, STRING node)
	    
	    INT eachNode
	    INT eachAttribute
	    
	    // make active incase all is set
	    IF GET_HASH_KEY(node) = XML_HASH_ALL
	        xmlSPRData.active = TRUE
	    ENDIF
		
		xmlSPRData.name			= -1
		xmlSPRData.status		= -1
		xmlSPRData.goals			= -1
		xmlSPRData.preReq		= -1

		// carry on through the nodes where it left off
	    FOR eachNode = xmlSPRData.currentNode TO (xmlSPRData.totalNodes-1)
	    
	        SWITCH GET_HASH_KEY(GET_XML_NODE_NAME())

	            // hit a SPR Race 
	            CASE XML_HASH_RECORDNODE

	                IF xmlSPRData.active

	                    IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0

	                        FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)

	                            SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))

									CASE XML_HASH_NAME
										xmlSPRData.name = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									BREAK
									CASE XML_HASH_STATUS
	                                    xmlSPRData.status = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
	                                BREAK
									CASE XML_HASH_GOALS
	                                    xmlSPRData.goals = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
	                                BREAK
									CASE XML_HASH_PRE_REQ
	                                    xmlSPRData.preReq = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
	                                BREAK
								
									DEFAULT
										SAVE_STRING_TO_DEBUG_FILE("unknown attribute hash: ")
										SAVE_INT_TO_DEBUG_FILE(GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)))
										SAVE_STRING_TO_DEBUG_FILE(": ")
										SAVE_FLOAT_TO_DEBUG_FILE(GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
										SAVE_NEWLINE_TO_DEBUG_FILE()
										
									BREAK
									
	                            ENDSWITCH

	                        ENDFOR
	                        
	                        xmlSPRData.currentNode++
	                        GET_NEXT_XML_NODE()

	                        RETURN TRUE

	                    ENDIF

	                ENDIF

	            BREAK

	            // check to see if this is the node the user wants to load
	            DEFAULT
	                xmlSPRData.active = (GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY(node)) OR (GET_HASH_KEY(node) = XML_HASH_ALL)
	            BREAK

	        ENDSWITCH

	        xmlSPRData.currentNode++
	        GET_NEXT_XML_NODE()

	    ENDFOR

	    RETURN FALSE

	ENDFUNC
	//#END_NO_WIKI

	// resets the structure and gets ready for the xml grab loop
	FUNC BOOL LOAD_TRI_XML(XML_TRI_DATA &xmlSPRData, STRING xml)

	    // see if the xml file can be loaded, return false if not   
	    xmlSPRData.loaded = LOAD_XML_FILE(xml)
	    IF NOT xmlSPRData.loaded
//	            PRINTSTRING("GET_PILOT_SCHOOLS_FROM_XML failed cannot load ")
//	            PRINTSTRING(xml)
//	            PRINTNL()
	        RETURN FALSE
	    ENDIF 
	    
	    // see if the xml file contains any nodes, return false if not  
	    xmlSPRData.totalNodes = GET_NUMBER_OF_XML_NODES()
	    IF xmlSPRData.totalNodes = 0
//	            PRINTSTRING("GET_PILOT_SCHOOLS_FROM_XML failed file contains no nodes ")
//	            PRINTSTRING(xml)
//	            PRINTNL()
	        RETURN FALSE
	    ENDIF 
	    
	    // reset the current node and active flag
	    xmlSPRData.currentNode = 0
	    xmlSPRData.active = FALSE

	    RETURN TRUE

	ENDFUNC

	PROC UNLOAD_TRI_XML(XML_TRI_DATA &xmlSPRData)
	    xmlSPRData.active = FALSE
	    xmlSPRData.loaded = FALSE
	    DELETE_XML_FILE()
	ENDPROC

	//
	FUNC BOOL GET_NEXT_TRI_FROM_XML(XML_TRI_DATA &xmlSPRData, STRING node)
	    RETURN getValuesFromSPRDataXml(xmlSPRData, node)
	ENDFUNC

	#ENDIF
	#IF NOT iDEBUG_USE_XML
	#IF IS_DEBUG_BUILD
	PROC Save_TRI_STRUCT_asScriptInit(INT iNode_count)
		INT iSkipping_PSC_6_destroyTargets = 0
		IF iNode_count >= 6
			iSkipping_PSC_6_destroyTargets = 1
		ENDIF
		
		SAVE_STRING_TO_DEBUG_FILE("  //TRI_STRUCT.")
		SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].name))
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
					//name stuff
		SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
		SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].name = \"")
		SAVE_STRING_TO_DEBUG_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].name)SAVE_STRING_TO_DEBUG_FILE("\"")
		SAVE_NEWLINE_TO_DEBUG_FILE()

		SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
		SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].status = INT_TO_ENUM(TRI_STATUS_ENUM, ")
		SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(g_savedGlobals.sSPRData.structSPR[iNode_count].status))SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		//flight school GOALS
		
		
		INT iGoal
		REPEAT NUMBER_OF_TRI_GOALS iGoal
			IF Is_FSG_Bit_Set(g_savedGlobals.sSPRData.structSPR[iNode_count], INT_TO_ENUM(TRI_GOAL_BITS, iGoal))
				SAVE_STRING_TO_DEBUG_FILE("TRI_Data_Set_Goal_Bit(g_savedGlobals.sSPRData.structSPR[TRI_")
				SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("],	(")
				
				SAVE_STRING_TO_DEBUG_FILE("SPRG_")
				SAVE_INT_TO_DEBUG_FILE(iGoal)
				SAVE_STRING_TO_DEBUG_FILE("))")SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDREPEAT
		
		//flight school GOALS
		
		//flight school PRE-REQUISITS
		IF (g_savedGlobals.sSPRData.structSPR[iNode_count].preReq = 0)
			//clear prereqs??
		ELSE
			INT iPreReq
			REPEAT NUMBER_OF_TRI_CLASSES iPreReq
				IF Is_FTRI_Bit_Set(g_savedGlobals.sSPRData.structSPR[iNode_count], INT_TO_ENUM(TRI_CLASSES_ENUM, iPreReq))
					SAVE_STRING_TO_DEBUG_FILE("TRI_Data_Set_PreReq_Bit(g_savedGlobals.sSPRData.structSPR[TRI_")
					SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("],	(")
					
					INT iSkipping_prereq_PSC_6_destroyTargets = 0
					IF iPreReq >= 6
						iSkipping_prereq_PSC_6_destroyTargets = 1
					ENDIF
					
					SAVE_STRING_TO_DEBUG_FILE("PSC_")
					SAVE_INT_TO_DEBUG_FILE(iPreReq+iSkipping_prereq_PSC_6_destroyTargets)
					
					SAVE_STRING_TO_DEBUG_FILE("))")
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
			ENDREPEAT
			
			
		ENDIF
		SAVE_NEWLINE_TO_DEBUG_FILE()
		//flight school PRE-REQUISITS
		
					//time stuff
		IF Is_FSG_Bit_Set(g_savedGlobals.sSPRData.structSPR[iNode_count], SPRG_0_timeTaken)
			SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
			SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].goalTime = ")
			SAVE_FLOAT_TO_DEBUG_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].goalTime)
			SAVE_NEWLINE_TO_DEBUG_FILE()

			SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
			SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].timeGold = ")
			SAVE_FLOAT_TO_DEBUG_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].timeGold)
			SAVE_NEWLINE_TO_DEBUG_FILE()

			SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
			SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].timeBronze = ")
			SAVE_FLOAT_TO_DEBUG_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].timeBronze)
			SAVE_NEWLINE_TO_DEBUG_FILE()SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF

					//distance stuff
		IF Is_FSG_Bit_Set(g_savedGlobals.sSPRData.structSPR[iNode_count], SPRG_2_distanceFromTarget)
			SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
			SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].goalDist = ")
			SAVE_FLOAT_TO_DEBUG_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].goalDist)
			SAVE_NEWLINE_TO_DEBUG_FILE()

			SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
			SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].distGold = ")
			SAVE_FLOAT_TO_DEBUG_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].distGold)
			SAVE_NEWLINE_TO_DEBUG_FILE()

			SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
			SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].distBronze = ")
			SAVE_FLOAT_TO_DEBUG_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].distBronze)
			SAVE_NEWLINE_TO_DEBUG_FILE()SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF

					//damage stuff
		IF Is_FSG_Bit_Set(g_savedGlobals.sSPRData.structSPR[iNode_count], SPRG_1_damageTaken)
			SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
			SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].goalDamage = ")
			SAVE_INT_TO_DEBUG_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].goalDamage)
			SAVE_NEWLINE_TO_DEBUG_FILE()SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF

					//checkpoint stuff
		IF Is_FSG_Bit_Set(g_savedGlobals.sSPRData.structSPR[iNode_count], SPRG_3_checkpointsPassed)
			SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
			SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].goalCheck = ")
			SAVE_INT_TO_DEBUG_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].goalCheck)
			SAVE_NEWLINE_TO_DEBUG_FILE()

			SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
			SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].checkTotal = ")
			SAVE_INT_TO_DEBUG_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].checkTotal)
			SAVE_NEWLINE_TO_DEBUG_FILE()SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF

					//place stuff
		IF Is_FSG_Bit_Set(g_savedGlobals.sSPRData.structSPR[iNode_count], SPRG_4_racePosition)
			SAVE_STRING_TO_DEBUG_FILE("g_savedGlobals.sSPRData.structSPR[TRI_")
			SAVE_INT_TO_DEBUG_FILE(iNode_count+iSkipping_TRI_6_destroyTargets)SAVE_STRING_TO_DEBUG_FILE("].goalPlace = ")
			SAVE_INT_TO_DEBUG_FILE(g_savedGlobals.sSPRData.structSPR[iNode_count].goalPlace)
			SAVE_NEWLINE_TO_DEBUG_FILE()SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		
	ENDPROC
	#ENDIF	
	#ENDIF
#ENDIF
*/
