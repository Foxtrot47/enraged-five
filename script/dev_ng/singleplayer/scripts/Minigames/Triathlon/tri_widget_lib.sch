// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	TRI_Widget_lib.sch
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Single Player Races - Widget library procs/functions file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

#IF IS_DEBUG_BUILD

USING "TRI_Head.sch"


// -----------------------------------
// COMMON HELPER PROCS/FUNCTIONS
// -----------------------------------

PROC TRI_Widget_Delete_Widget_Group(WIDGET_GROUP_ID GroupID)
	//DEBUG_MESSAGE("TRI_Widget_Delete_Widget_Group")
	IF DOES_WIDGET_GROUP_EXIST(GroupID)
		DELETE_WIDGET_GROUP(GroupID)
	ENDIF
ENDPROC




// -----------------------------------
// TOGGLE PROCS/FUNCTIONS
// -----------------------------------

PROC TRI_Widget_Toggle_Set(TRI_WIDGET_TOGGLE& Toggle, BOOL bValue, BOOL bReset = TRUE)
	//DEBUG_MESSAGE("TRI_Widget_Toggle_Set")
	Toggle.bCur = bValue
	IF bReset
		Toggle.bLst = bValue
	ENDIF
ENDPROC

FUNC BOOL TRI_Widget_Toggle_Get(TRI_WIDGET_TOGGLE& Toggle)
	//DEBUG_MESSAGE("TRI_Widget_Toggle_Get")
	RETURN Toggle.bCur
ENDFUNC

FUNC BOOL TRI_Widget_Toggle_Check(TRI_WIDGET_TOGGLE& Toggle)
	//DEBUG_MESSAGE("TRI_Widget_Toggle_Check")
	RETURN (Toggle.bCur <> Toggle.bLst)
ENDFUNC

PROC TRI_Widget_Toggle_Reset(TRI_WIDGET_TOGGLE& Toggle)
	//DEBUG_MESSAGE("TRI_Widget_Toggle_Reset")
	Toggle.bLst = Toggle.bCur
ENDPROC

PROC TRI_Widget_Toggle_Setup(TRI_WIDGET_TOGGLE& Toggle, STRING sName, BOOL bValue = FALSE)
	//DEBUG_MESSAGE("TRI_Widget_Toggle_Setup")
	TRI_Widget_Toggle_Set(Toggle, bValue)
	ADD_WIDGET_BOOL(sName, Toggle.bCur)
ENDPROC




// -----------------------------------
// BUTTON PROCS/FUNCTIONS
// -----------------------------------

PROC TRI_Widget_Button_Set(TRI_WIDGET_BUTTON& Button, BOOL bValue)
	//DEBUG_MESSAGE("TRI_Widget_Button_Set")
	Button.bCur = bValue
ENDPROC

FUNC BOOL TRI_Widget_Button_Check(TRI_WIDGET_BUTTON& Button)
	//DEBUG_MESSAGE("TRI_Widget_Button_Check")
	RETURN Button.bCur
ENDFUNC

PROC TRI_Widget_Button_Reset(TRI_WIDGET_BUTTON& Button)
	//DEBUG_MESSAGE("TRI_Widget_Button_Reset")
	Button.bCur = FALSE
ENDPROC

PROC TRI_Widget_Button_Setup(TRI_WIDGET_BUTTON& Button, STRING sName, BOOL bValue = FALSE)
	//DEBUG_MESSAGE("TRI_Widget_Button_Setup")
	TRI_Widget_Button_Set(Button, bValue)
	ADD_WIDGET_BOOL(sName, Button.bCur)
ENDPROC




// -----------------------------------
// INT PROCS/FUNCTIONS
// -----------------------------------

PROC TRI_Widget_Int_Set(TRI_WIDGET_INT& IntSlider, INT iValue, BOOL bReset = TRUE)
	//DEBUG_MESSAGE("TRI_Widget_Int_Set")
	IntSlider.iCur = iValue
	IF bReset
		IntSlider.iLst = iValue
	ENDIF
ENDPROC

FUNC INT TRI_Widget_Int_Get(TRI_WIDGET_INT& IntSlider)
	//DEBUG_MESSAGE("TRI_Widget_Int_Get")
	RETURN IntSlider.iCur
ENDFUNC

FUNC BOOL TRI_Widget_Int_Check(TRI_WIDGET_INT& IntSlider)
	//DEBUG_MESSAGE("TRI_Widget_Int_Check")
	RETURN (IntSlider.iCur <> IntSlider.iLst)
ENDFUNC

PROC TRI_Widget_Int_Reset(TRI_WIDGET_INT& IntSlider)
	//DEBUG_MESSAGE("TRI_Widget_Int_Reset")
	IntSlider.iLst = IntSlider.iCur
ENDPROC

PROC TRI_Widget_Int_Setup(TRI_WIDGET_INT& IntSlider, STRING sName, INT Min, INT Max, INT iStep)
	//DEBUG_MESSAGE("TRI_Widget_Int_Setup")
	TRI_Widget_Int_Set(IntSlider, 0)
	ADD_WIDGET_INT_SLIDER(sName, IntSlider.iCur, Min, Max, iStep)
ENDPROC




// -----------------------------------
// FLOAT PROCS/FUNCTIONS
// -----------------------------------

PROC TRI_Widget_Float_Set(TRI_WIDGET_FLOAT& FloatSlider, FLOAT fValue, BOOL bReset = TRUE)
	//DEBUG_MESSAGE("TRI_Widget_Float_Set")
	FloatSlider.fCur = fValue
	IF bReset
		FloatSlider.fLst = fValue
	ENDIF
ENDPROC

FUNC FLOAT TRI_Widget_Float_Get(TRI_WIDGET_FLOAT& FloatSlider)
	//DEBUG_MESSAGE("TRI_Widget_Float_Get")
	RETURN FloatSlider.fCur
ENDFUNC

FUNC BOOL TRI_Widget_Float_Check(TRI_WIDGET_FLOAT& FloatSlider)
	//DEBUG_MESSAGE("TRI_Widget_Float_Check")
	RETURN (FloatSlider.fCur <> FloatSlider.fLst)
ENDFUNC

PROC TRI_Widget_Float_Reset(TRI_WIDGET_FLOAT& FloatSlider)
	//DEBUG_MESSAGE("TRI_Widget_Float_Reset")
	FloatSlider.fLst = FloatSlider.fCur
ENDPROC

PROC TRI_Widget_Float_Setup(TRI_WIDGET_FLOAT& FloatSlider, STRING sName, FLOAT Min, FLOAT Max, FLOAT fStep)
	//DEBUG_MESSAGE("TRI_Widget_Float_Setup")
	TRI_Widget_Float_Set(FloatSlider, 0.0)
	ADD_WIDGET_FLOAT_SLIDER(sName, FloatSlider.fCur, Min, Max, fStep)
ENDPROC




// -----------------------------------
// VECTOR PROCS/FUNCTIONS
// -----------------------------------

PROC TRI_Widget_Vector_Set(TRI_WIDGET_VECTOR& VectorSlider, VECTOR vValue, BOOL bReset = TRUE)
	//DEBUG_MESSAGE("TRI_Widget_Vector_Set")
	VectorSlider.vCur = vValue
	IF bReset
		VectorSlider.vLst = vValue
	ENDIF
ENDPROC

FUNC VECTOR TRI_Widget_Vector_Get(TRI_WIDGET_VECTOR& VectorSlider)
	//DEBUG_MESSAGE("TRI_Widget_Vector_Get")
	RETURN VectorSlider.vCur
ENDFUNC

FUNC BOOL TRI_Widget_Vector_Check(TRI_WIDGET_VECTOR& VectorSlider)
	//DEBUG_MESSAGE("TRI_Widget_Vector_Check")
	RETURN NOT ARE_VECTORS_EQUAL(VectorSlider.vCur, VectorSlider.vLst)
ENDFUNC

PROC TRI_Widget_Vector_Reset(TRI_WIDGET_VECTOR& VectorSlider)
	//DEBUG_MESSAGE("TRI_Widget_Vector_Reset")
	VectorSlider.vLst = VectorSlider.vCur
ENDPROC

PROC TRI_Widget_Vector_Setup(TRI_WIDGET_VECTOR& VectorSlider, STRING sName, FLOAT Min, FLOAT Max, FLOAT fStep)
	//DEBUG_MESSAGE("TRI_Widget_Vector_Setup")
	TRI_Widget_Vector_Set(VectorSlider, <<0.0,0.0,0.0>>)
	TEXT_LABEL_63 szName = sName
	szName += " X"
	ADD_WIDGET_FLOAT_SLIDER(szName, VectorSlider.vCur.x, Min, Max, fStep)
	szName = sName
	szName += " Y"
	ADD_WIDGET_FLOAT_SLIDER(szName, VectorSlider.vCur.y, Min, Max, fStep)
	szName = sName
	szName += " Z"
	ADD_WIDGET_FLOAT_SLIDER(szName, VectorSlider.vCur.z, Min, Max, fStep)
ENDPROC




// -----------------------------------
// TEXTBOX PROCS/FUNCTIONS
// -----------------------------------

PROC TRI_Widget_Textbox_Set(TRI_WIDGET_TEXTBOX& Textbox, STRING sEntry, BOOL bReset = TRUE)
	//DEBUG_MESSAGE("TRI_Widget_Textbox_Set")
	Textbox.szCur = sEntry
	IF bReset
		Textbox.szLst = sEntry
	ENDIF
	SET_CONTENTS_OF_TEXT_WIDGET(Textbox.WidgetID, Textbox.szCur)
ENDPROC

FUNC TEXT_LABEL_31 TRI_Widget_Textbox_Get(TRI_WIDGET_TEXTBOX& Textbox)
	//DEBUG_MESSAGE("TRI_Widget_Textbox_Get")
	Textbox.szCur = GET_CONTENTS_OF_TEXT_WIDGET(Textbox.WidgetID)
	RETURN Textbox.szCur
ENDFUNC

// TODO: Something strange is going on with checks returning true over and over...
FUNC BOOL TRI_Widget_Textbox_Check(TRI_WIDGET_TEXTBOX& Textbox)
	//DEBUG_MESSAGE("TRI_Widget_Textbox_Check")
	Textbox.szCur = GET_CONTENTS_OF_TEXT_WIDGET(Textbox.WidgetID)
	RETURN NOT ARE_STRINGS_EQUAL(Textbox.szCur, Textbox.szLst)
ENDFUNC

PROC TRI_Widget_Textbox_Reset(TRI_WIDGET_TEXTBOX& Textbox)
	//DEBUG_MESSAGE("TRI_Widget_Textbox_Reset")
	Textbox.szLst = Textbox.szCur
ENDPROC

PROC TRI_Widget_Textbox_Setup(TRI_WIDGET_TEXTBOX& Textbox, STRING sName, STRING sEntry)
	//DEBUG_MESSAGE("TRI_Widget_Textbox_Setup")
	Textbox.WidgetID = ADD_TEXT_WIDGET(sName)
	TRI_Widget_Textbox_Set(Textbox, sEntry)
ENDPROC




// -----------------------------------
// LISTBOX PROCS/FUNCTIONS
// -----------------------------------

PROC TRI_Widget_Listbox_Set(TRI_WIDGET_LISTBOX& Listbox, INT iIndex, BOOL bReset = TRUE)
	//DEBUG_MESSAGE("TRI_Widget_Listbox_Set")
	Listbox.iCur = iIndex
	IF bReset
		Listbox.iLst = iIndex
	ENDIF
ENDPROC

FUNC INT TRI_Widget_Listbox_Get(TRI_WIDGET_LISTBOX& Listbox)
	//DEBUG_MESSAGE("TRI_Widget_Listbox_Get")
	RETURN Listbox.iCur
ENDFUNC

FUNC BOOL TRI_Widget_Listbox_Check(TRI_WIDGET_LISTBOX& Listbox)
	//DEBUG_MESSAGE("TRI_Widget_Listbox_Check")
	RETURN (Listbox.iCur <> Listbox.iLst)
ENDFUNC

PROC TRI_Widget_Listbox_Reset(TRI_WIDGET_LISTBOX& Listbox)
	//DEBUG_MESSAGE("TRI_Widget_Listbox_Reset")
	Listbox.iLst = Listbox.iCur
ENDPROC

PROC TRI_Widget_Listbox_Setup(TRI_WIDGET_LISTBOX& Listbox, STRING sName, TEXT_LABEL_31& szEntry[], INT iIndex = 0)
	//DEBUG_MESSAGE("TRI_Widget_Listbox_Setup")
	TRI_Widget_Listbox_Set(Listbox, iIndex)
	INT i = 0
	START_NEW_WIDGET_COMBO()
		REPEAT COUNT_OF(szEntry) i
			IF NOT IS_STRING_EMPTY(szEntry[i])
				ADD_TO_WIDGET_COMBO(szEntry[i])
			ENDIF
		ENDREPEAT
	STOP_WIDGET_COMBO(sName, Listbox.iCur)
ENDPROC

#ENDIF	//	IS_DEBUG_BUILD


// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!
