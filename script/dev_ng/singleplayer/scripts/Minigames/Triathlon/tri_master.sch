// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	TRI_Master.sch
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Single Player Races - Master procs/functions file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "TRI_Head.sch"
USING "TRI_Helpers.sch"
USING "TRI_Gate.sch"
USING "TRI_Racer.sch"
USING "TRI_Race.sch"

// -----------------------------------
// RACE PROCS/FUNCTIONS
// -----------------------------------

FUNC INT TRI_Master_Add_Race(STRING sRaceFileName, STRING sRaceName)
	//DEBUG_MESSAGE("TRI_Master_Add_Race")
	INT i
	REPEAT COUNT_OF(TRI_Master.szRaceFileName) i
		IF IS_STRING_EMPTY(TRI_Master.szRaceFileName[i])
			TRI_Master.szRaceFileName[i] = sRaceFileName
			TRI_Master.szRaceName[i] = sRaceName
			++TRI_Master.iRaceCnt
			RETURN i
		ELIF ARE_STRINGS_EQUAL(TRI_Master.szRaceFileName[i], sRaceFileName)
			RETURN -2
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

// -----------------------------------
// GENERAL PROCS/FUNCTIONS
// -----------------------------------




// -----------------------------------
// FILE I/O PROCS/FUNCTIONS
// -----------------------------------
#IF IS_DEBUG_BUILD
FUNC BOOL TRI_Master_Load()
	DEBUG_MESSAGE("TRI_Master_Load")

	// Assemble filename for Master XML file.
	TEXT_LABEL_63 szFileName
	szFileName = TRI_Master.szTRIPath
	szFileName += TRI_Master.szMainPath
	szFileName += "SPR_Master.xml"
	
	// Load Master XML file into memory.
	DEBUG_MESSAGE("TRI_Master_Load: Opening filepath: ", TRI_Master.szXMLPath)
	DEBUG_MESSAGE("TRI_Master_Load: Opening filename: ", szFileName)
	IF NOT TRI_XML_Load(szFileName)
		SCRIPT_ASSERT("TRI_Master_Load: Failed to load Master XML file!")
		DELETE_XML_FILE()
		RETURN FALSE
	ENDIF
	DEBUG_MESSAGE("TRI_Master_Load: Started reading Master XML file: TRI_Master")
	
	// Local variables.
	FLOAT fAttr
	STRING sAttr
	INT iNodeCnt, iNodeName, iAttrCnt
	
	// Reset race counting variable.
	TRI_Master.iRaceCnt = 0
	
	// Start at root node of Master XML file.
	TRI_XML_GetRootNode(iNodeCnt, iNodeName)
	
	// Check for "Master" root node before continuing.
	IF (iNodeName <> TRI_XML_HASH_MASTER)
		SCRIPT_ASSERT("TRI_Master_Load: Failed to find 'Master' root node")
		DELETE_XML_FILE()
		RETURN FALSE
	ENDIF
	DEBUG_MESSAGE("TRI_Master_Load: Inside 'Master' root node")
	TRI_XML_GetNextNode(iNodeCnt, iNodeName)
	
	// Check for "Races" parent node before continuing.
	IF (iNodeName <> TRI_XML_HASH_RACES)
		SCRIPT_ASSERT("TRI_Master_Load: Failed to find 'Races' parent node")
		DELETE_XML_FILE()
		RETURN FALSE
	ENDIF
	DEBUG_MESSAGE("TRI_Master_Load: Inside 'Races' parent node")
	
	// Loop through each node of "Races" branch.
	WHILE (iNodeCnt < GET_NUMBER_OF_XML_NODES())
	
		// Check for nodes in "Races" branch.
		SWITCH (iNodeName)
		
			// "Race" node.
			CASE TRI_XML_HASH_RACE
			
				// Check if "Race" data limit has been reached.
				IF (TRI_Master.iRaceCnt >= TRI_RACE_MAX)
					DEBUG_MESSAGE("TRI_Master_Load: Reached 'Race' data limit")
					BREAK
				ENDIF
				
				// Debug info.
				PRINTSTRING("TRI_Master_Load: Inside 'Race' node: ")
				PRINTINT(TRI_Master.iRaceCnt)
				PRINTNL()
				
				// Loop through each attribute of "Race" node and save data.
				REPEAT GET_NUMBER_OF_XML_NODE_ATTRIBUTES() iAttrCnt
					SWITCH (GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttrCnt)))
						CASE TRI_XML_HASH_FILENAME
							sAttr = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							TRI_Master.szRaceFileName[TRI_Master.iRaceCnt] = sAttr
							DEBUG_MESSAGE("TRI_Master_Load: Saved 'FileName' data")
						BREAK
						CASE TRI_XML_HASH_NAME
							sAttr = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
//							TRI_Master.szRaceName[TRI_Master.iRaceCnt] = sAttr
							DEBUG_MESSAGE("TRI_Master_Load: Saved 'Name' data")
						BREAK
						CASE TRI_XML_HASH_TIME
							fAttr = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							TRI_Master.fRaceTime[TRI_Master.iRaceCnt] = fAttr
							DEBUG_MESSAGE("TRI_Master_Load: Saved 'Time' data")
						BREAK
					ENDSWITCH
				ENDREPEAT
				
				// Increment master Race count.
				++TRI_Master.iRaceCnt
				
			BREAK
			
		ENDSWITCH
		
		// Get next node of "Races" branch.
		TRI_XML_GetNextNode(iNodeCnt, iNodeName)
		
	ENDWHILE
	
	// Unload Master XML file from memory.
	DELETE_XML_FILE()
	DEBUG_MESSAGE("TRI_Master_Load: Finished reading Master XML file: TRI_Master")
	
	// Master successfully loaded.
	RETURN TRUE
	
ENDFUNC

FUNC BOOL TRI_Master_Save()
	DEBUG_MESSAGE("TRI_Master_Save")

	// Local variables.
	INT iCnt
	TEXT_LABEL_63 szFileName
	
	// Assemble filename for Master XML file.
	szFileName = TRI_Master.szTRIPath
	szFileName += TRI_Master.szMainPath
	szFileName += "TRI_Master.xml"
	
	// Clear Master XML file and open it for saving.
	DEBUG_MESSAGE("TRI_Master_Save: Opening filepath: ", TRI_Master.szXMLPath)
	DEBUG_MESSAGE("TRI_Master_Save: Opening filename: ", szFileName)
	CLEAR_NAMED_DEBUG_FILE(TRI_Master.szXMLPath, szFileName)
	OPEN_NAMED_DEBUG_FILE(TRI_Master.szXMLPath, szFileName)
	DEBUG_MESSAGE("TRI_Master_Save: Started writing Master XML file: TRI_Master")
	
	// Write Master XML file header.
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<!-- This file has been generated using Single Player Races widget -->", TRI_Master.szXMLPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(TRI_Master.szXMLPath, szFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<!-- Do NOT edit this file by hand unless you know what you're doing -->", TRI_Master.szXMLPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(TRI_Master.szXMLPath, szFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<?xml version='1.0'?>", TRI_Master.szXMLPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(TRI_Master.szXMLPath, szFileName)
	DEBUG_MESSAGE("TRI_Master_Save: Wrote Master XML file header")
	
	// Write "Master" node (open).
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<Master>", TRI_Master.szXMLPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(TRI_Master.szXMLPath, szFileName)
	DEBUG_MESSAGE("TRI_Master_Save: Wrote 'Master' node (open)")
	
		// Write "Races" node (open).
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	<Races>", TRI_Master.szXMLPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(TRI_Master.szXMLPath, szFileName)
		DEBUG_MESSAGE("TRI_Master_Save: Wrote 'Races' node (open)")
		
		// Loop through race filenames.
		REPEAT TRI_Master.iRaceCnt iCnt
		
			// Write "Race" node (open/close), if needed.
			IF NOT IS_STRING_EMPTY(TRI_Master.szRaceFileName[iCnt])
				SAVE_STRING_TO_NAMED_DEBUG_FILE("		<Race FileName = \"", TRI_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE(TRI_Master.szRaceFileName[iCnt], TRI_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" Name = \"", TRI_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE(TRI_Master.szRaceName[iCnt], TRI_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" Time = \"", TRI_Master.szXMLPath, szFileName)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(TRI_Master.fRaceTime[iCnt], TRI_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", TRI_Master.szXMLPath, szFileName)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(TRI_Master.szXMLPath, szFileName)
				PRINTSTRING("TRI_Master_Save: Wrote 'Race' node (open/close): ")
				PRINTINT(iCnt)
				PRINTNL()
			ENDIF
			
		ENDREPEAT
		
		// Write "Races" node (close).
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	</Races>", TRI_Master.szXMLPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(TRI_Master.szXMLPath, szFileName)
		DEBUG_MESSAGE("TRI_Master_Save: Wrote 'Races' node (close)")
		
	// Write "Master" node (close).
	SAVE_STRING_TO_NAMED_DEBUG_FILE("</Master>", TRI_Master.szXMLPath, szFileName)
	DEBUG_MESSAGE("TRI_Master_Save: Wrote 'Master' node (close)")
	
	// Close Master XML file.
	CLOSE_DEBUG_FILE()
	DEBUG_MESSAGE("TRI_Master_Save: Finished writing Master XML file: TRI_Master")
	
	// Master successfully saved.
	RETURN TRUE
	
ENDFUNC
#ENDIF


// -----------------------------------
// MAIN PROCS/FUNCTIONS
// -----------------------------------




// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!
