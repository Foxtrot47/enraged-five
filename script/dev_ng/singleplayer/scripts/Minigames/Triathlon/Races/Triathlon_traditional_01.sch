// This file has been generated from its matching xml using Single Player Races widget
// Do NOT edit this file by hand because your changes will likely get overwritten

PROC TRI_Setup_Triathlon_traditional_01(TRI_RACE_STRUCT& Race)

	TRI_Race_Init(Race)

	CPRINTLN(DEBUG_TRIATHLON, "Setup_TRI: Prevent peds in area")
//	Race.sbiStart = PREVENT_PEDS_FROM_BEING_IN_AREA(<<2380.1182, 4271.2197, 30.4804>>, <<2398.7256, 4292.2305, 32.3537>>)
	
	Race.iGateCnt = 23

	TRI_Gate_Setup(Race.sGate[0], <<2344.3071,4276.4766,30.0000>>, 8.5000, TRI_CHKPT_TRI_SWIM)
	TRI_Gate_Setup(Race.sGate[1], <<2212.3203,4205.1431,30.3360>>, 8.5000, TRI_CHKPT_TRI_SWIM)
	TRI_Gate_Setup(Race.sGate[2], <<2216.1750,4032.2991,30.1240>>, 8.5000, TRI_CHKPT_TRI_SWIM)
	TRI_Gate_Setup(Race.sGate[3], <<2168.1221,3905.1250,30.1750>>, 8.5000, TRI_CHKPT_TRI_FIRST_TRANS)
	TRI_Gate_Setup(Race.sGate[4], <<2142.9500,3892.5349,32.1000>>, 50.0000, TRI_CHKPT_TRI_BIKE, FALSE)
	TRI_Gate_Setup(Race.sGate[5], <<2043.1547,3834.4133,34.8167>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[6], <<1972.6597,3794.6755,31.2204>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[7], <<1933.5640,3760.5691,31.3200>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[8], <<1946.8220,3717.2749,31.1175>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[9], <<1804.9470,3653.6079,33.2508>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[10], <<1766.2800,3675.3359,33.2691>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[11], <<1706.0610,3661.6040,33.9931>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[12], <<1663.7026,3708.0376,33.0052>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[13], <<1593.4935,3688.8545,33.4240>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[14], <<1554.1658,3762.5283,33.4527>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[15], <<1671.3547,3866.2966,33.8281>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[16], <<1751.2000,3925.2859,33.9080>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[17], <<1867.4130,3948.7529,32.0880>>, 12.0000, TRI_CHKPT_TRI_BIKE)
	TRI_Gate_Setup(Race.sGate[18], <<1968.2061,3884.2971,31.2565>>, 12.0000, TRI_CHKPT_TRI_SECOND_TRANS)
	TRI_Gate_Setup(Race.sGate[19], <<1882.0480,3848.5720,31.3166>>, 6.5000, TRI_CHKPT_TRI_RUN)
	TRI_Gate_Setup(Race.sGate[20], <<1857.7640,3893.7009,32.0166>>, 6.5000, TRI_CHKPT_TRI_RUN)
	TRI_Gate_Setup(Race.sGate[21], <<1773.0430,3870.5979,33.3167>>, 6.5000, TRI_CHKPT_TRI_RUN)
	TRI_Gate_Setup(Race.sGate[22], <<1758.0510,3897.6960,33.8378>>, 8.5000, TRI_CHKPT_TRI_FINISH)

	Race.iRacerCnt = 8

	TRI_Racer_Setup_Misc(Race.Racer[0])
	TRI_Racer_Setup_Name(Race.Racer[0], "Player")
	TRI_Racer_Setup_Entities(Race.Racer[0], PLAYER_PED_ID(), TRI_Master.PlayerVeh)
	TRI_Racer_Setup_Start(Race.Racer[0], <<2395.4331,4281.8599,31.6057>>, 98.0000)
	TRI_Racer_Setup_Types(Race.Racer[0], PEDTYPE_PLAYER1, PLAYER_ONE, DUMMY_MODEL_FOR_SCRIPT)

	TRI_Racer_Setup_Misc(Race.Racer[1])
	TRI_Racer_Setup_Name(Race.Racer[1], "Tim")
	TRI_Racer_Setup_Entities(Race.Racer[1], NULL, NULL)
	TRI_Racer_Setup_Start(Race.Racer[1], <<2397.0830,4276.6348,31.5807>>, 85.0000)
	TRI_Racer_Setup_Types(Race.Racer[1], PEDTYPE_CIVMALE, A_M_Y_RoadCyc_01, DUMMY_MODEL_FOR_SCRIPT)

	TRI_Racer_Setup_Misc(Race.Racer[2])
	TRI_Racer_Setup_Name(Race.Racer[2], "Bill")
	TRI_Racer_Setup_Entities(Race.Racer[2], NULL, NULL)
	TRI_Racer_Setup_Start(Race.Racer[2], <<2394.1079,4287.5601,31.2057>>, 120.0000)
	TRI_Racer_Setup_Types(Race.Racer[2], PEDTYPE_CIVMALE, A_M_Y_RoadCyc_01, DUMMY_MODEL_FOR_SCRIPT)

	TRI_Racer_Setup_Misc(Race.Racer[3])
	TRI_Racer_Setup_Name(Race.Racer[3], "Fred")
	TRI_Racer_Setup_Entities(Race.Racer[3], NULL, NULL)
	TRI_Racer_Setup_Start(Race.Racer[3], <<2397.6831,4274.6851,31.6057>>, 75.0000)
	TRI_Racer_Setup_Types(Race.Racer[3], PEDTYPE_CIVMALE, A_M_Y_RoadCyc_01, DUMMY_MODEL_FOR_SCRIPT)

	TRI_Racer_Setup_Misc(Race.Racer[4])
	TRI_Racer_Setup_Name(Race.Racer[4], "Chad")
	TRI_Racer_Setup_Entities(Race.Racer[4], NULL, NULL)
	TRI_Racer_Setup_Start(Race.Racer[4], <<2394.9331,4283.8101,31.5307>>, 98.0000)
	TRI_Racer_Setup_Types(Race.Racer[4], PEDTYPE_CIVMALE, A_M_Y_RoadCyc_01, DUMMY_MODEL_FOR_SCRIPT)

	TRI_Racer_Setup_Misc(Race.Racer[5])
	TRI_Racer_Setup_Name(Race.Racer[5], "Travis")
	TRI_Racer_Setup_Entities(Race.Racer[5], NULL, NULL)
	TRI_Racer_Setup_Start(Race.Racer[5], <<2396.6831,4278.5098,31.5807>>, 98.0000)
	TRI_Racer_Setup_Types(Race.Racer[5], PEDTYPE_CIVMALE, A_M_Y_RoadCyc_01, DUMMY_MODEL_FOR_SCRIPT)

	TRI_Racer_Setup_Misc(Race.Racer[6])
	TRI_Racer_Setup_Name(Race.Racer[6], "Kevin")
	TRI_Racer_Setup_Entities(Race.Racer[6], NULL, NULL)
	TRI_Racer_Setup_Start(Race.Racer[6], <<2396.1079,4279.9102,31.6307>>, 98.0000)
	TRI_Racer_Setup_Types(Race.Racer[6], PEDTYPE_CIVMALE, A_M_Y_RoadCyc_01, DUMMY_MODEL_FOR_SCRIPT)

	TRI_Racer_Setup_Misc(Race.Racer[7])
	TRI_Racer_Setup_Name(Race.Racer[7], "Ted")
	TRI_Racer_Setup_Entities(Race.Racer[7], NULL, NULL)
	TRI_Racer_Setup_Start(Race.Racer[7], <<2394.4331,4285.6348,31.2807>>, 98.0000)
	TRI_Racer_Setup_Types(Race.Racer[7], PEDTYPE_CIVMALE, A_M_Y_RoadCyc_01, DUMMY_MODEL_FOR_SCRIPT)

ENDPROC

// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!
