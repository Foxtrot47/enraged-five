USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE

/// PURPOSE: 
///    Sets up locate and cutscene for Tonya RC missions
FUNC BOOL SetupScene_TONYA(g_structRCScriptArgs& sRCLauncherData)

	// Variables
	BOOL bCreatedScene
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
		
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.activationRange = 11
			sRCLauncherData.bAllowVehicleActivation = FALSE
	
			// Set appropriate cutscene
			IF sRCLauncherData.eMissionId = RC_TONYA_1
				CPRINTLN(DEBUG_RANDOM_CHAR, "SETTING UP CUT SCENE LOAD: tonya_mcs_1")
				sRCLauncherData.sIntroCutscene = "tonya_mcs_1"
				
			ELIF sRCLauncherData.eMissionId = RC_TONYA_2
				CPRINTLN(DEBUG_RANDOM_CHAR, "SETTING UP CUT SCENE LOAD: tonya_mcs_2")
				sRCLauncherData.sIntroCutscene = "tonya_mcs_2"
				
			ELIF sRCLauncherData.eMissionId = RC_TONYA_5
				CPRINTLN(DEBUG_RANDOM_CHAR, "SETTING UP CUT SCENE LOAD: tonya_mcs_3")
				sRCLauncherData.sIntroCutscene = "tonya_mcs_3"
			ENDIF
			
			// Request models
			REQUEST_MODEL(IG_TONYA)
			
			// Request anims
			REQUEST_ANIM_DICT("special_ped@tonya@intro")
			REQUEST_ANIM_DICT("special_ped@tonya@base")
			REQUEST_ANIM_DICT("special_ped@tonya@franklin_5@franklin_5a")
			REQUEST_ANIM_DICT("special_ped@tonya@franklin_5@franklin_5b")
			REQUEST_ANIM_DICT("special_ped@tonya@franklin_5@franklin_5c")

			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE
			
			IF NOT HAS_MODEL_LOADED(IG_TONYA)
			OR NOT HAS_ANIM_DICT_LOADED("special_ped@tonya@intro")
			OR NOT HAS_ANIM_DICT_LOADED("special_ped@tonya@base")
			OR NOT HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_5@franklin_5a")
			OR NOT HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_5@franklin_5b")
			OR NOT HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_5@franklin_5c")
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Tonya
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				sRCLauncherData.pedID[0] = CREATE_PED(PEDTYPE_CIVFEMALE, IG_TONYA,  <<-16.53, -1473.12, 29.61>>, 319.56)
				
				// Setup Tonya
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
					
					// Attributes
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[0], TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(sRCLauncherData.pedID[0], TRUE)
					SET_PED_MODEL_IS_SUPPRESSED(IG_TONYA, TRUE)
					SET_PED_CAN_BE_TARGETTED(sRCLauncherData.pedID[0], FALSE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sRCLauncherData.pedID[0], FALSE)
					SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_UseKinematicModeWhenStationary, TRUE)
	
					// Appearance
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
	
		CASE IS_COMPLETE_SCENE
			
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
			AND IS_PLAYER_PLAYING((PLAYER_ID()))
				
				// Apply task sequence
				SEQUENCE_INDEX iSequence
				CLEAR_SEQUENCE_TASK(iSequence)
				OPEN_SEQUENCE_TASK(iSequence)
					TASK_PLAY_ANIM(NULL, "special_ped@tonya@intro", "idle_intro", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
					TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_USE_KINEMATIC_PHYSICS)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
				CLOSE_SEQUENCE_TASK(iSequence)
				
				TASK_PERFORM_SEQUENCE(sRCLauncherData.pedID[0], iSequence)
				CLEAR_SEQUENCE_TASK(iSequence)
				
				// Look at the player
				TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[0], PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
			ENDIF
	
			// Clear area around Tonya
			CLEAR_AREA_OF_OBJECTS( <<-16.53, -1473.12, 29.61>> , 1.0)
				
			// Release model
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_TONYA)

			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
