//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	ambient_Tonya.sc											//
//		AUTHOR			:																//
//		DESCRIPTION		:	Handles Tonya having conversation at end of Tonya 1			//
//																						//
////////////////////////////////////////////////////////////////////////////////////////// 


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// Includes
USING "commands_entity.sch"
USING "RC_Helper_Functions.sch"
USING "savegame_public.sch"
USING "rich_presence_public.sch"

ENUM TONYA_CALL_STATES
	TONYA_CALL_INIT = 0,
	TONYA_CALL_LOAD_ASSETS,
	TONYA_CALL_UPDATE,
	TONYA_CALL_CLEANUP
ENDENUM

TONYA_CALL_STATES tonyaCallStages = TONYA_CALL_INIT

PED_INDEX pedTonya
OBJECT_INDEX oTonyaCellPhone

INT iPostMissionTonyaStages = 0

FLOAT fCheckDistance 		= 50
FLOAT fCheckDistance2 		= 2500

VECTOR vTonyaEndingPosition = <<409.1539, -1626.6769, 28.2928>>
VECTOR vPlayerLocation

// Tonya Ending Positions
VECTOR vEndingPosition01 = << 409.27472, -1623.02185, 28.29278 >>
FLOAT fEndingheading01 = 202.6928

VECTOR vEndingPosition02 = <<415.6071, -1647.6045, 28.2928>>
FLOAT fEndingheading02 = 85.7173

VECTOR vChosenEndingPosition
FLOAT fChosenEndingHeading 

FLOAT fDistancePosition01, fDistancePosition02

structPedsForConversation MyLocalDispatchStruct

PROC SCRIPT_CLEANUP()

	REMOVE_ANIM_DICT("amb@world_human_stand_mobile@female@standing@call@enter")
	REMOVE_ANIM_DICT("amb@world_human_stand_mobile@female@standing@call@base")
	REMOVE_ANIM_DICT("amb@world_human_stand_mobile@female@standing@call@exit")
	REMOVE_ANIM_DICT("amb@world_human_smoking@female@enter")
	REMOVE_ANIM_DICT("amb@world_human_smoking@female@idle_a")
	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Phone_ING)

	IF DOES_ENTITY_EXIST(pedTonya)
		IF IS_ENTITY_OCCLUDED(pedTonya)
			DELETE_PED(pedTonya)
			CPRINTLN(DEBUG_AMBIENT, "DELETING PED - pedTonya") 
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedTonya)
			CPRINTLN(DEBUG_AMBIENT, "SETTING PED - pedTonya AS NO LONGER NEEDED") 
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "TERMINATING TONYACALL SCRIPT") 
    TERMINATE_THIS_THREAD()
ENDPROC

FUNC BOOL HANDLE_PLAYER_BUMP_AND_AGGRESSION()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedTonya)
		IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedTonya)
		OR IS_PED_SHOOTING(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(oTonyaCellPhone)
				DETACH_ENTITY(oTonyaCellPhone)
			ENDIF
			
			STOP_SCRIPTED_CONVERSATION(FALSE)
			
			IF GET_SCRIPT_TASK_STATUS(pedTonya, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
				SET_PED_KEEP_TASK(pedTonya, TRUE)
				TASK_SMART_FLEE_PED(pedTonya, PLAYER_PED_ID(), 1000, -1)
				PRINTLN("PLAYER BUMPED INTO TONYA, HAVE HER FLEE")
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC 

PROC REMOVE_TONYAS_PHONE()
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		IF GET_SEQUENCE_PROGRESS(pedTonya) = 1
			IF DOES_ENTITY_EXIST(oTonyaCellPhone)
				DELETE_OBJECT(oTonyaCellPhone)
				CPRINTLN(DEBUG_AMBIENT, "DELETING TONYA'S CELLPHONE")
				
				TASK_LOOK_AT_ENTITY(pedTonya, PLAYER_PED_ID(), -1)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		IF DOES_ENTITY_EXIST(oTonyaCellPhone)
			IF GET_SCRIPT_TASK_STATUS(pedTonya, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK
				DELETE_OBJECT(oTonyaCellPhone)
				CPRINTLN(DEBUG_AMBIENT, "DELETING TONYA'S CELLPHONE - 01")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HANDLE_TONYA_POST_DROPOFF()
	VECTOR vPlayerPosition, vTonyaPosition
	SEQUENCE_INDEX iSeq
	FLOAT fAnimTime
	INT iRand
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_ENTITY_DEAD(pedTonya)
		vTonyaPosition = GET_ENTITY_COORDS(pedTonya)
	ELSE
		vTonyaPosition = <<408.5002, -1624.5825, 29.2928>>
	ENDIF
	
	IF VDIST2(vPlayerPosition, vTonyaPosition) > 10000 // 100m
	OR IS_ENTITY_DEAD(pedTonya)
	OR HANDLE_PLAYER_BUMP_AND_AGGRESSION()
		IF VDIST2(vPlayerPosition, vTonyaPosition) > 10000
			CPRINTLN(DEBUG_AMBIENT, "DISTANCE FAIL")
		ENDIF
		IF IS_ENTITY_DEAD(pedTonya)
			CPRINTLN(DEBUG_AMBIENT, "DEAD FAIL")
		ENDIF
		IF HANDLE_PLAYER_BUMP_AND_AGGRESSION()
			CPRINTLN(DEBUG_AMBIENT, "BUMP FAIL")
		ENDIF
		CPRINTLN(DEBUG_AMBIENT, "RETURNING TRUE - HANDLE_TONYA_POST_DROPOFF VIA TONYACALL SCRIPT")
		RETURN TRUE
	ENDIF
	
	SWITCH iPostMissionTonyaStages
		CASE 0
			IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
				IF IS_ENTITY_AT_COORD(pedTonya, vChosenEndingPosition, <<1.0,1.0,1.0>>)
					
					CLEAR_SEQUENCE_TASK(iSeq)
					OPEN_SEQUENCE_TASK(iSeq)
						TASK_ACHIEVE_HEADING(NULL, fChosenEndingHeading)
						TASK_PLAY_ANIM(NULL, "amb@world_human_stand_mobile@female@standing@call@enter", "enter", SLOW_BLEND_IN, SLOW_BLEND_OUT)
						TASK_PLAY_ANIM(NULL, "amb@world_human_stand_mobile@female@standing@call@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(iSeq)
					TASK_PERFORM_SEQUENCE(pedTonya, iSeq)
					CLEAR_SEQUENCE_TASK(iSeq)
					
					CPRINTLN(DEBUG_AMBIENT, "iPostMissionTonyaStages = 1")
					iPostMissionTonyaStages = 1
				ELSE
//					CPRINTLN(DEBUG_AMBIENT, "FAILING DISTANCE CHECK")
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
				IF GET_SEQUENCE_PROGRESS(pedTonya) = 1
					IF NOT DOES_ENTITY_EXIST(oTonyaCellPhone)
						IF IS_ENTITY_PLAYING_ANIM(pedTonya, "amb@world_human_stand_mobile@female@standing@call@enter", "enter")
							fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(pedTonya, "amb@world_human_stand_mobile@female@standing@call@enter", "enter")
					
							IF fAnimTime >= 0.157
								oTonyaCellPhone = CREATE_OBJECT(Prop_Phone_ING, <<1,1,1>>)
								ATTACH_ENTITY_TO_ENTITY(oTonyaCellPhone, pedTonya, GET_PED_BONE_INDEX(pedTonya, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
								CPRINTLN(DEBUG_AMBIENT, "CREATING AND ATTACHING CELL PHONE TO TONYA")
							ENDIF
						ENDIF
					ENDIF
				ELIF GET_SEQUENCE_PROGRESS(pedTonya) = 2
					iRand = GET_RANDOM_INT_IN_RANGE() % 2
				
					IF iRand = 0
						IF CREATE_CONVERSATION(MyLocalDispatchStruct, "TOWAUD", "TONYA_CALL1", CONV_PRIORITY_HIGH)
							CPRINTLN(DEBUG_AMBIENT, "CALL1: iPostMissionTonyaStages = 2")
							iPostMissionTonyaStages = 2
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(MyLocalDispatchStruct, "TOWAUD", "TONYA_CALL2", CONV_PRIORITY_HIGH)
							CPRINTLN(DEBUG_AMBIENT, "CALL2: iPostMissionTonyaStages = 2")
							iPostMissionTonyaStages = 2
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_ENTITY_DEAD(pedTonya)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
					CLEAR_SEQUENCE_TASK(iSeq)
					OPEN_SEQUENCE_TASK(iSeq)
						TASK_PLAY_ANIM(NULL, "amb@world_human_stand_mobile@female@standing@call@exit", "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT)
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", -1, TRUE)
					CLOSE_SEQUENCE_TASK(iSeq)
					TASK_PERFORM_SEQUENCE(pedTonya, iSeq)
					CLEAR_SEQUENCE_TASK(iSeq)
				
					CPRINTLN(DEBUG_AMBIENT, "iPostMissionTonyaStages = 3")
					iPostMissionTonyaStages = 3
				ENDIF
			ENDIF
		BREAK
		CASE 3
			REMOVE_TONYAS_PHONE()
		
			IF VDIST2(vPlayerPosition, vTonyaPosition) < 25 //5m
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON
					IF CREATE_CONVERSATION(MyLocalDispatchStruct, "TOWAUD", "TONYA_MESS", CONV_PRIORITY_HIGH)
						SETTIMERA(0)
						CPRINTLN(DEBUG_AMBIENT, "iPostMissionTonyaStages = 4")
						iPostMissionTonyaStages = 4
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4
			REMOVE_TONYAS_PHONE()
		
			IF TIMERA() > 10000
				CPRINTLN(DEBUG_AMBIENT, "GO BACK: iPostMissionTonyaStages = 3")
				iPostMissionTonyaStages = 3
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC FIND_CLOSEST_ENDING_POSITION() 
	VECTOR vTonyaPositon

	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		vTonyaPositon = GET_ENTITY_COORDS(pedTonya)
		
		IF IS_VECTOR_ZERO(vChosenEndingPosition)
			fDistancePosition01 = VDIST(vTonyaPositon, vEndingPosition01)
			PRINTLN("TONYA CALL: fDistancePosition01 =  ", fDistancePosition01)
			
			fDistancePosition02 = VDIST(vTonyaPositon, vEndingPosition02)
			PRINTLN("TONYA CALL: fDistancePosition02 =  ", fDistancePosition02)
			
			IF fDistancePosition01 < fDistancePosition02
				vChosenEndingPosition = vEndingPosition01
				fChosenEndingHeading = fEndingheading01
				PRINTLN("TONYA CALL: WE CHOSE DISTANCE 1")
			ELSE
				vChosenEndingPosition = vEndingPosition02
				fChosenEndingHeading = fEndingheading02
				PRINTLN("TONYA CALL: WE CHOSE DISTANCE 2")
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<407.428497,-1626.572021,27.292778>>, <<412.824524,-1620.166626,33.292778>>, 6.000000)
					vChosenEndingPosition = vEndingPosition02
					fChosenEndingHeading = fEndingheading02
					PRINTLN("TONYA CALL: OVERRIDE - PLAYER IS IN SPOT 1, USING DISTANCE 2")
				ENDIF
			ENDIF
			
			PRINTLN("TONYA CALL: vChosenEndingPosition = ", vChosenEndingPosition)
			PRINTLN("TONYA CALL: fChosenEndingHeading = ", fChosenEndingHeading)
		ELSE
//			PRINTLN("TONYA CALL: WE HAVE CHOSEN A POSITION")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL FIND_TONYA()
	VECTOR vPlayerPosition
	PED_INDEX returnPed
	MODEL_NAMES mnToTest

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	GET_CLOSEST_PED(vPlayerPosition, fCheckDistance, TRUE, TRUE, returnPed, FALSE, TRUE)
	
	IF DOES_ENTITY_EXIST(returnPed) AND NOT IS_ENTITY_DEAD(returnPed)
	
		mnToTest = GET_ENTITY_MODEL(returnPed)
		
		IF mnToTest = IG_TONYA
			pedTonya = returnPed
			SET_ENTITY_AS_MISSION_ENTITY(pedTonya)
			SET_PED_MONEY(pedTonya, 0) 
			SET_PED_CAN_BE_TARGETTED(pedTonya, FALSE) 
			SET_PED_NAME_DEBUG(pedTonya, "TONYA") 
			SET_PED_RELATIONSHIP_GROUP_HASH(pedTonya, RELGROUPHASH_PLAYER) 
			ADD_PED_FOR_DIALOGUE(MyLocalDispatchStruct, 3, pedTonya, "TONYA", TRUE) 
			CPRINTLN(DEBUG_AMBIENT, "WE FOUND TONYA")
			
			FIND_CLOSEST_ENDING_POSITION()
			
			IF NOT IS_VECTOR_ZERO(vChosenEndingPosition)
				IF GET_SCRIPT_TASK_STATUS(pedTonya, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedTonya, vChosenEndingPosition, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fChosenEndingHeading)
					SET_PED_KEEP_TASK(pedTonya, TRUE)
					PRINTLN("RETASKING TONYA TO WALK TO A COORDINATE - KEEP TASK SET")
				ENDIF
			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP)
		CPRINTLN(DEBUG_AMBIENT, "Ambient - TonyaCall: FORCE CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("ambient_TonyaCall")) > 1
    	PRINTLN(DEBUG_AMBIENT, "Ambient - TonyaCall: Attempting to launch with an instance already active...")
    	TERMINATE_THIS_THREAD()
    ENDIF

	// Main loop
	WHILE (TRUE)
		
		// Entity check
		IS_PLAYER_PLAYING(PLAYER_ID())
		
		// Immediately cleanup if this script has been completed via debug
		IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_TOWING))
			SCRIPT_CLEANUP()
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			vPlayerLocation = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		
		IF VDIST2(vPlayerLocation, vTonyaEndingPosition) > fCheckDistance2
			SCRIPT_CLEANUP()
		ENDIF

		// Script update
		SWITCH tonyaCallStages
			CASE TONYA_CALL_INIT
				IF FIND_TONYA()
					IF CREATE_CONVERSATION(MyLocalDispatchStruct, "TOWAUD", "TONYA_HANG", CONV_PRIORITY_HIGH)
						CPRINTLN(DEBUG_AMBIENT, "GOING TO STATE - TONYA_CALL_LOAD_ASSETS")
						tonyaCallStages = TONYA_CALL_LOAD_ASSETS
					ENDIF
				ENDIF
			BREAK
			CASE TONYA_CALL_LOAD_ASSETS
				
				REQUEST_ANIM_DICT("amb@world_human_stand_mobile@female@standing@call@enter")
				REQUEST_ANIM_DICT("amb@world_human_stand_mobile@female@standing@call@base")
				REQUEST_ANIM_DICT("amb@world_human_stand_mobile@female@standing@call@exit")
				REQUEST_ANIM_DICT("amb@world_human_smoking@female@enter")
				REQUEST_ANIM_DICT("amb@world_human_smoking@female@idle_a")
				REQUEST_MODEL(Prop_Phone_ING)
				
				IF HAS_MODEL_LOADED(Prop_Phone_ING)
				AND HAS_ANIM_DICT_LOADED("amb@world_human_stand_mobile@female@standing@call@enter")
				AND HAS_ANIM_DICT_LOADED("amb@world_human_stand_mobile@female@standing@call@base")
				AND HAS_ANIM_DICT_LOADED("amb@world_human_stand_mobile@female@standing@call@exit")
				AND HAS_ANIM_DICT_LOADED("amb@world_human_smoking@female@enter")
				AND HAS_ANIM_DICT_LOADED("amb@world_human_smoking@female@idle_a")
					CPRINTLN(DEBUG_AMBIENT, "GOING TO STATE - TONYA_CALL_UPDATE")
					tonyaCallStages = TONYA_CALL_UPDATE
				ENDIF
			BREAK
			CASE TONYA_CALL_UPDATE
				IF HANDLE_TONYA_POST_DROPOFF()
					CPRINTLN(DEBUG_AMBIENT, "GOING TO STATE - TONYA_CALL_CLEANUP")
					tonyaCallStages = TONYA_CALL_CLEANUP
				ENDIF
			BREAK
			CASE TONYA_CALL_CLEANUP
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT
