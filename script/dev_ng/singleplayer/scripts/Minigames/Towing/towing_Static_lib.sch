// towing_Static_lib.sch
USING "controller_Towing.sch"

USING "commands_streaming.sch"
USING "commands_entity.sch"
USING "commands_vehicle.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "candidate_public.sch"

VEHICLE_INDEX launchVehicle
THREADID staticTowScript
INT iStaticMissionCandID = NO_CANDIDATE_ID
INT iStaticFlags

/// PURPOSE:
///    Updates the player approaching the static towing launchpoint.
/// RETURNS:
///    TRUE that the player is within TOWING_STATIC_MAX_DIST.
FUNC BOOL TOWING_STATIC_UpdateApproachWait(VECTOR vPlayerPos)
	IF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
		IF VDIST2(vPlayerPos, << 1391.0229, -2534.7876, 47.4669 >>) < (TOWING_STATIC_MAX_DIST*TOWING_STATIC_MAX_DIST)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Inserts the streams for the static towing.
PROC TOWING_STATIC_InitStreams()
	REQUEST_MODEL(TOWTRUCK)
ENDPROC

/// PURPOSE:
///    Lets us know when the streaming is done.
/// RETURNS:
///    TRUE when streaming is done. FALSE until then.
FUNC BOOL TOWING_STATIC_Stream()
	RETURN HAS_MODEL_LOADED(TOWTRUCK)
ENDFUNC

/// PURPOSE:
///    Creates the vehicles needed for towing.
PROC TOWING_STATIC_Spawn()
	launchVehicle = CREATE_VEHICLE(TOWTRUCK, <<1387.913, -2535.613, 47.4669>>, 50.2)
ENDPROC

/// PURPOSE:
///    Waits for the conditions to launch the script to be met.
/// PARAMS:
///    vPlayerPos - 
/// RETURNS:
///    TOWING_RETVAL_FAIL if we're needing to complete exit the event.
///    TOWING_RETVAL_NONE if we should continue waiting.
///    TOWING_RETVAL_PASS if we can launch the event.
FUNC TOWING_UPDATE_RETVAL TOWING_STATIC_LaunchWait(VECTOR vPlayerPos)
	// Here, monior where the player is. If he gets too far away, we need to quit out.
	IF VDIST2(vPlayerPos, << 1391.0229, -2534.7876, 47.4669 >>) > (TOWING_STATIC_LEAVE_DIST*TOWING_STATIC_LEAVE_DIST)
		RETURN TOWING_RETVAL_FAIL
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// If we launched via debug, just go ahead and get us in the car.
		IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_LaunchedViaDebug)
			CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_LaunchedViaDebug)
			IF NOT IS_ENTITY_DEAD(launchVehicle)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), launchVehicle)
			ENDIF
		ENDIF
	#ENDIF
	
	// Okay, check to see if the player is in the vehicle.
	IF NOT IS_ENTITY_DEAD(launchVehicle)
		PED_INDEX playerPed = PLAYER_PED_ID()
		IF IS_PED_IN_ANY_VEHICLE(playerPed)
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(playerPed)
			IF NOT IS_ENTITY_DEAD(tempVeh)
				IF (tempVeh = launchVehicle)
					IF (GET_PED_IN_VEHICLE_SEAT(tempVeh) = playerPed)
						//SET_VEHICLE_AS_NO_LONGER_NEEDED(launchVehicle)	
						
						REQUEST_SCRIPT("Towing")
						
						// We're in the car!
						RETURN TOWING_RETVAL_PASS
					ENDIF
				ENDIF
			ENDIF
		ENDIF
				
		// 
	ELSE
		// Whoa.. car's dead...
		RETURN TOWING_RETVAL_FAIL
	ENDIF
	
	// Nothing to report.
	RETURN TOWING_RETVAL_NONE
ENDFUNC

/// PURPOSE:
///    Launches the towing script.
FUNC TOWING_UPDATE_RETVAL TOWING_STATIC_Launch()
	IF HAS_SCRIPT_LOADED("Towing")
		//Deal with the mission candidate system here.
		m_enumMissionCandidateReturnValue eLaunchVal = Request_Mission_Launch(iStaticMissionCandID, MCTID_CONTACT_POINT)
		
		// Handle return code.
		IF (eLaunchVal = MCRET_ACCEPTED) 
			DEBUG_MESSAGE("PASSING static towing launch!")
			
			TOWING_LAUNCH_DATA launchArgs
			launchArgs.launchMode = TOWING_MODE_Patrol
			staticTowScript = START_NEW_SCRIPT_WITH_ARGS("Towing", launchArgs, SIZE_OF(launchArgs), MISSION_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("Towing")
			SET_BITMASK_AS_ENUM(iStaticFlags, TOWING_FLAG_MISSION_CAND_LAUNCHED)
			
			RETURN TOWING_RETVAL_PASS
			
		ELIF (eLaunchVal = MCRET_DENIED) 
			RETURN TOWING_RETVAL_FAIL
		
		ELSE
#IF IS_DEBUG_BUILD
			TEXT_LABEL IntegerAsString = iStaticMissionCandID
			DEBUG_MESSAGE("PROCESSING static towing launch, candidate ID: ", IntegerAsString)
#ENDIF	//	IS_DEBUG_BUILD
		ENDIF
	ENDIF

	RETURN TOWING_RETVAL_NONE
ENDFUNC

FUNC TOWING_UPDATE_RETVAL TOWING_STATIC_RunWait()
	IF NOT IS_THREAD_ACTIVE(staticTowScript)
		RETURN TOWING_RETVAL_PASS
	ENDIF
	
	// Nothing to report.
	RETURN TOWING_RETVAL_NONE
ENDFUNC

/// PURPOSE:
///    Clean all data for the static towing launcher.
PROC TOWING_STATIC_Cleanup()
	IF IS_BITMASK_AS_ENUM_SET(iStaticFlags, TOWING_FLAG_MISSION_CAND_LAUNCHED)
		CLEAR_BITMASK_AS_ENUM(iStaticFlags, TOWING_FLAG_MISSION_CAND_LAUNCHED)
		Mission_Over(iStaticMissionCandID)
	ENDIF
						
	IF IS_THREAD_ACTIVE(staticTowScript)
		TERMINATE_THREAD(staticTowScript)
	ENDIF
	
	SET_SCRIPT_AS_NO_LONGER_NEEDED("Towing")
	SET_VEHICLE_AS_NO_LONGER_NEEDED(launchVehicle)
	SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
ENDPROC

/// PURPOSE:
///    Handles the main update loop for the static location towing.
///    This includes streaming, waiting for approaches, cleaning, etc.
PROC TOWING_STATIC_MainUpdate(TOWING_STATE & eStaticState, VECTOR vPlayerPos, VEHICLE_INDEX & staticLaunchVeh)
	// We're going to init the static launch vehicle here, just in case we need it below.
	staticLaunchVeh = launchVehicle
	
	SWITCH (eStaticState)
		CASE TOWING_INIT
			// Do any init here.
			//DEBUG_MESSAGE("STATIC_TOWING: TOWING_INIT")
			eStaticState = TOWING_APPROACH_WAIT
		BREAK
		
		CASE TOWING_APPROACH_WAIT
			//DEBUG_MESSAGE("STATIC_TOWING: TOWING_APPROACH_WAIT")
			IF TOWING_STATIC_UpdateApproachWait(vPlayerPos)
				eStaticState = TOWING_STREAM_INIT
			ENDIF
		BREAK
		
		CASE TOWING_STREAM_INIT
			//DEBUG_MESSAGE("STATIC_TOWING: TOWING_STREAM_INIT")
			TOWING_STATIC_InitStreams()
			eStaticState = TOWING_STREAM_WAIT
		BREAK
		
		CASE TOWING_STREAM_WAIT
			//DEBUG_MESSAGE("STATIC_TOWING: TOWING_STREAM_WAIT")
			IF TOWING_STATIC_Stream()
				eStaticState = TOWING_SPAWN					
			ENDIF
		BREAK
		
		CASE TOWING_SPAWN
			//DEBUG_MESSAGE("STATIC_TOWING: TOWING_SPAWN")
			TOWING_STATIC_Spawn()
			eStaticState = TOWING_LAUNCH_WAIT
		BREAK
		
		CASE TOWING_LAUNCH_WAIT
			//DEBUG_MESSAGE("STATIC_TOWING: TOWING_LAUNCH_WAIT")
			TOWING_UPDATE_RETVAL launchWaitRetVal
			launchWaitRetVal = TOWING_STATIC_LaunchWait(vPlayerPos)
			
			IF (launchWaitRetVal = TOWING_RETVAL_PASS)
				eStaticState = TOWING_LAUNCH
	
			ELIF (launchWaitRetVal = TOWING_RETVAL_FAIL)
				TOWING_STATIC_Cleanup()
				eStaticState = TOWING_INIT
			ENDIF
		BREAK
		
		CASE TOWING_LAUNCH
			//DEBUG_MESSAGE("STATIC_TOWING: TOWING_LAUNCH")
			TOWING_UPDATE_RETVAL launchRetVal
			launchRetVal = TOWING_STATIC_Launch()
			
			IF (launchRetVal = TOWING_RETVAL_PASS)
				eStaticState = TOWING_RUN_WAIT
			ELIF (launchRetVal = TOWING_RETVAL_FAIL)
				TOWING_STATIC_Cleanup()
				eStaticState = TOWING_INIT
			ENDIF
		BREAK
		
		CASE TOWING_RUN_WAIT
			//DEBUG_MESSAGE("STATIC_TOWING: TOWING_RUN_WAIT")
			TOWING_UPDATE_RETVAL runWaitRetVal
			runWaitRetVal = TOWING_STATIC_RunWait()
			
			IF (runWaitRetVal = TOWING_RETVAL_PASS)
				TOWING_STATIC_Cleanup()
				eStaticState = TOWING_INIT
			ENDIF
		BREAK
	ENDSWITCH
	
	// Return the launch vehicle by ref. It may or may not exist yet. We don't really care here, but dynamic towing will.
	staticLaunchVeh = launchVehicle
ENDPROC

