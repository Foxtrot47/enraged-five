// towing_MissionData.sch

/// PURPOSE:
///    Gives the towing controller the position of the mission for this rank.
/// PARAMS:
///    iMissionRank - the player's current rank.
/// RETURNS:
///    The world location of the current mission rank.
FUNC VECTOR TOWING_RetrieveMissionPosition(INT iMissionRank)
	IF (iMissionRank = 0)
		// Took this position from Towing.sc
		RETURN <<1387.9126, -2535.6133, 47.4669>>
	ENDIF
	
	RETURN <<0, 0, 0>>
ENDFUNC
