//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     : Towing_Data                                             		//
//      AUTHOR          : Steven Messinger                                              //
//      DESCRIPTION     : Script that defines the data needed to run towing odd job     //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "model_enums.sch"
USING "minigames_helpers.sch"

CONST_INT MAX_CAR_POS 2   //The maximum amount of car positions per node
CONST_INT MAX_NUM_NODES 1 //ONLY one node now that patrol is cut 7 //The maximum number of locations 
CONST_INT MAX_NUM_ILLEGAL_NODES 53 //The maximum number of locations 
CONST_INT MAX_NUM_CAR_CHOICES 10
CONST_INT MAX_NUM_PED_CHOICES 6 
CONST_INT MAX_NUM_SEX_GUY_CHOICES 5
CONST_INT MAX_NUM_HOMELESS_CHOICES 6
CONST_INT NUM_ACCIDENT_NODES 17

CONST_INT NUM_GARAGES 8
CONST_INT NUM_IMPOUND 1

ENUM FAIL_REASON_ENUM // reasons for failure
	FAIL_ABANDONED_CAR = 0,
	FAIL_DISTANCE_FAIL,
	FAIL_TRANSPORT_DESTROYED,
	FAIL_WANTED,
	FAIL_VEHICLE_DISABLED,
	FAIL_TOW_DESTROYED,
	FAIL_TIME_EXPIRED,
	FAIL_COMMUTE_TIME,
	FAIL_PED_KILLED,
	FAIL_PED_KILLED_PASSENGER,
	FAIL_FRANKLIN_NO_EXIT,
	FAIL_ABANDONED_JOB,
	FAIL_ABANDONED_DRIVER,
	FAIL_DRIVER_GOT_CAR_BACK,
	FAIL_TIME_EXPIRED_TOTAL,
	FAIL_DAMAGE,
	FAIL_PED_ATTACKED,
	FAIL_PED_ATTACKED_TRAMP,
	FAIL_PED_THREATENED,
	FAIL_PED_THREATENED_WITH_WEAPON,
	FAIL_PED_THREATENED_WITH_WEAPON_AIMING,
	FAIL_ATTACKED_TOW_TRUCK_OWNER,
	FAIL_ATTACKED_TONYA,
	FAIL_ABANDONED_TONYA
ENDENUM

ENUM DAMAGE_TYPE_ENUM
	DAMAGE_FRONT_RIGHT = 0,
	DAMAGE_FRONT_LEFT,
	DAMAGE_HEAD_ON
ENDENUM

ENUM VARIATION
	VARIATION_TRADIITONAL = 0,
	VARIATION_PATROL
ENDENUM

VECTOR vGarages[NUM_GARAGES]
VECTOR vImpound[NUM_IMPOUND]

MODEL_NAMES mnExtraModel
MODEL_NAMES gbPedList[MAX_NUM_PED_CHOICES]
MODEL_NAMES gbCarList[MAX_NUM_CAR_CHOICES]

STRUCT ILLEGAL_NODE
	NODE_TYPE myType
	VECTOR vCarPos//the vectors to create the broken down car at.  
	FLOAT fCarRot  //the z rotation value
	
	BOOL bBackward
	//BOOL bUsed 
	
	VECTOR vPedPos
	FLOAT fPedRot
	
	VECTOR vSpeedNodeLocation
	
	//Angled area data
	VECTOR vAngledPos1
	VECTOR vAngledPos2
	FLOAT fAngledWidth
	
	MODEL_NAMES mnSpecificCar
	DAMAGE_TYPE_ENUM myDamage //type of damage to car
	INT extraDataIndex
ENDSTRUCT

//Data that's needed only in the accident variation that we don't want inherited by all the other nodes.
STRUCT ACCIDENT_DATA
	VECTOR vUtilityPos[3] //Vectors around tow node to be used for spawning props or bystanders
	VECTOR vBloodSplatter[2]
	//VECTOR vUtilityRot[MAX_NUM_BYSTANDERS]
	VECTOR vMin
	VECTOR vMax
	VECTOR vTrafficPos
	FLOAT fTrafficOrient
	INT numBystanders
ENDSTRUCT



//STRUCT TOW_NODE
//	VECTOR vCarPos[MAX_CAR_POS] //the vectors to create the broken down car at.  Currently each broken down spot has 2 possible car locations
//	FLOAT fCarRot[MAX_CAR_POS]  //the z rotation value
//	
//	VECTOR vDropPos[MAX_CAR_POS] //the vectors to tow the cars to
//	BOOL bUsed 
//	
//	MODEL_NAMES blockCarName
//	VECTOR vblockedCarPos
//	FLOAT fblockedHeading
//	//TODO Maybe specify the type of car?
//ENDSTRUCT

STRUCT DEBUG_POS_DATA
	BOOL bTurnOnDebugPos
	BOOL bNodeSelected
	INT iNode
ENDSTRUCT

PROC AddWidgets(DEBUG_POS_DATA& myData)
	UNUSED_PARAMETER(myData)
#IF IS_DEBUG_BUILD
	START_WIDGET_GROUP("Towing")
	ADD_WIDGET_BOOL("Draw Nodes", myData.bTurnOnDebugPos)
	ADD_WIDGET_INT_SLIDER("Node selection", myData.iNode, -1, MAX_NUM_ILLEGAL_NODES, 1)
	ADD_WIDGET_BOOL("Generate Selected Node", myData.bNodeSelected)
	STOP_WIDGET_GROUP()
#ENDIF
ENDPROC

PROC POPULATE_LOCATIONS()
	//If these indexes change we also need to change GET_DATA_FROM_GARAGE_LOCATION

	//City
	vGarages[0] = <<-227.6000, -1172.1000, 21.8963>> //<<-227.6, -1172.1, 22.94>>  //<<-213, -1136.2, 35.5>>
	vGarages[1] = <<-205.6866, -1384.3333, 30.2585>> //<< -206.18, -1383.62, 31.26 >> //this garage sucks to use for towing<< -68.9948, -1339.1691, 28.2917 >> //<<-94.19, -1349.2, 35.5>> 
	vGarages[2] = <<-205.6866, -1384.3333, 30.2585>> //<< -206.18, -1383.62, 31.26 >>
	
	vGarages[3] = <<532.4957, -172.2088, 53.6835>> //<< 532.4957, -172.2088, 53.7317 >> 
	vGarages[4] = <<1151.5066, -773.4066, 56.6100>> //<< 1151.5066, -773.4066, 56.5624 >> 
	vGarages[5] = <<808.4329, -822.9456, 25.1821>> //<< 808.4329, -822.9456, 25.1811 >> //<< 805.2443, -812.3316, 25.1810 >>
	//Up North
	vGarages[6] = <<2502.6130, 4080.1411, 37.6307>> //<< 2502.6130, 4080.1411, 37.6493 >>  //<<2479, 4074, 40>>
	vGarages[7]	= <<263.4725, 2601.8423, 43.8197>> //<< 272.6906, 2594.6504, 43.6522 >>   //<<251, 2618, 50>>
	
	vImpound[0] = <<401.6046, -1632.7806, 28.2928>> //<< 397.2509, -1627.4579, 28.2918 >>

ENDPROC

STRUCT GARAGE_DATA
	//Angled area data for large garage area
	VECTOR vCoordinate01
	VECTOR vCoordinate02
	FLOAT fWidth
	//Angled area data for "drop zone"
	VECTOR vDropZoneA
	VECTOR vDropZoneB
	FLOAT fDropZoneW
	//Other Data
	VECTOR vProcessZone
	VECTOR vCenterPosition
ENDSTRUCT


//Todo.  At some point it would be preferable to just have this data in a struct packaged with the garage vector
PROC GET_DATA_FROM_GARAGE_LOCATION(INT iIndexToUse, GARAGE_DATA& myGarageData)
	
	IF IS_VECTOR_ZERO(vGarages[0])
		SCRIPT_ASSERT("Make sure POPULATE_LOCATIONS() is called before GET_DATA_FROM_GARAGE_LOCATION")
	ENDIF
	
	SWITCH iIndexToUse
		// Vapid
		CASE 0
			myGarageData.vCoordinate01 = <<-230.851410,-1181.072266,21.060310>>
			myGarageData.vCoordinate02 = <<-230.465836,-1163.160400,28.996408>>
			myGarageData.fWidth = 22.250000
			myGarageData.vCenterPosition = vGarages[0]
		BREAK
		// Damage Repairs - 555-0142
		CASE 1
			myGarageData.vCoordinate01 = << -204.060287,-1390.023926,30.253416 >>
			myGarageData.vCoordinate02 = << -207.566544, -1382.582275, 33.469368 >>
			myGarageData.fWidth = 17.000000
			myGarageData.vCenterPosition = vGarages[1]
		BREAK
		CASE 2
			myGarageData.vCoordinate01 = << -204.060287,-1390.023926,30.253416 >>
			myGarageData.vCoordinate02 = << -207.566544, -1382.582275, 33.469368 >>
			myGarageData.fWidth = 17.000000
			myGarageData.vCenterPosition = vGarages[2]
		BREAK
		CASE 3
			myGarageData.vCoordinate01 = << 538.747, -177.535, 74.484 >>
			myGarageData.vCoordinate02 = << 528.747, -177.535, 34.484 >>
			myGarageData.fWidth = 28.000
			myGarageData.vCenterPosition = vGarages[3]
		BREAK
		CASE 4
			myGarageData.vCoordinate01 = << 1158.944, -776.686, 77.608 >>
			myGarageData.vCoordinate02 = << 1118.944, -776.686, 37.608 >>
			myGarageData.fWidth = 10.000
			myGarageData.vCenterPosition = vGarages[4]
		BREAK
		// Otto's Auto Parts
		CASE 5
			myGarageData.vCoordinate01 = << 798.455, -821.201, 46.186 >>
			myGarageData.vCoordinate02 = << 813.455, -821.201, 6.186 >>
			myGarageData.fWidth = 20.000
			myGarageData.vCenterPosition = vGarages[5]
		BREAK
		CASE 6
			myGarageData.vCoordinate01 = << 2504.934, 4085.125, 58.636 >>
			myGarageData.vCoordinate02 = << 2500.285, 4075.156, 18.636 >>
			myGarageData.fWidth = 12.000
			myGarageData.vCenterPosition = vGarages[6]
		BREAK
		CASE 7
			myGarageData.vCoordinate01 = <<256.562256,2600.457764,43.330601>>
			myGarageData.vCoordinate02 = <<268.670074,2602.716064,46.749611>>
			myGarageData.fWidth = 5.000000
			myGarageData.vCenterPosition = vGarages[7]
		BREAK
		// Impound
		CASE 8
			myGarageData.vCoordinate01 = <<398.747131,-1650.805908,27.293236>> //+-20
			myGarageData.vCoordinate02 = <<434.131104,-1610.011475,33.342937>>
			myGarageData.fWidth = 40.5
			myGarageData.vProcessZone = <<400.28537, -1632.59705, 28.29278>>
			myGarageData.vCenterPosition = vImpound[0]
			
			myGarageData.vDropZoneA = <<396.834747,-1639.044678,27.292776>> //<<396.946960,-1637.625122,27.292776>> //<<399.4, -1633.148, 49.29>> //+-20
			myGarageData.vDropZoneB = <<408.007294,-1625.608276,33.292774>> //<<406.118439,-1626.684204,31.792776>> //<<397.1, -1635.8, 9.2>>
			myGarageData.fDropZoneW = 10.000000 //8.750000
		BREAK
	ENDSWITCH
	
//	PRINTLN("idx = ", iIndexToUse)
//	PRINTLN("vCoordinate01 = ", myGarageData.vCoordinate01)
//	PRINTLN("vCoordinate02 = ", myGarageData.vCoordinate02)
//	PRINTLN("fWidth = ", myGarageData.fWidth)
	
ENDPROC

PROC POPULATE_ACTORS(INT& count, BOOL maleOnly = FALSE)
	IF maleOnly
		gbPedList[0] = A_M_Y_GENSTREET_02
		gbPedList[1] = A_M_Y_BEACH_03
		gbPedList[2] = G_M_Y_SALVAGOON_01 //gang
		gbPedList[3] = A_M_M_BEVHILLS_02
		count = 4
	ELSE
		gbPedList[0] = A_F_M_BEVHILLS_01
		gbPedList[1] = A_M_Y_GENSTREET_02
		gbPedList[2] = A_F_Y_Hipster_02
		gbPedList[3] = A_M_Y_BEACH_03
		gbPedList[4] = S_F_Y_SWEATSHOP_01 //poor
		gbPedList[5] = G_M_Y_SALVAGOON_01
		count = MAX_NUM_PED_CHOICES
	ENDIF
ENDPROC

PROC POPULATE_ACTORS_HOMELESS(INT& count)
	gbPedList[0] = A_M_M_Tramp_01
	gbPedList[1] = A_M_Y_MethHead_01
	gbPedList[2] = A_M_M_Tramp_01
	gbPedList[3] = A_M_Y_MethHead_01
	count = 4
ENDPROC

PROC POPULATE_ACTORS_SEXY_MEN(INT& count)
	gbPedList[0] = A_M_Y_YOGA_01
	gbPedList[1] = A_M_Y_BEACH_03
	gbPedList[2] = A_M_Y_MUSCLBEAC_01
	count = 3
ENDPROC

PROC POPULATE_CARS()
	gbCarList[0] = TAILGATER
	gbCarList[1] = ASTEROPE
	gbCarList[2] = PRIMO
	gbCarList[3] = PRIMO
	gbCarList[4] = SCHWARZER
	gbCarList[5] = EMPEROR
	gbCarList[6] = PREMIER
	gbCarList[7] = BUFFALO
	gbCarList[8] = INTRUDER
	gbCarList[9] = INTRUDER
ENDPROC

PROC POPULATE_SHIT_CARS()
	gbCarList[0] = buccaneer
	gbCarList[1] = VOODOO2
	gbCarList[2] = MANANA
	gbCarList[3] = ruiner
	gbCarList[4] = ruiner
	gbCarList[5] = VOODOO2
	gbCarList[6] = SURFER2 
	gbCarList[7] = EMPEROR2 //FEROCI changed to buffalo by bobby wright to fix build
	gbCarList[8] = STANIER
	gbCarList[9] = TAILGATER
ENDPROC

//Check an array of model names(nameList) to see if a specified model name(checkName) is in said array.
FUNC BOOL IS_MODEL_NAME_IN_LIST(MODEL_NAMES checkName, MODEL_NAMES& nameList[])
	INT idx
	
	IF checkName = DUMMY_MODEL_FOR_SCRIPT
		SCRIPT_ASSERT("IS_MODEL_NAME_IN_LIST: Passed in a NULL value for checkName")
		RETURN FALSE
	ENDIF
	
	REPEAT COUNT_OF(nameList) idx
		//only bother checking if we're both non NULL
		IF nameList[idx] <> DUMMY_MODEL_FOR_SCRIPT
			IF nameList[idx] = checkName
				DEBUG_MESSAGE("We found a model_name dupe")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_UNUSED_RANDOM_MODEL(MODEL_NAMES& excludeCars[], MODEL_NAMES& globalList[], INT MAX)

	INT count = 0
	DEBUG_MESSAGE("GET_UNUSED_RANDOM_MODEL")
	INT rand = GET_RANDOM_INT_IN_RANGE(0, MAX-1)
	WHILE IS_MODEL_NAME_IN_LIST(globalList[rand], excludeCars)  OR COUNT > 25
		count++
		//This enum isn't in our exclude list so we can use it
		rand = GET_RANDOM_INT_IN_RANGE(0, MAX-1)
	ENDWHILE
	
	RETURN globalList[rand]

	//RETURN DUMMY_MODEL_FOR_SCRIPT
	//TODO add some sort of fail safe exit... although our exclude list should never be big enough to cause this problem
ENDFUNC 


PROC POPULATE_ILLEGAL_NODES(ILLEGAL_NODE& myNodes[])

	//Node 1
	myNodes[0].vCarPos =  << 123.9990, -1081.6178, 28.1919 >>
	myNodes[0].fCarRot = 180.4770 
	myNodes[0].vPedPos = << 97.7377, -1072.4927, 28.2717 >>
	myNodes[0].fPedRot = 253.9961
	myNodes[0].myType = NODE_TYPE_HANDI	
	
	//Node 2
	myNodes[1].vCarPos =  << 123.9990, -1081.6178, 28.1919 >>
	myNodes[1].fCarRot = 180.4770 
	myNodes[1].vPedPos = << 97.7377, -1072.4927, 28.2717 >>
	myNodes[1].fPedRot = 253.9961
	myNodes[1].myType = NODE_TYPE_HANDI	

	//Node 3
	myNodes[2].vCarPos =  <<-301.9741, -898.8075, 30.0813>>
	myNodes[2].fCarRot = 168.6079
	myNodes[2].vPedPos = <<-301.0894, -934.5150, 30.0813>>
	myNodes[2].fPedRot = 66.2119
	myNodes[2].myType = NODE_TYPE_HANDI	
	
	//Node 4
	myNodes[3].vCarPos =  << -359.0859, -965.5469, 30.0701 >>
	myNodes[3].fCarRot = 145.3635
	myNodes[3].myType = NODE_TYPE_ABANDON	
	
	//Node 5
	myNodes[4].vCarPos =  << 4.5819, -1762.4952, 28.2918 >>
	myNodes[4].fCarRot = 51.8577
	myNodes[4].myType = NODE_TYPE_ABANDON	
	
	//Node 6
	myNodes[5].vCarPos =  << 260.5293, -1872.3273, 25.8171 >> //<< 265.0104, -1868.5302, 25.8558 >>
	myNodes[5].fCarRot = 55 //231.4195 //222.8761
	myNodes[5].myType = NODE_TYPE_TRAIN	
	myNodes[5].vPedPos = << 209.1288, -2149.0547, 13.3765 >>	//TRAIN
	myNodes[5].bBackward = TRUE
	myNodes[5].vAngledPos1 = <<331.456, -1789.7, 26.817>>
	myNodes[5].vAngledPos2 = <<190.235, -1957.1, 26.816>>
	myNodes[5].fAngledWidth = 7.2//6.9
	
	//Node 7
	myNodes[6].vCarPos =  << 967.3045, -1873.7893, 30.1425 >> 
	myNodes[6].fCarRot = 41.1599
	myNodes[6].myType = NODE_TYPE_ABANDON
	
	//Node 8
	myNodes[7].vCarPos =  <<-607.5065, -1216.3397, 13.4082>>  
	myNodes[7].fCarRot = 131.5235
	myNodes[7].myType = NODE_TYPE_ABANDON
	
	//Node 9
	myNodes[8].vCarPos =  << 432.3639, -619.0512, 27.5112 >> 
	myNodes[8].fCarRot = 263.5155
	myNodes[8].vPedPos = << 435.9689, -662.4229, 27.8383 >> 
	myNodes[8].fPedRot = 62.5486
	myNodes[8].myType = NODE_TYPE_HANDI
	
	//Node 10
	myNodes[9].vCarPos =  << -136.0945, -785.4554, 31.4112 >>
	myNodes[9].fCarRot = 276.5309
	myNodes[9].myType = NODE_TYPE_ABANDON
	
	//Node 11
	myNodes[10].vCarPos =  << -32.4880, -1354.8672, 28.1676 >>
	myNodes[10].fCarRot = 90.8486
	myNodes[10].myType = NODE_TYPE_ABANDON
	
	//Node 12 - Same as Node 20, too close to the garage.
	myNodes[11].vCarPos =  << -33.8858, -1602.9177, 28.2902 >> 
	myNodes[11].fCarRot = 142.2298
	myNodes[11].myType = NODE_TYPE_TRAIN
	myNodes[11].vPedPos = << 216.8692, -1814.2551, 24.6812 >>	//TRAIN
	myNodes[11].bBackward = FALSE
	myNodes[11].vAngledPos1 = << -111.725, -1538.781, -29.292 >>
	myNodes[11].vAngledPos2 = << 41.484, -1667.339, 29.292 >>
	myNodes[11].fAngledWidth = 7.8

	//Node 13
	myNodes[12].vCarPos =  << 220.6214, -852.1000, 29.1084 >> 
	myNodes[12].fCarRot = 249.5924
	myNodes[12].myType = NODE_TYPE_ABANDON
	
	//Node 14
	myNodes[13].vCarPos =  << 337.0184, -1156.9297, 28.2919 >>
	myNodes[13].fCarRot = 270.3139
	myNodes[13].myType = NODE_TYPE_ABANDON
	
	//Node 15
	myNodes[14].vCarPos =  << 953.1846, -2113.2559, 29.5516 >>
	myNodes[14].fCarRot = 265.5910
	myNodes[14].myType = NODE_TYPE_ABANDON
		
	//Node 16
	myNodes[15].vCarPos =  << -88.9769, -2003.4480, 17.0168 >>
	myNodes[15].fCarRot = 352.6010
	myNodes[15].myType = NODE_TYPE_ABANDON
	
	//Node 17
	myNodes[16].vCarPos =  << 211.0682, -791.7485, 29.9000 >>
	myNodes[16].fCarRot = 68.5508
	myNodes[16].myType = NODE_TYPE_ABANDON
	
	//Node 18
	myNodes[17].vCarPos =  << -327.4179, -1529.1274, 26.5696 >>
	myNodes[17].fCarRot = 179.9431
	myNodes[17].myType = NODE_TYPE_ABANDON
		
	//Node 19
	myNodes[18].vCarPos =  <<-596.5739, -889.5780, 24.4759>> 
	myNodes[18].fCarRot = 269.5022
	myNodes[18].vPedPos = <<-584.4996, -872.2784, 24.8909>> 
	myNodes[18].fPedRot = 83.1267
	myNodes[18].myType = NODE_TYPE_HANDI
	
	//Node 20
	myNodes[19].vCarPos =  << 408.6235, -989.5519, 28.2665 >>
	myNodes[19].fCarRot = 233.0824
	myNodes[19].myType = NODE_TYPE_ABANDON
	
	//Node 21
	myNodes[20].vCarPos =  << -33.8858, -1602.9177, 28.2902 >> 
	myNodes[20].fCarRot = 142.2298
	myNodes[20].myType = NODE_TYPE_TRAIN
	myNodes[20].vPedPos = << 216.8692, -1814.2551, 24.6812 >>	//TRAIN
	myNodes[20].bBackward = FALSE
	myNodes[20].vAngledPos1 = << -111.725, -1538.781, -29.292 >>
	myNodes[20].vAngledPos2 = << 41.484, -1667.339, 29.292 >>
	myNodes[20].fAngledWidth = 7.8
	//faceing left train below
	
	//Node 22
	myNodes[21].vCarPos =  << 432.3639, -619.0512, 27.5112 >> 
	myNodes[21].fCarRot = 263.5155
	myNodes[21].vPedPos = << 435.9689, -662.4229, 27.8383 >> 
	myNodes[21].fPedRot = 62.5486
	myNodes[21].myType = NODE_TYPE_HANDI
	
	//Node 23
	myNodes[22].vCarPos =  <<-310.2984, -686.4995, 32.1219>> 
	myNodes[22].fCarRot = 269.6159
	myNodes[22].vPedPos = << -373.4680, -672.8490, 30.4925 >>
	myNodes[22].fPedRot = 274.2857
	myNodes[22].myType = NODE_TYPE_HANDI
	
	///Node 24 ... sorta dupe.. remove?
	myNodes[23].vCarPos =  << -316.4254, -895.1236, 30.0701 >>
	myNodes[23].fCarRot = 347.1421
	myNodes[23].myType = NODE_TYPE_ABANDON
	
	///Node 25
	myNodes[24].vCarPos =  << 53.5780, -1417.2264, 28.3517 >> 
	myNodes[24].fCarRot = 224.8985
	myNodes[24].myType = NODE_TYPE_ABANDON
	
	///Node 26
	myNodes[25].vCarPos =  << 401.6395, -2054.6584, 20.5750 >>
	myNodes[25].fCarRot = 168.9083
	myNodes[25].myType = NODE_TYPE_ABANDON

	//Node 27
	myNodes[26].vCarPos =  << 146.2919, -2051.0713, 17.3217 >> 
	myNodes[26].fCarRot = 265.1393 
	myNodes[26].myType = NODE_TYPE_TRAIN
	myNodes[26].vPedPos = <<339.5770, -1779.0023, 28.1454>> //<< 403.1925, -1703.4618, 28.1448 >> //TRAIN
	myNodes[26].bBackward = FALSE
	myNodes[26].vAngledPos1 = << 149.063, -2005.992, -18.327 >>
	myNodes[26].vAngledPos2 = << 144.980, -2095.900, 18.327 >>
	myNodes[26].fAngledWidth = 8.5
	//faceing left train below
	
	//Node 28
	myNodes[27].vCarPos =  << 363.1678, -1749.9573, 28.2073 >>  
	myNodes[27].fCarRot = 229.6858
	myNodes[27].myType = NODE_TYPE_TRAIN
	myNodes[27].vPedPos = << 150.5184, -2010.4971, 17.7098 >> //<< 172.6316, -1985.9807, 17.3922 >> //TRAIN
	myNodes[27].bBackward = TRUE
	myNodes[27].vAngledPos1 = << 428.271, -1674.163, -29.211 >>
	myNodes[27].vAngledPos2 = << 299.180, -1826.923, 29.211 >>
	myNodes[27].fAngledWidth = 9.0
	//facing left train above
	
	//Node 29
	myNodes[28].vCarPos =  << 171.3444, -1776.8311, 28.0622 >>    
	myNodes[28].fCarRot = 321.1031
	myNodes[28].myType = NODE_TYPE_TRAIN
	myNodes[28].vPedPos = << -92.7820, -1554.6067, 32.2626 >> //<< -56.6226, -1584.7617, 28.6073 >> //TRAIN
	myNodes[28].bBackward = TRUE
	myNodes[28].vAngledPos1 = << 94.446, -1712.766, -29.071 >>
	myNodes[28].vAngledPos2 = << 248.549, -1840.251, 29.211 >>
	myNodes[28].fAngledWidth = 8.0
	//facing right train above
	
	//Node 30
	myNodes[29].vCarPos =  << 421.2757, -1277.6182, 29.2671 >> 
	myNodes[29].fCarRot = 359.1010 
	myNodes[29].vPedPos = << 455.9831, -1267.6547, 29.0609 >>  
	myNodes[29].fPedRot = 97.7539 
	myNodes[29].myType = NODE_TYPE_HANDI
	
	// Commenting out to Fix Bug # 468992... no longer a handicap spot.
//	//Node 31
//	myNodes[30].vCarPos =  << -335.4176, -1105.7421, 22.0251 >>
//	myNodes[30].fCarRot = 160.3168 
//	myNodes[30].vPedPos = << -281.3432, -1095.3059, 22.7558 >>  
//	myNodes[30].fPedRot = 165.0403  
//	myNodes[30].myType = NODE_TYPE_HANDI
	
	//Node 31
	myNodes[30].vCarPos =  << 211.0559, -1371.6879, 29.5776 >> 
	myNodes[30].fCarRot = 52.9320
	myNodes[30].vPedPos = << 209.6618, -1406.5208, 28.2921 >>   
	myNodes[30].fPedRot = 263.4376  
	myNodes[30].myType = NODE_TYPE_HANDI
	
	//Node 32
	myNodes[31].vCarPos =  << -219.3074, -1491.8456, 30.2593 >>     
	myNodes[31].fCarRot = 322.6262 
	myNodes[31].myType = NODE_TYPE_HANDI
	myNodes[31].vPedPos = << -177.2324, -1506.0100, 31.6696 >>
	
	//Node 33
	myNodes[32].vCarPos =  << 538.7713, -1524.8258, 28.1680 >>     
	myNodes[32].fCarRot = 50.4239
	myNodes[32].myType = NODE_TYPE_TRAIN
	myNodes[32].vPedPos = <<535.2374, -1093.3301, 27.4652>> //<< 534.9999, -1158.6533, 28.2390 >> //TRAIN
	myNodes[32].bBackward = FALSE
	myNodes[32].vAngledPos1 = << 557.229, -1501.367, -29.273 >>
	myNodes[32].vAngledPos2 = << 518.983, -1547.597, 29.273 >>
	myNodes[32].fAngledWidth = 8.200
	//left facing train above. dissapears
	
	//Node 34
	myNodes[33].vCarPos =  << 497.2474, -1199.3268, 28.3046 >>    
	myNodes[33].fCarRot = 212.2787
	myNodes[33].myType = NODE_TYPE_TRAIN
	myNodes[33].vPedPos = << 516.6573, -926.7039, 14.6979 >>//<< 530.8145, -676.5146, 14.4227 >> //TRAIN
	myNodes[33].bBackward = FALSE
	myNodes[33].vAngledPos1 = << 512.849, -1198.747, -29.311 >>
	myNodes[33].vAngledPos2 = << 483.086, -1202.507, 29.311 >>
	myNodes[33].fAngledWidth = 8.0
	//right facing train above
	
	//Node 35
	myNodes[34].vCarPos =  << 576.4442, -1705.2592, 28.0895 >>
	myNodes[34].fCarRot = 43.2791
	myNodes[34].myType = NODE_TYPE_ACCIDENT
	myNodes[34].mnSpecificCar = TAILGATER
	myNodes[34].myDamage = DAMAGE_FRONT_RIGHT
	myNodes[34].extraDataIndex = 0
	myNodes[34].vSpeedNodeLocation = << 585.9758, -1707.7788, 28.2677 >>
	
	//Node 36
	myNodes[35].vCarPos =  << 233.0704, -1138.8818, 28.2302 >>
	myNodes[35].fCarRot = 311.5479
	myNodes[35].myType = NODE_TYPE_ACCIDENT
	myNodes[35].mnSpecificCar = EMPEROR
	myNodes[35].myDamage = DAMAGE_FRONT_RIGHT
	myNodes[35].extraDataIndex = 1
	
	//Node 37
	myNodes[36].vCarPos =  << -100.5493, -1724.9733, 28.3857 >>
	myNodes[36].fCarRot = 81.5938 
	myNodes[36].myType = NODE_TYPE_ACCIDENT
	myNodes[36].mnSpecificCar = INTRUDER
	myNodes[36].myDamage = DAMAGE_FRONT_LEFT
	myNodes[36].extraDataIndex = 2
	myNodes[36].vSpeedNodeLocation = << -92.7179, -1721.0925, 28.3288 >>
	
	// 1st new node - Node 38
	myNodes[37].vCarPos = << -114.5507, -1318.9116, 28.1481 >> 
	myNodes[37].fCarRot = 157.6585
	myNodes[37].myType = NODE_TYPE_ACCIDENT
	myNodes[37].mnSpecificCar = ASTEROPE
	myNodes[37].myDamage = DAMAGE_FRONT_RIGHT
	myNodes[37].extraDataIndex = 3
	myNodes[37].vSpeedNodeLocation = << -111.4177, -1293.5677, 28.2889 >>
	
	// 2nd new node - Node 39
	myNodes[38].vCarPos = << 303.2563, -1699.0979, 28.1861 >> 
	myNodes[38].fCarRot = 213.8354
	myNodes[38].myType = NODE_TYPE_ACCIDENT
	myNodes[38].mnSpecificCar = EMPEROR2
	myNodes[38].myDamage = DAMAGE_FRONT_RIGHT
	myNodes[38].extraDataIndex = 4
	myNodes[38].vSpeedNodeLocation = << 284.1822, -1679.6572, 28.3083 >>
	
	// 3rd new node - Node 40
	myNodes[39].vCarPos = << 784.1456, -2046.7025, 28.1368 >>  
	myNodes[39].fCarRot = 12.6961
	myNodes[39].myType = NODE_TYPE_ACCIDENT
	myNodes[39].mnSpecificCar = BLISTA
	myNodes[39].myDamage = DAMAGE_FRONT_LEFT
	myNodes[39].extraDataIndex = 5
	myNodes[39].vSpeedNodeLocation = << 784.3650, -2070.9988, 28.3414 >>
	
	// 4th new node - Node 41
	myNodes[40].vCarPos = << 29.4785, -980.7521, 28.4051 >> 
	myNodes[40].fCarRot = 221.9553 
	myNodes[40].myType = NODE_TYPE_ACCIDENT
	myNodes[40].mnSpecificCar = EMPEROR
	myNodes[40].myDamage = DAMAGE_FRONT_RIGHT
	myNodes[40].extraDataIndex = 6
	myNodes[40].vSpeedNodeLocation = << 10.3525, -970.0844, 28.4022 >>
	
	// 5th new node - Node 42
	myNodes[41].vCarPos = << 101.5021, -1529.5457, 28.2147 >>  
	myNodes[41].fCarRot = 31.1362  
	myNodes[41].myType = NODE_TYPE_ACCIDENT
	myNodes[41].mnSpecificCar = PREMIER
	myNodes[41].myDamage = DAMAGE_FRONT_LEFT
	myNodes[41].extraDataIndex = 7
	myNodes[41].vSpeedNodeLocation = << 117.6439, -1547.8854, 28.2914 >>
	
	// 6th new node - Node 43
	myNodes[42].vCarPos = << 360.6348, -867.9800, 28.1345 >> 
	myNodes[42].fCarRot = 249.7990 
	myNodes[42].myType = NODE_TYPE_ACCIDENT
	myNodes[42].mnSpecificCar = BUFFALO
	myNodes[42].myDamage = DAMAGE_FRONT_RIGHT
	myNodes[42].extraDataIndex = 8
	myNodes[42].vSpeedNodeLocation = << 324.7404, -864.9786, 28.2923 >>
	
	// 7th new node - Node 44
	myNodes[43].vCarPos = <<261.1898, -2035.0508, 17.2895>>
	myNodes[43].fCarRot = 339.6013 
	myNodes[43].myType = NODE_TYPE_ACCIDENT
	myNodes[43].mnSpecificCar = PREMIER
	myNodes[43].myDamage = DAMAGE_FRONT_LEFT
	myNodes[43].extraDataIndex = 9
	myNodes[43].vSpeedNodeLocation = << 279.2888, -2017.8461, 18.4895 >>
	
	// 8th new node - Node 45
	myNodes[44].vCarPos = << 418.6230, -1867.9032, 25.6724 >>   
	myNodes[44].fCarRot = 103.9975 
	myNodes[44].myType = NODE_TYPE_ACCIDENT
	myNodes[44].mnSpecificCar = INTRUDER
	myNodes[44].myDamage = DAMAGE_FRONT_RIGHT 
	myNodes[44].extraDataIndex = 10
	myNodes[44].vSpeedNodeLocation = << 443.8106, -1847.2933, 26.8106 >>
	
	// 9th new node - Node 46
	myNodes[45].vCarPos = << 507.4709, -1668.5927, 28.6539 >> 
	myNodes[45].fCarRot = 78.9463 
	myNodes[45].myType = NODE_TYPE_ACCIDENT
	myNodes[45].mnSpecificCar = TAILGATER
	myNodes[45].myDamage = DAMAGE_FRONT_LEFT
	myNodes[45].extraDataIndex = 11
	myNodes[45].vSpeedNodeLocation = << 525.0562, -1678.7813, 28.4452 >>
	
	// 10th new node - Node 47
	myNodes[46].vCarPos = << -203.7249, -667.2370, 32.6054 >> 
	myNodes[46].fCarRot = 34.4328  
	myNodes[46].myType = NODE_TYPE_ACCIDENT
	myNodes[46].mnSpecificCar = ASTEROPE 
	myNodes[46].myDamage = DAMAGE_FRONT_RIGHT
	myNodes[46].extraDataIndex = 12
	myNodes[46].vSpeedNodeLocation = << -179.7505, -678.1224, 33.1625 >>
	
	// 11th new node - Node 48
	myNodes[47].vCarPos = << -318.0310, -865.0022, 30.4820 >> 
	myNodes[47].fCarRot = 238.9396  
	myNodes[47].myType = NODE_TYPE_ACCIDENT
	myNodes[47].mnSpecificCar = EMPEROR2 
	myNodes[47].myDamage = DAMAGE_FRONT_RIGHT
	myNodes[47].extraDataIndex = 13
	myNodes[47].vSpeedNodeLocation = << -338.2523, -859.7131, 30.5587 >>
	
	// 12th new node - Node 49
	myNodes[48].vCarPos = << 401.2838, -1480.8701, 28.3058 >>  
	myNodes[48].fCarRot = 281.6565  
	myNodes[48].myType = NODE_TYPE_ACCIDENT
	myNodes[48].mnSpecificCar = BLISTA 
	myNodes[48].myDamage = DAMAGE_HEAD_ON
	myNodes[48].extraDataIndex = 14
	myNodes[48].vSpeedNodeLocation = << 369.6188, -1495.7296, 28.2385 >>
	
	// Countryside
	myNodes[49].vCarPos = <<2912.9065, 4410.6245, 47.9319>> 
	myNodes[49].fCarRot = 51.9751 
	myNodes[49].myType = NODE_TYPE_ACCIDENT
	myNodes[49].mnSpecificCar = EMPEROR2 
	myNodes[49].myDamage = DAMAGE_FRONT_RIGHT
	myNodes[49].extraDataIndex = 15
	myNodes[49].vSpeedNodeLocation = <<2929.2373, 4389.6006, 49.1801>>
	
	// Countryside - near Paint Shop
	myNodes[50].vCarPos = <<1684.6714, 4789.8501, 40.9384>> 
	myNodes[50].fCarRot = 337.6293 
	myNodes[50].myType = NODE_TYPE_ACCIDENT
	myNodes[50].mnSpecificCar = TAILGATER 
	myNodes[50].myDamage = DAMAGE_FRONT_LEFT
	myNodes[50].extraDataIndex = 16
	myNodes[50].vSpeedNodeLocation = <<1677.4445, 4772.6963, 40.9936>>
	
	//Node 51
	myNodes[51].vCarPos = <<217.3860, -2545.0613, 5.1932>>  
	myNodes[51].fCarRot = 95.5124 
	myNodes[51].myType = NODE_TYPE_TRAIN
	myNodes[51].vPedPos = <<150.7310, -2066.6626, 17.2342>>//<< 530.8145, -676.5146, 14.4227 >> //TRAIN
	myNodes[51].bBackward = FALSE
	myNodes[51].vAngledPos1 = <<217.882950,-2593.547852,4.174078>> 
	myNodes[51].vAngledPos2 = <<217.181595,-2499.778564,12.436670>>
	myNodes[51].fAngledWidth = 8.0
	//right facing train above
	
	// New Tonya 5
	myNodes[52].vCarPos = <<-692.0270, -847.6826, 22.6477>>  
	myNodes[52].fCarRot = 245.0550
	myNodes[52].myType = NODE_TYPE_ACCIDENT
	myNodes[52].mnSpecificCar = TAILGATER 
	myNodes[52].myDamage = DAMAGE_FRONT_RIGHT
	myNodes[52].extraDataIndex = 15
	myNodes[52].vSpeedNodeLocation = <<-723.7541, -844.7679, 21.9551>>
	
ENDPROC

PROC POPULATE_EXTRA_DATA(ACCIDENT_DATA& extraData[])
	//Extra data for illegal node 35
	extraData[0].vUtilityPos[0] = << 568.8983, -1698.7230, 28.2631 >> //226
	extraData[0].vUtilityPos[1] = << 575.1474, -1697.5486, 28.2631 >> //180.4
	extraData[0].vUtilityPos[2] = << 584.8786, -1703.8748, 28.2631 >> //98.62
	extraData[0].numBystanders = 3
	extraData[0].vTrafficPos = << 565.9069, -1702.8134, 28.2227 >>
	extraData[0].fTrafficOrient = 58.4094 
	extraData[0].vMin = << 564.5385, -1689.7069, 38.2854 >>
	extraData[0].vMax = << 605.4141, -1715.2214, 18.0923 >>

	
	//Extra data for illegal node 36
	extraData[1].vUtilityPos[0] = << 240.0033, -1141.4331, 28.3033 >>
	extraData[1].vUtilityPos[1] = << 240.5303, -1137.7504, 28.3091 >>
	extraData[1].vUtilityPos[2] = << 229.8743, -1145.7627, 28.3013 >>
	extraData[1].numBystanders = 3
	extraData[1].vTrafficPos = << 239.0778, -1134.1409, 28.2367 >>
	extraData[1].fTrafficOrient = 266.7983 
	extraData[1].vMin = << 242.7403, -1124.7528, 38.3223 >>
	extraData[1].vMax = << 225.7671, -1152.4150, 18.2047 >>
	extraData[1].vBloodSplatter[0] = << 232.3824, -1137.8553, 28.1944 >>
	extraData[1].vBloodSplatter[1] = << 230.8579, -1136.2522, 28.0348 >>
	
	//Extra data for illegal node ?? - Node 37
	extraData[2].vUtilityPos[0] = << -95.1473, -1716.6565, 28.4491 >>
	extraData[2].vUtilityPos[1] = << -93.1677, -1717.8398, 28.4239 >>
	//extraData[2].vUtilityPos[2] = << -91.7477, -1719.5299, 28.3958 >>
	extraData[2].numBystanders = 2
	extraData[2].vTrafficPos = << -102.9092, -1729.6680, 28.7511 >>
	extraData[2].fTrafficOrient = 105.1153
	extraData[2].vBloodSplatter[0] = << -99.8953, -1726.7020, 28.4622 >>
	extraData[2].vBloodSplatter[1] = << -101.1025, -1728.6892, 28.6286 >>
	
	//1st new extra data node - Node 38
	extraData[3].vUtilityPos[0] = << -117.7085, -1321.0688, 28.2857 >>
	extraData[3].vUtilityPos[1] = << -116.6308, -1325.3618, 28.3192 >>
	extraData[3].numBystanders = 2
	extraData[3].vTrafficPos = << -112.3024, -1329.2186, 28.2685 >> 
	extraData[3].fTrafficOrient = 179.9845
	extraData[3].vBloodSplatter[0] = << -113.1683, -1319.4591, 28.2125 >>
	extraData[3].vBloodSplatter[1] = << -112.2886, -1322.4221, 28.2633 >>
	
	//2nd new extra data node - Node 39
	extraData[4].vUtilityPos[0] = << 308.2367, -1706.0255, 28.3827 >>
	extraData[4].vUtilityPos[1] = << 298.1392, -1700.7681, 28.3234 >>
	extraData[4].numBystanders = 2
	extraData[4].vTrafficPos = << 314.1078, -1704.3053, 28.3111 >>  
	extraData[4].fTrafficOrient = 227.3160
	extraData[4].vBloodSplatter[0] = << 304.5671, -1697.5216, 28.2629 >>
	extraData[4].vBloodSplatter[1] = << 308.3995, -1699.6951, 28.3051 >>
	
	//3rd new extra data node - Node 40
	extraData[5].vUtilityPos[0] = << 776.3322, -2046.0779, 28.2818 >>
	extraData[5].vUtilityPos[1] = << 776.0253, -2048.3916, 28.2653 >>
	extraData[5].numBystanders = 2
	extraData[5].vTrafficPos = << 787.8897, -2034.5416, 28.2393 >>   
	extraData[5].fTrafficOrient = 348.1702
	extraData[5].vBloodSplatter[0] = << 782.5989, -2047.4797, 28.1590 >>
	extraData[5].vBloodSplatter[1] = << 783.3265, -2051.1829, 28.1589 >>
	
	//4th new extra data node - Node 41
	extraData[6].vUtilityPos[0] = << 28.2787, -986.7457, 28.5094 >>
	extraData[6].vUtilityPos[1] = << 35.6153, -988.3468, 28.4971 >>
	extraData[6].vUtilityPos[2] = << 38.5530, -985.1667, 28.5576 >>
	extraData[6].numBystanders = 3
	extraData[6].vTrafficPos = << 43.6799, -982.0863, 28.4103 >>    
	extraData[6].fTrafficOrient = 251.1427
	extraData[6].vBloodSplatter[0] = << 30.5244, -980.0237, 28.4037 >>
	extraData[6].vBloodSplatter[1] = << 34.0509, -979.3044, 28.4079 >>
	
	//5th new extra data node - Node 42
	extraData[7].vUtilityPos[0] = << 99.1579, -1521.9631, 28.3247 >>
	extraData[7].vUtilityPos[1] = << 105.6990, -1527.3311, 28.3186 >>
	extraData[7].numBystanders = 2
	extraData[7].vTrafficPos = << 95.4077, -1529.4733, 28.3325 >>     
	extraData[7].fTrafficOrient = 51.3731
	extraData[7].vBloodSplatter[0] = << 100.3829, -1529.7828, 28.2238 >>
	extraData[7].vBloodSplatter[1] = << 97.5322, -1531.5796, 28.3374 >>
	
	//6th new extra data node - Node 43
	extraData[8].vUtilityPos[0] = << 370.2508, -871.9623, 28.2916 >>
	extraData[8].vUtilityPos[1] = << 364.3770, -872.4929, 28.2916 >>
	extraData[8].numBystanders = 2
	extraData[8].vTrafficPos = << 370.0709, -865.2366, 28.2507 >>      
	extraData[8].fTrafficOrient = 271.3831
	extraData[8].vBloodSplatter[0] = << 363.9474, -865.3307, 28.2621 >>
	extraData[8].vBloodSplatter[1] = << 361.2415, -866.9158, 28.1908 >>
	
	//7th new extra data node - Node 44
	extraData[9].vUtilityPos[0] = << 258.1324, -2035.0984, 17.2524 >>
	extraData[9].vUtilityPos[1] = << 263.1343, -2026.3652, 17.7171 >>
	extraData[9].numBystanders = 2
	extraData[9].vTrafficPos = << 260.0068, -2041.8058, 16.9454 >>       
	extraData[9].fTrafficOrient = 140.9065
	extraData[9].vBloodSplatter[0] = << 264.5263, -2036.1978, 17.2682 >>
	extraData[9].vBloodSplatter[1] = << 264.3815, -2032.3785, 17.3290 >>
	
	//8th new extra data node - Node 45
	extraData[10].vUtilityPos[0] = << 411.4076, -1869.4333, 25.5691 >>
	extraData[10].vUtilityPos[1] = << 416.1884, -1862.5890, 26.0458 >>
	extraData[10].numBystanders = 2
	extraData[10].vTrafficPos = << 412.9648, -1876.9008, 25.3104 >>        
	extraData[10].fTrafficOrient = 135.5291
	extraData[10].vBloodSplatter[0] = << 418.2990, -1872.5396, 25.6552 >>
	extraData[10].vBloodSplatter[1] = << 419.0923, -1869.0394, 25.6907 >>
	
	//9th new extra data node - Node 46
	extraData[11].vUtilityPos[0] = << 500.9577, -1664.8811, 28.7134 >>
	extraData[11].vUtilityPos[1] = << 501.1261, -1668.8090, 28.7152 >>
	extraData[11].numBystanders = 2
	extraData[11].vTrafficPos = << 503.8388, -1661.2373, 28.4841 >>         
	extraData[11].fTrafficOrient = 50.8842
	extraData[11].vBloodSplatter[0] = << 507.0525, -1670.2535, 28.7152 >>
	extraData[11].vBloodSplatter[1] = << 507.7841, -1665.1162, 28.4902 >>
	
	//10th new extra data node - Node 47
	extraData[12].vUtilityPos[0] = << -208.9247, -660.1125, 32.6290 >>
	extraData[12].vUtilityPos[1] = << -204.0615, -660.8978, 32.7011 >>
	extraData[12].vUtilityPos[2] = << -198.1905, -664.2867, 32.8159 >>
	extraData[12].numBystanders = 3
	extraData[12].vTrafficPos = << -213.0429, -666.7334, 32.6039 >>          
	extraData[12].fTrafficOrient = 70.1131
	extraData[12].vBloodSplatter[0] = << -210.6179, -667.8796, 32.6494 >>
	extraData[12].vBloodSplatter[1] = << -204.9018, -667.8836, 32.6425 >>
	
	//11th new extra data node - Node 48
	extraData[13].vUtilityPos[0] = << -310.1606, -869.7873, 30.6891 >>
	extraData[13].vUtilityPos[1] = << -315.0338, -869.8134, 30.6555 >>
	extraData[13].vUtilityPos[2] = << -319.5981, -868.6276, 30.6449 >>
	extraData[13].numBystanders = 3
	extraData[13].vTrafficPos = << -309.9870, -864.1183, 30.6228 >>           
	extraData[13].fTrafficOrient = 261.3995
	extraData[13].vBloodSplatter[0] = << -316.0121, -862.7733, 30.6267 >>
	extraData[13].vBloodSplatter[1] = << -317.5260, -863.8655, 30.5453 >>
	
	//12th new extra data node - Node 49
	extraData[14].vUtilityPos[0] = << 407.2907, -1479.6372, 28.2895 >>
	extraData[14].vUtilityPos[1] = << 404.6483, -1483.5702, 28.2895 >>
	extraData[14].numBystanders = 2
	extraData[14].vTrafficPos = << 403.5326, -1475.1968, 28.2951 >>          
	extraData[14].fTrafficOrient = 301.9695
	extraData[14].vBloodSplatter[0] = << 399.2320, -1477.7832, 28.2928 >>
	extraData[14].vBloodSplatter[1] = << 401.2551, -1479.7366, 28.1369 >>
	
	extraData[15].vUtilityPos[0] = <<-686.6597, -851.2509, 22.9054>>
	extraData[15].vUtilityPos[1] = <<-696.9954, -852.1266, 22.6746>>
	extraData[15].numBystanders = 2
	extraData[15].vTrafficPos = <<-674.3848, -844.9825, 23.1517>>         
	extraData[15].fTrafficOrient = 269.1391
	extraData[15].vBloodSplatter[0] = <<-692.3451, -845.8392, 22.7217>>
	extraData[15].vBloodSplatter[1] = <<-692.3451, -845.8392, 22.7217>>
	
ENDPROC

//
//FUNC BOOL UPDATE_BROKEN_VEHICLE_PED_TRADITIONAL(BROKEN_STATE& myState, VEHICLE_INDEX vehicle, PED_INDEX myPed)
//	SWITCH myState
//		CASE BROKEN_STATE_VISIBLE
//			DEBUG_MESSAGE("BROKEN_STATE_VISIBLE")
//			IF NOT IS_ENTITY_DEAD(vehicle)
//				IF (IS_ENTITY_VISIBLE(vehicle) AND GET_PLAYER_DISTANCE_FROM_ENTITY(vehicle) < 55) 
//					iFrameCount++
//				ENDIF
//			ELSE
//				SCRIPT_ASSERT("vehicle is dead")
//			ENDIF
//			IF iFrameCount > 70 OR GET_PLAYER_DISTANCE_FROM_ENTITY(vehicle) < 5 
//				//We've been onscreen for a bit... continue
//				iFrameCount = 0
//				PRINT_NOW("TOWT_OBJ_02", DEFAULT_GOD_TEXT_TIME, 1)
//				myState = BROKEN_STATE_PED_TALK
//			ENDIF
//		BREAK
//		CASE BROKEN_STATE_PED_TALK
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() //AND TIMERA() > 5000
//				PRINT_NOW("TOWT_OBJ_02", DEFAULT_GOD_TEXT_TIME, 1)
//				myState = BROKEN_STATE_PED_ENTER
//			ENDIF
//		BREAK
//		CASE BROKEN_STATE_PED_ENTER
//			DEBUG_MESSAGE("BROKEN_STATE_PED_ENTER")
//			CLEAR_PED_TASKS(myPed)
//			TASK_ENTER_VEHICLE(myPed, myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)
//			myState = BROKEN_STATE_HOOKING
//		BREAK
//		CASE BROKEN_STATE_HOOKING
//			IF IS_PED_IN_VEHICLE(myPed, myVehicle)
//				PRINT_NOW("TOWT_OBJ_03", DEFAULT_GOD_TEXT_TIME, 1)
//				RETURN TRUE
//			ENDIF
//		BREAK
//
//	ENDSWITCH
//	
//	RETURN FALSE
//ENDFUNC


////driver side..
//ATTACH_CAM_TO_ENTITY(cam, entity, <<-0.3825, 0.0116, 0.5912>>)
//POINT_CAM_AT_ENTITY(cam, entity, <<2.5902, 0.2597, 0.2724>>)
//SET_CAM_FOV(cam, 50.0063)
//Camera world coords: <<265.5549, -1868.0100, 27.0575>>  Camera rotation: <<-6.1645, 0.0000, 137.6511>>
//Entity world coords: <<265.0097, -1868.5299, 26.4353>>  Entity rotation: <<0.0195, 0.0662, -137.1235>>
//
////camera in passenger side..
//ATTACH_CAM_TO_ENTITY(cam, entity, <<0.7491, -0.0798, 0.5919>>)
//POINT_CAM_AT_ENTITY(cam, entity, <<-2.2167, 0.2801, 0.3180>>)
//SET_CAM_FOV(cam, 50.0000)
//Camera world coords: <<146.0932, -2051.4785, 18.3450>>  Camera rotation: <<-5.1670, 0.0000, -11.8426>>
//Entity world coords: <<146.2862, -2051.0710, 17.8125>>  Entity rotation: <<0.6399, -0.0044, -94.8653>>

//PROC POPULATE_HIGHWAY_NODES(TOW_NODE& myNodes[])
//
//	//Node 1
//	myNodes[0].vCarPos[0] = << 1280.4436, -2134.1248, 46.2833 >> //<< 1296.3278, -2155.3015, 47.2431 >>
//	myNodes[0].fCarRot[0] = 67.0593 //358.7533
//	
//	myNodes[0].vCarPos[1] =  << 1280.4436, -2134.1248, 46.2833 >>
//	myNodes[0].fCarRot[1] = 67.0593
//	
//	myNodes[0].vDropPos[0] = << 1308.5258, -2045.5276, 44.2904 >>
//	
//	myNodes[0].blockCarName = TRASH
//	myNodes[0].vblockedCarPos = << 1289.7864, -2161.2979, 47.5893 >> 
//	myNodes[0].fblockedHeading = 289.1697 
//	
////	//left lane 1st
////	<< 1291.2844, -2167.3704, 47.8431 >>   heading: 18.6986
////	
////	//right lane 1st
////	<< 1296.6509, -2168.1514, 47.9003 >>   heading: 15.8169 
////	
////	//left lane 2nd
////	<< 1295.3258, -2178.7913, 48.3701 >>   heading: 19.1429 
////	
////	//right lane 2nd
////	<< 1300.5004, -2178.8596, 48.4034 >>   heading: 17.9233 
//
//
//	//Node 2 //dupe of 2 but other side of highway
//	myNodes[1].vCarPos[0] = << 942.4255, -751.2482, 38.7148 >>
//	myNodes[1].fCarRot[0] = 15.9374
//	
//	myNodes[1].vCarPos[1] =  << 926.7599, -742.9189, 39.4909 >> 
//	myNodes[1].fCarRot[1] = 88.1585 
//	
//	myNodes[1].vDropPos[0] = << 350.4702, -434.6814, 43.5100 >>
//	
//	myNodes[1].blockCarName = TANKERCAR
//	myNodes[1].vblockedCarPos = << 953.5884, -774.1808, 37.1268 >>  
//	myNodes[1].fblockedHeading = 308.3193 
//	
//	
//	//left lane 1ane first
//	//<< 949.4032, -780.7028, 36.8848 >>   heading: 40.9406 
//	//next lane first
//	//<< 952.4431, -775.7198, 37.0851 >>   heading: 37.4805 
//	//next lane first
//	//<< 952.5361, -766.6473, 37.5795 >>   heading: 36.7324
//	//far right lane first
//	//<< 960.3724, -765.1198, 37.3139 >>  heading: 41.0332
//	
//	
//	//Node 3
//	myNodes[2].vCarPos[0] = << -110.7672, -529.5645, 29.4499 >>
//	myNodes[2].fCarRot[0] = 295.3433
//	
//	myNodes[2].vCarPos[1] =  << -87.3009, -523.0497, 30.2889 >>
//	myNodes[2].fCarRot[1] = 262.6707 
//	
//	myNodes[2].vDropPos[0] = << 249.5887, -570.7577, 42.2694 >>
//	
//	myNodes[2].blockCarName = TANKERCAR
//	myNodes[2].vblockedCarPos = << -118.2498, -525.2168, 29.1709 >> 
//	myNodes[2].fblockedHeading = 180.8536
//	
////	//left lane first
////	Player Position = << -134.4752, -517.0640, 28.4916 >> heading: 271.4897
////	//next lane first
////	Player Position = << -138.3778, -523.5275, 28.4263 >>   heading: 271.3339
////	//next lane first
////	Player Position = << -139.9547, -528.8607, 28.3731 >>   heading: 270.5410
////	//far right lane first
////	Player Position = << -142.0415, -534.3041, 28.2246 >>   heading: 270.6083
//	
//	//Node 4
//	myNodes[3].vCarPos[0] = << -427.1406, -1547.3049, 37.1688 >>
//	myNodes[3].fCarRot[0] = 227.5103
//	
//	myNodes[3].vCarPos[1] =  << -431.7332, -1542.1105, 37.1715 >>
//	myNodes[3].fCarRot[1] = 124.9524 
//	
//	myNodes[3].vDropPos[0] = << -737.6593, -1732.7079, 28.1844 >>
//	
//	myNodes[3].blockCarName = TRASH
//	myNodes[3].vblockedCarPos = << -426.1999, -1536.5449, 36.7925 >>
//	myNodes[3].fblockedHeading = 73.1155
//	//left lane first
//	//Player Position = << -419.6707, -1523.5425, 36.4629 >>   heading: 138.3820 
//	//far right lane first
//	//Player Position = << -424.1630, -1517.3378, 36.6347 >>   heading: 172.5482
//	
//	//Node 5
//	myNodes[4].vCarPos[0] = << -387.1980, -2355.5393, 62.3907 >>
//	myNodes[4].fCarRot[0] = 251.8665
//	
//	myNodes[4].vCarPos[1] =  << -383.2878, -2351.5869, 62.3818 >>
//	myNodes[4].fCarRot[1] = 224.6124 
//	
//	myNodes[4].vDropPos[0] = << 242.5529, -2704.1064, 17.3473 >> 
//	
//	myNodes[4].blockCarName = TRASH
//	myNodes[4].vblockedCarPos = << -397.0799, -2345.6577, 62.5055 >>
//	myNodes[4].fblockedHeading = 332.1026
//	//left lane first
//	//Player Position = << -406.0495, -2335.1921, 62.5597 >>   heading: 215.5446 
//	//far right lane first
//	//Player Position = << -414.2694, -2337.3196, 62.5704 >>   heading: 232.9479 
//	
//	//Node 6
//	myNodes[5].vCarPos[0] = << 716.9126, -2650.2014, 50.1640 >>
//	myNodes[5].fCarRot[0] = 247.3334
//	
//	myNodes[5].vCarPos[1] =  << 720.5433, -2642.8118, 50.3305 >>
//	myNodes[5].fCarRot[1] = 309.9200
//	
//	myNodes[5].vDropPos[0] = << 1108.5540, -2585.3984, 31.2405 >> 
//	
//	myNodes[5].blockCarName = TRASH
//	myNodes[5].vblockedCarPos = << 708.8206, -2648.7759, 49.8587 >>
//	myNodes[5].fblockedHeading = 7.8133
//	
////	//Node 6
////	Player Position = << 716.9126, -2650.2014, 50.1640 >>   heading: 247.3334 
////	Player Position = << 720.5433, -2642.8118, 50.3305 >>   heading: 309.9200
////	
////	//Blocker
////	Player Position = << 708.8206, -2648.7759, 49.8587 >>   heading: 7.8133
////	
////	
////	//left lane
////	Player Position = << 690.9175, -2648.2859, 49.0850 >>   heading: 282.4565
////	//Far right lane
////	Player Position = << 692.0345, -2653.9539, 49.0903 >>   heading: 278.7169
////	//Drop off
////	Player Position = << 1122.4149, -2584.2378, 30.9854 >>   heading: 280.1960
//
//	
//ENDPROC


