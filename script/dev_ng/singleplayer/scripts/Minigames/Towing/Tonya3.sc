
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Threat_Public.sch"
USING "RC_towing.sch"
USING "load_queue_public.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Tonya3.sc
//		AUTHOR			:	
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs sRCLauncherDataLocal

LoadQueueLarge sLoadQueue

ENUM eRC_MainState
	MS_INIT,
	MS_MEET_TONYA,
	MS_GO_TO_TRUCK,
	MS_DO_MISSION,
	MS_FAILED,
	MS_PASSED
ENDENUM

ENUM eRC_SubState
	SS_SETUP = 0,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

ENUM eRC_FAIL_STATE
	SS_FAIL_SETUP = 0,
	SS_FAIL_UPDATE,
	SS_FAIL_CLEANUP
ENDENUM

// Mission state
eRC_MainState 			  	m_eState = MS_INIT
eRC_SubState  			  	m_eSubState = SS_SETUP
eRC_FAIL_STATE 				m_FailState = SS_FAIL_SETUP
STRING					  	sFailReason = "DEFAULT"

TOWING_LAUNCH_DATA 			launchArgs

// Mission
VEHICLE_INDEX			  	vehTruck
VEHICLE_INDEX 				viPlayerLastVehicle

VECTOR					  	vTruckPos = <<401.6370, -1633.3003, 28.2928>> 
FLOAT					  	fTruckHeading = 231.5304

VECTOR						vShitSkipSpawnPosition = <<-215.5832, -1392.7834, 30.2617>>
FLOAT 						fShitSkipSpawnHeading = 108.4129

BOOL						bStartedReplay = FALSE
BOOL						bRemovedNodes = FALSE

INT 						iStageToUse
INT							iTrainTrackSpeedNode01, iTrainTrackSpeedNode02

//REPLAY VARIABLES
VECTOR vPlayerFirstCheckpointPosition = <<-10.7897, -1471.2037, 29.5520>>
FLOAT fPlayerFirstCheckpointHeading = 276.1668

// -----------------------------------
// DEBUG MODE
// -----------------------------------
#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

#IF IS_DEBUG_BUILD

	BOOL bDoingPSkip = FALSE
//	BOOL bDoingJSkip = FALSE
	
	MissionStageMenuTextStruct sSkipMenu[2]
	INT iDebugJumpStage = 0              
#ENDIF

PROC SETUP_DEBUG()
	#IF IS_DEBUG_BUILD
		sSkipMenu[0].sTxtLabel = "GO TO TRUCK"
		sSkipMenu[1].sTxtLabel = "TOWING"
	#ENDIF
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC Script_Cleanup()

	// Clear the mission title
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()

	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required")
	ENDIF

	// Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal, TRUE)
	
	// Release tow truck
	SAFE_RELEASE_VEHICLE(vehTruck)

	// Remove blips
	IF DOES_BLIP_EXIST(myVehicleBlip)
		REMOVE_BLIP(myVehicleBlip)
	ENDIF

	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<132.130447,-2597.911621,0.004743>>, <<340.572571,-2511.292969,11.788495>>, 70.000000)
	REMOVE_ROAD_NODE_SPEED_ZONE(iTrainTrackSpeedNode01)
	REMOVE_ROAD_NODE_SPEED_ZONE(iTrainTrackSpeedNode02)
	CPRINTLN(DEBUG_MISSION, "REMOVING SPEED NODE & SETTING ROADS BACK TO ORIGINAL")
		
	REMOVE_SCENARIO_BLOCKING_AREA(sbi01)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi02)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi03)
	
	MISSION_CLEANUP(sLoadQueue)

	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------
PROC Script_Passed()
	
	// The coords here are for the towing impound
	Random_Character_Passed(CP_OJ_TOW3)
	Script_Cleanup()
ENDPROC

PROC DO_FADE_IN_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_IN()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		WHILE NOT IS_SCREEN_FADED_IN()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC DO_FADE_OUT_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC HANDLE_FIRST_RETRY_SETUP()
	HANDLE_REPLAY_VEHICLE(vVehicleStartPosition, fVehicleStartHeading)
			
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) 
		IF NOT IS_REPLAY_BEING_SET_UP()
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerFirstCheckpointPosition)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerFirstCheckpointHeading)
			CPRINTLN(DEBUG_MISSION, "SETTING PLAYER'S POSITION AND HEADING")
		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	ENDIF

ENDPROC

PROC HANDLE_SECOND_RETRY_SETUP()

	HANDLE_REPLAY_VEHICLE(<<405.7806, -1652.3694, 28.2928>>, 139.8366)

	REQUEST_MODEL(TOWTRUCK)
	
	WHILE NOT HAS_MODEL_LOADED(TOWTRUCK)
		WAIT(0)
		CPRINTLN(DEBUG_MISSION, "WAITING ON TOW TRUCK MODEL TO LOAD")
	ENDWHILE
	
	IF NOT DOES_ENTITY_EXIST(myVehicle)
		myVehicle = CREATE_VEHICLE(TOWTRUCK, vTruckPos, fTruckHeading)
		SET_VEHICLE_ON_GROUND_PROPERLY(myVehicle)
		SET_VEHICLE_COLOUR_COMBINATION(myVehicle, 1)
		CPRINTLN(DEBUG_MISSION, "SETTING VEHICLE COLOR COMBINATION = 1")
	ENDIF
ENDPROC

PROC GO_TO_CHECKPOINT_1()
	IF IS_SCREEN_FADED_OUT()
	
		#IF IS_DEBUG_BUILD
			IF bDoingPSkip //OR bDoingJSkip
				
				CPRINTLN(DEBUG_MISSION, "DEBUG: GO_TO_CHECKPOINT_1")
				CLEAR_PRINTS()
				CLEAR_HELP()
				KILL_ANY_CONVERSATION()
				
				IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
					CLEAR_PED_TASKS_IMMEDIATELY(pedTonya)
					DELETE_PED(pedTonya)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - pedTonya")
				ENDIF
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
				IF DOES_BLIP_EXIST(myVehicleBlip)
					REMOVE_BLIP(myVehicleBlip)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - myVehicleBlip")
				ENDIF
				IF DOES_ENTITY_EXIST(vehTruck)
					DELETE_VEHICLE(vehTruck)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - vehTruck")
				ENDIF
				IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
					REMOVE_BLIP(brokenVehicles[0].blipIndex)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - brokenVehicles[0].blipIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
					DELETE_VEHICLE(brokenVehicles[0].vehicleIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - brokenVehicles[0].vehicleIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
					DELETE_PED(brokenVehicles[0].pedIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndexPass)
					DELETE_PED(brokenVehicles[0].pedIndexPass)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndexPass")
				ENDIF
				
				curBrokenCarCount = 0
				curCarNameIdx = 0
				bSpawnPed = FALSE
				launchArgs.bInit = TRUE
				curTutState = TOW_TUTORIAL_VISIBLE_DIA
				curStage = MISSION_STATE_INTRO
				
				bDoingPSkip = FALSE
			ENDIF
		#ENDIF
		
		HANDLE_FIRST_RETRY_SETUP()
		
		CLEAR_AREA(vPlayerFirstCheckpointPosition, 8.0, TRUE)
		
		END_REPLAY_SETUP()
		
		DO_SCREEN_FADE_IN(500)
		
		m_eState = MS_GO_TO_TRUCK
		CPRINTLN(DEBUG_MISSION, "REPLAY STAGE 1: GOING TO STATE - MS_GO_TO_TRUCK")
	ENDIF
ENDPROC

PROC GO_TO_CHECKPOINT_2()
	IF IS_SCREEN_FADED_OUT()
	
		#IF IS_DEBUG_BUILD
			IF bDoingPSkip //OR bDoingJSkip
				
				CPRINTLN(DEBUG_MISSION, "DEBUG: GO_TO_CHECKPOINT_2")
				CLEAR_PRINTS()
				CLEAR_HELP()
				KILL_ANY_CONVERSATION()
				
				IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
					CLEAR_PED_TASKS_IMMEDIATELY(pedTonya)
					DELETE_PED(pedTonya)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - pedTonya")
				ENDIF
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
				IF DOES_BLIP_EXIST(myVehicleBlip)
					REMOVE_BLIP(myVehicleBlip)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - myVehicleBlip")
				ENDIF
				IF DOES_ENTITY_EXIST(vehTruck)
					DELETE_VEHICLE(vehTruck)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - vehTruck")
				ENDIF
				IF DOES_ENTITY_EXIST(myVehicle)
					DELETE_VEHICLE(myVehicle)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - myVehicle")
				ENDIF
				IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
					REMOVE_BLIP(brokenVehicles[0].blipIndex)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - brokenVehicles[0].blipIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
					DELETE_VEHICLE(brokenVehicles[0].vehicleIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - brokenVehicles[0].vehicleIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
					DELETE_PED(brokenVehicles[0].pedIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndexPass)
					DELETE_PED(brokenVehicles[0].pedIndexPass)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndexPass")
				ENDIF
				
				bSuppressBlips = FALSE
				bTaskedBrokenDownPed = FALSE
				
				myVehicle = CREATE_VEHICLE(TOWTRUCK, vTruckPos, fTruckHeading)
				SET_VEHICLE_ON_GROUND_PROPERLY(myVehicle)
				
				curBrokenCarCount = 0
				curCarNameIdx = 0
				iPushStages = 0
				bSpawnPed = FALSE
				curTutState = TOW_TUTORIAL_VISIBLE_DIA
				trainState = TRAIN_STATE_PRINT
				trainPedState = TRAIN_STATE_PED_INIT
				brokenState = BROKEN_STATE_VISIBLE
				curStage = MISSION_STATE_INTRO
				launchArgs.bInit = TRUE
				
				CPRINTLN(DEBUG_MISSION, "CALLING - HANLDE_PLACING_PLAYER_AND_TONYA_INTO_TOWTRUCK_AND_MOVING")
				HANLDE_PLACING_PLAYER_AND_TONYA_INTO_TOWTRUCK_AND_MOVING()
				
				bDoingPSkip = FALSE
			ENDIF
		#ENDIF
			
		HANDLE_SECOND_RETRY_SETUP()
			
		CLEAR_AREA(vPlayerFirstCheckpointPosition, 8.0, TRUE)
		
		END_REPLAY_SETUP(myVehicle)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(-138.0317)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-0.1190)
		CPRINTLN(DEBUG_MISSION, "SETTING GAMEPLAY CAM RELATIVE HEADING = -138.0317")
		CPRINTLN(DEBUG_MISSION, "SETTING GAMEPLAY CAM RELATIVE PITCH = -0.1190")
		
		DO_SCREEN_FADE_IN(500)
		
		m_eState = MS_DO_MISSION
		CPRINTLN(DEBUG_MISSION, "REPLAY STAGE 2: GOING TO STATE - MS_DO_MISSION")
	ELSE
		CPRINTLN(DEBUG_MISSION, "SCREEN IS NOT FADED OUT")
	ENDIF
ENDPROC

PROC GO_TO_CHECKPOINT_3()
	IF IS_SCREEN_FADED_OUT()
	
		STOP_SCRIPTED_CONVERSATION(FALSE)
		CLEAR_PRINTS()
		CLEAR_HELP()
	
		#IF IS_DEBUG_BUILD
			IF bDoingPSkip //OR bDoingJSkip
				
				CPRINTLN(DEBUG_MISSION, "DEBUG: GO_TO_CHECKPOINT_2")
				
				IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
					CLEAR_PED_TASKS_IMMEDIATELY(pedTonya)
					DELETE_PED(pedTonya)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - pedTonya")
				ENDIF
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
				IF DOES_BLIP_EXIST(myVehicleBlip)
					REMOVE_BLIP(myVehicleBlip)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - myVehicleBlip")
				ENDIF
				IF DOES_ENTITY_EXIST(vehTruck)
					DELETE_VEHICLE(vehTruck)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - vehTruck")
				ENDIF
				IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
					REMOVE_BLIP(brokenVehicles[0].blipIndex)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - brokenVehicles[0].blipIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
					DELETE_VEHICLE(brokenVehicles[0].vehicleIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - brokenVehicles[0].vehicleIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
					DELETE_PED(brokenVehicles[0].pedIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndexPass)
					DELETE_PED(brokenVehicles[0].pedIndexPass)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndexPass")
				ENDIF
				
				bDoingPSkip = FALSE
			ENDIF
		#ENDIF
		
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TON3_UNHOOK)
		CPRINTLN(DEBUG_MISSION, "USING SHIT SKIP - NEGATING UNHOOK BONUS")
	
		CLEAR_AREA(vPlayerFirstCheckpointPosition, 8.0, TRUE)
		
		END_REPLAY_SETUP(myVehicle)
		
		vSkipSpawnPosition = vSkipSpawnPosition
		fSkipSpawnHeading = fSkipSpawnHeading
		
//		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), vShitSkipSpawnPosition)
//			SET_ENTITY_HEADING(PLAYER_PED_ID(), fShitSkipSpawnHeading)
//			CPRINTLN(DEBUG_MISSION, "MOVING PLAYER VIA GO_TO_CHECKPOINT_3")
//		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		
		DO_SCREEN_FADE_IN(500)
		
		CPRINTLN(DEBUG_MISSION, "TONYA 5: GOING TO STATE - MS_PASSED")
		m_eState = MS_PASSED
	ELSE
		CPRINTLN(DEBUG_MISSION, "SCREEN IS NOT FADED OUT")
	ENDIF
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

#IF IS_DEBUG_BUILD

	PROC JUMP_TO_STAGE(eRC_MainState stage, BOOL bIsDebugJump = FALSE)
		DO_FADE_OUT_WITH_WAIT()
		
		//Update mission checkpoint in case they skipped the stages where it gets set.
		IF bIsDebugJump
		
			SWITCH STAGE
				CASE MS_GO_TO_TRUCK
					GO_TO_CHECKPOINT_1()
				BREAK
				CASE MS_DO_MISSION
					GO_TO_CHECKPOINT_2()
				BREAK
			ENDSWITCH

		ENDIF
	ENDPROC
	
	// PURPOSE:	Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()
	
		eRC_MainState eStage
		
		IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iDebugJumpStage)
			IF iDebugJumpStage = 0
				eStage = MS_GO_TO_TRUCK
			ELIF iDebugJumpStage = 1
				eStage = MS_DO_MISSION
			ENDIF
			bDoingPSkip = TRUE
			JUMP_TO_STAGE(eStage, TRUE)
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
		
			CPRINTLN(DEBUG_MISSION, "USING P-SKIP")
			FADE_DOWN()
			bDoingPSkip = TRUE
			
			IF m_eState < MS_DO_MISSION AND (curStage <= MISSION_STATE_CHECK_DISTANCE)
				GO_TO_CHECKPOINT_1() 
				CPRINTLN(DEBUG_MISSION, "P-SKIP: GO_TO_CHECKPOINT_1")
			ELIF m_eState = MS_DO_MISSION AND (curStage <= MISSION_STATE_CHECK_DISTANCE)
				GO_TO_CHECKPOINT_1() 
				CPRINTLN(DEBUG_MISSION, "P-SKIP: GO_TO_CHECKPOINT_1 - 2")
			ELIF m_eState = MS_DO_MISSION AND (curStage > MISSION_STATE_CHECK_DISTANCE)
				GO_TO_CHECKPOINT_2()
				CPRINTLN(DEBUG_MISSION, "P-SKIP: GO_TO_CHECKPOINT_2")
			ENDIF
		ENDIF

		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			IF IS_CUTSCENE_ACTIVE()
				STOP_CUTSCENE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				CLEAR_HELP()
				
				SET_WIDESCREEN_BORDERS(FALSE, 0)
			ENDIF
			CLEAR_PRINTS()
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			CLEAR_HELP()
			CLEAR_PRINTS()
			WAIT_FOR_CUTSCENE_TO_STOP()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
		
		// J-Skip
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			CLEAR_HELP()
			CLEAR_PRINTS()
			
			IF m_eState < MS_DO_MISSION
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
				
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

PROC HANDLE_TONYA_FAIL_CONDITIONS()
	
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		IF DO_AGGRO_CHECK(pedTonya, NULL, aggroArgs, aggroReason, FALSE, TRUE, FALSE, TRUE, FALSE)
			bAggroedTonya = TRUE
			CPRINTLN(DEBUG_MISSION, "TONYA ATTACKED OUTSIDE TUTORIAL LOOP")
			TASK_SMART_FLEE_PED(pedTonya, PLAYER_PED_ID(), 1000, -1)
		ENDIF
		
		IF HAS_PLAYER_ABANDONED_TONYA()
			bAbandonedTonya = TRUE
			CPRINTLN(DEBUG_MISSION, "PLAYER HAS ABANDONED TONYA - POST TUTORIAL")
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HANDLE_FAIL_CONDITIONS()
//	DEBUG_MESSAGE("HANDLE_FAIL_CONDITIONS()")
	
	IF m_eState != MS_FAILED
		IF bFailRc 
			DEBUG_MESSAGE("Fail detected")
			sFailReason = strFailReason
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

/// PURPOSE: 
///   Handle mission failed
PROC STATE_Failed()

	SWITCH m_FailState
	
		CASE SS_FAIL_SETUP
			
			CPRINTLN(DEBUG_MISSION, "INSIDE FAIL SETUP")
			STOP_SCRIPTED_CONVERSATION(FALSE)
			
			// Update with fail reason
			IF ARE_STRINGS_EQUAL(sFailReason, "DEFAULT")
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason, TRUE)
			ENDIF
			m_FailState = SS_FAIL_UPDATE
		BREAK

		CASE SS_FAIL_UPDATE
			CPRINTLN(DEBUG_MISSION, "INSIDE FAIL UPDATE")
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				Script_Cleanup()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Make sure that the player's last vehicle is set as a mission entity so that it wont get cleared up aggeressively by garbage collection while on the mission
PROC SAVE_LAST_PLAYER_VEHICLE()
	VEHICLE_INDEX vehLast
	
	IF m_eState > MS_INIT
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vehLast = GET_LAST_DRIVEN_VEHICLE()
			
			IF DOES_ENTITY_EXIST(vehLast)
				IF IS_VEHICLE_DRIVEABLE(vehLast)
					IF vehLast != viPlayerLastVehicle
					AND NOT IS_ENTITY_IN_WATER(vehLast)
					AND NOT IS_ENTITY_A_MISSION_ENTITY(vehLast)
						
						//clear out the previous one
						IF viPlayerLastVehicle != NULL
							IF IS_ENTITY_A_MISSION_ENTITY(viPlayerLastVehicle)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(viPlayerLastVehicle)
							ENDIF
						ENDIF
						
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							//set the new vehicle as the current mission entity vehicle
							viPlayerLastVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							SET_ENTITY_AS_MISSION_ENTITY(viPlayerLastVehicle)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///   Franklin heads to truck
PROC STATE_GoToTruck()

	IF IS_FRANKLIN_IN_ANY_TOW_TRUCK(vehTruck, FALSE)
	
		CPRINTLN(DEBUG_MISSION, "IS_FRANKLIN_IN_ANY_TOW_TRUCK HAS RETURNED TRUE")
		
		IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
			CLEAR_PRINTS()
		ENDIF
		
		m_eSubState = SS_CLEANUP
	ENDIF
		
	SWITCH m_eSubState
		
		CASE SS_SETUP
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
				IF IS_REPEAT_PLAY_ACTIVE()
					PRINTLN("REPEAT PLAY ACTIVE")
					
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("REPEAT PLAY ACTIVE - FADING IN")
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ENDIF
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "towing_tonya_franklin_travel")
			
				// Set objective
				PRINT("TOW_TUT_INTRUCK", DEFAULT_GOD_TEXT_TIME, 1)
				
				// Request tow truck
				REQUEST_MODEL(TOWTRUCK)
					
				IF NOT IS_TIMER_STARTED(tDialogueBufferTimer)
					START_TIMER_NOW(tDialogueBufferTimer)
					CPRINTLN(DEBUG_MISSION, "STARTING TIMER - tDialogueBufferTimer")
				ELSE
					RESTART_TIMER_NOW(tDialogueBufferTimer)
					CPRINTLN(DEBUG_MISSION, "RESTARTING TIMER - tDialogueBufferTimer")
				ENDIF
			
				m_eSubState = SS_UPDATE
			ENDIF
		BREAK
			
		CASE SS_UPDATE
		
			IF NOT DOES_ENTITY_EXIST(vehReplayVehicle)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						vehReplayVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						SET_ENTITY_AS_MISSION_ENTITY(vehReplayVehicle, TRUE, TRUE)
						SET_PLAYERS_LAST_VEHICLE(vehReplayVehicle)
						CPRINTLN(DEBUG_MISSION, "FOUND REPLAY VEHICLE")
						
//						IF DOES_ENTITY_EXIST(vehReplayVehicle) AND NOT IS_ENTITY_DEAD(vehReplayVehicle)
//							CPRINTLN(DEBUG_MISSION, "OVERRIDING REPLAY VEHICLE")
//							OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vehReplayVehicle)
//						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			// Attempt to create tow truck
			IF NOT DOES_ENTITY_EXIST(vehTruck)
				
				REQUEST_MODEL(TOWTRUCK)
				REQUEST_VEHICLE_ASSET(TOWTRUCK)
			
				IF HAS_MODEL_LOADED(TOWTRUCK)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vTruckPos, <<50,50,50>>)
						vehTruck = CREATE_VEHICLE(TOWTRUCK, vVehicleStartPosition, fVehicleStartHeading)
						myVehicle = vehTruck
						SET_ENTITY_AS_MISSION_ENTITY(myVehicle)
						
						SET_VEH_RADIO_STATION(myVehicle, "RADIO_03_HIPHOP_NEW")
						CPRINTLN(DEBUG_MISSION, "SETTING STATION - RADIO_03_HIPHOP_NEW")
						
						CLEAR_AREA(vVehicleStartPosition, 25.0, TRUE)
						CPRINTLN(DEBUG_MISSION, "TOO CLOSE TO DEFAULT POSITION, SPAWN TRUCK AT LIQUOR STORE")
					ELSE
						vehTruck = CREATE_VEHICLE(TOWTRUCK, vTruckPos, fTruckHeading)
						myVehicle = vehTruck
						SET_ENTITY_AS_MISSION_ENTITY(myVehicle)
						
						SET_VEH_RADIO_STATION(myVehicle, "RADIO_03_HIPHOP_NEW")
						CPRINTLN(DEBUG_MISSION, "SETTING STATION - RADIO_03_HIPHOP_NEW")
						
						CLEAR_AREA(vTruckPos, 25.0, TRUE, TRUE)
						CPRINTLN(DEBUG_MISSION, "WE'RE NOT CLOSE ENOUGH TO SEE TRUCK SPAWN IN, USE DEFAULT LOCATION")
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehTruck)
					
						// Update blip for truck
						myVehicleBlip = ADD_BLIP_FOR_ENTITY(vehTruck)
						SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
						SET_BLIP_ROUTE(myVehicleBlip, TRUE)
						
						// Release model
						SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
					ENDIF
				ENDIF
			ENDIF
		
			// Detect player getting into the tow truck
			IF NOT IS_ENTITY_DEAD(vehTruck)
				IF (GET_PED_IN_VEHICLE_SEAT(vehTruck) = PLAYER_PED_ID())
				OR IS_FRANKLIN_IN_ANY_TOW_TRUCK(vehTruck, FALSE)
				
					CPRINTLN(DEBUG_MISSION, "GOING TO STATE - SS_CLEANUP")
					m_eSubState = SS_CLEANUP
				
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(vehTruck)
					DEBUG_MESSAGE("vehTruck is dead")
					failReason = FAIL_TRANSPORT_DESTROYED
					MISSION_FAILED()
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP	
			
			IF NOT bWanted
				
				// To Fix Bug # 2013256 - DS
				IF GET_PLAYER_DISTANCE_FROM_LOCATION(<<217.3860, -2545.0613, 5.1932>>) < 150.0
					illegalIdx = 28
					CPRINTLN(DEBUG_MISSION, "PLAYER IS TOO CLOSE TO TRAIN POSITION - USING OVERRIDE, illegalIdx = ", illegalIdx)
					
					g_savedGlobals.sTowingData.iLastNodeIndex = illegalIdx
					PRINTLN("SAVING LAST NODE INDEX = ", g_savedGlobals.sTowingData.iLastNodeIndex)
					
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_USED_RESET)
					PRINTLN("SETTING BIT - TOW_GLOBAL_USED_RESET, 2")
				ENDIF
			
				// Remove blip
				IF DOES_BLIP_EXIST(myVehicleBlip)
					REMOVE_BLIP(myVehicleBlip)
				ENDIF
				
				// Onto the next stage
				m_eState = MS_DO_MISSION
				m_eSubState = SS_SETUP
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC REMOVE_SPEED_NODES()
	IF NOT bRemovedNodes
		IF bTowHooked
			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<132.130447,-2597.911621,0.004743>>, <<340.572571,-2511.292969,11.788495>>, 70.000000)
			REMOVE_ROAD_NODE_SPEED_ZONE(iTrainTrackSpeedNode01)
			REMOVE_ROAD_NODE_SPEED_ZONE(iTrainTrackSpeedNode02)
			CPRINTLN(DEBUG_MISSION, "bRemovedNodes = TRUE")
			bRemovedNodes = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC STATE_Retry()

	REQUEST_ADDITIONAL_TEXT("TOW", ODDJOB_TEXT_SLOT) // load text for the mission
	REQUEST_ADDITIONAL_TEXT("DTRSHRD", MINIGAME_TEXT_SLOT) // load text for the mission
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
		DEBUG_MESSAGE("trying to load string table")
		WAIT(0)
	ENDWHILE
	
	IF IS_REPLAY_IN_PROGRESS()

		iStageToUse = Get_Replay_Mid_Mission_Stage()
		CPRINTLN(DEBUG_MISSION, "REPLAY IS IN PROGRESS, USING STAGE = ", iStageToUse)
		
		IF g_bShitskipAccepted
			IF iStageToUse <= 1
				iStageToUse = (iStageToUse + 1)
				CPRINTLN(DEBUG_MISSION, "TONYA 1 - SHIT SKIP: iStageToUse = ", iStageToUse)
			ENDIF
		ENDIF
		
		IF iStageToUse = 0
			GO_TO_CHECKPOINT_1()
		ELIF iStageToUse = 1
			GO_TO_CHECKPOINT_2()
		ELIF iStageToUse = 2
			GO_TO_CHECKPOINT_3()
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, "WE'RE NOT DOING A REPLAY - GOING TO STATE - MS_GO_TO_TRUCK")
		m_eState = MS_GO_TO_TRUCK
	ENDIF
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	RC_CLEANUP_LAUNCHER()
	
	launchArgs.nodeType = NODE_TYPE_TRAIN
	launchArgs.launchMode = TOWING_MODE_TRADITIONAL
	launchArgs.bInit = TRUE
	launchArgs.bRcVersion = TRUE
	illegalIdx = 51
	
	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		MISSION_CLEANUP(sLoadQueue)
		Script_Cleanup()
	ENDIF
	
	RANDOM_CHAR_DISPLAY_MISSION_TITLE(RC_TONYA_3)
	
	IF NOT IS_REPEAT_PLAY_ACTIVE()
		g_savedGlobals.sTowingData.iTowingJobsCompleted = 2
		CPRINTLN(DEBUG_MISSION, "TOWING RANK = ", g_savedGlobals.sTowingData.iTowingJobsCompleted)
	ENDIF
	
	sbi01 = ADD_SCENARIO_BLOCKING_AREA((<<414.1398, -640.0020, 27.5001>> - <<50,50,50>>), (<<414.1398, -640.0020, 27.5001>> + <<50,50,50>>))
	sbi02 = ADD_SCENARIO_BLOCKING_AREA((<<-229.8159, -1171.9999, 21.8557>> - <<50,50,50>>), (<<-229.8159, -1171.9999, 21.8557>> + <<50,50,50>>))
	sbi03 = ADD_SCENARIO_BLOCKING_AREA((<<535.2601, -173.7879, 53.5258>> - <<50,50,50>>), (<<535.2601, -173.7879, 53.5258>> + <<50,50,50>>))
	
	SET_ROADS_IN_ANGLED_AREA(<<133.492828,-2596.173096,0.000145>>, <<323.766876,-2513.722900,11.683878>>, 70.000000, FALSE, FALSE)
	iTrainTrackSpeedNode01 = ADD_ROAD_NODE_SPEED_ZONE(<<344.4851, -2506.6016, 4.9430>>, 20.0, 3.0)
	iTrainTrackSpeedNode02 = ADD_ROAD_NODE_SPEED_ZONE(<<220.8717, -2544.9497, 5.2096>>, 20.0, 0.0)
	
//	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA((vCarGenPosition - <<15,15,15>>), (vCarGenPosition + <<15,15,15>>), FALSE)
//	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA((vCarGenPosition - <<15,15,15>>), (vCarGenPosition + <<15,15,15>>))

	SETUP_DEBUG()
		
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		
		WAIT(0)
		
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RC_TON3")
		
 		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		// Assert check
		IS_PLAYER_PLAYING(PLAYER_ID())
		
		// Detect if Tonya has been killed/attacked
		IF HANDLE_FAIL_CONDITIONS()
			m_eState = MS_FAILED
		ENDIF
		
		UPDATE_LAW(brokenVehicles, bTowHooked, vCurDestObjective, myVehicleBlip, blipImpound, myVehicle, curStage)
		
		IF NOT bStartedReplay
			IF IS_REPLAY_IN_PROGRESS()
			
				iStageToUse = Get_Replay_Mid_Mission_Stage()
				CPRINTLN(DEBUG_MISSION, "START - REPLAY IS IN PROGRESS, USING STAGE = ", iStageToUse)
				
				IF NOT g_bShitskipAccepted
					IF iStageToUse = 0
						START_REPLAY_SETUP(vPlayerFirstCheckpointPosition, fPlayerFirstCheckpointHeading)
						CPRINTLN(DEBUG_MISSION, "CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
					ELIF iStageToUse = 1
						START_REPLAY_SETUP(<<404.1494, -1630.7926, 28.2928>>, 231.5304)
						CPRINTLN(DEBUG_MISSION, "CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
					ELIF iStageToUse = 2
						START_REPLAY_SETUP(vShitSkipSpawnPosition, fShitSkipSpawnHeading)
						CPRINTLN(DEBUG_MISSION, "CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
					ENDIF
				ELSE
					IF iStageToUse = 0
						START_REPLAY_SETUP(<<404.1494, -1630.7926, 28.2928>>, 231.5304)
						CPRINTLN(DEBUG_MISSION, "CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
					ELIF iStageToUse = 1
						START_REPLAY_SETUP(vShitSkipSpawnPosition, fShitSkipSpawnHeading)
						CPRINTLN(DEBUG_MISSION, "CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
					ENDIF
				ENDIF
				
				IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_USED_RESET)
					IF g_savedGlobals.sTowingData.iLastNodeIndex = -1
						illegalIdx = 51
						PRINTLN("REPLAY DEFAULT: illegalIdx = ", illegalIdx)
					ELSE
						illegalIdx = g_savedGlobals.sTowingData.iLastNodeIndex
						PRINTLN("REPLAY: illegalIdx = ", illegalIdx)
					ENDIF
				ENDIF
				
				CPRINTLN(DEBUG_MISSION, "bStartedReplay = TRUE, 1")
				bStartedReplay = TRUE
			ELSE
				// To Fix Bug # 1924262 - DS
				IF GET_PLAYER_DISTANCE_FROM_LOCATION(<<217.3860, -2545.0613, 5.1932>>) < 150.0
					illegalIdx = 28
					CPRINTLN(DEBUG_MISSION, "PLAYER IS TOO CLOSE TO TRAIN POSITION - USING OVERRIDE, illegalIdx = ", illegalIdx)
					
					g_savedGlobals.sTowingData.iLastNodeIndex = illegalIdx
					PRINTLN("SAVING LAST NODE INDEX = ", g_savedGlobals.sTowingData.iLastNodeIndex)
					
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_USED_RESET)
					PRINTLN("SETTING BIT - TOW_GLOBAL_USED_RESET, 1")
				ENDIF
				
				CPRINTLN(DEBUG_MISSION, "bStartedReplay = TRUE, 2")
				bStartedReplay = TRUE
			ENDIF
		ENDIF
		
		mnExtraModel = mnExtraModel // Needed to compile
		bCurrentlyTonyaAbandoned = bCurrentlyTonyaAbandoned
		bPrintAllowTonyaToEnter = bPrintAllowTonyaToEnter
		
		// Mission update
		SWITCH(m_eState)
		
			CASE MS_INIT
				STATE_Retry()
			BREAK
			
			CASE MS_GO_TO_TRUCK
				STATE_GoToTruck()
			BREAK
			
			CASE MS_DO_MISSION
				REMOVE_SPEED_NODES()
				
				IF DO_TOWING_CORE(launchArgs, sLoadQueue)
					Script_Passed()
				ENDIF
			BREAK
			
			CASE MS_FAILED
				STATE_Failed()
			BREAK
			
			CASE MS_PASSED
				Script_Passed()
			BREAK
		ENDSWITCH
	
		// Check debug completion/failure
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
