// towing_Dynamic_lib.sch
USING "controller_Towing.sch"
USING "timer_public.sch"
USING "dialogue_public.sch"
USING "script_oddjob_funcs.sch"
USING "blip_control_public.sch"
USING "oddjob_aggro.sch"
USING "mission_control_public.sch"
USING "script_player.sch"

TOWING_LAUNCH_DATA 			launchArgs
structTimer 				offerTimer
structTimer					tBeckonTimer
structTimer					tCutsceneTimer
structTimer					tNoJobsConvoTimer
TOWING_DYNAMIC_OFFER_STATE	dynOffer = TOW_DYN_OFFR_WAIT_PRINT
TOWTRUCK_SPAWN_STATE 		eTruckState = TTSS_WAITING
AGGRO_ARGS 					aggroArgs 
EAggro 						aggroReason
structPedsForConversation	pedConvo
//structPedsForConversation MyLocalDispatchStruct

THREADID					dynamicTowScript
INT 						iDynMissionCandID = NO_CANDIDATE_ID
INT 						iDynFlags
INT							iLastMissionTime
INT 						iDelay
//INT 						myFrameCount = 0 //visible frame count

INT							iBeckonConvoStages
INT 						iFirstCutsceneStages = 0 
INT							iSecondCutsceneStages = 0
INT							iThirdCutsceneStages = 0

VECTOR						vSphereCheckPosition = <<405.8286, -1627.9960, 28.2928>>
FLOAT 						fSphereCheckRadius = 1.0

// temp
BOOL 						bDebugPrint
BOOL						bSpawnTruckAllowed = FALSE
BOOL						bSkippedCutscene = FALSE
BOOL						bRunAlternateConvo = FALSE
BOOL						bRunMichaelConvo = FALSE
BOOL						bRunTrevorConvo = FALSE
BOOL						bFailConditionsSet = FALSE
BOOL 						bSetToInit = FALSE
BOOL						bPlayedNoJobsConvo = FALSE

MODEL_NAMES					mnModelToUse

enumCharacterList 			ePlayer

BLIP_INDEX					blipTruck
PED_INDEX					pedTonya
VEHICLE_INDEX				vehTruck

CAMERA_INDEX 				cam01, cam02
//OBJECT_INDEX 				oGateToHide

/// PURPOSE:
///    Checks to see if the player is in any tow truck, as long as it doesn't belong to the static towing stuff.
/// RETURNS:
///    TRUE if he is, FALSE if not.
FUNC BOOL TOWING_DYNAMIC_IsPlayerInTruck(PED_INDEX playerPed)
	IF IS_PED_IN_ANY_VEHICLE(playerPed)
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(playerPed)
		IF NOT IS_ENTITY_DEAD(tempVeh)
			IF (GET_PED_IN_VEHICLE_SEAT(tempVeh) = playerPed)
				MODEL_NAMES tempModel = GET_ENTITY_MODEL(tempVeh)
				IF (tempModel = TOWTRUCK) OR (tempModel = TOWTRUCK2)
					
					vehTruck = tempVeh
					
					IF DOES_ENTITY_EXIST(vehTruck) AND NOT IS_ENTITY_DEAD(vehTruck)
						mnModelToUse = GET_ENTITY_MODEL(vehTruck)
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Grabs a new towing mission to launch dynamically, at random. Does not repeat the last picked mission.
/// PARAMS:
///    bDoBroadcast - Do we do the broadcast offer with this?
/// RETURNS:
///    
FUNC TOWING_MODE TOWING_DYNAMIC_PickRandomMissionType(BOOL bDoBroadcast = FALSE)	
	TOWING_MODE tempType = INT_TO_ENUM(TOWING_MODE, GET_RANDOM_INT_IN_RANGE(0, 65535) % ENUM_TO_INT(TOWING_MODES_NUM_DYN_TYPES))
	
	// Need to get a random number between 1 and (TOWING_NUM_MODES - 1)
	//INT iAttempts = 0
	//WHILE ((tempType = offerType) OR (tempType = g_savedGlobals.sTowingData.lastDynMission)) AND iAttempts < 25
	//	tempType = INT_TO_ENUM(TOWING_MODE, GET_RANDOM_INT_IN_RANGE(0, 65535) % ENUM_TO_INT(TOWING_MODES_NUM_DYN_TYPES))
	//	iAttempts++
	//	
	//	WAIT(0) //Why not.
	//ENDWHILE
	
	// Did we break out because of attempts?
	//IF (iAttempts >= 25)
	//	IF (g_savedGlobals.sTowingData.lastDynMission = TOWING_MODE_Dynamic_BrokenDown)
	//		tempType = TOWING_MODE_Dynamic_IllegallyParked
	//	ELSE
	//		tempType = TOWING_MODE_Dynamic_BrokenDown
	//	ENDIF
	//ENDIF
	//DEBUG_MESSAGE("Here's mission index we're about to offer: ", GET_STRING_FROM_INT(ENUM_TO_INT(tempType)))
	
	// If we're told to, broadcast the type we just picked.
	IF bDoBroadcast
		INT iRand = (GET_RANDOM_INT_IN_RANGE(0, 65534) % 3) + 1
		TEXT_LABEL_15 tlOffer = "TOW_OFFR_"
		tlOffer += iRand
		
		// Print out the offer for the type
		PRINT(tlOffer, DEFAULT_GOD_TEXT_TIME, 1)
	ENDIF
	
	RETURN tempType
ENDFUNC


/// PURPOSE:
///    Handles the radio states for the job offer.
PROC TOWING_DYNAMIC_HandleOffer()	
	SWITCH (dynOffer)
		CASE TOW_DYN_OFFR_WAIT_PRINT
//			DEBUG_MESSAGE("TOW_DYN_OFFR_WAIT_PRINT")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED() AND NOT IS_TIMER_STARTED(offerTimer)
					SET_FRONTEND_RADIO_ACTIVE(FALSE)
					RESTART_TIMER_NOW(offerTimer)					
				ELIF NOT IS_HELP_MESSAGE_BEING_DISPLAYED() AND IS_TIMER_STARTED(offerTimer)
					IF GET_TIMER_IN_SECONDS(offerTimer) >= 2.0
						dynOffer = TOW_DYN_OFFR_DIALOUGE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TOW_DYN_OFFR_DIALOUGE
			DEBUG_MESSAGE("TOW_DYN_OFFR_DIALOUGE")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ADD_PED_FOR_DIALOGUE(pedConvo, 4, NULL, "TOWDISPATCH")
				IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TOW_DISP_GEN", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("PLAYING DISPATCH DIALOGUE")
					dynOffer = TOW_DYN_OFFR_PRINT
				ENDIF
			ENDIF
		BREAK
		
		CASE TOW_DYN_OFFR_PRINT
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				dynOffer = TOW_DYN_OFFR_DISP
			ENDIF
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_FRONTEND_RADIO_ACTIVE(TRUE)
			ENDIF
		BREAK
		
		CASE TOW_DYN_OFFR_DISP
			IF GET_TIMER_IN_SECONDS(offerTimer) >= 6.0
				SET_FRONTEND_RADIO_ACTIVE(TRUE)	
				PRINT_HELP("TOW_JOBOFFRD")
				
				dynOffer = TOW_DYN_OFFR_WAIT_ACCEPT
			ENDIF
		BREAK
		
		CASE TOW_DYN_OFFR_WAIT_ACCEPT
			// If we wait too long to accept, terminate the offer.
			IF GET_TIMER_IN_SECONDS(offerTimer) > 30.0
				// We didn't accept the job fast enough, we lost it.
				IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_MissedJob)
					PRINT_HELP("TOW_MISSEDJOB")
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_MissedJob)
				ENDIF

				dynOffer = TOW_DYN_OFFR_WAIT_REOFFER
			ENDIF
		BREAK
		
		CASE TOW_DYN_OFFR_WAIT_REOFFER
			// In this case, it's the blank spot between when one offer was missed, and the accepting of a new offer.
			IF GET_TIMER_IN_SECONDS(offerTimer) > 60.0
				CANCEL_TIMER(offerTimer)
											
				dynOffer = TOW_DYN_OFFR_WAIT_PRINT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Hanhdles what happens after the player presses the button to accept the mission.
PROC TOWING_DYNAMIC_HandleMissionAccept()
	SET_FRONTEND_RADIO_ACTIVE(TRUE)							
	REQUEST_SCRIPT("Towing")
	CLEAR_HELP()
	
	//PRINT_NOW("TOW_ACCPT", DEFAULT_GOD_TEXT_TIME, 2)

	CANCEL_TIMER(offerTimer)
ENDPROC

PROC REMOVE_TOWING_CONTROLLER_ASSETS()
//	PRINTLN("INSIDE - REMOVE_TOWING_CONTROLLER_ASSETS")

	REMOVE_ANIM_DICT("amb@world_human_smoking@male@male_a@base")
	REMOVE_ANIM_DICT("oddjobs@towingcome_here")
	
	SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_SouCent_01)
	
	IF DOES_ENTITY_EXIST(pedTonya)
		IF IS_ENTITY_OCCLUDED(pedTonya)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_SouCent_01)
			DELETE_PED(pedTonya)
			PRINTLN("DELETING PED - pedTonya")
		ELSE
			SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_SouCent_01)
			SET_PED_AS_NO_LONGER_NEEDED(pedTonya)
			PRINTLN("SETTING PED AS NO LONGER NEEDED - pedTonya")
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehTruck)
		IF IS_ENTITY_OCCLUDED(vehTruck)
			SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
			IF IS_ENTITY_A_MISSION_ENTITY(vehTruck)	//	url:bugstar:2105480
				DELETE_VEHICLE(vehTruck)
			ENDIF
			PRINTLN("DELETING VEHICLE - vehTruck")
		ELSE
			SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTruck)
			PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED - vehTruck")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the launching of the script, once loaded.
/// RETURNS:
///    TRUE when the script has loaded and fired.
FUNC TOWING_UPDATE_RETVAL TOWING_DYNAMIC_Launch()
	IF HAS_SCRIPT_LOADED("Towing")
		m_enumMissionCandidateReturnValue eLaunchVal = Request_Mission_Launch(iMissionCandidateID, MCTID_CONTACT_POINT, MISSION_TYPE_RANDOM_CHAR)
//		iMissionCandidateID = ENUM_TO_INT(eLaunchVal)
		
		// Process state we've been handed.
		IF (eLaunchVal = MCRET_ACCEPTED) 
			CLEAR_HELP()
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				PRINTLN("TOWING CONTROLLER - CLEARING WANTED LEVEL")
			ENDIF
			
			// Default towing mode.
			launchArgs.launchMode = TOWING_MODE_Traditional
			
			// Okay, so in debug mode, we need to check and make sure we're not being forced into a mode.
			#IF IS_DEBUG_BUILD
				IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Abandoned)
					launchArgs.launchMode = TOWING_MODE_TRADITIONAL
					launchArgs.nodeType = NODE_TYPE_ABANDON
					launchArgs.bDebug = TRUE
//					PRINTLN("launchArgs.bDebug = TRUE - 01")
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//						PRINTLN("CLEARING BITMASK - TOW_GLOBAL_SWITCH")
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Accident)
					launchArgs.launchMode = TOWING_MODE_TRADITIONAL
					launchArgs.nodeType = NODE_TYPE_ACCIDENT
					launchArgs.bDebug = TRUE
//					PRINTLN("launchArgs.bDebug = TRUE - 02")
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//						PRINTLN("CLEARING BITMASK - TOW_GLOBAL_SWITCH")
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_BrokenDown)
					launchArgs.launchMode = TOWING_MODE_TRADITIONAL
					launchArgs.nodeType = NODE_TYPE_DYNAMIC
					launchArgs.bDebug = TRUE
//					PRINTLN("launchArgs.bDebug = TRUE - 03")
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//						PRINTLN("CLEARING BITMASK - TOW_GLOBAL_SWITCH")
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Handicap)
					launchArgs.launchMode = TOWING_MODE_TRADITIONAL
					launchArgs.nodeType = NODE_TYPE_HANDI
					launchArgs.bDebug = TRUE
//					PRINTLN("launchArgs.bDebug = TRUE - 04")
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//						PRINTLN("CLEARING BITMASK - TOW_GLOBAL_SWITCH")
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Selection)
					launchArgs.bDebug = TRUE
					launchArgs.bSpecifyNode = TRUE
//					PRINTLN("launchArgs.bDebug = TRUE - 05")
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//						PRINTLN("CLEARING BITMASK - TOW_GLOBAL_SWITCH")
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Train)
					launchArgs.launchMode = TOWING_MODE_TRADITIONAL
					launchArgs.nodeType = NODE_TYPE_TRAIN
					launchArgs.bDebug = TRUE
//					PRINTLN("launchArgs.bDebug = TRUE - 06")
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//						PRINTLN("CLEARING BITMASK - TOW_GLOBAL_SWITCH")
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA1)
					launchArgs.launchMode = TOWING_MODE_TRADITIONAL
					launchArgs.nodeType = NODE_TYPE_ABANDON
//					PRINTLN("LAUNCHING TONYA 1 VIA DEBUG - SETTING NODE TO NODE_TYPE_ABANDON")
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//						PRINTLN("CLEARING BITMASK - TOW_GLOBAL_SWITCH")
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA2)
					launchArgs.launchMode = TOWING_MODE_TRADITIONAL
					launchArgs.nodeType = NODE_TYPE_HANDI
//					PRINTLN("LAUNCHING TONYA 2 VIA DEBUG - SETTING NODE TO NODE_TYPE_HANDI")
					launchArgs.bDebug = TRUE
//					PRINTLN("launchArgs.bDebug = TRUE - 07")
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//						PRINTLN("CLEARING BITMASK - TOW_GLOBAL_SWITCH")
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA3)
					launchArgs.launchMode = TOWING_MODE_TRADITIONAL
					launchArgs.nodeType = NODE_TYPE_TRAIN
//					PRINTLN("LAUNCHING TONYA 3 VIA DEBUG - SETTING NODE TO NODE_TYPE_TRAIN")
					launchArgs.bDebug = TRUE
//					PRINTLN("launchArgs.bDebug = TRUE - 08")
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//						PRINTLN("CLEARING BITMASK - TOW_GLOBAL_SWITCH")
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA4)
					launchArgs.launchMode = TOWING_MODE_TRADITIONAL
					launchArgs.nodeType = NODE_TYPE_DYNAMIC
//					PRINTLN("LAUNCHING TONYA 4 VIA DEBUG - SETTING NODE TO NODE_TYPE_DYNAMIC")
					launchArgs.bDebug = TRUE
//					PRINTLN("launchArgs.bDebug = TRUE - 09")
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//						PRINTLN("CLEARING BITMASK - TOW_GLOBAL_SWITCH")
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA5)
					launchArgs.launchMode = TOWING_MODE_TRADITIONAL
					launchArgs.nodeType = NODE_TYPE_ACCIDENT
//					PRINTLN("LAUNCHING TONYA 5 VIA DEBUG - SETTING NODE TO NODE_TYPE_ACCIDENT")
					launchArgs.bDebug = TRUE
//					PRINTLN("launchArgs.bDebug = TRUE - 10")
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//						PRINTLN("CLEARING BITMASK - TOW_GLOBAL_SWITCH")
					ENDIF
				ENDIF
				
				IF bSetToInit
					bSetToInit = FALSE
//					PRINTLN("SETTING bSetToInit = FALSE")
				ENDIF
				
				// Just to be safe, wipe that data.
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_LaunchedViaDebug)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Abandoned)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Accident)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_BrokenDown)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Handicap)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Selection)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Train)
				
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA1)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA2)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA3)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA4)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA5)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_PROC)
			#ENDIF
			
			// Launch script. We're not caring about location, as the script defines where this is going to take place.
			dynamicTowScript = START_NEW_SCRIPT_WITH_ARGS("Towing", launchArgs, SIZE_OF(launchArgs), MISSION_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("Towing")
			
//			PRINTLN("WE'RE STARTING TOWING SCRIPT")
				
			IF NOT IS_ENTITY_DEAD(pedTonya)
				SET_PED_KEEP_TASK(pedTonya, TRUE)
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(pedTonya)
			REMOVE_ANIM_DICT("amb@world_human_smoking@male@male_a@base")
			REMOVE_ANIM_DICT("oddjobs@towingcome_here")
			
//			PED_INDEX entTemp = pedTonya
//			IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
//				REMOVE_PED_FROM_GROUP(pedTonya)
//			ENDIF
//			SET_PED_AS_NO_LONGER_NEEDED(pedTonya)
//			pedTonya = entTemp
//			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTruck)
//			
//			REMOVE_ANIM_DICT("amb@world_human_smoking@male@male_a@base")
//			REMOVE_ANIM_DICT("oddjobs@towingcome_here")
			
		#IF IS_DEBUG_BUILD
			TEXT_LABEL StringContainingLaunchMode = ENUM_TO_INT(launchArgs.launchMode)
			DEBUG_MESSAGE("TOWING_LAUNCH: Launching new towing script, variant: ", StringContainingLaunchMode)
		#ENDIF
			
			SET_BITMASK_AS_ENUM(iDynFlags, TOWING_FLAG_MISSION_CAND_LAUNCHED)
	
			DEBUG_MESSAGE("PASSING static towing launch!")
			RETURN TOWING_RETVAL_PASS
		
		ELIF (eLaunchVal = MCRET_DENIED)
			// Denied.
			RETURN TOWING_RETVAL_FAIL
		ELSE
			DEBUG_MESSAGE("PROCESSING dynamic towing launch...")
		ENDIF
	ENDIF
	
	RETURN TOWING_RETVAL_NONE
ENDFUNC


/// PURPOSE:
///    Clean all data for the dynamic towing launcher.
PROC TOWING_DYNAMIC_Cleanup()
	IF IS_BITMASK_AS_ENUM_SET(iDynFlags, TOWING_FLAG_MISSION_CAND_LAUNCHED)
		CLEAR_BITMASK_AS_ENUM(iDynFlags, TOWING_FLAG_MISSION_CAND_LAUNCHED)
		Mission_Over(iDynMissionCandID)
	ENDIF
	
//	IF DOES_ENTITY_EXIST(oGateToHide)
//		SET_ENTITY_VISIBLE(oGateToHide, TRUE)
//		SET_ENTITY_COLLISION(oGateToHide, TRUE)
//		PRINTLN("CLEANUP: SETTING GATE TO BE VISIBLE AGAIN")
//	ELSE
//		PRINTLN("CLEANUP: GATE DOES NOT EXIST")
//	ENDIF
	
	IF IS_THREAD_ACTIVE(dynamicTowScript)
		TERMINATE_THREAD(dynamicTowScript)
	ENDIF
	
	SET_SCRIPT_AS_NO_LONGER_NEEDED("Towing")
ENDPROC


/// PURPOSE:
///    Checks to see if dynamic towing is allowed to progress to a run state.
/// RETURNS:
///    TRUE if so.
FUNC BOOL TOWING_DYNAMIC_CanWeLaunch(TOWING_STATE & eState, BOOL bCheckRunning = FALSE, BOOL bCheckForFranklin = TRUE)		
	IF NOT bCheckRunning
		IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
			iLastMissionTime = GET_GAME_TIMER()
			iDelay = TOWING_MISSION_DELAY
//			PRINTLN("TOWING_DYNAMIC_CanWeLaunch: MISSION IS RUNNING - RETURN FALSE")
			RETURN FALSE
		ENDIF
		
//		IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_RANDOM_EVENT)
//			iLastMissionTime = GET_GAME_TIMER()
//			iDelay = TOWING_MISSION_DELAY
//			PRINTLN("TOWING_DYNAMIC_CanWeLaunch: RANDOM EVENT IS RUNNING - RETURN FALSE")
//			RETURN FALSE
//		ENDIF
		
		IF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_1)
			iLastMissionTime = GET_GAME_TIMER()
			iDelay = TOWING_MISSION_DELAY
//			PRINTLN("TOWING_DYNAMIC_CanWeLaunch: FRANKLIN 1 IS RUNNING - RETURN FALSE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
		// If we were in the middle of a job, we lose that. Notify the player.
		IF (eState >= TOWING_LAUNCH)
			IF (g_savedGlobals.sTowingData.iTowingJobsCompleted >= 5)
				PRINT_HELP("TOW_WANTED")
			ENDIF
		ENDIF
		iLastMissionTime = GET_GAME_TIMER()
		iDelay = TOWING_WANTED_DELAY
		
//		PRINTLN("TOWING_DYNAMIC_CanWeLaunch: PLAYER WANTED - RETURN FALSE")
		RETURN FALSE
	ENDIF
	
	IF bCheckForFranklin
		// Only Franklin can do towing.
		ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
		IF (ePlayer != CHAR_FRANKLIN)
//			PRINTLN("TOWING_DYNAMIC_CanWeLaunch: PLAYER NOT FRANKLIN - RETURN FALSE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// See if enough time has passed since the last mission.
	IF (GET_GAME_TIMER() - iLastMissionTime < iDelay)
//		PRINTLN("TOWING_DYNAMIC_CanWeLaunch: NOT ENOUGH TIME HAS PASSED - RETURN FALSE")
		RETURN FALSE
	ENDIF
	
//	PRINTLN("TOWING_DYNAMIC_CanWeLaunch: WE'RE GOOD TO RUN")
	RETURN TRUE
ENDFUNC



/// PURPOSE:
///    Responsible for ensuring that there's a tow truck at the starting area when the player needs one.
PROC TOWING_DYNAMIC_UPDATE_TRUCK(PED_INDEX playerPed, VECTOR vTruckPos, FLOAT fTruckHeading, TOWING_STATE & eDynamicState)
	FLOAT fDist2 = VDIST2(vTruckPos, GET_ENTITY_COORDS(playerPed))
	
	VEHICLE_INDEX vehTemp
	SWITCH eTruckState
		CASE TTSS_WAITING			
			// First, ensure that the player isn't in a tow truck, in good repair.
			IF IS_ENTITY_DEAD(playerPed)
				PRINTLN("TOWING_DYNAMIC_UPDATE_TRUCK: PLAYER IS DEAD")
				EXIT
			ENDIF
			
			// See if we're close enough to care.
			IF (fDist2 > 62500) // Must be closer than 250m2
//				PRINTLN("TOWING_DYNAMIC_UPDATE_TRUCK: WE'RE TOO FAR AWAY TO CARE")
				EXIT
			ENDIF
			IF (fDist2 < 25) AND NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_DEBUG_LAUNCH)
//				PRINTLN("TOWING_DYNAMIC_UPDATE_TRUCK: WE'RE CLOSE AND HAVEN'T LAUNCHED")
				EXIT
			ENDIF
			IF bFailConditionsSet
//				PRINTLN("bFailConditionsSet IS TRUE")
				EXIT
			ENDIF
			
			// Can't currently be on a towing mission.
			
			// Make sure we're not in a tow truck (that has enough health).
			IF IS_PED_IN_ANY_VEHICLE(playerPed)
				vehTemp = GET_VEHICLE_PED_IS_IN(playerPed)
				IF NOT IS_ENTITY_DEAD(vehTemp)
					MODEL_NAMES eModelTemp
					eModelTemp = GET_ENTITY_MODEL(vehTemp)
					IF ((eModelTemp = TOWTRUCK) OR (eModelTemp = TOWTRUCK2)) AND GET_ENTITY_HEALTH(vehTemp) > 400
//						PRINTLN("TOWING_DYNAMIC_UPDATE_TRUCK: MODEL CHECK VIA UPDATE TRUCK")
						EXIT
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT TOWING_DYNAMIC_CanWeLaunch(eDynamicState)
//				PRINTLN("TOWING_DYNAMIC_UPDATE_TRUCK: WE CAN'T LAUNCH VIA TOWING_DYNAMIC_UPDATE_TRUCK")
				EXIT
			ENDIF
			
			// We've gotten far enough that we need a truck. Go there.
			eTruckState = TTSS_REQUEST
		BREAK
		
		CASE TTSS_REQUEST
			// Need a truck. Ask for the model.
			REQUEST_MODEL(TOWTRUCK)
//			PRINTLN("TOWING_DYNAMIC_UPDATE_TRUCK: REQUESTING TOWTRUCK MODEL")
			eTruckState = TTSS_CREATE
		BREAK
		
		CASE TTSS_CREATE
			// Spawn and blip out new truck.
			IF HAS_MODEL_LOADED(TOWTRUCK)
				IF NOT IS_SPHERE_VISIBLE(vSphereCheckPosition, fSphereCheckRadius) OR IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_DEBUG_LAUNCH)
					IF NOT DOES_ENTITY_EXIST(vehTruck)
						vehTruck = CREATE_VEHICLE(TOWTRUCK, vTruckPos, fTruckHeading)
						SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
						
//						PRINTLN("CREATING TOW TRUCK - 01")
						eTruckState = TTSS_UPDATE_TRUCK
					ENDIF
				ELSE
//					PRINTLN("TOWING_DYNAMIC_UPDATE_TRUCK: WE CAN SEE THE SPAWN POSITION - LOOK AWAY!!!")
				ENDIF
			ELSE
//				PRINTLN("WAITING ON TRUCK MODEL TO LOAD")
			ENDIF
		BREAK
		
		CASE TTSS_UPDATE_TRUCK
			// Update the truck.
			IF IS_PED_IN_ANY_VEHICLE(playerPed)
				IF (GET_VEHICLE_PED_IS_IN(playerPed) = vehTruck)
//					PRINTLN("TOWING_DYNAMIC_UPDATE_TRUCK: WE'RE IN THE TRUCK GOING TO - eTruckState = TTSS_WAITING")
					eTruckState = TTSS_WAITING
				ENDIF
				
			ENDIF

			IF IS_ENTITY_DEAD(vehTruck)
//				PRINTLN("TOWING_DYNAMIC_UPDATE_TRUCK: TRUCK IS DEAD")
				eTruckState = TTSS_WAITING
			ENDIF
				
			// Not in the truck.
			// See if we've gotten too far away.
			IF (fDist2 > 62500.0) // Must be closer than 250m2
				// Reset us. We can now spawn another truck.

				DELETE_VEHICLE(vehTruck)
//				PRINTLN("TOWING_DYNAMIC_UPDATE_TRUCK: DELETING TRUCK TOO FAR AWAY")
				eTruckState = TTSS_WAITING
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC HANDLE_NO_JOBS_CONVO(TOWING_STATE & eDynamicState, PED_INDEX playerPed)
	IF NOT TOWING_DYNAMIC_CanWeLaunch(eDynamicState)
		IF TOWING_DYNAMIC_IsPlayerInTruck(playerPed)
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)	
			OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CURRENTLY_ON_MISSION_TO_TYPE()
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
				AND NOT (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
				AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					ADD_PED_FOR_DIALOGUE(pedConvo, 1, PLAYER_PED_ID(), "FRANKLIN")
					ADD_PED_FOR_DIALOGUE(pedConvo, 4, NULL, "TOWDISPATCH")
					IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TOW_JOBS_AVL", CONV_PRIORITY_AMBIENT_HIGH)
						
						IF NOT IS_TIMER_STARTED(tNoJobsConvoTimer)
							START_TIMER_NOW(tNoJobsConvoTimer)
							PRINTLN("STARTING TIMER - tNoJobsConvoTimer")
						ELSE
							RESTART_TIMER_NOW(tNoJobsConvoTimer)
							PRINTLN("RESTARTING TIMER - tNoJobsConvoTimer")
						ENDIF
						
						PRINTLN("bPlayedNoJobsConvo = TRUE")
						bPlayedNoJobsConvo = TRUE
					ENDIF
				ELSE
//					PRINTLN("SOMETHING IS STOPING THE CONVO")
				ENDIF
			ELSE
//					PRINTLN("CONTROL IS NOT PRESSED")
			ENDIF
		ELSE
//			PRINTLN("NOT IN THE TRUCK")
		ENDIF
	ENDIF
	
	IF bPlayedNoJobsConvo
		IF IS_TIMER_STARTED(tNoJobsConvoTimer)
			IF GET_TIMER_IN_SECONDS(tNoJobsConvoTimer) > 4.0
				IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TOW_NO_JOBS", CONV_PRIORITY_AMBIENT_HIGH)
					bPlayedNoJobsConvo = FALSE
					PRINTLN("bPlayedNoJobsConvo = FALSE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Given the number of missions passed, returns where the truck and Tonya should spawn (by reference).
PROC TOWING_DYNAMIC_GetSpawnCoords(INT iMissionsCompleted, VECTOR & vTruckPos, FLOAT & fTruckHeading, VECTOR & vTonyaPos, FLOAT & fTonyaHeading)
	SWITCH iMissionsCompleted
		CASE 0
			// 1st: We meet Tonya, she walks with us to the truck, she rides with us to the mission from there.
			//vTonyaPos = <<-15.6, -1473.7, 30.6>>	fTonyaHeading = 0.0
			vTonyaPos = <<-14.3934, -1472.6962, 29.5896>> 	fTonyaHeading = 314.2467
		BREAK
		CASE 1
			// 2nd: We meet Tonya, she walks with us to the truck, she rides with us to the mission from there.
			//vTonyaPos = <<-15.6, -1473.7, 30.6>>	fTonyaHeading = 0.0
			vTonyaPos =  <<-14.3934, -1472.6962, 29.5896>>	fTonyaHeading = 314.2467
		BREAK
		CASE 2
			// 3rd: Player has called Tonya for a job. Tonya won't spawn for this one.
			vTonyaPos = <<0,0,0>>					fTonyaHeading = 0.0
		BREAK
		CASE 3
			// 4th: Player has called Tonya for a job. Tonya won't spawn for this one.
			vTonyaPos = <<0,0,0>>					fTonyaHeading = 0.0
		BREAK
		CASE 4
			// 5th: We meet Tonya, she walks with us to the truck, she rides with us to the mission from there. 
			//vTonyaPos = <<-15.6, -1473.7, 30.6>>	fTonyaHeading = 0.0
			vTonyaPos =  <<-14.3934, -1472.6962, 29.5896>>	fTonyaHeading = 314.2467
		BREAK
		
		DEFAULT 
			// If it's greater than 4 completed, we're in endless summer mode. Tonya doesn't spawn here.
			vTonyaPos = <<0,0,0>>					fTonyaHeading = 0.0
		BREAK
	ENDSWITCH
	
	vTruckPos = <<401.6370, -1633.3003, 28.2928>>
	fTruckHeading = 231.5304
ENDPROC

PROC TONYA_FIRST_CUTSCENE_CLEANUP()

	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	DISABLE_CELLPHONE(FALSE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	ENDIF
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	IF DOES_CAM_EXIST(cam01)
		DESTROY_CAM(cam01)
	ENDIF
	IF DOES_CAM_EXIST(cam02)
		DESTROY_CAM(cam02)
	ENDIF
	
	bSkippedCutscene = FALSE
	
	PRINTLN("RUNNING CUTSCENE CLEANUP")
ENDPROC

FUNC BOOL RUN_FIRST_TONYA_CUTSCENE()

	IF NOT bSkippedCutscene
		IF IS_TIMER_STARTED(tCutsceneTimer)
			IF GET_TIMER_IN_SECONDS(tCutsceneTimer) > 1.0
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
					DO_SCREEN_FADE_OUT(500)
					WHILE NOT IS_SCREEN_FADED_OUT()
						WAIT(0)
					ENDWHILE
					
					CLEAR_PRINTS()
					CLEAR_HELP()
					HANG_UP_AND_PUT_AWAY_PHONE()
					STOP_SCRIPTED_CONVERSATION(FALSE)
					
					PRINTLN("TOWING: FIRST CUTSCENE HAS BEEN SKIPPED")
					
					PRINTLN("iFirstCutsceneStages = 3")
					iFirstCutsceneStages = 3
					
					bSkippedCutscene = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iFirstCutsceneStages
		CASE 0
			cam01 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-12.2070, -1472.5962, 31.2021>>, <<-5.2325, -0.0000, 96.5678>>, 35.0, TRUE)
			cam02 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-12.2642, -1472.0994, 31.2021>>, <<-5.2325, -0.0000, 96.5678>>, 35.0, FALSE)
			
			CLEAR_PRINTS()
			CLEAR_HELP()
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			DISABLE_CELLPHONE(TRUE)
			WAIT(0)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS | SPC_REMOVE_FIRES | SPC_REMOVE_PROJECTILES)
			PRINTLN("TURNING OFF PLAYER CONTROL")
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SET_CAM_ACTIVE_WITH_INTERP(cam02, cam01, 12000)
		
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedTonya)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-13.3596, -1471.8689, 29.5836>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 137.1648)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
				
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedTonya, -1)
				TASK_LOOK_AT_ENTITY(pedTonya, PLAYER_PED_ID(), -1)
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(pedConvo, 1, PLAYER_PED_ID(), "FRANKLIN")
			CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_TOW_1", CONV_PRIORITY_VERY_HIGH)
			
			IF NOT IS_TIMER_STARTED(tCutsceneTimer)
				START_TIMER_NOW(tCutsceneTimer)
				PRINTLN("STATING TIMER - tCutsceneTimer")
			ENDIF
			
			PRINTLN("iFirstCutsceneStages = 1")
			iFirstCutsceneStages = 1
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				TONYA_FIRST_CUTSCENE_CLEANUP()
			
				PRINTLN("iFirstCutsceneStages = 2")
				iFirstCutsceneStages = 2
			ELSE
//				PRINTLN("CONVERSATION IS STILL GOING")
			ENDIF
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 2
			RETURN TRUE
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 3
			TONYA_FIRST_CUTSCENE_CLEANUP()
			
			PRINTLN("iFirstCutsceneStages = 4")
			iFirstCutsceneStages = 4
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 4
			PRINTLN("SKIPPING CUTSCENE: iFirstCutsceneStages = 4")
		
			DO_SCREEN_FADE_IN(1000)
			WHILE NOT IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
			
			RETURN TRUE
		BREAK
		//------------------------------------------------------------------------------------------------
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL RUN_SECOND_TONYA_CUTSCENE()

	IF NOT bSkippedCutscene
		IF IS_TIMER_STARTED(tCutsceneTimer)
			IF GET_TIMER_IN_SECONDS(tCutsceneTimer) > 1.0
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
					DO_SCREEN_FADE_OUT(500)
					WHILE NOT IS_SCREEN_FADED_OUT()
						WAIT(0)
					ENDWHILE
					
					CLEAR_PRINTS()
					CLEAR_HELP()
					HANG_UP_AND_PUT_AWAY_PHONE()
					STOP_SCRIPTED_CONVERSATION(FALSE)
					
					PRINTLN("TOWING: SECOND CUTSCENE HAS BEEN SKIPPED")
					
					PRINTLN("iSecondCutsceneStages = 3")
					iSecondCutsceneStages = 3
					
					bSkippedCutscene = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iSecondCutsceneStages
		CASE 0
			cam01 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-12.2070, -1472.5962, 31.2021>>, <<-5.2325, -0.0000, 96.5678>>, 35.0, TRUE)
			cam02 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-12.2642, -1472.0994, 31.2021>>, <<-5.2325, -0.0000, 96.5678>>, 35.0, FALSE)
			
			CLEAR_PRINTS()
			CLEAR_HELP()
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			DISABLE_CELLPHONE(TRUE)
			WAIT(0)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS | SPC_REMOVE_FIRES | SPC_REMOVE_PROJECTILES)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SET_CAM_ACTIVE_WITH_INTERP(cam02, cam01, 12000)
		
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedTonya)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-13.3596, -1471.8689, 29.5836>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 137.1648)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
				
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedTonya, -1)
				TASK_LOOK_AT_ENTITY(pedTonya, PLAYER_PED_ID(), -1)
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(pedConvo, 1, PLAYER_PED_ID(), "FRANKLIN")
			CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_TOW_2", CONV_PRIORITY_VERY_HIGH)
			
			IF NOT IS_TIMER_STARTED(tCutsceneTimer)
				START_TIMER_NOW(tCutsceneTimer)
				PRINTLN("STATING TIMER - tCutsceneTimer")
			ENDIF
			
			PRINTLN("iSecondCutsceneStages = 1")
			iSecondCutsceneStages = 1
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				TONYA_FIRST_CUTSCENE_CLEANUP()
			
				PRINTLN("iSecondCutsceneStages = 2")
				iSecondCutsceneStages = 2
			ENDIF
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 2
			RETURN TRUE
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 3
			TONYA_FIRST_CUTSCENE_CLEANUP()
			
			PRINTLN("iSecondCutsceneStages = 4")
			iSecondCutsceneStages = 4
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 4
			PRINTLN("SKIPPING CUTSCENE: iSecondCutsceneStages = 4")
		
			DO_SCREEN_FADE_IN(1000)
			WHILE NOT IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
			
			RETURN TRUE
		BREAK
		//------------------------------------------------------------------------------------------------
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL RUN_LAST_TONYA_CUTSCENE()
	
	SWITCH iThirdCutsceneStages
		CASE 0
			cam01 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-12.2070, -1472.5962, 31.2021>>, <<-5.2325, -0.0000, 96.5678>>, 35.0, TRUE)
			cam02 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-12.2642, -1472.0994, 31.2021>>, <<-5.2325, -0.0000, 96.5678>>, 35.0, FALSE)
			
			CLEAR_PRINTS()
			CLEAR_HELP()
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			DISABLE_CELLPHONE(TRUE)
			WAIT(0)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS | SPC_REMOVE_FIRES | SPC_REMOVE_PROJECTILES)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SET_CAM_ACTIVE_WITH_INTERP(cam02, cam01, 12000)
		
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedTonya)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-13.3596, -1471.8689, 29.5836>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 137.1648)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
				
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedTonya, -1)
				TASK_LOOK_AT_ENTITY(pedTonya, PLAYER_PED_ID(), -1)
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(pedConvo, 1, PLAYER_PED_ID(), "FRANKLIN")
			CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_TOW_5", CONV_PRIORITY_VERY_HIGH)
			
			IF NOT IS_TIMER_STARTED(tCutsceneTimer)
				START_TIMER_NOW(tCutsceneTimer)
				PRINTLN("STATING TIMER - tCutsceneTimer")
			ENDIF
			
			PRINTLN("iThirdCutsceneStages = 1")
			iThirdCutsceneStages = 1
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				TONYA_FIRST_CUTSCENE_CLEANUP()
			
				PRINTLN("iThirdCutsceneStages = 2")
				iThirdCutsceneStages = 2
			ENDIF
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 2
			RETURN TRUE
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 3
		
		BREAK
		//------------------------------------------------------------------------------------------------
		CASE 4
		
		BREAK
		//------------------------------------------------------------------------------------------------
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL TOWING_DYNAMIC_HandleTonyaCutscene(INT iMission)
//	PRINTLN("iMission = ", iMission)

	SWITCH iMission
		CASE 0
			IF RUN_FIRST_TONYA_CUTSCENE()
				PRINTLN("FIRST CUTSCENE IS DONE")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 1
			IF RUN_SECOND_TONYA_CUTSCENE()
				PRINTLN("SECOND CUTSCENE IS DONE")
				RETURN TRUE
			ENDIF
		BREAK
		
		// These two are over the phone.
		//CASE 3
		//BREAK
		//CASE 4
		//BREAK
		
		CASE 4
			IF RUN_LAST_TONYA_CUTSCENE()
				PRINTLN("LAST CUTSCENE IS DONE")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC HANDLE_BECKON_CONVO()

	ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	
	IF (ePlayer != CHAR_FRANKLIN)
		bRunAlternateConvo = TRUE
//		PRINTLN("bRunAlternateConvo = TRUE")
		IF ePlayer = CHAR_MICHAEL
			bRunMichaelConvo = TRUE
//			PRINTLN("bRunMichaelConvo = TRUE")
		ELIF ePlayer = CHAR_TREVOR
			bRunTrevorConvo = TRUE
//			PRINTLN("bRunTrevorConvo = TRUE")
		ENDIF
	ENDIF
	
	SWITCH iBeckonConvoStages
		CASE 0
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	
				ADD_PED_FOR_DIALOGUE(pedConvo, 3, NULL, "TONYA")
				
				IF bRunAlternateConvo
					IF bRunMichaelConvo
						ADD_PED_FOR_DIALOGUE(pedConvo, 0, NULL, "MICHAEL")
					ELIF bRunTrevorConvo
						ADD_PED_FOR_DIALOGUE(pedConvo, 2, NULL, "TREVOR")
					ENDIF
					
					CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_OTH_1", CONV_PRIORITY_HIGH)
				ELSE
					CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_BECK_1", CONV_PRIORITY_HIGH)
				ENDIF
				
				IF NOT IS_TIMER_STARTED(tBeckonTimer)
					START_TIMER_NOW(tBeckonTimer)
				ELSE
					RESTART_TIMER_NOW(tBeckonTimer)
				ENDIF
			
				PRINTLN("iBeckonConvoStages = 1")
				iBeckonConvoStages = 1
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_TIMER_STARTED(tBeckonTimer)
					RESTART_TIMER_NOW(tBeckonTimer)
					
					PRINTLN("iBeckonConvoStages = 2")
					iBeckonConvoStages = 2
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------	
		CASE 2
			IF IS_TIMER_STARTED(tBeckonTimer)
				IF GET_TIMER_IN_SECONDS(tBeckonTimer) > GET_RANDOM_FLOAT_IN_RANGE(9, 15)
					IF bRunAlternateConvo
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_OTH_2", CONV_PRIORITY_HIGH)
					ELSE
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_BECK_2", CONV_PRIORITY_HIGH)
					ENDIF
					
					PRINTLN("iBeckonConvoStages = 3")
					iBeckonConvoStages = 3
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 3
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_TIMER_STARTED(tBeckonTimer)
					RESTART_TIMER_NOW(tBeckonTimer)
					
					PRINTLN("iBeckonConvoStages = 4")
					iBeckonConvoStages = 4
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 4
			IF IS_TIMER_STARTED(tBeckonTimer)
				IF GET_TIMER_IN_SECONDS(tBeckonTimer) > GET_RANDOM_FLOAT_IN_RANGE(9, 15)
				
					IF bRunAlternateConvo
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_OTH_3", CONV_PRIORITY_HIGH)
					ELSE
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_BECK_3", CONV_PRIORITY_HIGH)
					ENDIF
					
					PRINTLN("iBeckonConvoStages = 5")
					iBeckonConvoStages = 5
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 5
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_TIMER_STARTED(tBeckonTimer)
					RESTART_TIMER_NOW(tBeckonTimer)
					
					PRINTLN("iBeckonConvoStages = 6")
					iBeckonConvoStages = 6
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 6
			IF IS_TIMER_STARTED(tBeckonTimer)
				IF GET_TIMER_IN_SECONDS(tBeckonTimer) > GET_RANDOM_FLOAT_IN_RANGE(9, 15)
					
					IF bRunAlternateConvo
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_OTH_4", CONV_PRIORITY_HIGH)
					ELSE
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_BECK_4", CONV_PRIORITY_HIGH)
					ENDIF
					
					PRINTLN("iBeckonConvoStages = 7")
					iBeckonConvoStages = 7
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 7
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_TIMER_STARTED(tBeckonTimer)
					RESTART_TIMER_NOW(tBeckonTimer)
					
					PRINTLN("iBeckonConvoStages = 8")
					iBeckonConvoStages = 8
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 8
			IF IS_TIMER_STARTED(tBeckonTimer)
				IF GET_TIMER_IN_SECONDS(tBeckonTimer) > GET_RANDOM_FLOAT_IN_RANGE(9, 15)
				
					IF bRunAlternateConvo
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_OTH_5", CONV_PRIORITY_HIGH)
					ELSE
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_BECK_5", CONV_PRIORITY_HIGH)
					ENDIF
					
					PRINTLN("iBeckonConvoStages = 9")
					iBeckonConvoStages = 9
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 9
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_TIMER_STARTED(tBeckonTimer)
					RESTART_TIMER_NOW(tBeckonTimer)
					
					PRINTLN("iBeckonConvoStages = 10")
					iBeckonConvoStages = 10
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 10
			IF IS_TIMER_STARTED(tBeckonTimer)
				IF GET_TIMER_IN_SECONDS(tBeckonTimer) > GET_RANDOM_FLOAT_IN_RANGE(9, 15)
					IF bRunAlternateConvo
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_OTH_6", CONV_PRIORITY_HIGH)
					ELSE
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_BECK_6", CONV_PRIORITY_HIGH)
					ENDIF
					
					PRINTLN("iBeckonConvoStages = 11")
					iBeckonConvoStages = 11
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 11
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_TIMER_STARTED(tBeckonTimer)
					RESTART_TIMER_NOW(tBeckonTimer)
					
					PRINTLN("iBeckonConvoStages = 12")
					iBeckonConvoStages = 12
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 12
			IF IS_TIMER_STARTED(tBeckonTimer)
				IF GET_TIMER_IN_SECONDS(tBeckonTimer) > GET_RANDOM_FLOAT_IN_RANGE(9, 15)
					IF bRunAlternateConvo
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_OTH_7", CONV_PRIORITY_HIGH)
					ELSE
						CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_BECK_7", CONV_PRIORITY_HIGH)
					ENDIF
					
					PRINTLN("iBeckonConvoStages = 13")
					iBeckonConvoStages = 13
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 13
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_TIMER_STARTED(tBeckonTimer)
					RESTART_TIMER_NOW(tBeckonTimer)
					
					PRINTLN("iBeckonConvoStages = 0")
					iBeckonConvoStages = 0
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HANDLE_FAIL_CONDITIONS()

	IF DOES_ENTITY_EXIST(pedTonya)
		IF IS_ENTITY_DEAD(pedTonya)
			REMOVE_TOWING_CONTROLLER_ASSETS()
		
			PRINTLN("RETURNING TRUE ON EARLY FAIL CONDITIONS - TONYA IS DEAD")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF DO_AGGRO_CHECK(pedTonya, NULL, aggroArgs, aggroReason)
			KILL_ANY_CONVERSATION()
			SET_PED_KEEP_TASK(pedTonya, TRUE)
			TASK_SMART_FLEE_PED(pedTonya, PLAYER_PED_ID(), 1000, -1)
			PRINTLN("WE'VE AGGROED TONYA, MAKE HER RUN AWAY")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Handles the main update loop for the dynamic location towing.
///    This includes streaming, waiting for approaches, cleaning, etc.
PROC TOWING_DYNAMIC_MainUpdate(TOWING_STATE & eDynamicState, PED_INDEX playerPed, INT & iLastFinishTime, INT & iWaitTime, BOOL bIsTowingReplay)
	// Set the wait time to normal waits... This will be overridden when we get an offer.
	iWaitTime = TOWING_DEFAULT_WAIT_TIME
//	VEHICLE_INDEX tempVeh
//
//	MODEL_NAMES tempModel

	// Only run if the palyer isn't dead.
	IF NOT IS_ENTITY_DEAD(playerPed)
		VECTOR vTonya, vTruck
		FLOAT fTonya, fTruck
		
		IF NOT TOWING_DYNAMIC_CanWeLaunch(eDynamicState, FALSE, FALSE)
			/*
			IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_TOWING_TONYA)
				SET_STATIC_BLIP_ACTIVE_STATE (STATIC_BLIP_MINIGAME_TOWING_TONYA, FALSE)
				PRINTLN("TOP LEVEL: TURNING OFF STATIC BLIP - TONYA")
			ENDIF
			*/
		ENDIF
				
		TOWING_DYNAMIC_GetSpawnCoords(g_savedGlobals.sTowingData.iTowingJobsCompleted, vTruck, fTruck, vTonya, fTonya)
		
		// This will always handle the tow truck. -- Only want to run this at all times in Endless Summer
		IF (g_savedGlobals.sTowingData.iTowingJobsCompleted >= 5) OR bSpawnTruckAllowed
			TOWING_DYNAMIC_UPDATE_TRUCK(playerPed, vTruck, fTruck, eDynamicState)
			HANDLE_NO_JOBS_CONVO(eDynamicState, playerPed)
		ENDIF
		
		//Don't mess with the static blip states, they're configured from the minigame avilability flow flags
		//TOWING_DYNAMIC_UPDATE_BLIP(playerPed, fDist2, eDynamicState)
		
		#IF IS_DEBUG_BUILD
			// In debug mode, it's possible that we're being force launched from the debug menu.
			// If this is the case, then the player has been forced into a tow truck. Basically, 
			// we just need to throw him straight into a mission.
			IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_LaunchedViaDebug)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_LaunchedViaDebug)
				
//				PRINTLN("FORCING INTO MISSION VIA TOWING DYNAMIC LIB")
				
				// Kick the script off.
				TOWING_DYNAMIC_HandleMissionAccept()
				
				g_savedGlobals.sTowingData.iTowingJobsCompleted = 5
				
//				IF g_savedGlobals.sTowingData.iTowingJobsCompleted = 0
//					launchArgs.tutorialPed = pedTonya
//				ENDIF
					
				IF DOES_ENTITY_EXIST(vehTruck)
					SET_ENTITY_COORDS(vehTruck, <<408.8376, -1638.9524, 28.2928>>)
					SET_ENTITY_HEADING(vehTruck, 230.2387)
					
					launchArgs.tutorialTruck = vehTruck
					
					PRINTLN("vehTruck EXISTS SO PASSING THROUGH")
				ELSE
					REQUEST_MODEL(TOWTRUCK)
					
					WHILE NOT HAS_MODEL_LOADED(TOWTRUCK)
						WAIT(0)
						PRINTLN("WAITING ON TOW TRUCK TO LOAD")
					ENDWHILE
					
					launchArgs.tutorialTruck = CREATE_VEHICLE(TOWTRUCK, <<408.8376, -1638.9524, 28.2928>>, 230.2387)
					SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
					
					vehTruck = launchArgs.tutorialTruck
					PRINTLN("TOW TRUCK DID NOT EXIST, SO CREATING A NEW ONE")
				ENDIF
				
				IF DOES_ENTITY_EXIST(launchArgs.tutorialTruck)
					PRINTLN("GOING TO STATE - TOWING_LAUNCH VIA DEBUG LAUNCH")
					eDynamicState = TOWING_LAUNCH
				ENDIF
			ELSE
				IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA1)
					g_savedGlobals.sTowingData.iTowingJobsCompleted = 0
//					PRINTLN("SETTING IT TO LAUNCH TONYA 1")
					
					IF NOT bSetToInit
						eDynamicState = TOWING_INIT
//						PRINTLN("DEBUG: GOING TO STATE - TOWING_INIT")
						bSetToInit = TRUE
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA2)
					g_savedGlobals.sTowingData.iTowingJobsCompleted = 1
//					PRINTLN("SETTING IT TO LAUNCH TONYA 2")
					
					IF NOT bSetToInit
						eDynamicState = TOWING_INIT
//						PRINTLN("DEBUG: GOING TO STATE - TOWING_INIT")
						bSetToInit = TRUE
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA3)
					g_savedGlobals.sTowingData.iTowingJobsCompleted = 2
//					PRINTLN("SETTING IT TO LAUNCH TONYA 3")
					
					IF NOT bSetToInit
						ADD_CONTACT_TO_PHONEBOOK(CHAR_TOW_TONYA, FRANKLIN_BOOK)
					
						eDynamicState = TOWING_INIT
//						PRINTLN("DEBUG: GOING TO STATE - TOWING_INIT")
						bSetToInit = TRUE
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA4)
					g_savedGlobals.sTowingData.iTowingJobsCompleted = 3
//					PRINTLN("SETTING IT TO LAUNCH TONYA 4")
					
					IF NOT bSetToInit
						ADD_CONTACT_TO_PHONEBOOK(CHAR_TOW_TONYA, FRANKLIN_BOOK)
						
						eDynamicState = TOWING_INIT
//						PRINTLN("DEBUG: GOING TO STATE - TOWING_INIT")
						bSetToInit = TRUE
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA5)
					g_savedGlobals.sTowingData.iTowingJobsCompleted = 4
//					PRINTLN("SETTING IT TO LAUNCH TONYA 5")
					
					IF NOT bSetToInit
						eDynamicState = TOWING_INIT
//						PRINTLN("DEBUG: GOING TO STATE - TOWING_INIT")
						bSetToInit = TRUE
					ENDIF
				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_PROC)
					g_savedGlobals.sTowingData.iTowingJobsCompleted = 5
//					PRINTLN("SETTING IT TO LAUNCH TOWING PROCEDURAL")
					
					IF NOT bSetToInit
						eDynamicState = TOWING_INIT
//						PRINTLN("DEBUG: GOING TO STATE - TOWING_INIT")
						bSetToInit = TRUE
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
	
		SWITCH (eDynamicState)
			CASE TOWING_INIT
			
//				PRINTLN("INSIDE - TOWING_INIT")

				bSetToInit = bSetToInit
			
				// Any init here:
				SET_RANDOM_SEED(GET_CLOCK_SECONDS() * GET_CLOCK_MINUTES() * GET_CLOCK_HOURS())
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_NO_DELAY)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_10_DELAY)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_60_DELAY)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_MAX_DELAY)
						
				IF (g_savedGlobals.sTowingData.iTowingJobsCompleted >= 5)
					// Missions 5+ are the "endless summer" of towing missions.
					PRINTLN("GOING TO STATE -  TOWING_LAUNCH_WAIT")
					eDynamicState = TOWING_LAUNCH_WAIT
				ENDIF
			BREAK
			
			CASE TOWING_LAUNCH_WAIT
//				PRINTLN("TOWING_LAUNCH_WAIT")
			
				// Primary case. Here, we wait for the player to get into a car that isn't the static towing car.
				IF TOWING_DYNAMIC_CanWeLaunch(eDynamicState)
					IF TOWING_DYNAMIC_IsPlayerInTruck(playerPed)
						PRINTLN("TOWING_DYNAMIC_MainUpdate: GOING TO eDynamicState = TOWING_PRE_LAUNCH - 02")
						dynOffer = TOW_DYN_OFFR_WAIT_PRINT
						eDynamicState = TOWING_PRE_LAUNCH
					ENDIF
				ENDIF
			BREAK
			
			CASE TOWING_PRE_LAUNCH
			
//				PRINTLN("INSIDE - TOWING_PRE_LAUNCH")
			
				// Still, check to see if we're on a mission, and if so, kick us back.
				IF NOT TOWING_DYNAMIC_CanWeLaunch(eDynamicState)
					dynOffer = TOW_DYN_OFFR_WAIT_PRINT
					eDynamicState = TOWING_LAUNCH_WAIT
					PRINTLN("TOWING_DYNAMIC_MainUpdate: EARLY EXIT IN PRE-LAUNCH 01")
					EXIT
				ENDIF
				
				// This is the state we sit in receiving offers. 
				// First off, if we're not in the car, send us back.
				IF g_savedGlobals.sTowingData.iTowingJobsCompleted <> 0
					IF NOT TOWING_DYNAMIC_IsPlayerInTruck(playerPed)
						dynOffer = TOW_DYN_OFFR_WAIT_PRINT
						eDynamicState = TOWING_LAUNCH_WAIT
						PRINTLN("TOWING_DYNAMIC_MainUpdate: EARLY EXIT IN PRE-LAUNCH 02")
						//confirm radio is back on B*-2257265
						SET_FRONTEND_RADIO_ACTIVE(TRUE)
						EXIT
					ENDIF
				ENDIF
				
				// Adding in an additional check to see if we can launch to fix Bug # 642641 - DS
				IF TOWING_DYNAMIC_CanWeLaunch(eDynamicState)
					IF NOT bDebugPrint
//						PRINTLN("TOWING_DYNAMIC_MainUpdate: WE PASSED CHECK TO LAUNCH TOWING")
						bDebugPrint = TRUE
					ENDIF
				
					// Okay, we're still in the car. Manage our offers.
					TOWING_DYNAMIC_HandleOffer()
					iWaitTime = 0
					
					// Handle mission accept at any time!
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)	
					OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
					
						DISABLE_SELECTOR_THIS_FRAME()
						PRINTLN("TOWING: DISABLING SELECTOR - 01")
						
//						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//								tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//								IF NOT IS_ENTITY_DEAD(tempVeh)
//									IF (GET_PED_IN_VEHICLE_SEAT(tempVeh) = PLAYER_PED_ID())
//										tempModel = GET_ENTITY_MODEL(tempVeh)
//										IF (tempModel = TOWTRUCK) OR (tempModel = TOWTRUCK2)
//											SET_VEHICLE_LIGHTS(tempVeh, FORCE_VEHICLE_LIGHTS_OFF)
//											PRINTLN("TURNING OFF TOW TRUCK LIGHTS")
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
						
						DEBUG_MESSAGE("DYN_TOWING: IS_BUTTON_PRESSED(PAD1, LEFTSHOCK)")
						TOWING_DYNAMIC_HandleMissionAccept()
						
						launchArgs.tutorialPed = pedTonya
						launchArgs.tutorialTruck = vehTruck
						
						eDynamicState = TOWING_LAUNCH
					ELSE
//						PRINTLN("BUTTON PRESS NOT REGISTERING")
					ENDIF
				ENDIF
			BREAK
			
			CASE TOWING_LAUNCH
			
//				PRINTLN("INSIDE - TOWING_LAUNCH")

				DISABLE_SELECTOR_THIS_FRAME()
				PRINTLN("TOWING: DISABLING SELECTOR - 02")
				
				iWaitTime = 0
			
				TOWING_UPDATE_RETVAL launchRetVal
				launchRetVal = TOWING_DYNAMIC_Launch()
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_DEBUG_LAUNCH)
				
				PRINTLN("TOWING_DYNAMIC_MainUpdate: LAUNCHING SCRIPT")
				
				IF (launchRetVal = TOWING_RETVAL_PASS)
				
					IF NOT bIsTowingReplay
						Store_Minigame_Replay_Starting_Snapshot(GET_THIS_SCRIPT_NAME(), FALSE)
						PRINTLN("STORING TOWING SNAPSHOT")
					ENDIF
				
					dynOffer = TOW_DYN_OFFR_WAIT_PRINT
					eDynamicState = TOWING_RUN_WAIT
				
				ELIF (launchRetVal = TOWING_RETVAL_FAIL)
					TOWING_DYNAMIC_Cleanup()
					dynOffer = TOW_DYN_OFFR_WAIT_PRINT
					eDynamicState = TOWING_INIT
				ENDIF
			BREAK
			
			CASE TOWING_RUN_WAIT
			
//				PRINTLN("TOWING_RUN_WAIT")
				
				IF NOT IS_THREAD_ACTIVE(dynamicTowScript)	
					
					IF IS_BITMASK_AS_ENUM_SET(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_TowingFailed)
						PRINTLN("GOING TO STATE - TOWING_WAIT_FOR_FAILSCREEN_REPLAY")
						eDynamicState = TOWING_WAIT_FOR_FAILSCREEN_REPLAY
					ELSE
						// passed 
						CPRINTLN(DEBUG_MINIGAME, "Clearing the mission candidate!!")
						Mission_Over(iMissionCandidateID)
						Reset_All_Replay_Variables()
						
						CLEAR_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_TowingFailed)
					
						REMOVE_TOWING_CONTROLLER_ASSETS()
						TOWING_DYNAMIC_Cleanup()
						vehTruck = NULL
						pedTonya = NULL
						
						// We should wait a bit before a re-offer.
						IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_NO_DELAY)
							iDelay = 0
							PRINTLN("TOW_GLOBAL_NO_DELAY: DELAY EQUALS = ", iDelay)
						ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_10_DELAY)
							iDelay = 10000
							PRINTLN("TOW_GLOBAL_10_DELAY: DELAY EQUALS = ", iDelay)
						ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_60_DELAY)
							iDelay = 60000
							PRINTLN("TOW_GLOBAL_60_DELAY: DELAY EQUALS = ", iDelay)
						ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_MAX_DELAY)
							iDelay = 120000
							PRINTLN("TOW_GLOBAL_MAX_DELAY: DELAY EQUALS = ", iDelay)
						ELSE
							iDelay = 30000
							PRINTLN("DEFAULT: DELAY EQUALS = ", iDelay)
						ENDIF
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_NO_DELAY)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_10_DELAY)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_60_DELAY)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_MAX_DELAY)
						
						IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
							REMOVE_PED_FROM_GROUP(pedTonya)
						ENDIF
						
						IF DOES_BLIP_EXIST(blipTruck)
							REMOVE_BLIP(blipTruck)
							PRINTLN("REMOVING BLIP - blipTruck")
						ENDIF
						
						// Should really only do this if we actually succeeded the mission.
						//g_savedGlobals.sTowingData.iTowingJobsCompleted += 1
						
						iLastFinishTime = GET_GAME_TIMER()
							
						PRINTLN("GOING TO STATE - TOWING_POST_FINISH_DELAY")
						eDynamicState = TOWING_POST_FINISH_DELAY
					ENDIF
				ELSE
					SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_SouCent_01)
//					PRINTLN("SETTING TONYA MODEL AS NO LONGER NEEDED")
				ENDIF
			BREAK
			
			CASE TOWING_WAIT_FOR_FAILSCREEN_REPLAY
//				PRINTLN("TOWING_WAIT_FOR_FAILSCREEN_REPLAY")
			
				IF (g_replay.replayStageID = RS_ACTIVE)
					CPRINTLN(DEBUG_MINIGAME, "Got replay is active.")
					
					IF g_bLaunchMinigameReplay
						PRINTLN("Flag set, replay starting. Replay script name: ", g_replay.replayScriptName)
						bIsTowingReplay = TRUE
						
						IF mnModelToUse = TOWTRUCK
							REQUEST_MODEL(TOWTRUCK)
							PRINTLN("REQUESTING MODEL - TOWTRUCK")
						ELIF mnModelToUse = TOWTRUCK2
							REQUEST_MODEL(TOWTRUCK2)
							PRINTLN("REQUESTING MODEL - TOWTRUCK2")
						ELSE
							REQUEST_MODEL(TOWTRUCK)
							PRINTLN("DEFAULT: REQUESTING MODEL - TOWTRUCK")
							mnModelToUse = TOWTRUCK
						ENDIF
						
						REQUEST_SCRIPT("towing")
						
						IF g_savedGlobals.sTowingData.iTowingJobsCompleted >= 5
							IF HAS_MODEL_LOADED(mnModelToUse)
						
								CLEAR_AREA_OF_VEHICLES(<<408.8376, -1638.9524, 28.2928>>, 8.0)
								IF NOT DOES_ENTITY_EXIST(vehTruck)
								
									IF mnModelToUse = TOWTRUCK
										PRINTLN("TOWING_WAIT_FOR_FAILSCREEN_REPLAY: mnModelToUse = TOWTRUCK")
									ELIF mnModelToUse = TOWTRUCK2
										PRINTLN("TOWING_WAIT_FOR_FAILSCREEN_REPLAY: mnModelToUse = TOWTRUCK2")
									ENDIF
								
									vehTruck = CREATE_VEHICLE(mnModelToUse, <<408.8376, -1638.9524, 28.2928>>, 230.2387)
									SET_MODEL_AS_NO_LONGER_NEEDED(mnModelToUse)
									
									PRINTLN("CREATING TOW TRUCK - 02")
								ENDIF
								
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(vehTruck)
									SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTruck)
									PRINTLN("SETTING THE PLAYER INTO THE TRUCK")
								
									SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<408.8376, -1638.9524, 28.2928>>)
									SET_ENTITY_HEADING(vehTruck, 230.2387)
									
									PRINTLN("TOWING_WAIT_FOR_FAILSCREEN_REPLAY -> TOWING_LAUNCH - POST TONYA")
									eDynamicState = TOWING_LAUNCH
								ENDIF
							ELSE
								PRINTLN("WAITING ON MODELS TO LOAD")
							ENDIF
						ELSE
							PRINTLN("TOWING_WAIT_FOR_FAILSCREEN_REPLAY -> TOWING_LAUNCH")
							eDynamicState = TOWING_LAUNCH
						ENDIF
					ENDIF
				ELIF (g_replay.replayStageID = RS_NOT_REQUIRED)
					CPRINTLN(DEBUG_MINIGAME, "Replay refused...")
					CPRINTLN(DEBUG_MINIGAME, "Clearing the mission candidate!!")
					Mission_Over(iMissionCandidateID)
					Reset_All_Replay_Variables()
					bIsTowingReplay = FALSE
					
					iLastFinishTime = GET_GAME_TIMER()
					eDynamicState = TOWING_POST_FINISH_DELAY
				ENDIF
			BREAK
			
			CASE TOWING_POST_FINISH_DELAY
	
				// Wait before we re-offer, or if the player isn't in the truck, get us ready to re-offer.
				IF ((GET_GAME_TIMER() - iLastFinishTime) > iDelay)
					iLastFinishTime = 0
					
					PRINTLN("GOING TO STATE - TOWING_INIT")
					eDynamicState = TOWING_INIT
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC
