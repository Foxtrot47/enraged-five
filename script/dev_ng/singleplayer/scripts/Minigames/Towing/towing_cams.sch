USING "script_oddjob_funcs.sch"
USING "Towing.sch"

FUNC BOOL SETUP_SPECIAL_CAM(TOW_CAM_ARGS& towArgs, ENTITY_INDEX entity, INT& iToggle)
	DESTROY_CAM(towArgs.towCam)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	VECTOR vAttach
	VECTOR vPoint
	FLOAT fFov
	MODEL_NAMES mnBrokenDownVehicle
	
	UNUSED_PARAMETER(iToggle)
 	SWITCH chosenNodeType
		CASE NODE_TYPE_DYNAMIC
			vAttach = <<-1.4812, -0.4765, 0.7772>> //<<-2.0617, -0.0898, 1.0993>>
			vPoint = <<0.6369, 1.5805, 0.2461>> //<<-0.1480, 2.0858, 0.3219>>
			fFov = 50
		BREAK
		CASE NODE_TYPE_HANDI //and sexxxx
			//outside of car
			//IF iToggle = 0
				vAttach = <<0.4088, 2.5854, 1.3483>>
				vPoint = <<-0.1605, -0.2434, 0.5274>>
				fFov = 50
				//iToggle = 1
//			ELSE 
//				//insdie of car
//				vAttach = <<-0.4883, -2.1660, 1.1279>>
//				vPoint = <<0.3760, 0.6225, 0.4367>>
//				fFov = 52.3985	
//				iToggle = 0
//			ENDIF
		BREAK
		CASE NODE_TYPE_ABANDON
			mnBrokenDownVehicle = GET_ENTITY_MODEL(entity)
			
			SWITCH mnBrokenDownVehicle
				CASE buccaneer
					vAttach = <<0.2323, 0.4607, 0.6705>>
					vPoint = <<1.2216, -2.3210, 0.1387>>
					PRINTLN("MODEL = BUCCANEER")
				BREAK
				//---------------------------------------
				CASE FUTO
					vAttach = <<0.2335, 0.4782, 0.6074>>
					vPoint = <<1.2177, -2.2207, -0.2570>>
					PRINTLN("MODEL = FUTO")
				BREAK
				//---------------------------------------
				CASE MANANA
					vAttach = <<0.2692, 0.2233, 0.5622>>
					vPoint = <<1.1703, -2.3976, -0.5863>>
					PRINTLN("MODEL = MANANA")
				BREAK
				//---------------------------------------
				CASE RUINER
					vAttach = <<0.3012, 0.2936, 0.3770>>
					vPoint = <<0.9845, -2.4433, -0.6440>>
					PRINTLN("MODEL = RUINER")
				BREAK
				//---------------------------------------
				CASE TORNADO3
					vAttach = <<0.3196, 0.6089, 0.4949>>
					vPoint = <<0.8955, -2.2021, -0.3804>>
					PRINTLN("MODEL = TORNADO3")
				BREAK
				//---------------------------------------
				CASE VOODOO2
					vAttach = <<0.3016, 0.5664, 0.5030>>
					vPoint = <<0.7218, -2.1898, -0.6047>>
					PRINTLN("MODEL = VOODOO2")
				BREAK
				//---------------------------------------
				CASE SURFER2
					vAttach = <<0.1942, 1.6207, 0.6794>>
					vPoint = <<0.9982, -1.1537, -0.1309>>
					PRINTLN("MODEL = SURFER2")
				BREAK
				//---------------------------------------
				CASE EMPEROR2
					vAttach = <<0.2288, 0.5263, 0.6031>>
					vPoint = <<1.3152, -2.0483, -0.4883>>
					PRINTLN("MODEL = EMPEROR2")
				BREAK
				//---------------------------------------
				CASE STANIER
					vAttach = <<0.2013, 0.6176, 0.5514>>
					vPoint = <<1.0234, -2.1522, -0.2563>>
					PRINTLN("MODEL = STANIER")
				BREAK
				//---------------------------------------
				CASE TAILGATER
					vAttach = <<0.2166, 0.6582, 0.5162>>
					vPoint = <<1.2456, -2.0704, -0.1882>>
					PRINTLN("MODEL = TAILGATER")
				BREAK
				//---------------------------------------
				
				DEFAULT
					vAttach = <<-0.0255, 0.4396, 0.6165>>
					vPoint = <<2.1178, -1.5317, -0.1047>>
					PRINTLN("USING DEFAULT")
				BREAK
			ENDSWITCH
			
			fFov = 61.4303
		BREAK
		CASE NODE_TYPE_ACCIDENT
			vAttach = <<3.1631, 4.1912, 1.4362>>
			vPoint = <<1.1460, 2.1282, 0.6146>>
			fFov = 45
		BREAK
	ENDSWITCH
	
	
	IF NOT DOES_CAM_EXIST(towArgs.towCam)
		towArgs.towCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, fFov, FALSE)
	ENDIF
		
	ATTACH_CAM_TO_ENTITY(towArgs.towCam, entity, vAttach)
	POINT_CAM_AT_ENTITY(towArgs.towCam, entity, vPoint)
	
	SET_CAM_CONTROLS_MINI_MAP_HEADING(towArgs.towCam, TRUE)

	SET_CAM_ACTIVE(towArgs.towCam, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
	RETURN TRUE
ENDFUNC

//Camera that looks at the train from inside the stalled car
FUNC BOOL SETUP_TRAIN_CAM(TOW_CAM_ARGS& towArgs, ENTITY_INDEX entity, ENTITY_INDEX train, INT& iToggle, BOOL bDisablePedCam)
	VECTOR vTemp, vTemp2
	
	
	FLOAT fDistance
	DEBUG_MESSAGE("SETUP_TRAIN_CAM")
	DESTROY_CAM(towArgs.towCam)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
	//TODO if the vehicle isn't flying.. don't do bomb cam.
	//IF EVALUATE_DROP_CONDITIONS(cargoArgs)
		IF NOT DOES_CAM_EXIST(towArgs.towCam)
			towArgs.towCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, 45, FALSE)
			SET_CAM_CONTROLS_MINI_MAP_HEADING(towArgs.towCam, TRUE)
		ENDIF
		
		IF bDisablePedCam
			IF iToggle = 0
				iToggle = 1
			ENDIF
		ENDIF
		
		IF iToggle = 0
			PRINTLN("iToggle = 0")
			vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entity, <<0.8612, 0.2242, 0.6459>>)
			fDistance = GET_ENTITY_DISTANCE_FROM_LOCATION(train, vTemp)
			vTemp2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entity, <<-1.0507, -0.0044, 0.6659>>)
			
			IF fDistance < GET_ENTITY_DISTANCE_FROM_LOCATION(train, vTemp2)
				ATTACH_CAM_TO_ENTITY(towArgs.towCam, entity, <<-1.0507, -0.0044, 0.6659>>)
				//SET_CAM_COORD(towArgs.towCam, vTemp2)
				POINT_CAM_AT_ENTITY(towArgs.towCam, entity, <<1.9171, 0.4113, 0.5271>>)
				SET_CAM_FOV(towArgs.towCam, 45.0000)
				
				// Stop train scene if it's running
				IF IS_AUDIO_SCENE_ACTIVE("TOWING_FOCUS_CAM_INSIDE_TRAIN")
					STOP_AUDIO_SCENE("TOWING_FOCUS_CAM_INSIDE_TRAIN")
					PRINTLN("STOPPING AUDIO SCENE - TOWING_FOCUS_CAM_INSIDE_TRAIN")
				ENDIF
				
				// Start inside care if it's not running
				IF NOT IS_AUDIO_SCENE_ACTIVE("TOWING_FOCUS_CAM_INSIDE_CAR")
					START_AUDIO_SCENE("TOWING_FOCUS_CAM_INSIDE_CAR")
					PRINTLN("STARTING AUDIO SCENE - TOWING_FOCUS_CAM_INSIDE_CAR")
				ENDIF
			ELSE
				ATTACH_CAM_TO_ENTITY(towArgs.towCam, entity, <<0.8612, 0.2242, 0.6459>>)
				//SET_CAM_COORD(towArgs.towCam, vTemp)
				POINT_CAM_AT_ENTITY(towArgs.towCam, entity, <<-2.1158, -0.0459, 0.3911>>)
				SET_CAM_FOV(towArgs.towCam, 50.0000)
				
				// Stop train scene if it's running
				IF IS_AUDIO_SCENE_ACTIVE("TOWING_FOCUS_CAM_INSIDE_TRAIN")
					STOP_AUDIO_SCENE("TOWING_FOCUS_CAM_INSIDE_TRAIN")
					PRINTLN("STOPPING AUDIO SCENE - TOWING_FOCUS_CAM_INSIDE_TRAIN")
				ENDIF
				
				// Start inside care if it's not running
				IF NOT IS_AUDIO_SCENE_ACTIVE("TOWING_FOCUS_CAM_INSIDE_CAR")
					START_AUDIO_SCENE("TOWING_FOCUS_CAM_INSIDE_CAR")
					PRINTLN("STARTING AUDIO SCENE - TOWING_FOCUS_CAM_INSIDE_CAR")
				ENDIF
			ENDIF
			iToggle++
		ELIF iToggle = 1
			PRINTLN("iToggle = 1")
			ATTACH_CAM_TO_ENTITY(towArgs.towCam, train, <<-0.0021, 6.5179, 3.2083>>)
			POINT_CAM_AT_ENTITY(towArgs.towCam, train, <<0.0107, 9.1227, 1.7200>>)
			SET_CAM_FOV(towArgs.towCam, 67.9193)
			iToggle = 0
			
			IF IS_AUDIO_SCENE_ACTIVE("TOWING_FOCUS_CAM_INSIDE_CAR")
				STOP_AUDIO_SCENE("TOWING_FOCUS_CAM_INSIDE_CAR")
				PRINTLN("STOPPING AUDIO SCENE INSIDE CAR - 02")
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("TOWING_FOCUS_CAM_INSIDE_TRAIN")
				START_AUDIO_SCENE("TOWING_FOCUS_CAM_INSIDE_TRAIN")
				PRINTLN("GOING INSIDE TRAIN")
			ENDIF
		ENDIF
		
		SET_CAM_ACTIVE(towArgs.towCam, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
		towArgs.iCamTime = GET_GAME_TIMER()
		
		RETURN TRUE
ENDFUNC

PROC CLEANUP_CAM(TOW_CAM_ARGS& towArgs)
	towArgs.iCamTime = 0
	DESTROY_CAM(towArgs.towCam)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
ENDPROC


