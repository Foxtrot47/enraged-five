//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     : Towing                                     		            //
//      AUTHOR          : Steven Messinger                                              //
//      DESCRIPTION     : Core logic for the tow truck oddjob       					//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "Towing.sch"
USING "towing_cams.sch"
USING "end_screen.sch"
USING "minigame_midsized_message.sch"
USING "load_queue_public.sch"

SCRIPT_SHARD_BIG_MESSAGE endMessage

STRING angryPedRoot = ""
//STRING angryPedLine = ""
STRING SpecialAnimDict = "REACTION@MALE_STAND@BIG_VARIATIONS@A"

//TODO these should be linked in a struct with the peds that need them
STRING SpecialAnims[MAX_NUM_BYSTANDERS]
STRING sCharacterName

VECTOR vCurDestObjective[MAX_NUM_NODES]
VECTOR vDest //used by traditional towing mode to set a destinatino
VECTOR vMin, vMax

//END_SCREEN_DATASET towingEndScreen

//current size is 7
STREAMED_MODEL streamReqs[9]
STREAMED_ANIM streamReqsAnim[7]

LoadQueueLarge sLoadQueue

#IF IS_DEBUG_BUILD
	DEBUG_POS_DATA myDebugData
	VECTOR vStartVec = << 1387.9126, -2535.6133, 47.4669 >>
	FLOAT fStartFloat
	BOOL bDebugInTutorial = FALSE
#ENDIF

//BOOL bFirstTime = TRUE

BOOL bTriggeredEarly = FALSE
BOOL bPrintHook = TRUE
BOOL bPrintDest = TRUE
BOOL bPrintHintCam = TRUE
BOOL bTrainClose = FALSE
BOOL bRammedVehicle = FALSE
BOOL bTaskedBrokenDownPed = FALSE
BOOL bPlayedSound = FALSE
BOOL bGrabbedSoundID = FALSE
BOOL bUnlockDoors = FALSE
BOOL bStopTimer = FALSE
BOOL bDisplayHelp = TRUE
BOOL bAggroedTonya = FALSE
BOOL bAbandonedTonya = FALSE
BOOL bSkipTutorial = FALSE
BOOL bPostTonya = FALSE
BOOL bPlayTooCloseConvo = FALSE
//BOOL bEnableBonusCam = TRUE
BOOL bDisablePedCam = FALSE
BOOL bDisableCin = FALSE
BOOL bInCombat = FALSE
BOOL bSpawnPed = FALSE
BOOL bTimeWarned = FALSE
BOOL bDistWarned = FALSE
BOOL bReverseOrder = FALSE
BOOL bDisableCam = FALSE
BOOL bRunningTwoOrFive = FALSE
BOOL bRunningThreeOrFour = FALSE
BOOL bSupressObjective = FALSE
BOOL bSuppressBlips = FALSE
BOOL bDoTutorial = FALSE 
BOOL bTrainsDisabled = FALSE
BOOL bRequiresPed = FALSE
BOOL bStreamPeds = FALSE
BOOL bGenDisabled = FALSE
BOOL bAttack = FALSE
BOOL bSexVariation = FALSE
BOOL bHomelessVariation = FALSE
BOOL bGpsActive = FALSE
BOOL bTonyaAbandonWarning = FALSE
BOOL bReplayVehicleAvailable = FALSE
BOOL bDriverGotCarBack = FALSE
BOOL bAngryPedGaveUp = FALSE
BOOL bWrongCarAttached = FALSE
BOOL bPlayInitialUpsetLine = FALSE
BOOL bPlayFullConvo = FALSE
BOOL bPrintSpecialCaseObj = FALSE
BOOL bShakeCar = TRUE
BOOL bPlayBeckonLine = FALSE
BOOL bReattachConvo = FALSE
BOOL bWakeUp = FALSE


//TEXT_LABEL_23 TestResumptionLabel

PED_INDEX piSpeakers[2]
PED_INDEX pedTrainDriver
PED_INDEX pedTonya

BLIP_INDEX myVehicleBlip
BLIP_INDEX tempBlip

//TOW_NODE towNodes[MAX_NUM_NODES]

MODEL_NAMES carNames[MAX_NUM_CAR_NAMES]
MODEL_NAMES pedNames[MAX_NUM_PED_NAMES]

VARIATION towVariation = VARIATION_TRADIITONAL

TOW_CAM_ARGS towArgs

REL_GROUP_HASH grpBuddies

BROKEN_VEHICLE brokenVehicles[MAX_NUM_BROKEN_CARS]
GARAGE_DATA myGarageData

INT curTowIndex = -1
INT curBrokenCarCount = 0
INT curCarNameIdx = 0
INT iToggle = 0
INT fPanicDist = 0
//Car health info
INT iLastHealth = 0
INT iHealthDelta = 0
INT iInitialHealth = 0
//Tow Truck health info
INT iTruckLastHealth = 0
INT iTruckHealthDelta = 0
INT iClosestIdx
INT iSpeedNode01
INT iSpeedNode02
INT iAdditionalHelpPrintStages
INT iNumPeds = 0
INT outOfVehicleTime = 0
INT iPushStages = 0

VECTOR vExtra
VECTOR vExtra2
VECTOR vCarGenPosition = <<-229.8159, -1171.9999, 21.8557>>

VECTOR getPointVec
FLOAT getPointHead
FLOAT fMissionTime

TEXT_LABEL_63 sLastObjective

FAIL_REASON_ENUM failReason

structTimer printDelayTimer
structTimer	genericTimer
structTimer	totalTimer
structTimer printTimer
structTimer chaseTimer
structTimer failTimer
structTimer wakeTimer
//structTimer tEndingTimer
structTimer tAdditionalHelpPrintTimer
//INT iAllowSkipCutsceneTime = 0

VEHICLE_INDEX myVehicle
VEHICLE_INDEX helperCar
PED_INDEX helperPed

SCENARIO_BLOCKING_INDEX sbi01, sbi02


//INT iGpsIndex = -1

STRING sMyObjective
//STRING sMyHelp

MISSION_STATE curStage = MISSION_STATE_INTRO
STAGING_STATE stagingState = STAGING_STATE_INIT
BROKEN_STATE brokenState = BROKEN_STATE_VISIBLE
ANGRY_PED pedState = ANGRY_PED_INIT
TRAIN_STATE trainState = TRAIN_STATE_PRINT
TRAIN_STATE_PED trainPedState = TRAIN_STATE_PED_INIT
TOW_PED_DIALOUGE pedDialougeState = TOW_PED_BEG

DAMAGE_PRINTS printState = DAMAGE_PRINTS_FIRST
DAMAGE_PRINTS truckPrintState = DAMAGE_PRINTS_FIRST
GIVEUP_ARGS giveUpArgs

PTFX_ID sparksEffects[8]


#IF NOT IS_JAPANESE_BUILD
	BOOL bPlaySexyConvo = FALSE
	BOOL bGuyTryingToEnterCar = FALSE
#ENDIF

#IF IS_JAPANESE_BUILD
	INT iPedID
#ENDIF


PROC MISSION_CLEANUP()

	SET_DEFAULT_RICH_PRESENCE_FOR_SP()
	PRINTLN("SETTING RICH PRESENCE BACK TO DEFAULT")
	
	// To Fix Bug # 554724
	CLEAR_HELP()
	
	CLEAR_ADDITIONAL_TEXT(ODDJOB_TEXT_SLOT, TRUE) 
	
	IF HAS_SCALEFORM_MOVIE_LOADED(endMessage.siMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(endMessage.siMovie)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(myVehicle)
		SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_SIREN(myVehicle, FALSE)
	ENDIF
	
	REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedNode01)
	REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedNode02)
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbi01)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi02)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA((vCarGenPosition - <<15,15,15>>), (vCarGenPosition + <<15,15,15>>), TRUE)
	
	INT idx
	REPEAT MAX_NUM_BROKEN_CARS idx
		//IF brokenVehicles[openIdx].speedIDA
		REMOVE_ROAD_NODE_SPEED_ZONE(brokenVehicles[idx].speedIDA) 
		REMOVE_ROAD_NODE_SPEED_ZONE(brokenVehicles[idx].speedIDB) 
		REMOVE_ROAD_NODE_SPEED_ZONE(brokenVehicles[idx].speedIDC)
	ENDREPEAT
	
	IF NOT IS_VECTOR_ZERO(extraData[GET_IDX()].vMin)
		ALLOW_PEDS_TO_BE_IN_AREA(sblockingIndex, extraData[GET_IDX()].vMin, extraData[GET_IDX()].vMax)
	ENDIF
	
	SET_VEHICLE_AS_NO_LONGER_NEEDED(brokenVehicles[0].vehicleIndex)
	
	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
		SET_PED_AS_NO_LONGER_NEEDED(brokenVehicles[0].pedIndex)
		PRINTLN("SETTING PED - brokenVehicles[0].pedIndex AS NO LONGER NEEDED")
		
		IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
			IF IS_PED_IN_GROUP(brokenVehicles[0].pedIndex)
				REMOVE_PED_FROM_GROUP(brokenVehicles[0].pedIndex)
				PRINTLN("REMOVING PED FROM GROUP")
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedTrainDriver)
		IF NOT IS_ENTITY_DEAD(pedTrainDriver)
			SET_PED_AS_NO_LONGER_NEEDED(pedTrainDriver)
			PRINTLN("SETTING PED - pedTrainDriver AS NO LONGER NEEDED")
		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(myVehicle)
		SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(myVehicle)
		PRINTLN("SETTING TOW TRUCK AS NO LONGER NEEDED VIA MISSION CLEANUP")
	ENDIF
	
	//Turn back on car spawning
	IF bGenDisabled
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMin, vMax, TRUE)
	ENDIF

	// cleanup anything you need to. You don't need to dereference script-created entities like peds and vehicles,
	// as code will do this automatically when the script terminates.
	IF bTrainsDisabled AND NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex)
		SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(brokenVehicles[0].blockingIndex, TRUE)
		SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(brokenVehicles[0].blockingIndex)
		SET_RANDOM_TRAINS(TRUE)
		
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_MAIN, TRUE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_QUARRY, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_CAR_STEAL_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_METRO, TRUE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_FIRST_HOOD_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_SECOND_HOOD_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_SECOND_FAME_OR_SHAME_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_DOWNTOWN_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_THIRD_HOOD_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_PROLOGUE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_JEWEL_HEIST_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_PROLOGUE_2, FALSE)
		
		PRINTLN("PUTTING TRAINS BACK TO INITIALIZATION VALUES")
	ENDIF	
	
	ODDJOB_STOP_SOUND(soundID)
	ODDJOB_STOP_SOUND(soundID2)
	STOP_BREAKING_PFX(sparksEffects)
	ODDJOB_STOP_SOUND(soundID3)
				
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	IF DOES_CAM_EXIST(towArgs.towCam) 
		CLEANUP_CAM(towArgs)
	ENDIF
	
	//CLEANUP_MINIGAME_INSTRUCTIONS(helpScaleform)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, FALSE)
	PRINTLN("TERMINATING THREAD")
	TERMINATE_THIS_THREAD() // important, kills the script
ENDPROC

PROC MISSION_FAILED()

	//Don't allow fail if the mission passed UI is up
	IF curStage < MISSION_STATE_ENDUI_WAIT
	
		STRING strFailReason
		
		// anything that should happen when mission fails
		CLEAR_PRINTS()
		STOP_SCRIPTED_CONVERSATION(TRUE)
		
		PRINTLN("FAILING MISSION")
		
		// This sets us up for a replay through the launcher.
		SET_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_TowingFailed)
	
		// will usually print a line of god text on fail describing the reason
		SWITCH failReason
			CASE FAIL_TRANSPORT_DESTROYED
//				PRINT_NOW("TOW_FAIL_05", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_05"
			BREAK
			CASE FAIL_ABANDONED_CAR
//				PRINT_NOW("TOW_FAIL_10", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_10"
			BREAK
			CASE FAIL_TIME_EXPIRED
//				PRINT_NOW("DTRSHRD_FAIL_03", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "DTRSHRD_FAIL_03"
			BREAK
			CASE FAIL_DISTANCE_FAIL
//				PRINT_NOW("TOW_FAIL_08", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_08"
			BREAK
			CASE FAIL_ABANDONED_JOB
//				PRINT_NOW("TOW_FAIL_08", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_08"
			BREAK
			CASE FAIL_DRIVER_GOT_CAR_BACK
//				PRINT_NOW("TOW_FAIL_16", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_16"
			BREAK
			CASE FAIL_VEHICLE_DISABLED
//				PRINT_NOW("TOW_FAIL_12", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_12"
			BREAK
			CASE FAIL_TOW_DESTROYED
//				PRINT_NOW("TOW_FAIL_01", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_01"
			BREAK
//			CASE FAIL_COMMUTE_TIME
//				PRINT_WITH_NUMBER("", FLOOR(MAX_COMMUTE), DEFAULT_GOD_TEXT_TIME, 1)
//			BREAK
			CASE FAIL_WANTED
//				PRINT_NOW("TOW_FAIL_03", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_03"
			BREAK
			CASE FAIL_PED_KILLED
//				PRINT_NOW("TOW_FAIL_04", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_04"
			BREAK
			CASE FAIL_PED_KILLED_PASSENGER
//				PRINT_NOW("TOW_FAIL_04a", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_04a"
			BREAK
			CASE FAIL_TIME_EXPIRED_TOTAL
//				PRINT_NOW("TOW_FAIL_06", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_06"
			BREAK
			CASE FAIL_DAMAGE
//				PRINT_NOW("TOW_FAIL_07", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_07"
			BREAK 
			CASE FAIL_PED_ATTACKED
//				PRINT_NOW("TOW_FAIL_09", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_09"
			BREAK
			CASE FAIL_PED_ATTACKED_TRAMP
//				PRINT_NOW("TOW_FAIL_09a", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_09a"
			BREAK
			CASE FAIL_PED_THREATENED
//				PRINT_NOW("TOW_FAIL_02", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_02"
			BREAK
			CASE FAIL_ATTACKED_TOW_TRUCK_OWNER
//				PRINT_NOW("TOW_FAIL_11", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_11"
			BREAK
			CASE FAIL_ATTACKED_TONYA
//				PRINT_NOW("TOW_FAIL_13", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_13"
			BREAK
			CASE FAIL_ABANDONED_TONYA
//				PRINT_NOW("TOW_FAIL_14", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_14"
			BREAK
		ENDSWITCH
		
		IF IS_ENTITY_DEAD(PLAYER_PED_ID())
			PRINTLN("PLAYER IS DEAD")
			strFailReason = "TOW_FAIL_15"
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(strFailReason)
			MISSION_FLOW_SET_FAIL_REASON(strFailReason)
		ENDIF
		SET_FORCE_CLEANUP_FAIL_REASON()
		
		PRINTLN("Setting us up for a replay!")
		Setup_Minigame_Replay(GET_THIS_SCRIPT_NAME())
		
		// Wait for replay controller to fade the screen out
		WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			WAIT(0)
		ENDWHILE
		
		MISSION_CLEANUP()
	ENDIF
ENDPROC

PROC SET_AGGRO_FIELDS_TO_NOT_CHECK()
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Aiming)
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Name:  CREATE_CAR
//Purpose: Creates a car at INT openIdx in the vehicle array brokenVehicles
/////////////////////////////////////////////////////////////////////////////////////////////////////////
FUNC BOOL CREATE_CAR(VECTOR spawnVec, FLOAT fOrient, INT& openIdx, BOOL bDoDamaged = TRUE, BOOL bBlip = TRUE)
	DEBUG_MESSAGE("CREATE_CAR")
	openIdx = FIND_FIRST_OPEN(brokenVehicles)
	
	FLOAT fClosestCheck, fMiddleCheck, fOuterCheck
	
	IF openIdx = -1
		SCRIPT_ASSERT("There is no open spot in the broken Vehicle array")
		RETURN FALSE
	ENDIF

	IF curCarNameIdx >= MAX_NUM_PED_NAMES
		curCarNameIdx = 0
	ENDIF
	
	PRINTLN("Trying to create car enum:", carNames[curCarNameIdx])

	brokenVehicles[openIdx].vehicleIndex = CREATE_VEHICLE(carNames[curCarNameIdx], spawnVec, fOrient)
	SET_ENTITY_LOAD_COLLISION_FLAG(brokenVehicles[openIdx].vehicleIndex, TRUE)
	PRINTLN("SETTING COLLISION FLAG ON CAR")
	
	curCarNameIdx++
	
	fObjDist = GET_PLAYER_DISTANCE_FROM_LOCATION(spawnVec, FALSE)
	
	IF chosenNodeType = NODE_TYPE_TRAIN
		fClosestCheck = 20
		fMiddleCheck = 40
		fOuterCheck = 60
	ELSE
		fClosestCheck = 4
		fMiddleCheck = 20
		fOuterCheck = 40
	ENDIF
	
	PRINTLN("fClosestCheck = ", fClosestCheck)
	PRINTLN("fMiddleCheck = ", fMiddleCheck) 
	PRINTLN("fOuterCheck = ", fOuterCheck)
	
	IF NOT IS_VECTOR_ZERO(illegalNodes[illegalIdx].vSpeedNodeLocation)
		brokenVehicles[openIdx].speedIDA = ADD_ROAD_NODE_SPEED_ZONE(illegalNodes[illegalIdx].vSpeedNodeLocation, fClosestCheck, 0)
	ELSE
		brokenVehicles[openIdx].speedIDA = ADD_ROAD_NODE_SPEED_ZONE(spawnVec, fClosestCheck, 0)
	ENDIF
	
	brokenVehicles[openIdx].speedIDB = ADD_ROAD_NODE_SPEED_ZONE(spawnVec, fMiddleCheck, 2.5)
	brokenVehicles[openIdx].speedIDC = ADD_ROAD_NODE_SPEED_ZONE(spawnVec, fOuterCheck, 4)
		
	//SET_ROADS_IN_AREA(vMin, vMax, FALSE)
	//SCRIPT_ASSERT("Turning off roads in area")
	//ADD_BLIP_FOR_COORD(vMin)
	//ADD_BLIP_FOR_COORD(vMax)
	
	IF bDoDamaged
		IF brokenVehicles[openIdx].bDoFire
			//-100 is a hood fire
			SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(brokenVehicles[openIdx].vehicleIndex, TRUE)
			SET_VEHICLE_ENGINE_HEALTH(brokenVehicles[openIdx].vehicleIndex, 1)
			SET_DISABLE_VEHICLE_ENGINE_FIRES(brokenVehicles[openIdx].vehicleIndex, TRUE)
			PRINTLN("bDoFire IS TRUE")
		ELSE
			SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(brokenVehicles[openIdx].vehicleIndex, TRUE)
			SET_VEHICLE_ENGINE_HEALTH(brokenVehicles[openIdx].vehicleIndex, 1)
			SET_DISABLE_VEHICLE_ENGINE_FIRES(brokenVehicles[openIdx].vehicleIndex, TRUE)
			PRINTLN("bDoFire IS FALSE")
			//front
			//SET_VEHICLE_DAMAGE(brokenVehicles[openIdx].vehicleIndex, <<0.0, 1.2, 0.0>>, 1600.0, 1600.0, TRUE)
		ENDIF
		
		//windshield
		//SET_VEHICLE_DAMAGE(brokenVehicles[openIdx].vehicleIndex, <<0.0, 0.75, 0.05>>, 1600.0, 1600.0, TRUE)
		//left window
		//SET_VEHICLE_DAMAGE(brokenVehicles[openIdx].vehicleIndex, <<-0.7, 0.0, 0.0>>, 1600.0, 1600.0, TRUE)
		//right window
		//SET_VEHICLE_DAMAGE(brokenVehicles[openIdx].vehicleIndex, <<0.7, 0.0, 0.0>>, 1600.0, 1600.0, TRUE)
		//front
		//SET_VEHICLE_DAMAGE(brokenVehicles[openIdx].vehicleIndex, <<0.0, 1.2, 0.0>>, 1600.0, 1600.0, TRUE)
						
		//We don't want to add a blip if we're patrol and currently towing
	ENDIF
	
	IF NOT bSuppressBlips AND bBlip
		IF DOES_ENTITY_EXIST(brokenVehicles[openIdx].vehicleIndex)
			brokenVehicles[openIdx].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[openIdx].vehicleIndex)
			//SET_BLIP_SCALE(brokenVehicles[openIdx].blipIndex, 0.75)
			SET_BLIP_COLOUR(brokenVehicles[openIdx].blipIndex, BLIP_COLOUR_BLUE)
		ENDIF
	ENDIF
	curBrokenCarCount++	
	//totalBrokenCarCount++
	
	START_TIMER_NOW(brokenVehicles[openIdx].carTimer)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL EXTRA_HOOK_CHECK()
	IF bDoTutorial 
		IF bHookPlayed AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CHECK_TOW_ATTACHMENTS(STRING objective, VECTOR vDestination)
	BOOL bProcede = TRUE
	INT idx
	
	IF NOT bPostTonya AND NOT bRunningThreeOrFour
		IF DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
			
			ENTITY_INDEX vehHookedCar
			vehHookedCar = GET_ENTITY_ATTACHED_TO_TOW_TRUCK(myVehicle)
		
			IF DOES_ENTITY_EXIST(vehHookedCar) AND NOT IS_ENTITY_DEAD(vehHookedCar)
				IF IS_ENTITY_A_VEHICLE(vehHookedCar)
					IF GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(vehHookedCar) <> brokenVehicles[0].vehicleIndex
						IF NOT bWrongCarAttached
							ADD_PED_FOR_DIALOGUE(pedConvo, 3, pedTonya, "TONYA")
						
							IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_WRONG", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("bWrongCarAttached = TRUE")
								bWrongCarAttached = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				bWrongCarAttached = FALSE
			ENDIF
		ENDIF
	ENDIF

	REPEAT MAX_NUM_BROKEN_CARS idx
		IF DOES_ENTITY_EXIST(brokenVehicles[idx].vehicleIndex)
			IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[idx].vehicleIndex) 
				IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[idx].vehicleIndex) AND EXTRA_HOOK_CHECK()
					
//					SET_VEHICLE_LIGHTS(myVehicle, FORCE_VEHICLE_LIGHTS_ON)
					SET_VEHICLE_SIREN(myVehicle, TRUE)

					CLEAR_TEXT_ON_HOOKING()
					
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					bTowHooked = TRUE
					IF bRequiresPed AND NOT IS_PED_IN_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle)
						bProcede = FALSE
						PRINTLN("bProcede = FALSE")
						//We are hooked skip ahead and get the ped in the car
						IF trainPedState < TRAIN_STATE_PED_PENDING
							SETTIMERB(0)
							DEBUG_MESSAGE("Setting timer b to 0")
							trainPedState = TRAIN_STATE_PED_ENTER_TRUCK
						ENDIF
					ENDIF
					IF bProcede
						PRINTLN("bProcede = TRUE")
					
						//don't print bjective if we jsut hooked
						CLEAR_PRINT_DELAY(printTimer)
						SET_BLIP_ROUTE(brokenVehicles[idx].blipIndex, FALSE)
						bGpsActive = FALSE
						SCRIPT_REMOVE_BLIP_SAFE(brokenVehicles[idx].blipIndex)
						
						brokenVehicles[idx].pickupTime = GET_TIMER_IN_SECONDS(brokenVehicles[idx].carTimer)
						
						//TODO it's preferable to not have any specific checks in here...
						IF towVariation = VARIATION_TRADIITONAL	
							brokenVehicles[idx].blipIndex = ADD_BLIP_FOR_COORD(vDestination)
							fObjDist = GET_PLAYER_DISTANCE_FROM_LOCATION(vDestination, FALSE)
							PRINTLN("ATTEMPTING TO ADD BLIP")
							vCurDestObjective[0] = vDestination
						ENDIF
						
						IF chosenNodeType = NODE_TYPE_ACCIDENT
							SET_VEHICLE_DOOR_SHUT(brokenVehicles[0].vehicleIndex, SC_DOOR_FRONT_LEFT, FALSE)
							PRINTLN("CLOSING DOOR ON ACCIDENT VEHICLE")
						ENDIF
						
						RESTART_TIMER_NOW(failTimer)
						
						SET_BLIP_COLOUR(brokenVehicles[idx].blipIndex, BLIP_COLOUR_YELLOW)
						SET_BLIP_ROUTE(brokenVehicles[idx].blipIndex, TRUE)						
						
						IF chosenNodeType = NODE_TYPE_HANDI AND bSexVariation
							// SPECIAL CASE - HANDLED IN HANDICAP VARIATION						
						ELSE
							IF bPrintDest
								PRINT_NOW(objective, DEFAULT_GOD_TEXT_TIME, 1)
								bPrintDest = FALSE
							ENDIF
						ENDIF
						
						sLastObjective = objective
						CLEAR_AREA_OF_VEHICLES(vCurDestObjective[0], 6.0)
					
						curTowIndex = idx
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_IN_DROP_OFF_AREA(VECTOR& vAngledA, VECTOR& vAngledB, FLOAT& fWidth)
	IF curTowIndex <> -1
		//If this location has a process zone require the player to be in it
		IF NOT IS_VECTOR_ZERO(myGarageData.vProcessZone)
			vDest = myGarageData.vProcessZone
			//PRINTLN("Useing vDropZoneA because processizng zone was non o:", myGarageData.vDropZoneA)
			vAngledA = myGarageData.vDropZoneA
			vAngledB = myGarageData.vDropZoneB
			fWidth = myGarageData.fDropZoneW
		ELSE
			//PRINTLN("Useing vCoordinate01 because processizng zone was o:", myGarageData.vCoordinate01)
			vAngledA = myGarageData.vCoordinate01
			vAngledB = myGarageData.vCoordinate02
			fWidth = myGarageData.fWidth
		ENDIF
		
		IF NOT bDoTutorial
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDest, <<30,30,LOCATE_SIZE_HEIGHT>>, TRUE)
					// Do nothing... need this for the chevron
//					PRINTLN("DOING CHECK FOR VEHICLE INSIDE DROP OFF")
					IF chosenNodeType = NODE_TYPE_HANDI OR chosenNodeType = NODE_TYPE_ACCIDENT
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
							SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_UNLOCKED)
							PRINTLN("UNLOCKING DOORS ON BROKEN DOWN CAR")
							bUnlockDoors = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(brokenVehicles[curTowIndex].vehicleIndex)
			IF IS_ENTITY_IN_ANGLED_AREA(brokenVehicles[curTowIndex].vehicleIndex, vAngledA, vAngledB, fWidth, FALSE, FALSE)
//			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()  - To Fix Bug # 971799, no need to wait until the conversation is done
				
				IF bDisplayHelp
					PRINT_HELP("TOWT_HELP_UH")
//					PRINTLN("PRINTING HELP 1")
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[curTowIndex].vehicleIndex)
					IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[curTowIndex].vehicleIndex) //OR NOT bDoTutorial 
					OR (IS_PED_IN_VEHICLE(PLAYER_PED_ID(), brokenVehicles[curTowIndex].vehicleIndex) AND NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[curTowIndex].vehicleIndex))
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_ANY_CONVERSATION()
						ENDIF
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_TOWING(STRING sObjective)
	//INT idx
	
	VECTOR vAngledA
	VECTOR vAngledB
	FLOAT fWidth
	INT iRand
	
	IF curTowIndex = -1
		SCRIPT_ASSERT("curTowIndex = -1")
	ENDIF

	IF brokenVehicles[curTowIndex].bDoFire
		IF (GET_TIMER_IN_SECONDS(brokenVehicles[curTowIndex].carTimer) - brokenVehicles[curTowIndex].pickupTime) > 120.0
			EXPLODE_VEHICLE(brokenVehicles[curTowIndex].vehicleIndex)
		ENDIF
	ENDIF
	
	//Stop banter when you arrive at the destination
//	IF IS_ENTITY_IN_ANGLED_AREA(brokenVehicles[curTowIndex].vehicleIndex, myGarageData.vCoordinate01, myGarageData.vCoordinate02, myGarageData.fWidth, FALSE, FALSE)
//		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//			STOP_SCRIPTED_CONVERSATION(FALSE)
//		ENDIF
//	ENDIF
//	
	IF IS_VEHICLE_IN_DROP_OFF_AREA(vAngledA, vAngledB, fWidth)
		IF NOT IS_ENTITY_DEAD(brokenVehicles[curTowIndex].pedIndex) AND IS_PED_IN_VEHICLE(brokenVehicles[curTowIndex].pedIndex, myVehicle)
			BRING_VEHICLE_TO_HALT(MyVehicle, 6, 2)
			
			bDropComplete = TRUE
				
//			VECTOR COORD = GET_ENTITY_COORDS(brokenVehicles[curTowIndex].vehicleIndex)
			
			IF DOES_ENTITY_EXIST(brokenVehicles[curTowIndex].pedIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[curTowIndex].pedIndex)
				// Peds for Broken Down and Train Track Clearing
				OPEN_SEQUENCE_TASK(taskSequence)
					TASK_CLEAR_LOOK_AT(NULL)
					TASK_LEAVE_ANY_VEHICLE(NULL)
//					IF DOES_SCENARIO_EXIST_IN_AREA(COORD, 50, TRUE)
//						TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, COORD, 50, -1)
//						PRINTLN("TASKING PED TO USE SCENARIO")
//					ELSE
//						TASK_WANDER_STANDARD(NULL)
//						PRINTLN("TASKING PED TO GO TO COORDINATE")
//					ENDIF
					TASK_GOTO_ENTITY_OFFSET(NULL, brokenVehicles[0].vehicleIndex, DEFAULT_TIME_BEFORE_WARP, 2.0, 0, PEDMOVE_WALK)
					
					iRand = GET_RANDOM_INT_IN_RANGE() % 3
					IF iRand = 0
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
						PRINTLN("USING - WORLD_HUMAN_STAND_MOBILE")
					ELIF iRand = 1
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
						PRINTLN("USING - WORLD_HUMAN_SMOKING")
					ELIF iRand = 2
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_IMPATIENT", 0, TRUE)
						PRINTLN("USING - WORLD_HUMAN_STAND_IMPATIENT")
					ELSE
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
						PRINTLN("USING - WORLD_HUMAN_STAND_MOBILE")
					ENDIF
					
				CLOSE_SEQUENCE_TASK(taskSequence)
				TASK_PERFORM_SEQUENCE(brokenVehicles[curTowIndex].pedIndex, taskSequence)
				CLEAR_SEQUENCE_TASK(taskSequence)
				
				SET_PED_KEEP_TASK(brokenVehicles[curTowIndex].pedIndex, TRUE)
			ENDIF
			
			// Tonya
			IF g_savedGlobals.sTowingData.iTowingJobsCompleted = 0
			OR g_savedGlobals.sTowingData.iTowingJobsCompleted = 1
			OR g_savedGlobals.sTowingData.iTowingJobsCompleted = 4
				IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
					OPEN_SEQUENCE_TASK(taskSequence)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_WANDER_STANDARD(NULL, 227.0820)
					CLOSE_SEQUENCE_TASK(taskSequence)
					IF NOT IS_ENTITY_DEAD(pedTonya)
						TASK_PERFORM_SEQUENCE(pedTonya, taskSequence)
						SET_PED_KEEP_TASK(pedTonya, TRUE)
						PRINTLN("TASKING TONYA TO EXIT")
					ENDIF
					CLEAR_SEQUENCE_TASK(taskSequence)
				ELSE
					PRINTLN("TONYA SHOULD EXIT, BUT SHE DOESN'T")
				ENDIF
			ELSE
				PRINTLN("TONYA SHOULD NOT EXIST")
			ENDIF
			
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINTLN("KILLING CONVERSATION, SO WE CAN HEAR THANK YOU LINE.")
				KILL_ANY_CONVERSATION()
			ENDIF
		
			IF g_savedGlobals.sTowingData.iTowingJobsCompleted = 0
			OR g_savedGlobals.sTowingData.iTowingJobsCompleted = 1
			OR g_savedGlobals.sTowingData.iTowingJobsCompleted = 4
				IF g_savedGlobals.sTowingData.iTowingJobsCompleted = 0
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TUT_ENDA", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
					PRINTLN("PLAYING FIRST TONYA GOODBYE")
				ELIF g_savedGlobals.sTowingData.iTowingJobsCompleted = 1
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TUT_ENDB", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
					PRINTLN("PLAYING SECOND TONYA GOODBYE")
				ELIF g_savedGlobals.sTowingData.iTowingJobsCompleted = 4
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TUT_ENDC", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
					PRINTLN("PLAYING THIRD TONYA GOODBYE")
				ENDIF
			ELSE
				IF chosenNodeType = NODE_TYPE_DYNAMIC
					IF g_savedGlobals.sTowingData.iBreakDownCompleted = 0
					OR g_savedGlobals.sTowingData.iBreakDownCompleted = 1
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWBREAKM")
						CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_BRK_THNK", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES
						PRINTLN("g_savedGlobals.sTowingData.iBreakDownCompleted = ", g_savedGlobals.sTowingData.iBreakDownCompleted)
						PRINTLN("PLAYING BROKEN DOWN, THANK YOU LINE.")
					ELIF g_savedGlobals.sTowingData.iBreakDownCompleted = 2
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWBREAKHIPM")
						CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_BRK_THAN", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES
						PRINTLN("g_savedGlobals.sTowingData.iBreakDownCompleted = ", g_savedGlobals.sTowingData.iBreakDownCompleted)
						PRINTLN("PLAYING BROKEN DOWN, THANK YOU LINE.")
					ELSE
//						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWBREAKM")
//						// NEED - Generic "Thank you."
//						CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_BRK_THNK", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES
//						PRINTLN("g_savedGlobals.sTowingData.iBreakDownCompleted = ", g_savedGlobals.sTowingData.iBreakDownCompleted)
//						PRINTLN("PLAYING BROKEN DOWN, THANK YOU LINE.")
					ENDIF
				ELIF chosenNodeType = NODE_TYPE_TRAIN
					IF g_savedGlobals.sTowingData.iTrainCompleted = 0
					OR g_savedGlobals.sTowingData.iTrainCompleted = 1
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWTRAINF")
						CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_THANK", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES
						PRINTLN("g_savedGlobals.sTowingData.iTrainCompleted = ", g_savedGlobals.sTowingData.iTrainCompleted)
						PRINTLN("PLAYING TRAIN TRACK CLEARING, THANK YOU LINE.")
					ELIF g_savedGlobals.sTowingData.iTrainCompleted = 2
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWILLEGALMAN2")
						CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_THANK2", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES
						PRINTLN("g_savedGlobals.sTowingData.iTrainCompleted = ", g_savedGlobals.sTowingData.iTrainCompleted)
						PRINTLN("PLAYING TRAIN TRACK CLEARING, THANK YOU LINE.")
					ELSE
//						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWTRAINM")
//						// NEED - Generic "Thank you."
//						CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TRAIN_TK", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES
//						PRINTLN("g_savedGlobals.sTowingData.iTrainCompleted = ", g_savedGlobals.sTowingData.iTrainCompleted)
//						PRINTLN("PLAYING TRAIN TRACK CLEARING, THANK YOU LINE.")
					ENDIF
				ENDIF
			ENDIF

		ELSE
			//BRING_VEHICLE_TO_HALT(PLAYER_PED_ID(), MyVehicle, 6, 1)
		ENDIF
		
//		PRINT_NOW("TOW_OBJ_04", DEFAULT_GOD_TEXT_TIME, 1)
		
		IF NOT bDoTutorial
			//DETACH_VEHICLE_FROM_TOW_TRUCK(myVehicle, brokenVehicles[curTowIndex].vehicleIndex)
		ENDIF
		
		bTowHooked = FALSE
		REMOVE_BLIP(brokenVehicles[curTowIndex].blipIndex)
		//brokenVehicles[curTowIndex].vehicleIndex = NULL
		RESTART_TIMER_NOW(genericTimer)
		curBrokenCarCount--
		RETURN TRUE
	ENDIF

	//Check for tow breaking
	//IF NOT bUnlockDoors
		IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[curTowIndex].vehicleIndex)
			IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[curTowIndex].vehicleIndex)
				bTowHooked = FALSE
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				SET_BLIP_ROUTE(brokenVehicles[curTowIndex].blipIndex, FALSE)
				REMOVE_BLIP(brokenVehicles[curTowIndex].blipIndex)
				
				IF DOES_ENTITY_EXIST(brokenVehicles[curTowIndex].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[curTowIndex].vehicleIndex)
					IF NOT DOES_BLIP_EXIST(brokenVehicles[curTowIndex].blipIndex)
						brokenVehicles[curTowIndex].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[curTowIndex].vehicleIndex)
						SET_BLIP_COLOUR(brokenVehicles[curTowIndex].blipIndex, BLIP_COLOUR_BLUE)
					ENDIF
				ENDIF
					
//				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					TestResumptionLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
//					PRINTLN("KILLING ANY CONVERSATION THAT MIGHT BE ONGOING")
//					KILL_FACE_TO_FACE_CONVERSATION()
//					
//					bInterrupt = TRUE
//					PRINTLN("bInterrupt = TRUE")
//				ENDIF
				
				IF chosenNodeType = NODE_TYPE_TRAIN
					IF g_savedGlobals.sTowingData.iTrainCompleted = 0
					OR g_savedGlobals.sTowingData.iTrainCompleted = 1
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWTRAINF")
					
						IF NOT bReattachConvo
							IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_UNHOOK", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								PRINTLN("bReattachConvo = TRUE")
								bReattachConvo = TRUE
							ENDIF
						ENDIF
					ELIF g_savedGlobals.sTowingData.iTrainCompleted = 2
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWILLEGALMAN2")
					
						IF NOT bReattachConvo
							IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_UNHOOK2", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								PRINTLN("bReattachConvo = TRUE")
								bReattachConvo = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELIF chosenNodeType = NODE_TYPE_DYNAMIC
					IF g_savedGlobals.sTowingData.iBreakDownCompleted = 0
					OR g_savedGlobals.sTowingData.iBreakDownCompleted = 1
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWBREAKM")
					
						IF NOT bReattachConvo
							IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_BRK_UNHK", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								PRINTLN("bReattachConvo = TRUE")
								bReattachConvo = TRUE
							ENDIF
						ENDIF
					ELIF g_savedGlobals.sTowingData.iBreakDownCompleted = 2
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWBREAKHIPM")
						
						IF NOT bReattachConvo
							IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_BRK_HOOK", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								PRINTLN("bReattachConvo = TRUE")
								bReattachConvo = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				RESTART_TIMER_NOW(genericTimer)
				IF NOT bSupressObjective	
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						STOP_SCRIPTED_CONVERSATION (TRUE)
						PRINTLN("STOPPING CONVERSATION TO PRINT REHOOK OBJECTIVE")
						CLEAR_PRINTS()
						PRINT_NOW(sObjective, DEFAULT_GOD_TEXT_TIME, 1)
					ENDIF
			
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF bPrintHook
							CLEAR_PRINTS()
							PRINT_NOW(sObjective, DEFAULT_GOD_TEXT_TIME, 1)
							bPrintHook = FALSE
							PRINTLN("PRINTING OBJECTIVE TO REATTACH - 01")
						ELSE
							CLEAR_PRINTS()
							PRINT_NOW(sObjective, DEFAULT_GOD_TEXT_TIME, 1)
							PRINTLN("PRINTING OBJECTIVE TO REATTACH - 02")
						ENDIF
						
						sLastObjective = sObjective
					ENDIF
				ELSE
					PRINTLN("bSupressObjective IS TRUE")
				ENDIF
				//bFirstTime = FALSE
				curStage = MISSION_STATE_TOWING
			ENDIF
		ENDIF
	//ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_ALL_BLIPS(INT exceptMe)
	bSuppressBlips = TRUE
	INT idx
	REPEAT MAX_NUM_BROKEN_CARS idx
		IF idx <> exceptMe
			IF DOES_BLIP_EXIST(brokenVehicles[idx].blipIndex)
				REMOVE_BLIP(brokenVehicles[idx].blipIndex)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC ADD_ALL_BLIPS()
	bSuppressBlips = FALSE
	INT idx
	REPEAT MAX_NUM_BROKEN_CARS idx
		IF DOES_ENTITY_EXIST(brokenVehicles[idx].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[idx].vehicleIndex)
			brokenVehicles[idx].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[idx].vehicleIndex)
			SET_BLIP_COLOUR(brokenVehicles[idx].blipIndex, BLIP_COLOUR_BLUE)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_PLAYER_BUMPED_ENTITY(ENTITY_INDEX myEntity)
	VEHICLE_INDEX vehicle
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ENDIF
	
	IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), myEntity) OR IS_ENTITY_TOUCHING_ENTITY(vehicle, myEntity)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_MOVING_WITH_BROKEN_VEHICLE()
	FLOAT fTempSpeed
	
	IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
		IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
			fTempSpeed = GET_ENTITY_SPEED(myVehicle)
//			PRINTLN("TOW TRUCK SPEED = ", fTempSpeed)
			
			IF fTempSpeed > 1.0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_BROKEN_VEHICLE_PED(BROKEN_STATE& myState, INT idx, STRING audioLine) //VEHICLE_INDEX vehicle, PED_INDEX myPed)

	audioLine = audioLine

	SWITCH myState
		CASE BROKEN_STATE_VISIBLE
			DEBUG_MESSAGE("BROKEN_STATE_VISIBLE")
			PRINTLN("UPDATE_BROKEN_VEHICLE_PED: idx = ", idx)
			
			IF NOT bSuppressBlips
				IF SHOULD_TRIGGER_ENTITY(brokenVehicles[idx].vehicleIndex, myVehicle, myFrameCount, 10, 150, 4, TRUE) OR bReverseOrder OR bTowHooked
					IF NOT bTaskedBrokenDownPed
						myFrameCount = 0
						ODDJOB_STOP_SOUND(soundID3)
						DEBUG_MESSAGE("Script Conversation")
						DEBUG_MESSAGE("Clear ped tasks")
						
						IF GET_SCRIPT_TASK_STATUS(brokenVehicles[idx].pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
							TASK_TURN_PED_TO_FACE_ENTITY(brokenVehicles[idx].pedIndex, PLAYER_PED_ID(), -1)
							PRINTLN("TASKING PED TO FACE ENTITY")
						ENDIF
			
						bTaskedBrokenDownPed = TRUE
					ENDIF
					
					IF bTaskedBrokenDownPed
						IF IS_ENTITY_AT_COORD(myVehicle, GET_ENTITY_COORDS(brokenVehicles[idx].pedIndex), <<25,25,25>>)
							
							IF IS_ENTITY_PLAYING_ANIM(brokenVehicles[idx].pedIndex, "oddjobs@towingcome_here", "come_here_idle_a")
								STOP_ANIM_TASK(brokenVehicles[idx].pedIndex, "oddjobs@towingcome_here", "come_here_idle_a", SLOW_BLEND_OUT)
							ENDIF
							
							IF NOT IS_PLAYER_MOVING_WITH_BROKEN_VEHICLE()
								IF chosenNodeType = NODE_TYPE_TRAIN
									IF g_savedGlobals.sTowingData.iTrainCompleted = 0
									OR g_savedGlobals.sTowingData.iTrainCompleted = 1
										
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[idx].pedIndex, "TOWTRAINF")
										CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_GREET", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
										
										PRINTLN("PROCEDURAL TOWING TRAIN 0 AND 1")
									ELIF g_savedGlobals.sTowingData.iTrainCompleted = 2
										
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[idx].pedIndex, "TOWILLEGALMAN2")
										CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_GREET2", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
									
										PRINTLN("PROCEDURAL TOWING TRAIN 2")
									ELSE
										PRINTLN("PROCEDURAL TOWING TRAIN DEFAULT")
									ENDIF
									
									TASK_ENTER_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_BLOCK_SEAT_SHUFFLING | ECF_RESUME_IF_INTERRUPTED)
								ELIF chosenNodeType = NODE_TYPE_DYNAMIC
									IF g_savedGlobals.sTowingData.iBreakDownCompleted = 0
									OR g_savedGlobals.sTowingData.iBreakDownCompleted = 1
										
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[idx].pedIndex, "TOWBREAKM")
										CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_BRK_GRT", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
									
										PRINTLN("PROCEDURAL TOWING BROKEN 0 AND 1")
									ELIF g_savedGlobals.sTowingData.iBreakDownCompleted = 2
										
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[idx].pedIndex, "TOWBREAKHIPM")
										CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_BRK_GRT2", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
									
										PRINTLN("PROCEDURAL TOWING BROKEN 2")
									ELSE		
										PRINTLN("PROCEDURAL TOWING DEFAULT")
									ENDIF
									
									TASK_ENTER_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_BLOCK_SEAT_SHUFFLING | ECF_RESUME_IF_INTERRUPTED)
								ENDIF
									
								IF bReverseOrder
									REMOVE_BLIP(brokenVehicles[idx].blipIndex)
									IF DOES_ENTITY_EXIST(brokenVehicles[idx].pedIndex)
										brokenVehicles[idx].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[idx].pedIndex)
										SET_BLIP_COLOUR(brokenVehicles[idx].blipIndex, BLIP_COLOUR_BLUE)
										SET_BLIP_SCALE(brokenVehicles[idx].blipIndex, BLIP_SIZE_PED)
									ENDIF
								ENDIF
								//in train we ned to do this only
								//remove blip from vehicle and blip the ped
								SETTIMERA(0)
								myState = BROKEN_STATE_PED_TALK
							ENDIF
						ELIF IS_ENTITY_AT_COORD(myVehicle, GET_ENTITY_COORDS(brokenVehicles[idx].pedIndex), <<50,50,50>>)
							IF NOT bPlayBeckonLine
								IF g_savedGlobals.sTowingData.iBreakDownCompleted = 0
								OR g_savedGlobals.sTowingData.iBreakDownCompleted = 1
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[idx].pedIndex, "TOWBREAKM")
									
									IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_CALLING", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
										bPlayBeckonLine = TRUE
									ENDIF
								ELIF g_savedGlobals.sTowingData.iBreakDownCompleted = 2
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[idx].pedIndex, "TOWBREAKHIPM")
									
									IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_BRK_CALL", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
										bPlayBeckonLine = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				DEBUG_MESSAGE("bSuppressBlips is true")
			ENDIF
		BREAK
		CASE BROKEN_STATE_PED_TALK
			DEBUG_MESSAGE("BROKEN_STATE_PED_TALK")
			
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND TIMERA() > 3000
				bStopTimer = TRUE

				RESTART_TIMER_NOW(failTimer)
				RESTART_TIMER_NOW(printDelayTimer)
				SCRIPT_REMOVE_BLIP_SAFE(brokenVehicles[idx].blipIndex)
				IF NOT DOES_BLIP_EXIST(brokenVehicles[0].blipIndexPed)
					IF DOES_ENTITY_EXIST(brokenVehicles[idx].pedIndex)
						brokenVehicles[0].blipIndexPed = ADD_BLIP_FOR_ENTITY(brokenVehicles[idx].pedIndex)
						SET_BLIP_COLOUR(brokenVehicles[0].blipIndexPed, BLIP_COLOUR_BLUE)
						SET_BLIP_SCALE(brokenVehicles[0].blipIndexPed, BLIP_SIZE_PED)
					ENDIF
				ENDIF
				PRINT_NOW("TOWT_OBJ_02", DEFAULT_GOD_TEXT_TIME, 1)
				DEBUG_MESSAGE("Tasking player to enter tow truck")
				myState = BROKEN_STATE_PED_ENTER
//			ENDIF
		BREAK
		CASE BROKEN_STATE_PED_ENTER
			DEBUG_MESSAGE("BROKEN_STATE_PED_ENTER")
			//brokenVehicles[idx].bPedTasked = TRUE
			//CLEAR_PED_TASKS(brokenVehicles[idx].pedIndex)
			ADD_RELATIONSHIP_GROUP("TOWBUDDIES", grpBuddies)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, grpBuddies, RELGROUPHASH_PLAYER)
//			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), grpBuddies)
			SET_PED_RELATIONSHIP_GROUP_HASH(brokenVehicles[idx].pedIndex, grpBuddies)

//			bEnableBonusCam = FALSE
			
			REMOVE_ALL_BLIPS(idx)
//			IF bReverseOrder
//				myState = BROKEN_STATE_HOOKING
//			ELSE
				myState = BROKEN_STATE_DIALOUGE
//			ENDIF
		BREAK
		CASE BROKEN_STATE_DIALOUGE
			//DEBUG_MESSAGE("BROKEN_STATE_DIALOUGE")
			IF NOT IS_PED_IN_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle) 
				IF UPDATE_GIVE_UP(giveUpArgs, brokenVehicles[idx].pedIndex)
					PRINTLN("Pringing abandon job because UPDATE_GIVE_UP returned true")
					failReason = FAIL_ABANDONED_JOB
					MISSION_FAILED()
				ENDIF
			ENDIF
			
//			DEBUG_MESSAGE("BROKEN_STATE_DIALOUGE")
			IF IS_PED_IN_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle)
				IF NOT IS_ENTITY_DEAD(myVehicle)
					SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_LOCKED_NO_PASSENGERS)
					SCRIPT_REMOVE_BLIP_SAFE(brokenVehicles[0].blipIndexPed)
				ENDIF
			ENDIF
			
			IF IS_PED_IN_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle) AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF chosenNodeType = NODE_TYPE_TRAIN
					IF g_savedGlobals.sTowingData.iTrainCompleted = 0
					OR g_savedGlobals.sTowingData.iTrainCompleted = 1
						IF bReverseOrder
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWTRAINF")
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_INTRUCK", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						ELSE
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWTRAINF")
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_INTRUCK", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						ENDIF
					ELIF g_savedGlobals.sTowingData.iTrainCompleted = 2
						IF bReverseOrder
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWILLEGALMAN2")
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_GETIN", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						ELSE
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWILLEGALMAN2")
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_GETIN", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						ENDIF
					ENDIF
					
					myState = BROKEN_STATE_HOOKING
				ELIF chosenNodeType = NODE_TYPE_DYNAMIC
					IF g_savedGlobals.sTowingData.iBreakDownCompleted = 0
					OR g_savedGlobals.sTowingData.iBreakDownCompleted = 1
						IF bReverseOrder
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWBREAKM")
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_BRK_STRT", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						ELSE
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWBREAKM")
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_BRK_STRT", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						ENDIF
					ELIF g_savedGlobals.sTowingData.iBreakDownCompleted = 2
						IF bReverseOrder
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWBREAKHIPM")
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_BRK_STR2", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						ELSE
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWBREAKHIPM")
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_BRK_STR2", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						ENDIF
					ENDIF
					
					myState = BROKEN_STATE_HOOKING
				ENDIF
			ENDIF
		BREAK
		CASE BROKEN_STATE_HOOKING
			DEBUG_MESSAGE("BROKEN_STATE_HOOKING")
			IF IS_PED_IN_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle) //AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				//At this point we need to de-blip the rest of the cars
				REMOVE_ALL_BLIPS(idx)
				
				IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					IF bReverseOrder AND IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
						PRINTLN("bReverseOrder AND bTowHooked returning true.")
						RETURN TRUE
					ENDIF
				ENDIF

				IF NOT DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
					IF DOES_ENTITY_EXIST(brokenVehicles[idx].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[idx].vehicleIndex)
						brokenVehicles[0].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[idx].vehicleIndex)
						SET_BLIP_COLOUR(brokenVehicles[idx].blipIndex, BLIP_COLOUR_BLUE)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
						PRINT_NOW("TOWT_OBJ_03", DEFAULT_GOD_TEXT_TIME, 1)
						sLastObjective = "TOWT_OBJ_03"				
					ENDIF
				ENDIF
				
				RESTART_TIMER_NOW(printDelayTimer)
				myState = BROKEN_STATE_VISIBLE
				RETURN TRUE
				PRINTLN("**TRUE!!")
			ENDIF
		BREAK

	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_BROKEN_VEHICLE_ILLEGAL(BOOL bWithPed = FALSE, BOOL bBlip = FALSE)
	INT num = 0
	IF NOT CREATE_CAR(illegalNodes[illegalIdx].vCarPos, illegalNodes[illegalIdx].fCarRot, num, FALSE, bBlip)
		RETURN FALSE
	ENDIF
	fObjDist = GET_PLAYER_DISTANCE_FROM_LOCATION(illegalNodes[illegalIdx].vCarPos, FALSE)
	vCurDestObjective[0] = illegalNodes[illegalIdx].vCarPos
	IF illegalNodes[illegalIdx].myType = NODE_TYPE_ABANDON	
		//front
		SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<0.0, 1.2, 0.0>>, 1600.0, 1600.0, TRUE)
		//windshield
		SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<0.0, 0.75, 0.05>>, 1600.0, 1600.0, TRUE)
		//left window
		SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<-0.7, 0.0, 0.0>>, 1600.0, 1600.0, TRUE)
		//right window
		SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<0.7, 0.0, 0.0>>, 1600.0, 1600.0, TRUE)
	ELIF illegalNodes[illegalIdx].myType = NODE_TYPE_ACCIDENT	
		//DOORS
		//SET_VEHICLE_DOORS_BROKEN 
//		SCRIPT_ASSERT("here")
		getPointHead = -1.0
		bSpawnPed = TRUE //we want peds to be spawned

		//Need to create the ambulance as well.
		helperCar = CREATE_VEHICLE(AMBULANCE, extraData[GET_IDX()].vTrafficPos, extraData[GET_IDX()].fTrafficOrient)
		IF NOT IS_ENTITY_DEAD(helperCar)
			SET_VEHICLE_SIREN(helperCar, TRUE)
			SET_VEHICLE_ENGINE_ON(helperCar, TRUE, TRUE)
			helperPed = CREATE_PED_INSIDE_VEHICLE(helperCar, PEDTYPE_CIVMALE, S_M_M_PARAMEDIC_01) 
		ENDIF	
		
		IF NOT IS_VECTOR_ZERO(extraData[GET_IDX()].vMin)
			sblockingIndex = PREVENT_PEDS_FROM_BEING_IN_AREA(extraData[GET_IDX()].vMin, extraData[GET_IDX()].vMax)
		ENDIF
		
		SWITCH illegalNodes[illegalIdx].myDamage
			CASE DAMAGE_FRONT_LEFT
				//Front Left
				SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<-0.5223, 2.4550, 0.0784>>, 200, 250, TRUE)
				SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<-1.0,1.5,-0.5>>, 200, 250, TRUE)
			BREAK
			CASE DAMAGE_FRONT_RIGHT
				//Front Right
				SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<0.6012, 2.4222, 0.0245>>, 200, 250, TRUE)
				SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<1.0,1.5,-0.5>>, 200, 250, TRUE)
			BREAK
			CASE DAMAGE_HEAD_ON
				SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<0.1414, 1.8401, 0.0342>>, 200, 350, TRUE)
			BREAK
		ENDSWITCH
		
		SET_VEHICLE_DOOR_OPEN(brokenVehicles[0].vehicleIndex, SC_DOOR_FRONT_LEFT)
		
	ENDIF
	
	IF bWithPed
		bPedCreated = TRUE
	
		IF g_savedGlobals.sTowingData.iTrainCompleted = 0
		OR g_savedGlobals.sTowingData.iTrainCompleted = 1
			brokenVehicles[0].pedIndex = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].vehicleIndex, PEDTYPE_MISSION,  A_F_M_BEVHILLS_01, VS_DRIVER)
			PRINTLN("1ST TRAIN EVENT: PROCEDURAL - PED CREATION - SHOULD BE FEMALE")
		ELIF g_savedGlobals.sTowingData.iTrainCompleted = 2
			brokenVehicles[0].pedIndex = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].vehicleIndex, PEDTYPE_MISSION,  A_M_Y_GENSTREET_02, VS_DRIVER)
			PRINTLN("2ND TRAIN EVENT: PROCEDURAL - PED CREATION - SHOULD BE MALE")
		ELSE
			PRINTLN("bPedCreated = TRUE - 01")
			brokenVehicles[0].pedIndex = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].vehicleIndex, PEDTYPE_MISSION,  pedNames[0], VS_DRIVER)
			PRINTLN("CREATING PED INSIDE - CREATE_BROKEN_VEHICLE_ILLEGAL")
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL CREATE_BROKEN_VEHICLE_ACCIDENT(BOOL bBlip = FALSE)
	INT num = 0
	IF NOT CREATE_CAR(illegalNodes[illegalIdx].vCarPos, illegalNodes[illegalIdx].fCarRot, num, FALSE, bBlip)
		RETURN FALSE
	ENDIF
	fObjDist = GET_PLAYER_DISTANCE_FROM_LOCATION(illegalNodes[illegalIdx].vCarPos, FALSE)
	vCurDestObjective[0] = illegalNodes[illegalIdx].vCarPos
	


	RETURN TRUE
ENDFUNC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//NAME: CREATE_BROKEN_DOWN_VEHICLE
//Description: Calls the comm shared CREATE_CAR but does anything custom needed by the Traditional Towing Mode
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
FUNC BOOL CREATE_BROKEN_DOWN_VEHICLE(VECTOR spawnVec, FLOAT fOrient, INT& openIdx, BOOL bBlip = FALSE)

	VECTOR vTemp, vModelMin, vModelMax // vEngineOffset
	VECTOR vDimensions
	VECTOR vBrokenDownVehiclePosition
	FLOAT fBrokenDownVehicleHeading
		
	//Created a care at openIdx in the vehicle array brokenVehicles
	IF NOT CREATE_CAR(spawnVec, fOrient, openIdx, TRUE, bBlip)
		RETURN FALSE
	ENDIF
	
	DEBUG_MESSAGE("CREATE_BROKEN_DOWN_VEHICLE")
	
	IF openIdx = -1
		SCRIPT_ASSERT("openIdx = -1")
	ENDIF

	SET_VEHICLE_DOOR_OPEN(brokenVehicles[openIdx].vehicleIndex, SC_DOOR_BONNET)
	
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(brokenVehicles[openIdx].vehicleIndex), vModelMin, vModelMax)
	PRINTLN("MINIMUM = ", vModelMin)
	PRINTLN("MAXIMUM = ", vModelMax)
	
	vDimensions = vModelMax - vModelMin
	PRINTLN("DIMENSIONS = ", vDimensions)
	
	vDimensions = vDimensions / 2.0
	PRINTLN("USABLE DIMENSIONS = ", vDimensions)
	
	bSpawnPed = TRUE
	PRINTLN("bSpawnPed = TRUE")

	vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(brokenVehicles[openIdx].vehicleIndex, <<1.5,0,0>>) //vEngineOffset)
		
	vBrokenDownVehiclePosition = GET_ENTITY_COORDS(brokenVehicles[openIdx].vehicleIndex)
	fBrokenDownVehicleHeading = GET_ENTITY_HEADING(brokenVehicles[openIdx].vehicleIndex)
	
	iSpeedNode01 = ADD_ROAD_NODE_SPEED_ZONE(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBrokenDownVehiclePosition, fBrokenDownVehicleHeading, <<-1.5,-30,0>>), 10, 0)
	iSpeedNode02 = ADD_ROAD_NODE_SPEED_ZONE(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBrokenDownVehiclePosition, fBrokenDownVehicleHeading, <<-1.5,30,0>>), 10, 0)
	CLEAR_AREA_OF_VEHICLES(vTemp, 30)
	
	extraData[GET_IDX()].vUtilityPos[0] = <<vTemp.x, vTemp.y, vTemp.z+1>>
	RETURN TRUE

ENDFUNC

PROC UPDATE_PED_GIVE_UP(BOOL bCheckDetach = FALSE)
	BOOL bGiveUp = FALSE
	FLOAT fDist
	STRING sLine

	fDist = GET_DISTANCE_BETWEEN_ENTITIES(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex) 

	IF bCheckDetach
		IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
			IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
			AND NOT IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex)
				bGiveUp = TRUE
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(brokenVehicles[0].pedIndex, PLAYER_PED_ID())
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(PLAYER_PED_ID(), brokenVehicles[0].pedIndex)
			RESTART_TIMER_NOW(chaseTimer)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(brokenVehicles[0].pedIndex)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	IF IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
		pedState = ANGRY_PED_DONE
	ELIF fDist > 35 OR (GET_TIMER_IN_SECONDS(chaseTimer) > 30 AND fDist > 5) OR bGiveUp
	OR (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE))
		DEBUG_MESSAGE("Give up")

		IF bAttack
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWABDHANDMAN2")
			sLine = "TOW_PED_ANCU"
		ELIF bSexVariation
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWSEXMANLYGUY")
			sLine = "TOW_YELL"
		ELSE
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWABDHANDMAN1")
			sLine = "TOW_PED_ANC2"
		ENDIF
		
		piSpeakers[0] = brokenVehicles[0].pedIndex
		piSpeakers[1] = brokenVehicles[0].pedIndexPass
		
		bAngryPedGaveUp = TRUE

		ODDJOB_START_CONVERSATION(MyLocalPedStruct, "TOWAUD", sLine, piSpeakers)
		//CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", sLine, CONV_PRIORITY_VERY_HIGH)
		
		REMOVE_BLIP(brokenVehicles[0].blipIndexPed)
		IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
			CLEAR_PED_TASKS(brokenVehicles[0].pedIndex)
			TASK_SMART_FLEE_PED(brokenVehicles[0].pedIndex, PLAYER_PED_ID(), 1000, -1)
		ENDIF
		//TASK_WANDER_STANDARD(brokenVehicles[0].pedIndex)
		pedState = ANGRY_PED_DONE
	ENDIF
ENDPROC

PROC HANDLE_ENGINE_START_AUDIO()

	FLOAT fAnimTime
		
	IF bDisablePedCam OR (NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
	AND NOT IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex))
//		PRINTLN("bDisablePedCam = TRUE")
		IF soundID3 <> - 1
			STOP_SOUND(soundID3)
			RELEASE_SOUND_ID(soundID3)
			soundID3 = -1
			PRINTLN("STOPPING SOUND")
			EXIT
		ENDIF
	ENDIF	
		
	IF NOT bGrabbedSoundID
		soundID3 = GET_SOUND_ID()
		PRINTLN("soundID3 = ", soundID3)
		bGrabbedSoundID = TRUE
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
		IF IS_ENTITY_PLAYING_ANIM(brokenVehicles[0].pedIndex, "oddjobs@towing", "Start_Engine_Loop")
			fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(brokenVehicles[0].pedIndex, "oddjobs@towing", "Start_Engine_Loop")
//			PRINTLN("fAnimTime = ", fAnimTime)
			
			IF (fAnimTime >= 0.190 AND fAnimTime <= 0.262)
			OR (fAnimTime >= 0.400 AND fAnimTime <= 0.600)
			OR (fAnimTime >= 0.900 AND fAnimTime <= 1.0)
			
				IF (fAnimTime >= 0.190 AND fAnimTime <= 0.262)
//					PRINTLN("CASE 1 MET")
				ENDIF
				IF (fAnimTime >= 0.400 AND fAnimTime <= 0.600)
//					PRINTLN("CASE 2 MET")
				ENDIF
				IF (fAnimTime >= 0.900 AND fAnimTime <= 1.0)
//					PRINTLN("CASE 3 MET")
				ENDIF
				
				IF NOT bPlayedSound
					PLAY_SOUND_FROM_ENTITY(soundID3, "TOWING_ENGINE_TURNING_MASTER", brokenVehicles[0].vehicleIndex)
					PRINTLN("PLAYING SOUND soundID3 = ", soundID3)
					bPlayedSound = TRUE
				ENDIF
			ELSE
				STOP_SOUND(soundID3)
				PRINTLN("STOPPING SOUND: ", soundID3)
				bPlayedSound = FALSE
			ENDIF
		ELSE
			
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_PED_PLAYED_ANIM_WHEN_PUSHED()

	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) 
		IF bPushedOffTrack
		
			SWITCH iPushStages
				CASE 0
					IF GET_SEQUENCE_PROGRESS(brokenVehicles[0].pedIndex) = 1
						PRINTLN("iPushStages = 1")
						iPushStages = 1
					ENDIF
				BREAK
				CASE 1
					IF NOT IS_ENTITY_PLAYING_ANIM(brokenVehicles[0].pedIndex, "oddjobs@towingpleadingidle_b", "idle_d")
						PRINTLN("iPushStages = 2")
						iPushStages = 2
					ENDIF
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_TRAIN_PED()

	HANDLE_ENGINE_START_AUDIO()
	
	SWITCH trainPedState
		CASE TRAIN_STATE_PED_INIT
			//DEBUG_MESSAGE("TRAIN_STATE_PED_INIT")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(),"FRANKLIN")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, brokenVehicles[0].pedIndex, sCharacterName)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
			fPanicDist = GET_RANDOM_INT_IN_RANGE(35, 80)
			trainPedState =  TRAIN_STATE_PED_FIRST
		BREAK
		CASE TRAIN_STATE_PED_FIRST
			//DEBUG_MESSAGE("TRAIN_STATE_PED_FIRST")		
			
			OPEN_SEQUENCE_TASK(taskSequence)
				//TASK_PLAY_ANIM(NULL, "oddjobs@towing","Start_Engine_Enter",NORMAL_BLEND_IN,NORMAL_BLEND_OUT)//, -1, AF_LOOPING)
				TASK_PLAY_ANIM(NULL, "oddjobs@towing","Start_Engine_Loop",NORMAL_BLEND_IN,NORMAL_BLEND_OUT, -1, AF_LOOPING)
			CLOSE_SEQUENCE_TASK(taskSequence)
			TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)		
			CLEAR_SEQUENCE_TASK(taskSequence)
			//SET_ENTITY_ANIM_CURRENT_TIME()
			//GET_SCRIPT_TASK_STATUS()
			
			//Trigger first line about stalled car if the player is close enough to hear it or in the cam
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(illegalNodes[illegalIdx].vCarPos) < 40 OR towArgs.iCamTime > 0
				IF chosenNodeType = NODE_TYPE_TRAIN
					IF g_savedGlobals.sTowingData.iTrainCompleted = 0
					OR g_savedGlobals.sTowingData.iTrainCompleted = 1
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWTRAINF")
						CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_GETOUT", CONV_PRIORITY_HIGH)
						trainPedState =  TRAIN_STATE_PED_LOOK
					ELIF g_savedGlobals.sTowingData.iTrainCompleted = 2
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWILLEGALMAN2")
						CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_GETOUT2", CONV_PRIORITY_HIGH)
						trainPedState =  TRAIN_STATE_PED_LOOK
					ENDIF
				ENDIF
			ELIF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) < fPanicDist+10
				TASK_PLAY_ANIM(NULL, "oddjobs@towing","Start_Engine_Exit",NORMAL_BLEND_IN,NORMAL_BLEND_OUT)
				//skip ahead if the train is getting close
				trainPedState =  TRAIN_STATE_PED_LOOK
			ENDIF
		BREAK
		CASE TRAIN_STATE_PED_LOOK
			//DEBUG_MESSAGE("TRAIN_STATE_PED_LOOK")
			//temp remove this check
			IF NOT IS_VECTOR_ZERO(illegalNodes[illegalIdx].vAngledPos1)
			illegalNodes[illegalIdx].vAngledPos1.z = 45
			illegalNodes[illegalIdx].vAngledPos2.z = -45
				IF NOT IS_ENTITY_IN_ANGLED_AREA(brokenVehicles[0].vehicleIndex, illegalNodes[illegalIdx].vAngledPos1, illegalNodes[illegalIdx].vAngledPos2, illegalNodes[illegalIdx].fAngledWidth)
					//SCRIPT_ASSERT("entity isn't in angeled area")
//					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_TRN_PUSH", CONV_PRIORITY_VERY_HIGH)
					ODDJOB_STOP_SOUND(soundID3)
					bDisablePedCam = TRUE
					bDisableCam = TRUE
					
					bPushedOffTrack = TRUE
					PRINTLN("SETTING bDisablePedCam = TRUE BECAUSE PED IS OUTSIDE ANGLED AREA")
					IF IS_PED_IN_ANY_VEHICLE(brokenVehicles[0].pedIndex)
						OPEN_SEQUENCE_TASK(taskSequence)
							TASK_LEAVE_ANY_VEHICLE(NULL)
							TASK_PLAY_ANIM(NULL, "oddjobs@towingpleadingidle_b", "idle_d", SLOW_BLEND_IN, SLOW_BLEND_OUT, 10000, AF_UPPERBODY)
						CLOSE_SEQUENCE_TASK(taskSequence)
						TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)	
						CLEAR_SEQUENCE_TASK(taskSequence)
						trainPedState =  TRAIN_STATE_PED_PENDING
					ENDIF
				ENDIF
			ENDIF
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) < fPanicDist+15
				//TOW_TRN_PPE1 //flee
				//TOW_TRN_PPS1 //stuck
				IF g_savedGlobals.sTowingData.iTrainCompleted = 0
				OR g_savedGlobals.sTowingData.iTrainCompleted = 1
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWTRAINF")
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_GETOUT", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("PLAYING AUDIO LINE - TOW_GETOUT")
				ELIF g_savedGlobals.sTowingData.iTrainCompleted = 2
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWILLEGALMAN2")
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_GETOUT2", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("PLAYING AUDIO LINE - TOW_GETOUT2")
				ENDIF
				
				ODDJOB_STOP_SOUND(soundID3)
				TASK_LOOK_AT_ENTITY(brokenVehicles[0].pedIndex, brokenVehicles[0].blockingIndex, -1, SLF_FAST_TURN_RATE)
				SETTIMERB(0)
				DEBUG_MESSAGE("Setting timer b to 0")
				trainPedState =  TRAIN_STATE_PED_PANIC
			ENDIF
		BREAK
		CASE TRAIN_STATE_PED_PANIC
			//DEBUG_MESSAGE("TRAIN_STATE_PED_PANIC")
			//look for at least a second
			//IF TIMERB() > 3000
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) < fPanicDist
					//have ped exit car and run
					IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
						IF iToggle = 1
							CLEANUP_CAM(towArgs)
						ENDIF
						
						bDisablePedCam = TRUE
						IF IS_PED_IN_ANY_VEHICLE(brokenVehicles[0].pedIndex)
							OPEN_SEQUENCE_TASK(taskSequence)
								TASK_LEAVE_ANY_VEHICLE(NULL)
//								TASK_SMART_FLEE_COORD(NULL, illegalNodes[illegalIdx].vCarPos, 15.0, 3000)
								TASK_GO_TO_COORD_ANY_MEANS(NULL, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(illegalNodes[illegalIdx].vCarPos, illegalNodes[illegalIdx].fCarRot, 
										<< 0, 15, 0 >>), PEDMOVEBLENDRATIO_SPRINT, NULL)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
							CLOSE_SEQUENCE_TASK(taskSequence)
							SET_PED_FLEE_ATTRIBUTES(brokenVehicles[0].pedIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
							TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)	
							CLEAR_SEQUENCE_TASK(taskSequence)
							DEBUG_MESSAGE("TRAIN_STATE_PED_PENDING")
							trainPedState =  TRAIN_STATE_PED_PENDING
						ENDIF
					ENDIF
				ENDIF
			//ENDIF
		BREAK
		CASE TRAIN_STATE_PED_PENDING
			//DEBUG_MESSAGE("TRAIN_STATE_PED_PENDING")
			IF trainState = TRAIN_STATE_DONE AND bTowHooked
			OR HAS_PED_PLAYED_ANIM_WHEN_PUSHED()
//				bEnableBonusCam = FALSE
				SETTIMERB(3000)
				DEBUG_MESSAGE("TRAIN_STATE_PED_ENTER_TRUCK")
				trainPedState = TRAIN_STATE_PED_ENTER_TRUCK
			ENDIF
			//If the train moves past and the tow truck is attached then transition to the tow truck.
		BREAK
		CASE TRAIN_STATE_PED_ENTER_TRUCK
			//DEBUG_MESSAGE("TRAIN_STATE_PED_ENTER_TRUCK")
			bReverseOrder = TRUE
			//Don't get out right away if skipped
			IF TIMERB() > 3000
				IF UPDATE_BROKEN_VEHICLE_PED(brokenState, 0, "TOW_TRAIN1")
					trainPedState =  TRAIN_STATE_PED_END
				ENDIF
			ENDIF
		BREAK
		CASE TRAIN_STATE_PED_END
			//DEBUG_MESSAGE("TRAIN_STATE_PED_END")
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_TRAIN()
	VECTOR tempa
#IF IS_DEBUG_BUILD
	VECTOR tempb
	FLOAT fDistanceBetween
#ENDIF	//	IS_DEBUG_BUILD

	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) AND NOT IS_ENTITY_DEAD(myVehicle)
		IF IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, myVehicle)
			IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndexExtra)
				REMOVE_BLIP(brokenVehicles[0].blipIndexExtra)
				PRINTLN("REMOVING BLIP - brokenVehicles[0].blipIndexExtra")
				ODDJOB_STOP_SOUND(soundID3)
			ENDIF
		ELIF NOT IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex)
			ODDJOB_STOP_SOUND(soundID3)
		ENDIF
	ENDIF
	
	SWITCH trainState
		CASE TRAIN_STATE_PRINT
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(illegalNodes[illegalIdx].vCarPos) < 100
				brokenVehicles[0].blockingIndex = CREATE_MISSION_TRAIN(6, illegalNodes[illegalIdx].vPedPos, illegalNodes[illegalIdx].bBackward)
				IF DOES_ENTITY_EXIST(brokenVehicles[0].blockingIndex)
					brokenVehicles[0].blipIndexExtra = ADD_BLIP_FOR_ENTITY(brokenVehicles[0].blockingIndex)
				ENDIF
				
				pedTrainDriver = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].blockingIndex, PEDTYPE_CIVMALE, S_M_M_LSMETRO_01)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTrainDriver, TRUE)
				
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex)
					SET_TRAIN_CRUISE_SPEED(brokenVehicles[0].blockingIndex, 0.0)
				ENDIF
				PRINT_NOW("TOW_OBJ_05", DEFAULT_GOD_TEXT_TIME, 1)
				SETTIMERA(0)
				PRINTLN("Initial Train Distance:", VDIST(illegalNodes[illegalIdx].vPedPos, illegalNodes[illegalIdx].vCarPos), " Train index: ", illegalIdx)
				
				ODDJOB_PLAY_SOUND("CROSSING_BELL", soundID4, TRUE, NULL, 226.8878, -2538.4209, 4.8665, "TONYA_03_SOUNDSET")
				
				trainState = TRAIN_STATE_WAIT
			ENDIF
		BREAK
		CASE TRAIN_STATE_WAIT
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(illegalNodes[illegalIdx].vCarPos) < 80 OR TIMERA() > 10000
			
				// Start oncoming train scene
				IF NOT IS_AUDIO_SCENE_ACTIVE("TOWING_ONCOMING_TRAIN_SCENE")
					START_AUDIO_SCENE("TOWING_ONCOMING_TRAIN_SCENE")
					PRINTLN("STARTING AUDIO SCENE - TOWING_ONCOMING_TRAIN_SCENE")
				ENDIF
			
				trainState = TRAIN_STATE_START
			ENDIF
		BREAK
		CASE TRAIN_STATE_START
			//PRINTLN(GET_DISTANCE_BETWEEN_ENTITIES(brokenVehicles[0].blockingIndex, brokenVehicles[0].vehicleIndex))
			IF NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex)
				PRINTLN("Setting the train speed to 22.0")
				SET_TRAIN_CRUISE_SPEED(brokenVehicles[0].blockingIndex, 26.0)
				SET_TRAIN_SPEED(brokenVehicles[0].blockingIndex, 22.0)
			ENDIF
			trainState = TRAIN_STATE_BREAK
		BREAK
		CASE TRAIN_STATE_BREAK
			IF NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) < 175
					SIMULATE_BREAKING(brokenVehicles[0].blockingIndex)
					ODDJOB_PLAY_SOUND("TOWING_TRAIN_BRAKES_MASTER", soundID2, TRUE, brokenVehicles[0].blockingIndex)
					START_BREAKING_PFX(sparksEffects, brokenVehicles[0].blockingIndex)
					ODDJOB_PLAY_SOUND("TOWING_TRAIN_HORN_MASTER", soundID, TRUE, brokenVehicles[0].blockingIndex)
					trainState = TRAIN_STATE_CHECKS
				ENDIF
			ELSE
				trainState = TRAIN_STATE_DONE
			ENDIF
		BREAK
		CASE TRAIN_STATE_CHECKS
			DEBUG_MESSAGE("Checks")
			IF NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
				
				SIMULATE_BREAKING(brokenVehicles[0].blockingIndex)
				
				IF NOT IS_ENTITY_IN_ANGLED_AREA(brokenVehicles[0].vehicleIndex, illegalNodes[illegalIdx].vAngledPos1, illegalNodes[illegalIdx].vAngledPos2, illegalNodes[illegalIdx].fAngledWidth)
					STOP_BREAKING_PFX(sparksEffects)
//					PRINTLN("STOPPING BREAKING EFFECTS, SINCE WE'RE NOT ON THE TRACKS")
				ENDIF
				
				tempa = GET_ENTITY_COORDS(brokenVehicles[0].blockingIndex)
				PRINTLN("tempa = ", tempa)
#IF IS_DEBUG_BUILD
				tempb = GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex)
				fDistanceBetween = VDIST(tempa, tempb)
				PRINTLN("fDistanceBetween = ", fDistanceBetween)
#ENDIF	//	IS_DEBUG_BUILD
				
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) < 3
					bTrainClose = TRUE
				ENDIF
				
				PRINTLN("Is entity touching entity:", "Train pos: ", tempa, " Car pos: ", tempb)
				IF IS_ENTITY_TOUCHING_ENTITY(brokenVehicles[0].blockingIndex, brokenVehicles[0].vehicleIndex) 
					PRINTLN("Train is touching car")
					IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
							DETACH_VEHICLE_FROM_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
						ENDIF
					ENDIF
					EXPLODE_VEHICLE(brokenVehicles[0].vehicleIndex)
					trainState = TRAIN_STATE_STOP
				ELIF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].vehicleIndex, tempa) < 10
				AND IS_ENTITY_IN_ANGLED_AREA(brokenVehicles[0].vehicleIndex, illegalNodes[illegalIdx].vAngledPos1, illegalNodes[illegalIdx].vAngledPos2, illegalNodes[illegalIdx].fAngledWidth)
					bTrainClose = TRUE
					// HACK: Adding in to fix entity touching entity bug
					IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
							DETACH_VEHICLE_FROM_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
						ENDIF
					ENDIF
					EXPLODE_VEHICLE(brokenVehicles[0].vehicleIndex)
					PRINTLN("HITTING DITANCE CHECK!")
					// HACK: Adding in to fix entity touching entity bug
				ENDIF
				
				//The train was close to the target vehicle but now is getting further away.  
				IF bTrainClose AND GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) > 4
					PRINTLN("Train is getting further away")
					trainState = TRAIN_STATE_STOP
				ENDIF
				
			ELSE
				trainState = TRAIN_STATE_STOP
			ENDIF
		BREAK
		CASE TRAIN_STATE_STOP
			IF NOT bTowHooked
				PRINT_NOW("TOWT_OBJ_03G", DEFAULT_GOD_TEXT_TIME, 1)
				
				IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndexExtra)
					REMOVE_BLIP(brokenVehicles[0].blipIndexExtra)
					PRINTLN("REMOVING BLIP - brokenVehicles[0].blipIndexExtra - NOT bTowHooked")
				ENDIF
			ENDIF
			
			DEBUG_MESSAGE("Stop")
			REMOVE_BLIP(brokenVehicles[0].blipIndexExtra)
			ODDJOB_STOP_SOUND(soundID)
			ODDJOB_STOP_SOUND(soundID2)
			STOP_BREAKING_PFX(sparksEffects)
			ODDJOB_STOP_SOUND(soundID3)
			ODDJOB_STOP_SOUND(soundID4)
			trainState = TRAIN_STATE_OBJ
		BREAK
		CASE TRAIN_STATE_OBJ
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
				IF NOT bTowHooked
					PRINT_NOW("TOWT_OBJ_03G", DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("TOWING_ONCOMING_TRAIN_SCENE")
					STOP_AUDIO_SCENE("TOWING_ONCOMING_TRAIN_SCENE")
					PRINTLN("STOPPING AUDIO SCENE - TOWING_ONCOMING_TRAIN_SCENE VIA MAIN UPDATE")
				ENDIF
				
				trainState = TRAIN_STATE_DONE
			ENDIF
		BREAK
		CASE TRAIN_STATE_DONE
	
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_PED_DIALOUGE()
//	PRINTLN("UPDATE_PED_DIALOUGE")

	SWITCH pedDialougeState
		CASE TOW_PED_BEG
//		PRINTLN("TOW_PED_BEG")
//		//should be able to just let this trigger: CREATE_CONVERSATION_FROM_SPECIFIC_LINE()
		
			IF GET_SEQUENCE_PROGRESS(brokenVehicles[0].pedIndex) >= 1 AND NOT IS_MESSAGE_BEING_DISPLAYED()
				
				PRINTLN("GOING TO STATE - TOW_PED_FACE")
				pedDialougeState = TOW_PED_FACE
			ENDIF
		BREAK
		CASE TOW_PED_FACE
//			PRINTLN("TOW_PED_FACE")

			IF bPlayInitialUpsetLine AND NOT bPlayFullConvo
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					IF NOT bPrintSpecialCaseObj
						IF NOT bWakeUp
							PRINT_NOW("TOWT_OBJ_06", DEFAULT_GOD_TEXT_TIME, 1)
							bPrintSpecialCaseObj = TRUE
							PRINTLN("bWakeUp IS FALSE, PRINT OBJECTIVE")
						ELSE
							PRINTLN("bWakeUp IS TRUE, DO NOT PRINT OBJECTIVE")
						ENDIF
					ENDIF
				
					IF bPrintSpecialCaseObj AND NOT IS_MESSAGE_BEING_DISPLAYED()
						IF GET_SCRIPT_TASK_STATUS(brokenVehicles[0].pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
							TASK_TURN_PED_TO_FACE_ENTITY(brokenVehicles[0].pedIndex, brokenVehicles[0].pedIndexPass)
						ENDIF
					
						PRINTLN("PLAYING CONVO - TOW_SEX_SCAR")
						bPlayFullConvo = TRUE
						
						PRINTLN("GOING TO STATE - TOW_PED_CHASE")
						pedDialougeState= TOW_PED_CHASE
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(brokenVehicles[0].pedIndex) > 20.0
				PRINTLN("STOPPING SCRIPTED CONVERSATION, BECAUSE PLAYER HAS EXCEEDED 90M DISTANCE - 01")
				STOP_SCRIPTED_CONVERSATION(TRUE)
				RETURN TRUE
			ENDIF
		BREAK
		CASE TOW_PED_CHASE
		
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(brokenVehicles[0].pedIndex) > 20.0
				PRINTLN("STOPPING SCRIPTED CONVERSATION, BECAUSE PLAYER HAS EXCEEDED 90M DISTANCE - 02")
				STOP_SCRIPTED_CONVERSATION(TRUE)
				RETURN TRUE
			ENDIF
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				PRINTLN("RETURN TRUE - UPDATE_PED_DIALOUGE - 02")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC HANDLE_PLAYER_CLOSE_TO_SEX_COUPLE()
	VECTOR vPlayerPosition, vBrokenDownVehiclePosition
	
	IF NOT bPlayTooCloseConvo
		IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
			IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
				vBrokenDownVehiclePosition = GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex)
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
	
		IF bSexVariation
			IF VDIST2(vPlayerPosition, vBrokenDownVehiclePosition) < 5*5
				STOP_SCRIPTED_CONVERSATION(TRUE)
				
				#IF NOT FEATURE_GEN9_RELEASE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TASK_LOOK_AT_ENTITY(brokenVehicles[0].pedIndex, PLAYER_PED_ID(), 5000)
					
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWSEXMANLYGUY")
					IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HANDI_CL", CONV_PRIORITY_VERY_HIGH)
					
						PRINTLN("PLAYING PLAYER TOO CLOSE - CONVO")
						bPlayTooCloseConvo = TRUE
					ENDIF
				ENDIF
				#ENDIF
				#IF FEATURE_GEN9_RELEASE
				bPlayTooCloseConvo = TRUE	//Bypass playing any dialogue here
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


#IF NOT IS_JAPANESE_BUILD

	FUNC BOOL UPDATE_ANGRY_PED()
		SEQUENCE_INDEX iSeq
		VECTOR vPlayerPosition
		
		HANDLE_PLAYER_CLOSE_TO_SEX_COUPLE()

		SWITCH pedState
			CASE ANGRY_PED_INIT
				
				IF bSexVariation
					IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						brokenVehicles[0].pedIndex = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].vehicleIndex, PEDTYPE_MISSION, pedNames[0], VS_BACK_LEFT)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWSEXMANLYGUY")
						
						brokenVehicles[0].pedIndexPass = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].vehicleIndex, PEDTYPE_MISSION, mnExtraModel, VS_BACK_RIGHT)			
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndexPass, TRUE)
						SET_PED_CONFIG_FLAG(brokenVehicles[0].pedIndexPass, PCF_DontInfluenceWantedLevel, TRUE)
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndexPass, "TOWSEXPRISSYGUY")
#IF NOT FEATURE_GEN9_RELEASE 
						TASK_PLAY_ANIM(brokenVehicles[0].pedIndex, "oddjobs@towing", "m_blow_job_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						TASK_PLAY_ANIM(brokenVehicles[0].pedIndexPass, "oddjobs@towing", "f_blow_job_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
#ENDIF
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
							SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_CANNOT_ENTER)
						ENDIF
						bPedCreated = TRUE
						PRINTLN("bPedCreated = TRUE - 02")
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
						PRINTLN("CREATING PED - bSexVariation")
					ENDIF
				ELIF bHomelessVariation
					IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						brokenVehicles[0].pedIndex = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].vehicleIndex, PEDTYPE_MISSION, pedNames[0], VS_FRONT_RIGHT)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
						SET_PED_CONFIG_FLAG(brokenVehicles[0].pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, brokenVehicles[0].pedIndex, sCharacterName)
						TASK_PLAY_ANIM(brokenVehicles[0].pedIndex,"RANDOM@CAR_SLEEPING","Sleeping_Idle",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						bPedCreated = TRUE
						PRINTLN("bPedCreated = TRUE - 03")
						
						PRINTLN("CREATING PED - bHomelessVariation")
					ELSE
						PRINTLN("VEHICLE DEAD - NO PED CREATION")
					ENDIF
				ELIF GET_RANDOM_BOOL()
					DEBUG_MESSAGE("bAttack = TRUE")
					bAttack = TRUE
				ELSE
					DEBUG_MESSAGE("bAttack = FALSE")
					bAttack = FALSE
				ENDIF
				pedState = ANGRY_PED_CREATE
			BREAK
			CASE ANGRY_PED_CREATE
				//Once the player is close to the tow car spawn the angry ped
				
				IF bSexVariation
					#IF NOT FEATURE_GEN9_RELEASE
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex), <<15,15,15>>)
							IF NOT bPlaySexyConvo
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_SEX_SNDS", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("bPlaySexyConvo = TRUE - 01")
									bPlaySexyConvo = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					#ENDIF
					#IF FEATURE_GEN9_RELEASE
					IF NOT bPlaySexyConvo
						bPlaySexyConvo = TRUE		//Bypass playing any conversations here
					ENDIF
					#ENDIF
				ENDIF
			
				//attaching the hook and bumping / horn should wake her up..
				IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex) OR SHOULD_WAKE_UP(myVehicle, brokenVehicles[0].vehicleIndex, NOT(bSexVariation OR bHomelessVariation) )
						IF SHOULD_WAKE_UP(myVehicle, brokenVehicles[0].vehicleIndex, NOT(bSexVariation OR bHomelessVariation) )
							bWakeUp = TRUE
							PRINTLN("bWakeUp = TRUE")
						ENDIF
						
						IF NOT bHomelessVariation
							START_TIMER_AT(wakeTimer, 1.0)
						ELSE
							START_TIMER_NOW(wakeTimer)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_TIMER_STARTED(wakeTimer) AND GET_TIMER_IN_SECONDS(wakeTimer) > 1.0 
					IF bSexVariation AND IS_TIMER_STARTED(wakeTimer)
					
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_ANY_CONVERSATION()
							//Hey what the hell is going on!  Can't a guy fuck in peace?
						ENDIF
					
						//jump out all angry and 
						bShakeCar = FALSE
						
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
							OPEN_SEQUENCE_TASK(taskSequence)
								TASK_LEAVE_ANY_VEHICLE(NULL, 750)
	//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_GOTO_POS(brokenVehicles[0].vehicleIndex), PEDMOVEBLENDRATIO_RUN)
	//							TASK_GO_TO_COORD_ANY_MEANS(NULL, GET_GOTO_POS(brokenVehicles[0].vehicleIndex), PEDMOVEBLENDRATIO_RUN, NULL)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								//pick one of these two..
								TASK_PLAY_ANIM(NULL, "oddjobs@towingpleadingidle_b", "idle_d", NORMAL_BLEND_OUT, SLOW_BLEND_DURATION, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(taskSequence)
							TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
							CLEAR_SEQUENCE_TASK(taskSequence)
							
							OPEN_SEQUENCE_TASK(taskSequence)
								TASK_LEAVE_ANY_VEHICLE(NULL)
	//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_GOTO_POS_PASSENGER(brokenVehicles[0].vehicleIndex), PEDMOVEBLENDRATIO_RUN)
	//							TASK_GO_TO_COORD_ANY_MEANS(NULL, GET_GOTO_POS_PASSENGER(brokenVehicles[0].vehicleIndex), PEDMOVEBLENDRATIO_RUN, NULL)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, brokenVehicles[0].pedIndex)
								TASK_PLAY_ANIM(NULL, "oddjobs@towingpleadingidle_a", "idle_c", SLOW_BLEND_DURATION, SLOW_BLEND_DURATION, 30000, AF_LOOPING)
								TASK_SMART_FLEE_PED(NULL, brokenVehicles[0].pedIndex, 1000, -1)
								TASK_WANDER_STANDARD(NULL)
							CLOSE_SEQUENCE_TASK(taskSequence)
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndexPass)
								TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndexPass, taskSequence)
							ENDIF
							CLEAR_SEQUENCE_TASK(taskSequence)
	//						bEnableBonusCam = FALSE
						ENDIF
						
						PRINTLN("GOING TO STATE - ANGRY_PED_EXIT")
						pedState = ANGRY_PED_EXIT
					ELIF bHomelessVariation
						//homeless attack variation.
						IF g_savedGlobals.sTowingData.iAbandonCompleted% 2 = 0
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								OPEN_SEQUENCE_TASK(taskSequence)
									TASK_LEAVE_ANY_VEHICLE(NULL)
									TASK_GO_TO_ENTITY(NULL, myVehicle, DEFAULT_TIME_BEFORE_WARP, 9.0, PEDMOVEBLENDRATIO_WALK)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									TASK_PLAY_ANIM(NULL, "oddjobs@towingangryidle_a", "idle_b", NORMAL_BLEND_OUT, SLOW_BLEND_DURATION, 7000)
									TASK_WANDER_STANDARD(NULL)
								CLOSE_SEQUENCE_TASK(taskSequence)
								TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
								CLEAR_SEQUENCE_TASK(taskSequence)
							
	//							bEnableBonusCam = FALSE
							ENDIF
							
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, brokenVehicles[0].pedIndex, "TOWABDBITCHM")
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_ANGR", CONV_PRIORITY_VERY_HIGH)
						ELSE
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								SETUP_PED_FOR_COMBAT(brokenVehicles[0].pedIndex)
								SET_PED_COMBAT_ATTRIBUTES(brokenVehicles[0].pedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(brokenVehicles[0].pedIndex, CA_USE_VEHICLE, FALSE)
								OPEN_SEQUENCE_TASK(taskSequence)
									TASK_LEAVE_ANY_VEHICLE(NULL)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									//TASK_PLAY_ANIM(NULL, "gestures@male", "anger_a")
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(taskSequence)
								TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
								CLEAR_SEQUENCE_TASK(taskSequence)
								
								IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
									tempBlip = ADD_BLIP_FOR_ENTITY(brokenVehicles[0].pedIndex)
									SET_BLIP_COLOUR(tempBlip, BLIP_COLOUR_RED)
									SET_BLIP_SCALE(tempBlip, BLIP_SIZE_PED)
									REGISTER_BLIPPED_ENTITY(brokenVehicles[0].pedIndex, tempBlip, TRUE)
									PRINTLN("ADDING RED BLIP FOR PED")
								ENDIF
								
								bAttack = TRUE
								PRINTLN("SETTING bAttack = TRUE VIA bHomelessVariation")
								
	//							bEnableBonusCam = FALSE
							ENDIF
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_DRG", CONV_PRIORITY_VERY_HIGH)
						ENDIF
						//who woke me up?
						
						pedState = ANGRY_PED_EXIT
					ELIF bAttack
						brokenVehicles[0].pedIndex = CREATE_PED(PEDTYPE_CIVMALE, pedNames[0], illegalNodes[illegalIdx].vPedPos, illegalNodes[illegalIdx].fPedRot) 
						PRINTLN("CREATING PED - bAttack IS TRUE")
						
						//GIVE_WEAPON_TO_PED(brokenVehicles[0].pedIndex, WEAPONTYPE_PISTOL, 200)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
						SET_PED_CONFIG_FLAG(brokenVehicles[0].pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
						bPedCreated = TRUE
						PRINTLN("bPedCreated = TRUE - 04")
						
						angryPedRoot = "TOW_PED_ANGR"
						bPedCreated = FALSE
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
							OPEN_SEQUENCE_TASK(taskSequence)
								TASK_GO_TO_ENTITY(NULL, myVehicle, DEFAULT_TIME_BEFORE_WARP, 15.0, PEDMOVEBLENDRATIO_SPRINT) //brokenVehicles[0].vehicleIndex
								TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(taskSequence)
							TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
							CLEAR_SEQUENCE_TASK(taskSequence)
	//						bEnableBonusCam = FALSE
						ENDIF
						pedState = ANGRY_PED_WAIT
					ELSE
						brokenVehicles[0].pedIndex = CREATE_PED(PEDTYPE_CIVMALE, pedNames[0], illegalNodes[illegalIdx].vPedPos, illegalNodes[illegalIdx].fPedRot) 
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
						SET_PED_CONFIG_FLAG(brokenVehicles[0].pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
						PRINTLN("CREATING PED - REGULAR")
						SET_ENTITY_LOAD_COLLISION_FLAG(brokenVehicles[0].pedIndex, TRUE)
						
						bPedCreated = TRUE
						PRINTLN("bPedCreated = TRUE - 05")
						
						angryPedRoot = "TOW_ILLEG1"
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
							OPEN_SEQUENCE_TASK(taskSequence)
								TASK_GOTO_ENTITY_OFFSET(NULL, PLAYER_PED_ID(), DEFAULT_TIME_BEFORE_WARP, 12.0, 0, PEDMOVEBLENDRATIO_SPRINT)
								//TASK_GOTO_ENTITY_OFFSET_XY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_BEFORE_WARP, 12.0, 0, 2, PEDMOVEBLENDRATIO_SPRINT)
								//TASK_GO_TO_ENTITY(NULL, myVehicle, DEFAULT_TIME_BEFORE_WARP, 10.0, PEDMOVEBLENDRATIO_SPRINT) //brokenVehicles[0].vehicleIndex
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								//TASK_PLAY_ANIM(NULL, "gestures@male", "anger_a")
							CLOSE_SEQUENCE_TASK(taskSequence)
							TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
							CLEAR_SEQUENCE_TASK(taskSequence)
	//						bEnableBonusCam = FALSE
						ENDIF
						pedState = ANGRY_PED_WAIT
					ENDIF
				ENDIF
			BREAK
			CASE ANGRY_PED_EXIT
//				PRINTLN("INSIDE STATE - ANGRY_PED_EXIT")
				
				IF bSexVariation
					#IF NOT FEATURE_GEN9_RELEASE
					IF NOT bPlayInitialUpsetLine
						IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex) OR bWakeUp
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_SEX_INIT", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("PLAYING CONVO - TOW_SEX_SCAR - 01")
										bPlayInitialUpsetLine = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					#ENDIF
					#IF FEATURE_GEN9_RELEASE
					bPlayInitialUpsetLine = TRUE		//Skip playing any conversations here
					#ENDIF
					
				ENDIF
			
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
					IF NOT IS_PED_IN_ANY_VEHICLE(brokenVehicles[0].pedIndex)
						IF bHomelessVariation
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
								IF GET_SCRIPT_TASK_STATUS(brokenVehicles[0].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
									IF GET_SEQUENCE_PROGRESS(brokenVehicles[0].pedIndex) = 2 // check task status
										IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
											vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
										ENDIF
										
										IF VDIST2(vPlayerPosition, GET_ENTITY_COORDS(brokenVehicles[0].pedIndex)) < (20*20)
											IF g_savedGlobals.sTowingData.iAbandonCompleted% 2 = 0 //TODO pick the right enums for variaitons.. male - female
												CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_ANG1", CONV_PRIORITY_VERY_HIGH)
											ELSE
												CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_ANG2", CONV_PRIORITY_VERY_HIGH)
											ENDIF
										ENDIF
										
										PRINTLN("GOING TO STATE - ANGRY_PED_DONE")
										pedState = ANGRY_PED_DONE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
								IF UPDATE_PED_DIALOUGE() OR (NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex) AND NOT bWakeUp)
	//								IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
										KILL_ANY_CONVERSATION()
										PRINTLN("KILLING CONVERSATION - TOW TRUCK NO LONGER ATTACHED")
										
										PRINTLN("GOING TO STATE - ANGRY_PED_CHASE")
										pedState = ANGRY_PED_CHASE
	//								ENDIF
								ELSE
									IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex) AND bWakeUp
										IF bPlayInitialUpsetLine
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												PRINTLN("GOING TO STATE - ANGRY_PED_CHASE - BUMP CASE")
												pedState = ANGRY_PED_CHASE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
	//							PRINTLN("STOPPED HERE")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE ANGRY_PED_WAIT
//				DEBUG_MESSAGE("ANGRY_PED_WAIT")
				
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
					//IF GET_SEQUENCE_PROGRESS(brokenVehicles[0].pedIndex) >= 1 

					IF GET_DISTANCE_BETWEEN_ENTITIES(brokenVehicles[0].pedIndex, PLAYER_PED_ID()) < 12 //HACK.. TASK_GO_TO_ENTITY has a LONG delay before thinking it's done
						CLEAR_PED_TASKS(brokenVehicles[0].pedIndex)
						
						IF bAttack
							TASK_COMBAT_PED(brokenVehicles[0].pedIndex, PLAYER_PED_ID())
						ELIF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
							DEBUG_MESSAGE("play anim")
							OPEN_SEQUENCE_TASK(taskSequence)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								TASK_PLAY_ANIM(NULL, "oddjobs@towingpleadingidle_a", "idle_c", NORMAL_BLEND_OUT, SLOW_BLEND_DURATION, 5000)
							CLOSE_SEQUENCE_TASK(taskSequence)
							TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
							CLEAR_SEQUENCE_TASK(taskSequence)
						ENDIF
						
						DEBUG_MESSAGE(">= 1 or within 20m")
						bInCombat = TRUE
						
						IF bSexVariation
							pedState = ANGRY_PED_CHASE
						ELSE
							pedState = ANGRY_PED_DIALOUGE
						ENDIF
						
					ENDIF
				ENDIF
			BREAK
			CASE ANGRY_PED_DIALOUGE
	//			DEBUG_MESSAGE("ANGRY_PED_DIALOUGE")
				
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
					brokenVehicles[0].blipIndexPed = ADD_BLIP_FOR_ENTITY(brokenVehicles[0].pedIndex)
					IF bAttack
						SET_BLIP_COLOUR(brokenVehicles[0].blipIndexPed, BLIP_COLOUR_RED)
						SET_BLIP_SCALE(brokenVehicles[0].blipIndexPed, BLIP_SIZE_PED)
						PRINTLN("SETTING BLIP TO BE RED, BECAUSE bAttack IS TRUE")
					ELSE
						SET_BLIP_COLOUR(brokenVehicles[0].blipIndexPed, BLIP_COLOUR_BLUE)
						SET_BLIP_SCALE(brokenVehicles[0].blipIndexPed, BLIP_SIZE_PED)
						PRINTLN("SETTING BLIP TO BE BLUE, BECAUSE bAttack IS FALSE")
					ENDIF
				ENDIF
				
				IF bAttack
					IF chosenNodeType = NODE_TYPE_HANDI
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWABDHANDMAN2")
						PRINTLN("SETTING UP DIALOGUE FOR HANDICAP - ATTACK")
						REGISTER_QUICK_CONV_QUEUE(MyLocalPedStruct, "TOWAUD", "TOW_PED_ANGR")
					ELSE
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, brokenVehicles[0].pedIndex, "TOWABDATTACKM")
						PRINTLN("SETTING UP DIALOGUE FOR ABANDON - ATTACK")
						REGISTER_QUICK_CONV_QUEUE(MyLocalPedStruct, "TOWAUD", "TOW_HOM_DRG")
					ENDIF
				ELSE
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, brokenVehicles[0].pedIndex, sCharacterName)
					PRINTLN("SETTING UP DIALOGUE - REGULAR ?")
					REGISTER_QUICK_CONV_QUEUE(MyLocalPedStruct, "TOWAUD", angryPedRoot)
				ENDIF
				
				bSupressObjective = TRUE
				SETTIMERA(0)
				
				IF bAttack
					RESTART_TIMER_NOW(chaseTimer)
					pedState = ANGRY_PED_ENTER
				ELSE
					pedState = ANGRY_PED_CHASE
				ENDIF
			BREAK
			CASE ANGRY_PED_CHASE
			
//				PRINTLN("INSIDE - ANGRY_PED_CHASE")
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					IF bSexVariation 
						SETTIMERB(0)
					ENDIF
					
					RESTART_TIMER_NOW(chaseTimer)
					IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) 
					AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex) 
					AND NOT IS_ENTITY_DEAD(myVehicle)
						IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
							TASK_FOLLOW_TO_OFFSET_OF_ENTITY(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, <<0,0,0>>, PEDMOVEBLENDRATIO_SPRINT, -1, 10.0)
							PRINTLN("TASK GUY TO CHASE SINCE THE VEHICLE IS STILL ATTACHED TO TOW TRUCK")
						ELSE
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
								SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_UNLOCKED)
							ENDIF
						
							CLEAR_SEQUENCE_TASK(iSeq)
							OPEN_SEQUENCE_TASK(iSeq)
								TASK_ENTER_VEHICLE(NULL, brokenVehicles[0].vehicleIndex)
								TASK_VEHICLE_DRIVE_WANDER(NULL, brokenVehicles[0].vehicleIndex, 5.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
							CLOSE_SEQUENCE_TASK(iSeq)
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, iSeq)
								PRINTLN("TASKING GUY TO ENTER AND WANDER SINCE VEHICLE IS NO LONGER ATTACHED")
							ENDIF
							CLEAR_SEQUENCE_TASK(iSeq)
							bGuyTryingToEnterCar = TRUE
						ENDIF
					ENDIF
					
					PRINTLN("GOING TO STATE - ANGRY_PED_ENTER")
					pedState = ANGRY_PED_ENTER
				ENDIF
			BREAK
			CASE ANGRY_PED_ENTER
			
	//			PRINTLN("INSIDE STATE - ANGRY_PED_ENTER")
			
				UPDATE_PED_GIVE_UP()
				
				IF NOT bAngryPedGaveUp 				// Adding this flag here to prevent the ped from giving up then getting a late task to re-enter
					//take into account the sex ped
					IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
							IF bSexVariation 
								IF TIMERB() > 8000
									RESTART_TIMER_NOW(chaseTimer)
									IF NOT bGuyTryingToEnterCar
										SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_UNLOCKED)
										TASK_ENTER_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, DEFAULT_TIME_BEFORE_WARP)
										PRINTLN("TASKING PED TO ENTER VEHICLE")
									ENDIF
									
									PRINTLN("GOING TO STATE - ANGRY_PED_DRIVE - 01")
									pedState = ANGRY_PED_DRIVE
								ENDIF
							ELSE
								RESTART_TIMER_NOW(chaseTimer)
								SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_UNLOCKED)
								TASK_ENTER_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, DEFAULT_TIME_BEFORE_WARP)
								
								PRINTLN("GOING TO STATE - ANGRY_PED_DRIVE - 02")
								pedState = ANGRY_PED_DRIVE
							ENDIF
						ELSE
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
								IF TIMERB() > 7000
									IF NOT bAttack
										IF IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex)
											IF NOT IS_ENTITY_DEAD(myVehicle)
												IF GET_ENTITY_SPEED(myVehicle) < 2.0
													IF GET_SCRIPT_TASK_STATUS(brokenVehicles[0].pedIndex, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK
														TASK_VEHICLE_DRIVE_WANDER(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, 2.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
		//												PRINTLN("TASKING GUY DRIVE WANDER")
													ENDIF
												ELSE
													CLEAR_SEQUENCE_TASK(iSeq)
													OPEN_SEQUENCE_TASK(iSeq)
														TASK_LEAVE_ANY_VEHICLE(NULL, 5000, ECF_JUMP_OUT)
														TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
													CLOSE_SEQUENCE_TASK(iSeq)
													IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
														TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, iSeq)
														PRINTLN("TASKING GUY TO JUMP OUT")
													ENDIF
													CLEAR_SEQUENCE_TASK(iSeq)
													
													PRINTLN("GOING TO STATE - ANGRY_PED_DRIVE - 03")
													pedState = ANGRY_PED_DRIVE
												ENDIF
											ENDIF
										ELSE
											IF NOT IS_ENTITY_DEAD(myVehicle)
												IF GET_ENTITY_SPEED(myVehicle) < 5.0
													IF GET_SCRIPT_TASK_STATUS(brokenVehicles[0].pedIndex, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
														IF IS_VEHICLE_SEAT_FREE(brokenVehicles[0].vehicleIndex)
															TASK_ENTER_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, DEFAULT_TIME_NEVER_WARP)
		//													PRINTLN("TASKING HANDI CAP PED 2 TO ENTER HIS VEHICLE")
														ENDIF
													ENDIF
												ELSE
													IF GET_SCRIPT_TASK_STATUS(brokenVehicles[0].pedIndex, SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY) <> PERFORMING_TASK
														TASK_FOLLOW_TO_OFFSET_OF_ENTITY(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, <<0,0,0>>, PEDMOVEBLENDRATIO_SPRINT, -1, 10.0)
		//												PRINTLN("TASKING HANDI CAP PED 2 TO FOLLOW VEHICLE AGAIN")
													ENDIF
												ENDIF	
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE ANGRY_PED_DRIVE
				UPDATE_PED_GIVE_UP(TRUE)
				
				//TODO add a give up time or if you re-hook
				IF IS_PED_IN_ANY_VEHICLE(brokenVehicles[0].pedIndex) 
				AND NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
				
					IF bAttack
						angryPedRoot = "TOW_PED_ANTH"
					ELIF bSexVariation
						angryPedRoot = "TOW_ILLEG_SE"
					ENDIF
					
					PRINTLN("ANGRY PED IS IN HIS VEHICLE")
					
					bDriverGotCarBack = TRUE
					
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", angryPedRoot, CONV_PRIORITY_VERY_HIGH)
				
					bSupressObjective = FALSE
					TASK_VEHICLE_DRIVE_WANDER(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, 5.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
					//TASK_VEHICLE_DRIVE_WANDER(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleInde, 5.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
					pedState = ANGRY_PED_DONE
					RETURN TRUE
				ENDIF
			BREAK
			CASE ANGRY_PED_DONE
			
	//			PRINTLN("INSIDE STATE - ANGRY_PED_DONE")
			
				bInCombat = FALSE
				bSupressObjective = FALSE
				IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndexPed)
					REMOVE_BLIP(brokenVehicles[0].blipIndexPed)
				ENDIF
			BREAK
		ENDSWITCH
		RETURN FALSE
	ENDFUNC
#ENDIF

#IF IS_JAPANESE_BUILD
	
	FUNC BOOL UPDATE_ANGRY_PED_CLEAN()
		
		SWITCH pedState
			CASE ANGRY_PED_INIT
			
				IF bHomelessVariation
					IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						brokenVehicles[0].pedIndex = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].vehicleIndex, PEDTYPE_MISSION, pedNames[0], VS_FRONT_RIGHT)
						SET_MODEL_AS_NO_LONGER_NEEDED(pedNames[0])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
						SET_PED_CONFIG_FLAG(brokenVehicles[0].pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iPedID, brokenVehicles[0].pedIndex, sCharacterName)
						TASK_PLAY_ANIM(brokenVehicles[0].pedIndex,"RANDOM@CAR_SLEEPING","Sleeping_Idle",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						bPedCreated = TRUE
						PRINTLN("bPedCreated = TRUE - 03")
					ELSE
						PRINTLN("VEHICLE DEAD - NO PED CREATION")
					ENDIF
				ELIF GET_RANDOM_BOOL()
					DEBUG_MESSAGE("bAttack = TRUE")
					bAttack = FALSE
				ELSE
					DEBUG_MESSAGE("bAttack = FALSE")
					bAttack = FALSE
				ENDIF
				pedState = ANGRY_PED_CREATE
			BREAK
			CASE ANGRY_PED_CREATE
				
				//attaching the hook and bumping / horn should wake her up..
				IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
						IF NOT bHomelessVariation
							START_TIMER_AT(wakeTimer, 1.0)
						ELSE
							START_TIMER_NOW(wakeTimer)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_TIMER_STARTED(wakeTimer) AND GET_TIMER_IN_SECONDS(wakeTimer) > 1.0 
					IF IS_TIMER_STARTED(wakeTimer)
						IF bHomelessVariation
							//homeless attack variation.
							IF g_savedGlobals.sTowingData.iAbandonCompleted% 2 = 0
								IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
									OPEN_SEQUENCE_TASK(taskSequence)
										TASK_LEAVE_ANY_VEHICLE(NULL)
										TASK_GO_TO_ENTITY(NULL, myVehicle, DEFAULT_TIME_BEFORE_WARP, 9.0, PEDMOVEBLENDRATIO_WALK)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
										TASK_PLAY_ANIM(NULL, "oddjobs@towingangryidle_a", "idle_b", NORMAL_BLEND_OUT, SLOW_BLEND_DURATION, 7000)
										TASK_WANDER_STANDARD(NULL)
									CLOSE_SEQUENCE_TASK(taskSequence)
									IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
										TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
									ENDIF
									CLEAR_SEQUENCE_TASK(taskSequence)
								
		//							bEnableBonusCam = FALSE
								ENDIF
								
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, brokenVehicles[0].pedIndex, "TOWABDBITCHM")
								CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_ANGR", CONV_PRIORITY_VERY_HIGH)
							ELSE
								IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
									SETUP_PED_FOR_COMBAT(brokenVehicles[0].pedIndex)
									SET_PED_COMBAT_ATTRIBUTES(brokenVehicles[0].pedIndex, CA_USE_COVER, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(brokenVehicles[0].pedIndex, CA_USE_VEHICLE, FALSE)
									OPEN_SEQUENCE_TASK(taskSequence)
										TASK_LEAVE_ANY_VEHICLE(NULL)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
										//TASK_PLAY_ANIM(NULL, "gestures@male", "anger_a")
										TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
									CLOSE_SEQUENCE_TASK(taskSequence)
									IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
										TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
									ENDIF
									CLEAR_SEQUENCE_TASK(taskSequence)
									
									IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
										tempBlip = ADD_BLIP_FOR_ENTITY(brokenVehicles[0].pedIndex)
										SET_BLIP_COLOUR(tempBlip, BLIP_COLOUR_RED)
										SET_BLIP_SCALE(tempBlip, BLIP_SIZE_PED)
										REGISTER_BLIPPED_ENTITY(brokenVehicles[0].pedIndex, tempBlip, TRUE)
										PRINTLN("ADDING RED BLIP FOR PED")
									ENDIF
									
									bAttack = TRUE
									PRINTLN("SETTING bAttack = TRUE VIA bHomelessVariation")
									
		//							bEnableBonusCam = FALSE
								ENDIF
								CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_DRG", CONV_PRIORITY_VERY_HIGH)
							ENDIF
							//who woke me up?
							
							pedState = ANGRY_PED_EXIT
						ELIF bAttack
							brokenVehicles[0].pedIndex = CREATE_PED(PEDTYPE_CIVMALE, pedNames[0], illegalNodes[illegalIdx].vPedPos, illegalNodes[illegalIdx].fPedRot) 
							PRINTLN("CREATING PED - bAttack IS TRUE")
							
							//GIVE_WEAPON_TO_PED(brokenVehicles[0].pedIndex, WEAPONTYPE_PISTOL, 200)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
							SET_PED_CONFIG_FLAG(brokenVehicles[0].pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
							bPedCreated = TRUE
							PRINTLN("bPedCreated = TRUE - 04")
							
							angryPedRoot = "TOW_PED_ANGR"
							bPedCreated = FALSE
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								OPEN_SEQUENCE_TASK(taskSequence)
									TASK_GO_TO_ENTITY(NULL, myVehicle, DEFAULT_TIME_BEFORE_WARP, 15.0, PEDMOVEBLENDRATIO_SPRINT) //brokenVehicles[0].vehicleIndex
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(taskSequence)
								TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
								CLEAR_SEQUENCE_TASK(taskSequence)
		//						bEnableBonusCam = FALSE
							ENDIF
							pedState = ANGRY_PED_WAIT
						ELSE
							brokenVehicles[0].pedIndex = CREATE_PED(PEDTYPE_CIVMALE, pedNames[0], illegalNodes[illegalIdx].vPedPos, illegalNodes[illegalIdx].fPedRot) 
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
							SET_PED_CONFIG_FLAG(brokenVehicles[0].pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
							PRINTLN("CREATING PED - REGULAR")
							SET_ENTITY_LOAD_COLLISION_FLAG(brokenVehicles[0].pedIndex, TRUE)
							SET_PED_MOVEMENT_CLIPSET(brokenVehicles[0].pedIndex, "move_m@JOG@")
							SET_MODEL_AS_NO_LONGER_NEEDED(pedNames[0])
							
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, brokenVehicles[0].pedIndex, "TOWHANDIBEGM")
							
							bPedCreated = TRUE
							PRINTLN("bPedCreated = TRUE - 05")
							
							angryPedRoot = "TOW_ILLEG1"
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								OPEN_SEQUENCE_TASK(taskSequence)
									TASK_GOTO_ENTITY_OFFSET(NULL, PLAYER_PED_ID(), DEFAULT_TIME_BEFORE_WARP, 8.0, 0, PEDMOVEBLENDRATIO_RUN)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									//TASK_PLAY_ANIM(NULL, "gestures@male", "anger_a")
								CLOSE_SEQUENCE_TASK(taskSequence)
								IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
									TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
								ENDIF
								CLEAR_SEQUENCE_TASK(taskSequence)
		//						bEnableBonusCam = FALSE
							ENDIF
							pedState = ANGRY_PED_WAIT
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE ANGRY_PED_EXIT
	//			PRINTLN("INSIDE STATE - ANGRY_PED_EXIT")
				
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
					IF NOT IS_PED_IN_ANY_VEHICLE(brokenVehicles[0].pedIndex)
						IF bHomelessVariation
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
								IF GET_SCRIPT_TASK_STATUS(brokenVehicles[0].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
									IF GET_SEQUENCE_PROGRESS(brokenVehicles[0].pedIndex) = 2 // check task status
										IF g_savedGlobals.sTowingData.iAbandonCompleted% 2 = 0 //TODO pick the right enums for variaitons.. male - female
											CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_ANG1", CONV_PRIORITY_VERY_HIGH)
										ELSE
											CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_ANG2", CONV_PRIORITY_VERY_HIGH)
										ENDIF
										
										PRINTLN("GOING TO STATE - ANGRY_PED_DONE")
										pedState = ANGRY_PED_DONE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
								IF UPDATE_PED_DIALOUGE() OR NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
	//								IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
										KILL_ANY_CONVERSATION()
										PRINTLN("KILLING CONVERSATION - TOW TRUCK NO LONGER ATTACHED")
										
										PRINTLN("GOING TO STATE - ANGRY_PED_CHASE")
										pedState = ANGRY_PED_CHASE
	//								ENDIF
								ENDIF
							ELSE
	//							PRINTLN("STOPPED HERE")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE ANGRY_PED_WAIT
	//			DEBUG_MESSAGE("ANGRY_PED_WAIT")
				
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
					//IF GET_SEQUENCE_PROGRESS(brokenVehicles[0].pedIndex) >= 1 

					IF GET_DISTANCE_BETWEEN_ENTITIES(brokenVehicles[0].pedIndex, PLAYER_PED_ID()) < 12 //HACK.. TASK_GO_TO_ENTITY has a LONG delay before thinking it's done
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
							CLEAR_PED_TASKS(brokenVehicles[0].pedIndex)
						ENDIF
						
						IF bAttack
							TASK_COMBAT_PED(brokenVehicles[0].pedIndex, PLAYER_PED_ID())
						ELIF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
							DEBUG_MESSAGE("play anim")
							OPEN_SEQUENCE_TASK(taskSequence)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								TASK_PLAY_ANIM(NULL, "oddjobs@towingpleadingidle_a", "idle_c", NORMAL_BLEND_OUT, SLOW_BLEND_DURATION, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(taskSequence)
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
							ENDIF
							CLEAR_SEQUENCE_TASK(taskSequence)
						ENDIF
						
						DEBUG_MESSAGE(">= 1 or within 20m")
						bInCombat = TRUE
						
						SETTIMERB(0)
						RESTART_TIMER_NOW(chaseTimer)
						PRINTLN("GOING TO STATE - ANGRY_PED_DIALOUGE - 01")
						pedState = ANGRY_PED_DIALOUGE
					ENDIF
				ENDIF
			BREAK
			CASE ANGRY_PED_DIALOUGE
	//			DEBUG_MESSAGE("ANGRY_PED_DIALOUGE")
				
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iPedID, brokenVehicles[0].pedIndex, sCharacterName)
				
				bSupressObjective = TRUE
				REGISTER_QUICK_CONV_QUEUE(MyLocalPedStruct, "TOWAUD", angryPedRoot)
		
				SETTIMERA(0)
				
				IF bAttack
					RESTART_TIMER_NOW(chaseTimer)
					pedState = ANGRY_PED_ENTER
				ELSE
					PRINTLN("GOING TO STATE - ANGRY_PED_ENTER - 01")
					pedState = ANGRY_PED_ENTER
				ENDIF
			BREAK
			CASE ANGRY_PED_CHASE
			
	//			PRINTLN("INSIDE - ANGRY_PED_CHASE")
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				
					RESTART_TIMER_NOW(chaseTimer)
					IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) 
					AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex) 
					AND NOT IS_ENTITY_DEAD(myVehicle)
						IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
							TASK_FOLLOW_TO_OFFSET_OF_ENTITY(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, <<0,0,0>>, PEDMOVEBLENDRATIO_RUN, -1, 10.0)
							PRINTLN("TASK GUY TO CHASE SINCE THE VEHICLE IS STILL ATTACHED TO TOW TRUCK")
						ENDIF
					ENDIF
					
					PRINTLN("GOING TO STATE - ANGRY_PED_ENTER - 02")
					pedState = ANGRY_PED_ENTER
				ENDIF
			BREAK
			CASE ANGRY_PED_ENTER
			
	//			PRINTLN("INSIDE STATE - ANGRY_PED_ENTER")
			
				UPDATE_PED_GIVE_UP(FALSE)
				
				IF NOT bAngryPedGaveUp 				// Adding this flag here to prevent the ped from giving up then getting a late task to re-enter
					//take into account the sex ped
					IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
	//						IF TIMERB() > 5000
	//							
	//							RESTART_TIMER_NOW(chaseTimer)
	//							SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_UNLOCKED)
	//							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
	//								TASK_ENTER_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, DEFAULT_TIME_BEFORE_WARP)
	//								PRINTLN("GOING TO STATE - ANGRY_PED_DRIVE - 02")
	//							ENDIF
								
								pedState = ANGRY_PED_DRIVE
	//						ENDIF
						ELSE
							PRINTLN("GOING TO STATE - ANGRY_PED_DRIVE - 03")
							pedState = ANGRY_PED_DRIVE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE ANGRY_PED_DRIVE
			
				UPDATE_PED_GIVE_UP(FALSE)
				
				//TODO add a give up time or if you re-hook
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
					IF IS_PED_IN_ANY_VEHICLE(brokenVehicles[0].pedIndex) 
					AND (NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex) AND 
					NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex))
					
						IF bAttack
							angryPedRoot = "TOW_PED_ANTH"
						ENDIF
						
						PRINTLN("ANGRY PED IS IN HIS VEHICLE")
						
						bDriverGotCarBack = TRUE
						
						CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", angryPedRoot, CONV_PRIORITY_VERY_HIGH)
					
						bSupressObjective = FALSE
						TASK_VEHICLE_DRIVE_WANDER(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, 5.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
						//TASK_VEHICLE_DRIVE_WANDER(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleInde, 5.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
						pedState = ANGRY_PED_DONE
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE ANGRY_PED_DONE
			
	//			PRINTLN("INSIDE STATE - ANGRY_PED_DONE")
			
				bInCombat = FALSE
				bSupressObjective = FALSE
				IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndexPed)
					REMOVE_BLIP(brokenVehicles[0].blipIndexPed)
				ENDIF
			BREAK
		ENDSWITCH
		RETURN FALSE
	ENDFUNC
#ENDIF

FUNC BOOL HANDLE_CREATE_STATE(STRING sObjective, STRING sHelp, INT& openIdx, BOOL bDynamic = FALSE, BOOL bWithPed = FALSE)
	DEBUG_MESSAGE("HANDLE_CREATE_STATE")
	fObjDist = GET_PLAYER_DISTANCE_FROM_LOCATION(getPointVec, FALSE)
	vCurDestObjective[0] = getPointVec
	
	//Moved actual prints to MISSION_STATE_PRINTS.. not ideal but needed to get cars spawning earlier.
	sMyObjective = sObjective
	sHelp = sHelp
	//sMyHelp = sHelp
			
	IF bDynamic
		IF CREATE_BROKEN_DOWN_VEHICLE(getPointVec, getPointHead, openIdx, FALSE)
			INIT_DAMAGE_TRACKING(brokenVehicles[0].vehicleIndex, iLastHealth, iInitialHealth)
			SET_FORCE_HD_VEHICLE(brokenVehicles[0].vehicleIndex, TRUE)
			SET_VEHICLE_TYRES_CAN_BURST(brokenVehicles[0].vehicleIndex, FALSE)
			SET_ENTITY_LOAD_COLLISION_FLAG(brokenVehicles[0].vehicleIndex, TRUE)
			SET_VEHICLE_HAS_STRONG_AXLES(brokenVehicles[0].vehicleIndex, TRUE)
			PRINTLN("HANDLE_CREATE_STATE - bDynamic IS TRUE")
			RETURN TRUE
		ENDIF
	ELSE
		IF CREATE_BROKEN_VEHICLE_ILLEGAL(bWithPed, FALSE)
			INIT_DAMAGE_TRACKING(brokenVehicles[0].vehicleIndex, iLastHealth, iInitialHealth)
			SET_FORCE_HD_VEHICLE(brokenVehicles[0].vehicleIndex, TRUE)
			SET_VEHICLE_TYRES_CAN_BURST(brokenVehicles[0].vehicleIndex, FALSE)
			SET_ENTITY_LOAD_COLLISION_FLAG(brokenVehicles[0].vehicleIndex, TRUE)
			SET_VEHICLE_HAS_STRONG_AXLES(brokenVehicles[0].vehicleIndex, TRUE)
			PRINTLN("HANDLE_CREATE_STATE - bDynamic IS FALSE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FIND_CLOSE_GARAGE()
	INT idx 
	idx = FIND_CLOSEST_LOCATION_INDEX(vGarages, vCurDestObjective[0], 400)
	IF idx <> -1
		vDest = vGarages[idx]
		GET_DATA_FROM_GARAGE_LOCATION(idx, myGarageData)
	ELSE
		SCRIPT_ASSERT("Couldn't find a garage defaulting to 0")
		vDest = vGarages[0]
	ENDIF
ENDPROC

PROC HANDLE_TIMER()
	IF NOT bStopTimer
		INT iTimer = ROUND((FAIL_TIME_TOTAL - GET_TIMER_IN_SECONDS_SAFE(failTimer)) * 1000.0)
	//	PRINTLN("TIMER = ", iTimer)
		
		IF chosenNodeType = NODE_TYPE_DYNAMIC
			IF iTimer/1000 < 60
				DRAW_GENERIC_TIMER(iTimer, "TOW_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1,
										PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
			ELSE
				DRAW_GENERIC_TIMER(iTimer, "TOW_TIME")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_ADDITIONAL_HELP_PRINTS()
	INT iRand
	
	IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex) 
	AND IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
	OR ((g_savedGlobals.sTowingData.iTowingJobsCompleted = 0) OR g_savedGlobals.sTowingData.iTowingJobsCompleted > 2)
//		PRINTLN("EXITING EARLY")
		EXIT
	ENDIF
	
	SWITCH iAdditionalHelpPrintStages
		CASE 0
			IF NOT IS_TIMER_STARTED(tAdditionalHelpPrintTimer)
				START_TIMER_NOW(tAdditionalHelpPrintTimer)
				PRINTLN("STARTING TIMER - tAdditionalHelpPrintTimer")
				
				iAdditionalHelpPrintStages = 1
			ENDIF
		BREAK
		//-------------------------------------------------------------------------
		CASE 1
			IF IS_TIMER_STARTED(tAdditionalHelpPrintTimer)
				IF GET_TIMER_IN_SECONDS(tAdditionalHelpPrintTimer) > 10
					IF NOT IS_HELP_MESSAGE_ON_SCREEN() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
						iRand = GET_RANDOM_INT_IN_RANGE() % 2
						IF iRand = 0
							PRINT_HELP("TOW_TUT_04C")
							PRINTLN("PRINTING FIRST ADDITIONAL HELP MESSAGE - A")
						ELIF iRand = 1
							PRINT_HELP("TOW_TUT_02")
							PRINTLN("PRINTING FIRST ADDITIONAL HELP MESSAGE - B")
						ELSE
							PRINT_HELP("TOW_TUT_04C")
							PRINTLN("PRINTING FIRST ADDITIONAL HELP MESSAGE - C")
						ENDIF
						
						RESTART_TIMER_NOW(tAdditionalHelpPrintTimer)
						
						iAdditionalHelpPrintStages = 2
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//-------------------------------------------------------------------------
		CASE 2
			IF IS_TIMER_STARTED(tAdditionalHelpPrintTimer)
				IF GET_TIMER_IN_SECONDS(tAdditionalHelpPrintTimer) > 15
					IF NOT IS_HELP_MESSAGE_ON_SCREEN() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINT_HELP("TOW_TUT_03")
						PRINTLN("PRINTING SECOND ADDITIONAL HELP MESSAGE")
						
						RESTART_TIMER_NOW(tAdditionalHelpPrintTimer)
						
						iAdditionalHelpPrintStages = 3
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//-------------------------------------------------------------------------
		CASE 3
			IF IS_TIMER_STARTED(tAdditionalHelpPrintTimer)
				IF GET_TIMER_IN_SECONDS(tAdditionalHelpPrintTimer) > 15
					IF NOT IS_HELP_MESSAGE_ON_SCREEN() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINT_HELP("TOW_TUT_01")
						PRINTLN("PRINTING THIRD ADDITIONAL HELP MESSAGE")
						iAdditionalHelpPrintStages = 4
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//-------------------------------------------------------------------------
		CASE 4
		
		BREAK
		//-------------------------------------------------------------------------
	ENDSWITCH

ENDPROC

PROC SET_SCALEFORM_MIDSIZED_MESSAGE_WITH_THREE_NUMBERS_IN_STRAPLINE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, INT iNum, INT iNum2, INT iNum3, STRING strapLine, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW)

	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SHOW_SHARD_MIDSIZED_MESSAGE") 
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING") 
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor) 
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp) 
		END_TEXT_COMMAND_SCALEFORM_STRING() 
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapline)
			ADD_TEXT_COMPONENT_INTEGER(iNum) 
			ADD_TEXT_COMPONENT_INTEGER(iNum2) 
			ADD_TEXT_COMPONENT_INTEGER(iNum3) 
		END_TEXT_COMMAND_SCALEFORM_STRING() 
	END_SCALEFORM_MOVIE_METHOD() 

	// Restart our timer for updating. 
	RESTART_TIMER_NOW(scaleformStruct.movieTimer) 

	// Store the duration. 
	scaleformStruct.iDuration = iDuration 
	scaleformStruct.eEndFlash = HUD_COLOUR_WHITE
ENDPROC



//Core logic for the traditional towing modes of broken down and illegally parked
FUNC BOOL UPDATE_TRADITIONAL()
	INT openIdx, iStatsTime
	STRING objective
	VECTOR vAngledA, vAngledB
	FLOAT fWidth
	
	SWITCH curStage
		CASE MISSION_STATE_INTRO
			DEBUG_MESSAGE("MISSION_STATE_INTRO")
			
			SET_RICH_PRESENCE_FOR_SP_MINIGAME(MINIGAME_TOWING)
			PRINTLN("SETTING RICH PRESENCE AT START")
			
			IF chosenNodeType = NODE_TYPE_DYNAMIC
				//we need to get starting data
				IF GET_POINT(getPointVec, getPointHead, vExtra, vExtra2)
					curStage = MISSION_STATE_CONVERSATION
					DEBUG_MESSAGE("Calling GET_POINT")
				ENDIF
			ELSE
				//we already have our data continue
				curStage = MISSION_STATE_CONVERSATION
			ENDIF
			
		BREAK
		CASE MISSION_STATE_CONVERSATION
//			DEBUG_MESSAGE("MISSION_STATE_CONVERSATION")	
			
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				SWITCH chosenNodeType
					CASE NODE_TYPE_DYNAMIC
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
				
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_ACC_EXP2", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("DYNAMIC: GOING TO STATE - MISSION_STATE_CREATE VIA MISSION_STATE_CONVERSATION")
							curStage = MISSION_STATE_CREATE
						ENDIF
					BREAK
					CASE NODE_TYPE_ABANDON
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
						
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_ABN_EXP2", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("ABANDON: GOING TO STATE - MISSION_STATE_CREATE VIA MISSION_STATE_CONVERSATION")
							curStage = MISSION_STATE_CREATE
						ENDIF
					BREAK
					CASE NODE_TYPE_HANDI
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
					
						IF bSexVariation
							IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HAN_EXP2", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("HANDI-SEX: GOING TO STATE - MISSION_STATE_CREATE VIA MISSION_STATE_CONVERSATION")
								curStage = MISSION_STATE_CREATE
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HAN_EXP2", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("HANDI-REG: GOING TO STATE - MISSION_STATE_CREATE VIA MISSION_STATE_CONVERSATION")
								curStage = MISSION_STATE_CREATE
							ENDIF
						ENDIF
						
					BREAK
					CASE NODE_TYPE_TRAIN
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
						
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_TRN_EXP2", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("TRAIN: GOING TO STATE - MISSION_STATE_CREATE VIA MISSION_STATE_CONVERSATION")
							curStage = MISSION_STATE_CREATE
						ENDIF
					BREAK
					CASE NODE_TYPE_ACCIDENT
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
					
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_ACD_EXP2", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("ACCIDENT: GOING TO STATE - MISSION_STATE_CREATE VIA MISSION_STATE_CONVERSATION")
							curStage = MISSION_STATE_CREATE
						ENDIF
					BREAK
				ENDSWITCH	
			ENDIF
		BREAK
		CASE MISSION_STATE_CREATE
			DEBUG_MESSAGE("MISSION_STATE_CREATE")
			//SETUP_MINIGAME_INSTRUCTIONS(helpScaleform, TRUE, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"TOW_CTRL_01", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT),"TOW_CTRL_04", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y),"TOW_CTRL_02") //GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB),"TOW_CTRL_03",
			SWITCH chosenNodeType
				CASE NODE_TYPE_DYNAMIC
					DEBUG_MESSAGE("NODE_TYPE_DYNAMIC")
					IF HANDLE_CREATE_STATE("TOWT_OBJ_01", "TOWT_HELP_01", openIdx, TRUE)
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
							SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_CANNOT_ENTER)
						ENDIF
						FIND_CLOSE_GARAGE()
						START_TIMER_NOW(failTimer)
						curStage = MISSION_STATE_PRINTS
					ENDIF
				BREAK
				CASE NODE_TYPE_ABANDON
					DEBUG_MESSAGE("NODE_TYPE_ABANDON")
					IF HANDLE_CREATE_STATE("TOWT_OBJ_AB", "TOWT_HELP_AB", openIdx, FALSE)	
						vMin = <<vCurDestObjective[0].x+15.0, vCurDestObjective[0].y+15.0, vCurDestObjective[0].z+15.0>>
						vMax = <<vCurDestObjective[0].x-15.0, vCurDestObjective[0].y-15.0, vCurDestObjective[0].z-15.0>>
						SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMin, vMax, FALSE)
						bGenDisabled = TRUE
						
						SET_VEHICLE_UNDRIVEABLE(brokenVehicles[0].vehicleIndex, TRUE)
						PRINTLN("SETING VEHICLE AS UNDRIVEABLE - ABANDONDED")
						
						START_TIMER_NOW(failTimer)
						curStage = MISSION_STATE_PRINTS
					ENDIF
				BREAK
				CASE NODE_TYPE_HANDI
					DEBUG_MESSAGE("NODE_TYPE_HANDI")
					IF HANDLE_CREATE_STATE("TOWT_OBJ_01B", "TOWT_HELP_03", openIdx, FALSE)
						START_TIMER_NOW(failTimer)
						curStage = MISSION_STATE_PRINTS	
					ENDIF
				BREAK
				CASE NODE_TYPE_TRAIN
					DEBUG_MESSAGE("NODE_TYPE_TRAIN")
					IF HANDLE_CREATE_STATE("TOWT_OBJ_TR", "TOWT_HELP_TR", openIdx, FALSE, TRUE)
					
						SET_VEHICLE_UNDRIVEABLE(brokenVehicles[0].vehicleIndex, TRUE)
						PRINTLN("SETING VEHICLE AS UNDRIVEABLE - TRAIN")
						
						FIND_CLOSE_GARAGE()
						
						SET_RANDOM_TRAINS(FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_MAIN, FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_QUARRY, FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_CAR_STEAL_SETPIECE, FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_METRO, FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_FIRST_HOOD_SETPIECE, FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_SECOND_HOOD_SETPIECE, FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_SECOND_FAME_OR_SHAME_SETPIECE, FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_DOWNTOWN_SETPIECE, FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_THIRD_HOOD_SETPIECE, FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_PROLOGUE, FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_JEWEL_HEIST_SETPIECE, FALSE)
						SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_PROLOGUE_2, FALSE)
						
						CPRINTLN(DEBUG_MISSION, "DISABLING TRAINS")
						
						bTrainsDisabled = TRUE
						START_TIMER_NOW(failTimer)
						curStage = MISSION_STATE_PRINTS
					ENDIF
				BREAK
				CASE NODE_TYPE_ACCIDENT
					DEBUG_MESSAGE("NODE_TYPE_ACCIDENT")
					IF HANDLE_CREATE_STATE("TOWT_OBJ_CC", "TOWT_HELP_04", openIdx, FALSE, FALSE)
						
						SET_VEHICLE_UNDRIVEABLE(brokenVehicles[0].vehicleIndex, TRUE)
						PRINTLN("SETING VEHICLE AS UNDRIVEABLE - ACCIDENT")
						
						FIND_CLOSE_GARAGE()
						START_TIMER_NOW(failTimer)
						curStage = MISSION_STATE_PRINTS	
					ENDIF
				BREAK
				DEFAULT
					DEBUG_MESSAGE("NO node type selected")
				BREAK	
			ENDSWITCH
			
			IF NOT DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
				IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
					brokenVehicles[0].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[0].vehicleIndex)
					SET_BLIP_COLOUR(brokenVehicles[0].blipIndex, BLIP_COLOUR_BLUE)
					SET_BLIP_ROUTE(brokenVehicles[0].blipIndex, TRUE)
					PRINTLN("CREATING TOWING BLIP")
				ENDIF
			ENDIF
		BREAK
		CASE MISSION_STATE_PED
//			DEBUG_MESSAGE("MISSION_STATE_PED")
			
			HANDLE_TIMER()
			//TODO this should be pulled out
			IF UPDATE_BROKEN_VEHICLE_PED(brokenState, 0, "TOW_BREAK1")
				curStage = MISSION_STATE_TOWING
			ENDIF
		BREAK
		CASE MISSION_STATE_PRINTS
//			DEBUG_MESSAGE("MISSION_STATE_PRINTS")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//SETUP_MINIGAME_INSTRUCTIONS(helpScaleform, TRUE, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"TOW_CTRL_01", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT),"TOW_CTRL_04", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y),"TOW_CTRL_02") //GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB),"TOW_CTRL_03",
				PRINT_NOW(sMyObjective, DEFAULT_GOD_TEXT_TIME, 1)
				sLastObjective = sMyObjective
				//REGISTER_PRINT_DELAY(printTimer, sMyHelp)
				
				IF chosenNodeType = NODE_TYPE_DYNAMIC
					curStage = MISSION_STATE_PED
				ELSE
					curStage = MISSION_STATE_TOWING
				ENDIF
			ENDIF
		BREAK		
		CASE MISSION_STATE_TOWING
//			DEBUG_MESSAGE("MISSION_STATE_TOWING")
			SWITCH chosenNodeType
				CASE NODE_TYPE_DYNAMIC
				CASE NODE_TYPE_TRAIN
				CASE NODE_TYPE_ACCIDENT
					objective = "TOWT_OBJ_04"
				BREAK
				CASE NODE_TYPE_ABANDON
				CASE NODE_TYPE_HANDI
					//if we havn't already set VDest
					IF IS_VECTOR_ZERO(vDest)
						vDest = vImpound[0]
					ENDIF
					GET_DATA_FROM_GARAGE_LOCATION(8, myGarageData) // because the impound is the last case
					//tow to impound
					objective = "TOWT_OBJ_06"
				BREAK
			ENDSWITCH
			
			IF chosenNodeType = NODE_TYPE_TRAIN AND NOT IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, myVehicle)
				bSupressObjective = TRUE
			ENDIF
			
			HANDLE_ADDITIONAL_HELP_PRINTS()
			
			IF CHECK_TOW_ATTACHMENTS(objective, vDest)
				IF chosenNodeType = NODE_TYPE_TRAIN AND NOT IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, myVehicle)
					bSupressObjective = FALSE
					bReverseOrder = TRUE
					IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
						brokenVehicles[0].blipIndexPed = ADD_BLIP_FOR_ENTITY(brokenVehicles[0].pedIndex)
						SET_BLIP_COLOUR(brokenVehicles[0].blipIndexPed, BLIP_COLOUR_BLUE)
						SET_BLIP_SCALE(brokenVehicles[0].blipIndexPed, BLIP_SIZE_PED)
					ENDIF
					
					curStage = MISSION_STATE_PED
				ELSE
					curStage = MISSION_STATE_DROPOFF
				ENDIF
			ENDIF
			
			//Added support for pushing the vehicle in the proper spot
			IF IS_VEHICLE_IN_DROP_OFF_AREA(vAngledA, vAngledB, fWidth)
				//Rammed!
				bRammedVehicle = TRUE
				curStage = MISSION_STATE_DROPOFF
			ENDIF
		
		BREAK
		CASE MISSION_STATE_DROPOFF
//			PRINTLN("MISSION_STATE_DROPOFF")
		
			//todo this is a bit hacky..lets solve this better
			IF chosenNodeType = NODE_TYPE_DYNAMIC
				//broken down vehicle
				objective = "TOWT_OBJ_03a"
			ELSE
				objective = "TOWT_OBJ_03Ga"
			ENDIF
			
			IF CHECK_TOWING(objective)
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(brokenVehicles[0].vehicleIndex, FALSE)
				IF curBrokenCarCount = 0
					bDisplayHelp = FALSE
					DEBUG_MESSAGE("curBrokenCarCount = 0")
					curStage = MISSION_STATE_ENDUI_SETUP
				ELSE
					PRINT_NOW("TOWT_OBJ_05", DEFAULT_GOD_TEXT_TIME, 1)
					sLastObjective = "TOWT_OBJ_05"
					curStage = MISSION_STATE_TOWING
				ENDIF
			ENDIF
		BREAK
		CASE MISSION_STATE_ENDUI_SETUP
//			PRINTLN("UPDATING MISSION_STATE_ENDUI_SETUP")
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				HANDLE_TOW_SUCCESS()
				
				IF bUnlockDoors
					IF DOES_BLIP_EXIST(myVehicleBlip)
						REMOVE_BLIP(myVehicleBlip)
						PRINTLN("REMOVING BLIP - myVehicleBlip VIA bUnlockDoors")
					ELSE
						PRINTLN("BLIP DOESN'T EXIST")
					ENDIF
				ELSE
					PRINTLN("bUnlockDoors IS FALSE")
				ENDIF
				
				CLEAR_HELP()
				
				fTotalPayment += PAYMENT_NORMAL
				PRINTLN("BEFORE: fTotalPayment = ", fTotalPayment)
				
//				SET_ENDSCREEN_DATASET_HEADER(towingEndScreen, "TOW_PASS", "TOW_TITLE")
				
				IF fUnhookStartTime <> 0 AND NOT bRammedVehicle //no bonus if you rammed it in
					//Add a bonus for getting vehicle in the processing zone quickly
					IF bPostTonya
						IF GET_GAME_TIMER() - fUnhookStartTime < 12000
//							ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(towingEndScreen, ESEF_RAW_INTEGER, "TOW_UI04B", "", 50, 0, ESCM_CHECKED)
							fTotalPayment += 50
							PRINTLN("UNHOOK BONUS: fTotalPayment = ", fTotalPayment)
						ELSE
//							ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(towingEndScreen, ESEF_RAW_INTEGER, "TOW_UI04B", "", 0, 0, ESCM_NO_MARK)
						ENDIF
						PRINTLN("Unhook time: ", GET_GAME_TIMER() - fUnhookStartTime)
					ENDIF
				ENDIF
				
				IF NOT bPostTonya
					fTotalPayment = 0
					PRINTLN("SINCE WE'RE DOING TONYA MISSIONS, NO PAYMENT GIVEN")
				ENDIF
				
				fMissionTime = GET_TIMER_IN_SECONDS(totalTimer)
				fMissionTime = (fMissionTime*1000)
				PRINTLN("MISSION TIME = ")
				
				iStatsTime = ROUND(fMissionTime)
				PRINTLN("iStatsTime = ", iStatsTime)
				
				// Stat Tracking
				PLAYSTATS_ODDJOB_DONE(iStatsTime, ENUM_TO_INT(MINIGAME_TOWING))
				PRINTLN("CALLING PLAYSTATS_ODDJOB_DONE - MINIGAME_TOWING")
				
//				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(towingEndScreen, ESEF_TIME_M_S, "TOW_UI03", "", FLOOR(fMissionTime), 0, ESCM_NO_MARK)
				
				IF chosenNodeType <> NODE_TYPE_ABANDON
					//Abandoned doesn't need these two
//					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(towingEndScreen, ESEF_RAW_INTEGER, "TOW_UI04", "", ROUND(fTotalFine), 0, ESCM_NO_MARK)
//					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(towingEndScreen, ESEF_RAW_INTEGER, "TOW_UI05", "", ROUND(fTotalPayment), 0, ESCM_NO_MARK)
				ENDIF
				
				IF bPostTonya
					fTotalPayment -= fTotalFine
					PRINTLN("AFTER: fTotalPayment = ", fTotalPayment)
				ELSE
					PRINTLN("WE'RE NOT POST TONYA")
				ENDIF
			
				//ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(towingEndScreen, ESEF_RAW_INTEGER, "TOW_UI06", "", ROUND(fTotalPayment), 0, ESCM_NO_MARK)
				
				iTotalPayment = ROUND(fTotalPayment)
				PRINTLN("iTotalPayment = ", iTotalPayment)
				
//				SET_SCALEFORM_BIG_MESSAGE_WITH_NUMBER_IN_STRAPLINE(endMessage, "TOW_UI07", ROUND(fTotalPayment), "TOW_UI08", 4000, HUD_COLOUR_WHITE, MG_BIG_MESSAGE_MIDSIZED)
				SET_SCALEFORM_MIDSIZED_MESSAGE_WITH_THREE_NUMBERS_IN_STRAPLINE(endMessage, "TOW_UI07", 500, ROUND(fTotalFine), ROUND(fTotalPayment), "TOW_UI08", 4000, HUD_COLOUR_WHITE)

				IF iTotalPayment > 0
					//CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, iTotalPayment)
					PROPERTY_PAY_INCOME(PROPERTY_TOWING_IMPOUND, iTotalPayment)
				ENDIF
				
				MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
				
				curStage = MISSION_STATE_ENDUI_WAIT
//			ENDIF
		BREAK
		CASE MISSION_STATE_ENDUI_WAIT
			
			IF IS_HELP_MESSAGE_ON_SCREEN()
				CLEAR_HELP()
			ENDIF
		
//			IF ENDSCREEN_PREPARE(towingEndScreen)
			IF NOT UPDATE_SHARD_BIG_MESSAGE(endMessage)
			
				CLEAR_THIS_PRINT("TOW_TUT_04A")
				CLEAR_THIS_PRINT("TOWT_OBJ_03Ga")
				
//				PRINTLN("LAUNCH_MINIGAME_MISSION_TRACKER HAS RETURNED TRUE")
//				IF NOT IS_TIMER_STARTED(tEndingTimer)
//					START_TIMER_NOW(tEndingTimer)
//					PRINTLN("STARTING tEndingTimer")
//				ENDIF
				
				curStage = MISSION_STATE_COMPLETE
			ENDIF
		BREAK
		CASE MISSION_STATE_COMPLETE
		
			PRINTLN("TOWING SCRIPT IS DONE")
			RETURN TRUE
		
			//Hack.. menu doesn't seem to dispaly properly if we shut down
//			IF RENDER_ENDSCREEN(towingEndScreen, TRUE)
//				IF IS_TIMER_STARTED(tEndingTimer)
//					IF GET_TIMER_IN_SECONDS(tEndingTimer) > 1.5
//						PRINTLN("TOWING SCRIPT IS DONE")
//						RETURN TRUE
//					ELSE
//	//					PRINTLN("WAITING ON tEndingTimer")
//					ENDIF
//				ELSE
//					PRINTLN("tEndingTimer IS NOT STARTED")
//				ENDIF
//			ENDIF
		BREAK
	ENDSWITCH

	vImpound[0] = vImpound[0]
	RETURN FALSE
ENDFUNC

PROC TELEPORT_PLAYER(VECTOR myPos)
	WHILE NOT FADE_DOWN()
		WAIT(0)
	ENDWHILE
	
	LOAD_SCENE(myPos)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND DOES_ENTITY_EXIST(myVehicle)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
		ENDIF
	ENDIF
	
	WAIT(500)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	WHILE NOT FADE_UP()
		WAIT(0)
	ENDWHILE
ENDPROC

// initialise mission
PROC INIT_MISSION(TOWING_LAUNCH_DATA& launchArgs)
	INT idx, numPedCohices
	
	SET_AGGRO_FIELDS_TO_NOT_CHECK()
	
	//INIT_ZVOLUME_WIDGETS()
	
	endMessage.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	
	//only for ones with a ped
	IF launchArgs.nodeType = NODE_TYPE_DYNAMIC
		SpecialAnims[0] = "base"
		ADD_STREAMED_ANIM(streamReqsAnim, "amb@world_human_smoking@male@male_a@base")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towingcome_here")
		ADD_STREAMED_ANIM(streamReqsAnim, "move_m@JOG@")
	ELIF (launchArgs.nodeType = NODE_TYPE_HANDI OR launchArgs.nodeType = NODE_TYPE_ABANDON) AND NOT bDoTutorial
		ADD_STREAMED_ANIM(streamReqsAnim, "Ped")
//		ADD_STREAMED_ANIM(streamReqsAnim, "GESTURES@MALE")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towing")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towingangryidle_a")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towingpleadingidle_b")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towingpleadingidle_a")
		ADD_STREAMED_ANIM(streamReqsAnim, "move_m@JOG@")
	ELIF bDoTutorial
		ADD_STREAMED_ANIM(streamReqsAnim, "amb@world_human_smoking@male@male_a@base")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towing")
	ENDIF
	
	IF launchArgs.nodeType = NODE_TYPE_ABANDON
//		ADD_STREAMED_ANIM(streamReqsAnim, "GESTURES@MALE")
		ADD_STREAMED_ANIM(streamReqsAnim, "RANDOM@CAR_SLEEPING")
	ELIF launchArgs.nodeType = NODE_TYPE_ACCIDENT
		//We need to stream an ambulance
		SpecialAnimDict = "REACTION@MALE_STAND@BIG_VARIATIONS@A"
		ADD_STREAMED_ANIM(streamReqsAnim, SpecialAnimDict)
		ADD_STREAMED_MODEL(streamReqs, AMBULANCE)
		ADD_STREAMED_MODEL(streamReqs, S_M_M_PARAMEDIC_01)
		SpecialAnims[0] = "react_big_variations_a"
		SpecialAnims[1] = "react_big_variations_b"
		SpecialAnims[2] = "react_big_variations_c"
		ADD_STREAMED_ANIM(streamReqsAnim, "move_m@JOG@")
	ENDIF
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, TRUE)
	
	IF launchArgs.nodeType = NODE_TYPE_TRAIN
		REQUEST_PTFX_ASSET()
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towing")
		ADD_STREAMED_ANIM(streamReqsAnim, "amb@world_human_smoking@male@male_a@base")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towingpleadingidle_b")
//		ADD_STREAMED_ANIM(streamReqsAnim, "GESTURES@MALE")
		ADD_STREAMED_MODEL(streamReqs, FREIGHTCONT1)
		ADD_STREAMED_MODEL(streamReqs, TANKERCAR)
		ADD_STREAMED_MODEL(streamReqs, FREIGHT)
		ADD_STREAMED_MODEL(streamReqs, FREIGHTCAR)
		ADD_STREAMED_MODEL(streamReqs, S_M_M_LSMETRO_01)
		ADD_STREAMED_MODEL(streamReqs, A_F_M_BEVHILLS_01)
		ADD_STREAMED_MODEL(streamReqs, A_M_Y_GENSTREET_02)
	ENDIF
	
	IF launchArgs.nodeType = NODE_TYPE_ABANDON
		POPULATE_SHIT_CARS()
	ELIF launchArgs.nodeType = NODE_TYPE_ACCIDENT
		carNames[idx] = illegalNodes[illegalIdx].mnSpecificCar
	ELSE
		POPULATE_CARS()
	ENDIF
	
	REPEAT MAX_NUM_CAR_NAMES idx
		IF carNames[idx] = DUMMY_MODEL_FOR_SCRIPT
			carNames[idx] = GET_UNUSED_RANDOM_MODEL(carNames, gbCarList, MAX_NUM_CAR_CHOICES)
			REQUEST_VEHICLE_ASSET(carNames[idx])
		ENDIF
		
		IF bSexVariation
			carNames[idx] = LANDSTALKER
			ADD_STREAMED_MODEL(streamReqs, carNames[idx])
			REQUEST_VEHICLE_ASSET(carNames[idx])
			
		ELIF carNames[idx] <> DUMMY_MODEL_FOR_SCRIPT
			ADD_STREAMED_MODEL(streamReqs, carNames[idx])
			REQUEST_VEHICLE_ASSET(carNames[idx])
		ELSE
			SCRIPT_ASSERT("Invalid car model...")
		ENDIF
	ENDREPEAT
	
	//We don't always need a ped!
	IF NOT bDoTutorial AND bStreamPeds	
		//Use this instead
		IF bSexVariation
			POPULATE_ACTORS_SEXY_MEN(numPedCohices)
			mnExtraModel = A_M_M_TranVest_01
#IF FEATURE_GEN9_RELEASE
			mnExtraModel = A_M_Y_Hipster_01
#ENDIF
			ADD_STREAMED_MODEL(streamReqs, mnExtraModel)
			PRINTLN("POPULATING ACTORS FOR bSexVariation")
		ELIF bHomelessVariation
			POPULATE_ACTORS_HOMELESS(numPedCohices)
			PRINTLN("POPULATING ACTORS FOR bHomelessVariation")
		ELIF launchArgs.nodeType = NODE_TYPE_DYNAMIC
			POPULATE_ACTORS(numPedCohices, TRUE)
			PRINTLN("POPULATING ACTORS FOR NODE_TYPE_DYNAMIC")
		ELIF (launchArgs.nodeType = NODE_TYPE_HANDI AND NOT bSexVariation)
			POPULATE_ACTORS(numPedCohices, TRUE)
			PRINTLN("POPULATING ACTORS FOR ATTACK - MALE ONLY")
		ELSE
			POPULATE_ACTORS(numPedCohices)
			PRINTLN("POPULATING ACTORS FOR DEFAULT")
		ENDIF
		
		REPEAT MAX_NUM_PED_NAMES idx
			pedNames[idx] = GET_UNUSED_RANDOM_MODEL(pedNames, gbPedList, numPedCohices)
			IF pedNames[idx] <> DUMMY_MODEL_FOR_SCRIPT
				ADD_STREAMED_MODEL(streamReqs, pedNames[idx])
			ELSE
				SCRIPT_ASSERT("Invalid ped model...")
			ENDIF
		ENDREPEAT
	ENDIF
	
	
	
	ADD_STREAMED_MODEL(streamReqs, missionCarName)
	
	LOAD_QUEUE_LARGE_ADD_ADDITIONAL_TEXT(sLoadQueue, "TOW", ODDJOB_TEXT_SLOT) // load text for the mission
	LOAD_QUEUE_LARGE_ADD_ADDITIONAL_TEXT(sLoadQueue, "DTRSHRD", MINIGAME_TEXT_SLOT) // load text for the mission
	
	//6
	REQUEST_ALL_MODELS(streamReqs)
	REQUEST_ALL_ANIMS(streamReqsAnim, sLoadQueue)
	
	WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(endMessage.siMovie)
		DEBUG_MESSAGE("Waiting on scaleform movie to load")
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		WAIT(0)
	ENDWHILE
	
	WHILE NOT ARE_MODELS_STREAMED(streamReqs)
	OR NOT HAS_VEHICLE_ASSET_LOADED(carNames[0])
		DEBUG_MESSAGE("Waiting for models to stream")
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		WAIT(0)
	ENDWHILE
	
	WHILE NOT ARE_ANIMS_STREAMED(sLoadQueue)
		DEBUG_MESSAGE("waiting for anims")
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		WAIT(0)
	ENDWHILE
	
	IF launchArgs.nodeType = NODE_TYPE_TRAIN
		WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\TOWING_TRAIN")
		OR NOT HAS_PTFX_ASSET_LOADED()
			DEBUG_MESSAGE("waiting for audio / effects")
			UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
			WAIT(0)
		ENDWHILE
	ENDIF

	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
		DEBUG_MESSAGE("trying to load string table")
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		WAIT(0)
	ENDWHILE

ENDPROC


FUNC BOOL UPDATE_DIST_FAIL_CONDITIONS()

	BOOL bGarage = FALSE
	BOOL bCar = FALSE
	STRING sLine

	IF chosenNodeType =  NODE_TYPE_DYNAMIC
		//only breakdown currently goes to a garage
		bGarage = TRUE
	ENDIF
	
	IF curStage < MISSION_STATE_DROPOFF
	   //still going to car
	   bCar = TRUE
	ENDIF
	
	IF bCar
		sLine = "TOW_DISP_CAR"
	ELSE
		IF bGarage
			sLine = "TOW_DISP_GAR"
		ELSE
			sLine = "TOW_DISP_IMP"
		ENDIF
	ENDIF
	
	//Abandon check
	IF NOT IS_VECTOR_ZERO(vCurDestObjective[0])
	
		IF NOT bDistWarned
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(vCurDestObjective[0], FALSE) > (fObjDist + WARN_DISTANCE)	
				CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", sLine, CONV_PRIORITY_VERY_HIGH)
				bDistWarned = TRUE
			ENDIF
		ENDIF

		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vCurDestObjective[0], FALSE) > (fObjDist + FAIL_DISTANCE)
			PRINTLN("Pringing abandon job because distance to cur objective is bigger than fObjDist + Fail")
			failReason = FAIL_ABANDONED_JOB
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_TIME_FAIL_CONDITIONS()
	//DEBUG_MESSAGE("UPDATE_TIME_FAIL_CONDITIONS")
	IF NOT bTimeWarned
		IF TIMER_DO_WHEN_READY(failTimer, WARN_TIME_TOTAL)
			bTimeWarned = TRUE
			CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_DISP3", CONV_PRIORITY_VERY_HIGH)
			//Do dialouge here to warn the player
		ENDIF
	ENDIF
	
	//PRINTLN("Fail timer",GET_TIMER_IN_SECONDS(failTimer))
	
	IF TIMER_DO_WHEN_READY(failTimer, FAIL_TIME_TOTAL)
		IF chosenNodeType <> NODE_TYPE_ABANDON
			failReason = FAIL_TIME_EXPIRED_TOTAL
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_FAIL_CONDITIONS(TOWING_LAUNCH_DATA launchArgs)
	VECTOR vPlayerPosition, vTowTruckPosition
	
	IF bDriverGotCarBack
		PRINTLN("FAIL - FAIL_DRIVE_GOT_CAR_BACK")
		failReason = FAIL_DRIVER_GOT_CAR_BACK
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_ANY_SHOP()
		PRINTLN("PLAYER HAS ABANDONED TOWING JOB BY GOING INTO ANY SHOP")
		failReason = FAIL_ABANDONED_JOB
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
		IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
			IF IS_VEHICLE_STUCK_TIMER_UP(brokenVehicles[0].vehicleIndex, VEH_STUCK_ON_ROOF, ROOF_TIME)
				DEBUG_MESSAGE("FAIL_DAMAGE - UPSIDEDOWN")
				failReason = FAIL_DAMAGE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bOutOfVehicle
		//if the player has been out of the vehicle for more than 5 seconds fail him
		IF (GET_GAME_TIMER() - outOfVehicleTime) > FAIL_TIME
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			ENDIF
			IF DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
				vTowTruckPosition = GET_ENTITY_COORDS(myVehicle)
			ENDIF
		
			IF VDIST2(vPlayerPosition, vTowTruckPosition) > 25*25
				DEBUG_MESSAGE("FAIL_ABANDONED_CAR")
				failReason = FAIL_ABANDONED_CAR
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bAggroedTonya
		failReason = FAIL_ATTACKED_TONYA
		PRINTLN("failReason = FAIL_ATTACKED_TONYA")
		RETURN TRUE
	ENDIF
	IF bAbandonedTonya
		failReason = FAIL_ABANDONED_TONYA
		PRINTLN("failReason = FAIL_ABANDONED_TONYA")
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(myVehicle)
		IF NOT IS_VEHICLE_DRIVEABLE(myVehicle) //AND bDrugsLoaded
			DEBUG_MESSAGE("myVehicle isn't driveable")
			failReason = FAIL_TRANSPORT_DESTROYED
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("myVehicle DOESN'T EXIST")
	ENDIF
	
	IF DOES_ENTITY_EXIST(myVehicle)
		IF IS_ENTITY_DEAD(myVehicle) //AND bDrugsLoaded//IS_VEHICLE_DRIVEABLE(myVehicle)
			DEBUG_MESSAGE("myVehicle is dead")
			failReason = FAIL_TRANSPORT_DESTROYED
			RETURN TRUE
		ENDIF
	ENDIF
	
	SWITCH launchArgs.launchMode
		CASE TOWING_MODE_TRADITIONAL
			IF curStage > MISSION_STATE_CREATE AND curStage < MISSION_STATE_COMPLETE
				IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
					IF IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						DEBUG_MESSAGE("brokenVehicles[0].vehicleIndex is dead")
						failReason = FAIL_TOW_DESTROYED
						RETURN TRUE
					ELSE
						IF UPDATE_DIST_FAIL_CONDITIONS()
							DEBUG_MESSAGE("UPDATE_DIST_FAIL_CONDITIONS")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF UPDATE_TIME_FAIL_CONDITIONS()
				RETURN TRUE
			ENDIF
			
			SWITCH chosenNodeType
				CASE NODE_TYPE_DYNAMIC
				CASE NODE_TYPE_TRAIN
				CASE NODE_TYPE_HANDI
				//CASE NODE_TYPE_ABANDON
					IF bPedCreated
						IF NOT bAttack
//							PRINTLN("bAttack = FALSE")
							IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
								IF IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
									failReason = FAIL_PED_KILLED
									RETURN TRUE
								ENDIF
							ENDIF
						ELSE
							PRINTLN("bAttack = TRUE")
						ENDIF
						
						IF bSexVariation
							IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndexPass)
								IF IS_ENTITY_DEAD(brokenVehicles[0].pedIndexPass)
									PRINTLN("failReason = FAIL_PED_KILLED_PASSENGER")
									failReason = FAIL_PED_KILLED_PASSENGER
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		DEBUG_MESSAGE("wanted")
		failReason = FAIL_WANTED
		RETURN TRUE
	ENDIF
	
	IF NOT DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(myVehicle)
		ADD_VEHICLE_UPSIDEDOWN_CHECK(myVehicle)
		PRINTLN("ADD_VEHICLE_UPSIDEDOWN_CHECK for myVehicle")
	ELIF IS_VEHICLE_STUCK_ON_ROOF(myVehicle)
		failReason = FAIL_VEHICLE_DISABLED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MISSION_SKIP()
	PRINTLN("INSIDE - MISSION_SKIP")

	IF curStage < MISSION_STATE_ENDUI_SETUP
		curStage = MISSION_STATE_ENDUI_SETUP
		PRINTLN("S-SKIP: GOING TO STATE MISSION_STATE_ENDUI_SETUP")
	ELSE
		PRINTLN("curStage > MISSION_STATE_ENDUI_SETUP")
	ENDIF
ENDPROC

PROC MISSION_PASSED()
	
	CLEAR_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_TowingFailed)

	SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_60_DELAY)
	ODDJOB_AUTO_SAVE()
	MISSION_CLEANUP()
ENDPROC

PROC HANLDE_PLACING_PLAYER_AND_TONYA_INTO_TOWTRUCK_AND_MOVING()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
		PRINTLN("SETTING THE PLAYER INTO THE TRUCK")
	ENDIF
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
	AND NOT IS_ENTITY_DEAD(myVehicle)
		SET_PED_INTO_VEHICLE(pedTonya, myVehicle, VS_FRONT_RIGHT)
		PRINTLN("SETTING TONYA INTO THE TRUCK")
	ENDIF
	
	SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<408.8376, -1638.9524, 28.2928>>)
	SET_ENTITY_HEADING(myVehicle, 230.2387)
ENDPROC

// debug builds only
#IF IS_DEBUG_BUILD

	// debug skips
	PROC DO_DEBUG_SKIPS()
		VECTOR vTemp
		// mission pass S
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			PRINTLN("HIT - S KEY")
			
			IF NOT bSkipTutorial
				bSkipTutorial = TRUE
				PRINTLN("bSkipTutorial = TRUE")
			ENDIF
			
			MISSION_SKIP()
		ENDIF
		
		// mission failed F
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			failReason = FAIL_TRANSPORT_DESTROYED
			MISSION_FAILED()
		ENDIF
		
		// skip stage J
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		
			IF bDebugInTutorial
			OR bRunningTwoOrFive
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
						KILL_ANY_CONVERSATION()
						CLEAR_PRINTS()
					
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						PRINTLN("CLEARING PLAYER'S TASKS")
						
						IF bDoTutorial
						OR bRunningTwoOrFive
							IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
								IF IS_PED_IN_ANY_VEHICLE(pedTonya)
									CLEAR_PED_TASKS_IMMEDIATELY(pedTonya)
									PRINTLN("CLEARING TONYA'S TASKS")
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT DOES_ENTITY_EXIST(myVehicle)
							myVehicle = CREATE_VEHICLE(missionCarName, <<408.8376, -1638.9524, 28.2928>>, 230.2387)
							PRINTLN("CREATING TUTORIAL TRUCK")

							HANLDE_PLACING_PLAYER_AND_TONYA_INTO_TOWTRUCK_AND_MOVING()
							PRINTLN("CREATED TRUCK: WE'RE IN A VEHICLE - MOVE PLAYER AND TONYA")
						ELSE
							HANLDE_PLACING_PLAYER_AND_TONYA_INTO_TOWTRUCK_AND_MOVING()
							PRINTLN("WE'RE IN A VEHICLE - MOVE PLAYER AND TONYA")
						ENDIF
					ELSE
						HANLDE_PLACING_PLAYER_AND_TONYA_INTO_TOWTRUCK_AND_MOVING()
						PRINTLN("WE'RE NOT IN A VEHICLE - MOVE PLAYER AND TONYA")
					ENDIF
				ENDIF
			ELSE
				vTemp = vCurDestObjective[0]

				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex) AND NOT bTowHooked
					vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(brokenVehicles[0].vehicleIndex, <<0,-9.0,0>>)
				ENDIF
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					//offset
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vTemp)
				ENDIF
				// skip forward a stage
			ENDIF
		ENDIF
		
		// previous stage P
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			// skip back a stage
		ENDIF
	ENDPROC
#ENDIF

PROC HANDLE_CINEMATIC_CAM()
	//SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
	VECTOR vPlayerPos, vBrokenDownPos
	FLOAT fDeltaZ
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
		vBrokenDownPos = GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex)
	ENDIF
	fDeltaZ = (vPlayerPos.z - vBrokenDownPos.z)
//	PRINTLN("fDeltaZ = ", fDeltaZ)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
			EXIT
		ENDIF
	ENDIF
	IF fDeltaZ > 5
		EXIT
	ENDIF
		
	IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
		IF bPrintHintCam AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
			IF chosenNodeType = NODE_TYPE_TRAIN 
				IF trainState > TRAIN_STATE_PRINT AND trainState < TRAIN_STATE_DONE
					PRINT_HELP("TOW_HELP_CIN")
				
					bPrintHintCam = FALSE
				ENDIF
			ELSE
				IF NOT bTowHooked AND GET_PLAYER_DISTANCE_FROM_ENTITY(brokenVehicles[0].vehicleIndex) < 75
					PRINT_HELP("TOW_HELP_CIN")
					bPrintHintCam = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(towArgs.towCam) AND NOT VALIDATE_HINT_CAM()
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		CLEANUP_CAM(towArgs)
	ENDIF
	
	bDisableCin = TRUE
	IF SHOULD_CONTROL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
		IF NOT DOES_CAM_EXIST(towArgs.towCam)
			SWITCH chosenNodeType
				CASE NODE_TYPE_TRAIN
					IF trainState > TRAIN_STATE_PRINT AND trainState < TRAIN_STATE_DONE
					AND NOT bDisableCam
						SETUP_TRAIN_CAM(towArgs, brokenVehicles[0].pedIndex, brokenVehicles[0].blockingIndex, iToggle, bDisablePedCam)
					ELSE
						//bEnableBonusCam = FALSE
						SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
					ENDIF
				BREAK
				DEFAULT
					IF VALIDATE_HINT_CAM() AND GET_PLAYER_DISTANCE_FROM_ENTITY(brokenVehicles[0].vehicleIndex) < 100
						//speical case
						IF chosenNodeType = NODE_TYPE_HANDI
							IF bSexVariation
								SETUP_SPECIAL_CAM(towArgs, brokenVehicles[0].vehicleIndex, iToggle)
							ENDIF
						ELSE
							SETUP_SPECIAL_CAM(towArgs, brokenVehicles[0].vehicleIndex, iToggle)
						ENDIF
					ELSE
						//bEnableBonusCam = FALSE
						SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		IF DOES_CAM_EXIST(towArgs.towCam)
			CLEANUP_CAM(towArgs)
		ENDIF
	ENDIF
	
ENDPROC


#IF IS_DEBUG_BUILD
	INT idx
	PROC PRINT_DEBUG_INFO()
		REPEAT MAX_NUM_ILLEGAL_NODES idx
			IF illegalNodes[idx].myType = NODE_TYPE_TRAIN
				PRINTLN("Towing Node: ", idx, " Train distance: ", VDIST(illegalNodes[idx].vCarPos, illegalNodes[idx].vPedPos))
			ENDIF
		ENDREPEAT
	ENDPROC
#ENDIF

PROC INIT_CHARACTER_NAME()
	//TODO.. factor in maile or female
	
	SWITCH chosenNodeType
		CASE NODE_TYPE_ABANDON
			IF g_savedGlobals.sTowingData.iAbandonCompleted% 2 = 0
				sCharacterName = "TOWABDBITCHM"//, TOWABDBITCHM
			ELSE
				sCharacterName = "TOWABDATTACKM"//, TOWABDNATTACKF, 
			ENDIF
		BREAK
		CASE NODE_TYPE_ACCIDENT
			//No peds in accident variation
		BREAK
		CASE NODE_TYPE_DYNAMIC
			sCharacterName = "TOWBREAKM"//, TOWBREAKF
		BREAK
		CASE NODE_TYPE_HANDI
			IF bSexVariation
				//This is handeled in the handi setup..
				//TOWSEXMANLYGUY
				//TOWSEXPRISSYGUY
			ELSE
				sCharacterName = "TOWHANDIBEGM" //, TOWHANDIBEGF
			ENDIF
		BREAK
		CASE NODE_TYPE_TRAIN
			sCharacterName = "TOWTRAINM" //, TOWTRAINF
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL HAS_PLAYER_ABANDONED_TONYA()
	VECTOR vPlayerPostion, vTonyaPosition
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPostion = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF

	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		vTonyaPosition = GET_ENTITY_COORDS(pedTonya)
	ENDIF
	
	IF VDIST2(vPlayerPostion, vTonyaPosition) > 50*50
		IF NOT bTonyaAbandonWarning
			PRINT("TOW_TUT_RETONYA", DEFAULT_GOD_TEXT_TIME, 1)
			bTonyaAbandonWarning = TRUE
			PRINTLN("GIVING PLAYER WARNING ON ABANDONING TONYA")
			
			IF NOT DOES_BLIP_EXIST(blipTonya)
				IF NOT IS_ENTITY_DEAD(pedTonya)
					blipTonya = ADD_BLIP_FOR_ENTITY(pedTonya)
					SET_BLIP_COLOUR(blipTonya, BLIP_COLOUR_BLUE)
					SET_BLIP_SCALE(blipTonya, 0.5)
					PRINTLN("ADDING BLIP - blipTonya")
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
				REMOVE_BLIP(brokenVehicles[0].blipIndex)
				PRINTLN("REMOVING BLIP - brokenVehicles[0].blipIndex")
			ENDIF
		ENDIF
	ELSE
		IF bTonyaAbandonWarning
			bTonyaAbandonWarning = FALSE
			PRINTLN("SETTING - bTonyaAbandonWarning = FALSE")
			
			IF DOES_BLIP_EXIST(blipTonya)
				REMOVE_BLIP(blipTonya)
				PRINTLN("REMOVING BLIP - blipTonya")
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
				brokenVehicles[0].blipIndex = ADD_BLIP_FOR_COORD(vCurDestObjective[0])
				IF bTowHooked
					SET_BLIP_COLOUR(brokenVehicles[0].blipIndex, BLIP_COLOUR_YELLOW)
					SET_BLIP_ROUTE(brokenVehicles[0].blipIndex, TRUE)
					SET_BLIP_ROUTE_COLOUR(brokenVehicles[0].blipIndex, BLIP_COLOUR_YELLOW)
				ELSE
					SET_BLIP_COLOUR(brokenVehicles[0].blipIndex, BLIP_COLOUR_BLUE)
					SET_BLIP_ROUTE(brokenVehicles[0].blipIndex, TRUE)
					SET_BLIP_ROUTE_COLOUR(brokenVehicles[0].blipIndex, BLIP_COLOUR_BLUE)
				ENDIF
				PRINTLN("ADDING BLIP - brokenVehicles[0].blipIndex INSIDE ABANDON TONYA CHECK")
			ENDIF
		ENDIF
	ENDIF
	
	IF VDIST2(vPlayerPostion, vTonyaPosition) > 150*150
		IF bTonyaAbandonWarning
			PRINTLN("RETURNING TRUE - HAS_PLAYER_ABANDONED_TONYA")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_REPLAY_VEHICLE(VECTOR vVehiclePos, FLOAT fVehHeading)

	IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		PRINTLN("REPLAY VEHICLE IS AVAILABLE - TOWING")
		bReplayVehicleAvailable = TRUE
	ELSE
		PRINTLN("REPLAY VEHICLE IS NOT AVAILABLE - TOWING")
	ENDIF
			
	IF bReplayVehicleAvailable
		REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
		WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
			PRINTLN("TOWING: Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED")
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF bReplayVehicleAvailable
		//create the replay vehicle but make sure it isnt a water based vehicle (so we dont spawn it on land)
		VEHICLE_INDEX vehPlayer
		vehPlayer = CREATE_REPLAY_CHECKPOINT_VEHICLE(vVehiclePos, fVehHeading)
		IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehPlayer))
		AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
		AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
		ELSE
			DELETE_VEHICLE(vehPlayer)
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_FOR_RIGHT_CHARACTER()
	enumCharacterList ePlayer
	
	ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer != CHAR_FRANKLIN)
		PRINTLN("WE'RE NOT FRANKLIN, CLEANING UP")
		MISSION_CLEANUP()
	ENDIF
	
ENDPROC

// main script
SCRIPT(TOWING_LAUNCH_DATA launchArgs)

	DEBUG_MESSAGE("Towing Start")
	SET_MISSION_FLAG(TRUE)
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		IF NOT IS_REPLAY_BEING_PROCESSED()
			SET_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_TowingFailed)
			Setup_Minigame_Replay(GET_THIS_SCRIPT_NAME())
		ENDIF
	
		DEBUG_MESSAGE("FORCE CLEANUP HAS OCCURRED!!!")
		MISSION_FAILED()
	ENDIF
	
	IF DOES_ENTITY_EXIST(launchArgs.tutorialPed) AND DOES_ENTITY_EXIST(launchArgs.tutorialTruck)
		IF NOT IS_ENTITY_DEAD(launchArgs.tutorialPed) AND NOT IS_ENTITY_DEAD(launchArgs.tutorialTruck)
			SET_ENTITY_AS_MISSION_ENTITY(launchArgs.tutorialPed, TRUE, TRUE)
			SET_ENTITY_AS_MISSION_ENTITY(launchArgs.tutorialTruck, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	ENDIF
	
	POPULATE_ILLEGAL_NODES(illegalNodes)
	POPULATE_EXTRA_DATA(extraData)
	
	POPULATE_LOCATIONS()
	INT selectedNode = 0
	
	#IF IS_DEBUG_BUILD
		myDebugData.iNode = -1
		AddWidgets(myDebugData)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINT_DEBUG_INFO()
		IF launchArgs.bSpecifyNode
			
			IF IS_SCREEN_FADED_OUT()
				CLEAR_AREA(<<408.8376, -1638.9524, 28.2928>>, 8.0, TRUE)
				DO_SCREEN_FADE_IN(500)
			ENDIF
		
			WHILE NOT myDebugData.bNodeSelected
				WAIT(0)
//				PRINTLN("WAITING ON SPECIFIC NODE")
			ENDWHILE
			
			PRINTLN("INSIDE HERE")
			
			//so we've selected a node
			selectedNode = myDebugData.iNode
			launchArgs.nodeType = illegalNodes[myDebugData.iNode].myType
			launchArgs.launchMode = TOWING_MODE_TRADITIONAL
		ENDIF
	#ENDIF
	
	START_TIMER_NOW(totalTimer)
	
	SWITCH launchArgs.launchMode 
		CASE TOWING_MODE_TRADITIONAL
			
			IF launchArgs.bDebug
//			AND NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
			
				DEBUG_MESSAGE("launchArgs.bDebug is TRUE")
				chosenNodeType = launchArgs.nodeType
				
				IF selectedNode > 0
					illegalIdx = selectedNode 
				ELSE
					illegalIdx = FIND_VALID_NODE(100000, chosenNodeType, g_savedGlobals.sTowingData.iLastNodeIndex)
				ENDIF
				
				//illegalIdx = 49
				
				//for now not tutorial unless launched in flow
				//IF g_savedGlobals.sTowingData.iTowingJobsCompleted = 0
					//bDoTutorial = TRUE
				//ENDIF
				
//				IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//					SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_SWITCH)
//					PRINTLN("SWITCHING FROM DEBUG")
//				ENDIF
				
				DEBUG_MESSAGE("This is a debug run of Towing.. selecting specific node type")
				//This is a debug launch.. need to warp the player and do a manual setup
			ELSE
				DEBUG_MESSAGE("launchArgs.bDebug is FALSE")
				IF g_savedGlobals.sTowingData.iTowingJobsCompleted = 0
//					bEnableBonusCam = FALSE
					bDoTutorial = TRUE
				ENDIF
				
				IF NOT IS_REPLAY_IN_PROGRESS()
					SELECTION_LOGIC()
					PRINTLN("NO REPLAY, CHOSEN INDEX = ", illegalIdx)
				ELSE
					illegalIdx = g_savedGlobals.sTowingData.iLastNodeIndex
					PRINTLN("REPLAY, CHOSEN INDEX = ", illegalIdx)
					
					chosenNodeType = g_savedGlobals.sTowingData.lastNodeType
					PRINTLN("REPLAY, CHOSEN NODE TYPE = ", chosenNodeType)
				ENDIF
				
				launchArgs.nodeType = chosenNodeType //TODO.. this is problematic.. two variables for the same thing.. some confusion here
			ENDIF
				
			DEBUG_MESSAGE("TOWING_TRADITIONAL")
			towVariation = VARIATION_TRADIITONAL
		BREAK
	ENDSWITCH
	
	//If it's the first time we actually have the dispatch guy there.  Otherwise he's NULL
	IF NOT bDoTutorial
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "TOWDISPATCH")
		//ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
	ENDIF

	//this should be in seleciton logic bHomelessVariation
	IF chosenNodeType = NODE_TYPE_HANDI
		bStreamPeds = TRUE
		IF g_savedGlobals.sTowingData.iHandiCompleted% 2 = 0
			bSexVariation = TRUE
			PRINTLN("bSexVariation = TRUE")
		ENDIF
	ELIF chosenNodeType = NODE_TYPE_ABANDON
		bHomelessVariation = TRUE
		bStreamPeds = TRUE
		PRINTLN("bHomelessVariation = TRUE")
	ENDIF
	
		//Broken down type requires a ped 
	IF chosenNodeType = NODE_TYPE_DYNAMIC OR chosenNodeType = NODE_TYPE_TRAIN
		bRequiresPed = TRUE
		bStreamPeds = TRUE
		iNumPeds = 1
		PRINTLN("bRequiresPed = TRUE")
	ELIF chosenNodeType = NODE_TYPE_ACCIDENT
		bStreamPeds = TRUE
		iNumPeds = extraData[GET_IDX()].numBystanders
	ENDIF
	
	INIT_CHARACTER_NAME()
	
	INIT_MISSION(launchArgs)
	
	IF g_savedGlobals.sTowingData.iTowingJobsCompleted >= 5
		bPostTonya = TRUE
		PRINTLN("bPostTonya = TRUE")
		bPostTonya = bPostTonya
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			myVehicle = launchArgs.tutorialTruck//GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			PRINTLN("PLAYER IS IN A TOW TRUCK ALREADY - 01")
			
			VEHICLE_INDEX vehTemp
			vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(vehTemp) AND NOT IS_ENTITY_DEAD(vehTemp)
				IF GET_ENTITY_MODEL(vehTemp) = TOWTRUCK
				OR GET_ENTITY_MODEL(vehTemp) = TOWTRUCK2
					myVehicle = vehTemp
					PRINTLN("VEHICLE ASSIGNMENT IN PLACE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_LaunchedViaDebug)
			CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_LaunchedViaDebug)
			
			SWITCH towVariation
				CASE VARIATION_TRADIITONAL
					vStartVec = << 522.2961, -1669.1116, 28.2273 >> //<< -67.7942, -1344.1104, 28.2917 >>
					fStartFloat = 52.2 //171.5033 
				BREAK
			ENDSWITCH
			REQUEST_MODEL(missionCarName)
			
			WHILE NOT HAS_MODEL_LOADED(missionCarName)
				WAIT(0)
			ENDWHILE
			
			PRINTLN("SETTING PLAYER INTO TOWING TRUCK VEHICLE")
			myVehicle = CREATE_VEHICLE(missionCarName, vStartVec, fStartFloat)
			
			IF g_savedGlobals.sTowingData.iAbandonCompleted = 0
				g_savedGlobals.sTowingData.iAbandonCompleted = 1
			ENDIF
			
			IF g_savedGlobals.sTowingData.iHandiCompleted = 0
				g_savedGlobals.sTowingData.iHandiCompleted = 1
			ENDIF
			
			IF g_savedGlobals.sTowingData.iTrainCompleted = 0
				g_savedGlobals.sTowingData.iTrainCompleted = 1
			ENDIF
			
			IF g_savedGlobals.sTowingData.iBreakDownCompleted = 0
				g_savedGlobals.sTowingData.iBreakDownCompleted = 1
			ENDIF
			
			IF g_savedGlobals.sTowingData.iAccidentCompleted = 0
				g_savedGlobals.sTowingData.iAccidentCompleted = 1
			ENDIF
			
			TELEPORT_PLAYER(vStartVec)
		ENDIF
	#ENDIF
	
	SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
	SET_WANTED_LEVEL_MULTIPLIER(0.2)
	
	IF DOES_ENTITY_EXIST(myVehicle)
		IF NOT IS_ENTITY_DEAD(myVehicle)
			SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
			ADD_VEHICLE_UPSIDEDOWN_CHECK(myVehicle)
			SET_VEHICLE_CAN_LEAK_PETROL(myVehicle, FALSE)
			SET_VEHICLE_HAS_STRONG_AXLES(myVehicle, TRUE)
		ENDIF
	ENDIF
	
	sbi01 = ADD_SCENARIO_BLOCKING_AREA((<<414.1398, -640.0020, 27.5001>> - <<50,50,50>>), (<<414.1398, -640.0020, 27.5001>> + <<50,50,50>>))
	sbi02 = ADD_SCENARIO_BLOCKING_AREA((<<-229.8159, -1171.9999, 21.8557>> - <<50,50,50>>), (<<-229.8159, -1171.9999, 21.8557>> + <<50,50,50>>))
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA((vCarGenPosition - <<15,15,15>>), (vCarGenPosition + <<15,15,15>>), FALSE)
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA((vCarGenPosition - <<15,15,15>>), (vCarGenPosition + <<15,15,15>>))
	
	SET_CELLPHONE_PROFILE_TO_NORMAL()
	
    WHILE TRUE
		// main loop	
       	WAIT(0)
		
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		
		CHECK_FOR_RIGHT_CHARACTER()
		
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		
		IF bPostTonya
		AND NOT IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
			IF IS_SCREEN_FADED_OUT()
				NEW_LOAD_SCENE_START(<<408.8376, -1638.9524, 28.2928>>, << -3.2400, 0.0000, -62.6001 >>, 20.0)
				
				WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
					WAIT(0)
					PRINTLN("WAITING ON NEW SCENE TO LOAD")
				ENDWHILE
				
				NEW_LOAD_SCENE_STOP()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				CLEAR_AREA_OF_VEHICLES(<<408.8376, -1638.9524, 28.2928>>, 8.0)
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
		
		//UPDATE_ZVOLUME_WIDGETS()
		
//		IF NOT bPlayerInTowTruck
//			CHECK_FOR_PLAYER_IN_TOW_TRUCK()
//		ENDIF
		
		#IF IS_DEBUG_BUILD
		
		IF myDebugData.bTurnOnDebugPos
			myDebugData.bTurnOnDebugPos = FALSE
			DEBUG_DARW_BLIPS()
		ENDIF
		
		#ENDIF	
		
		IF curStage < MISSION_STATE_ENDUI_SETUP
			UPDATE_MISSION_CAR(myVehicle, myVehicleBlip, brokenVehicles, vCurDestObjective, sLastObjective, outOfVehicleTime, 
			iClosestIdx, bSuppressBlips, bGpsActive, bInCombat, bUnlockDoors, bWanted, curStage)
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			SWITCH towVariation
				CASE VARIATION_TRADIITONAL
				
					IF curStage > MISSION_STATE_CREATE AND curStage < MISSION_STATE_ENDUI_SETUP
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
							//DRAW_MINIGAME_INSTRUCTIONS(helpScaleform)
						ENDIF
						IF chosenNodeType <> NODE_TYPE_ABANDON
						AND chosenNodeType <> NODE_TYPE_ACCIDENT
							IF NOT UPDATE_DAMAGE_TRACKING(printState, brokenVehicles[0].vehicleIndex, myVehicle, iLastHealth, 
							iHealthDelta, bPostTonya)
							
								failReason = FAIL_DAMAGE
								MISSION_FAILED()
								//mission failed on damage
							ENDIF
						ENDIF
						IF bDoTutorial OR bRunningTwoOrFive
							UPDATE_TOWTRUCK_DAMAGE_TRACKING(truckPrintState, myVehicle, iTruckLastHealth, iTruckHealthDelta, bPostTonya)
						ENDIF
					ENDIF
					
					//TODO! Tutorial needs to supress other prints
					IF bDoTutorial //OR curStage = MISSION_STATE_DROPOFF
						IF curStage >=  MISSION_STATE_TOWING
							//IF chosenNodeType = NODE_TYPE_ABANDON OR chosenNodeType = NODE_TYPE_DYNAMIC
								UPDATE_TUTORIAL(myVehicle, brokenVehicles[0].vehicleIndex, myGarageData, brokenVehicles[0].blipIndex, vCurDestObjective, sLastObjective, vDropOffPosition, bDoTutorial, brokenVehicles[0].pedIndex)
							//ENDIF
						ENDIF
					ENDIF
					
					bDisableCin = FALSE
					IF (chosenNodeType = NODE_TYPE_HANDI OR chosenNodeType = NODE_TYPE_ABANDON) AND curStage > MISSION_STATE_CREATE AND curStage < MISSION_STATE_COMPLETE
						IF bSexVariation AND bShakeCar
							#IF NOT IS_JAPANESE_BUILD
							#IF NOT FEATURE_GEN9_RELEASE
								REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
								SHAKE_CAR(brokenVehicles[0].vehicleIndex, brokenVehicles[0].pedIndex)
							#ENDIF
							#ENDIF
						ENDIF
						//no extra ped when we're doing a tutorial
						IF NOT bDoTutorial
							#IF IS_JAPANESE_BUILD
								UPDATE_ANGRY_PED_CLEAN()
							#ENDIF
							#IF NOT IS_JAPANESE_BUILD
								UPDATE_ANGRY_PED()
							#ENDIF
						ENDIF
					ELIF chosenNodeType = NODE_TYPE_TRAIN AND curStage > MISSION_STATE_CREATE AND curStage < MISSION_STATE_COMPLETE
						IF UPDATE_TRAIN()
							MISSION_CLEANUP()
						ENDIF
						
						IF trainState > TRAIN_STATE_PRINT //AND trainState < TRAIN_STATE_DONE
							UPDATE_TRAIN_PED()
						ENDIF
					ELIF chosenNodeType = NODE_TYPE_DYNAMIC
						//we need this special check because if the ped get's out of the vehicle we stop updating the main logic
						//which means we stop updating the ped checks in this case.. look into a better solution for this.
						IF curStage = MISSION_STATE_PED AND brokenState < BROKEN_STATE_PED_TALK AND NOT bTriggeredEarly
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
								IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), brokenVehicles[0].vehicleIndex)
								    bTriggeredEarly = TRUE
									CLEAR_PED_TASKS(brokenVehicles[0].pedIndex)
									TASK_TURN_PED_TO_FACE_ENTITY(brokenVehicles[0].pedIndex, PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bSpawnPed AND NOT bPedCreated
						SPAWN_PED(extraData[GET_IDX()].vUtilityPos, extraData[GET_IDX()].vBloodSplatter, illegalNodes[illegalIdx].vCarPos, brokenVehicles[0].pedIndex, brokenVehicles[0].pedBystanders, pedNames, getPointHead, SpecialAnimDict, SpecialAnims, iNumPeds)
					ELSE

					ENDIF
					
					IF chosenNodeType = NODE_TYPE_ACCIDENT
						UPDATE_STAGING(stagingState, helperCar, helperPed, myVehicle, brokenVehicles[0].pedBystanders, iNumPeds, 
						SpecialAnimDict, SpecialAnims, pedTonya, bPostTonya, brokenVehicles[0].vehicleIndex)
					ENDIF
					
					IF NOT bPostTonya
						IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
							IF DO_AGGRO_CHECK(pedTonya, NULL, aggroArgs, aggroReason, FALSE, TRUE, FALSE, TRUE, FALSE)
								bAggroedTonya = TRUE
								PRINTLN("TONYA ATTACKED OUTSIDE TUTORIAL LOOP")
								
								TASK_SMART_FLEE_PED(pedTonya, PLAYER_PED_ID(), 1000, -1)
							ENDIF
							
							IF HAS_PLAYER_ABANDONED_TONYA()
								bAbandonedTonya = TRUE
								PRINTLN("PLAYER HAS ABANDONED TONYA - POST TUTORIAL")
							ENDIF
						ENDIF
					ENDIF
					
					IF bPedCreated AND NOT bAttack
						IF DO_AGGRO_CHECK(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, aggroArgs, aggroReason, 
						FALSE, TRUE, TRUE, TRUE, FALSE)
							
							PRINTLN("PLAYER AGGROED brokenVehicles[0].pedIndex")
							
							KILL_ANY_CONVERSATION()
							
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								CLEAR_PED_TASKS(brokenVehicles[0].pedIndex)
								SET_PED_KEEP_TASK(brokenVehicles[0].pedIndex, TRUE)
								TASK_SMART_FLEE_PED(brokenVehicles[0].pedIndex, PLAYER_PED_ID(), 1000, -1)
								
								PRINTLN("PLAYER AGGROED DRIVER")
								failReason = FAIL_PED_THREATENED
							ENDIF
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndexPass)
								CLEAR_PED_TASKS(brokenVehicles[0].pedIndexPass)
								SET_PED_KEEP_TASK(brokenVehicles[0].pedIndexPass, TRUE)
								
								CLEAR_SEQUENCE_TASK(taskSequence)
								OPEN_SEQUENCE_TASK(taskSequence)
									TASK_LEAVE_ANY_VEHICLE(NULL, 1, ECF_DONT_CLOSE_DOOR)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
								CLOSE_SEQUENCE_TASK(taskSequence)
								IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndexPass)
									TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndexPass, taskSequence)
								ENDIF
								CLEAR_SEQUENCE_TASK(taskSequence)
								
								PRINTLN("PLAYER AGGROED PASSENGER 01")
								failReason = FAIL_PED_THREATENED
							ENDIF
							
							IF NOT bDoTutorial
								SWITCH aggroReason
									CASE EAggro_Attacked
										IF chosenNodeType = NODE_TYPE_ABANDON
											failReason = FAIL_PED_ATTACKED_TRAMP
											PRINTLN("failReason = FAIL_PED_ATTACKED_TRAMP")
											MISSION_FAILED()
										ELSE
											failReason = FAIL_PED_ATTACKED
											PRINTLN("failReason = FAIL_PED_ATTACKED")
											MISSION_FAILED()
										ENDIF
									BREAK
									CASE EAggro_Wanted
										CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_DISP_LAW", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
										failReason = FAIL_WANTED
										MISSION_FAILED()
									BREAK
//									DEFAULT
//										failReason = FAIL_PED_THREATENED
//									BREAK
								ENDSWITCH 
							ELSE
								failReason = FAIL_ATTACKED_TOW_TRUCK_OWNER
								MISSION_FAILED()
							ENDIF
							
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								MISSION_FAILED()
							ENDIF
						ENDIF
						
						IF DO_AGGRO_CHECK(brokenVehicles[0].pedIndexPass, brokenVehicles[0].vehicleIndex, aggroArgs, aggroReason, FALSE, TRUE, FALSE, TRUE, FALSE)
							KILL_ANY_CONVERSATION()
							
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndexPass)
								CLEAR_PED_TASKS(brokenVehicles[0].pedIndexPass)
								SET_PED_KEEP_TASK(brokenVehicles[0].pedIndexPass, TRUE)
								TASK_SMART_FLEE_PED(brokenVehicles[0].pedIndexPass, PLAYER_PED_ID(), 1000, -1)
								PRINTLN("PLAYER AGGROED PASSENGER 02")
								
								failReason = FAIL_PED_THREATENED
							ENDIF
						ENDIF
					ELSE
						IF bAttack
//							PRINTLN("bAttack")
						ENDIF
					ENDIF
					
//					IF chosenNodeType = NODE_TYPE_DYNAMIC
//						bEnableBonusCam = FALSE
//					ENDIF
//					
//					IF bEnableBonusCam
//						HANDLE_CINEMATIC_CAM()
//					ELIF DOES_CAM_EXIST(towArgs.towCam)
//						CLEANUP_CAM(towArgs)
//					ENDIF
					
					//TODO switch to destination
					IF NOT bDisableCin AND VALIDATE_HINT_CAM(FALSE)
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
//							PRINTLN("DOING A HINT CAM!!!")
							IF NOT bTowHooked
								IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
									IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
										CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex))
									ENDIF
								ENDIF
							ELSE
								CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vDest)
							ENDIF
						ENDIF
					ENDIF
					
					//In this case we don't want to check hookec because when hooked we look at destination
					IF NOT VALIDATE_HINT_CAM(FALSE)
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					ENDIF
				
					UPDATE_PRINT_DELAY(printTimer, 7, TRUE)
					UPDATE_QUICK_CONV_QUEUE()
					//UPDATE_CLOSEST_NODE()
					UPDATE_BLIPPED_ENTITIES()
					
					IF (NOT bOutOfVehicle OR curStage >= MISSION_STATE_ENDUI_SETUP)
					OR bSkipTutorial
					OR bUnlockDoors
						
//						PRINTLN("PASSING THIS CHECK")
					
						IF UPDATE_TRADITIONAL()
							MISSION_PASSED()
						ENDIF
					ENDIF
					
					IF curStage = MISSION_STATE_DROPOFF
						IF chosenNodeType = NODE_TYPE_TRAIN OR chosenNodeType = NODE_TYPE_DYNAMIC
							MANAGE_PED_CONVERSATION(MyLocalPedStruct, brokenVehicles, myVehicle)
						ENDIF
					ENDIF
					
				BREAK
			ENDSWITCH
			
			IF UPDATE_FAIL_CONDITIONS(launchArgs)
				MISSION_FAILED()
			ENDIF
	
		ENDIF
			
		#IF IS_DEBUG_BUILD
			bDebugInTutorial = FALSE
			DO_DEBUG_SKIPS()
		#ENDIF
	ENDWHILE
ENDSCRIPT
