//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     : Towing                                     		            //
//      AUTHOR          : Steven Messinger                                              //
//      DESCRIPTION     : Core logic for the tow truck oddjob       					//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "Towing.sch"
USING "towing_cams.sch"
//USING "z_volumes.sch"
USING "cheat_controller_public.sch"
USING "doors_script_public.sch"

STRING strFailReason
STRING angryPedRoot = ""
//STRING angryPedLine = ""
STRING SpecialAnimDict = "REACTION@MALE_STAND@BIG_VARIATIONS@A"

//TODO these should be linked in a struct with the peds that need them
STRING SpecialAnims[MAX_NUM_BYSTANDERS]
STRING sCharacterName

VECTOR vCurDestObjective[MAX_NUM_NODES]
VECTOR vDest //used by traditional towing mode to set a destinatino
VECTOR vMin, vMax

//current size is 7
STREAMED_MODEL streamReqs[7]
STREAMED_ANIM streamReqsAnim[7]

#IF IS_DEBUG_BUILD
	DEBUG_POS_DATA myDebugData
	VECTOR vStartVec = << 1387.9126, -2535.6133, 47.4669 >>
	FLOAT fStartFloat
#ENDIF

BOOL bFailRc = FALSE
BOOL bTriggeredEarly = FALSE
BOOL bPrintHook = TRUE
BOOL bPrintDest = TRUE
BOOL bPrintHintCam = TRUE
BOOL bTrainClose = FALSE
BOOL bRammedVehicle = FALSE
BOOL bTaskedBrokenDownPed = FALSE
BOOL bUnlockDoors = FALSE
BOOL bStopTimer = FALSE
BOOL bDisplayHelp = TRUE
BOOL bAggroedTonya = FALSE
BOOL bAbandonedTonya = FALSE
BOOL bSkipTutorial = FALSE
BOOL bPostTonya = FALSE
BOOL bDisablePedCam = FALSE
BOOL bDisableCin = FALSE
BOOL bInCombat = FALSE
BOOL bSpawnPed = FALSE
BOOL bTimeWarned = FALSE
BOOL bDistWarned = FALSE
BOOL bReverseOrder = FALSE
BOOL bDisableCam = FALSE
BOOL bSupressObjective = FALSE
BOOL bSuppressBlips = FALSE
BOOL bDoTutorial = FALSE 
BOOL bRequiresPed = FALSE
BOOL bGenDisabled = FALSE
BOOL bAttack = FALSE
BOOL bHomelessVariation = FALSE
BOOL bGpsActive = FALSE
BOOL bTonyaAbandonWarning = FALSE
BOOL bReplayVehicleAvailable = FALSE
BOOL bDriverGotCarBack = FALSE
BOOL bAngryPedGaveUp = FALSE
BOOL bWrongCarAttached = FALSE
BOOL bPlayInitialUpsetLine = FALSE
BOOL bPlayFullConvo = FALSE
BOOL bPrintSpecialCaseObj = FALSE
BOOL bTonyaReattachConvo = FALSE
BOOL bPrintReattach = FALSE
BOOL bPlayedCellPhoneCall = FALSE
BOOL bPlayedWayBackConvo = FALSE
BOOL bFailedToExit = FALSE
BOOL bGaveWarning = FALSE
BOOL bPlayedLastConvo = FALSE
BOOL bInterrupt = FALSE
BOOL bCarCameUnHooked = FALSE
BOOL bPrintObjective = FALSE
BOOL bStartTimer = FALSE
BOOL bPlayedNearEndingConvo = FALSE
BOOL bPlayedBackToImpountConvo = FALSE
BOOL bPlayedPostHookupConvo = FALSE
BOOL bPrintDriveToImpoundObjective = FALSE
BOOL bRunnningWithTonya = FALSE
BOOL bPrintGoToRepairShop = FALSE
BOOL bPlayBeckonLine = FALSE
BOOL bVehicleInDropoffArea = FALSE
BOOL bCurrentlyTonyaAbandoned = FALSE
BOOL bPrintAllowTonyaToEnter = FALSE
BOOL bKillAnyConversation = FALSE
BOOL bPlayWaitConvo = FALSE
BOOL bReturnHelpPrinted = FALSE
BOOL bClearedPrintsOnce = FALSE
BOOL bPrintHurryHelp = FALSE
BOOL bPlayStopConvo = FALSE
BOOL bPlayedGetOutConvo = FALSE
BOOL bPlayAbandonLine = FALSE
BOOL bPlayedDamagedVehicleLine = FALSE
BOOL bPlayHookedUpAudio = FALSE
BOOL bPrintSlowDownHelp = FALSE
BOOL bMovedVehicle = FALSE
BOOL bDrawCorona = FALSE
BOOL bRemovedCoordinateBlip = FALSE
//BOOL bFailForBeingWanted = FALSE

PED_INDEX piSpeakers[2]
PED_INDEX pedTrainDriver
PED_INDEX pedTonya

BLIP_INDEX myVehicleBlip
BLIP_INDEX tempBlip
BLIP_INDEX blipImpound

//TOW_NODE towNodes[MAX_NUM_NODES]

MODEL_NAMES carNames[MAX_NUM_CAR_NAMES]
MODEL_NAMES pedNames[MAX_NUM_PED_NAMES]

VARIATION towVariation = VARIATION_TRADIITONAL

TOW_CAM_ARGS towArgs

REL_GROUP_HASH grpBuddies

BROKEN_VEHICLE brokenVehicles[MAX_NUM_BROKEN_CARS]
GARAGE_DATA myGarageData

INT curTowIndex = -1
INT curBrokenCarCount = 0
INT curCarNameIdx = 0
//INT iStageToUse = 0
INT iToggle = 0
INT fPanicDist = 0
//Car health info
INT iLastHealth = 0
INT iHealthDelta = 0
INT iInitialHealth = 0
//Tow Truck health info
INT iTruckLastHealth = 0
INT iTruckHealthDelta = 0
INT iClosestIdx
INT iSpeedNode01
INT iSpeedNode02
INT iAdditionalHelpPrintStages
INT iPostMissionTonyaStages
INT iNumPeds = 0
INT outOfVehicleTime = 0
INT iTonyaDropoffStages = 0
INT iPostDropOffStages = 0
INT iPedID
INT iAwayFromImpoundStages = 0
INT iPushStages = 0

VECTOR vExtra
VECTOR vExtra2
VECTOR vCarGenPosition = <<408.8376, -1638.9524, 28.2928>>
VECTOR vBDVPos // used to store off broken down vehicle position

VECTOR vSkipSpawnPosition = <<405.5971, -1635.6908, 28.2928>>
FLOAT fSkipSpawnHeading = 233.0139

VECTOR getPointVec
FLOAT getPointHead
FLOAT fConvoBufferTime = 5.0

TEXT_LABEL_63 sLastObjective
TEXT_LABEL_23 TestResumptionLabel

FAIL_REASON_ENUM failReason

structTimer printDelayTimer
structTimer	genericTimer
structTimer	totalTimer
structTimer printTimer
structTimer chaseTimer
structTimer failTimer
structTimer wakeTimer
structTimer tAdditionalHelpPrintTimer
structTimer tDropOffTimer
structTimer tAbandonTimer
structTimer tConvoBufferTimer
structTimer tLastChatTimer
structTimer tDelayPhoneCallTimer
structTimer tEndingTimer
structTimer tNearCarStallTimer
structTimer tStopTimer
//INT iAllowSkipCutsceneTime = 0

VEHICLE_INDEX myVehicle
VEHICLE_INDEX helperCar
PED_INDEX helperPed

SCENARIO_BLOCKING_INDEX sbi01, sbi02, sbi03

//INT iGpsIndex = -1

STRING sMyObjective
//STRING sMyHelp

MISSION_STATE curStage = MISSION_STATE_INTRO
STAGING_STATE stagingState = STAGING_STATE_INIT
BROKEN_STATE brokenState = BROKEN_STATE_VISIBLE
ANGRY_PED pedState = ANGRY_PED_INIT
TRAIN_STATE trainState = TRAIN_STATE_PRINT
TRAIN_STATE_PED trainPedState = TRAIN_STATE_PED_INIT
TOW_PED_DIALOUGE pedDialougeState = TOW_PED_BEG

DAMAGE_PRINTS printState = DAMAGE_PRINTS_FIRST
DAMAGE_PRINTS truckPrintState = DAMAGE_PRINTS_FIRST
GIVEUP_ARGS giveUpArgs

PTFX_ID sparksEffects[8]

// GATE VARIABLES

ENUM TOWING_GATES
	TOWING_GATES_SOUTH = 0,
	TOWING_GATES_EAST
ENDENUM

MISSION_DOOR_STRUCT sMyDoors[2]

INT iGateStates
INT iGateSouth, iGateEast

//REPLAY VARIABLES
//VECTOR vPlayerFirstCheckpointPosition = <<-10.7897, -1471.2037, 29.5520>>
//FLOAT fPlayerFirstCheckpointHeading = 276.1668
//VECTOR vTonyaFirstCheckpointPosition = <<-10.9438, -1473.6119, 29.5508>>
//FLOAT fTonyaFirstCheckpointHeading = 264.1491

PROC HANDLE_GATE_OPENING()
	
	SWITCH iGateStates
		CASE 0
			iGateSouth = HASH("SouthGate")
			iGateEast = HASH("EastGate")
			
			MISSION_DOORS_ADD_DOOR(sMyDoors[TOWING_GATES_SOUTH], iGateSouth, <<416.4166, -1652.7782, 28.2917>>, prop_facgate_08)
			MISSION_DOORS_ADD_DOOR(sMyDoors[TOWING_GATES_EAST], iGateEast, <<415.2836, -1621.8584, 28.3294>>, prop_facgate_08)
			
			CPRINTLN(DEBUG_MISSION, "iGateStates = 1")
			iGateStates = 1
		BREAK
		CASE 1
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), <<415.6646, -1622.2667, 28.2919>>) < 50.0
			
				MISSION_DOORS_SET_DOOR_STATE(sMyDoors[TOWING_GATES_SOUTH], TRUE, 1.0, 2000)
				MISSION_DOORS_SET_DOOR_STATE(sMyDoors[TOWING_GATES_EAST], TRUE, 1.0, 2000)
				
				CPRINTLN(DEBUG_MISSION, "iGateStates = 2")
				iGateStates = 2
			ENDIF
		BREAK
		CASE 2
			MISSION_DOORS_UPDATE(sMyDoors)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC MISSION_CLEANUP(LoadQueueLarge &sLoadQueue, BOOL bTerminateThread = TRUE)
	
	// To Fix Bug # 554724
	CLEAR_HELP()
	
	IF NOT IS_ENTITY_DEAD(myVehicle)
		SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_SIREN(myVehicle, FALSE)
	ENDIF
	
	REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedNode01)
	REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedNode02)
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbi01)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi02)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA((vCarGenPosition - <<15,15,15>>), (vCarGenPosition + <<15,15,15>>), TRUE)
	
	INT idx
	REPEAT MAX_NUM_BROKEN_CARS idx
		//IF brokenVehicles[openIdx].speedIDA
		REMOVE_ROAD_NODE_SPEED_ZONE(brokenVehicles[idx].speedIDA) 
		REMOVE_ROAD_NODE_SPEED_ZONE(brokenVehicles[idx].speedIDB) 
		REMOVE_ROAD_NODE_SPEED_ZONE(brokenVehicles[idx].speedIDC)
	ENDREPEAT
	
	IF NOT IS_VECTOR_ZERO(extraData[GET_IDX()].vMin)
		ALLOW_PEDS_TO_BE_IN_AREA(sblockingIndex, extraData[GET_IDX()].vMin, extraData[GET_IDX()].vMax)
	ENDIF

	SET_VEHICLE_AS_NO_LONGER_NEEDED(brokenVehicles[0].vehicleIndex)
	
	IF NOT IS_ENTITY_DEAD(pedTonya)
		IF DOES_ENTITY_EXIST(pedTonya)
			IF IS_PED_IN_GROUP(pedTonya)
				REMOVE_PED_FROM_GROUP(pedTonya)
				PRINTLN("REMOVING pedTonya FROM GROUP")
			ENDIF
		ENDIF
		
		IF bTerminateThread
			SET_PED_KEEP_TASK(pedTonya, TRUE)
			SET_PED_AS_NO_LONGER_NEEDED(pedTonya)
			PRINTLN("SETTING PED - pedTonya AS NO LONGER NEEDED")
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
		SET_PED_AS_NO_LONGER_NEEDED(brokenVehicles[0].pedIndex)
		PRINTLN("SETTING PED - brokenVehicles[0].pedIndex AS NO LONGER NEEDED")
		
		IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
			IF IS_PED_IN_GROUP(brokenVehicles[0].pedIndex)
				REMOVE_PED_FROM_GROUP(brokenVehicles[0].pedIndex)
				PRINTLN("REMOVING PED FROM GROUP")
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedTrainDriver)
		IF NOT IS_ENTITY_DEAD(pedTrainDriver)
			SET_PED_AS_NO_LONGER_NEEDED(pedTrainDriver)
			PRINTLN("SETTING PED - pedTrainDriver AS NO LONGER NEEDED")
		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(myVehicle)
		SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(myVehicle)
		PRINTLN("SETTING TOW TRUCK AS NO LONGER NEEDED VIA MISSION CLEANUP")
	ENDIF
	
	//Turn back on car spawning
	IF bGenDisabled
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMin, vMax, TRUE)
	ENDIF

	// cleanup anything you need to. You don't need to dereference script-created entities like peds and vehicles,
	// as code will do this automatically when the script terminates.
	IF DOES_ENTITY_EXIST(brokenVehicles[0].blockingIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex)
		SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(brokenVehicles[0].blockingIndex, TRUE)
		SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(brokenVehicles[0].blockingIndex)
	ENDIF
	
	IF chosenNodeType = NODE_TYPE_TRAIN
		SET_RANDOM_TRAINS(TRUE)
		
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_MAIN, TRUE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_QUARRY, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_CAR_STEAL_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_METRO, TRUE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_FIRST_HOOD_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_SECOND_HOOD_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_SECOND_FAME_OR_SHAME_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_DOWNTOWN_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_THIRD_HOOD_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_PROLOGUE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_JEWEL_HEIST_SETPIECE, FALSE)
		SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_PROLOGUE_2, FALSE)
		
		PRINTLN("PUTTING TRAINS BACK TO INITIALIZATION VALUES")
	ENDIF	
	
	ODDJOB_STOP_SOUND(soundID)
	ODDJOB_STOP_SOUND(soundID2)
	STOP_BREAKING_PFX(sparksEffects)
	ODDJOB_STOP_SOUND(soundID3)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	IF DOES_CAM_EXIST(towArgs.towCam) 
		CLEANUP_CAM(towArgs)
	ENDIF
	
	CLEANUP_LOAD_QUEUE_LARGE(sLoadQueue)
	
	Set_Leave_Area_Flag_For_All_Blipped_Missions()
	
	//CLEANUP_MINIGAME_INSTRUCTIONS(helpScaleform)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, FALSE)
	IF bTerminateThread
		PRINTLN("TERMINATING THREAD")
		TERMINATE_THIS_THREAD() // important, kills the script
	ENDIF
ENDPROC

PROC MISSION_FAILED()

	//Don't allow fail if the mission passed UI is up
	IF curStage < MISSION_STATE_ENDUI_WAIT
	
		// anything that should happen when mission fails
		//CLEAR_PRINTS()
		//STOP_SCRIPTED_CONVERSATION(TRUE)
		
		PRINTLN("FAILING MISSION")
		
		// This sets us up for a replay through the launcher.
		//SET_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_MinigameFailed)
	
		// will usually print a line of god text on fail describing the reason
		SWITCH failReason
			CASE FAIL_TRANSPORT_DESTROYED
//				PRINT_NOW("TOW_FAIL_05", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_05"
			BREAK
			CASE FAIL_ABANDONED_CAR
//				PRINT_NOW("TOW_FAIL_10", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_10"
			BREAK
			CASE FAIL_TIME_EXPIRED
//				PRINT_NOW("DTRSHRD_FAIL_03", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "DTRSHRD_FAIL_03"
			BREAK
			CASE FAIL_DISTANCE_FAIL
//				PRINT_NOW("TOW_FAIL_08", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_08"
			BREAK
			CASE FAIL_FRANKLIN_NO_EXIT
//				PRINT_NOW("TOW_FAIL_08", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_17"
			BREAK
			CASE FAIL_ABANDONED_JOB
//				PRINT_NOW("TOW_FAIL_08", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_08"
			BREAK
			CASE FAIL_ABANDONED_DRIVER
//				PRINT_NOW("TOW_FAIL_08a", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_08a"
			BREAK
			CASE FAIL_DRIVER_GOT_CAR_BACK
//				PRINT_NOW("TOW_FAIL_16", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_16"
			BREAK
			CASE FAIL_VEHICLE_DISABLED
//				PRINT_NOW("TOW_FAIL_12", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_12"
			BREAK
			CASE FAIL_TOW_DESTROYED
//				PRINT_NOW("TOW_FAIL_01", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_01"
			BREAK
//			CASE FAIL_COMMUTE_TIME
//				PRINT_WITH_NUMBER("", FLOOR(MAX_COMMUTE), DEFAULT_GOD_TEXT_TIME, 1)
//			BREAK
			CASE FAIL_WANTED
//				PRINT_NOW("TOW_FAIL_03", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_03a"
			BREAK
			CASE FAIL_PED_KILLED
//				PRINT_NOW("TOW_FAIL_04", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_04"
			BREAK
			CASE FAIL_PED_KILLED_PASSENGER
//				PRINT_NOW("TOW_FAIL_04a", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_04a"
			BREAK
			CASE FAIL_TIME_EXPIRED_TOTAL
//				PRINT_NOW("TOW_FAIL_06", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_06"
			BREAK
			CASE FAIL_DAMAGE
//				PRINT_NOW("TOW_FAIL_07", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_07"
			BREAK 
			CASE FAIL_PED_ATTACKED
//				PRINT_NOW("TOW_FAIL_09", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_09"
			BREAK
			CASE FAIL_PED_ATTACKED_TRAMP
//				PRINT_NOW("TOW_FAIL_09a", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_09a"
			BREAK
			CASE FAIL_PED_THREATENED
//				PRINT_NOW("TOW_FAIL_02", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_02"
			BREAK
			CASE FAIL_PED_THREATENED_WITH_WEAPON
//				PRINT_NOW("TOW_FAIL_02b", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_02b"
			BREAK
			CASE FAIL_PED_THREATENED_WITH_WEAPON_AIMING
//				PRINT_NOW("TOW_FAIL_02b", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_02b"
			BREAK
			CASE FAIL_ATTACKED_TOW_TRUCK_OWNER
//				PRINT_NOW("TOW_FAIL_11", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_11"
			BREAK
			CASE FAIL_ATTACKED_TONYA
//				PRINT_NOW("TOW_FAIL_13", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_13"
			BREAK
			CASE FAIL_ABANDONED_TONYA
//				PRINT_NOW("TOW_FAIL_14", DEFAULT_GOD_TEXT_TIME, 1)
				strFailReason = "TOW_FAIL_14"
			BREAK
		ENDSWITCH
		
		IF IS_ENTITY_DEAD(PLAYER_PED_ID())
			PRINTLN("PLAYER IS DEAD")
			strFailReason = "TOW_FAIL_15"
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(strFailReason)
			MISSION_FLOW_SET_FAIL_REASON(strFailReason)
		ENDIF
	
		bFailRc = TRUE

//		SET_FORCE_CLEANUP_FAIL_REASON()
//		
//		PRINTLN("Setting us up for a replay!")
//		Setup_Minigame_Replay(GET_THIS_SCRIPT_NAME())
//		
//		// Wait for replay controller to fade the screen out
//		WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
//			WAIT(0)
//		ENDWHILE
//		
//		MISSION_CLEANUP()
	ENDIF
ENDPROC

PROC SET_AGGRO_FIELDS_TO_NOT_CHECK()
//	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Aiming)
//	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_ShotNear)
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_HeardShot)
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Attacked)
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Name:  CREATE_CAR
//Purpose: Creates a car at INT openIdx in the vehicle array brokenVehicles
/////////////////////////////////////////////////////////////////////////////////////////////////////////
FUNC BOOL CREATE_CAR(VECTOR spawnVec, FLOAT fOrient, INT& openIdx, BOOL bDoDamaged = TRUE, BOOL bBlip = TRUE)	
	DEBUG_MESSAGE("CREATE_CAR")
	openIdx = FIND_FIRST_OPEN(brokenVehicles)
	
	FLOAT fClosestCheck, fMiddleCheck, fOuterCheck
	
	IF openIdx = -1
		SCRIPT_ASSERT("There is no open spot in the broken Vehicle array")
		RETURN FALSE
	ENDIF

	IF curCarNameIdx >= MAX_NUM_PED_NAMES
		curCarNameIdx = 0
	ENDIF
	
	PRINTLN("Trying to create car enum:", carNames[curCarNameIdx])
	
	IF chosenNodeType = NODE_TYPE_ABANDON
		carNames[0] = PRIMO
		PRINTLN("carNames[0] = PRIMO")
	ELIF chosenNodeType = NODE_TYPE_HANDI
		carNames[0] = landstalker
		PRINTLN("carNames[0] = landstalker")
	ELIF chosenNodeType = NODE_TYPE_TRAIN
		carNames[0] = schwarzer
		PRINTLN("carNames[0] = schwarzer")
	ELIF chosenNodeType = NODE_TYPE_DYNAMIC
		carNames[0] = tailgater
		PRINTLN("carNames[0] = tailgater")
	ELIF chosenNodeType = NODE_TYPE_ACCIDENT
		carNames[0] = INTRUDER
		PRINTLN("carNames[0] = INTRUDER")
	ENDIF

	brokenVehicles[openIdx].vehicleIndex = CREATE_VEHICLE(carNames[0], spawnVec, fOrient)
	SET_ENTITY_LOAD_COLLISION_FLAG(brokenVehicles[openIdx].vehicleIndex, TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(carNames[0])
//	SET_VEHICLE_COLOURS(brokenVehicles[openIdx].vehicleIndex, 0, 0)
	PRINTLN("SETTING COLLISION FLAG ON CAR")
	
	IF chosenNodeType = NODE_TYPE_DYNAMIC
		SET_VEHICLE_COLOURS(brokenVehicles[openIdx].vehicleIndex, 27, 27)
		PRINTLN("SETTING VEHICLE'S COLORS")
	ENDIF
	
	curCarNameIdx++
	
	fObjDist = GET_PLAYER_DISTANCE_FROM_LOCATION(spawnVec, FALSE)
	
	IF chosenNodeType = NODE_TYPE_TRAIN
		fClosestCheck = 20
		fMiddleCheck = 40
		fOuterCheck = 60
	ELSE
		fClosestCheck = 4
		fMiddleCheck = 20
		fOuterCheck = 40
	ENDIF
	
	PRINTLN("fClosestCheck = ", fClosestCheck)
	PRINTLN("fMiddleCheck = ", fMiddleCheck) 
	PRINTLN("fOuterCheck = ", fOuterCheck)
	
	IF NOT IS_VECTOR_ZERO(illegalNodes[illegalIdx].vSpeedNodeLocation)
		brokenVehicles[openIdx].speedIDA = ADD_ROAD_NODE_SPEED_ZONE(illegalNodes[illegalIdx].vSpeedNodeLocation, fClosestCheck, 0)
	ELSE
		brokenVehicles[openIdx].speedIDA = ADD_ROAD_NODE_SPEED_ZONE(spawnVec, fClosestCheck, 0)
		PRINTLN("CREATING SPEED ZONE 1")
	ENDIF
	
	brokenVehicles[openIdx].speedIDB = ADD_ROAD_NODE_SPEED_ZONE(spawnVec, fMiddleCheck, 2.5)
	PRINTLN("CREATING SPEED ZONE 2")
	brokenVehicles[openIdx].speedIDC = ADD_ROAD_NODE_SPEED_ZONE(spawnVec, fOuterCheck, 4)
	PRINTLN("CREATING SPEED ZONE 3")
		
	//SET_ROADS_IN_AREA(vMin, vMax, FALSE)
	//SCRIPT_ASSERT("Turning off roads in area")
	//ADD_BLIP_FOR_COORD(vMin)
	//ADD_BLIP_FOR_COORD(vMax)
	
	IF bDoDamaged
		IF brokenVehicles[openIdx].bDoFire
			//-100 is a hood fire
			SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(brokenVehicles[openIdx].vehicleIndex, TRUE)
			SET_VEHICLE_ENGINE_HEALTH(brokenVehicles[openIdx].vehicleIndex, 1)
			SET_VEHICLE_ENGINE_CAN_DEGRADE(brokenVehicles[openIdx].vehicleIndex, FALSE)
			PRINTLN("bDoFire IS TRUE")
		ELSE
			SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(brokenVehicles[openIdx].vehicleIndex, TRUE)
			SET_VEHICLE_ENGINE_HEALTH(brokenVehicles[openIdx].vehicleIndex, 1)
			SET_VEHICLE_ENGINE_CAN_DEGRADE(brokenVehicles[openIdx].vehicleIndex, FALSE)
			PRINTLN("bDoFire IS FALSE")
			//front
			//SET_VEHICLE_DAMAGE(brokenVehicles[openIdx].vehicleIndex, <<0.0, 1.2, 0.0>>, 1600.0, 1600.0, TRUE)
		ENDIF
		
		//windshield
		//SET_VEHICLE_DAMAGE(brokenVehicles[openIdx].vehicleIndex, <<0.0, 0.75, 0.05>>, 1600.0, 1600.0, TRUE)
		//left window
		//SET_VEHICLE_DAMAGE(brokenVehicles[openIdx].vehicleIndex, <<-0.7, 0.0, 0.0>>, 1600.0, 1600.0, TRUE)
		//right window
		//SET_VEHICLE_DAMAGE(brokenVehicles[openIdx].vehicleIndex, <<0.7, 0.0, 0.0>>, 1600.0, 1600.0, TRUE)
		//front
		//SET_VEHICLE_DAMAGE(brokenVehicles[openIdx].vehicleIndex, <<0.0, 1.2, 0.0>>, 1600.0, 1600.0, TRUE)
						
		//We don't want to add a blip if we're patrol and currently towing
	ENDIF
	
	IF NOT bSuppressBlips AND bBlip
		IF DOES_ENTITY_EXIST(brokenVehicles[openIdx].vehicleIndex)
			brokenVehicles[openIdx].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[openIdx].vehicleIndex)
			//SET_BLIP_SCALE(brokenVehicles[openIdx].blipIndex, 0.75)
			SET_BLIP_COLOUR(brokenVehicles[openIdx].blipIndex, BLIP_COLOUR_BLUE)
			PRINTLN("ADDING BLIP - brokenVehicles[openIdx].blipIndex")
		ENDIF
	ELSE
		IF bSuppressBlips
			PRINTLN("bSuppressBlips = TRUE")
		ELSE
			PRINTLN("bSuppressBlips = FALSE")
		ENDIF
		IF bBlip
			PRINTLN("bBlip = TRUE")
		ELSE
			PRINTLN("bBlip = FALSE")
		ENDIF
	ENDIF
	curBrokenCarCount++	
	//totalBrokenCarCount++
	
	START_TIMER_NOW(brokenVehicles[openIdx].carTimer)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL EXTRA_HOOK_CHECK()
	IF bDoTutorial 
		IF bHookPlayed //AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC CHECK_FOR_HOOKING_WRONG_CAR()

	IF bRunnningWithTonya
		IF NOT bVehicleInDropoffArea
		
			IF DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
			
				ENTITY_INDEX vehHookedCar
				vehHookedCar = GET_ENTITY_ATTACHED_TO_TOW_TRUCK(myVehicle)
			
				IF DOES_ENTITY_EXIST(vehHookedCar) AND NOT IS_ENTITY_DEAD(vehHookedCar)
					IF IS_ENTITY_A_VEHICLE(vehHookedCar)
						IF GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(vehHookedCar) <> brokenVehicles[0].vehicleIndex
						
							IF NOT bWrongCarAttached
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, pedTonya, "TONYA")
								
								PRINT_HELP("TOW_TUT_07")
								
								IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_WRONG", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("bWrongCarAttached = TRUE")
									bWrongCarAttached = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					bWrongCarAttached = FALSE
				ENDIF
			ENDIF
		ELSE
//			PRINTLN("bVehicleInDropoffArea IS TRUE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CHECK_TOW_ATTACHMENTS(STRING objective, VECTOR vDestination)
	BOOL bProcede = TRUE
	INT idx
	
	CHECK_FOR_HOOKING_WRONG_CAR()
	
	REPEAT MAX_NUM_BROKEN_CARS idx
		IF DOES_ENTITY_EXIST(brokenVehicles[idx].vehicleIndex)
			IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[idx].vehicleIndex) 
				IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[idx].vehicleIndex) AND EXTRA_HOOK_CHECK()
					
					IF NOT bClearedPrintsOnce
						CLEAR_PRINTS()
						bClearedPrintsOnce = TRUE
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_LOWEST)
					
//					SET_VEHICLE_LIGHTS(myVehicle, FORCE_VEHICLE_LIGHTS_ON)
					SET_VEHICLE_SIREN(myVehicle, TRUE)
					
					CLEAR_TEXT_ON_HOOKING()
					
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					bTowHooked = TRUE
					IF bRequiresPed AND NOT IS_PED_IN_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle)
						bProcede = FALSE
//						PRINTLN("bProcede = FALSE")
						//We are hooked skip ahead and get the ped in the car
						IF trainPedState < TRAIN_STATE_PED_PENDING
							SETTIMERB(0)
							DEBUG_MESSAGE("Setting timer b to 0")
							trainPedState = TRAIN_STATE_PED_ENTER_TRUCK
						ENDIF
					ENDIF
					IF bProcede
						PRINTLN("bProcede = TRUE")
					
						//don't print bjective if we jsut hooked
						CLEAR_PRINT_DELAY(printTimer)
						IF DOES_BLIP_EXIST(brokenVehicles[idx].blipIndex)
							SET_BLIP_ROUTE(brokenVehicles[idx].blipIndex, FALSE)
							PRINTLN("CLEARING BLIP ROUTE - 01")
						ENDIF
						bGpsActive = FALSE
						SCRIPT_REMOVE_BLIP_SAFE(brokenVehicles[idx].blipIndex)
						
						brokenVehicles[idx].pickupTime = GET_TIMER_IN_SECONDS(brokenVehicles[idx].carTimer)
						
						//TODO it's preferable to not have any specific checks in here...
						IF towVariation = VARIATION_TRADIITONAL	
							brokenVehicles[idx].blipIndex = ADD_BLIP_FOR_COORD(vDestination)
							fObjDist = GET_PLAYER_DISTANCE_FROM_LOCATION(vDestination, FALSE)
							PRINTLN("ATTEMPTING TO ADD BLIP")
							vCurDestObjective[0] = vDestination
						ENDIF
						
						IF chosenNodeType = NODE_TYPE_ACCIDENT
							SET_VEHICLE_DOOR_SHUT(brokenVehicles[0].vehicleIndex, SC_DOOR_FRONT_LEFT, FALSE)
							PRINTLN("CLOSING DOOR ON ACCIDENT VEHICLE")
						ENDIF
						
						RESTART_TIMER_NOW(failTimer)
						
						IF DOES_BLIP_EXIST(brokenVehicles[idx].blipIndex)
							SET_BLIP_COLOUR(brokenVehicles[idx].blipIndex, BLIP_COLOUR_YELLOW)
							SET_BLIP_ROUTE(brokenVehicles[idx].blipIndex, TRUE)
						ENDIF
						
						IF NOT bDoTutorial
						AND NOT (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya5")) = 1)
							IF bPrintDest
								PRINT_NOW(objective, DEFAULT_GOD_TEXT_TIME, 1)
								PRINTLN("PRINTING OBJECTIVE IN CHECK TOW ATTACHMENTS")
								bPrintDest = FALSE
							ENDIF
						ENDIF
						
						sLastObjective = objective
						CLEAR_AREA_OF_VEHICLES(vCurDestObjective[0], 6.0)
					
						curTowIndex = idx
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC HANDLE_ADDITIONAL_HELP_PRINTS_AT_DROPOFF()

	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDest, <<30,30,LOCATE_SIZE_HEIGHT>>, FALSE)
		IF NOT IS_TIMER_STARTED(tDropOffTimer)
			START_TIMER_NOW(tDropOffTimer)
			PRINTLN("STARTING TIMER - tDropOffTimer")
		ELSE
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
				IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
					IF GET_TIMER_IN_SECONDS(tDropOffTimer) > 30
						PRINT_HELP("TOWT_HELP_DP")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_IN_DROP_OFF_AREA(VECTOR& vAngledA, VECTOR& vAngledB, FLOAT& fWidth)
	IF curTowIndex <> -1
		//If this location has a process zone require the player to be in it
		IF NOT IS_VECTOR_ZERO(myGarageData.vProcessZone)
			vDest = myGarageData.vProcessZone
			//PRINTLN("Useing vDropZoneA because processizng zone was non o:", myGarageData.vDropZoneA)
			vAngledA = myGarageData.vDropZoneA
			vAngledB = myGarageData.vDropZoneB
			fWidth = myGarageData.fDropZoneW
		ELSE
			//PRINTLN("Useing vCoordinate01 because processizng zone was o:", myGarageData.vCoordinate01)
			vAngledA = myGarageData.vCoordinate01
			vAngledB = myGarageData.vCoordinate02
			fWidth = myGarageData.fWidth
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
			IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
				bDrawCorona = TRUE
//				PRINTLN("bDrawCorona = TRUE")
			ELSE
				bDrawCorona = FALSE
//				PRINTLN("bDrawCorona = FALSE")
			ENDIF
		ENDIF
		
		IF NOT bDoTutorial
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDest, <<30,30,LOCATE_SIZE_HEIGHT>>, bDrawCorona)
				// Do nothing... need this for the chevron
//				PRINTLN("DOING CHECK FOR VEHICLE INSIDE DROP OFF")
				IF chosenNodeType = NODE_TYPE_HANDI OR chosenNodeType = NODE_TYPE_ACCIDENT
					IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
//						SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_UNLOCKED)
//						PRINTLN("UNLOCKING DOORS ON BROKEN DOWN CAR")
//						bUnlockDoors = TRUE
					ENDIF
				ENDIF
			ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDest, <<3,3,LOCATE_SIZE_HEIGHT>>, FALSE)
				IF NOT IS_ENTITY_DEAD(myVehicle)
					BRING_VEHICLE_TO_HALT(myVehicle, 1.0, 0)
				ENDIF
//				bUnlockDoors = FALSE
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(brokenVehicles[curTowIndex].vehicleIndex)
			IF IS_ENTITY_IN_ANGLED_AREA(brokenVehicles[curTowIndex].vehicleIndex, vAngledA, vAngledB, fWidth, FALSE, FALSE)
//			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()  - To Fix Bug # 971799, no need to wait until the conversation is done
				
				IF bDisplayHelp
					PRINT_HELP("TOWT_HELP_UH")
//					PRINTLN("PRINTING HELP 1")
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[curTowIndex].vehicleIndex)
					IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[curTowIndex].vehicleIndex) //OR NOT bDoTutorial 
					OR (IS_PED_IN_VEHICLE(PLAYER_PED_ID(), brokenVehicles[curTowIndex].vehicleIndex) AND NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[curTowIndex].vehicleIndex))
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_ANY_CONVERSATION()
						ENDIF
						
						CLEAR_HELP()
						
						bVehicleInDropoffArea = TRUE
//						PRINTLN("bVehicleInDropoffArea = TRUE")
						
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				HANDLE_ADDITIONAL_HELP_PRINTS_AT_DROPOFF()
			ENDIF
		ENDIF
	ENDIF
	
	bVehicleInDropoffArea = FALSE
//	PRINTLN("bVehicleInDropoffArea = FALSE")
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_NEAR_CAR_STALL_DIALOGUE()
	VECTOR vPlayerPos, vCarPosition
	
	IF bPlayWaitConvo
		EXIT
	ENDIF
	
	IF bDoTutorial
		EXIT
		PRINTLN("DOING THE TUTORIAL, EXIT EARLY")
	ENDIF
	
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya1")) = 1)
	OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
		IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<402.5694, -1632.3905, 28.2928>>, <<50,50,50>>, FALSE)
			EXIT
			PRINTLN("TOO FAR TO CARE ABOUT STALLING")
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
		vCarPosition = GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex)
	ENDIF
	
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya5")) = 1)
		IF NOT bPlayWaitConvo
			IF VDIST2(vPlayerPos, <<-227.7806, -1170.6628, 21.8945>>) < 400 // 20m
				IF GET_TIMER_IN_SECONDS(tNearCarStallTimer) > 90.0
					IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_WAITING3", CONV_PRIORITY_VERY_HIGH)
						PRINTLN("bPlayWaitConvo = TRUE")
						bPlayWaitConvo = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF VDIST2(vPlayerPos, vCarPosition) < 400 // 20m
		IF NOT IS_TIMER_STARTED(tNearCarStallTimer)
			START_TIMER_NOW(tNearCarStallTimer)
		ELSE
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya1")) = 1)
				IF GET_TIMER_IN_SECONDS(tNearCarStallTimer) > 25.0
					IF NOT bPlayWaitConvo
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_WAIT", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("bPlayWaitConvo = TRUE")
							bPlayWaitConvo = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
				IF GET_TIMER_IN_SECONDS(tNearCarStallTimer) > 20.0
					IF NOT bPlayWaitConvo
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_WAIT3", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("bPlayWaitConvo = TRUE")
							bPlayWaitConvo = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL CHECK_TOWING(STRING sObjective)
	//INT idx
	
	VECTOR vAngledA
	VECTOR vAngledB
	FLOAT fWidth
//	INT iRand

	IF curTowIndex = -1
		SCRIPT_ASSERT("curTowIndex = -1")
	ENDIF

	IF brokenVehicles[curTowIndex].bDoFire
		IF (GET_TIMER_IN_SECONDS(brokenVehicles[curTowIndex].carTimer) - brokenVehicles[curTowIndex].pickupTime) > 120.0
			EXPLODE_VEHICLE(brokenVehicles[curTowIndex].vehicleIndex)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_IN_DROP_OFF_AREA(vAngledA, vAngledB, fWidth)
		IF NOT IS_ENTITY_DEAD(brokenVehicles[curTowIndex].pedIndex) AND IS_PED_IN_VEHICLE(brokenVehicles[curTowIndex].pedIndex, myVehicle)
		OR (NOT IS_ENTITY_DEAD(pedTonya) AND IS_PED_IN_VEHICLE(pedTonya, myVehicle))
			BRING_VEHICLE_TO_HALT(MyVehicle, 6, 2)
						
			bDropComplete = TRUE
			
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
				IF DOES_ENTITY_EXIST(brokenVehicles[curTowIndex].pedIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[curTowIndex].pedIndex)
					// Peds for Broken Down and Train Track Clearing
					OPEN_SEQUENCE_TASK(taskSequence)
						TASK_CLEAR_LOOK_AT(NULL)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<538.5295, -174.2699, 53.4930>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 64.6725)
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
						PRINTLN("USING - WORLD_HUMAN_STAND_MOBILE")
					CLOSE_SEQUENCE_TASK(taskSequence)
					TASK_PERFORM_SEQUENCE(brokenVehicles[curTowIndex].pedIndex, taskSequence)
					CLEAR_SEQUENCE_TASK(taskSequence)
					
					SET_PED_KEEP_TASK(brokenVehicles[curTowIndex].pedIndex, TRUE)
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(brokenVehicles[curTowIndex].pedIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[curTowIndex].pedIndex)
					// Peds for Broken Down and Train Track Clearing
					OPEN_SEQUENCE_TASK(taskSequence)
						TASK_CLEAR_LOOK_AT(NULL)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-215.5652, -1385.4003, 30.2583>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 234.3862)
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
						PRINTLN("USING - WORLD_HUMAN_STAND_MOBILE")
						
	//					iRand = GET_RANDOM_INT_IN_RANGE() % 3
	//					IF iRand = 0
	//						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
	//						PRINTLN("USING - WORLD_HUMAN_STAND_MOBILE")
	//					ELIF iRand = 1
	//						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
	//						PRINTLN("USING - WORLD_HUMAN_STAND_MOBILE")
	//					ELIF iRand = 2
	//						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
	//						PRINTLN("USING - WORLD_HUMAN_STAND_MOBILE")
	//					ELSE
	//						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
	//						PRINTLN("USING - WORLD_HUMAN_STAND_MOBILE")
	//					ENDIF
					CLOSE_SEQUENCE_TASK(taskSequence)
					TASK_PERFORM_SEQUENCE(brokenVehicles[curTowIndex].pedIndex, taskSequence)
					CLEAR_SEQUENCE_TASK(taskSequence)
					
					SET_PED_KEEP_TASK(brokenVehicles[curTowIndex].pedIndex, TRUE)
				ENDIF
			ENDIF
		
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINTLN("KILLING CONVERSATION, SO WE CAN HEAR THANK YOU LINE.")
				KILL_ANY_CONVERSATION()
			ENDIF
		
//			IF g_savedGlobals.sTowingData.iTowingJobsCompleted = 0
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya1")) = 1)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<398.966614,-1641.232666,27.292774>>, <<411.138153,-1626.688721,33.292782>>, 15.000000)
					IF NOT bPlayedLastConvo
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TUT_ENDa", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
							PRINTLN("PLAYING FIRST TONYA GOODBYE")
							bPlayedLastConvo = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<398.966614,-1641.232666,27.292774>>, <<411.138153,-1626.688721,33.292782>>, 15.000000)
					IF NOT bPlayedLastConvo
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TUT_ENDb", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
							PRINTLN("PLAYING SECOND TONYA GOODBYE")
							bPlayedLastConvo = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF chosenNodeType = NODE_TYPE_DYNAMIC
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_BREAK2", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES
					PRINTLN("PLAYING BROKEN DOWN, THANK YOU LINE.")
				ELIF chosenNodeType = NODE_TYPE_TRAIN
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TRAIN_TK", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES
					PRINTLN("PLAYING TRAIN TRACK CLEARING, THANK YOU LINE.")
				ENDIF
			ENDIF

		ELSE
			//BRING_VEHICLE_TO_HALT(PLAYER_PED_ID(), MyVehicle, 6, 1)
		ENDIF
		
//		PRINT_NOW("TOW_OBJ_04", DEFAULT_GOD_TEXT_TIME, 1)
		
		IF NOT bDoTutorial
			//DETACH_VEHICLE_FROM_TOW_TRUCK(myVehicle, brokenVehicles[curTowIndex].vehicleIndex)
		ENDIF		
		
		bTowHooked = FALSE
		REMOVE_BLIP(brokenVehicles[curTowIndex].blipIndex)
		//brokenVehicles[curTowIndex].vehicleIndex = NULL
		RESTART_TIMER_NOW(genericTimer)
		curBrokenCarCount--
		RETURN TRUE
	ELSE
		HANDLE_NEAR_CAR_STALL_DIALOGUE()
	ENDIF
	
//	IF chosenNodeType = NODE_TYPE_TRAIN OR chosenNodeType = NODE_TYPE_DYNAMIC
//		MANAGE_PED_CONVERSATION(MyLocalPedStruct, brokenVehicles, myVehicle)
//	ENDIF
	
	//Check for tow breaking
	IF NOT bUnlockDoors
		IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[curTowIndex].vehicleIndex)
			IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[curTowIndex].vehicleIndex)
				bTowHooked = FALSE
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				bCarCameUnHooked = TRUE
				
				IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya1")) = 1)
					PRINTLN("INFORMING STAT STYSTEM - UNHOOK, TONYA 1")
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TON1_UNHOOK)
				ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
					PRINTLN("INFORMING STAT STYSTEM - UNHOOK, TONYA 2")
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TON2_UNHOOK)
				ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya3")) = 1)
					PRINTLN("INFORMING STAT STYSTEM - UNHOOK, TONYA 3")
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TON3_UNHOOK)
				ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
					PRINTLN("INFORMING STAT STYSTEM - UNHOOK, TONYA 4")
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TON4_UNHOOK)
				ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya5")) = 1)
					PRINTLN("INFORMING STAT STYSTEM - UNHOOK, TONYA 5")
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TON5_UNHOOK)
				ENDIF
				
				IF DOES_BLIP_EXIST(brokenVehicles[curTowIndex].blipIndex)
					SET_BLIP_ROUTE(brokenVehicles[curTowIndex].blipIndex, FALSE)
					REMOVE_BLIP(brokenVehicles[curTowIndex].blipIndex)
					PRINTLN("CLEARING BLIP ROUTE - 02")
				ENDIF
				
				IF DOES_ENTITY_EXIST(brokenVehicles[curTowIndex].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[curTowIndex].vehicleIndex)
					IF NOT DOES_BLIP_EXIST(brokenVehicles[curTowIndex].blipIndex)
						brokenVehicles[curTowIndex].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[curTowIndex].vehicleIndex)
						SET_BLIP_COLOUR(brokenVehicles[curTowIndex].blipIndex, BLIP_COLOUR_BLUE)
					ENDIF
				ENDIF
				
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TestResumptionLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
					PRINTLN("KILLING ANY CONVERSATION THAT MIGHT BE ONGOING")
					KILL_FACE_TO_FACE_CONVERSATION()
					
					bInterrupt = TRUE
					PRINTLN("bInterrupt = TRUE")
				ENDIF
				
				IF bRunnningWithTonya
					IF NOT bTonyaReattachConvo
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TONYA_DETACH", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("bTonyaReattachConvo = TRUE")
							bTonyaReattachConvo = TRUE
						ENDIF
					ENDIF
				ELSE
					IF chosenNodeType = NODE_TYPE_TRAIN
						IF NOT bTonyaReattachConvo
							IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_LOSTCAR", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("bTonyaReattachConvo = TRUE 01")
								bTonyaReattachConvo = TRUE
							ENDIF
						ENDIF
					ELIF chosenNodeType = NODE_TYPE_DYNAMIC
						IF NOT bTonyaReattachConvo
							IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_CAR_UNHO", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("bTonyaReattachConvo = TRUE 02")
								bTonyaReattachConvo = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				RESTART_TIMER_NOW(genericTimer)
				IF NOT bSupressObjective	
				
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF bPrintHook
							CLEAR_PRINTS()
							PRINT_NOW(sObjective, DEFAULT_GOD_TEXT_TIME, 1)
							bPrintHook = FALSE
							PRINTLN("PRINTING OBJECTIVE TO REATTACH - 01")
						ELSE
							CLEAR_PRINTS()
							PRINT_NOW(sObjective, DEFAULT_GOD_TEXT_TIME, 1)
							PRINTLN("PRINTING OBJECTIVE TO REATTACH - 02")
						ENDIF
						
						sLastObjective = sObjective
					ENDIF
				ELSE
					PRINTLN("bSupressObjective IS TRUE")
					sLastObjective = sObjective
				ENDIF
				//bFirstTime = FALSE
				
				PRINTLN("GOING TO STATE - MISSION_STATE_TOWING")
				curStage = MISSION_STATE_TOWING
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_ALL_BLIPS(INT exceptMe)
	bSuppressBlips = TRUE
	INT idx
	REPEAT MAX_NUM_BROKEN_CARS idx
		IF idx <> exceptMe
			IF DOES_BLIP_EXIST(brokenVehicles[idx].blipIndex)
				REMOVE_BLIP(brokenVehicles[idx].blipIndex)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC ADD_ALL_BLIPS()
	bSuppressBlips = FALSE
	INT idx
	REPEAT MAX_NUM_BROKEN_CARS idx
		IF DOES_ENTITY_EXIST(brokenVehicles[idx].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[idx].vehicleIndex)
			brokenVehicles[idx].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[idx].vehicleIndex)
			SET_BLIP_COLOUR(brokenVehicles[idx].blipIndex, BLIP_COLOUR_BLUE)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_PLAYER_BUMPED_ENTITY(ENTITY_INDEX myEntity)
	VEHICLE_INDEX vehicle
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ENDIF
	
	IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), myEntity) OR IS_ENTITY_TOUCHING_ENTITY(vehicle, myEntity)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_MOVING_WITH_BROKEN_VEHICLE()
	FLOAT fTempSpeed
	
	IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
		IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
			fTempSpeed = GET_ENTITY_SPEED(myVehicle)
//			PRINTLN("TOW TRUCK SPEED = ", fTempSpeed)
			
			IF fTempSpeed > 1.0
				IF NOT bPrintSlowDownHelp
					PRINT_HELP("TOWT_HELP_SL")
					
					PRINTLN("bPrintSlowDownHelp = TRUE")
					bPrintSlowDownHelp = TRUE
				ENDIF
			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_BROKEN_VEHICLE_PED(BROKEN_STATE& myState, INT idx, STRING audioLine) //VEHICLE_INDEX vehicle, PED_INDEX myPed)
	VECTOR vPlayerPostion, vPedPostion
//	FLOAT fTempDistance
	
	SWITCH myState
		CASE BROKEN_STATE_VISIBLE
			DEBUG_MESSAGE("BROKEN_STATE_VISIBLE")
			PRINTLN("UPDATE_BROKEN_VEHICLE_PED: idx = ", idx)
			
			IF NOT bSuppressBlips
				IF SHOULD_TRIGGER_ENTITY(brokenVehicles[idx].vehicleIndex, myVehicle, myFrameCount, 10, 150, 2, TRUE) OR bReverseOrder OR bTowHooked
					IF NOT bTaskedBrokenDownPed
						myFrameCount = 0
						ODDJOB_STOP_SOUND(soundID3)
						DEBUG_MESSAGE("Script Conversation")
						DEBUG_MESSAGE("Clear ped tasks")
						
						IF NOT IS_ENTITY_DEAD(brokenVehicles[idx].pedIndex)
							IF GET_SCRIPT_TASK_STATUS(brokenVehicles[idx].pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
								TASK_TURN_PED_TO_FACE_ENTITY(brokenVehicles[idx].pedIndex, PLAYER_PED_ID(), -1)
								PRINTLN("TASKING PED TO FACE ENTITY")
							ENDIF
						ENDIF
			
						bTaskedBrokenDownPed = TRUE
					ENDIF
					
					IF bTaskedBrokenDownPed
						IF IS_ENTITY_AT_COORD(myVehicle, GET_ENTITY_COORDS(brokenVehicles[idx].pedIndex), <<25,25,25>>)
							
							IF IS_ENTITY_PLAYING_ANIM(brokenVehicles[idx].pedIndex, "oddjobs@towingcome_here", "come_here_idle_a")
								STOP_ANIM_TASK(brokenVehicles[idx].pedIndex, "oddjobs@towingcome_here", "come_here_idle_a", SLOW_BLEND_OUT)
							ENDIF
							
							IF NOT IS_PLAYER_MOVING_WITH_BROKEN_VEHICLE()
								IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya3")) = 1)
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, brokenVehicles[idx].pedIndex, "TOWTRAINM")
									CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_TRAIN1", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
									PRINTLN("PLAYING LINE - SHIT MYSELF LINE")
									
									SET_PED_MOVEMENT_CLIPSET(brokenVehicles[idx].pedIndex, "move_m@JOG@")
									
									CLEAR_HELP()
									
									IF NOT IS_ENTITY_DEAD(brokenVehicles[idx].pedIndex)
										SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_UNLOCKED)
										TASK_ENTER_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_BLOCK_SEAT_SHUFFLING | ECF_RESUME_IF_INTERRUPTED)
									ENDIF
								ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, brokenVehicles[idx].pedIndex, "TOWBREAKDOWN1")
									CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", audioLine, CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
									
//									SET_PED_MOVEMENT_CLIPSET(brokenVehicles[idx].pedIndex, "move_m@JOG@")
									
									CLEAR_HELP()
									
									IF NOT IS_ENTITY_DEAD(brokenVehicles[idx].pedIndex)
										SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_UNLOCKED)
										TASK_ENTER_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK, ECF_BLOCK_SEAT_SHUFFLING | ECF_RESUME_IF_INTERRUPTED)
									ENDIF
								ELSE
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iPedID, brokenVehicles[idx].pedIndex, sCharacterName)
									CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", audioLine, CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
									
									SET_PED_MOVEMENT_CLIPSET(brokenVehicles[idx].pedIndex, "move_m@JOG@")
									
									CLEAR_HELP()
									
									IF NOT IS_ENTITY_DEAD(brokenVehicles[idx].pedIndex)
										SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_UNLOCKED)
										TASK_ENTER_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_BLOCK_SEAT_SHUFFLING | ECF_RESUME_IF_INTERRUPTED)
									ENDIF
								ENDIF
								
								IF bReverseOrder
									IF DOES_BLIP_EXIST(brokenVehicles[idx].blipIndex)
										REMOVE_BLIP(brokenVehicles[idx].blipIndex)
										PRINTLN("REMOVING BLIP - brokenVehicles[idx].blipIndex")
									ENDIF
									IF NOT DOES_BLIP_EXIST(brokenVehicles[idx].blipIndex)
										IF DOES_ENTITY_EXIST(brokenVehicles[idx].pedIndex)
											IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndexPed)
												REMOVE_BLIP(brokenVehicles[0].blipIndexPed)
												PRINTLN("REMOVING BLIP - brokenVehicles[0].blipIndexPed") // To fix assert
											ENDIF
											brokenVehicles[idx].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[idx].pedIndex)
											SET_BLIP_COLOUR(brokenVehicles[idx].blipIndex, BLIP_COLOUR_BLUE)
											SET_BLIP_SCALE(brokenVehicles[idx].blipIndex, BLIP_SIZE_PED)
											PRINTLN("ADDING BLIP - brokenVehicles[idx].blipIndex")
										ENDIF
									ENDIF
								ENDIF
								//in train we ned to do this only
								//remove blip from vehicle and blip the ped
								SETTIMERA(0)
								myState = BROKEN_STATE_PED_TALK
							ENDIF
						ELIF IS_ENTITY_AT_COORD(myVehicle, GET_ENTITY_COORDS(brokenVehicles[idx].pedIndex), <<50,50,50>>)
							IF NOT bPlayBeckonLine
								IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, brokenVehicles[idx].pedIndex, "TOWBREAKDOWN1")
									
									IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_CALLOUT", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
										bPlayBeckonLine = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				DEBUG_MESSAGE("bSuppressBlips is true")
			ENDIF
		BREAK
		CASE BROKEN_STATE_PED_TALK
			DEBUG_MESSAGE("BROKEN_STATE_PED_TALK")
			
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND TIMERA() > 3000
				bStopTimer = TRUE

				RESTART_TIMER_NOW(failTimer)
				RESTART_TIMER_NOW(printDelayTimer)
				SCRIPT_REMOVE_BLIP_SAFE(brokenVehicles[idx].blipIndex)
				IF NOT DOES_BLIP_EXIST(brokenVehicles[0].blipIndexPed)
					IF DOES_ENTITY_EXIST(brokenVehicles[idx].pedIndex)
						brokenVehicles[0].blipIndexPed = ADD_BLIP_FOR_ENTITY(brokenVehicles[idx].pedIndex)
						SET_BLIP_COLOUR(brokenVehicles[0].blipIndexPed, BLIP_COLOUR_BLUE)
						SET_BLIP_SCALE(brokenVehicles[0].blipIndexPed, BLIP_SIZE_PED)
					ENDIF
				ENDIF
				PRINT_NOW("TOWT_OBJ_02", DEFAULT_GOD_TEXT_TIME, 1)
				DEBUG_MESSAGE("Tasking player to enter tow truck")
				myState = BROKEN_STATE_PED_ENTER
//			ENDIF
		BREAK
		CASE BROKEN_STATE_PED_ENTER
			DEBUG_MESSAGE("BROKEN_STATE_PED_ENTER")
			//brokenVehicles[idx].bPedTasked = TRUE
			//CLEAR_PED_TASKS(brokenVehicles[idx].pedIndex)
			ADD_RELATIONSHIP_GROUP("TOWBUDDIES", grpBuddies)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, grpBuddies, RELGROUPHASH_PLAYER)
//			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), grpBuddies)
			SET_PED_RELATIONSHIP_GROUP_HASH(brokenVehicles[idx].pedIndex, grpBuddies)

//			bEnableBonusCam = FALSE
			
			REMOVE_ALL_BLIPS(idx)
//			IF bReverseOrder
//				myState = BROKEN_STATE_HOOKING
//			ELSE
				myState = BROKEN_STATE_DIALOUGE
//			ENDIF
		BREAK
		CASE BROKEN_STATE_DIALOUGE
			//DEBUG_MESSAGE("BROKEN_STATE_DIALOUGE")
			IF NOT IS_PED_IN_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle) 
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					vPlayerPostion = GET_ENTITY_COORDS(PLAYER_PED_ID())
				ENDIF
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
					vPedPostion = GET_ENTITY_COORDS(brokenVehicles[0].pedIndex)
				ENDIF
				
				IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
					IF GET_SCRIPT_TASK_STATUS(brokenVehicles[0].pedIndex, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
						TASK_ENTER_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_BLOCK_SEAT_SHUFFLING | ECF_RESUME_IF_INTERRUPTED)
						PRINTLN("RETASKING TO ENTER TOW TRUCK")
					ENDIF
				ENDIF
				
//				fTempDistance = VDIST(vPlayerPostion, vPedPostion)
//				PRINTLN("fTempDistance = ", fTempDistance)
			
				IF NOT bReturnHelpPrinted
					IF VDIST2(vPlayerPostion, vPedPostion) > 2500 // 50m
						PRINT_NOW("TOW_OBJ_REOWNER", DEFAULT_GOD_TEXT_TIME, 1)
						bReturnHelpPrinted = TRUE
						PRINTLN("bReturnHelpPrinted = TRUE")
					ENDIF
				ENDIF
			
				IF UPDATE_GIVE_UP(giveUpArgs, brokenVehicles[idx].pedIndex)
					PRINTLN("Pringing abandon job because UPDATE_GIVE_UP returned true")
					failReason = FAIL_ABANDONED_JOB
					MISSION_FAILED()
				ENDIF
			ENDIF
			
//			DEBUG_MESSAGE("BROKEN_STATE_DIALOUGE")
			IF IS_PED_IN_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle)
				IF NOT IS_ENTITY_DEAD(myVehicle)
					SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_LOCKED_NO_PASSENGERS)
					SCRIPT_REMOVE_BLIP_SAFE(brokenVehicles[0].blipIndexPed)

					PRINTLN("bPedInTowTruck = TRUE")
				ENDIF
			ENDIF
			
			IF IS_PED_IN_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle) AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF bReverseOrder	
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TRAIN2", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
				ELSE
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_BREAKS", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
				ENDIF
				
				PRINTLN("GOING TO STATE - BROKEN_STATE_HOOKING")
				myState = BROKEN_STATE_HOOKING
			ENDIF
		BREAK
		CASE BROKEN_STATE_HOOKING
			DEBUG_MESSAGE("BROKEN_STATE_HOOKING")
			
			IF IS_PED_IN_VEHICLE(brokenVehicles[idx].pedIndex, myVehicle) //AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				//At this point we need to de-blip the rest of the cars
				REMOVE_ALL_BLIPS(idx)
				
				IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					IF bReverseOrder AND IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
						PRINTLN("bReverseOrder AND bTowHooked returning true.")
						RETURN TRUE
					ENDIF
				ENDIF

				IF NOT DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
					IF DOES_ENTITY_EXIST(brokenVehicles[idx].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[idx].vehicleIndex)
						brokenVehicles[0].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[idx].vehicleIndex)
						SET_BLIP_COLOUR(brokenVehicles[idx].blipIndex, BLIP_COLOUR_BLUE)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
						PRINT_NOW("TOWT_OBJ_03", DEFAULT_GOD_TEXT_TIME, 1)
						sLastObjective = "TOWT_OBJ_03"				
					ENDIF
				ENDIF
				
				RESTART_TIMER_NOW(printDelayTimer)
				myState = BROKEN_STATE_VISIBLE
				RETURN TRUE
				PRINTLN("**TRUE!!")
			ENDIF
		BREAK

	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_BROKEN_VEHICLE_ILLEGAL(BOOL bWithPed = FALSE, BOOL bBlip = FALSE)
	PRINTLN("INSIDE - CREATE_BROKEN_VEHICLE_ILLEGAL")

	INT num = 0
	IF NOT CREATE_CAR(illegalNodes[illegalIdx].vCarPos, illegalNodes[illegalIdx].fCarRot, num, FALSE, bBlip)
		RETURN FALSE
	ENDIF
	fObjDist = GET_PLAYER_DISTANCE_FROM_LOCATION(illegalNodes[illegalIdx].vCarPos, FALSE)
	vCurDestObjective[0] = illegalNodes[illegalIdx].vCarPos
	IF illegalNodes[illegalIdx].myType = NODE_TYPE_ABANDON	
		//front
		SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<0.0, 1.2, 0.0>>, 1600.0, 1600.0, TRUE)
		//windshield
		SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<0.0, 0.75, 0.05>>, 1600.0, 1600.0, TRUE)
		//left window
		SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<-0.7, 0.0, 0.0>>, 1600.0, 1600.0, TRUE)
		//right window
		SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<0.7, 0.0, 0.0>>, 1600.0, 1600.0, TRUE)
	ELIF illegalNodes[illegalIdx].myType = NODE_TYPE_ACCIDENT	
		//DOORS
		//SET_VEHICLE_DOORS_BROKEN 
//		SCRIPT_ASSERT("here")
		getPointHead = -1.0
		bSpawnPed = TRUE //we want peds to be spawned

		//Need to create the ambulance as well.
		helperCar = CREATE_VEHICLE(AMBULANCE, extraData[GET_IDX()].vTrafficPos, extraData[GET_IDX()].fTrafficOrient)
		SET_ENTITY_AS_MISSION_ENTITY(helperCar)
		IF NOT IS_ENTITY_DEAD(helperCar)
			SET_VEHICLE_SIREN(helperCar, TRUE)
			SET_VEHICLE_ENGINE_ON(helperCar, TRUE, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(AMBULANCE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(helperCar)
			helperPed = CREATE_PED_INSIDE_VEHICLE(helperCar, PEDTYPE_CIVMALE, S_M_M_PARAMEDIC_01)
			SET_ENTITY_AS_MISSION_ENTITY(helperPed)
			PRINTLN("CREATING GUY IN AMBULANCE")
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PARAMEDIC_01)
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(extraData[GET_IDX()].vMin)
			sblockingIndex = PREVENT_PEDS_FROM_BEING_IN_AREA(extraData[GET_IDX()].vMin, extraData[GET_IDX()].vMax)
		ENDIF
		
		SWITCH illegalNodes[illegalIdx].myDamage
			CASE DAMAGE_FRONT_LEFT
				//Front Left
				SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<-0.5223, 2.4550, 0.0784>>, 200, 250, TRUE)
				SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<-1.0,1.5,-0.5>>, 200, 250, TRUE)
			BREAK
			CASE DAMAGE_FRONT_RIGHT
				//Front Right
				SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<0.6012, 2.4222, 0.0245>>, 200, 250, TRUE)
				SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<1.0,1.5,-0.5>>, 200, 250, TRUE)
			BREAK
			CASE DAMAGE_HEAD_ON
				SET_VEHICLE_DAMAGE(brokenVehicles[0].vehicleIndex, <<0.1414, 1.8401, 0.0342>>, 200, 350, TRUE)
			BREAK
		ENDSWITCH
		
		SET_VEHICLE_DOOR_OPEN(brokenVehicles[0].vehicleIndex, SC_DOOR_FRONT_LEFT)
		
	ENDIF
	
	IF bWithPed
		bPedCreated = TRUE
		PRINTLN("bPedCreated = TRUE - 01")
		
		IF chosenNodeType = NODE_TYPE_TRAIN
			brokenVehicles[0].pedIndex = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].vehicleIndex, PEDTYPE_MISSION, A_M_M_BEVHILLS_02, VS_DRIVER)
			SET_ENTITY_LOAD_COLLISION_FLAG(brokenVehicles[0].pedIndex, TRUE)
			PRINTLN("OVERRIDING MODEL - A_M_M_BEVHILLS_02")
			
			SET_PED_COMPONENT_VARIATION(brokenVehicles[0].pedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(brokenVehicles[0].pedIndex, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(brokenVehicles[0].pedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BEVHILLS_02)
		ELSE
			brokenVehicles[0].pedIndex = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].vehicleIndex, PEDTYPE_MISSION,  pedNames[0], VS_DRIVER)
			PRINTLN("CREATING PED INSIDE - CREATE_BROKEN_VEHICLE_ILLEGAL")
			
			SET_MODEL_AS_NO_LONGER_NEEDED(pedNames[0])
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL CREATE_BROKEN_VEHICLE_ACCIDENT(BOOL bBlip = FALSE)
	INT num = 0
	IF NOT CREATE_CAR(illegalNodes[illegalIdx].vCarPos, illegalNodes[illegalIdx].fCarRot, num, FALSE, bBlip)
		RETURN FALSE
	ENDIF
	fObjDist = GET_PLAYER_DISTANCE_FROM_LOCATION(illegalNodes[illegalIdx].vCarPos, FALSE)
	vCurDestObjective[0] = illegalNodes[illegalIdx].vCarPos
	
	RETURN TRUE
ENDFUNC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//NAME: CREATE_BROKEN_DOWN_VEHICLE
//Description: Calls the comm shared CREATE_CAR but does anything custom needed by the Traditional Towing Mode
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
FUNC BOOL CREATE_BROKEN_DOWN_VEHICLE(VECTOR spawnVec, FLOAT fOrient, INT& openIdx, BOOL bBlip = FALSE)

	VECTOR vTemp, vModelMin, vModelMax // vEngineOffset
	VECTOR vDimensions
	VECTOR vBrokenDownVehiclePosition
	FLOAT fBrokenDownVehicleHeading
		
	//Created a care at openIdx in the vehicle array brokenVehicles
	IF NOT CREATE_CAR(spawnVec, fOrient, openIdx, TRUE, bBlip)
		RETURN FALSE
	ENDIF
	
	DEBUG_MESSAGE("CREATE_BROKEN_DOWN_VEHICLE")
	
	IF openIdx = -1
		SCRIPT_ASSERT("openIdx = -1")
	ENDIF

	SET_VEHICLE_DOOR_OPEN(brokenVehicles[openIdx].vehicleIndex, SC_DOOR_BONNET)
	
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(brokenVehicles[openIdx].vehicleIndex), vModelMin, vModelMax)
	PRINTLN("MINIMUM = ", vModelMin)
	PRINTLN("MAXIMUM = ", vModelMax)
	
	vDimensions = vModelMax - vModelMin
	PRINTLN("DIMENSIONS = ", vDimensions)
	
	vDimensions = vDimensions / 2.0
	PRINTLN("USABLE DIMENSIONS = ", vDimensions)
	
	bSpawnPed = TRUE
	PRINTLN("bSpawnPed = TRUE")

	vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(brokenVehicles[openIdx].vehicleIndex, <<1.5,0,0>>) //vEngineOffset)
		
	vBrokenDownVehiclePosition = GET_ENTITY_COORDS(brokenVehicles[openIdx].vehicleIndex)
	PRINTLN("CREATING SPEED ZONE 4")
	fBrokenDownVehicleHeading = GET_ENTITY_HEADING(brokenVehicles[openIdx].vehicleIndex)
	PRINTLN("CREATING SPEED ZONE 5")
	
	iSpeedNode01 = ADD_ROAD_NODE_SPEED_ZONE(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBrokenDownVehiclePosition, fBrokenDownVehicleHeading, <<-1.5,-30,0>>), 10, 0)
	iSpeedNode02 = ADD_ROAD_NODE_SPEED_ZONE(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBrokenDownVehiclePosition, fBrokenDownVehicleHeading, <<-1.5,30,0>>), 10, 0)
	CLEAR_AREA_OF_VEHICLES(vTemp, 30)
	
	extraData[GET_IDX()].vUtilityPos[0] = <<vTemp.x, vTemp.y, vTemp.z+1>>
	RETURN TRUE

ENDFUNC

PROC UPDATE_PED_GIVE_UP(BOOL bCheckDetach = FALSE)
	BOOL bGiveUp = FALSE
	FLOAT fDist
//	STRING sLine

	fDist = GET_DISTANCE_BETWEEN_ENTITIES(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex) 

	IF bCheckDetach
		IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
			IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
			AND NOT IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex)
				bGiveUp = TRUE
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(brokenVehicles[0].pedIndex, PLAYER_PED_ID())
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(PLAYER_PED_ID(), brokenVehicles[0].pedIndex)
			RESTART_TIMER_NOW(chaseTimer)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(brokenVehicles[0].pedIndex)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	IF IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
		pedState = ANGRY_PED_DONE
	ELIF fDist > 25 OR (GET_TIMER_IN_SECONDS(chaseTimer) > 30 AND fDist > 5) OR bGiveUp
	OR (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE))
		DEBUG_MESSAGE("Give up")

//		IF bAttack
//			sLine = "TOW_PED_ANCU"
//		ELSE
//			sLine = "TOW_PED_ANCU"
//		ENDIF
		
		piSpeakers[0] = brokenVehicles[0].pedIndex
		piSpeakers[1] = brokenVehicles[0].pedIndexPass
		
		bAngryPedGaveUp = TRUE
		
		piSpeakers = piSpeakers
		
//		ODDJOB_START_CONVERSATION(MyLocalPedStruct, "TOWAUD", sLine, piSpeakers)
		//CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", sLine, CONV_PRIORITY_VERY_HIGH)
		
		REMOVE_BLIP(brokenVehicles[0].blipIndexPed)
		IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
			CLEAR_PED_TASKS(brokenVehicles[0].pedIndex)
			TASK_SMART_FLEE_PED(brokenVehicles[0].pedIndex, PLAYER_PED_ID(), 1000, -1)
		ENDIF
		//TASK_WANDER_STANDARD(brokenVehicles[0].pedIndex)
		pedState = ANGRY_PED_DONE
	ENDIF
ENDPROC

FUNC BOOL HAS_PED_PLAYED_ANIM_WHEN_PUSHED()

	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) 
		IF bPushedOffTrack
		
			SWITCH iPushStages
				CASE 0
					IF GET_SEQUENCE_PROGRESS(brokenVehicles[0].pedIndex) = 1
						PRINTLN("iPushStages = 1")
						iPushStages = 1
					ENDIF
				BREAK
				CASE 1
					IF NOT IS_ENTITY_PLAYING_ANIM(brokenVehicles[0].pedIndex, "oddjobs@towingpleadingidle_b", "idle_d")
						PRINTLN("iPushStages = 2")
						iPushStages = 2
					ENDIF
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_TRAIN_PED()

	IF soundID3 = -1
		IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
			IF GET_PED_IN_VEHICLE_SEAT(brokenVehicles[0].vehicleIndex) = brokenVehicles[0].pedIndex
				ODDJOB_PLAY_SOUND("ENGINE_FAIL", soundID3, TRUE, brokenVehicles[0].vehicleIndex, 0, 0, 0, "TONYA_03_SOUNDSET")
			ELSE
//				PRINTLN("TRYING TO STOP SOUND 1")
				ODDJOB_STOP_SOUND(soundID3)
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH trainPedState
		CASE TRAIN_STATE_PED_INIT
			//DEBUG_MESSAGE("TRAIN_STATE_PED_INIT")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(),"FRANKLIN")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iPedID, brokenVehicles[0].pedIndex, sCharacterName)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
			fPanicDist = GET_RANDOM_INT_IN_RANGE(35, 80)
			trainPedState =  TRAIN_STATE_PED_FIRST
		BREAK
		CASE TRAIN_STATE_PED_FIRST
			//DEBUG_MESSAGE("TRAIN_STATE_PED_FIRST")		
			
			OPEN_SEQUENCE_TASK(taskSequence)
				//TASK_PLAY_ANIM(NULL, "oddjobs@towing","Start_Engine_Enter",NORMAL_BLEND_IN,NORMAL_BLEND_OUT)//, -1, AF_LOOPING)
				TASK_PLAY_ANIM(NULL, "oddjobs@towing","Start_Engine_Loop",NORMAL_BLEND_IN,NORMAL_BLEND_OUT, -1, AF_LOOPING)
			CLOSE_SEQUENCE_TASK(taskSequence)
			IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
				TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
			ENDIF
			CLEAR_SEQUENCE_TASK(taskSequence)
			//SET_ENTITY_ANIM_CURRENT_TIME()
			//GET_SCRIPT_TASK_STATUS()
			
			//Trigger first line about stalled car if the player is close enough to hear it or in the cam
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(illegalNodes[illegalIdx].vCarPos) < 40 OR towArgs.iCamTime > 0
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iPedID, brokenVehicles[0].pedIndex, "TOWTRAINM")
				CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_TRN_PEDA", CONV_PRIORITY_HIGH)
				trainPedState =  TRAIN_STATE_PED_LOOK
			ELIF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) < fPanicDist+10
				TASK_PLAY_ANIM(NULL, "oddjobs@towing","Start_Engine_Exit",NORMAL_BLEND_IN,NORMAL_BLEND_OUT)
				//skip ahead if the train is getting close
				trainPedState =  TRAIN_STATE_PED_LOOK
			ENDIF
		BREAK
		CASE TRAIN_STATE_PED_LOOK
//			DEBUG_MESSAGE("TRAIN_STATE_PED_LOOK")
			//temp remove this check
			IF NOT IS_VECTOR_ZERO(illegalNodes[illegalIdx].vAngledPos1)
			illegalNodes[illegalIdx].vAngledPos1.z = 45
			illegalNodes[illegalIdx].vAngledPos2.z = -45
				IF NOT IS_ENTITY_IN_ANGLED_AREA(brokenVehicles[0].vehicleIndex, illegalNodes[illegalIdx].vAngledPos1, illegalNodes[illegalIdx].vAngledPos2, illegalNodes[illegalIdx].fAngledWidth)
					//SCRIPT_ASSERT("entity isn't in angeled area")
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, brokenVehicles[0].pedIndex, "TOWTRAINM")
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_TRN_PUSH", CONV_PRIORITY_VERY_HIGH)
					ODDJOB_STOP_SOUND(soundID3)
					bDisablePedCam = TRUE
					bDisableCam = TRUE
					
					bPushedOffTrack = TRUE
					PRINTLN("SETTING bDisablePedCam = TRUE BECAUSE PED IS OUTSIDE ANGLED AREA")
					IF IS_PED_IN_ANY_VEHICLE(brokenVehicles[0].pedIndex)
						OPEN_SEQUENCE_TASK(taskSequence)
							TASK_LEAVE_ANY_VEHICLE(NULL)
//							TASK_GOTO_ENTITY_OFFSET(NULL, brokenVehicles[0].vehicleIndex, DEFAULT_TIME_BEFORE_WARP, 1.0, 0, PEDMOVE_WALK)							
							TASK_PLAY_ANIM(NULL, "oddjobs@towingpleadingidle_b", "idle_d", SLOW_BLEND_IN, SLOW_BLEND_OUT, 10000, AF_UPPERBODY)
						CLOSE_SEQUENCE_TASK(taskSequence)
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
							TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
						ENDIF
						CLEAR_SEQUENCE_TASK(taskSequence)
						
						SET_PED_MOVEMENT_CLIPSET(brokenVehicles[0].pedIndex, "move_m@JOG@")
						
						trainPedState =  TRAIN_STATE_PED_PENDING
					ENDIF
				ENDIF
			ELSE
				PRINTLN("VECTOR IS ZERO")
			ENDIF
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) < fPanicDist+15
				//TOW_TRN_PPE1 //flee
				//TOW_TRN_PPS1 //stuck
				CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_TRN_PEDA", CONV_PRIORITY_VERY_HIGH)
				ODDJOB_STOP_SOUND(soundID3)
				TASK_LOOK_AT_ENTITY(brokenVehicles[0].pedIndex, brokenVehicles[0].blockingIndex, -1, SLF_FAST_TURN_RATE)
				SETTIMERB(0)
				DEBUG_MESSAGE("Setting timer b to 0")
				trainPedState =  TRAIN_STATE_PED_PANIC
			ENDIF
		BREAK
		CASE TRAIN_STATE_PED_PANIC
//			DEBUG_MESSAGE("TRAIN_STATE_PED_PANIC")
			//look for at least a second
			//IF TIMERB() > 3000
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) < fPanicDist
					//have ped exit car and run
					IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
						IF iToggle = 1
							CLEANUP_CAM(towArgs)
						ENDIF
						
						bDisablePedCam = TRUE
						IF IS_PED_IN_ANY_VEHICLE(brokenVehicles[0].pedIndex)
							OPEN_SEQUENCE_TASK(taskSequence)
								TASK_LEAVE_ANY_VEHICLE(NULL)
//								TASK_SMART_FLEE_COORD(NULL, illegalNodes[illegalIdx].vCarPos, 15.0, 3000)
								TASK_GO_TO_COORD_ANY_MEANS(NULL, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(illegalNodes[illegalIdx].vCarPos, illegalNodes[illegalIdx].fCarRot, 
										<< 0, 15, 0 >>), PEDMOVEBLENDRATIO_SPRINT, NULL)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
							CLOSE_SEQUENCE_TASK(taskSequence)
							SET_PED_FLEE_ATTRIBUTES(brokenVehicles[0].pedIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
							ENDIF
							CLEAR_SEQUENCE_TASK(taskSequence)
							DEBUG_MESSAGE("TRAIN_STATE_PED_PENDING")
							trainPedState =  TRAIN_STATE_PED_PENDING
						ENDIF
					ENDIF
				ENDIF
			//ENDIF
		BREAK
		CASE TRAIN_STATE_PED_PENDING
//			DEBUG_MESSAGE("TRAIN_STATE_PED_PENDING")
			IF trainState = TRAIN_STATE_DONE AND bTowHooked
			OR HAS_PED_PLAYED_ANIM_WHEN_PUSHED()
//				bEnableBonusCam = FALSE
				SETTIMERB(3000)
				DEBUG_MESSAGE("TRAIN_STATE_PED_ENTER_TRUCK")
				trainPedState = TRAIN_STATE_PED_ENTER_TRUCK
			ENDIF
			//If the train moves past and the tow truck is attached then transition to the tow truck.
		BREAK
		CASE TRAIN_STATE_PED_ENTER_TRUCK
//			DEBUG_MESSAGE("TRAIN_STATE_PED_ENTER_TRUCK - 01")
			bReverseOrder = TRUE
			//Don't get out right away if skipped
			IF TIMERB() > 3000
				IF UPDATE_BROKEN_VEHICLE_PED(brokenState, 0, "TOW_TRAIN1")
					PRINTLN("GOING TO STATE - TRAIN_STATE_PED_END")
					trainPedState =  TRAIN_STATE_PED_END
				ENDIF
			ENDIF
		BREAK
		CASE TRAIN_STATE_PED_END
			//DEBUG_MESSAGE("TRAIN_STATE_PED_END")
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC HANDLE_TRAIN_EXPLODING_CAR()

	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
		IF IS_ENTITY_TOUCHING_ENTITY(brokenVehicles[0].blockingIndex, brokenVehicles[0].vehicleIndex) 
			
			IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
				IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
					DETACH_VEHICLE_FROM_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
				ENDIF
			ENDIF
			EXPLODE_VEHICLE(brokenVehicles[0].vehicleIndex)
			PRINTLN("TRAIN IS TOUCHING CAR, LET'S BLOW IT UP")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL UPDATE_TRAIN()
	VECTOR tempa
#IF IS_DEBUG_BUILD
//	VECTOR tempb
//	FLOAT fDistanceBetween
#ENDIF	//	IS_DEBUG_BUILD
	
	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) AND NOT IS_ENTITY_DEAD(myVehicle)
		IF IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, myVehicle)
			IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndexExtra)
				REMOVE_BLIP(brokenVehicles[0].blipIndexExtra)
				PRINTLN("REMOVING BLIP - brokenVehicles[0].blipIndexExtra")
//				PRINTLN("TRYING TO STOP SOUND 3")
				ODDJOB_STOP_SOUND(soundID3)
			ENDIF
		ELIF NOT IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex)
//			PRINTLN("TRYING TO STOP SOUND 2")
			ODDJOB_STOP_SOUND(soundID3)
		ELSE
//			PRINTLN("bPedInTowTruck IS FALSE - 01")	
		ENDIF
	ENDIF
	
	HANDLE_TRAIN_EXPLODING_CAR()
	
	SWITCH trainState
		CASE TRAIN_STATE_PRINT
//			DEBUG_MESSAGE("TRAIN_STATE_PRINT")
		
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(illegalNodes[illegalIdx].vCarPos) < 100
				brokenVehicles[0].blockingIndex = CREATE_MISSION_TRAIN(6, illegalNodes[illegalIdx].vPedPos, illegalNodes[illegalIdx].bBackward)
				IF DOES_ENTITY_EXIST(brokenVehicles[0].blockingIndex)
					brokenVehicles[0].blipIndexExtra = ADD_BLIP_FOR_ENTITY(brokenVehicles[0].blockingIndex)
					PRINTLN("ADDING BLIP ON TRAIN")
				ENDIF
				
				pedTrainDriver = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].blockingIndex, PEDTYPE_CIVMALE, S_M_M_LSMETRO_01)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTrainDriver, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_LSMETRO_01)
				
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex)
					SET_TRAIN_CRUISE_SPEED(brokenVehicles[0].blockingIndex, 0.0)
				ENDIF
				PRINT_NOW("TOW_OBJ_05", DEFAULT_GOD_TEXT_TIME, 1)
				SETTIMERA(0)
				PRINTLN("Initial Train Distance:", VDIST(illegalNodes[illegalIdx].vPedPos, illegalNodes[illegalIdx].vCarPos), " Train index: ", illegalIdx)
				
				ODDJOB_PLAY_SOUND("CROSSING_BELL", soundID4, TRUE, NULL, 226.8878, -2538.4209, 4.8665, "TONYA_03_SOUNDSET")
				
				trainState = TRAIN_STATE_WAIT
			ENDIF
		BREAK
		CASE TRAIN_STATE_WAIT
//			DEBUG_MESSAGE("TRAIN_STATE_WAIT")
		
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(illegalNodes[illegalIdx].vCarPos) < 80 OR TIMERA() > 10000
			
				// Start oncoming train scene
				IF NOT IS_AUDIO_SCENE_ACTIVE("TOWING_ONCOMING_TRAIN_SCENE")
					START_AUDIO_SCENE("TOWING_ONCOMING_TRAIN_SCENE")
					PRINTLN("STARTING AUDIO SCENE - TOWING_ONCOMING_TRAIN_SCENE")
				ENDIF
			
				trainState = TRAIN_STATE_START
			ENDIF
		BREAK
		CASE TRAIN_STATE_START
			DEBUG_MESSAGE("TRAIN_STATE_START")
		
			//PRINTLN(GET_DISTANCE_BETWEEN_ENTITIES(brokenVehicles[0].blockingIndex, brokenVehicles[0].vehicleIndex))
			IF NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex)
				PRINTLN("Setting the train speed to 22.0")
				SET_TRAIN_CRUISE_SPEED(brokenVehicles[0].blockingIndex, 26.0)
				SET_TRAIN_SPEED(brokenVehicles[0].blockingIndex, 22.0)
			ENDIF
			trainState = TRAIN_STATE_BREAK
		BREAK
		CASE TRAIN_STATE_BREAK
			DEBUG_MESSAGE("TRAIN_STATE_BREAK")
		
			IF NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) < 175
					SIMULATE_BREAKING(brokenVehicles[0].blockingIndex)
					ODDJOB_PLAY_SOUND("TRAIN_BREAKS", soundID2, TRUE, brokenVehicles[0].blockingIndex, 0, 0, 0, "TONYA_03_SOUNDSET")
					START_BREAKING_PFX(sparksEffects, brokenVehicles[0].blockingIndex)
					ODDJOB_PLAY_SOUND("TRAIN_HORN", soundID, TRUE, brokenVehicles[0].blockingIndex, 0, 0, 0, "TONYA_03_SOUNDSET")
					trainState = TRAIN_STATE_CHECKS
				ENDIF
			ELSE
				trainState = TRAIN_STATE_DONE
			ENDIF
		BREAK
		CASE TRAIN_STATE_CHECKS
			DEBUG_MESSAGE("Checks")
			
			IF NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
				
				SIMULATE_BREAKING(brokenVehicles[0].blockingIndex)
				
				IF NOT IS_ENTITY_IN_ANGLED_AREA(brokenVehicles[0].vehicleIndex, illegalNodes[illegalIdx].vAngledPos1, illegalNodes[illegalIdx].vAngledPos2, illegalNodes[illegalIdx].fAngledWidth)
					STOP_BREAKING_PFX(sparksEffects)
//					PRINTLN("STOPPING BREAKING EFFECTS, SINCE WE'RE NOT ON THE TRACKS")
				ENDIF
				
				tempa = GET_ENTITY_COORDS(brokenVehicles[0].blockingIndex)

				#IF IS_DEBUG_BUILD
//					tempb = GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex)
//					fDistanceBetween = VDIST(tempa, tempb)
//					PRINTLN("fDistanceBetween = ", fDistanceBetween)
				#ENDIF
				
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) < 3
					bTrainClose = TRUE
				ENDIF
				
//				PRINTLN("Is entity touching entity:", "Train pos: ", tempa, " Car pos: ", tempb)

				IF IS_ENTITY_TOUCHING_ENTITY(brokenVehicles[0].blockingIndex, brokenVehicles[0].vehicleIndex) 
					PRINTLN("Train is touching car")
					IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
							DETACH_VEHICLE_FROM_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
						ENDIF
					ENDIF
					EXPLODE_VEHICLE(brokenVehicles[0].vehicleIndex)
					trainState = TRAIN_STATE_STOP
				ELIF GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].vehicleIndex, tempa) < 10
				AND IS_ENTITY_IN_ANGLED_AREA(brokenVehicles[0].vehicleIndex, illegalNodes[illegalIdx].vAngledPos1, illegalNodes[illegalIdx].vAngledPos2, illegalNodes[illegalIdx].fAngledWidth)
					bTrainClose = TRUE
					// HACK: Adding in to fix entity touching entity bug
					IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
							DETACH_VEHICLE_FROM_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
						ENDIF
					ENDIF
					EXPLODE_VEHICLE(brokenVehicles[0].vehicleIndex)
					PRINTLN("HITTING DITANCE CHECK!")
					// HACK: Adding in to fix entity touching entity bug
				ENDIF
				
				//The train was close to the target vehicle but now is getting further away.  
				IF bTrainClose AND GET_ENTITY_DISTANCE_FROM_LOCATION(brokenVehicles[0].blockingIndex, illegalNodes[illegalIdx].vCarPos) > 4
					PRINTLN("Train is getting further away")
					trainState = TRAIN_STATE_STOP
				ENDIF
				
			ELSE
				trainState = TRAIN_STATE_STOP
			ENDIF
		BREAK
		CASE TRAIN_STATE_STOP
			DEBUG_MESSAGE("TRAIN_STATE_STOP")
		
			IF NOT bOutOfVehicle
				IF NOT bTowHooked
					PRINT_NOW("TOWT_OBJ_03G", DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
			ELSE
				sLastObjective = "TOWT_OBJ_03G"
			ENDIF
			
			ODDJOB_STOP_SOUND(soundID)
			ODDJOB_STOP_SOUND(soundID2)
			STOP_BREAKING_PFX(sparksEffects)
			ODDJOB_STOP_SOUND(soundID3)
			ODDJOB_STOP_SOUND(soundID4)
			trainState = TRAIN_STATE_OBJ
		BREAK
		CASE TRAIN_STATE_OBJ
			DEBUG_MESSAGE("TRAIN_STATE_OBJ")
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				IF NOT bOutOfVehicle
					IF NOT bTowHooked
						PRINT_NOW("TOWT_OBJ_03G", DEFAULT_GOD_TEXT_TIME, 1)
					ENDIF
				ELSE
					sLastObjective = "TOWT_OBJ_03G"
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("TOWING_ONCOMING_TRAIN_SCENE")
					STOP_AUDIO_SCENE("TOWING_ONCOMING_TRAIN_SCENE")
					PRINTLN("STOPPING AUDIO SCENE - TOWING_ONCOMING_TRAIN_SCENE VIA MAIN UPDATE")
				ENDIF
				
				trainState = TRAIN_STATE_DONE
			ENDIF
		BREAK
		CASE TRAIN_STATE_DONE
		
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_PED_DIALOUGE()
//	PRINTLN("UPDATE_PED_DIALOUGE")

	TEXT_LABEL_23 tTestLabel
	SWITCH pedDialougeState
		CASE TOW_PED_BEG
//		PRINTLN("TOW_PED_BEG")
//		//should be able to just let this trigger: CREATE_CONVERSATION_FROM_SPECIFIC_LINE()
		
			IF GET_SEQUENCE_PROGRESS(brokenVehicles[0].pedIndex) >= 1 AND NOT IS_MESSAGE_BEING_DISPLAYED()
				
				PRINTLN("GOING TO STATE - TOW_PED_FACE")
				pedDialougeState = TOW_PED_FACE
			ENDIF
		BREAK
		CASE TOW_PED_FACE
//			PRINTLN("TOW_PED_FACE")
			tTestLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL() 
			
			IF bPlayInitialUpsetLine AND NOT bPlayFullConvo
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					IF NOT bPrintSpecialCaseObj
						PRINT_NOW("TOWT_OBJ_06", DEFAULT_GOD_TEXT_TIME, 1)
						bPrintSpecialCaseObj = TRUE
					ENDIF
				
					IF bPrintSpecialCaseObj AND NOT IS_MESSAGE_BEING_DISPLAYED()
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_SEX_SCAR", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("PLAYING CONVO - TOW_SEX_SCAR")
							bPlayFullConvo = TRUE
							
							PRINTLN("GOING TO STATE - TOW_PED_CHASE")
							pedDialougeState= TOW_PED_CHASE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF ARE_STRINGS_EQUAL(tTestLabel, "TOW_SEX_SCAR_2")
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
					IF GET_SCRIPT_TASK_STATUS(brokenVehicles[0].pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
						TASK_TURN_PED_TO_FACE_ENTITY(brokenVehicles[0].pedIndex, brokenVehicles[0].pedIndexPass)
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(brokenVehicles[0].pedIndex) > 20.0
				PRINTLN("STOPPING SCRIPTED CONVERSATION, BECAUSE PLAYER HAS EXCEEDED 90M DISTANCE - 01")
				STOP_SCRIPTED_CONVERSATION(TRUE)
				RETURN TRUE
			ENDIF
		BREAK
		CASE TOW_PED_CHASE
		
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(brokenVehicles[0].pedIndex) > 20.0
				PRINTLN("STOPPING SCRIPTED CONVERSATION, BECAUSE PLAYER HAS EXCEEDED 90M DISTANCE - 02")
				STOP_SCRIPTED_CONVERSATION(TRUE)
				RETURN TRUE
			ENDIF
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				PRINTLN("RETURN TRUE - UPDATE_PED_DIALOUGE - 02")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//Function used by handicap towing to trigger the ped that runs out and complaings that about you towing their car.
//TODO add suport for ped that attacks the player
FUNC BOOL UPDATE_ANGRY_PED()
	
	SWITCH pedState
		CASE ANGRY_PED_INIT
		
			IF bHomelessVariation
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					brokenVehicles[0].pedIndex = CREATE_PED_INSIDE_VEHICLE(brokenVehicles[0].vehicleIndex, PEDTYPE_MISSION, pedNames[0], VS_FRONT_RIGHT)
					SET_MODEL_AS_NO_LONGER_NEEDED(pedNames[0])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
					SET_PED_CONFIG_FLAG(brokenVehicles[0].pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iPedID, brokenVehicles[0].pedIndex, sCharacterName)
					TASK_PLAY_ANIM(brokenVehicles[0].pedIndex,"RANDOM@CAR_SLEEPING","Sleeping_Idle",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
					bPedCreated = TRUE
					PRINTLN("bPedCreated = TRUE - 03")
				ELSE
					PRINTLN("VEHICLE DEAD - NO PED CREATION")
				ENDIF
			ELIF GET_RANDOM_BOOL()
				DEBUG_MESSAGE("bAttack = TRUE")
				bAttack = FALSE
			ELSE
				DEBUG_MESSAGE("bAttack = FALSE")
				bAttack = FALSE
			ENDIF
			pedState = ANGRY_PED_CREATE
		BREAK
		CASE ANGRY_PED_CREATE
			
			//attaching the hook and bumping / horn should wake her up..
			IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
				IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
					IF NOT bHomelessVariation
						START_TIMER_AT(wakeTimer, 1.0)
					ELSE
						START_TIMER_NOW(wakeTimer)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_TIMER_STARTED(wakeTimer) AND GET_TIMER_IN_SECONDS(wakeTimer) > 1.0 
				IF IS_TIMER_STARTED(wakeTimer)
					IF bHomelessVariation
						//homeless attack variation.
						IF g_savedGlobals.sTowingData.iAbandonCompleted% 2 = 0
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								OPEN_SEQUENCE_TASK(taskSequence)
									TASK_LEAVE_ANY_VEHICLE(NULL)
									TASK_GO_TO_ENTITY(NULL, myVehicle, DEFAULT_TIME_BEFORE_WARP, 9.0, PEDMOVEBLENDRATIO_WALK)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									TASK_PLAY_ANIM(NULL, "oddjobs@towingangryidle_a", "idle_b", NORMAL_BLEND_OUT, SLOW_BLEND_DURATION, 7000)
									TASK_WANDER_STANDARD(NULL)
								CLOSE_SEQUENCE_TASK(taskSequence)
								IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
									TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
								ENDIF
								CLEAR_SEQUENCE_TASK(taskSequence)
							
	//							bEnableBonusCam = FALSE
							ENDIF
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_ANGR", CONV_PRIORITY_VERY_HIGH)
						ELSE
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								SETUP_PED_FOR_COMBAT(brokenVehicles[0].pedIndex)
								SET_PED_COMBAT_ATTRIBUTES(brokenVehicles[0].pedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(brokenVehicles[0].pedIndex, CA_USE_VEHICLE, FALSE)
								OPEN_SEQUENCE_TASK(taskSequence)
									TASK_LEAVE_ANY_VEHICLE(NULL)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									//TASK_PLAY_ANIM(NULL, "gestures@male", "anger_a")
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(taskSequence)
								IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
									TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
								ENDIF
								CLEAR_SEQUENCE_TASK(taskSequence)
								
								IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
									tempBlip = ADD_BLIP_FOR_ENTITY(brokenVehicles[0].pedIndex)
									SET_BLIP_COLOUR(tempBlip, BLIP_COLOUR_RED)
									SET_BLIP_SCALE(tempBlip, BLIP_SIZE_PED)
									REGISTER_BLIPPED_ENTITY(brokenVehicles[0].pedIndex, tempBlip, TRUE)
									PRINTLN("ADDING RED BLIP FOR PED")
								ENDIF
								
								bAttack = TRUE
								PRINTLN("SETTING bAttack = TRUE VIA bHomelessVariation")
								
	//							bEnableBonusCam = FALSE
							ENDIF
							CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_DRG", CONV_PRIORITY_VERY_HIGH)
						ENDIF
						//who woke me up?
						
						pedState = ANGRY_PED_EXIT
					ELIF bAttack
						brokenVehicles[0].pedIndex = CREATE_PED(PEDTYPE_CIVMALE, pedNames[0], illegalNodes[illegalIdx].vPedPos, illegalNodes[illegalIdx].fPedRot) 
						PRINTLN("CREATING PED - bAttack IS TRUE")
						
						//GIVE_WEAPON_TO_PED(brokenVehicles[0].pedIndex, WEAPONTYPE_PISTOL, 200)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
						SET_PED_CONFIG_FLAG(brokenVehicles[0].pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
						bPedCreated = TRUE
						PRINTLN("bPedCreated = TRUE - 04")
						
						angryPedRoot = "TOW_PED_ANGR"
						bPedCreated = FALSE
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
							OPEN_SEQUENCE_TASK(taskSequence)
								TASK_GO_TO_ENTITY(NULL, myVehicle, DEFAULT_TIME_BEFORE_WARP, 15.0, PEDMOVEBLENDRATIO_SPRINT) //brokenVehicles[0].vehicleIndex
								TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(taskSequence)
							TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
							CLEAR_SEQUENCE_TASK(taskSequence)
	//						bEnableBonusCam = FALSE
						ENDIF
						pedState = ANGRY_PED_WAIT
					ELSE
						brokenVehicles[0].pedIndex = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_GENFAT_01, illegalNodes[illegalIdx].vPedPos, illegalNodes[illegalIdx].fPedRot) 
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(brokenVehicles[0].pedIndex, TRUE)
						SET_PED_CONFIG_FLAG(brokenVehicles[0].pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
						PRINTLN("CREATING PED - REGULAR")
						SET_ENTITY_LOAD_COLLISION_FLAG(brokenVehicles[0].pedIndex, TRUE)
						SET_PED_MOVEMENT_CLIPSET(brokenVehicles[0].pedIndex, "move_m@JOG@")
						SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_GENFAT_01)
						
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, brokenVehicles[0].pedIndex, "TOWILLEGALMAN1")
						
						bPedCreated = TRUE
						PRINTLN("bPedCreated = TRUE - 05")
						
						angryPedRoot = "TOW_ILLEG2"
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
							OPEN_SEQUENCE_TASK(taskSequence)
								TASK_GOTO_ENTITY_OFFSET(NULL, PLAYER_PED_ID(), DEFAULT_TIME_BEFORE_WARP, 8.0, 0, PEDMOVEBLENDRATIO_RUN)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								//TASK_PLAY_ANIM(NULL, "gestures@male", "anger_a")
							CLOSE_SEQUENCE_TASK(taskSequence)
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
							ENDIF
							CLEAR_SEQUENCE_TASK(taskSequence)
	//						bEnableBonusCam = FALSE
						ENDIF
						pedState = ANGRY_PED_WAIT
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE ANGRY_PED_EXIT
//			PRINTLN("INSIDE STATE - ANGRY_PED_EXIT")
			
			IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
				IF NOT IS_PED_IN_ANY_VEHICLE(brokenVehicles[0].pedIndex)
					IF bHomelessVariation
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
							IF GET_SCRIPT_TASK_STATUS(brokenVehicles[0].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
								IF GET_SEQUENCE_PROGRESS(brokenVehicles[0].pedIndex) = 2 // check task status
									IF g_savedGlobals.sTowingData.iAbandonCompleted% 2 = 0 //TODO pick the right enums for variaitons.. male - female
										CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_ANG1", CONV_PRIORITY_VERY_HIGH)
									ELSE
										CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HOM_ANG2", CONV_PRIORITY_VERY_HIGH)
									ENDIF
									
									PRINTLN("GOING TO STATE - ANGRY_PED_DONE")
									pedState = ANGRY_PED_DONE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
							IF UPDATE_PED_DIALOUGE() OR NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
//								IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
									KILL_ANY_CONVERSATION()
									PRINTLN("KILLING CONVERSATION - TOW TRUCK NO LONGER ATTACHED")
									
									PRINTLN("GOING TO STATE - ANGRY_PED_CHASE")
									pedState = ANGRY_PED_CHASE
//								ENDIF
							ENDIF
						ELSE
//							PRINTLN("STOPPED HERE")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE ANGRY_PED_WAIT
//			DEBUG_MESSAGE("ANGRY_PED_WAIT")
			
			IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
				//IF GET_SEQUENCE_PROGRESS(brokenVehicles[0].pedIndex) >= 1 

				IF GET_DISTANCE_BETWEEN_ENTITIES(brokenVehicles[0].pedIndex, PLAYER_PED_ID()) < 12 //HACK.. TASK_GO_TO_ENTITY has a LONG delay before thinking it's done
					IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
						CLEAR_PED_TASKS(brokenVehicles[0].pedIndex)
					ENDIF
					
					IF bAttack
						TASK_COMBAT_PED(brokenVehicles[0].pedIndex, PLAYER_PED_ID())
					ELIF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
						DEBUG_MESSAGE("play anim")
						OPEN_SEQUENCE_TASK(taskSequence)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
							TASK_PLAY_ANIM(NULL, "oddjobs@towingpleadingidle_a", "idle_c", NORMAL_BLEND_OUT, SLOW_BLEND_DURATION, -1, AF_LOOPING)
						CLOSE_SEQUENCE_TASK(taskSequence)
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
							TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndex, taskSequence)
						ENDIF
						CLEAR_SEQUENCE_TASK(taskSequence)
					ENDIF
					
					DEBUG_MESSAGE(">= 1 or within 20m")
					bInCombat = TRUE
					
					SETTIMERB(0)
					RESTART_TIMER_NOW(chaseTimer)
					PRINTLN("GOING TO STATE - ANGRY_PED_DIALOUGE - 01")
					pedState = ANGRY_PED_DIALOUGE
				ENDIF
			ENDIF
		BREAK
		CASE ANGRY_PED_DIALOUGE
//			DEBUG_MESSAGE("ANGRY_PED_DIALOUGE")
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iPedID, brokenVehicles[0].pedIndex, sCharacterName)
			
			bSupressObjective = TRUE
			PRINTLN("bSupressObjective = TRUE 01")
			REGISTER_QUICK_CONV_QUEUE(MyLocalPedStruct, "TOWAUD", angryPedRoot)
	
			SETTIMERA(0)
			
			IF bAttack
				RESTART_TIMER_NOW(chaseTimer)
				pedState = ANGRY_PED_ENTER
			ELSE
				PRINTLN("GOING TO STATE - ANGRY_PED_ENTER - 01")
				pedState = ANGRY_PED_ENTER
			ENDIF
		BREAK
		CASE ANGRY_PED_CHASE
		
//			PRINTLN("INSIDE - ANGRY_PED_CHASE")
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
			
				RESTART_TIMER_NOW(chaseTimer)
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) 
				AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex) 
				AND NOT IS_ENTITY_DEAD(myVehicle)
					IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
						TASK_FOLLOW_TO_OFFSET_OF_ENTITY(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, <<0,0,0>>, PEDMOVEBLENDRATIO_RUN, -1, 10.0)
						PRINTLN("TASK GUY TO CHASE SINCE THE VEHICLE IS STILL ATTACHED TO TOW TRUCK")
					ENDIF
				ENDIF
				
				PRINTLN("GOING TO STATE - ANGRY_PED_ENTER - 02")
				pedState = ANGRY_PED_ENTER
			ENDIF
		BREAK
		CASE ANGRY_PED_ENTER
		
//			PRINTLN("INSIDE STATE - ANGRY_PED_ENTER")
		
			UPDATE_PED_GIVE_UP(FALSE)
			
			IF NOT bAngryPedGaveUp 				// Adding this flag here to prevent the ped from giving up then getting a late task to re-enter
				//take into account the sex ped
				IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
//						IF TIMERB() > 5000
//							
//							RESTART_TIMER_NOW(chaseTimer)
//							SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_UNLOCKED)
//							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
//								TASK_ENTER_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, DEFAULT_TIME_BEFORE_WARP)
//								PRINTLN("GOING TO STATE - ANGRY_PED_DRIVE - 02")
//							ENDIF
							
							pedState = ANGRY_PED_DRIVE
//						ENDIF
					ELSE
						PRINTLN("GOING TO STATE - ANGRY_PED_DRIVE - 03")
						pedState = ANGRY_PED_DRIVE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE ANGRY_PED_DRIVE
		
			UPDATE_PED_GIVE_UP(FALSE)
			
			//TODO add a give up time or if you re-hook
			IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
				IF IS_PED_IN_ANY_VEHICLE(brokenVehicles[0].pedIndex) 
				AND (NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex) AND 
				NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex))
				
					IF bAttack
						angryPedRoot = "TOW_PED_ANTH"
					ENDIF
					
					PRINTLN("ANGRY PED IS IN HIS VEHICLE")
					
					bDriverGotCarBack = TRUE
					
					CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", angryPedRoot, CONV_PRIORITY_VERY_HIGH)
				
					bSupressObjective = FALSE
					TASK_VEHICLE_DRIVE_WANDER(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, 5.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
					//TASK_VEHICLE_DRIVE_WANDER(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleInde, 5.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
					pedState = ANGRY_PED_DONE
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE ANGRY_PED_DONE
		
//			PRINTLN("INSIDE STATE - ANGRY_PED_DONE")
		
			bInCombat = FALSE
			bSupressObjective = FALSE
			IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndexPed)
				REMOVE_BLIP(brokenVehicles[0].blipIndexPed)
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_CREATE_STATE(STRING sObjective, STRING sHelp, INT& openIdx, BOOL bDynamic = FALSE, BOOL bWithPed = FALSE)
	DEBUG_MESSAGE("HANDLE_CREATE_STATE")
	fObjDist = GET_PLAYER_DISTANCE_FROM_LOCATION(getPointVec, FALSE)
	vCurDestObjective[0] = getPointVec
	
	//Moved actual prints to MISSION_STATE_PRINTS.. not ideal but needed to get cars spawning earlier.
	sMyObjective = sObjective
	sHelp = sHelp
	//sMyHelp = sHelp
			
	IF bDynamic
		IF CREATE_BROKEN_DOWN_VEHICLE(getPointVec, getPointHead, openIdx, FALSE)
			INIT_DAMAGE_TRACKING(brokenVehicles[0].vehicleIndex, iLastHealth, iInitialHealth)
			SET_FORCE_HD_VEHICLE(brokenVehicles[0].vehicleIndex, TRUE)
			SET_VEHICLE_TYRES_CAN_BURST(brokenVehicles[0].vehicleIndex, FALSE)
			SET_ENTITY_LOAD_COLLISION_FLAG(brokenVehicles[0].vehicleIndex, TRUE)
			SET_VEHICLE_HAS_STRONG_AXLES(brokenVehicles[0].vehicleIndex, TRUE)
			PRINTLN("HANDLE_CREATE_STATE - bDynamic IS TRUE")
			RETURN TRUE
		ENDIF
	ELSE
		IF CREATE_BROKEN_VEHICLE_ILLEGAL(bWithPed, FALSE)
			INIT_DAMAGE_TRACKING(brokenVehicles[0].vehicleIndex, iLastHealth, iInitialHealth)
			SET_FORCE_HD_VEHICLE(brokenVehicles[0].vehicleIndex, TRUE)
			SET_VEHICLE_TYRES_CAN_BURST(brokenVehicles[0].vehicleIndex, FALSE)
			SET_ENTITY_LOAD_COLLISION_FLAG(brokenVehicles[0].vehicleIndex, TRUE)
			SET_VEHICLE_HAS_STRONG_AXLES(brokenVehicles[0].vehicleIndex, TRUE)
			PRINTLN("HANDLE_CREATE_STATE - bDynamic IS FALSE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FIND_CLOSE_GARAGE()
	INT idx 
	idx = FIND_CLOSEST_LOCATION_INDEX(vGarages, vCurDestObjective[0], 400)
	IF idx <> -1
		vDest = vGarages[idx]
		GET_DATA_FROM_GARAGE_LOCATION(idx, myGarageData)
	ELSE
		SCRIPT_ASSERT("Couldn't find a garage defaulting to 0")
		vDest = vGarages[0]
	ENDIF
ENDPROC

PROC HANDLE_TIMER()
	IF NOT bStopTimer
		INT iTimer = ROUND((FAIL_TIME_TOTAL - GET_TIMER_IN_SECONDS_SAFE(failTimer)) * 1000.0)
	//	PRINTLN("TIMER = ", iTimer)
		
		IF chosenNodeType = NODE_TYPE_DYNAMIC
			IF iTimer/1000 < 60
				DRAW_GENERIC_TIMER(iTimer, "TOW_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1,
										PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
			ELSE
				DRAW_GENERIC_TIMER(iTimer, "TOW_TIME")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_ADDITIONAL_HELP_PRINTS()
	INT iRand
	VECTOR vPlayerPos, vBrokenVehiclePos
	
	IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex) 
	AND IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
	OR ((g_savedGlobals.sTowingData.iTowingJobsCompleted = 0) OR g_savedGlobals.sTowingData.iTowingJobsCompleted > 2)
//		CLEAR_HELP()
//		PRINTLN("EXITING EARLY")
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(myVehicle)
		vPlayerPos = GET_ENTITY_COORDS(myVehicle)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
		vBrokenVehiclePos = GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex)
	ENDIF
	
	BOOL bProcede = FALSE
	IF VDIST2(vPlayerPos, vBrokenVehiclePos) < 900 // 30m
		IF NOT IS_TIMER_STARTED(tAdditionalHelpPrintTimer)
			START_TIMER_NOW(tAdditionalHelpPrintTimer)
			PRINTLN("STARTING TIMER - tAdditionalHelpPrintTimer")
		ELSE
			IF GET_TIMER_IN_SECONDS(tAdditionalHelpPrintTimer) > 1
				bProcede = TRUE
			ENDIF
		ENDIF
	ELSE
		IF VDIST2(vPlayerPos, vBrokenVehiclePos) > 2500 //50m
			RESTART_TIMER_NOW(tAdditionalHelpPrintTimer)
//			PRINTLN("WE'RE TOO FAR AWAY FROM BROKEN DOWN CAR, RESTART TIMER")
		ENDIF
	ENDIF
	
	IF bProcede
		SWITCH iAdditionalHelpPrintStages
			CASE 0
				IF NOT IS_HELP_MESSAGE_ON_SCREEN()
					
					PRINT_HELP("TOW_TUT_02") // Align the tow truck so its back is flush with the target vehicle.
					PRINTLN("PRINTING THIRD ADDITIONAL HELP MESSAGE")
				
					RESTART_TIMER_NOW(tAdditionalHelpPrintTimer)
					
					iAdditionalHelpPrintStages = 1
				ENDIF
			BREAK
			//-------------------------------------------------------------------------
			CASE 1
				IF IS_TIMER_STARTED(tAdditionalHelpPrintTimer)
					IF GET_TIMER_IN_SECONDS(tAdditionalHelpPrintTimer) > 14
						IF NOT IS_HELP_MESSAGE_ON_SCREEN()
							PRINT_HELP("TOW_TUT_03", DEFAULT_HELP_TEXT_TIME) // Use the ~INPUT_VEH_MOVE_UD~ to lower and raise the crane.
							PRINTLN("PRINTING SECOND ADDITIONAL HELP MESSAGE")
							
							RESTART_TIMER_NOW(tAdditionalHelpPrintTimer)
							
							iAdditionalHelpPrintStages = 2
						ENDIF
					ENDIF
				ENDIF
			BREAK
			//-------------------------------------------------------------------------
			CASE 2
				IF IS_TIMER_STARTED(tAdditionalHelpPrintTimer)
					IF GET_TIMER_IN_SECONDS(tAdditionalHelpPrintTimer) > 14
						IF NOT IS_HELP_MESSAGE_ON_SCREEN()
							iRand = GET_RANDOM_INT_IN_RANGE() % 2
							IF iRand = 0
								PRINT_HELP("TOW_TUT_04C") // When a vehicle is attached to the tow truck, hold ~INPUT_VEH_HEADLIGHT~ to unhook the vehicle.
								PRINTLN("PRINTING FIRST ADDITIONAL HELP MESSAGE - A")
							ELIF iRand = 1
								PRINT_HELP("TOWT_HELP_CR") // If a vehicle is attached, the crane can only be raised and lowered while the tow truck is stationary.
								PRINTLN("PRINTING FIRST ADDITIONAL HELP MESSAGE - B")
							ENDIF
							
							iAdditionalHelpPrintStages = 3
						ENDIF
					ENDIF
				ENDIF
			BREAK
			//-------------------------------------------------------------------------
			CASE 3
			
			BREAK
			//-------------------------------------------------------------------------
		ENDSWITCH
	ENDIF

ENDPROC

FUNC BOOL HANDLE_TONYA_POST_DROPOFF()
	VECTOR vPlayerPosition, vTonyaPosition
	INT iRandInt
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_ENTITY_DEAD(pedTonya)
		vTonyaPosition = GET_ENTITY_COORDS(pedTonya)
	ELSE
		vTonyaPosition = <<408.5002, -1624.5825, 29.2928>>
	ENDIF
	
	IF VDIST2(vPlayerPosition, vTonyaPosition) > 10000 // 100m
	OR IS_ENTITY_DEAD(pedTonya)
//	OR IS_PED_INJURED(pedTonya)
		STOP_SCRIPTED_CONVERSATION(FALSE)
		PRINTLN("RETURNING TRUE - HANDLE_TONYA_POST_DROPOFF")
		RETURN TRUE
	ENDIF
	
	SWITCH iPostMissionTonyaStages
		CASE 0
			IF NOT IS_ENTITY_DEAD(pedTonya)
				IF GET_SEQUENCE_PROGRESS(pedTonya) = 2
					iRandInt = GET_RANDOM_INT_IN_RANGE() % 2
					
					IF iRandInt = 0
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TONYA_CALL1", CONV_PRIORITY_HIGH)
							PRINTLN("PLAYING CONVO - TONYA_CALL1")
							PRINTLN("iPostMissionTonyaStages = 1")
							iPostMissionTonyaStages = 1
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TONYA_CALL2", CONV_PRIORITY_HIGH)
							PRINTLN("PLAYING CONVO - TONYA_CALL2")
							PRINTLN("iPostMissionTonyaStages = 1")
							iPostMissionTonyaStages = 1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_ENTITY_DEAD(pedTonya)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TASK_START_SCENARIO_AT_POSITION(pedTonya, "WORLD_HUMAN_AA_SMOKE", <<408.5002, -1624.5825, 29.2928>>, 202.6928, -1, TRUE)
					PRINTLN("iPostMissionTonyaStages = 2")
					iPostMissionTonyaStages = 2
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF VDIST2(vPlayerPosition, vTonyaPosition) < 25 //5m
				IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TONYA_MESS", CONV_PRIORITY_HIGH)
					PRINTLN("iPostMissionTonyaStages = 3")
					iPostMissionTonyaStages = 3
				ENDIF
			ENDIF
		BREAK
		CASE 3
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_TONYA_PHONECALL()
//	FLOAT fTemp

	IF NOT IS_TIMER_STARTED(tDelayPhoneCallTimer)
		START_TIMER_NOW(tDelayPhoneCallTimer)
		PRINTLN("tDelayPhoneCallTimer")
	ENDIF
	
//	fTemp = GET_TIMER_IN_SECONDS(tDelayPhoneCallTimer)
//	PRINTLN("TIME = ", fTemp)

	IF IS_TIMER_STARTED(tDelayPhoneCallTimer)
		IF GET_TIMER_IN_SECONDS(tDelayPhoneCallTimer) > 7.0
			IF NOT bPlayedCellPhoneCall
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					ENDIF
					
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "TONYA")
					
					IF g_savedGlobals.sTowingData.iTowingJobsCompleted = 2
						PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_TOW_TONYA, "TOWAUD", "TONYA_WRAP3", CONV_MISSION_CALL)
						PRINTLN("TONYA 3 - PHONE CALL WRAP UP")
					ELIF g_savedGlobals.sTowingData.iTowingJobsCompleted = 3
						PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_TOW_TONYA, "TOWAUD", "TONYA_WRAP4", CONV_MISSION_CALL)
						PRINTLN("TONYA 4 - PHONE CALL WRAP UP")
					ENDIF
					PRINTLN("TONYA - CELL PHONE CALL")
					bPlayedCellPhoneCall = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bPlayedCellPhoneCall AND HAS_CELLPHONE_CALL_FINISHED()
	
		PRINTLN("RETURNING TRUE - TONYA - HAS_ENDING_CELL_PHONE_CALL_COMPLETED")
		RETURN TRUE
	ELSE
		PRINTLN("WAITING FOR CELL PHONE CALL TO FINISH")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANLDE_LEAVING_TOWTRUCK_TO_DROPOFF()
	// Special Case Handling of leaving the tow truck
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
			// If we're out of the car, remove the impound blip
			IF DOES_BLIP_EXIST(blipImpound)
				PRINTLN("REMOVING BLIP - blipImpound")
				REMOVE_BLIP(blipImpound)
			ENDIF
			// Add the blip on the tow truck
			IF NOT DOES_BLIP_EXIST(myVehicleBlip)
				myVehicleBlip = ADD_BLIP_FOR_ENTITY(myVehicle)
				SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
				PRINTLN("ADDING BLIP - myVehicleBlip")
			ENDIF
		ELSE
			// We're back in the tow truck, reblip the impound
			IF NOT DOES_BLIP_EXIST(blipImpound)
				SCRIPT_REMOVE_BLIP_SAFE(blipImpound)
				blipImpound = ADD_BLIP_FOR_COORD(<<406.3804, -1635.8673, 28.2928>>)
				SET_BLIP_ROUTE(blipImpound, TRUE)
				PRINTLN("ADDING BLIP - blipImpound")
			ENDIF
			// We're back in the tow truck, remove the tow truck blip
			IF DOES_BLIP_EXIST(myVehicleBlip)
				PRINTLN("REMOVING BLIP - myVehicleBlip")
				REMOVE_BLIP(myVehicleBlip)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HANDLE_TONYA_DROPOFF()
	
	HANLDE_LEAVING_TOWTRUCK_TO_DROPOFF()

	SWITCH iTonyaDropoffStages
		CASE 0
			vCurDestObjective[0] = <<406.3804, -1635.8673, 28.2928>>
			fObjDist = GET_PLAYER_DISTANCE_FROM_LOCATION(vCurDestObjective[0], FALSE)	
			SCRIPT_REMOVE_BLIP_SAFE(blipImpound)
			blipImpound = ADD_BLIP_FOR_COORD(<<406.3804, -1635.8673, 28.2928>>)
			SET_BLIP_ROUTE(blipImpound, TRUE)
			
			IF NOT IS_TIMER_STARTED(tLastChatTimer)
				START_TIMER_NOW(tLastChatTimer)
				PRINTLN("STARTING TIMER - tLastChatTimer")
			ELSE
				RESTART_TIMER_NOW(tLastChatTimer)
				PRINTLN("RESTARTING TIMER - tLastChatTimer")
			ENDIF
			
			PRINTLN("iTonyaDropoffStages = 1")
			iTonyaDropoffStages = 1
		BREAK
		//--------------------------------------------------------------------------
		CASE 1
			IF NOT bPlayedLastConvo
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
					IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_DROPS", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("PLAYING THIRD TONYA GOODBYE")
						bPlayedLastConvo = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT bPrintDriveToImpoundObjective
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINT_NOW("TOWT_DRIVE", DEFAULT_GOD_TEXT_TIME, 1)
						PRINTLN("bPrintDriveToImpoundObjective = TRUE")
						bPrintDriveToImpoundObjective = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT bPlayedBackToImpountConvo AND bPlayedLastConvo
				IF IS_TIMER_STARTED(tLastChatTimer)
					IF GET_TIMER_IN_SECONDS(tLastChatTimer) > 25.0
						IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_LASTCHAT", CONV_PRIORITY_VERY_HIGH)
							bPlayedBackToImpountConvo = TRUE
							PRINTLN("bPlayedBackToImpountConvo = TRUE")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedTonya) AND NOT IS_ENTITY_DEAD(myVehicle)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle) AND IS_PED_IN_VEHICLE(pedTonya, myVehicle)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<406.3804, -1635.8673, 28.2928>>, <<5,5,LOCATE_SIZE_HEIGHT>>, TRUE)
					
						IF DOES_BLIP_EXIST(blipImpound)
							REMOVE_BLIP(blipImpound)
							PRINTLN("REMOVING BLIP - blipImpound")
						ENDIF
					
						BRING_VEHICLE_TO_HALT(myVehicle, 5.0, 1)
						SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_UNLOCKED)
						
						CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TUT_ENDC", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("PLAYING THIRD TONYA GOODBYE")
						
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK	
		//--------------------------------------------------------------------------
		CASE 2
			IF NOT IS_ENTITY_DEAD(pedTonya) AND NOT IS_ENTITY_DEAD(myVehicle)
				IF NOT IS_PED_IN_VEHICLE(pedTonya, myVehicle)
					PRINTLN("RETURNING TRUE ON HANDLE_TONYA_DROPOFF")
					RETURN TRUE
				ELSE
					PRINTLN("WAITING ON TONYA TO GET OUT")
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------
		CASE 3
		
		BREAK
		//--------------------------------------------------------------------------
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_POST_DROPOFF()

	SWITCH iPostDropOffStages
		CASE 0
			IF NOT IS_ENTITY_DEAD(pedTonya)
				IF IS_PED_IN_GROUP(pedTonya)
					REMOVE_PED_FROM_GROUP(pedTonya)
					PRINTLN("REMOVING TONYA FROM GROUP")
				ENDIF
			ENDIF
			
			IF NOT bPlayedLastConvo
				IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TUT_ENDa", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
					PRINTLN("PLAYING FIRST TONYA GOODBYE - 01")
					bPlayedLastConvo = TRUE
				ENDIF
			ENDIF
			
			IF bPlayedLastConvo
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
							PRINT_NOW("TOW_OBJ_07", DEFAULT_GOD_TEXT_TIME, 1) // Exit the tow truck.
						ENDIF
					ENDIF
				
					PRINTLN("iPostDropOffStages = 1")
					iPostDropOffStages = 1
				ELSE
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
							STOP_SCRIPTED_CONVERSATION(FALSE)
							
							PRINTLN("EARLY EXIT: iPostDropOffStages = 1")
							iPostDropOffStages = 1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------
		CASE 1
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
				
					SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				
					CLEAR_THIS_PRINT("TOW_OBJ_07")
				
					IF NOT IS_ENTITY_DEAD(pedTonya)
					
						IF NOT IS_PED_IN_VEHICLE(pedTonya, myVehicle)
							TASK_ENTER_VEHICLE(pedTonya, myVehicle)
							PRINTLN("TONYA IS OUTSIDE THE TRUCK, TASK HER TO ENTER")
						ELSE
							TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(pedTonya, myVehicle)
							PRINTLN("TONYA IS INSIDE THE TRUCK, TASK HER TO SHUFFLE SEATS")
						ENDIF
						
						PRINTLN("iPostDropOffStages = 2")
						iPostDropOffStages = 2
					ENDIF
				ELSE
					// Handle player not leaving the tow truck.
					IF NOT IS_TIMER_STARTED(tAbandonTimer)
						START_TIMER_NOW(tAbandonTimer)
						PRINTLN("STARTING TIMER - tAbandonTimer")
					ELSE
						IF GET_TIMER_IN_SECONDS(tAbandonTimer) > 30
						AND bGaveWarning
							CLEAR_THIS_PRINT("TOW_OBJ_07")
							
							PRINTLN("bFailedToExit = TRUE")
							bFailedToExit = TRUE
						ELIF GET_TIMER_IN_SECONDS(tAbandonTimer) > 10
							IF NOT bGaveWarning
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TONYA_EXIT", CONV_PRIORITY_HIGH)
									PRINTLN("bGaveWarning = TRUE")
									bGaveWarning = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
//					PRINTLN("FRANKLIN STILL IN TOW TRUCK")
//					PRINT_NOW("TOW_OBJ_07", DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------
		CASE 2
			IF NOT IS_ENTITY_DEAD(pedTonya) AND NOT IS_ENTITY_DEAD(myVehicle)
				IF GET_PED_IN_VEHICLE_SEAT(myVehicle) = pedTonya
					TASK_VEHICLE_DRIVE_WANDER(pedTonya, myVehicle, 15, DRIVINGMODE_AVOIDCARS)
				
					PRINTLN("iPostDropOffStages = 3")
					iPostDropOffStages = 3
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SAFELY_REMOVE_ASSETS()
	VECTOR vPlayerPosition
	INT idx
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
//		PRINTLN("illegalNodes[illegalIdx].vCarPos = ", illegalNodes[illegalIdx].vCarPos)
	ENDIF

	IF chosenNodeType = NODE_TYPE_HANDI
		IF VDIST2(vPlayerPosition, illegalNodes[illegalIdx].vCarPos) > 22500 // 150m
			IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
				IF IS_ENTITY_OCCLUDED(brokenVehicles[0].pedIndex)
					DELETE_PED(brokenVehicles[0].pedIndex)
					PRINTLN("DELETING PED - brokenVehicles[0].pedIndex")
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(brokenVehicles[0].pedIndex)
					PRINTLN("SETTING PED AS NO LONGER NEEDED - brokenVehicles[0].pedIndex")
				ENDIF
			ENDIF
			
			REMOVE_ANIM_DICT("oddjobs@towing")
			REMOVE_ANIM_DICT("oddjobs@towingangryidle_a")
			REMOVE_ANIM_DICT("oddjobs@towingpleadingidle_b")
			REMOVE_ANIM_DICT("oddjobs@towingpleadingidle_a")
			REMOVE_ANIM_DICT("RANDOM@BICYCLE_THIEF@IDLE_A")
			REMOVE_ANIM_DICT("move_m@JOG@")
		ELSE
//			PRINTLN("FAILING DISTANCE CHECK")
		ENDIF
	ELIF chosenNodeType = NODE_TYPE_TRAIN
		IF VDIST2(vPlayerPosition, illegalNodes[illegalIdx].vCarPos) > 22500 // 150m
			IF DOES_ENTITY_EXIST(pedTrainDriver) AND NOT IS_ENTITY_DEAD(pedTrainDriver)
				IF IS_ENTITY_OCCLUDED(pedTrainDriver)
					DELETE_PED(pedTrainDriver)
					PRINTLN("DELETING PED - pedTrainDriver")
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(pedTrainDriver)
					PRINTLN("SETTING PED AS NO LONGER NEEDED - pedTrainDriver")
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(brokenVehicles[0].blockingIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].blockingIndex)
				IF IS_ENTITY_OCCLUDED(brokenVehicles[0].blockingIndex)
					DELETE_MISSION_TRAIN(brokenVehicles[0].blockingIndex)
					PRINTLN("DELETING MISSION TRAIN - brokenVehicles[0].blockingIndex")
				ELSE
					SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(brokenVehicles[0].blockingIndex, TRUE)
					SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(brokenVehicles[0].blockingIndex)
					PRINTLN("SETTING MISSION TRAIN AS NO LONGER NEEDED")
				ENDIF
			ENDIF			
		ENDIF
	ELIF chosenNodeType = NODE_TYPE_ACCIDENT
		REPEAT 2 idx
			// Remove bystanders
			IF DOES_ENTITY_EXIST(brokenVehicles[0].pedBystanders[idx]) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].pedBystanders[idx])
				IF IS_ENTITY_OCCLUDED(brokenVehicles[0].pedBystanders[idx])
					DELETE_PED(brokenVehicles[0].pedBystanders[idx])
					PRINTLN("DELETING PED - brokenVehicles[0].pedBystanders[idx]")
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(brokenVehicles[0].pedBystanders[idx])
					PRINTLN("SETTING PED AS NO LONGER NEEDED - brokenVehicles[0].pedBystanders[idx]")
				ENDIF
			ENDIF
		ENDREPEAT
		
		// Ambulance driver
		IF DOES_ENTITY_EXIST(helperPed)
			SET_PED_AS_NO_LONGER_NEEDED(helperPed)
			PRINTLN("SETTING PED AS NO LONGER NEEDED - helperPed")
		ENDIF
		// Ambulance
		IF DOES_ENTITY_EXIST(helperCar)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(helperCar)
			PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED - helperCar")
		ENDIF
		
		REMOVE_ANIM_DICT("REACTION@MALE_STAND@BIG_VARIATIONS@A")
		REMOVE_ANIM_DICT("move_m@JOG@")
	ENDIF
ENDPROC

PROC HANDLE_TOW_TALK_CONVO()
	VECTOR vPlayerPos
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	// Special Case for Tonya 5
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya5")) = 1)
		IF NOT bPlayedPostHookupConvo
			IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_REPAIR", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("bPlayedPostHookupConvo = TRUE")
				bPlayedPostHookupConvo = TRUE
			ENDIF
		ELSE
			IF bPlayedPostHookupConvo
				IF NOT bPrintGoToRepairShop
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINT_NOW("TOWT_OBJ_04", DEFAULT_GOD_TEXT_TIME, 1)
						PRINTLN("bPrintGoToRepairShop = TRUE")
						bPrintGoToRepairShop = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF VDIST2(vBDVPos, vPlayerPos) > 10000 //100m
		SWITCH g_savedGlobals.sTowingData.iTowingJobsCompleted
			CASE 0
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedTonya) AND NOT IS_ENTITY_DEAD(myVehicle)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle) AND IS_PED_IN_VEHICLE(pedTonya, myVehicle)
	
						IF NOT bPlayedWayBackConvo
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_TALK", CONV_PRIORITY_HIGH)
									bPlayedWayBackConvo = TRUE
									PRINTLN("TONYA 1: PLAYING WAY BACK CONVO")
								ENDIF
							ENDIF
						ELSE
							IF bInterrupt
								IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, "TOWAUD", "TOW_TALK", TestResumptionLabel, CONV_PRIORITY_HIGH)
									PRINTLN("TRYING TO RESUME INTERRUPTED LINE")
									bInterrupt = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedTonya) AND NOT IS_ENTITY_DEAD(myVehicle)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle) AND IS_PED_IN_VEHICLE(pedTonya, myVehicle)
			
						IF NOT bPlayedWayBackConvo
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_TALK2", CONV_PRIORITY_HIGH)
									bPlayedWayBackConvo = TRUE
									PRINTLN("TONYA 2: PLAYING WAY BACK CONVO")
								ENDIF
							ENDIF
						ELSE
							IF bInterrupt
								IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, "TOWAUD", "TOW_TALK2", TestResumptionLabel, CONV_PRIORITY_HIGH)
									PRINTLN("TRYING TO RESUME INTERRUPTED LINE")
									bInterrupt = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedTonya) AND NOT IS_ENTITY_DEAD(myVehicle)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle) AND IS_PED_IN_VEHICLE(pedTonya, myVehicle)
						IF NOT bPlayedWayBackConvo
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_CHAT2", CONV_PRIORITY_HIGH)
									bPlayedWayBackConvo = TRUE
									PRINTLN("TONYA 5: PLAYING WAY BACK CONVO")
								ENDIF
							ENDIF
						ELSE
							IF bInterrupt
								IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, "TOWAUD", "TOW_CHAT2", TestResumptionLabel, CONV_PRIORITY_HIGH)
									PRINTLN("TRYING TO RESUME INTERRUPTED LINE")
									bInterrupt = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC HANDLE_NEAR_ENDING_CONVO()
	IF NOT bPlayedNearEndingConvo
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<399.885345,-1649.834595,27.294472>>, <<432.558990,-1612.315796,35.341759>>, 35.000000)
					IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_DROP", CONV_PRIORITY_HIGH)
						PRINTLN("bPlayedNearEndingConvo = TRUE")
						bPlayedNearEndingConvo = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya5")) = 1)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-233.339386,-1183.597900,21.073008>>, <<-232.138214,-1141.946289,29.081360>>, 29.250000)
					IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_SHOP", CONV_PRIORITY_HIGH)
						PRINTLN("bPlayedNearEndingConvo = TRUE")
						bPlayedNearEndingConvo = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_NEW_LOADING_VEHICLES()
	// OVERRIDES
	IF chosenNodeType = NODE_TYPE_ABANDON
		REQUEST_MODEL(PRIMO)
		PRINTLN("REQUESTING MODEL - PRIMO")
	ELIF chosenNodeType = NODE_TYPE_HANDI
		REQUEST_MODEL(landstalker)
		PRINTLN("REQUESTING MODEL - landstalker")
		REQUEST_MODEL(A_M_M_GENFAT_01)
		PRINTLN("REQUESTING MODEL - A_M_M_GENFAT_01")
	ELIF chosenNodeType = NODE_TYPE_TRAIN
		REQUEST_MODEL(schwarzer)
		PRINTLN("REQUESTING MODEL - schwarzer")
		REQUEST_MODEL(A_M_M_BEVHILLS_02)
		PRINTLN("REQUESTING MODEL - A_M_M_BEVHILLS_02")
	ELIF chosenNodeType = NODE_TYPE_DYNAMIC
		REQUEST_MODEL(tailgater)
		PRINTLN("REQUESTING MODEL - tailgater")
		REQUEST_MODEL(A_M_M_TOURIST_01)
		PRINTLN("REQUESTING MODEL - A_M_M_TOURIST_01")
	ELIF chosenNodeType = NODE_TYPE_ACCIDENT
		REQUEST_MODEL(INTRUDER)
		PRINTLN("REQUESTING MODEL - INTRUDER")
		REQUEST_MODEL(S_M_M_PARAMEDIC_01)
		PRINTLN("REQUESTING MODEL - S_M_M_PARAMEDIC_01")
		REQUEST_MODEL(A_M_Y_GENSTREET_02)
		PRINTLN("REQUESTING MODEL - A_M_Y_GENSTREET_02")
		REQUEST_MODEL(A_M_M_BEVHILLS_02)
		PRINTLN("REQUESTING MODEL - A_M_M_BEVHILLS_02")
	ENDIF
	
	// LOADING
	IF chosenNodeType = NODE_TYPE_ABANDON
		WHILE NOT HAS_MODEL_LOADED(PRIMO)
			PRINTLN("WAITING ON MODEL TO LOAD - PRIMO")
			WAIT(0)
		ENDWHILE
	ELIF chosenNodeType = NODE_TYPE_HANDI
		WHILE NOT HAS_MODEL_LOADED(landstalker)
		OR NOT HAS_MODEL_LOADED(A_M_M_GENFAT_01)
			PRINTLN("WAITING ON MODEL TO LOAD - landstalker, A_M_M_GENFAT_01")
			WAIT(0)
		ENDWHILE
	ELIF chosenNodeType = NODE_TYPE_TRAIN
		WHILE NOT HAS_MODEL_LOADED(schwarzer)
		OR NOT HAS_MODEL_LOADED(A_M_M_BEVHILLS_02)
			PRINTLN("WAITING ON MODEL TO LOAD - schwarzer AND A_M_M_BEVHILLS_02")
			WAIT(0)
		ENDWHILE
	ELIF chosenNodeType = NODE_TYPE_DYNAMIC
		WHILE NOT HAS_MODEL_LOADED(tailgater)
		OR NOT HAS_MODEL_LOADED(A_M_M_TOURIST_01)
			PRINTLN("WAITING ON MODEL TO LOAD - tailgater AND A_M_M_TOURIST_01")
			WAIT(0)
		ENDWHILE
	ELIF chosenNodeType = NODE_TYPE_ACCIDENT
		WHILE NOT HAS_MODEL_LOADED(INTRUDER)
		OR NOT HAS_MODEL_LOADED(S_M_M_PARAMEDIC_01)
		OR NOT HAS_MODEL_LOADED(A_M_Y_GENSTREET_02)
		OR NOT HAS_MODEL_LOADED(A_M_M_BEVHILLS_02)
			PRINTLN("WAITING ON MODEL TO LOAD - INTRUDER")
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

// initialise mission
FUNC BOOL INIT_MISSION(TOWING_LAUNCH_DATA& launchArgs, LoadQueueLarge &sLoadQueue)
	
	PRINTLN("INSIDE INIT, TIME: ", TIMERA())
	
	SET_AGGRO_FIELDS_TO_NOT_CHECK()
	
	//INIT_ZVOLUME_WIDGETS()
	
	HANDLE_NEW_LOADING_VEHICLES()
	
	//only for ones with a ped
	IF launchArgs.nodeType = NODE_TYPE_DYNAMIC
		SpecialAnims[0] = "base"
		ADD_STREAMED_ANIM(streamReqsAnim, "amb@world_human_smoking@male@male_a@base")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towingcome_here")
		ADD_STREAMED_ANIM(streamReqsAnim, "move_m@JOG@")
//		PRINTLN("ADDING JOGGER ANIMATION")
	ELIF (launchArgs.nodeType = NODE_TYPE_HANDI OR launchArgs.nodeType = NODE_TYPE_ABANDON) AND NOT bDoTutorial
		ADD_STREAMED_ANIM(streamReqsAnim, "Ped")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towing")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towingangryidle_a")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towingpleadingidle_b")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towingpleadingidle_a")
		ADD_STREAMED_ANIM(streamReqsAnim, "RANDOM@BICYCLE_THIEF@IDLE_A")
		ADD_STREAMED_ANIM(streamReqsAnim, "move_m@JOG@")
	ENDIF
	
	IF launchArgs.nodeType = NODE_TYPE_ACCIDENT
		//We need to stream an ambulance
		SpecialAnimDict = "REACTION@MALE_STAND@BIG_VARIATIONS@A"
		ADD_STREAMED_ANIM(streamReqsAnim, SpecialAnimDict)
		ADD_STREAMED_MODEL(streamReqs, AMBULANCE)
		ADD_STREAMED_MODEL(streamReqs, S_M_M_PARAMEDIC_01)
		PRINTLN("REQUESTING MODEL - S_M_M_PARAMEDIC_01")
		ADD_STREAMED_ANIM(streamReqsAnim, "move_m@JOG@")
		SpecialAnims[0] = "react_big_variations_a"
		SpecialAnims[1] = "react_big_variations_b"
		SpecialAnims[2] = "react_big_variations_c"
	ENDIF
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, TRUE)
	
	IF launchArgs.nodeType = NODE_TYPE_TRAIN
		REQUEST_PTFX_ASSET()
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towing")
		ADD_STREAMED_ANIM(streamReqsAnim, "amb@world_human_smoking@male@male_a@base")
		ADD_STREAMED_ANIM(streamReqsAnim, "oddjobs@towingpleadingidle_b")
		ADD_STREAMED_ANIM(streamReqsAnim, "move_m@JOG@")
		ADD_STREAMED_MODEL(streamReqs, FREIGHTCONT1)
		ADD_STREAMED_MODEL(streamReqs, TANKERCAR)
		ADD_STREAMED_MODEL(streamReqs, FREIGHT)
		ADD_STREAMED_MODEL(streamReqs, FREIGHTCAR)
		ADD_STREAMED_MODEL(streamReqs, S_M_M_LSMETRO_01)
	ENDIF
	
	gbCarList = gbCarList
	gbPedList = gbPedList
	
	ADD_STREAMED_MODEL(streamReqs, missionCarName)
	
	LOAD_QUEUE_LARGE_ADD_ADDITIONAL_TEXT(sLoadQueue, "TOW", ODDJOB_TEXT_SLOT) // load text for the mission
	LOAD_QUEUE_LARGE_ADD_ADDITIONAL_TEXT(sLoadQueue, "DTRSHRD", MINIGAME_TEXT_SLOT) // load text for the mission
	
	//6
	REQUEST_ALL_MODELS(streamReqs)
	REQUEST_ALL_ANIMS(streamReqsAnim, sLoadQueue)
	
	IF launchArgs.nodeType = NODE_TYPE_HANDI
		WHILE NOT HAS_MODEL_LOADED(A_M_M_GENFAT_01)
			DEBUG_MESSAGE("Waiting for A_M_M_GENFAT_01 to stream")
			UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
			WAIT(0)
		ENDWHILE
	ENDIF
		
	WHILE NOT ARE_ANIMS_STREAMED(sLoadQueue)
		DEBUG_MESSAGE("waiting for anims")
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		WAIT(0)
	ENDWHILE
	
	WHILE NOT ARE_MODELS_STREAMED(streamReqs)
		DEBUG_MESSAGE("Waiting for models to stream")
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		WAIT(0)
	ENDWHILE
	
	IF launchArgs.nodeType = NODE_TYPE_TRAIN
		WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\TOWING_TRAIN")
		OR NOT HAS_PTFX_ASSET_LOADED()
			UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
			DEBUG_MESSAGE("waiting for audio / effects")
			WAIT(0)
		ENDWHILE
	ENDIF

	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
		DEBUG_MESSAGE("trying to load string table")
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		WAIT(0)
	ENDWHILE
	
	RETURN TRUE
ENDFUNC

PROC HANDLE_PLAYER_STALL()
	FLOAT fTowTruckSpeed

	IF NOT IS_ENTITY_DEAD(myVehicle)
		fTowTruckSpeed = GET_ENTITY_SPEED(myVehicle)
	ENDIF
	
	IF fTowTruckSpeed < 1.0
	AND (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle, TRUE))
		IF NOT IS_TIMER_STARTED(tStopTimer)
			START_TIMER_NOW(tStopTimer)
			PRINTLN("STARTING TIMER - tStopTimer")
		ELSE
			IF NOT bPlayStopConvo
				IF GET_TIMER_IN_SECONDS(tStopTimer) > 30.0
					IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya3")) = 1)
					OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
						IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya3")) = 1)
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) AND IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, myVehicle, TRUE)
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_DAWDLE", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("bPlayStopConvo = TRUE")
									bPlayStopConvo = TRUE
								ENDIF
							ELSE
								IF IS_TIMER_STARTED(tStopTimer)
									RESTART_TIMER_NOW(tStopTimer)
								ENDIF
							ENDIF
						ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) AND IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, myVehicle, TRUE)
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_DAWDLE2", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("bPlayStopConvo = TRUE")
									bPlayStopConvo = TRUE
								ENDIF
							ELSE
								IF IS_TIMER_STARTED(tStopTimer)
									RESTART_TIMER_NOW(tStopTimer)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ENTITY_DEAD(pedTonya) AND IS_PED_IN_VEHICLE(pedTonya, myVehicle, TRUE)
							IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TONYA_STOP", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								PRINTLN("bPlayStopConvo = TRUE")
								bPlayStopConvo = TRUE
							ENDIF
						ELSE
							IF IS_TIMER_STARTED(tStopTimer)
								RESTART_TIMER_NOW(tStopTimer)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(tStopTimer)
			RESTART_TIMER_NOW(tStopTimer)
		ENDIF
	ENDIF

ENDPROC

PROC HANDLE_CAR_DAMAGE_DIALOGUE()
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya3")) = 1)
	OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedTonya) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(brokenVehicles[0].vehicleIndex, PLAYER_PED_ID())
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT bPlayedDamagedVehicleLine
					IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TONYA_CARDAM", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("bPlayedDamagedVehicleLine = TRUE")
						bPlayedDamagedVehicleLine = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HANDLE_PLAYER_BEING_AWAY_FROM_IMPOUND()
	PRINTLN("HANDLE_PLAYER_BEING_AWAY_FROM_IMPOUND")
	
	SWITCH iAwayFromImpoundStages
		CASE 0
			CLEAR_HELP()
		
			IF NOT DOES_BLIP_EXIST(blipImpound)
				SCRIPT_REMOVE_BLIP_SAFE(blipImpound)
				blipImpound = ADD_BLIP_FOR_COORD(<<406.3804, -1635.8673, 28.2928>>)
				SET_BLIP_ROUTE(blipImpound, TRUE)
			ENDIF
			
			PRINT_NOW("TOWT_DRIVE", DEFAULT_GOD_TEXT_TIME, 1)
			
			PRINTLN("iAwayFromImpoundStages = 1")
			iAwayFromImpoundStages = 1
		BREAK
		CASE 1	
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedTonya) AND NOT IS_ENTITY_DEAD(myVehicle)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle) AND IS_PED_IN_VEHICLE(pedTonya, myVehicle)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<398.966614,-1641.232666,27.292774>>, <<411.138153,-1626.688721,33.292782>>, 15.000000)
					
						IF DOES_BLIP_EXIST(blipImpound)
							REMOVE_BLIP(blipImpound)
							PRINTLN("REMOVING BLIP - blipImpound")
						ENDIF
					
						BRING_VEHICLE_TO_HALT(myVehicle, 5.0, 1)
						SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_UNLOCKED)
						
						PRINTLN("iAwayFromImpoundStages = 2")
						iAwayFromImpoundStages = 2
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			PRINTLN("RETURNING TRUE - HANDLE_PLAYER_BEING_AWAY_FROM_IMPOUND")
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_BLIP_SWITCH()
	VECTOR vPlayerPos, vVehiclePos

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
		vVehiclePos = GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex)
	ENDIF
	
	IF VDIST2(vPlayerPos, vVehiclePos) < 100 // 10m
		IF NOT bRemovedCoordinateBlip
			IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
				REMOVE_BLIP(brokenVehicles[0].blipIndex)
				PRINTLN("bRemovedCoordinateBlip = TRUE")
				bRemovedCoordinateBlip = TRUE
			ENDIF
		ENDIF
			
		IF NOT DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
			IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
				brokenVehicles[0].blipIndex = ADD_BLIP_FOR_ENTITY(brokenVehicles[0].vehicleIndex)
				SET_BLIP_COLOUR(brokenVehicles[0].blipIndex, BLIP_COLOUR_BLUE)
				SET_BLIP_ROUTE(brokenVehicles[0].blipIndex, TRUE)
				PRINTLN("CREATING TOWING BLIP - 01")
			ELSE
				PRINTLN("ENTITY DOES NOT EXIST")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_TONYA_DIALOGUE_HANDI() 
	
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
		IF NOT IS_ENTITY_DEAD(pedTonya) 
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) 
		AND NOT IS_ENTITY_DEAD(myVehicle) 
		AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle) AND IS_PED_IN_VEHICLE(pedTonya, myVehicle) 
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
						IF NOT bPlayHookedUpAudio
							IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_CANGO", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								PRINTLN("bPlayHookedUpAudio = TRUE")
								bPlayHookedUpAudio = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_PLAYERS_LAST_DRIVEN_VEHICLE()
	VECTOR vPlayerPosition, vLastVehiclePosition

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF

	IF DOES_ENTITY_EXIST(vehReplayVehicle) AND NOT IS_ENTITY_DEAD(vehReplayVehicle)
		vLastVehiclePosition = GET_ENTITY_COORDS(vehReplayVehicle)
		
		IF NOT bMovedVehicle
			IF VDIST2(vPlayerPosition, vLastVehiclePosition) > 40000
				IF NOT IS_ENTITY_ON_SCREEN(vehReplayVehicle)
				AND NOT IS_BIG_VEHICLE(vehReplayVehicle) // To Fix Bug # 1975611
				AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehReplayVehicle))
				AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehReplayVehicle))
				AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehReplayVehicle))
				
					IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya1")) = 1)
						SET_ENTITY_COORDS(vehReplayVehicle, <<417.5827, -1645.5754, 28.2928>>)
						SET_ENTITY_HEADING(vehReplayVehicle, 230.2828)
						PRINTLN("TONYA 1: MOVING VEHICLE TO LOCATION: ", <<417.5827, -1645.5754, 28.2928>>)
						SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(vehReplayVehicle, FALSE)
						SET_ENTITY_LOAD_COLLISION_FLAG(vehReplayVehicle, TRUE)
						SET_MISSION_VEHICLE_GEN_VEHICLE(vehReplayVehicle, <<417.5827, -1645.5754, 28.2928>>, 230.2828) // Impound Lot
					ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
						SET_ENTITY_COORDS(vehReplayVehicle, <<417.5827, -1645.5754, 28.2928>>)
						SET_ENTITY_HEADING(vehReplayVehicle, 230.2828)
						PRINTLN("TONYA 2: MOVING VEHICLE TO LOCATION: ", <<417.5827, -1645.5754, 28.2928>>)
						SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(vehReplayVehicle, FALSE)
						SET_ENTITY_LOAD_COLLISION_FLAG(vehReplayVehicle, TRUE)
						SET_MISSION_VEHICLE_GEN_VEHICLE(vehReplayVehicle, <<417.5827, -1645.5754, 28.2928>>, 230.2828) // Impound Lot
					ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya3")) = 1)
						SET_ENTITY_COORDS(vehReplayVehicle, <<-214.5613, -1397.9072, 30.2688>>)
						SET_ENTITY_HEADING(vehReplayVehicle, 176.0831)
						PRINTLN("TONYA 3: MOVING VEHICLE TO LOCATION: ", <<-214.5613, -1397.9072, 30.2688>>)
						SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(vehReplayVehicle, FALSE)
						SET_ENTITY_LOAD_COLLISION_FLAG(vehReplayVehicle, TRUE)
						SET_MISSION_VEHICLE_GEN_VEHICLE(vehReplayVehicle, <<-214.5613, -1397.9072, 30.2688>>, 176.0831) // Yellow Repair Shop
					ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
						SET_ENTITY_COORDS(vehReplayVehicle, <<537.5104, -168.8889, 53.5066>>)
						SET_ENTITY_HEADING(vehReplayVehicle, 358.4492)
						PRINTLN("TONYA 3: MOVING VEHICLE TO LOCATION: ", <<537.5104, -168.8889, 53.5066>>)
						SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(vehReplayVehicle, FALSE)
						SET_ENTITY_LOAD_COLLISION_FLAG(vehReplayVehicle, TRUE)
						SET_MISSION_VEHICLE_GEN_VEHICLE(vehReplayVehicle, <<537.5104, -168.8889, 53.5066>>, 358.4492) // North Repair Shop
					ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya5")) = 1)
						SET_ENTITY_COORDS(vehReplayVehicle, <<417.5827, -1645.5754, 28.2928>>)
						SET_ENTITY_HEADING(vehReplayVehicle, 230.2828)
						PRINTLN("TONYA 5: MOVING VEHICLE TO LOCATION: ", <<417.5827, -1645.5754, 28.2928>>)
						SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(vehReplayVehicle, FALSE)
						SET_ENTITY_LOAD_COLLISION_FLAG(vehReplayVehicle, TRUE)
						SET_MISSION_VEHICLE_GEN_VEHICLE(vehReplayVehicle, <<417.5827, -1645.5754, 28.2928>>, 230.2828) // Impound Lot
					ENDIF
					
					bMovedVehicle = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DISABLE_ALL_CONTROLS_EXCEPT_EXIT()
	PRINTLN("DISABLE CONTROLS ON TOW TRUCK")

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
ENDPROC

PROC ENABLE_CONTROLS_ON_TOWTRUCK()
	PRINTLN("ENABLE CONTROLS ON TOW TRUCK")

	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
ENDPROC

PROC HANDLE_TRAIN_REMOVAL()
	SET_RANDOM_TRAINS(FALSE)
	
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_MAIN, FALSE)
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_QUARRY, FALSE)
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_CAR_STEAL_SETPIECE, FALSE)
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_METRO, FALSE)
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_FIRST_HOOD_SETPIECE, FALSE)
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_SECOND_HOOD_SETPIECE, FALSE)
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_SECOND_FAME_OR_SHAME_SETPIECE, FALSE)
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_DOWNTOWN_SETPIECE, FALSE)
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_THIRD_HOOD_SETPIECE, FALSE)
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_PROLOGUE, FALSE)
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_JEWEL_HEIST_SETPIECE, FALSE)
	SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_PROLOGUE_2, FALSE)
	
	CPRINTLN(DEBUG_MISSION, "DISABLING TRAINS")
ENDPROC

//Core logic for the traditional towing modes of broken down and illegally parked
FUNC BOOL UPDATE_TRADITIONAL(TOWING_LAUNCH_DATA launchArgs, LoadQueueLarge &sLoadQueue)
	INT openIdx
	STRING objective
	VECTOR vAngledA, vAngledB, vPlayerPosition
	FLOAT fWidth, fDistanceToSite
	
	IF curStage > MISSION_STATE_CONVERSATION
		IF bStartTimer
			HANDLE_TIMER()
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableVehicleDamageReactions , TRUE)
	ENDIF
	
	SWITCH curStage
		CASE MISSION_STATE_INTRO
			DEBUG_MESSAGE("MISSION_STATE_INTRO")
			
			SET_VEH_RADIO_STATION(myVehicle, "RADIO_03_HIPHOP_NEW")
			PRINTLN("SETTING STATION - RADIO_03_HIPHOP_NEW")
			
			IF NOT IS_ENTITY_DEAD(myVehicle)
				SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_LOCKED_NO_PASSENGERS)
				PRINTLN("LOCKING PASSENGER DOORS")
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
				IF IS_PED_IN_GROUP(pedTonya)
					REMOVE_PED_FROM_GROUP(pedTonya)
					PRINTLN("REMOVING TONYA FROM PLAYER GROUP - TOWING.SCH")
				ENDIF
			ENDIF
			
			IF chosenNodeType = NODE_TYPE_TRAIN
				HANDLE_TRAIN_REMOVAL()
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, pedTonya, "TONYA")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "towing_tonya_franklin_towing", TRUE)
//			OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vehReplayVehicle)
			
			IF chosenNodeType = NODE_TYPE_DYNAMIC
				//we need to get starting data
				IF GET_POINT(getPointVec, getPointHead, vExtra, vExtra2)
				
					IF NOT IS_TIMER_STARTED(tConvoBufferTimer)
						START_TIMER_NOW(tConvoBufferTimer)
						PRINTLN("STARTING TIMER - tConvoBufferTimer")
					ELSE
						RESTART_TIMER_NOW(tConvoBufferTimer)
						PRINTLN("RESTARTING TIMER - tConvoBufferTimer")
					ENDIF
				
					curStage = MISSION_STATE_CONVERSATION
					DEBUG_MESSAGE("Calling GET_POINT")
				ENDIF
			ELSE
				IF NOT IS_TIMER_STARTED(tConvoBufferTimer)
					START_TIMER_NOW(tConvoBufferTimer)
					PRINTLN("STARTING TIMER - tConvoBufferTimer")
				ELSE
					RESTART_TIMER_NOW(tConvoBufferTimer)
					PRINTLN("RESTARTING TIMER - tConvoBufferTimer")
				ENDIF
			
				//we already have our data continue
				curStage = MISSION_STATE_CONVERSATION
			ENDIF
			
		BREAK
		CASE MISSION_STATE_CONVERSATION
			DEBUG_MESSAGE("MISSION_STATE_CONVERSATION")
			
			IF chosenNodeType = NODE_TYPE_DYNAMIC
				illegalNodes[illegalIdx].vCarPos = <<-476.1537, 132.6556, 62.9586>>
				PRINTLN("OVERRIDING CAR POSITION FOR TONYA 4")
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
				IF NOT bWanted
					brokenVehicles[0].blipIndex = ADD_BLIP_FOR_COORD(illegalNodes[illegalIdx].vCarPos)
					SET_BLIP_COLOUR(brokenVehicles[0].blipIndex, BLIP_COLOUR_BLUE)
					SET_BLIP_ROUTE(brokenVehicles[0].blipIndex, TRUE)
					SET_BLIP_NAME_FROM_TEXT_FILE(brokenVehicles[0].blipIndex, "TOW_BLIP_VEH")
					PRINTLN("CREATING TOWING BLIP - 02")
				ELSE
					PRINTLN("PLAYER WANTED, NOT ADDING OBJECTIVE BLIP YET")
				ENDIF
				
				vCurDestObjective[0] = illegalNodes[illegalIdx].vCarPos
				PRINTLN("SETTING vCurDestObjective[0] = ", illegalNodes[illegalIdx].vCarPos)
			ENDIF
			
			IF IS_TIMER_STARTED(tConvoBufferTimer)
				IF GET_TIMER_IN_SECONDS(tConvoBufferTimer) > fConvoBufferTime
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						SWITCH chosenNodeType
							CASE NODE_TYPE_DYNAMIC
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "TOWDISPATCH")
								
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_ACC_EXP", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("DYNAMIC: GOING TO STATE - MISSION_STATE_CHECK_DISTANCE VIA MISSION_STATE_CONVERSATION")
									curStage = MISSION_STATE_CHECK_DISTANCE
								ENDIF
							BREAK
							CASE NODE_TYPE_ABANDON
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, pedTonya, "TONYA")
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
								
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_ABN_EXP", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("ABANDON: GOING TO STATE - MISSION_STATE_CHECK_DISTANCE VIA MISSION_STATE_CONVERSATION")
									curStage = MISSION_STATE_CHECK_DISTANCE
								ENDIF
							BREAK
							CASE NODE_TYPE_HANDI
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
								
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_HAN_EXP", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("HANDI: GOING TO STATE - MISSION_STATE_CHECK_DISTANCE VIA MISSION_STATE_CONVERSATION")
									curStage = MISSION_STATE_CHECK_DISTANCE
								ENDIF
							BREAK
							CASE NODE_TYPE_TRAIN
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "TOWDISPATCH")
								
								IF illegalIdx = 28
									IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_TRN_EXP2", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("TRAIN: GOING TO STATE - MISSION_STATE_CHECK_DISTANCE VIA MISSION_STATE_CONVERSATION, OVERRIDE USED")
										curStage = MISSION_STATE_CHECK_DISTANCE
									ENDIF
								ELSE
									IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_TRN_EXP", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("TRAIN: GOING TO STATE - MISSION_STATE_CHECK_DISTANCE VIA MISSION_STATE_CONVERSATION")
										curStage = MISSION_STATE_CHECK_DISTANCE
									ENDIF
								ENDIF
							BREAK
							CASE NODE_TYPE_ACCIDENT
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
								
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_ACD_EXP", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("ACCIDENT: GOING TO STATE - MISSION_STATE_CHECK_DISTANCE VIA MISSION_STATE_CONVERSATION")
									curStage = MISSION_STATE_CHECK_DISTANCE
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF	
			ENDIF
		BREAK
		CASE MISSION_STATE_CHECK_DISTANCE
			
			IF NOT bPrintObjective
			
				IF chosenNodeType = NODE_TYPE_ABANDON
					sMyObjective = "TOWT_OBJ_AB"
				ELIF chosenNodeType = NODE_TYPE_HANDI
					sMyObjective = "TOWT_OBJ_01B"
				ELIF chosenNodeType = NODE_TYPE_TRAIN
					sMyObjective = "TOWT_OBJ_TR"
				ELIF chosenNodeType = NODE_TYPE_DYNAMIC
					sMyObjective = "TOWT_OBJ_01"
				ELIF chosenNodeType = NODE_TYPE_ACCIDENT
					sMyObjective = "TOWT_OBJ_CC"
				ENDIF
				sLastObjective = sMyObjective
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT bWanted
					PRINT_NOW(sMyObjective, DEFAULT_GOD_TEXT_TIME, 1)
					PRINTLN("PRINTING OBJECTIVE - sMyObjective")
					bPrintObjective = TRUE
				ENDIF
			ENDIF
			
			IF NOT bStartTimer
				IF bPrintObjective
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						START_TIMER_NOW(failTimer)
						PRINTLN("bStartTimer = TRUE")
						bStartTimer = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			ENDIF
			
			fDistanceToSite = VDIST(vPlayerPosition, illegalNodes[illegalIdx].vCarPos)
//			PRINTLN("fDistanceToSite = ", fDistanceToSite)

			CHECK_FOR_HOOKING_WRONG_CAR()
			
			HANDLE_PLAYERS_LAST_DRIVEN_VEHICLE()
			
			HANDLE_PLAYER_STALL()
			
			IF fDistanceToSite < 250
				IF INIT_MISSION(launchArgs, sLoadQueue)
					PRINTLN("ACCIDENT: GOING TO STATE - MISSION_STATE_CREATE VIA MISSION_STATE_INIT")
					curStage = MISSION_STATE_CREATE
				ENDIF
			ENDIF
		BREAK
		CASE MISSION_STATE_CREATE
			DEBUG_MESSAGE("MISSION_STATE_CREATE")
			//SETUP_MINIGAME_INSTRUCTIONS(helpScaleform, TRUE, ICON_BUTTON_X,"TOW_CTRL_01", ICON_BUTTON_A,"TOW_CTRL_04", ICON_LSTICK_UPDOWN,"TOW_CTRL_02") //ICON_BUTTON_RB,"TOW_CTRL_03",
			SWITCH chosenNodeType
				CASE NODE_TYPE_DYNAMIC
					DEBUG_MESSAGE("NODE_TYPE_DYNAMIC")
					IF HANDLE_CREATE_STATE("TOWT_OBJ_01", "TOWT_HELP_01", openIdx, TRUE)
						IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
							SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_CANNOT_ENTER)
						ENDIF
						FIND_CLOSE_GARAGE()
						PRINTLN("GOING TO STATE - MISSION_STATE_PRINTS - NODE_TYPE_DYNAMIC")
						curStage = MISSION_STATE_PRINTS
					ENDIF
				BREAK
				CASE NODE_TYPE_ABANDON
					DEBUG_MESSAGE("NODE_TYPE_ABANDON")
					IF HANDLE_CREATE_STATE("TOWT_OBJ_AB", "TOWT_HELP_AB", openIdx, FALSE)	
						vMin = <<vCurDestObjective[0].x+15.0, vCurDestObjective[0].y+15.0, vCurDestObjective[0].z+15.0>>
						vMax = <<vCurDestObjective[0].x-15.0, vCurDestObjective[0].y-15.0, vCurDestObjective[0].z-15.0>>
//						SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMin, vMax, FALSE)
						bGenDisabled = TRUE
						
						SET_VEHICLE_UNDRIVEABLE(brokenVehicles[0].vehicleIndex, TRUE)
						PRINTLN("SETING VEHICLE AS UNDRIVEABLE - ABANDONDED")
						
						PRINTLN("GOING TO STATE - MISSION_STATE_PRINTS - NODE_TYPE_ABANDON")
						curStage = MISSION_STATE_PRINTS
					ENDIF
				BREAK
				CASE NODE_TYPE_HANDI
					DEBUG_MESSAGE("NODE_TYPE_HANDI")
					IF HANDLE_CREATE_STATE("TOWT_OBJ_01B", "TOWT_HELP_03", openIdx, FALSE)
						SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_LOCKED)
						PRINTLN("LOCKING DOORS ON SUV IN TONYA 2")
						PRINTLN("GOING TO STATE - MISSION_STATE_PRINTS - NODE_TYPE_HANDI")
						curStage = MISSION_STATE_PRINTS	
					ENDIF
				BREAK
				CASE NODE_TYPE_TRAIN
					DEBUG_MESSAGE("NODE_TYPE_TRAIN")
					IF HANDLE_CREATE_STATE("TOWT_OBJ_TR", "TOWT_HELP_TR", openIdx, FALSE, TRUE)
					
						SET_VEHICLE_UNDRIVEABLE(brokenVehicles[0].vehicleIndex, TRUE)
						SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
						PRINTLN("SETING VEHICLE AS UNDRIVEABLE - TRAIN")
						
						FIND_CLOSE_GARAGE()
						PRINTLN("GOING TO STATE - MISSION_STATE_PRINTS - NODE_TYPE_TRAIN")
						curStage = MISSION_STATE_PRINTS
					ENDIF
				BREAK
				CASE NODE_TYPE_ACCIDENT
					DEBUG_MESSAGE("NODE_TYPE_ACCIDENT")
					IF HANDLE_CREATE_STATE("TOWT_OBJ_CC", "TOWT_HELP_04", openIdx, FALSE, FALSE)
						
						SET_VEHICLE_UNDRIVEABLE(brokenVehicles[0].vehicleIndex, TRUE)
						PRINTLN("SETING VEHICLE AS UNDRIVEABLE - ACCIDENT")
						
						FIND_CLOSE_GARAGE()
						
						PRINTLN("GOING TO STATE - MISSION_STATE_PRINTS - NODE_TYPE_ACCIDENT")
						curStage = MISSION_STATE_PRINTS	
					ENDIF
				BREAK
				DEFAULT
					DEBUG_MESSAGE("NO node type selected")
				BREAK	
			ENDSWITCH
		BREAK
		CASE MISSION_STATE_PED
//			DEBUG_MESSAGE("MISSION_STATE_PED")
			
			//TODO this should be pulled out
			IF UPDATE_BROKEN_VEHICLE_PED(brokenState, 0, "TOW_BREAK1")
				DEBUG_MESSAGE("GOING TO STATE - MISSION_STATE_TOWING - VIA MISSION_STATE_PED")
				curStage = MISSION_STATE_TOWING
			ENDIF
		BREAK
		CASE MISSION_STATE_PRINTS
//			DEBUG_MESSAGE("MISSION_STATE_PRINTS")
		
			IF chosenNodeType = NODE_TYPE_DYNAMIC
				DEBUG_MESSAGE("GOING TO STATE - MISSION_STATE_PED - VIA MISSION_STATE_PRINTS")
				curStage = MISSION_STATE_PED
			ELSE
				DEBUG_MESSAGE("GOING TO STATE - MISSION_STATE_TOWING - VIA MISSION_STATE_PRINTS")
				curStage = MISSION_STATE_TOWING
			ENDIF
		BREAK		
		CASE MISSION_STATE_TOWING
//			DEBUG_MESSAGE("MISSION_STATE_TOWING")

			HANDLE_BLIP_SWITCH()
			
			SWITCH chosenNodeType
				CASE NODE_TYPE_DYNAMIC
				CASE NODE_TYPE_TRAIN
				CASE NODE_TYPE_ACCIDENT
					objective = "TOWT_OBJ_04"
				BREAK
				CASE NODE_TYPE_ABANDON
				CASE NODE_TYPE_HANDI
					//if we havn't already set VDest
					IF IS_VECTOR_ZERO(vDest)
						vDest = vImpound[0]
					ENDIF
					GET_DATA_FROM_GARAGE_LOCATION(8, myGarageData) // because the impound is the last case
					//tow to impound
					objective = "TOWT_OBJ_06"
				BREAK
			ENDSWITCH
			
			IF chosenNodeType = NODE_TYPE_TRAIN AND NOT IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, myVehicle)
				bSupressObjective = TRUE
			ENDIF
			
			IF bTonyaReattachConvo
				IF NOT bPrintReattach
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINT_NOW("TOWT_OBJ_03Ga", DEFAULT_GOD_TEXT_TIME, 1)
						sLastObjective = "TOWT_OBJ_03Ga"						
						bPrintReattach = TRUE
					ENDIF
				ENDIF
			ELSE
				IF bCarCameUnHooked AND NOT bTonyaReattachConvo
					IF bDoTutorial
						IF NOT IS_VEHICLE_IN_DROP_OFF_AREA(vAngledA, vAngledB, fWidth)
							IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TONYA_DETACH", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("bTonyaReattachConvo = TRUE - 01")
								bTonyaReattachConvo = TRUE
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_VEHICLE_IN_DROP_OFF_AREA(vAngledA, vAngledB, fWidth)
							IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya3")) = 1)
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_LOSTCAR", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("bTonyaReattachConvo = TRUE - 02")
									bTonyaReattachConvo = TRUE
								ENDIF
							ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
								IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_CAR_UNHO", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("bTonyaReattachConvo = TRUE - 03")
									bTonyaReattachConvo = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF g_savedGlobals.sTowingData.iTowingJobsCompleted = 1
			OR g_savedGlobals.sTowingData.iTowingJobsCompleted = 2
				HANDLE_ADDITIONAL_HELP_PRINTS()
			ENDIF
			
			// Special Case Audio for Tonya 2
			HANDLE_TONYA_DIALOGUE_HANDI()
			
			IF IS_VECTOR_ZERO(vBDVPos)
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					vBDVPos = GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex)
	//				PRINTLN("vBDVPos = ", vBDVPos)
				ENDIF
			ENDIF
		
			HANDLE_PLAYER_STALL()
			
			IF NOT bWanted
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					SET_VEHICLE_DISABLE_TOWING(brokenVehicles[0].vehicleIndex, FALSE)
				ENDIF
			
				IF CHECK_TOW_ATTACHMENTS(objective, vDest)
					
					// Reset bools
					bCarCameUnHooked = FALSE
					bTonyaReattachConvo = FALSE
					bPrintReattach = FALSE
					
					IF chosenNodeType = NODE_TYPE_TRAIN AND NOT IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, myVehicle)
						bSupressObjective = FALSE
						bReverseOrder = TRUE
						IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
							brokenVehicles[0].blipIndexPed = ADD_BLIP_FOR_ENTITY(brokenVehicles[0].pedIndex)
							SET_BLIP_COLOUR(brokenVehicles[0].blipIndexPed, BLIP_COLOUR_BLUE)
							SET_BLIP_SCALE(brokenVehicles[0].blipIndexPed, BLIP_SIZE_PED)
							PRINTLN("ADDING BLIP - brokenVehicles[0].blipIndexPed")
						ENDIF
						
						PRINTLN("GOING TO STATE - MISSION_STATE_PED")
						curStage = MISSION_STATE_PED
					ELSE
						SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_FACGATE_08, <<414.9510, -1623.8135, 28.2928>>, FALSE, 1.0)
						SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_FACGATE_08, <<415.4840, -1653.1708, 28.2928>>, FALSE, 1.0)
						PRINTLN("TRYING TO UNLOCK DOORS AND KEEP OPEN")
						curStage = MISSION_STATE_DROPOFF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					SET_VEHICLE_DISABLE_TOWING(brokenVehicles[0].vehicleIndex, TRUE)
				ENDIF
			ENDIF
			
			//Added support for pushing the vehicle in the proper spot
			IF IS_VEHICLE_IN_DROP_OFF_AREA(vAngledA, vAngledB, fWidth)
				//Rammed!
				bRammedVehicle = TRUE
				curStage = MISSION_STATE_DROPOFF
			ENDIF
		
		BREAK
		CASE MISSION_STATE_DROPOFF
//			PRINTLN("MISSION_STATE_DROPOFF")
		
			//todo this is a bit hacky..lets solve this better
			IF chosenNodeType = NODE_TYPE_DYNAMIC
				//broken down vehicle
				objective = "TOWT_OBJ_03a"
			ELSE
				objective = "TOWT_OBJ_03Ga"
			ENDIF
			
			SAFELY_REMOVE_ASSETS()
			
			IF NOT bWanted
				HANDLE_TOW_TALK_CONVO()
				HANDLE_NEAR_ENDING_CONVO()
				HANDLE_PLAYER_STALL()
				
				IF CHECK_TOWING(objective)
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(brokenVehicles[0].vehicleIndex, FALSE)
					SET_VEHICLE_DOORS_LOCKED(brokenVehicles[0].vehicleIndex, VEHICLELOCK_LOCKED)
					PRINTLN("SETTING VEHICLE DOORS TO BE LOCKED ON BROKEN DOWN VEHCLE")
					
					SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(myVehicle, FALSE)
					
					IF curBrokenCarCount = 0
						bDisplayHelp = FALSE
						DEBUG_MESSAGE("curBrokenCarCount = 0")
						IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya1")) = 1)
						OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<395.946442,-1647.358887,26.292782>>, <<414.978851,-1623.999634,35.292778>>, 23.250000)
								PRINTLN("GOING TO STATE - MISSION_STATE_POST_DROPOFF VIA - MISSION_STATE_DROPOFF")
								curStage = MISSION_STATE_POST_DROPOFF
							ELSE
								PRINTLN("GOING TO STATE - MISSION_STATE_SPECIAL_CASE VIA - MISSION_STATE_DROPOFF")
								curStage = MISSION_STATE_SPECIAL_CASE
							ENDIF
						ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya3")) = 1)
						OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
							PRINTLN("GOING TO STATE - MISSION_STATE_TONYA_PHONE_CALL")
							curStage = MISSION_STATE_TONYA_PHONE_CALL
						ELSE
							PRINTLN("GOING TO STATE - MISSION_STATE_DROPOFF_TONYA")
							curStage = MISSION_STATE_DROPOFF_TONYA
						ENDIF
					ELSE
						PRINTLN("curBrokenCarCount = ", curBrokenCarCount)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
					bTowHooked = IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
				ENDIF
			ENDIF
		BREAK
		CASE MISSION_STATE_SPECIAL_CASE
			IF HANDLE_PLAYER_BEING_AWAY_FROM_IMPOUND()
				PRINTLN("GOING TO STATE - MISSION_STATE_POST_DROPOFF VIA - MISSION_STATE_SPECIAL_CASE")
				curStage = MISSION_STATE_POST_DROPOFF
			ENDIF
		BREAK
		CASE MISSION_STATE_DROPOFF_TONYA
			IF HANDLE_TONYA_DROPOFF()
				PRINTLN("GOING TO STATE - MISSION_STATE_POST_DROPOFF VIA - MISSION_STATE_DROPOFF")
				curStage = MISSION_STATE_POST_DROPOFF
			ENDIF
		BREAK
		CASE MISSION_STATE_POST_DROPOFF
			PRINTLN("MISSION_STATE_POST_DROPOFF")
			
			IF DOES_BLIP_EXIST(blipImpound)
				REMOVE_BLIP(blipImpound)
			ENDIF
			
			
//			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
			DISABLE_ALL_CONTROLS_EXCEPT_EXIT() // To Fix Bug # 1527410
			
			SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_UNLOCKED)
			PRINTLN("SETTING VEHICLE DOORS TO BE UNLOCKED ON TOW TRUCK")
			
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya1")) = 1)
				IF NOT bPlayedLastConvo
					IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TUT_ENDa", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("PLAYING FIRST TONYA GOODBYE")
						bPlayedLastConvo = TRUE
					ENDIF
				ENDIF
			ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
				IF NOT bPlayedLastConvo
					IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TUT_ENDb", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("PLAYING FIRST TONYA GOODBYE")
						bPlayedLastConvo = TRUE
					ENDIF
				ENDIF
			ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya5")) = 1)
				IF NOT bPlayedLastConvo
					IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD","TOW_TUT_ENDc", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("PLAYING FIRST TONYA GOODBYE")
						bPlayedLastConvo = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bPlayedLastConvo
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CLEAR_SEQUENCE_TASK(taskSequence)
					OPEN_SEQUENCE_TASK(taskSequence)
						TASK_LEAVE_ANY_VEHICLE(NULL, 0)
					CLOSE_SEQUENCE_TASK(taskSequence)
					IF NOT IS_ENTITY_DEAD(pedTonya)
						TASK_PERFORM_SEQUENCE(pedTonya, taskSequence)
						SET_PED_KEEP_TASK(pedTonya, TRUE)
						PRINTLN("TASKING TONYA TO LEAVE VEHICLE AND USE SCENARIO")
					ENDIF
//					CLEAR_SEQUENCE_TASK(taskSequence)
					
					PRINTLN("GOING TO STATE - MISSION_STATE_ENDUI_SETUP VIA - MISSION_STATE_DROPOFF")
					curStage = MISSION_STATE_ENDUI_SETUP
				ENDIF
			ENDIF
		BREAK
		CASE MISSION_STATE_TONYA_PHONE_CALL
			IF HANDLE_TONYA_PHONECALL()
				PRINTLN("GOING TO STATE - MISSION_STATE_ENDUI_SETUP VIA - MISSION_STATE_TONYA_PHONE_CALL")
				curStage = MISSION_STATE_ENDUI_SETUP
			ENDIF
		BREAK
		CASE MISSION_STATE_ENDUI_SETUP
			
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya1")) = 1)
			OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
			OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya5")) = 1)
				DISABLE_ALL_CONTROLS_EXCEPT_EXIT()
			ENDIF
		
			HANDLE_TOW_SUCCESS()
			
			CLEAR_HELP()
			
			IF iTotalPayment > 0
				CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, iTotalPayment)
			ENDIF
			
			IF NOT IS_TIMER_STARTED(tEndingTimer)
				START_TIMER_NOW(tEndingTimer)
				PRINTLN("STARTING tEndingTimer")
			ENDIF
			
			PRINTLN("GOING TO STATE - MISSION_STATE_COMPLETE")
			curStage = MISSION_STATE_COMPLETE
		BREAK
		
		CASE MISSION_STATE_COMPLETE
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya1")) = 1)
			OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
			OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya5")) = 1)
				DISABLE_ALL_CONTROLS_EXCEPT_EXIT()
			ENDIF
		
			IF IS_TIMER_STARTED(tEndingTimer)
				IF GET_TIMER_IN_SECONDS(tEndingTimer) > 3.0
					ENABLE_CONTROLS_ON_TOWTRUCK() // To Fix Bug # 1527410
//					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	fUnhookStartTime = fUnhookStartTime
	bRammedVehicle = bRammedVehicle
	fTotalPayment = fTotalPayment
	vImpound[0] = vImpound[0]
	RETURN FALSE
ENDFUNC

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

PROC TELEPORT_PLAYER(VECTOR myPos)
	WHILE NOT FADE_DOWN()
		WAIT(0)
	ENDWHILE
	
	LOAD_SCENE(myPos)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND DOES_ENTITY_EXIST(myVehicle)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
		ENDIF
	ENDIF
	
	WAIT(500)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	WHILE NOT FADE_UP()
		WAIT(0)
	ENDWHILE
ENDPROC

FUNC BOOL IS_PED_IN_BROKEN_VEHICLE()
	
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya3")) = 1)
	OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya4")) = 1)
		IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
			IF IS_PED_IN_VEHICLE(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex)
				PRINTLN("IS_PED_IN_BROKEN_VEHICLE IS TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_DIST_FAIL_CONDITIONS()

	BOOL bGarage = FALSE
	BOOL bCar = FALSE
	STRING sLine
	
//	PRINTLN("vCurDestObjective[0] = ", vCurDestObjective[0])
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		PRINTLN("PLAYER IS WANTED - RETURN FALSE")
		RETURN FALSE
	ENDIF

	IF chosenNodeType =  NODE_TYPE_DYNAMIC
		//only breakdown currently goes to a garage
		bGarage = TRUE
	ENDIF
	
	IF curStage < MISSION_STATE_DROPOFF
	   //still going to car
	   bCar = TRUE
	ENDIF
	
	IF bCar
		sLine = "TOW_DISP_CAR"
	ELSE
		IF bGarage
			sLine = "TOW_DISP_GAR"
		ELSE
			sLine = "TOW_DISP_IMP"
		ENDIF
	ENDIF
	
	//Abandon check
	IF NOT IS_VECTOR_ZERO(vCurDestObjective[0])
	
		IF NOT bDistWarned
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(vCurDestObjective[0], FALSE) > (fObjDist + 250)
				IF NOT bDistWarned
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				ENDIF
				
				IF NOT IS_PED_IN_BROKEN_VEHICLE()
					IF CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", sLine, CONV_PRIORITY_VERY_HIGH)
						PRINTLN("PLAYING RETURN LINE")
						bDistWarned = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vCurDestObjective[0], FALSE) > (fObjDist + FAIL_DISTANCE)
			IF IS_PED_IN_BROKEN_VEHICLE()
				PRINTLN("failReason = FAIL_ABANDONED_DRIVER")
				failReason = FAIL_ABANDONED_DRIVER
				RETURN TRUE
			ELSE
				PRINTLN("Pringing abandon job because distance to cur objective is bigger than fObjDist + Fail")
				failReason = FAIL_ABANDONED_JOB
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_TIME_FAIL_CONDITIONS()
	//DEBUG_MESSAGE("UPDATE_TIME_FAIL_CONDITIONS")
	IF NOT bTimeWarned
		IF TIMER_DO_WHEN_READY(failTimer, WARN_TIME_TOTAL)
			bTimeWarned = TRUE
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
			CREATE_CONVERSATION(MyLocalPedStruct, "TOWAUD", "TOW_DISP3", CONV_PRIORITY_VERY_HIGH)
			//Do dialouge here to warn the player
		ENDIF
	ENDIF
	
	IF NOT bPrintHurryHelp
		IF curStage = MISSION_STATE_DROPOFF
			IF bTimeWarned
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINT_HELP("TOWT_HELP_TM")
					bPrintHurryHelp = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//PRINTLN("Fail timer",GET_TIMER_IN_SECONDS(failTimer))
	
	IF TIMER_DO_WHEN_READY(failTimer, FAIL_TIME_TOTAL)
		failReason = FAIL_TIME_EXPIRED_TOTAL
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_TIRES_BURST()
	
	// Mid and Front
	IF IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_MID_LEFT)
	AND IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_MID_RIGHT)
	AND IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_FRONT_RIGHT)
	AND IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_FRONT_LEFT)
		PRINTLN("CHECK 1: RETURNING TRUE ON ARE_TIRES_BURST")
		RETURN TRUE
	ENDIF
	
	// Rear and Front
	IF IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_REAR_LEFT)
	AND IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_REAR_RIGHT)
	AND IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_FRONT_RIGHT)
	AND IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_FRONT_LEFT)
		PRINTLN("CHECK 2: RETURNING TRUE ON ARE_TIRES_BURST")
		RETURN TRUE
	ENDIF
	
	// Rear and Mid
	IF IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_REAR_LEFT)
	AND IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_REAR_RIGHT)
	AND IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_MID_LEFT)
	AND IS_VEHICLE_TYRE_BURST(myVehicle, SC_WHEEL_CAR_MID_RIGHT)
		PRINTLN("CHECK 3: RETURNING TRUE ON ARE_TIRES_BURST")
		RETURN TRUE
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_FAIL_CONDITIONS(TOWING_LAUNCH_DATA launchArgs)
	VECTOR vPlayerPosition, vTowTruckPosition

	IF bDriverGotCarBack
		PRINTLN("FAIL - FAIL_DRIVE_GOT_CAR_BACK")
		failReason = FAIL_DRIVER_GOT_CAR_BACK
		RETURN TRUE
	ENDIF
	
	IF bFailedToExit
		PRINTLN("FAIL - FAIL_FRANKLIN_NO_EXIT")
		failReason = FAIL_FRANKLIN_NO_EXIT
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_ANY_SHOP()
		PRINTLN("PLAYER HAS ABANDONED TOWING JOB BY GOING INTO ANY SHOP")
		failReason = FAIL_ABANDONED_JOB
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
		IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
			IF IS_VEHICLE_STUCK_TIMER_UP(brokenVehicles[0].vehicleIndex, VEH_STUCK_ON_ROOF, ROOF_TIME)
				DEBUG_MESSAGE("FAIL_DAMAGE - UPSIDEDOWN")
				failReason = FAIL_DAMAGE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bOutOfVehicle
		//if the player has been out of the vehicle for more than 5 seconds fail him
		IF (GET_GAME_TIMER() - outOfVehicleTime) > FAIL_TIME
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			ENDIF
			IF DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
				vTowTruckPosition = GET_ENTITY_COORDS(myVehicle)
			ENDIF
		
			IF VDIST2(vPlayerPosition, vTowTruckPosition) > 50*50
				DEBUG_MESSAGE("FAIL_ABANDONED_CAR")
				failReason = FAIL_ABANDONED_CAR
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bAggroedTonya
		failReason = FAIL_ATTACKED_TONYA
		PRINTLN("failReason = FAIL_ATTACKED_TONYA")
		RETURN TRUE
	ENDIF
	IF bAbandonedTonya
		failReason = FAIL_ABANDONED_TONYA
		PRINTLN("failReason = FAIL_ABANDONED_TONYA")
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(myVehicle)
		IF NOT IS_VEHICLE_DRIVEABLE(myVehicle) //AND bDrugsLoaded
		OR ARE_TIRES_BURST()
			DEBUG_MESSAGE("myVehicle isn't driveable")
			failReason = FAIL_TRANSPORT_DESTROYED
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("myVehicle DOESN'T EXIST")
		failReason = FAIL_TRANSPORT_DESTROYED
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(myVehicle)
		IF IS_ENTITY_DEAD(myVehicle) //AND bDrugsLoaded//IS_VEHICLE_DRIVEABLE(myVehicle)
			DEBUG_MESSAGE("myVehicle is dead")
			failReason = FAIL_TRANSPORT_DESTROYED
			RETURN TRUE
		ENDIF
	ENDIF
	
	SWITCH launchArgs.launchMode
		CASE TOWING_MODE_TRADITIONAL
			IF curStage > MISSION_STATE_CREATE AND curStage < MISSION_STATE_COMPLETE
				IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
					IF IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						DEBUG_MESSAGE("brokenVehicles[0].vehicleIndex is dead")
						failReason = FAIL_TOW_DESTROYED
						RETURN TRUE
					ELSE
						IF UPDATE_DIST_FAIL_CONDITIONS()
							DEBUG_MESSAGE("UPDATE_DIST_FAIL_CONDITIONS")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF curStage <= MISSION_STATE_DROPOFF
				IF UPDATE_TIME_FAIL_CONDITIONS()
					RETURN TRUE
				ENDIF
			ENDIF
			
			SWITCH chosenNodeType
				CASE NODE_TYPE_DYNAMIC
				CASE NODE_TYPE_TRAIN
				CASE NODE_TYPE_HANDI
				//CASE NODE_TYPE_ABANDON
					IF bPedCreated
						IF NOT bAttack
	//							PRINTLN("bAttack = FALSE")
							IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
								IF IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
									failReason = FAIL_PED_KILLED
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(myVehicle)
		ADD_VEHICLE_UPSIDEDOWN_CHECK(myVehicle)
		PRINTLN("ADD_VEHICLE_UPSIDEDOWN_CHECK for myVehicle")
	ELIF IS_VEHICLE_STUCK_ON_ROOF(myVehicle)
		failReason = FAIL_VEHICLE_DISABLED
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_ON_ROOF, ROOF_TIME) 
	OR IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_ON_SIDE, SIDE_TIME) 
	OR IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_HUNG_UP, HUNG_UP_TIME) 
	OR IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_JAMMED, JAMMED_TIME)
		PRINTLN("STUCK TIMER - FAIL")
		failReason = FAIL_VEHICLE_DISABLED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MISSION_SKIP()
	PRINTLN("INSIDE - MISSION_SKIP")

	IF curStage < MISSION_STATE_ENDUI_SETUP
		curStage = MISSION_STATE_ENDUI_SETUP
		PRINTLN("S-SKIP: GOING TO STATE MISSION_STATE_ENDUI_SETUP")
	ELSE
		PRINTLN("curStage > MISSION_STATE_ENDUI_SETUP")
	ENDIF
ENDPROC

PROC MISSION_PASSED()
	
	//CLEAR_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_MinigameFailed)

	//SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_60_DELAY)
	//ODDJOB_AUTO_SAVE()
	//MISSION_CLEANUP()
	//SET_BITMASK_AS_ENUM(launchArgs.iTowFlags, TOWING_FLAG_PASSED)
	//RETURN TRUE
ENDPROC

PROC HANLDE_PLACING_PLAYER_AND_TONYA_INTO_TOWTRUCK_AND_MOVING()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
		PRINTLN("SETTING THE PLAYER INTO THE TRUCK")
	ENDIF
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
	AND NOT IS_ENTITY_DEAD(myVehicle)
		SET_PED_INTO_VEHICLE(pedTonya, myVehicle, VS_FRONT_RIGHT)
		PRINTLN("SETTING TONYA INTO THE TRUCK")
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<401.6370, -1633.3003, 28.2928>>)
	ENDIF
		
	SET_ENTITY_HEADING(myVehicle, 231.5304)
	
ENDPROC

// debug builds only
#IF IS_DEBUG_BUILD

	VECTOR vCoordinatesToWarp
	FLOAT fHeadingToWarp
	
	VECTOR vVehiclePosition
	FLOAT fVehicleHeading, fTemp

	// debug skips
	PROC DO_DEBUG_SKIPS()
		VECTOR vTemp
		// mission pass S
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			PRINTLN("HIT - S KEY")
			
			IF NOT bSkipTutorial
				bSkipTutorial = TRUE
				PRINTLN("bSkipTutorial = TRUE")
			ENDIF
			
			MISSION_SKIP()
		ENDIF
		
		// mission failed F
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			failReason = FAIL_TRANSPORT_DESTROYED
			MISSION_FAILED()
		ENDIF
		
		// skip stage J
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		
			STOP_SCRIPTED_CONVERSATION(FALSE)
		
			vTemp = vCurDestObjective[0]
		
			IF NOT bTowHooked
				IF NOT IS_ENTITY_DEAD(myVehicle)
					
					IF chosenNodeType = NODE_TYPE_ABANDON
						vTemp = <<226.5837, -854.3120, 28.9568>>
						fTemp = 250.4885
						PRINTLN("J-SKIPPING: ABANDON")
						PRINTLN("vTemp = ", vTemp)
						PRINTLN("fTemp = ", fTemp)
						
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vTemp)
							SET_ENTITY_HEADING(myVehicle, fTemp)
						ENDIF
						IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
							IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(brokenVehicles[0].vehicleIndex, myVehicle)
								ATTACH_VEHICLE_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex, -1, <<0,2.0,0>>)
								PRINTLN("ATTACHING BROKEN DOWN VEHICLE TO TOW TRUCK")
							ENDIF
						ENDIF
					ELIF chosenNodeType = NODE_TYPE_HANDI
						vTemp = <<-604.2744, -889.6982, 24.1644>>    
						fTemp = 87.4990
						PRINTLN("J-SKIPPING: HANDI")
						PRINTLN("vTemp = ", vTemp)
						PRINTLN("fTemp = ", fTemp)
						
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vTemp)
							SET_ENTITY_HEADING(myVehicle, fTemp)
						ENDIF
						IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
							IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(brokenVehicles[0].vehicleIndex, myVehicle)
								ATTACH_VEHICLE_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex, -1, <<0,-1.5,0>>)
								PRINTLN("ATTACHING BROKEN DOWN VEHICLE TO TOW TRUCK")
							ENDIF
						ENDIF
					ELIF chosenNodeType = NODE_TYPE_TRAIN
						vTemp = <<211.1113, -2545.6040, 4.8816>> 
						fTemp = 98.9786
						PRINTLN("J-SKIPPING: TRAIN")
						PRINTLN("vTemp = ", vTemp)
						PRINTLN("fTemp = ", fTemp)
						
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vTemp)
							SET_ENTITY_HEADING(myVehicle, fTemp)
						ENDIF
						IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
							IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(brokenVehicles[0].vehicleIndex, myVehicle)
								ATTACH_VEHICLE_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex, -1, <<0,1.5,0>>)
								PRINTLN("ATTACHING BROKEN DOWN VEHICLE TO TOW TRUCK")
							ENDIF
						ENDIF
					ELIF chosenNodeType = NODE_TYPE_DYNAMIC
						vTemp = <<-469.1497, 132.3600, 63.1074>>
						fTemp = 267.6505
						
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vTemp)
							SET_ENTITY_HEADING(myVehicle, fTemp)
						ENDIF
						IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
							IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(brokenVehicles[0].vehicleIndex, myVehicle)
								ATTACH_VEHICLE_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex, -1, <<0,-1.5,0>>)
								PRINTLN("ATTACHING BROKEN DOWN VEHICLE TO TOW TRUCK")
							ENDIF
						ENDIF
					ELIF chosenNodeType = NODE_TYPE_ACCIDENT
						vTemp = <<-698.9984, -844.8035, 22.5729>>  
						fTemp = 66.5962 
						PRINTLN("J-SKIPPING: ACCIDENT")
						PRINTLN("vTemp = ", vTemp)
						PRINTLN("fTemp = ", fTemp)
						
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vTemp)
							SET_ENTITY_HEADING(myVehicle, fTemp)
						ENDIF
						IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
							IF NOT IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(brokenVehicles[0].vehicleIndex, myVehicle)
								ATTACH_VEHICLE_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex, -1, <<0,-2.0,0>>)
								PRINTLN("ATTACHING BROKEN DOWN VEHICLE TO TOW TRUCK")
							ENDIF
						ENDIF
					ELSE
						vVehiclePosition = GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex)
						fVehicleHeading = GET_ENTITY_HEADING(brokenVehicles[0].vehicleIndex)
						PRINTLN("fVehicleHeading = ", fVehicleHeading)
						
						vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vVehiclePosition, fVehicleHeading, <<0,8,0>>)
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vTemp)
							SET_ENTITY_HEADING(myVehicle, fVehicleHeading)
							PRINTLN("J-SKIPPING: DEFAULT")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("FAILING THIS CHECK - 01")
				ENDIF
			ELSE
				IF ARE_VECTORS_ALMOST_EQUAL(vTemp, <<401.6046, -1632.7806, 28.2928>>)
					vCoordinatesToWarp = <<405.6677, -1630.7886, 28.2928>>
					fHeadingToWarp = 315.0
					PRINTLN("J-SKIPPING: HOOOKED - VECTOR = ", vCoordinatesToWarp)
				ELSE
					vCoordinatesToWarp = vTemp
					fHeadingToWarp = 24.3
					PRINTLN("J-SKIPPING: NOT HOOOKED - VECTOR = ", vCoordinatesToWarp)
				ENDIF
				IF NOT ARE_VECTORS_ALMOST_EQUAL(vCoordinatesToWarp, <<0,0,0>>)
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vCoordinatesToWarp)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeadingToWarp)
					PRINTLN("USING ALTERNATE HEADING")
				ENDIF
			ENDIF
			
			IF bTowHooked
				IF IS_ENTITY_AT_COORD(myVehicle, vCoordinatesToWarp, <<20,20,20>>)
					IF NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
						IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
							DETACH_VEHICLE_FROM_TOW_TRUCK(myVehicle, brokenVehicles[0].vehicleIndex)
							PRINTLN("DETACHING VEHICLE")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("NOT AT RIGHT COORDINATE")
				ENDIF
			ELSE
				PRINTLN("bTowHooked IS FALSE")
			ENDIF
			
			IF chosenNodeType = NODE_TYPE_ACCIDENT
				IF curStage = MISSION_STATE_DROPOFF_TONYA
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<406.3804, -1635.8673, 28.2928>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 315.0)
						PRINTLN("J-SKIPPING: DROPPING OFF TONYA")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// previous stage P
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			// skip back a stage
		ENDIF
	ENDPROC
#ENDIF

PROC HANDLE_CINEMATIC_CAM()
	//SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
	VECTOR vPlayerPos, vBrokenDownPos
	FLOAT fDeltaZ
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
		vBrokenDownPos = GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex)
	ENDIF
	fDeltaZ = (vPlayerPos.z - vBrokenDownPos.z)
//	PRINTLN("fDeltaZ = ", fDeltaZ)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
			EXIT
		ENDIF
	ENDIF
	IF fDeltaZ > 5
		EXIT
	ENDIF
		
	IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
		IF bPrintHintCam AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
			IF chosenNodeType = NODE_TYPE_TRAIN 
				IF trainState > TRAIN_STATE_PRINT AND trainState < TRAIN_STATE_DONE
					PRINT_HELP("TOW_HELP_CIN")
				
					bPrintHintCam = FALSE
				ENDIF
			ELSE
				IF NOT bTowHooked AND GET_PLAYER_DISTANCE_FROM_ENTITY(brokenVehicles[0].vehicleIndex) < 75
					PRINT_HELP("TOW_HELP_CIN")
					bPrintHintCam = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(towArgs.towCam) AND NOT VALIDATE_HINT_CAM()
		CLEANUP_CAM(towArgs)
	ENDIF
	
	bDisableCin = TRUE
	IF SHOULD_CONTROL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
		IF NOT DOES_CAM_EXIST(towArgs.towCam)
			SWITCH chosenNodeType
				CASE NODE_TYPE_TRAIN
					IF trainState > TRAIN_STATE_PRINT AND trainState < TRAIN_STATE_DONE
					AND NOT bDisableCam
						SETUP_TRAIN_CAM(towArgs, brokenVehicles[0].pedIndex, brokenVehicles[0].blockingIndex, iToggle, bDisablePedCam)
					ELSE
						//bEnableBonusCam = FALSE
						SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
					ENDIF
				BREAK
				DEFAULT
					IF VALIDATE_HINT_CAM() AND GET_PLAYER_DISTANCE_FROM_ENTITY(brokenVehicles[0].vehicleIndex) < 100
						//speical case
						IF chosenNodeType = NODE_TYPE_HANDI

						ELSE
							SETUP_SPECIAL_CAM(towArgs, brokenVehicles[0].vehicleIndex, iToggle)
						ENDIF
					ELSE
						//bEnableBonusCam = FALSE
						SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		IF DOES_CAM_EXIST(towArgs.towCam)
			CLEANUP_CAM(towArgs)
		ENDIF
	ENDIF
	
ENDPROC


#IF IS_DEBUG_BUILD
	INT idx
	PROC PRINT_DEBUG_INFO()
		REPEAT MAX_NUM_ILLEGAL_NODES idx
			IF illegalNodes[idx].myType = NODE_TYPE_TRAIN
				PRINTLN("Towing Node: ", idx, " Train distance: ", VDIST(illegalNodes[idx].vCarPos, illegalNodes[idx].vPedPos))
			ENDIF
		ENDREPEAT
	ENDPROC
#ENDIF

PROC INIT_CHARACTER_NAME()
	//TODO.. factor in maile or female
	
	SWITCH chosenNodeType
		CASE NODE_TYPE_ABANDON
			IF g_savedGlobals.sTowingData.iAbandonCompleted% 2 = 0
				sCharacterName = "TOWABDBITCHM"//, TOWABDBITCHM
				iPedID = 2
				PRINTLN("iPedID = ", iPedID)
			ELSE
				sCharacterName = "TOWABDATTACKM"//, TOWABDNATTACKF, 
				iPedID = 2
				PRINTLN("iPedID = ", iPedID)
			ENDIF
		BREAK
		CASE NODE_TYPE_ACCIDENT
			//No peds in accident variation
		BREAK
		CASE NODE_TYPE_DYNAMIC
			sCharacterName = "TOWBREAKDOWN1"//, TOWBREAKF
			iPedID = 3
			PRINTLN("iPedID = ", iPedID)
		BREAK
		CASE NODE_TYPE_HANDI
			sCharacterName = "TOWHANDIBEGM" //, TOWHANDIBEGF
			iPedID = 5
			PRINTLN("iPedID = ", iPedID)
		BREAK
		CASE NODE_TYPE_TRAIN
			sCharacterName = "TOWTRAINM" //, TOWTRAINF
			iPedID = 3
			PRINTLN("iPedID = ", iPedID)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC HANDLE_TONYA_BLIPPING_TO_ENTER_CAR(VEHICLE_INDEX& vehTruck)
	VEHICLE_INDEX vToCheck
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vToCheck = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ELIF NOT bTonyaAbandonWarning
			CLEAR_THIS_PRINT("TOWT_WAIT")
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				IF NOT DOES_BLIP_EXIST(myVehicleBlip)
					myVehicleBlip = ADD_BLIP_FOR_ENTITY(vehTruck)
					SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
					SET_BLIP_ROUTE(myVehicleBlip, TRUE)
					PRINTLN("ADDING BACK IN TOW TRUCK BLIP")
				ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(blipTonya)
				REMOVE_BLIP(blipTonya)
				PRINTLN("REMOVING TONYA BLIP - EARLY")
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) 
	AND NOT IS_ENTITY_DEAD(pedTonya) 
	AND DOES_ENTITY_EXIST(vToCheck) 
	AND NOT IS_ENTITY_DEAD(vToCheck)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vToCheck) AND NOT IS_PED_IN_VEHICLE(pedTonya, vToCheck)
			CLEAR_THIS_PRINT("TOW_TUT_INTRUCK")
			
			// Remove blip
			IF DOES_BLIP_EXIST(myVehicleBlip)
				REMOVE_BLIP(myVehicleBlip)
				PRINTLN("REMOVING TRUCK BLIP - EARLY 01")
			ENDIF
			IF NOT DOES_BLIP_EXIST(blipTonya)
				blipTonya = ADD_BLIP_FOR_ENTITY(pedTonya)
				SET_BLIP_COLOUR(blipTonya, BLIP_COLOUR_BLUE)
				SET_BLIP_SCALE(blipTonya, BLIP_SIZE_PED)
				PRINTLN("ADDING TONYA BLIP - EARLY 01")
			ENDIF
			
			IF NOT bPrintAllowTonyaToEnter
				PRINT("TOWT_WAIT", DEFAULT_GOD_TEXT_TIME, 1)
				PRINTLN("SETTING - bPrintAllowTonyaToEnter = TRUE")
				bPrintAllowTonyaToEnter = TRUE
			ENDIF
		ELSE
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vToCheck) AND IS_PED_IN_VEHICLE(pedTonya, vToCheck)
				// Remove blip
				IF DOES_BLIP_EXIST(blipTonya)
					REMOVE_BLIP(blipTonya)
					PRINTLN("REMOVING TONYA BLIP - EARLY")
				ENDIF
				
				CLEAR_THIS_PRINT("TOWT_WAIT")
				
				// Update blip for truck
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					IF NOT DOES_BLIP_EXIST(myVehicleBlip)
						myVehicleBlip = ADD_BLIP_FOR_ENTITY(vehTruck)
						SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
						SET_BLIP_ROUTE(myVehicleBlip, TRUE)
						PRINTLN("ADDING BACK IN TOW TRUCK BLIP")
					ENDIF
				ENDIF
				
				bPrintAllowTonyaToEnter = FALSE
			
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_REBLIP_TONYA_ABANDON_CHECK(BOOL bCheckCar = TRUE)
	IF bTonyaAbandonWarning
		bTonyaAbandonWarning = FALSE
		PRINTLN("SETTING - bTonyaAbandonWarning = FALSE")
		
		IF DOES_BLIP_EXIST(blipTonya)
			REMOVE_BLIP(blipTonya)
			PRINTLN("REMOVING BLIP - blipTonya")
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(myVehicleBlip)
			IF DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
					myVehicleBlip = ADD_BLIP_FOR_ENTITY(myVehicle)
					PRINTLN("ADDING IN BLIP - myVehicleBlip VIA HANDLE_REBLIP_TONYA_ABANDON_CHECK")
					
					IF curStage = MISSION_STATE_INTRO
						SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
						SET_BLIP_ROUTE(myVehicleBlip, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bCheckCar
			IF NOT DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
				IF NOT IS_VECTOR_ZERO(vCurDestObjective[0])
					brokenVehicles[0].blipIndex = ADD_BLIP_FOR_COORD(vCurDestObjective[0])
					IF bTowHooked
						SET_BLIP_COLOUR(brokenVehicles[0].blipIndex, BLIP_COLOUR_YELLOW)
						SET_BLIP_ROUTE(brokenVehicles[0].blipIndex, TRUE)
						SET_BLIP_ROUTE_COLOUR(brokenVehicles[0].blipIndex, BLIP_COLOUR_YELLOW)
					ELSE
						SET_BLIP_COLOUR(brokenVehicles[0].blipIndex, BLIP_COLOUR_BLUE)
						SET_BLIP_ROUTE(brokenVehicles[0].blipIndex, TRUE)
						SET_BLIP_ROUTE_COLOUR(brokenVehicles[0].blipIndex, BLIP_COLOUR_BLUE)
					ENDIF
					PRINTLN("ADDING BLIP - brokenVehicles[0].blipIndex INSIDE ABANDON TONYA CHECK")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_ABANDONED_TONYA(BOOL bCheckCar = TRUE)
	VECTOR vPlayerPostion, vTonyaPosition
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		OR bOutOfVehicle
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPostion = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF

	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		vTonyaPosition = GET_ENTITY_COORDS(pedTonya)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		IF VDIST2(vPlayerPostion, vTonyaPosition) > 20*20
			IF NOT bTonyaAbandonWarning
			
				CLEAR_PRINTS()
				
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			
				PRINT("TOW_TUT_RETONYA", DEFAULT_GOD_TEXT_TIME, 1)
				bTonyaAbandonWarning = TRUE
				PRINTLN("GIVING PLAYER WARNING ON ABANDONING TONYA")
				
				IF NOT DOES_BLIP_EXIST(blipTonya)
					IF NOT IS_ENTITY_DEAD(pedTonya)
						blipTonya = CREATE_BLIP_FOR_ENTITY(pedTonya)
						SET_BLIP_COLOUR(blipTonya, BLIP_COLOUR_BLUE)
						PRINTLN("ADDING BLIP - blipTonya")
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(myVehicleBlip)
					REMOVE_BLIP(myVehicleBlip)
					PRINTLN("REMOVING BLIP - myVehicleBlip")
				ENDIF
				
				IF bCheckCar
					IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
						REMOVE_BLIP(brokenVehicles[0].blipIndex)
						PRINTLN("REMOVING BLIP - brokenVehicles[0].blipIndex")
					ENDIF
				ENDIF
				
				bCurrentlyTonyaAbandoned = TRUE
			ENDIF
			
			IF NOT bPlayAbandonLine
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_ABAN", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("PLAYING ABANDON LINE")
						bPlayAbandonLine = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF VDIST2(vPlayerPostion, vTonyaPosition) < 20*20
			
				bPlayAbandonLine = FALSE
			
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(pedTonya)
						HANDLE_REBLIP_TONYA_ABANDON_CHECK()
						bCurrentlyTonyaAbandoned = FALSE
					ENDIF
				ELSE
					HANDLE_REBLIP_TONYA_ABANDON_CHECK()
					bCurrentlyTonyaAbandoned = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		IF VDIST2(vPlayerPostion, vTonyaPosition) > 150*150
			IF bTonyaAbandonWarning
				PRINTLN("RETURNING TRUE - HAS_PLAYER_ABANDONED_TONYA")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_TONYA_FOOTSTEPS()
	VECTOR vPlayerPos, vTonyaPos
	
	IF NOT DOES_ENTITY_EXIST(pedTonya)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_ENTITY_DEAD(pedTonya)
		vTonyaPos = GET_ENTITY_COORDS(pedTonya)
	ENDIF
	
	IF VDIST2(vPlayerPos, vTonyaPos) < 25 // 5m
		SET_FORCE_FOOTSTEP_UPDATE(pedTonya, TRUE)
	ELSE
		SET_FORCE_FOOTSTEP_UPDATE(pedTonya, FALSE)
	ENDIF
ENDPROC

PROC HANDLE_TONYA_NO_PUNCHING()
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		SET_PED_RESET_FLAG(pedTonya, PRF_DisablePlayerMeleeFriendlyAttacks, TRUE)
	ENDIF
ENDPROC

PROC HANDLE_TONYA_PEDS_FLEE()
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		IF IS_PED_IN_GROUP(pedTonya)
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_REPLAY_VEHICLE(VECTOR vVehiclePos, FLOAT fVehHeading)

	IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		PRINTLN("REPLAY VEHICLE IS AVAILABLE - TOWING")
		bReplayVehicleAvailable = TRUE
	ELSE
		PRINTLN("REPLAY VEHICLE IS NOT AVAILABLE - TOWING")
	ENDIF
			
	IF bReplayVehicleAvailable
		REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
		WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
			PRINTLN("TOWING: Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED")
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF bReplayVehicleAvailable
		//create the replay vehicle but make sure it isnt a water based vehicle (so we dont spawn it on land)
		VEHICLE_INDEX vehPlayer
		vehPlayer = CREATE_REPLAY_CHECKPOINT_VEHICLE(vVehiclePos, fVehHeading)
		IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehPlayer))
		AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
		AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
		AND (GET_ENTITY_MODEL(vehPlayer) != TOWTRUCK)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
		ELSE
			IF (GET_ENTITY_MODEL(vehPlayer) != TOWTRUCK)
				PRINTLN("REPLAY VEHICLE IS A TOW TRUCK")
			ENDIF
			PRINTLN("DELETING REPLAY VEHICLE - DID NOT PASS TEST")
			DELETE_VEHICLE(vehPlayer)
		ENDIF
	ENDIF
ENDPROC

//STRUCT 

// main script
FUNC BOOL DO_TOWING_CORE(TOWING_LAUNCH_DATA& launchArgs, LoadQueueLarge &sLoadQueue)

//	DEBUG_MESSAGE("DO_TOWING_CORE")
	
	IF launchArgs.bInit
	
		launchArgs.bInit = FALSE
		DEBUG_MESSAGE("doing init")
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
		ENDIF
		
		POPULATE_ILLEGAL_NODES(illegalNodes)
		POPULATE_EXTRA_DATA(extraData)
		
		POPULATE_LOCATIONS()
		INT selectedNode = 0
		
		#IF IS_DEBUG_BUILD
			myDebugData.iNode = -1
			AddWidgets(myDebugData)
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINT_DEBUG_INFO()
			IF launchArgs.bSpecifyNode
				
				IF IS_SCREEN_FADED_OUT()
					CLEAR_AREA(<<408.8376, -1638.9524, 28.2928>>, 8.0, TRUE)
					DO_SCREEN_FADE_IN(500)
				ENDIF
			
				WHILE NOT myDebugData.bNodeSelected
					WAIT(0)
	//				PRINTLN("WAITING ON SPECIFIC NODE")
				ENDWHILE
				
				PRINTLN("INSIDE HERE")
				
				//so we've selected a node
				selectedNode = myDebugData.iNode
				launchArgs.nodeType = illegalNodes[myDebugData.iNode].myType
				launchArgs.launchMode = TOWING_MODE_TRADITIONAL
			ENDIF
		#ENDIF
		
		START_TIMER_NOW(totalTimer)

		SWITCH launchArgs.launchMode 
			CASE TOWING_MODE_TRADITIONAL
				
				IF launchArgs.bDebug
					DEBUG_MESSAGE("launchArgs.bDebug is TRUE")
					chosenNodeType = launchArgs.nodeType
					IF selectedNode > 0
						illegalIdx = selectedNode 
					ELSE
						illegalIdx = FIND_VALID_NODE(100000, chosenNodeType, g_savedGlobals.sTowingData.iLastNodeIndex)
					ENDIF
					
					DEBUG_MESSAGE("This is a debug run of Towing.. selecting specific node type")
					//This is a debug launch.. need to warp the player and do a manual setup
				ELSE
					DEBUG_MESSAGE("launchArgs.bDebug is FALSE")
//					IF g_savedGlobals.sTowingData.iTowingJobsCompleted = 0
//	//					bEnableBonusCam = FALSE
//						bDoTutorial = TRUE
//					ENDIF
					//SELECTION_LOGIC()
					chosenNodeType = launchArgs.nodeType //TODOx2.. this is problematic.. two variables for the same thing.. some confusion here
				ENDIF
					
				DEBUG_MESSAGE("TOWING_TRADITIONAL")
				towVariation = VARIATION_TRADIITONAL
			BREAK
		ENDSWITCH
		
		//If it's the first time we actually have the dispatch guy there.  Otherwise he's NULL
		IF NOT bDoTutorial
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "TOWDISPATCH")
			//ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
		ENDIF

		//this should be in seleciton logic bHomelessVariation
		IF chosenNodeType = NODE_TYPE_HANDI

		ELIF chosenNodeType = NODE_TYPE_ABANDON
			bHomelessVariation = TRUE
			PRINTLN("bHomelessVariation = TRUE")
		ENDIF
		
			//Broken down type requires a ped 
		IF chosenNodeType = NODE_TYPE_DYNAMIC OR chosenNodeType = NODE_TYPE_TRAIN
			bRequiresPed = TRUE
			iNumPeds = 1
			PRINTLN("bRequiresPed = TRUE")
		ELIF chosenNodeType = NODE_TYPE_ACCIDENT
			iNumPeds = extraData[GET_IDX()].numBystanders
		ENDIF
		
		INIT_CHARACTER_NAME()
	
		bPostTonya = FALSE
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				myVehicle = launchArgs.tutorialTruck//GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				PRINTLN("PLAYER IS IN A TOW TRUCK ALREADY - 01")
				
				VEHICLE_INDEX vehTemp
				vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehTemp) AND NOT IS_ENTITY_DEAD(vehTemp)
					IF GET_ENTITY_MODEL(vehTemp) = TOWTRUCK
					OR GET_ENTITY_MODEL(vehTemp) = TOWTRUCK2
						myVehicle = vehTemp
						SET_ENTITY_AS_MISSION_ENTITY(myVehicle)
						PRINTLN("VEHICLE ASSIGNMENT IN PLACE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_LaunchedViaDebug)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_LaunchedViaDebug)
				
				SWITCH towVariation
					CASE VARIATION_TRADIITONAL
						vStartVec = << 522.2961, -1669.1116, 28.2273 >> //<< -67.7942, -1344.1104, 28.2917 >>
						fStartFloat = 52.2 //171.5033 
					BREAK
				ENDSWITCH
				REQUEST_MODEL(missionCarName)
				
				WHILE NOT HAS_MODEL_LOADED(missionCarName)
					WAIT(0)
				ENDWHILE
				
				PRINTLN("SETTING PLAYER INTO TOWING TRUCK VEHICLE")
				myVehicle = CREATE_VEHICLE(missionCarName, vStartVec, fStartFloat)
				
				TELEPORT_PLAYER(vStartVec)
			ENDIF
		#ENDIF
		
		SET_WANTED_LEVEL_MULTIPLIER(0.1)
		
		IF DOES_ENTITY_EXIST(myVehicle)
			IF NOT IS_ENTITY_DEAD(myVehicle)
				SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
				ADD_VEHICLE_UPSIDEDOWN_CHECK(myVehicle)
				SET_VEHICLE_CAN_LEAK_PETROL(myVehicle, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(myVehicle, TRUE)
			ENDIF
		ENDIF
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya1")) = 1)
		OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya2")) = 1)
		OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tonya5")) = 1)
		
			MISSION_DOORS_SET_DOOR_STATE(sMyDoors[TOWING_GATES_EAST], FALSE)
			MISSION_DOORS_SET_DOOR_STATE(sMyDoors[TOWING_GATES_SOUTH], FALSE)
			
			MISSION_DOORS_CLEANUP_DOOR(sMyDoors[TOWING_GATES_EAST])
			MISSION_DOORS_CLEANUP_DOOR(sMyDoors[TOWING_GATES_SOUTH])
		
			bRunnningWithTonya = TRUE
			PRINTLN("bRunnningWithTonya = TRUE")
		ENDIF
	ENDIF
	
		//UPDATE_ZVOLUME_WIDGETS()
		
//		IF NOT bPlayerInTowTruck
//			CHECK_FOR_PLAYER_IN_TOW_TRUCK()
//		ENDIF
		
		#IF IS_DEBUG_BUILD
		
		IF myDebugData.bTurnOnDebugPos
			myDebugData.bTurnOnDebugPos = FALSE
			DEBUG_DARW_BLIPS()
		ENDIF
		
		#ENDIF	
		
		IF curStage < MISSION_STATE_ENDUI_SETUP
		AND (curStage != MISSION_STATE_TONYA_PHONE_CALL)
		AND (curStage != MISSION_STATE_POST_DROPOFF)
		AND (curStage != MISSION_STATE_DROPOFF_TONYA)
			UPDATE_MISSION_CAR(myVehicle, myVehicleBlip, brokenVehicles, vCurDestObjective, sLastObjective, outOfVehicleTime, 
			iClosestIdx, bSuppressBlips, bGpsActive, bInCombat, bUnlockDoors, bWanted, curStage)
			IF bOutOfVehicle
				IF DOES_BLIP_EXIST(blipImpound)
					REMOVE_BLIP(blipImpound)
				ENDIF
				
				IF chosenNodeType = NODE_TYPE_ABANDON
				OR chosenNodeType = NODE_TYPE_HANDI
				OR chosenNodeType = NODE_TYPE_ACCIDENT
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT bKillAnyConversation
							TestResumptionLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
							
							IF NOT bPlayedGetOutConvo
								PRINTLN("KILLING ANY CONVERSATION THAT MIGHT BE ONGOING - PLAYER LEFT VEHICLE")
								KILL_FACE_TO_FACE_CONVERSATION()
							
								bKillAnyConversation = TRUE
							ENDIF
						ENDIF
					ELSE
						IF NOT bPlayedGetOutConvo
							IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_GETOUT", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								PRINTLN("PLAYING - TONYA_GETOUT - CONVO")
								bPlayedGetOutConvo = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				bKillAnyConversation = FALSE
				bPlayedGetOutConvo = FALSE
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			SWITCH towVariation
				CASE VARIATION_TRADIITONAL
				
					IF curStage > MISSION_STATE_CONVERSATION AND curStage < MISSION_STATE_ENDUI_SETUP
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
							//DRAW_MINIGAME_INSTRUCTIONS(helpScaleform)
						ENDIF
						IF chosenNodeType <> NODE_TYPE_ABANDON
						AND chosenNodeType <> NODE_TYPE_ACCIDENT
							IF NOT UPDATE_DAMAGE_TRACKING(printState, brokenVehicles[0].vehicleIndex, myVehicle, iLastHealth, 
							iHealthDelta, bPostTonya)
							
								failReason = FAIL_DAMAGE
								MISSION_FAILED()
								//mission failed on damage
							ENDIF
						ENDIF
						IF bRunnningWithTonya
							UPDATE_TOWTRUCK_DAMAGE_TRACKING(truckPrintState, myVehicle, iTruckLastHealth, iTruckHealthDelta, bPostTonya)
						ENDIF
					ENDIF
					
					//TODO! Tutorial needs to supress other prints
					IF bDoTutorial //OR curStage = MISSION_STATE_DROPOFF
						IF curStage >=  MISSION_STATE_TOWING
							IF NOT bWanted
								UPDATE_TUTORIAL(myVehicle, brokenVehicles[0].vehicleIndex, myGarageData, brokenVehicles[0].blipIndex, vCurDestObjective, sLastObjective,
								vDropOffPosition, bDoTutorial, pedTonya)
							ENDIF
						ENDIF
					ENDIF
					
					bDisableCin = FALSE
					IF (chosenNodeType = NODE_TYPE_HANDI OR chosenNodeType = NODE_TYPE_ABANDON) AND curStage > MISSION_STATE_CREATE AND curStage < MISSION_STATE_COMPLETE
						
						//no extra ped when we're doing a tutorial
						IF NOT bDoTutorial
							IF UPDATE_ANGRY_PED()
//								MISSION_CLEANUP()
							ENDIF
						ELSE
//							PRINTLN("bDoTutorial IS TRUE")
						ENDIF
					ELIF chosenNodeType = NODE_TYPE_TRAIN AND curStage > MISSION_STATE_CREATE AND curStage < MISSION_STATE_COMPLETE
						IF UPDATE_TRAIN()
							MISSION_CLEANUP(sLoadQueue)
						ENDIF
						
						IF trainState > TRAIN_STATE_PRINT //AND trainState < TRAIN_STATE_DONE
							UPDATE_TRAIN_PED()
						ENDIF
					ELIF chosenNodeType = NODE_TYPE_DYNAMIC
						//we need this special check because if the ped get's out of the vehicle we stop updating the main logic
						//which means we stop updating the ped checks in this case.. look into a better solution for this.
						IF curStage = MISSION_STATE_PED AND brokenState < BROKEN_STATE_PED_TALK AND NOT bTriggeredEarly
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
								IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), brokenVehicles[0].vehicleIndex)
								    bTriggeredEarly = TRUE
									IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
										CLEAR_PED_TASKS(brokenVehicles[0].pedIndex)
										TASK_TURN_PED_TO_FACE_ENTITY(brokenVehicles[0].pedIndex, PLAYER_PED_ID())
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bSpawnPed AND NOT bPedCreated
						SPAWN_PED(extraData[GET_IDX()].vUtilityPos, extraData[GET_IDX()].vBloodSplatter, illegalNodes[illegalIdx].vCarPos, 
						brokenVehicles[0].pedIndex, brokenVehicles[0].pedBystanders, pedNames, getPointHead, SpecialAnimDict, 
						SpecialAnims, iNumPeds)
					ELSE

					ENDIF
					
					IF chosenNodeType = NODE_TYPE_ACCIDENT
						UPDATE_STAGING(stagingState, helperCar, helperPed, myVehicle, brokenVehicles[0].pedBystanders, iNumPeds, 
						SpecialAnimDict, SpecialAnims, pedTonya, bPostTonya, brokenVehicles[0].vehicleIndex)
					ENDIF
					
					IF bPedCreated AND NOT bAttack
						IF DO_AGGRO_CHECK(brokenVehicles[0].pedIndex, brokenVehicles[0].vehicleIndex, aggroArgs, aggroReason, 
						FALSE, TRUE, TRUE, TRUE, FALSE)
							
							PRINTLN("PLAYER AGGROED brokenVehicles[0].pedIndex")
							
							KILL_ANY_CONVERSATION()
							
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								CLEAR_PED_TASKS(brokenVehicles[0].pedIndex)
								SET_PED_KEEP_TASK(brokenVehicles[0].pedIndex, TRUE)
								TASK_SMART_FLEE_PED(brokenVehicles[0].pedIndex, PLAYER_PED_ID(), 1000, -1)
								
								IF aggroReason = EAggro_ShotNear
									PRINTLN("PLAYER AGGROED DRIVER WITH WEAPON")
									failReason = FAIL_PED_THREATENED_WITH_WEAPON
								ELIF aggroReason = EAggro_Aiming
									PRINTLN("PLAYER AGGROED DRIVER FAIL_PED_THREATENED_WITH_WEAPON_AIMING WEAPON")
									failReason = FAIL_PED_THREATENED_WITH_WEAPON_AIMING
								ELSE
									PRINTLN("PLAYER AGGROED DRIVER")
									failReason = FAIL_PED_THREATENED
								ENDIF
							ENDIF
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndexPass)
								CLEAR_PED_TASKS(brokenVehicles[0].pedIndexPass)
								SET_PED_KEEP_TASK(brokenVehicles[0].pedIndexPass, TRUE)
								
								CLEAR_SEQUENCE_TASK(taskSequence)
								OPEN_SEQUENCE_TASK(taskSequence)
									TASK_LEAVE_ANY_VEHICLE(NULL, 1, ECF_DONT_CLOSE_DOOR)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
								CLOSE_SEQUENCE_TASK(taskSequence)
								IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndexPass)
									TASK_PERFORM_SEQUENCE(brokenVehicles[0].pedIndexPass, taskSequence)
								ENDIF
								CLEAR_SEQUENCE_TASK(taskSequence)
								
								IF aggroReason = EAggro_ShotNear
									PRINTLN("PLAYER AGGROED PASSENGER WITH WEAPON")
									failReason = FAIL_PED_THREATENED_WITH_WEAPON
								ELSE
									PRINTLN("PLAYER AGGROED PASSENGER 01")
									failReason = FAIL_PED_THREATENED
								ENDIF
							ENDIF
							
							IF NOT bDoTutorial
								SWITCH aggroReason
									CASE EAggro_Attacked
										IF chosenNodeType = NODE_TYPE_ABANDON
											failReason = FAIL_PED_ATTACKED_TRAMP
											PRINTLN("failReason = FAIL_PED_ATTACKED_TRAMP")
											MISSION_FAILED()
										ELSE
											failReason = FAIL_PED_ATTACKED
											PRINTLN("failReason = FAIL_PED_ATTACKED")
											MISSION_FAILED()
										ENDIF
									BREAK
								ENDSWITCH 
							ELSE
								failReason = FAIL_ATTACKED_TOW_TRUCK_OWNER
								MISSION_FAILED()
							ENDIF
							
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndex)
								MISSION_FAILED()
							ENDIF
						ENDIF
						
						IF DO_AGGRO_CHECK(brokenVehicles[0].pedIndexPass, brokenVehicles[0].vehicleIndex, aggroArgs, aggroReason, FALSE, TRUE, FALSE, TRUE, FALSE)
							KILL_ANY_CONVERSATION()
							
							IF NOT IS_ENTITY_DEAD(brokenVehicles[0].pedIndexPass)
								CLEAR_PED_TASKS(brokenVehicles[0].pedIndexPass)
								SET_PED_KEEP_TASK(brokenVehicles[0].pedIndexPass, TRUE)
								TASK_SMART_FLEE_PED(brokenVehicles[0].pedIndexPass, PLAYER_PED_ID(), 1000, -1)
								PRINTLN("PLAYER AGGROED PASSENGER 02")
								
								IF aggroReason = EAggro_ShotNear
									PRINTLN("PLAYER AGGROED DRIVER WITH WEAPON - 01")
									failReason = FAIL_PED_THREATENED_WITH_WEAPON
								ELSE
									PRINTLN("PLAYER AGGROED DRIVER- 01")
									failReason = FAIL_PED_THREATENED
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF bAttack
//							PRINTLN("bAttack")
						ENDIF
					ENDIF
										
					//TODO switch to destination
					IF NOT bDisableCin AND VALIDATE_HINT_CAM(FALSE)
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
							IF NOT bTowHooked
//								SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
								
								IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
									IF NOT IS_ENTITY_DEAD(brokenVehicles[0].vehicleIndex)
										CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, GET_ENTITY_COORDS(brokenVehicles[0].vehicleIndex))
									
										IF IS_GAMEPLAY_HINT_ACTIVE()
											IF NOT IS_AUDIO_SCENE_ACTIVE("TOWING_FOCUS_CAM_ON_CAR")
												START_AUDIO_SCENE("TOWING_FOCUS_CAM_ON_CAR")
												PRINTLN("STARING AUDIO SCENE - TOWING_FOCUS_CAM_ON_CAR")
											ENDIF
										ELSE
											IF IS_AUDIO_SCENE_ACTIVE("TOWING_FOCUS_CAM_ON_CAR")
												STOP_AUDIO_SCENE("TOWING_FOCUS_CAM_ON_CAR")
												PRINTLN("STOPPING AUDIO SCENE - TOWING_FOCUS_CAM_ON_CAR")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vDest)
								//KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
								//SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
							ENDIF
						ELSE
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
							SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
						ENDIF
					ENDIF
					
					IF NOT VALIDATE_HINT_CAM(FALSE)
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					ENDIF
				
					UPDATE_PRINT_DELAY(printTimer, 7, TRUE)
					UPDATE_QUICK_CONV_QUEUE()
					//UPDATE_CLOSEST_NODE()
					UPDATE_BLIPPED_ENTITIES()
					
					IF (NOT bOutOfVehicle OR curStage >= MISSION_STATE_ENDUI_SETUP)
					OR bSkipTutorial
						
						IF UPDATE_TRADITIONAL(launchArgs, sLoadQueue)
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF curStage = MISSION_STATE_DROPOFF
						HANDLE_CAR_DAMAGE_DIALOGUE()
					ENDIF
						
					IF chosenNodeType = NODE_TYPE_TRAIN
						IF curStage = MISSION_STATE_DROPOFF
							MANAGE_PED_CONVERSATION(MyLocalPedStruct, brokenVehicles, myVehicle)
						ENDIF
					ELIF chosenNodeType = NODE_TYPE_DYNAMIC
						IF curStage = MISSION_STATE_TOWING
						OR curStage = MISSION_STATE_DROPOFF
							MANAGE_PED_CONVERSATION(MyLocalPedStruct, brokenVehicles, myVehicle)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			SET_AMBIENT_PED_RANGE_MULTIPLIER_THIS_FRAME(1.5)
			
			IF UPDATE_FAIL_CONDITIONS(launchArgs)
				DEBUG_MESSAGE("Setting Fail")
				MISSION_FAILED()
			ENDIF
			
			IF bFailRc
				DEBUG_MESSAGE("Setting RC")
				//SET_BITMASK_AS_ENUM(launchArgs.iTowFlags, TOWING_FLAG_FAILED)
			ENDIF
			
		ENDIF
			
		#IF IS_DEBUG_BUILD
			DO_DEBUG_SKIPS()
		#ENDIF
	//ENDWHILE
	
	RETURN FALSE
ENDFUNC
