

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "cutscene_public.sch"
USING "RC_launcher_public.sch"
USING "RC_Setup_public.sch"
USING "initial_scenes_Tonya.sch"

// *****************************************************************************************
//		SCRIPT NAME	:	launcher_Tonya.sc
//
//		AUTHOR		:	
//
//		DESCRIPTION	:	Launcher script that determines which RC mission scene to setup.
//						This launcher script should be attached to appropriate world point.
// *****************************************************************************************
// Constants
CONST_FLOAT WORLD_POINT_COORD_TOLERANCE 1.0

// Enums
ENUM BECKON_STAGE 
	BECKON_STAGE_INIT_DIALOGUE,
	BECKON_STAGE_PLAY_DIALOGUE,
	BECKON_STAGE_UPDATE_DIALOGUE,
	BECKON_STAGE_FINISH
ENDENUM

// Variables
VECTOR				 		vSceneOrigin = <<0,0,0>>					// Mission location

SCENARIO_BLOCKING_INDEX 	scenarioBlocker								// Scenario and area checks
SEQUENCE_INDEX 				iSeqTask
BOOL						bScenarioBlocked = FALSE
BOOL						bAnimsLoaded = FALSE

BECKON_STAGE 				eBeckonStage = BECKON_STAGE_INIT_DIALOGUE	// Conversation
structPedsForConversation	pedConversation				
structTimer					tBeckonTimer
INT							iCurrentBeckon
INT 						iCutsceneLoadRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST	// ID to register off-mission cutscene load request with cutscene_controller.

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Does any necessary cleanup and terminates the launcher's thread.
PROC Script_Cleanup(g_structRCScriptArgs& sData, BOOL bCleanupEntities = TRUE)
	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
	IF bCleanupEntities
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Cleaning up entities in Launcher")
		RC_CleanupSceneEntities(sData, FALSE)
	ENDIF
	
	REMOVE_ANIM_DICT("special_ped@tonya@intro")
	REMOVE_ANIM_DICT("special_ped@tonya@base")
	
	IF bAnimsLoaded
		REMOVE_ANIM_DICT("special_ped@tonya@franklin_1@franklin_1a")
		REMOVE_ANIM_DICT("special_ped@tonya@franklin_1@franklin_1b")
		REMOVE_ANIM_DICT("special_ped@tonya@franklin_1@franklin_1c")
		
		REMOVE_ANIM_DICT("special_ped@tonya@franklin_2@franklin_2a")
		REMOVE_ANIM_DICT("special_ped@tonya@franklin_2@franklin_2b")
		REMOVE_ANIM_DICT("special_ped@tonya@franklin_2@franklin_2c")
		
		REMOVE_ANIM_DICT("special_ped@tonya@franklin_3@franklin_3a")
		REMOVE_ANIM_DICT("special_ped@tonya@franklin_3@franklin_3b")
		
		REMOVE_ANIM_DICT("special_ped@tonya@franklin_4@franklin_4a")
		REMOVE_ANIM_DICT("special_ped@tonya@franklin_4@franklin_4b")
		REMOVE_ANIM_DICT("special_ped@tonya@franklin_4@franklin_4c")
		
		CPRINTLN(DEBUG_RANDOM_CHAR, "REMOVING ANIMATIONS - bAnimsLoaded IS TRUE")
	ENDIF
	
	REMOVE_ANIM_DICT("special_ped@tonya@franklin_5@franklin_5a")
	REMOVE_ANIM_DICT("special_ped@tonya@franklin_5@franklin_5b")
	REMOVE_ANIM_DICT("special_ped@tonya@franklin_5@franklin_5c")

	// Clear any cutscene requests with controller. 
	IF iCutsceneLoadRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST 
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Ending off-mission cutscene request")
		END_OFFMISSION_CUTSCENE_REQUEST(iCutsceneLoadRequestID) 
	ENDIF

	IF bScenarioBlocked
		REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlocker)
	ENDIF

	// Kill the thread
	PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Creates the initial scene
FUNC BOOL LOAD_INITIAL_SCENE(g_structRCScriptArgs& sData)
	
	// Setup Tonya scene
	IF NOT SetupScene_TONYA(sData)
		RETURN FALSE
	ENDIF
	
	// Setup scenario blocker
	scenarioBlocker = ADD_SCENARIO_BLOCKING_AREA((vSceneOrigin  - <<10,10,10>>), (vSceneOrigin + <<10,10,10>>))
	bScenarioBlocked = TRUE
	
	// Scene creation successful
	PRINT_LAUNCHER_DEBUG("Created initial scene")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handle Tonya talking to the player characters
PROC HANDLE_SCENE_DIALOGUE(g_structRCScriptArgs& sData)

	IF Is_Player_Timetable_Scene_In_Progress() OR IS_PLAYER_SWITCH_IN_PROGRESS()
		STOP_SCRIPTED_CONVERSATION(FALSE)
//		CPRINTLN(DEBUG_RANDOM_CHAR, "EXITING EARLY ON TONYA CONVO")
		EXIT
	ENDIF
	
	IF NOT bAnimsLoaded
		REQUEST_ANIM_DICT("special_ped@tonya@franklin_1@franklin_1a")
		REQUEST_ANIM_DICT("special_ped@tonya@franklin_1@franklin_1b")
		REQUEST_ANIM_DICT("special_ped@tonya@franklin_1@franklin_1c")
		REQUEST_ANIM_DICT("special_ped@tonya@franklin_2@franklin_2a")
		REQUEST_ANIM_DICT("special_ped@tonya@franklin_2@franklin_2b")
		REQUEST_ANIM_DICT("special_ped@tonya@franklin_2@franklin_2c")
		REQUEST_ANIM_DICT("special_ped@tonya@franklin_3@franklin_3a")
		REQUEST_ANIM_DICT("special_ped@tonya@franklin_3@franklin_3b")
		REQUEST_ANIM_DICT("special_ped@tonya@franklin_4@franklin_4a")
		REQUEST_ANIM_DICT("special_ped@tonya@franklin_4@franklin_4b")
		REQUEST_ANIM_DICT("special_ped@tonya@franklin_4@franklin_4c")
		
		IF HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_1@franklin_1a")
		AND HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_1@franklin_1b")
		AND HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_1@franklin_1c")
		AND HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_2@franklin_2a")
		AND HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_2@franklin_2b")
		AND HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_2@franklin_2c")
		AND HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_3@franklin_3a")
		AND HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_3@franklin_3b")
		AND HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_4@franklin_4a")
		AND HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_4@franklin_4b")
		AND HAS_ANIM_DICT_LOADED("special_ped@tonya@franklin_4@franklin_4c")
			bAnimsLoaded = TRUE
			
			CPRINTLN(DEBUG_RANDOM_CHAR, "ANIMATIONS HAVE LOADED")
		ELSE
			CPRINTLN(DEBUG_RANDOM_CHAR, "WAITING ON ANIMATION DICTIONARIES TO LOAD")
		ENDIF
	ENDIF
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
	
		// Get distance from player to Tonya
		FLOAT fDist2 = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vSceneOrigin)
		
		// Within range
		IF (fDist2 < 20.0*20.0) AND bAnimsLoaded
		
			// Handle beckon dialogue
			SWITCH eBeckonStage
			
				//--------------------------------------------------------------------------------------------
				// Initial dialogue
				CASE BECKON_STAGE_INIT_DIALOGUE
					
					// No dialogue currently playing...
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
						// Add Tonya
						ADD_PED_FOR_DIALOGUE(pedConversation, 3, sData.pedID[0], "TONYA")
						
						// Add relevant player character and create conversation
						IF sData.eMissionID = RC_TONYA_1
							CREATE_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECK_1", CONV_PRIORITY_AMBIENT_HIGH)  //Hey, Franklin, why don't you come over here and let me take care of that lonely ass?
							IF NOT IS_ENTITY_DEAD(sData.pedID[0])
								CLEAR_SEQUENCE_TASK(iSeqTask)
								OPEN_SEQUENCE_TASK(iSeqTask)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_5@franklin_5a", "hey_franklin_0", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_5@franklin_5b", "hey_franklin_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_5@franklin_5c", "hey_franklin_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
								CLOSE_SEQUENCE_TASK(iSeqTask)
								TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
								CLEAR_SEQUENCE_TASK(iSeqTask)
							ENDIF
						ELIF sData.eMissionID = RC_TONYA_2
							PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECK2", "TONYA_BECK2_1", CONV_PRIORITY_AMBIENT_HIGH)
							IF NOT IS_ENTITY_DEAD(sData.pedID[0])
								CLEAR_SEQUENCE_TASK(iSeqTask)
								OPEN_SEQUENCE_TASK(iSeqTask)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_5@franklin_5a", "hey_franklin_0", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_5@franklin_5b", "hey_franklin_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_5@franklin_5c", "hey_franklin_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
								CLOSE_SEQUENCE_TASK(iSeqTask)
								TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
								CLEAR_SEQUENCE_TASK(iSeqTask)
							ENDIF
						ELIF sData.eMissionID = RC_TONYA_5
							PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECKON", "TONYA_BECKON_1", CONV_PRIORITY_AMBIENT_HIGH)
							IF NOT IS_ENTITY_DEAD(sData.pedID[0])
								CLEAR_SEQUENCE_TASK(iSeqTask)
								OPEN_SEQUENCE_TASK(iSeqTask)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_5@franklin_5a", "hey_franklin_0", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_5@franklin_5b", "hey_franklin_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_5@franklin_5c", "hey_franklin_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
								CLOSE_SEQUENCE_TASK(iSeqTask)
								TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
								CLEAR_SEQUENCE_TASK(iSeqTask)
							ENDIF
						ENDIF
						
						// Start conversation timer
						IF NOT IS_TIMER_STARTED(tBeckonTimer)
							START_TIMER_NOW(tBeckonTimer)
						ELSE
							RESTART_TIMER_NOW(tBeckonTimer)
						ENDIF
						
						// Update stage
						eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
						iCurrentBeckon = 0
					ENDIF
				BREAK
			
				//--------------------------------------------------------------------------------------------
				// Wait for dialogue to finish
				CASE BECKON_STAGE_PLAY_DIALOGUE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_TIMER_STARTED(tBeckonTimer)
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(sData.pedID[0])
								TASK_LOOK_AT_ENTITY(sData.pedID[0], PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
								CPRINTLN(DEBUG_RANDOM_CHAR, "TASKING TONYA TO LOOK AT THE PLAYER")
							ENDIF
						
							RESTART_TIMER_NOW(tBeckonTimer)
							CPRINTLN(DEBUG_RANDOM_CHAR, "GOING TO STATE - BECKON_STAGE_UPDATE_DIALOGUE")
							eBeckonStage = BECKON_STAGE_UPDATE_DIALOGUE
						ENDIF
					ENDIF
				BREAK
				
				//---------------------------------------------------------------------------------------------	
				// Play further beckon dialogue
				CASE BECKON_STAGE_UPDATE_DIALOGUE
				
					IF IS_TIMER_STARTED(tBeckonTimer)
						IF GET_TIMER_IN_SECONDS(tBeckonTimer) > GET_RANDOM_FLOAT_IN_RANGE(9, 15)
									
							// Update current beckon
							iCurrentBeckon++
							CPRINTLN(DEBUG_RANDOM_CHAR, "iCurrentBeckon = ", iCurrentBeckon)
							
							// Play appropriate line
							SWITCH iCurrentBeckon
							
								CASE 1
									IF sData.eMissionID = RC_TONYA_1
										CREATE_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECK_2", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_4@franklin_4a", "whats_wrong_with_you_0", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_4@franklin_4b", "whats_wrong_with_you_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_4@franklin_4c", "whats_wrong_with_you_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 2: What's wrong with you, boy? Waiting on Tanisha when a real woman could give you a good time.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ELIF sData.eMissionID = RC_TONYA_2
										PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECK2", "TONYA_BECK2_2", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_4@franklin_4a", "whats_wrong_with_you_0", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_4@franklin_4b", "whats_wrong_with_you_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_4@franklin_4c", "whats_wrong_with_you_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 2: What's wrong with you, boy? Waiting on Tanisha when a real woman could give you a good time.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ELIF sData.eMissionID = RC_TONYA_5
										PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECKON", "TONYA_BECKON_2", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_4@franklin_4a", "whats_wrong_with_you_0", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_4@franklin_4b", "whats_wrong_with_you_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_4@franklin_4c", "whats_wrong_with_you_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 2: What's wrong with you, boy? Waiting on Tanisha when a real woman could give you a good time.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ENDIF
								BREAK
							
								CASE 2
									IF sData.eMissionID = RC_TONYA_1
										CREATE_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECK_3", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_1@franklin_1a", "seriously_franklin_get_your_fat_0", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_1@franklin_1b", "seriously_franklin_get_your_fat_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_1@franklin_1c", "seriously_franklin_get_your_fat_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 3: Seriously, Franklin, get your fat ass over and show a girl a good time.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ELIF sData.eMissionID = RC_TONYA_2
										PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECK2", "TONYA_BECK2_3", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_1@franklin_1a", "seriously_franklin_get_your_fat_0", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_1@franklin_1b", "seriously_franklin_get_your_fat_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_1@franklin_1c", "seriously_franklin_get_your_fat_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 3: Seriously, Franklin, get your fat ass over and show a girl a good time.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ELIF sData.eMissionID = RC_TONYA_5
										PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECKON", "TONYA_BECKON_3", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_1@franklin_1a", "seriously_franklin_get_your_fat_0", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_1@franklin_1b", "seriously_franklin_get_your_fat_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_1@franklin_1c", "seriously_franklin_get_your_fat_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 3: Seriously, Franklin, get your fat ass over and show a girl a good time.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ENDIF
								BREAK
								
								CASE 3
									IF sData.eMissionID = RC_TONYA_1
										CREATE_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECK_4", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_3@franklin_3a", "dont_be_actin_like_you_dont_know_me_0", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_3@franklin_3b", "dont_be_actin_like_you_dont_know_me_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 4: Hey Franklin, don't act like you don't know a sister. Come on, boy, come over here, don't be dissing your home girl.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ELIF sData.eMissionID = RC_TONYA_2
										PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECK2", "TONYA_BECK2_4", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_3@franklin_3a", "dont_be_actin_like_you_dont_know_me_0", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_3@franklin_3b", "dont_be_actin_like_you_dont_know_me_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 4: Hey Franklin, don't act like you don't know a sister. Come on, boy, come over here, don't be dissing your home girl.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ELIF sData.eMissionID = RC_TONYA_5
										PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECKON", "TONYA_BECKON_4", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_3@franklin_3a", "dont_be_actin_like_you_dont_know_me_0", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_3@franklin_3b", "dont_be_actin_like_you_dont_know_me_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 4: Hey Franklin, don't act like you don't know a sister. Come on, boy, come over here, don't be dissing your home girl.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ENDIF	
								BREAK
							
								CASE 4
									IF sData.eMissionID = RC_TONYA_1
										CREATE_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECK_5", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_2@franklin_2a", "seriously_franklin_im_tonya_0", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_2@franklin_2b", "seriously_franklin_im_tonya_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_2@franklin_2c", "seriously_franklin_im_tonya_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 5: Seriously Franklin, I ain't just some crackhead, okay? I'm Tonya, you know me. We almost family.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ELIF sData.eMissionID = RC_TONYA_2
										PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECK2", "TONYA_BECK2_5", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_2@franklin_2a", "seriously_franklin_im_tonya_0", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_2@franklin_2b", "seriously_franklin_im_tonya_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_2@franklin_2c", "seriously_franklin_im_tonya_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 5: Seriously Franklin, I ain't just some crackhead, okay? I'm Tonya, you know me. We almost family.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ELIF sData.eMissionID = RC_TONYA_5
										PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConversation, "TOWAUD", "TONYA_BECKON", "TONYA_BECKON_5", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_ENTITY_DEAD(sData.pedID[0])
											CLEAR_SEQUENCE_TASK(iSeqTask)
											OPEN_SEQUENCE_TASK(iSeqTask)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_2@franklin_2a", "seriously_franklin_im_tonya_0", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_2@franklin_2b", "seriously_franklin_im_tonya_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@franklin_2@franklin_2c", "seriously_franklin_im_tonya_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
												TASK_PLAY_ANIM(NULL, "special_ped@tonya@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
											CLOSE_SEQUENCE_TASK(iSeqTask)
											TASK_PERFORM_SEQUENCE(sData.pedID[0], iSeqTask)
											CLEAR_SEQUENCE_TASK(iSeqTask)
											CPRINTLN(DEBUG_RANDOM_CHAR, "BECKON 5: Seriously Franklin, I ain't just some crackhead, okay? I'm Tonya, you know me. We almost family.")
											eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
							
							// Either wait for new line of dialogue or stop 
							// playing if we have gone through all of the lines..
							IF iCurrentBeckon > 4
								CPRINTLN(DEBUG_RANDOM_CHAR, "GOING BACK TO FIRST BECKON 1")
								iCurrentBeckon = 1
								eBeckonStage = BECKON_STAGE_PLAY_DIALOGUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				//--------------------------------------------------------------------------------------------
				// Don't play any more lines once we have run out of dialogue
				CASE BECKON_STAGE_FINISH
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Main script loops
/// PARAMS:
///    in_coords - world point co-ords
SCRIPT(coords_struct in_coords)
	
	// Launcher priority for streaming requests
	SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)
	
	// Scene information to pass to mission script
	g_structRCScriptArgs sRCLauncherData			
	
	//Reset all basic values of the data, so each scene has to set them up correctly
	RC_Reset_LauncherData(sRCLauncherData)

	// Update scene origin from world point
	vSceneOrigin = in_coords.vec_coord[0]

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		
		// Ensure candidate id is released in the event that the player has died
		// or been arrested prior to mission launch
		IF sRCLauncherData.eMissionID <> NO_RC_MISSION
			IF (g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID <> NO_CANDIDATE_ID)
				PRINT_LAUNCHER_DEBUG("Relinquishing candidate id...")
				Mission_Over(g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID)
			ENDIF
		ENDIF
		
		// Standard cleanup
		Script_Cleanup(sRCLauncherData)
	ENDIF
	
	// Pick which mission activated us
	g_eRC_MissionIDs eRCMissions[3]
	eRCMissions[0] = RC_TONYA_1
	eRCMissions[1] = RC_TONYA_2
	eRCMissions[2] = RC_TONYA_5
	
	IF NOT DETERMINE_RC_TO_LAUNCH(eRCMissions, sRCLauncherData, vSceneOrigin, WORLD_POINT_COORD_TOLERANCE)
		// B* 1510945 - don't call cleanup if nothing has been setup yet 
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()	
	ENDIF
		 
	// Check with the Random Character Controller to see if this script is allowed to launch
	IF NOT CAN_RC_LAUNCH(sRCLauncherData.eMissionID)
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// Halt launcher as we are incorrect character
	IF Random_Character_Blocked_Due_To_Character(sRCLauncherData.eMissionID)
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()
	ENDIF
	

	IF IS_MISSION_AVAILABLE(SP_MISSION_LAMAR)	
		PRINT_LAUNCHER_DEBUG("SP_MISSION_LAMAR is available... [TERMINATING]")
		TERMINATE_THIS_THREAD()
	ELIF IS_MISSION_AVAILABLE(SP_MISSION_MICHAEL_2)
		PRINT_LAUNCHER_DEBUG("SP_MISSION_MICHAEL_2 is available... [TERMINATING]")
		TERMINATE_THIS_THREAD()
		
	ELIF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_0)
		PRINT_LAUNCHER_DEBUG("SP_MISSION_FRANKLIN_0 is available... [TERMINATING]")
		TERMINATE_THIS_THREAD()
		
	ELIF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_1)
		PRINT_LAUNCHER_DEBUG("SP_MISSION_FRANKLIN_1 is available... [TERMINATING]")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// The script is allowed to launch so set up the initial scene
	WHILE NOT LOAD_INITIAL_SCENE(sRCLauncherData)
		WAIT(0)
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			PRINT_LAUNCHER_DEBUG("Player out of range [TERMINATING]")
			Script_Cleanup(sRCLauncherData)
		ENDIF
	ENDWHILE

	// Clears area of non-mission entities and blood decals
	CLEAR_AREA(vSceneOrigin, 5, TRUE)
	
	// Loop to check conditions - should script terminate? or is player close enough to trigger mission?
	WHILE (TRUE)
		
		WAIT(0)

		// Is the player still in activation range
		IF NOT IS_RC_FINE_AND_IN_RANGE(sRCLauncherData)
		OR Random_Character_Blocked_Due_To_Character(sRCLauncherData.eMissionID)
			Script_Cleanup(sRCLauncherData)
		ENDIF
		
		IF IS_MISSION_AVAILABLE(SP_MISSION_LAMAR)
			PRINT_LAUNCHER_DEBUG("SP_MISSION_LAMAR became available... [TERMINATING]")
			Script_Cleanup(sRCLauncherData)
			
		ELIF IS_MISSION_AVAILABLE(SP_MISSION_MICHAEL_2)
			PRINT_LAUNCHER_DEBUG("SP_MISSION_MICHAEL_2 became available... [TERMINATING]")
			Script_Cleanup(sRCLauncherData)
			
		ELIF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_0)
			PRINT_LAUNCHER_DEBUG("SP_MISSION_FRANKLIN_0 became available... [TERMINATING]")
			Script_Cleanup(sRCLauncherData)
			
		ELIF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_1)
			PRINT_LAUNCHER_DEBUG("SP_MISSION_FRANKLIN_1 became available... [TERMINATING]")
			Script_Cleanup(sRCLauncherData)
		ENDIF
	
		// Update launcher blip
		SET_RC_AWAITING_TRIGGER(sRCLauncherData.eMissionID)
		MANAGE_PRELOADING_RC_CUTSCENE(iCutsceneLoadRequestID, sRCLauncherData.sIntroCutscene, vSceneOrigin)
	
		// Updates Tonya dialogue
		HANDLE_SCENE_DIALOGUE(sRCLauncherData)
		
		// Is the player close enough to start the mission off
		IF ARE_RC_TRIGGER_CONDITIONS_MET(sRCLauncherData)
	
			// Launch the mission script
			IF NOT LAUNCH_RC_MISSION(sRCLauncherData)
				Script_Cleanup(sRCLauncherData)
			ENDIF

			// Waits in a loop until we can terminate the launcher - flagged from mission at the moment
			IF IS_RC_LAUNCHER_SAFE_TO_TERMINATE(sRCLauncherData.eMissionID)
				Script_Cleanup(sRCLauncherData, FALSE)			
			ENDIF
		ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
