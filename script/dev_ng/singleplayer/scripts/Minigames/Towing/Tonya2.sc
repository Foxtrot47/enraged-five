
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Threat_Public.sch"
USING "RC_towing.sch"
USING "load_queue_public.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Tonya2.sc
//		AUTHOR			:	
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs sRCLauncherDataLocal

LoadQueueLarge sLoadQueue

ENUM eRC_MainState
	MS_INIT,
	MS_MEET_TONYA,
	MS_GO_TO_TRUCK,
	MS_DO_MISSION,
	MS_FAILED,
	MS_PASSED
ENDENUM

ENUM eRC_SubState
	SS_SETUP = 0,
	SS_CUTSCENE_SETUP,
	SS_CUTSCENE_LEADIN,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

ENUM eRC_FAIL_STATE
	SS_FAIL_SETUP = 0,
	SS_FAIL_UPDATE,
	SS_FAIL_CLEANUP
ENDENUM

// Mission state
eRC_MainState 			  	m_eState = MS_INIT
eRC_SubState  			  	m_eSubState = SS_SETUP
eRC_FAIL_STATE 				m_FailState = SS_FAIL_SETUP
STRING					  	sFailReason = "DEFAULT"

TOWING_LAUNCH_DATA launchArgs

// Mission
VEHICLE_INDEX			  	vehTruck
VEHICLE_INDEX 				viPlayerLastVehicle

MODEL_NAMES					mnTonya = IG_TONYA

VECTOR					  	vTruckPos = <<401.6370, -1633.3003, 28.2928>>
VECTOR 						vTonya2CarGenPosition = <<-597.2252, -886.2328, 24.4968>>
VECTOR 						vCutscenePosition = <<-16.5304, -1473.1208, 29.6110>> 

FLOAT					  	fTruckHeading = 231.5304

INT 						iStageToUse
INT							iTonyaDialogueStages
INT							iNavmeshBlocking

BOOL						bPlayedDelayInTruckConvo = FALSE
BOOL						bSetupTonyaAI = FALSE
BOOL						bTonyaGotCarDialogue01 = FALSE
BOOL						bTonyaCarJackingConvo = FALSE
BOOL						bTaskedTonyaToEnterTruck = FALSE
BOOL						bStartedReplay = FALSE
BOOL						bPrintFirstObjective = FALSE
BOOL						bMovedCar = FALSE
BOOL 						bIsPlayingConvo = FALSE

structTimer					tDelayTruckTimer
structTimer					tLeadInTimer

//REPLAY VARIABLES
VECTOR vPlayerFirstCheckpointPosition = <<-10.7897, -1471.2037, 29.5520>>
FLOAT fPlayerFirstCheckpointHeading = 276.1668
VECTOR vTonyaFirstCheckpointPosition = <<-14.3934, -1472.6962, 29.5896>> 
FLOAT fTonyaFirstCheckpointHeading = 314.2467

// -----------------------------------
// DEBUG MODE
// -----------------------------------
#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

#IF IS_DEBUG_BUILD

	BOOL bDoingPSkip = FALSE
//	BOOL bDoingJSkip = FALSE
	
	MissionStageMenuTextStruct sSkipMenu[2]
	INT iDebugJumpStage = 0              
#ENDIF

PROC SETUP_DEBUG()
	#IF IS_DEBUG_BUILD
		sSkipMenu[0].sTxtLabel = "GO TO TRUCK"
		sSkipMenu[1].sTxtLabel = "TOWING"
	#ENDIF
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC Script_Cleanup()

	// Clear the mission title
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()

	MISSION_DOORS_SET_DOOR_STATE(sMyDoors[TOWING_GATES_EAST], FALSE)
	MISSION_DOORS_SET_DOOR_STATE(sMyDoors[TOWING_GATES_SOUTH], FALSE)

	MISSION_DOORS_CLEANUP_DOOR(sMyDoors[TOWING_GATES_EAST])
	MISSION_DOORS_CLEANUP_DOOR(sMyDoors[TOWING_GATES_SOUTH])

	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()

	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required")
	ENDIF

	// Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	
	// Release tow truck
	SAFE_RELEASE_VEHICLE(vehTruck)

	// Remove blips
	IF DOES_BLIP_EXIST(myVehicleBlip)
		REMOVE_BLIP(myVehicleBlip)
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbi01)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi02)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi03) // Garage in northern part of the city
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA((vTonya2CarGenPosition - <<15,15,15>>), (vTonya2CarGenPosition + <<15,15,15>>), TRUE)
	
	MISSION_CLEANUP(sLoadQueue)
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------
PROC Script_Passed()
	
	REQUEST_SCRIPT("ambient_TonyaCall2")
	
	IF HAS_SCRIPT_LOADED("ambient_TonyaCall2")

		Random_Character_Passed(CP_OJ_TOW2)
		
		START_NEW_SCRIPT("ambient_TonyaCall2", DEFAULT_STACK_SIZE) 
		SET_SCRIPT_AS_NO_LONGER_NEEDED("ambient_TonyaCall2")
		CPRINTLN(DEBUG_MISSION, "STARTING SCRIPT - ambient_TonyaCall2")
		
		Script_Cleanup()
	ENDIF
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

PROC DO_FADE_OUT_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC SETUP_TONYA_AI()
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		IF NOT IS_PED_IN_GROUP(pedTonya)
			SET_PED_AS_GROUP_MEMBER(pedTonya, PLAYER_GROUP_ID())
			SET_PED_NEVER_LEAVES_GROUP(pedTonya, TRUE)
			SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedTonya, VS_FRONT_RIGHT)
//			SET_GROUP_FORMATION(PLAYER_GROUP_ID(), FORMATION_SURROUND_FACING_INWARDS)
			SET_PED_CONFIG_FLAG(pedTonya, PCF_ForcedToUseSpecificGroupSeatIndex, TRUE)
			SET_GROUP_FORMATION_SPACING(PLAYER_GROUP_ID(), 2.0)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				TASK_LOOK_AT_ENTITY(pedTonya, PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
			ENDIF
			CPRINTLN(DEBUG_MISSION, "CALLING - SETUP_TONYA_AI")
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_FIRST_RETRY_SETUP()
	CPRINTLN(DEBUG_MISSION, "INSIDE - HANDLE_FIRST_RETRY_SETUP")

	HANDLE_REPLAY_VEHICLE(vVehicleStartPosition, fVehicleStartHeading)
			
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) 
		IF NOT IS_REPLAY_BEING_SET_UP()
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerFirstCheckpointPosition)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerFirstCheckpointHeading)
			CPRINTLN(DEBUG_MISSION, "SETTING PLAYER'S POSITION AND HEADING")
		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedTonya)
		REQUEST_MODEL(mnTonya)
		
		WHILE NOT HAS_MODEL_LOADED(mnTonya)
			WAIT(0)
			CPRINTLN(DEBUG_MISSION, "WAITING ON TONYA MODEL TO LOAD")
		ENDWHILE
			
		pedTonya = CREATE_PED(PEDTYPE_CIVFEMALE, mnTonya, vTonyaFirstCheckpointPosition, fTonyaFirstCheckpointHeading)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTonya, TRUE)
	
		SET_PED_COMPONENT_VARIATION(pedTonya, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedTonya, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedTonya, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedTonya, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedTonya, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
		
		CPRINTLN(DEBUG_MISSION, "CREATING NEW TONYA - STAGE 0")
		ADD_PED_FOR_DIALOGUE(pedConvo, 3, pedTonya, "TONYA")
		SET_PED_MODEL_IS_SUPPRESSED(mnTonya, TRUE)
		
		ADD_RELATIONSHIP_GROUP("instructorRelGroup", relInstructor)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedTonya, relInstructor)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT, relInstructor, RELGROUPHASH_PLAYER)
		
		SET_ENTITY_COORDS(pedTonya, vTonyaFirstCheckpointPosition)
		SET_ENTITY_HEADING(pedTonya, fTonyaFirstCheckpointHeading)
		CPRINTLN(DEBUG_MISSION, "SETTING TONYA'S POSITION AND HEADING")
	ENDIF
ENDPROC

PROC HANDLE_SECOND_RETRY_SETUP()
	CPRINTLN(DEBUG_MISSION, "INSIDE - HANDLE_SECOND_RETRY_SETUP")
	
	HANDLE_REPLAY_VEHICLE(<<405.7806, -1652.3694, 28.2928>>, 139.8366)

	IF NOT DOES_ENTITY_EXIST(pedTonya)
		REQUEST_MODEL(mnTonya)
		REQUEST_MODEL(TOWTRUCK)
		REQUEST_VEHICLE_ASSET(TOWTRUCK)
		
		WHILE NOT HAS_MODEL_LOADED(mnTonya)
		OR NOT HAS_MODEL_LOADED(TOWTRUCK)
		OR NOT HAS_VEHICLE_ASSET_LOADED(TOWTRUCK)
			WAIT(0)
			CPRINTLN(DEBUG_MISSION, "WAITING ON TONYA MODEL TO LOAD")
		ENDWHILE
			
		pedTonya = CREATE_PED(PEDTYPE_CIVFEMALE, mnTonya, vTonyaFirstCheckpointPosition, fTonyaFirstCheckpointHeading)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTonya, TRUE)
		
		IF NOT DOES_ENTITY_EXIST(myVehicle)
			myVehicle = CREATE_VEHICLE(TOWTRUCK, vTruckPos, fTruckHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(myVehicle)
			CPRINTLN(DEBUG_MISSION, "CREATING TRUCK VIA HANDLE_SECOND_RETRY_SETUP")
		ENDIF
		
		SET_PED_COMPONENT_VARIATION(pedTonya, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedTonya, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedTonya, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedTonya, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedTonya, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
		CPRINTLN(DEBUG_MISSION, "CREATING NEW TONYA - STAGE 1")
		
		ADD_PED_FOR_DIALOGUE(pedConvo, 3, pedTonya, "TONYA")
		SET_PED_MODEL_IS_SUPPRESSED(mnTonya, TRUE)
		
		ADD_RELATIONSHIP_GROUP("instructorRelGroup", relInstructor)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedTonya, relInstructor)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT, relInstructor, RELGROUPHASH_PLAYER)
		
		SETUP_TONYA_AI()
	ENDIF
		
ENDPROC

PROC HANDLE_THIRD_RETRY_SETUP()
	CPRINTLN(DEBUG_MISSION, "INSIDE - HANDLE_THIRD_RETRY_SETUP")
	
	IF NOT DOES_ENTITY_EXIST(myVehicle)
		REQUEST_MODEL(TOWTRUCK)
		REQUEST_VEHICLE_ASSET(TOWTRUCK)
		
		WHILE NOT HAS_MODEL_LOADED(TOWTRUCK)
		OR NOT HAS_VEHICLE_ASSET_LOADED(TOWTRUCK)
			WAIT(0)
			CPRINTLN(DEBUG_MISSION, "WAITING ON TOWTRUCK MODEL TO LOAD")
		ENDWHILE
			
		IF NOT DOES_ENTITY_EXIST(myVehicle)
			myVehicle = CREATE_VEHICLE(TOWTRUCK, vTruckPos, fTruckHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(myVehicle)
			CPRINTLN(DEBUG_MISSION, "CREATING TRUCK VIA HANDLE_THIRD_RETRY_SETUP")
			SET_VEHICLE_COLOUR_COMBINATION(myVehicle, 1)
			CPRINTLN(DEBUG_MISSION, "SETTING VEHICLE COLOR COMBINATION = 1")
		ENDIF
	ENDIF
ENDPROC

PROC GO_TO_CHECKPOINT_1()
	IF IS_SCREEN_FADED_OUT()
	
		#IF IS_DEBUG_BUILD
			IF bDoingPSkip //OR bDoingJSkip
				
				CPRINTLN(DEBUG_MISSION, "DEBUG: GO_TO_CHECKPOINT_1")
				CLEAR_PRINTS()
				CLEAR_HELP()
				KILL_ANY_CONVERSATION()
				
				IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
					CLEAR_PED_TASKS_IMMEDIATELY(pedTonya)
					DELETE_PED(pedTonya)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - pedTonya")
				ELSE
					IF DOES_ENTITY_EXIST(pedTonya)
						CPRINTLN(DEBUG_MISSION, "TONYA EXISTS")
					ENDIF
					IF NOT IS_ENTITY_DEAD(pedTonya)
						CPRINTLN(DEBUG_MISSION, "TONYA IS NOT DEAD")
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
				IF DOES_BLIP_EXIST(myVehicleBlip)
					REMOVE_BLIP(myVehicleBlip)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - myVehicleBlip")
				ENDIF
				IF DOES_ENTITY_EXIST(vehTruck)
					DELETE_VEHICLE(vehTruck)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - vehTruck")
				ENDIF
				IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
					REMOVE_BLIP(brokenVehicles[0].blipIndex)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - brokenVehicles[0].blipIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
					DELETE_VEHICLE(brokenVehicles[0].vehicleIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - brokenVehicles[0].vehicleIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
					DELETE_PED(brokenVehicles[0].pedIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndexPass)
					DELETE_PED(brokenVehicles[0].pedIndexPass)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndexPass")
				ENDIF
				
				curBrokenCarCount = 0
				curCarNameIdx = 0
				bSpawnPed = FALSE
				launchArgs.bInit = TRUE
				curTutState = TOW_TUTORIAL_VISIBLE_DIA
				curStage = MISSION_STATE_INTRO
				
				bDoingPSkip = FALSE
			ENDIF
		#ENDIF
		
		HANDLE_FIRST_RETRY_SETUP()
			
		CLEAR_AREA(vPlayerFirstCheckpointPosition, 8.0, TRUE)
		
		END_REPLAY_SETUP()
		
		SETUP_TONYA_AI()
		
		DO_SCREEN_FADE_IN(500)
		
		m_eState = MS_GO_TO_TRUCK
		CPRINTLN(DEBUG_MISSION, "REPLAY STAGE 1: GOING TO STATE - MS_GO_TO_TRUCK")
	ENDIF
ENDPROC

PROC GO_TO_CHECKPOINT_2()
	IF IS_SCREEN_FADED_OUT()
	
		#IF IS_DEBUG_BUILD
			IF bDoingPSkip //OR bDoingJSkip
				
				CPRINTLN(DEBUG_MISSION, "DEBUG: GO_TO_CHECKPOINT_2")
				
				IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
					CLEAR_PED_TASKS_IMMEDIATELY(pedTonya)
					DELETE_PED(pedTonya)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - pedTonya")
				ENDIF
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
				IF DOES_BLIP_EXIST(myVehicleBlip)
					REMOVE_BLIP(myVehicleBlip)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - myVehicleBlip")
				ENDIF
				IF DOES_ENTITY_EXIST(vehTruck)
					DELETE_VEHICLE(vehTruck)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - vehTruck")
				ENDIF
				IF DOES_ENTITY_EXIST(myVehicle)
					DELETE_VEHICLE(myVehicle)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - myVehicle")
				ENDIF
				IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
					REMOVE_BLIP(brokenVehicles[0].blipIndex)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - brokenVehicles[0].blipIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
					DELETE_VEHICLE(brokenVehicles[0].vehicleIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - brokenVehicles[0].vehicleIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
					DELETE_PED(brokenVehicles[0].pedIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndexPass)
					DELETE_PED(brokenVehicles[0].pedIndexPass)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndexPass")
				ENDIF
				
				myVehicle = CREATE_VEHICLE(TOWTRUCK, vTruckPos, fTruckHeading)
				SET_VEHICLE_ON_GROUND_PROPERLY(myVehicle)
				
				curBrokenCarCount = 0
				curCarNameIdx = 0
				bSpawnPed = FALSE
				curTutState = TOW_TUTORIAL_VISIBLE_DIA
				curStage = MISSION_STATE_INTRO
				
				launchArgs.bInit = TRUE
				
				CPRINTLN(DEBUG_MISSION, "CALLING - HANLDE_PLACING_PLAYER_AND_TONYA_INTO_TOWTRUCK_AND_MOVING")
				HANLDE_PLACING_PLAYER_AND_TONYA_INTO_TOWTRUCK_AND_MOVING()
				
				bDoingPSkip = FALSE
			ENDIF
		#ENDIF
			
		HANDLE_SECOND_RETRY_SETUP()
			
		CLEAR_AREA(vPlayerFirstCheckpointPosition, 8.0, TRUE)
		
		END_REPLAY_SETUP(myVehicle)
		
		IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		AND NOT IS_ENTITY_DEAD(myVehicle)
			SET_PED_INTO_VEHICLE(pedTonya, myVehicle, VS_FRONT_RIGHT)
			CPRINTLN(DEBUG_MISSION, "SETTING TONYA INTO THE TRUCK")
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTonya, TRUE)
		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(-138.0317)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-0.1190)
		CPRINTLN(DEBUG_MISSION, "SETTING GAMEPLAY CAM RELATIVE HEADING = -138.0317")
		CPRINTLN(DEBUG_MISSION, "SETTING GAMEPLAY CAM RELATIVE PITCH = -0.1190")
		
		DO_SCREEN_FADE_IN(500)
		
		m_eState = MS_DO_MISSION
		CPRINTLN(DEBUG_MISSION, "REPLAY STAGE 2: GOING TO STATE - MS_DO_MISSION")
	ELSE
		CPRINTLN(DEBUG_MISSION, "SCREEN IS NOT FADED OUT")
	ENDIF
ENDPROC

PROC GO_TO_CHECKPOINT_3()
	IF IS_SCREEN_FADED_OUT()
	
		STOP_SCRIPTED_CONVERSATION(FALSE)
		CLEAR_PRINTS()
		CLEAR_HELP()
	
		#IF IS_DEBUG_BUILD
			IF bDoingPSkip //OR bDoingJSkip
				
				CPRINTLN(DEBUG_MISSION, "DEBUG: GO_TO_CHECKPOINT_2")
				
				IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
					CLEAR_PED_TASKS_IMMEDIATELY(pedTonya)
					DELETE_PED(pedTonya)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - pedTonya")
				ENDIF
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
				IF DOES_BLIP_EXIST(myVehicleBlip)
					REMOVE_BLIP(myVehicleBlip)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - myVehicleBlip")
				ENDIF
				IF DOES_ENTITY_EXIST(vehTruck)
					DELETE_VEHICLE(vehTruck)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - vehTruck")
				ENDIF
				IF DOES_BLIP_EXIST(brokenVehicles[0].blipIndex)
					REMOVE_BLIP(brokenVehicles[0].blipIndex)
					CPRINTLN(DEBUG_MISSION, "REMOVING BLIP - brokenVehicles[0].blipIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].vehicleIndex)
					DELETE_VEHICLE(brokenVehicles[0].vehicleIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING VEHICLE - brokenVehicles[0].vehicleIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndex)
					DELETE_PED(brokenVehicles[0].pedIndex)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndex")
				ENDIF
				IF DOES_ENTITY_EXIST(brokenVehicles[0].pedIndexPass)
					DELETE_PED(brokenVehicles[0].pedIndexPass)
					CPRINTLN(DEBUG_MISSION, "DELETING PED - brokenVehicles[0].pedIndexPass")
				ENDIF
				
				bDoingPSkip = FALSE
			ENDIF
		#ENDIF
		
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TON2_UNHOOK)
		CPRINTLN(DEBUG_MISSION, "USING SHIT SKIP - NEGATING UNHOOK BONUS")
		
		IF NOT IS_REPLAY_BEING_SET_UP()
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vSkipSpawnPosition)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fSkipSpawnHeading)
			ENDIF
		ENDIF
		
		HANDLE_THIRD_RETRY_SETUP()
		
		CLEAR_AREA(vPlayerFirstCheckpointPosition, 8.0, TRUE)
		
		END_REPLAY_SETUP()
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		
		DO_SCREEN_FADE_IN(500)
		
		CPRINTLN(DEBUG_MISSION, "TONYA 2: GOING TO STATE - MS_PASSED")
		m_eState = MS_PASSED
	ELSE
		CPRINTLN(DEBUG_MISSION, "SCREEN IS NOT FADED OUT")
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	
	PROC JUMP_TO_STAGE(eRC_MainState stage, BOOL bIsDebugJump = FALSE)
		DO_FADE_OUT_WITH_WAIT()
		
		//Update mission checkpoint in case they skipped the stages where it gets set.
		IF bIsDebugJump
		
			SWITCH STAGE
				CASE MS_GO_TO_TRUCK
					GO_TO_CHECKPOINT_1()
				BREAK
				CASE MS_DO_MISSION
					GO_TO_CHECKPOINT_2()
				BREAK
			ENDSWITCH

		ENDIF
	ENDPROC
	
	// PURPOSE:	Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()
	
		eRC_MainState eStage
		
		IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iDebugJumpStage)
			IF iDebugJumpStage = 0
				eStage = MS_GO_TO_TRUCK
			ELIF iDebugJumpStage = 1
				eStage = MS_DO_MISSION
			ENDIF
			bDoingPSkip = TRUE
			JUMP_TO_STAGE(eStage, TRUE)
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
		
			CPRINTLN(DEBUG_MISSION, "USING P-SKIP")
			FADE_DOWN()
			bDoingPSkip = TRUE
			
			IF m_eState < MS_DO_MISSION AND (curStage <= MISSION_STATE_CHECK_DISTANCE)
				GO_TO_CHECKPOINT_1() 
				CPRINTLN(DEBUG_MISSION, "P-SKIP: GO_TO_CHECKPOINT_1")
			ELIF m_eState = MS_DO_MISSION AND (curStage <= MISSION_STATE_CHECK_DISTANCE)
				GO_TO_CHECKPOINT_1() 
				CPRINTLN(DEBUG_MISSION, "P-SKIP: GO_TO_CHECKPOINT_1 - 2")
			ELIF m_eState = MS_DO_MISSION AND (curStage > MISSION_STATE_CHECK_DISTANCE)
				GO_TO_CHECKPOINT_2()
				CPRINTLN(DEBUG_MISSION, "P-SKIP: GO_TO_CHECKPOINT_2")
			ENDIF
		ENDIF

		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			IF IS_CUTSCENE_ACTIVE()
				STOP_CUTSCENE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				CLEAR_HELP()
				
				SET_WIDESCREEN_BORDERS(FALSE, 0)
			ENDIF
			CLEAR_PRINTS()
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			CLEAR_HELP()
			CLEAR_PRINTS()
			WAIT_FOR_CUTSCENE_TO_STOP()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
		
		// J-Skip
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			CLEAR_HELP()
			CLEAR_PRINTS()
			
			IF m_eState < MS_DO_MISSION
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
				AND DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
					SET_PED_INTO_VEHICLE(pedTonya, myVehicle, VS_FRONT_RIGHT)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

PROC HANDLE_TONYA_FAIL_CONDITIONS()
	
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya) AND curStage < MISSION_STATE_POST_DROPOFF
	
		IF m_eState < MS_DO_MISSION
			IF HAS_PLAYER_ABANDONED_TONYA(FALSE)
				bAbandonedTonya = TRUE
				CPRINTLN(DEBUG_MISSION, "PLAYER HAS ABANDONED TONYA - POST TUTORIAL")
			ENDIF
		ELSE
			IF HAS_PLAYER_ABANDONED_TONYA()
				bAbandonedTonya = TRUE
				CPRINTLN(DEBUG_MISSION, "PLAYER HAS ABANDONED TONYA - POST TUTORIAL")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HANDLE_FAIL_CONDITIONS()
//	DEBUG_MESSAGE("HANDLE_FAIL_CONDITIONS()")

	IF IS_CUTSCENE_PLAYING()
//		CPRINTLN(DEBUG_MISSION, "EXITING FAILING CONDITIONS EARLY, BECAUSE A CUT SCENE IS PLAYING")
		RETURN FALSE
	ENDIF
	
	IF m_eState != MS_FAILED
		HANDLE_TONYA_FAIL_CONDITIONS()
		
		IF NOT IS_ENTITY_DEAD(pedTonya)
			IF IS_PED_BEING_JACKED(pedTonya)
				IF GET_PEDS_JACKER(pedTonya) = PLAYER_PED_ID()
					sFailReason = "TOW_FAIL_13"
					CPRINTLN(DEBUG_MISSION, "PLAY LINE, TOW_FAIL_13 - PLAYER JACKING TONYA")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bAbandonedTonya
			sFailReason = "TOW_FAIL_14"
			CPRINTLN(DEBUG_MISSION, "bAbandonedTonya IS TRUE")
			RETURN TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedTonya)
			IF IS_ENTITY_DEAD(pedTonya)
				sFailReason = "TOW_FAIL_13b"
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF HAS_PLAYER_THREATENED_PED(pedTonya, FALSE, 0,10, TRUE)
				CPRINTLN(DEBUG_MISSION, "PLAYER HAS THREATENED TONYA")
				
				KILL_ANY_CONVERSATION()
				
				IF NOT IS_PED_INJURED(pedTonya)
					TASK_SMART_FLEE_PED(pedTonya, PLAYER_PED_ID(), 1000, -1)
					SET_PED_KEEP_TASK(pedTonya, TRUE)
					CPRINTLN(DEBUG_MISSION, "TASKING TONYA TO FLEE")
				ENDIF
				
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedTonya, PLAYER_PED_ID())
					sFailReason = "TOW_FAIL_13a"
					CPRINTLN(DEBUG_MISSION, "THE PLAYER HAS HURT TONYA - PLAY LINE, TOW_FAIL_13a")
				ELIF IS_PED_SHOOTING(PLAYER_PED_ID())
					sFailReason = "TOW_FAIL_18"
					CPRINTLN(DEBUG_MISSION, "PLAY LINE, TOW_FAIL_18")
				ELSE
					sFailReason = "TOW_FAIL_13"
					CPRINTLN(DEBUG_MISSION, "DEFAULT: PLAY LINE, TOW_FAIL_13")
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF bFailRc //IS_BITMASK_AS_ENUM_SET(launchArgs.iTowFlags, TOWING_FLAG_FAILED)
			DEBUG_MESSAGE("Fail detected")
			sFailReason = strFailReason
			RETURN TRUE
			//TODO DAVE: We also need to pass back the failReason so we can print the right string
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

/// PURPOSE: 
///   Handle mission failed
PROC STATE_Failed()

	SWITCH m_FailState
	
		CASE SS_FAIL_SETUP
			CPRINTLN(DEBUG_MISSION, "INSIDE FAIL SETUP")
			
			STOP_SCRIPTED_CONVERSATION(FALSE)
			
			// Update with fail reason
			IF ARE_STRINGS_EQUAL(sFailReason, "DEFAULT")
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason, TRUE)
			ENDIF
			m_FailState = SS_FAIL_UPDATE
		BREAK

		CASE SS_FAIL_UPDATE
			CPRINTLN(DEBUG_MISSION, "INSIDE FAIL UPDATE")
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				Script_Cleanup()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Make sure that the player's last vehicle is set as a mission entity so that it wont get cleared up aggeressively by garbage collection while on the mission
PROC SAVE_LAST_PLAYER_VEHICLE()
	VEHICLE_INDEX vehLast
	
	IF m_eState > MS_MEET_TONYA
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vehLast = GET_LAST_DRIVEN_VEHICLE()
			
			IF DOES_ENTITY_EXIST(vehLast)
				IF IS_VEHICLE_DRIVEABLE(vehLast)
					IF vehLast != viPlayerLastVehicle
					AND NOT IS_ENTITY_IN_WATER(vehLast)
					AND NOT IS_ENTITY_A_MISSION_ENTITY(vehLast)
						
						//clear out the previous one
						IF viPlayerLastVehicle != NULL
							IF IS_ENTITY_A_MISSION_ENTITY(viPlayerLastVehicle)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(viPlayerLastVehicle)
							ENDIF
						ENDIF
						
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							//set the new vehicle as the current mission entity vehicle
							viPlayerLastVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							SET_ENTITY_AS_MISSION_ENTITY(viPlayerLastVehicle)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_PLAYER_BEING_TOO_CLOSE()
	VECTOR vPlayerPosition, vTonyaPosition
	FLOAT fTempDistance
	
	IF m_eSubState < SS_UPDATE
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		IF NOT IS_ENTITY_DEAD(pedTonya)
			vTonyaPosition = GET_ENTITY_COORDS(pedTonya)
		ENDIF
		
		fTempDistance = VDIST(vPlayerPosition, vTonyaPosition)
		PRINTLN("fTempDistance = ", fTempDistance)
		
		IF fTempDistance < 3.0
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedTonya, -1, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
				TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), pedTonya)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///   Temp cutscene where Franklin meets Tonya
PROC STATE_MeetTonya()

	HANDLE_PLAYER_BEING_TOO_CLOSE()

	SWITCH m_eSubState
	
		CASE SS_SETUP
			
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				bIsPlayingConvo = TRUE
				CPRINTLN(DEBUG_MISSION, "bIsPlayingConvo = TRUE")
			ENDIF
			
			IF NOT IS_GAMEPLAY_HINT_ACTIVE()
				IF NOT IS_PED_INJURED(pedTonya)
					SET_GAMEPLAY_ENTITY_HINT(pedTonya, <<0,0,0>>, TRUE, -1, 3000)
					SET_GAMEPLAY_HINT_FOV(25.0)
					SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(-0.7)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.0)
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
				ENDIF
			ELSE
				STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
			ENDIF
			
			IF NOT IS_TIMER_STARTED(tLeadInTimer)
				START_TIMER_NOW(tLeadInTimer)
				CPRINTLN(DEBUG_MISSION, "STARTING TIMER - tLeadInTimer")
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedTonya, -1, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
			ENDIF

			IF NOT IS_ENTITY_DEAD(pedTonya)
				TASK_LOOK_AT_ENTITY(pedTonya, PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
			ENDIF
			
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			
			REQUEST_CUTSCENE("tonya_mcs_2")
			RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
			
			CPRINTLN(DEBUG_MISSION, "GOING TO STATE - SS_CUTSCENE_LEADIN")
			m_eSubState = SS_CUTSCENE_LEADIN
		BREAK
		
		CASE SS_CUTSCENE_LEADIN
			IF IS_TIMER_STARTED(tLeadInTimer)
				IF GET_TIMER_IN_SECONDS(tLeadInTimer) > 3.0
				OR ( bIsPlayingConvo AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() )
				OR ( GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedTonya) < 3.5 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() )
					CPRINTLN(DEBUG_MISSION, "GOING TO STATE - SS_CUTSCENE_SETUP")
					m_eSubState = SS_CUTSCENE_SETUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CUTSCENE_SETUP

			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
				
				CLEAR_HELP(TRUE)
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY) //, PLAYER_ONE)
				ENDIF
				IF NOT IS_ENTITY_DEAD(sRCLauncherDataLocal.pedID[0])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[0], "Tonya", CU_ANIMATE_EXISTING_SCRIPT_ENTITY) //, PLAYER_ONE)
					CPRINTLN(DEBUG_MISSION, "REGISTERING TONYA FOR CUTSCENE")
				ENDIF
				
				KILL_FACE_TO_FACE_CONVERSATION()
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
					CPRINTLN(DEBUG_MISSION, "TURNING OFF STEALTH MODE")
				ENDIF
				
				iNavmeshBlocking = ADD_NAVMESH_BLOCKING_OBJECT(vCutscenePosition, <<5,5,5>>, 0)
				
				RC_CLEANUP_LAUNCHER()
				START_CUTSCENE()
				CPRINTLN(DEBUG_MISSION, "STARTING CUTSCENE - tonya_mcs_2")
				
				IF IS_REPEAT_PLAY_ACTIVE()
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(1500)
					ENDIF
				ENDIF				
				m_eSubState = SS_UPDATE
			ELSE	
				REQUEST_CUTSCENE("tonya_mcs_2")
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			
			IF IS_CUTSCENE_PLAYING()
			
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT()
				ENDIF
			
				IF NOT bMovedCar
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-11.120177,-1476.167603,28.534128>>, <<-18.045006,-1470.340576,34.634628>>, 7.000000, vVehicleStartPosition, fVehicleStartHeading, <<3.55, 10.59, 5.55>>)
					CPRINTLN(DEBUG_MISSION, "MOVING VEHICLE AT MISSION START")
					bMovedCar = TRUE
				ENDIF
			
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
				
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
//					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 2000)
				ENDIF
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tonya")
				// Setup peds for dialogue
				IF NOT IS_ENTITY_DEAD(pedTonya)
					FORCE_PED_MOTION_STATE(pedTonya, MS_ON_FOOT_WALK)
					CPRINTLN(DEBUG_MISSION, "MAKING TONYA GO FORWARD")
				ENDIF
			ENDIF
				
			IF HAS_CUTSCENE_FINISHED()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_ENTITY_PROOFS(pedTonya, FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_PED_CAN_RAGDOLL(pedTonya, TRUE)
				
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
				IF IS_SCREEN_FADED_OUT()
					WAIT(0)
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				CPRINTLN(DEBUG_MISSION, "CUTSCENE HAS FINISHED")
				m_eSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT()
			ENDIF
		
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
			DISABLE_CELLPHONE(FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
			
			REMOVE_NAVMESH_BLOCKING_OBJECT(iNavmeshBlocking)
			CPRINTLN(DEBUG_MISSION, "REMOVING iNavmeshBlocking")

			// Reset cam
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			CPRINTLN(DEBUG_MISSION, "INSIDE - SS_CLEANUP VIA STATE_MeetTonya")
			
			// Update state
			m_eState = MS_GO_TO_TRUCK
			m_eSubState = SS_SETUP
		BREAK
	ENDSWITCH
ENDPROC

PROC SAFELY_REMOVE_TONYA()
	VECTOR vPlayerPosition, vTonyaPosition

	IF NOT DOES_ENTITY_EXIST(pedTonya)
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	IF DOES_ENTITY_EXIST(pedTonya) AND NOT IS_ENTITY_DEAD(pedTonya)
		vTonyaPosition = GET_ENTITY_COORDS(pedTonya)
	ENDIF

	IF VDIST2(vPlayerPosition, vTonyaPosition) > 22500
		IF IS_ENTITY_OCCLUDED(pedTonya)
			DELETE_PED(pedTonya)
			CPRINTLN(DEBUG_MISSION, "DELETING PED - pedTonya")
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedTonya)
			CPRINTLN(DEBUG_MISSION, "SETTING PED AS NO LONGER NEEEDED- pedTonya")
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_TONYA_DIALOGUE()

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				STOP_SCRIPTED_CONVERSATION(TRUE)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF bCurrentlyTonyaAbandoned
		EXIT
	ENDIF
	
	SWITCH iTonyaDialogueStages
		CASE 0
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_TIMER_STARTED(tDialogueBufferTimer)
					IF GET_TIMER_IN_SECONDS(tDialogueBufferTimer) > 5.0
					AND GET_TIMER_IN_SECONDS(tDialogueBufferTimer) < 15.0
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())AND NOT IS_ENTITY_DEAD(pedTonya)
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
									IF NOT bTonyaGotCarDialogue
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_CAR3", CONV_PRIORITY_HIGH)
												CPRINTLN(DEBUG_MISSION, "TONYA - GOT CAR CONVO")
												bTonyaGotCarDialogue = TRUE
											ENDIF
										ENDIF
									ENDIF
								ELSE
//									CPRINTLN(DEBUG_MISSION, "WE'VE BEEN A CAR AT SOME POINT - 01")
									bTonyaGotCarDialogue = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELIF GET_TIMER_IN_SECONDS(tDialogueBufferTimer) > 20.0
						IF NOT bTonyaGotCarDialogue01
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())AND NOT IS_ENTITY_DEAD(pedTonya)
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_CAR4", CONV_PRIORITY_HIGH)
											CPRINTLN(DEBUG_MISSION, "TONYA - GOT CAR CONVO2")
											bTonyaGotCarDialogue01 = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			IF NOT bTonyaCarJackingConvo
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_JACKING(PLAYER_PED_ID())
						IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_JACK2", CONV_PRIORITY_HIGH)
							CPRINTLN(DEBUG_MISSION, "TONYA - JACKING CAR CONVO")
							bTonyaCarJackingConvo = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())AND NOT IS_ENTITY_DEAD(pedTonya)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_PED_IN_ANY_VEHICLE(pedTonya)
				OR (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_PED_IN_ANY_VEHICLE(pedTonya) AND GET_TIMER_IN_SECONDS(tDialogueBufferTimer) > 30.0)
					CPRINTLN(DEBUG_MISSION, "iTonyaDialogueStages = 1")
					iTonyaDialogueStages = 1
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_TALK2", CONV_PRIORITY_HIGH)
					CPRINTLN(DEBUG_MISSION, "iTonyaDialogueStages = 2")
					iTonyaDialogueStages = 2
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------
		CASE 2
		
		BREAK
	ENDSWITCH
ENDPROC

PROC HANDLE_TONYA_GOING_TO_TRUCK()
	SEQUENCE_INDEX iSeq

	IF NOT bTaskedTonyaToEnterTruck
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(pedTonya) AND DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
					IF IS_PED_IN_GROUP(pedTonya)
						REMOVE_PED_FROM_GROUP(pedTonya)
						CPRINTLN(DEBUG_MISSION, "REMOVING TONYA FROM PLAYER GROUND")
					ENDIF
				
					OPEN_SEQUENCE_TASK(iSeq)
						IF IS_PED_IN_ANY_VEHICLE(pedTonya)
							TASK_LEAVE_ANY_VEHICLE(NULL)
						ENDIF
						TASK_ENTER_VEHICLE(NULL, myVehicle, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
					CLOSE_SEQUENCE_TASK(iSeq)
					TASK_PERFORM_SEQUENCE(pedTonya, iSeq)
					CPRINTLN(DEBUG_MISSION, "TASKING TONYA TO ENTER TRUCK")
					CLEAR_SEQUENCE_TASK(iSeq)
					bTaskedTonyaToEnterTruck = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// To Fix Bug # 1895550
	IF bTaskedTonyaToEnterTruck
		IF NOT IS_PED_INJURED(pedTonya) AND NOT IS_ENTITY_DEAD(myVehicle)
			IF NOT IS_PED_IN_VEHICLE(pedTonya, myVehicle)
				IF GET_SCRIPT_TASK_STATUS(pedTonya, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedTonya, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
					TASK_ENTER_VEHICLE(pedTonya, myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
					CPRINTLN(DEBUG_MISSION, "TASKING TONYA TO ENTER THE TRUCK AGAIN")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///   Franklin heads to truck with Tonya
PROC STATE_GoToTruck()
		
	SWITCH m_eSubState
		
		CASE SS_SETUP
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "towing_tonya_franklin_travel")
			
			ADD_PED_FOR_DIALOGUE(pedConvo, 1, PLAYER_PED_ID(), "FRANKLIN")
			ADD_PED_FOR_DIALOGUE(pedConvo, 3, pedTonya, "TONYA")
					
			// Request tow truck
			REQUEST_MODEL(TOWTRUCK)
					
			// Blip truck location
			IF NOT DOES_BLIP_EXIST(myVehicleBlip)
				myVehicleBlip = ADD_BLIP_FOR_COORD(<<401.6370, -1633.3003, 28.2928>>)
				SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
				SET_BLIP_ROUTE(myVehicleBlip, TRUE)
			ENDIF
			
			IF NOT IS_TIMER_STARTED(tDialogueBufferTimer)
				START_TIMER_NOW(tDialogueBufferTimer)
				CPRINTLN(DEBUG_MISSION, "STARTING TIMER - tDialogueBufferTimer")
			ELSE
				RESTART_TIMER_NOW(tDialogueBufferTimer)
				CPRINTLN(DEBUG_MISSION, "RESTARTING TIMER - tDialogueBufferTimer")
			ENDIF
		
			CPRINTLN(DEBUG_MISSION, "GOING TO STATE - SS_UPDATE VIA STATE_GoToTruck")
			m_eSubState = SS_UPDATE
		BREAK
			
		CASE SS_UPDATE
		
			HANDLE_GATE_OPENING()
		
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_ForcePlayerToEnterVehicleThroughDirectDoorOnly,TRUE)
			ENDIF
			
			IF NOT bPrintFirstObjective AND NOT bCurrentlyTonyaAbandoned
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					// Set objective
					PRINT("TOW_TUT_INTRUCK", DEFAULT_GOD_TEXT_TIME, 1)
					bPrintFirstObjective = TRUE
					CPRINTLN(DEBUG_MISSION, "bPrintFirstObjective = TRUE")
				ENDIF
			ENDIF
			
			IF NOT bSetupTonyaAI
				SETUP_TONYA_AI()
				CPRINTLN(DEBUG_MISSION, "bSetupTonyaAI = TRUE")
				bSetupTonyaAI = TRUE
			ENDIF
		
			// Grab the vehicle the player is using to get to the impound lot to be saved post mission.
			IF NOT DOES_ENTITY_EXIST(vehReplayVehicle)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						vehReplayVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						SET_ENTITY_AS_MISSION_ENTITY(vehReplayVehicle, TRUE, TRUE)
						SET_PLAYERS_LAST_VEHICLE(vehReplayVehicle)
						CPRINTLN(DEBUG_MISSION, "FOUND REPLAY VEHICLE")
						
//						IF DOES_ENTITY_EXIST(vehReplayVehicle) AND NOT IS_ENTITY_DEAD(vehReplayVehicle)
//							CPRINTLN(DEBUG_MISSION, "OVERRIDING REPLAY VEHICLE")
//							OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vehReplayVehicle)
//						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT bTaskedTonyaToEnterTruck
				HANDLE_TONYA_BLIPPING_TO_ENTER_CAR(vehTruck)
			ENDIF
		
			// Attempt to create tow truck
			IF NOT DOES_ENTITY_EXIST(vehTruck)
			
				REQUEST_MODEL(TOWTRUCK)
				REQUEST_VEHICLE_ASSET(TOWTRUCK)
			
				IF HAS_MODEL_LOADED(TOWTRUCK)
				AND HAS_VEHICLE_ASSET_LOADED(TOWTRUCK)
				
					vehTruck = CREATE_VEHICLE(TOWTRUCK, vTruckPos, fTruckHeading)
					myVehicle = vehTruck
					SET_ENTITY_AS_MISSION_ENTITY(myVehicle)
					
					SET_VEH_RADIO_STATION(myVehicle, "RADIO_03_HIPHOP_NEW")
					CPRINTLN(DEBUG_MISSION, "SETTING STATION - RADIO_03_HIPHOP_NEW")
					
					CPRINTLN(DEBUG_MISSION, "CREATING TRUCK VIA STATE_GoToTruck")
					CLEAR_AREA(vTruckPos, 25.0, TRUE, TRUE)
					
					IF DOES_ENTITY_EXIST(vehTruck)
						
						// Remove blip for coord...
						REMOVE_BLIP(myVehicleBlip)
						
						// Update blip for truck
						myVehicleBlip = ADD_BLIP_FOR_ENTITY(vehTruck)
						SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
						SET_BLIP_ROUTE(myVehicleBlip, TRUE)
						
						CPRINTLN(DEBUG_MISSION, "ADDING BLIP TO TRUCK")
						// Release model
						SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
					ENDIF
				ENDIF
			ENDIF
			
			// Handle truck conversation
			IF NOT IS_ENTITY_DEAD(pedTonya)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedTonya)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<423.742371,-1652.842163,27.268002>>, <<397.839600,-1630.331665,40.292778>>, 42.000000) 
					AND IS_ENTITY_IN_ANGLED_AREA(pedTonya, <<423.742371,-1652.842163,27.268002>>, <<397.839600,-1630.331665,40.292778>>, 42.000000) 
						
						HANDLE_TONYA_GOING_TO_TRUCK()
						
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT bPlayedTruckDialogue
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_ATTRK2", CONV_PRIORITY_HIGH)
											CPRINTLN(DEBUG_MISSION, "PLAYING ACKNOWLEDGE TRUCK CONVO")
											
											IF NOT IS_TIMER_STARTED(tDelayTruckTimer)
												START_TIMER_NOW(tDelayTruckTimer)
												CPRINTLN(DEBUG_MISSION, "STARTING TIMER - tDelayTruckTimer")
											ENDIF
											
											bPlayedTruckDialogue = TRUE
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_ENTITY_DEAD(vehTruck) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTruck)
										IF NOT bPlayedDelayInTruckConvo
											IF IS_TIMER_STARTED(tDelayTruckTimer)
												IF GET_TIMER_IN_SECONDS(tDelayTruckTimer) > 20
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF CREATE_CONVERSATION(pedConvo, "TOWAUD", "TONYA_WAIT2", CONV_PRIORITY_HIGH)
															RESTART_TIMER_NOW(tDelayTruckTimer)
															CPRINTLN(DEBUG_MISSION, "bPlayedDelayInTruckConvo = TRUE")
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						HANDLE_TONYA_DIALOGUE()
					ENDIF
				ENDIF
			ENDIF
			
			// Detect player getting into the tow truck
			IF NOT bTonyaAbandonWarning AND NOT bWanted
				IF NOT IS_ENTITY_DEAD(vehTruck)
					IF ((GET_PED_IN_VEHICLE_SEAT(vehTruck) = PLAYER_PED_ID()) AND IS_PED_IN_VEHICLE(pedTonya, vehTruck))
					OR IS_FRANKLIN_IN_ANY_TOW_TRUCK(vehTruck, TRUE, pedTonya)
						
						// Remove blip
						IF DOES_BLIP_EXIST(myVehicleBlip)
							REMOVE_BLIP(myVehicleBlip)
							CPRINTLN(DEBUG_MISSION, "REMOVING TRUCK BLIP - 01")
						ENDIF
						// Remove blip
						IF DOES_BLIP_EXIST(blipTonya)
							REMOVE_BLIP(blipTonya)
							CPRINTLN(DEBUG_MISSION, "REMOVING TONYA BLIP - 01")
						ENDIF
						
						CLEAR_THIS_PRINT("TOWT_WAIT")
						
						CPRINTLN(DEBUG_MISSION, "STATE_GoToTruck: GOING TO STATE - SS_CLEANUP")
						m_eSubState = SS_CLEANUP
					ELSE
						IF ((GET_PED_IN_VEHICLE_SEAT(vehTruck) = PLAYER_PED_ID()) AND NOT IS_PED_IN_VEHICLE(pedTonya, vehTruck))
							IF NOT bPrintAllowTonyaToEnter
								PRINT("TOWT_WAIT", DEFAULT_GOD_TEXT_TIME, 1)
								bPrintAllowTonyaToEnter = TRUE
							ENDIF
							
							SET_VEH_RADIO_STATION(myVehicle, "RADIO_03_HIPHOP_NEW")
							CPRINTLN(DEBUG_MISSION, "TONYA 2: SETTING STATION - RADIO_03_HIPHOP_NEW")
							
							// Remove blip
							IF DOES_BLIP_EXIST(myVehicleBlip)
								REMOVE_BLIP(myVehicleBlip)
								CPRINTLN(DEBUG_MISSION, "REMOVING TRUCK BLIP - 02")
							ENDIF
							IF NOT DOES_BLIP_EXIST(blipTonya)
								blipTonya = ADD_BLIP_FOR_ENTITY(pedTonya)
								SET_BLIP_COLOUR(blipTonya, BLIP_COLOUR_BLUE)
								SET_BLIP_SCALE(blipTonya, BLIP_SIZE_PED)
								CPRINTLN(DEBUG_MISSION, "ADDING TONYA BLIP - 01")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION, "TRUCK IS DEAD")
				
					IF DOES_ENTITY_EXIST(vehTruck)
						DEBUG_MESSAGE("vehTruck is dead")
						failReason = FAIL_TRANSPORT_DESTROYED
						MISSION_FAILED()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP		
			
			IF NOT bWanted
				// Onto the next stage
				m_eState = MS_DO_MISSION
				m_eSubState = SS_SETUP
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC STATE_Retry()

	REQUEST_ADDITIONAL_TEXT("TOW", ODDJOB_TEXT_SLOT) // load text for the mission
	REQUEST_ADDITIONAL_TEXT("DTRSHRD", MINIGAME_TEXT_SLOT) // load text for the mission
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
		DEBUG_MESSAGE("trying to load string table")
		WAIT(0)
	ENDWHILE

	IF IS_REPLAY_IN_PROGRESS()

		iStageToUse = Get_Replay_Mid_Mission_Stage()
		CPRINTLN(DEBUG_MISSION, "REPLAY IS IN PROGRESS, USING STAGE = ", iStageToUse)
		
		IF g_bShitskipAccepted
			IF iStageToUse <= 1
				iStageToUse = (iStageToUse + 1)
				CPRINTLN(DEBUG_MISSION, "TONYA 1 - SHIT SKIP: iStageToUse = ", iStageToUse)
			ENDIF
		ENDIF
		
		IF iStageToUse = 0
			GO_TO_CHECKPOINT_1()
		ELIF iStageToUse = 1
			GO_TO_CHECKPOINT_2()
		ELIF iStageToUse = 2
			GO_TO_CHECKPOINT_3()
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, "WE'RE NOT DOING A REPLAY - GOING TO STATE - MS_MEET_TONYA")
		m_eState = MS_MEET_TONYA
	ENDIF
ENDPROC

PROC INIT_TONYA()
	REQUEST_ADDITIONAL_TEXT("TOW", ODDJOB_TEXT_SLOT)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
		DEBUG_MESSAGE("trying to load string table")
		RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
		WAIT(0)
	ENDWHILE
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	launchArgs.nodeType = NODE_TYPE_HANDI
	launchArgs.launchMode = TOWING_MODE_TRADITIONAL
	launchArgs.bInit = TRUE
	launchArgs.bRcVersion = TRUE
	pedTonya = sRCLauncherDataLocal.pedID[0]
	illegalIdx = 18

	IF NOT IS_REPEAT_PLAY_ACTIVE()
		g_savedGlobals.sTowingData.iTowingJobsCompleted = 1
		CPRINTLN(DEBUG_MISSION, "TOWING RANK = ", g_savedGlobals.sTowingData.iTowingJobsCompleted)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		MISSION_CLEANUP(sLoadQueue)
		Script_Cleanup()
	ENDIF
	
	RANDOM_CHAR_DISPLAY_MISSION_TITLE(RC_TONYA_2)
	
	sbi01 = ADD_SCENARIO_BLOCKING_AREA((<<414.1398, -640.0020, 27.5001>> - <<50,50,50>>), (<<414.1398, -640.0020, 27.5001>> + <<50,50,50>>))
	sbi02 = ADD_SCENARIO_BLOCKING_AREA((<<-229.8159, -1171.9999, 21.8557>> - <<50,50,50>>), (<<-229.8159, -1171.9999, 21.8557>> + <<50,50,50>>))
	sbi03 = ADD_SCENARIO_BLOCKING_AREA((<<535.2601, -173.7879, 53.5258>> - <<50,50,50>>), (<<535.2601, -173.7879, 53.5258>> + <<50,50,50>>))
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA((vTonya2CarGenPosition - <<15,15,15>>), (vTonya2CarGenPosition + <<15,15,15>>), FALSE)
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA((vTonya2CarGenPosition - <<15,15,15>>), (vTonya2CarGenPosition + <<15,15,15>>))
	
	SET_AGGRO_FIELDS_TO_NOT_CHECK()
	
	SETUP_DEBUG()
	
	INIT_TONYA()
	
	SET_WANTED_LEVEL_MULTIPLIER(0.1)
	STOP_SCRIPTED_CONVERSATION(TRUE)
	
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		
		WAIT(0)
		
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RC_TON2")
		
 		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		// Assert check
		IS_PLAYER_PLAYING(PLAYER_ID())
		
		// Detect if Tonya has been killed/attacked
		IF HANDLE_FAIL_CONDITIONS()
			m_eState = MS_FAILED
		ENDIF
		
		UPDATE_LAW(brokenVehicles, bTowHooked, vCurDestObjective, myVehicleBlip, blipImpound, myVehicle, curStage, pedTonya)
		
		IF NOT bStartedReplay
			IF IS_REPLAY_IN_PROGRESS()
			
				iStageToUse = Get_Replay_Mid_Mission_Stage()
				CPRINTLN(DEBUG_MISSION, "START - REPLAY IS IN PROGRESS, USING STAGE = ", iStageToUse)
				
				IF NOT g_bShitskipAccepted
					IF iStageToUse = 0
						START_REPLAY_SETUP(vPlayerFirstCheckpointPosition, fPlayerFirstCheckpointHeading)
						CPRINTLN(DEBUG_MISSION, "CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
					ELIF iStageToUse = 1
						START_REPLAY_SETUP(<<404.1494, -1630.7926, 28.2928>>, 231.5304)
						CPRINTLN(DEBUG_MISSION, "CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
					ELIF iStageToUse = 2
						START_REPLAY_SETUP(vSkipSpawnPosition, fSkipSpawnHeading)
						CPRINTLN(DEBUG_MISSION, "CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
					ENDIF
				ELSE
					IF iStageToUse = 0
						START_REPLAY_SETUP(<<404.1494, -1630.7926, 28.2928>>, 231.5304)
						CPRINTLN(DEBUG_MISSION, "SHITSKIP: CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
					ELIF iStageToUse = 1
						START_REPLAY_SETUP(<<404.1494, -1630.7926, 28.2928>>, 231.5304)
						CPRINTLN(DEBUG_MISSION, "SHITSKIP: CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
					ENDIF
				ENDIF
				
				CPRINTLN(DEBUG_MISSION, "bStartedReplay = TRUE")
				bStartedReplay = TRUE
			ELSE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF m_eState < MS_GO_TO_TRUCK
						SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
						CPRINTLN(DEBUG_MISSION, "SETTING - PEDMOVEBLENDRATIO_WALK")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		mnExtraModel = mnExtraModel // Needed to compile
		
		HANDLE_TONYA_FOOTSTEPS()
		HANDLE_TONYA_NO_PUNCHING()
		HANDLE_TONYA_PEDS_FLEE()
		
		// Mission update
		SWITCH(m_eState)
		
			CASE MS_INIT
				STATE_Retry()
			BREAK
			
			CASE MS_MEET_TONYA
				STATE_MeetTonya()
			BREAK
			
			CASE MS_GO_TO_TRUCK
				STATE_GoToTruck()
			BREAK
			
			CASE MS_DO_MISSION
				IF DO_TOWING_CORE(launchArgs, sLoadQueue)
					Script_Passed()
				ENDIF
			BREAK
			
			CASE MS_FAILED
				STATE_Failed()
			BREAK
			
			CASE MS_PASSED
				Script_Passed()
			BREAK
		ENDSWITCH
		
		// Check debug completion/failure
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
