/*////////////////////////////////////////////////////////////////////////////////////////
//		SCRIPT NAME		:	controller_Towing.sc										//
//		AUTHOR			:	Ryan Paradis												//
//		DESCRIPTION		:	Replaces the launcher_Towing we had. This controller:		//
//								- Creates a tow truck at the one position currently 	//
//									being used by the launcher							//
//								- Also functions like the taxi controller, in that it 	//
//									will procedurally launch any tow truck mission		//
//									and offer them to the player						//
////////////////////////////////////////////////////////////////////////////////////////*/


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "minigames_helpers.sch"

//USING "towing_Static_lib.sch"
USING "towing_Dynamic_lib.sch"

USING "commands_entity.sch"
USING "script_player.sch"
USING "flow_public_core_override.sch"

//TOWING_STATE		eStaticState 	= TOWING_INIT
TOWING_STATE		eDynamicState 	= TOWING_INIT
INT					iWaitTime = 0

/// PURPOSE:
///    Cleanup and shutdown this controller.
PROC TOWING_CONTROLLER_ThreadCleanup()
	// Clean static towing.
	//TOWING_STATIC_Cleanup()
	
	// Clean dynamic towing.
	TOWING_DYNAMIC_Cleanup()
	
	CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_NO_DELAY)
	CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_10_DELAY)
	CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_60_DELAY)
	
	// End our life.
	PRINTLN("==================================== TOWING CONTROLLER TERMINATING ====================================")
	TERMINATE_THIS_THREAD()
ENDPROC


/// PURPOSE:
///    Checks to see if towing is unlocked in flow.
/// RETURNS:
///    TRUE if it's unlocked or we're in debug, FALSE if it's not unlocked.
FUNC BOOL TOWING_CONTROLLER_CheckMinigameBitset()

	IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
		IF (g_savedGlobals.sFlow.isGameflowActive)
			IF (GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_TOWING)))
			AND GET_CURRENT_PROPERTY_OWNER(PROPERTY_TOWING_IMPOUND) = CHAR_FRANKLIN
				// FlowBitset bit set, minigame is allowed. Let's go!
//				PRINTLN("GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_TOWING)) returned true. Starting towing controller.")
//				SCRIPT_ASSERT("starting towing controller!")
				RETURN TRUE
			ENDIF
		ELSE
//			PRINTLN("gameflowbit not active but we're in debug mode so running anyway!")
//			SCRIPT_ASSERT("gameflowbit not active but we're in debug mode so running anyway!")
			// We're in debug, but that shouldn't stop the fun.
			RETURN TRUE
		ENDIF
	ELSE
		eDynamicState = TOWING_INIT
//		PRINTLN("we're on a mission! not running towing controller!")
	ENDIF
	
//	PRINTLN("THE BIT IS OFF, NO TOWING FOR YOU!!!")
	RETURN FALSE
ENDFUNC

// Main loop.
SCRIPT
	PRINTLN("==================================== TOWING CONTROLLER STARTING ====================================")
	// Script init here.
	
	// Do we need to cleanup?
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_REPEAT_PLAY | FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_DEBUG_MENU)
		TOWING_CONTROLLER_ThreadCleanup()
	ENDIF
	
	// Set us to relaunch when a save is loaded.
	Register_Script_To_Relaunch_List(LAUNCH_BIT_MG_CTRL_TOWING)
	
	// Going to store the last time we finished a dynamic job so we know when to offer another.
	INT iDynamicJobFinishedTime
	
	BOOL bIsReplayLaunch_Towing = FALSE
	
	// Main loop.
	WHILE TRUE
	
//		PRINTLN("CONTROLLER RUNNING")
	
		// Make sure we can run.
		IF TOWING_CONTROLLER_CheckMinigameBitset()
			// Grab play data. both share this.
			PED_INDEX playerPed = PLAYER_PED_ID()
			IF NOT IS_ENTITY_DEAD(playerPed)
				iWaitTime = 0
				
				TOWING_DYNAMIC_MainUpdate(eDynamicState, playerPed, iDynamicJobFinishedTime, iWaitTime, bIsReplayLaunch_Towing)
			ENDIF
			
			WAIT(iWaitTime)
		ELSE
			// Can't run. Don't spam the system.
			REMOVE_TOWING_CONTROLLER_ASSETS()
//			PRINTLN("WAITING 2000")
			WAIT(2000)
		ENDIF	
	ENDWHILE
ENDSCRIPT
