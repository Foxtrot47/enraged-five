// Assassination_briefing_lib.sch
USING "globals.sch"

USING "minigame_UIInputs.sch"
USING "context_control_public.sch"

USING "cellphone_public.sch"
USING "Assassination_support_lib.sch"
USING "controller_Assassination_lib.sch"
USING "script_oddjob_funcs.sch"
USING "script_useContext.sch"
USING "email_public.sch"

USING "script_clock.sch"
USING "lineactivation.sch"
USING "rgeneral_include.sch"
USING "taxi_functions.sch"
USING "replay_private.sch"


ENUM ASC_CONTROL_STATE
	ASC_INIT_CONTROLLER,
	ASC_WAIT_ANSWER_PAYPHONE,
	ASC_ANSWERED_PAYPHONE,
	ASC_PHONE_CUTSCENE,
	ASC_PHONE_CUTSCENE_EXIT
ENDENUM
ASC_CONTROL_STATE eAscState = ASC_INIT_CONTROLLER

ENUM ASSASSINATION_FLAGS
	ASSF_NEED_KILL_CONVO 	= BIT1,
	ASSF_REQUESTED_ANIMS 	= BIT2
ENDENUM


// ********************************** Script Globals *************************************

sAssassinMissionData	missionData
ASSASSINATION_FLAGS		eBitflags
structPedsForConversation convoPed
structTimer				phoneConvoTimer
structTimer				tempCameraTimer
INT						iPayphoneRingID = -1
//INT						iPayphoneCamStage = 0
INT 					iCurAssassinRank
FLOAT					fConvoWaitTime = 1.0
CAMERA_INDEX			cutsceneCam = NULL
//CAMERA_INDEX 			cutsceneCamDest = NULL
OBJECT_INDEX			payphoneProp
PED_INDEX				pedLester
structTimelapse			sTimelapse

// ********************************** ************** *************************************


PROC INIT_ASS_BRIEFING()
	PRINTLN("eAscState = ASC_INIT_CONTROLLER")
	eAscState = ASC_INIT_CONTROLLER
ENDPROC

FUNC BOOL UPDATE_ASS_BRIEFING()
	SWITCH eAscState
		CASE ASC_INIT_CONTROLLER
			IF missionData.bCreatePed
				REQUEST_MODEL(CS_LESTERCREST)
				REQUEST_ANIM_DICT("amb@world_human_smoking@male@male_a@base")
				
				IF HAS_MODEL_LOADED(CS_LESTERCREST)
				AND HAS_ANIM_DICT_LOADED("amb@world_human_smoking@male@male_a@base")
					PRINTLN("PED: eAscState = ASC_ANSWERED_PAYPHONE")
					eAscState = ASC_ANSWERED_PAYPHONE
				ELSE
					PRINTLN("WAITING ON MODELS AND ANIMATIONS TO LOAD")
				ENDIF
			ELSE
				PRINTLN("NO PED: eAscState = ASC_ANSWERED_PAYPHONE")
				eAscState = ASC_ANSWERED_PAYPHONE
			ENDIF
		BREAK
		
		CASE ASC_ANSWERED_PAYPHONE
			PRINTLN("<ASSASSIN> ASC_ANSWERED_PAYPHONE")
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				PRINTLN("REMOVING PLAYER CONTROL")
			ENDIF
			CLEAR_HELP()
			
			IF missionData.bCreatePed
				pedLester = CREATE_PED(PEDTYPE_MISSION, CS_LESTERCREST, <<-1700.3940, -1069.1013, 12.1651>>, 346.1607)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLester, TRUE)
				SET_ENTITY_AS_MISSION_ENTITY(pedLester)
				TASK_PLAY_ANIM(pedLester,"amb@world_human_smoking@male@male_a@base", "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				PRINTLN("CREATED LESTER")
			ENDIF
			
			// Start the cutscene.
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
			ASSASSIN_CONTROLLER_StartPhoneAnswerCutscene(missionData, cutsceneCam, convoPed)
			CLEAR_BITMASK_ENUM_AS_ENUM(eBitflags, ASSF_REQUESTED_ANIMS)

			// Handle the answering Phase.									
			fConvoWaitTime = 2.2
			RESTART_TIMER_NOW(phoneConvoTimer)
			RESTART_TIMER_NOW(tempCameraTimer)
			
			eAscState = ASC_PHONE_CUTSCENE
		BREAK

		CASE ASC_PHONE_CUTSCENE
			PRINTLN("<ASSASSIN> ASC_PHONE_CUTSCENE")
			
			SET_TEXT_CENTRE(TRUE) 
			SET_TEXT_SCALE(1.0, 1.0) 
			DISPLAY_TEXT(0.5, 0.5, "ASS_STUB")
			
			BOOL bContinue
			bContinue = FALSE
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
				bContinue = TRUE
			ENDIF
		
			IF TIMER_DO_ONCE_WHEN_READY(phoneConvoTimer, fConvoWaitTime)
				ASSASSIN_CONTROLLER_RunPhoneConvo(iCurAssassinRank, convoPed)
				SET_BITMASK_ENUM_AS_ENUM(eBitflags, ASSF_NEED_KILL_CONVO)
			ELIF NOT IS_TIMER_STARTED(phoneConvoTimer)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					bContinue = TRUE
				ELSE
					//change camera angle every 3 sec. 
					IF TIMER_DO_WHEN_READY(tempCameraTimer, 3.0)
						RESTART_TIMER_NOW(tempCameraTimer)
//						ASSASSIN_CONTROLLER_SwapPhoneCutsceneCam(missionData, cutsceneCam)
					ENDIF
					ASSASSIN_CONTROLLER_UpdatePhoneAnswerCutscene()
				ENDIF
			ENDIF
			
			IF bContinue
				CANCEL_TIMER(phoneConvoTimer)
				
				// Start the player.
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				ASSASSIN_CONTROLLER_EndPhoneAnswerCutscene(cutsceneCam)
				
				//reset sound ID and play audio of player hanging up the phone
				ODDJOB_STOP_SOUND(iPayphoneRingID)
				ODDJOB_PLAY_SOUND("PAYPHONE_RECEIVER_SLAM_MASTER", iPayphoneRingID)

				eAscState = ASC_PHONE_CUTSCENE_EXIT
			ENDIF
		BREAK
		
		CASE ASC_PHONE_CUTSCENE_EXIT
			PRINTLN("<ASSASSIN> ASC_PHONE_CUTSCENE_EXIT")
			DISABLE_CELLPHONE(FALSE)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			ENDIF
			
//			IF iCurAssassinRank >= 0 AND iCurAssassinRank < NUM_ASSASSINATION_MISSIONS
//				BEGIN_EMAIL_THREAD_INT(ENUM_TO_INT(ASS_THREAD_HOTEL) + iCurAssassinRank)
//			ENDIF

			SET_OBJECT_AS_NO_LONGER_NEEDED(payphoneProp)
			PRINTLN("eAscState = ASC_PHONE_CUTSCENE_EXIT!!!")
			//email
			ASSASSIN_CONTROLLER_GiveMissionLoadout(missionData.eGiveEquipment)
			CANCEL_TIMER(phoneConvoTimer)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			RETURN FALSE

		ENDSWITCH
	RETURN TRUE
ENDFUNC

