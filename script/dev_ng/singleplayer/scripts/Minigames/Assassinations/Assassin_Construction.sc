
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes
USING "script_player.sch"
USING "controller_assassination_lib.sch"
USING "cutscene_public.sch"
USING "Assassin_shared.sch"
USING "assassination_support_lib.sch"
USING "Assassination_briefing_lib.sch"
USING "script_oddjob_funcs.sch"
USING "taxi_functions.sch"
USING "oddjob_aggro.sch"
USING "completionpercentage_public.sch"
USING "replay_public.sch"
USING "net_fps_cam.sch"
USING "clearMissionArea.sch"
USING "timelapse.sch"
USING "commands_event.sch"
USING "replay_private.sch"
USING "commands_recording.sch"

//CONST_INT TOTAL_NUM_WORKERS 26	//this is 1 greater than num peds in all waves wave because it is also includeing the target (who isn't assigned to a wave)
CONST_INT NUM_WAVE_1_WORKERS 5	
CONST_INT NUM_WAVE_2_WORKERS 3
CONST_INT NUM_WAVE_3_WORKERS 6
CONST_INT NUM_WAVE_4_WORKERS 2
CONST_INT NUM_WAVE_5_WORKERS 4
CONST_INT NUM_WAVE_6_WORKERS 3
CONST_INT NUM_WARNING_GUYS 3
CONST_INT NUM_PATROL_POINTS 20	//	patrol path points used for guys patrolling on ground level of the mission
CONST_INT NUM_WORKER_MODELS 2
CONST_INT MAX_NUMBER_COVER_PROPS 5
CONST_FLOAT CHANGE_ACCURACY_TIME 12.0

ENUM Assassin_Stages
	STAGE_TIME_LAPSE,
	STAGE_BRIEF_INIT,
	STAGE_BRIEF_UPDATE,
	STAGE_BRIEF_EXIT,
	STAGE_INIT,
	STAGE_STREAMING,
	STAGE_SPAWNING,
	STAGE_GET_TO_SITE,
	STAGE_PANIC_CUTSCENE,
	STAGE_ENTER_SITE,
//	STAGE_GET_TO_ELEVATOR,
	STAGE_ELEVATOR_CUTSCENE,
	STAGE_GET_TO_SECOND_ELEVATOR,
	STAGE_SECOND_ELEVATOR_CUTSCENE,
	STAGE_ROOFTOP_BATTLE,
	STAGE_KILL_CAM,
	STAGE_FLEE_POLICE,
	STAGE_CLEANUP
ENDENUM
Assassin_Stages MainStage = STAGE_BRIEF_INIT

//kill types
ENUM eKillType
	KT_STANDARD,
	KT_BONUS
ENDENUM
eKillType killType = KT_STANDARD

//kill types
ENUM eHeliStage
	HS_ATTACKING,
	HS_FLY_TO_POINT,
	HS_FLEE_PLAYER
ENDENUM
eHeliStage heliStage = HS_ATTACKING


ENUM eWorkerState
	WS_PATROL = 0,
	WS_REACT = 1,
	WS_COMBAT
ENDENUM
eWorkerState  workerState

// ------------------------------Cutscene variables------------------------------

ENUM PAYPHONE_CUTSCENE
	PAYPHONE_CUTSCENE_INIT = 0,
	PAYPHONE_CUTSCENE_UPDATE,
	PAYPHONE_CUTSCENE_SKIP,
	PAYPHONE_CUTSCENE_CLEANUP
ENDENUM
PAYPHONE_CUTSCENE payPhoneCutsceneStages = PAYPHONE_CUTSCENE_INIT

OBJECT_INDEX oPayPhone, oPayPhoneAnimated
CAMERA_INDEX camPayPhoneIntro01
VECTOR vPhoneLocation = <<809.2970, -1075.8088, 27.6534>>
VECTOR vPlayerWarpPosition = <<809.2200, -1075.8560, 27.6541>> 
FLOAT fPlayerWarpHeading = 85.4771

FLOAT fCutsceneLength = 45.0
FLOAT fRightX, fRightY

VECTOR vCameraPosition01 = <<810.9202, -1075.5016, 29.1399>>   
VECTOR vCameraRotation01 = <<-1.9308, 0.0000, 78.5519>>
structTimer tPayphoneCutsceneTimer
structTimer tPayphoneCameraTimer
BOOL bSkipCutscene = FALSE
BOOL bPlayedPayPhoneConvo = FALSE
BOOL bContinueWithScene = FALSE
//BOOL bTouchedRightStick = FALSE
BOOL bTargetKillLinePlayed = FALSE
BOOL bGuardsCreatedForFirstStage = FALSE
BOOL bSkippedEndingScreen = FALSE
BOOL bPlayMusic = FALSE
BOOL bDoneFlash = FALSE

VECTOR scenePosition = <<0,0,0>>
VECTOR sceneRotation = <<0,0,0>>
INT iSceneId
INT iEndingScreenStages = 0
INT iIntroCameraStages = 0

SIMPLE_USE_CONTEXT cucEndScreen

STREAMVOL_ID svPhoneCutscene

// ------------------------------Cutscene variables------------------------------


STRUCT WORKER_STRUCT // contains details for all mission peds
	PED_INDEX				ped							//Ped Index
//	BLIP_INDEX				blip						//Blip Index
	BOOL					bDead						//Bool used to detect if he has been killed used in HANDLE_ENEMY_BLIPS function
	AI_BLIP_STRUCT			blipWorker
	VECTOR					loc							//Ped start location
	FLOAT					head						//Ped start heading
	WEAPON_TYPE      	   	wpn							//Ped Weapon type
	COMBAT_MOVEMENT			move						//Ped movement type
	COMBAT_RANGE			range						//Ped combat range
	COMBAT_ABILITY_LEVEL	ability						//Ped combat ability
	COMBAT_ATTRIBUTE		attribute					//Ped combat attribute
ENDSTRUCT
WORKER_STRUCT	wave1[NUM_WAVE_1_WORKERS]
WORKER_STRUCT	wave2[NUM_WAVE_2_WORKERS]
WORKER_STRUCT	wave3[NUM_WAVE_3_WORKERS]
WORKER_STRUCT	wave4[NUM_WAVE_4_WORKERS]
WORKER_STRUCT	wave5[NUM_WAVE_5_WORKERS]
WORKER_STRUCT	wave6[NUM_WAVE_6_WORKERS]
WORKER_STRUCT	warningGuys[NUM_WARNING_GUYS]
BOOL 			bTaskedToAim[NUM_WARNING_GUYS]

// -----------------------------------
// VARIABLES
// -----------------------------------

//FIRST_PERSON_CAM_STRUCT camFirstPerson

BLIP_INDEX bTargetBlip
BLIP_INDEX bElevatorBlip
BLIP_INDEX bSiteBlip
BLIP_INDEX blipRooftop

COVERPOINT_INDEX ciSecondFloorPosition
COVERPOINT_INDEX ciRoofPosition

//STREAMVOL_ID svPanicCutscene

BOOL bWarningHelp
BOOL bPanicking
BOOL bPlayerSpotted
BOOL bWave1Attacking
//BOOL bCheckingWave1ForPanic = TRUE
BOOL bBlipsCleared
BOOL bElevatorAttack
BOOL bPedsOutOfVehicle
BOOL bCarSent
BOOL bCutsceneSkipped
BOOL bCutsceneFading
BOOL bAdvanceOnPlayer
BOOL bLeaveDefensiveArea
BOOL bElevatorMsgPrinted
BOOL bAbandoned
BOOL bReplaying = FALSE
BOOL bRooftopWaveCreated
BOOL bHeliPedsCreated
BOOL bMissionFailed = FALSE
BOOL bHeliAttacking = FALSE
BOOL heliFleeLinePlayed = FALSE
//BOOL bPrintElevatorHelp = FALSE
BOOL bApplyForceToDeadGuy = FALSE
BOOL bTeleportCraneGuy = FALSE
BOOL bEarlyOut = FALSE
BOOL bHeliModelLoaded = FALSE
BOOL bHeliOutOfControl
BOOL bRidingFinalElevator
BOOL bPlayerReachedBottom
BOOL bPlayerInSite = FALSE
BOOL bEarlyAggro = FALSE
BOOL bTriggerRooftop = FALSE
BOOL bLoadSceneRequested = FALSE
BOOL bIntroSceneCreated = FALSE
//BOOL bFirstCheckpointSceneLoad = FALSE
//BOOL bSecondCheckpointSceneLoad = FALSE
//BOOL bThirdCheckpointSceneLoad  = FALSE
//BOOL bFourthCheckpointSceneLoad  = FALSE
//BOOL bFifthCheckpointSceneLoad  = FALSE
//BOOL bDebugSkipping = FALSE
BOOL bPlayedLined = FALSE
BOOL bReplayVehicleAvailable = FALSE
BOOL bSwitchedSeats = FALSE
BOOL bNeedToCheckOnRoof = FALSE
BOOL bSetCheckpoint = FALSE
BOOL bUpdateGuysInCar = FALSE
BOOL bOkayToAttack = FALSE
BOOL bPlayedCellPhoneCall = FALSE
BOOL bPrintLastObjective = FALSE
BOOL bPrintedReminderMessage = FALSE
BOOL bFranklinJumpLinePlayed = FALSE

//CAMERA_INDEX camKillcam
CAMERA_INDEX camIntroA, camIntroB

FLOAT fTruckHead = 140

//INT iCutsceneStage
INT iCurrentPlayerLine = 0
INT iStageNum = 0 
INT iElevButtonStage = 0
INT iNumDead
INT iEnemySpeechStage
INT iCoverTime
INT iSoundIdElevator = -1
INT iFrameCountAsn
//INT iNumDeadPedsInElevator = 0
INT iCraneEnemyStage
INT iRoadNode01
INT iFinalElevatorCutsceneStage
INT iWarningGuysUpdate
INT iWarningGuardIndex
INT iFirstTimeWarning = 7
INT iSecondTimeWarning = 5
INT iTriggerEarlyUpdateStages = 0
INT iStageToUse
INT iParachutePickup
INT iElevSyncScene

MODEL_NAMES WorkerModel[NUM_WORKER_MODELS]
MODEL_NAMES TruckModel
MODEL_NAMES CombatCarModel
MODEL_NAMES ElevDoorModel
MODEL_NAMES ElevModel
MODEL_NAMES HeliModel
MODEL_NAMES ElevButtonModel

//OBJECT_INDEX objElevDoorClosed[4]
//OBJECT_INDEX objElevDoorControl[6]
OBJECT_INDEX objElev[3]
OBJECT_INDEX objElevButton[3]
OBJECT_INDEX objCoverProps[MAX_NUMBER_COVER_PROPS]

PED_INDEX Player
PED_INDEX piTarget
PED_INDEX piCutscenePed
PED_INDEX piClosestEnemy
PED_INDEX piDeadElevatorGuys[2]
PED_INDEX pedLeftSide, pedRightSide
PED_INDEX pedSpotted

VECTOR vCraneSpawnPos = << -110.9849, -972.0396, 295.4 >> //<< -117.2265, -975.1129, 295.4 >>
VECTOR vTruckPos = << -134.2935, -982.4351, 26.2731 >>
VECTOR vSitePos = <<-133.51, -997.09, 26.28>>
VECTOR vElev1Bottom = << -184.11, -1015.63, 30.06 >>
//VECTOR vElev1Top = << -182.5666, -1012.135, 113.1590 >>
VECTOR vElev2Bottom = << -158.02, -940.52, 114.25 >>
VECTOR vElev2Top = << -154.6688, -941.5961, 268.2575 >>
//VECTOR vElev1CamRot = << -5.9642, 0.0000, 156.4878 >> //<< -13.29, -0.02, 140.32 >>
//VECTOR vElev2CamRot = << -1.4808, 0.0000, 68.1868 >>
//VECTOR vAttachmentOffset = <<1.0433, -1.7817, -0.8980>>
VECTOR vRooftopPosition = << -152.0844, -943.5401, 268.1324 >>
VECTOR vMyElevRot[3]
//VECTOR vMyElevDoor[4]
VECTOR vMyElevPos[2]
VECTOR vPatrolPathPoint[NUM_PATROL_POINTS]
VECTOR vCranePedPos
VECTOR vButtonOffset = <<-0.469401, -0.863015, -0.6957>>
VECTOR vButtonPressAnimOffset = <<-0.369, -1.705, -1.329>>
VECTOR vAngledAreaMinWarningGuy[NUM_WARNING_GUYS]
VECTOR vAngledAreaMaxWarningGuy[NUM_WARNING_GUYS]
FLOAT fAngledAreaWidthWarningGuy[NUM_WARNING_GUYS]
//VECTOR vPlayerPos
//VECTOR vPlayerRot
//VECTOR vCranePos0 = << -122.8084, -978.1938, 117.8043 >>
//VECTOR vCranePos1 = << -128.9403, -981.0928, 113.1314 >>
VECTOR vIntroCutsceneTriggerPos = << -109.3679, -908.5847, 28.2615 >>
VECTOR vPlayerElevatorOffset = << -0.2, -2.2, -1.3 >>
//VECTOR vCurrentHeliPoint
VECTOR vHeliPos = << -140.2973, -966.4312, 268.1324 >>
FLOAT fHeliHead = 95.6565
VECTOR vTargetPos = << -146.7524, -959.7552, 268.1324 >>
FLOAt fTargetHead = 241.6704

VECTOR vLastStageCoverPosition = << -150.5756, -945.9047, 268.1324 >>   
FLOAT fLastStageCoverHeading = 134.4706

VECTOR	vMoveVehiclePosition01 = <<797.9703, -1064.6918, 26.7261>> 
FLOAT 	fMoveVehicleHeading01 = 6.2218

VECTOR	vMoveVehiclePosition02 = <<-87.0079, -915.3242, 28.1966>>
FLOAT 	fMoveVehicleHeading02 = 70.2099

VEHICLE_INDEX viTruck
VEHICLE_INDEX viCombatCar
VEHICLE_INDEX viPlayerCar
VEHICLE_INDEX viHeli
VEHICLE_INDEX vehPlayerIsIn
VEHICLE_INDEX viReplayVehicle

REL_GROUP_HASH relEnemies

structTimer missionTimer
structTimer speechTimer
structTimer warningTimer
structTimer spottedTimer
structTimer heliFleeTimer, heliFleeTimer2
structTimer changeAccuracyTimer01
structTimer changeAccuracyTimer02
structTimer tPanicFailSafeTimer

BOOL bChangeAccuracy01 = FALSE
BOOL bChangeAccuracy02 = FALSE

structPedsForConversation siteConv

PICKUP_INDEX pickupHealth
PICKUP_INDEX pickupParachute
PICKUP_INDEX pickupParachuteTable

WEAPON_TYPE wtFailWeapon
WEAPON_TYPE wtElevatorWeapon

VECTOR vTrailEnd = << -132.82999, -981.02496, 26.27540>>
INT iPetrolStage


// -----------------------------------
// DEBUG MODE
// -----------------------------------
#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

#IF IS_DEBUG_BUILD
//	INT iJSkipStage
	DEBUG_POS_DATA myDebugData
	BOOL bDoingPSkip = FALSE
	BOOL bDoingJSkip = FALSE
	
	MissionStageMenuTextStruct sSkipMenu[4]
	INT iDebugJumpStage = 0              
#ENDIF
// -----------------------------------
// MAIN PROCS/FUNCTIONS
// -----------------------------------


PROC timeLapseCutscene()
	IF NOT IS_REPLAY_IN_PROGRESS()
	    IF DO_TIMELAPSE(SP_MISSION_ASSASSIN_5, sTimelapse, FALSE, FALSE)
	    	MainStage = STAGE_BRIEF_INIT
	    ENDIF
	ELSE
	    MainStage = STAGE_BRIEF_INIT
	ENDIF
ENDPROC

PROC SETUP_UI_LABELS()
	endLabel.tlMissionPassed = "ASS_CS_PASSED"
	endLabel.tlMissionName = g_sMissionStatsName
	endLabel.tlTimeTaken = "ASS_CS_TIMER"
	endLabel.tlBaseReward = "ASS_CS_BASE"
	endLabel.tlBonusDesc = "ASS_CS_BDESC"
	endLabel.tlCashEarned = "ASS_CS_CASH"
	endLabel.tlCompletionGold = "ASS_CS_COMP"//"ASS_CS_COMPG"
	endLabel.tlCompletionSilver = "ASS_CS_COMP"//"ASS_CS_COMPS"
	endLabel.tlCompletionBronze = "ASS_CS_COMP"//"ASS_CS_COMPB"
	endLabel.tlCompletionNoMedal = "ASS_CS_COMP"//"ASS_CS_COMPN"
ENDPROC

PROC SETUP_DEBUG()
	#IF IS_DEBUG_BUILD
		sSkipMenu[0].sTxtLabel = "TRAVEL_STAGE"
		sSkipMenu[1].sTxtLabel = "ENTER THE SITE"
		sSkipMenu[2].sTxtLabel = "SECOND FLOOR BATTLE"
		sSkipMenu[3].sTxtLabel = "ROOFTOP BATTLE"
	#ENDIF
ENDPROC

PROC GRAB_RETRY_VEHICLE()
	IF NOT DOES_ENTITY_EXIST(viReplayVehicle)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				viReplayVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				PRINTLN("FOUND REPLAY VEHICLE")
				
				IF DOES_ENTITY_EXIST(viReplayVehicle) AND NOT IS_ENTITY_DEAD(viReplayVehicle)
					OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(viReplayVehicle)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_HEALTH_PACK()
	IF NOT DOES_PICKUP_EXIST(pickupHealth)
		pickupHealth = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<-170.470, -1016.089, 114.537>>, <<0, 0, 65.0>>)
		PRINTLN("CREATING HEALTH PICKUP")
	ENDIF
ENDPROC

PROC REMOVE_HEALTH_PACK()
	IF DOES_PICKUP_EXIST(pickupHealth)
		REMOVE_PICKUP(pickupHealth)
		PRINTLN("REMOVING HEALTH PACK")
	ENDIF
ENDPROC

PROC CREATE_PARACHUTE_PACK()
	IF NOT DOES_PICKUP_EXIST(pickupParachute)
		iParachutePickup = 0
	
		SET_BIT(iParachutePickup, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND)) 
		SET_BIT(iParachutePickup, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		pickupParachute = CREATE_PICKUP(PICKUP_PARACHUTE, <<-140.6149, -945.9811, 268.1331>>, iParachutePickup, 1)
		
		IF NOT DOES_PICKUP_EXIST(pickupParachuteTable)
			pickupParachuteTable = CREATE_PICKUP_ROTATE(PICKUP_PARACHUTE, <<-149.709, -966.410, 269.230>>, << 87, 22, 90>>)
			PRINTLN("CREATING PARACHUTE PICKUP ON TABLE")
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_PARACHUTE_PACK()
	IF DOES_PICKUP_EXIST(pickupParachute)
		REMOVE_PICKUP(pickupParachute)
		PRINTLN("REMOVING PARACHUTE PACK")
	ENDIF
	
	IF DOES_PICKUP_EXIST(pickupParachuteTable)
		REMOVE_PICKUP(pickupParachuteTable)
		PRINTLN("REMOVING PARACHUTE PACK FROM TABLE")
	ENDIF
ENDPROC

PROC REMOVE_WORKER_WAVE(WORKER_STRUCT& workerStruct[], INT iNumGuysInWave)
	INT tempInt

	FOR tempInt = 0 TO iNumGuysInWave - 1
		IF DOES_ENTITY_EXIST(workerStruct[tempInt].ped)
			IF IS_ENTITY_OCCLUDED(workerStruct[tempInt].ped)
				DELETE_PED(workerStruct[tempInt].ped)
				PRINTLN("REMOVING PED: ", tempInt)
			ELSE
				//to fix bugs 1114187, 1136149
				IF IS_PED_IN_ANY_HELI(workerStruct[tempInt].ped)
					IF NOT IS_PED_INJURED(workerStruct[tempInt].ped)
						SET_PED_COMBAT_ATTRIBUTES(workerStruct[tempInt].ped, CA_LEAVE_VEHICLES, FALSE)
						SET_PED_FLEE_ATTRIBUTES(workerStruct[tempInt].ped, FA_USE_VEHICLE, FALSE)
						SET_PED_KEEP_TASK(workerStruct[tempInt].ped, TRUE)
					ENDIF
				ENDIF
				
				SET_PED_AS_NO_LONGER_NEEDED(workerStruct[tempInt].ped)
				PRINTLN("SETTING PED AS NO LONGER NEEDED: ", tempInt)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    1 = Ground floor enemies; 2 = guys in car; 3 = mid-level; 4 = guys in elevator; 5 = Rooftop enemies
///    (Does not include Target)
PROC CLEANUP_WORKER_WAVE(WORKER_STRUCT& workerStruct[], INT iNumGuysInWave)
	INT tempInt

	FOR tempInt = 0 TO iNumGuysInWave - 1
		IF DOES_ENTITY_EXIST(workerStruct[tempInt].ped)
			IF NOT IS_PED_INJURED(workerStruct[tempInt].ped)
				IF IS_ENTITY_OCCLUDED(workerStruct[tempInt].ped)
				OR NOT IS_ENTITY_ON_SCREEN(workerStruct[tempInt].ped)
					IF NOT IS_PED_INJURED(workerStruct[tempInt].ped)
						SET_ENTITY_HEALTH(workerStruct[tempInt].ped, 0)
						PRINTLN("SETTING PED HEALTH TO ZERO - WAVE: ", tempInt)
					ENDIF
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(workerStruct[tempInt].ped)
					PRINTLN("SETTING PED AS NO LONGER NEEDED - WAVE: ", tempInt)
				ENDIF
			ENDIF
		ENDIF
		
//		IF DOES_BLIP_EXIST(workerStruct[tempInt].blip)
//			REMOVE_BLIP(workerStruct[tempInt].blip)
//		ENDIF
	ENDFOR

	REMOVE_PED_FOR_DIALOGUE(siteConv, 4)
	REMOVE_PED_FOR_DIALOGUE(siteConv, 5)
	REMOVE_PED_FOR_DIALOGUE(siteConv, 6)
	REMOVE_PED_FOR_DIALOGUE(siteConv, 7)
	REMOVE_PED_FOR_DIALOGUE(siteConv, 8)
ENDPROC	


/// PURPOSE:
///    Setup initial camera parameters for the mission
PROC CREATE_CAMERAS()	
//	camKillcam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -192.3310, -1018.3026, 30.3871 >>, << -1.5705, 0.0000, -63.3390 >>, 55)
	camIntroA = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -150.9599, -944.2627, 114.3693 >>, << 3.6074, -0.2888, 15.6398 >>, 50)
	camIntroB = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -150.6499, -945.0825, 114.3487 >>, << 1.2634, -0.2776, 28.5068 >>, 50)
ENDPROC


/// PURPOSE:
///    Cleanup all enemy ped blips
PROC CLEANUP_PED_BLIPS(BOOL bIncludeTarget)

	CLEANUP_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
	CLEANUP_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
	CLEANUP_WORKER_WAVE(wave3, NUM_WAVE_3_WORKERS)
	CLEANUP_WORKER_WAVE(wave4, NUM_WAVE_4_WORKERS)
	CLEANUP_WORKER_WAVE(wave5, NUM_WAVE_5_WORKERS)
	
	IF bIncludeTarget
		IF DOES_ENTITY_EXIST(piTarget)
			IF NOT IS_PED_INJURED(piTarget)
				IF IS_PED_IN_ANY_HELI(piTarget)
					SET_PED_COMBAT_ATTRIBUTES(piTarget, CA_LEAVE_VEHICLES, FALSE)
					SET_PED_FLEE_ATTRIBUTES(piTarget, FA_USE_VEHICLE, FALSE)
					SET_PED_KEEP_TASK(piTarget, TRUE)
				ENDIF
			ENDIF
					
			SET_PED_AS_NO_LONGER_NEEDED(piTarget)
		ENDIF
		
		IF DOES_BLIP_EXIST(bTargetBlip)
			REMOVE_BLIP(bTargetBlip)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    cleanup all COORD blips
PROC CLEANUP_DEST_BLIPS()
	IF DOES_BLIP_EXIST(bElevatorBlip)
		REMOVE_BLIP(bElevatorBlip)
	ENDIF
	
	IF DOES_BLIP_EXIST(bSiteBlip)
		REMOVE_BLIP(bSiteBlip)
	ENDIF	
ENDPROC


/// PURPOSE:
///    remove vehicle requests
PROC CLEANUP_VEHICLES()
	IF DOES_ENTITY_EXIST(viTruck)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(viTruck)
	ENDIF
	IF DOES_ENTITY_EXIST(viHeli)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(viHeli)
	ENDIF
ENDPROC

PROC CLEANUP_WARNING_GUYS()
	INT idx

	REPEAT NUM_WARNING_GUYS idx
		IF DOES_ENTITY_EXIST(warningGuys[idx].ped)
			IF IS_ENTITY_OCCLUDED(warningGuys[idx].ped)
				DELETE_PED(warningGuys[idx].ped)
				PRINTLN("REMOVING WARNING GUYS: ", idx)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(warningGuys[idx].ped)
				PRINTLN("SETTING AS NO LONGER NEEDED - WARNING GUYS: ", idx)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVING_HELI_AND_TARGET()
	IF DOES_ENTITY_EXIST(viHeli)
		IF IS_ENTITY_OCCLUDED(viHeli)
			DELETE_VEHICLE(viHeli)
			PRINTLN("REMOVING - viHeli")
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(viHeli)
			PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED - viHeli - 01")
		ENDIF
	ELSE
		PRINTLN("viHeli DOES NOT EXIST")
	ENDIF
	
	IF DOES_ENTITY_EXIST(piTarget)
		IF IS_ENTITY_OCCLUDED(piTarget)
			DELETE_PED(piTarget)
			PRINTLN("REMOVING - piTarget")
		ELSE
			IF NOT IS_PED_INJURED(piTarget)
				IF IS_PED_IN_ANY_HELI(piTarget)
					SET_PED_COMBAT_ATTRIBUTES(piTarget, CA_LEAVE_VEHICLES, FALSE)
					SET_PED_FLEE_ATTRIBUTES(piTarget, FA_USE_VEHICLE, FALSE)
					SET_PED_KEEP_TASK(piTarget, TRUE)
				ENDIF
			ENDIF
			
			SET_PED_AS_NO_LONGER_NEEDED(piTarget)
			PRINTLN("SETTING PED AS NO LONGER NEEDED - piTarget")
		ENDIF
	ELSE
		PRINTLN("piTarget DOES NOT EXIST")
	ENDIF
ENDPROC

PROC REMOVE_DEAD_ELEVATOR_GUYS()
	IF DOES_ENTITY_EXIST(piDeadElevatorGuys[0])
		DELETE_PED(piDeadElevatorGuys[0])
		PRINTLN("REMOVING PED - piDeadElevatorGuys[0]")
	ENDIF
	IF DOES_ENTITY_EXIST(piDeadElevatorGuys[1])
		DELETE_PED(piDeadElevatorGuys[1])
		PRINTLN("REMOVING PED - piDeadElevatorGuys[1]")
	ENDIF
ENDPROC

PROC HANDLE_COVER_PROPS(BOOL bCleanup = FALSE)
	INT idx
	VECTOR vCoverPropPosition[MAX_NUMBER_COVER_PROPS]
	FLOAT fCoverPropHeading[MAX_NUMBER_COVER_PROPS]
	
	vCoverPropPosition[0] = <<-113.3033, -958.0001, 26.2774>>  
	vCoverPropPosition[1] = <<-143.1153, -1007.3051, 26.2751>> 
	vCoverPropPosition[2] = <<-185.3750, -1036.0153, 26.10>> 
	vCoverPropPosition[3] = <<-130.0018, -970.9893, 26.3>>
	vCoverPropPosition[4] = <<-162.4316, -1039.5804, 26.2758>> 
	
	fCoverPropHeading[0] = 160.3873 
	fCoverPropHeading[1] = 149.8891
	fCoverPropHeading[2] = 254.0606
	fCoverPropHeading[3] = 353.8129
	fCoverPropHeading[4] = 314.2361
	
	REPEAT MAX_NUMBER_COVER_PROPS idx
		IF bCleanup
			IF DOES_ENTITY_EXIST(objCoverProps[idx])
				SET_OBJECT_AS_NO_LONGER_NEEDED(objCoverProps[idx])
//				DELETE_OBJECT(objCoverProps[idx])
				PRINTLN("SETTING COVER PROP AS NO LONGER NEEDED INDEX: ", vCoverPropPosition[idx])
			ENDIF
		ELSE
			IF NOT DOES_ENTITY_EXIST(objCoverProps[idx])
				objCoverProps[idx] = CREATE_OBJECT(prop_conc_blocks01a, vCoverPropPosition[idx])
				SET_ENTITY_HEADING(objCoverProps[idx], fCoverPropHeading[idx])
				FREEZE_ENTITY_POSITION(objCoverProps[idx], TRUE)
				PRINTLN("COVER PROP POSITIONS = ", vCoverPropPosition[idx])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


PROC CLEANUP_ALL_AUDIO_MIX_SCENES()
	IF DOES_ENTITY_EXIST(viCombatCar)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(viCombatCar)
	ENDIF
	IF DOES_ENTITY_EXIST(viHeli)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(viHeli)
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_CUTSCENE")
		STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_CUTSCENE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_SHOOTOUT_START")
		STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_SHOOTOUT_START")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_CAR_ARRIVES")
		STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_CAR_ARRIVES")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_SHOOTOUT_ROOFTOP")
		STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_SHOOTOUT_ROOFTOP")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_HELICOPTER_SCENE")
		STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_HELICOPTER_SCENE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_LEAVE_THE_AREA")
		STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_LEAVE_THE_AREA")
	ENDIF
ENDPROC


// Cleanup the script on termination – must not include any WAITs
PROC Script_Cleanup()
	
	IF DOES_ENTITY_EXIST(oPayPhoneAnimated)
		SET_MODEL_AS_NO_LONGER_NEEDED(P_PHONEBOX_01B_S)
		DELETE_OBJECT(oPayPhoneAnimated)
		PRINTLN("DELETING OBJECT - oPayPhoneAnimated")
	ENDIF
	
	IF DOES_ENTITY_EXIST(oPayPhone)
		SET_ENTITY_VISIBLE(oPayPhone, TRUE)
		SET_ENTITY_COLLISION(oPayPhone, TRUE)
		PRINTLN("TURNING BACK ON VISIBILITY AND COLLISION")
	ENDIF

	IF IS_TIMER_STARTED(missionTimer)
		g_savedGlobals.sAssassinData.fConstructionMissionTime = GET_TIMER_IN_SECONDS(missionTimer)
		PRINTLN("CLEANUP: missionTimer = ", g_savedGlobals.sAssassinData.fConstructionMissionTime)
	ELSE
		PRINTLN("CLEANUP: missionTimer WAS NOT STARTED!!!")
	ENDIF

	bHeliModelLoaded = FALSE
	PRINTLN("CLEANUP: bHeliModelLoaded = FALSE")
	bHeliPedsCreated = FALSE
	PRINTLN("CLEANUP: bHeliPedsCreated = FALSE")
	
	HANDLE_COVER_PROPS(TRUE)
	REMOVE_HEALTH_PACK()
	REMOVE_PARACHUTE_PACK()
	
//	IF STREAMVOL_IS_VALID(svPanicCutscene)
//		STREAMVOL_DELETE(svPanicCutscene)
//		PRINTLN("REMOVING - svPanicCutscene - 03")
//	ENDIF

	END_SRL()
	
//	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
//		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
//	ENDIF
	REMOVE_ROAD_NODE_SPEED_ZONE(iRoadNode01)
	
	REMOVE_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
	REMOVE_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
	REMOVE_WORKER_WAVE(wave3, NUM_WAVE_3_WORKERS)
	REMOVE_WORKER_WAVE(wave4, NUM_WAVE_4_WORKERS)
	REMOVE_WORKER_WAVE(wave5, NUM_WAVE_5_WORKERS)
	REMOVE_WORKER_WAVE(wave6, NUM_WAVE_6_WORKERS)
	REMOVE_DEAD_ELEVATOR_GUYS()
	
	CLEANUP_WARNING_GUYS()
	REMOVING_HELI_AND_TARGET()
	
	CLEANUP_PED_BLIPS(TRUE)
	CLEANUP_DEST_BLIPS()
	CLEANUP_VEHICLES()
	SET_CREATE_RANDOM_COPS(TRUE) 

	SET_MODEL_AS_NO_LONGER_NEEDED(WorkerModel[0])
	SET_MODEL_AS_NO_LONGER_NEEDED(WorkerModel[1])
	SET_MODEL_AS_NO_LONGER_NEEDED(ElevDoorModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(ElevModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(ElevButtonModel)
	
	SET_PED_MODEL_IS_SUPPRESSED(WorkerModel[0], FALSE)
	
	//make the ground elevators render again
	REMOVE_MODEL_HIDE(vElev1Bottom, 5, PROP_CONSLIFT_LIFT)
	REMOVE_MODEL_HIDE(vElev2Bottom, 5, PROP_CONSLIFT_LIFT)
	REQUEST_IPL("DT1_21_prop_lift_on")
	REMOVE_IPL("DT1_21_ConSiteAssass")
	
	//detach the player from any elevator he may be attached to and delete the script created elevators
	IF DOES_ENTITY_EXIST(objElev[1])
		IF IS_ENTITY_ATTACHED_TO_ENTITY(Player, objElev[1])
			DETACH_ENTITY(Player)
		ENDIF
//		IF IS_ENTITY_AT_COORD(objElev[1], vElev2Bottom, <<5,5,5>>)
//			DELETE_OBJECT(objElev[1])
//		ENDIF
	ENDIF	
	IF DOES_ENTITY_EXIST(objElev[0])
		IF IS_ENTITY_ATTACHED_TO_ENTITY(Player, objElev[0])
			DETACH_ENTITY(Player)
		ENDIF
	ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_MAX_WANTED_LEVEL(6)
	SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
	
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	
	KILL_ANY_CONVERSATION()
	
	CLEAR_PED_NON_CREATION_AREA()
	
	CLEANUP_ALL_AUDIO_MIX_SCENES()
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	REMOVE_DECALS_IN_RANGE(vTrailEnd, 10)
	
	//restore phone stuff
	IF DOES_ENTITY_EXIST(oPayPhone)
		SET_ENTITY_VISIBLE(oPayPhone, TRUE)
		SET_ENTITY_COLLISION(oPayPhone, TRUE)
		SET_ENTITY_CAN_BE_DAMAGED(oPayPhone, TRUE)
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC


PROC INIT_MODELS()
	WorkerModel[0] = A_M_Y_BUSINESS_01
	WorkerModel[1] = S_M_M_HIGHSEC_01
	TruckModel = UTILLITRUCK3
	CombatCarModel = SEMINOLE
	ElevDoorModel = PROP_CONSLIFT_DOOR
	ElevModel = PROP_CONSLIFT_LIFT
	HeliModel = MAVERICK
	ElevButtonModel = PROP_SUB_RELEASE
ENDPROC

PROC SETUP_MISSION_REWARDS()		
	fBaseReward = 8000
	fBonusReward = 2000
ENDPROC

PROC REQUEST_STREAMS_NO_PEDS()
//	INIT_ZVOLUME_WIDGETS()

//	REQUEST_MODEL(WorkerModel[0])
//	REQUEST_MODEL(WorkerModel[1])
	REQUEST_MODEL(TruckModel)
	REQUEST_MODEL(CombatCarModel)
	REQUEST_MODEL(ElevDoorModel)
	REQUEST_MODEL(ElevModel)
	REQUEST_MODEL(prop_conc_blocks01a)
	REQUEST_MODEL(ElevButtonModel)
	REQUEST_ANIM_DICT("oddjobs@assassinate@construction@")
	REQUEST_WAYPOINT_RECORDING("OJAScs_101")
	REQUEST_VEHICLE_RECORDING(102, "OJAScs")
	REQUEST_SCRIPT_AUDIO_BANK("Freight_Elevator")
	REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\ASSASSINATION_MULTI")

	REQUEST_ADDITIONAL_TEXT("ASS_CS", MINIGAME_TEXT_SLOT)
ENDPROC


FUNC BOOL ARE_STREAMS_LOADED_NO_PEDS()
	BOOL returnBool = FALSE	
	
//	IF HAS_MODEL_LOADED(WorkerModel[0])
//	AND HAS_MODEL_LOADED(WorkerModel[1])
	IF HAS_MODEL_LOADED(TruckModel)
	AND HAS_MODEL_LOADED(CombatCarModel)
	AND HAS_MODEL_LOADED(ElevDoorModel)
	AND HAS_MODEL_LOADED(ElevModel)
	AND HAS_MODEL_LOADED(prop_conc_blocks01a)
	AND HAS_MODEL_LOADED(ElevButtonModel)
	AND HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
	AND HAS_ANIM_DICT_LOADED("oddjobs@assassinate@construction@")
	AND GET_IS_WAYPOINT_RECORDING_LOADED("OJAScs_101")
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(102, "OJAScs")
	AND REQUEST_SCRIPT_AUDIO_BANK("Freight_Elevator")
	AND REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\ASSASSINATION_MULTI")
//	AND REQUEST_SCRIPT_AUDIO_BANK("Security_Camera")
		PRINTLN("Streams loaded")
		returnBool = TRUE
	ELSE
		PRINTLN("Loading streams")
	ENDIF

	RETURN returnBool
ENDFUNC

FUNC BOOL ARE_CONSTRUCTION_PED_MODELS_LOADED()
	BOOL bLoaded
	
	REQUEST_MODEL(WorkerModel[0])
	REQUEST_MODEL(WorkerModel[1])
	
	IF HAS_MODEL_LOADED(WorkerModel[0])
	AND HAS_MODEL_LOADED(WorkerModel[1])
		PRINTLN("CONSTRUCTION PED MODELS LOADED")
		bLoaded = TRUE
	ELSE
		bLoaded = FALSE
	ENDIF
	
	RETURN bLoaded
ENDFUNC

FUNC BOOL HAS_HELICOPTER_MODEL_LOADED()
	BOOL bLoaded
	
	REQUEST_MODEL(HeliModel)
	
	IF HAS_MODEL_LOADED(HeliModel)
		PRINTLN("HELI MODEL LOADED!")
		bLoaded = TRUE
	ELSE
		bLoaded = FALSE
	ENDIF
	
	RETURN bLoaded

ENDFUNC

PROC SET_ELEVATOR_PARAMS()
//	//front door ground elevator
//	vMyElevDoor[0]= << -185.04, -1018.39, 31.28 >>
//	
//	//back door ground elevator
//	vMyElevDoor[1] = << -183.15, -1013.12, 29.070 >>
//	
//	//front door in panic cutscene
// 	vMyElevDoor[2] = << -155.650, -941.260, 115.420 >>
// 	
//	//back door panic cutscene
//	vMyElevDoor[3] = << -160.870, -939.400, 113.020 >>
	
	//ground elevator
	vMyElevPos[0] = << -182.24, -1016.44, 28.59 >>
	vMyElevRot[0] = <<0,0,250.24>>
	
	//2nd level elevator
	vMyElevPos[1] = << -158.840, -942.240, 112.8 >>
	vMyElevRot[1] = <<0,0,160.2>>
ENDPROC


/// PURPOSE:
///    Fill ped struct data for construction workers
PROC SETUP_WORKER_PARAMS()
		
//WAVE 1 - GROUND FLOOR
/* 0 */	wave1[0].loc =		<< -132.1519, -983.1443, 26.2731 >>
		wave1[0].head = 	320.6974
		wave1[0].wpn = 		WEAPONTYPE_PISTOL 
		wave1[0].move = 	CM_WILLADVANCE
		wave1[0].range = 	CR_MEDIUM
		wave1[0].ability = 	CAL_PROFESSIONAL
		wave1[0].attribute = CA_USE_COVER
		
/* 1 */	wave1[1].loc =		<< -128.4393, -1007.1317, 26.2731 >>
		wave1[1].head = 	347.4975 
		wave1[1].wpn = 		WEAPONTYPE_PUMPSHOTGUN 
		wave1[1].move = 	CM_WILLADVANCE
		wave1[1].range = 	CR_NEAR
		wave1[1].ability = 	CAL_PROFESSIONAL
		wave1[1].attribute = CA_USE_COVER

		//NEAR ELEVATOR
/* 4 */	wave1[2].loc =		<<-170.5017, -969.5618, 28.2893>>
		wave1[2].head = 	158.0000
		wave1[2].wpn = 		WEAPONTYPE_PISTOL 
		wave1[2].move = 	CM_DEFENSIVE
		wave1[2].range = 	CR_MEDIUM
		wave1[2].ability = 	CAL_PROFESSIONAL
		wave1[2].attribute = CA_USE_COVER
		
		//GUARDING THE ELEVATOR
/* 5 */	wave1[3].loc =		<<-189.2459, -1014.7320, 28.2884>>
		wave1[3].head = 	238.8071
		wave1[3].wpn = 		WEAPONTYPE_ASSAULTRIFLE 
		wave1[3].move = 	CM_DEFENSIVE
		wave1[3].range = 	CR_MEDIUM
		wave1[3].ability = 	CAL_PROFESSIONAL
		wave1[3].attribute = CA_AGGRESSIVE
		
		//NEAR THE GROUND FLOOR CRANE
/* 6 */	wave1[4].loc =		<< -118.25, -1051.072, 27.3 >> 
		wave1[4].head = 	286.9180
		wave1[4].wpn = 		WEAPONTYPE_PISTOL
		wave1[4].move = 	CM_DEFENSIVE
		wave1[4].range = 	CR_MEDIUM
		wave1[4].ability = 	CAL_PROFESSIONAL
		wave1[4].attribute = CA_USE_COVER

//WAVE 2 - GUYS IN CAR THAT DRIVE TO ELEVATOR
/* 0 */	wave2[0].loc =		<< -137.2297, -1095.2305, 20.6838 >> 
		wave2[0].head = 	160
		wave2[0].wpn = 		WEAPONTYPE_PISTOL
		wave2[0].move = 	CM_WILLADVANCE
		wave2[0].range = 	CR_MEDIUM
		wave2[0].ability = 	CAL_PROFESSIONAL
		wave2[0].attribute = CA_USE_COVER		
		
/* 1 */	wave2[1].loc =		<< -135.9752, -1091.7012, 20.6838 >>
		wave2[1].head = 	160
		wave2[1].wpn = 		WEAPONTYPE_MICROSMG
		wave2[1].move = 	CM_DEFENSIVE
		wave2[1].range = 	CR_MEDIUM
		wave2[1].ability = 	CAL_PROFESSIONAL
		wave2[1].attribute = CA_USE_COVER	
		
/* 2 */	wave2[2].loc =		<< -136.1536, -1092.4149, 20.6838 >>
		wave2[2].head = 	160
		wave2[2].wpn = 		WEAPONTYPE_PISTOL
		wave2[2].move = 	CM_DEFENSIVE
		wave2[2].range = 	CR_MEDIUM
		wave2[2].ability = 	CAL_PROFESSIONAL
		wave2[2].attribute = CA_USE_COVER	


//WAVE 3 - 18th Floor - Midlevel
/* 0 */	wave3[0].loc =		<<-176.6884, -999.7880, 113.1382>>		
		wave3[0].head = 	165.9047								
		wave3[0].wpn = 		WEAPONTYPE_PISTOL
		wave3[0].move = 	CM_DEFENSIVE
		wave3[0].range = 	CR_NEAR
		wave3[0].ability = 	CAL_PROFESSIONAL
		wave3[0].attribute = CA_AGGRESSIVE

/* 1 */ wave3[1].loc =		<< -157.0080, -979.8131, 113.1330 >>
		wave3[1].head = 	246.3884 
		wave3[1].wpn = 		WEAPONTYPE_PISTOL 
		wave3[1].move = 	CM_DEFENSIVE
		wave3[1].range = 	CR_NEAR
		wave3[1].ability = 	CAL_PROFESSIONAL
		wave3[1].attribute = CA_AGGRESSIVE
		
/* 2 */ wave3[2].loc =		<< -154.27, -945.61, 113.1339 >>
		wave3[2].head = 	160.8846
		wave3[2].wpn = 		WEAPONTYPE_CARBINERIFLE
		wave3[2].move = 	CM_DEFENSIVE
		wave3[2].range = 	CR_NEAR
		wave3[2].ability = 	CAL_PROFESSIONAL
		wave3[2].attribute = CA_AGGRESSIVE
		
/* 3 */	wave3[3].loc =		<< -145.5338, -986.0398, 113.1282 >>
		wave3[3].head = 	137
		wave3[3].wpn = 		WEAPONTYPE_MICROSMG 
		wave3[3].move = 	CM_DEFENSIVE
		wave3[3].range = 	CR_NEAR
		wave3[3].ability = 	CAL_PROFESSIONAL
		wave3[3].attribute = CA_AGGRESSIVE
		
/* 4 */	wave3[4].loc =		<< -158.9804, -951.3898, 113.1328 >>
		wave3[4].head = 	173
		wave3[4].wpn = 		WEAPONTYPE_MICROSMG 
		wave3[4].move = 	CM_DEFENSIVE
		wave3[4].range = 	CR_NEAR
		wave3[4].ability = 	CAL_PROFESSIONAL
		wave3[4].attribute = CA_AGGRESSIVE
		
/* 5 */	wave3[5].loc =		<< -146.8379, -964.5045, 113.1339 >>
		wave3[5].head = 	156.7863
		wave3[5].wpn = 		WEAPONTYPE_PISTOL 
		wave3[5].move = 	CM_DEFENSIVE
		wave3[5].range = 	CR_NEAR
		wave3[5].ability = 	CAL_PROFESSIONAL
		wave3[5].attribute = CA_AGGRESSIVE
		
//		//guy hiding behind tarp near elevator
///* 6 */	wave3[6].loc =		<< -174.748, -989.5677, 114.1327  >>	
//		wave3[6].head = 	154.4059
//		wave3[6].wpn = 		WEAPONTYPE_PISTOL
//		wave3[6].move = 	CM_DEFENSIVE
//		wave3[6].range = 	CR_NEAR
//		wave3[6].ability = 	CAL_PROFESSIONAL
//		wave3[6].attribute = CA_AGGRESSIVE

		
// WAVE 4 THAT ARRIVES FROM TOP ELEVATOR
/* 0 */ wave4[0].loc =		<< -152.9496, -946.5169, 123.2578 >>
		wave4[0].head = 	124.2627
		wave4[0].wpn = 		WEAPONTYPE_PUMPSHOTGUN
		wave4[0].move = 	CM_WILLADVANCE
		wave4[0].range = 	CR_NEAR
		wave4[0].ability = 	CAL_PROFESSIONAL
		wave4[0].attribute = CA_AGGRESSIVE
		
/* 1 */	wave4[1].loc =		<< -151.9618, -945.3404, 123.2578 >>
		wave4[1].head = 	338.7249
		wave4[1].wpn = 		WEAPONTYPE_PISTOL 
		wave4[1].move = 	CM_DEFENSIVE
		wave4[1].range = 	CR_NEAR
		wave4[1].ability = 	CAL_PROFESSIONAL
		wave4[1].attribute = CA_AGGRESSIVE
	
//WAVE 5 - ROOFTOP ENEMIES
/* 0 */	wave5[0].loc =	<< -147.71979, -952.74762, 268.13504 >>
		wave5[0].head = 	14.5077
		wave5[0].wpn = 	WEAPONTYPE_PISTOL
		wave5[0].move = 	CM_WILLADVANCE
		wave5[0].range = 	CR_NEAR
		wave5[0].ability = CAL_PROFESSIONAL
		wave5[0].attribute = CA_AGGRESSIVE
		
/* 1 */	wave5[1].loc =	<< -156.95184, -966.8706, 268.2581 >>
		wave5[1].head = 	336.0895
		wave5[1].wpn = 	WEAPONTYPE_PISTOL
		wave5[1].move = 	CM_WILLADVANCE
		wave5[1].range = 	CR_NEAR
		wave5[1].ability = CAL_PROFESSIONAL
		wave5[1].attribute = CA_AGGRESSIVE
		
/* 2 */	wave5[2].loc =	<< -147.4382, -967.9998, 268.1325 >>
		wave5[2].head = 	50.9751
		wave5[2].wpn = 	WEAPONTYPE_PISTOL
		wave5[2].move = 	CM_DEFENSIVE
		wave5[2].range = 	CR_MEDIUM
		wave5[2].ability = CAL_PROFESSIONAL
		wave5[2].attribute = CA_AGGRESSIVE
	
		//guy on crane
/* 3 */	wave5[3].loc =	vCraneSpawnPos
		wave5[3].head = 	90
		wave5[3].wpn = 	WEAPONTYPE_CARBINERIFLE
		wave5[3].move = 	CM_DEFENSIVE
		wave5[3].range = 	CR_FAR
		wave5[3].ability = CAL_PROFESSIONAL
		wave5[3].attribute = CA_AGGRESSIVE
		
	//add their relationship grounp
	ADD_RELATIONSHIP_GROUP("ConstructionEnemies", relEnemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relEnemies, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relEnemies)
ENDPROC

/// PURPOSE:
///    Set construction worker attributes once they have been created
PROC SET_WORKER_PARAMS(WORKER_STRUCT& workerStruct[], INT idx, BOOL bSetCombatRange = TRUE)
	GIVE_WEAPON_TO_PED(workerStruct[idx].ped, workerStruct[idx].wpn , INFINITE_AMMO, TRUE)  
	SET_PED_AS_ENEMY(workerStruct[idx].ped , TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(workerStruct[idx].ped, relEnemies)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(workerStruct[idx].ped, TRUE)

	SET_PED_COMBAT_ABILITY(workerStruct[idx].ped, workerStruct[idx].ability)
	IF bSetCombatRange
		SET_PED_COMBAT_RANGE(workerStruct[idx].ped, workerStruct[idx].range)
	ENDIF
	SET_PED_COMBAT_ATTRIBUTES(workerStruct[idx].ped, workerStruct[idx].attribute, TRUE)
	SET_PED_COMBAT_MOVEMENT(workerStruct[idx].ped, workerStruct[idx].move)
	SET_PED_ACCURACY(workerStruct[idx].ped, 20)
ENDPROC

/// PURPOSE:
///    Create elevator props 
//PROC CREATE_ELEVATOR_DOORS()
//	//create closed elevator doors for elevator props that never move
//		//panic cutscene lift
//			IF NOT DOES_ENTITY_EXIST(objElevDoorClosed[0])
//				objElevDoorClosed[0] = CREATE_OBJECT(ElevDoorModel, <<  -179.430, -1014.410, 113 >>)
//				SET_ENTITY_ROTATION(objElevDoorClosed[0], <<0,1,70.24>>)
//			ENDIF
//			IF NOT DOES_ENTITY_EXIST(objElevDoorClosed[1])
//				objElevDoorClosed[1] = CREATE_OBJECT(ElevDoorModel, << -181.260, -1019.510, 113 >>)
//				SET_ENTITY_ROTATION(objElevDoorClosed[1], <<0,1,70.24>>)
//			ENDIF
//		//top level elevator
//			IF NOT DOES_ENTITY_EXIST(objElevDoorClosed[2])
//				objElevDoorClosed[2] = CREATE_OBJECT(ElevDoorModel, << -162.010, -943.190, 268.520 >>)
//				SET_ENTITY_ROTATION(objElevDoorClosed[2], << 0, 0, 339 >>)
//			ENDIF
//			IF NOT DOES_ENTITY_EXIST(objElevDoorClosed[3])
//				objElevDoorClosed[3] = CREATE_OBJECT(ElevDoorModel, << -156.850, -945.060, 268.520 >>)
//				SET_ENTITY_ROTATION(objElevDoorClosed[3], << 0, 0, 339 >>)
//			ENDIF
//	
//	//create dynamic elevator doors
//		//bottom entrance open
//			IF NOT DOES_ENTITY_EXIST(objElevDoorControl[0])
//				objElevDoorControl[0] = CREATE_OBJECT(ElevDoorModel, vMyElevDoor[0])
//				SET_ENTITY_ROTATION(objElevDoorControl[0], <<0,1,70.24>>)
//			ENDIF
//		//bottom exit
//			IF NOT DOES_ENTITY_EXIST(objElevDoorControl[1])
//				objElevDoorControl[1] = CREATE_OBJECT(ElevDoorModel, vMyElevDoor[1])
//				SET_ENTITY_ROTATION(objElevDoorControl[1], <<0,1,70.24>>)
//			ENDIF
			
			
		//START LOCATION FOR MOVING ELEVATOR
//		//second lift open front
//			IF NOT DOES_ENTITY_EXIST(objElevDoorControl[2])
//				objElevDoorControl[2] = CREATE_OBJECT(ElevDoorModel, vMyElevDoor[2])
//				SET_ENTITY_ROTATION(objElevDoorControl[2], <<0,0, 160.2>>)
//			ENDIF
//		//second lift closed back
//			IF NOT DOES_ENTITY_EXIST(objElevDoorControl[3])
//				objElevDoorControl[3] = CREATE_OBJECT(ElevDoorModel, vMyElevDoor[3])
//				SET_ENTITY_ROTATION(objElevDoorControl[3], <<0,0, 160.2>>)	
//			ENDIF
//ENDPROC

PROC CREATE_GROUND_ELEVATOR()
	objElev[0] = CREATE_OBJECT(ElevModel, vMyElevPos[0])
	SET_ENTITY_ROTATION(objElev[0], vMyElevRot[0])
	objElevButton[0] = CREATE_OBJECT(ElevButtonModel, vMyElevPos[0])
	ATTACH_ENTITY_TO_ENTITY(objElevButton[0], objElev[0], 0, vButtonOffset, <<0,0,0>>)
ENDPROC

PROC REMOVE_SECOND_ELEVATOR()
	IF DOES_ENTITY_EXIST(objElev[1])
		DELETE_OBJECT(objElev[1])
		PRINTLN("REMOVING ELEVATOR")
	ENDIF
	
	IF DOES_ENTITY_EXIST(objElevButton[1])
		DELETE_OBJECT(objElevButton[1])
		PRINTLN("REMOVING ELEVATOR BUTTON")
	ENDIF
ENDPROC

PROC CREATE_SECOND_ELEVATOR(BOOL bForCutscene = TRUE)
	IF NOT DOES_ENTITY_EXIST(objElev[1])
		IF bForCutscene
			objElev[1] = CREATE_OBJECT(ElevModel, vMyElevPos[1])	
		ELSE
			objElev[1] = CREATE_OBJECT(ElevModel, vMyElevPos[1])
			SET_ENTITY_COORDS(objElev[1], << -158.840, -942.240, 125.8 >>)
//			SET_ENTITY_COORDS(objElevDoorControl[2], << -155.650, -941.260, 125.03 >>)
//			SET_ENTITY_COORDS(objElevDoorControl[3], << -160.870, -939.400, 125.03 >>)
		ENDIF
		SET_ENTITY_ROTATION(objElev[1], vMyElevRot[1])
	ELSE
		SET_ENTITY_COORDS(objElev[1], << -158.840, -942.240, 125.8 >>)
//		SET_ENTITY_COORDS(objElevDoorControl[2], << -155.650, -941.260, 125.03 >>)
//		SET_ENTITY_COORDS(objElevDoorControl[3], << -160.870, -939.400, 125.03 >>)
		SET_ENTITY_ROTATION(objElev[1], vMyElevRot[1])
	ENDIF
	
	objElevButton[1] = CREATE_OBJECT(ElevButtonModel, vMyElevPos[01])
	ATTACH_ENTITY_TO_ENTITY(objElevButton[1], objElev[1], 0, vButtonOffset, <<0,0,0>>)
ENDPROC

/// PURPOSE: Create work truck and construction worker peds on ground level of site
PROC CREATE_VEHICLES()
	IF NOT DOES_ENTITY_EXIST(viTruck)
		viTruck = CREATE_VEHICLE(TruckModel, vTruckPos, fTruckHead)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(viCombatCar)
		viCombatCar = CREATE_VEHICLE(CombatCarModel, << -130.3431, -1096.4409, 21.3323 >>, 337)
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(viCombatCar, "ASSASSINATION_CONSTRUCT_CAR_GROUP")
	ENDIF
ENDPROC

/// PURPOSE: Setup Patrol paths
PROC SETUP_PATROL_PATHS()
	vPatrolPathPoint[0] = << -139.78418, -992.19635, 26.27540 >>
	vPatrolPathPoint[1] = << -119.22578, -986.67188, 26.28716  >>
	vPatrolPathPoint[2] = << -129.02084, -981.08838, 26.27540 >>
	OPEN_PATROL_ROUTE("miss_Ass0")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[0], << -139.40767, -993.47321, 26.27540 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[1], << -116.13918, -987.49841, 26.38541 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[2], << -128.46848, -979.03406, 26.27540 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	vPatrolPathPoint[3] = << -138.6994, -979.7902, 27.3835 >>
	vPatrolPathPoint[4] = << -168.2610, -970.3499, 28.3159 >>
	vPatrolPathPoint[5] = << -150.2031, -982.3716, 27.3835 >>
	OPEN_PATROL_ROUTE("miss_Ass1")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[3], << -138.0908, -980.1388, 26.3835 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[4], << 0,0,0 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[5], << -150.6753, -982.7073, 26.3835 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	vPatrolPathPoint[6] = << -119.74, -987.43, 27.2835 >>
	vPatrolPathPoint[7] = << -126.7858, -1005.1038, 26.2731 >>
	vPatrolPathPoint[8] = << -134.0714, -1033.0955, 26.2731 >>
	OPEN_PATROL_ROUTE("miss_Ass2")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[6], << 0,0,0 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[7], << 0,0,0 >>)
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[8], << 0,0,0 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	vPatrolPathPoint[9] = << -118.0965, -1040.0308, 26.2740 >>
	vPatrolPathPoint[10] = << -114.6962, -1053.8435, 26.2942 >>
	vPatrolPathPoint[11] = << -143.5082, -1044.1169, 26.2913 >>
	OPEN_PATROL_ROUTE("miss_Ass3")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[9], << -108.7927, -1033.7714, 26.2731 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[10], << -115.0062, -1054.9329, 26.2892 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[11], << -144.8010, -1043.6625, 26.2912 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	vPatrolPathPoint[12] = << -129.8310, -1003.8873, 26.2731 >>
	vPatrolPathPoint[13] = << -142.1477, -1007.6038, 26.2731 >>
	OPEN_PATROL_ROUTE("miss_Ass4")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[12], << -130.3284, -998.4552, 26.2731 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[13], << -142.5782, -1008.2757, 26.2731 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	vPatrolPathPoint[14] = << -193.2124, -1058.5308, 23.4276 >>
	vPatrolPathPoint[15] = << -176.1570, -1028.9437, 27.2832 >>
	vPatrolPathPoint[16] = << -170.2736, -1037.5872, 27.280 >>
	OPEN_PATROL_ROUTE("miss_Ass5")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[14], << -194.7176, -1062.0897, 21.8847 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[15], << -167.7465, -1038.0892, 26.2798 >>)
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[16], << -194.7176, -1062.0897, 21.8847 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	vPatrolPathPoint[17] = <<-180.4631, -1022.1616, 28.2893>>
	vPatrolPathPoint[18] = <<-188.5225, -1019.4911, 28.2893>>
	OPEN_PATROL_ROUTE("miss_Ass6")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[17], << -194.4449, -1017.9739, 28.2895 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[18], << -182.4519, -1021.0443, 28.2863 >>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
ENDPROC


/// PURPOSE:
///    Task peds on foot on ground level to attack player
PROC WAVE_1_WORKER_ASSAULT()
	INT i

	FOR i = 0 TO NUM_WAVE_1_WORKERS - 1
		IF NOT IS_PED_INJURED(wave1[i].ped)			
			IF i = 0
				SET_PED_COMBAT_MOVEMENT(wave1[i].ped, CM_WILLADVANCE)
				PRINTLN("CALLING CM_WILLADVANCE ON PED: ", i)
			ELIF i = 1
				SET_PED_COMBAT_MOVEMENT(wave1[i].ped, CM_WILLADVANCE)
				PRINTLN("CALLING CM_WILLADVANCE ON PED: ", i)
			ELIF i = 2
				SET_PED_SPHERE_DEFENSIVE_AREA(wave1[i].ped, wave1[i].loc, 10)
				PRINTLN("SET_PED_SPHERE_DEFENSIVE_AREA ON PED: ", i)
			ELIF i = 3
				SET_PED_SPHERE_DEFENSIVE_AREA(wave1[i].ped, wave1[i].loc, 10)
				PRINTLN("SET_PED_SPHERE_DEFENSIVE_AREA ON PED: ", i)
			ELIF i = 4
				SET_PED_COMBAT_MOVEMENT(wave1[i].ped, CM_WILLADVANCE)
				PRINTLN("CALLING CM_WILLADVANCE ON PED: ", i)
			ELSE
				SET_PED_SPHERE_DEFENSIVE_AREA(wave1[i].ped, << -176.2501, -1026.9131, 26.2752 >>, 25)
				SET_PED_COMBAT_MOVEMENT(wave1[i].ped, CM_DEFENSIVE)
				PRINTLN("CALLING CM_DEFENSIVE ON PED: ", i)
			ENDIF
			
			
			IF GET_SCRIPT_TASK_STATUS(wave1[i].ped, SCRIPT_TASK_PATROL) = PERFORMING_TASK
				SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(wave1[i].ped)
				CLEAR_PED_TASKS(wave1[i].ped)
				PRINTLN("CALLING CM_DEFENSIVE ON PED: ", i)
			ENDIF
			
			TASK_COMBAT_PED(wave1[i].ped, Player)
			SET_PED_KEEP_TASK(wave1[i].ped, TRUE)
			
//			//blip the guys
//			IF NOT DOES_BLIP_EXIST(wave1[i].blip)
//				wave1[i].blip = CREATE_BLIP_ON_ENTITY(wave1[i].ped, FALSE)
//			ENDIF	
		ENDIF
	ENDFOR
ENDPROC	

// send group of guys in car on ground level to drive near the elevator and attack the player
PROC WAVE_2_WORKER_ASSAULT()
	INT i
	
	IF DOES_ENTITY_EXIST(viCombatCar)
		IF IS_VEHICLE_DRIVEABLE(viCombatCar)
			IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(viCombatCar)
				FOR i = 0 TO NUM_WAVE_2_WORKERS - 1
					IF NOT IS_PED_INJURED(wave2[i].ped)
//						IF NOT DOES_BLIP_EXIST(wave2[i].blip)
//							wave2[i].blip = CREATE_BLIP_ON_ENTITY(wave2[i].ped, FALSE)
							IF wave2[i].ped = GET_PED_IN_VEHICLE_SEAT(viCombatCar)
								IF NOT IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_CAR_ARRIVES")
									START_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_CAR_ARRIVES")
								ENDIF
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(wave2[i].ped, viCombatCar, "OJAScs_101", DF_SwerveAroundAllCars|DF_SteerAroundObjects|DF_SteerAroundPeds)
								SET_PED_KEEP_TASK(wave2[i].ped, TRUE)
								bUpdateGuysInCar = TRUE
							ELSE
								TASK_COMBAT_PED(wave2[i].ped, Player)
								SET_PED_KEEP_TASK(wave2[i].ped, TRUE)
								bUpdateGuysInCar = TRUE
							ENDIF
//						ENDIF
					ENDIF
				ENDFOR
				
				bPanicking = TRUE
				bCarSent = TRUE
				PRINTLN("WAVE_2_WORKER_ASSAULT()")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Task peds on second level of site to attack player
PROC WAVE_3_WORKER_ASSAULT()
	INT i
	SEQUENCE_INDEX tempSeq
	

	FOR i = 0 TO NUM_WAVE_3_WORKERS - 1
		IF NOT IS_PED_INJURED(wave3[i].ped)
//			CLEAR_PED_TASKS_IMMEDIATELY(wave3[i].ped)
		
			IF GET_SCRIPT_TASK_STATUS(wave3[i].ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
				
				IF i = 0
					SET_PED_SPHERE_DEFENSIVE_AREA(wave3[i].ped, wave3[i].loc, 10)
					SET_ENTITY_LOAD_COLLISION_FLAG(wave3[i].ped, TRUE)
				ELIF i = 1	// guy behind pallets in center of the level as you round the first corner
					SET_PED_SPHERE_DEFENSIVE_AREA(wave3[i].ped, wave3[i].loc, 10)
				ELIF i = 2	// guy guarding 2nd elevator position
					SET_PED_SPHERE_DEFENSIVE_AREA(wave3[i].ped, << -151.3457, -948.0886, 113.1339 >>, 10)
				ELIF i = 3	// guy behind pallet prop on opposite corner of the floor
					SET_PED_SPHERE_DEFENSIVE_AREA(wave3[i].ped, << -145.5338, -986.0398, 113.1282 >>, 10)
				ELIF i = 4	//guy behind guard 3 guarding the area on opposite corner of the building
					SET_PED_SPHERE_DEFENSIVE_AREA(wave3[i].ped, << -139.7715, -968.0640, 113.1339 >>, 10)
				ELIF i = 5 // guy guarding center of the floor
					SET_PED_SPHERE_DEFENSIVE_AREA(wave3[i].ped, << -154.0288, -960.1566, 113.1317 >>, 10)
				ENDIF
			
				
				OPEN_SEQUENCE_TASK(tempSeq)
					IF i = 0
//						TASK_GO_STRAIGHT_TO_COORD(NULL, <<-162.70424, -999.03363, 113.13822>>, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_NEVER_WARP, 1.5)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-162.70424, -999.03363, 113.13822>>, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_RUN, FALSE, 1.5, 1.5, FALSE)
					ENDIF	
					TASK_COMBAT_PED(NULL, Player)
				CLOSE_SEQUENCE_TASK(tempSeq)
				TASK_PERFORM_SEQUENCE(wave3[i].ped, tempSeq)
				CLEAR_SEQUENCE_TASK(tempSeq)
				
				SET_PED_COMBAT_MOVEMENT(wave3[i].ped, CM_WILLADVANCE)
				SET_COMBAT_FLOAT(wave3[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3)
				SET_PED_KEEP_TASK(wave3[i].ped, TRUE)
				PRINTLN("SETTING WAVE 3 PED TO ADVANCE : ", i)
			ENDIF
		ENDIF	
	ENDFOR
ENDPROC


//guys in elevator should exit and attack the player when this is called
PROC WAVE_4_WORKER_ASSAULT()
	INT i
	SEQUENCE_INDEX tempSeq
	VECTOR vGoToCoordinate
	
	FOR i = 0 TO NUM_WAVE_4_WORKERS - 1
		IF NOT IS_PED_INJURED(wave4[i].ped)
			DETACH_ENTITY(wave4[i].ped, FALSE, FALSE)
			IF i = 0
				SET_PED_SPHERE_DEFENSIVE_AREA(wave4[i].ped, << -145.5655, -941.1028, 113.2452 >>, 10)
			ELIF i = 1
				SET_PED_SPHERE_DEFENSIVE_AREA(wave4[i].ped, << -152.5679, -947.0895, 113.1339 >>, 10)
			ENDIF
			
			SET_PED_KEEP_TASK(wave4[i].ped, TRUE)
			
			//go to the position right outside the elevator doors and attack the player (no navmesh inside of elevator)
			CLEAR_SEQUENCE_TASK(tempSeq)
			OPEN_SEQUENCE_TASK(tempSeq)
				IF i = 0
					vGoToCoordinate = <<-153.0079, -944.3201, 113.1388>>
				ELSE
					vGoToCoordinate = <<-152.1749, -941.6376, 113.1388>>
				ENDIF
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vGoToCoordinate, Player, PEDMOVE_RUN, FALSE, 0.5, 0)
				TASK_COMBAT_PED(NULL, Player)
			CLOSE_SEQUENCE_TASK(tempSeq)
			TASK_PERFORM_SEQUENCE(wave4[i].ped, tempSeq)
			CLEAR_SEQUENCE_TASK(tempSeq)
		ENDIF
	ENDFOR
ENDPROC

//task rooftop enemies to fight the player
PROC WAVE_5_WORKER_ASSAULT()
	SEQUENCE_INDEX tempSeq
	INT i
	
	FOR i = 0 TO NUM_WAVE_5_WORKERS - 1
		IF i = 0	//guy should run to position behind the lumber pallet and combat the player
			IF NOT IS_PED_INJURED(wave5[i].ped)
				IF IS_SCREEN_FADED_IN()
					SET_PED_SPHERE_DEFENSIVE_AREA(wave5[i].ped, wave5[i].loc, 15)
					CLEAR_SEQUENCE_TASK(tempSeq)
					OPEN_SEQUENCE_TASK(tempSeq)
						TASK_AIM_GUN_AT_ENTITY(NULL, Player, 250)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, << -141.36034, -946.22839, 268.13504 >>, Player, PEDMOVE_RUN, TRUE)
						TASK_COMBAT_PED(NULL, Player)
					CLOSE_SEQUENCE_TASK(tempSeq)
					TASK_PERFORM_SEQUENCE(wave5[i].ped, tempSeq)
					CLEAR_SEQUENCE_TASK(tempSeq)
					
					SET_PED_COMBAT_MOVEMENT(wave5[i].ped, CM_WILLADVANCE)
					SET_PED_KEEP_TASK(wave5[i].ped, TRUE)
				ENDIF
			ENDIF
		ELIF i = 1	//guy runs to position outside elevator doors 
			IF NOT IS_PED_INJURED(wave5[i].ped)
				CLEAR_SEQUENCE_TASK(tempSeq)
				OPEN_SEQUENCE_TASK(tempSeq)
					TASK_GO_TO_COORD_ANY_MEANS(NULL, << -140.1210, -959.6016, 268.2571 >>, PEDMOVE_SPRINT, NULL)
					TASK_COMBAT_PED(NULL, Player)
				CLOSE_SEQUENCE_TASK(tempSeq)
				TASK_PERFORM_SEQUENCE(wave5[i].ped, tempSeq)
				CLEAR_SEQUENCE_TASK(tempSeq)	
				
				SET_PED_COMBAT_MOVEMENT(wave5[i].ped, CM_WILLADVANCE)
				SET_PED_KEEP_TASK(wave5[i].ped, TRUE)
			ENDIF
		ELIF i = 2	
			IF NOT IS_PED_INJURED(wave5[i].ped)
				TASK_COMBAT_PED(wave5[i].ped, Player)
				SET_PED_SPHERE_DEFENSIVE_AREA(wave5[i].ped, wave5[i].loc, 10)
				
				SET_PED_COMBAT_MOVEMENT(wave5[i].ped, CM_WILLADVANCE)
				SET_PED_KEEP_TASK(wave5[i].ped, TRUE)
			ENDIF
		ENDIF
		
		PRINTLN("RAN WORK WAVE ASSAULT 5")
		
//		//blip the guys
//		IF NOT DOES_BLIP_EXIST(wave5[i].blip)
//			PRINTLN("ADDING BLIP FOR WORKER ", i)
//			wave5[i].blip = CREATE_BLIP_ON_ENTITY(wave5[i].ped, FALSE)
//		ENDIF	
	ENDFOR
ENDPROC

FUNC BOOL IS_PLAYER_ARMED()
	WEAPON_TYPE wtCurrentPedWeapon

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtCurrentPedWeapon)
		
		IF wtCurrentPedWeapon <> WEAPONTYPE_UNARMED
			PRINTLN("PLAYER IS ARMED")
			RETURN TRUE
		ELSE
			PRINTLN("PLAYER IS NOT ARMED")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SETUP_WARNING_GUY_PRE_COMBAT(INT idx)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(warningGuys[idx].ped, TRUE)
	GIVE_WEAPON_TO_PED(warningGuys[idx].ped, WEAPONTYPE_MICROSMG, -1, FALSE)
	SET_PED_COMBAT_MOVEMENT(warningGuys[idx].ped, CM_WILLADVANCE)
	SET_PED_COMBAT_RANGE(warningGuys[idx].ped, CR_FAR)
	TASK_PLAY_ANIM(warningGuys[idx].ped, "oddjobs@assassinate@construction@", "unarmed_fold_arms", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
	SET_PED_RELATIONSHIP_GROUP_HASH(warningGuys[idx].ped, relEnemies)
ENDPROC

PROC CREATE_WARNING_GUYS()
	IF NOT DOES_ENTITY_EXIST(warningGuys[0].ped)
		warningGuys[0].ped = CREATE_PED(PEDTYPE_MISSION, WorkerModel[1], <<-111.9202, -943.5192, 28.2288>>, 344.7112)
		SET_ENTITY_LOAD_COLLISION_FLAG(warningGuys[0].ped, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(warningGuys[0].ped, CA_CAN_CHARGE, TRUE)
		STOP_PED_WEAPON_FIRING_WHEN_DROPPED(warningGuys[0].ped)
		SETUP_WARNING_GUY_PRE_COMBAT(0)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(warningGuys[1].ped)
		warningGuys[1].ped = CREATE_PED(PEDTYPE_MISSION, WorkerModel[1], <<-197.8959, -1078.9697, 20.6882>>, 166.4473)
		SET_ENTITY_LOAD_COLLISION_FLAG(warningGuys[1].ped, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(warningGuys[1].ped, CA_CAN_CHARGE, TRUE)
		STOP_PED_WEAPON_FIRING_WHEN_DROPPED(warningGuys[1].ped)
		SETUP_WARNING_GUY_PRE_COMBAT(1)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(warningGuys[2].ped)
		warningGuys[2].ped = CREATE_PED(PEDTYPE_MISSION, WorkerModel[1], << -82.0262, -1023.1237, 27.3748 >>, 269.8311)
		SET_ENTITY_LOAD_COLLISION_FLAG(warningGuys[2].ped, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(warningGuys[2].ped, CA_CAN_CHARGE, TRUE)
		STOP_PED_WEAPON_FIRING_WHEN_DROPPED(warningGuys[2].ped)
		SETUP_WARNING_GUY_PRE_COMBAT(2)
	ENDIF
	
	PRINTLN("CREATING WARNING GUYS")
ENDPROC

FUNC BOOL UPDATE_AGGRO_WARNING_GUARDS()
	INT idx

	REPEAT NUM_WARNING_GUYS idx
		IF NOT IS_ENTITY_DEAD(warningGuys[idx].ped)
			IF DO_AGGRO_CHECK(warningGuys[idx].ped, NULL, aggroArgs, aggroReason, FALSE, TRUE, FALSE, FALSE)
				PRINTLN("RETURNING TRUE ON AGGRO CHECK 01, PED = ", idx)
				RETURN TRUE
			ENDIF
		ELSE
//			IF DOES_ENTITY_EXIST(warningGuys[idx].ped)
//				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(warningGuys[idx].ped, PLAYER_PED_ID())
//					PRINTLN("RETURNING TRUE ON AGGRO CHECK 02, PED = ", idx)
//					RETURN TRUE
//				ENDIF
//			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


FUNC BOOL UPDATE_WARNING_GUARDS()
//	VECTOR vPlayerPosition
//	VECTOR vGuardPosition[3]
	INT idx
	
	IF NOT IS_ENTITY_DEAD(Player)
		IF IS_ENTITY_IN_ANGLED_AREA(Player, <<-113.319, -941.315, -100>>, <<-171.373, -1095.764, 100>>, 100, FALSE, FALSE)
		OR IS_ENTITY_IN_ANGLED_AREA(Player, vAngledAreaMaxWarningGuy[0], vAngledAreaMinWarningGuy[0], fAngledAreaWidthWarningGuy[0])
		OR IS_ENTITY_IN_ANGLED_AREA(Player, vAngledAreaMaxWarningGuy[1], vAngledAreaMinWarningGuy[1], fAngledAreaWidthWarningGuy[1])
		OR IS_ENTITY_IN_ANGLED_AREA(Player, vAngledAreaMaxWarningGuy[2], vAngledAreaMinWarningGuy[2], fAngledAreaWidthWarningGuy[2])
			bPlayerInSite = TRUE
	//		PRINTLN("bPlayerInSite = TRUE")
		ELSE
			bPlayerInSite = FALSE
	//		PRINTLN("bPlayerInSite = FALSE")
		ENDIF
	ENDIF
	
	IF NOT bEarlyAggro 
		IF UPDATE_AGGRO_WARNING_GUARDS() OR bPanicking
			IF iWarningGuysUpdate < 5
				iWarningGuysUpdate = 5
				PRINTLN("bEarlyAggro = TRUE")
				bEarlyAggro = TRUE
			ENDIF
		ENDIF
	ENDIF
		
	SWITCH	iWarningGuysUpdate
		CASE 0
//			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//				vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
//			ENDIF
			REPEAT NUM_WARNING_GUYS idx
				IF NOT IS_PED_INJURED(warningGuys[idx].ped)
//					vGuardPosition[idx] = GET_ENTITY_COORDS(warningGuys[idx].ped)
//					PRINTLN("vGuardPosition FOR INDEX: ", idx, " IS = ", vGuardPosition[idx])
					
					IF bTaskedToAim[idx] 
						TASK_PLAY_ANIM(warningGuys[idx].ped, "oddjobs@assassinate@construction@", "unarmed_fold_arms", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
						bTaskedToAim[idx] = FALSE
//						PRINTLN("SETTING bTaskedToAim TO FALSE, INDEX = ", idx)
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(Player, vAngledAreaMaxWarningGuy[0], vAngledAreaMinWarningGuy[0], fAngledAreaWidthWarningGuy[0], FALSE)
						iWarningGuardIndex = 0
					ELIF IS_ENTITY_IN_ANGLED_AREA(Player, vAngledAreaMaxWarningGuy[1], vAngledAreaMinWarningGuy[1], fAngledAreaWidthWarningGuy[1], FALSE)
						iWarningGuardIndex = 1
					ELIF IS_ENTITY_IN_ANGLED_AREA(Player, vAngledAreaMaxWarningGuy[2], vAngledAreaMinWarningGuy[2], fAngledAreaWidthWarningGuy[2], FALSE)
						iWarningGuardIndex = 2
					ELSE
						iWarningGuardIndex = -1
					ENDIF
					
					IF iWarningGuardIndex != -1
						PRINTLN("PLAYER CLOSE TO GUARD ", iWarningGuardIndex)
										
						IF NOT IS_TIMER_STARTED(warningTimer)
							START_TIMER_NOW(warningTimer)
							PRINTLN("STARTING TIMER NOW")
						ELSE
							RESTART_TIMER_NOW(warningTimer)
							PRINTLN("RESTARTING TIMER NOW")
						ENDIF
						
						IF NOT IS_PED_INJURED(warningGuys[iWarningGuardIndex].ped)
							TASK_TURN_PED_TO_FACE_ENTITY(warningGuys[iWarningGuardIndex].ped, PLAYER_PED_ID(), -1)
	//						TASK_LOOK_AT_ENTITY(warningGuys[idx].ped, PLAYER_PED_ID(), -1)
						ENDIF

						PRINTLN("GOING TO CASE 1")
						iWarningGuysUpdate = 1
						BREAK
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		//=========================================================================================================================
		CASE 1
			// FIRST WARNING
			ADD_PED_FOR_DIALOGUE(siteConv, 8, warningGuys[iWarningGuardIndex].ped, "OJAScsWARN1")					
			IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_WRN1", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("PLAYING FIRST CONVERSATION")
			
				PRINTLN("GOING TO CASE 2")
				iWarningGuysUpdate = 2
			ENDIF
		BREAK
		//=========================================================================================================================
		CASE 2
			IF IS_TIMER_STARTED(warningTimer)
				IF GET_TIMER_IN_SECONDS(warningTimer) > iFirstTimeWarning
					IF NOT IS_PLAYER_ARMED()
					AND bPlayerInSite
						PRINTLN("GOING TO CASE 3")
						iWarningGuysUpdate = 3
					ELSE
						IF NOT IS_PLAYER_ARMED()
							PRINTLN("PLAYER HAS LEFT AREA AND IS NOT ARMED, GOING BACK TO FIRST STATE")
							iWarningGuysUpdate = 0
						ELSE
							PRINTLN("PLAYER IS ARMED: GOING TO CASE 3")
							iWarningGuysUpdate = 3
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//=========================================================================================================================
		CASE 3
			// SECOND WARNING
			IF NOT IS_PED_INJURED(warningGuys[iWarningGuardIndex].ped)
				ADD_PED_FOR_DIALOGUE(siteConv, 8, warningGuys[iWarningGuardIndex].ped, "OJAScsWARN1")
			
				IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_WRN2", CONV_PRIORITY_VERY_HIGH)
				
					IF NOT IS_PED_INJURED(warningGuys[iWarningGuardIndex].ped)
	//					CLEAR_PED_TASKS(warningGuys[iWarningGuardIndex].ped)
						TASK_AIM_GUN_AT_ENTITY(warningGuys[iWarningGuardIndex].ped, PLAYER_PED_ID(), -1, TRUE)
						PRINTLN("TASKING GUY TO AIM AT PLAYER")
						bTaskedToAim[iWarningGuardIndex] = TRUE
					ENDIF
					
					IF IS_TIMER_STARTED(warningTimer)
						RESTART_TIMER_NOW(warningTimer)
						PRINTLN("CASE 3 - RESTARTING TIMER NOW")
					ENDIF
					
					PRINTLN("GOING TO CASE 4")
					iWarningGuysUpdate = 4
				ENDIF
			ELSE
				iWarningGuysUpdate = 0
				PRINTLN("GOING BACK TO TO CASE 0")
			ENDIF
		BREAK
		//=========================================================================================================================
		CASE 4
			IF IS_TIMER_STARTED(warningTimer)
				IF GET_TIMER_IN_SECONDS(warningTimer) > iSecondTimeWarning
					IF bPlayerInSite
						ADD_PED_FOR_DIALOGUE(siteConv, 8, warningGuys[iWarningGuardIndex].ped, "OJAScsWARN1")					
						CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_WRN3", CONV_PRIORITY_VERY_HIGH)
						
						PRINTLN("GOING TO CASE 5")
						iWarningGuysUpdate = 5
					ELSE
						PRINTLN("SECOND WARNING: PLAYER HAS LEFT AREA AND IS NOT ARMED, GOING BACK TO FIRST STATE")
						iWarningGuysUpdate = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//=========================================================================================================================
		CASE 5
			REPEAT NUM_WARNING_GUYS idx
				IF NOT IS_PED_INJURED(warningGuys[idx].ped)
					CLEAR_PED_TASKS(warningGuys[idx].ped)
					TASK_COMBAT_PED(warningGuys[idx].ped, PLAYER_PED_ID())
					PRINTLN("TASKING GUYS TO COMBAT: ", idx)
				ENDIF
			ENDREPEAT
			
			PRINTLN("bPanicking = TRUE VIA UPDATE_WARNING_GUARDS")
			bPanicking = TRUE
			
			PRINTLN("GOING TO CASE 6")
			iWarningGuysUpdate = 6
		BREAK
		//=========================================================================================================================
		CASE 6
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


PROC  UPDATE_PATROLLING_GUARDS()
	
	SWITCH workerState
		CASE WS_PATROL
			IF bPlayerSpotted
			OR GET_NUMBER_OF_FIRES_IN_RANGE(vTrailEnd, 10) > 0
				workerState = WS_REACT
			ELSE
				//currently they are getting tasked to patrol inside of CREATE_WORKER_WAVE - should maybe eventually move here
			ENDIF
		BREAK
		CASE WS_REACT
			IF NOT IS_TIMER_STARTED(spottedTimer)
				RESTART_TIMER_NOW(spottedTimer)
				
				IF NOT IS_PED_INJURED(pedSpotted)
					ADD_PED_FOR_DIALOGUE(siteConv, 4, pedSpotted, "OJAcsGUARD")
					CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_ALERT", CONV_PRIORITY_HIGH)
					
					IF GET_SCRIPT_TASK_STATUS(pedSpotted, SCRIPT_TASK_PATROL) = PERFORMING_TASK
						SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedSpotted)
						CLEAR_PED_TASKS(pedSpotted)
						PRINTLN("spottedTimer STARTED - PLAY LINE!!")
					ENDIF
								
					IF NOT IS_PED_IN_ANY_VEHICLE(pedSpotted)
						IF GET_DISTANCE_BETWEEN_ENTITIES(pedSpotted, Player) > 5
							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(pedSpotted, Player, Player, PEDMOVEBLENDRATIO_RUN, FALSE)
						ELSE
							TASK_AIM_GUN_AT_ENTITY(pedSpotted, Player, -1)
						ENDIF
					ENDIF
				ENDIF
			ELIF GET_TIMER_IN_SECONDS(spottedTimer) > 0.25
				bPanicking = TRUE
				
				workerState = WS_COMBAT
			ENDIF
		BREAK
		CASE WS_COMBAT
			//peds have been tasked to combat
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Allows spawning of waves of enemies in sequences
PROC CREATE_WORKER_WAVE(INT waveNumber)		
	INT i
	INT iNumToSubtract
	
	//ground level - create the guys and tell them to patrol their assigned path if they have one
	IF waveNumber = 1
		FOR i = 0 TO NUM_WAVE_1_WORKERS - 1
			IF NOT DOES_ENTITY_EXIST(wave1[i].ped)
			AND NOT IS_VECTOR_ZERO(wave1[i].loc)
				//mix up enemy enums
				IF i % 2 = 0 
					wave1[i].ped = CREATE_PED(PEDTYPE_MISSION, WorkerModel[0], wave1[i].loc, wave1[i].head, TRUE)
					SET_PED_COMPONENT_VARIATION(wave1[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, GET_RANDOM_INT_IN_RANGE(1, 4), 0) //(uppr)	// 0 is reserved for the target
				ELSE
					wave1[i].ped = CREATE_PED(PEDTYPE_MISSION, WorkerModel[1], wave1[i].loc, wave1[i].head, TRUE)
				ENDIF
			
				IF i = 0
					TASK_PATROL(wave1[i].ped, "miss_Ass0", PAS_ALERT)
				ELIF i = 1
					TASK_PATROL(wave1[i].ped, "miss_Ass4", PAS_ALERT)
//				ELIF i = 2
//					TASK_PATROL(wave1[i].ped, "miss_Ass2", PAS_ALERT)
//				ELIF i = 3
//					TASK_PATROL(wave1[i].ped, "miss_Ass1", PAS_ALERT)
				ELIF i = 2
//					TASK_PATROL(wave1[i].ped, "miss_Ass5", PAS_ALERT)
					TASK_GUARD_CURRENT_POSITION(wave1[i].ped, 10, 10, TRUE)
				ELIF i = 3
					TASK_PATROL(wave1[i].ped, "miss_Ass6", PAS_ALERT)
				ELIF i = 4
					TASK_PATROL(wave1[i].ped, "miss_Ass3", PAS_ALERT)
//				ELSE
//					TASK_GUARD_CURRENT_POSITION(wave1[i].ped, 10, 10, TRUE)
				ENDIF
				
				SET_WORKER_PARAMS(wave1, i, FALSE)
				SET_ENTITY_LOAD_COLLISION_FLAG(wave1[i].ped, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(wave1[i].ped, CA_CAN_CHARGE, TRUE)
				STOP_PED_WEAPON_FIRING_WHEN_DROPPED(wave1[i].ped)
			ENDIF
		ENDFOR
	ENDIF
	
	//guys in car on ground level - create them inside of their vehicle and have them hang out there until told to attack
	IF waveNumber = 2
		FOR i = 0 TO NUM_WAVE_2_WORKERS - 1
			IF NOT DOES_ENTITY_EXIST(wave2[i].ped)
				IF DOES_ENTITY_EXIST(viCombatCar)
					IF IS_VEHICLE_DRIVEABLE(viCombatCar)
						IF i = 0
							wave2[i].ped = CREATE_PED_INSIDE_VEHICLE(viCombatCar, PEDTYPE_MISSION, WorkerModel[0])
							SET_PED_COMPONENT_VARIATION(wave2[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, GET_RANDOM_INT_IN_RANGE(1, 4), 0) //(uppr)	// 0 is reserved for the target
						ELIF i = 1
							wave2[i].ped = CREATE_PED_INSIDE_VEHICLE(viCombatCar, PEDTYPE_MISSION, WorkerModel[0], VS_FRONT_RIGHT)
							SET_PED_COMPONENT_VARIATION(wave2[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, GET_RANDOM_INT_IN_RANGE(1, 4), 0) //(uppr)	// 0 is reserved for the target
						ELIF i = 2
							wave2[i].ped = CREATE_PED_INSIDE_VEHICLE(viCombatCar, PEDTYPE_MISSION, WorkerModel[1], VS_BACK_LEFT)
						ENDIF
						SET_PED_COMBAT_ATTRIBUTES(wave2[i].ped, CA_LEAVE_VEHICLES, FALSE)
					ENDIF
					
					SET_WORKER_PARAMS(wave2, i)
					SET_PED_COMBAT_ATTRIBUTES(wave2[i].ped, CA_CAN_CHARGE, TRUE)
					STOP_PED_WEAPON_FIRING_WHEN_DROPPED(wave2[i].ped)
				ENDIF
			ENDIF
		ENDFOR
		
	//mid level enemies - create them at their assigned locations
	ELIF waveNumber = 3
		FOR i = 0 TO NUM_WAVE_3_WORKERS - 1
			IF NOT DOES_ENTITY_EXIST(wave3[i].ped)
				IF i % 2 = 0 
					wave3[i].ped = CREATE_PED(PEDTYPE_MISSION, WorkerModel[0], wave3[i].loc, wave3[i].head, TRUE)
					SET_PED_COMPONENT_VARIATION(wave3[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, GET_RANDOM_INT_IN_RANGE(1, 4), 0) //(uppr)	// 0 is reserved for the target
				ELSE
					wave3[i].ped = CREATE_PED(PEDTYPE_MISSION, WorkerModel[1], wave3[i].loc, wave3[i].head, TRUE)
				ENDIF

				SET_WORKER_PARAMS(wave3, i)
				SET_PED_COMBAT_ATTRIBUTES(wave3[i].ped, CA_CAN_CHARGE, TRUE)
				STOP_PED_SPEAKING(wave3[i].ped, TRUE)
				
				IF i = 0
					IF NOT IS_PED_INJURED(wave3[i].ped)
						TASK_PLAY_ANIM(wave3[i].ped, "oddjobs@assassinate@construction@", "unarmed_fold_arms", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
	//group of enemies descending in elevator	
	ELIF waveNumber = 4
		FOR i = 0 TO NUM_WAVE_4_WORKERS - 1
			IF NOT DOES_ENTITY_EXIST(wave4[i].ped)
				//specifically set these guys uniforms since they will be recreated as corpses later in the script if the player kills them in the elevator
				IF i % 2 = 0 
					wave4[i].ped = CREATE_PED(PEDTYPE_MISSION, WorkerModel[0], wave4[i].loc, wave4[i].head, TRUE)
					SET_PED_COMPONENT_VARIATION(wave4[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, GET_RANDOM_INT_IN_RANGE(1, 4), 0) //(uppr)	// 0 is reserved for the target
					TASK_AIM_GUN_AT_ENTITY(wave4[i].ped, PLAYER_PED_ID(), -1)
					SET_PED_COMPONENT_VARIATION(wave4[i].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(wave4[i].ped, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(wave4[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(wave4[i].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(wave4[i].ped, INT_TO_ENUM(PED_COMPONENT,11), 1, 0, 0) //(jbib)
				ELSE
					wave4[i].ped = CREATE_PED(PEDTYPE_MISSION, WorkerModel[1], wave4[i].loc, wave4[i].head, TRUE)
					TASK_AIM_GUN_AT_ENTITY(wave4[i].ped, PLAYER_PED_ID(), -1)
					SET_PED_COMPONENT_VARIATION(wave4[i].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
					SET_PED_COMPONENT_VARIATION(wave4[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(wave4[i].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(wave4[i].ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
					SET_PED_COMPONENT_VARIATION(wave4[i].ped, INT_TO_ENUM(PED_COMPONENT,11), 1, 1, 0) //(jbib)
				ENDIF
			
				IF DOES_ENTITY_EXIST(objElev[1])
					IF i = 0
						ATTACH_ENTITY_TO_ENTITY(wave4[i].ped, objElev[1], 0, vPlayerElevatorOffset, <<0, 0, 84.24 >>)
						ADD_PED_FOR_DIALOGUE(siteConv, 4, wave4[i].ped, "OJAcsGUARD")
					ELIF i = 1
						ATTACH_ENTITY_TO_ENTITY(wave4[i].ped, objElev[1], 0, << -1.34, -2.36, -1.34 >>, <<0, 0, 84.24 >>)
						ADD_PED_FOR_DIALOGUE(siteConv, 5, wave4[i].ped, "OJAcsGUARD2")
					ENDIF
				ENDIF
			ENDIF
			
//			//blip the guys when created
//			IF NOT DOES_BLIP_EXIST(wave4[i].blip)
//				wave4[i].blip = CREATE_BLIP_ON_ENTITY(wave4[i].ped, FALSE)
//				SET_BLIP_PRIORITY(wave4[i].blip, BLIPPRIORITY_HIGHEST)
//			ENDIF
			
			SET_WORKER_PARAMS(wave4, i)
			SET_PED_COMBAT_ATTRIBUTES(wave4[i].ped, CA_CAN_CHARGE, TRUE)
		ENDFOR
		
	//create rooftop enemies aqnd have them immedaitely attack after creation
	ELIF waveNumber = 5
		IF NOT bRooftopWaveCreated
		
			IF bTriggerRooftop
				iNumToSubtract = 1
			ELSE
				iNumToSubtract = 1
			ENDIF
		
			FOR i = 0 TO NUM_WAVE_5_WORKERS - iNumToSubtract
				IF NOT DOES_ENTITY_EXIST(wave5[i].ped)
					IF i % 2 = 0 
						wave5[i].ped = CREATE_PED(PEDTYPE_MISSION, WorkerModel[0], wave5[i].loc, wave5[i].head, TRUE)
						SET_PED_COMPONENT_VARIATION(wave5[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, GET_RANDOM_INT_IN_RANGE(1, 4), 0) //(uppr)	// 0 is reserved for the target
						PRINTLN("CREATED PED WITH INDEX = ", i)
					ELSE
						wave5[i].ped = CREATE_PED(PEDTYPE_MISSION, WorkerModel[1], wave5[i].loc, wave5[i].head, TRUE)
						PRINTLN("CREATED PED WITH INDEX = ", i)
					ENDIF
					
					//guy on crane
//					IF NOT bTriggerRooftop
						IF i = 3
							TASK_AIM_GUN_AT_ENTITY(wave5[i].ped, PLAYER_PED_ID(), -1, TRUE)
							FREEZE_ENTITY_POSITION(wave5[i].ped, TRUE)
						ENDIF
//					ELSE
//						PRINTLN("NOT TASKING GUY 3")
//					ENDIF
					SET_WORKER_PARAMS(wave5, i)
					SET_PED_COMBAT_ATTRIBUTES(wave5[i].ped, CA_CAN_CHARGE, TRUE)
				ENDIF
//				//blip the guys
//				IF NOT DOES_BLIP_EXIST(wave5[i].blip)
//					wave5[i].blip = CREATE_BLIP_ON_ENTITY(wave5[i].ped, FALSE)
//				ENDIF
			ENDFOR
			bRooftopWaveCreated = TRUE
		ENDIF
	
	//guys in back of helictoper
	ELIF waveNumber = 6
		FOR i = 0 TO NUM_WAVE_6_WORKERS - 1
			IF NOT DOES_ENTITY_EXIST(wave6[i].ped)
				IF IS_VEHICLE_DRIVEABLE(viHeli)
					IF i = 0
						wave6[i].ped = CREATE_PED_INSIDE_VEHICLE(viHeli, PEDTYPE_MISSION, WorkerModel[0], VS_DRIVER)
						SET_PED_COMPONENT_VARIATION(wave6[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, GET_RANDOM_INT_IN_RANGE(1, 4), 0) //(uppr)	// 0 is reserved for the target
						GIVE_WEAPON_TO_PED(wave6[0].ped, WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO, TRUE)
					ELIF i = 1
						wave6[1].ped = CREATE_PED_INSIDE_VEHICLE(viHeli, PEDTYPE_MISSION, WorkerModel[1], VS_BACK_RIGHT)
						GIVE_WEAPON_TO_PED(wave6[1].ped, WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE)
//						TASK_COMBAT_PED(wave6[i].ped, Player)
					ELIF i = 2
						wave6[2].ped = CREATE_PED_INSIDE_VEHICLE(viHeli, PEDTYPE_MISSION, WorkerModel[0], VS_BACK_LEFT)
						SET_PED_COMPONENT_VARIATION(wave6[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, GET_RANDOM_INT_IN_RANGE(1, 4), 0) //(uppr)	// 0 is reserved for the target
						GIVE_WEAPON_TO_PED(wave6[2].ped, WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE)
//						TASK_COMBAT_PED(wave6[i].ped, Player)
					ENDIF
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(wave6[i].ped, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(wave6[i].ped, CA_LEAVE_VEHICLES, FALSE)
					SET_PED_COMBAT_MOVEMENT(wave6[i].ped, CM_WILLADVANCE)
					SET_PED_ACCURACY(wave6[i].ped, 0)
					SET_PED_RELATIONSHIP_GROUP_HASH(wave6[i].ped, relEnemies)
					SET_PED_COMBAT_ATTRIBUTES(wave6[i].ped, CA_CAN_CHARGE, TRUE)
					SET_PED_CONFIG_FLAG(wave6[i].ped, PCF_FallsOutOfVehicleWhenKilled, TRUE)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC	



/// PURPOSE:
///    Create the assassination target and set up attributes
PROC CREATE_TARGET(BOOL bForIntroCutscene)	
	IF NOT DOES_ENTITY_EXIST(piTarget)
	
		IF bForIntroCutscene
			piTarget = CREATE_PED(PEDTYPE_MISSION, WorkerModel[0], << -153.2591, -941.5044, 113.1339 >>, 263)
			SET_PED_DEFAULT_COMPONENT_VARIATION(piTarget)
			ADD_PED_FOR_DIALOGUE(siteConv, 3, piTarget, "MAFIABOSS")
			
			piCutscenePed = CREATE_PED(PEDTYPE_MISSION, WorkerModel[1], << -153.6352, -942.8479, 113.1339 >>, 66)
			SET_PED_RANDOM_COMPONENT_VARIATION(piCutscenePed)
			GIVE_WEAPON_TO_PED(piCutscenePed, WEAPONTYPE_SMG, INFINITE_AMMO, TRUE)
		ENDIF

		IF NOT IS_PED_INJURED(piCutscenePed)
			ADD_PED_FOR_DIALOGUE(siteConv, 5, piCutscenePed, "OJAcsGUARD2")
		ENDIF
	
		SET_PED_RELATIONSHIP_GROUP_HASH(piTarget, relEnemies)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piTarget, TRUE)
	ENDIF
ENDPROC


/// PURPOSE:
///    Handles the players max wanted level settings during the mission
PROC HANDLE_PLAYER_MAX_WANTED_LEVEL()
	IF Mainstage > STAGE_PANIC_CUTSCENE
	AND Mainstage < STAGE_CLEANUP
		//disable emergency response
		IF GET_MAX_WANTED_LEVEL() != 0
			SET_MAX_WANTED_LEVEL(0)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
			ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
			SET_CREATE_RANDOM_COPS(FALSE)
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_HELI_PILOT_ALIVE()
	IF DOES_ENTITY_EXIST(wave6[0].ped)
		IF NOT IS_PED_INJURED(wave6[0].ped)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_PLAYER_ABANDONED_TARGET(BOOL& bFailType)
	VECTOR vTempPosPlayer
	
	IF HAS_TARGET_ESCAPED(piTarget, 250)
	OR ( IS_HELI_PILOT_ALIVE() AND HAS_TARGET_ESCAPED(piTarget, 250, 325) )			//to make sure that we dont fail prematurely if the pilot is killed and the heli is falling with the alive target still inside
		PRINTLN("HAS_PLAYER_ABANDONED_TARGET - HAS TARGET ESCAPED RETURNED TRUE")
		RETURN TRUE
	ELSE
		IF Mainstage >= STAGE_GET_TO_SECOND_ELEVATOR
//			IF Mainstage < STAGE_SECOND_ELEVATOR_CUTSCENE
//				vTempPosPlayer = GET_ENTITY_COORDS(Player)
//				IF vTempPosPlayer.z < vElev1Top.z - 50
//					bFailType = TRUE
//					PRINTLN("HAS_PLAYER_ABANDONED_TARGET - vTempPosPlayer.z < vElev1Top.z - 50")
//					RETURN TRUE
//				ENDIF
//			ELSE
				IF MainStage = STAGE_ROOFTOP_BATTLE
					vTempPosPlayer = GET_ENTITY_COORDS(Player)
//					IF bTriggerRooftop
//						IF DOES_ENTITY_EXIST(piTarget)
//						IF VDIST2(vTempPosPlayer, vElev2Top) > 122500 //350m
//							PRINTLN("bTriggerRooftop - HAS_PLAYER_ABANDONED_TARGET")
//							RETURN TRUE
//						ENDIF
//					ELSE
					IF NOT bTriggerRooftop
						IF vTempPosPlayer.z < vElev2Top.z - 50
						AND (DOES_ENTITY_EXIST(piTarget) AND NOT IS_PED_INJURED(piTarget) AND NOT IS_PED_IN_ANY_VEHICLE(piTarget))
							PRINTLN("HAS_PLAYER_ABANDONED_TARGET - vTempPosPlayer.z < vElev2Top.z - 50 AND TARGET NOT KILLED")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
//			ENDIF
		ELSE
			//only check this when the screen is faded in so it doesn't try and get the player's position while the screen is faded out if they have died 
			//and are retrying the mission (since they have been warped to a hospital far away while the screen is faded when this is the case)
			IF IS_SCREEN_FADED_IN()
				IF MainStage < STAGE_PANIC_CUTSCENE
					IF HAS_PLAYER_ABANDONED_ASSASSINATION_AREA(bWarningHelp, vSitePos, "ASS_CS_RTNY", 500)
						bFailType = TRUE
						RETURN TRUE
					ENDIF
				ELSE
					IF HAS_PLAYER_ABANDONED_ASSASSINATION_AREA(bWarningHelp, vSitePos, "ASS_CS_RTNR", 250)
						bFailType = TRUE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_PLAYER_IN_CONSTRUCTION_AREA()
	
	IF IS_ENTITY_IN_ANGLED_AREA(Player, <<-140.629898,-1029.230835,26.298155>>, <<-109.261307,-943.294983,29.229403>>, 107.00, FALSE, FALSE)
	OR IS_ENTITY_IN_ANGLED_AREA(Player, <<-169.274902,-1096.598145,18.688881>>, <<-142.460571,-1029.004639,27.310280>>, 99.50, FALSE, FALSE)
//		PRINTLN("Player is in Site!")
		RETURN TRUE
	ELSE
//		PRINTLN("Player not in Site!")
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL DO_ADDITIONAL_PANIC_CHECKS(PED_INDEX pedToCheck)
	FLOAT fDistFromPlayer
	
	IF DOES_ENTITY_EXIST(pedToCheck)
		IF NOT IS_PED_INJURED(pedToCheck)
			IF IS_PLAYER_IN_CONSTRUCTION_AREA()
//			AND CAN_PED_SEE_HATED_PED(pedToCheck, Player)
				fDistFromPlayer = GET_PLAYER_DISTANCE_FROM_ENTITY(pedToCheck, FALSE)
				IF ( fDistFromPlayer <= 33 AND CAN_PED_SEE_HATED_PED(pedToCheck, Player) )
				OR ( IS_PED_IN_ANY_VEHICLE(Player) AND fDistFromPlayer <= 5 )
					PRINTLN("pedWorker freaking out because player is SPOTTED!")
					RETURN TRUE
				ENDIF
			ENDIF
//		ELSE
//			PRINTLN("panicking because ped is injured!")
//			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RELOAD_PLAYERS_WEAPON()
	WEAPON_TYPE wtReturn
	INT iBullets, iMaxAmmo, iTotalAmmo

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtReturn)
		
		IF wtReturn = WEAPONTYPE_UNARMED	//we are holstering the weapon prior to using the sync scene now so I added this - 4/20/13 MB)
		AND (wtElevatorWeapon != WEAPONTYPE_UNARMED AND wtElevatorWeapon != WEAPONTYPE_INVALID)
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtElevatorWeapon, TRUE)
			
			GET_AMMO_IN_CLIP(PLAYER_PED_ID(), wtElevatorWeapon, iBullets)
			PRINTLN("AMMO IN CLIP = ", iBullets)
//			
			iMaxAmmo = GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), wtElevatorWeapon)
			PRINTLN("MAX AMMO IN CLIP = ", iMaxAmmo)
			
			iTotalAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtElevatorWeapon)
			PRINTLN("TOTAL AMMO IN INVENTORY = ", iTotalAmmo)
			
			IF iBullets < iMaxAmmo
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE) !=	PERFORMING_TASK
			AND iTotalAmmo > iBullets

				TASK_RELOAD_WEAPON(PLAYER_PED_ID(), TRUE)
				PRINTLN("SETTING CLIP TO HAVE MAX AMMO: ", iMaxAmmo)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    do panic checks for enemies on ground level of the site - check one ped per frame since aggro checks are expensive
PROC DO_ENEMY_PANIC_CHECKS()
	INT idx

	//check one ped per frame - switch between guys on foot and guys in car (waves 1 and 2)
	IF NOT bPanicking
		//wave 1 guys on foot on ground level
		IF iFrameCountAsn = 0
			FOR idx = 0 TO 2
				IF DO_AGGRO_CHECK(wave1[idx].ped, NULL, aggroArgs, aggroReason, FALSE, TRUE, FALSE, FALSE)		//don't instantly return true if the is dead
				OR CHECK_ATTACKED_ALLOW_STEALTH(wave1[idx].ped, NULL)
					bPanicking = TRUE
					PRINTLN("bPanicking = TRUE SET BY WAVE 1 WORKER idx = ", idx)
					EXIT
				ENDIF
			ENDFOR
		ELIF iFrameCountAsn = 1
			FOR idx = 3 TO NUM_WAVE_1_WORKERS - 1
				IF DO_AGGRO_CHECK(wave1[idx].ped, NULL, aggroArgs, aggroReason, FALSE, TRUE, FALSE, FALSE)		//don't instantly return true if the is dead
				OR CHECK_ATTACKED_ALLOW_STEALTH(wave1[idx].ped, NULL)
					bPanicking = TRUE
					PRINTLN("bPanicking = TRUE SET BY WAVE 1 WORKER idx = ", idx)
					EXIT
				ENDIF
			ENDFOR
		ELIF iFrameCountAsn = 2
			FOR idx = 0 TO NUM_WAVE_2_WORKERS - 1
				IF DO_AGGRO_CHECK(wave2[idx].ped, NULL, aggroArgs, aggroReason, FALSE, TRUE, FALSE, FALSE)		//don't instantly return true if the is dead
				OR CHECK_ATTACKED_ALLOW_STEALTH(wave2[idx].ped, NULL)
					bPanicking = TRUE
					PRINTLN("bPanicking = TRUE SET BY WAVE 2 WORKER idx = ", idx)
					EXIT
				ENDIF
			ENDFOR
		ENDIF
			
		//increment frame counter by 1
		iFrameCountAsn ++
		
		//once we have checked all the guys in each wave, switch to check the next wave
		IF iFrameCountAsn > 2
			//re-initialize this
			iFrameCountAsn = 0
		ENDIF
		
		
		
		
		
		IF NOT bPlayerSpotted
			INT iHatedIdxCheck
			
			//run these for all peds every frame
			REPEAT NUM_WAVE_1_WORKERS iHatedIdxCheck
				IF DO_ADDITIONAL_PANIC_CHECKS(wave1[iHatedIdxCheck].ped)
					pedSpotted = wave1[iHatedIdxCheck].ped
					bPlayerSpotted = TRUE
					PRINTLN("bPlayerSpotted = TRUE.  wave1[", idx)
					EXIT
				ENDIF
			ENDREPEAT
			
			//re-initalize
			iHatedIdxCheck = 0
			
	//		IF NOT bPlayerSpotted		
			//run these for all peds every frame
			REPEAT NUM_WAVE_2_WORKERS iHatedIdxCheck
				IF DO_ADDITIONAL_PANIC_CHECKS(wave2[iHatedIdxCheck].ped)
					pedSpotted = wave2[iHatedIdxCheck].ped
					bPlayerSpotted = TRUE
					PRINTLN("bPlayerSpotted = TRUE.  wave2[", idx)
					EXIT
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC


///// PURPOSE:
/////    Play kill camera of helicopter pilot dying
//FUNC BOOL HAS_HELI_KILL_CAM_FINISHED()
//
//	SWITCH iCutsceneStage
//		CASE 0
//			ODDJOB_ENTER_CUTSCENE()
//			GET_ASSASSIN_CUTSCENE_START_TIME()
//			CLEAR_PRINTS()
//			SET_ENTITY_INVINCIBLE(Player, TRUE)
//			IF NOT IS_PED_IN_ANY_VEHICLE(Player)
//				FREEZE_ENTITY_POSITION(Player, TRUE)
//			ELSE
//				VEHICLE_INDEX vehTemp
//				vehTemp = GET_VEHICLE_PED_IS_IN(Player)
//				FREEZE_ENTITY_POSITION(vehTemp, TRUE)
//			ENDIF
//			SET_CAM_ACTIVE(camKillcam, TRUE)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			
//			SET_TIME_SCALE(0.3)
//			SETTIMERB(0)
//			iCutsceneStage++
//		BREAK
//		
//		CASE 1
//			//handle slow motion effect for the split second at the beginning of the kill camera
//			float fGameSpeed
//			fGameSpeed = TO_FLOAT(TIMERB() - 40) / 220.0
//			If fGameSpeed > 1.0
//				fGameSpeed = 1.0
//			ENDIF
//			IF fGameSpeed < 0.3
//				fGameSpeed = 0.3
//			endif
//			SET_TIME_SCALE(fGameSpeed)
//			
//			IF TIMERB() > 1800
//			OR HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
//			OR ( IS_ENTITY_OCCLUDED(viHeli) AND IS_ENTITY_OCCLUDED(piTarget) )
//				ODDJOB_EXIT_CUTSCENE()
////				SET_PLAYER_WANTED_LEVEL_NO_DROP_NOW(2)
//				SET_ENTITY_INVINCIBLE(Player, FALSE)
//				IF NOT IS_PED_IN_ANY_VEHICLE(Player)
//					FREEZE_ENTITY_POSITION(Player, FALSE)
//				ELSE
//					VEHICLE_INDEX vehTemp
//					vehTemp = GET_VEHICLE_PED_IS_IN(Player)
//					FREEZE_ENTITY_POSITION(vehTemp, FALSE)
//				ENDIF
//				
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				CLEAR_HELP(TRUE)
//				PRINTLN("KILL CAM FINISHED")
//				
//				CUTSCENE_SKIP_FADE_IN()
//				SET_TIME_SCALE(1.0)
//				
//				RETURN TRUE
//			ENDIF
//		BREAK
//	ENDSWITCH
//	
//	RETURN FALSE
//ENDFUNC


// Perform any special commands if the script passes
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Passed()	

	SWITCH iEndingScreenStages
		CASE 0
			IF NOT bResultsCreated
				IF NOT bRidingFinalElevator
					killType = KT_BONUS
					killType = killType
					bBonus = TRUE
					ASSASSIN_MISSION_MarkMissionPassedWithBonus()
					PRINTLN("CONSTRUCTION - PLAYER GOT BONUS")
				ELSE
					killType = KT_STANDARD
					bBonus = FALSE
					PRINTLN("CONSTRUCTION - NO BONUS AWARDED")
				ENDIF
				
				g_savedGlobals.sAssassinData.fConstructionMissionTime = GET_TIMER_IN_SECONDS(missionTimer)
				PRINTLN("GLOBAL CONSTRUCTION MISSION TIME = ", g_savedGlobals.sAssassinData.fConstructionMissionTime)
				
				fTimeTaken = g_savedGlobals.sAssassinData.fConstructionMissionTime
				PRINTLN("fTimeTaken = ", fTimeTaken)
				
				SETUP_RESULTS_UI()
				bResultsCreated = TRUE
				
				PRINTLN("iEndingScreenStages = 1")
				iEndingScreenStages = 1
			ENDIF
		BREAK
		CASE 1
			IF NOT bPlayMusic
				PLAY_MISSION_COMPLETE_AUDIO("FRANKLIN_BIG_01")
				bPlayMusic = TRUE
			ENDIF
		
			IF ENDSCREEN_PREPARE(assassinationEndScreen)
			AND IS_MISSION_COMPLETE_READY_FOR_UI()
				
				INIT_SIMPLE_USE_CONTEXT(cucEndScreen, FALSE, FALSE, FALSE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(cucEndScreen, "ASS_CS_CONT",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
				ADD_SIMPLE_USE_CONTEXT_INPUT(cucEndScreen, "ES_XPAND",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
				
				SETTIMERA(0)
			
				PRINTLN("iEndingScreenStages = 2")
				iEndingScreenStages = 2
			ENDIF
		BREAK
		CASE 2
			IF RENDER_ENDSCREEN(assassinationEndScreen, FALSE)
				bSkippedEndingScreen = TRUE
			ENDIF
			IF NOT bSkippedEndingScreen
				UPDATE_SIMPLE_USE_CONTEXT(cucEndScreen)
			ENDIF
			//Only read button input until player presses accept
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,			INPUT_FRONTEND_ENDSCREEN_ACCEPT)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,	INPUT_FRONTEND_PAUSE_ALTERNATE)		// B*2276517 - Player can now press esc to close endscreen, if player control is on during endscreen
				IF NOT bSkippedEndingScreen
					PRINTLN("bSkippedEndingScreen = TRUE")
					bSkippedEndingScreen = TRUE
					//Start anim-out
					ENDSCREEN_START_TRANSITION_OUT(assassinationEndScreen)
				ENDIF
			ENDIF
			
			IF bSkippedEndingScreen
				//Wait until the end screen stops transition-out
				IF RENDER_ENDSCREEN(assassinationEndScreen)
					ENDSCREEN_SHUTDOWN(assassinationEndScreen)
					
					++g_savedGlobals.sAssassinData.iCurrentAssassinRank 
					ASSASSIN_MISSION_MarkMissionPassed()
					ASSASSIN_MISSION_AwardAssReward(missionData)
					MISSION_FLOW_MISSION_PASSED()
					ODDJOB_AUTO_SAVE()
					
					Script_Cleanup()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC


// Perform any special commands if the script fails
PROC Script_Failed()
	// this is now called once the screen has faded out for the fail screen.
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
		
	Script_Cleanup() // must only take 1 frame and terminate the thread
ENDPROC



/// PURPOSE:
///    Check to see if the player either killed the target
FUNC BOOL DID_PLAYER_KILL_TARGET(PED_INDEX& ped, VEHICLE_INDEX& veh)
//	VECTOR vTempPosTarget
//	VECTOR vTempPosPlayer
//	VECTOR vKillPos

	IF DOES_ENTITY_EXIST(ped)
		IF IS_PED_INJURED(ped) OR IS_ENTITY_DEAD(ped)
//			vTempPosTarget = GET_ENTITY_COORDS(ped, FALSE)
//			vKillPos = << vTempPosTarget.x-5, vTempPosTarget.y+6, vTempPosTarget.z+3>>
//			SET_CAM_COORD(camToAttach, vKillPos)
//			SET_CAM_FOV(camToAttach, 45)
//			POINT_CAM_AT_ENTITY(camToAttach, ped, <<0,0,0>>)
			IF DOES_ENTITY_EXIST(veh)
				SET_ENTITY_LOD_DIST(veh, 500)
			ENDIF

			RETURN TRUE
//		ELSE
//			//blow up the helicopter if it gets 75 m below the player
//			vTempPosTarget = GET_ENTITY_COORDS(ped)
//			vTempPosPlayer = GET_ENTITY_COORDS(Player)
//			IF vTempPosPlayer.z - vTempPosTarget.z > 75
//				IF DOES_ENTITY_EXIST(veh)
//					SET_ENTITY_LOD_DIST(veh, 500)
//					IF IS_PED_IN_VEHICLE(ped, veh)
//						vKillPos = << vTempPosTarget.x-5, vTempPosTarget.y+6, vTempPosTarget.z+3>>
//						SET_CAM_COORD(camToAttach, vKillPos)
//						POINT_CAM_AT_ENTITY(camToAttach, ped, <<0,0,0>>)
//						EXPLODE_VEHICLE(veh)
//						EXPLODE_VEHICLE_IN_CUTSCENE(veh)
//						SET_ENTITY_HEALTH(ped, 0)
//						PRINTLN("vTempPosPlayer.z - vTempPosTarget.z > 75 - TARGET IS IN VEHICLE")
//					ELSE
//						vKillPos = << vTempPosTarget.x-5, vTempPosTarget.y+6, vTempPosTarget.z+3>>
//						SET_CAM_COORD(camToAttach, vKillPos)
//						POINT_CAM_AT_ENTITY(camToAttach, ped, <<0,0,0>>)
//						EXPLODE_VEHICLE(veh)
//						EXPLODE_VEHICLE_IN_CUTSCENE(veh)
//						SET_ENTITY_HEALTH(ped, 0)
//						PRINTLN("vTempPosPlayer.z - vTempPosTarget.z > 75 - TARGET NOT IN VEHICLE")
//					ENDIF
//				ENDIF
//				
//				RETURN TRUE
//			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE: Check to see how many guys are left in the wave.
FUNC INT GET_NUM_ENEMIES_LEFT_IN_WAVE(WORKER_STRUCT& workerStruct[], INT iNumGuysInWave)
	INT numGuysLeft
	INT i 
	
	FOR i = 0 TO iNumGuysInWave - 1
		IF DOES_ENTITY_EXIST(workerStruct[i].ped)
			IF NOT IS_PED_INJURED(workerStruct[i].ped)
				numGuysLeft++
			ENDIF
		ENDIF
	ENDFOR
	
//	PRINTLN("numGuysLeft = ", numGuysLeft)
	
	RETURN numGuysLeft
ENDFUNC

/// PURPOSE: Task any remaning peds in the wave to attack the player
PROC TASK_REMAINING_PEDS_IN_WAVE_TO_ATTACK_PLAYER(WORKER_STRUCT& workerStruct[], INT iNumGuysInWave)
	INT i
	
	FOR i = 0 TO iNumGuysInWave - 1
		IF NOT IS_PED_INJURED(workerStruct[i].ped)
			REMOVE_PED_DEFENSIVE_AREA(workerStruct[i].ped)
			SET_PED_COMBAT_MOVEMENT(workerStruct[i].ped, CM_WILLADVANCE)
			TASK_CLEAR_DEFENSIVE_AREA(workerStruct[i].ped)
			TASK_COMBAT_PED(workerStruct[i].ped, Player)
		ENDIF
	ENDFOR
ENDPROC


//Grabs the closest enemy within 50 m to the player that is not pedToExclude
FUNC PED_INDEX GET_CLOSEST_LIVING_ENEMY(FLOAT fDistMax)
	PED_INDEX tempPed = NULL
	VECTOR vTemp
	INT tempInt
	FLOAT fClosest = fDistMax * fDistMax  //square this number since we are calcculating from vDist2
	FLOAT tempDist
	
	vTemp = GET_ENTITY_COORDS(Player)

	IF MainStage < STAGE_ELEVATOR_CUTSCENE		//check guys in wave 1
		FOR tempInt = 0 TO NUM_WAVE_1_WORKERS - 1
			IF NOT IS_PED_INJURED(wave1[tempInt].ped)
				tempDist = VDIST2(vTemp, GET_ENTITY_COORDS(wave1[tempInt].ped))
				IF tempDist < fClosest 
					fClosest = tempDist
					tempPed = wave1[tempInt].ped
				ENDIF
			ENDIF
		ENDFOR
	ELIF MainStage < STAGE_SECOND_ELEVATOR_CUTSCENE		//check guys in wave 3
		FOR tempInt = 0 TO NUM_WAVE_3_WORKERS - 1
			IF NOT IS_PED_INJURED(wave3[tempInt].ped)
				tempDist = VDIST2(vTemp, GET_ENTITY_COORDS(wave3[tempInt].ped))
				IF tempDist < fClosest 
					fClosest = tempDist
					tempPed = wave3[tempInt].ped
					PRINTLN("CLOSEST PED IN WAVE 3 = ", tempInt)
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		FOR tempInt = 0 TO NUM_WAVE_5_WORKERS - 1		//check guys in wave 5
			IF NOT IS_PED_INJURED(wave5[tempInt].ped)
				tempDist = VDIST2(vTemp, GET_ENTITY_COORDS(wave5[tempInt].ped))
				IF tempDist < fClosest 
					fClosest = tempDist
					tempPed = wave5[tempInt].ped
				ENDIF
			ENDIF
		ENDFOR
	ENDIF

	RETURN tempPed
ENDFUNC


//have Trevor play some lines as he is killing bad guys throughout the mission
PROC UPDATE_COMBAT_DIALOGUE_PLAYER()
	IF bPanicking
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				SWITCH iCurrentPlayerLine
					CASE 0
						IF iNumDead >= 1
							IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_TREV5", CONV_PRIORITY_HIGH)
								iCurrentPlayerLine ++
								PRINTLN("iCurrentPlayerLine = ", iCurrentPlayerLine)
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF iNumDead >= 2
							IF NOT DOES_ENTITY_EXIST(piTarget)
							OR NOT IS_ENTITY_DEAD(piTarget)
							AND Mainstage < STAGE_ROOFTOP_BATTLE		//line references the player looking for Bonelli
								IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_TREV1", CONV_PRIORITY_HIGH)
									iCurrentPlayerLine ++
									PRINTLN("iCurrentPlayerLine = ", iCurrentPlayerLine)
								ENDIF
							ELSE
								iCurrentPlayerLine = 3
								PRINTLN("iCurrentPlayerLine = ", iCurrentPlayerLine)
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF iNumDead >= 5
							IF Mainstage < STAGE_ROOFTOP_BATTLE		//line references the player looking for Bonelli
								IF NOT DOES_ENTITY_EXIST(piTarget)
								OR NOT IS_ENTITY_DEAD(piTarget)
									IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_TREV2", CONV_PRIORITY_HIGH)
										iCurrentPlayerLine ++
										PRINTLN("iCurrentPlayerLine = ", iCurrentPlayerLine)
									ENDIF
								ELSE
									iCurrentPlayerLine = 3
									PRINTLN("iCurrentPlayerLine = ", iCurrentPlayerLine)
								ENDIF
							ELSE
								iCurrentPlayerLine = 3
								PRINTLN("iCurrentPlayerLine = ", iCurrentPlayerLine)
							ENDIF
						ENDIF
					BREAK
					CASE 3
						IF iNumDead >= 7
							IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_TREV3", CONV_PRIORITY_HIGH)
								iCurrentPlayerLine ++
								PRINTLN("iCurrentPlayerLine = ", iCurrentPlayerLine)
							ENDIF
						ENDIF
					BREAK
					CASE 4
						IF Mainstage < STAGE_ROOFTOP_BATTLE		//line references the player looking for Bonelli
							IF iNumDead >= 11
								IF NOT DOES_ENTITY_EXIST(piTarget)
								OR NOT IS_ENTITY_DEAD(piTarget)
									IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_TREV6", CONV_PRIORITY_HIGH)
										iCurrentPlayerLine ++
										PRINTLN("iCurrentPlayerLine = ", iCurrentPlayerLine)
									ENDIF
								ELSE
									iCurrentPlayerLine = 6
									PRINTLN("iCurrentPlayerLine = ", iCurrentPlayerLine)
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 5
						IF iNumDead >= 13
							IF NOT DOES_ENTITY_EXIST(piTarget)
							OR NOT IS_ENTITY_DEAD(piTarget)
								IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_TREV4", CONV_PRIORITY_HIGH)
									iCurrentPlayerLine ++
								ENDIF
							ELSE
								iCurrentPlayerLine = 6
								PRINTLN("iCurrentPlayerLine = ", iCurrentPlayerLine)
							ENDIF
						ENDIF
					BREAK
					CASE 6
						IF iNumDead >= 15
							IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_SHIT", CONV_PRIORITY_VERY_HIGH)
								iCurrentPlayerLine ++
								PRINTLN("iCurrentPlayerLine = ", iCurrentPlayerLine)
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_NUM_ENEMIES_KILLED_IN_WAVE(WORKER_STRUCT& workerStruct[], INT iNumGuysInWave)
	INT iNumKilled, idx

	FOR idx = 0 TO iNumGuysInWave - 1
		IF DOES_ENTITY_EXIST(workerStruct[idx].ped)
			IF IS_PED_INJURED(workerStruct[idx].ped)
				iNumKilled++
			ENDIF
		ENDIF
	ENDFOR
	
//	PRINTLN("iNumKilled = ", iNumKilled)
	RETURN iNumKilled
ENDFUNC	


PROC PLAY_ENEMY_SPOTTED_FRANKLIN_LINE(PED_INDEX pedToPlayLine)
	IF NOT IS_PED_INJURED(pedToPlayLine)
		ADD_PED_FOR_DIALOGUE(siteConv, 4, pedToPlayLine, "OJAcsGUARD")
		IF IS_PED_ARMED(Player, WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
		AND NOT IS_PED_IN_ANY_VEHICLE(Player)
			//"shoot him he has a gun"
			PLAY_SINGLE_LINE_FROM_CONVERSATION(siteConv, "OJASAUD", "OJAScs_SPOT", "OJAScs_SPOT_3", CONV_PRIORITY_VERY_HIGH)
			PRINTLN("PLAYING LINE - OJAScs_SPOT_3")
		ELSE
			//"this is private property motherfucker"
			PLAY_SINGLE_LINE_FROM_CONVERSATION(siteConv, "OJASAUD", "OJAScs_SPOT", "OJAScs_SPOT_1", CONV_PRIORITY_VERY_HIGH)
			PRINTLN("PLAYING LINE - OJAScs_SPOT_1")
		ENDIF
	ENDIF
ENDPROC


//play enemy combat dialogue at various stages of the mission
PROC UPDATE_COMBAT_DIALOGUE_ENEMY()
//	PED_INDEX pedToSpeak
//	BLIP_INDEX tempBlip, tempBlip01

	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF IS_SCREEN_FADED_IN()
		AND NOT IS_MESSAGE_BEING_DISPLAYED()
			
			//make sure to advance the speech stage
			IF MainStage >= STAGE_ROOFTOP_BATTLE
			AND iEnemySpeechStage < 10
				iEnemySpeechStage = 10
			ELIF MainStage >= STAGE_GET_TO_SECOND_ELEVATOR
			AND iEnemySpeechStage < 4
				iEnemySpeechStage = 4
			ENDIF
			
			SWITCH iEnemySpeechStage
				CASE 0 
					IF bPanicking
						IF NOT IS_TIMER_STARTED(speechTimer)
							START_TIMER_NOW(speechTimer)
						ELIF GET_TIMER_IN_SECONDS(speechTimer) > 2	//add delay so that the player doesn't immediately yell the frame that he is alerted
							piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(35)
							IF piClosestEnemy <> NULL
								PLAY_ENEMY_SPOTTED_FRANKLIN_LINE(piClosestEnemy)
								
								iEnemySpeechStage++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 1
					//2 dead
					IF iNumDead >= 2
						piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(35)
						IF piClosestEnemy <> NULL
							ADD_PED_FOR_DIALOGUE(siteConv, 5, piClosestEnemy, "OJAcsGUARD2")
							IF GET_RANDOM_INT_IN_RANGE() % 2 = 0
								//"I need backup. Call reinforcements!"
								CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G1a", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING CONVO - OJAScs_G1a")
							ELSE
								//I got him! Over here guys.
								CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G1c", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING CONVO - OJAScs_G1c")
							ENDIF
							
							iEnemySpeechStage++
						ENDIF
					ENDIF
				BREAK
				CASE 2
					//3 dead
					IF iNumDead > 3
					OR bPedsOutOfVehicle
						piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(35)
						IF piClosestEnemy <> NULL
							ADD_PED_FOR_DIALOGUE(siteConv, 7, piClosestEnemy, "OJAcsGUARD4")
							IF GET_RANDOM_INT_IN_RANGE() % 2 = 0
								//"No way you're getting by me!"
								CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G3a", CONV_PRIORITY_VERY_HIGH)
							ELSE
								//"Get this son of a bitch! What's wrong with you guys?"
								CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G3c", CONV_PRIORITY_VERY_HIGH)
							ENDIF
							
							RESTART_TIMER_NOW(speechTimer)
							iEnemySpeechStage++
						ENDIF
					ENDIF
				BREAK
				CASE 3
					//when near elevator guard on ground
					IF GET_PLAYER_DISTANCE_FROM_LOCATION(vElev1Bottom) <= 50
						IF IS_TIMER_STARTED(speechTimer)
							IF GET_TIMER_IN_SECONDS(speechTimer) > 15
								piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(15)
								IF piClosestEnemy <> NULL
									ADD_PED_FOR_DIALOGUE(siteConv, 5, piClosestEnemy, "OJAcsGUARD2")
									IF GET_RANDOM_INT_IN_RANGE() % 2 = 0
										//"You really think you'll make it to this elevator alive?"
										CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G1d", CONV_PRIORITY_VERY_HIGH)
									ELSE
										//Your luck streak just ran out. It's too late to go back now!
										CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G1b", CONV_PRIORITY_VERY_HIGH)
									ENDIF
									
									iEnemySpeechStage++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 4
					//after reaching mid level
					IF MainStage >= STAGE_GET_TO_SECOND_ELEVATOR
						IF IS_TIMER_STARTED(speechTimer)
							IF GET_TIMER_IN_SECONDS(speechTimer) > 13
								piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(50)
								IF piClosestEnemy <> NULL
									IF CAN_PED_SEE_HATED_PED(piClosestEnemy, PLAYER_PED_ID())
										ADD_PED_FOR_DIALOGUE(siteConv, 7, piClosestEnemy, "OJAcsGUARD4")
										//"He's here! Get this motherfucker!"
										CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G3d", CONV_PRIORITY_VERY_HIGH)
										
										RESTART_TIMER_NOW(speechTimer)
										iEnemySpeechStage++
									ENDIF
								ENDIF
							ENDIF
						ELSE
							RESTART_TIMER_NOW(speechTimer)
						ENDIF
					ENDIF
				BREAK
				CASE 5
					//more guard panic/shit talking
					IF IS_TIMER_STARTED(speechTimer)
						IF GET_TIMER_IN_SECONDS(speechTimer) > 8
							IF GET_NUM_ENEMIES_KILLED_IN_WAVE(wave3, NUM_WAVE_3_WORKERS) >= 2

								piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(50)
								IF piClosestEnemy <> NULL
									IF CAN_PED_SEE_HATED_PED(piClosestEnemy, PLAYER_PED_ID())
										ADD_PED_FOR_DIALOGUE(siteConv, 6, piClosestEnemy, "OJAcsGUARD3")

										IF GET_RANDOM_INT_IN_RANGE() % 2 = 0
											//"How the hell did he get up here? Someone stop him!", "I'm not fucking around anymore! This is it!"
											CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G2a", CONV_PRIORITY_VERY_HIGH)
										ELSE
											//"Guard your positions. Do not let him through!", "Let's end this chump!
											CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G2b", CONV_PRIORITY_VERY_HIGH)
										ENDIF
										
										RESTART_TIMER_NOW(speechTimer)
										iEnemySpeechStage++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						START_TIMER_AT(speechTimer, 0)
					ENDIF
				BREAK
				CASE 6
					//calling for more support
					IF GET_NUM_ENEMIES_KILLED_IN_WAVE(wave3, NUM_WAVE_3_WORKERS) >= 4
						piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(75)
						IF piClosestEnemy <> NULL
							IF CAN_PED_SEE_HATED_PED(piClosestEnemy, PLAYER_PED_ID())
								ADD_PED_FOR_DIALOGUE(siteConv, 4, piClosestEnemy, "OJAcsGUARD")
								IF GET_RANDOM_INT_IN_RANGE() % 2 = 0
									//"We need more support!"
									PLAY_SINGLE_LINE_FROM_CONVERSATION(siteConv, "OJASAUD", "OJAScs_GDb", "OJAScs_GDb_1", CONV_PRIORITY_VERY_HIGH)
								ELSE
									//"Someone help me out over here!"
									PLAY_SINGLE_LINE_FROM_CONVERSATION(siteConv, "OJASAUD", "OJAScs_GDb", "OJAScs_GDb_3", CONV_PRIORITY_VERY_HIGH)
								ENDIF
								
								RESTART_TIMER_NOW(speechTimer)
								iEnemySpeechStage++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 7
					//if player is hanging out in COVER or less than 5 guys left
					IF GET_TIMER_IN_SECONDS(speechTimer) > 12
						IF IS_PED_IN_COVER(Player)
						OR GET_NUM_ENEMIES_LEFT_IN_WAVE(wave3, NUM_WAVE_3_WORKERS) < 4
							piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(75)
							IF piClosestEnemy <> NULL
								IF IS_PED_IN_COVER(Player)
									ADD_PED_FOR_DIALOGUE(siteConv, 7, piClosestEnemy, "OJAcsGUARD4")
									//"You gonna hide there the whole time? Come and get me!"
									CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G3b", CONV_PRIORITY_VERY_HIGH)
								ELSE
									ADD_PED_FOR_DIALOGUE(siteConv, 5, piClosestEnemy, "OJAcsGUARD2")
									//"It's only one man, not an army! Someone put a bullet in him!"
									CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G1e", CONV_PRIORITY_VERY_HIGH)
								ENDIF
								RESTART_TIMER_NOW(speechTimer)
								iEnemySpeechStage++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 8
					//more banter of guy threatening the player
					IF GET_NUM_ENEMIES_LEFT_IN_WAVE(wave3, NUM_WAVE_3_WORKERS) <= 2
						piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(50)
						IF piClosestEnemy <> NULL
							ADD_PED_FOR_DIALOGUE(siteConv, 8, piClosestEnemy, "OJAcsGUARD5")
							IF GET_RANDOM_INT_IN_RANGE() % 2 = 0
								//"Surrender and I'll grant you a quick death."
								PLAY_SINGLE_LINE_FROM_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G4", "OJAScs_G4_1", CONV_PRIORITY_VERY_HIGH)
							ELSE
								//"Come at me!"
								PLAY_SINGLE_LINE_FROM_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G4", "OJAScs_G4_3", CONV_PRIORITY_VERY_HIGH)
							ENDIF
							iEnemySpeechStage++
						ENDIF
					ENDIF
				BREAK
				CASE 9
					//more banter of guys starting to panic
					IF GET_NUM_ENEMIES_LEFT_IN_WAVE(wave3, NUM_WAVE_3_WORKERS) <= 1
						piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(50)
						IF piClosestEnemy <> NULL
							ADD_PED_FOR_DIALOGUE(siteConv, 4, piClosestEnemy, "OJAcsGUARD")
							//"Dammit I need help here!"
							CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_GDa", CONV_PRIORITY_VERY_HIGH)
							iEnemySpeechStage++
						ELIF MainStage = STAGE_ROOFTOP_BATTLE
							iEnemySpeechStage++
						ENDIF
					ENDIF
				BREAK
				CASE 10
					IF DOES_ENTITY_EXIST(wave5[0].ped)
					AND DOES_ENTITY_EXIST(piTarget)
						IF NOT IS_PED_INJURED(wave5[0].ped)
						AND NOT IS_PED_INJURED(piTarget)
							ADD_PED_FOR_DIALOGUE(siteConv, 4, wave5[0].ped, "OJAcsGUARD")
							CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_ROOF", CONV_PRIORITY_VERY_HIGH)
							iEnemySpeechStage++
						ELSE
							iEnemySpeechStage++
						ENDIF
					ENDIF
				BREAK
				CASE 11
					IF DOES_ENTITY_EXIST(piTarget)
						IF NOT IS_PED_INJURED(piTarget)
							CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_BOSS", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
							iEnemySpeechStage++
						ELSE
							iEnemySpeechStage++
						ENDIF
					ENDIF
				BREAK
				CASE 12
					//player on rooftop
					IF MainStage = STAGE_ROOFTOP_BATTLE
						piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(35)
						IF piClosestEnemy <> NULL
							IF piClosestEnemy <> piTarget
								ADD_PED_FOR_DIALOGUE(siteConv, 8, piClosestEnemy, "OJAcsGUARD5")
								IF GET_RANDOM_INT_IN_RANGE() % 2 = 0
									//"I'm gonna toss your body to the street!"
									CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G5a", CONV_PRIORITY_VERY_HIGH)
								ELSE
									//"It's a long way down!"
									CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G5b", CONV_PRIORITY_VERY_HIGH)
								ENDIF
							ENDIF
							
							RESTART_TIMER_NOW(speechTimer)
							iEnemySpeechStage++
						ENDIF
					ENDIF
				BREAK
				CASE 13
					IF IS_TIMER_STARTED(speechTimer)
						IF NOT IS_PED_INJURED(piTarget)
							IF GET_TIMER_IN_SECONDS(speechTimer) > 10
								CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_BOSS4", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								iEnemySpeechStage++
							ENDIF
						ELSE
							iEnemySpeechStage++
						ENDIF
					ELSE
						START_TIMER_AT(speechTimer, 0)
					ENDIF
				BREAK
				CASE 14
					//player on rooftop
					IF IS_TIMER_STARTED(speechTimer)
						IF GET_TIMER_IN_SECONDS(speechTimer) > 9
							piClosestEnemy = GET_CLOSEST_LIVING_ENEMY(35)
							IF piClosestEnemy <> NULL
								IF piClosestEnemy <> piTarget
									ADD_PED_FOR_DIALOGUE(siteConv, 5, piClosestEnemy, "OJAcsGUARD2")
									IF GET_RANDOM_INT_IN_RANGE() % 2 = 0
										//"This is the end of the line prick!"
										CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G6a", CONV_PRIORITY_VERY_HIGH)
									ELSE
										//"Put him in a bodybag!"
										CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_G6b", CONV_PRIORITY_VERY_HIGH)
									ENDIF
								ENDIF
								
								RESTART_TIMER_NOW(speechTimer)
								iEnemySpeechStage++
							ENDIF
						ENDIF
					ELSE
						START_TIMER_AT(speechTimer, 0)
					ENDIF
				BREAK
				CASE 15
					IF NOT IS_PED_INJURED(piTarget)
						IF GET_TIMER_IN_SECONDS(speechTimer) > 25
							//"I've got all day. I'm not leaving until you're not breathing!"
							CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_BOSS2", CONV_PRIORITY_VERY_HIGH)
							
							iEnemySpeechStage++
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

//Script-controlled damage effect that are applied to the car when the player fires bullets near the vhiecles tires and windows
PROC UPDATE_SCRIPTED_VEHICLE_DAMAGE(VEHICLE_INDEX vehIndex)
	VECTOR vRearRight, vRearLeft, vFrontRight, vFrontLeft
	
	IF NOT IS_ENTITY_DEAD(vehIndex)
		IF NOT IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_REAR_RIGHT)
			vRearRight = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehIndex), GET_ENTITY_HEADING(vehIndex), << 0.660, -1.180, 0.100 >>)
            IF IS_BULLET_IN_AREA(vRearRight, 1.0)
                SET_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_REAR_RIGHT)
                SMASH_VEHICLE_WINDOW(vehIndex, SC_WINDOW_REAR_RIGHT)
                SET_VEHICLE_DAMAGE(vehIndex, <<0.5, -0.75, 0.05>>, 100.0, 100.0, TRUE)
            ENDIF
		ENDIF
	      
		IF NOT IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_FRONT_RIGHT)
			vFrontRight = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehIndex), GET_ENTITY_HEADING(vehIndex), << 0.660, 1.180, 0.100 >>)
		    IF IS_BULLET_IN_AREA(vFrontRight, 1.0)
	            SET_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_FRONT_RIGHT)
	            SMASH_VEHICLE_WINDOW(vehIndex, SC_WINDOW_FRONT_RIGHT)
	            SET_VEHICLE_DAMAGE(vehIndex, <<0.5, 0.75, 0.05>>, 100.0, 100.0, TRUE)
	        ENDIF
	    ENDIF
		
		IF NOT IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_REAR_LEFT)
			vRearLeft = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehIndex), GET_ENTITY_HEADING(vehIndex), << -0.660, -1.180, 0.100 >>)		
		    IF IS_BULLET_IN_AREA(vRearLeft, 1.0)
	            SET_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_REAR_LEFT)
	            SMASH_VEHICLE_WINDOW(vehIndex, SC_WINDOW_REAR_LEFT)
	            SET_VEHICLE_DAMAGE(vehIndex, <<0.5, 0.75, 0.05>>, 100.0, 100.0, TRUE)
	        ENDIF
	    ENDIF
		
		IF NOT IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_FRONT_LEFT)
			vFrontLeft = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehIndex), GET_ENTITY_HEADING(vehIndex), << -0.660, 1.180, 0.100 >>)
		    IF IS_BULLET_IN_AREA(vFrontLeft, 1.0)
	            SET_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_FRONT_LEFT)
	            SMASH_VEHICLE_WINDOW(vehIndex, SC_WINDOW_FRONT_LEFT)
	            SET_VEHICLE_DAMAGE(vehIndex, <<0.5, 0.75, 0.05>>, 100.0, 100.0, TRUE)
	        ENDIF
	    ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Blip enemies cleanup their blips when killed
PROC HANDLE_ENEMY_BLIPS(WORKER_STRUCT& workerStruct[], INT iNumGuysInWave)
	INT tempInt

	FOR tempInt = 0 TO iNumGuysInWave - 1
		IF DOES_ENTITY_EXIST(workerStruct[tempInt].ped)
			
			UPDATE_AI_PED_BLIP(workerStruct[tempInt].ped, workerStruct[tempInt].blipWorker)
			
			IF IS_PED_INJURED(workerStruct[tempInt].ped)
//				IF DOES_BLIP_EXIST(workerStruct[tempInt].blip)
				IF NOT workerStruct[tempInt].bDead
//					REMOVE_BLIP(workerStruct[tempInt].blip)
					iNumDead++
					PRINTLN("iNumDead = ", iNumDead)
					workerStruct[tempInt].bDead = TRUE
				ENDIF	
			ENDIF
		ENDIF
	ENDFOR
ENDPROC


/// PURPOSE:
///    Call this before advancing stages to initialize stage variables
PROC INITIALIZE_NEXT_STAGE()
	iStageNum = 0
	iElevButtonStage = 0
	iElevSyncScene = 0
//	iCutsceneStage = 0
	bCutsceneSkipped = FALSE
	bCutsceneFading = FALSE
	bAdvanceOnPlayer = FALSE
	RESTART_TIMER_NOW(speechTimer)
ENDPROC


/// PURPOSE:
///    Determine which Kill camera to use
PROC HANDLE_KILL_CAMERA_LOGIC()
	IF MainStage >= STAGE_ROOFTOP_BATTLE
	AND MainStage < STAGE_KILL_CAM
		IF DID_PLAYER_KILL_TARGET(piTarget, viHeli)
		AND IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
//			REMOVE_BLIP(bTargetBlip)
			
			//tell guy on crane to keep shooting at the player if he is still alive
			IF NOT IS_PED_INJURED(wave5[3].ped)
				SET_PED_COMBAT_ATTRIBUTES(wave5[3].ped, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
				SET_ENTITY_COORDS(wave5[3].ped, << -119.0637, -973.7914, 292.04 >>)	//position on the crane
				TASK_COMBAT_PED(wave5[3].ped, Player)
			ENDIF
			
			INITIALIZE_NEXT_STAGE()
			MainStage = STAGE_KILL_CAM
			PRINTLN("MainStage = STAGE_KILL_CAM")
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_PED_ALIVE_AND_IN_VEHICLE(PED_INDEX ped)
	IF NOT IS_PED_INJURED(ped)
		IF IS_PED_IN_ANY_VEHICLE(ped)
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Handle enemy response behavior and blip handling
PROC HANDLE_ENEMY_BEHAVIOR()
	//Handle the worker's combat response to the player
		DO_ENEMY_PANIC_CHECKS()
		
	//handle blipping and unblipping of enemies
//		IF MainStage < STAGE_ELEVATOR_CUTSCENE
			HANDLE_ENEMY_BLIPS(wave1, NUM_WAVE_1_WORKERS)
			IF bUpdateGuysInCar
				HANDLE_ENEMY_BLIPS(wave2, NUM_WAVE_2_WORKERS)
			ENDIF
			HANDLE_ENEMY_BLIPS(warningGuys, NUM_WARNING_GUYS)
//		ELIF MainStage < STAGE_SECOND_ELEVATOR_CUTSCENE
			HANDLE_ENEMY_BLIPS(wave3, NUM_WAVE_3_WORKERS)
			HANDLE_ENEMY_BLIPS(wave4, NUM_WAVE_4_WORKERS)
//		ELSE
			HANDLE_ENEMY_BLIPS(wave5, NUM_WAVE_5_WORKERS)
			
			IF DOES_ENTITY_EXIST(piTarget)
				IF NOT IS_PED_INJURED(piTarget)
					HANDLE_ENEMY_BLIPS(wave6, NUM_WAVE_6_WORKERS)
				ELSE
					//may have to add a condition to re-update the ped blip if the are not in vehicle and useable
					IF IS_PED_ALIVE_AND_IN_VEHICLE(wave6[0].ped)
						CLEANUP_AI_PED_BLIP(wave6[0].blipWorker)
					ELSE
						UPDATE_AI_PED_BLIP(wave6[0].ped, wave6[0].blipWorker)
					ENDIF
					IF IS_PED_ALIVE_AND_IN_VEHICLE(wave6[1].ped)
						CLEANUP_AI_PED_BLIP(wave6[1].blipWorker)
					ELSE
						UPDATE_AI_PED_BLIP(wave6[1].ped, wave6[1].blipWorker)
					ENDIF
					IF IS_PED_ALIVE_AND_IN_VEHICLE(wave6[2].ped)
						CLEANUP_AI_PED_BLIP(wave6[2].blipWorker)
					ELSE
						UPDATE_AI_PED_BLIP(wave6[2].ped, wave6[2].blipWorker)
					ENDIF
				ENDIF
			ENDIF
//		ENDIF
		
	//update franklin speech
		UPDATE_COMBAT_DIALOGUE_PLAYER()
		
	//update enemy combat AI
		UPDATE_COMBAT_DIALOGUE_ENEMY()
ENDPROC


///// PURPOSE:
/////    Creates dead peds inside of an elevator (maximum of 2 peds)
//PROC CREATE_DEAD_PEDS_IN_ELEVATOR(INT iNumPedsToCreate, ENTITY_INDEX elevator)
//	INT iTemp
//	INT iMaxPedsToCreate = 2
//		
//	FOR iTemp = 0 TO iNumPedsToCreate - 1
//		IF iTemp <= iMaxPedsToCreate
//			IF iTemp = 0
//				piDeadElevatorGuys[iTemp] = CREATE_PED(PEDTYPE_MISSION, WorkerModel[0], << -159.3574, -939.2502, 268.5 >>, 300)
//				SET_PED_COMPONENT_VARIATION(piDeadElevatorGuys[iTemp], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
//				SET_PED_COMPONENT_VARIATION(piDeadElevatorGuys[iTemp], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
//				SET_PED_COMPONENT_VARIATION(piDeadElevatorGuys[iTemp], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
//				SET_PED_COMPONENT_VARIATION(piDeadElevatorGuys[iTemp], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
//				SET_PED_COMPONENT_VARIATION(piDeadElevatorGuys[iTemp], INT_TO_ENUM(PED_COMPONENT,11), 1, 0, 0) //(jbib)
//				TASK_PLAY_ANIM(piDeadElevatorGuys[iTemp], "mp_common_miss", "dead_ped_idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)	// | AF_PRIORITY_HIGH
//				ATTACH_ENTITY_TO_ENTITY(piDeadElevatorGuys[iTemp], elevator, 0, << 2.1, -1.9, -2.1 >>, << 0, 0, 106 >>, TRUE, TRUE, TRUE)
//			ELSE
//				piDeadElevatorGuys[iTemp] = CREATE_PED(PEDTYPE_MISSION, WorkerModel[1], << -159.9370, -940.4857, 268.5 >>, 350)
//				SET_PED_COMPONENT_VARIATION(piDeadElevatorGuys[iTemp], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
//				SET_PED_COMPONENT_VARIATION(piDeadElevatorGuys[iTemp], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
//				SET_PED_COMPONENT_VARIATION(piDeadElevatorGuys[iTemp], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
//				SET_PED_COMPONENT_VARIATION(piDeadElevatorGuys[iTemp], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
//				SET_PED_COMPONENT_VARIATION(piDeadElevatorGuys[iTemp], INT_TO_ENUM(PED_COMPONENT,11), 1, 1, 0) //(jbib)
//				TASK_PLAY_ANIM(piDeadElevatorGuys[iTemp], "mp_common_miss", "dead_ped_idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)	// | AF_PRIORITY_HIGH
//				ATTACH_ENTITY_TO_ENTITY(piDeadElevatorGuys[iTemp], elevator, 0, << -2, -2.4, -2.1 >>, << 0, 0, 0 >>, TRUE, TRUE, TRUE)
//			ENDIF
//			SET_PED_CAN_BE_TARGETTED(piDeadElevatorGuys[iTemp], FALSE )
//			SET_PED_FLEE_ATTRIBUTES(piDeadElevatorGuys[iTemp], FA_NEVER_FLEE, TRUE )
//			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piDeadElevatorGuys[iTemp], TRUE )
//			SET_ENTITY_HEALTH(piDeadElevatorGuys[iTemp], 100)
//			DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(piDeadElevatorGuys[iTemp])
//			SET_PED_DIES_WHEN_INJURED(piDeadElevatorGuys[iTemp], TRUE)
//			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(piDeadElevatorGuys[iTemp], FALSE)
//		ENDIF
//	ENDFOR
//ENDPROC

//PROC PLAY_ELEVATOR_AUDIO_DOOR_CLOSE(INT& soundID, ENTITY_INDEX door)
//	IF soundID = -1
//		soundID = GET_SOUND_ID()
//		ODDJOB_PLAY_SOUND("FREIGHT_ELEVATOR_01_DOOR_CLOSE", soundID, TRUE, door)
//	ENDIF
//ENDPROC

//PROC PLAY_ELEVATOR_AUDIO_DOOR_OPEN(INT& soundID, ENTITY_INDEX door)
//	IF soundID = -1
//		soundID = GET_SOUND_ID()
//		ODDJOB_PLAY_SOUND("FREIGHT_ELEVATOR_01_DOOR_OPEN", soundID, TRUE, door)
//	ENDIF
//ENDPROC
 

///// PURPOSE:
/////    Move the elevator updwards at a desired speed
//PROC MOVE_ELEVATOR_DOOR(ENTITY_INDEX door, FLOAT distPerFrame, BOOL bClose)
//	VECTOR vDoorPos
//	
//	IF DOES_ENTITY_EXIST(door)
//		vDoorPos = GET_ENTITY_COORDS(door)
//		IF bClose
//			SET_ENTITY_COORDS(door, <<vDoorPos.x, vDoorPos.y, vDoorPos.z-distPerFrame>>)
//			PLAY_ELEVATOR_AUDIO_DOOR_CLOSE(iSoundIdElevator, door)
//		ELSE
//			SET_ENTITY_COORDS(door, <<vDoorPos.x, vDoorPos.y, vDoorPos.z+distPerFrame>>)
//			PLAY_ELEVATOR_AUDIO_DOOR_OPEN(iSoundIdElevator, door)
//		ENDIF
//	ENDIF
//ENDPROC


PROC PLAY_ELEVATOR_AUDIO_MOTOR(INT& soundID, ENTITY_INDEX elevator)
	IF soundID = -1
		soundID = GET_SOUND_ID()
		ODDJOB_PLAY_SOUND("FREIGHT_ELEVATOR_02_MOTOR", soundID, TRUE, elevator)
	ENDIF
ENDPROC


/// PURPOSE:
///    Move the elevator updwards at a desried speed
PROC MOVE_ELEVATOR(ENTITY_INDEX elevator, ENTITY_INDEX frontDoor, ENTITY_INDEX backDoor, FLOAT distPerFrame, BOOL bAttachPlayer = TRUE, BOOL bMoveUp = TRUE, BOOL bMoveDoors = TRUE)
	VECTOR vElevPos
	FLOAT fTempHead
	VEHICLE_INDEX tempVeh
	
	bMoveUp = bMoveUp
	frontDoor = frontDoor
	backDoor = backDoor
	bMoveDoors = bMoveDoors
	
	IF DOES_ENTITY_EXIST(elevator)
		vElevPos = GET_ENTITY_COORDS(elevator)
		
		IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
			distPerFrame /= 5		//make sure the elevator travels slower if the weapon wheel is displayed
		ENDIF
		
		IF bMoveUp
			SET_ENTITY_COORDS(elevator, <<vElevPos.x, vElevPos.y, vElevPos.z+distPerFrame>>)
		ELSE
			SET_ENTITY_COORDS(elevator, <<vElevPos.x, vElevPos.y, vElevPos.z-distPerFrame>>)
		ENDIF
	ENDIF
	IF bAttachPlayer
		IF IS_PED_IN_ANY_VEHICLE(Player)
			tempVeh = GET_VEHICLE_PED_IS_IN(Player)
		ENDIF
//		CLEAR_PED_TASKS(Player)

		IF tempVeh = NULL
			IF NOT IS_ENTITY_ATTACHED(Player)
				fTempHead = GET_ENTITY_HEADING(Player)
				//fix to make sure that player is oriented in the correct heading in the first elevator cutscene(account for elevator rotation)
				IF fTempHead + 90 >= 360
					fTempHead = (fTempHead + 90) - 360
				ELSE
					fTempHead += 90
				ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(Player)
					ATTACH_ENTITY_TO_ENTITY(Player, elevator, 0, vPlayerElevatorOffset, <<0, 0, fTempHead >>, FALSE, FALSE, TRUE)
					PRINTLN("ATTACHING THE PLAYER TO THE LIFT")	
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_ATTACHED(tempVeh)
				fTempHead = GET_ENTITY_HEADING(tempVeh)
				//fix to make sure that player is oriented in the correct heading in the first elevator cutscene
				IF fTempHead + 90 >= 360
					fTempHead = (fTempHead + 90) - 360
				ELSE
					fTempHead += 90
				ENDIF

				ATTACH_ENTITY_TO_ENTITY(tempVeh, elevator, 0, << 0.4, -2.2, -1.2 >>, <<0, 0, fTempHead >>)
				PRINTLN("PLAYER IN A VEHICLE - ATTACHING THE VEHICLE TO THE LIFT")				
			ENDIF
		ENDIF
	ENDIF
	
	SET_FOLLOW_CAM_IGNORE_ATTACH_PARENT_MOVEMENT_THIS_UPDATE()
	
	PLAY_ELEVATOR_AUDIO_MOTOR(iSoundIdElevator, elevator)
ENDPROC


/// PURPOSE:
///    Call this function every frame while the elevator is active to get it to gradually come to a stop when it reaches its desired height
/// PARAMS:
///    fCurrentHeight - The elevator's current height (get this every frame)
///    fHeightToStopSlowing - The height at which you want the elevator to be traveling at max speed
///    fMinSpeedHeight - The height at which we want the elevator to be traveling at the minimum speed
///    fMinDistPerFrame - Minimum distance along the z-axis the elevator is allowed to travel per frame
///    fMaxDistPerFrame - Maximum distance along the z-axis the elevator is allowed to travel per frame
/// RETURNS:
///    Distance per frame the elevator should be traveling along the z axis
FUNC FLOAT GET_ELEVATOR_TARGET_MOVE_SPEED_START(FLOAT fCurrentHeight, FLOAT fHeightToStopSlowing, FLOAT fMinSpeedHeight, FLOAT fMinDistPerFrame, FLOAT fMaxDistPerFrame)
	//gradually slow down the elevator as it approaches its destination
	FLOAT fTotalHeightDelta
	FLOAT fCurrentHeightDelta
	FLOAT fCurrentPercentage
	FLOAT fNewDistPerFrame
	

	IF fCurrentHeight <= fHeightToStopSlowing 
		
		//get the total distance between where we want to stop slowing and the fMinSpeedHeight
		fTotalHeightDelta = fHeightToStopSlowing - fMinSpeedHeight
		
		//get the distance between the elevators current height and the fHeightToStopSlowing
		fCurrentHeightDelta = fHeightToStopSlowing - fCurrentHeight
		
		//get a % of how far along the speed-up distance the elevator is
		fCurrentPercentage = fCurrentHeightDelta / fTotalHeightDelta
		
		//calculate the new movespeed
		fNewDistPerFrame = (1 - fCurrentPercentage) * fMaxDistPerFrame
	ELSE
		fNewDistPerFrame = fMaxDistPerFrame
	ENDIF
	
	//safeguard to ensure we go no slower than fMinDistPerFrame
	IF fNewDistPerFrame < fMinDistPerFrame
		fNewDistPerFrame = fMinDistPerFrame
	ELIF fNewDistPerFrame > fMaxDistPerFrame
		fNewDistPerFrame = fMaxDistPerFrame
	ENDIF
	
	RETURN fNewDistPerFrame
ENDFUNC


/// PURPOSE:
///    Call this function every frame while the elevator is active to get it to gradually come to a stop when it reaches its desired height
/// PARAMS:
///    fCurrentHeight - The elevator's current height (get this every frame)
///    fHeightToStartSlowing - The height at which you want the elevator to start slowing down
///    fStopHeight - The height at which you want the elevator to come to a complete stop
///    fMinSpeedHeight - The height at which we want the elevator to be traveling at the minimum speed
///    fMinDistPerFrame - Minimum distance along the z-axis the elevator is allowed to travel per frame
///    fMaxDistPerFrame - Maximum distance along the z-axis the elevator is allowed to travel per frame
/// RETURNS:
///    Distance per frame the elevator should be traveling along the z axis
FUNC FLOAT GET_ELEVATOR_TARGET_MOVE_SPEED_STOP(FLOAT fCurrentHeight, FLOAT fHeightToStartSlowing, FLOAT fStopHeight, FLOAT fMinSpeedHeight, FLOAT fMinDistPerFrame, FLOAT fMaxDistPerFrame)
	//gradually slow down the elevator as it approaches its destination
	FLOAT fTotalHeightDelta
	FLOAT fCurrentHeightDelta
	FLOAT fCurrentPercentage
	FLOAT fSpeedRange
	FLOAT fNewDistPerFrame
	
	IF fCurrentHeight < fStopHeight
		IF fCurrentHeight >= fHeightToStartSlowing 
			
			//get the total distance between where we want to start slowing and the fMinSpeedHeight
			fTotalHeightDelta = fMinSpeedHeight - fHeightToStartSlowing
			
			//get the distance between the elevators current height and the fMinSpeedHeight
			fCurrentHeightDelta = fMinSpeedHeight - fCurrentHeight
			
			//get a % of how far along the slowdown distance the elevator is
			fCurrentPercentage = fCurrentHeightDelta / fTotalHeightDelta
			
			//establish a min/max value for the possible speeds
			fSpeedRange = fMaxDistPerFrame - fMinDistPerFrame
			
			//calculate the new movespeed
			fNewDistPerFrame = (fCurrentPercentage * fSpeedRange) + fMinDistPerFrame
		ELSE
			fNewDistPerFrame = fMaxDistPerFrame
		ENDIF
	ENDIF
	
	//safeguard to ensure we go no slower than fMinDistPerFrame
	IF fNewDistPerFrame < fMinDistPerFrame
		fNewDistPerFrame = fMinDistPerFrame
	ENDIF
	
	RETURN fNewDistPerFrame
ENDFUNC


/// PURPOSE:
///    Place the first elevator and doors at the top position correctly with the player inside
PROC SET_GROUND_ELEVATOR_TO_TOP(BOOL bAttachPlayer = TRUE)
//	VEHICLE_INDEX vehIndex
	
//	IF IS_PED_IN_ANY_VEHICLE(Player)
//		vehIndex = GET_VEHICLE_PED_IS_IN(Player)
//		DETACH_ENTITY(vehIndex)
//	ELSE
		DETACH_ENTITY(Player)
//	ENDIF
	
	ODDJOB_STOP_SOUND(iSoundIdElevator)
	
	//move the elevator to the top or create it if it doesnt already exist
	IF DOES_ENTITY_EXIST(objElev[0])
		SET_ENTITY_COORDS(objElev[0], << -182.170, -1016.090, 115.46 >>)
		PRINTLN("objElev[0] EXISTS - TELEPORTING TO THE TOP")
	ELSE
		objElev[0] = CREATE_OBJECT(ElevModel, << -182.170, -1016.090, 115.46 >>)
		SET_ENTITY_COORDS(objElev[0], << -182.170, -1016.090, 115.46 >>)
		SET_ENTITY_ROTATION(objElev[0], vMyElevRot[0])
		SET_ENTITY_LOAD_COLLISION_FLAG(objElev[0], TRUE)
		PRINTLN("objElev[0] DOES NOT EXIST - CREATING IT")
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(objElevButton[0]) AND DOES_ENTITY_EXIST(objElev[0]) 
		objElevButton[0] = CREATE_OBJECT(ElevButtonModel, vMyElevPos[0])
		ATTACH_ENTITY_TO_ENTITY(objElevButton[0], objElev[0], 0, vButtonOffset, <<0,0,0>>)
		PRINTLN("ATTACHING BUTTON TO ELEVATOR")
	ENDIF
	
	IF bAttachPlayer
		ATTACH_ENTITY_TO_ENTITY(Player, objElev[0], 0, vPlayerElevatorOffset, <<0, 0, 340 >>, FALSE, FALSE, TRUE)
		DETACH_ENTITY(Player, FALSE, FALSE)
		PRINTLN("Detaching Player from ground elevator")
		SET_ENTITY_HEADING(Player, 340.3911)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	ENDIF
ENDPROC

/// PURPOSE:
///    place the second elevator and doors at the very top of the structure with the player inside
PROC SET_SECOND_ELEVATOR_TO_TOP(BOOL bResetElevator = TRUE, BOOL bAttachPlayer = TRUE)
//	VEHICLE_INDEX vehIndex
	
//	IF IS_PED_IN_ANY_VEHICLE(Player)
//		vehIndex = GET_VEHICLE_PED_IS_IN(Player)
//		DETACH_ENTITY(vehIndex)
//	ELSE
		DETACH_ENTITY(Player)
//	ENDIF
	
	ODDJOB_STOP_SOUND(iSoundIdElevator)
	
	//move the elevator to the top or create it if it doesnt already exist
	IF bResetElevator
		IF DOES_ENTITY_EXIST(objElev[1])
			SET_ENTITY_COORDS(objElev[1], << -158.840, -942.240, 270.46 >>)
		ELSE
			objElev[1] = CREATE_OBJECT(ElevModel, << -158.840, -942.240, 270.46 >>)
			SET_ENTITY_COORDS(objElev[1], << -158.840, -942.240, 270.46 >>)
			SET_ENTITY_ROTATION(objElev[1], vMyElevRot[1])
		ENDIF
	ENDIF
	
	IF bAttachPlayer
		ATTACH_ENTITY_TO_ENTITY(Player, objElev[1], 0, vPlayerElevatorOffset, <<0, 0, 269 >>, FALSE, FALSE, TRUE)
		DETACH_ENTITY(Player, FALSE, FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(objElevButton[1])
		objElevButton[1] = CREATE_OBJECT(ElevButtonModel, vMyElevPos[1])
		ATTACH_ENTITY_TO_ENTITY(objElevButton[1], objElev[1], 0, vButtonOffset, <<0,0,0>>)
	ENDIF
ENDPROC


PROC SET_SECOND_ELEVATOR_TO_BOTTOM(BOOL bDoorOpen = TRUE)
	VEHICLE_INDEX vehIndex
	
	IF IS_PED_IN_ANY_VEHICLE(Player)
		vehIndex = GET_VEHICLE_PED_IS_IN(Player)
		DETACH_ENTITY(vehIndex)
	ELSE
		DETACH_ENTITY(Player)
	ENDIF
	
	ODDJOB_STOP_SOUND(iSoundIdElevator)

	//move the elevator to the top or create it if it doesnt already exist
	IF DOES_ENTITY_EXIST(objElev[1])
		SET_ENTITY_COORDS(objElev[1], << -158.840, -942.240, 31.44 >>)
	ELSE
		objElev[1] = CREATE_OBJECT(ElevModel, << -158.840, -942.240, 31.44 >>)
		SET_ENTITY_COORDS(objElev[1], << -158.840, -942.240, 31.44 >>)
		SET_ENTITY_ROTATION(objElev[1], vMyElevRot[1])
	ENDIF
	
	IF vehIndex = NULL
		ATTACH_ENTITY_TO_ENTITY(Player, objElev[1], 0, vPlayerElevatorOffset, <<0, 0, 74.1208 >>, FALSE, FALSE, TRUE)
	ELSE
		ATTACH_ENTITY_TO_ENTITY(vehIndex, objElev[1], 0, << 0.4, -2.2, -1.2 >>, <<0, 0, 74.1208 >>)
	ENDIF
	
	IF bDoorOpen
//		SET_ENTITY_COORDS(objElevDoorControl[2], << -155.650, -941.260, 272.21 >>) //272.21
//		SET_ENTITY_COORDS(objElevDoorControl[3], << -160.720, -939.380, 269.520 >>)
	ELSE
//		SET_ENTITY_COORDS(objElevDoorControl[2], << -155.650, -941.260, 269.520>>)
//		SET_ENTITY_COORDS(objElevDoorControl[3], << -160.720, -939.380, 269.520>>)
	ENDIF	
	
	//teleport player to top
	IF vehIndex <> NULL
		DETACH_ENTITY(vehIndex, FALSE, FALSE)
	ELSE
		DETACH_ENTITY(Player, FALSE, FALSE)
	ENDIF
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
ENDPROC

/// PURPOSE:
///    place the second elevator and doors at the very top of the structure with the player inside
PROC SEND_ENEMIES_DOWN_IN_ELEVATOR()
	VECTOR tempVector

	IF NOT bElevatorAttack
		tempVector = GET_ENTITY_COORDS(objElev[1])
		IF tempVector.z >= 115.5
			MOVE_ELEVATOR(objElev[1], NULL, NULL, 0.2, FALSE, FALSE, FALSE)
//			MOVE_ELEVATOR_DOOR(objElevDoorControl[3], 0.2, TRUE)
			
			//stop moving the elevator dooor downwards when the elevator is almost at its destination to simulate it opening just prior reaching the destination
			IF tempVector.z >= 117.8
//				MOVE_ELEVATOR_DOOR(objElevDoorControl[2], 0.2, TRUE)
			ENDIF
		ELSE
			WAVE_4_WORKER_ASSAULT()
			ODDJOB_STOP_SOUND(iSoundIdElevator)
			bElevatorAttack = TRUE
		ENDIF
	ENDIF
ENDPROC


PROC EXIT_VEHICLE_AND_ATTACK_PLAYER()
	SEQUENCE_INDEX tempSeq
	INT i
	
	FOR i = 0 TO NUM_WAVE_2_WORKERS - 1
		IF NOT IS_PED_INJURED(wave2[i].ped)
			SET_PED_COMBAT_ATTRIBUTES(wave2[i].ped, CA_LEAVE_VEHICLES, TRUE)
			//have one of the car guys go after the player while the other two defend around it
			IF i <> 2
				SET_PED_SPHERE_DEFENSIVE_AREA(wave2[i].ped, wave1[3].loc, 10)
				SET_PED_COMBAT_MOVEMENT(wave2[i].ped, CM_WILLADVANCE)
			ENDIF
//			CLEAR_PED_TASKS(wave2[i].ped)
			CLEAR_SEQUENCE_TASK(tempSeq)
			OPEN_SEQUENCE_TASK(tempSeq)
				TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR)
				TASK_COMBAT_PED(NULL, Player)
			CLOSE_SEQUENCE_TASK(tempSeq)
			TASK_PERFORM_SEQUENCE(wave2[i].ped, tempSeq)
			CLEAR_SEQUENCE_TASK(tempSeq)
		ENDIF
	ENDFOR

	bUpdateGuysInCar = TRUE
	bPedsOutOfVehicle = TRUE
ENDPROC

/// PURPOSE:
///		Send a car ful of enemies to the elevator base
PROC MAKE_CAR_PEDS_ATTACK_PLAYER()
	IF NOT bPedsOutOfVehicle
		IF IS_VEHICLE_DRIVEABLE(viCombatCar)
			IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(viCombatCar)
			OR IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(viCombatCar))
			OR GET_ENTITY_DISTANCE_FROM_LOCATION(viCombatCar, << -177.2643, -1036.0834, 26.2322 >>, FALSE) <= 8	//near the vehicle's drive destination
			OR IS_VEHICLE_STUCK_TIMER_UP(viCombatCar, VEH_STUCK_ON_ROOF, 3000)
			OR IS_VEHICLE_STUCK_TIMER_UP(viCombatCar, VEH_STUCK_HUNG_UP, 3000)
			OR IS_VEHICLE_STUCK_TIMER_UP(viCombatCar, VEH_STUCK_JAMMED, 3000)
				
				IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_CAR_ARRIVES")
					STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_CAR_ARRIVES")
				ENDIF
			
				EXIT_VEHICLE_AND_ATTACK_PLAYER()
			ENDIF
		ELSE
			EXIT_VEHICLE_AND_ATTACK_PLAYER()
		ENDIF
	ENDIF
ENDPROC

//Control behavior of wave 2 enemies that attack the player in their car
PROC HANDLE_WAVE_2_ENEMY_ASSAULT()
	VECTOR vTempPos
	
	IF NOT bEarlyOut
		// To Fix Bug # 654423 - DS
		IF DOES_ENTITY_EXIST(viCombatCar) AND NOT IS_ENTITY_DEAD(viCombatCar)
			vTempPos = GET_ENTITY_COORDS(viCombatCar)
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vTempPos, TRUE) <= 5
				EXIT_VEHICLE_AND_ATTACK_PLAYER()
				bEarlyOut = TRUE
				PRINTLN("bEarlyOut = TRUE")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bCarSent //AND NOT bEarlyOut
//		// send the car to drive to the elevator if the player is near the center of the site or gets near the car after panic is triggered
		IF DOES_ENTITY_EXIST(viCombatCar)
			IF ( bPanicking AND IS_ENTITY_IN_ANGLED_AREA(Player, << -125.446, -999.097, 27.228 >>, << -173.264, -1095.935, 27.228 >>, 98, FALSE, FALSE) )
			OR ( bPanicking AND GET_PLAYER_DISTANCE_FROM_ENTITY(viCombatCar) < 50 )
			OR ( bPanicking AND GET_PLAYER_DISTANCE_FROM_LOCATION(vElev1Bottom, FALSE) < 50 )
				WAVE_2_WORKER_ASSAULT()
				bCarSent = TRUE
				PRINTLN("bCarSent = TRUE")
			ENDIF
		ENDIF
	ELSE
		//tell guys to exit the car and attack the player once they reach their destination
		MAKE_CAR_PEDS_ATTACK_PLAYER()
		
//		//controls script-controlled damage effects applied to the enemies' vehicle (tires, windows, etc)
//		UPDATE_SCRIPTED_VEHICLE_DAMAGE(viCombatCar)
	ENDIF
ENDPROC


//PURPOSE: Create the peds and helicopter necessary for the rooftop battle section of the mission
PROC CREATE_HELI_PEDS(BOOL bCreateTargetInVehicle = FALSE)
	
	REQUEST_MODEL(HeliModel)
	PRINTLN("REQUEST_MODEL- HeliModel")
	
	IF NOT bHeliModelLoaded
		IF HAS_MODEL_LOADED(HeliModel)
			PRINTLN("bHeliModelLoaded IS TRUE - 01")
			bHeliModelLoaded = TRUE
		ENDIF
	ELSE
		PRINTLN("WAITING ON HELI MODEL TO LOAD")
	ENDIF
	
	IF bHeliModelLoaded
		IF NOT bHeliPedsCreated
			//create target
			IF NOT DOES_ENTITY_EXIST(viHeli)
				viHeli = CREATE_VEHICLE(HeliModel, vHeliPos, fHeliHead)
				SET_VEHICLE_ENGINE_ON(viHeli, TRUE, TRUE)
				SET_VEHICLE_RADIO_ENABLED(viHeli, FALSE)
				SET_ENTITY_LOAD_COLLISION_FLAG(viHeli, TRUE)		
				IF bCreateTargetInVehicle
					piTarget = CREATE_PED_INSIDE_VEHICLE(viHeli, PEDTYPE_MISSION, WorkerModel[0], VS_FRONT_RIGHT)
				ELSE
					piTarget = CREATE_PED(PEDTYPE_MISSION, WorkerModel[0], vTargetPos, fTargetHead)
				ENDIF
				SET_PED_DEFAULT_COMPONENT_VARIATION(piTarget)
				ADD_PED_FOR_DIALOGUE(siteConv, 3, piTarget, "MAFIABOSS")
				SET_PED_COMBAT_ATTRIBUTES(piTarget, CA_LEAVE_VEHICLES, FALSE)
				GIVE_WEAPON_TO_PED(piTarget, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
				SET_PED_COMBAT_MOVEMENT(piTarget, CM_WILLADVANCE)
				SET_PED_ACCURACY(piTarget, 10)
				SET_ENTITY_LOAD_COLLISION_FLAG(piTarget, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(piTarget, relEnemies)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piTarget, TRUE)
				
				PRINTLN("CREATING TARGET")
				
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(viHeli, "ASSASSINATION_CONSTRUCT_HELI_GROUP")
				
				CREATE_WORKER_WAVE(6)
				
				bHeliPedsCreated = TRUE
			ENDIF
		ELSE
			PRINTLN("bHeliPedsCreated IS TRUE")
		ENDIF
		
		//blip the target if this is set
//		IF bBlipTarget
//			IF NOT DOES_BLIP_EXIST(bTargetBlip)
//				IF NOT IS_PED_INJURED(piTarget)
//					bTargetBlip = CREATE_BLIP_ON_ENTITY(piTarget, FALSE)
//					SET_BLIP_SCALE(bTargetBlip, 1)
//				ENDIF
//			ENDIF
//		ENDIF
	ELSE
		PRINTLN("bHeliModelLoaded IS FALSE")
	ENDIF
ENDPROC


//Have helicopter enemy ped periodically  alternate seats in the back of the helictoper if there is only one left
PROC CONTROL_ENEMY_SEAT_SWAPPING_IN_HELICOPTER()
	
	SEQUENCE_INDEX tempSeq
	
	IF NOT bSwitchedSeats
		IF DOES_ENTITY_EXIST(viHeli) AND NOT IS_ENTITY_DEAD(viHeli)
		
			IF GET_PED_IN_VEHICLE_SEAT(viHeli, VS_BACK_LEFT) <> NULL
				pedLeftSide = GET_PED_IN_VEHICLE_SEAT(viHeli, VS_BACK_LEFT)
				pedLeftSide = pedLeftSide
//				PRINTLN("FOUND A GUY IN THE LEFT SEAT")
			ENDIF
			IF GET_PED_IN_VEHICLE_SEAT(viHeli, VS_BACK_RIGHT) <> NULL
				pedRightSide = GET_PED_IN_VEHICLE_SEAT(viHeli, VS_BACK_RIGHT)
//				PRINTLN("FOUND A GUY IN THE RIGHT SEAT")
			ENDIF
			
			IF IS_VEHICLE_SEAT_FREE(viHeli, VS_BACK_LEFT)
				CLEAR_SEQUENCE_TASK(tempSeq)
				OPEN_SEQUENCE_TASK(tempSeq)
					TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(NULL, viHeli)
					TASK_COMBAT_PED(NULL, Player)
				CLOSE_SEQUENCE_TASK(tempSeq)
				IF NOT IS_ENTITY_DEAD(pedRightSide)
					TASK_PERFORM_SEQUENCE(pedRightSide, tempSeq)
				ENDIF
				CLEAR_SEQUENCE_TASK(tempSeq)
				
				bSwitchedSeats = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//grabs a random specified point around the rooftop - if it is too close to the player it returns NULL_VECTOR()
FUNC VECTOR PICK_NEXT_HELI_POINT()
	VECTOR tempVec
	INT tempInt
	INT numHeliPoints = 6
//	FLOAT fMinHeliDist = 20
	
	tempInt = GET_RANDOM_INT_IN_RANGE() % numHeliPoints
	
	IF tempInt = 0
		tempVec = << -153.0297, -939.2963, 278.8077 >>
	ELIF tempInt = 1
		tempVec = << -135.2749, -946.9122, 278.5359 >>
	ELIF tempInt = 2
		tempVec = << -139.7467, -970.9389, 278.9403 >>
	ELIF tempInt = 3
		tempVec = << -143.7813, -982.7974, 278.8965 >>
	ELIF tempInt = 4
		tempVec = << -168.5419, -975.7294, 279.1384 >>
	ELSE
		tempVec = << -162.3326, -959.7117, 279.2721 >>
	ENDIF
	
	RETURN tempVec
ENDFUNC


PROC CONTROL_HELI_ATTACKING_PLAYER()
//	SEQUENCE_INDEX tempSeq
	
	SWITCH heliStage
		CASE HS_ATTACKING
//			IF NOT IS_TIMER_STARTED(heliAttackTimer)
//				RESTART_TIMER_NOW(heliAttackTimer)
				IF NOT IS_ENTITY_DEAD(piTarget) AND NOT IS_ENTITY_DEAD(viHeli)
					IF IS_PED_IN_VEHICLE(piTarget, viHeli)
						SET_HELI_BLADES_SPEED(viHeli, 1.0)
						TASK_HELI_MISSION(GET_PED_IN_VEHICLE_SEAT(viHeli), viHeli, NULL, Player, <<0,0,10>>, MISSION_CIRCLE, 20, 30, -1, 325, 40)
						PRINTLN("TARGET IS IN THE CHOPPER, GO!")
					ELSE
						PRINTLN("WAITING ON TARGET TO ENTER HELICOPTER")
					ENDIF
				ENDIF
				
				RESTART_TIMER_NOW(heliFleeTimer)
				heliStage = HS_FLY_TO_POINT
//			ELIF GET_TIMER_IN_SECONDS(heliAttackTimer) > 15
//				heliStage = HS_FLY_TO_POINT
//			ENDIF
		BREAK
		CASE HS_FLY_TO_POINT
			IF NOT IS_PED_INJURED(wave6[0].ped)		//pilot
				IF DOES_ENTITY_EXIST(piTarget)
					IF NOT IS_PED_INJURED(piTarget)
						IF IS_PED_INJURED(wave6[1].ped)			//guys in back of heli
						AND IS_PED_INJURED(wave6[2].ped)
							IF NOT IS_TIMER_STARTED(heliFleeTimer2)
								RESTART_TIMER_NOW(heliFleeTimer2)
							ELSE
								IF GET_TIMER_IN_SECONDS(heliFleeTimer2) > 45
									TASK_HELI_MISSION(wave6[0].ped, viHeli, NULL, Player, <<0,0,0>>, MISSION_FLEE, 30, 300, -1, 325, 40)
									PRINTLN("heliStage = HS_FLEE_PLAYER - guys in back of heli dead")
									heliStage = HS_FLEE_PLAYER
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_TIMER_STARTED(heliFleeTimer)
								RESTART_TIMER_NOW(heliFleeTimer)
							ELIF GET_TIMER_IN_SECONDS(heliFleeTimer) > 180
								TASK_HELI_MISSION(wave6[0].ped, viHeli, NULL, Player, <<0,0,0>>, MISSION_FLEE, 30, 300, -1, 325, 40)
								PRINTLN("heliStage = HS_FLEE_PLAYER - 2 minutes are up")
								heliStage = HS_FLEE_PLAYER
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE HS_FLEE_PLAYER
			IF NOT heliFleeLinePlayed
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_PED_INJURED(piTarget)
						IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_BOSS3", CONV_PRIORITY_VERY_HIGH)
							heliFleeLinePlayed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC CHECK_FOR_PLAYER_ON_ROOF()
	
	IF NOT bSetCheckpoint 
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_ON_FOOT(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-160.140701,-981.493591,273.441498>>, <<-144.774521,-941.679199,266.633087>>, 42.000000)
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "assassin_construction_rooftop_battle")
					PRINTLN("SETTING MISSION STAGE TO 3")
					bSetCheckpoint = TRUE
				ELSE
//					PRINTLN("PLAYER IS NOT ON ROOF AREA")
				ENDIF
			ELSE	
//				PRINTLN("PLAYER IS NOT ON FOOT")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TASK_LIVING_HELI_ENEMIES_TO_EXIT_VEHICLE_AND_ATTACK_PLAYER()
	INT idx
	REPEAT NUM_WAVE_6_WORKERS idx
		IF NOT IS_PED_INJURED(wave6[idx].ped)
			SET_PED_COMBAT_ATTRIBUTES(wave6[idx].ped, CA_LEAVE_VEHICLES, TRUE)
			TASK_COMBAT_PED(wave6[idx].ped, PLAYER_PED_ID())
			PRINTLN("TASK_LIVING_HELI_ENEMIES_TO_EXIT_VEHICLE_AND_ATTACK_PLAYER. Tasking worker :", idx)
		ENDIF
	ENDREPEAT
ENDPROC

PROC TASK_TARGET_TO_EXIT_HELI_AND_COWER()
	SEQUENCE_INDEX tempSeq
	IF NOT IS_PED_INJURED(piTarget)
		SET_PED_COMBAT_ATTRIBUTES(piTarget, CA_LEAVE_VEHICLES, TRUE)
		
		OPEN_SEQUENCE_TASK(tempSeq)
			TASK_LEAVE_ANY_VEHICLE(NULL)
			TASK_COWER(NULL)
		CLOSE_SEQUENCE_TASK(tempSeq)
		TASK_PERFORM_SEQUENCE(piTarget, tempSeq)
		CLEAR_SEQUENCE_TASK(tempSeq)
		PRINTLN("TASK_TARGET_TO_EXIT_HELI_AND_COWER - target should be cowering!")
	ENDIF
ENDPROC

//PURPOSE: Control the playback of the helicpoter vehicle recording during the rooftop battle
PROC CONTROL_HELICOPTER_BEHAVIOR()

	//have enemy ped in back of helictoper move around if there is only one left
	CONTROL_ENEMY_SEAT_SWAPPING_IN_HELICOPTER()

	IF DOES_ENTITY_EXIST(viHeli)
		IF DOES_ENTITY_EXIST(piTarget)
			IF NOT IS_VEHICLE_DRIVEABLE(viHeli)
//			OR IS_PED_INJURED(piTarget)
			OR IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(viHeli))
				//make sure any playback stops if the heli is non-driveable or the target is killed
				IF NOT bHeliOutOfControl
//					IF IS_VEHICLE_DRIVEABLE(viHeli)
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viHeli)
//							STOP_PLAYBACK_RECORDED_VEHICLE(viHeli)
//						ENDIF
//					ELSE
						IF NOT IS_ENTITY_DEAD(viHeli)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viHeli)
								STOP_PLAYBACK_RECORDED_VEHICLE(viHeli)
							ENDIF

							IF IS_ENTITY_IN_AIR(viHeli)
								SET_ENTITY_LOAD_COLLISION_FLAG(viHeli, TRUE)
								SET_VEHICLE_OUT_OF_CONTROL(viHeli)
								SET_VEHICLE_ENGINE_HEALTH(viHeli, 10)
								SET_VEHICLE_DOORS_LOCKED(viHeli, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
							ELSE
								TASK_LIVING_HELI_ENEMIES_TO_EXIT_VEHICLE_AND_ATTACK_PLAYER()
								TASK_TARGET_TO_EXIT_HELI_AND_COWER()
							ENDIF
							bHeliOutOfControl = TRUE
						ENDIF
//					ENDIF
				ENDIF
			ELSE
				IF bHeliAttacking = FALSE
//					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viHeli)
//						IF GET_TIME_POSITION_IN_RECORDING(viHeli) < 104000	//near end of recording path
//							IF GET_PED_IN_VEHICLE_SEAT(viHeli, VS_BACK_LEFT) = NULL
//							AND GET_PED_IN_VEHICLE_SEAT(viHeli, VS_BACK_RIGHT) = NULL
//								STOP_PLAYBACK_RECORDED_VEHICLE(viHeli)
//								SET_CURRENT_PED_VEHICLE_WEAPON(piTarget, WEAPONTYPE_VEHICLE_PLAYER_LASER)
//								SET_PED_FIRING_PATTERN(piTarget, FIRING_PATTERN_FULL_AUTO)
//								bHeliAttacking = TRUE
//							ENDIF
//						ELSE
//							STOP_PLAYBACK_RECORDED_VEHICLE(viHeli)
//							SET_CURRENT_PED_VEHICLE_WEAPON(piTarget, WEAPONTYPE_VEHICLE_PLAYER_LASER)
//							SET_PED_FIRING_PATTERN(piTarget, FIRING_PATTERN_FULL_AUTO)
//							bHeliAttacking = TRUE
//						ENDIF
//					ENDIF
					IF NOT IS_PED_INJURED(piTarget)
						IF IS_PED_IN_VEHICLE(piTarget, viHeli)
							bHeliAttacking = TRUE
						ENDIF
					ENDIF		
				ELSE
					CONTROL_HELI_ATTACKING_PLAYER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CONTROL_CRANE_ENEMY_BEHAVIOR()
//	VECTOR vShootPos
//	SEQUENCE_INDEX tempSeq
	
	IF NOT bApplyForceToDeadGuy
		IF NOT IS_ENTITY_DEAD(wave5[3].ped)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(wave5[3].ped, PLAYER_PED_ID())
			OR IS_PED_INJURED(wave5[3].ped)
				APPLY_FORCE_TO_ENTITY(wave5[3].ped, APPLY_TYPE_IMPULSE, <<0,-4,2>>, <<0,0.20,0>>, 0, TRUE, TRUE, TRUE)
				SET_ENTITY_COLLISION(wave5[3].ped, FALSE)
				SET_ENTITY_HAS_GRAVITY(wave5[3].ped, TRUE)
				SET_ENTITY_HEALTH(wave5[3].ped, 0)
				PRINTLN("PLAYER HAS DAMAGED DUDE ON CRANE")
				bApplyForceToDeadGuy = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF NOT bTeleportCraneGuy
		IF NOT IS_ENTITY_DEAD(wave5[3].ped)
			IF NOT IS_PED_INJURED(wave5[3].ped) AND NOT IS_PED_RAGDOLL(wave5[3].ped)
				IF NOT IS_ENTITY_AT_COORD(wave5[3].ped, vCraneSpawnPos, <<5,5,5>>)
					SET_ENTITY_COORDS(wave5[3].ped, vCraneSpawnPos)
					bTeleportCraneGuy = TRUE
					PRINTLN("TELEPORTING CRANE DUDE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iCraneEnemyStage
		CASE 0
			IF bOkayToAttack
				//unfreeze enemy on crane and tell him to shoot at the player once crane bounds have streamed in around him
				IF NOT IS_PED_INJURED(wave5[3].ped)
					IF NOT IS_ENTITY_WAITING_FOR_WORLD_COLLISION(wave5[3].ped)
						SET_PED_ACCURACY(wave5[3].ped, 0)
						FREEZE_ENTITY_POSITION(wave5[3].ped, FALSE)
//						SET_ENTITY_INVINCIBLE(wave5[3].ped, FALSE)
						IF GET_SCRIPT_TASK_STATUS(wave5[3].ped, SCRIPT_TASK_SHOOT_AT_ENTITY) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(wave5[3].ped, SCRIPT_TASK_SHOOT_AT_ENTITY) <> WAITING_TO_START_TASK 
							TASK_SHOOT_AT_ENTITY(wave5[3].ped, Player, -1, FIRING_TYPE_CONTINUOUS)
						ENDIF
					ENDIF
				ENDIF
				
				iCoverTime = GET_RANDOM_INT_IN_RANGE(5000, 10000)
				iCoverTime = iCoverTime
				iCraneEnemyStage++
			ENDIF
		BREAK
		
		CASE 1		
			//crane enemy runs back and forth across the crane shooting at the player
			IF NOT IS_PED_INJURED(wave5[3].ped)
				vCranePedPos = GET_ENTITY_COORDS(wave5[3].ped)
				IF vCranePedPos.z > 294
					IF NOT IS_ENTITY_AT_COORD(wave5[3].ped, vCraneSpawnPos, <<2,2,2>>)
						IF GET_SCRIPT_TASK_STATUS(wave5[3].ped, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(wave5[3].ped, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
//							CLEAR_PED_TASKS(wave5[3].ped)
							TASK_GO_STRAIGHT_TO_COORD(wave5[3].ped, vCraneSpawnPos, PEDMOVE_RUN)
						ENDIF						
					ELSE
						IF GET_SCRIPT_TASK_STATUS(wave5[3].ped, SCRIPT_TASK_SHOOT_AT_ENTITY) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(wave5[3].ped, SCRIPT_TASK_SHOOT_AT_ENTITY) <> WAITING_TO_START_TASK
							TASK_SHOOT_AT_ENTITY(wave5[3].ped, Player, -1, FIRING_TYPE_CONTINUOUS)
						ENDIF
					ENDIF
					
//					//have the target take cover intermittently
//					IF TIMERA() >= iCoverTime
//						IF GET_RANDOM_BOOL()
//							vShootPos = vCranePos0
//						ELSE
//							vShootPos = vCranePos1
//						ENDIF
//						
//						CLEAR_SEQUENCE_TASK(tempSeq)
//						OPEN_SEQUENCE_TASK(tempSeq)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, vShootPos, PEDMOVE_RUN)
//							TASK_SHOOT_AT_ENTITY(NULL, Player, -1, FIRING_TYPE_CONTINUOUS)
//						CLOSE_SEQUENCE_TASK(tempSeq)
//						TASK_PERFORM_SEQUENCE(wave5[3].ped, tempSeq)
//						CLEAR_SEQUENCE_TASK(tempSeq)
//						
//						iCoverTime = GET_RANDOM_INT_IN_RANGE(5000, 10000)
//						SETTIMERA(0)
//						iCraneEnemyStage++
//					ENDIF
//				ELSE
//					//enemy has fallen down to lower crane level but hasn't died - tell him to shoot from there
//					IF GET_SCRIPT_TASK_STATUS(wave5[3].ped, SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
//						SET_PED_SPHERE_DEFENSIVE_AREA(wave5[3].ped, << -119.3303, -973.5360, 292.8179 >>, 1)
//						TASK_COMBAT_PED(wave5[3].ped, Player)
//					ENDIF
				ENDIF
			ENDIF
		BREAK
		
//		CASE 2
//			IF TIMERA() >= iCoverTime
//				iCoverTime = GET_RANDOM_INT_IN_RANGE(10000, 15000)
//				SETTIMERA(0)
//				iCraneEnemyStage--
//			ENDIF
//		BREAK
	ENDSWITCH
ENDPROC

//grab the player's position when they triggered the intro cutscene.
//if they arrived at the destination in the back of a taxi then just assign the player position at the location of the intro cutscene trigger point
//PROC STORE_PLAYER_POSITION()
//	FLOAT fPlayerHeight
//	
//	IF GET_GROUND_Z_FOR_3D_COORD(vIntroCutsceneTriggerPos, fPlayerHeight)
//		vPlayerPos = << vIntroCutsceneTriggerPos.x, vIntroCutsceneTriggerPos.y, fPlayerHeight >>
//	ELSE
//		vPlayerPos = vIntroCutsceneTriggerPos
//	ENDIF
//	vPlayerRot = GET_ENTITY_ROTATION(Player)
//ENDPROC

PROC HANDLE_DEBLIP_ROOFTOP_ENEMIES()
	VECTOR tempVec
	
	IF NOT bBlipsCleared
		tempVec = GET_ENTITY_COORDS(Player)
		
		IF tempVec.z <= 200
			CLEANUP_PED_BLIPS(TRUE)
			bBlipsCleared = TRUE
		ENDIF
	ENDIF
ENDPROC



PROC HANDLE_SCRIPT_CAMERA_CONTROL_IN_ELEVATOR(CAMERA_INDEX cam, BOOL bFirstElevator)

	FLOAT fRX, fRY // , fLX, fLY
	FLOAT fXSpeed
	FLOAT fYSpeed
	VECTOR vNewCamRot, vRailCamRot, vCurrentCamRot
//	BOOL bPlaySound = TRUE
//	BOOL bUpdateHorizontal = TRUE	//used for checks when to play or not play sounds
	
	fRX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
	fRY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)

	//normalize the position of the right analog stick to determine whether or not to adjust on the X or Z axis	
	IF fRX * fRX > fRY * fRY
		fXSpeed = fRX * 3.8 	//FIRST_PERSON_CAM_MOVE_SPEED UP/DOWN
//		bUpdateHorizontal = FALSE
	ELSE
		fYSpeed = -fRY * 3.8 	//FIRST_PERSON_CAM_MOVE_SPEED LEFT/RIGHT
	ENDIF
	IF IS_LOOK_INVERTED()
		fYSpeed *= -1
	ENDIF
	
	//handle camera movement and sound
	IF DOES_CAM_EXIST(cam)
        // rotation up/down
        vRailCamRot.x += fYSpeed

        // rotation l/r
        vRailCamRot.z -= fXSpeed                 
        
		vCurrentCamRot = GET_CAM_ROT(cam)
		
		//handle restraints
			vNewCamRot.x = vCurrentCamRot.x + vRailCamRot.x
			IF vNewCamRot.x > 89.5
				vNewCamRot.x = 89.5
//				IF bUpdateHorizontal
//					bPlaySound = FALSE
//				ENDIF
			ELIF vNewCamRot.x  < -50.5
				vNewCamRot.x = -50.5
//				IF bUpdateHorizontal
//					bPlaySound = FALSE
//				ENDIF
			ENDIF
			
			//set l/r movement restraints for the first elevator
			IF bFirstElevator
				vNewCamRot.z = vCurrentCamRot.z + vRailCamRot.z
				IF vNewCamRot.z < 80
				AND vNewCamRot.z > 0
					vNewCamRot.z = 80
//					IF NOT bUpdateHorizontal
//						bPlaySound = FALSE
//					ENDIF
				ELIF vNewCamRot.z  > -116
				AND vNewCamRot.z < 0
					vNewCamRot.z = -116
//					IF NOT bUpdateHorizontal
//						bPlaySound = FALSE
//					ENDIF
				ENDIF
			ELSE
				//restraint values for 2nd elevator
				vNewCamRot.z = vCurrentCamRot.z + vRailCamRot.z
				IF vNewCamRot.z < -35
					vNewCamRot.z = -35
//					IF NOT bUpdateHorizontal
//						bPlaySound = FALSE
//					ENDIF
				ELIF vNewCamRot.z > 150
					vNewCamRot.z = 150
//					IF NOT bUpdateHorizontal
//						bPlaySound = FALSE
//					ENDIF
				ENDIF
			ENDIF

        SET_CAM_ROT(cam, vNewCamRot)
	ENDIF
ENDPROC


PROC SETUP_CONSTRUCTION_DESTINATION_BLIP(BOOL bPrintObjective = TRUE)
	//add blip for the site
	IF NOT DOES_BLIP_EXIST(bSiteBlip)
		bSiteBlip = CREATE_BLIP_FOR_COORD(vIntroCutsceneTriggerPos, TRUE)
		SET_BLIP_NAME_FROM_TEXT_FILE(bSiteBlip, "ASS_CS_STATION")
		SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(bSiteBlip, << -79.8767, -917.7642, 28.1898 >>, 269.6892)
		IF bPrintObjective
			PRINT_NOW("ASS_CS_GOSITE", DEFAULT_GOD_TEXT_TIME, 1)
			TRIGGER_MUSIC_EVENT("ASS5_DRIVE")
			PRINTLN("TRIGGERING MUSIC EVENT: ASS5_DRIVE")
		ENDIF
	ENDIF
ENDPROC


PROC PRINT_ELEVATOR_ACTIVATION_HELP()
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ASS_CS_ELEHELP")
		// Bug Fix - 553429
//		CLEAR_HELP()
		PRINT_HELP("ASS_CS_ELEHELP")
	ENDIF
ENDPROC


//print help explaining how to activate the elevator
FUNC BOOL CAN_PLAYER_ACTIVATE_ELEVATOR(VECTOR vCoordToCheck)
//	FLOAT fTempPos
//	fTempPos = GET_ENTITY_DISTANCE_FROM_LOCATION(Player, vCoordToCheck)
//	PRINTLN("DISTANCE = ", fTempPos)

	IF GET_ENTITY_DISTANCE_FROM_LOCATION(Player, vCoordToCheck) <= 1.65
	AND NOT IS_ENTITY_IN_AIR(Player)
	AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
	AND GET_CLOSEST_LIVING_ENEMY(3.0) = NULL
	AND NOT IS_CELLPHONE_ON_HOME_SCREEN()
	AND NOT IS_PED_JUMPING(Player)
	AND NOT IS_PED_IN_ANY_VEHICLE(Player)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



//Handles the loading and unloading of the panic cutscene STREAMVOL so that we make sure it is loaded in when close to the site and unloads if the player dirves away
PROC HANDLE_STREAMVOL_LOADING_UNLOADING()
	IF NOT bLoadSceneRequested
		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vIntroCutsceneTriggerPos) <= DEFAULT_CUTSCENE_LOAD_DIST
//			svPanicCutscene = STREAMVOL_CREATE_FRUSTUM(<< -146.1097, -944.1489, 114.8948 >>, << -7.6114, -0.0000, 0.6829 >>, 125.0, FLAG_MAPDATA)
			PREFETCH_SRL("ass_construction")
			
			PRINTLN("CALLING REQUEST TO PREFETCH SRL")
			
			bLoadSceneRequested = TRUE
		ENDIF
	ELSE
		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vIntroCutsceneTriggerPos) > DEFAULT_CUTSCENE_UNLOAD_DIST
//			IF STREAMVOL_IS_VALID(svPanicCutscene)
//				STREAMVOL_DELETE(svPanicCutscene)
			IF IS_SRL_LOADED()
				END_SRL()
				PRINTLN("CALLING REQUEST TO END_SRL")
				bLoadSceneRequested = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC
						

PROC ALTER_WAVE_ACCURACY(INT iWave, INT iAccuracy)
	INT idx

	SWITCH iWave
		CASE 1
			FOR idx = 0 TO NUM_WAVE_1_WORKERS - 1
				IF DOES_ENTITY_EXIST(wave1[idx].ped)
					IF NOT IS_PED_INJURED(wave1[idx].ped)
						SET_PED_ACCURACY(wave1[idx].ped, iAccuracy)
						PRINTLN("WAVE 1 - SETTING PED ACCURACY TO: ", iAccuracy, " ON PED: ", idx)
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		CASE 2
			FOR idx = 0 TO NUM_WAVE_2_WORKERS - 1
				IF DOES_ENTITY_EXIST(wave2[idx].ped)
					IF NOT IS_PED_INJURED(wave2[idx].ped)
						SET_PED_ACCURACY(wave2[idx].ped, iAccuracy)
						PRINTLN("WAVE 2 - SETTING PED ACCURACY TO: ", iAccuracy, " ON PED: ", idx)
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		CASE 3
			FOR idx = 0 TO NUM_WAVE_3_WORKERS - 1
				IF DOES_ENTITY_EXIST(wave3[idx].ped)
					IF NOT IS_PED_INJURED(wave3[idx].ped)
						SET_PED_ACCURACY(wave3[idx].ped, iAccuracy)
						PRINTLN("WAVE 3 - SETTING PED ACCURACY TO: ", iAccuracy, " ON PED: ", idx)
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		CASE 4
			FOR idx = 0 TO NUM_WAVE_4_WORKERS - 1
				IF DOES_ENTITY_EXIST(wave4[idx].ped)
					IF NOT IS_PED_INJURED(wave4[idx].ped)
						SET_PED_ACCURACY(wave4[idx].ped, iAccuracy)
						PRINTLN("WAVE 4 - SETTING PED ACCURACY TO: ", iAccuracy, " ON PED: ", idx)
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		CASE 5
			FOR idx = 0 TO NUM_WAVE_5_WORKERS - 1
				IF DOES_ENTITY_EXIST(wave5[idx].ped)
					IF NOT IS_PED_INJURED(wave5[idx].ped)
						SET_PED_ACCURACY(wave5[idx].ped, iAccuracy)
						PRINTLN("WAVE 5 - SETTING PED ACCURACY TO: ", iAccuracy, " ON PED: ", idx)
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		CASE 6
			FOR idx = 0 TO NUM_WAVE_6_WORKERS - 1
				IF DOES_ENTITY_EXIST(wave6[idx].ped)
					IF NOT IS_PED_INJURED(wave6[idx].ped)
						SET_PED_ACCURACY(wave6[idx].ped, iAccuracy)
						PRINTLN("WAVE 6 - SETTING PED ACCURACY TO: ", iAccuracy, " ON PED: ", idx)
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("INVALID WAVE NUMBER PASSED TO ALTER_WAVE_ACCURACY")
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC CONTROL_ROOFTOP_ACCURACY()
	// Altering accuracy for a few seconds, so the player doesn't get killed right away.  Addressing Bug # 734059
	IF IS_TIMER_STARTED(changeAccuracyTimer02)
		IF NOT bChangeAccuracy02
			IF GET_TIMER_IN_SECONDS(changeAccuracyTimer02) >= CHANGE_ACCURACY_TIME
				PRINTLN("ROOFTOP: CHANGE_ACCURACY_TIME IS UP, ALTERING ACCURACY")
				ALTER_WAVE_ACCURACY(5, 15)
				ALTER_WAVE_ACCURACY(6, 15)
				bChangeAccuracy02 = TRUE
			ENDIF
		ENDIF
	ELSE
		START_TIMER_NOW(changeAccuracyTimer02)
		PRINTLN("STARTING TIMER - changeAccuracyTimer02")
		
		ALTER_WAVE_ACCURACY(5, 0)
		ALTER_WAVE_ACCURACY(6, 0)
	ENDIF
ENDPROC

PROC UPDATE_TRIGGER_EARLY_ROOFTOP_BATTLE()
	VECTOR vPlayerPosition
	FLOAT fDistance
	BOOL bPlayerParachuting
	
	SWITCH iTriggerEarlyUpdateStages
		CASE 0
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
//				PRINTLN("vPlayerPosition = ", vPlayerPosition)
				
				IF GET_PED_PARACHUTE_STATE(Player) != PPS_INVALID
					IF GET_PED_PARACHUTE_STATE(Player) = PPS_DEPLOYING
					OR GET_PED_PARACHUTE_STATE(Player) = PPS_PARACHUTING
						bPlayerParachuting = TRUE
						PRINTLN("bPlayerParachuting = TRUE")
					ENDIF
				ENDIF
				
				IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
				OR bPlayerParachuting
				OR IS_ENTITY_IN_ANGLED_AREA(Player, <<-117.224998,-977.256531,200.162506>>, <<-135.922775,-971.229309,268.880737>>, 11.000000)
					fDistance = VDIST2(vPlayerPosition, vRooftopPosition)
					PRINTLN("fDistance = ", fDistance)
					
					IF fDistance < 300 * 300
					AND vPlayerPosition.z > 150
						bTriggerRooftop = TRUE
						PRINTLN("bTriggerRooftop = TRUE")
						
						IF DOES_BLIP_EXIST(bSiteBlip)
							REMOVE_BLIP(bSiteBlip)
							PRINTLN("bTriggerRooftop: REMOVING BLIP - bSiteBlip")
						ENDIF
						
						PRINTLN("iTriggerEarlyUpdateStages = 1")
						iTriggerEarlyUpdateStages = 1
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//======================================================================================================================
		CASE 1
			IF bTriggerRooftop
				REMOVE_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
				REMOVE_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
				REMOVE_WORKER_WAVE(wave3, NUM_WAVE_3_WORKERS)
				CLEANUP_WARNING_GUYS()
				
				IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_SHOOTOUT_START")
					STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_SHOOTOUT_START")
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_CAR_ARRIVES")
					STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_CAR_ARRIVES")
				ENDIF
				IF DOES_ENTITY_EXIST(viCombatCar)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(viCombatCar)
				ENDIF
				
				iStageNum = 0
				MainStage = STAGE_ROOFTOP_BATTLE
				PRINTLN("MainStage = STAGE_ROOFTOP_BATTLE - EARLY TRIGGER")
				
				PRINTLN("iTriggerEarlyUpdateStages = 2")
				iTriggerEarlyUpdateStages = 2
			ENDIF
		BREAK
		//======================================================================================================================
		CASE 2
		
		BREAK
	ENDSWITCH
	
ENDPROC

PROC HANDLE_REPLAY_WEAPON_EQUIP()
	wtFailWeapon = Get_Fail_Weapon(1)
	
	IF wtFailWeapon <> WEAPONTYPE_INVALID
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), wtFailWeapon, 0, FALSE, FALSE)
		PRINTLN("GIVING PLAYER LAST KNOWN WEAPON")
	ELSE
		PRINTLN("WEAPON IS INVALID")
		wtFailWeapon = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), wtFailWeapon, 0, FALSE, FALSE)
	ENDIF
ENDPROC

PROC GO_TO_CHECKPOINT_1()
	
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
		
//			bDebugSkipping = TRUE
		
			PRINTLN("GO_TO_CHECKPOINT_1() - bDoingPSkip")
			
			SET_ENTITY_COORDS(Player, << 804.2744, -1077.5593, 27.5087 >>)
			SET_ENTITY_HEADING(Player, 129.6961)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			PRINTLN("MOVING PLAYER HERE -  << 135.6720, -1059.0262, 28.1924 >>")
			
			REMOVE_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
			REMOVE_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
			CLEANUP_WARNING_GUYS()
			REMOVING_HELI_AND_TARGET()
			
			CLEANUP_DEST_BLIPS()
			
			INITIALIZE_NEXT_STAGE()
			
			REMOVE_SECOND_ELEVATOR()
			
//			IF bFirstCheckpointSceneLoad
//				bFirstCheckpointSceneLoad = FALSE
//			ENDIF
			
			bPanicking = FALSE
			bDoingPSkip = FALSE
			bDoingJSkip = FALSE
			bLoadSceneRequested = FALSE
			bIntroSceneCreated = FALSE
			bEarlyAggro = FALSE
			bGuardsCreatedForFirstStage = FALSE
			iWarningGuysUpdate = 0
		ENDIF
	#ENDIF
	
	bGuardsCreatedForFirstStage = FALSE
	
	SETUP_CONSTRUCTION_DESTINATION_BLIP()
	
//	CREATE_WORKER_WAVE(1)
//	CREATE_WORKER_WAVE(2)
//	CREATE_WARNING_GUYS()

	
	END_REPLAY_SETUP()
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	FADE_IN()
	
	MainStage = STAGE_GET_TO_SITE
	PRINTLN("MainStage = STAGE_GET_TO_SITE")
ENDPROC

PROC GO_TO_CHECKPOINT_2()

	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip

			SET_ENTITY_COORDS(Player, << -102.4433, -907.4056, 28.2065 >>)
			SET_ENTITY_HEADING(Player, 160.4677)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			PRINTLN("MOVING PLAYER HERE -  << 135.6720, -1059.0262, 28.1924 >>")
			
			PRINTLN("GO_TO_CHECKPOINT_2() - bDoingPSkip")
			
//			bDebugSkipping = TRUE
		
			REMOVE_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
			REMOVE_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
			CLEANUP_WARNING_GUYS()
			REMOVING_HELI_AND_TARGET()
			
			CLEANUP_DEST_BLIPS()
			
			REMOVE_SECOND_ELEVATOR()
			
			INITIALIZE_NEXT_STAGE()
			
//			IF bSecondCheckpointSceneLoad
//				bSecondCheckpointSceneLoad = FALSE
//			ENDIF
			
			bDoingJSkip = FALSE
			bDoingPSkip = FALSE
			
			bEarlyAggro = FALSE
			iWarningGuysUpdate = 0
		ENDIF
	#ENDIF
	
	bPanicking = FALSE
	
	WHILE NOT ARE_CONSTRUCTION_PED_MODELS_LOADED()
		WAIT(0)
	ENDWHILE
	
	CREATE_WORKER_WAVE(1)
	CREATE_WORKER_WAVE(2)
	CREATE_WARNING_GUYS()
	
	END_SRL()
	
	HANDLE_REPLAY_WEAPON_EQUIP()
	
	TRIGGER_MUSIC_EVENT("ASS5_RESTART1")
	PRINTLN("TRIGGERING MUSIC - ASS5_RESTART1")
	
	END_REPLAY_SETUP()
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	FADE_IN()
	MainStage = STAGE_ENTER_SITE
	PRINTLN("MainStage = STAGE_ENTER_SITE")
ENDPROC

PROC GO_TO_CHECKPOINT_3()
	
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
			PRINTLN("GO_TO_CHECKPOINT_3() - bDoingPSkip")
			
//			bDebugSkipping = TRUE
			
			REMOVE_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
			REMOVE_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
			REMOVE_WORKER_WAVE(wave3, NUM_WAVE_3_WORKERS)
			CLEANUP_WARNING_GUYS()
			REMOVING_HELI_AND_TARGET()
			
			CLEANUP_DEST_BLIPS()
		
			bHeliModelLoaded = FALSE
			bHeliPedsCreated = FALSE
			bRooftopWaveCreated = FALSE
			bElevatorAttack = FALSE
			
//			IF bThirdCheckpointSceneLoad
//				bThirdCheckpointSceneLoad = FALSE
//			ENDIF
			
			IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<-182.0891, -1004.0555, 113.1355>>)
				REMOVE_COVER_POINT(ciSecondFloorPosition)
				PRINTLN("REMOVING COVER POINT - ciSecondFloorPosition")
			ENDIF
	
			ciSecondFloorPosition = ADD_COVER_POINT(<<-182.0891, -1004.0555, 113.1355>>, 341.2941, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)
	
			SET_ENTITY_COORDS(Player, << -182.1094, -1004.0585, 113.1362 >>, FALSE)
			SET_ENTITY_HEADING(Player, 250.235)
			
			INITIALIZE_NEXT_STAGE()
			bDoingPSkip = FALSE
			bDoingJSkip = FALSE
		ENDIF
	#ENDIF
	
	WHILE NOT ARE_CONSTRUCTION_PED_MODELS_LOADED()
		WAIT(0)
	ENDWHILE
	
	CREATE_WORKER_WAVE(3)
	
	IF NOT IS_PED_INJURED(wave3[0].ped)
		SET_ENTITY_COORDS(wave3[0].ped, << -152.26974, -1003.76453, 113.13822 >>)
	ENDIF
	
	WAVE_3_WORKER_ASSAULT()
	
	SET_GROUND_ELEVATOR_TO_TOP(FALSE)
	
	GIVE_PLAYER_WEAPONS(WEAPONTYPE_PISTOL, 50)
	
	ALTER_WAVE_ACCURACY(3, 0)
	
	HANDLE_REPLAY_WEAPON_EQUIP()
		
	IF NOT IS_TIMER_STARTED(changeAccuracyTimer01)
		START_TIMER_NOW(changeAccuracyTimer01)
		PRINTLN("STARTING TIMER - changeAccuracyTimer01")
	ELSE
		RESTART_TIMER_NOW(changeAccuracyTimer01)
		PRINTLN("RESTARTING TIMER - changeAccuracyTimer01")
	ENDIF
		
	TRIGGER_MUSIC_EVENT("ASS5_RESTART2")
	PRINTLN("TRIGGERING MUSIC - ASS5_RESTART2")
	
	FORCE_PED_AI_AND_ANIMATION_UPDATE(Player, TRUE)
	TASK_PUT_PED_DIRECTLY_INTO_COVER(Player, << -182.1094, -1004.0585, 113.1362 >>, -1)
		
	END_REPLAY_SETUP()
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(90)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	FADE_IN()
	MainStage = STAGE_GET_TO_SECOND_ELEVATOR
	PRINTLN("MainStage = STAGE_GET_TO_SECOND_ELEVATOR")
ENDPROC

PROC GO_TO_CHECKPOINT_4()
	PRINTLN("GO_TO_CHECKPOINT_4")
	
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
		
			PRINTLN("GO_TO_CHECKPOINT_4() - DEBUG SKIPPING")
			
//			bDebugSkipping = TRUE
		
			REMOVE_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
			REMOVE_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
			REMOVE_WORKER_WAVE(wave3, NUM_WAVE_3_WORKERS)
			REMOVE_WORKER_WAVE(wave4, NUM_WAVE_4_WORKERS)
			REMOVE_WORKER_WAVE(wave5, NUM_WAVE_5_WORKERS)
			REMOVE_WORKER_WAVE(wave6, NUM_WAVE_6_WORKERS)
			CLEANUP_WARNING_GUYS()
			REMOVING_HELI_AND_TARGET()
			
			CLEANUP_DEST_BLIPS()
			
			bHeliModelLoaded = FALSE
			bHeliPedsCreated = FALSE
			bRooftopWaveCreated = FALSE
			bOkayToAttack = FALSE
			
			IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<-150.5964, -945.8911, 268.1318>>)
				REMOVE_COVER_POINT(ciRoofPosition)
				PRINTLN("REMOVING COVER POINT - ciRoofPosition")
			ENDIF
			
			ciRoofPosition = ADD_COVER_POINT(<<-150.5964, -945.8911, 268.1318>>, 231.5456, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_180)
			
			SET_ENTITY_COORDS(Player, vLastStageCoverPosition, FALSE)
			SET_ENTITY_HEADING(Player, fLastStageCoverHeading)
			
			INITIALIZE_NEXT_STAGE()
			bDoingPSkip = FALSE
			bDoingJSkip = FALSE
		ENDIF
	#ENDIF
	
	WHILE NOT ARE_CONSTRUCTION_PED_MODELS_LOADED()
		WAIT(0)
	ENDWHILE
	
	WHILE NOT HAS_HELICOPTER_MODEL_LOADED()
		WAIT(0)
	ENDWHILE
	
	
	SET_SECOND_ELEVATOR_TO_TOP(TRUE, FALSE)
	
	CREATE_HELI_PEDS(TRUE)
	CREATE_WORKER_WAVE(5)
		
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	FORCE_PED_AI_AND_ANIMATION_UPDATE(Player, TRUE)
	TASK_PUT_PED_DIRECTLY_INTO_COVER(Player, vLastStageCoverPosition, -1)
	
	END_REPLAY_SETUP()
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(90)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()

	FADE_IN()
	
	bTargetKillLinePlayed = FALSE
	bFranklinJumpLinePlayed = FALSE
	
	TRIGGER_MUSIC_EVENT("ASS5_RESTART3")
	PRINTLN("TRIGGERING MUSIC - ASS5_RESTART3")
	
	MainStage = STAGE_ROOFTOP_BATTLE
	PRINTLN("MainStage = STAGE_ROOFTOP_BATTLE")
ENDPROC

PROC GO_TO_CHECKPOINT_5()
	PRINTLN("GO_TO_CHECKPOINT_5")
	
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
		
			PRINTLN("GO_TO_CHECKPOINT_5() - DEBUG SKIPPING")
			
//			bDebugSkipping = TRUE
		
			REMOVE_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
			REMOVE_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
			REMOVE_WORKER_WAVE(wave3, NUM_WAVE_3_WORKERS)
			REMOVE_WORKER_WAVE(wave4, NUM_WAVE_4_WORKERS)
			REMOVE_WORKER_WAVE(wave5, NUM_WAVE_5_WORKERS)
			REMOVE_WORKER_WAVE(wave6, NUM_WAVE_6_WORKERS)
			CLEANUP_WARNING_GUYS()
			REMOVING_HELI_AND_TARGET()
			
			CLEANUP_DEST_BLIPS()

			bHeliModelLoaded = FALSE
			bHeliPedsCreated = FALSE
			
			IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<-150.5964, -945.8911, 268.1318>>)
				REMOVE_COVER_POINT(ciRoofPosition)
				PRINTLN("REMOVING COVER POINT - ciRoofPosition")
			ENDIF
			
			SET_ENTITY_COORDS(Player, <<-102.2210, -941.3790, 28.2330>>, FALSE)
			SET_ENTITY_HEADING(Player, 67.1012)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()

			bDoingPSkip = FALSE
			bDoingJSkip = FALSE
		ENDIF
	#ENDIF

	SET_SECOND_ELEVATOR_TO_TOP(TRUE, FALSE)
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	END_REPLAY_SETUP()
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	FADE_IN()
	MainStage = STAGE_CLEANUP
	PRINTLN("MainStage = STAGE_CLEANUP")
ENDPROC

PROC GO_TO_CLEANUP()
	
	//explode the heli
	IF DOES_ENTITY_EXIST(viHeli) AND IS_VEHICLE_DRIVEABLE(viHeli)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viHeli)
			STOP_PLAYBACK_RECORDED_VEHICLE(viHeli)
		ENDIF
//		ADD_EXPLOSION(GET_ENTITY_COORDS(viHeli), EXP_TAG_PETROL_PUMP)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		FAKE_KEY_PRESS(KEY_K)
	#ENDIF
	
	//remove the target and blips
	DELETE_PED(piTarget)
//	IF DOES_BLIP_EXIST(bTargetBlip)
//		REMOVE_BLIP(bTargetBlip)
//	ENDIF
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	PRINTLN("Assassination Passed via J-SKIP")
	
	INITIALIZE_NEXT_STAGE()
	FADE_IN(500)
	MainStage = STAGE_CLEANUP
ENDPROC



/// PURPOSE:
///		Create necessary objects for primary stage of the mission
PROC DO_STAGE_SPAWNING()
	CREATE_CAMERAS()
	CREATE_VEHICLES()
//	CREATE_ELEVATOR_DOORS()
	SETUP_UI_LABELS()
	HANDLE_COVER_PROPS()
	CREATE_HEALTH_PACK()
	CREATE_PARACHUTE_PACK()
	
	INITIALIZE_NEXT_STAGE()
	
	IF bReplaying
	
		IF g_savedGlobals.sAssassinData.fConstructionMissionTime <> 0
			RESTART_TIMER_AT(missionTimer, g_savedGlobals.sAssassinData.fConstructionMissionTime)
			PRINTLN("RESTARTING TIMER - missionTimer AT: ", g_savedGlobals.sAssassinData.fConstructionMissionTime)	
		ENDIF
	
		//iStageToUse is now determined right before the main WHILE loop of the script!

		//player at payphone
		IF iStageToUse = 0
			GO_TO_CHECKPOINT_1()
		//player outside of site on ground level
		ELIF iStageToUse = 1
			GO_TO_CHECKPOINT_2()
		//player on mid level of the building
		ELIF iStageToUse = 2
			GO_TO_CHECKPOINT_3()
		//rooftop
		ELIF iStageToUse = 3
			GO_TO_CHECKPOINT_4()
		ELIF iStageToUse = 4
			GO_TO_CHECKPOINT_5()
		ENDIF
	ELSE
		IF NOT IS_TIMER_STARTED(missionTimer)
			START_TIMER_NOW(missionTimer)
			PRINTLN("STARTING TIMER - missionTimer")
			
			g_savedGlobals.sAssassinData.fConstructionMissionTime = 0
			PRINTLN("CONSTRUCTION GLOBAL MISSION TIMER = ", g_savedGlobals.sAssassinData.fConstructionMissionTime)
		ENDIF
	
//		CREATE_WORKER_WAVE(1)
//		CREATE_WORKER_WAVE(2)
//		CREATE_WARNING_GUYS()
		SETUP_CONSTRUCTION_DESTINATION_BLIP()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "assassin_construction_get_to_site")
		OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(viReplayVehicle)
		MainStage = STAGE_GET_TO_SITE
		PRINTLN("MainStage = STAGE_GET_TO_SITE  (from DO_STAGE_SPAWNING)")
	ENDIF
ENDPROC

PROC REMOVE_SKINNED_PHONE_PROP()
	VECTOR vPlayerPos
	
	IF NOT DOES_ENTITY_EXIST(oPayPhoneAnimated)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	IF VDIST2(vPlayerPos, vPhoneLocation) > 10000 // 100m
		IF DOES_ENTITY_EXIST(oPayPhoneAnimated)
			SET_MODEL_AS_NO_LONGER_NEEDED(P_PHONEBOX_01B_S)
			DELETE_OBJECT(oPayPhoneAnimated)
			PRINTLN("DELETING OBJECT - oPayPhoneAnimated")
		ENDIF
		
		IF DOES_ENTITY_EXIST(oPayPhone)
			SET_ENTITY_VISIBLE(oPayPhone, TRUE)
			SET_ENTITY_COLLISION(oPayPhone, TRUE)
			PRINTLN("TURNING BACK ON VISIBILITY AND COLLISION")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle blip behavior and enemy patrol behavior in first stage
PROC DO_STAGE_GET_TO_SITE()

	REMOVE_SKINNED_PHONE_PROP()

	GRAB_RETRY_VEHICLE()
	
	//handles streaming/creation of enemies when the player is en route to the destination
	IF NOT bGuardsCreatedForFirstStage
	OR bPanicking = TRUE
		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vIntroCutsceneTriggerPos, FALSE) < 500
			IF ARE_CONSTRUCTION_PED_MODELS_LOADED()
				CREATE_WORKER_WAVE(1)
				CREATE_WORKER_WAVE(2)
				CREATE_WARNING_GUYS()
				bGuardsCreatedForFirstStage = TRUE
				PRINTLN("bGuardsCreatedForFirstStage = TRUE")
			ENDIF
		ENDIF
	ENDIF
	
	//advance past the cutscene if you go the area while wanted
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		IF IS_PLAYER_IN_CONSTRUCTION_AREA()
		OR GET_PLAYER_DISTANCE_FROM_LOCATION(vIntroCutsceneTriggerPos) < 25
			bPanicking = TRUE
			PRINTLN("bPanicking = TRUE VIA WANTED LEVEL ")
		ENDIF
	ENDIF
		
	//if the player attacks the guards prior to triggering the cutscene, skip the cutscene and advance to the next stage
	IF bPanicking
//		IF STREAMVOL_IS_VALID(svPanicCutscene)
//			STREAMVOL_DELETE(svPanicCutscene)
//			PRINTLN("REMOVING - svPanicCutscene")
//		ENDIF
		END_SRL()
		
		IF bGuardsCreatedForFirstStage
			INITIALIZE_NEXT_STAGE()
			MainStage = STAGE_ENTER_SITE
			PRINTLN("MainStage = STAGE_ENTER_SITE - bPanicking = TRUE")
			EXIT
		ENDIF
	ENDIF


	SWITCH iStageNum
		CASE 0
			IF NOT DOES_ENTITY_EXIST(objElev[0])
				CREATE_GROUND_ELEVATOR()
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vIntroCutsceneTriggerPos, <<5,5,LOCATE_SIZE_HEIGHT>>, TRUE)
				
			ELSE
				//draw corona
			ENDIF
			
			//create site blip and wait for player to get close to the construction site
			IF IS_PLAYER_NEAR_COORD_AND_NOT_A_TAXI_PASSENGER(vIntroCutsceneTriggerPos, 5)
			AND IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
				IF DOES_BLIP_EXIST(bSiteBlip)
					REMOVE_BLIP(bSiteBlip)
				ENDIF
				
				iStageNum ++
			ELSE
				//make sure the volume around the panic cutscene streams in/out properly based on player proximity to the vIntroCutsceneTriggerPos
				HANDLE_STREAMVOL_LOADING_UNLOADING()
			ENDIF
		BREAK
		
		CASE 1
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 10, 1)
			OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				IF NOT bPanicking
					RESTART_TIMER_NOW(tPanicFailSafeTimer)
					PRINTLN("RESTARTING TIMER - tPanicFailSafeTimer")
					
					INITIALIZE_NEXT_STAGE()
					MainStage = STAGE_PANIC_CUTSCENE
					PRINTLN("MainStage = STAGE_PANIC_CUTSCENE")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Play a cutscene of an enemy calling the target on the upper levels to verify the player's identity
PROC DO_STAGE_PANIC_CUTSCENE()
	VECTOR vElevPos
	FLOAT fElevatorDistPerFrame
	
	SWITCH iStageNum
		CASE 0
			//prep the player
			IF ( IS_PLAYER_READY_FOR_ASSASSINATION_CUTSCENE(5.0, FALSE)
			AND IS_SRL_LOADED() )
//			AND STREAMVOL_HAS_LOADED(svPanicCutscene)) 
			OR (IS_PLAYER_READY_FOR_ASSASSINATION_CUTSCENE() AND IS_TIMER_STARTED(tPanicFailSafeTimer) AND GET_TIMER_IN_SECONDS(tPanicFailSafeTimer) > 15)
				IF NOT bIntroSceneCreated
					iRoadNode01 = ADD_ROAD_NODE_SPEED_ZONE(<< -69.7777, -923.6335, 28.2933 >>, 10.0, 0)
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(vehPlayerIsIn)
								IF NOT IS_ENTITY_DEAD(vehPlayerIsIn)
									SET_VEH_RADIO_STATION(vehPlayerIsIn, "OFF")
									PRINTLN("TURNING OFF RADIO")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//create target and extra ped that are used in the intro cutscene 
					CREATE_TARGET(TRUE)
					IF NOT IS_PED_INJURED(piTarget)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(piTarget, TRUE)
						TASK_PLAY_ANIM(piTarget, "oddjobs@assassinate@construction@", "CS_GetInLift", INSTANT_BLEND_IN, SLOW_BLEND_OUT, 9500)
					ENDIF
					IF NOT IS_PED_INJURED(piCutscenePed)
//						TASK_PLAY_ANIM(piCutscenePed, "oddjobs@assassinate@construction@", "react_small_variations_g", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(piCutscenePed, TRUE)
						TASK_PLAY_ANIM(piCutscenePed, "oddjobs@assassinate@construction@", "idle_a", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
						TASK_LOOK_AT_ENTITY(piCutscenePed, piTarget, -1)
					ENDIF
					
					CREATE_MODEL_HIDE(vElev2Bottom, 5, PROP_CONSLIFT_LIFT, TRUE)
					CREATE_SECOND_ELEVATOR()
					IF DOES_ENTITY_EXIST(objElev[1])
						IF NOT IS_PED_INJURED(piTarget)
							//attach since we want him to move with the elevator later
							ATTACH_ENTITY_TO_ENTITY(piTarget, objElev[1], 0, << -1.75, -1.88, -1.3 >>, <<0, 0, 84.2 >>)
						ENDIF
					ENDIF
					
					//create intro cutscene cameras
					camIntroA = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-147.566757,-958.613892,114.883949>>,<<-1.648425,-0.298680,24.550936>>,31.352049)
					camIntroB = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-147.235367,-957.665466,114.866714>>,<<-1.648426,-0.298680,25.520023>>,31.352049) 
					
					SHAKE_CAM(camIntroA, "HAND_SHAKE", 0.2)
					SHAKE_CAM(camIntroB, "HAND_SHAKE", 0.2)
					
					bIntroSceneCreated = TRUE
				ELSE
					START_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_CUTSCENE")
					PRINTLN("ASSASSINATION_CONSTRUCT_CUTSCENE STARTED!")
					
					SET_SRL_READAHEAD_TIMES(5, 5, 5, 5)
					BEGIN_SRL()
					
					SET_SRL_POST_CUTSCENE_CAMERA(<<-112.1211, -907.9638, 30.1577>>, <<-1.9677, 0.0000, 71.4750>>)
					
					ODDJOB_ENTER_CUTSCENE()
					GET_ASSASSIN_CUTSCENE_START_TIME()
					
					OVERRIDE_LODSCALE_THIS_FRAME(1.0)
					
					SET_CAM_ACTIVE_WITH_INTERP(camIntroB, camIntroA, 4800, GRAPH_TYPE_LINEAR)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					ADD_PED_FOR_DIALOGUE(siteConv, 3, piTarget, "MAFIABOSS")
					
					IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_SCARE", CONV_PRIORITY_VERY_HIGH)
						PRINTLN("CALLING CONVERSATION IN PANIC CUTSCENE - 01")
						bPlayedLined = TRUE
					ENDIF
					
					bGrabbedDistance = FALSE // Reset Grab Distance, so it's not the distance from payphone to building.
					
					SETTIMERA(0)
					iStageNum ++
				ENDIF
			ENDIF
		BREAK
		
		//show closeup of target
		CASE 1
		
			SET_SRL_TIME( TO_FLOAT(TIMERA()) )
			
			IF NOT bPlayedLined
				IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_SCARE", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("CALLING CONVERSATION IN PANIC CUTSCENE - 02")
					bPlayedLined = TRUE
				ENDIF
			ENDIF
		
			IF TIMERA() >= 4800
				SET_CAM_PARAMS(camIntroA, <<-152.648544,-944.800049,114.202782>>,<<2.386309,-0.295268,43.695774>>,31.342575)
				SET_CAM_PARAMS(camIntroB, <<-152.750519,-944.693359,114.208893>>,<<2.386309,-0.295268,43.695774>>,31.342575)
				SHAKE_CAM(camIntroA, "HAND_SHAKE", 0.2)
				SHAKE_CAM(camIntroB, "HAND_SHAKE", 0.2)
				SET_CAM_ACTIVE_WITH_INTERP(camIntroB, camIntroA, 6000, GRAPH_TYPE_LINEAR)
				
				iStageNum++
			ENDIF
			
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)
		BREAK
		
		//Cut to shot further out once it's time to start elevator.
		CASE 2
			
			SET_SRL_TIME( TO_FLOAT(TIMERA()) )
			
			IF TIMERA() >= 7500
				SET_CAM_PARAMS(camIntroA, <<-165.200241,-938.027649,113.466736>>,<<4.389886,0.213688,-112.112709>>,32.212269)
				SET_CAM_PARAMS(camIntroB, <<-164.622894,-937.124084,113.467522>>,<<77.824348,0.213692,-131.064331>>,32.212269)
				SHAKE_CAM(camIntroA, "HAND_SHAKE", 0.2)
				SHAKE_CAM(camIntroB, "HAND_SHAKE", 0.2)
				SET_CAM_ACTIVE_WITH_INTERP(camIntroB, camIntroA, 4250, GRAPH_TYPE_SIN_ACCEL_DECEL)

				ODDJOB_STOP_SOUND(iSoundIdElevator)
				iStageNum++
			ENDIF
			
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)
		BREAK
		
		
		//elevator ascends, cut to shot outside the building.
		CASE 3
			
			SET_SRL_TIME( TO_FLOAT(TIMERA()) )
			
			IF TIMERA() >= 9250
//				SET_CAM_PARAMS(camIntroA, <<-160.488113,-921.966370,112.177086>>,<<64.558716,0.213686,-174.816742>>,50.066635)
//				SET_CAM_PARAMS(camIntroB, <<-160.456223,-922.317749,112.918907>>,<<64.558716,0.213686,-174.816742>>,50.066635)
//				SHAKE_CAM(camIntroA, "HAND_SHAKE", 0.2)
//				SHAKE_CAM(camIntroB, "HAND_SHAKE", 0.2)
//				SET_CAM_ACTIVE_WITH_INTERP(camIntroB, camIntroA, 2700, GRAPH_TYPE_LINEAR)
			
//				SET_ENTITY_COORDS(objElev[1], GET_ENTITY_COORDS(objElev[1]) + <<0.0, 0.0, 15.0>>)
			
				//stop elevator door sound and start moving the entire elevator
				ODDJOB_STOP_SOUND(iSoundIdElevator)
//				MOVE_ELEVATOR(objElev[1], objElevDoorControl[2], objElevDoorControl[3], 0.25, FALSE, TRUE, TRUE)
				
//				RESTORE_PLAYER_POSITION()
				//attach cam to outside of elevator

				iStageNum++
//			ELSE
//				//close the elevator door
//				IF TIMERA() >= 1000
//				AND TIMERA() <= 2000
////					MOVE_ELEVATOR_DOOR(objElevDoorControl[2], 0.15, TRUE)
//				ENDIF
			ENDIF
			
			vElevPos = GET_ENTITY_COORDS(objElev[1])
			fElevatorDistPerFrame = GET_ELEVATOR_TARGET_MOVE_SPEED_START(vElevPos.z, vMyElevPos[1].z + 7.5, vMyElevPos[1].z + 2.763, 0.1, 0.2)
			MOVE_ELEVATOR(objElev[1], NULL, NULL, fElevatorDistPerFrame, FALSE, TRUE, TRUE)
		BREAK
		
		//shot looking at building exterior
		CASE 4
		
			IF NOT bDoneFlash
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA() 
					IF TIMERA() >= 12550
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						
						PRINTLN("bDoneFlash = TRUE")
						bDoneFlash = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			SET_SRL_TIME( TO_FLOAT(TIMERA()) )
			
			IF TIMERA() >= 12850
			
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
//				DO_SCREEN_FADE_OUT(500)
				iStageNum++
			ELSE
//				IF TIMERA() >= 500
//					IF NOT IS_ENTITY_DEAD(piTarget)
//						IF IS_ENTITY_PLAYING_ANIM(piTarget, "oddjobs@assassinate@construction@", "CS_GetInLift")
//							STOP_ANIM_TASK(piTarget, "oddjobs@assassinate@construction@", "CS_GetInLift", SLOW_BLEND_OUT)
//						ENDIF
//					ENDIF
//				ENDIF
			ENDIF
			
			vElevPos = GET_ENTITY_COORDS(objElev[1])
			fElevatorDistPerFrame = GET_ELEVATOR_TARGET_MOVE_SPEED_START(vElevPos.z, vMyElevPos[1].z + 7.5, vMyElevPos[1].z + 2.763, 0.1, 0.25)
			MOVE_ELEVATOR(objElev[1], NULL, NULL, fElevatorDistPerFrame, FALSE, TRUE, TRUE)
		BREAK
		
		//shut down cutscene, teleport player back to ground level, and return to gameplay
		CASE 5
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
				CLEAR_PRINTS()
				SET_SRL_TIME( TO_FLOAT(TIMERA()) )
				
			ELSE
				ODDJOB_EXIT_CUTSCENE()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				//remove the cutscene peds_
				REMOVE_PED_FOR_DIALOGUE(siteConv, 3)
				REMOVE_PED_FOR_DIALOGUE(siteConv, 5)
				DELETE_PED(piTarget)
				DELETE_PED(piCutscenePed)
				
				//stop elevator sounds
				ODDJOB_STOP_SOUND(iSoundIdElevator)
				
				//fade back in and advance to next stage
				CUTSCENE_SKIP_FADE_IN()

				//davance to next stage
				INITIALIZE_NEXT_STAGE()
				
//				IF STREAMVOL_IS_VALID(svPanicCutscene)
//					STREAMVOL_DELETE(svPanicCutscene)
//					PRINTLN("REMOVING - svPanicCutscene - 01")
//				ENDIF
	
				END_SRL()
				
				IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_CUTSCENE")
					STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_CUTSCENE")
					PRINTLN("ASSASSINATION_CONSTRUCT_CUTSCENE STOPPED!")
				ENDIF
								
				MainStage = STAGE_ENTER_SITE
				PRINTLN("Cutscene Finished - MainStage = STAGE_ENTER_SITE")
			ENDIF
			
			vElevPos = GET_ENTITY_COORDS(objElev[1])
			fElevatorDistPerFrame = GET_ELEVATOR_TARGET_MOVE_SPEED_START(vElevPos.z, vMyElevPos[1].z + 7.5, vMyElevPos[1].z + 2.763, 0.1, 0.25)
			MOVE_ELEVATOR(objElev[1], NULL, NULL, 0.25, FALSE, TRUE, TRUE)
		BREAK
	ENDSWITCH
	
	
	//see if player is trying to skip the cutscene and handle accordingly (wait until stage 2 since we teleport the player in stage 1)
	IF iStageNum >= 2
		IF NOT bCutsceneSkipped
			IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
				
				IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_CUTSCENE")
					STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_CUTSCENE")
					PRINTLN("ASSASSINATION_CONSTRUCT_CUTSCENE STOPPED!")
				ENDIF
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
//				RESTORE_PLAYER_POSITION()
				iStageNum = 5
				bCutsceneSkipped = TRUE
				ODDJOB_STOP_SOUND(iSoundIdElevator)
				
				END_SRL()
//				IF STREAMVOL_IS_VALID(svPanicCutscene)
//					STREAMVOL_DELETE(svPanicCutscene)
//					PRINTLN("REMOVING - svPanicCutscene - 02")
//				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYERS_LAST_VEHICLE_IN_BOTTOM_ELEVATOR()
	VEHICLE_INDEX vehLastPlayer
	vehLastPlayer = GET_PLAYERS_LAST_VEHICLE()
	
	IF DOES_ENTITY_EXIST(vehLastPlayer)
		IF IS_ENTITY_IN_ANGLED_AREA(vehLastPlayer, <<-185.042389,-1018.321350,29.051546>>, <<-183.152817,-1013.211792,31.051546>>, 2.000000)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC TASK_CLOSEST_LIVING_GUARDS_TO_FLEE_PLAYER(FLOAT fDistanceProbe = 10.0)
	VECTOR vTemp
	INT tempInt
	FLOAT tempDist
	
	vTemp = GET_ENTITY_COORDS(Player)

	IF MainStage < STAGE_ELEVATOR_CUTSCENE		//check guys in wave 1
		FOR tempInt = 0 TO NUM_WAVE_1_WORKERS - 1
			IF NOT IS_PED_INJURED(wave1[tempInt].ped)
				tempDist = VDIST2(vTemp, GET_ENTITY_COORDS(wave1[tempInt].ped))
				IF tempDist < fDistanceProbe * fDistanceProbe
					TASK_SMART_FLEE_PED(wave1[tempInt].ped, Player, 30, -1)
				ENDIF
			ENDIF
		ENDFOR
		
		tempInt = 0
		
		FOR tempInt = 0 TO NUM_WAVE_2_WORKERS - 1
			IF NOT IS_PED_INJURED(wave2[tempInt].ped)
				tempDist = VDIST2(vTemp, GET_ENTITY_COORDS(wave2[tempInt].ped))
				IF tempDist < fDistanceProbe * fDistanceProbe
					TASK_SMART_FLEE_PED(wave2[tempInt].ped, Player, 30, -1)
				ENDIF
			ENDIF
		ENDFOR
	ELIF MainStage < STAGE_SECOND_ELEVATOR_CUTSCENE		//check guys in wave 3
		FOR tempInt = 0 TO NUM_WAVE_3_WORKERS - 1
			IF NOT IS_PED_INJURED(wave3[tempInt].ped)
				tempDist = VDIST2(vTemp, GET_ENTITY_COORDS(wave3[tempInt].ped))
				IF tempDist < fDistanceProbe * fDistanceProbe
					TASK_SMART_FLEE_PED(wave3[tempInt].ped, Player, 30, -1)
				ENDIF
			ENDIF
		ENDFOR
		
		tempInt = 0
		
		FOR tempInt = 0 TO NUM_WAVE_4_WORKERS - 1
			IF NOT IS_PED_INJURED(wave4[tempInt].ped)
				tempDist = VDIST2(vTemp, GET_ENTITY_COORDS(wave4[tempInt].ped))
				IF tempDist < fDistanceProbe * fDistanceProbe
					TASK_SMART_FLEE_PED(wave4[tempInt].ped, Player, 30, -1)
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		FOR tempInt = 0 TO NUM_WAVE_5_WORKERS - 1		//check guys in wave 5
			IF NOT IS_PED_INJURED(wave5[tempInt].ped)
				tempDist = VDIST2(vTemp, GET_ENTITY_COORDS(wave5[tempInt].ped))
				IF tempDist < fDistanceProbe * fDistanceProbe
					TASK_SMART_FLEE_PED(wave5[tempInt].ped, Player, 30, -1)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

/// PURPOSE:
///    Unblip the site blip and blip the elevator. Trigger elevator cutscene when near.
PROC DO_STAGE_ENTER_SITE()
	
	SWITCH iStageNum
		CASE 0
			//set mission checkpoint
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "assassin_construction_enter_site")
			
			START_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_SHOOTOUT_START")
			
			TRIGGER_MUSIC_EVENT("ASS5_START")
			PRINTLN("TRIGGERING MUSIC - ASS5_START")
			
//			//hide existing elevator since we will need to create our own with doors that will be scripted to move
//			CREATE_MODEL_HIDE(vElev1Bottom, 5, PROP_CONSLIFT_LIFT, TRUE)

			IF NOT DOES_ENTITY_EXIST(objElev[0])
				CREATE_GROUND_ELEVATOR()
			ENDIF
				
			//cleanup existing blip, create new blip inside site and tell player to enter the site
			CLEANUP_DEST_BLIPS()
			
//			IF NOT bPanicking	//to fix Bug 544290
				PRINT_NOW("ASS_CS_ROOF", DEFAULT_GOD_TEXT_TIME, 1)
				PRINTLN("PRINTING OBJECTIVE TO GET TO THE ROOFTOP")
				
				IF NOT DOES_BLIP_EXIST(blipRooftop)
					blipRooftop = ADD_BLIP_FOR_COORD(<< -184.5990, -1015.8192, 254.2567 >>)
//					SET_BLIP_COLOUR(blipRooftop, BLIP_COLOUR_RED)
					SET_BLIP_ROUTE(blipRooftop, FALSE)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipRooftop, "ASS_CS_TARGET")
					PRINTLN("ADDING BLIP - blipRooftop")
				ENDIF
//			ENDIF
			iStageNum++
		BREAK

		// wait for player to activate the elevator
		CASE 1
			IF CAN_PLAYER_ACTIVATE_ELEVATOR(vElev1Bottom)
			AND NOT IS_PLAYERS_LAST_VEHICLE_IN_BOTTOM_ELEVATOR()
				PRINT_ELEVATOR_ACTIVATION_HELP()
				
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ASS_CS_ELEHELP")
//						REMOVE_BLIP(bElevatorBlip)
						
						viPlayerCar = GET_PLAYERS_LAST_VEHICLE()
						IF viPlayerCar != NULL
							IF IS_VEHICLE_DRIVEABLE(viPlayerCar)
								SET_ENTITY_AS_MISSION_ENTITY(viPlayerCar)
							ENDIF
						ENDIF
						
						TRIGGER_MUSIC_EVENT("ASS5_LIFT")
						PRINTLN("TRIGGERING MUSIC - ASS5_LIFT")
												
//						TASK_CLOSEST_LIVING_GUARDS_TO_FLEE_PLAYER()
						
						INITIALIZE_NEXT_STAGE()	
						MainStage = STAGE_ELEVATOR_CUTSCENE
						PRINTLN("MainStage = STAGE_ELEVATOR_CUTSCENE")
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ASS_CS_ELEHELP")
					CLEAR_HELP()
				ENDIF
				
				IF bAdvanceOnPlayer = FALSE
					IF ( GET_NUM_ENEMIES_LEFT_IN_WAVE(wave1, NUM_WAVE_1_WORKERS) + GET_NUM_ENEMIES_LEFT_IN_WAVE(wave2, NUM_WAVE_2_WORKERS) ) = 1
						TASK_REMAINING_PEDS_IN_WAVE_TO_ATTACK_PLAYER(wave1, NUM_WAVE_1_WORKERS)
						TASK_REMAINING_PEDS_IN_WAVE_TO_ATTACK_PLAYER(wave2, NUM_WAVE_2_WORKERS)
						PRINTLN("TASKING THE LAST GUY TO CHARGE THE PLAYER")
						bAdvanceOnPlayer = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	//controls behavior of guys in the vehicle on the ground level (when to drive, exit the vehicle and attack)
	HANDLE_WAVE_2_ENEMY_ASSAULT()
	
	IF bPanicking
	AND NOT bWave1Attacking
		WAVE_1_WORKER_ASSAULT()
		bWave1Attacking = TRUE
		PRINTLN("bWave1Attacking = TRUE")
	ENDIF
ENDPROC


FUNC BOOL HANDLE_PLAYER_PRESSING_ELEVATOR_BUTTON(OBJECT_INDEX elevatorObj, OBJECT_INDEX elevatorButtonObj, FLOAT fPlayerFaceButtonHeading)

	SWITCH iElevButtonStage
		CASE 0
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(elevatorObj, vButtonPressAnimOffset), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, fPlayerFaceButtonHeading) 
			iElevButtonStage ++
		BREAK
		CASE 1
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
//			IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(elevatorObj, vButtonPressAnimOffset)) < 0.1
//				TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), elevatorButtonObj)
				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtElevatorWeapon)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				elevatorButtonObj = elevatorButtonObj
				iElevButtonStage ++
			ENDIF
		BREAK
		CASE 2
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
				iElevSyncScene = CREATE_SYNCHRONIZED_SCENE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(elevatorObj, vButtonPressAnimOffset), GET_ENTITY_ROTATION(elevatorObj))
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iElevSyncScene, "oddjobs@assassinate@construction@", "ig_1_button", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				iElevButtonStage ++
			ENDIF
		BREAK
		CASE 3
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iElevSyncScene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(iElevSyncScene) > 0.9
				iElevButtonStage ++
			ENDIF
		BREAK
		CASE 4
			VECTOR vAttach
			vAttach = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(elevatorObj, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			vAttach.z = -1.333402
			ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), elevatorObj, 0, vAttach, <<0,0,GET_ENTITY_HEADING(PLAYER_PED_ID())-GET_ENTITY_HEADING(elevatorObj)>>, TRUE, TRUE, FALSE)		
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			RETURN TRUE
		BREAK
	ENDSWITCH	
	
	RETURN FALSE
ENDFUNC


PROC DO_STAGE_ELEVATOR_CUTSCENE()
	SEQUENCE_INDEX tempSeq
	tempSeq = tempSeq
	VECTOR tempVec 
	FLOAT fElevatorDistPerFrame
	
	IF DOES_ENTITY_EXIST(objElev[0])
		tempVec = GET_ENTITY_COORDS(objElev[0])
	ENDIF
	
	SWITCH iStageNum
		//set scripts safe for the cutscene
		CASE 0
			ODDJOB_ENTER_CUTSCENE(SPC_LEAVE_CAMERA_CONTROL_ON)		// Setting this control flag to allow us to control the 3rd person camerawhile the elevator is active
			GET_ASSASSIN_CUTSCENE_START_TIME()
			
			ODDJOB_STOP_SOUND(iSoundIdElevator)
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			//set this value to the appropriate level for enemy speech on mid level of building to be used with the UPDATE_COMBAT_DIALOGUE_ENEMY function
			IF iEnemySpeechStage < 4
				PRINTLN("SETTING iEnemySpeechStage TO 4")
				iEnemySpeechStage = 4
			ENDIF
			
//			FREEZE_ENTITY_POSITION(Player, TRUE)
			SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
			CLEAR_AREA(vElev1Bottom, 10, TRUE)
						
			SETTIMERA(0)
			iStageNum ++
		BREAK
		
		CASE 1
			IF HANDLE_PLAYER_PRESSING_ELEVATOR_BUTTON(objElev[0], objElevButton[0], 264.1952)
//				TASK_LOOK_AT_COORD(PLAYER_PED_ID(), << -182.86542, -1012.34216, 114.13878 >>, -1)
				iStageNum ++
			ENDIF
		BREAK
		
		//show elevator going up
		CASE 2
			IF TIMERA() >= 0
				//close the elvator door behind the player
				ODDJOB_STOP_SOUND(iSoundIdElevator)
				
//				//cam attached to elevator
//				INIT_FIRST_PERSON_CAMERA(camFirstPerson, << -188.2900, -1022.0970, 31.3926 >>, vElev1CamRot, 45, 90, 90)
//				ATTACH_CAM_TO_ENTITY(camFirstPerson.theCam, objElev[0], vAttachmentOffset)

				//create next wave of enemies on mid-level
				CREATE_WORKER_WAVE(3)
				
				FREEZE_ENTITY_POSITION(Player, FALSE)
				SETTIMERA(0)
				iStageNum ++
//			ELSE
//				//close the elevator door - monitor the height so that we know when to stop it
//				VECTOR tempVec2
//				IF DOES_ENTITY_EXIST(objElevDoorControl[0])
//					tempVec2 = GET_ENTITY_COORDS(objElevDoorControl[0])
//				ENDIF
//				IF tempVec2.z >= 30.45 //do this to make sure the door doesn't descend too much (Bug 403374)
//					IF TIMERA() > 500
//					AND TIMERA() <= 1600
////						MOVE_ELEVATOR_DOOR(objElevDoorControl[0], 0.15, TRUE)
//					ENDIF
//				ENDIF
			ENDIF
		BREAK
		
		//go back to gameplay after reaching mid level
		CASE 3
			IF DOES_ENTITY_EXIST(objElev[0])
				IF tempVec.z >= 115.4
				OR bCutsceneSkipped
					IF IS_SCREEN_FADING_OUT()
						IF NOT IS_SCREEN_FADING_IN()
							CLEANUP_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
							CLEANUP_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
							DO_SCREEN_FADE_IN(1000)
						ENDIF
					ELSE
//						SET_ENTITY_COORDS(objElev[0], << -182.170, -1016.090, 115.46 >>)
//						SET_ENTITY_COORDS(objElevDoorControl[0], << -184.97, -1018.08, 114.551 >>)
						IF NOT IS_PED_IN_ANY_VEHICLE(Player)
							DETACH_ENTITY(Player, FALSE, FALSE)
						ELSE
							DETACH_ENTITY(GET_VEHICLE_PED_IS_IN(Player), FALSE, FALSE)
						ENDIF
						
//						SET_ENTITY_HEADING(Player, 340.3911)
//						CLEAR_FIRST_PERSON_CAMERA(camFirstPerson)
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						
						SETTIMERA(0)
						
						ODDJOB_STOP_SOUND(iSoundIdElevator)
						iStageNum ++
					ENDIF
				ELSE
//					//gradually slow down the elevator as it approaches its destination
					fElevatorDistPerFrame = GET_ELEVATOR_TARGET_MOVE_SPEED_STOP(tempVec.z, 105.0, 115.4, 111.0, 0.10, 0.25)
					MOVE_ELEVATOR(objElev[0], NULL, NULL, fElevatorDistPerFrame)
					
					//have the enemy spot franklin as the elevator starts slowing down and trigger panic
					IF tempVec.z >= 113.8
						IF NOT bPanicking
							PLAY_ENEMY_SPOTTED_FRANKLIN_LINE(wave3[0].ped)
							bPanicking = TRUE
						ENDIF
						
						WAVE_3_WORKER_ASSAULT()
					ENDIF
					
					RELOAD_PLAYERS_WEAPON()
					
//					UPDATE_FIRST_PERSON_CAMERA(camFirstPerson)
				ENDIF
			ENDIF
		BREAK
		
		
		//tell the enemies to attack the player return control to the player
		CASE 4
//			IF TIMERA() >= 1650
				//stop elevator and camera sounds and play sound of elevator door opening in front of player
				ODDJOB_STOP_SOUND(iSoundIdElevator)
				
				//cleanup ground level enemies and create next wave on mid-level of the building
				CLEANUP_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
				CLEANUP_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
				CLEANUP_WARNING_GUYS()
				WAVE_3_WORKER_ASSAULT()
				
				ODDJOB_EXIT_CUTSCENE()
				
//				IF bCutsceneSkipped
					CUTSCENE_SKIP_FADE_IN()
//				ENDIF
				INITIALIZE_NEXT_STAGE()
				MainStage = STAGE_GET_TO_SECOND_ELEVATOR
				PRINTLN("MainStage = STAGE_GET_TO_SECOND_ELEVATOR")
//			ELSE
//				IF TIMERA() > 500
//				AND TIMERA() <= 1600
////					MOVE_ELEVATOR_DOOR(objElevDoorControl[1], 0.15, FALSE)
//				ENDIF
//			ENDIF
		BREAK		
	ENDSWITCH
	
	//see if the player is trying to skip the cutscene while ascending in the elevator
	IF iStageNum >= 3
		IF NOT bCutsceneFading
			IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
				bCutsceneFading = TRUE
				PRINTLN("bCutsceneFading = TRUE")
			ENDIF
		ELSE
			IF IS_SCREEN_FADED_OUT()
				KILL_ANY_CONVERSATION()
				
				//set this value to the appropriate level for enemy speech on mid level of building to be used with the UPDATE_COMBAT_DIALOGUE_ENEMY function
				IF iEnemySpeechStage < 4
					PRINTLN("SETTING iEnemySpeechStage TO 4 VIA CUTSCENE SKIP")
					iEnemySpeechStage = 4
				ENDIF
				
				SET_GROUND_ELEVATOR_TO_TOP()
				
//				CLEAR_FIRST_PERSON_CAMERA(camFirstPerson)

				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				bCutsceneSkipped = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Handle Enemy response on second level.  Wait for player to get to the base of the crane to trigger the crane cutscene.
PROC DO_STAGE_GET_TO_SECOND_ELEVATOR()
	PED_INDEX tempPed
	INT iNumLeft
//	INT iTemp
	
	IF NOT bChangeAccuracy01
		IF IS_TIMER_STARTED(changeAccuracyTimer01)
			IF GET_TIMER_IN_SECONDS(changeAccuracyTimer01) >= CHANGE_ACCURACY_TIME
				ALTER_WAVE_ACCURACY(3, 20)
				ALTER_WAVE_ACCURACY(4, 20)
				PRINTLN("CHANGE_ACCURACY_TIME IS UP, ALTERING ACCURACY")
				bChangeAccuracy01 = TRUE
			ENDIF
		ELSE
//			PRINTLN("TIMER HAS NOT STARTED - changeAccuracyTimer01")
		ENDIF
	ENDIF
	
	SWITCH iStageNum	
		CASE 0
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "assassin_construction_get_to_second_elevator")
			IF NOT bPanicking
				bPanicking = TRUE
				PRINTLN("set panic if not already set in DO_STAGE_GET_TO_SECOND_ELEVATOR")
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(blipRooftop)
				blipRooftop = ADD_BLIP_FOR_COORD(<< -154.7990, -941.62, 269.10 >>)
//				SET_BLIP_COLOUR(blipRooftop, BLIP_COLOUR_RED)
				SET_BLIP_ROUTE(blipRooftop, FALSE)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipRooftop, "ASS_CS_TARGET")
				PRINTLN("ADDING BLIP - blipRooftop in STAGE_GET_TO_SECOND_ELEVATOR")
			ELSE
				SET_BLIP_COORDS(blipRooftop, << -154.7990, -941.62, 269.10 >>)
			ENDIF
			
			//set this value to the appropriate level for enemy speech on mid level of building to be used with the UPDATE_COMBAT_DIALOGUE_ENEMY function
			IF iEnemySpeechStage < 4
				PRINTLN("SETTING iEnemySpeechStage TO 4 VIA DO_STAGE_GET_TO_SECOND_ELEVATOR")
				iEnemySpeechStage = 4
			ENDIF
			
			TRIGGER_MUSIC_EVENT("ASS5_TOP")
			PRINTLN("TRIGGERING MUSIC - ASS5_TOP")
			
			//cleanup any previous enemies from ground level waves
			CLEANUP_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
			CLEANUP_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
			
			RENDER_SCRIPT_CAMS(FALSE, TRUE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
						
//			PRINT_NOW("ASS_CS_ELEVAT2", DEFAULT_GOD_TEXT_TIME, 1)
			PRINT_NOW("ASS_CS_EHELP1", DEFAULT_GOD_TEXT_TIME, 1)
//			bElevatorBlip = CREATE_BLIP_FOR_COORD(vElev2Bottom, TRUE)
			
			SETTIMERA(0)
			iStageNum++
		BREAK
		
		CASE 1
			IF DOES_ENTITY_EXIST(objElev[1])
				CREATE_SECOND_ELEVATOR(FALSE)
				iStageNum++
			ELSE
				CREATE_MODEL_HIDE(vElev2Bottom, 5, PROP_CONSLIFT_LIFT, TRUE)
				CREATE_SECOND_ELEVATOR(FALSE)
				iStageNum++
			ENDIF
		BREAK
		
		CASE 2
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(Player, << -148.6073, -952.5458, 113.1339 >>) <= 5
				CREATE_WORKER_WAVE(4)
				CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScs_LIFT", CONV_PRIORITY_VERY_HIGH)
				iStageNum++
			ENDIF
		BREAK
		
		CASE 3
			IF bElevatorAttack
				IF CAN_PLAYER_ACTIVATE_ELEVATOR(vElev2Bottom)
					PRINT_ELEVATOR_ACTIVATION_HELP()
					
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ASS_CS_ELEHELP")
//							FOR iTemp = 0 TO NUM_WAVE_4_WORKERS - 1
//								IF DOES_ENTITY_EXIST(wave4[iTemp].ped)
//									IF IS_ENTITY_IN_ANGLED_AREA(wave4[iTemp].ped, <<-160.578, -939.448, 114.418>>, <<-155.410, -941.329, 114.418>>, 3, FALSE, FALSE)
//										iNumDeadPedsInElevator ++
//									ENDIF
//								ENDIF
//							ENDFOR
//							REMOVE_BLIP(bElevatorBlip)
							
							ODDJOB_STOP_SOUND(iSoundIdElevator)
							
//							TASK_CLOSEST_LIVING_GUARDS_TO_FLEE_PLAYER()
							
							INITIALIZE_NEXT_STAGE()
							MainStage = STAGE_SECOND_ELEVATOR_CUTSCENE
							PRINTLN("MainStage = STAGE_SECOND_ELEVATOR_CUTSCENE (Player took elevator)")
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ASS_CS_ELEHELP")
						CLEAR_HELP()
					ELSE
						IF NOT bElevatorMsgPrinted
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								PRINT_NOW("ASS_CS_ELEVAT3", DEFAULT_GOD_TEXT_TIME, 1)
								bElevatorMsgPrinted = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SEND_ENEMIES_DOWN_IN_ELEVATOR()
			ENDIF
		BREAK
	ENDSWITCH
	
	
	
	//task enemies to advance on player if there are only a few enemies left in the wave
	IF bAdvanceOnPlayer = FALSE
		iNumLeft = GET_NUM_ENEMIES_LEFT_IN_WAVE(wave3, NUM_WAVE_3_WORKERS)
		IF iNumLeft = 3
			TASK_REMAINING_PEDS_IN_WAVE_TO_ATTACK_PLAYER(wave2, NUM_WAVE_2_WORKERS)
			PRINTLN("TASKING THE LAST GUY TO CHARGE THE PLAYER")
			bAdvanceOnPlayer = TRUE
		ELIF iNumLeft = 5
			IF NOT bLeaveDefensiveArea
				IF GET_CLOSEST_LIVING_ENEMY(50) <> NULL
					tempPed = GET_CLOSEST_LIVING_ENEMY(50)
					REMOVE_PED_DEFENSIVE_AREA(tempPed)
					SET_PED_COMBAT_MOVEMENT(tempPed, CM_WILLADVANCE)
					TASK_CLEAR_DEFENSIVE_AREA(tempPed)
					TASK_COMBAT_PED(tempPed, Player)
					bLeaveDefensiveArea = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC DELETE_WAVE_4_PEDS_IN_ELEVATOR_IF_DEAD()
	INT idx
	VECTOR wave4WorkerPos
	
	REPEAT NUM_WAVE_4_WORKERS idx
		IF IS_PED_INJURED(wave4[idx].ped)
			wave4WorkerPos = GET_ENTITY_COORDS(wave4[idx].ped, FALSE)
			IF GET_DISTANCE_BETWEEN_COORDS(wave4WorkerPos, vElev2Bottom) < 3.25
				DELETE_PED(wave4[idx].ped)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Play the 2nd elevator cutscene of the player traveling to the rooftop level
PROC DO_STAGE_SECOND_ELEVATOR_CUTSCENE()
	VECTOR tempVec
//	VECTOR tempDoorPos
	FLOAT fElevatorDistPerFrame
	
	
	SWITCH iStageNum
		//take away player control, wait a second before starting the cutscene
		CASE 0
			
			IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_SHOOTOUT_START")
				STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_SHOOTOUT_START")
			ENDIF
			
			START_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_SHOOTOUT_ROOFTOP")
			
			ODDJOB_STOP_SOUND(iSoundIdElevator)
			ODDJOB_ENTER_CUTSCENE(SPC_LEAVE_CAMERA_CONTROL_ON)
			GET_ASSASSIN_CUTSCENE_START_TIME()
			
			CLEAR_PRINTS()
			CLEAR_HELP()
//			FREEZE_ENTITY_POSITION(Player, TRUE)
			SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
			CLEAR_AREA(vElev2Bottom, 10, TRUE)
			
			CLEAR_HELP()
			
			SETTIMERA(0)
			iStageNum++
		BREAK
		
		CASE 1
			IF HANDLE_PLAYER_PRESSING_ELEVATOR_BUTTON(objElev[1], objElevButton[1], 152.2146)
				DELETE_WAVE_4_PEDS_IN_ELEVATOR_IF_DEAD()
//				TASK_LOOK_AT_COORD(PLAYER_PED_ID(), << -154.71185, -941.35815, 269.13300 >>, -1)
				iStageNum ++
			ENDIF
		BREAK
		
		//show elevator
		CASE 2
			IF TIMERA() >= 0
				ODDJOB_STOP_SOUND(iSoundIdElevator)
				
//				//elevator cam
//				INIT_FIRST_PERSON_CAMERA(camFirstPerson, << -152.0316, -942.8273, 115.6051 >>, vElev2CamRot, 45, 100, 90)
//				ATTACH_CAM_TO_ENTITY(camFirstPerson.theCam, objElev[1], vAttachmentOffset)

				//cleanup peds on mid-level of the building
				CLEANUP_WORKER_WAVE(wave3, NUM_WAVE_3_WORKERS)
				CLEANUP_WORKER_WAVE(wave4, NUM_WAVE_4_WORKERS)
				CLEANUP_PED_BLIPS(FALSE)
				
				FREEZE_ENTITY_POSITION(Player, FALSE)
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				iStageNum ++
//			ELSE
//				//close the door behind the player
////				tempDoorPos = GET_ENTITY_COORDS(objElevDoorControl[2])
//				IF TIMERA() > 500
//				AND TIMERA() <= 1600
//				AND tempDoorPos.z > 114.65
////					MOVE_ELEVATOR_DOOR(objElevDoorControl[2], 0.15, TRUE)
//				ENDIF
			ENDIF
		BREAK
		
		//return to game camera after reaching the top of the building
		CASE 3
			IF DOES_ENTITY_EXIST(objElev[1])
				tempVec = GET_ENTITY_COORDS(objElev[1])
				IF tempVec.z >= 270.46
					IF IS_SCREEN_FADING_OUT()
						IF NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(1000)
						ENDIF
					ELSE
						DETACH_ENTITY(Player, FALSE, FALSE)
												
						SETTIMERA(0)
						
						ODDJOB_STOP_SOUND(iSoundIdElevator)
						iStageNum ++
					ENDIF
				ELSE
					
					IF tempVec.z >= 235.0
						IF NOT DOES_ENTITY_EXIST(viHeli)
							CREATE_HELI_PEDS()
						ENDIF
						IF NOT bRooftopWaveCreated
							CREATE_WORKER_WAVE(5)
						ENDIF
					ENDIF
				
//					//gradually slow down the elevator as it approaches its destination
					fElevatorDistPerFrame = GET_ELEVATOR_TARGET_MOVE_SPEED_STOP(tempVec.z, 260.0, 270.46, 266.0, 0.10, 0.25)
					MOVE_ELEVATOR(objElev[1], NULL, NULL, fElevatorDistPerFrame)
					
					RELOAD_PLAYERS_WEAPON()
//					UPDATE_FIRST_PERSON_CAMERA(camFirstPerson)
				ENDIF
			ENDIF
		BREAK
		
		//
		CASE 4
			ODDJOB_STOP_SOUND(iSoundIdElevator)
			
			SETTIMERA(0)
		
			iStageNum ++
		BREAK
		
		CASE 5		
			ODDJOB_EXIT_CUTSCENE()
			
			CUTSCENE_SKIP_FADE_IN()
			
			INITIALIZE_NEXT_STAGE()	
			MainStage = STAGE_ROOFTOP_BATTLE
			PRINTLN("MainStage = STAGE_ROOFTOP_BATTLE - Rooftop Cutscene finished")
		BREAK
	ENDSWITCH
	
	
	
	//handle cutscene camera skip
	IF iStageNum >= 3
		IF NOT bCutsceneFading
			IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
				bCutsceneFading = TRUE
				PRINTLN("bCutsceneFading = TRUE")
			ENDIF
		ELSE
			IF IS_SCREEN_FADED_OUT()
				IF NOT bRooftopWaveCreated
					CREATE_WORKER_WAVE(5)
				ENDIF
				
				IF NOT bHeliPedsCreated
					CREATE_HELI_PEDS()
				ENDIF
				
				IF bRooftopWaveCreated AND bHeliPedsCreated
					KILL_ANY_CONVERSATION()
					SET_SECOND_ELEVATOR_TO_TOP()
				
					SET_ENTITY_HEADING(Player, 243.9563)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					ODDJOB_EXIT_CUTSCENE()
					CUTSCENE_SKIP_FADE_IN()
					INITIALIZE_NEXT_STAGE()
					
					MainStage = STAGE_ROOFTOP_BATTLE
					PRINTLN("MainStage = STAGE_ROOFTOP_BATTLE - Rooftop Cutscene skipped")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Handle enemy's behavior on crane
PROC DO_STAGE_ROOFTOP_BATTLE()
	INT iPlacementFlags
	PICKUP_INDEX  weapPickup
	
	
	SWITCH iStageNum
		CASE 0
			IF bTriggerRooftop
				bNeedToCheckOnRoof = TRUE
				PRINTLN("bNeedToCheckOnRoof = TRUE")
			ELSE
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "assassin_construction_rooftop_battle")
				PRINTLN("SETTING MISSION STAGE TO 3")
			ENDIF
			
			TRIGGER_MUSIC_EVENT("ASS5_ROOF")
			PRINTLN("TRIGGERING MUSIC - ASS5_ROOF")
			
			START_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_HELICOPTER_SCENE")
			
			IF DOES_BLIP_EXIST(blipRooftop)
				REMOVE_BLIP(blipRooftop)
				PRINTLN("REMOVING BLIP - blipRooftop")
			ENDIF
			
			END_SRL()
			
//			IF STREAMVOL_IS_VALID(svPanicCutscene)
//				STREAMVOL_DELETE(svPanicCutscene)
//				PRINTLN("REMOVING - svPanicCutscene - 04")
//			ENDIF

			SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
			
			//create rooftop enemies and start the helicopter vehicle playback
			CREATE_WORKER_WAVE(5)
			CREATE_HELI_PEDS()
			
			IF NOT IS_ENTITY_DEAD(piTarget) AND NOT IS_ENTITY_DEAD(viHeli)
			AND NOT IS_PED_IN_VEHICLE(piTarget, viHeli)
				TASK_ENTER_VEHICLE(piTarget, viHeli, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)
			ENDIF
			
			IF bHeliModelLoaded
//				//play vehicle recording on heli
//				IF IS_VEHICLE_DRIVEABLE(viHeli)
//					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viHeli)
//						START_PLAYBACK_RECORDED_VEHICLE(viHeli, 102, "OJAScs")
//						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(viHeli, 3000)
//						SET_PLAYBACK_SPEED(viHeli, 1)
//					ELSE
//						SET_PLAYBACK_SPEED(viHeli, 1)
//					ENDIF
//				ENDIF
				
//				//create closed elevator doors for other elevator located at the top level
//				IF DOES_ENTITY_EXIST(objElevDoorClosed[0])
//					SET_ENTITY_COORDS(objElevDoorClosed[0], << -162.010, -943.190, 269.520 >>)
//					SET_ENTITY_ROTATION(objElevDoorClosed[0], << 0, 0, 339 >>)
//				ENDIF
//				IF DOES_ENTITY_EXIST(objElevDoorClosed[1])
//					SET_ENTITY_COORDS(objElevDoorClosed[1], << -156.850, -945.060, 269.520 >>)
//					SET_ENTITY_ROTATION(objElevDoorClosed[1], << 0, 0, 339 >>)
//				ENDIF
//				IF DOES_ENTITY_EXIST(objElevDoorClosed[2])
//					DELETE_OBJECT(objElevDoorClosed[2])
//				ENDIF
//				IF DOES_ENTITY_EXIST(objElevDoorClosed[3])
//					DELETE_OBJECT(objElevDoorClosed[3])
//				ENDIF
				
				//create weapon pickup on opposite side of rooftop
				IF NOT DOES_PICKUP_EXIST(weapPickup)
					iPlacementFlags = 0
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
//					weapPickup = CREATE_PICKUP(PICKUP_WEAPON_DLC_ASSAULTMG, << -148.4082, -976.2664, 268.2574 >>, iPlacementFlags, 150)
				ENDIF
				
				PRINTLN("ROOFTOP BATTLE: GOING TO CASE 1")
			   	
				iStageNum++
			ENDIF
		BREAK
		
		CASE 1
			IF IS_SCREEN_FADED_IN()
				IF NOT IS_ENTITY_DEAD(wave6[1].ped)
					TASK_COMBAT_PED(wave6[1].ped, Player)
					PRINTLN("TASKING WAVE 6, GUY 1 TO COMBAT")
				ENDIF
				IF NOT IS_ENTITY_DEAD(wave6[2].ped)
					TASK_COMBAT_PED(wave6[2].ped, Player)
					PRINTLN("TASKING WAVE 6, GUY 2 TO COMBAT")
				ENDIF
				
				//attack! attack!
				WAVE_5_WORKER_ASSAULT()
				
				SETTIMERA(0)
				iStageNum++
			ENDIF
		BREAK
	
		CASE 2
			IF TIMERA() > 4000
				bOkayToAttack = TRUE
				PRINT_NOW("ASS_CS_KILL", DEFAULT_GOD_TEXT_TIME, 1)
				iStageNum++
			ENDIF
		BREAK
		
		CASE 3
			//empty state - waiting for player to kill the target
		BREAK
	ENDSWITCH
	
	CONTROL_ROOFTOP_ACCURACY()
	
	IF bNeedToCheckOnRoof
		CHECK_FOR_PLAYER_ON_ROOF()
	ENDIF
	
	//handles all helictoper combat behavior
	CONTROL_HELICOPTER_BEHAVIOR()
	
	//controls behavior of enemy positioned on the crane
	CONTROL_CRANE_ENEMY_BEHAVIOR()
ENDPROC


/// PURPOSE: Play cutscene camera of the target when killed on the street level
PROC DO_STAGE_KILL_CAM()
//	INT tempInt
	
	SWITCH iStageNum
		CASE 0
			bUpdateGuysInCar = FALSE
			
			TRIGGER_MUSIC_EVENT("ASS5_KILL")
			PRINTLN("TRIGGERING MUSIC - ASS5_KILL")
			
			IF DOES_ENTITY_EXIST(viHeli)
				IF NOT IS_ENTITY_DEAD(viHeli) AND NOT IS_ENTITY_DEAD(GET_PED_IN_VEHICLE_SEAT(viHeli))
					TASK_HELI_MISSION(GET_PED_IN_VEHICLE_SEAT(viHeli), viHeli, NULL, Player, <<0,0,180>>, MISSION_FLEE, 20, 30, -1, 325, 40)
				ELSE
					SET_VEHICLE_AS_NO_LONGER_NEEDED(viHeli)
					PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED - viHeli - 02")
				ENDIF
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0)
									
			SETTIMERA(0)
			iStageNum++
		BREAK
		
		CASE 1
			IF TIMERA() >= 3000	
				CLEANUP_DEST_BLIPS()
				
//				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
//					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
//				ENDIF
				
//				IF bTriggerRooftop
//					killType = KT_BONUS
//					INITIALIZE_NEXT_STAGE()
//					MainStage = STAGE_CLEANUP
//					PRINTLN("bTriggerRooftop: MainStage = STAGE_CLEANUP")
//				ELSE
					INITIALIZE_NEXT_STAGE()
					MainStage = STAGE_FLEE_POLICE
					PRINTLN("MainStage = STAGE_FLEE_POLICE")
//				ENDIF
			ELSE
				HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, siteConv, "OJASAUD", "OJAS_COCOM", CONV_PRIORITY_VERY_HIGH)
			ENDIF
		BREAK
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Play the 2nd elevator cutscene of the player traveling to the rooftop level
PROC UPDATE_FINAL_ELEVATOR_CUTSCENE()
	VECTOR tempVec
	
	SWITCH iFinalElevatorCutsceneStage
		//take away player control, wait a second before starting the cutscene
		CASE 0
			ODDJOB_STOP_SOUND(iSoundIdElevator)
			ODDJOB_ENTER_CUTSCENE(SPC_LEAVE_CAMERA_CONTROL_ON)
			GET_ASSASSIN_CUTSCENE_START_TIME()
			
			REMOVE_WORKER_WAVE(wave1, NUM_WAVE_1_WORKERS)
			REMOVE_WORKER_WAVE(wave2, NUM_WAVE_2_WORKERS)
			REMOVE_WORKER_WAVE(wave3, NUM_WAVE_3_WORKERS)
			REMOVE_WORKER_WAVE(wave4, NUM_WAVE_4_WORKERS)
			REMOVE_WORKER_WAVE(wave5, NUM_WAVE_5_WORKERS)
			REMOVE_WORKER_WAVE(wave6, NUM_WAVE_6_WORKERS)
//			REMOVE_DEAD_ELEVATOR_GUYS()
			
			CLEANUP_WARNING_GUYS()
			
			CLEAR_PRINTS()
			CLEAR_HELP()
//			FREEZE_ENTITY_POSITION(Player, TRUE)
			SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
			CLEAR_AREA(vElev2Top, 10, TRUE)
			
			CLEAR_HELP()
			
			SETTIMERA(0)
			iFinalElevatorCutsceneStage ++
		BREAK
		
		CASE 1
			IF HANDLE_PLAYER_PRESSING_ELEVATOR_BUTTON(objElev[1], objElevButton[1], 152.2146)
//				TASK_LOOK_AT_COORD(PLAYER_PED_ID(), << -161.82307, -939.10950, 29.28928 >>, -1)
				iFinalElevatorCutsceneStage ++
			ENDIF
		BREAK
		
		//show elevator
		CASE 2
			IF TIMERA() >= 0
				ODDJOB_STOP_SOUND(iSoundIdElevator)
				
//				//elevator cam
//				INIT_FIRST_PERSON_CAMERA(camFirstPerson, << -152.0316, -942.8273, 115.6051 >>, <<vElev2CamRot.x, vElev2CamRot.y, vElev2CamRot.z >>, 45, 90, 90)
//				ATTACH_CAM_TO_ENTITY(camFirstPerson.theCam, objElev[1], vAttachmentOffset)

				FREEZE_ENTITY_POSITION(Player, FALSE)
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				iFinalElevatorCutsceneStage ++
			ENDIF
		BREAK
		
		//return to game camera after reaching the bottom of the building
		CASE 3
			IF DOES_ENTITY_EXIST(objElev[1])
				tempVec = GET_ENTITY_COORDS(objElev[1])
				IF tempVec.z <= 32
					IF IS_SCREEN_FADING_OUT()
						IF NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(1000)
						ENDIF
					ELSE
						SET_SECOND_ELEVATOR_TO_BOTTOM(TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//						SET_ENTITY_HEADING(Player, 74.1208)
					
						ODDJOB_STOP_SOUND(iSoundIdElevator)
						iFinalElevatorCutsceneStage ++
					ENDIF
				ELSE
					MOVE_ELEVATOR(objElev[1], NULL, NULL, 0.25, TRUE, FALSE)
					
//					RELOAD_PLAYERS_WEAPON()
//					UPDATE_FIRST_PERSON_CAMERA(camFirstPerson)
				ENDIF
			ENDIF
		BREAK
		
		//
		CASE 4
			ODDJOB_STOP_SOUND(iSoundIdElevator)
			SETTIMERA(0)
			
//			CLEAR_FIRST_PERSON_CAMERA(camFirstPerson)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			ODDJOB_EXIT_CUTSCENE()
			
			bPlayerReachedBottom = TRUE
			EXIT
		BREAK
	ENDSWITCH
	
	//handle cutscene camera skip
	IF iFinalElevatorCutsceneStage >= 3
		IF NOT bCutsceneFading
			IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
				bCutsceneFading = TRUE
				PRINTLN("bCutsceneFading = TRUE")
			ENDIF
		ELSE
			IF IS_SCREEN_FADED_OUT()
				KILL_ANY_CONVERSATION()
				SET_SECOND_ELEVATOR_TO_BOTTOM()
				
				SET_ENTITY_HEADING(Player, 74.1208)
//				CLEAR_FIRST_PERSON_CAMERA(camFirstPerson)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				ODDJOB_EXIT_CUTSCENE()
				CUTSCENE_SKIP_FADE_IN()
				
				bPlayerReachedBottom = TRUE
				EXIT
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ENDING_CELL_PHONE_CALL_COMPLETED()
	IF NOT bPlayedCellPhoneCall
		
		ADD_PED_FOR_DIALOGUE(siteConv, 3, NULL, "LESTER")
		
		IF PLAYER_CALL_CHAR_CELLPHONE(siteConv, CHAR_LESTER, "OJASAUD", "OJAS_CONST_C", CONV_MISSION_CALL)
			PRINTLN("BUS - CELL PHONE CALL")
			bPlayedCellPhoneCall = TRUE
		ENDIF
	ENDIF
	
	IF bPlayedCellPhoneCall AND HAS_CELLPHONE_CALL_FINISHED()
	
		PRINTLN("RETURNING TRUE - HAS_ENDING_CELL_PHONE_CALL_COMPLETED")
		RETURN TRUE
	ELSE
		PRINTLN("WAITING FOR CELL PHONE CALL TO FINISH")
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE: Monitor players wanted level and advance to next stage when it becomes cleared.
PROC DO_STAGE_FLEE_POLICE()
	
	HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, siteConv, "OJASAUD", "OJAS_COCOM", CONV_PRIORITY_VERY_HIGH)
	
	//handles printing of messages to leave the rooftop
	IF NOT bPrintLastObjective 
		IF NOT IS_THIS_PRINT_BEING_DISPLAYED("ASS_CS_PARA")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_PLAYER_IN_CONSTRUCTION_AREA()
					PRINT_NOW("ASS_CS_PARA", DEFAULT_GOD_TEXT_TIME, 1)
					IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_SHOOTOUT_ROOFTOP")
						STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_SHOOTOUT_ROOFTOP")
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_HELICOPTER_SCENE")
						STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_HELICOPTER_SCENE")
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_LEAVE_THE_AREA")
						START_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_LEAVE_THE_AREA")
					ENDIF
				ENDIF
				RESTART_TIMER_NOW(tPanicFailSafeTimer)				//timer to use in the next segment
				bPrintLastObjective = TRUE
			ENDIF
		ENDIF
	ELSE
		IF NOT bPrintedReminderMessage
		AND iStageNum = 0
			IF GET_TIMER_IN_SECONDS(tPanicFailSafeTimer) > DEFAULT_GOD_TEXT_TIME / 1000
			AND NOT IS_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP("ASS_CS_REMIND")
				bPrintedReminderMessage = TRUE
			ENDIF
		ENDIF
	ENDIF
		
	HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, siteConv, "OJASAUD", "OJAS_COCOM", CONV_PRIORITY_VERY_HIGH)
		
	SWITCH iStageNum
		CASE 0
			IF GET_PED_PARACHUTE_STATE(Player) != PPS_INVALID
				
				IF NOT bFranklinJumpLinePlayed
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAS_JUMP", CONV_PRIORITY_VERY_HIGH)
						bFranklinJumpLinePlayed = TRUE
					ENDIF
				ENDIF
				
				IF GET_PED_PARACHUTE_STATE(Player) = PPS_DEPLOYING
				OR GET_PED_PARACHUTE_STATE(Player) = PPS_PARACHUTING
				
					CLEAR_HELP(TRUE)
//					PRINT_NOW("ASS_CS_LAND", DEFAULT_GOD_TEXT_TIME, 1)

					TRIGGER_MUSIC_EVENT("ASS5_STOP")
					PRINTLN("TRIGGERING MUSIC - ASS5_STOP")
					
					
					IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_SHOOTOUT_ROOFTOP")
						STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_SHOOTOUT_ROOFTOP")
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_HELICOPTER_SCENE")
						STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_HELICOPTER_SCENE")
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_LEAVE_THE_AREA")
						START_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_LEAVE_THE_AREA")
					ENDIF
					
					
					PRINTLN("USING PARACHUTE")
						
					iStageNum++
				ELSE
					IF GET_PED_PARACHUTE_STATE(Player) = PPS_SKYDIVING
						IF DOES_BLIP_EXIST(blipRooftop)
							REMOVE_BLIP(blipRooftop)
							PRINTLN("REMOVING BLIP - blipRooftop VIA DO_STAGE_FLEE_POLICE")
						ENDIF
						PRINT_HELP("ASS_CS_PARA2")
					ENDIF
				ENDIF
			ELSE
				//support for final elevator cutscene riding down
				IF CAN_PLAYER_ACTIVATE_ELEVATOR(<< -158.3, -940.8, 269.2 >>)
					PRINT_ELEVATOR_ACTIVATION_HELP()
						
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ASS_CS_ELEHELP")
							ODDJOB_STOP_SOUND(iSoundIdElevator)
							
							TRIGGER_MUSIC_EVENT("ASS5_STOP")
							PRINTLN("TRIGGERING MUSIC - ASS5_STOP")
							PRINTLN("USING ELEVATOR")
		
							REMOVE_ALL_SHOCKING_EVENTS(FALSE)
							
							iStageNum++
							bRidingFinalElevator = TRUE
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ASS_CS_ELEHELP")
						CLEAR_HELP()
					ENDIF
				ENDIF
				
				IF NOT IS_PLAYER_IN_CONSTRUCTION_AREA()
				AND NOT IS_PED_RAGDOLL(Player)
					iStageNum = 3		//to fix bug 
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT bRidingFinalElevator
				IF GET_PED_PARACHUTE_STATE(Player) = PPS_LANDING
					SETTIMERA(0)
					
					REMOVE_ALL_SHOCKING_EVENTS(FALSE)
					iStageNum++
				ELSE
					IF NOT HAS_PED_GOT_WEAPON(Player, GADGETTYPE_PARACHUTE) 
						IF NOT IS_PED_INJURED(Player)
							SETTIMERA(0)
							
							REMOVE_ALL_SHOCKING_EVENTS(FALSE)
							iStageNum++
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bPlayerReachedBottom
					IF DOES_ENTITY_EXIST(viPlayerCar)
						IF IS_VEHICLE_DRIVEABLE(viPlayerCar)
							SET_VEHICLE_ON_GROUND_PROPERLY(viPlayerCar)
							PRINTLN("Players last vehicle being set properly on the ground")
						ELSE
							PRINTLN("Players last vehicle not driveable")
						ENDIF
					ELSE
						PRINTLN("Players last vehicle does not exist")
					ENDIF
							
					iStageNum++
				ELSE
					UPDATE_FINAL_ELEVATOR_CUTSCENE()
				ENDIF
			ENDIF
		BREAK

		CASE 2
			//check to make sure that the player survived the parachute jump
			IF GET_PED_PARACHUTE_STATE(Player) = PPS_INVALID
			AND NOT IS_ENTITY_IN_AIR(Player) 
				IF NOT IS_PED_INJURED(Player)
					IF IS_PLAYER_IN_CONSTRUCTION_AREA()
						PRINT_NOW("ASS_CS_PARA", DEFAULT_GOD_TEXT_TIME, 1)
						iStageNum++
					ELSE
						iStageNum++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_PLAYER_IN_CONSTRUCTION_AREA()
				IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_CONSTRUCT_LEAVE_THE_AREA")
					STOP_AUDIO_SCENE("ASSASSINATION_CONSTRUCT_LEAVE_THE_AREA")
				ENDIF
				PRINTLN("Assassination Passed")
				IF HAS_ENDING_CELL_PHONE_CALL_COMPLETED()
					INITIALIZE_NEXT_STAGE()
					MainStage = STAGE_CLEANUP
					PRINTLN("MainStage = STAGE_CLEANUP")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	HANDLE_DEBLIP_ROOFTOP_ENEMIES()
ENDPROC



PROC MANAGE_PETROL()
	
	//make sure this thing starts leakig petrol if there is a fire near the oil pool position
	IF iPetrolStage > 0
		IF GET_NUMBER_OF_FIRES_IN_RANGE(vTrailEnd, 5) > 0
			IF IS_VEHICLE_DRIVEABLE(viTruck)
				IF GET_VEHICLE_PETROL_TANK_HEALTH(viTruck) > 499
					SET_VEHICLE_PETROL_TANK_HEALTH(viTruck,499)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	
	SWITCH iPetrolStage		
		CASE 0
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(vTrailEnd, FALSE) < 100
				IF IS_VEHICLE_DRIVEABLE(viTruck)
					
					SET_ENTITY_PROOFS(viTruck, FALSE, FALSE, FALSE, FALSE, FALSE)
//					SET_VEHICLE_PETROL_TANK_HEALTH(viTruck,499)
					SET_VEHICLE_CAN_LEAK_PETROL(viTruck,TRUE)
					SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(viTruck,FALSE)
				ENDIF
				
				ADD_PETROL_DECAL(vTrailEnd, 1.5,2,1)
				iPetrolStage ++
			ENDIF
		BREAK
		
		CASE 1
			ADD_PETROL_DECAL(vTrailEnd + <<0.25,0.35,0>>,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 2
			ADD_PETROL_DECAL(vTrailEnd + <<0.50,0.7,0>> ,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 3
			ADD_PETROL_DECAL(vTrailEnd + <<0.75,1.05,0>> ,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 4
			ADD_PETROL_DECAL(vTrailEnd + <<1.0,1.4,0>>,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 5
			ADD_PETROL_DECAL(vTrailEnd + <<1.25,1.75,0>>,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 6
			ADD_PETROL_DECAL(vTrailEnd + <<1.5,2.15,0>>,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 7
			ADD_PETROL_DECAL(vTrailEnd + <<1.8,2.4,0>>,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 8
			START_PETROL_TRAIL_DECALS(1)
			ADD_PETROL_TRAIL_DECAL_INFO(vTrailEnd ,1)
			iPetrolStage ++
		BREAK
		
		CASE 9
			ADD_PETROL_TRAIL_DECAL_INFO(vTrailEnd + <<0.50,0.7,0>> ,1)
			iPetrolStage ++
		BREAK
		
		CASE 10
			ADD_PETROL_TRAIL_DECAL_INFO(vTrailEnd + <<1.0,1.4,0>> ,1)
			iPetrolStage ++
		BREAK
		
		CASE 11
			ADD_PETROL_TRAIL_DECAL_INFO(vTrailEnd + <<1.5,2.15,0>> ,1)
			iPetrolStage ++
		BREAK
		
		CASE 12
			END_PETROL_TRAIL_DECALS()
			PRINTSTRING("Petrol Trail Created")PRINTNL()
			iPetrolStage ++
		BREAK
		
		CASE 13
			//empty state
		BREAK
		
	ENDSWITCH
ENDPROC


//DEBUG ONLY
#IF IS_DEBUG_BUILD
	PROC SHOW_ENEMY_POSITIONS()
		TEXT_LABEL_3 tempstring
		VECTOR pedPos
		INT idx
		
		REPEAT NUM_WAVE_1_WORKERS idx
			IF DOES_ENTITY_EXIST(wave1[idx].ped)
				IF NOT IS_PED_INJURED(wave1[idx].ped)
					tempstring = idx
					DRAW_DEBUG_SPHERE(wave1[idx].loc, 0.25)
					DRAW_DEBUG_TEXT(tempstring, wave1[idx].loc )
					pedPos = GET_ENTITY_COORDS(wave1[idx].ped)
					DRAW_DEBUG_TEXT(tempstring, pedPos )
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT NUM_WAVE_2_WORKERS idx
			IF DOES_ENTITY_EXIST(wave2[idx].ped)
				IF NOT IS_PED_INJURED(wave2[idx].ped)
					tempstring = idx
					DRAW_DEBUG_SPHERE(wave2[idx].loc, 0.25)
					DRAW_DEBUG_TEXT(tempstring, wave2[idx].loc )
					pedPos = GET_ENTITY_COORDS(wave2[idx].ped)
					DRAW_DEBUG_TEXT(tempstring, pedPos )
				ENDIF
			ENDIF
		ENDREPEAT
	ENDPROC
#ENDIF

/// PURPOSE:
///    Called when the mission is failed.
///    Sets the fail reason and tells replay controller to start setting up the replay
///    (showing fail reason, fading out etc)
PROC SET_MISSION_FAILED()

	IF 	bMissionFailed = FALSE // (should only do this failed stuff once)
	
		TRIGGER_MUSIC_EVENT("ASS5_FAIL")
		PRINTLN("TRIGGERING MUSIC - ASS5_FAIL")
	
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-99.4653, -940.7606, 28.2290>>, 82.5402)
		SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-99.4940, -937.6118, 28.1219>>, 250.2531)
	
		STRING strFailReason
		IF NOT bAbandoned
			strFailReason = "ASS_CS_ESCAPED"
		ELSE
			strFailReason = "ASS_CS_ESCAPED" // To Fix Bug # 1209444
		ENDIF
		bMissionFailed = TRUE
		ASSASSIN_MISSION_MarkMissionFailed(strFailReason)
		MISSION_FLOW_MISSION_FAILED()
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_TOUCHED_RIGHT_STICK()
	
	fRightX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) 
	fRightY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	
//	PRINTLN("fRightX = ", fRightX)
//	PRINTLN("fRightY = ", fRightY)
	
	IF fRightX > 0 
	OR fRightY > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ADJUST_CAMERA(CAMERA_INDEX & camToChange, FLOAT fTimeToSwitch, VECTOR vPosition, VECTOR vRotation, FLOAT fCamFOV, BOOL bUseShake = FALSE, FLOAT fShakeValue = 0.5)
	IF DOES_CAM_EXIST(camToChange)
		IF IS_TIMER_STARTED(tPayphoneCameraTimer)
			IF GET_TIMER_IN_SECONDS(tPayphoneCameraTimer) >= fTimeToSwitch
				SET_CAM_COORD(camPayPhoneIntro01, vPosition)
				SET_CAM_ROT(camPayPhoneIntro01, vRotation)
				SET_CAM_FOV(camPayPhoneIntro01, fCamFOV)
				
				IF bUseShake
					SHAKE_CAM(camPayPhoneIntro01, "HAND_SHAKE", fShakeValue)
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_INTRO_CAMERAS()
	
	SWITCH iIntroCameraStages
		CASE 0
			IF ADJUST_CAMERA(camPayPhoneIntro01, 4.0, <<799.7321, -1083.4171, 30.3753>>, <<-1.3455, 0.0000, -42.1124>>, 42.5517, TRUE, 0.3)
				PRINTLN("iIntroCameraStages = 1")
				iIntroCameraStages = 1
			ENDIF
		BREAK
		CASE 1
			IF ADJUST_CAMERA(camPayPhoneIntro01, 11.0, <<814.8909, -1078.2717, 29.0784>>, <<-0.0000, -0.0387, 70.1654>>, 30.1956, TRUE, 0.3)
				PRINTLN("iIntroCameraStages = 2")
				iIntroCameraStages = 2
			ENDIF
		BREAK
		CASE 2
			IF ADJUST_CAMERA(camPayPhoneIntro01, 20.0, <<808.0440, -1074.6467, 29.2119>>, <<-4.8627, 0.0000, -129.3806>>, 36.3462, TRUE, 0.3)
				PRINTLN("iIntroCameraStages = 3")
				iIntroCameraStages = 3
			ENDIF
		BREAK
		CASE 3 
			IF ADJUST_CAMERA(camPayPhoneIntro01, 25.0, <<814.8909, -1078.2717, 29.0784>>, <<-0.0000, -0.0387, 70.1654>>, 30.1956, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 4")
				iIntroCameraStages = 4
			ENDIF
		BREAK
		CASE 4
			IF ADJUST_CAMERA(camPayPhoneIntro01, 30.0, <<808.0440, -1074.6467, 29.2119>>, <<-4.8627, 0.0000, -129.3806>>, 36.3462, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 5")
				iIntroCameraStages = 5
			ENDIF
		BREAK
		CASE 5
			IF ADJUST_CAMERA(camPayPhoneIntro01, 37.0, <<812.5805, -1076.6483, 28.9375>>, <<2.1171, 0.0247, 79.0055>>, 29.2734, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 6")
				iIntroCameraStages = 6
			ENDIF
		BREAK
		CASE 6
			
		BREAK
	ENDSWITCH
	
ENDPROC

PROC DO_STAGE_INTRO_CUTSCENE()

//	PRINTLN("TIME: ", TIMERB())
//	IF STREAMVOL_HAS_LOADED(svPhoneCutscene)
//		PRINTLN("WE'RE LOADED")
//	ENDIF
	
	SWITCH payPhoneCutsceneStages
	
		CASE PAYPHONE_CUTSCENE_INIT
			REQUEST_ANIM_DICT("oddjobs@assassinate@construction@call")
			PRINTLN("REQUESTING ANIM DICTIONARY - oddjobs@assassinate@construction@call")
			REQUEST_MODEL(P_PHONEBOX_01B_S)
			PRINTLN("REQUESTING MODEL - P_PHONEBOX_01B_S")
			
			IF HAS_ANIM_DICT_LOADED("oddjobs@assassinate@construction@call") AND HAS_MODEL_LOADED(P_PHONEBOX_01B_S)
				bContinueWithScene = TRUE
			ENDIF
		
			IF bContinueWithScene
				IF DOES_ENTITY_EXIST(oPayPhone)
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<805.021729,-1076.810791,26.622034>>, <<813.882324,-1076.814209,32.505760>>, 5.000000, <<814.4346, -1081.6877, 27.3991>>, 90.2655)
					PRINTLN("REPOSITIONING VEHICLE")
					
					CLEAR_AREA(<<809.308472,-1074.928101,27.6018>>, 3.0, TRUE)
					
					IF NOT IS_TIMER_STARTED(tPayphoneCameraTimer)
						START_TIMER_NOW(tPayphoneCameraTimer)
						PRINTLN("STARTING TIMER - tPayphoneCameraTimer")
					ENDIF
					
					IF DOES_ENTITY_EXIST(oPayPhone)

						SET_ENTITY_VISIBLE(oPayPhone, FALSE)
						SET_ENTITY_COLLISION(oPayPhone, FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(oPayPhone, FALSE)
						
						PRINTLN("TURNING OFF VISIBILITY AND COLLISION")
						
	//					oPayPhoneAnimated = CREATE_OBJECT(P_PHONEBOX_01B_S, <<809.308472,-1074.928101,27.679195>>) // Having to lower prop for some reason?! ... to fix Bug # 1352784
						oPayPhoneAnimated = CREATE_OBJECT(P_PHONEBOX_01B_S, <<809.308472,-1074.928101,27.6018>>)
						SET_ENTITY_ROTATION(oPayPhoneAnimated, <<0.000000,0.000000,0.091681>>)
						SET_ENTITY_COLLISION(oPayPhoneAnimated, FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(oPayPhoneAnimated, FALSE)
						
						PRINTLN("CREATING SKINNED VERSION OF PROP")
						
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
						PRINTLN("TURNING BACK ON PLAYER VISIBILITY")
					ENDIF
					
					svPhoneCutscene = STREAMVOL_CREATE_SPHERE(<<809.308472,-1074.928101,27.6018>>, 350, FLAG_MAPDATA)
					PRINTLN("CREATING STREAM VOLUME - svPhoneCutscene")
					
					// Start the cutscene.
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerWarpPosition)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerWarpHeading)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
						PRINTLN("MOVING FRANKLIN TO PHONE")
					ENDIF
					
					iSceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
					
					IF DOES_ENTITY_EXIST(oPayPhoneAnimated)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneId, oPayPhoneAnimated, -1)
						PRINTLN("oPayPhoneAnimated EXISTS, ATTACHING SCENE TO IT")
					ELSE
						PRINTLN("NOTHING EXISTS")
					ENDIF
				
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneId, "oddjobs@assassinate@construction@call", "ass_construction_call_p1", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT)
					PRINTLN("TASKING FRANKLIN TO TALK ON THE PHONE")
					
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oPayPhoneAnimated, iSceneId, "ass_construction_call_phone", "oddjobs@assassinate@construction@call", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					PRINTLN("PLAYING ANIMATION ON PAYPHONE PROP")
					
					REMOVE_PARTICLE_FX_IN_RANGE(vPlayerWarpPosition, 5.0)
					
					IF NOT DOES_CAM_EXIST(camPayPhoneIntro01)
						camPayPhoneIntro01 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCameraPosition01, vCameraRotation01, 34.0542, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						PRINTLN("CREATING CAMERA - camPayPhoneIntro01")
						SHAKE_CAM(camPayPhoneIntro01, "HAND_SHAKE", 0.2)
					ENDIF
					
//					PLAY_SYNCHRONIZED_CAM_ANIM(camPayPhoneIntro01, iSceneId, "ass_construction_call_cam", "oddjobs@assassinate@construction@call")
//					PRINTLN("PLAYING ANIMATION ON CAMERA")
					
					ADD_PED_FOR_DIALOGUE(siteConv, 3, NULL, "LESTER")
					ADD_PED_FOR_DIALOGUE(siteConv, 1, PLAYER_PED_ID(), "FRANKLIN")
					
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
//					DISABLE_NAVMESH_IN_AREA(<< vPhoneLocation.x-20, vPhoneLocation.y-20, vPhoneLocation.z-20 >>, << vPhoneLocation.x+20, vPhoneLocation.y+20, vPhoneLocation.z+20 >>, TRUE)
				   
				   	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS | SPC_REMOVE_FIRES | SPC_REMOVE_PROJECTILES) // | SPC_LEAVE_CAMERA_CONTROL_ON)
					PRINTLN("REMOVING PLAYER CONTROL")
					DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
				   
				   	payPhoneCutsceneStages = PAYPHONE_CUTSCENE_UPDATE
					PRINTLN("CONSTRUCTION: GOING TO STATE - PAYPHONE_CUTSCENE_UPDATE")
				ELSE
					REQUEST_ANIM_DICT("oddjobs@assassinate@construction@call")
					PRINTLN("REQUESTING ANIM DICTIONARY - oddjobs@assassinate@construction@call")
					REQUEST_MODEL(P_PHONEBOX_01B_S)
					PRINTLN("REQUESTING MODEL - P_PHONEBOX_01B_S")
					
					IF NOT DOES_ENTITY_EXIST(oPayPhone)
						oPayPhone = GET_CLOSEST_OBJECT_OF_TYPE(vPhoneLocation, 5.0, prop_phonebox_01b)
						PRINTLN("CONSTRUCTION: PAYPHONE WAS NOT FOUND IN TRIGGER SCENE, FIND IN SCRIPT")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_UPDATE
		
			HANDLE_INTRO_CAMERAS()

			IF IS_TIMER_STARTED(tPayphoneCutsceneTimer)
				IF GET_TIMER_IN_SECONDS(tPayphoneCutsceneTimer) > 1.0
					IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
						bSkipCutscene = TRUE
						
						IF NOT IS_SCREEN_FADED_OUT()
							FADE_DOWN()
						ENDIF
						
						payPhoneCutsceneStages = PAYPHONE_CUTSCENE_SKIP
						PRINTLN("CONSTRUCTION: GOING TO STATE - PAYPHONE_CUTSCENE_SKIP")
						BREAK
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT bPlayedPayPhoneConvo
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.05
							IF CREATE_CONVERSATION(siteConv, "OJASAUD", "OJAScnt_cs", CONV_PRIORITY_VERY_HIGH)
								IF NOT IS_TIMER_STARTED(tPayphoneCutsceneTimer)
									START_TIMER_NOW(tPayphoneCutsceneTimer)
									PRINTLN("CONSTRUCTION: STARTING TIMER - tPayphoneCutsceneTimer")
									bPlayedPayPhoneConvo = TRUE
									
									IF IS_REPEAT_PLAY_ACTIVE()
										FADE_IN()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_TIMER_STARTED(tPayphoneCutsceneTimer)
				IF GET_TIMER_IN_SECONDS(tPayphoneCutsceneTimer) > fCutsceneLength
				OR (IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.961)
				OR bSkipCutscene
					payPhoneCutsceneStages = PAYPHONE_CUTSCENE_CLEANUP
					PRINTLN("CONSTRUCTION: GOING TO STATE - PAYPHONE_CUTSCENE_CLEANUP")
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_SKIP
		
			STOP_SCRIPTED_CONVERSATION(FALSE)
		
			IF bSkipCutscene
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerWarpPosition)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerWarpHeading)
						PRINTLN("MOVING FRANKLIN - SKIP CUTSCENE")
					ENDIF
					
					STOP_SYNCHRONIZED_ENTITY_ANIM (oPayPhoneAnimated, INSTANT_BLEND_OUT, TRUE)
				
					WAIT(0)
					DO_SCREEN_FADE_IN(500)
					
					DESTROY_ALL_CAMS()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
					payPhoneCutsceneStages = PAYPHONE_CUTSCENE_CLEANUP
					PRINTLN("CONSTRUCTION: GOING TO STATE - PAYPHONE_CUTSCENE_CLEANUP")
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_CLEANUP
					
			IF NOT bSkipCutscene
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				DESTROY_ALL_CAMS()
			ENDIF
			
			IF STREAMVOL_IS_VALID(svPhoneCutscene)
				STREAMVOL_DELETE(svPhoneCutscene)
				PRINTLN("DELETING STREAM VOLUME - svPhoneCutscene VIA PAYPHONE_CUTSCENE_CLEANUP")
			ENDIF
			
			SET_ENTITY_COLLISION(oPayPhoneAnimated, TRUE)
			
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE)
			DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
			//to fix Bug 1173621 - (3/12/13) - MB
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			
			REMOVE_ANIM_DICT("oddjobs@assassinate@construction@call")
			SET_OBJECT_AS_NO_LONGER_NEEDED(oPayPhone)
			
			MainStage = STAGE_INIT
			PRINTLN("CONSTRUCTION: GOING TO STATE - STAGE_INIT")
		BREAK
	ENDSWITCH
ENDPROC

// -----------------------------------
// MAIN SCRIPT
// -----------------------------------
PROC Update_Loop()

	IF MainStage > STAGE_SPAWNING AND MainStage < STAGE_ROOFTOP_BATTLE
		UPDATE_TRIGGER_EARLY_ROOFTOP_BATTLE()
	ENDIF
	
	IF MainStage > STAGE_SPAWNING AND MainStage < STAGE_ELEVATOR_CUTSCENE
		MANAGE_PETROL()
	ENDIF
	
	//handle the wanted level settings
	HANDLE_PLAYER_MAX_WANTED_LEVEL()

	SWITCH MainStage
//		CASE STAGE_TIME_LAPSE
//			timeLapseCutscene()
//		BREAK
		CASE STAGE_BRIEF_INIT
			
			//initialize this
			iPetrolStage = 0
			iNumDead = 0
			iCurrentPlayerLine = 0
			heliFleeLinePlayed = FALSE
			
			//assign unsafe angled areas near the warning guys to fix issues like Bug 1545383 where guards can loop their lines if the player is close to them but not in the site
			vAngledAreaMinWarningGuy[0] = <<-100.453239,-947.640686,25.729670>>
			vAngledAreaMaxWarningGuy[0] = <<-122.555359,-939.633911,36.728752>>
			fAngledAreaWidthWarningGuy[0] = 15.0
			vAngledAreaMinWarningGuy[1] = <<-209.190720,-1087.516602,18.190535>>
			vAngledAreaMaxWarningGuy[1] = <<-188.870010,-1095.158325,34.437973>>
			fAngledAreaWidthWarningGuy[1] = 25.0
			vAngledAreaMinWarningGuy[2] = <<-84.503349,-1029.007690,24.754467>>
			vAngledAreaMaxWarningGuy[2] = <<-80.107407,-1016.939575,35.953331>>
			fAngledAreaWidthWarningGuy[2] = 15
				
			IF Is_Replay_In_Progress()
//				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
//					PRINTLN("REPLAY VEHICLE IS AVAILABLE - CONSTRUCTION")
//					bReplayVehicleAvailable = TRUE
//				ELSE
//					PRINTLN("REPLAY VEHICLE IS NOT AVAILABLE - CONSTRUCTION")
//				ENDIF
//				bReplaying = TRUE
				MainStage = STAGE_INIT
				PRINTLN("MainStage = STAGE_INIT")
				BREAK
			ENDIF
			
			DO_STAGE_INTRO_CUTSCENE()
		BREAK
		
		CASE STAGE_INIT
			IF Is_Replay_In_Progress()
				bReplaying = TRUE
			ENDIF
		
			INIT_MODELS()
			REQUEST_STREAMS_NO_PEDS()
			SETUP_MISSION_REWARDS()
						
			//restrict ambient peds from being created within the construction site
			SET_PED_NON_CREATION_AREA(<< -250.5509, -1126.1462, -100.7299 >>, << -55.8166, -858.6929, 100.7734 >>)
			ADD_SCENARIO_BLOCKING_AREA(<< -55.8166, -1126.1462, -100.7299 >>, << -250.5509, -858.6929, 100.7734 >>)
			
			//add a body armor pickup next to the second elevator on the mid-level of the building
//			CREATE_PICKUP_ROTATE(PICKUP_ARMOUR_STANDARD, <<-145.070, -958.325, 114.33>>, <<270 ,0, 0>>)
			
			SET_ELEVATOR_PARAMS()
			SETUP_WORKER_PARAMS()
			SETUP_PATROL_PATHS()
			
			//setup Franklin's dialogue info
			ADD_PED_FOR_DIALOGUE(siteConv, 1, Player, "FRANKLIN")
			
//			TRIGGER_MUSIC_EVENT("ASS5_DRIVE")
//			PRINTLN("TRIGGERING MUSIC - ASS5_DRIVE")
			
			MainStage = STAGE_STREAMING
			PRINTLN("MainStage = STAGE_STREAMING")
		BREAK
		
		CASE STAGE_STREAMING
			IF ARE_STREAMS_LOADED_NO_PEDS()
				SET_PED_MODEL_IS_SUPPRESSED(WorkerModel[0], TRUE)
				
				MainStage = STAGE_SPAWNING
				
				PRINTLN("MainStage = STAGE_SPAWNING")
			ENDIF
		BREAK
		
		CASE STAGE_SPAWNING
			DO_STAGE_SPAWNING()
		BREAK
		
		CASE STAGE_GET_TO_SITE
			UPDATE_WARNING_GUARDS()
			UPDATE_PATROLLING_GUARDS()
			DO_STAGE_GET_TO_SITE()
		BREAK

		CASE STAGE_PANIC_CUTSCENE
			DO_STAGE_PANIC_CUTSCENE()
		BREAK
		
		CASE STAGE_ENTER_SITE
			UPDATE_WARNING_GUARDS()
			UPDATE_PATROLLING_GUARDS()
			DO_STAGE_ENTER_SITE()
		BREAK
		
//		CASE STAGE_GET_TO_ELEVATOR
//			DO_STAGE_GET_TO_ELEVATOR()
//		BREAK
		
		CASE STAGE_ELEVATOR_CUTSCENE
			DO_STAGE_ELEVATOR_CUTSCENE()
		BREAK
		
		CASE STAGE_GET_TO_SECOND_ELEVATOR
			DO_STAGE_GET_TO_SECOND_ELEVATOR()
		BREAK
		
		CASE STAGE_SECOND_ELEVATOR_CUTSCENE
			DO_STAGE_SECOND_ELEVATOR_CUTSCENE()
		BREAK
		
		CASE STAGE_ROOFTOP_BATTLE
			DO_STAGE_ROOFTOP_BATTLE()
		BREAK

		CASE STAGE_KILL_CAM
			DO_STAGE_KILL_CAM()
		BREAK
		
		CASE STAGE_FLEE_POLICE
			DO_STAGE_FLEE_POLICE()
		BREAK
		
		CASE STAGE_CLEANUP
			Script_Passed()
		BREAK
	ENDSWITCH
	
	//make sure workers react appropriately
	IF MainStage <> STAGE_PANIC_CUTSCENE
		HANDLE_ENEMY_BEHAVIOR()
	ENDIF
	
	//play the kill cam if the target dies
	HANDLE_KILL_CAMERA_LOGIC()
	
	//check to see if target escaped
	IF HAS_PLAYER_ABANDONED_TARGET(bAbandoned)
		SET_MISSION_FAILED()
	ENDIF

ENDPROC



// debug only
#IF IS_DEBUG_BUILD
	PROC JUMP_TO_STAGE(Assassin_Stages stage, BOOL bIsDebugJump = FALSE)
		
		FADE_OUT()
		
		//Update mission checkpoint in case they skipped the stages where it gets set.
		IF bIsDebugJump
		
			SWITCH STAGE
				CASE STAGE_SPAWNING
					GO_TO_CHECKPOINT_1()
				BREAK
				CASE STAGE_ENTER_SITE
					GO_TO_CHECKPOINT_2()
				BREAK
				CASE STAGE_GET_TO_SECOND_ELEVATOR
					GO_TO_CHECKPOINT_3()
				BREAK
				CASE STAGE_ROOFTOP_BATTLE
					GO_TO_CHECKPOINT_4()
				BREAK
				CASE STAGE_CLEANUP
					GO_TO_CLEANUP()
				BREAK
			ENDSWITCH

		ENDIF
	ENDPROC

	PROC INITIALIZE_JSKIP()
		RENDER_SCRIPT_CAMS(FALSE, TRUE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDPROC

 	// debug: skips
	PROC DO_DEBUG_SKIPS()
		Assassin_Stages eStage
		
		IF IS_SCREEN_FADED_IN()
		
			IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iDebugJumpStage)
				IF iDebugJumpStage = 0
					eStage = STAGE_SPAWNING
				ELIF iDebugJumpStage = 1
					eStage = STAGE_ENTER_SITE
				ELIF iDebugJumpStage = 2
					eStage = STAGE_GET_TO_SECOND_ELEVATOR
				ELIF iDebugJumpStage = 3
					eStage = STAGE_ROOFTOP_BATTLE
				ENDIF
				bDoingPSkip = TRUE
				JUMP_TO_STAGE(eStage, TRUE)
			ENDIF
			
			
			// Debug Key: Check for Pass 
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				MainStage = STAGE_CLEANUP
			ENDIF

			// Debug Key: Check for Fail 
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
				SET_MISSION_FAILED()
			ENDIF
			
//			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_N))
//				SET_ENTITY_COORDS(Player, <<-87.3165, -916.2086, 28.2419>>)
//			ENDIF
			
			// skip stage P
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P) 
				bDoingPSkip = TRUE
				IF MainStage = STAGE_GET_TO_SITE OR MainStage = STAGE_ENTER_SITE
					eStage = STAGE_SPAWNING
					PRINTLN("eStage = STAGE_SPAWNING")
				ELIF /*MainStage = STAGE_GET_TO_ELEVATOR OR*/ MainStage = STAGE_GET_TO_SECOND_ELEVATOR
					eStage = STAGE_ENTER_SITE
					PRINTLN("eStage = STAGE_ENTER_SITE")
				ELIF MainStage = STAGE_ROOFTOP_BATTLE
					eStage = STAGE_GET_TO_SECOND_ELEVATOR
					PRINTLN("eStage = STAGE_GET_TO_SECOND_ELEVATOR")
				ELIF MainStage = STAGE_FLEE_POLICE
					eStage = STAGE_ROOFTOP_BATTLE
					PRINTLN("eStage = STAGE_ROOFTOP_BATTLE")
				ENDIF
				
				//TODO add cleanup
				JUMP_TO_STAGE(eStage, TRUE)
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) 		
				IF MainStage = STAGE_GET_TO_SITE
					bDoingJSkip = TRUE
					eStage = STAGE_ENTER_SITE
					PRINTLN("eStage = STAGE_ENTER_SITE")
					JUMP_TO_STAGE(eStage, TRUE)
				ELIF MainStage = STAGE_ENTER_SITE /*OR  MainStage = STAGE_GET_TO_ELEVATOR */
					bDoingJSkip = TRUE
					eStage = STAGE_GET_TO_SECOND_ELEVATOR
					PRINTLN("eStage = STAGE_GET_TO_SECOND_ELEVATOR")
					JUMP_TO_STAGE(eStage, TRUE)
				ELIF MainStage = STAGE_GET_TO_SECOND_ELEVATOR
					bDoingJSkip = TRUE
					eStage = STAGE_ROOFTOP_BATTLE
					PRINTLN("eStage = STAGE_ROOFTOP_BATTLE")
					JUMP_TO_STAGE(eStage, TRUE)
				ELIF MainStage = STAGE_ROOFTOP_BATTLE OR MainStage = STAGE_FLEE_POLICE
					bDoingJSkip = TRUE
					eStage = STAGE_CLEANUP
					PRINTLN("eStage = STAGE_CLEANUP")
					JUMP_TO_STAGE(eStage, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

// Initialisation and the script loop
SCRIPT
	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		DEBUG_MESSAGE("FORCE CLEANUP HAS OCCURRED!!!")
		
		TRIGGER_MUSIC_EVENT("ASS5_FAIL")
		PRINTLN("TRIGGERING MUSIC - ASS5_FAIL")
		
		SET_BITMASK_AS_ENUM(g_savedGlobals.sAssassinData.iGenericData, ACD_FAILED)
		Mission_Flow_Mission_Force_Cleanup()
		Script_Cleanup()
	ENDIF
	
	// Any initialisation (generally, only mission scripts should set
	// the mission flag to TRUE)
	SET_MISSION_FLAG(TRUE)
	
	
	IF Is_Replay_In_Progress()
		IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
			PRINTLN("REPLAY VEHICLE IS AVAILABLE - CONSTRUCTION")
			bReplayVehicleAvailable = TRUE
		ELSE
			PRINTLN("REPLAY VEHICLE IS NOT AVAILABLE - CONSTRUCTION")
		ENDIF
		bReplaying = TRUE
	ELSE
		bReplaying = FALSE
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		AddWidgets(myDebugData)
	#ENDIF
	
	Player = PLAYER_PED_ID()
	
	REMOVE_IPL("DT1_21_prop_lift_on")
	REQUEST_IPL("DT1_21_ConSiteAssass")
	
	// Fix Bug # 712684
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	ENDIF
	
	SETUP_DEBUG()
	
	ASSASSINATION_ClearUnneededGenericData()
	
	iCurAssassinRank = ENUM_TO_INT(ASSASSINATION_Construction) //g_savedGlobals.sAssassinData.iCurrentAssassinRank
	missionData = ASSASSINATION_GetMissionData(iCurAssassinRank)	
	PRINTLN("Current rank is... ", iCurAssassinRank)

	
	//Don't check EAggro_Attacked to allow the player to perform stealth kills
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Attacked)
	
	//allow player to go wanted without immediately triggering aggro
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
	
	//reduce enemy reactions to aim time by 50%  (0.75 seconds)
	aggroArgs.fAimTime = 750
	
	//set wanted level multiplier (note that HANDLE_PLAYER_MAX_WANTED_LEVEL() will disallow the player to get a wanted level once the player has played past the panic cutscene)
	SET_WANTED_LEVEL_MULTIPLIER(0.2)
	
	MainStage = STAGE_BRIEF_INIT
	PRINTLN("STARTING MISSION ON STAGE - STAGE_BRIEF_INIT")
	
	
	//HANDLE STREAMING AROUND REPLAY START POSITION
	IF IS_REPLAY_IN_PROGRESS()
	
		bHaveRetryOnce = TRUE
		PRINTLN("CONSTRUCTION: SETTING -  bHaveRetryOnce = TRUE")
		
		iStageToUse = Get_Replay_Mid_Mission_Stage()
		PRINTLN("iStageToUse = ", iStageToUse)
		
		IF g_bShitskipAccepted
			IF iStageToUse <= 3
				iStageToUse = (iStageToUse + 1)
				PRINTLN("SHIT SKIP: iStageToUse = ", iStageToUse)
			ENDIF
		ENDIF
		
		IF iStageToUse = 0
			IF bReplayVehicleAvailable
				REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
				WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
					PRINTLN("CONSTRUCTION: Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED - 01")
					WAIT(0)
				ENDWHILE
			
				//create the replay vehicle but make sure it isnt a water based vehicle (sso we dont spawn it on land)
				VEHICLE_INDEX vehPlayer
				vehPlayer = CREATE_REPLAY_CHECKPOINT_VEHICLE(vMoveVehiclePosition01, fMoveVehicleHeading01)
				IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
				ELSE
					DELETE_VEHICLE(vehPlayer)
				ENDIF
			ENDIF
			
			START_REPLAY_SETUP(<< 804.2744, -1077.5593, 27.5087 >>, 71.7369)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 1
			IF bReplayVehicleAvailable
				REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
				WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
					PRINTLN("CONSTRUCTION: Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED - 02")
					WAIT(0)
				ENDWHILE
			
				//create the replay vehicle but make sure it isnt a water based vehicle (sso we dont spawn it on land)
				VEHICLE_INDEX vehPlayer
				vehPlayer = CREATE_REPLAY_CHECKPOINT_VEHICLE(vMoveVehiclePosition02, fMoveVehicleHeading02)
				IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
				ELSE
					DELETE_VEHICLE(vehPlayer)
				ENDIF
			ENDIF

			START_REPLAY_SETUP(<< -102.4433, -907.4056, 28.2065 >>, 160.4677)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 2
			IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<-182.0891, -1004.0555, 113.1355>>)
				REMOVE_COVER_POINT(ciSecondFloorPosition)
				PRINTLN("REMOVING COVER POINT - ciSecondFloorPosition")
			ENDIF
	
			ciSecondFloorPosition = ADD_COVER_POINT(<<-182.0891, -1004.0555, 113.1355>>, 341.2941, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)
						
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(90)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			START_REPLAY_SETUP(<< -182.1094, -1004.0585, 113.1362 >>, 250.235)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 3
			IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<-150.5964, -945.8911, 268.1318>>)
				REMOVE_COVER_POINT(ciRoofPosition)
				PRINTLN("REMOVING COVER POINT - ciRoofPosition")
			ENDIF
			
			ciRoofPosition = ADD_COVER_POINT(<<-150.5964, -945.8911, 268.1318>>, 231.5456, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_180)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(Player, TRUE)
			TASK_PUT_PED_DIRECTLY_INTO_COVER(Player, vLastStageCoverPosition, -1)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(90)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			START_REPLAY_SETUP(vLastStageCoverPosition, fLastStageCoverHeading)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 4
			IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<-150.5964, -945.8911, 268.1318>>)
				REMOVE_COVER_POINT(ciRoofPosition)
				PRINTLN("REMOVING COVER POINT - ciRoofPosition")
			ENDIF
			
			START_REPLAY_SETUP(<<-102.2210, -941.3790, 28.2330>>, 67.1012)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ENDIF
	ENDIF
	
	
	// The script loop
	WHILE (TRUE)
		// Maintain the script – perform per-frame functionality
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ASS5")
		
		IF bMissionFailed
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				Script_Failed()
			ENDIF
		ELIF NOT IS_PED_INJURED(Player)
			Update_Loop()
			
			#IF IS_DEBUG_BUILD
				DO_DEBUG_SKIPS()
				
//				IF myDebugData.bTurnOnDebugPos 
//					IF NOT DOES_PICKUP_EXIST(pickupParachuteTable) //the ped you are attaching 
//						SET_BIT(iParachutePickupTable, ENUM_TO_INT(PLACEMENT_FLAG_FIXED)) 
//						
//						pickupParachuteTable = CREATE_PICKUP_ROTATE(PICKUP_PARACHUTE, << myDebugData.fOffsetX, myDebugData.fOffsetY, myDebugData.fOffsetZ >>, << myDebugData.fRotateX, myDebugData.fRotateY, myDebugData.fRotateZ >>, iParachutePickupTable)
//						PRINTLN("CREATING PARACHUTE PICKUP ON TABLE")
//					ELSE
//						REMOVE_PICKUP(pickupParachuteTable)
//					ENDIF 
//				ENDIF

				SHOW_ENEMY_POSITIONS()
   			 #ENDIF
		ENDIF
		
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


