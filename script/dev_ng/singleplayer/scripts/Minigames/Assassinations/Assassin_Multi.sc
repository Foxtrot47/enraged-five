
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      					//
//      SCRIPT NAME     : Assassin_Multi.sch       							        						//
//      AUTHOR          : Michael Bagley	                                            					//
//      DESCRIPTION     : 4 Assassination targets - Timed mission											//
//                        Window Washer                                                    					//
//                        Body Builder                                                   					//
//                        Guy on Bike                                                    					//
//                        Guy on Yacht                                                   					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "Assassin_Core.sch"
USING "Assassin_Shared.sch"
USING "Assassination_briefing_lib.sch"
USING "shared_hud_displays.sch"
USING "replay_public.sch"
USING "script_oddjob_queues.sch"
USING "clearMissionArea.sch"
USING "timelapse.sch"
USING "replay_private.sch"
USING "load_queue_public.sch"

#IF IS_DEBUG_BUILD
      USING "shared_debug.sch"
      USING "script_debug.sch"
      USING "select_mission_stage.sch"
#ENDIF

// ********************************** Script Globals *************************************
CONST_INT NUM_ASSASSIN_TARGETS 4
CONST_INT NUM_BYSTANDER_PEDS 2
CONST_INT NUM_GAS_STATION_VEHICLES 4
CONST_INT NUM_GAS_STATION_VEHICLE_MODELS 3
CONST_INT iTOTAL_TIME_ALLOTTED 540000

ENUM CURRENT_ASSASSIN_TARGET
	ASSASSIN_TARGET_BODYBUILDER = 0,
	ASSASSIN_TARGET_YACHT,
	ASSASSIN_TARGET_WASHER,
	ASSASSIN_TARGET_BIKER
ENDENUM
CURRENT_ASSASSIN_TARGET curTarget

ENUM CURRENT_TARGET_STAGE
	TARGET_STAGE_BLIP_TARGET = 0,
	TARGET_STAGE_WAITING,
	TARGET_STAGE_PANICKING
ENDENUM
CURRENT_TARGET_STAGE curTargetStage

ENUM BB_ANIM_STAGE
	BB_ANIM_INTRO = 0,
	BB_ANIM_WALK_TO_NEXT,
	BB_ANIM_PERFORM
ENDENUM
BB_ANIM_STAGE curBBAnimStage

ENUM BYSTAND_GOAL
	BG_WATCH = 0,
	BG_IDLE,
	BG_LEAVE
ENDENUM

// ------------------------------Cutscene variables------------------------------

ENUM PAYPHONE_CUTSCENE
	PAYPHONE_CUTSCENE_INIT = 0,
	PAYPHONE_CUTSCENE_UPDATE,
	PAYPHONE_CUTSCENE_SKIP,
	PAYPHONE_CUTSCENE_CLEANUP
ENDENUM
PAYPHONE_CUTSCENE payPhoneCutsceneStages = PAYPHONE_CUTSCENE_INIT

OBJECT_INDEX oPayPhone, oPayPhoneAnimated
CAMERA_INDEX camPayPhoneIntro01
CAMERA_INDEX camPayPhoneIntro01a
VECTOR vPhoneLocation = <<-700.1431, -917.7708, 18.2147>>
VECTOR vPlayerWarpPosition = <<-700.1431, -917.7708, 18.2147>> 
FLOAT fPlayerWarpHeading = 3.2830

FLOAT fCutsceneLength = 42.0
FLOAT fRightX, fRightY

VECTOR vCameraPosition01a = <<-699.1455, -917.3423, 19.7904>>  
VECTOR vCameraRotation01a = <<-5.6096, -0.1143, 75.6741>> 
VECTOR vCameraPosition01 = <<-699.1455, -917.3423, 19.7904>> 
VECTOR vCameraRotation01 = <<-2.4284, -0.1143, 81.9337>>
structTimer tPayphoneCutsceneTimer
structTimer tPayphoneCameraTimer
BOOL bSkipCutscene = FALSE
BOOL bPlayedPayPhoneConvo = FALSE

VECTOR scenePosition = <<0,0,0>>
VECTOR sceneRotation = <<0,0,0>>
INT iSceneIdPayPhone

STREAMVOL_ID svPhoneCutscene

structTimer tBeepTimer

// ------------------------------Cutscene variables------------------------------

BOOL bMissionTimerStarted
BOOL bMissionTimerExpired
BOOL bLocatePrinted
BOOL bReplaying = FALSE
BOOL bReplayVehicleAvailable = FALSE
BOOL bCheckpointDataSet = FALSE
BOOL bRopeDetached
BOOL bTargetCruising
BOOL bPanicking
BOOL bWarning
BOOL bPanicLinePlayed
BOOL bThreatenLinePlayed
BOOL bSunbatherPanic = FALSE
BOOL bAttackedBystanders
BOOL bSeenAimingAtTarget
//BOOL bSetActionMode = FALSE
BOOL bAttachedRope = FALSE
BOOL bTaskedToFlee = FALSE
BOOL bTaskedBystandersToFlee = FALSE
BOOL bDoingPSkip
BOOL bPlayerHasShitSkipped = FALSE
//BOOL bPrepareMusic = FALSE
BOOL bPlayedCellPhoneCall = FALSE
BOOL bAggroAtDistance = FALSE
BOOL bSplashSoundPlayed = FALSE
BOOL bPlayedBystanderPanicLine = FALSE
BOOL bGaveTextMessage = FALSE
BOOL bContinueWithScene = FALSE
//BOOL bTouchedRightStick = FALSE
BOOL bPedsForCurrentTargetCreated = FALSE
BOOL bTargetKillLinePlayed = FALSE
BOOL bPoliceChaseLinePlayed = FALSE
BOOL bNewsReportPlayed = FALSE

INT iNumTargetsKilled
INT iMissionTimerStart = 540000	//this and fMissionTime should be the same
INT iDialogueLine
INT iLineIDXToPlay = -1
INT iDialogueIdleLine
INT iTimeSoFar
INT iFrameCounter
INT iBystanderCounter
INT iIdleSpeechStage
INT iWarningSpeechStage
INT iRopeStages
INT iFranklinsReactionToTime = 0
INT iStageToUse = 0
INT iIntroCameraStages = 0
INT iSoundIdMulti = -1
INT iTimeToDisplayOnTimer
INT iCheckpointSavedTime = -1

FLOAT fCutsceneTriggerDist = 75
FLOAT fFailDist
FLOAT fSpeechTime = 3
FLOAT fMissionTime = 540000

ROPE_INDEX ropeWindowB
ROPE_INDEX ropeWindowA
//CAMERA_INDEX cutCam
VECTOR vFlexPos = << -1208.4615, -1559.6609, 3.6087 >>
VECTOR vReplayStartPos
FLOAT fReplayStartHead
VECTOR vVehReplayStartPos
FLOAT fVehReplayStartHead
VEHICLE_INDEX viJetski
VEHICLE_INDEX viYacht
VEHICLE_INDEX viRestStopCamper
VEHICLE_INDEX viRestStopCar
VEHICLE_INDEX viGasStationVehicle[NUM_GAS_STATION_VEHICLES]
VEHICLE_INDEX viPlayerLastVehicle
VEHICLE_INDEX viReplayVehicle

MODEL_NAMES modelParkedVehicle[NUM_GAS_STATION_VEHICLE_MODELS]

SCENARIO_BLOCKING_INDEX sbi01

//structTimer bystanderLeaveTimer
//structTimer bystanderArriveTimer
structTimer speechTimerTarget
structTimer warningTimer
structTimer aimTimer
structTimer printTimer
//structTimer tPrepMusicTimer
structTimer tLeaveAreaTimer
structTimer tDelayPhoneCallTimer
structTimer tPoliceScanner
structTimer tRadioNewsTimer

VECTOR 	vFirstCheckpointStartPos = <<-702.6807, -920.2365, 18.0144>>
VECTOR vTargetLocation[3]
FLOAT 	fFirstCheckpointStartHeading = 120.5394

INT iSceneId = -1

structPedsForConversation cutsceneConvStruct
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

SIMPLE_USE_CONTEXT cucEndScreenCore

LoadQueueLarge sLoadQueue

STRUCT BYSTANDER_PED_STRUCT
	PED_INDEX ped
	VECTOR vSpawnPos
	VECTOR vIdlePos
	FLOAT fHead
	BYSTAND_GOAL goal
ENDSTRUCT
BYSTANDER_PED_STRUCT bystanders[NUM_BYSTANDER_PEDS]


//DEBUG ONLY
#IF IS_DEBUG_BUILD
//	DEBUG_POS_DATA myDebugData
	
	MissionStageMenuTextStruct sSkipMenu[4]
	INT iDebugJumpStage = 0
	
//	PROC SHOW_BYSTANDER_POSITIONS()
//		TEXT_LABEL_3 tempstring
//		VECTOR pedPos
//		INT tempInt
//		
//		REPEAT NUM_BYSTANDER_PEDS tempInt
//			IF DOES_ENTITY_EXIST(bystanders[tempInt].ped)
//				IF NOT IS_PED_INJURED(bystanders[tempInt].ped)
//					tempstring = tempInt
//					DRAW_DEBUG_SPHERE(bystanders[tempInt].vSpawnPos, 0.25)
//					DRAW_DEBUG_TEXT(tempstring, bystanders[tempInt].vSpawnPos )
//					pedPos = GET_ENTITY_COORDS(bystanders[tempInt].ped)
//					DRAW_DEBUG_TEXT(tempstring, pedPos )
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDPROC
	
	PROC SETUP_DEBUG()
		sSkipMenu[0].sTxtLabel = "BODY_BUILDER"
		sSkipMenu[1].sTxtLabel = "GUY_ON_YACHT"
		sSkipMenu[2].sTxtLabel = "WINDOW_WASHER"
		sSkipMenu[3].sTxtLabel = "GUY_ON_BIKE"
	ENDPROC
#ENDIF
// ********************************** END SCRIPT GLOBALS  *************************************


PROC timeLapseCutscene()
	IF NOT IS_REPLAY_IN_PROGRESS()
	    IF DO_TIMELAPSE(SP_MISSION_ASSASSIN_2, sTimelapse, FALSE, FALSE)
			PRINTLN("GOING TO STATE - MISSION_BRIEF_INIT VIA timeLapseCutscene")
	    	curStage = MISSION_BRIEF_INIT
	    ENDIF
	ELSE
		PRINTLN("GOING TO STATE - MISSION_BRIEF_INIT VIA timeLapseCutscene, REPLAY")
	    curStage = MISSION_BRIEF_INIT
	ENDIF
ENDPROC

PROC GRAB_RETRY_VEHICLE()
	IF NOT DOES_ENTITY_EXIST(viReplayVehicle)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				viReplayVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				PRINTLN("FOUND REPLAY VEHICLE")
				
				IF DOES_ENTITY_EXIST(viReplayVehicle) AND NOT IS_ENTITY_DEAD(viReplayVehicle)
					OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(viReplayVehicle)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INIT_PARKED_VEHICLE_MODELS_FOR_GAS_STATION()
	modelParkedVehicle[0] = PREMIER
	modelParkedVehicle[1] = DILETTANTE
	modelParkedVehicle[2] = VIGERO
ENDPROC

PROC REQUEST_PARKED_VEHICLE_MODELS_FOR_GAS_STATION()
	INT idx
	REPEAT NUM_GAS_STATION_VEHICLE_MODELS idx
		REQUEST_MODEL(modelParkedVehicle[idx])
	ENDREPEAT
ENDPROC

FUNC BOOL ARE_PARKED_VEHICLE_MODELS_STREAMED()
	
	REQUEST_MODEL(PREMIER)
	REQUEST_MODEL(DILETTANTE)
	REQUEST_MODEL(VIGERO)
	
	WHILE NOT HAS_MODEL_LOADED(PREMIER)
	OR NOT HAS_MODEL_LOADED(DILETTANTE)
	OR NOT HAS_MODEL_LOADED(VIGERO)
		PRINTLN("WAITING ON PARKED CAR MODELS TO LOAD")
		RETURN FALSE
	ENDWHILE
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Releases all vehicle models for the parked cars that were created in the gas station of the phone cutscene (if in memory)
PROC SET_PARKED_VEHICLE_MODELS_AS_NO_LONGER_NEEDED()
	INT idx
	REPEAT NUM_GAS_STATION_VEHICLE_MODELS idx
		IF modelParkedVehicle[idx] != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(modelParkedVehicle[idx])
		ENDIF
	ENDREPEAT
ENDPROC


PROC INIT_BODYBUILDER_DATA_DONT_STREAM_PEDS(ASS_TARGET_DATA& targetData)
	targetData.targetEnum = U_M_Y_BABYD
	targetData.vTargetPos = vFlexPos
	targetData.bNonTemp = TRUE
	targetData.fTargetHead = 24.7111
	targetData.vDestPos = << -1183.1202, -1523.1781, 3.3640 >> //<< -1207.5627, -1553.2156, 3.3728 >>
	targetData.vWarpPos = << -1205.3925, -1548.0220, 3.3229 >>
	targetData.vCutscenePos = << -1202.3690, -1550.9016, 5.8757 >>
	targetData.vCutsceneHead = << -4.6, -0.0003, 152.2 >>
	targetData.fCutsceneDoF = 35
	targetData.sAnimDict[0] = "oddjobs@assassinate@multi@"
	targetData.sAnimDict[1] = "misscommon@response"
	
	aggroArgs.iShotIRange = 5
	
//	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.targetEnum)
//	ADD_STREAMED_MODEL(targetData.streamedModels, A_F_Y_BEACH_01)	//bystander ped
//	ADD_STREAMED_MODEL(targetData.streamedModels, A_F_Y_Fitness_02)	//bystander ped
	ADD_STREAMED_MODEL(targetData.streamedModels, PREMIER)	//bystander ped
	ADD_STREAMED_MODEL(targetData.streamedModels, DILETTANTE)	//bystander ped
	ADD_STREAMED_MODEL(targetData.streamedModels, VIGERO)	//bystander ped
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[0])
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[1])
	
	fFailDist = 175
	
	INIT_PARKED_VEHICLE_MODELS_FOR_GAS_STATION()
ENDPROC


FUNC BOOL IS_BODYBUILDER_DATA_LOADED_NO_PEDS(ASS_TARGET_DATA& targetData)
	IF ARE_MODELS_STREAMED(targetData.streamedModels)
	AND ARE_ANIMS_STREAMED(sLoadQueue)
	AND ARE_PARKED_VEHICLE_MODELS_STREAMED()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BODYBUILDER_DATA_LOADED_PEDS_ONLY(ASS_TARGET_DATA& targetData)
	REQUEST_MODEL(targetData.targetEnum)
	REQUEST_MODEL(A_F_Y_BEACH_01)
	REQUEST_MODEL(A_F_Y_Fitness_02)
	
	IF HAS_MODEL_LOADED(targetData.targetEnum)
	AND HAS_MODEL_LOADED(A_F_Y_BEACH_01)
	AND HAS_MODEL_LOADED(A_F_Y_Fitness_02)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_BODYBUILDER_PEDS_AND_MODELS_AS_NO_LONGER_NEEDED(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	IF targetData.targetEnum != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(targetData.targetEnum)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_BEACH_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Fitness_02)
	
	IF DOES_ENTITY_EXIST(args.myTarget)
		SET_PED_AS_NO_LONGER_NEEDED(args.myTarget)
	ENDIF
	
	INT idx
	FOR idx = 0 TO NUM_BYSTANDER_PEDS - 1
		IF DOES_ENTITY_EXIST(bystanders[idx].ped)
			SET_PED_AS_NO_LONGER_NEEDED(bystanders[idx].ped)
		ENDIF
	ENDFOR
ENDPROC

PROC INIT_LEAVE_AREA_LOCATIONS()
	vTargetLocation[0] = << -1208.4615, -1559.6609, 3.6087 >> // Body Builder
	vTargetLocation[1] = <<-2164.7234, -405.4532, 12.3954>>	// Yacht guy
	vTargetLocation[2] = <<-693.6130, 233.9349, 79.7011>> // window washer
ENDPROC

PROC INIT_YACHT_DATA_DONT_STREAM_PEDS(ASS_TARGET_DATA& targetData)
	targetData.targetEnum = A_M_M_BEACH_01
	targetData.vTargetPos = << -2211.7424, -587.6183, -0.4 >>
	targetData.fTargetHead = 222.5630
	targetData.bNonTemp = TRUE
	targetData.otherPedEnum = A_F_Y_BEACH_01	//girl on yacht
	targetData.vOtherPedPos = << -2211.7424, -587.6183, -0.4 >>
	
	 //sunbathing chick next to jetski
	targetData.auxPedEnum = A_F_Y_BEACH_01	// MAKE SURE TO ADD A STREAMED MODEL REQUESTIF THIS CHANGES FROM A_F_Y_BEACH_01!!! Currently not adding to streamed model list since it is the same as targetData.otherPedEnum
	targetData.vAuxPedPos = << -2155.47949, -459.90463, 2.53347 >>
	targetData.fAuxPedHead = 129.6666 
	
	targetData.vehEnum =  MARQUIS
	targetData.vVehPos = << -2211.7424, -587.6183, -0.4 >>
	targetData.fVehHead = 222.5630
	
	targetData.vDestPos = << -2172.9009, -395.7628, 12.3490 >> //<< -2178.8555, -410.6809, 12.1595 >>
	targetData.vWarpPos = << -2178.8555, -410.6809, 12.1595 >>
	targetData.vCutscenePos = << -2171.4619, -446.2333, 5.9263 >>
	targetData.vCutsceneHead = << -5.0921, -0.0249, 163.0108 >>
	targetData.fCutsceneDoF = 35
	
	targetData.sAnimDict[0] = "oddjobs@assassinate@multi@yachttarget@lapdance"
	targetData.sAnimDict[1] = "amb@world_human_sunbathe@female@back@idle_a"
	targetData.sAnimDict[2] = "veh@boat@marquis@rps@enter_exit"
	
	aggroArgs.iShotIRange = 5
	
//	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.targetEnum)
//	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.otherPedEnum)
	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.vehEnum)
	ADD_STREAMED_MODEL(targetData.streamedModels, SEASHARK)
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[0])
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[1])
	
//	LOAD_STREAM("MULT_ASSAS_WATER_SPLASH_master")
//	PRINTLN("REQUESTING LOAD_STREAM for 'MULT_ASSAS_WATER_SPLASH_master'")
	
	fFailDist = 400
ENDPROC

FUNC BOOL IS_YACHT_DATA_LOADED_NO_PEDS(ASS_TARGET_DATA& targetData)
	IF ARE_MODELS_STREAMED(targetData.streamedModels)
	AND ARE_ANIMS_STREAMED(sLoadQueue)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_YACHT_DATA_LOADED_PEDS_ONLY(ASS_TARGET_DATA& targetData)
	REQUEST_MODEL(targetData.targetEnum)
	REQUEST_MODEL(targetData.otherPedEnum)
	
	IF HAS_MODEL_LOADED(targetData.targetEnum)
	AND HAS_MODEL_LOADED(targetData.otherPedEnum)
//	AND LOAD_STREAM("MULT_ASSAS_WATER_SPLASH_master")
		PRINTLN("IS_YACHT_DATA_LOADED_PEDS_ONLY RETURNED TRUE!")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SET_YACHT_PEDS_AND_MODELS_AS_NO_LONGER_NEEDED(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	IF targetData.targetEnum != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(targetData.targetEnum)
	ENDIF
	IF targetData.otherPedEnum != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(targetData.otherPedEnum)
	ENDIF
	
	IF DOES_ENTITY_EXIST(args.myTarget)
		SET_PED_AS_NO_LONGER_NEEDED(args.myTarget)
	ENDIF
	IF DOES_ENTITY_EXIST(args.myOtherPed)
		SET_PED_AS_NO_LONGER_NEEDED(args.myOtherPed)
	ENDIF
	IF DOES_ENTITY_EXIST(args.myAuxPed)
		SET_PED_AS_NO_LONGER_NEEDED(args.myAuxPed)
	ENDIF
	
ENDPROC

PROC INIT_WASHER_DATA_DONT_STREAM_PEDS(ASS_TARGET_DATA& targetData)
	targetData.targetEnum = S_M_Y_WinClean_01
	targetData.propEnum[0] = Prop_BMU_02_LD 		// the platform
	targetData.propEnum[1] = Prop_BMU_02_LD_Sup  	// the anchors
	targetData.vPropPos[0] = << -643.750, 305.280, 99.200 >>	//<< -719.3, 269.5, 123.8 >>
	targetData.fPropHead[0] = 175.3
	targetData.vPropPos[1] = << -643.51, 308.26, 138.62 >>	// << -722.0, 268.2, 130.3 >>
	targetData.fPropHead[1] = 175 					// 297
	targetData.vTargetPos = << -642.6985, 305.8230, 111.5572 >>// << -720.8910, 271.1867, 124.7611 >>
	targetData.fTargetHead = 349.4722
	targetData.vDestPos = << -662.15826, 255.09444, 80.40974 >> //<< -681.2297, 274.3849, 80.4044 >>
	targetData.vCutscenePos = << -660.3444, 184.7394, 80.3851 >>
	targetData.vCutsceneHead = << 12.3694, 0.0001, -20.5050 >>
	targetData.fCutsceneDoF = 35
	targetData.sAnimDict[0] = "oddjobs@assassinate@multi@windowwasher"
//	targetData.sAnimDict[1] = "amb@world_human_stand_impatient@male@no_sign@base"
	targetData.vWarpPos = << -662.6647, 242.4725, 80.3006 >>
	targetData.bNonTemp = TRUE
	
	aggroArgs.iShotIRange = 15
	
//	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.targetEnum)
	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.propEnum[0])
	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.propEnum[1])
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[0])
//	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[1])
	
	LOAD_STREAM("WINDOWWASHERFALL_MASTER")
	PRINTLN("REQUESTING LOAD_STREAM for 'WINDOWWASHERFALL_MASTER'")
	
	fFailDist = 500
ENDPROC

FUNC BOOL IS_WASHER_DATA_LOADED_NO_PEDS(ASS_TARGET_DATA& targetData)
	IF ARE_MODELS_STREAMED(targetData.streamedModels)
	AND ARE_ANIMS_STREAMED(sLoadQueue)
//	AND LOAD_STREAM("WINDOWWASHERFALL_MASTER")
		PRINTLN("IS_WASHER_DATA_LOADED_NO_PEDS RETURNED TRUE!")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_WASHER_DATA_LOADED_PEDS_ONLY(ASS_TARGET_DATA& targetData)
	REQUEST_MODEL(targetData.targetEnum)
	
	IF HAS_MODEL_LOADED(targetData.targetEnum)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_WASHER_PEDS_AND_MODELS_AS_NO_LONGER_NEEDED(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	IF targetData.targetEnum != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(targetData.targetEnum)
	ENDIF
	
	IF DOES_ENTITY_EXIST(args.myTarget)
		SET_PED_AS_NO_LONGER_NEEDED(args.myTarget)
	ENDIF
	
ENDPROC

PROC INIT_BIKER_DATA_DONT_STREAM_PEDS(ASS_TARGET_DATA& targetData)
	targetData.targetEnum = U_M_Y_SBike
	targetData.vTargetPos = << -1406.7675, 738.4614, 182.4823 >>
	targetData.fTargetHead = 125.2133
	targetData.bNonTemp = TRUE
	targetData.weapTarget = WEAPONTYPE_PISTOL
	targetData.vDestPos = << -1408.1696, 742.0872, 182.0377 >>	//<< -1265.9923, 866.4904, 190.0261 >>
	targetData.vehEnum =  BATI
	targetData.vVehPos = << -1408.1696, 742.0872, 182.0377 >>
	targetData.fVehHead = 15.0077
	targetData.vWarpPos = << -1382.6913, 744.9668, 182.2854 >>
	targetData.vCutscenePos = << -1402.0939, 738.1398, 183.9362 >>
	targetData.vCutsceneHead = << -0.2429, -0.0000, 76.3241 >>
	targetData.fCutsceneDoF = 35
	
	aggroArgs.iShotIRange = 5
	
//	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.targetEnum)
	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.vehEnum)
	ADD_STREAMED_MODEL(targetData.streamedModels, BJXL)
	ADD_STREAMED_MODEL(targetData.streamedModels, CAMPER)
	
	fFailDist = 250
ENDPROC

FUNC BOOL IS_BIKER_DATA_LOADED_NO_PEDS(ASS_TARGET_DATA& targetData)
	IF ARE_MODELS_STREAMED(targetData.streamedModels)
	AND ARE_ANIMS_STREAMED(sLoadQueue)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BIKER_DATA_LOADED_PEDS_ONLY(ASS_TARGET_DATA& targetData)
	REQUEST_MODEL(targetData.targetEnum)
	
	IF HAS_MODEL_LOADED(targetData.targetEnum)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_BIKER_PEDS_AND_MODELS_AS_NO_LONGER_NEEDED(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	IF targetData.targetEnum != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(targetData.targetEnum)
	ENDIF
	
	IF DOES_ENTITY_EXIST(args.myTarget)
		SET_PED_AS_NO_LONGER_NEEDED(args.myTarget)
	ENDIF
ENDPROC

PROC UPDATE_MISSION_TIMER(INT& startTime, INT& iTimePassed, FLOAT& fTimeLimit, BOOL& bTimerStarted, BOOL& bTimeExpired)     
	
	// Grab starting game time
    IF NOT bTimerStarted
        startTime = GET_GAME_TIMER()
		PRINTLN("START TIME = ", startTime)
		bTimerStarted = TRUE
    ENDIF

    // Variable time - include iTimeOffsetReplay to address TimeRemaining when a player restarts a mission from a checkpoint so they don't start with full time)
	iTimePassed = GET_GAME_TIMER() - startTime
//	PRINTLN("iTimePassed = ", iTimePassed)
	 
	iTimeToDisplayOnTimer = ROUND(fTimeLimit) - iTimePassed
//	PRINTLN("iTimeToDisplayOnTimer = ", iTimeToDisplayOnTimer)
		
	IF iTimeToDisplayOnTimer < 60000
		IF iTimeToDisplayOnTimer <= 11000 AND iTimeToDisplayOnTimer >= 5000
			IF IS_TIMER_STARTED(tBeepTimer)
				IF GET_TIMER_IN_SECONDS(tBeepTimer) > 1
					PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
					RESTART_TIMER_NOW(tBeepTimer)
					PRINTLN("PLAYING BEEP")
				ENDIF
			ELSE
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				START_TIMER_NOW(tBeepTimer)
				PRINTLN("STARTING TIMER - tBeepTimer - 01")
			ENDIF
		ELIF iTimeToDisplayOnTimer <= 5000 AND iTimeToDisplayOnTimer > 0
			IF IS_TIMER_STARTED(tBeepTimer)
				IF GET_TIMER_IN_SECONDS(tBeepTimer) > 0.5
					PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
					RESTART_TIMER_NOW(tBeepTimer)
					PRINTLN("PLAYING BEEP")
				ENDIF
			ELSE
				START_TIMER_NOW(tBeepTimer)
				PRINTLN("STARTING TIMER - tBeepTimer - 02")
			ENDIF
		ENDIF
		
		DRAW_GENERIC_TIMER(iTimeToDisplayOnTimer, "ASS_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
	ELSE
		DRAW_GENERIC_TIMER(iTimeToDisplayOnTimer, "ASS_TIME")
	ENDIF
	
	IF iTimePassed > fTimeLimit
		
		PLAY_SOUND_FRONTEND(-1, "TIMER_STOP", "HUD_MINI_GAME_SOUNDSET")
		PRINTLN("PLAYING - TIMER_STOP")
		
        bTimeExpired = TRUE
		PRINTLN("bTimeExpired = TRUE")
    ENDIF
ENDPROC


//initialize all the necessities for the mission
PROC INIT_CURRENT_TARGET_DATA(ASS_TARGET_DATA& targetData)
	SWITCH curTarget
		CASE ASSASSIN_TARGET_BODYBUILDER
			INIT_BODYBUILDER_DATA_DONT_STREAM_PEDS(targetData)
			REQUEST_PARKED_VEHICLE_MODELS_FOR_GAS_STATION()
		BREAK
		CASE ASSASSIN_TARGET_YACHT
			INIT_YACHT_DATA_DONT_STREAM_PEDS(targetData)
		BREAK
		CASE ASSASSIN_TARGET_WASHER
			INIT_WASHER_DATA_DONT_STREAM_PEDS(targetData)
		BREAK
		CASE ASSASSIN_TARGET_BIKER
			INIT_BIKER_DATA_DONT_STREAM_PEDS(targetData)
		BREAK	
	ENDSWITCH
	REQUEST_ALL_MODELS(targetData.streamedModels)
	REQUEST_ALL_ANIMS(targetData.streamedAnims, sLoadQueue)
ENDPROC


FUNC BOOL IS_CURRENT_TARGET_DATA_LOADED_NO_PEDS(ASS_TARGET_DATA& targetData)
	PRINTLN("waiting for current target data to load")
	SWITCH curTarget
		CASE ASSASSIN_TARGET_WASHER
			IF NOT IS_WASHER_DATA_LOADED_NO_PEDS(targetData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE ASSASSIN_TARGET_YACHT
			IF NOT IS_YACHT_DATA_LOADED_NO_PEDS(targetData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE ASSASSIN_TARGET_BIKER
			IF NOT IS_BIKER_DATA_LOADED_NO_PEDS(targetData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE ASSASSIN_TARGET_BODYBUILDER
			IF NOT IS_BODYBUILDER_DATA_LOADED_NO_PEDS(targetData)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	PRINTLN("current target data loaded! - PEDS ONLY!")
	RETURN TRUE
ENDFUNC

//handles objective print messages
PROC DO_CURRENT_TARGET_OBJECTIVE_PRINT()
	SWITCH curTarget
		CASE ASSASSIN_TARGET_BODYBUILDER
			IF NOT bLocatePrinted
				PRINT_NOW("ASS_ML_T01", DEFAULT_GOD_TEXT_TIME, 1)
				REGISTER_PRINT_DELAY(printTimer, "ASS_TIME_TK")
				bLocatePrinted = TRUE
			ENDIF
		BREAK
		CASE ASSASSIN_TARGET_YACHT
			IF NOT bLocatePrinted
				PRINT_NOW("ASS_ML_T01", DEFAULT_GOD_TEXT_TIME, 1)
				bLocatePrinted = TRUE
			ENDIF
		BREAK
		CASE ASSASSIN_TARGET_WASHER
			IF NOT bLocatePrinted
				PRINT_NOW("ASS_ML_T01", DEFAULT_GOD_TEXT_TIME, 1)
				bLocatePrinted = TRUE
			ENDIF
		BREAK
		CASE ASSASSIN_TARGET_BIKER
			IF NOT bLocatePrinted
				PRINT_NOW("ASS_ML_T01", DEFAULT_GOD_TEXT_TIME, 1)
				bLocatePrinted = TRUE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//handles objective print messages
PROC DO_SET_CHECKPOINT()
	SWITCH curTarget
		CASE ASSASSIN_TARGET_BODYBUILDER
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "assassin_multi_locate_bodybuilder")
			OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(viReplayVehicle)
		BREAK
		CASE ASSASSIN_TARGET_YACHT
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "assassin_multi_locate_yacht_target")
		BREAK
		CASE ASSASSIN_TARGET_WASHER
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "assassin_multi_locate_window_washer")
		BREAK
		CASE ASSASSIN_TARGET_BIKER
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "assassin_multi_locate_biker")
		BREAK
	ENDSWITCH
ENDPROC

PROC CREATE_CURRENT_TARGET_BLIP(ASS_ARGS& args)
	IF DOES_BLIP_EXIST(args.myTargetBlip)
		REMOVE_BLIP(args.myTargetBlip)
	ENDIF
	args.myTargetBlip = CREATE_BLIP_ON_ENTITY(args.myTarget, FALSE)
	SET_BLIP_COLOUR(args.myTargetBlip, BLIP_COLOUR_RED)
	SET_BLIP_ROUTE(args.myTargetBlip, FALSE)
ENDPROC


//checks to see if the player has a wanted level and is within fDistToCheck of pedToCheck
FUNC BOOL ARE_COPS_NEAR_TARGET_WHILE_PLAYER_IS_WANTED(PED_INDEX pedToCheck, FLOAT fDistToCheck)
	VECTOR vPedPos
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		IF NOT IS_PED_INJURED(pedToCheck)
			vPedPos = GET_ENTITY_COORDS(pedToCheck)
			IF IS_COP_PED_IN_AREA_3D(<< vPedPos.x - fDistToCheck, vPedPos.y - fDistToCheck, vPedPos.z - fDistToCheck>>,
					<< vPedPos.x + fDistToCheck, vPedPos.y + fDistToCheck, vPedPos.z + fDistToCheck>>)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PLAY_FRANKLIN_LINE_REACTING_TO_POLICE_IF_WANTED()
	IF NOT bPoliceChaseLinePlayed
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "SPOT_POLICE")
				bPoliceChaseLinePlayed = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//plays a random anim for the bystander ped depending on their state
PROC PLAY_RANDOM_BYSTANDER_ANIM(PED_INDEX ped, BOOL bWatchingTarget = TRUE)
	INT randInt

	IF NOT IS_PED_RAGDOLL(ped)
		IF bWatchingTarget
			IF IS_PED_MALE(ped)
				randInt = GET_RANDOM_INT_IN_RANGE() % 3
				IF randInt = 0
					TASK_PLAY_ANIM(ped, "oddjobs@assassinate@multi@" , "react_big_variations_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1 , AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE())
				ELIF randInt = 1
					TASK_PLAY_ANIM(ped, "oddjobs@assassinate@multi@", "react_big_variations_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1 , AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE())
				ELSE
					TASK_PLAY_ANIM(ped, "oddjobs@assassinate@multi@", "react_big_variations_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1 , AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE())
				ENDIF
			ELSE
				randInt = GET_RANDOM_INT_IN_RANGE() % 2
				IF randInt = 0
					TASK_PLAY_ANIM(ped, "oddjobs@assassinate@multi@", "idle_a_pros", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1 , AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE())
				ELSE
					TASK_PLAY_ANIM(ped, "oddjobs@assassinate@multi@", "idle_a_pros", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1 , AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE())
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//creates peds that are spawned around bodybuilder target and has them play a random anim
PROC CREATE_BYSTANDER_PEDS()
	
	bystanders[0].vSpawnPos = <<-1209.0311, -1553.9404, 3.3721>>
	bystanders[1].vSpawnPos = <<-1214.1919, -1558.5959, 3.3476>>
	
	bystanders[0].goal = BG_IDLE
	bystanders[1].goal = BG_IDLE
	
	bystanders[0].ped = CREATE_PED(PEDTYPE_CIVFEMALE, A_F_Y_Fitness_02, bystanders[0].vSpawnPos, 169.9513)
	PLAY_RANDOM_BYSTANDER_ANIM(bystanders[0].ped)
	PRINTLN("CREATING FIRST BYSTANDER")
	
	bystanders[1].ped = CREATE_PED(PEDTYPE_CIVFEMALE, A_F_Y_BEACH_01, bystanders[1].vSpawnPos, 257.4617)
	PLAY_RANDOM_BYSTANDER_ANIM(bystanders[1].ped)
	PRINTLN("CREATING SECOND BYSTANDER")
	
	SET_PED_COMBAT_ATTRIBUTES(bystanders[0].ped, CA_ALWAYS_FLEE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(bystanders[0].ped, CA_USE_VEHICLE, FALSE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(bystanders[0].ped, TRUE)
	
	SET_PED_COMBAT_ATTRIBUTES(bystanders[1].ped, CA_ALWAYS_FLEE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(bystanders[1].ped, CA_USE_VEHICLE, FALSE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(bystanders[1].ped, TRUE)
	
ENDPROC

////have the bystanders leave the scene periodically if not already panicking
//PROC UPDATE_BYSTANDERS_LEAVING_AREA(INT iMaxNumPedsToLeave, INT iNumSecondsBetweenLeaving)
//	INT iPedNum
//	
//	//only have a max of iMaxNumPedsToLeave peds leave the scene so there is still a decent crowd at any given time as long as panic hasnt been triggered
//	IF iNumBystandersLeavingArea <= iMaxNumPedsToLeave
//		IF NOT IS_TIMER_STARTED(bystanderLeaveTimer)
//			RESTART_TIMER_NOW(bystanderLeaveTimer)
//		ELSE
//			IF GET_TIMER_IN_SECONDS(bystanderLeaveTimer) > iNumSecondsBetweenLeaving
//				iPedNum = GET_RANDOM_INT_IN_RANGE() % 11	//only search the first group of peds spawned near the gym
//				IF NOT IS_PED_INJURED(bystanders[iPedNum].ped)
//					IF bystanders[iPedNum].goal <> BG_LEAVE
//					AND GET_SCRIPT_TASK_STATUS(bystanders[iPedNum].ped, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
//						bystanders[iPedNum].goal = BG_LEAVE
//						iNumBystandersLeavingArea ++
//					ENDIF
//				ENDIF
//				
//				RESTART_TIMER_NOW(bystanderLeaveTimer)
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC


//PROC UPDATE_BYSTANDERS_ARRIVING(INT iNumSecondsBeforeTasking)
//	INT iPedNum
//	
//	IF NOT IS_TIMER_STARTED(bystanderArriveTimer)
//		RESTART_TIMER_NOW(bystanderArriveTimer)
//	ELSE
//		IF GET_TIMER_IN_SECONDS(bystanderArriveTimer) > iNumSecondsBeforeTasking
//			iPedNum = GET_RANDOM_INT_IN_RANGE(8, NUM_BYSTANDER_PEDS)
//			IF NOT IS_PED_INJURED(bystanders[iPedNum].ped)
//				IF bystanders[iPedNum].goal = BG_IDLE
//					CLEAR_PED_TASKS(bystanders[iPedNum].ped)
//					bystanders[iPedNum].goal = BG_WATCH
//					PRINTLN("bystanders[", iPedNum, "] = BG_WATCH")
//				ENDIF
//			ENDIF
//			
//			RESTART_TIMER_NOW(bystanderArriveTimer)
//		ENDIF
//	ENDIF
//ENDPROC

//update bystanders - only update one bystander per frame
PROC UPDATE_BYSTANDERS_STATES()
	INT idx
	SEQUENCE_INDEX tempSeq
	
	IF iBystanderCounter < NUM_BYSTANDER_PEDS
		idx = iBystanderCounter
	ELSE
		iBystanderCounter = 0
	ENDIF
	
	IF NOT IS_PED_INJURED(bystanders[idx].ped)
		SWITCH bystanders[idx].goal
			CASE BG_WATCH
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(bystanders[idx].ped, bystanders[idx].vSpawnPos) > 2
					IF GET_SCRIPT_TASK_STATUS(bystanders[idx].ped, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
						CLEAR_PED_TASKS(bystanders[idx].ped)
						OPEN_SEQUENCE_TASK(tempSeq)
							TASK_GO_TO_COORD_ANY_MEANS(NULL, bystanders[idx].vSpawnPos, PEDMOVE_WALK, NULL)
							TASK_TURN_PED_TO_FACE_COORD(NULL, vFlexPos)
						CLOSE_SEQUENCE_TASK(tempSeq)
						TASK_PERFORM_SEQUENCE(bystanders[idx].ped, tempSeq)
						CLEAR_SEQUENCE_TASK(tempSeq)
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(bystanders[idx].ped, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(bystanders[idx].ped, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK
						PLAY_RANDOM_BYSTANDER_ANIM(bystanders[idx].ped)
					ENDIF
				ENDIF
			BREAK
			CASE BG_IDLE
				IF GET_SCRIPT_TASK_STATUS(bystanders[idx].ped, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK
					PLAY_RANDOM_BYSTANDER_ANIM(bystanders[idx].ped, FALSE)
				ENDIF
			BREAK
			CASE BG_LEAVE
				IF GET_SCRIPT_TASK_STATUS(bystanders[idx].ped, SCRIPT_TASK_WANDER_STANDARD) <> PERFORMING_TASK
					TASK_WANDER_STANDARD(bystanders[idx].ped)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

	//increment this value
	iBystanderCounter ++
ENDPROC

//aggro checks for bystanders watching the weightlifter
FUNC BOOL HAS_PLAYER_ATTACKED_BYSTANDERS(BOOL& bBoolToFlip)
	//only check one ped per frame
	IF DO_AGGRO_CHECK(bystanders[iFrameCounter].ped, NULL, aggroArgs, aggroReason, FALSE, FALSE)	
		bBoolToFlip = TRUE
		RETURN TRUE
	ENDIF
	
	iFrameCounter ++
	IF iFrameCounter > NUM_BYSTANDER_PEDS - 1
		iFrameCounter = 0
	ENDIF
	
	RETURN FALSE
ENDFUNC



//player is most likely in the gym area poiting a gun at the target - this function will ensure that panic is triggered even if the target doesn't see player targetting him
FUNC BOOL IS_PLAYER_SEEN_THREATENING_TARGET_BY_BYSTANDERS(PED_INDEX pedTarget, structTimer& timerToCheck, FLOAT fTimeToCheck, FLOAT fDistToCheck, BOOL& bBoolToFlip)	
	IF GET_PLAYER_DISTANCE_FROM_ENTITY(pedTarget) < fDistToCheck
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
			IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedTarget)
			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedTarget)
				IF NOT IS_TIMER_STARTED(timerToCheck)
					RESTART_TIMER_NOW(timerToCheck)
				ELSE
					IF GET_TIMER_IN_SECONDS(timerToCheck) >= fTimeToCheck
						bBoolToFlip = TRUE
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				RESTART_TIMER_NOW(timerToCheck)
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_RANDOM_COORDINATE()
	VECTOR vToReturn
	INT iRand
	
	iRand = GET_RANDOM_INT_IN_RANGE() % 6
	SWITCH iRand
		CASE 0
			vToReturn = << -1328.2563, -1595.2444, 3.4691 >>
			PRINTLN("vToReturn = ", vToReturn)
		BREAK
		CASE 1
			vToReturn = << -1226.8425, -1627.8441, 3.0966 >>
			PRINTLN("vToReturn = ", vToReturn)
		BREAK
		CASE 2
			vToReturn = << -1277.8651, -1499.7965, 3.4114 >>
			PRINTLN("vToReturn = ", vToReturn)
		BREAK
		CASE 3
			vToReturn = << -1245.8002, -1473.1765, 3.2454 >>
			PRINTLN("vToReturn = ", vToReturn)
		BREAK
		CASE 4
			vToReturn = << -1153.4226, -1599.5081, 3.3860 >>
			PRINTLN("vToReturn = ", vToReturn)
		BREAK
		CASE 5
			vToReturn = << -1272.2239, -1612.4363, 3.0916 >>
			PRINTLN("vToReturn = ", vToReturn)
		BREAK
		DEFAULT
			vToReturn = << -1272.2239, -1612.4363, 3.0916 >>
			PRINTLN("DEFAULT: vToReturn = ", vToReturn)
		BREAK
	ENDSWITCH
	
	RETURN vToReturn
ENDFUNC

//tasks all bystander peds around weightlifter target to flee
PROC TASK_BYSTANDERS_TO_FLEE(BOOL bReleasePeds = FALSE)
	INT tempInt
	INT iRandInt
	
	IF NOT bTaskedBystandersToFlee
		FOR tempInt = 0 TO NUM_BYSTANDER_PEDS - 1
			IF NOT IS_PED_INJURED(bystanders[tempInt].ped)
				iRandInt = GET_RANDOM_INT_IN_RANGE() % 3
				IF (iRandInt = 0) OR (iRandInt = 1)
					IF GET_SCRIPT_TASK_STATUS(bystanders[tempInt].ped, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
						TASK_SMART_FLEE_PED(bystanders[tempInt].ped, PLAYER_PED_ID(), 200, -1)
						SET_PED_KEEP_TASK(bystanders[tempInt].ped, TRUE)
						PRINTLN("TASKING PED TO FLEE, INDEX = ", tempInt)
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(bystanders[tempInt].ped, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
						TASK_GO_TO_COORD_ANY_MEANS(bystanders[tempInt].ped, GET_RANDOM_COORDINATE(), PEDMOVEBLENDRATIO_RUN, NULL)
						SET_PED_KEEP_TASK(bystanders[tempInt].ped, TRUE)
						PRINTLN("TASKING PED TO GO TO COORDS, INDEX = ", tempInt)
					ENDIF
				ENDIF
			ENDIF
			
			IF bReleasePeds
				SET_PED_AS_NO_LONGER_NEEDED(bystanders[tempInt].ped)
			ENDIF
		ENDFOR
		
		bTaskedBystandersToFlee = TRUE
	ENDIF
ENDPROC

//detects to see if a bullet or explosion has impacted in a volume around where the ropes exist for the washer target and detaches the rope if hit
PROC HANDLE_SHOT_ROPE()
	IF curTarget = ASSASSIN_TARGET_WASHER
		IF NOT DOES_ROPE_EXIST(ropeWindowA)
//			PRINTLN("ropeWindowA DOES NOT EXIST")
		
	//		APPLY_FORCE_TO_ENTITY(objectToDetach, APPLY_TYPE_EXTERNAL_IMPULSE, <<-5,0,-4>>, <<-5,0,0>>, 0, TRUE, TRUE, TRUE)
			IF NOT IS_PED_INJURED(assArgs.myTarget)
				IF NOT bRopeDetached
					SET_ENTITY_LOAD_COLLISION_FLAG(assArgs.myTarget, TRUE)
					SET_PED_TO_RAGDOLL(assArgs.myTarget, 1, 1, TASK_RELAX)
				ENDIF
			ENDIF
			
//			IF LOAD_STREAM("WINDOWWASHERFALL_MASTER")

//				ODDJOB_STOP_SOUND(iSoundIdMulti)
//				ODDJOB_PLAY_SOUND("WINDOWWASHERFALL_MASTER", iSoundIdMulti, FALSE, NULL, -648.59, 305.73, 120)
				IF LOAD_STREAM("WINDOWWASHERFALL_MASTER")
					PLAY_STREAM_FROM_POSITION(<< -648.59, 305.73, 120 >>)
					PRINTLN("WINDOWWASHERFALL_MASTER PLAYING from ropeWindowA!!!!!")
				ELSE
					PRINTLN("WINDOWWASHERFALL_MASTER NOT PLAYING from ropeWindowA")
				ENDIF
//			ELSE
//				PRINTLN("WINDOWWASHERFALL_MASTER NOT LOADED for ropeWindowA!!!!!")
//			ENDIF
			
	//		SET_PED_TO_RAGDOLL_WITH_FALL(pedToRagdoll, 500, 1000, TYPE_FROM_HIGH, -GET_ENTITY_FORWARD_VECTOR(pedToRagdoll), 85, <<0,0,0>>, <<0,0,0>>)
	//		SET_CAM_PARAMS(camKill, << -652.6349, 304.7316, 136.5523 >>, << -23.1959, -0.0000, -84.5266 >>, 35)
			
			IF IS_FIRST_PERSON_AIM_CAM_ACTIVE()
				SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(1.0)
			ENDIF
			bRopeDetached = TRUE
			
		ELIF NOT DOES_ROPE_EXIST(ropeWindowB)
//			PRINTLN("ropeWindowB DOES NOT EXIST")
		
	//		APPLY_FORCE_TO_ENTITY(objectToDetach, APPLY_TYPE_EXTERNAL_IMPULSE, <<5,0,-4>>, <<5,0,0>>, 0, TRUE, TRUE, TRUE)
			IF NOT IS_PED_INJURED(assArgs.myTarget)
				IF NOT bRopeDetached
					SET_ENTITY_LOAD_COLLISION_FLAG(assArgs.myTarget, TRUE)
					SET_PED_TO_RAGDOLL(assArgs.myTarget, 1, 1, TASK_RELAX)
				ENDIF
			ENDIF
			
//			IF LOAD_STREAM("WINDOWWASHERFALL_MASTER")

//				ODDJOB_STOP_SOUND(iSoundIdMulti)
//				ODDJOB_PLAY_SOUND("WINDOWWASHERFALL_MASTER", iSoundIdMulti, FALSE, NULL, -638.19312, 306.01401, 121.42364)
				IF LOAD_STREAM("WINDOWWASHERFALL_MASTER")
					PLAY_STREAM_FROM_POSITION(<< -638.19312, 306.01401, 121.42364 >>)		//sound of wire breaking
					PRINTLN("WINDOWWASHERFALL_MASTER PLAYING from ropeWindowB!!!!!")
				ELSE
					PRINTLN("WINDOWWASHERFALL_MASTER NOT PLAYING from ropeWindowB")
				ENDIF
				
				PRINTLN("WINDOWWASHERFALL_MASTER PLAYING from ropeWindowB!!!!!")
//			ELSE
//				PRINTLN("WINDOWWASHERFALL_MASTER NOT LOADED for ropeWindowB !!!!!")
//			ENDIF
			
	//		SET_PED_TO_RAGDOLL_WITH_FALL(pedToRagdoll, 500, 1000, TYPE_FROM_HIGH, -GET_ENTITY_FORWARD_VECTOR(pedToRagdoll), 85, <<0,0,0>>, <<0,0,0>>)
	//		SET_CAM_PARAMS(camKill, << -633.9470, 302.5992, 138.1284 >>, << -34.5233, 0.0000, 73.4838 >>, 35)
			
			IF IS_FIRST_PERSON_AIM_CAM_ACTIVE()
				SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(1.0)
			ENDIF
			bRopeDetached = TRUE
		ENDIF
	ENDIF
ENDPROC


////make the target loop through a series of window washing animations if the player has not triggered panic in the Window Washer assassination
//PROC UPDATE_WASHER_ANIMS(ASS_ARGS& args)
////	SEQUENCE_INDEX tempSeq
//	
//	IF NOT bTaskedWindowWasherAnims
//		IF NOT bRopeDetached AND bRopeWindingDone
//			IF NOT IS_PED_INJURED(args.myTarget)
//				IF NOT IS_PED_RAGDOLL(args.myTarget)
//				OR NOT IS_ENTITY_IN_AIR(args.myTarget)
////					IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
////						CLEAR_SEQUENCE_TASK(tempSeq)
////						OPEN_SEQUENCE_TASK(tempSeq)
//							TASK_PLAY_ANIM(args.myTarget, "oddjobs@assassinate@multi@windowwasher", "_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
////						CLOSE_SEQUENCE_TASK(tempSeq)
////						TASK_PERFORM_SEQUENCE(args.myTarget, tempSeq)
////						CLEAR_SEQUENCE_TASK(tempSeq)
//						
//						bTaskedWindowWasherAnims = TRUE
////					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

//have target cycle through some flexing poses until the scnearios in the gym are are working
PROC UPDATE_BODYBUILDER_ANIMS(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	SEQUENCE_INDEX tempSeq
	FLOAT fTempHead
	
	SWITCH curBBAnimStage
		CASE BB_ANIM_INTRO
			IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				fTempHead = GET_ENTITY_HEADING(args.myTarget)
				
				//make sure target is facing the right way before tasking to cycle through his anims
				IF fTempHead < targetData.fTargetHead - 20 
				OR fTempHead > targetData.fTargetHead + 20
					IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_ACHIEVE_HEADING) <> PERFORMING_TASK
						TASK_ACHIEVE_HEADING(args.myTarget, targetData.fTargetHead)
					ENDIF
				ELSE
					OPEN_SEQUENCE_TASK(tempSeq)
						TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@multi@", "idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(tempSeq)
					TASK_PERFORM_SEQUENCE(args.myTarget, tempSeq)
					CLEAR_SEQUENCE_TASK(tempSeq)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//tells ped to turn and face the player and play the appropriate anim
PROC FACE_PLAYER_AND_PLAY_ANIM(PED_INDEX ped, STRING animDict, STRING animString)
	SEQUENCE_INDEX tempSeq
	
	CLEAR_PED_TASKS(ped)
	CLEAR_SEQUENCE_TASK(tempSeq)
	OPEN_SEQUENCE_TASK(tempSeq)
		TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
		TASK_PLAY_ANIM(NULL, animDict, animString, SLOW_BLEND_IN, SLOW_BLEND_OUT)
	CLOSE_SEQUENCE_TASK(tempSeq)
	TASK_PERFORM_SEQUENCE(ped, tempSeq)
	CLEAR_SEQUENCE_TASK(tempSeq)
ENDPROC

//gets closest bystander ped to VECTOR and distance passed in 
FUNC PED_INDEX GET_CLOSEST_BYSTANDER(BOOL bSearchForMale, VECTOR vSearchLoc, FLOAT fProbeDist)
	INT tempInt
	FLOAT fDist = fProbeDist
	FLOAt fCurDist
	PED_INDEX pedToReturn
	
	FOR tempInt = 0 TO NUM_BYSTANDER_PEDS - 1
		IF NOT IS_PED_INJURED(bystanders[tempInt].ped)
			IF bSearchForMale AND IS_PED_MALE(bystanders[tempInt].ped)
			OR ( NOT bSearchForMale AND NOT IS_PED_MALE(bystanders[tempInt].ped) )
				fCurDist = GET_ENTITY_DISTANCE_FROM_LOCATION(bystanders[tempInt].ped, vSearchLoc)
				IF fCurDist <= fDist
					pedToReturn = bystanders[tempInt].ped
					fDist = fCurDist
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN pedToReturn
ENDFUNC

//have bodybuilder start talking shit and reacting with anims to the player as he warns the player for getting too close
PROC MANAGE_BODYBUILDER_WARNING_BEHAVIOR(ASS_ARGS& args)
	IF NOT IS_PED_INJURED(args.myTarget)
		
		//turn and face player and play line
		IF iWarningSpeechStage = 0
			FACE_PLAYER_AND_PLAY_ANIM(args.myTarget, "misscommon@response", "bring_it_on")
			RESTART_TIMER_NOW(warningTimer)
			iWarningSpeechStage ++
			PRINTLN("iWarningSpeechStage = ", iWarningSpeechStage)
		ELIF iWarningSpeechStage = 1
			IF IS_ENTITY_PLAYING_ANIM(args.myTarget, "misscommon@response", "bring_it_on")
				CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bbWN1", CONV_PRIORITY_HIGH)	//"No pussies allowed in this gym buddy. Better leave before you trip over a dumbbell."
				RESTART_TIMER_NOW(warningTimer)
				iWarningSpeechStage ++
				PRINTLN("iWarningSpeechStage = ", iWarningSpeechStage)
			ENDIF
		
		//wait to see if the player has walked away or is still bothering the target
		ELIF iWarningSpeechStage = 2
			IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF GET_TIMER_IN_SECONDS(warningTimer) > 5.5
					IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) > 3.5
						bWarning = FALSE
					ENDIF
					iWarningSpeechStage ++
					PRINTLN("iWarningSpeechStage = ", iWarningSpeechStage)
				ENDIF
			ENDIF
		
		//turn and face the player and warn him once more
		ELIF iWarningSpeechStage = 3
			IF bWarning
				FACE_PLAYER_AND_PLAY_ANIM(args.myTarget, "misscommon@response", "threaten")
				RESTART_TIMER_NOW(warningTimer)
				iWarningSpeechStage ++
				PRINTLN("iWarningSpeechStage = ", iWarningSpeechStage)
			ENDIF
		ELIF iWarningSpeechStage = 4
			IF IS_ENTITY_PLAYING_ANIM(args.myTarget, "misscommon@response", "threaten")
				CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bbWN2", CONV_PRIORITY_HIGH)	//"Did you hear me numbnuts? Get outta here before I smash you."
				RESTART_TIMER_NOW(warningTimer)
				iWarningSpeechStage ++
				PRINTLN("iWarningSpeechStage = ", iWarningSpeechStage)
			ENDIF
		
		//react appropriately if the player continues to approach the target
		ELIF iWarningSpeechStage = 5
			IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK
				IF GET_TIMER_IN_SECONDS(warningTimer) > 7
					IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 3.5
						bPanicking = TRUE
//						PRINTLN("bPanicking = TRUE")
					ELSE
						bWarning = FALSE
//						PRINTLN("bWarning = FALSE")
					ENDIF
				ELSE
//					PRINTLN("TIMER STILL GOING")
				ENDIF
			ELSE
//				PRINTLN("BODY BUILDER STILL PERFORMING ANIMATION")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//manages idle chatter from target and onlookers when there has not been any panic triggered yet in the bodybuilder event
PROC MANAGE_BODYBUILDER_IDLE_SPEECH(ASS_ARGS& args)
//	PED_INDEX pedToSpeak
	
	IF NOT IS_TIMER_STARTED(speechTimerTarget)
		RESTART_TIMER_NOW(speechTimerTarget)
	ELSE
		IF iIdleSpeechStage < 7
			IF GET_TIMER_IN_SECONDS(speechTimerTarget) > fSpeechTime
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				SWITCH iIdleSpeechStage
					CASE 0
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bbId0", CONV_PRIORITY_HIGH)
					BREAK
					CASE 1
						ADD_PED_FOR_DIALOGUE(args.assConv, 4, bystanders[0].ped, "OJAmlGymOnlookerF")
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bbBY1", CONV_PRIORITY_HIGH)
					BREAK
					CASE 2
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bbId2", CONV_PRIORITY_HIGH)
					BREAK
					CASE 3
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bbBY3", CONV_PRIORITY_HIGH)
					BREAK
					CASE 4
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bbId0", CONV_PRIORITY_HIGH)
					BREAK
				ENDSWITCH
				
				//restart timer and increment speech stage
				RESTART_TIMER_NOW(speechTimerTarget)
				fSpeechTime = GET_RANDOM_FLOAT_IN_RANGE(5, 8)
				iIdleSpeechStage ++
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//manages bodybuilder speech while he is in the gym area
PROC UPDATE_BODYBUILDER_SPEECH(ASS_ARGS& args)
	IF NOT IS_PED_INJURED(args.myTarget)
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT bPanicking
				IF NOT bWarning
					IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 3.5
						bWarning = TRUE
					ELSE
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 20
						AND NOT IS_MESSAGE_BEING_DISPLAYED()
							//bodybuilder idle chatter as he is showing off to onlookers
							MANAGE_BODYBUILDER_IDLE_SPEECH(args)
						ENDIF
					ENDIF
				ELSE
					//have bodybuilder interact with target and threaten him
					MANAGE_BODYBUILDER_WARNING_BEHAVIOR(args)
				ENDIF
			ELSE
				IF NOT bPanicLinePlayed
					IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_SMART_FLEE_PED) = PERFORMING_TASK
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bbHLP", CONV_PRIORITY_HIGH)	//"Someone help! I can't go out like this!"
						bPanicLinePlayed = TRUE
					ENDIF
				ENDIF
				IF NOT bThreatenLinePlayed
					IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_COMBAT) = PERFORMING_TASK
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 15
							CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bbKIL", CONV_PRIORITY_HIGH)	//"Oh hell no! Do you have any idea who I am? I'll rip you apart!"
							bThreatenLinePlayed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//updating anims for 2 peds on yacht getting/giving lap dance
PROC UPDATE_YACHT_PEDS(ASS_ARGS& args)
	VECTOR vGuyOffsetPosition = <<-0.78, -4.48, 1.05>>
	VECTOR vGuyOffsetRotation = <<0, 0, 275.04>>
	VECTOR vGirlOffsetPosition = <<-0.18, -4.450, 1.5>>
	VECTOR vGirlOffsetRotation = <<0, 0, 0>>

	IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
		IF NOT IS_PED_INJURED(args.myTarget)
			IF NOT IS_ENTITY_PLAYING_ANIM(args.myTarget, "oddjobs@assassinate@multi@yachttarget@lapdance", "yacht_ld_m")
				TASK_PLAY_ANIM(args.myTarget, "oddjobs@assassinate@multi@yachttarget@lapdance", "yacht_ld_m", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
			IF NOT IS_ENTITY_ATTACHED(args.myTarget)
				ATTACH_ENTITY_TO_ENTITY(args.myTarget, args.myVehicle, 0, vGuyOffsetPosition, vGuyOffsetRotation, TRUE, TRUE)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(args.myOtherPed)
			IF NOT IS_ENTITY_PLAYING_ANIM(args.myOtherPed, "oddjobs@assassinate@multi@yachttarget@lapdance", "yacht_ld_f")
				TASK_PLAY_ANIM(args.myOtherPed, "oddjobs@assassinate@multi@yachttarget@lapdance", "yacht_ld_f", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
			IF NOT IS_ENTITY_ATTACHED(args.myOtherPed)
				ATTACH_ENTITY_TO_ENTITY(args.myOtherPed, args.myVehicle, 0, vGirlOffsetPosition, vGirlOffsetRotation, TRUE, TRUE, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

////shows the target play the pissing anim and then walk to his vehicle
//PROC UPDATE_BIKER_IN_CUTSCENE(ASS_ARGS& args)
//	SEQUENCE_INDEX tempSeq
//	
//	IF NOT IS_PED_INJURED(args.myTarget)
//		IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
//			IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
//				SET_VEHICLE_ON_GROUND_PROPERLY(args.myVehicle)
//				
//				OPEN_SEQUENCE_TASK(tempSeq)
//					TASK_ENTER_VEHICLE(NULL, args.myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
//				CLOSE_SEQUENCE_TASK(tempSeq)
//				TASK_PERFORM_SEQUENCE(args.myTarget, tempSeq)
//				CLEAR_SEQUENCE_TASK(tempSeq)
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC


PROC RESTART_SPEECH_TIMER_AND_ASSIGN_RANDOM_DELAY()
	RESTART_TIMER_NOW(speechTimerTarget)
	fSpeechTime = GET_RANDOM_FLOAT_IN_RANGE(3, 7)
ENDPROC

FUNC BOOL CAN_DIALOGUE_PLAY_WITH_DELAY()
	IF IS_TIMER_STARTED(speechTimerTarget)
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND GET_TIMER_IN_SECONDS(speechTimerTarget) > fSpeechTime
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
	iDialogueIdleLine ++
	iLineIDXToPlay = -1
	RESTART_SPEECH_TIMER_AND_ASSIGN_RANDOM_DELAY()
	PRINTLN("iDialogueIdleLine = ", iDialogueIdleLine)
ENDPROC


/// PURPOSE:
///    Goes through a series of dialogue lines that should be played by the yachtsman when idle
PROC HANDLE_YACHTSMAN_PLAYING_IDLE_LINES()
	
	SWITCH iDialogueIdleLine
		CASE 0
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF iLineIDXToPlay = -1
					iLineIDXToPlay = GET_RANDOM_INT_IN_RANGE(0, 4)
				ELSE
					IF iLineIDXToPlay = 0
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_1", CONV_PRIORITY_VERY_HIGH)
							ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
						ENDIF
					ELIF iLineIDXToPlay = 1
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_2", CONV_PRIORITY_HIGH)
							ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
						ENDIF
					ELIF iLineIDXToPlay = 2
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_3", CONV_PRIORITY_HIGH)
							ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
						ENDIF
					ELSE
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_4", CONV_PRIORITY_HIGH)
							ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF CAN_DIALOGUE_PLAY_WITH_DELAY()
				IF iLineIDXToPlay = -1
					iLineIDXToPlay = GET_RANDOM_INT_IN_RANGE(0, 4)
				ELSE
					IF iLineIDXToPlay = 0
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_5", CONV_PRIORITY_VERY_HIGH)
							ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
						ENDIF
					ELIF iLineIDXToPlay = 1
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_6", CONV_PRIORITY_HIGH)
							ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
						ENDIF
					ELIF iLineIDXToPlay = 2
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_7", CONV_PRIORITY_HIGH)
							ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
						ENDIF
					ELSE
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_8", CONV_PRIORITY_HIGH)
						ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF CAN_DIALOGUE_PLAY_WITH_DELAY()
				IF iLineIDXToPlay = -1
					iLineIDXToPlay = GET_RANDOM_INT_IN_RANGE(0, 3)
				ELSE
					IF iLineIDXToPlay = 0
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_9", CONV_PRIORITY_VERY_HIGH)
							ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
						ENDIF
					ELIF iLineIDXToPlay = 1
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_10", CONV_PRIORITY_HIGH)
							ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
						ENDIF
					ELIF iLineIDXToPlay = 2
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_11", CONV_PRIORITY_HIGH)
							ADVANCE_IDLE_LINE_AND_RESTART_IDLE_DIALOGUE_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
//		CASE 3
//			IF CAN_DIALOGUE_PLAY_WITH_DELAY()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_4", CONV_PRIORITY_HIGH)
//				RESTART_SPEECH_TIMER_AND_ASSIGN_RANDOM_DELAY()
//				iDialogueIdleLine ++
//			ENDIF
//		BREAK
//		CASE 4
//			IF CAN_DIALOGUE_PLAY_WITH_DELAY()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_5", CONV_PRIORITY_HIGH)
//				RESTART_SPEECH_TIMER_AND_ASSIGN_RANDOM_DELAY()
//				iDialogueIdleLine ++
//			ENDIF
//		BREAK
//		CASE 5
//			IF CAN_DIALOGUE_PLAY_WITH_DELAY()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_6", CONV_PRIORITY_HIGH)
//				RESTART_SPEECH_TIMER_AND_ASSIGN_RANDOM_DELAY()
//				iDialogueIdleLine ++
//			ENDIF
//		BREAK
//		CASE 6
//			IF CAN_DIALOGUE_PLAY_WITH_DELAY()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_7", CONV_PRIORITY_HIGH)
//				RESTART_SPEECH_TIMER_AND_ASSIGN_RANDOM_DELAY()
//				iDialogueIdleLine ++
//			ENDIF
//		BREAK
//		CASE 7
//			IF CAN_DIALOGUE_PLAY_WITH_DELAY()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_8", CONV_PRIORITY_HIGH)
//				RESTART_SPEECH_TIMER_AND_ASSIGN_RANDOM_DELAY()
//				iDialogueIdleLine ++
//			ENDIF
//		BREAK
//		CASE 8
//			IF CAN_DIALOGUE_PLAY_WITH_DELAY()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_9", CONV_PRIORITY_HIGH)
//				RESTART_SPEECH_TIMER_AND_ASSIGN_RANDOM_DELAY()
//				iDialogueIdleLine ++
//			ENDIF
//		BREAK
//		CASE 9
//			IF CAN_DIALOGUE_PLAY_WITH_DELAY()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_10", CONV_PRIORITY_HIGH)
//				RESTART_SPEECH_TIMER_AND_ASSIGN_RANDOM_DELAY()
//				iDialogueIdleLine ++
//			ENDIF
//		BREAK
//		CASE 10
//			IF CAN_DIALOGUE_PLAY_WITH_DELAY()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytT1", "OJASml_ytT1_11", CONV_PRIORITY_HIGH)
//				RESTART_SPEECH_TIMER_AND_ASSIGN_RANDOM_DELAY()
//				iDialogueIdleLine ++
//			ENDIF
//		BREAK
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Goes through a series of dialogue lines that should be played if the player is fucking around near the yacht
/// PARAMS:
///    iDiagLine - Dialogue Line counter to be used. Increments after a line is played
/// RETURNS:
///    TRUE if all warning lines have been exhausted
FUNC BOOL HAS_YACHTSMAN_PLAYED_ALL_WARNING_LINES(INT& iDiagLine)
	SWITCH iDiagLine
		CASE 0
			CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_spot", CONV_PRIORITY_VERY_HIGH)
			TASK_LOOK_AT_ENTITY(assArgs.myTarget, PLAYER_PED_ID(), 5000)
			iDiagLine ++
		BREAK
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_get", "OJASml_get_1", CONV_PRIORITY_HIGH)
				
				// Mike - I had to switch this, because it kept playing the same line over, since it's random... we should be okay to play the convo.
				CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_get", CONV_PRIORITY_HIGH)
				TASK_LOOK_AT_ENTITY(assArgs.myTarget, PLAYER_PED_ID(), 5000)
				RETURN TRUE
			ENDIF
		BREAK
//		CASE 2
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_get", "OJASml_get_2", CONV_PRIORITY_HIGH)
//				iDiagLine ++
//			ENDIF
//		BREAK
//		CASE 3
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_get", "OJASml_get_3", CONV_PRIORITY_HIGH)
//				RETURN TRUE
//			ENDIF
//		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Goes through a series of dialogue lines that should be played by the yachtsman after panic has been triggered
/// PARAMS:
///    iDiagLine - Dialogue Line counter to be used. Increments after a line is played
/// RETURNS:
///    TRUE if all panic lines have been exhausted
FUNC BOOL HAS_YACHTSMAN_PLAYED_ALL_PANIC_LINES(INT& iDiagLine)
	SWITCH iDiagLine
		CASE 0
			CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_ytTP2", CONV_PRIORITY_HIGH)
			iDiagLine ++
		BREAK
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_beg", "OJASml_beg_1", CONV_PRIORITY_HIGH)
				CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_beg", CONV_PRIORITY_HIGH)
				RETURN TRUE
			ENDIF
		BREAK
//		CASE 2
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_beg", "OJASml_beg_2", CONV_PRIORITY_HIGH)
//				iDiagLine ++
//			ENDIF
//		BREAK
//		CASE 3
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_beg", "OJASml_beg_3", CONV_PRIORITY_HIGH)
//				RETURN TRUE
//			ENDIF
//		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Goes through a series of dialogue lines that should be played by the yachtsman after panic has been triggered
/// PARAMS:
///    iDiagLine - Dialogue Line counter to be used. Increments after a line is played
/// RETURNS:
///    TRUE if all panic lines have been exhausted
FUNC BOOL HAS_WASHER_PLAYED_ALL_PANIC_LINES(INT& iDiagLine)
	SWITCH iDiagLine
		CASE 0
			CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_wwTP2", CONV_PRIORITY_HIGH)
			iDiagLine ++
		BREAK
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_wwPAN", "OJASml_wwPAN_1", CONV_PRIORITY_HIGH)
				CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_wwPAN", CONV_PRIORITY_HIGH)
				RETURN TRUE
			ENDIF
		BREAK
//		CASE 2
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_wwPAN", "OJASml_wwPAN_2", CONV_PRIORITY_HIGH)
//				iDiagLine ++
//			ENDIF
//		BREAK
//		CASE 3
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_wwPAN", "OJASml_wwPAN_3", CONV_PRIORITY_HIGH)
//				RETURN TRUE
//			ENDIF
//		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Goes through a series of dialogue lines that should be played by the yachtsman after panic has been triggered
/// PARAMS:
///    iDiagLine - Dialogue Line counter to be used. Increments after a line is played
/// RETURNS:
///    TRUE if all panic lines have been exhausted
FUNC BOOL HAS_BIKER_PLAYED_ALL_PANIC_LINES(INT& iDiagLine)
	SWITCH iDiagLine
		CASE 0
			CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_bkTP1", CONV_PRIORITY_HIGH)		//Jesus, put the gun down!  
			RESTART_SPEECH_TIMER_AND_ASSIGN_RANDOM_DELAY()
	
			iDiagLine ++
		BREAK
		CASE 1
			IF CAN_DIALOGUE_PLAY_WITH_DELAY()
				CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_bkTP2", CONV_PRIORITY_HIGH)		//begs for his life
				RETURN TRUE
			ENDIF
		BREAK
//		CASE 2
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_bkTP1", "OJASml_bkTP1_3", CONV_PRIORITY_HIGH)	//Jesus, what do you want from me?  
//				iDiagLine ++
//			ENDIF
//		BREAK
//		CASE 3
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_bkSHT", "OJASml_bkSHT_1", CONV_PRIORITY_HIGH)	//What the hell are you doing?
//				iDiagLine ++
//			ENDIF
//		BREAK
//		CASE 4
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_bkSHT", "OJASml_bkSHT_2", CONV_PRIORITY_HIGH)	//Are you crazy? 
//				iDiagLine ++
//			ENDIF
//		BREAK
//		CASE 5
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_bkSHT", "OJASml_bkSHT_3", CONV_PRIORITY_HIGH)	//Get away from me!
//				iDiagLine ++
//			ENDIF
//		BREAK
//		CASE 6
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_bkSHT", "OJASml_bkSHT_4", CONV_PRIORITY_HIGH)	//What is wrong with you?
//				iDiagLine ++
//			ENDIF
//		BREAK
//		CASE 7
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(assArgs.assConv, "OJASAUD", "OJASml_bkSHT", "OJASml_bkSHT_5", CONV_PRIORITY_HIGH)	//I'm serious, man. Back off!
//				RETURN TRUE
//			ENDIF
//		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_PLAYER_JACKING_OR_TOUCHING_YACHT(ASS_ARGS& args)
	IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle, TRUE)
		OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), args.myVehicle)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC CREATE_VEHICLES_AT_GAS_STATION_OF_PHONE_CUTSCENE()
	IF NOT DOES_ENTITY_EXIST(viGasStationVehicle[0])
		viGasStationVehicle[0] = CREATE_VEHICLE(modelParkedVehicle[0], <<-723.8201, -916.5402, 18.0145>>, 267.7980)		// - parking spot
	ENDIF
	IF NOT DOES_ENTITY_EXIST(viGasStationVehicle[1])
		viGasStationVehicle[1] = CREATE_VEHICLE(modelParkedVehicle[1], <<-735.5003, -931.6205, 18.0173>>, 359.3239)		// - pump near phone
		SET_ENTITY_LOD_DIST(viGasStationVehicle[1], 100)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(viGasStationVehicle[2])
		viGasStationVehicle[2] = CREATE_VEHICLE(modelParkedVehicle[0], <<-726.7143, -940.2681, 18.0173>>, 178.3615)		// - pump away from phone
	ENDIF
	IF NOT DOES_ENTITY_EXIST(viGasStationVehicle[3])
		viGasStationVehicle[3] = CREATE_VEHICLE(modelParkedVehicle[2], <<-730.5711, -909.9425, 18.0349>>, 357.4066)		// - in corner
	ENDIF
ENDPROC


PROC CLEANUP_VEHICLES_AT_GAS_STATION_OF_PHONE_CUTSCENE()
	INT idx
	REPEAT NUM_GAS_STATION_VEHICLES idx
		IF DOES_ENTITY_EXIST(viGasStationVehicle[idx])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(viGasStationVehicle[idx])
		ENDIF
	ENDREPEAT
ENDPROC


//go through and initialize the model array slots to DUMMY_MODEL_FOR_SCRIPT to clear them all out
PROC RESET_MODEL_ARRAY(STREAMED_MODEL& streamedModel[])
	INT modelIndex
	REPEAT COUNT_OF(streamedModel) modelIndex
		streamedModel[modelIndex].thisModel = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT
ENDPROC


//go through and initialize the anim array slots to NULL STRINGS to clear them all out
PROC RESET_ANIM_ARRAY(STREAMED_ANIM& streamedAnim[])
	INT animIndex
	REPEAT COUNT_OF(streamedAnim) animIndex
		streamedAnim[animIndex].thisAnim = NULL_STRING()
	ENDREPEAT
ENDPROC


PROC SET_ALL_PEDS_USED_IN_MISSION_AS_NO_LONGER_NEEDED(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	SET_BODYBUILDER_PEDS_AND_MODELS_AS_NO_LONGER_NEEDED(args, targetData)
	SET_YACHT_PEDS_AND_MODELS_AS_NO_LONGER_NEEDED(args, targetData)
	SET_WASHER_PEDS_AND_MODELS_AS_NO_LONGER_NEEDED(args, targetData)
	SET_BIKER_PEDS_AND_MODELS_AS_NO_LONGER_NEEDED(args, targetData)
ENDPROC


//resets all models/variables to their default state so that they can be ready for the next target
PROC RESET_TARGET_DATA(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	INT idx
	
	SET_PARKED_VEHICLE_MODELS_AS_NO_LONGER_NEEDED()
	CLEANUP_VEHICLES_AT_GAS_STATION_OF_PHONE_CUTSCENE()
	
	SET_ALL_PEDS_USED_IN_MISSION_AS_NO_LONGER_NEEDED(args, targetData)
	
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(targetData.streamedModels)
	SET_ALL_ANIMS_AS_NO_LONGER_NEEDED(targetData.streamedAnims)
	RESET_MODEL_ARRAY(targetData.streamedModels)
	RESET_ANIM_ARRAY(targetData.streamedAnims)
	
	CLEANUP_LOAD_QUEUE_LARGE(sLoadQueue)
	
	// To Fix Bug # 929669
	args.myTarget = NULL
//	SET_PED_AS_NO_LONGER_NEEDED(args.myTarget)
	
	IF curTarget != ASSASSIN_TARGET_YACHT
		SET_VEHICLE_AS_NO_LONGER_NEEDED(args.myVehicle)
		
		IF DOES_ENTITY_EXIST(viYacht)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(viYacht)
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(args.myVehicle)
			viYacht = args.myVehicle		//assign the yacht vehicle index. we will clear this up the next time this function is called (to fix bug 1490795 - 6/20/13 MB)
		ENDIF
	ENDIF
		
	IF DOES_ENTITY_EXIST(viJetski)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(viJetski)
	ENDIF
	IF DOES_ENTITY_EXIST(viRestStopCar)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(viRestStopCar)
	ENDIF
	IF DOES_ENTITY_EXIST(viRestStopCamper)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(viRestStopCamper)
	ENDIF
	bPanicking = FALSE
	iIdleSpeechStage = 0
	iKillCamStage = 0
	bThreatenLinePlayed = FALSE
	bPanicLinePlayed = FALSE
	bRopeDetached = FALSE
	bGaveTextMessage = FALSE
	iDialogueLine = 0
	bTargetKillLinePlayed = FALSE
	bPedsForCurrentTargetCreated = FALSE
	bPoliceChaseLinePlayed = FALSE
	bSplashSoundPlayed = FALSE
	bNewsReportPlayed = FALSE
	
	FOR idx = 3 TO 9
		REMOVE_PED_FOR_DIALOGUE(args.assConv, idx)
	ENDFOR
ENDPROC

/// PURPOSE:
///    Calls SET_PED_RESET_FLAG(assArgs.myTarget, PRF_SuppressLethalMeleeActions, TRUE) every frame to disallow targets from being knocked out in one punch
PROC DISABLE_KNOCKOUT_ON_TARGET()
	IF DOES_ENTITY_EXIST(assArgs.myTarget)
		IF NOT IS_PED_INJURED(assArgs.myTarget)
			SET_PED_RESET_FLAG(assArgs.myTarget, PRF_PreventAllMeleeTakedowns, TRUE)
			SET_PED_RESET_FLAG(assArgs.myTarget, PRF_SuppressLethalMeleeActions, TRUE) 
		ENDIF
	ENDIF
ENDPROC


//update loop that runs while the bodybuilder target is active
PROC UPDATE_TARGET_BODYBUILDER(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	PED_INDEX pedToSpeak
	WEAPON_TYPE wtPlayerIsUsing
	BOOL bOkayToCheck
	
	//to try and fix bug 1147295 - Characters can be assassinated with a single punch. - 3/2/13 - MB
	DISABLE_KNOCKOUT_ON_TARGET()
	
	SWITCH curTargetStage
		CASE TARGET_STAGE_BLIP_TARGET
			CREATE_CURRENT_TARGET_BLIP(args)

			curTargetStage = TARGET_STAGE_WAITING
		BREAK
		
		CASE TARGET_STAGE_WAITING
			IF DO_AGGRO_CHECK(args.myTarget, NULL, aggroArgs, aggroReason, TRUE, FALSE)
			OR HAS_PLAYER_ATTACKED_BYSTANDERS(bAttackedBystanders)
			OR IS_PLAYER_SEEN_THREATENING_TARGET_BY_BYSTANDERS(args.myTarget, aimTimer, 1.0, 20.0, bSeenAimingAtTarget)
			OR ( ARE_COPS_NEAR_TARGET_WHILE_PLAYER_IS_WANTED(args.myTarget, 25) AND bPanicking )
			OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, << -1218.0418 , -1549.6161, -3.3703 >>, << -1185.3494, -1597.1029, 10.5993 >>, 46.0)
			OR bPanicking	
				STOP_SCRIPTED_CONVERSATION(FALSE)
				
				IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtPlayerIsUsing)
					IF wtPlayerIsUsing <> WEAPONTYPE_INVALID
						bOkayToCheck = TRUE
						PRINTLN("bOkayToCheck = TRUE")
					ENDIF
				ENDIF
				
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
				OR (bOkayToCheck AND (wtPlayerIsUsing <> WEAPONTYPE_UNARMED))
					TASK_SMART_FLEE_PED(args.myTarget, PLAYER_PED_ID(), 500, -1)
					PRINTLN("TASKING BODYBUILDER TO FLEE")
				ELSE
					TASK_COMBAT_PED(args.myTarget, PLAYER_PED_ID())
				ENDIF
				
				TASK_BYSTANDERS_TO_FLEE()
				SET_ENTITY_LOAD_COLLISION_FLAG(args.myTarget, TRUE)
				bPanicking = TRUE
				curTargetStage = TARGET_STAGE_PANICKING
			ELSE
				IF NOT IS_PED_INJURED(args.myTarget)
					IF NOT bWarning
						UPDATE_BODYBUILDER_ANIMS(targetData, args)
					ENDIF
					
					//update peds leaving and arriving at scene
//					UPDATE_BYSTANDERS_LEAVING_AREA(5, 8)
//					UPDATE_BYSTANDERS_ARRIVING(3)
					UPDATE_BYSTANDERS_STATES()
				ENDIF
			ENDIF
		BREAK
		
		CASE TARGET_STAGE_PANICKING
			
			IF bAttackedBystanders
			OR bSeenAimingAtTarget
				pedToSpeak = GET_CLOSEST_BYSTANDER(FALSE, GET_ENTITY_COORDS(PLAYER_PED_ID()), 20)
				IF pedToSpeak <> NULL	
					ADD_PED_FOR_DIALOGUE(args.assConv, 4, pedToSpeak, "OJAmlGymOnlookerF")
				ENDIF
				IF bAttackedBystanders
					IF NOT bPlayedBystanderPanicLine
						IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bbWN3", CONV_PRIORITY_HIGH)	//"Help! What are you doing? No!
							bPlayedBystanderPanicLine = TRUE
							PRINTLN("bPlayedBystanderPanicLine = TRUE - 01")
						ENDIF
					ENDIF
				ELSE
					IF NOT bPlayedBystanderPanicLine
						IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bbWN4", CONV_PRIORITY_HIGH)	//"Run! He has a gun!
							bPlayedBystanderPanicLine = TRUE
							PRINTLN("bPlayedBystanderPanicLine = TRUE - 02")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT IS_PED_INJURED(args.myTarget)
				IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
					IF IS_PED_SHOOTING(PLAYER_PED_ID())
						CLEAR_PED_TASKS(args.myTarget)
						TASK_SMART_FLEE_PED(args.myTarget, PLAYER_PED_ID(), 500, -1)
						PRINTLN("TASKING BODY BUILDER TO FLEE - 01")
					ENDIF
				ENDIF
			ENDIF
			IF HAS_TARGET_ESCAPED(args.myTarget, fFailDist)
				SET_MISSION_FAILED(assArgs, targetData, FAIL_PED_ESCAPED)
			ENDIF
		BREAK
	ENDSWITCH
	
	//update the bodybuilder / bystander speech
	UPDATE_BODYBUILDER_SPEECH(args)
ENDPROC

//update loop that runs while the yacht target is active
PROC UPDATE_TARGET_YACHT(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	SEQUENCE_INDEX tempSeq
	FLOAT fPlayerDist
	FLOAT fScenePhase
//	INT iBoneIndex

	SWITCH curTargetStage
		CASE TARGET_STAGE_BLIP_TARGET
			CREATE_CURRENT_TARGET_BLIP(args)
			
			IF NOT IS_ENTITY_DEAD(args.myVehicle)
				SET_BOAT_SINKS_WHEN_WRECKED(args.myVehicle, TRUE)
			ENDIF
			IF NOT IS_ENTITY_DEAD(args.myTarget)
				SET_RAGDOLL_BLOCKING_FLAGS(args.myTarget, RBF_PLAYER_IMPACT)
			ENDIF
			IF NOT IS_ENTITY_DEAD(args.myOtherPed)
				SET_RAGDOLL_BLOCKING_FLAGS(args.myOtherPed, RBF_PLAYER_IMPACT)
				SET_ENTITY_HEALTH(args.myOtherPed, 105)
				PRINTLN("SETTIGN YACHT GIRL HEALTH TO 105")
			ENDIF
			
			REQUEST_ANIM_DICT("veh@boat@marquis@rps@enter_exit")
			
			PRINTLN("GOING TO STATE - TARGET_STAGE_WAITING")
			curTargetStage = TARGET_STAGE_WAITING
		BREAK
		
		CASE TARGET_STAGE_WAITING
			
			fPlayerDist = GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget)
			
			IF DO_AGGRO_CHECK(args.myTarget, args.myVehicle, aggroArgs, aggroReason, FALSE)
			OR DO_AGGRO_CHECK(args.myOtherPed, args.myVehicle, aggroArgs, aggroReason, FALSE)
			OR IS_PLAYER_JACKING_OR_TOUCHING_YACHT(args)
			OR ( ARE_COPS_NEAR_TARGET_WHILE_PLAYER_IS_WANTED(args.myTarget, 50) AND bPanicking )
			
				IF NOT IS_PED_INJURED(args.myTarget)
					
					IF fPlayerDist <= 75						
						IF NOT IS_ENTITY_DEAD(args.myVehicle)
							IF HAS_ANIM_DICT_LOADED("veh@boat@marquis@rps@enter_exit")
								iSceneId = CREATE_SYNCHRONIZED_SCENE (<<0,0,0>>, <<0,0,0>>)
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneId, args.myVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(args.myVehicle, "seat_dside_r"))
								TASK_SYNCHRONIZED_SCENE(args.myTarget, iSceneId, "veh@boat@marquis@rps@enter_exit", "jump_out", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_NONE, RBF_BULLET_IMPACT)
								PRINTLN("TASKING TO JUMP OUT")
							ENDIF
						ENDIF
					ELSE
						bAggroAtDistance = TRUE
						
						IF NOT IS_ENTITY_DEAD(args.myVehicle)
							iSceneId = CREATE_SYNCHRONIZED_SCENE (<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneId, args.myVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(args.myVehicle, "seat_dside_r"))
							TASK_SYNCHRONIZED_SCENE(args.myTarget, iSceneId, "oddjobs@assassinate@multi@yachttarget@lapdance", "exit_steer_m", SLOW_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_NONE, RBF_BULLET_IMPACT)
							PRINTLN("TASKING TO PLAY GO TO DRIVER SEAT")
						ENDIF
					ENDIF					
				ENDIF
				
				IF NOT IS_PED_INJURED(args.myOtherPed)	//girl on yacht
					OPEN_SEQUENCE_TASK(tempSeq)
						IF fPlayerDist <= 75
							TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@multi@yachttarget@lapdance", "exit_quick_f")
							TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@multi@yachttarget@lapdance", "exit_cower_loop_f", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						ELSE
							TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@multi@yachttarget@lapdance", "exit_steer_f")
							TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@multi@yachttarget@lapdance", "exit_cower_loop_f", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						ENDIF
					CLOSE_SEQUENCE_TASK(tempSeq)
					TASK_PERFORM_SEQUENCE(args.myOtherPed, tempSeq)
					CLEAR_SEQUENCE_TASK(tempSeq)
					
					SET_PED_KEEP_TASK(args.myOtherPed, TRUE)
					
					IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myOtherPed) <= 50
						ADD_PED_FOR_DIALOGUE(args.assConv, 5, args.myOtherPed, "YachtPed1")
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_ytG1", CONV_PRIORITY_HIGH)	//"Aaaah! Someone help us!"
					ENDIF
				ENDIF
				IF NOT IS_PED_INJURED(args.myAuxPed)	//sunbathing girl
					IF GET_SCRIPT_TASK_STATUS(args.myAuxPed, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
						CLEAR_PED_TASKS(args.myAuxPed)
						TASK_SMART_FLEE_PED(args.myAuxPed, PLAYER_PED_ID(), 200, -1)
						SET_PED_KEEP_TASK(args.myAuxPed, TRUE)
						PRINTLN("TASKING SUNBATHING GIRL TO FLEE")
					ENDIF
				ENDIF
				
				//initialize this variable so that we can use it in handling panic dialogue
				iDialogueLine = 0
				
				SET_ENTITY_LOAD_COLLISION_FLAG(args.myTarget, TRUE)
				curTargetStage = TARGET_STAGE_PANICKING
			ELSE
				//make sure peds on yacht are playing their anims
				UPDATE_YACHT_PEDS(args)
				
				IF fPlayerDist <= 15
				AND NOT bThreatenLinePlayed
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF HAS_YACHTSMAN_PLAYED_ALL_WARNING_LINES(iDialogueLine)
							bThreatenLinePlayed = TRUE
						ENDIF
					ENDIF
				ELSE
					IF fPlayerDist <= 25
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							HANDLE_YACHTSMAN_PLAYING_IDLE_LINES()
						ENDIF
					ENDIF
				ENDIF
				
				//have sunbathing girl on shore freak out if player jacks her jetski (but dont trigger panic on the target)
				IF NOT bSunbatherPanic
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viJetski)
						IF NOT IS_PED_INJURED(args.myAuxPed)
							IF GET_SCRIPT_TASK_STATUS(args.myAuxPed, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
								CLEAR_PED_TASKS(args.myAuxPed)
								TASK_SMART_FLEE_PED(args.myAuxPed, PLAYER_PED_ID(), 200, -1)
								SET_PED_KEEP_TASK(args.myAuxPed, TRUE)
								PRINTLN("TASKING SUNBATHING GIRL TO FLEE")
								IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myAuxPed) < 50
									ADD_PED_FOR_DIALOGUE(args.assConv, 4, args.myAuxPed, "OJAmlSUNBATHER" )
									CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_ytSUN", CONV_PRIORITY_HIGH)	//"Someone help! He just stole my jetski!"
								ENDIF
								bSunbatherPanic = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE TARGET_STAGE_PANICKING
		
			IF NOT IS_ENTITY_DEAD(args.myTarget)
				IF NOT IS_ENTITY_DEAD(args.myVehicle)
					IF GET_PED_IN_VEHICLE_SEAT(args.myVehicle) <> args.myTarget
						IF bAggroAtDistance
						
							IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
								fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iSceneId)
								PRINTLN("bAggroAtDistance - fScenePhase = ", fScenePhase)
							ENDIF
							
							IF fScenePhase > 0.80
								CLEAR_PED_TASKS(args.myTarget)
								SET_PED_INTO_VEHICLE(args.myTarget, args.myVehicle)
								PRINTLN("SETTING PED INTO VEHICLE SEAT - NO LONGER PLAYING ANIMATION")
							ELSE
//								PRINTLN("SYNCED SCENE STILL RUNNING")
							ENDIF
						ELSE
							IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
								fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iSceneId)
								PRINTLN("fScenePhase = ", fScenePhase)
							ENDIF
							
							IF fScenePhase > 0.90
								IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
									TASK_SMART_FLEE_PED(args.myTarget, PLAYER_PED_ID(), 1000, -1)
									PRINTLN("THE TARGET HAS JUMPED IN THE WATER, TASKING HIM TO FLEE")
								ENDIF
							ELIF NOT bSplashSoundPlayed
								IF fScenePhase > 0.36
//									IF LOAD_STREAM("MULT_ASSAS_WATER_SPLASH_master")
										ODDJOB_STOP_SOUND(iSoundIdMulti)
										ODDJOB_PLAY_SOUND("MULT_ASSAS_WATER_SPLASH_master", iSoundIdMulti, FALSE, args.myTarget)
//										PLAY_STREAM_FROM_PED(args.myTarget)
										PRINTLN("SPLASH SOUND PLAYED")
										bSplashSoundPlayed = TRUE
//									ELSE
//										PRINTLN("SPLASH SOUND NOT LOADED!!!")
//									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
							TASK_VEHICLE_MISSION_COORS_TARGET(args.myTarget, args.myVehicle, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 20, DRIVINGMODE_AVOIDCARS_RECKLESS, 500, -1)
							PRINTLN("THE TARGET IS IN THE DRIVER'S SEAT, TASKING HIM TO FLEE")
						ENDIF
					ENDIF
				ENDIF
			
			
				IF NOT bPanicLinePlayed
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 35
						IF HAS_YACHTSMAN_PLAYED_ALL_PANIC_LINES(iDialogueLine)
							bPanicLinePlayed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("TARGET IS DEAD")
			ENDIF
			
			IF HAS_TARGET_ESCAPED(args.myTarget, fFailDist)
				SET_MISSION_FAILED(assArgs, targetData, FAIL_PED_ESCAPED)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


//update loop that runs while the washer target is active
PROC UPDATE_TARGET_WASHER(ASS_ARGS& args)

	SWITCH curTargetStage
		CASE TARGET_STAGE_BLIP_TARGET	
			CREATE_CURRENT_TARGET_BLIP(args)
			
			curTargetStage = TARGET_STAGE_WAITING
		BREAK
		
		CASE TARGET_STAGE_WAITING
			IF NOT bRopeDetached
				IF DO_AGGRO_CHECK(args.myTarget, NULL, aggroArgs, aggroReason, FALSE)
				OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(args.myTarget), 15.0, FALSE)
				OR bPanicking
				OR GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 5
				OR ( IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID()) AND GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) <= 35.0 )
					IF NOT IS_PED_INJURED(args.myTarget)
						PRINTLN("WASHER GOING TO PANIC MODE")
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) <= 100
							CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_wwTP", CONV_PRIORITY_HIGH)	//"what the hell was that"
						ENDIF
						TASK_PLAY_ANIM(args.myTarget, "oddjobs@assassinate@multi@windowwasher", "_shot_reaction", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
						curTargetStage = TARGET_STAGE_PANICKING
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TARGET_STAGE_PANICKING
			IF NOT bPanicLinePlayed
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) <= 100
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF HAS_WASHER_PLAYED_ALL_PANIC_LINES(iDialogueLine)
						bPanicLinePlayed = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			
			//to fix an issue I saw today where if the target was hurt but not killed would stand up and not cower since he was broken out of his previous anim task
			IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_COWER) != PERFORMING_TASK
				TASK_COWER(args.myTarget, -1)
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bAttachedRope
		HANDLE_SHOT_ROPE()
	ENDIF
ENDPROC

//update loop that runs while the biker target is active
PROC UPDATE_TARGET_BIKER(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	SEQUENCE_INDEX tempSeq

	SWITCH curTargetStage
		CASE TARGET_STAGE_BLIP_TARGET
			CREATE_CURRENT_TARGET_BLIP(args)
			
			SET_ENTITY_LOAD_COLLISION_FLAG(args.myTarget, TRUE)
			
//			IF NOT bPanicking	//set target onto bike after cutscene has finished
//				IF NOT IS_PED_INJURED(args.myTarget)
//					IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
//						SET_ENTITY_COORDS(args.myVehicle, targetData.vVehPos)
//						SET_ENTITY_HEADING(args.myVehicle, targetData.fVehHead)
//						SET_ENTITY_LOAD_COLLISION_FLAG(args.myTarget, TRUE)
//						IF NOT IS_PED_IN_ANY_VEHICLE(args.myTarget)
//							SET_PED_INTO_VEHICLE(args.myTarget, args.myVehicle)
//							SET_VEHICLE_ON_GROUND_PROPERLY(args.myVehicle)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
			SETTIMERA(0)
			curTargetStage = TARGET_STAGE_WAITING
		BREAK
		
		CASE TARGET_STAGE_WAITING
			IF DO_AGGRO_CHECK(args.myTarget, args.myVehicle, aggroArgs, aggroReason, TRUE, FALSE)
			OR bPanicking
				IF NOT IS_PED_INJURED(args.myTarget)
					SET_ENTITY_LOAD_COLLISION_FLAG(args.myTarget, TRUE)
//					CLEAR_PED_TASKS(args.myTarget)
					IF IS_PED_IN_VEHICLE(args.myTarget, args.myVehicle)
						IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								TASK_VEHICLE_MISSION_PED_TARGET(args.myTarget, args.myVehicle, PLAYER_PED_ID(), MISSION_FLEE, 35, DRIVINGMODE_AVOIDCARS_RECKLESS, 500, -1)
							ELSE
								TASK_VEHICLE_MISSION(args.myTarget, args.myVehicle, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_FLEE, 35, DRIVINGMODE_AVOIDCARS_RECKLESS, 500, -1)
							ENDIF
							
							PRINTLN("WE'VE AGGROED THE BIKER, MAKE HIM GO FASTER")
							
//							ADD_VEHICLE_SUBTASK_ATTACK_PED(args.myTarget, PLAYER_PED_ID())
						ENDIF
					ELSE
//						TASK_COMBAT_PED(args.myTarget, PLAYER_PED_ID())
						TASK_SMART_FLEE_PED(args.myTarget, PLAYER_PED_ID(), 1000, -1)
						PRINTLN("TASKING BIKER TO FLEE PLAYER - 01")
					ENDIF
				ENDIF
				SETTIMERA(0)
				curTargetStage = TARGET_STAGE_PANICKING
			ELSE
				IF NOT bTargetCruising
					IF NOT IS_PED_IN_ANY_VEHICLE(args.myTarget)
						SET_PED_INTO_VEHICLE(args.myTarget, args.myVehicle)
						SET_VEHICLE_ON_GROUND_PROPERLY(args.myVehicle)
					ENDIF
					SET_VEHICLE_FORWARD_SPEED(args.myVehicle, 4)
					OPEN_SEQUENCE_TASK(tempSeq)
						TASK_VEHICLE_MISSION_COORS_TARGET(NULL, args.myVehicle, << -1637.6823, 1018.6192, 152.1026 >>, MISSION_GOTO, 22, DRIVINGMODE_AVOIDCARS_RECKLESS, 10, -1)
						TASK_VEHICLE_DRIVE_WANDER(NULL, args.myVehicle, 22, DRIVINGMODE_AVOIDCARS_RECKLESS)
					CLOSE_SEQUENCE_TASK(tempSeq)
					TASK_PERFORM_SEQUENCE(args.myTarget, tempSeq)
					CLEAR_SEQUENCE_TASK(tempSeq)
					bTargetCruising = TRUE
				ENDIF
			ENDIF
		BREAK

		CASE TARGET_STAGE_PANICKING
			IF HAS_TARGET_ESCAPED(args.myTarget, fFailDist)
				SET_MISSION_FAILED(assArgs, targetData, FAIL_PED_ESCAPED)
			ELSE
				IF NOT IS_PED_INJURED(args.myTarget)
					IF NOT bTaskedToFlee
						IF NOT IS_PED_IN_ANY_VEHICLE(args.myTarget)
							IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
								SET_PED_COMBAT_ATTRIBUTES(args.myTarget, CA_USE_VEHICLE, FALSE)
	//							TASK_COMBAT_PED(args.myTarget, PLAYER_PED_ID())
								TASK_SMART_FLEE_PED(args.myTarget, PLAYER_PED_ID(), 1000, -1)
								PRINTLN("TASKING BIKER TO FLEE PLAYER - 02")
								bTaskedToFlee = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					//have target threaten the player while escaping
					IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 15
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_PED_RAGDOLL(args.myTarget)
						IF NOT bThreatenLinePlayed
							IF HAS_BIKER_PLAYED_ALL_PANIC_LINES(iDialogueLine)
								bThreatenLinePlayed = TRUE
								SETTIMERA(0)
							ENDIF
						ELIF NOT bPanicLinePlayed
							IF TIMERA() >= 10000
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_bkTP2", CONV_PRIORITY_HIGH)
								bPanicLinePlayed = TRUE
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	// To Fix Bug # 732292
	//make sure that the player always has a minimum of 2 star wanted level during this update loop
//	IF ARE_PLAYER_FLASHING_STARS_ABOUT_TO_DROP(PLAYER_ID())
//		SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
//	ENDIF
ENDPROC


//does update loop for current active target
PROC UPDATE_CURRENT_TARGET(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	SWITCH curTarget
		CASE ASSASSIN_TARGET_BODYBUILDER
			UPDATE_TARGET_BODYBUILDER(targetData, args)
		BREAK
		
		CASE ASSASSIN_TARGET_YACHT
			UPDATE_TARGET_YACHT(targetData, args)
		BREAK
		
		CASE ASSASSIN_TARGET_WASHER
			UPDATE_TARGET_WASHER(args)
		BREAK
		
		CASE ASSASSIN_TARGET_BIKER
			UPDATE_TARGET_BIKER(targetData, args)
		BREAK

	ENDSWITCH
	
	CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, args.myTarget)
	
ENDPROC


PROC ATTACH_ROPE_TO_PROPS(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	VECTOR vPlayerPosition, vTargetPos
	VECTOR vPlatformPosition
	VECTOR vPlatformPositionForAttachment
	SEQUENCE_INDEX tempSeq
	
	IF DOES_ENTITY_EXIST(args.myProp[0])
		vPlatformPositionForAttachment = GET_ENTITY_COORDS(args.myProp[0])
//		PRINTLN("vPlatformPositionForAttachment = ", vPlatformPositionForAttachment)
	ELSE
		vPlatformPositionForAttachment = targetData.vPropPos[0]
//		PRINTLN("args.myProp[0] DOES NOT EXIST USING VECTOR = ", targetData.vPropPos[0])
	ENDIF
	
	float topZ = 142.1
	float ropeLen = topZ - vPlatformPositionForAttachment.z
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_ENTITY_DEAD(args.myTarget)
		vTargetPos = GET_ENTITY_COORDS(args.myTarget)
	ENDIF
	
	SWITCH iRopeStages
		CASE 0
			IF NOT bAttachedRope
				IF curTarget = ASSASSIN_TARGET_WASHER
				
//					PRINTLN("INSIDE ATTACH_ROPE_TO_PROPS")
					
					IF VDIST2(vPlayerPosition, vTargetPos) < 40000 // 200 * 200
						IF DOES_ENTITY_EXIST(args.myProp[0])
							IF DOES_ENTITY_EXIST(args.myProp[1])
								IF NOT ROPE_ARE_TEXTURES_LOADED()
									ROPE_LOAD_TEXTURES()
								ELSE
	//								IF NOT IS_PED_INJURED(args.myTarget)
	//									ATTACH_ENTITY_TO_ENTITY(args.myTarget, args.myProp[0], 0, <<0,0,0.25>>, <<0,0,180>>, TRUE, TRUE)
	//									IF curTargetStage < TARGET_STAGE_PANICKING
	//										TASK_PLAY_ANIM(args.myTarget, "oddjobs@assassinate@multi@windowwasher", "_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
	//										PRINTLN("TASKING WASHER TO PLAY IDLE ANIMATION")
	//									ENDIF
	//								ENDIF																								
									
									IF NOT DOES_ROPE_EXIST(ropeWindowA)
										ropeWindowA = ADD_ROPE(<< -648.59, 305.73, topZ >>, <<0,90,0>>, ropeLen, PHYSICS_ROPE_THIN, -1, 5, 0.5, FALSE, FALSE, TRUE, 5, TRUE)
										PRINTLN("CREATING ropeWindowA")
									ENDIF
									IF NOT DOES_ROPE_EXIST(ropeWindowB)
										ropeWindowB = ADD_ROPE(<< -638.93, 304.92, topZ >>, <<0,90,0>>, ropeLen, PHYSICS_ROPE_THIN, -1, 5, 0.5, FALSE, FALSE, TRUE, 5, TRUE)
										PRINTLN("CREATING ropeWindowB")
									ENDIF
									
									IF DOES_ROPE_EXIST(ropeWindowB)
									AND DOES_ROPE_EXIST(ropeWindowA)						
									
										ATTACH_ENTITIES_TO_ROPE(ropeWindowA, args.myProp[1], args.myProp[0], << -648.59, 305.73, topZ >>, << -648.59, 305.73, vPlatformPositionForAttachment.z >>, ropeLen, 0, 0)
										ATTACH_ENTITIES_TO_ROPE(ropeWindowB, args.myProp[1], args.myProp[0], << -638.93, 304.92, topZ >>, << -638.93, 304.92, vPlatformPositionForAttachment.z >>, ropeLen, 0, 0)
										
										PRINTLN("bAttachedRope = TRUE")
										bAttachedRope = TRUE
										
										PRINTLN("iRopeStages = 1")
										iRopeStages = 1
									ELSE
										SCRIPT_ASSERT("Ropes do not exist")
									ENDIF
								ENDIF
							ELSE
								SCRIPT_ASSERT("args.myProp[1] DOES NOT EXIST")
							ENDIF
						ELSE
							SCRIPT_ASSERT("args.myProp[0] DOES NOT EXIST")
						ENDIF
					ELSE
//						PRINTLN("WE'RE GREATER THAN 125m")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//===================================================================================================================
		CASE 1
			IF VDIST2(vPlayerPosition, vTargetPos) < 18225 //135m
				// The player has seen the platform
				IF IS_ENTITY_ON_SCREEN(args.myProp[0]) //AND NOT IS_ENTITY_OCCLUDED(args.myProp[0])
					
					IF curTargetStage < TARGET_STAGE_PANICKING
						IF NOT IS_PED_INJURED(args.myTarget)
							OPEN_SEQUENCE_TASK(tempSeq)
								TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@multi@windowwasher", "_idle_to_up")
								TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@multi@windowwasher", "_up_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(tempSeq)
							TASK_PERFORM_SEQUENCE(args.myTarget, tempSeq)
							CLEAR_SEQUENCE_TASK(tempSeq)
						ENDIF
					ENDIF
						
					PRINTLN("PLATFORM IS ONSCREEN")
					PRINTLN("iRopeStages = 2")
					iRopeStages = 2
				ELSE
//					PRINTLN("PLATFORM IS NOT ONSCREEN")
				ENDIF
			ELSE
//				PRINTLN("NOT CLOSE ENOUGH TO TARGET")
			ENDIF
		BREAK
		//===================================================================================================================
		CASE 2
			vPlatformPosition = GET_ENTITY_COORDS(args.myProp[0])
			
//			PRINTLN("ROPE Z POSITION = ", vPlatformPosition.z)

			IF IS_ENTITY_DEAD(args.myTarget)
				PRINTLN("iRopeStages = 3")
				iRopeStages = 3				
			ENDIF
		
			IF DOES_ROPE_EXIST(ropeWindowA) AND DOES_ROPE_EXIST(ropeWindowB)
				START_ROPE_WINDING(ropeWindowA)
				START_ROPE_WINDING(ropeWindowB)
			
				IF vPlatformPosition.z >= 125.0
					
					IF curTargetStage < TARGET_STAGE_PANICKING
						IF NOT IS_PED_INJURED(args.myTarget)
							OPEN_SEQUENCE_TASK(tempSeq)
								TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@multi@windowwasher", "_up_to_wash")
								TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@multi@windowwasher", "_wash_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(tempSeq)
							TASK_PERFORM_SEQUENCE(args.myTarget, tempSeq)
							CLEAR_SEQUENCE_TASK(tempSeq)
						ENDIF
					ENDIF
					
					STOP_ROPE_WINDING(ropeWindowA)
					STOP_ROPE_WINDING(ropeWindowB)
				
					PRINTLN("iRopeStages = 3")
					iRopeStages = 3
					
					APPLY_FORCE_TO_ENTITY(args.myProp[0], APPLY_TYPE_EXTERNAL_FORCE, << -20.0, 0.0, -20.0 >>, << -4.78, 0.65, -0.78 >>, 0, FALSE, FALSE, TRUE )
					
//					INCREASE_ROPE_CONSTRAINT_DISTANCE(ropeWindowA, 0.1)
//					INCREASE_ROPE_CONSTRAINT_DISTANCE(ropeWindowB, 0.1)
				ENDIF
			ELSE
				IF DOES_ROPE_EXIST(ropeWindowA)
					STOP_ROPE_WINDING(ropeWindowA)
				ENDIF
				
				IF DOES_ROPE_EXIST(ropeWindowB)
					STOP_ROPE_WINDING(ropeWindowB)
				ENDIF
			ENDIF
		BREAK
		//===================================================================================================================
		CASE 3
//			bRopeWindingDone = TRUE
			IF DOES_ROPE_EXIST(ropeWindowA)
				STOP_ROPE_WINDING(ropeWindowA)
			ENDIF
			
			IF DOES_ROPE_EXIST(ropeWindowB)
				STOP_ROPE_WINDING(ropeWindowB)
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC



//do anything else needed for current target that CREATE_ASSASSIANTION_SCENE doesn't already do
PROC CREATE_ADDITIONAL_NEEDS_FOR_CURRENT_TARGET(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	SWITCH curTarget
		CASE ASSASSIN_TARGET_BODYBUILDER
			IF NOT IS_PED_INJURED(args.myTarget)
				SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
				SET_COMBAT_FLOAT(args.myTarget, CCF_FIGHT_PROFICIENCY, 1.0)
				ADD_PED_FOR_DIALOGUE(cutsceneConvStruct, 3, args.myTarget, "OJAmlBODYBUILDER")	//to have him speak in the intro cutscene
				ADD_PED_FOR_DIALOGUE(args.assConv, 3, args.myTarget, "OJAmlBODYBUILDER")
			ENDIF
			
			CREATE_BYSTANDER_PEDS()
		BREAK
		
		CASE ASSASSIN_TARGET_YACHT
			//create sunbathing girl on shore next to jetski
			IF NOT DOES_ENTITY_EXIST(args.myAuxPed) 
				args.myAuxPed = CREATE_PED(PEDTYPE_CIVFEMALE, targetData.auxPedEnum, targetData.vAuxPedPos, targetData.fAuxPedHead)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(args.myAuxPed)
				TASK_START_SCENARIO_AT_POSITION(args.myAuxPed, "WORLD_HUMAN_SUNBATHE", targetData.vAuxPedPos, targetData.fAuxPedHead )
				SET_PED_COMBAT_ATTRIBUTES(args.myAuxPed, CA_ALWAYS_FLEE, TRUE)
			ENDIF
			
			IF NOT IS_PED_INJURED(args.myOtherPed)	//girl on yacht
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(args.myOtherPed, TRUE)
				SET_PED_DIES_IN_WATER(args.myOtherPed, FALSE)
				SET_PED_COMPONENT_VARIATION(args.myOtherPed, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(args.myOtherPed, INT_TO_ENUM(PED_COMPONENT,2), 1, 2, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(args.myOtherPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 3, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(args.myOtherPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 3, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(args.myOtherPed, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			ENDIF
			
			IF DOES_ENTITY_EXIST(args.myVehicle)
				SET_VEHICLE_RADIO_ENABLED(args.myVehicle, TRUE)
				SET_VEHICLE_RADIO_LOUD(args.myVehicle, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(args.myVehicle, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(args.myTarget)
				SET_PED_DIES_IN_WATER(args.myTarget, FALSE)
				ADD_PED_FOR_DIALOGUE(cutsceneConvStruct, 3, args.myTarget, "OJAmlYACHTTARGET")	//to have him speak in the intro cutscene
				ADD_PED_FOR_DIALOGUE(args.assConv, 3, args.myTarget, "OJAmlYACHTTARGET")
				SET_ENTITY_LOAD_COLLISION_FLAG(args.myTarget, TRUE)
			ENDIF
			
//			IF NOT IS_ENTITY_DEAD(args.myTarget) AND NOT IS_ENTITY_DEAD(args.myVehicle)
//				SET_PED_INTO_VEHICLE(args.myTarget, args.myVehicle, VS_BACK_LEFT)
//			ENDIF
			
			viJetski = CREATE_VEHICLE(SEASHARK, << -2190.7407, -470.3163, -0.4779 >>, 308.1181)
			
			//to block the peds and jetskis parked next to the one we create
			REMOVE_SCENARIO_BLOCKING_AREA(sbi01)
			sbi01 = ADD_SCENARIO_BLOCKING_AREA((<< -2182.36279, -468.89261, -0.10296 >> - <<15,15,15>>), (<< -2182.36279, -468.89261, -0.10296 >> + <<15,15,15>>))
			
			iLineIDXToPlay = -1
			iDialogueIdleLine = 0
		BREAK
		
		CASE ASSASSIN_TARGET_WASHER
			
			IF NOT IS_PED_INJURED(args.myTarget)
			AND DOES_ENTITY_EXIST(args.myProp[0])
			AND DOES_ENTITY_EXIST(args.myProp[1])
				ADD_PED_FOR_DIALOGUE(args.assConv, 3, args.myTarget, "OJAmlWASHER")
				ATTACH_ENTITY_TO_ENTITY(args.myTarget, args.myProp[0], 0, << -2.1, -0.4, 0.25 >>, << 0, 0, 180 >>, TRUE, TRUE)
				TASK_PLAY_ANIM(args.myTarget, "oddjobs@assassinate@multi@windowwasher", "_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				
				SET_ENTITY_LOD_DIST(args.myProp[0], 225)
				SET_ENTITY_LOD_DIST(args.myProp[1], 225)
				
				PRINTLN("LOD DISTANCES SET FOR WASHER PROPS!")
			ENDIF
			
//			camKill = CREATE_CAMERA(CAMTYPE_SCRIPTED)
//			SET_CAM_PARAMS(camKill, << -648.3, 296.3, 134.3538 >>, << -12.6, -0.0000, -24.7813 >>, 35)
		BREAK
		
		CASE ASSASSIN_TARGET_BIKER
			IF NOT IS_PED_INJURED(args.myTarget)
				SET_PED_COMBAT_ATTRIBUTES(args.myTarget, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
				GIVE_PED_HELMET(args.myTarget, FALSE, PV_FLAG_DEFAULT_HELMET)
				SET_PED_HELMET(args.myTarget, TRUE)
				PRINTLN("GIVING BIKER HELMET")
				ADD_PED_FOR_DIALOGUE(cutsceneConvStruct, 3, args.myTarget, "OJAmlBIKER")	//to have him speak in the intro cutscene
				ADD_PED_FOR_DIALOGUE(args.assConv, 3, args.myTarget, "OJAmlBIKER")
				
				//start the biker on his bike
				IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
					SET_PED_INTO_VEHICLE(args.myTarget, args.myVehicle)
					SET_VEHICLE_ON_GROUND_PROPERLY(args.myVehicle)
				ENDIF
			ENDIF
			viRestStopCamper = CREATE_VEHICLE(CAMPER, << -1400.5751, 738.8264, 182.1478 >>, 194.6070)
			SET_VEHICLE_ON_GROUND_PROPERLY(viRestStopCamper)
			viRestStopCar = CREATE_VEHICLE(BJXL, <<-1371.2687, 734.4243, 182.5433>>, 182.7660)
			SET_VEHICLE_ON_GROUND_PROPERLY(viRestStopCar)
		BREAK
	ENDSWITCH
	
	
	//bump up the target's lockon to fix Bug 470575
	IF NOT IS_PED_INJURED(args.myTarget)
		SET_ENTITY_IS_TARGET_PRIORITY(args.myTarget, TRUE)
	ENDIF
ENDPROC


//reset these variables before activating the next target
PROC INITIALIZE_NEXT_STAGE(ASS_TARGET_DATA& targetData)
	INT tempInt
	
	bLocatePrinted = FALSE
	curTarget = INT_TO_ENUM(CURRENT_ASSASSIN_TARGET, iNumTargetsKilled)
	curTargetStage = TARGET_STAGE_BLIP_TARGET
	introCutsceneState = INTRO_CUTSCENE_CREATE
	cutArgs.cutsceneState = CUTSCENE_STATE_START
	
	//reset these variables
	targetData.vehEnum =  DUMMY_MODEL_FOR_SCRIPT
	targetData.auxVehEnum = DUMMY_MODEL_FOR_SCRIPT
	targetData.targetEnum = DUMMY_MODEL_FOR_SCRIPT
	targetData.vTargetPos = NULL_VECTOR()
	targetData.fTargetHead = 0
	targetData.otherPedEnum = DUMMY_MODEL_FOR_SCRIPT
	targetData.vOtherPedPos = NULL_VECTOR()
	targetData.auxPedEnum = DUMMY_MODEL_FOR_SCRIPT
	targetData.vAuxPedPos = NULL_VECTOR()
	targetData.fAuxPedHead = 0
	targetData.auxVehEnum = DUMMY_MODEL_FOR_SCRIPT
	targetData.vAuxVehPos = NULL_VECTOR()
	targetData.fAuxVehHead = 0
	targetData.vehEnum =  DUMMY_MODEL_FOR_SCRIPT
	targetData.vVehPos = NULL_VECTOR()
	targetData.fVehHead = 0
	targetData.vDestPos = NULL_VECTOR()
	targetData.vWarpPos = NULL_VECTOR()
	targetData.weapTarget = WEAPONTYPE_INVALID

	REPEAT maxProps tempInt
		targetData.vPropPos[tempInt] = NULL_VECTOR()
	ENDREPEAT
ENDPROC

////cutscene to show bodybuilder
//FUNC BOOL ASSASSIN_INTRO_CUSTOM_BODYBUILDER()
//	PED_INDEX pedToSpeak
//	
//	SWITCH introCutsceneState
//		CASE INTRO_CUTSCENE_CREATE
//			//create camera to interp to
//			cutCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1205.9753, -1549.2823, 5.8757 >>, << -4.5982, -0.0003, 170.5105 >>, 35)
//			SET_CAM_ACTIVE_WITH_INTERP(cutCam, myCamera, 10500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
//			SETTIMERA(0)
//			
//			//grab a nearby female bystander and have her talk during cutscene
//			pedToSpeak = GET_CLOSEST_BYSTANDER(FALSE, vFlexPos, 15)
//			IF pedToSpeak <> NULL	
//				ADD_PED_FOR_DIALOGUE(cutsceneConvStruct, 4, pedToSpeak, "OJAmlGymOnlookerF")
//				CREATE_CONVERSATION(cutsceneConvStruct, "OJASAUD", "OJASml_bbBY3", CONV_PRIORITY_HIGH)	//"Have you ever seen such a perfectly sculpted bod before? *whistles*
//			ENDIF
//			introCutsceneState = INTRO_CUTSCENE_TRANSITIONA
//		BREAK
//		CASE INTRO_CUTSCENE_TRANSITIONA
//			IF TIMERA() >= 7000
//				SET_CAM_PARAMS(cutCam, << -1211.1, -1557.95, 4.9 >>, << -1.4, -0.0003, -114.8 >>, 35)
//				SHAKE_CAM(cutCam, "HAND_SHAKE", 0.25)
//				SETTIMERA(0)
//				
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					CREATE_CONVERSATION(cutsceneConvStruct, "OJASAUD", "OJASml_bbId0", CONV_PRIORITY_HIGH)	//"Ugh! *grunts*" - Target
//				ENDIF
//				introCutsceneState = INTRO_CUTSCENE_TRANSITIONB
//			ENDIF
//		BREAK
//		CASE INTRO_CUTSCENE_TRANSITIONB
//			IF TIMERA() >= 2000
//				introCutsceneState = INTRO_CUTSCENE_END
//			ENDIF
//		BREAK
//		CASE INTRO_CUTSCENE_END
//			REMOVE_PED_FOR_DIALOGUE(cutsceneConvStruct, 3)
//			REMOVE_PED_FOR_DIALOGUE(cutsceneConvStruct, 4)
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
//		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//		REMOVE_PED_FOR_DIALOGUE(cutsceneConvStruct, 3)
//		REMOVE_PED_FOR_DIALOGUE(cutsceneConvStruct, 4)
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

////cutscene to show yacht target with girl
//FUNC BOOL ASSASSIN_INTRO_CUSTOM_YACHT()
//	CAMERA_INDEX tempCam
//
//	SWITCH introCutsceneState
//		CASE INTRO_CUTSCENE_CREATE
//			//create camera to interp to
//			tempCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -2172.7625, -446.5406, 5.9327 >>, << -6.3389, -0.0378, 166.4011 >>, 35)
//			SET_CAM_ACTIVE_WITH_INTERP(tempCam, myCamera, 6500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
//			SETTIMERA(0)
//			introCutsceneState = INTRO_CUTSCENE_TRANSITIONA
//		BREAK
//		CASE INTRO_CUTSCENE_TRANSITIONA
//			IF TIMERA() >= 4000
//				SET_CAM_PARAMS(myCamera,<< -2215.9033, -587.4893, 2.2662 >>, << -7.9346, -0.0000, -19.8626 >>, 35)
//				tempCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -2216.8484, -587.1091, 2.0171 >>, << -8.9524, -0.0000, -30.1767 >>, 35)
//				SET_CAM_ACTIVE_WITH_INTERP(tempCam, myCamera, 6500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
//				SETTIMERA(0)
//				CREATE_CONVERSATION(cutsceneConvStruct, "OJASAUD", "OJASml_ytT1", CONV_PRIORITY_HIGH)	//"Shake it girl! Don't be shy, get closer!"
//				introCutsceneState = INTRO_CUTSCENE_TRANSITIONB
//			ENDIF
//		BREAK
//		CASE INTRO_CUTSCENE_TRANSITIONB
//			IF TIMERA() >= 6000
//				introCutsceneState = INTRO_CUTSCENE_END
//			ENDIF
//		BREAK
//		CASE INTRO_CUTSCENE_END
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	
//	IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
//		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

////cutscene to show window washer
//FUNC BOOL ASSASSIN_INTRO_CUSTOM_WASHER()
//	CAMERA_INDEX tempCam
//	
//	SWITCH introCutsceneState
//		CASE INTRO_CUTSCENE_CREATE
//			tempCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -659.5249, 186.9299, 82.3559 >>, << 16.1452, 0.0001, -13.9383 >>, 35)
//			SET_CAM_ACTIVE_WITH_INTERP(tempCam, myCamera, 6500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
//			SETTIMERA(0)
//			introCutsceneState = INTRO_CUTSCENE_TRANSITIONA
//		BREAK
//		CASE INTRO_CUTSCENE_TRANSITIONA
//			IF TIMERA() >= 4000
//				SET_CAM_PARAMS(myCamera, << -631.8769, 297.3616, 133.1730 >>, << -2.7339, 0.0001, 57.6381 >>, 35)
//				tempCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -634.4907, 299.0185, 133.1846 >>, << -2.7339, 0.0001, 55.9964 >>, 35)
//				SET_CAM_ACTIVE_WITH_INTERP(tempCam, myCamera, 8500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
//				SETTIMERA(0)
//				introCutsceneState = INTRO_CUTSCENE_TRANSITIONB
//			ENDIF
//		BREAK
//		CASE INTRO_CUTSCENE_TRANSITIONB
//			IF TIMERA() >= 4000
//				introCutsceneState = INTRO_CUTSCENE_END
//			ENDIF
//		BREAK
//		CASE INTRO_CUTSCENE_END
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	
//	IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

////cutscene to show biker
//FUNC BOOL ASSASSIN_INTRO_CUSTOM_BIKER()
//	CAMERA_INDEX tempCam
//
//	SWITCH introCutsceneState
//		CASE INTRO_CUTSCENE_CREATE
//			//create camera to interp to
//			tempCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1402.3571, 737.4949, 183.9362 >>, << -0.2429, -0.0000, 66.1243 >>, 35)
//			SET_CAM_ACTIVE_WITH_INTERP(tempCam, myCamera, 8000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
//			SETTIMERA(0)
//			introCutsceneState = INTRO_CUTSCENE_TRANSITIONA
//		BREAK
//		CASE INTRO_CUTSCENE_TRANSITIONA
//			IF TIMERA() >= 3000
//				SET_CAM_PARAMS(myCamera, << -1407.5659, 736.5976, 184.1755 >>, << -1.8515, 0.0000, -13.3541 >>, 35)
//				tempCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1407.6451, 736.6487, 184.1740 >>, << -1.8515, 0.0000, -10.2147 >>, 35)
//				SET_CAM_ACTIVE_WITH_INTERP(tempCam, myCamera, 5500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
//				CREATE_CONVERSATION(cutsceneConvStruct, "OJASAUD", "OJASml_bkPEE", CONV_PRIORITY_HIGH)	//"I knew I shouldn't have chugged that mega Sprunk before i left"
//				SETTIMERA(0)
//				introCutsceneState = INTRO_CUTSCENE_END
//			ENDIF
//		BREAK
//		CASE INTRO_CUTSCENE_END
//			IF TIMERA() >= 6000
//				RETURN TRUE
//			ENDIF
//		BREAK
//	ENDSWITCH
//	
//	IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
//		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

////grab the cutscene info for the current target
//PROC GET_CURRENT_TARGET_CUTSCENE()
//	SWITCH curTarget
//		CASE ASSASSIN_TARGET_YACHT
//			fpCutsceneCustomIntro = &ASSASSIN_INTRO_CUSTOM_YACHT
//		BREAK
//		CASE ASSASSIN_TARGET_WASHER
//			fpCutsceneCustomIntro = &ASSASSIN_INTRO_CUSTOM_WASHER
//		BREAK
//		CASE ASSASSIN_TARGET_BIKER
//			fpCutsceneCustomIntro = &ASSASSIN_INTRO_CUSTOM_BIKER
//		BREAK
//		CASE ASSASSIN_TARGET_BODYBUILDER
//			fpCutsceneCustomIntro = &ASSASSIN_INTRO_CUSTOM_BODYBUILDER
//		BREAK
//	ENDSWITCH
//ENDPROC


//PROC SETUP_LOAD_SCENE_FOR_CHECKPOINT(VECTOR vPosition, FLOAT fRadius = 10.0)
//	BOOL bLoadSceneCalled = FALSE
//	
//	IF IS_SCREEN_FADED_OUT()
//	OR IS_REPLAY_IN_PROGRESS()
//		NEW_LOAD_SCENE_START_SPHERE(vPosition, fRadius)
//		bLoadSceneCalled = TRUE
//		PRINTLN("ASSASSINATION MULTI: CALLING NEW_LOAD_SCENE_START_SPHERE")
//	ELSE
//		PRINTLN("ASSASSINATION MULTI CHECKPOINT: SCREEN IS NOT FADED OUT")
//	ENDIF
//	
//	IF bLoadSceneCalled
//		WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
//			WAIT(0)
//			PRINTLN("ASSASSINATION MULTI: WAITING FOR SCENE TO LOAD")
//		ENDWHILE
//			
//		PRINTLN("ASSASSINATION MULTI: SCENE HAS LOADED")
//		NEW_LOAD_SCENE_STOP()
//	ENDIF
//ENDPROC


//do distance check to see if player is close enough to trigger the target cutscene
FUNC BOOL IS_PLAYER_IN_CUTSCENE_RANGE(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	
	SWITCH curTarget
		CASE ASSASSIN_TARGET_BODYBUILDER
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vDestPos) < fCutsceneTriggerDist
			OR GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < fCutsceneTriggerDist
				IF DOES_BLIP_EXIST(args.myTargetBlip)
					REMOVE_BLIP(args.myTargetBlip)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		CASE ASSASSIN_TARGET_YACHT
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vDestPos) < fCutsceneTriggerDist
			OR GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < fCutsceneTriggerDist
				IF DOES_BLIP_EXIST(args.myTargetBlip)
					REMOVE_BLIP(args.myTargetBlip)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		CASE ASSASSIN_TARGET_WASHER
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vDestPos) < fCutsceneTriggerDist
			OR GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < fCutsceneTriggerDist
				IF DOES_BLIP_EXIST(args.myTargetBlip)
					REMOVE_BLIP(args.myTargetBlip)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		CASE ASSASSIN_TARGET_BIKER
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vDestPos) < fCutsceneTriggerDist
			OR GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < ( fCutsceneTriggerDist + 50 )
				IF DOES_BLIP_EXIST(args.myTargetBlip)
					REMOVE_BLIP(args.myTargetBlip)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_REACTIONS_TO_TIME(ASS_ARGS& args)

	SWITCH iFranklinsReactionToTime
		CASE 0
			IF iTimeSoFar >= fMissionTime * 1/2
			AND NOT IS_MESSAGE_BEING_DISPLAYED()
				ADD_PED_FOR_DIALOGUE(args.assConv, 1, PLAYER_PED_ID(), "FRANKLIN")
				CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_TIME2", CONV_PRIORITY_HIGH)	// Ah shit, not much time left.  I can do this!
				iFranklinsReactionToTime = 1
				PRINTLN("iFranklinsReactionToTime = 1")
			ENDIF
		BREAK
//		CASE 1
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
////				PRINT_HELP("ASS_TIME1_TK")
//				iFranklinsReactionToTime = 2
//				PRINTLN("iFranklinsReactionToTime = 2")
//			ENDIF
//		BREAK
//		CASE 2
//			
////			IF bTimeToGoRed
////				CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASml_TIME1", CONV_PRIORITY_HIGH)	//"Shit I am runnin' out of time!
////				iFranklinsReactionToTime = 3
////				PRINTLN("iFranklinsReactionToTime = 3")
////			ENDIF
//		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC 

//handles the updating of the mission timer and logic to fail the mission if it expires before the player kills all the targets
PROC FAIL_MISSION_IF_TIMER_EXPIRES(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	//fail the mission if time expires
	IF curStage >= MISSION_STATE_INIT
	AND curStage <> MISSION_STATE_SUCCESS
		IF NOT bMissionTimerExpired
			UPDATE_MISSION_TIMER(iMissionTimerStart, iTimeSoFar, fMissionTime, bMissionTimerStarted, bMissionTimerExpired)
			UPDATE_REACTIONS_TO_TIME(args)
		ELSE
			SET_MISSION_FAILED(assArgs, targetData, FAIL_TIME_EXPIRED)
		ENDIF
	ENDIF
ENDPROC


FUNC FLOAT GET_PLAYERS_MULTI_MISSION_TIME()
	RETURN  TO_FLOAT(iTOTAL_TIME_ALLOTTED - iTimeToDisplayOnTimer) 
ENDFUNC


//checks to see if player has killed targets within allotted time set for bonus requirement
PROC CHECK_FOR_TIME_BONUS()
	FLOAT fBonusTime
	//set bonus to require the player to have a minute or greater left on the clock
	fBonusTime = (iTOTAL_TIME_ALLOTTED - 60000)
	PRINTLN("fBonusTime = ", fBonusTime)
	
	//check if player completed the mission with a minute or greater left
	IF GET_PLAYERS_MULTI_MISSION_TIME() <= fBonusTime
		PRINTLN("AWARDING SPEEDY KILLER BONUS")
		bBonus = TRUE
		passReasonCore = PASS_BONUS
	ELSE
		PRINTLN("GET_PLAYERS_MULTI_MISSION_TIME() = ", GET_PLAYERS_MULTI_MISSION_TIME())
	ENDIF

ENDPROC

//checks before intro cutscene has played for the current target
FUNC BOOL HAS_PLAYER_TRIGGERED_PREMATURE_PANIC_FOR_CURRENT_TARGET(ASS_ARGS& args)
	
	IF curTarget = 	ASSASSIN_TARGET_BODYBUILDER
		IF DO_AGGRO_CHECK(args.myTarget, NULL, aggroArgs, aggroReason, FALSE, FALSE)
		OR HAS_PLAYER_ATTACKED_BYSTANDERS(bAttackedBystanders)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vFlexPos, 25)
			PRINTLN("PLAYER HAS TRIGGERD PANIC - ASSASSIN_TARGET_BODYBUILDER")
			RETURN TRUE
		ENDIF
	ELIF curTarget = ASSASSIN_TARGET_YACHT
		IF DO_AGGRO_CHECK(args.myTarget, args.myVehicle, aggroArgs, aggroReason, FALSE, FALSE)
		OR DO_AGGRO_CHECK(args.myOtherPed, NULL, aggroArgs, aggroReason, FALSE, FALSE)
			PRINTLN("PLAYER HAS TRIGGERD PANIC - ASSASSIN_TARGET_YACHT")
			RETURN TRUE
		ENDIF
	ELSE
		IF DO_AGGRO_CHECK(args.myTarget, args.myVehicle, aggroArgs, aggroReason, FALSE, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//adjust fail distance variable if the cutscene was triggered at a distance larger than the original fFailDist value so the script wont immediately fail for losing the target after triggering the cutscene
PROC ADJUST_FAIL_DISTANCE_IF_NEEDED(PED_INDEX targetPed)
	IF GET_PLAYER_DISTANCE_FROM_ENTITY(targetPed, FALSE) > fFailDist
		fFailDist = GET_PLAYER_DISTANCE_FROM_ENTITY(targetPed, FALSE) + 50
	ENDIF
ENDPROC


/// PURPOSE:
///    Make sure that the player's last vehicle is set as a mission entity so that it wont get cleared up aggeressively by garbage collection while on the mission
PROC SAVE_LAST_PLAYER_VEHICLE()
	VEHICLE_INDEX vehLast
	
	IF curStage > MISSION_STATE_INIT
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vehLast = GET_LAST_DRIVEN_VEHICLE()
			
			IF DOES_ENTITY_EXIST(vehLast)
				IF IS_VEHICLE_DRIVEABLE(vehLast)
					IF vehLast != viPlayerLastVehicle
					AND NOT IS_ENTITY_IN_WATER(vehLast)
					AND NOT IS_ENTITY_A_MISSION_ENTITY(vehLast)
						
//						//clear out the previous one
//						IF viPlayerLastVehicle != NULL
//							IF IS_ENTITY_A_MISSION_ENTITY(viPlayerLastVehicle)
//								SET_VEHICLE_AS_NO_LONGER_NEEDED(viPlayerLastVehicle)
//							ENDIF
//						ENDIF
							
						//set the new vehicle as the current mission entity vehicle
						viPlayerLastVehicle = vehLast
						SET_ENTITY_AS_MISSION_ENTITY(viPlayerLastVehicle)
						SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(viPlayerLastVehicle, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PROC HANDLE_MUSIC_PREP()
//	IF IS_TIMER_STARTED(tPrepMusicTimer)
//		IF GET_TIMER_IN_SECONDS(tPrepMusicTimer) > 5.0
//			// Preparing music events
//			IF NOT bPrepareMusic
//				IF iNumTargetsKilled = 1
//					PREPARE_MUSIC_EVENT("ASS2_YCT_OS")
//					PRINTLN("PREPARING MUSIC EVENT - ASS2_YCT_OS")
//					bPrepareMusic = TRUE
//				ELIF iNumTargetsKilled = 2
//					PREPARE_MUSIC_EVENT("ASS2_WW_OS")
//					PRINTLN("PREPARING MUSIC EVENT - ASS2_WW_OS")
//					bPrepareMusic = TRUE
//				ELIF iNumTargetsKilled = 3
//					PREPARE_MUSIC_EVENT("ASS2_BKR_OS")
//					PRINTLN("PREPARING MUSIC EVENT - ASS2_BKR_OS")
//					bPrepareMusic = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

PROC HANDLE_LEAVE_AREA_TRIGGER()
	VECTOR vPlayerPosition

	IF IS_TIMER_STARTED(tLeaveAreaTimer)
		IF GET_TIMER_IN_SECONDS(tLeaveAreaTimer) > 30.0

			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			ENDIF
			
			IF iNumTargetsKilled = 1

				IF VDIST(vPlayerPosition, vTargetLocation[0]) < 100
				OR IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						PRINTLN("SETTING WANTED LEVEL - LEAVE AREA CHECK FAIL - 01")
					ENDIF
				ENDIF
			ELIF iNumTargetsKilled = 2

				IF VDIST(vPlayerPosition, vTargetLocation[1]) < 100
				OR IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						PRINTLN("SETTING WANTED LEVEL - LEAVE AREA CHECK FAIL - 02")
					ENDIF
				ENDIF
			ELIF iNumTargetsKilled = 3

				IF VDIST(vPlayerPosition, vTargetLocation[2]) < 100
				OR IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						PRINTLN("SETTING WANTED LEVEL - LEAVE AREA CHECK FAIL - 03")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC HANDLE_RADIO_NEWS_EVENTS()
	
	IF NOT bNewsReportPlayed
		IF IS_TIMER_STARTED(tRadioNewsTimer)
			IF GET_TIMER_IN_SECONDS(tRadioNewsTimer) > 40.0
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
					ADD_PED_FOR_DIALOGUE(cutsceneConvStruct, 8, NULL, "OJASNEWS")
					
					SWITCH iNumTargetsKilled
						CASE 1
							CREATE_CONVERSATION(cutsceneConvStruct, "OJASAUD", "OJAS_NEWS1", CONV_PRIORITY_HIGH)
						BREAK
						CASE 2
							CREATE_CONVERSATION(cutsceneConvStruct, "OJASAUD", "OJAS_NEWS2", CONV_PRIORITY_HIGH)
						BREAK
						CASE 3
							CREATE_CONVERSATION(cutsceneConvStruct, "OJASAUD", "OJAS_NEWS3", CONV_PRIORITY_HIGH)
						BREAK
						CASE 4
							CREATE_CONVERSATION(cutsceneConvStruct, "OJASAUD", "OJAS_NEWS4", CONV_PRIORITY_HIGH)
						BREAK
					ENDSWITCH
					
					bNewsReportPlayed = TRUE
					CANCEL_TIMER(tRadioNewsTimer)
				ENDIF
			ENDIF
		ELSE
			RESTART_TIMER_NOW(tRadioNewsTimer)
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL HAS_ENDING_CELL_PHONE_CALL_COMPLETED(ASS_ARGS& args)
	IF NOT IS_TIMER_STARTED(tDelayPhoneCallTimer)
		START_TIMER_NOW(tDelayPhoneCallTimer)
		PRINTLN("STARTING TIMER - tDelayPhoneCallTimer")
	ELSE
		IF NOT bPlayedCellPhoneCall
			IF GET_TIMER_IN_SECONDS(tDelayPhoneCallTimer) > 5.0
				
				ADD_PED_FOR_DIALOGUE(args.assConv, 1, PLAYER_PED_ID(), "FRANKLIN")
				ADD_PED_FOR_DIALOGUE(args.assConv, 3, NULL, "LESTER")
				
				IF PLAYER_CALL_CHAR_CELLPHONE(args.assConv, CHAR_LESTER, "OJASAUD", "OJAS_MULTI_C", CONV_MISSION_CALL)
					PRINTLN("MULIT - CELL PHONE CALL")
					bPlayedCellPhoneCall = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bPlayedCellPhoneCall AND HAS_CELLPHONE_CALL_FINISHED()
		
			PRINTLN("RETURNING TRUE - HAS_ENDING_CELL_PHONE_CALL_COMPLETED")
			RETURN TRUE
		ELSE
			PRINTLN("WAITING FOR CELL PHONE CALL TO FINISH")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC SEND_TEXT_MESSAGE_FOR_CURRENT_TARGET()
	STRING sTextMsg
	
	SWITCH curTarget
		CASE ASSASSIN_TARGET_BODYBUILDER
			sTextMsg = "ASS_ML_TXT_BB"
		BREAK
		CASE ASSASSIN_TARGET_YACHT
			sTextMsg = "ASS_ML_TXT_YT"
		BREAK
		CASE ASSASSIN_TARGET_WASHER
			sTextMsg = "ASS_ML_TXT_WW"
		BREAK
		CASE ASSASSIN_TARGET_BIKER
			sTextMsg = "ASS_ML_TXT_BK"
		BREAK
	ENDSWITCH
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sTextMsg)
		SEND_SILENT_TEXT_MESSAGE_TO_PLAYER_CHARACTER(CHAR_LESTER, SILENT_FRANKLIN, sTextMsg, TXTMSG_UNLOCKED)
//		SEND_TEXT_MESSAGE_TO_CHARACTER_BUFFER(CHAR_FRANKLIN, CHAR_LESTER, sTextMsg, TXTMSG_UNLOCKED)
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_TOUCHED_RIGHT_STICK()
	
	fRightX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) 
	fRightY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	
//	PRINTLN("fRightX = ", fRightX)
//	PRINTLN("fRightY = ", fRightY)
	
	IF fRightX > 0 
	OR fRightY > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ADJUST_CAMERA(CAMERA_INDEX & camToChange, FLOAT fTimeToSwitch, VECTOR vPosition, VECTOR vRotation, FLOAT fCamFOV, BOOL bUseShake = FALSE, FLOAT fShakeValue = 0.5)
	IF DOES_CAM_EXIST(camToChange)
		IF IS_TIMER_STARTED(tPayphoneCameraTimer)
			IF GET_TIMER_IN_SECONDS(tPayphoneCameraTimer) >= fTimeToSwitch
				SET_CAM_COORD(camPayPhoneIntro01, vPosition)
				SET_CAM_ROT(camPayPhoneIntro01, vRotation)
				SET_CAM_FOV(camPayPhoneIntro01, fCamFOV)
				
				IF bUseShake
					SHAKE_CAM(camPayPhoneIntro01, "HAND_SHAKE", fShakeValue)
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_INTRO_CAMERAS()
	
	SWITCH iIntroCameraStages
		CASE 0
			IF ADJUST_CAMERA(camPayPhoneIntro01, 8.0, <<-728.4891, -933.7799, 19.9947>>, <<-4.0865, 0.0581, -61.3739>>, 30.7441, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 1")
				iIntroCameraStages = 1
			ENDIF
		BREAK
		CASE 1
			IF ADJUST_CAMERA(camPayPhoneIntro01, 14.0, <<-702.2474, -918.3937, 19.7582>>, <<-1.9904, 0.0000, -56.7173>>, 30.0, TRUE, 0.3)
				PRINTLN("iIntroCameraStages = 2")
				iIntroCameraStages = 2
			ENDIF
		BREAK
		CASE 2
			IF ADJUST_CAMERA(camPayPhoneIntro01, 23.0, <<-698.2410, -917.8708, 19.5330>>, <<-3.3396, -0.0000, 75.4344>>, 40.0, TRUE, 0.3)
				PRINTLN("iIntroCameraStages = 3")
				iIntroCameraStages = 3
			ENDIF
		BREAK
		CASE 3 // Tighten the Fov on this one
			IF ADJUST_CAMERA(camPayPhoneIntro01, 28.0, <<-716.4577, -945.2582, 19.8525>>, <<-1.1558, -0.0000, -21.0827>>, 31.0, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 4")
				iIntroCameraStages = 4
			ENDIF
		BREAK
		CASE 4
			IF ADJUST_CAMERA(camPayPhoneIntro01, 33.0, <<-702.0552, -917.9715, 19.7381>>, <<-3.1893, 0.0000, -63.3392>>, 33.4611, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 5")
				iIntroCameraStages = 5
			ENDIF
		BREAK
		CASE 5
			IF ADJUST_CAMERA(camPayPhoneIntro01, 39.0, <<-697.3740, -918.0989, 19.8386>>, <<-4.5194, 0.0000, 86.2972>>, 42.0, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 6")
				iIntroCameraStages = 6
			ENDIF
		BREAK
		CASE 6
		
		BREAK
	ENDSWITCH
	
ENDPROC

PROC REMOVE_SKINNED_PHONE_PROP()
	VECTOR vPlayerPosition
	
	IF NOT DOES_ENTITY_EXIST(oPayPhoneAnimated)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	IF VDIST2(vPlayerPosition, vPhoneLocation) > 10000 // 100m
		IF DOES_ENTITY_EXIST(oPayPhoneAnimated)
			SET_MODEL_AS_NO_LONGER_NEEDED(P_PHONEBOX_01B_S)
			DELETE_OBJECT(oPayPhoneAnimated)
			PRINTLN("DELETING OBJECT - oPayPhoneAnimated")
		ENDIF
		
		IF DOES_ENTITY_EXIST(oPayPhone)
			SET_ENTITY_VISIBLE(oPayPhone, TRUE)
			SET_ENTITY_COLLISION(oPayPhone, TRUE)
			PRINTLN("TURNING BACK ON VISIBILITY AND COLLISION")
		ENDIF
	ENDIF
ENDPROC

PROC DO_STAGE_INTRO_CUTSCENE(ASS_ARGS& args)

//	PRINTLN("TIME: ", TIMERB())
//	IF STREAMVOL_HAS_LOADED(svPhoneCutscene)
//		PRINTLN("WE'RE LOADED")
//	ENDIF
	
	IF payPhoneCutsceneStages < PAYPHONE_CUTSCENE_CLEANUP
		SET_HD_AREA(<< -717.83478, -932.17358, 18.01735 >>, 26.0)
//		PRINTLN("SET_HD_AREA of 26.0 m around << -718.92120, -932.86932, 18.01735 >>")
	ENDIF
	
	
	
	SWITCH payPhoneCutsceneStages
	
		CASE PAYPHONE_CUTSCENE_INIT
			REQUEST_ANIM_DICT("oddjobs@assassinate@multi@call")
			PRINTLN("REQUESTING ANIM DICTIONARY - oddjobs@assassinate@multi@call")
			REQUEST_MODEL(P_PHONEBOX_01B_S)
			PRINTLN("REQUESTING MODEL - P_PHONEBOX_01B_S")
					
			INIT_PARKED_VEHICLE_MODELS_FOR_GAS_STATION()
			
			SETTIMERA(0)
			
//			WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\ASSASSINATION_MULTI") AND TIMERA() < 5000
//				PRINTLN("SCRIPT\\ASSASSINATION_MULTI - Waiting on audio bank to load")
//				WAIT(0)
//			ENDWHILE
//			PRINTLN("Loaded audio bank SCRIPT\\ASSASSINATION_MULTI")
//			
			REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\ASSASSINATION_MULTI")
									
			IF HAS_ANIM_DICT_LOADED("oddjobs@assassinate@multi@call") AND HAS_MODEL_LOADED(P_PHONEBOX_01B_S)
			AND ARE_PARKED_VEHICLE_MODELS_STREAMED()
				bContinueWithScene = TRUE
			ENDIF
		
			IF bContinueWithScene
				IF DOES_ENTITY_EXIST(oPayPhone)
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-736.790771,-924.130310,18.163729>>, 
								<<-696.861389,-923.751709,19.658636>>, 45.0, <<-711.86017, -920.71411, 18.01450>>, 0.0954, 
								GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
					PRINTLN("REPOSITIONING VEHICLE")
					
					CLEAR_AREA(<<-700.122620,-916.869873,18.1667>>, 5.0, TRUE)
					
					IF NOT IS_TIMER_STARTED(tPayphoneCameraTimer)
						START_TIMER_NOW(tPayphoneCameraTimer)
						PRINTLN("STARTING TIMER - tPayphoneCameraTimer")
					ENDIF
					
					IF DOES_ENTITY_EXIST(oPayPhone)

						SET_ENTITY_VISIBLE(oPayPhone, FALSE)
						SET_ENTITY_COLLISION(oPayPhone, FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(oPayPhone, FALSE)
						
						PRINTLN("TURNING OFF VISIBILITY AND COLLISION")
						
//						oPayPhoneAnimated = CREATE_OBJECT(P_PHONEBOX_01B_S, <<-700.122620,-916.869873,18.214081>>) // Having to lower prop for some reason?! ... to fix Bug # 1352784
						oPayPhoneAnimated = CREATE_OBJECT(P_PHONEBOX_01B_S, <<-700.122620,-916.869873,18.1667>>)
						SET_ENTITY_ROTATION(oPayPhoneAnimated, <<0.000000,0.000000,0.000000>>)
						SET_ENTITY_COLLISION(oPayPhoneAnimated, FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(oPayPhoneAnimated, FALSE)
						
						PRINTLN("CREATING SKINNED VERSION OF PROP")
					ENDIF
					
					svPhoneCutscene = STREAMVOL_CREATE_SPHERE(<<-700.122620,-916.869873,18.1667>>, 350, FLAG_MAPDATA)
					PRINTLN("CREATING STREAM VOLUME - svPhoneCutscene")
					
					// Start the cutscene.
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerWarpPosition)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerWarpHeading)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
						PRINTLN("MOVING FRANKLIN TO PHONE")
					ENDIF
					
					iSceneIdPayPhone = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
					
					IF DOES_ENTITY_EXIST(oPayPhoneAnimated)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneIdPayPhone, oPayPhoneAnimated, -1)
						PRINTLN("oPayPhone EXISTS, ATTACHING SCENE TO IT")
					ELSE
						PRINTLN("NOTHING EXISTS")
					ENDIF
					
					// Franklin Animation
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneIdPayPhone, "oddjobs@assassinate@multi@call", "ass_multi_target_call_p1", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT)
					PRINTLN("TASKING FRANKLIN TO TALK ON THE PHONE")
					
					// Phone Prop Animation
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oPayPhoneAnimated, iSceneIdPayPhone, "ass_multi_target_call_phone", "oddjobs@assassinate@multi@call", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					PRINTLN("PLAYING ANIMATION ON PAYPHONE PROP")
					
					// Camera Creation
					IF NOT DOES_CAM_EXIST(camPayPhoneIntro01)
						camPayPhoneIntro01a = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCameraPosition01a, vCameraRotation01a, 35.0, TRUE) // Change to animated once cam is online
						camPayPhoneIntro01 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCameraPosition01, vCameraRotation01, 35.0, FALSE) // Change to animated once cam is online
						SET_CAM_ACTIVE_WITH_INTERP(camPayPhoneIntro01, camPayPhoneIntro01a, 2500)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						PRINTLN("CREATING CAMERA - camPayPhoneIntro01")
						SHAKE_CAM(camPayPhoneIntro01, "HAND_SHAKE", 0.2)
					ENDIF
					
					CREATE_VEHICLES_AT_GAS_STATION_OF_PHONE_CUTSCENE()
					
					// Camera Animation
//					PLAY_SYNCHRONIZED_CAM_ANIM(camPayPhoneIntro01, iSceneIdPayPhone, "ass_multi_target_call_cam", "oddjobs@assassinate@multi@call")
//					PRINTLN("PLAYING ANIMATION ON CAMERA")
					
					// Setup speakers
					ADD_PED_FOR_DIALOGUE(args.assConv, 3, NULL, "LESTER")
					ADD_PED_FOR_DIALOGUE(args.assConv, 1, PLAYER_PED_ID(), "FRANKLIN")
					
					// Turn off HUD elements
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
//					DISABLE_NAVMESH_IN_AREA(<< vPhoneLocation.x-20, vPhoneLocation.y-20, vPhoneLocation.z-20 >>, << vPhoneLocation.x+20, vPhoneLocation.y+20, vPhoneLocation.z+20 >>, TRUE)
				   
				   	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS | SPC_REMOVE_FIRES | SPC_REMOVE_PROJECTILES) // | SPC_LEAVE_CAMERA_CONTROL_ON)
					PRINTLN("REMOVING PLAYER CONTROL")
					DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
				   
				   	payPhoneCutsceneStages = PAYPHONE_CUTSCENE_UPDATE
					PRINTLN("MULTI: GOING TO STATE - PAYPHONE_CUTSCENE_UPDATE")
				ELSE
					REQUEST_ANIM_DICT("oddjobs@assassinate@multi@call")
					PRINTLN("REQUESTING ANIM DICTIONARY - oddjobs@asssasinate@multi@call")
					REQUEST_MODEL(P_PHONEBOX_01B_S)
					PRINTLN("REQUESTING MODEL - P_PHONEBOX_01B_S")
					
					IF NOT DOES_ENTITY_EXIST(oPayPhone)
						oPayPhone = GET_CLOSEST_OBJECT_OF_TYPE(vPhoneLocation, 5.0, prop_phonebox_01b)
						PRINTLN("MULTI: PAYPHONE WAS NOT FOUND IN TRIGGER SCENE, FIND IN SCRIPT")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_UPDATE
		
			HANDLE_INTRO_CAMERAS()
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			ENDIF
			
			IF IS_TIMER_STARTED(tPayphoneCutsceneTimer)
				IF GET_TIMER_IN_SECONDS(tPayphoneCutsceneTimer) > 1.0
					IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
						bSkipCutscene = TRUE
						
						IF NOT IS_SCREEN_FADED_OUT()
							FADE_DOWN()
						ENDIF
						
						payPhoneCutsceneStages = PAYPHONE_CUTSCENE_SKIP
						PRINTLN("MULTI: GOING TO STATE - PAYPHONE_CUTSCENE_SKIP")
						BREAK
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT bPlayedPayPhoneConvo
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneIdPayPhone)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneIdPayPhone) > 0.05
							IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAScnt_ml", CONV_PRIORITY_VERY_HIGH)
								IF NOT IS_TIMER_STARTED(tPayphoneCutsceneTimer)
									START_TIMER_NOW(tPayphoneCutsceneTimer)
									PRINTLN("STARTING TIMER - tPayphoneCutsceneTimer")
									bPlayedPayPhoneConvo = TRUE
									
									IF IS_REPEAT_PLAY_ACTIVE()
										FADE_IN()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF (IS_TIMER_STARTED(tPayphoneCutsceneTimer) AND GET_TIMER_IN_SECONDS(tPayphoneCutsceneTimer) > fCutsceneLength)
			OR (IS_SYNCHRONIZED_SCENE_RUNNING(iSceneIdPayPhone) AND GET_SYNCHRONIZED_SCENE_PHASE(iSceneIdPayPhone) >= 0.978)
			OR bSkipCutscene
				payPhoneCutsceneStages = PAYPHONE_CUTSCENE_CLEANUP
				PRINTLN("MULTI: GOING TO STATE - PAYPHONE_CUTSCENE_CLEANUP")
			ENDIF
			
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_SKIP
		
			STOP_SCRIPTED_CONVERSATION(FALSE)
		
			IF bSkipCutscene
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-700.2628, -917.5313, 18.2147>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 73.3924)
						PRINTLN("MOVING FRANKLIN - SKIP CUTSCENE")
					ENDIF
					
					STOP_SYNCHRONIZED_ENTITY_ANIM (oPayPhoneAnimated, INSTANT_BLEND_OUT, TRUE)
					
					WAIT(0)
					DO_SCREEN_FADE_IN(500)
					
					DESTROY_ALL_CAMS()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
										
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
					payPhoneCutsceneStages = PAYPHONE_CUTSCENE_CLEANUP
					PRINTLN("VICE: GOING TO STATE - PAYPHONE_CUTSCENE_CLEANUP")
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_CLEANUP
		
			IF NOT bSkipCutscene
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				DESTROY_ALL_CAMS()
			ENDIF
			
			IF STREAMVOL_IS_VALID(svPhoneCutscene)
				STREAMVOL_DELETE(svPhoneCutscene)
				PRINTLN("DELETING STREAM VOLUME - svPhoneCutscene VIA PAYPHONE_CUTSCENE_CLEANUP")
			ENDIF
			
			SET_ENTITY_COLLISION(oPayPhoneAnimated, TRUE)
			
			CLEAR_HD_AREA()
			
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE)
			DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
			//to fix Bug 1173621 - (3/12/13) - MB
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			
			REMOVE_ANIM_DICT("oddjobs@assassinate@multi@call")
			SET_OBJECT_AS_NO_LONGER_NEEDED(oPayPhone)
			
			curStage = MISSION_STATE_INIT
			PRINTLN("MULTI: GOING TO STATE - MISSION_STATE_INIT")
		BREAK
	ENDSWITCH
ENDPROC



//main update loop for the assassination
FUNC BOOL UPDATE_ASSASSINATION_MULTI(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
//	INT iWantedLevel = 0
	SEQUENCE_INDEX tempSeq

	SWITCH curStage
//		CASE MISSION_STATE_TIME_LAPSE
//			timeLapseCutscene()
//		BREAK
		
		CASE MISSION_BRIEF_INIT
			IF Is_Replay_In_Progress()
				curStage = MISSION_STATE_INIT
				BREAK
			ENDIF
			
			DO_STAGE_INTRO_CUTSCENE(args)
		BREAK
		
		//print the objectives and help messages immediately after launching the mission
		CASE MISSION_STATE_INIT
			targetData.myType = MISSION_NAME_MULTI
			
			IF bReplaying
			AND NOT bCheckpointDataSet	
				//handle checkpoint start pos, wanted level, player vehicle position (if applicable), and time remaining
				
//				IF g_savedGlobals.sAssassinData.fMultiMissionTime <> 0
//					RESTART_TIMER_AT(tMissionTime, g_savedGlobals.sAssassinData.fMultiMissionTime)
//					PRINTLN("RESTARTING TIMER - missionTimer AT: ", g_savedGlobals.sAssassinData.fMultiMissionTime)	
//				ENDIF
				
				//assign the iCheckpointSavedTime based on the saved global variable
				iCheckpointSavedTime = g_replay.iReplayInt[0]
				PRINTLN("iCheckpointSavedTime = ", iCheckpointSavedTime)
				
				
				IF iStageToUse = 0
					iNumTargetsKilled = 0
					fMissionTime = fMissionTime		
				
				ELIF iStageToUse = 1
					iNumTargetsKilled = 1			
					
					PRINTLN("g_savedGlobals.sAssassinData.fMultiMissionTime = ", g_savedGlobals.sAssassinData.fMultiMissionTime) 
						
					IF iCheckpointSavedTime < (fMissionTime) * 3/4
					OR bPlayerHasShitSkipped
						fMissionTime = (fMissionTime * 3/4)
					ELSE
						fMissionTime = TO_FLOAT(iCheckpointSavedTime)
					ENDIF
					PRINTLN("iStageToUse = 1 - fMissionTime = ", fMissionTime)
					
				ELIF iStageToUse = 2
					iNumTargetsKilled = 2
					
					PRINTLN("g_savedGlobals.sAssassinData.fMultiMissionTime = ", g_savedGlobals.sAssassinData.fMultiMissionTime) 
						
					IF iCheckpointSavedTime < (fMissionTime) * 1/2
					OR bPlayerHasShitSkipped
						fMissionTime = (fMissionTime * 1/2)
					ELSE
						fMissionTime = TO_FLOAT(iCheckpointSavedTime)
					ENDIF
					
					PRINTLN("iStageToUse = 2 - fMissionTime = ", fMissionTime)

				ELIF iStageToUse = 3
					iNumTargetsKilled = 3
					
					PRINTLN("g_savedGlobals.sAssassinData.fMultiMissionTime = ", g_savedGlobals.sAssassinData.fMultiMissionTime) 
					
					IF iCheckpointSavedTime < (fMissionTime) * 1/4
					OR bPlayerHasShitSkipped
						fMissionTime = (fMissionTime * 1/4)
					ELSE
						fMissionTime = TO_FLOAT(iCheckpointSavedTime)
					ENDIF
					PRINTLN("iStageToUse = 3 - fMissionTime = ", fMissionTime)

				ELIF iStageToUse = 4	
					fMissionTime = iTOTAL_TIME_ALLOTTED
					PRINTLN("iStageToUse = 4 - fMissionTime = ", fMissionTime)
				ENDIF

				START_TIMER_NOW(tMissionTime)
				
				//set the current target
				IF iStageToUse < 4
					curTarget = INT_TO_ENUM(CURRENT_ASSASSIN_TARGET, iNumTargetsKilled)
				ENDIF
				
				bCheckpointDataSet = TRUE
			ELSE
				IF NOT IS_TIMER_STARTED(tMissionTime)
					START_TIMER_NOW(tMissionTime)
					PRINTLN("STARTING TIMER - tMissionTime")
					
					g_savedGlobals.sAssassinData.fMultiMissionTime = 0
					PRINTLN("MULTI GLOBAL MISSION TIMER = ", g_savedGlobals.sAssassinData.fMultiMissionTime)
				ENDIF
			ENDIF
			
//			IF iNumTargetsKilled = 0
//				PREPARE_MUSIC_EVENT("ASS2_BB_OS")
//				PRINTLN("PREPARING MUSIC EVENT - ASS2_BB_OS")
//			ENDIF
			
//			// Timer for music prep
//			IF NOT IS_TIMER_STARTED(tPrepMusicTimer)
//				START_TIMER_NOW(tPrepMusicTimer)
//				PRINTLN("STARTING TIMER - tPrepMusicTimer")
//			ELSE
//				RESTART_TIMER_NOW(tPrepMusicTimer)
//				PRINTLN("RESTARTING TIMER - tPrepMusicTimer")
//				IF bPrepareMusic
//					bPrepareMusic = FALSE
//					PRINTLN("bPrepareMusic = FALSE")
//				ENDIF
//			ENDIF
				
			
			IF iStageToUse < 4
				//initialize necessities for mission and create peds/vehicles/props for current assassination scene
				INIT_CURRENT_TARGET_DATA(targetData)
				INIT_LEAVE_AREA_LOCATIONS()
			ENDIF
			
			//Do any necessary requests and check to see if a replay vehicle should be created
			IF bReplayVehicleAvailable
				INIT_MISSION_REQUESTS_AND_UI_LABELS(targetData, sLoadQueue, TRUE)
				CLEAR_AREA(vVehReplayStartPos, 5.0, TRUE)
				
				//create the replay vehicle but make sure it isnt a water based vehicle (sso we dont spawn it on land)
				VEHICLE_INDEX vehPlayer
				vehPlayer = CREATE_REPLAY_CHECKPOINT_VEHICLE(vVehReplayStartPos, fVehReplayStartHead)
				IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
				ELSE
					DELETE_VEHICLE(vehPlayer)
				ENDIF
			ELSE
				INIT_MISSION_REQUESTS_AND_UI_LABELS(targetData, sLoadQueue)
			ENDIF
			
			IF curTarget = ASSASSIN_TARGET_BODYBUILDER
			AND iStageToUse != 4
				//create these if they haven't already been created
				CREATE_VEHICLES_AT_GAS_STATION_OF_PHONE_CUTSCENE()
			ENDIF
			
			IF bReplaying
				END_REPLAY_SETUP()
				
				IF IS_SCREEN_FADED_OUT()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
			ENDIF
			
			//make sure game fades in (if retrying mission)
			FADE_IN()
			
			IF iStageToUse < 4
				curStage = MISSION_STATE_STREAMING
				PRINTLN("curStage = MISSION_STATE_STREAMING")
			ELSE
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				curStage = MISSION_STATE_SUCCESS
				PRINTLN("curStage = MISSION_STATE_SUCCESS")
			ENDIF
		BREAK
		
		CASE MISSION_STATE_STREAMING
			IF ARE_MISSION_REQUESTS_FINISHED_STREAMING(targetData, sLoadQueue)
			AND IS_CURRENT_TARGET_DATA_LOADED_NO_PEDS(targetData)
				PRINTLN("GOING TO STATE - MISSION_STATE_PRINTS")
				curStage = MISSION_STATE_PRINTS
			ENDIF
		BREAK
		
		CASE MISSION_STATE_PRINTS
		
			SETTIMERA(0)
			
			SEND_TEXT_MESSAGE_FOR_CURRENT_TARGET()
			
			DO_SET_CHECKPOINT()
			
			//STORE OFF THE CHECKPOINT TIMER AS THE GLOBAL MISSION VARIABLE SO THAT IT CAN BE ACCESSED ON REPLAYS
			iCheckpointSavedTime = iTimeToDisplayOnTimer
			g_replay.iReplayInt[0] = iCheckpointSavedTime	//store this off here so it can be accessed on replays
			PRINTLN("STORING OFF iCheckpointSavedTime AS: ", iCheckpointSavedTime)
		
			// blip location and set route
			IF NOT DOES_BLIP_EXIST(args.myTargetBlip)
				args.myTargetBlip = ADD_BLIP_FOR_COORD(targetData.vDestPos)
				SET_BLIP_ROUTE(args.myTargetBlip, TRUE)
				SET_BLIP_ROUTE_COLOUR(args.myTargetBlip, BLIP_COLOUR_RED)
				SET_BLIP_COLOUR(args.myTargetBlip, BLIP_COLOUR_RED)
				PRINTLN("CREATING BLIP MULTI - args.myTargetBlip")
			ENDIF
			
			curStage = MISSION_STATE_TRAVEL
			PRINTLN("curStage = MISSION_STATE_TRAVEL")
		BREAK
		
		CASE MISSION_STATE_TRAVEL
		
			REMOVE_SKINNED_PHONE_PROP()
			
//			HANDLE_RADIO_NEWS_EVENTS()
			
			IF NOT bGaveTextMessage
				IF TIMERA() > 15000
					DO_CURRENT_TARGET_OBJECTIVE_PRINT() // Moving to fix Bug # 1487350 - DS
					bGaveTextMessage = TRUE
				ENDIF
			ENDIF
			
			IF iNumTargetsKilled = 0
				GRAB_RETRY_VEHICLE()
			ENDIF
			
			// Special check to see if the player is close enough to attach the ropes to the window washer prop.
			ATTACH_ROPE_TO_PROPS(targetData, args)
			
			IF bAttachedRope
				HANDLE_SHOT_ROPE()
			ENDIF
			
//			HANDLE_MUSIC_PREP()
			HANDLE_LEAVE_AREA_TRIGGER()
			
			
			//remove the yacht from memory if the player has killed the yacht target and is a good distance away from the yacht
			IF curTarget >= ASSASSIN_TARGET_WASHER
				IF DOES_ENTITY_EXIST(viYacht)
					IF GET_DISTANCE_BETWEEN_ENTITIES(viYacht, PLAYER_PED_ID()) > 500
						SET_VEHICLE_AS_NO_LONGER_NEEDED(viYacht)
					ENDIF
				ENDIF
			ENDIF
						
			
			IF NOT bPedsForCurrentTargetCreated			
				IF curTarget = ASSASSIN_TARGET_BODYBUILDER
					IF GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vDestPos) < 400
					OR GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vTargetPos) < 400
						IF IS_BODYBUILDER_DATA_LOADED_PEDS_ONLY(targetData)
							CREATE_ASSASSINATION_SCENE(targetData, assArgs)
							CREATE_ADDITIONAL_NEEDS_FOR_CURRENT_TARGET(targetData, assArgs)
							
							bPedsForCurrentTargetCreated = TRUE
						ENDIF
					ENDIF
				ELIF curTarget = ASSASSIN_TARGET_YACHT
					IF GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vDestPos) < 400
					OR GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vTargetPos) < 400
						IF IS_YACHT_DATA_LOADED_PEDS_ONLY(targetData)
							CREATE_ASSASSINATION_SCENE(targetData, assArgs)
							CREATE_ADDITIONAL_NEEDS_FOR_CURRENT_TARGET(targetData, assArgs)
							
							bPedsForCurrentTargetCreated = TRUE
						ENDIF
					ENDIF
				ELIF curTarget = ASSASSIN_TARGET_WASHER
					IF GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vDestPos) < 400
					OR GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vTargetPos) < 400
						IF IS_WASHER_DATA_LOADED_PEDS_ONLY(targetData)
							CREATE_ASSASSINATION_SCENE(targetData, assArgs)
							CREATE_ADDITIONAL_NEEDS_FOR_CURRENT_TARGET(targetData, assArgs)
							
							bPedsForCurrentTargetCreated = TRUE
						ENDIF
					ENDIF
				ELIF curTarget = ASSASSIN_TARGET_BIKER
					IF GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vDestPos) < 500
					OR GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vTargetPos) < 500
						IF IS_BIKER_DATA_LOADED_PEDS_ONLY(targetData)
							CREATE_ASSASSINATION_SCENE(targetData, assArgs)
							CREATE_ADDITIONAL_NEEDS_FOR_CURRENT_TARGET(targetData, assArgs)
							
							bPedsForCurrentTargetCreated = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF CHECK_FOR_TARGET_DEATH(assArgs, targetData)
				OR ( bRopeDetached = TRUE AND curTarget = ASSASSIN_TARGET_WASHER)
					IF curTarget = ASSASSIN_TARGET_BODYBUILDER
						TASK_BYSTANDERS_TO_FLEE(TRUE)
					ENDIF
					curStage = MISSION_STATE_KILLCAM
					PRINTLN("curStage = MISSION_STATE_KILLCAM")
				ELSE
					IF NOT HAS_PLAYER_TRIGGERED_PREMATURE_PANIC_FOR_CURRENT_TARGET(assArgs)
						IF IS_PLAYER_IN_CUTSCENE_RANGE(targetData, args)
						OR ( IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), args.myTarget) AND GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 150.0 )
						OR ( IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), args.myTarget) AND GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 150.0 )
							ADJUST_FAIL_DISTANCE_IF_NEEDED(args.myTarget)
							
	//						curStage = MISSION_STATE_CUTSCENE_SETUP
	//						PRINTLN("curStage = MISSION_STATE_CUTSCENE_SETUP")
							
							curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
							PRINTLN("curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL")
						ELSE
							//play necessary anims for the target so they aren't seen standing still prior to triggering the cutscene
							IF curTarget = ASSASSIN_TARGET_BODYBUILDER
								IF NOT bWarning
									UPDATE_BODYBUILDER_ANIMS(targetData, args)
								ENDIF
							ELIF curTarget = ASSASSIN_TARGET_YACHT
								UPDATE_YACHT_PEDS(args)
							ENDIF
						ENDIF
					ELSE
						bPanicking = TRUE
						ADJUST_FAIL_DISTANCE_IF_NEEDED(args.myTarget)
						curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
						PRINTLN("PED PANICKING - BYPASSING CUTSCENE AND MOVING STRAIGHT TO MISSION_STATE_WAITING_FOR_TARGET_KILL")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
//		//intro cutscene has been triggered - set it up properly
//		CASE MISSION_STATE_CUTSCENE_SETUP
//			//setup cutscene
//			/* To Fix Bug # 565173 - for some reason if you disable vehicle controls while on an incline, 
//			 your car will float up in the air.  Need to find a better solution for this.*/
//			IF curTarget = ASSASSIN_TARGET_WASHER
//				IF IS_PLAYER_READY_FOR_ASSASSINATION_CUTSCENE_WASHER()
//					//reset washer to make sure he is playing his anims during his cutscene in next stage (to fix Bug 535924)
////					IF curTarget = ASSASSIN_TARGET_WASHER
////						IF NOT IS_PED_INJURED(args.myTarget)
////							CLEAR_PED_TASKS(args.myTarget)
////							SET_ENTITY_COORDS(args.myTarget, targetData.vTargetPos)
////							SET_ENTITY_HEADING(args.myTarget, targetData.fTargetHead)
////						ENDIF
////					ENDIF
//					
////					GET_CURRENT_TARGET_CUTSCENE()
//					GET_ASSASSIN_CUTSCENE_START_TIME()
//					curStage = MISSION_STATE_CUTSCENE
//					PRINTLN("curStage = MISSION_STATE_CUTSCENE")
//				ENDIF
//			ELSE
//				IF IS_PLAYER_READY_FOR_ASSASSINATION_CUTSCENE()
//					//reset washer to make sure he is playing his anims during his cutscene in next stage (to fix Bug 535924)
////					IF curTarget = ASSASSIN_TARGET_WASHER
////						IF NOT IS_PED_INJURED(args.myTarget)
////							CLEAR_PED_TASKS(args.myTarget)
////							SET_ENTITY_COORDS(args.myTarget, targetData.vTargetPos)
////							SET_ENTITY_HEADING(args.myTarget, targetData.fTargetHead)
////						ENDIF
////					ENDIF
//					
////					GET_CURRENT_TARGET_CUTSCENE()
//					GET_ASSASSIN_CUTSCENE_START_TIME()
//					
//					curStage = MISSION_STATE_CUTSCENE
//					PRINTLN("curStage = MISSION_STATE_CUTSCENE")
//				ENDIF
//			ENDIF
//		BREAK
		
//		//intro cutscene has been triggered - set it up properly
//		CASE MISSION_STATE_CUTSCENE
//			IF DO_CUTSCENE_CUSTOM(cutArgs.cutsceneState, cutArgs.cutSceneTimer, targetData.vCutscenePos, targetData.vCutsceneHead, myCamera, "", fpCutsceneCustomIntro, FALSE, FALSE, FALSE, targetData.fCutsceneDoF)
//				CLEAR_AREA_OF_COPS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 35)
//				FADE_IN()
//				
//				IF curTarget = ASSASSIN_TARGET_YACHT
//					IF DOES_ENTITY_EXIST(args.myVehicle) AND IS_VEHICLE_DRIVEABLE(args.myVehicle)
//						SET_ENTITY_LOD_DIST(args.myVehicle, 500)
//					ENDIF
//				ENDIF
//				
//				IF CHECK_FOR_TARGET_DEATH(args, targetData)
//					IF curTarget = ASSASSIN_TARGET_BODYBUILDER
//						TASK_BYSTANDERS_TO_FLEE(TRUE)
//					ENDIF
//					curStage = MISSION_STATE_KILLCAM
//					PRINTLN("curStage = MISSION_STATE_KILLCAM")
//				ELSE
//					curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
//					PRINTLN("curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL")
//				ENDIF
//			ELSE
//				IF curTarget = ASSASSIN_TARGET_BODYBUILDER
//					IF NOT IS_PED_INJURED(args.myTarget)
//						UPDATE_BODYBUILDER_ANIMS(targetData, args)
//					ENDIF
//				ELIF curTarget = ASSASSIN_TARGET_YACHT
//					UPDATE_YACHT_PEDS(args)
//				ELIF curTarget = ASSASSIN_TARGET_BIKER
//					UPDATE_BIKER_IN_CUTSCENE(args)
////				ELIF curTarget = ASSASSIN_TARGET_WASHER
////					UPDATE_WASHER_ANIMS(args)
//				ENDIF
//			ENDIF
//		BREAK

		//waiting for player to kill the target
		CASE MISSION_STATE_WAITING_FOR_TARGET_KILL	
		
			// Special check to see if the player is close enough to attach the ropes to the window washer prop.
			ATTACH_ROPE_TO_PROPS(targetData, args)
		
			IF CHECK_FOR_TARGET_DEATH(assArgs, targetData)
			OR bRopeDetached
				IF bRopeDetached
				AND NOT IS_PED_INJURED(args.myTarget)
					//have window washer target scream as he falls
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					PLAY_PAIN(args.myTarget, AUD_DAMAGE_REASON_FALLING)
				ENDIF
				
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				
//				IF curTarget = ASSASSIN_TARGET_BODYBUILDER
//					TRIGGER_MUSIC_EVENT("ASS2_BB_OS")
//					PRINTLN("TRIGGERING MUSIC - ASS2_BB_OS")
//				ELIF curTarget = ASSASSIN_TARGET_YACHT
//					TRIGGER_MUSIC_EVENT("ASS2_YCT_OS")
//					PRINTLN("TRIGGERING MUSIC - ASS2_YCT_OS")
//				ELIF curTarget = ASSASSIN_TARGET_WASHER
//					TRIGGER_MUSIC_EVENT("ASS2_WW_OS")
//					PRINTLN("TRIGGERING MUSIC - ASS2_WW_OS")
//				ELIF curTarget = ASSASSIN_TARGET_BIKER
//					TRIGGER_MUSIC_EVENT("ASS2_BKR_OS")
//					PRINTLN("TRIGGERING MUSIC - ASS2_BKR_OS")
//				ENDIF
			
				IF NOT IS_TIMER_STARTED(tLeaveAreaTimer)
					START_TIMER_NOW(tLeaveAreaTimer)
					PRINTLN("STARTING TIMER - tLeaveAreaTimer")
				ELSE
					RESTART_TIMER_NOW(tLeaveAreaTimer)
					PRINTLN("RESTARTING TIMER - tLeaveAreaTimer")
				ENDIF
			
				IF curTarget = ASSASSIN_TARGET_BODYBUILDER
					TASK_BYSTANDERS_TO_FLEE(TRUE)
				ENDIF
				
				KILL_ANY_CONVERSATION()
				
				SETTIMERA(0)
				curStage = MISSION_STATE_KILLCAM
				PRINTLN("curStage = MISSION_STATE_KILLCAM")
			ELSE
				UPDATE_CURRENT_TARGET(targetData, assArgs)
				
//				HANDLE_RADIO_NEWS_EVENTS()
			ENDIF
		BREAK

		//play the kill cam cutscene
		CASE MISSION_STATE_KILLCAM
//			IF ( curTarget = ASSASSIN_TARGET_WASHER AND HAS_SLOW_KILLCAM_FINISHED(args.myTarget, TRUE, FALSE) )	//don't play slow motion camera
//			OR ( curTarget <> ASSASSIN_TARGET_WASHER AND HAS_SLOW_KILLCAM_FINISHED(args.myTarget) )
				STRING sKillLine
				
				IF curTarget = ASSASSIN_TARGET_BODYBUILDER
					sKillLine = "OJAS_BDCOM"
				ELIF curTarget = ASSASSIN_TARGET_YACHT
					sKillLine = "OJAS_YACOM"
				ELIF curTarget = ASSASSIN_TARGET_WASHER
					sKillLine = "OJAS_WWCOM"
				ELIF curTarget = ASSASSIN_TARGET_BIKER
					sKillLine = "OJAS_BICOM"
				ENDIF
				
				IF DOES_BLIP_EXIST(args.myTargetBlip)
					REMOVE_BLIP(args.myTargetBlip)
				ENDIF
				
				HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, args.assConv, "OJASAUD", sKillLine, CONV_PRIORITY_VERY_HIGH)
				
				IF bTargetKillLinePlayed
				OR TIMERA() > 1000
					
					IF NOT IS_PED_INJURED(args.myTarget)
					AND curTarget = ASSASSIN_TARGET_WASHER
						SET_ENTITY_HEALTH(args.myTarget, 0)		//to account for window washer possibly still being alive if the ropes were shot out and he is still falling
					ENDIF
					
					IF curTarget = ASSASSIN_TARGET_YACHT
						IF NOT IS_PED_INJURED(args.myOtherPed)	//make sure the girl on the yacht contiunes to cower if she isn't already dead
							OPEN_SEQUENCE_TASK(tempSeq)
								TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@multi@yachttarget@lapdance", "exit_quick_f")
								TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@multi@yachttarget@lapdance", "exit_cower_loop_f", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(tempSeq)
							TASK_PERFORM_SEQUENCE(args.myOtherPed, tempSeq)
							CLEAR_SEQUENCE_TASK(tempSeq)
						ENDIF
						
						IF DOES_ENTITY_EXIST(args.myVehicle)
							IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
								SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(args.myVehicle, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					//increment this
					iNumTargetsKilled ++
					
//					//reset variables to prepare for next target
//					INITIALIZE_NEXT_STAGE(targetData)
					
					//increment the players wanted level by 1 after each target is killed (starting after killing the 2nd target)
	//				IF iNumTargetsKilled > 1
	//					SET_PLAYER_WANTED_LEVEL_NO_DROP_NOW(iNumTargetsKilled - 1)
	//				ENDIF
					
					//create next target or advance to next stage if all have been eliminated
					IF iNumTargetsKilled >= NUM_ASSASSIN_TARGETS
					
						curStage = MISSION_STATE_SUCCESS
						PRINTLN("curStage = MISSION_STATE_SUCCESS")
					ELSE
						//flush ped asset requests to ready for next target
						RESET_TARGET_DATA(targetData, assArgs)
												
						//reset all variables to prepare for next target
						INITIALIZE_NEXT_STAGE(targetData)
						
						curStage = MISSION_STATE_INIT
						PRINTLN("INIT NEXT TARGET!")
					ENDIF
				ENDIF
//			ENDIF
		BREAK
		
		//pass the mission
		CASE MISSION_STATE_SUCCESS
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				IF HAS_ENDING_CELL_PHONE_CALL_COMPLETED(args)
					RETURN TRUE
				ENDIF
			ELSE
				IF NOT bLoseCopsPrinted
					PRINT_NOW("ASS_ML_COPS", DEFAULT_GOD_TEXT_TIME, 1)
					RESTART_TIMER_NOW(tPoliceScanner)					
					bLoseCopsPrinted = TRUE
				ELSE
					IF TIMER_DO_ONCE_WHEN_READY(tPoliceScanner, 2)
						PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_ASS_MULTI_01", 0.0)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC



PROC SKIP_CLEANUP_HELPER(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	IF bDoingPSkip			
				
		//rope stuff
	// DELETE_ROPE now doesn't crash even rope doesn't exists anymore
	// DELETE_ROPE will also delete all parent ropes, in this specific scenarion , rope A or B might have been broken
		ROPE_UNLOAD_TEXTURES()
		DELETE_ROPE(ropeWindowA)
		PRINTLN("DELETING ropeWindowA THROUGH P-SKIP")

		DELETE_ROPE(ropeWindowB)
		PRINTLN("DELETING ropeWindowB THROUGH P-SKIP")

		IF bAttachedRope
			bAttachedRope = FALSE
			PRINTLN("SETTING bAttachedRope = FALSE VIA P-SKIP")
		ENDIF

		REMOVE_ALL_ENTITIES_IN_ASS_STRUCT(args)
		
		iRopeStages = 0
		
//		IF DOES_CAM_EXIST(camKill)
//			DESTROY_CAM(camKill)
//		ENDIF
		
		RESET_TARGET_DATA(targetData, assArgs)

		INIT_MISSION_REQUESTS_AND_UI_LABELS(targetData, sLoadQueue)
		
		INITIALIZE_NEXT_STAGE(targetData)
		INIT_CURRENT_TARGET_DATA(targetData)
		
		WHILE NOT IS_CURRENT_TARGET_DATA_LOADED_NO_PEDS(targetData)
			WAIT(0)
		ENDWHILE
		
		bCheckpointDataSet = TRUE
		
//		//setup the scene
//		CREATE_ASSASSINATION_SCENE(targetData, assArgs)
//		CREATE_ADDITIONAL_NEEDS_FOR_CURRENT_TARGET(targetData, assArgs)
		
		curStage = MISSION_STATE_PRINTS
		PRINTLN("curStage = MISSION_STATE_PRINTS - DOING p-SKIP")
		
		bDoingPSkip = FALSE
		FADE_IN()
		
		IF iNumTargetsKilled < 4
			curStage = MISSION_STATE_PRINTS
		ELSE
			curStage = MISSION_STATE_SUCCESS
		ENDIF
	ENDIF
ENDPROC


PROC GO_TO_CHECKPOINT_1(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	PRINTLN("Trying to start from checkpoint 1")
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
//	GIVE_PLAYER_WEAPONS(WEAPONTYPE_HEAVYSNIPER, 10)
//	GIVE_PLAYER_WEAPONS(WEAPONTYPE_PISTOL, 25)
	
	//teleport player to checkpoint pos
	SET_ENTITY_COORDS(PLAYER_PED_ID(), vFirstCheckpointStartPos)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), fFirstCheckpointStartHeading)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		
	iNumTargetsKilled = 0
	
	SKIP_CLEANUP_HELPER(args, targetData)
ENDPROC

PROC GO_TO_CHECKPOINT_2(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	PRINTLN("Trying to start from checkpoint 2")
	VECTOR vCheckpointStartPos
	FLOAT fCheckpointStartHeading
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	vCheckpointStartPos = << -1205.3925, -1548.0220, 3.3229 >>
	fCheckpointStartHeading = 30.1673

//	GIVE_PLAYER_WEAPONS(WEAPONTYPE_HEAVYSNIPER, 10)
//	GIVE_PLAYER_WEAPONS(WEAPONTYPE_PISTOL, 25)

	//teleport player to checkpoint pos
	SET_ENTITY_COORDS(PLAYER_PED_ID(), vCheckpointStartPos)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), fCheckpointStartHeading)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	iNumTargetsKilled = 1
	
	SKIP_CLEANUP_HELPER(args, targetData)
ENDPROC

PROC GO_TO_CHECKPOINT_3(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	PRINTLN("Trying to start from checkpoint 3")
	VECTOR vCheckpointStartPos
	FLOAT fCheckpointStartHeading
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	vCheckpointStartPos = << -2178.8555, -410.6809, 12.1595 >>
	fCheckpointStartHeading = 13.1631
	
//	GIVE_PLAYER_WEAPONS(WEAPONTYPE_HEAVYSNIPER, 10)
//	GIVE_PLAYER_WEAPONS(WEAPONTYPE_PISTOL, 25)
	
	//teleport player to checkpoint pos
	SET_ENTITY_COORDS(PLAYER_PED_ID(), vCheckpointStartPos)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), fCheckpointStartHeading)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	iNumTargetsKilled = 2
	
	SKIP_CLEANUP_HELPER(args, targetData)
ENDPROC

PROC GO_TO_CHECKPOINT_4(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	PRINTLN("Trying to start from checkpoint 4")
	VECTOR vCheckpointStartPos
	FLOAT fCheckpointStartHeading
		
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())

	vCheckpointStartPos = << -702.8256, 224.2350, 79.1613 >>
	fCheckpointStartHeading = 291.4638
	
//	GIVE_PLAYER_WEAPONS(WEAPONTYPE_HEAVYSNIPER, 10)
//	GIVE_PLAYER_WEAPONS(WEAPONTYPE_PISTOL, 25)

	//teleport player to checkpoint pos
	SET_ENTITY_COORDS(PLAYER_PED_ID(), vCheckpointStartPos)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), fCheckpointStartHeading)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	iNumTargetsKilled = 3
	
	SKIP_CLEANUP_HELPER(args, targetData)
ENDPROC

PROC GO_TO_CHECKPOINT_5(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	PRINTLN("Trying to start from checkpoint 5")
	VECTOR vCheckpointStartPos
	FLOAT fCheckpointStartHeading
		
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	IF DOES_BLIP_EXIST(args.myTargetBlip)
		REMOVE_BLIP(args.myTargetBlip)
	ENDIF
	
	vCheckpointStartPos = <<-1986.3782, 531.8684, 108.2430>>
	fCheckpointStartHeading = 149.1485

	//teleport player to checkpoint pos
	SET_ENTITY_COORDS(PLAYER_PED_ID(), vCheckpointStartPos)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), fCheckpointStartHeading)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	iNumTargetsKilled = 4
	
	SKIP_CLEANUP_HELPER(args, targetData)
ENDPROC


PROC DO_FADE_OUT_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    args - 
///    targetData - 
///    targetsKilled - NEW number we want for iNumTargetsKilled
///    bIsDebugJump - 
PROC JUMP_TO_STAGE(ASS_ARGS& args, ASS_TARGET_DATA& targetData, INT targetsKilled, BOOL bIsDebugJump = FALSE)
	DO_FADE_OUT_WITH_WAIT()
	bDoingPSkip = TRUE
	//Update mission checkpoint in case they skipped the stages where it gets set.
	IF bIsDebugJump	
		IF targetsKilled = 0
			GO_TO_CHECKPOINT_1(args, targetData)
		ELIF targetsKilled = 1
			GO_TO_CHECKPOINT_2(args, targetData)
		ELIF targetsKilled = 2
			GO_TO_CHECKPOINT_3(args, targetData)
		ELIF targetsKilled = 3
			GO_TO_CHECKPOINT_4(args, targetData)
		ELIF targetsKilled = 4
			GO_TO_CHECKPOINT_5(args, targetData)
		ENDIF
	ENDIF
ENDPROC


// debug builds only for J-skipping
#IF IS_DEBUG_BUILD
	
	// debug skips
	PROC DO_DEBUG_SKIPS_MULTI(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
		
		IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iDebugJumpStage)
			bDoingPSkip = TRUE
			JUMP_TO_STAGE(args, targetData, iDebugJumpStage, TRUE)
        ENDIF

		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			
			IF curStage = MISSION_STATE_TRAVEL
				//if we're locating the target, go to the checkpoint for the previous target
				IF iNumTargetsKilled = 0
					PRINTLN("You haven't progressed far enough to use the P-SKIP feature.")
				ELSE
					JUMP_TO_STAGE(args, targetData, (iNumTargetsKilled - 1), TRUE)
				ENDIF
			ELIF curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
				//if we're trying to kill a target go to the checkpoint for current target
				JUMP_TO_STAGE(args, targetData, iNumTargetsKilled, TRUE)
			ELSE
				PRINTLN("Can't P-SKIP while in this state! You need to be in locate or kill state.")
			ENDIF
		ENDIF
	
		// mission pass S
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			curStage = MISSION_STATE_SUCCESS
		ENDIF
		
		// mission failed F
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			SET_MISSION_FAILED(assArgs, targetData, FAIL_TIME_EXPIRED)
		ENDIF
		
		//output debug cam coords relative to entity
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_N)
			OUTPUT_DEBUG_CAM_RELATIVE_TO_ENTITY(args.myVehicle)
		ENDIF
	
		// skip stage J
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
//			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
//			
//			IF curStage < MISSION_STATE_WAITING_FOR_TARGET_KILL
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), targetData.vDestPos)
//			ELSE
//				IF NOT IS_PED_INJURED(args.myTarget)
//					IF curTarget = ASSASSIN_TARGET_BODYBUILDER
//						TASK_BYSTANDERS_TO_FLEE(TRUE)
//					ENDIF
//					SET_ENTITY_HEALTH(args.myTarget, 0)
//					curStage = MISSION_STATE_KILLCAM
//				ELSE
//					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
//				ENDIF
//			ENDIF
			
			bDoingPSkip = TRUE
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			
			IF iNumTargetsKilled <= 3
				iNumTargetsKilled ++
				JUMP_TO_STAGE(args, targetData, iNumTargetsKilled, TRUE)
			ELSE
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)

				REMOVE_BLIP(args.myTargetBlip)
				
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				curStage = MISSION_STATE_SUCCESS
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

//PROC CHECK_FOR_ACTION_MODE()
//	IF NOT bSetActionMode
//		IF curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL 
//			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
//					PRINTLN("MULTI: SETTING ACTION MODE ON THE PLAYER TO TRUE")
//					bSetActionMode = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ELSE
//		IF curStage <> MISSION_STATE_WAITING_FOR_TARGET_KILL 
//			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
//					PRINTLN("MULTI: SETTING ACTION MODE ON THE PLAYER TO FALSE")
//					bSetActionMode = FALSE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC




//main script
SCRIPT(ASS_TARGET_DATA targetData)
	DEBUG_MESSAGE("Assassination Multi Start")

	SET_MISSION_FLAG(TRUE)
	
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		CLEAR_HD_AREA()
		FLUSH_TEXT_MESSAGE_FEED_ENTRIES()
		DEBUG_MESSAGE("FORCE CLEANUP HAS OCCURRED!!!")
		g_savedGlobals.sAssassinData.fMultiMissionTime = GET_PLAYERS_MULTI_MISSION_TIME()
		PRINTLN("g_savedGlobals.sAssassinData.fMultiMissionTime = ", g_savedGlobals.sAssassinData.fMultiMissionTime)
		SET_BITMASK_AS_ENUM(g_savedGlobals.sAssassinData.iGenericData, ACD_FAILED)
		Mission_Flow_Mission_Force_Cleanup()
		
		//restore phone stuff
		IF DOES_ENTITY_EXIST(oPayPhone)
			SET_ENTITY_VISIBLE(oPayPhone, TRUE)
			SET_ENTITY_COLLISION(oPayPhone, TRUE)
			SET_ENTITY_CAN_BE_DAMAGED(oPayPhone, TRUE)
		ENDIF
		
		MISSION_CLEANUP(sLoadQueue)
	ENDIF
	
	IF Is_Replay_In_Progress()
		IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
			bReplayVehicleAvailable = TRUE
		ENDIF
		bReplaying = TRUE
	ELSE
		bReplaying = FALSE
	ENDIF
	
	//disable this since we don't want a target's intro cutscene to be skipped just because the player approached the scene wanted
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
	PRINTLN("SETTING BIT TO NOT CHECK WANTED LEVEL")
	
	//don't allow the player to hail taxis while the mission is active
	DISABLE_TAXI_HAILING(TRUE)
	
	#IF IS_DEBUG_BUILD
//		AddWidgets(myDebugData)
		SETUP_DEBUG()
	#ENDIF
	
	// Fix Bug # 712684
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	ENDIF
	
	sbi01 = ADD_SCENARIO_BLOCKING_AREA((vFlexPos - <<2,2,2>>), (vFlexPos + <<2,2,2>>))
	
	ASSASSINATION_ClearUnneededGenericData()
	
	iCurAssassinRank = ENUM_TO_INT(ASSASSINATION_Multi) //g_savedGlobals.sAssassinData.iCurrentAssassinRank
	missionData = ASSASSINATION_GetMissionData(iCurAssassinRank)	
	PRINTLN("Current rank is... ", iCurAssassinRank)
	
	curStage = MISSION_BRIEF_INIT
	PRINTLN("STARTING MISSION ON STAGE - MISSION_BRIEF_INIT")
	
	
	//HANDLE STREAMING AROUND REPLAY START POSITION
	IF IS_REPLAY_IN_PROGRESS()
		
		FLUSH_TEXT_MESSAGE_FEED_ENTRIES()
		
		bHaveRetryOnce = TRUE
		PRINTLN("MULTI: SETTING -  bHaveRetryOnce = TRUE")
	
		iStageToUse = Get_Replay_Mid_Mission_Stage()
		PRINTLN("START - REPLAY IS IN PROGRESS, USING STAGE = ", iStageToUse)
		
		IF g_bShitskipAccepted
			bPlayerHasShitSkipped = TRUE
			iStageToUse ++		//increment this by one since we shit skipped
		ENDIF
		
		IF iStageToUse = 0
			vReplayStartPos = vFirstCheckpointStartPos
			fReplayStartHead = fFirstCheckpointStartHeading
					
			vVehReplayStartPos = <<-711.8262, -920.7407, 18.0144>>
			fVehReplayStartHead = 0.0954
			
			START_REPLAY_SETUP(vFirstCheckpointStartPos, fFirstCheckpointStartHeading)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 1
			vReplayStartPos = << -1205.3925, -1548.0220, 3.3229 >>
			fReplayStartHead = 30.1673
			
			vVehReplayStartPos = << -1192.2059, -1532.9329, 3.4095 >>
			fVehReplayStartHead = 304.3640

			START_REPLAY_SETUP(vReplayStartPos, fReplayStartHead)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 2
			vReplayStartPos = << -2178.8555, -410.6809, 12.1595 >>
			fReplayStartHead = 13.1631

			vVehReplayStartPos = << -2175.7822, -400.1200, 12.1925 >>
			fVehReplayStartHead = 347.9469
			
			START_REPLAY_SETUP(vReplayStartPos, fReplayStartHead)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 3
			vReplayStartPos = << -702.8256, 224.2350, 79.1613 >>
			fReplayStartHead = 291.4638
			
			vVehReplayStartPos = << -700.4276, 229.6938, 79.2745 >>
			fVehReplayStartHead = 296.3371
					
			START_REPLAY_SETUP(vReplayStartPos, fReplayStartHead)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 4
			vReplayStartPos = <<-1986.3782, 531.8684, 108.2430>>
			fReplayStartHead = 149.1485
			
			vVehReplayStartPos = <<-1984.4498, 529.9053, 107.9867>>
			fVehReplayStartHead = 163.9831
					
			START_REPLAY_SETUP(vReplayStartPos, fReplayStartHead)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ENDIF
	ENDIF
	
	
	//main loop
	WHILE TRUE
		WAIT(0)
		
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ASS2")
		
//		DRAW_DEBUG_SPHERE(<< -717.83478, -932.17358, 18.01735 >>, 26.0)
				
		UPDATE_PRINT_DELAY(printTimer, 14, TRUE)
		
		IF UPDATE_FAIL_CONDITIONS(targetData)
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				
				IF IS_TIMER_STARTED(tMissionTime)
					g_savedGlobals.sAssassinData.fMultiMissionTime = GET_PLAYERS_MULTI_MISSION_TIME()
					PRINTLN("CLEANUP: tMissionTime = ", g_savedGlobals.sAssassinData.fMultiMissionTime)
				ELSE
					PRINTLN("CLEANUP: tMissionTime WAS NOT STARTED!!!")
				ENDIF
				
				REMOVE_SCENARIO_BLOCKING_AREA(sbi01)
				
				FLUSH_TEXT_MESSAGE_FEED_ENTRIES() 
				
				MISSION_FAILED(sLoadQueue)
			ENDIF
		ELIF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF UPDATE_ASSASSINATION_MULTI(targetData, assArgs)
			
				g_savedGlobals.sAssassinData.fMultiMissionTime = GET_PLAYERS_MULTI_MISSION_TIME()
				PRINTLN("GLOBAL MULTI MISSION TIME = ", g_savedGlobals.sAssassinData.fMultiMissionTime)
				
				fTimeTaken = g_savedGlobals.sAssassinData.fMultiMissionTime / 1000
				PRINTLN("fTimeTaken = ", fTimeTaken)
				
				//manually override the mission time to fit this mission so that it displays properly on the end of the results screen
				RESTART_TIMER_AT(tMissionTime, fTimeTaken)
				PAUSE_TIMER(tMissionTime)
				PRINTLN("tMissionTime =  ", GET_TIMER_IN_SECONDS(tMissionTime))
			
				IF NOT bPlayerHasShitSkipped
					CHECK_FOR_TIME_BONUS()
				ENDIF
				
				FLUSH_TEXT_MESSAGE_FEED_ENTRIES()
				
				MISSION_PASSED(missionData, cucEndScreenCore, sLoadQueue)
			ELSE
				//to fix bug 1052071 - make sure that the player's last vehicle doesnt get cleaned up (3/4/13 - MB)
				SAVE_LAST_PLAYER_VEHICLE()	
				
				PLAY_FRANKLIN_LINE_REACTING_TO_POLICE_IF_WANTED()
			ENDIF
			
			//handle the mission timer stuff
			FAIL_MISSION_IF_TIMER_EXPIRES(targetData, assArgs)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			DO_DEBUG_SKIPS_MULTI(assArgs, targetData)
//			IF DOES_ENTITY_EXIST(assArgs.myProp[0])	//assArgs.myProp[myDebugData.iDebugFloat]
//				SET_ENTITY_COORDS(assArgs.myProp[0], << myDebugData.vDebugVector.X, myDebugData.vDebugVector.Y, myDebugData.vDebugVector.Z >>)
//				SET_ENTITY_HEADING(assArgs.myProp[0], myDebugData.fRotateZ)
//			ENDIF

//			IF myDebugData.bTurnOnDebugPos 
//				IF DOES_ENTITY_EXIST(assArgs.myOtherPed) //the ped you are attaching 
//				AND DOES_ENTITY_EXIST(assArgs.myVehicle) 
//					ATTACH_ENTITY_TO_ENTITY(assArgs.myOtherPed, assArgs.myVehicle, 0, << myDebugData.fOffsetX, myDebugData.fOffsetY, myDebugData.fOffsetZ >>, << myDebugData.fRotateX, myDebugData.fRotateY, myDebugData.fRotateZ >>) 
//					SET_ENTITY_ROTATION(assArgs.myOtherPed, << myDebugData.fRotateX, myDebugData.fRotateY, myDebugData.fRotateZ >>)
//				ENDIF 
//			ENDIF

//			IF myDebugData.bTurnOnDebugPos 
//				IF DOES_ENTITY_EXIST(assArgs.myTarget)
//				AND DOES_ENTITY_EXIST(assArgs.myProp[0]) 
//					ATTACH_ENTITY_TO_ENTITY(assArgs.myTarget, assArgs.myProp[0], 0, << myDebugData.fOffsetX, myDebugData.fOffsetY, myDebugData.fOffsetZ >>, << myDebugData.fRotateX, myDebugData.fRotateY, myDebugData.fRotateZ >>, TRUE, TRUE)
//				ENDIF 
//			ENDIF
			
//			SHOW_BYSTANDER_POSITIONS()
		#ENDIF
		
	ENDWHILE
ENDSCRIPT
