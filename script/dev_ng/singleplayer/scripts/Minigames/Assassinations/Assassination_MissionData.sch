// Assassination_MissionData.sch

CONST_INT 	NUM_ASSASSINATION_MISSIONS		5

/// PURPOSE: This enum is important because you can change the 
/// ordering of this to change the order in which missions get played.
ENUM ASSASSINATION_MISSIONS
	ASSASSINATION_Hotel = 0,
	ASSASSINATION_Multi,
	ASSASSINATION_Vice,
	ASSASSINATION_Bus,
	ASSASSINATION_Construction
ENDENUM

ENUM ASS_SCENE_TYPE
	ASS_SCENE_TYPE_PAY_PHONE,
	ASS_SCENE_TYPE_FREE_COORD
ENDENUM

ENUM eAssassinPackage_WeaponOutfit
	eASM_WeaponOutfit_INVALID = -1,
	
	eASM_BulletProofVest = BIT0
ENDENUM

ENUM eAssassinMissionFlags
	ASM_MOVING_MISSION	= BIT0			// Setting this flag makes the launcher skip distance checks for terminating the mission.
ENDENUM

STRUCT sAssassinMissionData
	ASS_SCENE_TYPE					eSceneType			// Are we doing a pay phone cut-scene or a free-roam cut-scene?
	VECTOR							vPackageLoc			// The location of the package, where player gets guns/equipment
	VECTOR							vPackageRotation		// Rotation value for package
	VECTOR							vCutsceneCoord		// The location of the payphone, where the player gets all his data and launches the mission.
	VECTOR							vCamCoord1			// The location of the first camera cut
	VECTOR							vCamRot1			// The rotation of the first camera cut
	VECTOR							vCamCoord2			// The location of the second camera cut
	VECTOR							vCamRot2			// The rotation of the second camera cut
	STRING							missionFilename		// The script to launch to start the mission
	FLOAT							fBaseReward			// The minimum reward the player can get for completing the mission without doing any of the special criteria.
	FLOAT							fBonusReward		// The money to be awarded if the player gets the bonus.
	VECTOR							vCutscenePlayer		// The position where the player will be during the package cutscene shot
	VECTOR 							vSafeCarPosition	// The position to reposition the player's vehicle when starting mission.
	FLOAT 							fSafeCarHeading		// The heading used in repositioning the car
	eAssassinPackage_WeaponOutfit	eGiveEquipment		// If this is set, the player gets this weapon and/or outfit when he picks up the package.
	VECTOR 							vPlayerPosition
	FLOAT 							fPlayerHeading
	BOOL							bCreatePed
	// Internal data
	BLIP_INDEX						genericBlip	
ENDSTRUCT

FUNC VECTOR ASSASSINATION_GetMissionLocation(ASSASSINATION_MISSIONS mission)
	SWITCH mission
		CASE ASSASSINATION_Hotel
			RETURN << -1704.4268, -1077.3157, 12.1111>>
		CASE ASSASSINATION_Multi
			RETURN << -700.4290, -916.7467, 18.2143 >>
		CASE ASSASSINATION_Vice
			RETURN << 214.1641, -852.8006, 29.3929 >>
		CASE ASSASSINATION_Bus
			RETURN << -21.9871, -107.4823, 55.9970 >>
		CASE ASSASSINATION_Construction
			RETURN << 806.1469, -1070.2103, 27.3361 >>
	ENDSWITCH
	RETURN <<0, 0, 0>>
ENDFUNC


// This function is invoked by the controller thread to get the mission we're going to launch next
FUNC sAssassinMissionData ASSASSINATION_GetMissionData(INT iRankAttempting)
	sAssassinMissionData dataTemp

			
	IF (iRankAttempting = ENUM_TO_INT(ASSASSINATION_Hotel))
		dataTemp.eSceneType = ASS_SCENE_TYPE_FREE_COORD
		dataTemp.vPackageLoc = <<0, 0, 0>>
		dataTemp.vPackageRotation = <<0, 0, 0>>
		dataTemp.vCutsceneCoord = ASSASSINATION_GetMissionLocation(ASSASSINATION_Hotel)
		dataTemp.missionFilename = "Assassin_Valet"
		dataTemp.fBaseReward = 7000.0
		dataTemp.fBonusReward = 2000.0
		dataTemp.eGiveEquipment = eASM_WeaponOutfit_INVALID
		dataTemp.vCutscenePlayer = << -1700.0151, -1066.3350, 12.1440 >>
		dataTemp.vSafeCarPosition = << -1691.5642, -1066.5138, 12.0760 >>
		dataTemp.fSafeCarHeading = 35.4714
		dataTemp.vCamCoord1 = <<-1700.4670, -1066.6720, 13.8795>>
		dataTemp.vCamRot1 = <<-4.8332, -0.0000, -177.1283>>
		dataTemp.vCamCoord2 = <<-1700.4670, -1066.6720, 13.8795>>
		dataTemp.vCamRot2 = <<-4.8332, -0.0000, -177.1283>>
		dataTemp.vPlayerPosition = <<-1700.0983, -1067.9395, 12.1547>> 
		dataTemp.fPlayerHeading = 162.4559
		dataTemp.bCreatePed = TRUE
		
	ELIF (iRankAttempting = ENUM_TO_INT(ASSASSINATION_Multi))
		dataTemp.eSceneType = ASS_SCENE_TYPE_PAY_PHONE
		dataTemp.vPackageLoc = << -699.3992, -917.5043, 18.2143 >>
		dataTemp.vPackageRotation = <<0, 0, 0>>
		dataTemp.vCutsceneCoord = ASSASSINATION_GetMissionLocation(ASSASSINATION_Multi)
		dataTemp.missionFilename = "Assassin_Multi"
		dataTemp.fBaseReward = 5000.0
		dataTemp.fBonusReward = 2000.0
		dataTemp.eGiveEquipment = eASM_WeaponOutfit_INVALID
		dataTemp.vCutscenePlayer = << -700.1855, -917.9558, 18.2143 >>
		dataTemp.vSafeCarPosition = << -699.9455, -921.7786, 18.0144 >>
		dataTemp.fSafeCarHeading = 78.0874
		dataTemp.vCamCoord1 = << -697.8064, -921.4629, 20.5104 >>
		dataTemp.vCamRot1 = << -13.5249, -0.0000, 30.6033 >>
		dataTemp.vCamCoord2 = << -702.4851, -921.2747, 21.1235 >>
		dataTemp.vCamRot2 = << -22.5196, 0.0000, -43.0435 >>
		dataTemp.vPlayerPosition = <<0, 0, 0>> 
		dataTemp.fPlayerHeading = 0
		dataTemp.bCreatePed = FALSE
	ELIF (iRankAttempting = ENUM_TO_INT(ASSASSINATION_Vice))
		dataTemp.eSceneType = ASS_SCENE_TYPE_PAY_PHONE
		dataTemp.vPackageLoc = << 215.1206, -853.3143, 29.3684 >>
		dataTemp.vPackageRotation = <<0, 0, 87.1787>>
		dataTemp.vCutsceneCoord = ASSASSINATION_GetMissionLocation(ASSASSINATION_Vice)
		dataTemp.missionFilename = "Assassin_Hooker"
		dataTemp.fBaseReward = 3000.0
		dataTemp.fBonusReward = 2000.0
		dataTemp.eGiveEquipment = eASM_WeaponOutfit_INVALID
		dataTemp.vCutscenePlayer = << 213.7994, -853.9389, 29.3929 >>
		dataTemp.vSafeCarPosition = << 205.2641, -847.2667, 29.4903 >>
		dataTemp.fSafeCarHeading = 140.1039
		dataTemp.vCamCoord1 = << 216.7391, -856.0031, 32.7127 >>
		dataTemp.vCamRot1 = << -25.1365, 0.0000, 41.3912 >>
		dataTemp.vCamCoord2 = << 210.4668, -851.3092, 32.1099 >>
		dataTemp.vCamRot2 = << -16.3326, 0.0000, -127.0114 >>
		dataTemp.vPlayerPosition = <<213.8733, -853.8161, 29.3922>>  
		dataTemp.fPlayerHeading = 344.0112
		dataTemp.bCreatePed = FALSE
	ELIF (iRankAttempting = ENUM_TO_INT(ASSASSINATION_Bus))
		dataTemp.eSceneType = ASS_SCENE_TYPE_PAY_PHONE
		dataTemp.vPackageLoc = << -22.5499, -107.3546, 56.0161 >>
		dataTemp.vPackageRotation = <<0, 0, 269.7924>>
		dataTemp.vCutsceneCoord = ASSASSINATION_GetMissionLocation(ASSASSINATION_Bus)
		dataTemp.missionFilename = "Assassin_Bus"
		dataTemp.fBaseReward = 5000.0
		dataTemp.fBonusReward = 2000.0
		dataTemp.eGiveEquipment = eASM_WeaponOutfit_INVALID
		dataTemp.vCutscenePlayer = << -22.3125, -108.9183, 56.0068 >>
		dataTemp.vSafeCarPosition = << -17.2677, -118.5915, 55.8734 >>
		dataTemp.fSafeCarHeading = 1.4374
		dataTemp.vCamCoord1 = << -26.1094, -108.0298, 59.0520 >>
		dataTemp.vCamRot1 = << -21.2059, -0.0000, -109.0176 >>
		dataTemp.vCamCoord2 = << -20.1189, -111.9639, 59.4377 >>
		dataTemp.vCamRot2 = << -27.0037, 0.0000, 29.4640 >>
		dataTemp.vPlayerPosition = <<0, 0, 0>> 
		dataTemp.fPlayerHeading = 0
		dataTemp.bCreatePed = FALSE
	ELIF (iRankAttempting = ENUM_TO_INT(ASSASSINATION_Construction))
		dataTemp.eSceneType = ASS_SCENE_TYPE_PAY_PHONE
		dataTemp.vPackageLoc = << 806.1469, -1070.2103, 27.3361 >>
		dataTemp.vPackageRotation = <<0, 0, 90>>
		dataTemp.vCutsceneCoord = ASSASSINATION_GetMissionLocation(ASSASSINATION_Construction)
		dataTemp.missionFilename = "Assassin_Construction"
		dataTemp.fBaseReward = 8000.0
		dataTemp.fBonusReward = 2000.0
		dataTemp.eGiveEquipment = eASM_BulletProofVest
		dataTemp.vCutscenePlayer = << 804.9559, -1070.4604, 27.3361 >>
		dataTemp.vSafeCarPosition = << 799.8408, -1079.1416, 27.3210 >>
		dataTemp.fSafeCarHeading = 69.6524
		dataTemp.vCamCoord1 = << 801.8048, -1068.0675, 30.3496 >>
		dataTemp.vCamRot1 = << -20.8953, -0.0000, -132.9451 >>
		dataTemp.vCamCoord2 = << 805.8168, -1074.4961, 28.9803 >>
		dataTemp.vCamRot2 = << -1.5585, -0.0000, 6.9143 >>
		dataTemp.vPlayerPosition = <<804.8776, -1070.5231, 27.3416>>  
		dataTemp.fPlayerHeading = 287.8741
		dataTemp.bCreatePed = FALSE
	ENDIF
	
	RETURN dataTemp
ENDFUNC

// This function is invoked by the controller thread to get the mission we're going to launch next
PROC ASSASSINATION_CleanMissionData(sAssassinMissionData & dataStruct)
	REMOVE_BLIP(dataStruct.genericBlip)
	
	dataStruct.vCutsceneCoord = <<0,0,0>>
	dataStruct.missionFilename = ""
	
	dataStruct.eGiveEquipment = eASM_WeaponOutfit_INVALID
ENDPROC
