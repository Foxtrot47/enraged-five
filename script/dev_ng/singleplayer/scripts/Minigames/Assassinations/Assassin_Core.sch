//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     : Assassin_Core.sch                                		        //
//      AUTHOR          : Michael Bagley	                                            //
//      DESCRIPTION     : Header for assassinations					 					//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "minigame_uiinputs.sch"
USING "script_Oddjob_funcs.sch"
USING "assassination_support_lib.sch"
USING "Assassin_shared.sch"
USING "taxi_functions.sch"
USING "completionpercentage_public.sch"
USING "shared_hud_displays.sch"
USING "script_blips.sch"
USING "script_oddjob_queues.sch"
USING "script_usecontext.sch"
USING "commands_recording.sch"
USING "load_queue_public.sch"

// ********************************** Script Globals *************************************
CONST_INT maxPrints 2
CONST_INT maxModels 11
CONST_INT maxAnims 10
CONST_INT maxProps 4
PASS_REASON_ENUM passReasonCore

//VECTOR vBusSafeWarpPosition
//VECTOR vHookerSafeWarpPosition
//VECTOR vMultiSafeWarpPosition
//
//FLOAT fBusSafeWarpHeading
//FLOAT fHookerSafeWarpHeading
//FLOAT fMultiSafeWarpHeading

INT iSpeedNode 

PED_INDEX piHookerCutscenePed

BOOL bLoseCopsPrinted
BOOL bCreateRelationshipGroup = FALSE
BOOL bSkippedEndingScreen = FALSE
BOOL bPlayMusic = FALSE

//CustomCutsceneCaller fpCutsceneCustomIntro
CAMERA_INDEX myCamera
structTimer tBonusTimer
structTimer tMissionTime

REL_GROUP_HASH relTemp

ENUM MISSION_STATE
	MISSION_STATE_TIME_LAPSE = 0,
	MISSION_BRIEF_INIT,
	MISSION_BRIEF_UPDATE,
	MISSION_BRIEF_EXIT,
	MISSION_STATE_INIT,
	MISSION_STATE_STREAMING,
	MISSION_STATE_PRINTS,
	MISSION_STATE_TRAVEL,
	MISSION_STATE_CUTSCENE_SETUP,
	MISSION_STATE_CUTSCENE,
	MISSION_STATE_WAITING_FOR_TARGET_KILL,
	MISSION_STATE_KILLCAM,
	MISSION_STATE_SUCCESS,
	MISSION_STATE_FAILED
ENDENUM
MISSION_STATE curStage = MISSION_BRIEF_INIT

ENUM INTRO_CUTSCENE
	INTRO_CUTSCENE_CREATE = 0,
	INTRO_CUTSCENE_TRANSITIONA,
	INTRO_CUTSCENE_TRANSITIONB,
	INTRO_CUTSCENE_TRANSITIONC,
	INTRO_CUTSCENE_TRANSITIOND,
	INTRO_CUTSCENE_TRANSITIONE,
	INTRO_CUTSCENE_END
ENDENUM
INTRO_CUTSCENE introCutsceneState = INTRO_CUTSCENE_CREATE

ENUM FAIL_REASON_ENUM
	FAIL_PED_ESCAPED = 0,
	FAIL_TIME_EXPIRED,
	FAIL_BLOWN_COVER,
	FAIL_VEHICLE_DESTROYED,
	FAIL_ABANDONED_MISSION
ENDENUM

ENUM ASS_MISSION_NAME
	MISSION_NAME_BUS,
	MISSION_NAME_HOOKER,
	MISSION_NAME_MULTI
ENDENUM

//All of the data we need to populate to set up a simple unique assaination mission
STRUCT ASS_TARGET_DATA
	//Target variables
		MODEL_NAMES targetEnum
		VECTOR vTargetPos
		FLOAT fTargetHead
		MODEL_NAMES vehEnum
		VECTOR vDestPos
		VECTOR vVehPos
		FLOAT fVehHead
		WEAPON_TYPE weapTarget
//		INT iHealth
	
	//secondary ped variables
		MODEL_NAMES otherPedEnum
		MODEL_NAMES otherVehEnum
		MODEL_NAMES auxPedEnum
		MODEL_NAMES auxVehEnum
		VECTOR vOtherPedPos
		FLOAT fOtherPedHead
		VECTOR vAuxPedPos
		FLOAT fAuxPedHead
		VECTOR vOtherVehPos
		FLOAT fOtherVehHead
		VECTOR vAuxVehPos
		FLOAT fAuxVehHead
	
	//object data
		STREAMED_MODEL streamedModels[maxModels]
		STREAMED_ANIM streamedAnims[maxAnims]
	
	//stringtables
		TEXT_LABEL_23 sStringTable
		TEXT_LABEL_23 OBJECTIVES[maxPrints]
	
	//camera data
		VECTOR vCutscenePos
		VECTOR vCutsceneHead
		FLOAT fCutsceneDoF
	
	//Animations
		TEXT_LABEL_63 sAnimDict[maxAnims]
		
	//Props
		MODEL_NAMES propEnum[maxProps]
		VECTOR vPropPos[maxProps]
		FLOAT fPropHead[maxProps]

	//Some custom bools to tweak
		BOOL bDriving
		BOOL bNonTemp
		BOOL bLeaveVehicles
		BOOL bFailed
		
	//J-skip warp position
		VECTOR vWarpPos
		
	ASS_MISSION_NAME myType
	
ENDSTRUCT

INT iEndingScreenStages

//peds, objects, vehicle variables
STRUCT ASS_ARGS
	//target variables
	PED_INDEX myTarget
	VEHICLE_INDEX myVehicle
	BLIP_INDEX myTargetBlip
	VECTOR vTargetBlipTaxiPos
	FLOAT fTargetBlipTaxiHead
	
	//secondary ped variables
	PED_INDEX myOtherPed
	VEHICLE_INDEX myOtherVehicle
	
	//additional ped/vehicle indices
	PED_INDEX myAuxPed
	VEHICLE_INDEX myAuxVehicle
	
	//props
	OBJECT_INDEX myProp[maxProps]
	
	//conversation data
	structPedsForConversation assConv
	
	//Fail Enum
	FAIL_REASON_ENUM failReason
	
	//agro check Bool
	BOOL bDoAssAggroChecks
ENDSTRUCT
ASS_ARGS assArgs

// ********************************** END SCRIPT GLOBALS*************************************

//PROC INIT_SAFE_WARP_POSITIONS(ASS_TARGET_DATA& targetData)
//	IF targetData.myType = MISSION_NAME_BUS
//		vBusSafeWarpPosition = <<-23.0183, -109.4773, 56.0190>> 
//		fBusSafeWarpHeading = 162.2133
//		PRINTLN("INIT_SAFE_WARP_POSITIONS - BUS")
//	ELIF targetData.myType = MISSION_NAME_HOOKER
//		vHookerSafeWarpPosition = <<213.8733, -853.8161, 29.3922>> 
//		fHookerSafeWarpHeading = 344.0112
//		PRINTLN("INIT_SAFE_WARP_POSITIONS - HOOKER")
//	ELIF targetData.myType = MISSION_NAME_MULTI
//		vMultiSafeWarpPosition = <<-701.1385, -920.1099, 18.0144>> 
//		fMultiSafeWarpHeading = 102.6182
//		PRINTLN("INIT_SAFE_WARP_POSITIONS - MULTI")
//	ELSE
//		PRINTLN("INIT_SAFE_WARP_POSITIONS - VECTOR NOT DEFINED!!!")
//	ENDIF
//ENDPROC

PROC REMOVE_ALL_ENTITIES_IN_ASS_STRUCT(ASS_ARGS &args)
	IF NOT IS_PED_INJURED(args.myTarget)
		DELETE_PED(args.myTarget)
		PRINTLN("KILLING args.myTarget")
	ENDIF
	
	IF DOES_BLIP_EXIST(args.myTargetBlip)
		REMOVE_BLIP(args.myTargetBlip)
		PRINTLN("DELETING args.myTargetBlip")
	ENDIF

	IF DOES_ENTITY_EXIST(args.myOtherPed)
		DELETE_PED(args.myOtherPed)
		PRINTLN("DELETING args.myOtherPed")
	ENDIF
	
	IF DOES_ENTITY_EXIST(args.myOtherVehicle)
		DELETE_VEHICLE(args.myOtherVehicle)
		PRINTLN("DELETING args.myOtherVehicle")
	ENDIF
	
	IF DOES_ENTITY_EXIST(args.myAuxPed)
		DELETE_PED(args.myAuxPed)
		PRINTLN("DELETING args.myAuxPed")
	ENDIF
	
	IF DOES_ENTITY_EXIST(args.myAuxVehicle)
		DELETE_VEHICLE(args.myAuxVehicle)
		PRINTLN("DELETING args.myAuxVehicle")
	ENDIF
	
	INT propCnt = 0
	REPEAT maxProps propCnt
		IF DOES_ENTITY_EXIST(args.myProp[propCnt])
			DELETE_OBJECT(args.myProp[propCnt])
			PRINTLN("DELETING args.myProp[", propCnt, "]")
		ENDIF
	ENDREPEAT
ENDPROC
 
// initialize mision
PROC INIT_MISSION_REQUESTS_AND_UI_LABELS(ASS_TARGET_DATA& targetData, LoadQueueLarge &sLoadQueue, BOOL bRequestCheckpointVehicle = FALSE)
	//lower wanted player multiplier
	SET_WANTED_LEVEL_MULTIPLIER(0.2)
	
	///requests
	IF targetData.myType = MISSION_NAME_HOOKER
		REQUEST_ADDITIONAL_TEXT("ASS_hk", MINIGAME_TEXT_SLOT) // load text for the mission
		REQUEST_WAYPOINT_RECORDING("OJAShk_101")
		REQUEST_WAYPOINT_RECORDING("OJAShk_102")
		REQUEST_WAYPOINT_RECORDING("OJAShk_103")
		REQUEST_WAYPOINT_RECORDING("OJAShk_104")
		REQUEST_ANIM_DICT("ODDJOBS@assassinate@vice@incar")
		REQUEST_ANIM_DICT("mini@hookers_spvanilla")
	ELIF targetData.myType = MISSION_NAME_BUS
		REQUEST_ADDITIONAL_TEXT("ASS_bs", MINIGAME_TEXT_SLOT) // load text for the mission
		REQUEST_WAYPOINT_RECORDING("OJASbs_102")
//		REQUEST_WAYPOINT_RECORDING("OJASbs_104")
		REQUEST_WAYPOINT_RECORDING("OJASbs01")
		REQUEST_WAYPOINT_RECORDING("OJASbs02")
		REQUEST_WAYPOINT_RECORDING("OJASbs03")
		REQUEST_WAYPOINT_RECORDING("OJASbs04")
		REQUEST_ANIM_DICT("ODDJOBS@assassinate@old_lady")
		REQUEST_CLIP_SET("move_m@casual@d")
		REQUEST_VEHICLE_ASSET(targetData.vehEnum)
		REQUEST_VEHICLE_ASSET(targetData.otherVehEnum)
		REQUEST_PTFX_ASSET()
	ELIF targetData.myType = MISSION_NAME_MULTI
		REQUEST_ADDITIONAL_TEXT("ASS_ml", MINIGAME_TEXT_SLOT) // load text for the mission
		REQUEST_PTFX_ASSET()
	ENDIF
	
	REQUEST_ALL_MODELS(targetData.streamedModels)
	REQUEST_ALL_ANIMS(targetData.streamedAnims, sLoadQueue)
	
	IF bRequestCheckpointVehicle
		REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
		WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
			PRINTLN("Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED")
			UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
			WAIT(0)
		ENDWHILE
	ENDIF
	//wait for requests to load
	IF targetData.myType = MISSION_NAME_BUS
		WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs_102")
//		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs_104")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs01")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs02")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs03")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs04")
		OR NOT HAS_ANIM_DICT_LOADED("ODDJOBS@assassinate@old_lady")
		OR NOT HAS_VEHICLE_ASSET_LOADED(targetData.vehEnum)
		OR NOT HAS_VEHICLE_ASSET_LOADED(targetData.otherVehEnum)
		OR NOT HAS_PTFX_ASSET_LOADED()
		OR NOT HAS_CLIP_SET_LOADED("move_m@casual@d")
			PRINTLN("Waiting for MISSION_NAME_BUS streaming requests")
			UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
			WAIT(0)
		ENDWHILE
	ELIF targetData.myType = MISSION_NAME_HOOKER
		WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJAShk_101")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJAShk_102")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJAShk_103")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJAShk_104")
		OR NOT HAS_ANIM_DICT_LOADED("ODDJOBS@assassinate@vice@incar")
		OR NOT HAS_ANIM_DICT_LOADED("mini@hookers_spvanilla")
			PRINTLN("Waiting for MISSION_NAME_HOOKER streaming requests")
			UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
			WAIT(0)
		ENDWHILE
	ELIF targetData.myType = MISSION_NAME_MULTI
		WHILE NOT HAS_PTFX_ASSET_LOADED()
			PRINTLN("Waiting for MISSION_NAME_MULTI streaming requests")
			UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
			WAIT(0)
		ENDWHILE
	ENDIF
	
	//make sure all models and anims in targetData.streamedModels and targetData.streamedAnims are streamed in
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
	OR NOT ARE_MODELS_STREAMED(targetData.streamedModels)
	OR NOT ARE_ANIMS_STREAMED(sLoadQueue)
		PRINTLN("Waiting for streaming")
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		DEBUG_MESSAGE("LOADING ", targetData.sStringTable, "into MINIGAME_TEXT_SLOT")
		WAIT(0)
	ENDWHILE
	
	
	//setup any other custom params and setup UI labels for end of mission screen
	IF targetData.myType = MISSION_NAME_HOOKER
		CLEAR_AREA_OF_VEHICLES(targetData.vOtherPedPos, 100)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -690.7630, -1607.4674, -100.9649 >>, << -404.4197, -1856.5288, 100.5908 >>, FALSE)
		SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
		endLabel.tlMissionPassed = "ASS_HK_PASSED"
		endLabel.tlMissionName = "ASS_HK_NAME"
		endLabel.tlTimeTaken = "ASS_HK_TIMER"
		endLabel.tlBaseReward = "ASS_HK_BASE"
		endLabel.tlBonusDesc = "ASS_HK_BDESC"
		endLabel.tlCashEarned = "ASS_HK_CASH"
		endLabel.tlCompletionGold = "ASS_HK_COMP"//"ASS_HK_COMPG"
		endLabel.tlCompletionSilver = "ASS_HK_COMP"//"ASS_HK_COMPS"
		endLabel.tlCompletionBronze = "ASS_HK_COMP"//"ASS_HK_COMPB"
		endLabel.tlCompletionNoMedal = "ASS_HK_COMP"//"ASS_HK_COMPN"
		endLabel.tlContinue = "ASS_HK_CONT"
		fBaseReward = 3000
		fBonusReward = 2000
	ELIF targetData.myType = MISSION_NAME_BUS
		endLabel.tlMissionPassed = "ASS_BS_PASSED"
		endLabel.tlMissionName = "ASS_BS_NAME"
		endLabel.tlTimeTaken = "ASS_BS_TIMER"
		endLabel.tlBaseReward = "ASS_BS_BASE"
		endLabel.tlBonusDesc = "ASS_BS_BDESC"
		endLabel.tlCashEarned = "ASS_BS_CASH"
		endLabel.tlCompletionGold = "ASS_BS_COMP"//"ASS_BS_COMPG"
		endLabel.tlCompletionSilver = "ASS_BS_COMP"//"ASS_BS_COMPS"
		endLabel.tlCompletionBronze = "ASS_BS_COMP"//"ASS_BS_COMPB"
		endLabel.tlCompletionNoMedal = "ASS_BS_COMP"//"ASS_BS_COMPN"
		endLabel.tlContinue = "ASS_BS_CONT"
		fBaseReward = 5000
		fBonusReward = 2000
	ELIF targetData.myType = MISSION_NAME_MULTI
		endLabel.tlMissionPassed = "ASS_ML_PASSED"
		endLabel.tlMissionName = "ASS_ML_NAME"
//		endLabel.tlTargetTime = "ASS_ML_TTIME"
		endLabel.tlTimeTaken = "ASS_ML_TIMER"
		endLabel.tlBaseReward = "ASS_ML_BASE"
		endLabel.tlBonusDesc = "ASS_ML_BDESC"
		endLabel.tlCashEarned = "ASS_ML_CASH"
		endLabel.tlCompletionGold = "ASS_ML_COMP"//"ASS_ML_COMPG"
		endLabel.tlCompletionSilver = "ASS_ML_COMP"//"ASS_ML_COMPS"
		endLabel.tlCompletionBronze = "ASS_ML_COMP"//"ASS_ML_COMPB"
		endLabel.tlCompletionNoMedal = "ASS_ML_COMP"//"ASS_ML_COMPN"
		endLabel.tlContinue = "ASS_ML_CONT"
		fBaseReward = 5000
		fBonusReward = 2000
	ENDIF

	CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
ENDPROC

FUNC BOOL ARE_MISSION_REQUESTS_FINISHED_STREAMING(ASS_TARGET_DATA& targetData, LoadQueueLarge &sLoadQueue, BOOL bRequestCheckpointVehicle = FALSE)
	IF bRequestCheckpointVehicle
		IF NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
			PRINTLN("Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED")
			RETURN FALSE
		ENDIF
	ENDIF
	
	//wait for requests to load
	IF targetData.myType = MISSION_NAME_BUS
		IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs_102")
//		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs_104")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs01")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs02")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs03")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASbs04")
		OR NOT HAS_VEHICLE_ASSET_LOADED(targetData.vehEnum)
		OR NOT HAS_VEHICLE_ASSET_LOADED(targetData.otherVehEnum)
		OR NOT HAS_PTFX_ASSET_LOADED()
			PRINTLN("Waiting for MISSION_NAME_BUS streaming requests")
			RETURN FALSE
		ENDIF
	ELIF targetData.myType = MISSION_NAME_HOOKER
		IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJAShk_101")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJAShk_102")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJAShk_103")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJAShk_104")
		OR NOT HAS_ANIM_DICT_LOADED("ODDJOBS@assassinate@vice@incar")
			PRINTLN("Waiting for MISSION_NAME_HOOKER streaming requests")
		ENDIF
	ELIF targetData.myType = MISSION_NAME_MULTI
		IF NOT HAS_PTFX_ASSET_LOADED()
			PRINTLN("Waiting for MISSION_NAME_MULTI streaming requests")
		ENDIF
	ENDIF
	
	//make sure all models and anims in targetData.streamedModels and targetData.streamedAnims are streamed in
	IF NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
	AND NOT ARE_MODELS_STREAMED(targetData.streamedModels)
	AND NOT ARE_ANIMS_STREAMED(sLoadQueue)
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		PRINTLN("Waiting for streaming")
		RETURN FALSE
	ENDIF
	
	//if we get down here we're done!
	RETURN TRUE
ENDFUNC

PROC MISSION_CLEANUP(LoadQueueLarge &sLoadQueue)
	// cleanup anything you need to. You don't need to dereference script-created entities like peds and vehicles,
	// as code will do this automatically when the script terminates.
	
	ROPE_UNLOAD_TEXTURES()
	
	DISABLE_TAXI_HAILING(FALSE)
	STOP_SCRIPTED_CONVERSATION(FALSE)
	DISABLE_CELLPHONE(FALSE)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
	ENDIF
	
	iSpeedNode = iSpeedNode
	REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedNode)
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1)
	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -690.7630, -1607.4674, -100.9649 >>, << -404.4197, -1856.5288, 100.5908 >>, TRUE) //roads around vice
	
	//hooker
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<< -706.106, -1708.865, -100 >>, << -512.576, -1842.173, 100 >>, 100)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-480.974152,-1801.921509,19.280453>>, <<-712.896790,-1671.164429,29.938675>>, 170.000000)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-674.891479,-1683.639404,9.116446>>, <<-446.189026,-1807.699585,31.724640>>, 200.000000)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-662.699280,-1645.326172,24.071472>>, <<-672.214417,-1777.590088,38.812481>>, 45.000000)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-382.960785,-1688.731567,17.898500>>, <<-431.251160,-1811.230469,37.682495>>, 80.000000)
	
//	SET_ROADS_IN_ANGLED_AREA(<< -713.916, -1705.752, -100 >>, << -524.669, -1845.074, 100 >>, 105, FALSE, TRUE) // roads around vice mission
//	SET_ROADS_IN_AREA(<< 119.4051, -578.6129, -100 >>, << 501.5385, -1243.6771, 100.3242 >>, TRUE) // downtown area for bus mission (large)
	
	
	VECTOR vBusLot = << 414.1398, -640.0020, 27.5001 >> //(same position used in Bus mission to disable parked vehicles in the bus station parking lot)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< vBusLot.x - 40, vBusLot.y - 40, vBusLot.z - 40 >>,
			<< vBusLot.x + 40, vBusLot.y + 40, vBusLot.z + 40 >>, TRUE)
	//bus
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<< 426.035, -644.930, -100 >>, << 421.035, -644.930, 100 >>, 55.000)
	PRINTLN("SETTING ROADS BACK TO ORIGINAL IN CLEANUP")
	
	SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1)
	CLEAR_PED_NON_CREATION_AREA()
	STOP_ANY_PED_MODEL_BEING_SUPPRESSED()
	
	STOP_STREAM()
	
	CLEANUP_LOAD_QUEUE_LARGE(sLoadQueue)
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	PRINTLN("*******************************************************TERMINATING ASSASSINATION MISSION*******************************************************")
	TERMINATE_THIS_THREAD() // important, kills the script
ENDPROC

PROC MISSION_FAILED(LoadQueueLarge &sLoadQueue)
	// this is now called once the screen has faded out for the fail screen.
	
//	UNUSED_PARAMETER(targetData)
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
//	IF DOES_PLAYER_NEED_SAFE_WARP()
//	
//		INIT_SAFE_WARP_POSITIONS(targetData)
//	
//		IF targetData.myType = MISSION_NAME_BUS
//			MISSION_FLOW_SET_FAIL_WARP_LOCATION(vBusSafeWarpPosition, fBusSafeWarpHeading)
//			SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-22.5563, -114.0767, 55.8765>>, 70.2070)
//			PRINTLN("BUS - SAFE WARPING PLAYER TO POSITION: ", vBusSafeWarpPosition)
//		ELIF targetData.myType = MISSION_NAME_HOOKER
//			MISSION_FLOW_SET_FAIL_WARP_LOCATION(vHookerSafeWarpPosition, fHookerSafeWarpHeading)
//			SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<220.3515, -852.6978, 29.1105>>, 250.9746)
//			PRINTLN("HOOKER - SAFE WARPING PLAYER TO POSITION: ", vHookerSafeWarpPosition)
//		ELIF targetData.myType = MISSION_NAME_MULTI
//			MISSION_FLOW_SET_FAIL_WARP_LOCATION(vMultiSafeWarpPosition, fMultiSafeWarpHeading)
//			SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-711.7985, -921.2972, 18.0144>>, 0.0651)
//			PRINTLN("MULTI - SAFE WARPING PLAYER TO POSITION: ", vMultiSafeWarpPosition)
//		ENDIF
//	ELSE
//		PRINTLN("PLAYER DOES NOT NEED SAFE WARP")
//	ENDIF
		
	MISSION_CLEANUP(sLoadQueue) // must only take 1 frame and terminate the thread
ENDPROC

/// PURPOSE:
///    Called when the mission is failed.
///    Sets the fail reason and tells replay controller to start setting up the replay
///    (showing fail reason, fading out etc)
PROC SET_MISSION_FAILED(ASS_ARGS args, ASS_TARGET_DATA& targetData, FAIL_REASON_ENUM eFail)
	TEXT_LABEL tlFailMsg
	
	IF targetData.bFailed = FALSE // (we should only call this fail stuff once)
	
	
		// fail reason now needs to be set as soon as mission is failed
		// as the fail text gets shown before the fade out to the fail screen
		
		// set fail reason
		args.failReason = eFail
			
		//find out which fail message to display
		SWITCH args.failReason
			CASE FAIL_PED_ESCAPED
				IF targetData.myType = MISSION_NAME_HOOKER
					tlFailMsg = "ASS_HK_LOST"	//"You failed to assassinate the target"
				ELIF targetData.myType = MISSION_NAME_BUS
					tlFailMsg = "ASS_BS_LOST"	//"You failed to assassinate the target"
				ELIF targetData.myType = MISSION_NAME_MULTI
					tlFailMsg = "ASS_ML_LOST"	//"You failed to assassinate the targets in time"
				ENDIF
			BREAK
			CASE FAIL_TIME_EXPIRED
				IF targetData.myType = MISSION_NAME_HOOKER
					tlFailMsg = "ASS_HK_LOST"	//"The target escaped"
				ELIF targetData.myType = MISSION_NAME_BUS
					tlFailMsg = "ASS_BS_LEFTBUS"  //"You abandoned the bus"
				ELIF targetData.myType = MISSION_NAME_MULTI
					tlFailMsg = "ASS_ML_TIMEFAIL"	//"You failed to assassinate the targets in time"
				ENDIF
			BREAK
			CASE FAIL_BLOWN_COVER
				IF targetData.myType = MISSION_NAME_HOOKER
					tlFailMsg = "ASS_HK_COVER"	//"Your cover is blown"
				ELIF targetData.myType = MISSION_NAME_BUS
					tlFailMsg = "ASS_BS_COVER"	//"Your cover is blown"
				ENDIF
			BREAK
			CASE FAIL_VEHICLE_DESTROYED
				IF targetData.myType = MISSION_NAME_BUS
					tlFailMsg = "ASS_BS_VEHDEST"	//"The bus was destroyed."
				ENDIF
			BREAK
			CASE FAIL_ABANDONED_MISSION
				IF targetData.myType = MISSION_NAME_HOOKER
					tlFailMsg = "ASS_HK_ABAND"	//"You failed to assassinate the target"
				ELIF targetData.myType = MISSION_NAME_BUS
					tlFailMsg = "ASS_BS_ABAND"	//"You failed to assassinate the target"
				ENDIF
			BREAK
		ENDSWITCH
		
		// set mission failed (fail text will now start displaying)
		targetData.bFailed = TRUE
		ASSASSIN_MISSION_MarkMissionFailed(tlFailMsg)
		MISSION_FLOW_MISSION_FAILED() 
	ENDIF
ENDPROC

//Check in here for anything that could fail the mission
FUNC BOOL UPDATE_FAIL_CONDITIONS(ASS_TARGET_DATA& targetData)
	IF targetData.bFailed
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


// anything that should happen when mission passes
PROC MISSION_PASSED(sAssassinMissionData missionData, SIMPLE_USE_CONTEXT& cucEndScreenCore, LoadQueueLarge &sLoadQueue)
	CLEAR_PRINTS()
		
	SWITCH iEndingScreenStages
		CASE 0
			IF NOT bResultsCreated
				IF passReasonCore = PASS_BONUS
					ASSASSIN_MISSION_MarkMissionPassedWithBonus()
					PRINTLN("SETTING ASSASSIN_MISSION_MarkMissionPassedWithBonus VIA CORE")
					bBonus = TRUE
				ENDIF
			
				fTimeTaken = GET_TIMER_IN_SECONDS(tMissionTime)
				PRINTLN("fTimeTaken = ", fTimeTaken)
				
				SETUP_RESULTS_UI()
				bResultsCreated = TRUE
				
				PRINTLN("iEndingScreenStages = 1")
				iEndingScreenStages = 1
			ENDIF
		BREAK
		CASE 1
			IF NOT bPlayMusic
				PLAY_MISSION_COMPLETE_AUDIO("FRANKLIN_BIG_01")
				bPlayMusic = TRUE
			ENDIF
		
			IF ENDSCREEN_PREPARE(assassinationEndScreen)
			AND IS_MISSION_COMPLETE_READY_FOR_UI()
			
				INIT_SIMPLE_USE_CONTEXT(cucEndScreenCore, FALSE, FALSE, FALSE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(cucEndScreenCore, endLabel.tlContinue,	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
				ADD_SIMPLE_USE_CONTEXT_INPUT(cucEndScreenCore, "ES_XPAND",			FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
				
				DISABLE_CELLPHONE(TRUE)
				
				SETTIMERA(0)
			
				PRINTLN("iEndingScreenStages = 2")
				iEndingScreenStages = 2
			ENDIF
		BREAK
		CASE 2
			IF RENDER_ENDSCREEN(assassinationEndScreen, FALSE)
				bSkippedEndingScreen = TRUE
			ENDIF
			IF NOT bSkippedEndingScreen
				UPDATE_SIMPLE_USE_CONTEXT(cucEndScreenCore)
			ENDIF
			
			//Only read button input until player presses accept
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,			INPUT_FRONTEND_ENDSCREEN_ACCEPT)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,	INPUT_FRONTEND_PAUSE_ALTERNATE)		// B*2276517 - Player can now press esc to close endscreen, if player control is on during endscreen
				IF NOT bSkippedEndingScreen
					PRINTLN("bSkippedEndingScreen = TRUE")
					bSkippedEndingScreen = TRUE
					//Start anim-out
					ENDSCREEN_START_TRANSITION_OUT(assassinationEndScreen)
				ENDIF
			ENDIF
			
			IF bSkippedEndingScreen
				//Wait until the end screen stops transition-out
				IF RENDER_ENDSCREEN(assassinationEndScreen)
					ENDSCREEN_SHUTDOWN(assassinationEndScreen)
					
					DISABLE_CELLPHONE(FALSE)
					
					++g_savedGlobals.sAssassinData.iCurrentAssassinRank
					ASSASSIN_MISSION_MarkMissionPassed()
					MISSION_FLOW_MISSION_PASSED()
					ASSASSIN_MISSION_AwardAssReward(missionData)
					
					ODDJOB_AUTO_SAVE()
					MISSION_CLEANUP(sLoadQueue)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


//Get the appropriate cutscene function for the selected assassination
PROC GET_CUSTOM_CUTSCENE(ASS_TARGET_DATA& targetData)
	IF targetData.fCutsceneDoF = 0
		targetData.fCutsceneDoF = 65
	ENDIF		
ENDPROC

PROC CREATE_ASSASSINATION_TARGET(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	IF NOT DOES_ENTITY_EXIST(args.myTarget)
		args.myTarget = CREATE_PED(PEDTYPE_CIVMALE, targetData.targetEnum, targetData.vTargetPos, targetData.fTargetHead)
		SET_PED_MODEL_IS_SUPPRESSED(targetData.targetEnum, TRUE)
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(args.myTarget, TRUE)
		SET_PED_CAN_BE_TARGETTED(args.myTarget, TRUE)
		SET_PED_CAN_BE_SHOT_IN_VEHICLE(args.myTarget, TRUE)
		SET_PED_CONFIG_FLAG(args.myTarget, PCF_GetOutUndriveableVehicle, TRUE)
//		SET_PED_CONFIG_FLAG(args.myTarget, PCF_AllowToBeTargetedInAVehicle, TRUE)
		SET_ENTITY_IS_TARGET_PRIORITY(args.myTarget, TRUE)
		
		//target should hate the player and cops
		IF NOT bCreateRelationshipGroup
	 		ADD_RELATIONSHIP_GROUP("Target", relTemp)
			bCreateRelationshipGroup = TRUE
		ENDIF
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relTemp, RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relTemp)
		
		SET_PED_RELATIONSHIP_GROUP_HASH(args.myTarget, relTemp)
		
		IF targetData.bNonTemp
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(args.myTarget, TRUE)
		ENDIF
		IF targetData.bLeaveVehicles
			SET_PED_COMBAT_ATTRIBUTES(args.myTarget, CA_LEAVE_VEHICLES, FALSE)
			SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(args.myTarget, TRUE)
		ENDIF	
//		IF targetData.iHealth != 0
//			SET_ENTITY_HEALTH(args.myTarget, targetData.iHealth)
//		ENDIF
		IF targetData.weapTarget <> WEAPONTYPE_INVALID
			GIVE_WEAPON_TO_PED(args.myTarget, targetData.weapTarget, -1)
		ENDIF
		
		IF targetData.myType = MISSION_NAME_BUS
			SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,2), 2, 2, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,8), 1, 2, 0) //(accs)
			
			ADD_PED_FOR_DIALOGUE(args.assConv, 3, args.myTarget, "INVESTOR")
			
		ELIF targetData.myType = MISSION_NAME_HOOKER
			ADD_PED_FOR_DIALOGUE(args.assConv, 3, args.myTarget, "OJAhkJUNKIE")
			SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_CONFIG_FLAG(args.myTarget, PCF_PreventPedFromReactingToBeingJacked, TRUE)
			SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(args.myTarget, TRUE)
			SET_PED_MONEY(args.myTarget, GET_RANDOM_INT_IN_RANGE(80, 100))
			GIVE_WEAPON_TO_PED(args.myTarget, WEAPONTYPE_COMBATPISTOL, -1)
			SET_PED_ACCURACY(args.myTarget, 50)
//		ELIF targetData.myType = MISSION_NAME_MULTI
//			SET_ENTITY_HEALTH(args.myTarget, 100)
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_ASSASSINATION_TARGET_VEHICLE(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	//create any necessary vehicles
	IF targetData.vehEnum != DUMMY_MODEL_FOR_SCRIPT
		IF NOT DOES_ENTITY_EXIST(args.myVehicle)
			args.myVehicle = CREATE_VEHICLE(targetData.vehEnum, targetData.vVehPos, targetData.fVehHead)
			IF targetData.myType = MISSION_NAME_HOOKER
				SET_VEHICLE_EXTRA(args.myVehicle, 2, TRUE)
				REMOVE_VEHICLE_WINDOW(args.myVehicle, SC_WINDOW_FRONT_RIGHT)
				REMOVE_VEHICLE_WINDOW(args.myVehicle, SC_WINDOW_FRONT_LEFT)
				SET_VEHICLE_DOORS_LOCKED(args.myVehicle, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
//				SET_VEHICLE_CAN_BE_TARGETTED(args.myVehicle, TRUE)
			ELIF targetData.myType = MISSION_NAME_BUS
//				SET_VEHICLE_TYRES_CAN_BURST(args.myVehicle, FALSE)
				SET_ENTITY_LOAD_COLLISION_FLAG(args.myVehicle, TRUE)
				SET_VEHICLE_COLOUR_COMBINATION(args.myVehicle, 0)
			ENDIF
			ADD_VEHICLE_UPSIDEDOWN_CHECK(args.myVehicle)
			
			IF targetData.bDriving = TRUE	
				IF DOES_ENTITY_EXIST(args.myVehicle)
					IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
						IF NOT IS_PED_INJURED(args.myTarget)
							SET_PED_INTO_VEHICLE(args.myTarget,args.myVehicle)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//function that will create all the props, actors, and cars to setup the scene the player will see when he gets to his target
PROC CREATE_ASSASSINATION_SCENE(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	
	//set up player's conversation data
	ADD_PED_FOR_DIALOGUE(args.assConv, 1, PLAYER_PED_ID(), "FRANKLIN")
	
	//set taxi drop-off location data if the player hails a taxi to the assassination scene
		IF targetData.myType = MISSION_NAME_HOOKER
			args.vTargetBlipTaxiPos = << -547.1670, -1827.5135, 21.9023 >>
			args.fTargetBlipTaxiHead = 81.6995
		ELIF targetData.myType = MISSION_NAME_BUS
			args.vTargetBlipTaxiPos = << 359.6962, -687.8932, 28.1880 >>
			args.fTargetBlipTaxiHead = 326.4130
		ENDIF
		
		//Create a blip for the first destination
		IF DOES_BLIP_EXIST(args.myTargetBlip)
			REMOVE_BLIP(args.myTargetBlip)
		ENDIF
	
		IF targetData.myType = MISSION_NAME_HOOKER
			SET_BLIP_NAME_FROM_TEXT_FILE(args.myTargetBlip, "ASS_HK_DESTBLIP")
		ELIF targetData.myType = MISSION_NAME_BUS
//			SET_BLIP_NAME_FROM_TEXT_FILE(args.myTargetBlip, "ASS_BS_BLIP")
		ENDIF
		IF NOT ARE_VECTORS_EQUAL(args.vTargetBlipTaxiPos, NULL_VECTOR()) 
			SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(args.myTargetBlip, args.vTargetBlipTaxiPos, args.fTargetBlipTaxiHead)
		ENDIF
		
		
	//create target, target blip, and set target attributes
	//don't create for Bus or Vice mission since the target will be created later in those scripts when needed
	IF targetData.myType <> MISSION_NAME_BUS
	AND targetData.myType <> MISSION_NAME_HOOKER
		CREATE_ASSASSINATION_TARGET(targetData, args)
	ENDIF
	
	//the Target's vehicle in the Vice mission will be created later in that script when the target is created
	IF targetData.myType <> MISSION_NAME_HOOKER
		CREATE_ASSASSINATION_TARGET_VEHICLE(targetData, args)
	ENDIF

	//create any secondary peds (if set)
		IF targetData.otherPedEnum != DUMMY_MODEL_FOR_SCRIPT
		AND targetData.myType <> MISSION_NAME_BUS
			IF NOT DOES_ENTITY_EXIST(args.myOtherPed)
				args.myOtherPed = CREATE_PED(PEDTYPE_CIVMALE, targetData.otherPedEnum, targetData.vOtherPedPos, targetData.fOtherPedHead)	
				IF targetData.myType = MISSION_NAME_HOOKER	//hooker ped
					TASK_PLAY_ANIM(args.myOtherPed, "ODDJOBS@ASSASSINATE@VICE@HOOKER", "idle_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
					SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(args.myOtherPed, TRUE)
					ADD_PED_FOR_DIALOGUE(args.assConv, 4, args.myOtherPed, "OJAhkHOOKER")
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(args.myOtherPed, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(args.myOtherPed, CA_ALWAYS_FLEE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(args.myOtherPed, FA_DISABLE_HANDS_UP, TRUE)
					SET_PED_CONFIG_FLAG(args.myOtherPed, PCF_PreventPedFromReactingToBeingJacked, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
	//create any additional vehicles/peds (if set)
		IF targetData.otherVehEnum != DUMMY_MODEL_FOR_SCRIPT
			IF targetData.myType != MISSION_NAME_BUS	//(don't do this for bus mission since we will create the bike vehicles later in that script)
				args.myOtherVehicle = CREATE_VEHICLE(targetData.otherVehEnum, targetData.vOtherVehPos, targetData.fOtherVehHead)
			ENDIF
			
			IF targetData.myType = MISSION_NAME_HOOKER
				SET_VEHICLE_EXTRA(args.myOtherVehicle, 3, FALSE)
				REMOVE_VEHICLE_WINDOW(args.myOtherVehicle, SC_WINDOW_FRONT_RIGHT)
				REMOVE_VEHICLE_WINDOW(args.myOtherVehicle, SC_WINDOW_FRONT_LEFT)
				
				//guy in the car in the intro cutscene
				piHookerCutscenePed = CREATE_PED_INSIDE_VEHICLE(args.myOtherVehicle, PEDTYPE_CIVMALE, targetData.auxPedEnum)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piHookerCutscenePed, TRUE)
				ADD_PED_FOR_DIALOGUE(assArgs.assConv, 5, piHookerCutscenePed, "KerbCrawler")
//				TASK_PLAY_ANIM(piHookerCutscenePed, "ODDJOBS@ASSASSINATE@VICE@PARK", "idle_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
		ENDIF
		
		IF targetData.myType = MISSION_NAME_MULTI
			IF NOT DOES_BLIP_EXIST(args.myTargetBlip)
				args.myTargetBlip = ADD_BLIP_FOR_COORD(targetData.vDestPos)
				SET_BLIP_ROUTE(args.myTargetBlip, TRUE)
				SET_BLIP_ROUTE_COLOUR(args.myTargetBlip, BLIP_COLOUR_RED)
				SET_BLIP_COLOUR(args.myTargetBlip, BLIP_COLOUR_RED)
				PRINTLN("CREATING BLIP MULTI - args.myTargetBlip")
			ENDIF
		ELIF targetData.myType = MISSION_NAME_BUS
			args.myTargetBlip = ADD_BLIP_FOR_COORD(targetData.vDestPos)
			SET_BLIP_ROUTE(args.myTargetBlip, TRUE)
			SET_BLIP_ROUTE_COLOUR(args.myTargetBlip, BLIP_COLOUR_BLUE)
			SET_BLIP_COLOUR(args.myTargetBlip, BLIP_COLOUR_BLUE)
			SET_BLIP_NAME_FROM_TEXT_FILE(args.myTargetBlip, "ASS_BS_BLIP")
			PRINTLN("CREATING BLIP BUS- args.myTargetBlip")
		ELSE
			args.myTargetBlip = ADD_BLIP_FOR_COORD(targetData.vDestPos)
			SET_BLIP_ROUTE(args.myTargetBlip, TRUE)
			PRINTLN("CREATING BLIP - args.myTargetBlip")
		ENDIF
	
	// create any necessary props
		INT tempInt
		REPEAT maxProps tempInt
			IF NOT IS_VECTOR_ZERO(targetData.vPropPos[tempInt])
				args.myProp[tempInt] = CREATE_OBJECT(targetData.propEnum[tempInt], targetData.vPropPos[tempInt])
				SET_ENTITY_RECORDS_COLLISIONS(args.myProp[tempInt], TRUE)
				SET_ENTITY_COLLISION(args.myProp[tempInt], TRUE)
				PRINTLN("PROP HAS COLLISIONS = ", tempInt)
				
				IF tempInt = 1
					FREEZE_ENTITY_POSITION(args.myProp[tempInt], TRUE)
				ENDIF
				IF targetData.fPropHead[tempInt] != 0
					SET_ENTITY_HEADING(args.myProp[tempInt], targetData.fPropHead[tempInt])
				ENDIF
			ENDIF
		ENDREPEAT

//	//create any weapon pickups
//		IF NOT IS_VECTOR_ZERO(targetData.vPickupPos)
//			IF NOT IS_VECTOR_ZERO(targetData.vPickupPos)
//				CREATE_PICKUP(targetData.ptType, targetData.vPickupPos)
//			ENDIF
//		ENDIF
ENDPROC

PROC SET_HOOKER_PED_COMPONENTS(PED_INDEX ped)
	SET_PED_COMPONENT_VARIATION(ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
	SET_PED_COMPONENT_VARIATION(ped, INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
	SET_PED_COMPONENT_VARIATION(ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
ENDPROC


PROC BLIP_HOOKER_DESTINATION(ASS_TARGET_DATA& targetData, ASS_ARGS& args, BOOL bBlipOnRetry = TRUE)
	//Create a blip for the first destination
	IF bBlipOnRetry
		IF DOES_BLIP_EXIST(args.myTargetBlip)
//			REMOVE_BLIP(args.myTargetBlip)
		ELSE
			IF DOES_ENTITY_EXIST(args.myOtherPed)
				args.myTargetBlip = ADD_BLIP_FOR_ENTITY(assArgs.myOtherPed)
			ELSE
				args.myTargetBlip = ADD_BLIP_FOR_COORD(targetData.vOtherPedPos)		//hooker creation position
			ENDIF
		
			SET_BLIP_ROUTE(args.myTargetBlip, TRUE)
			SET_BLIP_COLOUR(args.myTargetBlip, BLIP_COLOUR_BLUE)
			SET_BLIP_ROUTE_COLOUR(args.myTargetBlip, BLIP_COLOUR_BLUE)
	//			SET_BLIP_NAME_FROM_TEXT_FILE(args.myTargetBlip, "ASS_HK_DESTBLIP")
			PRINTLN("CREATING BLIP - args.myTargetBlip")
		ENDIF
	ENDIF
ENDPROC


PROC CREATE_ASSASSINATION_SCENE_HOOKER(ASS_TARGET_DATA& targetData, ASS_ARGS& args, BOOL bBlipOnRetry = TRUE)

	//create any secondary peds (if set)
		IF NOT DOES_ENTITY_EXIST(args.myOtherPed)
			IF targetData.otherPedEnum != DUMMY_MODEL_FOR_SCRIPT
				args.myOtherPed = CREATE_PED(PEDTYPE_CIVFEMALE, targetData.otherPedEnum, targetData.vOtherPedPos, targetData.fOtherPedHead)	
	//			TASK_PLAY_ANIM(args.myOtherPed, "ODDJOBS@ASSASSINATE@VICE@HOOKER", "idle_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
	//			TASK_PLAY_ANIM(args.myOtherPed, "ODDJOBS@ASSASSINATE@VICE@HOOKER", "Hooker_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
				SET_HOOKER_PED_COMPONENTS(args.myOtherPed)
				TASK_PLAY_ANIM(args.myOtherPed, "amb@world_human_prostitute@hooker@idle_a", "idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
				SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(args.myOtherPed, TRUE)
				ADD_PED_FOR_DIALOGUE(args.assConv, 4, args.myOtherPed, "OJAhkHOOKER")
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(args.myOtherPed, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(args.myOtherPed, CA_ALWAYS_FLEE, TRUE)
				SET_PED_FLEE_ATTRIBUTES(args.myOtherPed, FA_DISABLE_HANDS_UP, TRUE)
				SET_PED_CONFIG_FLAG(args.myOtherPed, PCF_PreventPedFromReactingToBeingJacked, TRUE)
				SET_PED_CAN_BE_TARGETTED(args.myOtherPed, FALSE)
			ENDIF
		ENDIF
		
	//create any additional vehicles/peds (if set)
		IF NOT DOES_ENTITY_EXIST(piHookerCutscenePed)
			IF targetData.otherVehEnum != DUMMY_MODEL_FOR_SCRIPT
				args.myOtherVehicle = CREATE_VEHICLE(targetData.otherVehEnum, targetData.vOtherVehPos, targetData.fOtherVehHead)
				SET_VEHICLE_EXTRA(args.myOtherVehicle, 3, FALSE)
				REMOVE_VEHICLE_WINDOW(args.myOtherVehicle, SC_WINDOW_FRONT_RIGHT)
				REMOVE_VEHICLE_WINDOW(args.myOtherVehicle, SC_WINDOW_FRONT_LEFT)
				
				//guy in the car in the intro cutscene
				piHookerCutscenePed = CREATE_PED_INSIDE_VEHICLE(args.myOtherVehicle, PEDTYPE_CIVMALE, targetData.auxPedEnum)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piHookerCutscenePed, TRUE)
				ADD_PED_FOR_DIALOGUE(assArgs.assConv, 5, piHookerCutscenePed, "KerbCrawler")
//				TASK_PLAY_ANIM(piHookerCutscenePed, "ODDJOBS@ASSASSINATE@VICE@PARK", "idle_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
		ENDIF
		
		BLIP_HOOKER_DESTINATION(targetData, args, bBlipOnRetry)
	
		IF NOT ARE_VECTORS_EQUAL(args.vTargetBlipTaxiPos, NULL_VECTOR()) 
			SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(args.myTargetBlip, args.vTargetBlipTaxiPos, args.fTargetBlipTaxiHead)
		ENDIF
	
	// create any necessary props
		INT tempInt
		REPEAT maxProps tempInt
			IF NOT IS_VECTOR_ZERO(targetData.vPropPos[tempInt])
				args.myProp[tempInt] = CREATE_OBJECT(targetData.propEnum[tempInt], targetData.vPropPos[tempInt])
				IF tempInt = 1
					FREEZE_ENTITY_POSITION(args.myProp[tempInt], TRUE)
				ENDIF
				IF targetData.fPropHead[tempInt] != 0
					SET_ENTITY_HEADING(args.myProp[tempInt], targetData.fPropHead[tempInt])
				ENDIF
			ENDIF
		ENDREPEAT
ENDPROC

//return TRUE if the target is no longer alive and remove his blip
FUNC BOOL CHECK_FOR_TARGET_DEATH(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	IF IS_SCREEN_FADED_IN()
		IF targetData.myType <> MISSION_NAME_BUS
			IF DOES_ENTITY_EXIST(args.myTarget)
				IF IS_ENTITY_DEAD(args.myTarget)
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0)
					
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(args.myTarget)
				IF IS_PED_INJURED(args.myTarget)
					IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
					AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(args.myTarget, args.myVehicle)
						PRINTLN("AWARDING BONUS - KILLED BY BUS")
						passReasonCore = PASS_BONUS
					ELSE
						//added check to award the player a bonus if they collided with the target and he falls off the bike and but dies from ground impact (not the initial impact)
						IF IS_TIMER_STARTED(tBonusTimer)
						AND GET_TIMER_IN_SECONDS(tBonusTimer) < 3.0
							PRINTLN("CHECK_FOR_TARGET_DEATH - AWARDING BONUS - TARGET DIED WITHIN BONUS TIMER")
							passReasonCore = PASS_BONUS
						ELSE
							PRINTLN("CHECK_FOR_TARGET_DEATH - NOT AWARDING BONUS - TARGET DIED AFTER BUS TIMER EXPIRED")
						ENDIF
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0)
					
					RETURN TRUE
				ELSE
					IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
							IF GET_ENTITY_SPEED(args.myVehicle) > 10
							AND IS_ENTITY_TOUCHING_ENTITY(args.myVehicle, args.myTarget)
								IF IS_PED_IN_ANY_VEHICLE(args.myTarget)
									KNOCK_PED_OFF_VEHICLE(args.myTarget)
								ENDIF
								SET_ENTITY_HEALTH(args.myTarget, 0)
								passReasonCore = PASS_BONUS
								
								PRINTLN("CHECK_FOR_TARGET_DEATH - AWARDING BONUS - KNOCKED OFF HIS BIKE AT SPEED")
								
								REPLAY_RECORD_BACK_FOR_TIME(3.0)
								
								RETURN TRUE
							ELSE
//								PRINTLN("CHECK_FOR_TARGET_DEATH - DIDNT HIT THE TARGET AT ENOUGH SPEED OR NEVER TOUCHED")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


 //Intro cutscene for the Bus assassination
FUNC BOOL ASSASSIN_INTRO_CUSTOM_BUS()
	CAMERA_INDEX tempCam
	
	SWITCH introCutsceneState
		//show camera shot panning to outside of bus station
		CASE INTRO_CUTSCENE_CREATE
			GET_ASSASSIN_CUTSCENE_START_TIME()

			//create camera to interp to
			tempCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 420.7911, -676.8039, 32.4078 >>, << -6.7643, -0.0000, -5.5403 >>, 39.8344)
			SET_CAM_ACTIVE_WITH_INTERP(tempCam, myCamera, 6500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
			SETTIMERA(0)
			
			PRINT_NOW("ASS_BS_INFO", DEFAULT_GOD_TEXT_TIME, 1)
			introCutsceneState = INTRO_CUTSCENE_TRANSITIONA
		BREAK
		CASE INTRO_CUTSCENE_TRANSITIONA
			IF TIMERA() >= 7000
				introCutsceneState = INTRO_CUTSCENE_END
			ENDIF
		BREAK
		CASE INTRO_CUTSCENE_END
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			CLEAR_HELP()
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	
	//check if player is trying to skip cutscene and end the cutscene cleanly if so
	IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		CLEAR_HELP()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Intro cutscene of a car pulling away from the hooker and her playing a rejection anim as the car drives away
FUNC BOOL ASSASSIN_INTRO_CUSTOM_HOOKER()
	CAMERA_INDEX transitionCam, transitionCam2, transitionCam3, transitionCam4
	
	SWITCH introCutsceneState
		//close up shot of hooker
		CASE INTRO_CUTSCENE_CREATE
			GET_ASSASSIN_CUTSCENE_START_TIME()
			
			CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJAShk_DEAL", CONV_PRIORITY_VERY_HIGH)
			
			//remove windows in cutscene vehicle so that they don't obscure any cutscene shots
			IF IS_VEHICLE_DRIVEABLE(assArgs.myOtherVehicle)
				REMOVE_VEHICLE_WINDOW(assArgs.myOtherVehicle, SC_WINDOW_FRONT_RIGHT)
				REMOVE_VEHICLE_WINDOW(assArgs.myOtherVehicle, SC_WINDOW_FRONT_LEFT)
			ENDIF

			//reset the players vehicle properly off screen
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			ENDIF
			
			//set up cameras for next shot
			transitionCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -594.4413, -1810.6296, 23.6320 >>, << 0.6454, -0.0000, 62.1310 >>, 35)
			transitionCam2 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -594.6440, -1810.7355, 23.4633 >>, << -5.4366, -0.0000, 70.6504 >>, 40.6185)
			SET_CAM_ACTIVE_WITH_INTERP(transitionCam2, transitionCam, 4500)
			
			//get the cutscene ped in the car ready for the next shot
			IF NOT IS_PED_INJURED(piHookerCutscenePed)
				CLEAR_PED_TASKS(piHookerCutscenePed)
			ENDIF
			
			SETTIMERA(0)
			introCutsceneState = INTRO_CUTSCENE_TRANSITIONA
		BREAK
		
		//camera follows her close up again as she propositions a deal
		CASE INTRO_CUTSCENE_TRANSITIONA
			IF TIMERA() > 2700
				IF NOT IS_PED_INJURED(assArgs.myOtherPed)
					transitionCam3 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -596.8697, -1812.2986, 23.2769 >>, << -5.1263, 0.0680, -30.9335 >>, 35)
					transitionCam4 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -596.8176, -1812.2112, 23.2679 >>, << -5.1263, 0.0680, -30.9335 >>, 35)
					SET_CAM_NEAR_DOF(transitionCam3, 0.5)
					SET_CAM_FAR_DOF(transitionCam3, 7.0)
					SET_CAM_NEAR_DOF(transitionCam4, 0.5)
					SET_CAM_FAR_DOF(transitionCam4, 7.0)
					SHAKE_CAM(transitionCam3, "HAND_SHAKE", 0.75)
					SET_CAM_ACTIVE_WITH_INTERP(transitionCam4, transitionCam3, 2500)
					SETTIMERA(0)
					introCutsceneState = INTRO_CUTSCENE_TRANSITIONB
				ENDIF
			ELIF TIMERA() > 2300
				IF NOT IS_PED_INJURED(piHookerCutscenePed)
					IF GET_SCRIPT_TASK_STATUS(piHookerCutscenePed, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK
						TASK_PLAY_ANIM(piHookerCutscenePed, "ODDJOBS@ASSASSINATE@VICE@PARK", "idle_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//distance shot of her playing rejection anim as the car drives away
		CASE INTRO_CUTSCENE_TRANSITIONB
			IF TIMERA() > 2600
				IF NOT IS_PED_INJURED(assArgs.myOtherPed)
					IF IS_VEHICLE_DRIVEABLE(assArgs.myOtherVehicle)
						transitionCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -591.1110, -1812.7780, 23.5779 >>, << -1.9585, -0.0000, 61.1133 >>, 25)
						transitionCam2 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -590.7567, -1812.1222, 23.7707 >>, << -1.9585, -0.0000, 61.1133 >>, 25)
						
						SET_CAM_ACTIVE_WITH_INTERP(transitionCam2, transitionCam, 4500)
						SET_ENTITY_ALL_ANIMS_SPEED(assArgs.myOtherPed, 1)	//RESTORE ANIM SPEEDS TO DEFAULT
						TASK_PLAY_ANIM(assArgs.myOtherPed, "ODDJOBS@ASSASSINATE@VICE@HOOKER", "rejection", SLOW_BLEND_IN)
					ENDIF
				ENDIF
				
				SETTIMERA(0)
				introCutsceneState = INTRO_CUTSCENE_END 
			ENDIF
		BREAK

		CASE INTRO_CUTSCENE_END
			IF TIMERA() >= 4000
				RETURN TRUE
			ELIF TIMERA() > 1800
				IF NOT IS_PED_INJURED(piHookerCutscenePed)
					IF IS_VEHICLE_DRIVEABLE(assArgs.myOtherVehicle)
						IF GET_SCRIPT_TASK_STATUS(piHookerCutscenePed, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) <> PERFORMING_TASK
							TASK_VEHICLE_DRIVE_TO_COORD(piHookerCutscenePed, assArgs.myOtherVehicle, <<-590.38, -1586.85, 26.76>>, 12, DRIVINGSTYLE_NORMAL, ruiner, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS, 3, -1)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	
	//check if player is trying to skip cutscene and end the cutscene gracefully if so
	IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
		IF NOT IS_PED_INJURED(piHookerCutscenePed)
			IF IS_VEHICLE_DRIVEABLE(assArgs.myOtherVehicle)
				TASK_VEHICLE_DRIVE_TO_COORD(piHookerCutscenePed, assArgs.myOtherVehicle, <<-590.38, -1586.85, 26.76>>, 12, DRIVINGSTYLE_NORMAL, ruiner, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS, 3, -1)
			ENDIF
			KILL_ANY_CONVERSATION()
		ENDIF
		IF NOT IS_PED_INJURED(assArgs.myOtherPed)
			SET_ENTITY_ALL_ANIMS_SPEED(assArgs.myOtherPed, 1)	//RESTORE ANIM SPEEDS TO DEFAULT
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
