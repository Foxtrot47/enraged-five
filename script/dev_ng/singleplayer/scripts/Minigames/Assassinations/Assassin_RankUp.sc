//////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      			//
//      SCRIPT NAME     : ASSASSIN Rank Up debug script                                 			//
//      AUTHOR          : Michael Bagley	                                            			//
//      DESCRIPTION     : Quickly increment the player's global save data rank for assassinations	//
//                                                                                      			//
//////////////////////////////////////////////////////////////////////////////////////////////////////

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "controller_assassination_lib.sch"
USING "SP_globals_saved.sch"
USING "script_player.sch"
USING "commands_script.sch"

// Initialisation and the script loop
SCRIPT
	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// The script loop
	WHILE (TRUE)
		// Maintain the script – perform per-frame functionality
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			++g_savedGlobals.sAssassinData.iCurrentAssassinRank 
			ASSASSIN_MISSION_MarkMissionPassed()

			PRINTLN("CURRENT ASSASSIN RANK = ", g_savedGlobals.sAssassinData.iCurrentAssassinRank)
			TERMINATE_THIS_THREAD()
		ENDIF
	 	
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
