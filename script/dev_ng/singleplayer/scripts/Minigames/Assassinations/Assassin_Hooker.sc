
//////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      			//
//      SCRIPT NAME     : Assassin_Hooker.sch       							        			//
//      AUTHOR          : Michael Bagley	                                            			//
//      DESCRIPTION     : Vice Assassination Script - Kill the target after he picks up the hooker	//
//                                                                                      			//
//////////////////////////////////////////////////////////////////////////////////////////////////////

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "Assassin_Core.sch"
USING "Assassin_Shared.sch"
USING "Assassination_briefing_lib.sch"
USING "replay_public.sch"
USING "clearMissionArea.sch"
USING "chase_hint_cam.sch"
USING "timelapse.sch"
USING "replay_private.sch"
USING "load_queue_public.sch"

#IF IS_DEBUG_BUILD
      USING "shared_debug.sch"
      USING "script_debug.sch"
      USING "select_mission_stage.sch"
#ENDIF



// ********************************** Script Globals *************************************
ENUM CAM_VICE_ENUM
	CAM_VICE_PICK_CAM = 0,
	CAM_VICE_CONTROL_CAM
ENDENUM


ENUM MISSION_VICE_STAGE
	MISSION_VICE_SETUP,
	MISSION_SEND_FIRST_CAR,
	MISSION_VICE_SEND_CARS,
	MISSION_VICE_CHECK_CARS,
	MISSION_VICE_ENTER_CAR,
	MISSION_VICE_FOLLOW_CAR,
	MISSION_VICE_PARK_CAR,
	MISSION_VICE_TASK_PLAYER,
	MISSION_VICE_CAR_FUN,
	MISSION_VICE_TARGET_LEAVE,
	MISSION_VICE_TARGET_POST_KNOCKOUT,
	MISSION_VICE_TARGET_ESCAPE,
	MISSION_VICE_TARGET_COMBAT
ENDENUM
MISSION_VICE_STAGE targetStage

// ------------------------------Cutscene variables------------------------------

ENUM PAYPHONE_CUTSCENE
	PAYPHONE_CUTSCENE_INIT = 0,
	PAYPHONE_CUTSCENE_UPDATE,
	PAYPHONE_CUTSCENE_SKIP,
	PAYPHONE_CUTSCENE_CLEANUP
ENDENUM
PAYPHONE_CUTSCENE payPhoneCutsceneStages = PAYPHONE_CUTSCENE_INIT

OBJECT_INDEX oPayPhone, oPayPhoneAnimated
CAMERA_INDEX camPayPhoneIntro01
VECTOR vPhoneLocation = <<214.3677, -853.6607, 29.3826>>
VECTOR vPlayerWarpPosition = <<214.1545, -853.6061, 29.3878>>
FLOAT fPlayerWarpHeading = 346.6454

FLOAT fCutsceneLength = 40.0
FLOAT fRightX, fRightY

VECTOR vCameraPosition01 = <<215.2688, -853.8094, 30.9712>> 
VECTOR vCameraRotation01 = <<-3.6234, -0.0263, 57.0304>>
structTimer tPayphoneCutsceneTimer
structTimer tPayphoneCameraTimer
BOOL bSkipCutscene = FALSE
BOOL bPlayedPayPhoneConvo = FALSE
BOOL bTargetKillLinePlayed = FALSE
BOOL bPlayerWentWanted = FALSE

VECTOR scenePosition = <<0,0,0>>
VECTOR sceneRotation = <<0,0,0>>
INT iSceneId
INT iIntroCameraStages = 0
INT iNavMeshBlocking

STREAMVOL_ID svPhoneCutscene

// ------------------------------Cutscene variables------------------------------

ASS_ARGS asnArgs
ASS_TARGET_DATA targetData
//PASS_REASON_ENUM passReasonVice

//BOOL bTestingJapaneseVersion
BOOL bTargetSpottedPlayerInScrapyard
BOOL bPimpDialogue
BOOL bHookDialogue
BOOL bDrugDialogue
BOOL bPrintedMsg
BOOL bPlayerBlockingDeal
BOOL bTargetCarStuck
BOOL bTestDummyCarForPanic
BOOL bReplaying = FALSE
BOOL bFirstObjectivePrinted
BOOL bPedConfigFlagsRestored
BOOL bReplayVehicleAvailable = FALSE
BOOL bDetachGirlFromCar = FALSE
BOOL bPanicLinePlayed = FALSE
//BOOL bSetActionMode = FALSE
BOOL bIntroDone = FALSE
BOOL bRunIntro = FALSE
BOOL bTaskedTarget = FALSE
BOOL bFinishedWaypointRecording = FALSE
BOOL bTriggeredSecondMusicCue = FALSE
BOOL bPickupLinePlayed = FALSE
BOOL bCasualConversationFinished = FALSE
BOOL bPlayedRegularLine = FALSE
BOOL bPlayedCheapLine = FALSE
BOOL bHookerInCar = FALSE
BOOL bHookerTaskedToEnterCar = FALSE
BOOL bTailedTooClose = FALSE
BOOL bPlayedCellPhoneCall = FALSE
BOOL bTooCloseToBouncingCar = FALSE
BOOL bTaskedToLookAtTarget = FALSE
//BOOL bMinorAttacked = FALSE
//BOOL bTooCloseConvo01 = FALSE
BOOL bTriggeredAggro = FALSE
//BOOL bGivenSubtask = FALSE
BOOL bPlayPanicAnimation = FALSE
//BOOL bDebugSkipping = FALSE
BOOL bLeaveAreaPrinted = FALSE
BOOL bChaseHintCamKilled = FALSE
BOOL bKillTargetCheckpointSet = FALSE

//BOOL bFirstCheckpointSceneLoad = FALSE
//BOOL bSecondCheckpointSceneLoad = FALSE
//BOOL bThirdCheckpointSceneLoad = FALSE
BOOL bPlayedFirstConvo = FALSE
BOOL bGrabbedPosition = FALSE
BOOL bCreatedViceScene = FALSE
BOOL bContinueWithScene = FALSE
BOOL bTaskedHookerCheap = FALSE
BOOL bTriggeredCopsMusicCue = FALSE

STRING sPanicLine
STRING sHookerAnimDict = "mini@hookers_spvanilla"

INT iSpeechStage
INT iFrameCountAsn
INT iStageToUse
INT iFollowTooCloseStages = 0
INT iFirstCarStages = 0
INT iCounter = 0
INT iWalkByGirlStages = 0

FLOAT fTooCloseTime = -1

VEHICLE_INDEX viParkedCar
VEHICLE_INDEX viReplayVehicle

VEHICLE_INDEX viFirstCar
PED_INDEX pedFirstDriver

PED_INDEX pedWalkByGirl
MODEL_NAMES mnToUse

VECTOR vTargetStartPos
VECTOR vDummyStartPos
VECTOR vIntroCutscenePos[2]
VECTOR vTargetCarPos
VECTOR vJunkyardPos = << -442.38562, -1704.35461, 17.89104 >>
VECTOR vSphereToCheckForSendCars = << -524.8151, -1771.9265, 20.3315 >>
VECTOR vPlayerKillPosition
VECTOR vRandomVehicleDriveToPos = << -694.35645, -1607.84534, 21.08559 >>

//VECTOR vTargetVehiclePosition = << -595.08, -1811.84, 22.5398 >>
//FLOAT fTargetVehicleHeading = 66.860649

VECTOR	vMoveVehiclePosition01 = <<219.4298, -851.6688, 29.1372>> 
FLOAT 	fMoveVehicleHeading01 = 249.6805

VECTOR	vMoveVehiclePosition02 = << -558.4304, -1825.6777, 22.0848 >> 
FLOAT 	fMoveVehicleHeading02 = 68.2288

VECTOR vPlayerCheckpointPosNearHooker = << -556.8734, -1822.5284, 22.2149 >>
FLOAT fPlayerCheckpointHeadNearHooker = 77.9792

FLOAT fJunkyardRadius = 35.0

TEXT_LABEL_15 tlTargetWaypoint
TEXT_LABEL_15 tlDummyWaypoint

SCENARIO_BLOCKING_INDEX sbi01

structTimer vehicleTimer
structTimer panicTimer
structTimer printTimer
structTimer tMusicCueTimer
//structTimer tTextTimer
structTimer tLookAtTimer
structTimer tTooCloseTimer
structTimer tBlockTimer
structTimer	tBlockTimerInterval
structTimer tCellPhoneBufferTimer
structTimer tFirstCarTimer
structTimer tInSpawnAreaTimer
structTimer tLeaveAreaCopTimer

REL_GROUP_HASH relTarget

CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

SIMPLE_USE_CONTEXT cucEndScreenCore

MODEL_NAMES mnFirstCarModel = PRIMO
MODEL_NAMES mnFirstCarPedModel = A_M_M_SOUCENT_04

LoadQueueLarge sLoadQueue

#IF NOT IS_JAPANESE_BUILD
	//BOOL bCheckForSexPhoto
	BOOL bSexDialogueFinished
	BOOL bSexInCarFinished
	BOOL bPlayedCreeperLine = FALSE
	INT iSexStage
	structTimer dialogueTimer
	structTimer sexTimer
#ENDIF


STRUCT AMBIENT_PEDS_IN_SCENE
	//ambient ped variables
	PED_INDEX piPimp
	PED_INDEX piHooker[4]
	PED_INDEX piDealer
ENDSTRUCT
AMBIENT_PEDS_IN_SCENE	ambVice


#IF IS_DEBUG_BUILD
	DEBUG_POS_DATA myDebugData
	BOOL bDoingPSkip = FALSE
	BOOL bDoingJSkip = FALSE
	MissionStageMenuTextStruct sSkipMenu[3]
	INT iDebugJumpStage = 0              
#ENDIF
// ********************************** END SCRIPT GLOBALS*************************************

PROC SETUP_DEBUG()
	#IF IS_DEBUG_BUILD
		sSkipMenu[0].sTxtLabel = "TRAVEL_STAsGE"
		sSkipMenu[1].sTxtLabel = "MEET_GIRL"
		sSkipMenu[2].sTxtLabel = "WAIT_FOR_TARGET" 
	#ENDIF
ENDPROC


PROC timeLapseCutscene()
	IF NOT IS_REPLAY_IN_PROGRESS()
	    IF DO_TIMELAPSE(SP_MISSION_ASSASSIN_3, sTimelapse, FALSE, FALSE)
	    	curStage = MISSION_BRIEF_INIT
	    ENDIF
	ELSE
	    curStage = MISSION_BRIEF_INIT
	ENDIF
ENDPROC


//init needed data for mission
PROC INIT_VICE_DATA()
	
	vIntroCutscenePos[0] = << -558.4283, -1827.5858, 22.1976 >>
	vIntroCutscenePos[1] = << -632.94434, -1797.23608, 23.01625 >>
	
	//fill in some stuff for now
	targetData.targetEnum = A_M_M_BUSINESS_01 
	targetData.otherPedEnum = S_F_Y_Hooker_03
	targetData.auxPedEnum = A_M_Y_Genstreet_02
	targetData.vehEnum = PEYOTE
	targetData.auxVehEnum = EMPEROR2
	targetData.otherVehEnum = ruiner
	targetData.vTargetPos = << -579.8, -1796.049, 22.39 >>
	targetData.fTargetHead = 352
	targetData.weapTarget = WEAPONTYPE_PISTOL
	//the hooker and car shown in the intro cutscene
	targetData.vOtherPedPos = << -595.316, -1809.967, 22.102 >>			//<< -594.3811, -1808.9480, 22.2765 >>
	targetData.fOtherPedHead = 158.3596
	targetData.vVehPos = << -588.9072, -1769.1724, 21.6539 >>
	targetData.fVehHead = 130
	targetData.vOtherVehPos = << -596.2801, -1811.0856, 22.1921 >>
	targetData.fOtherVehHead = 70
	
	//decoy car/pedthat pulls up to hooker prior to target
	targetData.vAuxVehPos = << -576.8151, -1734.9265, 22.4815 >>
	targetData.fAuxVehHead = 234

	//cutscene data
	targetData.vCutscenePos = << -594.4413, -1810.6296, 23.6320 >>
	targetData.vCutsceneHead = << 0.6454, -0.0000, 62.1310 >>
	targetData.fCutsceneDoF = 35
	
	//asset requests
	targetData.sStringTable = "ASS_hk"
	targetData.sAnimDict[0] = "ODDJOBS@ASSASSINATE@VICE@SEX"
	targetData.sAnimDict[1] = "ODDJOBS@ASSASSINATE@VICE@HOOKER"
	targetData.sAnimDict[2] = "ODDJOBS@ASSASSINATE@VICE@PARK"
	targetData.sAnimDict[3] = "ODDJOBS@ASSASSINATE@VICE@IDLE"
	targetData.sAnimDict[4] = "ODDJOBS@ASSASSINATE@VICE@DEALER"
	targetData.sAnimDict[5] = "amb@world_human_prostitute@hooker@idle_a"
//	targetData.sAnimDict[6] = "amb@world_human_smoking@male@male_a@base"
//	targetData.sAnimDict[7] = "mini@prostitutes"
	
//	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.targetEnum)
//	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.otherPedEnum)
//	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.auxPedEnum)
//	ADD_STREAMED_MODEL(targetData.streamedModels, S_F_Y_HOOKER_02)
//	ADD_STREAMED_MODEL(targetData.streamedModels, mnFirstCarPedModel)
	
	ADD_STREAMED_MODEL(targetData.streamedModels, mnFirstCarModel)
	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.vehEnum)
	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.otherVehEnum)
	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.auxVehEnum)

	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[0])
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[1])
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[2])
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[3])
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[4])
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[5])
//	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[6])
//	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[6])
//	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[7])
	
	targetData.OBJECTIVES[0] = "ASS_HK_WAIT"
	
	targetData.vWarpPos = << -556.8734, -1822.5284, 22.2149 >>
	targetData.myType = MISSION_NAME_HOOKER
	targetData.bDriving = TRUE
	targetData.bNonTemp = TRUE
	targetData.bLeaveVehicles = TRUE
	
	aggroArgs.fAimRange = 28.0
	
	SET_PED_MODEL_IS_SUPPRESSED(targetData.targetEnum, TRUE)
	SET_ROADS_IN_ANGLED_AREA(<< -706.106, -1708.865, -100 >>, << -512.576, -1842.173, 100 >>, 100, FALSE, FALSE)
ENDPROC

FUNC BOOL ARE_MODELS_LOADED_FOR_VICE_LOT()
	BOOL bLoaded
	REQUEST_MODEL(targetData.otherPedEnum)
	REQUEST_MODEL(targetData.auxPedEnum)
	REQUEST_MODEL(S_F_Y_HOOKER_02)
	REQUEST_MODEL(mnFirstCarPedModel)
	
	IF HAS_MODEL_LOADED(targetData.otherPedEnum)
	AND HAS_MODEL_LOADED(targetData.auxPedEnum)
	AND HAS_MODEL_LOADED(S_F_Y_HOOKER_02)
	AND HAS_MODEL_LOADED(mnFirstCarPedModel)
		PRINTLN("MODELS_LOADED_FOR_VICE_LOT")
		bLoaded = TRUE
	ELSE
		bLoaded = FALSE
	ENDIF
	
	RETURN bLoaded
ENDFUNC


FUNC BOOL ARE_MODELS_LOADED_FOR_FINAL_CARS()
	BOOL bLoaded
	
	REQUEST_MODEL(targetData.targetEnum)
	
	IF HAS_MODEL_LOADED(targetData.targetEnum)
		PRINTLN("MODELS_LOADED_FOR_FINAL_CARS")
		bLoaded = TRUE
	ELSE
		bLoaded = FALSE
	ENDIF
	
	RETURN bLoaded
ENDFUNC


PROC RELEASE_ALL_AMBIENT_PED_MODELS_FOR_VICE_LOT(AMBIENT_PEDS_IN_SCENE& ambData)
	SET_MODEL_AS_NO_LONGER_NEEDED(targetData.auxPedEnum)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_F_Y_HOOKER_02)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnFirstCarPedModel)
	
	//create ambient life in Vice Assassination
	//pimp arguing with hooker
	IF DOES_ENTITY_EXIST(ambData.piPimp)
		SET_PED_AS_NO_LONGER_NEEDED(ambData.piPimp)
	ENDIF
	
	IF DOES_ENTITY_EXIST(ambData.piHooker[2])
		SET_PED_AS_NO_LONGER_NEEDED(ambData.piHooker[2])
	ENDIF
	
//	peds near the parked van
	IF DOES_ENTITY_EXIST(ambData.piDealer)
		SET_PED_AS_NO_LONGER_NEEDED(ambData.piDealer)
	ENDIF
	
	IF DOES_ENTITY_EXIST(ambData.piHooker[3])
		SET_PED_AS_NO_LONGER_NEEDED(ambData.piHooker[3])
	ENDIF
	
//	hookers on the sidewalk
	IF DOES_ENTITY_EXIST(ambData.piHooker[0])
		SET_PED_AS_NO_LONGER_NEEDED(ambData.piHooker[0])
	ENDIF
	IF NOT DOES_ENTITY_EXIST(ambData.piHooker[1])
		SET_PED_AS_NO_LONGER_NEEDED(ambData.piHooker[1])
	ENDIF
ENDPROC


//all peds positioned in scne around hooker (area used in intro cutscene)
PROC CREATE_AMBIENT_PEDS_FOR_HOOKER(ASS_ARGS& args, AMBIENT_PEDS_IN_SCENE& ambData)
	//create ambient life in Vice Assassination
	//pimp arguing with hooker
	IF NOT DOES_ENTITY_EXIST(ambData.piPimp)
		ambData.piPimp = CREATE_PED(PEDTYPE_CIVMALE, targetData.auxPedEnum, << -586.5785, -1802.6594, 22.0327 >>, 355.9268)
		SET_PED_COMBAT_ATTRIBUTES(ambData.piPimp, CA_ALWAYS_FLEE, TRUE)
		TASK_PLAY_ANIM(ambData.piPimp, "ODDJOBS@ASSASSINATE@VICE@HOOKER", "argue_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		ADD_PED_FOR_DIALOGUE(args.assConv, 8, ambData.piPimp, "OJAhkPIMP")
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piPimp, TRUE)
		SET_PED_CAN_BE_TARGETTED(ambData.piPimp, FALSE)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(ambData.piHooker[2])
		ambData.piHooker[2] = CREATE_PED(PEDTYPE_CIVFEMALE, S_F_Y_Hooker_02, << -586.5862, -1801.6840, 22.0134 >>, 197.3808)
		SET_PED_COMBAT_ATTRIBUTES(ambData.piHooker[2], CA_ALWAYS_FLEE, TRUE)
		TASK_PLAY_ANIM(ambData.piHooker[2], "ODDJOBS@ASSASSINATE@VICE@HOOKER", "argue_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piHooker[2], TRUE)
		SET_PED_CAN_BE_TARGETTED(ambData.piHooker[2], FALSE)
	ENDIF
	
//	peds near the parked van
	IF NOT DOES_ENTITY_EXIST(ambData.piDealer)
		ambData.piDealer = CREATE_PED(PEDTYPE_CIVMALE, targetData.auxPedEnum, <<-602.4943, -1792.1892, 22.5209>>, 212.3040)
		SET_PED_COMBAT_ATTRIBUTES(ambData.piDealer, CA_ALWAYS_FLEE, TRUE)
		TASK_PLAY_ANIM(ambData.piDealer, "ODDJOBS@ASSASSINATE@VICE@DEALER", "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		ADD_PED_FOR_DIALOGUE(args.assConv, 7, ambData.piDealer, "OJAhkDealer")
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piDealer, TRUE)
		SET_PED_CAN_BE_TARGETTED(ambData.piDealer, FALSE)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(ambData.piHooker[3])
		ambData.piHooker[3] = CREATE_PED(PEDTYPE_CIVFEMALE, S_F_Y_Hooker_02, <<-602.1377, -1793.4839, 22.4829>>, 14.4277)
		SET_PED_COMBAT_ATTRIBUTES(ambData.piHooker[3], CA_ALWAYS_FLEE, TRUE)
		TASK_PLAY_ANIM(ambData.piHooker[3], "ODDJOBS@ASSASSINATE@VICE@HOOKER", "hooker_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piHooker[3], TRUE)
		SET_PED_CAN_BE_TARGETTED(ambData.piHooker[3], FALSE)
	ENDIF
	
//	hookers on the sidewalk
	IF NOT DOES_ENTITY_EXIST(ambData.piHooker[0])
		ambData.piHooker[0] = CREATE_PED(PEDTYPE_CIVFEMALE, S_F_Y_Hooker_02, << -618.6262, -1795.1394, 22.7487>>, 157.3886)
		SET_PED_COMBAT_ATTRIBUTES(ambData.piHooker[0], CA_ALWAYS_FLEE, TRUE)
		TASK_PLAY_ANIM(ambData.piHooker[0], "ODDJOBS@ASSASSINATE@VICE@HOOKER", "hooker_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		ADD_PED_FOR_DIALOGUE(args.assConv, 6, ambData.piHooker[0], "OJAhkHOOKER2")
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piHooker[0], TRUE)
		SET_PED_CAN_BE_TARGETTED(ambData.piHooker[0], FALSE)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(ambData.piHooker[1])
		ambData.piHooker[1] = CREATE_PED(PEDTYPE_CIVFEMALE, S_F_Y_Hooker_02, << -620.7957, -1794.6422, 22.7487 >>, 213.7140)
		SET_PED_COMBAT_ATTRIBUTES(ambData.piHooker[1], CA_ALWAYS_FLEE, TRUE)
		TASK_PLAY_ANIM(ambData.piHooker[1], "ODDJOBS@ASSASSINATE@VICE@HOOKER", "hooker_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piHooker[1], TRUE)
		SET_PED_CAN_BE_TARGETTED(ambData.piHooker[1], FALSE)
	ENDIF
	
//	car in parking lot
	IF NOT DOES_ENTITY_EXIST(viparkedCar)
		viparkedCar = CREATE_VEHICLE(ruiner, << -598.1845, -1790.4774, 22.6120 >>, 308.4139)
		SET_MODEL_AS_NO_LONGER_NEEDED(ruiner)
	ENDIF
ENDPROC

PROC DELETE_AMBIENT_PEDS_FOR_HOOKER(AMBIENT_PEDS_IN_SCENE& ambData)
	IF DOES_ENTITY_EXIST(ambData.piPimp)
		DELETE_PED(ambData.piPimp)
	ENDIF
	IF DOES_ENTITY_EXIST(ambData.piHooker[0])
		DELETE_PED(ambData.piHooker[0])
	ENDIF
	IF DOES_ENTITY_EXIST(ambData.piHooker[1])
		DELETE_PED(ambData.piHooker[1])
	ENDIF
	IF DOES_ENTITY_EXIST(ambData.piHooker[2])
		DELETE_PED(ambData.piHooker[2])
	ENDIF
	IF DOES_ENTITY_EXIST(ambData.piDealer)
		DELETE_PED(ambData.piDealer)
	ENDIF
	IF DOES_ENTITY_EXIST(ambData.piHooker[3])
		DELETE_PED(ambData.piHooker[3])
	ENDIF
	IF DOES_ENTITY_EXIST(ambData.piHooker[3])
		DELETE_PED(ambData.piHooker[3])
	ENDIF
	IF DOES_ENTITY_EXIST(viParkedCar)
		DELETE_VEHICLE(viParkedCar)
	ENDIF
	//area near where the car in the parking lot is created
	CLEAR_AREA(<< -598.1845, -1790.4774, 22.6120 >>, 10, TRUE)
ENDPROC



FUNC BOOL CHECK_FOR_TARGET_SPOTTING_PLAYER_WHILE_PARKED(ASS_ARGS& args)
	IF NOT IS_PED_INJURED(args.myTarget)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-460.899048,-1728.564941,17.262728>>, <<-472.210144,-1717.901123,23.657265>>, 12.000000)
		OR ( IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), args.myTarget) < 15.0 ) 
			PRINTLN("TARGET SPOTTED PLAYER WHILE PARKED!")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_FOR_TARGET_SPOTTING_PLAYER(ASS_ARGS& args)
	IF NOT IS_PED_INJURED(args.myTarget)
		IF GET_DISTANCE_BETWEEN_ENTITIES(args.myTarget, PLAYER_PED_ID(), FALSE) < 7
			IF IS_PED_FACING_PED(args.myTarget, PLAYER_PED_ID(), 160)
				PRINTLN("TARGET FACING PLAYER!")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



#IF NOT IS_JAPANESE_BUILD
	//state machine for dialogue as target and the hooker are playing sex animations
	PROC MANAGE_SEX_DIALOGUE(ASS_ARGS& args)
		SWITCH iSpeechStage
			CASE 0
				IF TIMERB() >= 6000
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						IF CAN_ASSASSIN_SPEECH_PLAY(args.myTarget)
							CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_SEX", CONV_PRIORITY_HIGH)
						ENDIF
						iSpeechStage++
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF TIMERB() >= 14000
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						IF CAN_ASSASSIN_SPEECH_PLAY(args.myTarget)
							CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_SEX2", CONV_PRIORITY_HIGH)
						ENDIF
						iSpeechStage++
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF TIMERB() >= 21000
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						IF CAN_ASSASSIN_SPEECH_PLAY(args.myTarget)
							CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_SEX3", CONV_PRIORITY_HIGH)
						ENDIF
						iSpeechStage++
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF TIMERB() >= 25000
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						IF CAN_ASSASSIN_SPEECH_PLAY(args.myTarget)
							CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_SEX4", CONV_PRIORITY_HIGH)
						ENDIF
						iSpeechStage++
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF TIMERB() >= 32000
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						IF CAN_ASSASSIN_SPEECH_PLAY(args.myTarget)
							CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_SEX5", CONV_PRIORITY_HIGH)
						ENDIF
						iSpeechStage++
					ENDIF
				ENDIF
			BREAK
			CASE 5
				IF TIMERB() >= 37000
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						IF CAN_ASSASSIN_SPEECH_PLAY(args.myTarget)
							CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_SEX6", CONV_PRIORITY_HIGH)
						ENDIF
						iSpeechStage++
					ENDIF
				ENDIF
			BREAK
			CASE 6
				IF TIMERB() >= 40000
//					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						bSexDialogueFinished = TRUE
//					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDPROC

	
	//check this before playing the car sex anims in the Vice Assassination
	FUNC BOOL ARE_BOTH_PEDS_IN_A_CAR_TOGETHER(PED_INDEX pedFemale, PED_INDEX pedMale)
		VEHICLE_INDEX vehTemp
		IF NOT IS_PED_INJURED(pedFemale)
			IF IS_PED_IN_ANY_VEHICLE(pedFemale)
				vehTemp = GET_VEHICLE_PED_IS_IN(pedFemale)
				IF IS_VEHICLE_DRIVEABLE(vehTemp)
					IF NOT IS_PED_INJURED(pedMale)
						IF IS_PED_IN_VEHICLE(pedMale, vehTemp)
							IF GET_PED_IN_VEHICLE_SEAT(vehTemp) = pedMale
							AND GET_PED_IN_VEHICLE_SEAT(vehTemp, VS_FRONT_RIGHT) = pedFemale
								RETURN TRUE
					 		ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDFUNC
	
	
	
	//make the car bounce when the car sex anims are playing
	PROC BOUNCE_THE_CAR(PED_INDEX pedFemale, PED_INDEX pedMale)
		IF NOT IS_PED_INJURED(pedFemale)
		    IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(pedMale))
				IF IS_ENTITY_PLAYING_ANIM(pedFemale, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_f")
					IF IS_ENTITY_PLAYING_ANIM(pedFemale, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_f")
						IF NOT IS_TIMER_STARTED(sexTimer)
							RESTART_TIMER_NOW(sexTimer)
						ENDIF
						
						//don't start bouncing the car immediately
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedFemale, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_f") > 0.1
							IF TIMER_DO_WHEN_READY(sexTimer, GET_RANDOM_FLOAT_IN_RANGE(0.5, 0.7))
								RESTART_TIMER_NOW(sexTimer)
								
								//apply force to the car to make it bounce
								APPLY_FORCE_TO_ENTITY(GET_VEHICLE_PED_IS_IN(pedMale), APPLY_TYPE_IMPULSE, <<0.0, 0.0, 0.1>>, <<-0.0067, -0.8283, -0.2766>>, 0, TRUE, TRUE, TRUE, TRUE) 
							ENDIF
						ENDIF
					ENDIF
				ENDIF
		    ENDIF
		ENDIF
	ENDPROC
	
	
	//control the different stages of the car sex anims in the Vice Assassination
	PROC SEX_IN_CAR(PED_INDEX pedFemale, PED_INDEX pedMale, ASS_ARGS& args)
	
		IF NOT IS_ENTITY_DEAD(pedFemale) AND NOT IS_ENTITY_DEAD(pedMale)
			IF GET_SCRIPT_TASK_STATUS(pedFemale, SCRIPT_TASK_LOOK_AT_ENTITY) <> PERFORMING_TASK
				TASK_LOOK_AT_ENTITY(pedFemale, pedMale, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
	//			PRINTLN("TASKING GUY AND GIRL TO LOOK AT EACH OTHER - 01")
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD(pedFemale) AND NOT IS_ENTITY_DEAD(pedMale)
			IF GET_SCRIPT_TASK_STATUS(pedMale, SCRIPT_TASK_LOOK_AT_ENTITY) <> PERFORMING_TASK
				TASK_LOOK_AT_ENTITY(pedMale, pedFemale, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
	//			PRINTLN("TASKING GUY AND GIRL TO LOOK AT EACH OTHER - 02")
			ENDIF
		ENDIF
		
		SWITCH iSexStage
			CASE 0
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				OR TIMERA() > 20000
					IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
						SET_VEHICLE_RADIO_LOUD(args.myVehicle, TRUE)
					ENDIF
					TASK_PLAY_ANIM(pedMale, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_intro_m", SLOW_BLEND_IN, SLOW_BLEND_OUT)
					TASK_PLAY_ANIM(pedFemale, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_intro_f", SLOW_BLEND_IN, SLOW_BLEND_OUT)
	//				bCheckForSexPhoto = TRUE
					
					iSexStage++
				ELSE
					IF NOT bTargetSpottedPlayerInScrapyard
						IF CHECK_FOR_TARGET_SPOTTING_PLAYER_WHILE_PARKED(args)
							bTargetSpottedPlayerInScrapyard = TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF ARE_BOTH_PEDS_IN_A_CAR_TOGETHER(pedFemale, pedMale)
					IF IS_ENTITY_PLAYING_ANIM(pedMale, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_intro_m")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedMale, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_intro_m") > 0.9
							TASK_PLAY_ANIM(pedMale, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_m", SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
							TASK_PLAY_ANIM(pedFemale, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_f", SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
							SETTIMERB(0)
							iSexStage++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF ARE_BOTH_PEDS_IN_A_CAR_TOGETHER(pedFemale, pedMale)
					BOUNCE_THE_CAR(pedFemale, pedMale)
					IF NOT bSexDialogueFinished
						MANAGE_SEX_DIALOGUE(args)
					ELSE
						IF IS_ENTITY_PLAYING_ANIM(pedMale, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_m")
							IF GET_ENTITY_ANIM_CURRENT_TIME(pedMale, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_m") > 0.9
								TASK_PLAY_ANIM(pedMale, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_m", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1)
								TASK_PLAY_ANIM(pedFemale, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_f", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1)
								iSexStage++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF ARE_BOTH_PEDS_IN_A_CAR_TOGETHER(pedFemale, pedMale)
					IF ( IS_ENTITY_PLAYING_ANIM(pedMale, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_m")
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedMale, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_m") > 0.2 )
							//one last shake to the car
							APPLY_FORCE_TO_ENTITY(GET_VEHICLE_PED_IS_IN(pedMale), APPLY_TYPE_IMPULSE,  <<0.0, 0.0, 0.5>>, <<-0.4, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)  
							iSexStage++
					ELIF HAS_ENTITY_ANIM_FINISHED(pedMale, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_m")
						//failsafe to make sure that the stage advances
						iSexStage++
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF NOT IS_ENTITY_PLAYING_ANIM(pedMale, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_m")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CAN_ASSASSIN_SPEECH_PLAY(args.myTarget)
							//target tells the hooker he isn't gonna pay her and threatens her
							PLAY_SINGLE_LINE_FROM_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_LEAVE", "OJAShk_LEAVE_1", CONV_PRIORITY_HIGH)		
						ENDIF
						
						//have target and hooker look at each other while talking
						IF NOT IS_PED_INJURED(args.myTarget)
							IF NOT IS_PED_INJURED(args.myOtherPed)
								TASK_PLAY_ANIM(args.myTarget, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_base_m", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
								TASK_PLAY_ANIM(args.myOtherPed, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_base_f", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
							ENDIF
						ENDIF
						SETTIMERB(0)
						iSexStage++
					ENDIF
				ENDIF
			BREAK
			CASE 5
				//wait a few seconds to advance after target tells the hooker to leave
				IF TIMERB() > 5000
					bSexInCarFinished = TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDPROC


	//check to see if the player is near the target when playing sex anims and handle the panic response
	FUNC BOOL IS_PLAYER_NEAR_BOUNCING_CAR(PED_INDEX pedToCheck, structPedsForConversation& dialogStruct, structTimer& timerToUse, BOOL bCheckingGuy = TRUE)	
		IF NOT IS_PED_INJURED(pedToCheck)
			IF IS_ENTITY_PLAYING_ANIM(pedToCheck, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_m")
			OR IS_ENTITY_PLAYING_ANIM(pedToCheck, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_f")
				IF NOT IS_TIMER_STARTED(timerToUse)
					IF GET_PLAYER_DISTANCE_FROM_ENTITY(pedToCheck) < 10
						IF ( bCheckingGuy AND IS_PED_FACING_PED(pedToCheck, PLAYER_PED_ID(), 160) )
						OR ( bCheckingGuy = FALSE AND NOT IS_PED_FACING_PED(pedToCheck, PLAYER_PED_ID(), 160) )	//hack - anim makes her face the player, but she plays the anim from the seat facing forwards so we need to inverse the logic
							START_TIMER_NOW(timerToUse)
							PRINTLN("STARTING BOUNCING CAR TIMER NOW")
						ENDIF
					ENDIF
				ELSE
					IF NOT bPlayedCreeperLine
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(pedToCheck) < 10
							IF bCheckingGuy
								IF IS_PED_FACING_PED(pedToCheck, PLAYER_PED_ID(), 160)
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										KILL_ANY_CONVERSATION()
									ELSE
										CREATE_CONVERSATION(dialogStruct, "OJASAUD", "OJAShk_LOOK", CONV_PRIORITY_VERY_HIGH)
										
										PRINTLN("PLAYED CREEPER LINE FROM GUY")
										bPlayedCreeperLine = TRUE
										START_TIMER_NOW(timerToUse)
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_PED_FACING_PED(pedToCheck, PLAYER_PED_ID(), 160)	//hack - anim makes her face the player, but she plays the anim from the seat facing forwards so we need to inverse the logic
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										KILL_ANY_CONVERSATION()
									ELSE
										CREATE_CONVERSATION(dialogStruct, "OJASAUD", "OJAShk_LOOK1", CONV_PRIORITY_VERY_HIGH)
										
										PRINTLN("PLAYED CREEPER LINE FROM GIRL")
										bPlayedCreeperLine = TRUE
										START_TIMER_NOW(timerToUse)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF GET_TIMER_IN_SECONDS(timerToUse) > 10
							IF GET_PLAYER_DISTANCE_FROM_ENTITY(pedToCheck) < 10
								IF ( bCheckingGuy AND IS_PED_FACING_PED(pedToCheck, PLAYER_PED_ID(), 160) )		//note - these checks seem to be terrible - may need to just switch to angled area checks instead of ped_facing
								OR ( bCheckingGuy = FALSE AND NOT IS_PED_FACING_PED(pedToCheck, PLAYER_PED_ID(), 160) )	//hack - anim makes her face the player, but she plays the anim from the seat facing forwards so we need to inverse the logic
									KILL_ANY_CONVERSATION()
									
									PRINTLN("bTooCloseToBouncingCar = TRUE !!!")
									bTooCloseToBouncingCar = TRUE
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF					
				ENDIF
				
//				IF bCheckingGuy
//					IF IS_PED_FACING_PED(pedToCheck, PLAYER_PED_ID(), 160)
//						PRINTLN("GUY FACING PLAYER")
//					ENDIF
//				ELSE
//					IF IS_PED_FACING_PED(pedToCheck, PLAYER_PED_ID(), 160)
//						PRINTLN("GIRL FACING PLAYER")
//					ENDIF
//				ENDIF
				
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(pedToCheck) < 10
					IF GET_JACK_TARGET(PLAYER_PED_ID()) = pedToCheck
					OR IS_PED_JACKING(PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDFUNC
	
	
	//PROC AWARD_BONUS_FOR_SUCCESSFUL_PHOTO(ASS_ARGS& args)
	//	IF passReasonVice <> PASS_BONUS
	//		IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
	//			IF CELL_CAM_IS_CHAR_VISIBLE_NO_FACE_CHECK(args.myTarget)
	//			AND CELL_CAM_IS_CHAR_VISIBLE_NO_FACE_CHECK(args.myOtherPed)
	//				PRINT_HELP("ASS_HK_PHOTO")
	//				passReasonCore = PASS_BONUS
	//	//		ELSE	//print a help message explaining to get a better photo
	//	//			IF CELL_CAM_IS_CHAR_VISIBLE_NO_FACE_CHECK(args.myTarget)
	//	//			AND CELL_CAM_IS_CHAR_VISIBLE_NO_FACE_CHECK(args.myOtherPed)
	//	//				PRINT_HELP("ASS_HK_CAMHELP")
	//	//			ENDIF
	//			ENDIF
	//		ENDIF
	//	ENDIF
	//ENDPROC

#ENDIF


//dialogue used by the ambient peds in the scene when approached by the player
PROC MANAGE_AMBIENT_VICE_DIALOGUE(ASS_ARGS& args, AMBIENT_PEDS_IN_SCENE& ambData)
	//only update this before the target flees
	IF targetStage <> MISSION_VICE_TARGET_ESCAPE
	AND targetStage <> MISSION_VICE_TARGET_COMBAT
		IF DOES_ENTITY_EXIST(ambData.piPimp)
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(ambData.piPimp) < 5
				IF NOT bPimpDialogue
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
						IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_PIMP", CONV_PRIORITY_HIGH)
							SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(ambData.piPimp, TRUE)
							bPimpDialogue = TRUE
						ENDIF
					ENDIF
				ELSE
					KILL_CURRENT_AMBIENT_MISSION_DIALOGUE_IF_TOUCHED(ambData.piPimp)
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(ambData.piHooker[0])
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(ambData.piHooker[0]) < 5
				IF NOT bHookDialogue
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
						IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_PROST", CONV_PRIORITY_HIGH)
							PRINTLN("PLAYING LINE 1")
							SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(ambData.piHooker[0], TRUE)
							bHookDialogue = TRUE
						ENDIF
					ENDIF
				ELSE
					KILL_CURRENT_AMBIENT_MISSION_DIALOGUE_IF_TOUCHED(ambData.piHooker[0])
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(ambData.piDealer)
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(ambData.piDealer) < 5
				IF NOT bDrugDialogue
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
						IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_DRUG", CONV_PRIORITY_HIGH)
							SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(ambData.piDealer, TRUE)
							bDrugDialogue = TRUE
						ENDIF
					ENDIF
				ELSE
					KILL_CURRENT_AMBIENT_MISSION_DIALOGUE_IF_TOUCHED(ambData.piDealer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//set all vice peds to react to non-temp events and play reaction anims
PROC SET_AMBIENT_VICE_PEDS_TO_REACT_TO_NON_TEMP_EVENTS(AMBIENT_PEDS_IN_SCENE& ambData)
	//Have ambient life flee
	IF NOT IS_PED_INJURED(ambData.piPimp)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piPimp, FALSE)
	ENDIF
	IF NOT IS_PED_INJURED(ambData.piHooker[2])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piHooker[2], FALSE)
	ENDIF
	IF NOT IS_PED_INJURED(ambData.piDealer)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piDealer, FALSE)
	ENDIF
	IF NOT IS_PED_INJURED(ambData.piHooker[3])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piHooker[3], FALSE)
	ENDIF
	IF NOT IS_PED_INJURED(ambData.piHooker[0])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piHooker[0], FALSE)
	ENDIF
	IF NOT IS_PED_INJURED(ambData.piHooker[1])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambData.piHooker[1], FALSE)
	ENDIF
ENDPROC


FUNC BOOL IS_PLAYER_SEEN_BY_HOOKER(ASS_ARGS& args)
	IF NOT IS_PED_INJURED(args.myOtherPed)
		IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myOtherPed) < 10
		AND IS_PED_FACING_PED(args.myOtherPed, PLAYER_PED_ID(), 120)
		OR GET_PLAYER_DISTANCE_FROM_ENTITY(args.myOtherPed) < 6
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//manage the hooker's dialogue to the player - tell him to get lost if he is hanging out next to her screwing up her business
PROC MANAGE_HOOKER_DIALOGUE(ASS_ARGS& args)
	SEQUENCE_INDEX tempSeq
	SWITCH iSpeechStage
		CASE 0
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myOtherPed) < 7
				SETTIMERB(0)
				iSpeechStage++
			ENDIF	
		BREAK
		//face the player and tell him to leave
		CASE 1
			IF TIMERB() >= 1500
				IF IS_PLAYER_SEEN_BY_HOOKER(args)
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						CLEAR_PED_TASKS(args.myOtherPed)
						CLEAR_SEQUENCE_TASK(tempSeq)
						OPEN_SEQUENCE_TASK(tempSeq)
							IF NOT IS_PED_FACING_PED(args.myOtherPed, PLAYER_PED_ID(), 120)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
							ENDIF
							TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_outro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
							TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_reject", SLOW_BLEND_IN, SLOW_BLEND_OUT)
							TASK_PLAY_ANIM(NULL, sHookerAnimDict, "reject_outro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
							TASK_ACHIEVE_HEADING(NULL, targetData.fOtherPedHead)
							TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_wait", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
						CLOSE_SEQUENCE_TASK(tempSeq)
						TASK_PERFORM_SEQUENCE(args.myOtherPed, tempSeq)
						CLEAR_SEQUENCE_TASK(tempSeq)
						
						TASK_LOOK_AT_ENTITY(args.myOtherPed, PLAYER_PED_ID(), 5000)
						
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_TALK", CONV_PRIORITY_VERY_HIGH)
						
						SETTIMERB(0)
						iSpeechStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_PED_INJURED(args.myOtherPed)
				IF TIMERB() >= DEFAULT_GOD_TEXT_TIME
//					CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_TALK", CONV_PRIORITY_VERY_HIGH)
					iSpeechStage++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF TIMERB() >= 9000
				IF IS_PLAYER_SEEN_BY_HOOKER(args)
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						CLEAR_PED_TASKS(args.myOtherPed)
						CLEAR_SEQUENCE_TASK(tempSeq)
						OPEN_SEQUENCE_TASK(tempSeq)
							IF NOT IS_PED_FACING_PED(args.myOtherPed, PLAYER_PED_ID(), 120)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
							ENDIF
							TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_outro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
							TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_reject", SLOW_BLEND_IN, SLOW_BLEND_OUT)
							TASK_PLAY_ANIM(NULL, sHookerAnimDict, "reject_outro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
							TASK_ACHIEVE_HEADING(NULL, targetData.fOtherPedHead)
							TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_wait", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
						CLOSE_SEQUENCE_TASK(tempSeq)
						TASK_PERFORM_SEQUENCE(args.myOtherPed, tempSeq)
						CLEAR_SEQUENCE_TASK(tempSeq)
						SETTIMERB(0)
						iSpeechStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_PED_INJURED(args.myOtherPed)
				IF IS_ENTITY_PLAYING_ANIM(args.myOtherPed, sHookerAnimDict, "idle_reject")
				OR TIMERB() >= DEFAULT_GOD_TEXT_TIME
					CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_TALK2", CONV_PRIORITY_VERY_HIGH)
					iSpeechStage++
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF TIMERB() >= DEFAULT_GOD_TEXT_TIME
				IF IS_PLAYER_SEEN_BY_HOOKER(args)
					CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_TALK3", CONV_PRIORITY_VERY_HIGH)
					SETTIMERB(0)
					iSpeechStage++
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF TIMERB() >= 4000
				IF targetStage < MISSION_VICE_ENTER_CAR
					SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
					PRINTLN("HOOKER: FAILING - PLAYER HARASSING THE GIRL")
				ELSE
					bPlayerBlockingDeal = TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC



//determine the order that the cars will approach the hooker based on the number of mission attempts by the player (alternate order each time)
PROC POSITION_VICE_VEHICLES_FROM_SAVEDATA(ASS_ARGS& args)
	IF IS_VEHICLE_DRIVEABLE(args.myAuxVehicle)
		IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
			IF NOT IS_PED_INJURED(args.myAuxPed)
				IF NOT IS_PED_INJURED(args.myOtherPed)
					IF g_savedGlobals.sAssassinData.iAttemptsOnCur % 2 = 0
						//target car in back
						vTargetStartPos = << -576.8151, -1734.9265, 22.4815 >>
						vDummyStartPos = << -524.8151, -1771.9265, 20.3315 >>
					ELSE
						//target car in front
						vTargetStartPos = << -524.8151, -1771.9265, 20.3315 >>
						vDummyStartPos = << -576.8151, -1734.9265, 22.4815 >>
					ENDIF
					
					SET_PED_COORDS_KEEP_VEHICLE(args.myTarget, vTargetStartPos)
					SET_ENTITY_HEADING(args.myVehicle, 234)
					SET_VEHICLE_ON_GROUND_PROPERLY(args.myVehicle)
					SET_VEHICLE_RADIO_ENABLED(args.myVehicle, TRUE)
//					SET_RADIO_TO_STATION_NAME("RADIO_02_POP")
					SET_VEHICLE_RADIO_LOUD(args.myVehicle, TRUE)
					
					SET_PED_COORDS_KEEP_VEHICLE(args.myAuxPed, vDummyStartPos)
					SET_ENTITY_HEADING(args.myAuxVehicle, 234)
					SET_VEHICLE_ON_GROUND_PROPERLY(args.myAuxVehicle)
					SET_VEHICLE_RADIO_ENABLED(args.myAuxVehicle, TRUE)
//					SET_RADIO_TO_STATION_NAME("RADIO_01_CLASS_ROCK")
					SET_VEHICLE_RADIO_LOUD(args.myVehicle, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Checks to see if the players last vehicle is parked in front of the hooker blocking other vehicles from driving up to her
/// RETURNS:
///    TRUE if there is a vehicle detected in teh anged area around the hooker
FUNC BOOL IS_PLAYER_VEHICLE_BLOCKING_HOOKER()
	VEHICLE_INDEX vehPlayer = GET_PLAYERS_LAST_VEHICLE()
	
	IF DOES_ENTITY_EXIST(vehPlayer)
		IF IS_ENTITY_IN_ANGLED_AREA(vehPlayer, << -589.952087, -1813.227417, 22.181004 >>, << -598.982971, -1809.424072, 23.080276 >>, 6.5)
			PRINTLN("IS_PLAYER_VEHICLE_BLOCKING_HOOKER() returned TRUE!")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//send the cars to the hooker based on their setup from POSITION_VICE_VEHICLES_FROM_SAVEDATA
PROC SEND_CARS_TO_HOOKER(ASS_ARGS& args)
	IF IS_VEHICLE_DRIVEABLE(args.myAuxVehicle)
		IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
			IF NOT IS_PED_INJURED(args.myAuxPed)
				IF NOT IS_PED_INJURED(args.myOtherPed)
					IF g_savedGlobals.sAssassinData.iAttemptsOnCur % 2 = 1
						tlTargetWaypoint = "OJAShk_101"
						tlDummyWaypoint = "OJAShk_103"
					ELSE
						tlTargetWaypoint = "OJAShk_103"
						tlDummyWaypoint = "OJAShk_101"						
					ENDIF	
					
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(args.myTarget, args.myVehicle, tlTargetWaypoint, DF_SwerveAroundAllCars|DF_StopAtLights|DF_SteerAroundObjects|DF_StopForCars|DF_SteerAroundPeds|DF_SteerAroundStationaryCars|DF_DriveIntoOncomingTraffic)
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(args.myAuxPed, args.myAuxVehicle, tlDummyWaypoint, DF_SwerveAroundAllCars|DF_StopAtLights|DF_SteerAroundObjects|DF_StopForCars|DF_SteerAroundPeds|DF_SteerAroundStationaryCars|DF_DriveIntoOncomingTraffic)
					
					SET_VEHICLE_DOORS_LOCKED(args.myAuxVehicle, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					
					PRINTLN("TASKING TO FOLLOW WAYPOINTS")
					
					SET_ENTITY_LOAD_COLLISION_FLAG(args.myTarget, TRUE)
					bTestDummyCarForPanic = TRUE
				ELSE
					PRINTLN("args.myOtherPed IS INJURED")
				ENDIF
			ELSE
				PRINTLN("args.myAuxPed IS INJURED")
			ENDIF
		ELSE
			PRINTLN("args.myVehicle IS NOT DRIVEABLE")
		ENDIF
	ELSE
		PRINTLN("args.myAuxVehicle IS NOT DRIVEABLE")
	ENDIF
ENDPROC

//update function to have the dummy car pull up next to the hooker and interact with her
PROC CONTROL_DUMMY_CAR_BEHAVIOR(ASS_ARGS& args)
	SEQUENCE_INDEX tempSeq
	INT iClosestWaypoint
	
	IF NOT IS_TIMER_STARTED(vehicleTimer)
		IF IS_VEHICLE_DRIVEABLE(args.myAuxVehicle)
			IF NOT IS_PED_INJURED(args.myAuxPed)
				IF NOT IS_PED_INJURED(args.myOtherPed)
					IF WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(tlDummyWaypoint, GET_ENTITY_COORDS(args.myAuxVehicle), iClosestWaypoint)
						IF iClosestWaypoint >= 14
						//have guy stop car and look at hooker
							
							IF NOT IS_PLAYER_VEHICLE_BLOCKING_HOOKER()
							
								IF GET_SCRIPT_TASK_STATUS(args.myAuxPed,  SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(args.myAuxPed,  SCRIPT_TASK_PLAY_ANIM) <> WAITING_TO_START_TASK
									CLEAR_PED_TASKS(args.myAuxPed)
									TASK_LOOK_AT_ENTITY(args.myOtherPed, args.myAuxVehicle, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
//									TASK_PLAY_ANIM(args.myAuxPed, "ODDJOBS@ASSASSINATE@VICE@IDLE", "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
									TASK_LOOK_AT_ENTITY(args.myAuxPed, args.myOtherPed, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
									BRING_VEHICLE_TO_HALT(args.myAuxVehicle, 10, 1)
								ENDIF
								
								IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
									START_VEHICLE_HORN(args.myAuxVehicle, 750)
									
									//have hooker look at car and tell him to leave
	//								TASK_TURN_PED_TO_FACE_ENTITY(args.myOtherPed, args.myAuxPed, -1)
									
									START_TIMER_NOW(vehicleTimer)
									IF CAN_ASSASSIN_SPEECH_PLAY(args.myOtherPed)
										
										ADD_PED_FOR_DIALOGUE(args.assConv, 5, args.myAuxPed, "OJAhkCrawler2")
									
										IF NOT bPlayedCheapLine
											IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_CHEAP", CONV_PRIORITY_HIGH)
												bPlayedCheapLine = TRUE
												PRINTLN("bPlayedCheapLine = TRUE - 01")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							
							ELSE
								PRINTLN("SET_MISSION_FAILED - IN CONTROL_DUMMY_CAR_BEHAVIOR")
								SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(args.myTarget)
			IF IS_TIMER_STARTED(vehicleTimer)
				IF GET_TIMER_IN_SECONDS(vehicleTimer) > 0.5	//tell him to leave now
					IF NOT bTaskedHookerCheap
						IF NOT IS_PED_INJURED(args.myOtherPed)
							CLEAR_SEQUENCE_TASK(tempSeq)
							OPEN_SEQUENCE_TASK(tempSeq)
								IF GET_ENTITY_HEADING(args.myOtherPed) < targetData.fOtherPedHead - 30
								OR GET_ENTITY_HEADING(args.myOtherPed) > targetData.fOtherPedHead + 30
									TASK_ACHIEVE_HEADING(NULL, targetData.fOtherPedHead)		//make her face towards the car if she isnt already facing that general direction
								ENDIF
								TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_outro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
								TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_reject", SLOW_BLEND_IN, SLOW_BLEND_OUT)
								TASK_PLAY_ANIM(NULL, sHookerAnimDict, "reject_outro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
								TASK_ACHIEVE_HEADING(NULL, targetData.fOtherPedHead)
								TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_wait", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(tempSeq)
							TASK_PERFORM_SEQUENCE(args.myOtherPed, tempSeq)
							CLEAR_SEQUENCE_TASK(tempSeq)
							
							PRINTLN("bTaskedHookerCheap = TRUE")
							bTaskedHookerCheap = TRUE
						ENDIF
					ELSE
						IF GET_TIMER_IN_SECONDS(vehicleTimer) > 5.0
							IF IS_VEHICLE_DRIVEABLE(args.myAuxVehicle)
								IF NOT IS_PED_INJURED(args.myAuxPed)
									IF GET_SCRIPT_TASK_STATUS(args.myAuxPed, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(args.myAuxPed, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> WAITING_TO_START_TASK
										TASK_VEHICLE_DRIVE_WANDER(args.myAuxPed, args.myAuxVehicle, 6, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
										PRINTLN("TASKING CHEAP GUY TO WANDER")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//only test the false vehicle for panic when it is near the hooker
			IF bTestDummyCarForPanic
				IF DOES_ENTITY_EXIST(args.myAuxPed)
				AND DOES_ENTITY_EXIST(args.myOtherPed)
					IF GET_DISTANCE_BETWEEN_ENTITIES(args.myAuxPed, args.myOtherPed) > 10 
						bTestDummyCarForPanic = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//trigger panic if the player gets too close to the target's vehicle when he is stopped waiting for the hooker to enter
PROC TRIGGER_PANIC_IF_BLOCKING_DEAL(ASS_ARGS& args)
	IF NOT IS_PED_INJURED(args.myTarget)
		IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 5
			IF NOT IS_TIMER_STARTED(panicTimer)
				START_TIMER_NOW(panicTimer)
			ELIF GET_TIMER_IN_SECONDS(panicTimer) > 3
				bPlayerBlockingDeal = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CHECK_FOR_PROJECTILES(PED_INDEX pedToCheck)
	IF DOES_ENTITY_EXIST(pedToCheck)
		IF IS_PED_IN_ANY_VEHICLE(pedToCheck)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_PLAYER_PISSED_OFF_PEDS_IN_VICE_SCENE(AMBIENT_PEDS_IN_SCENE& ambData, ASS_ARGS& args, INT& iFrame, BOOL bDoWantedCheck, BOOL& bFlipThisIfTrue)
	BOOL bReturnVal
	PED_INDEX pedToCheck
	VEHICLE_INDEX vehToCheck = NULL
	
	IF iFrame = 0
		pedToCheck = args.myTarget
		vehToCheck = args.myVehicle
	ELIF iFrame = 1
		IF bTestDummyCarForPanic
			pedToCheck = args.myAuxPed
			vehToCheck = args.myAuxVehicle
		ENDIF
	ELIF iFrame = 2
		pedToCheck = args.myOtherPed
	ELIF iFrame = 3
		pedToCheck = ambData.piPimp
	ELIF iFrame = 4
		pedToCheck = ambData.piHooker[2]
	ELIF iFrame = 5
		pedToCheck = ambData.piDealer
	ELIF iFrame = 6
		pedToCheck = ambData.piHooker[3]
	ELIF iFrame = 7
		pedToCheck = ambData.piHooker[0]
	ELIF iFrame = 8
		pedToCheck = ambData.piHooker[1]
	ELIF iFrame = 9
		pedToCheck = pedFirstDriver
	ENDIF
	
	
	BOOL bCheckForMajorAttack = FALSE
	IF pedToCheck = pedFirstDriver
		bCheckForMajorAttack = TRUE
	ENDIF

	
	//do checks for peds only at certain points in the script
	INT maxFrameToCheck
	IF bHookerInCar
		maxFrameToCheck = 2		//we only care about the target and hooker now since they have parked away from the scene
	ELSE
		maxFrameToCheck = 9
	ENDIF
	
	IF maxFrameToCheck = 9
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, << -594.81824, -1792.03528, 21.93077 >>, 35.0)
			PRINTLN("RETURNING TRUE ON AGGRO CHECK - explosion near vice scene!!")
			bReturnVal = TRUE
		ENDIF
	ENDIF
	
	IF iFrame <= maxFrameToCheck
		IF DOES_ENTITY_EXIST(pedToCheck)
			IF vehToCheck = args.myVehicle AND (pedToCheck = args.myTarget)
				IF DO_AGGRO_CHECK(pedToCheck, vehToCheck, aggroArgs, aggroReason, FALSE, SHOULD_CHECK_FOR_PROJECTILES(pedToCheck), bCheckForMajorAttack)
				OR IS_VEHICLE_DRIVEABLE(args.myVehicle)	AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(args.myVehicle, PLAYER_PED_ID())
				OR GET_PED_IN_VEHICLE_SEAT(args.myVehicle) = PLAYER_PED_ID()
//					IF aggroReason = EAggro_MinorAttacked
//						bMinorAttacked = TRUE
//						PRINTLN("PLAYER AGGROED, BUT IT WAS A MINOR ATTACK")
//					ENDIF
				
					PRINTLN("RETURNING TRUE ON AGGRO CHECK - args.myTarget")
					bReturnVal = TRUE
				ENDIF
			ELIF (pedToCheck = args.myAuxPed)
				IF DO_AGGRO_CHECK(pedToCheck, vehToCheck, aggroArgs, aggroReason, FALSE, SHOULD_CHECK_FOR_PROJECTILES(pedToCheck))
				OR GET_JACK_TARGET(PLAYER_PED_ID()) = pedToCheck
				OR IS_VEHICLE_DRIVEABLE(args.myAuxVehicle)	AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(args.myAuxVehicle, PLAYER_PED_ID())
					PRINTLN("RETURNING TRUE ON AGGRO CHECK - args.myAuxPed")
					bReturnVal = TRUE
				ENDIF
			ELSE
				IF DO_AGGRO_CHECK(pedToCheck, vehToCheck, aggroArgs, aggroReason, TRUE, SHOULD_CHECK_FOR_PROJECTILES(pedToCheck))
				OR GET_JACK_TARGET(PLAYER_PED_ID()) = pedToCheck
					PRINTLN("RETURNING TRUE ON AGGRO CHECK. iFrame = ", iFrame)
					bReturnVal = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF bDoWantedCheck
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				PRINTLN("RETURNING TRUE ON AGGRO CHECK - WANTED CHECK")
				bReturnVal = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//increment it by one - reset it if it exceeds the checks
	iFrame ++
	IF iFrame > maxFrameToCheck
		iFrame = 0
	ENDIF
	
	// Check for player being inside a police car
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		VECTOR vPlayerPosition
		
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF VDIST2(vPlayerPosition, << -593.13214, -1791.83606, 21.91110 >>) < 1600 //40m
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehPlayerIsIn
				vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehPlayerIsIn) AND NOT IS_ENTITY_DEAD(vehPlayerIsIn)
					MODEL_NAMES mnTemp
					mnTemp = GET_ENTITY_MODEL(vehPlayerIsIn)
					
					IF mnTemp = POLICE 
					OR mnTemp = POLICE2
					OR mnTemp = POLICE3
					OR mnTemp = POLICE4
						PRINTLN("PLAYER IN A POLICE VEHICLE")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bReturnVal = TRUE
//		IF NOT IS_TARGET_AND_PLAYER_UNDER_FREEWAY()
//			SET_PLAYER_WANTED_LEVEL_NO_DROP_NOW()
//			PRINTLN("SETTING PLAYER'S WANTED LEVEL 01")
//		ENDIF
		
		bFlipThisIfTrue = TRUE
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//checks prior to the intro cutscene to see if the player has triggered panic on any of the script-created peds/vehicles
FUNC BOOL HAS_PANIC_TRIGGERED_IN_VICE_MISSION(ASS_ARGS& args, AMBIENT_PEDS_IN_SCENE& ambData, BOOL bDoWantedChecksForAmbientPeds)
	IF bDoWantedChecksForAmbientPeds
		IF HAS_PLAYER_PISSED_OFF_PEDS_IN_VICE_SCENE(ambData, args, iFrameCountAsn, TRUE, bTriggeredAggro)
			RETURN TRUE
		ENDIF
	ELSE
		IF HAS_PLAYER_PISSED_OFF_PEDS_IN_VICE_SCENE(ambData, args, iFrameCountAsn, FALSE, bTriggeredAggro)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


PROC SET_AMBIENT_VICE_PEDS_TO_FLEE(ASS_ARGS& args, AMBIENT_PEDS_IN_SCENE& ambData, BOOL bForceKeepTask = FALSE)
	//tell the fake target to flee
	IF NOT IS_PED_INJURED(args.myAuxPed)
		CLEAR_PED_TASKS(args.myAuxPed)
		IF IS_PED_IN_ANY_VEHICLE(args.myAuxPed)
			TASK_VEHICLE_MISSION_COORS_TARGET(args.myAuxPed, GET_VEHICLE_PED_IS_IN(args.myAuxPed), GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 15, DRIVINGMODE_AVOIDCARS_RECKLESS, 9999, -1)
		ELSE
			TASK_SMART_FLEE_PED(args.myAuxPed, PLAYER_PED_ID(), 500, -1)
		ENDIF
		SET_PED_KEEP_TASK(args.myAuxPed, TRUE)
	ENDIF
	
	//tell hooker to flee
	IF NOT IS_PED_INJURED(args.myOtherPed)
//		CLEAR_PED_TASKS(args.myOtherPed)
		TASK_SMART_FLEE_PED(args.myOtherPed, PLAYER_PED_ID(), 500, -1)
		SET_PED_KEEP_TASK(args.myOtherPed, TRUE)
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		PLAY_PAIN(args.myOtherPed, AUD_DAMAGE_REASON_SCREAM_TERROR)
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		bForceKeepTask = TRUE
	ENDIF
	
	//Have ambient life flee
	IF NOT IS_PED_INJURED(ambData.piPimp)
		IF aggroReason <> EAggro_MinorAttacked
			TASK_SMART_FLEE_PED(ambData.piPimp, PLAYER_PED_ID(), 500, -1)
		ENDIF
		
		SET_PED_KEEP_TASK(ambData.piPimp, TRUE)
	ENDIF
	IF NOT IS_PED_INJURED(ambData.piHooker[2])
		IF aggroReason <> EAggro_MinorAttacked
			TASK_SMART_FLEE_PED(ambData.piHooker[2], PLAYER_PED_ID(), 500, -1)
		ENDIF
		IF bForceKeepTask
			SET_PED_KEEP_TASK(ambData.piHooker[2], TRUE)
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(ambData.piDealer)
		IF aggroReason <> EAggro_MinorAttacked
			TASK_SMART_FLEE_PED(ambData.piDealer, PLAYER_PED_ID(), 500, -1)
		ENDIF
		IF bForceKeepTask
			SET_PED_KEEP_TASK(ambData.piDealer, TRUE)
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(ambData.piHooker[3])
		IF aggroReason <> EAggro_MinorAttacked
			TASK_SMART_FLEE_PED(ambData.piHooker[3], PLAYER_PED_ID(), 500, -1)
		ENDIF
		IF bForceKeepTask
			SET_PED_KEEP_TASK(ambData.piHooker[3], TRUE)
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(ambData.piHooker[0])
		IF aggroReason <> EAggro_MinorAttacked
			TASK_SMART_FLEE_PED(ambData.piHooker[0], PLAYER_PED_ID(), 500, -1)
		ENDIF
		IF bForceKeepTask
			SET_PED_KEEP_TASK(ambData.piHooker[0], TRUE)
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(ambData.piHooker[1])
		IF aggroReason <> EAggro_MinorAttacked
			TASK_SMART_FLEE_PED(ambData.piHooker[1], PLAYER_PED_ID(), 500, -1)
		ENDIF
		IF bForceKeepTask
			SET_PED_KEEP_TASK(ambData.piHooker[1], TRUE)
		ENDIF
	ENDIF
ENDPROC


PROC DESTROY_SEX_CAM()
	IF DOES_CAM_EXIST(camHint)
		SET_CAM_ACTIVE(camHint, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		DESTROY_CAM(camHint)
		PRINTLN("DESTROYING CAMERA")
	ENDIF
ENDPROC

//grab a camera position randomly from a preset number of hard-coded locations and render it to the screen if the circle button is pressed
PROC HANDLE_SEX_CAMERAS(ASS_ARGS& args)
	
	IF NOT IS_PED_INJURED(args.myTarget)
		IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 35
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF SHOULD_CONTROL_CHASE_HINT_CAM(localChaseHintCamStruct)
				
				IF NOT bChaseHintCamKilled
				AND IS_GAMEPLAY_HINT_ACTIVE()
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					bChaseHintCamKilled = TRUE
				ELSE
					
					IF NOT DOES_CAM_EXIST(camHint)
						camHint = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-470.1433, -1720.1132, 18.5970>>, <<-1.0803, -0.0000, -80.8725>>, 35.0)
						PRINTLN("CREATE CAMERA")
					ENDIF
		
					IF NOT IS_CAM_ACTIVE(camHint)
						SET_CAM_ACTIVE(camHint, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						DISPLAY_HUD(FALSE)
						DISPLAY_RADAR(FALSE)
						PRINTLN("TURNING CAMERA ON")
					ENDIF
				ENDIF
			ELSE
				DESTROY_SEX_CAM()
			ENDIF
		ELSE
			DESTROY_SEX_CAM()
		ENDIF
	ENDIF
ENDPROC


//switch to custom cinematic cameras if the player presses hte Circle button in a vehicle
PROC CONTROL_HINT_CAMS_IN_VICE_MISSION(ASS_ARGS& args)
	IF targetStage >= MISSION_VICE_TASK_PLAYER
	AND targetStage <= MISSION_VICE_TARGET_LEAVE
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		
		HANDLE_SEX_CAMERAS(args)
	ELSE
		//destroy the sex camera if it already exists
		DESTROY_SEX_CAM()
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF targetStage > MISSION_VICE_CHECK_CARS
				CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, args.myTarget)
			ELSE
				CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, args.myOtherPed)
			ENDIF
		ELSE
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		ENDIF
	ENDIF
ENDPROC


PROC START_HOOKER_CUTSCENE_ANIMS(ASS_ARGS& args, BOOL bPlayFirstAnim = TRUE)
	SEQUENCE_INDEX tempSeq
	
	IF targetData.myType = MISSION_NAME_HOOKER
		IF NOT IS_PED_INJURED(args.myOtherPed)
			SET_ENTITY_COORDS(args.myOtherPed, << -595.316, -1809.967, 22.102 >>)
			SET_ENTITY_HEADING(args.myOtherPed, 132.840)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(args.myOtherPed)
			
			PRINTLN("MOVING GIRL")
			
			//tell hooker to walk to position on sidewalk and play her anims
			CLEAR_SEQUENCE_TASK(tempSeq)
			
			OPEN_SEQUENCE_TASK(tempSeq)
				IF bPlayFirstAnim
					TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, 10000)
					PRINTLN("PLAYING FIRST ANIMATION")
				ENDIF
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -594.3811, -1808.9480, 22.27985 >>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT ,targetData.fOtherPedHead)
				TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_wait", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
			CLOSE_SEQUENCE_TASK(tempSeq)
			TASK_PERFORM_SEQUENCE(args.myOtherPed, tempSeq)
			CLEAR_SEQUENCE_TASK(tempSeq)
		ENDIF
	ENDIF
ENDPROC

//the dummy car and ped that pull up to the hooker
PROC CREATE_FALSE_JOHN_IN_VEHICLE(ASS_ARGS& args)	
	IF NOT DOES_ENTITY_EXIST(args.myAuxVehicle)
		args.myAuxVehicle = CREATE_VEHICLE(targetData.auxVehEnum, targetData.vAuxVehPos, targetData.fAuxVehHead)
		SET_VEHICLE_EXTRA(args.myAuxVehicle, 2, TRUE)
		REMOVE_VEHICLE_WINDOW(args.myAuxVehicle, SC_WINDOW_FRONT_RIGHT)
		REMOVE_VEHICLE_WINDOW(args.myAuxVehicle, SC_WINDOW_FRONT_LEFT)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(args.myAuxPed)
		args.myAuxPed = CREATE_PED_INSIDE_VEHICLE(args.myAuxVehicle, PEDTYPE_MISSION, targetData.auxPedEnum)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(args.myAuxPed, TRUE)
	ENDIF
ENDPROC

//PROC REMOVE_FALSE_JOHN(ASS_ARGS& args)
//	
//	IF NOT IS_ENTITY_DEAD(args.myAuxPed)
//		IF GET_SCRIPT_TASK_STATUS(args.myAuxPed, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = PERFORMING_TASK
//			IF DOES_ENTITY_EXIST(args.myAuxPed)
//				IF IS_ENTITY_OCCLUDED(args.myAuxPed)
//					SET_PED_AS_NO_LONGER_NEEDED(args.myAuxPed)
//					PRINTLN("PED IS VISIBLE: args.myAuxPed - SET AS NO LONGER NEEDED")
//				ELSE
//					DELETE_PED(args.myAuxPed)
//					PRINTLN("DELETE PED - args.myAuxPed")
//				ENDIF
//			ENDIF
//			IF DOES_ENTITY_EXIST(args.myAuxVehicle)
//				IF IS_ENTITY_OCCLUDED(args.myAuxVehicle)
//					SET_VEHICLE_AS_NO_LONGER_NEEDED(args.myAuxVehicle)
//					PRINTLN("VEHICLE IS VISIBLE: args.myAuxVehicle - SET AS NO LONGER NEEDED")
//				ELSE
//					DELETE_VEHICLE(args.myAuxVehicle)
//					PRINTLN("DELETE VEHICLE - args.myAuxVehicle")
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

FUNC BOOL HANDLE_FIRST_CAR(ASS_ARGS& args)
	INT iClosestWaypoint
	SEQUENCE_INDEX tempSeq
	
	SWITCH iFirstCarStages
		CASE 0
			IF NOT DOES_ENTITY_EXIST(viFirstCar)
				viFirstCar = CREATE_VEHICLE(mnFirstCarModel, << -524.8151, -1771.9265, 20.3315 >>, 234)
				SET_MODEL_AS_NO_LONGER_NEEDED(mnFirstCarModel)
				PRINTLN("CREATING VEHICLE - viFirstCar")
				SET_VEHICLE_ON_GROUND_PROPERLY(viFirstCar)
				SET_VEHICLE_RADIO_ENABLED(viFirstCar, TRUE)
//				SET_RADIO_TO_STATION_NAME("RADIO_01_CLASS_ROCK")
				SET_VEHICLE_RADIO_LOUD(viFirstCar, TRUE)
				
				IF DOES_ENTITY_EXIST(viFirstCar)
					pedFirstDriver = CREATE_PED_INSIDE_VEHICLE(viFirstCar, PEDTYPE_MISSION, mnFirstCarPedModel)
					SET_MODEL_AS_NO_LONGER_NEEDED(mnFirstCarPedModel)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFirstDriver, TRUE)
					
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedFirstDriver, viFirstCar, "OJAShk_103", DF_SwerveAroundAllCars|DF_StopAtLights|DF_SteerAroundObjects|DF_StopForCars|DF_SteerAroundPeds|DF_SteerAroundStationaryCars|DF_DriveIntoOncomingTraffic)
					
					SET_VEHICLE_DOORS_LOCKED(viFirstCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					
					PRINTLN("iFirstCarStages = 1")
					iFirstCarStages = 1
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------
		CASE 1
			IF NOT IS_ENTITY_DEAD(viFirstCar) AND NOT IS_ENTITY_DEAD(pedFirstDriver)
				IF WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("OJAShk_103", GET_ENTITY_COORDS(viFirstCar), iClosestWaypoint)
					IF iClosestWaypoint >= 14
						
						CLEAR_PED_TASKS(pedFirstDriver)
						BRING_VEHICLE_TO_HALT(viFirstCar, 10, 1)
						START_VEHICLE_HORN(viFirstCar, 750)
						
						IF NOT IS_ENTITY_DEAD(pedFirstDriver) AND NOT IS_ENTITY_DEAD(args.myOtherPed)
							TASK_LOOK_AT_ENTITY(pedFirstDriver, args.myOtherPed, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
							TASK_LOOK_AT_ENTITY(args.myOtherPed, pedFirstDriver, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
							
							OPEN_SEQUENCE_TASK(tempSeq)
								IF GET_ENTITY_HEADING(args.myOtherPed) < targetData.fOtherPedHead - 30
								OR GET_ENTITY_HEADING(args.myOtherPed) > targetData.fOtherPedHead + 30
									TASK_ACHIEVE_HEADING(NULL, targetData.fOtherPedHead)		//make her face towards the car if she isnt already facing that general direction
									PRINTLN("TASK TO ACHIEVE HEADING")
								ENDIF
								TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_outro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
								TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_b", SLOW_BLEND_IN, SLOW_BLEND_OUT)
								TASK_PLAY_ANIM(NULL, sHookerAnimDict, "idle_wait", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(tempSeq)
							TASK_PERFORM_SEQUENCE(args.myOtherPed, tempSeq)
							CLEAR_SEQUENCE_TASK(tempSeq)
							PRINTLN("TASKING FIRST CAR DRIVER AND HOOKER TO LOOK AT EACH OTHER")
						ENDIF
						
						IF NOT IS_TIMER_STARTED(tFirstCarTimer)
							START_TIMER_NOW(tFirstCarTimer)
							PRINTLN("STARTING TIMER - tFirstCarTimer")
						ELSE
							RESTART_TIMER_NOW(tFirstCarTimer)
							PRINTLN("RESTARTING TIMER - tFirstCarTimer")
						ENDIF
						
						ADD_PED_FOR_DIALOGUE(args.assConv, 5, pedFirstDriver, "VICE_FIRST")
						
						IF NOT bPlayedFirstConvo
							IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_FIRST", CONV_PRIORITY_HIGH)
								PRINTLN("bPlayedFirstConvo = TRUE")
								bPlayedFirstConvo = TRUE
							ENDIF
						ENDIF
						
						PRINTLN("iFirstCarStages = 2")
						iFirstCarStages = 2
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------
		CASE 2
			IF NOT bPlayedFirstConvo
				IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_FIRST", CONV_PRIORITY_HIGH)
					PRINTLN("bPlayedFirstConvo = TRUE")
					bPlayedFirstConvo = TRUE
				ENDIF
			ENDIF
		
			IF IS_TIMER_STARTED(tFirstCarTimer)
				IF GET_TIMER_IN_SECONDS(tFirstCarTimer) > 7
				
					//make the dummy car wander and no longer follow a waypoint recording
					IF IS_VEHICLE_DRIVEABLE(viFirstCar)
						IF NOT IS_PED_INJURED(pedFirstDriver)
							OPEN_SEQUENCE_TASK(tempSeq)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, viFirstCar, vRandomVehicleDriveToPos, MISSION_GOTO, 15, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS, 1.0, -1)
								TASK_VEHICLE_DRIVE_WANDER(NULL, viFirstCar, 15, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
							CLOSE_SEQUENCE_TASK(tempSeq)
							TASK_PERFORM_SEQUENCE(pedFirstDriver, tempSeq)
							CLEAR_SEQUENCE_TASK(tempSeq)
							PRINTLN("TASKING FIRST CAR TO WANDER")
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(pedFirstDriver) AND NOT IS_ENTITY_DEAD(args.myOtherPed)
						TASK_CLEAR_LOOK_AT(pedFirstDriver)
						TASK_CLEAR_LOOK_AT(args.myOtherPed)
						PRINTLN("CLEARING LOOK AT TASKS")
					ENDIF
					
					PRINTLN("iFirstCarStages = 3")
					iFirstCarStages = 3
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------
		CASE 3
			IF DOES_ENTITY_EXIST(viFirstCar)
				IF NOT IS_ENTITY_ON_SCREEN(viFirstCar)
					
					IF DOES_ENTITY_EXIST(pedFirstDriver)
						SET_PED_AS_NO_LONGER_NEEDED(pedFirstDriver)
						PRINTLN("SETTING PED AS NO LONGER NEEDED - pedFirstDriver")
					ENDIF
					
					SET_VEHICLE_AS_NO_LONGER_NEEDED(viFirstCar)
					PRINTLN("SETTING ENTITY AS NO LONGER NEEDED - viFirstCar")	
					
					PRINTLN("iFirstCarStages = 4")
					iFirstCarStages = 4
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------
		CASE 4
			RETURN TRUE
		BREAK
		//----------------------------------------------------------------------------------------------------------
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_FIRST_CAR(BOOL bForceDelete = FALSE)
	IF DOES_ENTITY_EXIST(pedFirstDriver)
		IF IS_ENTITY_OCCLUDED(pedFirstDriver)
		AND NOT bForceDelete
			SET_PED_AS_NO_LONGER_NEEDED(pedFirstDriver)
			PRINTLN("PED IS VISIBLE: pedFirstDriver - SET AS NO LONGER NEEDED")
		ELSE
			DELETE_PED(pedFirstDriver)
			PRINTLN("DELETE PED - pedFirstDriver")
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(viFirstCar)
		IF IS_ENTITY_OCCLUDED(viFirstCar)
		AND NOT bForceDelete
			SET_VEHICLE_AS_NO_LONGER_NEEDED(viFirstCar)
			PRINTLN("VEHICLE IS VISIBLE: viFirstCar - SET AS NO LONGER NEEDED")
		ELSE
			DELETE_VEHICLE(viFirstCar)
			PRINTLN("DELETE VEHICLE - viFirstCar")
		ENDIF
	ENDIF
ENDPROC


PROC PRINT_RETURN_TO_GIRL_MESSAGE_AND_HANDLE_FAIL_TIMER()
	IF NOT IS_TIMER_STARTED(tInSpawnAreaTimer)
		START_TIMER_NOW(tInSpawnAreaTimer)
		PRINTLN("STARTING TIMER - tInSpawnAreaTimer")
		
		PRINT_NOW("ASS_HK_RT", DEFAULT_GOD_TEXT_TIME, 1)
	ELSE
		IF GET_TIMER_IN_SECONDS(tInSpawnAreaTimer) > 25
			PRINTLN("FAILING MISSION - PLAYER HAS BEEN IN RESTRICTED AREA FOR TOO LONG")
			SET_MISSION_FAILED(asnArgs, targetData, FAIL_ABANDONED_MISSION)
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL OKAY_TO_SPAWN_NEXT_ROUND_OF_CARS()
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		//angled area encompasses the road that the target spawns on
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-439.426483,-1816.019653,17.484024>>, <<-645.871460,-1681.039185,31.151434>>, 61.000000)
			PRINT_RETURN_TO_GIRL_MESSAGE_AND_HANDLE_FAIL_TIMER()
		ENDIF
	ENDIF
	
	IF NOT IS_SPHERE_VISIBLE(vSphereToCheckForSendCars, 10.0)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-723.255127,-1752.763306,13.969912>>, <<-537.536438,-1891.546997,41.430756>>, 135.750000)
		PRINTLN("SPAWN LOCATION IS OFFSCREEN, CLEAR TO CONTINUE")
		RETURN TRUE
	ELSE
		PRINT_RETURN_TO_GIRL_MESSAGE_AND_HANDLE_FAIL_TIMER()
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PROC VICE_PED_LEAVE_VEHICLE_AND_ATTACK_PLAYER(PED_INDEX pedToTask)
//	SEQUENCE_INDEX tempSeq
//						
//	SET_PED_COMBAT_ATTRIBUTES(pedToTask, CA_LEAVE_VEHICLES, TRUE)
//	SET_PED_COMBAT_ATTRIBUTES(pedToTask, CA_AGGRESSIVE, TRUE)
//	
//	CLEAR_SEQUENCE_TASK(tempSeq)
//	OPEN_SEQUENCE_TASK(tempSeq)
//		TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR)
//		TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
//	CLOSE_SEQUENCE_TASK(tempSeq)
//	TASK_PERFORM_SEQUENCE(pedToTask, tempSeq)
//	CLEAR_SEQUENCE_TASK(tempSeq)
//	
//	PRINTLN("INSIDE - VICE_PED_LEAVE_VEHICLE_AND_ATTACK_PLAYER")
//ENDPROC

//PROC VICE_PED_LEAVE_VEHICLE_AND_ATTACK_PLAYER_DURING_SEX_GUY(PED_INDEX& pedToTask)
//	SEQUENCE_INDEX tempSeq
//						
//	SET_PED_COMBAT_ATTRIBUTES(pedToTask, CA_LEAVE_VEHICLES, TRUE)
//	SET_PED_COMBAT_ATTRIBUTES(pedToTask, CA_AGGRESSIVE, TRUE)
//	
//	CLEAR_SEQUENCE_TASK(tempSeq)
//	OPEN_SEQUENCE_TASK(tempSeq)
//		IF bPlayPanicAnimation
////			IF IS_ENTITY_PLAYING_ANIM(pedToTask, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_m")
//				TASK_PLAY_ANIM(NULL, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_outro_m", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//				PRINTLN("GUY IS IN SEX ACT - BUT PANIC IS SET, SO PLAY PANIC ANIMATIONS - 01")
////			ELSE
////				PRINTLN("THE GUY IS NOT PLAYING THE LOOPING SEX ANIMATION")
////			ENDIF
//		ELSE
//			TASK_PLAY_ANIM(NULL, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_m", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//			PRINTLN("bPlayPanicAnimation IS FALSE - VIA VICE_PED_LEAVE_VEHICLE_AND_ATTACK_PLAYER_DURING_SEX_GUY")
//		ENDIF
//		TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR)
//		TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
//	CLOSE_SEQUENCE_TASK(tempSeq)
//	TASK_PERFORM_SEQUENCE(pedToTask, tempSeq)
//	CLEAR_SEQUENCE_TASK(tempSeq)
//	
//	PRINTLN("INSIDE - VICE_PED_LEAVE_VEHICLE_AND_ATTACK_PLAYER_DURING_SEX_GUY")
//ENDPROC

//PROC VICE_PED_LEAVE_VEHICLE_AND_ATTACK_PLAYER_DURING_SEX_GIRL(PED_INDEX& pedToTask)
//	SEQUENCE_INDEX tempSeq
//						
//	SET_PED_COMBAT_ATTRIBUTES(pedToTask, CA_LEAVE_VEHICLES, TRUE)
//	SET_PED_COMBAT_ATTRIBUTES(pedToTask, CA_AGGRESSIVE, TRUE)
//	
//	CLEAR_SEQUENCE_TASK(tempSeq)
//	OPEN_SEQUENCE_TASK(tempSeq)
//		IF bPlayPanicAnimation
////			IF IS_ENTITY_PLAYING_ANIM(pedToTask, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_f")
//				TASK_PLAY_ANIM(NULL, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_outro_f", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//				PRINTLN("GIRL IS IN SEX ACT - BUT PANIC IS SET, SO PLAY PANIC ANIMATIONS")
////			ELSE
////				PRINTLN("THE GIRL IS NOT PLAYING THE LOOPING SEX ANIMATION")
////			ENDIF
//		ELSE
//			TASK_PLAY_ANIM(NULL, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_f", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//			PRINTLN("bPlayPanicAnimation IS FALSE - VIA VICE_PED_LEAVE_VEHICLE_AND_ATTACK_PLAYER_DURING_SEX_GIRL")
//		ENDIF
//		TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR)
//		TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
//	CLOSE_SEQUENCE_TASK(tempSeq)
//	TASK_PERFORM_SEQUENCE(pedToTask, tempSeq)
//	CLEAR_SEQUENCE_TASK(tempSeq)
//	
//	PRINTLN("INSIDE - VICE_PED_LEAVE_VEHICLE_AND_ATTACK_PLAYER_DURING_SEX_GIRL")
//ENDPROC

PROC VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GIRL(PED_INDEX pedToTask)
	SEQUENCE_INDEX tempSeq
	BOOL bPlayingSexAnim
	
	IF NOT IS_ENTITY_DEAD(pedToTask)
		SET_PED_COMBAT_ATTRIBUTES(pedToTask, CA_LEAVE_VEHICLES, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedToTask, CA_USE_VEHICLE, FALSE)
	ENDIF
	
	TASK_CLEAR_LOOK_AT(pedToTask)
	
	IF IS_ENTITY_PLAYING_ANIM(pedToTask, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_f")
	OR IS_ENTITY_PLAYING_ANIM(pedToTask, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_intro_f")
		bPlayingSexAnim = TRUE
		PRINTLN("GIRL IS PLAYING SEX ANIMATION")
	ELSE
		bPlayingSexAnim = FALSE
		PRINTLN("GIRL IS NOT PLAYING SEX ANIMATION")
	ENDIF
	
	SET_PED_FLEE_ATTRIBUTES(pedToTask, FA_USE_VEHICLE, FALSE)
	
	CLEAR_SEQUENCE_TASK(tempSeq)
	OPEN_SEQUENCE_TASK(tempSeq)
		IF bPlayPanicAnimation
			IF bPlayingSexAnim
				TASK_PLAY_ANIM(NULL, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_outro_f", SLOW_BLEND_IN, SLOW_BLEND_OUT, 3500)
				PRINTLN("GIRL IS IN SEX ACT - BUT PANIC IS SET, SO PLAY PANIC ANIMATIONS")
			ELSE
				PRINTLN("bPlayPanicAnimation IS TRUE, BUT GIRL IS NOT PERFORMING SEX ANIMATION")
			ENDIF
		ELSE
			IF bPlayingSexAnim
				TASK_PLAY_ANIM(NULL, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_f", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
				PRINTLN("GIRL IS IN SEX ACT - HAVE HER PLAY OUTRO FIRST BEFORE EXITING VEHICLE")
			ELSE
				PRINTLN("bPlayPanicAnimation IS FALSE, BUT GIRL IS NOT PERFORMING SEX ANIMATION")
			ENDIF
		ENDIF
//		TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR)
		TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
	CLOSE_SEQUENCE_TASK(tempSeq)
	IF NOT IS_ENTITY_DEAD(pedToTask)
		TASK_PERFORM_SEQUENCE(pedToTask, tempSeq)
	ENDIF
	CLEAR_SEQUENCE_TASK(tempSeq)
	
	PRINTLN("INSIDE - VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER - GIRL")
ENDPROC

PROC VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GUY(PED_INDEX pedToTask)
	SEQUENCE_INDEX tempSeq
	BOOL bPlayingSexAnim
	
	IF NOT IS_ENTITY_DEAD(pedToTask)
		SET_PED_COMBAT_ATTRIBUTES(pedToTask, CA_LEAVE_VEHICLES, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedToTask, CA_USE_VEHICLE, FALSE)
	ENDIF
	
	TASK_CLEAR_LOOK_AT(pedToTask)
	SET_PED_CAN_BE_TARGETTED(pedToTask, TRUE)
	
	IF IS_ENTITY_PLAYING_ANIM(pedToTask, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_m")
	OR IS_ENTITY_PLAYING_ANIM(pedToTask, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_intro_m")
		bPlayingSexAnim = TRUE
		PRINTLN("GUY IS PLAYING SEX ANIMATION")
	ELSE
		bPlayingSexAnim = FALSE
		PRINTLN("GUY IS NOT PLAYING SEX ANIMATION")
	ENDIF
	
	SET_PED_FLEE_ATTRIBUTES(pedToTask, FA_USE_VEHICLE, FALSE)
	
	CLEAR_SEQUENCE_TASK(tempSeq)
	OPEN_SEQUENCE_TASK(tempSeq)
		IF bPlayPanicAnimation
			IF bPlayingSexAnim
				TASK_PLAY_ANIM(NULL, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_outro_m", SLOW_BLEND_IN, SLOW_BLEND_OUT, 3000)
				PRINTLN("GUY IS IN SEX ACT - BUT PANIC IS SET, SO PLAY PANIC ANIMATIONS")
			ELSE
				PRINTLN("bPlayPanicAnimation IS TRUE, BUT GUY IS NOT PERFORMING SEX ANIMATION")
			ENDIF
		ELSE
			IF bPlayingSexAnim
				TASK_PLAY_ANIM(NULL, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_m", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
				PRINTLN("GUY IS IN SEX ACT - HAVE HIM PLAY OUTRO FIRST BEFORE EXITING VEHICLE")
			ELSE
				PRINTLN("GUY IS NOT PERFORMING SEX ANIMATION")
			ENDIF
		ENDIF
//		TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR)
//		TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
		TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
	CLOSE_SEQUENCE_TASK(tempSeq)
	IF NOT IS_ENTITY_DEAD(pedToTask)
		TASK_PERFORM_SEQUENCE(pedToTask, tempSeq)
	ENDIF
	CLEAR_SEQUENCE_TASK(tempSeq)
	
	PRINTLN("INSIDE - VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER - GUY")
ENDPROC

PROC DETACH_GIRL_FROM_CAR(ASS_ARGS& args)
	FLOAT fAnimTime
	
	IF NOT IS_PED_INJURED(args.myTarget)
		IF IS_ENTITY_PLAYING_ANIM(args.myTarget, "ODDJOBS@assassinate@vice@incar", "passenger_KO_female_m")
			fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(args.myTarget, "ODDJOBS@assassinate@vice@incar", "passenger_KO_female_m")
			PRINTLN("fAnimTime = ", fAnimTime)
		ENDIF
	ENDIF
	
	IF NOT bDetachGirlFromCar 
		IF fAnimTime >= 0.196
			IF NOT IS_PED_INJURED(args.myOtherPed)
				SET_PED_CONFIG_FLAG(args.myOtherPed,PCF_FallsOutOfVehicleWhenKilled,TRUE)
				SET_ENTITY_HEALTH(args.myOtherPed, 0)
				PRINTLN("DETACHING GIRL FROM CAR AND SETTING HEALTH TO ZERO")
				
				bDetachGirlFromCar = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_SECOND_MUSIC_CUE()
	IF NOT bTriggeredSecondMusicCue
		IF IS_TIMER_STARTED(tMusicCueTimer)
			IF GET_TIMER_IN_SECONDS(tMusicCueTimer) > 5
				TRIGGER_MUSIC_EVENT("ASS3_START")
				PRINTLN("TRIGGERING MUSIC - ASS3_START")
				bTriggeredSecondMusicCue = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_FOR_DRIVING_TOO_CLOSE(ASS_ARGS& args)
	VECTOR vPlayerPosition
	VECTOR vTargetCarPosition

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(args.myVehicle)
		vTargetCarPosition = GET_ENTITY_COORDS(args.myVehicle)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) 
		IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 2.0
			EXIT
		ENDIF
	ENDIF

	SWITCH iFollowTooCloseStages
		CASE 0
			IF VDIST2(vPlayerPosition, <<-595.5570, -1806.4512, 22.2803>>) >  625 //25m
				PRINTLN("iFollowTooCloseStages = 1")
				iFollowTooCloseStages = 1		
			ENDIF
		BREAK
		//========================================================================
		CASE 1
			IF VDIST2(vPlayerPosition, vTargetCarPosition) < 225 //15m
					
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_CLOSE", CONV_PRIORITY_HIGH)
						IF NOT IS_TIMER_STARTED(tTooCloseTimer)
							START_TIMER_NOW(tTooCloseTimer)
							PRINTLN("STARTING TIMER - tTooCloseTimer")
						ELSE
							RESTART_TIMER_NOW(tTooCloseTimer)
							PRINTLN("RESTARTING TIMER - tTooCloseTimer")
						ENDIF
						
						fTooCloseTime = GET_TIMER_IN_SECONDS(tTooCloseTimer)
						PRINTLN("iFollowTooCloseStages = 2. fTooCloseTime = ", fTooCloseTime)
						iFollowTooCloseStages = 2
					ENDIF
				ELSE
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ENDIF
		BREAK
		//========================================================================
		CASE 2
			IF IS_TIMER_STARTED(tTooCloseTimer)
				IF GET_TIMER_IN_SECONDS(tTooCloseTimer) > fTooCloseTime + 15
				AND VDIST2(vPlayerPosition, vTargetCarPosition) < 100 // 10m

					bTailedTooClose = TRUE
				
					PRINTLN("iFollowTooCloseStages = 3")
					iFollowTooCloseStages = 3
				ENDIF
			ENDIF
		BREAK
		//========================================================================
		CASE 3
			
		BREAK
	ENDSWITCH
	
ENDPROC


FUNC BOOL HAS_CASUAL_CONVERSATION_FINISHED(INT& iSpeechLineCounter)
	
	IF fTooCloseTime = -1
		SWITCH iSpeechLineCounter
			CASE 0
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJAShk_CLS_1", CONV_PRIORITY_HIGH)
					iSpeechLineCounter ++
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJAShk_CLS_2", CONV_PRIORITY_HIGH)
					iSpeechLineCounter ++
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJAShk_CLS_3", CONV_PRIORITY_HIGH)
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    checks to see if any vehicle is blocking the target's car from moving
/// PARAMS:
///    vehToCheck - car to check if blocked
///    timerStuck - timer used 
///    timerInterval - how often should we check (setting to 1.0 will check the timer every 1 second for example)
///    vectorToUseInChecks - location to check if blocked
///    fTimeBeforeConsideredStuck - how long before this should return true
///    fDistToBeConsideredStuck - how little of a distance vehToCheck has moved before the vehicle is considered stuck
/// RETURNS:
///    TRUE if fTimeBeforeConsideredStuck AND fDistToBeConsideredStuck return TRUE
FUNC BOOL IS_CAR_BLOCKED(VEHICLE_INDEX vehToCheck, structTimer& timerStuck, structTimer& timerInterval, VECTOR& vectorToUseInChecks, FLOAT fTimeBeforeConsideredStuck, FLOAT fDistToBeConsideredStuck)
	
	IF DOES_ENTITY_EXIST(vehToCheck)
		IF IS_TIMER_STARTED(timerInterval)
		
			//only check this function once a second
			IF GET_TIMER_IN_SECONDS(timerInterval) >= 1.0
				
				IF IS_TIMER_STARTED(timerStuck)
					IF GET_TIMER_IN_SECONDS(timerStuck) > fTimeBeforeConsideredStuck
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehToCheck, vectorToUseInChecks) <= fDistToBeConsideredStuck
						
							PRINTLN("RETURNING TRUE ON IS_CAR_BLOCKED")
							RETURN TRUE
						ELSE
							//store off the car's position
							IF NOT IS_ENTITY_DEAD(vehToCheck)
								vectorToUseInChecks = GET_ENTITY_COORDS(vehToCheck)		
								RESTART_TIMER_NOW(timerStuck)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//store off the car's position
					IF NOT IS_ENTITY_DEAD(vehToCheck)
						vectorToUseInChecks = GET_ENTITY_COORDS(vehToCheck)		
						RESTART_TIMER_NOW(timerStuck)
					ENDIF
				ENDIF
				
				RESTART_TIMER_NOW(timerInterval)
			ENDIF
		ELSE
			RESTART_TIMER_NOW(timerInterval)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_CAR_NEAR_JUNKYARD_PRIOR_TO_TARGET_ENTERING()
	#IF NOT IS_JAPANESE_BUILD
		VEHICLE_INDEX vehScrapyard
		
		IF targetStage >= MISSION_VICE_CHECK_CARS
			IF targetStage < MISSION_VICE_PARK_CAR
			OR ( targetStage = MISSION_VICE_PARK_CAR AND NOT IS_ENTITY_DEAD(assArgs.myVehicle) AND IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(assArgs.myVehicle) AND GET_VEHICLE_WAYPOINT_PROGRESS(assArgs.myVehicle) <= 30 )
				vehScrapyard = GET_CLOSEST_VEHICLE(vJunkyardPos, fJunkyardRadius, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
//				IF GET_CLOSEST_VEHICLE(vJunkyardPos, fJunkyardRadius, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES) != NULL
//				OR GET_CLOSEST_VEHICLE(vJunkyardPos, fJunkyardRadius, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES) != NULL
//				OR GET_CLOSEST_VEHICLE(vJunkyardPos, fJunkyardRadius, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES) != NULL
				IF vehScrapyard != NULL
					RETURN TRUE
				ENDIF
				
//				DRAW_DEBUG_SPHERE(vJunkyardPos, fJunkyardRadius)
			ENDIF
		ENDIF
		
		RETURN FALSE
	#ENDIF	
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    	Prints message to "Assassinate the target" when there isnt any dialogue or other objectives diaplyed on screen
///    Flips the bPrintedMsg BOOL to true when done
PROC PRINT_MESSAGE_TO_KILL_TARGET_WHEN_SAFE(ASS_ARGS& args)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		//remove the hooker's existing blip
		REMOVE_BLIP(args.mytargetblip)
		
		//create red blip on target
		args.mytargetblip = CREATE_BLIP_ON_ENTITY(args.myTarget, FALSE)
							
		PRINT_NOW("ASS_HK_WAIT2", DEFAULT_GOD_TEXT_TIME, 1)
		bPrintedMsg = TRUE
	ENDIF				
ENDPROC


/// PURPOSE:
///    Store off a string for the panic line to use based on the circumstances that panic was triggered
/// RETURNS:
///    String to use for the panic line
FUNC STRING STORE_PANIC_LINE()

	//play appropriate panic line
	#IF NOT IS_JAPANESE_BUILD
		IF IS_PLAYER_NEAR_BOUNCING_CAR(assArgs.myTarget, assArgs.assConv, dialogueTimer)
			PRINTLN("PLAYING BOUNCING CONVO")
			RETURN "OJAShk_HELP2"		//"You just signed your own death warrant buddy!"
		ENDIF
	#ENDIF
	
	IF bPlayerBlockingDeal
		PRINTLN("PLAYING BLOCKING CONVO")
		RETURN "OJAShk_HELP3"		//"Can't you see I'm trying to do business here?
	ELIF bTailedTooClose
		PRINTLN("PLAYING PANIC CONVO")
		RETURN "OJAShk_PANIC"		//"Fuck this guy! I'm getting out of here.
	ELIF bTargetCarStuck
		PRINTLN("PLAYING BLOCKING CONVO")	
		RETURN "OJAShk_BLOCK"		//"Fuck this let's get outta here.
	ELSE
		PRINTLN("PLAYING FRIGHTENED CONVO")
		RETURN "OJAShk_HELP"		//"Get away from me you maniac!"
	ENDIF
	
	RETURN ""
ENDFUNC


/// PURPOSE:
///    Plays the appropriate panic line based on data stored into sPanicLine
PROC PLAY_APPROPRIATE_PANIC_LINE(ASS_ARGS& args)
	IF NOT bPanicLinePlayed
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
			IF NOT IS_STRING_NULL_OR_EMPTY(sPanicLine)
				IF ARE_STRINGS_EQUAL(sPanicLine, "OJAShk_HELP")
				AND IS_PED_INJURED(args.myOtherPed)
				OR bTargetSpottedPlayerInScrapyard		//don't have hooker play the "Oh my God, stop!" line if aggro was triggered by the player spotting them in the scrapyard
					PLAY_SINGLE_LINE_FROM_CONVERSATION(args.assConv, "OJASAUD", sPanicLine, "OJAShk_HELP_3", CONV_PRIORITY_VERY_HIGH)
				ELSE
					CREATE_CONVERSATION(args.assConv, "OJASAUD", sPanicLine, CONV_PRIORITY_VERY_HIGH)
				ENDIF
			ENDIF
			bPanicLinePlayed = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC GRAB_RETRY_VEHICLE()
	IF NOT DOES_ENTITY_EXIST(viReplayVehicle)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				viReplayVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				PRINTLN("FOUND REPLAY VEHICLE")
				
				IF DOES_ENTITY_EXIST(viReplayVehicle) AND NOT IS_ENTITY_DEAD(viReplayVehicle)
					OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(viReplayVehicle)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_PLAYER_BEING_ON_ROOFTOP()

	IF iCounter = 0
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-628.644836,-1780.581421,40.331997>>, <<-600.087219,-1760.645996,48.331993>>, 24.750000)
				aggroArgs.fAimRange = 5.0
//				PRINTLN("PLAYER IS ON ROOFTOP, INCREASE CHECK AIM DISTANCE")
			ELSE
				aggroArgs.fAimRange = 28.0
//				PRINTLN("PLAYER IS NOT ON ROOFTOP, DECREASE CHECK AIM DISTANCE")
			ENDIF
		ENDIF
	ENDIF
	
	iCounter++
	
	IF iCounter > 10
		iCounter = 0
	ENDIF
ENDPROC

PROC HANDLE_COPS_MUSIC_TRIGGER()

	IF NOT bTriggeredCopsMusicCue
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF PREPARE_MUSIC_EVENT("ASS3_COPS")
				TRIGGER_MUSIC_EVENT("ASS3_COPS")
				PRINTLN("TRIGGERING MUSIC - ASS3_COPS")
				bTriggeredCopsMusicCue = TRUE
				PRINTLN("bTriggeredCopsMusicCue = TRUE")
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

//update loop for the Vice Assassination after the intro cutscene has played until the target has been killed
PROC UPDATE_LOOP_VICE(ASS_ARGS& args, AMBIENT_PEDS_IN_SCENE& ambHook)
	SEQUENCE_INDEX tempSeq
	VECTOR vClosestVehNode
	INT iClosestWaypoint
	VECTOR vTargetPos
	VECTOR vPlayerPosition, vTargetPosition // Used for check to play panic animations
	FLOAT fTemp
	
	HANDLE_PLAYER_BEING_ON_ROOFTOP()
	
	//checks to see if the player is blocking the junkyard or if the target car is stuck
	IF NOT bPlayerBlockingDeal
		IF NOT bTargetCarStuck
			IF IS_ANY_CAR_NEAR_JUNKYARD_PRIOR_TO_TARGET_ENTERING()
				bTargetCarStuck = TRUE
				
				PRINTLN("bTargetCarStuck = TRUE - CAR NEAR JUNKYARD")
			ENDIF
		ENDIF
		
		IF targetStage >= MISSION_VICE_CHECK_CARS
		AND targetStage < MISSION_VICE_TASK_PLAYER 

			//don't run the blocked car check when the target pulls up to the girl since the car will be stopped and we don't want this to falsely return true
			IF targetStage = MISSION_VICE_ENTER_CAR
			OR (NOT IS_ENTITY_DEAD(args.myOtherPed) AND (GET_SCRIPT_TASK_STATUS(args.myOtherPed, SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK))
		
				IF IS_TIMER_STARTED(tBlockTimer)
					CANCEL_TIMER(tBlockTimer)
				ENDIF
			ELSE
				#IF NOT IS_JAPANESE_BUILD
					IF IS_CAR_BLOCKED(args.myVehicle, tBlockTimer, tBlockTimerInterval, vTargetCarPos, 10.0, 6.0)
						bTargetCarStuck = TRUE
						PRINTLN("bTargetCarStuck = TRUE - IS_CAR_BLOCKED returned true")
					ENDIF
				#ENDIF
				
				#IF IS_JAPANESE_BUILD
					//let the checks be more lenient since we don't want it returning true prematurely since the car is released into traffic and could be stopped at a stoplight or something
					IF IS_CAR_BLOCKED(args.myVehicle, tBlockTimer, tBlockTimerInterval, vTargetCarPos, 20.0, 6.0)
						bTargetCarStuck = TRUE
						PRINTLN("bTargetCarStuck = TRUE - IS_CAR_BLOCKED returned true")
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//have any peds in the vice scene play their lines if the player approaches them
	MANAGE_AMBIENT_VICE_DIALOGUE(args, ambHook)	
		
	// *****PANIC CHECKS AND ANY OTHER CHECKS FOR THE MISSION *************************************
	IF targetStage < MISSION_VICE_TARGET_ESCAPE
		IF HAS_PLAYER_PISSED_OFF_PEDS_IN_VICE_SCENE(ambHook, args, iFrameCountAsn, TRUE, bTriggeredAggro)
#IF NOT IS_JAPANESE_BUILD
		OR IS_PLAYER_NEAR_BOUNCING_CAR(args.myTarget, args.assConv, dialogueTimer)
		OR IS_PLAYER_NEAR_BOUNCING_CAR(args.myOtherPed, args.assConv, dialogueTimer, FALSE)
#ENDIF
		OR bPlayerBlockingDeal
		OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		OR bTailedTooClose
		OR bTargetCarStuck
		OR bTargetSpottedPlayerInScrapyard
			PRINTLN("WE'VE AGGROED SOMETHING, SHOULD BE FAILING")
		
			REMOVE_BLIP(args.mytargetblip)
//			bCheckForSexPhoto = FALSE
			
			SET_AMBIENT_VICE_PEDS_TO_FLEE(args, ambHook)
			
			RELEASE_ALL_AMBIENT_PED_MODELS_FOR_VICE_LOT(ambHook)
			
			IF NOT IS_ENTITY_DEAD(args.myTarget) AND NOT IS_ENTITY_DEAD(args.myOtherPed) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF aggroReason = EAggro_ShotNear
				OR aggroReason = EAggro_HeardShot
				OR aggroReason = EAggro_MinorAttacked
				OR IS_PED_SHOOTING(PLAYER_PED_ID())
					vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
					vTargetPosition = GET_ENTITY_COORDS(args.myTarget)
					
					fTemp = fTemp
					fTemp = VDIST(vPlayerPosition, vTargetPosition)
					PRINTLN("DISTANCE FROM TARGET = ", fTemp)
					
					IF VDIST2(vPlayerPosition, vTargetPosition) > 25 // 5m
						bPlayPanicAnimation = TRUE
						PRINTLN("bPlayPanicAnimation = TRUE")
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(args.myTarget) AND NOT IS_PED_INJURED(args.myTarget)
			
				//if the player is close to the target, stop any ongoing converstations and have the target play a panic line
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 30
					STOP_SCRIPTED_CONVERSATION(FALSE)
					sPanicLine = STORE_PANIC_LINE()
				ENDIF

				//have target exit the vehicle and attack if his vehicle is undriveable
				IF DOES_ENTITY_EXIST(args.myVehicle) AND NOT IS_VEHICLE_DRIVEABLE(args.myVehicle)
					SET_PED_COMBAT_ATTRIBUTES(args.myTarget, CA_LEAVE_VEHICLES, TRUE)
					SET_VEHICLE_DOORS_LOCKED(args.myVehicle, VEHICLELOCK_UNLOCKED)
					VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GUY(args.myTarget)
					PRINTLN("TARGET VEHICLE IS NOT DRIVEALBE - TASKING TARGET TO LEAVE AND FLEE PLAYER")
										
					//have hooker leave the car as well
					IF NOT IS_PED_INJURED(args.myOtherPed)
						IF GET_SCRIPT_TASK_STATUS(args.myOtherPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK							
							VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GIRL(args.myOtherPed)
						ENDIF
					ENDIF
				ELSE
					IF NOT bHookerTaskedToEnterCar
						SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
						PRINTLN("FAILING MISSION! PANIC TRIGGERED EARLY AND bHookerTaskedToEnterCar = FALSE - 01")
					ELSE
						//otherwise task him to drive off and shoot at the player while driving
						
						PRINTLN("bHookerTaskedToEnterCar MUST BE TRUE")
						
						IF NOT bTaskedTarget
							IF bFinishedWaypointRecording
								PRINTLN("WAYPOINT RECORDING FINISHED - TASKING GUY TO LEAVE VEHICLE")
								
								IF bTooCloseToBouncingCar
									VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GUY(args.myTarget)
									VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GIRL(args.myOtherPed)
									PRINTLN("bTooCloseToBouncingCar IS TRUE, TASKING TARGET TO FLEE")
								ELSE
									VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GUY(args.myTarget)
									VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GIRL(args.myOtherPed)
									PRINTLN("TASKING GUY AND GIRL TO FLEE")
								ENDIF
								
								SET_VEHICLE_DOORS_LOCKED(args.myVehicle, VEHICLELOCK_UNLOCKED)	//allow player to jack if he wants
								
								IF NOT bHookerInCar
								AND NOT bPrintedMsg
									//print message to "Assassinate the target" since it hasn't yet been printed
									PRINT_NOW("ASS_HK_WAIT2", DEFAULT_GOD_TEXT_TIME, 1)
								ENDIF
								
								bTaskedTarget = TRUE
							ELSE
								PRINTLN("WAYPOINT RECORDING NOT FINISHED - TASKING GUY TO COMBAT WITH VEHICLE")
								CLEAR_PED_TASKS(args.myTarget)
								TASK_VEHICLE_MISSION_COORS_TARGET(args.myTarget, args.myVehicle, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 40, DRIVINGMODE_AVOIDCARS_RECKLESS, 9999, -1)
								ADD_VEHICLE_SUBTASK_ATTACK_PED(args.myTarget, PLAYER_PED_ID())
								
//								//note that bTailedTooClose and bTargetCarStuck get RESET IN THE AGGRO CHECK FUNCTION
//								IF NOT bTailedTooClose AND NOT bMinorAttacked
//								AND NOT bTargetCarStuck
//									ADD_VEHICLE_SUBTASK_ATTACK_PED(args.myTarget, PLAYER_PED_ID())
//									bGivenSubtask = TRUE
//								ENDIF
								
								bTaskedTarget = TRUE
							ENDIF
						ELSE
//							IF bTriggeredAggro
//							AND NOT bGivenSubtask
//								IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
//									ADD_VEHICLE_SUBTASK_ATTACK_PED(args.myTarget, PLAYER_PED_ID())
//									bGivenSubtask = TRUE
//								ENDIF
//							ENDIF
						ENDIF
							
						IF NOT IS_PED_INJURED(args.myOtherPed)
							IF ( GET_ENTITY_SPEED(args.myOtherPed) < 5 AND IS_PED_IN_VEHICLE(args.myOtherPed, args.myVehicle) )
							OR NOT IS_PED_IN_VEHICLE(args.myOtherPed, args.myVehicle)
								IF IS_PED_IN_VEHICLE(args.myOtherPed, args.myVehicle)
									IF NOT bPlayPanicAnimation
										VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GIRL(args.myOtherPed)
										PRINTLN("TASKING GIRLS TO LEAVE AND FLEE - 01")
									ENDIF
								ELSE
									TASK_SMART_FLEE_PED(args.myOtherPed, PLAYER_PED_ID(), 500, -1)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				SET_PED_KEEP_TASK(args.myTarget, TRUE)
			ELSE
				IF NOT bHookerTaskedToEnterCar
					SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
					PRINTLN("FAILING MISSION! PANIC TRIGGERED EARLY AND bHookerTaskedToEnterCar = FALSE - 02")
				ENDIF
			ENDIF
			
			//target should hate the player and cops
		 	ADD_RELATIONSHIP_GROUP("TargetGroup", relTarget)
			
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relTarget, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relTarget)
			
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relTarget, RELGROUPHASH_COP)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, relTarget)
			
			IF DOES_ENTITY_EXIST(args.myTarget)
				SET_PED_FLEE_ATTRIBUTES(args.myTarget, FA_UPDATE_TO_NEAREST_HATED_PED, TRUE)
				
				SET_PED_RELATIONSHIP_GROUP_HASH(args.myTarget, relTarget)
				
				IF NOT targetData.bFailed
					args.mytargetblip = CREATE_BLIP_ON_ENTITY(args.myTarget, FALSE)
				ENDIF
			ENDIF
			
			PRINTLN("targetStage = MISSION_VICE_TARGET_ESCAPE")
			targetStage = MISSION_VICE_TARGET_ESCAPE
		ENDIF
	ELSE
		IF NOT bPedConfigFlagsRestored
			IF NOT IS_PED_INJURED(args.myTarget)
				SET_PED_CONFIG_FLAG(args.myTarget, PCF_PreventPedFromReactingToBeingJacked, FALSE)
			ENDIF
			IF NOT IS_PED_INJURED(args.myOtherPed)
				SET_PED_CONFIG_FLAG(args.myOtherPed, PCF_PreventPedFromReactingToBeingJacked, FALSE)
			ENDIF
			bPedConfigFlagsRestored = TRUE
		ENDIF
	ENDIF
	
	
	
	// *****MAIN LOOP FOR TARGET STAGE *************************************
	SWITCH targetStage
		//Blip the hooker and have her play idle animations on sidewalk waiting for the target to show up
		CASE MISSION_VICE_SETUP
//			PRINTLN("MISSION_VICE_SETUP")
		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "assassin_hooker_wait_for_target")
			PRINTLN("SETTING REPLAY STAGE 1")
			SET_PED_NON_CREATION_AREA(<< -708.9319, -1841.2540, -100 >>, << -430.8133, -1647.6523, 100 >>)
			SET_PED_NON_CREATION_AREA(<< -708.9319, -1841.2540, -100 >>, << -430.8133, -1647.6523, 100 >>)
			
			ADD_PED_FOR_DIALOGUE(args.assConv, 1, PLAYER_PED_ID(), "FRANKLIN")
			
//			CREATE_FALSE_JOHN_IN_VEHICLE(args)
			
			IF NOT IS_PED_INJURED(args.myOtherPed)
				
				//remove the destination blip and blip the hooker
				IF DOES_BLIP_EXIST(args.mytargetblip)
					REMOVE_BLIP(args.mytargetblip)
				ENDIF
				args.mytargetblip = CREATE_BLIP_ON_ENTITY(args.myOtherPed)
												
				PRINTLN("GOING TO STATE - MISSION_SEND_FIRST_CAR")
				targetStage = MISSION_SEND_FIRST_CAR
			ELSE
				PRINTLN("args.myOtherPed IS INJURED")
			ENDIF
		BREAK
		 
		CASE MISSION_SEND_FIRST_CAR
//			PRINTLN("MISSION_SEND_FIRST_CAR")
			
			// Once the false John is off screen, get rid of his ass!
//			REMOVE_FALSE_JOHN(args)
		
			IF HANDLE_FIRST_CAR(args)
			AND OKAY_TO_SPAWN_NEXT_ROUND_OF_CARS()
			AND ARE_MODELS_LOADED_FOR_FINAL_CARS()
				IF IS_THIS_PRINT_BEING_DISPLAYED("ASS_HK_RT")
					CLEAR_PRINTS()
				ENDIF
				
				CREATE_ASSASSINATION_TARGET(targetData, args)
				CREATE_ASSASSINATION_TARGET_VEHICLE(targetData, args)
				CREATE_FALSE_JOHN_IN_VEHICLE(args)
				
				//determine the order that the cars should pull up to the hooker based on player's savedata
				POSITION_VICE_VEHICLES_FROM_SAVEDATA(args)
				
				SETTIMERA(0)				
				PRINTLN("GOING TO STATE - MISSION_VICE_SEND_CARS")
				targetStage = MISSION_VICE_SEND_CARS
			ENDIF
			
			// Music cue
			IF NOT IS_TIMER_STARTED(tMusicCueTimer)
				START_TIMER_NOW(tMusicCueTimer)
				PRINTLN("STARTING TIMER - tMusicCueTimer")
			ENDIF
			HANDLE_SECOND_MUSIC_CUE()
		BREAK
		
		//tell the target to start driving towards the hooker after X amt of time
		CASE MISSION_VICE_SEND_CARS
//			PRINTLN("MISSION_VICE_SEND_CARS")
		
			IF ( IS_VEHICLE_DRIVEABLE(args.myAuxVehicle) AND IS_ENTITY_ON_SCREEN(args.myAuxVehicle) )
			OR ( IS_VEHICLE_DRIVEABLE(args.myVehicle) AND IS_ENTITY_ON_SCREEN(args.myVehicle) )
			OR TIMERA() >= 10000
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -595.623, -1809.406, -23.282 >>, << -598.470, -1815.800, 23.282 >>, 15.0, FALSE, FALSE) // Area near the girl
					SEND_CARS_TO_HOOKER(args)
					
					PRINTLN("SENDING CARS - targetStage = MISSION_VICE_CHECK_CARS")
					targetStage = MISSION_VICE_CHECK_CARS
				ELSE
					PRINT_HELP("ASS_HK_HELP1")
//					PRINTLN("PLAYER IS IN ANGLED AREA")
				ENDIF
			ENDIF
			
			IF NOT bKillTargetCheckpointSet
				ADD_PED_FOR_DIALOGUE(args.assConv, 1, PLAYER_PED_ID(), "FRANKLIN")
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "assassin_hooker_kill_target")
				bKillTargetCheckpointSet = TRUE
			ENDIF
			
			// Music cue
			IF NOT IS_TIMER_STARTED(tMusicCueTimer)
				START_TIMER_NOW(tMusicCueTimer)			
				PRINTLN("STARTING TIMER - tMusicCueTimer")
			ENDIF
			HANDLE_SECOND_MUSIC_CUE()
		BREAK

		//Have the target stop the car and sound the horn once he arrives at the hooker
		CASE MISSION_VICE_CHECK_CARS
//			PRINTLN("MISSION_VICE_CHECK_CARS")
		
			//control behavior of false john - have him pull up and proposition the hooker and then drive away shortly after
			CONTROL_DUMMY_CAR_BEHAVIOR(args)
		
			HANDLE_SECOND_MUSIC_CUE()
			
			IF NOT bTaskedToLookAtTarget
				IF bPlayedCheapLine // At this point, the girl should be done with the first ped.
//					TASK_CLEAR_LOOK_AT(args.myOtherPed)
					TASK_LOOK_AT_ENTITY(args.myOtherPed, args.myTarget, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					TASK_LOOK_AT_ENTITY(args.myTarget, args.myOtherPed, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					PRINTLN("TASKING GIRL AND TARGET TO LOOK AT EACH OTHER")
					bTaskedToLookAtTarget = TRUE
				ENDIF
			ENDIF
			
			//check to see if target has pulled up to hooker
			IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
				IF NOT IS_PED_INJURED(args.myTarget)
					IF WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(tlTargetWaypoint, GET_ENTITY_COORDS(args.myVehicle), iClosestWaypoint)
						IF iClosestWaypoint >= 14
							CLEAR_PED_TASKS(args.myTarget)
							BRING_VEHICLE_TO_HALT(args.myVehicle, 10, 1)
							START_VEHICLE_HORN(args.myVehicle, 750)
							SETTIMERA(0)
							
							//make the dummy car wander and no longer follow a waypoint recording
							IF IS_VEHICLE_DRIVEABLE(args.myAuxVehicle)
								IF NOT IS_PED_INJURED(args.myAuxPed)
									CLEAR_PED_TASKS(args.myAuxPed)
									TASK_VEHICLE_DRIVE_WANDER(args.myAuxPed, args.myAuxVehicle, 15, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
								ENDIF
							ENDIF
							
							//allow peds in vice scene to now react on their own to events since I no longer care about them
							SET_AMBIENT_VICE_PEDS_TO_REACT_TO_NON_TEMP_EVENTS(ambHook)
							
							bHookerTaskedToEnterCar = TRUE
							
							PRINTLN("GOING TO STATE - MISSION_VICE_ENTER_CAR")
							targetStage = MISSION_VICE_ENTER_CAR
						ELSE
//							PRINTLN("iClosestWaypoint >= 14 IS FAILING")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("MISSION_VICE_CHECK_CARS: args.myVehicle IS NOT DRIVEABLE")
				ENDIF
			ELSE
				PRINTLN("MISSION_VICE_CHECK_CARS: args.myTarget IS INJURED")
			ENDIF
		BREAK
		
		//Have the hooker enter the targets car
		CASE MISSION_VICE_ENTER_CAR
			//add a little delay so the hooker doesnt immediately enter
			IF TIMERA() >= 1500		
				IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
					IF NOT IS_PED_INJURED(args.myOtherPed)
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) > 5
//							TASK_PLAY_ANIM(args.myTarget, "ODDJOBS@ASSASSINATE@VICE@IDLE", "idle_a", SLOW_BLEND_IN)
							IF CAN_ASSASSIN_SPEECH_PLAY(args.myTarget)
								IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
									CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_GETIN", CONV_PRIORITY_VERY_HIGH)
								ENDIF
							ENDIF
							TASK_ENTER_VEHICLE(args.myOtherPed, args.myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK, ECF_USE_RIGHT_ENTRY)
							
//							vTargetVehiclePosition = GET_ENTITY_COORDS(args.myVehicle)
//							fTargetVehicleHeading = GET_ENTITY_HEADING(args.myVehicle)
//							
//							PRINTLN("vTargetVehiclePosition = ", vTargetVehiclePosition)
//							PRINTLN("fTargetVehicleHeading = ", fTargetVehicleHeading)
							
							bTestDummyCarForPanic = FALSE
							targetStage = MISSION_VICE_FOLLOW_CAR
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//trigger panic if the player gets too close to the target as he is stopped waiting for the hooker
			TRIGGER_PANIC_IF_BLOCKING_DEAL(args)
		BREAK
		
		//Target starts driving to destination and player is told to follow them
		CASE MISSION_VICE_FOLLOW_CAR
			
			HANDLE_COPS_MUSIC_TRIGGER()
	
			IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
				IF NOT IS_PED_INJURED(args.myTarget)
					IF NOT IS_PED_INJURED(args.myOtherPed)
						IF GET_PED_IN_VEHICLE_SEAT(args.myVehicle, VS_FRONT_RIGHT) = args.myOtherPed
						AND NOT IS_PED_GETTING_INTO_A_VEHICLE(args.myOtherPed)
						
							bHookerInCar = TRUE
						
//							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "assassin_hooker_kill_target")
							
							TRIGGER_MUSIC_EVENT("ASS3_KILL")
							PRINTLN("TRIGGERING MUSIC - ASS3_KILL")
							
							PREPARE_MUSIC_EVENT("ASS3_RADIO_PASS")
							PRINTLN("PREPARING MUSIC - ASS3_RADIO_PASS")
							
							//print the kill target message if we can and blip the target red
							PRINT_MESSAGE_TO_KILL_TARGET_WHEN_SAFE(args)
							
							#IF NOT IS_JAPANESE_BUILD
								//NOTE - if we ever have issues with this in the future, try adding a flag for DF_GoOffRoadWhenAvoiding
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(args.myTarget, args.myVehicle, "OJAShk_104", DRIVINGMODE_STOPFORCARS, 0, 
									EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 15)
							#ENDIF
							
							#IF IS_JAPANESE_BUILD
								OPEN_SEQUENCE_TASK(tempSeq)
									TASK_VEHICLE_MISSION_COORS_TARGET(NULL, args.myVehicle, << -701.80682, -1615.33716, 21.89242 >>, MISSION_GOTO, 15, DRIVINGMODE_STOPFORCARS, 100, -1, FALSE)
									TASK_VEHICLE_DRIVE_WANDER(NULL, args.myVehicle, 20.0, DRIVINGMODE_STOPFORCARS)
								CLOSE_SEQUENCE_TASK(tempSeq)
								TASK_PERFORM_SEQUENCE(args.myTarget, tempSeq)
								CLEAR_SEQUENCE_TASK(tempSeq)
							#ENDIF
							
							SETTIMERA(0)
							
							//initialize this
							iSpeechStage = 0
							
							targetStage = MISSION_VICE_PARK_CAR
						ELSE
//							PRINTLN("GIRL NOT IN VEHICLE")
						ENDIF
					ELSE
						PRINTLN("GIRL IS INJURED")
					ENDIF
				ELSE
					PRINTLN("TARGET IS INJURED")
				ENDIF
			ELSE
				PRINTLN("TARGET VEHICLE NOT DRIVEABLE")
			ENDIF
		BREAK
		
		//park the car after it finishes driving to its destination under the freeway and play dialogue between target and hooker
		CASE MISSION_VICE_PARK_CAR
		
			HANDLE_COPS_MUSIC_TRIGGER()
			
			//handles the conversation while driving
			IF NOT bPrintedMsg
				PRINT_MESSAGE_TO_KILL_TARGET_WHEN_SAFE(args)
				//this function above sets bPrintedMsg to TRUE after it prints the objective and blips the target red 
			ELSE
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
					IF NOT bPickupLinePlayed
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_SET", CONV_PRIORITY_HIGH)
							bPickupLinePlayed = TRUE
						ENDIF
					ELSE
						IF NOT bCasualConversationFinished
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CAN_ASSASSIN_SPEECH_PLAY(args.myTarget)
								AND CAN_ASSASSIN_SPEECH_PLAY(args.myOtherPed)
									IF HAS_CASUAL_CONVERSATION_FINISHED(iSpeechStage)
										bCasualConversationFinished = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			//checks for player tailing too close
			CHECK_FOR_DRIVING_TOO_CLOSE(args)
			
			
			#IF NOT IS_JAPANESE_BUILD
				IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) = FINISHED_TASK
					iSpeechStage = 0	//initialize this for the upcoming sex dialogue in the next stage
					bFinishedWaypointRecording = TRUE
					
					IF NOT IS_ENTITY_DEAD(args.myOtherPed) AND NOT IS_ENTITY_DEAD(args.myTarget)
						IF GET_SCRIPT_TASK_STATUS(args.myOtherPed, SCRIPT_TASK_LOOK_AT_ENTITY) <> PERFORMING_TASK
							TASK_LOOK_AT_ENTITY(args.myOtherPed, args.myTarget, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
							PRINTLN("TASKING GUY AND GIRL TO LOOK AT EACH OTHER - 01a")
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(args.myOtherPed) AND NOT IS_ENTITY_DEAD(args.myTarget)
						IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_LOOK_AT_ENTITY) <> PERFORMING_TASK
							TASK_LOOK_AT_ENTITY(args.myTarget, args.myOtherPed, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
							PRINTLN("TASKING GUY AND GIRL TO LOOK AT EACH OTHER - 02b")
						ENDIF
					ENDIF
				
					IF NOT IS_PED_INJURED(args.myTarget)
						IF NOT IS_PED_INJURED(args.myOtherPed)
							IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
								CLEAR_PED_TASKS(args.myTarget)
								BRING_VEHICLE_TO_HALT(args.myVehicle, 10, 1)
															
								TASK_PLAY_ANIM(args.myTarget, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_base_m", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
								TASK_PLAY_ANIM(args.myOtherPed, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_base_f", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
							ENDIF
						ENDIF
					ENDIF
					
					RELEASE_ALL_AMBIENT_PED_MODELS_FOR_VICE_LOT(ambHook)
					
					targetStage = MISSION_VICE_TASK_PLAYER
					PRINTLN("targetStage = MISSION_VICE_TASK_PLAYER")
					SETTIMERA(0)
				ENDIF
			#ENDIF
		BREAK
		
		//Print objective to kill the target once the conversation has paused and no text is on screen
		CASE MISSION_VICE_TASK_PLAYER
			#IF NOT IS_JAPANESE_BUILD
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) > 50		//advance if the player is far away so we aren't displaying conversation subtitles
				OR TIMERA() > 10000
				OR CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_INCAR", CONV_PRIORITY_HIGH)		
					SETTIMERA(0)
					iSpeechStage = 0
					targetStage = MISSION_VICE_CAR_FUN
					PRINTLN("targetStage = MISSION_VICE_CAR_FUN")
				ENDIF
			#ENDIF
		BREAK
		
		
		//play anims for car sex and bounce the car
		CASE MISSION_VICE_CAR_FUN
			#IF NOT IS_JAPANESE_BUILD
				//manage peds having sex in car
				IF NOT bSexInCarFinished
					SEX_IN_CAR(args.myOtherPed, args.myTarget, args)
				ELSE
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						IF NOT IS_PED_INJURED(args.myTarget)
							IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
								//temp fix to make hooker fall out of car until we get proper anims of target punching the hooker (BUG 336272)
								IF NOT IS_PED_INJURED(args.myOtherPed)
									TASK_PLAY_ANIM(args.myTarget, "ODDJOBS@assassinate@vice@incar", "passenger_KO_female_m")
									TASK_PLAY_ANIM(args.myOtherPed, "ODDJOBS@assassinate@vice@incar", "passenger_KO_female_f")
								ENDIF
							ENDIF
	//						bCheckForSexPhoto = FALSE
							SET_VEHICLE_DOORS_LOCKED(args.myVehicle, VEHICLELOCK_UNLOCKED)
							SETTIMERA(0)
							targetStage = MISSION_VICE_TARGET_LEAVE
						ENDIF
					ENDIF
				ENDIF
			#ENDIF
		BREAK
		
		CASE MISSION_VICE_TARGET_LEAVE
			 			
			DETACH_GIRL_FROM_CAR(args)
		
			IF TIMERA() >= 1000
				IF NOT IS_PED_INJURED(args.myTarget)
					IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
						
						CLEAR_SEQUENCE_TASK(tempSeq)
						OPEN_SEQUENCE_TASK(tempSeq)
							TASK_LEAVE_VEHICLE(NULL, args.myVehicle)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -458.7189, -1719.6924, 17.6259 >>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 150.2688)
							TASK_PLAY_ANIM(NULL, "ODDJOBS@ASSASSINATE@VICE@DEALER","base", SLOW_BLEND_IN, SLOW_BLEND_OUT, 20000, AF_LOOPING)
							TASK_WANDER_STANDARD(NULL, 150.2688)
						CLOSE_SEQUENCE_TASK(tempSeq)
						IF NOT IS_ENTITY_DEAD(args.myTarget)
							TASK_PERFORM_SEQUENCE(args.myTarget, tempSeq)
							PRINTLN("TASKING TARGET TO SEQUENCE")
						ENDIF
						CLEAR_SEQUENCE_TASK(tempSeq)
						
//						TASK_VEHICLE_DRIVE_WANDER(args.myTarget, args.myVehicle, 20, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
						targetStage = MISSION_VICE_TARGET_POST_KNOCKOUT
					ENDIF
				ENDIF
			ENDIF
			
			IF bDetachGirlFromCar
				IF CHECK_FOR_TARGET_SPOTTING_PLAYER_WHILE_PARKED(args)
					bTargetSpottedPlayerInScrapyard = TRUE
				ENDIF
			ENDIF
		BREAK

		CASE MISSION_VICE_TARGET_POST_KNOCKOUT
			IF CHECK_FOR_TARGET_SPOTTING_PLAYER(args)
				bTargetSpottedPlayerInScrapyard = TRUE
			ENDIF
		BREAK
		
		
		//Panic stage - Player has aggroed the peds - task them to flee if they have not been tasked to yet
		CASE MISSION_VICE_TARGET_ESCAPE
		
			HANDLE_COPS_MUSIC_TRIGGER()
		
			IF NOT IS_PED_INJURED(args.myTarget)
				
				PLAY_APPROPRIATE_PANIC_LINE(args)
				
				IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
					
					//TODO - not sure if we should be calling this every time - what if the vehicle is moving?
//					DETACH_GIRL_FROM_CAR(args)
					
					//have the target exit his vehicle and attack the player if stuck or in water
					IF IS_VEHICLE_STUCK_TIMER_UP(args.myVehicle, VEH_STUCK_ON_ROOF, 3000)
					OR IS_VEHICLE_STUCK_TIMER_UP(args.myVehicle, VEH_STUCK_HUNG_UP, 3000)
					OR IS_VEHICLE_STUCK_TIMER_UP(args.myVehicle, VEH_STUCK_JAMMED, 3000)
					OR IS_ENTITY_IN_WATER(args.myVehicle)
						IF IS_PED_IN_VEHICLE(args.myTarget, args.myVehicle)
							//teleport the target to the closest node if stuck but not upside down or in the water
							IF ( NOT IS_ENTITY_ON_SCREEN(args.myVehicle) AND IS_ENTITY_OCCLUDED(args.myVehicle) )
							AND NOT IS_VEHICLE_STUCK_ON_ROOF(args.myVehicle)
							AND NOT IS_ENTITY_IN_WATER(args.myVehicle)
	                            PRINTLN("ASSASSINATION VICE - TELEPORTING STUCK VEHICLE TO CLOSEST VEHICLE NODE")
								
								GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(args.myVehicle), vClosestVehNode)        
								SET_PED_COORDS_KEEP_VEHICLE(args.myTarget, vClosestVehNode)
								RESET_VEHICLE_STUCK_TIMER(args.myVehicle, VEH_STUCK_ON_ROOF)
								RESET_VEHICLE_STUCK_TIMER(args.myVehicle, VEH_STUCK_HUNG_UP)
								RESET_VEHICLE_STUCK_TIMER(args.myVehicle, VEH_STUCK_JAMMED)
							ELSE
								SET_VEHICLE_DOORS_LOCKED(args.myVehicle, VEHICLELOCK_UNLOCKED)	//allow player to jack if he wants
								
								VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GUY(args.myTarget)
								
								targetStage = MISSION_VICE_TARGET_COMBAT
								PRINTLN("ASSASSINATION VICE - STUCK VEHICLE ON SCREEN - targetStage = MISSION_VICE_TARGET_COMBAT")
							ENDIF
							
							IF NOT IS_PED_INJURED(args.myOtherPed)
								IF GET_SCRIPT_TASK_STATUS(args.myOtherPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK	//check to see if she is already fleeing	
									VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GIRL(args.myOtherPed)
									PRINTLN("TASKING GIRLS TO LEAVE AND FLEE - 02")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//tell target to leave the car and attack the player if his car becomes undriveable
					IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK	
						SET_VEHICLE_DOORS_LOCKED(args.myVehicle, VEHICLELOCK_UNLOCKED)
						
						VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GUY(args.myTarget)						
						
						targetStage = MISSION_VICE_TARGET_COMBAT
						PRINTLN("ASSASSINATION VICE - VEHICLE NOT DRIVEABLE- TARGET EXITING AND ATTACKING PLAYER")
					ENDIF
				ENDIF
			ENDIF						
		BREAK
		
		CASE MISSION_VICE_TARGET_COMBAT
			//target should attack the player when on foot but try to escape if the cops show up
			IF NOT IS_PED_INJURED(args.myTarget)
				
				PLAY_APPROPRIATE_PANIC_LINE(args)
				
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					vTargetPos = GET_ENTITY_COORDS(args.myTarget)
					IF IS_COP_PED_IN_AREA_3D(<< vTargetPos.x - 30, vTargetPos.y - 30, vTargetPos.z - 30 >>, << vTargetPos.x + 30, vTargetPos.y +30, vTargetPos.z + 30 >> )
						IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
							TASK_SMART_FLEE_PED(args.myTarget, PLAYER_PED_ID(), 500, -1)
						ENDIF
					ELSE
						//retask target to attack the player if there are no cops around and the player is near the target
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) <= 10
						AND IS_PED_IN_COMBAT(PLAYER_PED_ID(), args.myTarget)
							IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_SMART_FLEE_PED) != PERFORMING_TASK
								TASK_SMART_FLEE_PED(args.myTarget, PLAYER_PED_ID(), 500, -1)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_SMART_FLEE_PED) != PERFORMING_TASK
						TASK_SMART_FLEE_PED(args.myTarget, PLAYER_PED_ID(), 500, -1)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

	//fail the script if the target escaped - TODO we should probably up this distance once we detect panic has been triggered
	IF HAS_TARGET_ESCAPED(args.myTarget, 300)
	AND bHookerTaskedToEnterCar
		IF DOES_BLIP_EXIST(args.myTargetBlip)
			REMOVE_BLIP(args.myTargetBlip)
		ENDIF
		
		IF targetStage != MISSION_VICE_CAR_FUN
		AND targetStage != MISSION_VICE_TARGET_LEAVE
			SET_MISSION_FAILED(asnArgs, targetData, FAIL_PED_ESCAPED)
		ELSE
			//to fix bug 1043148 - MB 2/9/13
			SET_MISSION_FAILED(asnArgs, targetData, FAIL_ABANDONED_MISSION)
		ENDIF
		PRINTLN("HOOKER: FAILING - TARGET ESCAPED")
	ELSE
		IF DOES_BLIP_EXIST(args.myTargetBlip)
			// handles flashing of blip if the player fails to follow to the sex destination (only check after she enters the car and 
			IF targetStage > MISSION_VICE_FOLLOW_CAR
			AND targetStage <= MISSION_VICE_TARGET_ESCAPE
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) > 75
					IF NOT IS_BLIP_FLASHING(args.myTargetBlip)
						SET_BLIP_FLASHES(args.myTargetBlip, TRUE)
					ENDIF
				ELSE
					IF IS_BLIP_FLASHING(args.myTargetBlip)
						SET_BLIP_FLASHES(args.myTargetBlip, FALSE)
					ENDIF
				ENDIF
			ELSE
				IF IS_BLIP_FLASHING(args.myTargetBlip)
					SET_BLIP_FLASHES(args.myTargetBlip, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//play dialogue if player approaches the hooker
	IF targetStage < MISSION_VICE_ENTER_CAR
		MANAGE_HOOKER_DIALOGUE(args)
	ENDIF
	
	//handle hint cams
	CONTROL_HINT_CAMS_IN_VICE_MISSION(args)
	
//	//check for bonus
//	IF bCheckForSexPhoto
//		AWARD_BONUS_FOR_SUCCESSFUL_PHOTO(args)
//	ENDIF
ENDPROC

PROC PLAY_HOOKER_CUTSCENE_NO_CAMERAS(ASS_ARGS& args)
	
	SEQUENCE_INDEX tempSeq
	
	
	IF bIntroDone
		EXIT
	ENDIF
	
	IF DO_AGGRO_CHECK(piHookerCutscenePed, assArgs.myOtherVehicle, aggroArgs, aggroReason, TRUE, SHOULD_CHECK_FOR_PROJECTILES(piHookerCutscenePed))
		SET_AMBIENT_VICE_PEDS_TO_FLEE(assArgs, ambVice)
		SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
		PRINTLN("FAILING COVER BLOWN - ATTACKED INTRO PED")
	ENDIF
	
	SWITCH introCutsceneState
		//close up shot of hooker
		CASE INTRO_CUTSCENE_CREATE
			GET_ASSASSIN_CUTSCENE_START_TIME()
			
			IF bReplaying
				START_HOOKER_CUTSCENE_ANIMS(args)
			ENDIF
			
			//remove windows in cutscene vehicle so that they don't obscure any cutscene shots
			IF IS_VEHICLE_DRIVEABLE(assArgs.myOtherVehicle)
				REMOVE_VEHICLE_WINDOW(assArgs.myOtherVehicle, SC_WINDOW_FRONT_RIGHT)
				REMOVE_VEHICLE_WINDOW(assArgs.myOtherVehicle, SC_WINDOW_FRONT_LEFT)
			ENDIF

			SETTIMERA(0)
			PRINTLN("GOING TO STATE - INTRO_CUTSCENE_TRANSITIONA")
			introCutsceneState = INTRO_CUTSCENE_TRANSITIONA
		BREAK
		
		//camera follows her close up again as she propositions a deal
		CASE INTRO_CUTSCENE_TRANSITIONA
			IF TIMERA() > 1700
				IF NOT IS_PED_INJURED(assArgs.myOtherPed)
					IF NOT bPlayedRegularLine
						IF CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJAShk_REG", CONV_PRIORITY_VERY_HIGH)
						OR TIMERA() > 5000	//safeguard to advance the mission if the conversation never plays
							bPlayedRegularLine = TRUE
							PRINTLN("bPlayedRegularLine = TRUE - 02")
							
							SETTIMERA(0)
							introCutsceneState = INTRO_CUTSCENE_END
						ENDIF
					ENDIF
				ENDIF
			ELIF TIMERA() > 1300
				IF NOT IS_PED_INJURED(piHookerCutscenePed)				
					IF GET_SCRIPT_TASK_STATUS(piHookerCutscenePed, SCRIPT_TASK_LOOK_AT_ENTITY) <> PERFORMING_TASK
						
						TASK_LOOK_AT_ENTITY(piHookerCutscenePed, assArgs.myOtherPed, 5000, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						
						ADD_PED_FOR_DIALOGUE(args.assConv, 5, piHookerCutscenePed, "KerbCrawler")
						IF NOT bPlayedRegularLine
							IF CREATE_CONVERSATION(assArgs.assConv, "OJASAUD", "OJAShk_REG", CONV_PRIORITY_VERY_HIGH)
								bPlayedRegularLine = TRUE
								PRINTLN("bPlayedRegularLine = TRUE - 01")
								
								SETTIMERA(0)
								introCutsceneState = INTRO_CUTSCENE_END
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE INTRO_CUTSCENE_END
			IF TIMERA() >= 2500
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_PED_INJURED(piHookerCutscenePed)
						IF IS_VEHICLE_DRIVEABLE(assArgs.myOtherVehicle)	
							OPEN_SEQUENCE_TASK(tempSeq)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, args.myOtherVehicle, vRandomVehicleDriveToPos, MISSION_GOTO, 15, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS, 1.0, -1)
								TASK_VEHICLE_DRIVE_WANDER(NULL, args.myOtherVehicle, 15, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
							CLOSE_SEQUENCE_TASK(tempSeq)
							TASK_PERFORM_SEQUENCE(piHookerCutscenePed, tempSeq)
							CLEAR_SEQUENCE_TASK(tempSeq)
							SET_PED_KEEP_TASK(piHookerCutscenePed, TRUE)
//							SET_PED_AS_NO_LONGER_NEEDED(piHookerCutscenePed)
//							SET_VEHICLE_AS_NO_LONGER_NEEDED(args.myOtherVehicle)
									
							REMOVE_PED_FOR_DIALOGUE(args.assConv, 5)
							
							PRINTLN("bIntroDone = TRUE")
							bIntroDone = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC RUN_INTRO_OBJECTIVE_AND_DIALOGUE(ASS_ARGS& args)
	VECTOR vPlayerPosition, vHookerPosition
	
	IF bRunIntro OR bHookerInCar
		// Not sure where to put this?  Need to stop ped look at task.
		IF IS_TIMER_STARTED(tLookAtTimer)
			IF GET_TIMER_IN_SECONDS(tLookAtTimer) > 2.0
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LOOK_AT_ENTITY) = PERFORMING_TASK
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						PRINTLN("CLEARING LOOK AT TASK")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(assArgs.myOtherPed)
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		vHookerPosition = GET_ENTITY_COORDS(assArgs.myOtherPed)
	ENDIF
	
	IF VDIST2(vPlayerPosition, vHookerPosition) < 2500 //50m
//		IF bPlayedRegularLine 
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), assArgs.myOtherPed, 3000)
					PRINTLN("TASKING PLAYER TO LOOK AT GIRL")
					
					IF NOT IS_TIMER_STARTED(tLookAtTimer)
						START_TIMER_NOW(tLookAtTimer)
						PRINTLN("STARTING TIMER - tLookAtTimer")
					ELSE
						RESTART_TIMER_NOW(tLookAtTimer)
						PRINTLN("RESTARTING TIMER - tLookAtTimer")
					ENDIF
				ENDIF
				
				CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAShk_FIND", CONV_PRIORITY_HIGH)
				REGISTER_PRINT_DELAY(printTimer, "ASS_HK_IDT")
				PRINTLN("bRunIntro = TRUE")
				bRunIntro = TRUE
			ENDIF
//		ELSE
//			PRINTLN("WAITING ON REGULAR LINE TO PLAY")
//		ENDIF
	ENDIF
ENDPROC

PROC PLACE_TARGET_AND_GIRL_INTO_VEHICLE()
	IF DOES_ENTITY_EXIST(assArgs.myVehicle) 
		IF DOES_ENTITY_EXIST(assArgs.myTarget) AND NOT IS_ENTITY_DEAD(assArgs.myTarget) 
		AND DOES_ENTITY_EXIST(assArgs.myOtherPed) AND NOT IS_ENTITY_DEAD(assArgs.myOtherPed)
			SET_PED_INTO_VEHICLE(assArgs.myTarget, assArgs.myVehicle, VS_DRIVER)
			
			CLEAR_PED_TASKS_IMMEDIATELY(assArgs.myOtherPed)
			SET_PED_INTO_VEHICLE(assArgs.myOtherPed, assArgs.myVehicle, VS_FRONT_RIGHT)
			PRINTLN("PLACING TARGET AND GIRL INTO CAR")
		ELSE
			IF NOT DOES_ENTITY_EXIST(assArgs.myTarget)
				PRINTLN("TARGET DOES NOT EXIST")
			ENDIF
			IF NOT DOES_ENTITY_EXIST(assArgs.myOtherPed)
				PRINTLN("GIRL DOES NOT EXIST")
			ENDIF
		ENDIF
	ELSE
		PRINTLN("TARGET VEHICLE DOES NOT EXIST")
	ENDIF
ENDPROC

PROC REMOVE_INTRO_GUY_AND_CAR()
	IF DOES_ENTITY_EXIST(piHookerCutscenePed)
		DELETE_PED(piHookerCutscenePed)
		PRINTLN("DELETING PED - piHookerCutscenePed")
	ENDIF
	IF DOES_ENTITY_EXIST(assArgs.myOtherVehicle)
		DELETE_VEHICLE(assArgs.myOtherVehicle)
		PRINTLN("DELETING VEHICLE - assArgs.myOtherVehicle")
	ENDIF
ENDPROC

PROC SETUP_FRANKLIN_FOR_DIALOGUE_AND_SET_TAXI_DESTINATION()
	//set up player's conversation data
	ADD_PED_FOR_DIALOGUE(assArgs.assConv, 1, PLAYER_PED_ID(), "FRANKLIN")
	
	//set taxi drop-off location data if the player hails a taxi to the assassination scene
	assArgs.vTargetBlipTaxiPos = <<-544.4251, -1829.6187, 21.9962>>
	assArgs.fTargetBlipTaxiHead = 95.3853
	
ENDPROC

PROC GO_TO_CHECKPOINT_1()
	PRINTLN("Get_Replay_Mid_Mission_Stage() = 0")

	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<213.5436, -854.7532, 29.3921>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 64.8106) 
	
//			bDebugSkipping = TRUE
		
			DELETE_AMBIENT_PEDS_FOR_HOOKER(ambVice)
			REMOVE_ALL_ENTITIES_IN_ASS_STRUCT(assArgs)
			IF DOES_ENTITY_EXIST(assArgs.myVehicle)
				DELETE_VEHICLE(assArgs.myVehicle)
				PRINTLN("KILLING args.myVehicle")
			ENDIF
			IF DOES_ENTITY_EXIST(piHookerCutscenePed)
				DELETE_PED(piHookerCutscenePed)
				PRINTLN("DELETING PED - piHookerCutscenePed")
			ENDIF

//			CREATE_AMBIENT_PEDS_FOR_HOOKER(assArgs, ambVice)
//			CREATE_ASSASSINATION_TARGET(targetData, assArgs)
//			CREATE_ASSASSINATION_TARGET_VEHICLE(targetData, assArgs)
			
			targetStage = MISSION_VICE_SETUP
			bFirstObjectivePrinted = FALSE
			bIntroDone = FALSE
			bHookerInCar = FALSE
			bHookerTaskedToEnterCar = FALSE
			bPrintedMsg = FALSE
			bPlayPanicAnimation = FALSE
			bPanicLinePlayed = FALSE
			bLeaveAreaPrinted = FALSE
			bChaseHintCamKilled = FALSE
			#IF NOT IS_JAPANESE_BUILD
				bPlayedCreeperLine  = FALSE
			#ENDIF
			bCreatedViceScene = FALSE
			bKillTargetCheckpointSet = FALSE
			introCutsceneState = INTRO_CUTSCENE_CREATE
			
//			IF bFirstCheckpointSceneLoad
//				bFirstCheckpointSceneLoad = FALSE
//			ENDIF
		ENDIF
	#ENDIF
	
	BLIP_HOOKER_DESTINATION(targetData, assArgs)
	SETUP_FRANKLIN_FOR_DIALOGUE_AND_SET_TAXI_DESTINATION()
	
//	CREATE_ASSASSINATION_SCENE_HOOKER(targetData, assArgs)
	
	//fade in the game from the mission retry screen
	#IF IS_DEBUG_BUILD
		bDoingPSkip = FALSE
	#ENDIF

	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
	END_REPLAY_SETUP()
	
	FADE_IN()
	
	curStage = MISSION_STATE_PRINTS
ENDPROC

PROC GO_TO_CHECKPOINT_2()
	
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
		
//			bDebugSkipping = TRUE
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerCheckpointPosNearHooker)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerCheckpointHeadNearHooker) 
			
			DELETE_AMBIENT_PEDS_FOR_HOOKER(ambVice)
			REMOVE_ALL_ENTITIES_IN_ASS_STRUCT(assArgs)
			IF DOES_ENTITY_EXIST(assArgs.myVehicle)
				DELETE_VEHICLE(assArgs.myVehicle)
				PRINTLN("KILLING args.myVehicle")
			ENDIF
			
			WHILE NOT ARE_MODELS_LOADED_FOR_VICE_LOT()
				WAIT(0)
			ENDWHILE
			WHILE NOT ARE_MODELS_LOADED_FOR_FINAL_CARS()
				WAIT(0)
			ENDWHILE
			
			CREATE_AMBIENT_PEDS_FOR_HOOKER(assArgs, ambVice)
			REMOVE_INTRO_GUY_AND_CAR()
			CLEANUP_FIRST_CAR()
			targetStage = MISSION_VICE_SETUP
			bFirstObjectivePrinted = FALSE
			bIntroDone = FALSE
			bDoingPSkip = FALSE
			bHookerInCar = FALSE
			bHookerTaskedToEnterCar = FALSE
			bTailedTooClose = FALSE
			bTargetCarStuck = FALSE
			bTargetSpottedPlayerInScrapyard = FALSE
			bTaskedToLookAtTarget = FALSE
			bPrintedMsg = FALSE
//			bMinorAttacked = FALSE
//			bTooCloseConvo01 = FALSE
			bPlayPanicAnimation = FALSE
			bPanicLinePlayed = FALSE
			bLeaveAreaPrinted = FALSE
			bChaseHintCamKilled = FALSE
			bPlayedFirstConvo = FALSE
			#IF NOT IS_JAPANESE_BUILD
				bPlayedCreeperLine  = FALSE
			#ENDIF
			bCreatedViceScene = FALSE
			bKillTargetCheckpointSet = FALSE
			bTaskedHookerCheap = FALSE
			introCutsceneState = INTRO_CUTSCENE_CREATE
			iFirstCarStages = 0
			
//			IF bSecondCheckpointSceneLoad
//				bSecondCheckpointSceneLoad = FALSE
//			ENDIF
		ENDIF
	#ENDIF
	
	PRINTLN("Get_Replay_Mid_Mission_Stage() = 1")
	
	WHILE NOT ARE_MODELS_LOADED_FOR_VICE_LOT()
		WAIT(0)
	ENDWHILE
	WHILE NOT ARE_MODELS_LOADED_FOR_FINAL_CARS()
		WAIT(0)
	ENDWHILE
	
	CLEAR_AREA_OF_PEDS(<< -607.2620, -1800.8997, 22.3211 >>, 10)
	
	SETUP_FRANKLIN_FOR_DIALOGUE_AND_SET_TAXI_DESTINATION()
	CREATE_ASSASSINATION_SCENE_HOOKER(targetData, assArgs, FALSE)
	START_HOOKER_CUTSCENE_ANIMS(assArgs)
	CREATE_AMBIENT_PEDS_FOR_HOOKER(assArgs, ambVice)
				
	TRIGGER_MUSIC_EVENT("ASS3_RESTART1")
	PRINTLN("TRIGGERING MUSIC - ASS3_RESTART1")
	
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	END_REPLAY_SETUP()
	
	//fade in the game from the mission retry screen
	FADE_IN()	
	
	targetStage = MISSION_VICE_SETUP
	curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
ENDPROC

PROC GO_TO_CHECKPOINT_3()

	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
		
//			bDebugSkipping = TRUE
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerCheckpointPosNearHooker)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerCheckpointHeadNearHooker) 
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			DELETE_AMBIENT_PEDS_FOR_HOOKER(ambVice)
			REMOVE_ALL_ENTITIES_IN_ASS_STRUCT(assArgs)
			CLEANUP_FIRST_CAR(TRUE)
			
			IF DOES_ENTITY_EXIST(assArgs.myVehicle)
				DELETE_VEHICLE(assArgs.myVehicle)
				PRINTLN("KILLING args.myVehicle")
			ENDIF
			
			WHILE NOT ARE_MODELS_LOADED_FOR_VICE_LOT()
				WAIT(0)
			ENDWHILE
			WHILE NOT ARE_MODELS_LOADED_FOR_FINAL_CARS()
				WAIT(0)
			ENDWHILE
			
			CREATE_AMBIENT_PEDS_FOR_HOOKER(assArgs, ambVice)
//			REMOVE_FALSE_JOHN()
			bFirstObjectivePrinted = TRUE
			bIntroDone = TRUE
			bTailedTooClose = FALSE
			bTargetCarStuck = FALSE
			bTargetSpottedPlayerInScrapyard = FALSE
			bTaskedToLookAtTarget = FALSE
//			bTooCloseConvo01 = FALSE
			bPlayPanicAnimation = FALSE
			bPrintedMsg = FALSE
			bPanicLinePlayed = FALSE
			bLeaveAreaPrinted = FALSE
			bChaseHintCamKilled = FALSE
			bPlayedFirstConvo = TRUE
			bGrabbedPosition = FALSE
			#IF NOT IS_JAPANESE_BUILD
				bPlayedCreeperLine  = FALSE
			#ENDIF
			bCreatedViceScene = FALSE
			bKillTargetCheckpointSet = FALSE
			bTaskedHookerCheap = FALSE
			introCutsceneState = INTRO_CUTSCENE_CREATE
			iFirstCarStages = 4
			vPlayerKillPosition = <<0,0,0>>
			
//			IF bThirdCheckpointSceneLoad
//				bThirdCheckpointSceneLoad = FALSE
//			ENDIF
			
			PRINTLN("DEBUG: INSIDE - GO_TO_CHECKPOINT_3")
			bDoingPSkip = FALSE
		ENDIF
	#ENDIF
	
	PRINTLN("INSIDE - GO_TO_CHECKPOINT_3")
	
	bIntroDone = TRUE
	bRunIntro = TRUE
	bHookerTaskedToEnterCar = FALSE
	bPlayerWentWanted = FALSE
	fTooCloseTime = -1
	
	WHILE NOT ARE_MODELS_LOADED_FOR_VICE_LOT()
		WAIT(0)
	ENDWHILE
	WHILE NOT ARE_MODELS_LOADED_FOR_FINAL_CARS()
		WAIT(0)
	ENDWHILE
	
	
	IF targetData.otherPedEnum != DUMMY_MODEL_FOR_SCRIPT
		assArgs.myOtherPed = CREATE_PED(PEDTYPE_CIVFEMALE, targetData.otherPedEnum, targetData.vOtherPedPos, targetData.fOtherPedHead)	
		
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(assArgs.myOtherPed, TRUE)
		ADD_PED_FOR_DIALOGUE(assArgs.assConv, 4, assArgs.myOtherPed, "OJAhkHOOKER")
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(assArgs.myOtherPed, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(assArgs.myOtherPed, CA_ALWAYS_FLEE, TRUE)
		SET_PED_FLEE_ATTRIBUTES(assArgs.myOtherPed, FA_DISABLE_HANDS_UP, TRUE)
		SET_PED_CONFIG_FLAG(assArgs.myOtherPed, PCF_PreventPedFromReactingToBeingJacked, TRUE)
		SET_HOOKER_PED_COMPONENTS(assArgs.myOtherPed)
	
		START_HOOKER_CUTSCENE_ANIMS(assArgs, FALSE)
		
		REMOVE_BLIP(assArgs.myTargetBlip)
		IF NOT DOES_BLIP_EXIST(assArgs.myTargetBlip)
			assArgs.myTargetBlip = CREATE_BLIP_ON_ENTITY(assArgs.myOtherPed)
			SET_BLIP_COLOUR(assArgs.myTargetBlip, BLIP_COLOUR_BLUE)
			SET_BLIP_ROUTE(assArgs.myTargetBlip, TRUE)
			SET_BLIP_ROUTE_COLOUR(assArgs.myTargetBlip, BLIP_COLOUR_BLUE)
		ENDIF
	ENDIF
	
	bTargetKillLinePlayed = FALSE
	
	//cleanup vehicles and create important ones
	CLEANUP_FIRST_CAR()
	REMOVE_INTRO_GUY_AND_CAR()
	
	CREATE_ASSASSINATION_TARGET(targetData, assArgs)
	CREATE_ASSASSINATION_TARGET_VEHICLE(targetData, assArgs)
	CREATE_FALSE_JOHN_IN_VEHICLE(assArgs)
	POSITION_VICE_VEHICLES_FROM_SAVEDATA(assArgs)
	CREATE_AMBIENT_PEDS_FOR_HOOKER(assArgs, ambVice)
	SETTIMERA(10000)	//to make sure the cars get sent immediately
	
	
//	PLACE_TARGET_AND_GIRL_INTO_VEHICLE()
	
//	IF DOES_ENTITY_EXIST(assArgs.myVehicle) AND IS_VEHICLE_DRIVEABLE(assArgs.myVehicle)
//		SET_ENTITY_COORDS(assArgs.myVehicle, vTargetVehiclePosition)
//		SET_ENTITY_HEADING(assArgs.myVehicle, fTargetVehicleHeading)
//		PRINTLN("MOVING TARGET CAR TO POSITION = ", vTargetVehiclePosition)
//		PRINTLN("MOVING TARGET CAR TO HEADING = ", fTargetVehicleHeading)
//	ENDIF
	
	CLEAR_AREA_OF_PEDS(<< -607.2620, -1800.8997, 22.3211 >>, 10)
	CLEAR_AREA(vJunkyardPos, fJunkyardRadius, TRUE)		//makes sure there are no cars in the junkyard area 
	
	TRIGGER_MUSIC_EVENT("ASS3_RESTART2")
	PRINTLN("TRIGGERING MUSIC - ASS3_RESTART2")
	
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()

	
	BOOL bSetPlayerInVehicle = FALSE
	IF DOES_ENTITY_EXIST(viReplayVehicle) AND NOT IS_ENTITY_DEAD(viReplayVehicle) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		bSetPlayerInVehicle = TRUE
	ENDIF
	
	IF bSetPlayerInVehicle
		END_REPLAY_SETUP(viReplayVehicle)
	ELSE
		END_REPLAY_SETUP()
	ENDIF
	
	//fade in the game from the mission retry screen
	FADE_IN()
	
	targetStage = MISSION_VICE_SEND_CARS
	curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
ENDPROC

//PROC GO_TO_CHECKPOINT_3()
//		
//	IF bReplayVehicleAvailable
//		REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
//		WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
//			PRINTLN("HOOKER: Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED - 03")
//			WAIT(0)
//		ENDWHILE
//	ENDIF
//	
//	
//	BOOL bSetPlayerIntoVehicle = FALSE
//	VEHICLE_INDEX vehPlayer
//	
//	IF bReplayVehicleAvailable
//		//create the replay vehicle but make sure it isnt a water based vehicle (so we dont spawn it on land)
//		vehPlayer = CREATE_REPLAY_CHECKPOINT_VEHICLE(vMoveVehiclePosition02, fMoveVehicleHeading02)
//		
//		IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehPlayer))
//		AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
//		AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
//			SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
//			
//			IF DOES_ENTITY_EXIST(vehPlayer) AND NOT IS_ENTITY_DEAD(vehPlayer) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//				bSetPlayerIntoVehicle = TRUE
//			ELSE
//				PRINTLN("VEHICLE DOESN'T EXIST")
//			ENDIF
//		ELSE
//			DELETE_VEHICLE(vehPlayer)
//		ENDIF
//	ELSE
//		PRINTLN("REPLAY VEHICLE DOESN'T EXIST")
//	ENDIF
//	
//	IF bSetPlayerIntoVehicle
//		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayer)
//	ELSE
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), targetData.vWarpPos)
//		SET_ENTITY_HEADING(PLAYER_PED_ID(), 77.9792)
//		PRINTLN("NO REPLAY VEHICLE AVAILABLE")
//	ENDIF
//
//
//	#IF IS_DEBUG_BUILD
//		IF bDoingPSkip OR bDoingJSkip
//		
//			bDebugSkipping = TRUE
//			
//			DELETE_AMBIENT_PEDS_FOR_HOOKER(ambVice)
//			REMOVE_ALL_ENTITIES_IN_ASS_STRUCT(assArgs)
//			IF DOES_ENTITY_EXIST(assArgs.myVehicle)
//				DELETE_VEHICLE(assArgs.myVehicle)
//				PRINTLN("KILLING args.myVehicle")
//			ENDIF
//			CREATE_AMBIENT_PEDS_FOR_HOOKER(assArgs, ambVice)
//			bFirstObjectivePrinted = TRUE
//			bIntroDone = TRUE
//			bTailedTooClose = FALSE
//			bTargetCarStuck = FALSE
//			bTargetSpottedPlayerInScrapyard = FALSE
//			bTaskedToLookAtTarget = FALSE
//			bTooCloseConvo01 = FALSE
//			bPlayPanicAnimation = FALSE
//			bPrintedMsg = FALSE
//			bPanicLinePlayed = FALSE
//			bLeaveAreaPrinted = FALSE
//			bChaseHintCamKilled = FALSE
//			introCutsceneState = INTRO_CUTSCENE_CREATE
//			
//			IF bThirdCheckpointSceneLoad
//				bThirdCheckpointSceneLoad = FALSE
//			ENDIF
//			
//			PRINTLN("DEBUG: INSIDE - GO_TO_CHECKPOINT_3")
//			bDoingPSkip = FALSE
//		ENDIF
//	#ENDIF
//	
//	PRINTLN("INSIDE - GO_TO_CHECKPOINT_3")
//	
//	bIntroDone = TRUE
//	bRunIntro = TRUE
//	bHookerTaskedToEnterCar = TRUE
//	
//	CREATE_ASSASSINATION_TARGET(targetData, assArgs)
//	CREATE_ASSASSINATION_TARGET_VEHICLE(targetData, assArgs)
//	
//	IF targetData.otherPedEnum != DUMMY_MODEL_FOR_SCRIPT
//		assArgs.myOtherPed = CREATE_PED_INSIDE_VEHICLE(assArgs.myVehicle, PEDTYPE_CIVFEMALE, targetData.otherPedEnum, VS_FRONT_RIGHT)
//		SET_HOOKER_PED_COMPONENTS(assArgs.myOtherPed)
//		ADD_PED_FOR_DIALOGUE(assArgs.assConv, 4, assArgs.myOtherPed, "OJAhkHOOKER")
//		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(assArgs.myOtherPed, TRUE)
//		SET_PED_COMBAT_ATTRIBUTES(assArgs.myOtherPed, CA_ALWAYS_FLEE, TRUE)
//		SET_PED_FLEE_ATTRIBUTES(assArgs.myOtherPed, FA_DISABLE_HANDS_UP, TRUE)
//		SET_PED_CONFIG_FLAG(assArgs.myOtherPed, PCF_PreventPedFromReactingToBeingJacked, TRUE)
//		SET_PED_CAN_BE_TARGETTED(assArgs.myOtherPed, FALSE)
//		PRINTLN("CREATING GIRL")
//	ENDIF
//	
//	REMOVE_INTRO_GUY_AND_CAR()
//	PLACE_TARGET_AND_GIRL_INTO_VEHICLE()
//	
//	IF DOES_ENTITY_EXIST(assArgs.myVehicle) AND IS_VEHICLE_DRIVEABLE(assArgs.myVehicle)
//		SET_ENTITY_COORDS(assArgs.myVehicle, vTargetVehiclePosition)
//		SET_ENTITY_HEADING(assArgs.myVehicle, fTargetVehicleHeading)
//		PRINTLN("MOVING TARGET CAR TO POSITION = ", vTargetVehiclePosition)
//		PRINTLN("MOVING TARGET CAR TO HEADING = ", fTargetVehicleHeading)
//	ENDIF
//	
//	CLEAR_AREA_OF_PEDS(<< -607.2620, -1800.8997, 22.3211 >>, 10)
//	CLEAR_AREA(vJunkyardPos, fJunkyardRadius, TRUE)		//makes sure there are no cars in the junkyard area 
//	
//	TRIGGER_MUSIC_EVENT("ASS3_RESTART2")
//	PRINTLN("TRIGGERING MUSIC - ASS3_RESTART2")
//	
//	IF NOT bDebugSkipping
//		IF NOT bThirdCheckpointSceneLoad
//			IF IS_SCREEN_FADED_OUT()
//			OR IS_REPLAY_IN_PROGRESS()
//				NEW_LOAD_SCENE_START_SPHERE(targetData.vWarpPos, 10.0)
//				bThirdCheckpointSceneLoad = TRUE
//				PRINTLN("ASSASSINATION HOOKER CHECKPOINT 3: CALLING NEW_LOAD_SCENE_START_SPHERE")
//			ELSE
//				PRINTLN("ASSASSINATION HOOKER CHECKPOINT 3: SCREEN IS NOT FADED OUT")
//			ENDIF
//		ELSE
//			PRINTLN("ASSASSINATION HOOKER CHECKPOINT 3: bThirdCheckpointSceneLoad IS TRUE")
//		ENDIF
//		
//		WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
//			WAIT(0)
//			PRINTLN("HOOKER - WAITING FOR SCENE TO LOAD - 3")
//		ENDWHILE
//		
//		PRINTLN("HOOKER - SCENE HAS LOADED - 3")
//		NEW_LOAD_SCENE_STOP()
//	ENDIF
//	
//	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
//
//	//fade in the game from the mission retry screen
//	FADE_IN()
//	
//	targetStage = MISSION_VICE_FOLLOW_CAR
//	curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
//ENDPROC



PROC GO_TO_CHECKPOINT_4()

	PRINTLN("INSIDE - GO_TO_CHECKPOINT_4")
	
	
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
			DELETE_AMBIENT_PEDS_FOR_HOOKER(ambVice)
			REMOVE_ALL_ENTITIES_IN_ASS_STRUCT(assArgs)
			
			IF DOES_ENTITY_EXIST(assArgs.myVehicle)
				DELETE_VEHICLE(assArgs.myVehicle)
				PRINTLN("KILLING args.myVehicle")
			ENDIF
			
			PRINTLN("DEBUG: INSIDE - GO_TO_CHECKPOINT_4")
			bDoingPSkip = FALSE
		ENDIF
	#ENDIF
	
	DELETE_AMBIENT_PEDS_FOR_HOOKER(ambVice)
	REMOVE_ALL_ENTITIES_IN_ASS_STRUCT(assArgs)
	REMOVE_INTRO_GUY_AND_CAR()
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	
	KILL_ANY_CONVERSATION()
	
	END_REPLAY_SETUP()
	
	//fade in the game from the mission retry screen
	FADE_IN()	
	
	curStage = MISSION_STATE_SUCCESS
ENDPROC

FUNC BOOL HAS_ENDING_CELL_PHONE_CALL_COMPLETED(ASS_ARGS& args)
	IF NOT bPlayedCellPhoneCall

		ADD_PED_FOR_DIALOGUE(args.assConv, 3, NULL, "LESTER")
		ADD_PED_FOR_DIALOGUE(args.assConv, 1, PLAYER_PED_ID(), "Franklin")
		
		IF PLAYER_CALL_CHAR_CELLPHONE(args.assConv, CHAR_LESTER, "OJASAUD", "OJAS_VICE_C", CONV_MISSION_CALL)
			PRINTLN("VICE - CELL PHONE CALL")
			bPlayedCellPhoneCall = TRUE
		ENDIF
	ENDIF
	
	IF bPlayedCellPhoneCall AND HAS_CELLPHONE_CALL_FINISHED()
	
		PRINTLN("RETURNING TRUE - HAS_ENDING_CELL_PHONE_CALL_COMPLETED")
		RETURN TRUE
	ELSE
		PRINTLN("WAITING FOR CELL PHONE CALL TO FINISH")
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_PLAYER_NEAR_EITHER_VECTOR(VECTOR vPos, FLOAT fDist, VECTOR vPosSecond, FLOAT fDistSecond)
	IF GET_PLAYER_DISTANCE_FROM_LOCATION(vPos) <= fDist
	OR GET_PLAYER_DISTANCE_FROM_LOCATION(vPosSecond) <= fDistSecond
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL HAS_PLAYER_LEFT_AREA()
	VECTOR vCurrentPosition
	FLOAT fDistance
	
	IF NOT bGrabbedPosition
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			vPlayerKillPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			PRINTLN("PLAYER POSITION AFTER KILL: ", vPlayerKillPosition)
			bGrabbedPosition = TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vCurrentPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
//		PRINTLN("vCurrentPosition = ", vCurrentPosition)
	ENDIF

	fDistance = VDIST2(vPlayerKillPosition, vCurrentPosition)
//	PRINTLN("fDistance = ", fDistance)
	
	IF fDistance != 0
		IF fDistance > 30625 // 175m
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_TOUCHED_RIGHT_STICK()
	
	fRightX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) 
	fRightY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	
//	PRINTLN("fRightX = ", fRightX)
//	PRINTLN("fRightY = ", fRightY)
	
	IF fRightX > 0 
	OR fRightY > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ADJUST_CAMERA(CAMERA_INDEX & camToChange, FLOAT fTimeToSwitch, VECTOR vPosition, VECTOR vRotation, FLOAT fCamFOV, BOOL bUseShake = FALSE, FLOAT fShakeValue = 0.5)
	IF DOES_CAM_EXIST(camToChange)
		IF IS_TIMER_STARTED(tPayphoneCameraTimer)
			IF GET_TIMER_IN_SECONDS(tPayphoneCameraTimer) >= fTimeToSwitch
				SET_CAM_COORD(camPayPhoneIntro01, vPosition)
				SET_CAM_ROT(camPayPhoneIntro01, vRotation)
				SET_CAM_FOV(camPayPhoneIntro01, fCamFOV)
				
				IF bUseShake
					SHAKE_CAM(camPayPhoneIntro01, "HAND_SHAKE", fShakeValue)
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_WALKBY_GIRL(BOOL bHardRemove = FALSE)
	IF bHardRemove
		IF DOES_ENTITY_EXIST(pedWalkByGirl)
			DELETE_PED(pedWalkByGirl)
			PRINTLN("DELETING PED - pedWalkByGirl")
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(pedWalkByGirl) 
			IF NOT IS_ENTITY_ON_SCREEN(pedWalkByGirl) OR IS_ENTITY_OCCLUDED(pedWalkByGirl)
				DELETE_PED(pedWalkByGirl)
				PRINTLN("DELETING PED - pedWalkByGirl - SHOULD NOT BE VISIBLE")
			ELSE
				SET_MODEL_AS_NO_LONGER_NEEDED(mnToUse)
				SET_PED_AS_NO_LONGER_NEEDED(pedWalkByGirl)
				PRINTLN("SETTING PED AS NO LONGER NEEDED - pedWalkByGirl")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_WALKBY_GIRL()
	
	SEQUENCE_INDEX iSeq
	INT iRand

	SWITCH iWalkByGirlStages
		CASE 0
			iRand = GET_RANDOM_INT_IN_RANGE() % 3
			
			IF iRand = 0
				mnToUse = A_F_M_BEACH_01
				PRINTLN("mnToUse = A_F_M_BEACH_01")
			ELIF iRand = 1
				mnToUse = A_F_Y_SouCent_03
				PRINTLN("mnToUse = A_F_Y_SouCent_03")
			ELSE
				mnToUse = A_F_Y_SCDressy_01
				PRINTLN("mnToUse = A_F_Y_SCDressy_01")
			ENDIF
			
			PRINTLN("iWalkByGirlStages = 1")
			iWalkByGirlStages = 1
		BREAK
		CASE 1
		
			REQUEST_MODEL(mnToUse)
			
			IF HAS_MODEL_LOADED(mnToUse)
				CLEAR_AREA_OF_PEDS(<<195.6209, -848.7115, 29.8400>>, 1.0)
				
				pedWalkByGirl = CREATE_PED(PEDTYPE_CIVFEMALE, mnToUse, <<195.6209, -848.7115, 29.8400>>, 252.7701)
				IF mnToUse = A_F_M_BEACH_01
					SET_PED_COMPONENT_VARIATION(pedWalkByGirl, INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
					SET_PED_COMPONENT_VARIATION(pedWalkByGirl, INT_TO_ENUM(PED_COMPONENT,2), 3, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(pedWalkByGirl, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(pedWalkByGirl, INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(pedWalkByGirl, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
					SET_PED_COMPONENT_VARIATION(pedWalkByGirl, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
				ENDIF
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWalkByGirl, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(mnToUse)
				PRINTLN("SETTING MODEL AS NO LONGER NEEDED")
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedWalkByGirl) AND NOT IS_ENTITY_DEAD(pedWalkByGirl)
				
				CLEAR_SEQUENCE_TASK(iSeq)
				OPEN_SEQUENCE_TASK(iSeq)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<207.2075, -853.1223, 29.5467>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 245.8158)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<212.1299, -854.8853, 29.4230>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 245.8158)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<229.6208, -861.2581, 28.9823>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 245.8158)
					TASK_WANDER_STANDARD(NULL, 248.3764)
				CLOSE_SEQUENCE_TASK(iSeq)
				TASK_PERFORM_SEQUENCE(pedWalkByGirl, iSeq)
				CLEAR_SEQUENCE_TASK(iSeq)
				SET_PED_KEEP_TASK(pedWalkByGirl, TRUE)
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(pedWalkByGirl, PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
				ENDIF
			
				PRINTLN("iWalkByGirlStages = 2")
				iWalkByGirlStages = 2
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_ENTITY_DEAD(pedWalkByGirl)
				IF IS_ENTITY_AT_COORD(pedWalkByGirl, <<229.6208, -861.2581, 28.9823>>, <<1,1,1>>)

					PRINTLN("iWalkByGirlStages = 3")
					iWalkByGirlStages = 3
				ENDIF
			ENDIF
		BREAK
		CASE 3
		
		BREAK
	ENDSWITCH

ENDPROC

PROC HANDLE_INTRO_CAMERAS()

	IF iIntroCameraStages >= 1
		HANDLE_WALKBY_GIRL()
	ENDIF
	
	SWITCH iIntroCameraStages
		CASE 0
			IF ADJUST_CAMERA(camPayPhoneIntro01, 6.0, <<212.3080, -852.1151, 30.8906>>, <<-2.0463, 0.0491, -123.0298>>, 25.5584, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 1")
				iIntroCameraStages = 1
			ENDIF
		BREAK
		CASE 1
			IF ADJUST_CAMERA(camPayPhoneIntro01, 12.0, <<217.3616, -852.5518, 30.5727>>, <<0.3070, -0.0981, 113.7617>>, 37.2383, TRUE, 0.3)
				PRINTLN("iIntroCameraStages = 2")
				iIntroCameraStages = 2
			ENDIF
		BREAK
		CASE 2
			IF ADJUST_CAMERA(camPayPhoneIntro01, 19.0, <<212.0356, -856.7463, 30.7787>>, <<-4.4880, 0.0000, -21.4972>>, 33.2457, TRUE, 0.3)
				PRINTLN("iIntroCameraStages = 3")
				iIntroCameraStages = 3
			ENDIF
		BREAK
		CASE 3 // Tighten the Fov on this one
			IF ADJUST_CAMERA(camPayPhoneIntro01, 23.0, <<209.2922, -849.7044, 30.8068>>, <<-0.0685, -0.0000, -134.7147>>, 33.8575, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 4")
				iIntroCameraStages = 4
			ENDIF
		BREAK
		CASE 4
			IF ADJUST_CAMERA(camPayPhoneIntro01, 28.0, <<212.3080, -852.1151, 30.8906>>, <<-2.0463, 0.0491, -123.0298>>, 26.1877, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 5")
				iIntroCameraStages = 5
			ENDIF
		BREAK
		CASE 5
			IF ADJUST_CAMERA(camPayPhoneIntro01, 34.0, <<216.3470, -855.0798, 30.8728>>, <<-1.8635, 0.0174, 55.7844>>, 33.2457, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 6")
				iIntroCameraStages = 6
			ENDIF
		BREAK
		CASE 6
		
		BREAK
	ENDSWITCH
	
ENDPROC

PROC REMOVE_SKINNED_PHONE_PROP()
	VECTOR vPlayerPos
	
	IF NOT DOES_ENTITY_EXIST(oPayPhoneAnimated)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	IF VDIST2(vPlayerPos, vPhoneLocation) > 10000 // 100m
		IF DOES_ENTITY_EXIST(oPayPhoneAnimated)
			SET_MODEL_AS_NO_LONGER_NEEDED(P_PHONEBOX_01B_S)
			DELETE_OBJECT(oPayPhoneAnimated)
			PRINTLN("DELETING OBJECT - oPayPhoneAnimated")
		ENDIF
		
		IF DOES_ENTITY_EXIST(oPayPhone)
			SET_ENTITY_VISIBLE(oPayPhone, TRUE)
			SET_ENTITY_COLLISION(oPayPhone, TRUE)
			PRINTLN("TURNING BACK ON VISIBILITY AND COLLISION")
		ENDIF
	ENDIF
ENDPROC

PROC DO_STAGE_INTRO_CUTSCENE(ASS_ARGS& args)

//	PRINTLN("TIME: ", TIMERB())
//	IF STREAMVOL_HAS_LOADED(svPhoneCutscene)
//		PRINTLN("WE'RE LOADED")
//	ENDIF
	
	SWITCH payPhoneCutsceneStages
	
		CASE PAYPHONE_CUTSCENE_INIT
			REQUEST_ANIM_DICT("oddjobs@assassinate@vice@call")
			PRINTLN("REQUESTING ANIM DICTIONARY - oddjobs@asssasinate@vice@call")
			REQUEST_MODEL(P_PHONEBOX_01B_S)
			PRINTLN("REQUESTING MODEL - P_PHONEBOX_01B_S")
			
			IF HAS_ANIM_DICT_LOADED("oddjobs@assassinate@vice@call") AND HAS_MODEL_LOADED(P_PHONEBOX_01B_S)
				bContinueWithScene = TRUE
			ENDIF
			
			IF bContinueWithScene
				IF DOES_ENTITY_EXIST(oPayPhone)
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<216.874176,-854.317871,28.321516>>, <<210.036118,-851.764282,34.494335>>, 
					5.000000, <<220.3139, -851.9946, 29.1151>>, 250.2144)
					PRINTLN("REPOSITIONING VEHICLE")
					
					CLEAR_AREA(<<214.449524,-852.867249,29.3496>>, 3.5, TRUE)
					
					IF NOT IS_TIMER_STARTED(tPayphoneCameraTimer)
						START_TIMER_NOW(tPayphoneCameraTimer)
						PRINTLN("STARTING TIMER - tPayphoneCameraTimer")
					ENDIF
					
					IF DOES_ENTITY_EXIST(oPayPhone)

						SET_ENTITY_VISIBLE(oPayPhone, FALSE)
						SET_ENTITY_COLLISION(oPayPhone, FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(oPayPhone, FALSE)
						
						PRINTLN("TURNING OFF VISIBILITY AND COLLISION")
						
//						oPayPhoneAnimated = CREATE_OBJECT(P_PHONEBOX_01B_S, <<214.449524,-852.867249,29.387089>>) // Having to lower prop for some reason?! ... to fix Bug # 1352784
						oPayPhoneAnimated = CREATE_OBJECT(P_PHONEBOX_01B_S, <<214.449524,-852.867249,29.3496>>)
						SET_ENTITY_ROTATION(oPayPhoneAnimated, <<0.000000,0.000000,-21.253237>>)
						SET_ENTITY_COLLISION(oPayPhoneAnimated, FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(oPayPhoneAnimated, FALSE)
						
						PRINTLN("CREATING SKINNED VERSION OF PROP")
						
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
						PRINTLN("TURNING BACK ON PLAYER VISIBILITY")
					ENDIF
					
					svPhoneCutscene = STREAMVOL_CREATE_SPHERE(<<214.449524,-852.867249,29.3496>>, 350, FLAG_MAPDATA)
					PRINTLN("CREATING STREAM VOLUME - svPhoneCutscene")
					
					// Start the cutscene.
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					CLEAR_AREA_OF_PEDS(<<214.449524,-852.867249,29.3396>>, 5.0)
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerWarpPosition)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerWarpHeading)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
						PRINTLN("MOVING FRANKLIN TO PHONE")
					ENDIF
				
	//				scenePosition = << 214.473, -852.907, 29.367 >>
	//				sceneRotation = << 0.000, 0.000, -31.000 >>
	//				iSceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
					
					iSceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
					
					IF DOES_ENTITY_EXIST(oPayPhoneAnimated)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneId, oPayPhoneAnimated, -1)
						PRINTLN("oPayPhoneAnimated EXISTS, ATTACHING SCENE TO IT")
					ELSE
						PRINTLN("NOTHING EXISTS")
					ENDIF
				
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneId, "oddjobs@assassinate@vice@call", "ass_call_vice_p1", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT)
					PRINTLN("TASKING FRANKLIN TO TALK ON THE PHONE")
					
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oPayPhoneAnimated, iSceneId, "ass_call_vice_phone", "oddjobs@assassinate@vice@call", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					PRINTLN("PLAYING ANIMATION ON PAYPHONE PROP")	
						
					IF NOT DOES_CAM_EXIST(camPayPhoneIntro01)
						camPayPhoneIntro01 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCameraPosition01, vCameraRotation01, 31.3943, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						PRINTLN("CREATING CAMERA - camPayPhoneIntro01")	
						SHAKE_CAM(camPayPhoneIntro01, "HAND_SHAKE", 0.2)
					ENDIF
					
//					PLAY_SYNCHRONIZED_CAM_ANIM(camPayPhoneIntro01, iSceneId, "ass_call_vice_cam", "oddjobs@assassinate@vice@call")
//					PRINTLN("PLAYING ANIMATION ON CAMERA")
					
					ADD_PED_FOR_DIALOGUE(args.assConv, 3, NULL, "LESTER")
					ADD_PED_FOR_DIALOGUE(args.assConv, 1, PLAYER_PED_ID(), "FRANKLIN")
					
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					iNavMeshBlocking = ADD_NAVMESH_BLOCKING_OBJECT(<<214.449524,-852.867249,29.3496>>, <<1.3,1.3,2>>, 152.0039)
				   	PRINTLN("CREATING NAVMESH BLOCKING OBJECT - iNavMeshBlocking")
					
				   	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS | SPC_REMOVE_FIRES | SPC_REMOVE_PROJECTILES) // | SPC_LEAVE_CAMERA_CONTROL_ON)
					PRINTLN("REMOVING PLAYER CONTROL")
					DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
				   
				   	payPhoneCutsceneStages = PAYPHONE_CUTSCENE_UPDATE
					PRINTLN("VICE: GOING TO STATE - PAYPHONE_CUTSCENE_UPDATE")
				ELSE
					REQUEST_ANIM_DICT("oddjobs@assassinate@vice@call")
					PRINTLN("REQUESTING ANIM DICTIONARY - oddjobs@asssasinate@vice@call")
					REQUEST_MODEL(P_PHONEBOX_01B_S)
					PRINTLN("REQUESTING MODEL - P_PHONEBOX_01B_S")
					
					IF NOT DOES_ENTITY_EXIST(oPayPhone)
						oPayPhone = GET_CLOSEST_OBJECT_OF_TYPE(vPhoneLocation, 5.0, prop_phonebox_01b)
						PRINTLN("VICE: PAYPHONE WAS NOT FOUND IN TRIGGER SCENE, FIND IN SCRIPT")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_UPDATE
		
			HANDLE_INTRO_CAMERAS()

			IF IS_TIMER_STARTED(tPayphoneCutsceneTimer)
				IF GET_TIMER_IN_SECONDS(tPayphoneCutsceneTimer) > 1.0
				
					IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
						bSkipCutscene = TRUE
						
						IF NOT IS_SCREEN_FADED_OUT()
							FADE_DOWN()
						ENDIF
						
						payPhoneCutsceneStages = PAYPHONE_CUTSCENE_SKIP
						PRINTLN("VICE: GOING TO STATE - PAYPHONE_CUTSCENE_SKIP")
						BREAK
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT bPlayedPayPhoneConvo
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.05
							IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAScnt_hk", CONV_PRIORITY_VERY_HIGH)
								IF NOT IS_TIMER_STARTED(tPayphoneCutsceneTimer)
									START_TIMER_NOW(tPayphoneCutsceneTimer)
									PRINTLN("STARTING TIMER - tPayphoneCutsceneTimer")
									bPlayedPayPhoneConvo = TRUE
									
									IF IS_REPEAT_PLAY_ACTIVE()
										FADE_IN()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF IS_TIMER_STARTED(tPayphoneCutsceneTimer)
				IF GET_TIMER_IN_SECONDS(tPayphoneCutsceneTimer) > fCutsceneLength
				OR (IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.958)
				OR bSkipCutscene
					payPhoneCutsceneStages = PAYPHONE_CUTSCENE_CLEANUP
					PRINTLN("VICE: GOING TO STATE - PAYPHONE_CUTSCENE_CLEANUP")
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_SKIP
		
			STOP_SCRIPTED_CONVERSATION(FALSE)
		
			IF bSkipCutscene
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<213.8028, -853.5229, 29.3963>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 63.2918)
						PRINTLN("MOVING FRANKLIN - SKIP CUTSCENE")
					ENDIF
					
					REMOVE_WALKBY_GIRL(TRUE)
					
					STOP_SYNCHRONIZED_ENTITY_ANIM (oPayPhoneAnimated, INSTANT_BLEND_OUT, TRUE)
				
					WAIT(0)
					DO_SCREEN_FADE_IN(500)
					
					DESTROY_ALL_CAMS()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
					payPhoneCutsceneStages = PAYPHONE_CUTSCENE_CLEANUP
					PRINTLN("VICE: GOING TO STATE - PAYPHONE_CUTSCENE_CLEANUP")
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_CLEANUP
			
			IF NOT bSkipCutscene
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				DESTROY_ALL_CAMS()
			ENDIF
			
			IF STREAMVOL_IS_VALID(svPhoneCutscene)
				STREAMVOL_DELETE(svPhoneCutscene)
				PRINTLN("DELETING STREAM VOLUME - svPhoneCutscene VIA PAYPHONE_CUTSCENE_CLEANUP")
			ENDIF
			
			REMOVE_WALKBY_GIRL()
			
			SET_ENTITY_COLLISION(oPayPhoneAnimated, TRUE)
			
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE) 
			DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
		
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
			//to fix Bug 1173621 - (3/12/13) - MB
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			
			REMOVE_ANIM_DICT("oddjobs@assassinate@vice@call")
			SET_OBJECT_AS_NO_LONGER_NEEDED(oPayPhone)
			
			IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlocking)
				REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlocking)
				PRINTLN("REMOVING NAVMESH BLOCKING OBJECT - iNavMeshBlocking")
			ENDIF
			
			curStage = MISSION_STATE_INIT
			PRINTLN("VICE: GOING TO STATE - MISSION_STATE_INIT")
		BREAK
	ENDSWITCH
ENDPROC

//MAIN UPDATE LOOP FOR THE ASSASSINATION
FUNC BOOL UPDATE_ASSASSINATION_VICE(ASS_ARGS& args, AMBIENT_PEDS_IN_SCENE& ambHook)

	IF NOT IS_ENTITY_DEAD(args.myOtherPed)
		IF IS_ENTITY_PLAYING_ANIM(args.myOtherPed, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_intro_f")
		OR IS_ENTITY_PLAYING_ANIM(args.myOtherPed, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_f")
		OR IS_ENTITY_PLAYING_ANIM(args.myOtherPed, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_outro_f")
		OR IS_ENTITY_PLAYING_ANIM(args.myOtherPed, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_f")
		OR IS_ENTITY_PLAYING_ANIM(args.myOtherPed, "ODDJOBS@assassinate@vice@incar", "passenger_KO_female_f")
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(args.myTarget)
		IF IS_ENTITY_PLAYING_ANIM(args.myTarget, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_intro_m")
		OR IS_ENTITY_PLAYING_ANIM(args.myTarget, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_loop_m")
		OR IS_ENTITY_PLAYING_ANIM(args.myTarget, "ODDJOBS@ASSASSINATE@VICE@SEX", "frontseat_carsex_outro_m")
		OR IS_ENTITY_PLAYING_ANIM(args.myOtherPed, "ODDJOBS@ASSASSINATE@VICE@SEX", "FrontSeat_CarSex_Normal_Outro_m")
		OR IS_ENTITY_PLAYING_ANIM(args.myTarget, "ODDJOBS@assassinate@vice@incar", "passenger_KO_female_m")
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		ENDIF	
	ENDIF
	
	SWITCH curStage
//		CASE MISSION_STATE_TIME_LAPSE
//			timeLapseCutscene()
//		BREAK
		CASE MISSION_BRIEF_INIT
		
			IF Is_Replay_In_Progress()
				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
					PRINTLN("REPLAY VEHICLE IS AVAILABLE - HOOKER")
					bReplayVehicleAvailable = TRUE
				ELSE
					PRINTLN("REPLAY VEHICLE IS NOT AVAILABLE - HOOKER")
				ENDIF
				bReplaying = TRUE
				curStage = MISSION_STATE_INIT
				BREAK
			ENDIF
			
			DO_STAGE_INTRO_CUTSCENE(args)
		BREAK
		
		//initialize necessities for mission and create peds/vehicles/props for assassination scene
		CASE MISSION_STATE_INIT
			INIT_VICE_DATA()
			
			IF bReplayVehicleAvailable
				INIT_MISSION_REQUESTS_AND_UI_LABELS(targetData, sLoadQueue, TRUE)
			ELSE
				INIT_MISSION_REQUESTS_AND_UI_LABELS(targetData, sLoadQueue)
			ENDIF
			
//			CREATE_AMBIENT_PEDS_FOR_HOOKER(assArgs, ambVice)
			
			IF bReplaying
			
				IF g_savedGlobals.sAssassinData.fViceMissionTime <> 0
					RESTART_TIMER_AT(tMissionTime, g_savedGlobals.sAssassinData.fViceMissionTime)
					PRINTLN("RESTARTING TIMER - missionTimer AT: ", g_savedGlobals.sAssassinData.fViceMissionTime)	
				ENDIF
			
			
				IF iStageToUse = 0
					GO_TO_CHECKPOINT_1()
				ELIF iStageToUse = 1
					GO_TO_CHECKPOINT_2()
				ELIF iStageToUse = 2
					GO_TO_CHECKPOINT_3()
				ELIF iStageToUse = 3
					GO_TO_CHECKPOINT_4()
				ENDIF
				
				FADE_IN()
			ELSE
				IF NOT IS_TIMER_STARTED(tMissionTime)
					START_TIMER_NOW(tMissionTime)
					PRINTLN("STARTING TIMER - tMissionTime")
					
					g_savedGlobals.sAssassinData.fViceMissionTime = 0
					PRINTLN("VICE GLOBAL MISSION TIMER = ", g_savedGlobals.sAssassinData.fViceMissionTime)
				ENDIF
				
				BLIP_HOOKER_DESTINATION(targetData, assArgs)
				SETUP_FRANKLIN_FOR_DIALOGUE_AND_SET_TAXI_DESTINATION()
				SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(args.mytargetblip, assArgs.vTargetBlipTaxiPos, assArgs.fTargetBlipTaxiHead)
				
//				CREATE_ASSASSINATION_SCENE_HOOKER(targetData, assArgs)
				
//				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 50, FALSE, FALSE)
				PRINTLN("curStage = MISSION_STATE_PRINTS - 02")
				curStage = MISSION_STATE_PRINTS
			ENDIF
		BREAK
		
		//print the objectives and help messages immediately after launching the mission
		CASE MISSION_STATE_PRINTS
			
			//tell the player to lose their wanted level if needed and handle blipping of destination
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				PRINT_NOW(targetData.OBJECTIVES[0], DEFAULT_GOD_TEXT_TIME, 1)
				bFirstObjectivePrinted = TRUE
			ELSE
				REMOVE_BLIP(args.myTargetBlip)
				PRINTLN("REMOVING BLIP - args.myTargetBlip")
				PRINT_NOW("ASS_HK_COPS", DEFAULT_GOD_TEXT_TIME, 1)
			ENDIF
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "assassin_hooker_locate_hooker")
			OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(viReplayVehicle)			
			
//			IF NOT IS_TIMER_STARTED(tTextTimer)
//				START_TIMER_NOW(tTextTimer)
//				PRINTLN("STARTING TIMER - tTextTimer")
//			ELSE
//				RESTART_TIMER_NOW(tTextTimer)
//				PRINTLN("RESTARTING TIMER - tTextTimer")
//			ENDIF
			
			SETUP_FRANKLIN_FOR_DIALOGUE_AND_SET_TAXI_DESTINATION()
			
			ADD_PED_FOR_DIALOGUE(args.assConv, 1, PLAYER_PED_ID(), "FRANKLIN")
			
			PRINTLN("curStage = MISSION_STATE_TRAVEL")
			curStage = MISSION_STATE_TRAVEL
		BREAK
		
		//player is en route to the target. Trigger intro cutscene when in range if not wanted
		CASE MISSION_STATE_TRAVEL
		
			REMOVE_SKINNED_PHONE_PROP()
		
			GRAB_RETRY_VEHICLE()
			
			IF NOT bCreatedViceScene
				IF GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vOtherPedPos) < 250
					IF ARE_MODELS_LOADED_FOR_VICE_LOT()
						CREATE_AMBIENT_PEDS_FOR_HOOKER(assArgs, ambVice)
						CREATE_ASSASSINATION_SCENE_HOOKER(targetData, assArgs)
						bCreatedViceScene = TRUE
					ENDIF
				ENDIF
			ENDIF
			
//			IF NOT bSentText
//				IF IS_TIMER_STARTED(tTextTimer)
//					IF GET_TIMER_IN_SECONDS(tTextTimer) > 5.0
//						SEND_TEXT_MESSAGE_TO_CHARACTER_BUFFER(CHAR_FRANKLIN, CHAR_LESTER, "ASSC_VICE_TEXT", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
//						bSentText = TRUE
//					ENDIF
//				ENDIF
//			ENDIF

			// b* 2439095 - added
			IF HAS_PANIC_TRIGGERED_IN_VICE_MISSION(args, ambHook, FALSE)
				SET_AMBIENT_VICE_PEDS_TO_FLEE(assArgs, ambVice)
				SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
				PRINTLN("FAILING COVER BLOWN 03")
			ENDIF
				
			IF DOES_BLIP_EXIST(args.myTargetBlip)
				//set bit to check for wanted as a parameter for triggering panic
				CHECK_FOR_WANTED_IN_AGGRO_CHECKS()
				
//				IF IS_ENTITY_OCCLUDED(assArgs.myOtherPed)
//					PRINTLN("ENTITY IS OCCLUDED")
//				ELSE
//					PRINTLN("ENTITY IS NOT OCCLUDED")
//				ENDIF
//				IF IS_ENTITY_ON_SCREEN(assArgs.myOtherPed)
//					PRINTLN("ENTITY IS ONSCREEN")
//				ELSE
//					PRINTLN("ENTITY IS NOT ONSCREEN")
//				ENDIF
//				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-545.783264,-1804.771973,20.571665>>, <<-566.846558,-1792.358643,28.833838>>, 17.250000)
//					PRINTLN("PLAYER IS IN ANGLED AREA")
//				ENDIF
						
				//monitor to see if player has become wanted or blown their cover prior to triggering the intro cutscene and handle accordingly
				IF NOT HAS_PANIC_TRIGGERED_IN_VICE_MISSION(args, ambHook, TRUE)
				AND NOT DO_AGGRO_CHECK(piHookerCutscenePed, args.myOtherVehicle, aggroArgs, aggroReason, TRUE, SHOULD_CHECK_FOR_PROJECTILES(piHookerCutscenePed))	//ped in car in intro cutscene
				AND NOT DO_AGGRO_CHECK(assArgs.myOtherPed, args.myOtherVehicle, aggroArgs, aggroReason, TRUE, SHOULD_CHECK_FOR_PROJECTILES(assArgs.myOtherPed))	
					IF IS_PLAYER_NEAR_EITHER_VECTOR(vIntroCutscenePos[0], 35, vIntroCutscenePos[1], 35)
					OR ( DOES_ENTITY_EXIST(assArgs.myOtherPed) AND  GET_PLAYER_DISTANCE_FROM_ENTITY(assArgs.myOtherPed, FALSE) <= 35 )
						IF bCreatedViceScene
						AND (NOT IS_ENTITY_DEAD(assArgs.myOtherPed) AND IS_ENTITY_ON_SCREEN(assArgs.myOtherPed) AND NOT IS_ENTITY_OCCLUDED(assArgs.myOtherPed))
						AND (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-545.783264,-1804.771973,20.571665>>, <<-566.846558,-1792.358643,28.833838>>, 17.250000)) // Hack check to fix Bug # 646393
							
							//remove the destination blip
							REMOVE_BLIP(args.myTargetBlip)
							
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									TRIGGER_MUSIC_EVENT("ASS3_RADIO_FADE_OUT")
									PRINTLN("TRIGGERING MUSIC - ASS3_RADIO_FADE_OUT")
								ELSE
									PRINTLN("PLAYER IS NOT IN A VEHICLE - NO MUSIC")
								ENDIF
							ENDIF
							
	//						PRINT_NOW("ASS_HK_WAIT", DEFAULT_GOD_TEXT_TIME, 1)
							PRINTLN("curStage = MISSION_STATE_CUTSCENE_SETUP")
							curStage = MISSION_STATE_CUTSCENE_SETUP
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vOtherPedPos) > 100		//hooker position
						REMOVE_BLIP_AND_PRINT_OBJECTIVE_MSG_IF_WANTED(args.myTargetBlip, "ASS_HK_COPS")
					ELSE
						PRINTLN("FAILING SCRIPT - HAS_PANIC_TRIGGERED_IN_VICE_MISSION - MISSION_NAME_HOOKER")
						SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
						PRINTLN("FAILING COVER BLOWN 01")
					ENDIF
				ENDIF
			ELSE
				
//				PRINTLN("DOES_BLIP_EXIST(args.myTargetBlip) = FALSE!")
				
				//print destination objective message when not wanted and re-blip the hooker/destination
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
//					IF DOES_ENTITY_EXIST(assArgs.myOtherPed)
//						args.myTargetBlip = ADD_BLIP_FOR_ENTITY(assArgs.myOtherPed)
//						SET_BLIP_COLOUR(args.myTargetBlip, BLIP_COLOUR_BLUE)
//						SET_BLIP_ROUTE(args.myTargetBlip, TRUE)
//						SET_BLIP_ROUTE_COLOUR(args.myTargetBlip, BLIP_COLOUR_BLUE)
//					ELSE
//						PRINTLN("GIRL DOESN'T EXIST YET")
//					ENDIF
					
//					IF DOES_ENTITY_EXIST(assArgs.myOtherPed)
						BLIP_HOOKER_DESTINATION(targetData, assArgs)
//					ENDIF

					IF NOT bFirstObjectivePrinted
						PRINT_NOW("ASS_HK_WAIT", DEFAULT_GOD_TEXT_TIME, 1)
						bFirstObjectivePrinted = TRUE
					ENDIF
				ELSE
					//flip the bit so that it doesn't check for wanted in aggro checks (HAS_PANIC_TRIGGERED_IN_VICE_MISSION will handle this below)
					DONT_CHECK_FOR_WANTED_IN_AGGRO_CHECKS()
					
					IF GET_PLAYER_DISTANCE_FROM_LOCATION(vIntroCutscenePos[0]) < 50
					OR HAS_PANIC_TRIGGERED_IN_VICE_MISSION(args, ambHook, FALSE)
//					AND (NOT IS_ENTITY_DEAD(assArgs.myOtherPed) AND IS_ENTITY_ON_SCREEN(assArgs.myOtherPed) AND NOT IS_ENTITY_OCCLUDED(assArgs.myOtherPed))
						SET_AMBIENT_VICE_PEDS_TO_FLEE(assArgs, ambVice)
						SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
						PRINTLN("FAILING COVER BLOWN 02")
					ENDIF
				ENDIF
			ENDIF
		BREAK	
		
		//intro cutscene has been triggered - set it up properly
		CASE MISSION_STATE_CUTSCENE_SETUP
			//stop the player/player car gracefully
			START_HOOKER_CUTSCENE_ANIMS(args)
			PRINTLN("curStage = MISSION_STATE_CUTSCENE")
			curStage = MISSION_STATE_CUTSCENE
		BREAK
		
		//play the appropriate intro cutscene
		CASE MISSION_STATE_CUTSCENE

//			fpCutsceneCustomIntro = fpCutsceneCustomIntro
			
			PRINTLN("curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL")
			curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
		BREAK	
		
		//waiting for player to kill the target
		CASE MISSION_STATE_WAITING_FOR_TARGET_KILL
			
			PLAY_HOOKER_CUTSCENE_NO_CAMERAS(args)
			RUN_INTRO_OBJECTIVE_AND_DIALOGUE(args)
			
			IF ( bHookerTaskedToEnterCar AND CHECK_FOR_TARGET_DEATH(assArgs, targetData) )
				//everyone flee!
				SET_AMBIENT_VICE_PEDS_TO_FLEE(assArgs, ambVice, TRUE)
				
				RELEASE_ALL_AMBIENT_PED_MODELS_FOR_VICE_LOT(ambHook)
				
				IF DOES_BLIP_EXIST(args.myTargetBlip)
					REMOVE_BLIP(args.myTargetBlip)
				ENDIF
				
				VICE_PED_LEAVE_VEHICLE_AND_FLEE_PLAYER_GIRL(args.myOtherPed)
				PRINTLN("TASKING GIRLS TO LEAVE AND FLEE - 03")
			
				IF NOT IS_ENTITY_DEAD(args.myVehicle) 
					IF IS_ENTITY_DEAD(args.myTarget) AND IS_PED_IN_VEHICLE(args.myTarget, args.myVehicle)
						START_VEHICLE_HORN(args.myVehicle, 100000)
						PRINTLN("PLAYING HORN ON TARGET'S VEHICLE")
					ENDIF
					
					SET_VEHICLE_DOORS_LOCKED(args.myVehicle, VEHICLELOCK_UNLOCKED)
					PRINTLN("UNLOCKING TARGET VEHICLE'S DOOR")
				ENDIF
				
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				
				ADD_PED_FOR_DIALOGUE(args.assConv, 1, PLAYER_PED_ID(), "FRANKLIN")
				HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, args.assConv, "OJASAUD", "OJAS_VICOM", CONV_PRIORITY_VERY_HIGH)
				
				PRINTLN("curStage = MISSION_STATE_KILLCAM")
				curStage = MISSION_STATE_KILLCAM
			ELSE
				//main target update loop
				UPDATE_LOOP_VICE(assArgs, ambVice)
			ENDIF
		BREAK		
		
		//add objective to leave the scrapyard if the player is inside it (to fix bug 1181504 - 3/19/13 - MB)
		CASE MISSION_STATE_KILLCAM
			IF NOT HAS_PLAYER_LEFT_AREA()
				IF NOT bLeaveAreaPrinted
				
					DESTROY_SEX_CAM()
				
					IF NOT IS_TIMER_STARTED(tLeaveAreaCopTimer)
						START_TIMER_NOW(tLeaveAreaCopTimer)
						PRINTLN("STARTING TIMER - tLeaveAreaCopTimer")
					ELSE
						RESTART_TIMER_NOW(tLeaveAreaCopTimer)
						PRINTLN("RESTARTING TIMER - tLeaveAreaCopTimer")
					ENDIF
				
					PRINT_NOW("ASS_HK_LEAVE", DEFAULT_GOD_TEXT_TIME, 1)
					bLeaveAreaPrinted = TRUE
				ENDIF
				
				// If the player has not left the area within 30 seconds, trigger a wanted level.
				IF IS_TIMER_STARTED(tLeaveAreaCopTimer)
					IF GET_TIMER_IN_SECONDS(tLeaveAreaCopTimer) > 30.0
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							PRINTLN("SETTING THE PLAYER'S WANTED LEVEL - 2")
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bPlayerWentWanted
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
						bPlayerWentWanted = TRUE
					ENDIF
				ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("ASS3_RADIO_PASS")
					TRIGGER_MUSIC_EVENT("ASS3_RADIO_PASS")
					PRINTLN("TRIGGERING MUSIC - ASS3_RADIO_PASS")
				ENDIF
				
				PRINTLN("curStage = MISSION_STATE_SUCCESS")
				curStage = MISSION_STATE_SUCCESS
			ENDIF
			
			HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, args.assConv, "OJASAUD", "OJAS_VICOM", CONV_PRIORITY_VERY_HIGH)
		BREAK

		
		//pass the mission
		CASE MISSION_STATE_SUCCESS
			HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, args.assConv, "OJASAUD", "OJAS_VICOM", CONV_PRIORITY_VERY_HIGH)
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					// Pad the cell phone call by 3 seconds - to fix Bug # 1081593
					IF IS_TIMER_STARTED(tCellPhoneBufferTimer)
						IF GET_TIMER_IN_SECONDS(tCellPhoneBufferTimer) > 3.0
							IF HAS_ENDING_CELL_PHONE_CALL_COMPLETED(args)
								RETURN TRUE
							ENDIF
						ENDIF
					ELSE
						START_TIMER_NOW(tCellPhoneBufferTimer)
						PRINTLN("STARTING TIMER - tCellPhoneBufferTimer")
					ENDIF
				ELSE
					IF NOT bLoseCopsPrinted
						PRINT_NOW("ASS_HK_COPS", DEFAULT_GOD_TEXT_TIME, 1)
						bLoseCopsPrinted = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	
//	//do mission timeout checks prior to mission
//	IF curStage < MISSION_STATE_WAITING_FOR_TARGET_KILL 
//		IF HAS_PLAYER_ABANDONED_ASSASSINATION_AREA(bWarningHelp, targetData.vOtherPedPos, "ASS_HK_FAILWARN", 600)		//uses the hooker spawn position
//			PRINTLN("FAILING HOOKER ASSASSINATION SCRIPT BECAUSE PLAYER ABANDONED THE AREA")
//			SET_MISSION_FAILED(asnArgs, targetData, FAIL_ABANDONED_MISSION)
//			PRINTLN("HOOKER: FAILING - PLAYER HAS ABANDONDED MISSION")
//		ENDIF
//	ENDIF

	RETURN FALSE
ENDFUNC


PROC DO_FADE_OUT_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC JUMP_TO_STAGE(MISSION_STATE state, MISSION_VICE_STAGE stage,  BOOL bIsDebugJump = FALSE)
	
	DO_FADE_OUT_WITH_WAIT()
	
	//Update mission checkpoint in case they skipped the stages where it gets set.
	IF bIsDebugJump
	
		SWITCH state
			CASE MISSION_STATE_PRINTS
				GO_TO_CHECKPOINT_1()
			BREAK
			CASE MISSION_STATE_WAITING_FOR_TARGET_KILL
				SWITCH stage
					CASE MISSION_VICE_SETUP
						GO_TO_CHECKPOINT_2()
					BREAK
					CASE MISSION_VICE_SEND_CARS
						GO_TO_CHECKPOINT_3()
					BREAK
//					CASE MISSION_VICE_TARGET_COMBAT
//						GO_TO_CHECKPOINT_4()
//					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

// debug builds only for J-skipping
#IF IS_DEBUG_BUILD
	MISSION_STATE eState
	MISSION_VICE_STAGE eStage
	
	// debug skips
	PROC DO_DEBUG_SKIPS_VICE()
		
		IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iDebugJumpStage)
			IF iDebugJumpStage = 0
				eState = MISSION_STATE_PRINTS
				eStage = MISSION_VICE_SETUP
			ELIF iDebugJumpStage = 1
				eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
				eStage = MISSION_VICE_SETUP
			ELIF iDebugJumpStage = 2
				eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
				eStage = MISSION_VICE_SEND_CARS
			ENDIF
			bDoingPSkip = TRUE
			JUMP_TO_STAGE(eState, eStage, TRUE)
		ENDIF
		
		
		// mission pass S
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			CLEAR_HELP()
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			curStage = MISSION_STATE_SUCCESS
		ENDIF
		
		// mission failed F
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			SET_MISSION_FAILED(asnArgs, targetData, FAIL_ABANDONED_MISSION)
		ENDIF
		
		// skip stage P
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P) 
			
			bDoingPSkip = TRUE
			
			IF curStage > MISSION_STATE_INIT AND curStage < MISSION_STATE_SUCCESS
				IF curStage > MISSION_STATE_INIT AND curStage < MISSION_STATE_WAITING_FOR_TARGET_KILL
					eState = MISSION_STATE_PRINTS
					eStage = MISSION_VICE_SETUP
					PRINTLN("P-SKIP: eState = MISSION_STATE_PRINTS")
					PRINTLN("P-SKIP: eStage = MISSION_VICE_SETUP")
				ELIF curStage >= MISSION_STATE_WAITING_FOR_TARGET_KILL AND curStage < MISSION_STATE_SUCCESS
					IF targetStage >= MISSION_VICE_SETUP AND targetStage < MISSION_VICE_SEND_CARS
						eState = MISSION_STATE_PRINTS
						eStage = MISSION_VICE_SETUP
						PRINTLN("P-SKIP: eState = MISSION_STATE_PRINTS")
						PRINTLN("P-SKIP: eStage = MISSION_VICE_SETUP")
					ELIF targetStage >= MISSION_VICE_SEND_CARS AND targetStage <= MISSION_VICE_CHECK_CARS
						eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
						eStage = MISSION_VICE_SETUP
						PRINTLN("P-SKIP: eState = MISSION_STATE_PRINTS")
						PRINTLN("P-SKIP: eStage = MISSION_VICE_SETUP")
					ELIF targetStage >= MISSION_VICE_ENTER_CAR AND targetStage <= MISSION_VICE_TARGET_COMBAT
						eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
						eStage = MISSION_VICE_SEND_CARS
						PRINTLN("P-SKIP: eState = MISSION_STATE_WAITING_FOR_TARGET_KILL")
						PRINTLN("P-SKIP: eStage = MISSION_VICE_SETUP")
					ENDIF
				ENDIF
				
				//TODO add cleanup
				JUMP_TO_STAGE(eState, eStage, TRUE)
			ENDIF
		ENDIF
		
		// skip stage J
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			bDoingJSkip = TRUE
			
			IF curStage > MISSION_STATE_INIT AND curStage < MISSION_STATE_SUCCESS
				IF curStage < MISSION_STATE_WAITING_FOR_TARGET_KILL
					eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
					eStage = MISSION_VICE_SETUP
					PRINTLN("J-SKIP: eState = MISSION_STATE_WAITING_FOR_TARGET_KILL")
					PRINTLN("J-SKIP: eStage = MISSION_VICE_SETUP")
				ELIF curStage >= MISSION_STATE_WAITING_FOR_TARGET_KILL AND curStage < MISSION_STATE_SUCCESS
					IF targetStage < MISSION_VICE_SEND_CARS
						eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
						eStage = MISSION_VICE_SEND_CARS
						PRINTLN("J-SKIP: eState = MISSION_STATE_WAITING_FOR_TARGET_KILL")
						PRINTLN("J-SKIP: eStage = MISSION_VICE_SEND_CARS")
					ELSE
						CLEAR_HELP()
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						curStage = MISSION_STATE_SUCCESS
					ENDIF
				ENDIF
				
				//TODO add cleanup
				IF curStage != MISSION_STATE_SUCCESS
					JUMP_TO_STAGE(eState, eStage, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

//PROC CHECK_FOR_ACTION_MODE()
//	IF NOT bSetActionMode
//		IF targetStage >= MISSION_VICE_TASK_PLAYER
//			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
//					PRINTLN("HOOKER: SETTING ACTION MODE ON THE PLAYER")
//					bSetActionMode = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC


PROC SAVE_MISSION_TIMER_FOR_MISSION()
	IF IS_TIMER_STARTED(tMissionTime)
		g_savedGlobals.sAssassinData.fViceMissionTime = GET_TIMER_IN_SECONDS(tMissionTime)
		PRINTLN("CLEANUP: tMissionTime = ", g_savedGlobals.sAssassinData.fViceMissionTime)
	ELSE
		PRINTLN("CLEANUP: tMissionTime WAS NOT STARTED!!!")
	ENDIF
ENDPROC


//main script
SCRIPT
	DEBUG_MESSAGE("Assassination Vice Start")
	SET_MISSION_FLAG(TRUE)

	IF (HAS_FORCE_CLEANUP_OCCURRED())
		DEBUG_MESSAGE("FORCE CLEANUP HAS OCCURRED!!!")
		
		SAVE_MISSION_TIMER_FOR_MISSION()
		
		TRIGGER_MUSIC_EVENT("ASS3_FAIL")
		PRINTLN("TRIGGERING MUSIC - ASS3_FAIL - 02")
		
		SET_BITMASK_AS_ENUM(g_savedGlobals.sAssassinData.iGenericData, ACD_FAILED)
		Mission_Flow_Mission_Force_Cleanup()
		
		//restore phone stuff
		IF DOES_ENTITY_EXIST(oPayPhone)
			SET_ENTITY_VISIBLE(oPayPhone, TRUE)
			SET_ENTITY_COLLISION(oPayPhone, TRUE)
			SET_ENTITY_CAN_BE_DAMAGED(oPayPhone, TRUE)
		ENDIF
		
		MISSION_CLEANUP(sLoadQueue)
	ENDIF
	
	
	
	IF Is_Replay_In_Progress()
	
		bHaveRetryOnce = TRUE
		PRINTLN("VICE: SETTING -  bHaveRetryOnce = TRUE")
	
		iStageToUse = Get_Replay_Mid_Mission_Stage()
		PRINTLN("START - REPLAY IS IN PROGRESS, iStageToUse = ", iStageToUse)
		
		IF g_bShitskipAccepted
			iStageToUse ++
			PRINTLN("INCREMENTING iStageToUse DUE TO SHITSKIP. NEW STAGE TO USE = ", iStageToUse)
		ENDIF
		
		
		IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
			PRINTLN("REPLAY VEHICLE IS AVAILABLE")
			bReplayVehicleAvailable = TRUE
		ELSE
			PRINTLN("REPLAY VEHICLE IS NOT AVAILABLE")
		ENDIF
		
		IF bReplayVehicleAvailable
			REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
			WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
				PRINTLN("HOOKER: Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED")
				WAIT(0)
			ENDWHILE
		ENDIF
		
		
		IF bReplayVehicleAvailable
			//create the replay vehicle but make sure it isnt a water based vehicle (so we dont spawn it on land)
			IF iStageToUse = 0
				viReplayVehicle = CREATE_REPLAY_CHECKPOINT_VEHICLE(vMoveVehiclePosition01, fMoveVehicleHeading01)
			ELIF iStageToUse = 1
			OR iStageToUse = 2
				viReplayVehicle = CREATE_REPLAY_CHECKPOINT_VEHICLE(vMoveVehiclePosition02, fMoveVehicleHeading02)
			ELIF iStageToUse = 3
				viReplayVehicle = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-385.7669, -1720.9408, 17.9849>>, 331.5191)		//outside the scrapyard
			ENDIF
			
			IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(viReplayVehicle))
			AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viReplayVehicle))
			AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(viReplayVehicle))
				SET_VEHICLE_ON_GROUND_PROPERLY(viReplayVehicle)
			ELSE
				DELETE_VEHICLE(viReplayVehicle)
			ENDIF
		ENDIF
		
		IF iStageToUse = 0
			START_REPLAY_SETUP(<<213.5436, -854.7532, 29.3921>>, 64.8106)
			PRINTLN("CALLING START REPLAY SETUP FOR GO_TO_CHECKPOINT_1 ")
		ELIF iStageToUse = 1
		OR iStageToUse = 2
			START_REPLAY_SETUP(vPlayerCheckpointPosNearHooker, fPlayerCheckpointHeadNearHooker)
		ELIF iStageToUse = 3
			START_REPLAY_SETUP(<<-380.8187, -1721.7848, 18.0069>>, 331.5191)			//outside the scrapyard
		ENDIF
		bReplaying = TRUE
	ELSE
		bReplaying = FALSE
	ENDIF

	#IF IS_DEBUG_BUILD
		AddWidgets(myDebugData)
//		GET_RANDOM_VEHICLE_IN_SPHERE()
	#ENDIF
	
	SETUP_DEBUG()
	
	// Turning off roads along path that the target takes in his waypoint recording
	SET_ROADS_IN_ANGLED_AREA(<<-480.974152,-1801.921509,19.280453>>, <<-712.896790,-1671.164429,29.938675>>, 170.000000, FALSE, FALSE)
	SET_ROADS_IN_ANGLED_AREA(<<-662.699280,-1645.326172,24.071472>>, <<-672.214417,-1777.590088,38.812481>>, 45.000000, FALSE, FALSE)
	SET_ROADS_IN_ANGLED_AREA(<<-382.960785,-1688.731567,17.898500>>, <<-431.251160,-1811.230469,37.682495>>, 80.000000, FALSE, FALSE)
	SET_ROADS_IN_ANGLED_AREA(<<-674.891479,-1683.639404,9.116446>>, <<-446.189026,-1807.699585,31.724640>>, 200.000000, FALSE, FALSE)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.4)
//	SET_PED_MODEL_IS_SUPPRESSED(S_M_Y_Cop_01, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PEYOTE, TRUE)
	iSpeedNode = ADD_ROAD_NODE_SPEED_ZONE(<<-390.0703, -1698.8711, 17.8068>>, 10.0, 0)
	
	PRINTLN("TURNING OFF ROADS ALONG WAYPOINT PATH")
	
	// Fix Bug # 712684
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	ENDIF
	
	ASSASSINATION_ClearUnneededGenericData()
	
	iCurAssassinRank = ENUM_TO_INT(ASSASSINATION_Vice) //g_savedGlobals.sAssassinData.iCurrentAssassinRank
	missionData = ASSASSINATION_GetMissionData(iCurAssassinRank)	
	PRINTLN("Current rank is... ", iCurAssassinRank)
	
	curStage = MISSION_BRIEF_INIT
	PRINTLN("STARTING MISSION ON STAGE - MISSION_BRIEF_INIT")
	
	CLEAR_AREA(<< -463.2923, -1708.1483, 17.6777 >>, 50.0, TRUE)
	sbi01 = ADD_SCENARIO_BLOCKING_AREA((<<-463.2923, -1708.1483, 17.6777>> - <<50,50,50>>), (<<-463.2923, -1708.1483, 17.6777>> + <<50,50,50>>))
	
	//main loop
	WHILE TRUE
		WAIT(0)
		
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ASS3")
		
//		UPDATE_ZVOLUME_WIDGETS()

//		PRINTLN("MISSION STATE = ", ENUM_TO_INT(curStage))
//		PRINTLN("TARGET STATE = ", ENUM_TO_INT(targetStage))
		
		#IF IS_DEBUG_BUILD
			DO_DEBUG_SKIPS_VICE()
		#ENDIF
		
//		CHECK_FOR_ACTION_MODE()
		UPDATE_PRINT_DELAY(printTimer, 7, TRUE)
		
		IF UPDATE_FAIL_CONDITIONS(targetData)
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				SAVE_MISSION_TIMER_FOR_MISSION()
				
				REMOVE_SCENARIO_BLOCKING_AREA(sbi01)
				PRINTLN("REMOVING SCENARIO BLOCK AREA - sbi01")
				
				TRIGGER_MUSIC_EVENT("ASS3_FAIL")
				PRINTLN("TRIGGERING MUSIC - ASS3_FAIL - 01")
				
				//to fix bug 1182530 - Player spawned on sidewalk and out of the vehicle when retrying from a fail - (3/11/13)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					g_replay.iReplayInt[0] = 1			//set this integer to true that we will check later in GO_TO_CHECKPOINT_2()
				ELSE
					g_replay.iReplayInt[0] = 0			//reset this value
				ENDIF
				
				MISSION_FAILED(sLoadQueue)
			ENDIF
		ELIF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF UPDATE_ASSASSINATION_VICE(assArgs, ambVice)
				IF IS_TIMER_STARTED(tMissionTime)
					g_savedGlobals.sAssassinData.fViceMissionTime = GET_TIMER_IN_SECONDS(tMissionTime)
					PRINTLN("GLOBAL VICE MISSION TIME = ", g_savedGlobals.sAssassinData.fViceMissionTime)
				
					fTimeTaken = g_savedGlobals.sAssassinData.fViceMissionTime
					PRINTLN("fTimeTaken = ", fTimeTaken)
				ENDIF
				
				IF NOT bPlayerWentWanted			//we need to track if the player went wanted forcibly by script 
					IF NOT bLoseCopsPrinted
					AND NOT g_bShitskipAccepted
						bBonus = TRUE
						passReasonCore = PASS_BONUS
						PRINTLN("passReasonCore = PASS_BONUS")
					ENDIF
				ENDIF
				
				IF NOT bBonus
					passReasonCore = PASS_STANDARD
					PRINTLN("passReasonCore = PASS_STANDARD")
				ENDIF
				
				MISSION_PASSED(missionData, cucEndScreenCore, sLoadQueue)
			ENDIF
		ENDIF
		
//		DRAW_DEBUG_SPHERE(myDebugData.vDebugVector, myDebugData.fDebugFloat)
	ENDWHILE
	
ENDSCRIPT
