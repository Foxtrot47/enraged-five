
//Compile out Title Update changes to header functions.
//Must be before includes.
CONST_INT 	USE_TU_CHANGES	1

// Always include these two files at the top of each script
USING "script_player.sch"
USING "controller_assassination_lib.sch"
USING "Assassination_briefing_lib.sch"
USING "script_oddjob_funcs.sch"
USING "building_control_public.sch"
USING "assassination_support_lib.sch"
USING "Assassin_shared.sch"
USING "Chase_hint_cam.sch"
USING "shared_hud_displays.sch"
USING "script_oddjob_funcs.sch"
USING "taxi_functions.sch"
USING "oddjob_aggro.sch"
USING "completionpercentage_public.sch"
USING "replay_public.sch"
USING "clearMissionArea.sch"
USING "minigame_big_message.sch"
USING "replay_private.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF


CONST_INT NUM_LIMO_GUARDS 3
CONST_INT NUM_GUARDS 5				//bad name for this but it is used in other places in teh script so I dont want to remove entirely
CONST_INT NUM_ESCORT_PEDS 2
CONST_INT NUM_BODYGUARDS 2
CONST_INT NUM_POLICE_BLOCKADE_POSITIONS 7
CONST_INT NUM_AIRLOCK_SPAWN_LOCATIONS 3

ENUM Assassin_Stages
	STAGE_BRIEF_INIT,
	STAGE_BRIEF_UPDATE,
	STAGE_BRIEF_EXIT,
	STAGE_INTRO_CUTSCENE,
	STAGE_INIT,
	STAGE_STREAMING,
	STAGE_SPAWNING,
	STAGE_GET_TO_HOTEL,
	STAGE_GUARD_CUTSCENE,
	STAGE_PLANT_BOMB,
	STAGE_GUARDS_GO_TO_POSTS,
	STAGE_TARGET_LEAVE_LOBBY,
	STAGE_ENTER_LIMO,
	STAGE_LEAVE_HOTEL,
	STAGE_ESCAPING,
	STAGE_KILL_CAM,
	STAGE_EVADE_POLICE,
	STAGE_LEAVE_AREA,
	STAGE_MISSION_PASSED
ENDENUM
Assassin_Stages MainStage = STAGE_INTRO_CUTSCENE

//kill types
ENUM eKillType
	KT_STANDARD,
	KT_BONUS
ENDENUM
eKillType killType = KT_STANDARD

//Limo Guard states
ENUM assLimoGuardState
	LGS_WALK_TO_POSTS = 0,
	LGS_STAND_GUARD,
	LGS_WARNING_CHECK,
	LGS_RESPOND_TO_PLAYER,
	LGS_ENTER_LIMO,
	LGS_LEAVE_IN_LIMO,
	LGS_DRIVE_TO_END,
	LGS_DRIVING,
	LGS_ALERTED,
	LGS_ALERTED2,
	LGS_PANIC,
	LGS_ATTACKING
ENDENUM
assLimoGuardState LimoGuardState[NUM_LIMO_GUARDS]

//Motorcade ped states
ENUM assEscortState
	ES_WAITING = 0,
	ES_DRIVE_TO_LIMO,
	ES_DRIVING_TO_LIMO,
	ES_PROTECT,
	ES_RESPOND_TO_PLAYER,
	ES_WARNING_CHECK,
	ES_ALERTED,
	ES_ALERTED2,
	ES_PANIC,
	ES_ATTACKING
ENDENUM
assEscortState EscortState[NUM_ESCORT_PEDS]

//Target State
ENUM assTargetState
	TS_ENTER_LIMO = 0,
	TS_LEAVE_IN_LIMO,
	TS_ALERTED,
	TS_ALERTED2,
	TS_FLEE
ENDENUM
assTargetState TargetState

ENUM assBodyGuardState
	BS_WALK_WITH_TARGET = 0,
	BS_ENTER_VEHICLE,
	BS_RIDE_IN_VEHICLE,
	BS_ALERTED,
	BS_ALERTED2,
	BS_PANIC,
	BS_ATTACKING
ENDENUM
assBodyGuardState BodyGuardState[NUM_BODYGUARDS]

//kill types
ENUM eFailType
	FT_FAILED,
	FT_VEHICLE,
	FT_BLOWN_COVER,
	FT_BLOWN_COVER_WANTED,
	FT_BLOWN_COVER_HIT_PARKED_CAR,
	FT_DISREGARDED_WARNINGS,
	FT_ESCAPED,
	FT_ABANDONED,
	FT_STICKY,
	FT_LESTER_ATTACKED
ENDENUM
eFailType failType = FT_FAILED

// -----------------------------------
// VARIABLES
// -----------------------------------
PED_INDEX Player
PED_INDEX piTarget
PED_INDEX piBodyguard[NUM_BODYGUARDS]
PED_INDEX piLimoGuard[NUM_LIMO_GUARDS]
PED_INDEX piEscortPed[NUM_ESCORT_PEDS]
//PED_INDEX piCutsceneGuard[NUM_LIMO_GUARDS]
PED_INDEX piWalkingGuard
PED_INDEX piLester
ENTITY_INDEX LesterCane

AI_BLIP_STRUCT blipCombatLimoGuards[NUM_LIMO_GUARDS]
AI_BLIP_STRUCT blipCombatEscort[NUM_ESCORT_PEDS]
AI_BLIP_STRUCT blipBodyguard[NUM_BODYGUARDS]

BLIP_INDEX bHotelBlip
BLIP_INDEX bTargetBlip
BLIP_INDEX bLimoGuardBlip[NUM_LIMO_GUARDS]
BLIP_INDEX bEscortBlip[NUM_ESCORT_PEDS]

SCENARIO_BLOCKING_INDEX sbiHotel, sbiHotel01
STREAMVOL_ID svIntroCutscene
BOOL bGuardAttendingTarget = FALSE
BOOL bLoadSceneRequested = FALSE
BOOL bObjectivePrinted
BOOL bPanicking
BOOL bWarned
BOOL bGuardWalkingTowardsPlayer
BOOL bLeftHotel
BOOL bWarningLinePlayed 
BOOL bFailMissionForCloseVehicle
BOOL bHotelTimerStarted
BOOL bHotelTimerExpired
BOOL bReplaying = FALSE
BOOL bMissionFailed = FALSE
BOOL bPlayerInTowTruck = FALSE
//BOOL bLimoDestroyed = FALSE
BOOL bLeaveInLimo
BOOL bLimoStuck
BOOL bDriveToLimo
BOOL bCombatSubtaskSet
BOOL bLimoCruisingStreets
BOOL bTargetDead = FALSE
BOOL bPlayerIsInFlyingVehicle = FALSE
//BOOL bFirstCheckpointSceneLoad = FALSE
//BOOL bSecondCheckpointSceneLoad = FALSE
//BOOL bThirdCheckpointSceneLoad  = FALSE
//BOOL bFourthCheckpointSceneLoad  = FALSE
BOOL bIsPlayerInHotelArea = FALSE
BOOL bReplayVehicleAvailable = FALSE
BOOL bTaxiLeave = FALSE
BOOL bLimoGuardsTaskedToEnterVehicle = FALSE
BOOL bPlayerUsedSniperRifle = FALSE
BOOL bPlayerUsedStickyBomb = FALSE
BOOL bAlerted = FALSE
BOOL bAlertedSpecial = FALSE
BOOL bPlayedTargetLine = FALSE
BOOL bTargetLeaving = FALSE
BOOL bInPursuit = FALSE
//BOOL bDebugSkipping = FALSE
BOOL bPathIsBlocked = FALSE
BOOL bTargetFleeing = FALSE
BOOL bTriggerAlertMusic = FALSE
BOOL bTriggerAlertMessage = FALSE
//BOOL bPlayedCellPhoneCall = FALSE
BOOL bLeftArea = FALSE
//BOOL bGaveSecondWarning = FALSE
//BOOL bDoNotRunPanicCheck = FALSE
BOOL bTriggerGuardWalk = FALSE
BOOL bPanickedExtras = FALSE
BOOL bTaskedGuyToAim = FALSE
BOOL bPlayedSearchDialogue = FALSE
BOOL bPrintLastObjective = FALSE
BOOL bPlayIntialDialogue = FALSE
BOOL bEnterVehicle = FALSE
BOOL bPrepareLostCue = FALSE
BOOL bTriggeredLastCue = FALSE
BOOL bGuardsTellExtrasToLeave = FALSE
BOOL bBufferHit = FALSE
BOOL bTargetPanicLinePlayed = FALSE
BOOL bGuardPanicLinePlayed = FALSE
BOOL bTargetDeadLinePlayed = FALSE
BOOL bGaveAbandondedWarning = FALSE
BOOL bPrintLoseWantedLevel = FALSE
BOOL bTargetKillLinePlayed = FALSE
BOOL bSkippedEndingScreen = FALSE
BOOL bResetAbandonCheck = FALSE
BOOL bPlayMusic = FALSE
BOOL bPlayerTriggeredExplosionFromParkingGarage = FALSE
BOOL bPlayerParkedCarInAlley = FALSE
BOOL bDoFlash = FALSE

INT iLesterUpdateStages = 0
INT i
INT iStageNum
INT iCutsceneStage
INT iNumSafeLocations
INT iGuardAnim[NUM_GUARDS]
INT iTargetAirlockSpawn
INT iHotelTimerStart
//INT iTimeSoFar
//INT iBombCount
INT iFrameCountAsn
INT iExtraCreationStages
INT iExtraUpdateStages
INT iHandleSniperAttackStages
INT iGuardAimingState
INT iStageToUse
INT iGuardTimeToTellExtrasToLeave = 37000
INT iNumVehiclesInArea = 0
INT iNumVehiclesFound = 0
INT iTimeRemaining = 0
INT iTargetPanicLine = 0
INT iEndingScreenStages = 0
INT iLastStoredVehicleIdx = -1
INT iTimeToCut

FLOAT fBombTime = 90000
FLOAT fTargetCarHeading
FLOAT fTargetCarLobbyHead
FLOAT fGuardHead[NUM_GUARDS]
FLOAT fWarnTime[NUM_GUARDS]
FLOAT fEscortCarHead
FLOAT fSightDist
FLOAT fAirlockHead[NUM_AIRLOCK_SPAWN_LOCATIONS]
FLOAT fAirlockSpawnHead[NUM_AIRLOCK_SPAWN_LOCATIONS]
FLOAT fAreaWidth = 135.000
//FLOAT fEscortCarHeading = 256.543091
FLOAT fPanicTime
FLOAT fToHotelDistance = 0

INT iIndexToUse
FLOAT fClosestVector = 0

VECTOR vAirlockPos[NUM_AIRLOCK_SPAWN_LOCATIONS]
VECTOR vAirlockSpawnPos[NUM_AIRLOCK_SPAWN_LOCATIONS]
//VECTOR vHotelStart
VECTOR vGuardPos[NUM_GUARDS]
VECTOR vEscortCarPos
VECTOR vTargetCarPos
VECTOR vTargetCarLobbyPos
VECTOR vAreaCoord01 = << -1162.985, -161.715, -38.221 >>
VECTOR vAreaCoord02 = << -1327.630, -266.202, 38.221 >>
VECTOR vCenterPoint = << -1236.6611, -197.3160, 39.6313 >>
VECTOR vNewBlipPosition = << -1266.02734, -218.98291, 41.44594 >>	//<<-1261.5192, -225.5875, 41.4460>>
//VECTOR vEscortCarPosition = << -1230.39, -182.561, 38.726 >>
VECTOR vTargetLastPosition
VECTOR vHotelCenterPos = << -1237.73145, -189.54518, 40.63728 >>
FLOAT fHotelCenterRadius = 40

VEHICLE_INDEX viTargetCar
VEHICLE_INDEX viEscortCar
VEHICLE_INDEX viCarThatTriggeredAlert
VEHICLE_INDEX vehPlaneVehicle // if the player is in a plane or heli when triggering cutscene
VEHICLE_INDEX viParkedCar[2]
VEHICLE_INDEX viReplayVehicle
VEHICLE_INDEX viLastDriven[3]

SEQUENCE_INDEX seqIndex

MODEL_NAMES TargetModel = A_M_Y_BEACHVESP_02
MODEL_NAMES TargetCarModel = FBI2
MODEL_NAMES GuardModel = S_M_M_HIGHSEC_01
MODEL_NAMES GuardModel_01 = S_M_M_HIGHSEC_02
MODEL_NAMES	EscortCarModel = WASHINGTON
			
CAMERA_INDEX camInit
CAMERA_INDEX camDest
	
REL_GROUP_HASH relGroupGuards

structTimer warnTimer
structTimer tMissionTimer
structTimer tBufferTimer
structTimer tTargetSpeakTimer
structTimer tBlockingTimer
structTimer tExtrasTimer
structTimer tCheckoutTimer
structTimer tFailTimer
structTimer tLesterTimer
//structTimer tAudioCueTimer

structTimer tBeepTimer
BOOL bLoadedSoundSet = FALSE

structPedsForConversation hotelConv
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

SIMPLE_USE_CONTEXT cucEndScreen

VECTOR vCheckpointRetryPosition = <<-1263.6769, -222.2767, 41.4460>> 
FLOAT fCheckpointRetryHeading = 304.2010

// Lead In variables
VECTOR vWaitingPosition = <<-1507.2521, -934.3307, 8.6562>>
FLOAT fWaitingHeading = 137.9228
CAMERA_INDEX camLeadIn01, camLeadIn02

VECTOR vCameraPosition01 = <<-1510.733276,-935.959961,10.902136>>
VECTOR vCameraRotation01 = <<15.074911,-0.024198,-71.861885>>

VECTOR vCameraPosition02 = <<-1510.877563,-936.115479,9.798549>>  
VECTOR vCameraRotation02 = <<4.686822,-0.024198,-72.459717>>
structTimer		tLeadinTimer

OBJECT_INDEX oFailSafeBench

#IF IS_DEBUG_BUILD
	BOOL bDoingPSkip
	BOOL bDoingJSkip
	DEBUG_POS_DATA myDebugData
	
	MissionStageMenuTextStruct sSkipMenu[2]
	INT iDebugJumpStage = 0              
#ENDIF

STRUCT STRUCT_EXTRAS
	PED_INDEX pedTaxiGuys[2]
	PED_INDEX pedTaxiDriver
	VEHICLE_INDEX vehTaxi
	
	PED_INDEX pedSmokers[2]
	PED_INDEX pedTalkers[2]
	
	BOOL bGuysCreated
ENDSTRUCT
STRUCT_EXTRAS stExtras

PROC SETUP_DEBUG()
	#IF IS_DEBUG_BUILD
		sSkipMenu[0].sTxtLabel = "TRAVEL_STAGE"
		sSkipMenu[1].sTxtLabel = "ASSASSINATION_STAGE"   
	#ENDIF
ENDPROC



/// PURPOSE:
///    Checks if the entity exists and is not dead.
/// PARAMS:
///    mEntity - the entity we are checking.
/// RETURNS:
///    True if the entity exists and is not dead.
FUNC BOOL IS_ENTITY_ALIVE(ENTITY_INDEX mEntity)
	IF DOES_ENTITY_EXIST(mEntity)
		IF NOT IS_ENTITY_DEAD(mEntity)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC



PROC CREATE_EXTRAS()

SEQUENCE_INDEX iSeq
	
	SWITCH iExtraCreationStages
		CASE 0
			REQUEST_MODEL(A_M_M_BEVHILLS_01)
			REQUEST_MODEL(A_F_M_BEVHILLS_01)
			REQUEST_MODEL(A_F_M_BEVHILLS_02)
			REQUEST_MODEL(A_M_Y_HIPSTER_01)
			REQUEST_MODEL(TAXI)
			REQUEST_WAYPOINT_RECORDING("OJAS_HotelTaxi01")
			PRINTLN("REQUESTING EXTRAS")
			REQUEST_ANIM_DICT("oddjobs@assassinate@hotel@")
			
			IF HAS_MODEL_LOADED(A_M_M_BEVHILLS_01)
			AND HAS_MODEL_LOADED(A_F_M_BEVHILLS_01)
			AND HAS_MODEL_LOADED(A_F_M_BEVHILLS_02)
			AND HAS_MODEL_LOADED(TAXI)
			AND HAS_MODEL_LOADED(A_M_Y_HIPSTER_01)
			AND HAS_ANIM_DICT_LOADED("oddjobs@assassinate@hotel@")
			AND GET_IS_WAYPOINT_RECORDING_LOADED("OJAS_HotelTaxi01")
				iExtraCreationStages++
				PRINTLN("iExtraCreationStages = ", iExtraCreationStages)
			ELSE
				PRINTLN("WAITING ON EXTRAS")
			ENDIF
		BREAK
		//=======================================================================================================================
		CASE 1
			stExtras.pedTaxiGuys[0] = CREATE_PED(PEDTYPE_CIVMALE, A_F_M_BEVHILLS_01, << -1220.6788, -203.2689, 38.3251 >>, 321.2841)
			stExtras.pedTaxiGuys[1] = CREATE_PED(PEDTYPE_CIVMALE, A_F_M_BEVHILLS_02, << -1219.5959, -201.3663, 38.3251 >>, 110.7199)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(stExtras.pedTaxiGuys[0], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(stExtras.pedTaxiGuys[1], TRUE)
			
			OPEN_SEQUENCE_TASK(iSeq)
				TASK_LOOK_AT_ENTITY(NULL, stExtras.pedTaxiGuys[1], -1)
//				TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@hotel@", "idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
				TASK_START_SCENARIO_AT_POSITION(NULL, "WORLD_HUMAN_TOURIST_MOBILE", << -1220.6788, -203.2689, 38.3251 >>, 321.2841)
			CLOSE_SEQUENCE_TASK(iSeq)
			IF NOT IS_ENTITY_DEAD(stExtras.pedTaxiGuys[0])
				TASK_PERFORM_SEQUENCE(stExtras.pedTaxiGuys[0], iSeq)
			ENDIF
			CLEAR_SEQUENCE_TASK(iSeq)
			
			OPEN_SEQUENCE_TASK(iSeq)
				TASK_LOOK_AT_ENTITY(NULL, stExtras.pedTaxiGuys[0], -1)
				TASK_START_SCENARIO_AT_POSITION(NULL, "WORLD_HUMAN_TOURIST_MAP", << -1219.5959, -201.3663, 38.3251 >>, 110.7199)
			CLOSE_SEQUENCE_TASK(iSeq)
			IF NOT IS_ENTITY_DEAD(stExtras.pedTaxiGuys[1])
				TASK_PERFORM_SEQUENCE(stExtras.pedTaxiGuys[1], iSeq)
			ENDIF
			CLEAR_SEQUENCE_TASK(iSeq)
			
			stExtras.vehTaxi = CREATE_VEHICLE(TAXI, << -1221.7302, -199.0665, 38.1751 >>, 152.5038)
			SET_VEHICLE_ON_GROUND_PROPERLY(stExtras.vehTaxi)
//			SET_VEHICLE_DOORS_LOCKED(stExtras.vehTaxi, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			
			stExtras.pedTaxiDriver = CREATE_PED_INSIDE_VEHICLE(stExtras.vehTaxi, PEDTYPE_CIVMALE, A_M_Y_HIPSTER_01)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(stExtras.pedTaxiDriver, TRUE)
			TASK_LOOK_AT_ENTITY(stExtras.pedTaxiDriver, stExtras.pedTaxiGuys[0], -1)
			
			stExtras.pedTalkers[0] = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_BEVHILLS_01, << -1211.5972, -184.5331, 38.3255 >>, 4.5193)
			stExtras.pedTalkers[1] = CREATE_PED(PEDTYPE_CIVMALE, A_M_Y_HIPSTER_01, << -1211.6544, -182.7625, 38.3255 >>, 166.2532)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(stExtras.pedTalkers[0], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(stExtras.pedTalkers[1], TRUE)
			TASK_PLAY_ANIM(stExtras.pedTalkers[0], "oddjobs@assassinate@hotel@", "idle_a", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(stExtras.pedTalkers[1], "oddjobs@assassinate@hotel@", "argue_b", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
			
			stExtras.pedSmokers[0] = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_BEVHILLS_01, << -1229.0994, -176.3197, 38.3255 >>, 231.8036)
			stExtras.pedSmokers[1] = CREATE_PED(PEDTYPE_CIVMALE, A_F_M_BEVHILLS_02, << -1249.7793, -162.4767, 39.4131 >>, 220.1651)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(stExtras.pedSmokers[0], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(stExtras.pedSmokers[1], TRUE)
			TASK_PLAY_ANIM(stExtras.pedSmokers[0], "oddjobs@assassinate@hotel@","base", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(stExtras.pedSmokers[1], "oddjobs@assassinate@hotel@","base", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
			
			stExtras.bGuysCreated = TRUE
			
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BEVHILLS_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_F_M_BEVHILLS_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_F_M_BEVHILLS_02)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_HIPSTER_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(TAXI)
			
			iExtraCreationStages++
			PRINTLN("iExtraCreationStages = ", iExtraCreationStages)
		BREAK
		//=======================================================================================================================
		CASE 2
			
		BREAK
	ENDSWITCH

	
ENDPROC

PROC REMOVE_PED(PED_INDEX& pedToRemove)
	IF DOES_ENTITY_EXIST(pedToRemove)
		IF IS_ENTITY_OCCLUDED(pedToRemove)
			DELETE_PED(pedToRemove)
			PRINTLN("DELETING PED")
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedToRemove)
			PRINTLN("SETTING PED AS NO LONGER NEEDED")
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_VEHICLE(VEHICLE_INDEX& vehToRemove)
	IF DOES_ENTITY_EXIST(vehToRemove)
		IF IS_ENTITY_OCCLUDED(vehToRemove)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehToRemove)
			PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED")
//		ELSE
//			DELETE_VEHICLE(vehToRemove)
//			PRINTLN("DELETING VEHICLE")
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_EXTRAS()
	REMOVE_VEHICLE(stExtras.vehTaxi)
	REMOVE_PED(stExtras.pedTaxiGuys[0])
	REMOVE_PED(stExtras.pedTaxiGuys[1])
	REMOVE_PED(stExtras.pedTalkers[0])
	REMOVE_PED(stExtras.pedTalkers[1])
	REMOVE_PED(stExtras.pedSmokers[0])
	REMOVE_PED(stExtras.pedSmokers[1])
	REMOVE_PED(stExtras.pedTaxiDriver)
ENDPROC

/// PURPOSE:
///    Called when the mission is failed.
///    Sets the fail reason and tells replay controller to start setting up the replay
///    (showing fail reason, fading out etc)
PROC SET_MISSION_FAILED(eFailType eFail)

	IF 	bMissionFailed = FALSE // (should only do this failed stuff once)
		failType = eFail
	
		KILL_ANY_CONVERSATION()
		CLEAR_PRINTS()
		
		TRIGGER_MUSIC_EVENT("ASS1_FAIL")
		PRINTLN("TRIGGERING MUSIC - ASS1_FAIL - 01")
		
		IF DOES_PLAYER_NEED_SAFE_WARP()
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-1698.5100, -1067.7682, 12.1417>>, 316.3502)
		ENDIF	
		
		
		//to fix bug 1212108 - Starting on foot having driven there in previous attempt- (3/23/13)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			g_replay.iReplayInt[0] = 1			//set this integer to true that we will check later in GO_TO_CHECKPOINT_X
		ELSE
			g_replay.iReplayInt[0] = 0			//reset this value
		ENDIF
		
		
		STRING strFailReason
		SWITCH failType
			CASE FT_FAILED
				strFailReason = "ASS_VA_FAILED"
				PRINTLN("FAIL REASON - FT_FAILED")
			BREAK
			CASE FT_VEHICLE
				strFailReason = "ASS_VA_VEHICLE"
				PRINTLN("FAIL REASON - FT_VEHICLE")
			BREAK
			CASE FT_BLOWN_COVER
			CASE FT_DISREGARDED_WARNINGS
				strFailReason = "ASS_VA_COVER"
				PRINTLN("FAIL REASON - FT_BLOWN_COVER")
			BREAK
			CASE FT_STICKY
				strFailReason = "ASS_VA_COVER"
				PRINTLN("FAIL REASON - FT_STICKY")
			BREAK
			CASE FT_BLOWN_COVER_WANTED
				strFailReason = "ASS_VA_WANTED"
				PRINTLN("FAIL REASON - ASS_VA_WANTED")
			BREAK
			CASE FT_ESCAPED
				strFailReason = "ASS_VA_ESCAPED"
				PRINTLN("FAIL REASON - ASS_VA_ESCAPED")
			BREAK
			CASE FT_ABANDONED
				strFailReason = "ASS_VA_ABAND"
				PRINTLN("FAIL REASON - ASS_VA_ABAND")
			BREAK
			CASE FT_LESTER_ATTACKED
				strFailReason = "ASS_VA_LATTACK"
				PRINTLN("FAIL REASON - ASS_VA_LATTACK")
			BREAK
		ENDSWITCH
		bMissionFailed = TRUE
		ASSASSIN_MISSION_MarkMissionFailed(strFailReason)
		MISSION_FLOW_MISSION_FAILED()
	ENDIF
ENDPROC

FUNC BOOL CHECK_FOR_PLAYER_RAMMING_VEHICLE(VEHICLE_INDEX vehToCheck)
	VEHICLE_INDEX vehTemp
	FLOAT fSpeed
	
	IF DOES_ENTITY_EXIST(vehToCheck)
		IF IS_VEHICLE_DRIVEABLE(vehToCheck)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					
					IF DOES_ENTITY_EXIST(vehTemp)
					AND IS_VEHICLE_DRIVEABLE(vehTemp)
						fSpeed = GET_ENTITY_SPEED(vehTemp)
	//					PRINTLN("PLAYER'S VEHICLE SPEED = ", fSpeed)

						IF fSpeed > 7.0
							IF IS_ENTITY_TOUCHING_ENTITY(vehTemp, vehToCheck)
							OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTemp, vehToCheck)
								
								PRINTLN("CHECK_FOR_PLAYER_RAMMING_VEHICLE RETURNED TRUE!")
								
		//						IF IS_ENTITY_TOUCHING_ENTITY(vehTemp, vehToCheck)
		//							PRINTLN("ENTITY TOUCHING ENTITY CHECK RETURNED TRUE")
		//						ENDIF
		//						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTemp, vehToCheck)
		//							PRINTLN("ENTITY DAMAGED ENTITY CHECK RETURNED TRUE")
		//						ENDIF
						
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL RUN_AGGRO_CHECK_ON_EXTRAS()
	
	IF NOT IS_ENTITY_DEAD(stExtras.pedTaxiDriver)
		IF IS_PED_BEING_JACKED(stExtras.pedTaxiDriver)
			IF GET_PEDS_JACKER(stExtras.pedTaxiDriver) = PLAYER_PED_ID()
				PRINTLN("RETURNING TRUE, PLAYER IS JACKING TAXI DRIVER")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(stExtras.vehTaxi)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), stExtras.vehTaxi)
			PRINTLN("RETURNING TRUE, PLAYER GOT IN TAXI")
			RETURN TRUE
		ENDIF
	ENDIF
	IF DO_AGGRO_CHECK(stExtras.pedTaxiGuys[0], NULL, aggroArgs, aggroReason, FALSE, FALSE, FALSE, TRUE, FALSE)
		PRINTLN("AGGRO CHECK RETURNING TRUE ON stExtras.pedTaxiGuys[0]")
		RETURN TRUE
	ENDIF
	IF DO_AGGRO_CHECK(stExtras.pedTaxiGuys[1], NULL, aggroArgs, aggroReason, FALSE, FALSE, FALSE, TRUE, FALSE)
		PRINTLN("AGGRO CHECK RETURNING TRUE ON stExtras.pedTaxiGuys[1]")
		RETURN TRUE
	ENDIF
	IF DO_AGGRO_CHECK(stExtras.pedTalkers[0], NULL, aggroArgs, aggroReason, FALSE, FALSE, FALSE, TRUE, FALSE)
		PRINTLN("AGGRO CHECK RETURNING TRUE ON stExtras.pedTalkers[0]")
		RETURN TRUE
	ENDIF
	IF DO_AGGRO_CHECK(stExtras.pedTalkers[1], NULL, aggroArgs, aggroReason, FALSE, FALSE, FALSE, TRUE, FALSE)
		PRINTLN("AGGRO CHECK RETURNING TRUE ON stExtras.pedTalkers[1]")
		RETURN TRUE
	ENDIF
	IF DO_AGGRO_CHECK(stExtras.pedSmokers[0], NULL, aggroArgs, aggroReason, FALSE, FALSE, FALSE, TRUE, FALSE)
		PRINTLN("AGGRO CHECK RETURNING TRUE ON stExtras.pedSmokers[0]")
		RETURN TRUE
	ENDIF
	IF DO_AGGRO_CHECK(stExtras.pedSmokers[1], NULL, aggroArgs, aggroReason, FALSE, FALSE, FALSE, TRUE, FALSE)
		PRINTLN("AGGRO CHECK RETURNING TRUE ON stExtras.pedSmokers[1]")
		RETURN TRUE
	ENDIF
	IF CHECK_FOR_PLAYER_RAMMING_VEHICLE(stExtras.vehTaxi)
		PRINTLN("AGGRO CHECK RETURNING TRUE ON stExtras.vehTaxi")
		RETURN TRUE
	ENDIF
	
	//Performance of this?
	IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, <<-1231.3260, -201.7195, 38.2114>>, 20.0)
//	AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_STEAM, <<-1231.3260, -201.7195, 38.2114>>, 20.0)
//	AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_WATER_HYDRANT, <<-1231.3260, -201.7195, 38.2114>>, 20.0)
		PRINTLN("AGGRO CHECK RETURNING TRUE ON UPDATE PED EXTRAS")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_FLEE()
	IF DOES_ENTITY_EXIST(stExtras.pedTaxiDriver) 
	AND NOT IS_ENTITY_DEAD(stExtras.pedTaxiDriver) 
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		TASK_SMART_FLEE_PED(stExtras.pedTaxiDriver, PLAYER_PED_ID(), 1000, -1)
	ENDIF
	IF DOES_ENTITY_EXIST(stExtras.pedTaxiGuys[0]) 
	AND NOT IS_ENTITY_DEAD(stExtras.pedTaxiGuys[0]) 
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		TASK_SMART_FLEE_PED(stExtras.pedTaxiGuys[0], PLAYER_PED_ID(), 1000, -1)
		SET_PED_FLEE_ATTRIBUTES(stExtras.pedTaxiGuys[0], FA_USE_VEHICLE, FALSE)
	ENDIF
	IF DOES_ENTITY_EXIST(stExtras.pedTaxiGuys[1]) 
	AND NOT IS_ENTITY_DEAD(stExtras.pedTaxiGuys[1]) 
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		TASK_SMART_FLEE_PED(stExtras.pedTaxiGuys[1], PLAYER_PED_ID(), 1000, -1)
		SET_PED_FLEE_ATTRIBUTES(stExtras.pedTaxiGuys[1], FA_USE_VEHICLE, FALSE)
	ENDIF
	IF DOES_ENTITY_EXIST(stExtras.pedTalkers[0]) 
	AND NOT IS_ENTITY_DEAD(stExtras.pedTalkers[0]) 
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		TASK_SMART_FLEE_PED(stExtras.pedTalkers[0], PLAYER_PED_ID(), 1000, -1)
	ENDIF
	IF DOES_ENTITY_EXIST(stExtras.pedTalkers[1]) 
	AND NOT IS_ENTITY_DEAD(stExtras.pedTalkers[1]) 
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		TASK_SMART_FLEE_PED(stExtras.pedTalkers[1], PLAYER_PED_ID(), 1000, -1)
	ENDIF
	IF DOES_ENTITY_EXIST(stExtras.pedSmokers[0]) 
	AND NOT IS_ENTITY_DEAD(stExtras.pedSmokers[0]) 
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		TASK_SMART_FLEE_PED(stExtras.pedSmokers[0], PLAYER_PED_ID(), 1000, -1)
	ENDIF
	IF DOES_ENTITY_EXIST(stExtras.pedSmokers[1]) 
	AND NOT IS_ENTITY_DEAD(stExtras.pedSmokers[1]) 
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		TASK_SMART_FLEE_PED(stExtras.pedSmokers[1], PLAYER_PED_ID(), 1000, -1)
	ENDIF
ENDPROC



/// PURPOSE:
///    Loops through LimoGuard, EscortPed, and Bodyguard arrays and returns the closest ped index
/// PARAMS:
///    fDistToCheck - Probe distance around the player
/// RETURNS:
///    
FUNC PED_INDEX GET_CLOSEST_GUARD(FLOAT fDistToCheck = 70.0, BOOL bCheckGuysInVehicles = TRUE)
	INT tempInt
	FLOAT fCurDist
	FLOAT fCloseDist = fDistToCheck
	PED_INDEX closestPed
	
	FOR tempInt = 0 TO NUM_LIMO_GUARDS - 1
		IF NOT IS_PED_INJURED(piLimoGuard[tempInt])
			IF bCheckGuysInVehicles
			OR ( NOT bCheckGuysInVehicles AND NOT IS_PED_IN_ANY_VEHICLE(piLimoGuard[tempInt]) )
				fCurDist = GET_PLAYER_DISTANCE_FROM_ENTITY(piLimoGuard[tempInt], TRUE)
//				PRINTLN("LIMO GUARDS: DISTANCE TO PLAYER FOR INDEX: ", tempInt, " DISTANCE = ", fCurDist)
				IF fCurDist < fCloseDist
//					PRINTLN("BEFORE: fCloseDist = ", fCloseDist)
//					PRINTLN("BEFORE: fCurDist = ", fCurDist)
					fCloseDist = fCurDist
//					PRINTLN("AFTER: fCloseDist = ", fCloseDist)
//					PRINTLN("AFTER: fCurDist = ", fCurDist)
					closestPed = piLimoGuard[tempInt]
//					PRINTLN("CLOSEST piLimoGuard INDEX: ", tempInt)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR tempInt = 0 TO NUM_ESCORT_PEDS - 1
		IF NOT IS_PED_INJURED(piEscortPed[tempInt])
			IF bCheckGuysInVehicles
			OR ( NOT bCheckGuysInVehicles AND NOT IS_PED_IN_ANY_VEHICLE(piEscortPed[tempInt]) )
				fCurDist = GET_PLAYER_DISTANCE_FROM_ENTITY(piEscortPed[tempInt], TRUE)
//				PRINTLN("ESCORT GUARDS: DISTANCE TO PLAYER FOR INDEX: ", tempInt, " DISTANCE = ", fCurDist)
				IF fCurDist < fCloseDist
					fCloseDist = fCurDist
					closestPed = piEscortPed[tempInt]
//					PRINTLN("CLOSEST piEscortPed INDEX: ", tempInt)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR tempInt = 0 TO NUM_BODYGUARDS - 1
		IF NOT IS_PED_INJURED(piBodyguard[tempInt])
			IF bCheckGuysInVehicles
			OR ( NOT bCheckGuysInVehicles AND NOT IS_PED_IN_ANY_VEHICLE(piEscortPed[tempInt]) )
				fCurDist = GET_PLAYER_DISTANCE_FROM_ENTITY(piBodyguard[tempInt], TRUE)
//				PRINTLN("BODYGUARDS: DISTANCE TO PLAYER FOR INDEX: ", tempInt, " DISTANCE = ", fCurDist)
				IF fCurDist < fCloseDist
					fCloseDist = fCurDist
					closestPed = piBodyguard[tempInt]
//					PRINTLN("CLOSEST piBodyguard INDEX: ", tempInt)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN closestPed
ENDFUNC


FUNC PED_INDEX GET_CLOSEST_GUARD_TO_LIMO(FLOAT fDistToCheck)
	INT tempInt
	FOR tempInt = 0 TO NUM_LIMO_GUARDS - 1
		IF NOT IS_PED_INJURED(piLimoGuard[tempInt])
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(piLimoGuard[tempInt], vTargetCarLobbyPos) < fDistToCheck
				RETURN piLimoGuard[tempInt]
			ENDIF
		ENDIF
	ENDFOR
	
	FOR tempInt = 0 TO NUM_ESCORT_PEDS - 1
		IF NOT IS_PED_INJURED(piEscortPed[tempInt])
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(piEscortPed[tempInt], vTargetCarLobbyPos) < fDistToCheck
				RETURN piEscortPed[tempInt]
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN NULL
ENDFUNC


/// PURPOSE:
///    Fill ped struct data for dialogue
PROC SETUP_PED_FOR_DIALOGUE(PED_INDEX ped, INT intPedNumber, STRING strVoiceIdentifier)
	IF NOT IS_PED_INJURED(ped)
		ADD_PED_FOR_DIALOGUE(hotelConv, intPedNumber, ped, strVoiceIdentifier)
	ENDIF
ENDPROC


/// PURPOSE:
///    Gives a ped a task sequence to walk to the front of the hotel and wander
/// PARAMS:
///    pedToTask - ped
///    bOnlyTaskIfNotAlreadyUsingSequence - if TRUE will only give the ped a task to do this is not already using a sequence. Set to FALSE to task regardless.
PROC TASK_WALK_TO_HOTEL_ENTRANCE_AND_WANDER(PED_INDEX pedToTask, BOOL bOnlyTaskIfNotAlreadyUsingSequence = TRUE)
	SEQUENCE_INDEX tempSeq
	
	IF NOT IS_PED_INJURED(pedToTask)
		IF ( bOnlyTaskIfNotAlreadyUsingSequence AND GET_SCRIPT_TASK_STATUS(pedToTask, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK )
		OR NOT bOnlyTaskIfNotAlreadyUsingSequence 
			OPEN_SEQUENCE_TASK(tempSeq)
				TASK_GO_TO_COORD_ANY_MEANS(NULL, << -1214.3433, -132.1107, 40.2416 >>, PEDMOVEBLENDRATIO_WALK, NULL)
				TASK_WANDER_STANDARD(NULL)
			CLOSE_SEQUENCE_TASK(tempSeq)
			TASK_PERFORM_SEQUENCE(pedToTask, tempSeq)
			CLEAR_SEQUENCE_TASK(tempSeq)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Checks to see if a ped is running a ragdoll task and is not already tasked with TASK_WALK_TO_HOTEL_ENTRANCE_AND_WANDER and tasks them with it if not
/// PARAMS:
///    pedToTask - ped to check/task
///    bForce - defaults to FALSE. Set the TRUE if you want to explicitly give the task without the ped having to ragdoll first
PROC HANDLE_PLAYER_APPROACHING_EXTRA(PED_INDEX pedToTask, BOOL bForce = FALSE)
	IF NOT IS_PED_INJURED(pedToTask)
		IF GET_SCRIPT_TASK_STATUS(pedToTask, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(pedToTask, FALSE) < 1.0
			OR bForce
				TASK_WALK_TO_HOTEL_ENTRANCE_AND_WANDER(pedToTask)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    State machine for controlling teh bahvior of the peds that stand outside the taxi and the taxi driver
PROC UPDATE_TAXI_EXTRAS()
	PED_INDEX tempGuard
	
	SWITCH iExtraUpdateStages
		CASE 0
			IF bTaxiLeave
			OR bWarningLinePlayed
			OR GET_PLAYER_DISTANCE_FROM_ENTITY(stExtras.pedTaxiGuys[0], FALSE) < 1.0
			OR GET_PLAYER_DISTANCE_FROM_ENTITY(stExtras.pedTaxiGuys[1], FALSE) < 1.0
				IF NOT IS_ENTITY_DEAD(stExtras.pedTaxiGuys[0]) AND NOT IS_ENTITY_DEAD(stExtras.pedTaxiGuys[1])
					TASK_LOOK_AT_ENTITY(stExtras.pedTaxiGuys[0], stExtras.pedTaxiGuys[1], -1)
					TASK_LOOK_AT_ENTITY(stExtras.pedTaxiGuys[1], stExtras.pedTaxiGuys[0], -1)
				ENDIF
				
				IF DOES_ENTITY_EXIST(stExtras.pedTaxiGuys[1]) 
				AND DOES_ENTITY_EXIST(stExtras.vehTaxi) 
				AND NOT IS_PED_INJURED(stExtras.pedTaxiGuys[1]) 
				AND IS_VEHICLE_DRIVEABLE(stExtras.vehTaxi)
					CLEAR_PED_TASKS(stExtras.pedTaxiGuys[1])
					TASK_ENTER_VEHICLE(stExtras.pedTaxiGuys[1], stExtras.vehTaxi, DEFAULT_TIME_BEFORE_WARP, VS_BACK_RIGHT, PEDMOVEBLENDRATIO_WALK)
					PRINTLN("UPDATE_EXTRAS: TASKING GUYS TO ENTER VEHICLE - 02")
				ENDIF
				
				IF NOT IS_TIMER_STARTED(tExtrasTimer)
					START_TIMER_NOW(tExtrasTimer)
					PRINTLN("STARTING TIMER - tExtrasTimer")
				ENDIF
				
				iExtraUpdateStages++
				PRINTLN("iExtraUpdateStages = ", iExtraUpdateStages)
			ENDIF
		BREAK
		//=======================================================================================================================
		CASE 1	
			IF NOT bEnterVehicle
				IF IS_TIMER_STARTED(tExtrasTimer)
					IF GET_TIMER_IN_SECONDS(tExtrasTimer) > 3.0
						IF DOES_ENTITY_EXIST(stExtras.pedTaxiGuys[0])
						AND DOES_ENTITY_EXIST(stExtras.vehTaxi) 
						AND NOT IS_PED_INJURED(stExtras.pedTaxiGuys[0])
						AND IS_VEHICLE_DRIVEABLE(stExtras.vehTaxi)
							IF GET_SCRIPT_TASK_STATUS(stExtras.pedTaxiGuys[0], SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
								CLEAR_PED_TASKS(stExtras.pedTaxiGuys[0])
								TASK_ENTER_VEHICLE(stExtras.pedTaxiGuys[0], stExtras.vehTaxi, DEFAULT_TIME_BEFORE_WARP, VS_BACK_LEFT, PEDMOVEBLENDRATIO_WALK)
								PRINTLN("UPDATE_EXTRAS: TASKING GUYS TO ENTER VEHICLE - 01")
								bEnterVehicle = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					START_TIMER_NOW(tExtrasTimer)
				ENDIF
			ENDIF
		
			IF NOT IS_PED_INJURED(stExtras.pedTaxiGuys[0]) AND NOT IS_PED_INJURED(stExtras.pedTaxiGuys[1])
				IF IS_PED_IN_VEHICLE(stExtras.pedTaxiGuys[0], stExtras.vehTaxi)
				AND IS_PED_IN_VEHICLE(stExtras.pedTaxiGuys[1], stExtras.vehTaxi)
					IF NOT IS_ENTITY_DEAD(stExtras.pedTaxiDriver) AND NOT IS_ENTITY_DEAD(stExtras.vehTaxi)
						TASK_CLEAR_LOOK_AT(stExtras.pedTaxiDriver)
						RESTART_TIMER_NOW(tExtrasTimer)
					ENDIF
				
					iExtraUpdateStages++
					PRINTLN("iExtraUpdateStages = ", iExtraUpdateStages)
				ENDIF
			ENDIF
		BREAK
		//=======================================================================================================================
		CASE 2
			IF IS_TIMER_STARTED(tExtrasTimer)
				IF GET_TIMER_IN_SECONDS(tExtrasTimer) > 3.0
					IF NOT IS_PED_INJURED(stExtras.pedTaxiDriver)
					AND IS_VEHICLE_DRIVEABLE(stExtras.vehTaxi)
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(stExtras.pedTaxiDriver, stExtras.vehTaxi,
								"OJAS_HotelTaxi01", DRIVINGMODE_AVOIDCARS | DF_SteerAroundPeds, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 10)
					
						iExtraUpdateStages++
						PRINTLN("iExtraUpdateStages = ", iExtraUpdateStages)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//=======================================================================================================================
		CASE 3
			IF NOT IS_ENTITY_DEAD(stExtras.pedTaxiDriver) AND NOT IS_ENTITY_DEAD(stExtras.vehTaxi)
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(stExtras.vehTaxi)
				OR bGuardsTellExtrasToLeave
					TASK_VEHICLE_DRIVE_WANDER(stExtras.pedTaxiDriver, stExtras.vehTaxi, 10.0, DRIVINGMODE_STOPFORCARS)
					PRINTLN("TASKING TO WANDER")
					
					iExtraUpdateStages++
					PRINTLN("iExtraUpdateStages = ", iExtraUpdateStages)
				ENDIF
			ENDIF
		BREAK
		//=======================================================================================================================
		CASE 4			
			IF bGuardsTellExtrasToLeave
				IF NOT IS_PED_INJURED(piEscortPed[0])
					tempGuard = piEscortPed[0]
				ELSE
					tempGuard = GET_CLOSEST_GUARD_TO_LIMO(25.0)
				ENDIF
					
				IF DOES_ENTITY_EXIST(tempGuard)
					IF NOT IS_PED_INJURED(tempGuard)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							SETUP_PED_FOR_DIALOGUE(tempGuard, 5, "OJAvaGUARD")
							CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_LEAVE", CONV_PRIORITY_VERY_HIGH)		//tells extras to leave
						ENDIF
					ENDIF
				ENDIF
				
				RESTART_TIMER_NOW(tExtrasTimer)
				iExtraUpdateStages++
				PRINTLN("iExtraUpdateStages = ", iExtraUpdateStages)
			ENDIF
		BREAK
		//=======================================================================================================================
		CASE 5
			//empty - already tasked
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Handles updating of talker and smoker extra peds
PROC UPDATE_NON_TAXI_EXTRAS()
	IF bHotelTimerStarted
		IF iTimeRemaining < (iGuardTimeToTellExtrasToLeave - 5.0)
			// TALKER PEDS - 2
			TASK_WALK_TO_HOTEL_ENTRANCE_AND_WANDER(stExtras.pedTalkers[0])
			TASK_WALK_TO_HOTEL_ENTRANCE_AND_WANDER(stExtras.pedTalkers[1])
			
			// SMOKING PEDS - 2
			TASK_WALK_TO_HOTEL_ENTRANCE_AND_WANDER(stExtras.pedSmokers[0])
			TASK_WALK_TO_HOTEL_ENTRANCE_AND_WANDER(stExtras.pedSmokers[1])
		ELSE
			//if either one of these peds are approached, we want to force them both to walk away
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(stExtras.pedTalkers[0], FALSE) < 1.0
			OR GET_PLAYER_DISTANCE_FROM_ENTITY(stExtras.pedTalkers[1], FALSE) < 1.0
			OR bWarningLinePlayed
				HANDLE_PLAYER_APPROACHING_EXTRA(stExtras.pedTalkers[0], TRUE)
				HANDLE_PLAYER_APPROACHING_EXTRA(stExtras.pedTalkers[1], TRUE)
			ENDIF
			
			IF NOT bWarningLinePlayed
				HANDLE_PLAYER_APPROACHING_EXTRA(stExtras.pedSmokers[0])
				HANDLE_PLAYER_APPROACHING_EXTRA(stExtras.pedSmokers[1])
			ELSE
				HANDLE_PLAYER_APPROACHING_EXTRA(stExtras.pedSmokers[0], TRUE)
				HANDLE_PLAYER_APPROACHING_EXTRA(stExtras.pedSmokers[1], TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Main update loop for the extra peds
PROC UPDATE_EXTRAS()
	PED_INDEX pedSpeaker
	VECTOR vSpeakerPos
		
	IF stExtras.bGuysCreated
		
		IF NOT bPanickedExtras
			IF MainStage < STAGE_GUARDS_GO_TO_POSTS
				IF RUN_AGGRO_CHECK_ON_EXTRAS()
					
					pedSpeaker = GET_CLOSEST_GUARD()
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedSpeaker)
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(pedSpeaker, FALSE) < 10
							vSpeakerPos = GET_ENTITY_COORDS(pedSpeaker)
							vSpeakerPos = vSpeakerPos
							PRINTLN("pedSpeaker located at << ", vSpeakerPos.x, ", ", vSpeakerPos.y, ", ", vSpeakerPos.z, " >>.")
							ADD_PED_FOR_DIALOGUE(hotelConv, 3, pedSpeaker, "OJAvaGuard")
							CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_AT_EX", CONV_PRIORITY_VERY_HIGH)
						ENDIF
					ENDIF
					
					HANDLE_FLEE()
					
					PRINTLN("bPanickedExtras = TRUE - 01")
					bPanickedExtras = TRUE
				ENDIF
				
				IF bPanicking
					HANDLE_FLEE()
					
					PRINTLN("bPanickedExtras = TRUE - 02")
					bPanickedExtras = TRUE
				ELSE
					UPDATE_TAXI_EXTRAS()
					
					UPDATE_NON_TAXI_EXTRAS()
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
ENDPROC


PROC SETUP_UI_LABELS()
	endLabel.tlMissionPassed = "ASS_VA_PASSED"
	endLabel.tlMissionName = g_sMissionStatsName
	endLabel.tlTimeTaken = "ASS_VA_TIMER"
	endLabel.tlBaseReward = "ASS_VA_BASE"
	endLabel.tlBonusDesc = "ASS_VA_BDESC"
	endLabel.tlCashEarned = "ASS_VA_CASH"
	endLabel.tlCompletionGold = "ASS_VA_COMP"//"ASS_VA_COMPG"
	endLabel.tlCompletionSilver = "ASS_VA_COMP"//"ASS_VA_COMPS"
	endLabel.tlCompletionBronze = "ASS_VA_COMP"//"ASS_VA_COMPB"
	endLabel.tlCompletionNoMedal = "ASS_VA_COMP"//"ASS_VA_COMPN"
ENDPROC


//set bPanicking to TRUE and set fail type
PROC FAIL_MISSION_FOR_CLOSE_VEHICLE()
//	bPanicking = TRUE
	failType = FT_VEHICLE
	bFailMissionForCloseVehicle = TRUE
	PRINTLN("OJASHOTEL - bFailMissionForCloseVehicle = TRUE")
ENDPROC

FUNC BOOL IS_PLAYER_IN_AIR_VEHICLE()
	MODEL_NAMES mnAirVehicle
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()) OR IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
		vehPlaneVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		mnAirVehicle = GET_ENTITY_MODEL(vehPlaneVehicle)
		
		IF mnAirVehicle != BLIMP
		AND mnAirVehicle != blimp2
			bPlayerIsInFlyingVehicle = TRUE
			PRINTLN("PLAYER IS IN A PLANE OR HELICOPTER")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MOVE_PLAYER_AND_PLANE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1264.0720, -223.5235, 59.6540 >>)
//		SET_ENTITY_HEADING(PLAYER_PED_ID(), 313.420)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vNewBlipPosition)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 299.4985)
		PRINTLN("MOVING PLAYER TO CUTSCENE BLIP POSITION FROM MOVE_PLAYER_AND_PLANE")
	ENDIF
	IF DOES_ENTITY_EXIST(vehPlaneVehicle)
		IF IS_VEHICLE_DRIVEABLE(vehPlaneVehicle)
			SET_VEHICLE_ENGINE_ON(vehPlaneVehicle, FALSE, TRUE)
			SET_ENTITY_COORDS(vehPlaneVehicle, <<-1251.8732, -256.7999, 38.2695>>)
			SET_ENTITY_HEADING(vehPlaneVehicle, 296.7603)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPlaneVehicle)
			PRINTLN("MOVING PLANE TO POSITION IN WIDE ALLEY ON GROUND")
		ENDIF
	ENDIF
ENDPROC


PROC REPOSITION_PLAYER_AND_VEHICLE_FOR_GUARD_CUTSCENE()
	
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1252.462769,-227.589386,39.032867>>, <<-1267.238770,-207.992447,44.673515>>, 17.000000, <<-1266.4049, -219.1991, 41.4459>>, 304.8644, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
	
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1266.1729, -214.0011, 41.4459>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 310.0126)
		PRINTLN("MOVING PLAYER TO CUTSCENE BLIP POSITION FROM MOVE_PLAYER_AND_PLANE")
	ENDIF
ENDPROC

//check if player is withing range of triggering cutscene
FUNC BOOL IS_PLAYER_IN_RANGE_OF_HOTEL()
	FLOAT fDistanceToStart
	BOOL bTriggerdInFlyingVehicle = FALSE
	
	fDistanceToStart = GET_ENTITY_DISTANCE_FROM_LOCATION(Player, vNewBlipPosition)
	
	IF fDistanceToStart < 200
		CREATE_EXTRAS()
	ENDIF
	
	IF IS_PLAYER_IN_AIR_VEHICLE()
		IF IS_ENTITY_AT_COORD(Player, vNewBlipPosition, <<25,25,90>>, TRUE)
			bTriggerdInFlyingVehicle = TRUE
			PRINTLN("bTriggerdInFlyingVehicle = TRUE")
		ENDIF
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		IF fDistanceToStart < 2
		OR IS_ENTITY_AT_COORD(Player, vNewBlipPosition, <<2,2,LOCATE_SIZE_HEIGHT>>, TRUE)
		OR bTriggerdInFlyingVehicle
			
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1141.085, -192.660, 75.756 >>, << -1246.085, -192.660, 100.756 >>, 115, FALSE, TRUE)
				// If the player is in a helicopter or plane and is too close, fail
				IF IS_PLAYER_IN_AIR_VEHICLE() AND bTriggerdInFlyingVehicle
					PRINTLN("RETURNING TRUE ON IS_PLAYER_IN_RANGE_OF_HOTEL - IN AIR VEHICLE")
					RETURN TRUE
				ELSE
					IF NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
						PRINTLN("RETURNING TRUE ON IS_PLAYER_IN_RANGE_OF_HOTEL")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Release all peds
PROC CLEANUP_PEDS()
	INT idx
	REPEAT NUM_BODYGUARDS idx
		IF DOES_ENTITY_EXIST(piBodyguard[idx])
			IF NOT IS_PED_INJURED(piBodyguard[idx])
				CLEANUP_AI_PED_BLIP(blipBodyguard[idx])
			
				CLEAR_PED_TASKS(piBodyguard[idx])
				SET_PED_AS_NO_LONGER_NEEDED(piBodyguard[idx])
				PRINTLN("SETTING PED AS NO LONGER NEEDED - piBodyguard[", idx, "].")
			ENDIF
		ENDIF
	ENDREPEAT

	IF DOES_ENTITY_EXIST(piTarget)
		IF NOT IS_PED_INJURED(piTarget)
			CLEAR_PED_TASKS(piTarget)
			SET_PED_AS_NO_LONGER_NEEDED(piTarget)
			PRINTLN("SETTING PED AS NO LONGER NEEDED - piTarget")
		ENDIF
	ENDIF

	REPEAT NUM_LIMO_GUARDS idx
		IF DOES_ENTITY_EXIST(piLimoGuard[idx])
			IF NOT IS_PED_INJURED(piLimoGuard[idx])
				CLEANUP_AI_PED_BLIP(blipCombatLimoGuards[idx])
			
				CLEAR_PED_TASKS(piLimoGuard[idx])
				SET_PED_AS_NO_LONGER_NEEDED(piLimoGuard[idx])
				PRINTLN("SETTING PED AS NO LONGER NEEDED - piLimoGuard")
			ENDIF
		ENDIF
	ENDREPEAT
		
	REPEAT NUM_ESCORT_PEDS idx
		IF DOES_ENTITY_EXIST(piEscortPed[idx])
			IF NOT IS_PED_INJURED(piEscortPed[idx])
				CLEANUP_AI_PED_BLIP(blipCombatEscort[idx])
			
				CLEAR_PED_TASKS(piEscortPed[idx])
				SET_PED_AS_NO_LONGER_NEEDED(piEscortPed[idx])
				PRINTLN("SETTING PED AS NO LONGER NEEDED - piEscortPed[", idx, "].")
			ENDIF
		ENDIF
	ENDREPEAT
	
	SET_MODEL_AS_NO_LONGER_NEEDED(TargetModel)
ENDPROC

/// PURPOSE:
///    Setup initial camera parameters for intro cutscene
PROC CREATE_CAMERAS()
	camInit = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1244.3435, -251.5660, 50.3194>>, <<-9.2001, 0.0498, 6.3064>>, 38.000000)
	camDest = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1243.5876, -251.4826, 50.3187>>, <<-9.2001, 0.0498, 6.3064>>, 38.000000)
ENDPROC

/// PURPOSE:
///    Cleanup all enemy ped blips
PROC CLEANUP_PED_BLIPS(BOOL bIncludeTarget = TRUE)
	INT idx
	
	IF bIncludeTarget
		IF DOES_BLIP_EXIST(bTargetBlip)
			REMOVE_BLIP(bTargetBlip)
		ENDIF
	ENDIF
	
	REPEAT NUM_LIMO_GUARDS idx
		IF DOES_BLIP_EXIST(bLimoGuardBlip[idx])
			REMOVE_BLIP(bLimoGuardBlip[idx])
		ENDIF
	ENDREPEAT
	
	REPEAT NUM_ESCORT_PEDS idx
		IF DOES_BLIP_EXIST(bEscortBlip[idx])
			REMOVE_BLIP(bEscortBlip[idx])
		ENDIF
	ENDREPEAT
	
	REPEAT NUM_BODYGUARDS idx
		IF DOES_BLIP_EXIST(bEscortBlip[idx])
			REMOVE_BLIP(bEscortBlip[idx])
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    cleanup all COORD blips
PROC CLEANUP_DEST_BLIPS()
	IF DOES_BLIP_EXIST(bHotelBlip)
		REMOVE_BLIP(bHotelBlip)
	ENDIF
ENDPROC



/// PURPOSE:
///    Release all vehicles
PROC CLEANUP_VEHICLES()	
	IF DOES_ENTITY_EXIST(viTargetCar)
		IF IS_VEHICLE_DRIVEABLE(viTargetCar)
			SET_VEHICLE_DOORS_LOCKED(viTargetCar, VEHICLELOCK_UNLOCKED)
		ENDIF

		SET_VEHICLE_AS_NO_LONGER_NEEDED(viTargetCar)
		PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED: viTargetCar")
	ENDIF
	
	IF DOES_ENTITY_EXIST(viEscortCar)
		IF NOT IS_ENTITY_DEAD(viEscortCar)
			IF IS_ENTITY_ON_SCREEN(viEscortCar) AND NOT IS_ENTITY_OCCLUDED(viEscortCar)
			AND NOT IS_SCREEN_FADED_OUT()
				SET_VEHICLE_AS_NO_LONGER_NEEDED(viEscortCar)
				PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED: viEscortCar")
			ELSE
				IF DOES_ENTITY_EXIST(piEscortPed[0]) AND NOT IS_ENTITY_DEAD(piEscortPed[0])
					IF NOT IS_ENTITY_DEAD(viEscortCar)
						IF IS_PED_IN_VEHICLE(piEscortPed[0], viEscortCar)
							CLEAR_PED_TASKS_IMMEDIATELY(piEscortPed[0])
							PRINTLN("DETACHING PED FROM WITHIN VEHICLE - piEscortPed[0]")
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(piEscortPed[1]) AND NOT IS_ENTITY_DEAD(piEscortPed[1])
					IF NOT IS_ENTITY_DEAD(viEscortCar)
						IF IS_PED_IN_VEHICLE(piEscortPed[1], viEscortCar)
							CLEAR_PED_TASKS_IMMEDIATELY(piEscortPed[1])
							PRINTLN("DETACHING PED FROM WITHIN VEHICLE - piEscortPed[1]")
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viEscortCar)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						PRINTLN("PLAYER IS IN THE ESCORT CAR - CLEARING PED TASKS IMMEDIATELY")
					ENDIF
				ENDIF
				
				
				SET_VEHICLE_AS_NO_LONGER_NEEDED(viEscortCar)
				PRINTLN("SETTING VEHICLE: viEscortCar as no longer needed")
//				DELETE_VEHICLE(viEscortCar)
//				PRINTLN("DELETING VEHICLE: viEscortCar")
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(viParkedCar[0])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(viParkedCar[0])
	ENDIF
	IF DOES_ENTITY_EXIST(viParkedCar[1])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(viParkedCar[1])
	ENDIF
	
	IF DOES_ENTITY_EXIST(viReplayVehicle) AND NOT IS_ENTITY_DEAD(viReplayVehicle)
		IF IS_ENTITY_A_MISSION_ENTITY(viReplayVehicle) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(viReplayVehicle)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(viReplayVehicle)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Release all vehicle recording requests
PROC CLEANUP_VEH_RECORDINGS()
	REMOVE_VEHICLE_RECORDING(102, "ASSOJva")
	REMOVE_VEHICLE_RECORDING(107, "ASSOJva")
	REMOVE_WAYPOINT_RECORDING("OJASva_101")
	REMOVE_WAYPOINT_RECORDING("OJASva_101a")
	REMOVE_WAYPOINT_RECORDING("OJASva_104")
ENDPROC


/// PURPOSE:
///    define all the variables
PROC INITIALIZE_VARIABLES()
	
	//important vectors used around the hotel location
//	vHotelStart = << -1226.4880, -185.6017, 38.1758 >> 			// <<-1243.275, -194.76, 39.155>>  //hotel circle area
	vEscortCarPos = <<-1343.4927, -153.1990, 47.1825>>			//location where the motorcade vehicle is spawned
	fEscortCarHead = 260.3612									//motorcade car heading
	vTargetCarPos = <<-1242.2126, -241.5975, 38.7100>>			//spawn location of limo vehicle
	fTargetCarHeading = 96.8755									//limo car spawn heading
	vTargetCarLobbyPos = <<-1221.631348, -186.881943, 38.799603 >>	//location where the limo parks outside of the lobby doors
	fTargetCarLobbyHead = 201.076294								//heading of the limo when it is parked outside the lobby doors
	fSightDist = 40												//distance that a guard will detect the player (if they have proper LOS)
	aggroArgs.fAimRange = 40
	
  	// USE FOR 3 AIR LOCK POSITIONS
	vAirlockPos[0] = << -1221.1538, -170.9907, 38.3253 >> 		//left airlock
	vAirlockPos[1] = << -1210.5562, -192.8820, 38.3253 >>		//center airlock
	vAirlockPos[2] = << -1216.3691, -203.6806, 38.3253 >>		//right airlock
	
	fAirlockHead[0] = 157.3177
	fAirlockHead[1] = 58.3594
	fAirlockHead[2] = 65.6443
	
	// 2 AIR LOCK POSITIONS
//	vAirlockPos[0] = << -1210.5562, -192.8820, 38.3253 >>		//center airlock
//	vAirlockPos[1] = << -1216.3691, -203.6806, 38.3253 >>		//right airlock
//	
//	fAirlockHead[0] = 58.3594
//	fAirlockHead[1] = 65.6443
	
	//guard post positions / headings
	vGuardPos[0] = << -1220.3773, -169.4070, 38.3253  >> 	//left airlock
	vGuardPos[1] = << -1208.9630, -193.8791, 38.3253 >>		//center airlock
	vGuardPos[2] = << -1214.9547, -205.0824, 38.3253 >>		//right airlock	
	vGuardPos[3] = << -1217.5182, -197.1300, 38.3254 >>		//outside lobby doors (left)
	vGuardPos[4] = << -1231.8916, -194.3820, 38.1753 >>		//near fountain
	fGuardHead[0] = 160.0748
	fGuardHead[1] = 58.3365
	fGuardHead[2] = 58.0214
	fGuardHead[3] = 97.6664
	fGuardHead[4] = 225.8689
	
	//ped reaction time - randomize these times so that ped's don't all play their alert behavior at the same frame if they detect the player
	FOR i = 0 TO 4
		fWarnTime[i] = GET_RANDOM_FLOAT_IN_RANGE(0, 2)
	ENDFOR
	
	//mission rewards
	fBaseReward = 7000
	fBonusReward = 2000
	
	failType = FT_FAILED
	bGuardPanicLinePlayed = FALSE
	bTargetDeadLinePlayed = FALSE
	bTriggerAlertMessage = FALSE
	bPlayerParkedCarInAlley = FALSE
	bPlayerTriggeredExplosionFromParkingGarage = FALSE
ENDPROC

PROC SETUP_MISSION_PARAMS()
	VECTOR vOutsideHotel = <<-1220.3440, -193.4014, 38.1754>>
	VECTOR vParkingGarage = <<-1279.6578, -222.0533, 41.4460>>
	
	ADD_PED_FOR_DIALOGUE(hotelConv, 1, Player, "FRANKLIN")
	
	SET_WANTED_LEVEL_MULTIPLIER(0.2)
	
	ADD_RELATIONSHIP_GROUP("Guards", relGroupGuards)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, relGroupGuards)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupGuards, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupGuards, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupGuards)
	
	//clear the area around the hotel of peds
//	CLEAR_AREA_OF_PEDS(<<-1220.57, -185.96, 38.40>>, 200)
	CLEAR_AREA(<<-1220.57, -185.96, 38.40>>, 200, TRUE)
	SET_PED_NON_CREATION_AREA(<< -1298.6843, -324.0442, -35.5780 >>, << -1113.3724, -46.1583, 55.6090 >>)
	
	sbiHotel = ADD_SCENARIO_BLOCKING_AREA( (vOutsideHotel - << 15, 15, 15 >>), (vOutsideHotel + << 15, 15, 15 >>) ) // Removing peds
	sbiHotel01 = ADD_SCENARIO_BLOCKING_AREA( (vParkingGarage - << 35, 35, 45 >>), (vParkingGarage + << 35, 35, 45 >>) ) // Parking garage
ENDPROC

/// PURPOSE:
///    do all streaming requests
PROC REQUEST_STREAMS()

	REQUEST_MODEL(TargetCarModel)
	REQUEST_MODEL(EscortCarModel)
//	REQUEST_MODEL(GuardModel)
//	REQUEST_MODEL(GuardModel_01)
//	REQUEST_MODEL(TargetModel)
	REQUEST_ANIM_DICT("ODDJOBS@ASSASSINATE@GUARD")
	REQUEST_ANIM_DICT("oddjobs@assassinate@hotel@")
	REQUEST_ADDITIONAL_TEXT("ASS_VA", MINIGAME_TEXT_SLOT)
		
	//limo pulling up to the hotel
	REQUEST_VEHICLE_RECORDING(102, "ASSOJva")
	
	//limo driving into the hotel
	REQUEST_VEHICLE_RECORDING(107, "ASSOJva")
	
	//leaving the hotel
	REQUEST_WAYPOINT_RECORDING("OJASva_101")
	REQUEST_WAYPOINT_RECORDING("OJASva_101a")
	
	//motorcade pulling up to the limo
	REQUEST_WAYPOINT_RECORDING("OJASva_104")
	
ENDPROC

/// PURPOSE:
///    Returns TRUE when all streaming requests are complete
FUNC BOOL VEHICLES_RECORDINGS_AND_ANIMS_LOADED()
	BOOL returnBool = FALSE			
		IF HAS_MODEL_LOADED(TargetCarModel)
		AND HAS_MODEL_LOADED(EscortCarModel)
//		AND HAS_MODEL_LOADED(GuardModel)
//		AND HAS_MODEL_LOADED(GuardModel_01)
//		AND HAS_MODEL_LOADED(TargetModel)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(102, "ASSOJva")  //limo pulling up to the hotel lobby
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(107, "ASSOJva")  //limo driving up to the hotel
		AND GET_IS_WAYPOINT_RECORDING_LOADED("OJASva_101")
		AND GET_IS_WAYPOINT_RECORDING_LOADED("OJASva_101a")
		AND GET_IS_WAYPOINT_RECORDING_LOADED("OJASva_104")
		AND HAS_ANIM_DICT_LOADED("ODDJOBS@ASSASSINATE@GUARD")
		AND HAS_ANIM_DICT_LOADED("oddjobs@assassinate@hotel@")
		AND HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
			PRINTLN("OJASHOTEL - Streams loaded")
			returnBool = TRUE
		ELSE
			PRINTLN("OJASHOTEL - Loading Streams")
		ENDIF
	
	RETURN returnBool
ENDFUNC

PROC REQUEST_GUARD_MODELS()
	REQUEST_MODEL(GuardModel)
	REQUEST_MODEL(GuardModel_01)
ENDPROC

FUNC BOOL ARE_MODELS_LOADED_FOR_GUARDS()
	BOOL bLoaded
	REQUEST_GUARD_MODELS()
	
	IF HAS_MODEL_LOADED(GuardModel)
	AND HAS_MODEL_LOADED(GuardModel_01)
		PRINTLN("MODELS_LOADED_FOR_GUARDS")
		bLoaded = TRUE
	ELSE
		bLoaded = FALSE
	ENDIF
	
	RETURN bLoaded
ENDFUNC


FUNC BOOL ARE_MODELS_LOADED_FOR_TARGET()
	BOOL bLoaded
	
	REQUEST_MODEL(TargetModel)
	
	IF HAS_MODEL_LOADED(TargetModel)
		PRINTLN("MODELS_LOADED_FOR_TARGET")
		bLoaded = TRUE
	ELSE
		bLoaded = FALSE
	ENDIF
	
	RETURN bLoaded
ENDFUNC

PROC UNLOAD_GUARD_MODELS()
	SET_MODEL_AS_NO_LONGER_NEEDED(GuardModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(GuardModel_01)
ENDPROC

PROC UNLOAD_TARGET_MODEL()
	SET_MODEL_AS_NO_LONGER_NEEDED(TargetModel)
ENDPROC


/// PURPOSE:
///    Setup ped attributes for ped
PROC SET_PED_PARAMS(PED_INDEX ped, BOOL bAggressive = TRUE)
	IF ped <> piTarget
		GIVE_WEAPON_TO_PED(ped, WEAPONTYPE_PISTOL, INFINITE_AMMO)  
	ENDIF
	IF NOT bAggressive
		SET_PED_COMBAT_MOVEMENT(ped, CM_DEFENSIVE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_USE_COVER, TRUE)
	ELSE
		SET_PED_COMBAT_MOVEMENT(ped, CM_WILLADVANCE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_AGGRESSIVE, TRUE)
	ENDIF
	
	SET_PED_ACCURACY(ped, 60)
	SET_PED_COMBAT_ABILITY(ped, CAL_PROFESSIONAL)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
	SET_PED_AS_ENEMY(ped, TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(ped, relGroupGuards)
ENDPROC

/// PURPOSE:
///    Set traffic params around the htoel while the mission is running
PROC SET_LOW_DENSITY_HOTEL_TRAFFIC(BOOL bSet)
	IF bSet
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA((<<-1237.3966, -193.9340, 39.1642>> - <<150,150,150>>), (<<-1237.3966, -193.9340, 39.1642>> + <<150,150,150>>), FALSE)
//		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1272.1578, -207.0374, -100.3460 >>, << -1317.5679, -285.4805, 100.6734 >>, TRUE)
		SET_ROADS_IN_AREA(<< -1349.1761, -39.0123, -100.7554 >>, << -1173.2150, -297.7677, 100.8606 >>, FALSE)
		PRINTLN("CALLING OFF GENERATORS")
	ELSE
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1340.9053, -68.1138, -100.7554 >>, << -1176.6222, -287.8646, 100.8606 >>, TRUE)
//		SET_ROADS_IN_AREA(<< -1349.1761, -39.0123, -100.7554 >>, << -1173.2150, -297.7677, 100.8606 >>, TRUE)
		SET_ROADS_BACK_TO_ORIGINAL(<< -1349.1761, -39.0123, -100.7554 >>, << -1173.2150, -297.7677, 100.8606 >>)
	ENDIF
ENDPROC


/// PURPOSE:
///    Grab a random animation string (defaults to grabbing an idle animation)
PROC PLAY_RANDOM_ANIM(PED_INDEX ped, INT& iAnimToExclude)
	INT tempInt
	tempInt = SCRIPT_GET_RANDOM_INT_NOT_LAST(0, 3, iAnimToExclude)
	

		IF tempInt = 0
			TASK_PLAY_ANIM(ped, "ODDJOBS@ASSASSINATE@GUARD", "unarmed_earpiece_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE(), TRUE)
		ELIF tempInt = 1
			TASK_PLAY_ANIM(ped, "ODDJOBS@ASSASSINATE@GUARD", "unarmed_earpiece_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE(), TRUE)
		ELIF tempInt = 2
			TASK_PLAY_ANIM(ped, "ODDJOBS@ASSASSINATE@GUARD", "unarmed_fold_arms", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE(), TRUE)
		ELIF tempInt = 3
			TASK_PLAY_ANIM(ped, "ODDJOBS@ASSASSINATE@GUARD", "unarmed_look", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE(), TRUE)
		ENDIF
	
	iAnimToExclude = tempInt
ENDPROC

/// PURPOSE:
///    Have a ped play an animation (defaults to random idle if not specified)
PROC UPDATE_GUARD_ANIMS(PED_INDEX ped, INT& iAnimToExclude, BOOL idleAnim = TRUE)
	IF NOT IS_PED_INJURED(ped)
		IF idleAnim
			PLAY_RANDOM_ANIM(ped, iAnimToExclude)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: see if the entity passed is in a location deemed "safe" by the script
FUNC BOOL IS_ENTITY_IN_SAFE_LOCATION(ENTITY_INDEX entity)
	IF DOES_ENTITY_EXIST(entity)
//		IF NOT IS_ENTITY_IN_ANGLED_AREA(entity, <<-1252.143311,-212.687668,35.351608>>, <<-1216.625488,-185.542618,45.424759>>, 63.250000)
		IF NOT IS_ENTITY_IN_ANGLED_AREA(entity, <<-1252.301514,-213.903275,35.112217>>, <<-1205.807617,-183.068085,45.325413>>, 51.000000)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(entity, <<-1250.809326,-192.521835,35.331249>>, <<-1231.866333,-161.365524,45.025352>>, 12.800000)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE		
ENDFUNC


FUNC BOOL IS_VEHICLE_BLOCKING_THIS_AIRLOCK(INT iAirlockToCheck, FLOAT fDist)
	IF IS_ANY_VEHICLE_NEAR_POINT(vAirlockPos[iAirlockToCheck], fDist)
		PRINTLN("RETURNING TRUE ON AIRLOCK CHECK")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//RETURNS TRUE if there is a vehicle blocking any of the airlock lobby door locations
FUNC BOOL IS_VEHICLE_BLOCKING_ANY_AIRLOCK(FLOAT fDist)
	INT tempInt
	REPEAT NUM_AIRLOCK_SPAWN_LOCATIONS tempInt
		IF IS_VEHICLE_BLOCKING_THIS_AIRLOCK(tempInt, fDist)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC 


//checks to see if any vehicle is inside of the hotel circle area driveway where the target pulls out
FUNC BOOL IS_NON_SCRIPT_CREATED_VEHICLE_IN_HOTEL_AREA(VEHICLE_INDEX& vehToAssign)
	VECTOR tempVec
	
	vehToAssign = GET_CLOSEST_VEHICLE(<< -1221.954, -210.746, 38.638 >>, 27.000, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
	IF vehToAssign != NULL
	AND (vehToAssign != viTargetCar)
	AND (vehToAssign != viEscortCar) 
	AND (vehToAssign != stExtras.vehTaxi)
		IF DOES_ENTITY_EXIST(vehToAssign) AND NOT IS_ENTITY_DEAD(vehToAssign)
			IF GET_PED_IN_VEHICLE_SEAT(vehToAssign) = NULL
				tempVec = GET_ENTITY_COORDS(vehToAssign)
				PRINTLN("CLOSE VEHICLE LOCATED AT: <<", tempVec.x, ", ", tempVec.y, ", ", tempVec.z, ">>.")
				tempVec = tempVec
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	IF viReplayVehicle != NULL
		IF DOES_ENTITY_EXIST(viReplayVehicle) AND NOT IS_ENTITY_DEAD(viReplayVehicle)
			IF NOT IS_ENTITY_IN_SAFE_LOCATION(viReplayVehicle)
				tempVec = GET_ENTITY_COORDS(viReplayVehicle)
				PRINTLN("REPLAY VEHICLE LOCATED AT: <<", tempVec.x, ", ", tempVec.y, ", ", tempVec.z, ">>.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL DID_PLAYER_BLOCK_ALLEY_WITH_A_PARKED_CAR(VEHICLE_INDEX& vehToCheck, BOOL& bBoolToFlip)
	IF NOT DOES_ENTITY_EXIST(vehToCheck)
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(viEscortCar)
		RETURN FALSE
	ENDIF
		
//	IF IS_ENTITY_IN_ANGLED_AREA(vehToCheck, <<-1253.50000, -218.57869,39.329353>>, <<-1288.232178,-172.406372,46.207092>>, 8.000000, FALSE)		//main alley
//	OR IS_ENTITY_IN_ANGLED_AREA(vehToCheck, <<-1287.918213,-173.088760,41.658070>>, <<-1316.612305,-153.279510,48.951576>>, 8.000000, FALSE)		//alley extension
//	OR IS_ENTITY_IN_ANGLED_AREA(vehToCheck, <<-1287.918213,-173.088760,41.658070>>, <<-1316.612305,-153.279510,48.951576>>, 8.000000, FALSE)		//right near the motorcade
		
		PRINTLN("PLAYER LEFT A PARKED CAR IN THE MOTORCADE ALLEY THAT IS BLOCKING THE ESCORT CAR!!")
		IF GET_DISTANCE_BETWEEN_ENTITIES(vehToCheck, viEscortCar) <= 5
			viCarThatTriggeredAlert = vehToCheck
			bBoolToFlip = TRUE
			RETURN TRUE
		ENDIF
//	ENDIF
	
	RETURN FALSE
ENDFUNC



PROC GRAB_FURTHEST_VECTOR()
	INT idx
	VECTOR vPlayerPosition
	FLOAT fTempDistance
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	REPEAT NUM_AIRLOCK_SPAWN_LOCATIONS idx
		
		fTempDistance = VDIST(vPlayerPosition, vAirlockPos[idx])
		PRINTLN("fTempDistance = ", fTempDistance)
		PRINTLN("fClosestVector = ", fClosestVector)
		
		IF fTempDistance > fClosestVector
			fClosestVector = fTempDistance
			iIndexToUse = idx
			PRINTLN("iIndexToUse = ", idx)
		ENDIF	
	ENDREPEAT
	
	vAirlockSpawnPos[0] = vAirlockPos[iIndexToUse]
	fAirlockSpawnHead[0] = fAirlockHead[iIndexToUse]
	
ENDPROC


FUNC BOOL IS_PLAYER_WAITING_NEAR_HOTEL_STEPS_FOR_TARGET()

	//uses coords from bug 1526664 - (hiding near steps and can see target spawn in)
	IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), << -1237.56604, -156.88245, 39.41317 >>) < 13
		PRINTLN("IS_PLAYER_WAITING_NEAR_HOTEL_STEPS_FOR_TARGET()   RETURNED TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_PLAYER_IN_PARKING_GARAGE()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1272.181030,-241.254532,62.904064>>, <<-1300.203491,-201.396820,40.404076>>, 44.5, FALSE, FALSE)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1270.125488,-253.932114,62.904072>>, <<-1311.413818,-193.658829,38.232914>>, 44.5, FALSE, FALSE) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Locate safe airlock spawn locations based on checks to make sure the player wont be able to see the target spawn on screen
PROC LOCATE_SAFE_SPAWN_LOCATIONS()
	INT tempInt
	
	iNumSafeLocations = 0	//initialize this
	
	REPEAT NUM_AIRLOCK_SPAWN_LOCATIONS tempInt
		IF NOT IS_SPHERE_VISIBLE(vAirlockPos[tempInt], 5)
		AND NOT IS_VEHICLE_BLOCKING_THIS_AIRLOCK(tempInt, 6.0)
			vAirlockSpawnPos[iNumSafeLocations] = vAirlockPos[tempInt]
			fAirlockSpawnHead[iNumSafeLocations] = fAirlockHead[tempInt]
			iNumSafeLocations++	 
			PRINTLN("OJASHOTEL - vAirlockPos[", tempInt, "]. Is safe - SPHERE NOT VISIBLE.")
		ELSE
			PRINTLN("OJASHOTEL - vAirlockPos[", tempInt, "]. NOT safe - SPHERE IS VISIBLE or vehicle blocking airlock")
		ENDIF
	ENDREPEAT
	
	//check to make sure at least one location has been deemed as safe
	IF iNumSafeLocations = 0
		IF IS_PLAYER_IN_PARKING_GARAGE()
			vAirlockSpawnPos[0] = vAirlockPos[1]
			fAirlockSpawnHead[0] = fAirlockHead[1]
			iNumSafeLocations++
			PRINTLN("No safe spawn locations. Player is in parking garage - setting to 1.")
		ELIF IS_PLAYER_WAITING_NEAR_HOTEL_STEPS_FOR_TARGET()
			vAirlockSpawnPos[0] = vAirlockPos[0]
			fAirlockSpawnHead[0] = fAirlockHead[0]
			iNumSafeLocations++
			PRINTLN("No safe spawn locations. Player is near the steps - setting to 0.")
		ELSE
			//just do what we always did
			REPEAT NUM_AIRLOCK_SPAWN_LOCATIONS tempInt
				IF GET_PLAYER_DISTANCE_FROM_LOCATION(vAirlockPos[tempInt]) > 30
				AND NOT IS_VEHICLE_BLOCKING_THIS_AIRLOCK(tempInt, 6.0)
					vAirlockSpawnPos[iNumSafeLocations] = vAirlockPos[tempInt]
					fAirlockSpawnHead[iNumSafeLocations] = fAirlockHead[tempInt]
					iNumSafeLocations++
					PRINTLN("OJASHOTEL - vAirlockPos[", tempInt, "]. Is safe - DISTANCE.")
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	//if for some reason we still dont have a safe one, just grab the one furthest from the player
	IF iNumSafeLocations = 0
		GRAB_FURTHEST_VECTOR()
	ENDIF
	
	PRINTLN("OJASHOTEL - Number of safe spawn locations = ", iNumSafeLocations)
ENDPROC

/// PURPOSE:
///    Create the assassination target and setup attributes
PROC CREATE_TARGET()
//	iTargetAirlockSpawn = GET_RANDOM_INT_IN_RANGE() % 3
//	
//	//first try using the airlock based on the player's save data
//	IF iTargetAirlockSpawn <> -1 AND NOT IS_SPHERE_VISIBLE(vAirlockPos[iTargetAirlockSpawn], 5) AND NOT IS_VEHICLE_BLOCKING_THIS_AIRLOCK(iTargetAirlockSpawn, 6.0)
//		piTarget = CREATE_PED(PEDTYPE_MISSION, TargetModel, vAirlockPos[iTargetAirlockSpawn], fAirlockHead[iTargetAirlockSpawn])
//		PRINTLN("OJASHOTEL - CREATING TARGET FROM SAVEDATA AT vAirlockSpawnPos[", iTargetAirlockSpawn, "].")
//	ELSE
//		//otherwise find a new safe airlock to spawn him
		LOCATE_SAFE_SPAWN_LOCATIONS()

		iTargetAirlockSpawn = GET_RANDOM_INT_IN_RANGE(0, iNumSafeLocations)
		piTarget = CREATE_PED(PEDTYPE_MISSION, TargetModel, vAirlockSpawnPos[iTargetAirlockSpawn], fAirlockSpawnHead[iTargetAirlockSpawn])
		PRINTLN("OJASHOTEL - CREATING TARGET SAFE SPAWN LOGIC DATA AT vAirlockSpawnPos[", iTargetAirlockSpawn, "].")
//	ENDIF
	SET_ENTITY_LOAD_COLLISION_FLAG(piTarget, TRUE)
	SET_PED_DEFAULT_COMPONENT_VARIATION(piTarget)
	SET_PED_PARAMS(piTarget)
	SET_MODEL_AS_NO_LONGER_NEEDED(TargetModel)
	
	//blip the target
	IF NOT DOES_BLIP_EXIST(bTargetBlip)
		bTargetBlip = CREATE_BLIP_ON_ENTITY(piTarget, FALSE)
	ENDIF
	SET_BLIP_SCALE(bTargetBlip, 1)
ENDPROC


/// PURPOSE: 
///    Create the Limo
PROC CREATE_TARGET_CAR()
	IF NOT DOES_ENTITY_EXIST(viTargetCar)
		viTargetCar = CREATE_VEHICLE(TargetCarModel, vTargetCarPos, fTargetCarHeading)
		SET_MODEL_AS_NO_LONGER_NEEDED(TargetCarModel)
		SET_VEHICLE_DISABLE_TOWING(viTargetCar, TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(viTargetCar, TRUE)
		SET_VEHICLE_PROVIDES_COVER(viTargetCar, TRUE)
	ENDIF
ENDPROC


//create peds that drive the limo
PROC CREATE_LIMO_GUARDS()
	MODEL_NAMES mnModelToUse

	INT idx
	REPEAT NUM_LIMO_GUARDS idx
		IF NOT DOES_ENTITY_EXIST(piLimoGuard[idx])
			IF idx = 2
				mnModelToUse = GuardModel_01
				PRINTLN("mnModelToUse = GuardModel_01")
			ELSE
				mnModelToUse = GuardModel
				PRINTLN("mnModelToUse = GuardModel")
			ENDIF
			piLimoGuard[idx] = CREATE_PED(PEDTYPE_MISSION, mnModelToUse, vGuardPos[idx], fGuardHead[idx])
			IF idx = 0
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
			ELIF idx = 1
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,11), 0, 2, 0) //(jbib)
			ELIF idx = 2
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(piLimoGuard[idx], INT_TO_ENUM(PED_COMPONENT,11), 1, 1, 0) //(jbib)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(piLimoGuard[idx])
			SET_PED_CAN_BE_TARGETTED(piLimoGuard[idx], FALSE)	//so player cannot target guards through airlock doors
			SET_PED_KEEP_TASK(piLimoGuard[idx], TRUE)
			SET_PED_COMBAT_ATTRIBUTES(piLimoGuard[idx], CA_REQUIRES_LOS_TO_SHOOT, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(piLimoGuard[idx], CA_CAN_SHOOT_WITHOUT_LOS , FALSE)
			SET_PED_TARGET_LOSS_RESPONSE(piLimoGuard[idx], TLR_SEARCH_FOR_TARGET)
			SET_PED_CONFIG_FLAG(piLimoGuard[idx], PCF_OpenDoorArmIK, TRUE)
			SET_PED_PARAMS(piLimoGuard[idx], TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

//creates the limo guards that are used spefically used in the intro cutscene
PROC CREATE_CUTSCENE_PEDS()
	IF DOES_ENTITY_EXIST(viTargetCar)
		IF IS_VEHICLE_DRIVEABLE(viTargetCar)
			REPEAT NUM_LIMO_GUARDS i
				IF NOT DOES_ENTITY_EXIST(piLimoGuard[i])
					IF i = 0
						piLimoGuard[i] = CREATE_PED_INSIDE_VEHICLE(viTargetCar, PEDTYPE_MISSION, GuardModel)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
					ELIF i = 1
						piLimoGuard[i] = CREATE_PED_INSIDE_VEHICLE(viTargetCar, PEDTYPE_MISSION, GuardModel, VS_FRONT_RIGHT)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,11), 0, 2, 0) //(jbib)
					ELIF i = 2
						piLimoGuard[i] = CREATE_PED_INSIDE_VEHICLE(viTargetCar, PEDTYPE_MISSION, GuardModel_01, VS_BACK_LEFT)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
						SET_PED_COMPONENT_VARIATION(piLimoGuard[i], INT_TO_ENUM(PED_COMPONENT,11), 1, 1, 0) //(jbib)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Create the motorcade car and bike vehicles
PROC CREATE_MOTORCADE_FOLLOW_CAR()
	IF NOT DOES_ENTITY_EXIST(viEscortCar)
		viEscortCar = CREATE_VEHICLE(EscortCarModel, vEscortCarPos, fEscortCarHead)
		SET_VEHICLE_COLOUR_COMBINATION(viEscortCar, 0)
		PRINTLN("CREATING ESCORT CAR")
	ENDIF
ENDPROC

/// PURPOSE: Create the motorcade peds
PROC CREATE_MOTORCADE_PEDS()
	INT tempInt
	
	REPEAT NUM_ESCORT_PEDS tempInt
		IF NOT DOES_ENTITY_EXIST(piEscortPed[tempInt])
			IF tempInt = 0	//driver
				piEscortPed[tempInt] = CREATE_PED_INSIDE_VEHICLE(viEscortCar, PEDTYPE_MISSION, GuardModel)
				PRINTLN("CREATING ESCORT PED 01")
			ELIF tempInt = 1	//passenger
				piEscortPed[tempInt] = CREATE_PED_INSIDE_VEHICLE(viEscortCar, PEDTYPE_MISSION, GuardModel_01, VS_FRONT_RIGHT)
				PRINTLN("CREATING ESCORT PED 02")
			ELSE
				SCRIPT_ASSERT("TRYING TO CREATE A MOTORCADE PED AT A VALUE HIGHER THAN ALLOWED - INCREASE NUM_ESCORT_PEDS!" )
			ENDIF
			
			SET_PED_PARAMS(piEscortPed[tempInt], TRUE)
			SET_PED_COMBAT_ATTRIBUTES(piEscortPed[tempInt], CA_LEAVE_VEHICLES, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(piEscortPed[tempInt], CA_REQUIRES_LOS_TO_SHOOT, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(piEscortPed[tempInt], CA_CAN_SHOOT_WITHOUT_LOS , FALSE)
			SET_PED_TARGET_LOSS_RESPONSE(piEscortPed[tempInt], TLR_SEARCH_FOR_TARGET)
			SET_PED_KEEP_TASK(piEscortPed[tempInt], TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

// creates the vehicle and guards that are in the motorcade follow car
PROC CREATE_MOTORCADE_ACTORS_AND_VEHICLE()
	CREATE_MOTORCADE_FOLLOW_CAR()
	CREATE_MOTORCADE_PEDS()
	SET_MODEL_AS_NO_LONGER_NEEDED(EscortCarModel)
ENDPROC


PROC CREATE_BODYGUARDS()
	//grab airlock position that target was created and spawn the bodyguard inside that at an offset to the target
	VECTOR tempVec[2]
	VECTOR vTargetPosition
	FLOAT fTargetHead
	
	IF DOES_ENTITY_EXIST(piTarget) AND NOT IS_ENTITY_DEAD(piTarget)
		vTargetPosition = GET_ENTITY_COORDS(piTarget)
		PRINTLN("TARGET SPAWN POSITION = ", vTargetPosition)
		fTargetHead = GET_ENTITY_HEADING(piTarget)
		PRINTLN("TARGET SPAWN HEADING = ", fTargetHead)
	ELSE
		PRINTLN("TARGET DOES NOT EXIST!!!")
	ENDIF
	
	tempVec[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTargetPosition, fTargetHead, <<1, -1, 0>>)
	PRINTLN("BODYGUARD[0] SPAWN POSITION = ", tempVec[0])
	tempVec[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTargetPosition, fTargetHead, <<-1, -1, 0>>)
	PRINTLN("BODYGUARD[1] SPAWN POSITION = ", tempVec[1])

	INT idx
	REPEAT NUM_BODYGUARDS idx
		IF idx = 0
			piBodyguard[idx] = CREATE_PED(PEDTYPE_MISSION, GuardModel, << tempVec[0].x, tempVec[0].y, 38.3253>>, fAirlockSpawnHead[iTargetAirlockSpawn])
		ELSE
			piBodyguard[idx] = CREATE_PED(PEDTYPE_MISSION, GuardModel_01, << tempVec[1].x, tempVec[1].y, 38.3253>>, fAirlockSpawnHead[iTargetAirlockSpawn])
		ENDIF
		SET_ENTITY_LOAD_COLLISION_FLAG(piBodyguard[idx], TRUE)
		SET_PED_DEFAULT_COMPONENT_VARIATION(piBodyguard[idx])
		SET_PED_COMBAT_ATTRIBUTES(piBodyguard[idx], CA_LEAVE_VEHICLES, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(piBodyguard[idx], CA_REQUIRES_LOS_TO_SHOOT, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piBodyguard[idx], CA_CAN_SHOOT_WITHOUT_LOS , FALSE)
		SET_PED_COMBAT_ATTRIBUTES(piBodyguard[idx], CA_AGGRESSIVE, TRUE)
		SET_PED_TARGET_LOSS_RESPONSE(piBodyguard[idx], TLR_SEARCH_FOR_TARGET)
		SET_PED_KEEP_TASK(piBodyguard[idx], TRUE)
		SET_PED_PARAMS(piBodyguard[idx])
		SET_PED_TO_LOAD_COVER(piBodyguard[idx], TRUE)
		PRINTLN("CREATING BODYGUARD[", idx, "]")
	ENDREPEAT
ENDPROC

PROC REMOVE_STREAMVOL_FOR_INTRO_CUTSCENE()
	IF STREAMVOL_IS_VALID(svIntroCutscene)
		STREAMVOL_DELETE(svIntroCutscene)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the loading and unloading of the intro cutscene STREAMVOL so that we make sure it is loaded in when close to the hotel and unloads if the player dirves away
PROC HANDLE_STREAMVOL_AND_GUARD_MODEL_LOADING_UNLOADING()
	
	IF NOT bLoadSceneRequested
		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vNewBlipPosition) <= DEFAULT_CUTSCENE_LOAD_DIST
			REQUEST_GUARD_MODELS()
			svIntroCutscene = STREAMVOL_CREATE_FRUSTUM(<<-1244.3435, -251.5660, 50.3194>>, <<-9.2001, 0.0498, 6.3064>>, 80, FLAG_MAPDATA)
			bLoadSceneRequested = TRUE
		ENDIF
	ELSE
		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vNewBlipPosition) > DEFAULT_CUTSCENE_UNLOAD_DIST
			IF STREAMVOL_IS_VALID(svIntroCutscene)
				STREAMVOL_DELETE(svIntroCutscene)
				UNLOAD_GUARD_MODELS()
				bLoadSceneRequested = FALSE
			ENDIF
		ELSE
			IF NOT STREAMVOL_IS_VALID(svIntroCutscene)
				IF NOT IS_PLAYER_A_TAXI_PASSENGER()
					bLoadSceneRequested = FALSE
					PRINTLN("RESETTING bLoadSceneRequested = FALSE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC HANDLE_SPECIAL_CASE_AUDIO_CUE()
	VEHICLE_INDEX vehTemp

	IF NOT bPrepareLostCue 
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				IF DOES_ENTITY_EXIST(vehTemp) AND NOT IS_ENTITY_DEAD(vehTemp)
					IF GET_IS_VEHICLE_ENGINE_RUNNING(vehTemp)
						IF PREPARE_MUSIC_EVENT("ASS1_LOST")
							PRINTLN("PREPARING MUSIC - ASS1_LOST")
							bPrepareLostCue = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_PLAYER_IN_AIRLOCK()
	INT idx
	
	REPEAT NUM_AIRLOCK_SPAWN_LOCATIONS idx
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF GET_PLAYER_DISTANCE_FROM_LOCATION(vAirlockPos[idx]) < 5.0
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1223.1853, -185.4723, 38.1753>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 119.5633)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

// Cleanup the script on termination – must not include any WAITs
PROC Script_Cleanup()
	
	IF IS_TIMER_STARTED(tMissionTimer)
		g_savedGlobals.sAssassinData.fHotelMissionTime = GET_TIMER_IN_SECONDS(tMissionTimer)
		PRINTLN("CLEANUP: missionTimer = ", g_savedGlobals.sAssassinData.fHotelMissionTime)
	ELSE
		PRINTLN("CLEANUP: missionTimer WAS NOT STARTED!!!")
	ENDIF
	
	IF NOT bTriggeredLastCue
		TRIGGER_MUSIC_EVENT("ASS1_LOST")
		PRINTLN("TRIGGERING MUSIC - ASS1_LOST - VIA CLEANUP")
	ENDIF
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\ASSASSINATION_MULTI")
	PRINTLN("RELEASING AUDIO BANK - SCRIPT\\ASSASSINATION_MULTI")

	CLEANUP_EXTRAS()
	CLEANUP_PEDS()
	CLEANUP_VEHICLES()
	CLEANUP_PED_BLIPS()
	CLEANUP_DEST_BLIPS()
	CLEANUP_VEH_RECORDINGS()
//	CLEAR_AREA(<< -1220.57, -185.96, 38.40 >>, 50, TRUE)
	SET_LOW_DENSITY_HOTEL_TRAFFIC(FALSE)
	KILL_ANY_CONVERSATION()
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
		SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[0], TRUE, TRUE)
		SET_OBJECT_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.object[0])
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbiHotel)
	REMOVE_SCENARIO_BLOCKING_AREA(sbiHotel01)
	
	REMOVE_STREAMVOL_FOR_INTRO_CUTSCENE()
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
//	SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
//	PRINTLN("SETTING FLAG TO FALSE - AllowScoreAndRadio")
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		Store_Fail_Weapon(PLAYER_PED_ID(), 1)
		PRINTLN("ATTEMPTING TO STORE THE PLAYER'S LAST WEAPON")
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
	ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(1)
	CLEAR_PED_NON_CREATION_AREA()
	
	SET_PED_MODEL_IS_SUPPRESSED(GuardModel, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(GuardModel_01, FALSE)
	
	HANDLE_PLAYER_IN_AIRLOCK()
	
	TERMINATE_THIS_THREAD()
ENDPROC

// Perform any special commands if the script passes
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Passed()	
	
	HANDLE_SPECIAL_CASE_AUDIO_CUE()
	
	SWITCH iEndingScreenStages
		CASE 0
			IF NOT bResultsCreated
				IF killType = KT_BONUS
					bBonus = TRUE
					ASSASSIN_MISSION_MarkMissionPassedWithBonus()
					PRINTLN("HOTEL - PLAYER GOT BONUS")
				ENDIF
				
				g_savedGlobals.sAssassinData.fHotelMissionTime = GET_TIMER_IN_SECONDS(tMissionTimer)
				PRINTLN("GLOBAL HOTEL MISSION TIME = ", g_savedGlobals.sAssassinData.fHotelMissionTime)
				
				fTimeTaken = g_savedGlobals.sAssassinData.fHotelMissionTime
				PRINTLN("fTimeTaken = ", fTimeTaken)
				
				SETUP_RESULTS_UI()
				bResultsCreated = TRUE
				
				PRINTLN("iEndingScreenStages = 1")
				iEndingScreenStages = 1
			ENDIF
		BREAK
		CASE 1
			IF NOT bPlayMusic
				PLAY_MISSION_COMPLETE_AUDIO("FRANKLIN_BIG_01")
				bPlayMusic = TRUE
			ENDIF
		
			IF ENDSCREEN_PREPARE(assassinationEndScreen)
			AND IS_MISSION_COMPLETE_READY_FOR_UI()
			
				INIT_SIMPLE_USE_CONTEXT(cucEndScreen, FALSE, FALSE, FALSE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(cucEndScreen, "ASS_VA_CONT",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
				ADD_SIMPLE_USE_CONTEXT_INPUT(cucEndScreen, "ES_XPAND",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
			
				SETTIMERA(0)
			
				PRINTLN("iEndingScreenStages = 2")
				iEndingScreenStages = 2
			ENDIF
		BREAK
		CASE 2
			IF RENDER_ENDSCREEN(assassinationEndScreen, FALSE)
				bSkippedEndingScreen = TRUE
			ENDIF
			
			IF NOT bSkippedEndingScreen
				UPDATE_SIMPLE_USE_CONTEXT(cucEndScreen)
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,			INPUT_FRONTEND_ENDSCREEN_ACCEPT)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,	INPUT_FRONTEND_PAUSE_ALTERNATE)		// B*2276517 - Player can now press esc to close endscreen, if player control is on during endscreen
				IF NOT bSkippedEndingScreen
					PRINTLN("bSkippedEndingScreen = TRUE")
					bSkippedEndingScreen = TRUE
					//Start anim-out
					ENDSCREEN_START_TRANSITION_OUT(assassinationEndScreen)
				ENDIF
			ENDIF
			
			IF bSkippedEndingScreen
				//Wait until the end screen stops transition-out
				IF RENDER_ENDSCREEN(assassinationEndScreen)
					ENDSCREEN_SHUTDOWN(assassinationEndScreen)
					
					++g_savedGlobals.sAssassinData.iCurrentAssassinRank 
					ASSASSIN_MISSION_MarkMissionPassed()
					ASSASSIN_MISSION_AwardAssReward(missionData)
					MISSION_FLOW_MISSION_PASSED()
					ODDJOB_AUTO_SAVE()
					
					Script_Cleanup()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

// Perform any special commands if the script fails
PROC Script_Failed()
	// this is now called once the screen has faded out for the fail screen.
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
		
	Script_Cleanup() // must only take 1 frame and terminate the thread
ENDPROC


PROC ADD_PARKED_CARS_TO_GARAGE()
	viParkedCar[0] = CREATE_VEHICLE(EscortCarModel , << -1270.0448, -251.4953, 41.4459 >>, 214.63)
	viParkedCar[1] = CREATE_VEHICLE(EscortCarModel, << -1265.1992, -218.4823, 45.9981 >>, 127.85)
ENDPROC



/// PURPOSE: Call this before advancing stages
PROC INITIALIZE_NEXT_STAGE()
	iStageNum = 0
ENDPROC

//PURPOSE: Skip the intro cutscene and advance to the next stage cleanly
PROC HANDLE_GUARD_CUTSCENE_SKIP(BOOL bDoScreenFadeIn)
	
//	//delete cutscene peds if they still exist
//	REPEAT NUM_LIMO_GUARDS i
//		IF DOES_ENTITY_EXIST(piCutsceneGuard[i])
//			DELETE_PED(piCutsceneGuard[i])
//		ENDIF
//	ENDREPEAT
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	//setup the limo properly in front of the hotel and exit out of the cutscene
	IF IS_VEHICLE_DRIVEABLE(viTargetCar)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viTargetCar)
			STOP_PLAYBACK_RECORDED_VEHICLE(viTargetCar)
		ENDIF
		REMOVE_VEHICLE_RECORDING(102, "ASSOJva")
		REMOVE_VEHICLE_RECORDING(107, "ASSOJva")
		
		//warp the vehicle to the proper position
		SET_ENTITY_COORDS(viTargetCar, vTargetCarLobbyPos)
		SET_ENTITY_HEADING(viTargetCar, fTargetCarLobbyHead)
		
		SET_VEHICLE_DOOR_SHUT(viTargetCar, SC_DOOR_FRONT_LEFT)
		SET_VEHICLE_DOOR_SHUT(viTargetCar, SC_DOOR_FRONT_RIGHT)
		SET_VEHICLE_DOOR_SHUT(viTargetCar, SC_DOOR_REAR_LEFT)
		SET_VEHICLE_DOOR_SHUT(viTargetCar, SC_DOOR_REAR_LEFT)

		//shut down the cutscene camera and return to gameplay cam
		iCutsceneStage = 0	//re-initialize this for the kill camera
		CLEAR_PRINTS()
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
 		ODDJOB_EXIT_CUTSCENE()
		
		IF bDoScreenFadeIn
			CUTSCENE_SKIP_FADE_IN()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Play intro cutscene explaining the mission when the player gets within range of the htoel
FUNC BOOL HAS_INTRO_CUTSCENE_FINISHED()
	INT iTemp

	SWITCH iCutsceneStage
		CASE 0
			ODDJOB_ENTER_CUTSCENE()
			GET_ASSASSIN_CUTSCENE_START_TIME()
			
			SHAKE_CAM(camInit, "HAND_SHAKE", 0.2)
			SHAKE_CAM(camDest, "HAND_SHAKE", 0.2)
			SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, 6000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			CLEAR_HELP()
			CLEAR_PRINTS()
			PRINT_NOW("ASS_VA_SNIPE1", DEFAULT_GOD_TEXT_TIME, 1)
			
			IF bPlayerIsInFlyingVehicle
				MOVE_PLAYER_AND_PLANE()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				PRINTLN("bPlayerIsInFlyingVehicle IS TRUE, NEED TO REPOSITION THE PLAYER")
			ELSE
				REPOSITION_PLAYER_AND_VEHICLE_FOR_GUARD_CUTSCENE()
				PRINTLN("repositioning player and car for guard cutscene")
			ENDIF
			
			SETTIMERA(0)
			
			iCutsceneStage++
		BREAK
		
		//close up of car pulling up and guys getting out
		CASE 1
			IF NOT IS_ENTITY_DEAD(viTargetCar)
				IF IS_VEHICLE_DRIVEABLE(viTargetCar)
					IF TIMERA() > 6000
						//Switch back to the original recording of the car parking up.
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viTargetCar)
							STOP_PLAYBACK_RECORDED_VEHICLE(viTargetCar)
						ENDIF
						
						START_PLAYBACK_RECORDED_VEHICLE(viTargetCar, 102, "ASSOJva")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(viTargetCar, 4500)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(viTargetCar)
					
						PRINT_NOW("ASS_VA_SNIPE2", DEFAULT_GOD_TEXT_TIME, 1)
					
						SET_CAM_PARAMS(camInit, <<-1218.860718,-191.049606,38.836159>>,<<3.519839,-0.000706,42.271080>>,38.000000)
						SET_CAM_NEAR_DOF(camInit, 0.5)
						SET_CAM_FAR_DOF(camInit, 7.0)
						SHAKE_CAM(camInit, "HAND_SHAKE", 0.2)
						SET_CAM_PARAMS(camDest, <<-1218.909668,-190.879135,38.845852>>,<<3.519839,-0.000706,43.338146>>,38.000000)
						SET_CAM_NEAR_DOF(camDest, 0.7)
						SET_CAM_FAR_DOF(camDest, 10.0)
						SHAKE_CAM(camDest, "HAND_SHAKE", 0.2)
						SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, 2000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
						
						iCutsceneStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//have guys leave limo and pause vehicle recording playback when they arrive at the lobby
		CASE 2
			IF NOT IS_ENTITY_DEAD(viTargetCar)
				IF IS_VEHICLE_DRIVEABLE(viTargetCar)
					IF GET_TIME_POSITION_IN_RECORDING(viTargetCar) >= 7000.0

						//limo pulling up to the hotel
						PAUSE_PLAYBACK_RECORDED_VEHICLE(viTargetCar)
						
						//have cutscene peds exit the vehicle				
						REPEAT NUM_LIMO_GUARDS i
							//delay tasking guards to exit all at once so it wont look so robotic - tell em to exit and go to center airlock
							IF NOT IS_ENTITY_DEAD(piLimoGuard[i])
								IF i = 0
									iTemp = 1250
								ELIF i = 1
									iTemp = 400
								ELSE
									iTemp = 0
								ENDIF
								IF GET_SCRIPT_TASK_STATUS(piLimoGuard[i], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
									CLEAR_SEQUENCE_TASK(seqIndex)
									OPEN_SEQUENCE_TASK(seqIndex)
										TASK_LEAVE_ANY_VEHICLE(NULL, iTemp)
//										TASK_GO_TO_COORD_ANY_MEANS(NULL, vAirlockPos[1], PEDMOVEBLENDRATIO_WALK, NULL)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vAirlockPos[i], PEDMOVEBLENDRATIO_WALK)
									CLOSE_SEQUENCE_TASK(seqIndex)
									TASK_PERFORM_SEQUENCE(piLimoGuard[i], seqIndex)
									CLEAR_SEQUENCE_TASK(seqIndex)
								ENDIF
							ENDIF
						ENDREPEAT
						
						SETTIMERA(0)
						iCutsceneStage ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF TIMERA() >= 2000
		
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					
					SET_CAM_PARAMS(camInit, <<-1265.5488, -213.4919, 43.0913>>, <<-0.0550, 0.0543, -50.0795>>,38.000000)
					SHAKE_CAM(camInit, "HAND_SHAKE", 0.2)
					SET_CAM_PARAMS(camDest, <<-1266.0629, -213.9220, 43.0918>>, <<-0.0550, 0.0543, -50.0795>>,38.000000)
					SHAKE_CAM(camDest, "HAND_SHAKE", 0.2)
					SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, 4000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
					
					iTimeToCut = 4000
					PRINTLN("iTimeToCut = ", iTimeToCut)
				ELSE
					SET_CAM_PARAMS(camInit, <<-1266.1925, -215.2170, 43.2442>>, <<-3.1088, 0.0498, -48.5594>>,38.000000)
					SHAKE_CAM(camInit, "HAND_SHAKE", 0.2)
					SET_CAM_PARAMS(camDest, <<-1267.5302, -215.7824, 43.1067>>, <<-9.2365, 0.0498, -48.3561>>,38.000000)
					SHAKE_CAM(camDest, "HAND_SHAKE", 0.2)
					SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, 4000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
				
					iTimeToCut = 4000
					PRINTLN("iTimeToCut = ", iTimeToCut)
				ENDIF
				
				SETTIMERA(0)
				iCutsceneStage ++
			ENDIF
		BREAK
		CASE 4
			IF NOT bDoFlash
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					IF TIMERA() >= iTimeToCut - 300
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						
						PRINTLN("bDoFlash = TRUE")
						bDoFlash = TRUE
					ENDIF
				ENDIF
			ENDIF
					
			IF TIMERA() >= iTimeToCut
				IF IS_VEHICLE_DRIVEABLE(viTargetCar)
	 				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viTargetCar)
						STOP_PLAYBACK_RECORDED_VEHICLE(viTargetCar)
					ENDIF
				ENDIF
				REMOVE_VEHICLE_RECORDING(102, "ASSOJva")
				REMOVE_VEHICLE_RECORDING(107, "ASSOJva")
				
				SET_VEHICLE_DOOR_SHUT(viTargetCar, SC_DOOR_FRONT_RIGHT)
				SET_VEHICLE_DOOR_SHUT(viTargetCar, SC_DOOR_FRONT_LEFT)
				SET_VEHICLE_DOOR_SHUT(viTargetCar, SC_DOOR_REAR_LEFT)
				
				CLEAR_HELP(TRUE)

				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					PRINTLN("CALLING RENDER SCRIPT CAMS - FALSE, TRUE")
				ELSE
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE, 0, CAM_SPLINE_SLOW_IN_SMOOTH)
					PRINTLN("CALLING STOP RENDERING CAMS USING CATCH UP")
				ENDIF
				
				ODDJOB_EXIT_CUTSCENE()
					
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	
	//handle cutscene skipping and make sure that the player's vehicle doesn't explode in the cutscene if entered while on fire
	IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
		CLEAR_HELP(TRUE)
		HANDLE_GUARD_CUTSCENE_SKIP(TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL ARE_BODYGUARDS_IN_VEHICLES()
	INT idx
	BOOL bInVehicles = TRUE
	
	FOR idx = 0 TO NUM_BODYGUARDS - 1
		IF NOT IS_PED_INJURED(piBodyguard[idx])
			IF NOT IS_PED_IN_ANY_VEHICLE(piBodyguard[idx])
				bInVehicles = FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN bInVehicles
ENDFUNC


/// PURPOSE: RETURN true if the target is in the limo
FUNC BOOL IS_TARGET_IN_LIMO()
	IF IS_VEHICLE_DRIVEABLE(viTargetCar)
		IF NOT IS_PED_INJURED(piTarget)
			IF NOT bPanicking
				IF IS_PED_IN_VEHICLE(piTarget, viTargetCar)
				AND ARE_BODYGUARDS_IN_VEHICLES()
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_PED_IN_VEHICLE(piTarget, viTargetCar)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Tell ped to exit a vehicle if they are in one and aim at the player
PROC EXIT_VEHICLE_AND_AIM_GUN_AT_PLAYER(PED_INDEX ped)
	SEQUENCE_INDEX tempSeq
	
	IF NOT IS_PED_INJURED(Player)
	AND NOT IS_PED_INJURED(ped)
		IF NOT IS_ENTITY_DEAD(ped)
			SET_PED_COMBAT_ATTRIBUTES(ped, CA_LEAVE_VEHICLES, TRUE)
		ENDIF
	
		CLEAR_SEQUENCE_TASK(tempSeq)
		OPEN_SEQUENCE_TASK(tempSeq)
			TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR)
			TASK_AIM_GUN_AT_ENTITY(NULL, Player, -1)
		CLOSE_SEQUENCE_TASK(tempSeq)
		TASK_PERFORM_SEQUENCE(ped, tempSeq)
		CLEAR_SEQUENCE_TASK(tempSeq)
		
		PRINTLN("Tasking ped to exit vehicle and aim gun at player!")
	ENDIF
ENDPROC

/// PURPOSE: Tell ped to exit a vehicle if they are in one and attack the player
PROC EXIT_VEHICLE_AND_ATTACK_PLAYER(PED_INDEX ped)
	
	IF NOT IS_ENTITY_DEAD(Player)
		IF NOT IS_PED_IN_ANY_VEHICLE(Player)
			IF NOT IS_ENTITY_DEAD(ped)
				SET_PED_COMBAT_ATTRIBUTES(ped, CA_LEAVE_VEHICLES, TRUE)
			ENDIF
		
			CLEAR_SEQUENCE_TASK(seqIndex)
			OPEN_SEQUENCE_TASK(seqIndex)
				TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR)
				TASK_COMBAT_PED(NULL, Player)
			CLOSE_SEQUENCE_TASK(seqIndex)
			IF NOT IS_ENTITY_DEAD(ped)
				TASK_PERFORM_SEQUENCE(ped, seqIndex)
			ENDIF
			CLEAR_SEQUENCE_TASK(seqIndex)
			
			PRINTLN("THE PLAYER IS NOT IN A CAR, WE SHOULD BE LEAVING!!!")
		ELSE
			IF NOT IS_PED_INJURED(ped)
				IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
					CLEAR_PED_TASKS(ped)
					TASK_COMBAT_PED(ped, Player)
					PRINTLN("TASKING TO USE COMBAT - PLAYER MUST BE IN A CAR")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_VEHICLE_STUCK(VEHICLE_INDEX veh, INT iStuckTimeToCheck)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_ROOF, iStuckTimeToCheck)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_HUNG_UP, iStuckTimeToCheck)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_JAMMED, iStuckTimeToCheck)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_SIDE, iStuckTimeToCheck )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
/// limo is not driveable - guards should fight the player and target should flee
FUNC BOOL CHECK_FOR_DESTROYED_LIMO()
	IF ( DOES_ENTITY_EXIST(viTargetCar) AND NOT IS_VEHICLE_DRIVEABLE(viTargetCar) )
	OR ( DOES_ENTITY_EXIST(piLimoGuard[0]) AND IS_PED_INJURED(piLimoGuard[0]) ) //driver has been killed
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_BOTH_ESCORT_PEDS_IN_VEHICLE()
	IF NOT IS_PED_INJURED(piEscortPed[0])
	AND NOT IS_PED_INJURED(piEscortPed[1])
		IF IS_PED_IN_VEHICLE(piEscortPed[0], viEscortCar)
		AND IS_PED_IN_VEHICLE(piEscortPed[1], viEscortCar)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC




/// PURPOSE: RETURN handle the escape behavior of the peds entering the limo
FUNC BOOL HAS_HOTEL_TARGET_ESCAPED(eFailType& fail)
	FLOAT fDistToCheck
	BOOL bCheckCarVisibility
	
	//check to see if the target is in the vehicle
	IF MainStage > STAGE_TARGET_LEAVE_LOBBY
		IF NOT IS_PED_INJURED(piTarget)
			IF IS_VEHICLE_DRIVEABLE(viTargetCar)
				IF IS_PED_IN_VEHICLE(piTarget, viTargetCar)
					bCheckCarVisibility = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		//alter distances to check based on whether the target is in a vehicle or not
		IF bTargetFleeing
			fDistToCheck = 100
		ELSE
			fDistToCheck = 200
		ENDIF

		//check to see if target escaped
		IF DOES_ENTITY_EXIST(piTarget)
			IF NOT IS_PED_INJURED(piTarget)
			
				IF GET_DISTANCE_BETWEEN_ENTITIES (Player, piTarget) >= fDistToCheck
					IF bCheckCarVisibility
						IF IS_VEHICLE_DRIVEABLE(viTargetCar)
							IF IS_ENTITY_OCCLUDED(viTargetCar)
							OR NOT IS_ENTITY_ON_SCREEN(viTargetCar)
								fail = FT_ESCAPED
								PRINTLN("Lost target in vehicle")
								RETURN TRUE
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_OCCLUDED(piTarget)
							fail = FT_ESCAPED
							PRINTLN("Lost target on foot")
							RETURN TRUE
//						ELSE
//							PRINTLN("TARGET CAN BE SEEN")
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(bTargetBlip)
						IF GET_DISTANCE_BETWEEN_ENTITIES (Player, piTarget) >= fDistToCheck * 0.5
							IF NOT IS_BLIP_FLASHING(bTargetBlip)
								SET_BLIP_FLASHES(bTargetBlip, TRUE)
							ENDIF
						ELSE
							IF IS_BLIP_FLASHING(bTargetBlip)
								SET_BLIP_FLASHES(bTargetBlip, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
//		//fail the player if they abandon the hotel area after the intro cutscene has played
//		IF MainStage >= STAGE_PLANT_BOMB
//			IF HAS_PLAYER_ABANDONED_ASSASSINATION_AREA(bWarningHelp, vHotelStart, "ASS_VA_RETURN", 175)
//				fail = FT_ABANDONED
//				RETURN TRUE
//			ENDIF
//		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: Fail the mission if the target escapes
PROC FAIL_IF_TARGET_ESCAPES()
	IF HAS_HOTEL_TARGET_ESCAPED(failType)
		SET_MISSION_FAILED(FT_ESCAPED)
		PRINTLN("OJASHOTEL - Target escaped")
	ENDIF
ENDPROC



/// PURPOSE:
///    Checks to see if the player alerted the guards but was not seen and sets the appropriate bool if we detect that alert state should be set
/// PARAMS:
///    vPositionToCheck - Vector to check player's distance away from
PROC DO_ALERTED_CHECK(VECTOR vPositionToCheck, FLOAT fKillDistanceAlerted = 15.0, FLOAT fKillDistanceAlertedSpecial = 70.0)
	BOOL bShotAlerted
	FLOAT fKillDistance
	
	IF NOT DOES_ENTITY_EXIST(piTarget)
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(viTargetCar)
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(viTargetCar)
	OR bPlayerTriggeredExplosionFromParkingGarage
		bPlayerUsedStickyBomb = TRUE
		PRINTLN("bPlayerUsedStickyBomb = TRUE")
	ENDIF
	
	IF DOES_ENTITY_EXIST(piTarget)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(piTarget, WEAPONTYPE_SNIPERRIFLE)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(piTarget, WEAPONTYPE_HEAVYSNIPER)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(piTarget, WEAPONTYPE_REMOTESNIPER)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(piTarget, WEAPONTYPE_DLC_MARKSMANRIFLE)
			DEBUG_MESSAGE("BONUS AWARDED - SNIPER KILL")
			bPlayerUsedSniperRifle = TRUE
			PRINTLN("bPlayerUsedSniperRifle = TRUE")
			killType = KT_BONUS
		ENDIF
	ENDIF
	
	IF aggroReason = EAggro_ShotNear
	OR aggroReason = EAggro_HeardShot
		IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(viTargetCar)		//only go to alert state if the car hasn't left yet
			fKillDistanceAlerted *= 2		//double the distance to check for alerted
			bShotAlerted = TRUE
			PRINTLN("bShotAlerted = TRUE")
		ENDIF
	ENDIF
	
	
	//set the appropriate bool if we detect that alert state should be set
	IF NOT bPanicking
		IF bPlayerUsedSniperRifle
		OR bPlayerUsedStickyBomb
		OR bShotAlerted
			fKillDistance = GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vPositionToCheck)
			PRINTLN("fKillDistance = ", fKillDistance)
			
			IF fKillDistance > fKillDistanceAlerted
				bAlerted = TRUE
				PRINTLN("bAlerted = TRUE")
				
				IF fKillDistance > fKillDistanceAlertedSpecial
					bAlertedSpecial = TRUE
					PRINTLN("bAlertedSpecial = TRUE")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("WE PANICKED EARLY, SOMEHOW")
	ENDIF
ENDPROC


/// PURPOSE:
///    Checks to see if the player alerted the guards but they do not know the player's position
/// RETURNS:
///    TRUE if bAlertedor bAlertedSpecial has been set
FUNC BOOL SHOULD_TRIGGER_ALERT()
	IF bAlerted
	OR bAlertedSpecial
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE: Check for Target Kill
PROC CHECK_FOR_TARGET_KILL()
//	VECTOR vTempCamPos
		
	IF DOES_ENTITY_EXIST(piTarget)
		IF IS_PED_INJURED(piTarget)
			//award a bonus if the player killed via sniper rifle
			IF IS_ENTITY_DEAD(piTarget)
				bTargetDead = TRUE
				PRINTLN("bTargetDead = TRUE")
				
				IF DOES_BLIP_EXIST(bTargetBlip)
					REMOVE_BLIP(bTargetBlip)
					PRINTLN("REMOVING TARGETS BLIP")
				ENDIF
				
				//check to see how we should respond
				DO_ALERTED_CHECK(vTargetLastPosition)
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0)
			
			INITIALIZE_NEXT_STAGE()
			MainStage = STAGE_KILL_CAM
			PRINTLN("OJASHOTEL - MainStage = STAGE_KILL_CAM")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if bGuardPanicLinePlayed is set to FALSE and if so plays the appropriate panic line
/// PARAMS:
///    ped - speaker
PROC PLAY_GUARD_PANIC_LINE(PED_INDEX ped)
	TEXT_LABEL_23 tlCurrentConvoRoot
	
	IF NOT bGuardPanicLinePlayed
		IF DOES_ENTITY_EXIST(ped) AND NOT IS_PED_INJURED(ped)
		AND GET_PLAYER_DISTANCE_FROM_ENTITY(ped) < 50
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF failType = FT_BLOWN_COVER_HIT_PARKED_CAR
					SETUP_PED_FOR_DIALOGUE(ped, 3, "OJAvaGUARD")
					CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_LIMO", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("OJASHOTEL - PLAY_GUARD_PANIC_LINE - OJASva_LIMO")
				ELIF failType = FT_STICKY
					SETUP_PED_FOR_DIALOGUE(GET_CLOSEST_GUARD_TO_LIMO(25.0), 6, "OJAvaGUARD2")
					PLAY_SINGLE_LINE_FROM_CONVERSATION (hotelConv, "OJASAUD", "OJASva_CAR3", "OJASva_CAR3_1", CONV_PRIORITY_VERY_HIGH)
				ELSE
					SETUP_PED_FOR_DIALOGUE(ped, 3, "OJAvaGUARD")
					CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_GTFO3", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("OJASHOTEL - PLAY_GUARD_PANIC_LINE - GTFO3")
				ENDIF
				
				bGuardPanicLinePlayed = TRUE
			ELSE
				tlCurrentConvoRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				IF NOT ARE_STRINGS_EQUAL(tlCurrentConvoRoot, "OJAS_FEED")
					KILL_ANY_CONVERSATION()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Checks to see if bGuardPanicLinePlayed is set to FALSE and if so plays the appropriate panic line
/// PARAMS:
///    ped - speaker
PROC PLAY_TARGET_DEAD_LINE(PED_INDEX ped)
	IF NOT bTargetDeadLinePlayed
		IF DOES_ENTITY_EXIST(piTarget)
		AND IS_PED_INJURED(piTarget)	
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF DOES_ENTITY_EXIST(ped) AND NOT IS_PED_INJURED(ped)
				AND GET_PLAYER_DISTANCE_FROM_ENTITY(ped) < 75
					SETUP_PED_FOR_DIALOGUE(ped, 3, "OJAvaGUARD")
					CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJAS_FEED", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("OJASHOTEL - PLAY_GUARD_PANIC_LINE - OJAS_FEED")
					
					bTargetDeadLinePlayed = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: instantly trigger panic if these conditions are true
PROC TRIGGER_PANIC_NEAR_GUARDS_IF_PREV_WARNED(PED_INDEX testPed, VECTOR vGuardDistance)	
	IF NOT bPanicking
		IF NOT IS_PED_INJURED(testPed)
			IF IS_ENTITY_AT_ENTITY(Player, testPed, vGuardDistance)
				IF IS_PED_FACING_PED(testPed, Player, 160)
					IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(testPed, Player)
						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(testPed, Player, Player, PEDMOVE_WALK, FALSE, -1)
						
						failType = FT_DISREGARDED_WARNINGS
						bPanicking = TRUE
						PRINTLN("OJASHOTEL - TRIGGERING PANIC - PLAYER HAS DISREGARDED PREVIOUS WARNINGS - FT_DISREGARDED_WARNINGS")
//						PLAY_GUARD_PANIC_LINE(testPed)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(testPed)
					IF GET_SCRIPT_TASK_STATUS(testPed, SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) != PERFORMING_TASK
						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(testPed, Player, Player, PEDMOVE_WALK, FALSE, -1)
						PRINTLN("OJASHOTEL - TRIGGER_PANIC_NEAR_GUARDS_IF_PREV_WARNED - Walk while aiming!")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Ped should warn the player if they are seen
FUNC BOOL IS_PLAYER_SEEN_BY_HOTEL_PED(PED_INDEX testPed, FLOAT fDistToSee)
	
	IF NOT bPanicking
		IF NOT IS_ENTITY_IN_SAFE_LOCATION(Player)
			IF DOES_ENTITY_EXIST(testPed)
				IF NOT IS_PED_INJURED(testPed)
					IF GET_PLAYER_DISTANCE_FROM_ENTITY(testPed, FALSE) <= fDistToSee
						IF IS_PED_FACING_PED(testPed, Player, 120)
							IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(testPed, Player/*, LOS_FLAGS_BOUNDING_BOX*/)
							
								//TODO - Ask Dave if he knows what these angled areas are (2/19/13 - MB)
								IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1251.791, -147.043, 42.679 >>, << -1197.245, -212.232, 50.679 >>, 35.000, TRUE, TRUE)
								AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1259.282, -204.172, -60.654 >>, << -1304.335, -235.719, 60.654 >>, 85.000, TRUE, TRUE)
									PRINTLN("TRIGGERING IS_PLAYER_SEEN_BY_HOTEL_PED 01")
//									PRINTLN("fDistToSee = ", fDistToSee)
									RETURN TRUE
								ELSE
									PRINTLN("PLAYER IS IN ANGLED AREA")
								ENDIF
							ELSE
								PRINTLN("LOS CHECK HAS FAILED IN CHECK 01")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(testPed)
				IF NOT IS_PED_INJURED(testPed)
					IF IS_ENTITY_AT_ENTITY(Player, testPed, << 5, 5, 1 >>)
//					OR IS_NON_SCRIPT_CREATED_VEHICLE_IN_HOTEL_AREA(viCarThatTriggeredAlert) AND IS_PED_ON_SPECIFIC_VEHICLE(Player, viCarThatTriggeredAlert) AND GET_PLAYER_DISTANCE_FROM_ENTITY(testPed, FALSE) <= fDistToSee
						IF IS_PED_FACING_PED(testPed, Player, 120)
							IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(testPed, Player)
								PRINTLN("TRIGGERING IS_PLAYER_SEEN_BY_HOTEL_PED 02")
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DID_PED_SEE_PLAYER_PLANTING_BOMB(PED_INDEX testPed)
	//do these checks if after the player has been warned or the guards are leaving the lobby
	IF bWarningLinePlayed OR MainStage >= STAGE_GUARDS_GO_TO_POSTS
		IF IS_PED_PLANTING_BOMB(Player)
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(testPed) < 10
				IF IS_PED_FACING_PED(testPed, Player, 90)
					IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(testPed, Player)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Event trap to see if the player is attacking the target, guards, or the target's escort
FUNC BOOL HAS_PLAYER_AGGROED_HOTEL_PED(PED_INDEX testPed)
	VEHICLE_INDEX tempVehicle
	
	IF NOT bPanicking	
		IF DOES_ENTITY_EXIST(testPed)
			IF NOT IS_PED_INJURED(testPed)
				IF IS_PED_IN_ANY_VEHICLE(testPed)
					tempVehicle = GET_VEHICLE_PED_IS_IN(testPed)
				ELSE
					tempVehicle = NULL
				ENDIF
			ENDIF
			
			IF DO_AGGRO_CHECK(testPed, tempVehicle, aggroArgs, aggroReason, FALSE, FALSE)
			OR IS_PED_RAGDOLL(testPed) AND GET_PLAYER_DISTANCE_FROM_ENTITY(testPed) < 1.5 
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	
	RETURN FALSE
ENDFUNC


PROC DETACH_TOW_TRUCK_FROM_LIMO_IF_ATTACHED()
	VEHICLE_INDEX tempVeh
	
	//checks to see if player is trying to mess with the target car in a tow truck
	IF IS_PED_IN_ANY_VEHICLE(Player)
		tempVeh = GET_VEHICLE_PED_IS_IN(Player)
		IF GET_ENTITY_MODEL(tempVeh) = TOWTRUCK
		OR GET_ENTITY_MODEL(tempVeh) = TOWTRUCK2
			bPlayerInTowTruck = TRUE
		ENDIF
	ELSE
		bPlayerInTowTruck = FALSE
	ENDIF
	
	IF bPlayerInTowTruck
	 	IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(tempVeh, viTargetCar)
			DETACH_VEHICLE_FROM_TOW_TRUCK(tempVeh, viTargetCar)
		ENDIF
	ENDIF
ENDPROC


// Stickybomb checks on the limo while parked
FUNC BOOL IS_STICKYBOMB_VISIBLE_ON_PARKED_LIMO(eFailType& failReason)
	
	IF IS_VEHICLE_DRIVEABLE(viTargetCar)
		IF HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(PLAYER_PED_ID(), viTargetCar, WEAPONTYPE_STICKYBOMB)			
		
			// checks the entire limo area above the bumper to the roof
			IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-1223.024170,-187.307602,39.025375>>, <<-1220.311157,-186.248093,40.425377>>, 6.500000, WEAPONTYPE_STICKYBOMB)
			
			// excludes the front/rear bumpers - checks the limo area from the bottom of the limo to the roof
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-1223.024170,-187.307602,38.025375>>, <<-1220.311157,-186.248093,40.425377>>, 4.500000, WEAPONTYPE_STICKYBOMB)
				PRINTLN("Projectile detected in obvious location on limo.")
				failReason = FT_STICKY
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



//FUNC BOOL IS_PLAYER_THROWING_STICKYBOMBS_AT_VEHICLE(VEHICLE_INDEX vehToCheck)
//	WEAPON_TYPE wtReturn
//	ENTITY_INDEX entFreeAim
//	
//	IF DOES_ENTITY_EXIST(vehToCheck)
//		IF IS_VEHICLE_DRIVEABLE(vehToCheck)
//			IF GET_PED_IN_VEHICLE_SEAT(vehToCheck) != NULL		//make sure there are peds inside
//				IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtReturn)
//					IF wtReturn = WEAPONTYPE_STICKYBOMB
//						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehToCheck) < 40		//player throwing range
//							IF GET_ENTITY_PLAYER_IS_FREE_AIMING_AT(PLAYER_ID(), entFreeAim)
//								IF IS_ENTITY_A_VEHICLE(entFreeAim)
//								AND GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entFreeAim) = vehToCheck
//								OR GET_PED_INDEX_FROM_ENTITY_INDEX(entFreeAim) = GET_PED_IN_VEHICLE_SEAT(vehToCheck)
//									IF ( IS_PED_ON_FOOT(PLAYER_PED_ID()) AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK) )
//									OR ( IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK) )
//										PRINTLN("IS_PLAYER_THROWING_STICKYBOMBS_AT_VEHICLE  - RETURNED TRUE!")
//										RETURN TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC


FUNC BOOL IS_PLAYER_ATTEMPTING_JACK_ON_LOCKED_CAR(VEHICLE_INDEX vehToCheck)

	IF DOES_ENTITY_EXIST(vehToCheck)
		IF IS_VEHICLE_DRIVEABLE(vehToCheck)
			IF GET_VEHICLE_DOOR_LOCK_STATUS(vehToCheck) = VEHICLELOCK_LOCKOUT_PLAYER_ONLY
			AND GET_VEHICLE_PED_IS_ENTERING(Player) = vehToCheck
					PRINTLN("PLAYER ATTEMPTING JACK ON CAR RETURNING TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Set panic states for each ped group
FUNC BOOL DO_ADDITIONAL_HOTEL_PANIC_CHECKS(eFailType & failReason)
	VEHICLE_INDEX tempVeh
	FLOAT fShootDistance
	WEAPON_TYPE wtReturn
	
	IF NOT bPanicking
		
		//check to see if the guards noticed C4 on the car
		IF bTriggerGuardWalk
		AND NOT bLeftHotel
			//maybe add a bool here to only check once so that it doesn't try to check this every frame?
			IF IS_STICKYBOMB_VISIBLE_ON_PARKED_LIMO(failType)
				failReason = FT_STICKY
				PRINTLN("IS_STICKYBOMB_VISIBLE_ON_PARKED_LIMO IS RETURNING TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_PED_ARMED(Player, ENUM_TO_INT(WF_INCLUDE_GUN))
			IF IS_PED_SHOOTING(Player)
			
				fShootDistance = GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), <<-1230.5994, -196.5408, 38.1528>>)	//next to fountain
				PRINTLN("fShootDistance = ", fShootDistance)
	
				GET_CURRENT_PED_WEAPON(Player, wtReturn)
	
				IF fShootDistance > 15 AND (wtReturn = WEAPONTYPE_SNIPERRIFLE) OR (wtReturn = WEAPONTYPE_HEAVYSNIPER) OR (wtReturn = WEAPONTYPE_REMOTESNIPER) OR (wtReturn = WEAPONTYPE_DLC_MARKSMANRIFLE)
					IF DOES_ENTITY_EXIST(piTarget)
						bAlerted = TRUE
						bPlayerUsedSniperRifle = TRUE
						PRINTLN("SETTING bAlerted AND bPlayerUsedSniperRifle BECAUSE PLAYER IS FIRING WITH A SNIPER RIFLE")
					ENDIF
				ELSE
					IF fShootDistance < 50
						PRINTLN("OJASHOTEL - Ped panicking because player is shooting near lobby!")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYER_THROWING_STICKYBOMBS_AT_VEHICLE(viTargetCar)
		OR IS_PLAYER_ATTEMPTING_JACK_ON_LOCKED_CAR(viTargetCar)
		OR ( DOES_ENTITY_EXIST(viTargetCar) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(viTargetCar, Player) )
			PRINTLN("ADDITONAL PANIC CHECKS ON LIMO RETURNED TRUE")
			RETURN TRUE
		ENDIF
		
		IF IS_PLAYER_THROWING_STICKYBOMBS_AT_VEHICLE(viEscortCar)
		OR ( DOES_ENTITY_EXIST(viEscortCar) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(viEscortCar, Player) )
			PRINTLN("ADDITONAL PANIC CHECKS ON ESCORT CAR RETURNED TRUE")
			RETURN TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(viTargetCar) AND NOT IS_ENTITY_DEAD(viTargetCar)
			IF IS_PED_IN_VEHICLE(Player, viTargetCar)
				SET_VEHICLE_ALARM(viTargetCar, TRUE)
				START_VEHICLE_ALARM(viTargetCar)
			
				PRINTLN("TRIGGERING PANIC, BECAUSE THE PLAYER IS IN THE TARGET CAR")
				RETURN TRUE
			ENDIF
		ENDIF
		
		//check to see if player is messing with target's vehicle
		IF DOES_ENTITY_EXIST(viTargetCar)
			IF IS_VEHICLE_DRIVEABLE(viTargetCar)
				IF IS_PED_IN_ANY_VEHICLE(Player)
					tempVeh = GET_VEHICLE_PED_IS_IN(Player)
					IF IS_ENTITY_TOUCHING_ENTITY(tempVeh, viTargetCar)
					OR ( bPlayerInTowTruck AND IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(tempVeh, viTargetCar) )
						failReason = FT_BLOWN_COVER_HIT_PARKED_CAR
						PRINTLN("OJASHOTEL - Ped panicking because player is touching the target's car or has attaching a tow truck to it")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//stage where target is told to enter limo
		IF MainStage = STAGE_ENTER_LIMO
			IF IS_VEHICLE_DRIVEABLE(viTargetCar)
				IF GET_DISTANCE_BETWEEN_ENTITIES(Player, viTargetCar) <= 15
					PRINTLN("OJASHOTEL - PANIC - Player approached the limo after target has left - immedidately trigger panic")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		
		//check to see if the player is throwing stickybombs inside of the hotel area after being warned by one of the limo guards 
		//we need to run this check here to ensure that it gets hit every frame since it does a check for just pressed
		IF LimoGuardState[0] = LGS_WARNING_CHECK
		OR LimoGuardState[1] = LGS_WARNING_CHECK
		OR LimoGuardState[2] = LGS_WARNING_CHECK
			IF IS_PLAYER_THROWING_STICKYBOMBS()
				PRINTLN("OJASHOTEL - PANIC - PLAYER THROWING STICKYBOMBS AFTER BEING WARNED IN THE HOTEL AREA")
				RETURN TRUE
			ENDIF
		ENDIF
		
		
		//check the hotel area for an explosion and don't worry about the alert state if an explosion happens prior to the target exiting
		IF Mainstage < STAGE_TARGET_LEAVE_LOBBY
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vHotelCenterPos, fHotelCenterRadius)
				PRINTLN("OJASHOTEL - PANIC - Explosion near the hotel!!")
				RETURN TRUE
			ENDIF
		ELSE
			//otherwise if the player is inside of the parking garage and causes an explosion after the target leaves, lets set a bool to make the alerted check kick in
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vHotelCenterPos, fHotelCenterRadius)
				IF IS_PLAYER_IN_PARKING_GARAGE()
					bPlayerTriggeredExplosionFromParkingGarage = TRUE
					PRINTLN("bPlayerTriggeredExplosionFromParkingGarage = TRUE")
				ELSE
					//if the player isnt in the parking garage trigger straight up panic
					PRINTLN("OJASHOTEL - PANIC - Explosion near the hotel and player not in the garage!")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		
//		//check a central vector near the fountain
//		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vHotelStart, 30)
//		AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_STEAM, vHotelStart, 45)
//		AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_WATER_HYDRANT, vHotelStart, 45)
//			PRINTLN("OJASHOTEL - PANIC - Explosion near the hotel!!")
//			RETURN TRUE
//		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Remove an enemy's blip if he is killed
PROC REMOVE_ENEMY_BLIP_IF_KILLED(PED_INDEX ped, BLIP_INDEX blip)
	IF DOES_ENTITY_EXIST(ped)
		IF IS_PED_INJURED(ped)
			IF DOES_BLIP_EXIST(blip)
				REMOVE_BLIP(blip)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC VECTOR GET_RANDOM_COORDINATE()
	VECTOR vToReturn
	INT iRand
	
	iRand = GET_RANDOM_INT_IN_RANGE() % 3
	SWITCH iRand
		CASE 0
			vToReturn = <<-1270.2063, -200.3586, 40.8333>>
			PRINTLN("vToReturn = ", vToReturn)
		BREAK
		CASE 1
			vToReturn = <<-1268.4390, -215.9683, 59.6540>>
			PRINTLN("vToReturn = ", vToReturn)
		BREAK
		CASE 2
			vToReturn = <<-1241.5585, -239.3237, 38.7884>>
			PRINTLN("vToReturn = ", vToReturn)
		BREAK
	ENDSWITCH
	
	RETURN vToReturn
ENDFUNC




/// PURPOSE: Switch the ped's move state to run if panic is triggered while walking to the limo or if the player has been warned and is not in a safe location
FUNC BOOL MAKE_PED_RUN_IF_PANICKING(PED_INDEX ped)
	SEQUENCE_INDEX iSeq
	
//	IF DO_AGGRO_CHECK(ped, NULL, aggroArgs, aggroReason, TRUE, FALSE)
	IF bPanicking
	OR bAlerted
	OR bAlertedSpecial
		IF NOT IS_PED_INJURED(ped)
		
			PRINTLN("SETTING PEDMOVEBLENDRATIO_RUN ON PED")
			
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				STOP_SCRIPTED_CONVERSATION(FALSE)
				PRINTLN("STOPPING CONVERSATIONS VIA MAKE_PED_RUN_IF_PANICKING")
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(ped)
				IF NOT bTargetFleeing
					CLEAR_SEQUENCE_TASK(iSeq)
					OPEN_SEQUENCE_TASK(iSeq)
						TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@hotel@", "alert_gunshot", NORMAL_BLEND_IN, SLOW_BLEND_OUT, 1000)
						TASK_SMART_FLEE_PED(NULL, Player, 200, -1)
					CLOSE_SEQUENCE_TASK(iSeq)
					IF NOT IS_ENTITY_DEAD(piTarget)
						TASK_PERFORM_SEQUENCE(piTarget, iSeq)
						PRINTLN("TASKING TARGET TO PERFORM SEQUENCE VIA MAKE_PED_RUN_IF_PANICKING")
					ENDIF
					CLEAR_SEQUENCE_TASK(iSeq)
					
					bTargetFleeing = TRUE
					
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("OUR PED IS NOT IN A VEHICLE")
			ENDIF
		ENDIF
	ELSE
//		PRINTLN("OUR CHECKS ARE FAILING!!")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_CAR()
	VECTOR vPlayerPosition, vCarPosition
	FLOAT fDistanceToCar

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF NOT IS_ENTITY_DEAD(viTargetCar)
			vCarPosition = GET_ENTITY_COORDS(viTargetCar)
		ENDIF
		
		fDistanceToCar = VDIST2(vPlayerPosition, vCarPosition)
		PRINTLN("fDistanceToCar = ", fDistanceToCar)
		
		IF fDistanceToCar <= 25 // 5m
			PRINTLN("RETURNING TRUE - IS_PLAYER_NEAR_CAR")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC PLAY_SINGLE_LINE_WARNING()
	PED_INDEX pedSpeaker
	
	IF NOT bWarningLinePlayed
		IF NOT bLimoCruisingStreets
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				pedSpeaker = GET_CLOSEST_GUARD()
				SETUP_PED_FOR_DIALOGUE(pedSpeaker, 3, "OJAvaGUARD")
			
				IF IS_PLAYER_NEAR_CAR()
					CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_CAR", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("PLAYING LINE TO GET AWAY FROM THE CAR")
					bWarningLinePlayed = TRUE
				ELSE
					PLAY_SINGLE_LINE_FROM_CONVERSATION (hotelConv, "OJASAUD", "OJASva_GTFO2", "OJASva_GTFO2_1", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("PLAYING LINE TO LEAVE THE AREA")
					bWarningLinePlayed = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//tell guard to exit a vehicle if in one and warn the player by aiming their gun and possibly approaching them
PROC WARN_PLAYER_AND_WALK_TOWARDS(PED_INDEX& ped)
	SEQUENCE_INDEX iSeq
	
	
	IF piWalkingGuard = NULL
		piWalkingGuard = GET_CLOSEST_GUARD(70, FALSE)
	ENDIF
	
	
	IF NOT IS_PED_INJURED(ped)
		IF NOT IS_PED_IN_ANY_VEHICLE(ped)
			IF NOT bGuardWalkingTowardsPlayer				//we only want one guard to walk towards the player
				IF ped = piWalkingGuard
					CLEAR_SEQUENCE_TASK(iSeq)
					OPEN_SEQUENCE_TASK(iSeq)
						TASK_GO_TO_ENTITY(NULL, Player, DEFAULT_TIME_BEFORE_WARP, 2.0, PEDMOVEBLENDRATIO_WALK)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, Player, -1)
					CLOSE_SEQUENCE_TASK(iSeq)
					TASK_PERFORM_SEQUENCE(ped, iSeq)
					CLEAR_SEQUENCE_TASK(iSeq)

					bGuardWalkingTowardsPlayer = TRUE
					VECTOR tempVec = GET_ENTITY_COORDS(ped)
					PRINTLN("bGuardWalkingTowardsPlayer = TRUE. Guard located at << ", tempVec.x, ", ", tempVec.y, ", ", tempVec.z, " >>.")
				ELSE
					IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK
						TASK_TURN_PED_TO_FACE_ENTITY(ped, Player, -1)
						PRINTLN("TASKING PED TO TURN TOWARD THE PLAYER")
					ELSE
						PRINTLN("PED IS PERFORMING WALK TO TASK")
					ENDIF
				ENDIF
			ELSE
				IF ped != piWalkingGuard
					IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK
						TASK_TURN_PED_TO_FACE_ENTITY(ped, Player, -1)
						PRINTLN("TASKING PED TO TURN TOWARD THE PLAYER")
					ELSE
						PRINTLN("PED IS PERFORMING WALK TO TASK")
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//sets them targetable
		SET_PED_CAN_BE_TARGETTED(ped, TRUE)
		
		//will handle playing of warning line if not already played
		PLAY_SINGLE_LINE_WARNING()
	ENDIF
ENDPROC



//PURPOSE: Have the motorcade ped return to his vehicle ; 0 = guy in passenger seat, 1 = guy incar
PROC RETASK_MOTORCADE_PED_TO_RETURN_TO_VEHICLE(INT iEscort)
	VEHICLE_SEAT vsEscort
	
	//set this to true as player has been warned
	bWarned = TRUE
	PRINTLN("bWarned = TRUE 02")
	
	IF iEscort = 0
		vsEscort = VS_DRIVER
	ELIF iEscort = 1
		vsEscort = VS_FRONT_RIGHT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(piEscortPed[iEscort])
		IF IS_VEHICLE_DRIVEABLE(viEscortCar)
			IF NOT IS_PED_IN_VEHICLE(piEscortPed[iEscort], viEscortCar)
				IF GET_SCRIPT_TASK_STATUS(piEscortPed[iEscort], SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
					TASK_ENTER_VEHICLE(piEscortPed[iEscort], viEscortCar, DEFAULT_TIME_BEFORE_WARP, vsEscort)
					PRINTLN("TASKING PED TO ENTER VEHICLE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//ensures that the ped that has been tasked to advance on the player while aiming retains their task
PROC RETASK_PED_TO_WALK_WHILE_AIMING_AT_PLAYER(PED_INDEX ped)
	IF NOT IS_PED_INJURED(ped)
		IF GET_PLAYER_DISTANCE_FROM_ENTITY(ped) < 5
			IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK
				TASK_AIM_GUN_AT_ENTITY(ped, Player, -1)
				PRINTLN("OJASHOTEL - RETASK_PED_TO_WALK_WHILE_AIMING_AT_PLAYER - RETASKING PED TO STAND AND AIM!")
			ENDIF
		ELSE
			IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK
				//make sure we only have one guy doing this
				IF ped = piWalkingGuard
					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(ped, Player, Player, PEDMOVE_WALK, FALSE, -1)
				ELSE
					IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK
						TASK_AIM_GUN_AT_ENTITY(ped, Player, -1)
					ENDIF
				ENDIF
				
				PRINTLN("OJASHOTEL - RETASK_PED_TO_WALK_WHILE_AIMING_AT_PLAYER - RETASKING PED TO WALK AND AIM")
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC TASK_LIMO_GUARDS_TO_ENTER_VEHICLES()
	INT idx
	REPEAT NUM_LIMO_GUARDS idx
		LimoGuardState[idx] = LGS_ENTER_LIMO
	ENDREPEAT
	PRINTLN("bLimoGuardsTaskedToEnterVehicle = TRUE")
	bLimoGuardsTaskedToEnterVehicle = TRUE
ENDPROC


/// PURPOSE:
///    Ensures that the vehicle's driver uses their vehicle to try and attack the player (to help fix a bug about guards not getting to the player fast enough)
/// PARAMS:
///    veh - vehicle to use to combat player with
///    ped - driver of the vehicle
///    fDistFromPlayerBeforeEnteringVeh - Threshold distance that the ped is away from the player before being told to enter the vehicle
///    fDistanceFromVehSafeToEnter - distance between the ped and his vehicle to be safe to enter the vehice
///    fDistToPlayerToExitVehAndAttackOnFoot - once the ped reaches the player (and the player is on foot) exit the vehicle and attack at this distance
PROC UPDATE_PED_ATTACKING_IN_VEHICLE(VEHICLE_INDEX veh, PED_INDEX ped, FLOAT fDistFromPlayerBeforeEnteringVeh = 50.0, FLOAT fDistanceFromVehSafeToEnter = 50.0, FLOAT fDistToPlayerToExitVehAndAttackOnFoot = 10.0)
	IF NOT IS_PED_INJURED(ped)
		IF IS_VEHICLE_DRIVEABLE(veh)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF GET_DISTANCE_BETWEEN_ENTITIES(ped, veh, FALSE) > fDistFromPlayerBeforeEnteringVeh
				AND GET_PLAYER_DISTANCE_FROM_ENTITY(ped) < fDistanceFromVehSafeToEnter
					IF NOT IS_PED_IN_VEHICLE(ped, veh)
						IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
							TASK_ENTER_VEHICLE(ped, veh)
							PRINTLN("OJASHOTEL - UPDATE_PED_ATTACKING_IN_VEHICLE - enter vehicle")
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
							TASK_VEHICLE_MISSION(ped, veh, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_ATTACK, 35.0, DRIVINGMODE_AVOIDCARS, -1, -1)
							ADD_VEHICLE_SUBTASK_ATTACK_PED(ped, PLAYER_PED_ID())
							PRINTLN("OJASHOTEL - UPDATE_PED_ATTACKING_IN_VEHICLE - TASK_VEHICLE_MISSION")
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_IN_VEHICLE(ped, veh)
						IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_COMBAT) != PERFORMING_TASK
							TASK_COMBAT_PED(ped, PLAYER_PED_ID())
							PRINTLN("OJASHOTEL - UPDATE_PED_ATTACKING_IN_VEHICLE - TASK_COMBAT_PED - not far enough away to try and enter vehicle")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_PED_IN_VEHICLE(ped, veh)
					IF GET_PLAYER_DISTANCE_FROM_ENTITY(ped) > fDistToPlayerToExitVehAndAttackOnFoot
						IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
							TASK_VEHICLE_MISSION_PED_TARGET(ped, veh, PLAYER_PED_ID(), MISSION_ATTACK, 35.0, DRIVINGMODE_AVOIDCARS, -1, -1)
							ADD_VEHICLE_SUBTASK_ATTACK_PED(ped, PLAYER_PED_ID())
							PRINTLN("OJASHOTEL - UPDATE_PED_ATTACKING_IN_VEHICLE - TASK_VEHICLE_MISSION_PED_TARGET")
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_COMBAT) != PERFORMING_TASK
							TASK_COMBAT_PED(ped, PLAYER_PED_ID())
							PRINTLN("OJASHOTEL - UPDATE_PED_ATTACKING_IN_VEHICLE - TASK_COMBAT_PED < fDistToPlayerToExitVehAndAttackOnFoot")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_COMBAT) != PERFORMING_TASK
				TASK_COMBAT_PED(ped, PLAYER_PED_ID())
				PRINTLN("OJASHOTEL - UPDATE_PED_ATTACKING_IN_VEHICLE - TASK_COMBAT_PED")
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Update the target state
PROC UPDATE_ESCORT_STATE(INT iEscort)
	PED_INDEX escortPed
	escortPed = piEscortPed[iEscort]
	
	SWITCH EscortState[iEscort]
		CASE ES_WAITING
			IF bDriveToLimo
				EscortState[iEscort] = ES_DRIVE_TO_LIMO
				PRINTLN("OJASHOTEL - EscortState[", iEscort, "] = ES_DRIVE_TO_LIMO")
			ELIF bLeaveInLimo
				EscortState[iEscort] = ES_PROTECT
				PRINTLN("OJASHOTEL - EscortState[", iEscort, "] = ES_PROTECT")
//			ELSE
//				IF bPanicking
//					IF NOT IS_ENTITY_DEAD(escortPed)
//						IF GET_SCRIPT_TASK_STATUS(escortPed, SCRIPT_TASK_COMBAT) != PERFORMING_TASK
//							PRINTLN("OJASHOTEL - TASK LIMO GUARD TO COMBAT IN LGS_LEAVE_IN_LIMO")
//							TASK_COMBAT_PED(escortPed, Player)
//						ENDIF
//					ENDIF
//				ENDIF
			ENDIF
		BREAK
		
		CASE ES_DRIVE_TO_LIMO
			//play the vehicle recordings and wait
			IF IS_VEHICLE_DRIVEABLE(viEscortCar)
				IF ARE_BOTH_ESCORT_PEDS_IN_VEHICLE()
					IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(viEscortCar)
						IF GET_PED_IN_VEHICLE_SEAT(viEscortCar) = piEscortPed[iEscort]
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(piEscortPed[iEscort], viEscortCar, "OJASva_104", DF_SwerveAroundAllCars|DF_StopAtLights|DF_SteerAroundObjects|DF_SteerAroundPeds|DF_GoOffRoadWhenAvoiding|DF_DriveIntoOncomingTraffic, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, 18, 6)
							PRINTLN("OJASHOTEL - TASKING piEscortPed[", iEscort, "] TO DRIVE TO LIMO - using AI_SLOWDOWN")
						ENDIF
					ENDIF
					
					EscortState[iEscort] = ES_DRIVING_TO_LIMO
					PRINTLN("OJASHOTEL - EscortState[", iEscort, "] = ES_DRIVING_TO_LIMO - TASKED TO FOLLOW WAYPOINT RECORDING")
				ELSE
					PRINTLN("HERE 02")
					RETASK_MOTORCADE_PED_TO_RETURN_TO_VEHICLE(iEscort)
				ENDIF
			ENDIF
		BREAK
		
		CASE ES_DRIVING_TO_LIMO
			IF IS_VEHICLE_DRIVEABLE(viEscortCar)
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(viEscortCar)
					
					VECTOR vTemp
					FLOAT fTemp
					
					vTemp = GET_ENTITY_COORDS(viEscortCar)
					fTemp = GET_ENTITY_HEADING(viEscortCar)
					
					PRINTLN("VECTOR = ", vTemp)
					PRINTLN("HEADING = ", fTemp)
					
					bDriveToLimo = FALSE
					EscortState[iEscort] = ES_WAITING
					PRINTLN("OJASHOTEL - EscortState[", iEscort, "] = ES_WAITING 01")
				ENDIF
			ENDIF
		BREAK

		CASE ES_PROTECT
		
			//tell the motorcade to escort the limo
			IF IS_VEHICLE_DRIVEABLE(viEscortCar)
				IF NOT IS_ENTITY_DEAD(piEscortPed[iEscort])
					IF IS_PED_IN_VEHICLE(piEscortPed[iEscort], viEscortCar)
					AND IS_VEHICLE_DRIVEABLE(viTargetCar)
						IF NOT bPanicking
							IF GET_PED_IN_VEHICLE_SEAT(viEscortCar) = piEscortPed[iEscort]
								IF GET_SCRIPT_TASK_STATUS(piEscortPed[iEscort], SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//									TASK_VEHICLE_MISSION(piEscortPed[iEscort], viEscortCar, viTargetCar, MISSION_ESCORT_REAR, 45, DRIVINGMODE_AVOIDCARS, -1, -1)
									TASK_VEHICLE_ESCORT(piEscortPed[iEscort], viEscortCar, viTargetCar, VEHICLE_ESCORT_REAR, 45, DRIVINGMODE_AVOIDCARS|DF_DriveIntoOncomingTraffic, 10, -1, 10)
									PRINTLN("TASKING - TASK_VEHICLE_MISSION ON ESCORT PED DRIVER")
								ENDIF
							ENDIF
						ELSE
							// Driver only
							IF GET_PED_IN_VEHICLE_SEAT(viEscortCar) = piEscortPed[iEscort]
								IF NOT bCombatSubtaskSet
									IF IS_ENTITY_DEAD(piTarget)
										IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
											TASK_COMBAT_PED(escortPed, PLAYER_PED_ID())
											PRINTLN("PLAYER IS IN A VEHICLE - TASKING ESCORT DRIVER GUARD TO COMBAT")
										ELSE
											PRINTLN("PLAYER IS NOT IN A VEHICLE - SENDING ESCORT GUARD[", iEscort, "] TO PANIC")
											EscortState[iEscort] = ES_PANIC
										ENDIF
									ELSE
//										TASK_VEHICLE_MISSION(escortPed, viEscortCar, viTargetCar, MISSION_ESCORT_REAR, 45, DRIVINGMODE_AVOIDCARS, -1, -1)
										TASK_VEHICLE_ESCORT(piEscortPed[iEscort], viEscortCar, viTargetCar, VEHICLE_ESCORT_REAR, 45, DRIVINGMODE_AVOIDCARS|DF_DriveIntoOncomingTraffic, 10, -1, 10)
										ADD_VEHICLE_SUBTASK_ATTACK_PED(escortPed, PLAYER_PED_ID())
										PRINTLN("TASKING ESCORT GUARD DRIVER TO VEHICLE ESCORT")
										bCombatSubtaskSet = TRUE
									ENDIF
								ENDIF
							ELSE
								IF NOT bTargetFleeing
									IF GET_SCRIPT_TASK_STATUS(piEscortPed[iEscort], SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
										IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
											IF GET_ENTITY_SPEED(viEscortCar) < 5.0
												EscortState[iEscort] = ES_PANIC
												PRINTLN("PLAYER IS NOT IN A VEHICLE - SENDING ESCORT GUARD PASSENGER TO PANIC - CAR NOT MOVING FAST")
											ENDIF
										ENDIF
										TASK_COMBAT_PED(escortPed, PLAYER_PED_ID())
										PRINTLN("ESCORT: TARGET NOT FLEEING, TASKING ESCORT PED TO COMBAT WITH INDEX = ", iEscort)
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(piEscortPed[iEscort], SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
										TASK_COMBAT_PED(escortPed, PLAYER_PED_ID())
//										PRINTLN("ESCORT: TARGET FLEEING, TASKING TO COMBAT INDEX = ", iEscort)
									ENDIF
								ENDIF
							ENDIF	
						ENDIF
					ELSE
						IF NOT bAlerted
							EscortState[iEscort] = ES_PANIC
							PRINTLN("OJASHOTEL - EscortState[", iEscort, "SENDING TO ES_PANIC VIA TARGET CAR IS NOT DRIVEABLE")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ES_RESPOND_TO_PLAYER
			IF NOT IS_TIMER_STARTED(warnTimer)
				RESTART_TIMER_NOW(warnTimer)
				PRINTLN("OJASHOTEL - ES_RESPOND_TO_PLAYER - restarting timer")
			ENDIF
			
			//peds in vehicles will only play the warning line and not leave their vehicle currently (just say warning line)
			WARN_PLAYER_AND_WALK_TOWARDS(piEscortPed[iEscort])
			EscortState[iEscort] = ES_WARNING_CHECK
			PRINTLN("OJASHOTEL - EscortState[", iEscort, "] = ES_WARNING_CHECK")
		BREAK
		
		CASE ES_WARNING_CHECK
			IF NOT IS_ENTITY_IN_SAFE_LOCATION(Player)
				IF GET_TIMER_IN_SECONDS(warnTimer) >= 27
				OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF NOT IS_PED_INJURED(escortPed)
						IF IS_ENTITY_AT_ENTITY(Player, escortPed, <<5,5,3>>)
							PRINTLN("ESCORT STATE - TRIGGERING PANIC")
							TRIGGER_PANIC_NEAR_GUARDS_IF_PREV_WARNED(escortPed, <<5,5,3>>)
						ELSE
							PRINTLN("HERE 03")
							RETASK_MOTORCADE_PED_TO_RETURN_TO_VEHICLE(iEscort)
						ENDIF
					ENDIF
//				ELSE
//					RETASK_PED_TO_WALK_WHILE_AIMING_AT_PLAYER(escortPed)
				ENDIF
			ELSE
				EscortState[iEscort] = ES_WAITING
				PRINTLN("OJASHOTEL - EscortState[", iEscort, "] = ES_WAITING 02")
			ENDIF
		BREAK
		
		CASE ES_ALERTED
//			IF NOT IS_PED_INJURED(piEscortPed[iEscort])
//				VECTOR vTempPos = GET_ENTITY_COORDS(piEscortPed[iEscort])
//				//sets a peds defensive area around the target
//				SET_PED_SPHERE_DEFENSIVE_AREA(piEscortPed[iEscort], vTempPos, 15.0)
//				SET_PED_COMBAT_MOVEMENT(piEscortPed[iEscort], CM_DEFENSIVE)
//				SET_PED_COMBAT_ATTRIBUTES(piEscortPed[iEscort], CA_USE_COVER, TRUE)
//			ENDIF

			IF bAlertedSpecial
				OPEN_SEQUENCE_TASK(seqIndex)
					TASK_LEAVE_ANY_VEHICLE(NULL)
					TASK_LOOK_AT_COORD(NULL, GET_RANDOM_COORDINATE(), GET_RANDOM_INT_IN_RANGE(1000, 1500), SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					TASK_LOOK_AT_COORD(NULL, GET_RANDOM_COORDINATE(), GET_RANDOM_INT_IN_RANGE(2000, 2500), SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), GET_RANDOM_INT_IN_RANGE(10000, 11000), 30, PEDMOVEBLENDRATIO_WALK)
					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_COORD(NULL, PLAYER_PED_ID(), GET_RANDOM_COORDINATE(), PEDMOVEBLENDRATIO_WALK, FALSE)
				CLOSE_SEQUENCE_TASK(seqIndex) 
				
				IF NOT IS_PED_INJURED(piEscortPed[iEscort])
					TASK_PERFORM_SEQUENCE(piEscortPed[iEscort], seqIndex)
				ENDIF
				
				CLEAR_SEQUENCE_TASK(seqIndex)
			ELSE
				OPEN_SEQUENCE_TASK(seqIndex)
					TASK_LEAVE_ANY_VEHICLE(NULL)
//					TASK_SEEK_COVER_FROM_POS(NULL, vTempPos, -1)
					TASK_AIM_GUN_AT_COORD(NULL, GET_RANDOM_COORDINATE(), GET_RANDOM_INT_IN_RANGE(3000, 3500), TRUE)
					TASK_AIM_GUN_AT_COORD(NULL, GET_RANDOM_COORDINATE(), GET_RANDOM_INT_IN_RANGE(3000, 3500))
					TASK_AIM_GUN_AT_COORD(NULL, GET_RANDOM_COORDINATE(), GET_RANDOM_INT_IN_RANGE(3000, 3500))
					TASK_AIM_GUN_AT_COORD(NULL, GET_RANDOM_COORDINATE(), GET_RANDOM_INT_IN_RANGE(3000, 3500))
					TASK_AIM_GUN_AT_COORD(NULL, GET_RANDOM_COORDINATE(), -1)
					
				CLOSE_SEQUENCE_TASK(seqIndex)
				
				IF NOT IS_PED_INJURED(piEscortPed[iEscort])
					TASK_PERFORM_SEQUENCE(piEscortPed[iEscort], seqIndex)
				ENDIF
				
				CLEAR_SEQUENCE_TASK(seqIndex)
			ENDIF
			
			EscortState[iEscort] = ES_ALERTED2
			PRINTLN("OJASHOTEL - EscortState[", iEscort, "] = ES_ALERTED2")
		BREAK
		
		CASE ES_ALERTED2
			//ped has been tasked with alert tasks - wait here and retask should he lose his current task (bug 1511746 - 6/25/13 - MB)
			IF NOT IS_PED_INJURED(piEscortPed[iEscort])
				IF GET_SCRIPT_TASK_STATUS(piEscortPed[iEscort], SCRIPT_TASK_ANY) != PERFORMING_TASK
					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(piEscortPed[iEscort], Player, Player, PEDMOVE_WALK, FALSE, -1)
				ENDIF
			ENDIF
		BREAK
		
		//guard is panicked - handle attack response
		CASE ES_PANIC
			IF failType != FT_BLOWN_COVER_HIT_PARKED_CAR
			AND failType != FT_DISREGARDED_WARNINGS
				EXIT_VEHICLE_AND_ATTACK_PLAYER(escortPed)
			ELSE
				EXIT_VEHICLE_AND_AIM_GUN_AT_PLAYER(escortPed)
			ENDIF
			
			EscortState[iEscort] = ES_ATTACKING
			PRINTLN("OJASHOTEL - EscortState[", iEscort, "] = ES_ATTACKING")
		BREAK
		
		CASE ES_ATTACKING
			IF iEscort = 0
				IF failType != FT_BLOWN_COVER_HIT_PARKED_CAR
					UPDATE_PED_ATTACKING_IN_VEHICLE(viEscortCar, escortPed)
				ENDIF
			ELSE
				//empty state - ped has already been tasked to combat the player
			ENDIF
			
			PLAY_GUARD_PANIC_LINE(escortPed)
			
			PLAY_TARGET_DEAD_LINE(escortPed)
		BREAK
	ENDSWITCH
		
		
		
/////// ALWAYS RUN THESE PANIC/WARNING CHECKS
	IF NOT bPanicking
		IF bAlerted
			IF EscortState[iEscort] <> ES_ALERTED
			AND EscortState[iEscort] <> ES_ALERTED2
				EscortState[iEscort] = ES_ALERTED
				PRINTLN("OJASHOTEL - EscortState[", iEscort, "] = ES_ALERTED")
			ENDIF
		ELSE
			IF DO_AGGRO_CHECK(escortPed, viEscortCar, aggroArgs, aggroReason, FALSE, FALSE)
			OR CHECK_FOR_PLAYER_RAMMING_VEHICLE(viEscortCar)
//			OR bPlayerTriggeredExplosionFromParkingGarage
				PED_INDEX tempPed = GET_CLOSEST_GUARD()
				IF tempPed != NULL
					DO_ALERTED_CHECK(GET_ENTITY_COORDS(tempPed))
				ENDIF
				
				IF NOT SHOULD_TRIGGER_ALERT()
					bPanicking = TRUE
					PRINTLN("bPanicking = TRUE VIA HAS_PLAYER_AGGROED_HOTEL_PED - UPDATE_ESCORT_STATE")
//					IF NOT IS_PED_INJURED(escortPed)
//						PLAY_GUARD_PANIC_LINE(escortPed)
//					ENDIF
					PRINTLN("OJASHOTEL - PLAYER AGGROED EscortPED")
				ENDIF
			ELSE
				IF NOT bLeftHotel
					IF IS_PLAYER_SEEN_BY_HOTEL_PED(escortPed, 10)
						
						IF EscortState[iEscort] <> ES_RESPOND_TO_PLAYER
						AND EscortState[iEscort] <> ES_WARNING_CHECK
							IF NOT bWarned
								CLEAR_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_ShotNear)
								
	//							IF NOT DOES_BLIP_EXIST(bEscortBlip[iEscort])
	//								IF NOT IS_PED_INJURED(escortPed)
	//									bEscortBlip[iEscort] = CREATE_BLIP_ON_ENTITY(escortPed, FALSE)
	//								ENDIF
	//							ENDIF
								EscortState[iEscort] = ES_RESPOND_TO_PLAYER
								PRINTLN("OJASHOTEL - EscortState[", iEscort, " = ES_RESPOND_TO_PLAYER")
							ELSE
								IF NOT IS_ENTITY_IN_SAFE_LOCATION(Player)
									bPanicking = TRUE
//									PLAY_GUARD_PANIC_LINE(escortPed)
									IF NOT DOES_ENTITY_EXIST(piTarget)
										failType = FT_DISREGARDED_WARNINGS
									ENDIF
									EscortState[iEscort] = ES_PANIC
									PRINTLN("OJASHOTEL - PANIC - IS_PLAYER_SEEN_BY_HOTEL_PED IN ESCORT UPDATE.  EscortState[", iEscort, "] = ES_PANIC")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(escortPed)
			IF IS_PED_IN_VEHICLE(escortPed, viEscortCar)
				IF NOT IS_VEHICLE_DRIVEABLE(viEscortCar)
				OR NOT IS_VEHICLE_DRIVEABLE(viTargetCar)
				OR bLimoStuck
//				OR bLimoDestroyed
				OR NOT bLeftHotel
					IF EscortState[iEscort] < ES_PANIC
						EscortState[iEscort] = ES_PANIC	
						PRINTLN("OJASHOTEL - EscortState[", iEscort, " = ES_PANIC VIA PANIC BOOL BEING SET")
					ENDIF
				ENDIF
			ELSE
				IF EscortState[iEscort] < ES_PANIC
					EscortState[iEscort] = ES_PANIC	
					PRINTLN("OJASHOTEL - EscortState[", iEscort, " = ES_PANIC VIA PANIC BOOL BEING SET")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//handle escort blips
	REMOVE_ENEMY_BLIP_IF_KILLED(escortPed, bEscortBlip[iEscort])
ENDPROC

PROC UPDATE_GUARDS_AIMING(PED_INDEX testPed)
	SWITCH iGuardAimingState
		CASE 0
			IF NOT IS_ENTITY_DEAD(testPed)
//			AND testPed = piWalkingGuard
				IF GET_SCRIPT_TASK_STATUS(testPed, SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK
					TASK_AIM_GUN_AT_ENTITY(testPed, PLAYER_PED_ID(), -1)
					PRINTLN("TASKING LIMO GUY TO AIM")
				ENDIF
					
				SETUP_PED_FOR_DIALOGUE(testPed, 3, "OJAvaGUARD")
				CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_FWARN", CONV_PRIORITY_VERY_HIGH)
				bWarningLinePlayed = TRUE
				PRINTLN("bWarningLinePlayed = TRUE - VIA UPDATE_GUARDS_AIMING")
				iGuardAimingState = 1
				
//				bGaveSecondWarning = TRUE
	
//				ELSE
//					PRINTLN("STILL PERFORMING AIMING TASK")
//				ENDIF
			ENDIF
		BREAK
		CASE 1
			RETASK_PED_TO_WALK_WHILE_AIMING_AT_PLAYER(testPed)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_PATH_BLOCKED()
	
	INT idx
	
	VEHICLE_INDEX vehArray[10]
	iNumVehiclesFound = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), vehArray)
	iNumVehiclesFound = iNumVehiclesFound
	
	iNumVehiclesFound = COUNT_OF(vehArray)
	PRINTLN("iNumVehiclesFound = ", iNumVehiclesFound)
	
	REPEAT COUNT_OF(vehArray) idx
		IF DOES_ENTITY_EXIST(vehArray[idx]) AND NOT IS_ENTITY_DEAD(vehArray[idx])
			IF IS_ENTITY_IN_ANGLED_AREA(vehArray[idx], <<-1218.843506,-278.622986,36.819595>>, <<-1248.403320,-225.846725,43.156059>>, 8.500000)
				iNumVehiclesInArea++
				PRINTLN("NUMBER VEHICLES IN AREA = ", iNumVehiclesInArea)
			ELSE
				PRINTLN("NEARBY VEHICLES NOT IN ANGLED AREA: INDEX = ", idx)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iNumVehiclesInArea >= 1
		bPathIsBlocked = TRUE
		PRINTLN("RETURNING TRUE - IS_PATH_BLOCKED")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



FUNC BOOL ARE_ALL_GUARDS_DEAD_IN_LIMO()
	PED_INDEX tempPed
	
	IF IS_VEHICLE_DRIVEABLE(viTargetCar)
		tempPed = GET_PED_IN_VEHICLE_SEAT(viTargetCar, VS_DRIVER)
		IF tempPed = NULL
		OR ( tempPed != NULL AND IS_PED_INJURED(tempPed) )
			tempPed = GET_PED_IN_VEHICLE_SEAT(viTargetCar, VS_FRONT_RIGHT)
			IF tempPed = NULL
			OR ( tempPed != NULL AND IS_PED_INJURED(tempPed) )
				tempPed = GET_PED_IN_VEHICLE_SEAT(viTargetCar, VS_BACK_RIGHT)
				IF tempPed = NULL
				OR ( tempPed != NULL AND IS_PED_INJURED(tempPed) )
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_TARGET_PLAYED_ALL_PANIC_LINES()
	SWITCH iTargetPanicLine
		CASE 0
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PLAY_SINGLE_LINE_FROM_CONVERSATION(hotelConv, "OJASAUD", "OJASva_PANIC", "OJASva_PANIC_1", CONV_PRIORITY_HIGH)
				RESTART_TIMER_NOW(tTargetSpeakTimer)
				iTargetPanicLine ++
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND GET_TIMER_IN_SECONDS(tTargetSpeakTimer) > 7.0
				PLAY_SINGLE_LINE_FROM_CONVERSATION(hotelConv, "OJASAUD", "OJASva_PANIC", "OJASva_PANIC_2", CONV_PRIORITY_HIGH)
				RESTART_TIMER_NOW(tTargetSpeakTimer)
				iTargetPanicLine ++
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND GET_TIMER_IN_SECONDS(tTargetSpeakTimer) > 8.0
				PLAY_SINGLE_LINE_FROM_CONVERSATION(hotelConv, "OJASAUD", "OJASva_PANIC", "OJASva_PANIC_3", CONV_PRIORITY_HIGH)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: State machine for the peds who drive with the target in the SUV(formerly a limo)
PROC UPDATE_LIMO_GUARD_STATE(INT iLimoGuard)
	PED_INDEX limoPed
	FLOAT fCheckDist
	limoPed = piLimoGuard[iLimoGuard]


	
//	// Adding a flag, so if the player is given the final warning... leaves, then returns, we don't care anymore about being seen.
//	IF bGaveSecondWarning
//		IF IS_ENTITY_IN_SAFE_LOCATION(Player)
//			bDoNotRunPanicCheck = TRUE
////			PRINTLN("bDoNotRunPanicCheck = TRUE")
//		ENDIF
//	ENDIF


	SWITCH LimoGuardState[iLimoGuard]
		//task limo peds to enter the target vehicle - ensure that one follows the target on his way
		//tell the guard to go to their assigned post
		CASE LGS_WALK_TO_POSTS
			IF NOT IS_PED_INJURED(limoPed)
				CLEAR_SEQUENCE_TASK(seqIndex)
				OPEN_SEQUENCE_TASK(seqIndex)
					TASK_GO_TO_COORD_ANY_MEANS(NULL, vGuardPos[iLimoGuard], PEDMOVE_WALK, NULL)
					TASK_ACHIEVE_HEADING(NULL, fGuardHead[iLimoGuard])
				CLOSE_SEQUENCE_TASK(seqIndex)
				TASK_PERFORM_SEQUENCE(limoPed, seqIndex)
				CLEAR_SEQUENCE_TASK(seqIndex)
				
				LimoGuardState[iLimoGuard] = LGS_STAND_GUARD
				PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_STAND_GUARD")
			ENDIF
		BREAK
		
		//have guards cycle through idle animations when not currently tasked with an anim or sequence task
		CASE LGS_STAND_GUARD
			IF DOES_ENTITY_EXIST(limoPed)
				IF NOT IS_ENTITY_DEAD(limoPed)
					IF NOT bPanickedExtras // We're going to fail the player, no need to stay in animation
						IF GET_SCRIPT_TASK_STATUS(limoPed, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(limoPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
							IF IS_ENTITY_AT_COORD(limoPed, vGuardPos[iLimoGuard], <<3,3,3>>)
								UPDATE_GUARD_ANIMS(limoPed, iGuardAnim[iLimoGuard])
							ELSE
								LimoGuardState[iLimoGuard] = LGS_WALK_TO_POSTS
								PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_WALK_TO_POSTS")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//handle guard reaction to spotting the player ped
		CASE LGS_RESPOND_TO_PLAYER		
		
//			PRINTLN("INSIDE - LGS_RESPOND_TO_PLAYER !!!!!!!!!!")
		
			IF NOT IS_TIMER_STARTED(warnTimer)
				RESTART_TIMER_NOW(warnTimer)
				PRINTLN("RESTART TIMER NOW - warnTimer")
				WARN_PLAYER_AND_WALK_TOWARDS(limoPed)
				
				LimoGuardState[iLimoGuard] = LGS_WARNING_CHECK
				PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_WARNING_CHECK - Timer not started")
			ELSE
				//wait until delay time for the ped has been reached if set (we don't want all the guys snapping at once)
				IF GET_TIMER_IN_SECONDS(warnTimer) > fWarnTime[iLimoGuard]
				OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					WARN_PLAYER_AND_WALK_TOWARDS(limoPed)
					LimoGuardState[iLimoGuard] = LGS_WARNING_CHECK
					PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_WARNING_CHECK")
				ENDIF
			ENDIF
		BREAK
		
		//give player time to get away from the guard and handle the guard's reposnse based on the player's actions
		CASE LGS_WARNING_CHECK

			IF NOT IS_ENTITY_IN_SAFE_LOCATION(Player)
				
				bInPursuit = TRUE
				
				IF bTargetLeaving
					LimoGuardState[iLimoGuard] = LGS_ENTER_LIMO
					PRINTLN("SETTING STATE TO LGS_ENTER_LIMO ON GUARD: ", iLimoGuard)
					BREAK
				ELSE
					IF ( GET_TIMER_IN_SECONDS(warnTimer) >= 27 AND bWarningLinePlayed )
						TRIGGER_PANIC_NEAR_GUARDS_IF_PREV_WARNED(limoPed, <<15,15,3>>)
						PRINTLN("TRIGGERING PANIC - TIME IS UP!")
					ELIF GET_TIMER_IN_SECONDS(warnTimer) >= 15
						UPDATE_GUARDS_AIMING(limoPed)
						PRINTLN("UPDATE_GUARDS_AIMING - LimoGuardState[", iLimoGuard, "].")
//					ELSE
//						PRINTLN("UPDATE_GUARDS_AIMING - LimoGuardState[", iLimoGuard, "]. Doing nothing")
					ENDIF
				ENDIF
			ELSE
				bInPursuit = FALSE
				
				bWarned = TRUE
				PRINTLN("bWarned!!!")
	
				IF NOT bLimoGuardsTaskedToEnterVehicle
					LimoGuardState[iLimoGuard] = LGS_WALK_TO_POSTS
					PRINTLN("SENDING LIMO GUARD[", iLimoGuard, "] TO STATE - LGS_WALK_TO_POSTS")
				ELSE
					LimoGuardState[iLimoGuard] = LGS_ENTER_LIMO
					PRINTLN("SENDING LIMO GUARD[", iLimoGuard, "] TO STATE - LGS_ENTER_LIMO")
				ENDIF

			ENDIF
		BREAK
		CASE LGS_ENTER_LIMO
			
			//sets them targetable
			IF NOT IS_PED_INJURED(limoPed)
				SET_PED_CAN_BE_TARGETTED(limoPed, TRUE)
				SET_PED_RESET_FLAG(limoPed, PRF_SearchForClosestDoor, TRUE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(viTargetCar)	
				IF NOT IS_PED_INJURED(limoPed)
					PRINTLN("OJASHOTEL - TASK_LIMO_PEDS_TO_ENTER_LIMO")
					IF iLimoGuard = 0
						TASK_LOOK_AT_ENTITY(piLimoGuard[0], piLimoGuard[1], -1)
						IF NOT IS_PED_IN_VEHICLE(limoPed, viTargetCar)
							TASK_ENTER_VEHICLE(limoPed, viTargetCar, 30000, VS_DRIVER, PEDMOVE_WALK)
							PRINTLN("TASKING LIMO GUARD 0 TO ENTER VEHICLE")
						ENDIF
					ELIF iLimoGuard = 1
						TASK_LOOK_AT_ENTITY(piLimoGuard[1], piLimoGuard[0], -1)
						IF NOT IS_PED_IN_VEHICLE(limoPed, viTargetCar)	
							TASK_ENTER_VEHICLE(limoPed, viTargetCar, 30000, VS_FRONT_RIGHT, PEDMOVE_WALK)
							PRINTLN("TASKING LIMO GUARD 1 TO ENTER VEHICLE")
						ENDIF
					ELIF iLimoGuard = 2
						IF IS_VEHICLE_DRIVEABLE(viEscortCar)
							IF NOT IS_PED_IN_VEHICLE(limoPed, viEscortCar)
								TASK_ENTER_VEHICLE(limoPed, viEscortCar, 30000, VS_BACK_LEFT, PEDMOVE_WALK)
								PRINTLN("TASKING LIMO GUARD 2 TO ENTER VEHICLE")
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			
				LimoGuardState[iLimoGuard] = LGS_LEAVE_IN_LIMO
				PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_LEAVE_IN_LIMO")
			ELSE
				LimoGuardState[iLimoGuard] = LGS_PANIC
				PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_PANIC - from LGS_ENTER_LIMO")
			ENDIF
		BREAK
		
		//change peds walk speed to run if panicking
		CASE LGS_LEAVE_IN_LIMO
		
			//sets them targetable
			IF NOT IS_PED_INJURED(limoPed)
				SET_PED_RESET_FLAG(limoPed, PRF_SearchForClosestDoor, TRUE)
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(limoPed)
				IF GET_SCRIPT_TASK_STATUS(limoPed, SCRIPT_TASK_LOOK_AT_ENTITY) != PERFORMING_TASK
					IF iLimoGuard = 0
						TASK_LOOK_AT_ENTITY(piLimoGuard[iLimoGuard], piLimoGuard[1], -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					ELIF iLimoGuard = 1
						TASK_LOOK_AT_ENTITY(piLimoGuard[iLimoGuard], piLimoGuard[0], -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					ENDIF
				ENDIF
			ENDIF
		
			IF IS_VEHICLE_DRIVEABLE(viTargetCar)
				IF NOT IS_TARGET_IN_LIMO()
					IF bPanicking
						IF NOT IS_PED_INJURED(limoPed)
							IF GET_SCRIPT_TASK_STATUS(limoPed, SCRIPT_TASK_COMBAT) != PERFORMING_TASK
								IF IS_ENTITY_ALIVE(piLimoGuard[0]) // url:bugstar:2344952
									TASK_CLEAR_LOOK_AT(piLimoGuard[0])
								ENDIF
								
								IF IS_ENTITY_ALIVE(piLimoGuard[1]) // url:bugstar:2344952
									TASK_CLEAR_LOOK_AT(piLimoGuard[1])
								ENDIF
							
								PRINTLN("OJASHOTEL - TASK LIMO GUARD TO COMBAT IN LGS_LEAVE_IN_LIMO")
								TASK_COMBAT_PED(limoPed, Player)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					LimoGuardState[iLimoGuard] = LGS_DRIVE_TO_END
					PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_DRIVE_TO_END 01")
				ENDIF
			ELSE
				LimoGuardState[iLimoGuard] = LGS_PANIC
				PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_PANIC! - from LGS_LEAVE_IN_LIMO")
			ENDIF
			
			IF bTargetDead
				IF LimoGuardState[iLimoGuard] <> LGS_PANIC
				AND LimoGuardState[iLimoGuard] <> LGS_ATTACKING
					LimoGuardState[iLimoGuard] = LGS_PANIC
					PRINTLN("TARGET IS DEAD SENDING TO PANIC STATE: ", iLimoGuard)
				ENDIF
			ENDIF
		BREAK
	    
		//task the limo driver to exit the hotel area based on whether panic has been triggered or not
		CASE LGS_DRIVE_TO_END
			IF NOT bPanicking		
				IF IS_VEHICLE_DRIVEABLE(viTargetCar)
					IF GET_PED_IN_VEHICLE_SEAT(viTargetCar) = limoPed
						IF NOT IS_PED_INJURED(limoPed)
							
							SET_VEHICLE_ENGINE_ON(viTargetCar, TRUE, TRUE)
							
							IF IS_PATH_BLOCKED()
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(limoPed, viTargetCar,  "OJASva_101a", DF_SwerveAroundAllCars|DF_StopAtLights|DF_SteerAroundObjects|DF_StopForCars|DF_SteerAroundPeds, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 10)
								PRINTLN("OJASHOTEL - Tasking driver to drive to end position - path blocked")
							ELSE
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(limoPed, viTargetCar,  "OJASva_101", DF_SwerveAroundAllCars|DF_StopAtLights|DF_SteerAroundObjects|DF_StopForCars|DF_SteerAroundPeds)
								PRINTLN("OJASHOTEL - Tasking driver to drive to end position")
							ENDIF
						ENDIF
					ENDIF
					
					LimoGuardState[iLimoGuard] = LGS_DRIVING
					PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_DRIVING")
				ENDIF
				
			ELSE
				IF IS_ENTITY_ALIVE(piLimoGuard[0]) // url:bugstar:2344952
					TASK_CLEAR_LOOK_AT(piLimoGuard[0])
				ENDIF
				
				IF IS_ENTITY_ALIVE(piLimoGuard[1]) // url:bugstar:2344952
					TASK_CLEAR_LOOK_AT(piLimoGuard[1])
				ENDIF
			
				LimoGuardState[iLimoGuard] = LGS_DRIVING
				PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_DRIVE_TO_END 02")
			ENDIF
		BREAK
		
		//peds have been tasked to drive the streets - monitor for panic and handle response accordingly
		CASE LGS_DRIVING
			IF bPanicking
				IF IS_VEHICLE_DRIVEABLE(viTargetCar)
				AND GET_PED_IN_VEHICLE_SEAT(viTargetCar) = limoPed
				AND GET_PED_IN_VEHICLE_SEAT(viTargetCar, VS_BACK_LEFT) = piTarget
					IF NOT IS_PED_INJURED(limoPed)
						IF bLimoCruisingStreets
						OR (bPathIsBlocked AND NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(viTargetCar))
							
							CLEAR_PED_TASKS(limoPed)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF GET_SCRIPT_TASK_STATUS(limoPed, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
										TASK_VEHICLE_MISSION(limoPed, viTargetCar, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_FLEE, 35.0, DRIVINGMODE_AVOIDCARS, -1, -1)
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(limoPed, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
										TASK_VEHICLE_MISSION_PED_TARGET(limoPed, viTargetCar, PLAYER_PED_ID(), MISSION_FLEE, 35.0, DRIVINGMODE_AVOIDCARS, -1, -1)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(limoPed)
						IF GET_SCRIPT_TASK_STATUS(limoPed, SCRIPT_TASK_COMBAT) != PERFORMING_TASK
							TASK_COMBAT_PED(limoPed, Player)
							PRINTLN("LIMO: TARGET FLEEING, TASKING iLimoGuard TO COMBAT with index = ", iLimoGuard)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bLimoCruisingStreets
				OR (bPathIsBlocked AND NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(viTargetCar))
					IF IS_VEHICLE_DRIVEABLE(viTargetCar)
						IF GET_PED_IN_VEHICLE_SEAT(viTargetCar) = limoPed
							IF NOT IS_ENTITY_DEAD(limoPed)
								IF GET_SCRIPT_TASK_STATUS(limoPed, SCRIPT_TASK_VEHICLE_MISSION)	<> PERFORMING_TASK
									TASK_VEHICLE_MISSION_COORS_TARGET(limoPed, viTargetCar, vTargetCarLobbyPos, MISSION_FLEE, 25.0, DRIVINGMODE_STOPFORCARS, -1, -1)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE LGS_ALERTED
			IF bAlertedSpecial
				OPEN_SEQUENCE_TASK(seqIndex)
					TASK_LEAVE_ANY_VEHICLE(NULL)
					TASK_LOOK_AT_COORD(NULL, GET_RANDOM_COORDINATE(), GET_RANDOM_INT_IN_RANGE(1000, 1500), SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
//					TASK_LOOK_AT_COORD(NULL, GET_RANDOM_COORDINATE(), GET_RANDOM_INT_IN_RANGE(2000, 2500), SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), GET_RANDOM_INT_IN_RANGE(15000, 16000), 30, PEDMOVEBLENDRATIO_WALK)
					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_COORD(NULL, PLAYER_PED_ID(), GET_RANDOM_COORDINATE(), PEDMOVEBLENDRATIO_WALK, FALSE)
				CLOSE_SEQUENCE_TASK(seqIndex)
				
				IF NOT IS_ENTITY_DEAD(piLimoGuard[iLimoGuard])
					TASK_PERFORM_SEQUENCE(piLimoGuard[iLimoGuard], seqIndex)
				ENDIF

				CLEAR_SEQUENCE_TASK(seqIndex)
			ELSE
				OPEN_SEQUENCE_TASK(seqIndex)
					TASK_LEAVE_ANY_VEHICLE(NULL)
					TASK_AIM_GUN_AT_COORD(NULL, GET_RANDOM_COORDINATE(), GET_RANDOM_INT_IN_RANGE(3000, 3500), TRUE)
//					TASK_AIM_GUN_AT_COORD(NULL, GET_RANDOM_COORDINATE(), GET_RANDOM_INT_IN_RANGE(3000, 3500))
					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_COORD(NULL, PLAYER_PED_ID(), GET_RANDOM_COORDINATE(), PEDMOVEBLENDRATIO_WALK, FALSE)
				CLOSE_SEQUENCE_TASK(seqIndex)
				IF NOT IS_ENTITY_DEAD(piLimoGuard[iLimoGuard])
					TASK_PERFORM_SEQUENCE(piLimoGuard[iLimoGuard], seqIndex)
				ENDIF
				CLEAR_SEQUENCE_TASK(seqIndex)
			ENDIF
			
			LimoGuardState[iLimoGuard] = LGS_ALERTED2
			PRINTLN("OJASHOTEL - LimoGuardState[ ", iLimoGuard, "] = LGS_ALERTED2")
		BREAK
		
		CASE LGS_ALERTED2
			//ped has been tasked with alert tasks - wait here and retask should he lose his current task (bug 1511746 - 6/25/13 - MB)
			IF NOT IS_PED_INJURED(piLimoGuard[iLimoGuard])
				IF GET_SCRIPT_TASK_STATUS(piLimoGuard[iLimoGuard], SCRIPT_TASK_ANY) != PERFORMING_TASK
					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(piLimoGuard[iLimoGuard], Player, Player, PEDMOVE_WALK, FALSE, -1)
				ENDIF
			ENDIF
		BREAK
		
		CASE LGS_PANIC
			IF failType = FT_BLOWN_COVER_HIT_PARKED_CAR
			OR failType = FT_DISREGARDED_WARNINGS
				EXIT_VEHICLE_AND_AIM_GUN_AT_PLAYER(limoPed)
			ELIF failType <> FT_STICKY
				EXIT_VEHICLE_AND_ATTACK_PLAYER(limoPed)
				LimoGuardState[iLimoGuard] = LGS_ATTACKING
				PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_ATTACKING")
			ELSE
				PRINTLN("FAIL TYPE DOES EQUAL STICKY")
			ENDIF
		BREAK
		
		CASE LGS_ATTACKING
			IF iLimoGuard = 0
				UPDATE_PED_ATTACKING_IN_VEHICLE(viTargetCar, limoPed)
			ENDIF
			PLAY_GUARD_PANIC_LINE(limoPed)
			
			PLAY_TARGET_DEAD_LINE(limoPed)
		BREAK
	ENDSWITCH
	
	
	
	/////// ALWAYS RUN THESE PANIC CHECKS
	IF MainStage < STAGE_GUARDS_GO_TO_POSTS
		fCheckDist = 5		//so that they can only see in the airlock areas
	ELSE
		fCheckDist = fSightDist
	ENDIF
	
	IF NOT bPanicking
		IF bAlerted
			IF LimoGuardState[iLimoGuard] <> LGS_ALERTED
			AND LimoGuardState[iLimoGuard] <> LGS_ALERTED2
				LimoGuardState[iLimoGuard] = LGS_ALERTED
				PRINTLN("OJASHOTEL - LimoGuardState[ ", iLimoGuard, "] = LGS_ALERTED")
			ENDIF
		ELSE
			IF HAS_PLAYER_AGGROED_HOTEL_PED(limoPed)
//			OR bPlayerTriggeredExplosionFromParkingGarage
				PED_INDEX tempPed = GET_CLOSEST_GUARD()
				IF tempPed != NULL
					DO_ALERTED_CHECK(GET_ENTITY_COORDS(tempPed))
				ENDIF
				
				IF NOT SHOULD_TRIGGER_ALERT()
					bPanicking = TRUE
					PRINTLN("bPanicking = TRUE VIA HAS_PLAYER_AGGROED_HOTEL_PED - UPDATE_LIMO_GUARD_STATE")
				ENDIF
			
			//new stuff testing
			ELSE
				IF NOT bLeftHotel
					IF IS_PLAYER_SEEN_BY_HOTEL_PED(limoPed, fCheckDist)
						IF LimoGuardState[iLimoGuard] <> LGS_RESPOND_TO_PLAYER
						AND LimoGuardState[iLimoGuard] <> LGS_WARNING_CHECK
						AND NOT bTargetLeaving
							IF NOT bWarned
								CLEAR_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_ShotNear)
								
								LimoGuardState[iLimoGuard] = LGS_RESPOND_TO_PLAYER
								PRINTLN("OJASHOTEL - LimoGuardState[", iLimoGuard, "] = LGS_RESPOND_TO_PLAYER. ", "fDistToCheck is ", fCheckDist)
							ELSE
								IF NOT IS_ENTITY_IN_SAFE_LOCATION(Player)	// AND NOT bDoNotRunPanicCheck
									PRINTLN("OJASHOTEL - PANIC - IS_PLAYER_SEEN_BY_HOTEL_PED - 02")
									bPanicking = TRUE
									
									IF NOT DOES_ENTITY_EXIST(piTarget)
										failType = FT_DISREGARDED_WARNINGS
									ENDIF
									
									PRINTLN("bPanicking = TRUE VIA IS_PLAYER_SEEN_BY_HOTEL_PED - UPDATE_LIMO_GUARD_STATE")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
//		IF bLimoDestroyed
//			IF LimoGuardState[iLimoGuard] < LGS_PANIC
//				LimoGuardState[iLimoGuard] = LGS_PANIC
//				PRINTLN("OJASHOTEL - LimoGuardState[iLimoGuard] = LGS_PANIC!!!!!")
//			ENDIF
//		ELSE
			IF LimoGuardState[iLimoGuard] <> LGS_PANIC
			AND LimoGuardState[iLimoGuard] <> LGS_ATTACKING
			AND LimoGuardState[iLimoGuard] <> LGS_DRIVING
				LimoGuardState[iLimoGuard] = LGS_PANIC
				PRINTLN("GENERAL PANIC CHECK - SENDING TO LGS_PANIC: ", iLimoGuard)
			ENDIF
//		ENDIF
	ENDIF
	
	

	//blip handling
	REMOVE_ENEMY_BLIP_IF_KILLED(limoPed, bLimoGuardBlip[iLimoGuard])
ENDPROC


/// PURPOSE: Update the target state
PROC UPDATE_TARGET_STATE()
	SEQUENCE_INDEX iSeq

	IF DOES_ENTITY_EXIST(piTarget)
		vTargetLastPosition = GET_ENTITY_COORDS(piTarget, FALSE)
	ENDIF
	
	SWITCH TargetState
		CASE TS_ENTER_LIMO
		
			IF NOT IS_ENTITY_DEAD(piTarget)
				IF IS_VEHICLE_DRIVEABLE(viTargetCar)
				AND NOT IS_ENTITY_DEAD(piLimoGuard[0])	//limo driver
					IF NOT IS_PED_IN_VEHICLE(piTarget, viTargetCar)
						IF GET_SCRIPT_TASK_STATUS(piTarget, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							IF NOT bPanicking
							AND NOT bAlerted
								TASK_ENTER_VEHICLE(piTarget, viTargetCar, 30000, VS_BACK_LEFT, PEDMOVE_WALK)
								PRINTLN("piTarget ENTER VEHICLE - WALK!")
							ELSE
								TASK_ENTER_VEHICLE(piTarget, viTargetCar, 30000, VS_BACK_LEFT, PEDMOVE_RUN)
								PRINTLN("PANIC! - TARGET ENTERING VEHICLE!!")
							ENDIF
							
							IF NOT IS_TIMER_STARTED(tTargetSpeakTimer)
								START_TIMER_NOW(tTargetSpeakTimer)
								PRINTLN("STARTING TIMER - tTargetSpeakTimer")
							ELSE
								RESTART_TIMER_NOW(tTargetSpeakTimer)
								PRINTLN("RESTARTING TIMER - tTargetSpeakTimer")
							ENDIF
						ENDIF
						
						IF NOT bPlayedTargetLine
							IF IS_TIMER_STARTED(tTargetSpeakTimer)
								IF GET_TIMER_IN_SECONDS(tTargetSpeakTimer) > 3.5
									IF NOT IS_PED_INJURED(piTarget)
										ADD_PED_FOR_DIALOGUE(hotelConv, 4, piTarget, "OJAva_TARGET")
										IF CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_TARG", CONV_PRIORITY_HIGH)
											PRINTLN("PLAYING TARGET'S LINE")
										
											bPlayedTargetLine = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF MAKE_PED_RUN_IF_PANICKING(piTarget)
							KILL_ANY_CONVERSATION()
						
							TargetState = TS_FLEE
							PRINTLN("GOING TO STATE - TS_FLEE VIA TARGET PANIC CHECK - 01")
						ENDIF
						
					ELSE
						bLeftHotel = TRUE
					
						TargetState = TS_LEAVE_IN_LIMO
						PRINTLN("OJASHOTEL - TargetState = TS_LEAVE_IN_LIMO")
					ENDIF
				ELSE
					TargetState = TS_FLEE
					PRINTLN("OJASHOTEL - TargetState = TS_FLEE")
				ENDIF
			ENDIF
		BREAK
		CASE TS_LEAVE_IN_LIMO
		
			//stay here until the target reaches the destination - he should just be riding until then
			//insert panic in car anims here when we get them
	
			IF NOT bPlayedTargetLine
				IF NOT IS_PED_INJURED(piTarget)
					ADD_PED_FOR_DIALOGUE(hotelConv, 4, piTarget, "OJAva_TARGET")
					IF CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_TARG", CONV_PRIORITY_HIGH)
						PRINTLN("PLAYING TARGET'S LINE")
					
						bPlayedTargetLine = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			
								
			IF NOT bTargetPanicLinePlayed
				IF ARE_ALL_GUARDS_DEAD_IN_LIMO()
					IF GET_DISTANCE_BETWEEN_ENTITIES(piTarget, PLAYER_PED_ID()) <= 5
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_CLOSEST_GUARD(25.0) = NULL
							IF HAS_TARGET_PLAYED_ALL_PANIC_LINES()
								bTargetPanicLinePlayed = TRUE
								PRINTLN("OJASHOTEL - Played all target panic lines in car")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
//			IF bLimoDestroyed = TRUE
//				TargetState = TS_FLEE
//				PRINTLN("OJASHOTEL - bLimoDestroyed - TargetState = TS_FLEE")
//			ENDIF
		BREAK
		CASE TS_FLEE
		
			IF NOT IS_ENTITY_DEAD(piTarget)
				IF IS_PED_IN_VEHICLE(piTarget, viTargetCar)
					IF GET_SCRIPT_TASK_STATUS(piTarget, SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
						TASK_LEAVE_ANY_VEHICLE(piTarget, 0, ECF_DONT_CLOSE_DOOR)
					ENDIF
				ELSE
					IF NOT bTargetFleeing
						SET_PED_MAX_MOVE_BLEND_RATIO(piTarget, PEDMOVEBLENDRATIO_RUN)
						
						CLEAR_SEQUENCE_TASK(iSeq)
						OPEN_SEQUENCE_TASK(iSeq)
							TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@hotel@", "alert_gunshot", NORMAL_BLEND_IN, SLOW_BLEND_OUT, 1000)
							TASK_SMART_FLEE_PED(NULL, Player, 200, -1)
						CLOSE_SEQUENCE_TASK(iSeq)
						IF NOT IS_ENTITY_DEAD(piTarget)
							TASK_PERFORM_SEQUENCE(piTarget, iSeq)
							PRINTLN("TASKING TARGET TO PERFORM SEQUENCE")
						ENDIF
						CLEAR_SEQUENCE_TASK(iSeq)
				
						bTargetFleeing = TRUE
						PRINTLN("bTargetFleeing = TRUE")
					ELSE
						//safeguard to fix bug 1526704 if bTargetFleeing previously got set to true while the target was entering the vheicle
						IF GET_SCRIPT_TASK_STATUS(piTarget, SCRIPT_TASK_ANY) != PERFORMING_TASK
							TASK_SMART_FLEE_PED(piTarget, Player, 200, -1)	
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bTargetPanicLinePlayed
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_TARG2", CONV_PRIORITY_VERY_HIGH)
						bTargetPanicLinePlayed = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	
/////// ALWAYS RUN THESE PANIC CHECKS
	IF NOT bPanicking
		IF bAlerted
			// handle this
		ELSE
			IF HAS_PLAYER_AGGROED_HOTEL_PED(piTarget)
			OR bPlayerTriggeredExplosionFromParkingGarage			//call this here since UPDATE_TARGET_STATE gets called every frame
				PED_INDEX tempPed = GET_CLOSEST_GUARD()
				IF tempPed != NULL
					DO_ALERTED_CHECK(GET_ENTITY_COORDS(tempPed))
				ENDIF
				
				IF NOT SHOULD_TRIGGER_ALERT()
					bPanicking  = TRUE
					PRINTLN("TARGET PANICKED!")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Update the target state
PROC UPDATE_BODYGUARD_STATE(INT iBodyGuard)
	VEHICLE_INDEX bodyGuardVehicle
	VEHICLE_SEAT bodyGuardSeat
	SEQUENCE_INDEX tempSeq
	FLOAT fAnimTime
	
	
	IF NOT DOES_ENTITY_EXIST(piBodyguard[iBodyGuard])
		EXIT
	ENDIF
	
	
	SWITCH BodyGuardState[iBodyGuard]
		CASE BS_WALK_WITH_TARGET
			IF NOT IS_ENTITY_DEAD(piTarget)
				IF NOT IS_PED_IN_ANY_VEHICLE(piTarget)
					IF NOT IS_PED_INJURED(piBodyguard[iBodyGuard])
						IF iBodyGuard = 0
							TASK_FOLLOW_TO_OFFSET_OF_ENTITY(piBodyguard[iBodyGuard], piTarget, << 1.5, 0, 0 >>, PEDMOVEBLENDRATIO_WALK)
						ELIF iBodyGuard = 1
							IF IS_VEHICLE_DRIVEABLE(viEscortCar)
								OPEN_SEQUENCE_TASK(tempSeq)
									TASK_GO_TO_COORD_ANY_MEANS(NULL, << -1222.99207, -191.09319, 38.17538 >>, PEDMOVEBLENDRATIO_WALK, NULL) 
									TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_GUARD_STAND", 0)
									TASK_ENTER_VEHICLE(NULL, viEscortCar, 30000, VS_BACK_RIGHT, PEDMOVE_WALK)
								CLOSE_SEQUENCE_TASK(tempSeq)
								TASK_PERFORM_SEQUENCE(piBodyguard[iBodyGuard], tempSeq)
								CLEAR_SEQUENCE_TASK(tempSeq)
							ENDIF
						ENDIF
						
						BodyGuardState[iBodyGuard] = BS_ENTER_VEHICLE
						PRINTLN("BodyGuardState[", iBodyGuard, "] = BS_ENTER_VEHICLE")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE BS_ENTER_VEHICLE
			IF iBodyGuard = 0
				bodyGuardVehicle = viTargetCar
				bodyGuardSeat = VS_BACK_RIGHT
			ELIF iBodyGuard = 1
				bodyGuardVehicle = viEscortCar
				bodyGuardSeat = VS_BACK_RIGHT
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(piBodyguard[iBodyGuard])
				IF IS_VEHICLE_DRIVEABLE(bodyGuardVehicle)
					IF NOT IS_ENTITY_DEAD(piBodyguard[iBodyGuard])
						IF NOT IS_PED_IN_VEHICLE(piBodyguard[iBodyGuard], bodyGuardVehicle)
							IF IS_PED_IN_ANY_VEHICLE(piTarget)
								IF GET_SCRIPT_TASK_STATUS(piBodyguard[iBodyGuard], SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
									TASK_ENTER_VEHICLE(piBodyguard[iBodyGuard], bodyGuardVehicle, 30000, bodyGuardSeat, PEDMOVE_WALK)
								ENDIF
							ENDIF
						ELSE
							BodyGuardState[iBodyGuard] = BS_RIDE_IN_VEHICLE
							PRINTLN("BodyGuardState[", iBodyGuard, "] = BS_RIDE_IN_VEHICLE")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE BS_RIDE_IN_VEHICLE
			IF iBodyGuard = 0
				bodyGuardVehicle = viTargetCar
			ELIF iBodyGuard = 1
				bodyGuardVehicle = viEscortCar
			ENDIF
			
			//tell the motorcade to escort the limo
			IF IS_VEHICLE_DRIVEABLE(bodyGuardVehicle)
				IF NOT IS_ENTITY_DEAD(piBodyguard[iBodyGuard])
					IF IS_PED_IN_VEHICLE(piBodyguard[iBodyGuard], bodyGuardVehicle)
					AND IS_VEHICLE_DRIVEABLE(viTargetCar)
						IF bPanicking
							IF NOT bTargetFleeing
								IF GET_SCRIPT_TASK_STATUS(piBodyguard[iBodyGuard], SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
									IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										IF GET_ENTITY_SPEED(bodyGuardVehicle) < 5.0
											BodyGuardState[iBodyGuard] = BS_PANIC
											PRINTLN("PLAYER IS NOT IN A VEHICLE - BodyGuardState[", iBodyGuard, "] = BS_PANIC")
										ENDIF
									ENDIF
									TASK_COMBAT_PED(piBodyguard[iBodyGuard], PLAYER_PED_ID())
									PRINTLN("BODYGUARD: TARGET NOT FLEEING, TASKING BODYGUARD TO COMBAT WITH INDEX = ", iBodyGuard)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(piBodyguard[iBodyGuard], SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
									TASK_COMBAT_PED(piBodyguard[iBodyGuard], PLAYER_PED_ID())
//									PRINTLN("BODYGUARD: TARGET FLEEING, TASKING TO COMBAT INDEX = ", iBodyGuard)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT bAlerted
							BodyGuardState[iBodyGuard] = BS_PANIC
							PRINTLN("BODYGUARD: SENDING TO BS_PANIC VIA TARGET CAR IS NOT DRIVEABLE")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE BS_ALERTED
			IF NOT IS_PED_INJURED(piBodyguard[iBodyGuard])
				IF IS_PED_USING_ANY_SCENARIO(piBodyguard[iBodyGuard])
					SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(piBodyguard[iBodyGuard])
				ENDIF
			ENDIF
			
			IF bAlertedSpecial
				OPEN_SEQUENCE_TASK(seqIndex)
					TASK_LEAVE_ANY_VEHICLE(NULL)
					TASK_LOOK_AT_COORD(NULL, GET_RANDOM_COORDINATE(), GET_RANDOM_INT_IN_RANGE(2000, 2500), SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), GET_RANDOM_INT_IN_RANGE(15000, 16000), 30, PEDMOVEBLENDRATIO_WALK)
					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_COORD(NULL, PLAYER_PED_ID(), GET_RANDOM_COORDINATE(), PEDMOVEBLENDRATIO_WALK, FALSE)
				CLOSE_SEQUENCE_TASK(seqIndex)

				TASK_PERFORM_SEQUENCE(piBodyguard[iBodyGuard], seqIndex)

				CLEAR_SEQUENCE_TASK(seqIndex)
			ELSE
				IF NOT IS_PED_INJURED(piBodyguard[iBodyGuard])
					//sets a peds defensive area around the target
					SET_PED_SPHERE_DEFENSIVE_AREA(piBodyguard[iBodyGuard], vTargetLastPosition, 15.0)
					SET_PED_COMBAT_MOVEMENT(piBodyguard[iBodyGuard], CM_DEFENSIVE)
					SET_PED_COMBAT_ATTRIBUTES(piBodyguard[iBodyGuard], CA_USE_COVER, TRUE)
					SET_PED_MAX_MOVE_BLEND_RATIO(piBodyguard[iBodyGuard], PEDMOVEBLENDRATIO_RUN)
									
									
						IF NOT bGuardAttendingTarget
						AND NOT IS_PED_IN_ANY_VEHICLE(piBodyGuard[iBodyGuard])
							
							OPEN_SEQUENCE_TASK(seqIndex)
							
								IF IS_PED_INJURED(piTarget)
									TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@hotel@", "alert_gunshot", NORMAL_BLEND_IN, SLOW_BLEND_OUT, 1250)
									TASK_AIM_GUN_AT_COORD(NULL, GET_RANDOM_COORDINATE(), 3000, FALSE, TRUE)
									TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
								ELSE
									TASK_PLAY_ANIM(NULL, "oddjobs@assassinate@hotel@", "alert_gunshot", NORMAL_BLEND_IN, SLOW_BLEND_OUT, 700)
									TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, piTarget, << 1.5, 0, 0 >>, PEDMOVEBLENDRATIO_RUN)
								ENDIF
							
							CLOSE_SEQUENCE_TASK(seqIndex)
							
							bGuardAttendingTarget = TRUE
							PRINTLN("OJASHOTEL - BodyGuard[", iBodyGuard, " attending target")
						ELSE
							VECTOR vWheelPos
							VECTOR vLimoCoords
							IF DOES_ENTITY_EXIST(viTargetCar)
							AND IS_VEHICLE_DRIVEABLE(viTargetCar)
								vWheelPos = GET_WORLD_POSITION_OF_ENTITY_BONE(viTargetCar, GET_ENTITY_BONE_INDEX_BY_NAME(viTargetCar, "wheel_lr") )
								vLimoCoords = GET_ENTITY_COORDS(viTargetCar)
								vLimoCoords = vWheelPos - vLimoCoords
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(piBodyguard[iBodyGuard], viTargetCar, vLimoCoords, 1.5)
								PRINTLN("OJASHOTEL - BodyGuard found defensive area position - 1.5 m")
							ELSE
								PRINTLN("Unable to set ped_defensive_sphere_for ped")
							ENDIF
							
							OPEN_SEQUENCE_TASK(seqIndex)
								
								TASK_LEAVE_ANY_VEHICLE(NULL)
								
								IF IS_PED_INJURED(piTarget)
								AND NOT ARE_VECTORS_EQUAL(NULL_VECTOR(), vWheelPos)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, vWheelPos, GET_RANDOM_COORDINATE(), PEDMOVEBLENDRATIO_RUN, FALSE)
									TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, vWheelPos, -1, FALSE, SLOW_BLEND_DURATION, FALSE, FALSE, NULL, TRUE)
									PRINTLN("OJASHOTEL - BodyGuard[", iBodyGuard, " seeking cover to coords")
								ELSE
									TASK_AIM_GUN_AT_COORD(NULL, GET_RANDOM_COORDINATE(), 1500, TRUE)
									IF NOT IS_ENTITY_DEAD(piTarget)
										TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, piTarget, << 0, -1.5, 0 >>, PEDMOVEBLENDRATIO_RUN)
									ENDIF
								ENDIF
								
							CLOSE_SEQUENCE_TASK(seqIndex)
						ENDIF
						
					IF NOT IS_ENTITY_DEAD(piBodyguard[iBodyGuard])
						TASK_PERFORM_SEQUENCE(piBodyguard[iBodyGuard], seqIndex)
					ENDIF
					CLEAR_SEQUENCE_TASK(seqIndex)
				ENDIF
			ENDIF
			
			BodyGuardState[iBodyGuard] = BS_ALERTED2
			PRINTLN("OJASHOTEL - BodyGuardState[", iBodyGuard, "] = BS_ALERTED2")
		BREAK
		
		CASE BS_ALERTED2
			IF NOT IS_ENTITY_DEAD(piBodyguard[iBodyGuard])
				IF IS_ENTITY_PLAYING_ANIM(piBodyguard[iBodyGuard], "oddjobs@assassinate@hotel@", "enter")
					
					fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(piBodyguard[iBodyGuard], "oddjobs@assassinate@hotel@", "enter")
					PRINTLN("fAnimTime = ", fAnimTime)
					
					IF fAnimTime < 0.5
						SET_ENTITY_ANIM_SPEED(piBodyguard[iBodyGuard], "oddjobs@assassinate@hotel@", "enter", 3.5)
					ELSE
						SET_ENTITY_ANIM_SPEED(piBodyguard[iBodyGuard], "oddjobs@assassinate@hotel@", "enter", 1.0)
					ENDIF
				ENDIF
			ENDIF
			//note that in UPDATE_PED_STATES() that PRF_BlockWeaponFire is being called every frame so that the ped doesnt try to fire since he has a combat task (but we want him to seek cover
		BREAK
		
		CASE BS_PANIC
			IF iBodyGuard = 0
				bodyGuardVehicle = viTargetCar
			ELIF iBodyGuard = 1
				bodyGuardVehicle = viEscortCar
			ENDIF
			
			IF NOT IS_PED_INJURED(piTarget)
				IF NOT IS_PED_INJURED(piBodyguard[iBodyGuard])
					IF IS_PED_IN_ANY_VEHICLE(piTarget)
						TASK_COMBAT_PED(piBodyguard[iBodyGuard], Player)
					ELSE
						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(piBodyguard[iBodyGuard], piTarget, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_SPRINT, TRUE, 0.005)
					ENDIF
				ENDIF
				
				BodyGuardState[iBodyGuard] = BS_ATTACKING
				PRINTLN("BodyGuard[", iBodyGuard, "] = BS_ATTACKING - target dead")
			ELSE
				IF IS_VEHICLE_DRIVEABLE(bodyGuardVehicle)
					IF GET_ENTITY_SPEED(bodyGuardVehicle) < 5.0
						EXIT_VEHICLE_AND_ATTACK_PLAYER(piBodyguard[iBodyGuard])
						BodyGuardState[iBodyGuard] = BS_ATTACKING
						PRINTLN("BodyGuard[", iBodyGuard, "] = BS_ATTACKING1")
					ELSE
						IF GET_SCRIPT_TASK_STATUS(piBodyguard[iBodyGuard], SCRIPT_TASK_COMBAT) != PERFORMING_TASK
							TASK_COMBAT_PED(piBodyguard[iBodyGuard], Player)
							PRINTLN("BodyGuard[", iBodyGuard, "] - TASK_COMBAT_PED2")
						ENDIF
					ENDIF
				ELSE
					EXIT_VEHICLE_AND_ATTACK_PLAYER(piBodyguard[iBodyGuard])
					BodyGuardState[iBodyGuard] = BS_ATTACKING
					PRINTLN("BodyGuard[", iBodyGuard, "] = BS_ATTACKING - vehicle undriveable")
				ENDIF
			ENDIF
		BREAK
		
		CASE BS_ATTACKING
			//ped has been tasked to attack on foot at this point
			PLAY_GUARD_PANIC_LINE(piBodyguard[iBodyGuard])
			
			PLAY_TARGET_DEAD_LINE(piBodyguard[iBodyGuard])
		BREAK
	ENDSWITCH
		
		
		
		
	/////// ALWAYS RUN THESE PANIC CHECKS
	IF NOT bPanicking
	//	AND NOT IS_ENTITY_DEAD(piTarget)
		IF bAlerted
			IF BodyGuardState[iBodyGuard] <> BS_ALERTED
			AND BodyGuardState[iBodyGuard] <> BS_ALERTED2
				BodyGuardState[iBodyGuard] = BS_ALERTED
				PRINTLN("OJASHOTEL - BodyGuardState[ ", iBodyGuard, "] = BS_ALERTED")
			ENDIF
		ELSE
			IF HAS_PLAYER_AGGROED_HOTEL_PED(piBodyguard[iBodyGuard])
				bPanicking  = TRUE
				PRINTLN("PLAYER AGGROED BODYGUARD!")
			ENDIF
		ENDIF
	ELSE
//		IF BodyGuardState[iBodyGuard] < BS_RIDE_IN_VEHICLE
//		AND NOT bLeftHotel
		IF BodyGuardState[iBodyGuard] < BS_PANIC
			IF NOT IS_PED_INJURED(piBodyguard[iBodyGuard])
				IF IS_PED_USING_ANY_SCENARIO(piBodyguard[iBodyGuard])
					SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(piBodyguard[iBodyGuard])
				ENDIF
				
				TASK_CLEAR_DEFENSIVE_AREA(piBodyguard[iBodyGuard])
				SET_PED_COMBAT_MOVEMENT(piBodyguard[iBodyGuard], CM_WILLADVANCE)
				SET_PED_COMBAT_ATTRIBUTES(piBodyguard[iBodyGuard], CA_AGGRESSIVE, TRUE)
			ENDIF
				
			BodyGuardState[iBodyGuard] = BS_PANIC
			PRINTLN("BodyGuardState[", iBodyGuard, "] = BS_PANIC")
		ENDIF
	ENDIF
ENDPROC


//update the hotel timer at the bottom of the screen
PROC UPDATE_HOTEL_TIMER(INT& startTime, FLOAT& fPickupTime, BOOL& bTimerStarted, BOOL& bTimeExpired)
	FLOAT fTimer
	
    // Grab starting game time
    IF NOT bTimerStarted
        startTime = GET_GAME_TIMER()
		PRINTLN("startTime = ", startTime)
		
		fPickupTime = (fPickupTime/1000)
        PRINTLN("OJASHOTEL - fPickupTime = ", fPickupTime)
		
		IF NOT IS_TIMER_STARTED(tCheckoutTimer)
			START_TIMER_NOW(tCheckoutTimer)
			PRINTLN("STARTING TIMER - tCheckoutTimer")
		ENDIF
		
        bTimerStarted = TRUE
    ENDIF
      
	IF IS_TIMER_STARTED(tCheckoutTimer)
		fTimer = GET_TIMER_IN_SECONDS(tCheckoutTimer)
//		PRINTLN("fTimer = ", fTimer)
		
		IF fTimer >= fPickupTime
			bTimeExpired = TRUE
			PRINTLN("bTimeExpired = TRUE")
		ENDIF
	ENDIF
	
	iTimeRemaining = ROUND((fPickupTime - fTimer))
//	PRINTLN("iTimeRemaining = ", iTimeRemaining)
	iTimeRemaining = iTimeRemaining * 1000
	
	IF iTimeRemaining < 63000
		bDriveToLimo = TRUE
//		PRINTLN("SETTING bDriveToLimo = TRUE")
	ENDIF
	IF iTimeRemaining < 65000
		bTaxiLeave = TRUE
//		PRINTLN("SETTING bTaxiLeave = TRUE")	
	ENDIF
	
	IF iTimeRemaining < iGuardTimeToTellExtrasToLeave
		bGuardsTellExtrasToLeave = TRUE
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\ASSASSINATION_MULTI")
		bLoadedSoundSet = FALSE
	ELSE
		bLoadedSoundSet = TRUE
	ENDIF

	IF iTimeRemaining < 30000
		bTriggerGuardWalk = TRUE
		
		IF bLoadedSoundSet
			IF IS_TIMER_STARTED(tBeepTimer)
				IF GET_TIMER_IN_SECONDS(tBeepTimer) > 1
					PLAY_SOUND_FRONTEND(-1, "ASSASSINATIONS_HOTEL_TIMER_COUNTDOWN", "ASSASSINATION_MULTI")
					RESTART_TIMER_NOW(tBeepTimer)
					PRINTLN("PLAYING BEEP")
				ENDIF
			ELSE
				PLAY_SOUND_FRONTEND(-1, "ASSASSINATIONS_HOTEL_TIMER_COUNTDOWN", "ASSASSINATION_MULTI")
				
				START_TIMER_NOW(tBeepTimer)
				PRINTLN("STARTING TIMER - tBeepTimer - 01")
			ENDIF
		ENDIF
		
		DRAW_GENERIC_TIMER(iTimeRemaining, "ASS_VA_TIMERED", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1,
										PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
	ELSE
		DRAW_GENERIC_TIMER(iTimeRemaining, "ASS_VA_TIMELEFT")
	ENDIF
	
//	DRAW_CLOCK("ASS_VA_TIMELEFT", TRUE, iWarningRedTimeHours, iWarningRedTimeMinutes)
 
ENDPROC


//checks to see if a vehicle has safely exited the hotel area and reached the street
FUNC BOOL IS_CAR_AT_STREET(VEHICLE_INDEX vehToCheck, FLOAT fDistToCheck)
	IF DOES_ENTITY_EXIST(vehToCheck)
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehToCheck, << -1217.4419, -281.5099, 36.7495 >>) <= fDistToCheck
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_GUARDS_IN_VEHICLE()
	IF DOES_ENTITY_EXIST(piLimoGuard[0]) AND DOES_ENTITY_EXIST(piLimoGuard[1])
	AND NOT IS_PED_INJURED(piLimoGuard[0]) AND NOT IS_PED_INJURED(piLimoGuard[1])
		IF IS_PED_IN_VEHICLE(piLimoGuard[0], viTargetCar) AND IS_PED_IN_VEHICLE(piLimoGuard[1], viTargetCar)
			PRINTLN("GUARDS ARE IN VEHICLE, BRING OUT THE TARGET")
			RETURN TRUE
		ELSE
			PRINTLN("GUARDS ARE NOT IN VEHICLE")
		ENDIF
	ELSE
		PRINTLN("GUARDS DO NOT EXIST OR ARE INJURED")
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CHECK_FOR_PLAYER_BLOCKING_WITH_VEHICLES()
	VEHICLE_INDEX vehTempVehicle

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehTempVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//			PRINTLN("PLAYER IS IN A VEHICLE")
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehTempVehicle)
			IF IS_ENTITY_IN_ANGLED_AREA(vehTempVehicle, << -1214.517, -158.910, -39.165 >>, << -1243.589, -238.784, 39.165 >>, 15.000, FALSE, FALSE)
			AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTempVehicle)
//				PRINTLN("VEHICLE IS IN ANGLED AREA")
				
				IF GET_ENTITY_SPEED(vehTempVehicle) <= 0.5
					IF NOT IS_TIMER_STARTED(tBlockingTimer)
						START_TIMER_NOW(tBlockingTimer)
//						PRINTLN("STARTING TIMER NOW")
					ENDIF
				ELSE
					IF IS_TIMER_STARTED(tBlockingTimer)
						RESTART_TIMER_NOW(tBlockingTimer)
//						PRINTLN("PLAYER IS MOVING: RE-STARTING TIMER NOW")
					ENDIF
				ENDIF
				
				IF IS_TIMER_STARTED(tBlockingTimer)
					IF GET_TIMER_IN_SECONDS(tBlockingTimer) > 10.0
						PRINTLN("FAILING: PLAYER HAS A VEHICLE IN ANGLED AREA AND IT SEEMS TO BE STOPPED")
						SET_MISSION_FAILED(FT_BLOWN_COVER)
					ENDIF
				ENDIF
			ELSE
				IF IS_TIMER_STARTED(tBlockingTimer)
					RESTART_TIMER_NOW(tBlockingTimer)
//					PRINTLN("PLAYER LEFT AREA: RE-STARTING TIMER NOW")
				ENDIF
//				PRINTLN("VEHICLE IS NOT IN ANGLED AREA")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ENDING_CELL_PHONE_CALL_COMPLETED()
//	IF NOT bPlayedCellPhoneCall
//		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		
//		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
////				SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
////				PRINTLN("SETTING FLAG - AllowScoreAndRadio")
//			ENDIF
//		ENDIF
		
		IF bPrepareLostCue
			TRIGGER_MUSIC_EVENT("ASS1_LOST")
			PRINTLN("TRIGGERING MUSIC - ASS1_LOST")
			bTriggeredLastCue = TRUE
		ELSE
			PRINTLN("UNABLE TO PREPARE MUSIC CUE - ASS1_LOST")
		ENDIF
		
		CLEANUP_PEDS()
		
		
//		ADD_PED_FOR_DIALOGUE(hotelConv, 3, NULL, "LESTER")
//		
//		ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
//		PLAYER_CALL_CHAR_CELLPHONE(hotelConv, CHAR_LESTER, "OJASAUD", "OJAS_HOTEL_C", CONV_MISSION_CALL)
//		PRINTLN("CELL PHONE CALL")
//		bPlayedCellPhoneCall = TRUE
//	ENDIF
//	
//	IF bPlayedCellPhoneCall AND HAS_CELLPHONE_CALL_FINISHED()
	
		PRINTLN("RETURNING TRUE - HAS_ENDING_CELL_PHONE_CALL_COMPLETED")
		RETURN TRUE
		
//		IF DOES_BLIP_EXIST(blipRadius)
//			REMOVE_BLIP(blipRadius)
//			PRINTLN("REMOVING blipRadius")
//		ENDIF

//		IF bPrepareLostCue
//			TRIGGER_MUSIC_EVENT("ASS1_LOST")
//			PRINTLN("TRIGGERING MUSIC - ASS1_LOST - LATE")
//			bTriggeredLastCue = TRUE
//		ELSE
//			PRINTLN("UNABLE TO PREPARE MUSIC CUE - ASS1_LOST - LATE")
//			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					TRIGGER_MUSIC_EVENT("ASS1_STOP")
//					PRINTLN("TRIGGERING MUSIC - ASS1_STOP")
//					bTriggeredLastCue = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//
//	ELSE
//		PRINTLN("WAITING FOR CELL PHONE CALL TO FINISH")
//	ENDIF
	
//	RETURN FALSE
ENDFUNC

PROC DO_STAGE_INTRO_CUTSCENE()

	SWITCH iStageNum
		CASE 0
			REQUEST_CUTSCENE("ASS_INT_2_ALT1")
			REQUEST_MODEL(IG_LESTERCREST)
			REQUEST_MODEL(PROP_CS_WALKING_STICK)
			REQUEST_ANIM_DICT("oddjobs@assassinate@hotel@leadin")
			REQUEST_ANIM_DICT("oddjobs@assassinate@hotel@leaning@")
			REQUEST_CLIP_SET("move_lester_CaneUp")
			
			//to fix Bug 1173621 - (3/12/13) - MB
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			
		   	IF HAS_MODEL_LOADED(IG_LESTERCREST)
			AND HAS_MODEL_LOADED(PROP_CS_WALKING_STICK)
			AND HAS_ANIM_DICT_LOADED("oddjobs@assassinate@hotel@leadin")
			AND HAS_ANIM_DICT_LOADED("oddjobs@assassinate@hotel@leaning@")
			AND HAS_CLIP_SET_LOADED("move_lester_CaneUp")
		
				iStageNum ++
				PRINTLN("DO_STAGE_INTRO_CUTSCENE, iStageNum = ", iStageNum)
			ELSE
				PRINTLN("Requesting model for Lester and his walking stick piLester for Cutscene")
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE 1
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				ODDJOB_ENTER_CUTSCENE(SPC_REMOVE_FIRES|SPC_REMOVE_EXPLOSIONS|SPC_REMOVE_PROJECTILES)
				
				// Do stuff here
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1514.611450,-927.466675,7.133712>>, <<-1496.948120,-942.223999,16.187229>>, 30.000000, <<-1523.1742, -924.6732, 9.1221>>, 53.0177, TRUE, TRUE, TRUE, TRUE)
				
				CLEAR_AREA(<< -1507.71497, -941.13135, 8.37286 >>, 10, TRUE)
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWaitingPosition)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWaitingHeading)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				TASK_PLAY_ANIM(PLAYER_PED_ID(), "oddjobs@assassinate@hotel@leaning@", "idle_a", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_TURN_OFF_COLLISION)
				
				IF NOT DOES_CAM_EXIST(camLeadIn01)
					camLeadIn01 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCameraPosition01, vCameraRotation01, 42.203197, TRUE)
					PRINTLN("CREATING CAMERA - camLeadIn01")
					SHAKE_CAM(camLeadIn01, "HAND_SHAKE", 0.3)
				ENDIF
				
				IF NOT DOES_CAM_EXIST(camLeadIn02)
					camLeadIn02 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCameraPosition02, vCameraRotation02, 42.203197, FALSE)
					SET_CAM_ACTIVE_WITH_INTERP(camLeadIn02, camLeadIn01, 5000)
					SHAKE_CAM(camLeadIn01, "HAND_SHAKE", 0.3)
				ENDIF
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				IF IS_REPEAT_PLAY_ACTIVE()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					PRINTLN("REPEAT PLAY ACTIVE - FADING IN")
				ENDIF
				
				iStageNum ++
				PRINTLN("DO_STAGE_INTRO_CUTSCENE, iStageNum = ", iStageNum)
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE 2
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "oddjobs@assassinate@hotel@leaning@", "idle_a")
					IF NOT IS_TIMER_STARTED(tLeadinTimer)
						START_TIMER_NOW(tLeadinTimer)
						PRINTLN("STARTING TIMER - tLeadinTimer")
					ENDIF
				ENDIF
				
//				IF HAS_MODEL_LOADED(prop_bench_08)
//					IF NOT DOES_ENTITY_EXIST(oFailSafeBench)
//						oFailSafeBench = CREATE_OBJECT(prop_bench_08, << -1510.1029, -947.7194, 8.2332 >>)
//						SET_ENTITY_HEADING(oFailSafeBench, 142.29)
//						SET_ENTITY_COLLISION(oFailSafeBench, FALSE)
//					ENDIF
//				ENDIF
				
				IF IS_TIMER_STARTED(tLeadinTimer)
					IF GET_TIMER_IN_SECONDS(tLeadinTimer) > 6.0
						iStageNum ++
						PRINTLN("DO_STAGE_INTRO_CUTSCENE, iStageNum = ", iStageNum)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE 3	
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				iStageNum ++
				PRINTLN("DO_STAGE_INTRO_CUTSCENE, iStageNum = ", iStageNum)
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE 4
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			REGISTER_ENTITY_FOR_CUTSCENE(piLester, "Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_LESTERCREST)
			PRINTLN("REGISTERING LESTER FOR CUTSCENE")
			
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "WalkingStick_Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_CS_WALKING_STICK)
			PRINTLN("REGISTERING LESTER'S CANE")
		
			START_CUTSCENE()
			
			//to fix bug 1528120 - Clear player wanted level
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			
//			FADE_IN()
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			iStageNum ++
			PRINTLN("DO_STAGE_INTRO_CUTSCENE, iStageNum = ", iStageNum)
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE 5
			IF IS_CUTSCENE_PLAYING()
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
				iStageNum ++
				PRINTLN("DO_STAGE_INTRO_CUTSCENE, iStageNum = ", iStageNum)
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE 6
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT()
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(piLester)
//				PRINTLN("piLester DOES NOT EXIST")
				
            	IF DOES_CUTSCENE_ENTITY_EXIST("Lester")
//					PRINTLN("CUTSCENE ENTITY - Lester EXISTS")
				
            		IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester")) //, IG_LESTERCREST))
                   		piLester =  GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester"))
       					PRINTLN("CREATING PED - piLester")
//					ELSE
//						PRINTLN("NOT FINDING LESTER")
                    ENDIF
            	ENDIF
            ENDIF
			
			IF NOT DOES_ENTITY_EXIST(LesterCane)
            	IF DOES_CUTSCENE_ENTITY_EXIST("WalkingStick_Lester")
            		IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("WalkingStick_Lester"))
                   		LesterCane = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("WalkingStick_Lester")
       					PRINTLN("CREATING LESTER'S CANE")
                    ENDIF
            	ENDIF
            ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				// Do other stuff here
			ENDIF
			
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester")) 
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester") //, IG_LESTERCREST)
					IF DOES_ENTITY_EXIST(piLester) AND NOT IS_PED_INJURED(piLester)
						SET_ENTITY_COORDS(piLester, << -1509.458, -948.195, 7.750 >>)
						SET_ENTITY_HEADING(piLester, -11.000)
						TASK_PLAY_ANIM(piLester, "oddjobs@assassinate@hotel@leadin", "lester_leadin", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0, FALSE, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(piLester)
						PRINTLN("TASKING LESTER TO PLAY IDLE ANIMATION")
					ELSE
						PRINTLN("LESTER DOES NOT EXIST")
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester")) 
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("WalkingStick_Lester")
					IF DOES_ENTITY_EXIST(piLester)
					AND NOT IS_PED_INJURED(piLester)
						// re-attach the cane to his hand at an angle that looks good when he walks away
						ATTACH_ENTITY_TO_ENTITY(LesterCane, piLester, GET_PED_BONE_INDEX(piLester, BONETAG_PH_R_HAND), << 0,0,0 >>, << 0,0,0 >>)
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			AND NOT IS_CUTSCENE_PLAYING()
	
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				IF NOT IS_PED_INJURED(piLester)
					SET_ENTITY_PROOFS(piLester, FALSE, FALSE, FALSE, FALSE, FALSE)
					SET_PED_CAN_RAGDOLL(piLester, TRUE)
				ENDIF				
									
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				IF IS_SCREEN_FADED_OUT()
					WAIT(0)
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				ODDJOB_EXIT_CUTSCENE()
				
				iStageNum ++
				PRINTLN("DO_STAGE_INTRO_CUTSCENE, iStageNum = ", iStageNum)
			ENDIF

		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE 7
			RESTART_TIMER_NOW(tLesterTimer)
			
			INITIALIZE_NEXT_STAGE()
			MainStage = STAGE_INIT
			PRINTLN("GOING TO STATE - STAGE_INIT")
		BREAK
	ENDSWITCH
ENDPROC


PROC HANDLE_PLAYER_ABANDON_MISSION()
	VECTOR vPlayerPos
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		bResetAbandonCheck = TRUE
		EXIT
	ENDIF

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	IF bResetAbandonCheck
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			fToHotelDistance = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vNewBlipPosition)
			PRINTLN("RESET: HOTEL DISTANCE = ", fToHotelDistance)
			bResetAbandonCheck = FALSE
		ENDIF
	ELSE
		IF fToHotelDistance = 0
			fToHotelDistance = VDIST(<<-1510.2909, -946.9932, 8.2780>>, vNewBlipPosition)
			PRINTLN("HOTEL DISTANCE = ", fToHotelDistance)
		ENDIF
	ENDIF
	
	IF VDIST(vPlayerPos, vNewBlipPosition) > (fToHotelDistance + 400)
		PRINTLN("FAILING - MISSION ABANDONED")
		SET_MISSION_FAILED(FT_ABANDONED)
	ELIF VDIST(vPlayerPos, vNewBlipPosition) > (fToHotelDistance + 200)
		IF NOT bGaveAbandondedWarning
			PRINT_NOW("ASS_VA_RHOTEL", DEFAULT_GOD_TEXT_TIME, 1)
			PRINTLN("bGaveAbandondedWarning = TRUE")
			bGaveAbandondedWarning = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle blip behavior in first stage
PROC DO_STAGE_GET_TO_HOTEL()
	VEHICLE_INDEX tempVeh
	
	SWITCH iStageNum
		// do wanted checks immediately
		CASE 0
			FADE_IN()
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "assassin_hotel_go_to_hotel")
			
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				PRINT_NOW("ASS_VA_LOSECOPS", DEFAULT_GOD_TEXT_TIME, 1)
			ENDIF
			CLEAR_AREA_OF_VEHICLES(<< -1220.57, -185.96, 38.40 >>, 50)	//area around where the limo pulls up
			
			iStageNum ++
		BREAK
		
		//create blip for hotel once player is not wanted
		CASE 1
		
			HANDLE_PLAYER_ABANDON_MISSION()
		
			IF NOT DOES_ENTITY_EXIST(viReplayVehicle)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_DRIVER) = PLAYER_PED_ID()
							viReplayVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							PRINTLN("FOUND REPLAY VEHICLE")
							
							IF DOES_ENTITY_EXIST(viReplayVehicle) AND NOT IS_ENTITY_DEAD(viReplayVehicle)
								OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(viReplayVehicle)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				IF NOT IS_PLAYER_IN_RANGE_OF_HOTEL()
					IF NOT DOES_BLIP_EXIST(bHotelBlip)
						bHotelBlip = CREATE_BLIP_FOR_COORD(vNewBlipPosition, TRUE)
						SET_BLIP_NAME_FROM_TEXT_FILE(bHotelBlip, "ASS_VA_DESTBLIP")
						
						//if the player hails a taxi it will drop them off in front of the hotel
						SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(bHotelBlip, <<-1234.3065, -250.6783, 38.2238>>, 28.9140)
						
						IF NOT bObjectivePrinted
							PRINT_NOW("ASS_VA_GOHOTEL", DEFAULT_GOD_TEXT_TIME, 1)
							bObjectivePrinted = TRUE
						ELSE
							IF IS_THIS_PRINT_BEING_DISPLAYED("ASS_VA_LOSECOPS")
								CLEAR_PRINTS()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF STREAMVOL_HAS_LOADED(svIntroCutscene)
					AND ARE_MODELS_LOADED_FOR_GUARDS()
						CLEANUP_DEST_BLIPS()
						INITIALIZE_NEXT_STAGE()
						MainStage = STAGE_GUARD_CUTSCENE
						PRINTLN("OJASHOTEL - MainStage = STAGE_GUARD_CUTSCENE")
					ENDIF
				ENDIF		
			ELSE
				IF NOT IS_PLAYER_IN_RANGE_OF_HOTEL()
					//remove destination blip and tell player to lose cops again
					REMOVE_BLIP_AND_PRINT_OBJECTIVE_MSG_IF_WANTED(bHotelBlip, "ASS_VA_LOSECOPS")
				ELSE
					//otherwise fail the mission for being wanted near the hotel
					PRINTLN("OJASHOTEL - FAILING THE MISSION BECAUSE PLAYER IS WANTED NEAR THE HOTEL PRIOR TO TRIGGERING INTRO CUTSCENE")
					SET_MISSION_FAILED(FT_BLOWN_COVER_WANTED)
				ENDIF
			ENDIF
			
					
			HANDLE_STREAMVOL_AND_GUARD_MODEL_LOADING_UNLOADING()
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE: Handle cutscene of guards pulling up to the hotel when player arrives
PROC DO_STAGE_GUARD_CUTSCENE()
	
	SWITCH iStageNum
		CASE 0
			//bring car to a stop gracefully or wait until player isnt ragdolling to enter the cutscene
			IF IS_PLAYER_READY_FOR_ASSASSINATION_CUTSCENE() OR bPlayerIsInFlyingVehicle
			AND STREAMVOL_HAS_LOADED(svIntroCutscene)
				//clear area around hotel and create limo and limo guards
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1248.397583,-183.366013,37.728809>>, <<-1216.436768,-196.872726,44.075413>>, 59.250000, vCheckpointRetryPosition, fCheckpointRetryHeading)
				
				CLEAR_AREA_OF_VEHICLES(<< -1220.57, -185.96, 38.40 >>, 50)	//area around where the limo pulls up
				CLEAR_AREA_OF_PROJECTILES(<< -1220.57, -185.96, 38.40 >>, 50)
				CREATE_TARGET_CAR()
				CREATE_CUTSCENE_PEDS()
			
				IF NOT IS_ENTITY_DEAD(viTargetCar)
					IF IS_VEHICLE_DRIVEABLE(viTargetCar)
						START_PLAYBACK_RECORDED_VEHICLE(viTargetCar, 107, "ASSOJva")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(viTargetCar, 2500)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						SET_VEH_RADIO_STATION(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), "OFF")
					ENDIF
				ENDIF
				
				iStageNum ++
			ELSE
				IF STREAMVOL_HAS_LOADED(svIntroCutscene)
					PRINTLN("STREAMVOL HAS LOADED")
				ELSE
					PRINTLN("STREAMVOL HAS NOT LOADED")
				ENDIF
			ENDIF
		BREAK

		CASE 1
			IF HAS_INTRO_CUTSCENE_FINISHED()
				//re-initialize iCutsceneStage variable for Kill Camera
				iCutsceneStage = 0
				
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				
				REMOVE_STREAMVOL_FOR_INTRO_CUTSCENE()
								
				INITIALIZE_NEXT_STAGE()
				PRINTLN("OJASHOTEL - MainStage = STAGE_PLANT_BOMB")
				MainStage = STAGE_PLANT_BOMB
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Allow the player to place a bomb on the target's vehicle if they arrive before the target exits the hotel
PROC DO_STAGE_PLANT_BOMB()	
	
//	IF IS_VEHICLE_BLOCKING_ANY_AIRLOCK(6.0)
//		PRINTLN("GOING TO FAIL FOR BLOCKING AIRLOCK")
//		FAIL_MISSION_FOR_CLOSE_VEHICLE()
//	ENDIF
	
	SWITCH iStageNum
		//wait for player to plant a bomb on the limo
		CASE 0
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "assassin_hotel_plan_attack")
			
			//re-enable wanted as an aggro check
			CLEAR_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
			
			IF IS_VEHICLE_DRIVEABLE(viTargetCar)
				SET_VEHICLE_ENGINE_ON(viTargetCar, FALSE, TRUE)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(viReplayVehicle) AND NOT IS_ENTITY_DEAD(viReplayVehicle)
						OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(viReplayVehicle)
						PRINTLN("OVERRIDING CHECKPOINT VEHICLE IF THE PLAYER WAS IN A VEHICLE BUT NOT AT CHECKPOINT")
					ENDIF
				ENDIF
			ENDIF
			
			//create motorcade peds and peds guarding airlock door positions
			CREATE_MOTORCADE_ACTORS_AND_VEHICLE()
			CREATE_LIMO_GUARDS()
//			CREATE_DOOR_GUARDS()

			TRIGGER_MUSIC_EVENT("ASS1_START")
			PRINTLN("TRIGGERING MUSIC - ASS1_START")
			
			//don't check this bit until the player has been warned previously or until guards are ready to leave their posts (DO_ADDITIONAL_HOTEL_PANIC_CHECKS should cover shotnear)
			//this will allow the player to safely plant a bomb on the targets vehicle
			SET_BIT(aggroArgs.iBitFieldDontCheck, ENUM_TO_INT(EAggro_ShotNear))
			
//			//store this number to check if bombs have been planted later in the script
//			iBombCount = GET_AMMO_IN_PED_WEAPON(Player, WEAPONTYPE_STICKYBOMB)
			
			PRINT_NOW("ASS_VA_PLANT", DEFAULT_GOD_TEXT_TIME, 1)

			iStageNum++
		BREAK
		CASE 1
			//60% of time has passed - start telling the guards to exit the hotel and guard their posts
			IF bTriggerGuardWalk
				
				IF NOT IS_NON_SCRIPT_CREATED_VEHICLE_IN_HOTEL_AREA(viCarThatTriggeredAlert)
				AND NOT DID_PLAYER_BLOCK_ALLEY_WITH_A_PARKED_CAR(viLastDriven[0], bPlayerParkedCarInAlley)
				AND NOT DID_PLAYER_BLOCK_ALLEY_WITH_A_PARKED_CAR(viLastDriven[1], bPlayerParkedCarInAlley)
				AND NOT DID_PLAYER_BLOCK_ALLEY_WITH_A_PARKED_CAR(viLastDriven[2], bPlayerParkedCarInAlley)
					SETTIMERA(0)
					INITIALIZE_NEXT_STAGE()
					MainStage = STAGE_GUARDS_GO_TO_POSTS
					PRINTLN("OJASHOTEL - MainStage = STAGE_GUARDS_GO_TO_POSTS")
				ELSE
					PRINTLN("VEHICLES MUST BE IN THE AREA")
					FAIL_MISSION_FOR_CLOSE_VEHICLE()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE: Tell the guards to walk to their guardposts
PROC DO_STAGE_GUARDS_GO_TO_POSTS()
	SWITCH iStageNum
		CASE 0
			IF NOT bInPursuit
				
				CLEAR_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_ShotNear) //re-allow all aggro checks
	
				//Tell guards to enter the limo
				TASK_LIMO_GUARDS_TO_ENTER_VEHICLES()
	
				SETTIMERA(0)
				iStageNum++
				
			ENDIF
		BREAK
		
		CASE 1
			//only advance to the next stage once the timer expires and we know that the player hasn't parked their vehicle close enough to fail the mission
			IF bHotelTimerExpired
			AND NOT bFailMissionForCloseVehicle
			AND ARE_GUARDS_IN_VEHICLE()
			AND ARE_MODELS_LOADED_FOR_TARGET()
				INITIALIZE_NEXT_STAGE()
				MainStage = STAGE_TARGET_LEAVE_LOBBY
				PRINTLN("OJASHOTEL - MainStage = STAGE_TARGET_LEAVE_LOBBY")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE: have the target and limo guards walk to the limo
PROC DO_STAGE_TARGET_LEAVE_LOBBY()
		
	//remove any existing blips
	CLEANUP_PED_BLIPS(FALSE)
	
	//create and blip target, set his update loop to enter the vehicle, and task player to kill him
	CREATE_TARGET()
	CREATE_BODYGUARDS()
	
	//release models now that they are no longer needed
	CLEANUP_EXTRAS()
	UNLOAD_GUARD_MODELS()
	
	PRINT_NOW("ASS_VA_KILL", DEFAULT_GOD_TEXT_TIME, -1)
	
//	//check to see if there is undetonated C4 that the player planted so we can properly display help messages to explain how to detonate later
//	IF GET_AMMO_IN_PED_WEAPON(Player, WEAPONTYPE_STICKYBOMB) < iBombCount
//		IF IS_PROJECTILE_TYPE_IN_AREA(<< -1298.6843, -324.0442, -35.5780 >>, << -1113.3724, -46.1583, 55.6090 >>, WEAPONTYPE_STICKYBOMB)
//			bPlantMsg = TRUE
//			bPlayerHasPlantedBomb = TRUE
//		ENDIF
//	ENDIF
	
	SETTIMERA(0)
	
	bTargetLeaving = TRUE
	INITIALIZE_NEXT_STAGE()				
	MainStage = STAGE_ENTER_LIMO
	PRINTLN("OJASHOTEL - MainStage = STAGE_ENTER_LIMO")
	
ENDPROC

/// PURPOSE: Target is escorted to the limo by armed security
PROC DO_STAGE_ENTER_LIMO()
	SWITCH iStageNum
		CASE 0
			//make sure the driver, target, and limo are alive/driveable before leaving the hotel
			//otherwise have the surviving peds exit the vehicle, attack the player, and have the target flee on foot
			IF IS_VEHICLE_DRIVEABLE(viTargetCar)
				IF IS_TARGET_IN_LIMO()
					IF NOT IS_PED_INJURED(piLimoGuard[0])	// Driver ped
						IF IS_VEHICLE_DRIVEABLE(viTargetCar)
							IF IS_PED_IN_VEHICLE(piLimoGuard[0], viTargetCar)
							
								bLeaveInLimo = TRUE
								INITIALIZE_NEXT_STAGE()
								MainStage = STAGE_LEAVE_HOTEL
								PRINTLN("OJASHOTEL - MainStage = STAGE_LEAVE_HOTEL")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	
	//remind the player how to use stickybombs if they have attached any to the target's car.
//	REMIND_PLAYER_HOW_TO_USE_STICKYBOMBS()
ENDPROC


/// PURPOSE: Target drives out of hotel area
PROC DO_STAGE_LEAVE_HOTEL()
	IF NOT bLimoCruisingStreets
		IF DOES_ENTITY_EXIST(viTargetCar)
			IF IS_VEHICLE_DRIVEABLE(viTargetCar)
				
				SET_VEHICLE_DOORS_LOCKED(viTargetCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY) // To prevent the player from jacking the target car
			
				IF IS_CAR_AT_STREET(viTargetCar, 5)
//				OR IS_TARGET_CAR_AT_PLAYER_CAR(5.0)
					bLimoCruisingStreets = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bLimoStuck
		IF IS_VEHICLE_STUCK(viTargetCar, 10000)
			bLimoStuck = TRUE
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Play a kill camera of the target
PROC DO_STAGE_KILL_CAM()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vAreaCoord01, vAreaCoord02, fAreaWidth, TRUE, FALSE)
		PRINTLN("PLAYER IS IN ANGLED AREA")
		bIsPlayerInHotelArea = TRUE
	ELSE
		PRINTLN("PLAYER IS NOT IN ANGLED AREA")
		bIsPlayerInHotelArea = FALSE
	ENDIF
	
	IF DOES_BLIP_EXIST(bTargetBlip)
		REMOVE_BLIP(bTargetBlip)
		PRINTLN("REMOVING bTargetBlip IN DO_STAGE_KILL_CAM")
	ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(0.5)
	PRINTLN("SETTING WANTED MULTIPLIER TO 0.5")
	
	IF bIsPlayerInHotelArea
		INITIALIZE_NEXT_STAGE()
		MainStage = STAGE_LEAVE_AREA
		PRINTLN("OJASHOTEL - MainStage = STAGE_LEAVE_AREA")
	ELSE
		INITIALIZE_NEXT_STAGE()
		MainStage = STAGE_EVADE_POLICE
		PRINTLN("OJASHOTEL - MainStage = STAGE_EVADE_POLICE")
	ENDIF
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
ENDPROC



/// PURPOSE: Target is escaping the player after having been alerted
PROC DO_STAGE_EVADE_POLICE()

//	HANDLE_SPECIAL_CASE_AUDIO_CUE()
	
	HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, hotelConv, "OJASAUD", "OJAS_HOCOM", CONV_PRIORITY_VERY_HIGH)
	
	SWITCH iStageNum
		CASE 0
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				IF NOT bPrintLoseWantedLevel
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							PRINTLN("EVADE POLICE - PLAYER IS WANTED")
							PRINT_NOW("ASS_VA_LOSECOPS", DEFAULT_GOD_TEXT_TIME, 1)
							iStageNum++
							bPrintLoseWantedLevel = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELIF GET_CLOSEST_GUARD() != NULL
				IF NOT bPrintLastObjective 
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							PRINT_NOW("ASS_VA_GO", DEFAULT_GOD_TEXT_TIME, -1)
							PRINTLN("EVADE POLICE - LEAVE AREA")
							iStageNum++
							bPrintLastObjective = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				AND GET_CLOSEST_GUARD(125) = NULL
					PRINTLN("FAIL SAFE: EVADE POLICE - GOING TO CASE 1")
					iStageNum++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			AND GET_CLOSEST_GUARD(125.0) = NULL		//player has evaded all peds
				HANDLE_SPECIAL_CASE_AUDIO_CUE()
				CLEANUP_PEDS()
				CLEANUP_PED_BLIPS()
				CLEANUP_VEHICLES()
				PRINTLN("EVADE POLICE - NO LONGER WANTED AND NOT GUARDS AROUND")
				iStageNum++
			ENDIF
		BREAK
		CASE 2
			HANDLE_SPECIAL_CASE_AUDIO_CUE()
			PRINTLN("EVADE POLICE - GOING TO STATE - STAGE_MISSION_PASSED")
			MainStage = STAGE_MISSION_PASSED
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_STAGE_LEAVE_AREA()
	FLOAT fDistanceFromLocation
	
	IF NOT bPrintLastObjective 
		IF NOT IS_MESSAGE_BEING_DISPLAYED()
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT_NOW("ASS_VA_GO", DEFAULT_GOD_TEXT_TIME, -1)
				bPrintLastObjective = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, hotelConv, "OJASAUD", "OJAS_HOCOM", CONV_PRIORITY_VERY_HIGH)
	
	SWITCH iStageNum
		CASE 0			
			PRINTLN("GOING TO STATE 1 IN DO_STAGE_LEAVE_AREA")
			iStageNum++
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				fDistanceFromLocation = GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vCenterPoint)

				IF fDistanceFromLocation > 155
				AND GET_CLOSEST_GUARD() = NULL		//player has evaded all peds
					bLeftArea = TRUE
					PRINTLN("bLeftArea = TRUE")
					
					CLEANUP_PEDS()
					
					iStageNum++
				ELSE
					//ensure the player cannot lose their wanted level if they haven't left the area
					SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
				ENDIF
			ENDIF
		BREAK
		CASE 2
			// If we're not wanted, go ahead and make the phone call
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				IF GET_CLOSEST_GUARD() = NULL		//player has evaded all peds
					HANDLE_SPECIAL_CASE_AUDIO_CUE()
					
					IF HAS_ENDING_CELL_PHONE_CALL_COMPLETED()
						PRINTLN("GOING TO STATE - STAGE_MISSION_PASSED")
						MainStage = STAGE_MISSION_PASSED
					ENDIF
				ENDIF
			ELSE
				// If we're wanted, go to a state and check until we lose it..
				IF NOT bPrintLoseWantedLevel
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							PRINTLN("WE'RE WANTED - GO TO CASE 3")
							PRINT_NOW("ASS_VA_LOSECOPS", DEFAULT_GOD_TEXT_TIME, 1)
							iStageNum++
							bPrintLoseWantedLevel = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				PRINTLN("WE'RE NO LONGER WANTED, GOING TO CASE 2")
				iStageNum = 2
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_STAGE_MISSION_PASSED()
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	Script_Passed()
ENDPROC


PROC TRIGGER_PANIC_IF_BOMB_IN_AIRLOCK_IF_NOT_ALREADY_PANICKED(VECTOR vecAngledAreaPoint1, VECTOR vecAngledAreaPoint2, FLOAT DistanceOfOppositeFace)
	IF NOT bPanicking
		IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vecAngledAreaPoint1, vecAngledAreaPoint2, DistanceOfOppositeFace, WEAPONTYPE_STICKYBOMB)
			bPanicking = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Handle all of the enemy ped loops - only check one ped per frame for performance reasons
PROC UPDATE_MISSION_PED_STATES()
	INT idx
	
	//check the target every frame
	UPDATE_TARGET_STATE()
	
//	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piLimoGuard[0], Player)
//	OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piLimoGuard[1], Player)
//	OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piLimoGuard[2], Player)
//		SCRIPT_ASSERT("DAMAGED A LIMO GUARD!")
//	ELSE
//		PRINTLN("PLAYER HAS NOT DAMAGED ANY LIMO GUARDS")
//	ENDIF
	
	//TODO - put the loops below into the update states so it isnt doing this every frame?
	REPEAT NUM_LIMO_GUARDS idx
		IF DOES_ENTITY_EXIST(piLimoGuard[idx])
			IF bPanicking AND NOT bLeftArea
				UPDATE_AI_PED_BLIP(piLimoGuard[idx], blipCombatLimoGuards[idx], -1, NULL, FALSE, FALSE, 150)
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT NUM_ESCORT_PEDS idx
		IF DOES_ENTITY_EXIST(piEscortPed[idx])
			IF bPanicking AND NOT bLeftArea
				UPDATE_AI_PED_BLIP(piEscortPed[idx], blipCombatEscort[idx], -1, NULL, FALSE, FALSE, 150)
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT NUM_BODYGUARDS idx
		IF DOES_ENTITY_EXIST(piBodyguard[idx])
			IF bPanicking AND NOT bLeftArea
				UPDATE_AI_PED_BLIP(piBodyguard[idx], blipBodyguard[idx], -1, NULL, FALSE, FALSE, 150)
			ENDIF
			
			IF NOT IS_PED_INJURED(piBodyguard[idx])
				IF BodyGuardState[idx] = BS_ALERTED2
					//constantly call this every time since the ped has been tasked to combat BS_ALERTED but we just want them to look like they are in cover, but not shoot
					//this needs to be called here bcause UPDATE_BODYGUARD_STATE doesnt get called every frame (see later in this function)
					SET_PED_RESET_FLAG(piBodyguard[idx], PRF_BlockWeaponFire, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	IF iFrameCountAsn = 0
		UPDATE_BODYGUARD_STATE(0)
	ELIF iFrameCountAsn = 1
		UPDATE_BODYGUARD_STATE(1)
	ELIF iFrameCountAsn = 2
		IF MainStage >= STAGE_PLANT_BOMB
			UPDATE_LIMO_GUARD_STATE(0)
		ENDIF
	ELIF iFrameCountAsn = 3
		IF MainStage >= STAGE_PLANT_BOMB
			UPDATE_LIMO_GUARD_STATE(1)
		ENDIF
	ELIF iFrameCountAsn = 4
		IF MainStage >= STAGE_PLANT_BOMB
			UPDATE_LIMO_GUARD_STATE(2)
			
			TRIGGER_PANIC_IF_BOMB_IN_AIRLOCK_IF_NOT_ALREADY_PANICKED(<<-1222.557617,-173.747086,38.325413>>, <<-1220.101196,-169.053146,42.075413>>, 4.000000)
		ENDIF
	ELIF iFrameCountAsn = 5
		UPDATE_ESCORT_STATE(0)
		
		TRIGGER_PANIC_IF_BOMB_IN_AIRLOCK_IF_NOT_ALREADY_PANICKED(<<-1213.432251,-191.326141,38.325413>>, <<-1208.552856,-193.861816,42.075344>>, 4.000000)
	ELIF iFrameCountAsn = 6
		UPDATE_ESCORT_STATE(1)
		
		TRIGGER_PANIC_IF_BOMB_IN_AIRLOCK_IF_NOT_ALREADY_PANICKED(<<-1219.140259,-202.312424,38.325344>>, <<-1214.269287,-204.903503,42.075344>>, 4.000000)
	ENDIF
	
	//increment it by one - reset it if it exceeds the checks
	iFrameCountAsn ++
	IF iFrameCountAsn > 6
		iFrameCountAsn = 0
	ENDIF
	
	IF bPanicking
		IF NOT IS_ENTITY_DEAD(piTarget)
			SET_PED_RESET_FLAG(piTarget, PRF_PanicInVehicle, TRUE)
//			PRINTLN("SETTING PANIC FLAG ON TARGET VIA PANIC CHECK")
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_HINT_CAMERAS()
	IF DOES_ENTITY_EXIST(piTarget)
		IF NOT IS_PED_INJURED(piTarget)
			CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, piTarget)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: 
///    	Handles all ped update states
PROC UPDATE_PED_STATES()	
	
	//updates guard states and panic checks for the guards - throttles these to update one ped per frame for performance reasons
	UPDATE_MISSION_PED_STATES()
	
	IF NOT bAlerted
		IF NOT bPanicking
			//check for the player aggressively approaching the limo or setting off explosions around the hotel lobby
			IF DO_ADDITIONAL_HOTEL_PANIC_CHECKS(failType)
				bPanicking = TRUE
				PRINTLN("bPanicking = TRUE VIA DO_ADDITIONAL_HOTEL_PANIC_CHECKS - UPDATE_PED_STATES")
			ENDIF
		ENDIF
	ENDIF
ENDPROC



FUNC BOOL CHECK_FOR_PED_SEEING_PLAYER()
	INT idx

	REPEAT NUM_LIMO_GUARDS idx
		IF DOES_ENTITY_EXIST(piLimoGuard[idx]) AND NOT IS_ENTITY_DEAD(piLimoGuard[idx])
			
			SET_PED_RESET_FLAG(piLimoGuard[idx], PRF_IgnoreTargetsCoverForLOS, TRUE)
		
			IF CAN_PED_SEE_HATED_PED(piLimoGuard[idx], PLAYER_PED_ID())
				PRINTLN("LIMO GUARD HAS SPOTTED THE PLAYER: ", idx)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT NUM_ESCORT_PEDS idx
		IF DOES_ENTITY_EXIST(piEscortPed[idx]) AND NOT IS_ENTITY_DEAD(piEscortPed[idx])
			
			SET_PED_RESET_FLAG(piEscortPed[idx], PRF_IgnoreTargetsCoverForLOS, TRUE)
		
			IF CAN_PED_SEE_HATED_PED(piEscortPed[idx], PLAYER_PED_ID())
				PRINTLN("ESCORT GUARD HAS SPOTTED THE PLAYER: ", idx)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT NUM_BODYGUARDS idx
		IF DOES_ENTITY_EXIST(piBodyguard[idx]) AND NOT IS_ENTITY_DEAD(piBodyguard[idx])
			
			SET_PED_RESET_FLAG(piBodyguard[idx], PRF_IgnoreTargetsCoverForLOS, TRUE)
		
			IF CAN_PED_SEE_HATED_PED(piBodyguard[idx], PLAYER_PED_ID())
				PRINTLN("BODY GUARD HAS SPOTTED THE PLAYER: ", idx)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


FUNC PED_INDEX GRAB_ANY_GUARD()
	PED_INDEX pedToSpeak
	INT idx
	
	REPEAT NUM_LIMO_GUARDS idx
		IF DOES_ENTITY_EXIST(piLimoGuard[idx]) AND NOT IS_PED_INJURED(piLimoGuard[idx])
			pedToSpeak = piLimoGuard[idx]
			PRINTLN("FOUND PED TO SPEAK, LIMO GUARD INDEX = ", idx)
		ENDIF
	ENDREPEAT
	
	IF pedToSpeak = NULL
		REPEAT NUM_ESCORT_PEDS idx
			IF DOES_ENTITY_EXIST(piEscortPed[idx]) AND NOT IS_PED_INJURED(piEscortPed[idx])
				pedToSpeak = piEscortPed[idx]
				PRINTLN("FOUND PED TO SPEAK, ESCORT GUARD INDEX = ", idx)
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF pedToSpeak = NULL
		REPEAT NUM_BODYGUARDS idx
			IF DOES_ENTITY_EXIST(piBodyguard[idx]) AND NOT IS_PED_INJURED(piBodyguard[idx])
				pedToSpeak = piBodyguard[idx]
				PRINTLN("FOUND PED TO SPEAK, BODY GUARD INDEX = ", idx)
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN pedToSpeak
ENDFUNC


PROC HANDLE_SNIPER_ATTACK_BUFFER()
//	INT idx
	PED_INDEX pedToSpeak

	SWITCH iHandleSniperAttackStages
		CASE 0 
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				IF NOT IS_ENTITY_DEAD(piLimoGuard[0])
					TASK_CLEAR_LOOK_AT(piLimoGuard[0])
				ENDIF
				IF NOT IS_ENTITY_DEAD(piLimoGuard[1])
					TASK_CLEAR_LOOK_AT(piLimoGuard[1])
				ENDIF
				
				pedToSpeak = GRAB_ANY_GUARD()
				
				// Try playing initial dialgoue first here.
				IF bPlayerUsedStickyBomb
					IF NOT bPlayIntialDialogue
						SETUP_PED_FOR_DIALOGUE(pedToSpeak, 3, "OJAvaGUARD")
						IF CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_EXPLO", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("PLAYING EXPLOSION CONVO")
							bPlayIntialDialogue = TRUE
						ENDIF
					ENDIF
				ELSE
					IF NOT bTargetFleeing AND IS_PED_INJURED(piTarget)
						IF NOT bPlayIntialDialogue
							SETUP_PED_FOR_DIALOGUE(pedToSpeak, 3, "OJAvaGUARD")
							IF CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_SNIPE", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING SNIPE CONVO IN SNIPER ATTACK BUFFER CASE 0")
								bPlayIntialDialogue = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_TIMER_STARTED(tBufferTimer)
					START_TIMER_NOW(tBufferTimer)
					PRINTLN("STARTING TIMER - tBufferTimer")
				ELSE
					RESTART_TIMER_NOW(tBufferTimer)
					PRINTLN("RESTARTING TIMER - tBufferTimer")
				ENDIF
			
				PRINTLN("iHandleSniperAttackStages = 1")
				iHandleSniperAttackStages = 1
			ENDIF
		BREAK
		//-------------------------------------------------------------------------------------------------------------------
		CASE 1 
	
			PRINTLN("iHandleSniperAttackStages = 2")
			iHandleSniperAttackStages = 2
		BREAK
		//-------------------------------------------------------------------------------------------------------------------
		CASE 2 
	
			IF bPlayIntialDialogue OR NOT IS_PED_INJURED(piTarget)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT bPlayerUsedStickyBomb  // Dialogue doesn't quite work with stick bomb kill
					IF NOT bPlayedSearchDialogue
						pedToSpeak = GRAB_ANY_GUARD()
						SETUP_PED_FOR_DIALOGUE(pedToSpeak, 3, "OJAvaGUARD")
						IF CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_LOOK", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("PLAYING LOOK CONVO")
							bPlayedSearchDialogue = TRUE
						ENDIF
					ELSE
						IF NOT bTargetDeadLinePlayed
						AND IS_PED_INJURED(piTarget)
							pedToSpeak = GRAB_ANY_GUARD()
							PLAY_TARGET_DEAD_LINE(pedToSpeak)		//bTargetDeadLinePlayed = TRUE gets set inside this function
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bPlayerUsedStickyBomb
					IF NOT bPlayIntialDialogue
						SETUP_PED_FOR_DIALOGUE(pedToSpeak, 3, "OJAvaGUARD")
						IF CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_EXPLO", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("PLAYING EXPLOSION CONVO")
							bPlayIntialDialogue = TRUE
						ENDIF
					ENDIF
				ELSE
					IF NOT bTargetFleeing
						IF NOT bPlayIntialDialogue
							SETUP_PED_FOR_DIALOGUE(pedToSpeak, 3, "OJAvaGUARD")
							IF CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_SNIPE", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING SNIPE CONVO IN SNIPER ATTACK BUFFER CASE 2")
								bPlayIntialDialogue = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

//			IF bAlertedSpecial 
//				fPanicTime = 22.0
////				PRINTLN("fPanicTime = 22.0")
//			ELSE
//				fPanicTime = 15.0
////				PRINTLN("fPanicTime = 15.0")
//			ENDIF
			
			IF IS_ENTITY_IN_SAFE_LOCATION(Player)
				fPanicTime = 2.5
			ELSE
				fPanicTime = 5.0
			ENDIF
			
			IF IS_TIMER_STARTED(tBufferTimer)
				IF (GET_TIMER_IN_SECONDS(tBufferTimer) > fPanicTime AND CHECK_FOR_PED_SEEING_PLAYER())
				OR IS_PED_SHOOTING(PLAYER_PED_ID())
					
					KILL_ANY_CONVERSATION()
//					CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_SPOT", CONV_PRIORITY_VERY_HIGH)		//moved this to the part where we trigger the score
					
					IF NOT bPanicking
						bPanicking = TRUE
						bBufferHit = TRUE
						PRINTLN("bPanicking = TRUE VIA - HANDLE_SNIPER_ATTACK_BUFFER")
					ENDIF
					
					bAlerted = FALSE
					PRINTLN("bAlerted = FALSE")
				ENDIF
			ENDIF
		BREAK
		//-------------------------------------------------------------------------------------------------------------------
		CASE 3 
		
		BREAK
	ENDSWITCH
ENDPROC

PROC REMOVE_LIMO_GUARDS()
	INT idx
	REPEAT NUM_LIMO_GUARDS idx
		IF DOES_ENTITY_EXIST(piLimoGuard[idx])
//			DELETE_PED(piLimoGuard[idx])
			SET_PED_AS_NO_LONGER_NEEDED(piLimoGuard[idx])
			PRINTLN("SETTING NO LONGER NEEDED -  LIMO PED: ", idx)
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVE_TARGET_CAR()
	IF DOES_ENTITY_EXIST(viTargetCar)
//		DELETE_VEHICLE(viTargetCar)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(viTargetCar)
		PRINTLN("SETTING  viTargetCar as no longer needed!")
	ENDIF
ENDPROC

PROC REMOVE_TARGET_AND_BLIP()
	IF DOES_ENTITY_EXIST(piTarget)
//		DELETE_PED(piTarget)
		SET_PED_AS_NO_LONGER_NEEDED(piTarget)
		PRINTLN("SETTING TARGET NO LONGER NEEDED")
	ENDIF
	IF DOES_BLIP_EXIST(bTargetBlip)
		REMOVE_BLIP(bTargetBlip)
		PRINTLN("REMOVING bTargetBlip")
	ENDIF
ENDPROC

PROC REMOVE_BODYGUARDS()
	INT idx
	REPEAT NUM_BODYGUARDS idx
		IF DOES_ENTITY_EXIST(piBodyguard[idx])
//			DELETE_PED(piBodyguard[idx])
			SET_PED_TO_LOAD_COVER(piBodyguard[idx], FALSE)
			SET_PED_AS_NO_LONGER_NEEDED(piBodyguard[idx])
			PRINTLN("SETTING piBodyguard[idx] No Longer Needed!")
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE: Create the motorcade car and bike vehicles
PROC REMOVE_MOTORCADE_FOLLOW_CAR()
	IF DOES_ENTITY_EXIST(viEscortCar)
//		DELETE_VEHICLE(viEscortCar)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(viEscortCar)
		PRINTLN("SETTING  viEscortCar as no longer needed!")
	ENDIF
ENDPROC

PROC REMOVE_MOTORCADE_PEDS()
	INT tempInt

	REPEAT NUM_ESCORT_PEDS tempInt
		IF DOES_ENTITY_EXIST(piEscortPed[tempInt])
//			DELETE_PED(piEscortPed[tempInt])
			SET_PED_AS_NO_LONGER_NEEDED(piEscortPed[tempInt])
			PRINTLN("SETTING piEscortPed[idx] No Longer Needed!")
		ENDIF
	ENDREPEAT
ENDPROC



PROC GO_TO_CHECKPOINT_1()
	
	// If we're skipping to this stage load all that's needed or delete what's not.
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1510.1810, -946.9595, 8.2738>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 325.8271)
			
			PRINTLN("REPOSITIONING PLAYER FOR GO_TO_CHECKPOINT_1")
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
//			bDebugSkipping = TRUE
			
			//limo pulling up to the hotel
			REQUEST_VEHICLE_RECORDING(102, "ASSOJva")
			//limo driving into the hotel
			REQUEST_VEHICLE_RECORDING(107, "ASSOJva")
			REQUEST_MODEL(FBI2)
			
			INITIALIZE_NEXT_STAGE()
			bHotelTimerStarted = FALSE
			bHotelTimerExpired = FALSE
			bTaxiLeave = FALSE
			bDriveToLimo = FALSE
			bAlerted = FALSE
			bBufferHit = FALSE
			bPlayerUsedSniperRifle = FALSE
			bGaveAbandondedWarning = FALSE
			
			REMOVE_LIMO_GUARDS()
			REMOVE_TARGET_CAR()
			REMOVE_TARGET_AND_BLIP()
			REMOVE_BODYGUARDS()
			REMOVE_MOTORCADE_FOLLOW_CAR()
			REMOVE_MOTORCADE_PEDS()
			
			iHandleSniperAttackStages = 0
			
//			IF bFirstCheckpointSceneLoad
//				bFirstCheckpointSceneLoad = FALSE
//			ENDIF
			
			iCutsceneStage = 0
		
			WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(102, "ASSOJva")  //limo pulling up to the hotel lobby
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(107, "ASSOJva")  //limo driving up to the hotel
			OR NOT HAS_MODEL_LOADED(FBI2)
				WAIT(0)
				PRINTLN("P-SKIPPING: WAITING RECORDING TO LOAD")
			ENDWHILE
			
			bDoingPSkip = FALSE
		ENDIF
	#ENDIF
	
		
//	HANDLE_REPLAY_WEAPON_EQUIP()
	
	END_REPLAY_SETUP()
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	//fade in the game from the mission retry screen
	FADE_IN()
	PRINTLN("GOING TO STAGE - STAGE_GET_TO_HOTEL")
	MainStage = STAGE_GET_TO_HOTEL
ENDPROC

PROC GO_TO_CHECKPOINT_2()
	INT idx
	
	
	// If we're skipping to this stage load all that's needed or delete what's not.
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1266.1729, -214.0011, 41.4459>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 310.0126)
			
			PRINTLN("REPOSITIONING PLAYER FOR GO_TO_CHECKPOINT_2")
			
//			bDebugSkipping = TRUE
			
			CLEANUP_DEST_BLIPS()
			
			//limo pulling up to the hotel
			REQUEST_WAYPOINT_RECORDING("OJASva_101")
			REQUEST_WAYPOINT_RECORDING("OJASva_101a")
			REQUEST_WAYPOINT_RECORDING("OJASva_104")
			INITIALIZE_NEXT_STAGE()
			REMOVE_LIMO_GUARDS()
			REMOVE_TARGET_CAR()
			REMOVE_TARGET_AND_BLIP()
			REMOVE_BODYGUARDS()
			REMOVE_MOTORCADE_FOLLOW_CAR()
			REMOVE_MOTORCADE_PEDS()
			iCutsceneStage = 0
		
			bHotelTimerStarted = FALSE
			bHotelTimerExpired = FALSE
			bTaxiLeave = FALSE
			bDriveToLimo = FALSE
			bLeaveInLimo = FALSE
			bAlerted = FALSE
			bBufferHit = FALSE
			bPlayerUsedSniperRifle = FALSE
//			bDoNotRunPanicCheck = FALSE
//			bGaveSecondWarning = FALSE
			bPanickedExtras = FALSE
			bPlayedSearchDialogue = FALSE
			bPlayIntialDialogue = FALSE
			bAlertedSpecial = FALSE
			bPrepareLostCue = FALSE
			bTriggeredLastCue = FALSE
			
			iHandleSniperAttackStages = 0
			iGuardAimingState = 0
			iTargetPanicLine = 0
			
			fPanicTime = 0 
			
			bPanicking = FALSE
			bInPursuit = FALSE
			bTargetLeaving = FALSE
			
			TargetState = TS_ENTER_LIMO
			
			REPEAT NUM_LIMO_GUARDS idx
				LimoGuardState[idx] = LGS_WALK_TO_POSTS
			ENDREPEAT
			REPEAT NUM_ESCORT_PEDS idx
				EscortState[idx] = ES_WAITING
			ENDREPEAT
			REPEAT NUM_BODYGUARDS idx
				BodyGuardState[idx] = BS_WALK_WITH_TARGET
			ENDREPEAT
			
//			IF bSecondCheckpointSceneLoad
//				bSecondCheckpointSceneLoad = FALSE
//			ENDIF
			
			WHILE NOT ARE_MODELS_LOADED_FOR_GUARDS()
				WAIT(0)
			ENDWHILE
			
			CLEAR_AREA_OF_VEHICLES(<< -1220.57, -185.96, 38.40 >>, 50)	//area around where the limo pulls up
			CREATE_TARGET_CAR()
			CREATE_CUTSCENE_PEDS()
			
			WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASva_101")  //limo pulling up to the hotel lobby
			AND NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASva_101a")
			OR iExtraCreationStages < 2
				CREATE_EXTRAS()
			
				WAIT(0)
				PRINTLN("P-SKIPPING: WAITING FOR SCENE TO LOAD")
			ENDWHILE
			
			bDoingPSkip = FALSE
		ENDIF
	#ENDIF

	WHILE iExtraCreationStages < 2
		CREATE_EXTRAS()
	
		WAIT(0)
		PRINTLN("P-SKIPPING: WAITING FOR SCENE TO LOAD")
	ENDWHILE
	
	CREATE_TARGET_CAR()
	HANDLE_GUARD_CUTSCENE_SKIP(FALSE)
	
	IF NOT IS_BIT_SET(g_replay.iReplayBits, ENUM_TO_INT(RB_REPLAY_SETUP_STARTED))
		REMOVE_STREAMVOL_FOR_INTRO_CUTSCENE()
	ENDIF
	
	bPlayedTargetLine = FALSE
	bTargetKillLinePlayed = FALSE
	
//	HANDLE_REPLAY_WEAPON_EQUIP()

	CLEAR_AREA_OF_VEHICLES(<< -1220.57, -185.96, 38.40 >>, 50)
	
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	
	TRIGGER_MUSIC_EVENT("ASS1_RESTART1")
	PRINTLN("TRIGGERING MUSIC - ASS1_RESTART1")		
	
	WHILE NOT ARE_MODELS_LOADED_FOR_GUARDS()
		WAIT(0)
	ENDWHILE
	
	END_REPLAY_SETUP()
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	//fade in the game from the mission retry screen
	MainStage = STAGE_PLANT_BOMB
	PRINTLN("MainStage = STAGE_PLANT_BOMB")
	FADE_IN()
ENDPROC

PROC PLACE_TARGET_IN_CAR()
	IF DOES_ENTITY_EXIST(piTarget) AND DOES_ENTITY_EXIST(viTargetCar)
		IF NOT IS_ENTITY_DEAD(piTarget) AND IS_VEHICLE_DRIVEABLE(viTargetCar)
			SET_PED_INTO_VEHICLE(piTarget, viTargetCar, VS_BACK_LEFT)
			PRINTLN("PLACING TARGET INTO CAR")
		ENDIF
	ENDIF
ENDPROC

PROC PLACE_BODYGUARD_IN_CAR()
	IF DOES_ENTITY_EXIST(piBodyguard) AND DOES_ENTITY_EXIST(viTargetCar)
		IF NOT IS_ENTITY_DEAD(piBodyguard) AND IS_VEHICLE_DRIVEABLE(viTargetCar)
			SET_PED_INTO_VEHICLE(piBodyguard, viTargetCar, VS_BACK_RIGHT)
			PRINTLN("PLACING BODYGUARD INTO CAR")
		ENDIF
	ENDIF
ENDPROC

PROC PLACE_ESCORTS_IN_CAR()
	IF DOES_ENTITY_EXIST(piEscortPed[0]) AND DOES_ENTITY_EXIST(viEscortCar)
		IF NOT IS_ENTITY_DEAD(piEscortPed[0]) AND IS_VEHICLE_DRIVEABLE(viEscortCar)
			SET_PED_INTO_VEHICLE(piEscortPed[0], viEscortCar)
			PRINTLN("SETTING ESCORT PED 0 INTO VEHICLE")
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(piEscortPed[1]) AND DOES_ENTITY_EXIST(viEscortCar)
		IF NOT IS_ENTITY_DEAD(piEscortPed[1]) AND IS_VEHICLE_DRIVEABLE(viEscortCar)
			SET_PED_INTO_VEHICLE(piEscortPed[1], viEscortCar, VS_FRONT_RIGHT)
			PRINTLN("SETTING ESCORT PED 0 INTO VEHICLE")
		ENDIF
	ENDIF
ENDPROC

PROC PLACE_LIMO_GUARDS_IN_CAR()
	IF DOES_ENTITY_EXIST(piLimoGuard[0]) AND NOT IS_ENTITY_DEAD(piLimoGuard[0]) 
	AND DOES_ENTITY_EXIST(viTargetCar) AND IS_VEHICLE_DRIVEABLE(viTargetCar)
		SET_PED_INTO_VEHICLE(piLimoGuard[0], viTargetCar, VS_DRIVER)
		PRINTLN("SETTING piLimoGuard[0] INTO VEHICLE")
	ENDIF
	IF DOES_ENTITY_EXIST(piLimoGuard[1]) AND NOT IS_ENTITY_DEAD(piLimoGuard[1]) 
	AND DOES_ENTITY_EXIST(viTargetCar) AND IS_VEHICLE_DRIVEABLE(viTargetCar)
		SET_PED_INTO_VEHICLE(piLimoGuard[1], viTargetCar, VS_FRONT_RIGHT)
		PRINTLN("SETTING piLimoGuard[1] INTO VEHICLE")
	ENDIF
	IF DOES_ENTITY_EXIST(piLimoGuard[2]) AND NOT IS_ENTITY_DEAD(piLimoGuard[2]) 
	AND DOES_ENTITY_EXIST(viEscortCar) AND IS_VEHICLE_DRIVEABLE(viEscortCar)
		SET_PED_INTO_VEHICLE(piLimoGuard[2], viEscortCar, VS_BACK_RIGHT)
		PRINTLN("SETTING piLimoGuard[3] INTO VEHICLE")
	ENDIF
ENDPROC

//PROC GO_TO_CHECKPOINT_3()
//	INT idx
//
//	SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1262.3417, -245.4084, 41.4460>>)
//	SET_ENTITY_HEADING(PLAYER_PED_ID(), 295.7458)
//	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//	
//	// If we're skipping to this stage load all that's needed or delete what's not.
//	#IF IS_DEBUG_BUILD
//		IF bDoingPSkip OR bDoingJSkip
//		
//			bDebugSkipping = TRUE
//		
//			PRINTLN("INSIDE P-SKIP")
//			
//			CLEANUP_DEST_BLIPS()
//			
//			//limo pulling up to the hotel
//			REQUEST_WAYPOINT_RECORDING("OJASva_101")
//			REQUEST_WAYPOINT_RECORDING("OJASva_101a")
//			REQUEST_WAYPOINT_RECORDING("OJASva_104")
//			REQUEST_MODEL(FBI2)
//			
//			INITIALIZE_NEXT_STAGE()
//			REMOVE_LIMO_GUARDS()
//			REMOVE_TARGET_CAR()
//			REMOVE_TARGET_AND_BLIP()
//			REMOVE_BODYGUARDS()
//			REMOVE_MOTORCADE_FOLLOW_CAR()
//			REMOVE_MOTORCADE_PEDS()
//			CLEANUP_EXTRAS()
//			iCutsceneStage = 0
//
//			bHotelTimerExpired = TRUE
//			bAlerted = FALSE
//			bPlayerUsedSniperRifle = FALSE
//			
//			iHandleSniperAttackStages = 0
//		
//			IF bThirdCheckpointSceneLoad
//				bThirdCheckpointSceneLoad = FALSE
//			ENDIF
//		
//			CLEAR_AREA_OF_VEHICLES(<< -1220.57, -185.96, 38.40 >>, 50)	//area around where the limo pulls up
//			
//			WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASva_101")  //limo pulling up to the hotel lobby
//			AND NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASva_101a") 
//			OR NOT HAS_MODEL_LOADED(FBI2)
//				WAIT(0)
//				PRINTLN("P-SKIPPING: WAITING FOR SCENE TO LOAD")
//			ENDWHILE
//			
//			bDoingPSkip = FALSE
//		ENDIF
//	#ENDIF
//	
//	IF NOT bDebugSkipping
//		IF NOT bThirdCheckpointSceneLoad
//			IF IS_SCREEN_FADED_OUT()
//				NEW_LOAD_SCENE_START_SPHERE(<< -635.2563, -1212.3376, 11.4140 >>, 10.0)
//				bThirdCheckpointSceneLoad = TRUE
//				PRINTLN("ASSASSINATION HOTEL CHECKPOINT 3: CALLING NEW_LOAD_SCENE_START_SPHERE")
//			ELSE
//				PRINTLN("ASSASSINATION HOTEL CHECKPOINT 3: SCREEN IS NOT FADED OUT")
//			ENDIF
//		ELSE
//			PRINTLN("ASSASSINATION HOTEL CHECKPOINT 3: bFirstCheckpointSceneLoad IS TRUE")
//		ENDIF
//		
//		WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
//			WAIT(0)
//			PRINTLN("WAITING FOR SCENE TO LOAD - 3")
//		ENDWHILE
//		
//		PRINTLN("ASSASSINATION HOTEL: SCENE HAS LOADED")
//		NEW_LOAD_SCENE_STOP()
//	ENDIF
//	
//	// Target
//	CREATE_TARGET_CAR()
//	SET_ENTITY_COORDS(viTargetCar, vTargetCarLobbyPos)
//	SET_ENTITY_HEADING(viTargetCar, fTargetCarLobbyHead)
//
//	// Escort Peds
//	CREATE_MOTORCADE_ACTORS_AND_VEHICLE()
//	SET_ENTITY_COORDS(viEscortCar, vEscortCarPosition)
//	SET_ENTITY_HEADING(viEscortCar, fEscortCarHeading)
//	
//	// Limo Guards
//	CREATE_LIMO_GUARDS()
//	PLACE_LIMO_GUARDS_IN_CAR()
//	
//	bPanicking = FALSE
//	bAlerted = FALSE
//		
//	TargetState = TS_ENTER_LIMO
//	
//	REPEAT NUM_LIMO_GUARDS idx
//		LimoGuardState[idx] = LGS_LEAVE_IN_LIMO
//	ENDREPEAT
//	REPEAT NUM_ESCORT_PEDS idx
//		EscortState[idx] = ES_PROTECT
//	ENDREPEAT
//	REPEAT NUM_BODYGUARDS idx
//		BodyGuardState[idx] = BS_WALK_WITH_TARGET
//	ENDREPEAT
//	
//	bLeftHotel = TRUE
//	
////	HANDLE_REPLAY_WEAPON_EQUIP()
//	
//	//fade in the game from the mission retry screen
//	
//	MainStage = STAGE_TARGET_LEAVE_LOBBY
//	PRINTLN("MainStage = STAGE_TARGET_LEAVE_LOBBY")
//	FADE_IN()
//ENDPROC

PROC GO_TO_CHECKPOINT_4()
	
	// If we're skipping to this stage load all that's needed or delete what's not.
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1184.2440, -319.2575, 36.7445>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 26.0711)
			
			PRINTLN("REPOSITIONING PLAYER FOR GO_TO_CHECKPOINT_4")
			
//			bDebugSkipping = TRUE
		
			PRINTLN("INSIDE P-SKIP")
			
			CLEANUP_DEST_BLIPS()
			
			//limo pulling up to the hotel
			REQUEST_WAYPOINT_RECORDING("OJASva_101")
			REQUEST_WAYPOINT_RECORDING("OJASva_101a")
			REQUEST_WAYPOINT_RECORDING("OJASva_104")
			REQUEST_MODEL(FBI2)
			
//			INITIALIZE_NEXT_STAGE()
			REMOVE_LIMO_GUARDS()
			REMOVE_TARGET_CAR()
			REMOVE_TARGET_AND_BLIP()
			REMOVE_BODYGUARDS()
			REMOVE_MOTORCADE_FOLLOW_CAR()
			REMOVE_MOTORCADE_PEDS()
			iCutsceneStage = 0

			bHotelTimerExpired = TRUE
			bAlerted = FALSE
			bPlayerUsedSniperRifle = FALSE
			bBufferHit = FALSE
			
			iHandleSniperAttackStages = 0
		
			CLEAR_AREA_OF_VEHICLES(<< -1220.57, -185.96, 38.40 >>, 50)	//area around where the limo pulls up
			
			WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASva_101")  //limo pulling up to the hotel lobby
			AND NOT GET_IS_WAYPOINT_RECORDING_LOADED("OJASva_101a")
			OR NOT HAS_MODEL_LOADED(FBI2)
				WAIT(0)
				PRINTLN("P-SKIPPING: WAITING FOR SCENE TO LOAD")
			ENDWHILE
			
			bDoingPSkip = FALSE
		ENDIF
	#ENDIF
	
	TRIGGER_MUSIC_EVENT("ASS1_STOP")
	PRINTLN("TRIGGERING MUSIC - ASS1_STOP")	
	
	IF NOT IS_BIT_SET(g_replay.iReplayBits, ENUM_TO_INT(RB_REPLAY_SETUP_STARTED))
		REMOVE_STREAMVOL_FOR_INTRO_CUTSCENE()
	ENDIF
	
	//fade in the game from the mission retry screen
	
	END_REPLAY_SETUP()
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	MainStage = STAGE_MISSION_PASSED
	PRINTLN("MainStage = STAGE_MISSION_PASSED")
	FADE_IN()
ENDPROC

PROC CHECK_FOR_LESTER_DEATH()
	IF DOES_ENTITY_EXIST(piLester) 
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piLester, PLAYER_PED_ID())
//			IF IS_ENTITY_DEAD(pedLester) OR IS_PED_INJURED(pedLester)
				PRINTLN("FAILING - FT_LESTER_ATTACKED")
				SET_MISSION_FAILED(FT_LESTER_ATTACKED)
//			ELSE
//				PRINTLN("LESTER IS OKAY")
//			ENDIF
		ELSE
//			PRINTLN("PLAYER HAS NOT DAMAGED LESTER")
		ENDIF
	ELSE
		PRINTLN("LESTER DOES NOT EXIST")
	ENDIF
ENDPROC

PROC DELETE_LESTER()
	IF DOES_ENTITY_EXIST(piLester)
		DELETE_PED(piLester)
	ENDIF
ENDPROC

PROC UPDATE_LESTER()
	SEQUENCE_INDEX tempSeq
	
	IF DOES_ENTITY_EXIST(piLester)
		
		CHECK_FOR_LESTER_DEATH()
				
		IF NOT IS_PED_INJURED(piLester)
			FLOAT fLesterDistFromPlayer = GET_PLAYER_DISTANCE_FROM_ENTITY(piLester)
			
			SET_PED_CAN_PLAY_AMBIENT_ANIMS(piLester, FALSE)
			SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(piLester, FALSE)
			SET_PED_CAN_BE_TARGETTED(piLester, FALSE)
			SET_RAGDOLL_BLOCKING_FLAGS(piLester, RBF_PLAYER_IMPACT)
			SET_PED_CONFIG_FLAG(piLester, PCF_DisableExplosionReactions, TRUE)
			
			IF fLesterDistFromPlayer < 5
				IF IS_TIMER_STARTED(tLesterTimer)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF TIMER_DO_ONCE_WHEN_READY(tLesterTimer, 10)
							ADD_PED_FOR_DIALOGUE(hotelConv, 3, piLester, "LESTER")
							CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJAS_FOLLOW", CONV_PRIORITY_VERY_HIGH)
						ENDIF
					ENDIF
				ENDIF
			ELIF fLesterDistFromPlayer > 100
				IF IS_ENTITY_OCCLUDED(piLester)
				OR NOT IS_ENTITY_ON_SCREEN(piLester)
					DELETE_LESTER()
				ENDIF
			ENDIF
			
			SWITCH iLesterUpdateStages
				CASE 0
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF IS_PED_SHOOTING(PLAYER_PED_ID())
						OR ( DOES_ENTITY_EXIST(piLester) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piLester, PLAYER_PED_ID()) )
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(oFailSafeBench, PLAYER_PED_ID())
						
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(oFailSafeBench, PLAYER_PED_ID())
								PRINTLN("WE DAMAGED THE BENCH")
							ENDIF
						
							SET_PED_MOVEMENT_CLIPSET(piLester, "move_lester_CaneUp")
							
							TASK_PLAY_ANIM(piLester, "oddjobs@assassinate@hotel@leadin", "Lester_Getup", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
							
							PRINTLN("iLesterUpdateStages = 1")
							iLesterUpdateStages = 1
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF IS_ENTITY_PLAYING_ANIM(piLester, "oddjobs@assassinate@hotel@leadin", "Lester_Getup")
						IF GET_ENTITY_ANIM_CURRENT_TIME(piLester, "oddjobs@assassinate@hotel@leadin", "Lester_Getup") >= 0.376 

							OPEN_SEQUENCE_TASK(tempSeq)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1502.02856, -948.68701, 7.65061>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, 1.0, ENAV_NO_STOPPING)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1462.3389, -964.1780, 6.3394>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, 1.0, ENAV_NO_STOPPING)
								TASK_WANDER_STANDARD(NULL, GET_ENTITY_HEADING(piLester))
							CLOSE_SEQUENCE_TASK(tempSeq)
							TASK_PERFORM_SEQUENCE(piLester, tempSeq)
							CLEAR_SEQUENCE_TASK(tempSeq)
							FORCE_PED_MOTION_STATE(piLester, MS_ON_FOOT_WALK)
						
							PRINTLN("iLesterUpdateStages = 2")
							iLesterUpdateStages = 2
						ENDIF
					ENDIF
				BREAK
				CASE 2
				
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_SPOTTED_DIALOGUE_FROM_CLOSEST_GUARD()
	PED_INDEX pedToSpeak
	
	IF bBufferHit	
		pedToSpeak = GET_CLOSEST_GUARD()
		IF NOT IS_PED_INJURED(pedToSpeak)
			SETUP_PED_FOR_DIALOGUE(pedToSpeak, 3, "OJAvaGUARD")
			CREATE_CONVERSATION(hotelConv, "OJASAUD", "OJASva_SPOT", CONV_PRIORITY_VERY_HIGH)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Make sure that the player's last vehicle is set as a mission entity so that it wont get cleared up aggeressively by garbage collection while on the mission
///    Stores off the last 3 vehicles so that we can also use this function to detect if the player haws parked any cars inside the alley to try and break the escort car pulling up
PROC SAVE_LAST_PLAYER_VEHICLES()
	VEHICLE_INDEX vehLast
	
	IF MainStage > STAGE_GET_TO_HOTEL
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehLast = GET_LAST_DRIVEN_VEHICLE()
				
				IF DOES_ENTITY_EXIST(vehLast)
					IF IS_VEHICLE_DRIVEABLE(vehLast)
						IF vehLast != viLastDriven[0]
						AND vehLast != viLastDriven[1]
						AND vehLast != viLastDriven[2]
							
							//reset this if we've already stored off 3 vehicles
							IF iLastStoredVehicleIdx >= 2
								iLastStoredVehicleIdx = -1
							ENDIF
							
							viLastDriven[iLastStoredVehicleIdx + 1] = vehLast
							SET_ENTITY_AS_MISSION_ENTITY(viLastDriven[iLastStoredVehicleIdx + 1])
							SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(viLastDriven[iLastStoredVehicleIdx + 1], FALSE)
							PRINTLN("SETTING PLAYERS LAST VEHICLE_AS MISSION ENTITY")
							
							//increment this
							iLastStoredVehicleIdx += 1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_FIND_BENCH()
	IF NOT DOES_ENTITY_EXIST(oFailSafeBench)
		oFailSafeBench = GET_CLOSEST_OBJECT_OF_TYPE(<< -1510.1029, -947.7194, 8.2332 >>, 5, prop_bench_08)
		PRINTLN("FOUND BENCH")
	ENDIF
ENDPROC

// -----------------------------------
// MAIN SCRIPT
// -----------------------------------
PROC Update_Loop()
	
	//handles triggering of musical score change on target kill
	IF NOT bTriggerAlertMusic
		IF MainStage > STAGE_KILL_CAM
			TRIGGER_MUSIC_EVENT("ASS1_ALERT")
			PRINTLN("TRIGGERING MUSIC - ASS1_ALERT")
			bTriggerAlertMusic = TRUE
		ENDIF
	ENDIF
	
	//handles guard line if spotted and triggers musical score change
	IF NOT bTriggerAlertMessage
		IF bPanicking
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			PLAY_SPOTTED_DIALOGUE_FROM_CLOSEST_GUARD()
			
			bTriggerAlertMessage = TRUE
			
			IF NOT bTriggerAlertMusic
				TRIGGER_MUSIC_EVENT("ASS1_ALERT")
				PRINTLN("TRIGGERING MUSIC - ASS1_ALERT - 1")
				bTriggerAlertMusic = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	//handles the saving of vehicles driven by the player so that they don't get aggressively cleaned up while the mission is active (to fix bug 1505704 - 6/24/13 - MB)
	//also stores off the last 3 vehicles the player has driven to check if they've parked them in teh alley
	SAVE_LAST_PLAYER_VEHICLES()		
			
	
	SWITCH MainStage
		
		CASE STAGE_INTRO_CUTSCENE
			IF Is_Replay_In_Progress()
			
				MainStage = STAGE_INIT
				BREAK
			ENDIF
			
			HANDLE_FIND_BENCH()
			
			DO_STAGE_INTRO_CUTSCENE()
		BREAK
		
		CASE STAGE_INIT
			IF Is_Replay_In_Progress()
				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
					PRINTLN("REPLAY VEHICLE IS AVAILABLE - HOTEL")
					bReplayVehicleAvailable = TRUE
				ELSE
					PRINTLN("REPLAY VEHICLE IS NOT AVAILABLE")
				ENDIF
				bReplaying = TRUE
			ENDIF
			
			INITIALIZE_VARIABLES()
			SETUP_UI_LABELS()
			SETUP_MISSION_PARAMS()
			SET_LOW_DENSITY_HOTEL_TRAFFIC(TRUE)
			REQUEST_STREAMS()
			
			//don't do wanted checks in aggro en route to the hotel(to fix Bug 1202878 - 3/18/13 - MB)
			SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
			
			MainStage = STAGE_STREAMING
			PRINTLN("OJASHOTEL - MainStage = STAGE_STREAMING")
		BREAK
		
		CASE STAGE_STREAMING
			IF VEHICLES_RECORDINGS_AND_ANIMS_LOADED()
		
				MainStage = STAGE_SPAWNING			
				PRINTLN("OJASHOTEL - MainStage = STAGE_SPAWNING")
			ENDIF
		BREAK
	
		CASE STAGE_SPAWNING
			CREATE_CAMERAS()
			ADD_PARKED_CARS_TO_GARAGE()
			
			IF bReplaying
			
				IF g_savedGlobals.sAssassinData.fHotelMissionTime <> 0
					RESTART_TIMER_AT(tMissionTimer, g_savedGlobals.sAssassinData.fHotelMissionTime)
					PRINTLN("RESTARTING TIMER - tMissionTimer AT: ", g_savedGlobals.sAssassinData.fHotelMissionTime)	
				ENDIF
			
				iStageToUse = Get_Replay_Mid_Mission_Stage()
				PRINTLN("iStageToUse = ", iStageToUse)
				
				IF g_bShitskipAccepted
					IF iStageToUse <= 2
						iStageToUse = (iStageToUse + 1)
						PRINTLN("SHIT SKIP: iStageToUse = ", iStageToUse)
					ENDIF
				ENDIF
					
				//player has accepted package
				IF iStageToUse = 0
					GO_TO_CHECKPOINT_1()
				ELIF iStageToUse = 1
					GO_TO_CHECKPOINT_2()
				ELIF iStageToUse = 2
					GO_TO_CHECKPOINT_4()
				ENDIF
			ELSE
				IF NOT IS_TIMER_STARTED(tMissionTimer)
					START_TIMER_NOW(tMissionTimer)
					PRINTLN("STARTING TIMER - tMissionTimer")
					
					g_savedGlobals.sAssassinData.fHotelMissionTime = 0
					PRINTLN("HOTEL GLOBAL MISSION TIMER = ", g_savedGlobals.sAssassinData.fHotelMissionTime)
				ENDIF
			
				MainStage = STAGE_GET_TO_HOTEL
				PRINTLN("OJASHOTEL - MainStage = STAGE_GET_TO_HOTEL")
			ENDIF
		BREAK
		
		CASE STAGE_GET_TO_HOTEL
			UPDATE_LESTER()
			DO_STAGE_GET_TO_HOTEL()
		BREAK
		
		CASE STAGE_GUARD_CUTSCENE	
			DO_STAGE_GUARD_CUTSCENE()
		BREAK
		
		CASE STAGE_PLANT_BOMB
			UPDATE_LESTER()
			CHECK_FOR_PLAYER_BLOCKING_WITH_VEHICLES()
			DO_STAGE_PLANT_BOMB()
		BREAK

		CASE STAGE_GUARDS_GO_TO_POSTS
			CHECK_FOR_PLAYER_BLOCKING_WITH_VEHICLES()
			DO_STAGE_GUARDS_GO_TO_POSTS()
		BREAK
		
		CASE STAGE_TARGET_LEAVE_LOBBY
			DO_STAGE_TARGET_LEAVE_LOBBY()
			CHECK_FOR_TARGET_KILL()
		BREAK
		
		CASE STAGE_ENTER_LIMO		
			DO_STAGE_ENTER_LIMO()
			CHECK_FOR_TARGET_KILL()
		BREAK
			
		CASE STAGE_LEAVE_HOTEL
			DO_STAGE_LEAVE_HOTEL()
			CHECK_FOR_TARGET_KILL()
		BREAK

		CASE STAGE_KILL_CAM
			DO_STAGE_KILL_CAM()
		BREAK
		
		CASE STAGE_EVADE_POLICE
			DO_STAGE_EVADE_POLICE()
		BREAK
		
		CASE STAGE_LEAVE_AREA
			DO_STAGE_LEAVE_AREA()
		BREAK
		
		CASE STAGE_MISSION_PASSED
			DO_STAGE_MISSION_PASSED()
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_FADE_OUT_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC JUMP_TO_STAGE(Assassin_Stages stage, BOOL bIsDebugJump = FALSE)
	DO_FADE_OUT_WITH_WAIT()
	
	//Update mission checkpoint in case they skipped the stages where it gets set.
	IF bIsDebugJump
	
		SWITCH STAGE
			CASE STAGE_GET_TO_HOTEL
				GO_TO_CHECKPOINT_1()
			BREAK
			CASE STAGE_PLANT_BOMB
				GO_TO_CHECKPOINT_2()
			BREAK
			CASE STAGE_MISSION_PASSED
				GO_TO_CHECKPOINT_4()
			BREAK
		ENDSWITCH

	ENDIF
ENDPROC

// debug only
#IF IS_DEBUG_BUILD
	
	PROC INITIALIZE_JSKIP()
		RENDER_SCRIPT_CAMS(FALSE, TRUE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_HELP(TRUE)
	ENDPROC
	
 	// debug: skips
	PROC DO_DEBUG_SKIPS()
		Assassin_Stages eStage
	
		IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iDebugJumpStage)
			IF iDebugJumpStage = 0
				eStage = STAGE_GET_TO_HOTEL
				PRINTLN("Z MENU: eStage = STAGE_GET_TO_HOTEL")
			ELIF iDebugJumpStage = 1
				eStage = STAGE_PLANT_BOMB
				PRINTLN("Z MENU: eStage = STAGE_PLANT_BOMB")
			ENDIF
		
			bDoingPSkip = TRUE
			JUMP_TO_STAGE(eStage, TRUE)
		ENDIF
		
		// Debug Key: Check for Pass 
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			MainStage = STAGE_MISSION_PASSED
		ENDIF

		// Debug Key: Check for Fail 
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			SET_MISSION_FAILED(FT_ABANDONED)
		ENDIF
		
		// skip stage P
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P) 
			
			bDoingPSkip = TRUE
			
			IF MainStage > STAGE_SPAWNING AND MainStage < STAGE_MISSION_PASSED
				IF MainStage > STAGE_SPAWNING AND MainStage <= STAGE_GUARDS_GO_TO_POSTS
					eStage = STAGE_GET_TO_HOTEL
					PRINTLN("eStage = STAGE_GET_TO_HOTEL")
				ELIF MainStage > STAGE_GUARDS_GO_TO_POSTS AND MainStage < STAGE_MISSION_PASSED
					eStage = STAGE_PLANT_BOMB
					PRINTLN("eStage = STAGE_PLANT_BOMB")
				ENDIF
				
				//TODO add cleanup
				JUMP_TO_STAGE(eStage, TRUE)
			ENDIF
		ENDIF
		
		// skip stage J
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		
			bDoingJSkip = TRUE
			
			IF MainStage > STAGE_SPAWNING AND MainStage < STAGE_MISSION_PASSED
				IF MainStage = STAGE_GET_TO_HOTEL
					eStage = STAGE_PLANT_BOMB
					PRINTLN("J-SKIP: eStage = STAGE_PLANT_BOMB")
				ELIF MainStage >= STAGE_PLANT_BOMB
					eStage = STAGE_MISSION_PASSED
					PRINTLN("J-SKIP: eStage = STAGE_MISSION_PASSED")
				ENDIF
				
				//TODO add cleanup
				JUMP_TO_STAGE(eStage, TRUE)
			ENDIF		
		ENDIF
	ENDPROC
#ENDIF

//PROC CHECK_FOR_ACTION_MODE()
//	IF NOT bSetActionMode
//		IF bPanicking
//			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
//				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
//			ENDIF
//			PRINTLN("SETTING ACTION MODE ON THE PLAYER")
//			bSetActionMode = TRUE
//		ENDIF
//	ENDIF
//ENDPROC

//FUNC BOOL IS_PLAYER_ARMED()
//	WEAPON_TYPE wtCurrentWeapon
//
//	IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtCurrentWeapon)
//		IF wtCurrentWeapon = WEAPONTYPE_UNARMED
//		OR wtCurrentWeapon = WEAPONTYPE_INVALID
//			PRINTLN("PLAYER IS UNARMED")
//			RETURN FALSE
//		ELSE
//			PRINTLN("PLAYER IS ARMED")
//		ENDIF
//	ELSE
//		PRINTLN("PLAYER HAS NOTHING")
//	ENDIF
//	
//	RETURN TRUE
//ENDFUNC

// Initialisation and the script loop
SCRIPT
	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		DEBUG_MESSAGE("FORCE CLEANUP HAS OCCURRED!!!")
		
		TRIGGER_MUSIC_EVENT("ASS1_FAIL")
		PRINTLN("TRIGGERING MUSIC - ASS1_FAIL - 02")
		
		SET_BITMASK_AS_ENUM(g_savedGlobals.sAssassinData.iGenericData, ACD_FAILED)
		Mission_Flow_Mission_Force_Cleanup()
		Script_Cleanup()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)	
	Player = PLAYER_PED_ID()
	
	
	//to fix bug 1549652 - Clear player wanted level
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF
	
	
	IF Is_Replay_In_Progress()
		IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
			PRINTLN("REPLAY VEHICLE IS AVAILABLE - CONSTRUCTION")
			bReplayVehicleAvailable = TRUE
		ELSE
			PRINTLN("REPLAY VEHICLE IS NOT AVAILABLE - CONSTRUCTION")
		ENDIF
		bReplaying = TRUE
	ELSE
		bReplaying = FALSE
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		AddWidgets(myDebugData)
	#ENDIF
	
	SETUP_DEBUG()
	
	// Fix Bug # 712684
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	ENDIF
	
	ASSASSINATION_ClearUnneededGenericData()
	
	iCurAssassinRank = ENUM_TO_INT(ASSASSINATION_Hotel) //g_savedGlobals.sAssassinData.iCurrentAssassinRank
	missionData = ASSASSINATION_GetMissionData(iCurAssassinRank)	
	PRINTLN("Current rank is... ", iCurAssassinRank)
	
	
	SET_PED_MODEL_IS_SUPPRESSED(GuardModel, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(GuardModel_01, TRUE)
	
	PRINTLN("ya it worked.. ", iCurAssassinRank)
	
	INVALIDATE_IDLE_CAM()
	
	//HANDLE STREAMING AROUND REPLAY START POSITION
	IF IS_REPLAY_IN_PROGRESS()
	
		bHaveRetryOnce = TRUE
		PRINTLN("HOTEL: SETTING -  bHaveRetryOnce = TRUE")
		
		iStageToUse = Get_Replay_Mid_Mission_Stage()
		PRINTLN("iStageToUse = ", iStageToUse)
		
		IF g_bShitskipAccepted
			IF iStageToUse <= 1
				iStageToUse = (iStageToUse + 1)
				PRINTLN("SHIT SKIP: iStageToUse = ", iStageToUse)
			ENDIF
		ENDIF
		
		// Due to url:bugstar:2105614, this needs to be called before START_REPLAY_SETUP
		REMOVE_STREAMVOL_FOR_INTRO_CUTSCENE()
		
		IF iStageToUse = 0
			IF bReplayVehicleAvailable
				REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
				WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
					PRINTLN("CONSTRUCTION: Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED - 01")
					WAIT(0)
				ENDWHILE
			
				//create the replay vehicle but make sure it isnt a water based vehicle (sso we dont spawn it on land)
				VEHICLE_INDEX vehPlayer
				vehPlayer = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-1523.1742, -924.6732, 9.1221>>, 53.0177)
				IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_BIG_VEHICLE(vehPlayer)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
				ELSE
					DELETE_VEHICLE(vehPlayer)
				ENDIF
			ENDIF
			
			START_REPLAY_SETUP(<< -1510.1810, -946.9595, 8.2738 >>, 10.0)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 1
			IF bReplayVehicleAvailable
				REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
				WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
					PRINTLN("CONSTRUCTION: Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED - 02")
					WAIT(0)
				ENDWHILE
			
				//create the replay vehicle but make sure it isnt a water based vehicle (sso we dont spawn it on land)
				VEHICLE_INDEX vehPlayer
				vehPlayer = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-1266.4049, -219.1991, 41.4459>>, 304.8644)
				IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_BIG_VEHICLE(vehPlayer)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
				ELSE
					DELETE_VEHICLE(vehPlayer)
				ENDIF
			ENDIF

			START_REPLAY_SETUP(<<-1266.1729, -214.0011, 41.4459>>, 310.0126)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 2
			IF bReplayVehicleAvailable
				REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
				WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
					PRINTLN("CONSTRUCTION: Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED - 02")
					WAIT(0)
				ENDWHILE
			
				//create the replay vehicle but make sure it isnt a water based vehicle (sso we dont spawn it on land)
				VEHICLE_INDEX vehPlayer
				vehPlayer = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-1188.4861, -316.9884, 36.6841>>, 29.9702)
				IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_BIG_VEHICLE(vehPlayer)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
				ELSE
					DELETE_VEHICLE(vehPlayer)
				ENDIF
			ENDIF
			
			START_REPLAY_SETUP(<<-1184.2440, -319.2575, 36.7445>>, 26.0711)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ENDIF
	ENDIF
	
	// The script loop
	WHILE (TRUE)
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ASS1")
	
		IF bMissionFailed
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				Script_Failed()
			ENDIF

		ELIF NOT IS_PED_INJURED(Player)
			
			UPDATE_EXTRAS()
			
			IF MainStage < STAGE_GUARDS_GO_TO_POSTS
				aggroArgs.iShotIRange = 1
			ELSE
				aggroArgs.iShotIRange = 5
			ENDIF
			
			
			//main update loop for script stages
			Update_Loop()
			

			UPDATE_PED_STATES()

			IF bAlerted
				HANDLE_SNIPER_ATTACK_BUFFER()
			ENDIF

			//update the on screen timer representation
			IF NOT bHotelTimerExpired
				IF MainStage >= STAGE_PLANT_BOMB
				AND MainStage < STAGE_TARGET_LEAVE_LOBBY
					UPDATE_HOTEL_TIMER(iHotelTimerStart, fBombTime, bHotelTimerStarted, bHotelTimerExpired)
				ENDIF
			ENDIF
			
			//handle circle/hint cam cuts throughout the mission
			UPDATE_HINT_CAMERAS()
			
			//MISSION FAIL CHECKS
			FAIL_IF_TARGET_ESCAPES()
			
			//fail mission if player triggers panic prior to the target leaving the hotel
			IF ( bPanicking OR bFailMissionForCloseVehicle ) AND MainStage < STAGE_TARGET_LEAVE_LOBBY
			OR (bPanickedExtras AND MainStage < STAGE_TARGET_LEAVE_LOBBY)
				IF NOT IS_TIMER_STARTED(tFailTimer)
				AND MainStage > STAGE_GUARD_CUTSCENE
					START_TIMER_NOW(tFailTimer)
					PRINTLN("STARTING TIMER - tFailTimer")
				ELSE
					IF MainStage < STAGE_GUARD_CUTSCENE			//to fix bug 1207388 - need to immediately fail in this case - 3/21/13 MB)
					OR GET_TIMER_IN_SECONDS(tFailTimer) > 5.0
						IF failType = FT_VEHICLE
							SET_MISSION_FAILED(FT_VEHICLE)
						ELIF failType = FT_DISREGARDED_WARNINGS
							SET_MISSION_FAILED(FT_DISREGARDED_WARNINGS)
						ELSE
							SET_MISSION_FAILED(FT_BLOWN_COVER)
							PRINTLN("OJASHOTEL - FAILING SCRIPT BECAUSE THE PLAYER HAS BLOWN THEIR COVER EARLY")
						ENDIF
					ELIF GET_TIMER_IN_SECONDS(tFailTimer) > 2.5
						IF NOT bTaskedGuyToAim
							
							IF NOT bPlayerParkedCarInAlley
								piWalkingGuard = GET_CLOSEST_GUARD(70.0, FALSE)
							ELSE
								piWalkingGuard = GET_CLOSEST_GUARD(70.0)
							ENDIF
						
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF NOT IS_PED_INJURED(piWalkingGuard)
									CLEAR_PED_TASKS(piWalkingGuard)
								
									IF failType = FT_VEHICLE
										IF DOES_ENTITY_EXIST(viCarThatTriggeredAlert)
											IF NOT IS_PED_IN_ANY_VEHICLE(piWalkingGuard)
												TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(piWalkingGuard, viCarThatTriggeredAlert, viCarThatTriggeredAlert, PEDMOVEBLENDRATIO_RUN, FALSE)
											ENDIF
										ENDIF
	//									PED_INDEX pedClosest = GET_CLOSEST_GUARD(50.0)
										IF DOES_ENTITY_EXIST(piWalkingGuard) AND NOT IS_PED_INJURED(piWalkingGuard)
											SETUP_PED_FOR_DIALOGUE(piWalkingGuard, 6, "OJAvaGUARD2")
											PLAY_SINGLE_LINE_FROM_CONVERSATION (hotelConv, "OJASAUD", "OJASva_CAR2", "OJASva_CAR2_1", CONV_PRIORITY_VERY_HIGH)
											PRINTLN("PLAYING CONVO - OJASva_CAR2")
										ELSE
											PRINTLN("WE COULD NOT FIND PED WITH CALL - GET_CLOSEST_GUARD")
										ENDIF
										PRINTLN("FAIL REASON: failType = FT_VEHICLE")
									ELIF failType = FT_STICKY
										IF DOES_ENTITY_EXIST(viTargetCar)
										AND IS_VEHICLE_DRIVEABLE(viTargetCar)
											TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(piWalkingGuard, viTargetCar, viTargetCar, PEDMOVEBLENDRATIO_RUN, FALSE)
										ENDIF
										SETUP_PED_FOR_DIALOGUE(GET_CLOSEST_GUARD_TO_LIMO(25.0), 6, "OJAvaGUARD2")
										PLAY_SINGLE_LINE_FROM_CONVERSATION (hotelConv, "OJASAUD", "OJASva_CAR3", "OJASva_CAR3_1", CONV_PRIORITY_VERY_HIGH)
										
										PRINTLN("FAIL REASON: failType = FT_STICKY")
									ELSE
										TASK_AIM_GUN_AT_ENTITY(piWalkingGuard, PLAYER_PED_ID(), -1)
										PRINTLN("TASKING CLOSEST LIMO GUARD TO AIM AT PLAYER")
										PLAY_GUARD_PANIC_LINE(piWalkingGuard)
									ENDIF
								ENDIF
							ENDIF
									
							bTaskedGuyToAim = TRUE

						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//handles player trying to mess with the limo with a tow truck vehicle at any point in the mission
			DETACH_TOW_TRUCK_FROM_LIMO_IF_ATTACHED()
			
//			IF IS_STICKYBOMB_VISIBLE_ON_PARKED_LIMO(failType)
//				SCRIPT_ASSERT("BOMB NOT SAFE!")
//			ENDIF
			
			//DEBUG ONLY
			#IF IS_DEBUG_BUILD
				DO_DEBUG_SKIPS()
   			#ENDIF
		ENDIF
		WAIT(0)
		
	ENDWHILE
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


