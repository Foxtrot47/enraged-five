//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     : Assassin_Shared.sch                                		    //
//      AUTHOR          : Mike Bagley		                                            //
//      DESCRIPTION     : Shared functions for assassinations							//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////


USING "minigame_stats_tracker_helpers.sch"
USING "oddjob_aggro.sch"
USING "script_player.sch"
USING "cutscene_public.sch"
USING "script_oddjob_funcs.sch"
USING "timer_public.sch"
USING "shop_public.sch"
USING "cheat_controller_public.sch"

// ********************************** Script Globals *************************************
AGGRO_ARGS aggroArgs
CUTSCENE_ARGS cutArgs
EAggro aggroReason
FLOAT fTimeTaken
FLOAT fBaseReward
FLOAT fBonusReward
BOOL bBonus
BOOL bHaveRetryOnce = FALSE
BOOL bResultsCreated
BOOL bGrabbedDistance = FALSE
FLOAT fStoredDistance
//BOOL bWarningHelp

//CAMERA_INDEX camKill
CAMERA_INDEX camHint
INT iKillCamStage
INT iCutsceneStartTime
INT iMissionPercentage = 100 // Default to 100
INT iMedal = 3 // Default to Gold

structTimer assFailTimer

END_SCREEN_DATASET assassinationEndScreen

//END RESULTS SCREEN STUFF
STRUCT END_RESULTS_LABELS
	TEXT_LABEL_23 tlMissionPassed
	TEXT_LABEL_23 tlMissionName
//	TEXT_LABEL_23 tlTargetTime
	TEXT_LABEL_23 tlTimeTaken
	TEXT_LABEL_23 tlBaseReward
	TEXT_LABEL_23 tlBonusDesc
	TEXT_LABEL_23 tlCashEarned
	TEXT_LABEL_23 tlCompletionGold
	TEXT_LABEL_23 tlCompletionSilver
	TEXT_LABEL_23 tlCompletionBronze
	TEXT_LABEL_23 tlCompletionNoMedal
	TEXT_LABEL_23 tlContinue
ENDSTRUCT
END_RESULTS_LABELS endLabel

ENUM PASS_REASON_ENUM
	PASS_STANDARD = 0,
	PASS_BONUS
ENDENUM
// ********************************** ************** *************************************

// ********************************** DEBUG ONLY FUNCTIONS *************************************
#IF IS_DEBUG_BUILD
	STRUCT DEBUG_POS_DATA
		BOOL bTurnOnDebugPos
		VECTOR vDebugVector
		FLOAT fDebugFloat
		FLOAT fDebugX
		FLOAT fDebugY
		FLOAT fdebugWidth
		FLOAT fdebugHeight
		FLOAT fRotateX
		FLOAT fRotateY
		FLOAT fRotateZ
		FLOAT fOffsetX
		FLOAT fOffsetY
		FLOAT fOffsetZ
		FLOAT fFOV
		ENTITY_INDEX myObject
		VECTOR myObjectPos
	ENDSTRUCT

	PROC AddWidgets(DEBUG_POS_DATA& myData)
//		UNUSED_PARAMETER(myData)
		START_WIDGET_GROUP("Assassin")
			ADD_WIDGET_BOOL("turn on positioning", myData.bTurnOnDebugPos)
			ADD_WIDGET_VECTOR_SLIDER("DebugPosition", myData.vDebugVector, -2000, 2000, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("DebugRotation", myData.fDebugFloat, -360, 360, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("DebugX", myData.fDebugX, 0, 1, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("DebugY", myData.fDebugY, 0, 1, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("DebugWidth", myData.fdebugWidth, 0, 2, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("DebugHeight", myData.fdebugHeight, 0, 2, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("RotateX", myData.fRotateX, 0, 360, 1)
			ADD_WIDGET_FLOAT_SLIDER("RotateY", myData.fRotateY, 0, 360, 1)
			ADD_WIDGET_FLOAT_SLIDER("RotateZ", myData.fRotateZ, 0, 360, 1)
			ADD_WIDGET_FLOAT_SLIDER("OffsetX", myData.fOffsetX, -2000, 2000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("OffsetY", myData.fOffsetY, -2000, 2000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("OffsetZ", myData.fOffsetZ, -2000, 2000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("FOV", myData.fFOV, 0, 125, 0.1)
		STOP_WIDGET_GROUP()
	ENDPROC
#ENDIF
// ********************************** ************** *************************************
PROC GIVE_PLAYER_WEAPONS(WEAPON_TYPE wtWeapon, INT iDesiredAmmoCount)
	INT iAmmoCount

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), wtWeapon)
			PRINTLN("PLAYER HAS WEAPON ALREADY")
			iAmmoCount = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtWeapon)
			PRINTLN("NUMBER OF AMMO 01 = ", iAmmoCount)
			IF iAmmoCount < iDesiredAmmoCount
				SET_PED_AMMO(PLAYER_PED_ID(), wtWeapon, iDesiredAmmoCount)
				PRINTLN("GIVING PLAYER WEAPON WITH AMMO")
			ENDIF
		ELSE
			iAmmoCount = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtWeapon)
			PRINTLN("NUMBER OF AMMO 02 = ", iAmmoCount)
			
			IF iAmmoCount < iDesiredAmmoCount
				SET_PED_AMMO(PLAYER_PED_ID(), wtWeapon, iDesiredAmmoCount)
			ENDIF
			
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), wtWeapon, 0, FALSE, FALSE)
			
			PRINTLN("GIVING PLAYER WEAPON")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_READY_FOR_ASSASSINATION_CUTSCENE(FLOAT fVehicleStopDist = 5.0, BOOL bUseExraSecondDelay = TRUE)
	VEHICLE_INDEX vehPlayer
	
	IF ( IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 
		fVehicleStopDist, 1) AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID()))
	OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_TIMER_STARTED(cutArgs.cutSceneTimer)
			RESTART_TIMER_NOW(cutArgs.cutSceneTimer)
			IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())	//handle instance of player trying to ghost ride his vehicle into the scene
				vehPlayer = GET_PLAYERS_LAST_VEHICLE()
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					SET_VEHICLE_FORWARD_SPEED(vehPlayer, 0)
				ENDIF
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ELSE
			//delay intro cutscene for a second so that it isnt such an abrupt transition from gameplay to cutscene
			IF GET_TIMER_IN_SECONDS(cutArgs.cutSceneTimer) > 1
			OR bUseExraSecondDelay = FALSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_READY_FOR_ASSASSINATION_CUTSCENE_WASHER()
	VEHICLE_INDEX vehPlayer
	
	IF ( IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID()))
	OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_TIMER_STARTED(cutArgs.cutSceneTimer)
			RESTART_TIMER_NOW(cutArgs.cutSceneTimer)
			IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())	//handle instance of player trying to ghost ride his vehicle into the scene
				vehPlayer = GET_PLAYERS_LAST_VEHICLE()
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					SET_VEHICLE_FORWARD_SPEED(vehPlayer, 0)
				ENDIF
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ELSE
			//delay intro cutscene for a second so that it isnt such an abrupt transition from gameplay to cutscene
			IF GET_TIMER_IN_SECONDS(cutArgs.cutSceneTimer) > 1
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//removes a specific blip and prints a specified message
PROC REMOVE_BLIP_AND_PRINT_OBJECTIVE_MSG_IF_WANTED(BLIP_INDEX blipToRemove, STRING msgToDisplay)
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		IF DOES_BLIP_EXIST(blipToRemove)
			REMOVE_BLIP(blipToRemove)
			CLEAR_PRINTS()
			PRINT_NOW(msgToDisplay, DEFAULT_GOD_TEXT_TIME, 1)
		ENDIF
	ENDIF
ENDPROC


PROC SET_PLAYER_WANTED_LEVEL_NO_DROP_NOW(INT playerWantedLevel = 1, FLOAT fDifficulty = 0.1, FLOAT fWantedMultiplier = 0.3)
	SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), playerWantedLevel)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), fDifficulty)
	SET_WANTED_LEVEL_MULTIPLIER(fWantedMultiplier)
ENDPROC


FUNC BOOL HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(BOOL& bBoolToFlipWhenPlayed, structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                enumSubtitlesState ShouldDisplaySubtitles = DO_NOT_DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN, BOOL cloneConversation = FALSE)
	IF NOT bBoolToFlipWhenPlayed
		IF CREATE_CONVERSATION(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen, cloneConversation)
			PRINTLN("FRANKLIN PLAYING KILL LINE")
			bBoolToFlipWhenPlayed = TRUE
			
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC 

PROC UPDATE_STATS()

	// -------------------------------------------HOTEL-------------------------------------------
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Assassin_Valet")) = 1)
	
		INFORM_MISSION_STATS_OF_INCREMENT(ASS1_MIRROR_TIME, FLOOR(fTimeTaken), TRUE)
		PRINTLN("HOTEL STATS: TIME - ", FLOOR(fTimeTaken))
		
		IF bBonus
			INFORM_MISSION_STATS_OF_INCREMENT(ASS1_MIRROR_SNIPER_USED, 1, TRUE)
			PRINTLN("HOTEL STATS: BONUS IS TRUE")
		ELSE
			INFORM_MISSION_STATS_OF_INCREMENT(ASS1_MIRROR_SNIPER_USED, 0, TRUE)
			PRINTLN("HOTEL STATS: BONUS IS FALSE")
		ENDIF
		
		IF bBonus
			INFORM_MISSION_STATS_OF_INCREMENT(ASS1_MIRROR_CASH, ROUND(fBonusReward + fBaseReward), TRUE)
			PRINTLN("HOTEL STATS: MONEY - ", ROUND(fBonusReward + fBaseReward))
		ELSE
			INFORM_MISSION_STATS_OF_INCREMENT(ASS1_MIRROR_CASH, ROUND(fBaseReward), TRUE)
			PRINTLN("HOTEL STATS: MONEY - ", ROUND(fBaseReward))
		ENDIF
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS1_MIRROR_PERCENT, iMissionPercentage, TRUE)
		PRINTLN("HOTEL STATS: PERCENTAGE - ", iMissionPercentage)
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS1_MIRROR_MEDAL, iMedal, TRUE)
		PRINTLN("HOTEL STATS: MEDAL - ", iMedal)
	
	// -------------------------------------------MULTI-------------------------------------------
	ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Assassin_Multi")) = 1)
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS2_MIRROR_TIME, FLOOR(fTimeTaken), TRUE)
		PRINTLN("MULTI STATS: TIME - ", FLOOR(fTimeTaken))
		
		IF bBonus
			INFORM_MISSION_STATS_OF_INCREMENT(ASS2_MIRROR_QUICKBOOL, 1, TRUE)
			PRINTLN("MULTI STATS: BONUS IS TRUE")
		ELSE
			INFORM_MISSION_STATS_OF_INCREMENT(ASS2_MIRROR_QUICKBOOL, 0, TRUE)
			PRINTLN("MULTI STATS: BONUS IS FALSE")
		ENDIF
		
		IF bBonus
			INFORM_MISSION_STATS_OF_INCREMENT(ASS2_MIRROR_CASH, ROUND(fBonusReward + fBaseReward), TRUE)
			PRINTLN("MULTI STATS: MONEY - ", ROUND(fBonusReward + fBaseReward))
		ELSE
			INFORM_MISSION_STATS_OF_INCREMENT(ASS2_MIRROR_CASH, ROUND(fBaseReward), TRUE)
			PRINTLN("MULTI STATS: MONEY - ", ROUND(fBaseReward))
		ENDIF
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS2_MIRROR_PERCENT, iMissionPercentage, TRUE)
		PRINTLN("MULTI STATS: PERCENTAGE - ", iMissionPercentage)
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS2_MIRROR_MEDAL, iMedal, TRUE)
		PRINTLN("MULTI STATS: MEDAL - ", iMedal)
	
	// -------------------------------------------VICE-------------------------------------------
	ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Assassin_Hooker")) = 1)
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS3_MIRROR_TIME, FLOOR(fTimeTaken), TRUE)
		PRINTLN("VICE STATS: TIME - ", FLOOR(fTimeTaken))
		
		IF bBonus
			INFORM_MISSION_STATS_OF_INCREMENT(ASS3_MIRROR_CLEAN_ESCAPE, 1, TRUE)
			PRINTLN("VICE STATS: BONUS IS TRUE")
		ELSE
			INFORM_MISSION_STATS_OF_INCREMENT(ASS3_MIRROR_CLEAN_ESCAPE, 0, TRUE)
			PRINTLN("VICE STATS: BONUS IS FALSE")
		ENDIF
		
		IF bBonus
			INFORM_MISSION_STATS_OF_INCREMENT(ASS3_MIRROR_CASH, ROUND(fBonusReward + fBaseReward), TRUE)
			PRINTLN("VICE STATS: MONEY - ", ROUND(fBonusReward + fBaseReward))
		ELSE
			INFORM_MISSION_STATS_OF_INCREMENT(ASS3_MIRROR_CASH, ROUND(fBaseReward), TRUE)
			PRINTLN("VICE STATS: MONEY - ", ROUND(fBaseReward))
		ENDIF
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS3_MIRROR_PERCENTAGE, iMissionPercentage, TRUE)
		PRINTLN("VICE STATS: PERCENTAGE - ", iMissionPercentage)
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS3_MIRROR_MEDAL, iMedal, TRUE)
		PRINTLN("VICE STATS: MEDAL - ", iMedal)
	
	// -------------------------------------------BUS-------------------------------------------
	ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Assassin_Bus")) = 1)
	
		INFORM_MISSION_STATS_OF_INCREMENT(ASS4_MIRROR_TIME, FLOOR(fTimeTaken), TRUE)
		PRINTLN("BUS STATS: TIME - ", FLOOR(fTimeTaken))
		
		IF bBonus
			INFORM_MISSION_STATS_OF_INCREMENT(ASS4_MIRROR_HIT_AND_RUN, 1, TRUE)
			PRINTLN("BUS STATS: BONUS IS TRUE")
		ELSE
			INFORM_MISSION_STATS_OF_INCREMENT(ASS4_MIRROR_HIT_AND_RUN, 0, TRUE)
			PRINTLN("BUS STATS: BONUS IS FALSE")
		ENDIF
		
		IF bBonus
			INFORM_MISSION_STATS_OF_INCREMENT(ASS4_MIRROR_CASH, ROUND(fBonusReward + fBaseReward), TRUE)
			PRINTLN("BUS STATS: MONEY - ", ROUND(fBonusReward + fBaseReward))
		ELSE
			INFORM_MISSION_STATS_OF_INCREMENT(ASS4_MIRROR_CASH, ROUND(fBaseReward), TRUE)
			PRINTLN("BUS STATS: MONEY - ", ROUND(fBaseReward))
		ENDIF
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS4_MIRROR_PERCENTAGE, iMissionPercentage, TRUE)
		PRINTLN("BUS STATS: PERCENTAGE - ", iMissionPercentage)
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS4_MIRROR_MEDAL, iMedal, TRUE)
		PRINTLN("BUS STATS: MEDAL - ", iMedal)
	
	// -------------------------------------------CONTSTRUCTION-------------------------------------------
	ELIF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Assassin_Construction")) = 1)
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS5_MIRROR_TIME, FLOOR(fTimeTaken), TRUE)
		PRINTLN("CONTSTRUCTION STATS: TIME - ", FLOOR(fTimeTaken))
		
		IF bBonus
			INFORM_MISSION_STATS_OF_INCREMENT(ASS5_MIRROR_NO_FLY, 1, TRUE)
			PRINTLN("CONTSTRUCTION STATS: BONUS IS TRUE")
		ELSE
			INFORM_MISSION_STATS_OF_INCREMENT(ASS5_MIRROR_NO_FLY, 0, TRUE)
			PRINTLN("CONTSTRUCTION STATS: BONUS IS FALSE")
		ENDIF
		
		IF bBonus
			INFORM_MISSION_STATS_OF_INCREMENT(ASS5_MIRROR_CASH, ROUND(fBonusReward + fBaseReward), TRUE)
			PRINTLN("CONTSTRUCTION STATS: MONEY - ", ROUND(fBonusReward + fBaseReward))
		ELSE
			INFORM_MISSION_STATS_OF_INCREMENT(ASS5_MIRROR_CASH, ROUND(fBaseReward), TRUE)
			PRINTLN("CONTSTRUCTION STATS: MONEY - ", ROUND(fBaseReward))
		ENDIF
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS5_MIRROR_PERCENT, iMissionPercentage, TRUE)
		PRINTLN("CONTSTRUCTION STATS: PERCENTAGE - ", iMissionPercentage)
		
		INFORM_MISSION_STATS_OF_INCREMENT(ASS5_MIRROR_MEDAL, iMedal, TRUE)
		PRINTLN("CONTSTRUCTION STATS: MEDAL - ", iMedal)
	ELSE
		SCRIPT_ASSERT("We're not running any Assassination script, but we're trying to update stats!")
	ENDIF
ENDPROC

PROC SETUP_RESULTS_UI()
	IF NOT bResultsCreated

		SET_ENDSCREEN_DATASET_HEADER(assassinationEndScreen, endLabel.tlMissionPassed, g_sMissionStatsName)
		
		fTimeTaken = fTimeTaken * 1000
		
		IF g_bShitskipAccepted	//Invalidate stats if shit-skipped the mission
			ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(assassinationEndScreen, ESEF_RAW_STRING, endLabel.tlTimeTaken, "MTPHPERSKI", FLOOR(fTimeTaken), 0, ESCM_INVALIDATED)
			ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(assassinationEndScreen, ESEF_RAW_STRING, endLabel.tlBonusDesc, "MTPHPERSKI", 0, 0, ESCM_INVALIDATED)
			ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(assassinationEndScreen, ESEF_RAW_STRING, endLabel.tlCashEarned, "MTPHPERSKI", ROUND(fBonusReward + fBaseReward), 0, ESCM_INVALIDATED)
			
			//Reset shitskipped bool
			ResetShitskipVariables()
		ELSE
			IF bHaveRetryOnce
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(assassinationEndScreen, ESEF_RAW_STRING, endLabel.tlTimeTaken, "MTPHPERRET", FLOOR(fTimeTaken), 0, ESCM_INVALIDATED, FALSE)
				PRINTLN("bHaveRetryOnce IS TRUE, INVALIDATING TIME STAT")
			ELSE
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(assassinationEndScreen, ESEF_TIME_M_S, endLabel.tlTimeTaken, "", FLOOR(fTimeTaken), 0, ESCM_NO_MARK, FALSE)
				PRINTLN("bHaveRetryOnce IS FALSE, VALIDATING TIME STAT")
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(endLabel.tlBaseReward)
	//			ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(assassinationEndScreen, ESEF_DOLLAR_VALUE, endLabel.tlBaseReward, "", ROUND(fBaseReward), 0, ESCM_NO_MARK, FALSE)

				IF bBonus
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(assassinationEndScreen, ESEF_RAW_STRING, endLabel.tlBonusDesc, "", 0, 0, ESCM_CHECKED, FALSE)
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(assassinationEndScreen, ESEF_DOLLAR_VALUE, endLabel.tlCashEarned, "", ROUND(fBonusReward + fBaseReward), 0, ESCM_CHECKED, FALSE)
				ELSE
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(assassinationEndScreen, ESEF_RAW_STRING, endLabel.tlBonusDesc, "", 0, 0, ESCM_UNCHECKED, FALSE)
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(assassinationEndScreen, ESEF_DOLLAR_VALUE, endLabel.tlCashEarned, "", ROUND(fBaseReward), 0, ESCM_NO_MARK, FALSE)
				ENDIF
			ELSE
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(assassinationEndScreen, ESEF_DOLLAR_VALUE, endLabel.tlCashEarned, "", ROUND(fBaseReward), 0, ESCM_NO_MARK, FALSE)
			ENDIF
		ENDIF
		
		BOOL bShitskiped = FALSE
		IF g_bShitskipAccepted
			INT i
			REPEAT g_iMissionStatsBeingTracked i	
				IF ENUM_TO_INT(g_MissionStatTrackingArray[i].target) >= 0
				AND NOT g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].bHidden
					IF g_MissionStatTrackingArray[i].invalidationReason = MSSIR_SKIP
						CPRINTLN(DEBUG_MISSION_STATS, "SHITSKIPPED - stat[", i, "] ", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target))
						
						bShitskiped = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF bShitskiped = TRUE
			iMissionPercentage = 50
			iMedal = 1
			SET_ENDSCREEN_COMPLETION_LINE_STATE(assassinationEndScreen, TRUE, endLabel.tlCompletionBronze, iMissionPercentage, 0, ESC_PERCENTAGE_COMPLETION, ESMS_BRONZE)
			PRINTLN("SHITSKIPPED - NO MEDAL")
		ELSE
			IF bBonus // You get the bonus, you get Gold.
				IF bHaveRetryOnce
					iMissionPercentage = 75
					iMedal = 2
					SET_ENDSCREEN_COMPLETION_LINE_STATE(assassinationEndScreen, TRUE, endLabel.tlCompletionSilver, iMissionPercentage, 0, ESC_PERCENTAGE_COMPLETION, ESMS_SILVER)
					PRINTLN("AWARDING - ESMS_SILVER, WITH BONUS")
				ELSE
					iMissionPercentage = 100
					iMedal = 3
					SET_ENDSCREEN_COMPLETION_LINE_STATE(assassinationEndScreen, TRUE, endLabel.tlCompletionGold, iMissionPercentage, 0, ESC_PERCENTAGE_COMPLETION, ESMS_GOLD)
					PRINTLN("AWARDING - ESMS_GOLD")
				ENDIF
			ELIF (NOT bBonus AND NOT bHaveRetryOnce) // No bonus, one retry. Silver
				iMissionPercentage = 75
				iMedal = 2
				SET_ENDSCREEN_COMPLETION_LINE_STATE(assassinationEndScreen, TRUE, endLabel.tlCompletionSilver, iMissionPercentage, 0, ESC_PERCENTAGE_COMPLETION, ESMS_SILVER)
				PRINTLN("AWARDING - ESMS_SILVER")
			ELSE 
				iMissionPercentage = 50
				iMedal = 1
				SET_ENDSCREEN_COMPLETION_LINE_STATE(assassinationEndScreen, TRUE, endLabel.tlCompletionBronze, iMissionPercentage, 0, ESC_PERCENTAGE_COMPLETION, ESMS_BRONZE)
				PRINTLN("AWARDING - ESMS_BRONZE")
			ENDIF
		ENDIF
		
		UPDATE_STATS()
		
		PRINTLN("INSIDE - SETUP_RESULTS_UI")
		bResultsCreated = TRUE
	ENDIF
ENDPROC


PROC CUTSCENE_SKIP_FADE_OUT()
	DO_SCREEN_FADE_OUT(500)
	WHILE NOT IS_SCREEN_FADED_OUT()
		WAIT(0)
	ENDWHILE
ENDPROC

PROC CUTSCENE_SKIP_FADE_IN()
	DO_SCREEN_FADE_IN(1000)
	WHILE NOT IS_SCREEN_FADED_IN()
		WAIT(0)
	ENDWHILE
ENDPROC

FUNC BOOL IS_PLAYER_SKIPPING_CUTSCENE()
	IF TIMERA() >= DEFAULT_CAR_STOPPING_TO_CUTSCENE
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_ASSASSIN_CUTSCENE_START_TIME()
	iCutsceneStartTime = GET_GAME_TIMER()
	
	RETURN iCutsceneStartTime
ENDFUNC

FUNC BOOL HAS_TARGET_ESCAPED(PED_INDEX pedToCheck, FLOAT fDistToCheck, FLOAT fFailDistDisregardSightChecks = 0.0)
	VEHICLE_INDEX vehToCheck
	FLOAT fPlayerDistFromPed
	
	IF DOES_ENTITY_EXIST(pedToCheck)
		IF NOT IS_PED_INJURED(pedToCheck)
			IF IS_PED_IN_ANY_VEHICLE(pedToCheck)
				vehToCheck = GET_VEHICLE_PED_IS_IN(pedToCheck)	
				fPlayerDistFromPed = GET_PLAYER_DISTANCE_FROM_ENTITY(pedToCheck)
				IF fPlayerDistFromPed > fDistToCheck
					IF NOT IS_ENTITY_ON_SCREEN(vehToCheck)
					OR IS_ENTITY_OCCLUDED(vehToCheck)
						RETURN TRUE
					ELSE
						IF fFailDistDisregardSightChecks != 0
							IF fPlayerDistFromPed > fFailDistDisregardSightChecks
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())		//since the player could potentially be able to make up some ground while they are still in sight
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(pedToCheck) > fDistToCheck
					IF NOT IS_ENTITY_ON_SCREEN(pedToCheck)
					OR IS_ENTITY_OCCLUDED(pedToCheck)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Return TRUE if the killcam has finished playing
FUNC BOOL HAS_SLOW_KILLCAM_FINISHED(PED_INDEX pedToFocus, BOOL bPlayCamera = TRUE, BOOL bUseSlowMoEffect = TRUE, FLOAT fSlowestTimeScale = 0.1)
	VECTOR vTempCamPos
	FLOAT fCutsceneTime
	
	IF bPlayCamera
		SWITCH iKillCamStage
			CASE 0 
				//ecit out early if the target is too far away to render the kill camera
				vTempCamPos = GET_ENTITY_COORDS(pedToFocus, FALSE)
				IF GET_DISTANCE_BETWEEN_COORDS( GET_ENTITY_COORDS(PLAYER_PED_ID()), vTempCamPos ) >= 250.0
					PRINTLN("OJAS - ASSASSINATIONS - SKIPPING KILL CAM BECAUSE TARGET IS TOO FAR AWAY!")
					RETURN TRUE
				ENDIF
				
				GET_ASSASSIN_CUTSCENE_START_TIME()
				CLEAR_PRINTS()
				CLEAR_HELP()
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					WAIT(0)		//wait a frame to make sure that the sniper scope is not displayed on screen during kill cam
				ENDIF
				
//				IF NOT DOES_CAM_EXIST(camKill)
//					camKill = CREATE_CAMERA()
//					SET_CAM_PARAMS(camKill, << vTempCamPos.x - 4.0, vTempCamPos.y + 4.0,  vTempCamPos.z + 3.5 >>, <<0,0,0>>, 35)
//				ENDIF
//				POINT_CAM_AT_PED_BONE(camKill, pedToFocus, BONETAG_HEAD, <<0,0,0>>, FALSE)
//				
//				SET_CAM_ACTIVE(camKill, TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				
				IF bUseSlowMoEffect
					SET_TIME_SCALE(fSlowestTimeScale)
				ENDIF
				SETTIMERB(0)
				iKillCamStage++
			BREAK
			CASE 1
				IF bUseSlowMoEffect
					float fGameSpeed
					fGameSpeed = TO_FLOAT(TIMERB() - 40) / 220.0
					If fGameSpeed > 1.0
						fGameSpeed = 1.0
					ENDIF
					IF fGameSpeed < fSlowestTimeScale
						fGameSpeed = fSlowestTimeScale
					endif
					SET_TIME_SCALE(fGameSpeed)
					fCutsceneTime = 700
				ELSE
					fCutsceneTime = 2000
				ENDIF
				
				if TIMERB() > fCutsceneTime
				OR HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
				OR IS_ENTITY_OCCLUDED(pedToFocus)
					ODDJOB_EXIT_CUTSCENE()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					CLEAR_HELP(TRUE)
//					DESTROY_CAM(camKill)
					PRINTLN("KILL CAM FINISHED")
					
					CUTSCENE_SKIP_FADE_IN()
					
					SET_TIME_SCALE(1.0)
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		SET_TIME_SCALE(1.0)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC KILL_CURRENT_AMBIENT_MISSION_DIALOGUE_IF_TOUCHED(PED_INDEX ped)
	IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), ped)
		IF IS_SCRIPTED_SPEECH_PLAYING(ped)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Only allow the player to aim and shoot from their current position
PROC DISABLE_PLAYER_ACTIONS()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WHISTLE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PICKUP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
ENDPROC

//Cut to a camera shot of the target
//If bAttachToPed then vCamCoords is used as the offset of the ped you are attaching the camera to (otherwise use world coords)
PROC CONTROL_CIRCLE_CAM_CUT_FOR_TARGET(ENTITY_INDEX target, VECTOR vCamCoords, VECTOR vCamRot, BOOL bPointCamAtEntity = TRUE, BOOL bSetCamRenderingFalseOnExit = TRUE)
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		DISABLE_PLAYER_ACTIONS()
		
		IF NOT DOES_CAM_EXIST(camHint)
			camHint = CREATE_CAMERA(CAMTYPE_SCRIPTED)
			
			SET_CAM_COORD(camHint, vCamCoords)
			IF bPointCamAtEntity
				POINT_CAM_AT_ENTITY(camHint, target, <<0,0,0>>)
			ELSE
				POINT_CAM_AT_COORD(camHint, vCamRot)
			ENDIF
		ENDIF
		
		IF NOT IS_CAM_ACTIVE(camHint)
			SET_CAM_ACTIVE(camHint, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
		ENDIF	
	ELSE
		IF DOES_CAM_EXIST(camHint)
			IF IS_CAM_ACTIVE(camHint)
				SET_CAM_ACTIVE(camHint, FALSE)
				
				IF bSetCamRenderingFalseOnExit
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
				ENDIF
				
				DESTROY_CAM(camHint)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC KILL_HINT_CAM_IF_ACTIVE()
	IF DOES_CAM_EXIST(camHint)
		IF IS_CAM_ACTIVE(camHint)
			SET_CAM_ACTIVE(camHint, FALSE)
			DESTROY_CAM(camHint)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			CLEAR_FOCUS()
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_ASSASSIN_SPEECH_PLAY(PED_INDEX pedToCheck)
	IF GET_PLAYER_DISTANCE_FROM_ENTITY(pedToCheck) < 50
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_COORD_AND_NOT_A_TAXI_PASSENGER(VECTOR vLocation, FLOAT fDistToCheck)
	IF GET_PLAYER_DISTANCE_FROM_LOCATION(vLocation) <= fDistToCheck
	AND NOT IS_PLAYER_A_TAXI_PASSENGER()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_ENTITY_AND_NOT_A_TAXI_PASSENGER(ENTITY_INDEX myObject, FLOAT fDistToCheck)
	IF GET_PLAYER_DISTANCE_FROM_ENTITY(myObject) <= fDistToCheck
	AND NOT IS_PLAYER_A_TAXI_PASSENGER()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_SAFE_FOR_ASSASSIN_MISSION(VEHICLE_INDEX vehToCheck)
	IF DOES_ENTITY_EXIST(vehToCheck)
		IF NOT IS_VEHICLE_DRIVEABLE(vehToCheck)
			RETURN FALSE
		ELSE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehToCheck, PLAYER_PED_ID())
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//remove the wanted check from aggro checks
PROC DONT_CHECK_FOR_WANTED_IN_AGGRO_CHECKS()
	IF NOT IS_BITMASK_AS_ENUM_SET(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
		SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
	ENDIF
ENDPROC

//add the wanted bit check for aggro checks
PROC CHECK_FOR_WANTED_IN_AGGRO_CHECKS()
	IF IS_BITMASK_AS_ENUM_SET(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
		CLEAR_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
	ENDIF
ENDPROC

// put out fires near the player and on their car 
PROC STOP_FIRES_AROUND_PLAYER_FOR_CUTSCENE()
	STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 15)
	IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
		STOP_ENTITY_FIRE(GET_PLAYERS_LAST_VEHICLE())
	ENDIF
ENDPROC


PROC PREP_PLAYER_FOR_CUTSCENE()
	BOOL bPlayerInCar
	VEHICLE_INDEX playerVeh
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		bPlayerInCar = TRUE
	ENDIF
	IF bPlayerInCar
		BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(playerVeh, 5, 3)
	ELSE
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ENDIF
	ENDIF
ENDPROC


//Returns TRUE if the player has abandoned the area - 
FUNC BOOL HAS_PLAYER_ABANDONED_ASSASSINATION_AREA(BOOL & bWarnBoolToCheck, VECTOR vPosToCheck, STRING warnMsg, FLOAT fFailDist)
	FLOAT fCurrentDistance
	
//	//TODO: change this! always returning false now!
//	RETURN FALSE
	
	IF NOT bGrabbedDistance
		fStoredDistance = GET_PLAYER_DISTANCE_FROM_LOCATION(vPosToCheck)
		PRINTLN("fStoredDistance = ", fStoredDistance)
		bGrabbedDistance = TRUE
	ENDIF
	
	fCurrentDistance = GET_PLAYER_DISTANCE_FROM_LOCATION(vPosToCheck)
//	PRINTLN("fCurrentDistance = ", fCurrentDistance)
	
	IF NOT bWarnBoolToCheck
		IF fCurrentDistance > (fStoredDistance + (fFailDist/2))
			//warn the player
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				PRINT_NOW(warnMsg, DEFAULT_GOD_TEXT_TIME, 1)
				bWarnBoolToCheck = TRUE
				RESTART_TIMER_NOW(assFailTimer)
			ENDIF
		ENDIF
	ELSE
		//fail the player if they have been told to return to the location and have ignored it
		IF fCurrentDistance > fFailDist
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF IS_TIMER_STARTED(assFailTimer)
					IF GET_TIMER_IN_SECONDS(assFailTimer) > 15
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_NEED_SAFE_WARP()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			PRINTLN("RETURNING TRUE ON - DOES_PLAYER_NEED_SAFE_WARP")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//functions to fade in and out safely
PROC FADE_IN(INT iTime = 500)
	IF NOT IS_SCREEN_FADED_IN()
		DO_SCREEN_FADE_IN(iTime)
		WHILE NOT IS_SCREEN_FADED_IN()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC FADE_OUT(INT iTime = 500)
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(iTime)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_ANY_SHOP_EXCEPT_AMMUNATION()
	IF IS_PLAYER_IN_ANY_SHOP()
		IF NOT IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_GUN)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
