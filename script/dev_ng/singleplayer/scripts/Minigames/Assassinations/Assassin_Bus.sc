
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      					//
//      SCRIPT NAME     : Assassin_Bus.sch       							        						//
//      AUTHOR          : Michael Bagley	                                            					//
//      DESCRIPTION     : Bus Assassination Script - Player drives a bus searching bus stops for the target	//
//                                                                                      					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "Assassin_Core.sch"
USING "Assassin_Shared.sch"
USING "Assassination_briefing_lib.sch"
USING "replay_public.sch"
USING "clearMissionArea.sch"
USING "replay_public.sch"
USING "load_queue_public.sch"

// ********************************** Script Globals *************************************
CONST_INT NUM_BUS_STOPS 4
CONST_INT NUM_BUS_STOP_PEDS 3
CONST_INT NUM_BUS_SEATS 9
CONST_FLOAT BUS_ABANDON_DISTANCE 10000.0
CONST_FLOAT BUS_SPEED_FACTOR 5.0
CONST_FLOAT BUS_STOP_PAD 6.0

ENUM MISSION_BUS_STAGE
	MISSION_BUS_STREAM_PEDS,
	MISSION_BUS_CREATE_PEDS,
	MISSION_BUS_ENTER_BUS,
	MISSION_BUS_UPDATE_BUS_PEDS,
	MISSION_BUS_TARGET_FLEE,
	MISSION_BUS_UPDATE_BIKE,
	MISSION_BUS_FLEE_ON_FOOT
ENDENUM
MISSION_BUS_STAGE missionStage

ASS_ARGS asnArgs

// ------------------------------Cutscene variables------------------------------

ENUM PAYPHONE_CUTSCENE
	PAYPHONE_CUTSCENE_INIT = 0,
	PAYPHONE_CUTSCENE_UPDATE,
	PAYPHONE_CUTSCENE_SKIP,
	PAYPHONE_CUTSCENE_CLEANUP
ENDENUM
PAYPHONE_CUTSCENE payPhoneCutsceneStages = PAYPHONE_CUTSCENE_INIT

OBJECT_INDEX oPayPhone, oPayPhoneAnimated
CAMERA_INDEX camPayPhoneIntro01
CAMERA_INDEX camPayPhoneIntro01a
VECTOR vPhoneLocation = <<-26.1052, -109.8479, 56.0793>> 
VECTOR vPlayerWarpPosition = <<-26.1052, -109.8479, 56.0793>>  
FLOAT fPlayerWarpHeading = 159.0238 

FLOAT fCutsceneLength = 30.0
FLOAT fRightX, fRightY
VECTOR vCameraPosition01a = <<-27.2053, -109.6966, 57.6161>> 
VECTOR vCameraRotation01a = <<-8.1281, -0.0574, -140.8007>> 
VECTOR vCameraPosition01 = <<-27.2053, -109.6968, 57.6161>> 
VECTOR vCameraRotation01 = <<-1.4844, -0.0472, -124.6345>>  
structTimer tPayphoneCutsceneTimer
structTimer tPayphoneCameraTimer
BOOL bSkipCutscene = FALSE
BOOL bPlayedPayPhoneConvo = FALSE
BOOL bContinueWithScene = FALSE
//BOOL bTouchedRightStick = FALSE

VECTOR scenePosition = <<0,0,0>>
VECTOR sceneRotation = <<0,0,0>>
INT iSceneId
INT iIntroCameraStages = 0
INT iNavMeshBlocking

STREAMVOL_ID svPhoneCutscene

// ------------------------------END Cutscene variables------------------------------


BOOL bWantedLevelSet
BOOL bPlayerInBus
BOOL bReturnMsgPrinted
BOOL bOffBikeMsgPlayed
BOOL bBailMsgPlayed
//BOOL bPassengersLeftBus
BOOL bTargetFleeing
BOOL bCutsceneSkipped
//BOOL bTargetBrawlMsg
BOOL bBusFacingCorrectly
BOOL bApproachBackwards
BOOL bBusFacingPerpendicular
BOOL bNeedToLeaveArea = FALSE
BOOL bBusDamaged
BOOL bReplaying = FALSE
//BOOL bFirstObjectivePrinted
//BOOL bFirstCheckpointSceneLoad = FALSE
//BOOL bSecondCheckpointSceneLoad = FALSE
//BOOL bThirdCheckpointSceneLoad = FALSE
BOOL bTaskedPassengers = FALSE
BOOL bRunDistanceToStop = TRUE
//BOOL bRemovedAnims = FALSE
BOOL bReactionAnimsLoaded = FALSE
BOOL bPrintCinematicHelp = FALSE
BOOL bFirstPress = FALSE
BOOL bGrabbedSpeaker = FALSE
BOOL bTaskedOldLady = FALSE
BOOL bTaskedToEnterBus = FALSE
BOOL bUsingBus = FALSE
BOOL bUsingCoach = FALSE
BOOL bReplayVehicleAvailable = FALSE
BOOL bSetThirdCheckpoint = FALSE
BOOL bTriggerEraticAnimations = FALSE
//BOOL bUseCinematicCam = FALSE
BOOL bPlayedCellPhoneCall = FALSE
BOOL bGrabbedDistanceToNextStop = FALSE
BOOL bTargetKillLinePlayed = FALSE
BOOL bLoadSceneRequested = FALSE
BOOL bClearedHelp = FALSE
BOOL bPoliceScannerLinePlayed = FALSE
BOOL bResetBikeSpeed = FALSE
//BOOL bClosingBusDoorsForCutscene = FALSE
BOOL bDoneFlash = FALSE

INT iBusCutsceneStage
INT iMocapBusSceneStage
INT iBusStop = -1
INT iFrameToCheck
INT iFranklinChaseSpeech
INT iFranklinSpeechTime = GET_RANDOM_INT_IN_RANGE(5, 15)
INT iPassengerChaseSpeech
INT iPassengerPreChaseSpeech
INT iPassengerReactionSpeech
INT iPassengerAbandonSpeech
INT iPassengerSpeechTime = GET_RANDOM_INT_IN_RANGE(5, 10)
INT iFootChaseSpeech
INT iNumBusPeds = 0
INT iNumInnocentsDead
INT iNumDeadMsg
INT iLastBusHealthChecked = 1000
INT iBusHealthSpeech
INT iBusStopOrder[NUM_BUS_STOPS]
INT iNumStopsVisited
INT iRecordingTime
//INT iTargetBikeFrameCounter
INT iRunAlternateStage
INT iStageToUse
INT iLeaveBusStages = 0

FLOAT fBikeTopSpeedModifier = 0.50	//percentage increase to bump the vehicles max top speed by
FLOAT fBikeNormalSpeed
FLOAT fDesiredBikeSpeed
FLOAT fFirstCamCutTime
FLOAT fDefaultAssassinSceneRange = 15
FLOAT fScreamTime

VECTOR vCombatPos
VECTOR vBusDoorPos
VECTOR vBusWarpPos
VECTOR vBusCamPos
VECTOR vBusCamRot
VECTOR vBusPreWarpPos
VECTOR vBusLot = << 414.1398, -640.0020, 27.5001 >>
//VECTOR vMissionCenter = << 288.6, -860.02, 29.28 >>	//roughly the center of the downtown area used in the mission
VECTOR vBusPos, vPlayerPosition // Used to track abandon distance
FLOAT fBusWarpHead
FLOAT fDistanceToNextStop

// Move Vehicle Positions
VECTOR 	vMoveVehiclePosition01 = <<-25.1870, -113.0940, 55.9396>>
FLOAT 	fMoveVehicleHeading01 = 69.4062

PED_INDEX busPed4, busPed5, busPed6, busPed7, pedBusPassengerShooting
PED_INDEX pedBusLotScenarios[4]
//PED_INDEX pedCurrentlyEnteringBus

VEHICLE_INDEX tempBus1, tempBus2
VEHICLE_INDEX vehPlayerIsIn
VEHICLE_INDEX viReplayVehicle
VEHICLE_INDEX viPlayerLastVehicle

CAMERA_INDEX busCam
CAMERA_INDEX busCam2
//CAMERA_INDEX franklinCam

SCENARIO_BLOCKING_INDEX sbiBusLot[6]

MODEL_NAMES mnCarPlayerIsIn

structTimer busTimer
structTimer failTimer
structTimer speechTimerFranklin
structTimer speechTimerPassenger
structTimer speechTimerPassengerPreChase
structTimer speechDelayTimer
structTimer playbackTimer
//structTimer passengerExitTimer
structTimer tScreamTimer
structTimer tPoliceScanner

SCENARIO_BLOCKING_INDEX sb01, sb02
STREAMVOL_ID svBusStopCutscene

// Cutscene
PED_INDEX pedOldLady
//CustomCutsceneCaller fpCutsceneCustomIntro

WEAPON_TYPE mPlayerWeapon

SIMPLE_USE_CONTEXT cucEndScreenCore

LoadQueueLarge sLoadQueue

STRUCT BUS_STOP_STRUCT // contains vectors and headings for police blockade positions
	BOOL					bActive         	// check to see if the bus stop is active
	VECTOR					vStopPos			// location to spawn the roadblock
	FLOAT					fStopHead			// bus stop heading	
	VECTOR					vVehPos				// position for targets vehicle (if positioned there)
	FLOAT 					fVehHead			// heading for the targets vehicle(if positioned there)
	BLIP_INDEX				blipStop			// blip for the bus stop
	TEXT_LABEL				tlBlip				// blip name
	PED_INDEX				piAmbPed[4]			// ambient bus peds at the bus stop
	VEHICLE_INDEX			viBike				// bmx bike parked at stop
	FLOAT 					fCorrectHeading 	// proper heading for each stop
	VECTOR					vCoronaPosition		// position for in game corona
	VECTOR					vStreamVolPos		//location for streamvol
	VECTOR					vStreamVolDir		//direction vector for streamvol
ENDSTRUCT
BUS_STOP_STRUCT	busStop[NUM_BUS_STOPS]

CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

// -----------------------------------
// DEBUG MODE
// -----------------------------------
#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

#IF IS_DEBUG_BUILD
	DEBUG_POS_DATA myDebugData
	BOOL bDoingPSkip = FALSE
	BOOL bDoingJSkip = FALSE
	
	MissionStageMenuTextStruct sSkipMenu[3]
	INT iDebugJumpStage = 0 
//	INT iSkip

	VECTOR vSpherePosition, vPlayerDebugPositon
	FLOAT fDebugHeight, fPlayerDebugHeading
	
	PROC SETUP_DEBUG()
		sSkipMenu[0].sTxtLabel = "TRAVEL_STAGE"
		sSkipMenu[1].sTxtLabel = "PICKUP_PASSENGERS"
		sSkipMenu[2].sTxtLabel = "KILL_TARGET"
	ENDPROC
#ENDIF


// ********************************** END SCRIPT GLOBALS  *************************************


//initialize all the necessities for the mission
PROC INIT_BUS_DATA(ASS_TARGET_DATA& targetData)
	targetData.targetEnum = A_M_M_Business_01
	targetData.otherPedEnum = A_M_M_EastSA_02
	targetData.auxPedEnum = A_M_O_Tramp_01
	targetData.vehEnum = BUS
	targetData.otherVehEnum = SCORCHER
	targetData.vTargetPos = << 477.8577, -605.9913, 28.5108 >>
	targetData.fTargetHead = 352
	targetData.vDestPos = << 421.5518, -640.2146, 27.5068 >>
	targetData.vVehPos = << 421.5518, -640.2146, 27.5068 >>
	targetData.fVehHead = 178.95

	//cutscene
	targetData.vCutscenePos = << 420.3106, -676.6454, 36.0428 >>
	targetData.vCutsceneHead = << -1.2817, 0.0000, -9.4208 >>
	targetData.fCutsceneDoF = 39.8344
	targetData.sStringTable = "ASS_BS"
	targetData.sAnimDict[0] = "oddjobs@assassinate@bus@"
	targetData.sAnimDict[1] = "misscommon@response"
	
	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.vehEnum)
	ADD_STREAMED_MODEL(targetData.streamedModels, targetData.otherVehEnum)
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[0])
	ADD_STREAMED_ANIM(targetData.streamedAnims, targetData.sAnimDict[1])

	targetData.OBJECTIVES[0] = "ASS_BS_ENTER"
	
	targetData.vWarpPos = << 421.4034, -675.3058, 28.2859 >>
	targetData.myType = MISSION_NAME_BUS
	targetData.bNonTemp = TRUE
	targetData.bLeaveVehicles = TRUE
ENDPROC


FUNC BOOL ARE_MODELS_LOADED_FOR_EARLY_BUS_STOPS(ASS_TARGET_DATA& targetData)
	BOOL bLoaded
	REQUEST_MODEL(targetData.auxPedEnum)
	REQUEST_MODEL(targetData.otherPedEnum)
	REQUEST_MODEL(A_M_M_EASTSA_01)
	
	IF HAS_MODEL_LOADED(targetData.auxPedEnum)
	AND HAS_MODEL_LOADED(targetData.otherPedEnum)
	AND HAS_MODEL_LOADED(A_M_M_EASTSA_01)
		PRINTLN("MODELS_LOADED_FOR_EARLY_BUS_STOPS")
		bLoaded = TRUE
	ELSE
		bLoaded = FALSE
	ENDIF
	
	RETURN bLoaded
ENDFUNC


FUNC BOOL ARE_MODELS_LOADED_FOR_FINAL_BUS_STOP(ASS_TARGET_DATA& targetData)
	BOOL bLoaded
	
	REQUEST_MODEL(targetData.targetEnum)
	REQUEST_MODEL(a_f_o_genstreet_01)
	
	IF HAS_MODEL_LOADED(targetData.targetEnum)
	AND HAS_MODEL_LOADED(a_f_o_genstreet_01)
		PRINTLN("MODELS_LOADED_FOR_FINAL_BUS_STOP")
		bLoaded = TRUE
	ELSE
		PRINTLN("WAITING FOR MODELS TO LOAD FOR FINAL_BUS_STOP")
		bLoaded = FALSE
	ENDIF
	
	RETURN bLoaded
ENDFUNC


PROC SET_BUS_MISSION_TRAFFIC_SETTINGS()	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.5)
	SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
	
	//so cars won't drive into bus during intro cutscene
	SET_ROADS_IN_ANGLED_AREA(<< 426.035, -644.930, -100 >>, << 421.035, -644.930, 100 >>, 55.000, FALSE, FALSE)
	
	VECTOR tempVec[6]
	tempVec[0] = << 421.94028, -652.32758, 27.50035 >>	// near parking lot entrance
	tempVec[1] = << 404.63669, -636.35431, 27.50008 >> 	// inside lot entrance west
	tempVec[2] = << 425.59662, -613.08936, 27.56621 >> 	// northernmost part of the lot
	tempVec[3] = << 423.34677, -636.78638, 27.50008 >> 	// center of lot
	tempVec[4] = << 421.88361, -667.14240, 28.04054 >> 	// lot entrance
	tempVec[5] = << 423.51428, -677.39557, 28.33767 >>	//road in front of entrance
	
	sbiBusLot[0] = ADD_SCENARIO_BLOCKING_AREA( (tempVec[0] - <<8,8,8>>), (tempVec[0] + <<8,8,8>>) )
	sbiBusLot[1] = ADD_SCENARIO_BLOCKING_AREA( (tempVec[1] - <<8.5,8.5,8.5>>), (tempVec[1] + <<8.5,8.5,8.5>>) )
	sbiBusLot[2] = ADD_SCENARIO_BLOCKING_AREA( (tempVec[2] - <<8.5,8.5,8.5>>), (tempVec[2] + <<8.5,8.5,8.5>>) )
	sbiBusLot[3] = ADD_SCENARIO_BLOCKING_AREA( (tempVec[3] - <<9.5,9.5,9.5>>), (tempVec[3] + <<9.5,9.5,9.5>>) )
	sbiBusLot[4] = ADD_SCENARIO_BLOCKING_AREA( (tempVec[4] - <<8.0,8.0,8.0>>), (tempVec[4] + <<8.0,8.0,8.0>>) )
	sbiBusLot[5] = ADD_SCENARIO_BLOCKING_AREA( (tempVec[5] - <<15.0,15.0,15.0>>), (tempVec[5] + <<15.0,15.0,15.0>>) )
ENDPROC

//setup data for each bus stop
PROC INIT_BUS_STOP_DATA()
	// first stop
	busStop[0].bActive = FALSE
	busStop[0].vStopPos = << 303.6007, -765.1572, 28.3112 >>
	busStop[0].fStopHead = 255.4414
	busStop[0].vVehPos = << 300.9491, -757.7515, 28.3240 >>
	busStop[0].fVehHead = 342.2245	
	busStop[0].tlBlip = "ASS_BS_STOP0"
	busStop[0].fCorrectHeading = 160.5196
	busStop[0].vCoronaPosition = <<307.4789, -766.0983, 28.2473>>
	busStop[0].vStreamVolPos = <<298.8621, -771.5198, 29.4770>>
	busStop[0].vStreamVolDir = <<8.7930, 0.0000, -52.1743>>
	//<<308.3141, -769.4610, 30.3267>>, <<-8.3640, 0.0000, 78.0163>>		//data for shot inside bus
	
	// second stop
	busStop[1].bActive = FALSE
	busStop[1].vStopPos = << 356.10, -1067.84, 28.57 >>
	busStop[1].fStopHead = 6.6156
	busStop[1].vVehPos = << 362.1344, -1073.3867, 28.5679 >>
	busStop[1].fVehHead = 272.8605
	busStop[1].tlBlip = "ASS_BS_STOP1"
	busStop[1].fCorrectHeading = 269.7676
	busStop[1].vCoronaPosition = <<355.5678, -1063.8975, 28.3927>>
	busStop[1].vStreamVolPos = <<364.4333, -1071.3138, 30.1969>>
	busStop[1].vStreamVolDir = <<0.8394, -0.0000, 42.3892>>
	//<<358.9606, -1062.5612, 30.3524>>, <<-5.0965, -0.0000, -172.7951>>
	
	// third stop
	busStop[2].bActive = FALSE
	busStop[2].vStopPos = << 264.05, -1120.78, 28.35 >>
	busStop[2].fStopHead = 179.7187
	busStop[2].vVehPos = << 258.1293, -1119.1965, 28.3053 >>
	busStop[2].fVehHead = 95.72
	busStop[2].tlBlip = "ASS_BS_STOP3"
	busStop[2].fCorrectHeading = 88.6072
	busStop[2].vCoronaPosition = <<264.2879, -1124.3931, 28.2193>>
	busStop[2].vStreamVolPos = <<254.3144, -1118.1940, 30.1446>>
	busStop[2].vStreamVolDir = <<-0.9803, 0.0000, -137.7603>>
	//<<261.3863, -1126.6526, 30.2008>>, <<-5.5612, 0.0000, 6.1281>>
	
	// fourth stop
	busStop[3].bActive = FALSE
	busStop[3].vStopPos = << 115.62, -781.09, 30.40 >>
	busStop[3].fStopHead = 154.9924 
	busStop[3].vVehPos = << 109.4136, -774.7261, 30.4493 >>
	busStop[3].fVehHead = 68.8722
	busStop[3].tlBlip = "ASS_BS_STOP2"
	busStop[3].fCorrectHeading = 68.4520
	busStop[3].vCoronaPosition = <<115.7016, -785.4189, 30.3382>>
	busStop[3].vStreamVolPos = <<104.8598, -774.3508, 31.6923>>
	busStop[3].vStreamVolDir = <<4.8718, 0.0000, -144.4729>>
ENDPROC

PROC REMOVE_BUS_PASSENGERS()
	INT idx, idx2 

	REPEAT NUM_BUS_STOPS idx
		REPEAT NUM_BUS_STOP_PEDS idx2
			IF DOES_ENTITY_EXIST(busStop[idx].piAmbPed[idx2])
				DELETE_PED(busStop[idx].piAmbPed[idx2])
				PRINTLN("REMOVE PED FROM STOP: ", idx, " PED: ", idx2)
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

PROC CREATE_FILLED_BUS(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	
	IF DOES_ENTITY_EXIST(args.myVehicle) AND NOT IS_ENTITY_DEAD(args.myVehicle)
		// ------------------------------------------------------BUS STOP 1----------------------------------------------------------------
		busStop[0].piAmbPed[0] = CREATE_PED_INSIDE_VEHICLE(args.myVehicle, PEDTYPE_CIVMALE, targetData.auxPedEnum, INT_TO_ENUM(VEHICLE_SEAT, 3))
//		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
//		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
//		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,3), 2, 1, 0) //(uppr)
//		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
//		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[0].piAmbPed[0], TRUE)
		
		busStop[0].piAmbPed[1] = CREATE_PED_INSIDE_VEHICLE(args.myVehicle, PEDTYPE_CIVMALE, A_M_M_EASTSA_01, INT_TO_ENUM(VEHICLE_SEAT, 4))
		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[0].piAmbPed[1], TRUE)
		
		busStop[0].piAmbPed[2] = CREATE_PED_INSIDE_VEHICLE(args.myVehicle, PEDTYPE_CIVMALE, targetData.otherPedEnum, INT_TO_ENUM(VEHICLE_SEAT, 5))
		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(busStop[0].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[0].piAmbPed[2], TRUE)
		
		// ------------------------------------------------------BUS STOP 2----------------------------------------------------------------
		busStop[1].piAmbPed[0] = CREATE_PED_INSIDE_VEHICLE(args.myVehicle, PEDTYPE_CIVMALE, targetData.auxPedEnum, INT_TO_ENUM(VEHICLE_SEAT, 6))
//		SET_PED_COMPONENT_VARIATION(busStop[1].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
//		SET_PED_COMPONENT_VARIATION(busStop[1].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 3, 0) //(uppr)
//		SET_PED_COMPONENT_VARIATION(busStop[1].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[1].piAmbPed[0], TRUE)
		
		busStop[1].piAmbPed[1] = CREATE_PED_INSIDE_VEHICLE(args.myVehicle, PEDTYPE_CIVMALE, A_M_M_EASTSA_01, INT_TO_ENUM(VEHICLE_SEAT, 7))
		SET_PED_COMPONENT_VARIATION(busStop[1].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
		SET_PED_COMPONENT_VARIATION(busStop[1].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(busStop[1].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[1].piAmbPed[1], TRUE)
		
		busStop[1].piAmbPed[2] = CREATE_PED_INSIDE_VEHICLE(args.myVehicle, PEDTYPE_CIVMALE, targetData.otherPedEnum, INT_TO_ENUM(VEHICLE_SEAT, 8))
		SET_PED_COMPONENT_VARIATION(busStop[1].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(busStop[1].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(busStop[1].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[1].piAmbPed[2], TRUE)
		
		// ------------------------------------------------------BUS STOP 3----------------------------------------------------------------
		busStop[2].piAmbPed[0] = CREATE_PED_INSIDE_VEHICLE(args.myVehicle, PEDTYPE_CIVMALE, targetData.auxPedEnum, INT_TO_ENUM(VEHICLE_SEAT, 0))
//		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
//		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
//		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
//		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[2].piAmbPed[0], TRUE)
		
		busStop[2].piAmbPed[1] = CREATE_PED_INSIDE_VEHICLE(args.myVehicle, PEDTYPE_CIVMALE, A_M_M_EASTSA_01, INT_TO_ENUM(VEHICLE_SEAT, 1))
		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[2].piAmbPed[1], TRUE)
		
		busStop[2].piAmbPed[2] = CREATE_PED_INSIDE_VEHICLE(args.myVehicle, PEDTYPE_CIVMALE, targetData.otherPedEnum, INT_TO_ENUM(VEHICLE_SEAT, 2))
		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(busStop[2].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[2].piAmbPed[2], TRUE)
	ENDIF
ENDPROC

PROC CREATE_BUS_PEDS_FOR_CURRENT_STOP(ASS_TARGET_DATA& targetData, INT iStopNum)
	VECTOR pedPos[3]
	
	//switch these positions around based on the stop so that the same ped enums won't always appear in the same location
	IF iStopNum = 0	
		pedPos[0] = << 303.68, -766.33, 28.31 >>
		pedPos[1] = << 305.28, -762.28, 28.41 >>
		pedPos[2] = << 302.70, -770.61, 28.45 >>
	ELIF iStopNum = 1
		pedPos[0] = << 351.41, -1068.09, 28.60 >>
		pedPos[1] = << 360.98, -1068.01, 28.53 >>
		pedPos[2] = << 357.6896, -1071.0891, 28.5567 >>
	ELIF iStopNum = 2
		pedPos[0] = << 260.96, -1119.53, 28.36 >>
		pedPos[1] = << 266.74, -1119.44, 28.37 >>
		pedPos[2] = << 258.61, -1121.24, 28.45 >>
	ELIF iStopNum = 3
		pedPos[0] = << 119.20, -781.85, 30.53 >>
		pedPos[1] = << 116.39, -778.02, 30.40 >>
		pedPos[2] = << 119.10, -781.83, 30.38 >>
	ENDIF
	
	CLEAR_AREA(busStop[iStopNum].vStopPos, 10, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< busStop[iStopNum].vStopPos.x - 30, busStop[iStopNum].vStopPos.y - 30, busStop[iStopNum].vStopPos.z - 30 >>,
			<< busStop[iStopNum].vStopPos.x + 30, busStop[iStopNum].vStopPos.y + 30, busStop[iStopNum].vStopPos.z + 30 >>, FALSE)
	
	//create bus stop peds and setup their variations
	IF NOT DOES_ENTITY_EXIST(busStop[iStopNum].piAmbPed[0])
		busStop[iStopNum].piAmbPed[0] = CREATE_PED(PEDTYPE_CIVMALE, targetData.auxPedEnum, pedPos[0], busStop[iStopNum].fStopHead)
		IF iStopNum = 0
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,3), 2, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
		ELIF iStopNum = 1
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
		ELIF iStopNum = 2
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
		ELIF iStopNum = 3
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
		ENDIF
		TASK_LOOK_AT_ENTITY(busStop[iStopNum].piAmbPed[0], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
		TASK_PLAY_ANIM(busStop[iStopNum].piAmbPed[0], "oddjobs@assassinate@bus@", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[iStopNum].piAmbPed[0], TRUE)
		
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(busStop[iStopNum].piAmbPed[0], "PASSENGERS_Group")
		PRINTLN("ADDING PASSENGER - busStop[iStopNum].piAmbPed[0] TO AUDIO GROUP - PASSENGERS_Group")
	ENDIF
	IF NOT DOES_ENTITY_EXIST(busStop[iStopNum].piAmbPed[1])
		busStop[iStopNum].piAmbPed[1] = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_EASTSA_01, pedPos[1], busStop[iStopNum].fStopHead)
		IF iStopNum = 0
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,3), 1, 3, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		ELIF iStopNum = 1
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
		ELIF iStopNum = 2
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
		ELIF iStopNum = 3
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,3), 2, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		ENDIF
		TASK_LOOK_AT_ENTITY(busStop[iStopNum].piAmbPed[1], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
		TASK_PLAY_ANIM(busStop[iStopNum].piAmbPed[1], "oddjobs@assassinate@bus@", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[iStopNum].piAmbPed[1], TRUE)
		
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(busStop[iStopNum].piAmbPed[1], "PASSENGERS_Group")
		PRINTLN("ADDING PASSENGER - busStop[iStopNum].piAmbPed[1] TO AUDIO GROUP - PASSENGERS_Group")
	ENDIF
	IF NOT DOES_ENTITY_EXIST(busStop[iStopNum].piAmbPed[2])
		busStop[iStopNum].piAmbPed[2] = CREATE_PED(PEDTYPE_CIVMALE, targetData.otherPedEnum, pedPos[2], busStop[iStopNum].fStopHead)
		IF iStopNum = 0
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
		ELIF iStopNum = 1
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		ELIF iStopNum = 2
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		ELIF iStopNum = 3
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(busStop[iStopNum].piAmbPed[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		ENDIF
		TASK_LOOK_AT_ENTITY(busStop[iStopNum].piAmbPed[2], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
		TASK_PLAY_ANIM(busStop[iStopNum].piAmbPed[2], "oddjobs@assassinate@bus@", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(busStop[iStopNum].piAmbPed[2], TRUE)
		
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(busStop[iStopNum].piAmbPed[2], "PASSENGERS_Group")
		PRINTLN("ADDING PASSENGER - busStop[iStopNum].piAmbPed[2] TO AUDIO GROUP - PASSENGERS_Group")
	ENDIF
	
	//create bike vehicle at bus stop
	IF iStopNum = 3
		IF NOT DOES_ENTITY_EXIST(busStop[iStopNum].viBike)
			busStop[iStopNum].viBike = CREATE_VEHICLE(targetData.otherVehEnum, busStop[iStopNum].vVehPos, busStop[iStopNum].fVehHead)
			SET_VEHICLE_ON_GROUND_PROPERLY(busStop[iStopNum].viBike)
		ELSE
			PRINTLN("busStop[iStopNum].viBike already exists - no need to create!")
		ENDIF
	ENDIF
ENDPROC


//create the target and set his bike at the specific Bus Stop
PROC CREATE_TARGET_AT_BUS_STOP(ASS_TARGET_DATA& targetData, ASS_ARGS& args, INT iStopNumber)
	VECTOR targetSpawnPos
	
	//only set these vectors up for the last 2 stops since he can only spawn at those 2 stops
	IF iStopNumber = 2
		targetSpawnPos = << 260.71, -1121.16, 28.42 >>
	ELIF iStopNumber = 3
		targetSpawnPos = << 110.80412, -777.96265, 30.43521 >>
	ENDIF
	
	//delete the ped standing next to the bike vehicle and replace him with the target
	IF DOES_ENTITY_EXIST(busStop[iStopNumber].piAmbPed[2])
		DELETE_PED(busStop[iStopNumber].piAmbPed[2])
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(args.myTarget)
		targetData.vTargetPos = targetSpawnPos
		targetData.fTargetHead = busStop[iStopNumber].fStopHead
		CREATE_ASSASSINATION_TARGET(targetData, args)
		SET_ENTITY_HEALTH(args.myTarget, 175)	//set his health a bit lower than the default value so that he is more likely to die when hit by a car
	ENDIF
	
	args.myOtherVehicle = busStop[iStopNumber].viBike
	
	IF NOT IS_ENTITY_PLAYING_ANIM(args.myTarget, "oddjobs@assassinate@bus@", "base")
		TASK_PLAY_ANIM(args.myTarget, "oddjobs@assassinate@bus@", "base", NORMAL_BLEND_IN, FAST_BLEND_OUT, -1, AF_LOOPING)
	ENDIF
ENDPROC

PROC REMOVE_FILLER_BUSES()
	IF DOES_ENTITY_EXIST(tempBus1)
		DELETE_VEHICLE(tempBus1)
		PRINTLN("tempBus1")
	ENDIF
	IF DOES_ENTITY_EXIST(tempBus2)
		DELETE_VEHICLE(tempBus2)
		PRINTLN("tempBus2")
	ENDIF
ENDPROC

PROC FILL_LOT_WITH_ADDITIONAL_BUSES(ASS_TARGET_DATA& targetData)
	tempBus1 = CREATE_VEHICLE(targetData.vehEnum, << 439.7705, -571.9359, 27.5068 >>, 325)
//	SET_VEHICLE_TYRES_CAN_BURST(tempBus1, FALSE)
	SET_VEHICLE_ON_GROUND_PROPERLY(tempBus1)
	tempBus2 = CREATE_VEHICLE(targetData.vehEnum, << 406.7046, -625.4039, 27.5068 >>, 91)
//	SET_VEHICLE_TYRES_CAN_BURST(tempBus2, FALSE)
	SET_VEHICLE_ON_GROUND_PROPERLY(tempBus2)
ENDPROC

FUNC BOOL FILL_LOT_WITH_AMBIENT_PEDS(ASS_TARGET_DATA& targetData)
	
	IF NOT DOES_ENTITY_EXIST(pedBusLotScenarios[3])
		REQUEST_MODEL(targetData.auxPedEnum)
		REQUEST_MODEL(targetData.otherPedEnum)
		REQUEST_MODEL(A_M_M_EASTSA_01)
		
		IF HAS_MODEL_LOADED(targetData.auxPedEnum)
		AND HAS_MODEL_LOADED(targetData.otherPedEnum)
		AND HAS_MODEL_LOADED(A_M_M_EASTSA_01)

			IF NOT DOES_ENTITY_EXIST(pedBusLotScenarios[0])
				pedBusLotScenarios[0] = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_EASTSA_01, << 435.07727, -650.21936, 27.74271 >>, 85.9928)
				SET_PED_COMPONENT_VARIATION(pedBusLotScenarios[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedBusLotScenarios[0], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(pedBusLotScenarios[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(pedBusLotScenarios[0], INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(pedBusLotScenarios[0], INT_TO_ENUM(PED_COMPONENT,10), 1, 1, 0) //(decl)
				
				IF DOES_SCENARIO_EXIST_IN_AREA(<< 435.07727, -650.21936, 27.74271 >>, 2.0, TRUE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedBusLotScenarios[0], << 435.07727, -650.21936, 27.74271 >>, 2.0, -1)
				ELSE
					TASK_WANDER_STANDARD(pedBusLotScenarios[0])
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedBusLotScenarios[1])
				pedBusLotScenarios[1] = CREATE_PED(PEDTYPE_CIVMALE, targetData.auxPedEnum, << 436.31714, -642.88324, 27.73943 >>, 32.8396)
				IF DOES_SCENARIO_EXIST_IN_AREA(<< 436.31714, -642.88324, 27.73943 >>, 2.0, TRUE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedBusLotScenarios[1], << 436.31714, -642.88324, 27.73943 >>, 2.0, -1)
				ELSE
					TASK_WANDER_STANDARD(pedBusLotScenarios[1])
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedBusLotScenarios[2])
				pedBusLotScenarios[2] = CREATE_PED(PEDTYPE_CIVMALE, targetData.otherPedEnum, << 435.65689, -645.55878, 27.73775 >>, 343.8998)
				IF DOES_SCENARIO_EXIST_IN_AREA(<< 435.65689, -645.55878, 27.73775 >>, 2.0, TRUE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedBusLotScenarios[2], << 435.65689, -645.55878, 27.73775 >>, 2.0, -1)
				ELSE
					TASK_WANDER_STANDARD(pedBusLotScenarios[2])
				ENDIF
			ENDIF
			
			pedBusLotScenarios[3] = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_EASTSA_01, << 436.68021, -629.73248, 27.71444 >>, 179.4746)
			SET_PED_COMPONENT_VARIATION(pedBusLotScenarios[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(pedBusLotScenarios[1], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(pedBusLotScenarios[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(pedBusLotScenarios[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(pedBusLotScenarios[1], INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)
			IF DOES_SCENARIO_EXIST_IN_AREA(<< 436.68021, -629.73248, 27.71444 >>, 2.0, TRUE)
				TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedBusLotScenarios[3], << 436.68021, -629.73248, 27.71444 >>, 2.0, -1)
			ELSE
				TASK_WANDER_STANDARD(pedBusLotScenarios[3])
			ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RELEASE_AMBIENT_PEDS_IN_LOT()
	IF DOES_ENTITY_EXIST(pedBusLotScenarios[0])
		SET_PED_AS_NO_LONGER_NEEDED(pedBusLotScenarios[0])
	ENDIF
	IF DOES_ENTITY_EXIST(pedBusLotScenarios[1])
		SET_PED_AS_NO_LONGER_NEEDED(pedBusLotScenarios[1])
	ENDIF
	IF DOES_ENTITY_EXIST(pedBusLotScenarios[2])
		SET_PED_AS_NO_LONGER_NEEDED(pedBusLotScenarios[2])
	ENDIF
	IF DOES_ENTITY_EXIST(pedBusLotScenarios[3])
		SET_PED_AS_NO_LONGER_NEEDED(pedBusLotScenarios[3])
	ENDIF
ENDPROC

PROC SET_BUSES_IN_LOT_AS_NO_LONGER_NEEDED()
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempBus1)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(tempBus1)
	ENDIF
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempBus2)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(tempBus2)
	ENDIF
ENDPROC


PROC REMOVE_STREAMVOL_FOR_BUS_STOP_CUTSCENE()
	IF STREAMVOL_IS_VALID(svBusStopCutscene)
		STREAMVOL_DELETE(svBusStopCutscene)
		bLoadSceneRequested = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the loading and unloading of the bus stop cutscenes so that we make sure it is loaded in when close to the current bus stop and unloads if the player dirves away
PROC HANDLE_STREAMVOL_LOADING_UNLOADING_FOR_CURRENT_STOP(INT iCurStop)
	
	IF NOT bLoadSceneRequested
		IF GET_PLAYER_DISTANCE_FROM_LOCATION(busStop[iCurStop].vStopPos) <= DEFAULT_CUTSCENE_LOAD_DIST
			IF NOT IS_VECTOR_ZERO(busStop[iCurStop].vStreamVolPos)
				svBusStopCutscene = STREAMVOL_CREATE_FRUSTUM(busStop[iCurStop].vStreamVolPos, busStop[iCurStop].vStreamVolDir, 25, FLAG_MAPDATA)
				IF iNumStopsVisited = 3
					REQUEST_CUTSCENE("ASS_MCS_1")
					SET_SRL_POST_CUTSCENE_CAMERA(<< 133.2094, -791.9310, 35.3551 >>, << -4.9404, 0.0000, 68.7852 >>)
					PRINTLN("REQUESTING CUT SCENE - ASS_MCS_1 - 02")
				ENDIF
				bLoadSceneRequested = TRUE
			ENDIF
		ENDIF
	ELSE
		IF GET_PLAYER_DISTANCE_FROM_LOCATION(busStop[iCurStop].vStopPos) > DEFAULT_CUTSCENE_UNLOAD_DIST
			REMOVE_STREAMVOL_FOR_BUS_STOP_CUTSCENE()
			REMOVE_CUTSCENE()
		ENDIF
	ENDIF
ENDPROC

//create blips for active bus stops (ones that the player has not visited yet)
PROC CREATE_BUS_STOP_BLIPS()
	INT tempInt
	VECTOR tempVec
	
	//only create blips for bus stops that the player has not visited yet
	FOR tempInt = 0 TO NUM_BUS_STOPS - 1 
		IF busStop[tempInt].bActive = TRUE
			IF NOT DOES_BLIP_EXIST(busStop[tempInt].blipStop)
				PRINTLN("CREATING BLIP: ", tempInt)
				//get position in street in front of the bus stop
				tempVec = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(busStop[tempInt].vStopPos, busStop[tempInt].fStopHead, <<0,3,0>>)
				busStop[tempInt].blipStop = CREATE_BLIP_FOR_COORD(tempVec, TRUE)
				SET_BLIP_NAME_FROM_TEXT_FILE(busStop[tempInt].blipStop, busStop[tempInt].tlBlip )
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//deblip any active bus stop blips
PROC REMOVE_BUS_STOP_BLIPS()
	REMOVE_BLIP(busStop[0].blipStop)
	REMOVE_BLIP(busStop[1].blipStop)
	REMOVE_BLIP(busStop[2].blipStop)
	REMOVE_BLIP(busStop[3].blipStop)
ENDPROC


PROC SET_TARGET_IN_GRAY_SUIT(ASS_ARGS& args)
	IF NOT IS_PED_INJURED(args.myTarget)
		SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
		SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,2), 2, 2, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(args.myTarget, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
	ENDIF
ENDPROC


//PROC HANDLE_BUS_PASSENGER_SPEECH_ABANDONING_ROUTE()
//	SWITCH iAbandonLine
//		CASE 0
//	ENDSWITCH
//ENDPROC

//create peds for next stop, blip the stop, and set active
PROC ACTIVATE_NEXT_BUS_STOP(ASS_TARGET_DATA& targetData, ASS_ARGS& args, INT iStopToActivate, BOOL bAddBlip = TRUE)
	
	busStop[iStopToActivate].bActive = TRUE		
	CREATE_BUS_PEDS_FOR_CURRENT_STOP(targetData, iStopToActivate)
	
	//check to see if the next stop to activate should be the one that the target will be at
	IF iStopToActivate = iBusStop
		CREATE_TARGET_AT_BUS_STOP(targetData, args, iBusStop)
		SET_TARGET_IN_GRAY_SUIT(args)
		//enable target panic checks now that his visibility and collision have been restored and he has been teleported
		args.bDoAssAggroChecks = TRUE
	ENDIF
	
	IF bAddBlip
		CREATE_BUS_STOP_BLIPS()
	ENDIF
	
//	bClosingBusDoorsForCutscene = FALSE
ENDPROC

//shuffle the route so that it is different each time the script is launched
PROC CREATE_BUS_ROUTE(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	//hard code the route - Bug 483685 - [LB] route need to be more realistic. 
	iBusStopOrder[0] = 0
	iBusStopOrder[1] = 1
	iBusStopOrder[2] = 2
	iBusStopOrder[3] = 3

	iBusStop = iBusStopOrder[3]
	
	ACTIVATE_NEXT_BUS_STOP(targetData, args, iBusStopOrder[0], FALSE)
ENDPROC


//spech timer stuff
PROC RESTART_ALL_SPEECH_TIMERS()
	RESTART_TIMER_NOW(speechTimerFranklin)
	RESTART_TIMER_NOW(speechDelayTimer)
	RESTART_TIMER_NOW(speechTimerPassenger)
ENDPROC

PROC RESTART_FRANKLIN_SPEECH_TIMER_AND_ADVANCE_SPEECH_STAGE(INT iMinTime, INT iMaxTime)
	iFranklinSpeechTime = GET_RANDOM_INT_IN_RANGE(iMinTime, iMaxTime)
	RESTART_TIMER_NOW(speechTimerFranklin)
	RESTART_TIMER_NOW(speechDelayTimer)
	iFranklinChaseSpeech ++
ENDPROC

PROC RESTART_PASSENGER_SPEECH_TIMER_AND_ADVANCE_SPEECH_STAGE(INT iMinTime, INT iMaxTime)
	iPassengerSpeechTime = GET_RANDOM_INT_IN_RANGE(iMinTime, iMaxTime)
	RESTART_TIMER_NOW(speechTimerPassenger)
	RESTART_TIMER_NOW(speechDelayTimer)
	iPassengerChaseSpeech ++
ENDPROC

PROC RESTART_PASSENGER_SPEECH_TIMER_AND_ADVANCE_SPEECH_STAGE_PRE_CHASE(INT iMinTime, INT iMaxTime)
	iPassengerSpeechTime = GET_RANDOM_INT_IN_RANGE(iMinTime, iMaxTime)
	RESTART_TIMER_NOW(speechTimerPassengerPreChase)
	iPassengerPreChaseSpeech++
ENDPROC

FUNC BOOL IS_PED_ALIVE_AND_IN_BUS(ASS_ARGS& args, PED_INDEX pedToCheck)
	IF NOT IS_PED_INJURED(pedToCheck)
	AND IS_PED_IN_VEHICLE(pedToCheck, args.myVehicle)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//player and target are involved in foot chase
PROC MANAGE_PASSENGER_PRE_CHASE_DIALOGUE(ASS_ARGS& args)
	
	IF iNumStopsVisited = 0
		PRINTLN("EXITING EARLY - iNumStopsVisited = 0")
		EXIT
	ENDIF
	
	IF IS_TIMER_STARTED(speechTimerPassengerPreChase)
		IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
				IF (GET_TIMER_IN_SECONDS(speechTimerPassengerPreChase) >= 7)
				OR bFirstPress
					SWITCH iPassengerPreChaseSpeech
						CASE 0
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed4)
								ADD_PED_FOR_DIALOGUE(args.assConv, 4, busPed4, "BusPed1")
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_BCH1", CONV_PRIORITY_VERY_HIGH)
								bFirstPress = FALSE
								
								RESTART_TIMER_NOW(speechTimerPassengerPreChase)
								iPassengerPreChaseSpeech = 1
								
								PRINTLN("FIRST LINE IS PLAYED INSIDE BUS")
							ELSE
								PRINTLN("ADVANCING TO NEXT PRE-CHASE DIALOGUE STAGE - IS_PED_INJURED(busPed4)")
							ENDIF
						BREAK
						CASE 1
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed5)
								ADD_PED_FOR_DIALOGUE(args.assConv, 5, busPed5, "BusPed2")
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_BCH2", CONV_PRIORITY_VERY_HIGH)
								
								RESTART_TIMER_NOW(speechTimerPassengerPreChase)
								iPassengerPreChaseSpeech = 2
								
								PRINTLN("SECOND LINE IS PLAYED INSIDE BUS")
							ELSE
								PRINTLN("ADVANCING TO NEXT PRE-CHASE DIALOGUE STAGE - IS_PED_INJURED(busPed5)")
							ENDIF
						BREAK
						CASE 2
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed6)
								ADD_PED_FOR_DIALOGUE(args.assConv, 6, busPed6, "BusPed3")
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_BCH3", CONV_PRIORITY_VERY_HIGH)
								
								RESTART_TIMER_NOW(speechTimerPassengerPreChase)
								iPassengerPreChaseSpeech = 3
								
								PRINTLN("THIRD LINE IS PLAYED INSIDE BUS")
							ELSE
								PRINTLN("ADVANCING TO NEXT PRE-CHASE DIALOGUE STAGE - IS_PED_INJURED(busPed6)")
							ENDIF
						BREAK
						CASE 3
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed4)
								ADD_PED_FOR_DIALOGUE(args.assConv, 4, busPed4, "BusPed1")
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_BCH4", CONV_PRIORITY_VERY_HIGH)
								
								RESTART_TIMER_NOW(speechTimerPassengerPreChase)
								iPassengerPreChaseSpeech = 4
								
								PRINTLN("FOURTH LINE IS PLAYED INSIDE BUS")
							ELSE
								PRINTLN("ADVANCING TO NEXT PRE-CHASE DIALOGUE STAGE - IS_PED_INJURED(busPed4)")
							ENDIF
						BREAK
						CASE 4
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed5)
								ADD_PED_FOR_DIALOGUE(args.assConv, 5, busPed5, "BusPed2")
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_BCH5", CONV_PRIORITY_VERY_HIGH)
								
								RESTART_TIMER_NOW(speechTimerPassengerPreChase)
								iPassengerPreChaseSpeech = 5
								
								PRINTLN("FIFTH LINE IS PLAYED INSIDE BUS")
							ELSE
								PRINTLN("ADVANCING TO NEXT PRE-CHASE DIALOGUE STAGE - IS_PED_INJURED(busPed5)")
							ENDIF
						BREAK
						CASE 5
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed6)
								ADD_PED_FOR_DIALOGUE(args.assConv, 6, busPed6, "BusPed3")
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_BCH6", CONV_PRIORITY_VERY_HIGH)
								
								RESTART_TIMER_NOW(speechTimerPassengerPreChase)
								iPassengerPreChaseSpeech = 6
								
								PRINTLN("SIXTH LINE IS PLAYED INSIDE BUS")
							ELSE
								PRINTLN("ADVANCING TO NEXT PRE-CHASE DIALOGUE STAGE - IS_PED_INJURED(busPed6)")
							ENDIF
						BREAK
						CASE 6
						
						BREAK
					ENDSWITCH
					
//					RESTART_PASSENGER_SPEECH_TIMER_AND_ADVANCE_SPEECH_STAGE_PRE_CHASE(7, 14)
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		START_TIMER_NOW(speechTimerPassengerPreChase)
		PRINTLN("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TIMER IS NOT STARTED, STARTING NOW!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
	ENDIF

ENDPROC

PROC REMOVE_SPECIAL_HINT_CAM()
	IF DOES_CAM_EXIST(camHint)
		IF IS_CAM_ACTIVE(camHint)
			SET_CAM_ACTIVE(camHint, FALSE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DISPLAY_HUD(TRUE)
			DESTROY_CAM(camHint)
			PRINTLN("DESTROYING CAMERA INSIDE BUS")
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_CINEMATIC_CAM(ASS_ARGS& args, BOOL bChaseIsOn = TRUE)
	
	IF iNumStopsVisited = 0
		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
		
		IF IS_HELP_MESSAGE_ON_SCREEN()
			CLEAR_HELP()
		ENDIF
		
		EXIT
	ELSE
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
	ENDIF
	
	IF NOT bPrintCinematicHelp
		IF iNumStopsVisited > 0
			PRINT_HELP("ASS_HELP_02")
			bPrintCinematicHelp = TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
			
			bClearedHelp = FALSE
			
			IF SHOULD_CONTROL_CHASE_HINT_CAM(localChaseHintCamStruct, TRUE, FALSE)
				
				IF NOT bChaseIsOn
					IF iPassengerPreChaseSpeech = 0
						bFirstPress = TRUE
					ENDIF
					PRINTLN("CALLING - MANAGE_PASSENGER_PRE_CHASE_DIALOGUE")
					MANAGE_PASSENGER_PRE_CHASE_DIALOGUE(args)
					
					IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_BUS_SCREAMS_INT_SCENE")
						STOP_AUDIO_SCENE("ASSASSINATION_BUS_SCREAMS_INT_SCENE")
						PRINTLN("CHASE IS NOT GOING, STOPPING AUDIO SCENE - ASSASSINATION_BUS_SCREAMS_INT_SCENE - 01")
					ENDIF
				ELSE
					IF NOT IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_BUS_SCREAMS_INT_SCENE")
						START_AUDIO_SCENE("ASSASSINATION_BUS_SCREAMS_INT_SCENE")
						PRINTLN("CHASE IS ON STARTING AUDIO SCENE - ASSASSINATION_BUS_SCREAMS_INT_SCENE")
					ENDIF
				ENDIF
				
				IF NOT DOES_CAM_EXIST(camHint)
					camHint = CREATE_CAMERA(CAMTYPE_SCRIPTED)
					SET_CAM_CONTROLS_MINI_MAP_HEADING(camHint, TRUE)
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ASS_HELP_02")
						CLEAR_HELP()
					ENDIF
					
					CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
					
					IF bUsingBus
						ATTACH_CAM_TO_ENTITY(camHint, args.myVehicle, <<0.0467, -6.4343, 1.5665>>)
						POINT_CAM_AT_ENTITY(camHint, args.myVehicle, <<-0.0074, -3.4691, 1.1139>>)
						PRINTLN("USING BUS INSIDE CAMERA POSITIONS")
					ELIF bUsingCoach
						ATTACH_CAM_TO_ENTITY(camHint, args.myVehicle, <<-0.0387, -5.3568, 0.8942>>)
						POINT_CAM_AT_ENTITY(camHint, args.myVehicle, <<0.0245, -2.4095, 0.3379>>)
						PRINTLN("USING COACH INSIDE CAMERA POSITIONS")
					ELSE
						ATTACH_CAM_TO_ENTITY(camHint, args.myVehicle, <<0.0467, -6.4343, 1.5665>>)
						POINT_CAM_AT_ENTITY(camHint, args.myVehicle, <<-0.0074, -3.4691, 1.1139>>)
						PRINTLN("USING DEFAULTS INSIDE CAMERA POSITIONS")
					ENDIF
					SET_CAM_FOV(camHint, 35.0)
					PRINTLN("CREATING CAMERA INSIDE BUS")
				ENDIF
				
				IF NOT IS_CAM_ACTIVE(camHint)
					SET_CAM_ACTIVE(camHint, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					DISPLAY_HUD(FALSE)
					PRINTLN("SETTING CAMERA ACTIVE AND RENDERING")
				ENDIF	
			ELSE
				IF DOES_CAM_EXIST(camHint)
					IF IS_CAM_ACTIVE(camHint)
						SET_CAM_ACTIVE(camHint, FALSE)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						DISPLAY_HUD(TRUE)
	//						DISPLAY_RADAR(TRUE)
						DESTROY_CAM(camHint)
						PRINTLN("DESTROYING CAMERA INSIDE BUS")
						
						CASCADE_SHADOWS_INIT_SESSION() 
						
						IF IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_BUS_SCREAMS_INT_SCENE")
							STOP_AUDIO_SCENE("ASSASSINATION_BUS_SCREAMS_INT_SCENE")
							PRINTLN("TOGGLE CAM IS OFF, STOPPING AUDIO SCENE - ASSASSINATION_BUS_SCREAMS_INT_SCENE - 02")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT bClearedHelp
				CLEAR_HELP()
				PRINTLN("CLEARING HELP - OUT OF BUS")
				bClearedHelp = TRUE
			ENDIF
			REMOVE_SPECIAL_HINT_CAM()
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_SCREAM_FROM_RANDOM_BUS_PASSENGER(VEHICLE_INDEX vehToCheck, BOOL bForcePanicScream = FALSE)
	
	IF NOT IS_TIMER_STARTED(tScreamTimer)
		RESTART_TIMER_NOW(tScreamTimer)
	ENDIF
	
	IF GET_TIMER_IN_SECONDS(tScreamTimer) > fScreamTime
		INT iSeat = GET_RANDOM_INT_IN_RANGE(0, 8)
		INT iReason = GET_RANDOM_INT_IN_RANGE(0, 2)
		PED_INDEX pedScream
		
		IF IS_VEHICLE_DRIVEABLE(vehToCheck)
			pedScream = GET_PED_IN_VEHICLE_SEAT(vehToCheck, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
			IF NOT IS_PED_INJURED(pedScream)
				IF NOT bForcePanicScream
					IF iReason = 0
						PLAY_PAIN(pedScream, AUD_DAMAGE_REASON_SCREAM_SHOCKED)
					ELIF iReason = 1
						PLAY_PAIN(pedScream, AUD_DAMAGE_REASON_SCREAM_TERROR)
					ELIF iReason = 2
						PLAY_PAIN(pedScream, AUD_DAMAGE_REASON_SCREAM_PANIC)
					ENDIF
//				ELSE
//					PLAY_PAIN(pedScream, AUD_DAMAGE_REASON_SCREAM_PANIC)			//set this up so coders can easily diagnose byg 1067458 0 disable this after the bug is fixed
				ENDIF
			ENDIF
		ENDIF
		
		fScreamTime = GET_RANDOM_FLOAT_IN_RANGE(0, 3)
		
		RESTART_TIMER_NOW(tScreamTimer)
	ENDIF
	
//	PRINTLN("fScreamTime = ", fScreamTime)
ENDPROC

PROC UPDATE_BUS_PASSENGER_ANIMS(ASS_ARGS& args, BOOL bRunAnimations = TRUE)
	INT idx
	PED_INDEX tempPed
	
	IF NOT bRunAnimations
		bTaskedPassengers = FALSE
		PRINTLN("WE'RE NO LONGER RUNNING ANIMATIONS, SETTING bTaskedPassengers = FALSE")
	ENDIF
	
	REQUEST_ANIM_DICT("veh@bus@passenger@rds@idle_panic")
	REQUEST_ANIM_DICT("veh@bus@passenger@rps@idle_panic")

	IF NOT bReactionAnimsLoaded
		IF HAS_ANIM_DICT_LOADED("veh@bus@passenger@rds@idle_panic")
		AND HAS_ANIM_DICT_LOADED("veh@bus@passenger@rps@idle_panic")
			bReactionAnimsLoaded = TRUE
			PRINTLN("bReactionAnimsLoaded = TRUE")
		ELSE
			PRINTLN("WAITING ON BUS PANIC ANIMATIONS")
		ENDIF
	ENDIF
	
	IF NOT bTriggerEraticAnimations
		IF NOT IS_ENTITY_DEAD(args.myVehicle) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(args.myVehicle) OR IS_PED_SHOOTING(PLAYER_PED_ID())
				bTriggerEraticAnimations = TRUE
				PRINTLN("bTriggerEraticAnimations = TRUE")	
			ENDIF
		ENDIF
	ENDIF
	
	IF bReactionAnimsLoaded AND bTriggerEraticAnimations
		IF NOT bTaskedPassengers
			REPEAT NUM_BUS_SEATS idx
				IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
					IF GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, idx)) <> NULL
						tempPed = GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, idx))
						
						IF NOT bRunAnimations
							IF GET_RANDOM_INT_IN_RANGE() % 3 < 1	// 33% chance to clear tasks	
								IF IS_ENTITY_PLAYING_ANIM(tempPed, "veh@bus@passenger@rds@idle_panic", "sit")
								OR IS_ENTITY_PLAYING_ANIM(tempPed, "veh@bus@passenger@rps@idle_panic", "sit")
									CLEAR_PED_TASKS(tempPed)
									PRINTLN("CLEARING PED TASKS")
								ENDIF
							ENDIF
						ENDIF
						
						IF bRunAnimations
							IF idx % 2 = 0		//we know they are on the driver's side
								TASK_PLAY_ANIM(tempPed, "veh@bus@passenger@rps@idle_panic", "sit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE())
								PRINTLN("TASKING PASSENGER TO PLAY ANIMATION: rps@idle_panic. SEAT: ", idx)
							ELSE
								TASK_PLAY_ANIM(tempPed, "veh@bus@passenger@rds@idle_panic", "sit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE())
								PRINTLN("TASKING PASSENGER TO PLAY ANIMATION: rds@idle_panic. SEAT: ", idx)
							ENDIF
//						ELSE
//							IF IS_ENTITY_PLAYING_ANIM(tempPed, "veh@bus@passenger@rds@idle_panic", "sit")
//								CLEAR_PED_TASKS(tempPed)
//								PRINTLN("CLEARING PED TASKS - 0")
//							ENDIF
						ENDIF

					ENDIF
				ENDIF
			ENDREPEAT
			
			PRINTLN("SETTING bTaskedPassengers = TRUE")
			bTaskedPassengers = TRUE
		ENDIF
		
		IF bRunAnimations
			PLAY_SCREAM_FROM_RANDOM_BUS_PASSENGER(args.myVehicle)
		
			IF NOT IS_AUDIO_SCENE_ACTIVE("ASSASSINATION_BUS_SCREAMS_EXT_SCENE")
				START_AUDIO_SCENE("ASSASSINATION_BUS_SCREAMS_EXT_SCENE")
				PRINTLN("STARING AUDIO SCENE - ASSASSINATION_BUS_SCREAMS_EXT_SCENE")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_BUS_PASSENGERS_FOR_DIALOGUE(ASS_ARGS& args)
	PED_INDEX tempPed
	INT tempInt
	
	//initialize these
	iNumBusPeds = 0
	
	//check all possible bus seats before setting up passsenger peds for dialogue
	REPEAT NUM_BUS_SEATS tempInt
		IF iNumBusPeds < 4
			IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
				IF GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, tempInt)) <> NULL
					tempPed = GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, tempInt))
					IF iNumBusPeds = 0
						ADD_PED_FOR_DIALOGUE(args.assConv, 4, tempPed, "BusPed1")
						busPed4 = tempPed
						PRINTLN("Bus ped in seat ", tempInt, " added as busPed4 to dialogue.")
						iNumBusPeds++
					ELIF iNumBusPeds = 1
						ADD_PED_FOR_DIALOGUE(args.assConv, 5, tempPed, "BusPed2")
						busPed5 = tempPed
						PRINTLN("Bus ped in seat ", tempInt, " added as busPed5 to dialogue.")
						iNumBusPeds++
					ELIF iNumBusPeds = 2
						ADD_PED_FOR_DIALOGUE(args.assConv, 6, tempPed, "BusPed3")
						busPed6 = tempPed
						PRINTLN("Bus ped in seat ", tempInt, " added as busPed6 to dialogue.")
						iNumBusPeds++
					ELIF iNumBusPeds = 3
						ADD_PED_FOR_DIALOGUE(args.assConv, 7, tempPed, "BusPed4")
						busPed7 = busPed7
						busPed7 = tempPed
						PRINTLN("Bus ped in seat ", tempInt, " added as busPed7 to dialogue.")
						iNumBusPeds++
					ENDIF
				ELSE
					PRINTLN("No ped in bus seat ", tempInt)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("iNumBusPeds = ", iNumBusPeds)
ENDPROC


//Handle Franklin's banter as he driving the bus chasing the target on his bike
PROC MANAGE_FRANKLIN_CHASE_SPEECH_IN_BUS(ASS_ARGS& args)
	
	SWITCH iFranklinChaseSpeech
		CASE 0
			CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_SPOT", CONV_PRIORITY_VERY_HIGH)
		BREAK
		CASE 1
			CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_CHAS1", CONV_PRIORITY_VERY_HIGH)
		BREAK
		CASE 2
			CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_CHAS2", CONV_PRIORITY_VERY_HIGH)
		BREAK
		CASE 3
			CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_CHAS3", CONV_PRIORITY_VERY_HIGH)
		BREAK
		CASE 4
			CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_CHAS4", CONV_PRIORITY_VERY_HIGH)
		BREAK
		CASE 5
			CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_CHAS5", CONV_PRIORITY_VERY_HIGH)
		BREAK
		CASE 6
			CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_CHAS6", CONV_PRIORITY_VERY_HIGH)
		BREAK
	ENDSWITCH
	
	RESTART_FRANKLIN_SPEECH_TIMER_AND_ADVANCE_SPEECH_STAGE(6,12)
ENDPROC

//passengers panicking as player is chasing the target
PROC MANAGE_PASSENGER_CHASE_DIALOGUE(ASS_ARGS& args)
	IF IS_TIMER_STARTED(speechTimerPassenger)
		IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
				IF GET_TIMER_IN_SECONDS(speechTimerPassenger) >= iPassengerSpeechTime
					SWITCH iPassengerChaseSpeech
						CASE 0
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed4)
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_PASS1", CONV_PRIORITY_VERY_HIGH)
							ENDIF
						BREAK
						CASE 1
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed5)
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_PASS2", CONV_PRIORITY_VERY_HIGH)
							ELSE
								PRINTLN("ADVANCING TO NEXT CHASE DIALOGUE STAGE - IS_PED_INJURED(busPed5)")
							ENDIF
						BREAK
						CASE 2
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed6)
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_PASS3", CONV_PRIORITY_VERY_HIGH)
							ELSE
								PRINTLN("ADVANCING TO NEXT CHASE DIALOGUE STAGE - IS_PED_INJURED(busPed6)")
							ENDIF
						BREAK
						CASE 3
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed4)
								ADD_PED_FOR_DIALOGUE(args.assConv, 6, busPed4, "BusPed3")
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_ARGUE", CONV_PRIORITY_VERY_HIGH)
							ELSE
								PRINTLN("ADVANCING TO NEXT CHASE DIALOGUE STAGE - IS_PED_INJURED(busPed4)!!!!")
							ENDIF
						BREAK
						CASE 4
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed4)
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_PASS4", CONV_PRIORITY_VERY_HIGH)
							ELSE
								PRINTLN("ADVANCING TO NEXT CHASE DIALOGUE STAGE - IS_PED_INJURED(busPed4)")
							ENDIF
						BREAK
						CASE 5
							IF IS_PED_ALIVE_AND_IN_BUS(args, busPed5)
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_PASS5", CONV_PRIORITY_VERY_HIGH)
							ELSE
								PRINTLN("ADVANCING TO NEXT CHASE DIALOGUE STAGE - IS_PED_INJURED(busPed5)")

							ENDIF
						BREAK
					ENDSWITCH
					
					RESTART_PASSENGER_SPEECH_TIMER_AND_ADVANCE_SPEECH_STAGE(10, 20)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//speech reactions for a ped on the bus after the player damages it
PROC MANAGE_BUS_PED_SPEECH_FOR_DAMAGED_BUS(ASS_ARGS& args)
	INT tempHealth = GET_ENTITY_HEALTH(args.myVehicle)
	
	SWITCH iBusHealthSpeech
		CASE 0
			IF GET_VEHICLE_NUMBER_OF_PASSENGERS(args.myVehicle) >= 2
				iLastBusHealthChecked = GET_ENTITY_HEALTH(args.myVehicle)
				iBusHealthSpeech++
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF tempHealth < iLastBusHealthChecked - 50
				OR HAS_ENTITY_COLLIDED_WITH_ANYTHING(args.myVehicle)
					IF IS_PED_ALIVE_AND_IN_BUS(args, busPed4)
						ADD_PED_FOR_DIALOGUE(args.assConv, 4, busPed4, "BusPed1")
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_DMG4", CONV_PRIORITY_VERY_HIGH)
						iLastBusHealthChecked = tempHealth
						iBusHealthSpeech++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF tempHealth < iLastBusHealthChecked - 150
				OR HAS_ENTITY_COLLIDED_WITH_ANYTHING(args.myVehicle)
					IF IS_PED_ALIVE_AND_IN_BUS(args, busPed5)
						ADD_PED_FOR_DIALOGUE(args.assConv, 5, busPed5, "BusPed2")
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_DMG5", CONV_PRIORITY_VERY_HIGH)
						iLastBusHealthChecked = tempHealth
						iBusHealthSpeech++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF tempHealth < iLastBusHealthChecked - 200
				OR HAS_ENTITY_COLLIDED_WITH_ANYTHING(args.myVehicle)
					IF IS_PED_ALIVE_AND_IN_BUS(args, busPed6)
						ADD_PED_FOR_DIALOGUE(args.assConv, 6, busPed6, "BusPed3")
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_DMG6", CONV_PRIORITY_VERY_HIGH)
						iLastBusHealthChecked = tempHealth
						iBusHealthSpeech++
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


//speech reactions for a ped on the bus after the player damages it
PROC MANAGE_BUS_PED_SPEECH_FOR_HITTING_PEDS(ASS_ARGS& args)
	INT iRand

	IF iNumInnocentsDead > 0
		SWITCH iPassengerReactionSpeech
			CASE 0
				iRand = GET_RANDOM_INT_IN_RANGE() % 2
				PRINTLN("iRand = ", iRand)
				
				IF iRand = 0
					IF IS_PED_ALIVE_AND_IN_BUS(args, busPed4)
						ADD_PED_FOR_DIALOGUE(args.assConv, 4, busPed4, "BusPed1")
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_DMG", CONV_PRIORITY_VERY_HIGH)
						iNumDeadMsg = iNumInnocentsDead
						iPassengerReactionSpeech++
						PRINTLN("PLAYING CONVO - OJASbs_DMG")
					ENDIF
				ELSE
					IF IS_PED_ALIVE_AND_IN_BUS(args, busPed6)
						ADD_PED_FOR_DIALOGUE(args.assConv, 6, busPed6, "BusPed3")
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_DMG3", CONV_PRIORITY_VERY_HIGH)
						iNumDeadMsg = iNumInnocentsDead
						iPassengerReactionSpeech++
						PRINTLN("PLAYING CONVO - OJASbs_DMG3")
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF iNumInnocentsDead > iNumDeadMsg
					IF IS_PED_ALIVE_AND_IN_BUS(args, busPed5)
						ADD_PED_FOR_DIALOGUE(args.assConv, 5, busPed5, "BusPed2")
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_DMG2", CONV_PRIORITY_VERY_HIGH)
						iNumDeadMsg = iNumInnocentsDead
						iPassengerReactionSpeech++
					ENDIF
				ENDIF
			BREAK
			CASE 3
				
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


//manage the bus ped dialogue when the player damages the bus or runs people over
PROC MANAGE_PASSENGER_EVENT_DIALOGUE(ASS_ARGS& args)
//	INT iRound
	
	IF NOT IS_MESSAGE_BEING_DISPLAYED()
		IF IS_TIMER_STARTED(speechTimerPassenger)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
						IF iNumStopsVisited <> 0   //player has already visited a bus stop and there are passengers on the bus
//							iRound = ROUND(GET_TIMER_IN_SECONDS(speechTimerPassenger))
//							
//							IF iRound % 2 = 0
								MANAGE_BUS_PED_SPEECH_FOR_DAMAGED_BUS(args)
//							ELSE
								MANAGE_BUS_PED_SPEECH_FOR_HITTING_PEDS(args)
//							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			START_TIMER_NOW(speechTimerPassenger)		
		ENDIF
	ENDIF
ENDPROC

//player and target are involved in foot chase
PROC MANAGE_FOOT_CHASE_DIALOGUE(ASS_ARGS& args)
	IF NOT bBailMsgPlayed
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myVehicle) < 20
					CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_BAIL", CONV_PRIORITY_VERY_HIGH)
				ENDIF
			ENDIF
			bBailMsgPlayed = TRUE
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(args.myTarget)
			IF NOT IS_PED_IN_ANY_VEHICLE(args.myTarget)
				IF GET_TIMER_IN_SECONDS(speechTimerFranklin) >= iFranklinSpeechTime
					IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 50
					AND GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) > 10
						SWITCH iFootChaseSpeech
							CASE 0
								ADD_PED_FOR_DIALOGUE(args.assConv, 3, args.myTarget, "INVESTOR")
								CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_FOOT2", CONV_PRIORITY_VERY_HIGH)
							BREAK
							CASE 1
								IF NOT bCutsceneSkipped
									IF GET_TIMER_IN_SECONDS(speechTimerFranklin) >= iFranklinSpeechTime
										CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_FOOT3", CONV_PRIORITY_VERY_HIGH)
									ENDIF
								ENDIF
							BREAK
							CASE 2
								IF GET_TIMER_IN_SECONDS(speechTimerFranklin) >= iFranklinSpeechTime
									CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_FOOT4", CONV_PRIORITY_VERY_HIGH)
								ENDIF
							BREAK
							CASE 3
								IF GET_TIMER_IN_SECONDS(speechTimerFranklin) >= iFranklinSpeechTime
									CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_FOOT5", CONV_PRIORITY_VERY_HIGH)
								ENDIF
							BREAK
							CASE 4
								IF GET_TIMER_IN_SECONDS(speechTimerFranklin) >= iFranklinSpeechTime
									CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_FOOT6", CONV_PRIORITY_VERY_HIGH)
								ENDIF
							BREAK
						ENDSWITCH
						
						RESTART_TIMER_NOW(speechTimerFranklin)
						iFootChaseSpeech++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//Manage all bus dialogue for mission
PROC MANAGE_BUS_DIALOGUE(ASS_ARGS& args)
	FLOAT fBusSpeed
	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF IS_TIMER_STARTED(speechDelayTimer)
			IF GET_TIMER_IN_SECONDS(speechDelayTimer) > 5
				IF bTargetFleeing
					IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
							fBusSpeed = GET_ENTITY_SPEED(args.myVehicle) 
							IF fBusSpeed > 10
								IF NOT IS_PED_INJURED(args.myTarget)
									IF GET_TIMER_IN_SECONDS(speechTimerFranklin) >= iFranklinSpeechTime
									AND GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) <= 50
										IF IS_VEHICLE_DRIVEABLE(args.myOtherVehicle)
											IF IS_PED_IN_VEHICLE(args.myTarget, args.myOtherVehicle)
												//play lines that Franklin says while chasing the target
												MANAGE_FRANKLIN_CHASE_SPEECH_IN_BUS(args)
											ELSE
												IF NOT bOffBikeMsgPlayed
													//play the line "my shift is up bitches"
													PLAY_SINGLE_LINE_FROM_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_FOOT", "OJASbs_FOOT_1", CONV_PRIORITY_VERY_HIGH)
													RESTART_FRANKLIN_SPEECH_TIMER_AND_ADVANCE_SPEECH_STAGE(3, 12)
													bOffBikeMsgPlayed = TRUE
												ENDIF
											ENDIF
										ENDIF
									ELIF GET_TIMER_IN_SECONDS(speechTimerPassenger) >= iPassengerSpeechTime
										//play lines of passengers panicking
										MANAGE_PASSENGER_CHASE_DIALOGUE(args)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							//play lines of Franklin yelling at the target while chasing on foot
							MANAGE_FOOT_CHASE_DIALOGUE(args)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			RESTART_TIMER_NOW(speechDelayTimer)
		ENDIF
		
		//play lines for when the player runs over peds or damages the bus
		MANAGE_PASSENGER_EVENT_DIALOGUE(args)
	ENDIF
ENDPROC

////throttles peds leaving vehicle. they should exit one at a time when the bus is travling at a slow enough speed for the ped to exit safely
//PROC TASK_BUS_PEDS_TO_EXIT_VEHICLE_WHEN_SAFE(ASS_ARGS& args)
//	PED_INDEX tempPed
//	INT tempInt
//	SEQUENCE_INDEX tempSeq
//	
//	IF NOT IS_TIMER_STARTED(passengerExitTimer)
//		START_TIMER_NOW(passengerExitTimer)
//	ELIF GET_TIMER_IN_SECONDS(passengerExitTimer) > 2
//		IF NOT bPassengersLeftBus
//		
//			IF bBailMsgPlayed
//			OR IS_PED_INJURED(args.myTarget)
//				IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
//					IF GET_ENTITY_SPEED(args.myVehicle) <= 2
//						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
//							FOR tempInt = 0 to NUM_BUS_SEATS - 1
//								tempPed = GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, tempInt))
//								IF tempPed <> NULL
//								
//									SET_PED_DESIRED_MOVE_BLEND_RATIO(tempPed, PEDMOVEBLENDRATIO_RUN)
//									SET_PED_KEEP_TASK(tempPed, TRUE)
//								
//									IF NOT IS_PED_INJURED(tempPed)
//										CLEAR_SEQUENCE_TASK(tempSeq)
//										OPEN_SEQUENCE_TASK(tempSeq)
//											TASK_LEAVE_VEHICLE(NULL, args.myVehicle)
//											TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
//										CLOSE_SEQUENCE_TASK(tempSeq)
//										TASK_PERFORM_SEQUENCE(tempPed, tempSeq)
//										CLEAR_SEQUENCE_TASK(tempSeq)
//									ENDIF
//									
//									PRINTLN("TASKING PASSENGERS TO LEAVE THE BUS - SAFE")
//									
//									//exit out since we want to stagger guys leaving and not all at once
//									RESTART_TIMER_NOW(passengerExitTimer)
//									EXIT	
//								ENDIF
//								
//								//flip the bool to no longer check this once we've gone through all the seats
//								IF tempInt = NUM_BUS_SEATS - 1
//									bPassengersLeftBus = TRUE
//								ENDIF
//							ENDFOR
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

//PROC TASK_BUS_PEDS_TO_EXIT_VEHICLE(ASS_ARGS& args)
//	PED_INDEX pedBusPassengers
//	INT tempInt
//	SEQUENCE_INDEX tempSeq
//	
//	PRINTLN("INSIDE - TASK_BUS_PEDS_TO_EXIT_VEHICLE")	
//	
//	FOR tempInt = 0 to NUM_BUS_SEATS - 1
//		IF NOT IS_ENTITY_DEAD(args.myVehicle)
//		pedBusPassengers = GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, tempInt))
//			
//			IF pedBusPassengers <> NULL
//				IF NOT IS_ENTITY_DEAD(pedBusPassengers)
//					SET_PED_DESIRED_MOVE_BLEND_RATIO(pedBusPassengers, PEDMOVEBLENDRATIO_RUN)
//					SET_PED_KEEP_TASK(pedBusPassengers, TRUE)
//				
//					IF NOT IS_PED_INJURED(pedBusPassengers)
//						CLEAR_SEQUENCE_TASK(tempSeq)
//						OPEN_SEQUENCE_TASK(tempSeq)
//							TASK_LEAVE_VEHICLE(NULL, args.myVehicle)
//							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
//						CLOSE_SEQUENCE_TASK(tempSeq)
//						TASK_PERFORM_SEQUENCE(pedBusPassengers, tempSeq)
//						CLEAR_SEQUENCE_TASK(tempSeq)
//					ENDIF
//					
////					PRINTLN("TASKING PASSENGERS TO JUMP OUT")
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDFOR
//	
//ENDPROC

//PROC SET_BUS_PASSENGERS_TO_HATE_PLAYER(ASS_ARGS& args)
//	INT tempInt
//	PED_INDEX tempPed
//	
//	IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
//		REPEAT NUM_BUS_SEATS tempInt
//			tempPed = GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, tempInt))
//			IF tempPed <> NULL
//				IF NOT IS_PED_INJURED(tempPed)
//					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_LEAVE_VEHICLES, FALSE)
//					SET_PED_RELATIONSHIP_GROUP_HASH(tempPed, RELGROUPHASH_HATES_PLAYER)
//					PRINTLN("PED IN BUS SEAT ", tempInt, "HATES PLAYER AND IS SET TO COMBAT THEM")
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
//ENDPROC

//PROC MAKE_BUS_PEDS_LEAVE_BUS(VEHICLE_INDEX veh, BOOL bOneAtATime = TRUE)
//	INT tempInt
//	PED_INDEX tempPed
//	SEQUENCE_INDEX tempSeq
//	
//	IF IS_VEHICLE_DRIVEABLE(veh)
//		IF GET_ENTITY_SPEED(veh) < 5
//			FOR tempInt = 0 to NUM_BUS_SEATS - 1
//				tempPed = GET_PED_IN_VEHICLE_SEAT(veh, INT_TO_ENUM(VEHICLE_SEAT, tempInt))
//				IF tempPed <> NULL
//					IF NOT IS_PED_INJURED(tempPed)
//						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_LEAVE_VEHICLES, TRUE)
//						RESTART_TIMER_NOW(passengerExitTimer)
//						IF bOneAtATime
//							EXIT
//						ELSE
//							OPEN_SEQUENCE_TASK(tempSeq)
//								TASK_LEAVE_ANY_VEHICLE(NULL)
//								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
//							CLOSE_SEQUENCE_TASK(tempSeq)
//							TASK_PERFORM_SEQUENCE(tempPed, tempSeq)
//							CLEAR_SEQUENCE_TASK(tempSeq)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDFOR
//		ENDIF
//	ENDIF
//ENDPROC


PROC TASK_PLAYER_TO_KILL_TARGET(ASS_ARGS& args, BOOL bDisplayEvenIfConversationOngoing = FALSE)
	IF NOT DOES_BLIP_EXIST(args.myTargetBlip)
		IF NOT IS_PED_INJURED(args.myTarget)
			IF bDisplayEvenIfConversationOngoing
			OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				REMOVE_BLIP(args.myTargetBlip)
				REMOVE_BUS_STOP_BLIPS()
				IF DOES_BLIP_EXIST(args.mytargetblip)
					REMOVE_BLIP(args.mytargetblip)
				ENDIF
				args.myTargetBlip = ADD_BLIP_FOR_ENTITY(args.myTarget)
				PRINT_NOW("ASS_BS_KILL", DEFAULT_GOD_TEXT_TIME, 1)
				RESTART_ALL_SPEECH_TIMERS()
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//PURPOSE:  Controls Rubberbanding on the Target's Vehicle when fleeing the player on his bike
PROC ADJUST_TARGETS_VEHICLE_PLAYBACK_SPEED(PED_INDEX pedToCheck, VEHICLE_INDEX vehToAdjust)
	FLOAT fDistBetweenPlayerAndAI //, fSpeed
	INT iCurRecordingProgress
	
	IF NOT IS_TIMER_STARTED(playbackTimer)
		START_TIMER_NOW(playbackTimer)
	ENDIF
	
	// ONLY UPDATE EVERY OTHER SECOND
	IF GET_TIMER_IN_SECONDS(playbackTimer) > 1
		fDistBetweenPlayerAndAI = GET_PLAYER_DISTANCE_FROM_ENTITY(pedToCheck)
	
		IF DOES_ENTITY_EXIST(pedToCheck) AND IS_VEHICLE_DRIVEABLE(vehToAdjust)
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehToAdjust)
				// Check if the player and target are far away
				
				
				//FOR REFERENCE - last time I checked (6/25/13) fBikeNormalSpeed = 22.916668
				
				IF fDistBetweenPlayerAndAI > 150
					fDesiredBikeSpeed = 10
				ELIF fDistBetweenPlayerAndAI < 100 AND fDistBetweenPlayerAndAI > 75
					fDesiredBikeSpeed = 11
				ELIF fDistBetweenPlayerAndAI < 75 AND fDistBetweenPlayerAndAI > 50
					fDesiredBikeSpeed = 12
				ELIF fDistBetweenPlayerAndAI < 50 AND fDistBetweenPlayerAndAI > 35
					fDesiredBikeSpeed = 13
				ELIF fDistBetweenPlayerAndAI < 35
					fDesiredBikeSpeed = 14
				ENDIF

				// adjust scaling of rubberbanding based on the target's progress in their current playback
				iCurRecordingProgress = GET_VEHICLE_WAYPOINT_PROGRESS(vehToAdjust)
				
				FLOAT fCurRecordingProgress = TO_FLOAT(iCurRecordingProgress)
				FLOAT fRecordingTime = TO_FLOAT(iRecordingTime)
				
				//scale down bike desired speed even more if the vehicle is over X% through the waypoint path
				//special case to always keep the ped at at least regular speed for the jump moment through the mall so that he has enough speed to make it
				IF iBusStop = 3
				AND fCurRecordingProgress >= fRecordingTime * 0.625
				AND fCurRecordingProgress <= fRecordingTime * 0.75
					IF fDesiredBikeSpeed < fBikeNormalSpeed
						fDesiredBikeSpeed = fBikeNormalSpeed
					ENDIF
				ELSE
					IF fCurRecordingProgress > fRecordingTime * 0.75
						IF fDesiredBikeSpeed > fBikeNormalSpeed - (fBikeNormalSpeed * 0.15)
							fDesiredBikeSpeed = fBikeNormalSpeed - (fBikeNormalSpeed * 0.15)
						ENDIF
					ELIF fCurRecordingProgress > fRecordingTime * 0.5
						IF fDesiredBikeSpeed > fBikeNormalSpeed - (fBikeNormalSpeed * 0.05)
							fDesiredBikeSpeed = fBikeNormalSpeed - (fBikeNormalSpeed * 0.05)
						ENDIF
					ENDIF
				ENDIF
				
				IF fDesiredBikeSpeed <> 0
					SET_DRIVE_TASK_CRUISE_SPEED(pedToCheck, fDesiredBikeSpeed)
					
//					fSpeed = GET_ENTITY_SPEED(pedToCheck)
//					PRINTLN("fSpeed = ", fSpeed)
					
//					PRINTLN("iBusStop = ", iBusStop, " fCurRecordingProgress = ", fCurRecordingProgress, " fRecordingTime = ", fRecordingTime, " progression is: ", fCurRecordingProgress / fRecordingTime)
//					PRINTLN("TARGET'S DESIRED BIKE SPEED IS: ", fDesiredBikeSpeed)
//					PRINTLN("fDistBetweenPlayerAndAI = ", fDistBetweenPlayerAndAI)
				ENDIF
			ENDIF
		ENDIF
	ELIF GET_TIMER_IN_SECONDS(playbackTimer) >= 2.0
		RESTART_TIMER_NOW(playbackTimer)
	ENDIF
	
ENDPROC


FUNC BOOL IS_BIKE_SAFE_FOR_TARGET(ASS_ARGS& args)
	IF NOT IS_ENTITY_DEAD(args.myVehicle) AND NOT IS_ENTITY_DEAD(args.myOtherVehicle)
		IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
			IF IS_VEHICLE_DRIVEABLE(args.myOtherVehicle)
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(args.myOtherVehicle, busStop[iBusStop].vVehPos) > 5
				OR IS_ENTITY_TOUCHING_ENTITY(args.myVehicle, args.myOtherVehicle)
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Sets up a quick camera cut of a camera shot located to the left of Franklin's head
PROC SHOW_CAM_INSIDE_BUS(ASS_ARGS& args)

	IF NOT DOES_CAM_EXIST(busCam)
		busCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), 35, FALSE)
	ENDIF
	
	IF NOT DOES_CAM_EXIST(busCam2)
		busCam2 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), 34, FALSE)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
	AND NOT IS_ENTITY_DEAD(args.myVehicle)
		IF bUsingBus
			ATTACH_CAM_TO_ENTITY(busCam, args.myVehicle, <<-1.7004, 5.5826, 1.0005>>)
			POINT_CAM_AT_ENTITY(busCam, args.myVehicle, <<1.2599, 5.9690, 0.7051>>)
			PRINTLN("USING BUS CAMERA POSITIONS")
		ELIF bUsingCoach
			ATTACH_CAM_TO_ENTITY(busCam, args.myVehicle, <<-1.6144, 4.8097, 0.4117>>)
			POINT_CAM_AT_ENTITY(busCam, args.myVehicle, <<1.3280, 4.7560, -0.1711>>)
			PRINTLN("USING COACH CAMERA POSITIONS")
		ELSE
			ATTACH_CAM_TO_ENTITY(busCam, args.myVehicle, <<-1.7004, 5.5826, 1.0005>>)
			POINT_CAM_AT_ENTITY(busCam, args.myVehicle, <<1.2599, 5.9690, 0.7051>>)
			PRINTLN("USING DEFAULT BUS CAMERA POSITIONS")
		ENDIF
		
		SET_CAM_NEAR_CLIP(busCam, 0.55)
		SET_CAM_ACTIVE(busCam, TRUE)
	ENDIF
	
	CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.3)
	
	RENDER_SCRIPT_CAMS(TRUE,FALSE)
ENDPROC

PROC KILL_BUS_CUTSCENE_AND_RESTORE_PLAYER_CONTROL(ASS_ARGS& args)
	//kill cutscene cams and restore control back to the player as the target flees on his bike
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ODDJOB_EXIT_CUTSCENE()
	DO_SCREEN_FADE_IN(500)
	FREEZE_ENTITY_POSITION(args.myVehicle, FALSE)
	
	PRINTLN("SETTING bDoneFlash = FALSE")
	bDoneFlash = FALSE
	
	//no longer force loading around cutscene coords
	CLEAR_FOCUS()	
ENDPROC


FUNC PED_INDEX GET_PED_CLOSEST_TO_BUS_DOORS(VECTOR vBusEntryPoint, INT busStopInt)
	PED_INDEX ped
	FLOAT fDist
	FLOAT fClosestDist = 1000	//arbitrary high number
	INT i
	
	//find passenger closest to bus door position
	FOR i = 0 TO 2
		IF NOT IS_ENTITY_DEAD(busStop[busStopInt].piAmbPed[i])
			fDist = GET_ENTITY_DISTANCE_FROM_LOCATION(busStop[busStopInt].piAmbPed[i], vBusEntryPoint)
			IF fDist < fClosestDist
				ped = busStop[busStopInt].piAmbPed[i]
				fClosestDist = fDist
			ENDIF
		ENDIF
	ENDFOR

	RETURN ped
ENDFUNC


//position the bus passenger peds outside of the bus doors
PROC LINE_UP_PASSENGERS_OUTSIDE_BUS_DOORS(ASS_ARGS& args, INT busStopInt)
	VECTOR tempVec
	PED_INDEX pedClosest
	VECTOR vPlayer
	FLOAT fPlayer
	FLOAT fHeight
	FLOAT fTempHead
	
	fTempHead = busStop[busStopInt].fStopHead
	
	//get player pos and heading for bus door pos calculations
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
		fPlayer = GET_ENTITY_HEADING(PLAYER_PED_ID())
	ENDIF
	IF bUsingBus
		tempVec = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayer, fPlayer, << 2.519, 5.6, 0.1098 >>)
		GET_GROUND_Z_FOR_3D_COORD(tempVec, fHeight)
	ELIF bUsingCoach
		tempVec = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayer, fPlayer, << 2.519, 5.6, 0.1098 >>)
		GET_GROUND_Z_FOR_3D_COORD(tempVec, fHeight)
	ENDIF
	
	//Find ped currently closest to bus doors so that we can position them at the bus doors for the next cutscene shot.
	//In the case where the bus is not oriented correctly, grab the ped furthest away so that you don't see the closest ped warping in the cutscene
	IF busStopInt = iBusStop
		pedClosest = args.myTarget
	ELSE
		pedClosest = GET_PED_CLOSEST_TO_BUS_DOORS(vBusDoorPos, busStopInt)
	ENDIF

	//line up front ped in line outside the bus doors
	IF NOT IS_PED_INJURED(pedClosest)
		CLEAR_PED_TASKS_IMMEDIATELY(pedClosest)
		SET_ENTITY_COORDS(pedClosest, << tempVec.x, tempVec.y, fHeight >>)
		SET_ENTITY_HEADING(pedClosest, fTempHead)
	ENDIF
ENDPROC


//since bus enter/exit anims only work for the first 4 seats, we need to warp peds to seats in the back to keep the front ones free until the bus is full
PROC WARP_PASSENGERS_INTO_FREE_SEATS(ASS_ARGS& args, INT busStopInt)
	INT i
	INT iSeatNum
		
	FOR i = 0 TO 2	//only have the first 3 peds enter the bus due to limited seating on the vehicle
		IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
			IF NOT IS_PED_INJURED(busStop[busStopInt].piAmbPed[i])
				CLEAR_PED_TASKS(busStop[busStopInt].piAmbPed[i])
				
				PRINTLN("ASSIGNING SEAT DATA BASED ON iNumStopsVisited. iNumStopsVisited = ", iNumStopsVisited) 
				//assign seat data
				IF iNumStopsVisited = 1
					IF i = 0
						iSeatNum = 3
					ELIF i = 1
						iSeatNum = 4
					ELIF i = 2
						iSeatNum = 5
					ENDIF
				ELIF iNumStopsVisited = 2
					IF i = 0
						iSeatNum = 6
					ELIF i = 1
						iSeatNum = 7
					ELIF i = 2
						iSeatNum = 8
					ENDIF
				ELIF iNumStopsVisited = 3
					IF i = 0
						iSeatNum = 0
					ELIF i = 1
						iSeatNum = 1
					ELIF i = 2
						iSeatNum = 2
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_SEAT_FREE(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, iSeatNum))
					SET_PED_INTO_VEHICLE(busStop[busStopInt].piAmbPed[i], args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, iSeatNum))					
					PRINTLN("SETTING PED INTO VEHICLE")
				ELSE
					PRINTLN("UNABLE TO SET PED INTO FREE SEAT BECAUSE SEAT NUMBER ", INT_TO_ENUM(VEHICLE_SEAT, iSeatNum), " IS OCCUPIED!")
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC



//peds standing outside bus doors turn to watch the target as he flees the scene
PROC TASK_PASSENGERS_OUTSIDE_BUS_TO_LOOK_AT_TARGET(PED_INDEX pedToLookAt, INT busStopInt)
	INT i
	SEQUENCE_INDEX tempSeq
	STRING sAnimName
	
	IF NOT IS_PED_INJURED(pedToLookAt)
		FOR i = 0 TO 2
			IF NOT IS_PED_INJURED(busStop[busStopInt].piAmbPed[i])
				IF i = 0
					sAnimName = "give_me_a_break"
				ELIF i = 1
					sAnimName = "numbnuts"
				ELSE
					sAnimName = "threaten"
				ENDIF
				
				CLEAR_PED_TASKS(busStop[busStopInt].piAmbPed[i])
				OPEN_SEQUENCE_TASK(tempSeq)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedToLookAt)
					TASK_PLAY_ANIM(NULL, "misscommon@response", sAnimName)
					TASK_WANDER_STANDARD(NULL)
				CLOSE_SEQUENCE_TASK(tempSeq)
				TASK_PERFORM_SEQUENCE(busStop[busStopInt].piAmbPed[i], tempSeq)
				CLEAR_SEQUENCE_TASK(tempSeq)
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC CLOSE_BUS_DOORS(VEHICLE_INDEX busVeh, BOOL bShutInstantly = TRUE)
	IF IS_VEHICLE_DRIVEABLE(busVeh)
		SET_VEHICLE_DOOR_SHUT(busVeh, SC_DOOR_FRONT_RIGHT, bShutInstantly)
		SET_VEHICLE_DOOR_SHUT(busVeh, SC_DOOR_FRONT_LEFT, bShutInstantly)
		PRINTLN("CLOSING BUS DOORS")
	ENDIF
ENDPROC

PROC OPEN_BUS_DOORS(VEHICLE_INDEX busVeh)
	IF IS_VEHICLE_DRIVEABLE(busVeh)
		IF NOT bUsingCoach			//to fix assert about no door since there is only one for this vehicle
			SET_VEHICLE_DOOR_OPEN(busVeh, SC_DOOR_FRONT_RIGHT)
		ENDIF
		SET_VEHICLE_DOOR_OPEN(busVeh, SC_DOOR_FRONT_LEFT)
		PRINTLN("OPENING BUS DOORS")
	ENDIF
ENDPROC

PROC CUTSCENE_PREPARATION()//(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	pedOldLady = CREATE_PED(PEDTYPE_CIVFEMALE, A_F_O_GENSTREET_01, << 110.6556, -776.3679, 30.4390 >>, 346.1708)
	SET_PED_COMPONENT_VARIATION(pedOldLady, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedOldLady, INT_TO_ENUM(PED_COMPONENT,2), 1, 2, 0) //(hair)
	SET_PED_COMPONENT_VARIATION(pedOldLady, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedOldLady, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedOldLady, INT_TO_ENUM(PED_COMPONENT,8), 1, 2, 0) //(accs)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedOldLady, TRUE)
	
//	args.myOtherVehicle = CREATE_VEHICLE(targetData.otherVehEnum, busStop[iBusStop].vVehPos, busStop[iBusStop].fVehHead)
ENDPROC

//handle all necessities for bus and passengers when coming out of a cutscene
PROC READY_BUS_AND_PASSENGERS_FOR_NEXT_STAGE_AND_EXIT_CUTSCENE(ASS_ARGS& args, INT busStopInt)	
	
	bRunDistanceToStop = TRUE
	iRunAlternateStage = 0
	
	//reset this to allow player to run over a ped on the way to next stop w/o being penalized
	iNumInnocentsDead = 0	
	
	//re-init this
	iBusCutsceneStage = 0
	
	//increment num stops visited
	iNumStopsVisited ++
	PRINTLN("iNumStopsVisited = ", iNumStopsVisited)
	bGrabbedDistanceToNextStop = FALSE
	PRINTLN("SETTING - bGrabbedDistanceToNextStop = FALSE")
	
	IF iNumStopsVisited = 2
		CUTSCENE_PREPARATION()//(args, targetData)
	ENDIF

	WARP_PASSENGERS_INTO_FREE_SEATS(args, busStopInt)
	SETUP_BUS_PASSENGERS_FOR_DIALOGUE(args)
	
	KILL_BUS_CUTSCENE_AND_RESTORE_PLAYER_CONTROL(args)
	CLOSE_BUS_DOORS(args.myVehicle)
	REMOVE_STREAMVOL_FOR_BUS_STOP_CUTSCENE()
	CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
	CASCADE_SHADOWS_INIT_SESSION()
ENDPROC


PROC TASK_PED_TO_START_WAYPOINT_RECORDING_ON_BIKE(PED_INDEX pedToFlee, VEHICLE_INDEX vehToUse)
	TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedToFlee, vehToUse, "OJASbs_102", DF_SwerveAroundAllCars|DF_SteerAroundObjects|DF_SteerAroundPeds, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
	vCombatPos = << -455.28925, -326.13641, 41.22218 >>
	WAYPOINT_RECORDING_GET_NUM_POINTS("OJASbs_102", iRecordingTime)
	PRINTLN("START_TARGET_PLAYBACK_ON_BIKE: TASKING PED TO USE WAYPOINT RECORDING, BUS STOP 3")
	
	SET_ENTITY_LOAD_COLLISION_FLAG(pedToFlee, TRUE)
	fBikeNormalSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED(vehToUse)
	PRINTLN("fBikeNormalSpeed = ", fBikeNormalSpeed)
	MODIFY_VEHICLE_TOP_SPEED(vehToUse, (fBikeTopSpeedModifier * 100))	//allow the bike to achieve speeds faster than its default		
	
	SET_PED_COMBAT_ATTRIBUTES(pedToFlee, CA_LEAVE_VEHICLES, TRUE)
	SET_VEHICLE_FORWARD_SPEED(vehToUse, 1.5)
ENDPROC


PROC START_TARGET_PLAYBACK_ON_BIKE(PED_INDEX& pedToFlee, VEHICLE_INDEX& vehToUse)
	
	PRINTLN("START_TARGET_PLAYBACK_ON_BIKE BEING CALLED")

	//have target start vehicle recording
	IF IS_VEHICLE_DRIVEABLE(vehToUse)
		IF NOT IS_PED_INJURED(pedToFlee)
			IF IS_PED_IN_VEHICLE(pedToFlee, vehToUse)
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehToUse)	
					
					TASK_PED_TO_START_WAYPOINT_RECORDING_ON_BIKE(pedToFlee, vehToUse)
					
					bTargetFleeing = TRUE
				ELSE
					PRINTLN("START_TARGET_PLAYBACK_ON_BIKE: WAYPOINT RECORDING IS GOING")
				ENDIF
			ELSE
				PRINTLN("START_TARGET_PLAYBACK_ON_BIKE: PED IS NOT IN THE VEHICLE")
			ENDIF
		ELSE
			PRINTLN("START_TARGET_PLAYBACK_ON_BIKE: PED IS INJURED")
		ENDIF
	ELSE
		PRINTLN("START_TARGET_PLAYBACK_ON_BIKE: VEHICLE IS NOT DRIVEABLE")
	ENDIF
ENDPROC


//PROC RESET_BIKE_AND_TELEPORT_TARGET_NEAR_IT(PED_INDEX& pedToFlee, VEHICLE_INDEX& vehToUse)
//	VECTOR vTargetWarp
//	FLOAT fTargetHead
//	FLOAT fTargetHeight
//	
//	IF iBusStop = 0
//		vTargetWarp = << 301.6020, -758.9800, 28.3159 >>
//		fTargetHead = 340.3962
//	ELIF iBusStop = 1
//		vTargetWarp = << 361.2802, -1072.4950, 28.5595 >>
//		fTargetHead = 264.2537
//	ELIF iBusStop = 2
//		IF bBusFacingCorrectly
//			vTargetWarp = << 258.8023, -1120.3799, 28.3287 >>
//			fTargetHead = 76.3996
//		ELSE
//			vTargetWarp = << 263.0038, -1123.6873, 28.2180 >>   
//			fTargetHead = 56.2486
//		ENDIF
//	ELIF iBusStop = 3
//		IF bBusFacingCorrectly
//			vTargetWarp = << 110.2194, -775.8312, 30.4426 >>
//			fTargetHead = 77.2454
//		ELSE
//			vTargetWarp = << 116.2432, -779.7109, 30.4000 >>  
//			fTargetHead = 79.7910
//		ENDIF
//	ENDIF
//	
//	GET_GROUND_Z_FOR_3D_COORD(vTargetWarp, fTargetHeight)
//	IF fTargetHeight = 0
//		fTargetHeight = busStop[iBusStop].vVehPos.z
//	ENDIF
//	
//	IF NOT IS_PED_INJURED(pedToFlee)
//		CLEAR_AREA_OF_OBJECTS(busStop[iBusStop].vVehPos, 10)
//		CLEAR_PED_TASKS(pedToFlee)
//		SET_ENTITY_COORDS(pedToFlee, <<vTargetWarp.x, vTargetWarp.y, fTargetHeight>>)
//		SET_ENTITY_HEADING(pedToFlee, fTargetHead)
//		PRINTLN("SETTING TARGET POS AND ROTATION DATA TO << ", vTargetWarp.x, " ", vTargetWarp.y, " ", fTargetHeight, " >> , ", busStop[iBusStop].fVehHead)
//		IF IS_VEHICLE_DRIVEABLE(vehToUse)
//			SET_ENTITY_COORDS(vehToUse, busStop[iBusStop].vVehPos)
//			SET_ENTITY_HEADING(vehToUse, busStop[iBusStop].fVehHead)
//			SET_VEHICLE_ON_GROUND_PROPERLY(vehToUse)
//			SET_VEHICLE_FIXED(vehToUse)
//		ENDIF
//	ENDIF
//ENDPROC

//Look at the player's approach angle at the bus stop and reposition him for the cutscene parallel to the bus stop either facing one way or the other
PROC REPOSITION_BUS_BASED_ON_PLAYER_APPROACH_ANGLE(ASS_ARGS& args, INT busStopInt, BOOL bTask = TRUE)
	FLOAT fWarpHeight
	VECTOR vOffset = <<0,-3,0>>
	
	CLEAR_PED_TASKS(PLAYER_PED_ID())
	IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(args.myVehicle)
		VEHICLE_WAYPOINT_PLAYBACK_PAUSE(args.myVehicle)
		PRINTLN("PAUSING PLAYBACK ON WAYPOINT")
	ENDIF
	
	IF busStopInt = 0	//closest to station
		vBusWarpPos = << 308.0244, -765.3675, 29.2651 >>
		fBusWarpHead = 160.6500
		vBusPreWarpPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBusWarpPos, fBusWarpHead, vOffset)
		PRINTLN("REPOSTION: busStopInt = 0")
	ELIF busStopInt = 1	//railroad tracks
		vBusWarpPos = <<355.0645, -1064.2660, 28.3992>> 
		fBusWarpHead = 269.7723
		vBusPreWarpPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBusWarpPos, fBusWarpHead, vOffset)
		PRINTLN("REPOSTION: busStopInt = 1")
	ELIF busStopInt = 2	//near freeway
		vBusWarpPos = << 265.4390, -1125.0682, 29.2194 >>
		fBusWarpHead = 88.8280
		vBusPreWarpPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBusWarpPos, fBusWarpHead, vOffset)
		PRINTLN("REPOSTION: busStopInt = 2")
	ELIF busStopInt = 3	//through the mall escape path
		vBusWarpPos = << 115.0176, -785.3170, 31.3490 >>
		fBusWarpHead = 68.7612
		vBusPreWarpPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBusWarpPos, fBusWarpHead, vOffset)
		PRINTLN("REPOSTION: busStopInt = 3")
	ENDIF
			
	//reposition the player to the new correct location
	GET_GROUND_Z_FOR_3D_COORD(vBusWarpPos, fWarpHeight)
	SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vBusPreWarpPos)
	PRINTLN("WARPING BUS TO POSITION = ", vBusPreWarpPos)
	SET_ENTITY_HEADING(args.myVehicle, fBusWarpHead)
	SET_VEHICLE_ON_GROUND_PROPERLY(args.myVehicle)
//	FREEZE_ENTITY_POSITION(args.myVehicle, TRUE)
	SET_VEHICLE_FORWARD_SPEED(args.myVehicle, 5.0)
	SET_VEHICLE_FIXED(args.myVehicle)
	IF bUsingBus
		vBusDoorPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBusWarpPos, fBusWarpHead, << 2, 5.6201, 0.1098 >>)
		PRINTLN("USING BUS: vBusDoorPos = ", << 2, 5.6201, 0.1098 >>)
	ELIF bUsingCoach
		vBusDoorPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBusWarpPos, fBusWarpHead, << 2, 5.6201, 0.1098 >>)
		PRINTLN("USING COACH: vBusDoorPos = ", << 2, 5.6201, 0.1098 >>)
	ENDIF
	//have Franklin look at peds as they enter the bus
	IF bTask
		TASK_LOOK_AT_COORD(PLAYER_PED_ID(), vBusDoorPos, 5000)
	ENDIF
ENDPROC



/// PURPOSE:
///    Checks if the entity exists and is not dead.
/// PARAMS:
///    mEntity - the entity we are checking.
/// RETURNS:
///    True if the entity exists and is not dead.
FUNC BOOL IS_ENTITY_ALIVE(ENTITY_INDEX mEntity)
	IF DOES_ENTITY_EXIST(mEntity)
		IF NOT IS_ENTITY_DEAD(mEntity)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ASSASSINATION_PRE_REQUEST_CUTSCENE()

	PLAYER_INDEX pId = GET_PLAYER_INDEX()
	
	IF NOT IS_PLAYER_DEAD(pId)
		// Stop player control and remove anything we dont want in the area
		SET_PLAYER_CONTROL(pId, FALSE, SPC_REMOVE_EXPLOSIONS)
		SET_PLAYER_CONTROL(pId, FALSE, SPC_REMOVE_PROJECTILES)
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles entering cutscenes. Turns player control off, disables phone
///    Clears area of peds, clears wanted level, hides radar + HUD, Clears Prints + Help
///    Puts widescreen borders on, handles hiding player's weapon
///    repositions player's last vehicle, fades in
/// PARAMS:
///    vTriggerLocation - where is the cutscene taking place
///    vMoveToLocation - where do you want to move the player's last vehicle to
///    fMoveToHeading - the heading for the player's last vehicle
///    bUseWidescreenBorders - TRUE turn widescreen borders on
///    bPutPlayerWeaponAway -  TRUE handle hiding the player's weapon
///    bFadeIn = should we fade the screen in?
///    bClearPeds = do we want to clear the nearby area of peds
PROC ASSASSINATION_START_CUTSCENE_MODE(VECTOR vTriggerLocation, VECTOR vMoveToLocation, FLOAT fMoveToHeading, BOOL bUseWidescreenBorders = TRUE, BOOL bPutPlayerWeaponAway = TRUE, BOOL bFadeIn = TRUE, BOOL bClearPeds = TRUE)

	PLAYER_INDEX pId = GET_PLAYER_INDEX()
	IF IS_PLAYER_PLAYING(pId)
		PRINTSTRING("......")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING("RC_START_CUTSCENE_MODE")PRINTNL()
				
		ASSASSINATION_PRE_REQUEST_CUTSCENE()
		
		REMOVE_PLAYER_HELMET(GET_PLAYER_INDEX(), FALSE) // remove player's bike helmet if he has one
		
		IF bClearPeds = TRUE // Clear area around player
			CLEAR_AREA_OF_PEDS(GET_PLAYER_COORDS(GET_PLAYER_INDEX()), 50.0)
		ENDIF
		
		// clear the cut-scene area of projectiles and objects
		CLEAR_AREA_OF_OBJECTS(vTriggerLocation, 30.0)
		CLEAR_AREA_OF_PROJECTILES(vTriggerLocation, 30.0)
		
		// Clear the player's wanted level
		SET_PLAYER_WANTED_LEVEL(pId,0)
		SET_PLAYER_WANTED_LEVEL_NOW(pId)
		
		// Stop anyone from attacking the player
		SET_EVERYONE_IGNORE_PLAYER(pId, TRUE)
		
		// Remove radar and HUD
		DISPLAY_RADAR(FALSE)
		DISPLAY_HUD(FALSE)
		
		// Clear objective and help text
		CLEAR_PRINTS()
		CLEAR_HELP()
		
		// Widescreen borders option
		IF bUseWidescreenBorders = TRUE
			SET_WIDESCREEN_BORDERS(TRUE, 0)
		ENDIF
		 
		// Automatically put the player's weapon away
		IF bPutPlayerWeaponAway = TRUE
			mPlayerWeapon = WEAPONTYPE_INVALID
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID()) // remove player's weapon
				mPlayerWeapon = GET_SELECTED_PED_WEAPON(PLAYER_PED_ID()) // store current weapon
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),  WEAPONTYPE_UNARMED, TRUE)
			ENDIF
		ENDIF
		
		// move the player's last vehicle if it is in way of cutscene, and clear area of vehicles
		// only do this if the player isn't in the car 
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
			// if trigger location or move location is set to 0,0,0 we won't move the car
			// as this must be a special case cut-scene
			IF NOT ARE_VECTORS_EQUAL(vTriggerLocation, <<0.0,0.0,0.0>>)
			AND NOT ARE_VECTORS_EQUAL(vMoveToLocation, <<0.0,0.0,0.0>>)
				PRINTSTRING("......")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING("Repositioning the player's last vehicle")PRINTNL()
				printnl()PRINTVECTOR(vMoveToLocation)PRINTNL()
//				RESOLVE_VEHICLES_AT_MISSION_TRIGGER_WITH_LOCATION(vTriggerLocation, vMoveToLocation, fMoveToHeading)
				fMoveToHeading = fMoveToHeading
			ENDIF
		ENDIF
		
		IF bFadeIn = TRUE
//			SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles delaying a cutscene until it's loaded, the last line of dialogue has finished, and the player's car has been halted.
///    NOTE: The player's control will be disabled whilst this is called.
FUNC BOOL ASSASSINATION_IS_CUTSCENE_OK_TO_START(BOOL check_for_cutscene_loaded = TRUE, FLOAT stopping_distance = 10.0)
	
	BOOL b_cutscene_ok_to_start = TRUE // See B*649704 - we need to do all this stuff rather than return false as soon as a condition returns false
	
	REQUEST_CUTSCENE("ASS_MCS_1")
	SET_SRL_POST_CUTSCENE_CAMERA(<< 133.2094, -791.9310, 35.3551 >>, << -4.9404, 0.0000, 68.7852 >>)
	PRINTLN("REQUESTING CUT SCENE - ASS_MCS_1 - 01")
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), stopping_distance, 1)
			b_cutscene_ok_to_start = FALSE
		ENDIF
		IF NOT IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			b_cutscene_ok_to_start = FALSE
		ENDIF
	ELSE
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	ENDIF
	IF check_for_cutscene_loaded = TRUE
		IF NOT HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			b_cutscene_ok_to_start = FALSE
		ENDIF
	ENDIF
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		KILL_ANY_CONVERSATION()
		b_cutscene_ok_to_start = FALSE
	ENDIF
	IF b_cutscene_ok_to_start = FALSE
		RETURN FALSE
	ENDIF
	IF IS_SCRIPTED_CONVERSATION_ONGOING() // Double check to make sure any conversations are killed
		STOP_SCRIPTED_CONVERSATION(FALSE)
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles exiting cutscenes.
///    shows radar + HUD, Clears Help, turns player control back on, re-enables phone
///    Puts widescreen borders off, handles re-eqipping player's weapon
/// PARAMS:
///    bUseWidescreenBorders - TRUE turn widescreen borders off 
///     bReEquipPlayerWeapon -  TRUE = reequip the weapon the player had before the cut-scene
PROC ASSASSINATION_END_CUTSCENE_MODE(BOOL bUseWidescreenBorders = TRUE, BOOL  bReEquipPlayerWeapon= TRUE)
	PLAYER_INDEX pId = GET_PLAYER_INDEX()
	
	PRINTSTRING("......")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING("RC_END_CUTSCENE_MODE")PRINTNL()
	SET_EVERYONE_IGNORE_PLAYER(pId, FALSE)
	SET_PLAYER_CONTROL(pId, TRUE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	CLEAR_HELP()
	
	IF bUseWidescreenBorders = TRUE
		SET_WIDESCREEN_BORDERS(FALSE, 0)
	ENDIF
	
	IF bReEquipPlayerWeapon = TRUE
		IF mPlayerWeapon <> WEAPONTYPE_INVALID
		AND mPlayerWeapon <> WEAPONTYPE_OBJECT
		AND mPlayerWeapon <> GADGETTYPE_PARACHUTE
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID()) // put player's stored weapon back in his hand
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), mPlayerWeapon, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_OLD_LADY()
	IF NOT bTaskedOldLady
		IF NOT IS_PED_INJURED(pedOldLady)
			CLEAR_PED_TASKS(pedOldLady)
			TASK_PLAY_ANIM(pedOldLady, "oddjobs@assassinate@bus@", "looking_for_help", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			PLAY_PAIN(pedOldLady, AUD_DAMAGE_REASON_SCREAM_SHOCKED)
			PRINTLN("TASKING OLD LADY TO PLAY POST CUTSCENE ANIMATION")
			
			bTaskedOldLady = TRUE
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(pedOldLady)
			IF DO_AGGRO_CHECK(pedOldLady, NULL, aggroArgs, aggroReason, FALSE, FALSE)
				IF GET_SCRIPT_TASK_STATUS(pedOldLady, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
					CLEAR_PED_TASKS(pedOldLady)
					TASK_SMART_FLEE_PED(pedOldLady, PLAYER_PED_ID(), 1000, -1)
					PRINTLN("TASKING OLD LADY TO FLEE THE PLAYER")
				ENDIF
			ENDIF
			
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(pedOldLady) > 100
				IF NOT IS_ENTITY_ON_SCREEN(pedOldLady)
				OR IS_ENTITY_OCCLUDED(pedOldLady)
					DELETE_PED(pedOldLady)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CUTSCENE_PEDS(ASS_ARGS& args, BOOL bHide)
	IF bHide
		PRINTLN("bHide = TRUE")
	ELSE
		PRINTLN("bHide = FALSE")
	ENDIF

	SET_ENTITY_VISIBLE(args.myTarget, bHide)
	SET_ENTITY_VISIBLE(pedOldLady, bHide)
	SET_ENTITY_VISIBLE(args.myVehicle, bHide)
	SET_ENTITY_VISIBLE(args.myOtherVehicle, bHide)
//	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), bHide)
ENDPROC

PROC PLAY_TARGET_FLEE_CUTSCENE_MOCAP(PED_INDEX& pedToFlee, ASS_ARGS& args)//, ASS_TARGET_DATA& targetData)

//	PRINTLN("INSIDE - PLAY_TARGET_FLEE_CUTSCENE_MOCAP")

	SWITCH iMocapBusSceneStage
		CASE 0
//			IF TIMERA() > 2000
				IF ASSASSINATION_IS_CUTSCENE_OK_TO_START(TRUE, 3.0)
	//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					//remove the previous streamvol request
					REMOVE_STREAMVOL_FOR_BUS_STOP_CUTSCENE()
					CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
					CASCADE_SHADOWS_INIT_SESSION()
					IF NOT IS_ENTITY_DEAD(args.myVehicle)
						SET_VEHICLE_USE_PLAYER_LIGHT_SETTINGS(args.myVehicle, TRUE)
						PRINTLN("SETTING - SET_VEHICLE_USE_PLAYER_LIGHT_SETTINGS - TRUE")
					ENDIF
						
					OPEN_BUS_DOORS(args.myVehicle)
							
					// Register the player
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					// Register target 
					IF NOT IS_ENTITY_DEAD(pedToFlee)
						REGISTER_ENTITY_FOR_CUTSCENE(pedToFlee, "Mugger", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_ENTITY_MODEL(pedToFlee))
					ENDIF
					// Register old lady
					IF NOT IS_ENTITY_DEAD(pedOldLady)
						REGISTER_ENTITY_FOR_CUTSCENE(pedOldLady, "Mugging_Victim", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, A_F_O_GENSTREET_01)
					ENDIF
					// Register bus
					IF NOT IS_ENTITY_DEAD(args.myVehicle)
						IF bUsingCoach
							REGISTER_ENTITY_FOR_CUTSCENE(args.myVehicle, "Franklins_Coach", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, COACH)
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Franklins_Bus", CU_DONT_ANIMATE_ENTITY, BUS)
							PRINTLN("REGISTERING COACH BUS")
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(args.myVehicle, "Franklins_Bus", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, BUS)
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Franklins_Coach", CU_DONT_ANIMATE_ENTITY, COACH)
							PRINTLN("REGISTERING BUS")
						ENDIF
					ENDIF
					// Register bike
					IF NOT IS_ENTITY_DEAD(args.myOtherVehicle)
						REGISTER_ENTITY_FOR_CUTSCENE(args.myOtherVehicle, "Stolen_Bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, scorcher)
					ENDIF
					
					START_CUTSCENE()
					PRINTLN("STARTING CUTSCENE")
					
					ASSASSINATION_START_CUTSCENE_MODE(<< 102.7480, -782.9255, 35.7965 >>, << 102.7480, -782.9255, 35.7965 >>, 0)
					
					iMocapBusSceneStage++
				ELSE
					PRINTLN("CAN NOT START CUT SCENE!!!")
				ENDIF
//			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------------
		CASE 1
			IF IS_CUTSCENE_PLAYING()
				IF g_bShitskipAccepted
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				IF DOES_CAM_EXIST(busCam) AND IS_CAM_ACTIVE(busCam)
					SET_CAM_ACTIVE(busCam, FALSE)
					DESTROY_CAM(busCam)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					PRINTLN("DESTROYING CAM 01 - busCam")
				ELSE
					IF DOES_CAM_EXIST(busCam)
						DESTROY_CAM(busCam)
						PRINTLN("DESTROYING CAM 02 - busCam")
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedToFlee) AND NOT IS_PED_INJURED(pedToFlee)
				AND IS_VEHICLE_DRIVEABLE(args.myOtherVehicle)
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Mugger")
					AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Stolen_Bike")
//						SET_PED_INTO_VEHICLE(pedToFlee, args.myOtherVehicle)
						TASK_ENTER_VEHICLE(pedToFlee, args.myOtherVehicle)
						SET_PED_COMBAT_ATTRIBUTES(pedToFlee, CA_LEAVE_VEHICLES, FALSE)
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedToFlee, KNOCKOFFVEHICLE_HARD)
						
						TASK_PED_TO_START_WAYPOINT_RECORDING_ON_BIKE(pedToFlee, args.myOtherVehicle)
					
						bTargetFleeing = TRUE
						
						PRINTLN("PUTTING GUY ON BIKE IN MOCAP SCENE")
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Mugging_Victim")
					IF DOES_ENTITY_EXIST(pedOldLady) AND NOT IS_PED_INJURED(pedOldLady)
						PRINTLN("OLD LADY IS OKAY TO EXIT?")
						HANDLE_OLD_LADY()
					ENDIF
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND DOES_ENTITY_EXIST(args.myVehicle) AND NOT IS_ENTITY_DEAD(args.myVehicle)
						
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					
//						PRINTLN("OLD LADY IS OKAY TO EXIT?")
//						HANDLE_OLD_LADY()
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			
				PRINTLN("CUTSCENE IS NO LONGER PLAYING")

				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				ASSASSINATION_END_CUTSCENE_MODE()
		
				IF NOT IS_PED_INJURED(pedToFlee)
					IF DOES_ENTITY_EXIST(args.myOtherVehicle)
					AND IS_VEHICLE_DRIVEABLE(args.myOtherVehicle)
//						RESET_BIKE_AND_TELEPORT_TARGET_NEAR_IT(pedToFlee, vehToUse)
						PRINTLN("args.myOtherVehicle exists and driveable - setting target on it!!")
						SET_PED_INTO_VEHICLE(pedToFlee, args.myOtherVehicle)
						SET_PED_COMBAT_ATTRIBUTES(pedToFlee, CA_LEAVE_VEHICLES, FALSE)
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedToFlee, KNOCKOFFVEHICLE_HARD)
					ELSE
						IF NOT DOES_ENTITY_EXIST(args.myOtherVehicle)
							PRINTLN("args.myOtherVehicle Does not exist!!")
						ELIF NOT IS_VEHICLE_DRIVEABLE(args.myOtherVehicle)
							PRINTLN("args.myOtherVehicle Not driveable!!")
						ENDIF
						TASK_SMART_FLEE_PED(pedToFlee, PLAYER_PED_ID(), 500, -1)
						PRINTLN("TARGET FLEEING ON FOOT! - 01")
					ENDIF
				ENDIF
					
				START_TARGET_PLAYBACK_ON_BIKE(args.myTarget, args.myOtherVehicle)
								
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				ODDJOB_EXIT_CUTSCENE()
				FREEZE_ENTITY_POSITION(args.myVehicle, FALSE)
				
				CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAS_MCS1LO", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
				
				TASK_PLAYER_TO_KILL_TARGET(args, TRUE)
				
				PRINTLN("ADVANCING TO CASE 2 VIA PLAY_TARGET_FLEE_CUTSCENE_MOCAP")
				iMocapBusSceneStage++
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------------
		CASE 2
			TASK_PASSENGERS_OUTSIDE_BUS_TO_LOOK_AT_TARGET(args.myTarget, iBusStop)
			CLOSE_BUS_DOORS(args.myVehicle)
			
			IF NOT IS_ENTITY_DEAD(args.myVehicle)
				SET_VEHICLE_USE_PLAYER_LIGHT_SETTINGS(args.myVehicle, FALSE)
				PRINTLN("SETTING - SET_VEHICLE_USE_PLAYER_LIGHT_SETTINGS - FALSE")
			ENDIF
			
			missionStage = MISSION_BUS_UPDATE_BIKE
		BREAK
	ENDSWITCH
	
ENDPROC


//tell the target to leave the bike and run on foot
PROC TASK_TARGET_TO_FLEE_ON_FOOT(PED_INDEX pedToFlee)
	SEQUENCE_INDEX tempSeq
	
	IF NOT IS_ENTITY_DEAD(pedToFlee)
		IF GET_SCRIPT_TASK_STATUS(pedToFlee, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(pedToFlee, SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(pedToFlee, vCombatPos) <= 50
				CLEAR_SEQUENCE_TASK(tempSeq)
				OPEN_SEQUENCE_TASK(tempSeq)
					TASK_GO_TO_COORD_ANY_MEANS(NULL, vCombatPos, PEDMOVEBLENDRATIO_RUN, NULL)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
				CLOSE_SEQUENCE_TASK(tempSeq)
				TASK_PERFORM_SEQUENCE(pedToFlee, tempSeq)
				CLEAR_SEQUENCE_TASK(tempSeq)
			ELSE
				TASK_SMART_FLEE_PED(pedToFlee, PLAYER_PED_ID(), 500, -1)
			ENDIF
			
			PRINTLN("missionStage = MISSION_BUS_FLEE_ON_FOOT")
			missionStage = MISSION_BUS_FLEE_ON_FOOT
		ENDIF
	ENDIF
ENDPROC

//control the targets flee behavior based on their distance to the player and whether or not they are fleeing in a vehicle
PROC HANDLE_TARGET_FLEE_BEHAVIOR_ON_FOOT(ASS_ARGS& args)
//	FLOAT fPlayerDistToTarget
	
	IF NOT IS_PED_INJURED(args.myTarget)
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			fPlayerDistToTarget = GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget)
//			IF fPlayerDistToTarget < 5
//			AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED
//				//have the target engage player in fist fight if player is curently unarmed
//				IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
//				AND GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_COMBAT) <> WAITING_TO_START_TASK
//					TASK_COMBAT_PED(args.myTarget, PLAYER_PED_ID())
//					IF NOT bTargetBrawlMsg
//						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						AND NOT IS_MESSAGE_BEING_DISPLAYED()
//							CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_BRAWL", CONV_PRIORITY_VERY_HIGH)
//							bTargetBrawlMsg = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
				IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					TASK_SMART_FLEE_PED(args.myTarget, PLAYER_PED_ID(), 500, -1)
					PRINTLN("TARGET FLEEING ON FOOT! - 02")
				ENDIF
				
				//slow down the target's run speed on foot
				SET_PED_MAX_MOVE_BLEND_RATIO(args.myTarget, PEDMOVE_RUN)
//			ENDIF
		ELSE
			IF GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_SMART_FLEE_PED) <> WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(args.myTarget, SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
				TASK_SMART_FLEE_PED(args.myTarget, PLAYER_PED_ID(), 500, -1)
				PRINTLN("TARGET FLEEING ON FOOT! - 03")
			ENDIF
		ENDIF
	ENDIF
	
	TASK_PLAYER_TO_KILL_TARGET(args)
ENDPROC


PROC TARGET_FLEE_ON_BIKE(PED_INDEX pedToFlee, VEHICLE_INDEX vehToUse, ASS_ARGS& args)
	IF IS_VEHICLE_DRIVEABLE(vehToUse) 
		IF NOT IS_ENTITY_DEAD(pedToFlee) 
			IF IS_PED_IN_ANY_VEHICLE(pedToFlee)
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehToUse)
					PRINTLN("WAYPOINT RECORDING IS NOT GOING!!!!")
				
					IF GET_SCRIPT_TASK_STATUS(pedToFlee, SCRIPT_TASK_LEAVE_VEHICLE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedToFlee, SCRIPT_TASK_LEAVE_VEHICLE) <> WAITING_TO_START_TASK
						TASK_LEAVE_VEHICLE(pedToFlee, vehToUse)
						PRINTLN("TASKING TO LEAVE VEHICLE")
					ENDIF
				ELSE
					//handle rubber banding
					ADJUST_TARGETS_VEHICLE_PLAYBACK_SPEED(args.myTarget, args.myOtherVehicle)
	//				PRINTLN("WAYPOINT RECORDING IS GOING")
	//				PRINTLN("VEHICLES ESTIMATED MAX SPEED IS: ", GET_VEHICLE_ESTIMATED_MAX_SPEED(args.myOtherVehicle))
	//				PRINTLN("ACTUAL SPEED IS: ", GET_ENTITY_SPEED(args.myOtherVehicle))
				ENDIF
				
//				PRINTLN("SHOULD BE USING WAYPOINT PATH")
				
				//handle the target's bike getting stuck
				IF IS_VEHICLE_PERMANENTLY_STUCK(args.myOtherVehicle)
					IF GET_SCRIPT_TASK_STATUS(pedToFlee, SCRIPT_TASK_LEAVE_VEHICLE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedToFlee, SCRIPT_TASK_LEAVE_VEHICLE) <> WAITING_TO_START_TASK
						TASK_LEAVE_VEHICLE(pedToFlee, vehToUse)
					ENDIF
					PRINTLN("VEHICLE STUCK TIMER UP!!! TASKING TARGET TO FLEE ON FOOT")
				ENDIF
				
				TASK_PLAYER_TO_KILL_TARGET(args)
			ELSE
				IF GET_SCRIPT_TASK_STATUS(pedToFlee, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedToFlee, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
					TASK_TARGET_TO_FLEE_ON_FOOT(pedToFlee)
					PRINTLN("TARGET NOT ON SET VEHICLE")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("TARGET_FLEE_ON_BIKE - PED IS DEAD")
		ENDIF
	ELSE
		TASK_TARGET_TO_FLEE_ON_FOOT(pedToFlee)
		PRINTLN("TARGET_FLEE_ON_BIKE - VEHICLE IS NOT DRIVEABLE")
	ENDIF
ENDPROC


FUNC BOOL IS_PLAYER_SHOOTING_FROM_BUS()
	IF bPlayerInBus
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_ARMED(PLAYER_PED_ID(), ENUM_TO_INT(WF_INCLUDE_GUN))
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
				AND NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_PLAYER_INJURED_BUS_STOP_PEDS(INT& iFrame)
	PED_INDEX pedToCheck
	INT tempInt
	
	//determine which bus stop number is active
	IF busStop[0].bActive
		tempInt = 0
	ELIF busStop[1].bActive
		tempInt = 1
	ELIF busStop[2].bActive
		tempInt = 2
	ELIF busStop[3].bActive
		tempInt = 3
	ENDIF

	//only check one ped per frame
	IF iFrame = 0
		pedToCheck = busStop[tempInt].piAmbPed[0]
	ELIF iFrame = 1 
		pedToCheck = busStop[tempInt].piAmbPed[1]
	ELIF iFrame = 2
		pedToCheck = busStop[tempInt].piAmbPed[2]
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedToCheck)
		IF NOT IS_PED_INJURED(pedToCheck)
			IF DO_AGGRO_CHECK(pedToCheck, NULL, aggroArgs, aggroReason, FALSE, FALSE)
			OR IS_PLAYER_SHOOTING_FROM_BUS()
				PRINTLN("CHECKING STOP: ", tempInt)
				PRINTLN("PLAYER AGGROD BUS STOP PED")
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("HAS_PLAYER_INJURED_BUS_STOP_PEDS - PASSENGER DIED!!")
			RETURN TRUE
		ENDIF
	ENDIF

	//increment it by one - reset it if it exceeds the checks
	iFrame ++
	IF iFrame > 2
		iFrame = 0
	ENDIF
	
	RETURN FALSE
ENDFUNC


// tell the peds to enter the bus and set up their dialogue info. - busStopInt should be the bus stop number to pass in to update
PROC TASK_CLOSEST_PED_TO_BUS_TO_WALK_TO_IT(ASS_ARGS& args, INT busStopInt)

	PED_INDEX pedClosest
	
	PRINTLN("TASK_CLOSEST_PED_TO_BUS_TO_WALK_TO_IT - busStopInt = ", busStopInt)
	
	IF busStopInt = 1
		pedClosest = GET_PED_CLOSEST_TO_BUS_DOORS(<< 360.98, -1068.01, 28.53 >>, busStopInt)
	ELIF busStopInt = 3
		pedClosest = args.myTarget
	ELSE
		pedClosest = GET_PED_CLOSEST_TO_BUS_DOORS(vBusDoorPos, busStopInt)
	ENDIF

	IF DOES_ENTITY_EXIST(pedClosest) AND NOT IS_ENTITY_DEAD(pedClosest)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedClosest)

		IF busStopInt = 0
			SET_PED_MOVEMENT_CLIPSET(pedClosest, "move_m@casual@d")
			TASK_FOLLOW_NAV_MESH_TO_COORD(pedClosest, << 305.26889, -768.14148, 28.17335 >>, PEDMOVEBLENDRATIO_WALK) 
                                        FORCE_PED_MOTION_STATE(pedClosest, MS_ON_FOOT_WALK)
		ELIF busStopInt = 1
			SET_PED_MOVEMENT_CLIPSET(pedClosest, "move_m@casual@d")
			TASK_GO_STRAIGHT_TO_COORD(pedClosest, << 358.69525, -1065.93530, 28.39328 >>, PEDMOVEBLENDRATIO_WALK)
//			TASK_FOLLOW_NAV_MESH_TO_COORD(pedClosest, << 358.69525, -1065.93530, 28.39328 >>, PEDMOVEBLENDRATIO_WALK) 
//                                        FORCE_PED_MOTION_STATE(pedClosest, MS_ON_FOOT_WALK)
		ELIF busStopInt = 3
			TASK_GO_STRAIGHT_TO_COORD(pedClosest, << 111.68324, -781.50983, 30.26971 >>, PEDMOVEBLENDRATIO_WALK)	//near curb
		ELSE
			//works for these stops - leave it alone
			TASK_ENTER_VEHICLE(pedClosest, args.myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK, ECF_DONT_CLOSE_DOOR)
		ENDIF
		
		PRINTLN("TASKING GUY TO WALK TO BUS")
	ENDIF

ENDPROC

FUNC BOOL CHECK_TO_SEE_BUS_PERPENDICULAR(INT busStopInt)
	FLOAT fBusHead
	FLOAT fHeadingToTest
	BOOL bPerpendicular
	
	fBusHead = GET_ENTITY_HEADING(PLAYER_PED_ID())
	
	PRINTLN("fBusHead = ", fBusHead)
	PRINTLN("busStop[busStopInt].fStopHead = ", busStop[busStopInt].fStopHead)
	
	fHeadingToTest = (busStop[busStopInt].fStopHead - 180)
	PRINTLN("fHeadingToTest = ", fHeadingToTest)

	IF COS(ABSF(fBusHead - fHeadingToTest)) <= 0.766
		bBusFacingPerpendicular = FALSE
		bPerpendicular = FALSE
		PRINTLN("bBusFacingPerpendicular = FALSE")
	ELSE
		bBusFacingPerpendicular = TRUE
		PRINTLN("bBusFacingPerpendicular = TRUE")
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ASS_BS_BOARD")
			CLEAR_THIS_PRINT("ASS_BS_BOARD")
		ENDIF
		PRINT_HELP("ASS_HELP_01")
		bPerpendicular = TRUE
	ENDIF
	
	RETURN bPerpendicular
ENDFUNC

PROC DETERMINE_PLAYER_APPROACH_TO_STOP(INT busStopInt)
	FLOAT fBusHead
	FLOAT fStopHeadAdjusted
	VECTOR vFront
	VECTOR vBusStop
	VECTOR vPlayerLocation
	
	vPlayerLocation = GET_ENTITY_COORDS(PLAYER_PED_ID())
	fBusHead = GET_ENTITY_HEADING(PLAYER_PED_ID())
	
	PRINTLN("fBusHead = ", fBusHead)
	PRINTLN("busStop[busStopInt].fStopHead = ", busStop[busStopInt].fStopHead)
	PRINTLN("fStopHeadAdjusted = ", busStop[busStopInt].fStopHead + 180)
	PRINTLN("busStopInt = ", busStopInt)
	fStopHeadAdjusted = busStop[busStopInt].fStopHead + 180

	PRINTLN("busStop[busStopInt].vStopPos = ", busStop[busStopInt].vStopPos)
	PRINTLN("vPlayerLocation = ", vPlayerLocation)
	
	vFront = << -SIN(fBusHead), COS(fBusHead), 0>>
	vBusStop = (busStop[busStopInt].vStopPos - vPlayerLocation)
	
	VECTOR vSpeedVector = GET_ENTITY_SPEED_VECTOR(PLAYER_PED_ID())
	VECTOR vForwardVector = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
	
	IF NOT bBusFacingPerpendicular
		IF DOT_PRODUCT(vFront, vBusStop) < 0
		AND DOT_PRODUCT(vSpeedVector, vForwardVector) > 0
			bApproachBackwards = TRUE
			bBusFacingCorrectly = FALSE
			PRINTLN("DOT PRODUCT LESS THAN ZERO: bApproachBackwards = TRUE")
			PRINTLN("DOT PRODUCT 01 = ", DOT_PRODUCT(vFront, vBusStop))
		ELSE
			bApproachBackwards = FALSE
			bBusFacingCorrectly = TRUE
			PRINTLN("DOT PRODUCT GREATER THAN ZERO: bApproachBackwards = FALSE")
			PRINTLN("DOT PRODUCT 02 = ", DOT_PRODUCT(vFront, vBusStop))
		ENDIF
	ELSE
		PRINTLN("bBusFacingPerpendicular = TRUE")
	ENDIF

	IF NOT bBusFacingPerpendicular AND NOT bApproachBackwards
		IF fStopHeadAdjusted < 360
			IF fBusHead > busStop[busStopInt].fStopHead
			AND fBusHead < fStopHeadAdjusted
				bBusFacingCorrectly = FALSE
				PRINTLN("LESS THAN 360: bBusFacingCorrectly = FALSE")
			ELSE
				bBusFacingCorrectly = TRUE
				PRINTLN("LESS THAN 360: bBusFacingCorrectly = TRUE")
			ENDIF
		ELSE
			IF fBusHead < busStop[busStopInt].fStopHead
			AND fBusHead > fStopHeadAdjusted - 360
				bBusFacingCorrectly = TRUE
				PRINTLN("bBusFacingCorrectly = TRUE")
			ELSE
				bBusFacingCorrectly = FALSE
				PRINTLN("bBusFacingCorrectly = FALSE")
			ENDIF
		ENDIF
	ENDIF
		
	
	IF bBusFacingCorrectly
	AND busStopInt <> iBusStop	//target walks slower than other peds so need to allow for a longer camera shot for the bus stop cutscene
		fFirstCamCutTime = 2.0
		PRINTLN("fFirstCamCutTime = 2.0")
	ELSE
		fFirstCamCutTime = 5.0
		PRINTLN("fFirstCamCutTime = 5.0")
	ENDIF
	
	
	//HACK!! always set the target's bus stop to facing correctly to make the transition smooth into the mocap cutscene	
	IF busStopInt = iBusStop
		bBusFacingCorrectly = TRUE
		fFirstCamCutTime = 2.0
		PRINTLN("bBusFacingCorrectly = TRUE and cut time = 2.0 (target cutscene)")
	ENDIF
ENDPROC

PROC RUN_ALTERNATE_CUTSCENE(ASS_ARGS& args, INT busStopInt)

	SWITCH iRunAlternateStage
		CASE 0
			SWITCH busStopInt
				CASE 0
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(), args.myVehicle, "OJASbs01", DRIVINGMODE_STOPFORCARS, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 3.0)
					CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_WRNG", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("busStopInt = 0")
				BREAK
				CASE 1
					// NEED TO UPDATE
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(), args.myVehicle, "OJASbs02", DRIVINGMODE_STOPFORCARS, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 3.0)
					CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_WRNGa", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("busStopInt = 1")
				BREAK
				CASE 2
					// NEED TO UPDATE
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(), args.myVehicle, "OJASbs03", DRIVINGMODE_STOPFORCARS, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 3.0)
					CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_WRNGb", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("busStopInt = 2")
				BREAK
				CASE 3
					// NEED TO UPDATE
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(), args.myVehicle, "OJASbs04", DRIVINGMODE_STOPFORCARS, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 3.0)
					CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_WRNGc", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("busStopInt = 3")
				BREAK
			ENDSWITCH
			
			PRINTLN("TASKED TO FOLLOW WAYPOINT RECORDING")
		
			iRunAlternateStage++
		BREAK
		//-------------------------------------------------------------------------------------------------------
		CASE 1
			IF IS_TIMER_STARTED(busTimer)
				IF GET_TIMER_IN_SECONDS(busTimer) > 3.0
					
					// Need to switch back to normal camera
					IF iNumStopsVisited = 0
						vBusCamPos = << 298.8621, -771.5198, 29.4770 >> 
						vBusCamRot = << 8.7930, 0.0000, -52.1743 >>
					ELIF iNumStopsVisited = 1
						vBusCamPos = << 364.4333, -1071.3138, 30.1969 >> 
						vBusCamRot = << 0.8394, 0.0000, 42.3892 >>
					ELIF iNumStopsVisited = 2
						vBusCamPos = << 254.3144, -1118.1940, 30.1446 >>
						vBusCamRot = << -0.9803, 0.0000, -137.7603 >>
					ELIF iNumStopsVisited = 3
						vBusCamPos = << 104.8598, -774.3508, 31.6923 >>
						vBusCamRot = << 4.8718, -0.0000, -144.4729 >>
					ENDIF
					
					PRINTLN("vBusCamPos 01 = ", vBusCamPos)
					PRINTLN("vBusCamRot 02 = ", vBusCamRot)
					
					DETACH_CAM(busCam)
					STOP_CAM_POINTING(busCam)
					SET_CAM_COORD(busCam, vBusCamPos)
					SET_CAM_ROT(busCam, vBusCamRot)
				
					REPOSITION_BUS_BASED_ON_PLAYER_APPROACH_ANGLE(args, busStopInt)

					OPEN_BUS_DOORS(args.myVehicle)
					
					iRunAlternateStage++
				ENDIF
			ENDIF
		BREAK
		//-------------------------------------------------------------------------------------------------------
		CASE 2
		
		BREAK
	ENDSWITCH

ENDPROC

PROC REMOVE_EXISTING_BUS_PASSENGERS_ON_STOLEN_BUS(ASS_ARGS& args)

	PED_INDEX pedTemp[8]
	INT idx

	IF DOES_ENTITY_EXIST(args.myVehicle) AND NOT IS_ENTITY_DEAD(args.myVehicle)
		REPEAT 8 idx
			IF NOT IS_VEHICLE_SEAT_FREE(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, idx))
				pedTemp[idx] = GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, idx))
				PRINTLN("FOUND A PED: ", idx)
				SET_ENTITY_AS_MISSION_ENTITY(pedTemp[idx])
				
				IF DOES_ENTITY_EXIST(pedTemp[idx])
					DELETE_PED(pedTemp[idx])
					PRINTLN("DELETING PED WITH INDEX: ", idx)
				ENDIF
			ELSE
				PRINTLN("VEHICLE SEAT IS FREE: ", idx)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC



PROC MOVE_LAST_PLAYER_DRIVEN_VEHICLE_TO_FRANKLINS_SAFEHOUSE()
	IF DOES_ENTITY_EXIST(viPlayerLastVehicle)
	AND IS_VEHICLE_DRIVEABLE(viPlayerLastVehicle)
		SET_MISSION_VEHICLE_GEN_VEHICLE(viPlayerLastVehicle, <<-25.2554, -1438.2389, 29.6542>>, 2.3100)
	ENDIF
ENDPROC




// Handle the cutscene of peds getting on the bus when the player arrives at a stop - busStopInt is the bus stop numebr to update
PROC MANAGE_BUS_STOP_CUTSCENE(ASS_ARGS& args, ASS_TARGET_DATA& targetData, INT busStopInt)
	VECTOR vVehicleDeform
	PED_INDEX pedClosest
	VECTOR vBusPosition, vLookAtPos
	FLOAT fBusHeading
	
	
	//to fix bug 1517047 - Franklin seems to have very exaggerated stopping anim when bus pulls up to stop
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_PreventGoingIntoShuntInVehicleState, TRUE)
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableInVehicleActions, TRUE)
	
//	//to try and fix bug 1548986 (issues with one bus door not closing properly)
//	IF DOES_ENTITY_EXIST(pedCurrentlyEnteringBus)
//		SET_PED_RESET_FLAG(pedCurrentlyEnteringBus, PRF_DisableInVehicleActions, TRUE)
//	ENDIF


	SWITCH iBusCutsceneStage
		CASE 0
			IF busStop[busStopInt].bActive
								
				IF DOES_CAM_EXIST(camHint)
					IF IS_CAM_ACTIVE(camHint)
						SET_CAM_ACTIVE(camHint, FALSE)
					ENDIF
				ENDIF
				
				ODDJOB_ENTER_CUTSCENE()
				
				//check to see if player approached stop backwards or not
				DETERMINE_PLAYER_APPROACH_TO_STOP(busStopInt)
				
				//check to see if bus entry area is damaged
				vVehicleDeform = GET_VEHICLE_DEFORMATION_AT_POS(args.myVehicle, <<1.21, 6.15, 0.3>>)	//position just in front of bus doors
				PRINTLN("vVehicleDeform AT DOOR POS <<1.21, 6.15, 0.3>> IS: << ", vVehicleDeform.x, vVehicleDeform.y, vVehicleDeform.z, " >>")
				PRINTLN("DISPLACEMENT IS:  ", GET_DISTANCE_BETWEEN_COORDS(vVehicleDeform, <<0,0,0>>))
				IF GET_DISTANCE_BETWEEN_COORDS(vVehicleDeform, <<0,0,0>>) > 0.23	//enough displacement to appear damaged
					bBusDamaged = TRUE
					SET_VEHICLE_DOOR_BROKEN(args.myVehicle, SC_DOOR_FRONT_RIGHT, TRUE)
					SET_VEHICLE_DOOR_BROKEN(args.myVehicle, SC_DOOR_FRONT_LEFT, TRUE)
				ENDIF
				
				// CAMERAS
				
				IF iNumStopsVisited = 0
					
					//make sure to set this (just in case it never got set in MISSION_BUS_ENTER_BUS earlier (to fix bug 1062610 - 3/14/13 - MB)
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "assassin_bus_stage_drive_to_stops")
					
					MOVE_LAST_PLAYER_DRIVEN_VEHICLE_TO_FRANKLINS_SAFEHOUSE()
					
					IF bBusFacingCorrectly
						vBusCamPos = << 298.8621, -771.5198, 29.4770 >> 
						vBusCamRot = << 8.7930, 0.0000, -52.1743 >>
					ENDIF
				ELIF iNumStopsVisited = 1
					IF bBusFacingCorrectly
						vBusCamPos = << 364.4333, -1071.3138, 30.1969 >> 
						vBusCamRot = << 0.8394, 0.0000, 42.3892 >>
					ENDIF
				ELIF iNumStopsVisited = 2
					IF bBusFacingCorrectly
						vBusCamPos = <<255.5488, -1118.9313, 30.1211>> 
						vBusCamRot = <<-0.9803, 0.0000, -137.7603>>
					ENDIF
				ELIF iNumStopsVisited = 3
										
					IF bBusFacingCorrectly
						vBusCamPos = << 104.8598, -774.3508, 31.6923 >>
						vBusCamRot = << 4.8718, -0.0000, -144.4729 >>
					ENDIF
				ENDIF
				
				PRINTLN("vBusCamPos = ", vBusCamPos)
				PRINTLN("vBusCamRot = ", vBusCamRot)
				
				//create cameras that show the shot of passengers waiting on the bus
				busCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vBusCamPos, vBusCamRot)
				SET_CAM_FOV(busCam, 38)
				SET_CAM_ACTIVE(busCam, TRUE)
				
				IF NOT bBusFacingCorrectly
					ATTACH_CAM_TO_ENTITY(busCam, args.myVehicle, <<-4.4984, 8.6802, -0.3745>>)
					POINT_CAM_AT_ENTITY(busCam, args.myVehicle, <<-2.0642, 7.0391, 0.2430>>)
				ENDIF
				
				//clear any objects nearby so any nearby broken props won't fly through the scene
				CLEAR_AREA_OF_OBJECTS(busStop[busStopInt].vStopPos, 30)
				CLEAR_AREA_OF_PEDS(busStop[busStopInt].vStopPos, 30)
				REMOVE_PARTICLE_FX_IN_RANGE(busStop[busStopInt].vStopPos, 7.0)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				IF busStopInt = 0
					REMOVE_EXISTING_BUS_PASSENGERS_ON_STOLEN_BUS(args)
				ENDIF
				
				IF bBusFacingCorrectly
					//position the bus in a set location at the bus stop depending on whether or not it was approached at the correct angle
					REPOSITION_BUS_BASED_ON_PLAYER_APPROACH_ANGLE(args, busStopInt)
				ELSE
					IF busStopInt = 0	//closest to station
						vBusWarpPos = << 306.7456, -771.3451, 28.2966 >>  
						fBusWarpHead = 340.8777
					ELIF busStopInt = 1	//railroad tracks
						vBusWarpPos = << 373.7143, -1064.0206, 28.2864 >> 
						fBusWarpHead = 89.7653
					ELIF busStopInt = 2	//near freeway
						// NEED TO UPDATE
						vBusWarpPos = << 250.6179, -1126.4532, 28.2040 >> 
						fBusWarpHead = 274.4086
					ELIF busStopInt = 3	//through the mall escape path
						// NEED TO UPDATE
						vBusWarpPos = << 96.6454, -779.0001, 30.4853 >>  
						fBusWarpHead = 249.5191
					ENDIF
					
					SET_ENTITY_COORDS(args.myVehicle, vBusWarpPos)
					SET_ENTITY_HEADING(args.myVehicle, fBusWarpHead)
					SET_VEHICLE_FIXED(args.myVehicle)
					SET_VEHICLE_ON_GROUND_PROPERLY(args.myVehicle)
					SET_VEHICLE_FORWARD_SPEED(args.myVehicle, 2)
					
					vBusPosition = GET_ENTITY_COORDS(args.myVehicle)
					fBusHeading = GET_ENTITY_HEADING(args.myVehicle)
					vLookAtPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBusPosition, fBusHeading, << 2, 5.6201, 0.1098 >>)
					
					//have Franklin look at peds as they enter the bus
					TASK_LOOK_AT_COORD(PLAYER_PED_ID(), vLookAtPos, 5000)
								
					PRINTLN("bBusFacingCorrectly IS FALSE, GOING TO SHOW ALTERNATE CUTSCENE")
				ENDIF
				
				//open the bus doors
				OPEN_BUS_DOORS(args.myVehicle)
				
				//remove any floating help that may currenty exist
				CLEAR_HELP()
				
				//reset this timer used in bus stop cutscenes
				IF NOT IS_TIMER_STARTED(busTimer)
					START_TIMER_NOW(busTimer)
					PRINTLN("STARTING TIMER - busTimer")
				ELSE
					RESTART_TIMER_NOW(busTimer)
					PRINTLN("RESTARTING TIMER - busTimer")
				ENDIF
				
				bTaskedToEnterBus = FALSE
				
				// remove all the bus stop blips if the player arrives at the stop with the target
//				IF busStopInt = iBusStop
//					IF bBusFacingCorrectly
//						//initialize this
//						iBusCutsceneStage = 0
//						TASK_CLOSEST_PED_TO_BUS_TO_WALK_TO_IT(args, busStopInt)
//						PRINTLN("missionStage = MISSION_BUS_TARGET_FLEE")
//						missionStage = MISSION_BUS_TARGET_FLEE
//					ELSE
//						iBusCutsceneStage++
//						PRINTLN("iBusCutsceneStage GOING TO CASE 1 - AT TARGET STOP")
//					ENDIF
//				ELSE
					iBusCutsceneStage++
					PRINTLN("iBusCutsceneStage GOING TO CASE 1")
//				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------------
		CASE 1
					
			IF NOT bBusFacingCorrectly
				RUN_ALTERNATE_CUTSCENE(args, busStopInt)
			ENDIF
			
			IF NOT bTaskedToEnterBus
				IF bBusFacingCorrectly
//					IF IS_TIMER_STARTED(busTimer)
//						IF GET_TIMER_IN_SECONDS(busTimer) > 1.0
							//peds walk to their assigned vector outside the bus doors
							TASK_CLOSEST_PED_TO_BUS_TO_WALK_TO_IT(args, busStopInt)
							bTaskedToEnterBus = TRUE
							PRINTLN("TASK_CLOSEST_PED_TO_BUS_TO_WALK_TO_IT inside of case 1 - 01")
//						ENDIF
//					ENDIF
				ELSE
					IF IS_TIMER_STARTED(busTimer)
						IF GET_TIMER_IN_SECONDS(busTimer) > 4.0
							//peds walk to their assigned vector outside the bus doors
							TASK_CLOSEST_PED_TO_BUS_TO_WALK_TO_IT(args, busStopInt)
							bTaskedToEnterBus = TRUE
							PRINTLN("TASK_CLOSEST_PED_TO_BUS_TO_WALK_TO_IT inside of case 1 - 02")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_TIMER_STARTED(busTimer)
				IF GET_TIMER_IN_SECONDS(busTimer) > fFirstCamCutTime
//					IF NOT bBusFacingCorrectly
						IF busStopInt = iBusStop
							//initialize this
							iBusCutsceneStage = 0
							PRINTLN("BUS NOT FACING CORRECTLY: missionStage = MISSION_BUS_TARGET_FLEE")
							missionStage = MISSION_BUS_TARGET_FLEE
							BREAK
						ENDIF
//					ENDIF
				
					bTaskedToEnterBus = FALSE
				
					//cut to camera inside bus and have Franklin look at the peds entering the bus
					LINE_UP_PASSENGERS_OUTSIDE_BUS_DOORS(args, busStopInt)
					SHOW_CAM_INSIDE_BUS(args)
					
					pedClosest = GET_PED_CLOSEST_TO_BUS_DOORS(vBusDoorPos, busStopInt)
//					pedCurrentlyEnteringBus = pedClosest
					
					//play any lines for the cutscene and have the ped closest to the bus enter if the bus isn't damaged
					//if the bus is damaged, have the ped make a comment about it and refuse to enter the bus
					IF busStopInt = 0
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_NOPE", CONV_PRIORITY_VERY_HIGH)
						PLAY_SINGLE_LINE_FROM_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_NOPE", "OJASbs_NOPE_1", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						IF NOT IS_PED_INJURED(pedClosest)
							TASK_ENTER_VEHICLE(pedClosest, args.myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_WARP_ENTRY_POINT|ECF_DONT_CLOSE_DOOR)
							PRINTLN("BUS STOP 0: TASKING GUY TO ENTER BUS")
							IF DOES_ENTITY_EXIST(pedClosest)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedClosest, TRUE)
								PRINTLN("CALLING FORCE ANIMATION UPDATE ON CLOSEST PED - 1")
							ENDIF
						ENDIF
					ELIF busStopInt = 1
						IF NOT IS_PED_INJURED(pedClosest)
							TASK_ENTER_VEHICLE(pedClosest, args.myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_WARP_ENTRY_POINT|ECF_DONT_CLOSE_DOOR)
							PRINTLN("BUS STOP 1: TASKING GUY TO ENTER BUS")
							IF DOES_ENTITY_EXIST(pedClosest)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedClosest, TRUE)
								PRINTLN("CALLING FORCE ANIMATION UPDATE ON CLOSEST PED - 2")
							ENDIF
						ENDIF
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_NOPE3", CONV_PRIORITY_VERY_HIGH)
					ELIF busStopInt = 2
						IF NOT IS_PED_INJURED(pedClosest)
							TASK_ENTER_VEHICLE(pedClosest, args.myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_WARP_ENTRY_POINT|ECF_DONT_CLOSE_DOOR)
							PRINTLN("BUS STOP 2: TASKING GUY TO ENTER BUS")
							IF DOES_ENTITY_EXIST(pedClosest)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedClosest, TRUE)
								PRINTLN("CALLING FORCE ANIMATION UPDATE ON CLOSEST PED - 3")
							ENDIF
						ENDIF
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_NOPE4", CONV_PRIORITY_VERY_HIGH)
					ENDIF
					
					RESTART_TIMER_NOW(busTimer)
					iBusCutsceneStage++
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------------
		CASE 2
			IF NOT bDoneFlash
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA() 
					IF GET_TIMER_IN_SECONDS(busTimer) > 1.2
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						
						PRINTLN("bDoneFlash = TRUE")
						bDoneFlash = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			IF ( GET_TIMER_IN_SECONDS(busTimer) > 1.5 AND NOT bBusDamaged )
			OR ( bBusDamaged AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() )
				
				READY_BUS_AND_PASSENGERS_FOR_NEXT_STAGE_AND_EXIT_CUTSCENE(args, busStopInt)
		
				//restart the timer
				RESTART_TIMER_NOW(busTimer)
				
				//remove the blip from the minimap, put the target at one of the remaining stops and re-initialize data for next stage
				REMOVE_BLIP(busStop[busStopInt].blipStop)
				busStop[busStopInt].bActive = FALSE
				ACTIVATE_NEXT_BUS_STOP(targetData, args, iBusStopOrder[iNumStopsVisited])
//			ELSE
//				IF bClosingBusDoorsForCutscene = FALSE
//					IF GET_TIMER_IN_SECONDS(busTimer) > 1.0
//						SCRIPT_ASSERT("CLOSE THE BUS DOORS")
//						PRINTLN("CLOSE THE BUS DOORS")
//						CLOSE_BUS_DOORS(args.myVehicle, FALSE)
//						bClosingBusDoorsForCutscene = TRUE
//					ENDIF
//				ENDIF	
			ENDIF
		BREAK
	ENDSWITCH
	

	//Handle all the necessary data for the player skipping the cutscene
	IF iBusCutsceneStage = 1
		IF HANDLE_SKIP_CUTSCENE(iCutsceneStartTime)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(args.myVehicle)
//				SET_ENTITY_COORDS(args.myVehicle, busStop[busStopInt].vStopPos)
				SET_ENTITY_HEADING(args.myVehicle, busStop[busStopInt].fCorrectHeading)
			ENDIF
			REMOVE_BLIP(busStop[busStopInt].blipStop)
			
			IF busStopInt = iBusStop
				busStop[busStopInt].bActive = FALSE
				READY_BUS_AND_PASSENGERS_FOR_NEXT_STAGE_AND_EXIT_CUTSCENE(args, busStopInt)
				iBusCutsceneStage = 0
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				IF NOT IS_PED_INJURED(args.myTarget)
					IF IS_VEHICLE_DRIVEABLE(args.myOtherVehicle)
						SET_ENTITY_VISIBLE(args.myTarget, TRUE)
						CLEAR_PED_TASKS(args.myTarget)
						SET_ENTITY_COORDS(args.myOtherVehicle, busStop[iBusStop].vVehPos)
						SET_ENTITY_HEADING(args.myOtherVehicle, busStop[iBusStop].fVehHead)
						SET_VEHICLE_ON_GROUND_PROPERLY(args.myOtherVehicle)
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(args.myTarget, KNOCKOFFVEHICLE_HARD)
						IF NOT IS_PED_IN_VEHICLE(args.myTarget, args.myOtherVehicle)
							SET_PED_INTO_VEHICLE(args.myTarget, args.myOtherVehicle)
						ENDIF
					ENDIF
				ENDIF

				CLEAR_AREA(busStop[iBusStop].vVehPos, 30, FALSE)
				CLEAR_AREA_OF_OBJECTS(busStop[iBusStop].vVehPos, 30)
				
				TASK_PASSENGERS_OUTSIDE_BUS_TO_LOOK_AT_TARGET(args.myTarget, iBusStop)
				KILL_BUS_CUTSCENE_AND_RESTORE_PLAYER_CONTROL(args)
				TASK_PLAYER_TO_KILL_TARGET(args, TRUE)
				START_TARGET_PLAYBACK_ON_BIKE(args.myTarget, args.myOtherVehicle)
				PRINTLN("missionStage = MISSION_BUS_UPDATE_BIKE  -  CUTSCENE SKIPPED")
				missionStage = MISSION_BUS_UPDATE_BIKE
			ELSE
				READY_BUS_AND_PASSENGERS_FOR_NEXT_STAGE_AND_EXIT_CUTSCENE(args, busStopInt)			
//				PRINT_NOW("ASS_BS_OTHER", DEFAULT_GOD_TEXT_TIME, 1)
				busStop[busStopInt].bActive = FALSE
				ACTIVATE_NEXT_BUS_STOP(targetData, args, iBusStopOrder[iNumStopsVisited])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_ABANDON_DIALOGUE(ASS_ARGS& args)
	INT iRand
	
	SWITCH iPassengerAbandonSpeech
		CASE 0
			iRand = GET_RANDOM_INT_IN_RANGE() % 2
			PRINTLN("iRand = ", iRand)
			
			IF iRand = 0
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_PED_ALIVE_AND_IN_BUS(args, busPed6)
						ADD_PED_FOR_DIALOGUE(args.assConv, 6, busPed6, "BusPed3")
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_WAY3", CONV_PRIORITY_VERY_HIGH)
						PRINTLN("PLAYING THIRD ABANDON CONVO")
						iPassengerAbandonSpeech++
					ENDIF
				ENDIF
			ELSE
				IF IS_PED_ALIVE_AND_IN_BUS(args, busPed4)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						ADD_PED_FOR_DIALOGUE(args.assConv, 4, busPed4, "BusPed1")
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_WAY", CONV_PRIORITY_VERY_HIGH)
						PRINTLN("PLAYING FIRST ABANDON CONVO")
						iPassengerAbandonSpeech++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_PED_ALIVE_AND_IN_BUS(args, busPed5)
					ADD_PED_FOR_DIALOGUE(args.assConv, 5, busPed5, "BusPed2")
					CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_WAY2", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("PLAYING SECOND ABANDON CONVO")
					iPassengerAbandonSpeech++
				ENDIF
			ENDIF
		BREAK
		CASE 3
		
		BREAK
	ENDSWITCH
ENDPROC

//deblip the bus stop once visited and have bus peds enter the bus
PROC UPDATE_BUS_PEDS_AND_BLIPS(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
	INT iCurStop
	FLOAT fDistForPickup
	
	//choose next bus stop to update
	iCurStop = iBusStopOrder[iNumStopsVisited]
	
	HANDLE_STREAMVOL_LOADING_UNLOADING_FOR_CURRENT_STOP(iCurStop)
	
	// Special case for last stop
	IF iCurStop = 3
		IF NOT IS_ENTITY_DEAD(args.myVehicle) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle, TRUE )
//				IF IS_ENTITY_IN_ANGLED_AREA(args.myVehicle, <<110.972397,-784.841736,29.916990>>, <<130.084183,-791.836243,35.787785>>, 8.500000)
//				IF IS_ENTITY_IN_ANGLED_AREA(args.myVehicle, <<96.225983,-782.354553,29.558491>>, <<142.175232,-799.789612,37.994461>>, 15.000000)
				
//				fDistForPickup = (GET_ENTITY_SPEED(PLAYER_PED_ID())/BUS_SPEED_FACTOR) + BUS_STOP_PAD
//				IF GET_ENTITY_DISTANCE_FROM_LOCATION(args.myVehicle, busStop[iCurStop].vStopPos) <= fDistForPickup
//					BRING_VEHICLE_TO_HALT(args.myVehicle, 5.0, -1)
					
//					SETTIMERA(0)
//					PRINTLN("missionStage = MISSION_BUS_TARGET_FLEE")
//					missionStage = MISSION_BUS_TARGET_FLEE
//					EXIT
//				ELSE
//					PRINTLN("HAVE NOT MET DISTANCE REQUIREMENT")
//				ENDIF
				
				//check to see if the player ran into the angled area surrounding the final bus stop prop
				IF IS_ENTITY_IN_ANGLED_AREA(args.myVehicle, <<112.874443,-780.204834,29.419115>>, <<118.278450,-782.233887,34.384232>>, 2.500000)
					SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
					PRINTLN("FAILING BUS ASSASSINATION: Player rammed target's bus stop!")
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	//clear the area around the bus stop of vehicles if any exist within X radius and the point is not on screen
	IF NOT IS_SPHERE_VISIBLE(busStop[iCurStop].vStopPos, 15)
		CLEAR_AREA_OF_VEHICLES(busStop[iCurStop].vStopPos, 15)
	ENDIF
	
	IF bRunDistanceToStop
		fDistForPickup = (GET_ENTITY_SPEED(PLAYER_PED_ID())/BUS_SPEED_FACTOR) + BUS_STOP_PAD		//this is dynamic depending on the player's speed
//		PRINTLN("fDistForPickup = ", fDistForPickup)
		//distance to check if the bus is near the bus stop

		//stop this timer if running since it is only used for cutscenes
					
		IF IS_TIMER_STARTED(busTimer)
			CANCEL_TIMER(busTimer)
			PRINTLN("CANCEL_TIMER")
		ENDIF
	ENDIF
	
	// Once we've entered perpendicular to the stop, we need to leave the area and re-enter.
	IF bBusFacingPerpendicular
		bNeedToLeaveArea = TRUE
		PRINTLN("SETTING bNeedToLeaveArea = TRUE")
	ENDIF
	IF bNeedToLeaveArea
		IF NOT IS_ENTITY_DEAD(args.myVehicle)
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(args.myVehicle, busStop[iCurStop].vStopPos) >= 10
				bNeedToLeaveArea = FALSE
				PRINTLN("FAR ENOUGH AWAY - bNeedToLeaveArea = FALSE")
			ELSE
				FLOAT fTempBusPosition
				fTempBusPosition = GET_ENTITY_DISTANCE_FROM_LOCATION(args.myVehicle, busStop[iCurStop].vStopPos)
				PRINTLN("NEED TO MOVE AWAY FROM BUS STOP: DISTANCE = ", fTempBusPosition)
			ENDIF
		ENDIF
	ENDIF
	
	IF bRunDistanceToStop
		IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)					//only draw the corona if the player is in the bus
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), busStop[iCurStop].vCoronaPosition, <<5,5,LOCATE_SIZE_HEIGHT>>, TRUE)
				// Do nothing, use corona
			ENDIF
		ENDIF
	ENDIF
		
	//check to see if player has approached the bus stop
	IF NOT IS_ENTITY_DEAD(args.myVehicle)		
		//check the active blip
		IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
			IF busStop[iCurStop].bActive
				
				VECTOR vPlayerBoneCoords
				vPlayerBoneCoords = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0,0,0>>)
				
				IF GET_DISTANCE_BETWEEN_COORDS(vPlayerBoneCoords, busStop[iCurStop].vCoronaPosition) <= fDistForPickup
				AND IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
				AND STREAMVOL_HAS_LOADED(svBusStopCutscene)
				AND NOT CHECK_TO_SEE_BUS_PERPENDICULAR(iCurStop) // If we're perpendicular do not run
				AND NOT bNeedToLeaveArea // If we've entered perpendicular, we need to get far enough away to re-trigger.
					bRunDistanceToStop = FALSE
					PRINTLN("SETTING bRunDistanceToStop = FALSE")
				ENDIF
				
				IF iNumStopsVisited > 0
					
					RELEASE_AMBIENT_PEDS_IN_LOT()
					
					IF NOT bGrabbedDistanceToNextStop
						fDistanceToNextStop = GET_ENTITY_DISTANCE_FROM_LOCATION(args.myVehicle, busStop[iCurStop].vStopPos)
						PRINTLN("fDistanceToNextStop = ", fDistanceToNextStop)
						bGrabbedDistanceToNextStop = TRUE
					ENDIF
				
					IF GET_ENTITY_DISTANCE_FROM_LOCATION(args.myVehicle, busStop[iCurStop].vStopPos) > (fDistanceToNextStop + 100)
					AND GET_ENTITY_DISTANCE_FROM_LOCATION(args.myVehicle, busStop[iCurStop].vStopPos) < (fDistanceToNextStop + 200)
						HANDLE_ABANDON_DIALOGUE(args)
					ELIF GET_ENTITY_DISTANCE_FROM_LOCATION(args.myVehicle, busStop[iCurStop].vStopPos) > (fDistanceToNextStop + 200)
						PRINTLN("FAILING MISSION FOR GOING TO FAR FROM THE NEXT BUS STOP")
						SET_MISSION_FAILED(asnArgs, targetData, FAIL_ABANDONED_MISSION)
					ENDIF
				ENDIF
			ENDIF
			IF NOT bRunDistanceToStop
				MANAGE_BUS_STOP_CUTSCENE(args, targetData, iCurStop)
			
		//		IF iCurStop = 3
		//			PLAY_TARGET_FLEE_CUTSCENE_MOCAP(args.myTarget, args)
		//			
		//			PRINTLN("EARLY BREAK: missionStage = MISSION_BUS_TARGET_FLEE")
		//			missionStage = MISSION_BUS_TARGET_FLEE
		//			EXIT
		//		ELSE
		//			PRINTLN("EARLY BREAK: missionStage = MISSION_BUS_TARGET_FLEE")
		//			MANAGE_BUS_STOP_CUTSCENE(args, targetData, iCurStop)
		//		ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


//checks to the health of the bus vehicle
PROC MONITOR_PLAYER_BUS(ASS_TARGET_DATA& targetData, ASS_ARGS& args)	
	
	//we only want to monitor this until the target has been discovered
	IF missionStage < MISSION_BUS_TARGET_FLEE
		IF DOES_ENTITY_EXIST(args.myVehicle)
			
			IF NOT IS_VEHICLE_DRIVEABLE(args.myVehicle) //AND iNumStopsVisited > 0
				IF DOES_BLIP_EXIST(args.myTargetBlip)
					REMOVE_BLIP(args.myTargetBlip)
				ENDIF
				SET_MISSION_FAILED(asnArgs, targetData, FAIL_VEHICLE_DESTROYED)
				PRINTLN("FAILING BUS ASSASSINATION SCRIPT BECUASE BUS IS NO LONGER DRIVEABLE")
			ENDIF
			IF IS_VEHICLE_STUCK_TIMER_UP(args.myVehicle, VEH_STUCK_ON_ROOF, 1000)
				SET_MISSION_FAILED(asnArgs, targetData, FAIL_VEHICLE_DESTROYED)
				PRINTLN("FAILING BUS ASSASSINATION SCRIPT BECUASE BUS TIMER IS UP")
			ENDIF
			
//			IF NOT IS_VEHICLE_DRIVEABLE(args.myVehicle) AND iNumStopsVisited = 0
//				IF DOES_BLIP_EXIST(args.myTargetBlip)
//					REMOVE_BLIP(args.myTargetBlip)
//					PRINTLN("REMOVING BLIP ON BUS - BUS IS NOT DRIVEABLE BUT HAS NO PASSENGERS")
//				ENDIF
//			ENDIF
			
//			IF iNumStopsVisited > 0
				//checks to see if player abandoned bus
				IF missionStage >= MISSION_BUS_UPDATE_BUS_PEDS
					IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
							IF bPlayerInBus = TRUE
								bPlayerInBus = FALSE
								REMOVE_BUS_STOP_BLIPS()
								IF DOES_BLIP_EXIST(args.myTargetBlip)
									REMOVE_BLIP(args.myTargetBlip)
								ENDIF
								
								IF NOT DOES_BLIP_EXIST(args.myTargetBlip)
									args.myTargetBlip = CREATE_BLIP_ON_ENTITY(args.myVehicle)
								ENDIF
								
								//start the fail timer
								IF NOT IS_TIMER_STARTED(failTimer)
									START_TIMER_NOW(failTimer)
								ELSE
									RESTART_TIMER_NOW(failTimer)
								ENDIF
								
								IF NOT bReturnMsgPrinted
									PRINT_NOW("ASS_BS_RETURN", DEFAULT_GOD_TEXT_TIME, 1)
									bReturnMsgPrinted = TRUE
								ENDIF
							ENDIF
						ELSE
							IF NOT bPlayerInBus
								bPlayerInBus = TRUE
								CLEAR_PRINTS()
								CREATE_BUS_STOP_BLIPS()
								
								IF DOES_BLIP_EXIST(args.myTargetBlip)
									REMOVE_BLIP(args.myTargetBlip)
								ENDIF
								
								IF IS_TIMER_STARTED(failTimer)
									PAUSE_TIMER(failTimer)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
//			ENDIF
		ENDIF
		
		//fail the mission if the player has been out of the vehicle for X amt of time
		IF NOT bPlayerInBus
			// If we're out of the vehicle, grab the coordinates of the bus and player's position.
			IF NOT IS_ENTITY_DEAD(args.myVehicle)
				vBusPos = GET_ENTITY_COORDS(args.myVehicle)
//				PRINTLN("vBusPos = ", vBusPos)
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
//				PRINTLN("vPlayerPosition = ", vPlayerPosition)
			ENDIF
			
			IF VDIST2(vPlayerPosition, vBusPos) > BUS_ABANDON_DISTANCE
				IF NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
					PRINTLN("FAILING BUS ASSASSINATION SCRIPT BECAUSE FAIL TIMER EXPIRED (PLAYER ABANDONED THE BUS")
					SET_MISSION_FAILED(asnArgs, targetData, FAIL_TIME_EXPIRED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//returns TRUE if the Player is in a bus vehicle
FUNC BOOL IS_PLAYER_IN_VEHICLE_FOR_BUS_MISSION(ASS_ARGS& args)
	VEHICLE_INDEX tempVeh
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
			SET_ENTITY_AS_MISSION_ENTITY(args.myVehicle)
			RETURN TRUE
		ELSE
			tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF GET_ENTITY_MODEL(tempVeh) = BUS
				args.myVehicle = tempVeh
				SET_ENTITY_AS_MISSION_ENTITY(args.myVehicle)
				bUsingBus = TRUE
				PRINTLN("USING BUS - IS_PLAYER_IN_VEHICLE_FOR_BUS_MISSION")
			ELIF GET_ENTITY_MODEL(tempVeh) = COACH
				args.myVehicle = tempVeh
				SET_ENTITY_AS_MISSION_ENTITY(args.myVehicle)
				bUsingCoach = TRUE
				PRINTLN("USING COACH - IS_PLAYER_IN_VEHICLE_FOR_BUS_MISSION")
			ENDIF
		ENDIF
	ENDIF	
		
	RETURN FALSE
ENDFUNC


//return the number of innocent bus peds killed by the player
FUNC INT GET_NUM_INNOCENT_BUS_PEDS_KILLED_BY_PLAYER(INT& iCounter)
	//give the player a couple of free deaths before penalizing them for killing innocents with the bus
	IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
		iCounter++
		CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
	ENDIF
	
	RETURN iCounter
ENDFUNC


/// PURPOSE:
///    Checks to see if the player has rammed the target's bike or has approached the target on foot
/// PARAMS:
///    pedTarget - The target ped to check
///    veh - The vehicle to check
///    fDistanceForPanic - Player distance from the target to return true
/// RETURNS:
///    TRUE if the player has approached on foot or is touching teh targets bike
FUNC BOOL HAS_PLAYER_DAMAGED_TARGETS_BIKE_OR_APPROACHED_TARGET(PED_INDEX pedTarget, VEHICLE_INDEX veh, FLOAT fDistanceForPanic = 15.0)
	IF DOES_ENTITY_EXIST(veh)
		IF IS_VEHICLE_DRIVEABLE(veh)
			IF IS_PED_ON_FOOT(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(pedTarget)
					IF NOT IS_PED_INJURED(pedTarget)
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(pedTarget) < fDistanceForPanic
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), veh)
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//check to see if the player has killed the target via killing them with their bus
//tBonusTimer variable is checked in the CHECK_FOR_TARGET_DEATH function in assassin_core.sch
PROC CHECK_FOR_HIT_AND_RUN_BONUS(ASS_ARGS& args)

	IF NOT IS_TIMER_STARTED(tBonusTimer)
		RESTART_TIMER_NOW(tBonusTimer)
	ELSE
		IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
			IF DOES_ENTITY_EXIST(args.myTarget)
//				IF NOT IS_PED_INJURED(args.myTarget)
					IF DOES_ENTITY_EXIST(args.myOtherVehicle)
						IF IS_VEHICLE_DRIVEABLE(args.myOtherVehicle)
						AND IS_PED_IN_VEHICLE(args.myTarget, args.myOtherVehicle)
							IF IS_ENTITY_TOUCHING_ENTITY(args.myVehicle, args.myTarget)
							OR IS_ENTITY_TOUCHING_ENTITY(args.myVehicle, args.myOtherVehicle)
								PRINTLN("RESTARTING TIMER - tBonusTimer 1")
								RESTART_TIMER_NOW(tBonusTimer)
							ENDIF
						ELSE
							IF IS_ENTITY_TOUCHING_ENTITY(args.myVehicle, args.myTarget)
							OR IS_ENTITY_TOUCHING_ENTITY(args.myVehicle, args.myOtherVehicle)
								PRINTLN("RESTARTING TIMER - tBonusTimer 2")
								RESTART_TIMER_NOW(tBonusTimer)
							ELSE
//								PRINTLN("WE'RE NOT TOUCHING ANYTHING")
							ENDIF
						ENDIF
					ENDIF
//				ENDIF
			ENDIF
		ELSE
			PRINTLN("args.myVehicle IS NOT DRIVEABLE")
		ENDIF
	ENDIF			
ENDPROC


FUNC BOOL BUS_DO_CUSTOM_SHOP_FAIL_CHECKS()
	IF iNumStopsVisited >= 1
		IF IS_PLAYER_IN_ANY_SHOP_EXCEPT_AMMUNATION()
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_PLAYER_IN_ANY_SHOP_EXCEPT_AMMUNATION()
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

//run these panic checks while the mission is updating
PROC DO_PANIC_CHECKS_FOR_BUS_MISSION(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
//	SEQUENCE_INDEX iSeq
//	VECTOR vBikePosition
	

	IF NOT bWantedLevelSet = TRUE
		IF missionStage < MISSION_BUS_TARGET_FLEE
			//do panic checks but don't trigger panic if the player walks into them or their bus
			IF DO_AGGRO_CHECK(args.myTarget, NULL, aggroArgs, aggroReason, FALSE, FALSE)
			OR HAS_PLAYER_INJURED_BUS_STOP_PEDS(iFrameToCheck)
			OR iNumInnocentsDead >= 2
			OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			OR NOT IS_VEHICLE_DRIVEABLE(args.myVehicle)
			OR HAS_PLAYER_DAMAGED_TARGETS_BIKE_OR_APPROACHED_TARGET(args.myTarget, args.myOtherVehicle)
			
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					PRINTLN("PLAYER IS WANTED")
				ENDIF
			
				//set player wanted level if they have injured a couple of peds
				IF iNumInnocentsDead >= 2
					PRINTLN("iNumInnocentsDead >= 2")
				ENDIF
				bWantedLevelSet = TRUE
				
				//Handle how to fail or advance the mission
				IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
					IF DOES_ENTITY_EXIST(args.myTarget)
						IF NOT IS_PED_INJURED(args.myTarget)
							IF GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 30
							OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(args.myTarget, PLAYER_PED_ID())
								REMOVE_BUS_STOP_BLIPS()
								IF DOES_BLIP_EXIST(args.mytargetblip)
									REMOVE_BLIP(args.mytargetblip)
								ENDIF
								
								PRINTLN("NOT FAILING")
								
//								TASK_PLAYER_TO_KILL_TARGET(args)
								bBailMsgPlayed = TRUE	//don't play Franklin's line about his shift being up
								
								//check to see if the targets bike is ok for the target to flee on
								IF IS_BIKE_SAFE_FOR_TARGET(args)
								AND NOT IS_ENTITY_ON_SCREEN(args.myVehicle)
								AND NOT IS_ENTITY_ON_SCREEN(args.myTarget)
									SET_PED_INTO_VEHICLE(args.myTarget, args.myOtherVehicle)
									START_TARGET_PLAYBACK_ON_BIKE(args.myTarget, args.myOtherVehicle)
									PRINTLN("missionStage = MISSION_BUS_UPDATE_BIKE - GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 30")
									missionStage = MISSION_BUS_UPDATE_BIKE
								ELSE
//									IF NOT IS_ENTITY_DEAD(args.myOtherVehicle)
//										vBikePosition = GET_ENTITY_COORDS(args.myOtherVehicle)
//									ENDIF
//								
//									OPEN_SEQUENCE_TASK(iSeq)
//					                  TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vBikePosition, PEDMOVEBLENDRATIO_RUN)
//					                  TASK_ENTER_VEHICLE(NULL, args.myOtherVehicle)
//					                  TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL, args.myOtherVehicle, "OJASbs_102", 
//									  	DF_SwerveAroundAllCars|DF_SteerAroundObjects|DF_SteerAroundPeds, 0, 
//										EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
//					            	CLOSE_SEQUENCE_TASK(iSeq)
//									IF NOT IS_ENTITY_DEAD(args.myTarget)
//										TASK_PERFORM_SEQUENCE(args.myTarget, iSeq)
//									ENDIF
//									CLEAR_SEQUENCE_TASK(iSeq)
//									
//									WAYPOINT_RECORDING_GET_NUM_POINTS("OJASbs_102", iRecordingTime)
//									
//									PRINTLN("SEQUENCE TASK TARGET TO BIKE")
//									missionStage = MISSION_BUS_UPDATE_BIKE
									
									bCutsceneSkipped = TRUE
									CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_FOOT3", CONV_PRIORITY_VERY_HIGH)
									
									bTargetFleeing = TRUE
									PRINTLN("missionStage = MISSION_BUS_FLEE_ON_FOOT - GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) < 30 AND BIKE NOT SAFE TO TELEPOT PED ONTO")
									missionStage = MISSION_BUS_FLEE_ON_FOOT
								ENDIF
							ELSE
								SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
								PRINTLN("FAILING BUS ASSASSINATION: GET_PLAYER_DISTANCE_FROM_ENTITY(args.myTarget) > 30")
							ENDIF
						ENDIF
					ELSE
						SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
						PRINTLN("FAILING BUS ASSASSINATION: COVER BLOWN, KILLED BUS STOP PEDS")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT bWantedLevelSet
				IF iNumInnocentsDead >= 4
					IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						PRINTLN("SETTING WANTED LEVEL BECAUSE iNumInnocentsDead >= 4")
						SET_PLAYER_WANTED_LEVEL_NO_DROP_NOW()
						bWantedLevelSet = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PROC REMOVE_ANIMS()
//	IF NOT bRemovedAnims
//		REMOVE_ANIM_DICT("oddjobs@assassinate@bus@")
//		REMOVE_ANIM_DICT("misscommon@response")
//		PRINTLN("REMOVING ANIMS")
//		bRemovedAnims = TRUE
//	ENDIF
//ENDPROC


PROC HANDLE_POLICE_SCANNER_LINE_FOR_BUS_MISSION()
	IF NOT bPoliceScannerLinePlayed
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF NOT IS_TIMER_STARTED(tPoliceScanner)
				RESTART_TIMER_NOW(tPoliceScanner)
			ELSE
				IF TIMER_DO_ONCE_WHEN_READY(tPoliceScanner, 10.0)
					PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_ASS_BUS_01", 0.0)
					bPoliceScannerLinePlayed = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC CHECK_FOR_PLAYER_SHOOTING(ASS_ARGS& args)
	INT tempInt

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_SHOOTING(PLAYER_PED_ID())
			IF iNumStopsVisited > 0
				
				IF NOT bGrabbedSpeaker
					REPEAT NUM_BUS_SEATS tempInt
						IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
							IF GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, tempInt)) <> NULL
								pedBusPassengerShooting = GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, tempInt))
								ADD_PED_FOR_DIALOGUE(args.assConv, 7, pedBusPassengerShooting, "BusPed7")
								PRINTLN("FOUND SPEAKER")
								bGrabbedSpeaker = TRUE
							ENDIF
						ENDIF
					ENDREPEAT
				
			
					IF IS_PED_ALIVE_AND_IN_BUS(args, pedBusPassengerShooting)
						CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_SHOT", CONV_PRIORITY_VERY_HIGH)
						SET_PLAYER_WANTED_LEVEL_NO_DROP_NOW()
						PRINTLN("PLAYER SHOOTING")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_REPLAY_MARKER()
	IF NOT bSetThirdCheckpoint
		PRINTLN("missionStage = MISSION_BUS_UPDATE_BIKE")
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "assassin_bus_stage_kill_target")
		PRINTLN("SETTING CHECKPOINT 3")
		bSetThirdCheckpoint = TRUE
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_TOUCHED_RIGHT_STICK()
	
	fRightX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) 
	fRightY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	
//	PRINTLN("fRightX = ", fRightX)
//	PRINTLN("fRightY = ", fRightY)
	
	IF fRightX > 0 
	OR fRightY > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ADJUST_CAMERA(CAMERA_INDEX & camToChange, FLOAT fTimeToSwitch, VECTOR vPosition, VECTOR vRotation, FLOAT fCamFOV, BOOL bUseShake = FALSE, FLOAT fShakeValue = 0.5)
	IF DOES_CAM_EXIST(camToChange)
		IF IS_TIMER_STARTED(tPayphoneCameraTimer)
			IF GET_TIMER_IN_SECONDS(tPayphoneCameraTimer) >= fTimeToSwitch
				SET_CAM_COORD(camPayPhoneIntro01, vPosition)
				SET_CAM_ROT(camPayPhoneIntro01, vRotation)
				SET_CAM_FOV(camPayPhoneIntro01, fCamFOV)
				
				IF bUseShake
					SHAKE_CAM(camPayPhoneIntro01, "HAND_SHAKE", fShakeValue)
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_INTRO_CAMERAS()
	
	SWITCH iIntroCameraStages
		CASE 0
			IF ADJUST_CAMERA(camPayPhoneIntro01, 4.0, <<-24.8539, -110.4900, 57.5606>>, <<-1.6505, 0.0546, 82.1926>>, 35.0, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 1")
				iIntroCameraStages = 1
			ENDIF
		BREAK
		CASE 1
			IF ADJUST_CAMERA(camPayPhoneIntro01, 11.0, <<-27.3634, -109.3758, 57.7306>>, <<-6.4572, 0.0825, -115.4860>>, 40.0, TRUE, 0.3)
				PRINTLN("iIntroCameraStages = 2")
				iIntroCameraStages = 2
			ENDIF
		BREAK
		CASE 2
			IF ADJUST_CAMERA(camPayPhoneIntro01, 19.0, <<-24.5192, -111.0343, 57.4342>>, <<0.7037, -0.0769, 66.6607>>, 28.6009, TRUE, 0.3)
				PRINTLN("iIntroCameraStages = 3")
				iIntroCameraStages = 3
			ENDIF
		BREAK
		CASE 3 // Tighten the Fov on this one
			IF ADJUST_CAMERA(camPayPhoneIntro01, 27.0, <<-28.7803, -108.8278, 57.6104>>, <<-3.9667, -0.0497, -116.5466>>, 35.1220, TRUE, 0.2)
				PRINTLN("iIntroCameraStages = 4")
				iIntroCameraStages = 4
			ENDIF
		BREAK
		CASE 4
		
		BREAK
	ENDSWITCH
	
ENDPROC

PROC REMOVE_SKINNED_PHONE_PROP()
	VECTOR vPlayerPos
	
	IF NOT DOES_ENTITY_EXIST(oPayPhoneAnimated)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	IF VDIST2(vPlayerPos, vPhoneLocation) > 10000 // 100m
		IF DOES_ENTITY_EXIST(oPayPhoneAnimated)
			SET_MODEL_AS_NO_LONGER_NEEDED(P_PHONEBOX_01B_S)
			DELETE_OBJECT(oPayPhoneAnimated)
			PRINTLN("DELETING OBJECT - oPayPhoneAnimated")
		ENDIF
		
		IF DOES_ENTITY_EXIST(oPayPhone)
			SET_ENTITY_VISIBLE(oPayPhone, TRUE)
			SET_ENTITY_COLLISION(oPayPhone, TRUE)
			PRINTLN("TURNING BACK ON VISIBILITY AND COLLISION")
		ENDIF
	ENDIF
ENDPROC


PROC DO_STAGE_INTRO_CUTSCENE(ASS_ARGS& args)
	
//	PRINTLN("TIME: ", TIMERB())
//	IF STREAMVOL_HAS_LOADED(svPhoneCutscene)
//		PRINTLN("WE'RE LOADED")
//	ENDIF
	
	SWITCH payPhoneCutsceneStages
	
		CASE PAYPHONE_CUTSCENE_INIT
			REQUEST_ANIM_DICT("oddjobs@assassinate@bus@call")
			PRINTLN("REQUESTING ANIM DICTIONARY - oddjobs@assassinate@bus@call")
			REQUEST_MODEL(P_PHONEBOX_01B_S)
			PRINTLN("REQUESTING MODEL - P_PHONEBOX_01B_S")
			
			IF HAS_ANIM_DICT_LOADED("oddjobs@assassinate@bus@call") AND HAS_MODEL_LOADED(P_PHONEBOX_01B_S)
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				bContinueWithScene = TRUE
			ENDIF
			
			IF bContinueWithScene
				IF DOES_ENTITY_EXIST(oPayPhone)
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-23.959661,-110.568062,56.035263>>, <<-28.194920,-109.038986,57.127155>>, 6.000000, <<-33.4344, -110.1356, 56.1898>>, 68.6902)
					PRINTLN("REPOSITIONING VEHICLE")
					
					CLEAR_AREA(<<-26.48, -110.66, 56.0227>>, 3.0, TRUE)
					
					IF NOT IS_TIMER_STARTED(tPayphoneCameraTimer)
						START_TIMER_NOW(tPayphoneCameraTimer)
						PRINTLN("STARTING TIMER - tPayphoneCameraTimer")
					ENDIF
					
					// Hide other model
					IF DOES_ENTITY_EXIST(oPayPhone)

						SET_ENTITY_VISIBLE(oPayPhone, FALSE)
						SET_ENTITY_COLLISION(oPayPhone, FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(oPayPhone, FALSE)
						
						PRINTLN("TURNING OFF VISIBILITY AND COLLISION")
						
	//					oPayPhoneAnimated = CREATE_OBJECT(P_PHONEBOX_01B_S, <<-24.48, -105.83, 56.07>>) // Having to lower prop for some reason?! ... to fix Bug # 1352784
						oPayPhoneAnimated = CREATE_OBJECT(P_PHONEBOX_01B_S, <<-26.48, -110.66, 56.0227>>)
						SET_ENTITY_ROTATION(oPayPhoneAnimated, <<0.00, 0.00, 157.62>>)
						SET_ENTITY_COLLISION(oPayPhoneAnimated, FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(oPayPhoneAnimated, FALSE)
						
						PRINTLN("CREATING SKINNED VERSION OF PROP")
						
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
						PRINTLN("TURNING BACK ON PLAYER VISIBILITY")
					ENDIF
					
					svPhoneCutscene = STREAMVOL_CREATE_SPHERE(<<-26.48, -110.66, 56.0227>>, 350, FLAG_MAPDATA)
					PRINTLN("CREATING STREAM VOLUME - svPhoneCutscene")
					
					// Start the cutscene.
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					CLEAR_AREA_OF_PEDS(vPlayerWarpPosition, 5.0)
					
					// Position the player somewhat close.
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerWarpPosition)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerWarpHeading)
						REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
						PRINTLN("MOVING FRANKLIN TO PHONE")
					ENDIF
					
					// Create scene
					iSceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
					
					IF DOES_ENTITY_EXIST(oPayPhoneAnimated)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneId, oPayPhoneAnimated, -1)
						PRINTLN("oPayPhoneAnimated EXISTS, ATTACHING SCENE TO IT")
					ELSE
						PRINTLN("NOTHING EXISTS")
					ENDIF
					
					// Franklin Animation
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneId, "oddjobs@assassinate@bus@call", "ass_bus_call_p1", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT)
					PRINTLN("TASKING FRANKLIN TO TALK ON THE PHONE")
					
					// Phone Prop Animation
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oPayPhoneAnimated, iSceneId, "ass_bus_call_phone", "oddjobs@assassinate@bus@call", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					PRINTLN("PLAYING ANIMATION ON PAYPHONE PROP")
					
					// Create Camera
					IF NOT DOES_CAM_EXIST(camPayPhoneIntro01)
						camPayPhoneIntro01a = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCameraPosition01a, vCameraRotation01a, 36.0, TRUE)
						camPayPhoneIntro01 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCameraPosition01, vCameraRotation01, 36.0, FALSE)
						SET_CAM_ACTIVE_WITH_INTERP(camPayPhoneIntro01, camPayPhoneIntro01a, 2500)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						PRINTLN("CREATING CAMERA - camPayPhoneIntro01")
						SHAKE_CAM(camPayPhoneIntro01, "HAND_SHAKE", 0.2)
					ENDIF
					
//					// Animate Camera
//					PLAY_SYNCHRONIZED_CAM_ANIM(camPayPhoneIntro01, iSceneId, "ass_bus_call_cam", "oddjobs@assassinate@bus@call")
//					PRINTLN("PLAYING ANIMATION ON CAMERA")
					
					// Setup speakers
					ADD_PED_FOR_DIALOGUE(args.assConv, 3, NULL, "LESTER")
					ADD_PED_FOR_DIALOGUE(args.assConv, 1, PLAYER_PED_ID(), "FRANKLIN")
					
					// Turn off HUD elements
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					iNavMeshBlocking = ADD_NAVMESH_BLOCKING_OBJECT(<<-26.48, -110.66, 56.0227>>, <<3,3,3>>, 0)
				   	PRINTLN("BUS: CREATING NAVMESH BLOCKING OBJECT - iNavMeshBlocking")
				   
				   	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS | SPC_REMOVE_FIRES | SPC_REMOVE_PROJECTILES) // | SPC_LEAVE_CAMERA_CONTROL_ON)
					PRINTLN("REMOVING PLAYER CONTROL")
					DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
				   
				   	payPhoneCutsceneStages = PAYPHONE_CUTSCENE_UPDATE
					PRINTLN("BUS: GOING TO STATE - PAYPHONE_CUTSCENE_UPDATE")
				ELSE
					REQUEST_ANIM_DICT("oddjobs@assassinate@bus@call")
					PRINTLN("REQUESTING ANIM DICTIONARY - oddjobs@assassinate@bus@call")
					REQUEST_MODEL(P_PHONEBOX_01B_S)
					PRINTLN("REQUESTING MODEL - P_PHONEBOX_01B_S")
					
					IF NOT DOES_ENTITY_EXIST(oPayPhone)
						oPayPhone = GET_CLOSEST_OBJECT_OF_TYPE(vPhoneLocation, 5.0, prop_phonebox_01b)
						PRINTLN("BUS: PAYPHONE WAS NOT FOUND IN TRIGGER SCENE, FIND IN SCRIPT")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_UPDATE
			
			HANDLE_INTRO_CAMERAS()
			
			IF IS_TIMER_STARTED(tPayphoneCutsceneTimer)
				IF GET_TIMER_IN_SECONDS(tPayphoneCutsceneTimer) > 1.0
					IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
						bSkipCutscene = TRUE
						
						IF NOT IS_SCREEN_FADED_OUT()
							FADE_DOWN()
						ENDIF
						
						payPhoneCutsceneStages = PAYPHONE_CUTSCENE_SKIP
						PRINTLN("BUS: GOING TO STATE - PAYPHONE_CUTSCENE_SKIP")
						BREAK
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT bPlayedPayPhoneConvo
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.05
							IF CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJAScnt_bs", CONV_PRIORITY_VERY_HIGH)
								IF NOT IS_TIMER_STARTED(tPayphoneCutsceneTimer)
									START_TIMER_NOW(tPayphoneCutsceneTimer)
									PRINTLN("BUS: STARTING TIMER - tPayphoneCutsceneTimer")
									bPlayedPayPhoneConvo = TRUE
									
									IF IS_REPEAT_PLAY_ACTIVE()
										FADE_IN()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_TIMER_STARTED(tPayphoneCutsceneTimer)
				IF GET_TIMER_IN_SECONDS(tPayphoneCutsceneTimer) > fCutsceneLength
				OR (IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.962)
				OR bSkipCutscene
					payPhoneCutsceneStages = PAYPHONE_CUTSCENE_CLEANUP
					PRINTLN("BUS: GOING TO STATE - PAYPHONE_CUTSCENE_CLEANUP")
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_SKIP
		
			STOP_SCRIPTED_CONVERSATION(FALSE)
		
			IF bSkipCutscene
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-25.8488, -109.9882, 56.0738>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 236.2218)
						PRINTLN("MOVING FRANKLIN - SKIP CUTSCENE")
					ENDIF
					
					STOP_SYNCHRONIZED_ENTITY_ANIM (oPayPhoneAnimated, INSTANT_BLEND_OUT, TRUE)
					
					WAIT(0)
					DO_SCREEN_FADE_IN(500)
					
					DESTROY_ALL_CAMS()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
					payPhoneCutsceneStages = PAYPHONE_CUTSCENE_CLEANUP
					PRINTLN("BUS: GOING TO STATE - PAYPHONE_CUTSCENE_CLEANUP")
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------
		CASE PAYPHONE_CUTSCENE_CLEANUP
	
			IF NOT bSkipCutscene
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				DESTROY_ALL_CAMS()
			ENDIF
			
			IF STREAMVOL_IS_VALID(svPhoneCutscene)
				STREAMVOL_DELETE(svPhoneCutscene)
				PRINTLN("DELETING STREAM VOLUME - svPhoneCutscene VIA PAYPHONE_CUTSCENE_CLEANUP")
			ENDIF
			
			SET_ENTITY_COLLISION(oPayPhoneAnimated, TRUE)
			
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE) 
			DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)

			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
			//to fix Bug 1173621 - (3/12/13) - MB
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			
			REMOVE_ANIM_DICT("oddjobs@assassinate@bus@call")
			SET_OBJECT_AS_NO_LONGER_NEEDED(oPayPhone)
			
			curStage = MISSION_STATE_INIT
			PRINTLN("BUS: GOING TO STATE - MISSION_STATE_INIT")
		BREAK
	ENDSWITCH
ENDPROC

PROC REMOVE_NAVMESH_BLOCKING()
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlocking)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlocking)
		PRINTLN("BUS: REMOVING NAVMESH BLOCKING OBJECT - iNavMeshBlocking")
	ENDIF
ENDPROC

//update loop for the Bus assassination
PROC UPDATE_LOOP_BUS(ASS_TARGET_DATA& targetData, ASS_ARGS& args)	
	
	CHECK_FOR_PLAYER_SHOOTING(args)
	
	SWITCH missionStage
		
		CASE MISSION_BUS_STREAM_PEDS
			
			//remove previous destination blip and blip the bus
			IF DOES_BLIP_EXIST(args.myTargetBlip)
				REMOVE_BLIP(args.myTargetBlip)
			ENDIF
			
//			IF NOT bDoingJSkip
				IF NOT DOES_BLIP_EXIST(args.myTargetBlip)
					args.myTargetBlip = CREATE_BLIP_ON_ENTITY(args.myVehicle)
				ENDIF
//			ENDIF
			
			IF ARE_MODELS_LOADED_FOR_EARLY_BUS_STOPS(targetData)
			AND ARE_MODELS_LOADED_FOR_FINAL_BUS_STOP(targetData)
				missionStage = MISSION_BUS_CREATE_PEDS
			ENDIF
		BREAK
		
		//Blip the bus and print objective to the player to enter it.
		CASE MISSION_BUS_CREATE_PEDS
			IF ARE_MODELS_LOADED_FOR_EARLY_BUS_STOPS(targetData)
			AND ARE_MODELS_LOADED_FOR_FINAL_BUS_STOP(targetData)
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				
				INIT_BUS_STOP_DATA()
				CREATE_BUS_ROUTE(targetData, args)
				
//				//remove previous destination blip and blip the bus
//				IF DOES_BLIP_EXIST(args.myTargetBlip)
//					REMOVE_BLIP(args.myTargetBlip)
//				ENDIF
//				
//	//			IF NOT bDoingJSkip
//					IF NOT DOES_BLIP_EXIST(args.myTargetBlip)
//						args.myTargetBlip = CREATE_BLIP_ON_ENTITY(args.myVehicle)
//					ENDIF
//	//			ENDIF
				
				//tell player to enter the bus
	//			PRINT_NOW("ASS_BS_ENTER", DEFAULT_GOD_TEXT_TIME, 1)	
				
				PRINTLN("missionStage = MISSION_BUS_ENTER_BUS")
				missionStage = MISSION_BUS_ENTER_BUS
			ENDIF
		BREAK
		
		CASE MISSION_BUS_ENTER_BUS
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(args.myVehicle, PLAYER_PED_ID())
				DEBUG_MESSAGE("FAILING ASSASSINATION SCRIPT - PLAYER HARMING BUS")
				SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
				EXIT
			ENDIF
		
			IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
				IF IS_PLAYER_IN_VEHICLE_FOR_BUS_MISSION(args)
					SET_BUSES_IN_LOT_AS_NO_LONGER_NEEDED()
					
					SET_VEHICLE_MODEL_IS_SUPPRESSED(BUS, TRUE)
					
					//remove bus blip
					IF DOES_BLIP_EXIST(args.myTargetBlip)
						REMOVE_BLIP(args.myTargetBlip)
					ENDIF
					
					#IF IS_DEBUG_BUILD
//						iSkip = 1
					#ENDIF
					
					IF GET_PLAYER_DISTANCE_FROM_LOCATION(vBusLot) <= 75.0
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "assassin_bus_stage_drive_to_stops")
					ENDIF
					
					bPlayerInBus = TRUE
					
					CREATE_BUS_STOP_BLIPS()
					PRINT_NOW("ASS_BS_FIND", DEFAULT_GOD_TEXT_TIME, 1)
					SETTIMERA(0)
					
					PRINTLN("missionStage = MISSION_BUS_UPDATE_BUS_PEDS")
					missionStage = MISSION_BUS_UPDATE_BUS_PEDS
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_BUS_UPDATE_BUS_PEDS
			UPDATE_BUS_PEDS_AND_BLIPS(args, targetData)
			HANDLE_CINEMATIC_CAM(args, FALSE)
		BREAK

		//Panic stage - Player has alerted the target - task him to flee
		CASE MISSION_BUS_TARGET_FLEE
			PLAY_TARGET_FLEE_CUTSCENE_MOCAP(args.myTarget, args)
		BREAK
		
		CASE MISSION_BUS_UPDATE_BIKE
			HANDLE_POLICE_SCANNER_LINE_FOR_BUS_MISSION()
			SET_REPLAY_MARKER()
			HANDLE_OLD_LADY()
			TARGET_FLEE_ON_BIKE(args.myTarget, args.myOtherVehicle, args)
//			TASK_BUS_PEDS_TO_EXIT_VEHICLE_WHEN_SAFE(args)
//			HANDLE_CINEMATIC_CAM(args)
			CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, args.myTarget)
			UPDATE_BUS_PASSENGER_ANIMS(args)
		BREAK
		
		CASE MISSION_BUS_FLEE_ON_FOOT
			HANDLE_POLICE_SCANNER_LINE_FOR_BUS_MISSION()
			HANDLE_TARGET_FLEE_BEHAVIOR_ON_FOOT(args)
//			TASK_BUS_PEDS_TO_EXIT_VEHICLE_WHEN_SAFE(args)
			CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, args.myTarget)
		BREAK
	ENDSWITCH
	
	
	//HANDLE PANIC CHECKS
		DO_PANIC_CHECKS_FOR_BUS_MISSION(targetData, args)
	
	//DISTANCE FAIL CHECK
		IF missionStage > MISSION_BUS_TARGET_FLEE
			IF HAS_TARGET_ESCAPED(args.myTarget, 175)
				IF NOT IS_PED_INJURED(args.myTarget)
					IF NOT IS_ENTITY_ON_SCREEN(args.myTarget)
					OR IS_ENTITY_OCCLUDED(args.myTarget)
						IF DOES_BLIP_EXIST(args.myTargetBlip)
							REMOVE_BLIP(args.myTargetBlip)
						ENDIF
						PRINTLN("FAILING BUS ASSASSINATION SCRIPT BECAUSE TARGET ESCAPED DISTANCE FAIL CHECK")
						SET_MISSION_FAILED(asnArgs, targetData, FAIL_PED_ESCAPED)
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	//FAIL CHECK TO SEE IF THE BUS IS DRIVEABLE
		MONITOR_PLAYER_BUS(targetData, args)
	
	//MANAGES ALL BUS DIALOGUE/SPEECH
		MANAGE_BUS_DIALOGUE(args)
		
	//monitor number of peds that the player has innocently run over
		GET_NUM_INNOCENT_BUS_PEDS_KILLED_BY_PLAYER(iNumInnocentsDead)
		
	//check to see if the player killed the target via hit and run in the bus
		CHECK_FOR_HIT_AND_RUN_BONUS(args)
		
ENDPROC





/// PURPOSE:
///    Make sure that the player's last vehicle is set as a mission entity so that it wont get cleared up aggeressively by garbage collection while on the mission
PROC SAVE_LAST_PLAYER_VEHICLE()
	VEHICLE_INDEX vehLast
	
	IF curStage > MISSION_STATE_INIT
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vehLast = GET_LAST_DRIVEN_VEHICLE()
			
			IF DOES_ENTITY_EXIST(vehLast)
				IF IS_VEHICLE_DRIVEABLE(vehLast)
					IF vehLast != viPlayerLastVehicle
					AND GET_ENTITY_MODEL(vehLast) != BUS
					AND GET_ENTITY_MODEL(vehLast) != COACH

						//set the new vehicle as the current mission entity vehicle
						viPlayerLastVehicle = vehLast
						SET_ENTITY_AS_MISSION_ENTITY(viPlayerLastVehicle, TRUE, TRUE)
						SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(viPlayerLastVehicle, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC





PROC CHECK_FOR_PLAYER_IN_BUS(ASS_ARGS& args)
	IF iNumStopsVisited > 0
//		PRINTLN("EXITING EARLY, BECAUSE WE ALREADY FOUND A BUS AND HAVE PASSENGERS")
		EXIT
	ENDIF

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehPlayerIsIn = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
			mnCarPlayerIsIn = GET_ENTITY_MODEL(vehPlayerIsIn)
		
			IF mnCarPlayerIsIn <> DUMMY_MODEL_FOR_SCRIPT
				IF mnCarPlayerIsIn = BUS
				
					DISABLE_CELLPHONE(TRUE)
//					PRINTLN("DISABLING CELLPHONE - NOW THAT THE PLAYER IS IN A BUS")
					
					args.myVehicle = vehPlayerIsIn
					bUsingBus = TRUE
//					PRINTLN("bUsingBus = TRUE")

					SET_VEHICLE_HAS_STRONG_AXLES(args.myVehicle, TRUE)
					
//					IF DOES_ENTITY_EXIST(viPlayerLastVehicle)
//					AND IS_VEHICLE_DRIVEABLE(viPlayerLastVehicle)
//						SET_MISSION_VEHICLE_GEN_VEHICLE(viPlayerLastVehicle, <<-25.2554, -1438.2389, 29.6542>>, 2.3100)
//					ENDIF
					
					curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
				ELIF mnCarPlayerIsIn = COACH
					DISABLE_CELLPHONE(TRUE)
//					PRINTLN("DISABLING CELLPHONE - NOW THAT THE PLAYER IS IN A BUS")

					args.myVehicle = vehPlayerIsIn
					bUsingCoach = TRUE
//					PRINTLN("bUsingCoach = TRUE")
			
					SET_VEHICLE_HAS_STRONG_AXLES(args.myVehicle, TRUE)
					
//					IF DOES_ENTITY_EXIST(viPlayerLastVehicle)
//					AND IS_VEHICLE_DRIVEABLE(viPlayerLastVehicle)
//						SET_MISSION_VEHICLE_GEN_VEHICLE(viPlayerLastVehicle, <<-25.2554, -1438.2389, 29.6542>>, 2.3100)
//					ENDIF
					
					curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
				ENDIF
			ELSE
				PRINTLN("MODEL IS DUMMY_MODEL_FOR_SCRIPT")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_PLAYER_BUS_AT_TARGET_BUS_STOP(ASS_ARGS& args)
	IF DOES_ENTITY_EXIST(args.myVehicle)

//		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
//			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 118.3466, -786.1586, 30.3105 >>)
//			PRINTLN("PLAYER IS IN THE BUS, LET'S DETACH AND MOVE HIM")
//		ENDIF
		
		DELETE_VEHICLE(args.myVehicle)
		PRINTLN("DELETING VEHICLE - args.myVehicle")
	ENDIF
	
	IF bUsingCoach
		args.myVehicle = CREATE_VEHICLE(COACH, << 118.3466, -786.1586, 30.3105 >>, 68.7988)
		PRINTLN("CREATING VEHICLE - args.myVehicle - USING COACH MODEL")
		SET_VEHICLE_ON_GROUND_PROPERLY(args.myVehicle)
		SET_VEHICLE_COLOUR_COMBINATION(args.myVehicle, 0)
	ELSE
		args.myVehicle = CREATE_VEHICLE(BUS, << 118.3466, -786.1586, 30.3105 >>, 68.7988)
		PRINTLN("DEFAULT: CREATING VEHICLE - args.myVehicle - USING BUS MODEL")
		SET_VEHICLE_ON_GROUND_PROPERLY(args.myVehicle)
		SET_VEHICLE_COLOUR_COMBINATION(args.myVehicle, 0)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
		SET_ENTITY_COORDS(args.myVehicle, << 118.3466, -786.1586, 30.3105 >>)
		SET_ENTITY_HEADING(args.myVehicle, 68.7988)
//		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
	ENDIF
ENDPROC

PROC GO_TO_CHECKPOINT_1(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	
	UNUSED_PARAMETER(targetData)
	UNUSED_PARAMETER(args)
		
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
			REMOVE_FILLER_BUSES()
			REMOVE_ALL_ENTITIES_IN_ASS_STRUCT(assArgs)
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -22.4026, -110.8498, 56.0027 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 187.4018)
			PRINTLN("Bus Assassination - SETTING PLAYER COORDS TO << -22.4026, -110.8498, 56.0027 >>")
			
			IF DOES_ENTITY_EXIST(args.myVehicle)
				DELETE_VEHICLE(args.myVehicle)
				PRINTLN("CHECKPOINT 1: DELETING VEHICLE - args.myVehicle")
			ENDIF
			
			IF DOES_BLIP_EXIST(args.myTargetBlip)
				REMOVE_BLIP(args.myTargetBlip)
				PRINTLN("bDoingPSkip: REMOVING BLIP - args.myTargetBlip")
			ENDIF
			IF DOES_BLIP_EXIST(busStop[0].blipStop)
				REMOVE_BLIP(busStop[0].blipStop)
				PRINTLN("bDoingPSkip: REMOVING BLIP - busStop[0].blipStop")
			ENDIF
			IF DOES_BLIP_EXIST(busStop[1].blipStop)
				REMOVE_BLIP(busStop[1].blipStop)
				PRINTLN("bDoingPSkip: REMOVING BLIP - busStop[1].blipStop")
			ENDIF
			IF DOES_BLIP_EXIST(busStop[2].blipStop)
				REMOVE_BLIP(busStop[2].blipStop)
				PRINTLN("bDoingPSkip: REMOVING BLIP - busStop[2].blipStop")
			ENDIF
			IF DOES_BLIP_EXIST(busStop[3].blipStop)
				REMOVE_BLIP(busStop[3].blipStop)
				PRINTLN("bDoingPSkip: REMOVING BLIP - busStop[3].blipStop")
			ENDIF
			
//			iSkip = 0
			iNumStopsVisited = 0 
			
//			bFirstCheckpointSceneLoad = FALSE
			bGrabbedDistanceToNextStop = FALSE
			
			CREATE_ASSASSINATION_SCENE(targetData, assArgs)
			
			FILL_LOT_WITH_ADDITIONAL_BUSES(targetData)
			
//			INIT_BUS_STOP_DATA()
			
			busStop[0].bActive = TRUE
		ENDIF
	#ENDIF
	
	bTargetFleeing = FALSE
	bCutsceneSkipped = FALSE
	
	// Attempting to fix Bug #937994.
	DISPLAY_RADAR(TRUE)

	END_REPLAY_SETUP()
	
	REMOVE_STREAMVOL_FOR_BUS_STOP_CUTSCENE()
		
	FADE_IN()
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	curStage = MISSION_STATE_PRINTS
ENDPROC

PROC GO_TO_CHECKPOINT_2(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	
	CLEAR_AREA(vBusLot, 50, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< vBusLot.x - 40, vBusLot.y - 40, vBusLot.z - 40 >>,
			<< vBusLot.x + 40, vBusLot.y + 40, vBusLot.z + 40 >>, FALSE)
	
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
			REMOVE_FILLER_BUSES()
			REMOVE_ALL_ENTITIES_IN_ASS_STRUCT(assArgs)
			
			FILL_LOT_WITH_ADDITIONAL_BUSES(targetData)
			
			WHILE NOT FILL_LOT_WITH_AMBIENT_PEDS(targetData)
				WAIT(0)
			ENDWHILE
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), targetData.vWarpPos)
			
			IF DOES_ENTITY_EXIST(args.myVehicle)
				DELETE_VEHICLE(args.myVehicle)
				PRINTLN("REMOVING args.myVehicle")
			ENDIF
				
			CREATE_ASSASSINATION_SCENE(targetData, assArgs)
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 421.5518, -640.2153, 27.5072 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 178.9499)
			
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
			
			IF DOES_BLIP_EXIST(args.myTargetBlip)
				REMOVE_BLIP(args.myTargetBlip)
				PRINTLN("bDoingPSkip: REMOVING BLIP - args.myTargetBlip")
			ENDIF
			IF DOES_BLIP_EXIST(busStop[0].blipStop)
				REMOVE_BLIP(busStop[0].blipStop)
				PRINTLN("bDoingPSkip: REMOVING BLIP - busStop[0].blipStop")
			ENDIF
			IF DOES_BLIP_EXIST(busStop[1].blipStop)
				REMOVE_BLIP(busStop[1].blipStop)
				PRINTLN("bDoingPSkip: REMOVING BLIP - busStop[1].blipStop")
			ENDIF
			IF DOES_BLIP_EXIST(busStop[2].blipStop)
				REMOVE_BLIP(busStop[2].blipStop)
				PRINTLN("bDoingPSkip: REMOVING BLIP - busStop[2].blipStop")
			ENDIF
			IF DOES_BLIP_EXIST(busStop[3].blipStop)
				REMOVE_BLIP(busStop[3].blipStop)
				PRINTLN("bDoingPSkip: REMOVING BLIP - busStop[3].blipStop")
			ENDIF
			
			bGrabbedDistanceToNextStop = FALSE
			
			INIT_BUS_STOP_DATA()
			CREATE_BUS_STOP_BLIPS()
			
//			bSecondCheckpointSceneLoad = FALSE
			
			bReplaying = FALSE
			
			iNumStopsVisited = 0 
		ENDIF
	#ENDIF
	
	IF DOES_ENTITY_EXIST(args.myVehicle)
		IF NOT bReplaying
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), targetData.vVehPos)
				PRINTLN("CHECKPOINT 2: PLAYER IS IN THE BUS, LET'S DETACH AND MOVE HIM")
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << 423.25, -641.36, 28.5068 >>)
				PRINTLN("MOVING GUY, NOT IN CAR")
			ENDIF
		ENDIF
		
		DELETE_VEHICLE(args.myVehicle)
		PRINTLN("CHECKPOINT 2: DELETING VEHICLE - args.myVehicle")
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(args.myVehicle)
		IF bUsingCoach
			args.myVehicle = CREATE_VEHICLE(COACH, targetData.vVehPos, targetData.fVehHead)
			PRINTLN("CREATING VEHICLE - args.myVehicle - USING COACH MODEL")
			SET_VEHICLE_ON_GROUND_PROPERLY(args.myVehicle)
			SET_VEHICLE_COLOUR_COMBINATION(args.myVehicle, 0)
		ELSE
			args.myVehicle = CREATE_VEHICLE(BUS, targetData.vVehPos, targetData.fVehHead)
			PRINTLN("CREATING VEHICLE - args.myVehicle - USING BUS MODEL")
			SET_VEHICLE_ON_GROUND_PROPERLY(args.myVehicle)
			SET_VEHICLE_COLOUR_COMBINATION(args.myVehicle, 0)
		ENDIF
	ENDIF
	
	IF NOT bReplaying
		IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
		ENDIF
	ENDIF
	
//	IF NOT bSecondCheckpointSceneLoad
//		IF IS_SCREEN_FADED_OUT()
//		OR IS_REPLAY_IN_PROGRESS()
//			NEW_LOAD_SCENE_START_SPHERE(<< 423.25, -641.36, 28.5068 >>, 10.0)
//			bSecondCheckpointSceneLoad = TRUE
//			PRINTLN("ASSASSINATION BUS CHECKPOINT 2: CALLING NEW_LOAD_SCENE_START_SPHERE")
//		ELSE
//			PRINTLN("ASSASSINATION BUS CHECKPOINT 2: SCREEN IS NOT FADED OUT")
//		ENDIF
//	ELSE
//		PRINTLN("ASSASSINATION BUS CHECKPOINT 2: bFirstCheckpointSceneLoad IS TRUE")
//	ENDIF
		
	IF DOES_BLIP_EXIST(args.mytargetblip)
		REMOVE_BLIP(args.mytargetblip)
	ENDIF
	IF bReplaying
		WHILE NOT FILL_LOT_WITH_AMBIENT_PEDS(targetData)
			WAIT(0)
			PRINTLN("WAITING FOR SCENE TO LOAD - 2")
		ENDWHILE
		PRINTLN("ASSASSINATION BUS: SCENE HAS LOADED - 2")
		bReplaying = FALSE
	ENDIF
	
//	NEW_LOAD_SCENE_STOP()
	
	WHILE NOT ARE_MODELS_LOADED_FOR_EARLY_BUS_STOPS(targetData)
		WAIT(0)
		PRINTLN("WAITING FOR MODELS TO LOAD FOR EARLY BUS STOPS")
	ENDWHILE
	WHILE NOT ARE_MODELS_LOADED_FOR_FINAL_BUS_STOP(targetData)
		WAIT(0)
		PRINTLN("WAITING FOR MODELS TO LOAD FOR FINAL BUS STOP")
	ENDWHILE
	
	END_REPLAY_SETUP(args.myVehicle)
	
	FADE_IN()
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	// Attempting to fix Bug #937994.
	DISPLAY_RADAR(TRUE)
	
	INIT_BUS_STOP_DATA()
	CREATE_BUS_ROUTE(targetData, args)
	
	bTargetFleeing = FALSE
	bCutsceneSkipped = FALSE
	bTargetKillLinePlayed = FALSE
	
	REMOVE_STREAMVOL_FOR_BUS_STOP_CUTSCENE()
	
	missionStage = MISSION_BUS_ENTER_BUS
	PRINTLN("ASSASSINATION BUS: GOING TO STATE - MISSION_BUS_ENTER_BUS")
	
	curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
	PRINTLN("ASSASSINATION BUS: GOING TO STATE - MISSION_STATE_WAITING_FOR_TARGET_KILL")
ENDPROC


//special case for shitskipping - need to warp the player to the target bus stop and start the mocap cutscene
PROC GO_TO_CHECKPOINT_3_MOCAP(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	
	PRINTLN("USING - GO_TO_CHECKPOINT_3_MOCAP")
	
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
//			bThirdCheckpointSceneLoad = FALSE
			
			IF bSetThirdCheckpoint
				bSetThirdCheckpoint = FALSE
				PRINTLN("bSetThirdCheckpoint = FALSE")
			ENDIF
		ENDIF
		
		bReplaying = FALSE
	#ENDIF
	
	REMOVE_BUS_PASSENGERS()
	
	CREATE_PLAYER_BUS_AT_TARGET_BUS_STOP(args)
	
	IF NOT bReplaying
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
	ENDIF
	
//	IF NOT bThirdCheckpointSceneLoad
//		IF IS_SCREEN_FADED_OUT()
//		OR IS_REPLAY_IN_PROGRESS()
//			NEW_LOAD_SCENE_START_SPHERE(<< 118.3466, -786.1586, 30.3105 >>, 10.0)
//			bThirdCheckpointSceneLoad = TRUE
//			PRINTLN("ASSASSINATION BUS CHECKPOINT 3 MOCAP: CALLING NEW_LOAD_SCENE_START_SPHERE")
//		ELSE
//			PRINTLN("ASSASSINATION BUS CHECKPOINT 3 MOCAP: SCREEN IS NOT FADED OUT")
//		ENDIF
//	ELSE
//		PRINTLN("ASSASSINATION BUS CHECKPOINT 3 MOCAP: bFirstCheckpointSceneLoad IS TRUE")
//	ENDIF
	
	IF DOES_BLIP_EXIST(args.mytargetblip)
		REMOVE_BLIP(args.mytargetblip)
	ENDIF
	
//	IF bReplaying
//		WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
//			WAIT(0)
//			PRINTLN("WAITING FOR SCENE TO LOAD - 3 - MOCAP")
//		ENDWHILE
//		PRINTLN("ASSASSINATION BUS: SCENE HAS LOADED - 3 - MOCAP")
//		bReplaying = FALSE
//	ENDIF
	
	WHILE NOT ARE_MODELS_LOADED_FOR_EARLY_BUS_STOPS(targetData)
		WAIT(0)
	ENDWHILE
	WHILE NOT ARE_MODELS_LOADED_FOR_FINAL_BUS_STOP(targetData)
		WAIT(0)
	ENDWHILE
	
	//special data to set the active stop as the targets
	INIT_BUS_STOP_DATA()
	iBusStop = 3
	iNumStopsVisited = 3
	bTargetFleeing = FALSE
	bCutsceneSkipped = FALSE
	bTargetKillLinePlayed = FALSE
	
	ACTIVATE_NEXT_BUS_STOP(targetData, args, iBusStopOrder[iBusStop], FALSE)
	
	IF DOES_ENTITY_EXIST(busStop[3].viBike)
		args.myOtherVehicle = busStop[3].viBike
	ELSE
		busStop[3].viBike = CREATE_VEHICLE(targetData.otherVehEnum, busStop[3].vVehPos, busStop[3].fVehHead)
		SET_VEHICLE_ON_GROUND_PROPERLY(busStop[3].viBike)
		args.myOtherVehicle = busStop[3].viBike
	ENDIF
	
	CREATE_TARGET_AT_BUS_STOP(targetData, args, 3)
	SET_TARGET_IN_GRAY_SUIT(args)
	
	CREATE_FILLED_BUS(targetData, args)
	
	END_REPLAY_SETUP(args.myVehicle)
	
	REMOVE_STREAMVOL_FOR_BUS_STOP_CUTSCENE()
	
	missionStage = MISSION_BUS_TARGET_FLEE
	PRINTLN("ASSASSINATION BUS: GOING TO STATE - MISSION_BUS_TARGET_FLEE")
	
	curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
	PRINTLN("ASSASSINATION BUS: GOING TO STATE - MISSION_STATE_WAITING_FOR_TARGET_KILL")
ENDPROC

PROC GO_TO_CHECKPOINT_3(ASS_TARGET_DATA& targetData, ASS_ARGS& args)
	
	PRINTLN("USING - GO_TO_CHECKPOINT_3")
	
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
		
//			bThirdCheckpointSceneLoad = FALSE
			
			WHILE NOT ARE_MODELS_LOADED_FOR_EARLY_BUS_STOPS(targetData)
			AND NOT ARE_MODELS_LOADED_FOR_FINAL_BUS_STOP(targetData)
				PRINTLN("WAITING FOR ALL PED MODELS TO LOAD FOR CHECKPOINT_3")
				WAIT(0)
			ENDWHILE
			
			IF bSetThirdCheckpoint
				bSetThirdCheckpoint = FALSE
				PRINTLN("bSetThirdCheckpoint = FALSE")
			ENDIF
			
			bReplaying = FALSE
		ENDIF
	#ENDIF
	
	REMOVE_BUS_PASSENGERS()
	
	CREATE_PLAYER_BUS_AT_TARGET_BUS_STOP(args)
	
	IF NOT bReplaying
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
	ENDIF
	
//	IF NOT bThirdCheckpointSceneLoad
//		IF IS_SCREEN_FADED_OUT()
//		OR IS_REPLAY_IN_PROGRESS()
//			NEW_LOAD_SCENE_START_SPHERE(<< 118.3466, -786.1586, 30.3105 >>, 10.0)
//			bThirdCheckpointSceneLoad = TRUE
//			PRINTLN("ASSASSINATION BUS CHECKPOINT 3: CALLING NEW_LOAD_SCENE_START_SPHERE")
//		ELSE
//			PRINTLN("ASSASSINATION BUS CHECKPOINT 3: SCREEN IS NOT FADED OUT")
//		ENDIF
//	ELSE
//		PRINTLN("ASSASSINATION BUS CHECKPOINT 3: bFirstCheckpointSceneLoad IS TRUE")
//	ENDIF
	
	IF DOES_BLIP_EXIST(args.mytargetblip)
		REMOVE_BLIP(args.mytargetblip)
	ENDIF
	
//	IF bReplaying
//		WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
//			WAIT(0)
//			PRINTLN("WAITING FOR SCENE TO LOAD - 3")
//		ENDWHILE
//		PRINTLN("ASSASSINATION BUS: SCENE HAS LOADED - 3")
//		bReplaying = FALSE
//	ENDIF
	
	WHILE NOT ARE_MODELS_LOADED_FOR_EARLY_BUS_STOPS(targetData)
		WAIT(0)
	ENDWHILE
	WHILE NOT ARE_MODELS_LOADED_FOR_FINAL_BUS_STOP(targetData)
		WAIT(0)
	ENDWHILE
	
//	NEW_LOAD_SCENE_STOP()

	END_REPLAY_SETUP(args.myVehicle)
	
	FADE_IN()
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	IF NOT DOES_ENTITY_EXIST(busStop[3].viBike)
		busStop[3].viBike = CREATE_VEHICLE(targetData.otherVehEnum, busStop[3].vVehPos, busStop[3].fVehHead)
		SET_VEHICLE_ON_GROUND_PROPERLY(busStop[3].viBike)
	ENDIF
	
	bTargetKillLinePlayed = FALSE
	bPoliceScannerLinePlayed = FALSE
	
	iNumStopsVisited = 3
	
	CREATE_TARGET_AT_BUS_STOP(targetData, args, 3)
	SET_TARGET_IN_GRAY_SUIT(args)
	args.myOtherVehicle = busStop[3].viBike
	
	SET_ENTITY_COORDS(args.myOtherVehicle, << 110.2194, -775.8312, 30.4426 >>)
	SET_ENTITY_HEADING(args.myOtherVehicle, 77.2454)
	SET_PED_INTO_VEHICLE(args.myTarget, args.myOtherVehicle)
	
	bReactionAnimsLoaded = FALSE
	
	TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(args.myTarget, args.myOtherVehicle, "OJASbs_102", DF_SwerveAroundAllCars|DF_SteerAroundObjects|DF_SteerAroundPeds, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
	vCombatPos = << -455.28925, -326.13641, 41.22218 >>
	WAYPOINT_RECORDING_GET_NUM_POINTS("OJASbs_102", iRecordingTime)
	SET_ENTITY_LOAD_COLLISION_FLAG(args.myTarget, TRUE)
	fBikeNormalSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED(args.myOtherVehicle)
	MODIFY_VEHICLE_TOP_SPEED(args.myOtherVehicle, (fBikeTopSpeedModifier * 100))	//allow the bike to achieve speeds faster than its default		
	SET_PED_COMBAT_ATTRIBUTES(args.myTarget, CA_LEAVE_VEHICLES, TRUE)
	SET_VEHICLE_FORWARD_SPEED(args.myOtherVehicle, 1.5)
	bTargetFleeing = TRUE
	
	FREEZE_ENTITY_POSITION(args.myVehicle, FALSE)
//	TASK_PLAYER_TO_KILL_TARGET(args)
	
	CREATE_FILLED_BUS(targetData, args)
	SETUP_BUS_PASSENGERS_FOR_DIALOGUE(args)
	
	// Attempting to fix Bug #937994.
	DISPLAY_RADAR(TRUE)
	
	REMOVE_STREAMVOL_FOR_BUS_STOP_CUTSCENE()
	
	missionStage = MISSION_BUS_UPDATE_BIKE 
	PRINTLN("ASSASSINATION BUS: GOING TO STATE - MISSION_BUS_UPDATE_BIKE")
	
	curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
	PRINTLN("ASSASSINATION BUS: GOING TO STATE - MISSION_STATE_WAITING_FOR_TARGET_KILL")
ENDPROC

PROC GO_TO_CHECKPOINT_4(ASS_ARGS& args)
	
	PRINTLN("USING - GO_TO_CHECKPOINT_4")
	
	#IF IS_DEBUG_BUILD
		IF bDoingPSkip OR bDoingJSkip
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<79.6878, -802.4135, 30.5188>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 266.8253)
			
			bReplaying = FALSE
		ENDIF
	#ENDIF
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	IF DOES_BLIP_EXIST(args.mytargetblip)
		REMOVE_BLIP(args.mytargetblip)
		PRINTLN("GO_TO_CHECKPOINT_4: REMOVING BLIP - args.mytargetblip")
	ENDIF
	
	IF DOES_ENTITY_EXIST(args.myTarget)
		DELETE_PED(args.myTarget)
		PRINTLN("GO_TO_CHECKPOINT_4: DELETING PED - args.myTarget")
	ENDIF
	
	IF DOES_ENTITY_EXIST(args.myOtherVehicle)
		DELETE_VEHICLE(args.myOtherVehicle)
		PRINTLN("GO_TO_CHECKPOINT_4: DELETING VEHICLE - args.myOtherVehicle")
	ENDIF
	
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	
	KILL_ANY_CONVERSATION()
	
	END_REPLAY_SETUP()
	
	FADE_IN()
	
	// Attempting to fix Bug #937994.
	DISPLAY_RADAR(TRUE)
	
	curStage = MISSION_STATE_SUCCESS
	PRINTLN("ASSASSINATION BUS: GOING TO STATE - MISSION_STATE_SUCCESS")
ENDPROC

FUNC BOOL HAS_ENDING_CELL_PHONE_CALL_COMPLETED(ASS_ARGS& args)
	IF NOT bPlayedCellPhoneCall
		DISABLE_CELLPHONE(FALSE)
		
		ADD_PED_FOR_DIALOGUE(args.assConv, 3, NULL, "LESTER")
		
		IF PLAYER_CALL_CHAR_CELLPHONE(args.assConv, CHAR_LESTER, "OJASAUD", "OJAS_BUS_C", CONV_MISSION_CALL)
			PRINTLN("BUS - CELL PHONE CALL")
			bPlayedCellPhoneCall = TRUE
		ENDIF
	ENDIF
	
	IF bPlayedCellPhoneCall AND HAS_CELLPHONE_CALL_FINISHED()
	
		PRINTLN("RETURNING TRUE - HAS_ENDING_CELL_PHONE_CALL_COMPLETED")
		RETURN TRUE
	ELSE
		PRINTLN("WAITING FOR CELL PHONE CALL TO FINISH")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GRAB_RETRY_VEHICLE()
	 
	IF NOT DOES_ENTITY_EXIST(viReplayVehicle)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				viReplayVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				PRINTLN("FOUND REPLAY VEHICLE")
				
				IF DOES_ENTITY_EXIST(viReplayVehicle) AND NOT IS_ENTITY_DEAD(viReplayVehicle)
					OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(viReplayVehicle)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_FRANKLIN_LEFT_BUS(ASS_ARGS& args)
	VECTOR vPlayerPos, vVehiclePos
	
	SWITCH iLeaveBusStages
		CASE 0
			IF NOT IS_ENTITY_DEAD(args.myVehicle) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
					PRINT_NOW("ASS_BS_EXIT", DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
			ENDIF
			PRINTLN("iLeaveBusStages = 1")
			iLeaveBusStages = 1
		BREAK
		CASE 1
			IF NOT IS_ENTITY_DEAD(args.myVehicle) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
					
					vVehiclePos = GET_ENTITY_COORDS(args.myVehicle, FALSE)
					vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
					
					IF VDIST2(vVehiclePos, vPlayerPos) < 2500 // 50m
						PRINT_NOW("ASS_BS_AREA", DEFAULT_GOD_TEXT_TIME, 1)
					ENDIF
					PRINTLN("iLeaveBusStages = 2")
					iLeaveBusStages = 2
				ENDIF
			ELIF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND DOES_ENTITY_EXIST(args.myVehicle)
				// If the Bus is dead, we stil need to check if the player has left it.
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
					vVehiclePos = GET_ENTITY_COORDS(args.myVehicle, FALSE)
					vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
					
					IF VDIST2(vVehiclePos, vPlayerPos) < 2500 // 50m
						PRINT_NOW("ASS_BS_AREA", DEFAULT_GOD_TEXT_TIME, 1)
					ENDIF
					PRINTLN("BUS IS DEAD: iLeaveBusStages = 2")
					iLeaveBusStages = 2
				ENDIF
			ELSE
				PRINTLN("FAIL SAFE: iLeaveBusStages = 2")
				iLeaveBusStages = 2
			ENDIF
		BREAK
		CASE 2
			IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myVehicle)
					PRINTLN("iLeaveBusStages = 0 - Franklin re-entered teh bus. tell him to leave again")
					iLeaveBusStages = 0
				ENDIF
			ENDIF
			
			vVehiclePos = GET_ENTITY_COORDS(args.myVehicle, FALSE)
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			
			IF VDIST2(vVehiclePos, vPlayerPos) > 2500 // 50m
				IF IS_THIS_PRINT_BEING_DISPLAYED("ASS_BS_EXIT")
					CLEAR_THIS_PRINT("ASS_BS_EXIT")
				ENDIF
				PRINTLN("RETURNING TRUE - HAS_FRANKLIN_LEFT_BUS")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_BIKE_RESET(ASS_ARGS& args)

	IF NOT bResetBikeSpeed
		IF DOES_ENTITY_EXIST(args.myOtherVehicle) AND NOT IS_ENTITY_DEAD(args.myOtherVehicle)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), args.myOtherVehicle)
				
				MODIFY_VEHICLE_TOP_SPEED(args.myOtherVehicle, 0)
				
				PRINTLN("bResetBikeSpeed = TRUE")
				bResetBikeSpeed = TRUE
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

//main update loop for the assassination
FUNC BOOL UPDATE_ASSASSINATION_BUS(ASS_TARGET_DATA& targetData, ASS_ARGS& args)

	SWITCH curStage
		CASE MISSION_BRIEF_INIT
		
			IF Is_Replay_In_Progress()
//				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
//					PRINTLN("REPLAY VEHICLE IS AVAILABLE - BUS")
//					bReplayVehicleAvailable = TRUE
//				ELSE
//					PRINTLN("BUS: REPLAY VEHICLE IS NOT AVAILABLE")
//				ENDIF
				bReplaying = TRUE
				curStage = MISSION_STATE_INIT
				BREAK
			ENDIF
			
			DO_STAGE_INTRO_CUTSCENE(args)
		BREAK
		
		//print the objectives and help messages immediately after launching the mission
		CASE MISSION_STATE_INIT
		
			//initialize necessities for mission and create peds/vehicles/props for assassination scene
			INIT_BUS_DATA(targetData)
			INIT_MISSION_REQUESTS_AND_UI_LABELS(targetData, sLoadQueue)
			
			SET_BUS_MISSION_TRAFFIC_SETTINGS()	
			SET_PED_MODEL_IS_SUPPRESSED(targetData.targetEnum, TRUE)
			CREATE_ASSASSINATION_SCENE(targetData, assArgs)
			FILL_LOT_WITH_ADDITIONAL_BUSES(targetData)
			
			IF bReplaying
			
				IF g_savedGlobals.sAssassinData.fBusMissionTime <> 0
					RESTART_TIMER_AT(tMissionTime, g_savedGlobals.sAssassinData.fBusMissionTime)
					PRINTLN("RESTARTING TIMER - missionTimer AT: ", g_savedGlobals.sAssassinData.fBusMissionTime)	
				ENDIF
			
				IF iStageToUse = 0
					GO_TO_CHECKPOINT_1(targetData, args)
				ELIF iStageToUse = 1
					GO_TO_CHECKPOINT_2(targetData, args)
				ELIF iStageToUse = 4
					GO_TO_CHECKPOINT_3_MOCAP(targetData, args)
				ELIF iStageToUse = 2
					GO_TO_CHECKPOINT_3(targetData, args)
				ELIF iStageToUse = 3
					GO_TO_CHECKPOINT_4(args)
				ENDIF
			ELSE
				IF NOT IS_TIMER_STARTED(tMissionTime)
					START_TIMER_NOW(tMissionTime)
					PRINTLN("STARTING TIMER - tMissionTime")
					
					g_savedGlobals.sAssassinData.fBusMissionTime = 0
					PRINTLN("BUS GLOBAL MISSION TIMER = ", g_savedGlobals.sAssassinData.fBusMissionTime)
				ENDIF
			
				PRINTLN("GOING TO STATE - MISSION_STATE_STREAMING")
				curStage = MISSION_STATE_STREAMING
			ENDIF
		BREAK
		
		CASE MISSION_STATE_STREAMING
			
			IF ARE_MISSION_REQUESTS_FINISHED_STREAMING(targetData, sLoadQueue)			
				PRINTLN("curStage = MISSION_STATE_PRINTS")
				curStage = MISSION_STATE_PRINTS
			ENDIF
		BREAK		
		
		CASE MISSION_STATE_PRINTS
		
			IF NOT IS_SCREEN_FADING_IN()
				//tell the player to lose their wanted level if needed
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					PRINT_NOW(targetData.OBJECTIVES[0], DEFAULT_GOD_TEXT_TIME, 1)
//					bFirstObjectivePrinted = TRUE
				ELSE
					REMOVE_BLIP(args.myTargetBlip)
					PRINT_NOW("ASS_BS_COPS", DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
				
				SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(args.myTargetBlip, <<388.7943, -681.7061, 28.1490>>, 272.5919)
				PRINTLN("SETTING TAXI LOCATION")
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "assassin_bus_enter_bus")
				OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(viReplayVehicle)
				
				curStage = MISSION_STATE_TRAVEL
			ENDIF
		BREAK
		
		//player is en route to the target. Trigger intro cutscene when in range if not wanted
		CASE MISSION_STATE_TRAVEL
		
			REMOVE_SKINNED_PHONE_PROP()
		
			GRAB_RETRY_VEHICLE()
		
			IF DOES_BLIP_EXIST(args.myTargetBlip)	//station blip
				//set bit to check for wanted as a parameter for triggering panic so that the player will fail if they approach the station while wanted
				CHECK_FOR_WANTED_IN_AGGRO_CHECKS()
				
				//monitor to see if player has become wanted or blown their cover prior to triggering the intro cutscene and handle accordingly
				IF IS_PLAYER_NEAR_COORD_AND_NOT_A_TAXI_PASSENGER(targetData.vDestPos, fDefaultAssassinSceneRange)
				AND IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
					IF DOES_BLIP_EXIST(args.myTargetBlip)
						REMOVE_BLIP(args.myTargetBlip)
					ENDIF
//					curStage = MISSION_STATE_CUTSCENE_SETUP

					DISABLE_CELLPHONE(TRUE)
					PRINTLN("DISABLING CELLPHONE - NOW THAT THE PLAYER HAS REACHED BUS DEPOT")
					curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
				ELSE
					REMOVE_BLIP_AND_PRINT_OBJECTIVE_MSG_IF_WANTED(args.myTargetBlip, "ASS_BS_COPS")
					
					IF GET_PLAYER_DISTANCE_FROM_LOCATION(targetData.vDestPos) < 200.0
						FILL_LOT_WITH_AMBIENT_PEDS(targetData)
					ENDIF
				ENDIF
			ELSE
				//print destination objective message when not wanted
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					args.myTargetBlip = ADD_BLIP_FOR_COORD(targetData.vDestPos)
					SET_BLIP_ROUTE(args.myTargetBlip, TRUE)
//					IF NOT bFirstObjectivePrinted
//						PRINT_NOW("ASS_BS_LOOK", DEFAULT_GOD_TEXT_TIME, 1)
//						bFirstObjectivePrinted = TRUE
//					ENDIF
				ELSE
					//don't do wanted checks here because we dont want to fail the player for becoming wanted if they aren't near the station
					DONT_CHECK_FOR_WANTED_IN_AGGRO_CHECKS()
				ENDIF
			ENDIF
			
			// If the player enters a bus early, go ahead and skip right to the first objective, i.e. pick up passengers.
			CHECK_FOR_PLAYER_IN_BUS(args)
			
			//make sure we are retaining the players vehicle prior to entering the bus and saving it at franklins house when the mission is over
			SAVE_LAST_PLAYER_VEHICLE()
			
			
			//Fail mission if player is wanted near the station or the vehicle is destroyed
			IF (IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0) AND GET_PLAYER_DISTANCE_FROM_ENTITY(args.myVehicle) < fDefaultAssassinSceneRange )
			OR NOT IS_VEHICLE_SAFE_FOR_ASSASSIN_MISSION(args.myVehicle)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, targetData.vVehPos, 25)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(args.myVehicle, PLAYER_PED_ID())
				DEBUG_MESSAGE("FAILING ASSASSINATION SCRIPT - WANTED LEVEL FAIL")
				SET_MISSION_FAILED(asnArgs, targetData, FAIL_BLOWN_COVER)
			ENDIF
		BREAK
		
//		//intro cutscene has been triggered - set it up properly
//		CASE MISSION_STATE_CUTSCENE_SETUP
//	
//			IF IS_PLAYER_READY_FOR_ASSASSINATION_CUTSCENE()
//				curStage = MISSION_STATE_CUTSCENE
//			ENDIF
//		BREAK
		
//		//play the appropriate intro cutscene
//		CASE MISSION_STATE_CUTSCENE
//		
//			fpCutsceneCustomIntro = &ASSASSIN_INTRO_CUSTOM_BUS
//			GET_CUSTOM_CUTSCENE(targetData)
//			
//			//play the cutscene
//			IF DO_CUTSCENE_CUSTOM(cutArgs.cutsceneState, cutArgs.cutSceneTimer, targetData.vCutscenePos, targetData.vCutsceneHead, myCamera, "", fpCutsceneCustomIntro, FALSE, FALSE, FALSE, targetData.fCutsceneDoF)
//				DO_SCREEN_FADE_IN(500)
//				
//				DISABLE_CELLPHONE(TRUE)
//				PRINTLN("DISABLING CELLPHONE - NOW THAT THE PLAYER HAS REACHED BUS DEPOT")
//				
//				curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
//			ENDIF
//		BREAK	
		
		//waiting for player to kill the target
		CASE MISSION_STATE_WAITING_FOR_TARGET_KILL
		
			HANDLE_BIKE_RESET(assArgs)
		
			CHECK_FOR_PLAYER_IN_BUS(args)
			
			//make sure we are retaining the players vehicle prior to entering the bus and saving it at franklins house when the mission is over
			SAVE_LAST_PLAYER_VEHICLE()
			
			
			IF CHECK_FOR_TARGET_DEATH(assArgs, targetData)
				IF passReasonCore = PASS_BONUS 
					PED_BONETAG pedBonetag
					IF GET_PED_LAST_DAMAGE_BONE(args.myTarget, pedBonetag)
					AND pedBonetag <> BONETAG_NULL
						IF NOT IS_ENTITY_ALIVE(args.myTarget)
							START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_ojbusass_bus_impact", args.myTarget, <<0,0,0>>, GET_ENTITY_FORWARD_VECTOR(args.myVehicle), pedBonetag, 2)
						ENDIF
					ELSE
						IF NOT IS_ENTITY_ALIVE(args.myTarget)
							START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_ojbusass_bus_impact", args.myTarget, <<0,0,0>>, GET_ENTITY_FORWARD_VECTOR(args.myVehicle), BONETAG_HEAD, 2)
						ENDIF
					ENDIF
					
//					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
//						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//						PRINTLN("SETTING WANTED LEVEL - 2")
//					ENDIF
				ENDIF
				
				HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, args.assConv, "OJASAUD", "OJAS_BUCOM", CONV_PRIORITY_VERY_HIGH)
				
				UPDATE_BUS_PASSENGER_ANIMS(args)	//TO FIX BUG 1271508
				
				PRINTLN("GOING TO STATE - MISSION_STATE_KILLCAM")
				curStage = MISSION_STATE_KILLCAM
			ELSE
				UPDATE_LOOP_BUS(targetData, assArgs)
				CHECK_FOR_HIT_AND_RUN_BONUS(args)
			ENDIF
		BREAK
		
		//play the kill cam cutscene
		CASE MISSION_STATE_KILLCAM
//			IF HAS_SLOW_KILLCAM_FINISHED(args.myTarget, FALSE)
				IF DOES_BLIP_EXIST(args.myTargetBlip)
					REMOVE_BLIP(args.myTargetBlip)
				ENDIF
				
				//to fix bugs with camera remaining locked on the target or help message on screen explaining how to focus onthe target
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				CLEAR_HELP()
				
				REMOVE_SPECIAL_HINT_CAM()
				
				bTargetFleeing = FALSE
				
//				CREATE_CONVERSATION(args.assConv, "OJASAUD", "OJASbs_WRNG", CONV_PRIORITY_VERY_HIGH)
				
//				MANAGE_BUS_DIALOGUE(args)
				
				UPDATE_BUS_PASSENGER_ANIMS(args, FALSE)
				
				STOP_SCRIPTED_CONVERSATION(FALSE)
				
				HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, args.assConv, "OJASAUD", "OJAS_BUCOM", CONV_PRIORITY_VERY_HIGH)
				
				PRINTLN("GOING TO STATE - MISSION_STATE_SUCCESS")
				curStage = MISSION_STATE_SUCCESS
//			ENDIF
		BREAK
		
		//pass the mission
		CASE MISSION_STATE_SUCCESS
		
			HANDLE_BIKE_RESET(assArgs)
		
			HANDLE_FRANKLIN_PLAYING_TARGET_KILL_LINE(bTargetKillLinePlayed, args.assConv, "OJASAUD", "OJAS_BUCOM", CONV_PRIORITY_VERY_HIGH)
			
			PLAY_SCREAM_FROM_RANDOM_BUS_PASSENGER(args.myVehicle)
			
			IF HAS_FRANKLIN_LEFT_BUS(args)
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					PRINTLN("WANTED LEVEL = 0")
					
					IF HAS_ENDING_CELL_PHONE_CALL_COMPLETED(args)
						RETURN TRUE
					ELSE
						PRINTLN("WAITING ON PHONE CALL TO FINISH")
					ENDIF
				ELSE
					IF NOT bLoseCopsPrinted
						SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
						PRINT_NOW("ASS_BS_COPS", DEFAULT_GOD_TEXT_TIME, 1)
						RESTART_TIMER_NOW(speechDelayTimer)
						RESTART_TIMER_NOW(speechTimerPassenger)
						PRINTLN("PRINTING LINE TO LOSE THE COPS")
						bLoseCopsPrinted = TRUE
					ELSE
						HANDLE_POLICE_SCANNER_LINE_FOR_BUS_MISSION()
					ENDIF
				ENDIF
			ELSE
				HANDLE_POLICE_SCANNER_LINE_FOR_BUS_MISSION()
			ENDIF
		BREAK
	ENDSWITCH
	
	
	
	//always run these while thei mission is active since we want to fail the player for entering shops (Bug 1039794)
	IF curStage >= MISSION_STATE_TRAVEL
		IF BUS_DO_CUSTOM_SHOP_FAIL_CHECKS()
			SET_MISSION_FAILED(asnArgs, targetData, FAIL_ABANDONED_MISSION)
			PRINTLN("FAILING BUS ASSASSINATION: Player entered a shop")
		ENDIF
	ENDIF
	
	
//	//do checks to see if player has abandoned the mission area prior to discovering the target
//	IF curStage <= MISSION_STATE_WAITING_FOR_TARGET_KILL
//	AND missionStage < MISSION_BUS_TARGET_FLEE
//		IF HAS_PLAYER_ABANDONED_ASSASSINATION_AREA(bWarningHelp, vMissionCenter, "ASS_BS_FAILWARN", 500)
//			PRINTLN("FAILING BUS ASSASSINATION SCRIPT BECAUSE PLAYER ABANDONED THE DOWNTOWN AREA")
//			SET_MISSION_FAILED(asnArgs, targetData, FAIL_ABANDONED_MISSION)
//		ENDIF
//	ENDIF

	RETURN FALSE
ENDFUNC

PROC DO_FADE_OUT_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

// debug builds only for J-skipping
#IF IS_DEBUG_BUILD
	
	PROC HANDLE_BUS_JSKIP_TELEPORT()
		IF IS_GAMEPLAY_CAM_RENDERING()
			IF IS_VEHICLE_DRIVEABLE(assArgs.myVehicle)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), assArgs.myVehicle)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), assArgs.myVehicle)
				ENDIF
			ENDIF
			IF busStop[0].bActive
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 308.5922, -763.7350, 28.2631 >>)
				SET_ENTITY_HEADING(assArgs.myVehicle, 160.5210)
			ELIF busStop[1].bActive
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 353.0733, -1064.4456, 28.4131 >>)
				SET_ENTITY_HEADING(assArgs.myVehicle, 269.8679)
			ELIF busStop[2].bActive
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 266.7450, -1125.1301, 28.2203 >>)
				SET_ENTITY_HEADING(assArgs.myVehicle, 88.6338)
			ELIF busStop[3].bActive
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 116.1863, -785.7573, 30.3428 >>)
				SET_ENTITY_HEADING(assArgs.myVehicle, 68.4340)
			ENDIF
		ENDIF
		
		RESTART_TIMER_AT(busTimer, 2)
	ENDPROC
	
	PROC JUMP_TO_STAGE(ASS_TARGET_DATA& targetData, ASS_ARGS& args, MISSION_STATE state, MISSION_BUS_STAGE stage, BOOL bIsDebugJump = FALSE)       
        DO_FADE_OUT_WITH_WAIT()
        
        //Update mission checkpoint in case they skipped the stages where it gets set.
        IF bIsDebugJump
            SWITCH state
                CASE MISSION_STATE_PRINTS
					PRINTLN("JUMP_TO_STAGE 01: MISSION_STATE_PRINTS")
                	GO_TO_CHECKPOINT_1(targetData, args)
                BREAK
                CASE MISSION_STATE_WAITING_FOR_TARGET_KILL
                    SWITCH stage
						CASE MISSION_BUS_CREATE_PEDS
							PRINTLN("JUMP_TO_STAGE 02: MISSION_STATE_PRINTS")
                			GO_TO_CHECKPOINT_1(targetData, args)
						BREAK
                        CASE MISSION_BUS_ENTER_BUS
                            GO_TO_CHECKPOINT_2(targetData, args)
                        BREAK
						CASE MISSION_BUS_TARGET_FLEE
                            GO_TO_CHECKPOINT_3(targetData, args)
                        BREAK
                    ENDSWITCH
                BREAK
            ENDSWITCH
	    ENDIF
	ENDPROC

	
	// debug skips
	PROC DO_DEBUG_SKIPS_BUS(ASS_ARGS& args, ASS_TARGET_DATA& targetData)
		MISSION_STATE eState
		MISSION_BUS_STAGE eStage
		
		IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iDebugJumpStage)
			IF iDebugJumpStage = 0
				eState = MISSION_STATE_PRINTS
			ELIF iDebugJumpStage = 1
				eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
				eStage = MISSION_BUS_ENTER_BUS
			ELIF iDebugJumpStage = 2
				eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
				eStage = MISSION_BUS_TARGET_FLEE
			ENDIF
			bDoingPSkip = TRUE
			JUMP_TO_STAGE(targetData, args, eState, eStage, TRUE)
		ENDIF
		
		// mission pass S
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			KILL_ANY_CONVERSATION()
			curStage = MISSION_STATE_SUCCESS
		ENDIF
		
		// mission failed F
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			SET_MISSION_FAILED(asnArgs, targetData, FAIL_ABANDONED_MISSION)
		ENDIF
		
		//output debug cam coords relative to entity
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_N)
			OUTPUT_DEBUG_CAM_RELATIVE_TO_ENTITY(args.myVehicle)
		ENDIF
		
		// skip to previous stage
        IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)           
            IF curStage = MISSION_STATE_TRAVEL
				eState = MISSION_STATE_PRINTS
				eStage = MISSION_BUS_STREAM_PEDS
                PRINTLN("eStage = MISSION_STATE_PRINTS")
				bDoingPSkip = TRUE
				JUMP_TO_STAGE(targetData, args, eState, eStage, TRUE)
			ELIF curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
				IF missionStage = MISSION_BUS_ENTER_BUS OR missionStage = MISSION_BUS_UPDATE_BUS_PEDS
			   		eState = MISSION_STATE_PRINTS
					eStage = MISSION_BUS_STREAM_PEDS
                    PRINTLN("eStage = MISSION_STATE_PRINTS")
				ELIF missionStage = MISSION_BUS_TARGET_FLEE OR missionStage = MISSION_BUS_UPDATE_BIKE
					eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
					eStage = MISSION_BUS_ENTER_BUS
					PRINTLN("eStage = MISSION_BUS_ENTER_BUS")
				ELIF missionStage = MISSION_BUS_FLEE_ON_FOOT
					eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
			  		eStage = MISSION_BUS_TARGET_FLEE
					PRINTLN("eStage = MISSION_BUS_TARGET_FLEE")
			   	ENDIF
				bDoingPSkip = TRUE
				JUMP_TO_STAGE(targetData, args, eState, eStage, TRUE)
			ELSE
				PRINTLN("Can't P-SKIP while in this state! You need to be in locate or kill state.")
			ENDIF
		ENDIF

		// skip stage J
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF curStage = MISSION_STATE_TRAVEL
				eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
				eStage = MISSION_BUS_ENTER_BUS
				PRINTLN("eStage = MISSION_BUS_ENTER_BUS")
				bDoingJSkip = TRUE
				JUMP_TO_STAGE(targetData, args, eState, eStage, TRUE)
			ELIF curStage = MISSION_STATE_WAITING_FOR_TARGET_KILL
				IF missionStage = MISSION_BUS_ENTER_BUS OR missionStage = MISSION_BUS_UPDATE_BUS_PEDS
			   		eState = MISSION_STATE_WAITING_FOR_TARGET_KILL
			  		eStage = MISSION_BUS_TARGET_FLEE
					PRINTLN("eStage = MISSION_BUS_TARGET_FLEE")
					bDoingJSkip = TRUE
					JUMP_TO_STAGE(targetData, args, eState, eStage, TRUE)
				ELIF missionStage = MISSION_BUS_TARGET_FLEE OR missionStage = MISSION_BUS_UPDATE_BIKE
					SET_ENTITY_HEALTH(args.myTarget, 0)
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					PRINTLN("eStage = MISSION_STATE_SUCCESS")
					bDoingJSkip = TRUE
					curStage = MISSION_STATE_SUCCESS
			   	ENDIF
			ELSE
				PRINTLN("Can't J-SKIP while in this state! You need to be in locate or kill state.")
			ENDIF
		ENDIF
		
	ENDPROC
#ENDIF

PROC REMOVE_BUS()
	IF DOES_ENTITY_EXIST(vehPlayerIsIn)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerIsIn)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 421.5518, -640.2153, 27.5072 >>)
			PRINTLN("DETACHING PLAYER FROM VEHICLE")
		ENDIF
		
		SET_ENTITY_AS_MISSION_ENTITY(vehPlayerIsIn)
		DELETE_VEHICLE(vehPlayerIsIn)
		PRINTLN("DELETING VEHICLE - vehPlayerIsIn VIA PRE-CLEANUP")
	ENDIF
ENDPROC

PROC SET_BUS_PASSENGERS_TO_REMAIN_IN_BUS()
	INT idx, pedIdx

	REPEAT NUM_BUS_STOPS idx
		FOR pedIdx = 0 TO 2
			IF NOT IS_PED_INJURED(busStop[idx].piAmbPed[pedIdx])
			AND IS_PED_IN_ANY_VEHICLE(busStop[idx].piAmbPed[pedIdx])
				SET_PED_COMBAT_ATTRIBUTES(busStop[idx].piAmbPed[pedIdx], CA_LEAVE_VEHICLES, FALSE)
				SET_PED_FLEE_ATTRIBUTES(busStop[idx].piAmbPed[pedIdx], FA_USE_VEHICLE, FALSE)
				SET_PED_KEEP_TASK(busStop[idx].piAmbPed[pedIdx], TRUE)
			ENDIF
		ENDFOR
	ENDREPEAT
ENDPROC

PROC REMOVE_PEDS_FROM_AUDIO_GROUP(ASS_ARGS& args)
	PED_INDEX tempPed
	INT idx

	REPEAT NUM_BUS_SEATS idx
		IF IS_VEHICLE_DRIVEABLE(args.myVehicle)
			IF GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, idx)) <> NULL
				tempPed = GET_PED_IN_VEHICLE_SEAT(args.myVehicle, INT_TO_ENUM(VEHICLE_SEAT, idx))
				
				IF DOES_ENTITY_EXIST(tempPed) AND NOT IS_ENTITY_DEAD(tempPed)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(tempPed)
					PRINTLN("REMOVING PED FROM AUDIO GROUP")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC



PROC SAVE_MISSION_TIMER_FOR_MISSION()
	IF IS_TIMER_STARTED(tMissionTime)
		g_savedGlobals.sAssassinData.fBusMissionTime = GET_TIMER_IN_SECONDS(tMissionTime)
		PRINTLN("CLEANUP: tMissionTime = ", g_savedGlobals.sAssassinData.fBusMissionTime)
	ELSE
		PRINTLN("CLEANUP: tMissionTime WAS NOT STARTED!!!")
	ENDIF
ENDPROC




//main script
SCRIPT(ASS_TARGET_DATA targetData)
	DEBUG_MESSAGE("Assassination Bus Start")
	
	SET_MISSION_FLAG(TRUE)
	
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		DEBUG_MESSAGE("FORCE CLEANUP HAS OCCURRED!!!")
		SET_BITMASK_AS_ENUM(g_savedGlobals.sAssassinData.iGenericData, ACD_FAILED)
		
		SAVE_MISSION_TIMER_FOR_MISSION()
		
		Mission_Flow_Mission_Force_Cleanup()
		
		//restore phone stuff
		IF DOES_ENTITY_EXIST(oPayPhone)
			SET_ENTITY_VISIBLE(oPayPhone, TRUE)
			SET_ENTITY_COLLISION(oPayPhone, TRUE)
			SET_ENTITY_CAN_BE_DAMAGED(oPayPhone, TRUE)
		ENDIF
		
		REMOVE_NAVMESH_BLOCKING()
		
		MISSION_CLEANUP(sLoadQueue)
	ENDIF
	
	IF Is_Replay_In_Progress()
		IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
			PRINTLN("REPLAY VEHICLE IS AVAILABLE - CONSTRUCTION")
			bReplayVehicleAvailable = TRUE
		ELSE
			PRINTLN("REPLAY VEHICLE IS NOT AVAILABLE - CONSTRUCTION")
		ENDIF
		bReplaying = TRUE
	ELSE
		bReplaying = FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		AddWidgets(myDebugData)
		SETUP_DEBUG()
	#ENDIF
	
	// Fix Bug # 712684
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	ENDIF
	
	sb01 = PREVENT_PEDS_FROM_BEING_IN_AREA((<<305.0073, -764.5070, 28.3097>> + <<2,2,2>>), (<<305.0073, -764.5070, 28.3097>> - <<2,2,2>>))
	sb02 = PREVENT_PEDS_FROM_BEING_IN_AREA((<<355.6028, -1067.1489, 28.5672>> + <<2,2,2>>), (<<355.6028, -1067.1489, 28.5672>> - <<2,2,2>>))
	
	ASSASSINATION_ClearUnneededGenericData()
	
	iCurAssassinRank = ENUM_TO_INT(ASSASSINATION_Bus) //g_savedGlobals.sAssassinData.iCurrentAssassinRank
	missionData = ASSASSINATION_GetMissionData(iCurAssassinRank)	
	PRINTLN("Current rank is... ", iCurAssassinRank)
	
	
	//Note we don't have to check this in Aggro checks because the IS_PLAYER_SHOOTING_FROM_BUS() function will now cover this (3/14/13 - MB)
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_HeardShot)
	
	SET_WANTED_LEVEL_MULTIPLIER(0.2)
	PRINTLN("SETTING WANTED LEVEL MULTIPLIER TO 0.2")
		
	
	
	//HANDLE STREAMING AROUND REPLAY START POSITION
	IF IS_REPLAY_IN_PROGRESS()
	
		bHaveRetryOnce = TRUE
		PRINTLN("BUS: SETTING -  bHaveRetryOnce = TRUE")
		
		iStageToUse = Get_Replay_Mid_Mission_Stage()
		PRINTLN("iStageToUse = ", iStageToUse)
		
		IF g_bShitskipAccepted
			IF iStageToUse < 1
			OR iStageToUse >= 2
				iStageToUse = (iStageToUse + 1)
				PRINTLN("SHIT SKIP: iStageToUse = ", iStageToUse)
			ELIF iStageToUse = 1
				iStageToUse = 4	//special mocap stage
				PRINTLN("SHIT SKIP: GO TO MOCAP CUTSCENE! iStageToUse = ", iStageToUse)
			ENDIF
		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		
		IF iStageToUse = 0
			IF bReplayVehicleAvailable
				REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
				WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
					PRINTLN("CONSTRUCTION: Waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED - 01")
					WAIT(0)
				ENDWHILE
			
				//create the replay vehicle but make sure it isnt a water based vehicle (sso we dont spawn it on land)
				VEHICLE_INDEX vehPlayer
				vehPlayer = CREATE_REPLAY_CHECKPOINT_VEHICLE(vMoveVehiclePosition01, fMoveVehicleHeading01)
				IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
				AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
				ELSE
					DELETE_VEHICLE(vehPlayer)
				ENDIF
			ENDIF
			
			START_REPLAY_SETUP(<< -22.4026, -110.8498, 56.0027 >>, 187.4018)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 1
	
			START_REPLAY_SETUP(<< 421.5518, -640.2153, 27.5072 >>, 178.9499)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 2
		
			START_REPLAY_SETUP(<< 96.6454, -779.0001, 30.4853 >>, 249.5191)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 4
			//mocap cutscene only triggererable via Shitskip
			START_REPLAY_SETUP(<< 96.6454, -779.0001, 30.4853 >>, 249.5191)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ELIF iStageToUse = 3

			START_REPLAY_SETUP(<<79.6878, -802.4135, 30.5188>>, 266.8253)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ENDIF
	ENDIF
	
	//main loop
	WHILE TRUE
		WAIT(0)
		
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ASS4")
		
//		PRINTLN("CURRENT STATE: ", ENUM_TO_INT(curStage))
//		PRINTLN("STATE: ", ENUM_TO_INT(missionStage))
		
		IF UPDATE_FAIL_CONDITIONS(targetData)
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				SAVE_MISSION_TIMER_FOR_MISSION()
				
				REMOVE_PEDS_FROM_AUDIO_GROUP(assArgs)
			
				ALLOW_PEDS_TO_BE_IN_AREA(sb01, (<<305.0073, -764.5070, 28.3097>> + <<2,2,2>>), (<<305.0073, -764.5070, 28.3097>> - <<2,2,2>>))
				ALLOW_PEDS_TO_BE_IN_AREA(sb02, (<<305.0073, -764.5070, 28.3097>> + <<2,2,2>>), (<<305.0073, -764.5070, 28.3097>> - <<2,2,2>>))
				REMOVE_BUS()
				
				REMOVE_SCENARIO_BLOCKING_AREA(sbiBusLot[0])
				REMOVE_SCENARIO_BLOCKING_AREA(sbiBusLot[1])
				REMOVE_SCENARIO_BLOCKING_AREA(sbiBusLot[2])
				REMOVE_SCENARIO_BLOCKING_AREA(sbiBusLot[3])
				REMOVE_SCENARIO_BLOCKING_AREA(sbiBusLot[4])
				REMOVE_SCENARIO_BLOCKING_AREA(sbiBusLot[5])
				
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				CLEAR_HELP()
				CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
				
				REMOVE_NAVMESH_BLOCKING()
				
				SET_WANTED_LEVEL_MULTIPLIER(1.0)
				PRINTLN("SETTING WANTED LEVEL MULTIPLIER TO 1.0")
				MISSION_FAILED(sLoadQueue)
			ENDIF
		ELIF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF UPDATE_ASSASSINATION_BUS(targetData, assArgs)

				g_savedGlobals.sAssassinData.fBusMissionTime = GET_TIMER_IN_SECONDS(tMissionTime)
				PRINTLN("GLOBAL BUS MISSION TIME = ", g_savedGlobals.sAssassinData.fBusMissionTime)
				
				fTimeTaken = g_savedGlobals.sAssassinData.fBusMissionTime
				PRINTLN("fTimeTaken = ", fTimeTaken)
				
				SET_BUS_PASSENGERS_TO_REMAIN_IN_BUS()
				
				MOVE_LAST_PLAYER_DRIVEN_VEHICLE_TO_FRANKLINS_SAFEHOUSE()
				
				MISSION_PASSED(missionData, cucEndScreenCore, sLoadQueue)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			DO_DEBUG_SKIPS_BUS(assArgs, targetData)
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) 
				vPlayerDebugPositon = GET_ENTITY_COORDS(PLAYER_PED_ID()) 
				fPlayerDebugHeading = GET_ENTITY_HEADING(PLAYER_PED_ID()) 
			ENDIF 
			vSpherePosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerDebugPositon, fPlayerDebugHeading, << myDebugData.fOffsetX, myDebugData.fOffsetY, myDebugData.fOffsetZ >>) 

			GET_GROUND_Z_FOR_3D_COORD(vSpherePosition, fDebugHeight) 
			DRAW_DEBUG_SPHERE(vSpherePosition, 0.1)
			
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_H)
//				PLAY_SCREAM_FROM_RANDOM_BUS_PASSENGER(assArgs.myVehicle, TRUE)
//			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_H)
				OPEN_BUS_DOORS(assArgs.myVehicle)
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_V)
				CLOSE_BUS_DOORS(assArgs.myVehicle)
			ENDIF
		#ENDIF
	ENDWHILE
ENDSCRIPT
