// launcher_Assassination_support_lib.sch
USING "minigames_helpers.sch"
USING "script_drawing.sch"
USING "script_clock.sch"
USING "assassination_support_lib.sch"
USING "minigame_UIInputs.sch"
USING "cutscene_public.sch"
USING "script_useContext.sch"
USING "clearMissionArea.sch"

CONST_FLOAT		NEXT_LINE 			0.035
CONST_FLOAT		LINE_BREAK			0.0525
CONST_FLOAT		INDENT				0.03
CONST_INT		MAX_ASSASSIN_RANK	5

PROC ASSASSINATION_ClearUnneededGenericData()
	BOOL bDiedOnMission = IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sAssassinData.iGenericData, ACD_DIED_SENDCELLTEXT)
	g_savedGlobals.sAssassinData.iGenericData = 0
	
	IF bDiedOnMission
		SET_BITMASK_AS_ENUM(g_savedGlobals.sAssassinData.iGenericData, ACD_DIED_SENDCELLTEXT)
	ENDIF
ENDPROC

FUNC BOOL ASSASSIN_MISSION_IsTimeToLaunchMission()	
	RETURN IS_NOW_AFTER_TIMEOFDAY(g_savedGlobals.sAssassinData.TODForNextMission)
ENDFUNC

PROC ASSASSIN_CONTROLLER_GiveMissionLoadout(eAssassinPackage_WeaponOutfit eEquipment)
	IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(PLAYER_ID()))	
		// See if we're to give a bulletproof vest
		IF (ENUM_TO_INT(eEquipment & eASM_BulletProofVest) <> 0)
			ADD_ARMOUR_TO_PED(GET_PLAYER_PED(PLAYER_ID()), GET_PLAYER_MAX_ARMOUR(PLAYER_ID()))
		ENDIF
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
	ENDIF
ENDPROC

PROC ASSASSIN_CONTROLLER_RunPhoneConvo(INT iRank, structPedsForConversation& conversationped)	
	SWITCH (INT_TO_ENUM(ASSASSINATION_MISSIONS, iRank))
		CASE ASSASSINATION_Vice
			//Vice
			CREATE_CONVERSATION(conversationped, "OJASAUD", "OJAScnt_hk", CONV_PRIORITY_VERY_HIGH)
		BREAK
		CASE ASSASSINATION_Bus
			//Bus
			CREATE_CONVERSATION(conversationped, "OJASAUD", "OJAScnt_bs", CONV_PRIORITY_VERY_HIGH)
		BREAK
		CASE ASSASSINATION_Hotel
			//Hotel
			CREATE_CONVERSATION(conversationped, "OJASAUD", "OJAScnt_va", CONV_PRIORITY_VERY_HIGH)
		BREAK
		CASE ASSASSINATION_Multi
			//Multi
			CREATE_CONVERSATION(conversationped, "OJASAUD", "OJAScnt_ml", CONV_PRIORITY_VERY_HIGH)
		BREAK
		CASE ASSASSINATION_Construction
			//Construction
			CREATE_CONVERSATION(conversationped, "OJASAUD", "OJAScnt_cs", CONV_PRIORITY_VERY_HIGH)
		BREAK
	ENDSWITCH
ENDPROC

PROC ASSASSIN_CONTROLLER_StartPhoneAnswerCutscene(sAssassinMissionData sMissionData, CAMERA_INDEX & cutCam, structPedsForConversation & conversationped)
	VECTOR vPhoneLocation = sMissionData.vCutsceneCoord
	
	
	//set scripts safe for cutscene so that it doesn't get interrupted by phone calls or random events
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
	
	//clear any nearby peds/vehicles but make sure not to nuke the player's car if it is nearby
//	VEHICLE_INDEX PlayerLastVehicle
//	BOOL bNearby
	
//	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//		PlayerLastVehicle = GET_PLAYERS_LAST_VEHICLE()
//		
//		IF DOES_ENTITY_EXIST(PlayerLastVehicle) AND NOT IS_ENTITY_DEAD(PlayerLastVehicle)
//			IF GET_ENTITY_DISTANCE_FROM_LOCATION(PlayerLastVehicle, sMissionData.vCutsceneCoord) <= 15
//				//get it out of the area before we clear (location above the payphone off camera so it wont get cleared by next script call)
//				SET_ENTITY_COORDS(PlayerLastVehicle, sMissionData.vSafeCarPosition)	
//				bNearby = TRUE
//			ENDIF
//		ENDIF
//	ENDIF

//	IF bNearby
//		//reset vehicle near the player
//		SET_ENTITY_COORDS(PlayerLastVehicle, sMissionData.vSafeCarPosition)	
//		SET_ENTITY_HEADING(PlayerLastVehicle, sMissionData.fSafeCarHeading)
//	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_VECTOR_ZERO(sMissionData.vPlayerPosition)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), sMissionData.vPlayerPosition)	
			SET_ENTITY_HEADING(PLAYER_PED_ID(), sMissionData.fPlayerHeading)
			PRINTLN("SETTING PLAYER'S POSITION")
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				PRINTLN("REMOVING PLAYER CONTROL")
			ENDIF
		ELSE
			PRINTLN("VECTOR IS ZERO, NOT MOVING PLAYER")
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				PRINTLN("REMOVING PLAYER CONTROL - 01")
			ENDIF
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMissionData.missionFilename, "Assassin_Valet")
		RESOLVE_VEHICLES_AT_MISSION_TRIGGER(<<-1685.4540, -1061.1909, 12.0177>>, 319.4578)
		PRINTLN("USING VALET WARP POSITION")
	ELIF ARE_STRINGS_EQUAL(sMissionData.missionFilename, "Assassin_Multi")
//		RESOLVE_VEHICLES_AT_MISSION_TRIGGER(<<-711.8262, -920.7407, 18.0144>>, 0.0954, FALSE, VEHICLE_TYPE_DEFAULT, TRUE, FALSE, FALSE, TRUE, FALSE)
		RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-736.790771,-924.130310,18.163729>>, <<-696.861389,-923.751709,19.658636>>, 45.0, <<-718.66425, -920.66919, 18.01450>>, 
				0.0954, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
		PRINTLN("USING MULTI POSITION")
	ELIF ARE_STRINGS_EQUAL(sMissionData.missionFilename, "Assassin_Hooker")
		RESOLVE_VEHICLES_AT_MISSION_TRIGGER(<<219.4298, -851.6688, 29.1372>> , 249.6805)
		PRINTLN("USING HOOKER POSITION")
	ELIF ARE_STRINGS_EQUAL(sMissionData.missionFilename, "Assassin_Bus")
		RESOLVE_VEHICLES_AT_MISSION_TRIGGER(<<-24.4315, -113.0674, 55.9033>>, 68.6517)
		PRINTLN("USING BUS POSITION")
	ELIF ARE_STRINGS_EQUAL(sMissionData.missionFilename, "Assassin_Construction")
		RESOLVE_VEHICLES_AT_MISSION_TRIGGER(<<797.9703, -1064.6918, 26.7261>>, 6.2218)
		PRINTLN("USING CONSTRUCTION POSITION")
	ENDIF
	
//	CLEAR_AREA(vPhoneLocation, 15, TRUE, TRUE)

	//create necessary cameras
	IF NOT DOES_CAM_EXIST(cutCam)
		cutCam = CREATE_CAMERA(CAMTYPE_SCRIPTED)
		PRINTLN("CREATING CAMERA - cutCam")
	ENDIF
	SET_CAM_PARAMS(cutCam, sMissionData.vCamCoord1, sMissionData.vCamRot1, 35)
	SET_CAM_ACTIVE(cutCam, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
//	//TODO: get rid of the stub and put in the mocap scene!
//	PRINT_HELP("ASS_STUB")

	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)

	WAIT(0)
	
	ADD_PED_FOR_DIALOGUE(conversationped, 3, NULL, "LESTER")
	ADD_PED_FOR_DIALOGUE(conversationped, 1, PLAYER_PED_ID(), "FRANKLIN")
	
	DISPLAY_RADAR(FALSE)
	DISPLAY_HUD(FALSE)
	DISABLE_NAVMESH_IN_AREA(<< vPhoneLocation.x-20, vPhoneLocation.y-20, vPhoneLocation.z-20 >>, << vPhoneLocation.x+20, vPhoneLocation.y+20, vPhoneLocation.z+20 >>, TRUE)
ENDPROC

PROC ASSASSIN_CONTROLLER_UpdatePhoneAnswerCutscene()

ENDPROC

PROC ASSASSIN_CONTROLLER_EndPhoneAnswerCutscene(CAMERA_INDEX & cutCam)	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	SET_CAM_ACTIVE(cutCam, FALSE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
	DISABLE_NAVMESH_IN_AREA(<< 286.2024, -1286.6108, 28.8614 >>, << 300.4982, -1273.8337, 28.7976 >>, FALSE)
ENDPROC

/// PURPOSE:
///    Swaps cameras. ATM, just switches between 2.
/// PARAMS:
///    sMissionData - 
///    cutCam - 
///    cutCam2 - 
///    iCamStage - 
PROC ASSASSIN_CONTROLLER_SwapPhoneCutsceneCam(sAssassinMissionData sMissionData, CAMERA_INDEX & cutCam)	
	
	IF NOT DOES_CAM_EXIST(cutCam)
		cutCam = CREATE_CAMERA(CAMTYPE_SCRIPTED)
		SET_CAM_PARAMS(cutCam, sMissionData.vCamCoord1, sMissionData.vCamRot1, 35)
		SET_CAM_ACTIVE(cutCam, TRUE)
		PRINTLN("CREATING CAM")
	ENDIF
//	IF NOT DOES_CAM_EXIST(cutCam2)
//		cutCam2 = CREATE_CAMERA(CAMTYPE_SCRIPTED)
//		SET_CAM_PARAMS(cutCam2, sMissionData.vCamCoord2, sMissionData.vCamRot2, 35)
//	ENDIF
	
//	SWITCH iCamStage
//		CASE 0
//			SET_CAM_ACTIVE(cutCam, TRUE)
//			SET_CAM_ACTIVE(cutCam2, FALSE)
//		BREAK
//		CASE 1
//			SET_CAM_ACTIVE(cutCam2, TRUE)
//			SET_CAM_ACTIVE(cutCam, FALSE)
//		BREAK
//	ENDSWITCH
	
	//advance the camera stage or reset if it has already shown them all
//	IF iCamStage >= 1
//		iCamStage = 0
//	ELSE
//		iCamStage++
//	ENDIF
ENDPROC
