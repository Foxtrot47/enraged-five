

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "minigames_helpers.sch"
USING "minigame_stats_tracker.sch"

ENUM MG_STATS_TRACKER_STATE
	MG_STATS_TRACKER_INIT,	
	MG_STATS_TRACKER_REQUESTS,
	MG_STATS_TRACKER_POPULATE,
	MG_STATS_TRACKER_COMPLETE
ENDENUM

//constants
MG_STATS_TRACKER_STATE		eMGTrackerState = MG_STATS_TRACKER_INIT
SCALEFORM_INDEX				iTrackerMov
structTimer					tTracker

PROC MG_STATS_TRACKER_CLEANUP()	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(iTrackerMov)
	g_bForceKillMGStatTracker = FALSE
	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT(structMGStatsTracker args)
	
	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		MG_STATS_TRACKER_CLEANUP()
	ENDIF
	
	// The script loop
	WHILE TRUE
		SWITCH eMGTrackerState
			CASE MG_STATS_TRACKER_INIT
				iTrackerMov = REQUEST_SCALEFORM_MOVIE("mission_complete")
				
				DEBUG_MESSAGE("Moving to MG_STATS_TRACKER_REQUESTS")
				eMGTrackerState = MG_STATS_TRACKER_REQUESTS
			BREAK
			
			CASE MG_STATS_TRACKER_REQUESTS
				IF HAS_SCALEFORM_MOVIE_LOADED(iTrackerMov)
					DEBUG_MESSAGE("Moving to MG_STATS_TRACKER_POPULATE")
					eMGTrackerState = MG_STATS_TRACKER_POPULATE
				ELSE
					DEBUG_MESSAGE("Waiting on mission_complete scaleform to load")
				ENDIF			
			BREAK
			
			CASE MG_STATS_TRACKER_POPULATE
				CALL_SCALEFORM_MOVIE_METHOD_WITH_STRING(iTrackerMov, "SET_MISSION_TITLE", args.sMGTitle, args.sMGDesc)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(iTrackerMov, "SET_MISSION_TITLE_COLOUR", args.eMGTitleColour.fTitleRed, args.eMGTitleColour.fTitleRed, args.eMGTitleColour.fTitleRed)
				IF args.mgTotals.fMGMedalTotal != -1
				AND NOT IS_STRING_NULL(args.mgTotals.sMGMedalTotal)
					CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(iTrackerMov, "SET_TOTAL", TO_FLOAT(ENUM_TO_INT(args.mgTotals.eMGMedal)), args.mgTotals.fMGMedalTotal, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, args.mgTotals.sMGMedalTotal) 
				ENDIF								
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(iTrackerMov, "SET_MEDAL", TO_FLOAT(ENUM_TO_INT(args.mgTotals.eMGMedal)))				
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(iTrackerMov, "SET_SOCIAL_CLUB_INFO", 0)
				
				PRINTSTRING("Number of data slots to print:")
				PRINTINT(args.iMGSlots+1)
				PRINTNL()
				
				INT I
				REPEAT args.iMGSlots+1 I
					BEGIN_SCALEFORM_MOVIE_METHOD(iTrackerMov, "SET_DATA_SLOT")   
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(args.mgDataSlot[I].iMGSlotNum)						
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(args.mgDataSlot[I].iMGPassed)												
						IF args.mgDataSlot[i].eMGStatType = mgStatTime			
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(mgStatString)) //cheating so we can format it
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)		
							PRINTSTRING("Time in milliseconds: ")
							PRINTINT(FLOOR(args.mgDataSlot[I].fMGObj_1*1000))
							PRINTNL()
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")										
				               ADD_TEXT_COMPONENT_SUBSTRING_TIME(FLOOR(args.mgDataSlot[I].fMGObj_1*1000), TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS)	
				            END_TEXT_COMMAND_SCALEFORM_STRING()																			
							IF NOT IS_STRING_NULL(args.mgDataSlot[I].sMGStat_1)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(args.mgDataSlot[I].sMGStat_1)
							ENDIF	
						ELIF args.mgDataSlot[i].eMGStatType = mgStatStringWithPlayerName 
							IF NOT IS_STRING_NULL(args.mgDataSlot[I].sMGStat_1)
							AND NOT IS_STRING_NULL(args.mgDataSlot[I].sMGStat_2)
								BEGIN_TEXT_COMMAND_SCALEFORM_STRING(args.mgDataSlot[I].sMGStat_2)										
					               ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(args.mgDataSlot[I].sMGStat_1)
					            END_TEXT_COMMAND_SCALEFORM_STRING()								
							ENDIF
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(args.mgDataSlot[I].eMGStatType))
							//This differentiates between passed in ints and floats, so we can properly display decimals and decimal places - AsD
							IF args.mgDataSlot[I].fMGObj_1 % 1 = 0
								//if im a whole number, do this
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(args.mgDataSlot[I].fMGObj_1)
							ELSE
								//if im not a whole number, do this
								BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")										
					               ADD_TEXT_COMPONENT_FLOAT(args.mgDataSlot[I].fMGObj_1, 2)
					            END_TEXT_COMMAND_SCALEFORM_STRING()	
							ENDIF
							IF args.mgDataSlot[I].fMGObj_2 % 1 = 0
								//if im a whole number, do this
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(args.mgDataSlot[I].fMGObj_2)
							ELSE
								//if im not a whole number, do this
								BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")										
					               ADD_TEXT_COMPONENT_FLOAT(args.mgDataSlot[I].fMGObj_2, 2)
					            END_TEXT_COMMAND_SCALEFORM_STRING()	
							ENDIF
							IF NOT IS_STRING_NULL(args.mgDataSlot[I].sMGStat_1)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(args.mgDataSlot[I].sMGStat_1)
							ENDIF							
							IF NOT IS_STRING_NULL(args.mgDataSlot[I].sMGStat_2)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(args.mgDataSlot[I].sMGStat_2)													
							ENDIF
						ENDIF							
					END_SCALEFORM_MOVIE_METHOD()		 					
				ENDREPEAT
				
				IF args.mgTotals.eMGMedal != mgMedalEmpty
				AND NOT IS_STRING_NULL(args.mgTotals.sMGMedalTotal)
					BEGIN_SCALEFORM_MOVIE_METHOD(iTrackerMov, "SET_TOTAL")   
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(args.mgTotals.eMGMedal))
						IF NOT IS_STRING_NULL(args.mgTotals.sMGMedalMedalLabel)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(args.mgTotals.sMGMedalMedalLabel)
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(args.mgTotals.fMGMedalTotal)												
						ENDIF						
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(args.mgTotals.sMGMedalTotal)						
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
				BEGIN_SCALEFORM_MOVIE_METHOD(iTrackerMov, "DRAW_MENU_LIST")
				END_SCALEFORM_MOVIE_METHOD()
				
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)	//JD - 5/24/12 - B*515411 - Puts the internet app/phone away after discussing with Steven Taylor. 
				
				DEBUG_MESSAGE("Moving to MG_STATS_TRACKER_COMPLETE")
				START_TIMER_NOW(tTracker)
				
				//5/4/12 - JD added this to know when scorecard has drawn so prevent B*495668 where in conditions where memory is taking time to be freed up
				IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SCORECARD_IS_DRAWING)
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SCORECARD_IS_DRAWING)
				ENDIF
				
				eMGTrackerState = MG_STATS_TRACKER_COMPLETE
			BREAK
			
			CASE MG_STATS_TRACKER_COMPLETE
				IF ((GET_TIMER_IN_SECONDS(tTracker) > args.fDisplayTime) AND args.fDisplayTime != -1.0)
				OR IS_SCREEN_FADED_OUT()
				//OR GET_MISSION_FLAG()	
				OR g_bForceKillMGStatTracker
					MG_STATS_TRACKER_CLEANUP()	
				ELSE
					//Inform flow components that a mission passed box is displaying.
					SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
				
					//Lining this up with mission_stat_watcher.sc using global values.	
					DRAW_SCALEFORM_MOVIE(iTrackerMov, 	MISSION_PASSED_CENTER_X,
														MISSION_PASSED_CENTER_Y,
														MISSION_PASSED_SIZE_X,
														MISSION_PASSED_SIZE_Y,
														255,255,255,0)	
						
					// Hide help text while the mission passed screen is visible.
					IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_HELP_TEXT) 
						HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
					ENDIF
					
				ENDIF			
			BREAK		
		
		ENDSWITCH
				
		WAIT(0)
	ENDWHILE
	
	MG_STATS_TRACKER_CLEANUP()

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

