//CONST_INT LAUNCHER_POINT_ENTER 1	-- When Imran comes back and says he wants to be able to launch on foot, 
//just uncomment and delete the line below.
//CONST_INT LAUNCHER_POINT_ENTER_IN_VEHICLE 		1
CONST_INT LAUNCHER_CUSTOM_SCRIPT_LAUNCH 		1
CONST_INT LAUNCHER_SNAP_CHECKPOINT_TO_GROUND 	1
#IF IS_DEBUG_BUILD
	CONST_INT LAUNCHER_DONT_CHECK_RELOAD_RANGE 	1
#ENDIF
CONST_INT LAUNCHER_IGNORE_VEHICLE_HEALTH		1
CONST_INT LAUNCHER_HAS_SCENE 					1
CONST_INT LAUNCHER_DONT_FADE_DOWN				1
CONST_INT LAUNCHER_TIME_RESTRICTED				1
CONST_INT LAUNCHER_HAS_TRIGGER_SCENE			1
CONST_INT LAUNCHER_VARIED_VEHICLE_ENTER			1
CONST_INT OR_LAUNCHER_DEBUG_WARP_FORCE_SPAWN_MS_OFFSET 15000

CONST_INT OR_NUM_RACERS 5
USING "flow_public_core.sch"
USING "generic_launcher_header.sch"
//USING "z_volumes.sch"

VECTOR 				vRaceLoc
OFFROAD_RACE_INDEX 	CurrentRaceIndex = OFFROAD_RACE_NONE
structTimer			racerOpdateTimers[5], loanerUpdateTimer, greetTimer, racerDialogueTimer
FLOAT				racerUpdateWaitTime[5], loanerUpdateWaitTime
PED_INDEX			dialogueSpeaker = NULL
BOOL 				bAllowScene = TRUE
BOOL				bHintCam = TRUE
BOOL				bSceneSpawned = FALSE
BOOL				bSaidSecondSpeech = FALSE
BOOL				bSaidThirdSpeech = FALSE
structPedsForConversation dialoguePedsStruct

#IF IS_DEBUG_BUILD	
BOOL				bOffroadDebugWarp = FALSE
#ENDIF

ENUM LOANER_GREETING_STATE
	LGS_INIT = 0,
	LGS_FACING_PLAYER,
	LGS_FACING_PLAYER_WAIT,
	LGS_CHALLENGING_PLAYER,
	LGS_GREETING_PLAYER,
	LGS_GREETING_PLAYER_WAIT
ENDENUM

LOANER_GREETING_STATE eLoanerState = LGS_INIT

STRUCT OR_LAUNCHER_RACER
	PED_INDEX			PedIdx
	VEHICLE_INDEX		VehIdx
	VECTOR				StartPosition
	FLOAT				StartHeading
	VECTOR				PedStandingPosition
	FLOAT				PedStandingHeading
	BOOL				bJustRagdolled = FALSE
	BOOL				bJustWalkedInto = FALSE
	STRING				sIdleDict
	STRING				sIdleClip
ENDSTRUCT

STRUCT OR_LAUNCHER_DATA
	OR_LAUNCHER_RACER		Racers[5]
	STREAMED_MODEL			VehicleModels[5]
	STREAMED_MODEL			AdditionalAssets[5]
	OR_LAUNCHER_RACER		Loaner
	STREAMED_MODEL			LoanerModel[1]
	STREAMED_MODEL			LoanerAsset[1]
	INT						iRagdollCount = 0
ENDSTRUCT

OR_LAUNCHER_DATA orArgs

FUNC STATIC_BLIP_NAME_ENUM GET_OFFROAD_STATIC_BLIP(OFFROAD_RACE_INDEX thisRace)
	SWITCH thisRace
		CASE OFFROAD_RACE_CANYON_CLIFFS
			RETURN STATIC_BLIP_MINIGAME_OFFROAD_RACE5
		CASE OFFROAD_RACE_RIDGE_RUN
			RETURN STATIC_BLIP_MINIGAME_OFFROAD_RACE8
		CASE OFFROAD_RACE_VALLEY_TRAIL
			RETURN STATIC_BLIP_MINIGAME_OFFROAD_RACE9
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			RETURN STATIC_BLIP_MINIGAME_OFFROAD_RACE10
		CASE OFFROAD_RACE_ECO_FRIENDLY
			RETURN STATIC_BLIP_MINIGAME_OFFROAD_RACE11
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			RETURN STATIC_BLIP_MINIGAME_OFFROAD_RACE12
	ENDSWITCH
//	SCRIPT_ASSERT("Can't get a valid static blip for offroad racing. If you bug this assert please assign it to Asa Dang.")
	RETURN STATIC_BLIP_NAME_DUMMY_FINAL
ENDFUNC

PROC LAUNCHER_CUSTOM_SCRIPT_INIT()
	scriptName = "Offroad_Races"
	
	SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)	
	
	// Make the launch size bigger, as we're trying to hit it in a vehicle.
	fLaunchScriptDist = LOCATE_SIZE_ANY_MEANS + 4.0
	
	eMinigame = MINIGAME_OFFROAD_RACES
	SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_ALLOW_MULTIPLE_COPIES)
	
	// The offroad races use a huge stack, and apparently need the mission stack size. 
	// # 100994
	iStackSize = MISSION_STACK_SIZE
		
	#IF IS_DEBUG_BUILD	
		IF GET_GAME_TIMER() < g_ORR_DebugLaunchWarpTime + OR_LAUNCHER_DEBUG_WARP_FORCE_SPAWN_MS_OFFSET
			bOffroadDebugWarp = TRUE
		ENDIF
	#ENDIF
	
	vehicleLaunch = NULL
	iAvailableAfterHour = 0		//always available
	iAvailableBeforeHour = 25
	
	// Can approach on foot, or in vehicle.
	eTransMode = TM_ANY
	
	//Removed death check for B*:1749611
	// Get and store which race we are.
	//IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		// We don't really care if he's dead. Gen launcher handles it.
		CurrentRaceIndex = GET_RACE_INDEX_BY_POSITION(vLaunchLocation, vRaceLoc)
	//ENDIF
	
	eLauncherStaticBlip = GET_OFFROAD_STATIC_BLIP(CurrentRaceIndex)
	fLauncherShutdownDist = TO_FLOAT(GENERIC_LAUNCHER_GET_BLIP_STREAMING_RANGE(eLauncherStaticBlip)) + 5.0
	
	//use the current race index to figure out if we should keep the blip active
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(eMinigame))
//		CPRINTLN(DEBUG_OR_RACES, "Static blip is not active when it should be. Turning it on now!")
//		SET_STATIC_BLIP_ACTIVE_STATE(GET_OFFROAD_STATIC_BLIP(CurrentRaceIndex), TRUE)
		bAllowScene = TRUE
	ELSE
		CPRINTLN(DEBUG_OR_RACES, "Offroad races not available in flow. Terminating.")
		GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
		bAllowScene = FALSE
	ENDIF
	
	CPRINTLN(DEBUG_OR_RACES, "Checking current race against unlocked race")
	CPRINTLN(DEBUG_OR_RACES, "Current race index is... ", CurrentRaceIndex)
	CPRINTLN(DEBUG_OR_RACES, "g_savedGlobals.sOffroadData.eCurrentRace is... ", g_savedGlobals.sOffroadData.eCurrentRace)
	
//	CurrentRaceIndex > g_savedGlobals.sOffroadData.eCurrentRace
	IF CurrentRaceIndex > g_savedGlobals.sOffroadData.eCurrentRace
		//we're at a race we havent unlocked yet.
		CPRINTLN(DEBUG_OR_RACES, "we're at a race we havent unlocked yet.")
		IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(GET_OFFROAD_STATIC_BLIP(CurrentRaceIndex))
			CPRINTLN(DEBUG_OR_RACES, "but somehow the blip is currently visible !!")
		ENDIF 
		eLauncherStaticBlip = STATIC_BLIP_NAME_DUMMY_FINAL
		GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
	ENDIF
	
	IF NOT IS_STATIC_BLIP_CURRENTLY_VISIBLE(GET_OFFROAD_STATIC_BLIP(CurrentRaceIndex)) AND IS_SCREEN_FADED_IN()
		CPRINTLN(DEBUG_OR_RACES, "Static blip isn't visible. We shouldn't be starting up !!")
		//Don't clear static blip if it should be visible (we're on mission, for example)
		IF eLauncherStaticBlip != STATIC_BLIP_NAME_DUMMY_FINAL
			IF GET_STATIC_BLIP_ACTIVE_STATE(eLauncherStaticBlip,TRUE)
				eLauncherStaticBlip = STATIC_BLIP_NAME_DUMMY_FINAL
			ENDIF
		ENDIF
		GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
	ENDIF
	
	IF CurrentRaceIndex = OFFROAD_RACE_MINEWARD_SPIRAL
		CPRINTLN(DEBUG_OR_RACES, "OFFROAD_RACE_MINEWARD_SPIRAL detected! Setting available hours!")
		iAvailableAfterHour = 20		// after 8pm
		iAvailableBeforeHour = 3		// before 4am
	ENDIF
	
	// Prompt is based off the type of race we are: 8 & 11 are full sized, all else are motorcycle.
	IF (CurrentRaceIndex = OFFROAD_RACE_RIDGE_RUN) OR (CurrentRaceIndex = OFFROAD_RACE_ECO_FRIENDLY)
		helpButtonPress = "PLAY_OFFROAD_V"
	ELSE
		helpButtonPress = "PLAY_OFFROAD_M"
	ENDIF
	
ENDPROC


FUNC THREADID LAUNCHER_CUSTOM_RUN_SCRIPT()	
	// Clear all previously printed helps.
	CLEAR_HELP()
//	DO_SCREEN_FADE_OUT(1000)
//	WHILE IS_SCREEN_FADING_OUT()
//		WAIT(0)
//	ENDWHILE
	
	RACE_LAUNCH_DATA launchData
	launchData.raceToLaunch = CurrentRaceIndex
	launchData.raceStartLoc = vRaceLoc
	INT i = 0
	
	REPEAT COUNT_OF(launchData.Racer) i
		launchData.Racer[i] = orArgs.Racers[i].PedIdx
		launchData.RaceVehicle[i] = orArgs.Racers[i].VehIdx
	ENDREPEAT
	
	launchData.Loaner = orArgs.Loaner.PedIdx
	launchData.LoanerVehicle = orArgs.Loaner.VehIdx
		
	DEBUG_MESSAGE("******************** Launching OffroadRacing.sc ********************")
	ANIMPOSTFX_PLAY("SwitchSceneNeutral", 0, TRUE)
	
	WAIT(400)
	THREADID threadTemp = START_NEW_SCRIPT_WITH_ARGS(scriptName, launchData, SIZE_OF(RACE_LAUNCH_DATA), iStackSize)
	SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptName)
	
	
	RETURN threadTemp
ENDFUNC

PROC OR_LAUNCHER_RESTORE_RACE_PATH()
	REMOVE_SCENARIO_BLOCKING_AREAS()
	RESET_SCENARIO_TYPES_ENABLED()
	// clear end roads of vehicles until end of race
	SWITCH CurrentRaceIndex
		CASE OFFROAD_RACE_CANYON_CLIFFS//CanyonCliffs
			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-223.42, 3948.36, 36.52>>, <<-212.37, 3818.97, 37.53>>, 50.0)
			BREAK
		CASE OFFROAD_RACE_RIDGE_RUN//RidgeRun
			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-509.125366,1987.582031,205.330505>>, <<-948.521484,2752.741455,25.351442>>, 300)
			BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL//ValleyTrail
			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-1943.822021, 4462.285645, 0>>, <<-229.078339, 4227.071777, 50>>, 250)
			//SET_ROADS_IN_AREA( <<-199.6227, 4215.4731, 43.7534>>,
			BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH//LakesideSplash
			BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY//EcoFriendly
			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<2242.488, 2568.146, -10>>, <<2242.488, 1568.146, 200>>, 1000.000)
			BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL//MinewardSpiral
			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA( <<2809.9475, 2993.6440, -9.3564>>, <<2952.9688, 2530.6360, 177.1921>>, 500.0 )
			BREAK
	ENDSWITCH
ENDPROC

//CALLED TO ACTIVATE THE NEXT RACE AND ITS BLIP
PROC LAUNCHER_ACTIVATE_NEXT_RACE()

		// Store the best palce we've earned for this race.
		IF (g_savedGlobals.sOffroadData.iRacePlaceEarned < g_savedGlobals.sOffroadData.iRaceBestPlaces[CurrentRaceIndex]) OR 
				(g_savedGlobals.sOffroadData.iRaceBestPlaces[CurrentRaceIndex] = 0)
			// Awesome! Store this place, and update the blip's name!
			g_savedGlobals.sOffroadData.iRaceBestPlaces[CurrentRaceIndex] = g_savedGlobals.sOffroadData.iRacePlaceEarned
			g_savedGlobals.sOffroadData.iRacePlaceEarned = 999
		ENDIF
		
		// If we just played the highest rank race available to us, set us to be allowed to play the next.
		IF (CurrentRaceIndex = g_savedGlobals.sOffroadData.eCurrentRace) OR (g_savedGlobals.sOffroadData.eCurrentRace = OFFROAD_RACE_NONE)//we havent initialized it yet
			IF (g_savedGlobals.sOffroadData.eCurrentRace != OFFROAD_RACE_ECO_FRIENDLY)
				g_savedGlobals.sOffroadData.eCurrentRace = 
					INT_TO_ENUM(OFFROAD_RACE_INDEX, ENUM_TO_INT(g_savedGlobals.sOffroadData.eCurrentRace) + 1)
				
				// Set the next race blip open.
				SWITCH (g_savedGlobals.sOffroadData.eCurrentRace)
					CASE OFFROAD_RACE_RIDGE_RUN
						CPRINTLN(DEBUG_OR_RACES, "unlocking blip active canyon")
					
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8, TRUE)
					BREAK
					
					CASE OFFROAD_RACE_MINEWARD_SPIRAL
						CPRINTLN(DEBUG_OR_RACES, "unlocking blip active mineward")
						
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE8, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12, TRUE)
					BREAK
					
					CASE OFFROAD_RACE_VALLEY_TRAIL
						CPRINTLN(DEBUG_OR_RACES, "unlocking blip active valley")
					
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE8, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE12, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE9, TRUE)
					BREAK
					
					CASE OFFROAD_RACE_LAKESIDE_SPLASH
						CPRINTLN(DEBUG_OR_RACES, "unlocking blip active lakeside")
					
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE8, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE12, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE9, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE9, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE10, TRUE)
					BREAK
					
					CASE OFFROAD_RACE_ECO_FRIENDLY
						CPRINTLN(DEBUG_OR_RACES, "unlocking blip active eco")
					
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE8, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE12, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE9, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE9, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE10, TRUE)
						SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE10, TRUE)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE11, TRUE)
					BREAK
				ENDSWITCH
				CPRINTLN( DEBUG_OR_RACES, "Printing help about unlock." )
				ADD_HELP_TO_FLOW_QUEUE("OFFR_NEW")
			ELSE
				
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
				SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE5, TRUE)
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8, TRUE)
				SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE8, TRUE)
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE9, TRUE)
				SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE9, TRUE)
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE10, TRUE)
				SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE10, TRUE)
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE11, TRUE)
				SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE11, TRUE)
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12, TRUE)
				SET_STATIC_BLIP_HAS_CHECKMARK(STATIC_BLIP_MINIGAME_OFFROAD_RACE12, TRUE)
			ENDIF
			
			
			// We just passed this race, set our blip to be short range.
			// set the blip name to the place we just got.
			SWITCH (g_savedGlobals.sOffroadData.eCurrentRace)
			 	CASE OFFROAD_RACE_CANYON_CLIFFS
					CPRINTLN(DEBUG_OR_RACES, "Making blip active canyon")
			
					SET_STATIC_BLIP_SHORT_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5)
					SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8)
			 	BREAK
				
				CASE OFFROAD_RACE_RIDGE_RUN
					CPRINTLN(DEBUG_OR_RACES, "Making blip active ridge")
				
					SET_STATIC_BLIP_SHORT_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8)
					SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12)
			 	BREAK
				
				CASE OFFROAD_RACE_MINEWARD_SPIRAL
					CPRINTLN( DEBUG_OR_RACES, "Making blip active mineward")
					SET_STATIC_BLIP_SHORT_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12)
					SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE9)
				BREAK
				
				CASE OFFROAD_RACE_VALLEY_TRAIL
					CPRINTLN(DEBUG_OR_RACES, "Making blip active Valley")
				
					SET_STATIC_BLIP_SHORT_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE9)
					SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE10)
			 	BREAK
				
				CASE OFFROAD_RACE_LAKESIDE_SPLASH
					CPRINTLN(DEBUG_OR_RACES, "Making blip active Lakeside")
					SET_STATIC_BLIP_SHORT_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE10)
					SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE11)
			 	BREAK
				
				CASE OFFROAD_RACE_ECO_FRIENDLY
					CPRINTLN(DEBUG_OR_RACES, "Making blip active ECO")
					SET_STATIC_BLIP_SHORT_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE11)
			 	BREAK
			ENDSWITCH
		ENDIF
ENDPROC


// CALLED WHEN THE LAUNCHED SCRIPT ENDS.
PROC LAUNCHER_CUSTOM_SCRIPT_END()	
	// If we won, always unlock the next race.
	// Handle a win flag here.
	IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sOffroadData.iBitFlags, OFFROAD_F_WonCurrentRace)
		CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sOffroadData.iBitFlags, OFFROAD_F_WonCurrentRace)
		OR_LAUNCHER_RESTORE_RACE_PATH()
		LAUNCHER_ACTIVATE_NEXT_RACE()
		
		MAKE_AUTOSAVE_REQUEST()
	ENDIF
ENDPROC

FUNC STRING OR_LAUNCHER_GET_RESPONSE_ANIM()
	INT iRandom = GET_RANDOM_INT_IN_RANGE() % 2
	STRING sRetVal
	
	SWITCH iRandom
		CASE 0
			sRetVal = "gesture_what_hard"
		BREAK
		CASE 1
		DEFAULT
			sRetVal = "gesture_what_soft"
		BREAK
	ENDSWITCH
	
	RETURN sRetVal
ENDFUNC

PROC OR_LAUNCHER_HANDLE_HINT_CAM()
	IF IS_PLAYER_PLAYING(PLAYER_ID()) AND NOT IS_ENTITY_DEAD(orArgs.Loaner.PedIdx)
		INT iCurHour
		iCurHour = GET_CLOCK_HOURS()
		IF (iCurHour <= iAvailableAfterHour) AND (iCurHour >= iAvailableBeforeHour)
			bHintCam = FALSE
			EXIT
		ENDIF
		SET_GAMEPLAY_ENTITY_HINT(orArgs.Loaner.PedIdx, <<0,0,0.75>>)
	ENDIF
ENDPROC

//SPECIAL FUNCTION TO PERFORM A TASK ONLY ONCE
//USED IN THIS CASE TO ACTIVATE THE NEXT RACE BLIP
FUNC BOOL PERFORM_ONE_TIME_TASK()
	CPRINTLN(DEBUG_OR_RACES,"Activating next OR race via 1-time action")
	LAUNCHER_ACTIVATE_NEXT_RACE()
	RETURN TRUE
ENDFUNC

PROC OR_LAUNCHER_HANDLE_LOANER_GREETING()
	INT iCurHour
	iCurHour = GET_CLOCK_HOURS()
	IF (iCurHour <= iAvailableAfterHour) AND (iCurHour >= iAvailableBeforeHour)
		eLoanerState = LGS_INIT
		EXIT
	ENDIF
	IF NOT IS_ENTITY_DEAD( dialogueSpeaker ) AND NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
		SWITCH eLoanerState
			CASE LGS_INIT
				IF VDIST2( GET_ENTITY_COORDS( dialogueSpeaker ), GET_ENTITY_COORDS( PLAYER_PED_ID() ) ) < 8*8
					AND NOT IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
					bSaidSecondSpeech = FALSE
					bSaidThirdSpeech = FALSE
					CPRINTLN( DEBUG_OR_RACES, "Moving from LGS_INIT to LGS_FACING_PLAYER" )
					eLoanerState = LGS_FACING_PLAYER
				ENDIF
			BREAK
			CASE LGS_FACING_PLAYER
				SEQUENCE_INDEX siChallenge
				OPEN_SEQUENCE_TASK(siChallenge)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
				//	TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
				//	TASK_PLAY_ANIM(NULL, "random@street_race", "_streetracer_challenge")
				//	TASK_PLAY_ANIM(NULL, "random@street_race", "_streetracer_wait_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT | AF_LOOPING | AF_NOT_INTERRUPTABLE)
				CLOSE_SEQUENCE_TASK(siChallenge)
				TASK_PERFORM_SEQUENCE(dialogueSpeaker, siChallenge)
				CLEAR_SEQUENCE_TASK(siChallenge)	
				CPRINTLN( DEBUG_OR_RACES, "Moving from LGS_FACING_PLAYER to LGS_FACING_PLAYER_WAIT" )
				eLoanerState = LGS_FACING_PLAYER_WAIT
			BREAK
			CASE LGS_FACING_PLAYER_WAIT
				IF (GET_SCRIPT_TASK_STATUS(dialogueSpeaker, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					OR IS_PED_FACING_PED(dialogueSpeaker, PLAYER_PED_ID(), 20.0))
					AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					TASK_TURN_PED_TO_FACE_ENTITY(dialogueSpeaker, PLAYER_PED_ID(), -1)
					
					IF NOT bSaidThirdSpeech
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(dialogueSpeaker, "PRERACE_CHAT", "A_M_Y_RACER_01_WHITE_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
					ELSE
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(dialogueSpeaker, "PRERACE_TAUNT", "A_M_Y_RACER_01_WHITE_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
					ENDIF
					
					//PLAY_SINGLE_LINE_FROM_CONVERSATION( dialoguePedsStruct, "SPR_OR", "OR_LAUNCH", "OR_LAUNCH_2", CONV_PRIORITY_LOW )
					CPRINTLN( DEBUG_OR_RACES, "Moving from LGS_FACING_PLAYER_WAIT to LGS_GREETING_PLAYER" )
					eLoanerState = LGS_CHALLENGING_PLAYER
				ENDIF
			BREAK
			CASE LGS_CHALLENGING_PLAYER
				IF NOT IS_AMBIENT_SPEECH_PLAYING(dialogueSpeaker)
					RESTART_TIMER_NOW( greetTimer )
				
					TASK_PLAY_ANIM(dialogueSpeaker, "random@street_race", "_streetracer_wait_loop", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT | AF_LOOPING | AF_NOT_INTERRUPTABLE)
					eLoanerState = LGS_GREETING_PLAYER
				ENDIF
			BREAK
			CASE LGS_GREETING_PLAYER
				IF VDIST2( GET_ENTITY_COORDS( dialogueSpeaker ), GET_ENTITY_COORDS( PLAYER_PED_ID() ) ) > 15*15
					CPRINTLN( DEBUG_OR_RACES, "Player is out of range, moving to LGS_INIT" )
					SEQUENCE_INDEX seq
					OPEN_SEQUENCE_TASK( seq )
						TASK_ACHIEVE_HEADING( NULL, orArgs.Loaner.PedStandingHeading, 1000 )
						TASK_PLAY_ANIM( NULL, orArgs.Loaner.sIdleDict, orArgs.Loaner.sIdleClip, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING )
					CLOSE_SEQUENCE_TASK( seq )
					TASK_PERFORM_SEQUENCE( dialogueSpeaker, seq )
					CLEAR_SEQUENCE_TASK( seq )
					eLoanerState = LGS_INIT
				ENDIF
				
				IF IS_TIMER_STARTED(greetTimer) 
					IF GET_TIMER_IN_SECONDS(greetTimer) >= 5.0
						AND NOT bSaidSecondSpeech
						bSaidSecondSpeech = TRUE
						
						eLoanerState = LGS_FACING_PLAYER
					ELIF GET_TIMER_IN_SECONDS(greetTimer) >= 10.0
						AND NOT bSaidThirdSpeech
						bSaidThirdSpeech = TRUE
						eLoanerState = LGS_FACING_PLAYER
					ENDIF
				ENDIF
			BREAK
			CASE LGS_GREETING_PLAYER_WAIT
			
			BREAK
		ENDSWITCH	
	ENDIF
ENDPROC

PROC OR_LAUNCHER_CLEAR_RACE_PATH()
	
	// clear end roads of vehicles until end of race
	SWITCH CurrentRaceIndex
		CASE OFFROAD_RACE_CANYON_CLIFFS//CanyonCliffs
			SET_ROADS_IN_ANGLED_AREA(<<-223.42, 3948.36, 36.52>>, <<-212.37, 3818.97, 37.53>>, 50.0, TRUE, FALSE)
			CLEAR_AREA_OF_VEHICLES(<<-227.43, 3888.90, 36.41>>, 50.0, TRUE)
			SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_HIKER", FALSE)
			BREAK
		CASE OFFROAD_RACE_RIDGE_RUN//RidgeRun
			SET_ROADS_IN_ANGLED_AREA(<<-509.125366,1987.582031,205.330505>>, <<-948.521484,2752.741455,25.351442>>, 300, FALSE, FALSE)
			BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL//ValleyTrail
			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
			SET_ROADS_IN_ANGLED_AREA(<<-1943.822021, 4462.285645, 0>>, <<-229.078339, 4227.071777, 50>>, 250, TRUE, FALSE)
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<-1943.822021, 4462.285645, 0>>, <<-229.078339, 4227.071777, 50>>, 250)
			SET_ROADS_IN_ANGLED_AREA( <<-196.580, 4217.640, 50.0830>>, <<-202.574, 4210.926, 40.0262>>, 10.0, TRUE, FALSE )
			SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_HIKER", FALSE)
			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_EMPTY", FALSE )
			SET_SCENARIO_TYPE_ENABLED("WORLD_MOUNTAIN_LION_WANDER", FALSE)
			BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH//LakesideSplash
			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_EMPTY", TRUE)
			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_BIKE_OFF_ROAD_RACE", TRUE)
			BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY//EcoFriendly
			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_BICYCLE_MOUNTAIN", FALSE)
			SET_ROADS_IN_ANGLED_AREA(<<2242.488, 2568.146, -10>>, <<2242.488, 1568.146, 200>>, 100.000, TRUE, FALSE)
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<2242.488, 2568.146, -10>>, <<2242.488, 1568.146, 200>>, 1000.000)
			BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL//MinewardSpiral
			SET_SCENARIO_GROUP_ENABLED("QUARRY", FALSE)
			SET_ROADS_IN_ANGLED_AREA(<<2809.9475, 2993.6440, -9.3564>>, <<2952.9688, 2530.6360, 177.1921>>, 500.0, TRUE, FALSE)
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<2809.9475, 2993.6440, -9.3564>>, <<2952.9688, 2530.6360, 177.1921>>, 500.0)
			BREAK
	ENDSWITCH
ENDPROC

PROC LAUNCHER_CUSTOM_UNLOAD_ASSETS()
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(orArgs.AdditionalAssets)
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(orArgs.VehicleModels)

ENDPROC

PROC LAUNCHER_CUSTOM_REQUEST_ASSETS()
	
	ADD_STREAMED_MODEL(orArgs.AdditionalAssets, A_M_Y_MOTOX_01) //add ped model
	ADD_STREAMED_MODEL(orArgs.LoanerAsset, A_M_Y_MOTOX_01) // Add the loaner model
	
	//add vehicle models:
	SWITCH CurrentRaceIndex
		CASE OFFROAD_RACE_CANYON_CLIFFS
			ADD_STREAMED_MODEL(orArgs.VehicleModels, SANCHEZ)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, BLAZER)
			BREAK
		CASE OFFROAD_RACE_RIDGE_RUN
			ADD_STREAMED_MODEL(orArgs.VehicleModels, MESA)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, BJXL)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, Patriot)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, dubsta2)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, BFINJECTION)
			BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL
			ADD_STREAMED_MODEL(orArgs.VehicleModels, SANCHEZ)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, BLAZER)
			BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			ADD_STREAMED_MODEL(orArgs.VehicleModels, SANCHEZ)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, BLAZER)
			BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY
			ADD_STREAMED_MODEL(orArgs.VehicleModels, MESA)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, BFINJECTION)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, BJXL)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, SADLER)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, rebel)
			BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			ADD_STREAMED_MODEL(orArgs.VehicleModels, SANCHEZ)
			ADD_STREAMED_MODEL(orArgs.VehicleModels, BLAZER)
			BREAK
	ENDSWITCH
	
	REQUEST_ANIM_DICT("amb@world_human_hang_out_street@male_a@idle_a")	
	REQUEST_ANIM_DICT("amb@world_human_hang_out_street@male_b@idle_a")	
	REQUEST_ANIM_DICT("amb@world_human_hang_out_street@male_c@idle_a")		
	REQUEST_ANIM_DICT("amb@world_human_aa_smoke@male@idle_a")
	REQUEST_ANIM_DICT("random@street_race")
	REQUEST_ANIM_DICT("gestures@m@standing@casual")
	
	IF CurrentRaceIndex = OFFROAD_RACE_CANYON_CLIFFS
		REQUEST_MODEL(prop_fncwood_14c)
		REQUEST_MODEL(prop_fncwood_14e)
	ENDIF
	
//	LOAD_STREAM("INTRO_STREAM", "DIRT_RACES_SOUNDSET")
	
	TEXT_LABEL_63 recName
	recName = "offroadrace"
	
	SWITCH CurrentRaceIndex
		CASE OFFROAD_RACE_CANYON_CLIFFS
			recName += 1
		BREAK
		CASE OFFROAD_RACE_RIDGE_RUN
			recName += 2
		BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			recName += 6
		BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL
			recName += 3
		BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			recName += 4
		BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY
			recName += 5
		BREAK
	ENDSWITCH
	recName += "car"
	REQUEST_VEHICLE_RECORDING(1, recName)
	
	REQUEST_ALL_MODELS(orArgs.AdditionalAssets)
	REQUEST_ALL_MODELS(orArgs.VehicleModels)
	REQUEST_ALL_MODELS(orArgs.LoanerAsset)
	
ENDPROC

FUNC BOOL LAUNCHER_CUSTOM_ASSETS_LOADED()
	RETURN ARE_MODELS_STREAMED(orArgs.VehicleModels) AND
		ARE_MODELS_STREAMED(orArgs.AdditionalAssets) AND
		ARE_MODELS_STREAMED(orArgs.LoanerAsset) AND
		HAS_ANIM_DICT_LOADED("amb@world_human_hang_out_street@male_a@idle_a") AND
		HAS_ANIM_DICT_LOADED("amb@world_human_hang_out_street@male_b@idle_a") AND
		HAS_ANIM_DICT_LOADED("amb@world_human_hang_out_street@male_c@idle_a") AND
		HAS_ANIM_DICT_LOADED("amb@world_human_aa_smoke@male@idle_a") AND
		HAS_ANIM_DICT_LOADED("random@street_race") AND
		HAS_ANIM_DICT_LOADED("gestures@m@standing@casual")
ENDFUNC

PROC LAUNCHER_CUSTOM_RELEASE_ASSETS()
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(orArgs.VehicleModels)
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(orArgs.AdditionalAssets)
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(orArgs.LoanerAsset)
	REMOVE_ANIM_DICT("amb@world_human_hang_out_street@male_a@idle_a")
	REMOVE_ANIM_DICT("amb@world_human_hang_out_street@male_b@idle_a")
	REMOVE_ANIM_DICT("amb@world_human_hang_out_street@male_c@idle_a")
	REMOVE_ANIM_DICT("amb@world_human_aa_smoke@male@idle_a")
	REMOVE_ANIM_DICT("random@street_race")
	REMOVE_ANIM_DICT("gestures@m@standing@casual")
	
	STRING scenarioName
	SWITCH CurrentRaceIndex
		CASE OFFROAD_RACE_CANYON_CLIFFS
			scenarioName = "CanyonCliffs_Start"
			BREAK
		CASE OFFROAD_RACE_RIDGE_RUN
			scenarioName = "RidgeRun_Start"
			BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL
			scenarioName = "ValleyTrail_Start"
			BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			scenarioName = "LakesideSplash_Start"
			BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY
			scenarioName = "EcoFriendly_Start"
			BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			scenarioName = "MinewardSpiral_Start"
			BREAK
	ENDSWITCH
	
//	REQUEST_ANIM_DICT("amb@world_human_aa_smoke@male@idle_a")	
//	REQUEST_ALL_MODELS(orArgs.AdditionalAssets)
//	REQUEST_ALL_MODELS(orArgs.VehicleModels)
//	REQUEST_ALL_MODELS(orArgs.LoanerAsset)
					
	IF NOT IS_STRING_NULL_OR_EMPTY(scenarioName)
		IF DOES_SCENARIO_GROUP_EXIST(scenarioName)
			IF IS_SCENARIO_GROUP_ENABLED(scenarioName)
				SET_SCENARIO_GROUP_ENABLED(scenarioName, FALSE)
			ENDIF
		ELSE
			CPRINTLN(DEBUG_OR_RACES, scenarioName, " scenario group does not exist!")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_OR_RACES, ">>>>>> WARNING! Do we know what race we're in? We didn't get an attached scenario group")
	ENDIF
	
ENDPROC

PROC OR_LAUNCHER_RESET_DATA()

	INT i = 0
	REPEAT COUNT_OF(orArgs.Racers) i 
		orArgs.Racers[i].StartPosition = <<0,0,0>>
		orArgs.Racers[i].StartHeading = 0
	ENDREPEAT
	
	orArgs.Loaner.StartPosition = <<0,0,0>>
	orArgs.Loaner.StartHeading = 0
ENDPROC

PROC OR_LAUNCHER_ADD_RACER(VECTOR thisStartPos, FLOAT thisStartHead, VECTOR thisStandingPos, FLOAT thisStandingHead)

	INT i = 0
	BOOL bFoundSpot = FALSE
	REPEAT COUNT_OF(orArgs.Racers) i 
		IF NOT bFoundSpot AND IS_VECTOR_ZERO(orArgs.Racers[i].StartPosition)
			//add racer to this one
			orArgs.Racers[i].StartPosition = thisStartPos
			orArgs.Racers[i].StartHeading = thisStartHead
			orArgs.Racers[i].PedStandingPosition = thisStandingPos
			orArgs.Racers[i].PedStandingHeading = thisStandingHead
			bFoundSpot = TRUE
		ENDIF
	ENDREPEAT
	
	IF bFoundSpot
		CPRINTLN(DEBUG_OR_RACES, "[OR_LAUNCHER] successfully add a racer to the array. Position:", thisStartPos, " Heading:", thisStartHead)
	ELSE
		CPRINTLN(DEBUG_OR_RACES, "[OR_LAUNCHER] couldn't add a racer to the array!")
	ENDIF
	
	
ENDPROC

PROC OR_LAUNCHER_ADD_LOANER(VECTOR thisStartPos, FLOAT thisStartHead, VECTOR thisStandingPos, FLOAT thisStandingHead)
	IF IS_VECTOR_ZERO(orArgs.Loaner.StartPosition)
		orArgs.Loaner.StartPosition = thisStartPos
		orArgs.Loaner.StartHeading = thisStartHead
		orArgs.Loaner.PedStandingPosition = thisStandingPos
		orArgs.Loaner.PedStandingHeading = thisStandingHead
	ENDIF
ENDPROC

FUNC MODEL_NAMES OR_LAUNCHER_GET_RANDOM_VEHICLE_MODEL(BOOL bIsPlayer = FALSE)
	INT k = 0, i = 0
		
	REPEAT COUNT_OF(orArgs.VehicleModels) i
		IF IS_MODEL_A_VEHICLE(orArgs.VehicleModels[i].thisModel)
			k++
		ENDIF
	ENDREPEAT
	
	//For whatever reason, in this race, only the player is ELIGIBLE for receiving the car in index 5 of the vehicle model array
	IF CurrentRaceIndex = OFFROAD_RACE_RIDGE_RUN
		IF NOT bIsPlayer
			k--
		ENDIF
	ENDIF
	
	k = GET_RANDOM_INT_IN_RANGE(0, k)
	
	CPRINTLN(DEBUG_OR_RACES, "OR_LAUNCHER_GET_RANDOM_VEHICLE_MODEL [random model] k = ", k)
	
	IF orArgs.VehicleModels[k].thisModel = DUMMY_MODEL_FOR_SCRIPT
		IF orArgs.VehicleModels[0].thisModel = DUMMY_MODEL_FOR_SCRIPT
			//add vehicle models:
			SWITCH CurrentRaceIndex
				CASE OFFROAD_RACE_CANYON_CLIFFS		RETURN SANCHEZ		BREAK
				CASE OFFROAD_RACE_RIDGE_RUN			RETURN MESA			BREAK
				CASE OFFROAD_RACE_VALLEY_TRAIL		RETURN SANCHEZ		BREAK
				CASE OFFROAD_RACE_LAKESIDE_SPLASH	RETURN SANCHEZ		BREAK
				CASE OFFROAD_RACE_ECO_FRIENDLY		RETURN MESA			BREAK
				CASE OFFROAD_RACE_MINEWARD_SPIRAL	RETURN SANCHEZ		BREAK
			ENDSWITCH
			CPRINTLN(DEBUG_OR_RACES, "OR_LAUNCHER_GET_RANDOM_VEHICLE_MODEL failed, forcing model to const")
		ELSE
			CPRINTLN(DEBUG_OR_RACES, "OR_LAUNCHER_GET_RANDOM_VEHICLE_MODEL failed, forcing model to index 0")
			RETURN orArgs.VehicleModels[0].thisModel
		ENDIF
	ENDIF
	
	RETURN orArgs.VehicleModels[k].thisModel

ENDFUNC


FUNC MODEL_NAMES OR_LAUNCHER_GET_RANDOM_DRIVER_MODEL()
	//don't have any other models yet
	RETURN A_M_Y_MOTOX_01

ENDFUNC


PROC OR_LAUNCHER_POPULATE_RACERS()
	//15*15
	IF (fFlatSquaredPlayerDistanceToLauncher > 3*3) #IF IS_DEBUG_BUILD OR bOffroadDebugWarp #ENDIF
		#IF IS_DEBUG_BUILD	
			bOffroadDebugWarp = FALSE //only use this flag once.
		#ENDIF
		//take data and populate racers
		// need: vectors, headings, vehicle models
		// might want: animations, tasks...
		SEQUENCE_INDEX seq
		INT idleToPlay
		INT idleDictToPlay
		STRING idleDictName
		STRING idleName
		INT i = 0
		REPEAT COUNT_OF(orArgs.Racers) i
			IF NOT IS_VECTOR_ZERO(orArgs.Racers[i].StartPosition)
//				WHILE IS_SPHERE_VISIBLE(orArgs.Racers[i].StartPosition, 5.0)
//					WAIT(1)
//				ENDWHILE
				CPRINTLN(DEBUG_OR_RACES, "Creating racer with ID ", i)
				orArgs.Racers[i].VehIdx = CREATE_VEHICLE(OR_LAUNCHER_GET_RANDOM_VEHICLE_MODEL(), orArgs.Racers[i].StartPosition, orArgs.Racers[i].StartHeading)
				SET_VEHICLE_ON_GROUND_PROPERLY(orArgs.Racers[i].VehIdx)
				SET_VEHICLE_HAS_STRONG_AXLES(orArgs.Racers[i].VehIdx, TRUE)
				IF IS_VECTOR_ZERO(orArgs.Racers[i].PedStandingPosition)
					orArgs.Racers[i].PedIdx = CREATE_PED_INSIDE_VEHICLE(orArgs.Racers[i].VehIdx, PEDTYPE_CIVMALE, OR_LAUNCHER_GET_RANDOM_DRIVER_MODEL())
					SET_VEHICLE_ENGINE_ON(orArgs.Racers[i].VehIdx, TRUE, FALSE)
				ELSE
					orArgs.Racers[i].PedIdx = CREATE_PED(PEDTYPE_CIVMALE, OR_LAUNCHER_GET_RANDOM_DRIVER_MODEL(), orArgs.Racers[i].PedStandingPosition, orArgs.Racers[i].PedStandingHeading)
					idleToPlay = i//GET_RANDOM_INT_IN_RANGE()%3
					SWITCH idleToPlay
						CASE 0
						CASE 3
							idleName = "idle_a"
							BREAK
						CASE 1
						CASE 4
							idleName = "idle_b"
							BREAK
						DEFAULT
							idleName = "idle_c"
							BREAK
					ENDSWITCH	
					idleDictToPlay = i//GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH idleDictToPlay
						CASE 0
						CASE 4
							idleDictName = "amb@world_human_hang_out_street@male_a@idle_a"
						BREAK
						CASE 1
						CASE 3
							idleDictName = "amb@world_human_hang_out_street@male_b@idle_a"
						BREAK
						CASE 2
						CASE 5
							idleDictName = "amb@world_human_hang_out_street@male_c@idle_a"
						BREAK
						
					ENDSWITCH
					orArgs.Racers[i].sIdleDict = idleDictName
					orArgs.Racers[i].sIdleClip = idleName
					OPEN_SEQUENCE_TASK(seq)
						TASK_PAUSE( NULL, GET_RANDOM_INT_IN_RANGE( 100, 500 ) )
						TASK_PLAY_ANIM(NULL, idleDictName, idleName, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED, GET_RANDOM_FLOAT_IN_RANGE())
					CLOSE_SEQUENCE_TASK(seq)
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( orArgs.Racers[i].PedIdx, TRUE )
					CPRINTLN(DEBUG_OR_RACES, "Tasking racer with smoking animation sequence")
					TASK_PERFORM_SEQUENCE(orArgs.Racers[i].PedIdx, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				SET_PED_HELMET(orArgs.Racers[i].PedIdx, TRUE)
				GIVE_PED_HELMET(orArgs.Racers[i].PedIdx, TRUE)
				
//				WAIT(GET_RANDOM_INT_IN_RANGE(500, 2000))
			ELSE
				CDEBUG1LN(DEBUG_MG_LAUNCHER, "cant spawn this racer. ZERO VECTOR!! ", orArgs.Racers[i].StartPosition)
			ENDIF
		ENDREPEAT
		
		// Adding information for loaner
		CPRINTLN(DEBUG_OR_RACES, "Creating loaner")
		orArgs.Loaner.VehIdx = CREATE_VEHICLE(OR_LAUNCHER_GET_RANDOM_VEHICLE_MODEL(TRUE), orArgs.Loaner.StartPosition, orArgs.Loaner.StartHeading)
		SET_VEHICLE_ON_GROUND_PROPERLY(orArgs.Loaner.VehIdx)
		SET_VEHICLE_HAS_STRONG_AXLES(orArgs.Loaner.VehIdx, TRUE)
		IF IS_VECTOR_ZERO(orArgs.Loaner.PedStandingPosition)
			orArgs.Loaner.PedIdx = CREATE_PED_INSIDE_VEHICLE(orArgs.Loaner.VehIdx, PEDTYPE_CIVMALE, OR_LAUNCHER_GET_RANDOM_DRIVER_MODEL())
			SET_VEHICLE_ENGINE_ON(orArgs.Loaner.VehIdx, TRUE, FALSE)
		ELSE
			orArgs.Loaner.PedIdx = CREATE_PED(PEDTYPE_CIVMALE, OR_LAUNCHER_GET_RANDOM_DRIVER_MODEL(), orArgs.Loaner.PedStandingPosition, orArgs.Loaner.PedStandingHeading)
			idleToPlay = GET_RANDOM_INT_IN_RANGE()%3
			SWITCH idleToPlay
				CASE 0
					idleName = "idle_a"
					BREAK
				CASE 1
					idleName = "idle_b"
					BREAK
				DEFAULT
					idleName = "idle_c"
					BREAK
			ENDSWITCH	
			orArgs.Loaner.sIdleDict = "amb@world_human_aa_smoke@male@idle_a"
			orArgs.Loaner.sIdleClip = idleName
			OPEN_SEQUENCE_TASK(seq)
				TASK_STAND_STILL(NULL, CLAMP_INT(GET_RANDOM_INT_IN_RANGE()%1000, 1, 1000))
				TASK_PLAY_ANIM(NULL, "amb@world_human_aa_smoke@male@idle_a", idleName, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, GET_RANDOM_FLOAT_IN_RANGE())
			CLOSE_SEQUENCE_TASK(seq)
			CPRINTLN(DEBUG_OR_RACES, "Tasking racer with smoking animation sequence")
			TASK_PERFORM_SEQUENCE(orArgs.Loaner.PedIdx, seq)
			CLEAR_SEQUENCE_TASK(seq)
		ENDIF
	ELSE
		IF fFlatSquaredPlayerDistanceToLauncher < 15*15
			CDEBUG1LN(DEBUG_MG_LAUNCHER, "we're too close to spawn dudes!")
		ELSE
			CDEBUG1LN(DEBUG_MG_LAUNCHER, "NO CLUE why we cant spawn dudes!")
		ENDIF
	ENDIF
	
	dialogueSpeaker = orArgs.Loaner.PedIdx
	
	IF NOT IS_ENTITY_DEAD(orArgs.Racers[0].PedIdx)
		ADD_PED_FOR_DIALOGUE(dialoguePedsStruct, 1, orArgs.Racers[0].PedIdx, "MALE1")
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(orArgs.Racers[1].PedIdx)
		ADD_PED_FOR_DIALOGUE(dialoguePedsStruct, 2, orArgs.Racers[1].PedIdx, "MALE2")
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(orArgs.Racers[2].PedIdx)
		ADD_PED_FOR_DIALOGUE(dialoguePedsStruct, 3, orArgs.Racers[3].PedIdx, "MALE3")
	ENDIF
	
	IF NOT IS_TIMER_STARTED(racerDialogueTimer)
		START_TIMER_NOW(racerDialogueTimer)
	ELSE
		RESTART_TIMER_NOW(racerDialogueTimer)
	ENDIF
	
//	ADD_PED_FOR_DIALOGUE(dialoguePedsStruct, 1, dialogueSpeaker, "OR_Taunt")
ENDPROC

PROC ORR_LAUNCHER_UPDATE_AMBIENT_DIALOGUE()
	IF NOT IS_TIMER_STARTED(racerDialogueTimer)
		START_TIMER_NOW(racerDialogueTimer)
		
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(dialogueSpeaker)
	AND NOT IS_ENTITY_DEAD(dialogueSpeaker)
		IF IS_AMBIENT_SPEECH_PLAYING(dialogueSpeaker)
			EXIT
		ENDIF
	ENDIF
	
	IF GET_TIMER_IN_SECONDS(racerDialogueTimer) >= 8.0
		INT iSpeaker = (GET_RANDOM_INT_IN_RANGE() % 3) + 1
		
		IF iSpeaker > 0 AND iSpeaker <= 3
			IF IS_ENTITY_DEAD(orArgs.Racers[iSpeaker-1].PedIdx)
				EXIT
			ENDIF
			STRING sRoot
			SWITCH iSpeaker
				CASE 1		sRoot = "ORR_GUY1"		BREAK
				CASE 2		sRoot = "ORR_GUY2"		BREAK
				CASE 3		sRoot = "ORR_GUY3"		BREAK
			ENDSWITCH
			CREATE_CONVERSATION(dialoguePedsStruct, "ORRAUD", sRoot, CONV_PRIORITY_AMBIENT_MEDIUM)
			
			RESTART_TIMER_NOW(racerDialogueTimer)
		ENDIF
	ENDIF
ENDPROC

PROC ORR_LAUNCHER_SET_PEDS_AGGRESSIVE()
	CPRINTLN(DEBUG_OR_RACES, "Setting peds as aggresive.")
	PED_INDEX pedArray[10]
	GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), pedArray, PEDTYPE_INVALID)
	SEQUENCE_INDEX siSeq
	INT i = 0
	REPEAT COUNT_OF(pedArray) i
		IF DOES_ENTITY_EXIST(pedArray[i]) AND NOT IS_PED_INJURED(pedArray[i])// AND NOT IS_PED_IN_ANY_VEHICLE(pedArray[i])
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
				TASK_SMART_FLEE_PED(pedArray[i], PLAYER_PED_ID(), 100, -1)
			ELSE
				OPEN_SEQUENCE_TASK( siSeq )
					TASK_TURN_PED_TO_FACE_ENTITY( NULL, PLAYER_PED_ID(), GET_RANDOM_INT_IN_RANGE( 300, 900 ) )
					TASK_COMBAT_PED(NULL, PLAYER_PED_ID(), COMBAT_PED_NONE, TASK_THREAT_RESPONSE_NONE)
				CLOSE_SEQUENCE_TASK( siSeq )
				
				TASK_PERFORM_SEQUENCE( pedArray[i], siSeq )
				CLEAR_SEQUENCE_TASK( siSeq )
			ENDIF
		ENDIF
	ENDREPEAT

	SWITCH (g_savedGlobals.sOffroadData.eCurrentRace)
		CASE OFFROAD_RACE_CANYON_CLIFFS
			CPRINTLN(DEBUG_OR_RACES, "Terminating offroad race")
			LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE)
			//GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
		BREAK
		
		CASE OFFROAD_RACE_RIDGE_RUN
			LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE)
		//	GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
		BREAK
		
		CASE OFFROAD_RACE_VALLEY_TRAIL
			LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE)
			//GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
		BREAK
		
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE)
			//GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
		BREAK
		
		CASE OFFROAD_RACE_ECO_FRIENDLY
			LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE)
			//GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
		BREAK
		
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE)
			//GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
		BREAK
	ENDSWITCH
	
//	TASK_COMBAT_PED()
ENDPROC

/// PURPOSE: has triggered panic on a particular ped
FUNC BOOL ORR_LAUNCHER_TEST_PED_FOR_PANIC(PED_INDEX testPed, BOOL bInjuredCheck = TRUE, BOOL bCombatCheck = FALSE, BOOL bTargettingCheck = FALSE, BOOL bIncludeTouch = FALSE)
	PED_INDEX Player = PLAYER_PED_ID()
	VECTOR tempVec
	
	IF orArgs.iRagdollCount >= 3
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(testPed)
		IF NOT IS_PED_INJURED(testPed)
			tempVec = GET_ENTITY_COORDS(testPed)
			
			IF bInjuredCheck
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(testPed, PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
			
			//player is in combat with the ped
			IF bCombatCheck
				IF IS_PED_IN_COMBAT(testPed, Player)
					CPRINTLN(DEBUG_OR_RACES, "Ped panicking because they are in combat with the player")
					RETURN TRUE
				ENDIF
			ENDIF
			
			//player is touching the target
			IF bIncludeTouch
				IF IS_ENTITY_TOUCHING_ENTITY(Player, testPed)
					CPRINTLN(DEBUG_OR_RACES, "Ped panicking because player is touching them")
					RETURN TRUE
				ENDIF
			ENDIF
			
			//player is targetting the enemy within 10 m
			IF bTargettingCheck
				IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), testPed)
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), testPed)
//					IF IS_PED_FACING_PED(testPed, Player, 90)
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(testPed) < 20
							CPRINTLN(DEBUG_OR_RACES, "Ped panicking because player is targetting ped!")
							RETURN TRUE
						ENDIF
//					ENDIF
				ENDIF
			ENDIF

			//player has damaged the target's vehicle (if they are in one)
			IF IS_PED_IN_ANY_VEHICLE(testPed)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_IN(testPed), Player)
					CPRINTLN(DEBUG_OR_RACES, "Ped panicking because player damaged the ped's vehicle!")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_PED_FLEEING(testPed)
				CPRINTLN(DEBUG_OR_RACES, "Ped responding to code driven event and fleeing!")
				RETURN TRUE
			ENDIF
			
			//Player is shooting near the target
			IF IS_PED_ARMED(Player, ENUM_TO_INT(WF_INCLUDE_GUN))
				IF IS_PED_SHOOTING(Player)
					IF IS_ENTITY_AT_COORD(testPed, GET_ENTITY_COORDS(Player), <<45, 45, 45>>)
						CPRINTLN(DEBUG_OR_RACES, "Ped panicking because player is shooting!")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BULLET_IN_AREA(tempVec, 4.0, TRUE)
				CPRINTLN(DEBUG_OR_RACES, "Ped panicking because bullet in area!")
				RETURN TRUE
			ENDIF
			
			//check to see if player is threatening ped with a thrown weapon
			IF IS_PROJECTILE_IN_AREA(<<tempVec.x-5, tempVec.y-5, tempVec.z-5>>, <<tempVec.x+5, tempVec.y+5, tempVec.z+5>>)
				CPRINTLN(DEBUG_OR_RACES, "Ped panicking because projectile in area!")
				RETURN TRUE
			ENDIF
			
			IF IS_PED_BEING_JACKED(testPed)
				IF GET_PEDS_JACKER(testPed) = Player
					CPRINTLN(DEBUG_OR_RACES, "Ped panicking because player is jacking their vehicle!")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, tempVec, 25)
			AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_STEAM, tempVec, 25)
			AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_WATER_HYDRANT, tempVec, 25)
				CPRINTLN(DEBUG_OR_RACES, "Ped panicking becuase there is an explosion near them")
				RETURN TRUE
			ENDIF
			
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(testPed)
		ELSE
			IF bInjuredCheck
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC OR_LAUNCHER_UPDATE_RACERS()
			
	INT waitTime, revTime
	INT i = 0
	SEQUENCE_INDEX seq
	REPEAT OR_NUM_RACERS i
		IF DOES_ENTITY_EXIST(orArgs.Racers[i].VehIdx) AND DOES_ENTITY_EXIST(orArgs.Racers[i].PedIdx)
			IF ORR_LAUNCHER_TEST_PED_FOR_PANIC(orArgs.Racers[i].PedIdx) OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), orArgs.Racers[i].VehIdx, TRUE)
				ORR_LAUNCHER_SET_PEDS_AGGRESSIVE()
				GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE) //in case the player tries to start the race at the same time.
			ENDIF
			IF NOT IS_PED_INJURED(orArgs.Racers[i].PedIdx)
				IF IS_TIMER_STARTED(racerOpdateTimers[i]) 
					IF GET_TIMER_IN_SECONDS(racerOpdateTimers[i]) > racerUpdateWaitTime[i]
						IF IS_VEHICLE_DRIVEABLE(orArgs.Racers[i].VehIdx) AND IS_PED_IN_VEHICLE(orArgs.Racers[i].PedIdx, orArgs.Racers[i].VehIdx)
							waitTime = GET_RANDOM_INT_IN_RANGE(250, 500)
							revTime = GET_RANDOM_INT_IN_RANGE(250, 500)
							OPEN_SEQUENCE_TASK(seq)
								TASK_VEHICLE_TEMP_ACTION(NULL, orArgs.Racers[i].VehIdx, TEMPACT_WAIT, waitTime)
								TASK_VEHICLE_TEMP_ACTION(NULL, orArgs.Racers[i].VehIdx, TEMPACT_REV_ENGINE, revTime)
							CLOSE_SEQUENCE_TASK(seq)
							IF NOT IS_PED_INJURED(orArgs.Racers[i].PedIdx)
								TASK_PERFORM_SEQUENCE(orArgs.Racers[i].PedIdx, seq)
							ENDIF
							CLEAR_SEQUENCE_TASK(seq)
						ELSE
							//tell peds to update idle animations
							//TASK_CHAT_TO_PED()
						ENDIF
						RESTART_TIMER_NOW(racerOpdateTimers[i])
						racerUpdateWaitTime[i] = GET_RANDOM_FLOAT_IN_RANGE(0, 3)
					ENDIF
				ELSE
					START_TIMER_NOW(racerOpdateTimers[i])
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD( orArgs.Racers[i].PedIdx )
				IF IS_ENTITY_TOUCHING_ENTITY( PLAYER_PED_ID(), orArgs.Racers[i].PedIdx ) //IS_PED_RESPONDING_TO_EVENT( orArgs.Racers[i].PedIdx, EVENT_PED_COLLISION_WITH_PLAYER )
					AND NOT IS_PED_RAGDOLL( orArgs.Racers[i].PedIdx )
					AND NOT IS_PED_GETTING_UP( orArgs.Racers[i].PedIdx )
					orArgs.Racers[i].bJustWalkedInto = TRUE
				ELIF OrArgs.Racers[i].bJustWalkedInto
					OPEN_SEQUENCE_TASK( seq )
						TASK_ACHIEVE_HEADING( NULL, orArgs.Racers[i].PedStandingHeading, 1000 )
						TASK_PLAY_ANIM( NULL, orArgs.Racers[i].sIdleDict, orArgs.Racers[i].sIdleClip, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING )
					CLOSE_SEQUENCE_TASK( seq )
					
					TASK_PERFORM_SEQUENCE( orArgs.Racers[i].PedIdx, seq )
					CLEAR_SEQUENCE_TASK( seq )
					
					orArgs.Racers[i].bJustWalkedInto = FALSE					
				ENDIF
				
				IF IS_PED_RAGDOLL( orArgs.Racers[i].PedIdx )
					OR IS_PED_GETTING_UP( orArgs.Racers[i].PedIdx )
					IF NOT orArgs.Racers[i].bJustRagdolled
						AND (IS_PED_RAGDOLL(  orArgs.Racers[i].PedIdx )
						OR IS_PED_GETTING_UP( orArgs.Racers[i].PedIdx ) )
						++orArgs.iRagdollCount
					ENDIF
					orArgs.Racers[i].bJustRagdolled = TRUE	
				ELIF orArgs.Racers[i].bJustRagdolled
					OPEN_SEQUENCE_TASK( seq )
						TASK_TURN_PED_TO_FACE_ENTITY( NULL, PLAYER_PED_ID(), 1000 )
						TASK_PLAY_ANIM( NULL, "gestures@m@standing@casual", OR_LAUNCHER_GET_RESPONSE_ANIM() )
						TASK_ACHIEVE_HEADING( NULL, orArgs.Racers[i].PedStandingHeading, 1000 )
						TASK_PLAY_ANIM( NULL, orArgs.Racers[i].sIdleDict, orArgs.Racers[i].sIdleClip, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING )
					CLOSE_SEQUENCE_TASK( seq )
					
					TASK_PERFORM_SEQUENCE( orArgs.Racers[i].PedIdx, seq )
					CLEAR_SEQUENCE_TASK( seq )
					
					orArgs.Racers[i].bJustRagdolled = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	////////////////////// LOANER ///////////////////////
	IF DOES_ENTITY_EXIST(orArgs.Loaner.VehIdx) AND DOES_ENTITY_EXIST(orArgs.Loaner.PedIdx)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), orArgs.Loaner.VehIdx)
			vehicleLaunch = orArgs.Loaner.VehIdx
		ENDIF
		IF ORR_LAUNCHER_TEST_PED_FOR_PANIC(orArgs.Loaner.PedIdx)// OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), orArgs.Loaner.VehIdx)
			ORR_LAUNCHER_SET_PEDS_AGGRESSIVE()
			GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE) //in case the player tries to start the race at the same time.
		ENDIF
		IF NOT IS_PED_INJURED(orArgs.Loaner.PedIdx)
			IF IS_TIMER_STARTED(loanerUpdateTimer) 
				IF GET_TIMER_IN_SECONDS(loanerUpdateTimer) > loanerUpdateWaitTime
					IF IS_VEHICLE_DRIVEABLE(orArgs.Loaner.VehIdx) AND IS_PED_IN_VEHICLE(orArgs.Loaner.PedIdx, orArgs.Loaner.VehIdx)
						waitTime = GET_RANDOM_INT_IN_RANGE(250, 500)
						revTime = GET_RANDOM_INT_IN_RANGE(250, 500)
						OPEN_SEQUENCE_TASK(seq)
							TASK_VEHICLE_TEMP_ACTION(NULL, orArgs.Loaner.VehIdx, TEMPACT_WAIT, waitTime)
							TASK_VEHICLE_TEMP_ACTION(NULL, orArgs.Loaner.VehIdx, TEMPACT_REV_ENGINE, revTime)
						CLOSE_SEQUENCE_TASK(seq)
						IF NOT IS_PED_INJURED(orArgs.Loaner.PedIdx)
							TASK_PERFORM_SEQUENCE(orArgs.Loaner.PedIdx, seq)
						ENDIF
						CLEAR_SEQUENCE_TASK(seq)
					ELSE
						//tell peds to update idle animations
						//TASK_CHAT_TO_PED()
					ENDIF
					RESTART_TIMER_NOW(loanerUpdateTimer)
					loanerUpdateWaitTime = GET_RANDOM_FLOAT_IN_RANGE(0, 3)
				ENDIF
			ELSE
				START_TIMER_NOW(loanerUpdateTimer)
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD( orArgs.Loaner.PedIdx )
			IF IS_ENTITY_TOUCHING_ENTITY( PLAYER_PED_ID(), orArgs.Loaner.PedIdx ) //IS_PED_RESPONDING_TO_EVENT( orArgs.Racers[i].PedIdx, EVENT_PED_COLLISION_WITH_PLAYER )
				AND NOT IS_PED_RAGDOLL( orArgs.Loaner.PedIdx )
				AND NOT IS_PED_GETTING_UP( orArgs.Loaner.PedIdx )
				orArgs.Loaner.bJustWalkedInto = TRUE
			ELIF OrArgs.Loaner.bJustWalkedInto
				OPEN_SEQUENCE_TASK( seq )
					TASK_ACHIEVE_HEADING( NULL, orArgs.Loaner.PedStandingHeading, 1000 )
					TASK_PLAY_ANIM( NULL, orArgs.Loaner.sIdleDict, orArgs.Loaner.sIdleClip, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING )
				CLOSE_SEQUENCE_TASK( seq )
				
				TASK_PERFORM_SEQUENCE( orArgs.Loaner.PedIdx, seq )
				CLEAR_SEQUENCE_TASK( seq )
				
				OrArgs.Loaner.bJustWalkedInto = FALSE
			ENDIF
			IF IS_PED_RAGDOLL( orArgs.Loaner.PedIdx )
				OR IS_PED_GETTING_UP( orArgs.Loaner.PedIdx )
				IF NOT orArgs.Loaner.bJustRagdolled
					++orArgs.iRagdollCount
				ENDIF
				orArgs.Loaner.bJustRagdolled = TRUE	
			ELIF orArgs.Loaner.bJustRagdolled
				OPEN_SEQUENCE_TASK( seq )
					TASK_TURN_PED_TO_FACE_ENTITY( NULL, PLAYER_PED_ID(), 1000 )
					TASK_PLAY_ANIM( NULL, "gestures@m@standing@casual", OR_LAUNCHER_GET_RESPONSE_ANIM() )
					TASK_ACHIEVE_HEADING( NULL, orArgs.Loaner.PedStandingHeading, 1000 )
					TASK_PLAY_ANIM( NULL, orArgs.Loaner.sIdleDict, orArgs.Loaner.sIdleClip )
				CLOSE_SEQUENCE_TASK( seq )
				
				TASK_PERFORM_SEQUENCE( orArgs.Loaner.PedIdx, seq )
				CLEAR_SEQUENCE_TASK( seq )
				
				eLoanerState = LGS_GREETING_PLAYER
				RESTART_TIMER_NOW( greetTimer )
				
				orArgs.Loaner.bJustRagdolled = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	/////////////////// END LOANER /////////////////
//	IF IS_TIMER_STARTED(dialogueTimer)
//		IF GET_TIMER_IN_SECONDS(dialogueTimer) > dialogueWaitTime
//			OR_LAUNCHER_RACER	dialogueRacer
//			TEXT_LABEL_15 launchLineLabel = "OR_LAUNCH_"
//			launchLineLabel += GET_RANDOM_INT_IN_RANGE(1, 9)
//			dialogueRacer = orArgs.Loaner
//			dialogueSpeaker = orArgs.Loaner.PedIdx//dialogueRacer.PedIdx
//			IF DOES_ENTITY_EXIST(dialogueRacer.VehIdx) AND DOES_ENTITY_EXIST(dialogueRacer.PedIdx)
//				IF NOT IS_PED_INJURED(dialogueSpeaker) AND IS_PED_IN_VEHICLE(dialogueRacer.PedIdx, dialogueRacer.VehIdx)
//					IF GET_DISTANCE_BETWEEN_ENTITIES(dialogueSpeaker, PLAYER_PED_ID()) <= 40.0
//	 					PLAY_SINGLE_LINE_FROM_CONVERSATION(dialoguePedsStruct, "SPR_OR", "OR_LAUNCH", launchLineLabel, CONV_PRIORITY_LOW)
//					ENDIF
//				ENDIF
//			ENDIF
//			RESTART_TIMER_NOW(dialogueTimer)
//			dialogueWaitTime = GET_RANDOM_FLOAT_IN_RANGE(6, 15)
//		ENDIF
//	ELSE
//		START_TIMER_NOW(dialogueTimer)
//	ENDIF
	
	
ENDPROC



/// PURPOSE:
///    Creates the other guys at the shooting range.
PROC LAUNCHER_CUSTOM_SPAWN_SCENE()	
	INT iCurHour
	iCurHour = GET_CLOCK_HOURS()
	IF ((iCurHour <= iAvailableAfterHour) AND (iCurHour >= iAvailableBeforeHour)) OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
		EXIT
	ENDIF
	
	IF bAllowScene
		STRING scenarioName
		OR_LAUNCHER_CLEAR_RACE_PATH()
			
		OR_LAUNCHER_RESET_DATA()
		
		SWITCH CurrentRaceIndex
			CASE OFFROAD_RACE_CANYON_CLIFFS //Canyon Cliffs
				//setup racers starting positions and headings
				OR_LAUNCHER_ADD_RACER(<<-1930.38, 4441.90, 38.12>>, 260.86, <<-1933.4417, 4440.8657, 37.2504>>, 184.1250)
				OR_LAUNCHER_ADD_RACER(<<-1931.24, 4440.26, 38.05>>, 257.2117, <<-1932.7954, 4438.2749, 37.4020>>, 78.7749)
				OR_LAUNCHER_ADD_RACER(<<-1935.01, 4442.57, 37.48>>, 263.59, <<-1934.9640, 4438.3062, 37.1741>>, 297.2304)
				OR_LAUNCHER_ADD_RACER(<<-1935.50, 4440.32, 37.48>>, 258.09, <<-1937.2506, 4438.7612, 36.9256>>, 23.8377)
				OR_LAUNCHER_ADD_RACER(<<-1939.63, 4440.46, 37.25>>, 253.32, <<-1938.3187, 4439.9131, 36.7166>>, 250.6112)
				OR_LAUNCHER_ADD_LOANER(<<-1939.5636, 4443.3057, 36.3803>>, 262.3551, <<-1936.2085, 4444.1250, 36.8594>>, 123.8187)
				BREAK
			CASE OFFROAD_RACE_RIDGE_RUN //Ridge Run
				OR_LAUNCHER_ADD_RACER(<<-520.2097, 2010.8163, 203.6035>>, 17.0169, <<-517.2971, 2008.3831, 204.0454>>, 2.9896)
				OR_LAUNCHER_ADD_RACER(<<-516.9937, 2013.9797, 203.5907>>, 19.3803, <<-517.1741, 2010.8132, 203.8412>>, 125.7555)
				OR_LAUNCHER_ADD_RACER(<<-518.3414, 2005.4489, 204.1878>>, 20.7231, <<-519.0882, 2013.6492, 203.3304>>, 195.7951 )
				OR_LAUNCHER_ADD_RACER(<<-514.8247, 2008.5737, 204.3629>>, 20.2278, <<-514.2933, 2004.9231, 204.5935>>, 126.8655)
				OR_LAUNCHER_ADD_LOANER(<<-516.1545, 1999.3740, 204.7715>>, 18.7907, <<-515.4201, 2003.6216, 204.5549>>, 1.7570)
				BREAK
			CASE OFFROAD_RACE_VALLEY_TRAIL //Valley Trail
				OR_LAUNCHER_ADD_RACER(<<-225.41, 4227.53, 44.45>>, 82.70, <<-231.9795, 4226.3101, 43.8587>>, 353.3938)
				OR_LAUNCHER_ADD_RACER(<<-233.16, 4225.81, 44.29>>, 76.66, <<-231.3181, 4229.2505, 43.8681>>, 179.3787)
				OR_LAUNCHER_ADD_RACER(<<-233.33, 4228.89, 44.35>>, 80.88, <<-229.4769, 4226.1016, 43.8909>>, 311.8915)
				OR_LAUNCHER_ADD_RACER(<<-229.58, 4225.21, 44.34>>, 75.78, <<-227.6066, 4228.8672, 43.9069>>, 186.6535)
				OR_LAUNCHER_ADD_RACER(<<-228.85, 4227.71, 44.57>>, 83.38, <<-226.7015, 4226.6826, 43.8597>>, 28.0083)
				OR_LAUNCHER_ADD_LOANER(<<-225.6736, 4224.5034, 43.8435>>, 77.7579, <<-227.2893, 4224.6987, 43.8335>>, 36.5500)
				BREAK
			CASE OFFROAD_RACE_LAKESIDE_SPLASH //Lakeside Splash
				OR_LAUNCHER_ADD_RACER(<<1608.91, 3845.06, 33.94>>, 307.24,  <<1610.4760, 3839.1633, 32.9854>>, 29.8332)
				OR_LAUNCHER_ADD_RACER(<<1606.20, 3838.41, 33.62>>, 307.44, <<1612.9076, 3841.4924, 32.9721>>, 78.3000)
				OR_LAUNCHER_ADD_RACER(<<1610.47, 3842.26, 33.61>>, 311.44, <<1608.0966, 3842.6758, 33.3217>>, 198.5355)
				OR_LAUNCHER_ADD_RACER(<<1604.49, 3840.42, 34.18>>, 308.19, <<1608.4274, 3837.3677, 32.9989>>, 9.9463)
				OR_LAUNCHER_ADD_LOANER(<<1603.4052, 3836.3240, 33.1144>>, 305.0550, <<1605.7883, 3840.2363, 33.2826>>, 288.7775)
				BREAK
			CASE OFFROAD_RACE_ECO_FRIENDLY //Eco Friendly
				OR_LAUNCHER_ADD_RACER(<<2046.7721, 2130.4421, 91.9486>>, 233.1493, <<2036.1232, 2133.9807, 92.6150>>, 260.0519)
				OR_LAUNCHER_ADD_RACER(<<2045.3445, 2126.3523, 91.9358>>, 236.3633, <<2044.4219, 2128.9607, 92.0880>>, 47.8797)
				OR_LAUNCHER_ADD_RACER(<<2036.5120, 2137.8071, 92.6507>>, 237.2446, <<2041.7780, 2131.3538, 92.4544>>, 226.7534)
				OR_LAUNCHER_ADD_RACER(<<2041.7390, 2134.3242, 92.4013>>, 232.9560, <<2039.6033, 2132.5635, 92.6648>>, 61.0834)
				OR_LAUNCHER_ADD_RACER(<<2039.7745, 2130.0249, 92.5324>>, 241.6337, <<2038.7444, 2134.5232, 92.6235>>, 128.9800)
				OR_LAUNCHER_ADD_LOANER(<<2030.8984, 2134.1968, 92.5014>>, 249.4471, <<2036.5801, 2135.3821, 92.6050>>, 233.2111)
				BREAK
			CASE OFFROAD_RACE_MINEWARD_SPIRAL // Mineward Spiral
				OR_LAUNCHER_ADD_RACER(<<2992.58, 2786.28, 43.21>>, 26.81, <<2998.7720, 2777.3918, 42.1008>>, 29.7228)
				OR_LAUNCHER_ADD_RACER(<<2995.76, 2787.72, 43.26>>, 33.42, <<2993.1204, 2779.7830, 42.3128>>, 286.9825)
				OR_LAUNCHER_ADD_RACER(<<2994.91, 2779.79, 42.73>>, 11.43, <<2999.2527, 2783.9700, 42.5421>>, 108.7426)
				OR_LAUNCHER_ADD_RACER(<<2999.44, 2781.11, 43.11>>, 23.89, <<2997.5823, 2787.4763, 42.6953>>, 157.4885)
				OR_LAUNCHER_ADD_RACER(<<3000.06, 2775.26, 42.47>>, 17.16, <<2994.7188, 2784.9050, 42.6444>>, 213.8879)//<<2992.8938, 2787.8120, 42.7398>>, 214.0832)
				OR_LAUNCHER_ADD_LOANER(<<2995.5249, 2775.5225, 41.9800>>, 4.3249, <<2993.5110, 2776.9482, 42.0941>>, 305.3785)
				BREAK
		ENDSWITCH
		
		SWITCH CurrentRaceIndex
			CASE OFFROAD_RACE_CANYON_CLIFFS
				scenarioName = "CanyonCliffs_Start"
				BREAK
			CASE OFFROAD_RACE_RIDGE_RUN
				scenarioName = "RidgeRun_Start"
				BREAK
			CASE OFFROAD_RACE_VALLEY_TRAIL
				scenarioName = "ValleyTrail_Start"
				BREAK
			CASE OFFROAD_RACE_LAKESIDE_SPLASH
				scenarioName = "LakesideSplash_Start"
				BREAK
			CASE OFFROAD_RACE_ECO_FRIENDLY
				scenarioName = "EcoFriendly_Start"
				BREAK
			CASE OFFROAD_RACE_MINEWARD_SPIRAL
				scenarioName = "MinewardSpiral_Start"
				BREAK
		ENDSWITCH
						
		IF NOT IS_STRING_NULL_OR_EMPTY(scenarioName)
			IF DOES_SCENARIO_GROUP_EXIST(scenarioName)
				IF NOT IS_SCENARIO_GROUP_ENABLED(scenarioName)
					SET_SCENARIO_GROUP_ENABLED(scenarioName, TRUE)
					CPRINTLN( DEBUG_OR_RACES, scenarioName, " group exists and is being made!" )
				ENDIF
			ELSE
				CPRINTLN(DEBUG_OR_RACES, scenarioName, " scenario group does not exist!")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_OR_RACES, ">>>>>> WARNING! Do we know what race we're in? We didn't get an attached scenario group")
		ENDIF
	ENDIF
		//create racers
		OR_LAUNCHER_POPULATE_RACERS()
	
ENDPROC

FUNC INT ORR_GET_NUM_RACERS()
	SWITCH CurrentRaceIndex
		CASE OFFROAD_RACE_CANYON_CLIFFS
		CASE OFFROAD_RACE_VALLEY_TRAIL
		CASE OFFROAD_RACE_ECO_FRIENDLY
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			RETURN 5
		BREAK
		CASE OFFROAD_RACE_RIDGE_RUN
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			RETURN 4
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC BOOL HAS_ORR_SCENE_SPAWNED()
	INT iIndex
	
	REPEAT ORR_GET_NUM_RACERS() iIndex
		IF NOT DOES_ENTITY_EXIST( orArgs.Racers[iIndex].PedIdx )
			OR NOT DOES_ENTITY_EXIST( orArgs.Racers[iIndex].VehIdx )
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	IF NOT DOES_ENTITY_EXIST( orArgs.Loaner.PedIdx )
		OR NOT DOES_ENTITY_EXIST( orArgs.Loaner.VehIdx )
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Waiting for the player to approach the start point.
PROC LAUNCHER_CUSTOM_APPROACH_WAIT()
	//UPDATE_ZVOLUME_WIDGETS()
	FLOAT fDist2 = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRaceLoc)
	IF fDist2 < 62500// 250*250
		OR_LAUNCHER_UPDATE_RACERS()
		OR_LAUNCHER_HANDLE_LOANER_GREETING()
		
		// When the player gets within 8 meters, focus the hint cam.
		IF bHintCam
			IF fDist2 < 64
				OR_LAUNCHER_HANDLE_HINT_CAM()
				bHintCam = FALSE
			ENDIF
//			
//			// If the hint cam was focused, and the player gets greater than 20m away, reset it to happen again.
//			IF fDist2 > 400
//				bHintCam = TRUE
//			ENDIF
		ENDIF
		
		IF fDist2 < 400 //20*20
			ORR_LAUNCHER_UPDATE_AMBIENT_DIALOGUE()
		ENDIF
	ENDIF
	
	IF NOT bSceneSpawned
		IF HAS_ORR_SCENE_SPAWNED()
			bSceneSpawned = TRUE
			INCREMENT_MINIGAMES_LOADED_COUNT()
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    The script has started. Emergency destroy everything.
PROC LAUNCHER_CUSTOM_CLEAR_SCENE()
//	INT i = 0
//	REPEAT COUNT_OF(orArgs.Racers) i
//		IF DOES_ENTITY_EXIST(orArgs.Racers[i].VehIdx)
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), orArgs.Racers[i].VehIdx)
//				SET_VEHICLE_AS_NO_LONGER_NEEDED(orArgs.Racers[i].VehIdx)
//			ELSE
//				DELETE_VEHICLE(orArgs.Racers[i].VehIdx)
//			ENDIF
//		ENDIF
//		IF DOES_ENTITY_EXIST(orArgs.Racers[i].PedIdx)
//			DELETE_PED(orArgs.Racers[i].PedIdx)
//		ENDIF
//	ENDREPEAT

	IF NOT bHintCam
		STOP_GAMEPLAY_HINT()
		bHintCam = TRUE
	ENDIF
	
	LAUNCHER_CUSTOM_RELEASE_ASSETS()

ENDPROC

USING "generic_launcher.sch"
