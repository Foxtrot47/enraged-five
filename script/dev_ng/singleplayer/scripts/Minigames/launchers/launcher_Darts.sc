// launcher_Darts.sc


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.



CONST_INT LAUNCHER_POINT_ENTER 					1		// Tells the script that you kick it off by entering a point.
CONST_INT LAUNCHER_SNAP_CHECKPOINT_TO_GROUND 	1
CONST_INT LAUNCHER_CUSTOM_SCRIPT_LAUNCH 		1 
CONST_INT LAUNCHER_DONT_CHECK_RELOAD_RANGE 		1
CONST_INT LAUNCHER_OBJECT_ATTACHED 				1
CONST_INT LAUNCHER_CHECK_INTERIOR				1
CONST_INT LAUNCHER_DONT_FADE_DOWN				1
//CONST_INT LAUNCHER_HAS_SCENE 					1
//CONST_INT LAUNCHER_CHECK_FOR_FIRE  				1

USING "generic_launcher_header.sch"
USING "../Darts/Darts_Args.sch"

// Models to setup scene.
MODEL_NAMES 	guy1
MODEL_NAMES 	guy2
PED_INDEX		pedGuy1
PED_INDEX		pedGuy2
BOOL 			bSpawnedScene = FALSE
BOOL 			bAllowScene = TRUE
INT				iTaskState = -1			// -1 = Not tasked, 0 = Stand Still, 1 = drinking
INT 			iDartsLocation

DARTS_ARGS			DartsArgs
OBJECT_INDEX		oScriptObject
INTERIOR_INSTANCE_INDEX		intRockClub = NULL
INTERIOR_INSTANCE_INDEX 	intHickBar = NULL
BOOL 				bBlipKilled
BOOL				bDartGuyBlockingActive
BOOL				bIntroDartsMovieRequested

/// PURPOSE:
///    How many dart launchers/blips do we have?
FUNC INT DARTS_GET_NUMBER_OF_LOCATIONS()
	RETURN 1//2
ENDFUNC

/// PURPOSE:
///   	Returns position of a darts minigame location
FUNC VECTOR DARTS_GET_LOCATION_BY_ID(INT myDartsIdx)
	SWITCH myDartsIdx
		CASE 1 //Rock club
			RETURN << -572.041, 294.196, 79.9374 >>
		CASE 2 //Hick Bar
			RETURN << 1992.27, 3050.60, 47.89 >>
	ENDSWITCH
	PRINTLN("Trying to get vector for location with ID ... ", myDartsIdx)
	SCRIPT_ASSERT("Could not find location for darts please check the launcher script")
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Returns static blip enum of a darts minigame location
FUNC STATIC_BLIP_NAME_ENUM DARTS_GET_STATIC_BLIP_BY_ID(INT myDartsIdx)
	SWITCH myDartsIdx
		CASE 1 //Rock club
			RETURN STATIC_BLIP_MINIGAME_DARTS1
		CASE 2 //Hick Bar
			RETURN STATIC_BLIP_MINIGAME_DARTS2
	ENDSWITCH
	//just send the first one if we dont get match
	SCRIPT_ASSERT("Could not find static blip for darts please check the launcher script")
	RETURN STATIC_BLIP_MINIGAME_DARTS2
ENDFUNC

/// PURPOSE:
///    
FUNC STATIC_BLIP_NAME_ENUM DARTS_GET_CLOSEST_STATIC_BLIP()
	//INT iClosestIdx = 1
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		VECTOR vTempPlayerCoords = vLaunchLocation
		FLOAT fClosest = 999999
		FLOAT fCurrent
		INT iCurrentIdx = 1
		
		REPEAT (DARTS_GET_NUMBER_OF_LOCATIONS() + 1) iCurrentIdx
			IF iCurrentIdx > 0
				fCurrent = VDIST2(vTempPlayerCoords, DARTS_GET_LOCATION_BY_ID(iCurrentIdx))
				IF fClosest > fCurrent
					fClosest = fCurrent
					//iClosestIdx = iCurrentIdx
				ENDIF
			ENDIF
		ENDREPEAT
		
		//PRINTLN("Closest darts location is ID ... ", iClosestIdx)
	ENDIF
	RETURN STATIC_BLIP_MINIGAME_DARTS2 //DARTS_GET_STATIC_BLIP_BY_ID(iClosestIdx)
ENDFUNC

/// PURPOSE:
///    Checks to see if we can run while a friend activity is going on.
FUNC BOOL LAUNCHER_CAN_RUN_FRIEND_ACTIVITY()
	
	// If we're on a friend activity, but our blip isn't active, quit.
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//			IF GET_PLAYER_DISTANCE_FROM_LOCATION(DARTS_GET_LOCATION_BY_ID(1)) < GET_PLAYER_DISTANCE_FROM_LOCATION(DARTS_GET_LOCATION_BY_ID(2))
//				RETURN IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_DARTS1)
//			ELSE
				RETURN IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_DARTS2)
//			ENDIF
		ENDIF
	ENDIF
	
	// We're not even on a friend activity. Who cares?
	RETURN TRUE
ENDFUNC

PROC LAUNCHER_CUSTOM_UNLOAD_ASSETS()
	
	PRINTLN("launcher darts -> unload assets")
	
	STATIC_BLIP_NAME_ENUM currentLocation
	currentLocation = DARTS_GET_CLOSEST_STATIC_BLIP()
	SWITCH currentLocation
		CASE STATIC_BLIP_MINIGAME_DARTS1
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
				CASE 0	guy1 = A_M_Y_VINEWOOD_01	BREAK
				CASE 1 	guy1 = A_M_Y_STLAT_01		BREAK
				CASE 2	guy1 = A_M_Y_VINEWOOD_04	BREAK
			ENDSWITCH
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0 	guy2 = A_M_Y_STWHI_02		BREAK
				CASE 1 	guy2 = A_M_Y_VINEWOOD_03	BREAK
			ENDSWITCH
			
			iDartsLocation = 1
		BREAK
		CASE STATIC_BLIP_MINIGAME_DARTS2
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0	
					guy1 = A_F_M_SALTON_01
					guy2 = A_F_O_SALTON_01
				BREAK
				CASE 1 	
					guy1 = A_F_O_SALTON_01	
					guy2 = A_F_M_SALTON_01
				BREAK
			ENDSWITCH
			
			iDartsLocation = 2
		BREAK
	ENDSWITCH
	
	REQUEST_MODEL(guy1)
	REQUEST_MODEL(guy2)
	
	// Also need the dart data.
	REQUEST_MODEL(PROP_DART_1)

ENDPROC

PROC LAUNCHER_CUSTOM_REQUEST_ASSETS()
	IF bAllowScene
		
		PRINTLN("launcher darts -> requested assets")
		
		STATIC_BLIP_NAME_ENUM currentLocation
		currentLocation = DARTS_GET_CLOSEST_STATIC_BLIP()
		SWITCH currentLocation
		CASE STATIC_BLIP_MINIGAME_DARTS1
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
				CASE 0	guy1 = A_M_Y_VINEWOOD_01	BREAK
				CASE 1 	guy1 = A_M_Y_STLAT_01		BREAK
				CASE 2	guy1 = A_M_Y_VINEWOOD_04	BREAK
			ENDSWITCH
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0 	guy2 = A_M_Y_STWHI_02		BREAK
				CASE 1 	guy2 = A_M_Y_VINEWOOD_03	BREAK
			ENDSWITCH
			
			iDartsLocation = 1
		BREAK
		CASE STATIC_BLIP_MINIGAME_DARTS2
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0	
					guy1 = A_F_M_SALTON_01
					guy2 = A_F_O_SALTON_01
				BREAK
				CASE 1 	
					guy1 = A_F_O_SALTON_01	
					guy2 = A_F_M_SALTON_01
				BREAK
			ENDSWITCH
			
			iDartsLocation = 2
		BREAK
	ENDSWITCH
	
	REQUEST_MODEL(guy1)
	REQUEST_MODEL(guy2)
		
		// Also need the dart data.
		REQUEST_MODEL(PROP_DART_1)
		
		IF DOES_ENTITY_EXIST(pedGuy1)
			SET_ENTITY_AS_MISSION_ENTITY(pedGuy1)
		ENDIF
		IF DOES_ENTITY_EXIST(pedGuy2)
			SET_ENTITY_AS_MISSION_ENTITY(pedGuy2)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL LAUNCHER_CUSTOM_ASSETS_LOADED()
	IF bAllowScene
		// Make sure those guys are loaded.
		RETURN HAS_MODEL_LOADED(guy1) AND HAS_MODEL_LOADED(guy2) AND HAS_MODEL_LOADED(PROP_DART_1)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC LAUNCHER_CUSTOM_RELEASE_ASSETS()
	IF bAllowScene
		PRINTLN("launcher darts -> custom releasing assets")
		
		PRINTLN("guy1 = ", guy1)
		IF guy1 != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(guy1)
		ENDIF
		IF guy2 != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(guy2)
		ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_DART_1)
	ENDIF
ENDPROC


/// PURPOSE:
///    Creates the other guys at the shooting range.
PROC LAUNCHER_CUSTOM_SPAWN_SCENE()	
	
	IF bAllowScene
		
		VECTOR vDartsLocation = DARTS_GET_LOCATION_BY_ID(iDartsLocation)
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		FLOAT fVDist2 = VDIST2(vPlayerPos, vDartsLocation)
		
		IF ((fVDist2 > 45) // Player can't be too close.
			OR g_bForceCustomDartSpawn)
		AND NOT DOES_ENTITY_EXIST(pedGuy1)
			PRINTLN("launcher darts -> custom spawn scene: > 15 distance, spawning guys")
			
			IF g_bForceCustomDartSpawn
				g_bForceCustomDartSpawn = FALSE
			ENDIF
			
			VECTOR vRockClubSpawnPos 	= <<-573.1373, 294.0269, 78.1765>>
			VECTOR vRockClubSpawnPos2 	= <<-574.1169, 292.7964, 78.1766>>
			VECTOR vHickBarSpawnPos		= <<1995.6138, 3049.8774, 46.2153>>
			VECTOR vHickBarSpawnPos2	= <<1995.4880, 3047.3833, 46.2153>> //<<1996.0013, 3048.4270, 46.2153>>
			FLOAT fRockClubHeading		= 172.6813
			FLOAT fRockClubHeading2		= 274.5094
			FLOAT fHickBarHeading		= 142.6717
			FLOAT fHickBarHeading2		= 44.8785
			
			/*
			Hick Bar Info
			Pos: <<1993.8701, 3049.6279, 46.2153>>
			Heading: 63.8589
			
			Rock Club Info
			Pos: <<-572.0725, 292.4112, 78.1765>>
			Heading: 348.4315
			*/
			
//			back alley coords where peds were spawning bc of interior issues: -13.6,-1090.6,26.7
			INTERIOR_INSTANCE_INDEX myInterior
			// need to get darts thang
			myInterior = GET_INTERIOR_AT_COORDS(DARTS_GET_LOCATION_BY_ID(iDartsLocation))
			IF IS_VALID_INTERIOR(myInterior)
				PIN_INTERIOR_IN_MEMORY(myInterior)
				WHILE NOT IS_INTERIOR_READY(myInterior)
					WAIT(0)
				ENDWHILE
				UNPIN_INTERIOR(myInterior)
			ENDIF
			
			pedGuy1 = CREATE_PED(PEDTYPE_MISSION, guy1, PICK_VECTOR((iDartsLocation = 1), vRockClubSpawnPos, vHickBarSpawnPos), PICK_FLOAT((iDartsLocation = 1), fRockClubHeading, fHickBarHeading))
			pedGuy2 = CREATE_PED(PEDTYPE_MISSION, guy2, PICK_VECTOR((iDartsLocation = 1), vRockClubSpawnPos2, vHickBarSpawnPos2), PICK_FLOAT((iDartsLocation = 1), fRockClubHeading2, fHickBarHeading2))
			
			SET_ENTITY_AS_MISSION_ENTITY(pedGuy1)
			SET_ENTITY_AS_MISSION_ENTITY(pedGuy2)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy1, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy2, TRUE)
			bDartGuyBlockingActive = TRUE
			
			CLEAR_BIT(g_iDartsTutorialFlags, gDARTS_ALT_START_POS)
			
			// Since there's no dart anims right now, let's have dude drink a beer			
			iTaskState = -1
			IF (fVDist2 < 20.0*20.0)
				iTaskState = 1
				
				TASK_START_SCENARIO_IN_PLACE(pedGuy1, "WORLD_HUMAN_HANG_OUT_STREET")//"WORLD_HUMAN_STAND_IMPATIENT") //"WORLD_HUMAN_DRINKING")
				TASK_START_SCENARIO_IN_PLACE(pedGuy2, "WORLD_HUMAN_HANG_OUT_STREET")
			ELSE
				iTaskState = 0
				TASK_STAND_STILL(pedGuy1, -1)
				FREEZE_ENTITY_POSITION(pedGuy1, TRUE)
				TASK_STAND_STILL(pedGuy2, -1)
				FREEZE_ENTITY_POSITION(pedGuy2, TRUE)
			ENDIF
			
			bSpawnedScene = TRUE
		ELSE
			IF (fVDist2 <= 45)
				PRINTLN("launcher darts -> custom spawn scene: less then 10 in, not spawning guys")
			ELIF DOES_ENTITY_EXIST(pedGuy1)
				PRINTLN("launcher darts -> custom spawn scene: pedguy1 already exists")
			ENDIF
			bSpawnedScene = FALSE
		ENDIF
	ELSE
		
		PRINTLN(">>>>>>>>>>>not spawning dudes")
		bSpawnedScene = FALSE
	ENDIF
ENDPROC

// Setup our script.
PROC LAUNCHER_CUSTOM_SCRIPT_INIT()	
	scriptName = "Darts"
	helpButtonPress = "PLAY_DARTS"
	
	eMinigame = MINIGAME_DARTS
	fLaunchScriptDist = 2.5
	vObjectLOSOffset = <<0, -0.5, 0>>
	
	eLauncherStaticBlip = STATIC_BLIP_MINIGAME_DARTS2
	
	BOOL bValidDartBoard = FALSE
	//INT boardIdx
	// since there's only one darts location right now, just poll for that specific one instead of running a repeat process
	// if another location is added, adjust as necessary
	//REPEAT DARTS_GET_NUMBER_OF_LOCATIONS() boardIdx
		//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), DARTS_GET_LOCATION_BY_ID(boardIdx+1), <<50, 50, 50>>, FALSE, FALSE)
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), DARTS_GET_LOCATION_BY_ID(2), <<100, 100, 100>>, FALSE, FALSE)
			//we're within 50m of a valid location.
			bValidDartBoard = TRUE
		ENDIF
	//ENDREPEAT
	
	
	IF NOT bValidDartBoard
		LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE)
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
		eTransMode = TM_ANY
	ENDIF
	
	// Get the interior for the game room.
	eInterior = GET_INTERIOR_AT_COORDS(vLaunchLocation)
	
	// Not sure if this works as a friend activity yet.
	SET_BITMASK_ENUM_AS_ENUM(launcherFlags, LAUNCHER_FRIEND_ACTIVITY)
	//clear the launcher for a mission leadin scene.
	SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FOR_MISSION_LEADIN) 
	
	PRINTLN("launcher darts -> script initialised")
	PRINTLN("player distance from dartboard when launched = ", GET_PLAYER_DISTANCE_FROM_LOCATION(DARTS_GET_LOCATION_BY_ID(2)))
	
	// if another location becomes present, uncomment this
//	#IF IS_DEBUG_BUILD
//		SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_ALLOW_MULTIPLE_COPIES)
//	#ENDIF
	
	iStackSize = MISSION_STACK_SIZE
ENDPROC

/// PURPOSE:
///    Launch out custom script.
FUNC THREADID LAUNCHER_CUSTOM_RUN_SCRIPT()
	VECTOR vRot = GET_ENTITY_ROTATION(oScriptObject)
	DartsArgs.vDartBoard = GET_ENTITY_COORDS(oScriptObject)
	DartsArgs.fBoardHeading = vRot.z
	DartsArgs.oiDartBoard = oScriptObject
	
	IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_ALT_START_POS)
		DartsArgs.eNextOpponentModel = guy2
		DartsArgs.piDartOpponent = pedGuy1
		PRINTLN("sent pedguy1 to start darts")
	ELSE
		DartsArgs.eNextOpponentModel = guy1
		DartsArgs.piDartOpponent = pedGuy2
		PRINTLN("sent pedguy2 to start darts")
	ENDIF
//	IF NOT IS_PED_INJURED(pedGuy1)
////		IF IS_CURRENTLY_ON_MISSION(MISSION_TYPE_FRIEND_ACTIVITY)
///    		OR IS_CURRENTLY_ON_MISSION(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
////			CLEAR_PED_TASKS(pedGuy1)
////			TASK_WANDER_STANDARD(pedGuy1)
////			PRINTLN("Player on friend mission, sending pedGuy1 packing")
////		ELSE
//			DartsArgs.piDartOpponent = pedGuy1
//			PRINTLN("Sending pedguy1 as an arg")
////		ENDIF
//	ENDIF
	
	PRINTLN("launcher darts -> script running")
	
	THREADID ourThread
	ourThread = START_NEW_SCRIPT_WITH_ARGS(scriptName, DartsArgs, SIZE_OF(DartsArgs), iStackSize)
	SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptName)
	
	RETURN ourThread
ENDFUNC

PROC LAUNCHER_CUSTOM_APPROACH_WAIT()
	PED_INDEX pedArray[25]
	INT pedCount
	INT loopCounter
	BOOL bKillLauncher = FALSE
	
	IF bAllowScene
		
//		IF NOT IS_ENTITY_DEAD(pedGuy1)
//			FREEZE_ENTITY_POSITION(pedGuy1, FALSE)
//		ENDIF
//		IF NOT IS_ENTITY_DEAD(pedGuy2)
//			FREEZE_ENTITY_POSITION(pedGuy2, FALSE)
//		ENDIF
		
		// Update their tasks:
		VECTOR vPlayerPos
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		FLOAT fVDist2 = VDIST2(vPlayerPos, vLaunchLocation)
		
		IF (iTaskState = 0)
			
			
			// We're standing still -- Are we close enough to start firing?
			IF (fVDist2 < 10.0*10.0)
				iTaskState = 1
				
				
				IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
					PRINTLN("launcher darts -> scenario tasks given")
					
					IF NOT IS_ENTITY_DEAD(pedGuy1)
						FREEZE_ENTITY_POSITION(pedGuy1, FALSE)
						TASK_LOOK_AT_ENTITY(pedGuy1, pedGuy2, -1, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						TASK_START_SCENARIO_IN_PLACE(pedGuy1, "WORLD_HUMAN_HANG_OUT_STREET") //"WORLD_HUMAN_DRINKING")
					ENDIF
					IF NOT IS_ENTITY_DEAD(pedGuy2)
						FREEZE_ENTITY_POSITION(pedGuy2, FALSE)
						TASK_LOOK_AT_ENTITY(pedGuy2, pedGuy1, -1, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						TASK_START_SCENARIO_IN_PLACE(pedGuy2, "WORLD_HUMAN_HANG_OUT_STREET") //"WORLD_HUMAN_DRINKING")
					ENDIF
				ELSE
					PRINTLN("launcher darts -> on friend activity, moving dart playing peds")
					IF NOT IS_ENTITY_DEAD(pedGuy1)
						FREEZE_ENTITY_POSITION(pedGuy1, FALSE)
						IF DOES_SCENARIO_EXIST_IN_AREA(<< 1991.67664, 3044.95654, 46.21505 >>, 0.5, TRUE)
							TASK_USE_NEAREST_SCENARIO_TO_COORD(pedGuy1, << 1991.67664, 3044.95654, 46.21505 >>, 0.5)
						ELSE
							TASK_FOLLOW_NAV_MESH_TO_COORD(pedGuy1, << 1991.67664, 3044.95654, 46.21505 >>, PEDMOVE_WALK)
						ENDIF
						SET_PED_AS_NO_LONGER_NEEDED(pedGuy1)
					ENDIF
					IF NOT IS_ENTITY_DEAD(pedGuy2)
						FREEZE_ENTITY_POSITION(pedGuy2, FALSE)
						IF DOES_SCENARIO_EXIST_IN_AREA(<< 1990.37036, 3045.12134, 46.21502 >>, 0.5, TRUE)
							TASK_USE_NEAREST_SCENARIO_TO_COORD(pedGuy2, << 1990.37036, 3045.12134, 46.21502 >>, 0.5)
						ELSE
							TASK_FOLLOW_NAV_MESH_TO_COORD(pedGuy2, << 1990.37036, 3045.12134, 46.21502 >>, PEDMOVE_WALK)
						ENDIF
						SET_PED_AS_NO_LONGER_NEEDED(pedGuy2)
					ENDIF
				ENDIF
			ENDIF
		ELIF (iTaskState = 1)
			
			// We're firing -- Are we far enough to stop firing?
			IF (fVDist2 >= 10.0*10.0)
				iTaskState = 0
				
				IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
					PRINTLN("launcher darts -> frozen tasks given")
					
					// STOP FIRING!
					IF NOT IS_ENTITY_DEAD(pedGuy1)
						TASK_STAND_STILL(pedGuy1, -1)
						IF NOT IS_ENTITY_ATTACHED(pedGuy1)
							FREEZE_ENTITY_POSITION(pedGuy1, TRUE)
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(pedGuy2)
						TASK_STAND_STILL(pedGuy2, -1)
						IF NOT IS_ENTITY_ATTACHED(pedGuy2)
							FREEZE_ENTITY_POSITION(pedGuy2, TRUE)
						ENDIF
					ENDIF
				ELSE
					PRINTLN("launcher darts -> on friend activity, deleting peds")
					IF NOT IS_ENTITY_DEAD(pedGuy1)
						SET_ENTITY_COORDS(pedGuy1, << 1991.67664, 3044.95654, 46.21505 >>)
						TASK_STAND_STILL(pedGuy1, -1)
						IF NOT IS_ENTITY_ATTACHED(pedGuy1)
							FREEZE_ENTITY_POSITION(pedGuy1, TRUE)
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(pedGuy2)
						SET_ENTITY_COORDS(pedGuy2, << 1990.37036, 3045.12134, 46.21502 >>)
						TASK_STAND_STILL(pedGuy2, -1)
						IF NOT IS_ENTITY_ATTACHED(pedGuy2)
							FREEZE_ENTITY_POSITION(pedGuy2, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bBlipKilled
		
		
		
		
		// Detect to see if the player is in combat. If he is, kill the blip.
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID()) 
			IF IS_PED_SHOOTING(PLAYER_PED_ID()) 
			OR IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0) 
			OR IS_ANY_PED_SHOOTING_IN_AREA(<< 937.0616, -117.8927, 72.9163 >>, << 1021.8488, -120.5357, 72.9163 >>, TRUE)
			OR IS_PROJECTILE_IN_AREA(<<1991.4733, 3045.9805, 49.5320>>, <<1996.9056, 3052.1697, 46.3392>>)
				#IF IS_DEBUG_BUILD
					PRINTLN("___________Killing launcher for one of these reasons___________")
//					PRINTLN("IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID()) is (1 for true) ", IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID()))
					PRINTLN("IS_PED_SHOOTING(PLAYER_PED_ID()) is (1 for true) ", IS_PED_SHOOTING(PLAYER_PED_ID()))
					PRINTLN("IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0) is (1 for true) ", IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0) )
					PRINTLN("IS_ANY_PED_SHOOTING_IN_AREA(<< 937.0616, -117.8927, 72.9163 >>, << 1021.8488, -120.5357, 72.9163 >>, TRUE) is (1 for true) ", IS_ANY_PED_SHOOTING_IN_AREA(<< 937.0616, -117.8927, 72.9163 >>, << 1021.8488, -120.5357, 72.9163 >>, TRUE))
					PRINTLN("IS_PROJECTILE_IN_AREA(<<1991.4733, 3045.9805, 49.5320>>, <<1996.9056, 3052.1697, 46.3392>>) (1 for true) ", IS_PROJECTILE_IN_AREA(<<1991.4733, 3045.9805, 49.5320>>, <<1996.9056, 3052.1697, 46.3392>>))
				#ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedGuy1)
					TASK_SMART_FLEE_PED(pedGuy1, PLAYER_PED_ID(), 1000.0, -1)
				ENDIF
				IF NOT IS_ENTITY_DEAD(pedGuy2)
					TASK_SMART_FLEE_PED(pedGuy2, PLAYER_PED_ID(), 1000.0, -1)
				ENDIF
				
				bKillLauncher = TRUE
			ELSE
				// only care about other peds when not on friend activity
				//taking this out for B* 1946440 - AsD 9/11/14
//				IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
//				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
					pedCount = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), pedArray)
					loopCounter = 0
					WHILE (loopCounter < pedCount)
						IF NOT IS_PED_INJURED(pedArray[loopCounter])
							IF IS_PED_IN_MELEE_COMBAT(pedArray[loopCounter])
							//OR IS_PED_FLEEING(pedArray[loopCounter])
							OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedArray[loopCounter])
								#IF IS_DEBUG_BUILD
									PRINTLN("___________Killing launcher for one of these reasons___________")
									PRINTLN("loopCounter is ", loopCounter)
									PRINTLN("IS_PED_IN_MELEE_COMBAT(pedArray[loopCounter]) is (1 for true) ", IS_PED_IN_MELEE_COMBAT(pedArray[loopCounter]))
									//PRINTLN("IS_PED_FLEEING(pedArray[loopCounter]) is (1 for true) ", IS_PED_FLEEING(pedArray[loopCounter]))
									PRINTLN("IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedArray[loopCounter]) is (1 for true) ", IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedArray[loopCounter]))
								#ENDIF
								
								IF NOT IS_ENTITY_DEAD(pedGuy1)
									TASK_SMART_FLEE_PED(pedGuy1, PLAYER_PED_ID(), 1000.0, -1)
								ENDIF
								IF NOT IS_ENTITY_DEAD(pedGuy2)
									TASK_SMART_FLEE_PED(pedGuy2, PLAYER_PED_ID(), 1000.0, -1)
								ENDIF
								
								bKillLauncher = TRUE
							ENDIF
						ENDIF
						loopCounter++
					ENDWHILE
//				ENDIF
			ENDIF
		ENDIF
		
		
		// added this check for B* 1356788: if the opponent peds are fleeing and not on a friend activity, kill the blip
		IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
		AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
			IF NOT IS_PED_INJURED(pedGuy1)
			AND NOT IS_PED_INJURED(pedGuy2)
				IF IS_PED_FLEEING(pedGuy1)
				OR IS_PED_FLEEING(pedGuy2)
					#IF IS_DEBUG_BUILD
						PRINTLN("___________Killing launcher for one of these reasons___________")
						PRINTLN("dart opponent guy is fleeing")
					#ENDIF
					bKillLauncher = TRUE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("___________Killing launcher for one of these reasons___________")
					PRINTLN("dart opponent guy is injured")
				#ENDIF
				bKillLauncher = TRUE
			ENDIF
		ENDIF
		
		IF bKillLauncher
			IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(DARTS_GET_CLOSEST_STATIC_BLIP())
				bBlipKilled = TRUE
				LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE)
			ENDIF
			IF bIntroDartsMovieRequested
				REMOVE_ANIM_DICT("mini@dartsintro")
				REMOVE_ANIM_DICT("mini@dartsintro_alt1")
				bIntroDartsMovieRequested = FALSE
				bStreamsCheck = FALSE
			ENDIF
		ENDIF
		
		
	ENDIF
	
//	IF bPlayerCanPlay
//		// tequi la la back door
//		IF GET_PLAYER_DISTANCE_FROM_LOCATION(<<-561.870667,293.605652,87.805046>>) < 2.0
//			SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_R, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
//		ENDIF
//		
//		// tequi la la front door
//		IF GET_PLAYER_DISTANCE_FROM_LOCATION(<<-564.544128,276.520905,83.277901>>) < 2.0
//			SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_F, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
//		ENDIF
//	ENDIF	
	
	IF iDartsLocation = 1
		// If he somehow gets inside, unlock the doors too.
		IF NOT IS_VALID_INTERIOR(intRockClub)
			intRockClub = GET_INTERIOR_AT_COORDS(<<-573.3155, 291.2963, 79.5509>>)
		ELSE
			// If the interiors are the same, just unlock the doors.
			IF (intRockClub = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
				SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_R, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
				SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_F, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
				IF NOT IS_PED_INJURED(pedGuy1)
				AND NOT IS_PED_INJURED(pedGuy2)
					IF bDartGuyBlockingActive
						PRINTLN("launcher darts -> player is in rock club interior, turning off blocking for peds")
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy1, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy2, FALSE)
						bDartGuyBlockingActive = FALSE
					ENDIF
				ENDIF
			ELSE	
				IF NOT IS_PED_INJURED(pedGuy1)
				AND NOT IS_PED_INJURED(pedGuy2)
					IF NOT bDartGuyBlockingActive
						PRINTLN("launcher darts -> player is out of rock club interior, turning on blocking for peds")
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy1, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy2, TRUE)
						bDartGuyBlockingActive = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF iDartsLocation = 2
		// checking the guys at the hick bar
		IF NOT IS_VALID_INTERIOR(intHickBar)
			intHickBar = GET_INTERIOR_AT_COORDS(<<1995.6138, 3049.8774, 46.2153>>)
		ELSE
			// If the interiors are the same, just unlock the doors.
			IF (intHickBar = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
				IF NOT IS_PED_INJURED(pedGuy1)
				AND NOT IS_PED_INJURED(pedGuy2)
					IF bDartGuyBlockingActive
						PRINTLN("launcher darts -> player is in hick bar interior, turning off blocking for peds")
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy1, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy2, FALSE)
						bDartGuyBlockingActive = FALSE
					ENDIF
				ENDIF
				
				IF NOT bIntroDartsMovieRequested
					REQUEST_ANIM_DICT("mini@dartsintro")
					REQUEST_ANIM_DICT("mini@dartsintro_alt1")
					bIntroDartsMovieRequested = TRUE
				ENDIF
				IF bIntroDartsMovieRequested
					IF NOT bStreamsCheck
						IF NOT HAS_ANIM_DICT_LOADED("mini@dartsintro")
						OR NOT HAS_ANIM_DICT_LOADED("mini@dartsintro_alt1")
							PRINTLN("launcher darts -> still streaming intro movies")
						ELSE
							PRINTLN("launcher darts -> streams are done, setting stream check to true")
							bStreamsCheck = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE	
				IF NOT IS_PED_INJURED(pedGuy1)
				AND NOT IS_PED_INJURED(pedGuy2)
					IF NOT bDartGuyBlockingActive
						PRINTLN("launcher darts -> player is out of hick bar interior, turning on blocking for peds")
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy1, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy2, TRUE)
						bDartGuyBlockingActive = TRUE
					ENDIF
				ENDIF
				
				IF bIntroDartsMovieRequested
					REMOVE_ANIM_DICT("mini@dartsintro")
					REMOVE_ANIM_DICT("mini@dartsintro_alt1")
					bIntroDartsMovieRequested = FALSE
					bStreamsCheck = FALSE
					PRINTLN("launcher darts -> removing intro movies, resetting stream flags")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    The script has started. Emergency destroy everything.
PROC LAUNCHER_CUSTOM_CLEAR_SCENE()
	IF (bSpawnedScene)
		DELETE_PED(pedGuy1)
		DELETE_PED(pedGuy2)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(guy1)
		SET_MODEL_AS_NO_LONGER_NEEDED(guy2)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_DART_1)
		
	ENDIF
	bSpawnedScene = FALSE
ENDPROC

PROC LAUNCHER_CUSTOM_CLEANUP()
	// If we killed the blip due to combat, reinstate it.
	IF bBlipKilled
		GENERIC_LAUNCHER_REINSTATE_EVENT_BLIP(DARTS_GET_CLOSEST_STATIC_BLIP())
	ENDIF
ENDPROC

USING "generic_launcher.sch"
