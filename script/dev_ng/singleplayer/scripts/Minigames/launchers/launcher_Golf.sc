CONST_INT LAUNCHER_POINT_ENTER 				1
//CONST_INT LAUNCHER_HAS_SCENE 				1
//CONST_INT LAUNCHER_NO_BUTTONPRESS 		1
//CONST_INT LAUNCHER_POINT_ENTER_IN_VEHICLE 	1
CONST_INT LAUNCHER_POINT_AUTO_TRIGGER_IN_VEHICLE 1
CONST_INT LAUNCHER_POINT_ENTER_WITH_FRIEND_IN_VEHICLE 1
CONST_INT LAUNCHER_IGNORE_VEHICLE_HEALTH	1
CONST_INT LAUNCHER_IGNORE_VEHICLE_FIRE		1
CONST_INT LAUNCHER_CLEAR_SCENE_ON_START		1
CONST_INT GOLF_IS_MP 						0
CONST_INT LAUNCHER_DONT_CHECK_RELOAD_RANGE 	1
CONST_INT LAUNCHER_TIME_RESTRICTED			1
CONST_INT LAUNCHER_MONEY_RESTRICTED			1
CONST_INT LAUNCHER_DONT_FADE_DOWN			1

USING "globals.sch"
USING "generic_launcher_header.sch"

/// PURPOSE:
///    Checks to see if we can run while a friend activity is going on.
FUNC BOOL LAUNCHER_CAN_RUN_FRIEND_ACTIVITY()
	// If we're on a friend activity, but our blip isn't active, quit.
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
		RETURN IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_GOLF)
	ENDIF
	
	// We're not even on a friend activity. Who cares?
	RETURN TRUE
ENDFUNC

PROC LAUNCHER_CUSTOM_SCRIPT_INIT()
	scriptName = "golf"
	eMinigame = MINIGAME_GOLF
	fLaunchScriptDist = 7.0
	
	SET_BITMASK_ENUM_AS_ENUM(launcherFlags, LAUNCHER_FRIEND_ACTIVITY)
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
		eTransMode = TM_ANY
	ENDIF
	
	IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_GOLF_CLUB) = GET_CURRENT_PLAYER_PED_ENUM()
		iPriceToLaunch = 0
		helpButtonPress = "PLAY_GOLF"
	ELSE
		iPriceToLaunch = GET_PRICE_OF_GOLF_GAME(DOES_ENTITY_EXIST(FRIEND_A_PED_ID()))
		helpButtonPress = "PAY_PLAY_GOLF"
	ENDIF
	
	// This needs to be set because someone set the stream in distance to be 209m inside streamed_scripts.sch
	eLauncherStaticBlip = STATIC_BLIP_MINIGAME_GOLF
	fLauncherShutdownDist = TO_FLOAT(GENERIC_LAUNCHER_GET_BLIP_STREAMING_RANGE(eLauncherStaticBlip)) + 5.0
	
	iStackSize = MISSION_STACK_SIZE
ENDPROC

PROC LAUNCHER_CUSTOM_CLEAR_SCENE()
	PED_INDEX closestPed
	
	IF GET_CLOSEST_PED(<<-1370.6245, 56.1227, 52.7033>>, 100, TRUE, TRUE, closestPed)
		PED_INDEX pedArray[100]
		INT pedCount 
		IF NOT IS_PED_INJURED(closestPed)
			pedCount = GET_PED_NEARBY_PEDS(closestPed, pedArray)
			CLEAR_PED_TASKS(closestPed)
			PRINTLN("Clearing closests ped tasks")
		ELSE
			PRINTLN("no closest ped?!!")
		ENDIF
		IF pedCount = 0 
			PRINTLN("no peds?!!")
		ELSE
			PRINTLN("Ped count is... ", pedCount)
		ENDIF
		
		INT pedCounter = 0
		REPEAT pedCount pedCounter
			IF NOT IS_PED_INJURED(pedArray[pedCounter])
				CLEAR_PED_TASKS(pedArray[pedCounter])
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_APPROACH_WAIT()
	#IF NOT LAUNCHER_POINT_ENTER
		SCRIPT_ASSERT("LAUNCHER TYPE CHANGED! CHECK THE LAUNCHER AND FIX THIS OR IT WILL NOT WORK PROPERLY")
	#ENDIF
	
//	IF iContextIntention != -1 
//	   AND (IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY) OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG))
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			VEHICLE_INDEX tempveh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
//			IF IS_PED_IN_VEHICLE(FRIEND_A_PED_ID(), tempveh)
//				IF IS_ENTITY_AT_COORD( tempveh, vLaunchLocation, <<fLaunchScriptDist, fLaunchScriptDist, LOCATE_SIZE_HEIGHT>>, FALSE, TRUE, eTransMode)
//				    RELEASE_CONTEXT_INTENTION(iContextIntention)
//					CLEAR_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_HELP_TEXT_DISPLAYED)
//					INT arraypos = GET_CONTEXT_INTENTION_ARRAY_INDEX(iContextIntention)
//					g_IntentionList[arraypos].bUsed = TRUE
//					g_IntentionList[arraypos].bActive = TRUE
//				ELSE
//					PRINTLN("IS_ENTITY_AT_COORD( tempveh,")
//				ENDIF				
//			ELSE
//				PRINTLN("IS_PED_IN_VEHICLE(FRIEND_A_PED_ID()")
//			ENDIF
//		ELSE
//			PRINTLN("IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()")	
//		ENDIF
//	ELSE
//		PRINTLN("friendliness not setup right")
//	
//	ENDIF
	
	#IF LAUNCHER_POINT_ENTER
		//since we're a point enter type launcher, we'll do a check to make sure the player is in range
		IF IS_ENTITY_AT_COORD( pedPlayer, vLaunchLocation, <<fLaunchScriptDist, fLaunchScriptDist, LOCATE_SIZE_HEIGHT>>, FALSE, TRUE, eTransMode)
		    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
		ENDIF	
	#ENDIF
	
	IF iPriceToLaunch > 0
		IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_GOLF_CLUB) = GET_CURRENT_PLAYER_PED_ENUM()
			PRINTLN("Player as owner detected! Changing price of game")
			iPriceToLaunch = 0
			helpButtonPress = "PLAY_GOLF"
		ENDIF
	ENDIF
ENDPROC

USING "generic_launcher.sch"

