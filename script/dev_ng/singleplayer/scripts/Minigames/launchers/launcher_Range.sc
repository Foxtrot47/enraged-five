

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


CONST_INT LAUNCHER_POINT_ENTER 				1
CONST_INT LAUNCHER_CLEAR_SCENE_ON_START 	1
CONST_INT LAUNCHER_HAS_SCENE 				1
CONST_INT LAUNCHER_DONT_CHECK_RELOAD_RANGE 	1
CONST_INT LAUNCHER_MONEY_RESTRICTED			1
CONST_INT LAUNCHER_CUSTOM_SCRIPT_LAUNCH		1

USING "generic_launcher_header.sch"

// Models to setup scene.
MODEL_NAMES 	guy1, guy2, guy3
PED_INDEX		pedGuy1, pedGuy2, pedGuy3
OBJECT_INDEX	earMuffs1, earMuffs2, earMuffs3
OBJECT_INDEX	goggles1, goggles2, goggles3
BOOL 			bSpawnedScene = FALSE

// This enables people shooting in the range! 
BOOL 			bAllowScene = TRUE
BOOL			bGuy1_Tasked = FALSE
BOOL			bGuy2_Tasked = FALSE
BOOL			bGuy3_Tasked = FALSE
BOOL			bAggroInShop = FALSE

RANGE_LOCATION eLocation

//INT				iTaskState = -1			// -1 = Not tasked, 0 = Stand Still, 1 = Firing
REL_GROUP_HASH 	shooterGroup


STRUCT LAUNCHER_TARGET
	OBJECT_INDEX objBacking
	OBJECT_INDEX objBull
	OBJECT_INDEX objIn1
	OBJECT_INDEX objIn2
	OBJECT_INDEX objIn3
	OBJECT_INDEX objArm
ENDSTRUCT
LAUNCHER_TARGET launcherTargets[3]

PROC LAUNCHER_CreateTarget(INT iGuyIndex)
	FLOAT rowB_Y = 0.40
	FLOAT rowC_Y = 0.20
	FLOAT rowD_Y = 0.50
	
	FLOAT col1_X
	FLOAT col2_X
	FLOAT col3_X
	
	// Temp variables for below. We calc these before each row so it only has to be done once. Saves a lot of math later.
	VECTOR vFwdRow, vLoc
	VECTOR m_vRangeForward, m_vRangeRight
	VECTOR vRowCol1X, vRowCol2X, vRowCol3X
	IF (eLocation = RANGELOC_PILLBOX_HILL)
		col1_X = -0.5
		col2_X = -0.2
		col3_X = 0.5
		m_vRangeForward = << 16.475, -1081.5134, 31.580 >> - << 12.617, -1092.350, 31.580 >>
		m_vRangeRight = << 17.325, -1094.090, 31.580 >> - << 12.617, -1092.350, 31.580 >> 
		vRowCol1X = << 12.617, -1092.350, 31.580 >> + m_vRangeRight * col1_X
		vRowCol2X = << 12.617, -1092.350, 31.580 >> + m_vRangeRight * col2_X
		vRowCol3X = << 12.617, -1092.350, 31.580 >> + m_vRangeRight * col3_X
	ELSE
		col1_X = 1.35
		col2_X = 1.05
		col3_X = 0.5
		m_vRangeForward = << 824.3217, -2179.7732, 31.3470 >> - << 824.2375, -2168.0388, 31.3470 >>
		m_vRangeRight = <<819.2051, -2168.0083, 32.3 >> - << 824.2375, -2168.0388, 32.3 >>
		vRowCol1X = << 824.2375, -2168.0388, 31.3470 >> + m_vRangeRight * col1_X
		vRowCol2X = << 824.2375, -2168.0388, 31.3470 >> + m_vRangeRight * col2_X
		vRowCol3X = << 824.2375, -2168.0388, 31.3470 >> + m_vRangeRight * col3_X
	ENDIF
	
	IF (iGuyIndex = 0)
		// Row B, Col 1
		vFwdRow = m_vRangeForward * rowB_Y
		vLoc = vRowCol1X + vFwdRow // GET CREATED AT VLOC
		
	ELIF (iGuyIndex = 1)
		// Row C, Col 2
		vFwdRow = m_vRangeForward * rowC_Y
		vLoc = vRowCol2X + vFwdRow // GET CREATED AT VLOC
	ELSE
		// Row D5
		vFwdRow = m_vRangeForward * rowD_Y
		vLoc = vRowCol3X + vFwdRow // GET CREATED AT VLOC
	ENDIF
	
	// Create the backing of the target
	launcherTargets[iGuyIndex].objBacking = CREATE_OBJECT_NO_OFFSET(PROP_TARGET_BACKBOARD_B, vLoc)

	// Create the bullseye of the target
	launcherTargets[iGuyIndex].objBull = CREATE_OBJECT_NO_OFFSET(PROP_TARGET_BULL_B, vLoc)
	ATTACH_ENTITY_TO_ENTITY(launcherTargets[iGuyIndex].objBull, launcherTargets[iGuyIndex].objBacking,0,<<0.0,-0.04,-0.410>>,<<0.0,0.0,0.0>>)		// -0.022

	// Create the Inner Zones of the target
	launcherTargets[iGuyIndex].objIn1 = CREATE_OBJECT_NO_OFFSET(PROP_TARGET_INNER1, vLoc)
	ATTACH_ENTITY_TO_ENTITY(launcherTargets[iGuyIndex].objIn1, launcherTargets[iGuyIndex].objBacking,0,<<0.0,-0.04,-0.410>>,<<0.0,0.0,0.0>>)
		
	// Create the Inner Zones of the target
	launcherTargets[iGuyIndex].objIn2 = CREATE_OBJECT_NO_OFFSET(PROP_TARGET_INNER2_B, vLoc)
	ATTACH_ENTITY_TO_ENTITY(launcherTargets[iGuyIndex].objIn2, launcherTargets[iGuyIndex].objBacking,0,<<0.0,-0.04,-0.410>>,<<0.0,0.0,0.0>>)
	
	// Create the Inner Zones of the target
	launcherTargets[iGuyIndex].objIn3 = CREATE_OBJECT_NO_OFFSET(PROP_TARGET_INNER3_B, vLoc)
	ATTACH_ENTITY_TO_ENTITY(launcherTargets[iGuyIndex].objIn3, launcherTargets[iGuyIndex].objBacking, 0,<<0.0,-0.04,-0.410>>,<<0.0,0.0,0.0>>)

	// Create the Arm
	launcherTargets[iGuyIndex].objArm = CREATE_OBJECT_NO_OFFSET(PROP_TARGET_ARM_B, vLoc)

	// Medium height variant.
	ATTACH_ENTITY_TO_ENTITY(launcherTargets[iGuyIndex].objBacking, launcherTargets[iGuyIndex].objArm, 0,<<0.0,0.007,-0.760>>,<<0.0,0.0,180.0>>)

	// Rotate the arm
	IF (eLocation = RANGELOC_PILLBOX_HILL)
		SET_ENTITY_ROTATION(launcherTargets[iGuyIndex].objArm,<<0.0,0.0,160.0>>)
	ELSE
		SET_ENTITY_ROTATION(launcherTargets[iGuyIndex].objArm,<<0.0,0.0,359.0>>)
	ENDIF
	FREEZE_ENTITY_POSITION(launcherTargets[iGuyIndex].objArm, TRUE)
	
	// Set it all as invincible! We can't have these things dying.
	SET_ENTITY_INVINCIBLE(launcherTargets[iGuyIndex].objBacking, TRUE)
	SET_ENTITY_INVINCIBLE(launcherTargets[iGuyIndex].objBull, TRUE)
	SET_ENTITY_INVINCIBLE(launcherTargets[iGuyIndex].objIn1, TRUE)
	SET_ENTITY_INVINCIBLE(launcherTargets[iGuyIndex].objIn2, TRUE)
	SET_ENTITY_INVINCIBLE(launcherTargets[iGuyIndex].objIn3, TRUE)
	SET_ENTITY_INVINCIBLE(launcherTargets[iGuyIndex].objArm, TRUE)
ENDPROC

PROC RANGE_LAUNCHER_PUT_GEAR_ON_PED(PED_INDEX rangePed, OBJECT_INDEX& muffs, OBJECT_INDEX& goggles)
	PRINTLN("PostStreamInit :: Putting Safety Gear on ped")
	muffs = CREATE_OBJECT(PROP_EAR_DEFENDERS_01, GET_ENTITY_COORDS(rangePed))
	ATTACH_ENTITY_TO_ENTITY(muffs, rangePed, GET_PED_BONE_INDEX(rangePed, BONETAG_HEAD), <<0.111, 0, 0>>, <<0, 90, 0>>)
	goggles = CREATE_OBJECT(PROP_SAFETY_GLASSES, GET_ENTITY_COORDS(rangePed))
	ATTACH_ENTITY_TO_ENTITY(goggles, rangePed, GET_PED_BONE_INDEX(rangePed, BONETAG_HEAD), <<0.059, 0.067, 0.000>>, <<180, 90, 0>>)
ENDPROC

/// PURPOSE:
///    Forces the range peds to aggro the player and stop blocking of non-temp events.
PROC RANGE_LAUNCHER_HANDLE_AGGRO_IN_SHOP()

	IF bAggroInShop
		EXIT
	ENDIF
	
	CPRINTLN( DEBUG_SHOOTRANGE, "RANGE_LAUNCHER_HANDLE_AGGRO_IN_SHOP called" )
	
	bAggroInShop = TRUE
		
	IF DOES_ENTITY_EXIST( pedGuy1 ) AND NOT IS_PED_DEAD_OR_DYING( pedGuy1 )
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( pedGuy1, FALSE )
		TASK_COMBAT_PED( pedGuy1, PLAYER_PED_ID() )
	ENDIF
	IF DOES_ENTITY_EXIST( pedGuy2 ) AND NOT IS_PED_DEAD_OR_DYING( pedGuy2 )
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( pedGuy2, FALSE )
		TASK_COMBAT_PED( pedGuy2, PLAYER_PED_ID() )
	ENDIF
	IF DOES_ENTITY_EXIST( pedGuy3 ) AND NOT IS_PED_DEAD_OR_DYING( pedGuy3 )
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( pedGuy3, FALSE )
		TASK_COMBAT_PED( pedGuy3, PLAYER_PED_ID() )
	ENDIF
	
ENDPROC

PROC LAUNCHER_CUSTOM_SCRIPT_INIT()
	scriptName = "range_modern"
	helpButtonPress = "PLAY_RANGE"
	
	// Need to shrink the range a bit, as it can be triggered outside the actual range.
	fLaunchScriptDist = 2.0
	fLauncherShutdownDist = TO_FLOAT(GENERIC_LAUNCHER_GET_BLIP_STREAMING_RANGE(STATIC_BLIP_MINIGAME_SHOOTING_RANGE1)) + 5.0
	eMinigame = MINIGAME_SHOOTING_RANGE
	
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(eMinigame))
		PRINTLN("Static blip is not active when it should be. Turning it on now!")
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_SHOOTING_RANGE1, TRUE)
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_SHOOTING_RANGE2, TRUE)
		bAllowScene = TRUE
	ELSE
		PRINTLN("Shooting range not available in flow. Turning off spawn scene.")
		bAllowScene = FALSE
	ENDIF

	// Figure out which range this is.
	IF VDIST(<<6.5940, -1100.1622, 28.7970>>, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 50.0
		PRINTLN("~~~~~~~~~~~~~~~~~~~~~~~~~~ LOADING PILLBOX! ~~~~~~~~~~~~~~~~~~~~~~~~")
		
		eLocation = RANGELOC_PILLBOX_HILL
		IF NOT IS_SHOP_OPEN_FOR_BUSINESS(GUN_SHOP_01_DT)
			bAllowScene = FALSE //turn off the trigger scene
		ENDIF
	ELSE
		PRINTLN("~~~~~~~~~~~~~~~~~~~~~~~~~~ LOADING CYPRESS! ~~~~~~~~~~~~~~~~~~~~~~~~")
		
		eLocation = RANGELOC_CYPRESS_FLATS
		IF NOT IS_SHOP_OPEN_FOR_BUSINESS(GUN_SHOP_11_ID1)
			PRINTLN("~~~~~~~~~~~~~~~~~~~~~~~~~~ SR NOT OPEN ~~~~~~~~~~~~~~~~~~~~~~~~")
			bAllowScene = FALSE //turn off the trigger scene
		ELSE
			PRINTLN("~~~~~~~~~~~~~~~~~~~~~~~~~~ SR GOOD! ~~~~~~~~~~~~~~~~~~~~~~~~")
		ENDIF
	ENDIF	
	
	iPriceToLaunch = PRICE_OF_RANGE
	
	#IF IS_DEBUG_BUILD
		iStackSize = MISSION_STACK_SIZE
	#ENDIF
	#IF IS_FINAL_BUILD
		iStackSize = MISSION_STACK_SIZE
	#ENDIF
ENDPROC


PROC LAUNCHER_CUSTOM_UNLOAD_ASSETS()
	// Request a few guys at the shooting range.
	guy1 = G_M_M_CHIGOON_02//Changed from A_M_O_GENBLACK01 by bobby wright to fix compile errors. 
	guy2 = G_M_M_CHIGOON_02//Changed from A_M_O_GENASIAN01 by bobby wright to fix compile errors. 
	guy3 = G_M_M_CHIGOON_02//Changed from A_M_O_GENASIAN01 by bobby wright to fix compile errors. 
	REQUEST_MODEL(guy1)
	REQUEST_MODEL(guy2)
	REQUEST_MODEL(guy3)
	
	// Also need the target data.
	REQUEST_MODEL(PROP_EAR_DEFENDERS_01)
	REQUEST_MODEL(PROP_SAFETY_GLASSES)
	REQUEST_MODEL(PROP_TARGET_ARM_B)
	REQUEST_MODEL(PROP_TARGET_BACKBOARD_B)
	REQUEST_MODEL(PROP_TARGET_BULL_B)
	REQUEST_MODEL(PROP_TARGET_INNER1)
	REQUEST_MODEL(PROP_TARGET_INNER2_B)
	REQUEST_MODEL(PROP_TARGET_INNER3_B)

ENDPROC

FUNC BOOL LAUNCHER_CUSTOM_REQUEST_ASSETS_WITH_LOADED_CHECK()
	// Request a few guys at the shooting range.
	guy1 = G_M_M_CHIGOON_02//Changed from A_M_O_GENBLACK01 by bobby wright to fix compile errors. 
	guy2 = G_M_M_CHIGOON_02//Changed from A_M_O_GENASIAN01 by bobby wright to fix compile errors. 
	guy3 = G_M_M_CHIGOON_02//Changed from A_M_O_GENASIAN01 by bobby wright to fix compile errors. 
	
	REQUEST_MODEL(guy1)
	IF NOT HAS_MODEL_LOADED(guy1)
		RETURN FALSE
	ENDIF
	
	REQUEST_MODEL(guy2)
	IF NOT HAS_MODEL_LOADED(guy2)
		RETURN FALSE
	ENDIF
	
	REQUEST_MODEL(guy3)
	IF NOT HAS_MODEL_LOADED(guy3)
		RETURN FALSE
	ENDIF
	
	// Also need the target data.
	REQUEST_MODEL(PROP_EAR_DEFENDERS_01)
	IF NOT HAS_MODEL_LOADED(PROP_EAR_DEFENDERS_01)
		RETURN FALSE
	ENDIF
	
	REQUEST_MODEL(PROP_SAFETY_GLASSES)
	IF NOT HAS_MODEL_LOADED(PROP_SAFETY_GLASSES)
		RETURN FALSE
	ENDIF
	
	REQUEST_MODEL(PROP_TARGET_ARM_B)
	IF NOT HAS_MODEL_LOADED(PROP_TARGET_ARM_B)
		RETURN FALSE
	ENDIF
	
	REQUEST_MODEL(PROP_TARGET_BACKBOARD_B)
	IF NOT HAS_MODEL_LOADED(PROP_TARGET_BACKBOARD_B)
		RETURN FALSE
	ENDIF
	
	REQUEST_MODEL(PROP_TARGET_BULL_B)
	IF NOT HAS_MODEL_LOADED(PROP_TARGET_BULL_B)
		RETURN FALSE
	ENDIF
	
	REQUEST_MODEL(PROP_TARGET_INNER1)
	IF NOT HAS_MODEL_LOADED(PROP_TARGET_INNER1)
		RETURN FALSE
	ENDIF
	
	REQUEST_MODEL(PROP_TARGET_INNER2_B)
	IF NOT HAS_MODEL_LOADED(PROP_TARGET_INNER2_B)
		RETURN FALSE
	ENDIF
	
	REQUEST_MODEL(PROP_TARGET_INNER3_B)
	IF NOT HAS_MODEL_LOADED(PROP_TARGET_INNER3_B)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC LAUNCHER_CUSTOM_REQUEST_ASSETS()
	IF bAllowScene
		LAUNCHER_CUSTOM_REQUEST_ASSETS_WITH_LOADED_CHECK()
	ENDIF
ENDPROC

FUNC BOOL LAUNCHER_CUSTOM_ASSETS_LOADED()
	IF bAllowScene
		// Make sure those guys are loaded.
		RETURN LAUNCHER_CUSTOM_REQUEST_ASSETS_WITH_LOADED_CHECK()
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC LAUNCHER_CUSTOM_RELEASE_ASSETS()
	IF bAllowScene
		// Also need the target data.
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_EAR_DEFENDERS_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_SAFETY_GLASSES)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_ARM_B)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_BACKBOARD_B)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_BULL_B)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_INNER1)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_INNER2_B)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_INNER3_B)
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the other guys at the shooting range.
PROC LAUNCHER_CUSTOM_SPAWN_SCENE()	

	IF bAllowScene
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		FLOAT fVDist2 = VDIST2(vPlayerPos, vLaunchLocation)
		IF (fVDist2 > 225)
			// Back alley coords where peds were spawning bc of interior issues: -13.6,-1090.6,26.7
			INTERIOR_INSTANCE_INDEX myInterior
			IF (eLocation = RANGELOC_PILLBOX_HILL)
				myInterior = GET_INTERIOR_AT_COORDS(<< 8.4083, -1095.4281, 28.8554 >>)
			ELSE
				myInterior = GET_INTERIOR_AT_COORDS(<<824.3015, -2162.9832, 30.9060>>)
			ENDIF
			IF IS_VALID_INTERIOR(myInterior)
				PIN_INTERIOR_IN_MEMORY(myInterior)
				WHILE NOT IS_INTERIOR_READY(myInterior)
					WAIT(0)
				ENDWHILE
				UNPIN_INTERIOR(myInterior)
			ENDIF
			
			// For now, just create 3 static peds.
			IF (eLocation = RANGELOC_PILLBOX_HILL)
				pedGuy1 = CREATE_PED(PEDTYPE_MISSION, guy1, << 8.4083, -1095.4281, 28.8554 >>, 336.03)		// Spot 0
				pedGuy2 = CREATE_PED(PEDTYPE_MISSION, guy2, << 10.1096, -1096.1354, 28.8554 >>, 336.03)		// Spot 2
				pedGuy3 = CREATE_PED(PEDTYPE_MISSION, guy3, <<13.7, -1097.7, 28.9>>, 336.03)				// Spot 4
			ELSE
				pedGuy1 = CREATE_PED(PEDTYPE_MISSION, guy1, <<817.0285, -2163.6570, 28.6569>>, 178.1556)	// Spot 4
				pedGuy2 = CREATE_PED(PEDTYPE_MISSION, guy2, <<819.0791, -2163.7432, 28.6568>>, 187.0609)	// Spot 5
				pedGuy3 = CREATE_PED(PEDTYPE_MISSION, guy3, <<821.4587, -2163.6165, 28.6567>>, 179.3586)	// Spot 7
			ENDIF
			
			RANGE_LAUNCHER_PUT_GEAR_ON_PED(pedGuy1, earMuffs1, goggles1)
			RANGE_LAUNCHER_PUT_GEAR_ON_PED(pedGuy2, earMuffs2, goggles2)
			RANGE_LAUNCHER_PUT_GEAR_ON_PED(pedGuy3, earMuffs3, goggles3)
			
			GIVE_WEAPON_TO_PED(pedGuy1, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
			GIVE_WEAPON_TO_PED(pedGuy2, WEAPONTYPE_COMBATPISTOL, INFINITE_AMMO, TRUE)
			GIVE_WEAPON_TO_PED(pedGuy3, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
			
			SET_ENTITY_AS_MISSION_ENTITY(pedGuy1)
			SET_ENTITY_AS_MISSION_ENTITY(pedGuy2)
			SET_ENTITY_AS_MISSION_ENTITY(pedGuy3)
			
			// Make them not shoot at one another.
			ADD_RELATIONSHIP_GROUP("range_IGNORE", shooterGroup)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedGuy1, shooterGroup)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedGuy2, shooterGroup)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedGuy3, shooterGroup)
			
			SET_PED_ACCURACY(pedGuy1, 20)
			SET_PED_ACCURACY(pedGuy2, 10)
			SET_PED_ACCURACY(pedGuy3, 15)
			
			SET_PED_STRAFE_CLIPSET(pedGuy1, "move_ped_strafing")
			SET_PED_STRAFE_CLIPSET(pedGuy2, "move_ped_strafing")
			SET_PED_STRAFE_CLIPSET(pedGuy3, "move_ped_strafing")
			
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_CIVMALE, shooterGroup)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_CIVFEMALE, shooterGroup)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_COP, shooterGroup)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, shooterGroup, RELGROUPHASH_CIVMALE)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, shooterGroup, RELGROUPHASH_CIVFEMALE)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, shooterGroup, RELGROUPHASH_COP)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy1, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy2, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuy3, TRUE)
			
			LAUNCHER_CreateTarget(0)
			LAUNCHER_CreateTarget(1)
			LAUNCHER_CreateTarget(2)
			
			//iTaskState = 0
			TASK_STAND_STILL(pedGuy1, -1)
			FREEZE_ENTITY_POSITION(pedGuy1, TRUE)
			bGuy1_Tasked = FALSE
			TASK_STAND_STILL(pedGuy2, -1)
			FREEZE_ENTITY_POSITION(pedGuy2, TRUE)
			bGuy2_Tasked = FALSE
			TASK_STAND_STILL(pedGuy3, -1)
			FREEZE_ENTITY_POSITION(pedGuy3, TRUE)
			bGuy3_Tasked = FALSE
			
			SET_PED_RESET_FLAG(pedGuy1, PRF_UseKinematicPhysics, TRUE)
			SET_PED_RESET_FLAG(pedGuy2, PRF_UseKinematicPhysics, TRUE)
			SET_PED_RESET_FLAG(pedGuy3, PRF_UseKinematicPhysics, TRUE)
			
			bSpawnedScene = TRUE
		ELSE
			bSpawnedScene = FALSE
		ENDIF
	ELSE
		
		PRINTLN(">>>>>>>>>>>not spawning dudes")
		bSpawnedScene = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    Waiting for the player to approach the start point.
PROC LAUNCHER_CUSTOM_APPROACH_WAIT()
	DRAW_DEBUG_SPHERE(<< 17.325, -1094.090, 31.580 >>, 0.2, 255,0,0)
	DRAW_DEBUG_SPHERE(<< 12.617, -1092.350, 31.580 >> , 0.2, 0, 255,0)

	IF DOES_ENTITY_EXIST(pedGuy1)
	AND NOT IS_PED_INJURED(pedGuy1)
		SET_PED_RESET_FLAG(pedGuy1, PRF_UseKinematicPhysics, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedGuy2)
	AND NOT IS_PED_INJURED(pedGuy2)
		SET_PED_RESET_FLAG(pedGuy2, PRF_UseKinematicPhysics, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedGuy3)
	AND NOT IS_PED_INJURED(pedGuy3)
		SET_PED_RESET_FLAG(pedGuy3, PRF_UseKinematicPhysics, TRUE)
	ENDIF
	
	IF bAllowScene
		// Need to clear the firing event so that peds don't freak out.
		IF NOT IS_ENTITY_DEAD(pedGuy1)
			FREEZE_ENTITY_POSITION(pedGuy1, FALSE)
			SET_PED_RESET_FLAG(pedGuy1, PRF_SupressGunfireEvents, TRUE)
		ENDIF
		IF NOT IS_ENTITY_DEAD(pedGuy2)
			FREEZE_ENTITY_POSITION(pedGuy2, FALSE)
			SET_PED_RESET_FLAG(pedGuy2, PRF_SupressGunfireEvents, TRUE)
		ENDIF
		IF NOT IS_ENTITY_DEAD(pedGuy3)
			FREEZE_ENTITY_POSITION(pedGuy3, FALSE)
			SET_PED_RESET_FLAG(pedGuy3, PRF_SupressGunfireEvents, TRUE)
		ENDIF
		
		// Update their tasks:
		VECTOR vPlayerPos
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		FLOAT fVDist2 = VDIST2(vPlayerPos, vLaunchLocation)
		
		// Okay, if the player is close make sure that they're shooting. Otherwise, they shouldn't be shooting.
		IF (fVDist2 < 400.0) // 20.0m2
			SEQUENCE_INDEX seqShoot
			VECTOR vBullseye
			
			IF NOT bGuy1_Tasked
				IF DOES_ENTITY_EXIST(launcherTargets[0].objBull) 
				AND DOES_ENTITY_EXIST(pedGuy1) AND NOT IS_PED_DEAD_OR_DYING(pedGuy1)
					SET_PED_ACCURACY(pedGuy1, 20)
					SET_PED_STRAFE_CLIPSET(pedGuy1, "move_ped_strafing")
					
					IF eLocation = RANGELOC_PILLBOX_HILL
						vBullseye = GET_ENTITY_COORDS(launcherTargets[0].objBull) - <<0,0,0.75>>
					ELSE
						vBullseye = GET_ENTITY_COORDS(launcherTargets[0].objBull) - <<0,0,0.77>>
					ENDIF
					OPEN_SEQUENCE_TASK(seqShoot)
						TASK_AIM_GUN_AT_COORD(NULL, vBullseye, 3000)
						TASK_SHOOT_AT_COORD(NULL, vBullseye, 2000, FIRING_TYPE_RANDOM_BURSTS)
						SET_SEQUENCE_TO_REPEAT(seqShoot, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seqShoot)
					TASK_PERFORM_SEQUENCE(pedGuy1, seqShoot)
					CLEAR_SEQUENCE_TASK(seqShoot)
					
					bGuy1_Tasked = TRUE
				ENDIF
			ENDIF
			
			IF NOT bGuy2_Tasked
				IF DOES_ENTITY_EXIST(launcherTargets[1].objBull) 
				AND DOES_ENTITY_EXIST(pedGuy2) AND NOT IS_PED_DEAD_OR_DYING(pedGuy2)
					SET_PED_ACCURACY(pedGuy2, 20)
					SET_PED_STRAFE_CLIPSET(pedGuy2, "move_ped_strafing")
					
					IF eLocation = RANGELOC_PILLBOX_HILL
						vBullseye = GET_ENTITY_COORDS(launcherTargets[1].objBull) - <<0,0,0.7>>
					ELSE
						vBullseye = GET_ENTITY_COORDS(launcherTargets[1].objBull) - <<0,0,0.7>>
					ENDIF
					OPEN_SEQUENCE_TASK(seqShoot)
						TASK_AIM_GUN_AT_COORD(NULL, vBullseye, 4000)
						TASK_SHOOT_AT_COORD(NULL, vBullseye, 1000, FIRING_TYPE_RANDOM_BURSTS)
						SET_SEQUENCE_TO_REPEAT(seqShoot, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seqShoot)
					TASK_PERFORM_SEQUENCE(pedGuy2, seqShoot)
					CLEAR_SEQUENCE_TASK(seqShoot)
					
					bGuy2_Tasked = TRUE
				ENDIF
			ENDIF
			
			IF NOT bGuy3_Tasked
				IF DOES_ENTITY_EXIST(launcherTargets[2].objBull) 
				AND DOES_ENTITY_EXIST(pedGuy3) AND NOT IS_PED_DEAD_OR_DYING(pedGuy3)
					SET_PED_ACCURACY(pedGuy3, 20)
					SET_PED_STRAFE_CLIPSET(pedGuy3, "move_ped_strafing")
					IF eLocation = RANGELOC_PILLBOX_HILL
						vBullseye = GET_ENTITY_COORDS(launcherTargets[2].objBull) - <<0,0,0.8>>
					ELSE
						vBullseye = GET_ENTITY_COORDS(launcherTargets[2].objBull) - <<0,0,0.68>>
					ENDIF
					OPEN_SEQUENCE_TASK(seqShoot)
						TASK_AIM_GUN_AT_COORD(NULL, vBullseye, 2000)
						TASK_SHOOT_AT_COORD(NULL, vBullseye, 1500, FIRING_TYPE_RANDOM_BURSTS)
						SET_SEQUENCE_TO_REPEAT(seqShoot, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seqShoot)
					TASK_PERFORM_SEQUENCE(pedGuy3, seqShoot)
					CLEAR_SEQUENCE_TASK(seqShoot)
					
					bGuy3_Tasked = TRUE
				ENDIF
			ENDIF
		ELSE
			// STOP FIRING!
			IF bGuy1_Tasked
				IF NOT IS_ENTITY_DEAD(pedGuy1)
					TASK_STAND_STILL(pedGuy1, -1)
					FREEZE_ENTITY_POSITION(pedGuy1, TRUE)
				ENDIF
				bGuy1_Tasked = FALSE
			ENDIF
			IF bGuy2_Tasked
				IF NOT IS_ENTITY_DEAD(pedGuy2)
					TASK_STAND_STILL(pedGuy2, -1)
					FREEZE_ENTITY_POSITION(pedGuy2, TRUE)
				ENDIF
				bGuy2_Tasked = FALSE
			ENDIF
			IF bGuy3_Tasked
				IF NOT IS_ENTITY_DEAD(pedGuy3)
					TASK_STAND_STILL(pedGuy3, -1)
					FREEZE_ENTITY_POSITION(pedGuy3, TRUE)
				ENDIF
				bGuy3_Tasked = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// So here, we need to detect if the player is withing range of the shooting range, and we'll be allowed to play. Control blip
	// visibility.
	// Also note that we can't see the blip unless we're in the range interior.
	INTERIOR_INSTANCE_INDEX hGunShop, hPlayerInterior
	IF (eLocation = RANGELOC_PILLBOX_HILL)
		hGunShop = GET_INTERIOR_AT_COORDS(<<13, -1098, 30>>)
		IF IS_BITMASK_AS_ENUM_SET(launcherFlags, LAUNCHER_BLIP_INACTIVE)
			// Blip shouldn't be active...
			IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_SHOOTING_RANGE1)
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_SHOOTING_RANGE1, FALSE)
			ENDIF
		ELSE
			// Blip should be active...
			IF NOT IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_SHOOTING_RANGE1)
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_SHOOTING_RANGE1, TRUE)
			ENDIF
		ENDIF
	ELSE
		hGunShop = GET_INTERIOR_AT_COORDS(<<821.0, -2162, 30>>)
		IF IS_BITMASK_AS_ENUM_SET(launcherFlags, LAUNCHER_BLIP_INACTIVE)
			// Blip shouldn't be active...
			IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_SHOOTING_RANGE2)
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_SHOOTING_RANGE2, FALSE)
			ENDIF
		ELSE
			// Blip should be active...
			IF NOT IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_SHOOTING_RANGE2)
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_SHOOTING_RANGE2, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	hPlayerInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
	IF (hPlayerInterior != hGunShop)
		SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_BLIP_INACTIVE)
	ENDIF
	
	// If the player is kicking off in the shop, we disable everything.
	IF IS_PLAYER_KICKING_OFF_IN_SHOP(GUN_SHOP_01_DT)
	OR IS_PLAYER_KICKING_OFF_IN_SHOP(GUN_SHOP_11_ID1)
		RANGE_LAUNCHER_HANDLE_AGGRO_IN_SHOP()
		SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SCRIPT_NOT_ALLOWING_RUN)
		SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_BLIP_INACTIVE)
	ENDIF
ENDPROC

/// PURPOSE:
///    The script has started. Emergency destroy everything.
PROC LAUNCHER_CUSTOM_CLEAR_SCENE()
	IF (bSpawnedScene)
		PRINTLN("Setting dudes as no longer needed")
		SET_OBJECT_AS_NO_LONGER_NEEDED(earMuffs1)
		SET_OBJECT_AS_NO_LONGER_NEEDED(earMuffs2)
		SET_OBJECT_AS_NO_LONGER_NEEDED(earMuffs3)
		SET_OBJECT_AS_NO_LONGER_NEEDED(goggles1)
		SET_OBJECT_AS_NO_LONGER_NEEDED(goggles2)
		SET_OBJECT_AS_NO_LONGER_NEEDED(goggles3)
		
		INT index = 0
		FOR index = 0 TO 2
			DELETE_OBJECT(launcherTargets[index].objBull)
			DELETE_OBJECT(launcherTargets[index].objIn3)
			DELETE_OBJECT(launcherTargets[index].objIn2)
			DELETE_OBJECT(launcherTargets[index].objIn1)
			DELETE_OBJECT(launcherTargets[index].objBacking)
			DELETE_OBJECT(launcherTargets[index].objArm)
		ENDFOR
		REMOVE_RELATIONSHIP_GROUP(shooterGroup)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(guy1)
		SET_MODEL_AS_NO_LONGER_NEEDED(guy2)
		SET_MODEL_AS_NO_LONGER_NEEDED(guy3)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_EAR_DEFENDERS_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_SAFETY_GLASSES)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_ARM_B)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_BACKBOARD_B)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_BULL_B)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_INNER1)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_INNER2_B)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TARGET_INNER3_B)
	ENDIF
	bSpawnedScene = FALSE
ENDPROC


FUNC THREADID LAUNCHER_CUSTOM_RUN_SCRIPT()
	SHOOTING_RANGE_LAUNCH_DATA launchData
	launchData.pedParticipant1 = pedGuy1
	launchData.pedParticipant2 = pedGuy2
	launchData.pedParticipant3 = pedGuy3
	launchData.objEarmuffs1 = earMuffs1
	launchData.objEarmuffs2 = earMuffs2
	launchData.objEarmuffs3 = earMuffs3
	launchData.objGlasses1 = goggles1
	launchData.objGlasses2 = goggles2
	launchData.objGlasses3 = goggles3
	launchData.eLocation = eLocation
	
    THREADID threadTemp = START_NEW_SCRIPT_WITH_ARGS(scriptName, launchData, SIZE_OF(launchData), iStackSize)
    SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptName)
	
	IF DOES_ENTITY_EXIST(pedGuy1) AND NOT IS_ENTITY_DEAD(pedGuy1)
		SET_PED_KEEP_TASK(pedGuy1, TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(pedGuy1)
	ENDIF
		IF DOES_ENTITY_EXIST(pedGuy2) AND NOT IS_ENTITY_DEAD(pedGuy2)
		SET_PED_KEEP_TASK(pedGuy2, TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(pedGuy2)
	ENDIF
		IF DOES_ENTITY_EXIST(pedGuy3) AND NOT IS_ENTITY_DEAD(pedGuy3)
		SET_PED_KEEP_TASK(pedGuy3, TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(pedGuy3)
	ENDIF

    RETURN threadTemp
ENDFUNC

USING "generic_launcher.sch"

