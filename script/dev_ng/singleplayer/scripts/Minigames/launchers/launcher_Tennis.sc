

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


CONST_INT 	IS_TENNIS_MULTIPLAYER				0
CONST_INT 	LAUNCHER_HAS_SCENE 					1
CONST_INT 	LAUNCHER_VOL_BUTTON_PRESS 			1
CONST_INT 	LAUNCHER_DONT_CHECK_RELOAD_RANGE 	1
CONST_INT 	LAUNCHER_DISABLE_FOR_MAGDEMO		1
CONST_INT 	LAUNCHER_DONT_FADE_DOWN				1
CONST_INT	MIN_AMBIENT_TENNIS_COURTS			1
CONST_INT	MAX_AMBIENT_TENNIS_COURTS			3	// Should not be above 6, limited by number of courts to play on at Venice.
CONST_INT	MIN_AMBIENT_COURT_INDEX				2
CONST_INT	MAX_AMBIENT_COURT_INDEX				7


USING "family_public.sch"
USING "generic_launcher_header.sch"
//USING "tennis_core_lib.sch"

ENUM TENNIS_LOCATION
	TENNIS_Venice = 1,
	TENNIS_MichaelsHouse,
	TENNIS_VineWoordHotel1,
	TENNIS_RichmanHotel1,
	TENNIS_RichmanHotel2,
	TENNIS_LSUCourt1,
	TENNIS_VespucciHotel,
	TENNIS_WeazelCourt1,
	TENNIS_ChumashHotel,
	TENNIS_MAX_COURT_LOCATIONS
ENDENUM
TENNIS_LOCATION	eThisCourtLoc

BOOL bAllowTennis = TRUE
BOOL bLaunchedWifeTennis = FALSE
BOOL bScriptRequested = FALSE

// Get the position of the launcher (where the player needs to go to trigger tennis).
FUNC VECTOR TENNIS_GET_LAUNCHER_LOCATION_BY_ID(INT iCourtID)
	TENNIS_LOCATION courtEnum = INT_TO_ENUM(TENNIS_LOCATION, iCourtID)
	
	SWITCH courtEnum	
		CASE TENNIS_Venice        	RETURN GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_TENNIS)
		CASE TENNIS_MichaelsHouse   RETURN GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_TENNIS_MICHAEL_HOUSE)
		CASE TENNIS_VineWoordHotel1 RETURN <<487.5186, -217.7697, 52.7864>>
		CASE TENNIS_RichmanHotel1   RETURN <<-1223.9077, 338.3685, 78.9859>>
		CASE TENNIS_RichmanHotel2   RETURN <<-1233.0884, 372.8109, 78.9812>>
		CASE TENNIS_LSUCourt1		RETURN <<-1618.4875, 266.4701, 58.5552>>
		CASE TENNIS_VespucciHotel   RETURN <<-936.0361, -1261.9667, 6.9773>>
		CASE TENNIS_WeazelCourt1	RETURN <<-1372.0155, -101.2864, 49.7046>>
		CASE TENNIS_ChumashHotel	RETURN <<-2869.9915, 9.2297, 10.6083>>
		DEFAULT RETURN <<0,0,0>>
	ENDSWITCH
ENDFUNC

// Get the nearest launcher location to the position given.
FUNC INT TENNIS_GET_NEAREST_LAUNCHER_LOCATION(VECTOR vTestPos)
	FLOAT fNearest = 9999999.9
	FLOAT fCurrent
	INT iNearest
	INT iCurrent
	
	REPEAT TENNIS_MAX_COURT_LOCATIONS iCurrent
		fCurrent = VDIST2(vTestPos, TENNIS_GET_LAUNCHER_LOCATION_BY_ID(iCurrent))
		IF fNearest > fCurrent
			fNearest = fCurrent
			iNearest = iCurrent
		ENDIF
	ENDREPEAT
	
	RETURN iNearest
ENDFUNC

FUNC STATIC_BLIP_NAME_ENUM TENNIS_GET_STATIC_BLIP_NAME_BY_ID(INT iCourtID)
	TENNIS_LOCATION courtEnum = INT_TO_ENUM(TENNIS_LOCATION, iCourtID)
	SWITCH courtEnum	
		CASE TENNIS_Venice        	RETURN STATIC_BLIP_MINIGAME_TENNIS
		CASE TENNIS_MichaelsHouse   RETURN STATIC_BLIP_MINIGAME_TENNIS_MICHAEL_HOUSE
		CASE TENNIS_VineWoordHotel1 RETURN STATIC_BLIP_MINIGAME_TENNIS_VINEWOOD_HOTEL1
		CASE TENNIS_RichmanHotel1   RETURN STATIC_BLIP_MINIGAME_TENNIS_RICHMAN_HOTEL1
		CASE TENNIS_RichmanHotel2   RETURN STATIC_BLIP_MINIGAME_TENNIS_RICHMAN_HOTEL1
		CASE TENNIS_LSUCourt1		RETURN STATIC_BLIP_MINIGAME_TENNIS_LSU_COURT1
		CASE TENNIS_VespucciHotel   RETURN STATIC_BLIP_MINIGAME_TENNIS_VESPUCCI_HOTEL
		CASE TENNIS_WeazelCourt1	RETURN STATIC_BLIP_MINIGAME_TENNIS_WEAZEL_COURT
		CASE TENNIS_ChumashHotel	RETURN STATIC_BLIP_MINIGAME_TENNIS_CHUMASH_HOTEL
		DEFAULT RETURN STATIC_BLIP_MINIGAME_TENNIS
	ENDSWITCH
	
ENDFUNC

FUNC STATIC_BLIP_NAME_ENUM TENNIS_GET_CLOSEST_TENNIS_STATIC_BLIP(VECTOR vTestPos)
	RETURN TENNIS_GET_STATIC_BLIP_NAME_BY_ID(TENNIS_GET_NEAREST_LAUNCHER_LOCATION(vTestPos))
ENDFUNC

/// PURPOSE:
///    Checks to see if we can run while a friend activity is going on.
FUNC BOOL LAUNCHER_CAN_RUN_FRIEND_ACTIVITY()
	// If we're on a friend activity, but our blip isn't active, quit.
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
		// Need to figure out which tennis blip we're checking.
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			// Check that blip!
			SWITCH INT_TO_ENUM(TENNIS_LOCATION, TENNIS_GET_NEAREST_LAUNCHER_LOCATION(GET_ENTITY_COORDS(PLAYER_PED_ID())))
				CASE TENNIS_Venice			RETURN IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_TENNIS)					BREAK
				CASE TENNIS_MichaelsHouse	RETURN IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_TENNIS_MICHAEL_HOUSE)		BREAK
				CASE TENNIS_VineWoordHotel1	RETURN IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_TENNIS_VINEWOOD_HOTEL1)	BREAK
				CASE TENNIS_RichmanHotel1	RETURN IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_TENNIS_RICHMAN_HOTEL1)		BREAK
				CASE TENNIS_LSUCourt1		RETURN IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_TENNIS_LSU_COURT1)			BREAK
				CASE TENNIS_VespucciHotel	RETURN IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_TENNIS_VESPUCCI_HOTEL)		BREAK
				CASE TENNIS_WeazelCourt1	RETURN IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_TENNIS_WEAZEL_COURT)		BREAK
				CASE TENNIS_ChumashHotel	RETURN IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_TENNIS_CHUMASH_HOTEL)		BREAK
				DEFAULT	RETURN FALSE	//we're on a friend activity, but we're at a court that isn't being checked
			ENDSWITCH
		ENDIF
	ENDIF
	
	// We're not even on a friend activity. Who cares?
	RETURN TRUE
ENDFUNC


// Scene setup loading
PROC LAUNCHER_CUSTOM_REQUEST_ASSETS()
	// Request tennis assets here here (instructor, ball, wife maybe?)
	IF bAllowTennis
		IF (eThisCourtLoc = TENNIS_Venice)
			REQUEST_SCRIPT("tennis_ambient")	
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL LAUNCHER_CUSTOM_ASSETS_LOADED()
	IF bAllowTennis
		IF (eThisCourtLoc = TENNIS_Venice)
			// If we're in Venice, stream the event script for people playing Tennis.
			RETURN HAS_SCRIPT_LOADED("tennis_ambient")
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL LAUNCHER_TENNIS_IS_MISSION_FLOW_AFTER_FAMILY_4() // check to see if we finished family 4
//	PRINTLN("LAUNCHER_TENNIS_IS_MISSION_FLOW_AFTER_FAMILY_4()")
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)
//		PRINTLN("We've completed family 4")
		RETURN TRUE
	ENDIF
//	PRINTLN("We have not completed family 4")
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCHER_TENNIS_IS_MISSION_FLOW_AFTER_FAMILY_5() // check to see if we're inbetween family 5/6
//	PRINTLN("LAUNCHER_TENNIS_IS_MISSION_FLOW_AFTER_FAMILY_5()")
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_5)
//		PRINTLN("We've completed family 5")
		RETURN TRUE
	ENDIF
//	PRINTLN("We have not completed family 5")
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCHER_TENNIS_IS_MISSION_FLOW_BEFORE_FAMILY_6() // check to see if we're inbetween family 5/6
//	PRINTLN("LAUNCHER_TENNIS_IS_MISSION_FLOW_BEFORE_FAMILY_6()")
	IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_6)
//		PRINTLN("We have not completed family 6")
		RETURN TRUE
	ENDIF
//	PRINTLN("We've completed family 6")
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCHER_TENNIS_IS_FRIEND_JIMMY_OR_AMANDA()
	IF NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())
		IF GET_NPC_PED_ENUM(FRIEND_A_PED_ID()) =  CHAR_JIMMY
		OR GET_NPC_PED_ENUM(FRIEND_A_PED_ID()) =  CHAR_AMANDA
			RETURN TRUE
		ENDIF
	ENDIF 
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCHER_TENNIS_IS_FRIEND_LAMAR()
	IF NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())
		IF GET_NPC_PED_ENUM(FRIEND_A_PED_ID()) =  CHAR_LAMAR
			RETURN TRUE
		ENDIF
	ENDIF 
	RETURN FALSE
ENDFUNC


FUNC BOOL LAUNCHER_TENNIS_IS_FRIEND_WITH_PLAYER()
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC LAUNCHER_CUSTOM_APPROACH_WAIT()
	
	
	enumCharacterList ePlayer = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	
	IF ePlayer = CHAR_FRANKLIN
		IF NOT LAUNCHER_TENNIS_IS_FRIEND_WITH_PLAYER()
			PRINTLN("Franklin has been detected so terminating launcher...")
			GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
			EXIT
		ENDIF
	ELIF ePlayer = CHAR_MICHAEL
		IF NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_PRELOADED_PLAYER_OUTFIT)
			SET_TENNIS_GLOBAL_FLAG(TGF_PRELOADED_PLAYER_OUTFIT)
			PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_TENNIS)
			CDEBUG2LN(DEBUG_TENNIS, "LAUNCHER_CUSTOM_APPROACH_WAIT :: PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_TENNIS) ", GET_FRAME_COUNT())
		ENDIF
	ELIF ePlayer = CHAR_TREVOR
		IF NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_PRELOADED_PLAYER_OUTFIT)
			SET_TENNIS_GLOBAL_FLAG(TGF_PRELOADED_PLAYER_OUTFIT)
			PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P2_TENNIS)
			CDEBUG2LN(DEBUG_TENNIS, "LAUNCHER_CUSTOM_APPROACH_WAIT :: PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P2_TENNIS) ", GET_FRAME_COUNT())
		ENDIF
	ENDIF
	
//	IF LAUNCHER_TENNIS_IS_FRIEND_WITH_PLAYER() // if we're on a friend activity
//		IF LAUNCHER_TENNIS_IS_FRIEND_LAMAR() // if we're with lamar
//			PRINTLN("Lamar has been detected so terminating launcher...")
//			GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
//			EXIT
//		ENDIF
//	ENDIF
	
	IF (eThisCourtLoc = TENNIS_MichaelsHouse)
		IF NOT bLaunchedWifeTennis
			IF (Get_Current_Event_For_FamilyMember(FM_MICHAEL_WIFE) = FE_M_WIFE_playing_tennis)
				IF NOT bScriptRequested
					REQUEST_SCRIPT("tennis_family")
					bScriptRequested = TRUE
				ELSE
					IF HAS_SCRIPT_LOADED("tennis_family")
						START_NEW_SCRIPT("tennis_family", MULTIPLAYER_MISSION_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED("tennis_family")
						bLaunchedWifeTennis = TRUE
					ENDIF
				ENDIF			
			ENDIF
		ELSE
			// If tennis has stopped, undo all of this.
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("tennis_family")) = 0
				bLaunchedWifeTennis = FALSE
				bScriptRequested = FALSE
			ENDIF
		ENDIF
		
		//Check if we're at michael's house. Kill the blip and the launcher not on a friend activity and if we're between family 5 and 6 or the char is franklin
		IF NOT LAUNCHER_TENNIS_IS_FRIEND_WITH_PLAYER()
			IF LAUNCHER_TENNIS_IS_MISSION_FLOW_AFTER_FAMILY_5() AND LAUNCHER_TENNIS_IS_MISSION_FLOW_BEFORE_FAMILY_6()
				PRINTLN("Can't play at Michael's between FAMILY_5 and FAMILY_6...")
				GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
				EXIT
			ENDIF
		ENDIF
	ENDIF	
	
	IF NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
		CDEBUG2LN(DEBUG_TENNIS, "HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID()) = FALSE ", GET_FRAME_COUNT())	//", PICK_STRING(HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID()), "TRUE ", "FALSE "), GET_FRAME_COUNT())
	ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_SPAWN_SCENE()
	// Setup your scene here.
	// Steal wife, get her on court, run any start tennis/init funcs here.
	IF bAllowTennis	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("tennis_ambient")) = 0
		// If we're in Venice, spawn a few courts playing Tennis.
		IF (eThisCourtLoc = TENNIS_Venice)
			TennisLaunchData sTennisData
			
			//Get a random number of games, between MIN_AMBIENT_TENNIS_COURTS and MAX_AMBIENT_TENNIS_COURTS
			INT iNumGames = GET_RANDOM_INT_IN_RANGE( MIN_AMBIENT_TENNIS_COURTS, MAX_AMBIENT_TENNIS_COURTS + 1 )
			INT iCourts[ MAX_AMBIENT_TENNIS_COURTS ]
			
			iCourts[0] = GET_RANDOM_INT_IN_RANGE( MIN_AMBIENT_COURT_INDEX, MAX_AMBIENT_COURT_INDEX + 1 )	// 0 is reserved for the player and 1 off limits so balls from the other game don't interrupt the player.
			
			INT i = 1
			WHILE i < iNumGames AND i < MAX_AMBIENT_TENNIS_COURTS
			
				iCourts[ i ] = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(iCourts, MAX_AMBIENT_TENNIS_COURTS, MIN_AMBIENT_COURT_INDEX, MAX_AMBIENT_COURT_INDEX)
				
				i++
				
			ENDWHILE

			CPRINTLN( DEBUG_TENNIS, "Playing ", iNumGames, " games!")
			#IF IS_DEBUG_BUILD
			i = 0
			WHILE i < iNumGames AND i < MAX_AMBIENT_TENNIS_COURTS
				CPRINTLN( DEBUG_TENNIS, "Playing on iCourts[", i, "]=", iCourts[i] )
				i++
			ENDWHILE
			#ENDIF
			
			i = 0
			WHILE i < iNumGames AND i < MAX_AMBIENT_TENNIS_COURTS
			
				IF iCourts[ i ] > 0
					sTennisData.eCourtToLaunch = INT_TO_ENUM(TENNISCOURT_LOCATION, iCourts[ i ])
					START_NEW_SCRIPT_WITH_ARGS("tennis_ambient", sTennisData, SIZE_OF(sTennisData), MULTIPLAYER_MISSION_STACK_SIZE)
				ENDIF
				
				i++
				
			ENDWHILE
		ENDIF
	ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_SCRIPT_INIT()
	eMinigame = MINIGAME_TENNIS
	
		
	scriptName = "tennis"
	iStackSize = MISSION_STACK_SIZE

	SET_BITMASK_ENUM_AS_ENUM(launcherFlags, LAUNCHER_FRIEND_ACTIVITY)
	helpButtonPress = "PLAY_TENNIS"
	
	// Figure out which court we're trying to run.
	// !!!! must do this first so we know where we are
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		eThisCourtLoc = INT_TO_ENUM(TENNIS_LOCATION, TENNIS_GET_NEAREST_LAUNCHER_LOCATION(vLaunchLocation))
	ENDIF
	
	eLauncherStaticBlip = TENNIS_GET_CLOSEST_TENNIS_STATIC_BLIP(vLaunchLocation)
	
	PRINTLN("Initializing court")
	
	bAllowTennis = TRUE
	
	enumCharacterList ePlayer = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())	
	
	
	IF ePlayer = CHAR_FRANKLIN
		PRINTLN("Franklin is not allowed to play")
		SET_BITMASK_ENUM_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FORCED_ON_INIT)
		EXIT
	ENDIF
	
	IF LAUNCHER_TENNIS_IS_FRIEND_WITH_PLAYER()
	AND NOT LAUNCHER_CAN_RUN_FRIEND_ACTIVITY()
		PRINTLN("[Tennis Launcher] We're on a friend activity and this court is disabled!")
		SET_BITMASK_ENUM_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FORCED_ON_INIT)
		EXIT
	ENDIF
	
	// If we're loading Mike's house, and the wife isn't playing Tennis, NO TENNIS!
	INT iHour = GET_CLOCK_HOURS()
	SWITCH eThisCourtLoc
		CASE TENNIS_MichaelsHouse	
		
			IF NOT IS_PLAYER_PED_AVAILABLE(CHAR_MICHAEL) //make sure michael is available since we're at his house
				PRINTLN("NOT IS_PLAYER_PED_AVAILABLE(CHAR_MICHAEL) detected, terminating launcher...")
				SET_BITMASK_ENUM_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FORCED_ON_INIT)
				EXIT
			ENDIF
			
			//Check if we're at michael's house. Kill the blip and the launcher if we're alone between family 5 and 6
			IF LAUNCHER_TENNIS_IS_FRIEND_WITH_PLAYER() //we're with a friend
				PRINTLN("We're on a friend activity so don't shut down early...")
			ELSE //we're not with a friend
				PRINTLN("We're not on a friend activity...")
				PRINTLN("Seeing who the player is...")
				IF ePlayer = CHAR_MICHAEL
					PRINTLN("Player is Michael")
					PRINTLN("Checking to see if we're between FAMILY_5 and FAMILY_6...")
					IF LAUNCHER_TENNIS_IS_MISSION_FLOW_AFTER_FAMILY_5() AND LAUNCHER_TENNIS_IS_MISSION_FLOW_BEFORE_FAMILY_6()
						PRINTLN("Can't play at Michael's between FAMILY_5 and FAMILY_6 without a friend ped...")
						SET_BITMASK_ENUM_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FORCED_ON_INIT)
						EXIT
					ELSE
						PRINTLN("Flow is not between FAMILY_5 and FAMILY_6 so we should be okay to play...")
					ENDIF
				ELIF ePlayer = CHAR_TREVOR
					PRINTLN("Player is Trevor")
					PRINTLN("Checking to see if we're AFTER FAMILY_6...")
					IF LAUNCHER_TENNIS_IS_MISSION_FLOW_AFTER_FAMILY_5() AND LAUNCHER_TENNIS_IS_MISSION_FLOW_BEFORE_FAMILY_6()
						PRINTLN("Can't play at Michael's between FAMILY_5 and FAMILY_6 without a friend ped...")
						SET_BITMASK_ENUM_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FORCED_ON_INIT)
						EXIT
					ELSE
						PRINTLN("Flow is not between FAMILY_5 and FAMILY_6 so we should be okay to play...")
					ENDIF
					IF NOT LAUNCHER_TENNIS_IS_MISSION_FLOW_AFTER_FAMILY_4()
						PRINTLN("Can't play at Michael's as Trevor before FAMILY_4 without a friend ped...")
						SET_BITMASK_ENUM_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FORCED_ON_INIT)
						EXIT
					ELSE
						PRINTLN("Flow after FAMILY_4 so we should be okay to play...")
					ENDIF
				ENDIF
			ENDIF
			
			
			PRINTLN("Setting up volume check at TENNIS_MichaelsHouse")
			bAllowTennis = TRUE
			
			// MIKE'S COURTS
			vVolPt1 = <<-765.014, 154.244, 67.25>>
			vVolPt2 = <<-782.014, 154.244, 67.25>>
			fVolWidth = 28.0
			fVolHeight = 2.0
		BREAK
		CASE TENNIS_Venice
			PRINTLN("Setting up volume check at TENNIS_Venice")
			// No ambient Tennis in Venice if it's early/late
			IF (iHour < 6) OR (iHour > 18)
				bAllowTennis = FALSE
			ENDIF
			
			// VENICE COURTS
			vVolPt1 = <<-1164.957642,-1667.261719, 4.28>>
			vVolPt2 = <<-1200.919312,-1615.872437, 4.28>>
			fVolWidth = 65.50
			fVolHeight = 5.0
		BREAK
		CASE TENNIS_VineWoordHotel1
			PRINTLN("Setting up volume check at TENNIS_VineWoordHotel1")
			IF (iHour < 6) OR (iHour > 18)
				bAllowTennis = FALSE
			ENDIF

			// Vinewood Hotel 1
			vVolPt1 = <<491.234802, -206.721466, 52.788494>>
			vVolPt2 = <<469.683502, -266.506958, 53.786392>>
			fVolWidth = 14.0
			fVolHeight = 5.0
		BREAK
		CASE TENNIS_RichmanHotel1
			PRINTLN("Setting up volume check at TENNIS_RichmanHotel1")
			IF (iHour < 6) OR (iHour > 18)
				bAllowTennis = FALSE
			ENDIF
			
			// Richman Hotel 1
			vVolPt1 = <<-1218.938843, 321.684998, 79.029518>>
			vVolPt2 = <<-1228.745972, 355.294708, 79.985886>>
			fVolWidth = 20.0
			fVolHeight = 2.0
		BREAK
		CASE TENNIS_RichmanHotel2
			PRINTLN("Setting up volume check at TENNIS_RichmanHotel2")
			IF (iHour < 6) OR (iHour > 18)
				bAllowTennis = FALSE
			ENDIF
			
			// Richman Hotel 2
			vVolPt1 = <<-1250.003418, 367.628326, 79.946281>>
			vVolPt2 = <<-1216.369263, 377.550262 ,80.102127>>
			fVolWidth = 20.0
			fVolHeight = 2.0
		BREAK
		CASE TENNIS_LSUCourt1
			PRINTLN("Setting up volume check at TENNIS_LSUCourt1")
			IF (iHour < 6) OR (iHour > 18)
				bAllowTennis = FALSE
			ENDIF
			
			// LSU Court
			vVolPt1 = <<-1610.861816, 249.700623, 60.116802>>
			vVolPt2 = <<-1626.516113, 282.946686, 59.830009>>
			fVolWidth = 32.0
			fVolHeight = 5.0
		BREAK
		CASE TENNIS_VespucciHotel
			PRINTLN("Setting up volume check at TENNIS_VespucciHotel")
			IF (iHour < 6) OR (iHour > 18)
				bAllowTennis = FALSE
			ENDIF
			
			// Vespucci Hotel
			vVolPt1 = <<-942.1958, -1251.2074, 7.1391>>
			vVolPt2 = <<-930.3892, -1271.5867, 8.0366>>
			fVolWidth = 12.0
			fVolHeight = 5.0
		BREAK
		CASE TENNIS_WeazelCourt1
			PRINTLN("Setting up volume check at TENNIS_WeazelCourt1")
			IF (iHour < 6) OR (iHour > 18)
				bAllowTennis = FALSE
			ENDIF
			
			// Weazel Court 1
			vVolPt1 = <<-1373.602, -83.734, 50.0>>
			vVolPt2 = <<-1369.198, -120.774, 50.0>>
			fVolWidth = 20.840
			fVolHeight = 5.0
			fMaxTriggerDist = 19.60 // cut corners
		BREAK
		CASE TENNIS_ChumashHotel
			PRINTLN("Setting up volume check at TENNIS_ChumashHotel")
			IF (iHour < 6) OR (iHour > 18)
				bAllowTennis = FALSE
			ENDIF
			
			// Chumash Hotel
			vVolPt1 = <<-2860.7622, 13.3794, 10.6083>>
			vVolPt2 = <<-2924.0659, 31.5452, 10.6083>>
			fVolWidth = 28.0
			fVolHeight = 5.0
		BREAK
	ENDSWITCH
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
		eTransMode = TM_ANY
	ENDIF
	// Set the outfit to preload if the launcher is starting up again
	
	
	IF ePlayer = CHAR_MICHAEL
		IF NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_PRELOADED_PLAYER_OUTFIT)
			SET_TENNIS_GLOBAL_FLAG(TGF_PRELOADED_PLAYER_OUTFIT)
			PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_TENNIS)
			CDEBUG2LN(DEBUG_TENNIS, "LAUNCHER_CUSTOM_SCRIPT_INIT :: PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_TENNIS) ", GET_FRAME_COUNT())
		ENDIF
	ELIF ePlayer = CHAR_TREVOR
		IF NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_PRELOADED_PLAYER_OUTFIT)
			SET_TENNIS_GLOBAL_FLAG(TGF_PRELOADED_PLAYER_OUTFIT)
			PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P2_TENNIS)
			CDEBUG2LN(DEBUG_TENNIS, "LAUNCHER_CUSTOM_SCRIPT_INIT :: PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P2_TENNIS) ", GET_FRAME_COUNT())
		ENDIF
	ENDIF
	
	// Check the weather...		
	IF (GET_RAIN_LEVEL() + GET_SNOW_LEVEL() > 0.1)
		bAllowTennis = FALSE
	ENDIF

	
	#IF IS_DEBUG_BUILD
		// # 160088 - Rob was experiencing an issue with teleporting from one launcher point to the other.
		// In debug only, allow multiple copies.
		SET_BITMASK_ENUM_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_ALLOW_MULTIPLE_COPIES)
	#ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_RELEASE_ASSETS()
	CDEBUG2LN(DEBUG_TENNIS, "LAUNCHER_CUSTOM_RELEASE_ASSETS called")
	CLEAR_TENNIS_GLOBAL_FLAG(TGF_PRELOADED_PLAYER_OUTFIT)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		RELEASE_PRELOADED_OUTFIT(PLAYER_PED_ID())
	ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_CLEANUP()
	CDEBUG2LN(DEBUG_TENNIS, "LAUNCHER_CUSTOM_CLEANUP called")
	CLEAR_TENNIS_GLOBAL_FLAG(TGF_PRELOADED_PLAYER_OUTFIT)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		RELEASE_PRELOADED_OUTFIT(PLAYER_PED_ID())
	ENDIF
ENDPROC


USING "generic_launcher.sch"

