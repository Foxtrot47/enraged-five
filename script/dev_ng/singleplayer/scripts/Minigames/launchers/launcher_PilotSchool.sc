//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	launcher_PilotSchool.sch									//
//		AUTHOR			:	Alwyn Roberts												//
//		DESCRIPTION		:	default functions and primary loop for minigame launchers	//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

//CONST_INT LAUNCHER_POINT_ENTER 1
CONST_INT LAUNCHER_CUSTOM_SCRIPT_LAUNCH 1
CONST_INT LAUNCHER_CLEAR_SCENE_ON_START 1
CONST_INT LAUNCHER_SNAP_CHECKPOINT_TO_GROUND 1
CONST_INT LAUNCHER_BUTTON_PRESS 1

#IF IS_DEBUG_BUILD
	CONST_INT LAUNCHER_DONT_CHECK_RELOAD_RANGE		1
#ENDIF

USING"script_camera.sch"
USING "generic_launcher_header.sch"
USING "flow_help_public.sch"
USING "script_oddjob_funcs.sch"
USING "ps_launcher_shared.sch"

ENUM PS_AMB_GUARD_STATE_ENUM
	PS_AMB_GUARD_IDLE_1,
	PS_AMB_GUARD_IDLE_2,
	PS_AMB_GUARD_IDLE_3,
	PS_AMB_GUARD_IDLE_4,
	PS_AMB_GUARD_TALK_1,
	PS_AMB_GUARD_TALK_2,
	PS_AMB_GUARD_TALK_3,
	PS_AMB_GUARD_GESTURE_1,
	PS_AMB_GUARD_GESTURE_2
ENDENUM

PS_LAUNCH_ARGS psArgs
VEHICLE_SAVE_ARGS myVehicleSaveData
BOOL bBlipKilled = FALSE


PROC LAUNCHER_CUSTOM_SCRIPT_INIT()
	eMinigame = MINIGAME_PILOT_SCHOOL
	helpButtonPress = "PLAY_PSCHOOL"
	scriptName = "Pilot_School"
	iStackSize = MISSION_STACK_SIZE
ENDPROC

FUNC THREADID LAUNCHER_CUSTOM_RUN_SCRIPT()
	
//	myVehicleSaveData.playerVehicle = GET_PLAYERS_LAST_VEHICLE()
//	
//	IF DOES_ENTITY_EXIST(myVehicleSaveData.playerVehicle) AND NOT IS_ENTITY_DEAD(myVehicleSaveData.playerVehicle) AND NOT IS_ENTITY_ON_SCREEN(myVehicleSaveData.playerVehicle)
//		ODDJOB_SAVE_VEHICLE_AND_WARP(myVehicleSaveData, <<0,0,0>>)
//	ENDIF
	
	DEBUG_MESSAGE("******************** Launching Pilot_School.sc ********************")
	// Nothing custom has been done, just run default.
	THREADID threadTemp = START_NEW_SCRIPT_WITH_ARGS(scriptName, psArgs, SIZE_OF(psArgs),iStackSize)
	SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptName)
	
	RETURN threadTemp
	
ENDFUNC

PROC LAUNCHER_CUSTOM_APPROACH_WAIT()
	IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_PILOT_SCHOOL)
		IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1154.1101, -2715.2026, 18.8074>>, <<0, 0, 1.8>>, TRUE)
	ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_SCRIPT_END()
	ODDJOB_RESTORE_SAVED_VEHICLE(myVehicleSaveData)
ENDPROC

PROC LAUNCHER_CUSTOM_CLEAR_SCENE()
	SET_ROADS_IN_ANGLED_AREA(<< -1093.842, -2375.285, -100>>, <<-1007.240, -2425.285, 100>>, 150.00, FALSE, TRUE)
ENDPROC

PROC LAUNCHER_CUSTOM_CLEANUP()
	IF bBlipKilled
		GENERIC_LAUNCHER_REINSTATE_EVENT_BLIP(STATIC_BLIP_MINIGAME_PILOT_SCHOOL)
	ENDIF
ENDPROC

USING "generic_launcher.sch"

