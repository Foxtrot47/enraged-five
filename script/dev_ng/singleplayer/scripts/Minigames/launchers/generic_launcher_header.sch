//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	generic_launcher_header.sch									//
//		AUTHOR			:	Ryan Paradis												//
//		DESCRIPTION		:	data structures, consts, and variables used by minigames launchers//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "minigames_helpers.sch"
USING "script_oddjob_funcs.sch"
USING "minigame_uiinputs.sch"
USING "friendActivity_public.sch" 
USING "selector_public.sch"
USING "flow_help_public.sch"
USING "context_control_public.sch"
USING "selector_public.sch"
USING "rich_presence_public.sch"
USING "minigame_public.sch"

//	**************************************** GENERAL USE FLAGS & INSTRUCTIONS ****************************************

// SHOULD WE SPAWN A LAUNCH VEHICLE?
// Must set variable: 	vehicleModel - Model of the vehicle to be created.
#IF NOT DEFINED(LAUNCHER_VEHICLE_SPAWN)
	CONST_INT LAUNCHER_VEHICLE_SPAWN  0
#ENDIF

// Should we set this script as a trigger script?

#IF NOT DEFINED(LAUNCHER_HAS_TRIGGER_SCENE)
	CONST_INT LAUNCHER_HAS_TRIGGER_SCENE	0
#ENDIF

// SHOULD WE START THE SCRIPT WHEN THE PLAYER GETS IN A VEHICLE? 
#IF NOT DEFINED(LAUNCHER_VARIED_VEHICLE_ENTER)
	CONST_INT LAUNCHER_VARIED_VEHICLE_ENTER  0
#ENDIF

// SHOULD WE START THE SCRIPT WHEN THE PLAYER GETS IN A VEHICLE? 
#IF NOT DEFINED(LAUNCHER_VEHICLE_ENTER)
	CONST_INT LAUNCHER_VEHICLE_ENTER  0
#ENDIF

// SHOULD WE START THE SCRIPT WHEN THE PLAYER IS STARTING TO GET IN A VEHICLE? 
#IF NOT DEFINED(LAUNCHER_VEHICLE_GETTING_IN)
	CONST_INT LAUNCHER_VEHICLE_GETTING_IN  0
#ENDIF

// CAN THE PLAYER ENTER ANY SEAT TO LAUNCH, OR DOES IT HAVE TO BE THE DRIVER'S SEAT?
#IF NOT DEFINED(LAUNCHER_VEHICLE_ENTER_ANY_SEAT)
	CONST_INT LAUNCHER_VEHICLE_ENTER_ANY_SEAT  0
#ENDIF

// DOES THE LAUNCH VEHICLE NEED TO BE IN A SPECIFIC SPOT?
#IF NOT DEFINED(LAUNCHER_VEHICLE_IGNORE_PROXIMITY)
	CONST_INT LAUNCHER_VEHICLE_IGNORE_PROXIMITY 0
#ENDIF

// SHOULD WE START THE SCRIPT WHEN THE PLAYER HITS THE LAUNCHER POINT IN A VEHICLE?
// Can set variable:	triggerVehicleType - a VEHICLE_TRIGGER_TYPE that he needs to be in. Defaults to ANY.
#IF NOT DEFINED(LAUNCHER_POINT_ENTER_IN_VEHICLE)
	CONST_INT LAUNCHER_POINT_ENTER_IN_VEHICLE  0
#ENDIF

#IF NOT DEFINED(LAUNCHER_POINT_AUTO_TRIGGER_IN_VEHICLE)
	CONST_INT LAUNCHER_POINT_AUTO_TRIGGER_IN_VEHICLE  0
#ENDIF

#IF NOT DEFINED(LAUNCHER_POINT_TRIGGER_WITH_FRIEND_IN_VEHICLE)
	CONST_INT LAUNCHER_POINT_TRIGGER_WITH_FRIEND_IN_VEHICLE  0
#ENDIF

#IF NOT DEFINED(LAUNCHER_IGNORE_VEHICLE_HEALTH)
	CONST_INT LAUNCHER_IGNORE_VEHICLE_HEALTH  0
#ENDIF

#IF NOT DEFINED(LAUNCHER_IGNORE_PLAYER_CONTROL)
	CONST_INT LAUNCHER_IGNORE_PLAYER_CONTROL  0
#ENDIF

#IF NOT DEFINED(LAUNCHER_DISABLE_FOR_MAGDEMO)
	CONST_INT LAUNCHER_DISABLE_FOR_MAGDEMO  0
#ENDIF

#IF NOT DEFINED(LAUNCHER_CHECK_SPAWN_DIST)
	CONST_INT LAUNCHER_CHECK_SPAWN_DIST  0
#ENDIF

// DOES THIS LAUNCHER ONLY RUN IN SPECIAL AND COLLECTORS EDITIONS?
#IF NOT DEFINED(LAUNCHER_COLLECTORS_AND_SPECIAL_EDITION_ONLY)
	CONST_INT LAUNCHER_COLLECTORS_AND_SPECIAL_EDITION_ONLY  0
#ENDIF

// SHOULD THE PLAYER NEED TO PRESS A BUTTON, NOT JUST IN A CIRCLE AROUND THE LACUNH POINT, BUT IN A VOLUME?
// Must set variable:	buttonPressVol - this is the volume we will check for the player
#IF NOT DEFINED(LAUNCHER_VOL_BUTTON_PRESS)
	CONST_INT LAUNCHER_VOL_BUTTON_PRESS  0
#ENDIF

// SHOULD THE GENERIC LAUNCHER RUN THEFUNCTION THAT ALLOWS YOU TO RUN A SCRIPT?
// This is generally used when you need to pass variables into the script.
#IF NOT DEFINED(LAUNCHER_CUSTOM_SCRIPT_LAUNCH)
	CONST_INT LAUNCHER_CUSTOM_SCRIPT_LAUNCH  0
#ENDIF

// SHOULD THE LAUNCHER CALL LAUNCHER_CUSTOM_CLEAR_SCENE() WHEN THE MINIGAME STARTS?
// Generally, you want this, so that you can order AI to do something on minigame starts.
#IF NOT DEFINED(LAUNCHER_CLEAR_SCENE_ON_START)
	CONST_INT LAUNCHER_CLEAR_SCENE_ON_START 0
#ENDIF

// SHOULD THE PLAYER HAVE TO BE A CERTAIN CHARACTER TO START THE MINIGAME?
// Must set variable:	characterRequired - the enumCharacterList value of the character the player must be.
#IF NOT DEFINED(LAUNCHER_CHAR_REQUIRED)
	CONST_INT LAUNCHER_CHAR_REQUIRED 0
#ENDIF

// SHOULD THE LAUNCHER CHECKPOINT SNAP TO THE GROUND?
// This is to prevent floating launchers. Odds are, you want this.
#IF NOT DEFINED(LAUNCHER_SNAP_CHECKPOINT_TO_GROUND)
	CONST_INT LAUNCHER_SNAP_CHECKPOINT_TO_GROUND 0 
#ENDIF

// DOES THIS LAUNCHER HAVE A SCENE THAT WILL RUN EVEN IF THE EVENT CAN'T BE PLAYED BY THE PLER
// No variables.
#IF NOT DEFINED(LAUNCHER_HAS_SCENE)
	CONST_INT LAUNCHER_HAS_SCENE 0
#ENDIF

// DOES THIS LAUNCHER HAVE A CUTSCENE THAT NEEDS TO BE LOADED?
// No variables.
#IF NOT DEFINED(LAUNCHER_HAS_CUT_SCENE)
	CONST_INT LAUNCHER_HAS_CUT_SCENE 0
#ENDIF

// SHOULD THE LAUNCHER CIRCUMVENT THE SHOULDER BUTTONPRESS TO START THE MINIGAME.
// No variables.
#IF NOT DEFINED(LAUNCHER_NO_BUTTONPRESS)
	CONST_INT LAUNCHER_NO_BUTTONPRESS 0
#ENDIF

// SHOULD THE LAUNCHER CIRCUMVENT THE FADE DOWN
// (Launcher normally fades down. Use this in case you don't want ANY fade)
#IF NOT DEFINED(LAUNCHER_DONT_FADE_DOWN)
	CONST_INT LAUNCHER_DONT_FADE_DOWN 0
#ENDIF

// SHOULD THE LAUNCHER CIRCUMVENT THE LOAD RANGE CHECK
// (Generally, you can't be within a few m of the launcher to get it to acknowledge that you're there after starting, or reloading. This ignores that.)
#IF NOT DEFINED(LAUNCHER_DONT_CHECK_RELOAD_RANGE)
	CONST_INT LAUNCHER_DONT_CHECK_RELOAD_RANGE 0
#ENDIF

// IS THIS A LAUNCHER THAT IS BROUGHT IN BY AN OBJECT?
#IF NOT DEFINED(LAUNCHER_OBJECT_ATTACHED)
	CONST_INT LAUNCHER_OBJECT_ATTACHED  0
#ENDIF

// SHOULD THE LAUNCHER ONLY WORK IN AN INTERIOR
// Must set variable:	eInterior - the interior that the player must be inside of for the launcher to work.
#IF NOT DEFINED(LAUNCHER_CHECK_INTERIOR)
	CONST_INT LAUNCHER_CHECK_INTERIOR  0
#ENDIF

// DOES THE LAUNCHER ALLOW REPLAYS ON FAILS?
// Must set variable:	eInterior - the interior that the player must be inside of for the launcher to work.
#IF NOT DEFINED(LAUNCHER_REPLAYS_ENABLED)
	CONST_INT LAUNCHER_REPLAYS_ENABLED  0
#ENDIF

// DOES THE LAUNCHER ONLY WORK DURING CERTAIN TIMES OF DAY?
// Must set variable:	eInterior - the interior that the player must be inside of for the launcher to work.
#IF NOT DEFINED(LAUNCHER_TIME_RESTRICTED)
	CONST_INT LAUNCHER_TIME_RESTRICTED  0
#ENDIF

//DOES THE LAUNCHER REQUIRE MONEY?
#IF NOT DEFINED(LAUNCHER_MONEY_RESTRICTED)
	CONST_INT LAUNCHER_MONEY_RESTRICTED  0
#ENDIF

//DOES THE LAUNCHER REQUIRE SPECIAL PED VARIATIONS?
#IF NOT DEFINED(LAUNCHER_PED_VARIATIONS)
	CONST_INT LAUNCHER_PED_VARIATIONS  0
#ENDIF

//DOES THE LAUNCHER REQUIRE MONEY?
#IF NOT DEFINED(LAUNCHER_ON_FOOT_RESTRICTED)
	CONST_INT LAUNCHER_ON_FOOT_RESTRICTED  0
#ENDIF

//SHOULD WE CHECK FOR FIRE?
#IF NOT DEFINED(LAUNCHER_CHECK_FOR_FIRE)
	CONST_INT LAUNCHER_CHECK_FOR_FIRE  0
#ENDIF

//  *********************************************************************************************************************
//  ***************************************************  END OF FLAGS  **************************************************
//  *********************************************************************************************************************

#IF NOT DEFINED(LAUNCHER_POINT_ENTER)
	#IF NOT LAUNCHER_VEHICLE_ENTER
		#IF NOT LAUNCHER_VOL_BUTTON_PRESS
			#IF NOT LAUNCHER_POINT_ENTER_IN_VEHICLE
				CONST_INT LAUNCHER_POINT_ENTER  1
			#ENDIF
		#ENDIF
	#ENDIF
	#IF LAUNCHER_VEHICLE_ENTER
		CONST_INT LAUNCHER_POINT_ENTER  0
	#ENDIF
	#IF LAUNCHER_VOL_BUTTON_PRESS
		CONST_INT LAUNCHER_POINT_ENTER  0
	#ENDIF
	#IF LAUNCHER_POINT_ENTER_IN_VEHICLE
		CONST_INT LAUNCHER_POINT_ENTER  1
	#ENDIF
#ENDIF

ENUM LAUNCHER_STATE
	STREAM_ASSETS,				// Request any standard assets for the type. Additional assets can be requested in LAUNCHER_CUSTOM_REQUEST_ASSETS
	STREAMING_WAIT,				// Checks to see if standard assets for the type are streamed. Additional checks can be added in LAUNCHER_CUSTOM_ASSETS_LOADED
	SCENE_SETUP,				// If we need to spawn a scene around the area, we do it here.
	APPROACH_WAIT,				// Waits for the player to trigger the mission, based on type. Exits if player is too far away. Custom logic can be added to LAUNCHER_CUSTOM_APPROACH_WAIT
	IDLE_WAIT,					// Low memory usage IDLE state waits for a chance to load assets back in or a signal to terminate the launcher.
	RUN_SCRIPT,					// Start the minigame script.
	RUN_WAIT,					// Waits for the launched script to shutdown. Custom logic can be added to LAUNCHER_CUSTOM_RUN_WAIT
	UNLOAD_ASSETS,				// Unloads assets to free up memory and moves to idle wait.
	
	WAIT_FOR_FAILSCREEN_REPLAY,	// After failing certain minigames, we go to this state to wait for some input on the fail screen.
	WAIT_FOR_TERMINATE,			// A non existant state that forces the script to just wait on termination.
	
	CLEANUP_LAUNCHER			// Standard cleanup for assets of launcher type. Custom cleanup can be added to LAUNCHER_CUSTOM_CLEANUP
ENDENUM

ENUM LAUNCHER_SLEEP_REASON
	ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE,
	ASLEEP_WAITING_FOR_PLAYER_TO_SPAWN_VEH,
	ASLEEP_WAITING_FOR_MISSION
	

ENDENUM

ENUM LAUNCHER_STATUS_FLAGS
	// Launch variations
	LAUNCHER_FRIEND_ACTIVITY = BIT0,						// This minigame is a friend activity, and can be launched while a friend is with the player
	LAUNCHER_MISSION_CANDIDATE_STARTED		= BIT1,			// Were we ever approved as a mission candidate? Can't call Mission_Over() unless this is set.
	
	LAUNCHER_SPAWNED_SCENE_CUSTOM			= BIT2,			// are we done spawning the custom scene? This gets done in the update loop, so only do it once.
	
	LAUNCHER_RUN_DESPITE_MISSION			= BIT3,			// We should be running even if there's a mission active. VERY SPECIAL CASE. GET APPROVAL FIRST.
	LAUNCHER_NEED_SETUP_ON_MISSION			= BIT4,			// Do we need our scene setup on a mission too?
	LAUNCHER_SHOULD_UNLOCK_CARS				= BIT5,			// Cars were spawned as locked, we'll need to unlock them when a mission is done.
	
	LAUNCHER_SCRIPT_NOT_ALLOWING_RUN		= BIT6,			// There are some cases where the init is not going to allow the minigame to run but the launcher needs to stay active.
	
	LAUNCHER_BLIP_INACTIVE					= BIT7,			// In some cases (Shooting Range), the launcher needs to control the blip based on player proximity. This flag
															// will let the launcher know that the blip could be active (most conditions met).
	
	LAUNCHER_FINISHED_INIT_FADE_DOWN		= BIT8,			// Are we finished with the initial fade down? Use this to check if we should block player input.
	
	LAUNCHER_OUTFIT_NOTIFIED				= BIT9,			// Have we notified the player that he needs a different outfit?
	
	LAUNCHER_ON_FOOT_NOTIFIED				= BIT10,		// Have we notified the player that he needs to be on foot for this MG?
	
	LAUNCHER_HELP_TEXT_DISPLAYED			= BIT11,		// Flag to tell us if we printed the help text. When we're clearing the help text, this will make sure we don't clear it multiple times.
	
	LAUNCHER_JUSTDONE_WAIT_FOR_LEAVE		= BIT12,		// After completing a mission, if the player ends where another mission starts, wait for him to leave the area.
	
	//Extra functionality
	LAUNCHER_PERFORM_ONE_TIME_TASK			= BIT18,			// Task that should be performed only once, can be set 
	// Shutdown variations
	LAUNCHER_SHUTDOWN_FOR_MISSION_LEADIN	= BIT19,		// Shutdown the launcher if a mission leadin is running (for minigame launchers with a large memory footprint)
	LAUNCHER_SHUTDOWN_ON_PLAYER_DEATH 		= BIT20,		// Does the launcher (and minigame) shutdown on player death? Default should be no
	LAUNCHER_SHUTDOWN_IF_MISSION_RUNNING 	= BIT21,		// Does the launcher (and minigame) shutdown if a mission starts running? Default should be no
	LAUNCHER_SHUTDOWN_ALLOW_MULTIPLE_COPIES = BIT22,		// Does the launcher allow multiple copies? If not, it will shutdown if there is one running already. Default is no
	LAUNCHER_SHUTDOWN_FORCED_ON_INIT		= BIT23,		// Flag made when a bitset index is not used, to terminate a script after the init function is called.
	LAUNCHER_SHUTDOWN_FORCED_ON_FINISH		= BIT24,		// After the mission is over, there's a chance we could be ordered to leave.

	// Misc checks
	LAUNCHER_PRICE_NOTIFIED					= BIT25,		// Have we notified the player that he can't afford this minigame?
	LAUNCHER_PRINTED_VEHICLE_FIRE			= BIT26,		// Have we printed that the vehicle is too flaming to enter?
	LAUNCHER_TOD_NOTIFIED					= BIT27,		// Have we notified the player that he can't do this race at this TOD?
	LAUNCHER_ALREADY_SNAPPED_CP 			= BIT28,		// Have we already snapped the CP after being told to?
	LAUNCHER_WRONG_VEHICLE_PRINTED 			= BIT29,		// Have we already printed the wrong vehicle type help msg?
	LAUNCHER_PRINTED_VEHICLE_DMG			= BIT30			// Have we printed that the vehicle is too damaged to enter?
	
ENDENUM

ENUM VEHICLE_TRIGGER_TYPE
	LAUNCHER_VEHTYPE_ANY_VEH = 0
ENDENUM

CONST_FLOAT	LAUNCHER_SHUTDOWN_DIST				125.0		// There is a default hysteresis for killing a script. To override, set fLauncherShutdownDist in LAUNCHER_CUSTOM_SCRIPT_INIT	
CONST_FLOAT LAUNCHER_TOO_CLOSE_DIST				10.0		//Distance to check if player is directly on/next to spawn point
CONST_FLOAT LAUNCHER_VEHICLE_SPAWN_DISTANCE		80.0		//Distance to check if player is looking at spawn point before spawning
CONST_INT	SLEEP_MODE_POLL_FREQ				150			// how many frames to wait between polling to reactivate (assuming 30 fps)

// Standard data for all launcher types. Can be accessed by any of the custom functions
THREADID				threadInstance
TEXT_LABEL_63			scriptName
	
// Need this label when we launch a script.
TEXT_LABEL_63 						scriptToRun
PED_INDEX							pedPlayer
VEHICLE_INDEX						vehicleLaunch
VECTOR 								vPlayerLocation
VECTOR								vLaunchLocation
FLOAT 								fPlayerDistToLauncher
INT					 				launcherFlags
LAUNCHER_STATE 						launcherState
INT									iContextIntention = NEW_CONTEXT_INTENTION
INT									iStackSize		= FRIEND_STACK_SIZE	// We use a default FRIENDS stack size, but allow individuals to override.
INT									iMissionCandInd = NO_CANDIDATE_ID	// This is for when we're requesting to launch a mission. Need a candidate ID.
SP_MINIGAMES						eMinigame		= MINIGAME_NONE		// This default means it can run all the time. If its set to anything else, it will check that and exit immediately.
MISSION_CANDIDATE_MISSION_TYPE_ENUM eMinigameMissionType 				// Used to query which other mission types this minigame is allowed to launch over the top of.
STRING								helpButtonPress	= "CC_SUBSTR"		// PRINT_HELP that is displayed when the palyer can launch a minigame.
FLOAT 								fLauncherShutdownDist 	= LAUNCHER_SHUTDOWN_DIST
PED_TRANSPORT_MODE 					eTransMode = TM_ON_FOOT
INT									iFrameCount = 0
STATIC_BLIP_NAME_ENUM				eLauncherStaticBlip	= STATIC_BLIP_NAME_DUMMY_FINAL
LAUNCHER_SLEEP_REASON				eLauncherSleepReason
structTimer							ojTimer

#IF IS_DEBUG_BUILD
	BOOL bDebugLaunched = FALSE
#ENDIF

//2d distance check stuff
FLOAT fFlatSquaredPlayerDistanceToLauncher
VECTOR vFlattenedPlayerLocation, vFlattenedLauncherLocation
								
#IF LAUNCHER_POINT_ENTER					// LAUNCH_SCRIPT_DIST is only used for Point Enter types
	FLOAT 					fLaunchScriptDist 	= LOCATE_SIZE_ON_FOOT_ONLY         
#ENDIF

#IF LAUNCHER_VEHICLE_ENTER
	FLOAT 					fHelpTextScriptDist 	= LOCATE_SIZE_ON_FOOT_ONLY    
#ENDIF

#IF LAUNCHER_POINT_ENTER_IN_VEHICLE
//	VEHICLE_TRIGGER_TYPE triggerVehicleType = LAUNCHER_VEHTYPE_ANY_VEH
	
	#IF NOT LAUNCHER_POINT_ENTER
		CONST_INT LAUNCHER_POINT_ENTER 1
	#ENDIF
#ENDIF

#IF LAUNCHER_OBJECT_ATTACHED
	MODEL_NAMES eAssociatedProp
	COLLISION_ARGS ObjAttachedArgs
//	BOOL bHaveNewData = FALSE
	BOOL bFailedLOSCheck = TRUE //LOS Check buffer bc our new LOS check takes more than a single frame
	BOOL bStreamsCheck
	VECTOR vObjectLOSOffset
	VECTOR vObjectLOSCoords
#ENDIF

#IF LAUNCHER_VOL_BUTTON_PRESS
	VECTOR vVolPt1
	VECTOR vVolPt2
	FLOAT fVolWidth
	FLOAT fVolHeight = 0 //only performs height check if this is greater than 0
	FLOAT fMaxTriggerDist // ignored if left at 0
#ENDIF

#IF LAUNCHER_VEHICLE_SPAWN
	MODEL_NAMES		vehicleModel
	FLOAT			vehicleHeading = 0.0
#ENDIF

#IF LAUNCHER_HAS_SCENE
	BOOL bPlayerCanPlay = TRUE
#ENDIF

#IF LAUNCHER_CHAR_REQUIRED
	enumCharacterList characterRequired = INT_TO_ENUM(enumCharacterList, -1)
#ENDIF

#IF LAUNCHER_CHECK_INTERIOR
	INTERIOR_INSTANCE_INDEX eInterior
#ENDIF

#IF LAUNCHER_REPLAYS_ENABLED
	BOOL bIsReplayLaunch = FALSE
#ENDIF

#IF LAUNCHER_TIME_RESTRICTED
	INT iAvailableAfterHour = 6		// Available after 6:59am
	INT iAvailableBeforeHour = 18	// Avaliable before 7:00pm
#ENDIF

#IF LAUNCHER_MONEY_RESTRICTED
	INT iPriceToLaunch = 0	//make sure the player has enough money before launching
#ENDIF

#IF LAUNCHER_PED_VARIATIONS
	STRUCT MG_PED_VARIATIONS_STRUCT
		INT pedCompTorsoDrw = -1 //-1 for no check!
		INT pedCompTorsoTxt = -1
		INT pedCompLegDrw = -1
		INT pedCompLegTxt = -1
		INT pedCompFeetDrw = -1
		INT pedCompFeetTxt = -1
	ENDSTRUCT
	
	MG_PED_VARIATIONS_STRUCT michaelVariations, franklinVariations, trevorVariations
	STRING outfitHelpLabel	
#ENDIF

/// PURPOSE:
///    A function available to all launchers that forces the launcher into a terminate state, where it waits until told to unload.
///    Also turns off the even'ts blip.
PROC GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE(STATIC_BLIP_NAME_ENUM eStaticBlip)
	IF launcherState < RUN_SCRIPT
		CPRINTLN(DEBUG_MG_LAUNCHER,"Called GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE")
		IF eStaticBlip != STATIC_BLIP_NAME_DUMMY_FINAL
			SET_STATIC_BLIP_ACTIVE_STATE(eStaticBlip, FALSE)
		ENDIF
		RELEASE_CONTEXT_INTENTION(iContextIntention)
		launcherState = WAIT_FOR_TERMINATE
	ENDIF
ENDPROC

/// PURPOSE:
///    A function available to all launchers that forces the launcher into a terminate state, where it waits until told to unload.
///    Also turns off the even'ts blip.
PROC GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(STATIC_BLIP_NAME_ENUM eStaticBlip)
	IF launcherState < RUN_SCRIPT
		CPRINTLN(DEBUG_MG_LAUNCHER,"Called GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP")
		IF eStaticBlip != STATIC_BLIP_NAME_DUMMY_FINAL
			IF ENUM_TO_INT(eStaticBlip) < 0 OR ENUM_TO_INT(eStaticBlip) >= g_iTotalStaticBlips
				CPRINTLN(DEBUG_BLIP, "GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP | Something very wrong | eStaticBlip = ", eStaticBlip)
			ENDIF
			SET_STATIC_BLIP_ACTIVE_STATE(eStaticBlip, FALSE)
		ELSE
			CPRINTLN(DEBUG_BLIP, "GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP | eStaticBlip = STATIC_BLIP_NAME_DUMMY_FINAL")
		ENDIF
		eLauncherStaticBlip = STATIC_BLIP_NAME_DUMMY_FINAL //set the shared static blip to a dummy val
		RELEASE_CONTEXT_INTENTION(iContextIntention)
		launcherState = WAIT_FOR_TERMINATE
	ENDIF
ENDPROC

/// PURPOSE:
///    A function that would reinstate the event's blip.
PROC GENERIC_LAUNCHER_REINSTATE_EVENT_BLIP(STATIC_BLIP_NAME_ENUM eStaticBlip)
	IF eStaticBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_ACTIVE_STATE(eStaticBlip, TRUE)
	ENDIF
ENDPROC


PROC LAUNCHER_GOTO_SLEEP(LAUNCHER_SLEEP_REASON sleepReason)
	IF sleepReason = ASLEEP_WAITING_FOR_MISSION
		CPRINTLN(DEBUG_MG_LAUNCHER, "<",GET_MINIGAME_DISPLAY_STRING_FROM_ID(eMinigame), "> Launcher going to sleep. Waiting on mission.")
		CPRINTLN(DEBUG_MG_LAUNCHER,"Sending MG launcher into sleep state until mission is over.")
	ELIF sleepReason = ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE
		CPRINTLN(DEBUG_MG_LAUNCHER, "<",GET_MINIGAME_DISPLAY_STRING_FROM_ID(eMinigame), "> Launcher going to sleep. Waiting for player to leave the area.")
		CPRINTLN(DEBUG_MG_LAUNCHER,"Sending MG launcher into sleep state until player leaves the area.")
	ENDIF
	eLauncherSleepReason = sleepReason
	iFrameCount = 0
	launcherState = UNLOAD_ASSETS
ENDPROC

FUNC INT GENERIC_LAUNCHER_GET_BLIP_STREAMING_RANGE(STATIC_BLIP_NAME_ENUM paramMinigameBlip)
	SWITCH paramMinigameBlip
		CASE STATIC_BLIP_MINIGAME_SHOOTING_RANGE1
		CASE STATIC_BLIP_MINIGAME_SHOOTING_RANGE2
		CASE STATIC_BLIP_MINIGAME_YOGA
		CASE STATIC_BLIP_MINIGAME_YOGA2
			RETURN MG_BRAIN_ACTIVATION_RANGE_SMALL
		BREAK

		CASE STATIC_BLIP_MINIGAME_DARTS1
        CASE STATIC_BLIP_MINIGAME_DARTS2
		CASE STATIC_BLIP_MINIGAME_PILOT_SCHOOL
		CASE STATIC_BLIP_MINIGAME_TRIATHLON1
		CASE STATIC_BLIP_MINIGAME_TRIATHLON2
		CASE STATIC_BLIP_MINIGAME_TRIATHLON3
		CASE STATIC_BLIP_MINIGAME_TENNIS
		CASE STATIC_BLIP_MINIGAME_TENNIS_MICHAEL_HOUSE
		CASE STATIC_BLIP_MINIGAME_TENNIS_VINEWOOD_HOTEL1
		CASE STATIC_BLIP_MINIGAME_TENNIS_RICHMAN_HOTEL1
		CASE STATIC_BLIP_MINIGAME_TENNIS_LSU_COURT1
		CASE STATIC_BLIP_MINIGAME_TENNIS_VESPUCCI_HOTEL
		CASE STATIC_BLIP_MINIGAME_TENNIS_WEAZEL_COURT
		CASE STATIC_BLIP_MINIGAME_TENNIS_CHUMASH_HOTEL
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_MAZE_BANK
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_CRANE
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_RIVER_CLIFF
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_GOLF_COURSE
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_ROCK_CLIFF
			RETURN MG_BRAIN_ACTIVATION_RANGE_MEDIUM
		BREAK
		
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_HARBOR
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_RACE_TRACK
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_WINDMILLS
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_NORTH_CLIFF
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_RUNAWAY_TRAIN
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_1K
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_1_5K
		CASE STATIC_BLIP_AMBIENT_BASEJUMP_CANAL
		CASE STATIC_BLIP_MINIGAME_GOLF
		CASE STATIC_BLIP_MINIGAME_HUNTING1
		CASE STATIC_BLIP_MINIGAME_STUNT_PLANES
		CASE STATIC_BLIP_MINIGAME_OFFROAD_RACE5
		CASE STATIC_BLIP_MINIGAME_OFFROAD_RACE8
		CASE STATIC_BLIP_MINIGAME_OFFROAD_RACE9
		CASE STATIC_BLIP_MINIGAME_OFFROAD_RACE10
		CASE STATIC_BLIP_MINIGAME_OFFROAD_RACE11
		CASE STATIC_BLIP_MINIGAME_OFFROAD_RACE12
			RETURN MG_BRAIN_ACTIVATION_RANGE_LARGE
		BREAK
	ENDSWITCH
	
	RETURN MG_BRAIN_ACTIVATION_RANGE_NONE
ENDFUNC


