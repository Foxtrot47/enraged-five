	// launcher_Basejumping.sc


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


	
CONST_INT LAUNCHER_SNAP_CHECKPOINT_TO_GROUND 	0
CONST_INT LAUNCHER_DONT_CHECK_RELOAD_RANGE 		0
CONST_INT LAUNCHER_CUSTOM_SCRIPT_LAUNCH 		1
CONST_INT LAUNCHER_CLEAR_SCENE_ON_START			1
CONST_INT LAUNCHER_HAS_SCENE					1
CONST_INT LAUNCHER_DONT_FADE_DOWN               1
CONST_INT LAUNCHER_NO_BUTTONPRESS               1
CONST_INT LAUNCHER_VEHICLE_ENTER                1
CONST_INT LAUNCHER_VEHICLE_ENTER_ANY_SEAT       1
CONST_INT LAUNCHER_VEHICLE_SPAWN                0
CONST_INT LAUNCHER_VEHICLE_IGNORE_PROXIMITY     1
CONST_INT LAUNCHER_DISABLE_FOR_MAGDEMO			1
CONST_INT LAUNCHER_IGNORE_PLAYER_CONTROL		1
CONST_INT LAUNCHER_HAS_TRIGGER_SCENE			1
CONST_INT LAUNCHER_CHECK_SPAWN_DIST				1

CONST_INT BJ_LAUNCHER_FORCE_SPAWN_TIME_MS_OFFSET 15000 //15 seconds to force spawn a heli after we debug warp

USING "generic_launcher_header.sch"
USING "../../scripts/Ambient/BaseJumping/bj_data.sch"

STREAMED_MODEL				VehicleModels[1]
//STREAMED_MODEL				PedModels[1]
BJ_JUMP_ID 					eJumpID
structPedsForConversation	conversationPedStruct
BJ_LAUNCHER_ARGS 			bjLauncherArgs
BOOL						bAllowScene
BOOL						bHasTriggeredScene = FALSE
BOOL                        bHelpOnScreen
BOOL						bPilotRequested
//VEHICLE_INDEX               vehicleLaunch //this is being declared in the gen launcher header.
CAMERA_INDEX				bjLaunchCam
structTimer					bjWaitForPlayer //timer to wait for player to enter the heli

#IF IS_DEBUG_BUILD
	BOOL						bLocalDebugLaunch
#ENDIF


// Setup our script.
PROC LAUNCHER_CUSTOM_SCRIPT_INIT()	
	eMinigame = MINIGAME_BASEJUMPING

	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FORCED_ON_INIT)
		EXIT
	ENDIF
	

	scriptName = "bj"
	
	eJumpID = BJ_GET_NEAREST_LAUNCHER_LOCATION(vLaunchLocation, TRUE)
	CPRINTLN(DEBUG_MG_LAUNCHER,"[launcher_BasejumpHeli.sc] >>>>>>>>> (LAUNCHER_CUSTOM_SCRIPT_INIT) eJumpID = ", eJumpID)
	
	SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SCRIPT_NOT_ALLOWING_RUN)
	
	SWITCH eJumpID															   
		CASE BJJUMPID_HARBOR
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_HARBOR
		BREAK
		CASE BJJUMPID_RACE_TRACK
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_RACE_TRACK
		BREAK
		CASE BJJUMPID_WINDMILLS
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_WINDMILLS
		BREAK
		CASE BJJUMPID_NORTH_CLIFF
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_NORTH_CLIFF
		BREAK
		CASE BJJUMPID_RUNAWAY_TRAIN
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_RUNAWAY_TRAIN
		BREAK
		CASE BJJUMPID_1K
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_1K
		BREAK
		CASE BJJUMPID_1_5K
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_1_5K
		BREAK
		CASE BJJUMPID_CANAL
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_CANAL
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Wrong type of jump for launcher_BasejumpHeli!")
		BREAK
	ENDSWITCH
	
	// Maze Bank shouldn't run while Extreme 3 RCM is available.
	IF eJumpID = BJJUMPID_MAZE_BANK
		IF IS_RC_MISSION_AVAILABLE(RC_EXTREME_3)
			SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FORCED_ON_INIT)
			EXIT
		ENDIF
	ENDIF
	
	// Runaway Train shouldn't run between Exile 3 and Trevor 4.
	IF eJumpID = BJJUMPID_RUNAWAY_TRAIN
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_3)
		AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_4)
			SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FORCED_ON_INIT)
			EXIT
		ENDIF
	ENDIF
	
	IF eJumpID = BJJUMPID_HARBOR
		//we have a large memory footprint, so shutdown for mission leadins
		SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FOR_MISSION_LEADIN)
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FORCED_ON_INIT)
		EXIT
	ENDIF
	
	SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_ALLOW_MULTIPLE_COPIES)
	SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_NEED_SETUP_ON_MISSION)

	fLauncherShutdownDist = TO_FLOAT(GENERIC_LAUNCHER_GET_BLIP_STREAMING_RANGE(eLauncherStaticBlip)) + 5.0
	
	IF eLauncherStaticBlip <> STATIC_BLIP_NAME_DUMMY_FINAL
		//check if we're active in flow
		IF NOT GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(eMinigame))
//			OR NOT IS_STATIC_BLIP_CURRENTLY_VISIBLE(eLauncherStaticBlip) //blip controller can get delayed
			CPRINTLN(DEBUG_MG_LAUNCHER,"Mission flow flag not set, or blip not active. not allowing trigger scene.")
			bAllowScene = FALSE			
			GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
		ELSE
			CPRINTLN(DEBUG_MG_LAUNCHER,"Allowing trigger scene.")
			bAllowScene = TRUE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MG_LAUNCHER,"We have an invalid static blip for this jump. Not allowing scene.")
		bAllowScene = FALSE
	
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("bj")) > 0
		
	ELSE
		CLEAR_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_RUN_DESPITE_MISSION)
	ENDIF
	
	//TEMP HACK PLEASE REMOVE
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_EXILE)
		SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_RUN_DESPITE_MISSION)	
	ENDIF
	//TEMP HACK PLEASE REMOVE
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MG_LAUNCHER,"Checking if our debug launch timestamp.")
		IF GET_GAME_TIMER() <= g_bjDebugLaunchTimeStamp + BJ_LAUNCHER_FORCE_SPAWN_TIME_MS_OFFSET
			CPRINTLN(DEBUG_MG_LAUNCHER,GET_GAME_TIMER(), " <= ", g_bjDebugLaunchTimeStamp, " + ", BJ_LAUNCHER_FORCE_SPAWN_TIME_MS_OFFSET)
			bLocalDebugLaunch = TRUE //store off whether we debug launched for this specific script so we dont interfere with any other bj launchers
			bDebugLaunched = TRUE
		ELSE
			CPRINTLN(DEBUG_MG_LAUNCHER,GET_GAME_TIMER(), " > ", g_bjDebugLaunchTimeStamp, " + ", BJ_LAUNCHER_FORCE_SPAWN_TIME_MS_OFFSET)
		ENDIF
		
	#ENDIF
	
	iStackSize = MISSION_STACK_SIZE
ENDPROC


PROC LAUNCHER_CUSTOM_UNLOAD_ASSETS()
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(VehicleModels)
//	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(PedModels)
	SET_VEHICLE_AS_NO_LONGER_NEEDED(bjLauncherArgs.myHeli)
	SET_PED_AS_NO_LONGER_NEEDED(bjLauncherArgs.myPilot)
ENDPROC

PROC BJ_LAUNCHER_LOAD_HELI_SCENE()
	IF eJumpID = BJJUMPID_NORTH_CLIFF
		ADD_STREAMED_MODEL(VehicleModels, FROGGER)
	ELSE
		ADD_STREAMED_MODEL(VehicleModels, MAVERICK)
	ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_REQUEST_ASSETS()

	//add ped models:
	//add vehicle models:
	SWITCH eJumpID															   //new locations:
		CASE BJJUMPID_HARBOR
		CASE BJJUMPID_RACE_TRACK
		CASE BJJUMPID_WINDMILLS
		CASE BJJUMPID_NORTH_CLIFF
		CASE BJJUMPID_RIVER_CLIFF
		CASE BJJUMPID_RUNAWAY_TRAIN
		CASE BJJUMPID_1K
		CASE BJJUMPID_1_5K
		CASE BJJUMPID_CANAL
			BJ_LAUNCHER_LOAD_HELI_SCENE()
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Wrong type of jump for launcher_BasejumpHeli!")
		BREAK
	ENDSWITCH
	
	REQUEST_ALL_MODELS(VehicleModels)
	//REQUEST_ALL_MODELS(PedModels)
ENDPROC

FUNC BOOL LAUNCHER_CUSTOM_ASSETS_LOADED()
	RETURN ARE_MODELS_STREAMED(VehicleModels)// AND ARE_MODELS_STREAMED(PedModels)
ENDFUNC

PROC LAUNCHER_CUSTOM_RELEASE_ASSETS()
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(VehicleModels)
	SET_VEHICLE_AS_NO_LONGER_NEEDED(bjLauncherArgs.myHeli)
	SET_PED_AS_NO_LONGER_NEEDED(bjLauncherArgs.myPilot)
	
	//SET_ALL_MODELS_AS_NO_LONGER_NEEDED(PedModels)
ENDPROC

PROC BJ_LAUNCHER_SPAWN_HELI_SCENE(VECTOR theseCoords, FLOAT thisHeading)
	FLOAT fGroundZ
	BOOL bSpawnHeliScene
	fPlayerDistToLauncher = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), theseCoords)
	//Only spawn the scene if the player is at least 15m away and the area isnt on screen
	
	//make sure we're at least 5m away before spawning anything to avoid spawning on top of the player
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND (fPlayerDistToLauncher > 5*5 OR IS_SCREEN_FADED_OUT())
		IF fPlayerDistToLauncher < 160*160
			//if we're within 160m, do an on-screen check
			IF NOT IS_SPHERE_VISIBLE(theseCoords, 6.0) OR IS_SCREEN_FADED_OUT()
				bSpawnHeliScene = TRUE
			ENDIF
		ELSE
			//otherwise, just make sure it spawns in.
			bSpawnHeliScene = TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD 
		IF bLocalDebugLaunch 
			CPRINTLN(DEBUG_MG_LAUNCHER,"We launched from the debug menu, so our bool is set to always spawn the heli")
		ELSE
			CPRINTLN(DEBUG_MG_LAUNCHER,"We didn't launch from the debug menu, so we're doing a dist check and a visibility check")
		ENDIF	
	#ENDIF
	
	IF bSpawnHeliScene #IF IS_DEBUG_BUILD OR bLocalDebugLaunch #ENDIF
		IF DOES_ENTITY_EXIST(bjLauncherArgs.myHeli)
			SCRIPT_ASSERT("launch vehicle already exists! deleting!")
			DELETE_VEHICLE(bjLauncherArgs.myHeli)
		ENDIF
		
		CLEAR_AREA(theseCoords, 4.0, TRUE, FALSE)
		
		IF eJumpID = BJJUMPID_HARBOR
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-835.7629, -1298.7775, 0.0000>>, <<-815.1855, -1283.9060, 10.0000>>, FALSE)
		ENDIF
		
		IF eJumpID = BJJUMPID_NORTH_CLIFF
			bjLauncherArgs.myHeli = CREATE_VEHICLE(FROGGER, theseCoords, thisHeading)
			SET_VEHICLE_COLOUR_COMBINATION(bjLauncherArgs.myHeli, 5) // Blue
		ELSE
			bjLauncherArgs.myHeli = CREATE_VEHICLE(MAVERICK, theseCoords, thisHeading)
			SET_VEHICLE_COLOUR_COMBINATION(bjLauncherArgs.myHeli, 6) // Green
		ENDIF
		vehicleLaunch = bjLauncherArgs.myHeli //we need this so the launcher knows we've spawned a veh
		
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehicleLaunch, TRUE)
		
		IF GET_GROUND_Z_FOR_3D_COORD(theseCoords, fGroundZ)
			SET_VEHICLE_ON_GROUND_PROPERLY(bjLauncherArgs.myHeli)
		ENDIF
		
		//SET_VEHICLE_DOORS_LOCKED(bjLauncherArgs.myHeli, VEHICLELOCK_LOCKED)
		SET_VEHICLE_HAS_STRONG_AXLES(bjLauncherArgs.myHeli, TRUE)
		//FREEZE_ENTITY_POSITION(bjLauncherArgs.myHeli, TRUE)
		
		bHasTriggeredScene = TRUE
	ELSE
		CPRINTLN(DEBUG_MG_LAUNCHER,"Not spawning heli because we are either too close or the coordinate is on screen!")
		CPRINTLN(DEBUG_MG_LAUNCHER,"bSpawnHeliScene is ", bSpawnHeliScene)
		bHasTriggeredScene = FALSE
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MG_LAUNCHER,"bLocalDebugLaunch is ", bLocalDebugLaunch)
		#ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Creates the other guys at the shooting range.
PROC LAUNCHER_CUSTOM_SPAWN_SCENE()	
	
	IF bAllowScene
		SWITCH eJumpID															   //new locations:
			CASE BJJUMPID_HARBOR
				BJ_LAUNCHER_SPAWN_HELI_SCENE(<<-829.3729, -1289.8170, 4.0005>>, 196.9543)
			BREAK
			CASE BJJUMPID_RACE_TRACK
				BJ_LAUNCHER_SPAWN_HELI_SCENE(<<1208.1965, 174.5777, 81.0003>>, 138.8786)
			BREAK
			CASE BJJUMPID_WINDMILLS
				BJ_LAUNCHER_SPAWN_HELI_SCENE(<<2463.9036, 1509.9642, 35.0372>>, 88.9199)
			BREAK
			CASE BJJUMPID_NORTH_CLIFF
				BJ_LAUNCHER_SPAWN_HELI_SCENE(<<-274.6559, 6633.8911, 7.5426>>, 130.4063)
			BREAK
			CASE BJJUMPID_RUNAWAY_TRAIN
				BJ_LAUNCHER_SPAWN_HELI_SCENE(<<-742.5047, 4493.4663, 75.2206>>, 180.9910)
			BREAK
			CASE BJJUMPID_1K
				BJ_LAUNCHER_SPAWN_HELI_SCENE(<<-1367.6865, 4381.9980, 41.4017>>, 277.8339)
			BREAK
			CASE BJJUMPID_1_5K
				BJ_LAUNCHER_SPAWN_HELI_SCENE(<<2517.9619, 4971.6191, 44.7057>>, 15.3895)
			BREAK
			CASE BJJUMPID_CANAL
				BJ_LAUNCHER_SPAWN_HELI_SCENE(<<1054.5343, -179.6562, 70.3162>>, 30.0460)
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("Wrong type of jump for launcher_BasejumpHeli!")
			BREAK
		ENDSWITCH
	ENDIF
	
	//if we debug launched to a jump, make sure we clear the bool (local bool)
	#IF IS_DEBUG_BUILD
		bLocalDebugLaunch = FALSE
	#ENDIF
ENDPROC

FUNC BOOL BJ_LAUNCHER_IS_PILOT_IN_PILOT_SEAT(PED_INDEX thisPed, VEHICLE_INDEX thisVeh)

	//alive checks
	IF NOT DOES_ENTITY_EXIST(thisPed)
		OR NOT DOES_ENTITY_EXIST(thisVeh)
		OR IS_ENTITY_DEAD(thisPed)
		OR IS_ENTITY_DEAD(thisVeh)
		RETURN FALSE
	ENDIF

	IF IS_PED_IN_VEHICLE(thisPed, thisVeh)
	AND GET_PED_VEHICLE_SEAT(thisPed, thisVeh) = VS_DRIVER
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_MG_LAUNCHER,"Pilot is not in the driver's seat!")
	RETURN FALSE
ENDFUNC

FUNC BOOL BJ_LAUNCHER_TEST_FOR_PANIC(PED_INDEX testPed, VEHICLE_INDEX testVeh, BOOL bInjuredCheck = TRUE, BOOL bCombatCheck = TRUE, BOOL bTargettingCheck = FALSE, BOOL bIncludeWantedLevel = FALSE, BOOL bIncludeTouch = FALSE)
	PED_INDEX Player = PLAYER_PED_ID()
	VECTOR tempVec
	
	IF (NOT DOES_ENTITY_EXIST(testVeh))
	OR IS_ENTITY_DEAD(testVeh)
	OR (NOT IS_VEHICLE_DRIVEABLE(testVeh))
	OR IS_ENTITY_ON_FIRE(testVeh)
		CPRINTLN(DEBUG_MG_LAUNCHER,"The launcher vehicle isn't fit for duty!")
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(testPed)
		IF NOT IS_PED_INJURED(testPed) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			tempVec = GET_ENTITY_COORDS(testPed)
			
			//player is in combat with the ped
			IF bCombatCheck
				IF IS_PED_IN_COMBAT(testPed, Player)
					CPRINTLN(DEBUG_MG_LAUNCHER,"Ped panicking because they are in combat with the player")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF bIncludeWantedLevel
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					CPRINTLN(DEBUG_MG_LAUNCHER,"Players got a wanted level!")
					RETURN TRUE
					
				ENDIF
			ENDIF	
			
			//player is touching the target
			IF bIncludeTouch
				IF IS_ENTITY_TOUCHING_ENTITY(Player, testPed)
					CPRINTLN(DEBUG_MG_LAUNCHER,"Ped panicking because player is touching them")
					RETURN TRUE
				ENDIF
			ENDIF
			
			//player is targetting the enemy within 10 m
			IF bTargettingCheck
				IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), testPed)
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), testPed)
					IF IS_PED_FACING_PED(testPed, Player, 90)
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(testPed) < 10
							CPRINTLN(DEBUG_MG_LAUNCHER,"Ped panicking because player is targetting ped!")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			//player has damaged the target's vehicle (if they are in one)
			IF IS_PED_IN_ANY_VEHICLE(testPed)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_IN(testPed), Player)
					CPRINTLN(DEBUG_MG_LAUNCHER,"Ped panicking because player damaged the ped's vehicle!")
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Player is shooting near the target
			IF IS_PED_ARMED(Player, ENUM_TO_INT(WF_INCLUDE_GUN))
				IF IS_PED_SHOOTING(Player)
					IF IS_ENTITY_AT_COORD(testPed, GET_ENTITY_COORDS(Player), <<45, 45, 45>>)
						CPRINTLN(DEBUG_MG_LAUNCHER,"Ped panicking because player is shooting!")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BULLET_IN_AREA(tempVec, 4.0, TRUE)
				CPRINTLN(DEBUG_MG_LAUNCHER,"Ped panicking because bullet in area!")
				RETURN TRUE
			ENDIF
			
			//check to see if player is threatening ped with a thrown weapon
			IF IS_PROJECTILE_IN_AREA(<<tempVec.x-5, tempVec.y-5, tempVec.z-5>>, <<tempVec.x+5, tempVec.y+5, tempVec.z+5>>)
				CPRINTLN(DEBUG_MG_LAUNCHER,"Ped panicking because projectile in area!")
				RETURN TRUE
			ENDIF
			
			IF IS_PED_BEING_JACKED(testPed)
			OR NOT BJ_LAUNCHER_IS_PILOT_IN_PILOT_SEAT(testPed, testVeh)
				CPRINTLN(DEBUG_MG_LAUNCHER,"Ped panicking because player is jacking their vehicle!")
				RETURN TRUE
			ENDIF
			
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, tempVec, 25)
			AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_STEAM, tempVec, 25)
			AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_WATER_HYDRANT, tempVec, 25)
				CPRINTLN(DEBUG_MG_LAUNCHER,"Ped panicking becuase there is an explosion near them")
				RETURN TRUE
			ENDIF
		ELSE
			IF bInjuredCheck
				CPRINTLN(DEBUG_MG_LAUNCHER,"Ped is injured in panicking check!")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC BJ_LAUNCHER_UPDATE_TRACKING_CAMERA(CAMERA_INDEX& camLaunch, VEHICLE_INDEX vehFocus)
	VECTOR vLookRot, vLookCoord, vCamCoord
	FLOAT fNewPitch
	
	IF DOES_CAM_EXIST(camLaunch) AND IS_CAM_ACTIVE(camLaunch)
		vCamCoord = GET_CAM_COORD(camLaunch)
		vLookRot = GET_CAM_ROT(camLaunch)
		IF IS_VEHICLE_DRIVEABLE(vehFocus)
			vLookCoord = GET_ENTITY_COORDS(vehFocus)
		ENDIF
		
		fNewPitch = ATAN2(vLookCoord.z - vCamCoord.z, GET_DISTANCE_BETWEEN_COORDS(vLookCoord, vCamCoord, FALSE))
		IF fNewPitch > vLookRot.x
			vLookRot.x = LERP_FLOAT(vLookRot.x, fNewPitch, 0.07)
		ENDIF
		SET_CAM_ROT(camLaunch, vLookRot)
	ENDIF
ENDPROC

PROC BJ_LAUNCHER_UPDATE_HELI_SCENE()
	SCRIPTTASKSTATUS taskStatus
	structTimer tmrTracking
	VEHICLE_INDEX vehPlayer
	PED_INDEX pedDummy
	SEQUENCE_INDEX seqFly
	VECTOR vPlayerPos
	FLOAT fDist
	FLOAT fBladeSpeed
	BOOL bPlayerInHeli
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	fBladeSpeed = PICK_FLOAT(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID) = MAVERICK, 0.45, 0.3)
	
	// Update the player's vehicle.
	vehPlayer = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(vehPlayer) AND vehPlayer <> vehicleLaunch
		bjLauncherArgs.lastPlrVehicle = vehPlayer
	ENDIF
	
	IF NOT bHasTriggeredScene //if we havent triggered our scene yet
		// Attempt to launch spawn scene.
// 		CPRINTLN(DEBUG_MG_LAUNCHER,"Attempting to spawn heli scene")
		LAUNCHER_CUSTOM_SPAWN_SCENE()
		WAIT(0)
		EXIT
	ENDIF
	
	// Check for early exit due to player fucking around.
	IF BJ_LAUNCHER_TEST_FOR_PANIC(bjLauncherArgs.myPilot, bjLauncherArgs.myHeli, TRUE, TRUE, TRUE, TRUE, FALSE)
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
//		GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
		LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE) //unload assets, turn off the blip, and wait for the player to leave the area before turning the blip on again.
		bHasTriggeredScene = FALSE
		IF DOES_ENTITY_EXIST(bjLauncherArgs.myPilot) AND NOT IS_PED_INJURED(bjLauncherArgs.myPilot)
			IF IS_VEHICLE_DRIVEABLE(bjLauncherArgs.myHeli) AND IS_PED_IN_VEHICLE(bjLauncherArgs.myPilot, bjLauncherArgs.myHeli)
				TASK_VEHICLE_MISSION_COORS_TARGET(bjLauncherArgs.myPilot, bjLauncherArgs.myHeli, GET_ENTITY_COORDS(bjLauncherArgs.myHeli) + 100.0 * GET_ENTITY_FORWARD_VECTOR(bjLauncherArgs.myHeli) + <<0, 0, 500>>, MISSION_GOTO, GET_VEHICLE_ESTIMATED_MAX_SPEED(bjLauncherArgs.myHeli), DRIVINGMODE_PLOUGHTHROUGH, 50, 50)
			ELSE
				TASK_SMART_FLEE_PED(bjLauncherArgs.myPilot, PLAYER_PED_ID(), 1000, -1)
			ENDIF
			SET_PED_KEEP_TASK(bjLauncherArgs.myPilot, TRUE)
			SET_PED_CONFIG_FLAG(bjLauncherArgs.myPilot, PCF_AICanDrivePlayerAsRearPassenger, FALSE)
			SET_PED_CONFIG_FLAG(bjLauncherArgs.myPilot, PCF_AIDriverAllowFriendlyPassengerSeatEntry, FALSE)
			bjLauncherArgs.myPilot = NULL
		ENDIF
		
		IF DOES_ENTITY_EXIST(bjLauncherArgs.myHeli)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(bjLauncherArgs.myHeli)
			bjLauncherArgs.myHeli = NULL
		ENDIF
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PLAY_BASEJUMP_S")
			CLEAR_HELP()
		ENDIF
		EXIT
	ENDIF
	
	fDist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), BJ_GET_LAUNCHER_LOCATION_BY_ID(ENUM_TO_INT(eJumpID)))
	
	// Update loading/unloading of the pilot model.
	IF fDist < 11664.0 // 108^2
		IF NOT bPilotRequested
			REQUEST_MODEL(BJ_GET_LAUNCH_PILOT_BY_ID(eJumpID))
			bPilotRequested = TRUE
		ENDIF
	ELIF fDist > 13924.0 // 118^2
		IF bPilotRequested
			SET_MODEL_AS_NO_LONGER_NEEDED(BJ_GET_LAUNCH_PILOT_BY_ID(eJumpID))
			bPilotRequested = FALSE
		ENDIF
	ENDIF
	
	// Update spawning/deleting the pilot.
	IF fDist < 10000.0 // 100^2
		IF (NOT DOES_ENTITY_EXIST(bjLauncherArgs.myPilot))
			IF HAS_MODEL_LOADED(BJ_GET_LAUNCH_PILOT_BY_ID(eJumpID)) AND DOES_ENTITY_EXIST(bjLauncherArgs.myHeli) AND IS_VEHICLE_DRIVEABLE(bjLauncherArgs.myHeli)
				bjLauncherArgs.myPilot = CREATE_PED_INSIDE_VEHICLE(bjLauncherArgs.myHeli, PEDTYPE_CIVMALE, BJ_GET_LAUNCH_PILOT_BY_ID(eJumpID))
					
				IF NOT IS_PED_INJURED(bjLauncherArgs.myPilot)
					SET_PED_HELMET(bjLauncherArgs.myPilot, FALSE)
					BJ_SET_PILOT_COMPONENTS_BY_ID(bjLauncherArgs.myPilot, eJumpID)
					
					//FOR SHOWING UP IN A TAXI:						
					INCREMENT_MINIGAMES_LOADED_COUNT()
					
					SET_AMBIENT_VOICE_NAME(bjLauncherArgs.myPilot, BJ_GET_LAUNCH_PILOT_VOICE_FOR_AMB_SPEECH_BY_ID(eJumpID))
					
					ADD_PED_FOR_DIALOGUE(conversationPedStruct, 0, bjLauncherArgs.myPilot, BJ_GET_LAUNCH_PILOT_VOICE_FOR_DIALOGUE_BY_ID(eJumpID), TRUE)
					
					TASK_STAND_STILL(bjLauncherArgs.myPilot, -1)
					SET_PED_CONFIG_FLAG(bjLauncherArgs.myPilot, PCF_AICanDrivePlayerAsRearPassenger, TRUE)
					SET_PED_CONFIG_FLAG(bjLauncherArgs.myPilot, PCF_AIDriverAllowFriendlyPassengerSeatEntry, TRUE)
				ENDIF
				
				SET_VEHICLE_ENGINE_ON(bjLauncherArgs.myHeli, TRUE, TRUE)
				SET_HELI_BLADES_SPEED(bjLauncherArgs.myHeli, fBladeSpeed)
			ENDIF
		
		ELSE
			SET_ENTITY_AS_MISSION_ENTITY(bjLauncherArgs.myPilot)
			IF (NOT DOES_CAM_EXIST(bjLaunchCam)) OR NOT IS_CAM_RENDERING(bjLaunchCam)
				SET_HELI_BLADES_SPEED(bjLauncherArgs.myHeli, fBladeSpeed)
			ENDIF
			
			// Dialogue from the pilot.
			IF NOT IS_BIT_SET(bjLauncherArgs.iFlags, 0)
				IF IS_SCREEN_FADED_IN()
					IF NOT IS_PED_INJURED(bjLauncherArgs.myPilot) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(bjLauncherArgs.myPilot)) < 400.0
							CREATE_CONVERSATION(conversationPedSTruct, "OJBJAUD", BJ_GET_GREET_DLG_BY_ID(eJumpID), CONV_PRIORITY_HIGH)
							TASK_LOOK_AT_ENTITY(bjLauncherArgs.myPilot, PLAYER_PED_ID(), 6000, SLF_DEFAULT)
							SET_BIT(bjLauncherArgs.iFlags, 0)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ELIF fDist > 12100.0 // 110^2
		IF DOES_ENTITY_EXIST(bjLauncherArgs.myPilot)
			pedDummy = bjLauncherArgs.myPilot
			SET_PED_KEEP_TASK(pedDummy, TRUE)
			IF NOT IS_ENTITY_ON_SCREEN(pedDummy)
				SET_ENTITY_AS_MISSION_ENTITY(pedDummy)
				DELETE_PED(pedDummy)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(pedDummy)
			ENDIF
		ENDIF
	ENDIF
	
	BJ_LAUNCHER_UPDATE_TRACKING_CAMERA(bjLaunchCam, bjLauncherArgs.myHeli)
	
	// The chopper should take off when the player gets in.
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), bjLauncherArgs.myHeli, TRUE)
	AND (NOT IS_PED_JACKING(PLAYER_PED_ID()))
	AND BJ_LAUNCHER_IS_PILOT_IN_PILOT_SEAT(bjLauncherArgs.myPilot, bjLauncherArgs.myHeli)
	//AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) <> VS_FRONT_RIGHT AND GET_SEAT_PED_IS_TRYING_TO_ENTER(PLAYER_PED_ID()) <> ENUM_TO_INT(VS_FRONT_RIGHT)
		vPlayerPos = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(bjLauncherArgs.myHeli, GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_ROOT, <<0,0,0>>))
		IF vPlayerPos.x < 0.0 OR vPlayerPos.y < 1.0
			SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
			SET_VEH_RADIO_STATION(bjLauncherArgs.myHeli, "OFF")
			
			CLEAR_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SCRIPT_NOT_ALLOWING_RUN)
			
			bPlayerInHeli = TRUE
			
			IF bHelpOnScreen
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PLAY_BASEJUMP_B")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PLAY_BASEJUMP_S")
					CLEAR_HELP(TRUE)
				ENDIF
				bHelpOnScreen = FALSE
			ENDIF
			
			IF NOT DOES_CAM_EXIST(bjLaunchCam)// AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), bjLauncherArgs.myHeli, <<2, 2, 0>>)
				ODDJOB_ENTER_CUTSCENE()
				DISABLE_CELLPHONE(TRUE)
				bjLaunchCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, BJ_GET_LAUNCH_HELI_SCENE_CAM_POS(eJumpID), BJ_GET_LAUNCH_HELI_SCENE_CAM_ROT(eJumpID), BJ_GET_LAUNCH_HELI_SCENE_CAM_FOV(eJumpID))
				SET_CAM_ACTIVE(bjLaunchCam, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SHAKE_CAM(bjLaunchCam, "HAND_SHAKE", 0.07)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(bjLauncherArgs.myHeli)
				taskStatus = GET_SCRIPT_TASK_STATUS(bjLauncherArgs.myPilot, SCRIPT_TASK_VEHICLE_MISSION)
				IF taskStatus <> WAITING_TO_START_TASK AND taskStatus <> PERFORMING_TASK
					SET_PED_CONFIG_FLAG(bjLauncherArgs.myPilot, PCF_PreventPedFromReactingToBeingJacked, TRUE)
					
					RESTART_TIMER_NOW(tmrTracking)
					WHILE GET_TIMER_IN_SECONDS(tmrTracking) < 1.0
						WAIT(0)
						IF IS_PED_INJURED(PLAYER_PED_ID()) OR IS_PED_JACKING(PLAYER_PED_ID())
							GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
							IF DOES_ENTITY_EXIST(bjLauncherArgs.myPilot) AND NOT IS_PED_INJURED(bjLauncherArgs.myPilot)
								IF IS_VEHICLE_DRIVEABLE(bjLauncherArgs.myHeli) AND IS_PED_IN_VEHICLE(bjLauncherArgs.myPilot, bjLauncherArgs.myHeli)
									TASK_VEHICLE_MISSION_COORS_TARGET(bjLauncherArgs.myPilot, bjLauncherArgs.myHeli, GET_ENTITY_COORDS(bjLauncherArgs.myHeli) + 100.0 * GET_ENTITY_FORWARD_VECTOR(bjLauncherArgs.myHeli) + <<0, 0, 500>>, MISSION_GOTO, GET_VEHICLE_ESTIMATED_MAX_SPEED(bjLauncherArgs.myHeli), DRIVINGMODE_PLOUGHTHROUGH, 50, 50)
								ELSE
									TASK_SMART_FLEE_PED(bjLauncherArgs.myPilot, PLAYER_PED_ID(), 1000, -1)
								ENDIF
								SET_PED_KEEP_TASK(bjLauncherArgs.myPilot, TRUE)
								SET_PED_CONFIG_FLAG(bjLauncherArgs.myPilot, PCF_AICanDrivePlayerAsRearPassenger, FALSE)
								SET_PED_CONFIG_FLAG(bjLauncherArgs.myPilot, PCF_AIDriverAllowFriendlyPassengerSeatEntry, FALSE)
							ENDIF
							
							IF DOES_ENTITY_EXIST(bjLauncherArgs.myHeli)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(bjLauncherArgs.myHeli)
							ENDIF
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PLAY_BASEJUMP_S")
								CLEAR_HELP()
							ENDIF
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							EXIT
						ELSE
							BJ_LAUNCHER_UPDATE_TRACKING_CAMERA(bjLaunchCam, bjLauncherArgs.myHeli)
						ENDIF
					ENDWHILE
					
					
					
					START_TIMER_NOW_SAFE(bjWaitForPlayer)
					WHILE NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), bjLauncherArgs.myHeli) AND GET_TIMER_IN_SECONDS_SAFE(bjWaitForPlayer) < 2.0
						WAIT(0)
					ENDWHILE
					CANCEL_TIMER(bjWaitForPlayer)
					
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), bjLauncherArgs.myHeli)
						bPlayerInHeli = FALSE
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						IF DOES_CAM_EXIST(bjLaunchCam)
							DESTROY_CAM(bjLaunchCam)
						ENDIF
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						EXIT
					ENDIF
					
					IF (NOT IS_ENTITY_DEAD(bjLauncherArgs.myPilot)) AND (NOT IS_ENTITY_DEAD(bjLauncherArgs.myHeli))
						OPEN_SEQUENCE_TASK(seqFly)
							TASK_STAND_STILL(NULL, 500)
							TASK_VEHICLE_MISSION_COORS_TARGET(NULL, bjLauncherArgs.myHeli, GET_ENTITY_COORDS(bjLauncherArgs.myHeli) + 100.0 * GET_ENTITY_FORWARD_VECTOR(bjLauncherArgs.myHeli) + <<0, 0, 500>>, MISSION_GOTO, GET_VEHICLE_ESTIMATED_MAX_SPEED(bjLauncherArgs.myHeli), DRIVINGMODE_PLOUGHTHROUGH, 50, 50)
						CLOSE_SEQUENCE_TASK(seqFly)
						TASK_PERFORM_SEQUENCE(bjLauncherArgs.myPilot, seqFly)
						CLEAR_SEQUENCE_TASK(seqFly)
						
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							SET_PED_HELMET(PLAYER_PED_ID(), FALSE)
							REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
						ENDIF
					ENDIF
					
					RESTART_TIMER_NOW(tmrTracking)
					WHILE GET_TIMER_IN_SECONDS(tmrTracking) < 2.0
						WAIT(0)
						BJ_LAUNCHER_UPDATE_TRACKING_CAMERA(bjLaunchCam, bjLauncherArgs.myHeli)
					ENDWHILE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bPlayerInHeli
		// Update help text based on player's distance.
		IF IS_VEHICLE_DRIVEABLE(bjLauncherArgs.myHeli)
		AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(bjLauncherArgs.myHeli)) < 225.0
		AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			IF (NOT bHelpOnScreen)
			OR (eJumpID = BJJUMPID_NORTH_CLIFF AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PLAY_BASEJUMP_B"))
			OR (eJumpID <> BJJUMPID_NORTH_CLIFF AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PLAY_BASEJUMP_S"))
				IF eJumpID = BJJUMPID_NORTH_CLIFF
					IF !g_bBrowserVisible
						PRINT_HELP_FOREVER("PLAY_BASEJUMP_B")
					ENDIF
				ELSE
					IF !g_bBrowserVisible
						PRINT_HELP_FOREVER("PLAY_BASEJUMP_S")
					ENDIF
				ENDIF
				bHelpOnScreen = TRUE
			ENDIF
			
		ELIF bHelpOnScreen
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PLAY_BASEJUMP_B")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PLAY_BASEJUMP_S")
				CLEAR_HELP(TRUE)
			ENDIF
			bHelpOnScreen = FALSE
		ENDIF
		
		IF (NOT IS_PED_INJURED(bjLauncherArgs.myPilot))
		AND GET_SCRIPT_TASK_STATUS(bjLauncherArgs.myPilot, SCRIPT_TASK_STAND_STILL) <> WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(bjLauncherArgs.myPilot, SCRIPT_TASK_STAND_STILL) <> PERFORMING_TASK
			TASK_STAND_STILL(bjLauncherArgs.myPilot, -1)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Waiting for the player to approach the start point.
PROC LAUNCHER_CUSTOM_APPROACH_WAIT()
	
//	SWITCH eJumpID															   //new locations:
//		CASE BJJUMPID_HARBOR
//		CASE BJJUMPID_RACE_TRACK
//		CASE BJJUMPID_WINDMILLS
//		CASE BJJUMPID_NORTH_CLIFF
//		CASE BJJUMPID_RUNAWAY_TRAIN
//		CASE BJJUMPID_1K
//		CASE BJJUMPID_1_5K
//		CASE BJJUMPID_CANAL
			BJ_LAUNCHER_UPDATE_HELI_SCENE()
//		BREAK
//		
//		DEFAULT
//			SCRIPT_ASSERT("Wrong type of jump for launcher_BasejumpHeli!")
//		BREAK
//	ENDSWITCH
	
ENDPROC

// Allows the launcher to run an update while the script it wants to launch is loading.
PROC LAUNCHER_CUSTOM_LOAD_SCRIPT_WAIT()
	BJ_LAUNCHER_UPDATE_TRACKING_CAMERA(bjLaunchCam, bjLauncherArgs.myHeli)
ENDPROC

/// PURPOSE:
///    The script has started. Emergency destroy everything.
PROC LAUNCHER_CUSTOM_CLEAR_SCENE()
	
//	IF DOES_ENTITY_EXIST(bjLauncherArgs.myPilot)
//		DELETE_PED(bjLauncherArgs.myPilot)
//	ENDIF
	IF DOES_ENTITY_EXIST(bjLauncherArgs.myHeli) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
//		SPECIAL_FUNCTION_DO_NOT_USE(PLAYER_PED_ID())
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(bjLauncherArgs.myHeli) + <<1, 1, 0>>)
//		DELETE_VEHICLE(bjLauncherArgs.myHeli)
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(bjLauncherArgs.myHeli)
	ENDIF
	
	IF IS_CAM_ACTIVE(bjLaunchCam)
		SET_CAM_ACTIVE(bjLaunchCam, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(bjLaunchCam)
	ENDIF
	LAUNCHER_CUSTOM_RELEASE_ASSETS()

ENDPROC

PROC BJ_LAUNCHER_INITIATE_HELI_SCENE()
	DO_SCREEN_FADE_OUT(3000)
	
	BJ_LAUNCHER_UPDATE_TRACKING_CAMERA(bjLaunchCam, bjLauncherArgs.myHeli)
	
	WHILE IS_SCREEN_FADING_OUT()
		WAIT(0)
		BJ_LAUNCHER_UPDATE_TRACKING_CAMERA(bjLaunchCam, bjLauncherArgs.myHeli)
	ENDWHILE
	
	IF IS_VEHICLE_DRIVEABLE(bjLauncherArgs.myHeli)
		SET_VEHICLE_DOORS_LOCKED(bjLauncherArgs.myHeli, VEHICLELOCK_UNLOCKED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Launch out custom script.
FUNC THREADID LAUNCHER_CUSTOM_RUN_SCRIPT()
//	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//		g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJ_GET_NEAREST_LAUNCHER_LOCATION(GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE))
//	ELSE
//		g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_HARBOR)
//	ENDIF
	g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(eJumpID)
	
	//VECTOR vStartCamPos = BJ_GET_COURSE_INITIAL_CAMERA_POSITION_BY_ID(ENUM_TO_INT(eJumpID))
	//VECTOR vStartCamRot = BJ_GET_COURSE_INITIAL_CAMERA_ROTATION_BY_ID(ENUM_TO_INT(eJumpID))
	
	//FLOAT fCamHeading = vStartCamRot.z
	//FLOAT fCamPitch = vStartCamRot.x
	
	//FLOAT dir_x = COS(fCamHeading)
	//FLOAT dir_y = SIN(fCamHeading)
	//FLOAT dir_z = dir_x*dir_y*TAN(fCamPitch)
	
	
	//VECTOR vCamFrustrum = <<dir_x, dir_y, dir_z>>
	
	//IF NOT IS_VECTOR_ZERO(vStartCamPos)
		//NEW_LOAD_SCENE_START(vStartCamPos, vCamFrustrum, 100)
	//ENDIF
	
//	IF NOT IS_VECTOR_ZERO(vVehicleStreamingLocation)
//	
//	ENDIF
	
	SWITCH eJumpID															   //new locations:
		CASE BJJUMPID_HARBOR
		CASE BJJUMPID_RACE_TRACK
		CASE BJJUMPID_WINDMILLS
		CASE BJJUMPID_NORTH_CLIFF
		CASE BJJUMPID_1K
		CASE BJJUMPID_1_5K
		CASE BJJUMPID_CANAL
			BJ_LAUNCHER_INITIATE_HELI_SCENE()
		BREAK
		
		CASE BJJUMPID_RUNAWAY_TRAIN
			BJ_LAUNCHER_INITIATE_HELI_SCENE()
			SET_BUILDING_STATE(BUILDINGNAME_IPL_EXL3_TRAIN_CRASH, BUILDINGSTATE_NORMAL, TRUE)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_EXL3_TRAIN_CRASH_2, BUILDINGSTATE_NORMAL, TRUE)
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Wrong type of jump for launcher_BasejumpHeli!")
		BREAK
	ENDSWITCH
	
	// Start the thread.
	THREADID ourThread = START_NEW_SCRIPT_WITH_ARGS(scriptName, bjLauncherArgs, SIZE_OF(bjLauncherArgs), iStackSize)
	SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptName)
	
	RETURN ourThread
ENDFUNC

PROC LAUNCHER_CUSTOM_SCRIPT_END()	
	IF eJumpID = BJJUMPID_HARBOR
		CLEAR_VEHICLE_GENERATOR_AREA_OF_INTEREST()
	ENDIF
	SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
ENDPROC

USING "generic_launcher.sch"
