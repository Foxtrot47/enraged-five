// *****************************************************************************************
//
//		MISSION NAME	:	launcher_Hunting_Ambient.sc
//		AUTHOR			:	David Roberts/Ryan Paradis
//		DESCRIPTION		:	Script that determines which ambient hunting mission to setup.
//							This launcher script should be attached to world points for each
//							of the ambient hunting locations
//
// *****************************************************************************************

CONST_INT	LAUNCHER_HAS_SCENE 1
CONST_INT 	LAUNCHER_CUSTOM_SCRIPT_LAUNCH 1
CONST_INT	LAUNCHER_CHAR_REQUIRED 1
CONST_INT	LAUNCHER_DONT_CHECK_RELOAD_RANGE 1
CONST_INT	LAUNCHER_TIME_RESTRICTED 1
CONST_INT	LAUNCHER_CLEAR_SCENE_ON_START 1

USING "generic_launcher_header.sch"
USING "LauncherInclude.sch"

//USING "z_volumes.sch"


VEHICLE_INDEX 	vehStoredLaunch, vehQuad, vehTrailer
BOOL 			bShouldSpawnScene = TRUE
BOOL 			bSpawnedScene = FALSE
BOOL			bSpawnedVehicles = FALSE
BOOL			bHaveTriggeredStream = FALSE

PROC LAUNCHER_CUSTOM_SCRIPT_INIT()
	scriptName = "hunting_ambient"
	
	eMinigame = MINIGAME_HUNTING	
	characterRequired = CHAR_TREVOR
	helpButtonPress = "PLAY_HUNT"
	
	iAvailableAfterHour = 5		// Available after 5:00am
	iAvailableBeforeHour = 19	// Available before 7:00pm
	
	fLaunchScriptDist = 2.0//LOCATE_SIZE_ON_FOOT_ONLY + 1.0
	
	eLauncherStaticBlip = STATIC_BLIP_MINIGAME_HUNTING1
	fLauncherShutdownDist = TO_FLOAT(GENERIC_LAUNCHER_GET_BLIP_STREAMING_RANGE(eLauncherStaticBlip)) + 5.0
	
	// If we don't set a minigame, we won't spawn vehicles.
	IF NOT (GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_HUNTING)))
		CDEBUG3LN(DEBUG_HUNTING, "HUNTING - Flowflag isn't active. Terminating launcher completely.")
		GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
		bShouldSpawnScene = FALSE
	ENDIF
	
	#IF IS_FINAL_BUILD
		iStackSize = MISSION_STACK_SIZE
	#ENDIF
	#IF IS_DEBUG_BUILD
		iStackSize = MISSION_STACK_SIZE
		IF GET_GAME_TIMER() <= g_bjDebugLaunchTimeStamp + 15000
			bDebugLaunched = TRUE
		ENDIF
	#ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_APPROACH_WAIT()
	//UPDATE_ZVOLUME_WIDGETS()
	// Ensure that the vehicle is not able to be started.
	IF DOES_ENTITY_EXIST(vehTrailer)
	AND NOT IS_ENTITY_DEAD(vehTrailer)
	AND NOT IS_ENTITY_ON_FIRE(vehTrailer)
		SET_VEHICLE_DOORS_LOCKED(vehTrailer, VEHICLELOCK_LOCKED)
		SET_VEHICLE_UNDRIVEABLE(vehTrailer, TRUE)
		
		// Nobble that towing!
		SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehTrailer, FALSE)
		SET_VEHICLE_DISABLE_TOWING(vehTrailer, TRUE)
		
		vehStoredLaunch = vehTrailer
		
		IF DOES_ENTITY_EXIST(vehQuad)
		AND NOT IS_ENTITY_DEAD(vehQuad)
			SET_VEHICLE_UNDRIVEABLE(vehQuad, TRUE)
			SET_VEHICLE_DISABLE_TOWING(vehQuad, TRUE)
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehQuad, FALSE)
			
			SET_VEHICLE_LIGHTS(vehQuad, FORCE_VEHICLE_LIGHTS_OFF)
		ENDIF
		
		//CLEAR_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_BLIP_INACTIVE)
	ELSE
		CDEBUG3LN(DEBUG_HUNTING, "HUNTING - Trailer doesn't exist! Not allowing player to launch!")
		
		// If the player explosed either vehicle, it can't be launched.
		IF (DOES_ENTITY_EXIST(vehTrailer)
		AND IS_ENTITY_DEAD(vehTrailer))
		OR NOT DOES_ENTITY_EXIST(vehTrailer)
			LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE) 
		ENDIF
		
		IF (DOES_ENTITY_EXIST(vehQuad)
		AND IS_ENTITY_DEAD(vehQuad))
		OR NOT DOES_ENTITY_EXIST(vehQuad)
			LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE) 
		ENDIF
		
		IF bHaveTriggeredStream
		AND HAS_ANIM_DICT_LOADED("oddjobs@hunterIntro")
		AND HAS_ANIM_DICT_LOADED("oddjobs@hunterOutro")
			CDEBUG3LN(DEBUG_HUNTING, "HUNTING - Unloading intro anims...")
			REMOVE_ANIM_DICT("oddjobs@hunterIntro")
			REMOVE_ANIM_DICT("oddjobs@hunterOutro")
			bHaveTriggeredStream = FALSE
		ENDIF
		
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	IF IS_ENTITY_ON_FIRE(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	// Manage preloading for the minigame.
	BOOL bStreamsRequired = FALSE
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
    AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		IF VDIST(<< -1702.6951, 4666.9414, 22.7091>>, vPlayerPos) < 8.0
			bStreamsRequired = TRUE
		ENDIF
	ENDIF
	IF bStreamsRequired
		CDEBUG3LN(DEBUG_HUNTING, "HUNTING - Preloading intro anims...")
		REQUEST_ANIM_DICT("oddjobs@hunterIntro")
		REQUEST_ANIM_DICT("oddjobs@hunterOutro")
		bHaveTriggeredStream = TRUE
	ELSE
		IF bHaveTriggeredStream
		AND HAS_ANIM_DICT_LOADED("oddjobs@hunterIntro")
		AND HAS_ANIM_DICT_LOADED("oddjobs@hunterOutro")
			CDEBUG3LN(DEBUG_HUNTING, "HUNTING - Unloading intro anims...")
			REMOVE_ANIM_DICT("oddjobs@hunterIntro")
			REMOVE_ANIM_DICT("oddjobs@hunterOutro")
			bHaveTriggeredStream = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_REQUEST_ASSETS()
	IF bShouldSpawnScene
		REQUEST_MODEL(BLAZER)
		REQUEST_MODEL(JOURNEY)
	ENDIF
ENDPROC

FUNC BOOL LAUNCHER_CUSTOM_ASSETS_LOADED()
	IF bShouldSpawnScene
		RETURN HAS_MODEL_LOADED(BLAZER) AND HAS_MODEL_LOADED(JOURNEY)
	ENDIF
	RETURN TRUE
ENDFUNC

PROC LAUNCHER_CUSTOM_UNLOAD_ASSETS()
	IF bShouldSpawnScene
		REQUEST_MODEL(BLAZER)
		REQUEST_MODEL(JOURNEY)
	ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_RELEASE_ASSETS()
	IF bShouldSpawnScene
		SET_MODEL_AS_NO_LONGER_NEEDED(BLAZER)
		SET_MODEL_AS_NO_LONGER_NEEDED(JOURNEY)
	ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_SPAWN_SCENE()	
	IF bShouldSpawnScene
		bSpawnedScene = TRUE

		IF NOT bSpawnedVehicles
			CLEAR_AREA_OF_VEHICLES(<< -1702.46, 4666.36, 22.1255 >>, 10.0)
		
			IF NOT DOES_ENTITY_EXIST(vehQuad)
				vehQuad = CREATE_VEHICLE(BLAZER, <<-1707.4337, 4666.5625, 22.1095>>, 323.2491)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(vehTrailer)
				vehTrailer = CREATE_VEHICLE(JOURNEY, << -1702.6951, 4666.9414, 22.7091>>, 80.03)
				SET_VEHICLE_HAS_STRONG_AXLES(vehTrailer, TRUE)
			ENDIF
			bSpawnedVehicles = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_CLEAR_SCENE()
	IF bShouldSpawnScene
		IF bSpawnedScene
			IF DOES_ENTITY_EXIST(vehQuad)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehQuad)
			ENDIF
			IF DOES_ENTITY_EXIST(vehTrailer)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTrailer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC LAUNCHER_CUSTOM_SCRIPT_END()
	IF bShouldSpawnScene
		vehTrailer = vehStoredLaunch
	ENDIF
ENDPROC

//PROC LAUNCHER_CUSTOM_CLEANUP()
//	IF IS_BITMASK_AS_ENUM_SET(launcherFlags, LAUNCHER_BLIP_INACTIVE)
//	AND (GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(eMinigame)))
//		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_HUNTING1, TRUE)
//	ENDIF
//ENDPROC

FUNC THREADID LAUNCHER_CUSTOM_RUN_SCRIPT()	
	HUNTING_LAUNCH_STRUCT launchData
	launchData.vehTrailer = vehTrailer
	launchData.vehQuad = vehQuad
	
	DEBUG_MESSAGE("******************** Launching OffroadRacing.sc ********************")
	
	THREADID threadTemp = START_NEW_SCRIPT_WITH_ARGS(scriptName, launchData, SIZE_OF(HUNTING_LAUNCH_STRUCT), iStackSize)
	SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptName)
	
	RETURN threadTemp
ENDFUNC

USING "generic_launcher.sch"



