// launcher_Yoga.sc
CONST_INT LAUNCHER_POINT_ENTER 					1		// Tells the script that you kick it off by entering a point.
CONST_INT LAUNCHER_SNAP_CHECKPOINT_TO_GROUND 	1
CONST_INT LAUNCHER_DONT_CHECK_RELOAD_RANGE 		1
CONST_INT LAUNCHER_CHAR_REQUIRED 				1
CONST_INT LAUNCHER_CLEAR_SCENE_ON_START			1

USING "generic_launcher_header.sch"

// PURPOSE: Location enum for Yoga minigame
// TODO: Share for launcher and main script when required
ENUM YOGA_LOCATION
	YOGA_MICHAELS_HOUSE,
	YOGA_NORTH_COAST,
	YOGA_INVALID
ENDENUM
YOGA_LOCATION 				eYogaLocation

/// PURPOSE: Specifies details of minigame specific objects.
STRUCT OBJECT_STRUCT
	OBJECT_INDEX			ObjectIndex				//Object index
	MODEL_NAMES				ModelName				//Object model
	VECTOR					vPosition				//Object postion
	VECTOR					vRotation				//Object rotation
ENDSTRUCT
OBJECT_STRUCT 				objYogaMat
//PED_INDEX TempPed 

// Debug widget
#IF IS_DEBUG_BUILD
	
	// Anim widgets
	WIDGET_GROUP_ID widgetGroup
	WIDGET_GROUP_ID	widgetGroupMat
	FLOAT			fYogaMatPosX  = 0.0
	FLOAT			fYogaMatPosY  = 0.0
	FLOAT			fYogaMatPosZ  = 0.0
	FLOAT			fYogaMatRotX  = 0.0
	FLOAT			fYogaMatRotY  = 0.0
	FLOAT			fYogaMatRotZ  = 0.0
	BOOL 			bEditMode = FALSE
	BOOL			bOutputData
	
	PROC CLEANUP_WIDGET()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(widgetGroupMat)
			DELETE_WIDGET_GROUP(widgetGroupMat)
		ENDIF
	ENDPROC
	
	PROC INIT_WIDGET()
		// Setup widget
		
		CLEANUP_WIDGET()
		
		widgetGroup = START_WIDGET_GROUP("Yoga Launcher")
		widgetGroupMat = START_WIDGET_GROUP("Yoga Mat")
				ADD_WIDGET_STRING("Position")
				ADD_WIDGET_FLOAT_SLIDER("Pos X:", fYogaMatPosX, -7000, 7000, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Pos Y:", fYogaMatPosY, -7000, 7000, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Pos Z:", fYogaMatPosZ, -7000, 7000, 0.1)
				ADD_WIDGET_STRING("Rotation")
				ADD_WIDGET_FLOAT_SLIDER("Rot X", fYogaMatRotX, -360, 360, 1)
				ADD_WIDGET_FLOAT_SLIDER("Rot Y", fYogaMatRotY, -360, 360, 1)
				ADD_WIDGET_FLOAT_SLIDER("Rot Z", fYogaMatRotZ, -360, 360, 1)
				ADD_WIDGET_BOOL("Edit Mode", bEditMode)	
				ADD_WIDGET_BOOL("Output Data", bOutputData)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()	
	ENDPROC
	
	PROC UPDATE_WIDGET()
		
		// Allow editing of Yoga Mat
		IF bEditMode
			IF DOES_ENTITY_EXIST(objYogaMat.ObjectIndex)
				SET_ENTITY_COORDS(objYogaMat.ObjectIndex, <<fYogaMatPosX, fYogaMatPosY, fYogaMatPosZ>>)
				SET_ENTITY_ROTATION(objYogaMat.ObjectIndex, <<fYogaMatRotX, fYogaMatRotY, fYogaMatRotZ>>)
			ENDIF
		ENDIF
		
		// Output position and location to temp_debug.txt
		IF bOutputData
		
			VECTOR vPos = <<fYogaMatPosX, fYogaMatPosY, fYogaMatPosZ>>
			VECTOR vRot = <<fYogaMatRotX, fYogaMatRotY, fYogaMatRotZ>>

			OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("objYogaMat.vPosition = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vPos)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("objYogaMat.vRotation = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vRot)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("objYogaMat.ModelName = PROP_YOGA_MAT_03")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("#IF IS_DEBUG_BUILD")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    fYogaMatPosX = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fYogaMatPosX)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    fYogaMatPosY = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fYogaMatPosY)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    fYogaMatPosZ = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fYogaMatPosZ)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    fYogaMatRotX = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fYogaMatRotX)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    fYogaMatRotY = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fYogaMatRotY)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    fYogaMatRotZ = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fYogaMatRotZ)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("#ENDIF")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			CLOSE_DEBUG_FILE()

			bOutputData = FALSE
		ENDIF
	ENDPROC
	
#ENDIF

FUNC YOGA_LOCATION GET_NEAREST_YOGA_LOCATION()

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		
		// Player's current position
		VECTOR vCurrentPos 
		vCurrentPos = GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()))
		
		// Yoga locations
		VECTOR vLocatePos[2]
		vLocatePos[0] = <<-790.906, 186.293, 71.835>>
		vLocatePos[1] = <<2862.15, 5945.49, 357.11>>
		
		// Distance from locations
		FLOAT fDistToYoga[2] 
		fDistToYoga[0] = VDIST2(vCurrentPos, vLocatePos[0])
		fDistToYoga[1] = VDIST2(vCurrentPos, vLocatePos[1])
		
		// Work out nearest Yoga location
		// NOTE: If we have more locations - we'll change this over to a 
		//       simple loop to iterate through them all.
		IF fDistToYoga[0] < fDistToYoga[1]
			RETURN YOGA_MICHAELS_HOUSE
		ELSE
			RETURN YOGA_NORTH_COAST
		ENDIF
	ENDIF

	RETURN YOGA_INVALID
ENDFUNC

PROC LAUNCHER_CUSTOM_SCRIPT_INIT()	

	scriptName = "Yoga"
	helpButtonPress = "PLAY_YOGA"
	
	eMinigame = MINIGAME_YOGA
	fLaunchScriptDist = 2.5
	
	fLauncherShutdownDist = 20.0
	
	#IF IS_DEBUG_BUILD
		INIT_WIDGET()
	#ENDIF
	
	iStackSize = MISSION_STACK_SIZE
	
	// Setup Yoga location
	eYogaLocation = GET_NEAREST_YOGA_LOCATION()
	IF eYogaLocation = YOGA_MICHAELS_HOUSE
		eLauncherStaticBlip = STATIC_BLIP_MINIGAME_YOGA
		CPRINTLN(DEBUG_AMBIENT, "YOGA: Launched at Michael's House")
		objYogaMat.vPosition = <<-791.0036, 186.3552, 71.8295>> 
		objYogaMat.vRotation = << 0.0, 0.0, -87.1403>>
		objYogaMat.ModelName = PROP_YOGA_MAT_03
		
		#IF IS_DEBUG_BUILD
			fYogaMatPosX = -791.53
			fYogaMatPosY = 186.37
			fYogaMatPosZ = 71.83
			fYogaMatRotX = 0.0
			fYogaMatRotY = 0.0
			fYogaMatRotZ = 0.0
		#ENDIF

	ELIF eYogaLocation = YOGA_NORTH_COAST
		eLauncherStaticBlip = STATIC_BLIP_MINIGAME_YOGA2
		CPRINTLN(DEBUG_AMBIENT, "YOGA: Launched at North Coast")
		objYogaMat.vPosition = << 2861.47, 5945.90, 357.06 >> 
		objYogaMat.vRotation = << 0.0, -0.5, 70.0 >>
		objYogaMat.ModelName = PROP_YOGA_MAT_03

		#IF IS_DEBUG_BUILD
		    fYogaMatPosX = 2862.0000
		    fYogaMatPosY = 5945.7002
		    fYogaMatPosZ = 357.1000
		    fYogaMatRotX = 0.0000
		    fYogaMatRotY = -0.5000
		    fYogaMatRotZ = -13.8000
		#ENDIF
	ELSE
		CPRINTLN(DEBUG_AMBIENT, "YOGA: Unable to find location - IS_PLAYER_PLAYING() returned FALSE")
	ENDIF

	// Yoga can only be done as Michael.
	characterRequired = CHAR_MICHAEL
ENDPROC

// Scene setup loading
PROC LAUNCHER_CUSTOM_REQUEST_ASSETS()
	
	IF NOT IS_MODEL_VALID(objYogaMat.ModelName)
		EXIT
	ENDIF
	
	REQUEST_MODEL(objYogaMat.ModelName)
ENDPROC

FUNC BOOL LAUNCHER_CUSTOM_ASSETS_LOADED()
	
	IF NOT IS_MODEL_VALID(objYogaMat.ModelName)
		RETURN FALSE
	ENDIF
	
	RETURN HAS_MODEL_LOADED(objYogaMat.ModelName)
ENDFUNC

PROC LAUNCHER_CUSTOM_SPAWN_SCENE()
	IF NOT DOES_ENTITY_EXIST(objYogaMat.ObjectIndex)
		objYogaMat.ObjectIndex = CREATE_OBJECT(objYogaMat.ModelName, objYogaMat.vPosition)
		SET_ENTITY_COORDS_NO_OFFSET(objYogaMat.ObjectIndex, objYogaMat.vPosition)
		SET_ENTITY_ROTATION(objYogaMat.ObjectIndex, objYogaMat.vRotation)
		FREEZE_ENTITY_POSITION(objYogaMat.ObjectIndex, TRUE)
	ENDIF
	
ENDPROC

PROC LAUNCHER_CUSTOM_APPROACH_WAIT()
	#IF IS_DEBUG_BUILD
		UPDATE_WIDGET()
	#ENDIF
	
	IF eYogaLocation = YOGA_NORTH_COAST
	
	
		IF IS_SCENARIO_TYPE_ENABLED("WORLD_MOUNTAIN_LION_WANDER")
			CPRINTLN(DEBUG_AMBIENT, "YOGA: Cougar's disabled!")
			SET_SCENARIO_TYPE_ENABLED("WORLD_MOUNTAIN_LION_WANDER",FALSE )
			SET_PED_MODEL_IS_SUPPRESSED(A_C_MTLION,TRUE)
		ENDIF
	
			/*
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())			
			SET_SCENARIO_PEDS_TO_BE_RETURNED_BY_NEXT_COMMAND( TRUE )
			IF GET_CLOSEST_PED( GET_ENTITY_COORDS(PLAYER_PED_ID()), 15, TRUE, FALSE, TempPed )
				//CPRINTLN(DEBUG_AMBIENT, "YOGA: Got a ped")
				IF DOES_ENTITY_EXIST(TempPed)
					//CPRINTLN(DEBUG_AMBIENT, "YOGA: Ped exists")
					IF NOT IS_PED_INJURED(TempPed)
						//CPRINTLN(DEBUG_AMBIENT, "YOGA: Ped is ok ", GET_ENTITY_MODEL(TempPed))
						
						IF GET_ENTITY_MODEL(TempPed) = A_C_MTLION
						
							//CPRINTLN(DEBUG_AMBIENT, "YOGA: Ped is a mountain lion")
							IF NOT IS_PED_FLEEING(TempPed)
								CPRINTLN(DEBUG_AMBIENT, "YOGA: Telling cougar to run away")
								TASK_SMART_FLEE_PED(TempPed, PLAYER_PED_ID(), 300, -1 )
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				SET_PED_AS_NO_LONGER_NEEDED(TempPed)
			
			ENDIF
		ENDIF
	*/
	ENDIF
	
		
ENDPROC

PROC LAUNCHER_CUSTOM_CLEAR_SCENE()
	SET_MODEL_AS_NO_LONGER_NEEDED(objYogaMat.ModelName)
	IF DOES_ENTITY_EXIST(objYogaMat.ObjectIndex)		
		//DELETE_OBJECT(objYogaMat.ObjectIndex)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objYogaMat.ObjectIndex)
	ENDIF
	
ENDPROC

PROC LAUNCHER_CUSTOM_CLEANUP()

	IF NOT IS_SCENARIO_TYPE_ENABLED("WORLD_MOUNTAIN_LION_WANDER")
		CPRINTLN(DEBUG_AMBIENT, "YOGA: Cougar's enabled!")
		SET_SCENARIO_TYPE_ENABLED("WORLD_MOUNTAIN_LION_WANDER",TRUE)
		SET_PED_MODEL_IS_SUPPRESSED(A_C_MTLION,FALSE)
	
	ENDIF

ENDPROC

USING "generic_launcher.sch"
