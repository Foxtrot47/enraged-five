// launcher_BasejumpPack.sc

CONST_INT LAUNCHER_SNAP_CHECKPOINT_TO_GROUND 	0
CONST_INT LAUNCHER_DONT_CHECK_RELOAD_RANGE 		1
CONST_INT LAUNCHER_CUSTOM_SCRIPT_LAUNCH 		1
CONST_INT LAUNCHER_DONT_FADE_DOWN               1
CONST_INT LAUNCHER_VEHICLE_SPAWN                0
CONST_INT LAUNCHER_DISABLE_FOR_MAGDEMO			1
CONST_INT LAUNCHER_HAS_TRIGGER_SCENE			1

CONST_INT BJ_LAUNCHER_FORCE_SPAWN_TIME_MS_OFFSET 15000 //15 seconds to force spawn a heli after we debug warp

USING "generic_launcher_header.sch"
USING "../../scripts/Ambient/BaseJumping/bj_data.sch"

STREAMED_MODEL				ObjectModels[1]
STREAMED_MODEL              VehicleModels[1]
BJ_JUMP_ID 					eJumpID
BJ_LAUNCHER_ARGS 			bjLauncherArgs
structTimer                 tmrEmergency
BOOL						bAllowScene
//BOOL                        bHelpOnScreen
OBJECT_INDEX                objLaunchPack
VEHICLE_INDEX                vehMoto


// Setup our script.
PROC LAUNCHER_CUSTOM_SCRIPT_INIT()	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_FORCED_ON_INIT)
		EXIT
	ENDIF
	
	scriptName = "bj"
	
	eJumpID = BJ_GET_NEAREST_LAUNCHER_LOCATION(vLaunchLocation, FALSE)
	PRINTLN("[launcher_BasejumpPack.sc] >>>>>>>>> (LAUNCHER_CUSTOM_SCRIPT_INIT) eJumpID = ", eJumpID)

	SWITCH eJumpID															   
		CASE BJJUMPID_MAZE_BANK
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_MAZE_BANK
		BREAK
		CASE BJJUMPID_CRANE
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_CRANE
		BREAK
		CASE BJJUMPID_RIVER_CLIFF
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_RIVER_CLIFF
		BREAK
		CASE BJJUMPID_GOLF_COURSE
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_GOLF_COURSE
		BREAK
		CASE BJJUMPID_ROCK_CLIFF
			eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_ROCK_CLIFF
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Wrong type of jump for launcher_BasejumpPack!")
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN("Checking if our debug launch timestamp.")
		IF GET_GAME_TIMER() > g_bjDebugLaunchTimeStamp + BJ_LAUNCHER_FORCE_SPAWN_TIME_MS_OFFSET
	#ENDIF
		IF eLauncherStaticBlip = STATIC_BLIP_AMBIENT_BASEJUMP_MAZE_BANK
			IF IS_RC_MISSION_AVAILABLE(RC_EXTREME_3)
				GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
				PRINTLN("Extreme 3 has a trigger here at maze bank! Terminated basejumping launcher")
			ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
		ENDIF
	#ENDIF
	
	SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_SHUTDOWN_ALLOW_MULTIPLE_COPIES)
	eMinigame = MINIGAME_BASEJUMPING
	fLauncherShutdownDist = TO_FLOAT(GENERIC_LAUNCHER_GET_BLIP_STREAMING_RANGE(eLauncherStaticBlip)) + 5.0
	fLaunchScriptDist = 1.1
	helpButtonPress = "PLAY_BASEJUMP_G"
	
	IF eLauncherStaticBlip <> STATIC_BLIP_NAME_DUMMY_FINAL
		//check if we're active in flow
		IF NOT GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(eMinigame))
//			OR NOT IS_STATIC_BLIP_CURRENTLY_VISIBLE(eLauncherStaticBlip)
			PRINTLN("Mission flow flag not set, or blip not active. not allowing trigger scene.")
			bAllowScene = FALSE
			GENERIC_LAUNCHER_FORCE_WAIT_TERMINATE_AND_CLEAR_STATIC_BLIP(eLauncherStaticBlip)
		ELSE
			PRINTLN("Allowing trigger scene.")
			bAllowScene = TRUE
		ENDIF
	ELSE
		PRINTLN("We have an invalid static blip for this jump. Not allowing scene.")
		bAllowScene = FALSE
	
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("bj")) > 0
		SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_RUN_DESPITE_MISSION)
	ELSE
		CLEAR_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_RUN_DESPITE_MISSION)
	ENDIF
	
//	//TEMP HACK PLEASE REMOVE
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_EXILE)
		SET_BITMASK_AS_ENUM(launcherFlags, LAUNCHER_RUN_DESPITE_MISSION)	
	ENDIF
//	//TEMP HACK PLEASE REMOVE
	
	iStackSize = MISSION_STACK_SIZE
ENDPROC


PROC LAUNCHER_CUSTOM_UNLOAD_ASSETS()
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(ObjectModels)
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(VehicleModels)
ENDPROC

PROC LAUNCHER_CUSTOM_REQUEST_ASSETS()

	//add ped models:
	//add vehicle models:
	SWITCH eJumpID															   //new locations:
		CASE BJJUMPID_CRANE
		CASE BJJUMPID_GOLF_COURSE
			ADD_STREAMED_MODEL(ObjectModels, P_PARACHUTE_S)
			REQUEST_ANIM_DICT("pickup_object")
			REQUEST_ANIM_DICT("oddjobs@basejump@ig_15")
		BREAK
		
		CASE BJJUMPID_MAZE_BANK
		CASE BJJUMPID_RIVER_CLIFF
			ADD_STREAMED_MODEL(ObjectModels, P_PARACHUTE_S)
			ADD_STREAMED_MODEL(VehicleModels, BATI)
			REQUEST_ANIM_DICT("pickup_object")
			REQUEST_ANIM_DICT("oddjobs@basejump@ig_15")
		BREAK
		
		CASE BJJUMPID_ROCK_CLIFF
			ADD_STREAMED_MODEL(ObjectModels, P_PARACHUTE_S)
			REQUEST_ANIM_DICT("pickup_object")
			REQUEST_ANIM_DICT("oddjobs@basejump@ig_15")
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Wrong type of jump for launcher_BasejumpPack!")
		BREAK
	ENDSWITCH
	
	REQUEST_ALL_MODELS(ObjectModels)
	REQUEST_ALL_MODELS(VehicleModels)
					
ENDPROC

FUNC BOOL LAUNCHER_CUSTOM_ASSETS_LOADED()
	SWITCH eJumpID															   //new locations:
		CASE BJJUMPID_CRANE
		CASE BJJUMPID_MAZE_BANK
		CASE BJJUMPID_GOLF_COURSE
		CASE BJJUMPID_RIVER_CLIFF
			RETURN HAS_ANIM_DICT_LOADED("pickup_object")
				AND HAS_ANIM_DICT_LOADED("oddjobs@basejump@ig_15")
				AND ARE_MODELS_STREAMED(ObjectModels) 
				AND ARE_MODELS_STREAMED(VehicleModels)
		BREAK
		
		CASE BJJUMPID_ROCK_CLIFF
			RETURN HAS_ANIM_DICT_LOADED("pickup_object")
				AND HAS_ANIM_DICT_LOADED("oddjobs@basejump@ig_15")
				AND ARE_MODELS_STREAMED(ObjectModels) 
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Wrong type of jump for launcher_BasejumpPack!")
		BREAK
	ENDSWITCH
	
	//if we get this far something is wrong, so just load the object/vehicles
	RETURN ARE_MODELS_STREAMED(ObjectModels) 
		AND ARE_MODELS_STREAMED(VehicleModels)
		
ENDFUNC

PROC LAUNCHER_CUSTOM_RELEASE_ASSETS()
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(ObjectModels)
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(VehicleModels)
	REMOVE_ANIM_DICT("pickup_object")
	REMOVE_ANIM_DICT("oddjobs@basejump@ig_15")
ENDPROC

/// PURPOSE:
///    Creates the other guys at the shooting range.
PROC LAUNCHER_CUSTOM_SPAWN_SCENE()	
	VECTOR vMotoPos
	
	IF bAllowScene
		PRINTLN("[launcher_BasejumpPack] spawning parachute pack and/or motorcycle!")
		SWITCH eJumpID															   //new locations:
			CASE BJJUMPID_MAZE_BANK
			CASE BJJUMPID_CRANE
			CASE BJJUMPID_GOLF_COURSE
				objLaunchPack = CREATE_OBJECT_NO_OFFSET(P_PARACHUTE_S, BJ_GET_LAUNCH_PACK_COORDS_BY_ID(eJumpID))
				SET_ENTITY_ROTATION(objLaunchPack, BJ_GET_LAUNCH_PACK_ROTATION_BY_ID(eJumpID))
				FREEZE_ENTITY_POSITION(objLaunchPack, TRUE)
			BREAK
			
			CASE BJJUMPID_RIVER_CLIFF
			CASE BJJUMPID_ROCK_CLIFF
				objLaunchPack = CREATE_OBJECT_NO_OFFSET(P_PARACHUTE_S, BJ_GET_LAUNCH_PACK_COORDS_BY_ID(eJumpID))//<<-767.2090, 4331.8320, 147.5061>>)
				SET_ENTITY_ROTATION(objLaunchPack, BJ_GET_LAUNCH_PACK_ROTATION_BY_ID(eJumpID))
				PLACE_OBJECT_ON_GROUND_PROPERLY(objLaunchPack)
				
				vMotoPos = BJ_GET_OPTIONAL_MOTO_LOCATION_BY_ID(eJumpID)
				IF NOT IS_VECTOR_ZERO(vMotoPos)
					vehMoto = CREATE_VEHICLE(BATI, vMotoPos, BJ_GET_OPTIONAL_MOTO_HEADING_BY_ID(eJumpID))
					vehMoto = vehMoto
				ENDIF
			BREAK
			DEFAULT
				SCRIPT_ASSERT("Wrong type of jump for launcher_BasejumpPack!")
			BREAK
		ENDSWITCH
	ENDIF	
ENDPROC

PROC BJ_LAUNCHER_UPDATE_PACK_SCENE()
	// Update the player's vehicle.
	VEHICLE_INDEX vehPlayer = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(vehPlayer)
		bjLauncherArgs.lastPlrVehicle = vehPlayer
	ENDIF
	
	IF DOES_ENTITY_EXIST(objLaunchPack)
		VECTOR vLaunchPackCoords = GET_ENTITY_COORDS(objLaunchPack)
		IF VDIST2(vLaunchPackCoords, vLaunchLocation) > 5*5
			PRINTLN("launcher_basejumpPack.sc is going to sleep bc the launch pack got moved")
			LAUNCHER_GOTO_SLEEP(ASLEEP_WAITING_FOR_PLAYER_TO_LEAVE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Waiting for the player to approach the start point.
PROC LAUNCHER_CUSTOM_APPROACH_WAIT()
	SWITCH eJumpID
		CASE BJJUMPID_MAZE_BANK
		CASE BJJUMPID_CRANE
		CASE BJJUMPID_RIVER_CLIFF
		CASE BJJUMPID_GOLF_COURSE
		CASE BJJUMPID_ROCK_CLIFF
			BJ_LAUNCHER_UPDATE_PACK_SCENE()
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Wrong type of jump for launcher_BasejumpPack!")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    The script has started. Emergency destroy everything.
PROC LAUNCHER_CUSTOM_CLEAR_SCENE()
	LAUNCHER_CUSTOM_RELEASE_ASSETS()
ENDPROC

PROC BJ_GET_PACK_SCENE_LAYOUT(VECTOR& vPosition, FLOAT& fHeading)
	vPosition = BJ_GET_LAUNCHER_LOCATION_BY_ID(ENUM_TO_INT(eJumpID))
	
	SWITCH eJumpID
		CASE BJJUMPID_CRANE
			fHeading = -1.800
		BREAK
		
		CASE BJJUMPID_MAZE_BANK
			fHeading = WRAP(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPosition), -180.0, 180.0)
			IF fHeading > 0
				fHeading = FMAX(fHeading, 111.6)
			ELSE
				fHeading = FMIN(fHeading, -104.04)
			ENDIF
		BREAK
		
		CASE BJJUMPID_GOLF_COURSE
			fHeading = WRAP(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPosition), -180.0, 180.0)
			IF fHeading > 0
				fHeading = FMAX(fHeading, 79.28)
			ELSE
				fHeading = FMIN(fHeading, -8.0)
			ENDIF
			
			// Move back from the bollard
		BREAK
		
		CASE BJJUMPID_RIVER_CLIFF
			fHeading = WRAP(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPosition), -180.0, 180.0)
		BREAK
		
		CASE BJJUMPID_ROCK_CLIFF
			fHeading = WRAP(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPosition), -180.0, 180.0)
		BREAK
	ENDSWITCH
ENDPROC

PROC BJ_LAUNCHER_INITIATE_PACK_SYNC_SCENE()
	IF IS_PED_INJURED(PLAYER_PED_ID())
		PRINTLN ("wtf")
		EXIT
	ENDIF
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	IF DOES_ENTITY_EXIST(objLaunchPack)
		DELETE_OBJECT(objLaunchPack)
	ENDIF
	
	ODDJOB_ENTER_CUTSCENE()
	
	objLaunchPack = CREATE_OBJECT_NO_OFFSET(P_PARACHUTE_S, BJ_GET_LAUNCH_PACK_COORDS_BY_ID(eJumpID))
	SET_ENTITY_ROTATION(objLaunchPack, BJ_GET_LAUNCH_PACK_ROTATION_BY_ID(eJumpID))
	//PLACE_OBJECT_ON_GROUND_PROPERLY(objLaunchPack)
	
	VECTOR vSceneRootPosition
	FLOAT fSceneRootHeading
	BJ_GET_PACK_SCENE_LAYOUT(vSceneRootPosition, fSceneRootHeading)
	VECTOR vSceneRootRotation = <<0, 0, fSceneRootHeading>>
	
	INT iPackSyncedScene = CREATE_SYNCHRONIZED_SCENE(vSceneRootPosition, vSceneRootRotation)
	
	PLAY_SYNCHRONIZED_ENTITY_ANIM(objLaunchPack, iPackSyncedScene, "puton_parachute_bag", "oddjobs@basejump@ig_15", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPackSyncedScene, "oddjobs@basejump@ig_15", "puton_parachute", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
	ENDIF
	
	PLAY_SOUND_FRONTEND(-1, "Grab_Parachute", "BASEJUMPS_SOUNDS")
	
	CAMERA_INDEX cSceneCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
	
	PLAY_SYNCHRONIZED_CAM_ANIM(cSceneCam, iPackSyncedScene,  "puton_parachute_cam", "oddjobs@basejump@ig_15")
	
	SET_CAM_ACTIVE(cSceneCam, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	WHILE GET_SYNCHRONIZED_SCENE_PHASE(iPackSyncedScene) < 0.6
		WAIT(0)
	ENDWHILE
	
//	CLEAR_PED_TASKS (PLAYER_PED_ID())
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//	DESTROY_CAM(cSceneCam)
//	STOP_SYNCHRONIZED_ENTITY_ANIM(objLaunchPack, INSTANT_BLEND_OUT, FALSE)
//	PLACE_OBJECT_ON_GROUND_PROPERLY(objLaunchPack)
	//SET_OBJECT_AS_NO_LONGER_NEEDED(objLaunchPack)
ENDPROC

// Put on the pack and (perhaps) walk toward a goal.
PROC BJ_LAUNCHER_INITIATE_PACK_SCENE(BOOL bUseLerpCam)
	structTimer tmrWalktoDuration
	SEQUENCE_INDEX seqPack
	CAMERA_INDEX camInitial, camFinal
	VECTOR vPackToPlayer, vWalkTo, vCameraDir
	FLOAT fDist
	BOOL bAnimSpeed
	
	vCameraDir = BJ_GET_COURSE_INITIAL_CAMERA_ROTATION_BY_ID(eJumpID)
	vCameraDir = NORMALISE_VECTOR(<<COS(vCameraDir.z), SIN(vCameraDir.z), TAN(vCameraDir.x)>>)
	NEW_LOAD_SCENE_START(BJ_GET_COURSE_INITIAL_CAMERA_POSITION_BY_ID(eJumpID), vCameraDir, 5000.0)
	
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
	
	DISPLAY_HUD(FALSE)
	DISPLAY_RADAR(FALSE)
	DISABLE_CELLPHONE(TRUE)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
		
		IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0.3
			TASK_STAND_STILL(PLAYER_PED_ID(), -1)
			WHILE (NOT IS_ENTITY_DEAD(PLAYER_PED_ID())) AND GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0.3
				WAIT(0)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
				ENDIF
			ENDWHILE
		ENDIF
		
		IF DOES_ENTITY_EXIST(objLaunchPack)
			vPackToPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_ENTITY_COORDS(objLaunchPack, FALSE)
			vPackToPlayer.z = 0.0
			fDist = VMAG(vPackToPlayer)
			OPEN_SEQUENCE_TASK(seqPack)
				IF fDist > 0.8
					vPackToPlayer *= 0.78 / fDist
					vWalkTo = GET_ENTITY_COORDS(objLaunchPack, FALSE) + vPackToPlayer					
					IF eJumpID = BJJUMPID_ROCK_CLIFF // 1394787 - special case for now...this task screws up the bank launcher if you stand on the hedge but this one is pretty clear, shouldn't be an issue - SiM
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vWalkTo, PEDMOVE_WALK, DEFAULT, DEFAULT, DEFAULT, GET_HEADING_BETWEEN_VECTORS(vWalkTo, GET_ENTITY_COORDS(objLaunchPack, FALSE)))
					ELSE
						TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkTo, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, GET_HEADING_BETWEEN_VECTORS(vWalkTo, GET_ENTITY_COORDS(objLaunchPack, FALSE)))
					ENDIF					
				ELIF fDist > 0.15
					TASK_ACHIEVE_HEADING(NULL, GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(objLaunchPack, FALSE)))
				ENDIF
				TASK_PLAY_ANIM(NULL, "pickup_object", "pickup_low")
			CLOSE_SEQUENCE_TASK(seqPack)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqPack)
			ENDIF
			CLEAR_SEQUENCE_TASK(seqPack)
			DISABLE_CELLPHONE(TRUE)
		ENDIF
	ENDIF
	
	START_TIMER_NOW(tmrEmergency)
	
	WHILE GET_TIMER_IN_SECONDS(tmrEmergency) < 6.0 AND (NOT IS_ENTITY_DEAD(PLAYER_PED_ID())) AND (NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "pickup_object", "pickup_low"))
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
		WAIT(0)
	ENDWHILE
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "pickup_object", "pickup_low")
		SET_ENTITY_ANIM_SPEED(PLAYER_PED_ID(), "pickup_object", "pickup_low", 0.8)
		bAnimSpeed = TRUE
	ENDIF
	
	WHILE GET_TIMER_IN_SECONDS(tmrEmergency) < 6.0 AND (NOT IS_ENTITY_DEAD(PLAYER_PED_ID())) AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "pickup_object", "pickup_low") < 0.22
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
		IF (NOT bAnimSpeed) AND IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "pickup_object", "pickup_low")
			SET_ENTITY_ANIM_SPEED(PLAYER_PED_ID(), "pickup_object", "pickup_low", 0.8)
			bAnimSpeed = TRUE
		ENDIF
		WAIT(0)
	ENDWHILE
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())	
		IF (NOT bAnimSpeed) AND IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "pickup_object", "pickup_low")
			SET_ENTITY_ANIM_SPEED(PLAYER_PED_ID(), "pickup_object", "pickup_low", 0.70)
			bAnimSpeed = TRUE
		ENDIF
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
	ENDIF
	
	CANCEL_TIMER(tmrEmergency)
	
	BJ_LAUNCHER_INITIATE_PACK_SYNC_SCENE()
		
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 5, 0) 		
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 1, 0)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0) 
		ENDIF
	ENDIF
	
	IF bUseLerpCam
		IF (NOT IS_VECTOR_ZERO(BJ_GET_WALKTO_DESTINATION_BY_ID(eJumpID))) AND (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()))
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), BJ_GET_WALKTO_DESTINATION_BY_ID(eJumpID), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objLaunchPack)
			DELETE_OBJECT(objLaunchPack)
		ENDIF
		
		camInitial = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, BJ_GET_WALKTO_CAMERA_INITIAL_POSITION_BY_ID(eJumpID), BJ_GET_WALKTO_CAMERA_INITIAL_ROTATION_BY_ID(eJumpID), BJ_GET_WALKTO_CAMERA_INITIAL_FOV_BY_ID(eJumpID), TRUE)
		camFinal = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, BJ_GET_WALKTO_CAMERA_FINAL_POSITION_BY_ID(eJumpID), BJ_GET_WALKTO_CAMERA_FINAL_ROTATION_BY_ID(eJumpID), BJ_GET_WALKTO_CAMERA_FINAL_FOV_BY_ID(eJumpID))
		
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		SHAKE_SCRIPT_GLOBAL("HAND_SHAKE", 0.2)
		
		START_TIMER_NOW(tmrWalktoDuration)
		
		WHILE GET_TIMER_IN_SECONDS(tmrWalktoDuration) < 1.5
			WAIT(0)
		ENDWHILE
		
		SET_CAM_ACTIVE_WITH_INTERP(camFinal, camInitial, BJ_GET_WALKTO_FINAL_LERP_DURATION_BY_ID(eJumpID))
		
		RESTART_TIMER_NOW(tmrWalktoDuration)
		
		WHILE GET_TIMER_IN_SECONDS(tmrWalktoDuration) < TO_FLOAT(BJ_GET_WALKTO_FINAL_LERP_DURATION_BY_ID(eJumpID)) / 1000.0 + 0.3
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
			ENDIF
			WAIT(0)
		ENDWHILE
	ELSE
		bjLauncherArgs.objLaunchPack = objLaunchPack
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Launch out custom script.
FUNC THREADID LAUNCHER_CUSTOM_RUN_SCRIPT()
//	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//		g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJ_GET_NEAREST_LAUNCHER_LOCATION(GET_ENTITY_COORDS(PLAYER_PED_ID()), FALSE))
//	ELSE
//		g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_MAZE_BANK)
//	ENDIF
	g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(eJumpID)
	
	//VECTOR vStartCamPos = BJ_GET_COURSE_INITIAL_CAMERA_POSITION_BY_ID(eJumpID))
	//VECTOR vStartCamRot = BJ_GET_COURSE_INITIAL_CAMERA_ROTATION_BY_ID(eJumpID))
	
	//FLOAT fCamHeading = vStartCamRot.z
	//FLOAT fCamPitch = vStartCamRot.x
	
	//FLOAT dir_x = COS(fCamHeading)
	//FLOAT dir_y = SIN(fCamHeading)
	//FLOAT dir_z = dir_x*dir_y*TAN(fCamPitch)
	
	
	//VECTOR vCamFrustrum = <<dir_x, dir_y, dir_z>>
	
	//IF NOT IS_VECTOR_ZERO(vStartCamPos)
		//NEW_LOAD_SCENE_START(vStartCamPos, vCamFrustrum, 100)
	//ENDIF
	
	SWITCH eJumpID															   //new locations:
		CASE BJJUMPID_MAZE_BANK
		CASE BJJUMPID_GOLF_COURSE
			BJ_LAUNCHER_INITIATE_PACK_SCENE(TRUE)
		BREAK
		
		CASE BJJUMPID_CRANE
		CASE BJJUMPID_ROCK_CLIFF
		CASE BJJUMPID_RIVER_CLIFF
			BJ_LAUNCHER_INITIATE_PACK_SCENE(FALSE)
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Wrong type of jump for launcher_BasejumpPack!")
		BREAK
	ENDSWITCH
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
	ENDIF
	
	// Start the thread.
	THREADID ourThread = START_NEW_SCRIPT_WITH_ARGS(scriptName, bjLauncherArgs, SIZE_OF(bjLauncherArgs), iStackSize)
	SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptName)
	
	RETURN ourThread
ENDFUNC

PROC LAUNCHER_CUSTOM_SCRIPT_END()	
	
ENDPROC

USING "generic_launcher.sch"
