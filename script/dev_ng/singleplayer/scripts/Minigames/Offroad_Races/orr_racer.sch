// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	ORR_Racer.sch
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Single Player Races - Racer procs/functions file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "ORR_Head.sch"
USING "ORR_Helpers.sch"
USING "ORR_Gate.sch"
USING "chase_hint_cam.sch"
USING "stunt_plane_public.sch"

// -----------------------------------
// SETUP PROCS/FUNCTIONS
// -----------------------------------

// bits for dialogue
//INT iMissedGateBits			// missed gates
//INT iClearedGateBits		// cleared gates
//INT iPerfectGateBits		// perfect gates
//INT iInvertedGateBits		// inverted gates
//INT iKnifeGateBits			// knife gates
//INT iPerfInvGateBits		// perfect inverted gates
//INT iPerfKniGateBits		// perfect knife gates

// tracks how many consecutive gates weve hit
//INT iStuntGateHit			
ENUM GateStuntBits
	Missed_Once			= BIT0,
	Missed_Twice		= BIT1,
	Missed_Third		= BIT2,
	Hit_Once			= BIT3,
	Hit_Twice			= BIT4,
	Hit_Third			= BIT5,
	Knife_Once			= BIT6,
	Knife_Twice			= BIT7,
	Knife_Third			= BIT8,
	Inverted_Once		= BIT9,
	Inverted_Twice		= BIT10,
	Inverted_Third		= BIT11,
	Perfect_Once		= BIT12,
	Perfect_Twice		= BIT13,
	Perfect_Third		= BIT14,
	P_Inverted_Once		= BIT15,
	P_Inverted_Twice	= BIT16,
	P_Inverted_Third	= BIT17,
	P_Knife_Once		= BIT18,
	P_Knife_Twice		= BIT19,
	P_Knife_Third		= BIT20
ENDENUM

#IF NOT DEFINED(OFFROAD_RACE_VEHICLE_CHECK)
FUNC INT OFFROAD_RACE_VEHICLE_CHECK(ORR_RACER_STRUCT& Racer, INT& iMesaTop)	
	CDEBUG1LN(DEBUG_OR_RACES, "Printing stuff inside of empty OFFROAD_RACE_VEHICLE_CHECK")
	iMesaTop = -1
	PRINTFLOAT(Racer.fClockTime)
	PRINTNL()
	RETURN -1
ENDFUNC
#ENDIF

#IF NOT DEFINED(UnfreezePlayerVehicle)
PROC UnfreezePlayerVehicle()	
	CDEBUG1LN(DEBUG_OR_RACES, "Printing stuff inside of empty UnfreezePlayerVehicle")
ENDPROC
#ENDIF

#IF NOT DEFINED(Play_Gate_Audio)
PROC Play_Gate_Audio(STRING sLine, INT iBitmaskInt)
		PRINTINT(iBitmaskInt)
		PRINTNL()
		PRINTSTRING(sLine)
		PRINTNL()
ENDPROC
#ENDIF

PROC ORR_Racer_Init(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Init")
	Racer.szName = "Racer"
	Racer.Driver = NULL
	Racer.Vehicle = NULL
	Racer.Blip = NULL
	Racer.iGateCur = -1
	Racer.iRank = 0
	Racer.fClockTime = 0.0	
	Racer.fPlsMnsLst = 0.0
	Racer.fPlsMnsTot = 0.0
	Racer.eReset = ORR_RACER_RESET_WAIT
	Racer.vStartPos = ORR_Master.vDefRcrPos
	Racer.fStartHead = ORR_Master.fDefRcrHead
	Racer.eDriverType = ORR_Master.eDefDrvType
	Racer.eDriverModel = ORR_Master.eDefDrvModel
	Racer.eVehicleModel = ORR_Master.eDefVehModel
ENDPROC

PROC ORR_Racer_Setup_Misc(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Setup_Misc")
	Racer.Blip = NULL
	Racer.iGateCur = -1
	Racer.iRank = 0
	Racer.fClockTime = 0.0
	Racer.eReset = ORR_RACER_RESET_WAIT
ENDPROC

PROC ORR_Racer_Setup_Name(ORR_RACER_STRUCT& Racer, STRING sName)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Setup_Name")
	Racer.szName = sName
ENDPROC

PROC ORR_Racer_Setup_Entities(ORR_RACER_STRUCT& Racer, PED_INDEX Driver, VEHICLE_INDEX Vehicle)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Setup_Entities")
	IF Driver <> NULL
		Racer.Driver = Driver
	ENDIF
	IF Vehicle <> NULL
		Racer.Vehicle = Vehicle
	ENDIF
ENDPROC

PROC ORR_Racer_Setup_Start(ORR_RACER_STRUCT& Racer, VECTOR vStartPos, FLOAT fStartHead)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Setup_Start")
		CPRINTLN( DEBUG_OR_RACES, "Setup start is setting the racers start pos..." )
		Racer.vStartPos = vStartPos
		Racer.fStartHead = fStartHead
ENDPROC

PROC ORR_Racer_Setup_Types(ORR_RACER_STRUCT& Racer, PED_TYPE eDriverType, MODEL_NAMES eDriverModel, MODEL_NAMES eVehicleModel)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Setup_Types")
	Racer.eDriverType = eDriverType
	Racer.eDriverModel = eDriverModel
	Racer.eVehicleModel = eVehicleModel
ENDPROC


// -----------------------------------
// DRIVER CREATION PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_Racer_Driver_Request(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Request")
	IF (Racer.eDriverModel = DUMMY_MODEL_FOR_SCRIPT)
		SCRIPT_ASSERT("ORR_Racer_Driver_Request: Model invalid!")
		EXIT
	ENDIF
	
	IF Racer.eDriverModel = A_M_Y_MOTOX_02
		CPRINTLN( DEBUG_OR_RACES, "Requesting A_M_Y_MOTOX_02" )
	ENDIF
	
	REQUEST_MODEL(Racer.eDriverModel)
ENDPROC

FUNC BOOL ORR_Racer_Driver_Stream(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Stream")
	IF (Racer.eDriverModel = DUMMY_MODEL_FOR_SCRIPT)
		SCRIPT_ASSERT("ORR_Racer_Driver_Stream: Model invalid!")
		RETURN FALSE
	ENDIF
	RETURN HAS_MODEL_LOADED(Racer.eDriverModel)
ENDFUNC

PROC ORR_Racer_Driver_Evict(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Evict")
	IF (Racer.eDriverModel = DUMMY_MODEL_FOR_SCRIPT)
		SCRIPT_ASSERT("ORR_Racer_Driver_Evict: Model invalid!")
		EXIT
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(Racer.eDriverModel)
ENDPROC

FUNC BOOL ORR_Racer_Driver_Create(ORR_RACER_STRUCT& Racer)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Create")

	// Check if driver model is valid before continuing.
	IF (Racer.eDriverModel = DUMMY_MODEL_FOR_SCRIPT)
		SCRIPT_ASSERT("ORR_Racer_Driver_Create: Model invalid!")
		RETURN FALSE
	ENDIF
	
	// Check if driver already exists.
	IF NOT IS_ENTITY_DEAD(Racer.Driver)
	
		// Set driver health to max.
		SET_ENTITY_HEALTH(Racer.Driver, 1000)
		
		// If vehicle also exists, put driver in it, if needed.
		IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Create: Tele Veh")
			IF NOT IS_PED_SITTING_IN_VEHICLE(Racer.Driver, Racer.Vehicle)
				SET_PED_INTO_VEHICLE(Racer.Driver, Racer.Vehicle)
			ENDIF			
			CLEAR_PED_TASKS( Racer.Driver )
		// Otherwise, vehicle doesn't exist, teleport driver to start.
		ELSE
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Create: Teleport")			
			SET_ENTITY_COORDS(Racer.Driver, Racer.vStartPos)
			SET_ENTITY_HEADING(Racer.Driver, Racer.fStartHead)
		ENDIF
		
	// Otherwise, if vehicle exists, create driver inside it and check for validity.
	ELIF NOT IS_ENTITY_DEAD(Racer.Vehicle)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Create: Create In Vehicle")
		IF Racer.eDriverModel = A_M_Y_MOTOX_02
			CPRINTLN( DEBUG_OR_RACES, "ORR_Racer_Driver_Create: Create in vehicle MOTOX02" )
		ELSE
			CPRINTLN( DEBUG_OR_RACES, "ORR_Racer_Driver_Create: Create in vehicle MOTOX01" )		
		ENDIF
		
		Racer.Driver = CREATE_PED_INSIDE_VEHICLE(Racer.Vehicle, Racer.eDriverType, Racer.eDriverModel)
		IF IS_ENTITY_DEAD(Racer.Driver)
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Create: Failed to create driver in vehicle!")
			RETURN FALSE
		ENDIF
	// Otherwise, vehicle doesn't exist, just create driver normally and check for validity.
	ELSE
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Create: Create On Foot")
		IF Racer.eDriverModel = A_M_Y_MOTOX_02
			CPRINTLN( DEBUG_OR_RACES, "ORR_Racer_Driver_Create: Create driver MOTOX02" )
		ELSE
			CPRINTLN( DEBUG_OR_RACES, "ORR_Racer_Driver_Create: Create driver MOTOX01" )		
		ENDIF
		Racer.Driver = CREATE_PED(Racer.eDriverType, Racer.eDriverModel, Racer.vStartPos, Racer.fStartHead)
		IF IS_ENTITY_DEAD(Racer.Driver)
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Create: Failed to create driver on foot!")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Check if driver type is player.
	IF (Racer.eDriverType <= PEDTYPE_PLAYER_UNUSED)
	
		// TODO: Put player driver logic here...
		
	// Otherwise, driver type isn't player.
	ELSE
		SET_DRIVER_ABILITY( Racer.Driver, 1.0 )
		SET_DRIVER_RACING_MODIFIER( Racer.Driver, 1.0 )
		// Set driver as mission entity.
		IF NOT IS_ENTITY_A_MISSION_ENTITY(Racer.Driver)
			SET_ENTITY_AS_MISSION_ENTITY(Racer.Driver)
		ENDIF
		
		// Set driver debug name.
		TEXT_LABEL szDebugName = Racer.szName
		szDebugName += "_Drv"
		SET_PED_NAME_DEBUG(Racer.Driver, szDebugName)
		
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(Racer.Driver)	
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(Racer.Driver, KNOCKOFFVEHICLE_HARD)
		IF ORR_DOES_RACE_REQUIRE_MOTORCYCLE()		
			SET_PED_HELMET(Racer.Driver, TRUE)
			GIVE_PED_HELMET(Racer.Driver, TRUE)
			SET_PED_CONFIG_FLAG(Racer.Driver, PCF_DontTakeOffHelmet, TRUE)
		ENDIF
	ENDIF
	
	// Racer driver successfully created.
	RETURN TRUE
	
ENDFUNC


///	PURPOSE:
///    In Triathlon, reset the player's health.
PROC ORR_Racer_TriRacer_Create(ORR_RACER_STRUCT& Racer)
	PRINTLN("[ORR_Racer.sch->ORR_Racer_TriRacer_Create] Procedure started.")

	// Check if driver already exists.
	IF NOT IS_ENTITY_DEAD(Racer.Driver)
		PRINTLN("[ORR_Racer.sch->ORR_Racer_TriRacer_Create] Setting driver to full health.")
		SET_ENTITY_HEALTH(Racer.Driver, 1000)
	ENDIF
ENDPROC


///	PURPOSE:
///    In Triathlon, place the recreated player after resetting.
PROC ORR_Racer_TriRacer_Place(ORR_RACE_STRUCT& Race, ORR_RACER_STRUCT& Racer)
	VECTOR vFwd

	// Check if the current gate is the first one.
	IF Racer.iGateCur > 0 
		// If not, warp the bike to the start of the checkpoint previous to the current.
		VECTOR vTempPlayerPos = Race.sGate[Racer.iGateCur - 1].vPos
		vTempPlayerPos.z += 10
		IF GET_GROUND_Z_FOR_3D_COORD(vTempPlayerPos, vTempPlayerPos.z)
			SET_ENTITY_COORDS(Racer.Driver, vTempPlayerPos)
		ELSE
		
			SET_ENTITY_COORDS(Racer.Driver, Race.sGate[Racer.iGateCur - 1].vPos)
		ENDIF
		
		// Get the vector between the previous and current checkpoint, to use as the driver's heading.
		vFwd = Race.sGate[Racer.iGateCur].vPos - Race.sGate[Racer.iGateCur - 1].vPos
	ELSE
		// Warp the player to the start position.
		SET_ENTITY_COORDS(Racer.Driver, Racer.vStartPos)
		
		// Get the vector between the starting position, and the current checkpoint, to use as the driver's heading.
		vFwd = Race.sGate[Racer.iGateCur].vPos - Racer.vStartPos	
	ENDIF

	// Set its heading towards the current checkpoint.
	IF NOT IS_ENTITY_DEAD(Racer.Driver)
		FLOAT fPlayerHeading
		fPlayerHeading = GET_HEADING_FROM_VECTOR_2D(vFwd.x, vFwd.y)
		SET_ENTITY_HEADING(Racer.Driver, fPlayerHeading)
	ENDIF
ENDPROC

PROC ORR_Racer_Driver_Freeze(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Freeze")
	IF NOT IS_ENTITY_DEAD(Racer.Driver)
		IF NOT IS_PED_IN_ANY_VEHICLE(Racer.Driver)
			FREEZE_ENTITY_POSITION(Racer.Driver, TRUE)
			SET_ENTITY_COLLISION(Racer.Driver, FALSE)
		ENDIF
		SET_ENTITY_INVINCIBLE(Racer.Driver, TRUE)
	ENDIF
ENDPROC

PROC ORR_Racer_Driver_UnFreeze(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_UnFreeze")
	IF NOT IS_ENTITY_DEAD(Racer.Driver)
		IF NOT IS_PED_IN_ANY_VEHICLE(Racer.Driver)
			FREEZE_ENTITY_POSITION(Racer.Driver, FALSE)
			SET_ENTITY_COLLISION(Racer.Driver, TRUE)
		ENDIF
		SET_ENTITY_INVINCIBLE(Racer.Driver, FALSE)
	ENDIF
ENDPROC

PROC ORR_Racer_Driver_Destroy(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Driver_Destroy")
	IF DOES_ENTITY_EXIST(Racer.Driver)
		// TODO: Decide if this player check is even needed...
		IF (Racer.Driver <> PLAYER_PED_ID())
			SET_ENTITY_AS_MISSION_ENTITY(Racer.Driver, TRUE, TRUE)
			DELETE_PED(Racer.Driver)
		ENDIF
	ENDIF
ENDPROC




// -----------------------------------
// VEHICLE CREATION PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_Racer_Vehicle_Request(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_Request")
	IF (Racer.eVehicleModel = DUMMY_MODEL_FOR_SCRIPT)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_Request: Model invalid!")
		EXIT
	ENDIF
	REQUEST_MODEL(Racer.eVehicleModel)
ENDPROC

FUNC BOOL ORR_Racer_Vehicle_Stream(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_Stream")
	IF (Racer.eVehicleModel = DUMMY_MODEL_FOR_SCRIPT)
		OR Racer.eVehicleModel = REBEL
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_Stream: Model invalid!")
		RETURN TRUE
	ENDIF
	RETURN HAS_MODEL_LOADED(Racer.eVehicleModel)
ENDFUNC

PROC ORR_Racer_Vehicle_Evict(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_Evict")
	IF (Racer.eVehicleModel = DUMMY_MODEL_FOR_SCRIPT)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_Evict: Model invalid!")
		EXIT
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(Racer.eVehicleModel)
ENDPROC


FUNC BOOL ORR_Racer_Vehicle_Create(ORR_RACER_STRUCT& Racer, BOOL bForceCreate)
	
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_Create called.")
	VEHICLE_INDEX viOldVeh
	// If forcing a create and vehicle already exists, destroy it.
	IF bForceCreate		
//		CDEBUG1LN(DEBUG_OR_RACES, "Inside force create vehicle")
//		IF DOES_ENTITY_EXIST(Racer.Vehicle)
//			IF NOT IS_ENTITY_DEAD(Racer.Driver)
//			AND NOT IS_ENTITY_DEAD(Racer.Vehicle)
//				IF IS_PED_IN_VEHICLE(Racer.Driver, Racer.Vehicle)
//					CLEAR_PED_TASKS_IMMEDIATELY(Racer.Driver)
//				ENDIF
//			ENDIF
//			IF IS_PED_IN_ANY_VEHICLE(Racer.Driver)
//				VEHICLE_INDEX iVehicle = GET_VEHICLE_PED_IS_IN(Racer.Driver)
//				IF IS_ENTITY_A_MISSION_ENTITY(iVehicle)
//					DELETE_VEHICLE(iVehicle)
//					CDEBUG1LN(DEBUG_OR_RACES, "Deleted the Racer.Vehicle inside force create vehicle")
//				ENDIF
//			ENDIF
//		ENDIF
		
	ENDIF
	
	// If vehicle already exists, set health to max and teleport it.
	IF NOT IS_ENTITY_DEAD(Racer.Vehicle)		
		//IF (ORR_Master.eRaceType = ORR_RACE_TYPE_PLANE) - removing for 162891
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_Create: Teleport")
			SET_ENTITY_HEALTH(Racer.Vehicle, 1000)
			SET_VEHICLE_FIXED(Racer.Vehicle)
			SET_VEHICLE_ENGINE_HEALTH(Racer.Vehicle, 1000.0)
			STOP_ENTITY_FIRE(Racer.Vehicle)
			//SET_ENTITY_COORDS(Racer.Vehicle, Racer.vStartPos) //Racer.vStartPos)
			//SET_ENTITY_HEADING(Racer.Vehicle, Racer.fStartHead)
//			SET_VEHICLE_RADIO_ENABLED(Racer.Vehicle, FALSE)
			
			IF Racer.Driver = PLAYER_PED_ID()
				CPRINTLN( DEBUG_OR_RACES, "Moving player on line 406 in orr_racer.sch" )
			ENDIF
		//ENDIF
	// Otherwise, vehicle doesn't exist, create it and check for validity.
	ELSE
		IF DOES_ENTITY_EXIST( Racer.Vehicle )
			viOldVeh = Racer.Vehicle
		ENDIF
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_Create: Create New Vehicle")
		IF (Racer.eVehicleModel = DUMMY_MODEL_FOR_SCRIPT)
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Create: No vehicle to create!")
			RETURN TRUE
		ELSE
			Racer.Vehicle = CREATE_VEHICLE(Racer.eVehicleModel, Racer.vStartPos, Racer.fStartHead)
			SET_VEHICLE_HAS_STRONG_AXLES(Racer.Vehicle, TRUE)
			SET_VEHICLE_ENGINE_HEALTH(Racer.Vehicle, 1000.0)
			SET_ENTITY_HEALTH(Racer.Vehicle, 1000)
			STOP_ENTITY_FIRE(Racer.Vehicle)
//			SET_VEHICLE_RADIO_ENABLED(Racer.Vehicle, FALSE)
		ENDIF	
		IF (Racer.Driver = PLAYER_PED_ID())
			IF ORR_Master.eRaceType = ORR_RACE_TYPE_OFFROAD
				// set vehicle color here
				IF (iVehColors = 0
				OR iVehColors = -1)
					CDEBUG1LN(DEBUG_OR_RACES, "iVehColors not valid skipping set vehicle colors")
				ELSE
					SET_VEHICLE_COLOUR_COMBINATION(Racer.Vehicle, iVehColors)
				ENDIF
				// set mesa hardtop here
				IF Racer.eVehicleModel = MESA
					IF iMesaRoof = 1
						IF DOES_EXTRA_EXIST(Racer.Vehicle, 1)
							IF NOT IS_VEHICLE_EXTRA_TURNED_ON(Racer.Vehicle, 1)
								SET_VEHICLE_EXTRA(Racer.Vehicle, 1, TRUE)
							ENDIF
						ENDIF
					ELSE
						IF DOES_EXTRA_EXIST(Racer.Vehicle, 1)
							IF IS_VEHICLE_EXTRA_TURNED_ON(Racer.Vehicle, 1)
								SET_VEHICLE_EXTRA(Racer.Vehicle, 1, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		IF (GET_ENTITY_HEIGHT_ABOVE_GROUND(Racer.Vehicle) <= ORR_ON_GROUND_DIST)
			SET_VEHICLE_ON_GROUND_PROPERLY(Racer.Vehicle)
			IF (GET_ENTITY_HEIGHT_ABOVE_GROUND(Racer.Vehicle) > 1.5)		
				VECTOR vEntityCoords = GET_ENTITY_COORDS(Racer.Vehicle)
				vEntityCoords.z -= GET_ENTITY_HEIGHT_ABOVE_GROUND(Racer.Vehicle) + 1.5
				SET_ENTITY_COORDS(Racer.Vehicle, vEntityCoords, TRUE)
			ENDIF
		ENDIF
		IF IS_ENTITY_DEAD(Racer.Vehicle)
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Create: Failed to create vehicle!")
			RETURN FALSE
		ENDIF
	ENDIF
		
	SET_VEHICLE_ENGINE_ON(Racer.Vehicle, TRUE, TRUE)
	
	IF NOT IS_ENTITY_DEAD( Racer.Vehicle )
		SET_VEHICLE_ON_GROUND_PROPERLY( Racer.Vehicle )
	ENDIF
		
	// If driver exists, put them in vehicle, if needed.
	IF NOT IS_ENTITY_DEAD(Racer.Driver)
		IF NOT IS_PED_SITTING_IN_VEHICLE(Racer.Driver, Racer.Vehicle)
			SET_PED_INTO_VEHICLE(Racer.Driver, Racer.Vehicle)
		ENDIF
		
		IF DOES_ENTITY_EXIST( viOldVeh )
			SET_ENTITY_AS_MISSION_ENTITY( viOldVeh, TRUE, TRUE )
			DELETE_VEHICLE( viOldVeh )
		ENDIF
		
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(Racer.Driver, KNOCKOFFVEHICLE_HARD)
		SET_VEHICLE_IS_RACING(Racer.Vehicle, TRUE)
		SET_DRIVER_ABILITY( Racer.Driver, 1.0 )
		SET_DRIVER_AGGRESSIVENESS( Racer.Driver, 1.0 )
		SET_DRIVER_RACING_MODIFIER(Racer.Driver, 1.0)
	ENDIF
	
	// Set vehicle as mission entity.
	IF NOT IS_ENTITY_A_MISSION_ENTITY(Racer.Vehicle)
		SET_ENTITY_AS_MISSION_ENTITY(Racer.Vehicle)
	ENDIF
	
	// Set vehicle debug name.
//	TEXT_LABEL szDebugName = Racer.szName
//	szDebugName += "_Veh"
//	SET_VEHICLE_NAME_DEBUG(Racer.Vehicle, szDebugName)
	
	// Check if driver type is player.
	IF (Racer.eDriverType <= PEDTYPE_PLAYER_UNUSED)
	
		// Store player vehicle in master struct.
		ORR_Master.PlayerVeh = Racer.Vehicle
		
		// Allow player to maintain speed during autopilot.
		SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(Racer.Vehicle, FALSE)
		
	// Otherwise, driver type isn't player.
	ELSE
	
		// TODO: Put ai driver logic here...
		
	ENDIF
	
	// Racer vehicle successfully created.
	RETURN TRUE
	
ENDFUNC


///	PURPOSE:
///    In Triathlon, recreate the player's bike after being reset.
PROC ORR_Racer_Tribike_Create(ORR_RACER_STRUCT& Racer)
	PRINTLN("[ORR_Racer.sch->ORR_Racer_Tribike_Create] Procedure started.")

	IF DOES_ENTITY_EXIST(Racer.Vehicle)
		IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
			PRINTLN("[ORR_Racer.sch->ORR_Racer_Tribike_Create] Player bike is NOT DEAD.  Fixing bike.")
		
			// Fix up the bike.
			SET_ENTITY_HEALTH(Racer.Vehicle, 1000)
			SET_VEHICLE_ENGINE_HEALTH(Racer.Vehicle, 1000.0)
		ELSE
			PRINTLN("[ORR_Racer.sch->ORR_Racer_Tribike_Create] Player bike IS DEAD.  Creating new bike.")
			IF NOT IS_ENTITY_DEAD(Racer.Driver)
				// If bike is dead, create a new one away from the water.
				VECTOR vCreateBikeHere = GET_ENTITY_COORDS(Racer.Driver)
				vCreateBikeHere.Z += 3.0
				Racer.Vehicle = CREATE_VEHICLE(TRIBIKE, vCreateBikeHere, GET_ENTITY_HEADING(Racer.Driver))
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(Racer.Driver)
			// Heal the player, just in case.
			SET_ENTITY_HEALTH(Racer.Driver, 1000)
		ENDIF
		
		// Teleport the player to be on the bike.
		IF NOT IS_PED_IN_ANY_VEHICLE(Racer.Driver)
			IF NOT IS_ENTITY_DEAD(Racer.Driver)
				IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
					PRINTLN("[ORR_Racer.sch->ORR_Racer_Tribike_Create] Player is NOT ON BIKE.  Setting player on bike.")
					SET_PED_INTO_VEHICLE(Racer.Driver, Racer.Vehicle)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_IN_ANY_VEHICLE(Racer.Driver)
			PRINTLN("[ORR_Racer.sch->ORR_Racer_Tribike_Create] Player STILL NOT ON BIKE!")
		ELSE
			PRINTLN("[ORR_Racer.sch->ORR_Racer_Tribike_Create] Player successfully put ON BIKE!")
		ENDIF
	ELSE
		PRINTLN("[ORR_Racer.sch->ORR_Racer_Tribike_Create] Player bike DOES NOT EXIST!")
	ENDIF
ENDPROC


///	PURPOSE:
///    In Triathlon, place the recreated player's bike in the correct position.
PROC ORR_Racer_Tribike_Place(ORR_RACE_STRUCT& Race, ORR_RACER_STRUCT& Racer)
	VECTOR vForwardPreviousToCurrentGate

	// Check if the current gate is the first one.
	IF Racer.iGateCur > 0 
		IF NOT IS_PED_IN_ANY_VEHICLE(Racer.Driver)
			IF NOT IS_ENTITY_DEAD(Racer.Driver)
				IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
					PRINTLN("[ORR_Racer.sch->ORR_Racer_Tribike_Place] Player is NOT IN BIKE.  Setting player in bike.")
					SET_PED_INTO_VEHICLE(Racer.Driver, Racer.Vehicle)
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[ORR_Racer.sch->ORR_Racer_Tribike_Place] Player ALREADY ON BIKE..")
		ENDIF
	
		// If not, warp the bike to the start of the checkpoint previous to the current.
		//SET_ENTITY_COORDS(Racer.Vehicle, Race.sGate[Racer.iGateCur - 1].vPos)
		
		// Teleport the player and his bike to the last gate cleared.
		PRINTLN("[ORR_Racer.sch->ORR_Racer_Tribike_Place] Teleporting player with bike.")
		SET_PED_COORDS_KEEP_VEHICLE(Racer.Driver, Race.sGate[Racer.iGateCur - 1].vPos)
		
		// Get the vector between the previous and current checkpoint, to use as the bike's heading.
		vForwardPreviousToCurrentGate = Race.sGate[Racer.iGateCur].vPos - Race.sGate[Racer.iGateCur - 1].vPos

	// We should never get here, but just in case.
	ELSE
		PRINTLN("[ORR_Racer.sch->ORR_Racer_Tribike_Place] ERROR: This should not be called if gate is 0!  The first gate is always a water gate!")	
	ENDIF
	
	// Set its heading towards the current checkpoint.
	IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
		FLOAT fBikeHeading
		fBikeHeading = GET_HEADING_FROM_VECTOR_2D(vForwardPreviousToCurrentGate.x, vForwardPreviousToCurrentGate.y)
		SET_ENTITY_HEADING(Racer.Vehicle, fBikeHeading)
	ENDIF
ENDPROC

PROC ORR_Racer_Vehicle_Freeze(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_Freeze")
	IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
		FREEZE_ENTITY_POSITION(Racer.Vehicle, TRUE)
		SET_ENTITY_COLLISION(Racer.Vehicle, FALSE)
		SET_ENTITY_INVINCIBLE(Racer.Vehicle, TRUE)
	ENDIF
ENDPROC

PROC ORR_Racer_Vehicle_UnFreeze(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_UnFreeze")
	IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
		FREEZE_ENTITY_POSITION(Racer.Vehicle, FALSE)
		SET_ENTITY_COLLISION(Racer.Vehicle, TRUE)
		SET_ENTITY_INVINCIBLE(Racer.Vehicle, FALSE)
	ENDIF
ENDPROC

PROC ORR_Racer_Vehicle_Destroy(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Vehicle_Destroy")
	IF DOES_ENTITY_EXIST(Racer.Vehicle)
		// TODO: Decide if this player check is even needed...
		IF (Racer.Vehicle <> ORR_Master.PlayerVeh)
			IF NOT ( IS_ENTITY_DEAD(Racer.Driver)  OR  IS_ENTITY_DEAD(Racer.Vehicle) )
				IF IS_PED_IN_VEHICLE(Racer.Driver, Racer.Vehicle)
					CLEAR_PED_TASKS_IMMEDIATELY(Racer.Driver)
				ENDIF
			ENDIF
			SET_ENTITY_AS_MISSION_ENTITY(Racer.Vehicle, TRUE, TRUE)
			DELETE_VEHICLE(Racer.Vehicle)
		ENDIF
	ENDIF
ENDPROC




// -----------------------------------
// MAIN CREATION PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_Racer_Request(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Request")
	ORR_Racer_Driver_Request(Racer)
	ORR_Racer_Vehicle_Request(Racer)
ENDPROC

FUNC BOOL ORR_Racer_Stream(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Stream")
	IF NOT ORR_Racer_Driver_Stream(Racer)
	OR NOT ORR_Racer_Vehicle_Stream(Racer)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC ORR_Racer_Evict(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Evict")
	ORR_Racer_Driver_Evict(Racer)
	ORR_Racer_Vehicle_Evict(Racer)
ENDPROC

FUNC BOOL ORR_Racer_Blip_Create(ORR_RACER_STRUCT& Racer, BOOL bFriendly = TRUE, FLOAT fScale = 1.0)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Blip_Create")
	/* SAVE: Just bliping the driver now, since they will be in the vehicle that we are bliping.
	IF IS_ENTITY_DEAD(Racer.Vehicle)
		IF (Racer.Driver <> PLAYER_PED_ID())
			IF NOT ORR_Blip_Entity_Create(Racer.Blip, Racer.Driver, bFriendly, fScale, Racer.szName)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Blip_Create: Failed to create racer driver blip!")
				RETURN FALSE
			ENDIF
		ENDIF
	ELIF (Racer.Driver <> PLAYER_PED_ID())
	OR NOT IS_PED_SITTING_IN_VEHICLE(Racer.Driver, Racer.Vehicle)
		IF NOT ORR_Blip_Entity_Create(Racer.Blip, Racer.Vehicle, bFriendly, fScale, Racer.szName)
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Blip_Create: Failed to create racer vehicle blip!")
			RETURN FALSE
		ENDIF
	ENDIF*/
	IF (Racer.Driver <> PLAYER_PED_ID())
		IF NOT ORR_Blip_Entity_Create(Racer.Blip, Racer.Driver, bFriendly, fScale, Racer.szName)
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Blip_Create: Failed to create racer driver blip!")
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC ORR_Racer_Blip_Destroy(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Blip_Destroy")
	ORR_Blip_Destroy(Racer.Blip)
ENDPROC

FUNC BOOL ORR_Racer_Create(ORR_RACER_STRUCT& Racer, BOOL bForceCreate)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Create")
	IF NOT ORR_Racer_Vehicle_Create(Racer, bForceCreate)
	OR NOT ORR_Racer_Driver_Create(Racer)
		RETURN FALSE
	ENDIF
	// TODO: Should this be done in every case?
	// BUG : 86339 - they don't want racers blipped any more.
	//ORR_Racer_Blip_Create(Racer)
	RETURN TRUE
ENDFUNC

PROC ORR_Racer_Freeze(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Freeze")
	ORR_Racer_Driver_Freeze(Racer)
	ORR_Racer_Vehicle_Freeze(Racer)
ENDPROC

PROC ORR_Racer_UnFreeze(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_UnFreeze")
	ORR_Racer_Driver_UnFreeze(Racer)
	ORR_Racer_Vehicle_UnFreeze(Racer)
ENDPROC

PROC ORR_Racer_Destroy(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Destroy")
	ORR_Racer_Blip_Destroy(Racer)
	ORR_Racer_Driver_Destroy(Racer)
	ORR_Racer_Vehicle_Destroy(Racer)
ENDPROC




// -----------------------------------
// HELPER PROCS/FUNCTIONS
// -----------------------------------

FUNC BOOL ORR_IS_RACER_IN_SPEED_VOLUME( ORR_RACE_STRUCT & Race, INT iRacer, INT iVolume )
	IF Race.Racer[iRacer].iSpeedVolumeCount > iVolume
		AND Race.Racer[iRacer].iCurrentSpeedVolume <> iVolume
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_DEAD( Race.Racer[iRacer].Driver )
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE( Race.Racer[iRacer].Driver )
		RETURN FALSE
	ENDIF
	
	IF NOT IS_VEHICLE_ON_ALL_WHEELS( Race.Racer[iRacer].Vehicle )
		RETURN FALSE
	ENDIF
	
	//B*1508824
	VECTOR DriverCoords = GET_ENTITY_FORWARD_VECTOR( Race.Racer[iRacer].Driver )
	VECTOR vGateTo = DriverCoords - Race.sGate[Race.Racer[iRacer].iGateCur].vPos
	
	IF GET_ANGLE_BETWEEN_2D_VECTORS( DriverCoords.x, DriverCoords.y, vGateTo.x, vGateTo.y) > ORR_fToleranceForSpeedBoostInDegrees
		RETURN FALSE
	ENDIF
	
	ORR_SPEED_RESTRICTION_VOLUME tVol = Race.tSpeedVolumes[iVolume]
	IF IS_ENTITY_IN_ANGLED_AREA( Race.Racer[iRacer].Driver, tVol.vPos1, tVol.vPos2, tVol.fWidth )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ORR_UPDATE_RACER_IN_SPEED_VOLUME( ORR_RACE_STRUCT & Race, INT iRacer )
	IF IS_ENTITY_DEAD( Race.Racer[iRacer].Vehicle )
		EXIT
	ENDIF
	
	IF PLAYER_PED_ID() = Race.Racer[iRacer].Driver
		EXIT
	ENDIF
	
	INT iIndex
	FLOAT fSpeed
	REPEAT Race.iSpeedVolumeCount iIndex
		IF ORR_IS_RACER_IN_SPEED_VOLUME( Race, iRacer, iIndex )
			IF Race.Racer[iRacer].iCurrentSpeedVolume <> iIndex
				Race.Racer[iRacer].iCurrentSpeedVolume = iIndex
				Race.Racer[iRacer].iSpeedVolumeCount++
			ENDIF
			IF Race.tSpeedVolumes[iIndex].fSpeed < 0
				fSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED( Race.Racer[iRacer].Vehicle )
			ELSE
				fSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED( Race.Racer[iRacer].Vehicle ) * Race.tSpeedVolumes[iIndex].fSpeed
			ENDIF
			
			IF GET_ENTITY_SPEED( Race.Racer[iRacer].Vehicle ) > 8.0
				SET_VEHICLE_FORWARD_SPEED( Race.Racer[iRacer].Vehicle, fSpeed )
				CDEBUG1LN(DEBUG_OR_RACES, "[SPEED BOOST] - !!!!!!!!!! Racer got boost to ", fSpeed)
			ENDIF
				
		ENDIF
	ENDREPEAT
ENDPROC

PROC ORR_Racer_Teleport(ORR_RACER_STRUCT& Racer, VECTOR vPos, FLOAT fHeading, FLOAT fSpeed)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Teleport")
	IF IS_ENTITY_DEAD(Racer.Vehicle) OR NOT IS_PED_IN_VEHICLE(Racer.Driver, Racer.Vehicle)
		CPRINTLN( DEBUG_OR_RACES, "Teleporting racer in Racer teleport..." )
		SET_ENTITY_COORDS(Racer.Driver, vPos)
		SET_ENTITY_HEADING(Racer.Driver, fHeading)
	ELSE	
		CPRINTLN( DEBUG_OR_RACES, "Teleporting vehicle in racer teleport..." )
		SET_ENTITY_COORDS(Racer.Vehicle, vPos)
		SET_ENTITY_HEADING(Racer.Vehicle, fHeading)
		SET_VEHICLE_FORWARD_SPEED(Racer.Vehicle, fSpeed)
		
		// In Tri, this code causes the player's bike to sometimes teleport below the ground.
		SET_VEHICLE_ON_GROUND_PROPERLY(Racer.Vehicle)			
		//WAIT(0)
	ENDIF
	
	IF Racer.Driver = PLAYER_PED_ID()
		CPRINTLN( DEBUG_OR_RACES, "ORR_racer_teleport - moving player on line 749 in orr_racer.sch" )
	ENDIF
ENDPROC

PROC ORR_Racer_Start(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Start")
	
	IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
		IF NOT IS_ENTITY_DEAD(Racer.Driver)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Racer.Vehicle)				
				STOP_PLAYBACK_RECORDED_VEHICLE(Racer.Vehicle)
			ENDIF
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Racer.Driver, TRUE)
		ENDIF
	ENDIF
	
	// Reset racer current gate.
	Racer.iGateCur = 0
	
	// Check if racer vehicle is alive.
	IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
	
		// Start racer vehicle engine, if needed.
		IF NOT IS_THIS_MODEL_A_BIKE(Racer.eVehicleModel)
			SET_VEHICLE_ENGINE_ON(Racer.Vehicle, TRUE, TRUE)
		ENDIF
		
		// Add stuck check for racer vehicle, if needed.
		// TODO: Make sure this isn't needed at all for stuck checks.
		//IF NOT DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(Racer.Vehicle)
		//	ADD_VEHICLE_STUCK_CHECK(Racer.Vehicle, ORR_VEH_STUCK_MIN_MOVE, ORR_VEH_STUCK_CHK_TIME)
		//ENDIF
		
	ENDIF
	
ENDPROC

PROC ORR_Racer_Finish(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Finish")
	
	// Check if racer vehicle is alive.
	IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
		IF ORR_Master.eRaceType <> ORR_RACE_TYPE_PLANE
			UnfreezePlayerVehicle()
			CDEBUG1LN(DEBUG_OR_RACES, "Unfreezing Players pre-race vehicle")
		ENDIF
		// Remove stuck check for racer vehicle, if needed.
		// TODO: Make sure this isn't needed at all for stuck checks.
		//IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(Racer.Vehicle)
		//	REMOVE_VEHICLE_STUCK_CHECK(Racer.Vehicle)
		//ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL ORR_IS_RACER_TOO_FAR_BACK( ORR_RACE_STRUCT & Race, ORR_RACER_STRUCT & Racer )
	IF Racer.Driver = PLAYER_PED_ID()
		RETURN FALSE
	ENDIF
	
	INT iPlayerGate, iRacerGate, iNewGate
	IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
	AND IS_VEHICLE_DRIVEABLE( Race.Racer[0].Vehicle )
		IF IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
			iPlayerGate = Race.Racer[0].iGateCur
			iRacerGate = Racer.iGateCur
			
			IF iPlayerGate - iRacerGate >= Race.iGateLagTolerance
				AND IS_ENTITY_OCCLUDED( Racer.Vehicle )
								
				IF NOT IS_TIMER_STARTED( Race.tRubberBandTimer )
					RESTART_TIMER_NOW( Race.tRubberBandTimer )
				ELIF GET_TIMER_IN_SECONDS( Race.tRubberBandTimer ) < ORR_AI_RUBBER_BAND_TIME
					RETURN FALSE
				ELSE
					RESTART_TIMER_NOW( Race.tRubberBandTimer )
				ENDIF
				
				iNewGate = ( iPlayerGate -1 ) - Race.iGateSkipDist // This is to get us to a gate that isn't the one the player is currently on
				
				iNewGate = CLAMP_INT( iNewGate, 0, Race.iGateCnt )
				
				Racer.iGateCur = iNewGate
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ORR_Racer_Crash_Check(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Crash_Check")
	IF (Racer.eVehicleModel <> DUMMY_MODEL_FOR_SCRIPT)
		IF Racer.Driver = PLAYER_PED_ID() //special cases for player in off-road, as player can get out of vehicle...spaghetti-o'd - SiM
			//IF IS_PED_IN_VEHICLE(Racer.Driver, Racer.Vehicle)
				IF IS_ENTITY_DEAD(Racer.Vehicle)		
					CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Crash_Check: Vehicle dead!")
					RETURN TRUE			
				ENDIF
				IF NOT IS_VEHICLE_DRIVEABLE(Racer.Vehicle)
					CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Crash_Check: Vehicle undrivable!")
					RETURN TRUE
				ENDIF				
			//ENDIF		
		ELSE	
			IF IS_ENTITY_DEAD(Racer.Vehicle)		
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Crash_Check: Vehicle dead!")
				RETURN TRUE			
			ENDIF
			IF NOT IS_VEHICLE_DRIVEABLE(Racer.Vehicle)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Crash_Check: Vehicle undrivable!")
				RETURN TRUE
			ENDIF
		ENDIF
		// TODO: Make sure this isn't needed at all for stuck checks.
		//IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(Racer.Vehicle)
		IF NOT IS_PLAYER_DEAD(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF (GET_ENTITY_SPEED(Racer.Vehicle) <= 10.0)
					// Make sure this is AI we are dealing with
					IF Racer.Driver <> PLAYER_PED_ID() 
						IF NOT IS_TIMER_STARTED( Racer.resetTimer )
							CDEBUG2LN( DEBUG_OR_RACES, "ORR_Racer_Crash_Check: Starting AI Idle timer..." )
							START_TIMER_NOW( Racer.resetTimer )
						ENDIF
						
						IF GET_TIMER_IN_SECONDS( Racer.resetTimer ) > ORR_AI_IDLE_TIME
							AND VDIST2( GET_ENTITY_COORDS( Racer.Driver ), GET_ENTITY_COORDS( PLAYER_PED_ID() ) ) > 225.0 // 15 m^2
							AND IS_ENTITY_OCCLUDED( Racer.Vehicle )
							CDEBUG1LN( DEBUG_OR_RACES, "ORR_Racer_Crash_Check: AI has been idling for too long, returning true" )
							CANCEL_TIMER( Racer.resetTimer )
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_STUCK_TIMER_UP(Racer.Vehicle, VEH_STUCK_ON_ROOF, ORR_VEH_STUCK_BIKE_ROOF_TIME)
						CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Crash_Check: Vehicle stuck (roof)!")
						RETURN TRUE
					ENDIF
					IF IS_VEHICLE_STUCK_TIMER_UP(Racer.Vehicle, VEH_STUCK_ON_SIDE, ORR_VEH_STUCK_BIKE_SIDE_TIME)
						CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Crash_Check: Vehicle stuck (side)!")
						RETURN TRUE
					ENDIF
					IF IS_VEHICLE_STUCK_TIMER_UP(Racer.Vehicle, VEH_STUCK_HUNG_UP, ORR_VEH_STUCK_BIKE_HUNG_TIME)
						CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Crash_Check: Vehicle stuck (hung)!")
						RETURN TRUE
					ENDIF
					IF IS_VEHICLE_STUCK_TIMER_UP(Racer.Vehicle, VEH_STUCK_JAMMED, ORR_VEH_STUCK_BIKE_JAM_TIME)
						CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_Crash_Check: Vehicle stuck (jam)!")
						RETURN TRUE
					ENDIF
				ELIF Racer.Driver <> PLAYER_PED_ID()
					IF IS_TIMER_STARTED( Racer.resetTimer )
						CDEBUG2LN( DEBUG_OR_RACES, "ORR_Racer_Crash_Check: Canceling reset timer" )
						CANCEL_TIMER( Racer.resetTimer )
					ENDIF
				ENDIF
			ENDIF
		ELIF Racer.Driver <> PLAYER_PED_ID()
			IF IS_TIMER_STARTED( Racer.resetTimer )
				CDEBUG2LN( DEBUG_OR_RACES, "ORR_Racer_Crash_Check: Canceling reset timer" )
				CANCEL_TIMER( Racer.resetTimer )
			ENDIF
		ENDIF
		//ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC ORR_RACE_GATE_STATUS ORR_Racer_Gate_Check(ORR_RACE_STRUCT& Race, ORR_GATE_STRUCT& GateCur, ORR_GATE_STRUCT& GateNxt, PED_INDEX pedDriver)
	UNUSED_PARAMETER(Race)
	UNUSED_PARAMETER(GateNxt)
	
	#IF NOT DEFINED(bAudioPlayedLastGate)
		BOOL bAudioPlayedLastGate
		bAudioPlayedLastGate = bAudioPlayedLastGate
	#ENDIF
	
	RETURN ORR_Gate_Check_Pass(GateCur, pedDriver)

ENDFUNC


// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!
