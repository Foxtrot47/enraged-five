// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	ORR_Helpers.sch
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Single Player Races - Helper procs/functions file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "ORR_Head.sch"
USING "lineactivation.sch"	// Include this so it compiles in release, for using GET_OFFSET_FROM_COORD_IN_WORLD_COORDS
USING "cutscene_public.sch"

structTimer cutsceneCameraOffroad, revEngineTimers[6]
structPedsForConversation myDialoguePeds

BOOL bORStreamLoaded

FLOAT racerUpdateWaitTime[6]

INT iCutDialogue

ENUM ORCutDialogue
	DialogueBit1 = BIT0,	// swap 1
	DialogueBit2 = BIT1,	// swap 2
	DialogueBit3 = BIT2,	// taunt
	DialogueBit4 = BIT3		// response
ENDENUM

ENUM ORR_CUTSCENE_STATE
	ORR_CUTSCENE_INIT = 0,
	ORR_CUTSCENE_INTRO_TRANSITION,
	ORR_CUTSCENE_INTRO_TRANSITION_02,
	ORR_CUTSCENE_TRANSITION_01,
	ORR_CUTSCENE_TRANSITION_02,
//	ORR_CUTSCENE_TRANSITION_03,
	ORR_CUTSCENE_TRANSITION_04,
	ORR_CUTSCENE_TRANSITION_05,
	ORR_CUTSCENE_SWAP_TRANSITION_01,
	ORR_CUTSCENE_SWAP_TRANSITION_02,
	ORR_CUTSCENE_SWAP_TRANSITION_03,
	ORR_CUTSCENE_SWAP_TRANSITION_04,
	ORR_CUTSCENE_FINISH,
	ORR_CUTSCENE_SKIP	
ENDENUM

ORR_CUTSCENE_STATE eSPRCutsceneState = ORR_CUTSCENE_INIT

FUNC BOOL TELEPORT_CUTSCENE_VEHICLE_TO_END_POS(ORR_RACE_STRUCT& Race, BOOL& bTeleRecs, FLOAT fDistChk = 0.1)
	UNUSED_PARAMETER(bTeleRecs)
	fDistChk = fDistChk 
	
	IF NOT IS_ENTITY_DEAD(Race.Racer[1].Vehicle)
	SET_VEHICLE_NAME_DEBUG(Race.Racer[1].Vehicle, "Racer 01")
	ENDIF
	IF NOT IS_ENTITY_DEAD(Race.Racer[2].Vehicle)
	SET_VEHICLE_NAME_DEBUG(Race.Racer[2].Vehicle, "Racer 02")
	ENDIF
	IF NOT IS_ENTITY_DEAD(Race.Racer[3].Vehicle)
	SET_VEHICLE_NAME_DEBUG(Race.Racer[3].Vehicle, "Racer 03")
	ENDIF
	IF NOT IS_ENTITY_DEAD(Race.Racer[4].Vehicle)
	SET_VEHICLE_NAME_DEBUG(Race.Racer[4].Vehicle, "Racer 04")
	ENDIF
	IF NOT IS_ENTITY_DEAD(Race.Racer[5].Vehicle)
	SET_VEHICLE_NAME_DEBUG(Race.Racer[5].Vehicle, "Racer 05")
	ENDIF
	
//	INT iIndex
//	REPEAT Race.iRacerCnt iIndex
//		IF NOT IS_ENTITY_DEAD( Race.Racer[iIndex].Vehicle )
//			SET_ENTITY_COORDS( Race.Racer[iIndex].Vehicle, Race.Racer[iIndex].vStartPos )
//			SET_ENTITY_HEADING( Race.Racer[iIndex].Vehicle, Race.Racer[iIndex].fStartHead )
//			SET_VEHICLE_ON_GROUND_PROPERLY( Race.Racer[iIndex].Vehicle )
//		ENDIF
//	ENDREPEAT
//	
//	RETURN TRUE

	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)		
		CASE CanyonCliffs
			IF NOT IS_ENTITY_DEAD(Race.Racer[1].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[1].Vehicle, <<-1932.5958, 4442.1899, 37.3184>>)//blazer
			SET_ENTITY_HEADING(Race.Racer[1].Vehicle, 261.2715)//260.86)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[1].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[2].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[2].Vehicle, <<-1933.2573, 4440.7388, 37.2692>>)//<<-1931.24, 4440.26, 38.05>>)// <<-1933.82, 4443.30, 37.70>>)//blazer
			SET_ENTITY_HEADING(Race.Racer[2].Vehicle, 256.9669)//263.59)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[2].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[3].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[3].Vehicle, <<-1935.01, 4442.57, 37.48>>)//<<-1934.50, 4441.53, 37.60>>)//sanchez
			SET_ENTITY_HEADING(Race.Racer[3].Vehicle, 258.09)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[3].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[4].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[4].Vehicle, <<-1935.50, 4440.32, 37.48>>)//<<-1934.49, 4440.04, 37.82>>)//sanchez
			SET_ENTITY_HEADING(Race.Racer[4].Vehicle, 268.03)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[4].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[5].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[5].Vehicle, <<-1939.63, 4440.46, 37.25>>)//sanchez
			SET_ENTITY_HEADING(Race.Racer[5].Vehicle, 253.32)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[5].Vehicle)
			ENDIF
		BREAK
		CASE RidgeRun
			IF NOT IS_ENTITY_DEAD(Race.Racer[1].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[1].Vehicle, <<-520.2454, 2010.9674, 203.5850>>)//<<-520.8440, 2012.9384, 203.3331>>)//BjXl
			SET_ENTITY_HEADING(Race.Racer[1].Vehicle, 16.9522)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[1].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[2].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[2].Vehicle, <<-516.7449, 2013.2365, 203.7095>>)//<<-517.4503, 2015.5652, 203.1973>>)//Bodhi2
			SET_ENTITY_HEADING(Race.Racer[2].Vehicle, 18.1295)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[2].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[3].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[3].Vehicle, <<-518.3414, 2005.4489, 204.1878>>)//<<-1140.58, 2616.04, 17.22>>)//BfInjection
			SET_ENTITY_HEADING(Race.Racer[3].Vehicle, 20.7231)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[3].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[4].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[4].Vehicle, <<-514.8247, 2008.5737, 204.3629>>)//<<-1139.16, 2612.40, 17.09>>)//Mesa
			SET_ENTITY_HEADING(Race.Racer[4].Vehicle, 20.2278)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[4].Vehicle)
			ENDIF

		BREAK
		CASE ValleyTrail
			IF NOT IS_ENTITY_DEAD(Race.Racer[1].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[1].Vehicle, <<-231.8486, 4225.4614, 43.8031>>)//<<-237.78, 4229.37, 44.31>>)//blazer
			SET_ENTITY_HEADING(Race.Racer[1].Vehicle, 76.66)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[1].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[2].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[2].Vehicle, <<-231.1335, 4228.4702, 43.8715>>)//sanchez
			SET_ENTITY_HEADING(Race.Racer[2].Vehicle, 80.88)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[2].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[3].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[3].Vehicle, <<-228.85, 4227.71, 44.57>>)//blazer
			SET_ENTITY_HEADING(Race.Racer[3].Vehicle, 83.38)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[3].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[4].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[4].Vehicle, <<-229.58, 4225.21, 44.34>>)//sanchez
			SET_ENTITY_HEADING(Race.Racer[4].Vehicle, 75.78)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[4].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[5].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[5].Vehicle, <<-225.41, 4227.53, 44.45>>)//sanchez
			SET_ENTITY_HEADING(Race.Racer[5].Vehicle, 82.70)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[5].Vehicle)
			ENDIF
		BREAK
		CASE LakesideSplash
			IF NOT IS_ENTITY_DEAD(Race.Racer[1].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[1].Vehicle, <<1607.0559, 3841.7207, 33.3075>>)//sanchez
			SET_ENTITY_HEADING(Race.Racer[1].Vehicle, 307.24)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[1].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[2].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[2].Vehicle,<<1608.2616, 3840.2798, 33.0403>>) //<<1607.16, 3846.85, 33.00>>)//blazer
			SET_ENTITY_HEADING(Race.Racer[2].Vehicle, 311.44)//310.77)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[2].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[3].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[3].Vehicle, <<1606.20, 3838.41, 33.62>>) //<<1602.99, 3843.35, 33.00>>)//sanchez
			SET_ENTITY_HEADING(Race.Racer[3].Vehicle, 307.44)//306.80)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[3].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[4].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[4].Vehicle, <<1604.49, 3840.42, 34.18>>)//blazer
			SET_ENTITY_HEADING(Race.Racer[4].Vehicle, 308.19)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[4].Vehicle)
			ENDIF
		BREAK
		CASE EcoFriendly
			IF NOT IS_ENTITY_DEAD(Race.Racer[1].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[1].Vehicle,<<2046.7721, 2130.4421, 91.9486>>)//<<2047.7039, 2132.3323, 91.9814>>)//BfInjection
			SET_ENTITY_HEADING(Race.Racer[1].Vehicle, 233.1493)//231.83)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[1].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[2].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[2].Vehicle, <<2045.3445, 2126.3523, 91.9358>>)//<<2045.5094, 2126.2439, 91.9109>>)//Sadler
			SET_ENTITY_HEADING(Race.Racer[2].Vehicle, 236.3633)//236.14)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[2].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[3].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[3].Vehicle, <<2034.8207, 2138.8853, 92.6919>>)//<<2034.08, 2133.17, 93.50>>) //<<2046.89, 2135.82, 93.17>>)//BjXl
			SET_ENTITY_HEADING(Race.Racer[3].Vehicle, 237.2035)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[3].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[4].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[4].Vehicle, <<2041.2371, 2134.6980, 92.4401>>)//Mesa
			SET_ENTITY_HEADING(Race.Racer[4].Vehicle, 232.9754)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[4].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[5].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[5].Vehicle, <<2039.4126, 2130.2275, 92.5788>> )//BjXl
			SET_ENTITY_HEADING(Race.Racer[5].Vehicle, 241.4449)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[5].Vehicle)
			ENDIF				
		BREAK
		CASE MinewardSpiral
			IF NOT IS_ENTITY_DEAD(Race.Racer[1].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[1].Vehicle, <<2994.4470, 2782.5908, 42.4550>>)//blazer or BfInjection
			SET_ENTITY_HEADING(Race.Racer[1].Vehicle, 26.81)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[1].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[2].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[2].Vehicle, <<2998.0430, 2784.2209, 42.5605>>)//sanchez
			SET_ENTITY_HEADING(Race.Racer[2].Vehicle, 33.42)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[2].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[3].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[3].Vehicle, <<2994.91, 2779.79, 42.73>>)//sanchez
			SET_ENTITY_HEADING(Race.Racer[3].Vehicle, 11.43)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[3].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[4].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[4].Vehicle, <<2999.44, 2781.11, 43.11>>)//blazer
			SET_ENTITY_HEADING(Race.Racer[4].Vehicle, 23.89)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[4].Vehicle)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[5].Vehicle)
			SET_ENTITY_COORDS(Race.Racer[5].Vehicle, <<3000.06, 2775.26, 42.47>>)//BjXl
			SET_ENTITY_HEADING(Race.Racer[5].Vehicle, 17.16)
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[5].Vehicle)
			ENDIF	
		BREAK		
		CASE ConstructionCourse
			
		BREAK
	ENDSWITCH
	RETURN TRUE
//	INT iRacerIndex
//	VECTOR vRacerStartPos, vRacerStartHead
//	
//	IF bTeleRecs
//		REPEAT ORR_RACER_MAX iRacerIndex
//			IF iRacerIndex > 0
//				IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerIndex].Vehicle)
//					FREEZE_ENTITY_POSITION(Race.Racer[iRacerIndex].Vehicle, TRUE)
//
//					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[iRacerIndex].Vehicle)
//						vRacerStartPos = GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_CURRENT_PLAYBACK_FOR_VEHICLE(Race.Racer[iRacerIndex].Vehicle), GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(Race.Racer[iRacerIndex].Vehicle)))
//						vRacerStartHead = GET_ROTATION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_CURRENT_PLAYBACK_FOR_VEHICLE(Race.Racer[iRacerIndex].Vehicle), GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(Race.Racer[iRacerIndex].Vehicle)))
//						STOP_PLAYBACK_RECORDED_VEHICLE(Race.Racer[iRacerIndex].Vehicle)
//						CDEBUG1LN(DEBUG_OR_RACES, "TELEPORT_CUTSCENE_VEHICLE_TO_END_POS:  Playback position set")
//					ELSE
//						vRacerStartPos = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iCutRecording[iRacerIndex],  GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCutRecording[iRacerIndex], "SPROffroadCut"), "SPROffroadCut")
//						vRacerStartHead = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iCutRecording[iRacerIndex], GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCutRecording[iRacerIndex], "SPROffroadCut"), "SPROffroadCut")
//						CDEBUG1LN(DEBUG_OR_RACES, "TELEPORT_CUTSCENE_VEHICLE_TO_END_POS:  NON playback position set")
//					ENDIF
//					
//					IF NOT IS_ENTITY_AT_COORD(Race.Racer[iRacerIndex].Vehicle, vRacerStartPos, <<fDistChk, fDistChk, fDistChk>>)
//						SET_ENTITY_COORDS(Race.Racer[iRacerIndex].Vehicle, vRacerStartPos)	
//						CDEBUG1LN(DEBUG_OR_RACES, "TELEPORT_CUTSCENE_VEHICLE_TO_END_POS:  Vehicle teleported to new position")
//					ENDIF		
//					SET_ENTITY_ROTATION(Race.Racer[iRacerIndex].Vehicle, vRacerStartHead)
//					SET_ENTITY_VELOCITY(Race.Racer[iRacerIndex].Vehicle, <<0, 0, 0>>)
//					SET_VEHICLE_FORWARD_SPEED(Race.Racer[iRacerIndex].Vehicle, 0.0)
//					SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[iRacerIndex].Vehicle)
//					FREEZE_ENTITY_POSITION(Race.Racer[iRacerIndex].Vehicle, FALSE)
//				ENDIF
//			ENDIF
//		ENDREPEAT
//		bTeleRecs = FALSE
//		CDEBUG1LN(DEBUG_OR_RACES, "TELEPORT_CUTSCENE_VEHICLE_TO_END_POS: FALSE racers teleported")
//		RETURN TRUE
//	ENDIF
//	
//	CDEBUG1LN(DEBUG_OR_RACES, "TELEPORT_CUTSCENE_VEHICLE_TO_END_POS: Default return false")
//	RETURN FALSE
ENDFUNC

FUNC BOOL TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS(ORR_RACE_STRUCT& Race, BOOL& bTelePlayer, FLOAT fDistChk = 0.1, BOOL bSkipped = FALSE)

	UNUSED_PARAMETER(bTelePlayer)
	fDistChk = fDistChk
	UNUSED_PARAMETER(Race)
	
	IF bSkipped	
		OR Race.bRestarting
		IF NOT IS_ENTITY_DEAD( Race.Racer[0].Vehicle )
			//SET_VEHICLE_RADIO_ENABLED( Race.Racer[0].Vehicle, FALSE )
		ENDIF
		SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)		
			CASE CanyonCliffs
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<-1937.8853, 4443.3193, 36.5555>>)//<<-1943.7817, 4447.5181, 35.6896>>)//<<-1938.34, 4443.20, 37.06>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 256.8476)//204.7505)//249.56)
			BREAK
			CASE RidgeRun
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<-517.2068, 2000.5529, 204.6160>>)//<<-518.6960, 1993.9945, 204.9752>>)//<<-516.1545, 1999.3740, 204.7715>>)//<<-1141.40, 2619.60, 17.30>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 20.3384)//345.9835)//18.7907)
			BREAK
			CASE ValleyTrail
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<-225.98, 4224.74, 44.36>>) //<<-226.44, 4226.15, 44.35>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 80.20)
			BREAK
			CASE LakesideSplash
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<1602.54, 3837.21, 33.72>>) //<<1600.01, 3839.72, 33.42>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 308.94)
			BREAK
			CASE EcoFriendly
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<2030.8984, 2134.1968, 92.5014>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 249.4471)
			BREAK
			CASE MinewardSpiral
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<2995.8235, 2774.8606, 41.9576>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 354.6729)
			BREAK		
			CASE ConstructionCourse
				
			BREAK
		ENDSWITCH
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()	

			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[0].Vehicle)
			RETURN TRUE
	ENDIF
	
	CPRINTLN( DEBUG_OR_RACES, "trying to teleport the player to the proper location iRaceCur is: ", ENUM_TO_INT( ORR_Master.iRaceCur ) )
	IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
		CPRINTLN( DEBUG_OR_RACES, "Teleporting player vehicle." )
		SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)		
			CASE CanyonCliffs
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<-1946.7852, 4448.6514, 35.2621>>)//<<-1937.8853, 4443.3193, 36.5555>>)//<<-1943.7817, 4447.5181, 35.6896>>)//<<-1938.34, 4443.20, 37.06>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 230.0273)//256.8476)//204.7505)//249.56)
			BREAK
			CASE RidgeRun
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<-517.9825, 1991.6240, 205.1206>>)//<<-517.2068, 2000.5529, 204.6160>>)//<<-518.6960, 1993.9945, 204.9752>>)//<<-516.1545, 1999.3740, 204.7715>>)//<<-1141.40, 2619.60, 17.30>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 330.0374)//20.3384)//345.9835)//18.7907)
			BREAK
			CASE ValleyTrail
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<-220.6522, 4222.0313, 43.7630>>)//<<-225.98, 4224.74, 44.36>>) //<<-226.44, 4226.15, 44.35>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 65.2566)//80.20)
			BREAK
			CASE LakesideSplash
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<1596.7162, 3829.3076, 32.9941>>)//<<1602.54, 3837.21, 33.72>>) //<<1600.01, 3839.72, 33.42>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 316.0527)//308.94)
			BREAK
			CASE EcoFriendly
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<2031.7719, 2134.9775, 92.5141>>)//<<2030.8984, 2134.1968, 92.5014>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 243.1813)//249.4471)
			BREAK
			CASE MinewardSpiral
				SET_ENTITY_COORDS(Race.Racer[0].Vehicle, <<2993.9941, 2766.8936, 41.7335>>)
				SET_ENTITY_HEADING(Race.Racer[0].Vehicle, 20.89)
			BREAK		
			CASE ConstructionCourse
				
			BREAK
		ENDSWITCH
			SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[0].Vehicle)
	ELIF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
		IF NOT IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
			CPRINTLN( DEBUG_OR_RACES, "Teleporting player..." )
			SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)		
				CASE CanyonCliffs
					SET_ENTITY_COORDS(Race.Racer[0].Driver, <<-1938.34, 4443.20, 37.06>>)
					SET_ENTITY_HEADING(Race.Racer[0].Driver, 249.56)
				BREAK
				CASE RidgeRun
					SET_ENTITY_COORDS(Race.Racer[0].Driver, <<-512.3987, 2001.4885, 204.8844>>)//<<-1141.40, 2619.60, 17.30>>)
					SET_ENTITY_HEADING(Race.Racer[0].Driver, 24.0095)
				BREAK
				CASE ValleyTrail
					SET_ENTITY_COORDS(Race.Racer[0].Driver, <<-225.98, 4224.74, 44.36>>) //<<-226.44, 4226.15, 44.35>>)
					SET_ENTITY_HEADING(Race.Racer[0].Driver, 80.20)
				BREAK
				CASE LakesideSplash
					SET_ENTITY_COORDS(Race.Racer[0].Driver, <<1602.54, 3837.21, 33.72>>) //<<1600.01, 3839.72, 33.42>>)
					SET_ENTITY_HEADING(Race.Racer[0].Driver, 308.94)
				BREAK
				CASE EcoFriendly
					SET_ENTITY_COORDS(Race.Racer[0].Driver, <<2037.60, 2136.15, 93.15>>)
					SET_ENTITY_HEADING(Race.Racer[0].Driver, 238.07)
				BREAK
				CASE MinewardSpiral
					SET_ENTITY_COORDS(Race.Racer[0].Driver, <<2996.96, 2773.84, 42.41>>)
					SET_ENTITY_HEADING(Race.Racer[0].Driver, 20.89)
				BREAK		
				CASE ConstructionCourse
					
				BREAK
			ENDSWITCH
		ELSE
			CPRINTLN( DEBUG_OR_RACES, "Teleporting player vehicle he is in..." )
			VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN( PLAYER_PED_ID() )
			//SET_ENTITY_AS_MISSION_ENTITY( viPlayerVeh )
			SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)		
				CASE CanyonCliffs
					SET_ENTITY_COORDS(viPlayerVeh, <<-1938.34, 4443.20, 37.06>>)
					SET_ENTITY_HEADING(viPlayerVeh, 249.56)
				BREAK
				CASE RidgeRun
					SET_ENTITY_COORDS(viPlayerVeh, <<-512.3987, 2001.4885, 204.8844>>)//<<-1141.40, 2619.60, 17.30>>)
					SET_ENTITY_HEADING(viPlayerVeh, 24.0095)
				BREAK
				CASE ValleyTrail
					SET_ENTITY_COORDS(viPlayerVeh, <<-225.98, 4224.74, 44.36>>) //<<-226.44, 4226.15, 44.35>>)
					SET_ENTITY_HEADING(viPlayerVeh, 80.20)
				BREAK
				CASE LakesideSplash
					SET_ENTITY_COORDS(viPlayerVeh, <<1602.54, 3837.21, 33.72>>) //<<1600.01, 3839.72, 33.42>>)
					SET_ENTITY_HEADING(viPlayerVeh, 308.94)
				BREAK
				CASE EcoFriendly
					SET_ENTITY_COORDS(viPlayerVeh, <<2037.60, 2136.15, 93.15>>)
					SET_ENTITY_HEADING(viPlayerVeh, 238.07)
				BREAK
				CASE MinewardSpiral
					SET_ENTITY_COORDS(viPlayerVeh, <<2996.96, 2773.84, 42.41>>)
					SET_ENTITY_HEADING(viPlayerVeh, 20.89)
				BREAK		
				CASE ConstructionCourse
					
				BREAK
			ENDSWITCH	
			SET_VEHICLE_ON_GROUND_PROPERLY( viPlayerVeh )
		ENDIF
	ENDIF
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	RETURN TRUE
//	INT iLastWaypoint
//	VECTOR vRacerStartPos
//	TEXT_LABEL sPlaWayRec
//	
//	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)		
//		CASE CanyonCliffs
//			sPlaWayRec = "offroad_cut_01"
//		BREAK
//		CASE RidgeRun
//			sPlaWayRec = "offroad_cut_02"
//		BREAK
//		CASE ValleyTrail
//			sPlaWayRec = "offroad_cut_03"
//		BREAK
//		CASE LakesideSplash
//			sPlaWayRec = "offroad_cut_04"
//		BREAK
//		CASE EcoFriendly
//			sPlaWayRec = "offroad_cut_05"
//		BREAK
//		CASE MinewardSpiral
//			sPlaWayRec = "offroad_cut_06"
//		BREAK		
//		CASE ConstructionCourse
//			sPlaWayRec = "offroad_cut_07"
//		BREAK
//	ENDSWITCH
	
//	IF bTelePlayer
//		IF WAYPOINT_RECORDING_GET_NUM_POINTS(sPlaWayRec, iLastWaypoint)
//			CDEBUG1LN(DEBUG_OR_RACES, "TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS: iLastWaypoint set to: ")
//			PRINTINT(iLastWaypoint)
//			PRINTNL()
//			IF WAYPOINT_RECORDING_GET_COORD(sPlaWayRec, (iLastWaypoint-1), vRacerStartPos)
//				CDEBUG1LN(DEBUG_OR_RACES, "TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS: vRacerStartPos set to: ")
//				PRINTVECTOR(vRacerStartPos)
//				PRINTNL()
//				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//				AND NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//					FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
//					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//						STOP_PLAYBACK_RECORDED_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//					ENDIF
//					IF NOT IS_ENTITY_AT_COORD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vRacerStartPos, <<fDistChk, fDistChk, fDistChk>>)
//						SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vRacerStartPos)	
//						CDEBUG1LN(DEBUG_OR_RACES, "TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS:  Player vehicle teleported to new position")
//					ENDIF	
//					SET_ENTITY_VELOCITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<0, 0, 0>>)
//					SET_VEHICLE_FORWARD_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 0.0)
//					SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), Race.Racer[1].fStartHead)
//					SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))								
//					FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
//					CDEBUG1LN(DEBUG_OR_RACES, "TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS:  Player vehicle settled and unfrozen")
//				ENDIF
//			ELSE
//			    CDEBUG1LN(DEBUG_OR_RACES, "!!! BAD SHIT HAPPENED FOR PLAYER TELEPORT !!!")
//			ENDIF
//		ELSE
//			SCRIPT_ASSERT("TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS: WAYPOINT_RECORDING_GET_NUM_POINTS returned FALSE")
//		ENDIF
//		bTelePlayer = FALSE
//		CDEBUG1LN(DEBUG_OR_RACES, "TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS: bTelePlayer = FALSE player teleported, returning TRUE")
//		RETURN TRUE
//	ENDIF
//	CDEBUG1LN(DEBUG_OR_RACES, "TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS: Default return false")
//	RETURN FALSE
ENDFUNC

PROC STOP_AFTER_WAYPOINT_RECORDING(ORR_RACE_STRUCT& Race, FLOAT fDistChk = 0.1)
//	INT iLastWaypoint
	VECTOR vEndofRecording
//	TEXT_LABEL sPlaWayRec
//	
//	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)		
//		CASE CanyonCliffs
//			sPlaWayRec = "offroad_cut_01"
//		BREAK
//		CASE RidgeRun
//			sPlaWayRec = "offroad_cut_02"
//		BREAK
//		CASE ValleyTrail
//			sPlaWayRec = "offroad_cut_03"
//		BREAK
//		CASE LakesideSplash
//			sPlaWayRec = "offroad_cut_04"
//		BREAK
//		CASE EcoFriendly
//			sPlaWayRec = "offroad_cut_05"
//		BREAK
//		CASE MinewardSpiral
//			sPlaWayRec = "offroad_cut_06"
//		BREAK		
//		CASE ConstructionCourse
//			sPlaWayRec = "offroad_cut_07"
//		BREAK	
//	ENDSWITCH
	
//	IF WAYPOINT_RECORDING_GET_NUM_POINTS(sPlaWayRec, iLastWaypoint)
//		IF WAYPOINT_RECORDING_GET_COORD(sPlaWayRec, (iLastWaypoint-1), vEndofRecording)
			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
				IF IS_ENTITY_AT_COORD(Race.Racer[0].Vehicle, vEndofRecording, <<fDistChk, fDistChk, fDistChk>>)
					FREEZE_ENTITY_POSITION(Race.Racer[0].Vehicle, TRUE)
//					STOP_PLAYBACK_RECORDED_VEHICLE(Race.Racer[0].Vehicle)
					SET_ENTITY_VELOCITY(Race.Racer[0].Vehicle, <<0, 0, 0>>)
					SET_VEHICLE_FORWARD_SPEED(Race.Racer[0].Vehicle, 0.0)
					SET_ENTITY_HEADING(Race.Racer[0].Vehicle, Race.Racer[1].fStartHead)
					SET_VEHICLE_ON_GROUND_PROPERLY(Race.Racer[0].Vehicle)								
					FREEZE_ENTITY_POSITION(Race.Racer[0].Vehicle, FALSE)
					CDEBUG1LN(DEBUG_OR_RACES, "STOP_AFTER_WAYPOINT_RECORDING:  Player vehicle settled and unfrozen")
				ENDIF
			ENDIF
//		ELSE
//		    CDEBUG1LN(DEBUG_OR_RACES, "STOP_AFTER_WAYPOINT_RECORDING:  WAYPOINT_RECORDING_GET_COORD returned false")
//		ENDIF
//	ENDIF
ENDPROC


// check everything loaded
FUNC BOOL Load_Cutscene_assets()
	
//	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
//		
//		CASE CanyonCliffs
////			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[0], "SPROffroadCut")
//			IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_01")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[1], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[2], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[3], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[4], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[5], "SPROffroadCut")
//				RETURN FALSE
//			ELSE 
//				RETURN TRUE
//			ENDIF
//		BREAK
//		
//		CASE RidgeRun
////			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[0], "SPROffroadCut")
//			IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_02")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[1], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[2], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[3], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[4], "SPROffroadCut")
//				RETURN FALSE
//			ELSE 
//				RETURN TRUE
//			ENDIF
//		BREAK
//
//		CASE ValleyTrail
////			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[0], "SPROffroadCut")
//			IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_03")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[1], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[2], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[3], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[4], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[5], "SPROffroadCut")
//				RETURN FALSE
//			ELSE 
//				RETURN TRUE
//			ENDIF
//		BREAK
//		
//		CASE LakesideSplash
////			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[0], "SPROffroadCut")
//			IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_04")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[1], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[2], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[3], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[4], "SPROffroadCut")
//			ELSE 
//				RETURN TRUE
//			ENDIF		
//		BREAK
//		CASE EcoFriendly
////			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[0], "SPROffroadCut")
//			IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_05")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[1], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[2], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[3], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[4], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[5], "SPROffroadCut")
//				RETURN FALSE
//			ELSE 
//				RETURN TRUE
//			ENDIF	
//		BREAK
//		CASE MinewardSpiral
////			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[0], "SPROffroadCut")
//			IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_06")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[1], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[2], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[3], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[4], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[5], "SPROffroadCut")
//				RETURN FALSE
//			ELSE 
//				RETURN TRUE
//			ENDIF		
//		BREAK
//		CASE ConstructionCourse
////			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[0], "SPROffroadCut")
//			IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_07")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[1], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[2], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[3], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[4], "SPROffroadCut")
//				AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[5], "SPROffroadCut")
//				RETURN FALSE
//			ELSE 
//				RETURN TRUE
//			ENDIF		
//		BREAK
//		
//	ENDSWITCH
	RETURN TRUE
ENDFUNC

PROC start_vehicle_recordings(ORR_RACE_STRUCT& Race)

//	IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
//	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[0], "SPROffroadCut")
//		FREEZE_ENTITY_POSITION(Race.Racer[0].Vehicle, FALSE)
//		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[0].Vehicle)
//			START_PLAYBACK_RECORDED_VEHICLE(Race.Racer[0].Vehicle, iCutRecording[0], "SPROffroadCut")
//		ENDIF
//	ENDIF
	IF NOT IS_ENTITY_DEAD(Race.Racer[1].Vehicle)
//	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[1], "SPROffroadCut")
		FREEZE_ENTITY_POSITION(Race.Racer[1].Vehicle, FALSE)
//		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[1].Vehicle)
//			START_PLAYBACK_RECORDED_VEHICLE(Race.Racer[1].Vehicle, iCutRecording[1], "SPROffroadCut")
//		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(Race.Racer[2].Vehicle)
//	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[2], "SPROffroadCut")
		FREEZE_ENTITY_POSITION(Race.Racer[2].Vehicle, FALSE)
//		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[2].Vehicle)
//			START_PLAYBACK_RECORDED_VEHICLE(Race.Racer[2].Vehicle, iCutRecording[2], "SPROffroadCut")
//			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(Race.Racer[2].Vehicle, 3000.0)
//		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(Race.Racer[3].Vehicle)
//	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[3], "SPROffroadCut")
		FREEZE_ENTITY_POSITION(Race.Racer[3].Vehicle, FALSE)
//		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[3].Vehicle)
//			START_PLAYBACK_RECORDED_VEHICLE(Race.Racer[3].Vehicle, iCutRecording[3], "SPROffroadCut")
//		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(Race.Racer[4].Vehicle)
//	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[4], "SPROffroadCut")
		FREEZE_ENTITY_POSITION(Race.Racer[4].Vehicle, FALSE)
//		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[4].Vehicle)
//			START_PLAYBACK_RECORDED_VEHICLE(Race.Racer[4].Vehicle, iCutRecording[4], "SPROffroadCut")
//		ENDIF
	ENDIF
	
//	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
//		
//		CASE CanyonCliffs		
//			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
//			AND GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_01")
//				FREEZE_ENTITY_POSITION(Race.Racer[0].Vehicle, FALSE)
//				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[0].Vehicle)
//					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Race.Racer[0].Driver, Race.Racer[0].Vehicle, "offroad_cut_01", DRIVINGMODE_PLOUGHTHROUGH, 0, EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
//				ENDIF
//			ENDIF
//		
//			IF NOT IS_ENTITY_DEAD(Race.Racer[5].Vehicle)
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[5], "SPROffroadCut")
//				FREEZE_ENTITY_POSITION(Race.Racer[5].Vehicle, FALSE)
//				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[5].Vehicle)
//					START_PLAYBACK_RECORDED_VEHICLE(Race.Racer[5].Vehicle, iCutRecording[5], "SPROffroadCut")
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE RidgeRun
//			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
//			AND GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_02")
//				FREEZE_ENTITY_POSITION(Race.Racer[0].Vehicle, FALSE)
//				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[0].Vehicle)
//					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Race.Racer[0].Driver, Race.Racer[0].Vehicle, "offroad_cut_02", DRIVINGMODE_PLOUGHTHROUGH, 0, EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
//				ENDIF
//			ENDIF
//		BREAK
//
//		CASE ValleyTrail
//			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
//			AND GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_03")
//				FREEZE_ENTITY_POSITION(Race.Racer[0].Vehicle, FALSE)
//				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[0].Vehicle)
//					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Race.Racer[0].Driver, Race.Racer[0].Vehicle, "offroad_cut_03", DRIVINGMODE_PLOUGHTHROUGH, 0, EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
//				ENDIF
//			ENDIF
//			
//			IF NOT IS_ENTITY_DEAD(Race.Racer[5].Vehicle)
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[5], "SPROffroadCut")
//				FREEZE_ENTITY_POSITION(Race.Racer[5].Vehicle, FALSE)
//				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[5].Vehicle)
//					START_PLAYBACK_RECORDED_VEHICLE(Race.Racer[5].Vehicle, iCutRecording[5], "SPROffroadCut")
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE LakesideSplash
//			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
//			AND GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_04")
//				FREEZE_ENTITY_POSITION(Race.Racer[0].Vehicle, FALSE)
//				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[0].Vehicle)
//					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Race.Racer[0].Driver, Race.Racer[0].Vehicle, "offroad_cut_04", DRIVINGMODE_PLOUGHTHROUGH, 0, EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE EcoFriendly
//			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
//			AND GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_05")
//				FREEZE_ENTITY_POSITION(Race.Racer[0].Vehicle, FALSE)
//				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[0].Vehicle)
//					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Race.Racer[0].Driver, Race.Racer[0].Vehicle, "offroad_cut_05", DRIVINGMODE_PLOUGHTHROUGH, 0, EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
//				ENDIF
//			ENDIF
//			
//			IF NOT IS_ENTITY_DEAD(Race.Racer[5].Vehicle)
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[5], "SPROffroadCut")
//				FREEZE_ENTITY_POSITION(Race.Racer[5].Vehicle, FALSE)
//				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[5].Vehicle)
//					START_PLAYBACK_RECORDED_VEHICLE(Race.Racer[5].Vehicle, iCutRecording[5], "SPROffroadCut")
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE MinewardSpiral
//			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
//			AND GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_06")
//				FREEZE_ENTITY_POSITION(Race.Racer[0].Vehicle, FALSE)
//				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[0].Vehicle)
//					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Race.Racer[0].Driver, Race.Racer[0].Vehicle, "offroad_cut_06", DRIVINGMODE_PLOUGHTHROUGH, 0, EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
//				ENDIF
//			ENDIF
//			
//			IF NOT IS_ENTITY_DEAD(Race.Racer[5].Vehicle)
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[5], "SPROffroadCut")
//				FREEZE_ENTITY_POSITION(Race.Racer[5].Vehicle, FALSE)
//				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[5].Vehicle)
//					START_PLAYBACK_RECORDED_VEHICLE(Race.Racer[5].Vehicle, iCutRecording[5], "SPROffroadCut")
//					// skipping time in vehicle recording so he gets to the line before cutscene finishes
//					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(Race.Racer[5].Vehicle, 2000.0)					
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE ConstructionCourse
//			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
//			AND GET_IS_WAYPOINT_RECORDING_LOADED("offroad_cut_07")
//				FREEZE_ENTITY_POSITION(Race.Racer[0].Vehicle, FALSE)
//				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[0].Vehicle)
//					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Race.Racer[0].Driver, Race.Racer[0].Vehicle, "offroad_cut_07", DRIVINGMODE_PLOUGHTHROUGH, 0, EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
//				ENDIF
//			ENDIF
//			
//			IF NOT IS_ENTITY_DEAD(Race.Racer[5].Vehicle)
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCutRecording[5], "SPROffroadCut")
//				FREEZE_ENTITY_POSITION(Race.Racer[5].Vehicle, FALSE)
//				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[5].Vehicle)
//					START_PLAYBACK_RECORDED_VEHICLE(Race.Racer[5].Vehicle, iCutRecording[5], "SPROffroadCut")
//					// skipping time in vehicle recording so he gets to the line before cutscene finishes
//					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(Race.Racer[5].Vehicle, 2000.0)					
//				ENDIF
//			ENDIF
//		BREAK
//
//	ENDSWITCH
	
ENDPROC

//Initialize next stage in cutscene
PROC ORR_ADVANCE_CUTSCENE()
	RESTART_TIMER_NOW(cutsceneCameraOffroad)
	eSPRCutsceneState = INT_TO_ENUM(ORR_CUTSCENE_STATE, ENUM_TO_INT(eSPRCutsceneState)+1)
ENDPROC

PROC ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_STATE eCutState)
	RESTART_TIMER_NOW(cutsceneCameraOffroad)
	eSPRCutsceneState = eCutState
ENDPROC

PROC ORR_REMOVE_CUTSCENE_CAMS()
	IF DOES_CAM_EXIST(cam4swap)
		DESTROY_CAM(cam4swap)
	ENDIF
	IF DOES_CAM_EXIST(cam3swap)
		DESTROY_CAM(cam3swap)
	ENDIF
	IF DOES_CAM_EXIST(cam2swap)
		DESTROY_CAM(cam2swap)
	ENDIF
	IF DOES_CAM_EXIST(cam1swap)
		DESTROY_CAM(cam1swap)
	ENDIF
	IF DOES_CAM_EXIST(cam1wide)
		DESTROY_CAM(cam1wide)
	ENDIF
	IF DOES_CAM_EXIST(cam2wide)
		DESTROY_CAM(cam2wide)
	ENDIF
	
ENDPROC

PROC ORR_CLEANUP_CUTSCENE()
	CDEBUG1LN(DEBUG_OR_RACES, "Inside cleanup cutscene Proc")
	ORR_REMOVE_CUTSCENE_CAMS()
	
//	REMOVE_VEHICLE_RECORDING(iCutRecording[0], "SPROffroadCut")
//	REMOVE_VEHICLE_RECORDING(iCutRecording[1], "SPROffroadCut")
//	REMOVE_VEHICLE_RECORDING(iCutRecording[2], "SPROffroadCut")
//	REMOVE_VEHICLE_RECORDING(iCutRecording[3], "SPROffroadCut")
//	REMOVE_VEHICLE_RECORDING(iCutRecording[4], "SPROffroadCut")
//	
//	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)	
//		CASE CanyonCliffs
//			REMOVE_VEHICLE_RECORDING(iCutRecording[5], "SPROffroadCut")
//			REMOVE_WAYPOINT_RECORDING("offroad_cut_01")
//		BREAK
//		
//		CASE RidgeRun
//			REMOVE_WAYPOINT_RECORDING("offroad_cut_02")
//		BREAK
//		
//		CASE ValleyTrail
//			REMOVE_VEHICLE_RECORDING(iCutRecording[5], "SPROffroadCut")
//			REMOVE_WAYPOINT_RECORDING("offroad_cut_03")
//		BREAK
//		
//		CASE LakesideSplash
//			REMOVE_WAYPOINT_RECORDING("offroad_cut_04")
//		BREAK
//		
//		CASE EcoFriendly
//			REMOVE_VEHICLE_RECORDING(iCutRecording[5], "SPROffroadCut")
//			REMOVE_WAYPOINT_RECORDING("offroad_cut_05")
//		BREAK
//		
//		CASE MinewardSpiral
//			REMOVE_VEHICLE_RECORDING(iCutRecording[5], "SPROffroadCut")
//			REMOVE_WAYPOINT_RECORDING("offroad_cut_06")
//		BREAK
//		
//		CASE ConstructionCourse
//			REMOVE_VEHICLE_RECORDING(iCutRecording[5], "SPROffroadCut")
//			REMOVE_WAYPOINT_RECORDING("offroad_cut_07")
//		BREAK
//	ENDSWITCH
ENDPROC

PROC OR_CUTSCENES_REV_RACER_ENGINES(ORR_RACE_STRUCT& Race)
			
	INT waitTime, revTime
	INT i = 0
	SEQUENCE_INDEX seq
	REPEAT Race.iRacerCnt i
		IF DOES_ENTITY_EXIST(Race.Racer[i].Vehicle) AND DOES_ENTITY_EXIST(Race.Racer[i].Driver)
			IF IS_TIMER_STARTED(revEngineTimers[i]) 
				IF GET_TIMER_IN_SECONDS(revEngineTimers[i]) > racerUpdateWaitTime[i]
					IF IS_VEHICLE_DRIVEABLE(Race.Racer[i].Vehicle)
						waitTime = GET_RANDOM_INT_IN_RANGE(1, 500)
						revTime = GET_RANDOM_INT_IN_RANGE(500, 2500)
						OPEN_SEQUENCE_TASK(seq)
							TASK_VEHICLE_TEMP_ACTION(NULL, Race.Racer[i].Vehicle, TEMPACT_WAIT, waitTime)
							TASK_VEHICLE_TEMP_ACTION(NULL, Race.Racer[i].Vehicle, TEMPACT_REV_ENGINE, revTime)
						CLOSE_SEQUENCE_TASK(seq)
						IF NOT IS_PED_INJURED(Race.Racer[i].Driver)
							TASK_PERFORM_SEQUENCE(Race.Racer[i].Driver, seq)
						ENDIF
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
					RESTART_TIMER_NOW(revEngineTimers[i])
					racerUpdateWaitTime[i] = GET_RANDOM_FLOAT_IN_RANGE(0, 3)
				ENDIF
			ELSE
				START_TIMER_NOW(revEngineTimers[i])
			ENDIF
		ENDIF
	ENDREPEAT
	
	
ENDPROC

PROC PLAY_INTRO_TAUNT_DLG(ORR_RACE_STRUCT& Race)
	IF NOT IS_ENTITY_DEAD(Race.Racer[1].Driver)
		INT iSpeaker = GET_RANDOM_INT_IN_RANGE(1, 4)
		STRING sSpeaker, sRoot
		
		SWITCH iSpeaker
			CASE 1		
				sSpeaker = "MALE1"	
				sRoot	= "ORR_GUY1C"
			BREAK
			CASE 2		
				sSpeaker = "MALE2"		
				sRoot	= "ORR_GUY2C"
			BREAK
			CASE 3		
				sSpeaker = "MALE3"	
				sRoot	= "ORR_GUY3C"
			BREAK
		ENDSWITCH
		ADD_PED_FOR_DIALOGUE(myDialoguePeds, iSpeaker, Race.Racer[1].Driver, sSpeaker)
		
		CREATE_CONVERSATION(myDialoguePeds, "ORRAUD", sRoot, CONV_PRIORITY_HIGH)
	ENDIF
ENDPROC

FUNC BOOL Offroad_Cutscenes(ORR_RACE_STRUCT& Race)
	BOOL bDoTeles = TRUE
	BOOL bDoPlayTele = TRUE
	
	TEXT_LABEL_63 rec
	rec = "offroadrace"
		SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
			CASE OFFROAD_RACE_CANYON_CLIFFS
				rec += 1
			BREAK
			CASE OFFROAD_RACE_RIDGE_RUN
				rec += 2
			BREAK
			CASE OFFROAD_RACE_MINEWARD_SPIRAL
				rec += 6
			BREAK
			CASE OFFROAD_RACE_VALLEY_TRAIL
				rec += 3
			BREAK
			CASE OFFROAD_RACE_LAKESIDE_SPLASH
				rec += 4
			BREAK
			CASE OFFROAD_RACE_ECO_FRIENDLY
				rec += 5
			BREAK
		ENDSWITCH
	rec += "car"
	
	CPRINTLN( DEBUG_OR_RACES, "Offroad_Cutscenes" )
	
	IF eSPRCutsceneState > ORR_CUTSCENE_INTRO_TRANSITION_02 //don't allow skip while initializing
	AND eSPRCutsceneState < ORR_CUTSCENE_TRANSITION_05 //don't allow skip when either finishing or skipping
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			CDEBUG1LN(DEBUG_OR_RACES, "Offroad cutscene skipped, going to gameplay camera")		
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				STOP_SCRIPTED_CONVERSATION(FALSE)
			ENDIF
			KILL_ANY_CONVERSATION()
			CLEAR_PRINTS()
			FADE_DOWN()
			ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_SKIP)
			CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_CUTSCENE_SKIP")	
		ENDIF
	ENDIF
	
	// stop player vehicle if they get to the end of the waypoint recording
//	STOP_AFTER_WAYPOINT_RECORDING(Race)
	
//	OR_CUTSCENES_REV_RACER_ENGINES(Race)
	
	
	//load the necessary audio stream
	IF NOT bORStreamLoaded
		IF LOAD_STREAM("INTRO_STREAM", "DIRT_RACES_SOUNDSET")
			bORStreamLoaded = TRUE
		ENDIF
	ENDIF
	
	
	SWITCH eSPRCutsceneState
	
		CASE ORR_CUTSCENE_INIT
//			SCRIPT_ASSERT("ORR_CUTSCENE_INIT")
			CLEAR_PRINTS()
			ODDJOB_ENTER_CUTSCENE(DEFAULT,DEFAULT,FALSE)
			
			IF NOT IS_TIMER_STARTED(cutsceneCameraOffroad)
				START_TIMER_NOW(cutsceneCameraOffroad)
			ELSE
				RESTART_TIMER_NOW(cutsceneCameraOffroad)
			ENDIF

			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)	
			IF ORR_DOES_RACE_REQUIRE_MOTORCYCLE()
				SET_PED_HELMET(PLAYER_PED_ID(), TRUE)
				GIVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, TRUE)
			ENDIF
			
			
			
			// Switch if we are doing swap cutscene or regular cutscene
//			IF bMakeSwapCams
////				SCRIPT_ASSERT("setupSwapCutscene")
//				IF setupSwapCutscene()
//					IF DOES_CAM_EXIST(cam1swap)		
////						ORR_CLEAR_OFFROAD_RACE_PATH(Race)
//					
//						#IF OFFROAD_USE_SWAP_SCENES
//							ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_SWAP_TRANSITION_01)
//						#ENDIF
//						#IF NOT OFFROAD_USE_SWAP_SCENES
//							ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_TRANSITION_01)
//						#ENDIF
//						CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_CUTSCENE_SWAP_TRANSITION_01")						
//					ENDIF
//				ENDIF
////				SCRIPT_ASSERT("done performing setupSwapCutscene")
//			ELSE
//				ORR_CLEAR_OFFROAD_RACE_PATH(Race)

			ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_INTRO_TRANSITION)
			CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_CUTSCENE_TRANSITION_01")
		BREAK
		
		CASE ORR_CUTSCENE_INTRO_TRANSITION
			IF GET_TIMER_IN_SECONDS( cutsceneCameraOffroad ) >= 2.5
				ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_TRANSITION_01)
				RESTART_TIMER_NOW( cutsceneCameraOffroad )
				
				//ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
				//SET_CAM_ACTIVE_WITH_INTERP( cam1wide, camTransition2, 400, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR )
			ENDIF
		BREAK
		
		CASE ORR_CUTSCENE_INTRO_TRANSITION_02
			IF GET_TIMER_IN_SECONDS( cutsceneCameraOffroad ) >= 0.4
				ANIMPOSTFX_STOP("MinigameTransitionIn")
				ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_TRANSITION_01)
				RESTART_TIMER_NOW( cutsceneCameraOffroad )
			ENDIF
		BREAK
		
		CASE ORR_CUTSCENE_TRANSITION_01
//			SCRIPT_ASSERT("ORR_CUTSCENE_TRANSITION_01")
			IF DOES_CAM_EXIST(cam1wide)
				ORR_SAVE_NEARBY_VEHICLES( Race )
				ORR_CLEAR_OFFROAD_RACE_PATH( Race )
			
				RemoveDonorPed()
				ActivatePlayerVehicleTeleport(Race)
//				SET_CAM_ACTIVE_WITH_INTERP(cam2wide, cam1wide, iCutInterpDur, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
				SET_CAM_ACTIVE_WITH_INTERP(cam2wide, cam1wide, ROUND(Race.fIntroCam1Time * 1000), GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				//RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				//WAIT(0)
				//RENDER_SCRIPT_CAMS(FALSE, TRUE, iCutInterpDur)
				#IF IS_DEBUG_BUILD
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)
				#ENDIF

				start_vehicle_recordings(Race)
				
				
				CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_CUTSCENE_TRANSITION_02")
				
				IF bORStreamLoaded
					PLAY_STREAM_FRONTEND()
				ELSE
					CERRORLN(DEBUG_OR_RACES, "INTRO_STREAM NOT LOADED!! UNABLE TO PLAY STREAM")
				ENDIF
				
				ORR_ADVANCE_CUTSCENE()
			ELSE	
				ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_SKIP)
				CDEBUG1LN(DEBUG_OR_RACES, "Camera didn't exist, Moving to ORR_CUTSCENE_SKIP")	
			ENDIF
		BREAK
		
		CASE ORR_CUTSCENE_TRANSITION_02
			// only do convesation stuff once if bit isnt set
			IF NOT IS_BITMASK_AS_ENUM_SET(iCutDialogue, DialogueBit4)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PLAY_INTRO_TAUNT_DLG(Race)
				SET_BITMASK_AS_ENUM(iCutDialogue, DialogueBit4)
			ENDIF	
			
			IF NOT Race.bCutsceneTasked1
				AND GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= Race.fStartTime[0]
				IF NOT IS_ENTITY_DEAD( Race.Racer[1].Driver )
					AND IS_PED_IN_ANY_VEHICLE( Race.Racer[1].Driver )
					AND NOT IS_ENTITY_DEAD(Race.Racer[1].Vehicle)
					CPRINTLN( DEBUG_OR_RACES, "PED IS IN VEH, TASKING")
					TASK_VEHICLE_DRIVE_TO_COORD( Race.Racer[1].Driver, Race.Racer[1].Vehicle, Race.vDriveTo[1], Race.fIntroSpeed[0], DRIVINGSTYLE_ACCURATE, Race.Racer[1].eVehicleModel, DRIVINGMODE_PLOUGHTHROUGH, 0.1, 5.0)
					//START_PLAYBACK_RECORDED_VEHICLE( Race.Racer[1].Vehicle, 1, rec )
					Race.bCutsceneTasked1 = TRUE
					
				ENDIF

			ENDIF
			
			IF NOT Race.bCutsceneTasked2
				AND GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= Race.fStartTime[1]
				IF NOT IS_ENTITY_DEAD( Race.Racer[2].Driver )
					AND IS_PED_IN_ANY_VEHICLE( Race.Racer[2].Driver )
					AND NOT IS_ENTITY_DEAD( Race.Racer[2].Vehicle )
					TASK_VEHICLE_DRIVE_TO_COORD( Race.Racer[2].Driver, Race.Racer[2].Vehicle, Race.vDriveTo[2], Race.fIntroSpeed[1], DRIVINGSTYLE_ACCURATE, Race.Racer[2].eVehicleModel, DRIVINGMODE_PLOUGHTHROUGH, 0.1, 5.0)
					//START_PLAYBACK_RECORDED_VEHICLE( Race.Racer[2].Vehicle, 2, rec )
					Race.bCutsceneTasked2 = TRUE
				
				ENDIF		
				
			ENDIF
			
			IF GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= Race.fIntroCam1Time - 1.3
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED( 1, rec )
				AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE( Race.Racer[0].Vehicle )
				IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
					START_PLAYBACK_RECORDED_VEHICLE_USING_AI( Race.Racer[0].Vehicle, 1, rec, DEFAULT, DRIVINGMODE_PLOUGHTHROUGH )
					//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE( Race.Racer[0].Vehicle, Race.fRecordingStartTime )
					//SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE
					//TASK_VEHICLE_DRIVE_TO_COORD( PLAYER_PED_ID(), Race.Racer[0].Vehicle, Race.vDriveTo[0], 4.0, DRIVINGSTYLE_ACCURATE, Race.Racer[0].eVehicleModel, DRIVINGMODE_PLOUGHTHROUGH, 0.1, 0.0)
				ENDIF
			ENDIF
			IF (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= Race.fIntroCam1Time)
				IF DOES_ENTITY_EXIST(Race.Racer[0].Vehicle)
				AND NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
					SET_VEHICLE_RADIO_ENABLED(Race.Racer[0].Vehicle, TRUE)
					SET_CUSTOM_RADIO_TRACK_LIST("RADIO_01_CLASS_ROCK", "OFF_ROAD_RADIO_ROCK_LIST", TRUE)
					CPRINTLN(DEBUG_OR_RACES, "Setting radio correctly")
					SET_VEH_RADIO_STATION(Race.Racer[0].Vehicle, "RADIO_01_CLASS_ROCK") 
				ENDIF
				
				SET_CAM_ACTIVE_WITH_INTERP( cam4wide, cam3wide, ROUND(Race.fIntroCam2Time * 1000), GRAPH_TYPE_LINEAR)//, GRAPH_TYPE_LINEAR )
				//SET UP CAM INIT for race girl shot
//				IF NOT IS_CAM_ACTIVE(cam2wide)
//					SET_CAM_ACTIVE(cam2wide, TRUE)
//				ENDIF
				
//				IF IS_CAM_ACTIVE(cam1wide)
//					IF ARE_VECTORS_ALMOST_EQUAL(GET_CAM_COORD(cam2wide), vCutsceneCamDest, 5)

//						RENDER_SCRIPT_CAMS(FALSE, TRUE)
//					ENDIF
//				ENDIF

				ORR_ADVANCE_CUTSCENE()
				
//				PLAY_SOUND_FRONTEND(-1, "INTRO", "DIRT_RACES_SOUNDSET")
				
				START_TIMER_NOW(cutsceneCameraOffroad)			

				PRINTLN("PLAYING INTRO SOUNDS!!!")
				// tune differently based on race
//				SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
//					
//					CASE CanyonCliffs
//						IF (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= 1.0)
//							CDEBUG1LN(DEBUG_OR_RACES, "CanyonCliffs: Moving to ORR_CUTSCENE_TRANSITION_04")	
//							ORR_ADVANCE_CUTSCENE()
//							PLAY_SOUND_FRONTEND(-1, "INTRO", "DIRT_RACES_SOUNDSET")
//						ENDIF
//					BREAK
//					CASE RidgeRun
//						IF (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= 1.0)
//							CDEBUG1LN(DEBUG_OR_RACES, "RidgeRun: Moving to ORR_CUTSCENE_TRANSITION_04")	
//							ORR_ADVANCE_CUTSCENE()
//							PLAY_SOUND_FRONTEND(-1, "INTRO", "DIRT_RACES_SOUNDSET")
//						ENDIF
//					BREAK
//					CASE ValleyTrail
//						IF (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= 1.0)
//							CDEBUG1LN(DEBUG_OR_RACES, "ValleyTrail: Moving to ORR_CUTSCENE_TRANSITION_04")	
//							ORR_ADVANCE_CUTSCENE()
//							PLAY_SOUND_FRONTEND(-1, "INTRO", "DIRT_RACES_SOUNDSET")
//						ENDIF
//					BREAK
//					CASE LakesideSplash
//						IF (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= 1.0)
//							CDEBUG1LN(DEBUG_OR_RACES, "LakesideSplash: Moving to ORR_CUTSCENE_TRANSITION_04")	
//							ORR_ADVANCE_CUTSCENE()
//							PLAY_SOUND_FRONTEND(-1, "INTRO", "DIRT_RACES_SOUNDSET")
//						ENDIF
//					BREAK
//					CASE EcoFriendly
//						IF (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= 1.0)
//							CDEBUG1LN(DEBUG_OR_RACES, "EcoFriendly: Moving to ORR_CUTSCENE_TRANSITION_04")	
//							ORR_ADVANCE_CUTSCENE()
//							PLAY_SOUND_FRONTEND(-1, "INTRO", "DIRT_RACES_SOUNDSET")
//						ENDIF
//					BREAK
//					CASE MinewardSpiral
//						IF (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= 1.0)
//							CDEBUG1LN(DEBUG_OR_RACES, "MinewardSpiral: Moving to ORR_CUTSCENE_TRANSITION_04")	
//							ORR_ADVANCE_CUTSCENE()
//							PLAY_SOUND_FRONTEND(-1, "INTRO", "DIRT_RACES_SOUNDSET")
//						ENDIF
//					BREAK
//					CASE ConstructionCourse
//						IF (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= 1.0)
//							CDEBUG1LN(DEBUG_OR_RACES, "ConstructionCourse: Moving to ORR_CUTSCENE_TRANSITION_04")	
//							ORR_ADVANCE_CUTSCENE()
//							PLAY_SOUND_FRONTEND(-1, "INTRO", "DIRT_RACES_SOUNDSET")
//						ENDIF
//					BREAK
//				ENDSWITCH
			ENDIF
		BREAK
			
		CASE ORR_CUTSCENE_TRANSITION_04
						
			#IF IS_DEBUG_BUILD
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
			#ENDIF
			
			IF GET_TIMER_IN_SECONDS( cutsceneCameraOffroad ) >= Race.fIntroCam2Time

				RESTART_TIMER_NOW( cutsceneCameraOffroad )
				STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
				SET_CAM_ACTIVE_WITH_INTERP( cam6wide, cam5wide, ROUND(Race.fIntroCam3Time * 1000), GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR )
				ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_TRANSITION_05)
				CANCEL_TIMER(Race.Display.tCnt)
			
				eCountdownStage = ORR_COUNTDOWN_STAGE_INIT
			ELIF GET_TIMER_IN_SECONDS( cutsceneCameraOffroad ) >= Race.fIntroCam2Time - 0.2
				IF NOT IS_ENTITY_DEAD( Race.Racer[0].Vehicle )
					STOP_PLAYBACK_RECORDED_VEHICLE( Race.Racer[0].Vehicle )
				ENDIF
			ENDIF
		BREAK		
		
		CASE ORR_CUTSCENE_TRANSITION_05
			IF GET_TIMER_IN_SECONDS( cutsceneCameraOffroad ) >= Race.fIntroCam3Time
				bCamerasMade = FALSE
				bSwapCamsMade = FALSE
				bMakeSwapCams = FALSE
				//RENDER_SCRIPT_CAMS(FALSE, TRUE)
				//IF (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= 1.0)				
				CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_CUTSCENE_FINISH")	
				ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_FINISH)
				//ENDIF		
			
			ENDIF
			
			IF GET_TIMER_IN_SECONDS( cutsceneCameraOffroad ) >= 1.0
				ORR_Countdown_Display(Race.Display)
			ENDIF
		BREAK
		
		CASE ORR_CUTSCENE_FINISH
			IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				CPRINTLN(DEBUG_OR_RACES, "Resetting cam every frame")
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			
			IF IS_SCREEN_FADED_IN()				
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					CPRINTLN(DEBUG_OR_RACES, "Resetting cam again")
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
				
				CLEAR_BITMASK_AS_ENUM(iORCutsceneBits, CutsceneBit1)	
				CLEAR_BITMASK_AS_ENUM(iCutDialogue, DialogueBit1)
				CLEAR_BITMASK_AS_ENUM(iCutDialogue, DialogueBit2)
				CLEAR_BITMASK_AS_ENUM(iCutDialogue, DialogueBit3)
				CLEAR_BITMASK_AS_ENUM(iCutDialogue, DialogueBit4)
				ORR_CLEANUP_CUTSCENE()
				CDEBUG1LN(DEBUG_OR_RACES, "Cutscene Complete")	
				bDoTeles = TRUE
				bDoPlayTele = TRUE
				ODDJOB_EXIT_CUTSCENE()
//				STOP_STREAM()
				
				RETURN TRUE
			ENDIF
		BREAK			
		
		CASE ORR_CUTSCENE_SKIP
			IF IS_SCREEN_FADED_OUT()					
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				RemoveDonorPed()
				ActivatePlayerVehicleTeleport(Race)
				
				#IF IS_DEBUG_BUILD
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
				#ENDIF
								
				IF (TELEPORT_CUTSCENE_VEHICLE_TO_END_POS(Race, bDoTeles)
				AND TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS(Race, bDoPlayTele, DEFAULT, TRUE))
					CPRINTLN(DEBUG_OR_RACES, "Resetting cam")
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					CDEBUG1LN(DEBUG_OR_RACES, "Racers teleported, fading in")
					FADE_UP()	
					ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_FINISH)
					CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_CUTSCENE_FINISH")	
				ENDIF
				
				
			ELSE
				IF NOT DOES_CAM_EXIST(cam1wide) OR NOT (OFFROAD_USE_SWAP_SCENES = 1)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_CUTSCENE_FINISH")	
					ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_FINISH)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(Race.Racer[0].Vehicle)
			AND NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
				SET_CUSTOM_RADIO_TRACK_LIST("RADIO_01_CLASS_ROCK", "OFF_ROAD_RADIO_ROCK_LIST", TRUE)
				SET_VEHICLE_RADIO_ENABLED(Race.Racer[0].Vehicle, TRUE)
				SET_VEH_RADIO_STATION(Race.Racer[0].Vehicle, "RADIO_01_CLASS_ROCK") 
			ENDIF
//			STOP_STREAM()
			Race.bCutsceneSkipped = TRUE
		BREAK	
		
		CASE ORR_CUTSCENE_SWAP_TRANSITION_01
			IF DOES_CAM_EXIST(cam1swap) AND OFFROAD_USE_SWAP_SCENES = 1
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_CUTSCENE_SWAP_TRANSITION_01: Swap cutscene started")
				SET_CAM_ACTIVE_WITH_INTERP(cam2swap, cam1swap, 3000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
				#IF IS_DEBUG_BUILD
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)
				#ENDIF
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				AND NOT IS_ENTITY_DEAD(donorPed)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), donorPed, -1)
					TASK_LOOK_AT_ENTITY(donorPed, PLAYER_PED_ID(), -1)
				ENDIF
					
				IF NOT IS_BITMASK_AS_ENUM_SET(iCutDialogue, DialogueBit1)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CDEBUG1LN(DEBUG_OR_RACES, "No conversation running, playing swap line 1")
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							ADD_PED_FOR_DIALOGUE(myDialoguePeds, 0, PLAYER_PED_ID(), "TREVOR")
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							ADD_PED_FOR_DIALOGUE(myDialoguePeds, 0, PLAYER_PED_ID(), "MICHAEL")
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							ADD_PED_FOR_DIALOGUE(myDialoguePeds, 0, PLAYER_PED_ID(), "FRANKLIN")
						ENDIF
						ADD_PED_FOR_DIALOGUE(myDialoguePeds, 1, donorPed, "OR_Swap")
						IF NOT IS_ENTITY_DEAD(playerVeh)
							PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialoguePeds, "SPR_OR", "OR_Cut01", "OR_Cut01_1", CONV_PRIORITY_HIGH)
							SET_BITMASK_AS_ENUM(iCutDialogue, DialogueBit1)
						ELSE
							PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialoguePeds, "SPR_OR", "OR_Cut02", "OR_Cut02_1", CONV_PRIORITY_HIGH)
							SET_BITMASK_AS_ENUM(iCutDialogue, DialogueBit1)
						ENDIF
						CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_CUTSCENE_SWAP_TRANSITION_02")
						ORR_ADVANCE_CUTSCENE()	
					ENDIF
				ENDIF
			ELSE		
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_CUTSCENE_SWAP_TRANSITION_01: SKIPPING, cam1swap not made")
				FADE_DOWN()
				#IF NOT OFFROAD_USE_SWAP_SCENES
					ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_SWAP_TRANSITION_04)
				#ENDIF
				#IF OFFROAD_USE_SWAP_SCENES
					ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_SKIP)
				#ENDIF
			
				
				CDEBUG1LN(DEBUG_OR_RACES, "Camera didn't exist, Moving to ORR_CUTSCENE_SKIP")	
			ENDIF		
		BREAK
		
		CASE ORR_CUTSCENE_SWAP_TRANSITION_02
				IF DOES_CAM_EXIST(cam2swap)	AND OFFROAD_USE_SWAP_SCENES	= 1
					SET_CAM_ACTIVE(cam2swap, TRUE)
					#IF IS_DEBUG_BUILD
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
					#ENDIF
					
//					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					AND (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= 3.0)
//						IF NOT IS_BITMASK_AS_ENUM_SET(iCutDialogue, DialogueBit2)
//							CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_CUTSCENE_SWAP_TRANSITION_03")	
//							RESTART_TIMER_NOW(cutsceneCameraOffroad)
//							CDEBUG1LN(DEBUG_OR_RACES, "No conversation running, playing swap line 2")		
//							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//								ADD_PED_FOR_DIALOGUE(myDialoguePeds, 0, PLAYER_PED_ID(), "TREVOR")
//							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//								ADD_PED_FOR_DIALOGUE(myDialoguePeds, 0, PLAYER_PED_ID(), "MICHAEL")
//							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//								ADD_PED_FOR_DIALOGUE(myDialoguePeds, 0, PLAYER_PED_ID(), "FRANKLIN")
//							ENDIF
//							ADD_PED_FOR_DIALOGUE(myDialoguePeds, 1, donorPed, "OR_Swap")
//							IF NOT IS_ENTITY_DEAD(playerVeh)
//								If GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//									PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialoguePeds, "SPR_OR", "OR_Cut01", "OR_Cut01_2", CONV_PRIORITY_HIGH)
//									SET_BITMASK_AS_ENUM(iCutDialogue, DialogueBit2)
//								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//									PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialoguePeds, "SPR_OR", "OR_Cut01", "OR_Cut01_3", CONV_PRIORITY_HIGH)
//									SET_BITMASK_AS_ENUM(iCutDialogue, DialogueBit2)
//								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//									PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialoguePeds, "SPR_OR", "OR_Cut01", "OR_Cut01_4", CONV_PRIORITY_HIGH)
//									SET_BITMASK_AS_ENUM(iCutDialogue, DialogueBit2)
//								ENDIF
//							ELSE
//								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//									PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialoguePeds, "SPR_OR", "OR_Cut02", "OR_Cut02_2", CONV_PRIORITY_HIGH)
//									SET_BITMASK_AS_ENUM(iCutDialogue, DialogueBit2)
//								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//									PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialoguePeds, "SPR_OR", "OR_Cut02", "OR_Cut02_3", CONV_PRIORITY_HIGH)
//									SET_BITMASK_AS_ENUM(iCutDialogue, DialogueBit2)
//								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//									PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialoguePeds, "SPR_OR", "OR_Cut02", "OR_Cut02_4", CONV_PRIORITY_HIGH)
//									SET_BITMASK_AS_ENUM(iCutDialogue, DialogueBit2)
//								ENDIF
//							ENDIF
//						ENDIF
//						ORR_ADVANCE_CUTSCENE()
//					ENDIF
				ELSE				
					FADE_DOWN()
					ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_SKIP)
					CDEBUG1LN(DEBUG_OR_RACES, "Camera didn't exist, Moving to ORR_CUTSCENE_SKIP")	
				ENDIF
		BREAK
		
		CASE ORR_CUTSCENE_SWAP_TRANSITION_03			
				IF DOES_CAM_EXIST(cam3swap) AND OFFROAD_USE_SWAP_SCENES	= 1
					SET_CAM_ACTIVE(cam3swap, TRUE)					
					
					IF (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= 1.0)
						CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_CUTSCENE_SWAP_TRANSITION_04")	
//						PLAY_SOUND_FRONTEND(iIntroSFXIDX, "INTRO", "DIRT_RACES_SOUNDSET")
						RESTART_TIMER_NOW(cutsceneCameraOffroad)
						ORR_ADVANCE_CUTSCENE()
					ENDIF
				ELSE				
					FADE_DOWN()
					ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_SKIP)
					CDEBUG1LN(DEBUG_OR_RACES, "Camera didn't exist, Moving to ORR_CUTSCENE_SKIP")	
				ENDIF		
		BREAK
		
		CASE ORR_CUTSCENE_SWAP_TRANSITION_04
			IF (GET_TIMER_IN_SECONDS(cutsceneCameraOffroad) >= 3.0)	OR NOT (OFFROAD_USE_SWAP_SCENES = 1)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				AND NOT IS_ENTITY_DEAD(donorPed)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					CLEAR_PED_TASKS(donorPed)
					
				ENDIF
				
//				IF DOES_ENTITY_EXIST(ORR_Master.savedVeh)
//					SET_ENTITY_VISIBLE(ORR_Master.savedVeh, FALSE)
//					SET_ENTITY_COLLISION(ORR_Master.savedVeh, FALSE)
//				ENDIF
				cam4swap = cam4swap
				ActivatePlayerVehicleTeleport(Race)
				makeCutsceneCams()
				CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_CUTSCENE_TRANSITION_01")	
				RESTART_TIMER_NOW(cutsceneCameraOffroad)
				ORR_SET_CUTSCENE_STATE(ORR_CUTSCENE_TRANSITION_01)
			ENDIF		
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC


// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!
