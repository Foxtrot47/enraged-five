// Offroad_Race_Scorecard_lib.sch
USING "screens_header.sch"
USING "hud_drawing.sch"
USING "screen_placements.sch"
USING "screen_placements_export.sch"
USING "socialclub_leaderboard.sch"
USING "orr_race_scorecard.sch"
USING "orr_sc_leaderboard_lib.sch"


USING "UIUtil.sch"

BOOL bBestTimesReady = FALSE
INT iRaceOnlineID
/// PURPOSE:
///    Sets up the race's scorecard
PROC ORR_Init_Scorecard(ORR_RACE_STRUCT &Race)
	RESET_ALL_SPRITE_PLACEMENT_VALUES(Race.uiScorecard.SpritePlacement)
	SET_STANDARD_INGAME_TEXT_DETAILS(Race.uiScorecard.aStyle)
	
	INIT_SCREEN_OFFROAD_MENU(Race.uiScorecard)
	
ENDPROC

PROC GET_RACE_BEST_TIMES(OFFROAD_RACE_INDEX raceIndex)
	IF NOT bBestTimesReady AND IS_PLAYER_ONLINE() AND (NETWORK_HAVE_ONLINE_PRIVILEGES() OR NOT NETWORK_HAS_AGE_RESTRICTIONS())
		SWITCH raceIndex
			CASE OFFROAD_RACE_CANYON_CLIFFS
				iRaceOnlineID = 100
			BREAK
			CASE OFFROAD_RACE_RIDGE_RUN
				iRaceOnlineID = 101
			BREAK
			CASE OFFROAD_RACE_VALLEY_TRAIL
				iRaceOnlineID = 102
			BREAK
			CASE OFFROAD_RACE_LAKESIDE_SPLASH
				iRaceOnlineID = 103
			BREAK
			CASE OFFROAD_RACE_ECO_FRIENDLY
				iRaceOnlineID = 104
			BREAK
			CASE OFFROAD_RACE_MINEWARD_SPIRAL
				iRaceOnlineID = 105
			BREAK
		ENDSWITCH
		
		bBestTimesReady = GET_SP_RACE_PERSONAL_GLOBAL_BEST(ORR_Master.iReadStage, ORR_Master.iLoadStage, ORR_Master.bSuccessful, iRaceOnlineID, ORR_Master.iGlobalBest, ORR_Master.iPersonalBest)
	ENDIF
ENDPROC

PROC ORR_SETUP_SCORECARD(ORR_RACE_STRUCT &Race)
	RESET_ENDSCREEN(Race.esd)
	
	END_SCREEN_MEDAL_STATUS eMedalStatus = ESMS_NO_MEDAL
	STRING sPositionLabel = ""
	IF Race.Racer[0].iRank = 1
		eMedalStatus = ESMS_GOLD
		sPositionLabel = "SPR_1stpl"
	ELIF Race.Racer[0].iRank = 2
		eMedalStatus = ESMS_SILVER
		sPositionLabel = "SPR_2ndpl"
	ELIF Race.Racer[0].iRank = 3
		eMedalStatus = ESMS_BRONZE
		sPositionLabel = "SPR_3rdpl"
	ELIF Race.Racer[0].iRank = 4
		eMedalStatus = ESMS_NO_MEDAL
		sPositionLabel = "SPR_4thpl"
	ELIF Race.Racer[0].iRank = 5
		eMedalStatus = ESMS_NO_MEDAL
		sPositionLabel = "SPR_5thpl"
	ELIF Race.Racer[0].iRank = 6
		eMedalStatus = ESMS_NO_MEDAL
		sPositionLabel = "SPR_6thpl"
	ENDIF
	
	CPRINTLN( DEBUG_OR_RACES, "Final clock time is ", FLOOR(Race.Racer[0].fClockTime*1000) )
	SET_ENDSCREEN_DATASET_HEADER(Race.esd, sPositionLabel, ORR_Master.szRaceName[ORR_Master.iRaceCur])
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(Race.esd, ESEF_TIME_M_S_MS_WITH_PERIOD, "SPR_TIME", "", FLOOR(Race.Racer[0].fClockTime*1000), 0, ESCM_NO_MARK)
	
	IF bBestTimesReady AND IS_PLAYER_ONLINE() AND (NETWORK_HAVE_ONLINE_PRIVILEGES() OR NOT NETWORK_HAS_AGE_RESTRICTIONS())
		/*WHILE NOT GET_SP_RACE_PERSONAL_GLOBAL_BEST(iReadStage, iLoadStage, bSuccessful, iRaceOnlineID, iGlobalBest, iPersonalBest)
			WAIT(0)
		ENDWHILE*/
		
		IF ORR_Master.iPersonalBest > FLOOR(Race.Racer[0].fClockTime*1000) OR ORR_Master.iPersonalBest <= 0
			ORR_Master.iPersonalBest = FLOOR(Race.Racer[0].fClockTime*1000)
		ENDIF
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(Race.esd, ESEF_TIME_M_S_MS_WITH_PERIOD, "SPR_BESTTIME", "", ORR_Master.iPersonalBest, 0, ESCM_NO_MARK)
		
		IF ORR_Master.iGlobalBest > FLOOR(Race.Racer[0].fClockTime*1000) OR ORR_Master.iGlobalBest <= 0
			ORR_Master.iGlobalBest = FLOOR(Race.Racer[0].fClockTime*1000)
		ENDIF
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(Race.esd, ESEF_TIME_M_S_MS_WITH_PERIOD, "LOB_SPLIT_0", "", ORR_Master.iGlobalBest, 0, ESCM_NO_MARK)
	ENDIF
	
	//ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(Race.esd, ESEF_TIME_M_S_MS_WITH_PERIOD, "SPR_BESTTIME", "", FLOOR(g_savedGlobals.sOffroadData.fBestTime[ORR_Master.iRaceCur]*1000), 0, ESCM_NO_MARK)	
	//ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(Race.esd, ESEF_TIME_M_S_MS_WITH_PERIOD, "LOB_SPLIT_0", "", FLOOR(g_savedGlobals.sOffroadData.fBestTime[ORR_Master.iRaceCur]*1000), 0, ESCM_NO_MARK)
	
//	IF Race.Racer[0].iRank < 4
		SET_ENDSCREEN_COMPLETION_LINE_STATE(Race.esd, TRUE, "SPR_RESULT", Race.Racer[0].iRank, Race.iRacerCnt, ESC_FRACTION_COMPLETION, eMedalStatus)
//	ENDIF
	ENDSCREEN_PREPARE(Race.esd)
ENDPROC

PROC ORR_SET_SCORECARD_FINISH(ORR_RACE_STRUCT &Race, INT TimeInMs = 300)
	Race.esd.iEndScreenDisplayFinish = Race.esd.IGameTimerReplacement+TimeInMs
	Race.esd.bHoldOnEnd = FALSE
ENDPROC

/// PURPOSE:
///    Draws the scorecard for the Offroad Race.
FUNC BOOL ORR_NEW_DRAW_SCORECARD(ORR_RACE_STRUCT &Race)
	RETURN RENDER_ENDSCREEN(Race.esd)
ENDFUNC


/// PURPOSE:
///    Draws the scorecard for the Offroad Race.
PROC ORR_Draw_Scorecard(ORR_RACE_STRUCT &Race, INT iPlayerIndex)	
	
	INT iMins, iSecs
	
	// Draw the BG
	DRAW_2D_SPRITE("Shared", "BGGradient_32x1024", Race.uiScorecard.SpritePlacement[OFFROAD_SC_BACKGROUND], FALSE)
	
	// Draw the title
//	SET_TEXT_CURSIVE(Race.uiScorecard.aStyle.TS_TITLE)
	DRAW_TEXT(Race.uiScorecard.TextPlacement[OFFROAD_SC_MENU_TITLE], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_TITLE, "OFFR_TITLE")
//	SET_TEXT_STANDARD(Race.uiScorecard.aStyle.TS_TITLE)

	// Draw the "Race stats" header
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[OFFROAD_SC_STATS_HEADER_BG])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[OFFROAD_SC_STATS_EDGE])
	SET_TEXT_BLACK(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_PRIMARY)
	DRAW_TEXT(Race.uiScorecard.TextPlacement[OFFROAD_SC_STATS_TITLE], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_PRIMARY, "SPR_RESULTS")
	
	// Draw the big box that will have a race picture.
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[OFFROAD_SC_RACE_IMG_BG])
//	DRAW_2D_SPRITE("MPHUD", "MP_Lock_Icon", Race.uiScorecard.SpritePlacement[OFFROAD_SC_RACE_IMG])
	SET_TEXT_WHITE(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_PRIMARY)
	
	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
		CASE CanyonCliffs
			DRAW_2D_SPRITE("SPROffroad", "SPR_Offroad_1", Race.uiScorecard.SpritePlacement[OFFROAD_SC_RACE_IMG])	
		BREAK
		CASE RidgeRun
			DRAW_2D_SPRITE("SPROffroad", "SPR_Offroad_2", Race.uiScorecard.SpritePlacement[OFFROAD_SC_RACE_IMG])	
		BREAK
		CASE ValleyTrail
			DRAW_2D_SPRITE("SPROffroad", "SPR_Offroad_3", Race.uiScorecard.SpritePlacement[OFFROAD_SC_RACE_IMG])	
		BREAK
		CASE LakesideSplash
			DRAW_2D_SPRITE("SPROffroad", "SPR_Offroad_4", Race.uiScorecard.SpritePlacement[OFFROAD_SC_RACE_IMG])	
		BREAK
		CASE EcoFriendly
			DRAW_2D_SPRITE("SPROffroad", "SPR_Offroad_5", Race.uiScorecard.SpritePlacement[OFFROAD_SC_RACE_IMG])	
		BREAK
		CASE MinewardSpiral
			DRAW_2D_SPRITE("SPROffroad", "SPR_Offroad_6", Race.uiScorecard.SpritePlacement[OFFROAD_SC_RACE_IMG])	
		BREAK
	ENDSWITCH
//	TEXT_LABEL sRaceImage = "SPR_Offroad_"
//	sRaceImage += ((ORR_Master.iRaceCur)+1)
	// race images vv
//	DRAW_2D_SPRITE("SPROffroad", sRaceImage, Race.uiScorecard.SpritePlacement[OFFROAD_SC_RACE_IMG])	
	
	// Draw the 8 stat boxes (only draw 7, do to space filler)
	INT index
	REPEAT 7 index
		OFFROAD_SCREEN_RECT eCurrentRect = OFFROAD_SC_STAT1_BG + INT_TO_ENUM(OFFROAD_SCREEN_RECT, index)
		DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[eCurrentRect])
	ENDREPEAT
	
	// Fill the 8 stat boxes with.. well.. stats.
	// Medal Earned
	STRING sText
	IF (Race.Racer[iPlayerIndex].iRank = 1)
		sText = "SPR_GOLD_MED"
		SET_TEXT_GOLD(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
		SET_SPRITE_HUD_COLOUR(Race.uiScorecard.SpritePlacement[OFFROAD_SC_MEDAL_AWARD_IMG], HUD_COLOUR_GOLD)
		DRAW_2D_SPRITE("Shared", "MedalDot_32", Race.uiScorecard.SpritePlacement[OFFROAD_SC_MEDAL_AWARD_IMG])
	ELIF (Race.Racer[iPlayerIndex].iRank = 2)
		sText = "SPR_SILVER_MED"
		SET_TEXT_SILVER(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
		SET_SPRITE_HUD_COLOUR(Race.uiScorecard.SpritePlacement[OFFROAD_SC_MEDAL_AWARD_IMG], HUD_COLOUR_SILVER)
		DRAW_2D_SPRITE("Shared", "MedalDot_32", Race.uiScorecard.SpritePlacement[OFFROAD_SC_MEDAL_AWARD_IMG])
	ELIF (Race.Racer[iPlayerIndex].iRank = 3)
		sText = "SPR_BRONZE_MED"
		SET_TEXT_BRONZE(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
		SET_SPRITE_HUD_COLOUR(Race.uiScorecard.SpritePlacement[OFFROAD_SC_MEDAL_AWARD_IMG], HUD_COLOUR_BRONZE)
		DRAW_2D_SPRITE("Shared", "MedalDot_32", Race.uiScorecard.SpritePlacement[OFFROAD_SC_MEDAL_AWARD_IMG])
	ELSE
		sText = "SC_LB_EMPTY"
		SET_TEXT_DARK_GREY(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
		SET_SPRITE_DARK_GREY(Race.uiScorecard.SpritePlacement[OFFROAD_SC_MEDAL_AWARD_IMG])
		DRAW_2D_SPRITE("Shared", "EmptyDot_32", Race.uiScorecard.SpritePlacement[OFFROAD_SC_MEDAL_AWARD_IMG])
	ENDIF
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[OFFROAD_SC_STAT1_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT+PIXEL_X_TO_FLOAT(22))
	DRAW_TEXT_WITH_ALIGNMENT(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT1VAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, sText, FALSE, TRUE)
	
	SET_TEXT_WHITE(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[OFFROAD_SC_STAT1_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
	DRAW_TEXT(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT1_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_MEDALEARN")

	// Race name
	DRAW_TEXT(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT2_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_RACENAME")
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[OFFROAD_SC_STAT2_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
	DRAW_TEXT_WITH_ALIGNMENT(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT2VAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, ORR_Master.szRaceName[ORR_Master.iRaceCur], FALSE, TRUE)

	// Position
	DRAW_TEXT(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT3_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_POSITION")
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[OFFROAD_SC_STAT3_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
	DRAW_TEXT_WITH_2_NUMBERS(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT3VAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_POS", Race.Racer[iPlayerIndex].iRank, Race.iRacerCnt, FONT_RIGHT)
	
	// Time
	DRAW_TEXT(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT4_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_TIME")
	iMins = ROUND(Race.Racer[iPlayerIndex].fClockTime) / 60
	iSecs = FLOOR(Race.Racer[iPlayerIndex].fClockTime - (TO_FLOAT(iMins) * 60.0))
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[OFFROAD_SC_STAT4_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
	IF (iSecs < 10)
		DRAW_TEXT_WITH_2_NUMBERS(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT4VAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_TIMEVAL_WZ", iMins, iSecs, FONT_RIGHT)
	ELSE
		DRAW_TEXT_WITH_2_NUMBERS(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT4VAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_TIMEVAL", iMins, iSecs, FONT_RIGHT)
	ENDIF
	
	// Vehicle Used
	DRAW_TEXT(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT5_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_VEHUSED")
	sText = "SPR_UNKNOWN"
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[OFFROAD_SC_STAT5_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		sText = GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(Race.Racer[iPlayerIndex].Vehicle))
	ENDIF
	DRAW_TEXT_WITH_ALIGNMENT(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT5VAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, sText, FALSE, TRUE)
	
	// Best Time
	IF g_savedGlobals.sOffroadData.fBestTime[ORR_Master.iRaceCur] >= 0
		DRAW_TEXT(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT6_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_BESTTIME")
		iMins = ROUND(g_savedGlobals.sOffroadData.fBestTime[ORR_Master.iRaceCur]) / 60
		iSecs = FLOOR(g_savedGlobals.sOffroadData.fBestTime[ORR_Master.iRaceCur] - (TO_FLOAT(iMins) * 60.0))
		SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[OFFROAD_SC_STAT6_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		IF (iSecs < 10)
			DRAW_TEXT_WITH_2_NUMBERS(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT6VAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_TIMEVAL_WZ", iMins, iSecs, FONT_RIGHT)
		ELSE
			DRAW_TEXT_WITH_2_NUMBERS(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT6VAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_TIMEVAL", iMins, iSecs, FONT_RIGHT)
		ENDIF
	ELSE
		DRAW_TEXT(Race.uiScorecard.TextPlacement[OFFROAD_SC_STAT6VAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SC_LB_EMPTY")
	ENDIF
	
	// Draw the 3 medals with times.
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[OFFROAD_SC_BRONZE_BG])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[OFFROAD_SC_SILVER_BG])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[OFFROAD_SC_GOLD_BG])
	DRAW_2D_SPRITE("SPROffroad", "Offroad_Bronze_128", Race.uiScorecard.SpritePlacement[OFFROAD_SC_BRONZE_IMG])
	DRAW_2D_SPRITE("SPROffroad", "Offroad_Silver_128", Race.uiScorecard.SpritePlacement[OFFROAD_SC_SILVER_IMG])
	DRAW_2D_SPRITE("SPROffroad", "Offroad_Gold_128", Race.uiScorecard.SpritePlacement[OFFROAD_SC_GOLD_IMG])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[OFFROAD_SC_BRONZE_OVERLAY])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[OFFROAD_SC_SILVER_OVERLAY])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[OFFROAD_SC_GOLD_OVERLAY])

	SET_TEXT_BLACK(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
	// Bronze goal is "3rd Place"
	DRAW_TEXT_WITH_ALIGNMENT(Race.uiScorecard.TextPlacement[OFFROAD_SC_BRONZEGOAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_3rd", TRUE, FALSE)
	// Silver goal is "2nd Place"
	DRAW_TEXT_WITH_ALIGNMENT(Race.uiScorecard.TextPlacement[OFFROAD_SC_SILVERGOAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_2nd", TRUE, FALSE)
	// Gold goal is "1st Place"
	DRAW_TEXT_WITH_ALIGNMENT(Race.uiScorecard.TextPlacement[OFFROAD_SC_GOLDGOAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_1st", TRUE, FALSE)

ENDPROC

PROC ORR_Setup_Leaderboard(ORR_RACE_STRUCT &Race)
	
//	Race = Race
	SET_SC_LEADERBOARD_TITLE(Race.uiLeaderboard, "OFFR_TITLE")
//	SET_SC_LEADERBOARD_HEADER(Race.uiLeaderboard, SECTION_WORLD, 0, SLOT_LAYOUT_2_STATS, 
//								"SPR_TIME", "SPR_RANK", "", "", ICON_TIME, ICON_POINT)
//	SET_SC_LEADERBOARD_SLOT_2_COLUMNS(Race.uiLeaderboard, SECTION_WORLD, 0, "Toby_Stark", 
//										SC_LEADERBOARD_MAKE_TIME_LABEL(1, 21), 
//										SC_LEADERBOARD_MAKE_INT_PRETTY(1))
//	SET_SC_LEADERBOARD_SLOT_2_COLUMNS(Race.uiLeaderboard, SECTION_WORLD, 1, "CrazyMadCheapSkate", 
//										SC_LEADERBOARD_MAKE_TIME_LABEL(1, 21), 
//										SC_LEADERBOARD_MAKE_INT_PRETTY(2))
//	HIGHLIGHT_SC_LEADERBOARD_SLOT(Race.uiLeaderboard, SECTION_WORLD, 0, TRUE)
//	
//	SET_SC_LEADERBOARD_HEADER(Race.uiLeaderboard, SECTION_FRIEND, 1, SLOT_LAYOUT_2_STATS, 
//								"SPR_TIME", "SPR_RANK", "", "", ICON_TIME, ICON_POINT)
//	SET_SC_LEADERBOARD_SLOT_2_COLUMNS(Race.uiLeaderboard, SECTION_FRIEND, 0, "Beef_cake_007", 
//										SC_LEADERBOARD_MAKE_TIME_LABEL(1, 45),
//										SC_LEADERBOARD_MAKE_INT_PRETTY(18))
//	SET_SC_LEADERBOARD_SLOT_2_COLUMNS(Race.uiLeaderboard, SECTION_FRIEND, 1, "CheeseMakesMeSleepy", 
//										SC_LEADERBOARD_MAKE_TIME_LABEL(2, 15),
//										SC_LEADERBOARD_MAKE_INT_PRETTY(597))
//	SET_SC_LEADERBOARD_SLOT_2_COLUMNS(Race.uiLeaderboard, SECTION_FRIEND, 2, "Effing_A_69", 
//										SC_LEADERBOARD_MAKE_TIME_LABEL(3, 1), 
//										SC_LEADERBOARD_MAKE_INT_PRETTY(1156))
//	HIGHLIGHT_SC_LEADERBOARD_SLOT(Race.uiLeaderboard, SECTION_FRIEND, 0, TRUE)
//	
//	SET_SC_LEADERBOARD_HEADER(Race.uiLeaderboard, SECTION_CREW, 2, SLOT_LAYOUT_2_STATS, 
//								"SPR_TIME", "SPR_RANK", "", "", ICON_TIME, ICON_POINT)
//	SET_SC_LEADERBOARD_SLOT_2_COLUMNS(Race.uiLeaderboard, SECTION_CREW, 0, "Ultimate_Pen", 
//										SC_LEADERBOARD_MAKE_TIME_LABEL(1, 53), 
//										SC_LEADERBOARD_MAKE_INT_PRETTY(489))
//	SET_SC_LEADERBOARD_SLOT_2_COLUMNS(Race.uiLeaderboard, SECTION_CREW, 1, "Brain_8674", 
//										SC_LEADERBOARD_MAKE_TIME_LABEL(2, 48), 
//										SC_LEADERBOARD_MAKE_INT_PRETTY(992))
//	SET_SC_LEADERBOARD_SLOT_2_COLUMNS(Race.uiLeaderboard, SECTION_CREW, 2, "bLACKhADDER", 
//										SC_LEADERBOARD_MAKE_TIME_LABEL(2, 57), 
//										SC_LEADERBOARD_MAKE_INT_PRETTY(1013))
										
ENDPROC


/*
MG_STATS_TRACKER_SET_TITLE_AND_DESC(ORR_Master.szRaceName[ORR_Master.iRaceCur], "RACECOMPLETE")			
	MG_STATS_TRACKER_ADD_DATA_SLOT_FRACTION(TO_FLOAT(Race.Racer[iPlayerIndex].iRank), TO_FLOAT(Race.iRacerCnt), "SPR_POSITION")
	IF (ORR_Master.eRaceType = ORR_RACE_TYPE_PLANE)	
		MG_STATS_TRACKER_ADD_DATA_SLOT_TIME(Race.Racer[iPlayerIndex].fPlsMnsTot, "TIMEBONUS_R")		
		MG_STATS_TRACKER_ADD_DATA_SLOT_TIME(Race.Racer[iPlayerIndex].fClockTime, "SPR_TIME")				
	ELSE		
		PRINTSTRING("Race.Racer[iPlayerIndex].fClockTime: ")
		PRINTFLOAT(Race.Racer[iPlayerIndex].fClockTime)
		PRINTNL()
		MG_STATS_TRACKER_ADD_DATA_SLOT_TIME(Race.Racer[iPlayerIndex].fClockTime, "SPR_TIME")		
	ENDIF
	MG_STATS_TRACKER_SET_MEDAL(mgMedalBronze)	
*/

