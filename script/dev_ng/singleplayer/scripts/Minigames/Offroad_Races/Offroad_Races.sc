// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	Offroad_Races.sc
//		AUTHOR			:	Troy Schram
//		DESCRIPTION		:	Drive off-road vehicles in races against ai/time
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// -----------------------------------
// SPR FILE INCLUDES
// -----------------------------------

USING "orr_head.sch"
USING "orr_helpers.sch"
USING "orr_camera.sch"
USING "orr_gate.sch" 
USING "orr_helpers_offroad.sch"
USING "orr_racer.sch"
USING "orr_scorecard_lib.sch"
USING "emergency_call.sch"

// SPR Main Data. // moved for setup in offroad
ORR_MAIN_STRUCT ORR_Main

USING "orr_race.sch"
//USING "Offroad_Races.sch"
USING "orr_cutscenes.sch"

// -----------------------------------
// VARIABLES
// -----------------------------------

// SPR Race Data.
ORR_RACE_STRUCT ORR_Race

// Script Specific Data.
VECTOR vScriptStartPos = <<2659.4451,4304.6997,44.5639>>
FLOAT fScriptStartHead = 258.0
BOOL bDoTeles
BOOL bDoPlayTele
//CAMERA_INDEX Intro_Cam_Start

// -----------------------------------
// RACE FILE INCLUDES
// -----------------------------------


USING "orr_course_data.sch"

// -----------------------------------
// MAIN PROCS/FUNCTIONS
// -----------------------------------

/// PURPOSE:
///    Setup SPR Race.
PROC ORR_Setup_Race()

	CDEBUG1LN(DEBUG_OR_RACES, "Setting bHasStartedCheerSound to FALSE, heroically")
	ORR_Race.bHasStartedCheerSound = FALSE

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		ADD_PED_FOR_DIALOGUE(myDialogueStruct, 0, PLAYER_PED_ID(), "TREVOR")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		ADD_PED_FOR_DIALOGUE(myDialogueStruct, 0, PLAYER_PED_ID(), "MICHAEL")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(myDialogueStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
	ENDIF
	ADD_PED_FOR_DIALOGUE(myDialogueStruct, 1, NULL, "OR_Taunt")
	
	// If in SCH File Mode, call setup race function.

		ORR_Race_Init(ORR_Race)
		SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
			CASE CanyonCliffs
				ORR_Setup_Offroad_Race_05(ORR_Race)	
			BREAK
			CASE RidgeRun
				ORR_Setup_Offroad_race_08(ORR_Race)		
			BREAK
			CASE ValleyTrail
				ORR_Setup_Offroad_race_09(ORR_Race)	
			BREAK
			CASE LakesideSplash
				ORR_Setup_Offroad_race_10(ORR_Race)	
			BREAK
			CASE EcoFriendly
				ORR_Setup_Offroad_race_11(ORR_Race)			
			BREAK
			CASE MinewardSpiral
				ORR_Setup_Offroad_race_12(ORR_Race)
			BREAK
		ENDSWITCH
		
		IF ORR_DOES_RACE_REQUIRE_MOTORCYCLE()
			INT iRacer
			REPEAT ORR_Race.iRacerCnt iRacer
				IF ORR_Race.Racer[iRacer].Driver <> PLAYER_PED_ID() 
					ORR_Race.Racer[iRacer].eDriverModel = ORR_GET_RANDOM_MOTORCYCLE_MODEL()
				ENDIF
			ENDREPEAT
		ENDIF
		
ENDPROC

// Race AI Timer
structTimer aiUpdate_timer

// Race AI
PROC AI_Update(ORR_RACE_STRUCT& Race)
     INT i     
     IF TIMER_DO_ONCE_WHEN_READY(aiUpdate_timer, 5.0)
          REPEAT Race.iRacerCnt i
              IF Race.Racer[i].Driver <> PLAYER_PED_ID()                   
				   ORR_Race_Racer_Update_Rubberband_Speed(Race, i)                   
              ENDIF
          ENDREPEAT
          RESTART_TIMER_AT(aiUpdate_timer, 0.0)
     ENDIF
	 
	 IF ORR_USE_WAYPOINT_RECORDING 						
		REPEAT Race.iRacerCnt i
			IF (Race.Racer[i].iGateCur < Race.iGateCnt)
	        	IF Race.Racer[i].Driver <> PLAYER_PED_ID()     
					AND Race.bFailChecking
				  	IF NOT IS_ENTITY_DEAD(Race.Racer[i].Driver)
					AND NOT IS_ENTITY_DEAD(Race.Racer[i].Vehicle)											
						IF NOT IS_PED_INJURED(Race.Racer[i].Driver)
							IF (GET_PED_IN_VEHICLE_SEAT(Race.Racer[i].Vehicle) != Race.Racer[i].Driver)
								IF (GET_SCRIPT_TASK_STATUS(Race.Racer[i].Driver, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK)
									IF IS_VEHICLE_EMPTY(Race.Racer[i].Vehicle)
										SET_PED_HELMET(Race.Racer[i].Driver, TRUE)
										TASK_ENTER_VEHICLE(Race.Racer[i].Driver, Race.Racer[i].Vehicle,DEFAULT_TIME_NEVER_WARP)
									ENDIF
								ENDIF
							ELIF (GET_SCRIPT_TASK_STATUS(Race.Racer[i].Driver, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) != PERFORMING_TASK)
							AND GET_TIMER_IN_SECONDS(Race.tClock) > 2.0 // LM - double check task after the first couple of seconds.  B* 1058843
								IF ORR_Master.iRaceCur+1 >= 1 AND ORR_Master.iRaceCur+1 <= 6
									TEXT_LABEL OffroadRecording = "Offroad_"
									SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
										CASE OFFROAD_RACE_CANYON_CLIFFS
											OffroadRecording += 1
										BREAK
										CASE OFFROAD_RACE_RIDGE_RUN
											OffroadRecording += 2
										BREAK
										CASE OFFROAD_RACE_MINEWARD_SPIRAL
											OffroadRecording += 6
										BREAK
										CASE OFFROAD_RACE_VALLEY_TRAIL
											OffroadRecording += 3
										BREAK
										CASE OFFROAD_RACE_LAKESIDE_SPLASH
											OffroadRecording += 4
										BREAK
										CASE OFFROAD_RACE_ECO_FRIENDLY
											OffroadRecording += 5
										BREAK
									ENDSWITCH
									CDEBUG2LN( DEBUG_OR_RACES, "Tasking to follow ", OffroadRecording )
									SET_PED_HELMET(Race.Racer[i].Driver, TRUE)
									CDEBUG1LN(DEBUG_OR_RACES, "[WARNING] - re task check 1")
									TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Race.Racer[i].Driver, Race.Racer[i].Vehicle, OffroadRecording, DRIVINGMODE_AVOIDCARS_RECKLESS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, GET_VEHICLE_ESTIMATED_MAX_SPEED(Race.Racer[i].Vehicle))			
								ELSE
									CDEBUG1LN(DEBUG_OR_RACES, "[WARNING] - offroad_races.sc Prevented script from loading Offroad_", ORR_Master.iRaceCur+1)
								ENDIF
							ENDIF					
						ENDIF
					ENDIF
				ENDIF
	        ENDIF
	    ENDREPEAT
	ENDIF

ENDPROC


/// PURPOSE:
///    Setup SPR Main.
/// RETURNS:
///    TRUE if still setting up SPR Main
FUNC BOOL ORR_Main_Setup(RACE_LAUNCH_DATA launchData)

	// Check if player is dead.
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Main_Setup: Player dead!")
		RETURN FALSE
	ENDIF
	
	DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
	
	INT i
	
	// Setup SPR Main State Machine.
	SWITCH (ORR_Main.eSetup)
	
		// Setup (init).
		CASE ORR_MAIN_SETUP_INIT
		
			DISABLE_SELECTOR()
			DISPLAY_RADAR(FALSE)
			
			SET_WAYPOINT_OFF()

			CLEAR_GPS_PLAYER_WAYPOINT()
			
			SET_WANTED_LEVEL_MULTIPLIER(0.1)
			
			SUPPRESS_EMERGENCY_CALLS()
			ENABLE_ALL_DISPATCH_SERVICES(FALSE)
						
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_SETUP - INIT")
			
			ORR_LOAD_COURSE_DATA(ORR_Race)
			ORR_Master.vDefRcrPos		= launchdata.raceStartLoc
			vScriptStartPos 			= launchdata.raceStartLoc
			ORR_Master.iRaceCur 		= ENUM_TO_INT(launchdata.raceToLaunch)
			
			STORE_PLAYER_PED_VARIATIONS( PLAYER_PED_ID(), FALSE )
			GET_PED_VARIATIONS(PLAYER_PED_ID(), ORR_Master.tPedProps)
		
			CLEAR_AREA_AROUND_ORR_POS(vScriptStartPos)	
			
			ORR_Setup_Race()
			NEW_LOAD_SCENE_START_SPHERE(vScriptStartPos, 60)
			
			TEXT_LABEL_63 recName
			recName = "offroadrace"
			SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
				CASE OFFROAD_RACE_CANYON_CLIFFS
					recName += 1
				BREAK
				CASE OFFROAD_RACE_RIDGE_RUN
					recName += 2
				BREAK
				CASE OFFROAD_RACE_MINEWARD_SPIRAL
					recName += 6
				BREAK
				CASE OFFROAD_RACE_VALLEY_TRAIL
					recName += 3
				BREAK
				CASE OFFROAD_RACE_LAKESIDE_SPLASH
					recName += 4
				BREAK
				CASE OFFROAD_RACE_ECO_FRIENDLY
					recName += 5
				BREAK
			ENDSWITCH
			recName += "car"
			REQUEST_VEHICLE_RECORDING(1, recName)
			
			//grab some racers
			i = 0
			REPEAT COUNT_OF(launchdata.Racer) i
				IF i < COUNT_OF(ORR_Race.Racer)
					ORR_Race.Racer[i+1].Driver = launchdata.Racer[i]
					ORR_Race.Racer[i+1].Vehicle = launchdata.RaceVehicle[i]
					IF DOES_ENTITY_EXIST(ORR_Race.Racer[i+1].Driver)
						SET_ENTITY_AS_MISSION_ENTITY(ORR_Race.Racer[i+1].Driver, TRUE, TRUE)
					ENDIF
					IF DOES_ENTITY_EXIST(ORR_Race.Racer[i+1].Vehicle)
						SET_ENTITY_AS_MISSION_ENTITY(ORR_Race.Racer[i+1].Vehicle, TRUE, TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(ORR_Race.Racer[i+1].Vehicle, TRUE)
						SET_VEHICLE_IS_RACING(ORR_Race.Racer[i+1].Vehicle, TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
			
			STRING scenarioName
			SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
				CASE OFFROAD_RACE_CANYON_CLIFFS
					scenarioName = "CanyonCliffs_Start"
					BREAK
				CASE OFFROAD_RACE_RIDGE_RUN
					scenarioName = "RidgeRun_Start"
					BREAK
				CASE OFFROAD_RACE_VALLEY_TRAIL
					scenarioName = "ValleyTrail_Start"
					BREAK
				CASE OFFROAD_RACE_LAKESIDE_SPLASH
					scenarioName = "LakesideSplash_Start"
					BREAK
				CASE OFFROAD_RACE_ECO_FRIENDLY
					scenarioName = "EcoFriendly_Start"
					BREAK
				CASE OFFROAD_RACE_MINEWARD_SPIRAL
					scenarioName = "MinewardSpiral_Start"
					BREAK
			ENDSWITCH
			
			IF NOT IS_STRING_NULL_OR_EMPTY(scenarioName)
				IF DOES_SCENARIO_GROUP_EXIST(scenarioName)
					IF IS_SCENARIO_GROUP_ENABLED(scenarioName)
						SET_SCENARIO_GROUP_ENABLED(scenarioName, FALSE)
						CPRINTLN( DEBUG_OR_RACES, "Scenario group Disabled." )
					ENDIF
				ELSE
					PRINTLN(scenarioName, " scenario group does not exist!")
				ENDIF
			ELSE
				PRINTLN(">>>>>> WARNING! Do we know what race we're in? We didn't get an attached scenario group")
			ENDIF
			
			SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
				CASE OFFROAD_RACE_CANYON_CLIFFS
					scenarioName = "CanyonCliffs"
					BREAK
				CASE OFFROAD_RACE_RIDGE_RUN
					scenarioName = "RidgeRun"
					BREAK
				CASE OFFROAD_RACE_VALLEY_TRAIL
					scenarioName = "ValleyTrail"
					BREAK
				CASE OFFROAD_RACE_LAKESIDE_SPLASH
					scenarioName = "LakesideSplash"
					BREAK
				CASE OFFROAD_RACE_ECO_FRIENDLY
					scenarioName = "EcoFriendly"
					BREAK
				CASE OFFROAD_RACE_MINEWARD_SPIRAL
					scenarioName = "MinewardSpiral"
					BREAK
			ENDSWITCH
			
			IF NOT IS_STRING_NULL_OR_EMPTY(scenarioName)
				IF DOES_SCENARIO_GROUP_EXIST(scenarioName)
					IF NOT IS_SCENARIO_GROUP_ENABLED(scenarioName)
						SET_SCENARIO_GROUP_ENABLED(scenarioName, TRUE)
						CPRINTLN( DEBUG_OR_RACES, "Scenario group enabled." )
					ENDIF
				ELSE
					PRINTLN(scenarioName, " scenario group does not exist!")
				ENDIF
			ELSE
				PRINTLN(">>>>>> WARNING! Do we know what race we're in? We didn't get an attached scenario group")
			ENDIF
		
			ORR_INIT_HINT_CAM()
			
			ORR_Master.bIntroFXDone = FALSE
			ORR_Init_Scorecard(ORR_Race)
			TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS(ORR_Race, bDoPlayTele)
			ANIMPOSTFX_STOP("SwitchSceneNeutral")
			ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
			MAKE_TRANSITION_CAM()
			RESTART_TIMER_NOW(ORR_Master.tmrFinishCut)
			
			IF IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
				CPRINTLN( DEBUG_OR_RACES, "Player is still in his vehicle..." )
			ENDIF
			WAIT(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				makeCutsceneCams()
			ENDIF
//			PLAY_STREAM_FRONTEND()
			
			
			// Turn on the slipstream effect
			SET_ENABLE_VEHICLE_SLIPSTREAMING(TRUE)
			
			
			IF IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
				CPRINTLN( DEBUG_OR_RACES, "Player is in his vehicle after cams..." )
			ELSE
				CPRINTLN( DEBUG_OR_RACES, "Player is not in vehicle after cams..." )
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_SETUP - LOAD_INIT")
			ORR_Main.eSetup = ORR_MAIN_SETUP_LOAD_INIT

			ORR_Race_Setup(ORR_Race)
		BREAK
		
//		// Setup Fade Out.
//		CASE ORR_MAIN_SETUP_FADE_OUT
////			IF ORR_FadeOut_Safe(1000)
//				CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_SETUP - LOAD_INIT")
//				ORR_Main.eSetup = ORR_MAIN_SETUP_LOAD_INIT
////			ENDIF
//		BREAK
		
		// Setup Load (init).
		CASE ORR_MAIN_SETUP_LOAD_INIT
			// Load stringtable (minigames).
			REQUEST_ANIM_DICT("MINI@RACING@BIKE@")
			REQUEST_ANIM_DICT("MINI@RACING@QUAD@")
			REQUEST_ADDITIONAL_TEXT("SP_SPR", MINIGAME_TEXT_SLOT)
//			REQUEST_SCRIPT_AUDIO_BANK("DIRT_RACE_01")
//			REQUEST_SCRIPT_AUDIO_BANK("DIRT_RACE_02")
//			REQUEST_SCRIPT_AUDIO_BANK("DIRT_RACE_03")
			IF INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur) = OFFROAD_RACE_CANYON_CLIFFS
				REQUEST_MODEL(prop_fncwood_14c)
				REQUEST_MODEL(prop_fncwood_14e)
			ENDIF
			REQUEST_STREAMED_TEXTURE_DICT("SPROffroad")
			REQUEST_STREAMED_TEXTURE_DICT("Shared")
			ORR_Race.uiLeaderboard = REQUEST_SC_LEADERBOARD_UI()
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
			// Load script start area.
			NEW_LOAD_SCENE_STOP()
			//LOAD_SCENE(vScriptStartPos)	// Removed this as it was causing the game to hang. This is a blocking call and appears to be unnecessary.
			
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_SETUP - LOAD_WAIT")
			ORR_Main.eSetup = ORR_MAIN_SETUP_LOAD_WAIT
		BREAK
		
		// Setup Load (wait).
		CASE ORR_MAIN_SETUP_LOAD_WAIT
		
			IF HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
			AND HAS_SCALEFORM_MOVIE_LOADED(ORR_Race.uiLeaderboard)
				START_AUDIO_SCENE("RACE_INTRO_GENERIC")
				
				IF ORR_Master.iRaceCur = ENUM_TO_INT(OFFROAD_RACE_LAKESIDE_SPLASH)
					// Remove tri IPLs that might be active here
					IF IS_IPL_ACTIVE("CS2_06_TriAf02")
						ORR_Master.bIPLActive = TRUE
						REMOVE_IPL("CS2_06_TriAf02")
					ENDIF
				ENDIF
				
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_SETUP - CREATE_INIT")
				ORR_Main.eSetup = ORR_MAIN_SETUP_PLACE_INIT
			ENDIF			
		BREAK
		
		// Setup Create (init).
		CASE ORR_MAIN_SETUP_CREATE_INIT
			// TODO: Add anything that need creating here...
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_SETUP - CREATE_WAIT")
			ORR_Main.eSetup = ORR_MAIN_SETUP_CREATE_WAIT
			RESTART_TIMER_NOW(ORR_Main.tSetup)
		BREAK
		
		// Setup Create (wait).
		CASE ORR_MAIN_SETUP_CREATE_WAIT
			IF TIMER_DO_WHEN_READY(ORR_Main.tSetup, 0.5)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_SETUP - PLACE_INIT")
				ORR_Main.eSetup = ORR_MAIN_SETUP_PLACE_INIT
			ENDIF
		BREAK
		
		// Setup Place (init).
		CASE ORR_MAIN_SETUP_PLACE_INIT
		
			// If player is in vehicle, skip placement.
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_SETUP - WAIT")
				ORR_Main.eSetup = ORR_MAIN_SETUP_WAIT
			// Otherwise, player is on foot, do placement.
			ELSE
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_SETUP - PLACE_WAIT")
				ORR_Main.eSetup = ORR_MAIN_SETUP_WAIT
				//RESTART_TIMER_NOW(ORR_Main.tSetup)
				CANCEL_TIMER( ORR_Main.tSetup )
			ENDIF
			
		BREAK
		
		// Setup Place (wait).
		CASE ORR_MAIN_SETUP_PLACE_WAIT
			IF TIMER_DO_WHEN_READY(ORR_Main.tSetup, 0.5)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_SETUP - FADE_IN")
				ORR_Main.eSetup = ORR_MAIN_SETUP_FADE_IN
			ENDIF
		BREAK
		
		// Setup Fade In.
		CASE ORR_MAIN_SETUP_FADE_IN
			//IF ORR_FadeIn_Safe(1000)
			//	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_SETUP - WAIT")
				ORR_Main.eSetup = ORR_MAIN_SETUP_WAIT

			//ENDIF
		BREAK
		
		// Setup (wait).
		CASE ORR_MAIN_SETUP_WAIT
			IF IS_TIMER_STARTED(ORR_Main.tSetup)
				CANCEL_TIMER(ORR_Main.tSetup)
			ENDIF
			RETURN FALSE
		BREAK
		
		// Setup (cleanup).
		CASE ORR_MAIN_SETUP_CLEANUP
			RETURN FALSE
		BREAK
		
	ENDSWITCH
	
	// Setup SPR Main still running.
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Update SPR Main.
/// RETURNS:
///    TRUE if still updating SPR Main.
FUNC BOOL ORR_Main_Update(RACE_LAUNCH_DATA launchData)

	// Check if player is dead.
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Main_Update: Player dead!")
		RETURN FALSE
	ENDIF
		
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)  // turn off traffic
	
	// Check if player vehicle exists yet.
	IF DOES_ENTITY_EXIST(ORR_Master.PlayerVeh)
	
		// Check if player vehicle is alive.
		IF NOT IS_ENTITY_DEAD(ORR_Master.PlayerVeh)
		
			// Check if player is out of player vehicle.
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), ORR_Master.PlayerVeh)
			
				// If player is in a vehicle, make that player vehicle.
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					ORR_Master.PlayerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				// Otherwise, player isn't in a vehicle, remind him of this.
				ELSE
					// TODO: Decide what to do if player exits player vehicle.
					//PRINT_NOW("ORR_FAIL_GETOUT", DEFAULT_GOD_TEXT_TIME, 1)
					//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Main_Update: Player not in vehicle!")
					//RETURN FALSE
				ENDIF
				
			ENDIF
			
		ENDIF
		
	// Otherwise, if player is in player vehicle, get a handle to it.
	ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		ORR_Master.PlayerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		CPRINTLN( DEBUG_OR_RACES, "Getting player vehicle he is in..." )
	ENDIF
	
	IF ORR_Race.fadeCheckpoint <> NULL
		ORR_Race.iFadeAlpha -= 25
		IF ORR_Race.iFadeAlpha <= 0
			DELETE_CHECKPOINT(ORR_Race.fadeCheckpoint)
			ORR_Race.fadeCheckpoint = NULL
		ELSE
			SET_CHECKPOINT_RGBA(ORR_Race.fadeCheckpoint, 255, 255, 255, IMIN(CEIL(1.5 * ORR_Race.iFadeAlpha), 255))
			SET_CHECKPOINT_RGBA2(ORR_Race.fadeCheckpoint, 255, 255, 255, ORR_Race.iFadeAlpha)
		ENDIF
	ENDIF
	
	// Update SPR Main State Machine.
	SWITCH (ORR_Main.eUpdate)
	
		// Update (init).
		CASE ORR_MAIN_UPDATE_INIT
			fScriptStartHead = fScriptStartHead
//			IF NOT IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
//				// Teleport player to script start position/heading.
//				CPRINTLN( DEBUG_OR_RACES, "Teleporting playing in main update init." )
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), vScriptStartPos)
//				SET_ENTITY_HEADING(PLAYER_PED_ID(), fScriptStartHead)
//				
//				// Reset gameplay camera behind player.
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)			
//			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_UPDATE - INIT")
			
			// Set SRP Mode as invalid.
			ORR_Main.iSPRMode = -1
					
			ORR_Master.iRaceCur = ENUM_TO_INT(launchdata.raceToLaunch)
			
			//REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\TARGET_PRACTICE")  // request audio for countdown timer
			
			OffroadRace_LoadRecordedWaypoint() //let's just request them once... - SiM - 10/31/2011
			
			IF IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
				CPRINTLN( DEBUG_OR_RACES, "Player is still in vehicle prior to setup." )
			ELSE
				CPRINTLN( DEBUG_OR_RACES, "Player is not in vehicle prior to setup." )
			ENDIF
			
			ORR_Race_Setup(ORR_Race) //start streaming in common stuff			
			ORR_OffRoad_SetupOutfits()	
			
			IF IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
				CPRINTLN( DEBUG_OR_RACES, "Player is still in vehicle post setup." )
			ELSE
				CPRINTLN( DEBUG_OR_RACES, "Player is not in vehicle post setup." )
			ENDIF
			
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)		
			IF IS_BITMASK_AS_ENUM_SET(iExitVehicelBit, vehicleExited)
				CLEAR_BITMASK_AS_ENUM(iExitVehicelBit, vehicleExited)
			ENDIF
			
			IF NOT ORR_Race.bRestarting
				IF Load_Cutscene_assets()						
					//IF (ORR_Race.eSetup > ORR_RACE_SETUP_LOAD_WAIT) // means everything is streamed in, not taking control away until then
					IF NOT DetermineOffroadCutscene(ORR_Race) 
						IF makeDonorPed( ORR_RACE, launchData.Loaner )
							IF bCamerasMade
								CDEBUG1LN(DEBUG_OR_RACES, "bCamerasMade TRUE, starting offroad_vehicle_settings")	
								
								IF Offroad_Vehicle_Settings(ORR_Race, ORR_Race.Racer[0], launchData.LoanerVehicle)
									IF DOES_ENTITY_EXIST( playerVeh )
										//SET_VEHICLE_RADIO_ENABLED(playerVeh, FALSE)
									ENDIF
									ActivatePlayerVehicleTeleport(ORR_Race)
									TELEPORT_CUTSCENE_VEHICLE_TO_END_POS(ORR_Race, bDoTeles)
									TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS(ORR_Race, bDoPlayTele)
									Offroad_Cutscenes(ORR_Race)
									
								//	SET_CUSTOM_RADIO_TRACK_LIST("RADIO_01_CLASS_ROCK", "OFF_ROAD_RADIO_ROCK_LIST", TRUE)
									SET_VEHICLE_RADIO_ENABLED(ORR_Race.Racer[0].Vehicle, FALSE)
									IF DOES_ENTITY_EXIST( ORR_Master.savedVeh )
										SET_ENTITY_VISIBLE( ORR_Master.savedVeh, TRUE )
										SET_ENTITY_AS_MISSION_ENTITY(ORR_Master.savedVeh, DEFAULT, TRUE)
										CPRINTLN( DEBUG_OR_RACES, "Making saved veh visible" )
									ELSE
										CPRINTLN( DEBUG_OR_RACES, "saved veh doesn't exist..." )
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					//ENDIF
				ENDIF
			ELIF NOT IS_ENTITY_DEAD( playerVeh )
				//SET_VEHICLE_RADIO_ENABLED( playerVeh, FALSE )
			ENDIF
			ORR_Main.eUpdate = ORR_MAIN_UPDATE_CHOOSE_RACE			
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_UPDATE - CHOOSE_RACE")
		BREAK
				
		// Update Choose Race.
		CASE ORR_MAIN_UPDATE_CHOOSE_RACE
			#IF IS_DEBUG_BUILD
				ORR_Race.bPlayerUsedDebugSkip = FALSE
			#ENDIF
			
			IF DOES_ENTITY_EXIST( ORR_Master.savedVeh )
				SET_ENTITY_VISIBLE( ORR_Master.savedVeh, TRUE )
				CPRINTLN( DEBUG_OR_RACES, "UPDATE CHOOSE RACE setting veh visible" )
			ELSE
				CPRINTLN( DEBUG_OR_RACES, "UPDATE CHOOSE RACE saved veh doesn't exist" )
			ENDIF
			// Check if race is restarting.
			IF ORR_Race.bRestarting
				IF IS_BITMASK_AS_ENUM_SET(iExitVehicelBit, vehicleExited)
					CLEAR_BITMASK_AS_ENUM(iExitVehicelBit, vehicleExited)
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)		
				// No longer restarting.				
				// Setup race.
				ORR_Setup_Race()	
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_UPDATE - FADE_OUT")
				bDoTeles = TRUE
				bDoPlayTele = TRUE
				ORR_Main.eUpdate = ORR_MAIN_RESETTING_LOAD
				ORR_Race.eSetup = ORR_RACE_SETUP_INIT
			// Otherwise, wait for player to choose a race.
			ELSE 
				ORR_Race_Setup(ORR_Race) //start streaming in common stuff
				SETUP_POST_RACE_DATA(ORR_Master.iRaceCur)
				Offroad_Cutscenes( ORR_Race )
				IF OffroadRace_GetIsWaypointLoaded()
					//IF NOT ORR_Race_Setup(ORR_Race)
						ORR_Main.eUpdate = ORR_MAIN_UPDATE_ORCUT_LOAD
						CDEBUG1LN(DEBUG_OR_RACES, "Moving to UPDATE_ORCUT_LOAD")
					//ENDIF
//										ENDIF
//									ELSE
//										CDEBUG1LN(DEBUG_OR_RACES, "bCamerasMade, not TRUE yet, going back through make cutscene proc")							
//										makeCutsceneCams()	
//										//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE) 			
//									ENDIF
				ENDIF
//							ENDIF	
//						ELSE
//							CDEBUG1LN(DEBUG_OR_RACES, "Waiting for ORR_Race.eSetup > ORR_RACE_SETUP_LOAD_WAIT")
//						ENDIF	
					//ENDIF			
				//ENDIF
			ENDIF										
		BREAK

		CASE ORR_MAIN_RESETTING_LOAD
			IF NOT ORR_Race_Setup(ORR_Race) //AND CHECK_RACE_LOADED(ORR_Master.iRaceCur)	
				ORR_CLEAR_OFFROAD_RACE_PATH(ORR_Race)
				Offroad_Helpers_Init(ORR_Race)
				TELEPORT_CUTSCENE_VEHICLE_TO_END_POS(ORR_Race, bDoTeles)
				TELPORT_PLAYER_CUTSCENE_VEHICLE_TOENDPOS(ORR_Race, bDoPlayTele)
				SUPPRESS_SHOCKING_EVENTS_NEXT_FRAME()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ORR_Main.eUpdate = ORR_MAIN_RESETTING_LOAD_SCENE
				
				IF DOES_ENTITY_EXIST(ORR_Race.Racer[0].Vehicle)
				AND NOT IS_ENTITY_DEAD(ORR_Race.Racer[0].Vehicle)
					SET_CUSTOM_RADIO_TRACK_LIST("RADIO_01_CLASS_ROCK", "OFF_ROAD_RADIO_ROCK_LIST", TRUE)
					SET_VEHICLE_RADIO_ENABLED(ORR_Race.Racer[0].Vehicle, TRUE)
					SET_VEH_RADIO_STATION(ORR_Race.Racer[0].Vehicle, "RADIO_01_CLASS_ROCK") 
				ENDIF
				
				
				IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
					NEW_LOAD_SCENE_START( GET_ENTITY_COORDS( PLAYER_PED_ID() ), GET_ENTITY_FORWARD_VECTOR( PLAYER_PED_ID() ), 5000 )
					START_TIMER_NOW( ORR_Main.tLoadTimeout )
				ENDIF
			ENDIF
		BREAK
		
		CASE ORR_MAIN_RESETTING_LOAD_SCENE
			IF IS_NEW_LOAD_SCENE_LOADED()
				OR GET_TIMER_IN_SECONDS( ORR_Main.tLoadTimeout ) > ORR_LOAD_SCENE_TIMEOUT
				CANCEL_TIMER( ORR_Main.tLoadTimeout )
				NEW_LOAD_SCENE_STOP()
				SUPPRESS_SHOCKING_EVENTS_NEXT_FRAME()
				ORR_Main.eUpdate = ORR_MAIN_RESETTING_LOAD_FADE
			ENDIF
		BREAK
		
		CASE ORR_MAIN_RESETTING_LOAD_FADE
			IF ORR_FadeIn_Safe(5000)
				ORR_Race.bRestartCameraPan = DOES_CAM_EXIST(ORR_Master.camFinalLBD) AND IS_CAM_ACTIVE(ORR_Master.camFinalLBD) AND IS_CAM_RENDERING(ORR_Master.camFinalLBD)
				ORR_Race.bRestarting = FALSE
				ORR_Race.eSetup = ORR_RACE_SETUP_INIT
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_RESETTING_LOAD: Moving to UPDATE_FADE_IN")
				IF ORR_Race.bRestartCameraPan
					//do nothing.
				ELSE
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ENDIF
				ORR_Main.eUpdate = ORR_MAIN_UPDATE_FADE_IN
			ENDIF
		BREAK
				
		CASE ORR_MAIN_UPDATE_ORCUT_LOAD
			ORR_DISABLE_VEHICLE_CONTROLS()
			IF NOT ORR_Race_Setup(ORR_Race) //AND CHECK_RACE_LOADED(ORR_Master.iRaceCur)				
				IF ORR_FadeIn_Safe(500)
										
					IF Offroad_Cutscenes(ORR_Race)
						ORR_Race.eSetup = ORR_RACE_SETUP_INIT
						CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_UPDATE ORCUT: Moving to UPDATE_FADE_IN")
						eSPRCutsceneState = ORR_CUTSCENE_INIT
						ORR_Main.eUpdate = ORR_MAIN_UPDATE_FADE_IN
					ENDIF
				ENDIF
			ENDIF
		BREAK		

		
		// Update Fade Out.
		CASE ORR_MAIN_UPDATE_FADE_IN
			ORR_DISABLE_VEHICLE_CONTROLS()
			IF ORR_FadeIn_Safe(500)
				ORR_Race.bBoostGoing = FALSE		
				// reset cutscenebit Int/bool
				iORCutsceneBits = 0
				bVehicleSettings = FALSE
				ORR_CountDownUI.iBitFlags = 0
				CDEBUG1LN(DEBUG_OR_RACES, "Inside CASE ORR_MAIN_UPDATE_FADE_IN")
				START_TIMER_AT(aiUpdate_timer, 10.0) //start the timer high so it tiggers right away
				IF ORR_Race.bRestartCameraPan
					ORR_Race.eUpdate = ORR_RACE_WAIT_BEFORE_RESTART_CAMERA_PAN
				ELSE
					ORR_Race.eUpdate = ORR_RACE_UPDATE_INIT
				ENDIF
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_UPDATE - WAIT")
				ORR_Main.eUpdate = ORR_MAIN_UPDATE_WAIT
			ENDIF
		BREAK
		
		// Update (wait).
		CASE ORR_MAIN_UPDATE_WAIT
			// If SPR Mode isn't Edit, update SPR Race.
			IF (ORR_Main.iSPRMode <> 1)
				IF IS_BITMASK_AS_ENUM_SET(ORR_CountDownUI.iBitFlags, CNTDWN_UI_Played_Go)
					AND NOT ORR_Race.bBoostGoing
					ORR_Race.bBoostGoing = TRUE
					eBoostState = BS_READY //reset boost state machine
					IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
						CPRINTLN( DEBUG_OR_RACES, "1ST PERSON FLASHES: Setting first person flash. Game timer = " , GET_GAME_TIMER() )
						SET_FIRST_PERSON_FLASH_EFFECT_VEHICLE_MODEL_HASH(ENUM_TO_INT(GET_ENTITY_MODEL(ORR_Race.Racer[0].Vehicle)))
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					ENDIF
					//SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE( PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER )
					iBoostTimer = GET_GAME_TIMER() + BOOST_START_WINDOW					
					CPRINTLN( DEBUG_OR_RACES, "Starting boost timer" )
				ENDIF
				
				HANDLE_START_BOOST( ORR_Race.Racer[0] )
				
				// Check if SPR Race is done updating.
				GET_RACE_BEST_TIMES(INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur))
				
				IF NOT ORR_Race_Update(ORR_Race)
				
					IF ORR_Race.bRestarting
						
					
						CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_UPDATE - CHOOSE_RACE")
						ORR_Main.eUpdate = ORR_MAIN_UPDATE_CHOOSE_RACE
					// Otherwise, race isn't valid, cleanup main.
					ELSE
						INT iIndex
						
						REPEAT ORR_Race.iRacerCnt iIndex
							IF iIndex <> 0
								IF NOT IS_ENTITY_DEAD(ORR_Race.Racer[iIndex].Driver)
									AND IS_ENTITY_A_MISSION_ENTITY(ORR_Race.Racer[iIndex].Driver)
									SET_PED_AS_NO_LONGER_NEEDED( ORR_Race.Racer[iIndex].Driver )
								ENDIF
							ENDIF
						ENDREPEAT
						
						CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_UPDATE - CLEANUP")
						SETTIMERA(0)
						ORR_Main.eUpdate = ORR_MAIN_UPDATE_PRECLEANUP_DELAY
					ENDIF
				ENDIF
				
				IF ORR_Race.eUpdate = ORR_RACE_UPDATE_WAIT
					// Race AI
					AI_Update(ORR_Race)
					//PED_ANIMS(Fluff)
				ENDIF
				
			ENDIF
			
		BREAK
		
		
		CASE ORR_MAIN_UPDATE_PRECLEANUP_DELAY
			IF TIMERA() >= 2000
				ORR_Main.eUpdate = ORR_MAIN_UPDATE_CLEANUP
			ENDIF
		BREAK
		
		// Update (cleanup).
		CASE ORR_MAIN_UPDATE_CLEANUP
			DELETE_TRANSITION_CAM()
			ORR_RESTORE_OFFROAD_RACE_PATH()
			ENABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_EXIT )
			ENABLE_ALL_CONTROL_ACTIONS( PLAYER_CONTROL )
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_TASKS( PLAYER_PED_ID() )
			ENDIF
			
			SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
				CASE CanyonCliffs
//					IF IS_SCENARIO_GROUP_ENABLED("CanyonCliffs")
//						SET_SCENARIO_GROUP_ENABLED("CanyonCliffs", FALSE)
//					ENDIF
				BREAK
				CASE RidgeRun
//					IF IS_SCENARIO_GROUP_ENABLED("RidgeRun")
//						SET_SCENARIO_GROUP_ENABLED("RidgeRun", FALSE)
//					ENDIF
				BREAK
				CASE ValleyTrail
//					IF IS_SCENARIO_GROUP_ENABLED("ValleyTrail")
//						SET_SCENARIO_GROUP_ENABLED("ValleyTrail", FALSE)
//					ENDIF
				BREAK
				CASE LakesideSplash
//					IF IS_SCENARIO_GROUP_ENABLED("LakesideSplash")
//						SET_SCENARIO_GROUP_ENABLED("LakesideSplash", FALSE)
//					ENDIF
				BREAK
				CASE EcoFriendly
//					IF IS_SCENARIO_GROUP_ENABLED("EcoFriendly")
//						SET_SCENARIO_GROUP_ENABLED("EcoFriendly", FALSE)
//					ENDIF
				BREAK
				CASE MinewardSpiral
//					IF DOES_SCENARIO_GROUP_EXIST("QUARRY")
//						SET_EXCLUSIVE_SCENARIO_GROUP("QUARRY")
//					ENDIF
//					IF IS_SCENARIO_GROUP_ENABLED("MinewardSpiral")
//						SET_SCENARIO_GROUP_ENABLED("MinewardSpiral", FALSE)
//					ENDIF
				BREAK
			ENDSWITCH
			//SET_ROADS_IN_ANGLED_AREA(ORR_Race.Racer[0].vStartPos, ORR_Race.sGate[0].vPos, 40.0, FALSE, TRUE)
			RETURN FALSE
		BREAK
		
	ENDSWITCH
	
	// Update SPR Main still running.
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Cleanup SPR Main.
PROC ORR_Main_Cleanup()
	
	DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	
	ANIMPOSTFX_STOP("SwitchSceneNetural")
	ANIMPOSTFX_STOP("RaceTurbo")
	ANIMPOSTFX_STOP("MinigameTransitionIn")
	ANIMPOSTFX_STOP("MinigameTransitionOut")
	ANIMPOSTFX_STOP("MinigameEndMichael")
	ANIMPOSTFX_STOP("MinigameEndFranklin")
	ANIMPOSTFX_STOP("MinigameEndTrevor")
	
	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
		CASE CanyonCliffs
//			IF IS_SCENARIO_GROUP_ENABLED("CanyonCliffs")
//				SET_SCENARIO_GROUP_ENABLED("CanyonCliffs", FALSE)
//			ENDIF
		BREAK
		CASE RidgeRun
//			IF IS_SCENARIO_GROUP_ENABLED("RidgeRun")
//				SET_SCENARIO_GROUP_ENABLED("RidgeRun", FALSE)
//			ENDIF
		BREAK
		CASE ValleyTrail
//			IF IS_SCENARIO_GROUP_ENABLED("ValleyTrail")
//				SET_SCENARIO_GROUP_ENABLED("ValleyTrail", FALSE)
//			ENDIF
		BREAK
		CASE LakesideSplash
//			IF IS_SCENARIO_GROUP_ENABLED("LakesideSplash")
//				SET_SCENARIO_GROUP_ENABLED("LakesideSplash", FALSE)
//			ENDIF
		BREAK
		CASE EcoFriendly
//			IF IS_SCENARIO_GROUP_ENABLED("EcoFriendly")
//				SET_SCENARIO_GROUP_ENABLED("EcoFriendly", FALSE)
//			ENDIF
		BREAK
		CASE MinewardSpiral
			IF DOES_SCENARIO_GROUP_EXIST("QUARRY")
				SET_EXCLUSIVE_SCENARIO_GROUP("QUARRY")
			ENDIF
//			IF IS_SCENARIO_GROUP_ENABLED("MinewardSpiral")
//				SET_SCENARIO_GROUP_ENABLED("MinewardSpiral", FALSE)
//			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_SCORECARD_INIT)
		CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_SCORECARD_INIT)
		SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	ENDIF
	
	ORR_Cleanup_Leaderboard(ORR_Race.uiLeaderboard)
	
	// turn off the slipstream effect
	SET_ENABLE_VEHICLE_SLIPSTREAMING(FALSE)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, FALSE)
	ENDIF
	
	// Evict stringtable (minigames).
	CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT, TRUE)
	RELEASE_SCRIPT_AUDIO_BANK()
	// Cleanup fluff here
	
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	ENABLE_ALL_DISPATCH_SERVICES(TRUE)
	
	ENABLE_SELECTOR()
	
	STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	// Cleanup race.
	ORR_Race_Cleanup(ORR_Race, FALSE)
	
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENABLE_ALL_CONTROL_ACTIONS( PLAYER_CONTROL )

	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	IF bQuitting
		SET_CINEMATIC_MODE_ACTIVE(FALSE)	
	ENDIF
	
	Set_Leave_Area_Flag_For_All_Blipped_Missions()
	
	// Terminate script.
	TERMINATE_THIS_THREAD()
	
ENDPROC

/// PURPOSE:
///    Process SPR Main debug inputs.
PROC ORR_Main_Debug()
	#IF IS_DEBUG_BUILD
		// Pass/Fail Off-Road Races (exiting for now).
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			IF (ORR_Master.iRaceCur < 0)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Main_Debug: attempting debug functionality when race isnt valid, doing nothing instead")
			ELSE
				ORR_Race.bPlayerUsedDebugSkip = TRUE
				ORR_Race.Racer[0].iGateCur = ORR_Race.iGateCnt - 1
				SET_ENTITY_COORDS(ORR_Race.Racer[0].Vehicle, ORR_Race.sGate[ORR_Race.iGateCnt - 1].vPos)
				SET_VEHICLE_ON_GROUND_PROPERLY(ORR_Race.Racer[0].Vehicle)
//				ORR_Race_Leaderboard_Init_Scorecard(ORR_Race)
//				TRIGGER_EOM_SCREEN(EOM_OUTCOME_RACE_COMPLETE, ORR_Master.szRaceName[ORR_Master.iRaceCur], FALSE)
//				ORR_Main_Cleanup()
			ENDIF
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			bDebugFailure = TRUE
			ORR_Race.bPlayerUsedDebugSkip = TRUE
			IF (ORR_Master.iRaceCur < 0)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Main_Debug: attempting debug functionality when race isnt valid, doing nothing instead")
			ELSE
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Main_Debug: Debug Fail: before cleanup")
				ORR_Main_Cleanup()
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Main_Debug: Debug Fail: after cleanup")
			ENDIF
		ENDIF
		// Jump off-road race gate (ahead).
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			ORR_Race.bPlayerUsedDebugSkip = TRUE
			ORR_Race_Racer_Gate_Jump_Cur(ORR_Race, 0, TRUE)
		// Jump off-road race gate (behind).
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			ORR_Race.bPlayerUsedDebugSkip = TRUE
			ORR_Race_Racer_Gate_Jump_Nxt(ORR_Race, 0, TRUE)
		ENDIF
	#ENDIF
	
ENDPROC




// -----------------------------------
// OFF-ROAD RACES SCRIPT
// -----------------------------------
SCRIPT (RACE_LAUNCH_DATA launchData)

	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances.
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY | FORCE_CLEANUP_FLAG_DEBUG_MENU | FORCE_CLEANUP_FLAG_SP_TO_MP))
		ORR_Main_Cleanup()
	ENDIF
	
	CWARNINGLN(DEBUG_OR_RACES, "AYY LOOK AT DIS")
	
	// Setup SPR Main Run/Setup/Update.
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_RUN - SETUP")
	ORR_Main.eRun = ORR_MAIN_RUN_SETUP
	ORR_Main.eSetup = ORR_MAIN_SETUP_INIT
	ORR_Main.eUpdate = ORR_MAIN_UPDATE_INIT
	
	LOAD_STREAM("INTRO_STREAM", "DIRT_RACES_SOUNDSET")

	// Off-Road Races main loop.
	WHILE TRUE
	
		// Run SPR Main State Machine.
		SWITCH (ORR_Main.eRun)
			CASE ORR_MAIN_RUN_SETUP	
				CASE ORR_MAIN_RUN_UPDATE
				IF NOT ORR_Main_Setup(launchData)
					#IF ORR_ENABLE_DEBUG_UPDATE
					CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_RUN - UPDATE")
					#ENDIF
					ORR_Main.eRun = ORR_MAIN_RUN_UPDATE
				ENDIF
				
				IF IS_TIMER_STARTED(ORR_Master.tmrFinishCut)
					AND NOT ORR_Master.bIntroFXDone
					IF GET_TIMER_IN_SECONDS(ORR_Master.tmrFinishCut) >= 0.5
						ORR_Master.bIntroFXDone = TRUE						
					ENDIF
				ENDIF			
			//BREAK
			
							
				IF NOT ORR_Main_Update(launchData)
					CDEBUG1LN(DEBUG_OR_RACES, "ORR_MAIN_RUN - CLEANUP")
					ORR_Main.eRun = ORR_MAIN_RUN_CLEANUP
				ENDIF
			BREAK
			
			CASE ORR_MAIN_RUN_CLEANUP
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
					ORR_Main_Cleanup()
				ENDIF
			BREAK
		ENDSWITCH
	
		IF bQuitting
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		ENDIF						
		
		// Process SPR Main Debug inputs.
		ORR_Main_Debug()
		
		// Wait until next frame.
		WAIT(0)
		
	ENDWHILE
	
	
	// Should never reach here.
	CDEBUG1LN(DEBUG_OR_RACES, "Should never reach ENDSCRIPT!!!")
	
ENDSCRIPT




// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!
