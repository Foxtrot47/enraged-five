// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	ORR_Head.sch
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Single Player Races - Head global data file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "minigames_helpers.sch"
//USING "minigame_uiinputs.sch"
USING "script_usecontext.sch"
USING "commands_xml.sch"
USING "commands_water.sch"
USING "screen_placements.sch"
USING "end_screen.sch"
USING "minigame_big_message.sch"

CONST_INT	ORR_ENABLE_WIDGETS				0				// 1 to use widgets
CONST_INT 	OFFROAD_USE_SWAP_SCENES 		0

#IF NOT DEFINED(ORR_RACE_IS_TRIATHLON)
	CONST_INT ORR_RACE_IS_TRIATHLON			0	
#ENDIF

CONST_INT ORR_ENABLE_DEBUG_UPDATE			0
CONST_INT ORR_ENABLE_DEBUG_DRAW_TO_SCREEN_INFO	0

// -----------------------------------
// CONSTANTS
// -----------------------------------



#IF NOT DEFINED(ORR_GATE_MAX)
CONST_INT 	ORR_GATE_MAX 					33				// Max race gates.
#ENDIF
CONST_INT	ORR_RACER_MAX					8				// Max race racers.
CONST_INT	ORR_FADE_LONG_TIME				5000			// Fade time (long) (ms).
CONST_INT	ORR_FADE_NORM_TIME				1000			// Fade time (normal) (ms).
CONST_INT	ORR_FADE_QUICK_TIME				500				// Fade time (quick) (ms).
CONST_FLOAT	ORR_CLEAR_AREA_RADIUS			100.0			// Clear area radius (m).
CONST_FLOAT	ORR_ON_GROUND_DIST				10.0			// On ground distance (m).
CONST_FLOAT ORR_GATE_CHKPNT_SCL				10.25			// Gate checkpoint size.
CONST_FLOAT ORR_GATE_CHKPNTNXT_SCL			7.0				// Gate next checkpoint size.
CONST_FLOAT ORR_GATE_BLIPCUR_SCL			1.2				// Gate current blip scale.
CONST_FLOAT ORR_GATE_BLIPNXT_SCL			0.7				// Gate next blip scale.
CONST_FLOAT ORR_GATE_MISS_DIST_SCL			5.0				// Gate miss distance scale.
CONST_FLOAT ORR_GATE_MISS_PENALTY			5.0				// Gate miss penalty (s).
CONST_FLOAT ORR_GATE_INNER_PASS_BONUS		1.0				// Gate inner pass bonus (s).
CONST_FLOAT ORR_GATE_OUTTER_PASS_BONUS		0.0				// Gate Outter pass bonus (s).
CONST_FLOAT ORR_GATE_OUTTER_KNIFE_BONUS		1.5				// Gate Outter Knife bonus (s).
CONST_FLOAT ORR_GATE_OUTTER_INVERT_BONUS	2.0				// Gate Outter Invert bonus (s).
CONST_FLOAT ORR_GATE_INNER_KNIFE_BONUS		2.5				// Gate Inner Knife bonus (s).
CONST_FLOAT ORR_GATE_INNER_INVERT_BONUS		3.0				// Gate Outter pass bonus (s).
CONST_FLOAT	ORR_GATE_STUNT_BONUS			4.0				// Stunt Gate hit bonus
CONST_FLOAT	ORR_GATE_STUNT_HIT				1.5				// Stunt Gate failure to do stunt
CONST_FLOAT ORR_GATE_WRONG_PENALTY			2.5				// Gate miss penalty (s).
CONST_INT	ORR_GATE_MISS_DISP_TIME			2500 			// Gate miss display time (ms).
CONST_FLOAT ORR_GATE_HEIGHT_CHECK			10.0			// Gate height check - to fix Bug # 666112 - DS

CONST_FLOAT ORR_STUNT_INVERT_MIN	-140.0			// The min amount the player needs to get the invert stunt bonus
CONST_FLOAT ORR_STUNT_INVERT_MAX	140.0			// The max amount the player can be inverted to get the invert stunt bonus

CONST_INT	ORR_VEH_STUCK_PLANE_ROOF_TIME	1000	// Vehicle stuck time (roof) (ms).
CONST_INT	ORR_VEH_STUCK_PLANE_SIDE_TIME	1000	// Vehicle stuck time (side) (ms).
CONST_INT	ORR_VEH_STUCK_PLANE_HUNG_TIME	2000	// Vehicle stuck time (hung) (ms).
CONST_INT	ORR_VEH_STUCK_PLANE_JAM_TIME	2000	// Vehicle stuck time (jam) (ms).

CONST_INT	ORR_VEH_STUCK_BIKE_ROOF_TIME	5000	// Vehicle stuck time (roof) (ms).
CONST_INT	ORR_VEH_STUCK_BIKE_SIDE_TIME	20000	// Vehicle stuck time (side) (ms).
CONST_INT	ORR_VEH_STUCK_BIKE_HUNG_TIME	30000	// Vehicle stuck time (hung) (ms).
CONST_INT	ORR_VEH_STUCK_BIKE_JAM_TIME		60000	// Vehicle stuck time (jam) (ms).

CONST_FLOAT ORR_AI_IDLE_TIME				5.0		// AI Racer idle time
CONST_FLOAT ORR_AI_RUBBER_BAND_TIME			0.8		// Offset for teleporting racers
CONST_FLOAT ORR_INIT_SPEED_INTERP_TIME		1.5

CONST_FLOAT	ORR_HUD_NUM_WIDTH		0.0165			// HUD number width for offsetting.
CONST_FLOAT	ORR_HUD_CLN_WIDTH		0.0265			// HUD colon width for offsetting.
CONST_FLOAT	ORR_HUD_PLS_WIDTH		0.0175			// HUD plus width for offsetting.
CONST_FLOAT	ORR_HUD_MNS_WIDTH		0.0095			// HUD minus width for offsetting.
CONST_FLOAT	ORR_HUD_RADAR_POS_X		0.109 			// HUD radar overlay pos (x).
CONST_FLOAT	ORR_HUD_RADAR_POS_Y		0.850 			// HUD radar overlay pos (y).
CONST_FLOAT	ORR_HUD_RADAR_WIDTH		0.110			// HUD radar overlay width.
CONST_FLOAT	ORR_HUD_RADAR_HEIGHT	0.200 			// HUD radar overlay height.
CONST_FLOAT	ORR_HUD_RADAR_ROT_SCL	-1.0 			// HUD radar overlay rot scale.

CONST_FLOAT ORR_UI_CR_HEAD_POS_X	0.4				// UI choose race heading pos (x).
CONST_FLOAT ORR_UI_CR_HEAD_POS_Y	0.05			// UI choose race heading pos (y). (formally .15)
CONST_FLOAT ORR_UI_CR_HEAD_SCALE	0.8				// UI choose race heading scale (x/y). (formally 1.0)
CONST_FLOAT ORR_UI_CR_INFO_POS_X	0.325			// UI choose race info pos (x).
CONST_FLOAT ORR_UI_CR_INFO_POS_Y	0.11			// UI choose race info pos (y). (formally .25)
CONST_FLOAT ORR_UI_CR_INFO_SCALE	0.5				// UI choose race info scale (x/y).
CONST_FLOAT ORR_UI_CR_INFO_SPC_X1	0.23			// UI choose race info space (x1).
CONST_FLOAT ORR_UI_CR_INFO_SPC_X2	0.282			// UI choose race info space (x2).
CONST_FLOAT ORR_UI_CR_INFO_SPC_Y	0.052			// UI choose race info space (y).
CONST_FLOAT ORR_UI_CR_SEL_POS_X		0.32			// UI choose race select pos (x).
CONST_FLOAT ORR_UI_CR_SEL_POS_Y		0.895			// UI choose race select pos (y). (formally .81)
CONST_FLOAT ORR_UI_CR_SEL_SCALE		0.625			// UI choose race select scale (x/y).
CONST_FLOAT ORR_UI_CR_ENT_POS_X		0.435			// UI choose race enter pos (x).
CONST_FLOAT ORR_UI_CR_ENT_POS_Y		0.895			// UI choose race enter pos (y). (formally .81)
CONST_FLOAT ORR_UI_CR_ENT_SCALE		0.625			// UI choose race enter scale (x/y).
CONST_FLOAT ORR_UI_CR_EXIT_POS_X	0.614			// UI choose race exit pos (x).
CONST_FLOAT ORR_UI_CR_EXIT_POS_Y	0.895			// UI choose race exit pos (y). (formally .81)
CONST_FLOAT ORR_UI_CR_EXIT_SCALE	0.625			// UI choose race exit scale (x/y).
CONST_FLOAT ORR_UI_CR_RECT_POS_X	0.5				// UI choose race rectangle pos (x).
CONST_FLOAT ORR_UI_CR_RECT_POS_Y	0.3				// UI choose race rectangle pos (y). (formally .5)
CONST_FLOAT ORR_UI_CR_RECT_WIDTH	0.385			// UI choose race rectange width.
CONST_FLOAT ORR_UI_CR_RECT_HEIGHT	1.30			// UI choose race rectangle height.
CONST_INT 	ORR_UI_CR_RECT_ALPHA	191				// UI choose race rectange alpha.

CONST_FLOAT ORR_UI_CD_NUM_SCALE		2.0				// UI countdown number scale for both x/y.
CONST_FLOAT ORR_UI_CD_NUM_POS_X		0.48			// UI countdown number start position (x).
CONST_FLOAT ORR_UI_CD_NUM_POS_Y		0.4				// UI countdown number start position (y).
CONST_FLOAT ORR_UI_CD_NUM_TIME		1.25			// UI countdown number display for time (s).
CONST_FLOAT ORR_UI_CD_GO_SCALE		2.0				// UI countdown "GO!" scale for both x/y.
CONST_FLOAT ORR_UI_CD_GO_POS_X		0.44			// UI countdown "GO!" start position (x).
CONST_FLOAT ORR_UI_CD_GO_POS_Y		0.4				// UI countdown "GO!" start position (y).
CONST_FLOAT ORR_UI_CD_GO_TIME		1.0				// UI countdown "GO!" display for time (s).

CONST_FLOAT ORR_UI_FINISH_SCALE		2.0				// UI finish scale for both x/y.
CONST_FLOAT ORR_UI_FINISH_POS_X		0.4				// UI finish start position (x).
CONST_FLOAT ORR_UI_FINISH_POS_Y		0.4				// UI finish start position (y).
CONST_FLOAT ORR_UI_FINISH_TIME		3.0				// UI finish display for time (s).

CONST_FLOAT ORR_UI_LB_HEAD_POS_X	0.4				// UI leaderboard heading pos (x).
CONST_FLOAT ORR_UI_LB_HEAD_POS_Y	0.15			// UI leaderboard heading pos (y).
CONST_FLOAT ORR_UI_LB_HEAD_SCALE	1.0				// UI leaderboard heading scale (x/y).
CONST_FLOAT ORR_UI_LB_INFO_POS_X	0.325			// UI leaderboard info pos (x).
CONST_FLOAT ORR_UI_LB_INFO_POS_Y	0.21			// UI leaderboard info pos (y).
CONST_FLOAT ORR_UI_LB_INFO_SCALE	0.5				// UI leaderboard info scale (x/y).
CONST_FLOAT ORR_UI_LB_INFO_SPC_X1	0.23			// UI leaderboard info space (x1).
CONST_FLOAT ORR_UI_LB_INFO_SPC_X2	0.282			// UI leaderboard info space (x2).
CONST_FLOAT ORR_UI_LB_INFO_SPC_Y	0.052			// UI leaderboard info space (y).
CONST_FLOAT ORR_UI_LB_CONT_POS_X	0.32			// UI leaderboard select pos (x).
CONST_FLOAT ORR_UI_LB_CONT_POS_Y	0.81			// UI leaderboard select pos (y).
CONST_FLOAT ORR_UI_LB_CONT_SCALE	0.625			// UI leaderboard select scale (x/y).
CONST_FLOAT ORR_UI_LB_EXIT_POS_X	0.614			// UI leaderboard exit pos (x).
CONST_FLOAT ORR_UI_LB_EXIT_POS_Y	0.81			// UI leaderboard exit pos (y).
CONST_FLOAT ORR_UI_LB_EXIT_SCALE	0.625			// UI leaderboard exit scale (x/y).
CONST_FLOAT ORR_UI_LB_RECT_POS_X	0.5				// UI leaderboard rectangle pos (x).
CONST_FLOAT ORR_UI_LB_RECT_POS_Y	0.5				// UI leaderboard rectangle pos (y).
CONST_FLOAT ORR_UI_LB_RECT_WIDTH	0.375			// UI leaderboard rectange width.
CONST_FLOAT ORR_UI_LB_RECT_HEIGHT	0.750			// UI leaderboard rectangle height.
CONST_INT 	ORR_UI_LB_RECT_ALPHA	191				// UI leaderboard rectange alpha.
CONST_INT 	ORR_UI_LB_RACER_LIMIT	10				// UI leaderboard racer limit.

CONST_INT	ORR_UI_TIME_THROTTLE	10				// UI throttle for updating the race time

CONST_INT	ORR_INPUT_STICK_LIMIT	128				// INPUT stick limit for both x/y.
CONST_INT	ORR_INPUT_STICK_DZ_X	32				// INPUT stick dead zone (x).
CONST_INT	ORR_INPUT_STICK_DZ_Y	32				// INPUT stick dead zone (x).

CONST_FLOAT ORR_CAM_INTERP_SPEED	100.0			// CAM interp speed (m/s).
CONST_INT 	ORR_CAM_INTERP_TIME_MIN	2000			// CAM interp time max (ms).
CONST_INT 	ORR_CAM_INTERP_TIME_MAX	6000			// CAM interp time max (ms).
CONST_FLOAT	ORR_CAM_ROTATE_INC		2.0				// CAM rotate increment (deg).
CONST_FLOAT	ORR_CAM_ROTATE_LIMIT	89.0			// CAM rotate limit (deg).

CONST_FLOAT	ORR_CAM_ZOOM_INC		0.1				// CAM zoom increment (scalar).
CONST_FLOAT	ORR_CAM_ZOOM_LIMIT_MIN	0.5				// CAM zoom min limit (scalar).
CONST_FLOAT	ORR_CAM_ZOOM_LIMIT_MAX	5.0				// CAM zoom max limit (scalar).
CONST_FLOAT ORR_CAM_GATE_FOCUS_DIST 50.0			// CAM gate focus distance (m).
CONST_FLOAT ORR_CAM_RACER_FOCUS_DIST 10.0			// CAM racer focus distance (m).

CONST_FLOAT	ORR_PLANE_SPD_MAX		60.0			// Plane speed max(m/s).
CONST_FLOAT	ORR_TRICK_ROT_MIN		0.0				// Trick rotation min (deg).
CONST_FLOAT	ORR_TRICK_ROT_MID		180.0			// Trick rotation mid (deg).
CONST_FLOAT	ORR_TRICK_ROT_MAX		360.0			// Trick rotation max (deg).
CONST_FLOAT	ORR_TRICK_ROT_JUMP		90.0			// Trick rotation jump (deg).
CONST_FLOAT	ORR_TRICK_ROT_BUFF		30.0			// Trick rotation buffer (deg).
CONST_FLOAT	ORR_TRICK_LOOP_MIN		1.0				// Trick loop bonus min (s).
CONST_FLOAT	ORR_TRICK_LOOP_MAX		15.0			// Trick loop bonus max (s).
CONST_FLOAT	ORR_TRICK_ROLL_MIN		0.2				// Trick roll bonus min (s).
CONST_FLOAT	ORR_TRICK_ROLL_MAX		3.0				// Trick roll bonus max (s).
CONST_FLOAT	ORR_TRICK_INVT_TIME		10.0			// Trick invert time (s).
CONST_FLOAT	ORR_TRICK_INVT_MIN		0.4				// Trick invert bonus min (s).
CONST_FLOAT	ORR_TRICK_INVT_MAX		6.0				// Trick invert bonus max (s).
CONST_FLOAT	ORR_TRICK_DEGRADE		2.0				// Trick bonus degrade (^x).
CONST_INT	ORR_TRICK_DISP_TIME		2500 			// Trick display time (ms).
CONST_FLOAT ORR_FAIL_TIME			20.0

CONST_INT ORR_STOP_DISTANCE			0
CONST_INT ORR_MAX_SPEED_VOLUMES		15

CONST_FLOAT	ORR_HINT_CAM_TIMER_THRESHOLD	1.0			// Threshold for going back from hint cam, like Alwyn's stuff does

CONST_FLOAT ORR_LOAD_SCENE_TIMEOUT			5.0			// Timeout for loading in


CONST_FLOAT ORR_TIME_WEAPON_GRENADE_FAIL	10.0	//used for B*1466276
CONST_INT ORR_NUM_WEAPON_GRENADE_FAIL	3	//used for B*1466276// -----------------------------------

CONST_FLOAT	ORR_fToleranceForSpeedBoostInDegrees		32.5

CONST_INT	ORR_iCashAwardFor1stPlace	500
// ENUMERATIONS
// -----------------------------------
ENUM BOOST_STATE
      BS_READY,
      BS_ACTIVE,
      BS_FINISHED
ENDENUM

BOOST_STATE eBoostState = BS_READY
INT iBoostTimer = -1
CONST_INT BOOST_START_WINDOW 300
CONST_INT BOOST_TIME 1500

ENUM ORR_RETURN_VALUES_ENUM
	ORV_INVALID	= -1,
	ORV_FALSE	= 0,
	ORV_TRUE,
	ORV_RESET
ENDENUM

ENUM ORR_RACE_NAMES
	CanyonCliffs = 0,
	RidgeRun,
	MinewardSpiral,
	ValleyTrail,
	LakesideSplash,
	EcoFriendly,
	ConstructionCourse,
	Number_Of_Races
ENDENUM

ENUM ORR_FAIL_CASES
	ORR_FAIL_INVALID = -1,
	ORR_FAIL_TOO_FAR,
	ORR_FAIL_WEAPON,
	ORR_FAIL_VEHICLE_DEATH,
	ORR_FAIL_IDLE,
	ORR_FAIL_VEHICLE_EXIT
ENDENUM


// SPR Main Run.
ENUM ORR_MAIN_RUN_ENUM
	ORR_MAIN_RUN_SETUP,
	ORR_MAIN_RUN_UPDATE,
	ORR_MAIN_RUN_CLEANUP
ENDENUM

// SPR Main Setup.
ENUM ORR_MAIN_SETUP_ENUM
	ORR_MAIN_SETUP_INIT,
	ORR_MAIN_SETUP_FADE_OUT,
	ORR_MAIN_SETUP_LOAD_INIT,
	ORR_MAIN_SETUP_LOAD_WAIT,
	ORR_MAIN_SETUP_CREATE_INIT,
	ORR_MAIN_SETUP_CREATE_WAIT,
	ORR_MAIN_SETUP_PLACE_INIT,
	ORR_MAIN_SETUP_PLACE_WAIT,
	ORR_MAIN_SETUP_FADE_IN,
	ORR_MAIN_SETUP_WAIT,
	ORR_MAIN_SETUP_CLEANUP
ENDENUM

// SPR Main Udpate.
ENUM ORR_MAIN_UPDATE_ENUM
	ORR_MAIN_UPDATE_INIT,
	ORR_MAIN_UPDATE_GET_IN_INIT,
	ORR_MAIN_UPDATE_GET_IN_WAIT,
	ORR_MAIN_UPDATE_CHOOSE_RACE,
	ORR_MAIN_RESETTING_LOAD,
	ORR_MAIN_UPDATE_ORCUT_LOAD,
	ORR_MAIN_QUIT_FADE_TELEPORT,
	ORR_MAIN_UPDATE_FADE_OUT,
	ORR_MAIN_UPDATE_SETUP_RACE_INIT,
	ORR_MAIN_UPDATE_SETUP_RACE_WAIT,
	ORR_MAIN_UPDATE_FADE_IN,
	ORR_MAIN_UPDATE_WAIT,
	ORR_MAIN_UPDATE_PRECLEANUP_DELAY,
	ORR_MAIN_RESETTING_LOAD_FADE,
	ORR_MAIN_RESETTING_LOAD_SCENE,
	ORR_MAIN_UPDATE_CLEANUP
ENDENUM

// SPR Camera Mode.
ENUM ORR_CAMERA_MODE_ENUM
	ORR_CAMERA_MODE_GAME,
	ORR_CAMERA_MODE_FREE,
	ORR_CAMERA_MODE_FLY
ENDENUM

// SPR Camera Focus.
ENUM ORR_CAMERA_FOCUS_ENUM
	ORR_CAMERA_FOCUS_GATES,
	ORR_CAMERA_FOCUS_RACERS
ENDENUM

// SPR Move Mode.
ENUM ORR_MOVE_MODE_ENUM
	ORR_MOVE_MODE_CAMERA,
	ORR_MOVE_MODE_GROUND,
	ORR_MOVE_MODE_PREV,
	ORR_MOVE_MODE_NEXT,
	ORR_MOVE_MODE_SELECT
ENDENUM

// SPR Racer Reset.
ENUM ORR_RACER_RESET_ENUM
	ORR_RACER_RESET_FAIL_OVER,
	ORR_RACER_RESET_FAIL_HINT,
	ORR_RACER_RESET_INIT,
	ORR_RACER_RESET_CHOOSE,
	ORR_RACER_RESET_FADE_OUT,
	ORR_RACER_RESET_NO_FADE_OUT,
	ORR_RACER_QUIT_FADE_OUT,
	ORR_RACER_RESET_CREATE,
	ORR_RACER_RESET_PLACE,
	ORR_RACER_RESET_GROUND_CLAMP,
	ORR_RACER_QUIT_PLACE,
	ORR_RACER_RESET_FADE_IN,
	ORR_RACER_QUIT_FADE_IN,
	ORR_RACER_RESET_WAIT,
	ORR_RACER_QUIT_EXIT
ENDENUM

// SPR Race Type.
ENUM ORR_RACE_TYPE_ENUM
	ORR_RACE_TYPE_PLANE,
	ORR_RACE_TYPE_OFFROAD,
	ORR_RACE_TYPE_TRIATHLON
ENDENUM

// SPR Race Setup.
ENUM ORR_RACE_SETUP_ENUM
	ORR_RACE_SETUP_INIT,
	ORR_RACE_SETUP_LOAD_INIT,
	ORR_RACE_SETUP_LOAD_WAIT,
	ORR_RACE_SETUP_CREATE_INIT,
	ORR_RACE_SETUP_CREATE_WAIT,
	ORR_RACE_SETUP_WAIT,
	ORR_RACE_SETUP_CLEANUP
ENDENUM

// SPR Race Update.
ENUM ORR_RACE_UPDATE_ENUM
	ORR_RACE_UPDATE_INIT,
	ORR_RACE_UPDATE_COUNTDOWN,
	ORR_RACE_UPDATE_FINISH,
	ORR_RACE_UPDATE_TAXI_IN,
	ORR_RACE_UPDATE_LEADERBOARD_WAIT,
	ORR_RACE_UPDATE_LEADERBOARD,
	ORR_RACE_UPDATE_FADE_OUT_BEFORE_FINISH,
	ORR_RACE_UPDATE_FADE_IN_BEFORE_FINISH,
	ORR_RACE_WAIT_BEFORE_RESTART_CAMERA_PAN,
	ORR_RACE_RESTART_CAMERA_PAN,
	ORR_RACE_UPDATE_WAIT,
	ORR_RACE_UPDATE_OUTRO,
	ORR_RACE_UPDATE_OUTRO_WAIT,
	ORR_RACE_UPDATE_OUTRO_SYNCH,
	ORR_RACE_UPDATE_CLEANUP
ENDENUM

// SPR Race Gate stunt types
ENUM ORR_RACE_STUNT_GATE_ENUM
	ORR_RACE_STUNT_GATE_NORMAL,
	ORR_RACE_STUNT_GATE_INVERTED, 
	ORR_RACE_STUNT_GATE_SIDE_LEFT,
	ORR_RACE_STUNT_GATE_SIDE_RIGHT
ENDENUM

//SPR Gate check types
ENUM ORR_RACE_GATE_STATUS
	ORR_RACE_GATE_STATUS_INVALID = -1,
	ORR_RACE_GATE_STATUS_PASS,
	ORR_RACE_GATE_STATUS_PASS_INNER,
	ORR_RACE_GATE_STATUS_PASS_OUTTER,
	ORR_RACE_GATE_STATUS_KNIFE_INNER,
	ORR_RACE_GATE_STATUS_INVERT_INNER,
	ORR_RACE_GATE_STATUS_KNIFE_OUTTER,
	ORR_RACE_GATE_STATUS_INVERT_OUTTER,
	ORR_RACE_GATE_STATUS_MISSED_GATE,
	ORR_RACE_GATE_STATUS_STUNT_KL,			// left knife stunt gate
	ORR_RACE_GATE_STATUS_STUNT_KL_HIT,		// left knife stunt gate hit, no stunt
	ORR_RACE_GATE_STATUS_STUNT_KL_MISS,		// left knife stunt gate miss
	ORR_RACE_GATE_STATUS_STUNT_KR,			// right knife stunt gate
	ORR_RACE_GATE_STATUS_STUNT_KR_HIT,		// right knife stunt gate hit, no stunt
	ORR_RACE_GATE_STATUS_STUNT_KR_MISS,		// right knife stunt gate miss
	ORR_RACE_GATE_STATUS_STUNT_INV,			// inverted stunt gate
	ORR_RACE_GATE_STATUS_STUNT_INV_HIT,		// inverted stunt gate hit, no stunt
	ORR_RACE_GATE_STATUS_STUNT_INV_MISS,	// inverted stunt gate miss
	ORR_RACE_GATE_STATUS_INCOMPLETE
ENDENUM

//SPR Gate check types
ENUM ORR_RACE_CHECKPOINT_TYPE
	ORR_CHKPT_OFFROAD_DEFAULT = 0,
	ORR_CHKPT_OFFROAD_FINISH,
	ORR_CHKPT_STUNT_DEFAULT,
	ORR_CHKPT_STUNT_STUNT,
	ORR_CHKPT_STUNT_FINISH,
	ORR_CHKPT_TRI_SWIM,
	ORR_CHKPT_TRI_BIKE,
	ORR_CHKPT_TRI_RUN,
	ORR_CHKPT_TRI_FINISH,
	ORR_CHKPT_TRI_FIRST_TRANS,
	ORR_CHKPT_TRI_SECOND_TRANS
ENDENUM

ENUM ORR_RACE_GATE_FLAGS
	ORR_RACE_GATE_FLAG_AP_IGNORE_GROUND 		= BIT0,
	ORR_RACE_GATE_FLAG_CHECK_VEHICLE 			= BIT1,
	ORR_RACE_GATE_FLAG_DO_NOT_SNAP_TO_GROUND 	= BIT2
ENDENUM

ENUM ORR_SC_SCREEN_TEXT
	ORR_SC_MAIN_TITLE,
	ORR_SC_SCORE_TITLE,
	ORR_SC_AWARD_TXT,
	ORR_SC_SCORE_TXT,
	ORR_SC_HISCORE_TXT,
	ORR_SC_TOTAL_TXT,
	ORR_SC_WEAPON_TXT,
	ORR_SC_FIRED_TXT,
	ORR_SC_HITS_TXT,
	ORR_SC_ACC_TXT,
	ORR_SC_TIMEREM_TXT,
	ORR_SC_AWARDVAL_TXT,
	ORR_SC_SCOREVAL_TXT,
	ORR_SC_HISCOREVAL_TXT,
	ORR_SC_TOTALVAL_TXT,
	ORR_SC_WEAPONVAL_TXT,
	ORR_SC_BRONZE_GOAL_TXT,
	ORR_SC_SILVER_GOAL_TXT,
	ORR_SC_GOLD_GOAL_TXT
ENDENUM

ENUM ORR_SC_SCREEN_SPRITE
	ORR_SCREEN_MAIN_BACKGROUND,
	ORR_SC_SOCIALCLUB_IMG,
	ORR_SC_HITS_IMG,
	ORR_SC_MEDAL_AWARD_IMG,
	ORR_SC_BRONZE_IMG,
	ORR_SC_SILVER_IMG,
	ORR_SC_GOLD_IMG
ENDENUM

ENUM ORR_SC_SCREEN_RECT
	ORR_SC_SCORE_BG,
	ORR_SC_SCORE_EDGE,
	ORR_SC_HIT_BG,
	ORR_SC_INFO1_BG,
	ORR_SC_INFO2_BG,
	ORR_SC_INFO3_BG,
	ORR_SC_INFO4_BG,
	ORR_SC_INFO5_BG,
	ORR_SC_INFO6_BG,
	ORR_SC_INFO7_BG,
	ORR_SC_INFO8_BG,
	ORR_SC_BRONZE_BG,
	ORR_SC_SILVER_BG,
	ORR_SC_GOLD_BG,
	ORR_SC_BRONZE_OVERLAY,
	ORR_SC_SILVER_OVERLAY,
	ORR_SC_GOLD_OVERLAY
ENDENUM

ENUM ORR_PLAYER_RESPAWN_STATE
	ORR_RESPAWN_SET,
	ORR_RESPAWN_INIT,
	ORR_RESPAWN_CHECK,
	ORR_RESPAWN_ACTIVE,
	ORR_RESPAWN_WAIT,
	ORR_RESPAWN_INVALID
ENDENUM

// -----------------------------------
// STRUCTURES
// -----------------------------------

// SPR Main.
STRUCT ORR_MAIN_STRUCT
	INT iSPRMode
	ORR_MAIN_RUN_ENUM eRun
	StructTimer tSetup
	ORR_MAIN_SETUP_ENUM eSetup
	ORR_MAIN_UPDATE_ENUM eUpdate
	structTimer tLoadTimeout
ENDSTRUCT

// SPR Display.
STRUCT ORR_DISPLAY_STRUCT	
	StructTimer tCnt
ENDSTRUCT

// SPR Input.
STRUCT ORR_INPUT_STRUCT
	INT iLS_X, iLS_Y
	INT iRS_X, iRS_Y
ENDSTRUCT

// SPR Camera.
STRUCT ORR_CAMERA_STRUCT
	CAMERA_INDEX Lst, Cur
	OBJECT_INDEX DummyCam
	FLOAT fZoom
	ORR_CAMERA_MODE_ENUM eMode
	ORR_CAMERA_FOCUS_ENUM eFocus
ENDSTRUCT

STRUCT ORR_HINT_CAM_STRUCT
	BOOL bActive
	FLOAT fCamTimer
//	ENTITY_INDEX Entity
	VECTOR Coord
//	PS_HINT_CAM_ENUM HintType
	CAMERA_INDEX CustomCam
ENDSTRUCT

// SPR Gate.
STRUCT ORR_GATE_STRUCT	
	//INT iPosX
	//INT iPosY
	VECTOR vPos
	FLOAT fRadius
	FLOAT fHeading
	FLOAT fHeightCheck 		// To Fix Bug # 666112 - DS
	BLIP_INDEX Blip
	CHECKPOINT_INDEX Chkpnt
	ORR_RACE_CHECKPOINT_TYPE eChkpntType
	ORR_RACE_STUNT_GATE_ENUM eStuntType
	INT iGateFlags //auto pilot should ignore ground check (185332)	
ENDSTRUCT

// SPR Racer.
STRUCT ORR_RACER_STRUCT
	TEXT_LABEL_31 szName
	PED_INDEX Driver
	VEHICLE_INDEX Vehicle
	BLIP_INDEX Blip
	INT iGateCur, iRank
	FLOAT fClockTime	
	FLOAT fPlsMnsLst
	FLOAT fPlsMnsTot
	VECTOR vStartPos
	FLOAT fStartHead
	PED_TYPE eDriverType
	MODEL_NAMES eDriverModel
	MODEL_NAMES eVehicleModel
	ORR_RACER_RESET_ENUM eReset
	structPedsForConversation pedConvo
	structTimer	resetTimer
	BOOL		bStarted = FALSE
	BOOL		bExploded = FALSE
	INT			iCurrentWaypointNode = 0
	structTimer	startTimer
	structTimer	vehRespawnTimeout
	INT			iSpeedVolumeCount = 0
	INT			iCurrentSpeedVolume = -1
	
	#IF ORR_RACE_IS_TRIATHLON
		FLOAT fMinMoveBlendRatioOnFoot, 	fMaxMoveBlendRatioOnFoot		
		FLOAT fMinMoveBlendRatioInWater, 	fMaxMoveBlendRatioInWater
		FLOAT fMinBikeSpeed, 				fMaxBikeSpeed
		
		INT iSkillPlacement
		
		TRI_RACER_COMPETE_MODE eCompeteMode
		
		structTimer timerInCurrentCompeteMode
		structTimer timerRacerVehicleIdle
		
		BOOL bHasBeenOnABike
		
		STRING szCurrentBikeRecordingName	// Necessary for Ironman, to switch a racer's waypoint vehicle recoring.
	#ENDIF
	
ENDSTRUCT

STRUCT ORR_PLAYER_VEHICLE_STRUCT
	INT iVehicleColor
	MODEL_NAMES vehicleName = DUMMY_MODEL_FOR_SCRIPT
	VEHICLE_SETUP_STRUCT sPlayerVehicle
ENDSTRUCT

STRUCT ORR_SPEED_RESTRICTION_VOLUME
	VECTOR vPos1, vPos2
	FLOAT fWidth
	FLOAT fSpeed
ENDSTRUCT

// SPR Race.
STRUCT ORR_RACE_STRUCT	
	BOOL bHasStartedCheerSound
	BOOL bRestarting, bFailChecking, bRestartCameraPan
	BOOL bUsingLoaner
	StructTimer tClock
	structTimer	tAudioTimer
	CHECKPOINT_INDEX fadeCheckpoint
	FLOAT fBestClockTime
	FLOAT fBestSplitTime
	INT iFadeAlpha
	INT iNumTimesPlayerShoots = 0
	INT iGateCheck
	INT iGateCnt, iRacerCnt		
	ORR_DISPLAY_STRUCT Display
	SCRIPT_SCALEFORM_BIG_MESSAGE bigMessageUI
	StructTimer tUpdate
	ORR_RACE_SETUP_ENUM eSetup
	ORR_RACE_UPDATE_ENUM eUpdate	
	ORR_GATE_STRUCT sGate[ORR_GATE_MAX]
	ORR_RACER_STRUCT Racer[ORR_RACER_MAX]
	MEGA_PLACEMENT_TOOLS uiScorecard
	SCALEFORM_INDEX uiLeaderboard
	END_SCREEN_DATASET esd
	ORR_RETURN_VALUES_ENUM eRetVal
	
	#IF IS_DEBUG_BUILD
		BOOL bPlayerUsedDebugSkip = FALSE
	#ENDIF
		
	BOOL		bAttackedDuringRace = FALSE
	BOOL		bRacersFleeing = FALSE
	
	VEHICLE_INDEX viStartingVeh[10]
	VEHICLE_INDEX viClosestVeh
	INT iLastLine
	INT iGateSkipDist = 1		// The amount of gates behind the player that AI will spawn to for rubberbanding (defaulting to 1, should be tweaked for difficulty)
	INT iGateLagTolerance = 3	// The amount of gates behind the player an AI can be before they jump (defaulting to 3, should be tweaked for difficulty)
	VECTOR vFinalSpawnPos 		// The location that the player will be spawned at when the race ends (should be set in orr_course_data.sch)
	FLOAT fFinalSpawnHeading	// The heading that the player will be spawned at when the race ends
	structTimer tRubberBandTimer
	structTimer tPlayerUsedWeapon	//Starts as soon as players shoots, and is restarted whenever you want
	FLOAT fStartBoostMin		// The speed that the racers will be launched off at initially
	FLOAT fStartBoostMax		// The speed that the racers will interpolate to on initial launch
	
	BOOL bBoostGoing = FALSE
	BOOL bCantResetHelpDisplaying = FALSE
	ORR_PLAYER_RESPAWN_STATE ePlayerRespawnState
	BOOL bPlayerRespawning = FALSE
	BOOL bShowLeaderboard = FALSE
	
	BOOL bCutsceneSkipped = FALSE
	BOOL bCutsceneTasked1 = FALSE
	BOOL bCutsceneTasked2 = FALSE
	BOOL bUseAlternateOutroCam = FALSE
	
	VECTOR vPlayerCarPos
	FLOAT fPlayerCarHeading
	
	ORR_SPEED_RESTRICTION_VOLUME tSpeedVolumes[ORR_MAX_SPEED_VOLUMES]
	INT iSpeedVolumeCount = 0
	
		
	VECTOR vFinishCamPos
	VECTOR vFinishCamRot
	VECTOR vLBDCamPos
	VECTOR vLBDCamRot
	FLOAT fFOV
	
	VECTOR vFinishLBDCamPos
	VECTOR vFinishLBDCamRot
	VECTOR vFinishLBDCam2Pos
	VECTOR vFinishLBDCam2Rot
	VECTOR vFinishLBDCam2VehPos
	VECTOR vFinishLBDCam2VehRot
	FLOAT fFinishInterpTime
	
	FLOAT fIntroCam1Time = 5.0
	FLOAT fIntroCam2Time = 2.5
	FLOAT fIntroCam3Time = 5.0	
	
	VECTOR vDriveTo[3]
	FLOAT fDriveWaitTime = 3.0
	FLOAT fStartTime[2]
	FLOAT fIntroSpeed[2]
	
	FLOAT fRecordingStartTime = 1.0
	
	VECTOR vFinalPlayerVehPos
	FLOAT fFinalPlayerVehHeading
	
	INT iCheerMasterSound=-1
ENDSTRUCT

#IF IS_DEBUG_BUILD

// SPR Widget toggle.
STRUCT ORR_WIDGET_TOGGLE
	BOOL bCur, bLst
ENDSTRUCT

// SPR Widget button.
STRUCT ORR_WIDGET_BUTTON
	BOOL bCur
ENDSTRUCT

// SPR Widget int.
STRUCT ORR_WIDGET_INT
	INT iCur, iLst
ENDSTRUCT

// SPR Widget float.
STRUCT ORR_WIDGET_FLOAT
	FLOAT fCur, fLst
ENDSTRUCT

// SPR Widget vector.
STRUCT ORR_WIDGET_VECTOR
	VECTOR vCur, vLst
ENDSTRUCT

// SPR Widget Textbox.
STRUCT ORR_WIDGET_TEXTBOX
	TEXT_WIDGET_ID WidgetID
	TEXT_LABEL_31 szCur, szLst
ENDSTRUCT

// SPR Widget Listbox.
STRUCT ORR_WIDGET_LISTBOX
	INT iCur, iLst
ENDSTRUCT

// SPR Widget Gates.
STRUCT ORR_WIDGET_GATES
	WIDGET_GROUP_ID GroupID
	ORR_WIDGET_BUTTON Create
	ORR_WIDGET_BUTTON Delete
	ORR_WIDGET_TOGGLE ShowAll
	ORR_WIDGET_BUTTON SnapSelected
	ORR_WIDGET_INT Selection
	ORR_WIDGET_VECTOR Position
	ORR_WIDGET_FLOAT Radius
	ORR_WIDGET_LISTBOX ChkpntType
	ORR_WIDGET_LISTBOX StuntType
	ORR_WIDGET_LISTBOX RelMode
	ORR_WIDGET_TOGGLE RelAbs
	ORR_WIDGET_VECTOR RelMove
ENDSTRUCT

// SPR Widget Racers.
STRUCT ORR_WIDGET_RACERS
	WIDGET_GROUP_ID GroupID
	ORR_WIDGET_BUTTON Create
	ORR_WIDGET_BUTTON Delete
	ORR_WIDGET_TOGGLE ShowAll
	ORR_WIDGET_INT Selection
	ORR_WIDGET_TEXTBOX Name
	ORR_WIDGET_VECTOR Position
	ORR_WIDGET_FLOAT Heading
	ORR_WIDGET_LISTBOX DriverType
	ORR_WIDGET_LISTBOX DriverModel
	ORR_WIDGET_LISTBOX VehicleModel
	ORR_WIDGET_LISTBOX RelMode
	ORR_WIDGET_TOGGLE RelAbs
	ORR_WIDGET_VECTOR RelMove
	BOOL bRacerQueue[ORR_RACER_MAX]
ENDSTRUCT

// SPR Widget Race.
STRUCT ORR_WIDGET_RACE
	WIDGET_GROUP_ID GroupID
	ORR_WIDGET_TEXTBOX Error
	//ORR_WIDGET_FLOAT Time
	ORR_WIDGET_TEXTBOX Name
	ORR_WIDGET_TEXTBOX FileName
	ORR_WIDGET_BUTTON Create
	ORR_WIDGET_LISTBOX Pick
	ORR_WIDGET_BUTTON Load
	ORR_WIDGET_BUTTON Save
	ORR_WIDGET_BUTTON Export
	ORR_WIDGET_GATES Gates
	ORR_WIDGET_RACERS Racers
	ORR_RACE_STRUCT Race
ENDSTRUCT

// SPR Widget Race Mode.
STRUCT ORR_WIDGET_RACEMODE
	WIDGET_GROUP_ID GroupID
ENDSTRUCT

// SPR Widget Edit Mode.
STRUCT ORR_WIDGET_EDITMODE
	WIDGET_GROUP_ID GroupID
	ORR_WIDGET_LISTBOX CamMode
	ORR_WIDGET_LISTBOX CamFocus
	ORR_WIDGET_RACE Race
	ORR_INPUT_STRUCT Input
	ORR_CAMERA_STRUCT Camera
ENDSTRUCT

// SPR Widget Main.
STRUCT ORR_WIDGET_MAIN
	WIDGET_GROUP_ID GroupID
	ORR_WIDGET_LISTBOX SPRMode
	ORR_WIDGET_RACEMODE RaceMode
	ORR_WIDGET_EDITMODE EditMode
ENDSTRUCT

#ENDIF	//	IS_DEBUG_BUILD

// SPR Master.
STRUCT ORR_MASTER_STRUCT
	ORR_RACE_TYPE_ENUM eRaceType
	VEHICLE_INDEX PlayerVeh, savedVeh
	ORR_RACE_CHECKPOINT_TYPE eDefChkPntType
	VECTOR vDefRcrPos
	FLOAT fDefRcrHead
	PED_TYPE eDefDrvType
	MODEL_NAMES eDefDrvModel
	MODEL_NAMES eDefVehModel
	INT iRaceCur, iRaceCnt
	FLOAT fRaceTime[NUM_OFFROAD_RACES]
	TEXT_LABEL_31 szRaceName[NUM_OFFROAD_RACES]
	TEXT_LABEL_31 szRaceFileName[NUM_OFFROAD_RACES]
	FLOAT fTimeGold[NUM_OFFROAD_RACES]
	FLOAT fTimeBronze[NUM_OFFROAD_RACES]
	BOOL bRecreateSavedVeh = FALSE
	
	INT iBestRank
	
	SIMPLE_USE_CONTEXT uiInput
	ORR_PLAYER_VEHICLE_STRUCT sPlayerVehicleData
	
	CAMERA_INDEX camFinalLBD
	CAMERA_INDEX camFinalLBD2
	STRUCTTIMER tmrFinishCut
	
	BOOL bPlayerStarted = FALSE
	BOOL bIntroFXDone = FALSE
	BOOL bAudioStarted = FALSE
	BOOL bRespawnPressed = FALSE
	BOOL bIPLActive = FALSE
	BOOL bWaterFailHelpActive = FALSE
	BOOL bDistFailHelpActive = FALSE
	
	BOOL bSuccessful
	INT iReadStage, iLoadStage
	INT iPersonalBest, iGlobalBest
	
	structTimer tmrCountdown
	structTimer	tmrFail
	PED_VARIATION_STRUCT tPedProps
	
	ORR_FAIL_CASES eFailReason = ORR_FAIL_INVALID
	MG_FAIL_SPLASH splashState
	STRING button
	//SCRIPT_SCALEFORM_UI uiInput
ENDSTRUCT


// -----------------------------------
// VARIABLES
// -----------------------------------

// SPR Master Data (DO NOT MOVE).
ORR_MASTER_STRUCT ORR_Master
BOOL bQuitting = FALSE
BOOL bFailedNoBlips = FALSE


// -----------------------------------
// PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_INCREASE_HINT_CAM_TIMER(ORR_HINT_CAM_STRUCT &ORR_HINT_CAM)
	ORR_HINT_CAM.fCamTimer += GET_FRAME_TIME()
ENDPROC

FUNC FLOAT ORR_GET_HINT_CAM_TIMER(ORR_HINT_CAM_STRUCT &ORR_HINT_CAM)
	RETURN ORR_HINT_CAM.fCamTimer
ENDFUNC

PROC ORR_RESET_HINT_CAM_TIMER(ORR_HINT_CAM_STRUCT &ORR_HINT_CAM)
	ORR_HINT_CAM.fCamTimer = 0
ENDPROC
