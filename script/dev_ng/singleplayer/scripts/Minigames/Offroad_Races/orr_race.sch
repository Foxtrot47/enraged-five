// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	ORR_Race.sch
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Single Player Races - Race procs/functions file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "ORR_Head.sch"
USING "ORR_Helpers.sch"
USING "ORR_Gate.sch"
USING "chase_hint_cam.sch"
USING "ORR_Racer.sch"
USING "minigame_stats_tracker_helpers.sch"
USING "CompletionPercentage_public.sch"
USING "socialclub_leaderboard.sch"

structTimer taxiTime
structTimer resetTimer
//structTimer manualResetTimer
structTimer tShutdownTimer
INT iIntroSFXIDX
BOOL bDebugFailure = FALSE

BLIP_INDEX returnToVehBlip //only used in off-road but may need for bike in Tris
BOOL ORR_USE_WAYPOINT_RECORDING = TRUE
INT iFrameCounter = 0
INT iOutroSynch
FLOAT fATVSpeedMod = 0.65
FLOAT fBikeSpeedMod = 0.83
CONST_FLOAT DISABLED_INPUT_DEAD_ZONE 0.2
BOOL bIsLBDContextSet = FALSE
BOOL bRankPredictReady = FALSE
BOOL bRankWritten		= FALSE
BOOL bIsJustOnline		= FALSE

// Determine when to initialize the complex use contexts for the scorecard and leaderboard, so they aren't set every frame.
BOOL bIsOutroContextSet 		= FALSE
BOOL bOffroadProfileReady		= FALSE

#IF NOT DEFINED(GET_HUD_STUNT_STRING)
FUNC STRING GET_HUD_STUNT_STRING()
	RETURN NULL_STRING()
ENDFUNC
#ENDIF

#IF NOT DEFINED(iSPRStuntFinishCam)
CAMERA_INDEX iSPRStuntFinishCam
#ENDIF

#IF NOT DEFINED(GET_HUD_STUNT_FLOAT)
FUNC FLOAT GET_HUD_STUNT_FLOAT()
	RETURN -1.0
ENDFUNC
#ENDIF

#IF NOT DEFINED(GET_HUD_STUNT_INT)
FUNC INT GET_HUD_STUNT_INT()
	RETURN ENUM_TO_INT(HUD_COLOUR_WHITE)
ENDFUNC
#ENDIF

#IF NOT DEFINED(RUN_OFFROAD_AUDIO)
PROC RUN_OFFROAD_AUDIO(ORR_RACE_STRUCT& Race)
IF (Race.iRacerCnt = -1)
	PRINTINT(Race.iRacerCnt)
	PRINTNL()
ENDIF
ENDPROC
#ENDIF

#IF NOT DEFINED(stop_vehicle_recordings)
PROC stop_vehicle_recordings(ORR_RACE_STRUCT& Race)	
	Race.iGateCnt = Race.iGateCnt
ENDPROC
#ENDIF

#IF NOT DEFINED(Disable_Airport_Icons)
PROC Disable_Airport_Icons()
/*	IF ORR_Master.eRaceType = ORR_RACE_TYPE_PLANE
		CDEBUG1LN(DEBUG_OR_RACES, "Inside dummy Disable_Airport_Icons proc")
	ENDIF */
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Gate_Reward)
PROC ORR_Gate_Reward(ORR_RACER_STRUCT& Racer, ORR_RACE_GATE_STATUS status)	
	Racer.fPlsMnsTot = Racer.fPlsMnsTot
	status = status	
ENDPROC
#ENDIF


#IF NOT DEFINED(ORR_Racer_ClockTime_Penalty)
PROC ORR_Racer_ClockTime_Penalty(ORR_RACER_STRUCT& Racer, FLOAT fPenalty)
	Racer.fPlsMnsTot = Racer.fPlsMnsTot
	fPenalty = fPenalty
ENDPROC
#ENDIF

#IF NOT DEFINED(runGateTimers)
PROC runGateTimers(ORR_RACER_STRUCT& Racer, BOOL bRacerIsPlayer, INT iGateCheck)
	Racer.fPlsMnsTot = Racer.fPlsMnsTot	
	bRacerIsPlayer = bRacerIsPlayer
	iGateCheck = iGateCheck	
ENDPROC
#ENDIF

#IF NOT DEFINED(rewardPlayer)
PROC rewardPlayer()
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Race_Racer_AutoPilot)
PROC ORR_Race_Racer_AutoPilot(ORR_RACE_STRUCT& Race)	
	Race.fBestClockTime = Race.fBestClockTime	
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Race_Racer_AutoCircle)
PROC ORR_Race_Racer_AutoCircle(ORR_RACE_STRUCT& Race)	
	Race.fBestClockTime = Race.fBestClockTime
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Racer_AutoPilot)
PROC ORR_Racer_AutoPilot(ORR_RACER_STRUCT& Racer, FLOAT fSpeed, BOOL bRotInterp)
	Racer.fStartHead = Racer.fStartHead
	fSpeed = fSpeed	
 	bRotInterp = bRotInterp
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Racer_AutoCircle)
PROC ORR_Racer_AutoCircle(ORR_RACER_STRUCT& Racer, FLOAT fSpeed, BOOL bRotInterp)
	Racer.fStartHead = Racer.fStartHead
	fSpeed = fSpeed
	bRotInterp= bRotInterp	
ENDPROC
#ENDIF

#IF NOT DEFINED(Tri_Showing_Player_Finished_Race)
FUNC BOOL Tri_Showing_Player_Finished_Race()	
	RETURN FALSE
ENDFUNC
#ENDIF

#IF NOT DEFINED(SET_TRI_RACERS_ONCE_RACE_ENDS)
PROC SET_TRI_RACERS_ONCE_RACE_ENDS(ORR_RACE_STRUCT& Race)	
	Race.iGateCnt = Race.iGateCnt
ENDPROC
#ENDIF

#IF NOT DEFINED(SET_CUTSCENE_CAMERA_AT_END_OF_RACE)
PROC SET_CUTSCENE_CAMERA_AT_END_OF_RACE(BOOL bActivateCam)
	bActivateCam = bActivateCam
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Init_Scorecard)
PROC ORR_Init_Scorecard(ORR_RACE_STRUCT &Race)	
	Race.iGateCheck = Race.iGateCheck
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Draw_Scorecard)
PROC ORR_Draw_Scorecard(ORR_RACE_STRUCT &Race, INT	&iPlayerIndex)		
	Race.iGateCheck = Race.iGateCheck
	iPlayerIndex = iPlayerIndex
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_NEW_DRAW_SCORECARD)
PROC ORR_NEW_DRAW_SCORECARD(ORR_RACE_STRUCT &Race)		
	Race = Race
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_SETUP_SCORECARD)
PROC ORR_SETUP_SCORECARD(ORR_RACE_STRUCT &Race)		
	CPRINTLN( DEBUG_OR_RACES, "IN A BLANK SCORECARD SETUP... SHOULD NOT BE HERE!!!" )
	Race = Race
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Setup_Leaderboard)
PROC ORR_Setup_Leaderboard(ORR_RACE_STRUCT &Race)		
	Race = Race
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Draw_Leaderboard)
PROC ORR_Draw_Leaderboard(ORR_RACE_STRUCT &Race)		
	Race = Race
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Cleanup_Leaderboard)
PROC ORR_Cleanup_Leaderboard(ORR_RACE_STRUCT &Race)		
	Race = Race
ENDPROC
#ENDIF

#IF NOT DEFINED(Cleanup_Finish_Camera)
PROC Cleanup_Finish_Camera()
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Race_Stunt_Manage_Finish_Camera)
PROC ORR_Race_Stunt_Manage_Finish_Camera(VEHICLE_INDEX& iPlayersPlane)		
	iPlayersPlane = iPlayersPlane
ENDPROC
#ENDIF

#IF NOT DEFINED(ORR_Race_Manage_Race_Beats)
PROC ORR_Race_Manage_Race_Beats(INT iGateCur, BOOL bFailChecking)			
	iGateCur = iGateCur
	bFailChecking = bFailChecking	
ENDPROC
#ENDIF

// -----------------------------------
// HELPER PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_Race_FakeRacer_Populate(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_FakeRacer_Populate")
	TEXT_LABEL_31 szRacerName[ORR_UI_LB_RACER_LIMIT - 1]
	ORR_RacerName_Populate(szRacerName)
	FLOAT fBestClockTime = Race.fBestClockTime
	IF (fBestClockTime > Race.Racer[0].fClockTime)
		fBestClockTime = Race.Racer[0].fClockTime
	ENDIF
	WHILE (Race.iRacerCnt < ORR_UI_LB_RACER_LIMIT)
		Race.Racer[Race.iRacerCnt].szName = szRacerName[Race.iRacerCnt - 1]
		Race.Racer[Race.iRacerCnt].fClockTime = GET_RANDOM_FLOAT_IN_RANGE(fBestClockTime, fBestClockTime * 1.25)
		++Race.iRacerCnt
	ENDWHILE
ENDPROC

PROC ORR_Race_ClockTime_SimAI(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_ClockTime_SimAI")
	
	INT i
	REPEAT Race.iRacerCnt i
		IF (Race.Racer[i].iGateCur <> Race.iGateCnt)
			Race.Racer[i].fClockTime = Race.Racer[0].fClockTime + ((Race.iGateCnt - Race.Racer[i].iGateCur) * GET_RANDOM_FLOAT_IN_RANGE(0.5, 2.5)) 
		ENDIF
	ENDREPEAT
ENDPROC

PROC ORR_Race_Rank_CalcFinal(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Rank_CalcFinal")
	INT i, j, iRank
	REPEAT Race.iRacerCnt i
		iRank = 1
		REPEAT Race.iRacerCnt j
			IF (i <> j)
				IF (Race.Racer[i].fClockTime > Race.Racer[j].fClockTime)
					++iRank
				// TODO: Should be doing something to offset tied ranks for leaderboard.
				// TODO: Also make sure if 2 guys share a rank, the next rank is 1 higher.
				//ELIF (Race.Racer[i].iGateCur = Race.Racer[j].iGateCur)
				//	++iRank
				ENDIF
			ENDIF
		ENDREPEAT
		Race.Racer[i].iRank = iRank	
	ENDREPEAT
ENDPROC

// TODO: My method of finding player racer here is different from above.
//		 May want to come up with a safe way we know which racer is player.
PROC ORR_Race_RankClock_FindPlayerBest(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_FindPlayerBest")
	INT i
	REPEAT Race.iRacerCnt i
		IF NOT IS_ENTITY_DEAD(Race.Racer[i].Driver)
			IF (Race.Racer[i].Driver = PLAYER_PED_ID())
				INT iBestRank = ORR_Global_BestRank_Get(ORR_Master.iRaceCur)
				IF (iBestRank <= 0) OR (iBestRank > Race.Racer[i].iRank)
					IF ORR_Master.iRaceCur >= 0 AND ORR_Master.iRaceCur < ENUM_TO_INT(NUM_OFFROAD_RACES)
						ORR_Global_BestRank_Set(ORR_Master.iRaceCur, Race.Racer[i].iRank)
					ENDIF
				ENDIF
				FLOAT fBestTime = ORR_Global_BestTime_Get(ORR_Master.iRaceCur)
				IF (fBestTime <= 0.0) OR (fBestTime > Race.Racer[i].fClockTime)
					IF ORR_Master.iRaceCur >= 0 AND ORR_Master.iRaceCur < ENUM_TO_INT(NUM_OFFROAD_RACES)
						CPRINTLN( DEBUG_OR_RACES, "Setting best time as ", fBestTime )
						ORR_Global_BestTime_Set(ORR_Master.iRaceCur, Race.Racer[i].fClockTime)
					ENDIF
				ELSE
					CPRINTLN( DEBUG_OR_RACES, "Best time is better than clock time ", fBestTime, " clock time: ", Race.Racer[i].fClockTime )
				ENDIF
					
				
			#IF IS_DEBUG_BUILD
				IF NOT Race.bPlayerUsedDebugSkip
			#ENDIF
//				IF IS_PLAYER_ONLINE()
//					ORR_WRITE_TIME_TO_LEADERBOARD(INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur), Race.Racer[0].fClockTime, Race.Racer[0].iRank)
//				ENDIF
					
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF

				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Removes all racer blips and sets the fail flag so blips won't be recreated.
PROC ORR_REMOVE_BLIPS_ON_FAIL(ORR_RACE_STRUCT& Race)
	IF NOT bFailedNoBlips
		bFailedNoBlips = TRUE
		
		INT idx
		FOR idx = 1 TO Race.iRacerCnt - 1
			IF DOES_BLIP_EXIST(Race.Racer[idx].Blip)
				REMOVE_BLIP(Race.Racer[idx].Blip)
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC ORR_Race_Leaderboard_Init_Scorecard(ORR_RACE_STRUCT& Race)		
	
	ORR_SETUP_SCORECARD(Race)
//	ORR_Init_Scorecard(Race)
	ORR_Setup_Leaderboard(Race)
	SETTIMERA(0)

	IF ((ORR_Master.iRaceCur+1) = 1)
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_OFF1)
		CDEBUG1LN(DEBUG_OR_RACES, "REGISTERED SCRIPT COMPLETION FOR OFFROAD RACE1")
	ELIF ((ORR_Master.iRaceCur+1) = 2)
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_OFF2)
		CDEBUG1LN(DEBUG_OR_RACES, "REGISTERED SCRIPT COMPLETION FOR OFFROAD RACE2")
	ELIF ((ORR_Master.iRaceCur+1) = 3)
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_OFF3)
		CDEBUG1LN(DEBUG_OR_RACES, "REGISTERED SCRIPT COMPLETION FOR OFFROAD RACE3")
	ELIF ((ORR_Master.iRaceCur+1) = 4)
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_OFF4)
		CDEBUG1LN(DEBUG_OR_RACES, "REGISTERED SCRIPT COMPLETION FOR OFFROAD RACE4")
	ELIF ((ORR_Master.iRaceCur+1) = 5)
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_OFF5)
		CDEBUG1LN(DEBUG_OR_RACES, "REGISTERED SCRIPT COMPLETION FOR OFFROAD RACE5")
	ELIF ((ORR_Master.iRaceCur+1) = 6)
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_OFF6)
		CDEBUG1LN(DEBUG_OR_RACES, "REGISTERED SCRIPT COMPLETION FOR OFFROAD RACE6")
	ENDIF

	SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)

	//MAKE_AUTOSAVE_REQUEST()
ENDPROC

PROC ORR_CREATE_OUTRO_CAMS(ORR_RACE_STRUCT& Race)
	IF DOES_CAM_EXIST(cam1wide)
		DESTROY_CAM(cam1wide)
	ENDIF
	
	cam1wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, Race.vFinishLBDCamPos, Race.vFinishLBDCamRot, 40.0000)
	
	IF DOES_CAM_EXIST(cam2wide)
		DESTROY_CAM(cam2wide)
	ENDIF	
	
	IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
		IF NOT Race.bUseAlternateOutroCam
			cam2wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, Race.vFinishLBDCam2Pos, Race.vFinishLBDCam2Rot, 40.0000)
		ELSE
			cam2wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, Race.vFinishLBDCam2VehPos, Race.vFinishLBDCam2VehRot, 40.0000)
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(cam3wide)
		DESTROY_CAM(cam3wide)
	ENDIF
	
	cam3wide = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
	
ENDPROC

PROC ORR_CREATE_OUTRO_SCENE(INT& iSynchScene)
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		EXIT
	ENDIF
	iSynchScene = CREATE_SYNCHRONIZED_SCENE( GET_ENTITY_COORDS(PLAYER_PED_ID()), <<0,0,GET_ENTITY_HEADING(PLAYER_PED_ID())>> )
ENDPROC

FUNC ORR_RETURN_VALUES_ENUM ORR_Race_Draw_Scorecard(ORR_RACE_STRUCT& Race)	
	//delay scorecard
	IF TIMERA() < 2000
		RETURN ORV_FALSE
	ENDIF

	// Won the race, award the player with an outfit
	IF Race.Racer[0].iRank <= 3					
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			CASE CHAR_MICHAEL
				IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_MODEL(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOTO_X)
					ADD_HELP_TO_FLOW_QUEUE( "OFF_OUTFIT_M" )
					SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_MODEL(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOTO_X, TRUE)
				ENDIF
			BREAK
			CASE CHAR_FRANKLIN
				IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_MODEL(), COMP_TYPE_OUTFIT, OUTFIT_P1_MOTO_X)
					ADD_HELP_TO_FLOW_QUEUE("OFF_OUTFIT_F")
					SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_MODEL(), COMP_TYPE_OUTFIT, OUTFIT_P1_MOTO_X, TRUE)
				ENDIF
			BREAK
			CASE CHAR_TREVOR
				IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_MODEL(), COMP_TYPE_OUTFIT, OUTFIT_P2_MOTO_X)
					ADD_HELP_TO_FLOW_QUEUE("OFF_OUTFIT_T")
					SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_MODEL(), COMP_TYPE_OUTFIT, OUTFIT_P2_MOTO_X, TRUE)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

	
	
	
	IF NOT IS_RADAR_HIDDEN()
		DISPLAY_RADAR(FALSE)
	ENDIF

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	// B*2239651 - Disabling pause_alternate during end screens (to avoid clash when exiting leaderboards)
	//CLEANUP_SIMPLE_USE_CONTEXT( ORR_Master.uiInput )
	IF TIMERA() > 3000 AND Race.bShowLeaderBoard
		IF IS_PLAYER_ONLINE()
			//CDEBUG1LN(DEBUG_SP_RACES, "[ORR_Race_Draw_Scorecard] Timer is over 3s AND we're showing the leaderboard, so initialize use context for BACK and CANCEL.")
			IF NOT bIsOutroContextSet
				CDEBUG1LN(DEBUG_SP_RACES, "[ORR_Race_Draw_Scorecard] bIsOutroContextSet is FALSE, but we're in the leaderboard, so setting to TRUE.")
				CLEANUP_SIMPLE_USE_CONTEXT( ORR_Master.uiInput )
				INIT_SIMPLE_USE_CONTEXT( ORR_Master.uiInput, FALSE, FALSE, TRUE, TRUE )
				ADD_SIMPLE_USE_CONTEXT_INPUT( ORR_Master.uiInput, "HUD_INPUT53", FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				IF bOffroadProfileReady
					ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SCLB_PROFILE", FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
				ENDIF
				bIsOutroContextSet = TRUE
			ENDIF
			IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL )
				CDEBUG1LN(DEBUG_SP_RACES, "[ORR_Race_Draw_Scorecard] Pressed CANCEL - AND - leaderboard is showing, so set switch leaderboard bool and bIsOutroContextSet to FALSE for reinit.")
				Race.bShowLeaderBoard = !Race.bShowLeaderBoard
				bIsOutroContextSet = FALSE
			ENDIF
			ORR_Draw_Leaderboard(Race.uiLeaderboard, INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur), ORR_Master.szRaceFileName[ORR_Master.iRaceCur])
			UPDATE_SIMPLE_USE_CONTEXT(ORR_Master.uiInput)
			
			IF SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(offroadLBControl)
				IF NOT bOffroadProfileReady
					bOffroadProfileReady = TRUE
					bIsOutroContextSet = FALSE
					CDEBUG1LN(DEBUG_SP_RACES, "[ORR_Race_Draw_Scorecard]Profile is ready, add the button")
				ELSE
					CDEBUG1LN(DEBUG_SP_RACES, "[ORR_Race_Draw_Scorecard]Profile is not ready yet")
				ENDIF
			ENDIF
		ELSE
			IF DO_SIGNED_OUT_WARNING(iBS)
				Race.bShowLeaderBoard = FALSE
				iBS = 0
				CLEANUP_SIMPLE_USE_CONTEXT( ORR_Master.uiInput )
				INIT_SIMPLE_USE_CONTEXT(ORR_Master.uiInput, FALSE, FALSE, TRUE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_CONT2",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_UI_RETRY",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "HUD_INPUT68",		FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			ENDIF
		ENDIF
	ELSE
		//CDEBUG1LN(DEBUG_SP_RACES, "[ORR_Race_Draw_Scorecard] Either timer is under 3s OR we're NOT showing the leaderboard, so initialize use context for A, X and RB.")
		IF NOT bIsOutroContextSet
			CDEBUG1LN(DEBUG_SP_RACES, "[ORR_Race_Draw_Scorecard] bIsOutroContextSet is FALSE, but we're showing the scorecard, so setting to TRUE.")
			CLEANUP_SIMPLE_USE_CONTEXT( ORR_Master.uiInput )
			INIT_SIMPLE_USE_CONTEXT(ORR_Master.uiInput, FALSE, FALSE, TRUE, TRUE)
			ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_CONT2",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
			ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_UI_RETRY",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
			
			IF NOT IS_PLAYER_ONLINE()
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "HUD_INPUT68",	FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			ENDIF
			bIsOutroContextSet = TRUE
		ENDIF
		
		IF IS_PLAYER_ONLINE()
			AND NOT bIsJustOnline
			bIsJustOnline = TRUE
			bIsLBDContextSet = FALSE
			CLEANUP_SIMPLE_USE_CONTEXT( ORR_Master.uiInput )
			INIT_SIMPLE_USE_CONTEXT(ORR_Master.uiInput, FALSE, FALSE, TRUE, TRUE)
			ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_CONT2",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
			ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_UI_RETRY",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
		ELIF NOT IS_PLAYER_ONLINE()
			AND bIsJustOnline
			bIsJustOnline = FALSE
		ENDIF
		
		IF IS_PLAYER_ONLINE()
			IF bIsJustOnline
				AND NOT sclb_rank_predict.bFinishedRead
				ORR_LOAD_SOCIAL_CLUB_LEADERBOARD(INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur), ORR_Master.szRaceFileName[ORR_Master.iRaceCur])
			ENDIF
			
			IF NOT bIsLBDContextSet
				AND sclb_rank_predict.bFinishedRead
				AND sclb_rank_predict.bFinishedWrite
				AND bRankPredictReady
				bIsLBDContextSet = TRUE
				CPRINTLN( DEBUG_OR_RACES, "Leaderboard is ready" )
				CLEANUP_SIMPLE_USE_CONTEXT( ORR_Master.uiInput )
				INIT_SIMPLE_USE_CONTEXT(ORR_Master.uiInput, FALSE, FALSE, TRUE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_CONT2",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_UI_RETRY",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "HUD_INPUT68",		FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			ENDIF
			
			IF sclb_rank_predict.bFinishedRead
			AND NOT sclb_rank_predict.bFinishedWrite
				ORR_WRITE_TIME_TO_LEADERBOARD(INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur), Race.Racer[0].fClockTime, Race.Racer[0].iRank)
				CPRINTLN( DEBUG_OR_RACES, "Writing leaderboard" )
				bRankWritten = TRUE
				sclb_rank_predict.bFinishedWrite = TRUE
			ENDIF
			
			IF NOT bRankPredictReady
				bRankPredictReady = GET_RANK_PREDICTION_DETAILS( offroadLBControl )
				
				IF bRankPredictReady
					CPRINTLN( DEBUG_OR_RACES, "Rank Prediction is complete." )
					sclb_useRankPrediction = TRUE
				ENDIF
			ENDIF
		ELSE
			bIsLBDContextSet = TRUE
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD )
			AND bIsLBDContextSet
			CDEBUG1LN(DEBUG_SP_RACES, "[ORR_Race_Draw_Scorecard] Pressed RB - AND - leaderboard is NOT showing, so set switch leaderboard bool and bIsOutroContextSet to FALSE for reinit.")
			Race.bShowLeaderBoard = !Race.bShowLeaderBoard
			bIsLBDContextSet = FALSE
			bIsOutroContextSet = FALSE
		ENDIF
		BOOL TheScoreCardFinishedRendering = ORR_NEW_DRAW_SCORECARD(Race)
//		ORR_Draw_Scorecard(Race, iPlayerIndex)
		IF TIMERA() > 3000
			UPDATE_SIMPLE_USE_CONTEXT(ORR_Master.uiInput)
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
			OR TheScoreCardFinishedRendering
				DISPLAY_RADAR(TRUE)
				MAKE_AUTOSAVE_REQUEST()									
				SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE)
				IF IS_PLAYER_ONLINE()
					IF NOT bRankWritten
						ORR_WRITE_TIME_TO_LEADERBOARD(INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur), Race.Racer[0].fClockTime, Race.Racer[0].iRank, FALSE)
					ELSE
						bRankWritten = FALSE
					ENDIF
				ENDIF
				ORR_SET_SCORECARD_FINISH(Race)
				RETURN ORV_TRUE
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
				UPDATE_SIMPLE_USE_CONTEXT(ORR_Master.uiInput)
				bBestTimesReady = FALSE
				IF IS_PLAYER_ONLINE()
					IF NOT bRankWritten
						ORR_WRITE_TIME_TO_LEADERBOARD(INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur), Race.Racer[0].fClockTime, Race.Racer[0].iRank, FALSE)
					ELSE
						bRankWritten = FALSE
					ENDIF
				ENDIF
				ORR_SET_SCORECARD_FINISH(Race)
				CLEANUP_SP_RACE_PERSONAL_GLOBAL_BEST(ORR_Master.iReadStage, ORR_Master.iLoadStage, ORR_Master.bSuccessful, ORR_Master.iGlobalBest, ORR_Master.iPersonalBest)
				RETURN ORV_RESET			
			ELSE		
				RETURN ORV_FALSE
			ENDIF
		ENDIF
//		RETURN ORV_TRUE
	ENDIF
	RETURN ORV_FALSE
ENDFUNC



// -----------------------------------
// GATE PROCS/FUNCTIONS
// -----------------------------------

FUNC BOOL ORR_Race_Gate_Activate(ORR_RACE_STRUCT& Race, INT iGate, BOOL bGateCur)
	VECTOR vNextGate
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Activate")
	IF (iGate < 0) OR (iGate > (ORR_GATE_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Activate: Gate index out of range!")
		RETURN FALSE
	ENDIF
	BLIP_SPRITE eBlipSprite = RADAR_TRACE_INVALID
	FLOAT fBlipScale = ORR_GATE_BLIPCUR_SCL
	IF (iGate = (Race.iGateCnt - 1))
		 eBlipSprite = RADAR_TRACE_RACEFLAG
	ELIF NOT bGateCur
		fBlipScale = ORR_GATE_BLIPNXT_SCL
	ENDIF	
	IF NOT ORR_Gate_Blip_Create(Race.sGate[iGate], eBlipSprite, fBlipScale, iGate, Race.iGateCnt)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Activate: Failed to create gate blip!")
		RETURN FALSE
	ENDIF
	IF bGateCur
		VECTOR vNextGatePos = Race.sGate[iGate].vPos
		VECTOR vPrevGatePos = <<0,0,0>>
		IF (iGate < (Race.iGateCnt))
			IF iGate <> Race.iGateCnt -1
				IF NOT ARE_VECTORS_ALMOST_EQUAL(Race.sGate[iGate].vPos, Race.sGate[iGate + 1].vPos)
					//update chase cam
					vNextGatePos = Race.sGate[iGate + 1].vPos
					ORR_SET_HINT_CAM_COORD(Race.sGate[iGate].vPos, vNextGatePos)
				ENDIF
			ELSE
				ORR_SET_HINT_CAM_COORD(Race.sGate[iGate].vPos, Race.sGate[iGate].vPos)
			ENDIF
		ENDIF
		IF ORR_Master.eRaceType = ORR_RACE_TYPE_OFFROAD
			IF (ORR_Master.iRaceCur = 1) // 1 for Ridgerun)
				IF IS_BITMASK_AS_ENUM_SET(Race.sGate[8].iGateFlags, ORR_RACE_GATE_FLAG_DO_NOT_SNAP_TO_GROUND)
					CLEAR_BITMASK_AS_ENUM(Race.sGate[8].iGateFlags, ORR_RACE_GATE_FLAG_DO_NOT_SNAP_TO_GROUND)
					CPRINTLN(DEBUG_OR_RACES, "ORR_Race_Gate_Activate: Cleared Bitmask as Enum for sGate[8]")
					PRINTNL()
				ENDIF
			ENDIF
		ENDIF
		IF (iGate > 0)
			vPrevGatePos = Race.sGate[iGate-1].vPos
		ENDIF
		IF NOT ORR_Gate_Chkpnt_Create(vPrevGatePos, Race.sGate[iGate], vNextGatePos, ORR_GATE_CHKPNT_SCL)	
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Activate: Failed to create gate checkpoint!")
			RETURN FALSE
		ENDIF
		//turn on the next checkpoint and make it tiny
		IF iGate+1 < Race.iGateCnt
			IF (iGate+2 >= ORR_GATE_MAX)
				vNextGate = Race.sGate[0].vPos
			ELSE
				vNextGate = Race.sGate[iGate+2].vPos
			ENDIF
			
			//just set the nextgatepos to the origin because we can't even see the arrow anyways
			IF NOT ORR_Gate_Chkpnt_Create(Race.sGate[iGate].vPos, Race.sGate[iGate+1], vNextGate, ORR_GATE_CHKPNT_SCL)//ORR_GATE_CHKPNTNXT_SCL)			
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Activate: Failed to create gate checkpoint!")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL ORR_Race_Gate_Activate_All(ORR_RACE_STRUCT& Race, BOOL bGateCur)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Activate_All")
	INT i
	BOOL bAllDone = TRUE
	REPEAT Race.iGateCnt i
		IF NOT ORR_Race_Gate_Activate(Race, i, bGateCur)
			bAllDone = FALSE
		ENDIF
	ENDREPEAT
	RETURN bAllDone
ENDFUNC

PROC ORR_Race_Gate_Deactivate(ORR_RACE_STRUCT& Race, INT iGate)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Deactivate")
	IF (iGate < 0) OR (iGate > (ORR_GATE_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Deactivate: Gate index out of range!")
		EXIT
	ENDIF
	ORR_Gate_Blip_Destroy(Race.sGate[iGate])
	ORR_Chkpnt_BeginFade(Race.sGate[iGate].Chkpnt, Race.fadeCheckpoint, Race.iFadeAlpha, (Race.sGate[iGate].eChkpntType = ORR_CHKPT_OFFROAD_FINISH))
	
	//turn off our extra checkpoint
	IF iGate+1 < Race.iGateCnt
		ORR_Gate_Blip_Destroy(Race.sGate[iGate+1])
		ORR_Chkpnt_Destroy(Race.sGate[iGate+1].Chkpnt)
	ENDIF
	
ENDPROC

PROC ORR_Race_Gate_Deactivate_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Deactivate_All")
	INT i
	REPEAT Race.iGateCnt i
		ORR_Race_Gate_Deactivate(Race, i)
	ENDREPEAT
ENDPROC

PROC ORR_Race_Gate_Init(ORR_RACE_STRUCT& Race, INT iGate)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Init")
	IF (iGate < 0) OR (iGate > (ORR_GATE_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Init: Gate index out of range!")
		EXIT
	ENDIF
	ORR_Gate_Blip_Destroy(Race.sGate[iGate])
	ORR_Chkpnt_Destroy(Race.sGate[iGate].Chkpnt)
	ORR_Gate_Init(Race.sGate[iGate])
ENDPROC

PROC ORR_Race_Gate_Init_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Gate_Init_All")
	INT i
	REPEAT Race.iGateCnt i
		ORR_Race_Gate_Init(Race, i)
	ENDREPEAT
ENDPROC




// -----------------------------------
// RACER DRIVER PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_Race_Racer_Driver_Request(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Request")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Request: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Driver_Request(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Driver_Request_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Request_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Driver_Request(Race, i)
	ENDREPEAT
ENDPROC

FUNC BOOL ORR_Race_Racer_Driver_Stream(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Stream")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Stream: Racer index out of range!")
		RETURN FALSE
	ENDIF
	RETURN ORR_Racer_Driver_Stream(Race.Racer[iRacer])
ENDFUNC

FUNC BOOL ORR_Race_Racer_Driver_Stream_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Stream_All")
	INT i
	BOOL bStreamDone = TRUE
	REPEAT Race.iRacerCnt i
		IF NOT ORR_Racer_Driver_Stream(Race, i)
			bStreamDone = FALSE
		ENDIF
	ENDREPEAT
	RETURN bStreamDone
ENDFUNC

PROC ORR_Race_Racer_Driver_Evict(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Evict")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Evict: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Driver_Evict(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Driver_Evict_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Evict_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Driver_Evict(Race, i)
	ENDREPEAT
ENDPROC

FUNC BOOL ORR_Race_Racer_Driver_Create(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Create")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Create: Racer index out of range!")
		RETURN FALSE
	ENDIF
	RETURN ORR_Racer_Driver_Create(Race.Racer[iRacer])
ENDFUNC

FUNC BOOL ORR_Race_Racer_Driver_Create_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Create_All")
	INT i
	BOOL bCreateDone = TRUE
	REPEAT Race.iRacerCnt i
		IF NOT ORR_Race_Racer_Driver_Create(Race, i)
			IF Race.Racer[i].Driver = PLAYER_PED_ID()
				CPRINTLN( DEBUG_OR_RACES, "Creating player ped vehicle...")
			ENDIF
			bCreateDone = FALSE
		ENDIF
	ENDREPEAT
	RETURN bCreateDone
ENDFUNC

PROC ORR_Race_Racer_Driver_Freeze(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Freeze")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Freeze: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Driver_Freeze(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Driver_Freeze_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Freeze_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Driver_Freeze(Race, i)
	ENDREPEAT
ENDPROC

PROC ORR_Race_Racer_Driver_UnFreeze(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_UnFreeze")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_UnFreeze: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Driver_UnFreeze(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Driver_UnFreeze_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_UnFreeze_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Driver_UnFreeze(Race, i)
	ENDREPEAT
ENDPROC

PROC ORR_Race_Racer_Driver_Destroy(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Destroy")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Destroy: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Driver_Destroy(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Driver_Destroy_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Driver_Destroy_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Driver_Destroy(Race, i)
	ENDREPEAT
ENDPROC




// -----------------------------------
// RACER VEHICLE PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_Race_Racer_Vehicle_Request(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Request")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Request: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Vehicle_Request(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Vehicle_Request_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Request_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Vehicle_Request(Race, i)
	ENDREPEAT
ENDPROC

FUNC BOOL ORR_Race_Racer_Vehicle_Stream(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Stream")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Stream: Racer index out of range!")
		RETURN FALSE
	ENDIF
	RETURN ORR_Racer_Vehicle_Stream(Race.Racer[iRacer])
ENDFUNC

FUNC BOOL ORR_Race_Racer_Vehicle_Stream_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Stream_All")
	INT i
	BOOL bStreamDone = TRUE
	REPEAT Race.iRacerCnt i
		IF NOT ORR_Racer_Vehicle_Stream(Race, i)
			bStreamDone = FALSE
		ENDIF
	ENDREPEAT
	RETURN bStreamDone
ENDFUNC

PROC ORR_Race_Racer_Vehicle_Evict(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Evict")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Evict: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Vehicle_Evict(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Vehicle_Evict_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Evict_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Vehicle_Evict(Race, i)
	ENDREPEAT
ENDPROC

FUNC BOOL ORR_Race_Racer_Vehicle_Create(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Create")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Create: Racer index out of range!")
		RETURN FALSE
	ENDIF
	RETURN ORR_Racer_Vehicle_Create(Race.Racer[iRacer])
ENDFUNC

FUNC BOOL ORR_Race_Racer_Vehicle_Create_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Create_All")
	INT i
	BOOL bCreateDone = TRUE
	REPEAT Race.iRacerCnt i
		IF NOT ORR_Race_Racer_Vehicle_Create(Race, i)
			bCreateDone = FALSE
		ENDIF
	ENDREPEAT
	RETURN bCreateDone
ENDFUNC

PROC ORR_Race_Racer_Vehicle_Freeze(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Freeze")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Freeze: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Vehicle_Freeze(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Vehicle_Freeze_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Freeze_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Vehicle_Freeze(Race, i)
	ENDREPEAT
ENDPROC

PROC ORR_Race_Racer_Vehicle_UnFreeze(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_UnFreeze")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_UnFreeze: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Vehicle_UnFreeze(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Vehicle_UnFreeze_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_UnFreeze_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Vehicle_UnFreeze(Race, i)
	ENDREPEAT
ENDPROC

PROC ORR_Race_Racer_Vehicle_Destroy(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Destroy")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Destroy: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Vehicle_Destroy(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Vehicle_Destroy_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Vehicle_Destroy_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Vehicle_Destroy(Race, i)
	ENDREPEAT
ENDPROC



// -----------------------------------
// RACER MAIN PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_Race_Racer_Request(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Request")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Request: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Request(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Request_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Request_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Request(Race, i)
	ENDREPEAT
ENDPROC

FUNC BOOL ORR_Race_Racer_Stream(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Stream")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Stream: Racer index out of range!")
		RETURN FALSE
	ENDIF
	RETURN ORR_Racer_Stream(Race.Racer[iRacer])
ENDFUNC

FUNC BOOL ORR_Race_Racer_Stream_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Stream_All")
	INT i
	BOOL bStreamDone = TRUE
	REPEAT Race.iRacerCnt i
		IF NOT ORR_Race_Racer_Stream(Race, i)
			bStreamDone = FALSE
		ENDIF
	ENDREPEAT
	RETURN bStreamDone
ENDFUNC

PROC ORR_Race_Racer_Evict(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Evict")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Evict: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Evict(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Evict_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Evict_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Evict(Race, i)
	ENDREPEAT
ENDPROC

FUNC BOOL ORR_Race_Racer_Blip_Create(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Blip_Create")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Blip_Create: Racer index out of range!")
		RETURN FALSE
	ENDIF
	RETURN ORR_Racer_Blip_Create(Race.Racer[iRacer])
ENDFUNC

FUNC BOOL ORR_Race_Racer_Blip_Create_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Blip_Create_All")
	INT i
	BOOL bCreateDone = TRUE
	REPEAT Race.iRacerCnt i
		IF NOT ORR_Race_Racer_Blip_Create(Race, i)
			bCreateDone = FALSE
		ENDIF
	ENDREPEAT
	RETURN bCreateDone
ENDFUNC

PROC ORR_Race_Racer_Blip_Destroy(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Blip_Destroy")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Blip_Destroy: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Blip_Destroy(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Blip_Destroy_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Blip_Destroy_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Blip_Destroy(Race, i)
	ENDREPEAT
ENDPROC

FUNC BOOL ORR_Race_Racer_Create(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Create")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Create: Racer index out of range!")
		RETURN FALSE
	ENDIF
	IF (iRacer > 0)
		RETURN ORR_Racer_Create(Race.Racer[iRacer], TRUE)
	ELSE
		RETURN ORR_Racer_Create(Race.Racer[iRacer], FALSE)
	ENDIF
ENDFUNC

FUNC BOOL ORR_Race_Racer_Create_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Create_All")
	INT i
	BOOL bCreateDone = TRUE
	REPEAT Race.iRacerCnt i
		IF i <> 0
			IF NOT ORR_Race_Racer_Create(Race, i)
				bCreateDone = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN bCreateDone
ENDFUNC

PROC ORR_Race_Racer_Freeze(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Freeze")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Freeze: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Freeze(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Freeze_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Freeze_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Freeze(Race, i)
	ENDREPEAT
ENDPROC

PROC ORR_Race_Racer_UnFreeze(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_UnFreeze")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_UnFreeze: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_UnFreeze(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_UnFreeze_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_UnFreeze_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_UnFreeze(Race, i)
	ENDREPEAT
ENDPROC

PROC ORR_Race_Racer_Destroy(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Destroy")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Destroy: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Destroy(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Destroy_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Destroy_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Destroy(Race, i)
	ENDREPEAT
ENDPROC

PROC ORR_Race_Racer_Init(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Init")
	IF (iRacer < 0) OR (iRacer > (ORR_RACER_MAX - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Init: Racer index out of range!")
		EXIT
	ENDIF
	ORR_Racer_Destroy(Race.Racer[iRacer])
	ORR_Racer_Init(Race.Racer[iRacer])
ENDPROC

PROC ORR_Race_Racer_Init_All(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Init_All")
	INT i
	REPEAT Race.iRacerCnt i
		ORR_Race_Racer_Init(Race, i)
	ENDREPEAT
ENDPROC


PROC ORR_RACE_TELL_AUDIENCE_TO_LOOK_AT_PLAYER()
	PED_INDEX pedArray[10]
	SEQUENCE_INDEX seq
	OPEN_SEQUENCE_TASK(seq)
		//TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 5)
		TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_USE_TORSO | SLF_EXTEND_YAW_LIMIT)
	CLOSE_SEQUENCE_TASK(seq)
	GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), pedArray, PEDTYPE_INVALID | PEDTYPE_MISSION)
	INT i = 0
	REPEAT COUNT_OF(pedArray) i
		IF DOES_ENTITY_EXIST(pedArray[i]) AND NOT IS_PED_INJURED(pedArray[i]) AND NOT IS_PED_IN_ANY_VEHICLE(pedArray[i])
			SET_ENTITY_AS_MISSION_ENTITY(pedArray[i], TRUE, TRUE)
			TASK_PERFORM_SEQUENCE(pedArray[i], seq)
			SET_PED_KEEP_TASK(pedArray[i], TRUE)
		ENDIF
	ENDREPEAT
	CLEAR_SEQUENCE_TASK(seq)
ENDPROC




// -----------------------------------
// RACER HELPER PROCS/FUNCTIONS
// -----------------------------------

// TODO: Make following Racer function follow above scheme...
PROC ORR_Race_Racer_Start(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Start")
	TEXT_LABEL OffroadRecording = "Offroad_"
	SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
		CASE OFFROAD_RACE_CANYON_CLIFFS
			OffroadRecording += 1
		BREAK
		CASE OFFROAD_RACE_RIDGE_RUN
			OffroadRecording += 2
		BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			OffroadRecording += 6
		BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL
			OffroadRecording += 3
		BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			OffroadRecording += 4
		BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY
			OffroadRecording += 5
		BREAK
	ENDSWITCH
	
	SEQUENCE_INDEX siStartRace
	INT iOffsetWait, iLottery
	iOffsetWait = iOffsetWait
	
	// Start race clock.
	RESTART_TIMER_NOW(Race.tClock)
	
	
	// Give back player control.
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	SET_VEHICLE_HANDBRAKE( Race.Racer[0].Vehicle, FALSE )
	IF (ORR_Master.eRaceType = ORR_RACE_TYPE_PLANE)
		SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
	ENDIF
	Race.bFailChecking = TRUE
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Start:  Race.bFailChecking set to TRUE")
	//RESTART_TIMER_NOW(manualResetTimer)
	
	FLOAT fSpeedMod = 1.0
	CDEBUG1LN(DEBUG_OR_RACES, "RACE_RACER_START: FAILCHECKING: ON")
	// Loop through all racers and start them.
	INT i
	REPEAT Race.iRacerCnt i
	
		// Start racer.
		ORR_Racer_Start(Race.Racer[i])
		
		IF i % 3 = 0
			AND i <> 0
			fSpeedMod -= 0.02
			CPRINTLN( DEBUG_OR_RACES, "fSpeed mod is ", fSpeedMod, " index is ", i )
		ENDIF
		
		IF ORR_Master.eRaceType <> ORR_RACE_TYPE_OFFROAD 
			// TODO: Put any player specific logic here...
			IF i != 0 //not the player!?
				ORR_Race_Racer_Task_Go_To_Next_Gate(Race, Race.Racer[i].Driver, Race.Racer[i].Vehicle, Race.sGate[Race.Racer[i].iGateCur].vPos, Race.sGate[Race.Racer[i].iGateCur].eChkpntType, i)			
			ENDIF
		ELSE
			IF ORR_USE_WAYPOINT_RECORDING 
				// Task AI to follow waypoint recording for offroad race
				// Pick random waypoint recording to start
				IF i > 0
					IF NOT IS_ENTITY_DEAD(Race.Racer[i].Driver)
					AND NOT IS_ENTITY_DEAD(Race.Racer[i].Vehicle)

						iLottery = GET_RANDOM_INT_IN_RANGE(0, 1000)
						
						//CPRINTLN(DEBUG_OR_RACES, "iLottery = ", iLottery)
						
						IF iLottery < 500
							iOffsetWait = GET_RANDOM_INT_IN_RANGE(1, 500)
						ELSE
							iOffsetWait = GET_RANDOM_INT_IN_RANGE(400, 700)
						ENDIF
						
						//CPRINTLN(DEBUG_OR_RACES, "iOffsetWait = ", iOffsetWait)
						CLEAR_SEQUENCE_TASK(siStartRace)
						OPEN_SEQUENCE_TASK(siStartRace)
							//TASK_STAND_STILL(NULL, iOffsetWait)
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL, Race.Racer[i].Vehicle, OffroadRecording, DRIVINGMODE_AVOIDCARS_RECKLESS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN  , -1, GET_VEHICLE_ESTIMATED_MAX_SPEED(Race.Racer[i].Vehicle) * fSpeedMod)
						CLOSE_SEQUENCE_TASK(siStartRace)
						TASK_PERFORM_SEQUENCE(Race.Racer[i].Driver, siStartRace)
						
						SET_VEHICLE_FORWARD_SPEED( Race.Racer[i].Vehicle, 8.0 )
					ENDIF
				ENDIF
			ELSE
				IF i != 0 //not the player!?
					ORR_Race_Racer_Task_Go_To_Next_Gate(Race, Race.Racer[i].Driver, Race.Racer[i].Vehicle, Race.sGate[Race.Racer[i].iGateCur].vPos, Race.sGate[Race.Racer[i].iGateCur].eChkpntType, i)			
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC AI_End_Of_Race_Stop(ORR_RACE_STRUCT& Race)
	INT i
	REPEAT Race.iRacerCnt i
	IF i > 0
		IF NOT IS_ENTITY_DEAD(Race.Racer[i].Driver)
		AND NOT IS_ENTITY_DEAD(Race.Racer[i].Vehicle)
			IF ORR_Master.eRaceType <> ORR_RACE_TYPE_TRIATHLON
				TASK_VEHICLE_TEMP_ACTION(Race.Racer[i].Driver,Race.Racer[i].Vehicle, TEMPACT_HANDBRAKETURNRIGHT, 5000)
			ENDIF
		ENDIF
	ENDIF
	ENDREPEAT
	
ENDPROC


PROC ORR_Race_Racer_Finish(ORR_RACE_STRUCT& Race)
	//CPRINTLN(DEBUG_OR_RACES, "[ORR_Race.sch->ORR_Race_Racer_Finish] Procedure started.")

	// Pause race clock, if needed.
	IF NOT IS_TIMER_PAUSED(Race.tClock)
		PAUSE_TIMER(Race.tClock)
	ENDIF
	
	ORR_Race_ClockTime_SimAI(Race)
			
	// Find player best rank and clock time.
 
		
	// Loop through all racers and finish them.
	INT iLoopCounter
	REPEAT Race.iRacerCnt iLoopCounter
		ORR_Racer_Finish(Race.Racer[iLoopCounter])
		IF iLoopCounter <> 0
			FINISH_AI_RACERS(Race.Racer[iLoopCounter], TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL ORR_IS_ANY_VEHICLE_NEAR( ORR_RACE_STRUCT & Race, INT iRacer, VECTOR vPos, FLOAT fDist )	
	INT iIndex
	
	REPEAT Race.iRacerCnt iIndex
		IF iRacer <> iIndex
			IF NOT IS_ENTITY_DEAD( Race.Racer[iIndex].Vehicle )
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( Race.Racer[iIndex].Vehicle, vPos ) < fDist
					CDEBUG1LN( DEBUG_OR_RACES, "ORR_IS_ANY_VEHICLE_NEAR - Vehicle ", iIndex, " is near." )
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ORR_RACE_GATE_JUMP_INITIAL(ORR_RACE_STRUCT& Race, INT iRacer)
	
	IF iRacer < 0 OR iRacer > (Race.iRacerCnt - 1)
		CDEBUG1LN( DEBUG_OR_RACES, "ORR_RACE_GATE_JUMP_INITIAL: Racer index is out of range!" )
		RETURN FALSE
	ENDIF
	IF IS_ENTITY_DEAD(Race.Racer[iRacer].Driver)
		CPRINTLN( DEBUG_OR_RACES, "ORR_RACE_GATE_JUMP_INITIAL: Racer is dead" )
		RETURN FALSE
	ENDIF
	IF Race.Racer[iRacer].eVehicleModel <> DUMMY_MODEL_FOR_SCRIPT
		IF IS_ENTITY_DEAD(Race.Racer[iRacer].Vehicle)
			CPRINTLN(DEBUG_OR_RACES, "ORR_RACE_GATE_JUMP_INITIAL: vehicle is dead!")
			RETURN FALSE
		ENDIF
	ENDIF
	
	VECTOR vPos
	FLOAT fHeading
	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)		
		CASE CanyonCliffs
			vPos = <<-1937.8853, 4443.3193, 36.5555>>//<<-1943.7817, 4447.5181, 35.6896>>)//<<-1938.34, 4443.20, 37.06>>)
			fHeading = 256.8476//204.7505)//249.56)
		BREAK
		CASE RidgeRun
			vPos = <<-517.2068, 2000.5529, 204.6160>>//<<-518.6960, 1993.9945, 204.9752>>)//<<-516.1545, 1999.3740, 204.7715>>)//<<-1141.40, 2619.60, 17.30>>)
			fHeading = 20.3384//345.9835)//18.7907)
		BREAK
		CASE ValleyTrail
			vPos = <<-225.98, 4224.74, 44.36>> //<<-226.44, 4226.15, 44.35>>)
			fHeading = 80.20
		BREAK
		CASE LakesideSplash
			vPos = <<1602.54, 3837.21, 33.72>> //<<1600.01, 3839.72, 33.42>>)
			fHeading = 308.94
		BREAK
		CASE EcoFriendly
			vPos = <<2030.8984, 2134.1968, 92.5014>>
			fHeading = 249.4471
		BREAK
		CASE MinewardSpiral
			vPos = <<2995.8235, 2774.8606, 41.9576>>
			fHeading = 354.6729
		BREAK		
		CASE ConstructionCourse
			
		BREAK
	ENDSWITCH
	
	IF ORR_IS_ANY_VEHICLE_NEAR( Race, iRacer, vPos, 5.0 )
		OR (Race.Racer[iRacer].Driver <> PLAYER_PED_ID() AND IS_SPHERE_VISIBLE( vPos, 10.0 ))
		//OR ( IS_BLIP_ON_MINIMAP( Race.Racer[iRacer].Blip ) AND iRacer <> 0 )
		CDEBUG1LN( DEBUG_OR_RACES, "ORR_Race_Racer_Gate_Jump_Cur - Is returning false, vehicle is near" )
		RETURN FALSE
	ENDIF
	
	FLOAT fSpeed = 0.0
	IF IS_THIS_MODEL_A_BIKE(Race.Racer[iRacer].eVehicleModel)
		fSpeed = 8.0
	ELIF IS_THIS_MODEL_A_CAR(Race.Racer[iRacer].eVehicleModel)
		fSpeed = 10.0
	ELIF IS_THIS_MODEL_A_PLANE(Race.Racer[iRacer].eVehicleModel)
		fSpeed = ORR_PLANE_SPD_MAX
	ENDIF
	
	ORR_Racer_Teleport(Race.Racer[iRacer], vPos, fHeading, fSpeed)
	
	RETURN TRUE
ENDFUNC

PROC ORR_Race_Racer_Gate_Jump(ORR_RACE_STRUCT& Race, INT iRacer, INT iGate, BOOL bBlockLoad = FALSE)
	
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Gate_Jump")
	IF (iGate < 0) OR (iGate > (Race.iGateCnt - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Gate_Jump: Gate index out of range!")
		EXIT
	ENDIF
	IF (iRacer < 0) OR (iRacer > (Race.iRacerCnt - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Gate_Jump: Racer index out of range!")
		EXIT
	ENDIF
	// TODO: Decide if this is the right error checking needed...
	IF IS_ENTITY_DEAD(Race.Racer[iRacer].Driver)
		CPRINTLN( DEBUG_OR_RACES, "ORR_Race_Racer_Gate_Jump: Racer is dead!" )
		EXIT
	ENDIF
	IF (Race.Racer[iRacer].eVehicleModel <> DUMMY_MODEL_FOR_SCRIPT)
		IF IS_ENTITY_DEAD(Race.Racer[iRacer].Vehicle)
			CPRINTLN( DEBUG_OR_RACES, "ORR_Race_Racer_Gate_Jump: Vehicle is dead!" )
			EXIT
		ENDIF
	ENDIF
	FLOAT fHeading = Race.sGate[iGate].fHeading
	FLOAT fSpeed = 0.0
	IF IS_THIS_MODEL_A_BIKE(Race.Racer[iRacer].eVehicleModel)
		fSpeed = 8.0
	ELIF IS_THIS_MODEL_A_CAR(Race.Racer[iRacer].eVehicleModel)
		fSpeed = 10.0
	ELIF IS_THIS_MODEL_A_PLANE(Race.Racer[iRacer].eVehicleModel)
		fSpeed = ORR_PLANE_SPD_MAX
	ENDIF
	IF bBlockLoad
		LOAD_SCENE(Race.sGate[iGate].vPos)
	ENDIF
	ORR_Racer_Teleport(Race.Racer[iRacer], Race.sGate[iGate].vPos, fHeading, fSpeed)
ENDPROC

FUNC BOOL ORR_Race_Racer_Gate_Jump_Cur(ORR_RACE_STRUCT& Race, INT iRacer, BOOL bBlockLoad = FALSE)
	
	VECTOR vGatePos = Race.sGate[Race.Racer[iRacer].iGateCur].vPos
	
	IF IS_ENTITY_DEAD( Race.Racer[iRacer].Vehicle )
		RETURN FALSE
	ENDIF
	
	IF ORR_IS_ANY_VEHICLE_NEAR( Race, iRacer, vGatePos, 5.0 )
		OR (Race.Racer[iRacer].Driver <> PLAYER_PED_ID() AND IS_SPHERE_VISIBLE( vGatePos, 10.0 ))
		OR (Race.Racer[iRacer].Driver <> PLAYER_PED_ID() AND IS_ENTITY_VISIBLE( Race.Racer[iRacer].Vehicle ) )
		//OR ( IS_BLIP_ON_MINIMAP( Race.Racer[iRacer].Blip ) AND iRacer <> 0 )
		//IF Race.Racer[iRacer].Driver = PLAYER_PED_ID()
			IF NOT IS_TIMER_STARTED( Race.Racer[iRacer].vehRespawnTimeout )
				START_TIMER_NOW(Race.Racer[iRacer].vehRespawnTimeout)
			ENDIF
			
			IF GET_TIMER_IN_SECONDS( Race.Racer[iRacer].vehRespawnTimeout ) >= ORR_RESPAWN_TIMEOUT
				CANCEL_TIMER(Race.Racer[iRacer].vehRespawnTimeout)
				IF Race.Racer[iRacer].Driver = PLAYER_PED_ID()
					ORR_Race_Racer_Gate_Jump(Race, iRacer, Race.Racer[iRacer].iGateCur - 1, bBlockLoad)
				ENDIF
				CDEBUG1LN( DEBUG_OR_RACES, "ORR_Race_Racer_Gate_Jump_Cur - Timeout reached, teleporting player" )
				
				RETURN TRUE
			ENDIF
		//ENDIF
		CDEBUG1LN( DEBUG_OR_RACES, "ORR_Race_Racer_Gate_Jump_Cur - Is returning false, vehicle is near" )
		RETURN FALSE
	ENDIF
		
	ORR_Race_Racer_Gate_Jump(Race, iRacer, Race.Racer[iRacer].iGateCur, bBlockLoad)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ORR_Race_Racer_Gate_Jump_Nxt(ORR_RACE_STRUCT& Race, INT iRacer, BOOL bBlockLoad = FALSE)
	IF IS_ENTITY_DEAD( Race.Racer[iRacer].Vehicle )
		RETURN FALSE
	ENDIF
	
	VECTOR vGatePos = Race.sGate[Race.Racer[iRacer].iGateCur-1].vPos
	IF ORR_IS_ANY_VEHICLE_NEAR( Race, iRacer, vGatePos, 5.0 )
		OR (Race.Racer[iRacer].Driver <> PLAYER_PED_ID() AND IS_SPHERE_VISIBLE( vGatePos, 10.0 ))
		OR (Race.Racer[iRacer].Driver <> PLAYER_PED_ID() AND IS_ENTITY_VISIBLE( Race.Racer[iRacer].Vehicle ) )
		//OR ( IS_BLIP_ON_MINIMAP( Race.Racer[iRacer].Blip ) AND iRacer <> 0 )
		
		//IF Race.Racer[iRacer].Driver = PLAYER_PED_ID()
			IF NOT IS_TIMER_STARTED( Race.Racer[iRacer].vehRespawnTimeout )
				START_TIMER_NOW(Race.Racer[iRacer].vehRespawnTimeout)
			ENDIF
			
			IF GET_TIMER_IN_SECONDS( Race.Racer[iRacer].vehRespawnTimeout ) >= ORR_RESPAWN_TIMEOUT
				CANCEL_TIMER(Race.Racer[iRacer].vehRespawnTimeout)
				
				IF Race.Racer[iRacer].Driver = PLAYER_PED_ID()
					ORR_Race_Racer_Gate_Jump(Race, iRacer, Race.Racer[iRacer].iGateCur - 1, bBlockLoad)
				ENDIF
				CDEBUG1LN( DEBUG_OR_RACES, "ORR_Race_Racer_Gate_Jump_Nxt - Timeout reached, teleporting player" )
				
				RETURN TRUE
			ENDIF
		//ENDIF
		CDEBUG1LN( DEBUG_OR_RACES, "ORR_Race_Racer_Gate_Jump_Nxt - Is returning false, vehicle is near" )
		
		RETURN FALSE
	ENDIF
	
	ORR_Race_Racer_Gate_Jump(Race, iRacer, Race.Racer[iRacer].iGateCur - 1, bBlockLoad)

	RETURN TRUE
ENDFUNC

PROC ORR_Race_Racer_Rank_Calc(ORR_RACE_STRUCT& Race, INT iRacer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Rank_Calc")
	IF (iRacer < 0) OR (iRacer > (Race.iRacerCnt - 1))
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Rank_Calc: Racer index out of range!")
		EXIT
	ENDIF
	INT nRank = Race.iRacerCnt
	INT i
	ORR_RACER_STRUCT tmpRacer = Race.Racer[iRacer]
	REPEAT Race.iRacerCnt i
		//skip ourselves
		IF tmpRacer.Driver <> Race.Racer[i].Driver
			IF NOT IS_ENTITY_DEAD(Race.Racer[i].Driver) AND NOT IS_ENTITY_DEAD(tmpRacer.Driver)
				IF tmpRacer.iGateCur > Race.Racer[i].iGateCur
					nRank--
				ELIF tmpRacer.iGateCur = Race.Racer[i].iGateCur
					//same checkpoint lets get the distance to the next checkpoint
					FLOAT ourRacer, evilRacer
					ourRacer = GET_ENTITY_DISTANCE_FROM_LOCATION(tmpRacer.Driver, Race.sGate[tmpRacer.iGateCur].vPos)
					evilRacer = GET_ENTITY_DISTANCE_FROM_LOCATION(Race.Racer[i].Driver, Race.sGate[Race.Racer[i].iGateCur].vPos)
					IF ourRacer < evilRacer
						nRank--
					ENDIF
				ENDIF
			ELSE
				nRank--
			ENDIF
		ENDIF
	ENDREPEAT
	Race.Racer[iRacer].iRank=nRank

ENDPROC

FUNC PED_INDEX GET_WINNING_PED(ORR_RACE_STRUCT& Race)
	INT idx
	REPEAT Race.iRacerCnt idx	
		IF (Race.Racer[idx].iRank = 1)
			RETURN Race.Racer[idx].Driver
		ENDIF
	ENDREPEAT
	RETURN NULL
ENDFUNC

PROC ORR_Race_Racer_Fix_Up_Start(VECTOR vCurGatePos1, VEHICLE_INDEX iPlaneIndex, VECTOR vCurGatePos, BOOL bIsStart)
	IF NOT IS_ENTITY_DEAD(iPlaneIndex)							
		FLOAT fHeading
		FLOAT fPitch
//		FLOAT fPitchRad
		VECTOR vRotation	
		VECTOR vPlayersPlaneCurrentPos
		VECTOR vGateDiff	
		// Stuntplane minimum hard deck check
		FLOAT fHardDeckGround
		VECTOR vHardDeckProbe
		vHardDeckProbe = GET_ENTITY_COORDS(iPlaneIndex)
		REQUEST_COLLISION_AT_COORD(vHardDeckProbe)
		GET_GROUND_Z_FOR_3D_COORD(vHardDeckProbe, fHardDeckGround)
			PRINTSTRING("Ground Height is: ")
			PRINTFLOAT(fHardDeckGround)
			PRINTNL()							
		
		vPlayersPlaneCurrentPos = GET_ENTITY_COORDS(iPlaneIndex)

//			PRINTSTRING("vPlayersPlaneCurrentPos is: ")						
//			PRINTVECTOR(vPlayersPlaneCurrentPos)						
//			PRINTNL()

		IF bIsStart
			CDEBUG1LN(DEBUG_OR_RACES, "iGateCur = 0 using vCurGatePos for vGateDiff")
			vGateDiff = vCurGatePos1 - vCurGatePos
		ELSE
			CDEBUG1LN(DEBUG_OR_RACES, "iGateCur != 0 using regular diff")
			vGateDiff = vCurGatePos - vPlayersPlaneCurrentPos
		ENDIF
						
		fHeading = GET_HEADING_FROM_VECTOR_2D(vGateDiff.x, vGateDiff.y)				
//			PRINTSTRING("fHeading is: ")						
//			PRINTFLOAT(fHeading)						
//			PRINTNL()		
		fPitch = GET_HEADING_FROM_VECTOR_2D(vGateDiff.x, vGateDiff.z)-270		
			PRINTSTRING("fPitch is: ")						
			PRINTFLOAT(fPitch)						
			PRINTNL()		
		WHILE fPitch > 180
			fPitch -= 360
		ENDWHILE
		WHILE fPitch < -180
			fPitch += 360
		ENDWHILE
				
		IF fPitch < -90		
			fPitch = -(180-ABSF(fPitch))
		ENDIF
		IF fPitch > 90		
			fPitch = (180-ABSF(fPitch))
		ENDIF
				
			PRINTSTRING("fPitch is: ")						
			PRINTFLOAT(fPitch)						
			PRINTNL()
			
				
		IF ( ABSF(vHardDeckProbe.z - fHardDeckGround) < 25.0 )
		AND ( (fPitch > 15) OR (fPitch < -15) )
			vHardDeckProbe.z = fHardDeckGround + 25.0
			SET_ENTITY_COORDS(iPlaneIndex, vHardDeckProbe)
			PRINTSTRING("MOVING PLAYER'S PLANE 25 METERS ABOVE THE GROUND")			
			PRINTNL()	
//		ELSE
//			PRINTSTRING("Player is already this many meters above the ground: !")			
//			PRINTFLOAT(ABSF(vHardDeckProbe.z - fHardDeckGround))
//			PRINTNL()		
		ENDIF	
						
		FREEZE_ENTITY_POSITION(iPlaneIndex, TRUE)								
		vRotation = GET_ENTITY_ROTATION(iPlaneIndex)					
		vRotation.x = fPitch //we may not want to set this if the next gate is lower than the restart position and is close (hit the ground a lot)...that or set it to a portion of the pitch, using height delta and distance delta as the ratio				
		vRotation.z = fHeading
		SET_ENTITY_ROTATION(iPlaneIndex, vRotation)						
		FREEZE_ENTITY_POSITION(iPlaneIndex, FALSE)		
		
			PRINTSTRING("GET_ENTITY_PITCH is: ")						
			PRINTFLOAT(GET_ENTITY_PITCH(iPlaneIndex))						
			PRINTNL()
	ENDIF
ENDPROC

PROC ORR_RACE_TASK_RACERS_TO_FLEE( ORR_RACE_STRUCT & Race )
	INT iIndex
	
	REPEAT Race.iRacerCnt iIndex
		IF NOT IS_ENTITY_DEAD( Race.Racer[iIndex].Driver )
			IF PLAYER_PED_ID() <> Race.Racer[iIndex].Driver
				CLEAR_PED_TASKS( Race.Racer[iIndex].Driver )
				TASK_SMART_FLEE_PED( Race.Racer[iIndex].Driver, PLAYER_PED_ID(), 100.0, -1 )
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC ORR_TELEPORT_RACER_TOO_FAR_BACK( ORR_RACE_STRUCT & Race, ORR_RACER_STRUCT & Racer )

	INT i_waypoint_to_warp_to
	VECTOR v_waypoint_to_warp_to, v_next_waypoint
	
	TEXT_LABEL OffroadRecording = "Offroad_"
	SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
		CASE OFFROAD_RACE_CANYON_CLIFFS
			OffroadRecording += 1
		BREAK
		CASE OFFROAD_RACE_RIDGE_RUN
			OffroadRecording += 2
		BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			OffroadRecording += 6
		BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL
			OffroadRecording += 3
		BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			OffroadRecording += 4
		BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY
			OffroadRecording += 5
		BREAK
	ENDSWITCH
	
	IF Race.Racer[0].iGateCur - (Race.iGateSkipDist-1) <= 0
		EXIT
	ENDIF
	
	WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(OffroadRecording, Race.sGate[Race.Racer[0].iGateCur - Race.iGateSkipDist -1].vPos, i_waypoint_to_warp_to)
	WAYPOINT_RECORDING_GET_COORD(OffroadRecording, i_waypoint_to_warp_to, v_waypoint_to_warp_to)
	FLOAT f_distance = GET_DISTANCE_BETWEEN_ENTITIES(Racer.Vehicle, PLAYER_PED_ID(), FALSE)
	FLOAT f_PlayerWarpPointDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_waypoint_to_warp_to, FALSE)
	
	// Make sure the point to warp to is far enough from the player that the warp won't be seen
	IF f_PlayerWarpPointDist > 10
		IF f_distance > 75
		AND NOT IS_SPHERE_VISIBLE(v_waypoint_to_warp_to, 3) // Waypoint at player's previous checkpoint
			IF f_distance > 75
			AND NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(Racer.Vehicle), 3.0) // Rival
				IF NOT IS_ANY_VEHICLE_NEAR_POINT(v_waypoint_to_warp_to, 5) // Make sure the new checkpoint we're warping to is clear
				OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(Racer.Vehicle, v_waypoint_to_warp_to) < 5 // In case it's this vehicle within the 5 metres
					Racer.iGateCur = Race.Racer[0].iGateCur - Race.iGateSkipDist -1
					
					WAYPOINT_RECORDING_GET_COORD(OffroadRecording, i_waypoint_to_warp_to+1, v_next_waypoint)
					SET_ENTITY_COORDS(Racer.Vehicle, v_waypoint_to_warp_to)
					SET_ENTITY_HEADING( Racer.Vehicle, Race.sGate[Racer.iGateCur].fHeading )
					//SET_ENTITY_HEADING_FACE_COORDS(sRacer[i_index].viCar, v_next_waypoint) // Face next checkpoint
					
					IF NOT IS_PED_IN_VEHICLE(Racer.Driver, Racer.Vehicle)
						SET_PED_INTO_VEHICLE(Racer.Driver, Racer.Vehicle)
					ENDIF
					
					CPRINTLN( DEBUG_OR_RACES, "Teleporting racer to ", v_waypoint_to_warp_to, " gate cur is ", Racer.iGateCur )
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Racer.Driver, Racer.Vehicle, OffroadRecording, DRIVINGMODE_AVOIDCARS_RECKLESS, i_waypoint_to_warp_to, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, GET_VEHICLE_ESTIMATED_MAX_SPEED(Racer.Vehicle))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

PROC OFFROAD_CLEAR_LAW_IN_AREA()
	IF IS_ENTITY_DEAD( PLAYER_PED_ID() )
		EXIT
	ENDIF
	
	CLEAR_AREA_OF_COPS( GET_ENTITY_COORDS( PLAYER_PED_ID() ), 800.0 )

ENDPROC


/// PURPOSE:
///    Reset SPR Race Racer.
/// RETURNS:
///    TRUE if resetting SPR Race Racer.
FUNC BOOL ORR_Race_Racer_Reset(ORR_RACE_STRUCT& Race, ORR_RACER_STRUCT& Racer, BOOL bRacerIsPlayer)
	//CDEBUG1LN(DEBUG_OR_RACES, "[ORR_Race->ORR_Race_Racer_Reset] Starting function...")
	// Reset Racer State Machine.
	SWITCH (Racer.eReset)
	
		// Failcase PLAYER HAS FORFEITED RACE
		CASE ORR_RACER_RESET_FAIL_OVER
			// Do not allow blips on racers to exist.
			ORR_REMOVE_BLIPS_ON_FAIL(Race)
								
			// Don't draw road and area names here. It'll render over our buttons.
			HIDE_ALL_BOTTOM_RIGHT_HUD()
			
			//IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
			//	SET_VEHICLE_DOORS_LOCKED(Race.Racer[0].Vehicle, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
			//ENDIF
			
			IF Race.bAttackedDuringRace
				AND NOT Race.bRacersFleeing
				ORR_RACE_TASK_RACERS_TO_FLEE( Race )
				Race.bRacersFleeing = TRUE
				CPRINTLN( DEBUG_OR_RACES, "Tasking racers to flee" )
			ELIF Race.bAttackedDuringRace
				CPRINTLN( DEBUG_OR_RACES, "Attacked during race" )
			ENDIF
			
			Race.bFailChecking = FALSE
			ORR_Race_Gate_Deactivate_All(Race)
			//IF (GET_TIMER_IN_SECONDS(exitTimer) <= 10.0)
				
			// Setup Scaleform UI for reset/restart. 
			// SHOULDN'T BE CALLING SETUP AND UPDATE EVERY FRAME. SETUP ONCE, UPDATE EVERY FRAME.
//			INIT_SIMPLE_USE_CONTEXT(ORR_Master.uiInput, FALSE, FALSE, TRUE, TRUE)
//			ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "IB_RETRY", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//			ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "FE_HLP16", FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			IF IS_THIS_PRINT_BEING_DISPLAYED("SPR_HELP_DAMG")
				CLEAR_THIS_PRINT("SPR_HELP_DAMG")
			ENDIF
			// If racer is player, display reset/restart options.
			IF bRacerIsPlayer
				//UPDATE_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI)
				//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT) 
				BOOL button
				IF MG_UPDATE_FAIL_SPLASH_SCREEN(Race.bigMessageUI, ORR_Master.splashState, "SPR_UI_FAILD", ORR_GET_FAIL_STRAPLINE(ORR_Master.eFailReason), button, FAIL_SPLASH_ENABLE_FX | FAIL_SPLASH_ENABLE_SOUNDS | FAIL_SPLASH_FADE_ON_RETRY)
					//SWITCH (ORR_Master.button)
						// Reset vehicle.
	/*					CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							Racer.eReset = ORR_RACER_RESET_FADE_OUT
						BREAK
	*/					// Restart race.
					IF button
						//iButtonSounds = GET_SOUND_ID()
						//ODDJOB_PLAY_SOUND("Phone_Generic_Key_02", iButtonSounds, FALSE)
						
						Race.bRestarting = TRUE
						//kill the chase came so if we were looking at 
						//it when we reset the camera is still not looking at it.
						IF IS_THIS_PRINT_BEING_DISPLAYED("SPR_RETR_DES")
							CLEAR_THIS_PRINT("SPR_RETR_DES")
						ENDIF
						IF NOT (ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON)
							// This command also sets the cinematic cam back to true, so turning off for Tri.
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						ENDIF
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEHICLELOCK_UNLOCKED)
								ENDIF
							ELSE
								IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
									SET_VEHICLE_DOORS_LOCKED(GET_PLAYERS_LAST_VEHICLE(), VEHICLELOCK_UNLOCKED)
								ENDIF
							ENDIF
						ENDIF
						
						
						Racer.eReset = ORR_RACER_RESET_FADE_OUT		
						
						//SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						//SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						Race.bFailChecking = FALSE
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)

						Race.Racer[0].iGateCur = 0		// Reset our gate position so the reset stuff runs too
						CANCEL_TIMER(Race.Display.tCnt)
						eBoostState = BS_READY
						Race.bBoostGoing = FALSE
						SET_PLAYER_CONTROL( PLAYER_ID(), TRUE )
						eCountdownStage = ORR_COUNTDOWN_STAGE_INIT
					ELSE
						Race.bRestarting = FALSE
						//ODDJOB_PLAY_SOUND("Phone_Generic_Key_03", iButtonSounds, FALSE)
						CPRINTLN( DEBUG_OR_RACES, "PLAYING SOUND FOR QUIT!!" )
						// Kill the chase cam in case it's being used.
						IF NOT (ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON)
							// This command also sets the cinematic cam back to true, so turning off for Tri.
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
						ENDIF
						
						IF ORR_Master.iRaceCur = ENUM_TO_INT(OFFROAD_RACE_LAKESIDE_SPLASH)
							AND ORR_Master.bIPLActive
							// Replace tri IPLs that might not be active here
							IF NOT IS_IPL_ACTIVE("CS2_06_TriAf02")
								REQUEST_IPL("CS2_06_TriAf02")
							ENDIF
						ENDIF

						ORR_Master.iRaceCur = -1 //forcing a quit here as there is no menu for offroad races
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEHICLELOCK_UNLOCKED)
								ENDIF
							ELSE
								IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
									SET_VEHICLE_DOORS_LOCKED(GET_PLAYERS_LAST_VEHICLE(), VEHICLELOCK_UNLOCKED)
								ENDIF
							ENDIF
						ENDIF
						bQuitting = TRUE
						INT iIndex
		
						REPEAT Race.iRacerCnt iIndex
							IF iIndex <> 0
								IF DOES_BLIP_EXIST(Race.Racer[iIndex].Blip)
									REMOVE_BLIP(Race.Racer[iIndex].Blip)
								ENDIF
							ENDIF
						ENDREPEAT
						Racer.eReset = ORR_RACER_QUIT_FADE_IN

					ENDIF
				ENDIF
			ENDIF
//			ELSE
//				Race.bRestarting = FALSE
//				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
//				IF ORR_Master.iRaceCur = ENUM_TO_INT(OFFROAD_RACE_LAKESIDE_SPLASH)
//					AND ORR_Master.bIPLActive
//					// Replace tri IPLs that might not be active here
//					IF NOT IS_IPL_ACTIVE("CS2_06_TriAf02")
//						REQUEST_IPL("CS2_06_TriAf02")
//					ENDIF
//				ENDIF
//				ORR_Master.iRaceCur = -1 //forcing a quit here as there is no menu for offroad races
//				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//							SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEHICLELOCK_UNLOCKED)
//						ENDIF
//					ELSE
//						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
//							SET_VEHICLE_DOORS_LOCKED(GET_PLAYERS_LAST_VEHICLE(), VEHICLELOCK_UNLOCKED)
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				CLEAR_PRINTS()
////				SET_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI, orrFailString, orrFailStrapline, 4000, HUD_COLOUR_RED, MG_BIG_MESSAGE_MISSION_FAILED)
//				CDEBUG1LN(DEBUG_OR_RACES, "EXIT_VEHICLE_FAILURE:  Race.bFailChecking set to FALSE")
//				bQuitting = TRUE
//				INT iIndex
//
//				REPEAT Race.iRacerCnt iIndex
//					IF iIndex <> 0
//						IF DOES_BLIP_EXIST(Race.Racer[iIndex].Blip)
//							REMOVE_BLIP(Race.Racer[iIndex].Blip)
//						ENDIF
//					ENDIF
//				ENDREPEAT
//				Race.Racer[0].eReset = ORR_RACER_QUIT_FADE_IN
//
//			ENDIF
			
		BREAK
	
		// Reset racer (init).
		CASE ORR_RACER_RESET_INIT
			IF bRacerIsPlayer
				IF Race.bRestarting = TRUE
					Race.bFailChecking = FALSE		
					CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACER_RESET_INIT:  Race.bFailChecking set to FALSE")
				ENDIF
			ELIF NOT Race.bFailChecking
				Racer.eReset = ORR_RACER_RESET_WAIT
			ENDIF
			
			Race.ePlayerRespawnState = ORR_RESPAWN_SET
			
			// Overwrite racer start pos with current gate pos.
			IF Racer.iGateCur > 0
				Racer.vStartPos = Race.sGate[Racer.iGateCur].vPos
			ENDIF

			// Setup Scaleform UI for reset/restart.
			IF bRacerIsPlayer
				// Write something to put the player into the vehicle
				//IF IS_VEHICLE_DRIVEABLE(Race.Racer[0].Vehicle)
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)			
				INIT_SIMPLE_USE_CONTEXT(ORR_Master.uiInput, FALSE, FALSE, TRUE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_UI_RESET",	FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "FE_HLP16",		FRONTEND_CONTROL, INPUT_FRONTEND_Y)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_UI_RESTART",	FRONTEND_CONTROL, INPUT_FRONTEND_X)
				//ENDIF
			ENDIF
				
			// If racer is player, go to choose state.
			IF bRacerIsPlayer
				// Write something to put the player into the vehicle
				//IF IS_VEHICLE_DRIVEABLE(Race.Racer[0].Vehicle)//NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
					//IF NOT IS_PED_IN_VEHICLE(Race.Racer[0].Driver, Race.Racer[0].Vehicle)
					//	SET_PED_INTO_VEHICLE(Race.Racer[0].Driver, Race.Racer[0].Vehicle, VS_DRIVER)
//						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)//, SPC_ALLOW_PLAYER_DAMAGE)
						CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_RACER_RESET_CHOOSE")
						Racer.eReset = ORR_RACER_RESET_CHOOSE					
					//ENDIF
				//ENDIF
			ELSE // Otherwise, racer is ai, go to create state.
				CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_RACER_RESET_CREATE")
				Racer.eReset = ORR_RACER_RESET_CREATE
			ENDIF
			
		BREAK
		
		// Choose reset/restart.
		CASE ORR_RACER_RESET_CHOOSE
			//IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
			//	SET_VEHICLE_DOORS_LOCKED(Race.Racer[0].Vehicle, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
			//ENDIF
			Race.bFailChecking = FALSE
			
			IF (GET_TIMER_IN_SECONDS(exitTimer) <= 10.0)
				// If racer is player, display reset/restart options.
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				IF bRacerIsPlayer
					
					UPDATE_SIMPLE_USE_CONTEXT(ORR_Master.uiInput)
				
					// Restart race.
					IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_RLEFT)
							
						CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_RACER_RESET_FADE_OUT")
						//iButtonSounds = GET_SOUND_ID()
						//ODDJOB_PLAY_SOUND("Phone_Generic_Key_02", iButtonSounds, FALSE)
						PLAY_SOUND_FRONTEND(-1, "YES", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						Racer.eReset = ORR_RACER_RESET_FADE_OUT
						//kill the chase came so if we were looking at 
						//it when we reset the camera is still not looking at it.
						IF NOT (ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON)
							// This command also sets the cinematic cam back to true, so turning off for Tri.
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						ENDIF
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						Race.bFailChecking = FALSE

						Race.Racer[0].iGateCur = 0		// Reset our gate position so the reset stuff runs too
						CANCEL_TIMER(Race.Display.tCnt)
						eBoostState = BS_READY
						Race.bBoostGoing = FALSE
						
						eCountdownStage = ORR_COUNTDOWN_STAGE_INIT
						CLEAR_PED_TASKS( PLAYER_PED_ID() )
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEHICLELOCK_UNLOCKED)
								ENDIF
							ENDIF
						ENDIF
				
					ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				
	
						CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_RACER_RESET_FADE_OUT")
						//iButtonSounds = GET_SOUND_ID()
						//ODDJOB_PLAY_SOUND("Phone_Generic_Key_02", iButtonSounds, FALSE)
						PLAY_SOUND_FRONTEND(-1, "YES", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						
						Racer.eReset = ORR_RACER_RESET_FADE_OUT
						
					ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RUP)
					
						Race.bRestarting = FALSE						
						//kill the chase came so if we were looking at 
						//it when we reset the camera is still not looking at it.
						IF NOT (ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON)
							// This command also sets the cinematic cam back to true, so turning off for Tri.
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						ENDIF
						
						IF (ORR_Master.eRaceType = ORR_RACE_TYPE_OFFROAD)	
							IF ORR_Master.iRaceCur = ENUM_TO_INT(OFFROAD_RACE_LAKESIDE_SPLASH)
								AND ORR_Master.bIPLActive
								// Replace tri IPLs that might not be active here
								IF NOT IS_IPL_ACTIVE("CS2_06_TriAf02")
									REQUEST_IPL("CS2_06_TriAf02")
								ENDIF
							ENDIF
							ORR_Master.iRaceCur = -1 //forcing a quit here as there is no menu for offroad races
							CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_RACER_QUIT_FADE_IN")
							//ODDJOB_PLAY_SOUND("Phone_Generic_Key_03", iButtonSounds, FALSE)
							PLAY_SOUND_FRONTEND(-1, "NO", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
										SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEHICLELOCK_UNLOCKED)
									ENDIF
								ENDIF
							ENDIF
							bQuitting = TRUE
							INT iIndex
		
							REPEAT Race.iRacerCnt iIndex
								IF iIndex <> 0
									IF DOES_BLIP_EXIST(Race.Racer[iIndex].Blip)
										REMOVE_BLIP(Race.Racer[iIndex].Blip)
									ENDIF
								ENDIF
							ENDREPEAT
							
							REPEAT Race.iGateCnt iIndex
								IF DOES_BLIP_EXIST(Race.sGate[iIndex].Blip)
									REMOVE_BLIP(Race.sGate[iIndex].Blip)
								ENDIF
							ENDREPEAT
							Racer.eReset = ORR_RACER_QUIT_FADE_IN
						ELSE
							CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_RACER_QUIT_FADE_OUT")
							ADJUST_TIMER(exitTimer, 10.0)
							//ODDJOB_PLAY_SOUND("Phone_Generic_Key_03", iButtonSounds, FALSE)
							PLAY_SOUND_FRONTEND(-1, "NO", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
										SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEHICLELOCK_UNLOCKED)
									ENDIF
								ENDIF
							ENDIF
							CLEAR_PRINTS()
							CLEAR_HELP()
							SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
							MG_INIT_FAIL_SPLASH_SCREEN(ORR_Master.splashState)
							ORR_REMOVE_BLIPS_ON_FAIL(Race)

							//SET_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI, orrFailString, orrFailStrapline, 4000, HUD_COLOUR_RED, MG_BIG_MESSAGE_CENTERED)
							Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
						ENDIF

					ENDIF
					
				ENDIF
			ELSE
				IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
					SET_VEHICLE_DOORS_LOCKED(Race.Racer[0].Vehicle, VEHICLELOCK_UNLOCKED)
				ENDIF
				CLEAR_PRINTS()
				SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
				
				ORR_REMOVE_BLIPS_ON_FAIL(Race)
				
				ORR_Master.eFailReason = ORR_FAIL_IDLE
				MG_INIT_FAIL_SPLASH_SCREEN(ORR_Master.splashState)
				//SET_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI, orrFailString, orrFailStrapline, 4000, HUD_COLOUR_RED, MG_BIG_MESSAGE_CENTERED)
				Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
			ENDIF
			
		BREAK
		
		// Fade out.
		CASE ORR_RACER_RESET_FADE_OUT
			Race.bAttackedDuringRace = FALSE
			Race.bRacersFleeing = FALSE
			IF ORR_FadeOut_Safe(ORR_FADE_QUICK_TIME)
				CLEAR_PRINTS()
				CLEAR_HELP()
				CPRINTLN(DEBUG_OR_RACES, "[ORR_Race.sch->ORR_Race_Racer_Reset] eReset: From ORR_RACER_RESET_FADE_OUT to ORR_RACER_RESET_CREATE")
//				IF DOES_ENTITY_EXIST(Race.Racer[0].Vehicle)
//					DELETE_VEHICLE(Race.Racer[0].Vehicle)
//				ENDIF
				Racer.eReset = ORR_RACER_RESET_CREATE				
			ENDIF
		BREAK
		
		// Fade out.
		CASE ORR_RACER_RESET_NO_FADE_OUT
			Race.bAttackedDuringRace = FALSE
			Race.bRacersFleeing = FALSE
			Race.bRestartCameraPan = TRUE
			CLEAR_PRINTS()
			CLEAR_HELP()
			CPRINTLN(DEBUG_OR_RACES, "[ORR_Race.sch->ORR_Race_Racer_Reset] eReset: From ORR_RACER_RESET_FADE_OUT to ORR_RACER_RESET_CREATE")
			Racer.eReset = ORR_RACER_RESET_CREATE
		BREAK
		
		// Fade out from quitting race.
		CASE ORR_RACER_QUIT_FADE_OUT
			IF ORR_FadeOut_Safe(ORR_FADE_QUICK_TIME)
			
				// Cleanup the race.
				ORR_Race_Gate_Deactivate_All(Race)
				CANCEL_TIMER(Race.tClock)
				
				// Give back player control.
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)//, SPC_ALLOW_PLAYER_DAMAGE)
				
				CDEBUG1LN(DEBUG_OR_RACES, "[ORR_Race->ORR_Race_Racer_Reset] CASE ORR_RACER_QUIT_FADE_OUT: eReset = ORR_RACER_QUIT_PLACE")
				Racer.eReset = ORR_RACER_QUIT_PLACE
			ENDIF
		BREAK
		
		
		// Create racer.
		CASE ORR_RACER_RESET_CREATE
			CPRINTLN(DEBUG_OR_RACES, "[ORR_Race.sch->ORR_Race_Racer_Reset] ORR_RACER_RESET_CREATE: Case started.")	
			IF Race.bRestarting
				OFFROAD_CLEAR_LAW_IN_AREA()
			ENDIF
			
			IF ORR_Racer_Create(Racer, TRUE)
				
				IF Race.bRestarting
					CPRINTLN(DEBUG_OR_RACES, "[ORR_Race.sch->ORR_Race_Racer_Reset] From ORR_RACER_RESET_CREATE to ORR_RACE_UPDATE_CLEANUP.")
					Race.eUpdate = ORR_RACE_UPDATE_CLEANUP
				ELSE
					//give back the player control here so they don't drop out of the sky
					IF bRacerIsPlayer
						// Give back player control.
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						ENABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_EXIT )
					ENDIF
					CPRINTLN(DEBUG_OR_RACES, "[ORR_Race.sch->ORR_Race_Racer_Reset] From ORR_RACER_RESET_CREATE to ORR_RACER_RESET_PLACE.")
					Racer.eReset = ORR_RACER_RESET_PLACE
				ENDIF
			ENDIF
		BREAK
		
		
		// Place racer during reset.
		CASE ORR_RACER_RESET_PLACE
			INT i, iPlayerIndex
			BOOL bTeleport
			bTeleport = FALSE
			// Jump racer to current gate.
			REPEAT Race.iRacerCnt i
				IF (Race.Racer[i].Driver = Racer.Driver)
					CDEBUG3LN( DEBUG_OR_RACES, "Teleporting racer ", i )
					IF Race.Racer[i].iGateCur = 0
						bTeleport = ORR_RACE_GATE_JUMP_INITIAL(Race, i)//ORR_Race_Racer_Gate_Jump_Cur(Race, i)
					ELSE
						bTeleport = ORR_Race_Racer_Gate_Jump_Nxt( Race, i )
					ENDIF
					
					iPlayerIndex = i
				ENDIF				
			ENDREPEAT
			
			IF bTeleport
				IF bRacerIsPlayer 							
					IF (Race.Racer[iPlayerIndex].iGateCur-1 >= 0)				
						VECTOR vOffroadReset					
						vOffroadReset = Race.sGate[Race.Racer[iPlayerIndex].iGateCur-1].vPos
						SET_ENTITY_COORDS(Race.Racer[iPlayerIndex].Vehicle, vOffroadReset)
						FREEZE_ENTITY_POSITION(Race.Racer[iPlayerIndex].Vehicle, TRUE)					
						LOAD_SCENE(vOffroadReset)
						FLOAT fGroundZ
						IF GET_GROUND_Z_FOR_3D_COORD(vOffroadReset, fGroundZ)
							vOffroadReset.z = fGroundZ
							SET_ENTITY_COORDS(Race.Racer[iPlayerIndex].Vehicle, vOffroadReset)																			
						ENDIF	
						FREEZE_ENTITY_POSITION(Race.Racer[iPlayerIndex].Vehicle, FALSE)					
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					CPRINTLN(DEBUG_OR_RACES, "[ORR_Race.sch->ORR_Race_Racer_Reset] From ORR_RACER_RESET_PLACE to ORR_RACER_RESET_FADE_IN.")
					Racer.eReset = ORR_RACER_RESET_FADE_IN				
				
				ELSE // Otherwise, racer is ai, go to wait state.
					CPRINTLN(DEBUG_OR_RACES, "[ORR_Race.sch->ORR_Race_Racer_Reset] From ORR_RACER_RESET_PLACE to ORR_RACER_RESET_WAIT.")
					Racer.eReset = ORR_RACER_RESET_GROUND_CLAMP
				ENDIF
			ENDIF
		BREAK
		
		CASE ORR_RACER_RESET_GROUND_CLAMP
			IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
				AND Race.bFailChecking
				IF (GET_ENTITY_HEIGHT_ABOVE_GROUND(Racer.Vehicle) > 1.5)
					VECTOR vEntityCoords
					vEntityCoords = GET_ENTITY_COORDS(Racer.Vehicle)
					vEntityCoords.z -= GET_ENTITY_HEIGHT_ABOVE_GROUND(Racer.Vehicle) + 1.5	
					SET_ENTITY_COORDS(Racer.Vehicle, vEntityCoords, TRUE)
				ENDIF
				
				IF Racer.Driver <> PLAYER_PED_ID()
					TEXT_LABEL OffroadRecording
					OffroadRecording = "Offroad_"
					SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
						CASE OFFROAD_RACE_CANYON_CLIFFS
							OffroadRecording += 1
						BREAK
						CASE OFFROAD_RACE_RIDGE_RUN
							OffroadRecording += 2
						BREAK
						CASE OFFROAD_RACE_MINEWARD_SPIRAL
							OffroadRecording += 6
						BREAK
						CASE OFFROAD_RACE_VALLEY_TRAIL
							OffroadRecording += 3
						BREAK
						CASE OFFROAD_RACE_LAKESIDE_SPLASH
							OffroadRecording += 4
						BREAK
						CASE OFFROAD_RACE_ECO_FRIENDLY
							OffroadRecording += 5
						BREAK
					ENDSWITCH
					CDEBUG3LN( DEBUG_OR_RACES, "Tasking vehicle to follow route ", OffroadRecording )
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Racer.Driver, Racer.Vehicle, OffroadRecording, DRIVINGMODE_AVOIDCARS_RECKLESS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, GET_VEHICLE_ESTIMATED_MAX_SPEED(Racer.Vehicle))			
				ENDIF
			ENDIF
			
			Racer.eReset = ORR_RACER_RESET_WAIT
		BREAK
		
		CASE ORR_RACER_QUIT_PLACE
			// Reset vehicle here.
			IF (ORR_Master.eRaceType = ORR_RACE_TYPE_PLANE)	 
				ORR_Racer_Vehicle_Create(Racer, TRUE)
				LOAD_SCENE(ORR_Master.vDefRcrPos)
			ENDIF
			
			// TODO: We may have to update this later on to determine where the player is teleported
			//		based on the race location, or if teleporting him is even desired.
			ORR_Racer_Teleport(Racer, ORR_Master.vDefRcrPos, ORR_Master.fDefRcrHead	, 0.0)	
			
			CDEBUG1LN(DEBUG_OR_RACES, "[ORR_Race->ORR_Race_Racer_Reset] CASE ORR_RACER_QUIT_PLACE: eReset = ORR_RACER_QUIT_FADE_IN")
			Racer.eReset=ORR_RACER_QUIT_FADE_IN
		BREAK
		
		
		// Fade in.
		CASE ORR_RACER_RESET_FADE_IN
			Race.bFailChecking = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACER_RESET_FADE_IN:  Race.bFailChecking set to TRUE")
/*			CDEBUG1LN(DEBUG_OR_RACES, "RACER_RESET_FADE_IN: Failchecking set to: ")
			PRINTBOOL(Race.bFailChecking)
			PRINTNL()
*/			// Auto-pilot racer while waiting for fade in.
			//RESTART_TIMER_NOW(manualResetTimer)
			IF ORR_Master.eRaceType = ORR_RACE_TYPE_PLANE
				ORR_Racer_AutoPilot(Race.Racer[i], ORR_PLANE_SPD_MAX, FALSE)
			ENDIF
			IF ORR_FadeIn_Safe(ORR_FADE_QUICK_TIME)
				CDEBUG1LN(DEBUG_OR_RACES, "RACER_RESET_FADE_IN: FAILCHECKING: ON")
				IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
					IF NOT IS_PED_IN_VEHICLE(Race.Racer[0].Driver, Race.Racer[0].Vehicle) AND NOT (ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON)
						SET_PED_INTO_VEHICLE(Race.Racer[0].Driver, Race.Racer[0].Vehicle, VS_DRIVER)
					ENDIF
				ENDIF
				
//				ORR_ENABLE_RADIO_ON_VEHICLE(Race.Racer[0].Vehicle, TRUE)				
								
				CDEBUG1LN(DEBUG_OR_RACES, "Going to RESET_WAIT from QUIT_FADE_IN")
				IF bRacerIsPlayer AND ORR_Master.eRaceType = ORR_RACE_TYPE_PLANE
					ORR_Racer_ClockTime_Penalty(Race.Racer[0], ORR_GATE_MISS_PENALTY)
				ENDIF
				Racer.eReset = ORR_RACER_RESET_WAIT
			ENDIF
			
			// Reset SPR Racer waiting.
			RETURN FALSE	
		BREAK
		
		CASE ORR_RACER_QUIT_FADE_IN
			bQuitting = TRUE
			// Auto-pilot racer while waiting for fade in.
			//ORR_Racer_AutoPilot(Race.Racer[i], ORR_PLANE_SPD_MAX, FALSE)
			IF ORR_FadeIn_Safe(ORR_FADE_QUICK_TIME)
			
				// Clean Scaleform UI for reset/restart.
				
				CDEBUG1LN(DEBUG_OR_RACES, "[ORR_Race->ORR_Race_Racer_Reset] CASE ORR_RACER_QUIT_FADE_IN: eUpdate = ORR_RACE_UPDATE_CLEANUP")
				Race.eUpdate = ORR_RACE_UPDATE_CLEANUP
			ENDIF
			// Reset SPR Racer waiting.
			RETURN FALSE
		
		BREAK
		
		
		// Reset racer (wait).
		CASE ORR_RACER_RESET_WAIT
			// If racer crashed, go to init state.
			IF  Racer.eVehicleModel <> DUMMY_MODEL_FOR_SCRIPT
				IF NOT IS_ENTITY_DEAD(Racer.Driver) 
					IF ORR_Racer_Crash_Check(Racer)
						//OR ORR_IS_RACER_TOO_FAR_BACK( Race, Racer )
//					OR ( NOT IS_PED_IN_VEHICLE(Racer.Driver, Racer.Vehicle) AND (ORR_Master.eRaceType != ORR_RACE_TYPE_OFFROAD) )
						//CDEBUG1LN(DEBUG_OR_RACES, "Player isn't in the vehicle, moving to ORR_RACER_RESET_INIT")
						IF Racer.Driver <> PLAYER_PED_ID()
							RESTART_TIMER_NOW(exitTimer)
							Racer.eReset = ORR_RACER_RESET_INIT
							RETURN TRUE
						ENDIF
					ELIF Racer.Driver <> PLAYER_PED_ID()
						IF ORR_IS_RACER_TOO_FAR_BACK( Race, Racer )
							ORR_TELEPORT_RACER_TOO_FAR_BACK( Race, Racer )
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Reset SPR Racer waiting.
			RETURN FALSE
			
		BREAK
		
		CASE ORR_RACER_QUIT_EXIT
		
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEHICLELOCK_UNLOCKED)
					ENDIF
				ENDIF
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
			ENDIF
			ORR_Race_Gate_Deactivate_All(Race)
			CANCEL_TIMER(Race.tClock)
			Race.eUpdate = ORR_RACE_UPDATE_CLEANUP
		BREAK
		
	ENDSWITCH
	
	// Reset SPR Racer running.
	RETURN TRUE
ENDFUNC


PROC ORR_RACER_UPDATE_BLIP_COORDS(VEHICLE_INDEX& veh, BLIP_INDEX& blip)
	IF IS_ENTITY_DEAD( PLAYER_PED_ID() )
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(veh) AND IS_VEHICLE_DRIVEABLE(veh)
		AND IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
		IF NOT DOES_BLIP_EXIST(blip)

			blip = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(veh))
			SET_BLIP_COLOUR(blip, BLIP_COLOUR_BLUE)
			SET_BLIP_PRIORITY(blip, BLIPPRIORITY_HIGHEST)
			BEGIN_TEXT_COMMAND_SET_BLIP_NAME("OFF_OPP")
			END_TEXT_COMMAND_SET_BLIP_NAME(blip)

		ELSE

			VECTOR vBlipPosition    = GET_BLIP_COORDS(blip)
			VECTOR vVehiclePosition = GET_ENTITY_COORDS(veh)

			vBlipPosition.X = vBlipPosition.X + ((vVehiclePosition.X - vBlipPosition.X) / 10.0)
			vBlipPosition.Y = vBlipPosition.Y + ((vVehiclePosition.Y - vBlipPosition.Y) / 10.0)
			vBlipPosition.Z = vBlipPosition.Z + ((vVehiclePosition.Z - vBlipPosition.Z) / 10.0)

			SET_BLIP_COORDS(blip, vBlipPosition)

		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blip)
			REMOVE_BLIP(blip)
		ENDIF
	ENDIF


ENDPROC

/// PURPOSE:
///    Update SPR Race Racers.
/// RETURNS:
///    TRUE if still updating SPR Race Racers.
FUNC BOOL ORR_Race_Racer_Update(ORR_RACE_STRUCT& Race, ORR_RACE_TYPE_ENUM eRaceType)
	// Local variables.
	FLOAT fClockTime	
	INT i
					
	// Cache current race clock time.
	IF IS_TIMER_STARTED(Race.tClock)
		fClockTime = GET_TIMER_IN_SECONDS(Race.tClock)	
	ENDIF


	ORR_Race_Manage_Race_Beats(Race.Racer[0].iGateCur, Race.bFailChecking)
	
	
	++iFrameCounter
	
	// Loop through all racers and update them.
	REPEAT Race.iRacerCnt i
	
		// Don't update racers that already finished the race. (or racers that are dead)
		IF NOT IS_PED_INJURED(Race.Racer[i].Driver) AND (Race.Racer[i].iGateCur < Race.iGateCnt)
			IF iFrameCounter % Race.iRacerCnt = i
				ORR_UPDATE_RACER_IN_SPEED_VOLUME( Race, i )
			ENDIF
			ORR_GATE_STRUCT GateCur, GateNxt
			INT iGateCur
			
			
			// Check if racer is player and update Scaleform UI, if needed.
			BOOL bRacerIsPlayer = FALSE
			
			IF (Race.Racer[i].Driver = PLAYER_PED_ID())
				bRacerIsPlayer = TRUE
			ENDIF
			
			//B*1466276 - Check if any racer has been shocked by an explosion caused from a grenade
			// This doesn't work 100%
//			IF IS_SHOCKING_EVENT_IN_SPHERE(EVENT_SHOCKING_EXPLOSION, GET_ENTITY_COORDS(Race.Racer[i].Driver),2.5)
//				//Copied this fail from the idle Fail, hopefully this handles everything
//				CDEBUG1LN(DEBUG_OR_RACES, "[FAIL] - player spooked another racer with an explosion")
//				IF (Race.Racer[0].iGateCur <= Race.iGateCnt-1)
//					IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur].blip)
//						SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur].blip, DISPLAY_NOTHING)
//					ENDIF
//				ENDIF
//				IF ((Race.Racer[0].iGateCur+1) <= (Race.iGateCnt-2))
//					IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur+1].blip)
//						SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur+1].blip, DISPLAY_NOTHING)
//					ENDIF
//				ENDIF
//				CANCEL_TIMER(idleTimer)
//				RESTART_TIMER_NOW(exitTimer)
//				Race.bFailChecking = FALSE
//				CDEBUG1LN(DEBUG_OR_RACES, "[FAIL] Explosion:  Race.bFailChecking set to FALSE")
//				CLEAR_PRINTS()
//				CPRINTLN( DEBUG_OR_RACES, "CLEARING HELP IN ORR_HELPERS LINE 2184" )
//				CLEAR_HELP()
//				SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
//				SET_SCALEFORM_OFFROAD_FAIL_MESSAGE(Race.bigMessageUI)
//
//				//SET_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI, orrFailString, orrFailStrapline, 4000, HUD_COLOUR_RED, MG_BIG_MESSAGE_CENTERED)
//				Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
//			ENDIF
			
			// Cache current index and cur/next gates.
			iGateCur = Race.Racer[i].iGateCur
			IF (Race.Racer[i].iGateCur >= Race.iGateCnt)
				GateCur = Race.sGate[Race.iGateCnt-1]
			ELSE
				GateCur = Race.sGate[iGateCur]
			ENDIF
			IF (iGateCur < (Race.iGateCnt-1))
				GateNxt = Race.sGate[iGateCur + 1]
			ELSE
				GateNxt = GateCur
			ENDIF

			// Udpate racer clock time (account for plus/minus) (clamp negatives).
			IF iGateCur <> Race.iGateCnt		
				Race.Racer[i].fClockTime = fClockTime - Race.Racer[i].fPlsMnsTot
				IF (Race.Racer[i].fClockTime < 0.0)
					Race.Racer[i].fClockTime = 0.0
				ENDIF
			ENDIF	
			
			//set racer blip
			IF NOT bRacerIsPlayer
			AND NOT bQuitting
			AND NOT bFailedNoBlips
				IF iFrameCounter % Race.iRacerCnt = i
					ORR_RACER_UPDATE_BLIP_COORDS(Race.Racer[i].Vehicle, Race.Racer[i].Blip)
				ENDIF
			ENDIF
			
//			IF bRacerIsPlayer
//				IF Race.ePlayerRespawnState = ORR_RESPAWN_CHECK
//					DRAW_GENERIC_METER( ROUND( GET_TIMER_IN_SECONDS( resetTimer ) * 1000 ), 1500, "RACES_RMETER", HUD_COLOUR_RED, 0, HUDORDER_EIGHTHBOTTOM, -1, -1, FALSE, TRUE )
//				ENDIF			
//			ENDIF			
			
				// Make sure racer isn't resetting before continuing.
			IF NOT ORR_Race_Racer_Reset(Race, Race.Racer[i], bRacerIsPlayer)
				IF NOT bRacerIsPlayer
				AND iFrameCounter % Race.iRacerCnt = i
					ORR_UPDATE_RACER_FALLING_FROM_CLIFFS( Race, i )
				ENDIF
				// Vehicle water check
				IF bRacerIsPlayer	
					IF IS_CONTROL_ENABLED( PLAYER_CONTROL, INPUT_VEH_EXIT )
						DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_EXIT )
					ENDIF
					
					IF ( IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_EXIT)
						OR IS_DISABLED_CONTROL_JUST_RELEASED( PLAYER_CONTROL, INPUT_VEH_EXIT ) )
						AND NOT Race.bPlayerRespawning
						Race.ePlayerRespawnState = ORR_RESPAWN_SET
						ORR_Master.bRespawnPressed = FALSE
						IF IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
							TASK_LEAVE_ANY_VEHICLE( PLAYER_PED_ID() )
						ENDIF
					ELIF IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_VEH_EXIT )
						OR IS_DISABLED_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_VEH_EXIT )
						OR Race.bPlayerRespawning
						SWITCH Race.ePlayerRespawnState
							CASE ORR_RESPAWN_SET
								ORR_Master.bRespawnPressed = TRUE
								RESTART_TIMER_NOW( resetTimer )
								CPRINTLN( DEBUG_OR_RACES, "Switching respawn state to RESPAWN_INIT" )
								Race.ePlayerRespawnState = ORR_RESPAWN_INIT
							BREAK
							CASE ORR_RESPAWN_INIT
								IF GET_TIMER_IN_SECONDS( resetTimer ) > 0.4
									RESTART_TIMER_NOW( resetTimer )
									Race.ePlayerRespawnState = ORR_RESPAWN_CHECK
									CPRINTLN( DEBUG_OR_RACES, "Switching respawn state to RESPAWN_CHECK" )
								ENDIF
							BREAK
							CASE ORR_RESPAWN_CHECK
								IF GET_TIMER_IN_SECONDS( resetTimer ) > 1.5
									Race.ePlayerRespawnState = ORR_RESPAWN_ACTIVE
									Race.Racer[0].eReset = ORR_RACER_RESET_FADE_OUT
									CLEAR_BITMASK_AS_ENUM( ORR_HELP_BIT, ORR_RESET_HELP )
									Race.bFailChecking = FALSE
									Race.bPlayerRespawning = TRUE
									ORR_Master.bRespawnPressed = FALSE
									CPRINTLN( DEBUG_OR_RACES, "Switching respawn state to RESPAWN_ACTIVE" )
								ENDIF
							BREAK
							CASE ORR_RESPAWN_ACTIVE
								Race.ePlayerRespawnState = ORR_RESPAWN_WAIT
							BREAK
							CASE ORR_RESPAWN_WAIT
								IF NOT IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_VEH_EXIT )
								AND NOT IS_DISABLED_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_VEH_EXIT )
									ENABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_EXIT )
									CPRINTLN( DEBUG_OR_RACES, "Switching respawn state to RESPAWN_WAIT" )
									Race.ePlayerRespawnState = ORR_RESPAWN_SET
									Race.bPlayerRespawning = FALSE
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
				
				
				IF bRacerIsPlayer
					IF Race.ePlayerRespawnState = ORR_RESPAWN_CHECK
						DRAW_GENERIC_METER( ROUND( GET_TIMER_IN_SECONDS( resetTimer ) * 1000 ), 1500, "RACES_RMETER", HUD_COLOUR_RED, 0, HUDORDER_EIGHTHBOTTOM, -1, -1, FALSE, TRUE )
					ENDIF			
				ENDIF
				
				//set racer blip
				IF NOT bRacerIsPlayer
				AND NOT bQuitting
				AND NOT bFailedNoBlips
					ORR_RACER_UPDATE_BLIP_COORDS(Race.Racer[i].Vehicle, Race.Racer[i].Blip)
				ENDIF
				
				// Calculate racer rank.	
				IF iFrameCounter % Race.iRacerCnt = i
					ORR_Race_Racer_Rank_Calc(Race, i)	
				ENDIF
				
				// Check if racer is player.
				IF bRacerIsPlayer
					// If it's time to display plus/minus, start timer.
					IF (Race.Racer[i].fPlsMnsLst <> 0.0)
						START_TIMER_NOW(Race.Display.tCnt)
					ENDIF
					
					IF IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_FORCE_GATE_ACTIVATION)
						ORR_Race_Gate_Activate(Race, iGateCur, TRUE)
						IF (iGateCur < (Race.iGateCnt - 1))
							ORR_Race_Gate_Activate(Race, iGateCur + 1, FALSE)
						ENDIF
						CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_FORCE_GATE_ACTIVATION)
					ENDIF										
				ENDIF
			
				// Check if racer has passed/missed their current gate.
				IF Race.bFailChecking AND bRacerIsPlayer
					Race.iGateCheck = ENUM_TO_INT(ORR_Racer_Gate_Check(Race, GateCur, GateNxt, Race.Racer[i].Driver))
					OOR_RACE_MANAGE_CHECKPOINT_ALPHAS(GateCur, GateNxt)
				ELIF NOT bRacerIsPlayer
					Race.iGateCheck = ENUM_TO_INT(ORR_Racer_Gate_Check(Race, GateCur, GateNxt, Race.Racer[i].Driver))
					
					IF ENUM_TO_INT(ORR_RACE_GATE_STATUS_PASS) = Race.iGateCheck
						CPRINTLN( DEBUG_OR_RACES, "Racer ", i, " has passed gate ", Race.Racer[i].iGateCur )
					ENDIF
				ENDIF
				
				IF (Race.iGateCheck <> ENUM_TO_INT(ORR_RACE_GATE_STATUS_INVALID))		
					
					// Increment racer current gate and re-cache it.
					++Race.Racer[i].iGateCur
					iGateCur = Race.Racer[i].iGateCur
					IF (iGateCur >= Race.iGateCnt-1)
						GateCur = Race.sGate[Race.iGateCnt-1]
					ELSE
						GateCur = Race.sGate[iGateCur]
					ENDIF

					// Check if racer is player.
					IF bRacerIsPlayer
						// If we're at the last gate, end the race.
						ORR_Race_Gate_Deactivate(Race, iGateCur - 1)
						IF (iGateCur = Race.iGateCnt)
							IF NOT IS_TIMER_PAUSED(Race.tClock)
								PAUSE_TIMER(Race.tClock)
							ENDIF
							
							// RACE END.
							IF (eRaceType = ORR_RACE_TYPE_OFFROAD)
								Race.bFailChecking = FALSE
								g_savedGlobals.sOffroadData.iRacePlaceEarned = 999
								IF (Race.Racer[i].iRank <= 3)
									CPRINTLN(DEBUG_OR_RACES, "********Offroad Win logged in ORR_RACE.sch ********")
									g_savedGlobals.sOffroadData.iRacePlaceEarned = Race.Racer[i].iRank
									
									// If we won AN OFFROAD RACE, send a signal to the controller.
									SET_BITMASK_AS_ENUM(g_savedGlobals.sOffroadData.iBitFlags, OFFROAD_F_WonCurrentRace)

								ENDIF
								
							ELIF (eRaceType = ORR_RACE_TYPE_TRIATHLON)
								IF (Race.Racer[i].iRank <= 3)
									CPRINTLN(DEBUG_OR_RACES, "******** Triathlon Win logged in ORR_RACE.sch ********")
									
									// If we won A TRIATHLON, change our global data.
									SET_BITMASK_AS_ENUM(g_savedGlobals.sTriathlonData.iBitFlags, TRIATHLON_WonRace)
								ENDIF
								TRIGGER_MUSIC_EVENT("MGTR_COMPLETE")
								CPRINTLN(DEBUG_OR_RACES, "[spr_race.sch->ORR_Race_Racer_Update] MGTR_COMPLETE triggered")
							ENDIF
							
							RETURN FALSE
							
						// Otherwise, activate new current/next gates.
						ELSE
							ORR_Race_Gate_Activate(Race, iGateCur, TRUE)
							IF (iGateCur < (Race.iGateCnt - 1))
								ORR_Race_Gate_Activate(Race, iGateCur + 1, FALSE)
							ENDIF
						ENDIF
						
						// If split is still displaying since last gate, cancel it.
						IF (Race.Racer[i].fPlsMnsLst = 0.0)
							IF IS_TIMER_STARTED(Race.Display.tCnt)
								CANCEL_TIMER(Race.Display.tCnt)
							ENDIF
						ENDIF
						

						// If it's time to display split, calculate it and restart timer.
						IF Race.iGateCnt > 0
							IF (Race.fBestSplitTime > 0.0)
								IF (iGateCur = ROUND(Race.iGateCnt / 2.0))									
									Race.Racer[i].fPlsMnsLst = 0.0
									RESTART_TIMER_NOW(Race.Display.tCnt)
								ENDIF
							ENDIF
						ENDIF
						
					// Otherwise, racer is ai controlled.
					ELSE
						IF (Race.Racer[i].iGateCur >= Race.iGateCnt)
							IF NOT IS_ENTITY_DEAD(Race.Racer[i].Driver)
								FINISH_AI_RACERS(Race.Racer[i])
							ENDIF
						ELSE
							IF ORR_Master.eRaceType <> ORR_RACE_TYPE_OFFROAD 
								// TODO: Put any player specific logic here...
								IF i != 0 //not the player!?
									ORR_Race_Racer_Task_Go_To_Next_Gate(Race, Race.Racer[i].Driver, Race.Racer[i].Vehicle, Race.sGate[Race.Racer[i].iGateCur].vPos, Race.sGate[Race.Racer[i].iGateCur].eChkpntType, i)			
								ENDIF
							ELSE
								//SEQUENCE_INDEX siSeq
								TEXT_LABEL OffroadRecording = "Offroad_"
								SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
									CASE OFFROAD_RACE_CANYON_CLIFFS
										OffroadRecording += 1
									BREAK
									CASE OFFROAD_RACE_RIDGE_RUN
										OffroadRecording += 2
									BREAK
									CASE OFFROAD_RACE_MINEWARD_SPIRAL
										OffroadRecording += 6
									BREAK
									CASE OFFROAD_RACE_VALLEY_TRAIL
										OffroadRecording += 3
									BREAK
									CASE OFFROAD_RACE_LAKESIDE_SPLASH
										OffroadRecording += 4
									BREAK
									CASE OFFROAD_RACE_ECO_FRIENDLY
										OffroadRecording += 5
									BREAK
								ENDSWITCH
								
								// Task AI to follow waypoint recording for offroad race
								// Pick random waypoint recording to start
								IF i > 0
									IF ORR_USE_WAYPOINT_RECORDING
										IF NOT IS_ENTITY_DEAD(Race.Racer[i].Driver)
										AND NOT IS_ENTITY_DEAD(Race.Racer[i].Vehicle)
											IF (GET_SCRIPT_TASK_STATUS(Race.Racer[i].Driver, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) = PERFORMING_TASK)
												CDEBUG1LN(DEBUG_OR_RACES, "AI on waypoint recording")
											ELIF Race.bFailChecking
												CDEBUG1LN(DEBUG_OR_RACES, "[WARNING] - task check 2")
												TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Race.Racer[i].Driver, Race.Racer[i].Vehicle, OffroadRecording, DRIVINGMODE_AVOIDCARS_RECKLESS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, GET_VEHICLE_ESTIMATED_MAX_SPEED(Race.Racer[i].Vehicle))
											ENDIF
										ENDIF
									ELSE
										IF i != 0 //not the player!?
											ORR_Race_Racer_Task_Go_To_Next_Gate(Race, Race.Racer[i].Driver, Race.Racer[i].Vehicle, Race.sGate[Race.Racer[i].iGateCur].vPos, Race.sGate[Race.Racer[i].iGateCur].eChkpntType, i)			
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							ORR_Race_Racer_Update_Rubberband_Speed(Race, i)
						ENDIF
					ENDIF			

				ENDIF	
			ELIF NOT bRacerIsPlayer
				//continue updating the gate check for AI
				Race.iGateCheck = ENUM_TO_INT(ORR_Racer_Gate_Check(Race, GateCur, GateNxt, Race.Racer[i].Driver))
				
				IF ENUM_TO_INT(ORR_RACE_GATE_STATUS_PASS) = Race.iGateCheck
					CPRINTLN( DEBUG_OR_RACES, "Racer ", i, " has passed gate ", Race.Racer[i].iGateCur )
				ENDIF
			ENDIF
		ELSE
			CPRINTLN( DEBUG_OR_RACES, "Racer ", i, " is injured and not updating..." )
		ENDIF			
	ENDREPEAT
	
	IF Race.bFailChecking = TRUE
//		CDEBUG1LN(DEBUG_OR_RACES, "ACTIVELY CHECKING FAIL CASES")
		RESET_TUTORIAL(Race)
		EXIT_VEHICLE_FAILURE(Race, returnToVehBlip)
		Dead_Race_Vehicle(Race, returnToVehBlip)
		wantedFailure(Race)
		ORR_FAIL_FOR_WEAPONS_ABUSE(Race)
		waterFailure(Race)
		idleFailure(Race)
		Disable_Airport_Icons()
		RUN_OFFROAD_AUDIO(Race)
		ORR_Race_Draw_HUD(Race)	
			
	ENDIF
	
	// Update SPR Race Racers still running.
	RETURN TRUE
ENDFUNC




// -----------------------------------
// FILE I/O PROCS/FUNCTIONS
// -----------------------------------

/// PURPOSE:
///    Init SPR Race.
PROC ORR_Race_Init(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Init")
	CANCEL_TIMER(Race.tClock)
	// TEMP: Pulling in best clock/split times from master.
	// TODO: Might want to put this into it's own function.
	IF (ORR_Master.iRaceCur <> -1)
		FLOAT fBestClockTime = ORR_Global_BestTime_Get(ORR_Master.iRaceCur)
		IF (fBestClockTime <= 0.0)
		OR (fBestClockTime > ORR_Master.fRaceTime[ORR_Master.iRaceCur])
			fBestClockTime = ORR_Master.fRaceTime[ORR_Master.iRaceCur]
		ENDIF
		Race.fBestClockTime = fBestClockTime
		Race.fBestSplitTime = Race.fBestClockTime / 2.0
	ENDIF
	ORR_Race_Gate_Init_All(Race)
	Race.iGateCnt = 0
	ORR_Race_Racer_Init_All(Race)
	Race.iRacerCnt = 0
	
	CLEAR_AREA_AROUND_ORR_POS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE))
	// TODO: Should I be resetting state machine data here?
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Load SPR Race.
/// RETURNS:
///    TRUE if SPR Race was successfully loaded.
FUNC BOOL ORR_Race_Load(ORR_RACE_STRUCT& Race, STRING sRaceFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load")
	sRaceFileName = sRaceFileName
	UNUSED_PARAMETER(Race)
/*
	
	// Assemble filename for Race XML file.
	TEXT_LABEL_63 szFileName
	szFileName = ORR_Master.szSPRPath
	szFileName += ORR_Master.szMainPath
	szFileName += ORR_Master.szRacesPath
	szFileName += sRaceFileName
	szFileName += ".xml"
	
	// Load Race XML file into memory.
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Opening filepath: ", ORR_Master.szXMLPath)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Opening filename: ", szFileName)
	IF NOT ORR_XML_Load(szFileName)
		SCRIPT_ASSERT("ORR_Race_Load: Failed to load Race XML file!")
		DELETE_XML_FILE()
		RETURN FALSE
	ENDIF
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Started reading Race XML file: ", sRaceFileName)
	
	// Local variables.
	INT iTempBuff
	FLOAT fTempBuff
	VECTOR vTempBuff
	FLOAT fGateRadius
	ORR_RACE_CHECKPOINT_TYPE eChkpntType
	//ORR_RACE_STUNT_GATE_ENUM eStuntType
	TEXT_LABEL_31 szRacerName
	PED_TYPE eDriverType
	MODEL_NAMES eDriverModel
	MODEL_NAMES eVehicleModel
	BOOL bDataLimitReached
	INT iNodeCnt, iNodeName, iAttrCnt
	
	// Init race data.
	ORR_Race_Init(Race)
	
	// Start at root node of Race XML file.
	ORR_XML_GetRootNode(iNodeCnt, iNodeName)
	
	// Check for "Race" root node before continuing.
	IF (iNodeName <> ORR_XML_HASH_RACE)
		SCRIPT_ASSERT("ORR_Race_Load: Failed to find 'Race' root node")
		DELETE_XML_FILE()
		RETURN FALSE
	ENDIF
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Inside 'Race' root node")
	ORR_XML_GetNextNode(iNodeCnt, iNodeName)
	
	// Check for "Gates" parent node before continuing.
	IF (iNodeName <> ORR_XML_HASH_GATES)
		SCRIPT_ASSERT("ORR_Race_Load: Failed to find 'Gates' parent node")
		DELETE_XML_FILE()
		RETURN FALSE
	ENDIF
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Inside 'Gates' parent node")
	ORR_XML_GetNextNode(iNodeCnt, iNodeName)
	
	// Loop through each node of "Gates" branch.
	bDataLimitReached = FALSE
	WHILE (iNodeName <> ORR_XML_HASH_RACERS)
	
		// Check for nodes in "Gates" branch.
		SWITCH (iNodeName)
		
			// "Gate" node.
			CASE ORR_XML_HASH_GATE
			
				// Check if "Gate" data limit has been reached.
				IF (Race.iGateCnt >= ORR_GATE_MAX)
					CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Reached 'Gate' data limit")
					bDataLimitReached = TRUE
					BREAK
				ENDIF
				
				// Debug info.
				PRINTSTRING("ORR_Race_Load: Inside 'Gate' node: ")
				PRINTINT(Race.iGateCnt)
				PRINTNL()
				
			BREAK
			
			// "Position" node.
			CASE ORR_XML_HASH_POS
			
				// Check if "Gate" data limit has been reached.
				IF bDataLimitReached
					BREAK
				ENDIF
				
				// Loop through each attribute of "Position" node and save data.
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Inside 'Position' node")
				vTempBuff = ORR_Master.vDefRcrPos
				REPEAT GET_NUMBER_OF_XML_NODE_ATTRIBUTES() iAttrCnt
					SWITCH (GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttrCnt)))
						CASE ORR_XML_HASH_X
							vTempBuff.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'X' data")
						BREAK
						CASE ORR_XML_HASH_Y
							vTempBuff.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'Y' data")
						BREAK
						CASE ORR_XML_HASH_Z
							vTempBuff.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'Z' data")
						BREAK
					ENDSWITCH
				ENDREPEAT
				
			BREAK
			
			// "Checkpoint" node.
			CASE ORR_XML_HASH_CHKPNT
			
				// Check if "Gate" data limit has been reached.
				IF bDataLimitReached
					BREAK
				ENDIF
				
				// Loop through each attribute of "Checkpoint" node and save data.
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Inside 'Checkpoint' node")
				fGateRadius = ORR_GATE_CHKPNT_SCL
				eChkpntType = ORR_CHKPT_OFFROAD_DEFAULT
				REPEAT GET_NUMBER_OF_XML_NODE_ATTRIBUTES() iAttrCnt
				
					SWITCH (GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttrCnt)))
						CASE ORR_XML_HASH_RADIUS
							fGateRadius = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'Radius' data")
						BREAK
						CASE ORR_XML_HASH_TYPE
							iTempBuff = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							eChkpntType = ORR_ChkpntType_GetEnum(iTempBuff)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'Type' data")
						BREAK
						CASE ORR_XML_HASH_STUNT
							iTempBuff = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							//eStuntType = ORR_StuntType_GetEnum(iTempBuff)
						BREAK
					ENDSWITCH
				ENDREPEAT
				
				// Setup race Gate using saved data from "Position"/"Checkpoint" nodes.
				ORR_Gate_Setup(Race.sGate[Race.iGateCnt], vTempBuff, fGateRadius, eChkpntType) //, eStuntType)
				PRINTSTRING("ORR_Race_Load: Setup Gate: ")
				PRINTINT(Race.iGateCnt)
				PRINTNL()
				
				// Increment race Gate count.
				++Race.iGateCnt
				
			BREAK
			
		ENDSWITCH
		
		// Get next node of "Gates" branch.
		ORR_XML_GetNextNode(iNodeCnt, iNodeName)
		
	ENDWHILE
	
	// Ensure there is at least 1 gate.
	IF (Race.iGateCnt <= 0)
		Race.iGateCnt = 1
	ENDIF
	
	// Check for "Racers" parent node before continuing.
	IF (GET_HASH_KEY(GET_XML_NODE_NAME()) <> ORR_XML_HASH_RACERS)
		SCRIPT_ASSERT("ORR_Race_Load: Failed to find 'Racers' parent node")
		DELETE_XML_FILE()
		RETURN FALSE
	ENDIF
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Inside 'Racers' parent node")
	ORR_XML_GetNextNode(iNodeCnt, iNodeName)
	
	// Loop through each node of "Racers" branch.
	bDataLimitReached = FALSE
	WHILE (iNodeCnt < GET_NUMBER_OF_XML_NODES())
	
		// Check for nodes in "Racers".
		SWITCH (iNodeName)
		
			// "Racer" node.
			CASE ORR_XML_HASH_RACER
			
				// Check if "Racer" data limit has been reached.
				IF (Race.iRacerCnt >= ORR_RACER_MAX)
					CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Reached 'Racer' data limit")
					bDataLimitReached = TRUE
					BREAK
				ENDIF
				
				// Debug info.
				PRINTSTRING("ORR_Race_Load: Inside 'Racer' node: ")
				PRINTINT(Race.iRacerCnt)
				PRINTNL()
				
				// Loop through each attribute of "Racer" node and save data.
				szRacerName = "Racer"
				REPEAT GET_NUMBER_OF_XML_NODE_ATTRIBUTES() iAttrCnt
					SWITCH (GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttrCnt)))
						CASE ORR_XML_HASH_NAME
							szRacerName = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'Name' data")
						BREAK
					ENDSWITCH
				ENDREPEAT
				
			BREAK
			
			// "Location" node.
			CASE ORR_XML_HASH_LOC
			
				// Check if "Racer" data limit has been reached.
				IF bDataLimitReached
					BREAK
				ENDIF
				
				// Loop through each attribute of "Location" node and save data.
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Inside 'Location' node")
				vTempBuff = ORR_Master.vDefRcrPos
				fTempBuff = ORR_Master.fDefRcrHead
				REPEAT GET_NUMBER_OF_XML_NODE_ATTRIBUTES() iAttrCnt
					SWITCH (GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttrCnt)))
						CASE ORR_XML_HASH_X
							vTempBuff.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'X' data")
						BREAK
						CASE ORR_XML_HASH_Y
							vTempBuff.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'Y' data")
						BREAK
						CASE ORR_XML_HASH_Z
							vTempBuff.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'Z' data")
						BREAK
						CASE ORR_XML_HASH_H
							fTempBuff = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'H' data")
						BREAK
					ENDSWITCH
				ENDREPEAT
				
			BREAK
			
			// "Driver" node.
			CASE ORR_XML_HASH_DRIVER
			
				// Check if "Racer" data limit has been reached.
				IF bDataLimitReached
					BREAK
				ENDIF
				
				// Loop through each attribute of "Driver" node and save data.
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Inside 'Driver' node")
				eDriverType = ORR_Master.eDefDrvType
				eDriverModel = ORR_Master.eDefDrvModel
				REPEAT GET_NUMBER_OF_XML_NODE_ATTRIBUTES() iAttrCnt
					SWITCH (GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttrCnt)))
						CASE ORR_XML_HASH_TYPE
							iTempBuff = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							eDriverType = ORR_DriverType_GetEnum(iTempBuff)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'Type' data")
						BREAK
						CASE ORR_XML_HASH_MODEL
							iTempBuff = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							eDriverModel = ORR_DriverModel_GetEnum(iTempBuff)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'Model' data")
						BREAK
					ENDSWITCH
				ENDREPEAT
				
			BREAK
			
			// "Vehicle" node.
			CASE ORR_XML_HASH_VEHICLE
			
				// Check if "Racer" data limit has been reached.
				IF bDataLimitReached
					BREAK
				ENDIF
				
				// Loop through each attribute of "Vehicle" node and save data.
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Inside 'Vehicle' node")
				eVehicleModel = ORR_Master.eDefVehModel
				REPEAT GET_NUMBER_OF_XML_NODE_ATTRIBUTES() iAttrCnt
					SWITCH (GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttrCnt)))
						CASE ORR_XML_HASH_MODEL
							iTempBuff = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttrCnt)
							eVehicleModel = ORR_VehicleModel_GetEnum(iTempBuff)
							CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Saved 'Model' data")
						BREAK
					ENDSWITCH
				ENDREPEAT
				
				// Setup race Racer using saved data from "Location"/"Driver"/"Vehicle" nodes.
				// TODO: Forcing entity for player. May want to figure out a better way to do this...
				ORR_Racer_Setup_Misc(Race.Racer[Race.iRacerCnt])
				ORR_Racer_Setup_Name(Race.Racer[Race.iRacerCnt], szRacerName)
				// TODO: Making all entities NULL from start for editor. Figure out way for race...
				//IF (Race.Racer[Race.iRacerCnt].eDriverType <= PEDTYPE_PLAYER_UNUSED)
					//ORR_Racer_Setup_Entities(Race.Racer[Race.iRacerCnt], PLAYER_PED_ID(), ORR_Master.PlayerVeh)
				//ELSE
					ORR_Racer_Setup_Entities(Race.Racer[Race.iRacerCnt], NULL, NULL)
				//ENDIF
				ORR_Racer_Setup_Start(Race.Racer[Race.iRacerCnt], vTempBuff, fTempBuff)
				ORR_Racer_Setup_Types(Race.Racer[Race.iRacerCnt], eDriverType, eDriverModel, eVehicleModel)
				PRINTSTRING("ORR_Race_Load: Setup Racer: ")
				PRINTINT(Race.iRacerCnt)
				PRINTNL()
				
				// Increment race Racer count.
				++Race.iRacerCnt
				
			BREAK
			
		ENDSWITCH
		
		// Get next node of "Racers" branch.
		ORR_XML_GetNextNode(iNodeCnt, iNodeName)
		
	ENDWHILE
	
	// Ensure there is at least 1 racer.
	IF (Race.iRacerCnt <= 0)
		Race.iRacerCnt = 1
	ENDIF
	
	// Unload Race XML file from memory.
	DELETE_XML_FILE()
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Load: Finished reading Race XML file: ", sRaceFileName)
*/	
	// Race successfully loaded.
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Save SPR Race.
/// RETURNS:
///    TRUE if SPR Race was successfully saved.
FUNC BOOL ORR_Race_Save(ORR_RACE_STRUCT& Race, STRING sRaceFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save")
	UNUSED_PARAMETER(Race)
	sRaceFileName = sRaceFileName
	
/*

	// Local variables.
	INT iCnt
	INT iTempBuff
	TEXT_LABEL_63 szFileName
	
	// Assemble filename for Race XML file.
	szFileName = ORR_Master.szSPRPath
	szFileName += ORR_Master.szMainPath
	szFileName += ORR_Master.szRacesPath
	szFileName += sRaceFileName
	szFileName += ".xml"
	
	// Clear Race XML file and open it for saving.
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Opening filepath: ", ORR_Master.szXMLPath)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Opening filename: ", szFileName)
	CLEAR_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
	OPEN_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Started writing Race XML file: ", sRaceFileName)
	
	// Write Race XML file header.
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<!-- This file has been generated using Single Player Races widget -->", ORR_Master.szXMLPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<!-- Feel free to edit this file by hand or by using the widget -->", ORR_Master.szXMLPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<?xml version='1.0'?>", ORR_Master.szXMLPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote Race XML file header")
	
	// Write "Race" node (open).
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<Race>", ORR_Master.szXMLPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote 'Race' node (open)")
	
		// Write "Gates" node (open).
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	<Gates>", ORR_Master.szXMLPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote 'Gates' node (open)")
		
		// Loop through race gates.
		REPEAT Race.iGateCnt iCnt
		
			// Write "Gate" node (open).
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		<Gate>", ORR_Master.szXMLPath, szFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
			PRINTSTRING("ORR_Race_Save: Wrote 'Gate' node (open): ")
			PRINTINT(iCnt)
			PRINTNL()
			
				// Write "Position" node (open/close).
				SAVE_STRING_TO_NAMED_DEBUG_FILE("			<Position ", ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("X = \"", ORR_Master.szXMLPath, szFileName)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.sGate[iCnt].vPos.x, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" ", ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("Y = \"", ORR_Master.szXMLPath, szFileName)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.sGate[iCnt].vPos.y, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" ", ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("Z = \"", ORR_Master.szXMLPath, szFileName)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.sGate[iCnt].vPos.z, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", ORR_Master.szXMLPath, szFileName)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote 'Position' node (open/close)")
				
				// Write "Checkpoint" node (open/close).
				SAVE_STRING_TO_NAMED_DEBUG_FILE("			<Checkpoint Radius = \"", ORR_Master.szXMLPath, szFileName)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.sGate[iCnt].fRadius, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" ", ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("Type = \"", ORR_Master.szXMLPath, szFileName)
				iTempBuff = ORR_ChkpntType_GetIndex(Race.sGate[iCnt].eChkpntType)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iTempBuff, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", ORR_Master.szXMLPath, szFileName)
//				SAVE_STRING_TO_NAMED_DEBUG_FILE("Stunt = \"", ORR_Master.szXMLPath, szFileName)
//				iTempBuff = ORR_StuntType_GetIndex(Race.sGate[iCnt].eStuntType)
//				SAVE_INT_TO_NAMED_DEBUG_FILE(iTempBuff, Spr_master.szXMLPath, szFileName)
//				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", ORR_Master.szXMLPath, szFileName)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote 'Checkpoint' node (open/close)")
				
			// Write "Gate" node (close).
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		</Gate>", ORR_Master.szXMLPath, szFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
			PRINTSTRING("ORR_Race_Save: Wrote 'Gate' node (close): ")
			PRINTINT(iCnt)
			PRINTNL()
			
		ENDREPEAT
		
		// Write "Gates" node (close).
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	</Gates>", ORR_Master.szXMLPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote 'Gates' node (close)")
		
		// Write "Racers" node (open).
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	<Racers>", ORR_Master.szXMLPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote 'Racers' node (open)")
		
		// Loop through race racers.
		REPEAT Race.iRacerCnt iCnt
		
			// Write "Racer" node (open).
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		<Racer Name = \"", ORR_Master.szXMLPath, szFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(Race.Racer[iCnt].szName, ORR_Master.szXMLPath, szFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" >", ORR_Master.szXMLPath, szFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
			PRINTSTRING("ORR_Race_Save: Wrote 'Racer' node (open): ")
			PRINTINT(iCnt)
			PRINTNL()
			
				// Write "Location" node (open/close).
				SAVE_STRING_TO_NAMED_DEBUG_FILE("			<Location ", ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("X = \"", ORR_Master.szXMLPath, szFileName)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.Racer[iCnt].vStartPos.x, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" ", ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("Y = \"", ORR_Master.szXMLPath, szFileName)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.Racer[iCnt].vStartPos.y, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" ", ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("Z = \"", ORR_Master.szXMLPath, szFileName)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.Racer[iCnt].vStartPos.z, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" ", ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("H = \"", ORR_Master.szXMLPath, szFileName)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.Racer[iCnt].fStartHead, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", ORR_Master.szXMLPath, szFileName)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote 'Location' node (open/close)")
				
				// Write "Driver" node (open/close).
				SAVE_STRING_TO_NAMED_DEBUG_FILE("			<Driver Type = \"", ORR_Master.szXMLPath, szFileName)
				iTempBuff = ORR_DriverType_GetIndex(Race.Racer[iCnt].eDriverType)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iTempBuff, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" Model = \"", ORR_Master.szXMLPath, szFileName)
				iTempBuff = ORR_DriverModel_GetIndex(Race.Racer[iCnt].eDriverModel)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iTempBuff, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", ORR_Master.szXMLPath, szFileName)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote 'Driver' node (open/close)")
				
				// Write "Vehicle" node (open/close).
				SAVE_STRING_TO_NAMED_DEBUG_FILE("			<Vehicle Model = \"", ORR_Master.szXMLPath, szFileName)
				iTempBuff = ORR_VehicleModel_GetIndex(Race.Racer[iCnt].eVehicleModel)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iTempBuff, ORR_Master.szXMLPath, szFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", ORR_Master.szXMLPath, szFileName)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote 'Vehicle' node (open/close)")
				
			// Write "Racer" node (close).
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		</Racer>", ORR_Master.szXMLPath, szFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
			PRINTSTRING("ORR_Race_Save: Wrote 'Racer' node (close): ")
			PRINTINT(iCnt)
			PRINTNL()
			
		ENDREPEAT
		
		// Write "Racers" node (close).
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	</Racers>", ORR_Master.szXMLPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szXMLPath, szFileName)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote 'Racers' node (close)")
		
	// Write "Race" node (close).
	SAVE_STRING_TO_NAMED_DEBUG_FILE("</Race>", ORR_Master.szXMLPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Wrote 'Race' node (close)")
	
	// Close Race XML file.
	CLOSE_DEBUG_FILE()
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Save: Finished writing Race XML file: ", sRaceFileName)
*/
	// Race successfully saved.
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Export SPR Race.
/// RETURNS:
///    TRUE if SPR Race was successfully exported.
FUNC BOOL ORR_Race_Export(ORR_RACE_STRUCT& Race, STRING sRaceFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export")
	UNUSED_PARAMETER(Race)
	sRaceFileName = sRaceFileName
	
/*

	// Local variables.
	INT iCnt
	TEXT_LABEL_31 szTempBuff
	TEXT_LABEL_63 szFileName
	
	// Assemble filename for Race SCH file.
	// TODO: Add SPR Path when it is needed.
	//szFileName = ORR_Master.szSPRPath
	szFileName = ORR_Master.szMainPath
	szFileName += ORR_Master.szRacesPath
	szFileName += sRaceFileName
	szFileName += ".sch"
	
	// Clear Race SCH file and open it for saving.
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export: Opening filepath: ", ORR_Master.szSCHPath)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export: Opening filename: ", szFileName)
	CLEAR_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	OPEN_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export: Started writing Race SCH file: ", sRaceFileName)
	
	// Write Race SCH file header.
	SAVE_STRING_TO_NAMED_DEBUG_FILE("// This file has been generated from its matching xml using Single Player Races widget", ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("// Do NOT edit this file by hand because your changes will likely get overwritten", ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export: Wrote Race SCH file header")
	
	// Write "ORR_Setup_RaceName" function (open).
	SAVE_STRING_TO_NAMED_DEBUG_FILE("PROC ORR_Setup_", ORR_Master.szSCHPath, szFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(sRaceFileName, ORR_Master.szSCHPath, szFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("(ORR_RACE_STRUCT& Race)", ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export: Wrote 'ORR_Setup_RaceName' function (open)")
	
	// Write "ORR_Race_Init" function call.
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	ORR_Race_Init(Race)", ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export: Wrote 'ORR_Race_Init' function call")
	
	// Write "Race.iGateCnt" assignment.
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Race.iGateCnt = ", ORR_Master.szSCHPath, szFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Race.iGateCnt, ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export: Wrote 'Race.iGateCnt' assignment")
	
	// Loop through race gates.
	REPEAT Race.iGateCnt iCnt
	
		// Write "ORR_Gate_Setup" function (open/close).
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	ORR_Gate_Setup(Race.sGate[", ORR_Master.szSCHPath, szFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iCnt, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("], <<", ORR_Master.szSCHPath, szFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.sGate[iCnt].vPos.x, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(",", ORR_Master.szSCHPath, szFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.sGate[iCnt].vPos.y, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(",", ORR_Master.szSCHPath, szFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.sGate[iCnt].vPos.z, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(">>, ", ORR_Master.szSCHPath, szFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.sGate[iCnt].fRadius, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", ORR_Master.szSCHPath, szFileName)
		szTempBuff = ORR_ChkpntType_GetString(Race.sGate[iCnt].eChkpntType)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(szTempBuff, ORR_Master.szSCHPath, szFileName)
//		SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", ORR_Master.szSCHPath, szFileName)
//		szTempBuff = ORR_StuntType_GetString(Race.sGate[iCnt].eStuntType)
//		SAVE_STRING_TO_NAMED_DEBUG_FILE(szTempBuff, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(")", ORR_Master.szSCHPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
		
		// Debug info.
		PRINTSTRING("ORR_Race_Export: Wrote 'ORR_Gate_Setup' function (open/close): ")
		PRINTINT(iCnt)
		PRINTNL()
		
	ENDREPEAT
	
	// Write a newline to space out gates and racers.
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	
	// Write "Race.iRacerCnt" assignment.
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Race.iRacerCnt = ", ORR_Master.szSCHPath, szFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Race.iRacerCnt, ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export: Wrote 'Race.iRacerCnt' assignment")
	
	// Loop through race racers.
	REPEAT Race.iRacerCnt iCnt
	
		// Write "ORR_Racer_Setup_Misc" function (open/close).
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	ORR_Racer_Setup_Misc(Race.Racer[", ORR_Master.szSCHPath, szFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iCnt, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("])", ORR_Master.szSCHPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
		
		// Write "ORR_Racer_Setup_Name" function (open/close).
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	ORR_Racer_Setup_Name(Race.Racer[", ORR_Master.szSCHPath, szFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iCnt, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("], \"", ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(Race.Racer[iCnt].szName, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\")", ORR_Master.szSCHPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
		
		// Write "ORR_Racer_Setup_Entities" function (open/close).
		// TODO: Forcing entity for player. May want to figure out a better way to do this...
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	ORR_Racer_Setup_Entities(Race.Racer[", ORR_Master.szSCHPath, szFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iCnt, ORR_Master.szSCHPath, szFileName)
		IF (Race.Racer[iCnt].eDriverType <= PEDTYPE_PLAYER_UNUSED)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("], PLAYER_PED_ID(), ", ORR_Master.szSCHPath, szFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("ORR_Master.PlayerVeh)", ORR_Master.szSCHPath, szFileName)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("], NULL, NULL)", ORR_Master.szSCHPath, szFileName)
		ENDIF
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
		
		// Write "ORR_Racer_Setup_Start" function (open/close).
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	ORR_Racer_Setup_Start(Race.Racer[", ORR_Master.szSCHPath, szFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iCnt, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("], <<", ORR_Master.szSCHPath, szFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.Racer[iCnt].vStartPos.x, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(",", ORR_Master.szSCHPath, szFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.Racer[iCnt].vStartPos.y, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(",", ORR_Master.szSCHPath, szFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.Racer[iCnt].vStartPos.z, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(">>, ", ORR_Master.szSCHPath, szFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Race.Racer[iCnt].fStartHead, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(")", ORR_Master.szSCHPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
		
		// Write "ORR_Racer_Setup_Types" function (open/close).
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	ORR_Racer_Setup_Types(Race.Racer[", ORR_Master.szSCHPath, szFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iCnt, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("], ", ORR_Master.szSCHPath, szFileName)
		szTempBuff = ORR_DriverType_GetString(Race.Racer[iCnt].eDriverType)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(szTempBuff, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", ORR_Master.szSCHPath, szFileName)
		szTempBuff = ORR_DriverModel_GetString(Race.Racer[iCnt].eDriverModel)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(szTempBuff, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", ORR_Master.szSCHPath, szFileName)
		szTempBuff = ORR_VehicleModel_GetString(Race.Racer[iCnt].eVehicleModel)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(szTempBuff, ORR_Master.szSCHPath, szFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(")", ORR_Master.szSCHPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
		
		// Debug info.
		PRINTSTRING("ORR_Race_Export: Wrote 'ORR_Racer_Setup' functions (open/close): ")
		PRINTINT(iCnt)
		PRINTNL()
		
	ENDREPEAT
	
	// Write "ORR_Setup_RaceName" function (close).
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDPROC", ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export: Wrote 'ORR_Setup_RaceName' function (close)")
	
	// Write Race SCH file footer.
	SAVE_STRING_TO_NAMED_DEBUG_FILE("// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!", ORR_Master.szSCHPath, szFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(ORR_Master.szSCHPath, szFileName)
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export: Wrote Race SCH file footer")
	
	// Close Race SCH file.
	CLOSE_DEBUG_FILE()
	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Export: Finished writing Race SCH file: ", sRaceFileName)
	
*/

	// Race successfully exported.
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Create SPR Race.
/// RETURNS:
///    TRUE if SPR Race was successfully created.
FUNC BOOL ORR_Race_Create(ORR_RACE_STRUCT& Race, STRING sRaceFileName)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Create")
	ORR_Race_Init(Race)
	IF ORR_Race_Save(Race, sRaceFileName)
		RETURN ORR_Race_Export(Race, sRaceFileName)
	ENDIF
	RETURN FALSE
ENDFUNC
#ENDIF



// -----------------------------------
// MAIN PROCS/FUNCTIONS
// -----------------------------------

/// PURPOSE:
///    Setup SPR Race.
/// RETURNS:
///    TRUE if still setting up SPR Race.
FUNC BOOL ORR_Race_Setup(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Setup")
	
	//B*1463231 - Setting the ambient vehicles to spawn
	#IF IS_DEBUG_BUILD
	#IF ORR_ENABLE_DEBUG_DRAW_TO_SCREEN_INFO
		DRAW_DEBUG_TEXT_2D("Set Veh Mult in Race setup = 0.2x", <<0.1,0.18,0>>)
	#ENDIF
	#ENDIF
	
	SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(0.2)
	
	// Setup SPR Race State Machine.
	SWITCH (Race.eSetup)
	
		// Setup (init).
		CASE ORR_RACE_SETUP_INIT
			ORR_Race_Racer_Request_All(Race)
		
			//CLEAR_PED_TASKS_IMMEDIATELY( PLAYER_PED_ID() )
			
			// We're retrying. We haven't failed. Allow blips
			bFailedNoBlips = FALSE
			
			SET_PLAYER_WANTED_LEVEL( PLAYER_ID(), 0 )
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_SETUP - INIT")
			
			// Disable cellphone.
			DISABLE_CELLPHONE(TRUE)

			REQUEST_STREAMED_TEXTURE_DICT("MPHUD")
			
			Race.uiLeaderboard = REQUEST_SC_LEADERBOARD_UI()			
			
			Race.bigMessageUI.siMovie = REQUEST_MG_BIG_MESSAGE()
			
			// Request the appropriate race textures.
			SWITCH (ORR_Master.eRaceType) 
				CASE ORR_RACE_TYPE_PLANE
					REQUEST_STREAMED_TEXTURE_DICT("SPRRaces")
				BREAK
				
				CASE ORR_RACE_TYPE_OFFROAD
					REQUEST_STREAMED_TEXTURE_DICT("SPROffroad")
				BREAK
				
				CASE ORR_RACE_TYPE_TRIATHLON
					REQUEST_STREAMED_TEXTURE_DICT("Triathlon")
				BREAK				
			ENDSWITCH
			
			IF ORR_Race_Racer_Stream_All(Race)
				ORR_Race_Racer_Create_All(Race)	
				
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_SETUP - LOAD_INIT")
				Race.eSetup = ORR_RACE_SETUP_LOAD_INIT
			ENDIF
		BREAK
		
		// Setup Load (init).
		CASE ORR_RACE_SETUP_LOAD_INIT

				// Request Countdown UI scaleform movie.
				REQUEST_MINIGAME_COUNTDOWN_UI(ORR_CountDownUI)
	//			ORR_COUNTDOWN_UI = REQUEST_SCALEFORM_MOVIE("COUNTDOWN")
				
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_SETUP - LOAD_WAIT")
				Race.eSetup = ORR_RACE_SETUP_LOAD_WAIT
		BREAK
		
		// Setup Load (wait).
		CASE ORR_RACE_SETUP_LOAD_WAIT

			
				/*
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPHUD")
				AND HAS_STREAMED_TEXTURE_DICT_LOADED("SPROffroad")
				AND HAS_STREAMED_TEXTURE_DICT_LOADED("SPRRaces")
					IF HAS_SCALEFORM_MOVIE_LOADED(ORR_COUNTDOWN_UI)
						CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_SETUP - CREATE_INIT")
						Race.eSetup = ORR_RACE_SETUP_CREATE_INIT
					ENDIF	
				ENDIF
				*/
				
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPHUD")
					IF HAS_SCALEFORM_MOVIE_LOADED(Race.uiLeaderboard)
						IF HAS_MINIGAME_COUNTDOWN_UI_LOADED(ORR_CountDownUI)
							// Check the appropriate race textures have loaded.
							IF HAS_STREAMED_TEXTURE_DICT_LOADED("SPROffroad")
								CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_SETUP - CREATE_INIT")
								Race.eSetup = ORR_RACE_SETUP_CREATE_INIT
							ENDIF
						ENDIF
					ENDIF
				ENDIF
		BREAK
		
		// Setup Create (init).
		CASE ORR_RACE_SETUP_CREATE_INIT
			//clear this so we dont set off fail checks immediately
			CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			IF ORR_Master.eRaceType = ORR_RACE_TYPE_OFFROAD
				//IF ORR_Main.eUpdate >= ORR_MAIN_RESETTING_LOAD
					CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_SETUP - CREATE_WAIT")
					Race.eSetup = ORR_RACE_SETUP_CREATE_WAIT
				//ENDIF
			ELSE
				ORR_Race_Racer_Create_All(Race)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_SETUP - CREATE_WAIT")
				Race.eSetup = ORR_RACE_SETUP_CREATE_WAIT
			ENDIF
		BREAK
		
		// Setup Create (wait).
		CASE ORR_RACE_SETUP_CREATE_WAIT	
			//ORR_RACE_TELL_AUDIENCE_TO_LOOK_AT_PLAYER()
			RETURN FALSE
		BREAK
		
		// Setup (wait).
		CASE ORR_RACE_SETUP_WAIT
			RETURN FALSE
		BREAK
		
		// Setup (cleanup).
		CASE ORR_RACE_SETUP_CLEANUP
			RETURN FALSE
		BREAK
		
	ENDSWITCH
	
	// Setup SPR Race still running.
	RETURN TRUE
	
ENDFUNC

PROC ORR_DISABLE_VEHICLE_CONTROLS()
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_AIM )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_ATTACK )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_ATTACK2 )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_BRAKE )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_CIN_CAM )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_CINEMATIC_LR )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_CINEMATIC_UD )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_EXIT )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_HANDBRAKE )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_DUCK )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_HEADLIGHT )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_JUMP )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_LOOK_LEFT )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_LOOK_RIGHT )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_MOVE_LR )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_MOVE_UD )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_PREV_RADIO )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_SPECIAL )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_SPECIAL_ABILITY_FRANKLIN )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_DETONATE )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_ATTACK )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_AIM )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_ACCURATE_AIM )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_SPECIAL )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO )
	
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_LOOK_LR )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_LOOK_UD )
ENDPROC

PROC ORR_START_RACE_AUDIO(ORR_RACE_STRUCT& Race)
	START_AUDIO_SCENE("OFFROAD_RACES_DURING_RACE")
	
	INT iIndex
	
	REPEAT Race.iRacerCnt iIndex
		IF iIndex <> 0
			IF DOES_ENTITY_EXIST( Race.Racer[iIndex].Vehicle )
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(Race.Racer[iIndex].Vehicle, "OFFROAD_RACES_DURING_RACE_GENERAL_VEHICLES")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC ORR_HANDLE_RACE_VEHICLE_AUDIO(ORR_RACE_STRUCT& Race)
	IF NOT IS_TIMER_STARTED(Race.tAudioTimer)
		START_TIMER_NOW(Race.tAudioTimer)
	ENDIF
	
	IF IS_ENTITY_DEAD( PLAYER_PED_ID() )
		EXIT
	ENDIF
	
	IF GET_TIMER_IN_SECONDS(Race.tAudioTimer) > 2.0
		FLOAT fClosestDist = 999999999.999
		FLOAT fCurDist = 0.0
		
		INT iIndex, iClosestIndex
		
		REPEAT Race.iRacerCnt iIndex
			IF iIndex <> 0
				IF NOT IS_ENTITY_DEAD( Race.Racer[iIndex].Vehicle )
					fCurDist = VDIST2( GET_ENTITY_COORDS( PLAYER_PED_ID() ), GET_ENTITY_COORDS( Race.Racer[iIndex].Vehicle ) )
					
					IF fCurDist < fClosestDist
						iClosestIndex = iIndex
						fClosestDist = fCurDist
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF DOES_ENTITY_EXIST( Race.viClosestVeh )
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(Race.viClosestVeh)
		ENDIF
		
		ADD_ENTITY_TO_AUDIO_MIX_GROUP( Race.Racer[iClosestIndex].Vehicle, "OFFROAD_RACES_DURING_RACE_CLOSEST_VEHICLE" )
		
		Race.viClosestVeh = Race.Racer[iClosestIndex].Vehicle
		
		RESTART_TIMER_NOW( Race.tAudioTimer )
	ENDIF
ENDPROC

/// PURPOSE:
///    Update SPR Race.
/// RETURNS:
///    TRUE if still updating SPR Race.
FUNC BOOL ORR_Race_Update(ORR_RACE_STRUCT& Race)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Update")
	INT iIndex
	// Update SPR Race State Machine.
	SWITCH (Race.eUpdate)
	
		// Update (init).
		CASE ORR_RACE_UPDATE_INIT
			
			SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
				CASE CanyonCliffs
					fATVSpeedMod = 0.85
					fBikeSpeedMod = 0.9
				BREAK
				CASE ValleyTrail
					fATVSpeedMod = 0.85
					fBikeSpeedMod = 0.9
				BREAK
				CASE LakesideSplash
					fATVSpeedMod = 0.85
					fBikeSpeedMod = 0.9
				BREAK
				CASE MinewardSpiral
					fATVSpeedMod = 0.85
					fBikeSpeedMod = 0.9
				BREAK
			ENDSWITCH
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_UPDATE - INIT")
			
			CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_PLAYED_STINGER)
			
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			stop_vehicle_recordings(Race)
			
			// Auto-pilot race racers.
			IF ORR_Master.eRaceType = ORR_RACE_TYPE_PLANE
				ORR_Race_Racer_AutoPilot(Race)
				IF NOT IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_FIRST_GATE_ACTIVATION)
					// Activate first/second race gates.
					ORR_Race_Gate_Activate(Race, 0, TRUE)
					ORR_Race_Gate_Activate(Race, 1, FALSE)
					SET_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_FIRST_GATE_ACTIVATION)
				ENDIF
			ENDIF
			
			IF Race.bRestarting
				OR Race.bCutsceneSkipped
				// Setup countdown display.			
				CANCEL_TIMER(Race.Display.tCnt)
				
				IF DOES_ENTITY_EXIST(Race.Racer[0].Vehicle)
				AND NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
					CPRINTLN(DEBUG_OR_RACES, "Resetting for song")
					SET_VEH_RADIO_STATION(Race.Racer[0].Vehicle, "RADIO_01_CLASS_ROCK") 
				ENDIF
				
				eCountdownStage = ORR_COUNTDOWN_STAGE_INIT
			ENDIF
			
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_UPDATE - COUNTDOWN")
			
			SET_PLAYER_CONTROL( PLAYER_ID(), TRUE )

			SET_VEHICLE_HANDBRAKE( Race.Racer[0].Vehicle, TRUE )
			ORR_DISABLE_VEHICLE_CONTROLS()
			

			
			REPEAT Race.iRacerCnt iIndex
				IF iIndex <> 0
					IF NOT IS_ENTITY_DEAD( Race.Racer[iIndex].Vehicle )
						SET_VEHICLE_USE_ALTERNATE_HANDLING( Race.Racer[iIndex].Vehicle, TRUE )
					ENDIF
				ENDIF
			ENDREPEAT
			Race.eUpdate = ORR_RACE_UPDATE_COUNTDOWN
		BREAK
		
		// Update Countdown.
		CASE ORR_RACE_UPDATE_COUNTDOWN	
			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
				FLOAT fSpeed
				fSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED( Race.Racer[0].Vehicle )
				
				IF (GET_ENTITY_MODEL( Race.Racer[0].Vehicle ) = BLAZER
				OR GET_ENTITY_MODEL( Race.Racer[0].Vehicle ) = BLAZER2
				OR GET_ENTITY_MODEL( Race.Racer[0].Vehicle ) = BLAZER3)
					SET_ENTITY_MAX_SPEED( Race.Racer[0].Vehicle, fSpeed * fATVSpeedMod )
				ELIF GET_ENTITY_MODEL( Race.Racer[0].Vehicle ) = SANCHEZ
					SET_ENTITY_MAX_SPEED( Race.Racer[0].Vehicle, fSpeed * fBikeSpeedMod )
				ENDIF
			ENDIF
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			
			// Display countdown until done, then start race.
			IF NOT ORR_Countdown_Display(Race.Display)
				STOP_SOUND(iIntroSFXIDX)
				STOP_AUDIO_SCENE("RACE_INTRO_GENERIC")
				
				ORR_START_RACE_AUDIO(Race)
				 //Once the race begins
				//UNFREEZE_RADIO_STATION("RADIO_01_CLASS_ROCK")
//				ORR_ENABLE_RADIO_ON_VEHICLE(Race.Racer[0].Vehicle, TRUE)
				
				// failsafe backup to switch to cutscene cam
								
				// clear one shot first checkpoints drawn bits
				IF IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_FIRST_GATE_ACTIVATION)
					CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_FIRST_GATE_ACTIVATION)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEHICLELOCK_UNLOCKED)
				ENDIF
				// Start race racers.
				DISPLAY_HUD(TRUE)
				ORR_Race_Racer_Start(Race)
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_UPDATE - WAIT")
				//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(DEFAULT, 10.0, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
				Race.eUpdate = ORR_RACE_UPDATE_WAIT
				
				RESTART_TIMER_NOW(ORR_Master.tmrCountdown)
			ELSE
				ORR_DISABLE_VEHICLE_CONTROLS()
			ENDIF
			
		BREAK
		
		// Update Finish.
		CASE ORR_RACE_UPDATE_FINISH	

			DISPLAY_HUD(FALSE)
			// show player controls after 1 second
			IF NOT IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_SCORECARD_INIT)
				SETTIMERA(0)
				ORR_Race_RankClock_FindPlayerBest( Race )
				
				ORR_Race_Leaderboard_Init_Scorecard(Race)
				INIT_SIMPLE_USE_CONTEXT(ORR_Master.uiInput, FALSE, FALSE, TRUE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_CONT2",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_UI_RETRY",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
				ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "HUD_INPUT68",		FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
				SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(ORR_Master.uiInput)
				SET_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_SCORECARD_INIT)
			ENDIF
					
			// manage pre-results camera pan
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP(TRUE)
			ENDIF
			IF IS_THIS_PRINT_BEING_DISPLAYED("SPR_RETR_FAIL")
				CLEAR_PRINTS()
			ENDIF
			
			CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_UPDATE_FINISH:  Running ORR_Race_Stunt_Manage_Finish_Camera")				
			ORR_Race_Stunt_Manage_Finish_Camera(Race, Race.Racer[0].Vehicle)
														
			IF DOES_CAM_EXIST(iSPRStuntFinishCam)
				IF NOT IS_CAM_ACTIVE(iSPRStuntFinishCam)
					SET_CAM_ACTIVE(iSPRStuntFinishCam, TRUE)
					IF IS_CINEMATIC_CAM_RENDERING()
						PRINTLN("cinematic cam is rendering")
					
					ENDIF
					
					PRINTLN("Trying to set camera active")
				ELSE
					RENDER_SCRIPT_CAMS(TRUE, FALSE, 0)
					
					SWITCH GET_CURRENT_PLAYER_PED_ENUM()
						CASE CHAR_MICHAEL
							ANIMPOSTFX_PLAY( "MinigameEndMichael", 500, FALSE )
						BREAK
						CASE CHAR_FRANKLIN
							ANIMPOSTFX_PLAY( "MinigameEndFranklin", 500, FALSE )
						BREAK
						CASE CHAR_TREVOR
							ANIMPOSTFX_PLAY( "MinigameEndTrevor", 500, FALSE )
						BREAK
					ENDSWITCH
					SHAKE_SCRIPT_GLOBAL( "HAND_SHAKE", 0.16 )
					CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_UPDATE - LEADERBOARD")
					SETTIMERA(0)
					ORR_KILL_HINT_CAM()
					ORR_SET_HINT_CAM_ACTIVE(FALSE)
					SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
					IF DOES_ENTITY_EXIST(Race.Racer[0].Vehicle) AND IS_VEHICLE_DRIVEABLE(Race.Racer[0].Vehicle)
						//BRING_VEHICLE_TO_HALT(Race.Racer[0].Vehicle, ORR_STOP_DISTANCE, 1)
						IF GET_IS_WAYPOINT_RECORDING_LOADED( tPlayerFinishWaypoint )
							SET_VEHICLE_IS_RACING( Race.Racer[0].Vehicle, FALSE )
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING( PLAYER_PED_ID(), Race.Racer[0].Vehicle, tPlayerFinishWaypoint, DRIVINGMODE_AVOIDCARS | DF_SteerAroundPeds, 0, EWAYPOINT_START_FROM_CLOSEST_POINT )
							CPRINTLN( DEBUG_OR_RACES, "Tasking to follow waypoint route." )
						ELSE
							CPRINTLN( DEBUG_OR_RACES, "Waypoint rec not loaded. Rec is ", tPlayerFinishWaypoint )
						ENDIF
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
					
					ENDIF
					ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
					SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
				
					IF Race.Racer[0].iRank <= 3
						PRINTLN("Race.Racer[0].iRank <= 3")
						IF ORR_IS_VEHICLE_BIKE(GET_ENTITY_MODEL(Race.Racer[0].Vehicle))
							PRINTLN("Player is in bike")
							//IF GET_RANDOM_BOOL()
								//TASK_PLAY_ANIM(PLAYER_PED_ID(), "MINI@RACING@BIKE@", "celebrate_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY)		
							//ELSE
								TASK_PLAY_ANIM(PLAYER_PED_ID(), "MINI@RACING@BIKE@", "celebrate_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY)	
							//ENDIF
						ELIF ORR_IS_VEHICLE_QUAD(GET_ENTITY_MODEL(Race.Racer[0].Vehicle))
							PRINTLN("Player is in quad")
							//IF GET_RANDOM_BOOL()
							//	PRINTLN("Playy anim 3")
							//	TASK_PLAY_ANIM(PLAYER_PED_ID(), "MINI@RACING@QUAD@", "celebrate_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY)		
							//ELSE
							//	PRINTLN("Playy anim 4") 
								TASK_PLAY_ANIM(PLAYER_PED_ID(), "MINI@RACING@QUAD@", "celebrate_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY)	
							//ENDIF
						ENDIF
					ENDIF
					
					STOP_OFFROAD_END_CHEER_AUDIO(Race)
					RELEASE_SCRIPT_AUDIO_BANK()
					
					RESTART_TIMER_NOW( ORR_Master.tmrFinishCut )
					IF IS_PLAYER_ONLINE()
						bIsJustOnline = TRUE
						ORR_LOAD_SOCIAL_CLUB_LEADERBOARD(INT_TO_ENUM(OFFROAD_RACE_INDEX,ORR_Master.iRaceCur), ORR_Master.szRaceFileName[ORR_Master.iRaceCur])
					ENDIF
					Race.eUpdate = ORR_RACE_UPDATE_LEADERBOARD_WAIT
				ENDIF
			ENDIF
			
		BREAK
		
		// Wait for the cam to be done
		CASE ORR_RACE_UPDATE_LEADERBOARD_WAIT
			IF GET_TIMER_IN_SECONDS( ORR_Master.tmrFinishCut ) >= 3.666
				// Finish race racers.
				ORR_Race_Racer_Finish(Race)
				STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
				SET_CAM_ACTIVE_WITH_INTERP(ORR_Master.camFinalLBD, iSPRStuntFinishCam, 666)
				PLAY_SOUND_FRONTEND(-1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
				ANIMPOSTFX_PLAY("MinigameTransitionIn", 1000, TRUE)
				
				//Set the ORR launcher to activate the next race blip
				SET_BITMASK(sLauncherData.iLauncherFlags,BIT18)
				//Autosave results
				MAKE_AUTOSAVE_REQUEST()
				
				Race.eUpdate = ORR_RACE_UPDATE_LEADERBOARD
			ELIF GET_TIMER_IN_SECONDS( ORR_Master.tmrFinishCut ) >= 2.0
				// If it's Triathlon, show player for a brief moment before showing the leaderboard.
				IF NOT IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_PLAYED_STINGER)
					MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
					IF NOT IS_MISSION_COMPLETE_PLAYING()
						SET_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_PLAYED_STINGER)
					ELSE	
						CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_UPDATE - Waiting on MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC")	
					ENDIF
					
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					IF IS_THIS_PRINT_BEING_DISPLAYED("SPR_RETR_FAIL")
						CLEAR_PRINTS()
					ENDIF
				ENDIF	
			ENDIF
		BREAK
		
		// Update Leaderboard.
		CASE ORR_RACE_UPDATE_LEADERBOARD
			#IF COMPILE_WIDGET_OUTPUT
				EXPORT_SCREEN_MEGA_PLACEMENT(Race.uiScorecard)
			#ENDIF
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_PLAYED_RANK_SOUND)
				IF Race.Racer[0].iRank > 1
					SET_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_PLAYED_RANK_SOUND)
					PLAY_SOUND_FRONTEND(-1, "RACE_PLACED", "HUD_AWARDS")
				ENDIF
			ENDIF
			
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				SET_MOUSE_CURSOR_THIS_FRAME()
			ENDIF

			Race.eRetVal = ORR_Race_Draw_Scorecard(Race)
			
			IF ORR_Master.sPlayerVehicleData.vehicleName <> DUMMY_MODEL_FOR_SCRIPT
				REQUEST_MODEL( ORR_Master.sPlayerVehicleData.vehicleName )
				IF NOT HAS_MODEL_LOADED( ORR_Master.sPlayerVehicleData.vehicleName )
					REQUEST_MODEL( ORR_Master.sPlayerVehicleData.vehicleName )
				ENDIF
			ENDIF
		
			// Wait for player to exit leaderboard.						
			IF (Race.eRetVal = ORV_TRUE)	
				CLEAR_RANK_REDICTION_DETAILS()
				ORR_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
				IF ORR_Master.iRaceCur = ENUM_TO_INT(OFFROAD_RACE_LAKESIDE_SPLASH)
					AND ORR_Master.bIPLActive
					// Replace tri IPLs that might not be active here
					IF NOT IS_IPL_ACTIVE("CS2_06_TriAf02")
						REQUEST_IPL("CS2_06_TriAf02")
					ENDIF
				ENDIF
				CLEAR_HELP()
				IF IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_SCORECARD_INIT)
					CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_SCORECARD_INIT)
				ENDIF
				
				// Clear the complex use contexts for user input for leaderboard and scoreboard.
				CLEANUP_SIMPLE_USE_CONTEXT( ORR_Master.uiInput )
							
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_UPDATE - CLEANUP")
				
				//RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
				
				IF Race.Racer[0].iRank = 1
					
					CPRINTLN( DEBUG_OR_RACES, ORR_Master.iBestRank )
					IF ORR_Master.iBestRank <> 1
						INCREMENT_PLAYER_PED_STAT( GET_CURRENT_PLAYER_PED_ENUM(), PS_DRIVING_ABILITY, 3 )
					ENDIF
				ENDIF
				
				IF ORR_Master.sPlayerVehicleData.vehicleName <> DUMMY_MODEL_FOR_SCRIPT
					IF HAS_MODEL_LOADED( ORR_Master.sPlayerVehicleData.vehicleName )
						OR NOT ORR_Master.bRecreateSavedVeh
					
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							VEHICLE_INDEX delVeh
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								delVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							ELSE
								delVeh = Race.Racer[0].Vehicle
							ENDIF
							IF ORR_Master.savedVeh <> delVeh
		//						SPECIAL_FUNCTION_DO_NOT_USE(PLAYER_PED_ID())
								//SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(delVeh) + <<2, 2, 0>>)
								SET_ENTITY_COORDS(PLAYER_PED_ID(), Race.vFinalSpawnPos)
								SET_ENTITY_HEADING( PLAYER_PED_ID(), Race.fFinalSpawnHeading )
								CPRINTLN(DEBUG_OR_RACES, "Detaching player from offroad race vehicle so we can delete it")
								SET_ENTITY_AS_MISSION_ENTITY(delVeh, TRUE, TRUE)
								
								//DELETE_VEHICLE(delVeh)
								CPRINTLN(DEBUG_OR_RACES, "[VEHICLE] Deleting race vehicle")
							ELSE
								IF DOES_ENTITY_EXIST( ORR_Master.savedVeh )
									CLEAR_PED_TASKS( PLAYER_PED_ID() )
									SET_ENTITY_COORDS( ORR_Master.savedVeh, Race.vFinalSpawnPos )
									SET_ENTITY_HEADING( ORR_Master.savedVeh, Race.fFinalSpawnHeading )
									SET_VEHICLE_ON_GROUND_PROPERLY( ORR_Master.savedVeh )
								ENDIF
								CPRINTLN(DEBUG_OR_RACES, "Race vehicle and player vehicle are the same!")
							ENDIF
							IF (DOES_ENTITY_EXIST(ORR_Master.savedVeh) AND IS_VEHICLE_DRIVEABLE(ORR_Master.savedVeh)
								AND ORR_Master.savedVeh <> delVeh)
								OR ORR_Master.bRecreateSavedVeh
								
								IF DOES_ENTITY_EXIST( Race.Racer[0].Vehicle )
									SET_ENTITY_AS_MISSION_ENTITY( Race.Racer[0].Vehicle, DEFAULT, TRUE )
									
									DELETE_VEHICLE( Race.Racer[0].Vehicle )
								ENDIF
								
								IF ORR_Master.bRecreateSavedVeh
									UPDATE_DYNAMIC_VEHICLE_GEN_POSITION(VEHGEN_MISSION_VEH, Race.vFinalPlayerVehPos, Race.fFinalPlayerVehHeading)
									//SET_MISSION_VEHICLE_GEN_VEHICLE(ORR_Master.savedVeh, Race.vFinalPlayerVehPos, Race.fFinalPlayerVehHeading)
									//DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MISSION_VEH)
									//ORR_Master.savedVeh = CREATE_VEHICLE( ORR_Master.sPlayerVehicleData.vehicleName, Race.sGate[Race.iGateCnt-1].vPos, Race.sGate[Race.iGateCnt-1].fHeading )
									//SET_VEHICLE_COLOUR_COMBINATION( ORR_Master.savedVeh, ORR_Master.sPlayerVehicleData.iVehicleColor )
									//SET_VEHICLE_SETUP( ORR_Master.savedVeh, ORR_Master.sPlayerVehicleData.sPlayerVehicle )
								ELSE
									SET_ENTITY_COLLISION(ORR_Master.savedVeh, TRUE)
									SET_ENTITY_COORDS(ORR_Master.savedVeh, Race.vFinalPlayerVehPos)
									SET_ENTITY_HEADING( ORR_Master.savedVeh, Race.fFinalPlayerVehHeading )
									SET_VEHICLE_ON_GROUND_PROPERLY(ORR_Master.savedVeh)
									SET_ENTITY_VISIBLE(ORR_Master.savedVeh, TRUE)
									CPRINTLN(DEBUG_OR_RACES, "Setting player vehicle down")
								ENDIF
								

//								IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), ORR_Master.savedVeh)
//									SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), ORR_Master.savedVeh)
//									CPRINTLN(DEBUG_OR_RACES, "Putting player inside his vehicle")
//								ENDIF

								CPRINTLN( DEBUG_OR_RACES, "Recreating saved vehicle..." )
							ELSE
								CPRINTLN(DEBUG_OR_RACES, "Vehicle doesn't exist??")
							ENDIF
						ENDIF
						
						IF IS_PED_WEARING_HELMET( PLAYER_PED_ID() )
							CPRINTLN( DEBUG_OR_RACES, "Player is wearing a helmet..." )
							REMOVE_PED_HELMET( PLAYER_PED_ID(), TRUE )
						ELSE
							CPRINTLN( DEBUG_OR_RACES, "Player has a prop on..." )
							CLEAR_PED_PROP( PLAYER_PED_ID(), ANCHOR_HEAD )
						ENDIF
						RESTORE_PLAYER_PED_VARIATIONS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
						SET_PED_VARIATIONS(PLAYER_PED_ID(), ORR_Master.tPedProps)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						
						STOP_SCRIPTED_CONVERSATION(FALSE)
						//ODDJOB_PLAY_SOUND("Phone_Generic_Key_02", iButtonSounds, FALSE)
						PLAY_SOUND_FRONTEND(-1, "YES", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						RESTART_TIMER_NOW(exitTimer)
						ORR_CREATE_OUTRO_CAMS(Race)
						SET_CAM_ACTIVE(cam1Wide, TRUE)
						ANIMPOSTFX_STOP( "MinigameTransitionIn" )
						ANIMPOSTFX_PLAY( "MinigameTransitionOut", 600, FALSE )
						ODDJOB_ENTER_CUTSCENE(DEFAULT, DEFAULT,FALSE)
						
						Race.eUpdate = ORR_RACE_UPDATE_OUTRO
					ENDIF
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), Race.vFinalSpawnPos)
					SET_ENTITY_HEADING( PLAYER_PED_ID(), Race.fFinalSpawnHeading )
					CPRINTLN(DEBUG_OR_RACES, "Detaching player from offroad race vehicle so we can delete it")
					SET_ENTITY_AS_MISSION_ENTITY(Race.Racer[0].Vehicle, TRUE, TRUE)
					
					IF NOT ORR_Master.bRecreateSavedVeh
						IF DOES_ENTITY_EXIST( Race.Racer[0].Vehicle )
							SET_ENTITY_COORDS( Race.Racer[0].Vehicle, Race.vFinalPlayerVehPos )
							SET_ENTITY_HEADING( Race.Racer[0].Vehicle, Race.fFinalSpawnHeading )
						ENDIF
					ELSE
						DELETE_VEHICLE( Race.Racer[0].Vehicle )
					ENDIF
					//DELETE_VEHICLE(delVeh)
					CPRINTLN(DEBUG_OR_RACES, "[VEHICLE] Deleting race vehicle")
					
					IF IS_PED_WEARING_HELMET( PLAYER_PED_ID() )
						CPRINTLN( DEBUG_OR_RACES, "Player is wearing a helmet..." )
						REMOVE_PED_HELMET( PLAYER_PED_ID(), TRUE )
					ELSE
						CPRINTLN( DEBUG_OR_RACES, "Player has a prop on..." )
						CLEAR_PED_PROP( PLAYER_PED_ID(), ANCHOR_HEAD )
					ENDIF
					
					RESTORE_PLAYER_PED_VARIATIONS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
					SET_PED_VARIATIONS(PLAYER_PED_ID(), ORR_Master.tPedProps)
					
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					
					STOP_SCRIPTED_CONVERSATION(FALSE)
					//ODDJOB_PLAY_SOUND("Phone_Generic_Key_02", iButtonSounds, FALSE)
					PLAY_SOUND_FRONTEND(-1, "YES", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
					RESTART_TIMER_NOW(exitTimer)
					ANIMPOSTFX_STOP( "MinigameTransitionIn" )
					ANIMPOSTFX_PLAY( "MinigameTransitionOut", 600, FALSE )
					ORR_CREATE_OUTRO_CAMS(Race)
					SET_CAM_ACTIVE(cam1Wide, TRUE)
					ODDJOB_ENTER_CUTSCENE(DEFAULT, DEFAULT, FALSE)
					
					Race.eUpdate = ORR_RACE_UPDATE_OUTRO				
				ENDIF
			ELIF Race.eRetVal = ORV_RESET
				CLEAR_RANK_REDICTION_DETAILS()
				ORR_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
				bRankPredictReady = FALSE
				bIsLBDContextSet = FALSE
				bOffroadProfileReady = FALSE
				
							
				IF IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_SCORECARD_INIT)
					CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_SCORECARD_INIT)
				ENDIF
				
				IF Race.Racer[0].iRank = 1

					IF ORR_Master.iBestRank <> 1
						INCREMENT_PLAYER_PED_STAT( GET_CURRENT_PLAYER_PED_ENUM(), PS_DRIVING_ABILITY, 3 )
					ENDIF
				ENDIF
				
				// Clear the complex use contexts for user input for leaderboard and scoreboard.
				CLEANUP_SIMPLE_USE_CONTEXT( ORR_Master.uiInput )
					
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				PLAY_SOUND_FRONTEND(-1, "YES", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				Race.eUpdate = ORR_RACE_UPDATE_OUTRO
				Race.Racer[0].iGateCur = 0		// Reset our gate position so the reset stuff runs too
				CANCEL_TIMER(Race.Display.tCnt)
				eBoostState = BS_READY
				Race.bBoostGoing = FALSE
				STOP_AUDIO_SCENE("OFFROAD_RACES_OUTRO_SCENE")
				CLEAR_PED_TASKS( PLAYER_PED_ID() )
				START_AUDIO_SCENE("RACE_INTRO_GENERIC")
				//ANIMPOSTFX_STOP( "MinigameTransitionIn" )
			ENDIF		
		BREAK
		
		CASE ORR_RACE_UPDATE_OUTRO
			IF ORR_NEW_DRAW_SCORECARD(Race)
				IF Race.eRetVal = ORV_TRUE
					PLAY_SOUND_FRONTEND(-1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
					SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
					SET_CAM_ACTIVE_WITH_INTERP(cam2wide, cam1wide, 1500, GRAPH_TYPE_LINEAR)
					RESTART_TIMER_NOW(exitTimer)
					Race.eUpdate = ORR_RACE_UPDATE_OUTRO_WAIT			
				ELIF Race.eRetVal = ORV_RESET
					Race.bFailChecking = FALSE
					Race.bRestarting = TRUE		// Let the game know we're restarting
					eCountdownStage = ORR_COUNTDOWN_STAGE_INIT
					Race.Racer[0].eReset = ORR_RACER_RESET_NO_FADE_OUT		// Set the player to reset fade out
					SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
					Race.eUpdate = ORR_RACE_UPDATE_WAIT	// Bring the player back to an updating game state
				ENDIF
//			IF GET_TIMER_IN_SECONDS( exitTimer) >= 1.5
				
			ENDIF
		BREAK
		
		CASE ORR_RACE_UPDATE_OUTRO_WAIT
			IF GET_TIMER_IN_SECONDS( exitTimer ) >= 1.5
			AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
					SET_CAM_ACTIVE(cam2wide, FALSE)
					SET_CAM_ACTIVE(cam3wide, TRUE)
					ORR_CREATE_OUTRO_SCENE( iOutroSynch )
					
					PLAY_SYNCHRONIZED_CAM_ANIM(cam3wide, iOutroSynch, "offroad_outro_cam", "MINI@RACING@QUAD@" )
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						SWITCH GET_CURRENT_PLAYER_PED_ENUM()
							CASE CHAR_MICHAEL
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iOutroSynch, "MINI@RACING@QUAD@", "offroad_outro_mic", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT)
							BREAK
							CASE CHAR_FRANKLIN
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iOutroSynch, "MINI@RACING@QUAD@", "offroad_outro_fra", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT)
							BREAK
							CASE CHAR_TREVOR
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iOutroSynch, "MINI@RACING@QUAD@", "offroad_outro_trv", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT)
							BREAK
						ENDSWITCH
					ENDIF
					
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iOutroSynch, FALSE)
					Race.eUpdate = ORR_RACE_UPDATE_OUTRO_SYNCH
					
				ELSE
					ODDJOB_EXIT_CUTSCENE(DEFAULT, DEFAULT, FALSE)
					Race.eUpdate = ORR_RACE_UPDATE_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE ORR_RACE_UPDATE_OUTRO_SYNCH
			
			IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
				IF GET_SYNCHRONIZED_SCENE_PHASE( iOutroSynch ) >= 1.0
					ODDJOB_EXIT_CUTSCENE(DEFAULT, DEFAULT, FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					CLEAR_PED_TASKS( PLAYER_PED_ID() )
					
					Race.eUpdate = ORR_RACE_UPDATE_CLEANUP
				ELIF (GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) < - DISABLED_INPUT_DEAD_ZONE
					OR GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) > DISABLED_INPUT_DEAD_ZONE
					OR GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < - DISABLED_INPUT_DEAD_ZONE
					OR GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > DISABLED_INPUT_DEAD_ZONE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 500)
					CPRINTLN( DEBUG_OR_RACES, "Exiting outro scene" )
					ODDJOB_EXIT_CUTSCENE(DEFAULT, DEFAULT, FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					Race.eUpdate = ORR_RACE_UPDATE_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		// Fade out right before cleaning and killing the script.
		CASE ORR_RACE_UPDATE_FADE_OUT_BEFORE_FINISH
		
			IF NOT IS_THREAD_ACTIVE(trackerThreadID) // checkin to see if stat tracker is still up for Tris
				//IF ORR_FadeOut_Safe(750)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						ENDIF
					ENDIF
					
					// Clear any minigames text.
					CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT, TRUE)
					
					// Restore the player's previous outfit.)
					//RESTORE_PLAYER_PED_VARIATIONS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
					
					IF Race.bUsingLoaner
						// Warp player nearby, to a safe location, once the race is completed.
						IF (ORR_Master.iRaceCur = ENUM_TO_INT(OFFROAD_RACE_CANYON_CLIFFS))
							IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
								SET_ENTITY_COORDS(Race.Racer[0].Driver, << 1761.1394, 3903.3284, 34.7834 >>)
								SET_ENTITY_HEADING(Race.Racer[0].Driver, 15.1770)
							ENDIF
						ELIF (ORR_Master.iRaceCur = ENUM_TO_INT(OFFROAD_RACE_RIDGE_RUN))
							IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
								SET_ENTITY_COORDS(Race.Racer[0].Driver, << -1340.3477, -1015.0042, 7.9419 >>)
								SET_ENTITY_HEADING(Race.Racer[0].Driver, 62.3626)
							ENDIF
						ELIF (ORR_Master.iRaceCur = ENUM_TO_INT(OFFROAD_RACE_VALLEY_TRAIL))
							IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
								SET_ENTITY_COORDS(Race.Racer[0].Driver, <<-2280.6921, 412.3260, 173.6019>>)
								SET_ENTITY_HEADING(Race.Racer[0].Driver, 183.1624)
								CPRINTLN(DEBUG_OR_RACES, "[ORR_Race.sch->ORR_Race_Update] CASE ORR_RACE_UPDATE_FADE_OUT_BEFORE_FINISH: Setting player heading to, ", GET_ENTITY_HEADING(Race.Racer[0].Driver))
							ELSE
								CPRINTLN(DEBUG_OR_RACES, "[ORR_Race.sch->ORR_Race_Update] CASE ORR_RACE_UPDATE_FADE_OUT_BEFORE_FINISH: Driver 0 is DEAD!?")
							ENDIF
						ENDIF
					ENDIF
					// Cleanup race.
					ORR_Race_Init(Race)
					
					RESTART_TIMER_NOW(taxiTime)
					Race.eUpdate = ORR_RACE_UPDATE_FADE_IN_BEFORE_FINISH
				//ENDIF
			ENDIF
		BREAK
		
		// Fade in after cleaning and killing the script.  Currently only used in Tri.
		CASE ORR_RACE_UPDATE_FADE_IN_BEFORE_FINISH
			// taxiTimer is not used in Tri, so instead of creating a new timer, just use taxiTimer instead.
			IF TIMER_DO_WHEN_READY(taxiTime, 0.5)
				CANCEL_TIMER(taxiTime)
				//ORR_FadeIn_Safe(750)
				Race.bRestarting = FALSE
				Race.eUpdate = ORR_RACE_UPDATE_CLEANUP
			ENDIF
		BREAK
		
		CASE ORR_RACE_WAIT_BEFORE_RESTART_CAMERA_PAN
			
			IF IS_CAM_INTERPOLATING(ORR_Master.camFinalLBD2)
//			IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				CPRINTLN(DEBUG_OR_RACES, "ORR_RACE_WAIT_BEFORE_RESTART_CAMERA_PAN : Moving to ORR_RACE_RESTART_CAMERA_PAN")
				ANIMPOSTFX_PLAY( "MinigameTransitionOut", 500, FALSE )
				ANIMPOSTFX_STOP( "MinigameTransitionIn" )
				Race.eUpdate = ORR_RACE_RESTART_CAMERA_PAN
			ELSE
				IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_CAM_COORD(ORR_Master.camFinalLBD), ORR_GET_STARTING_CAM_POS() + <<0, 0, 1000.0>>)//<<-1942.3542, 4444.5962, 90.0>>)
					CPRINTLN(DEBUG_OR_RACES, "ORR_RACE_WAIT_BEFORE_RESTART_CAMERA_PAN : Calling SET_CAM_COORD")
					SET_CAM_COORD(ORR_Master.camFinalLBD, ORR_GET_STARTING_CAM_POS() + <<0, 0, 1000.0>>)
					SET_CAM_ROT(ORR_Master.camFinalLBD, ORR_GET_STARTING_CAM_ROT())					
				ELSE
					CPRINTLN(DEBUG_OR_RACES, "ORR_RACE_WAIT_BEFORE_RESTART_CAMERA_PAN : Calling RENDER_SCRIPT_CAMS")
					IF DOES_CAM_EXIST(ORR_Master.camFinalLBD2)
						SET_CAM_ACTIVE_WITH_INTERP(ORR_Master.camFinalLBD2, ORR_Master.camFinalLBD, 500)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ORR_RACE_RESTART_CAMERA_PAN
			IF IS_FINISHED_DOING_RESTART_CAMERA_TRANSITION(Race)
				ORR_Master.bPlayerStarted = FALSE
				Race.bCutsceneSkipped = FALSE
				Race.eUpdate = ORR_RACE_UPDATE_INIT	// Bring the player back to an updating game state
			ENDIF
		BREAK
		
		// Update (wait).
		CASE ORR_RACE_UPDATE_WAIT
			IF NOT ORR_Master.bPlayerStarted
				AND NOT Race.bCutsceneSkipped
				IF eBoostState = BS_ACTIVE
				OR eBoostState = BS_FINISHED
					CPRINTLN( DEBUG_OR_RACES, "1ST PERSON FLASHES: Calling STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, 10.0). Game Timer = ", GET_GAME_TIMER())
					IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					AND eBoostState = BS_FINISHED
						SET_FIRST_PERSON_FLASH_EFFECT_VEHICLE_MODEL_HASH(ENUM_TO_INT(GET_ENTITY_MODEL(Race.Racer[0].Vehicle)))
					ENDIF
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, 10.0)
					ORR_Master.bPlayerStarted = TRUE
					
					//RENDER_SCRIPT_CAMS( FALSE, TRUE, 1 )
				ELIF IS_CONTROL_JUST_PRESSED( PLAYER_CONTROL, INPUT_VEH_ACCELERATE )
				OR IS_CONTROL_JUST_PRESSED( PLAYER_CONTROL, INPUT_VEH_BRAKE )
				OR IS_CONTROL_JUST_PRESSED( PLAYER_CONTROL, INPUT_LOOK_LR )
				OR IS_CONTROL_JUST_PRESSED( PLAYER_CONTROL, INPUT_LOOK_UD )
					DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
					CPRINTLN( DEBUG_OR_RACES, "1ST PERSON FLASHES: Disabling first person flash effect bc player input" )
					RENDER_SCRIPT_CAMS( FALSE, TRUE, 1300 )
					ORR_Master.bPlayerStarted = TRUE
					
					CPRINTLN( DEBUG_OR_RACES, "Render script cams to false" )
				ENDIF
			ELIF ORR_Master.bPlayerStarted
				AND NOT ORR_Master.bAudioStarted
				ORR_Master.bAudioStarted = TRUE
				ORR_HANDLE_RACE_VEHICLE_AUDIO(Race)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
				FLOAT fSpeed
				fSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED( Race.Racer[0].Vehicle )
				
				IF (GET_ENTITY_MODEL( Race.Racer[0].Vehicle ) = BLAZER
				OR GET_ENTITY_MODEL( Race.Racer[0].Vehicle ) = BLAZER2)
					SET_ENTITY_MAX_SPEED( Race.Racer[0].Vehicle, fSpeed * fATVSpeedMod )
				ELIF GET_ENTITY_MODEL( Race.Racer[0].Vehicle ) = SANCHEZ
					SET_ENTITY_MAX_SPEED( Race.Racer[0].Vehicle, fSpeed * fBikeSpeedMod )
				ENDIF
			ENDIF
			// need to draw it for a bit longer after the GO! first appears
			// think about adding a proper timer so we don't call this every frame afterwards
			IF GET_TIMER_IN_SECONDS(ORR_Master.tmrCountdown) <= 1.0
			AND eCountdownStage > ORR_COUNTDOWN_STAGE_INIT
				DRAW_SCALEFORM_MOVIE(ORR_CountDownUI.uiCountdown, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 100)
			ENDIF
			
			SET_PED_RESET_FLAG( PLAYER_PED_ID(), PRF_InRaceMode, TRUE )
			
			FLOAT fInterpTime, fSpeed
			
			// Interp initial speed to get a smooth launch
			REPEAT Race.iRacerCnt iIndex
				IF Race.Racer[iIndex].Driver <> PLAYER_PED_ID()
					AND NOT Race.Racer[iIndex].bStarted
					IF NOT IS_ENTITY_DEAD( Race.Racer[iIndex].Vehicle )
						IF NOT IS_TIMER_STARTED( Race.Racer[iIndex].startTimer )
							START_TIMER_NOW( Race.Racer[iIndex].startTimer )
						ELIF GET_TIMER_IN_SECONDS( Race.Racer[iIndex].startTimer ) > ORR_INIT_SPEED_INTERP_TIME
							CANCEL_TIMER( Race.Racer[iIndex].startTimer )
							Race.Racer[iIndex].bStarted = TRUE
						ELIF IS_VEHICLE_ON_ALL_WHEELS( Race.Racer[iIndex].Vehicle )
							fInterpTime = GET_TIMER_IN_SECONDS( Race.Racer[iIndex].startTimer ) / ORR_INIT_SPEED_INTERP_TIME
							
							fSpeed = INTERPOLATE_FLOAT(	Race.fStartBoostMin, Race.fStartBoostMax, fInterpTime )
							SET_VEHICLE_FORWARD_SPEED( Race.Racer[iIndex].Vehicle, fSpeed )
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_FIRST_GATE_ACTIVATION)
				// Activate first/second race gates.
				ORR_Race_Gate_Activate(Race, 0, TRUE)
				ORR_Race_Gate_Activate(Race, 1, FALSE)
				SET_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_FIRST_GATE_ACTIVATION)
				
				ORR_SET_HINT_CAM_ACTIVE(TRUE)
			ENDIF		
			IF NOT (ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON)
				
				//B*1518679 - Adding cheer sounds for offroad race at end
				IF ORR_Master.eRaceType = ORR_RACE_TYPE_OFFROAD
					PLAY_OFFROAD_CHEER_AUDIO_WHEN_PLAYER_IS_NEAR_FINISH_LINE(Race)
				ENDIF
				
				IF Race.bFailChecking //if we're checking it means we havent failed yet
					ORR_UPDATE_HINT_CAM(Race)
				ELSE
					KILL_RACE_HINT_CAM(localChaseHintCamStruct)
					SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
				ENDIF
			ENDIF
			
			// Update race racers and auto-restart race.
			IF NOT ORR_Race_Racer_Update(Race, ORR_Master.eRaceType)
				IF ORR_Master.iRaceCur >= 0 AND ORR_Master.iRaceCur < ENUM_TO_INT(NUM_OFFROAD_RACES)
					ORR_Master.iBestRank = ORR_Global_BestRank_Get( ORR_Master.iRaceCur )
				ENDIF
				
				// Setup finish display.
				CANCEL_TIMER(Race.Display.tCnt)
				
				STOP_AUDIO_SCENE("OFFROAD_RACES_DURING_RACE")
				START_AUDIO_SCENE("OFFROAD_RACES_OUTRO_SCENE")
				CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_UPDATE - FINISH")
				Race.eUpdate = ORR_RACE_UPDATE_FINISH
				CANCEL_TIMER(Race.tUpdate)
			ENDIF
			
			IF Race.bRestarting
				AND Race.Racer[0].eReset >= ORR_RACER_RESET_FADE_IN
				Race.eUpdate = ORR_RACE_UPDATE_COUNTDOWN
				Race.bRestarting = FALSE
			ENDIF
			
		BREAK
		
		// Update (cleanup).
		CASE ORR_RACE_UPDATE_CLEANUP
			
			STOP_AUDIO_SCENE("OFFROAD_RACES_OUTRO_SCENE")
			ENABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_LOOK_UD )
			ENABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_LOOK_LR )
			SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
			SET_PLAYER_CONTROL( PLAYER_ID(), TRUE )
			ENABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_EXIT )
			IF (ORR_Master.eRaceType = ORR_RACE_TYPE_PLANE)
				RETURN FALSE
			ELIF (ORR_Master.eRaceType = ORR_RACE_TYPE_OFFROAD)
				IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
					IF NOT IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_END_RETURN_TO_GAMPLAY_CAM)
//						RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
						//RENDER_SCRIPT_CAMS(FALSE, TRUE)
						IF NOT bQuitting
							AND NOT Race.bRestarting
							STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
						ENDIF
						SET_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_END_RETURN_TO_GAMPLAY_CAM)
					ELSE
						ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
						SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
						IF DOES_ENTITY_EXIST(Race.Racer[0].Vehicle)
							FREEZE_ENTITY_POSITION(Race.Racer[0].Vehicle, FALSE)
						ENDIF
						ORR_SET_HINT_CAM_ACTIVE(FALSE)
						
						CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_END_RETURN_TO_GAMPLAY_CAM)
						RETURN FALSE
					ENDIF
				ENDIF
				
			ELSE
				IF NOT IS_MESSAGE_BEING_DISPLAYED()				
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					RETURN FALSE					
				ELSE
					IF NOT IS_TIMER_STARTED(tShutdownTimer)
						START_TIMER_NOW_SAFE(tShutdownTimer)
					ELIF (GET_TIMER_IN_SECONDS(tShutdownTimer) > 5)	
						RETURN FALSE						
					ENDIF					
				ENDIF
			ENDIF
				
			IF IS_HUD_HIDDEN()
				CPRINTLN(DEBUG_OR_RACES, "ORR_RACE_UPDATE - ORR_ENABLE_RADIO_ON_VEHICLE")
				
				IF NOT IS_ENTITY_DEAD( Race.Racer[0].Vehicle )
					//SET_VEHICLE_RADIO_ENABLED(Race.Racer[0].Vehicle, TRUE)
				ENDIF
				DISPLAY_HUD(TRUE)
			ENDIF
			
			//B*1528358 - setting cash award to go through bank statement			
			AWARD_PLAYER_1ST_PLACE_CASH(Race)
		BREAK
		
	ENDSWITCH
	
//	
//	IF ORR_Master.iRaceCur = ENUM_TO_INT(MinewardSpiral)
//		SET_EXCLUSIVE_SCENARIO_GROUP("QUARRY")
//	ENDIF
	
	// Update SPR Race still running.
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Cleanup SPR Race.
PROC ORR_Race_Cleanup(ORR_RACE_STRUCT& Race, BOOL bDefault)
	TEXT_LABEL_63 rec = "offroadrace"
	SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
		CASE OFFROAD_RACE_CANYON_CLIFFS
			rec += 1
		BREAK
		CASE OFFROAD_RACE_RIDGE_RUN
			rec += 2
		BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			rec += 6
		BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL
			rec += 3
		BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			rec += 4
		BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY
			rec += 5
		BREAK
	ENDSWITCH
	rec += "car"
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Cleanup")
	IF bDefault
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_CLEANUP: Default bool hit, doing race init")
		// Init race data.
		ORR_Race_Init(Race)
	ELSE
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_RACE_CLEANUP: False bool hit, releasing offroad AI")
		// Release AI
		RELEASE_AI_RACERS(Race)
	ENDIF
	
	ORR_RELEASE_NEARBY_VEHICLES( Race )
	// Enable cellphone.
	DISABLE_CELLPHONE(FALSE)
	
	CLEAR_HELP()
	CLEAR_PRINTS()
	
	// Reseting this here, in case MG_UPDATE_FAIL_SPLASH_SCREEN failed to cleanup. (Very important this gets reset)
	SET_NO_LOADING_SCREEN(FALSE)

	IF ORR_Master.iRaceCur <> -1
		REMOVE_VEHICLE_RECORDING( 1, rec )
	ENDIF
	
	IF bDebugFailure
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Main_Debug: Debug Fail: before message")
		PRINT_NOW("SPR_MOVE_FAIL", 5000, 0)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Main_Debug: Debug Fail: after message")
		bDebugFailure = FALSE
	ENDIF
	// Cleanup countdown scaleform UI.
	//SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(ORR_COUNTDOWN_UI)
	
	//SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHUD")
	//SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("SRange")
	//SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("SPROffroad")
	//SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("SPRRaces")

	// TODO: Probably need more stuff in here...
	
ENDPROC




// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!
