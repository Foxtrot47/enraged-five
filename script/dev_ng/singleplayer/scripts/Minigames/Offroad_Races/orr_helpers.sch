// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	ORR_Helpers.sch
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Single Player Races - Helper procs/functions file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "event_public.sch"
USING "ORR_Head.sch"
USING "lineactivation.sch"	// Include this so it compiles in release, for using GET_OFFSET_FROM_COORD_IN_WORLD_COORDS
//USING "minigames_helpers.sch"

USING "script_camera.sch"
USING "rc_helper_functions.sch"

CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

STRING orrFailString = "SPR_UI_FAILD"
//STRING orrFailStrapline = "SPR_UI_FRETRY"

#IF NOT DEFINED(TASK_TRI_RACER_TO_FOLLOW_RACE_COURSE)
PROC TASK_TRI_RACER_TO_FOLLOW_RACE_COURSE(ORR_RACE_STRUCT& Race, VECTOR vCurrentGatePos, INT iRacerIndex)
	CPRINTLN(DEBUG_OR_RACES, "[spr_helpers.sch->TASK_TRI_RACER_TO_FOLLOW_RACE_COURSE] Dummy procedure called.")
	
	// Account for unreferenced variables.
	Race.iGateCnt 	= Race.iGateCnt
	vCurrentGatePos = vCurrentGatePos
	iRacerIndex 	= iRacerIndex
ENDPROC
#ENDIF

ENUM ORR_GENERAL_BITS
	ORR_SCORECARD_INIT 				= BIT0,
	ORR_SCORECARD_STREAMED 			= BIT1,
	ORR_PLAYED_STINGER 				= BIT2,
	ORR_FORCE_GATE_ACTIVATION 		= BIT3,
	ORR_FIRST_GATE_ACTIVATION 		= BIT4,
	ORR_PRINTED_IDLE_WARNING 		= BIT5,
	ORR_END_RETURN_TO_GAMPLAY_CAM 	= BIT6,
	ORR_END_HINT_CAM_ACTIVE 		= BIT7,
	ORR_AWARD_CASH 					= BIT8,
	ORR_PLAYED_RANK_SOUND			= BIT9
ENDENUM

ENUM ORR_COUNTDOWN_STAGE
	ORR_COUNTDOWN_STAGE_INIT,
	ORR_COUNTDOWN_RUN,
	ORR_COUNTDOWN_STAGE_WAIT
ENDENUM

/// PURPOSE: Bitfield enum for controlling help and objective prints
ENUM ORR_HELP_AND_OBJECTIVES
	ORR_DIST_HELP = BIT0,
	ORR_DAMG_HELP = BIT1,
	ORR_WARN_HELP = BIT2,	
	ORR_FAIL_HELP = BIT3,
	ORR_RETURN_WARN = BIT4,
	ORR_RETURN_FAIL = BIT5,
	ORR_RETURN_DES = BIT6,
	ORR_EXIT_WARN = BIT7,
	ORR_EXIT_FAIL = BIT8,
	ORR_EXIT_FAIL2 = BIT9,
	ORR_TXIT_WARN = BIT10,
	ORR_TXIT_FAIL = BIT11,
	ORR_MOVE_WARN = BIT12,
	ORR_MOVE_FAIL = BIT13,
	ORR_HELP_WANT = BIT14,
	ORR_RESET_HELP = BIT15,
	ORR_RESET_HELP1 = BIT16
ENDENUM

structTimer exitTimer
structTimer waterTimer
structTimer idleTimer
structTimer ExitVehicleTimer

BOOL bTriMounted = FALSE

INT iSPRGeneralBits
INT ORR_HELP_BIT
INT iExitVehicelBit
ORR_HINT_CAM_STRUCT ORR_HINT_CAM

ENUM ExitVehicleEnum
	vehicleExited = BIT0
ENDENUM

// INTS for use in determining vehicle settings when making offroad vehicles:
INT iVehColors, iMesaRoof

COUNTDOWN_UI					ORR_CountDownUI
ORR_COUNTDOWN_STAGE eCountdownStage = ORR_COUNTDOWN_STAGE_INIT
//structTimer vehRespawnTimeout
CONST_FLOAT ORR_RESPAWN_TIMEOUT	5.0

// -----------------------------------
// GLOBAL PROCS/FUNCTIONS
// -----------------------------------
FUNC BOOL ORR_DOES_RACE_REQUIRE_MOTORCYCLE()
	IF ORR_Master.eRaceType != ORR_RACE_TYPE_OFFROAD
		RETURN FALSE
	ENDIF
	
	SWITCH INT_TO_ENUM( OFFROAD_RACE_INDEX, ORR_Master.iRaceCur )
		CASE OFFROAD_RACE_CANYON_CLIFFS			RETURN TRUE		BREAK
		CASE OFFROAD_RACE_RIDGE_RUN				RETURN FALSE	BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL		RETURN TRUE		BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL			RETURN TRUE		BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH		RETURN TRUE		BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY			RETURN FALSE	BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC MODEL_NAMES ORR_GET_RANDOM_MOTORCYCLE_MODEL()
 	INT randInt = GET_RANDOM_INT_IN_RANGE(0, 10000) % 2
	SWITCH (randInt)
		CASE 0
			RETURN A_M_Y_MOTOX_01
		CASE 1
			//CPRINTLN( DEBUG_OR_RACES, "Returning A_M_Y_MOTOX_02" )
		//	RETURN A_M_Y_MOTOX_02
		DEFAULT
			CDEBUG1LN(DEBUG_OR_RACES, "WARNING! Random motorcycle model returned default value")
			RETURN A_M_Y_MOTOX_01
	ENDSWITCH
ENDFUNC

FUNC INT ORR_Global_BestRank_Get(INT iRace)
	INT iBestRank = 0
	iBestRank = g_savedGlobals.sOffroadData.iBestRank[iRace]

	RETURN iBestRank
ENDFUNC

PROC ORR_Global_BestRank_Set(INT iRace, INT iBestRank)
	g_savedGlobals.sOffroadData.iBestRank[iRace] = iBestRank

ENDPROC

FUNC FLOAT ORR_Global_BestTime_Get(INT iRace)
	FLOAT fBestTime
	fBestTime = g_savedGlobals.sOffroadData.fBestTime[iRace]
	RETURN fBestTime
ENDFUNC

PROC ORR_Global_BestTime_Set(INT iRace, FLOAT fBestTime)
	g_savedGlobals.sOffroadData.fBestTime[iRace] = fBestTime
	
ENDPROC

FUNC STRING ORR_GET_FAIL_STRAPLINE( ORR_FAIL_CASES eFail )
	STRING tRetVal
	
	SWITCH eFail
		CASE ORR_FAIL_IDLE
			tRetVal = "OFF_FAIL_IDLE"
		BREAK
		CASE ORR_FAIL_TOO_FAR
			tRetVal = "OFF_FAIL_DIST"
		BREAK
		CASE ORR_FAIL_WEAPON
			tRetVal = "OFF_FAIL_KILL"
		BREAK
		CASE ORR_FAIL_VEHICLE_DEATH
			tRetVal = "OFF_FAIL_VEH"
		BREAK
		CASE ORR_FAIL_VEHICLE_EXIT
			tRetVal = "OFF_FAIL_EXIT"
		BREAK
	ENDSWITCH
	
	RETURN tRetVal
ENDFUNC


// -----------------------------------
// DEBUG PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_Racer_DebugPrint(ORR_RACER_STRUCT& Racer)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Racer_DebugPrint")
	PRINTSTRING("DRIVER: ")
	PRINTINT(NATIVE_TO_INT(Racer.Driver))
	PRINTSTRING(" ")
	IF DOES_ENTITY_EXIST(Racer.Driver)
		PRINTSTRING("DOES EXIST & ")
		IF NOT IS_ENTITY_DEAD(Racer.Driver)
			PRINTSTRING("IS ALIVE")
		ELSE
			PRINTSTRING("ISN'T ALIVE")
		ENDIF
	ELSE
		PRINTSTRING("DOESN'T EXIST")
	ENDIF
	PRINTNL()
	PRINTSTRING("VEHICLE: ")
	PRINTINT(NATIVE_TO_INT(Racer.Vehicle))
	PRINTSTRING(" ")
	IF DOES_ENTITY_EXIST(Racer.Vehicle)
		PRINTSTRING("DOES EXIST & ")
		IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
			PRINTSTRING("IS ALIVE")
		ELSE
			PRINTSTRING("ISN'T ALIVE")
		ENDIF
	ELSE
		PRINTSTRING("DOESN'T EXIST")
	ENDIF
	PRINTNL()
ENDPROC

// -----------------------------------
// SCENE PROCS/FUNCTIONS
// -----------------------------------

FUNC BOOL ORR_FadeIn_Safe(INT iTime)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_FadeIn_Safe")
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(iTime)
	ELIF IS_SCREEN_FADED_IN()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ORR_FadeOut_Safe(INT iTime)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_FadeOut_Safe")
	IF IS_SCREEN_FADED_IN()
		DO_SCREEN_FADE_OUT(iTime)
	ELIF IS_SCREEN_FADED_OUT()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VECTOR ORR_GET_STARTING_CAM_POS()
	SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
		CASE OFFROAD_RACE_CANYON_CLIFFS
			RETURN <<-1942.3542, 4444.5962, 37.9733>>
		BREAK
		CASE OFFROAD_RACE_RIDGE_RUN
			RETURN <<-514.8898, 1994.2588, 207.3874>>
		BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			RETURN <<2995.1787, 2770.2710, 43.7110>>
		BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL
			RETURN <<-221.4457, 4223.8394, 45.9477>>
		BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			RETURN <<1598.8804, 3834.3931, 35.3646>>
		BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY
			RETURN <<2024.3276, 2136.7949, 94.9573>>
		BREAK
	ENDSWITCH
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC VECTOR ORR_GET_STARTING_CAM_ROT()
	SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
		CASE OFFROAD_RACE_CANYON_CLIFFS
			RETURN <<4.1808, 0.0, -106.0068>>
		BREAK
		CASE OFFROAD_RACE_RIDGE_RUN
			RETURN <<-6.8752, 0.0, 20.4662>>
		BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			RETURN <<0.4905, 0.0, -8.4486>>
		BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL
			RETURN <<-3.7549, 0.0, 78.2621>>
		BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			RETURN <<-4.6307, 0.0, -53.0313>>
		BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY
			RETURN <<-3.4761, 0.0, -111.7504>>
		BREAK
	ENDSWITCH
	RETURN <<0, 0, 0>>
ENDFUNC

// -----------------------------------
// MARKER PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_Blip_Destroy(BLIP_INDEX& Blip)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Blip_Destroy")
	IF DOES_BLIP_EXIST(Blip)
		REMOVE_BLIP(Blip)
	ENDIF
ENDPROC

FUNC BOOL ORR_Blip_Coord_Create(BLIP_INDEX& Blip, VECTOR vPos, BLIP_SPRITE eBlipSprite = RADAR_TRACE_INVALID, FLOAT fScale = 1.0, INT iCurGate = -1, INT iNumGates = -1, ORR_RACE_STUNT_GATE_ENUM eStuntType = ORR_RACE_STUNT_GATE_NORMAL)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Blip_Coord_Create")
	ORR_Blip_Destroy(Blip)
	Blip = ADD_BLIP_FOR_COORD(vPos)
	IF NOT DOES_BLIP_EXIST(Blip)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Blip_Coord_Create: Failed to create blip!")
		RETURN FALSE
	ENDIF
	IF (eBlipSprite <> RADAR_TRACE_INVALID)
		SET_BLIP_SPRITE(Blip, eBlipSprite)
	ENDIF
	SET_BLIP_SCALE(Blip, fScale)
	SET_BLIP_DISPLAY(Blip, DISPLAY_BOTH)
	// set knife gate blips to GREEN
	IF (eStuntType = ORR_RACE_STUNT_GATE_SIDE_LEFT) OR (eStuntType = ORR_RACE_STUNT_GATE_SIDE_RIGHT)
		SET_BLIP_COLOUR(Blip, BLIP_COLOUR_GREEN)
	ENDIF
	// set inverted gate blips to BLUE
	IF (eStuntType = ORR_RACE_STUNT_GATE_INVERTED)
		SET_BLIP_COLOUR(Blip, BLIP_COLOUR_BLUE)
	ENDIF
	IF iCurGate != -1	
	AND iNumGates != -1
		BEGIN_TEXT_COMMAND_SET_BLIP_NAME("GATEBLIP")
			ADD_TEXT_COMPONENT_INTEGER(iCurGate)
			ADD_TEXT_COMPONENT_INTEGER(iNumGates)
		END_TEXT_COMMAND_SET_BLIP_NAME(Blip)		
	ELSE		
		SET_BLIP_NAME_FROM_TEXT_FILE(Blip, "GATEBLIPDEF")		
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL ORR_Blip_Entity_Create(BLIP_INDEX& Blip, ENTITY_INDEX Entity, BOOL bFriendly = TRUE, FLOAT fScale = 1.0)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Blip_Entity_Create")
	ORR_Blip_Destroy(Blip)
	IF IS_ENTITY_DEAD(Entity)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Blip_Entity_Create: Entity is not alive!")
		RETURN FALSE
	ENDIF
	Blip = ADD_BLIP_FOR_ENTITY(Entity)
	IF NOT DOES_BLIP_EXIST(Blip)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Blip_Entity_Create: Failed to create blip!")
		RETURN FALSE
	ENDIF
	SET_BLIP_AS_FRIENDLY(Blip, bFriendly)
	SET_BLIP_SCALE(Blip, fScale)	
	RETURN TRUE
ENDFUNC

PROC ORR_Chkpnt_Destroy(CHECKPOINT_INDEX& Chkpnt)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Chkpnt_Destroy")
	IF (Chkpnt <> NULL)
		DELETE_CHECKPOINT(Chkpnt)
		Chkpnt = NULL
	ENDIF
ENDPROC

PROC ORR_Chkpnt_BeginFade(CHECKPOINT_INDEX& ChkpntToFade, CHECKPOINT_INDEX& FadeStorage, INT& iAlpha, BOOL bIsLastGate)
	ORR_Chkpnt_Destroy(FadeStorage)
	
	IF (ChkpntToFade <> NULL)
		FadeStorage = ChkpntToFade
		ChkpntToFade = NULL
		iAlpha = 255
		IF bIsLastGate
			iAlpha = 0
		ENDIF
		SET_CHECKPOINT_RGBA(FadeStorage, 255, 255, 255, iAlpha)
		SET_CHECKPOINT_RGBA2(FadeStorage, 255, 255, 255, iAlpha)
	ENDIF
ENDPROC

FUNC CHECKPOINT_TYPE ORR_Get_ChnkPnt_Type_From_ORR_Type(ORR_RACE_CHECKPOINT_TYPE eChkpntType, ORR_RACE_STUNT_GATE_ENUM eStuntType)	
	SWITCH eChkpntType
		CASE ORR_CHKPT_OFFROAD_DEFAULT
			RETURN CHECKPOINT_RACE_GROUND_CHEVRON_1
		BREAK	
		CASE ORR_CHKPT_OFFROAD_FINISH			
			RETURN CHECKPOINT_RACE_GROUND_FLAG
		BREAK
		
		CASE ORR_CHKPT_STUNT_DEFAULT			
			RETURN CHECKPOINT_PLANE_FLAT
		BREAK
		
		CASE ORR_CHKPT_STUNT_STUNT		
			If eStuntType = ORR_RACE_STUNT_GATE_SIDE_LEFT
				RETURN CHECKPOINT_PLANE_SIDE_L
			ELIF eStuntType = ORR_RACE_STUNT_GATE_SIDE_RIGHT
				RETURN CHECKPOINT_PLANE_SIDE_R
			ELIF eStuntType = ORR_RACE_STUNT_GATE_INVERTED
				RETURN CHECKPOINT_PLANE_INVERTED
			ENDIF
		BREAK
		
		CASE ORR_CHKPT_STUNT_FINISH			
			RETURN CHECKPOINT_RACE_AIR_FLAG
		BREAK
		
		CASE ORR_CHKPT_TRI_SWIM
			RETURN CHECKPOINT_RACE_TRI_SWIM_CHEVRON_1
		BREAK
		
		CASE ORR_CHKPT_TRI_BIKE
			RETURN CHECKPOINT_RACE_TRI_CYCLE_CHEVRON_1
		BREAK
		
		CASE ORR_CHKPT_TRI_RUN
			RETURN CHECKPOINT_RACE_TRI_RUN_CHEVRON_1
		BREAK
		
		CASE ORR_CHKPT_TRI_FINISH
			RETURN CHECKPOINT_RACE_TRI_RUN_FLAG
			//RETURN CHECKPOINT_RACE_AIR_FLAG	// Ideally, the top half of this checkpoint visually works as a finish line.
		BREAK
		
		CASE ORR_CHKPT_TRI_FIRST_TRANS		
			RETURN CHECKPOINT_RACE_TRI_SWIM_FLAG
		BREAK
		
		CASE ORR_CHKPT_TRI_SECOND_TRANS		
			RETURN CHECKPOINT_RACE_TRI_CYCLE_FLAG 
		BREAK
	ENDSWITCH
	RETURN CHECKPOINT_RACE_GROUND_CHEVRON_1
ENDFUNC

// Copied from ORR_Gate.sch
FUNC BOOL ORR_Helpers_Check_Condition_Flag(ORR_GATE_STRUCT& sGate, ENUM_TO_INT iFlag)	
	RETURN IS_BITMASK_AS_ENUM_SET(sGate.iGateFlags, iFlag)
ENDFUNC


//******************************************************************************
//******************************************************************************
// Hint Cam Stuff
//******************************************************************************
//******************************************************************************

PROC ORR_INIT_HINT_CAM()
	ORR_HINT_CAM.bActive = FALSE
//	ORR_HINT_CAM.Entity = NULL
	ORR_HINT_CAM.Coord = <<0,0,0>>
//	ORR_HINT_CAM.HintType = 
	ORR_HINT_CAM.CustomCam = NULL
ENDPROC

PROC ORR_SET_HINT_CAM_ACTIVE(BOOL bActive)
	ORR_HINT_CAM.bActive = bActive
	SET_CINEMATIC_BUTTON_ACTIVE(!bActive)
ENDPROC

FUNC BOOL IS_POINT_TOO_CLOSE_TO_PLAYER(VECTOR vPos)
	FLOAT fPlayerSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
	
	IF VDIST2(vPos, GET_PLAYER_COORDS(PLAYER_ID())) < (150.0 + fPlayerSpeed * fPlayerSpeed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_NEXT_GATE_POS(ORR_RACE_STRUCT& Race)
	INT iNextGate
	iNextGate = Race.Racer[0].iGateCur + 1
	IF iNextGate < Race.iGateCnt
		RETURN Race.sGate[iNextGate].vPos
	ELSE
		RETURN <<0,0,0>>
	ENDIF
ENDFUNC

PROC ORR_SET_HINT_CAM_COORD(VECTOR vThisPos, VECTOR vNextPos)
	//KILL_RACE_HINT_CAM(localChaseHintCamStruct)
	
	If IS_POINT_TOO_CLOSE_TO_PLAYER(vThisPos)
		PRINTLN("This point is too close: ", vThisPos, " Setting to next instead: ", vNextPos)
		ORR_HINT_CAM.Coord = vNextPos
	ELSE
		PRINTLN("not too close: ", vThisPos)
		ORR_HINT_CAM.Coord = vThisPos
	ENDIF
ENDPROC

PROC ORR_CLEAR_HINT_CAM_TARGET()
	ORR_HINT_CAM.Coord = <<0,0,0>>
ENDPROC

PROC ORR_KILL_HINT_CAM()
	ORR_INIT_HINT_CAM()
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
ENDPROC

FUNC BOOL ORR_IS_HINT_CAM_BUTTON_PRESSED()
	RETURN IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)// OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)//IS_BUTTON_PRESSED(PAD1, CIRCLE)
ENDFUNC


PROC ORR_UPDATE_HINT_CAM(ORR_RACE_STRUCT& Race)

	IF NOT ORR_HINT_CAM.bActive
		IF IS_POINT_TOO_CLOSE_TO_PLAYER(ORR_HINT_CAM.Coord)
			ORR_HINT_CAM.Coord = GET_NEXT_GATE_POS(Race)
		ENDIF
	ENDIF
	
	IF ORR_IS_HINT_CAM_BUTTON_PRESSED()
		ORR_HINT_CAM.bActive = TRUE
//		CPRINTLN(DEBUG_OR_RACES, "TURNING ON HINT CAM")
	ENDIF
	
	IF ORR_GET_HINT_CAM_TIMER(ORR_HINT_CAM) >= ORR_HINT_CAM_TIMER_THRESHOLD AND IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		ORR_HINT_CAM.bActive = FALSE
//		CPRINTLN(DEBUG_OR_RACES, "TURNING OFF HINT CAM")
	ENDIF
	
	IF ORR_HINT_CAM.bActive AND ORR_IS_HINT_CAM_BUTTON_PRESSED()
		ORR_INCREASE_HINT_CAM_TIMER(ORR_HINT_CAM)
	ELSE
		ORR_RESET_HINT_CAM_TIMER(ORR_HINT_CAM)
	ENDIF
	
	IF ORR_HINT_CAM.bActive
		IF NOT IS_VECTOR_ZERO(ORR_HINT_CAM.Coord) AND NOT IS_POINT_TOO_CLOSE_TO_PLAYER(ORR_HINT_CAM.Coord)
			CONTROL_RACE_HINT_CAM(localChaseHintCamStruct, ORR_HINT_CAM.Coord)
		ELSE
			IF DOES_CAM_EXIST(ORR_HINT_CAM.CustomCam)
				DESTROY_CAM(ORR_HINT_CAM.CustomCam)
			ENDIF
			
			KILL_RACE_HINT_CAM(localChaseHintCamStruct)
			ORR_HINT_CAM.bActive = FALSE
			CDEBUG1LN(DEBUG_OR_RACES, "no coord to point at")
		ENDIF
	ELSE	
		IF DOES_CAM_EXIST(ORR_HINT_CAM.CustomCam)
			DESTROY_CAM(ORR_HINT_CAM.CustomCam)
		ENDIF
			
		KILL_RACE_HINT_CAM(localChaseHintCamStruct)
		ORR_HINT_CAM.bActive = FALSE
		EXIT
	ENDIF
	
ENDPROC

PROC HANDLE_START_BOOST(ORR_RACER_STRUCT & Racer)
      SWITCH eBoostState
            CASE BS_READY
                  IF iBoostTimer > 0
						IF GET_GAME_TIMER() < iBoostTimer
							IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
								CPRINTLN(DEBUG_MISSION,"Player triggered boost")
								IF IS_VEHICLE_OK(Racer.Vehicle) AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), Racer.Vehicle)
									IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(Racer.Vehicle))
										SET_VEHICLE_BOOST_ACTIVE(Racer.Vehicle, TRUE)
										CPRINTLN( DEBUG_OR_RACES, "Applying boost to player in car." )
									ENDIF
									ANIMPOSTFX_PLAY("RaceTurbo", 0, FALSE)
									CPRINTLN( DEBUG_OR_RACES, "Applying boost to player with postfx." )
									//SET_VEHICLE_FORWARD_SPEED(sPlayerVehicle.vehPlayerVehicle, GET_VEHICLE_ESTIMATED_MAX_SPEED(sPlayerVehicle.vehPlayerVehicle))
									iBoostTimer = GET_GAME_TIMER() + BOOST_TIME
									eBoostState = BS_ACTIVE
									DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
									CPRINTLN( DEBUG_OR_RACES, "1ST PERSON FLASHES: Disabling first person flash effect bc player input" )
								ENDIF
                          	ENDIF
                        ELSE
                              CPRINTLN(DEBUG_MISSION,"Boost window timed out")
                              eBoostState = BS_FINISHED
                        ENDIF
                  ENDIF
            BREAK
            
            CASE BS_ACTIVE
                  IF GET_GAME_TIMER() > iBoostTimer
                        CPRINTLN(DEBUG_MISSION,"Boost finished")
                        IF IS_VEHICLE_OK(Racer.Vehicle) AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), Racer.Vehicle)
                              IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(Racer.Vehicle))
                                    SET_VEHICLE_BOOST_ACTIVE(Racer.Vehicle, FALSE)
                              ENDIF
                              eBoostState = BS_FINISHED
                        ENDIF
                  ELSE
                        IF IS_VEHICLE_OK(Racer.Vehicle) AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), Racer.Vehicle)
                              APPLY_FORCE_TO_ENTITY(Racer.Vehicle, APPLY_TYPE_FORCE, <<0.0, 50.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
                        ENDIF
                  ENDIF
            BREAK
      ENDSWITCH
ENDPROC

PROC SET_SCALEFORM_OFFROAD_FAIL_MESSAGE(SCRIPT_SCALEFORM_BIG_MESSAGE& scaleformStruct)
	STRING sMethodName = "TRANSITION_UP"
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SHOW_CENTERED_MP_MESSAGE_LARGE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_RED)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(orrFailString)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(ORR_GET_FAIL_STRAPLINE(ORR_Master.eFailReason))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.15)
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = 1500
ENDPROC

/// PURPOSE: Works out which checkpoint type is needed based upon angle between checkpoints 
FUNC CHECKPOINT_TYPE GET_CHEVRON_CHECKPOINT_TYPE(ORR_RACE_CHECKPOINT_TYPE sprType, VECTOR pos3, VECTOR pos, VECTOR pos2)
	VECTOR  vec1, vec2

	FLOAT  	fReturnAngle
	FLOAT	fChevron1 = 180.0
	FLOAT  	fChevron2 = 140.0
	FLOAT  	fChevron3 = 80.0
	CHECKPOINT_TYPE Chevron1, Chevron2, Chevron3
	
	IF sprType = ORR_CHKPT_OFFROAD_DEFAULT
		Chevron1 = CHECKPOINT_RACE_GROUND_CHEVRON_1
		Chevron2 = CHECKPOINT_RACE_GROUND_CHEVRON_2
		Chevron3 = CHECKPOINT_RACE_GROUND_CHEVRON_3
	ELIF sprType = ORR_CHKPT_STUNT_DEFAULT
		Chevron1 = CHECKPOINT_RACE_AIR_CHEVRON_1
		Chevron2 = CHECKPOINT_RACE_AIR_CHEVRON_2
		Chevron3 = CHECKPOINT_RACE_AIR_CHEVRON_3
	ELIF sprType = ORR_CHKPT_TRI_SWIM
		Chevron1 = CHECKPOINT_RACE_TRI_SWIM_CHEVRON_1
		Chevron2 = CHECKPOINT_RACE_TRI_SWIM_CHEVRON_2
		Chevron3 = CHECKPOINT_RACE_TRI_SWIM_CHEVRON_3
	ELIF sprType = ORR_CHKPT_TRI_BIKE
		Chevron1 = CHECKPOINT_RACE_TRI_CYCLE_CHEVRON_1
		Chevron2 = CHECKPOINT_RACE_TRI_CYCLE_CHEVRON_2
		Chevron3 = CHECKPOINT_RACE_TRI_CYCLE_CHEVRON_3
	ELIF sprType = ORR_CHKPT_TRI_RUN
		Chevron1 = CHECKPOINT_RACE_TRI_RUN_CHEVRON_1
		Chevron2 = CHECKPOINT_RACE_TRI_RUN_CHEVRON_2
		Chevron3 = CHECKPOINT_RACE_TRI_RUN_CHEVRON_3
	ENDIF

	IF NOT ARE_VECTORS_EQUAL(pos3, <<0,0,0>>)//if not first checkpoint
		

		vec1 = pos3 - pos
		vec2 = pos2 - pos
		 
		fReturnAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vec1.x, vec1.y, vec2.x, vec2.y)
		
		IF fReturnAngle > 180
			fReturnAngle = (360.0 - fReturnAngle)
		ENDIF
		
		IF fReturnAngle < fChevron3
			RETURN Chevron3
	   	ELIF fReturnAngle < fChevron2	
			RETURN Chevron2
		ELIF fReturnAngle < fChevron1
			RETURN Chevron1
		ENDIF
	ENDIF
	
	RETURN Chevron1	 
ENDFUNC

//FUNC BOOL ORR_Chkpnt_Create(CHECKPOINT_INDEX& Chkpnt, ORR_RACE_CHECKPOINT_TYPE ChkpntType, VECTOR vPos, VECTOR vPointAt, FLOAT fScale = ORR_GATE_CHKPNT_SCL)
FUNC BOOL ORR_Chkpnt_Create(VECTOR vPrevGate, ORR_GATE_STRUCT& sGate, VECTOR vPointAt, FLOAT fScale = ORR_GATE_CHKPNT_SCL)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Chkpnt_Create")
	VECTOR vGatePos = sGate.vPos
	CHECKPOINT_TYPE curCheckpointType
	ORR_Chkpnt_Destroy(sGate.Chkpnt) // Not sure whether this should be ORR_Chkpnt_BeginFade
	
	INT iRed, iGreen, iBlue, iAlpha
	IF sGate.eChkpntType != ORR_CHKPT_STUNT_DEFAULT
	AND sGate.eChkpntType != ORR_CHKPT_STUNT_STUNT
	AND sGate.eChkpntType != ORR_CHKPT_STUNT_FINISH
	AND sGate.eChkpntType != ORR_CHKPT_TRI_SWIM
	AND sGate.eChkpntType != ORR_CHKPT_OFFROAD_DEFAULT
	AND NOT ORR_Helpers_Check_Condition_Flag(sGate, ORR_RACE_GATE_FLAG_DO_NOT_SNAP_TO_GROUND)
		FLOAT fGroundZ
		//IF NOT GET_GROUND_Z_FOR_3D_COORD(sGate.vPos, fGroundZ)					
		//	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Chkpnt_Create: using default position, probe failed")
		//ELSE	
		fGroundZ = sGate.vPos.z
		FLOAT fHeightMod = 0.65
		vGatePos.z = fGroundZ + (fHeightMod*fScale) //2.6m is arbitrary...using it from my tunings in drag races - SM			
		//ENDIF		
	ENDIF
	fScale = CHECKPOINT_VISUAL_SIZE * CHECKPOINT_VISUAL_SIZE_MODIFIER
	
	// custom checkpoint offset for offroad races
	IF sGate.eChkpntType = ORR_CHKPT_OFFROAD_DEFAULT
		IF NOT ORR_Helpers_Check_Condition_Flag(sGate, ORR_RACE_GATE_FLAG_DO_NOT_SNAP_TO_GROUND)
			FLOAT fGroundZ
			//IF NOT GET_GROUND_Z_FOR_3D_COORD(sGate.vPos, fGroundZ)					
				//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Chkpnt_Create: using default position, probe failed")
			//	RETURN FALSE
			//ELSE
			// raises the arrow in relation to the ground
			fGroundZ = sGate.vPos.z
			
			FLOAT fHeightMod = 0.65
			vGatePos.z = fGroundZ + (fHeightMod*fScale) //2.6m is arbitrary...using it from my tunings in drag races - SM			
			//ENDIF

			curCheckpointType = GET_CHEVRON_CHECKPOINT_TYPE(sGate.eChkpntType, vPrevGate, vGatePos, vPointAt)
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(MG_GET_RACE_CHECKPOINT_DEFAULT_COLOUR()), iRed, iGreen, iBlue, iAlpha)
			sGate.Chkpnt = CREATE_CHECKPOINT(curCheckpointType, vGatePos, vPointAt, fScale, iRed, iGreen, iBlue, MG_GET_CHECKPOINT_ALPHA(vGatePos, 220, 255))
			GET_HUD_COLOUR(HUD_COLOUR_NORTH_BLUE, iRed, iGreen, iBlue, iAlpha)
			SET_CHECKPOINT_RGBA2(sGate.Chkpnt, iRed, iGreen, iBlue, MG_GET_CHECKPOINT_ALPHA(vGatePos, 70, 210))
			
			// lowers the cylinder in relation to the arrow checkpoint
			SET_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(sGate.Chkpnt, 0.95)
			SET_CHECKPOINT_CYLINDER_HEIGHT( sGate.Chkpnt, 4.0, 4.0, 100 )
		ENDIF	
	ELSE
		sGate.Chkpnt = CREATE_CHECKPOINT(ORR_Get_ChnkPnt_Type_From_ORR_Type(sGate.eChkpntType, sGate.eStuntType), vGatePos, vPointAt, fScale, 254, 207, 12, MG_GET_CHECKPOINT_ALPHA(vGatePos, 220, 255))
		GET_HUD_COLOUR(HUD_COLOUR_NORTH_BLUE, iRed, iGreen, iBlue, iAlpha)
		SET_CHECKPOINT_RGBA2(sGate.Chkpnt, iRed, iGreen, iBlue, MG_GET_CHECKPOINT_ALPHA(vGatePos, 70, 210))
		SET_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(sGate.Chkpnt, 0.95)
		SET_CHECKPOINT_CYLINDER_HEIGHT( sGate.Chkpnt, 4.0, 4.0, 100 )
	ENDIF	
	
	IF (sGate.Chkpnt = NULL)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Chkpnt_Create: Failed to create checkpoint!")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC Remove_Reset_UI()
//	REMOVE_MINIGAME_INSTRUCTION(ORR_Master.uiInput, ICON_UP)
ENDPROC

//PROC Display_Reset_UI(ORR_RACE_STRUCT& Race, ORR_RACER_STRUCT& Racer, BOOL bRacerIsPlayer)
//	// Setup Scaleform UI for reset/restart.
//	
//	// SHOULDN'T BE DOING THIS. YOU'RE SETTING UP AND UPDATING EVERY FRAME.
//	// SETUP ONCE, UPDATE EVERY FRAME.
//	//SETUP_MINIGAME_INSTRUCTIONS(
//	//	ORR_Master.uiInput, ICON_UP, "", 1,
//	//	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "SPR_UI_QUIT",
//	//	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "SPR_UI_RESTART",
//	//	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "SPR_UI_RESET")
//	INIT_SIMPLE_USE_CONTEXT(ORR_Master.uiInput, FALSE, FALSE, TRUE, TRUE)
//	ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_UI_RESET",	FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//	ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_UI_QUIT",		FRONTEND_CONTROL, INPUT_FRONTEND_Y)
//	ADD_SIMPLE_USE_CONTEXT_INPUT(ORR_Master.uiInput, "SPR_UI_RESTART",	FRONTEND_CONTROL, INPUT_FRONTEND_X)
//	
//	// If racer is player, display reset/restart options.
//	IF bRacerIsPlayer
//		
//		SWITCH (UPDATE_SIMPLE_USE_CONTEXT(ORR_Master.uiInput))
//			// Reset vehicle.
//			CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//				Remove_Reset_UI(Race)
//				Racer.eReset = ORR_RACER_RESET_FADE_OUT
//			BREAK
//			// Restart race.
//			CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X)
//				Race.bRestarting = TRUE
//				Remove_Reset_UI(Race)
//				Racer.eReset = ORR_RACER_RESET_FADE_OUT		
//			BREAK
//			CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
//				Race.bRestarting = FALSE						
//				Remove_Reset_UI(Race)
//				
//				IF (ORR_Master.eRaceType = ORR_RACE_TYPE_OFFROAD)									
//					ORR_Master.iRaceCur = -1 //forcing a quit here as there is no menu for offroad races
//					Racer.eReset = ORR_RACER_QUIT_FADE_IN
//				ELSE
//					CDEBUG1LN(DEBUG_OR_RACES, "Quitting from rest UI, going to RACER_QUIT_FADE_OUT")
//					Racer.eReset = ORR_RACER_QUIT_FADE_OUT
//				ENDIF
//			BREAK
//		ENDSWITCH
//	ENDIF	
//ENDPROC



PROC ORR_RESET_DO_VEHICLE_CHECKS(ORR_RACE_STRUCT& Race)
	INT iVehHealth, iEngHealth
	iVehHealth = GET_ENTITY_HEALTH(Race.Racer[0].Vehicle)
	
	IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
		iEngHealth = ROUND(GET_VEHICLE_ENGINE_HEALTH(Race.Racer[0].Vehicle))
	ENDIF
	
	// health check
	IF iVehHealth < 500.0
		IF (ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON)
			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
				AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), Race.Racer[0].Vehicle)
					bTriMounted = TRUE
				ENDIF
			ENDIF
			IF bTriMounted
				IF (Race.sGate[Race.Racer[0].iGateCur].eChkpntType = ORR_CHKPT_TRI_BIKE)  OR  (Race.sGate[Race.Racer[0].iGateCur].eChkpntType = ORR_CHKPT_TRI_SECOND_TRANS)
					IF NOT IS_MESSAGE_BEING_DISPLAYED()						
						PRINT_NOW_ONCE("SPR_HELP_DAMG", 5000, 0, ORR_HELP_BIT, ORR_DAMG_HELP)			
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// nuke all existing messages and conversations before displaying vehicle health prompt
			IF IS_MESSAGE_BEING_DISPLAYED()				
				CLEAR_PRINTS()	
			ENDIF
			PRINT_NOW_ONCE("SPR_HELP_DAMG", 5000, 0, ORR_HELP_BIT, ORR_DAMG_HELP)
		ENDIF
	ENDIF
	IF (ORR_Master.eRaceType = ORR_RACE_TYPE_OFFROAD)
		IF iEngHealth < 200.0
			CDEBUG1LN(DEBUG_OR_RACES, "Player Vehicle Engine health is too low, prompt to reset")
			// nuke all existing messages and conversations before displaying vehicle health prompt
			IF IS_MESSAGE_BEING_DISPLAYED()				
				CLEAR_PRINTS()	
			ENDIF
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
			ENDIF
			PRINT_NOW_ONCE("SPR_HELP_DAMG", 5000, 0, ORR_HELP_BIT, ORR_DAMG_HELP)			
		ENDIF
	ENDIF
ENDPROC


PROC ORR_RESET_DO_DISTANCE_TO_RACE_LINE_CHECKS(ORR_RACE_STRUCT& Race)
	VECTOR vCurGatePos, vPrevGatePos
	vCurGatePos = Race.sGate[Race.Racer[0].iGateCur].vPos
	IF (Race.Racer[0].iGateCur > 0)			
		vPrevGatePos = Race.sGate[Race.Racer[0].iGateCur-1].vPos
	ELSE		
		vPrevGatePos = ORR_Master.vDefRcrPos
	ENDIF
	
	// Check distance between race start and first gate
	
//	FLOAT fWarnDist = GET_DISTANCE_BETWEEN_COORDS(vCurGatePos, vPrevGatePos)
//	FLOAT fFailDist = fWarnDist
//	
//	SWITCH ORR_Master.eRaceType
//		CASE ORR_RACE_TYPE_OFFROAD
//			IF (Race.Racer[0].iGateCur = 0) //first gate
//				fWarnDist += 10.0
//				fFailDist += 75.0
//			ELSE
//				fWarnDist += 50.0
//				fFailDist += 75.0
//			ENDIF
//		BREAK
//	ENDSWITCH

	FLOAT fWarnDist = 75.0
				
	VECTOR vPlayerPosition = GET_ENTITY_COORDS(Race.Racer[0].Driver)		
	VECTOR vNearestPos = GET_CLOSEST_POINT_ON_LINE(vPlayerPosition, vPrevGatePos, vCurGatePos)				
	FLOAT fPlayerDistance = GET_DISTANCE_BETWEEN_COORDS(vNearestPos, vPlayerPosition)
	BOOL bWarningDist = (fPlayerDistance >= fWarnDist)
	//BOOL bFailureDist = (fPlayerDistance >= fFailDist)
	IF (ORR_Master.eRaceType = ORR_RACE_TYPE_OFFROAD)
		IF ABSF(vNearestPos.z-vPlayerPosition.z) > 15.0 //291946 - Canyon Cliff race – Not informed on how to reposition on falling off cliff
			bWarningDist = TRUE
		ENDIF
	ENDIF
	
	IF bWarningDist
		IF NOT IS_TIMER_STARTED(ORR_Master.tmrFail)
			START_TIMER_NOW(ORR_Master.tmrFail)
		ENDIF
		
		IF NOT Race.bPlayerRespawning
			AND NOT ORR_Master.bRespawnPressed
			AND GET_TIMER_IN_SECONDS(ORR_Master.tmrFail) >= ORR_FAIL_TIME
			CPRINTLN(DEBUG_OR_RACES, "Player has gone too far from race, going to FAIL OVER")
			RESTART_TIMER_NOW(exitTimer)
			Race.bFailChecking = FALSE
			CPRINTLN(DEBUG_OR_RACES, "RESET_TUTORIAL:  Race.bFailChecking set to FALSE")
			Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
			Race.ePlayerRespawnState = ORR_RESPAWN_SET
			CLEAR_PRINTS()
			CLEAR_HELP()
			SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
			ORR_Master.eFailReason = ORR_FAIL_TOO_FAR
			
			MG_INIT_FAIL_SPLASH_SCREEN(ORR_Master.splashState)
			//SET_SCALEFORM_OFFROAD_FAIL_MESSAGE(Race.bigMessageUI)
					
		ELSE
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_HELP_DIST_2") AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_HELP_DIST")
				ORR_Master.bDistFailHelpActive = TRUE
				PRINT_HELP_FOREVER("SPR_HELP_DIST")
			ENDIF
				//ENDIF
	//			ENDIF
			//IF fPlayerDistance <= (fFailDist - 15) //make sure we're a good distance away so we dont stack msgs
			PRINT_NOW_ONCE("SPR_HELP_WARN", 20000, 0, ORR_HELP_BIT, ORR_WARN_HELP)
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(ORR_Master.tmrFail)
			CANCEL_TIMER(ORR_Master.tmrFail)
		ENDIF
		
		CLEAR_THIS_PRINT("SPR_HELP_WARN")
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_HELP_DIST")
			AND ORR_Master.bDistFailHelpActive
			AND NOT ORR_Master.bWaterFailHelpActive
			ORR_Master.bDistFailHelpActive = FALSE
			CLEAR_HELP()
		ELIF ORR_Master.bDistFailHelpActive
			ORR_Master.bDistFailHelpActive = FALSE
		ENDIF
		IF IS_BITMASK_AS_ENUM_SET(ORR_HELP_BIT, ORR_WARN_HELP)
			CLEAR_BITMASK_AS_ENUM(ORR_HELP_BIT, ORR_WARN_HELP)
			CDEBUG1LN(DEBUG_OR_RACES, "RESET TUTORIAL: Clearing global WARN DIST BIT")
		ENDIF
	ENDIF
	
	//if we want to see where the intersection is in the world
//	#IF IS_DEBUG_BUILD
//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//	DRAW_DEBUG_SPHERE(vNearestPos, 1.0)				
//	#ENDIF	
ENDPROC

//	----------------------------------
//	RESET TUTORIAL // FAIL CASE
//	---------------------------------- 

PROC RESET_TUTORIAL(ORR_RACE_STRUCT& Race)
	IF Race.bFailChecking				
		ORR_RESET_DO_DISTANCE_TO_RACE_LINE_CHECKS(Race)
		ORR_RESET_DO_VEHICLE_CHECKS(Race)		
	ENDIF
ENDPROC

// EXIT VEHICLE FAILURE
PROC EXIT_VEHICLE_FAILURE(ORR_RACE_STRUCT& Race, BLIP_INDEX& returnToVehBlip)
	// OFFROAD
	IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)
	AND NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
		IF NOT IS_PED_IN_VEHICLE(Race.Racer[0].Driver, Race.Racer[0].Vehicle)
			IF NOT IS_ENTITY_IN_WATER(Race.Racer[0].Vehicle)
				//IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
					// add blip for vehicle
					IF NOT DOES_BLIP_EXIST(returnToVehBlip)
						returnToVehBlip = ADD_BLIP_FOR_ENTITY(Race.Racer[0].Vehicle)
						SET_BLIP_COLOUR(returnToVehBlip, BLIP_COLOUR_BLUE)
								
						IF (Race.Racer[0].iGateCur <= Race.iGateCnt-1)
							ORR_Blip_Destroy(Race.sGate[Race.Racer[0].iGateCur].Blip)
							ORR_Chkpnt_Destroy(Race.sGate[Race.Racer[0].iGateCur].Chkpnt)												
						ENDIF
						IF (Race.Racer[0].iGateCur <= Race.iGateCnt-2)
							ORR_Blip_Destroy(Race.sGate[Race.Racer[0].iGateCur+1].Blip)
							ORR_Chkpnt_Destroy(Race.sGate[Race.Racer[0].iGateCur+1].Chkpnt)
						ENDIF						
					ENDIF
					IF NOT IS_TIMER_STARTED(ExitVehicleTimer)
						START_TIMER_NOW_SAFE(ExitVehicleTimer)
						CDEBUG1LN(DEBUG_OR_RACES, "Started EXIT VEHICLE TIMER")
					ELSE
						IF (GET_TIMER_IN_SECONDS(ExitVehicleTimer) >= 1.0)
							// display timer flip to ON here
							ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
							IF NOT IS_BITMASK_AS_ENUM_SET(ORR_HELP_BIT, ORR_EXIT_WARN)
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
									PRINT_NOW_ONCE("SPR_EXIT_WARN", 10000, 0, ORR_HELP_BIT, ORR_EXIT_WARN)		
									SET_BITMASK_AS_ENUM(iExitVehicelBit, vehicleExited)
									CDEBUG1LN(DEBUG_OR_RACES, "EXIT_VEHICLE_FAILURE: Bit set for vehicle exit message")
								ENDIF
							ENDIF
						ENDIF
						IF (GET_TIMER_IN_SECONDS(ExitVehicleTimer) >= 3.0)
							IF NOT IS_BITMASK_AS_ENUM_SET(ORR_HELP_BIT, ORR_RESET_HELP)
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_HELP_DIST_2") AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_HELP_DIST")
									ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS( TRUE )
									PRINT_HELP_ONCE("SPR_HELP_DIST_2", ORR_HELP_BIT, ORR_RESET_HELP)
								ENDIF
								CDEBUG1LN(DEBUG_OR_RACES, "EXIT_VEHICLE_FAILURE: Bit set for reset help message")
							ENDIF
						ENDIF
						IF (GET_TIMER_IN_SECONDS(ExitVehicleTimer) >= 35.0)
							// display timer flip to OFF here
							CANCEL_TIMER(ExitVehicleTimer)
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_IDLE_WARN")
								CPRINTLN( DEBUG_OR_RACES, "CLEARING HELP TEXT IN ORR_HELPERS.SCH LINE 899" )
								CLEAR_HELP()
							ENDIF
							CDEBUG1LN(DEBUG_OR_RACES, "Failed to get back into vehicle")
							RESTART_TIMER_NOW(exitTimer)
							IF DOES_BLIP_EXIST(returnToVehBlip)
								REMOVE_BLIP(returnToVehBlip)
								IF (Race.Racer[0].iGateCur <= Race.iGateCnt-1)
									IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur].blip)
										SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur].blip, DISPLAY_BOTH)
									ENDIF
								ENDIF
								IF ((Race.Racer[0].iGateCur+1) <= (Race.iGateCnt-2))
									IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur+1].blip)
										SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur+1].blip, DISPLAY_BOTH)
									ENDIF
								ENDIF
							ENDIF
							Race.bFailChecking = FALSE
							CPRINTLN( DEBUG_OR_RACES, "CLEARING HELP TEXT IN ORR_HELPERS.SCH LINE 918" )
							CLEAR_PRINTS()
							CLEAR_HELP()
							SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
							ORR_Master.eFailReason = ORR_FAIL_VEHICLE_EXIT
							
							MG_INIT_FAIL_SPLASH_SCREEN(ORR_Master.splashState)
							//SET_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI, orrFailString, orrFailStrapline, 4000, HUD_COLOUR_RED, MG_BIG_MESSAGE_CENTERED)
							CDEBUG1LN(DEBUG_OR_RACES, "EXIT_VEHICLE_FAILURE:  Race.bFailChecking set to FALSE")
							Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
						ENDIF	
					ENDIF	
				//ENDIF
			ENDIF
		ELSE
			// display timer flip to OFF here
			CANCEL_TIMER(ExitVehicleTimer)
			IF DOES_BLIP_EXIST(returnToVehBlip)
				REMOVE_BLIP(returnToVehBlip)					
				IF NOT IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_FORCE_GATE_ACTIVATION)						
					SET_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_FORCE_GATE_ACTIVATION)
				ENDIF		
			ENDIF				
			CANCEL_TIMER(ExitVehicleTimer)
//			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_IDLE_WARN")
//				CPRINTLN( DEBUG_OR_RACES, "CLEARING HELP TEXT IN ORR_HELPERS LINE 939" )
//				CLEAR_HELP()
//			ENDIF
			IF IS_THIS_PRINT_BEING_DISPLAYED("SPR_EXIT_WARN")
				CLEAR_THIS_PRINT("SPR_EXIT_WARN")
			ENDIF
			IF IS_THIS_PRINT_BEING_DISPLAYED("SPR_EXIT_FAIL")
				CLEAR_THIS_PRINT("SPR_EXIT_FAIL")
			ENDIF
			IF IS_BITMASK_AS_ENUM_SET(ORR_HELP_BIT, ORR_EXIT_WARN)
				CLEAR_BITMASK_AS_ENUM(ORR_HELP_BIT, ORR_EXIT_WARN)
				CDEBUG1LN(DEBUG_OR_RACES, "EXIT_VEHICLE: CLEARED EXIT WARNING BIT")
			ENDIF
			IF IS_BITMASK_AS_ENUM_SET(ORR_HELP_BIT, ORR_RESET_HELP)
				CLEAR_BITMASK_AS_ENUM(ORR_HELP_BIT, ORR_RESET_HELP)
				CDEBUG1LN(DEBUG_OR_RACES, "EXIT_VEHICLE: CLEARED RESET HELP BIT")
			ENDIF
			
			IF IS_BITMASK_AS_ENUM_SET(ORR_HELP_BIT, ORR_EXIT_FAIL)
				CLEAR_BITMASK_AS_ENUM(ORR_HELP_BIT, ORR_EXIT_FAIL)
			ENDIF
		ENDIF
		// Other_vehicle_failure
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), Race.Racer[0].Vehicle)
			IF IS_THIS_PRINT_BEING_DISPLAYED("SPR_EXIT_WARN")
				CLEAR_THIS_PRINT("SPR_EXIT_WARN")
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "Player entered a vehicle that is NOT their race vehicle")
			CANCEL_TIMER(ExitVehicleTimer)
			RESTART_TIMER_NOW(exitTimer)
			IF DOES_BLIP_EXIST(returnToVehBlip)
				REMOVE_BLIP(returnToVehBlip)
				IF (Race.Racer[0].iGateCur <= Race.iGateCnt-1)
					IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur].blip)
						SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur].blip, DISPLAY_NOTHING)
					ENDIF
				ENDIF
				IF ((Race.Racer[0].iGateCur+1) <= (Race.iGateCnt-2))						
					IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur+1].blip)
						SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur+1].blip, DISPLAY_NOTHING)
					ENDIF
				ENDIF
			ENDIF
			Race.bFailChecking = FALSE
			CLEAR_PRINTS()
			CLEAR_HELP()
			SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
			ORR_Master.eFailReason = ORR_FAIL_VEHICLE_EXIT
			
			MG_INIT_FAIL_SPLASH_SCREEN(ORR_Master.splashState)
			//SET_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI, orrFailString, orrFailStrapline, 4000, HUD_COLOUR_RED, MG_BIG_MESSAGE_CENTERED)
			CDEBUG1LN(DEBUG_OR_RACES, "EXIT_VEHICLE_FAILURE.2:  Race.bFailChecking set to FALSE")
			Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
		ENDIF
	ELSE 
		IF IS_THIS_PRINT_BEING_DISPLAYED("SPR_EXIT_WARN")
			CLEAR_PRINTS()
		ENDIF
	ENDIF
ENDPROC

// DEAD VEHICLE FAILURE
PROC Dead_Race_Vehicle(ORR_RACE_STRUCT& Race, BLIP_INDEX& returnToVehBlip)
	IF Race.bFailChecking
		IF (ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON) 
		AND (Race.sGate[Race.Racer[0].iGateCur].eChkpntType != ORR_CHKPT_TRI_BIKE) AND (Race.sGate[Race.Racer[0].iGateCur].eChkpntType != ORR_CHKPT_TRI_SECOND_TRANS)
			EXIT	
		ENDIF
		
		IF NOT IS_VEHICLE_DRIVEABLE(Race.Racer[0].Vehicle)//(GET_ENTITY_HEALTH(Race.Racer[0].Vehicle) < 5)
			// Set failcase to false
			RESTART_TIMER_NOW(exitTimer)
			IF DOES_BLIP_EXIST(returnToVehBlip)
				REMOVE_BLIP(returnToVehBlip)
			ENDIF	
			IF (Race.Racer[0].iGateCur <= Race.iGateCnt-1)
				IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur].blip)
					SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur].blip, DISPLAY_NOTHING)
				ENDIF	
			ENDIF
			IF ((Race.Racer[0].iGateCur+1) <= (Race.iGateCnt-2))
				IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur+1].blip)
					SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur+1].blip, DISPLAY_NOTHING)
				ENDIF
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "Dead_Race_Vehicle:  Race.bFailChecking set to FALSE")
			Race.bFailChecking = FALSE
			CLEAR_PRINTS()
			CLEAR_HELP()
			SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
			ORR_Master.eFailReason = ORR_FAIL_VEHICLE_DEATH
			
			MG_INIT_FAIL_SPLASH_SCREEN(ORR_Master.splashState)
			//SET_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI, orrFailString, orrFailStrapline, 4000, HUD_COLOUR_RED, MG_BIG_MESSAGE_CENTERED)
			Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if player vehicle has entered water, and reset player accordingly.
PROC waterFailure(ORR_RACE_STRUCT& Race)
	IF NOT IS_ENTITY_DEAD(Race.Racer[0].Vehicle)					
		IF IS_ENTITY_IN_WATER(Race.Racer[0].Vehicle)				
			IF NOT IS_TIMER_STARTED(waterTimer)
				START_TIMER_NOW_SAFE(waterTimer)
				CPRINTLN(DEBUG_OR_RACES, "Started water Timer")
			ELSE			
				IF IS_PED_IN_VEHICLE(Race.Racer[0].Driver, Race.Racer[0].Vehicle)
					IF (IS_VEHICLE_MODEL(Race.Racer[0].Vehicle, SANCHEZ))
						IF IS_VEHICLE_ON_ALL_WHEELS(Race.Racer[0].Vehicle)
							IF IS_VEHICLE_STUCK_TIMER_UP(Race.Racer[0].Vehicle, VEH_STUCK_JAMMED, 1500) OR (GET_TIMER_IN_SECONDS(waterTimer) >= 4.0)
								RESTART_TIMER_NOW(waterTimer)
								RESTART_TIMER_NOW(exitTimer)
								CDEBUG1LN(DEBUG_OR_RACES, "waterFailure:  Race.bFailChecking set to FALSE")
								//Race.bFailChecking = FALSE
								//Race.Racer[0].eReset = ORR_RACER_RESET_INIT	
								//Race.ePlayerRespawnState = ORR_RESPAWN_SET
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_HELP_DIST")
									PRINT_HELP_FOREVER("SPR_HELP_DIST")
									ORR_Master.bWaterFailHelpActive = TRUE
								ENDIF
								CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_RACER_RESET_FADE_OUT")
							ENDIF
						ELIF (GET_TIMER_IN_SECONDS(waterTimer) >= 5.0)	
							RESTART_TIMER_NOW(waterTimer)
							RESTART_TIMER_NOW(exitTimer)
							CDEBUG1LN(DEBUG_OR_RACES, "waterFailure:  Race.bFailChecking set to FALSE")
							//Race.bFailChecking = FALSE
							//Race.ePlayerRespawnState = ORR_RESPAWN_SET
							//Race.Racer[0].eReset = ORR_RACER_RESET_INIT	
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_HELP_DIST")
								PRINT_HELP_FOREVER("SPR_HELP_DIST")
								ORR_Master.bWaterFailHelpActive = TRUE
							ENDIF
							CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_RACER_RESET_FADE_OUT")
						ENDIF
					ELSE
						IF IS_VEHICLE_ON_ALL_WHEELS(Race.Racer[0].Vehicle)
							IF IS_VEHICLE_STUCK_TIMER_UP(Race.Racer[0].Vehicle, VEH_STUCK_JAMMED, 1500) OR (GET_TIMER_IN_SECONDS(waterTimer) >= 4.0)
								RESTART_TIMER_NOW(waterTimer)
								RESTART_TIMER_NOW(exitTimer)
								CDEBUG1LN(DEBUG_OR_RACES, "waterFailure:  Race.bFailChecking set to FALSE")
								//Race.bFailChecking = FALSE
								//Race.Racer[0].eReset = ORR_RACER_RESET_INIT	
								//Race.ePlayerRespawnState = ORR_RESPAWN_SET
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_HELP_DIST")
									PRINT_HELP_FOREVER("SPR_HELP_DIST")
									ORR_Master.bWaterFailHelpActive = TRUE
								ENDIF
								CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_RACER_RESET_FADE_OUT")
							ENDIF
						ELIF (GET_TIMER_IN_SECONDS(waterTimer) >= 5.0)																		
							RESTART_TIMER_NOW(waterTimer)
							RESTART_TIMER_NOW(exitTimer)
							//Race.bFailChecking = FALSE
							CDEBUG1LN(DEBUG_OR_RACES, "WaterFailure.2:  Race.bFailChecking set to FALSE")
							//Race.Racer[0].eReset = ORR_RACER_RESET_INIT		
							//Race.ePlayerRespawnState = ORR_RESPAWN_SET
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_HELP_DIST")
								PRINT_HELP_FOREVER("SPR_HELP_DIST")
								ORR_Master.bWaterFailHelpActive = TRUE
							ENDIF
							CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_RACER_RESET_FADE_OUT")
						ENDIF
					ENDIF
				ELSE																
					IF (GET_TIMER_IN_SECONDS(waterTimer) >= 5.0)																		
						RESTART_TIMER_NOW(waterTimer)
						RESTART_TIMER_NOW(exitTimer)
						//Race.bFailChecking = FALSE
						CDEBUG1LN(DEBUG_OR_RACES, "waterFailure.3:  Race.bFailChecking set to FALSE")
						//Race.Racer[0].eReset = ORR_RACER_RESET_INIT	
						//Race.ePlayerRespawnState = ORR_RESPAWN_SET
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_HELP_DIST")
							PRINT_HELP_FOREVER("SPR_HELP_DIST")
							ORR_Master.bWaterFailHelpActive = TRUE
						ENDIF
						CDEBUG1LN(DEBUG_OR_RACES, "Moving to ORR_RACER_RESET_FADE_OUT")
					ENDIF
				ENDIF
			ENDIF
		ELIF IS_TIMER_STARTED(waterTimer)	
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SPR_HELP_DIST")
				AND ORR_Master.bWaterFailHelpActive
				AND NOT ORR_Master.bDistFailHelpActive
				ORR_Master.bWaterFailHelpActive = FALSE
				CLEAR_HELP()
			ELIF ORR_Master.bWaterFailHelpActive
				ORR_Master.bWaterFailHelpActive = FALSE
			ENDIF
			RESTART_TIMER_NOW(waterTimer)
		ENDIF
	ENDIF

ENDPROC

// NOT MOVING FAILURE
PROC idleFailure(ORR_RACE_STRUCT& Race)
	IF (GET_TIMER_IN_SECONDS(Race.tClock) > 15.0)
		IF ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON
			If (GET_ENTITY_SPEED(Race.Racer[0].Driver) < 0.75)
				IF NOT IS_TIMER_STARTED(idleTimer)
					START_TIMER_NOW_SAFE(idleTimer)
				ELSE 
					IF (GET_TIMER_IN_SECONDS(idleTimer) > 60.0)
						CDEBUG1LN(DEBUG_OR_RACES, "idleTimer over 30 failing due to idle")
						IF (Race.Racer[0].iGateCur <= Race.iGateCnt-1)
							IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur].blip)
								SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur].blip, DISPLAY_NOTHING)
							ENDIF
						ENDIF
						IF ((Race.Racer[0].iGateCur+1) <= (Race.iGateCnt-2))
							IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur+1].blip)
								SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur+1].blip, DISPLAY_NOTHING)
							ENDIF
						ENDIF
						CANCEL_TIMER(idleTimer)
						RESTART_TIMER_NOW(exitTimer)
						Race.bFailChecking = FALSE
						CDEBUG1LN(DEBUG_OR_RACES, "Idle Failure:  Race.bFailChecking set to FALSE")
						CLEAR_PRINTS()
						CPRINTLN( DEBUG_OR_RACES, "CLEARING HELP IN ORR_HELPERS LINE 1121" )
						CLEAR_HELP()
						SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
						ORR_Master.eFailReason = ORR_FAIL_IDLE
						MG_INIT_FAIL_SPLASH_SCREEN(ORR_Master.splashState)
						//SET_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI, orrFailString, orrFailStrapline, 4000, HUD_COLOUR_RED, MG_BIG_MESSAGE_CENTERED)

						Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
					ELIF GET_TIMER_IN_SECONDS(idleTimer) > 30
						IF NOT IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_PRINTED_IDLE_WARNING)
							PRINT_HELP_ONCE("SPR_IDLE_WARN", iSPRGeneralBits, ORR_PRINTED_IDLE_WARNING)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_TIMER_STARTED(idleTimer)
					CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_PRINTED_IDLE_WARNING)
					RESTART_TIMER_NOW(idleTimer)
				ENDIF
			ENDIF
		ELSE
			If (GET_ENTITY_SPEED(Race.Racer[0].Vehicle) < 5.0)
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_TIMER_STARTED(idleTimer)
					START_TIMER_NOW_SAFE(idleTimer)
				ELSE 
					IF (GET_TIMER_IN_SECONDS(idleTimer) > 60.0)
						CDEBUG1LN(DEBUG_OR_RACES, "idleTimer over 30 failing due to idle")
						IF (Race.Racer[0].iGateCur <= Race.iGateCnt-1)
							IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur].blip)
								SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur].blip, DISPLAY_NOTHING)
							ENDIF
							
						ENDIF
						IF ((Race.Racer[0].iGateCur+1) <= (Race.iGateCnt-2))
							IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur+1].blip)
								SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur+1].blip, DISPLAY_NOTHING)
							ENDIF
								
						ENDIF
						RESTART_TIMER_NOW(idleTimer)
						RESTART_TIMER_NOW(exitTimer)
						Race.bFailChecking = FALSE
						CDEBUG1LN(DEBUG_OR_RACES, "Idle Failure  Race.bFailChecking set to FALSE")
						CLEAR_PRINTS()
						CPRINTLN( DEBUG_OR_RACES, "CLEARING HELP IN ORR_HELPERS LINE 1161" )
						SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
						CLEAR_HELP()
						ORR_Master.eFailReason = ORR_FAIL_IDLE
						
						MG_INIT_FAIL_SPLASH_SCREEN(ORR_Master.splashState)
						//SET_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI, orrFailString, orrFailStrapline, 4000, HUD_COLOUR_RED, MG_BIG_MESSAGE_CENTERED)
						Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
					ELIF GET_TIMER_IN_SECONDS(idleTimer) > 30
						IF NOT IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_PRINTED_IDLE_WARNING)
							PRINT_HELP_ONCE("SPR_IDLE_WARN", iSPRGeneralBits, ORR_PRINTED_IDLE_WARNING)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_TIMER_STARTED(idleTimer)
					CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_PRINTED_IDLE_WARNING)
					RESTART_TIMER_NOW(idleTimer)
				ENDIF
			ENDIF	
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(idleTimer)
			CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_PRINTED_IDLE_WARNING)
			RESTART_TIMER_NOW(idleTimer)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ORR_IS_PED_A_RACER(ORR_RACE_STRUCT& Race, PED_INDEX& ped)
	INT i = 0
	REPEAT COUNT_OF(Race.Racer) i
		IF DOES_ENTITY_EXIST(Race.Racer[i].Driver)
			IF ped = Race.Racer[i].Driver
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE



ENDFUNC
//
//PROC DrawLiteralString(STRING sLiteral, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
//	//IF IS_KEYBOARD_KEY_PRESSED(KEY_L)
//		INT red, green, blue, alpha
//		GET_HUD_COLOUR(eColour, red, green, blue, alpha)
//		SET_TEXT_SCALE(0.45, 0.45)
//		SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha) * 0.5))
//		DISPLAY_TEXT_WITH_LITERAL_STRING(0.7795, 0.0305*TO_FLOAT(iColumn+1), "STRING", sLiteral)
//	//ENDIF
//ENDPROC
//
//PROC DrawLiteralStringInt(STRING sLiteral, INT sInt, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
//	TEXT_LABEL_63 sNewLiteral = sLiteral
//	sNewLiteral += sInt
//	
//	DrawLiteralString(sNewLiteral, iColumn, eColour)
//ENDPROC
//
//PROC DrawLiteralStringFloat(STRING sLiteral, FLOAT sFloat, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
//	TEXT_LABEL_63 sNewLiteral = sLiteral
//	
//	#IF IS_DEBUG_BUILD
//	sNewLiteral += GET_STRING_FROM_FLOAT(sFloat)
//	#ENDIF
//	#IF IS_FINAL_BUILD
//	sNewLiteral += ROUND(sFloat)
//	#ENDIF
//	
//	DrawLiteralString(sNewLiteral, iColumn, eColour)
//ENDPROC
//
//PROC DEBUG_PRINT_STUFF()
//	//first row
//	IF IS_PED_SHOOTING(PLAYER_PED_ID()) 
//		DrawLiteralString("Player is shooting", 0, HUD_COLOUR_RED)
//	ELSE
//		DrawLiteralString("Player is not shooting", 0)
//	ENDIF
//	
//	//second row
//	WEAPON_TYPE eWeaponType
//	IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eWeaponType)
//		IF eWeaponType = WEAPONTYPE_STICKYBOMB
//			DrawLiteralString("Player is holding a WEAPONTYPE_STICKYBOMB", 1, HUD_COLOUR_RED)
//		ELIF eWeaponType = WEAPONTYPE_GRENADE
//			DrawLiteralString("Player is holding a WEAPONTYPE_GRENADE", 1, HUD_COLOUR_RED)
//		ELSE
//			DrawLiteralString("Player is holding a different weapon", 1)
//		ENDIF
//	ELSE
//		DrawLiteralString("Player isn't holding a weapon", 1)
//	ENDIF
//	
//	
//	
//	
//ENDPROC


/// PURPOSE:
/// 	to resolve B*1466276 where the player can drop a bunch of grenades in a short period of time and not fail   
///     see JDiaz for any issues
PROC ORR_FAIL_FOR_WEAPONS_ABUSE(ORR_RACE_STRUCT& Race)
	BOOL bSetFail
	INT i = 0
	REPEAT COUNT_OF(Race.Racer) i
		IF DOES_ENTITY_EXIST(Race.Racer[i].Driver)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Driver, WEAPONTYPE_EXPLOSION)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Driver, WEAPONTYPE_GRENADE)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Driver, WEAPONTYPE_GRENADELAUNCHER)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Driver, WEAPONTYPE_STICKYBOMB)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Driver, WEAPONTYPE_RPG)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Driver, WEAPONTYPE_MOLOTOV) //b*2259878
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Driver, WEAPONTYPE_DLC_PROXMINE)
				bSetFail = TRUE
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(Race.Racer[i].Vehicle)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Vehicle, WEAPONTYPE_EXPLOSION)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Vehicle, WEAPONTYPE_GRENADE)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Vehicle, WEAPONTYPE_GRENADELAUNCHER)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Vehicle, WEAPONTYPE_STICKYBOMB)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Vehicle, WEAPONTYPE_RPG)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Vehicle, WEAPONTYPE_MOLOTOV) //b*2259878
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Race.Racer[i].Vehicle, WEAPONTYPE_DLC_PROXMINE)
				bSetFail = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bSetFail
		//Copied this fail from the idle Fail, hopefully this handles everything
		CDEBUG1LN(DEBUG_OR_RACES, "[FAIL] - player spooked another racer with an explosion")
		IF (Race.Racer[0].iGateCur <= Race.iGateCnt-1)
			IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur].blip)
				SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur].blip, DISPLAY_NOTHING)
			ENDIF
		ENDIF
		IF ((Race.Racer[0].iGateCur+1) <= (Race.iGateCnt-2))
			IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur+1].blip)
				SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur+1].blip, DISPLAY_NOTHING)
			ENDIF
		ENDIF
		CANCEL_TIMER(idleTimer)
		RESTART_TIMER_NOW(exitTimer)
		Race.bFailChecking = FALSE
		CDEBUG1LN(DEBUG_OR_RACES, "[FAIL] Explosion:  Race.bFailChecking set to FALSE")
		CLEAR_PRINTS()
		CLEAR_HELP()
		SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
		ORR_Master.eFailReason = ORR_FAIL_WEAPON
		
		MG_INIT_FAIL_SPLASH_SCREEN(ORR_Master.splashState)
		Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
	ENDIF
ENDPROC

// WANTED FAILURE
PROC wantedFailure(ORR_RACE_STRUCT& Race)
	
// Failcheck for illegal stuffs
	//WEAPON_TYPE playerWeapon
	//WEAPON_TYPE playerVehicleWeapon
	
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
		CPRINTLN(DEBUG_OR_RACES, "Wanted Failure:  Player is wanted.")
		CPRINTLN(DEBUG_OR_RACES, "Player's wanted level is: ", GET_PLAYER_WANTED_LEVEL(PLAYER_ID()))
		
		IF (Race.Racer[0].iGateCur <= Race.iGateCnt-1)
			IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur].blip)
				SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur].blip, DISPLAY_NOTHING)
			ENDIF
		ENDIF
		IF ((Race.Racer[0].iGateCur+1) <= (Race.iGateCnt-2))
			IF DOES_BLIP_EXIST(Race.sGate[Race.Racer[0].iGateCur+1].blip)
				SET_BLIP_DISPLAY(Race.sGate[Race.Racer[0].iGateCur+1].blip, DISPLAY_NOTHING)
			ENDIF
		ENDIF
		RESTART_TIMER_NOW(exitTimer)
		Race.bFailChecking = FALSE
		CPRINTLN(DEBUG_OR_RACES, "wantedFailure:  Race.bFailChecking set to FALSE")
		CLEAR_PRINTS()
		CLEAR_HELP()
		SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
		ORR_Master.eFailReason = ORR_FAIL_WEAPON
		
		MG_INIT_FAIL_SPLASH_SCREEN(ORR_Master.splashState)
		//SET_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI, orrFailString, orrFailStrapline, 4000, HUD_COLOUR_RED, MG_BIG_MESSAGE_CENTERED)
		Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
	ELIF GET_MAX_WANTED_LEVEL() = 0
		SET_MAX_WANTED_LEVEL(6) //setting this in case its been set too low.
	ENDIF	
//		IF IS_PED_SHOOTING(PLAYER_PED_ID())
//			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
//			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//			CPRINTLN(DEBUG_OR_RACES, "Wanted Failcase: Player has fired weapon, set wanted 1")
//		ENDIF
	
	WEAPON_TYPE curWeapon
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), curWeapon)
			IF curWeapon <> WEAPONTYPE_UNARMED
				IF IS_PED_SHOOTING( PLAYER_PED_ID() )
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					CPRINTLN(DEBUG_OR_RACES, "Wanted Failcase: Player has killed a racer. Setting wanted level to 1")
					Race.bAttackedDuringRace = TRUE
					CPRINTLN( DEBUG_OR_RACES, "Racer has been attacked, setting attacked during race to true" )
					EXIT				
				ENDIF
			ENDIF
		ENDIF
	ENDIF


ENDPROC


///// PURPOSE:
/////    Fail player if he assaults other racers.
/////    
///// PARAMS:
/////    Racer 			- 	Check if this racer has been assaulted.
/////    bfailchecking 	-  	Flag representing whether or not the race is checking for fail cases.
/////    PlayerRacer 		- 	The player.
//PROC assaultingAI(ORR_RACER_STRUCT& Racer, BOOL& bFailChecking, ORR_RACER_STRUCT& PlayerRacer)
//	IF ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON
//		IF NOT IS_ENTITY_DEAD(Racer.Driver)
//			IF NOT IS_ENTITY_DEAD(PlayerRacer.Driver)
//				//IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(Racer.Driver, PlayerRacer.Driver, FALSE)
//				IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(GET_PLAYER_INDEX())
//					BOOL bFailed
//					IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
//						bFailed = TRUE
//						CDEBUG1LN(DEBUG_OR_RACES, "[ORR_Helpers.sch->assaultingAI] Player has attacked AI, exiting the race")
//					ELIF IS_PED_SHOOTING(PLAYER_PED_ID())
//						bFailed = TRUE
//						CDEBUG1LN(DEBUG_OR_RACES, "[ORR_Helpers.sch->assaultingAI] Player has shot AI, exiting the race")
//					ELIF GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_PED_ID()) < 500
//						bFailed = TRUE
//						CDEBUG1LN(DEBUG_OR_RACES, "[ORR_Helpers.sch->assaultingAI] Player has run over AI, exiting the race")
//					ELSE
//						CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(GET_PLAYER_INDEX())
//					ENDIF
//					IF bFailed
//						RESTART_TIMER_NOW(exitTimer)
//						bFailChecking = FALSE
//						CDEBUG1LN(DEBUG_OR_RACES, "assaultingAI:  Race.bFailChecking set to FALSE")
//						CLEAR_PRINTS()
////						SET_SCALEFORM_BIG_MESSAGE(Race.bigMessageUI, orrFailString, orrFailStrapline, 4000, HUD_COLOUR_RED, MG_BIG_MESSAGE_MISSION_FAILED)
////						Race.Racer[0].eReset = ORR_RACER_RESET_FAIL_OVER
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

// -----------------------------------
// PLACEMENT PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_GetRelPos_Ground_Side(VECTOR& vRelPos, FLOAT fDist)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_GetRelPos_Ground_Side")
	VECTOR vSide = <<1.0, 0.0, 0.0>>
	vSide = vSide * fDist
	vRelPos = vRelPos + vSide
ENDPROC

PROC ORR_GetRelPos_Ground_Fwd(VECTOR& vRelPos, FLOAT fDist)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_GetRelPos_Ground_Fwd")
	VECTOR vFwd = <<0.0, 1.0, 0.0>>
	vFwd = vFwd * fDist
	vRelPos = vRelPos + vFwd
ENDPROC

PROC ORR_GetRelPos_Ground_Up(VECTOR& vRelPos, FLOAT fDist)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_GetRelPos_Ground_Up")
	FLOAT fHeight = 0.0
	IF NOT GET_GROUND_Z_FOR_3D_COORD(vRelPos, fHeight)
		GET_WATER_HEIGHT_NO_WAVES(vRelPos, fHeight)
	ENDIF
	vRelPos.z -= fHeight
	vRelPos.z += fDist
ENDPROC

PROC ORR_GetRelPos_Entity_Side(ENTITY_INDEX Entity, VECTOR& vRelPos, FLOAT fDist, BOOL bAbsolute)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_GetRelPos_Entity_Side")
	IF NOT IS_ENTITY_DEAD(Entity)
		VECTOR vSide, vTmp1, vTmp2, vTmp3
		GET_ENTITY_MATRIX(Entity, vTmp1, vSide, vTmp2, vTmp3)
		IF bAbsolute
			vRelPos.x = vTmp3.x
		ENDIF
		vSide = vSide * fDist
		vRelPos = vRelPos + vSide
	ENDIF
ENDPROC

PROC ORR_GetRelPos_Entity_Fwd(ENTITY_INDEX Entity, VECTOR& vRelPos, FLOAT fDist, BOOL bAbsolute)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_GetRelPos_Entity_Fwd")
	IF NOT IS_ENTITY_DEAD(Entity)
		VECTOR vFwd, vTmp1, vTmp2, vTmp3
		GET_ENTITY_MATRIX(Entity, vFwd, vTmp1, vTmp2, vTmp3)
		IF bAbsolute
			vRelPos.y = vTmp3.y
		ENDIF
		vFwd = vFwd * fDist
		vRelPos = vRelPos + vFwd
	ENDIF
ENDPROC

PROC ORR_GetRelPos_Entity_Up(ENTITY_INDEX Entity, VECTOR& vRelPos, FLOAT fDist, BOOL bAbsolute)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_GetRelPos_Entity_Up")
	IF NOT IS_ENTITY_DEAD(Entity)
		VECTOR vUp, vTmp1, vTmp2, vTmp3
		GET_ENTITY_MATRIX(Entity, vTmp1, vTmp2, vUp, vTmp3)
		IF bAbsolute
			vRelPos.z = vTmp3.z
		ENDIF
		vUp = vUp * fDist
		vRelPos = vRelPos + vUp
	ENDIF
ENDPROC

PROC ORR_GetRelPos_Coord_Side(VECTOR vCoord1, VECTOR vCoord2, VECTOR& vRelPos, FLOAT fDist, BOOL bAbsolute)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_GetRelPos_Coord_Side")
	IF bAbsolute
		vRelPos.x = vCoord1.x
	ENDIF
	VECTOR vFwd = vCoord2 - vCoord1
	VECTOR vUp = <<0.0, 0.0, 1.0>>
	VECTOR vSide = CROSS_PRODUCT(vFwd, vUp)
	vSide = NORMALISE_VECTOR(vSide)
	vSide = vSide * fDist
	vRelPos = vRelPos + vSide
ENDPROC

PROC ORR_GetRelPos_Coord_Fwd(VECTOR vCoord1, VECTOR vCoord2, VECTOR& vRelPos, FLOAT fDist, BOOL bAbsolute)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_GetRelPos_Coord_Fwd")
	IF bAbsolute
		vRelPos.y = vCoord1.y
	ENDIF
	VECTOR vFwd = vCoord2 - vCoord1
	vFwd = NORMALISE_VECTOR(vFwd)
	vFwd = vFwd * fDist
	vRelPos = vRelPos + vFwd
ENDPROC

PROC ORR_GetRelPos_Coord_Up(VECTOR vCoord1, VECTOR& vRelPos, FLOAT fDist, BOOL bAbsolute)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_GetRelPos_Coord_Up")
	IF bAbsolute
		vRelPos.z = vCoord1.z
	ENDIF
	vRelPos.z += fDist
ENDPROC



// -----------------------------------
// HUD/UI PROCS/FUNCTIONS
// -----------------------------------

/// PURPOSE:
///    Displays the countdown at the start of the race.
///    
/// PARAMS:
///    Display - Struct with a timer, and int and float counters.
/*PROC ORR_Set_Countdown_Message()
	
	
	IF nCountdownNumber > 0
		BEGIN_SCALEFORM_MOVIE_METHOD(ORR_COUNTDOWN_UI, "SET_MESSAGE")
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
				ADD_TEXT_COMPONENT_INTEGER(nCountdownNumber)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(252)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(219)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(78)
		END_SCALEFORM_MOVIE_METHOD()
	ELIF nCountdownNumber = 0
		BEGIN_SCALEFORM_MOVIE_METHOD(ORR_COUNTDOWN_UI, "SET_MESSAGE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SPR_COUNT_GO")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(127)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(255)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

ENDPROC

*/
/// PURPOSE:
///    Displays the countdown at the start of the race.
///    
/// PARAMS:
///    Display - Struct with a timer, and int and float counters.
///    
/// RETURNS:
///    BOOL - Countdown still displaying.
FUNC BOOL ORR_Countdown_Display(ORR_DISPLAY_STRUCT& Display)

	SWITCH(eCountdownStage)
	
		CASE ORR_COUNTDOWN_STAGE_INIT
			iBoostTimer = -1
			eBoostState = BS_READY
			
			CPRINTLN( DEBUG_OR_RACES, "Prepping boost state..." )
			START_TIMER_NOW_SAFE(Display.tCnt)
			SET_MINIGAME_COUNTDOWN_UI_NUMBER(ORR_CountDownUI, 3)
			eCountdownStage = ORR_COUNTDOWN_RUN
		BREAK
		
		CASE ORR_COUNTDOWN_RUN
			IF TIMER_DO_WHEN_READY(Display.tCnt, 1)
				UPDATE_MINIGAME_COUNTDOWN_UI(ORR_CountDownUI, TRUE, FALSE, TRUE, 3, TRUE)
				eCountdownStage = ORR_COUNTDOWN_STAGE_WAIT
			ENDIF
		BREAK

		CASE ORR_COUNTDOWN_STAGE_WAIT
			IF UPDATE_MINIGAME_COUNTDOWN_UI(ORR_CountDownUI, FALSE, FALSE, FALSE, 3, TRUE)
				RESTART_TIMER_NOW(ORR_Master.tmrCountdown)
				CANCEL_TIMER(ORR_CountDownUI.CountdownTimer)				
				RETURN FALSE
			ENDIF
		BREAK

/*		CASE ORR_COUNTDOWN_STAGE_2
			IF TIMER_DO_WHEN_READY(Display.tCnt, 2)
				ORR_Set_Countdown_Message(1)
				eCountdownStage = ORR_COUNTDOWN_STAGE_1
			ENDIF
		BREAK

		CASE ORR_COUNTDOWN_STAGE_1
			IF TIMER_DO_WHEN_READY(Display.tCnt, 3)
				ORR_Set_Countdown_Message(0)
				eCountdownStage = ORR_COUNTDOWN_STAGE_GO
			ENDIF
		BREAK
		
		CASE ORR_COUNTDOWN_STAGE_GO
			IF TIMER_DO_ONCE_WHEN_READY(Display.tCnt, 3.1)
				// Countdown no longer being displayed.
				RETURN FALSE
				eCountdownStage = ORR_COUNTDOWN_STAGE_WAIT
			ENDIF
		BREAK
*/		RETURN TRUE
	ENDSWITCH
	
//	DRAW_SCALEFORM_MOVIE(ORR_COUNTDOWN_UI, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 100)
	
	// Countdown still displaying.
	RETURN TRUE
	
ENDFUNC

/*
FUNC BOOL DEPRECATED_ORR_Countdown_Display(ORR_DISPLAY_STRUCT& Display)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Countdown_Display")

	// If timer isn't started, play a sound and start it.
	IF NOT IS_TIMER_STARTED(Display.tCnt)
		PLAY_SOUND_FRONTEND(-1, "PHONE_GENERIC_KEY_01", "HUD_MINI_GAME_SOUNDSET")
		RESTART_TIMER_NOW(Display.tCnt)
	// Otherwise, if ocunter > zero, display countdown (num) for proper amount of time.
	ELIF (Display.iCnt > 0)
		SET_TEXT_SCALE(ORR_UI_CD_NUM_SCALE, ORR_UI_CD_NUM_SCALE)
		DISPLAY_TEXT_WITH_NUMBER(ORR_UI_CD_NUM_POS_X, ORR_UI_CD_NUM_POS_Y, "SPR_COUNT_NUM", Display.iCnt)
		IF TIMER_DO_WHEN_READY(Display.tCnt, ORR_UI_CD_NUM_TIME)
			CANCEL_TIMER(Display.tCnt)
			--Display.iCnt
		ENDIF
	// Otherwise, ocunter is zero, display countdown (go) for proper amount of time.
	ELSE
		//SET_TEXT_SCALE(ORR_UI_CD_GO_SCALE, ORR_UI_CD_GO_SCALE)
		//DISPLAY_TEXT(ORR_UI_CD_GO_POS_X, ORR_UI_CD_GO_POS_Y, "SPR_COUNT_GO")
		//IF TIMER_DO_WHEN_READY(Display.tCnt, ORR_UI_CD_GO_TIME)
		//	CANCEL_TIMER(Display.tCnt)
			RETURN FALSE
		//ENDIF
	ENDIF
	
	// Countdown still displaying.
	RETURN TRUE
	
ENDFUNC
*/

FUNC BOOL ORR_Finish_Display(ORR_DISPLAY_STRUCT& Display)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Finish_Display")
	
	IF NOT IS_TIMER_STARTED(Display.tCnt)		
		RESTART_TIMER_NOW(Display.tCnt) //not sure if timer is used elsewhereso keeping it here, prob safe to remove tho- SiM - 10/31/2011
	ENDIF
	
	MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	RETURN TRUE

ENDFUNC

PROC ORR_Rank_Display(INT iRank, FLOAT fPosX, FLOAT fPosY, FLOAT fScale)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Rank_Display")
	TEXT_LABEL_15 szRank
	SWITCH iRank
		CASE 1
			szRank = "SPR_PLACE_ST"
		BREAK
		CASE 2
			szRank = "SPR_PLACE_ND"
		BREAK
		CASE 3
			szRank = "SPR_PLACE_RD"
		BREAK
		DEFAULT
			szRank = "SPR_PLACE_TH"
		BREAK	
	ENDSWITCH
	SET_TEXT_SCALE(fScale, fScale)
	DISPLAY_TEXT_WITH_NUMBER(fPosX, fPosY, szRank, iRank)
ENDPROC

PROC ORR_Clock_GetComponents(FLOAT fClock, INT& iMinutes, INT& iSeconds, INT& iMilliSecs)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Clock_GetComponents")

	// Get total milliseconds on clock.
	INT iTotal = ROUND(fClock * 1000.0)
	
	// Use millisecond total to get clock components (m/s/ms).
	iMinutes = (iTotal / 1000) / 60
	iSeconds = (iTotal - (iMinutes * 60 * 1000)) / 1000
	iMilliSecs = (iTotal - ((iSeconds + (iMinutes * 60)) * 1000))
	
ENDPROC

FUNC TEXT_LABEL_15 ORR_Clock_MakeString(FLOAT fClock, FLOAT& fPosX, FLOAT fScale, BOOL bDynamic)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Clock_MakeString")

	// Local variables.
	TEXT_LABEL_15 szClock
	INT iMinutes, iSeconds, iMilliSecs
	FLOAT fNumWidth = ORR_HUD_NUM_WIDTH * fScale
	FLOAT fClnWidth = ORR_HUD_CLN_WIDTH * fScale
	
	// Get clock components (m/s/ms).
	ORR_Clock_GetComponents(fClock, iMinutes, iSeconds, iMilliSecs)
	
	// Truncate milliseconds (dynamic - tenths, static - hundreths).
	IF bDynamic
		iMilliSecs /= 100
	ELSE
		iMilliSecs /= 10
	ENDIF
	
	// Assemble string from clock components (dynamic - #:##.#, static - ##:##.##).
	IF (iMinutes < 10)
		IF bDynamic
			IF (iMinutes >= 1)
				fPosX += fNumWidth
			ENDIF
		ELSE
			szClock += "0"
		ENDIF
	ENDIF
	IF (iMinutes < 1)
		IF bDynamic
			fPosX += fNumWidth
			fPosX += fClnWidth
		ELSE
			szClock += "0:"
		ENDIF
	ELSE
		szClock += iMinutes
		szClock += ":"
	ENDIF
	IF (iSeconds < 10)
		IF bDynamic
		AND (iMinutes < 1)
			fPosX += fNumWidth
		ELSE
			szClock += "0"
		ENDIF
	ENDIF
	IF (iSeconds < 1)
		IF bDynamic
		AND (iMinutes < 1)
			fPosX += fNumWidth
		ELSE
			szClock += "0"
		ENDIF
	ELSE
		szClock += iSeconds
	ENDIF
	szClock += "."
	szClock += iMilliSecs
	IF NOT bDynamic
	AND (iMilliSecs < 10)
		szClock += "0"
	ENDIF
	
	// Return clock string.
	RETURN szClock
	
ENDFUNC

PROC ORR_Clock_Display(FLOAT fClock, FLOAT fPosX, FLOAT fPosY, FLOAT fScale, BOOL bDynamic)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Clock_Display")

	// Make clock string and offset position to accommodate format (dynamic/static).
	ORR_Clock_MakeString(fClock, fPosX, fScale, bDynamic) //still using for scale
	
	// Display clock string at desired position/scale.
	SET_TEXT_SCALE(fScale, fScale)	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
		ADD_TEXT_COMPONENT_SUBSTRING_TIME(FLOOR(fClock*1000), TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS)
	END_TEXT_COMMAND_DISPLAY_TEXT(fPosX, fPosY)	
ENDPROC

FUNC TEXT_LABEL_15 ORR_PlusMinus_MakeString(FLOAT fPlusMinus, FLOAT& fPosX, FLOAT fScale, BOOL bPlusMinus)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_PlusMinus_MakeString")
	
	// Local variables.
	TEXT_LABEL_15 szPlusMinus
	INT iMinutes, iSeconds, iMilliSecs
	FLOAT fNumWidth = ORR_HUD_NUM_WIDTH * fScale
	FLOAT fClnWidth = ORR_HUD_CLN_WIDTH * fScale
	
	// Get clock components (m/s/ms).
	ORR_Clock_GetComponents(fPlusMinus, iMinutes, iSeconds, iMilliSecs)
	
	// Truncate milliseconds (hundreths).
	iMilliSecs /= 10
	
	// Start string with a plus/minus accordingly.
	IF bPlusMinus
		szPlusMinus = "+"
		fPosX -= (ORR_HUD_PLS_WIDTH * fScale)
	ELSE
		szPlusMinus = "-"
		fPosX -= (ORR_HUD_MNS_WIDTH * fScale)
	ENDIF
	
	// Assemble string from clock components (plus - +##:##.##, minus - -##:##.##).
	IF (iMinutes < 10)
		IF (iMinutes >= 1)
			fPosX += fNumWidth
		ENDIF
	ENDIF
	IF (iMinutes < 1)
		fPosX += fNumWidth
		fPosX += fClnWidth
	ELSE
		szPlusMinus += iMinutes
		szPlusMinus += ":"
	ENDIF
	IF (iSeconds < 10)
		IF (iMinutes < 1)
			fPosX += fNumWidth
		ELSE
			szPlusMinus += "0"
		ENDIF
	ENDIF
	IF (iSeconds < 1)
		IF (iMinutes < 1)
			fPosX += fNumWidth
		ELSE
			szPlusMinus += "0"
		ENDIF
	ELSE
		szPlusMinus += iSeconds
	ENDIF
	szPlusMinus += "."
	szPlusMinus += iMilliSecs
	IF (iMilliSecs < 10)
		szPlusMinus += "0"
	ENDIF
	
	// Return plus/minus string.
	RETURN szPlusMinus
	
ENDFUNC

PROC ORR_RadarOverlay_Display(FLOAT fRacerRoll)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_RadarOverlay_Display")

	// TODO: Ask Alwyn why this isn't drawing over HUD.
	
	// Calculate radar overlay rotation using racer roll.
	FLOAT fOverlayRot = fRacerRoll * ORR_HUD_RADAR_ROT_SCL
	
	// Display radar overlay (sprite).
	DRAW_SPRITE("PilotSchool", "PlaneRadarOver",
	ORR_HUD_RADAR_POS_X, ORR_HUD_RADAR_POS_Y,
	ORR_HUD_RADAR_WIDTH, ORR_HUD_RADAR_HEIGHT,
	fOverlayRot, 128, 255, 128, 128)
	
ENDPROC

PROC ORR_ChkpntType_Populate(TEXT_LABEL_31& szChkpntType[])
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_ChkpntType_Populate")
	szChkpntType[0] = "Offroad Default"
	szChkpntType[1] = "Offroad Finish"
	szChkpntType[2] = "Stunt Plane Default"
	szChkpntType[3] = "Stunt Plane Finish"
	szChkpntType[4] = "Triathlon Swim"
	szChkpntType[5] = "Triathlon Bike"
	szChkpntType[6] = "Triathlon Run"
	szChkpntType[7] = "Triathlon Finish"
	szChkpntType[8] = "Triathlon Swim>Bike"
	szChkpntType[9] = "Triathlon Bike>Run"
ENDPROC

FUNC ORR_RACE_CHECKPOINT_TYPE ORR_ChkpntType_GetEnum(INT iChkpntType)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_ChkpntType_GetEnum")
	ORR_RACE_CHECKPOINT_TYPE eChkpntType
	SWITCH iChkpntType
		// Ground Arrow.
		CASE 0 
			eChkpntType = ORR_CHKPT_OFFROAD_DEFAULT
		BREAK
		// Ground Flag.
		CASE 1 
			eChkpntType = ORR_CHKPT_OFFROAD_FINISH
		BREAK
		// Air Arrow.
		CASE 2 
			eChkpntType = ORR_CHKPT_STUNT_DEFAULT
		BREAK
		// Air Flag.
		CASE 3 
			eChkpntType = ORR_CHKPT_STUNT_FINISH
		BREAK
		// Water Arrow.
		CASE 4 
			eChkpntType = ORR_CHKPT_TRI_SWIM	
		BREAK
		// Water Arrow.
		CASE 5 
			eChkpntType = ORR_CHKPT_TRI_BIKE
		BREAK
		// Water Arrow.
		CASE 6 
			eChkpntType = ORR_CHKPT_TRI_RUN
		BREAK		
		// Water Flag.
		CASE 7 
			eChkpntType = ORR_CHKPT_TRI_FINISH
		BREAK
		CASE 8 
			eChkpntType = ORR_CHKPT_TRI_FIRST_TRANS
		BREAK
		CASE 9 
			eChkpntType = ORR_CHKPT_TRI_SECOND_TRANS
		BREAK
	ENDSWITCH
	RETURN eChkpntType
ENDFUNC

FUNC INT ORR_ChkpntType_GetIndex(ORR_RACE_CHECKPOINT_TYPE eChkpntType)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_ChkpntType_GetIndex")
	INT iChkpntType
	SWITCH eChkpntType
		// Ground Arrow.
		CASE ORR_CHKPT_OFFROAD_DEFAULT
			iChkpntType = 0
		BREAK
		// Ground Flag.
		CASE ORR_CHKPT_OFFROAD_FINISH
			iChkpntType = 1
		BREAK
		// Air Arrow.
		CASE ORR_CHKPT_STUNT_DEFAULT
			iChkpntType = 2
		BREAK
		// Air Flag.
		CASE ORR_CHKPT_STUNT_FINISH
			iChkpntType = 3
		BREAK
		// Water Arrow.
		CASE ORR_CHKPT_TRI_SWIM
			iChkpntType = 4
		BREAK
		// Water Arrow.
		CASE ORR_CHKPT_TRI_BIKE
			iChkpntType = 5
		BREAK
		// Water Arrow.
		CASE ORR_CHKPT_TRI_RUN
			iChkpntType = 6
		BREAK
		// Water Flag.
		CASE ORR_CHKPT_TRI_FINISH
			iChkpntType = 7
		BREAK
		// Water Flag.
		CASE ORR_CHKPT_TRI_FIRST_TRANS
			iChkpntType = 8
		BREAK
		// Water Flag.
		CASE ORR_CHKPT_TRI_SECOND_TRANS
			iChkpntType = 9
		BREAK
	ENDSWITCH
	RETURN iChkpntType
ENDFUNC

FUNC TEXT_LABEL_31 ORR_ChkpntType_GetString(ORR_RACE_CHECKPOINT_TYPE eChkpntType)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_ChkpntType_GetString")
	TEXT_LABEL_31 szChkpntType
	SWITCH eChkpntType
		// Ground Arrow.
		CASE ORR_CHKPT_OFFROAD_DEFAULT
			szChkpntType = "ORR_CHKPT_OFFROAD_DEFAULT"
		BREAK
		// Ground Flag.
		CASE ORR_CHKPT_OFFROAD_FINISH
			szChkpntType = "ORR_CHKPT_OFFROAD_FINISH"
		BREAK
		// Air Arrow.
		CASE ORR_CHKPT_STUNT_DEFAULT
			szChkpntType = "ORR_CHKPT_STUNT_DEFAULT"
		BREAK
		// Air Flag.
		CASE ORR_CHKPT_STUNT_FINISH
			szChkpntType = "ORR_CHKPT_STUNT_FINISH"
		BREAK
		// Water Arrow.
		CASE ORR_CHKPT_TRI_SWIM
			szChkpntType = "ORR_CHKPT_TRI_SWIM"
		BREAK
		// Water Flag.
		CASE ORR_CHKPT_TRI_BIKE
			szChkpntType = "ORR_CHKPT_TRI_BIKE"
		BREAK
		// Water Arrow.
		CASE ORR_CHKPT_TRI_RUN
			szChkpntType = "ORR_CHKPT_TRI_RUN"
		BREAK
		// Water Flag.
		CASE ORR_CHKPT_TRI_FINISH
			szChkpntType = "ORR_CHKPT_TRI_FINISH"
		BREAK
		// Water Flag.
		CASE ORR_CHKPT_TRI_FIRST_TRANS
			szChkpntType = "ORR_CHKPT_TRI_FIRST_TRANS"
		BREAK
		// Water Flag.
		CASE ORR_CHKPT_TRI_SECOND_TRANS
			szChkpntType = "ORR_CHKPT_TRI_SECOND_TRANS"
		BREAK
	ENDSWITCH
	RETURN szChkpntType
ENDFUNC

PROC ORR_DriverType_Populate(TEXT_LABEL_31& szDriverType[])
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_DriverType_Populate")
	szDriverType[0] = "Player"
	szDriverType[1] = "AI Male"
	szDriverType[2] = "AI Female"
	szDriverType[3] = "Triathlon Male"
ENDPROC

FUNC PED_TYPE ORR_DriverType_GetEnum(INT iDriverType)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_DriverType_GetEnum")
	PED_TYPE eDriverType
	SWITCH iDriverType
		// Player.
		CASE 0 
			eDriverType = PEDTYPE_PLAYER1
		BREAK
		// AI Male.
		CASE 1 
			eDriverType = PEDTYPE_CIVMALE
		BREAK
		// AI Female.
		CASE 2 
			eDriverType = PEDTYPE_CIVFEMALE
		BREAK
	ENDSWITCH
	RETURN eDriverType
ENDFUNC

FUNC INT ORR_DriverType_GetIndex(PED_TYPE eDriverType)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_DriverType_GetIndex")
	INT iDriverType
	SWITCH eDriverType
		// Player.
		CASE PEDTYPE_PLAYER1
			iDriverType = 0
		BREAK
		// AI Male.
		CASE PEDTYPE_CIVMALE
			iDriverType = 1
		BREAK
		// AI Female.
		CASE PEDTYPE_CIVFEMALE
			iDriverType = 2
		BREAK
	ENDSWITCH
	RETURN iDriverType
ENDFUNC

FUNC TEXT_LABEL_31 ORR_DriverType_GetString(PED_TYPE eDriverType)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_DriverType_GetString")
	TEXT_LABEL_31 szDriverType
	SWITCH eDriverType
		// Player.
		CASE PEDTYPE_PLAYER1
			szDriverType = "PEDTYPE_PLAYER1"
		BREAK
		// AI Male.
		CASE PEDTYPE_CIVMALE
			szDriverType = "PEDTYPE_CIVMALE"
		BREAK
		// AI Female.
		CASE PEDTYPE_CIVFEMALE
			szDriverType = "PEDTYPE_CIVFEMALE"
		BREAK
	ENDSWITCH
	RETURN szDriverType
ENDFUNC

PROC ORR_DriverModel_Populate(TEXT_LABEL_31& szDriverModel[])
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_DriverModel_Populate")
	szDriverModel[0] = "Player"
	szDriverModel[1] = "AI Male"
	szDriverModel[2] = "AI Female"
	szDriverModel[3] = "Triathlon Male"
ENDPROC

FUNC MODEL_NAMES ORR_DriverModel_GetEnum(INT iDriverModel)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_DriverModel_GetEnum")
	MODEL_NAMES eDriverModel
	SWITCH iDriverModel
		// Player.
		CASE 0 
			eDriverModel = PLAYER_ONE
		BREAK
		// AI Male.
		CASE 1 
			eDriverModel = A_M_Y_GENSTREET_01
		BREAK
		// AI Female.
		CASE 2 
			eDriverModel = A_F_Y_TOURIST_01
		BREAK
		// AI Female.
		CASE 3 
			eDriverModel = A_M_Y_RoadCyc_01
		BREAK
	ENDSWITCH
	RETURN eDriverModel
ENDFUNC

FUNC INT ORR_DriverModel_GetIndex(MODEL_NAMES eDriverModel)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_DriverModel_GetIndex")
	INT iDriverModel
	SWITCH eDriverModel
		// Player.
		CASE PLAYER_ONE
			iDriverModel = 0
		BREAK
		// AI Male.
		CASE A_M_Y_GENSTREET_01
			iDriverModel = 1
		BREAK
		// AI Female.
		CASE A_F_Y_TOURIST_01
			iDriverModel = 2
		BREAK
		// AI Female.
		CASE A_M_Y_RoadCyc_01
			iDriverModel = 3
		BREAK
	ENDSWITCH
	RETURN iDriverModel
ENDFUNC

FUNC TEXT_LABEL_31 ORR_DriverModel_GetString(MODEL_NAMES eDriverModel)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_DriverModel_GetString")
	TEXT_LABEL_31 szDriverModel
	SWITCH eDriverModel
		// Player.
		CASE PLAYER_ONE
			szDriverModel = "PLAYER_ONE"
		BREAK
		// AI Male.
		CASE A_M_Y_GENSTREET_01
			szDriverModel = "A_M_Y_GENSTREET_01"
		BREAK
		// AI Female.
		CASE A_F_Y_TOURIST_01
			szDriverModel = "A_F_Y_TOURIST_01"
		BREAK
		// AI Female.
		CASE A_M_Y_RoadCyc_01
			szDriverModel = "A_M_Y_RoadCyc_01"
		BREAK
	ENDSWITCH
	RETURN szDriverModel
ENDFUNC

PROC ORR_VehicleModel_Populate(TEXT_LABEL_31& szVehicleModel[])
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_VehicleModel_Populate")
	szVehicleModel[0] = "Cheetah (Sport)"
	szVehicleModel[1] = "Infernus (Sport)"
	szVehicleModel[2] = "JB700 (Sport)"
	szVehicleModel[3] = "Monroe (Sport)"
	szVehicleModel[4] = "Ninef 1 (Sport)"
	szVehicleModel[5] = "Ninef 2 (Sport)"
	szVehicleModel[6] = "Rapid GT 1 (Sport)"
	szVehicleModel[7] = "Rapid GT 2 (Sport)"
	szVehicleModel[8] = "Z Type (Sport)"
	szVehicleModel[9] = "Entity XF (Sport)"
	szVehicleModel[10] = "BF Injection 1 (2-Door)"
	szVehicleModel[11] = "Bison (2-Door)"
	szVehicleModel[12] = "Dominator (2-Door)"
	szVehicleModel[13] = "Feltzer 2010 (2-Door)"
	szVehicleModel[14] = "Penumbra (2-Door)"
	szVehicleModel[15] = "Phoenix (2-Door)"
	szVehicleModel[16] = "Stinger (2-Door)"
	szVehicleModel[17] = "Cargobob (Helicopter)"
	szVehicleModel[18] = "Maverick (Helicopter)"
	szVehicleModel[19] = "Picador (2-Door)"
	szVehicleModel[20] = "Tornado (2-Door)"
	szVehicleModel[21] = "Baller (4-Door)"
	szVehicleModel[22] = "Dubsta 1 (4-Door)"
	szVehicleModel[23] = "Dubsta 2 (4-Door)"
	szVehicleModel[24] = "FQ2 (4-Door)"
	szVehicleModel[25] = "Granger (4-Door)"
	szVehicleModel[26] = "Mesa (4-Door)"
	szVehicleModel[27] = "Radius (4-Door)"
	szVehicleModel[28] = "Sadler (4-Door)"
	szVehicleModel[29] = "Surfer 1 (4-Door)"
	szVehicleModel[30] = "Surfer 2 (4-Door)"
	szVehicleModel[31] = "BJXL (4-Door)"
	szVehicleModel[32] = "Jackal (4-Door)"
	szVehicleModel[33] = "Blazer (4-Wheeler)"
	szVehicleModel[34] = "Hexer (Motorcycle)"
	szVehicleModel[35] = "Vader 1 (Motorcycle)"
	szVehicleModel[36] = "Vader 2 (Motorcycle)"
	szVehicleModel[37] = "Sanchez (Dirtbike)"
	szVehicleModel[38] = "Faggio (Moped)"
	szVehicleModel[39] = "BMX (Bicycle)"
	szVehicleModel[40] = "Scorcher (Bicycle)"
	szVehicleModel[41] = "Skivvy 1 (Boat)" //deprecated!!!
	szVehicleModel[42] = "Skivvy 2 (Boat)" //deprecated!!!
	szVehicleModel[43] = "Seashark (Jetski)"
	szVehicleModel[44] = "Cuban 800 (Plane)"
	szVehicleModel[45] = "Stunt (Plane)"
	szVehicleModel[46] = "Vulkan (Plane)"
	szVehicleModel[47] = "On Foot (No Vehicle)"
ENDPROC

FUNC MODEL_NAMES ORR_VehicleModel_GetEnum(INT iVehicleModel)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_VehicleModel_GetEnum")
	MODEL_NAMES eVehicleModel
	SWITCH iVehicleModel
		// Cheetah (Sport).
		CASE 0
			eVehicleModel = CHEETAH
		BREAK
		// Infernus (Sport).
		CASE 1
			eVehicleModel = INFERNUS
		BREAK
		// JB700 (Sport).
		CASE 2
			eVehicleModel = JB700
		BREAK
		// Monroe (Sport).
		CASE 3
			eVehicleModel = MONROE
		BREAK
		// Ninef 1 (Sport).
		CASE 4
			eVehicleModel = NINEF
		BREAK
		// Ninef 2 (Sport).
		CASE 5
			eVehicleModel = NINEF2
		BREAK
		// Rapid GT 1 (Sport).
		CASE 6
			eVehicleModel = RAPIDGT
		BREAK
		// Rapid GT 2 (Sport).
		CASE 7
			eVehicleModel = RAPIDGT2
		BREAK
		// Z Type (Sport).
		CASE 8
			eVehicleModel = ZTYPE
		BREAK
		// Entity XF (Sport).
		CASE 9
			eVehicleModel = ENTITYXF
		BREAK
		// BF Injection 1 (2-Door).
		CASE 10
			eVehicleModel = BFINJECTION
		BREAK
		// Bison (2-Door).
		CASE 11
			eVehicleModel = BISON
		BREAK
		// Dominator (2-Door).
		CASE 12
			eVehicleModel = DOMINATOR
		BREAK
		// Feltzer 2010 (2-Door).
		CASE 13
			eVehicleModel = FELTZER2
		BREAK
		// Penumbra (2-Door).
		CASE 14
			eVehicleModel = PENUMBRA
		BREAK
		// Phoenix (2-Door).
		CASE 15
			eVehicleModel = PHOENIX
		BREAK
		// Stinger (2-Door).
		CASE 16
			eVehicleModel = STINGER
		BREAK
		// Maverick (Helicopter).
		CASE 17
			eVehicleModel = CARGOBOB
		BREAK
		// Cargobob (Helicopter).
		CASE 18
			eVehicleModel = MAVERICK
		BREAK		
		// Picador (2-Door).
		CASE 19
			eVehicleModel = PICADOR
		BREAK
		// Tornado (2-Door).
		CASE 20
			eVehicleModel = TORNADO
		BREAK
		// Baller (4-Door).
		CASE 21
			eVehicleModel = BALLER
		BREAK
		// Dubsta 1 (4-Door).
		CASE 22
			eVehicleModel = DUBSTA
		BREAK
		// Dubsta 2 (4-Door).
		CASE 23
			eVehicleModel = DUBSTA2
		BREAK
		// FQ2 (4-Door).
		CASE 24
			eVehicleModel = FQ2
		BREAK
		// Granger (4-Door).
		CASE 25
			eVehicleModel = GRANGER
		BREAK
		// Mesa (4-Door).
		CASE 26
			eVehicleModel = MESA
		BREAK
		// Radius (4-Door).
		CASE 27
			eVehicleModel = RADI
		BREAK
		// Sadler (4-Door).
		CASE 28
			eVehicleModel = SADLER
		BREAK
		// Surfer 1 (4-Door).
		CASE 29
			eVehicleModel = SURFER
		BREAK
		// Surfer 2 (4-Door).
		CASE 30
			eVehicleModel = SURFER2
		BREAK
		// BJXL (4-Door).
		CASE 31
			eVehicleModel = BJXL
		BREAK
		// Jackal (4-Door).
		CASE 32
			eVehicleModel = JACKAL
		BREAK
		// Blazer (4-Wheeler).
		CASE 33
			eVehicleModel = BLAZER
		BREAK
		// Hexer (Motorcycle).
		CASE 34
			eVehicleModel = HEXER
		BREAK
		// Vader 1 (Motorcycle).
		CASE 35
			eVehicleModel = VADER
		BREAK
		// Vader 2 (Motorcycle).
		CASE 36
			eVehicleModel = VADER
		BREAK
		// Sanchez (Dirtbike).
		CASE 37
			eVehicleModel = SANCHEZ
		BREAK
		// Faggio (Moped).
		CASE 38
			eVehicleModel = FAGGIO2
		BREAK
		// BMX (Bicycle).
		CASE 39
			eVehicleModel = BMX
		BREAK
		// Scorcher (Bicycle).
		CASE 40
			eVehicleModel = SCORCHER
		BREAK
		// Seashark (Jetski).
		CASE 43
			eVehicleModel = SEASHARK
		BREAK
		// Cuban 800 (Plane).
		CASE 44
			eVehicleModel = CUBAN800
		BREAK
		// Stunt (Plane).
		CASE 45
			eVehicleModel = STUNT
		BREAK
		// On Foot (No Vehicle).
		CASE 46
			eVehicleModel = DUMMY_MODEL_FOR_SCRIPT
		BREAK
	ENDSWITCH
	RETURN eVehicleModel
ENDFUNC

FUNC INT ORR_VehicleModel_GetIndex(MODEL_NAMES eVehicleModel)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_VehicleModel_GetIndex")
	INT iVehicleModel
	SWITCH eVehicleModel
		// Cheetah (Sport).
		CASE CHEETAH
			iVehicleModel = 0
		BREAK
		// Infernus (Sport).
		CASE INFERNUS
			iVehicleModel = 1
		BREAK
		// JB700 (Sport).
		CASE JB700
			iVehicleModel = 2
		BREAK
		// Monroe (Sport).
		CASE MONROE
			iVehicleModel = 3
		BREAK
		// Ninef 1 (Sport).
		CASE NINEF
			iVehicleModel = 4
		BREAK
		// Ninef 2 (Sport).
		CASE NINEF2
			iVehicleModel = 5
		BREAK
		// Rapid GT 1 (Sport).
		CASE RAPIDGT
			iVehicleModel = 6
		BREAK
		// Rapid GT 2 (Sport).
		CASE RAPIDGT2
			iVehicleModel = 7
		BREAK
		// Z Type (Sport).
		CASE ZTYPE
			iVehicleModel = 8
		BREAK
		// Entity XF (Sport).
		CASE ENTITYXF
			iVehicleModel = 9
		BREAK
		// BF Injection 1 (2-Door).
		CASE BFINJECTION
			iVehicleModel = 10
		BREAK
		// Bison (2-Door).
		CASE BISON
			iVehicleModel = 11
		BREAK
		// Dominator (2-Door).
		CASE DOMINATOR
			iVehicleModel = 12
		BREAK
		// Feltzer 2010 (2-Door).
		CASE FELTZER2
			iVehicleModel = 13
		BREAK
		// Penumbra (2-Door).
		CASE PENUMBRA
			iVehicleModel = 14
		BREAK
		// Phoenix (2-Door).
		CASE PHOENIX
			iVehicleModel = 15
		BREAK
		// Stinger (2-Door).
		CASE STINGER
			iVehicleModel = 16
		BREAK
		// Cargobob (Helicopter).
		CASE CARGOBOB
			iVehicleModel = 17
		BREAK
		// Maverick (Helicopter).
		CASE MAVERICK
			iVehicleModel = 18
		BREAK		
		// Picador (2-Door).
		CASE PICADOR
			iVehicleModel = 19
		BREAK
		// Tornado (2-Door).
		CASE TORNADO
			iVehicleModel = 20
		BREAK
		// Baller (4-Door).
		CASE BALLER
			iVehicleModel = 21
		BREAK
		// Dubsta 1 (4-Door).
		CASE DUBSTA
			iVehicleModel = 22
		BREAK
		// Dubsta 2 (4-Door).
		CASE DUBSTA2
			iVehicleModel = 23
		BREAK
		// FQ2 (4-Door).
		CASE FQ2
			iVehicleModel = 24
		BREAK
		// Granger (4-Door).
		CASE GRANGER
			iVehicleModel = 25
		BREAK
		// Mesa (4-Door).
		CASE MESA
			iVehicleModel = 26
		BREAK
		// Radius (4-Door).
		CASE RADI
			iVehicleModel = 27
		BREAK
		// Sadler (4-Door).
		CASE SADLER
			iVehicleModel = 28
		BREAK
		// Surfer 1 (4-Door).
		CASE SURFER
			iVehicleModel = 29
		BREAK
		// Surfer 2 (4-Door).
		CASE SURFER2
			iVehicleModel = 30
		BREAK
		// BJXL (4-Door).
		CASE BJXL
			iVehicleModel = 31
		BREAK
		// Jackal (4-Door).
		CASE JACKAL
			iVehicleModel = 32
		BREAK
		// Blazer (4-Wheeler).
		CASE BLAZER
			iVehicleModel = 33
		BREAK
		// Hexer (Motorcycle).
		CASE HEXER
			iVehicleModel = 34
		BREAK
		// Vader 1 (Motorcycle).
		CASE VADER
			iVehicleModel = 35
		BREAK
		// Vader 2 (Motorcycle).
//		CASE VADER2
//			iVehicleModel = 36
//		BREAK
		// Sanchez (Dirtbike).
		CASE SANCHEZ
			iVehicleModel = 37
		BREAK
		// Faggio (Moped).
		CASE FAGGIO2
			iVehicleModel = 38
		BREAK
		// BMX (Bicycle).
		CASE BMX
			iVehicleModel = 39
		BREAK
		// Scorcher (Bicycle).
		CASE SCORCHER
			iVehicleModel = 40
		BREAK
		// Seashark (Jetski).
		CASE SEASHARK
			iVehicleModel = 43
		BREAK
		// Cuban 800 (Plane).
		CASE CUBAN800
			iVehicleModel = 44
		BREAK
		// Stunt (Plane).
		CASE STUNT
			iVehicleModel = 45
		BREAK
		// On Foot (No Vehicle).
		CASE DUMMY_MODEL_FOR_SCRIPT
			iVehicleModel = 46
		BREAK
	ENDSWITCH
	RETURN iVehicleModel
ENDFUNC

FUNC BOOL ORR_IS_VEHICLE_QUAD(MODEL_NAMES eVehicleModel)
	SWITCH eVehicleModel
		CASE BLAZER
		CASE BLAZER2
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL ORR_IS_VEHICLE_BIKE(MODEL_NAMES eVehicleModel)
	SWITCH eVehicleModel					
		CASE SANCHEZ
		//CASE HEXER 
		//CASE PCJ
		//CASE POLICEB
		//CASE VADER
		//CASE RUFFIAN
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_31 ORR_VehicleModel_GetString(MODEL_NAMES eVehicleModel)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_VehicleModel_GetString")
	TEXT_LABEL_31 szVehicleModel
	SWITCH eVehicleModel
		// Cheetah (Sport).
		CASE CHEETAH
			szVehicleModel = "CHEETAH"
		BREAK
		// Infernus (Sport).
		CASE INFERNUS
			szVehicleModel = "INFERNUS"
		BREAK
		// JB700 (Sport).
		CASE JB700
			szVehicleModel = "JB700"
		BREAK
		// Monroe (Sport).
		CASE MONROE
			szVehicleModel = "MONROE"
		BREAK
		// Ninef 1 (Sport).
		CASE NINEF
			szVehicleModel = "NINEF"
		BREAK
		// Ninef 2 (Sport).
		CASE NINEF2
			szVehicleModel = "NINEF2"
		BREAK
		// Rapid GT 1 (Sport).
		CASE RAPIDGT
			szVehicleModel = "RAPIDGT"
		BREAK
		// Rapid GT 2 (Sport).
		CASE RAPIDGT2
			szVehicleModel = "RAPIDGT"//1624012
		BREAK
		// Z Type (Sport).
		CASE ZTYPE
			szVehicleModel = "ZTYPE"
		BREAK
		// Entity XF (Sport).
		CASE ENTITYXF
			szVehicleModel = "ENTITYXF"
		BREAK
		// BF Injection 1 (2-Door).
		CASE BFINJECTION
			szVehicleModel = "BFINJECTION"
		BREAK
		// Bison (2-Door).
		CASE BISON
			szVehicleModel = "BISON"
		BREAK
		// Dominator (2-Door).
		CASE DOMINATOR
			szVehicleModel = "DOMINATOR"
		BREAK
		// Penumbra (2-Door).
		CASE PENUMBRA
			szVehicleModel = "PENUMBRA"
		BREAK
		// Phoenix (2-Door).
		CASE PHOENIX
			szVehicleModel = "PHOENIX"
		BREAK
		// Stinger (2-Door).
		CASE STINGER
			szVehicleModel = "STINGER"
		BREAK
		// Cargobob (Helicopter).
		CASE CARGOBOB
			szVehicleModel = "CARGOBOB"
		BREAK
		// Maverick (Helicopter).
		CASE MAVERICK
			szVehicleModel = "MAVERICK"
		BREAK		
		// Picador (2-Door).
		CASE PICADOR
			szVehicleModel = "PICADOR"
		BREAK
		// Tornado (2-Door).
		CASE TORNADO
			szVehicleModel = "TORNADO"
		BREAK
		// Baller (4-Door).
		CASE BALLER
			szVehicleModel = "BALLER"
		BREAK
		// Dubsta 1 (4-Door).
		CASE DUBSTA
			szVehicleModel = "DUBSTA"
		BREAK
		// Dubsta 2 (4-Door).
		CASE DUBSTA2
			szVehicleModel = "DUBSTA2"
		BREAK
		// FQ2 (4-Door).
		CASE FQ2
			szVehicleModel = "FQ2"
		BREAK
		// Granger (4-Door).
		CASE GRANGER
			szVehicleModel = "GRANGER"
		BREAK
		// Mesa (4-Door).
		CASE MESA
			szVehicleModel = "MESA"
		BREAK
		// Radius (4-Door).
		CASE RADI
			szVehicleModel = "RADI"
		BREAK
		// Sadler (4-Door).
		CASE SADLER
			szVehicleModel = "SADLER"
		BREAK
		// Surfer 1 (4-Door).
		CASE SURFER
			szVehicleModel = "SURFER"
		BREAK
		// Surfer 2 (4-Door).
		CASE SURFER2
			szVehicleModel = "SURFER2"
		BREAK
		// BJXL (4-Door).
		CASE BJXL
			szVehicleModel = "BJXL"
		BREAK
		// Jackal (4-Door).
		CASE JACKAL
			szVehicleModel = "JACKAL"
		BREAK
		// Blazer (4-Wheeler).
		CASE BLAZER
			szVehicleModel = "BLAZER"
		BREAK
		// Hexer (Motorcycle).
		CASE HEXER
			szVehicleModel = "HEXER"
		BREAK
		// Vader 1 (Motorcycle).
		CASE VADER
			szVehicleModel = "VADER"
		BREAK
		// Vader 2 (Motorcycle).
//		CASE VADER2
//			szVehicleModel = "VADER2"
//		BREAK
		// Sanchez (Dirtbike).
		CASE SANCHEZ
			szVehicleModel = "SANCHEZ"
		BREAK
		// Faggio (Moped).
		CASE FAGGIO2
			szVehicleModel = "FAGGIO2"
		BREAK
		// BMX (Bicycle).
		CASE BMX
			szVehicleModel = "BMX"
		BREAK
		// Scorcher (Bicycle).
		CASE SCORCHER
			szVehicleModel = "SCORCHER"
		BREAK
		// Seashark (Jetski).
		CASE SEASHARK
			szVehicleModel = "SEASHARK"
		BREAK
		// Cuban 800 (Plane).
		CASE CUBAN800
			szVehicleModel = "CUBAN800"
		BREAK
		// Stunt (Plane).
		CASE STUNT
			szVehicleModel = "STUNT"
		BREAK
		// On Foot (No Vehicle).
		CASE DUMMY_MODEL_FOR_SCRIPT
			szVehicleModel = "DUMMY_MODEL_FOR_SCRIPT"
		BREAK
	ENDSWITCH
	RETURN szVehicleModel
ENDFUNC




// -----------------------------------
// MISC PROCS/FUNCTION
// -----------------------------------
// TODO: Organize these into proper categories.

FUNC TEXT_LABEL_31 ORR_RacerName_GetString(INT nLookUp)
	TEXT_LABEL_31 szRacerName
	SWITCH (nLookUp)
		CASE 0
			szRacerName = "Michael Bagley"
		BREAK
		CASE 1
			szRacerName = "Alan Blaine"
		BREAK
		CASE 2
			szRacerName = "Chris Bourassa"
		BREAK
		CASE 3
			szRacerName = "Jason Umbriet"
		BREAK		
		CASE 4
			szRacerName = "John Diaz"
		BREAK
		CASE 5
			szRacerName = "Goeffry Show"
		BREAK
		CASE 6
			szRacerName = "DJ Jones"
		BREAK
		CASE 7
			szRacerName = "Ghyan Koehne"
		BREAK
		CASE 8
			szRacerName = "Steven Messinger"
		BREAK
		CASE 9
			szRacerName = "Silas Morse"
		BREAK
		CASE 10
			szRacerName = "Ryan Paradis"
		BREAK
		CASE 11
			szRacerName = "Yomal Perera"
		BREAK
		CASE 12
			szRacerName = "Troy Schram"
		BREAK
		CASE 13
			szRacerName = "John Sripan"
		BREAK
		CASE 14
			szRacerName = "David Stinchcomb"
		BREAK
		CASE 15
			szRacerName = "Stephen Russo"		
		BREAK
		CASE 16
			szRacerName = "Adrian Castaneda"
		BREAK
		CASE 17
			szRacerName = "David Kunkler"
		BREAK
		CASE 18
			szRacerName = "Steve Martin"
		BREAK
		CASE 19
			szRacerName = "John Ricchio"
		BREAK
		CASE 20
			szRacerName = "Eric Smith"
		BREAK
		CASE 21
			szRacerName = "Ted Carson"
		BREAK
		CASE 22
			szRacerName = "Michael Currington"
		BREAK
		CASE 23
			szRacerName = "Fredrik Farnstrom"
		BREAK
		CASE 24
			szRacerName = "Andrew Gardner"
		BREAK
		CASE 25
			szRacerName = "Alan Goykhman"
		BREAK
		CASE 26
			szRacerName = "Jason Jurecka"
		BREAK
		CASE 27
			szRacerName = "Jonathan Martin"
		BREAK
		CASE 28
			szRacerName = "Bryan Musson"
		BREAK
		CASE 29
			szRacerName = "Robert Percival"
		BREAK
		CASE 30
			szRacerName = "C. Rakowsky"
		BREAK
		CASE 31
			szRacerName = "Ryan Satrappe"
		BREAK
		CASE 32
			szRacerName = "Tom Shepherd"
		BREAK
		CASE 33
			szRacerName = "Aaron Robuck"
		BREAK
		DEFAULT
			szRacerName = "Racer"
		BREAK
	ENDSWITCH
	RETURN szRacerName
ENDFUNC

PROC ORR_RacerName_Populate(TEXT_LABEL_31& szRacerName[])
	INT i, j, iLimit
	INT tempNumbers[10]
	REPEAT COUNT_OF(szRacerName) i
		//szRacerName[i] = ORR_RacerName_GetString()
		
		tempNumbers[i]=GET_RANDOM_INT_IN_RANGE(0, 34)
		/*PRINTSTRING("Picking a random number for slot ")
		PRINTINT(i)
		PRINTNL()*/
		IF (i > 0)
			j = i - 1
			iLimit = 0
			WHILE (j >= 0)
			/*PRINTINT(i)
			PRINTSTRING(" value of ")
			PRINTINT(tempNumbers[i])
			PRINTSTRING(" vs. ")
			PRINTINT(j)
			PRINTSTRING(" value of ")
			PRINTINT(tempNumbers[j])
			PRINTNL()*/
				IF tempNumbers[i] = tempNumbers[j] //ARE_STRINGS_EQUAL(szRacerName[i], szRacerName[j])
					IF (iLimit < 100)
						//szRacerName[i] = ORR_RacerName_GetString()
						tempNumbers[i]=GET_RANDOM_INT_IN_RANGE(0, 34)
						++iLimit
					ELSE
						--j
					ENDIF
				ELSE
					iLimit = 0
					--j
				ENDIF
			ENDWHILE
		ENDIF
	ENDREPEAT
	i=0
	REPEAT COUNT_OF(szRacerName) i
		szRacerName[i]=ORR_RacerName_GetString(tempNumbers[i])
	ENDREPEAT
	
ENDPROC

FUNC TEXT_LABEL ORR_Race_Racer_Format_Time(FLOAT fUnformattedTime)
	TEXT_LABEL sFormattedTime = "0:00\:000"
	
	INT iNumMinutes = 0
	INT iNumSeconds = 0
	INT iNumMilliSeconds = 0
	FLOAT fMilliseconds = 0
	WHILE fUnformattedTime >= 60.0
		iNumMinutes++
		fUnformattedTime -= 60.0
	ENDWHILE
	iNumSeconds = FLOOR(fUnformattedTime)
	fMilliseconds = (fUnformattedTime - iNumSeconds)*1000
	iNumMilliSeconds = FLOOR(fMilliseconds)
				
	sFormattedTime = iNumMinutes
	sFormattedTime += "'"
	IF iNumSeconds < 10
		sFormattedTime += "0"
	ENDIF
	sFormattedTime += iNumSeconds
	sFormattedTime += "\""		
	sFormattedTime += iNumMilliSeconds
	
	RETURN sFormattedTime
ENDFUNC

PROC ORR_Race_Racer_Update_Rubberband_Speed(ORR_RACE_STRUCT& Race, INT iRacerIndex)	
	IF iRacerIndex < 0 
	OR iRacerIndex > COUNT_OF(Race.Racer)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Race_Racer_Update_Rubberband_Cruise_Speed: Bad racer index!")
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerIndex].Vehicle)
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[iRacerIndex].Vehicle)
			SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
				CASE OFFROAD_RACE_CANYON_CLIFFS
				
					BREAK
				CASE OFFROAD_RACE_RIDGE_RUN
//					IF GET_VEHICLE_WAYPOINT_PROGRESS(Race.Racer[iRacerIndex].Vehicle) < 170
//						SET_ENTITY_MAX_SPEED(Race.Racer[iRacerIndex].Vehicle, 15.0)
//						SET_DRIVE_TASK_CRUISE_SPEED(Race.Racer[iRacerIndex].Driver, 15.0)
//						EXIT
//					ELIF GET_VEHICLE_WAYPOINT_PROGRESS(Race.Racer[iRacerIndex].Vehicle) > 170 AND GET_VEHICLE_WAYPOINT_PROGRESS(Race.Racer[iRacerIndex].Vehicle) < 260
//						SET_ENTITY_MAX_SPEED(Race.Racer[iRacerIndex].Vehicle, 20.0)
//						SET_DRIVE_TASK_CRUISE_SPEED(Race.Racer[iRacerIndex].Driver, 20.0)
//						EXIT
//					ELSE
//						SET_ENTITY_MAX_SPEED(Race.Racer[iRacerIndex].Vehicle, GET_VEHICLE_ESTIMATED_MAX_SPEED(Race.Racer[iRacerIndex].Vehicle))
//						SET_DRIVE_TASK_CRUISE_SPEED(Race.Racer[iRacerIndex].Driver, GET_VEHICLE_ESTIMATED_MAX_SPEED(Race.Racer[iRacerIndex].Vehicle))
//					ENDIF
					BREAK
				CASE OFFROAD_RACE_VALLEY_TRAIL
					
					BREAK
				CASE OFFROAD_RACE_LAKESIDE_SPLASH
					
					BREAK
				CASE OFFROAD_RACE_ECO_FRIENDLY
					
					BREAK
				CASE OFFROAD_RACE_MINEWARD_SPIRAL
					
					BREAK
				DEFAULT
					CPRINTLN(DEBUG_OR_RACES, "Didn't update rubberbanding for race #", ORR_Master.iRaceCur)
					BREAK
			ENDSWITCH
		ELSE
			CPRINTLN(DEBUG_OR_RACES, "Waypoint playback going not on... for racer #", iRacerIndex)
		ENDIF
	ENDIF
			

	IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerIndex].Driver)                   
        IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerIndex].Vehicle)
			IF IS_PED_IN_VEHICLE(Race.Racer[iRacerIndex].Driver, Race.Racer[iRacerIndex].Vehicle)
				FLOAT aiSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED(Race.Racer[iRacerIndex].Vehicle) // TO_FLOAT(GET_RANDOM_INT_IN_RANGE(60,80))			    
			    IF Race.Racer[0].iGateCur = Race.Racer[iRacerIndex].iGateCur		   
					aiSpeed*=0.9
//					PRINTSTRING("racer is on same gate as player, keep speed the same")
			    ELIF Race.Racer[iRacerIndex].iGateCur > Race.Racer[0].iGateCur //ai is too far ahead lets slow them down
			        aiSpeed*=0.69
//					PRINTSTRING("racer is ahead of player, reducing speed")
				ELSE
					aiSpeed*=1.0
//					PRINTSTRING("racer is behind player, setting to 1.25 max vehicle speed")
			    ENDIF
//			    TEXT_LABEL_3 txt=CEIL(aiSpeed)
//			    PRINTSTRING("updating the ai to SPEED of : ")
//				PRINTSTRING(txt)
//				PRINTNL()

				IF IS_PED_SITTING_IN_VEHICLE(Race.Racer[iRacerIndex].Driver, Race.Racer[iRacerIndex].Vehicle)
					IF IS_VEHICLE_SEAT_FREE(Race.Racer[iRacerIndex].Vehicle, VS_DRIVER)
						SET_PED_INTO_VEHICLE(Race.Racer[iRacerIndex].Driver, Race.Racer[iRacerIndex].Vehicle, VS_DRIVER)
					ENDIF
					SET_DRIVE_TASK_CRUISE_SPEED(Race.Racer[iRacerIndex].Driver, aiSpeed)
				ENDIF
			ENDIF
        ENDIF
		
/*		// Check if the race is Triathlon and on foot or water.
		IF (ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON)
			IF NOT ( Race.sGate[Race.Racer[iRacerIndex].iGateCur].eChkpntType = MARKER_ARROW )
			
				FLOAT fRacerSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(Race.Racer[iRacerIndex].Driver)
				
			    // If the player is ahead by a checkpoint lets speed the racer up a bit, or slow him down if the opposite happens.
			    IF Race.Racer[0].iGateCur > Race.Racer[iRacerIndex].iGateCur
			        fRacerSpeed *= 1.1
			    ELIF Race.Racer[iRacerIndex].iGateCur > Race.Racer[0].iGateCur
			        fRacerSpeed *= 0.9
			    ENDIF
				
				FLOAT fSpeedMin = TO_FLOAT(ENUM_TO_INT(MS_ON_FOOT_WALK))+((TO_FLOAT(ENUM_TO_INT(MS_ON_FOOT_RUN))-TO_FLOAT(ENUM_TO_INT(MS_ON_FOOT_WALK)))/2)
				
				IF fRacerSpeed < fSpeedMin
					fRacerSpeed = fSpeedMin
				ELIF fRacerSpeed >  ENUM_TO_INT(MS_ON_FOOT_SPRINT)
					fRacerSpeed = TO_FLOAT(ENUM_TO_INT(MS_ON_FOOT_SPRINT))
				ENDIF
				
				SET_PED_DESIRED_MOVE_BLEND_RATIO(Race.Racer[iRacerIndex].Driver, fRacerSpeed)
			
			ENDIF
		ENDIF
*/		
    ENDIF
ENDPROC

PROC ORR_Race_Racer_Task_Go_To_Next_Gate(ORR_RACE_STRUCT& Race, PED_INDEX iDriver, VEHICLE_INDEX iVehicle,	VECTOR vGateGoto, ORR_RACE_CHECKPOINT_TYPE eChkpntType, INT iRacerIndex)	
// Task ai to go to next gate position.
	IF NOT IS_ENTITY_DEAD(iDriver)
		IF (ORR_Master.eRaceType = ORR_RACE_TYPE_OFFROAD)
			//CLEAR_PED_TASKS(Race.Racer[i].Driver)			
			IF NOT IS_ENTITY_DEAD(iVehicle)
			
				TASK_VEHICLE_DRIVE_TO_COORD(iDriver, iVehicle, vGateGoto, GET_VEHICLE_ESTIMATED_MAX_SPEED(iVehicle), 
					DRIVINGSTYLE_RACING, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS_RECKLESS, 5.0, -1 )												
				
			ENDIF
		ELIF (ORR_Master.eRaceType = ORR_RACE_TYPE_TRIATHLON)
			eChkpntType = eChkpntType //may need for later
			TASK_TRI_RACER_TO_FOLLOW_RACE_COURSE(Race, vGateGoto, iRacerIndex)
		ENDIF
	ENDIF
ENDPROC
					
PROC ORR_SAFELY_SET_SCENARIO_GROUP_ENABLED(STRING sThisGroupName)
	IF NOT IS_STRING_NULL_OR_EMPTY(sThisGroupName)
		IF DOES_SCENARIO_GROUP_EXIST(sThisGroupName)
			IF NOT IS_SCENARIO_GROUP_ENABLED(sThisGroupName)
				SET_SCENARIO_GROUP_ENABLED(sThisGroupName, TRUE)
				SET_EXCLUSIVE_SCENARIO_GROUP(sThisGroupName)
				CDEBUG1LN(DEBUG_OR_RACES, "Scenario group ", sThisGroupName, " has been set exclusive")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_OR_RACES, "Scenario group ", sThisGroupName, " does not exist")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_OR_RACES, "Scenario group string is null or empty!")
	ENDIF
ENDPROC

PROC ORR_Race_Init_Offroad_Intro_Cam(CAMERA_INDEX& introCamIndex, VECTOR vPlayerVehInit, FLOAT fPlayerVehInit)
	introCamIndex = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")			
			
	VECTOR vStartCam
	vStartCam = vPlayerVehInit
	vStartCam.z += 5.0
	VECTOR vStartCamLookAt			
	vStartCamLookAt = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartCam, fPlayerVehInit, <<0, 10, 0>>)
	
	SET_CAM_PARAMS(introCamIndex, vStartCam, <<0,0,0>>, GET_GAMEPLAY_CAM_FOV())			
	POINT_CAM_AT_COORD(introCamIndex, vStartCamLookAt)
	//SET_CAM_ACTIVE_WITH_INTERP(Intro_Cam_Start, Intro_Cam_Start, 10000, GRAPH_TYPE_LINEAR)
	SET_CAM_ACTIVE(introCamIndex, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
ENDPROC

//Something is jacked here... Not sure what
FUNC BOOL IS_RACER_PAST_GATE(PED_INDEX pedRacer, ORR_GATE_STRUCT& sGate)
	
	VECTOR vDriverToGate
	VECTOR vDriverForward
	IF NOT IS_ENTITY_DEAD(pedRacer)
	
		//Get Vector from Driver to the Gate
		vDriverToGate = sGate.vPos - GET_ENTITY_COORDS(pedRacer)
	
		//Get his forward
		vDriverForward = GET_ENTITY_FORWARD_VECTOR(pedRacer)
		//Dot the 2
		IF DOT_PRODUCT_XY(vDriverToGate, vDriverForward) < 0
			CDEBUG1LN(DEBUG_OR_RACES, "[GATE] Driver is past gate, and didn't hit it.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
PROC AWARD_PLAYER_1ST_PLACE_CASH(ORR_RACE_STRUCT& Race)
	IF Race.Racer[0].iRank = 1
	//	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	//	CDEBUG1LN(DEBUG_OR_RACES, "[MONEY] Showing Cash $$$$$$$$$$$$$$$$$$$$$$$$$$$")
	
		IF NOT IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, ORR_AWARD_CASH)
			//Bank Account
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL	DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL, BAA_CREDIT, BAAC_RACES, ORR_iCashAwardFor1stPlace)			BREAK
				CASE CHAR_FRANKLIN	DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN, BAA_CREDIT, BAAC_RACES, ORR_iCashAwardFor1stPlace)		BREAK
				CASE CHAR_TREVOR	DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR, BAA_CREDIT, BAAC_RACES, ORR_iCashAwardFor1stPlace)			BREAK
					
			ENDSWITCH
			SET_BITMASK_AS_ENUM(iSPRGeneralBits, ORR_AWARD_CASH)
			CDEBUG1LN(DEBUG_OR_RACES, "[MONEY] ORR_RACE_UPDATE - Player given, ", ORR_iCashAwardFor1stPlace)
		ENDIF
		
	ENDIF
ENDPROC


PROC OOR_RACE_MANAGE_CHECKPOINT_ALPHAS(ORR_GATE_STRUCT& GateCur, ORR_GATE_STRUCT& GateNxt)
	INT iRed, iGreen, iBlue, iAlpha

	IF GateCur.Chkpnt != NULL
		HUD_COLOURS eColor = MG_GET_RACE_CHECKPOINT_DEFAULT_COLOUR()
		GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(eColor), iRed, iGreen, iBlue, iAlpha)
		SET_CHECKPOINT_RGBA(GateCur.Chkpnt, iRed, iGreen, iBlue, MG_GET_CHECKPOINT_ALPHA(GateCur.vPos, 220, 255))
		GET_HUD_COLOUR(HUD_COLOUR_NORTH_BLUE, iRed, iGreen, iBlue, iAlpha)
		SET_CHECKPOINT_RGBA2(GateCur.Chkpnt, iRed, iGreen, iBlue, MG_GET_CHECKPOINT_ALPHA(GateCur.vPos, 70, 240))
		
	ENDIF
	IF GateNxt.Chkpnt != NULL
		HUD_COLOURS eColor = MG_GET_RACE_CHECKPOINT_DEFAULT_COLOUR()
		GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(eColor), iRed, iGreen, iBlue, iAlpha)
		SET_CHECKPOINT_RGBA(GateNxt.Chkpnt, iRed, iGreen, iBlue, MG_GET_CHECKPOINT_ALPHA(GateNxt.vPos, 220, 255))
		GET_HUD_COLOUR(HUD_COLOUR_NORTH_BLUE, iRed, iGreen, iBlue, iAlpha)
		SET_CHECKPOINT_RGBA2(GateNxt.Chkpnt, iRed, iGreen, iBlue, MG_GET_CHECKPOINT_ALPHA(GateNxt.vPos, 70, 240))
	ENDIF
ENDPROC


// -----------------------------------
// TEST WORKSPACE
// -----------------------------------



// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!
