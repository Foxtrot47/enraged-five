// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	ORR_Gate.sch
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Single Player Races - Gate procs/functions file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "ORR_Head.sch"
USING "ORR_Helpers.sch"
USING "stunt_plane_public.sch"
USING "chase_hint_cam.sch"
USING "script_oddjob_funcs.sch"

// -----------------------------------
// SETUP PROCS/FUNCTIONS
// -----------------------------------

PROC ORR_Gate_Init(ORR_GATE_STRUCT& sGate)
//	CDEBUG1LN(DEBUG_OR_RACES, "ORR_Gate_Init")
	sGate.vPos = ORR_Master.vDefRcrPos
	sGate.fRadius = ORR_GATE_CHKPNT_SCL
	sGate.eChkpntType = ORR_CHKPT_OFFROAD_DEFAULT
	sGate.fHeightCheck = ORR_GATE_HEIGHT_CHECK 					// To Fix Bug # 666112 - DS
	sGate.fHeading = 0.0
ENDPROC

PROC ORR_Gate_Setup(ORR_GATE_STRUCT& sGate, VECTOR vPos, FLOAT fHeading, FLOAT fRadius, ORR_RACE_CHECKPOINT_TYPE eChkpntType, ORR_RACE_STUNT_GATE_ENUM eStuntType = ORR_RACE_STUNT_GATE_NORMAL)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Gate_Setup")
	sGate.vPos = vPos
	sGate.fRadius = fRadius
	sGate.eChkpntType = eChkpntType
	sGate.eStuntType = eStuntType	
	sGate.fHeading = fHeading
ENDPROC

PROC ORR_Gate_Set_Condition_Flag(ORR_GATE_STRUCT& sGate, ENUM_TO_INT iFlag)	
	SET_BITMASK_AS_ENUM(sGate.iGateFlags, iFlag)	
ENDPROC

PROC ORR_Gate_Clear_Condition_Flag(ORR_GATE_STRUCT& sGate, ENUM_TO_INT iFlag)	
	CLEAR_BITMASK_AS_ENUM(sGate.iGateFlags, iFlag)	
ENDPROC

FUNC BOOL ORR_Gate_Check_Condition_Flag(ORR_GATE_STRUCT& sGate, ENUM_TO_INT iFlag)	
	RETURN IS_BITMASK_AS_ENUM_SET(sGate.iGateFlags, iFlag)
ENDFUNC

// -----------------------------------
// CREATION PROCS/FUNCTIONS
// -----------------------------------

FUNC BOOL ORR_Gate_Blip_Create(ORR_GATE_STRUCT& sGate, BLIP_SPRITE eBlipSprite = RADAR_TRACE_INVALID, FLOAT fScale = 1.0, INT iCurGate = -1, INT iNumGates = -1)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Gate_Blip_Create")
	IF NOT ORR_Blip_Coord_Create(sGate.Blip, sGate.vPos, eBlipSprite, fScale, iCurGate, iNumGates, sGate.eStuntType)
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Gate_Blip_Create: Failed to create gate blip!")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC ORR_Gate_Blip_Destroy(ORR_GATE_STRUCT& sGate)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Gate_Blip_Destroy")
	ORR_Blip_Destroy(sGate.Blip)
ENDPROC

FUNC BOOL ORR_Gate_Chkpnt_Create(VECTOR vPrevGate, ORR_GATE_STRUCT& sGate, VECTOR vPointAt, FLOAT fScale = ORR_GATE_CHKPNT_SCL)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Gate_Chkpnt_Create")
	//IF NOT ORR_Chkpnt_Create(sGate.Chkpnt, sGate.eChkpntType, sGate.vPos, vPointAt, fScale)
	IF NOT ORR_Chkpnt_Create(vPrevGate, sGate, vPointAt, fScale)	
		CDEBUG1LN(DEBUG_OR_RACES, "ORR_Gate_Chkpnt_Create: Failed to create gate checkpoint!")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

// DEBUG DRAW FUNCTION FOR GATE CLEARING (STUNT PLANE RACES)
PROC DRAW_STUNT_TARGET(VECTOR vCenter, VECTOR vOrtho, FLOAT fSmallRadius, FLOAT fLargeRadius)
	SCRIPT_ASSERT("Running Draw Stunt Target")

	VECTOR vVertices[16]
	VECTOR vRight = CROSS_PRODUCT(vOrtho, <<0,0,1>>)
	VECTOR vSmallProj = fSmallRadius * vRight
	VECTOR vLargeProj = fLargeRadius * vRight
	INT iInnerVert, iOuterVert
	INT i
	
	// Determine horizontal crosshair verts.
	vVertices[ 0] = vCenter + vSmallProj
	vVertices[ 4] = vCenter - vSmallProj
	vVertices[ 8] = vCenter + vLargeProj
	vVertices[12] = vCenter - vLargeProj
	
	VECTOR vUp = CROSS_PRODUCT(vOrtho, vRight)
	
	vSmallProj = fSmallRadius * vUp
	vLargeProj = fLargeRadius * vUp
	
	// Determine vertical crosshair verts.
	vVertices[ 2] = vCenter + vSmallProj
	vVertices[ 6] = vCenter - vSmallProj
	vVertices[10] = vCenter + vLargeProj
	vVertices[14] = vCenter - vLargeProj
	
	vSmallProj = GET_VECTOR_OF_LENGTH((vRight + vUp) / 2.0, fSmallRadius)
	vLargeProj = GET_VECTOR_OF_LENGTH((vRight + vUp) / 2.0, fLargeRadius)
	
	// Determine vertical vert set 1.
	vVertices[ 1] = vCenter + vSmallProj
	vVertices[ 5] = vCenter - vSmallProj
	vVertices[ 9] = vCenter + vLargeProj
	vVertices[13] = vCenter - vLargeProj
	
	vSmallProj -= vRight * (fSmallRadius * 1.414)
	vLargeProj -= vRight * (fLargeRadius * 1.414)
	
	// Determine vertical vert set 2.
	vVertices[ 3] = vCenter + vSmallProj
	vVertices[ 7] = vCenter - vSmallProj
	vVertices[11] = vCenter + vLargeProj
	vVertices[15] = vCenter - vLargeProj
	
	// Draw the shape.
	REPEAT 8 i
		// Draw the radial line.
		DRAW_DEBUG_LINE(vCenter, vVertices[i+8], 127, 0, 0, 255)
		
		IF i = 7
			iInnerVert = 0
			iOuterVert = 8
		ELSE
			iInnerVert = i + 1
			iOuterVert = i + 9
		ENDIF
		
		// Draw the inner & outer lines.
		DRAW_DEBUG_LINE(vVertices[i  ], vVertices[iInnerVert], 0, 200, 0, 255)
		DRAW_DEBUG_LINE(vVertices[i+8], vVertices[iOuterVert], 0, 0, 200, 255)
	ENDREPEAT
ENDPROC

PROC DRAW_ENTITY_AXES(ENTITY_INDEX myIndex, FLOAT fscale)

VECTOR vFront, vSide, vUp, vPos

GET_ENTITY_MATRIX(myIndex, vFront, vSide, vUp, vPos)

DRAW_DEBUG_LINE(vPos, vPos + fScale * vSide, 255, 0, 0, 255)
DRAW_DEBUG_LINE(vPos, vPos + fScale * vFront, 255, 0, 0, 255)
DRAW_DEBUG_LINE(vPos, vPos + fScale * vUp, 255, 0, 0, 255)
ENDPROC

// -----------------------------------
// HELPER PROCS/FUNCTIONS
// -----------------------------------

FUNC ORR_RACE_GATE_STATUS ORR_Gate_Check_Pass(ORR_GATE_STRUCT& GateCur, PED_INDEX Driver)
	CONST_FLOAT fInnerRadius	110.0 //squared
	CONST_FLOAT fOutterRadius	900.0 //squared
	
	FLOAT fDist
	VECTOR vVecTo, vNormal
	FLOAT fGateSizeModifier = 5.0
	
	IF GateCur.fRadius < 15.0
		CPRINTLN( DEBUG_OR_RACES, "Checkpoint is below 15.0 setting to ", GateCur.fRadius )
		fGateSizeModifier = GateCur.fRadius
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(Driver)
		//B* 1197044 - widening gates for OFFROAD AI / Or if driver has passed gate, set his next one.
		// Changing this to be a half space test
		IF (Driver != PLAYER_PED_ID())
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( Driver, GateCur.vPos ) <= FMMC_CHECKPOINT_SIZE * fGateSizeModifier
				IF GateCur.eChkpntType = ORR_CHKPT_OFFROAD_DEFAULT
					OR GateCur.eChkpntType = ORR_CHKPT_OFFROAD_FINISH
					vVecTo = GET_ENTITY_COORDS( Driver ) - GateCur.vPos
					vNormal = <<-SIN(GateCur.fHeading), COS(GateCur.fHeading), 0.0>>
					//vNormal = NORMALISE_VECTOR( vNormal )
					fDist = DOT_PRODUCT( vVecTo, vNormal )
					IF fDist >= 0
					//IF IS_ENTITY_AT_COORD(Driver, GateCur.vPos, <<FMMC_CHECKPOINT_SIZE +1, FMMC_CHECKPOINT_SIZE+1, FMMC_CHECKPOINT_SIZE>>)
					//OR IS_RACER_PAST_GATE(Driver, GateCur)
						RETURN ORR_RACE_GATE_STATUS_PASS
					ENDIF

				ENDIF
			ENDIF
		ENDIF
		
		VECTOR vCheckpointDimensions = <<FMMC_CHECKPOINT_SIZE*2.3, FMMC_CHECKPOINT_SIZE*2.3, FMMC_CHECKPOINT_SIZE>>
		
		//IF GateCur.fRadius > 15.0
		//	vCheckpointDimensions = <<FMMC_CHECKPOINT_SIZE*2.0, FMMC_CHECKPOINT_SIZE*2.0, FMMC_CHECKPOINT_SIZE*2.0>>
		//ENDIF
		
		IF Driver = PLAYER_PED_ID() AND IS_PED_IN_ANY_VEHICLE(Driver, FALSE) AND IS_ENTITY_AT_COORD(Driver, GateCur.vPos, vCheckpointDimensions)  // To Fix Bug # 666112 - DS
			IF GateCur.eChkpntType = ORR_CHKPT_OFFROAD_DEFAULT
				OR GateCur.eChkpntType = ORR_CHKPT_OFFROAD_FINISH
				vVecTo = GET_ENTITY_COORDS( Driver ) - GateCur.vPos
				vNormal = <<-SIN(GateCur.fHeading), COS(GateCur.fHeading), 0.0>>
				//vNormal = NORMALISE_VECTOR( vNormal )
				fDist = DOT_PRODUCT( vVecTo, vNormal )
				IF fDist >= 0
				//IF IS_ENTITY_AT_COORD(Driver, GateCur.vPos, <<FMMC_CHECKPOINT_SIZE +1, FMMC_CHECKPOINT_SIZE+1, FMMC_CHECKPOINT_SIZE>>)
				//OR IS_RACER_PAST_GATE(Driver, GateCur)
					PLAY_SOUND_FRONTEND(-1, STUNT_PLANE_SFX_CHECKPOINT_CLEAR, "HUD_MINI_GAME_SOUNDSET")
				
					RETURN ORR_RACE_GATE_STATUS_PASS
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	
	RETURN ORR_RACE_GATE_STATUS_INVALID
ENDFUNC

FUNC BOOL ORR_Gate_Check_Miss(ORR_GATE_STRUCT& GateCur, ORR_GATE_STRUCT& GateNxt, PED_INDEX Driver)
	//CDEBUG1LN(DEBUG_OR_RACES, "ORR_Gate_Check_Miss")
	IF NOT ARE_VECTORS_ALMOST_EQUAL(GateCur.vPos, GateNxt.vPos)
		FLOAT fRadius = GateCur.fRadius * ORR_GATE_MISS_DIST_SCL
		IF IS_ENTITY_AT_COORD(Driver, GateCur.vPos, <<fRadius,fRadius,fRadius>>)
			//VECTOR vDrvVec = GET_ENTITY_COORDS(Driver) - GateCur.vPos
			VECTOR vGateVec = NORMALISE_VECTOR(GateNxt.vPos - GateCur.vPos)
			
			// subtract this
			IF DOT_PRODUCT(GET_ENTITY_COORDS(Driver), vGateVec) - DOT_PRODUCT(GateCur.vPos, vGateVec) > 15.0
			//IF (DOT_PRODUCT(vDrvVec, vGateVec) > 15.0)
				// do gate miss audio here:
				PLAY_SOUND_FRONTEND(-1, STUNT_PLANE_SFX_CHECKPOINT_MISS, "HUD_MINI_GAME_SOUNDSET")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ORR_Race_Racer_Get_CheckGround_For_AutoPilot(ORR_RACE_STRUCT& Race, ORR_RACER_STRUCT& Racer)
	SWITCH ORR_Master.eRaceType
		CASE ORR_RACE_TYPE_PLANE
			IF Racer.Driver = PLAYER_PED_ID()
			AND Racer.iGateCur != -1
				PRINTSTRING("ORR_Race_Racer_Get_CheckGround_For_AutoPilot: ignore ground is TRUE for gate: ")
				PRINTINT(Racer.iGateCur)
				PRINTNL()
				RETURN ORR_Gate_Check_Condition_Flag(Race.sGate[Race.Racer[0].iGateCur], ORR_RACE_GATE_FLAG_AP_IGNORE_GROUND)
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC


