// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	Offroad_Helpers.sch
//		AUTHOR			:	Asa Dang and Barton Slade
//		DESCRIPTION		:	Offroad Races - Helper procs/functions file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "orr_head.sch"
USING "rgeneral_include.sch"
USING "shared_hud_displays.sch"
USING "vehicle_gen_public.sch"

CONST_FLOAT OFFROAD_Z_FALL_TOLERANCE 20.0

VEHICLE_INDEX playerVeh, lastPlayerVeh, donorVeh
PED_INDEX donorPed
CAMERA_INDEX cam1wide, cam2wide, cam3wide, cam4wide, cam5wide, cam6wide, cam1swap, cam2swap, cam3swap, cam4swap, camTransition, camTransition2 // cam3close, cam4close, 
CAMERA_INDEX iSPRStuntFinishCam
BOOL bCamerasMade = FALSE
BOOL bSwapCamsMade = FALSE
BOOL bMakeSwapCams = FALSE
BOOL bVehicleSettings = FALSE
BOOL bRagdollCheck = FALSE

INT iCrashAudioBits
//INT iRankUpAudioBits
//INT iRankDnAudioBits
INT iORCutsceneBits
INT iVehSettingBits
INT iCutRecording[6]
INT iCutInterpDur
VECTOR vCutsceneCamDest

ENUM CrashMessageEnum
	CrashMessage0 = BIT0,
	CrashMessage1 = BIT1,
	CrashMessage2 = BIT2,
	CrashMessage3 = BIT3,
	CrashMessage4 = BIT4
ENDENUM

ENUM RankUpMessageEnum
	RankUpMessage0 = BIT0,
	RankUpMessage1 = BIT1,
	RankUpMessage2 = BIT2,
	RankUpMessage3 = BIT3,
	RankUpMessage4 = BIT4
ENDENUM

ENUM RankDnMessageEnum
	RankDnMessage0 = BIT0,
	RankDnMessage1 = BIT1,
	RankDnMessage2 = BIT2,
	RankDnMessage3 = BIT3,
	RankDnMessage4 = BIT4
ENDENUM

ENUM ORCutsceneEnum
	UseFullPlaVeh = BIT0,			// Use Fullsize player vehicle
	MakeFullDonor = BIT1,			// Make Fullsize donor vehicle
	UseMotoPlaVeh = BIT2,			// Use Motorcycle player vehicle
	MakeMotoDonor = BIT3,			// Make Motorcycle donor vehicle
	UseLastFullPlaVeh = BIT4,		// Use Last Fullsize Player
	MakeLastFullDonorKeep = BIT5,	// Make Last Fullsize Donor, keep Last Settings
	MakeLastFullDonor = BIT6,		// Make Last Fullsize Donor
	UseLastMotoPlaVeh = BIT7,		// Use Last Motorcycle Player
	MakeLastMotoDonorKeep = BIT8,	// Make Last Fullsize Donor, keep Last Settings
	MakeLastMotoDonor = BIT9,		// Make Last Fullsize Donor
	MakeFullDonorInvalid = BIT10,	// Make Fullsize Donor, brought invalid
	MakeMotoDonorInvalid = BIT11,	// Make Moto Donor, brought invalid
	CutsceneBit1 = BIT12,			// request donor ped model once
	CutsceneBit2 = BIT13,			// check player prerace vehicle once
	CutsceneBit3 = BIT14,			// check waypoint recordings loaded once 
	CutsceneBit4 = BIT15			// Do swap vehicle teleport once
ENDENUM

ENUM OFFR_FINISH_CUT_STATE
	OFR_FINISH_INIT = 0,
	OFR_FINISH_CREATE_CAM,
	OFR_FINISH_IDLE
ENDENUM
//OFFR_FINISH_CUT_STATE eOFRCutFinish = OFR_FINISH_INIT


structPedsForConversation myDialogueStruct

//
//		AI POST RACE
//

// ENUM for where the AI should stop
ENUM PostRaceEnum
	PostRank1 = 0,
	PostRank2,
	PostRank3,
	PostRank4,
	PostRank5,
	PostRank6,
	NUMBER_OF_POSITIONS
ENDENUM

// Post race vars
VECTOR vPostRacePos[NUMBER_OF_POSITIONS]
FLOAT fPostRaceHead[NUMBER_OF_POSITIONS]
TEXT_LABEL_63 tWaypointName
TEXT_LABEL_63 tPlayerFinishWaypoint


// Track the player's current and previous ranks.
INT iPlayerPreviousRank
INT iPlayerCurrentRank

PROC Offroad_Helpers_Init(ORR_RACE_STRUCT& Race)
	iPlayerPreviousRank = -1
	iPlayerCurrentRank 	= Race.Racer[0].iRank
	CPRINTLN(DEBUG_OR_RACES, "[Offroad_Helpers->Offroad_Helpers_Init] Set iPlayerPreviousRank to ", iPlayerPreviousRank, ", and iPlayerCurrentRank to ", iPlayerCurrentRank)
ENDPROC

PROC stop_vehicle_recordings(ORR_RACE_STRUCT& Race)
	
	INT iRacerIndex
	REPEAT ORR_RACER_MAX iRacerIndex
		IF (iRacerIndex > 0)
			IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerIndex].Vehicle)
				IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerIndex].Driver)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[iRacerIndex].Vehicle)				
						STOP_PLAYBACK_RECORDED_VEHICLE(Race.Racer[iRacerIndex].Vehicle)
					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Race.Racer[iRacerIndex].Driver, TRUE)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerIndex].Vehicle)
				IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerIndex].Driver)
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[iRacerIndex].Vehicle)
						STOP_PLAYBACK_RECORDED_VEHICLE(Race.Racer[iRacerIndex].Vehicle)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT			
ENDPROC

PROC ORR_RESTORE_OFFROAD_RACE_PATH()
	REMOVE_SCENARIO_BLOCKING_AREAS()
	RESET_SCENARIO_TYPES_ENABLED()
	RESET_EXCLUSIVE_SCENARIO_GROUP()
	// clear end roads of vehicles until end of race
//	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
//		CASE CanyonCliffs
//			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-223.42, 3948.36, 36.52>>, <<-212.37, 3818.97, 37.53>>, 50)
//			BREAK
//		CASE RidgeRun
//			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-509.125366,1987.582031,205.330505>>, <<-948.521484,2752.741455,25.351442>>, 300)
//			BREAK
//		CASE ValleyTrail
//			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-1943.822021, 4462.285645, 0>>, <<-229.078339, 4227.071777, 50>>, 250)
//			BREAK
//		CASE LakesideSplash
//			BREAK
//		CASE EcoFriendly
//			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<2242.488, 2568.146, -10>>, <<2242.488, 1568.146, 200>>, 1000)
//			BREAK
//		CASE MinewardSpiral
//			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA( <<2809.9475, 2993.6440, -9.3564>>, <<2952.9688, 2530.6360, 177.1921>>, 500.0 )
//			BREAK
//		CASE ConstructionCourse
//			BREAK
//	ENDSWITCH
ENDPROC

FUNC BOOL ORR_HAS_VEHICLE_FALLEN_TOO_FAR_FROM_RECORDING( ORR_RACE_STRUCT & Race, INT iRacer )
	IF IS_ENTITY_DEAD( Race.Racer[iRacer].Driver )
		OR IS_ENTITY_DEAD( Race.Racer[iRacer].Vehicle )
		RETURN FALSE
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS( Race.Racer[iRacer].Driver, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING ) <> PERFORMING_TASK
		RETURN FALSE
	ENDIF
	VECTOR vRacerCoords, vNextNodeCoords
	INT iWaypoint, iWaypointCount
	FLOAT fZDist
	TEXT_LABEL OffroadRecording = "Offroad_"
	SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
		CASE OFFROAD_RACE_CANYON_CLIFFS
			OffroadRecording += 1
		BREAK
		CASE OFFROAD_RACE_RIDGE_RUN
			OffroadRecording += 2
		BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			OffroadRecording += 6
		BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL
			OffroadRecording += 3
		BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			OffroadRecording += 4
		BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY
			OffroadRecording += 5
		BREAK
	ENDSWITCH
	
	IF NOT GET_IS_WAYPOINT_RECORDING_LOADED(OffroadRecording)
		RETURN FALSE
	ENDIF
	
	vRacerCoords = GET_ENTITY_COORDS( Race.Racer[iRacer].Vehicle )
	
	iWaypoint = GET_VEHICLE_WAYPOINT_TARGET_POINT( Race.Racer[iRacer].Vehicle )//GET_VEHICLE_WAYPOINT_PROGRESS( Race.Racer[iRacer].Vehicle )
	
	IF GET_IS_WAYPOINT_RECORDING_LOADED(OffroadRecording)
		WAYPOINT_RECORDING_GET_NUM_POINTS( OffroadRecording, iWaypointCount )
	ELSE
		PRINTLN("ORR_HAS_VEHICLE_FALLEN_TOO_FAR_FROM_RECORDING() Offroad recording wasn'", OffroadRecording, " waa not loaded!!!")
		SCRIPT_ASSERT("ORR_HAS_VEHICLE_FALLEN_TOO_FAR_FROM_RECORDING() Offroad recording wasn't loaded!")
	ENDIF
	
	IF iWaypoint <= 0
		OR iWaypointCount-1 = iWaypoint
		RETURN FALSE
	ENDIF
	
	WAYPOINT_RECORDING_GET_COORD( OffroadRecording, iWaypoint, vNextNodeCoords )
	
	IF vRacerCoords.z < vNextNodeCoords.z
		fZDist = vNextNodeCoords.z - vRacerCoords.z
		
		IF fZDist >= OFFROAD_Z_FALL_TOLERANCE
			CPRINTLN( DEBUG_OR_RACES, "ORR_HAS_VEHICLE_FALLEN_FROM_CLIFFS - fZDist is ", fZDist, " and racer z is ", vRacerCoords.z, " and waypoint node z is ", vNextNodeCoords.z, " and waypoint node num is ", iWaypoint, " and offset is", Race.Racer[iRacer].iCurrentWaypointNode )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ORR_UPDATE_RACER_FALLING_FROM_CLIFFS( ORR_RACE_STRUCT & Race, INT iRacer )
	IF ORR_HAS_VEHICLE_FALLEN_TOO_FAR_FROM_RECORDING( Race, iRacer )
		AND NOT Race.Racer[iRacer].bExploded
		AND (IS_VEHICLE_STOPPED( Race.Racer[iRacer].Vehicle ) OR IS_VEHICLE_ON_ALL_WHEELS( Race.Racer[iRacer].Vehicle ) )
		ADD_EXPLOSION( GET_ENTITY_COORDS( Race.Racer[iRacer].Vehicle ), EXP_TAG_CAR, 1.0 )
		
		Race.Racer[iRacer].bExploded = TRUE
	ENDIF
ENDPROC

PROC ORR_SAVE_NEARBY_VEHICLES(ORR_RACE_STRUCT & Race)
	INT iVehCount = GET_PED_NEARBY_VEHICLES( PLAYER_PED_ID(), Race.viStartingVeh )
	
	INT iIndex
	
	REPEAT iVehCount iIndex
		IF DOES_ENTITY_EXIST( Race.viStartingVeh[iIndex] )
			IF NOT IS_ENTITY_DEAD( Race.viStartingVeh[iIndex] )
				IF NOT IS_ENTITY_A_MISSION_ENTITY( Race.viStartingVeh[iIndex] )
					AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT( Race.viStartingVeh[iIndex] )
					SET_ENTITY_AS_MISSION_ENTITY( Race.viStartingVeh[iIndex] )
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC ORR_RELEASE_NEARBY_VEHICLES( ORR_RACE_STRUCT & Race )
	INT iIndex
	
	REPEAT 10 iIndex
		IF DOES_ENTITY_EXIST( Race.viStartingVeh[iIndex] )
			IF NOT IS_ENTITY_DEAD( RAce.viStartingVeh[iIndex] )
				IF IS_ENTITY_A_MISSION_ENTITY( Race.viStartingVeh[iIndex] )
					AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT( Race.viStartingVeh[iIndex] )
					SET_VEHICLE_AS_NO_LONGER_NEEDED( Race.viStartingVeh[iIndex] )
					
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC ORR_CLEAR_OFFROAD_RACE_PATH(ORR_RACE_STRUCT& Race)
	CLEAR_ANGLED_AREA_OF_VEHICLES(Race.Racer[0].vStartPos, Race.sGate[0].vPos, 50.0, TRUE)
	//DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(Race.Racer[0].vStartPos, 50.0)
	CLEAR_AREA_OF_VEHICLES(Race.Racer[0].vStartPos, 100.0, TRUE)
	
	INT iIndex
	REPEAT Race.iGateCnt iIndex
		CLEAR_AREA_OF_VEHICLES( Race.sGate[iIndex].vPos, 50.0, TRUE )//, TRUE )
	ENDREPEAT
	SET_ROADS_IN_ANGLED_AREA(Race.Racer[0].vStartPos, Race.sGate[0].vPos, 50.0, FALSE, FALSE)
	CPRINTLN( DEBUG_OR_RACES, "Clearing offroad race path..." )
	
	// clear end roads of vehicles until end of race
	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
		CASE CanyonCliffs
			SET_ROADS_IN_ANGLED_AREA(<<-223.42, 3948.36, 36.52>>, <<-212.37, 3818.97, 37.53>>, 50, FALSE, FALSE)
			CLEAR_AREA_OF_VEHICLES(<<-227.43, 3888.90, 36.41>>, 50.0, TRUE)
			CLEAR_AREA_OF_VEHICLES(<<-1938.3416, 4443.2607, 36.4759>>, 100.0, TRUE)
			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
			BREAK
		CASE RidgeRun
			SET_ROADS_IN_ANGLED_AREA(<<-509.125366,1987.582031,205.330505>>, <<-948.521484,2752.741455,25.351442>>, 300, FALSE, FALSE)
			BREAK
		CASE ValleyTrail
			SET_ROADS_IN_ANGLED_AREA(<<-1943.822021, 4462.285645, 0>>, <<-229.078339, 4227.071777, 50>>, 250, TRUE, FALSE)
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<-1943.822021, 4462.285645, 0>>, <<-229.078339, 4227.071777, 50>>, 250, TRUE)
//			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
			BREAK
		CASE LakesideSplash
			BREAK
		CASE EcoFriendly
			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_BICYCLE_MOUNTAIN", FALSE)
//			ADD_SCENARIO_BLOCKING_AREA(<< 2053.3015, 2287.5103, 92.5220 >>, << 2040.5310, 2284.3726, 93.4138 >>)
//			ADD_SCENARIO_BLOCKING_AREA(<< 2363.8264, 1844.6985, 99.0246 >>, << 2356.6707, 1846.5643, 100.5710 >>)
			SET_ROADS_IN_ANGLED_AREA(<<2242.488, 2568.146, -10>>, <<2242.488, 1568.146, 200>>, 1000, FALSE, FALSE)
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<2242.488, 2568.146, -10>>, <<2242.488, 1568.146, 200>>, 1000, TRUE)
			BREAK
		CASE MinewardSpiral
			SET_SCENARIO_GROUP_ENABLED("QUARRY", FALSE)
			SET_ROADS_IN_ANGLED_AREA(<<2809.9475, 2993.6440, -9.3564>>, <<2952.9688, 2530.6360, 177.1921>>, 500.0, TRUE, FALSE)
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<2809.9475, 2993.6440, -9.3564>>, <<2952.9688, 2530.6360, 177.1921>>, 500.0, TRUE)
//			SET_SCENARIO_GROUP_ENABLED("QUARRY", FALSE)
			BREAK
		CASE ConstructionCourse
			BREAK
	ENDSWITCH
	
ENDPROC

FUNC PED_INDEX Get_Closest_Racer_To_Player(ORR_RACE_STRUCT& Race)
	VECTOR vPlayerPos
	VECTOR vRacerPos
	INT iClosestPedIndex
	FLOAT fCurrentDistance
	FLOAT fClosestDistance
	PED_INDEX closestPedToPlayerIndex

	INT iRacerCounter
	REPEAT Race.iRacerCnt iRacerCounter
		IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerCounter].Driver)
			IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
				IF NOT (Race.Racer[iRacerCounter].Driver = Race.Racer[0].Driver)
					IF NOT IS_ENTITY_DEAD(Race.Racer[iRacerCounter].Vehicle)
						IF IS_PED_IN_VEHICLE(Race.Racer[iRacerCounter].Driver, Race.Racer[iRacerCounter].Vehicle)
							vRacerPos = GET_ENTITY_COORDS(Race.Racer[iRacerCounter].Vehicle)
						ELSE
							vRacerPos = GET_ENTITY_COORDS(Race.Racer[iRacerCounter].Driver)
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(Race.Racer[0].Driver)
						IF IS_PED_IN_VEHICLE(Race.Racer[iRacerCounter].Driver, Race.Racer[0].Vehicle)
							vPlayerPos = GET_ENTITY_COORDS(Race.Racer[0].Vehicle)
						ELSE
							vPlayerPos = GET_ENTITY_COORDS(Race.Racer[0].Driver)
						ENDIF
					ENDIF
					
					IF iRacerCounter = 1
						fClosestDistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vRacerPos)
						iClosestPedIndex = iRacerCounter
					ELSE
						fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vRacerPos)
						
						IF fCurrentDistance < fClosestDistance
							fClosestDistance = fCurrentDistance
							iClosestPedIndex = iRacerCounter
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	closestPedToPlayerIndex = Race.Racer[iClosestPedIndex].Driver
	
	RETURN closestPedToPlayerIndex
ENDFUNC

PROC FranklinOffroadSpecial(BOOL bRankedUp)

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		IF bRankedUp
			SPECIAL_ABILITY_CHARGE_SMALL(PLAYER_ID(), TRUE, TRUE)
			CDEBUG1LN(DEBUG_OR_RACES, "FranklinOffroadSpecial: franklins special incremented once")
		ENDIF
	ENDIF
ENDPROC

PROC Make_Finish_Camera(ORR_Race_Struct & Race)	
	//maybe find a point near a scene that we want, set it for each race, and have the camera point that way.
//	IF fHeadingDelta < 0
//    	vCamRot.z -= 90
//	ELSE
//    	vCamRot.z += 90
//	ENDIF
	IF NOT DOES_CAM_EXIST(iSPRStuntFinishCam)
		iSPRStuntFinishCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", Race.vFinishCamPos, Race.vFinishCamRot, Race.fFOV, FALSE)
	ENDIF
ENDPROC

//CAMERA_INDEX detachedStartCam
//VECTOR vDetachedStartPos, vDetachedStartRot, vDetachedEndPos//, vDetachedEndRot
//FLOAT fDetachedStartFOV//, fDetachedEndFOV

PROC ORR_Race_Stunt_Manage_Finish_Camera( ORR_Race_Struct& Race, VEHICLE_INDEX& vPlayerVehicle )
	IF NOT DOES_ENTITY_EXIST( vPlayerVehicle )
		EXIT
	ENDIF
	
	Make_Finish_Camera(Race)
	
	ORR_Master.camFinalLBD = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, Race.vLBDCamPos, Race.vLBDCamRot, 50 )
	ORR_Master.camFinalLBD2 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, ORR_GET_STARTING_CAM_POS(), ORR_GET_STARTING_CAM_ROT(), 50 )
	
	
	SET_CAM_ACTIVE( iSPRStuntFinishCam, TRUE )
ENDPROC

//PROC ORR_Race_Stunt_Manage_Finish_Camera(VEHICLE_INDEX& vPlayerVehicle)
//	IF NOT DOES_ENTITY_EXIST(vPlayerVehicle)
//		CPRINTLN(DEBUG_OR_RACES, "SPR_Race_Stunt_Manage_Finish_Camera: vPlayerVehicle doesnt exist, exiting")
//		PRINTNL()
//		EXIT
//	ENDIF 
//	INT iInterpDuration = 3000
//	FLOAT fInterpInSecs = TO_FLOAT(iInterpDuration/1000)
//	FLOAT fHeight, fZ
//	VECTOR vTemp
//
//	SWITCH eOFRCutFinish
//		CASE OFR_FINISH_INIT
//			IF (GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = VEHICLE_ZOOM_LEVEL_FAR
//			OR GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = VEHICLE_ZOOM_LEVEL_MEDIUM
//			OR GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = VEHICLE_ZOOM_LEVEL_NEAR)
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				
//				IF DOES_ENTITY_EXIST(vPlayerVehicle) AND NOT IS_ENTITY_DEAD(vPlayerVehicle)
//					
//					SET_ENTITY_CAN_BE_DAMAGED(vPlayerVehicle, FALSE)
//				ENDIF
//				
//				vDetachedStartPos = <<-234.2903, 3909.0745, 39.4255>>//GET_GAMEPLAY_CAM_COORD()
//				vDetachedStartRot = <<53.0105, -0.0000, 55.6845>>//GET_GAMEPLAY_CAM_ROT()
//				fDetachedStartFOV = GET_GAMEPLAY_CAM_FOV()
//				detachedStartCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vDetachedStartPos, vDetachedStartRot, fDetachedStartFOV)
//				GET_GROUND_Z_FOR_3D_COORD(vDetachedStartPos, fZ)
//				fHeight = vDetachedStartPos.z - fZ
//				
//				IF IS_VEHICLE_DRIVEABLE(vPlayerVehicle)
//					
//					vDetachedEndPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDetachedStartPos, vDetachedStartRot.z, <<0,(GET_ENTITY_SPEED(vPlayerVehicle)*fInterpInSecs)/2,0>>)
//					GET_NTH_CLOSEST_VEHICLE_NODE(vDetachedEndPos, 1, vTemp)
//					vDetachedEndPos.z = vTemp.z+2
//					GET_GROUND_Z_FOR_3D_COORD(vDetachedEndPos, fZ)
//					vDetachedEndPos.z = fZ + fHeight
////					vDetachedEndRot = vDetachedStartRot
////					fDetachedEndFOV = fDetachedStartFOV+10
//					CPRINTLN(DEBUG_OR_RACES, GET_THIS_SCRIPT_NAME(), " - GET_ENTITY_SPEED(vPlayerVehicle) = ", GET_ENTITY_SPEED(vPlayerVehicle), ", fInterpInSecs = ", fInterpInSecs)
//				ENDIF
//				Make_Finish_Camera(vPlayerVehicle)
////				iSPRStuntFinishCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vDetachedEndPos, vDetachedEndRot, fDetachedEndFOV)
//				
//				//SET_CAM_ACTIVE_WITH_INTERP(iSPRStuntFinishCam, detachedStartCam, iInterpDuration, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
//				//RENDER_SCRIPT_CAMS(TRUE,TRUE, DEFAULT, FALSE)
//				eOFRCutFinish = OFR_FINISH_IDLE
//			ENDIF
//		BREAK
//		
//		
//		CASE OFR_FINISH_IDLE
//			//do nothing
//		BREAK
//	ENDSWITCH	
//ENDPROC

PROC SETUP_POST_RACE_DATA(INT currentRace)
	TEXT_LABEL_31 waypointRec
	INT iIndex

	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, currentRace)
		CASE CanyonCliffs
			// load waypoint recording for race
			//REQUEST_WAYPOINT_RECORDING("OR_Post_1")
			
			waypointRec = "orr_race1_post_rank"
			
			REPEAT 6 iIndex
				waypointRec += iIndex+1
				IF NOT GET_IS_WAYPOINT_RECORDING_LOADED( waypointRec )
					REQUEST_WAYPOINT_RECORDING( waypointRec )
				ENDIF
				waypointRec = "orr_race1_post_rank"
			ENDREPEAT
			IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("orr_canyoncliffs_finish")
				REQUEST_WAYPOINT_RECORDING("orr_canyoncliffs_finish")
			ELSE
				CPRINTLN( DEBUG_OR_RACES, "Waypoint rec orr_canyoncliffs_finish has been loaded" )
			ENDIF
			tWaypointName = "orr_race1_post_rank"
			tPlayerFinishWaypoint = "orr_canyoncliffs_finish"
			CPRINTLN( DEBUG_OR_RACES, "Requesting waypoint rec ", tPlayerFinishWaypoint )
			// Set finish positions/headings for race
			vPostRacePos[PostRank1] 	= 	<<-212.7521, 3803.0120, 38.3336>>
			fPostRaceHead[PostRank1]	=	145.3736
			vPostRacePos[PostRank2] 	= 	<<-213.8073, 3811.7708, 37.8089>>
			fPostRaceHead[PostRank2]	=	144.8253
			vPostRacePos[PostRank3] 	= 	<<-214.4785, 3814.8494, 37.6728>>
			fPostRaceHead[PostRank3]	=	142.2629
			vPostRacePos[PostRank4] 	= 	<<-215.5335, 3817.4861, 37.5397>>
			fPostRaceHead[PostRank4]	=	143.4529
			vPostRacePos[PostRank5] 	= 	<<-216.7162, 3819.5698, 37.3961>>
			fPostRaceHead[PostRank5]	=	140.5308
			vPostRacePos[PostRank6] 	= 	<<-217.8613, 3822.4009, 37.1999>>
			fPostRaceHead[PostRank6]	=	152.0320
//			vPostRacePos[PostRank7] 	= 	<< -233.19, 3801.78, 40.15 >>
//			fPostRaceHead[PostRank7]	=	230.1037
			CDEBUG1LN(DEBUG_OR_RACES, "Post race stuff requested/set for Canyon Cliffs / CASE 0")
		BREAK
		CASE RidgeRun
			// load waypoint recording for race
			//REQUEST_WAYPOINT_RECORDING("OR_Post_2")
			REQUEST_WAYPOINT_RECORDING("orr_ridgerun_finish")
			
			waypointRec = "orr_race2_post_rank"
			
			REPEAT 5 iIndex
				waypointRec += iIndex+1
				IF NOT GET_IS_WAYPOINT_RECORDING_LOADED( waypointRec )
					REQUEST_WAYPOINT_RECORDING( waypointRec )
				ENDIF
				waypointRec = "orr_race2_post_rank"
			ENDREPEAT
			
			tWaypointName = "orr_race2_post_rank"
			tPlayerFinishWaypoint = "orr_ridgerun_finish"
			// Set finish positions/headings for race
			vPostRacePos[PostRank1] 	= 	<<-1178.9644, 2589.4067, 14.3822>>
			fPostRaceHead[PostRank1]	=	166.0974
			vPostRacePos[PostRank1] 	= 	<<-1182.0496, 2589.7671, 14.4012>>
			fPostRaceHead[PostRank1]	=	170.4144
			vPostRacePos[PostRank2] 	= 	<<-1186.6792, 2588.2346, 14.3241>>
			fPostRaceHead[PostRank2]	=	171.6951
			vPostRacePos[PostRank3] 	= 	<<-1190.0995, 2586.3242, 14.2684>>
			fPostRaceHead[PostRank3]	=	178.7070
			vPostRacePos[PostRank4] 	= 	<<-1194.2039, 2584.1697, 14.1352>>
			fPostRaceHead[PostRank4]	=	181.7394
			CDEBUG1LN(DEBUG_OR_RACES, "Post race stuff requested/set for Ridge Run / CASE 1")
		BREAK
		CASE ValleyTrail
			// load waypoint recording for race
			//REQUEST_WAYPOINT_RECORDING("OR_Post_4")
			REQUEST_WAYPOINT_RECORDING("orr_valleytrail_finish")
			
			waypointRec = "orr_race3_post_rank"
			
			REPEAT 6 iIndex
				waypointRec += iIndex+1
				IF NOT GET_IS_WAYPOINT_RECORDING_LOADED( waypointRec )
					REQUEST_WAYPOINT_RECORDING( waypointRec )
				ENDIF
				waypointRec = "orr_race3_post_rank"
			ENDREPEAT
			
			tWaypointName = "orr_race3_post_rank"
			tPlayerFinishWaypoint = "orr_valleytrail_finish"
			// Set finish positions/headings for race
			vPostRacePos[PostRank1] 	= 	<< -1876.48, 4415.30, 46.76 >>
			fPostRaceHead[PostRank1]	=	242.0357
			vPostRacePos[PostRank2] 	= 	<< -1880.11, 4417.62, 46.22 >>
			fPostRaceHead[PostRank2]	=	242.0357
			vPostRacePos[PostRank3] 	= 	<< -1883.44, 4419.53, 45.73 >>
			fPostRaceHead[PostRank3]	=	242.0357
			vPostRacePos[PostRank4] 	= 	<< -1887.30, 4421.62, 45.13 >>
			fPostRaceHead[PostRank4]	=	242.0357
			vPostRacePos[PostRank5] 	= 	<< -1891.44, 4423.88, 44.30 >>
			fPostRaceHead[PostRank5]	=	242.0357
			vPostRacePos[PostRank6] 	= 	<< -1895.06, 4426.13, 43.59 >>
			fPostRaceHead[PostRank6]	=	242.0357
//			vPostRacePos[PostRank7] 	= 	<< -1899.93, 4428.57, 42.94 >>
//			fPostRaceHead[PostRank7]	=	242.0357
			CDEBUG1LN(DEBUG_OR_RACES, "Post race stuff requested/set for Valley Trail / CASE 3")
		BREAK
		CASE LakesideSplash
			// load waypoint recording for race
			//REQUEST_WAYPOINT_RECORDING("OR_Post_3")
			waypointRec = "orr_race4_post_rank"
			
			REPEAT 5 iIndex
				waypointRec += iIndex+1
				IF NOT GET_IS_WAYPOINT_RECORDING_LOADED( waypointRec )
					REQUEST_WAYPOINT_RECORDING( waypointRec )
				ENDIF
				waypointRec = "orr_race4_post_rank"
			ENDREPEAT
			
			tWaypointName = "orr_race4_post_rank"
			REQUEST_WAYPOINT_RECORDING("orr_lakesidesplash_finish")
			tPlayerFinishWaypoint = "orr_lakesidesplash_finish"
			// Set finish positions/headings for race
			vPostRacePos[PostRank1] 	= 	<< 1667.19, 4562.59, 41.60 >>
			fPostRaceHead[PostRank1]	=	270.1976
			vPostRacePos[PostRank2] 	= 	<< 1661.51, 4562.63, 42.00 >>
			fPostRaceHead[PostRank2]	=	270.1976
			vPostRacePos[PostRank3] 	= 	<< 1656.94, 4562.33, 42.12 >>
			fPostRaceHead[PostRank3]	=	270.1976
			vPostRacePos[PostRank4] 	= 	<< 1652.28, 4562.09, 42.28 >>
			fPostRaceHead[PostRank4]	=	270.1976
			vPostRacePos[PostRank5] 	= 	<< 1647.19, 4561.91, 42.56 >>
			fPostRaceHead[PostRank5]	=	270.1976
			CDEBUG1LN(DEBUG_OR_RACES, "Post race stuff requested/set for Lakeside Splash / CASE 2")
		BREAK
		CASE EcoFriendly
			// load waypoint recording for race
			//REQUEST_WAYPOINT_RECORDING("OR_Post_5")
			REQUEST_WAYPOINT_RECORDING("orr_ecofriendly_finish")
			
			waypointRec = "orr_race5_post_rank"
			
			REPEAT 6 iIndex
				waypointRec += iIndex+1
				IF NOT GET_IS_WAYPOINT_RECORDING_LOADED( waypointRec )
					REQUEST_WAYPOINT_RECORDING( waypointRec )
				ENDIF
				waypointRec = "orr_race5_post_rank"
			ENDREPEAT
			
			tWaypointName = "orr_race5_post_rank"
			tPlayerFinishWaypoint = "orr_ecofriendly_finish"
			// Set finish positions/headings for race
			vPostRacePos[PostRank1] 	= 	<< 2528.10, 1863.87, 20.54 >>
			fPostRaceHead[PostRank1]	=	180.3076
			vPostRacePos[PostRank2] 	= 	<< 2527.99, 1870.72, 20.34 >>
			fPostRaceHead[PostRank2]	=	180.3076
			vPostRacePos[PostRank3] 	= 	<< 2527.84, 1877.52, 20.17 >>
			fPostRaceHead[PostRank3]	=	180.3076
			vPostRacePos[PostRank4] 	= 	<< 2527.71, 1884.05, 20.08 >>
			fPostRaceHead[PostRank4]	=	180.3076
			vPostRacePos[PostRank5] 	= 	<< 2527.60, 1892.10, 19.98 >>
			fPostRaceHead[PostRank5]	=	180.3076
			vPostRacePos[PostRank6] 	= 	<< 2527.49, 1899.62, 19.88 >>
			fPostRaceHead[PostRank6]	=	180.3076
			CDEBUG1LN(DEBUG_OR_RACES, "Post race stuff requested/set for Eco Friendly / CASE 4")
		BREAK
		CASE MinewardSpiral
			// load waypoint recording for race
			//REQUEST_WAYPOINT_RECORDING("OR_Post_6")
			waypointRec = "orr_race6_post_rank"
			
			REPEAT 6 iIndex
				waypointRec += iIndex+1
				IF NOT GET_IS_WAYPOINT_RECORDING_LOADED( waypointRec )
					REQUEST_WAYPOINT_RECORDING( waypointRec )
				ENDIF
				waypointRec = "orr_race6_post_rank"
			ENDREPEAT
			
			tWaypointName = "orr_race6_post_rank"
			REQUEST_WAYPOINT_RECORDING("orr_minewardspiral_finish")
			//tWaypointName = "OR_Post_6"
			tPlayerFinishWaypoint = "orr_minewardspiral_finish"
			// Set finish positions/headings for race
			vPostRacePos[PostRank1] 	= 	<< 2966.50, 2890.26, 58.41 >>
			fPostRaceHead[PostRank1]	=	126.6092
			vPostRacePos[PostRank2] 	= 	<< 2969.78, 2892.81, 58.46 >>
			fPostRaceHead[PostRank2]	=	126.6092
			vPostRacePos[PostRank3] 	= 	<< 2972.34, 2895.33, 58.41 >>
			fPostRaceHead[PostRank3]	=	126.6092
			vPostRacePos[PostRank4] 	= 	<< 2974.65, 2897.74, 58.40 >>
			fPostRaceHead[PostRank4]	=	126.6092
			vPostRacePos[PostRank5] 	= 	<< 2977.32, 2900.29, 58.56 >>
			fPostRaceHead[PostRank5]	=	126.6092
			vPostRacePos[PostRank6] 	= 	<< 2979.87, 2902.49, 58.75 >>
			fPostRaceHead[PostRank6]	=	126.6092
			CDEBUG1LN(DEBUG_OR_RACES, "Post race stuff requested/set for Mineward Spiral / CASE 5")
		BREAK
		CASE ConstructionCourse
			// load waypoint recording for race
			REQUEST_WAYPOINT_RECORDING("OR_Post_7")
			tWaypointName = "OR_Post_7"
			// Set finish positions/headings for race
			vPostRacePos[PostRank1] 	= 	<< 1211.3209, 2377.7466, 62.4187 >>
			fPostRaceHead[PostRank1]	=	5.7949
			vPostRacePos[PostRank2] 	= 	<< 1211.2759, 2374.0796, 62.4053 >>
			fPostRaceHead[PostRank2]	=	346.0673
			vPostRacePos[PostRank3] 	= 	<< 1209.6149, 2371.0510, 62.0576 >>
			fPostRaceHead[PostRank3]	=	314.3206
			vPostRacePos[PostRank4] 	= 	<< 1205.5090, 2369.2644, 61.1430 >>
			fPostRaceHead[PostRank4]	=	286.8246
			vPostRacePos[PostRank5] 	= 	<< 1200.0502, 2368.5339, 60.0050 >>
			fPostRaceHead[PostRank5]	=	265.4383
			vPostRacePos[PostRank6] 	= 	<< 1194.9589, 2368.5435, 59.1379 >>
			fPostRaceHead[PostRank6]	=	267.2410
			CDEBUG1LN(DEBUG_OR_RACES, "Post race stuff requested/set for Mineward Spiral / CASE 5")
		BREAK
		BREAK
	ENDSWITCH
	
	fPostRaceHead[PostRank1] = fPostRaceHead[PostRank1]
	
ENDPROC

PROC FINISH_AI_RACERS(ORR_RACER_STRUCT& Racer, BOOL bEndScript = FALSE)
	TEXT_LABEL_31 waypointRec
	VECTOR vNodePos
	waypointRec = tWaypointName
	waypointRec += (Racer.iRank)
	IF (Racer.Driver = PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_OR_RACES, "Player finished, dont do automated finish for player")
	ELSE
		CDEBUG1LN(DEBUG_OR_RACES, "AI Racer finished race, make them drive to designated spot")
		IF DOES_BLIP_EXIST(Racer.Blip)
			REMOVE_BLIP(Racer.Blip)
		ENDIF
		IF NOT IS_ENTITY_DEAD(Racer.Driver)
		AND NOT IS_ENTITY_DEAD(Racer.Vehicle)
//			
//			IF IS_ENTITY_AT_COORD(Racer.Vehicle, vPostRacePos[Racer.iRank-1], << 3.0, 3.0, 3.0 >>)
//				IF GET_SCRIPT_TASK_STATUS( Racer.Driver, SCRIPT_TASK_VEHICLE_PARK ) = PERFORMING_TASK
//					CDEBUG1LN(DEBUG_OR_RACES, "Racer vehicle is parked at end of race position, leave it alone")
//				ELSE
//					CPRINTLN( DEBUG_OR_RACES, "Tasking to park vehicle." )
//					// protection dont do every frame
//					TASK_VEHICLE_PARK(Racer.Driver, Racer.Vehicle, vPostRacePos[Racer.iRank-1], fPostRaceHead[Racer.iRank-1], PARK_TYPE_PERPENDICULAR_NOSE_IN )
//					CDEBUG1LN(DEBUG_OR_RACES, "Racer vehicle is NOT parked at end of race position, telling them to park")
//				ENDIF
//			ELSE
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Racer.Vehicle)
					CDEBUG1LN(DEBUG_OR_RACES, "AI is following waypoint recording")
				ELSE
					IF GET_IS_WAYPOINT_RECORDING_LOADED(waypointRec)
						CDEBUG1LN(DEBUG_OR_RACES, "AI is NOT yet tasked to follow waypoint recording, doing it now")
						
						IF NOT bEndScript
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(Racer.Driver, Racer.Vehicle, waypointRec, DRIVINGMODE_AVOIDCARS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT, -1, -1)
						ELSE
							//CLEAR_PED_TASKS( Racer.Driver )
							GET_CLOSEST_VEHICLE_NODE( GET_ENTITY_COORDS( Racer.Vehicle ), vNodePos )
							
							TASK_VEHICLE_DRIVE_TO_COORD( Racer.Driver, Racer.Vehicle, vNodePos, 14.0, DRIVINGSTYLE_RACING, Racer.eVehicleModel, DRIVINGMODE_AVOIDCARS, 10.0, -1 )
							SET_PED_KEEP_TASK( Racer.Driver, TRUE )
							SET_PED_AS_NO_LONGER_NEEDED( Racer.Driver )
						ENDIF
					ELSE
						// add last ditch 
						CPRINTLN( DEBUG_OR_RACES, "Tasking driver to go to his finish pos. waypointrec is ", waypointRec )
						
						TASK_VEHICLE_DRIVE_TO_COORD(Racer.Driver, Racer.Vehicle, vPostRacePos[Racer.iRank-1], 10.0, DRIVINGSTYLE_STRAIGHTLINE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 0.5, -1.0)
						CDEBUG1LN(DEBUG_OR_RACES, "Post race waypoint recording failed to load, using alternative method")
					ENDIF
				ENDIF
				
			//ENDIF	
		ENDIF
		
	ENDIF
	
ENDPROC

PROC RELEASE_AI_RACERS(ORR_RACE_STRUCT& Race)
INT i
	REPEAT Race.iRacerCnt i
		IF NOT IS_ENTITY_DEAD(Race.Racer[i].driver)
		AND NOT IS_ENTITY_DEAD(Race.Racer[i].Vehicle)
			IF (Race.Racer[i].Driver = PLAYER_PED_ID())
				CDEBUG1LN(DEBUG_OR_RACES, "Player ped, do nothing")
			ELSE
				CDEBUG1LN(DEBUG_OR_RACES, "AI ped: if they finished the race, tell them to keep their task to park. if they are still racing, tell them to clear their tasks")	
				IF DOES_BLIP_EXIST(Race.Racer[i].Blip)
					REMOVE_BLIP(Race.Racer[i].Blip)
				ENDIF
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(Race.Racer[i].Vehicle)
					CDEBUG1LN(DEBUG_OR_RACES, "AI is following waypoint recording, removing tasks")
					CLEAR_PED_TASKS(Race.Racer[i].driver)
				ELSE
					CDEBUG1LN(DEBUG_OR_RACES, "AI is parking, maintain task")
					IF GET_SCRIPT_TASK_STATUS( Race.Racer[i].Driver, SCRIPT_TASK_SMART_FLEE_PED ) <> PERFORMING_TASK
						TASK_STAND_STILL(Race.Racer[i].driver, -1)
					ENDIF
					SET_PED_KEEP_TASK(Race.Racer[i].driver, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


// Load waypoint recordings on race to race basis
PROC OffroadRace_LoadRecordedWaypoint()
	TEXT_LABEL OffroadRecording = "Offroad_"
	SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
		CASE OFFROAD_RACE_CANYON_CLIFFS
			OffroadRecording += 1
		BREAK
		CASE OFFROAD_RACE_RIDGE_RUN
			OffroadRecording += 2
		BREAK
		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			OffroadRecording += 6
		BREAK
		CASE OFFROAD_RACE_VALLEY_TRAIL
			OffroadRecording += 3
		BREAK
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			OffroadRecording += 4
		BREAK
		CASE OFFROAD_RACE_ECO_FRIENDLY
			OffroadRecording += 5
		BREAK
	ENDSWITCH
	REQUEST_WAYPOINT_RECORDING(OffroadRecording)
	CDEBUG1LN(DEBUG_OR_RACES, "REQUEST_WAYPOINT_RECORDING: ", OffroadRecording)
	
//	IF (ORR_Master.iRaceCur = 0)
//		OffroadRecording += "a"
//		REQUEST_WAYPOINT_RECORDING(OffroadRecording)
//	ENDIF
ENDPROC

// Helper function for last vehicle
FUNC BOOL IS_LAST_VEHICLE_VALID()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
					CDEBUG1LN(DEBUG_OR_RACES, "IS_LAST_VEHICLE_VALID: Last vehicle was a BOAT; RETURNING FALSE")
					RETURN FALSE
				ELIF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
					CDEBUG1LN(DEBUG_OR_RACES, "IS_LAST_VEHICLE_VALID: Last vehicle was a HELI; RETURNING FALSE")
					RETURN FALSE
				ELIF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
					CDEBUG1LN(DEBUG_OR_RACES, "IS_LAST_VEHICLE_VALID: Last vehicle was a PLANE; RETURNING FALSE")
					RETURN FALSE
				ELSE
					CDEBUG1LN(DEBUG_OR_RACES, "IS_LAST_VEHICLE_VALID: Last vehicle not a boat, heli or plane; RETURNING TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL OffroadRace_GetIsWaypointLoaded()
	IF NOT IS_BITMASK_AS_ENUM_SET(iORCutsceneBits, CutsceneBit3)
		TEXT_LABEL OffroadRecording = "Offroad_"
		SWITCH INT_TO_ENUM(OFFROAD_RACE_INDEX, ORR_Master.iRaceCur)
			CASE OFFROAD_RACE_CANYON_CLIFFS
				OffroadRecording += 1
			BREAK
			CASE OFFROAD_RACE_RIDGE_RUN
				OffroadRecording += 2
			BREAK
			CASE OFFROAD_RACE_MINEWARD_SPIRAL
				OffroadRecording += 6
			BREAK
			CASE OFFROAD_RACE_VALLEY_TRAIL
				OffroadRecording += 3
			BREAK
			CASE OFFROAD_RACE_LAKESIDE_SPLASH
				OffroadRecording += 4
			BREAK
			CASE OFFROAD_RACE_ECO_FRIENDLY
				OffroadRecording += 5
			BREAK
		ENDSWITCH
		IF GET_IS_WAYPOINT_RECORDING_LOADED(OffroadRecording)		
			SET_BITMASK_AS_ENUM(iORCutsceneBits, CutsceneBit3)
			RETURN TRUE
		ENDIF	
		CDEBUG1LN(DEBUG_OR_RACES, "Waiting on GET_IS_WAYPOINT_RECORDING_LOADED: ", OffroadRecording)
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

FUNC BOOL OFFROAD_RACE_IS_VEHICLE_OK_FOR_RACE( VEHICLE_INDEX testVehicle, BOOL bMotorcycle = FALSE )

	IF NOT DOES_ENTITY_EXIST(testVehicle) 
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_DEAD(testVehicle) 
		RETURN FALSE
	ENDIF
		
	//dlc vehicle.
	IF GET_ENTITY_MODEL(testVehicle) = INT_TO_ENUM(MODEL_NAMES, HASH("kalahari"))	
		RETURN NOT bMotorcycle
	ENDIF
	
	IF GET_ENTITY_MODEL(testVehicle) = INT_TO_ENUM(MODEL_NAMES, HASH("HUNTLEY"))
		RETURN NOT bMotorcycle
	ENDIF
	
	IF GET_ENTITY_MODEL(testVehicle) = INT_TO_ENUM(MODEL_NAMES, HASH("DUBSTA3"))
		RETURN NOT bMotorcycle
	ENDIF
			
	SWITCH GET_ENTITY_MODEL(testVehicle)		

		CASE BFINJECTION 
		CASE BALLER
		CASE BISON
		CASE BISON2
		CASE BJXL
		CASE BOBCATXL
		CASE CAVALCADE
		CASE DUBSTA
		CASE DUBSTA2
		CASE FBI2
		CASE GRESLEY
		CASE GRANGER
		CASE MESA
		CASE PATRIOT
		CASE RANCHERXL
		CASE REBEL
		CASE REBEL2
		CASE ROCOTO
		CASE SANDKING
		CASE SANDKING2
		CASE SERRANO
		CASE MONSTER
		CASE BRAWLER
		CASE CRUSADER			RETURN NOT bMotorcycle									BREAK
		
		CASE BLAZER						
		CASE BLAZER2					
		CASE BLAZER3					
		CASE SANCHEZ			RETURN bMotorcycle										BREAK	
		
		CASE DUMP				RETURN FALSE											BREAK
		
		CASE BODHI2				RETURN ( NOT bMotorcycle AND ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR ) )	BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

PROC OFFROAD_RACE_VEHICLE_CHECK(ORR_RACER_STRUCT& Racer)
	
	IF ORR_Master.eRaceType != ORR_RACE_TYPE_OFFROAD
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: We are in an offroad race")
	
	If Racer.Driver != PLAYER_PED_ID()
		CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: AI Racers, do nothing")
		EXIT
	ENDIF
	// Check the players current vehicle
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Player is in vehicle")
		// Check what race we are in; to see if its a truck of bike race
		IF NOT ORR_DOES_RACE_REQUIRE_MOTORCYCLE()
			CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: we are in a truck race")
			IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Checking playerVeh against vehicle the player is in")
				IF DOES_ENTITY_EXIST(playerVeh)
				
					IF OFFROAD_RACE_IS_VEHICLE_OK_FOR_RACE( playerVeh, ORR_DOES_RACE_REQUIRE_MOTORCYCLE() )
						bMakeSwapCams = FALSE
						SET_BITMASK_AS_ENUM(iVehSettingBits, UseFullPlaVeh)
						CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to FALSE; UseFullPlaVeh bit set")
					ELSE
						//bMakeSwapCams = TRUE
						SET_BITMASK_AS_ENUM(iVehSettingBits, MakeFullDonor)	
						CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to TRUE; MakeFullDonor bit set")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// Bike Race
			CDEBUG1LN(DEBUG_OR_RACES, "we are in a Bike race")
			IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Checking playerVeh against vehicle the player is in")
				IF DOES_ENTITY_EXIST(playerVeh)
					IF OFFROAD_RACE_IS_VEHICLE_OK_FOR_RACE( playerVeh, ORR_DOES_RACE_REQUIRE_MOTORCYCLE() )
						CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Player's vehicle is a bike, we are using it")							
						bMakeSwapCams = FALSE
						SET_BITMASK_AS_ENUM(iVehSettingBits, UseMotoPlaVeh)
						CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to FALSE; UseMotoPlaVeh bit set")
					ELSE
						bMakeSwapCams = TRUE
						SET_BITMASK_AS_ENUM(iVehSettingBits, MakeMotoDonor)		
						CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to TRUE; MakeMotoDonor bit set")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		// Last vehicle section
		CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Player is NOT in vehicle")
		IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE()) AND (NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())) AND IS_VEHICLE_SEAT_FREE(GET_PLAYERS_LAST_VEHICLE())
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF IS_LAST_VEHICLE_VALID()
					CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Last vehicle not a boat/heli/plane, do regular logic")
					// Check what race we are in; to see if its a truck of bike race
					IF NOT ORR_DOES_RACE_REQUIRE_MOTORCYCLE()
						CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: we are in a truck race")
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Distance between last vehicle and player is: ")
							PRINTFLOAT(GET_DISTANCE_BETWEEN_ENTITIES(GET_PLAYERS_LAST_VEHICLE(), PLAYER_PED_ID()))
							PRINTNL()
							IF (GET_DISTANCE_BETWEEN_ENTITIES(GET_PLAYERS_LAST_VEHICLE(), PLAYER_PED_ID()) < 20.0)						
								lastPlayerVeh = GET_PLAYERS_LAST_VEHICLE()
								CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Checking for players last vehicle against fullsize offroad vehicle list")
								IF DOES_ENTITY_EXIST(lastPlayerVeh)
									IF OFFROAD_RACE_IS_VEHICLE_OK_FOR_RACE( lastPlayerVeh, ORR_DOES_RACE_REQUIRE_MOTORCYCLE() )												
										
										CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Player's last vehicel is a fullsize offroad vehicle, we are using it")										
										bMakeSwapCams = FALSE
										SET_BITMASK_AS_ENUM(iVehSettingBits, UseLastFullPlaVeh)
										CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to FALSE; UseLastFullPlaVeh bit set")
									ELSE
										//bMakeSwapCams = TRUE
										SET_BITMASK_AS_ENUM(iVehSettingBits, MakeLastFullDonorKeep)		
										CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to TRUE; MakeLastFullDonorKeep bit set")
									ENDIF
								ENDIF
							ELSE
								//bMakeSwapCams = TRUE
								SET_BITMASK_AS_ENUM(iVehSettingBits, MakeLastFullDonor)		
								CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to TRUE; MakeLastFullDonor bit set")
							ENDIF
						ENDIF
					ELSE
						// Bike Race
						CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: we are in a Bike race")
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Distance between last vehicle and player is: ")
							PRINTFLOAT(GET_DISTANCE_BETWEEN_ENTITIES(GET_PLAYERS_LAST_VEHICLE(), PLAYER_PED_ID()))
							PRINTNL()
							IF (GET_DISTANCE_BETWEEN_ENTITIES(GET_PLAYERS_LAST_VEHICLE(), PLAYER_PED_ID()) < 20.0)		
								lastPlayerVeh = GET_PLAYERS_LAST_VEHICLE()
								CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Checking for players last vehicle against bike offroad vehicle list")
								IF DOES_ENTITY_EXIST(lastPlayerVeh)
									IF OFFROAD_RACE_IS_VEHICLE_OK_FOR_RACE( lastPlayerVeh, ORR_DOES_RACE_REQUIRE_MOTORCYCLE() )	
									
										CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Player's vehicle is a bike, we are using it")
										bMakeSwapCams = FALSE
										SET_BITMASK_AS_ENUM(iVehSettingBits, UseLastMotoPlaVeh)
										CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to FALSE; UseLastMotoPlaVeh bit set")
										
									ELSE
										//bMakeSwapCams = TRUE
										SET_BITMASK_AS_ENUM(iVehSettingBits, MakeLastMotoDonorKeep)	
										CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to TRUE; MakeLastMotoDonorKeep bit set")
									ENDIF
								ENDIF
							ELSE
								//bMakeSwapCams = TRUE
								SET_BITMASK_AS_ENUM(iVehSettingBits, MakeLastMotoDonor)	
								CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to TRUE; MakeLastMotoDonor bit set")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//bMakeSwapCams = TRUE
					CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Last vehicle WAS a boat/heli/plane, do doner logic")
					// Players last vehicle was a plane or boat or heli, do doner
					IF NOT ORR_DOES_RACE_REQUIRE_MOTORCYCLE()
						SET_BITMASK_AS_ENUM(iVehSettingBits, MakeFullDonorInvalid)		
						CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to TRUE; MakeFullDonorInvalid bit set")
					ELSE
						SET_BITMASK_AS_ENUM(iVehSettingBits, MakeMotoDonorInvalid)		
						CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to TRUE; MakeMotoDonorInvalid bit set")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// Player never in a vehicle
			CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: Player is on foot")
			//bMakeSwapCams = TRUE
			IF NOT ORR_DOES_RACE_REQUIRE_MOTORCYCLE()
				SET_BITMASK_AS_ENUM(iVehSettingBits, MakeFullDonorInvalid)		
				CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to TRUE; MakeFullDonorInvalid bit set")
			ELSE
				SET_BITMASK_AS_ENUM(iVehSettingBits, MakeMotoDonorInvalid)		
				CDEBUG1LN(DEBUG_OR_RACES, "OFFROAD_RACE_VEHICLE_CHECK: bMakeSwapCams set to TRUE; MakeMotoDonorInvalid bit set")
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC OFFROAD_SAVE_VEHICLE_SETTINGS(VEHICLE_INDEX viVehicle, BOOL bRecreateVeh = TRUE)
	ORR_Master.sPlayerVehicleData.iVehicleColor = GET_VEHICLE_COLOUR_COMBINATION( viVehicle )
	ORR_Master.sPlayerVehicleData.vehicleName = GET_ENTITY_MODEL( viVehicle )
	GET_VEHICLE_SETUP( viVehicle, ORR_Master.sPlayerVehicleData.sPlayerVehicle )
	ORR_Master.bRecreateSavedVeh = bRecreateVeh
ENDPROC

// The radio should not be disabled in offroad races (a custom playlist is set up).
// Commenting this out temporarily in case this change is reversed
//PROC ORR_ENABLE_RADIO_ON_VEHICLE(VEHICLE_INDEX& vehIndex, BOOL bEnable)
	//PRINTLN("I'm disabling the vehicle radio!")
	//SET_VEHICLE_RADIO_ENABLED(vehIndex, bEnable)
//ENDPROC

FUNC BOOL Offroad_Vehicle_Settings(ORR_RACE_STRUCT & Race, ORR_RACER_STRUCT& Racer, VEHICLE_INDEX viDonorVehicle)
	INT iBitToVehicleSettings
	CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: pre-get_lowest_bit is: ")
	PRINTINT(iBitToVehicleSettings)
	PRINTNL()
	
	iBitToVehicleSettings = GET_LOWEST_BIT_SET(iVehSettingBits)
	IF iBitToVehicleSettings = -1
		CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: Lowestbit returned -1, RETURNING FALSE; BAD!!")
		RETURN FALSE
	ENDIF
	iBitToVehicleSettings = SHIFT_LEFT(1, iBitToVehicleSettings)
	CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: shifted left bit is: ")
	PRINTINT(iBitToVehicleSettings)
	PRINTNL()

	IF NOT DOES_ENTITY_EXIST( viDonorVehicle )
		iBitToVehicleSettings = 0
		IF ORR_DOES_RACE_REQUIRE_MOTORCYCLE()
			SET_BITMASK_AS_ENUM( iBitToVehicleSettings, MakeMotoDonorInvalid )
		ELSE
			SET_BITMASK_AS_ENUM( iBitToVehicleSettings, MakeFullDonorInvalid )
		ENDIF
	ENDIF
	
	//IF bMakeSwapCams
		//CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: bMakeSwapCams TRUE")
		//IF DOES_CAM_EXIST(cam1swap)
//			IF IS_CAM_ACTIVE(cam1swap)
		IF DOES_CAM_EXIST(cam1wide)
				//CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: cam1swap made and active")
				IF bVehicleSettings
					CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: bVehicleSettings TRUE, returning true")
					RETURN TRUE
				ELSE
					CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: bVehicleSettings FALSE, going through switch")
					CPRINTLN( DEBUG_OR_RACES, "Offroad_vehicle_settings: iBitToVehicleSettings is ", iBitToVehicleSettings )
					SWITCH INT_TO_ENUM(ORCutsceneEnum, iBitToVehicleSettings)
						CASE MakeFullDonor
							// store off current vehicle
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							ENDIF
							
							//SET_ENTITY_AS_MISSION_ENTITY(playerVeh)
							IF IS_VEHICLE_DRIVEABLE(playerVeh)
								SET_MISSION_VEHICLE_GEN_VEHICLE( playerVeh, Race.vPlayerCarPos, Race.fPlayerCarHeading )
								SET_LAST_DRIVEN_VEHICLE( playerVeh )
								OFFROAD_SAVE_VEHICLE_SETTINGS( playerVeh )
							ENDIF
							
							ORR_Master.savedVeh = playerVeh
							// give them a truck to use
							CDEBUG1LN(DEBUG_OR_RACES, "Player's vehicle is NOT a truck, we are making one for them")
							donorVeh = viDonorVehicle
							IF NOT IS_ENTITY_DEAD(donorVeh)
								SET_VEHICLE_HAS_STRONG_AXLES(donorVeh, TRUE)
								SET_VEHICLE_EXTRA(donorVeh, 1, FALSE)
								iMesaRoof = 0
								IF (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = -1)
								OR (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = 0)
									CDEBUG1LN(DEBUG_OR_RACES, "Player's vehicle is NOT a truck, and the color combination is invalid so we are skipping it")
								ELSE
									IF NOT IS_ENTITY_DEAD(donorVeh)
										iVehColors = GET_VEHICLE_COLOUR_COMBINATION(donorVeh)
									ENDIF
								ENDIF
//								ORR_ENABLE_RADIO_ON_VEHICLE(donorVeh, FALSE)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), donorVeh, VS_DRIVER)
							ENDIF
							Racer.Vehicle = donorVeh
							bVehicleSettings = TRUE
							Race.bUsingLoaner = TRUE
							Racer.eVehicleModel = GET_ENTITY_MODEL( donorVeh )
							RETURN TRUE
						BREAK						
						CASE MakeMotoDonor
							// store off current vehicle
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							ENDIF
							//SET_ENTITY_AS_MISSION_ENTITY(playerVeh)
							IF IS_VEHICLE_DRIVEABLE(playerVeh)
								SET_MISSION_VEHICLE_GEN_VEHICLE( playerVeh, Race.vPlayerCarPos, Race.fPlayerCarHeading )
								SET_LAST_DRIVEN_VEHICLE( playerVeh )
								OFFROAD_SAVE_VEHICLE_SETTINGS( playerVeh )
							ENDIF
							
							ORR_Master.savedVeh = playerVeh
							// give them a truck to use
							CDEBUG1LN(DEBUG_OR_RACES, "Player's vehicle is NOT a bike, we are making one for them")
							donorVeh = viDonorVehicle
							IF NOT IS_ENTITY_DEAD(donorVeh)
								SET_VEHICLE_HAS_STRONG_AXLES(donorVeh, TRUE)
								IF (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = -1)
								OR (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = 0)
									CDEBUG1LN(DEBUG_OR_RACES, "Player's vehicle is NOT a bike, and the color combination is invalid so we are skipping it")
								ELSE
									IF NOT IS_ENTITY_DEAD(donorVeh)
										iVehColors = GET_VEHICLE_COLOUR_COMBINATION(donorVeh)
									ENDIF
								ENDIF
//								ORR_ENABLE_RADIO_ON_VEHICLE(donorVeh, FALSE)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), donorVeh, VS_DRIVER)
							ELIF NOT DOES_ENTITY_EXIST( donorVeh )
								donorVeh = CREATE_VEHICLE(SANCHEZ, GET_ENTITY_COORDS( PLAYER_PED_ID() ), GET_ENTITY_HEADING( PLAYER_PED_ID() ) )
								SET_VEHICLE_HAS_STRONG_AXLES( donorVeh, TRUE)
								
								IF GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = -1
								OR GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = 0
									CDEBUG1LN(DEBUG_OR_RACES, "Player's vehicle is NOT a bike, and the color combination is invalid so we are skipping it")
								ELSE
									IF NOT IS_ENTITY_DEAD(donorVeh)
										iVehColors = GET_VEHICLE_COLOUR_COMBINATION(donorVeh)
									ENDIF
								ENDIF
								
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), donorVeh, VS_DRIVER)
							ENDIF	
							Racer.Vehicle = donorVeh
							bVehicleSettings = TRUE
							Race.bUsingLoaner = TRUE
							Racer.eVehicleModel = GET_ENTITY_MODEL( donorVeh )
							RETURN TRUE
						BREAK						
						CASE MakeLastFullDonorKeep
							// store off last vehicle
							playerVeh = GET_PLAYERS_LAST_VEHICLE()
							//SET_ENTITY_AS_MISSION_ENTITY(playerVeh)
							
							IF IS_VEHICLE_DRIVEABLE(playerVeh)
								SET_MISSION_VEHICLE_GEN_VEHICLE( playerVeh, Race.vPlayerCarPos, Race.fPlayerCarHeading )
								SET_LAST_DRIVEN_VEHICLE( playerVeh )
								OFFROAD_SAVE_VEHICLE_SETTINGS( playerVeh )
							ENDIF
							CPRINTLN( DEBUG_OR_RACES, "Inside MakeLastFullDonorKeep...")
							ORR_Master.savedVeh = playerVeh
							// give them a truck to use
							donorVeh = viDonorVehicle
							IF NOT IS_ENTITY_DEAD(donorVeh)
								SET_VEHICLE_HAS_STRONG_AXLES(donorVeh, TRUE)
								IF (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = -1)
								OR (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = 0)
									CDEBUG1LN(DEBUG_OR_RACES, "Player's last vehicle is NOT a fullsize offroad vehicle, and the color combination is invalid so we are skipping it")
								ELSE
									IF NOT IS_ENTITY_DEAD(donorVeh)
										iVehColors = GET_VEHICLE_COLOUR_COMBINATION(donorVeh)
									ENDIF
								ENDIF
//								ORR_ENABLE_RADIO_ON_VEHICLE(donorVeh, FALSE)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), donorVeh, VS_DRIVER)
								SET_VEHICLE_EXTRA(donorVeh, 1, FALSE)
							ENDIF
							
							iMesaRoof = 0
							Racer.Vehicle = donorVeh
							bVehicleSettings = TRUE
							Race.bUsingLoaner = TRUE
							Racer.eVehicleModel = GET_ENTITY_MODEL( donorVeh )
							RETURN TRUE
						BREAK
						CASE MakeLastFullDonor
							donorVeh = viDonorVehicle
							IF NOT IS_ENTITY_DEAD(donorVeh)
								SET_VEHICLE_HAS_STRONG_AXLES(donorVeh, TRUE)
								IF (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = -1)
								OR (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = 0)
									CDEBUG1LN(DEBUG_OR_RACES, "Player's last vehicle is NOT a fullsize offroad vehicle, and the color combination is invalid so we are skipping it")
								ELSE
									IF NOT IS_ENTITY_DEAD(donorVeh)
										iVehColors = GET_VEHICLE_COLOUR_COMBINATION(donorVeh)
									ENDIF
								ENDIF
//								ORR_ENABLE_RADIO_ON_VEHICLE(donorVeh, FALSE)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), donorVeh, VS_DRIVER)
							ENDIF
							CPRINTLN( DEBUG_OR_RACES, "Inside MakeLastFullDonor..." )
							Racer.Vehicle = donorVeh
							bVehicleSettings = TRUE
							Race.bUsingLoaner = TRUE
							Racer.eVehicleModel = GET_ENTITY_MODEL( donorVeh )
							RETURN TRUE
						BREAK						
						CASE MakeLastMotoDonor
							donorVeh = viDonorVehicle
							IF NOT IS_ENTITY_DEAD(donorVeh)
								SET_VEHICLE_HAS_STRONG_AXLES(donorVeh, TRUE)
								IF (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = -1)
								OR (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = 0)
									CDEBUG1LN(DEBUG_OR_RACES, "Player's last vehicle is NOT a bike, and the color combination is invalid so we are skipping it")
								ELSE
									IF NOT IS_ENTITY_DEAD(donorVeh)
										iVehColors = GET_VEHICLE_COLOUR_COMBINATION(donorVeh)
									ENDIF
								ENDIF
//								ORR_ENABLE_RADIO_ON_VEHICLE(donorVeh, FALSE)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), donorVeh, VS_DRIVER)
//								IF DOES_ENTITY_EXIST(ORR_Master.savedVeh)
//									SET_ENTITY_VISIBLE(ORR_Master.savedVeh, FALSE)
//									SET_ENTITY_COLLISION(ORR_Master.savedVeh, FALSE)
//								ENDIF
							ENDIF
							CPRINTLN( DEBUG_OR_RACES, "Inside MakeLastMotoDonor..." )
							Racer.Vehicle = donorVeh
							bVehicleSettings = TRUE
							Race.bUsingLoaner = TRUE
							Racer.eVehicleModel = GET_ENTITY_MODEL( donorVeh )
							RETURN TRUE
						BREAK
						CASE MakeFullDonorInvalid
							
							// store off current vehicle
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							ENDIF
							//SET_ENTITY_AS_MISSION_ENTITY(playerVeh)
							
							IF IS_VEHICLE_DRIVEABLE(playerVeh)
								SET_MISSION_VEHICLE_GEN_VEHICLE( playerVeh, Race.vPlayerCarPos, Race.fPlayerCarHeading )
								SET_LAST_DRIVEN_VEHICLE( playerVeh )
								OFFROAD_SAVE_VEHICLE_SETTINGS(playerVeh, FALSE)
							ENDIF
							
							ORR_Master.savedVeh = playerVeh
							
							IF DOES_ENTITY_EXIST( viDonorVehicle )
								donorVeh = viDonorVehicle
							ELSE
								donorVeh = CREATE_VEHICLE(MESA, GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()))
							ENDIF
							IF NOT IS_ENTITY_DEAD(donorVeh)
								SET_VEHICLE_HAS_STRONG_AXLES(donorVeh, TRUE)
								SET_VEHICLE_EXTRA(donorVeh, 1, FALSE)
								iMesaRoof = 0
								IF (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = -1)
								OR (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = 0)
									CDEBUG1LN(DEBUG_OR_RACES, "Player's last vehicle is NOT a fullsize offroad vehicle, and the color combination is invalid so we are skipping it")
								ELSE
									IF NOT IS_ENTITY_DEAD(donorVeh)
										iVehColors = GET_VEHICLE_COLOUR_COMBINATION(donorVeh)
									ENDIF
								ENDIF
//								ORR_ENABLE_RADIO_ON_VEHICLE(donorVeh, FALSE)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), donorVeh, VS_DRIVER)
							ENDIF
							CPRINTLN( DEBUG_OR_RACES, "Inside MakeFullDonorInvalid" )
							Racer.Vehicle = donorVeh
							bVehicleSettings = TRUE
							Race.bUsingLoaner = TRUE
							Racer.eVehicleModel = GET_ENTITY_MODEL( donorVeh )
							RETURN TRUE
						BREAK
						CASE MakeMotoDonorInvalid
							// store off current vehicle
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							ENDIF
							//SET_ENTITY_AS_MISSION_ENTITY(playerVeh)
							IF IS_VEHICLE_DRIVEABLE(playerVeh)
								SET_MISSION_VEHICLE_GEN_VEHICLE( playerVeh, Race.vPlayerCarPos, Race.fPlayerCarHeading )
								SET_LAST_DRIVEN_VEHICLE( playerVeh )
								OFFROAD_SAVE_VEHICLE_SETTINGS(playerVeh, FALSE)
							ENDIF
							
							ORR_Master.savedVeh = playerVeh
						
							IF DOES_ENTITY_EXIST( viDonorVehicle )
								donorVeh = viDonorVehicle
							ELSE
								donorVeh = CREATE_VEHICLE(SANCHEZ, GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()))
							ENDIF
							IF NOT IS_ENTITY_DEAD(donorVeh)
								SET_VEHICLE_HAS_STRONG_AXLES(donorVeh, TRUE)
								IF (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = -1)
								OR (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = 0)
									CDEBUG1LN(DEBUG_OR_RACES, "Player's last vehicle is NOT a bike, and the color combination is invalid so we are skipping it")
								ELSE
									IF NOT IS_ENTITY_DEAD(donorVeh)
										iVehColors = GET_VEHICLE_COLOUR_COMBINATION(donorVeh)
									ENDIF
								ENDIF
//								ORR_ENABLE_RADIO_ON_VEHICLE(donorVeh, FALSE)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), donorVeh, VS_DRIVER)
							ENDIF
							CPRINTLN( DEBUG_OR_RACES, "Inside MakeMotoDonorInvalid" )
							Racer.Vehicle = donorVeh
							bVehicleSettings = TRUE
							Race.bUsingLoaner = TRUE
							Racer.eVehicleModel = GET_ENTITY_MODEL( donorVeh )
							RETURN TRUE
						BREAK
						CASE MakeLastMotoDonorKeep
							// store off current vehicle
							playerVeh = GET_PLAYERS_LAST_VEHICLE()
							//SET_ENTITY_AS_MISSION_ENTITY(playerVeh)
							
							IF IS_VEHICLE_DRIVEABLE(playerVeh)
								SET_MISSION_VEHICLE_GEN_VEHICLE( playerVeh, Race.vPlayerCarPos, Race.fPlayerCarHeading )
								OFFROAD_SAVE_VEHICLE_SETTINGS( playerVeh )
								SET_LAST_DRIVEN_VEHICLE( playerVeh )
							ENDIF
							ORR_Master.savedVeh = playerVeh
							
							IF DOES_ENTITY_EXIST( ORR_Master.savedVeh )
								CPRINTLN( DEBUG_OR_RACES, "Making bike, last vehicle is valid..." )
							ENDIF
							// give them a truck to use
							CDEBUG1LN(DEBUG_OR_RACES, "Player's last vehicle is NOT a bike, we are making one for them")
							donorVeh = viDonorVehicle
							IF NOT IS_ENTITY_DEAD(donorVeh)
								SET_VEHICLE_HAS_STRONG_AXLES(donorVeh, TRUE)
								IF (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = -1)
								OR (GET_NUMBER_OF_VEHICLE_COLOURS(donorVeh) = 0)
									CDEBUG1LN(DEBUG_OR_RACES, "Player's last vehicle is NOT a bike, and the color combination is invalid so we are skipping it")
								ELSE
									iVehColors = GET_VEHICLE_COLOUR_COMBINATION(donorVeh)
								ENDIF
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), donorVeh, VS_DRIVER)
//								ORR_ENABLE_RADIO_ON_VEHICLE(donorVeh, FALSE)
							ENDIF
							Racer.Vehicle = donorVeh
							bVehicleSettings = TRUE
							Race.bUsingLoaner = TRUE
							Racer.eVehicleModel = GET_ENTITY_MODEL( donorVeh )
							RETURN TRUE
						BREAK
						CASE UseFullPlaVeh
							SET_LAST_DRIVEN_VEHICLE( playerVeh )
							OFFROAD_SAVE_VEHICLE_SETTINGS( playerVeh, FALSE )
							
							ORR_Master.savedVeh = playerVeh
							Race.bUseAlternateOutroCam = TRUE
							IF DOES_ENTITY_EXIST(viDonorVehicle) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viDonorVehicle)
								CPRINTLN(DEBUG_OR_RACES, "Deleting donor vehicle")
								SET_ENTITY_AS_MISSION_ENTITY(viDonorVehicle, TRUE, TRUE)
								DELETE_VEHICLE(viDonorVehicle)
							ELSE
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viDonorVehicle)
									CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: Player is in donor vehicle so we cant delete it!")
								ENDIF
							ENDIF
							IF (GET_NUMBER_OF_VEHICLE_COLOURS(playerVeh) = -1)
							OR (GET_NUMBER_OF_VEHICLE_COLOURS(playerVeh) = 0)
								CDEBUG1LN(DEBUG_OR_RACES, "Player's vehicle is NOT a truck, and the color combination is invalid so we are skipping it")
							ELSE
								IF NOT IS_ENTITY_DEAD(playerVeh)
									iVehColors = GET_VEHICLE_COLOUR_COMBINATION(playerVeh)
								ENDIF
							ENDIF
							CPRINTLN( DEBUG_OR_RACES, "Inside UseFullPlaVeh" )
							Racer.eVehicleModel = (GET_ENTITY_MODEL(playerVeh))
							Racer.Vehicle = playerVeh	
							IF IS_VEHICLE_MODEL(playerVeh, MESA)
								IF DOES_EXTRA_EXIST(playerVeh, 1)
									IF IS_VEHICLE_EXTRA_TURNED_ON(playerVeh, 1)
										iMesaRoof = 1
									ELSE
										iMesaRoof = 0
									ENDIF
								ENDIF
							ENDIF
							bVehicleSettings = TRUE
							RETURN TRUE
						BREAK
						CASE UseMotoPlaVeh
							SET_LAST_DRIVEN_VEHICLE( playerVeh )
							OFFROAD_SAVE_VEHICLE_SETTINGS( playerVeh, FALSE )
							
							ORR_Master.savedVeh = playerVeh
							IF DOES_ENTITY_EXIST(viDonorVehicle) 
								IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viDonorVehicle)
									CPRINTLN(DEBUG_OR_RACES, "Deleting donor vehicle")
									SET_ENTITY_AS_MISSION_ENTITY(viDonorVehicle, TRUE, TRUE)
									DELETE_VEHICLE(viDonorVehicle)
								ELSE
									CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: Player is in donor vehicle so we cant delete it!")
								ENDIF
							ENDIF
							IF NOT IS_ENTITY_DEAD(playerVeh)
								IF (GET_NUMBER_OF_VEHICLE_COLOURS(playerVeh) = -1)
								OR (GET_NUMBER_OF_VEHICLE_COLOURS(playerVeh) = 0)
									CDEBUG1LN(DEBUG_OR_RACES, "Player's vehicle is a bike, and the color combination is invalid so we are skipping it")
								ELSE
									iVehColors = GET_VEHICLE_COLOUR_COMBINATION(playerVeh)
								ENDIF
							ENDIF
							CPRINTLN( DEBUG_OR_RACES, "Inside UseMotoPlaVeh" )
							Racer.eVehicleModel = (GET_ENTITY_MODEL(playerVeh))
							Racer.Vehicle = playerVeh	
							IF IS_VEHICLE_MODEL(playerVeh, MESA)
								IF DOES_EXTRA_EXIST(playerVeh, 1)
									IF IS_VEHICLE_EXTRA_TURNED_ON(playerVeh, 1)
										iMesaRoof = 1
									ELSE
										iMesaRoof = 0
									ENDIF
								ENDIF
							ENDIF
							bVehicleSettings = TRUE
							RETURN TRUE
						BREAK
						CASE UseLastFullPlaVeh
							IF DOES_ENTITY_EXIST(viDonorVehicle) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viDonorVehicle)
								CPRINTLN(DEBUG_OR_RACES, "Deleting donor vehicle")
								SET_ENTITY_AS_MISSION_ENTITY(viDonorVehicle, TRUE, TRUE)
								IF lastPlayerVeh <> viDonorVehicle
									CPRINTLN(DEBUG_OR_RACES, "Donor vehicle is last vehicle" )
									DELETE_VEHICLE(viDonorVehicle)
								ELSE
									lastPlayerVeh = viDonorVehicle
								ENDIF
							ELSE
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viDonorVehicle)
									CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: Player is in donor vehicle so we cant delete it!")
								ELIF NOT DOES_ENTITY_EXIST(lastPlayerVeh)
									CPRINTLN( DEBUG_OR_RACES, "Offroad_Vehicle_Settings: We have a problem, the player should be creating a vehicle" )
								ELSE
									CPRINTLN( DEBUG_OR_RACES, "Offroad_Vehicle_Settings: Last player vehicle is valid... there is something wrong.")
								ENDIF
							ENDIF
							IF NOT IS_ENTITY_DEAD(lastPlayerVeh)
								IF (GET_NUMBER_OF_VEHICLE_COLOURS(lastPlayerVeh) = -1)
								OR (GET_NUMBER_OF_VEHICLE_COLOURS(lastPlayerVeh) = 0)
									CDEBUG1LN(DEBUG_OR_RACES, "Player's last vehicle is a fullsize offroad vehicle, and the color combination is invalid so we are skipping it")
								ELSE
									iVehColors = GET_VEHICLE_COLOUR_COMBINATION(lastPlayerVeh)
								ENDIF
							ENDIF
							Racer.eVehicleModel = (GET_ENTITY_MODEL(lastPlayerVeh))
							Racer.Vehicle = lastPlayerVeh	
							IF IS_VEHICLE_MODEL(lastPlayerVeh, MESA)
								IF DOES_EXTRA_EXIST(lastPlayerVeh, 1)
									IF IS_VEHICLE_EXTRA_TURNED_ON(lastPlayerVeh, 1)
										iMesaRoof = 1
									ELSE
										iMesaRoof = 0
									ENDIF
								ENDIF
							ENDIF
							SET_ENTITY_AS_MISSION_ENTITY(lastPlayerVeh, true)
							CPRINTLN( DEBUG_OR_RACES, "Setting player into vehicle...")
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), lastPlayerVeh)
							bVehicleSettings = TRUE
							RETURN TRUE
						BREAK
						CASE UseLastMotoPlaVeh
							IF DOES_ENTITY_EXIST(viDonorVehicle) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viDonorVehicle)
								CPRINTLN(DEBUG_OR_RACES, "Deleting donor vehicle")
								SET_ENTITY_AS_MISSION_ENTITY(viDonorVehicle, TRUE, TRUE)
								IF viDonorVehicle <> lastPlayerVeh
									CPRINTLN( DEBUG_OR_RACES, "Last player veh is not donor vehicle, removing donor" )
									DELETE_VEHICLE(viDonorVehicle)
								ENDIF
							ELSE
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viDonorVehicle)
									CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: Player is in donor vehicle so we cant delete it!")
								ENDIF
							ENDIF
							IF NOT IS_ENTITY_DEAD(lastPlayerVeh)
								iVehColors = GET_VEHICLE_COLOUR_COMBINATION(lastPlayerVeh)
								IF (GET_NUMBER_OF_VEHICLE_COLOURS(lastPlayerVeh) = -1)
								OR (GET_NUMBER_OF_VEHICLE_COLOURS(lastPlayerVeh) = 0)
									CDEBUG1LN(DEBUG_OR_RACES, "Player's last vehicle is a bike, and the color combination is invalid so we are skipping it")
								ELSE
									Racer.eVehicleModel = (GET_ENTITY_MODEL(lastPlayerVeh))
								ENDIF
							ENDIF
							CPRINTLN( DEBUG_OR_RACES, "Inside UseLastMotoPlaVeh" )
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), lastPlayerVeh)
								SET_PED_INTO_VEHICLE( PLAYER_PED_ID(), lastPlayerVeh)
							ENDIF
							Racer.Vehicle = lastPlayerVeh	
							bVehicleSettings = TRUE
							RETURN TRUE
						BREAK
					ENDSWITCH
				ENDIF
//			ELSE
//				CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: Swap cam isnt active yet; returning false")
//				RETURN FALSE
//			ENDIF
		ELSE
			CPRINTLN( DEBUG_OR_RACES, "Offroad_Vehicle_Settings: Cam isn't ready yet, returning false" )
			RETURN FALSE
		ENDIF
	//ELSE
		CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: bMakeSwapCams TRUE")
		//ENDIF
	//ENDIF
	
	IF DOES_ENTITY_EXIST( ORR_Master.savedVeh )
		SET_ENTITY_AS_MISSION_ENTITY( ORR_Master.savedVeh )
	ENDIF
	
	CDEBUG1LN(DEBUG_OR_RACES, "Offroad_Vehicle_Settings: Default return FALSE; BAD!!!")
	RETURN FALSE
ENDFUNC

FUNC BOOL DetermineOffroadCutscene(ORR_RACE_STRUCT& Race)

	IF NOT IS_BITMASK_AS_ENUM_SET(iORCutsceneBits, CutsceneBit2)
		CDEBUG1LN(DEBUG_OR_RACES, "DetermineOffroadCutscene: CutsceneBit2 not set, running OFFROAD_RACE_VEHICLE_CHECK")
		OFFROAD_RACE_VEHICLE_CHECK(Race.Racer[0])
		SET_BITMASK_AS_ENUM(iORCutsceneBits, CutsceneBit2)
		CDEBUG1LN(DEBUG_OR_RACES, "DetermineOffroadCutscene: OFFROAD_RACE_VEHICLE_CHECK done, CutsceneBit2 set")
	ENDIF
	
//	IF IS_BITMASK_AS_ENUM_SET(iORCutsceneBits, CutsceneBit2)
//		CDEBUG1LN(DEBUG_OR_RACES, "DetermineOffroadCutscene: CutsceneBit2 set, checking cam bool")
//		IF bMakeSwapCams
//			Race.bUsingLoaner = TRUE
//			CDEBUG1LN(DEBUG_OR_RACES, "DetermineOffroadCutscene: bMakeSwapCams TRUE, RETURNING TRUE")
//			//RETURN TRUE
//		ELSE
//			CDEBUG1LN(DEBUG_OR_RACES, "DetermineOffroadCutscene: bMakeSwapCams FALSE, RETURNING FALSE")
//			//RETURN FALSE
//		ENDIF
//	ENDIF
	
	//CDEBUG1LN(DEBUG_OR_RACES, "DetermineOffroadCutscene: Default return FALSE; BAD!!!")
	RETURN FALSE
ENDFUNC


PROC makeSwapCutscenCams()
	IF bSwapCamsMade
		IF bMakeSwapCams
			SET_CAM_ACTIVE(cam1swap, TRUE)
		ENDIF
		EXIT
	ENDIF
	
	//KEY FOR DIFF CAMS0000000000000000000000**************
	//cam4swap = Shot of bike loaner
	//cam3swap = Shot of player on loaned bike
	//cam2swap = Low shot of bike loaner
	//cam1swap = Look away shot of the race
	
	
	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
		CASE CanyonCliffs
			CPRINTLN(DEBUG_OR_RACES, "Making swap-cutscene cams for CanyonCliffs")
			IF NOT DOES_CAM_EXIST(cam4swap)				
				cam4swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1951.8300, 4446.3164, 37.5441 >>, << -14.5753, 0.0000, -50.7763 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam3swap)
				cam3swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1949.0504, 4447.8218, 37.5105 >>, << -21.9295, -0.0000, 94.1647 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam2swap)
				cam2swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1951.8300, 4446.3164, 37.5441 >>, << -14.5753, 0.0000, -50.7763 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1swap)
				cam1swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1951.8300, 4446.3164, 42.5441 >>, << -14.5753, 0.0000, -50.7763 >>, 50.0, TRUE)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Cameras made")
		
		#IF OFFROAD_USE_SWAP_SCENES
			SET_CAM_ACTIVE(cam1swap, TRUE)
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  cam1swap active")
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Rendering script cams")
		#ENDIF
			bSwapCamsMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  bSwapCamsMade = TRUE")
		BREAK
		
		CASE RidgeRun
			CPRINTLN(DEBUG_OR_RACES, "Making swap-cutscene cams for RidgeRun")
			IF NOT DOES_CAM_EXIST(cam4swap)
				cam4swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-515.8344, 2000.2369, 207.0728>>, <<-14.2445, 0.0000, -63.6119>>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam3swap)
				cam3swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-515.8344, 2000.2369, 207.0728>>, <<-14.2445, 0.0000, -63.6119>>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam2swap)
				cam2swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-513.5111, 1997.9398, 207.1878>>, <<-10.6558, -0.0000, 114.3152>>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1swap)
				cam1swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-513.5111, 1997.9398, 212.1878>>, <<-10.6558, -0.0000, 114.3152>>, 50.0, TRUE)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Cameras made")
		#IF OFFROAD_USE_SWAP_SCENES
			SET_CAM_ACTIVE(cam1swap, TRUE)
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  cam1swap active")
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Rendering script cams")
		#ENDIF
			bSwapCamsMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  bSwapCamsMade = TRUE")
		BREAK
		
		CASE ValleyTrail	
			CPRINTLN(DEBUG_OR_RACES, "Making swap-cutscene cams for ValleyTrail")
			IF NOT DOES_CAM_EXIST(cam4swap)
				cam4swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -210.8671, 4223.7939, 46.8499 >>, << -15.5805, 0.0000, 147.2511 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam3swap)
				cam3swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -212.3710, 4220.0889, 45.5286 >>, << -15.1556, 0.0000, -50.8314 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam2swap)
				cam2swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -210.8671, 4223.7939, 46.8499 >>, << -15.5805, 0.0000, 147.2511 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1swap)
				cam1swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -210.8671, 4223.7939, 51.8499 >>, << -15.5805, 0.0000, 147.2511 >>, 50.0, TRUE)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Cameras made")
		#IF OFFROAD_USE_SWAP_SCENES
			SET_CAM_ACTIVE(cam1swap, TRUE)
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  cam1swap active")
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Rendering script cams")
		#ENDIF
			bSwapCamsMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  bSwapCamsMade = TRUE")
		BREAK
		
		CASE LakesideSplash
			CPRINTLN(DEBUG_OR_RACES, "Making swap-cutscene cams for LakesideSplash")
			IF NOT DOES_CAM_EXIST(cam4swap)
				cam4swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1594.8423, 3832.8967, 36.1087 >>, << -28.3459, 0.0000, 24.9542 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam3swap)
				cam3swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1593.2543, 3835.7412, 34.7320 >>, << -22.2727, 0.0000, 168.4172 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam2swap)
				cam2swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1594.8423, 3832.8967, 36.1087 >>, << -28.3459, 0.0000, 24.9542 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1swap)
				cam1swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1594.8423, 3832.8967, 41.1087 >>, << -28.3459, 0.0000, 24.9542 >>, 50.0, TRUE)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Cameras made")
		#IF OFFROAD_USE_SWAP_SCENES
			SET_CAM_ACTIVE(cam1swap, TRUE)
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  cam1swap active")
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Rendering script cams")
		#ENDIF
			bSwapCamsMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  bSwapCamsMade = TRUE")
		BREAK
		
		CASE EcoFriendly
			CPRINTLN(DEBUG_OR_RACES, "Making swap-cutscene cams for EcoFriendly")
			IF NOT DOES_CAM_EXIST(cam4swap)
				cam4swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 2028.9685, 2123.3948, 95.1566 >>, << -13.2486, 0.0000, 24.9273 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam3swap)
				cam3swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 2027.8495, 2125.6570, 94.0429 >>, << -12.6271, -0.0000, 179.2410 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam2swap)
				cam2swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 2028.9685, 2123.3948, 95.1566 >>, << -13.2486, 0.0000, 24.9273 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1swap)
				cam1swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 2028.9685, 2123.3948, 100.1566 >>, << -13.2486, 0.0000, 24.9273 >>, 50.0, TRUE)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Cameras made")
		#IF OFFROAD_USE_SWAP_SCENES
			SET_CAM_ACTIVE(cam1swap, TRUE)
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  cam1swap active")
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Rendering script cams")
		#ENDIF
			bSwapCamsMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  bSwapCamsMade = TRUE")
		BREAK
		
		CASE MinewardSpiral
			CPRINTLN(DEBUG_OR_RACES, "Making swap-cutscene cams for MinewardSpiral")
			IF NOT DOES_CAM_EXIST(cam4swap)
				cam4swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 2981.4998, 2752.6855, 45.2291 >>, << -19.8850, 0.0000, 9.9727 >>, 50.0)
			ENDIF
			//Bike Loaner End Pos
			IF NOT DOES_CAM_EXIST(cam3swap)
				cam3swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 2979.5957, 2755.6658, 44.3222 >>, << -18.7679, 0.0000, 177.1737 >>, 50.0)
			ENDIF
			//Shot of guy on ground
			IF NOT DOES_CAM_EXIST(cam2swap)
				cam2swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 2981.4998, 2752.6855, 45.2291 >>, << -19.8850, 0.0000, 9.9727 >>, 50.0)
			ENDIF
			//High up shot
			IF NOT DOES_CAM_EXIST(cam1swap)
				cam1swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2981.4998, 2752.6855, 50.2053>>, <<-19.8850, 0.0000, 27.8455>>, 50.0, TRUE)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Cameras made")
		#IF OFFROAD_USE_SWAP_SCENES
			SET_CAM_ACTIVE(cam1swap, TRUE)
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  cam1swap active")
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Rendering script cams")
		#ENDIF
			bSwapCamsMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  bSwapCamsMade = TRUE")
		BREAK
		
		CASE ConstructionCourse
			IF NOT DOES_CAM_EXIST(cam4swap)
				cam4swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1168.7645, 2383.0923, 58.2271 >>, << -7.2523, 0.3464, 147.9738 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam3swap)
				cam3swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1168.7645, 2383.0923, 58.2271 >>, << -7.2523, 0.3464, 147.9738 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam2swap)
				cam2swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1168.1868, 2383.0298, 58.5179 >>, << -7.0656, 0.0000, -58.3060 >>, 50.0)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1swap)
				cam1swap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1168.7645, 2383.0923, 58.2271 >>, << -7.2523, 0.3464, 147.9738 >>, 50.0, TRUE)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Cameras made")
		#IF OFFROAD_USE_SWAP_SCENES
			SET_CAM_ACTIVE(cam1swap, TRUE)
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  cam1swap active")
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  Rendering script cams")
		#ENDIF
			bSwapCamsMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeSwapCutscenCams:  bSwapCamsMade = TRUE")
		BREAK
		
		ODDJOB_ENTER_CUTSCENE()
				
	ENDSWITCH
	
ENDPROC

PROC MAKE_TRANSITION_CAM()
	SWITCH INT_TO_ENUM( ORR_RACE_NAMES, ORR_Master.iRaceCur )
		CASE CanyonCliffs
			camTransition = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED,  <<-1696.2106, 4309.4839, 78.3513>>, <<-17.1796, -0.0000, 57.1903>>, 42.5736)//<<-1754.4604, 4381.6934, 73.5485>>, <<4.5966, -0.0000, -147.2439>>, 44.9879 )
			camTransition2 = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED, <<-1694.7935, 4311.8262, 78.2980>>, <<-17.1796, 0.0000, 58.2317>>, 42.5736)//<<-1761.7247, 4379.4873, 70.2888>>, <<8.7652, -0.0000, -142.4407>>, 44.9879)
			SET_CAM_ACTIVE_WITH_INTERP(camTransition2, camTransition, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS( TRUE, FALSE )
		BREAK
		CASE RidgeRun
			camTransition = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED, <<-634.2543, 1916.1086, 192.3409>>, <<-9.1154, -0.0000, -9.1071>>, 45.0104 )
			camTransition2 = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED, <<-641.3137, 1917.6265, 190.8026>>, <<-9.1154, -0.0000, -15.3093>>, 45.0104)
			SET_CAM_ACTIVE_WITH_INTERP(camTransition2, camTransition, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS( TRUE, FALSE )
		BREAK 
		CASE ValleyTrail
			camTransition = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED, <<-236.4628, 4240.5273, 75.4924>>, <<-8.8267, 0.0000, 79.9126>>, 50.0000)//<<-232.2406, 4227.1323, 45.1519>>, <<3.3421, 0.0345, 82.9324>>, 41.6574 )
			camTransition2 = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED, <<-235.1967, 4236.5723, 71.9846>>, <<-8.8267, 0.0000, 79.9126>>, 50.0000)
			SET_CAM_ACTIVE_WITH_INTERP(camTransition2, camTransition, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS( TRUE, FALSE )
		BREAK
		CASE LakesideSplash
			camTransition = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED, <<1796.4657, 4040.9116, 48.7151>>, <<-4.5857, -0.0023, -137.7639>>, 44.9933, FALSE )
			camTransition2 = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED, <<1791.1207, 4036.0698, 48.7158>>, <<-4.8249, 0.0628, -133.4925>>, 44.9933)
			SET_CAM_ACTIVE_WITH_INTERP(camTransition2, camTransition, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS( TRUE, FALSE )
		BREAK
		CASE EcoFriendly
			camTransition = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED, <<2071.4714, 2157.5144, 147.4870>>, <<-7.5491, 0.0781, -126.4479>>, 45.0214, FALSE )
			camTransition2 = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED, <<2073.7583, 2160.3643, 147.2902>>, <<-10.4994, 0.2542, -133.5917>>, 45.0214)
			SET_CAM_ACTIVE_WITH_INTERP(camTransition2, camTransition, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS( TRUE, FALSE )
		BREAK
		CASE MinewardSpiral
			camTransition = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED, <<3098.0613, 2697.8962, 105.0805>>, <<-14.2440, -0.0027, 61.1833>>, 45.0418, FALSE )
			camTransition2 = CREATE_CAMERA_WITH_PARAMS( CAMTYPE_SCRIPTED, <<3101.7432, 2706.1934, 105.0836>>, <<-14.3949, 0.0565, 66.3009>>, 45.0418)
			SET_CAM_ACTIVE_WITH_INTERP(camTransition2, camTransition, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS( TRUE, FALSE )
		BREAK
		
	ENDSWITCH
	
	SHAKE_SCRIPT_GLOBAL("HAND_SHAKE", 0.3)
	
ENDPROC

FUNC BOOL IS_FINISHED_DOING_RESTART_CAMERA_TRANSITION(ORR_RACE_STRUCT& Race)
	UNUSED_PARAMETER(Race)
	IF IS_CAM_INTERPOLATING(ORR_Master.camFinalLBD2)
//	IS_INTERPOLATING_FROM_SCRIPT_CAMS()
		CPRINTLN(DEBUG_OR_RACES, "IS_FINISHED_DOING_RESTART_CAMERA_TRANSITION : Waiting for interpolation to finish")
		RETURN FALSE	
	ENDIF
	CPRINTLN(DEBUG_OR_RACES, "IS_FINISHED_DOING_RESTART_CAMERA_TRANSITION : returning TRUE")
	RETURN TRUE
ENDFUNC 
	
PROC DELETE_TRANSITION_CAM()
	IF DOES_CAM_EXIST( camTransition )
		DESTROY_CAM( camTransition )
	ENDIF
	
	IF DOES_CAM_EXIST( camTransition2 )
		DESTROY_CAM( camTransition2 )
	ENDIF
ENDPROC

PROC makeCutsceneCams()
	UNUSED_PARAMETER(iCutInterpDur)
	IF bCamerasMade
		IF NOT bMakeSwapCams
			SET_CAM_ACTIVE(cam1wide, TRUE)
		ENDIF
		EXIT
	ENDIF
	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
		
		CASE CanyonCliffs
			CPRINTLN(DEBUG_OR_RACES, "Making cutscene cams for CanyonCliffs")
			iCutInterpDur = 7000
			IF NOT DOES_CAM_EXIST(cam2wide)
				vCutsceneCamDest = <<-1934.7365, 4442.1748, 38.2098>>
				cam2wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1934.7644, 4441.7422, 38.3865>>, <<4.0163, -0.0469, -100.6342>>, 39.9908)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1wide)
				cam1wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1932.8745, 4441.5884, 38.3544>>, <<10.6304, -0.0316, -105.3918>>, 39.9908)//CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1943.7, 4444.8, 48.2 >>, << 0.7991, 0.0000, -16.1649 >>, 50.0)	
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam3wide)
				cam3wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1934.5687, 4441.1792, 38.5273>>, <<-10.8157, -0.0428, 50.6593>>, 38.4614)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam4wide)
				cam4wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1936.4519, 4441.9355, 38.0959>>, <<-9.9336, -0.0428, 47.3525>>, 38.4614)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam5wide)
				cam5wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1943.6908, 4444.6997, 38.5172>>, <<5.3246, -0.0005, -111.0771>>, 39.9908)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam6wide)
				cam6wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1943.2024, 4444.0132, 37.3637>>, <<2.6624, -0.0005, -104.1701>>, 39.9908)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    Cameras made")
			IF NOT bMakeSwapCams
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    NOT bMakeSwapCams")
			#IF OFFROAD_USE_SWAP_SCENES
				SET_CAM_ACTIVE(cam1wide, TRUE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    set came1wide active")
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    rendering script cams")
			#ENDIF
			ENDIF
			bCamerasMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    bCamerasMade = TRUE")
		BREAK
		
		CASE RidgeRun
			CPRINTLN(DEBUG_OR_RACES, "Making cutscene cams for RidgeRun")
			iCutInterpDur = 7000
			IF NOT DOES_CAM_EXIST(cam2wide)
				vCutsceneCamDest = <<-518.5645, 2011.6864, 205.3615>>
				cam2wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCutsceneCamDest, <<-10.1688, -0.2245, 20.8203>>, 44.0808)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1wide)
				cam1wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-519.1605, 2013.0159, 205.5103>>, <<-9.1715, -0.2245, 22.5841>>, 44.0808)//CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1943.7, 4444.8, 48.2 >>, << 0.7991, 0.0000, -16.1649 >>, 50.0)	
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam3wide)
				cam3wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-515.6855, 2007.8528, 206.1012>>, <<-0.4035, -0.1892, 163.2478>>, 27.6830)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam4wide)
				cam4wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-515.7711, 2004.5834, 206.0598>>, <<-0.3385, -0.1892, 163.0306>>, 27.6830)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam5wide)
				cam5wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-518.4795, 1993.7391, 207.9673>>, <<-7.2953, -0.2214, -3.5218>>, 45.9240)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam6wide)
				cam6wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-518.5731, 1993.9401, 206.8376>>, <<-8.4426, -0.2214, -5.8787>>, 45.9240)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    Cameras made")
			IF NOT bMakeSwapCams
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    NOT bMakeSwapCams")
			#IF OFFROAD_USE_SWAP_SCENES
				SET_CAM_ACTIVE(cam1wide, TRUE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    set came1wide active")
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    rendering script cams")
			#ENDIF
			ENDIF
			bCamerasMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    bCamerasMade = TRUE")
		BREAK
		
		CASE ValleyTrail
			CPRINTLN(DEBUG_OR_RACES, "Making cutscene cams for ValleyTrail")
			iCutInterpDur = 7000
			IF NOT DOES_CAM_EXIST(cam2wide)
				vCutsceneCamDest = <<-229.3903, 4226.5596, 45.0182>>
				cam2wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-229.2285, 4226.6611, 45.0023>>, <<-0.8733, 0.0915, 84.0911>>, 41.6574)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1wide)
				cam1wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-230.9695, 4226.9219, 45.0704>>, <<2.3940, -0.0406, 84.2233>>, 41.6574)//CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1943.7, 4444.8, 48.2 >>, << 0.7991, 0.0000, -16.1649 >>, 50.0)	
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam3wide)
				cam3wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-230.0989, 4226.4985, 45.2705>>, <<-6.5795, -0.1670, -125.0428>>, 39.0100)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam4wide)
				cam4wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-228.9741, 4225.8940, 45.1399>>, <<-3.7667, -0.1670, -129.7411>>, 39.0100)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam5wide)
				cam5wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-221.3025, 4223.6846, 46.7736>>, <<-2.7368, -0.0008, 70.4824>>, 41.6574)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam6wide)
				cam6wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-221.6005, 4223.7905, 45.3800>>, <<-4.8182, -0.0008, 70.4824>>, 41.6574)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    Cameras made")
			IF NOT bMakeSwapCams
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    NOT bMakeSwapCams")
			#IF OFFROAD_USE_SWAP_SCENES
				SET_CAM_ACTIVE(cam1wide, TRUE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    set came1wide active")
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    rendering script cams")
			#ENDIF
			ENDIF
			bCamerasMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    bCamerasMade = TRUE")
		BREAK
		
		CASE LakesideSplash
			CPRINTLN(DEBUG_OR_RACES, "Making cutscene cams for LakesideSplash")
			iCutInterpDur = 7000
			IF NOT DOES_CAM_EXIST(cam2wide)
				vCutsceneCamDest = <<1606.3958, 3839.9255, 34.8244>>
				cam2wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCutsceneCamDest, <<-2.9033, -1.8249, -46.6085>>, 40.1711)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1wide)
				cam1wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1607.8865, 3841.3264, 34.7757>>, <<-0.5879, -1.9369, -45.4532>>, 40.1711)//CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1943.7, 4444.8, 48.2 >>, << 0.7991, 0.0000, -16.1649 >>, 50.0)	
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam3wide)
				cam3wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1607.9375, 3837.9385, 34.4804>>, <<-4.4038, -1.7706, 108.5596>>, 40.1711)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam4wide)
				cam4wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1606.5629, 3837.2422, 34.3664>>, <<-3.1316, -1.7706, 107.7553>>, 40.1711)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam5wide)
				cam5wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1599.7683, 3833.8889, 35.7868>>, <<0.5473, -1.8780, -48.6451>>, 40.1711)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam6wide)
				cam6wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1599.6630, 3833.7466, 34.6625>>, <<-2.6611, -1.8780, -48.6451>>, 40.1711)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    Cameras made")
			IF NOT bMakeSwapCams
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    NOT bMakeSwapCams")
			#IF OFFROAD_USE_SWAP_SCENES
				SET_CAM_ACTIVE(cam1wide, TRUE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    set came1wide active")
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    rendering script cams")
			#ENDIF
			ENDIF
			bCamerasMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    bCamerasMade = TRUE")
		BREAK
		
		CASE EcoFriendly
			CPRINTLN(DEBUG_OR_RACES, "Making cutscene cams for EcoFriendly")
			iCutInterpDur = 7000
			IF NOT DOES_CAM_EXIST(cam2wide)
				vCutsceneCamDest = <<2044.4916, 2130.2898, 93.3795>>
				cam2wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2044.5486, 2130.0422, 93.4643>>, <<-2.8529, 0.2457, -129.0125>>, 40.8194)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1wide)
				cam1wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2047.2976, 2128.2144, 93.4141>>, <<0.7893, 0.1513, -125.6504>>, 40.8194)//CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1943.7, 4444.8, 48.2 >>, << 0.7991, 0.0000, -16.1649 >>, 50.0)	
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam3wide)
				cam3wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2041.6832, 2131.1018, 94.2686>>, <<-5.0825, -0.0000, 76.3259>>, 44.5242)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam4wide)
				cam4wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2039.3386, 2132.0979, 94.0928>>, <<-5.3187, -0.0000, 84.7292>>, 44.5242)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam5wide)
				cam5wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2030.2577, 2139.1392, 96.2295>>, <<-2.6047, -0.0000, -130.7596>>, 44.5242)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam6wide)
				cam6wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2031.4091, 2137.7559, 94.4353>>, <<-3.9361, -0.0000, -134.2198>>, 44.5242)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    Cameras made")
			IF NOT bMakeSwapCams
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    NOT bMakeSwapCams")
			#IF OFFROAD_USE_SWAP_SCENES
				SET_CAM_ACTIVE(cam1wide, TRUE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    set came1wide active")
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    rendering script cams")
			#ENDIF
			ENDIF
			bCamerasMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    bCamerasMade = TRUE")
		BREAK
		
		CASE MinewardSpiral
			CPRINTLN(DEBUG_OR_RACES, "Making cutscene cams for MinewardSpiral")
			iCutInterpDur = 7000
			IF NOT DOES_CAM_EXIST(cam2wide)
				vCutsceneCamDest = <<2996.6907, 2782.5027, 43.6393>>
				cam2wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2996.8455, 2782.2297, 43.4970>>, <<3.4209, 0.0045, 33.4847>>, 47.0393)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1wide)
				cam1wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2995.9468, 2783.4854, 43.6447>>, <<5.1925, 0.0045, 36.8585>>, 47.0393)//CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1943.7, 4444.8, 48.2 >>, << 0.7991, 0.0000, -16.1649 >>, 50.0)	
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam3wide)
				cam3wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2996.1445, 2780.4768, 43.3431>>, <<-6.6951, 0.0799, 163.7419>>, 41.8987)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam4wide)
				cam4wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2996.4949, 2777.8755, 43.1067>>, <<-4.1896, 0.0799, 147.3135>>, 41.8987)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(cam5wide)
				cam5wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2996.1218, 2771.0967, 44.5126>>, <<0.6591, -0.0658, 6.9874>>, 42.7619)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam6wide)
				cam6wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<2996.1218, 2771.1121, 43.1661>>, <<0.6591, -0.0658, 6.9874>>, 42.7619)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    Cameras made")
			IF NOT bMakeSwapCams
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    NOT bMakeSwapCams")
			#IF OFFROAD_USE_SWAP_SCENES
				SET_CAM_ACTIVE(cam1wide, TRUE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    set came1wide active")
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    rendering script cams")
			#ENDIF
			ENDIF
			bCamerasMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    bCamerasMade = TRUE")
		BREAK
		
		CASE ConstructionCourse
			iCutInterpDur = 7000
			IF NOT DOES_CAM_EXIST(cam2wide)
				vCutsceneCamDest = << 1169.6823, 2389.4417, 58.4490 >>
				cam2wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCutsceneCamDest, << -5.1495, 0.0000, 179.1833 >>, 45)
			ENDIF
			IF NOT DOES_CAM_EXIST(cam1wide)
				cam1wide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 1163.7627, 2427.6836, 64.9889 >>, << -4.8554, -0.0000, 123.0232 >>, 50.0, FALSE)
			ENDIF
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    Cameras made")
			IF NOT bMakeSwapCams
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    NOT bMakeSwapCams")
			#IF OFFROAD_USE_SWAP_SCENES
				SET_CAM_ACTIVE(cam1wide, TRUE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    set came1wide active")
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    rendering script cams")
			#ENDIF
			ENDIF
			bCamerasMade = TRUE
			CDEBUG1LN(DEBUG_OR_RACES, "makeCutsceneCams:    bCamerasMade = TRUE")
		BREAK
			
	ENDSWITCH
ENDPROC

PROC set_recording_value()
	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
		
		CASE CanyonCliffs
//			iCutRecording[0] = 101
			iCutRecording[1] = 102
			iCutRecording[2] = 103
			iCutRecording[3] = 104
			iCutRecording[4] = 105
			iCutRecording[5] = 106
		BREAK
		
		CASE RidgeRun
//			iCutRecording[0] = 111
			iCutRecording[1] = 112
			iCutRecording[2] = 113
			iCutRecording[3] = 114
			iCutRecording[4] = 115
		BREAK
		
		CASE ValleyTrail
//			iCutRecording[0] = 121
			iCutRecording[1] = 122
			iCutRecording[2] = 123
			iCutRecording[3] = 124
			iCutRecording[4] = 125
			iCutRecording[5] = 126
		BREAK
		
		CASE LakesideSplash
//			iCutRecording[0] = 131
			iCutRecording[1] = 132
			iCutRecording[2] = 133
			iCutRecording[3] = 134
			iCutRecording[4] = 135
		BREAK
		
		CASE EcoFriendly
//			iCutRecording[0] = 141
			iCutRecording[1] = 142
			iCutRecording[2] = 143
			iCutRecording[3] = 144
			iCutRecording[4] = 145
			iCutRecording[5] = 146
		BREAK
		
		CASE MinewardSpiral
//			iCutRecording[0] = 151
			iCutRecording[1] = 152
			iCutRecording[2] = 153
			iCutRecording[3] = 154
			iCutRecording[4] = 155
			iCutRecording[5] = 156
		BREAK
		CASE ConstructionCourse
//			iCutRecording[0] = 151
			iCutRecording[1] = 161
			iCutRecording[2] = 162
			iCutRecording[3] = 163
			iCutRecording[4] = 164
			iCutRecording[5] = 165
		BREAK
		
	ENDSWITCH
ENDPROC


PROC request_Cutscene_assets()
	CDEBUG1LN(DEBUG_OR_RACES, "Inside request Cutscenes assets Proc")
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	set_recording_value()
	
//	REQUEST_VEHICLE_RECORDING(iCutRecording[0], "SPROffroadCut")
//	REQUEST_VEHICLE_RECORDING(iCutRecording[1], "SPROffroadCut")
//	REQUEST_VEHICLE_RECORDING(iCutRecording[2], "SPROffroadCut")
//	REQUEST_VEHICLE_RECORDING(iCutRecording[3], "SPROffroadCut")
//	REQUEST_VEHICLE_RECORDING(iCutRecording[4], "SPROffroadCut")
			
	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
		
		CASE CanyonCliffs
//			REQUEST_WAYPOINT_RECORDING("offroad_cut_01")
			REQUEST_VEHICLE_RECORDING(iCutRecording[5], "SPROffroadCut")	
		BREAK
		
		CASE RidgeRun	
//			REQUEST_WAYPOINT_RECORDING("offroad_cut_02")
		BREAK
		
		CASE ValleyTrail
//			REQUEST_WAYPOINT_RECORDING("offroad_cut_03")
//			REQUEST_VEHICLE_RECORDING(iCutRecording[5], "SPROffroadCut")
		BREAK
		
		CASE LakesideSplash
//			REQUEST_WAYPOINT_RECORDING("offroad_cut_04")
		BREAK
		
		CASE EcoFriendly
//			REQUEST_WAYPOINT_RECORDING("offroad_cut_05")
//			REQUEST_VEHICLE_RECORDING(iCutRecording[5], "SPROffroadCut")
		BREAK
		
		CASE MinewardSpiral
//			REQUEST_WAYPOINT_RECORDING("offroad_cut_06")
//			REQUEST_VEHICLE_RECORDING(iCutRecording[5], "SPROffroadCut")
		BREAK
		
		CASE ConstructionCourse
//			REQUEST_WAYPOINT_RECORDING("offroad_cut_07")
//			REQUEST_VEHICLE_RECORDING(iCutRecording[5], "SPROffroadCut")
		BREAK
		
	ENDSWITCH

ENDPROC


PROC TeleportPlayerVehicle(ORR_RACE_STRUCT& Race, VECTOR vWarp, FLOAT fWarpHead)	
	VECTOR vWarpDistCheck = << 1.0, 1.0, 1.0 >>

	IF (Race.Racer[0].Vehicle = donorVeh)
		IF DOES_ENTITY_EXIST(playerVeh)
			IF NOT IS_ENTITY_DEAD(playerVeh)
				IF NOT IS_ENTITY_AT_COORD(playerVeh, vWarp, vWarpDistCheck)
					SET_ENTITY_AS_MISSION_ENTITY(playerVeh)
					SET_ENTITY_COORDS(playerVeh, vWarp)				
					SET_ENTITY_HEADING(playerVeh, fWarpHead)
					SET_VEHICLE_ON_GROUND_PROPERLY(playerVeh)
					
					CDEBUG1LN(DEBUG_OR_RACES, "playerVeh teleported")
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(donorPed)	
				SET_ENTITY_AS_MISSION_ENTITY(donorPed, TRUE, TRUE)
				DELETE_PED(donorPed)
				CDEBUG1LN(DEBUG_OR_RACES, "player showed up without a vehicle, destroying donor ped")				
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_OR_RACES, "TeleportPlayerVehicle:  playerVeh is dead. BAD!!!")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_OR_RACES, "TeleportPlayerVehicle:  playerVeh is the race vehicle. NOT teleporting")
	ENDIF
ENDPROC
	
PROC ActivatePlayerVehicleTeleport(ORR_RACE_STRUCT& Race)
VECTOR vPlayerVehEoR // End of Race warp location for player vehicle
FLOAT fPlayerHeadEor // End of Race warp heading for player vehicle

	IF NOT IS_BITMASK_AS_ENUM_SET(iORCutsceneBits, CutsceneBit4)
		vPlayerVehEoR = Race.vPlayerCarPos
		fPlayerHeadEor = Race.fPlayerCarHeading
		
		TeleportPlayerVehicle(Race, vPlayerVehEoR, fPlayerHeadEor)
		SET_BITMASK_AS_ENUM(iORCutsceneBits, CutsceneBit4)
		CDEBUG1LN(DEBUG_OR_RACES, "ActivatePlayerVehicleTeleport: Teleported Player Vehicle to end of race")
	
	ELSE
		CDEBUG1LN(DEBUG_OR_RACES, "ActivatePlayerVehicleTeleport: teleport vehicle allready done, skipping")
	ENDIF
	
ENDPROC

FUNC BOOL makeDonorPed(ORR_RACE_STRUCT& Race, PED_INDEX piDonor)

	IF NOT IS_BITMASK_AS_ENUM_SET(iORCutsceneBits, CutsceneBit1)
		//REQUEST_MODEL(a_m_y_motox_01)
		SET_BITMASK_AS_ENUM(iORCutsceneBits, CutsceneBit1)
	ELSE
		RETURN TRUE
	ENDIF
	
//	IF HAS_MODEL_LOADED(a_m_y_motox_01)
//		IF NOT DOES_ENTITY_EXIST(donorPed)
//			donorPed = CREATE_PED(PEDTYPE_CIVMALE, a_m_y_motox_01, Race.Racer[0].vStartPos, Race.Racer[0].fStartHead)
//			RETURN TRUE
//		ELSE
//			RETURN TRUE
//		ENDIF
//	ENDIF
	IF NOT IS_ENTITY_DEAD(piDonor)
		donorPed = piDonor
		SET_ENTITY_COORDS(donorPed, Race.Racer[0].vStartPos)
		SET_ENTITY_HEADING(donorPed, Race.Racer[0].fStartHead)
		CPRINTLN( DEBUG_OR_RACES, "Setting donor ped coords..." )
		RETURN TRUE
	ELSE 
		CPRINTLN( DEBUG_OR_RACES, "Creating new donor ped at player..." )
		donorPed = CREATE_PED(PEDTYPE_CIVMALE, a_m_y_motox_01, Race.Racer[0].vStartPos, Race.Racer[0].fStartHead)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL setupSwapVehicle(VECTOR vVehPosition, FLOAT fVehHeading)
	PED_INDEX seatPed
	
	IF DOES_ENTITY_EXIST(donorPed)
		IF DOES_ENTITY_EXIST(playerVeh)
			IF NOT IS_ENTITY_DEAD(playerVeh)
				IF NOT IS_ENTITY_DEAD(donorPed)
					seatPed = GET_PED_IN_VEHICLE_SEAT(playerVeh, VS_DRIVER)
					IF DOES_ENTITY_EXIST(seatPed)
						SPECIAL_FUNCTION_DO_NOT_USE(seatPed)
					ENDIF
					
					SET_PED_INTO_VEHICLE(donorPed, playerVeh, VS_DRIVER)
					CDEBUG1LN(DEBUG_OR_RACES, "setupSwapVehicle:  donerped in playerVeh")
					SET_ENTITY_COORDS(playerVeh, vVehPosition)
					SET_ENTITY_HEADING(playerVeh, fVehHeading)
					SET_VEHICLE_ON_GROUND_PROPERLY(playerVeh)
					FREEZE_ENTITY_POSITION(playerVeh, TRUE)
					CDEBUG1LN(DEBUG_OR_RACES, "setupSwapVehicle:  teleported playerVeh")
					CDEBUG1LN(DEBUG_OR_RACES, "setupSwapVehicle: Return True")
					RETURN TRUE					
				ENDIF
			ENDIF
		ELIF NOT IS_ENTITY_DEAD(donorPed)
			SET_ENTITY_COORDS(donorPed, vVehPosition)
			SET_ENTITY_HEADING(donorPed, fVehHeading)
			TASK_STAND_STILL(donorPed, -1)	
			CDEBUG1LN(DEBUG_OR_RACES, "setupSwapVehicle: Return True")
			RETURN TRUE			
		ENDIF
	ENDIF
	RETURN FALSE
	
ENDFUNC

FUNC BOOL setupSwapCutscene()
VECTOR swapPosition
FLOAT swapHeading

	// Teleport the players car based on race
	// Put cutscene ped in players car
	SWITCH INT_TO_ENUM(ORR_RACE_NAMES, ORR_Master.iRaceCur)
		CASE CanyonCliffs
			swapPosition = << -1947.0811, 4449.4565, 35.2390 >>
			swapHeading = 53.1715
			IF setupSwapVehicle(swapPosition, swapHeading)
				CDEBUG1LN(DEBUG_OR_RACES, "setupSwapCutscene:  Teleported playerVeh; returning TRUE")
				RETURN TRUE
			ENDIF
		BREAK
		CASE RidgeRun
			swapPosition = <<-516.7096, 1996.0557, 204.9577>>
			swapHeading = 297.2036
			IF setupSwapVehicle(swapPosition, swapHeading)
				CDEBUG1LN(DEBUG_OR_RACES, "setupSwapCutscene:  Teleported playerVeh; returning TRUE")
				RETURN TRUE
			ENDIF	
		BREAK
		CASE ValleyTrail
			swapPosition = << -215.0797, 4218.8267, 43.6957 >>
			swapHeading = 255.7209
			IF setupSwapVehicle(swapPosition, swapHeading)
				CDEBUG1LN(DEBUG_OR_RACES, "setupSwapCutscene:  Teleported playerVeh; returning TRUE")
				RETURN TRUE
			ENDIF
		BREAK
		CASE LakesideSplash
			swapPosition = << 1592.9427, 3838.9866, 31.7572 >>
			swapHeading = 127.2442
			IF setupSwapVehicle(swapPosition, swapHeading)
				CDEBUG1LN(DEBUG_OR_RACES, "setupSwapCutscene:  Teleported playerVeh; returning TRUE")
				RETURN TRUE
			ENDIF
		BREAK
		CASE EcoFriendly
			swapPosition = << 2027.0774, 2129.2927, 92.5260 >>
			swapHeading = 134.3667
			IF setupSwapVehicle(swapPosition, swapHeading)
				CDEBUG1LN(DEBUG_OR_RACES, "setupSwapCutscene:  Teleported playerVeh; returning TRUE")
				RETURN TRUE
			ENDIF
		BREAK
		CASE MinewardSpiral
			swapPosition = << 2980.8103, 2758.7708, 41.8429 >>
			swapHeading = 119.8546
			IF setupSwapVehicle(swapPosition, swapHeading)
				CDEBUG1LN(DEBUG_OR_RACES, "setupSwapCutscene:  Teleported playerVeh; returning TRUE")
				RETURN TRUE
			ENDIF
		BREAK
		CASE ConstructionCourse
			swapPosition = 	<< 1167.8413, 2380.8037, 56.6236 >>
			swapHeading = 45.4084
			IF setupSwapVehicle(swapPosition, swapHeading)
				CDEBUG1LN(DEBUG_OR_RACES, "setupSwapCutscene:  Teleported playerVeh; returning TRUE")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC RemoveDonorPed()
	CPRINTLN( DEBUG_OR_RACES, "RemoveDonorPed called" )
	IF DOES_ENTITY_EXIST(donorPed)
		CPRINTLN( DEBUG_OR_RACES, "Donor ped exists" )
		IF NOT IS_ENTITY_DEAD(donorPed)
			CPRINTLN( DEBUG_OR_RACES, "Deleting donor ped" )
			SET_ENTITY_AS_MISSION_ENTITY(donorPed, TRUE, TRUE)
			DELETE_PED(donorPed)
		ENDIF
	ENDIF
ENDPROC

PROC UnfreezePlayerVehicle()
	IF NOT IS_ENTITY_DEAD(playerVeh)
		FREEZE_ENTITY_POSITION(playerVeh, FALSE)
	ENDIF
	RemoveDonorPed()
ENDPROC

PROC ORR_OffRoad_SetupOutfits()
	IF ORR_DOES_RACE_REQUIRE_MOTORCYCLE()		
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 000, 0) //B* 1549346 but doing this for all characters just in case DLC hair (nothing conflicts on michael/franklin currently)
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			//SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOTO_X ,FALSE )
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			
//			PED_COMP_NAME_ENUM 
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_MOTO_X, FALSE)
			//SET_PED_COMPONENT_VARIATION(Race.Racer[0].Driver, PED_COMP_TORSO, 	9, 0)
			//SET_PED_COMPONENT_VARIATION(Race.Racer[0].Driver, PED_COMP_LEG, 	14, 0)
			//SET_PED_COMPONENT_VARIATION(Race.Racer[0].Driver, PED_COMP_FEET, 	6, 	0)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			
			/* TRAILER 2 - need to force Franklins model to A_M_Y_MOTOX_01 */
			//SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
			
//			SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, 6, 0)
//			
//			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HEAD, 000, 0)
//			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 000, 0)
//			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 020, 0)
//			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 017, 0)
//			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 000, 0)
//			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 005, 0)
//			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 000, 0)
//			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 000, 0)
//			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 000, 0)
//			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 000, 0)
			SET_PED_COMP_ITEM_CURRENT_SP( PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_MOTO_X, FALSE )
			
			/* */
			
			/* TRAILER 2 - need to force Franklins model to A_M_Y_MOTOX_01 
			SET_PLAYER_MODEL(PLAYER_ID(), A_M_Y_MOTOX_01)
			SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
			/* */
			
		ENDIF
		
	ENDIF
ENDPROC

PROC ORR_Race_Draw_Hud(ORR_RACE_STRUCT& Race)
	IF IS_RADAR_HIDDEN()
		DISPLAY_RADAR(TRUE)
	ENDIF
	
	// Check whether or not to display best time.
	STRING szBestORTimeLabel
	INT iBesORTime
	
	IF (g_savedGlobals.sOffroadData.fBestTime[ORR_Master.iRaceCur] > 0.0)
		szBestORTimeLabel 	= "SPR_TIMEBEST" 
		iBesORTime			= CEIL(g_savedGlobals.sOffroadData.fBestTime[ORR_Master.iRaceCur]*1000)
	ELSE
		szBestORTimeLabel 	= "-1"
		iBesORTime			= -1
	ENDIF
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	
	DRAW_CHECKPOINT_COUNTDOWN_RACE_HUD(FLOOR(Race.Racer[0].fClockTime*1000), // RaceTime - The timer
		"", 									// TimerTitle - The title of the timer. Defaults to TIME with "" passed in
		-1, 									// LapNumber - Number of laps
		-1, 									// LapMaxNumber - Max number of laps
		"", 									// LapTitle - The title of the lap meter. Defaults to LAP with "" Passed in
		Race.Racer[0].iRank, 					// PositionNum - The position Number
		Race.iRacerCnt, 						// PositionMaxNumber - The position maximum number
		"", 									// PositionTitle - The title of the position number. Defaults to POSITION with "" Passed in 
		CEIL(Race.Racer[0].fPlsMnsTot*1000), 	// ExtraTimeGiven - If any extra time is given to the timer, pass this in to display +xs -xs
		HUD_COLOUR_WHITE, 						// PlacementColour - The position numbers can change colour
		/*Race.Racer[0].iGateCur*/-1, 			// CheckpointNumber - if you have a checkpoint bar the current number
		/*Race.iGateCnt*/-1, 					// CheckpointMaxNum - if you have a checkpoint bar, the maximum number
	    "SPR_GATES", 							// CheckpointTitle - The title of the checkpoint bar, defaults to CHECKPOINT with "" passed in
		HUD_COLOUR_YELLOW, 						// CheckpointColour - the colour the bar should be
		-1, 									// MeterNumber - if you want a meter displayed pass in the current value
		-1, 									// MeterMaxNum - If you want a meter displayed pass in the max value
		"", 									// MeterTitle - the meters title. Defaults to DAMAGE with "" Passed in
		HUD_COLOUR_YELLOW, 						// MeterColour - The meter colour
		iBesORTime, 							// BestTime - If you want to show a best time then pass in a millisecond value
		szBestORTimeLabel, 						// BestTimeTitle - Title of the best time timer. Defaults to BEST TIME with "" passed in
		PODIUMPOS_NONE,							// MedalDisplay - If you want to show a medal postion next to the Best timer then pass in a position. Used for a target times.
		// INT_TO_ENUM(PODIUMPOS, g_savedGlobals.sSPRData.OffData.structSPR[SPR_Master.iRaceCur].medalscore),
		TRUE, 									// DisplayMilliseconds - True if you want the main timer to display milliseconds
		-1) 									// FlashingTime - How long you want the whole hud to flash for.
ENDPROC


///    
/// OFFROAD RACE AUDIO
///    

PROC OFFROAD_CRASH_AUDIO(ORR_RACER_STRUCT& Racer, STRING sLine, INT &iBitmaskInt)
	IF (Racer.Driver = PLAYER_PED_ID())
		IF NOT IS_PED_IN_VEHICLE(Racer.Driver, Racer.Vehicle)
			IF IS_PED_RAGDOLL(PLAYER_PED_ID())
				IF NOT bRagdollCheck
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//	AND NOT IS_THIS_PRINT_BEING_DISPLAYED("SPR_EXIT_WARN")
					AND NOT IS_MESSAGE_BEING_DISPLAYED()	
						// play specified single line		
						TEXT_LABEL tLine = sLine
						tLine = ""				
						IF ((NOT IS_BITMASK_AS_ENUM_SET(iBitmaskInt, CrashMessage4))
						AND (IS_BITMASK_AS_ENUM_SET(iBitmaskInt, CrashMessage3)))
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									tLine = "MICHAEL_NORMAL"
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									tLine = "TREVOR_NORMAL"
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									tLine = "FRANKLIN_NORMAL"
								ENDIF
								PRINTSTRING(tLine)
								UNUSED_PARAMETER(tLine)
								PRINTNL()
								IF (GET_RANDOM_INT_IN_RANGE(0, 10000) > 7500)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "CRASH_GENERIC", tLine, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
									//PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "SPR_OR", sLine, tLine, CONV_PRIORITY_LOW)
									SET_BITMASK_AS_ENUM(iBitmaskInt, CrashMessage4)
									bRagdollCheck = TRUE
								ENDIF
							ENDIF
						ELIF ((NOT IS_BITMASK_AS_ENUM_SET(iBitmaskInt, CrashMessage3))
						AND (IS_BITMASK_AS_ENUM_SET(iBitmaskInt, CrashMessage2)))
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									tLine = "MICHAEL_NORMAL"
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									tLine = "TREVOR_NORMAL"
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									tLine = "FRANKLIN_NORMAL"
								ENDIF
								PRINTSTRING(tLine)
								PRINTNL()
								IF (GET_RANDOM_INT_IN_RANGE(0, 10000) > 7500)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "CRASH_GENERIC", tLine, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
									SET_BITMASK_AS_ENUM(iBitmaskInt, CrashMessage3)
									bRagdollCheck = TRUE
								ENDIF
							ENDIF
						ELIF ((NOT IS_BITMASK_AS_ENUM_SET(iBitmaskInt, CrashMessage2))
						AND (IS_BITMASK_AS_ENUM_SET(iBitmaskInt, CrashMessage1)))
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									tLine = "MICHAEL_NORMAL"
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									tLine = "TREVOR_NORMAL"
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									tLine = "FRANKLIN_NORMAL"
								ENDIF
								PRINTSTRING(tLine)
								PRINTNL()
								IF (GET_RANDOM_INT_IN_RANGE(0, 10000) > 7500)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "CRASH_GENERIC", tLine, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
									SET_BITMASK_AS_ENUM(iBitmaskInt, CrashMessage2)
									bRagdollCheck = TRUE
								ENDIF
							ENDIF
						ELIF ((NOT IS_BITMASK_AS_ENUM_SET(iBitmaskInt, CrashMessage1))
						AND (IS_BITMASK_AS_ENUM_SET(iBitmaskInt, CrashMessage0)))
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									tLine = "MICHAEL_NORMAL"
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									tLine = "TREVOR_NORMAL"
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									tLine = "FRANKLIN_NORMAL"
								ENDIF
								PRINTSTRING(tLine)
								PRINTNL()
								IF (GET_RANDOM_INT_IN_RANGE(0, 10000) > 7500)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "CRASH_GENERIC", tLine, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
									SET_BITMASK_AS_ENUM(iBitmaskInt, CrashMessage1)
									bRagdollCheck = TRUE
								ENDIF
							ENDIF
						ELIF NOT IS_BITMASK_AS_ENUM_SET(iBitmaskInt, CrashMessage0)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									tLine = "MICHAEL_NORMAL"
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									tLine = "TREVOR_NORMAL"
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									tLine = "FRANKLIN_NORMAL"
								ENDIF
								PRINTSTRING(tLine)
								PRINTNL()
								IF (GET_RANDOM_INT_IN_RANGE(0, 10000) > 7500)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "CRASH_GENERIC", tLine, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
									SET_BITMASK_AS_ENUM(iBitmaskInt, CrashMessage0)
									bRagdollCheck = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// ped is no longer ragdoll and should be messaged to get back on bike
				IF NOT IS_TIMER_STARTED(ExitVehicleTimer)
					START_TIMER_NOW_SAFE(ExitVehicleTimer)
					CDEBUG1LN(DEBUG_OR_RACES, "Started EXIT VEHICLE TIMER")
				ELSE
					IF (GET_TIMER_IN_SECONDS(ExitVehicleTimer) >= 0.0)
						IF NOT IS_BITMASK_AS_ENUM_SET(iExitVehicelBit, vehicleExited)
							PRINT_NOW_ONCE("SPR_EXIT_WARN", 10000, 0, ORR_HELP_BIT, ORR_EXIT_WARN)		
							IF IS_THIS_PRINT_BEING_DISPLAYED("SPR_EXIT_WARN")
								SET_BITMASK_AS_ENUM(iExitVehicelBit, vehicleExited)
								CDEBUG1LN(DEBUG_OR_RACES, "EXIT_VEHICLE_FAILURE: Bit set for vehicle exit message")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			bRagdollCheck = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC OFFROAD_GESTURES(ORR_RACE_STRUCT& Race)
	PED_INDEX flipper
	flipper = Get_Closest_Racer_To_Player(Race)
	IF NOT DOES_ENTITY_EXIST(flipper)
		SCRIPT_ASSERT("ped doesn't exist")
	ENDIF
	
//	SEQUENCE_INDEX seq
//	OPEN_SEQUENCE_TASK(seq)
//		
//	CLOSE_SEQUENCE_TASK()
	INT chance
	chance = GET_RANDOM_INT_IN_RANGE()
	IF chance%100 > 50 // 50 % chance
		IF flipper = PLAYER_PED_ID()
		
		ELSE
			IF NOT IS_PED_INJURED(flipper) AND IS_PED_IN_ANY_VEHICLE(flipper) AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(flipper)) < 16
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				TASK_DRIVE_BY(flipper, PLAYER_PED_ID(), NULL, <<0, 0, 0>>, 10, 75, TRUE, FIRING_PATTERN_FULL_AUTO )
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC ORR_PLAY_RACER_RANKDN_DIALOGUE()
	//TEXT_LABEL tLine = "TAUNT_DN_"
	//tLine += GET_RANDOM_INT_IN_RANGE(1, 6)
	//PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "SPR_OR", "TAUNT_DN", tLine, CONV_PRIORITY_LOW)
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "RACE_RANKDOWN", "MICHAEL_NORMAL", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)		
		BREAK
		CASE CHAR_FRANKLIN
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "RACE_RANKDOWN", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
		BREAK
		CASE CHAR_TREVOR
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "RACE_RANKDOWN", "TREVOR_NORMAL", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)			
		BREAK
	ENDSWITCH
ENDPROC

PROC ORR_PLAY_RACER_RANKUP_DIALOGUE()
//	TEXT_LABEL tLine = "TAUNT_UP_"
//	tLine += GET_RANDOM_INT_IN_RANGE(1, 6)
//	PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "SPR_OR", "TAUNT_DN", tLine, CONV_PRIORITY_LOW)
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "RACE_RANKUP", "MICHAEL_NORMAL", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)		
		BREAK
		CASE CHAR_FRANKLIN
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "RACE_RANKUP", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
		BREAK
		CASE CHAR_TREVOR
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "RACE_RANKUP", "TREVOR_NORMAL", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)			
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_RACER_CHANGED_RANK(ORR_RACE_STRUCT& race, BOOL& bRankedUp)
	BOOL bRetVal = FALSE
	IF (GET_TIMER_IN_SECONDS(race.tClock) > 10.0)
		IF NOT IS_ENTITY_DEAD(race.Racer[0].Driver)
			iPlayerCurrentRank = Race.Racer[0].iRank
			
			IF iPlayerPreviousRank <> -1 AND iPlayerPreviousRank <> 0			
				IF iPlayerCurrentRank <> iPlayerPreviousRank
					bRetVal = TRUE
					
					//< new rank means we ranked up!
					IF iPlayerCurrentRank < iPlayerPreviousRank
						bRankedUp = TRUE
						CPRINTLN(DEBUG_OR_RACES, "Player has ranked up...")
					ELSE
						bRankedUp = FALSE
						CPRINTLN(DEBUG_OR_RACES, "Player has ranked down... Was rank ", iPlayerPreviousRank, " now is rank ", iPlayerCurrentRank)
					ENDIF
					iPlayerPreviousRank = iPlayerCurrentRank
				ENDIF
			ELSE
				iPlayerPreviousRank = iPlayerCurrentRank
			ENDIF
		ENDIF
	ENDIF		

	RETURN bRetVal
ENDFUNC

FUNC TEXT_LABEL_23 GET_DIALOGUE_LABEL(BOOL bRankedUp)
	TEXT_LABEL_23 sRetString
	
	IF bRankedUp
		sRetString = "Rankup"
	ELSE	
		sRetString = "Rankdn"
	ENDIF
	
	RETURN sRetString
ENDFUNC

FUNC TEXT_LABEL_23 GET_CURRENT_DIALOGUE_LINE(TEXT_LABEL_23 sRetString, INT iLastLine, INT& iCurLine)
	enumCharacterList eCharacter = GET_CURRENT_PLAYER_PED_ENUM()
	
	sRetString += "_"
	
	INT iRandomLine = GET_RANDOM_INT_IN_RANGE(1, 6)
	
	WHILE iRandomLine = iLastLine
		iRandomLine = GET_RANDOM_INT_IN_RANGE(1, 6)
	ENDWHILE	
	
	iCurLine = iRandomLine
	SWITCH iRandomLine
		CASE 1
			SWITCH eCharacter
				CASE CHAR_MICHAEL
					sRetString += 1
				BREAK
				CASE CHAR_TREVOR
					sRetString += 6
				BREAK
				CASE CHAR_FRANKLIN
					sRetString += 11
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH eCharacter
				CASE CHAR_MICHAEL
					sRetString += 2
				BREAK
				CASE CHAR_TREVOR
					sRetString += 7
				BREAK
				CASE CHAR_FRANKLIN
					sRetString += 12
				BREAK
			ENDSWITCH		
		BREAK
		CASE 3
			SWITCH eCharacter
				CASE CHAR_MICHAEL
					sRetString += 3
				BREAK
				CASE CHAR_TREVOR
					sRetString += 8
				BREAK
				CASE CHAR_FRANKLIN
					sRetString += 13
				BREAK
			ENDSWITCH		
		BREAK
		CASE 4
			SWITCH eCharacter
				CASE CHAR_MICHAEL
					sRetString += 4
				BREAK
				CASE CHAR_TREVOR
					sRetString += 9
				BREAK
				CASE CHAR_FRANKLIN
					sRetString += 14
				BREAK
			ENDSWITCH		
		BREAK
		CASE 5
			SWITCH eCharacter
				CASE CHAR_MICHAEL
					sRetString += 5
				BREAK
				CASE CHAR_TREVOR
					sRetString += 10
				BREAK
				CASE CHAR_FRANKLIN
					sRetString += 15
				BREAK
			ENDSWITCH		
		BREAK
	ENDSWITCH
	
	RETURN sRetString
ENDFUNC

PROC PLAY_DIALOGUE_LINE(BOOL bRankedUp, ORR_RACE_STRUCT& race)
	INT iRandomChance = GET_RANDOM_INT_IN_RANGE(0, 10000)
	INT iCurLine
	CPRINTLN(DEBUG_OR_RACES, "Calling Play dialogue line. Chance is ", iRandomChance)
	IF iRandomChance > 2000
		//TEXT_LABEL root = GET_DIALOGUE_LABEL(bRankedUp)
		TEXT_LABEL dialogue = GET_CURRENT_DIALOGUE_LINE(GET_DIALOGUE_LABEL(bRankedUp), race.iLastLine, iCurLine)
		
		//PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "SPR_OR", root, dialogue, CONV_PRIORITY_LOW)	
		
		IF bRankedUp
			CPRINTLN( DEBUG_OR_RACES, "Playing rank up dialogue." )
			ORR_PLAY_RACER_RANKUP_DIALOGUE()
		ELSE	
			ORR_PLAY_RACER_RANKDN_DIALOGUE()
			CPRINTLN( DEBUG_OR_RACES, "Playing rank down dialogue." )
		ENDIF
		CPRINTLN(DEBUG_OR_RACES, "Playing dialogue line ", dialogue)
		race.iLastLine = iCurLine
	ENDIF
ENDPROC

FUNC BOOL IS_CURRENTLY_PLAYING_RANK_DIALOGUE(BOOL bRankedUp)
	TEXT_LABEL_23 sTestRoot
	TEXT_LABEL_23 sPlayingRoot
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		sPlayingRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		sTestRoot = GET_DIALOGUE_LABEL(bRankedUp)
		
		IF ARE_STRINGS_EQUAL(sPlayingRoot, sTestRoot)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PLAY_RANK_DIALOGUE(BOOL bRankedUp, ORR_RACE_STRUCT& race)
	IF IS_CURRENTLY_PLAYING_RANK_DIALOGUE(bRankedUp)
		// we're already playing rank dialogue for the same rank up/down, exit early
		EXIT
	ELSE
		//this should auto cancel an opposite rank dialogue if its playing
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			//STOP_SCRIPTED_CONVERSATION(FALSE)
//			WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				WAIT(0)
//			ENDWHILE
		ENDIF
		
		PLAY_DIALOGUE_LINE(bRankedUp, race)
		//OFFROAD_GESTURES(race)
	ENDIF
ENDPROC

PROC UPDATE_OFFROAD_RANK_DIALOGUE(ORR_RACE_STRUCT& race, BOOL bChangedRank, BOOL bRankedUp)
	IF bChangedRank
		CPRINTLN(DEBUG_OR_RACES, "Player has changed rank, playing dialogue...")
		PLAY_RANK_DIALOGUE(bRankedUp, race)
	ENDIF
ENDPROC


PROC RUN_OFFROAD_AUDIO(ORR_RACE_STRUCT& Race)
	BOOL bChangedRank, bRankedUp
	
	bChangedRank = HAS_RACER_CHANGED_RANK(Race, bRankedUp)
	FranklinOffroadSpecial(bRankedUp)
	
	UPDATE_OFFROAD_RANK_DIALOGUE(race, bChangedRank, bRankedUp)
	OFFROAD_CRASH_AUDIO(Race.Racer[0], "Crash", iCrashAudioBits)
	//OFFROAD_RANKUP_AUDIO(Race, "Rankup", iRankUpAudioBits)
	//OFFROAD_RANKDOWN_AUDIO(Race, "RankDn", iRankDnAudioBits)
ENDPROC
/// PURPOSE: Requests and triggers the crowd cheering audio
///    
/// PARAMS:
///    Race - 
///    Racer - 
PROC PLAY_OFFROAD_CHEER_AUDIO_WHEN_PLAYER_IS_NEAR_FINISH_LINE(ORR_RACE_STRUCT& Race)
	
	//Check that the player has hit the 2nd to last gate
	IF Race.Racer[0].iGateCur >= Race.iGateCnt-2
		
		#IF IS_DEBUG_BUILD
		#IF ORR_ENABLE_DEBUG_DRAW_TO_SCREEN_INFO
			DRAW_DEBUG_TEXT_2D("Player on LAST GATE", <<0.1,0.18,0>>)
		#ENDIF
		#ENDIF
		
		IF REQUEST_SCRIPT_AUDIO_BANK("CROWD_CHEER")
			
			#IF IS_DEBUG_BUILD
			#IF ORR_ENABLE_DEBUG_DRAW_TO_SCREEN_INFO
				DRAW_DEBUG_TEXT_2D("CROWD_CHEER LOADED", <<0.1,0.2,0>>)
			#ENDIF
			#ENDIF
			
			IF NOT Race.bHasStartedCheerSound
				Race.iCheerMasterSound = GET_SOUND_ID()
				CDEBUG1LN(DEBUG_OR_RACES, "[AUDIO] - Audio Bank Crowd Cheer has loaded in for first time. Start playing #",Race.iCheerMasterSound," at ", Race.sGate[Race.iGateCnt-1].vPos)
				PLAY_SOUND_FROM_COORD(Race.iCheerMasterSound, "CROWD_CHEER_MASTER", Race.sGate[Race.iGateCnt-1].vPos)
				Race.bHasStartedCheerSound = TRUE
			ELSE
				IF HAS_SOUND_FINISHED(Race.iCheerMasterSound)
					CDEBUG1LN(DEBUG_OR_RACES, "[AUDIO] - Audio Bank Crowd Cheer has loaded in. Start playing #",Race.iCheerMasterSound," at ", Race.sGate[Race.iGateCnt-1].vPos)
					PLAY_SOUND_FROM_COORD(Race.iCheerMasterSound, "CROWD_CHEER_MASTER", Race.sGate[Race.iGateCnt-1].vPos)
				ELSE
					CDEBUG1LN(DEBUG_OR_RACES, "[AUDIO] - Cheers have not finished")
				ENDIF
			ENDIF
			
//		ELSE
//			CDEBUG1LN(DEBUG_OR_RACES, "[AUDIO] - Player has hit the 2nd to last gate, requesting Audio_Bank CROWD_CHEER ")
		ENDIF
	ENDIF

ENDPROC

PROC STOP_OFFROAD_END_CHEER_AUDIO(ORR_RACE_STRUCT& Race)	
	
	CDEBUG1LN(DEBUG_OR_RACES, "[AUDIO] - Audio Bank Crowd should be STOPPED")
	STOP_SOUND(Race.iCheerMasterSound)

ENDPROC

//Clear Area For Race Start
PROC CLEAR_AREA_AROUND_ORR_POS(VECTOR vRacePos)
	
	REMOVE_ALL_SHOCKING_EVENTS(FALSE)
	
	CLEAR_AREA(vRacePos,ORR_CLEAR_AREA_RADIUS, TRUE)
	CLEAR_AREA_OF_PEDS(vRacePos, ORR_CLEAR_AREA_RADIUS)
	CLEAR_AREA_OF_VEHICLES(vRacePos,ORR_CLEAR_AREA_RADIUS)
	CLEAR_AREA_OF_PROJECTILES( vRacePos, ORR_CLEAR_AREA_RADIUS * 5 )
	STOP_FIRE_IN_RANGE(vRacePos, ORR_CLEAR_AREA_RADIUS)
	
	CDEBUG1LN(DEBUG_OR_RACES, "[START] - Area around ",vRacePos, " has been cleared for ", ORR_CLEAR_AREA_RADIUS, " meters!!!!!!!!!!!!!!!!"  )
ENDPROC

//EOF



		

