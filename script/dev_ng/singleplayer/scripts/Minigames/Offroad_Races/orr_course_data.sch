// ****************************************************************************
// Course specific load/setup functions for Off Road Races.
// ****************************************************************************

// ****************************************************************************
// Canyon Cliffs

PROC ORR_Setup_Offroad_race_05(ORR_RACE_STRUCT& Race)
	
	ORR_SAFELY_SET_SCENARIO_GROUP_ENABLED("CanyonCliffs")
	
	ORR_Race_Init(Race)
	
	Race.vDriveTo[0] = <<-1938.34, 4443.20, 37.06>>
	Race.vDriveTo[1] = <<-1930.38, 4441.90, 38.12>>
	Race.vDriveTo[2] = <<-1931.2542, 4440.2725, 37.5294>>
	
	Race.fStartTime[0] = 0.0//1.0
	Race.fStartTime[1] = 1.3//2.4
	
	Race.fIntroSpeed[0] = 2.3
	Race.fIntroSpeed[1] = 2.3
	
	Race.fRecordingStartTime = 5000.0

	Race.iGateCnt = 24
	Race.iGateSkipDist = 1
	Race.iGateLagTolerance = 4
	Race.vFinalSpawnPos = <<-234.5070, 3902.6829, 36.8104>>
	Race.fFinalSpawnHeading = 206.9569
	Race.vPlayerCarPos = <<-1952.1681, 4443.8857, 35.3932>>
	Race.fPlayerCarHeading = 227.1812
	Race.vFinalPlayerVehPos = <<-224.4458, 3894.9614, 36.4271>>
	Race.fFinalPlayerVehHeading = 178.6138
	Race.fStartBoostMin = 6.0
	Race.fStartBoostMax = 18.0
	ORR_Race_Gate_Init_All(Race)
	
	Race.vFinishCamPos = <<-236.2838, 3891.3831, 37.9726>>
	Race.vFinishCamRot = <<4.4770, -0.1545, 25.6305>>
	Race.fFOV = 35.0
	Race.vLBDCamPos = <<-235.3277, 3889.1450, 69.6057>>
	Race.vLBDCamRot = <<58.3046, -0.1946, 25.6488>>
	Race.vFinishLBDCamPos = <<-236.3619, 3905.7151, 51.1564>>
	Race.vFinishLBDCamRot = <<56.5776, 0.0000, -155.0073>>
	Race.vFinishLBDCam2Pos = <<-236.5200, 3905.3157, 38.5798>>
	Race.vFinishLBDCam2Rot = <<-6.1578, -0.0004, -146.4747>>
	
	//Speed restriction volumes
	Race.iSpeedVolumeCount = 12
	
	Race.tSpeedVolumes[0].vPos1 = <<-1841.117, 4404.597, 63.8670>>
	Race.tSpeedVolumes[0].vPos2 = <<-1765.496, 4363.641, 40.2426>>
	Race.tSpeedVolumes[0].fWidth = 9.0
	Race.tSpeedVolumes[0].fSpeed = 0.9
	
	Race.tSpeedVolumes[1].vPos1 = <<-1546.062, 4207.391, 79.6808>>
	Race.tSpeedVolumes[1].vPos2 = <<-1512.674, 4220.854, 55.5324>>
	Race.tSpeedVolumes[1].fWidth = 9.0
	Race.tSpeedVolumes[1].fSpeed = 0.9
	
	Race.tSpeedVolumes[2].vPos1 = <<-1416.089, 4194.708, 56.9551>>
	Race.tSpeedVolumes[2].vPos2 = <<-1390.473, 4169.413, 40.4339>>
	Race.tSpeedVolumes[2].fWidth = 9.0
	Race.tSpeedVolumes[2].fSpeed = -1.0
	
	Race.tSpeedVolumes[3].vPos1 = <<-1322.066, 4184.635, 70.6514>>
	Race.tSpeedVolumes[3].vPos2 = <<-1334.891, 4141.501, 54.5091>>
	Race.tSpeedVolumes[3].fWidth = 9.0
	Race.tSpeedVolumes[3].fSpeed = -1.0
	
	Race.tSpeedVolumes[4].vPos1 = <<-1178.216, 4292.785, 86.6735>>
	Race.tSpeedVolumes[4].vPos2 = <<-1237.216, 4292.785, 67.5320>>
	Race.tSpeedVolumes[4].fWidth = 14.0
	Race.tSpeedVolumes[4].fSpeed = -1.0
	
	Race.tSpeedVolumes[5].vPos1 = <<-1038.013, 4234.870, 128.0485>>
	Race.tSpeedVolumes[5].vPos2 = <<-1036.901, 4175.880, 108.7185>>
	Race.tSpeedVolumes[5].fWidth = 14.0
	Race.tSpeedVolumes[5].fSpeed = -1.0
	
	Race.tSpeedVolumes[6].vPos1 = <<-941.876, 4152.233, 139.0120>>
	Race.tSpeedVolumes[6].vPos2 = <<-1000.261, 4143.736, 122.8091>>
	Race.tSpeedVolumes[6].fWidth = 14.0
	Race.tSpeedVolumes[6].fSpeed = -1.0
	
	Race.tSpeedVolumes[7].vPos1 = <<-936.788, 4141.902, 174.5561>>
	Race.tSpeedVolumes[7].vPos2 = <<-897.218, 4098.139, 133.7419>>
	Race.tSpeedVolumes[7].fWidth = 14.0
	Race.tSpeedVolumes[7].fSpeed = 0.6
	
	Race.tSpeedVolumes[8].vPos1 = <<-871.811, 4093.706, 175.2417>>
	Race.tSpeedVolumes[8].vPos2 = <<-827.555, 4054.688, 153.6356>>
	Race.tSpeedVolumes[8].fWidth = 9.0
	Race.tSpeedVolumes[8].fSpeed = 0.8
	
	Race.tSpeedVolumes[9].vPos1 = <<-709.295, 4014.701, 141.2825>>
	Race.tSpeedVolumes[9].vPos2 = <<-659.343, 4012.503, 119.5238>>
	Race.tSpeedVolumes[9].fWidth = 9.0
	Race.tSpeedVolumes[9].fSpeed = -1.0
	
	Race.tSpeedVolumes[10].vPos1 = <<-488.410, 3967.250, 98.0730>>
	Race.tSpeedVolumes[10].vPos2 = <<-537.137, 3956.036, 76.3871>>
	Race.tSpeedVolumes[10].fWidth = 9.0
	Race.tSpeedVolumes[10].fSpeed = -1.0
	
	Race.tSpeedVolumes[11].vPos1 = <<-307.550, 3988.647, 53.3230>>
	Race.tSpeedVolumes[11].vPos2 = <<-255.138, 3926.889, 31.5805>>
	Race.tSpeedVolumes[11].fWidth = 9.0
	Race.tSpeedVolumes[11].fSpeed = -1.0

	ORR_Gate_Setup(Race.sGate[0], <<-1866.1970,4416.5698,47.6783>>, 243.4072, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[1], <<-1719.3030,4323.6265,64.5414>>, 221.2501, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[2], <<-1649.4390,4210.0781,82.7008>>, 247.6203, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[3], <<-1559.9771,4206.7441,75.5405>>, 283.5007, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[4], <<-1468.9580,4225.5361,52.2794>>, 259.5412, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[5], <<-1386.4840,4165.8062,51.7286>>, 222.3696, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[6], <<-1345.5850,4126.9629,61.8040>>, 307.3036, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[7], <<-1321.0000,4186.4370,62.0636>>, 339.1380, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[8], <<-1276.7581,4278.8252,65.3138>>, 293.3126, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[9], <<-1185.7729,4291.9038,78.4686>>, 257.1151, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[10], <<-1072.9340,4272.6211,100.6904>>, 248.1561, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[11], <<-1038.1479,4229.4219,115.7768>>, 185.7003, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[12], <<-1032.5160,4165.0562,118.9887>>, 211.2260, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[13], <<-938.7848,4148.5850,140.8003>>, 223.9547, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[14], <<-891.4792,4096.8848,161.8249>>, 251.5976, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[15], <<-823.6310,4051.6890,162.4706>>, 265.8944, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[16], <<-753.7779,4038.2029,147.1398>>, 239.7252, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[17], <<-654.0638,4013.6890,126.4406>>, 255.6974, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[18], <<-591.6994,3972.3940,113.3035>>, 271.8129, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[19], <<-515.1089,3960.3130,87.3917>>, 286.0931, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[20], <<-425.4944,3943.4951,65.9412>>, 282.5892, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[21], <<-379.7583,3981.8640,52.2577>>, 319.5195, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[22], <<-330.6125,4012.5090,45.0532>>, 233.3578, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[23], <<-252.5999, 3920.9768, 39.3600>>, 222.7951, 15.0000, ORR_CHKPT_OFFROAD_FINISH)
	//ORR_Gate_Setup(Race.sGate[23], <<-246.9585,3911.1531,38.1027>>, 222.7951, 15.0000, ORR_CHKPT_OFFROAD_FINISH)

	Race.iRacerCnt = 6

	ORR_Racer_Setup_Misc(Race.Racer[0])
	ORR_Racer_Setup_Name(Race.Racer[0], "Player")
	ORR_Racer_Setup_Entities(Race.Racer[0], PLAYER_PED_ID(), ORR_Master.PlayerVeh)
	ORR_Racer_Setup_Start(Race.Racer[0], <<-1938.34, 4443.20, 37.06>>, 249.56)
	ORR_Racer_Setup_Types(Race.Racer[0], PEDTYPE_PLAYER1, PLAYER_ONE, SANCHEZ)

	ORR_Racer_Setup_Misc(Race.Racer[1])
	ORR_Racer_Setup_Name(Race.Racer[1], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[1], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[1], <<-1932.5958, 4442.1899, 37.3184>>, 261.2715)
	ORR_Racer_Setup_Types(Race.Racer[1], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, SANCHEZ)

	ORR_Racer_Setup_Misc(Race.Racer[2])
	ORR_Racer_Setup_Name(Race.Racer[2], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[2], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[2], <<-1933.2573, 4440.7388, 37.2692>>, 256.9669)
	ORR_Racer_Setup_Types(Race.Racer[2], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BLAZER)

	ORR_Racer_Setup_Misc(Race.Racer[3])
	ORR_Racer_Setup_Name(Race.Racer[3], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[3], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[3], <<-1935.01, 4442.57, 37.48>>, 258.09)
	ORR_Racer_Setup_Types(Race.Racer[3], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, SANCHEZ)

	ORR_Racer_Setup_Misc(Race.Racer[4])
	ORR_Racer_Setup_Name(Race.Racer[4], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[4], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[4], <<-1935.50, 4440.32, 37.48>>, 268.03)
	ORR_Racer_Setup_Types(Race.Racer[4], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, SANCHEZ)

	ORR_Racer_Setup_Misc(Race.Racer[5])
	ORR_Racer_Setup_Name(Race.Racer[5], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[5], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[5], <<-1939.63, 4440.46, 37.25>>, 253.32)
	ORR_Racer_Setup_Types(Race.Racer[5], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BLAZER)

ENDPROC

// ****************************************************************************
// Ridge Run

PROC ORR_Setup_Offroad_race_08(ORR_RACE_STRUCT& Race)
	
	ORR_SAFELY_SET_SCENARIO_GROUP_ENABLED("RidgeRun")
	
	ORR_Race_Init(Race)
	
	Race.vDriveTo[0] = <<-516.1545, 1999.3740, 204.7715>>
	Race.vDriveTo[1] = <<-521.4240, 2014.8163, 203.0018>>
	Race.vDriveTo[2] = <<-518.0005, 2017.2990, 202.7044>>
	
	Race.fStartTime[0] = 2.3
	Race.fStartTime[1] = 0.5
	
	Race.fIntroSpeed[0] = 5.0
	Race.fIntroSpeed[1] = 5.0
	
	Race.fRecordingStartTime = 1500.0
	
	Race.iGateSkipDist = 1
	Race.iGateLagTolerance = 3
	Race.iGateCnt = 16
	Race.vFinalSpawnPos = <<-1104.2290, 2638.1877, 23.7090>> //<<-1105.9921, 2633.9695, 23.3552>>
	Race.fFinalSpawnHeading = 110.3232//216.2397
	Race.vPlayerCarPos = <<-521.4766, 1995.4907, 204.7503>>
	Race.fPlayerCarHeading = 343.8705
	Race.vFinalPlayerVehPos = <<-1129.4838, 2614.2366, 18.2058>>
	Race.fFinalPlayerVehHeading = 115.8882
	Race.fStartBoostMin = 3.0
	Race.fStartBoostMax = 8.0
	ORR_Race_Gate_Init_All(Race)
	
	Race.vFinishCamPos = <<-1079.7181, 2619.5818, 31.9209>>
	Race.vFinishCamRot = <<4.6559, -0.0000, -46.3333>>
	Race.fFOV = 42.2756
	Race.vLBDCamPos = <<-1080.9214, 2618.4380, 51.9764>>
	Race.vLBDCamRot = <<75.9941, -0.0119, -46.2779>>
	Race.vFinishLBDCamPos = <<-1103.8585, 2638.7444, 42.4107>>
	Race.vFinishLBDCamRot = <<50.5641, 0.0013, 116.3548>>
	Race.vFinishLBDCam2Pos = <<-1101.3816, 2639.8845, 25.4833>>
	Race.vFinishLBDCam2Rot = <<-6.2272, 0.0009, 116.8833>>
	Race.vFinishLBDCam2VehPos = <<-1098.3188, 2638.1653, 26.8100>>
	Race.vFinishLBDCam2VehRot = <<-17.6339, 0.0009, 98.6135>>
	
	//Speed restriction volumes
	Race.iSpeedVolumeCount = 7
	
	Race.tSpeedVolumes[0].vPos1 = <<-597.733, 2068.197, 157.8279>>
	Race.tSpeedVolumes[0].vPos2 = <<-621.634, 2055.637, 146.9955>>
	Race.tSpeedVolumes[0].fWidth = 9.0
	Race.tSpeedVolumes[0].fSpeed = 0.8
	
	Race.tSpeedVolumes[1].vPos1 = <<-567.453, 2147.054, 148.2583>>
	Race.tSpeedVolumes[1].vPos2 = <<-560.084, 2121.080, 134.4063>>
	Race.tSpeedVolumes[1].fWidth = 9.0
	Race.tSpeedVolumes[1].fSpeed = 0.8
	
	Race.tSpeedVolumes[2].vPos1 = <<-734.955, 2294.898, 86.3563>>
	Race.tSpeedVolumes[2].vPos2 = <<-689.331, 2221.998, 72.3165>>
	Race.tSpeedVolumes[2].fWidth = 9.0
	Race.tSpeedVolumes[2].fSpeed = 0.8
	
	Race.tSpeedVolumes[3].vPos1 = <<-723.968, 2348.136, 76.6954>>
	Race.tSpeedVolumes[3].vPos2 = <<-743.881, 2329.903, 62.8225>>
	Race.tSpeedVolumes[3].fWidth = 9.0
	Race.tSpeedVolumes[3].fSpeed = 0.8
	
	Race.tSpeedVolumes[4].vPos1 = <<-711.953, 2462.458, 70.5703>>
	Race.tSpeedVolumes[4].vPos2 = <<-715.006, 2381.515, 54.2646>>
	Race.tSpeedVolumes[4].fWidth = 18.0
	Race.tSpeedVolumes[4].fSpeed = 0.8
	
	Race.tSpeedVolumes[5].vPos1 = <<-814.710, 2659.980, 63.2850>>
	Race.tSpeedVolumes[5].vPos2 = <<-852.704, 2635.868, 46.9925>>
	Race.tSpeedVolumes[5].fWidth = 9.0
	Race.tSpeedVolumes[5].fSpeed = 0.8
	
	Race.tSpeedVolumes[6].vPos1 = <<-997.000, 2692.443, 46.3381>>
	Race.tSpeedVolumes[6].vPos2 = <<-1029.413, 2661.229, 30.1374>>
	Race.tSpeedVolumes[6].fWidth = 9.0
	Race.tSpeedVolumes[6].fSpeed = 0.8

//	ORR_Gate_Setup(Race.sGate[0], <<-517.8138,2010.0862,207.5426>>, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[0], <<-573.6789,2037.1670,187.8733>>, 127.7385, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[1], <<-595.3907,1963.8900,171.9366>>, 133.1126, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[2], <<-633.6208,2025.6998,158.2910>>, 7.3997, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[3], <<-572.8118,2078.2720,149.4540>>, 294.3077, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[4], <<-570.9627,2162.0359,134.7248>>, 44.7838, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[5], <<-598.9607,2125.7041, 127.2645>>, 158.9312, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[6], <<-676.9645,2168.9529,104.7812>>, 26.8194, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[7], <<-720.4925,2272.2900,75.6379>>, 35.1564, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[8], <<-707.4363,2461.5571,61.3191>>, 345.3840, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[9], <<-691.5254,2530.9070,54.5241>>, 47.9461, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[10], <<-729.2248,2646.3401,57.4402>>, 12.3391, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[11], <<-785.4009,2668.7661,52.7145>>, 105.7909, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[12], <<-863.5698,2623.9851,56.1686>>, 138.3881, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[13], <<-915.1938,2593.0200,55.3226>>, 32.3294, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[14], <<-944.2743,2699.0779,37.1145>>, 66.8516, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[15], <<-1040.9342, 2649.8801, 35.7990>>, 134.2195, 15.0000, ORR_CHKPT_OFFROAD_FINISH) //, ORR_RACE_STUNT_GATE_NORMAL)
	//ORR_Gate_Setup(Race.sGate[15], <<-1059.1107,2633.1731,32.4490>>, 134.2195, 15.0000, ORR_CHKPT_OFFROAD_FINISH) //, ORR_RACE_STUNT_GATE_NORMAL)

	Race.iRacerCnt = 5

	ORR_Racer_Setup_Misc(Race.Racer[0])
	ORR_Racer_Setup_Name(Race.Racer[0], "Player")
	ORR_Racer_Setup_Entities(Race.Racer[0], PLAYER_PED_ID(), ORR_Master.PlayerVeh)
	ORR_Racer_Setup_Start(Race.Racer[0], <<-516.1545, 1999.3740, 204.7715>>, 18.7907)
	ORR_Racer_Setup_Types(Race.Racer[0], PEDTYPE_PLAYER1, PLAYER_ONE, MESA)

	ORR_Racer_Setup_Misc(Race.Racer[1])
	ORR_Racer_Setup_Name(Race.Racer[1], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[1], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[1], <<-520.2454, 2010.9674, 203.5850>>, 16.9522)
	ORR_Racer_Setup_Types(Race.Racer[1], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, dubsta2 )//BFINJECTION)

	ORR_Racer_Setup_Misc(Race.Racer[2])
	ORR_Racer_Setup_Name(Race.Racer[2], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[2], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[2], <<-516.7449, 2013.2365, 203.7095>>, 18.1295)
	ORR_Racer_Setup_Types(Race.Racer[2], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, MESA)

	ORR_Racer_Setup_Misc(Race.Racer[3])
	ORR_Racer_Setup_Name(Race.Racer[3], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[3], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[3], <<-518.3414, 2005.4489, 204.1878>>, 20.7231)
	ORR_Racer_Setup_Types(Race.Racer[3], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, PATRIOT )//BFINJECTION)

	ORR_Racer_Setup_Misc(Race.Racer[4])
	ORR_Racer_Setup_Name(Race.Racer[4], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[4], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[4], <<-514.8247, 2008.5737, 204.3629>>, 20.2278)
	ORR_Racer_Setup_Types(Race.Racer[4], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BJXL)

ENDPROC

// ****************************************************************************
// Valley Trail

PROC ORR_Setup_Offroad_race_09(ORR_RACE_STRUCT& Race)
	
	ORR_SAFELY_SET_SCENARIO_GROUP_ENABLED("ValleyTrail")
	
	ORR_Race_Init(Race)
	
	Race.vDriveTo[0] = <<0,0,0>>
	Race.vDriveTo[1] = <<-234.0323, 4226.0068, 43.7916>>
	Race.vDriveTo[2] = <<-234.0242, 4228.9849, 43.8350>>
	
	Race.fStartTime[0] = 0.8
	Race.fStartTime[1] = 0.0
	
	Race.fIntroSpeed[0] = 1.8
	Race.fIntroSpeed[1] = 1.8

	Race.fRecordingStartTime = 1300.0
	
	Race.iGateCnt = 22
	Race.iGateSkipDist = 1
	Race.iGateLagTolerance = 4
	Race.vFinalSpawnPos = <<-1921.7406, 4462.2300, 33.4587>>//<<-1921.5918, 4462.1934, 33.4728>>
	Race.fFinalSpawnHeading = 10.1185//103.1174
	Race.vPlayerCarPos = <<-212.5433, 4226.6582, 43.9920>>
	Race.fPlayerCarHeading = 71.6442
	Race.vFinalPlayerVehPos =  <<-1927.5424, 4480.6597, 29.8550>>
	Race.fFinalPlayerVehHeading = 129.8399
	Race.fStartBoostMin = 6.0
	Race.fStartBoostMax = 18.0
	
	Race.vFinishCamPos = <<-1939.8308, 4468.4092, 33.3119>> 
	Race.vFinishCamRot = <<-3.5928, 0.0935, -69.9291>>
	Race.fFOV = 40.5327
	Race.vLBDCamPos = <<-1937.8685, 4469.0874, 65.6972>>
	Race.vLBDCamRot = <<71.4933, 0.0589, -69.9981>>
	Race.vFinishLBDCamPos = <<-1922.0421, 4464.4902, 69.2131>>
	Race.vFinishLBDCamRot = <<41.2757, -0.0019, 16.1333>>
	Race.vFinishLBDCam2Pos = <<-1920.5764, 4459.1260, 35.2370>> 
	Race.vFinishLBDCam2Rot = <<-6.1614, -0.0002, 16.6887>> 
	
	//Speed restriction volumes
	Race.iSpeedVolumeCount = 3
	
	Race.tSpeedVolumes[0].vPos1 = <<-733.334, 4399.325, 65.4842>>
	Race.tSpeedVolumes[0].vPos2 = <<-552.611, 4355.336, 11.7453>>
	Race.tSpeedVolumes[0].fWidth = 27.0
	Race.tSpeedVolumes[0].fSpeed = 0.7
	
	Race.tSpeedVolumes[1].vPos1 = <<-905.852, 4375.275, 24.3108>>
	Race.tSpeedVolumes[1].vPos2 = <<-951.357, 4354.556, 10.5136>>
	Race.tSpeedVolumes[1].fWidth = 9.0
	Race.tSpeedVolumes[1].fSpeed = -1.0
	
	Race.tSpeedVolumes[1].vPos1 = <<-1426.591, 4302.318, 14.3013>>
	Race.tSpeedVolumes[1].vPos2 = <<-1476.591, 4302.318, 0.3053>>
	Race.tSpeedVolumes[1].fWidth = 14.0
	Race.tSpeedVolumes[1].fSpeed = -1.0
	
	ORR_Race_Gate_Init_All(Race)

	ORR_Gate_Setup(Race.sGate[0], <<-269.5131,4227.9741,43.1428>>, 97.9608, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[1], <<-330.7353,4242.2539,42.3877>>, 38.1068, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[2], <<-417.2221,4290.2358,56.6259>>, 51.9004, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[3], <<-506.6945,4359.4482,66.3928>>, 90.5887, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[4], <<-566.9642,4357.0962,58.1420>>, 70.7537, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[5], <<-733.3773,4412.3481,20.3150>>, 79.5961, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[6], <<-825.9259,4411.5889,19.3628>>, 97.3463, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[7], <<-898.7642,4377.7212,16.3963>>, 112.7456, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[8], <<-974.6707,4349.1821,11.7338>>, 96.1166, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[9], <<-1107.4510,4379.5420,11.8522>>, 86.8279, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[10], <<-1222.2150,4364.5879,7.0459>>, 100.2219, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[11], <<-1303.5140,4340.4722,5.7083>>, 115.8464, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[12], <<-1365.6801,4298.6089,1.4209>>, 92.9491, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[13], <<-1453.2111,4302.7642,1.4281>>, 87.7785, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[14], <<-1509.8950,4308.5171,4.6830>>, 74.4776, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[15], <<-1593.9070,4349.8140,1.8808>>, 44.8026, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[16], <<-1657.4840,4445.5562,1.6662>>, 61.0429, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[17], <<-1756.2560,4463.1221,4.7861>>, 60.7632, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[18], <<-1813.3616, 4479.8638, 16.5623>>, 75.1575, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[19], <<-1846.5181, 4500.3193, 21.1740>>, 87.4783, 1.5, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[20], <<-1879.9010,4482.6572,25.0771>>, 103.1315, 2.0, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[21], <<-1925.7230,4468.9521,31.4250>>, 153.8084, 15.0000, ORR_CHKPT_OFFROAD_FINISH)

	Race.iRacerCnt = 6

	ORR_Racer_Setup_Misc(Race.Racer[0])
	ORR_Racer_Setup_Name(Race.Racer[0], "Player")
	ORR_Racer_Setup_Entities(Race.Racer[0], PLAYER_PED_ID(), ORR_Master.PlayerVeh)
	ORR_Racer_Setup_Start(Race.Racer[0], <<-225.98, 4224.74, 44.36>>, 44.36)
	ORR_Racer_Setup_Types(Race.Racer[0], PEDTYPE_PLAYER1, PLAYER_ONE, SANCHEZ)

	ORR_Racer_Setup_Misc(Race.Racer[1])
	ORR_Racer_Setup_Name(Race.Racer[1], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[1], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[1], <<-231.8486, 4225.4614, 43.8031>>, 76.66)
	ORR_Racer_Setup_Types(Race.Racer[1], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BLAZER)

	ORR_Racer_Setup_Misc(Race.Racer[2])
	ORR_Racer_Setup_Name(Race.Racer[2], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[2], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[2], <<-231.1335, 4228.4702, 43.8715>>, 80.88)
	ORR_Racer_Setup_Types(Race.Racer[2], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, SANCHEZ)

	ORR_Racer_Setup_Misc(Race.Racer[3])
	ORR_Racer_Setup_Name(Race.Racer[3], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[3], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[3], <<-228.85, 4227.71, 44.57>>, 83.38)
	ORR_Racer_Setup_Types(Race.Racer[3], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BLAZER)

	ORR_Racer_Setup_Misc(Race.Racer[4])
	ORR_Racer_Setup_Name(Race.Racer[4], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[4], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[4], <<-229.58, 4225.21, 44.34>>, 75.78)
	ORR_Racer_Setup_Types(Race.Racer[4], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BLAZER)

	ORR_Racer_Setup_Misc(Race.Racer[5])
	ORR_Racer_Setup_Name(Race.Racer[5], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[5], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[5], <<-225.41, 4227.53, 44.45>>, 82.70)
	ORR_Racer_Setup_Types(Race.Racer[5], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, SANCHEZ)

ENDPROC

// ****************************************************************************
// Lakeside Splash

PROC ORR_Setup_Offroad_race_10(ORR_RACE_STRUCT& Race)

	ORR_SAFELY_SET_SCENARIO_GROUP_ENABLED("LakesideSplash")
	
	ORR_Race_Init(Race)
	
	Race.vDriveTo[0] = <<0,0,0>>
	Race.vDriveTo[1] =  <<1609.4054, 3843.6296, 33.3039>>
	Race.vDriveTo[2] = <<1610.47, 3842.26, 33.61>>
	
	Race.fStartTime[0] = 0.6
	Race.fStartTime[1] = 0.0
	
	Race.fIntroSpeed[0] = 2.1
	Race.fIntroSpeed[1] = 2.1
	
	Race.fRecordingStartTime = 3000.0

	Race.iGateCnt = 23
	Race.iGateSkipDist = 1
	Race.iGateLagTolerance = 3
	Race.vFinalSpawnPos = <<1771.8617, 4580.5806, 36.6618>>//<<1749.4401, 4553.7568, 37.4711>>
	Race.fFinalSpawnHeading = 245.6290//325.6999
	Race.vPlayerCarPos = <<1604.7012, 3815.5481, 33.8019>>
	Race.fPlayerCarHeading = 307.8146
	Race.vFinalPlayerVehPos = <<1770.2928, 4563.5322, 36.8169>>
	Race.fFinalPlayerVehHeading = 260.4078
	Race.fStartBoostMin = 6.0
	Race.fStartBoostMax = 18.0
	ORR_Race_Gate_Init_All(Race)
	
	Race.vFinishCamPos = <<1774.4712, 4582.7402, 38.2051>>
	Race.vFinishCamRot = <<-0.6042, 0.0069, -136.8127>>
	Race.fFOV = 45.0174
	Race.vLBDCamPos = <<1774.6030, 4582.5962, 56.7003>>
	Race.vLBDCamRot = <<55.0201, 0.0069, -136.8127>>
	Race.vFinishLBDCamPos = <<1773.0336, 4579.8394, 67.4021>>
	Race.vFinishLBDCamRot = <<48.0699, 0.0015, -108.3325>>
	Race.vFinishLBDCam2Pos = <<1768.6444, 4581.3774, 38.4301>>
	Race.vFinishLBDCam2Rot = <<-6.1888, 0.0001, -107.8030>>  
	
	//Speed restriction volumes
	Race.iSpeedVolumeCount = 10
	
	Race.tSpeedVolumes[0].vPos1 = <<1889.741, 3981.563, 38.8384>>
	Race.tSpeedVolumes[0].vPos2 = <<1808.741, 3981.563, 27.8598>>
	Race.tSpeedVolumes[0].fWidth = 9.0
	Race.tSpeedVolumes[0].fSpeed = -1.0
	
	Race.tSpeedVolumes[1].vPos1 = <<1925.476, 3976.481, 38.0079>>
	Race.tSpeedVolumes[1].vPos2 = <<1966.193, 3957.321, 27.4639>>
	Race.tSpeedVolumes[1].fWidth = 9.0
	Race.tSpeedVolumes[1].fSpeed = -1.0
	
	Race.tSpeedVolumes[2].vPos1 = <<2169.178, 3888.335, 37.9252>>
	Race.tSpeedVolumes[2].vPos2 = <<2214.175, 3887.770, 27.4639>>
	Race.tSpeedVolumes[2].fWidth = 9.0
	Race.tSpeedVolumes[2].fSpeed = -1.0
	
	Race.tSpeedVolumes[3].vPos1 = <<2320.415, 3976.647, 39.8941>>
	Race.tSpeedVolumes[3].vPos2 = <<2251.986, 3910.750, 28.9624>>
	Race.tSpeedVolumes[3].fWidth = 9.0
	Race.tSpeedVolumes[3].fSpeed = -1.0
	
	Race.tSpeedVolumes[4].vPos1 = <<2371.606, 4042.119, 37.1450>>
	Race.tSpeedVolumes[4].vPos2 = <<2343.242, 4000.942, 26.5060>>
	Race.tSpeedVolumes[4].fWidth = 9.0
	Race.tSpeedVolumes[4].fSpeed = -1.0
	
	Race.tSpeedVolumes[5].vPos1 = <<2388.561, 4183.628, 37.1450>>
	Race.tSpeedVolumes[5].vPos2 = <<2378.303, 4075.112, 26.5060>>
	Race.tSpeedVolumes[5].fWidth = 14.0
	Race.tSpeedVolumes[5].fSpeed = -1.0
	
	Race.tSpeedVolumes[6].vPos1 = <<2455.368, 4445.856, 46.1903>>
	Race.tSpeedVolumes[6].vPos2 = <<2436.290, 4338.539, 32.4383>>
	Race.tSpeedVolumes[6].fWidth = 14.0
	Race.tSpeedVolumes[6].fSpeed = -1.0
	
	Race.tSpeedVolumes[7].vPos1 = <<2294.331, 4666.395, 41.8361>>
	Race.tSpeedVolumes[7].vPos2 = <<2348.202, 4662.665, 28.3757>>
	Race.tSpeedVolumes[7].fWidth = 9.0
	Race.tSpeedVolumes[7].fSpeed = -1.0
	
	Race.tSpeedVolumes[8].vPos1 = <<2110.394, 4634.220, 41.0296>>
	Race.tSpeedVolumes[8].vPos2 = <<2146.925, 4652.833, 27.2709>>
	Race.tSpeedVolumes[8].fWidth = 9.0
	Race.tSpeedVolumes[8].fSpeed = -1.0
	
	Race.tSpeedVolumes[9].vPos1 = <<1749.124, 4556.489, 44.5424>>
	Race.tSpeedVolumes[9].vPos2 = <<1805.002, 4575.425, 30.4020>>
	Race.tSpeedVolumes[9].fWidth = 9.0
	Race.tSpeedVolumes[9].fSpeed = -1.0
	

	ORR_Gate_Setup(Race.sGate[0], <<1672.0090,3892.2351,33.2404>>, 310.0604, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[1], <<1723.9088, 3939.3005,32.7933>>, 300.7313, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)	
	ORR_Gate_Setup(Race.sGate[2], <<1888.5605,3980.4661,30.9868>>, 259.3970, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[3], <<1959.5233,3958.4260,31.5639>>, 233.6847, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[4], <<2017.4070,3916.7480,32.9635>>, 236.8508, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[5], <<2085.5007,3878.5559,30.7581>>, 272.6228, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[6], <<2256.2759, 3883.4570, 32.4238>>, 336.0722, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)//<<2197.0984,3886.9307,30.8068>>, 273.8577, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[7], <<2286.8035,3939.4680,32.7243>>, 308.4637, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[8], <<2366.8096,4022.5811,32.2681>>, 334.4590, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[9], <<2383.1489,4147.7695,32.8510>>, 331.0306, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[10], <<2424.8459,4228.9629,34.1509>>, 354.7785, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[11], <<2427.8420,4294.5938,35.2968>>, 348.9683, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[12], <<2443.3379,4377.2886,34.1383>>, 351.4983, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[13], <<2458.3020,4494.7002,34.3521>>, 7.2237, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[14], <<2443.6953,4599.0947,35.9352>>, 42.8614, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[15], <<2331.2395,4664.8984,34.4170>>, 86.8863, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[16], <<2241.1680,4668.5430,31.8399>>, 93.9850, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[17], <<2118.4641,4635.7021,31.6723>>, 125.8619, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[18], <<2031.1661,4570.6006,32.6676>>, 99.1116, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[19], <<1948.6539,4556.2642,33.2397>>, 68.1335, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[20], <<1908.0488,4575.8633,36.2975>>, 79.7179, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[21], <<1863.6133,4581.5044,35.2547>>, 98.7328, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[22], <<1791.3474, 4571.7173, 36.0361>>, 104.3401, 15.0000, ORR_CHKPT_OFFROAD_FINISH)
	//ORR_Gate_Setup(Race.sGate[22], <<1768.0754, 4561.2197, 36.6945>>, 104.3401, 15.0000, ORR_CHKPT_OFFROAD_FINISH)
	//ORR_Gate_Setup(Race.sGate[22], <<1752.5917,4556.5151,37.7766>>, 104.3401, 15.0000, ORR_CHKPT_OFFROAD_FINISH)

	Race.iRacerCnt = 5

	ORR_Racer_Setup_Misc(Race.Racer[0])
	ORR_Racer_Setup_Name(Race.Racer[0], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[0], PLAYER_PED_ID(), ORR_Master.PlayerVeh)
	ORR_Racer_Setup_Start(Race.Racer[0], <<1602.54, 3837.21, 33.72>>, 308.94)
	ORR_Racer_Setup_Types(Race.Racer[0], PEDTYPE_PLAYER1, PLAYER_ONE, SANCHEZ)

	ORR_Racer_Setup_Misc(Race.Racer[1])
	ORR_Racer_Setup_Name(Race.Racer[1], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[1], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[1], <<1607.0559, 3841.7207, 33.3075>>, 307.24)
	ORR_Racer_Setup_Types(Race.Racer[1], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, SANCHEZ)

	ORR_Racer_Setup_Misc(Race.Racer[2])
	ORR_Racer_Setup_Name(Race.Racer[2], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[2], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[2], <<1608.2616, 3840.2798, 33.0403>>, 311.44)
	ORR_Racer_Setup_Types(Race.Racer[2], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BLAZER)

	ORR_Racer_Setup_Misc(Race.Racer[3])
	ORR_Racer_Setup_Name(Race.Racer[3], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[3], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[3], <<1606.20, 3838.41, 33.62>>, 307.44)
	ORR_Racer_Setup_Types(Race.Racer[3], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BLAZER)

	ORR_Racer_Setup_Misc(Race.Racer[4])
	ORR_Racer_Setup_Name(Race.Racer[4], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[4], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[4], <<1604.49, 3840.42, 34.18>>, 308.19)
	ORR_Racer_Setup_Types(Race.Racer[4], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, SANCHEZ)

ENDPROC

// ****************************************************************************
// Eco Friendly

PROC ORR_Setup_Offroad_race_11(ORR_RACE_STRUCT& Race)

	ORR_SAFELY_SET_SCENARIO_GROUP_ENABLED("EcoFriendly")
		
	ORR_Race_Init(Race)
	
	Race.vDriveTo[0] = <<0,0,0>>
	Race.vDriveTo[1] =  <<2050.8811, 2127.6221, 91.4509>>//<<2050.8079, 2129.8689, 91.7045>>
	Race.vDriveTo[2] = <<2049.5093, 2123.8713, 91.4899>>//<<2048.2297, 2124.5498, 91.6742>>
	
	Race.fStartTime[0] = 1.2
	Race.fStartTime[1] = 0.0
	
	Race.fIntroSpeed[0] = 2.0
	Race.fIntroSpeed[1] = 2.0
	
	Race.fRecordingStartTime = 1900.0

	Race.iGateCnt = 32
	Race.iGateSkipDist = 1
	Race.iGateLagTolerance = 2
	Race.vFinalSpawnPos = <<2504.6455, 1998.7272, 19.1012>>
	Race.fFinalSpawnHeading = 206.8969
	Race.vPlayerCarPos = <<2048.6326, 2151.6023, 93.9434>>
	Race.fPlayerCarHeading = 179.1810
	Race.vFinalPlayerVehPos = <<2524.6726, 1987.0459, 18.8377>>
	Race.fFinalPlayerVehHeading = 297.3222
	Race.fStartBoostMin = 6.0
	Race.fStartBoostMax = 18.0
	ORR_Race_Gate_Init_All(Race)
	
	Race.vFinishCamPos = <<2499.1904, 2001.7711, 20.7579>>
	Race.vFinishCamRot = <<7.6037, 0.4053, 22.4732>>
	Race.fFOV = 45.5283
	Race.vLBDCamPos = <<2502.3394, 1995.1281, 72.8120>>
	Race.vLBDCamRot = <<78.4310, 0.4019, 22.4845>>
	Race.vFinishLBDCamPos = <<2505.4368, 1996.8837, 61.8389>>  
	Race.vFinishLBDCamRot = <<64.8241, -0.0019, -147.0777>> 
	Race.vFinishLBDCam2Pos = <<2502.6340, 2001.3615, 20.8696>>
	Race.vFinishLBDCam2Rot = <<-6.2077, 0.0009, -146.5390>>  
	Race.vFinishLBDCam2VehPos = <<2502.5132, 2006.2295, 21.5446>>
	Race.vFinishLBDCam2VehRot = <<-6.8036, 0.0009, -164.4838>>
	
	//Speed restriction volumes
	Race.iSpeedVolumeCount = 10
	
	Race.tSpeedVolumes[0].vPos1 = <<2167.324, 1977.030, 112.7385>>
	Race.tSpeedVolumes[0].vPos2 = <<2205.897, 1865.513, 89.9993>>
	Race.tSpeedVolumes[0].fWidth = 14.0
	Race.tSpeedVolumes[0].fSpeed = 0.65
	
	Race.tSpeedVolumes[1].vPos1 = <<2181.741, 1720.485, 106.2526>>
	Race.tSpeedVolumes[1].vPos2 = <<2126.277, 1667.074, 85.8048>>
	Race.tSpeedVolumes[1].fWidth = 18.0
	Race.tSpeedVolumes[1].fSpeed = 0.65
	
	Race.tSpeedVolumes[2].vPos1 = <<2166.598, 1804.395, 115.9017>>
	Race.tSpeedVolumes[2].vPos2 = <<2072.238, 1713.526, 89.7735>>
	Race.tSpeedVolumes[2].fWidth = 41.0
	Race.tSpeedVolumes[2].fSpeed = 0.65
	
	Race.tSpeedVolumes[3].vPos1 = <<2290.406, 1936.941, 136.6993>>
	Race.tSpeedVolumes[3].vPos2 = <<2258.399, 1876.945, 111.6761>>
	Race.tSpeedVolumes[3].fWidth = 23.0
	Race.tSpeedVolumes[3].fSpeed = 0.7
	
	Race.tSpeedVolumes[4].vPos1 = <<2260.006, 2041.987, 143.5354>>
	Race.tSpeedVolumes[4].vPos2 = <<2291.258, 1981.594, 119.6620>>
	Race.tSpeedVolumes[4].fWidth = 18.0
	Race.tSpeedVolumes[4].fSpeed = 0.7
	
	Race.tSpeedVolumes[5].vPos1 = <<2162.319, 2284.559, 124.8190>>
	Race.tSpeedVolumes[5].vPos2 = <<2173.939, 2176.180, 97.8743>>
	Race.tSpeedVolumes[5].fWidth = 18.0
	Race.tSpeedVolumes[5].fSpeed = 0.7
	
	Race.tSpeedVolumes[6].vPos1 = <<2077.626, 2441.118, 98.3924>>
	Race.tSpeedVolumes[6].vPos2 = <<2014.791, 2376.684, 73.0260>>
	Race.tSpeedVolumes[6].fWidth = 27.0
	Race.tSpeedVolumes[6].fSpeed = 0.7
	
	Race.tSpeedVolumes[7].vPos1 = <<2225.084, 2333.921, 94.4721>>
	Race.tSpeedVolumes[7].vPos2 = <<2245.739, 2208.612, 67.6869>>
	Race.tSpeedVolumes[7].fWidth = 27.0
	Race.tSpeedVolumes[7].fSpeed = 0.7
	
	Race.tSpeedVolumes[8].vPos1 = <<2318.396, 2328.963, 86.1274>>
	Race.tSpeedVolumes[8].vPos2 = <<2304.876, 2239.984, 61.5653>>
	Race.tSpeedVolumes[8].fWidth = 27.0
	Race.tSpeedVolumes[8].fSpeed = 0.7
	
	Race.tSpeedVolumes[9].vPos1 = <<2415.408, 2331.722, 68.2280>>
	Race.tSpeedVolumes[9].vPos2 = <<2482.094, 2138.929, 24.9998>>
	Race.tSpeedVolumes[9].fWidth = 27.0
	Race.tSpeedVolumes[9].fSpeed = 0.7

//	ORR_Gate_Setup(Race.sGate[0], <<2067.0339,2115.2261,99.9940>>, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[0], <<2141.9109,2052.6196,90.6277>>, 166.5133, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[1], <<2165.8240,1975.1660,93.4036>>, 203.6161, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[2], <<2206.0659,1867.3949,102.8335>>, 188.2386, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[3], <<2215.1479, 1764.0555, 96.0258>>, 153.5908, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[4], <<2158.2671,1701.6190,93.9455>>, 132.5378, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[5], <<2082.1289,1653.0500,95.5486>>, 46.0799, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[6], <<2069.6396,1704.7670,101.9224>>, 328.1978, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[7], <<2173.3879,1791.8561,106.1635>>, 315.5670, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[8], <<2262.2771,1883.9249,117.2448>>, 337.8492, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[9], <<2298.1001,1973.3621,130.1531>>, 31.9428, 35.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[10], <<2240.9819,2055.9170,128.8369>>, 69.9408, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[11], <<2187.0640,2115.3660,124.4313>>, 69.7971, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[12], <<2175.1350,2178.3589,115.5291>>, 333.4648, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[13], <<2155.7061, 2279.1260, 104.2192>>, 9.3879, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[14], <<2155.4709,2360.1211,107.0664>>, 342.6922, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[15], <<2125.4021,2410.4700,99.9028>>, 85.3731, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[16], <<2025.5320,2342.7380,92.4240>>, 115.5183, 1.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[17], <<2011.3170,2364.2620,87.7059>>, 304.8824, 0.3000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[18], <<2066.3379,2431.3894,84.2313>>, 308.0472, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[19], <<2120.6392,2450.4250,87.4022>>, 258.9786, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[20], <<2199.0439,2440.7649,86.1534>>, 215.4235, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[21], <<2231.5090,2356.2820,75.5773>>, 180.3866, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[22], <<2247.3750,2238.6169,79.7798>>, 202.7603, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[23], <<2280.4199,2173.1499,76.9772>>, 313.6382, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[24], <<2298.1238,2223.8635,76.3006>>, 352.7675, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[25], <<2331.9880,2336.1484,71.2906>>, 357.6190, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[26], <<2302.1941,2429.1050,65.3744>>, 326.6184, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[27], <<2358.0449,2420.0249,61.1470>>, 236.7312, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[28], <<2411.4988,2360.2466,59.7345>>, 191.8318, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[29], <<2445.2034,2244.7778,46.9808>>, 192.0339, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[30], <<2479.6311,2116.3940,32.8883>>, 184.7639, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT) //, ORR_RACE_STUNT_GATE_NORMAL)
	ORR_Gate_Setup(Race.sGate[31], <<2483.2065, 2026.7452, 23.1603>>, 198.5985, 15.0000, ORR_CHKPT_OFFROAD_FINISH) //, ORR_RACE_STUNT_GATE_NORMAL)
	//ORR_Gate_Setup(Race.sGate[31], <<2490.0156,2008.2854,20.2589>>, 198.5985, 15.0000, ORR_CHKPT_OFFROAD_FINISH) //, ORR_RACE_STUNT_GATE_NORMAL)

	Race.iRacerCnt = 6

	ORR_Racer_Setup_Misc(Race.Racer[0])
	ORR_Racer_Setup_Name(Race.Racer[0], "Player")
	ORR_Racer_Setup_Entities(Race.Racer[0], PLAYER_PED_ID(), ORR_Master.PlayerVeh)
	ORR_Racer_Setup_Start(Race.Racer[0], <<2037.60, 2136.15, 93.15>>, 238.07)
	ORR_Racer_Setup_Types(Race.Racer[0], PEDTYPE_PLAYER1, PLAYER_ONE, MESA)

	ORR_Racer_Setup_Misc(Race.Racer[1])
	ORR_Racer_Setup_Name(Race.Racer[1], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[1], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[1], <<2046.7721, 2130.4421, 91.9486>>, 233.1493)
	ORR_Racer_Setup_Types(Race.Racer[1], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BJXL)

	ORR_Racer_Setup_Misc(Race.Racer[2])
	ORR_Racer_Setup_Name(Race.Racer[2], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[2], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[2], <<2045.3445, 2126.3523, 91.9358>>, 236.3633)
	ORR_Racer_Setup_Types(Race.Racer[2], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, MESA)

	ORR_Racer_Setup_Misc(Race.Racer[3])
	ORR_Racer_Setup_Name(Race.Racer[3], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[3], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[3], <<2034.8207, 2138.8853, 92.6919>>, 237.2035)//<<2034.08, 2133.17, 93.50>>, 233.51)
	ORR_Racer_Setup_Types(Race.Racer[3], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BJXL)

	ORR_Racer_Setup_Misc(Race.Racer[4])
	ORR_Racer_Setup_Name(Race.Racer[4], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[4], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[4], <<2041.2371, 2134.6980, 92.4401>>, 232.9754)
	ORR_Racer_Setup_Types(Race.Racer[4], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BFINJECTION)

	ORR_Racer_Setup_Misc(Race.Racer[5])
	ORR_Racer_Setup_Name(Race.Racer[5], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[5], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[5], <<2039.4126, 2130.2275, 92.5788>>, 241.4449)
	ORR_Racer_Setup_Types(Race.Racer[5], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, SADLER)
	
//	VECTOR vBound = <<3,3,3>>
//	ADD_SCENARIO_BLOCKING_AREA(<<2488.9875, 2024.8365, 22.5652>> + vBound, <<2488.9875, 2024.8365, 22.5652>> - vBound)
//	ADD_SCENARIO_BLOCKING_AREA(<<2483.3384, 2044.4012, 26.8997>> + vBound, <<2483.3384, 2044.4012, 26.8997>> - vBound)
//	ADD_SCENARIO_BLOCKING_AREA(<<2481.6165, 2072.5833, 29.6611>> + vBound, <<2481.6165, 2072.5833, 29.6611>> - vBound)
//	ADD_SCENARIO_BLOCKING_AREA(<<2485.3765, 2105.0381, 31.9575>> + vBound, <<2485.3765, 2105.0381, 31.9575>> - vBound)
//	ADD_SCENARIO_BLOCKING_AREA(<<2490.0151, 2127.2642, 33.7661>> + vBound, <<2490.0151, 2127.2642, 33.7661>> - vBound)
//	ADD_SCENARIO_BLOCKING_AREA(<<2488.2205, 2138.4624, 36.2874>> + vBound, <<2488.2205, 2138.4624, 36.2874>> - vBound)
//	ADD_SCENARIO_BLOCKING_AREA(<<2484.3359, 2150.9033, 40.1570>> + vBound, <<2484.3359, 2150.9033, 40.1570>> - vBound)
//	ADD_SCENARIO_BLOCKING_AREA(<<2504.1477, 2002.3096, 19.1761>> + vBound, <<2504.1477, 2002.3096, 19.1761>> - vBound)

ENDPROC

// ****************************************************************************
// Mineward Spiral

PROC ORR_Setup_Offroad_race_12(ORR_RACE_STRUCT& Race)

	ORR_SAFELY_SET_SCENARIO_GROUP_ENABLED("MinewardSpiral")

	ORR_Race_Init(Race)
	
	Race.vDriveTo[0] = <<0,0,0>>
	Race.vDriveTo[1] = <<2992.58, 2786.28, 43.21>>
	Race.vDriveTo[2] =  <<2995.76, 2787.72, 43.26>>
	
	Race.fStartTime[0] = 1.0
	Race.fStartTime[1] = 0.0
	
	Race.fIntroSpeed[0] = 2.3
	Race.fIntroSpeed[1] = 2.3
	
	Race.fRecordingStartTime = 1600.0

	Race.iGateCnt = 32
	Race.iGateSkipDist = 1
	Race.iGateLagTolerance = 2
	Race.vFinalSpawnPos = <<2980.0066, 2887.9619, 58.5826>>//<<2979.8347, 2887.8220, 58.5673>>
	Race.fFinalSpawnHeading = 124.0450//327.7659
	Race.vPlayerCarPos = <<2998.2952, 2764.2883, 41.8084>>
	Race.fPlayerCarHeading = 330.3521
	Race.vFinalPlayerVehPos = <<2993.1956, 2896.8521, 58.8192>>
	Race.fFinalPlayerVehHeading = 328.6752
	Race.fStartBoostMin = 6.0
	Race.fStartBoostMax = 18.0
	ORR_Race_Gate_Init_All(Race)
	
	Race.vFinishCamPos = <<2986.7898, 2883.7031, 63.6161>>
	Race.vFinishCamRot = <<-5.6336, -0.0026, 102.4159>>
	Race.fFOV = 35.2836
	Race.vLBDCamPos = <<2985.1694, 2883.3459, 80.4381>>
	Race.vLBDCamRot = <<24.6443, -0.0026, 102.4159>>
	Race.vFinishLBDCamPos = <<2976.6326, 2885.5654, 104.9947>>
	Race.vFinishLBDCamRot = <<42.4712, -0.0019, 130.0713>>
	Race.vFinishLBDCam2Pos = <<2982.3706, 2890.2847, 60.3559>>
	Race.vFinishLBDCam2Rot = <<-6.1457, 0.0053, 130.5831>> 
	
	Race.fIntroCam2Time = 3.5
	
	//Speed restriction volumes
	Race.iSpeedVolumeCount = 9
	
	Race.tSpeedVolumes[0].vPos1 = <<2941.318, 2840.509, 57.7108>>
	Race.tSpeedVolumes[0].vPos2 = <<2918.521, 2826.042, 43.7679>>
	Race.tSpeedVolumes[0].fWidth = 5.0
	Race.tSpeedVolumes[0].fSpeed = 0.5
	
	//reuse
	Race.tSpeedVolumes[1].vPos1 = <<2904.722, 2773.682, 58.7576>>
	Race.tSpeedVolumes[1].vPos2 = <<2930.713, 2736.947, 48.0526>>
	Race.tSpeedVolumes[1].fWidth = 9.0
	Race.tSpeedVolumes[1].fSpeed = -1.0
	
	Race.tSpeedVolumes[2].vPos1 = <<2965.486, 2871.721, 61.1835>>
	Race.tSpeedVolumes[2].vPos2 = <<3009.263, 2797.698, 50.2022>>
	Race.tSpeedVolumes[2].fWidth = 9.0
	Race.tSpeedVolumes[2].fSpeed = -1.0
	
	Race.tSpeedVolumes[3].vPos1 = <<2824.695, 2884.227, 55.1744>>
	Race.tSpeedVolumes[3].vPos2 = <<2857.785, 2820.281, 44.5227>>
	Race.tSpeedVolumes[3].fWidth = 9.0
	Race.tSpeedVolumes[3].fSpeed = -1.0
	
	Race.tSpeedVolumes[4].vPos1 = <<2977.486, 2939.126, 82.2107>>
	Race.tSpeedVolumes[4].vPos2 = <<2961.479, 2917.383, 71.0420>>
	Race.tSpeedVolumes[4].fWidth = 9.0
	Race.tSpeedVolumes[4].fSpeed = -1.0
	
	Race.tSpeedVolumes[5].vPos1 = <<3039.690, 2953.354, 74.9672>>
	Race.tSpeedVolumes[5].vPos2 = <<3035.455, 2908.553, 64.3637>>
	Race.tSpeedVolumes[5].fWidth = 9.0
	Race.tSpeedVolumes[5].fSpeed = -1.0
	
	Race.tSpeedVolumes[6].vPos1 = <<3015.062, 2855.131, 77.5502>>
	Race.tSpeedVolumes[6].vPos2 = <<3032.751, 2818.144, 67.7152>>
	Race.tSpeedVolumes[6].fWidth = 9.0
	Race.tSpeedVolumes[6].fSpeed = -1.0
	
	Race.tSpeedVolumes[7].vPos1 = <<2904.722, 2773.682, 58.7576>>
	Race.tSpeedVolumes[7].vPos2 = <<2930.713, 2736.947, 48.0526>>
	Race.tSpeedVolumes[7].fWidth = 9.0
	Race.tSpeedVolumes[7].fSpeed = -1.0
	
	Race.tSpeedVolumes[8].vPos1 = <<2946.503, 2867.024, 61.4466>>
	Race.tSpeedVolumes[8].vPos2 = <<2912.593, 2843.979, 50.5097>>
	Race.tSpeedVolumes[8].fWidth = 9.0
	Race.tSpeedVolumes[8].fSpeed = -1.0

	ORR_Gate_Setup(Race.sGate[0], <<2979.9241,2810.3779,43.1119>>, 32.8606, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[1], <<2948.2971,2840.8083,46.0740>>, 97.0967, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[2], <<2907.5552, 2819.5347, 52.9237>>, 143.0102, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[3], <<2896.7947, 2785.0933, 53.6336>>, 185.8202, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[4], <<2923.7598, 2744.9326, 52.6429>>, 213.7990, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[5], <<2978.9668, 2728.0071, 53.3941>>, 318.1995, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[6], <<3017.3799, 2777.6450, 52.6012>>, 20.8136, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[7], <<2972.6665, 2855.1680, 55.8068>>, 42.1725, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[8], <<2929.9902, 2878.2051, 59.7260>>, 131.7729, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[9], <<2899.2019, 2852.9939, 63.2669>>, 91.4971, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[10], <<2851.1094, 2865.7749, 55.7528>>, 40.0000, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[11], <<2817.6074, 2901.4016, 44.9261>>, 178.2960, 0.5, ORR_CHKPT_OFFROAD_DEFAULT)//<<2818.3235, 2909.0647, 44.5046>>, 178.2960, 0.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[12], <<2833.6816, 2871.7195, 47.2565>>, 202.8879, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[13], <<2851.9697, 2827.0210, 51.5760>>, 209.3698, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[14], <<2877.8904, 2780.0374, 57.3181>>, 208.8235, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[15], <<2913.5400,2730.8879,61.8753>>, 205.3804, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[16], <<2956.1931,2682.4861,63.3001>>, 262.9329, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[17], <<3010.5291,2715.8569,63.2056>>, 287.6325, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[18], <<3047.1565,2767.0901,66.9343>>, 8.3449, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[19], <<3021.0540,2807.9880,65.2655>>, 34.6069, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[20], <<2991.9707,2884.7349,60.2772>>, 353.7100, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[21], <<3011.7900,2945.6609,65.9224>>, 107.1971, 2.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[22], <<2955.9341,2911.6580,70.9026>>, 338.0471, 2.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[23], <<2984.3101,2945.6201,78.5753>>, 312.7021, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[24], <<3034.9080,2967.0039,70.6033>>, 192.7614, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[25], <<3014.1589,2879.1411,71.7657>>, 179.7664, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[26], <<3039.5911,2812.7000,70.0086>>, 205.8111, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[27], <<3023.2141,2733.8689,60.6356>>, 109.1779, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[28], <<2944.2222,2713.8491,53.4180>>, 40.0634, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[29], <<2896.5930,2789.7310,53.8335>>, 350.9532, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[30], <<2919.0530,2848.1189,54.7335>>, 304.1605, 15.0000, ORR_CHKPT_OFFROAD_DEFAULT)
	ORR_Gate_Setup(Race.sGate[31], <<2963.2161,2876.1331,57.6815>>, 303.8434, 15.0000, ORR_CHKPT_OFFROAD_FINISH)

	Race.iRacerCnt = 6

	ORR_Racer_Setup_Misc(Race.Racer[0])
	ORR_Racer_Setup_Name(Race.Racer[0], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[0], PLAYER_PED_ID(), ORR_Master.PlayerVeh)
	ORR_Racer_Setup_Start(Race.Racer[0], <<2996.96, 2773.84, 42.41>>, 20.89)
	ORR_Racer_Setup_Types(Race.Racer[0], PEDTYPE_PLAYER1, PLAYER_ONE, SANCHEZ)

	ORR_Racer_Setup_Misc(Race.Racer[1])
	ORR_Racer_Setup_Name(Race.Racer[1], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[1], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[1], <<2992.58, 2786.28, 43.21>>, 26.81)
	ORR_Racer_Setup_Types(Race.Racer[1], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, SANCHEZ)

	ORR_Racer_Setup_Misc(Race.Racer[2])
	ORR_Racer_Setup_Name(Race.Racer[2], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[2], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[2], <<2998.0430, 2784.2209, 42.5605>>, 33.42)
	ORR_Racer_Setup_Types(Race.Racer[2], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BLAZER)

	ORR_Racer_Setup_Misc(Race.Racer[3])
	ORR_Racer_Setup_Name(Race.Racer[3], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[3], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[3], <<2994.91, 2779.79, 42.73>>, 11.43)
	ORR_Racer_Setup_Types(Race.Racer[3], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, SANCHEZ)

	ORR_Racer_Setup_Misc(Race.Racer[4])
	ORR_Racer_Setup_Name(Race.Racer[4], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[4], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[4], <<2999.44, 2781.11, 43.11>>, 23.89)
	ORR_Racer_Setup_Types(Race.Racer[4], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, BLAZER)

	ORR_Racer_Setup_Misc(Race.Racer[5])
	ORR_Racer_Setup_Name(Race.Racer[5], "Racer")
	ORR_Racer_Setup_Entities(Race.Racer[5], NULL, NULL)
	ORR_Racer_Setup_Start(Race.Racer[5], <<3000.06, 2775.26, 42.47>>, 17.16)
	ORR_Racer_Setup_Types(Race.Racer[5], PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01, SANCHEZ)

ENDPROC

// ****************************************************************************
// General load/setup functions for Off Road Races.
// ****************************************************************************

PROC ORR_LOAD_COURSE_DATA(ORR_RACE_STRUCT& Race)
	
	UNUSED_PARAMETER(Race)
	// Setup Master structure for Off-Road Races.
	ORR_Master.eRaceType		= ORR_RACE_TYPE_OFFROAD
	ORR_Master.PlayerVeh 		= NULL
	ORR_Master.eDefChkPntType 	= ORR_CHKPT_OFFROAD_DEFAULT
	ORR_Master.fDefRcrHead		= 97.0 
	ORR_Master.eDefDrvType 		= PEDTYPE_CIVMALE
	ORR_Master.eDefDrvModel 	= A_M_Y_MOTOX_01
	ORR_Master.eDefVehModel 	= BJXL
	
	// String labels for course titles and race completion goal times
	ORR_Master.szRaceName[0] =  "CANYONCLIFFS"
	ORR_Master.fRaceTime[0] =  165.0	
	ORR_Master.szRaceName[1] =  "RIDGERUN"
	ORR_Master.fRaceTime[1] =  80.0
	ORR_Master.szRaceName[2] = "MINEWARD"
	ORR_Master.fRaceTime[2] =  165.0
	ORR_Master.szRaceName[3] =  "VALLEYTRAIL"
	ORR_Master.fRaceTime[3] =  165.0
	ORR_Master.szRaceName[4] =  "LAKESIDE"
	ORR_Master.fRaceTime[4] =  165.0
	ORR_Master.szRaceName[5] =   "ECOFRIENDLY"
	ORR_Master.fRaceTime[5] =  165.0
	
ENDPROC

