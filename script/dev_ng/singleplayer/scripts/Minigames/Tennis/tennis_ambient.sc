CONST_INT	IS_TENNIS_MULTIPLAYER	0


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "globals.sch"
USING "tennis_core_lib.sch"

SCRIPT (TennisLaunchData launchData)
	CPRINTLN(DEBUG_TENNIS, "**************************************************   Ambient Tennis First Post  **************************************************")
	IF ((IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME_FRIENDS) AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("tennis")) = 0)	OR (g_bTriggerSceneActive)) AND NOT launchData.bFreemodeIntro
		CPRINTLN(DEBUG_TENNIS, PICK_STRING(g_bTriggerSceneActive, "g_bTriggerSceneActive is TRUE on startup. Quitting.", "Terminating Ambient Tennis, mission is running and it's not Tennis.sc"))
		TERMINATE_THIS_THREAD()
	ENDIF
	
//	launchData.bFreemodeIntro = TRUE
//	launchData.fMaxDistForIntro = 100
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP) 
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		TERMINATE_THIS_THREAD()
	ENDIF

	TENNIS_GAME_PROPERTIES thisProps
	TENNIS_GAME_STATUS thisStatus
	FLOAT fAmbientMaxDist = PICK_FLOAT(launchData.bFreemodeIntro, launchData.fMaxDistForIntro, AMBIENT_SHUTDOWN_DISTANCE)
	INT dummy=0
	TENNIS_PLAYER_ID eSelfID = (TENNIS_PLAYER_AWAY)
	TENNIS_PLAYER_ID eOtherID = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eSelfID))
	
	INITIALIZE_AMBIENT_TENNIS(thisProps, thisStatus, TA_AI_VS_AI, eSelfID, eOtherID, launchData)
	
	#IF IS_DEBUG_BUILD
		LOAD_TENNIS_AMBIENT_WIDGETS(thisProps, thisStatus, eSelfID, eOtherID)
	#ENDIF
	FLOAT fDummy
	WHILE(TRUE)
		CDEBUG3LN(DEBUG_TENNIS, "GET_FRAME_COUNT(", GET_FRAME_COUNT(), ")")
		UPDATE_TENNIS_TIMER_A(thisProps)
		IF g_bTriggerSceneActive
			CPRINTLN(DEBUG_TENNIS, "g_bTriggerSceneActive is TRUE. Quitting.")
			TENNIS_AMBIENT_CLEANUP(thisProps)
		ENDIF
		//SET_WIND(0)	//Commenting out for bug 266056
		
		IF GET_TENNIS_GAME_STATE(thisStatus) > TGSE_INIT AND HANDLE_TENNIS_FAIL_SATES(thisProps, eSelfID, dummy)
			CPRINTLN(DEBUG_TENNIS, "Ped has been injured, ending the game!")
			TENNIS_AMBIENT_CLEANUP(thisProps)
		ENDIF
		
		IF NETWORK_IS_IN_SESSION() AND NOT launchData.bFreemodeIntro
			CPRINTLN(DEBUG_TENNIS, "Ambient Tennis is active in a network game, shutting Ambient Tennis down!")
			TENNIS_AMBIENT_CLEANUP(thisProps)
		ENDIF
		
//		#IF IS_DEBUG_BUILD
//			PRINTLN("GET_CURRENT_STACK_SIZE(", GET_CURRENT_STACK_SIZE(), ")")
//		#ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), thisProps.tennisCourt.vLauncherPoint) < (AMBIENT_GAMEPLAY_DISTANCE * AMBIENT_GAMEPLAY_DISTANCE) OR launchData.bFreemodeIntro
				PROCESS_GAME_STATE(thisProps, thisStatus, dummy, eSelfID, eOtherID, fDummy, launchData.bFreemodeIntro)
			ENDIF
		ENDIF
		
		INCREASE_TENNIS_FRAME_COUNTER(thisProps.iFrameCounter)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), thisProps.tennisCourt.vLauncherPoint) > (fAmbientMaxDist*fAmbientMaxDist)
				CPRINTLN(DEBUG_TENNIS, "Max Distance reached")
				TENNIS_AMBIENT_CLEANUP(thisProps)
			ENDIF
		ENDIF
		
		IF (IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME_FRIENDS) AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("tennis")) = 0) AND NOT launchData.bFreemodeIntro
			CPRINTLN(DEBUG_TENNIS, "Terminating Ambient Tennis, mission is running and it's not Tennis.sc")
			TENNIS_AMBIENT_CLEANUP(thisProps)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_ELEMENTS_AMBIENT(thisProps, eSelfID, eOtherID)
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				CPRINTLN(DEBUG_TENNIS, "Fail key pressed")
				TENNIS_AMBIENT_CLEANUP(thisProps)
			ENDIF
		#ENDIF
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT

