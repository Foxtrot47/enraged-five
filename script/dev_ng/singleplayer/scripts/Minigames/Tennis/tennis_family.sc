CONST_INT	IS_TENNIS_MULTIPLAYER	0


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "globals.sch"
USING "tennis_core_lib.sch"
USING "family_private.sch"

PROC HANDLE_TENNIS_AMANDA_YELLING_AT_PLAYER(FLOAT &fTimer, TENNIS_COURT &thisCourt, TENNIS_PLAYER &playerAmanda)
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	STRING sDismiss = PICK_STRING(ePlayer = CHAR_MICHAEL, "DISMISS_MICHAEL", PICK_STRING(ePlayer = CHAR_TREVOR, "GENERIC_CURSE_HIGH", PICK_STRING(ePlayer = CHAR_FRANKLIN, "GENERIC_INSULT_MALE", "GENERIC_INSULT_MED")))
	FLOAT fWidth = GET_TENNIS_COURT_WIDTH(thisCourt)
	fTimer += GET_FRAME_TIME()
	IF fTimer > (FUCK_OFF_TIMER_THRESHOLD + GET_RANDOM_FLOAT_IN_RANGE(0,FUCK_OFF_TIMER_THRESHOLD)) 
	AND GET_TENNIS_PLAYER_SWING_STATE(playerAmanda) = TSS_NO_SWING
	AND NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(playerAmanda))
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_POINT_INSIDE_TENNIS_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), thisCourt.vCourtCorners[0] + (fWidth * 0.5 * thisCourt.vRightNorm), thisCourt.vCourtCorners[3] + (fWidth * 0.5 * thisCourt.vRightNorm), (fWidth * 1.5))
		CPRINTLN(DEBUG_TENNIS, "HANDLE_TENNIS_AMANDA_YELLING_AT_PLAYER :: Triggering ", sDismiss)
		TEXT_LABEL_31 texVoice = "AMANDA_NORMAL"
		PLAY_TENNIS_AMBIENT_SPEECH(playerAmanda, sDismiss, texVoice)
		fTimer = 0
	ENDIF
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_POLY_ANGLED_AREA(thisCourt.vCourtCorners[0] + (fWidth * 0.5 * thisCourt.vRightNorm), thisCourt.vCourtCorners[3] + (fWidth * 0.5 * thisCourt.vRightNorm), (fWidth * 1.5))
	#ENDIF
ENDPROC

SCRIPT
	CPRINTLN(DEBUG_TENNIS, "******************************** Family Tennis First Post ********************************")
	PRINTLN("Family Tennis, GET_ALLOCATED_STACK_SIZE() is ", GET_ALLOCATED_STACK_SIZE())
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)

	TENNIS_GAME_PROPERTIES thisProps
	TENNIS_GAME_STATUS thisStatus
	INT dummy=0
	FLOAT fFuckOffTimer = 0
	
	// Requests
	MODEL_NAMES model1 = GET_NPC_PED_MODEL(CHAR_TENNIS_COACH)
	MODEL_NAMES model2 = GET_NPC_PED_MODEL(CHAR_AMANDA)
	REQUEST_FAMILY_ASSETS(thisProps, model1, model2)
	TENNIS_PLAYER_ID eSelfID = (TENNIS_PLAYER_AWAY)
	TENNIS_PLAYER_ID eOtherID = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eSelfID))
	
	INITIALIZE_FAMILY_PED_TENNIS(thisProps, thisStatus, TA_AI_VS_AI, eSelfID, eOtherID)
	FLOAT fDummy
	WHILE(TRUE)
		UPDATE_TENNIS_TIMER_A(thisProps)
		
		HANDLE_TENNIS_AMANDA_YELLING_AT_PLAYER(fFuckOffTimer, thisProps.tennisCourt, thisProps.tennisPlayers[eSelfID])

		IF g_bTriggerSceneActive
			CPRINTLN(DEBUG_TENNIS, "g_bTriggerSceneActive is TRUE. Quitting")
			TENNIS_AMBIENT_CLEANUP(thisProps)
		ENDIF
				
		IF GET_TENNIS_GAME_STATE(thisStatus) > TGSE_INIT AND HANDLE_TENNIS_FAIL_SATES(thisProps, eSelfID, dummy)
			//("Ped has been injured, ending the game!")
			PRIVATE_Set_Current_Family_Member_Event(FM_MICHAEL_WIFE, FAMILY_MEMBER_BUSY)
			TENNIS_AMBIENT_CLEANUP(thisProps)
		ENDIF
		
		PROCESS_GAME_STATE(thisProps, thisStatus, dummy, eSelfID, eOtherID, fDummy)
		
		INCREASE_TENNIS_FRAME_COUNTER(thisProps.iFrameCounter)

		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("tennis")) > 0
			CPRINTLN(DEBUG_TENNIS, "Terminating Family Tennis, Tennis.sc is trying to run")
			PRIVATE_Set_Current_Family_Member_Event(FM_MICHAEL_WIFE, FAMILY_MEMBER_BUSY)
			TENNIS_AMBIENT_CLEANUP(thisProps)
		ENDIF

		IF NOT (Get_Current_Event_For_FamilyMember(FM_MICHAEL_WIFE) = FE_M_WIFE_playing_tennis)
			CPRINTLN(DEBUG_TENNIS, "Terminating Family Tennis, Wife is no longer playing")
			TENNIS_AMBIENT_CLEANUP(thisProps)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				TENNIS_AMBIENT_CLEANUP(thisProps)
			ENDIF
		#ENDIF
		
		WAIT(0)
	ENDWHILE

	
ENDSCRIPT
