USING "minigames_helpers.sch"
USING "minigame_UIInputs.sch"
USING "tennis_court.sch"
USING "tennis_debug.sch"
USING "tennis_helpers_lib.sch"
USING "tennis_ball_lib.sch"

FUNC STRING GET_STRING_FROM_TENNIS_ALOC(enumActivityLocation eLoc)
	RETURN PICK_STRING(eLoc = ALOC_tennis_beachCourt, "ALOC_tennis_beachCourt", 
		PICK_STRING(eLoc = ALOC_tennis_michaelHouse, "ALOC_tennis_michaelHouse", 
		PICK_STRING(eLoc = ALOC_tennis_vinewoodhotel1, "ALOC_tennis_vinewoodhotel1", 
		PICK_STRING(eLoc = ALOC_tennis_richmanHotel1, "ALOC_tennis_richmanHotel1", 
		PICK_STRING(eLoc = ALOC_tennis_LSUCourt1, "ALOC_tennis_LSUCourt1", 
		PICK_STRING(eLoc = ALOC_tennis_vespucciHotel, "ALOC_tennis_vespucciHotel", 
		PICK_STRING(eLoc = ALOC_tennis_weazelCourt1, "ALOC_tennis_weazelCourt1", 
		PICK_STRING(eLoc = ALOC_tennis_chumashHotel, "ALOC_tennis_chumashHotel", 
		"Unknown enumActivityLocation"))))))))
ENDFUNC

/// PURPOSE:
///    returns the nearest tennis court
/// PARAMS:
///    thisLocation - 
/// RETURNS:
///    returns the nearest tennis court
FUNC enumActivityLocation GET_CLOSEST_COURT(enumActivityLocation &thisLocation)
	FLOAT fShortestDist = 100000000
	FLOAT fTestDist
	VECTOR vPlayerPos
	BOOL bIsTargetDead = IS_ENTITY_DEAD( PLAYER_PED_ID() )
	
	IF IS_PLAYER_SCTV( PLAYER_ID () )
		bIsTargetDead = IS_ENTITY_DEAD( GET_PLAYER_PED( NETWORK_GET_PLAYER_INDEX( NETWORK_GET_HOST_OF_THIS_SCRIPT() ) ) )
	ENDIF
	
	IF NOT bIsTargetDead
		IF IS_PLAYER_SCTV( PLAYER_ID () )
			vPlayerPos = GET_ENTITY_COORDS( GET_PLAYER_PED( NETWORK_GET_PLAYER_INDEX( NETWORK_GET_HOST_OF_THIS_SCRIPT() ) ) )
		ELSE
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
			
		// Michael's House
		fTestDist = VDIST2(vPlayerPos, << -769.058, 165.294, 66.474 >>)
		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_michaelHouse distance^2 is: ", fTestDist)
		IF fTestDist < fShortestDist
			fShortestDist = fTestDist
			thisLocation = ALOC_tennis_michaelHouse
			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_michaelHouse")
		ENDIF
		// Vespucci Court 1
		fTestDist = VDIST2(vPlayerPos, <<-1171.28, -1599.59, 3.34>>)
		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_beachCourt distance^2 is: ", fTestDist)
		IF fTestDist < fShortestDist
			fShortestDist = fTestDist
			thisLocation = ALOC_tennis_beachCourt
			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_beachCourt")
		ENDIF
		// Vinewood Hotel
		fTestDist = VDIST2(vPlayerPos, <<487.5186, -217.7697, 52.7864>>)
		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_vinewoodhotel1 distance^2 is: ", fTestDist)
		IF fTestDist < fShortestDist
			fShortestDist = fTestDist
			thisLocation = ALOC_tennis_vinewoodhotel1
			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_vinewoodhotel1")
		ENDIF
		#IF NOT IS_TENNIS_MULTIPLAYER
		// Damn Overlook
		fTestDist = VDIST2(vPlayerPos, <<-49.9120, 942.5634, 231.1741>>)
		CPRINTLN(DEBUG_TENNIS, "NO_ACTIVITY_LOCATION distance^2 is: ", fTestDist)
		IF fTestDist < fShortestDist
			fShortestDist = fTestDist
			thisLocation = NO_ACTIVITY_LOCATION
			CPRINTLN(DEBUG_TENNIS, "thisLocation set to NO_ACTIVITY_LOCATION")
		ENDIF
		#ENDIF
		// Arch Mansion
//		fTestDist = VDIST2(vPlayerPos, <<-1513.0581, -68.1384, 53.6964>>)
//		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_archMansion distance^2 is: ", fTestDist)
//		IF fTestDist < fShortestDist
//			fShortestDist = fTestDist
//			thisLocation = ALOC_tennis_archMansion
//			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_archMansion")
//		ENDIF
		// Richman Hotel 1
		fTestDist = VDIST2(vPlayerPos, <<-1225.4536, 344.8268, 78.9859>>)
		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_richmanHotel1 distance^2 is: ", fTestDist)
		IF fTestDist < fShortestDist
			fShortestDist = fTestDist
			thisLocation = ALOC_tennis_richmanHotel1
			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_richmanHotel1")
		ENDIF
		// LSU Court 1
		fTestDist = VDIST2(vPlayerPos, <<-1623.4541, 257.1566, 58.5552>>)
		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_LSUCourt1 distance^2 is: ", fTestDist)
		IF fTestDist < fShortestDist
			fShortestDist = fTestDist
			thisLocation = ALOC_tennis_LSUCourt1
			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_LSUCourt1")
		ENDIF
		// Crescent Drive
//		fTestDist = VDIST2(vPlayerPos, <<-1938.7014, 479.8535, 101.6915>>)
//		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_crescentDrive distance^2 is: ", fTestDist)
//		IF fTestDist < fShortestDist
//			fShortestDist = fTestDist
//			thisLocation = ALOC_tennis_crescentDrive
//			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_crescentDrive")
//		ENDIF
		// Vespucci Hotel
		fTestDist = VDIST2(vPlayerPos, <<-939.6168, -1255.7323, 6.9773>>)
		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_vespucciHotel distance^2 is: ", fTestDist)
		IF fTestDist < fShortestDist
			fShortestDist = fTestDist
			thisLocation = ALOC_tennis_vespucciHotel
			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_vespucciHotel")
		ENDIF
		// Spire House
//		fTestDist = VDIST2(vPlayerPos, <<-974.1231, 218.4792, 65.7561>>)
//		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_spireHouse distance^2 is: ", fTestDist)
//		IF fTestDist < fShortestDist
//			fShortestDist = fTestDist
//			thisLocation = ALOC_tennis_spireHouse
//			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_spireHouse")
//		ENDIF
		// Pagoda Fireplace
//		fTestDist = VDIST2(vPlayerPos, <<-1011.9330, 181.4188, 60.6592>>)
//		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_pagodaFireplace distance^2 is: ", fTestDist)
//		IF fTestDist < fShortestDist
//			fShortestDist = fTestDist
//			thisLocation = ALOC_tennis_pagodaFireplace
//			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_pagodaFireplace")
//		ENDIF
		// Church Adjacent
//		fTestDist = VDIST2(vPlayerPos, <<-1619.8748, -348.7769, 48.2146>>)
//		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_churchAdjacent distance^2 is: ", fTestDist)
//		IF fTestDist < fShortestDist
//			fShortestDist = fTestDist
//			thisLocation = ALOC_tennis_churchAdjacent
//			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_churchAdjacent")
//		ENDIF
		// Weazel Court 2
//		fTestDist = VDIST2(vPlayerPos, <<-1345.4982, -105.2020, 49.7046>>)
//		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_weazelCourt2 distance^2 is: ", fTestDist)
//		IF fTestDist < fShortestDist
//			fShortestDist = fTestDist
//			thisLocation = ALOC_tennis_weazelCourt2
//			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_weazelCourt2")
//		ENDIF
		// Weazel Court 1
		fTestDist = VDIST2(vPlayerPos, <<-1371.2748, -107.9437, 49.7046>>)
		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_weazelCourt1 distance^2 is: ", fTestDist)
		IF fTestDist < fShortestDist
			fShortestDist = fTestDist
			thisLocation = ALOC_tennis_weazelCourt1
			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_weazelCourt1")
		ENDIF
//		// Weazel Court 3
//		fTestDist = VDIST2(vPlayerPos, <<-1309.1124, -105.5487, 46.8879>>)
//		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_weazelCourt3 distance^2 is: ", fTestDist)
//		IF fTestDist < fShortestDist
//			fShortestDist = fTestDist
//			thisLocation = ALOC_tennis_weazelCourt3
//			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_weazelCourt3")
//		ENDIF
//		// Weazel Court 4
//		fTestDist = VDIST2(vPlayerPos, <<-1269.3676, -108.6186, 44.7609>>)
//		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_weazelCourt4 distance^2 is: ", fTestDist)
//		IF fTestDist < fShortestDist
//			fShortestDist = fTestDist
//			thisLocation = ALOC_tennis_weazelCourt4
//			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_weazelCourt4")
//		ENDIF
//		// Weazel Court 5
//		fTestDist = VDIST2(vPlayerPos, <<-1298.5475, -145.4046, 44.7609>>)
//		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_weazelCourt5 distance^2 is: ", fTestDist)
//		IF fTestDist < fShortestDist
//			fShortestDist = fTestDist
//			thisLocation = ALOC_tennis_weazelCourt5
//			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_weazelCourt5")
//		ENDIF
		// Chumash Hotel
		fTestDist = VDIST2(vPlayerPos, <<-2869.9915, 9.2297, 10.6083>>)
		CPRINTLN(DEBUG_TENNIS, "ALOC_tennis_chumashHotel distance^2 is: ", fTestDist)
		IF fTestDist < fShortestDist
			fShortestDist = fTestDist
			thisLocation = ALOC_tennis_chumashHotel
			CPRINTLN(DEBUG_TENNIS, "thisLocation set to ALOC_tennis_chumashHotel")
		ENDIF
		
	ENDIF
	
	CPRINTLN(DEBUG_TENNIS, "GET_CLOSEST_COURT Returning ", GET_STRING_FROM_TENNIS_ALOC(thisLocation))
	
	RETURN thisLocation
ENDFUNC

PROC SET_TENNIS_COURT_VARS_OFF_CORNERS(TENNIS_COURT &thisCourt, TENNIS_GAME_PROPERTIES &thisProps, FLOAT fFrameScalar, FLOAT fCourtServiceRatio, BOOL bUseVeniceNet=FALSE)
//	CPRINTLN(DEBUG_TENNIS, "SET_TENNIS_COURT_VARS_OFF_CORNERS called")
	thisProps.vForward = (thisCourt.vCourtCorners[3]-thisCourt.vCourtCorners[0]) * fFrameScalar
	thisProps.vRight = thisCourt.vCourtCorners[1]-thisCourt.vCourtCorners[0]
	thisProps.vUp = CROSS_PRODUCT(thisProps.vRight,thisProps.vForward)
	
	thisCourt.vForwardNorm = thisProps.vForward/VMAG(thisProps.vForward)
	thisCourt.vRightNorm = thisProps.vRight/VMAG(thisProps.vRight)
		
	thisCourt.vServiceCorners[0] = thisCourt.vCourtCorners[0]+thisProps.vForward*fCourtServiceRatio
	thisCourt.vServiceCorners[1] = thisCourt.vCourtCorners[1]+thisProps.vForward*fCourtServiceRatio
	thisCourt.vServiceCorners[2] = thisCourt.vCourtCorners[2]-thisProps.vForward*fCourtServiceRatio
	thisCourt.vServiceCorners[3] = thisCourt.vCourtCorners[3]-thisProps.vForward*fCourtServiceRatio
		
	thisCourt.vCenterCourt = (thisCourt.vCourtCorners[0]+thisCourt.vCourtCorners[2])*0.5
	
	thisCourt.vNetSideline[0] = thisCourt.vCourtCorners[0] + thisProps.vForward
	thisCourt.vNetSideline[1] = thisCourt.vCourtCorners[1] + thisProps.vForward
	
	SET_TENNIS_COURT_WIDTH(thisCourt, VMAG(thisProps.vRight))
	SET_TENNIS_COURT_SERVICE_RATIO(thisCourt, fCourtServiceRatio)
	
	FLOAT fNetLength1, fNetLength2, fNetEndsHeight, fNetMiddleHeight
	IF bUseVeniceNet
		fNetLength1			= NET_LENGTH_1_VENICE
		fNetLength2			= NET_LENGTH_2_VENICE
		fNetEndsHeight		= NET_ENDS_HEIGHT_VENICE
		fNetMiddleHeight	= NET_MIDDLE_HEIGHT_VENICE
	ELSE
		fNetLength1			= NET_LENGTH_1_MICHAEL
		fNetLength2			= NET_LENGTH_2_MICHAEL
		fNetEndsHeight		= NET_ENDS_HEIGHT_MICHAEL
		fNetMiddleHeight	= NET_MIDDLE_HEIGHT_MICHAEL
	ENDIF
	thisCourt.vNetTop[0] = thisCourt.vCenterCourt + (thisProps.vRight * fNetLength1)
	thisCourt.vNetTop[0].z += fNetEndsHeight
	thisCourt.vNetTop[1] = thisCourt.vCenterCourt
	thisCourt.vNetTop[1].z += fNetMiddleHeight
	thisCourt.vNetTop[2] = thisCourt.vCenterCourt + (thisProps.vRight * fNetLength2)
	thisCourt.vNetTop[2].z += fNetEndsHeight
	
	CPRINTLN(DEBUG_TENNIS, "VMAG(thisProps.vForward)=", VMAG(thisProps.vForward))
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisCourt - the court to be initialized
///    thisProps - the game struct we're using
///    iAmbientCourt - the enum of the court we want for an ambient game, TC_VENICE_1 or TC_MICHAELS is the default for player games.
PROC INITIALIZE_TENNIS_COURT(TENNIS_COURT &thisCourt, TENNIS_GAME_PROPERTIES &thisProps, TENNISCOURT_LOCATION iAmbientCourt=TC_MICHAELS)
	CPRINTLN(DEBUG_TENNIS, "INITIALIZE_TENNIS_COURT called")
	GET_CLOSEST_COURT(thisProps.thisLocation)
	FLOAT fServiceToCourtRatio = 0.46
	
	// Coordinates 0 1 = baseline 1 2 = sideline 2 3 opp baseline 3 0 = sideline
	// 0 = close left, 1 = close right, 2 = far right, 3 = far left
	FLOAT fFrameMultipler = 0.5		// Need a frame multipler as not all the courts are the same size.
	IF (thisProps.thisLocation = ALOC_tennis_michaelHouse) AND thisProps.eTennisActivity <> TA_AI_VS_AI
		// Mike's house court.
		thisCourt.vCourtCorners[0] = << -768.966, 165.352, 66.474 >>
		thisCourt.vCourtCorners[1] = << -777.028, 165.364, 66.474 >>
		thisCourt.vCourtCorners[2] = << -777.045, 141.573, 66.474 >>
		thisCourt.vCourtCorners[3] = << -768.972, 141.566, 66.475 >>
		
		fFrameMultipler = 0.474
		thisProps.vForward = (thisCourt.vCourtCorners[3]-thisCourt.vCourtCorners[0])* fFrameMultipler // Halving so 1.0 = net
		thisProps.vRight = thisCourt.vCourtCorners[1]-thisCourt.vCourtCorners[0]
		thisProps.vUp = CROSS_PRODUCT(thisProps.vRight,thisProps.vForward)
		SET_TENNIS_COURT_WIDTH(thisCourt, VMAG(thisProps.vRight))
	
		thisCourt.vForwardNorm = thisProps.vForward/VMAG(thisProps.vForward)
		thisCourt.vRightNorm = thisProps.vRight/VMAG(thisProps.vRight)
		
		thisCourt.vServiceCorners[0] = << -768.994, 160.238, 66.474 >>
		thisCourt.vServiceCorners[1] = << -777.009, 160.230, 66.474 >>
		thisCourt.vServiceCorners[2] = << -776.999, 146.539, 66.474 >>
		thisCourt.vServiceCorners[3] = << -768.988, 146.552, 66.474 >>
		
		thisCourt.vCenterCourt = << -773.017, 153.584, 66.474 >>
		
		thisCourt.vNetSideline[0] = << -768.981, 153.584, 66.474 >>
		thisCourt.vNetSideline[1] = << -777.006, 153.584, 66.474 >>	
		
		thisCourt.vNetTop[0] = thisCourt.vCenterCourt + (thisProps.vRight * NET_LENGTH_1_MICHAEL)
		thisCourt.vNetTop[0].z += NET_ENDS_HEIGHT_MICHAEL
		thisCourt.vNetTop[1] = thisCourt.vCenterCourt
		thisCourt.vNetTop[1].z += NET_MIDDLE_HEIGHT_MICHAEL
		thisCourt.vNetTop[2] = thisCourt.vCenterCourt + (thisProps.vRight * NET_LENGTH_2_MICHAEL)
		thisCourt.vNetTop[2].z += NET_ENDS_HEIGHT_MICHAEL
		
		SET_TENNIS_PLAYING_COURT(thisCourt, TENNIS_COURT_MichaelHouse)
		SET_TENNIS_COURT_SERVICE_RATIO(thisCourt, 0.430)
		
		#IF NOT IS_TENNIS_MULTIPLAYER
		thisCourt.vLauncherPoint = <<-780.4614, 156.5187, 66.4744>>
		#ENDIF
		CPRINTLN(DEBUG_TENNIS, "VMAG(thisProps.vForward)=", VMAG(thisProps.vForward))
		
	ELIF (thisProps.thisLocation = ALOC_tennis_beachCourt) AND thisProps.eTennisActivity <> TA_AI_VS_AI
		#IF NOT IS_TENNIS_MULTIPLAYER
		thisCourt.vLauncherPoint = <<-1171.28, -1599.59, 3.34>>
		#ENDIF		
		thisCourt.vCourtCorners[0] = << -1173.349, -1604.720, 3.3738 >>
		thisCourt.vCourtCorners[1] = << -1180.108, -1609.459, 3.3738 >>
		thisCourt.vCourtCorners[2] = << -1166.443, -1628.969, 3.3738 >>
		thisCourt.vCourtCorners[3] = << -1159.670, -1624.238, 3.3738 >>
		SET_TENNIS_COURT_VARS_OFF_CORNERS(thisCourt, thisProps, 0.500, 0.460, TRUE)
		SET_TENNIS_PLAYING_COURT(thisCourt, TENNIS_COURT_CountryClub)
		
	ELIF (thisProps.thisLocation = ALOC_tennis_vinewoodhotel1) AND thisProps.eTennisActivity <> TA_AI_VS_AI
		#IF NOT IS_TENNIS_MULTIPLAYER
		thisCourt.vLauncherPoint = <<487.5186, -217.7697, 52.7864>>
		#ENDIF		
		thisCourt.vCourtCorners[0] = <<479.669, -227.811, 52.787>>
		thisCourt.vCourtCorners[1] = <<487.252, -230.572, 52.787>>
		thisCourt.vCourtCorners[2] = <<495.403, -208.181, 52.787>>
		thisCourt.vCourtCorners[3] = <<487.807, -205.400, 52.787>>
		SET_TENNIS_COURT_VARS_OFF_CORNERS(thisCourt, thisProps, 0.500, 0.428)
		SET_TENNIS_PLAYING_COURT(thisCourt, TENNIS_COURT_VinewoodHotel1)
		
	ELIF (thisProps.thisLocation = NO_ACTIVITY_LOCATION) AND thisProps.eTennisActivity <> TA_AI_VS_AI
		#IF NOT IS_TENNIS_MULTIPLAYER
		thisCourt.vLauncherPoint = <<-49.9120, 942.5634, 231.1741>>
		#ENDIF		
		thisCourt.vCourtCorners[0] = <<-54.665, 947.136, 231.174>>
		thisCourt.vCourtCorners[1] = <<-55.353, 939.187, 231.174>>
		thisCourt.vCourtCorners[2] = <<-31.706, 937.116, 231.174>>
		thisCourt.vCourtCorners[3] = <<-31.008, 945.056, 231.174>>
		SET_TENNIS_COURT_VARS_OFF_CORNERS(thisCourt, thisProps, 0.50, 0.428)
		SET_TENNIS_PLAYING_COURT(thisCourt, TENNIS_COURT_DamVista)
		
	ELIF (thisProps.thisLocation = ALOC_tennis_richmanHotel1) AND thisProps.eTennisActivity <> TA_AI_VS_AI
		#IF NOT IS_TENNIS_MULTIPLAYER
		thisCourt.vLauncherPoint = <<-1225.4536, 344.8268, 78.9859>>
		#ENDIF
		thisCourt.vCourtCorners[0] = <<-1223.264, 351.306, 78.9867>>
		thisCourt.vCourtCorners[1] = <<-1231.370, 348.946, 78.9867>>
		thisCourt.vCourtCorners[2] = <<-1224.658, 325.996, 78.9867>>
		thisCourt.vCourtCorners[3] = <<-1216.553, 328.359, 78.9867>>
		SET_TENNIS_COURT_VARS_OFF_CORNERS(thisCourt, thisProps, 0.500, 0.460, TRUE)
		SET_TENNIS_PLAYING_COURT(thisCourt, TENNIS_COURT_RichmanHotel1)
		
	ELIF (thisProps.thisLocation = ALOC_tennis_LSUCourt1) AND thisProps.eTennisActivity <> TA_AI_VS_AI
		#IF NOT IS_TENNIS_MULTIPLAYER
		thisCourt.vLauncherPoint = <<-1623.4541, 257.1566, 58.5552>>
		#ENDIF		
		thisCourt.vCourtCorners[0] = <<-1627.471, 275.479, 58.5552>>
		thisCourt.vCourtCorners[1] = <<-1634.971, 271.977, 58.5552>>
		thisCourt.vCourtCorners[2] = <<-1624.893, 250.367, 58.5552>>
		thisCourt.vCourtCorners[3] = <<-1617.398, 253.864, 58.5552>>
		SET_TENNIS_COURT_VARS_OFF_CORNERS(thisCourt, thisProps, 0.500, 0.460, TRUE)
		SET_TENNIS_PLAYING_COURT(thisCourt, TENNIS_COURT_LSUCourt1)
		
	ELIF (thisProps.thisLocation = ALOC_tennis_vespucciHotel) AND thisProps.eTennisActivity <> TA_AI_VS_AI
		#IF NOT IS_TENNIS_MULTIPLAYER
		thisCourt.vLauncherPoint = <<-939.6168, -1255.7323, 6.9773>>
		#ENDIF		
		thisCourt.vCourtCorners[1] = <<-945.684, -1253.143, 6.9773>>
		thisCourt.vCourtCorners[2] = <<-933.813, -1273.726, 6.9773>>
		thisCourt.vCourtCorners[3] = <<-926.823, -1269.700, 6.9773>>
		thisCourt.vCourtCorners[0] = <<-938.693, -1249.116, 6.9773>>
		SET_TENNIS_COURT_VARS_OFF_CORNERS(thisCourt, thisProps, 0.500, 0.428, TRUE)
		SET_TENNIS_PLAYING_COURT(thisCourt, TENNIS_COURT_VespucciHotel)
		
	ELIF (thisProps.thisLocation = ALOC_tennis_weazelCourt1) AND thisProps.eTennisActivity <> TA_AI_VS_AI
		#IF NOT IS_TENNIS_MULTIPLAYER
		thisCourt.vLauncherPoint = <<-1371.2748, -107.9437, 49.7046>>
		#ENDIF		
		thisCourt.vCourtCorners[0] = <<-1374.660, -114.005, 49.7046>>
		thisCourt.vCourtCorners[1] = <<-1366.627, -113.082, 49.7046>>
		thisCourt.vCourtCorners[2] = <<-1369.309, -89.432, 49.7046>>
		thisCourt.vCourtCorners[3] = <<-1377.338, -90.342, 49.7046>>
		SET_TENNIS_COURT_VARS_OFF_CORNERS(thisCourt, thisProps, 0.500, 0.430, TRUE)
		SET_TENNIS_PLAYING_COURT(thisCourt, TENNIS_COURT_WeazelCourt1)
		
	ELIF (thisProps.thisLocation = ALOC_tennis_chumashHotel) AND thisProps.eTennisActivity <> TA_AI_VS_AI
		#IF NOT IS_TENNIS_MULTIPLAYER
		thisCourt.vLauncherPoint = <<-2869.9915, 9.2297, 10.6083>>
		#ENDIF		
		thisCourt.vCourtCorners[0] = <<-2875.503, 5.330, 10.6083>>
		thisCourt.vCourtCorners[1] = <<-2867.721, 3.090, 10.6083>>
		thisCourt.vCourtCorners[2] = <<-2861.163, 25.966, 10.6083>>
		thisCourt.vCourtCorners[3] = <<-2868.950, 28.198, 10.6083>>
		SET_TENNIS_COURT_VARS_OFF_CORNERS(thisCourt, thisProps, 0.500, 0.458, TRUE)
		SET_TENNIS_PLAYING_COURT(thisCourt, TENNIS_COURT_chumashHotel)
		
	ELIF thisProps.eTennisActivity = TA_AI_VS_AI
		SET_TENNIS_PLAYING_COURT(thisCourt, TENNIS_COURT_Ambient)
	ENDIF
	
	SWITCH iAmbientCourt
		CASE TC_VENICE_1
			thisCourt.vCourtCorners[0] = << -1173.374, -1604.762, 3.1734 >>
			thisCourt.vCourtCorners[1] = << -1180.065, -1609.454, 3.1734 >>
			thisCourt.vCourtCorners[2] = << -1166.428, -1628.932, 3.1734 >>
			thisCourt.vCourtCorners[3] = << -1159.739, -1624.236, 3.1734 >>
			
			fFrameMultipler = 0.5
			thisProps.vForward = (thisCourt.vCourtCorners[3]-thisCourt.vCourtCorners[0])* fFrameMultipler // Halving so 1.0 = net
			thisProps.vRight = thisCourt.vCourtCorners[1]-thisCourt.vCourtCorners[0]
			thisProps.vUp = CROSS_PRODUCT(thisProps.vRight,thisProps.vForward)
			
			fServiceToCourtRatio = 0.46 //18/39
			thisCourt.vServiceCorners[0] = thisCourt.vCourtCorners[0]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[1] = thisCourt.vCourtCorners[1]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[2] = thisCourt.vCourtCorners[2]-thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[3] = thisCourt.vCourtCorners[3]-thisProps.vForward*fServiceToCourtRatio
			
			thisCourt.vCenterCourt = (thisCourt.vCourtCorners[0]+thisCourt.vCourtCorners[2])*0.5
			
			thisCourt.vNetSideline[0] = thisCourt.vCourtCorners[0] + thisProps.vForward
			thisCourt.vNetSideline[1] = thisCourt.vCourtCorners[1] + thisProps.vForward
			#IF NOT IS_TENNIS_MULTIPLAYER
			thisCourt.vLauncherPoint = <<-1171.28, -1599.59, 3.34>>
			#ENDIF
		BREAK
		CASE TC_VENICE_2
			thisCourt.vCourtCorners[0] = << -1157.20, -1627.22, 3.1734 >>
			thisCourt.vCourtCorners[1] = << -1163.84, -1632.52, 3.1734 >>
			thisCourt.vCourtCorners[2] = << -1150.21, -1652.12, 3.1734 >>
			thisCourt.vCourtCorners[3] = << -1143.55, -1647.38, 3.1734 >>
			
			fFrameMultipler = 0.5
			thisProps.vForward = (thisCourt.vCourtCorners[3]-thisCourt.vCourtCorners[0])* fFrameMultipler // Halving so 1.0 = net
			thisProps.vRight = thisCourt.vCourtCorners[1]-thisCourt.vCourtCorners[0]
			thisProps.vUp = CROSS_PRODUCT(thisProps.vRight,thisProps.vForward)
			
			fServiceToCourtRatio = 0.46 //18/39
			thisCourt.vServiceCorners[0] = thisCourt.vCourtCorners[0]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[1] = thisCourt.vCourtCorners[1]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[2] = thisCourt.vCourtCorners[2]-thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[3] = thisCourt.vCourtCorners[3]-thisProps.vForward*fServiceToCourtRatio
			
			thisCourt.vCenterCourt = (thisCourt.vCourtCorners[0]+thisCourt.vCourtCorners[2])*0.5
			
			thisCourt.vNetSideline[0] = thisCourt.vCourtCorners[0] + thisProps.vForward
			thisCourt.vNetSideline[1] = thisCourt.vCourtCorners[1] + thisProps.vForward
			#IF NOT IS_TENNIS_MULTIPLAYER
			thisCourt.vLauncherPoint = <<-1171.28, -1599.59, 3.34>>
			#ENDIF
		BREAK
		
		CASE TC_VENICE_3
			thisCourt.vCourtCorners[0] = << -1186.49, -1613.97, 3.1734 >>
			thisCourt.vCourtCorners[1] = << -1193.20, -1618.66, 3.1734 >>
			thisCourt.vCourtCorners[2] = << -1179.52, -1638.12, 3.1734 >>
			thisCourt.vCourtCorners[3] = << -1172.85, -1633.42, 3.1734 >>
			
			fFrameMultipler = 0.5
			thisProps.vForward = (thisCourt.vCourtCorners[3]-thisCourt.vCourtCorners[0])* fFrameMultipler // Halving so 1.0 = net
			thisProps.vRight = thisCourt.vCourtCorners[1]-thisCourt.vCourtCorners[0]
			thisProps.vUp = CROSS_PRODUCT(thisProps.vRight,thisProps.vForward)
			
			fServiceToCourtRatio = 0.46 //18/39
			thisCourt.vServiceCorners[0] = thisCourt.vCourtCorners[0]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[1] = thisCourt.vCourtCorners[1]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[2] = thisCourt.vCourtCorners[2]-thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[3] = thisCourt.vCourtCorners[3]-thisProps.vForward*fServiceToCourtRatio
			
			thisCourt.vCenterCourt = (thisCourt.vCourtCorners[0]+thisCourt.vCourtCorners[2])*0.5
			
			thisCourt.vNetSideline[0] = thisCourt.vCourtCorners[0] + thisProps.vForward
			thisCourt.vNetSideline[1] = thisCourt.vCourtCorners[1] + thisProps.vForward
			#IF NOT IS_TENNIS_MULTIPLAYER
			thisCourt.vLauncherPoint = <<-1171.28, -1599.59, 3.34>>
			#ENDIF
		BREAK
		
		CASE TC_VENICE_4
			thisCourt.vCourtCorners[0] = << -1170.43, -1637.13, 3.1734 >>
			thisCourt.vCourtCorners[1] = << -1177.05, -1641.73, 3.1734 >>
			thisCourt.vCourtCorners[2] = << -1163.35, -1661.24, 3.1734 >>
			thisCourt.vCourtCorners[3] = << -1156.67, -1656.57, 3.1734 >>
			
			fFrameMultipler = 0.5
			thisProps.vForward = (thisCourt.vCourtCorners[3]-thisCourt.vCourtCorners[0])* fFrameMultipler // Halving so 1.0 = net
			thisProps.vRight = thisCourt.vCourtCorners[1]-thisCourt.vCourtCorners[0]
			thisProps.vUp = CROSS_PRODUCT(thisProps.vRight,thisProps.vForward)
			
			fServiceToCourtRatio = 0.46 //18/39
			thisCourt.vServiceCorners[0] = thisCourt.vCourtCorners[0]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[1] = thisCourt.vCourtCorners[1]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[2] = thisCourt.vCourtCorners[2]-thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[3] = thisCourt.vCourtCorners[3]-thisProps.vForward*fServiceToCourtRatio
			
			thisCourt.vCenterCourt = (thisCourt.vCourtCorners[0]+thisCourt.vCourtCorners[2])*0.5
			
			thisCourt.vNetSideline[0] = thisCourt.vCourtCorners[0] + thisProps.vForward
			thisCourt.vNetSideline[1] = thisCourt.vCourtCorners[1] + thisProps.vForward
			#IF NOT IS_TENNIS_MULTIPLAYER
			thisCourt.vLauncherPoint = <<-1171.28, -1599.59, 3.34>>
			#ENDIF
		BREAK
		
		CASE TC_VENICE_5
			thisCourt.vCourtCorners[0] = << -1199.63, -1623.20, 3.1734 >>
			thisCourt.vCourtCorners[1] = << -1206.29, -1627.84, 3.1734 >>
			thisCourt.vCourtCorners[2] = << -1192.65, -1647.33, 3.1734 >>
			thisCourt.vCourtCorners[3] = << -1185.96, -1642.67, 3.1734 >>
			
			fFrameMultipler = 0.5
			thisProps.vForward = (thisCourt.vCourtCorners[3]-thisCourt.vCourtCorners[0])* fFrameMultipler // Halving so 1.0 = net
			thisProps.vRight = thisCourt.vCourtCorners[1]-thisCourt.vCourtCorners[0]
			thisProps.vUp = CROSS_PRODUCT(thisProps.vRight,thisProps.vForward)
			
			fServiceToCourtRatio = 0.46 //18/39
			thisCourt.vServiceCorners[0] = thisCourt.vCourtCorners[0]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[1] = thisCourt.vCourtCorners[1]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[2] = thisCourt.vCourtCorners[2]-thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[3] = thisCourt.vCourtCorners[3]-thisProps.vForward*fServiceToCourtRatio
			
			thisCourt.vCenterCourt = (thisCourt.vCourtCorners[0]+thisCourt.vCourtCorners[2])*0.5
			
			thisCourt.vNetSideline[0] = thisCourt.vCourtCorners[0] + thisProps.vForward
			thisCourt.vNetSideline[1] = thisCourt.vCourtCorners[1] + thisProps.vForward
			#IF NOT IS_TENNIS_MULTIPLAYER
			thisCourt.vLauncherPoint = <<-1171.28, -1599.59, 3.34>>
			#ENDIF
		BREAK
		
		CASE TC_VENICE_6
			thisCourt.vCourtCorners[0] = << -1183.47, -1646.29, 3.1734 >>
			thisCourt.vCourtCorners[1] = << -1190.16, -1650.97, 3.1734 >>
			thisCourt.vCourtCorners[2] = << -1176.48, -1670.47, 3.1734 >>
			thisCourt.vCourtCorners[3] = << -1169.79, -1665.81, 3.1734 >>
			
			fFrameMultipler = 0.5
			thisProps.vForward = (thisCourt.vCourtCorners[3]-thisCourt.vCourtCorners[0])* fFrameMultipler // Halving so 1.0 = net
			thisProps.vRight = thisCourt.vCourtCorners[1]-thisCourt.vCourtCorners[0]
			thisProps.vUp = CROSS_PRODUCT(thisProps.vRight,thisProps.vForward)
			
			fServiceToCourtRatio = 0.46 //18/39
			thisCourt.vServiceCorners[0] = thisCourt.vCourtCorners[0]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[1] = thisCourt.vCourtCorners[1]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[2] = thisCourt.vCourtCorners[2]-thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[3] = thisCourt.vCourtCorners[3]-thisProps.vForward*fServiceToCourtRatio
			
			thisCourt.vCenterCourt = (thisCourt.vCourtCorners[0]+thisCourt.vCourtCorners[2])*0.5
			
			thisCourt.vNetSideline[0] = thisCourt.vCourtCorners[0] + thisProps.vForward
			thisCourt.vNetSideline[1] = thisCourt.vCourtCorners[1] + thisProps.vForward
			#IF NOT IS_TENNIS_MULTIPLAYER
			thisCourt.vLauncherPoint = <<-1171.28, -1599.59, 3.34>>
			#ENDIF
		BREAK
		
		CASE TC_VENICE_7
			thisCourt.vCourtCorners[0] = << -1196.58, -1655.50, 3.1734 >>
			thisCourt.vCourtCorners[1] = << -1203.27, -1660.14, 3.1734 >>
			thisCourt.vCourtCorners[2] = << -1189.58, -1679.62, 3.1734 >>
			thisCourt.vCourtCorners[3] = << -1182.90, -1674.99, 3.1734 >>
			
			fFrameMultipler = 0.5
			thisProps.vForward = (thisCourt.vCourtCorners[3]-thisCourt.vCourtCorners[0])* fFrameMultipler // Halving so 1.0 = net
			thisProps.vRight = thisCourt.vCourtCorners[1]-thisCourt.vCourtCorners[0]
			thisProps.vUp = CROSS_PRODUCT(thisProps.vRight,thisProps.vForward)
			
			fServiceToCourtRatio = 0.46 //18/39
			thisCourt.vServiceCorners[0] = thisCourt.vCourtCorners[0]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[1] = thisCourt.vCourtCorners[1]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[2] = thisCourt.vCourtCorners[2]-thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[3] = thisCourt.vCourtCorners[3]-thisProps.vForward*fServiceToCourtRatio
			
			thisCourt.vCenterCourt = (thisCourt.vCourtCorners[0]+thisCourt.vCourtCorners[2])*0.5
			
			thisCourt.vNetSideline[0] = thisCourt.vCourtCorners[0] + thisProps.vForward
			thisCourt.vNetSideline[1] = thisCourt.vCourtCorners[1] + thisProps.vForward
			#IF NOT IS_TENNIS_MULTIPLAYER
			thisCourt.vLauncherPoint = <<-1171.28, -1599.59, 3.34>>
			#ENDIF
		BREAK
		
		CASE TC_VENICE_8
			thisCourt.vCourtCorners[0] = << -1212.78, -1632.34, 3.1734 >>
			thisCourt.vCourtCorners[1] = << -1219.50, -1636.97, 3.1734 >>
			thisCourt.vCourtCorners[2] = << -1205.75, -1656.57, 3.1734 >>
			thisCourt.vCourtCorners[3] = << -1199.12, -1651.88, 3.1734 >>
			
			fFrameMultipler = 0.5
			thisProps.vForward = (thisCourt.vCourtCorners[3]-thisCourt.vCourtCorners[0])* fFrameMultipler // Halving so 1.0 = net
			thisProps.vRight = thisCourt.vCourtCorners[1]-thisCourt.vCourtCorners[0]
			thisProps.vUp = CROSS_PRODUCT(thisProps.vRight,thisProps.vForward)
			
			fServiceToCourtRatio = 0.46 //18/39
			thisCourt.vServiceCorners[0] = thisCourt.vCourtCorners[0]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[1] = thisCourt.vCourtCorners[1]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[2] = thisCourt.vCourtCorners[2]-thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[3] = thisCourt.vCourtCorners[3]-thisProps.vForward*fServiceToCourtRatio
			
			thisCourt.vCenterCourt = (thisCourt.vCourtCorners[0]+thisCourt.vCourtCorners[2])*0.5
			
			thisCourt.vNetSideline[0] = thisCourt.vCourtCorners[0] + thisProps.vForward
			thisCourt.vNetSideline[1] = thisCourt.vCourtCorners[1] + thisProps.vForward
			#IF NOT IS_TENNIS_MULTIPLAYER
			thisCourt.vLauncherPoint = <<-1171.28, -1599.59, 3.34>>
			#ENDIF
		BREAK
		
		CASE TC_POSH_THEFT_MISSION
			thisCourt.vCourtCorners[0] = << -54.61, 947.00, 231.19 >>
			thisCourt.vCourtCorners[1] = << -55.26, 939.30, 231.19 >>
			thisCourt.vCourtCorners[2] = << -31.67, 937.09, 231.18 >>
			thisCourt.vCourtCorners[3] = << -31.11, 945.09, 231.19 >>
			
			fFrameMultipler = 0.5
			thisProps.vForward = (thisCourt.vCourtCorners[3]-thisCourt.vCourtCorners[0])* fFrameMultipler // Halving so 1.0 = net
			thisProps.vRight = thisCourt.vCourtCorners[1]-thisCourt.vCourtCorners[0]
			thisProps.vUp = CROSS_PRODUCT(thisProps.vRight,thisProps.vForward)
			
			fServiceToCourtRatio = 0.46 //18/39
			thisCourt.vServiceCorners[0] = thisCourt.vCourtCorners[0]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[1] = thisCourt.vCourtCorners[1]+thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[2] = thisCourt.vCourtCorners[2]-thisProps.vForward*fServiceToCourtRatio
			thisCourt.vServiceCorners[3] = thisCourt.vCourtCorners[3]-thisProps.vForward*fServiceToCourtRatio
			
			thisCourt.vCenterCourt = (thisCourt.vCourtCorners[0]+thisCourt.vCourtCorners[2])*0.5
			
			thisCourt.vNetSideline[0] = thisCourt.vCourtCorners[0] + thisProps.vForward
			thisCourt.vNetSideline[1] = thisCourt.vCourtCorners[1] + thisProps.vForward
			#IF NOT IS_TENNIS_MULTIPLAYER
			thisCourt.vLauncherPoint = <<-43.02, 942.03, 232.19>>
			#ENDIF
		BREAK
		
		DEFAULT
			CPRINTLN(DEBUG_TENNIS, "Not an ambient game or CASE ", iAmbientCourt, " is currently invalid.") 
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Checks the ball's current position to see if it bounced inside the service area
///    
/// RETURNS:
///    
FUNC BOOL IS_BALL_INSIDE_SERVICE_AREA(TENNIS_COURT &thisCourt, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER_ID eWhoIsServing, SERVING_SIDE_ENUM eServingSide)
	VECTOR vBallPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
	IF ABSF(vBallPos.z-thisCourt.vCourtCorners[0].z) <= 0.6
		CPRINTLN(DEBUG_TENNIS, "Game Timer: ", GET_GAME_TIMER(), " IS_BALL_INSIDE_SERVICE_AREA Ball is low enough for check")
		
		VECTOR vPointToCheck = thisProps.tennisBall.vCollPos
		VECTOR vCorner1, vCorner2
		
		IF eWhoIsServing = TENNIS_PLAYER_AWAY
			IF eServingSide = SERVING_FROM_RIGHT
				CPRINTLN(DEBUG_TENNIS, "IS_BALL_INSIDE_SERVICE_AREA: away player from right")
				vCorner1 = thisCourt.vNetSideline[0] + (thisProps.vRight * 0.25)
				vCorner2 = thisCourt.vServiceCorners[3] + (thisProps.vRight * 0.25)
			ELSE
				CPRINTLN(DEBUG_TENNIS, "IS_BALL_INSIDE_SERVICE_AREA: away player from left")
				vCorner1 = thisCourt.vNetSideline[0] + (thisProps.vRight * 0.75)
				vCorner2 = thisCourt.vServiceCorners[3] + (thisProps.vRight * 0.75)
			ENDIF
		ELIF eWhoIsServing = TENNIS_PLAYER_HOME
			IF eServingSide = SERVING_FROM_RIGHT
				CPRINTLN(DEBUG_TENNIS, "IS_BALL_INSIDE_SERVICE_AREA: home player from right")
				vCorner1 = thisCourt.vNetSideline[0] + (thisProps.vRight * 0.75)
				vCorner2 = thisCourt.vServiceCorners[0] + (thisProps.vRight * 0.75)
			ELSE
				CPRINTLN(DEBUG_TENNIS, "IS_BALL_INSIDE_SERVICE_AREA: home player from left")
				vCorner1 = thisCourt.vNetSideline[0] + (thisProps.vRight * 0.25)
				vCorner2 = thisCourt.vServiceCorners[0] + (thisProps.vRight * 0.25)
			ENDIF
		ENDIF
		RETURN IS_POINT_INSIDE_TENNIS_AREA(vPointToCheck, vCorner1, vCorner2, SERVICE_AREA_WIDTH)
	ELSE
		CPRINTLN(DEBUG_TENNIS, "Game Timer: ", GET_GAME_TIMER(), " IS_BALL_INSIDE_SERVICE_AREA Ball not low enough for check")
		RETURN FALSE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the ball is on the courtside passed in, uses a dot product
/// PARAMS:
///    eCourtSide - The courtside you want to check. NOT the player's ID.
///    thisProps - The general properties of the game.
/// RETURNS:
///    TRUE if the dot product is > 0, FALSE otherwise
FUNC BOOL TENNIS_IS_BALL_ON_COURT_SIDE( TENNIS_PLAYER_ID eCourtSide, TENNIS_GAME_PROPERTIES &thisProps )
	
	INT iCorner = PICK_INT( eCourtSide = TENNIS_PLAYER_AWAY, 0, 3 )
	
	VECTOR vBallPos = GET_TENNIS_BALL_POS( thisProps.tennisBall )
	VECTOR vCorner = thisProps.tennisCourt.vCourtCorners[iCorner]
	VECTOR vNet = thisProps.tennisCourt.vNetSideline[0]
	VECTOR vNetToBall = vBallPos - vNet
	VECTOR vNetToCorner = vCorner - vNet
	FLOAT fDot = DOT_PRODUCT( vNetToBall, vNetToCorner )
	
	CDEBUG2LN( DEBUG_TENNIS, "TENNIS_IS_BALL_ON_COURT_SIDE fDot = ", fDot )
	CDEBUG3LN( DEBUG_TENNIS, "iCorner      = ", iCorner )
	CDEBUG3LN( DEBUG_TENNIS, "vBallPos     = ", vBallPos )
	CDEBUG3LN( DEBUG_TENNIS, "vCorner      = ", vCorner )
	CDEBUG3LN( DEBUG_TENNIS, "vNet         = ", vNet )
	CDEBUG3LN( DEBUG_TENNIS, "vNetToBall   = ", vNetToBall )
	CDEBUG3LN( DEBUG_TENNIS, "vNetToCorner = ", vNetToCorner )
	
	#IF IS_DEBUG_BUILD
	VECTOR vBlack = <<0,0,0>>
	DEBUG_RECORD_SPHERE( "vBallPos", vBallPos, 0.1, vBlack )
	DEBUG_RECORD_SPHERE( "vCorner", vCorner, 0.1, vBlack )
	DEBUG_RECORD_SPHERE( "vNet", vNet, 0.1, vBlack )
	DEBUG_RECORD_LINE( "vNetToBall", vNet, vNet + vNetToBall, vBlack, vBlack )
	DEBUG_RECORD_LINE( "vNetToCorner", vNet, vNet + vNetToCorner, vBlack, vBlack )
	#ENDIF
	
	RETURN fDot > 0
	
ENDFUNC

/// PURPOSE:
///    Checks the ball's collision position set in HAS_BALL_BOUNCED to see if the ball was in bounds of the hit.
/// PARAMS:
///    eHitLast - ID of the player to last hit the ball
///    bCheckBounce - If TRUE checks the ball bounced position, false if you want the ball's current position
/// RETURNS:
///    
FUNC BOOL IS_BALL_INSIDE_COURT(TENNIS_PLAYER_ID eHitLast, TENNIS_GAME_PROPERTIES &thisProps, BOOL bCheckBounce = TRUE, BOOL bPrintDebug=FALSE)
	VECTOR vPoint = thisProps.tennisBall.vCollPos
	IF NOT bCheckBounce
		vPoint = GET_TENNIS_BALL_POS(thisProps.tennisBall)
	ENDIF
	
	IF bPrintDebug
		CPRINTLN(DEBUG_TENNIS, "Point we're checking: ", vPoint)
	ENDIF
	
	IF thisProps.tennisPlayers[eHitLast].eCourtSide = TENNIS_PLAYER_HOME
		IF bPrintDebug
			CPRINTLN(DEBUG_TENNIS, "Checking from TENNIS_PLAYER_HOME and checking v1=", thisProps.tennisCourt.vCourtCorners[0] + (thisProps.vRight * 0.5), ", v2=", thisProps.tennisCourt.vNetSideline[0] + (thisProps.vRight * 0.5), ", Width=", GET_TENNIS_COURT_WIDTH(thisProps.tennisCourt))
		ENDIF
		RETURN IS_POINT_INSIDE_TENNIS_AREA(vPoint, 
			thisProps.tennisCourt.vCourtCorners[0] + (thisProps.vRight * 0.5), 
			thisProps.tennisCourt.vNetSideline[0] + (thisProps.vRight * 0.5), GET_TENNIS_COURT_WIDTH(thisProps.tennisCourt))
	ELSE
		IF bPrintDebug
			CPRINTLN(DEBUG_TENNIS, "Checking from TENNIS_PLAYER_AWAY and checking v1=", thisProps.tennisCourt.vCourtCorners[3] + (thisProps.vRight * 0.5), ", v2=", thisProps.tennisCourt.vNetSideline[0] + (thisProps.vRight * 0.5), ", Width=", GET_TENNIS_COURT_WIDTH(thisProps.tennisCourt))
		ENDIF
		RETURN IS_POINT_INSIDE_TENNIS_AREA(vPoint, 
			thisProps.tennisCourt.vCourtCorners[3] + (thisProps.vRight * 0.5), 
			thisProps.tennisCourt.vNetSideline[0] + (thisProps.vRight * 0.5), GET_TENNIS_COURT_WIDTH(thisProps.tennisCourt))
	ENDIF
	
	CPRINTLN(DEBUG_TENNIS, "IS_BALL_INSIDE_COURT: Player has no courtside, returning FALSE")
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BALL_INSIDE_ENTIRE_COURT(TENNIS_GAME_PROPERTIES &thisProps, BOOL bCheckBounce = TRUE, BOOL bPrintDebug=FALSE)
	VECTOR vPoint = thisProps.tennisBall.vCollPos
	IF NOT bCheckBounce
		vPoint = GET_TENNIS_BALL_POS(thisProps.tennisBall)
	ENDIF
	
	IF bPrintDebug
		CPRINTLN(DEBUG_TENNIS, "Point we're checking: ", vPoint)
	ENDIF
	
	RETURN IS_POINT_INSIDE_TENNIS_AREA(vPoint, 
		thisProps.tennisCourt.vCourtCorners[0] + (thisProps.vRight * 0.5), 
		thisProps.tennisCourt.vCourtCorners[3] + (thisProps.vRight * 0.5), GET_TENNIS_COURT_WIDTH(thisProps.tennisCourt))
ENDFUNC

/// PURPOSE:
///    Automatically increases iBounceCount if the ball bounces
/// PARAMS:
///    thisProps - the properties of the game
///    eHitLast - the enum of the last player to hit the ball
/// RETURNS:
///    FALSE if the ball is outside the court or if the bouncecount is above 1
///    TRUE if the ball hasn't bounced or it has bounced inside the court and but only once.
FUNC BOOL UPDATE_BOUNCE_COUNT(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER_ID eHitLast)
	IF HAS_BALL_BOUNCED(thisProps.tennisBall, GET_TENNIS_FRAME_COUNTER(thisProps), thisProps.tennisCourt.vCenterCourt)
		CPRINTLN(DEBUG_TENNIS, "Game Timer: ",GET_GAME_TIMER(), " UPDATE_BOUNCE_COUNT: Ball has bounced, checking pos ", thisProps.tennisBall.vCollPos)
		IF IS_BALL_INSIDE_COURT(eHitLast, thisProps, TRUE, TRUE)
			INCREASE_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
			IF GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall) > 1
				CPRINTLN(DEBUG_TENNIS, "Game Timer: ",GET_GAME_TIMER(), " UPDATE_BOUNCE_COUNT: Returning FALSE, bounceCount > 1, Ball is inside court")
				RETURN FALSE
			ENDIF
		ELSE	
			INCREASE_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
			CPRINTLN(DEBUG_TENNIS, "Game Timer: ",GET_GAME_TIMER(), " UPDATE_BOUNCE_COUNT: Returning FALSE, ball is outside court")
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC	


PROC CLEAR_COURT_OF_PROPS(TENNIS_GAME_PROPERTIES &thisProps)
	// Clear the court.
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vCourtCorners[0], 3.0, CLEAROBJ_FLAG_BROADCAST)
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vCourtCorners[1], 3.0, CLEAROBJ_FLAG_BROADCAST)
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vCourtCorners[2], 3.0, CLEAROBJ_FLAG_BROADCAST)
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vCourtCorners[3], 3.0, CLEAROBJ_FLAG_BROADCAST)
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vServiceCorners[0], 4.0, CLEAROBJ_FLAG_BROADCAST)
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vServiceCorners[1], 4.0, CLEAROBJ_FLAG_BROADCAST)
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vServiceCorners[2], 4.0, CLEAROBJ_FLAG_BROADCAST)
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vServiceCorners[3], 4.0, CLEAROBJ_FLAG_BROADCAST)
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vCenterCourt, 5.0, CLEAROBJ_FLAG_BROADCAST)
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vNetSideline[0], 3.0, CLEAROBJ_FLAG_BROADCAST)
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vNetSideline[1], 3.0, CLEAROBJ_FLAG_BROADCAST)
ENDPROC

FUNC BOOL MOVE_VEHICLE_TO_SAFE_LOCATION_FOR_COURT(TENNIS_GAME_PROPERTIES &thisProps, VEHICLE_INDEX vehPlayer)
	IF NOT DOES_ENTITY_EXIST(vehPlayer)
	OR IS_ENTITY_DEAD(vehPlayer)
		CWARNINGLN(DEBUG_TENNIS, "MOVE_VEHICLE_TO_SAFE_LOCATION_FOR_COURT: NOT DOES_ENTITY_EXIST(vehPlayer) OR IS_ENTITY_DEAD(vehPlayer)")
		RETURN FALSE
	ENDIF
	
	IF thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_DamVista
	OR thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_Ambient
		CWARNINGLN(DEBUG_TENNIS, "MOVE_VEHICLE_TO_SAFE_LOCATION_FOR_COURT: eTennisCourt = TENNIS_COURT_DamVista OR eTennisCourt = TENNIS_COURT_Ambient")
		RETURN FALSE
	ENDIF
	
	IF VDIST( thisProps.tennisCourt.vCenterCourt, GET_ENTITY_COORDS( vehPlayer ) ) > COURT_VEHICLE_REPOSITION_DISTANCE
		CWARNINGLN(DEBUG_TENNIS, "MOVE_VEHICLE_TO_SAFE_LOCATION_FOR_COURT: VDIST > COURT_VEHICLE_REPOSITION_DISTANCE")
		RETURN FALSE
	ENDIF
	
	BOOL bLargeVehicle = MINIGAME_DOES_VEHICLE_NEED_LARGE_RESPAWN_SPACE( vehPlayer )

	VECTOR vSafeVehPos = <<0,0,0>>
	FLOAT fSafeVehPos = 0.0
	SWITCH thisProps.tennisCourt.eTennisCourt
		CASE TENNIS_COURT_MichaelHouse
			vSafeVehPos = <<-805.95, 162.74, 71.18>>
			fSafeVehPos = 110.37
			IF bLargeVehicle
				vSafeVehPos = <<-825.4709, 215.6689, 73.4421>>
				fSafeVehPos = 84.1430
			ENDIF
		BREAK
		CASE TENNIS_COURT_CountryClub
			vSafeVehPos = <<-1153.20, -1596.58, 3.86>>
			fSafeVehPos = 306.84
			IF bLargeVehicle
				vSafeVehPos = <<-1164.8694, -1703.5923, 3.3163>>
				fSafeVehPos = 300.9732
			ENDIF
		BREAK
		CASE TENNIS_COURT_VinewoodHotel1
			vSafeVehPos = <<508.79, -244.82, 47.93>>
			fSafeVehPos = 155.65
			IF bLargeVehicle
				vSafeVehPos = <<510.6842, -258.4083, 46.8062>>
				fSafeVehPos = 329.2708
			ENDIF
		BREAK
		CASE TENNIS_COURT_RichmanHotel1
			vSafeVehPos = <<-1253.99, 385.51, 74.97>>
			fSafeVehPos = 360.00
			IF bLargeVehicle
				vSafeVehPos = <<-1246.9839, 397.7199, 74.4786>>
				fSafeVehPos = 102.7385
			ENDIF
		BREAK	
		CASE TENNIS_COURT_LSUCourt1
			vSafeVehPos = <<-1655.90, 291.61, 60.08>>
			fSafeVehPos = 292.79
			IF bLargeVehicle
				vSafeVehPos = <<-1656.8270, 293.4891, 59.5975>>
				fSafeVehPos = 110.6497
			ENDIF
		BREAK
		CASE TENNIS_COURT_VespucciHotel
			vSafeVehPos = <<-938.62, -1230.65, 4.80>>
			fSafeVehPos = 300.67
			IF bLargeVehicle
				vSafeVehPos = <<-958.2239, -1203.2472, 4.1835>>
				fSafeVehPos = 210.0461
			ENDIF
		BREAK
		CASE TENNIS_COURT_WeazelCourt1
			vSafeVehPos = <<-1326.50, -119.45, 48.67>>
			fSafeVehPos = 5.13
			IF bLargeVehicle
				vSafeVehPos = <<-1406.2039, -68.0653, 51.7739>>
				fSafeVehPos = 120.6415
			ENDIF
		BREAK
		CASE TENNIS_COURT_chumashHotel
			vSafeVehPos = <<-2847.71, 42.12, 14.11>>
			fSafeVehPos = 248.96
			IF bLargeVehicle
				vSafeVehPos = <<-2826.7622, 45.4984, 13.6326>>
				fSafeVehPos = 249.5827
			ENDIF
		BREAK
	ENDSWITCH
	CLEAR_AREA_OF_VEHICLES(vSafeVehPos, 5.0)
	SET_ENTITY_COORDS(vehPlayer, vSafeVehPos)
	SET_ENTITY_HEADING(vehPlayer, fSafeVehPos)
	SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
	CPRINTLN(DEBUG_TENNIS, "MOVE_VEHICLE_TO_SAFE_LOCATION_FOR_COURT: vehicle inside court, moving to predefined safe location")
	RETURN TRUE
ENDFUNC

