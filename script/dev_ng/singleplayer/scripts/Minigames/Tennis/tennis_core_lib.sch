USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "commands_camera.sch"
//USING "commands_ped.sch"
USING "commands_object.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "script_maths.sch"
USING "commands_graphics.sch"   
USING "minigame_uiinputs.sch"
USING "friendActivity_public.sch"
USING "asset_management_public.sch"
USING "cheat_handler.sch"
//USING "text_queue.sch"

USING "tennis.sch"
USING "tennis_cameras.sch"
USING "tennis_UI.sch"
USING "tennis_UI_lib.sch"
USING "tennis_lib.sch"
USING "tennis_player.sch"
USING "tennis_player_lib.sch"
USING "tennis_ai.sch"
USING "tennis_debug_lib.sch"

PROC HANDLE_TENNIS_RESET_FLAGS(TENNIS_GAME_STATE_ENUM thisState, #IF NOT IS_TENNIS_MULTIPLAYER TENNIS_PLAYER &thisPlayer, #ENDIF INT iUtilFlags, BOOL bOverride = FALSE)
	IF ((thisState > TGSE_OPTION_MENU AND thisState < TGSE_OUTRO) 
		AND NOT IS_BITMASK_AS_ENUM_SET(iUtilFlags, TD_ANIM_PLAYING) 
		#IF NOT IS_TENNIS_MULTIPLAYER 	AND thisState <> TGSE_INTERSTITIAL #ENDIF 
		#IF IS_TENNIS_MULTIPLAYER 		AND thisState <> TGSE_SIDE_CHANGE #ENDIF 
		)
	OR bOverride
		//MoVE network calls, every frame
		#IF NOT IS_TENNIS_MULTIPLAYER
			IF DOES_ENTITY_EXIST(GET_TENNIS_PLAYER_INDEX(thisPlayer))
				IF IS_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisPlayer))
					SET_PED_RESET_FLAG(GET_TENNIS_PLAYER_INDEX(thisPlayer), PRF_ForcePedToStrafe, TRUE)
//					CDEBUG3LN(DEBUG_TENNIS, "Reset forced to strafe being called for ", thisPlayer.playerAI.texName)
				ENDIF
				SET_PED_RESET_FLAG(GET_TENNIS_PLAYER_INDEX(thisPlayer), PRF_DisablePlayerAutoVaulting, TRUE)
//				CDEBUG3LN(DEBUG_TENNIS, "Reset flags being called for ", thisPlayer.playerAI.texName)
			ENDIF
		#ENDIF
		
		#IF IS_TENNIS_MULTIPLAYER
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToStrafe, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
		#ENDIF
	ENDIF
ENDPROC

PROC TENNIS_EVERY_FRAME_DISABLES()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MULTIPLAYER_INFO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	
	//Hiding the area name from popping up during the game
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
	
	IF NOT IS_CELLPHONE_DISABLED()
		DISABLE_CELLPHONE(TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Call at the beginning of the state machine
/// PARAMS:
///    thisProps - 
///    thisState - 
///    iUtilFlags - 
///    eSelfID - 
///    iClientControlFlags - Only needed for a check in MP.
PROC COMMON_EVERY_FRAME_DATA(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATE_ENUM thisState, INT iUtilFlags, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, #IF IS_TENNIS_MULTIPLAYER TENNIS_PARTICIPANT_INFO &info, #ENDIF BOOL bIgnoreResetFlags = FALSE)
	#IF IS_DEBUG_BUILD
		IS_ANIM_PLAYING = IS_BITMASK_AS_ENUM_SET(iUtilFlags, TD_ANIM_PLAYING)
	#ENDIF
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	
	IF thisState >= TGSE_RALLY AND thisState < TGSE_POINT_WON
		INCREASE_TENNIS_BALL_BOUNCE_TIMEOUT(thisProps.tennisBall)
	ENDIF
	
	IF thisProps.eTennisActivity <> TA_AI_VS_AI
		TENNIS_EVERY_FRAME_DISABLES()
		
		IF IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			ENDIF
		ENDIF
	ENDIF
	
	#IF NOT IS_TENNIS_MULTIPLAYER
		UPDATE_TENNIS_BALL_TRAIL(thisProps.tennisBall, GET_TENNIS_BALL_POS(thisProps.tennisBall))
	#ENDIF
	
	#IF IS_TENNIS_MULTIPLAYER
		SET_IDLE_KICK_DISABLED_THIS_FRAME()
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT bIgnoreResetFlags
			thisProps.tennisPlayers[eSelfID].vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			HANDLE_TENNIS_RESET_FLAGS(thisState, iUtilFlags)
		ENDIF
		
		IF info.iPlayerCount = ENUM_TO_INT(TENNIS_PLAYERS)
			PED_INDEX otherPlayer
			otherPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iPlayers[eOtherID])))
			IF NOT IS_PED_INJURED(otherPlayer)
				thisProps.tennisPlayers[eOtherID].vPos = GET_ENTITY_COORDS(otherPlayer)
			ENDIF
		ENDIF
		
		IF NOT IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS)
			DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, TRUE)
		ENDIF
	#ENDIF
	#IF NOT IS_TENNIS_MULTIPLAYER
		IF NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
			BOOL bNotInjured = NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
			IF (bNotInjured AND NOT bIgnoreResetFlags AND NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_NEW_RALLY))
			OR (bNotInjured AND IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eSelfID], TPB_FORCED_UPDATE) AND thisProps.eTennisActivity = TA_AI_VS_AI)
				thisProps.tennisPlayers[eSelfID].vPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
				HANDLE_TENNIS_RESET_FLAGS(thisState, thisProps.tennisPlayers[eSelfID], iUtilFlags, IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eSelfID], TPB_FORCED_UPDATE))
			ENDIF
			bNotInjured = NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID])) AND DOES_ENTITY_EXIST(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
			IF (bNotInjured AND NOT bIgnoreResetFlags AND NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eOtherID].playerAI, TAF_TASKED_NEW_RALLY))
			OR (bNotInjured AND IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eOtherID], TPB_FORCED_UPDATE) AND thisProps.eTennisActivity = TA_AI_VS_AI)
				thisProps.tennisPlayers[eOtherID].vPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
				HANDLE_TENNIS_RESET_FLAGS(thisState, thisProps.tennisPlayers[eOtherID], iUtilFlags, IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eOtherID], TPB_FORCED_UPDATE))
			ENDIF
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    Call this only at the end of the loop state machine
/// PARAMS:
///    thisProps - 
PROC COMMON_TENNIS_END_OF_FRAME_DATA(TENNIS_GAME_PROPERTIES &thisProps)
	IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[0].oTennisRacket)
		thisProps.tennisPlayers[0].vPrevBatPos = GET_ENTITY_COORDS(thisProps.tennisPlayers[0].oTennisRacket)
	ENDIF
	IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[1].oTennisRacket)
		thisProps.tennisPlayers[1].vPrevBatPos = GET_ENTITY_COORDS(thisProps.tennisPlayers[1].oTennisRacket)
	ENDIF
ENDPROC

FUNC BOOL VALIDATE_QUIT_BUTTON_PRESSED(TENNIS_GAME_STATE_ENUM thisTGSE)
	IF thisTGSE = TGSE_OPTION_MENU
		IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
			RETURN TRUE
		ENDIF
		IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
			RETURN TRUE
		ENDIF
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    RETURNs TRUE if any participating ped has been injured or if the player has a wanted level
/// PARAMS:
///    thisProps - 
FUNC BOOL HANDLE_TENNIS_FAIL_SATES(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER_ID eSelfID, INT &iUtilFlags)
	PED_INDEX ped1
	ped1 = GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID])
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_PED_SHOOTING(PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_TENNIS, "IF IS_PED_SHOOTING(PLAYER_PED_ID())")
		IF NOT IS_PED_INJURED(ped1)
			TASK_SEEK_COVER_FROM_PED(ped1, PLAYER_PED_ID(), -1)
		ENDIF
		IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)]))
			TASK_SEEK_COVER_FROM_PED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)]), PLAYER_PED_ID(), -1)
		ENDIF
		RETURN TRUE
	ENDIF
//	IF thisProps.eTennisActivity = TA_AI_VS_AI AND IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMBIENT_SHUTDOWN)
//		CDEBUG1LN(DEBUG_TENNIS, "IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMBIENT_SHUTDOWN)")
//		RETURN TRUE
//	ENDIF
	
	IF IS_PED_INJURED(ped1)
		CDEBUG1LN(DEBUG_TENNIS, "IS_PED_INJURED(ped1)")
		RETURN TRUE
	ENDIF
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(ped1)
		CDEBUG1LN(DEBUG_TENNIS, "HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(ped1)")
		RETURN TRUE
	ENDIF
	IF IS_PED_IN_MELEE_COMBAT(ped1)
		CDEBUG1LN(DEBUG_TENNIS, "IS_PED_IN_MELEE_COMBAT(ped1)")
		RETURN TRUE
	ENDIF
	IF NOT IS_ENTITY_DEAD(ped1) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped1, PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_TENNIS, "HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped1)")
		RETURN TRUE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(ped1)
		CDEBUG1LN(DEBUG_TENNIS, "NOT DOES_ENTITY_EXIST(ped1)")
		RETURN TRUE
	ENDIF
	IF thisProps.eTennisActivity <> TA_AI_VS_AI AND IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		PRINT_NOW("TENN_WANTED", DEFAULT_GOD_TEXT_TIME, 0)
		SET_BITMASK_AS_ENUM(iUtilFlags, TD_QUIT_BIT)	// Set this so we can display help text and not clear it.
		CDEBUG1LN(DEBUG_TENNIS, "Quitting, player wanted level is too high.")
		RETURN TRUE
	ENDIF
	IF IS_PED_RAGDOLL(ped1)
		CPRINTLN(DEBUG_TENNIS, "Quitting, ped1 is ragdolling")
		RETURN TRUE
	ENDIF
	
	#IF NOT IS_TENNIS_MULTIPLAYER
		PED_INDEX ped2
		TENNIS_PLAYER_ID eOtherID = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eSelfID))
		ped2 = GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID])
		
		IF IS_PED_INJURED(ped2)
			CDEBUG1LN(DEBUG_TENNIS, "IS_PED_INJURED(ped2)")
			RETURN TRUE
		ENDIF
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(ped2)
			CDEBUG1LN(DEBUG_TENNIS, "HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(ped2)")
			RETURN TRUE
		ENDIF
		IF IS_PED_IN_MELEE_COMBAT(ped2)
			CDEBUG1LN(DEBUG_TENNIS, "IS_PED_IN_MELEE_COMBAT(ped2)")
			RETURN TRUE
		ENDIF
		IF NOT IS_ENTITY_DEAD(ped2) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped2, PLAYER_PED_ID())
			CDEBUG1LN(DEBUG_TENNIS, "HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped2)")
			RETURN TRUE
		ENDIF
		IF NOT DOES_ENTITY_EXIST(ped2)
			CDEBUG1LN(DEBUG_TENNIS, "NOT DOES_ENTITY_EXIST(ped2)")
			RETURN TRUE
		ENDIF
		IF IS_PED_RAGDOLL(ped2)
			CDEBUG1LN(DEBUG_TENNIS, "Quitting, ped1 is ragdolling")
			RETURN TRUE
		ENDIF
		
		IF thisProps.eTennisActivity = TA_AI_VS_AI
		AND (IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), ped1) OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), ped2))
			CDEBUG1LN(DEBUG_TENNIS, "Quitting, Player is touching one of the participants")
			RETURN TRUE
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_TENNIS_MID_MATCH_DIALOGUE(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_GAME_STATUS &thisStatus, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)	
	IF IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_LONG_RALLY)
		CLEAR_TENNIS_CONVO_FLAG(thisUIData, TCVF_LONG_RALLY)
		INT iPlayer = 1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus))
		PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[iPlayer], "TENNIS_LONG_RALLY", GET_TENNIS_PLAYER_VOICE(thisProps.tennisPlayers[iPlayer]), SPEECH_PARAMS_FORCE_SHOUTED)
		CDEBUG2LN(DEBUG_TENNIS, "HANDLE_TENNIS_MID_MATCH_DIALOGUE :: TENNIS_LONG_RALLY played by ", thisProps.tennisPlayers[iPlayer].playerAI.texName)
	ENDIF
	
	SWITCH GET_TENNIS_GAME_STATE(thisStatus)	
		CASE TGSE_PREPARE_SERVE
			RESTART_TIMER_NOW(thisProps.tServe)
		BREAK
	
		CASE TGSE_SERVE
			IF (thisUIData.playerChars[eOtherID] = CHAR_MICHAEL
			OR thisUIData.playerChars[eOtherID] = CHAR_AMANDA)
			AND IS_TIMER_STARTED(thisProps.tServe)
			AND (GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) != TSS_SERVE_TOSS)
				IF TIMER_DO_ONCE_WHEN_READY(thisProps.tServe, HURRY_SPEECH_DELAY)
					CDEBUG2LN(DEBUG_TENNIS, "TENNIS_HURRY_UP speech played")
					PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[eOtherID], "TENNIS_HURRY_UP", thisProps.tennisPlayers[eOtherID].texVoice, SPEECH_PARAMS_FORCE_SHOUTED)
				ENDIF
			ENDIF			
		BREAK
		
		CASE TGSE_TENNIS_PLAYERS_WALKING_WAIT		
			RESTART_TIMER_NOW(thisProps.tServe)
			IF IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
				IF (thisUIData.playerChars[eOtherID] = CHAR_AMANDA)
				AND NOT IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_AMANDA_CHAT_BAD_SELF)
					IF GET_RANDOM_FLOAT_IN_RANGE() < 0.33
						IF GET_TENNIS_STATUS_SCORED_LAST(thisStatus) = eSelfID
							CDEBUG2LN(DEBUG_TENNIS, "GAME_BAD_SELF speech played")
							PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[eOtherID], "GAME_BAD_SELF", thisProps.tennisPlayers[eOtherID].texVoice, SPEECH_PARAMS_FORCE_SHOUTED)
						ELSE
							CDEBUG2LN(DEBUG_TENNIS, "TENNIS_CHAT speech played")
							PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[eOtherID], "TENNIS_CHAT", thisProps.tennisPlayers[eOtherID].texVoice, SPEECH_PARAMS_FORCE_SHOUTED)
						ENDIF
					ENDIF
					SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_AMANDA_CHAT_BAD_SELF)
				ENDIF
				
				IF (thisUIData.playerChars[eOtherID] = CHAR_MICHAEL)
				AND NOT IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_MIKE_CHAT)
				AND GET_TENNIS_STATUS_SCORED_LAST(thisStatus) = eSelfID //these sound like Mike is dissappointed with himself 
					IF GET_RANDOM_FLOAT_IN_RANGE() < 0.33						
						CDEBUG2LN(DEBUG_TENNIS, "TENNIS_CHAT speech played")
						PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[eOtherID], "TENNIS_CHAT_WITH_PLAYER", thisProps.tennisPlayers[eOtherID].texVoice, SPEECH_PARAMS_FORCE_SHOUTED)
					ENDIF
					SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_MIKE_CHAT)
				ENDIF
			ENDIF
		BREAK		
		
		DEFAULT
			IF IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_AMANDA_CHAT_BAD_SELF)
				CLEAR_TENNIS_CONVO_FLAG(thisUIData, TCVF_AMANDA_CHAT_BAD_SELF)
			ENDIF
			IF IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_MIKE_CHAT)
				CLEAR_TENNIS_CONVO_FLAG(thisUIData, TCVF_MIKE_CHAT)
			ENDIF
		BREAK
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Handles the dialogue for the player's ped during a tennis game.
/// PARAMS:
///    thisUIData - 
///    eSelfID - the self ID of the human player
PROC HANDLE_TENNIS_DIALOGUE(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATUS &thisStatus, TENNIS_PLAYER &thisPlayer, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, TENNIS_PLAYER_ID eStishFocus)
	TEXT_LABEL_31 texVoice = GET_TENNIS_PLAYER_VOICE(thisPlayer)
	TEXT_LABEL_63 sConvo
	IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_SERVE_DSTAR) AND NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
		SWITCH thisUIData.playerChars[eSelfID]
			CASE CHAR_MICHAEL
				IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_MATCH_POINT_DSTAR)
					sConvo = "MGTN_matpM"
				ELIF thisUIData.playerChars[eOtherID] = CHAR_TREVOR AND NOT IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_MIKE_TREVOR_SERVE_PLAYED)
					SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_MIKE_TREVOR_SERVE_PLAYED)
					IF NOT IS_TENNIS_SAVED_GLOBAL_FLAG_SET(g_savedGlobals.sTennisData, TGF_TREVOR_MEETS_MICHAEL)
						sConvo = "MGTN_CON0"
						SET_TENNIS_SAVED_GLOBAL_FLAG(g_savedGlobals.sTennisData, TGF_TREVOR_MEETS_MICHAEL)
					ELSE
						FLOAT fRand
						fRand = GET_RANDOM_FLOAT_IN_RANGE()
					
						sConvo = PICK_STRING(fRand < 0.25, "MGTN_CON1", PICK_STRING(fRand < 0.50, "MGTN_CON2", PICK_STRING(fRand < 0.75, "MGTN_CON3", "MGTN_CON4")))
					ENDIF
				ELIF thisUIData.playerChars[eOtherID] = CHAR_AMANDA
					IF NOT IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_MIKE_AMANDA_SERVE_PLAYED)						
						IF GET_RANDOM_FLOAT_IN_RANGE() < 0.66
							IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_6)
								sConvo = "MGTN_MA_L"
								sConvo += GET_RANDOM_INT_IN_RANGE(1, 5)
							ELIF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_5)
								sConvo = "MGTN_MA_E"
								IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_3)
									sConvo += GET_RANDOM_INT_IN_RANGE(2, 6) //don't play line about coach
								ELSE
									sConvo += GET_RANDOM_INT_IN_RANGE(1, 6) //can play line about coach
								ENDIF
							ELSE
								sConvo = "MGTN_MA"
								sConvo += GET_RANDOM_INT_IN_RANGE(1, 8)
							ENDIF
						ELSE
							sConvo = "MGTN_MA"
							sConvo += GET_RANDOM_INT_IN_RANGE(1, 8)
						ENDIF
						SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_MIKE_AMANDA_SERVE_PLAYED)
					ELSE
						IF (thisStatus.playerScores[eSelfID].iPointsWon >= 1
						OR thisStatus.playerScores[eOtherID].iPointsWon >= 1)
						AND GET_RANDOM_FLOAT_IN_RANGE() < 0.5		
							SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_PLAY_AS_AMBIENT_SPEECH)
							sConvo = "TENNIS_ABOUT_TO_SERVE_MIDGAME_WITH_AMANDA"
						ELSE
							sConvo = "MGTN_serv1M"
						ENDIF
					ENDIF
				ELSE	
					sConvo = "MGTN_serv1M"
				ENDIF
			BREAK
			CASE CHAR_FRANKLIN
				IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_MATCH_POINT_DSTAR)
					sConvo = "MGTN_matpF"
				ELSE
					sConvo = "MGTN_serv1F"
				ENDIF
			BREAK
			CASE CHAR_TREVOR
				IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_MATCH_POINT_DSTAR)
					sConvo = "MGTN_matpT"
				ELIF thisUIData.playerChars[eOtherID] = CHAR_MICHAEL AND NOT IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_MIKE_TREVOR_SERVE_PLAYED)
					SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_MIKE_TREVOR_SERVE_PLAYED)
					
					IF NOT IS_TENNIS_SAVED_GLOBAL_FLAG_SET(g_savedGlobals.sTennisData, TGF_TREVOR_MEETS_MICHAEL)
						sConvo = "MGTN_CON0"
						SET_TENNIS_SAVED_GLOBAL_FLAG(g_savedGlobals.sTennisData, TGF_TREVOR_MEETS_MICHAEL)
					ELSE
						IF GET_RANDOM_FLOAT_IN_RANGE() < 0.66
							sConvo = PICK_STRING(GET_RANDOM_FLOAT_IN_RANGE() < 0.50, "MGTN_CON5", "MGTN_CON6")
						ELSE						
							sConvo = "TENNIS_START_WITH_MICHAEL"
							SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_PLAY_AS_AMBIENT_SPEECH)
						ENDIF
					ENDIF
				ELIF thisUIData.playerChars[eOtherID] = CHAR_AMANDA AND NOT IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_TREVOR_AMANDA_SERVE_PLAYED)
					SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_TREVOR_AMANDA_SERVE_PLAYED)
					sConvo = "MGTN_TA"
					sConvo += GET_RANDOM_INT_IN_RANGE(1, 7)
				ELIF thisUIData.playerChars[eOtherID] = CHAR_AMANDA
					sConvo = "MGTN_TA_S"
				ELSE
					sConvo = "MGTN_serv1T"
				ENDIF
			BREAK
		ENDSWITCH
		IF IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_PLAY_AS_AMBIENT_SPEECH)
			PLAY_TENNIS_AMBIENT_SPEECH(thisPlayer, sConvo, texVoice, SPEECH_PARAMS_FORCE_SHOUTED)
			CLEAR_TENNIS_CONVO_FLAG(thisUIData, TCVF_PLAY_AS_AMBIENT_SPEECH)
		ELSE
			CREATE_CONVERSATION(thisUIData.myDialogueStruct, thisUIData.sSubtitleGroupID, sConvo, CONV_PRIORITY_HIGH)
		ENDIF
		CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_SERVE_DSTAR)
		CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_MATCH_POINT_DSTAR)
		CPRINTLN(DEBUG_TENNIS, "TD_SERVE_DSTAR sConvo ", sConvo, " played")
	ENDIF
	IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_FAULT_DSTAR)
	AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_PAUSE_DIALOGUE)
		STRING sContext
		IF thisUIData.playerChars[eStishFocus] = CHAR_AMANDA AND thisUIData.playerChars[eSelfID] = CHAR_MICHAEL
			sContext = PICK_STRING(GET_RANDOM_FLOAT_IN_RANGE() < 0.5, "GAME_BAD_SELF", "TENNIS_LUCKY")
		ELSE
			sContext = "GAME_BAD_SELF"
		ENDIF
		CDEBUG2LN(DEBUG_TENNIS, "TD_FAULT_DSTAR speech played")
		PLAY_TENNIS_AMBIENT_SPEECH(thisPlayer, sContext, texVoice)
		CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_FAULT_DSTAR)
	ENDIF
	IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_POINT_W_DSTAR)
	AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_PAUSE_DIALOGUE)
		STRING sContext
		IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ACE)
			sContext = "TENNIS_ACE"
		ELSE	//IF GET_TENNIS_DEUCE_COUNT(thisUIData) <= 1
			FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE()
			sContext = PICK_STRING(fRand < 0.66, "GAME_GOOD_SELF", PICK_STRING(fRand < 0.33, "GAME_BAD_OTHER", "GAME_HECKLE"))
		ENDIF
		CDEBUG2LN(DEBUG_TENNIS, "TD_POINT_W_DSTAR speech played")
		PLAY_TENNIS_AMBIENT_SPEECH(thisPlayer, sContext, texVoice)
		CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_POINT_W_DSTAR)
	ENDIF
	IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_POINT_L_DSTAR)
	AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_PAUSE_DIALOGUE)
		STRING sContext
		BOOL bSpeechContext = TRUE
		IF eStishFocus = eOtherID
			IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ACE)
				sContext = "TENNIS_ACE"
			ELSE
				IF VALIDATE_AMANDA_IS_OPP_AND_LOSING_OVERALL(thisUIData, thisStatus, eSelfID, eOtherID)
					bSpeechContext = FALSE
					sContext = "MGTN_AW_PL"
				ELSE
					FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE()
					sContext = PICK_STRING(fRand < 0.66, "GAME_GOOD_SELF", PICK_STRING(fRand < 0.33, "GAME_BAD_OTHER", "GAME_HECKLE"))
				ENDIF
			ENDIF
		ELSE
			sContext = PICK_STRING(GET_RANDOM_FLOAT_IN_RANGE() < 0.5, "GAME_BAD_SELF", "TENNIS_LUCKY")
		ENDIF
		CDEBUG2LN(DEBUG_TENNIS, "TD_POINT_L_DSTAR speech played")
		IF bSpeechContext
			PLAY_TENNIS_AMBIENT_SPEECH(thisPlayer, sContext, texVoice)
		ELSE
			CREATE_CONVERSATION(thisUIData.myDialogueStruct, thisUIData.sSubtitleGroupID, sContext, CONV_PRIORITY_HIGH)
		ENDIF
		CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_POINT_L_DSTAR)
	ENDIF
	IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_GAME_WON_DSTAR)
	AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_PAUSE_DIALOGUE)
		CDEBUG2LN(DEBUG_TENNIS, "TD_GAME_WON_DSTAR speech played ", texVoice)
		PLAY_TENNIS_AMBIENT_SPEECH(thisPlayer, "TENNIS_WIN", texVoice)
		CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_GAME_WON_DSTAR)
	ENDIF		
	IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_GAME_LOST_DSTAR)
	AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_PAUSE_DIALOGUE)
		CDEBUG2LN(DEBUG_TENNIS, "TD_GAME_LOST_DSTAR speech played on ", texVoice)
		PLAY_TENNIS_AMBIENT_SPEECH(thisPlayer, "TENNIS_LOSE", texVoice)
		CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_GAME_LOST_DSTAR)
	ENDIF	
	IF IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_AMANDA_SERVE_TO_MICHAEL) AND NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
		CLEAR_TENNIS_CONVO_FLAG(thisUIData, TCVF_AMANDA_SERVE_TO_MICHAEL)
		sConvo = "MGTN_AM_S"
		CREATE_CONVERSATION(thisUIData.myDialogueStruct, thisUIData.sSubtitleGroupID, sConvo, CONV_PRIORITY_HIGH)
		CPRINTLN(DEBUG_TENNIS, "TCVF_AMANDA_SERVE_TO_MICHAEL sConvo ", sConvo, " played")
	ENDIF
	IF IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_AMANDA_SERVE_TO_TREVOR) AND NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
		CLEAR_TENNIS_CONVO_FLAG(thisUIData, TCVF_AMANDA_SERVE_TO_TREVOR)
		sConvo = "MGTN_AT_S"
		CREATE_CONVERSATION(thisUIData.myDialogueStruct, thisUIData.sSubtitleGroupID, sConvo, CONV_PRIORITY_HIGH)
		CPRINTLN(DEBUG_TENNIS, "TCVF_AMANDA_SERVE_TO_TREVOR sConvo ", sConvo, " played")
	ENDIF
		
	HANDLE_TENNIS_MID_MATCH_DIALOGUE(thisProps, thisUIData, thisStatus, eSelfID, eOtherID)
ENDPROC

PROC PROCESS_CAMERA_STATE(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eCourtSide, TENNIS_PLAYER_ID eHitLast)
	SWITCH GET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras)	
		CASE TENNIS_CAMERA_OPTION
		BREAK
		
		CASE TENNIS_CAMERA_FAR
			UPDATE_BASELINE_CAM_TRACKING(thisProps, thisUIData, eSelfID, thisUIData.tennisCameras.tennisCamFar, thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisCourt.vCourtCorners[1], thisProps.tennisCourt.vCourtCorners[3])
			
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_SELECT)
			AND NOT IS_CAM_INTERPOLATING(thisUIData.tennisCameras.tennisCamDyna)
			AND NOT IS_CAM_INTERPOLATING(thisUIData.tennisCameras.tennisCamFar)
			//AND GET_CLOSEST_COURT(thisProps.thisLocation) <> ALOC_tennis_michaelHouse
				SET_TENNIS_SAVED_GLOBAL_FLAG(g_savedGlobals.sTennisData, TGF_DEFAULT_CAM_DYNA)
				SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_DYNAMIC)
				CPRINTLN(DEBUG_TENNIS, "Changing to Dynamic Tennis Camera")
			ELIF NOT IS_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamFar) 
			OR NOT IS_CAM_RENDERING(thisUIData.tennisCameras.tennisCamFar)					
				SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamFar, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_TENNIS_CASCADE_SHADOWS_DEPTH_MODE(FALSE)
				CDEBUG3LN(DEBUG_TENNIS, "Setting Far Cam Active")
			ENDIF
//			CPRINTLN(DEBUG_TENNIS, "TENNIS_CAMERA_FAR")
		BREAK
		CASE TENNIS_CAMERA_FAR_OTHER_SIDE
			UPDATE_BASELINE_CAM_TRACKING(thisProps, thisUIData, eSelfID, thisUIData.tennisCameras.tennisCamFarOtherSide, thisProps.tennisCourt.vCourtCorners[2], thisProps.tennisCourt.vCourtCorners[3], thisProps.tennisCourt.vCourtCorners[1])
			
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_SELECT)
			AND NOT IS_CAM_INTERPOLATING(thisUIData.tennisCameras.tennisCamTut)
			AND NOT IS_CAM_INTERPOLATING(thisUIData.tennisCameras.tennisCamFarOtherSide)
				SET_TENNIS_SAVED_GLOBAL_FLAG(g_savedGlobals.sTennisData, TGF_DEFAULT_CAM_DYNA)
				SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_DYNAMIC)
			ELIF NOT IS_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamFarOtherSide) 
			OR NOT IS_CAM_RENDERING(thisUIData.tennisCameras.tennisCamFarOtherSide)								
				SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamFarOtherSide, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_TENNIS_CASCADE_SHADOWS_DEPTH_MODE(FALSE)
			ENDIF
//			CPRINTLN(DEBUG_TENNIS, "TENNIS_CAMERA_FAR_OTHER_SIDE")	
		BREAK
		CASE TENNIS_CAMERA_DYNAMIC
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_NO_DYNA_CAM)
				//Used for offsets and values for dynamic camera
				FLOAT fDist
				INT iAnchor
				VECTOR vTarget
				
				IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
					fDist = VDIST(GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID])), thisProps.tennisCourt.vCenterCourt)
				ENDIF
				// Anchor corner, depends on courtside
				iAnchor = PICK_INT(eCourtSide = TENNIS_PLAYER_AWAY, 0, 2)
				// BOOL to tell if a lob is incoming, we zoom out if it is
				BOOL bIsLobIncoming
				bIsLobIncoming = (eHitLast <> eSelfID) AND (thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)].ShotType = SHOT_LOB)	// OR bBehindAttacker)
				// Get the target for the left/right swing
				IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
					vTarget = GET_ENTITY_COORDS(thisProps.tennisBall.oBall)
				ELSE	// in case the ball doesn't exist yet.
					vTarget = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
				ENDIF
				// Adjust the dyna-cam
				ADJUST_TENNIS_CAMERA_DYNAMIC(thisUIData.tennisCameras.tennisCamDyna,	// Update the dynamic camera
											GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt),
											thisProps.tennisCourt.vCourtCorners[iAnchor],
											thisProps.tennisPlayers[eSelfID].vMyRight,
											thisProps.tennisPlayers[eSelfID].vMyForward,
											vTarget - thisProps.tennisCourt.vCenterCourt,
											thisUIData.tennisCameras.fDynaCamSwing, fDist,
											thisUIData.tennisCameras.fDynaDolly,
											thisUIData.tennisCameras.fDynaNudgeRSLR, thisUIData.tennisCameras.fDynaNudgeRSUD,
											bIsLobIncoming #IF IS_DEBUG_BUILD , USE_DYNA_DEFAULTS #ENDIF)
				#IF IS_DEBUG_BUILD
				IF DISPLAY_DYNA_CAM_VALUES
					VECTOR vDraw, vAdd
					vDraw = << 0.75, 0.2, 0.0 >>
					vAdd = << 0.0, 0.0125, 0.0 >>
					TEXT_LABEL_63 texDisplay
					
					texDisplay = GET_STRING_FROM_TENNIS_PLAYING_COURT( GET_TENNIS_PLAYING_COURT( thisProps.tennisCourt ) )
					DRAW_DEBUG_TEXT_2D( texDisplay, vDraw )
					vDraw += vAdd
					
					texDisplay = "vCourtCorners["
					texDisplay += iAnchor
					texDisplay += "]"
					DRAW_DEBUG_TEXT_2D( texDisplay, vDraw )
					vDraw += vAdd
					
					texDisplay = "bIsLobIncoming="
					texDisplay += PICK_STRING( bIsLobIncoming, "TRUE", "FALSE" )
					DRAW_DEBUG_TEXT_2D( texDisplay, vDraw )
					vDraw += vAdd
					
					texDisplay = "eSelfID="
					texDisplay += ENUM_TO_INT( eSelfID )
					DRAW_DEBUG_TEXT_2D( texDisplay, vDraw )
					vDraw += vAdd
					
					texDisplay = "centerToBall (in blue)="
					texDisplay += GET_STRING_FROM_VECTOR( vTarget - thisProps.tennisCourt.vCenterCourt )
					DRAW_DEBUG_TEXT_2D( texDisplay, vDraw )
					DRAW_DEBUG_LINE( vTarget, thisProps.tennisCourt.vCenterCourt )
					vDraw += vAdd
					
					texDisplay = "eCourtSide="
					texDisplay += GET_STRING_FROM_TENNIS_PLAYER_ID( eCourtSide )
					DRAW_DEBUG_TEXT_2D( texDisplay, vDraw )
					vDraw += vAdd
				ENDIF
				#ENDIF
			ELSE
				CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_NO_DYNA_CAM)
			ENDIF
										
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_SELECT)
			AND NOT IS_CAM_INTERPOLATING(thisUIData.tennisCameras.tennisCamFar)
			AND NOT IS_CAM_INTERPOLATING(thisUIData.tennisCameras.tennisCamDyna)
				CLEAR_TENNIS_SAVED_GLOBAL_FLAG(g_savedGlobals.sTennisData, TGF_DEFAULT_CAM_DYNA)
				IF eCourtSide = (TENNIS_PLAYER_AWAY)
					SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_FAR)
					CPRINTLN(DEBUG_TENNIS, "Changing to TENNIS_CAMERA_FAR")
				ELSE
					SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_FAR_OTHER_SIDE)
					CPRINTLN(DEBUG_TENNIS, "Changing to TENNIS_CAMERA_FAR_OTHER_SIDE")
				ENDIF
			ELIF NOT IS_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamDyna) 
			OR NOT IS_CAM_RENDERING(thisUIData.tennisCameras.tennisCamDyna)	
				CDEBUG3LN(DEBUG_TENNIS, "PROCESS_CAMERA_STATE :: Setting Dynacam active and rendering script cams")
				SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamDyna, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_TENNIS_CASCADE_SHADOWS_DEPTH_MODE(FALSE)
			ENDIF	
//			CPRINTLN(DEBUG_TENNIS, "TENNIS_CAMERA_DYNAMIC")	
		BREAK
		CASE TENNIS_CAMERA_QUICK_CUT
			SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut, TRUE)
			RENDER_SCRIPT_CAMS(TRUE,FALSE)
			//("QUICK CUT")
			CPRINTLN(DEBUG_TENNIS, "TENNIS_CAMERA_QUICK_CUT")	
		BREAK
		CASE TENNIS_CAMERA_MP_CHEAT
			SET_TENNIS_CAMERA_MP_CHEAT(thisUIData.tennisCameras, thisProps.vForward, thisProps.tennisCourt.vCenterCourt)
			IF NOT IS_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut) 
			OR NOT IS_CAM_RENDERING(thisUIData.tennisCameras.tennisCamTut)								
				SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			CPRINTLN(DEBUG_TENNIS, "TENNIS_CAMERA_MP_CHEAT")
		BREAK
		CASE TENNIS_CAMERA_SIDE_CHANGE
			IF NOT IS_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut) 
			OR NOT IS_CAM_RENDERING(thisUIData.tennisCameras.tennisCamTut)								
				SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			IF GET_TIMER_IN_SECONDS(thisUIData.tennisCameras.tennisCamTimer) * 1000 > SIDE_CHANGE_LENGTH
				SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, thisUIData.tennisCameras.eChosenCam, FALSE)
			ENDIF
			CPRINTLN(DEBUG_TENNIS, "TENNIS_CAMERA_SIDE_CHANGE")
		BREAK
		DEFAULT
			SCRIPT_ASSERT(" Default Camera Case, should not hit, contact Rob Pearsall")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    playerScores - []
/// RETURNS:
///    
FUNC BOOL VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(TENNIS_SCORES &playerScores[], INT iCurrentSet, BOOL bIsRematch)
	IF bIsRematch
		RETURN FALSE
	ENDIF
	IF playerScores[0].iPointsWon + playerScores[1].iPointsWon >= 2
		RETURN FALSE
	ENDIF
	IF playerScores[0].iGamesWon[iCurrentSet] > 0
		RETURN FALSE
	ENDIF
	IF playerScores[1].iGamesWon[iCurrentSet] > 0
		RETURN FALSE
	ENDIF
	IF playerScores[0].iSetsWon > 0
		RETURN FALSE
	ENDIF
	IF playerScores[1].iSetsWon > 0
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC PROCESS_OPTION_STATE(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATUS &thisStatus, TENNIS_GAME_SP_DATA &thisSPData, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
	STRING sConvo
	
	BOOL bCursorAccept = FALSE
	INT iCursorValueChange = 0
	INT iTempCursorChange = 0
	
	BOOL bSelectDifficulty = (thisProps.eTennisActivity <> TA_FRIEND_ACTIVITY AND NOT GET_TENNIS_STATUS_REMATCH(thisStatus) AND NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP))
	
	SWITCH GET_TENNIS_UI_STATE(thisUIData)
		CASE TENNIS_UI_SET_INIT
			BUILD_TENNIS_INTRO_MENU(thisUIData, bSelectDifficulty)
			SET_CURSOR_POSITION_FOR_MENU()
			
			SWITCH thisUIData.playerChars[eOtherID]
				CASE CHAR_MICHAEL		sConvo = PICK_STRING(GET_TENNIS_STATUS_REMATCH(thisStatus), "MGTN_remaQM", "MGTN_introQM")		BREAK
//				CASE CHAR_FRANKLIN		sConvo = PICK_STRING(GET_TENNIS_STATUS_REMATCH(thisStatus), "MGTN_remaQF", "MGTN_setsF")		BREAK
				CASE CHAR_TREVOR		sConvo = PICK_STRING(GET_TENNIS_STATUS_REMATCH(thisStatus), "MGTN_remaQT", "MGTN_introQT")		BREAK
				CASE CHAR_LAMAR			sConvo = PICK_STRING(GET_TENNIS_STATUS_REMATCH(thisStatus), "MGTN_remaQL", "MGTN_setsL")		BREAK
				CASE CHAR_AMANDA		sConvo = PICK_STRING(GET_TENNIS_STATUS_REMATCH(thisStatus), "MGTN_remaQA", "MGTN_setsA")		BREAK
				CASE CHAR_JIMMY			sConvo = PICK_STRING(GET_TENNIS_STATUS_REMATCH(thisStatus), "MGTN_remaQJ", "MGTN_introQJ")		BREAK	// Using intro instead of sets because we didn't record a line
				DEFAULT
					IF ARE_STRINGS_EQUAL(thisUIData.playerNames[eOtherID], "OPP_NAME_1")
						sConvo = PICK_STRING(GET_TENNIS_STATUS_REMATCH(thisStatus), "MGTN_remtch1", "MGTN_sets1p")
					ELIF ARE_STRINGS_EQUAL(thisUIData.playerNames[eOtherID], "OPP_NAME_2")
						sConvo = PICK_STRING(GET_TENNIS_STATUS_REMATCH(thisStatus), "MGTN_rematch", "MGTN_setsOp")
					ELIF ARE_STRINGS_EQUAL(thisUIData.playerNames[eOtherID], "OPP_NAME_3")
						sConvo = PICK_STRING(GET_TENNIS_STATUS_REMATCH(thisStatus), "MGTN_remtch3", "MGTN_sets3p")
					ELIF ARE_STRINGS_EQUAL(thisUIData.playerNames[eOtherID], "OPP_NAME_4")
						sConvo = PICK_STRING(GET_TENNIS_STATUS_REMATCH(thisStatus), "MGTN_remtch4", "MGTN_sets4p")
					ENDIF
				BREAK
			ENDSWITCH
			
			IF IS_SAFE_TO_START_CONVERSATION() AND NOT IS_STRING_NULL(sConvo)
				CREATE_CONVERSATION(thisUIData.myDialogueStruct, thisUIData.sSubtitleGroupID, sConvo, CONV_PRIORITY_LOW)
				CPRINTLN(DEBUG_TENNIS, "sConvo played is: ", sConvo)
			ENDIF
			
			SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_SET_SELECT)
		BREAK
		
		CASE TENNIS_UI_SET_SELECT
		
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	// B*2239651 - Disabling pause_alternate during end screens (to avoid clash when exiting leaderboards)

			// PC mouse support for menu
			IF IS_PC_VERSION()
				// PC MOUSE SUPPORT
				HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
				
				HANDLE_MENU_CURSOR(FALSE)
				
				// Special case cursor handling for this menu
//				IF g_iMenuCursorItem = thisUIData.iCurrentOption
//					IF thisUIData.iCurrentOption > 1
//						SET_MOUSE_CURSOR_STYLE(	MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER )
//					ENDIF
//				ENDIF
			

				
				IF IS_MENU_CURSOR_ACCEPT_PRESSED()
						
					IF g_iMenuCursorItem = thisUIData.iCurrentOption
					
						IF thisUIData.iCurrentOption = 0 OR thisUIData.iCurrentOption = 1
							iCursorValueChange = GET_CURSOR_MENU_ITEM_VALUE_CHANGE()
						ELSE
							bCursorAccept = TRUE
						ENDIF
						
					ELSE
						thisUIData.iCurrentOption = g_iMenuCursorItem	
						PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
					ENDIF
					
					BUILD_TENNIS_INTRO_MENU(thisUIData, bSelectDifficulty)
					g_iMenuCursorHoldTime = GET_GAME_TIMER() + MENU_CURSOR_ARROW_SCROLL_TIME
				ELSE
					IF IS_CURSOR_ACCEPT_HELD() AND (GET_GAME_TIMER() > g_iMenuCursorHoldTime)
						IF g_iMenuCursorItem = thisUIData.iCurrentOption
						
							IF thisUIData.iCurrentOption = 0 OR thisUIData.iCurrentOption = 1
								iTempCursorChange = GET_CURSOR_MENU_ITEM_VALUE_CHANGE()
								IF (iTempCursorChange = -1) OR (iTempCursorChange = 1)
									iCursorValueChange = iTempCursorChange									
								ENDIF	
							ENDIF
						ENDIF
						
						g_iMenuCursorHoldTime = GET_GAME_TIMER() + MENU_CURSOR_ARROW_SCROLL_TIME
					ENDIF
				ENDIF

			ENDIF
	
		
//			#IF IS_DEBUG_BUILD
//			BUILD_TENNIS_INTRO_MENU(thisUIData, bSelectDifficulty)
//			#ENDIF
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SCLB)
				IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) OR IS_ANALOG_JUST_PRESSED_DOWN(thisUIData.tennisUI, DEAD_ZONE_THRESHOLD))
				OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND GET_GAME_TIMER() > GET_TENNIS_UI_MENU_STAMP(thisUIData.tennisUI))
					thisUIData.iCurrentOption = PICK_INT(thisUIData.iCurrentOption < 2, thisUIData.iCurrentOption + 1, 0)
					IF (IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP) OR GET_TENNIS_STATUS_REMATCH(thisStatus) OR thisProps.eTennisActivity = TA_FRIEND_ACTIVITY) AND thisUIData.iCurrentOption = 1
						thisUIData.iCurrentOption = 2
					ENDIF
					BUILD_TENNIS_INTRO_MENU(thisUIData, bSelectDifficulty)
					SET_TENNIS_UI_MENU_STAMP(thisUIData.tennisUI, GET_GAME_TIMER() + MENU_STAMP_DELAY)
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
				ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) OR IS_ANALOG_JUST_PRESSED_UP(thisUIData.tennisUI, -DEAD_ZONE_THRESHOLD))
				OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND GET_GAME_TIMER() > GET_TENNIS_UI_MENU_STAMP(thisUIData.tennisUI))
					thisUIData.iCurrentOption = PICK_INT(thisUIData.iCurrentOption > 0, thisUIData.iCurrentOption - 1, 2)
					IF (IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP) OR GET_TENNIS_STATUS_REMATCH(thisStatus) OR thisProps.eTennisActivity = TA_FRIEND_ACTIVITY) AND thisUIData.iCurrentOption = 1
						thisUIData.iCurrentOption = 0
					ENDIF
					BUILD_TENNIS_INTRO_MENU(thisUIData, bSelectDifficulty)
					SET_TENNIS_UI_MENU_STAMP(thisUIData.tennisUI, GET_GAME_TIMER() + MENU_STAMP_DELAY)
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
				ENDIF
				
				IF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT) OR IS_ANALOG_JUST_PRESSED_RIGHT(thisUIData.tennisUI, DEAD_ZONE_THRESHOLD))
				OR (IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT))
				OR (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT) AND GET_GAME_TIMER() > GET_TENNIS_UI_MENU_STAMP(thisUIData.tennisUI))
				OR iCursorValueChange = 1
					IF thisUIData.iCurrentOption = 0
						thisUIData.iRulesSelection = PICK_INT(thisUIData.iRulesSelection < 5, thisUIData.iRulesSelection + 1, 0)
					ELIF thisUIData.iCurrentOption = 1
						thisUIData.iDiffSelection = PICK_INT(thisUIData.iDiffSelection < 2, thisUIData.iDiffSelection + 1, 0)
					ENDIF
					BUILD_TENNIS_INTRO_MENU(thisUIData, bSelectDifficulty)
					SET_TENNIS_UI_MENU_STAMP(thisUIData.tennisUI, GET_GAME_TIMER() + MENU_STAMP_DELAY)
					IF thisUIData.iCurrentOption <> 2
						PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
					ENDIF
					iCursorValueChange = 0
				ELIF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT) OR IS_ANALOG_JUST_PRESSED_LEFT(thisUIData.tennisUI, -DEAD_ZONE_THRESHOLD))
				OR (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT) AND GET_GAME_TIMER() > GET_TENNIS_UI_MENU_STAMP(thisUIData.tennisUI))
				OR iCursorValueChange = -1
					IF thisUIData.iCurrentOption = 0
						thisUIData.iRulesSelection = PICK_INT(thisUIData.iRulesSelection > 0, thisUIData.iRulesSelection - 1, 5)
					ELIF thisUIData.iCurrentOption = 1
						thisUIData.iDiffSelection = PICK_INT(thisUIData.iDiffSelection > 0, thisUIData.iDiffSelection - 1, 2)
					ENDIF
					BUILD_TENNIS_INTRO_MENU(thisUIData, bSelectDifficulty)
					SET_TENNIS_UI_MENU_STAMP(thisUIData.tennisUI, GET_GAME_TIMER() + MENU_STAMP_DELAY)
					IF thisUIData.iCurrentOption <> 2
						PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
					ENDIF
					iCursorValueChange = 0
				ENDIF
				
				IF (IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT))
				OR bCursorAccept = TRUE
					IF thisUIData.iCurrentOption = 2
						CPRINTLN(DEBUG_TENNIS, "PROCESS_OPTION_STATE :: Finalizing game options and starting Tennis")
						SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_STR_INFO)
						IF thisUIData.iRulesSelection = 0
							SET_TENNIS_STATUS_NUMBER_OF_SETS(thisStatus, 1)
							SET_TENNIS_MATCH_RULESET(thisStatus, TMR_ONE_GAME)
						ELIF thisUIData.iRulesSelection = 1
							SET_TENNIS_STATUS_NUMBER_OF_SETS(thisStatus, 1)
							SET_TENNIS_MATCH_RULESET(thisStatus, TMR_THREE_GAMES)
						ELIF thisUIData.iRulesSelection = 2
							SET_TENNIS_STATUS_NUMBER_OF_SETS(thisStatus, 1)
							SET_TENNIS_MATCH_RULESET(thisStatus, TMR_FIVE_GAMES)
						ELIF thisUIData.iRulesSelection = 3
							SET_TENNIS_STATUS_NUMBER_OF_SETS(thisStatus, 1)
							SET_TENNIS_MATCH_RULESET(thisStatus, TMR_SETS)
						ELIF thisUIData.iRulesSelection = 4
							SET_TENNIS_STATUS_NUMBER_OF_SETS(thisStatus, 3)
							SET_TENNIS_MATCH_RULESET(thisStatus, TMR_SETS)
						ELIF thisUIData.iRulesSelection = 5
							SET_TENNIS_STATUS_NUMBER_OF_SETS(thisStatus, 5)
							SET_TENNIS_MATCH_RULESET(thisStatus, TMR_SETS)
						ELSE
							SCRIPT_ASSERT("Setting Tennis to default TMR_SETS. Contact Rob Pearsall")
							SET_TENNIS_MATCH_RULESET(thisStatus, TMR_SETS)
							SET_TENNIS_STATUS_NUMBER_OF_SETS(thisStatus, 1)
						ENDIF
						
						IF thisProps.eTennisActivity <> TA_FRIEND_ACTIVITY AND NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
							thisSPData.eDifficulty = INT_TO_ENUM(AI_DIFFICULTY, thisUIData.iDiffSelection)
						ELSE
							thisSPData.eDifficulty = CALCULATE_TENNIS_FRIEND_DIFFICULTY(thisUIData, eOtherID)
						ENDIF
						SET_AI_DIFFICULTY(thisProps.tennisPlayers[eSelfID].playerAI, thisSPData.eDifficulty)
						SET_AI_DIFFICULTY(thisProps.tennisPlayers[eOtherID].playerAI, thisSPData.eDifficulty)
						IF thisSPData.eDifficulty = AD_HARD
							SET_TENNIS_PLAYER_STRENGTH_BOOST(thisProps.tennisPlayers[eOtherID], MAX_STRENGTH_BOOST)
						ENDIF
						
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_MINI_GAME_SOUNDSET")
						SETTIMERB(0)
						SETUP_TENNIS_SERVING_CONTROLS(thisUIData.tennisUI, GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_SERVE_TOSS, TRUE, GET_TENNIS_STATUS_NUM_SETS(thisStatus))
						#IF IS_DEBUG_BUILD
							IF GET_COMMANDLINE_PARAM_EXISTS("DR_tennis")
								DEBUG_RECORD_START()
								CDEBUG1LN(DEBUG_TENNIS, "DEBUG_RECORD_START(DR_tennis)")
							ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SCLB)
				IF TENNIS_SHOULD_SCLB_SHOW_PROFILE_BUTTON() AND NOT IS_TENNIS_UI_SECONDARY_FLAG_SET(thisUIData.tennisUI, TUSF_SCLB_PROFILE_BUTTON_SHOWN)
					SET_TENNIS_UI_SECONDARY_FLAG(thisUIData.tennisUI, TUSF_SCLB_PROFILE_BUTTON_SHOWN)
					INIT_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext, FALSE, FALSE, TRUE, TRUE)
					ADD_SIMPLE_USE_CONTEXT_INPUT(thisUIData.tennisUI.useContext, "FE_HLP3",			FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					ADD_SIMPLE_USE_CONTEXT_INPUT(thisUIData.tennisUI.useContext, "SCLB_PROFILE",	FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
				ELIF IS_TENNIS_UI_SECONDARY_FLAG_SET(thisUIData.tennisUI, TUSF_SCLB_PROFILE_BUTTON_SHOWN) AND NOT TENNIS_SHOULD_SCLB_SHOW_PROFILE_BUTTON()
					CLEAR_TENNIS_UI_SECONDARY_FLAG(thisUIData.tennisUI, TUSF_SCLB_PROFILE_BUTTON_SHOWN)
					INIT_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext, FALSE, FALSE, TRUE, TRUE)
					ADD_SIMPLE_USE_CONTEXT_INPUT(thisUIData.tennisUI.useContext, "FE_HLP3",			FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				ENDIF
			ENDIF
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD) OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
				IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SCLB)
					SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SCLB)
					INIT_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext, FALSE, FALSE, TRUE, TRUE)
					ADD_SIMPLE_USE_CONTEXT_INPUT(thisUIData.tennisUI.useContext, "FE_HLP3", FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					IF TENNIS_SHOULD_SCLB_SHOW_PROFILE_BUTTON() AND NOT IS_TENNIS_UI_SECONDARY_FLAG_SET(thisUIData.tennisUI, TUSF_SCLB_PROFILE_BUTTON_SHOWN)
						SET_TENNIS_UI_SECONDARY_FLAG(thisUIData.tennisUI, TUSF_SCLB_PROFILE_BUTTON_SHOWN)
						ADD_SIMPLE_USE_CONTEXT_INPUT(thisUIData.tennisUI.useContext, "SCLB_PROFILE", FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
					ELIF IS_TENNIS_UI_SECONDARY_FLAG_SET(thisUIData.tennisUI, TUSF_SCLB_PROFILE_BUTTON_SHOWN)
						CLEAR_TENNIS_UI_SECONDARY_FLAG(thisUIData.tennisUI, TUSF_SCLB_PROFILE_BUTTON_SHOWN)
					ENDIF
					PLAY_SOUND_FRONTEND(-1, "LEADERBOARD", "HUD_MINI_GAME_SOUNDSET")
				ENDIF
			ENDIF
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
			OR (IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) AND IS_PLAYER_ONLINE())
				PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_MINI_GAME_SOUNDSET")
			ENDIF
			
			IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SCLB)
				IF NOT IS_PLAYER_ONLINE()
					IF DO_SIGNED_OUT_WARNING(thisUIData.iBS)
						CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SCLB)
						BUILD_TENNIS_INTRO_MENU(thisUIData, bSelectDifficulty)
					ENDIF
				ELSE
					DISPLAY_TENNIS_SOCIAL_CLUB_LEADERBOARD(thisUIData.tennisUI.leaderboardUI, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt), FALSE)
					UPDATE_TENNIS_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext)
				ENDIF
			ELSE
				DRAW_TENNIS_INTRO_UI()
			ENDIF
		BREAK
		
		CASE TENNIS_UI_STR_INFO
			IF HAS_TENNIS_CHARACTER_SEEN_TUTORIAL() OR TIMERB() >= HELP_TEXT_TIME
				IF GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus) = eSelfID
					SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_HELP_SERVE)
				ELSE
					SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_AIM)
				ENDIF
				CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_PLAYER_CARD_SHOULD_SHOW)
				SET_TENNIS_CHARACTER_SEEN_TUTORIAL()
				CLEAR_HELP()
			ELSE
				PRINT_HELP("STR_INFO")
			ENDIF
			IF UPDATE_TENNIS_SERVING_CONTROLS(thisUIData.tennisUI, GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_SERVE_TOSS)
				SETUP_TENNIS_SERVING_CONTROLS(
					thisUIData.tennisUI, 
					GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_SERVE_TOSS, 
					VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(thisStatus.playerScores, GET_TENNIS_STATUS_CURRENT_SET(thisStatus), GET_TENNIS_STATUS_REMATCH(thisStatus)), GET_TENNIS_STATUS_NUM_SETS(thisStatus))
			ENDIF
			UPDATE_TENNIS_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext, FALSE)
		BREAK
		CASE TENNIS_UI_HELP_SERVE
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
			AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SIDE_CHANGING)
			AND NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
				IF UPDATE_TENNIS_SERVING_CONTROLS(thisUIData.tennisUI, GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_SERVE_TOSS)
					SETUP_TENNIS_SERVING_CONTROLS(
						thisUIData.tennisUI, 
						GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_SERVE_TOSS, 
						VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(thisStatus.playerScores, GET_TENNIS_STATUS_CURRENT_SET(thisStatus), GET_TENNIS_STATUS_REMATCH(thisStatus)), GET_TENNIS_STATUS_NUM_SETS(thisStatus))
				ENDIF
				UPDATE_TENNIS_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext, FALSE)
				HANDLE_TENNIS_INSTRUCTION_INPUT(thisUIData)	//, thisStatus)
			ENDIF
		BREAK
		CASE TENNIS_UI_PLAYING
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
			AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SIDE_CHANGING)
			AND NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
				BOOL bLobAvailable
				bLobAvailable = IS_TENNIS_PLAYER_IN_SERVICE_BOXES(thisProps.tennisPlayers[eOtherID], thisProps.tennisCourt, thisProps.vRight)
				IF UPDATE_TENNIS_PLAYING_CONTROLS(thisUIData.tennisUI, bLobAvailable)
					SETUP_TENNIS_PLAYING_CONTROLS(thisUIData.tennisUI, bLobAvailable, VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(thisStatus.playerScores, GET_TENNIS_STATUS_CURRENT_SET(thisStatus), GET_TENNIS_STATUS_REMATCH(thisStatus)), GET_TENNIS_STATUS_NUM_SETS(thisStatus))
				ENDIF
				UPDATE_TENNIS_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext, FALSE)
				HANDLE_TENNIS_INSTRUCTION_INPUT(thisUIData)	//, thisStatus)
			ENDIF
		BREAK
		
		CASE TENNIS_UI_SCOREBOARD
			CLEAR_TENNIS_HELP_MESSAGES()		
		BREAK
	ENDSWITCH			
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisProps - 
///    mModel1 - 
///    mModel2 - Amanda Model, she is eSelfID
PROC REQUEST_FAMILY_ASSETS(TENNIS_GAME_PROPERTIES &thisProps, MODEL_NAMES mModel1=A_F_Y_Beach_01, MODEL_NAMES mModel2=S_M_M_MovAlien_01)
	REQUEST_MODEL(PROP_TENNIS_BALL)
	REQUEST_MODEL(mModel1)
	REQUEST_MODEL(mModel2)
	REQUEST_MODEL(PROP_TENNIS_RACK_01B)
	REQUEST_ANIM_DICT("mini@tennis")
	REQUEST_ANIM_DICT("mini@tennis@female")
	REQUEST_ANIM_DICT("weapons@tennis@male")
	thisProps.mTheOpponentModel = mModel1
	thisProps.mThePlayerAIModel = mModel2
ENDPROC

/// PURPOSE:
///    Already deprecated :-/
/// PARAMS:
///    mModel1 - 
///    mModel2 - 
/// RETURNS:
///    
FUNC BOOL HAVE_FAMILY_ASSETS_LOADED(MODEL_NAMES mModel1, MODEL_NAMES mModel2)
	IF NOT HAS_MODEL_LOADED(PROP_TENNIS_BALL)
		PRINTSTRING("Waiting for PROP_TENNIS_BALL")
		PRINTNL()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(mModel1)
		PRINTSTRING("Waiting for mModel1")
		PRINTNL()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(mModel2)
		PRINTSTRING("Waiting for mModel2")
		PRINTNL()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(PROP_TENNIS_RACK_01B)
		PRINTSTRING("Waiting for PROP_TENNIS_RACK_01B")
		PRINTNL()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("mini@tennis")
		PRINTSTRING("Waiting for mini@tennis")
		PRINTNL()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("mini@tennis@female")
		PRINTSTRING("Waiting for mini@tennis@female")
		PRINTNL()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_REQUEST_TENNIS_ASSETS_LOADED(TENNIS_GAME_PROPERTIES &thisProps)
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\Tennis")
		IF thisProps.eTennisActivity <> TA_AI_VS_AI
			CPRINTLN(DEBUG_TENNIS, "Waiting for Script\\Tennis audio")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\TENNIS_VER2_A")
		IF thisProps.eTennisActivity <> TA_AI_VS_AI
			CPRINTLN(DEBUG_TENNIS, "Waiting for Script\\TENNIS_VER2_A audio")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(PROP_TENNIS_BALL)
		IF thisProps.eTennisActivity <> TA_AI_VS_AI
			CPRINTLN(DEBUG_TENNIS, "Waiting for PROP_TENNIS_BALL")
		ENDIF
		RETURN FALSE
	ENDIF
		
	#IF NOT IS_TENNIS_MULTIPLAYER
		IF NOT HAS_MODEL_LOADED(thisProps.mTheOpponentModel)
			IF thisProps.eTennisActivity <> TA_AI_VS_AI
				CPRINTLN(DEBUG_TENNIS, "Waiting for mTheOpponentModel")
			ENDIF
			RETURN FALSE
		ENDIF
		
		IF thisProps.eTennisActivity = TA_AI_VS_AI AND NOT HAS_MODEL_LOADED(thisProps.mThePlayerAIModel)
			IF thisProps.eTennisActivity <> TA_AI_VS_AI
				CPRINTLN(DEBUG_TENNIS, "Waiting for mThePlayerAIModel")
			ENDIF
			RETURN FALSE
		ENDIF
	#ENDIF
	
	IF NOT HAS_MODEL_LOADED(PROP_TENNIS_RACK_01B)
		IF thisProps.eTennisActivity <> TA_AI_VS_AI
			CPRINTLN(DEBUG_TENNIS, "Waiting for PROP_TENNIS_RACK_01B")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("mini@tennis")
		IF thisProps.eTennisActivity <> TA_AI_VS_AI
			CPRINTLN(DEBUG_TENNIS, "Waiting for mini@tennis")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF thisProps.eTennisActivity = TA_AI_VS_AI
		IF NOT HAS_ANIM_DICT_LOADED("mini@tennis@female")
			IF thisProps.eTennisActivity <> TA_AI_VS_AI
				CPRINTLN(DEBUG_TENNIS, "Waiting for mini@tennis@female")
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	#IF NOT IS_TENNIS_MULTIPLAYER
	IF thisProps.eTennisActivity <> TA_AI_VS_AI
		IF thisProps.etennisActivity <> TA_AI_VS_AI AND NOT HAS_MODEL_LOADED(Prop_VB_34_tencrt_lighting)
			CPRINTLN(DEBUG_TENNIS, "Waiting for Prop_VB_34_tencrt_lighting model")
			RETURN FALSE
		ENDIF
		IF thisProps.eTennisActivity <> TA_AI_VS_AI AND NOT HAS_ANIM_DICT_LOADED("mini@triathlon")
			CPRINTLN(DEBUG_TENNIS, "Waiting for mini@triathlon")
		ENDIF
	ENDIF
	#ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("weapons@tennis@male")
		CPRINTLN(DEBUG_TENNIS, "Waiting for weapons@tennis@male")
		RETURN FALSE
	ENDIF
	
	CPRINTLN(DEBUG_TENNIS, "HAVE_REQUEST_TENNIS_ASSETS_LOADED :: Returning TRUE")
		
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_HUMAN_DATA_ASSETS_LOADED(TENNIS_GAME_UI_DATA &thisUIData)
	
	IF NOT LOAD_MENU_ASSETS()
		CPRINTLN(DEBUG_TENNIS, "Waiting for LOAD_MENU_ASSETS")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)						
		CPRINTLN(DEBUG_TENNIS, "Waiting for MINIGAME_TEXT_SLOT")
		RETURN FALSE
	ENDIF
		
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(thisUIData.tennisUI.scorePanelSF)						
		CPRINTLN(DEBUG_TENNIS, "Waiting for scorePanelSF scaleform to load")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(thisUIData.tennisUI.uiShardMS.siMovie)
		CPRINTLN(DEBUG_TENNIS, "Waiting on uiShardMS scaleform to load, should be IS_TENNIS_MULTIPLAYER only!")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPHUD")
		CPRINTLN(DEBUG_TENNIS, "Waiting for MPHUD")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_PTFX_ASSET_LOADED()
		CPRINTLN(DEBUG_TENNIS, "Waiting for HAS_PTFX_ASSET_LOADED")
		RETURN FALSE
	ENDIF
	
	#IF NOT IS_TENNIS_MULTIPLAYER
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(thisUIData.tennisUI.leaderboardUI)
		CPRINTLN(DEBUG_TENNIS, "Waiting on leaderboardUI scaleform to load")
		RETURN FALSE
	ENDIF	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(thisUIData.tennisUI.matchUI.siMovie)
		CPRINTLN(DEBUG_TENNIS, "Waiting on matchUI scaleform to load")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("ShopUI_Title_Tennis")
		CPRINTLN(DEBUG_TENNIS, "Waiting for ShopUI_Title_Tennis")
		RETURN FALSE
	ENDIF
	#ENDIF
	
	CPRINTLN(DEBUG_TENNIS, "HAVE_HUMAN_DATA_ASSETS_LOADED :: Returning TRUE")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL REQUEST_TENNIS_ENDSCREEN(TENNIS_UI & thisUIData)
	thisUIData.thisEsd.splash = NULL
	RETURN TRUE
ENDFUNC

PROC SET_TENNIS_GAME_FIRST_SERVE(TENNIS_GAME_STATUS &thisStatus, TENNIS_PLAYER_ID newServe, INT &iUtilFlags)
	SET_TENNIS_GAME_STATE(thisStatus, TGSE_PREPARE_SERVE)
	SET_TENNIS_SERVE_COUNT(thisStatus, 0)
	SET_TENNIS_SERVING_SIDE(thisStatus, SERVING_FROM_RIGHT)
	SET_BITMASK_AS_ENUM(iUtilFlags, TD_SERVE_DSTAR)
	SET_BITMASK_AS_ENUM(iUtilFlags, TD_FIRST_SERVE)
	thisStatus.eWhoIsServing = newServe
	CPRINTLN(DEBUG_TENNIS, "Making player:", ENUM_TO_INT(newServe)," serve first")
ENDPROC

PROC SET_SINGLE_TENNIS_PLAYER_ANIM_GROUP(TENNIS_PLAYER &thisPlayer, STRING sAnimGroup)
	IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer))
		SET_PED_ANIM_GROUP_STRAFE(GET_TENNIS_PLAYER_INDEX(thisPlayer), sAnimGroup)
	ENDIF
ENDPROC

PROC SET_TENNIS_INTERSTITIAL_FLAGS(TENNIS_PLAYER_ID eScoredLast, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER &tennisPlayers[], INT &iUtilFlags)
	IF eScoredLast = eSelfID
		IF IS_TENNIS_AI_FLAG_SET(tennisPlayers[eSelfID].playerAI, TAF_TASKED_ANIM)
			SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_OPP_SAD)
			CDEBUG2LN(DEBUG_TENNIS, "SET_TENNIS_INTERSTITIAL_FLAGS :: SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_OPP_SAD)")
			SET_BITMASK_AS_ENUM(iUtilFlags, TD_FAULT_DSTAR)
		ELSE
			SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_PLR_HAPPY)
			CDEBUG2LN(DEBUG_TENNIS, "SET_TENNIS_INTERSTITIAL_FLAGS :: SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_PLR_HAPPY)")
			SET_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_W_DSTAR)
		ENDIF
	ELIF eScoredLast <> eSelfID
		IF IS_TENNIS_PLAYER_FLAG_SET(tennisPlayers[eSelfID], TPB_RAGE_ANIM) OR IS_TENNIS_AI_FLAG_SET(tennisPlayers[eScoredLast].playerAI, TAF_TASKED_ANIM)
			SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_PLR_SAD)
		ELIF NOT IS_TENNIS_PLAYER_FLAG_SET(tennisPlayers[eSelfID], TPB_RAGE_ANIM)
			SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_OPP_HAPPY)
		ENDIF
		CDEBUG2LN(DEBUG_TENNIS, "SET_TENNIS_INTERSTITIAL_FLAGS :: eScoredLast <> eSelfID")
		SET_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_L_DSTAR)
	ENDIF
ENDPROC

/// PURPOSE:
///    Used to set up the interstitial cutscene, gets the ped in the right position and sets the camera to the right spot
/// PARAMS:
///    thisProps - 
///    thisUIData - 
///    iPed - use either eSelfID or eOtherID depending on which ped you want to point the camera at
///    bRandomAngle - 
PROC SET_UP_TENNIS_INTERSTITIAL(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID ePed, BOOL bSyncScene)
	INT iOtherPed = 1 - ENUM_TO_INT(ePed)
	
	// Reset the react stamps so we move states on properly
	SET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisProps.tennisPlayers[0], 0)
	SET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisProps.tennisPlayers[1], 0)
	
	SET_TENNIS_CASCADE_SHADOWS_DEPTH_MODE(TRUE)
	
	SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[ePed]), FALSE)
	SET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[ePed], TSS_NO_SWING)
	SET_TENNIS_INTERSTITIAL_PLAYER(thisUIData, ePed)
	SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)]), TRUE)
	
	IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
		DETACH_ENTITY(thisProps.tennisBall.oBall)
		#IF IS_TENNIS_MULTIPLAYER
			SET_ENTITY_COORDS(thisProps.tennisBall.oBall, (<< -1176.0388, -1621.6958, 3.3738 >>))
		#ENDIF
	ENDIF
	
	// Spawn the ball under the court so it's not
	DEBUG_SPAWN_BALL(thisProps)
	
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vCenterCourt, 20.0)
	//CLEAR_AREA(thisProps.tennisCourt.vCenterCourt, 20, TRUE)
	
	CLEAR_HELP()
	
	IF NOT bSyncScene
		VECTOR vNewPos
		vNewPos = thisProps.tennisCourt.vCenterCourt + (thisProps.tennisPlayers[ePed].vMyForward * -5.0)
		IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[ePed]))
			SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[ePed]), vNewPos)
			SET_ENTITY_HEADING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[ePed]), GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[ePed].vMyForward.x, thisProps.tennisPlayers[ePed].vMyForward.y))
		ENDIF
		vNewPos = thisProps.tennisCourt.vCenterCourt + (thisProps.tennisPlayers[iOtherPed].vMyForward * -3.0)
		IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[iOtherPed]))
			SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[iOtherPed]), vNewPos)
		ENDIF
	
		#IF NOT IS_TENNIS_MULTIPLAYER
			FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[ePed]))
		#ENDIF
		
//		IF bRandomAngle
//			vOffset = GET_RANDOM_TENNIS_CAMERA_ANGLE(thisProps.tennisPlayers[ePed], thisUIData.sInterstitialPlrAnim, thisUIData.vInterstitialOffset)
//		ELSE
//			vOffset = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[ePed])) + (thisProps.tennisPlayers[ePed].vMyForward * CAM_DOLLY)
//		ENDIF
			
//		TENNIS_CAMERA_CUT_NOW(thisUIData.tennisCameras, TENNIS_CAMERA_QUICK_CUT, FALSE, thisProps.vForward, thisProps.tennisCourt.vCenterCourt, TRUE)
//		
//		SET_CAM_COORD(thisUIData.tennisCameras.tennisCamTut, vOffset)
//		SET_CAM_FOV(thisUIData.tennisCameras.tennisCamTut, 29.0)
//		thisUIData.tennisCameras.vCamFocusPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[ePed])) + <<0,0,0.3>>
//		POINT_CAM_AT_COORD(thisUIData.tennisCameras.tennisCamTut, thisUIData.tennisCameras.vCamFocusPos)
//		
//		SHAKE_CAM(thisUIData.tennisCameras.tennisCamTut, "HAND_SHAKE", 1.0)
	ENDIF
	
	SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_QUICK_CUT, FALSE)
	RESET_TENNIS_TIMER_AND_INTERSTITIAL_TIME(thisUIData, GET_TENNIS_REACT_ANIM_TIME(thisUIData.sInterstitialPlrAnim))
	
	CPRINTLN(DEBUG_TENNIS, "SET_UP_TENNIS_INTERSTITIAL: Finished")
ENDPROC

FUNC BOOL VALIDATE_RAGE_ANIM_USE(TENNIS_PLAYER &thisPlayer, enumCharacterList eChar, INT iUtilFlags)
	IF eChar <> CHAR_MICHAEL
		RETURN FALSE
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(iUtilFlags, TD_GAME_DONE_STISH)
		RETURN TRUE
	ENDIF
	
	IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_RAGE_ANIM)
		CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_RAGE_ANIM)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC TENNIS_SIDE_CHANGE_MAKE_PLAYER_WALK(TENNIS_COURT &thisCourt, TENNIS_PLAYER &playerTasked, BOOL bIsCamSide = FALSE)
	CPRINTLN(DEBUG_TENNIS, "TENNIS_SIDE_CHANGE_MAKE_PLAYER_WALK called")
	
	// Walk players with sequences in the next shot.
	VECTOR vDest = thisCourt.vNetSideline[playerTasked.eCourtSide] - (playerTasked.vMyRight * 3.0)
	SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(playerTasked), FALSE)
	
	SET_ENTITY_HEADING(GET_TENNIS_PLAYER_INDEX(playerTasked), GET_HEADING_BETWEEN_VECTORS(playerTasked.vPos, vDest))
	
	// If we are on the camera side of the court, we will wait a short period of time before walking the player
	// to provide a bit different behavior from opposing player
	UNUSED_PARAMETER(bIsCamSide)
//	IF bIsCamSide
//		SEQUENCE_INDEX seqIndex
//		OPEN_SEQUENCE_TASK(seqIndex)
//			TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(100, 400))
//			TASK_GO_STRAIGHT_TO_COORD(NULL, vDest, PEDMOVE_WALK)
//		CLOSE_SEQUENCE_TASK(seqIndex)
//		TASK_PERFORM_SEQUENCE(GET_TENNIS_PLAYER_INDEX(playerTasked), seqIndex)
//		CLEAR_SEQUENCE_TASK(seqIndex)
//	ELSE
		TASK_GO_STRAIGHT_TO_COORD(GET_TENNIS_PLAYER_INDEX(playerTasked), vDest, PEDMOVE_WALK)
//	ENDIF
	
	SET_PED_WEAPON_MOVEMENT_CLIPSET(GET_TENNIS_PLAYER_INDEX(playerTasked), "weapons@tennis@male")
	FORCE_PED_MOTION_STATE(GET_TENNIS_PLAYER_INDEX(playerTasked), MS_ON_FOOT_WALK, TRUE)
	SET_PED_MIN_MOVE_BLEND_RATIO(GET_TENNIS_PLAYER_INDEX(playerTasked), PEDMOVE_WALK)
	
	SET_TENNIS_AI_FLAG(playerTasked.playerAI, TAF_JUST_TASKED)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(playerTasked))
ENDPROC

PROC TENNIS_START_OPPONENT_SIDE_CHANGE(TENNIS_COURT &thisCourt, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eOtherID) 
	CDEBUG1LN(DEBUG_TENNIS, "TENNIS_START_OPPONENT_SIDE_CHANGE: Tasking opponent to start walking for side change")
	TENNIS_ACTIVATE_SIDE_CHANGE(thisProps, thisUIData.tennisCameras)
	TENNIS_SIDE_CHANGE_MAKE_PLAYER_WALK(thisCourt, thisProps.tennisPlayers[eOtherID])
	SET_TENNIS_FLOW_FLAG(thisProps, TFF_WALKING_TO_POSITION)
ENDPROC

FUNC BOOL VALIDATE_TENNIS_INTERSTITIAL_ENDING(TENNIS_COURT &thisCourt, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, STRING sAnimDict, TENNIS_PLAYER_ID eOtherID, TENNIS_GAME_STATUS &thisStatus)
	unused_parameter(eOtherID)
	TENNIS_PLAYER_ID eLeadID = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)))
	IF IS_SYNCHRONIZED_SCENE_RUNNING(thisUIData.iSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID) >= 0.98
		CERRORLN(DEBUG_TENNIS, "VALIDATE_TENNIS_INTERSTITIAL_ENDING :: Sync scene is over")
		RETURN TRUE
	ELIF IS_SYNCHRONIZED_SCENE_RUNNING(thisUIData.iSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID) >= 0.9
		IF GET_TENNIS_GAME_STATE(thisStatus) = TGSE_SIDE_CHANGE
			IF NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_CALL_SIDE_CHANGE_ONCE)
				SET_TENNIS_FLOW_FLAG(thisProps, TFF_CALL_SIDE_CHANGE_ONCE)
				CPRINTLN(DEBUG_TENNIS, "VALIDATE_TENNIS_INTERSTITIAL_ENDING: Starting side change for opponent due to sync scene phase")
				TENNIS_START_OPPONENT_SIDE_CHANGE(thisCourt, thisProps, thisUIData, eLeadID)
			ENDIF
		ENDIF
	ENDIF
	
	IF (NOT IS_SYNCHRONIZED_SCENE_RUNNING(thisUIData.iSceneID)) AND GET_TIMER_IN_SECONDS(thisUIData.interstitialTimer) > thisUIData.fInterstitialTime AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		CERRORLN(DEBUG_TENNIS, "VALIDATE_TENNIS_INTERSTITIAL_ENDING :: GET_TIMER_IN_SECONDS(", GET_TIMER_IN_SECONDS(thisUIData.interstitialTimer), ") > ", thisUIData.fInterstitialTime, " AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()")
		RETURN TRUE
	ENDIF
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE) AND GET_TIMER_IN_SECONDS(thisUIData.interstitialTimer) > 0.25
		CPRINTLN(DEBUG_TENNIS, "VALIDATE_TENNIS_INTERSTITIAL_ENDING: Current state is: ", GET_STRING_FROM_TENNIS_GAME_STATE(GET_TENNIS_GAME_STATE(thisStatus)))
		
		IF IS_AMBIENT_SPEECH_PLAYING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)]))
			CDEBUG2LN(DEBUG_TENNIS, "STOP_CURRENT_PLAYING_AMBIENT_SPEECH for ", thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)].playerAI.texName)
			STOP_CURRENT_PLAYING_AMBIENT_SPEECH(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)]))
		ENDIF
		// If we are checking for button press on the side change interstitial, add a delay so that we can
		// start the opponent's walk before cutting to the side change camera. If we are not doing a side
		// change just return true for button press
		IF GET_TENNIS_GAME_STATE(thisStatus) = TGSE_SIDE_CHANGE
			IF NOT IS_TIMER_STARTED(m_timerSkip)
				IF NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_CALL_SIDE_CHANGE_ONCE)
					SET_TENNIS_FLOW_FLAG(thisProps, TFF_CALL_SIDE_CHANGE_ONCE)
					CPRINTLN(DEBUG_TENNIS, "VALIDATE_TENNIS_INTERSTITIAL_ENDING: Starting side change for opponent due to player skip")
					TENNIS_START_OPPONENT_SIDE_CHANGE(thisCourt, thisProps, thisUIData, eLeadID)
				ENDIF
				
				START_TIMER_NOW(m_timerSkip)
			ENDIF
		ELSE
			CERRORLN(DEBUG_TENNIS, "VALIDATE_TENNIS_INTERSTITIAL_ENDING :: INPUT_FRONTEND_ACCEPT AND GET_TIMER_IN_SECONDS(", GET_TIMER_IN_SECONDS(thisUIData.interstitialTimer), ") > 0.5")
			RETURN TRUE
		ENDIF
		
	ENDIF
	IF GET_TIMER_IN_SECONDS(thisUIData.interstitialTimer) > CUT_TIMEOUT
		CERRORLN(DEBUG_TENNIS, "VALIDATE_TENNIS_INTERSTITIAL_ENDING :: GET_TIMER_IN_SECONDS(", GET_TIMER_IN_SECONDS(thisUIData.interstitialTimer), ") > CUT_TIMEOUT")
		RETURN TRUE
	ENDIF
	IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)])) AND NOT IS_STRING_NULL(thisUIData.sInterstitialPlrAnim)
		AND HAS_ENTITY_ANIM_FINISHED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)]), sAnimDict, thisUIData.sInterstitialPlrAnim)
		CERRORLN(DEBUG_TENNIS, "VALIDATE_TENNIS_INTERSTITIAL_ENDING :: HAS_ENTITY_ANIM_FINISHED ", thisUIData.sInterstitialPlrAnim)
		RETURN TRUE
	ENDIF
	
	// The timer will only be started upon player skip intention. If we've reached the
	// allotted delay the interstitial can now be considered over
	IF IS_TIMER_STARTED(m_timerSkip)
		IF GET_TIMER_IN_SECONDS(m_timerSkip) > SKIP_INTER_DELAY
			CANCEL_TIMER(m_timerSkip)
			CERRORLN(DEBUG_TENNIS, "VALIDATE_TENNIS_INTERSTITIAL_ENDING :: INPUT_FRONTEND_ACCEPT AND GET_TIMER_IN_SECONDS(", GET_TIMER_IN_SECONDS(thisUIData.interstitialTimer), ") > 0.25")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisProps - 
///    thisUIData - 
///    eSelfID - 
///    bEndInterstitial - Only used in MP when determing whether to move on the state or not.
PROC HANDLE_TENNIS_INTERSTITIALS(TENNIS_COURT &thisCourt, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_GAME_STATUS &thisStatus, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
	TENNIS_INTERSTITIAL_ACTOR eActor
	//VECTOR vNewPlayerPos
	BOOL bStartInterstitial = FALSE, bHappy
	STRING sAnimDict = PICK_STRING(IS_TENNIS_PED_MALE((thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)])), "mini@tennis", "mini@tennis@female")
	
	//Check if the interstitial is done or if we want to skip it
	IF VALIDATE_TENNIS_INTERSTITIAL_ENDING(thisCourt, thisProps, thisUIData, sAnimDict, eOtherId, thisStatus)
		IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
			CPRINTLN(DEBUG_TENNIS, "Interstitial came to an end, going back to previous camera")
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PLR_HAPPY)
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PLR_SAD)
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_OPP_HAPPY)
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_OPP_SAD)
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_GAME_DONE_STISH)
			CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_PLAYER_CARD_SHOULD_SHOW)
			CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_NEW_RALLY)
			CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[eOtherId].playerAI, TAF_TASKED_NEW_RALLY)
			SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)]), FALSE)
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				CDEBUG2LN(DEBUG_TENNIS, "HANDLE_TENNIS_INTERSTITIALS :: NEW_LOAD_SCENE_STOP()")
				NEW_LOAD_SCENE_STOP()
			ENDIF
			#IF NOT IS_TENNIS_MULTIPLAYER
				SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, thisUIData.tennisCameras.eChosenCam, FALSE)
			#ENDIF
			IF GET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras) = TENNIS_CAMERA_DYNAMIC
				//Prime the dynamic camera
				VECTOR vBallWillBe
				TENNIS_PLAYER_ID eServer = GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)
				TENNIS_PLAYER_ID eCourtSide = thisProps.tennisPlayers[eServer].eCourtSide
				SERVING_SIDE_ENUM eServingSide = GET_TENNIS_SERVING_SIDE(thisStatus)
				IF eCourtSide = TENNIS_PLAYER_AWAY
					vBallWillBe = PICK_VECTOR(eServingSide = SERVING_FROM_RIGHT, thisProps.tennisCourt.vCourtCorners[1], thisProps.tennisCourt.vCourtCorners[0])
				ELSE
					vBallWillBe = PICK_VECTOR(eServingSide = SERVING_FROM_RIGHT, thisProps.tennisCourt.vCourtCorners[3], thisProps.tennisCourt.vCourtCorners[2])
				ENDIF
				ADJUST_TENNIS_CAMERA_DYNAMIC(thisUIData.tennisCameras.tennisCamDyna,	// Prime the dynamic camera after an interstitial
											 GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt),
											 PICK_VECTOR(thisProps.tennisPlayers[eSelfID].eCourtSide = (TENNIS_PLAYER_AWAY), thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisCourt.vCourtCorners[2]),
											 thisProps.tennisPlayers[eSelfID].vMyRight,
											 thisProps.tennisPlayers[eSelfID].vMyForward,
											 vBallWillBe - thisProps.tennisCourt.vCenterCourt,
											 thisUIData.tennisCameras.fDynaCamSwing, DEFAULT_DYNA_DIST,
											 thisUIData.tennisCameras.fDynaDolly,
											 thisUIData.tennisCameras.fDynaNudgeRSLR, thisUIData.tennisCameras.fDynaNudgeRSUD, FALSE, TRUE)
				SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_NO_DYNA_CAM)
			ENDIF
			
			RESET_TENNIS_TIMER_AND_INTERSTITIAL_TIME(thisUIData, 0)
			TENNIS_PAUSE_TIMER(thisUIData.interstitialTimer)
			
			thisUIData.sInterstitialPlrAnim = ""
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			thisUIData.iSceneID = -1
			
			IF GET_TENNIS_GAME_STATE(thisStatus) != TGSE_SIDE_CHANGE
				SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), TRUE)
				SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), TRUE)
			
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					DISPLAY_TENNIS_SPLASH_UI(thisUIData.tennisUI, FALSE)
					CPRINTLN(DEBUG_TENNIS, "TUF_NO_SPLASH turned on")
				ENDIF
			ENDIF
		ENDIF
		
//	ELIF NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_STOP_INTERSTITIAL_CAM_POINTING)
//		IF NOT IS_ENTITY_DEAD(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)]))
//			vNewPlayerPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)]))
//			vNewPlayerPos.z += 0.3
//			vNewPlayerPos += thisUIData.vInterstitialOffset
//			thisUIData.vInterstitialOffset = LERP_VECTOR(thisUIData.vInterstitialOffset, (<<0,0,0>>), INTERSTITIAL_FORWARD_LERP)
//		ENDIF
//		thisUIData.tennisCameras.vCamFocusPos = LERP_VECTOR(thisUIData.tennisCameras.vCamFocusPos, vNewPlayerPos, INTERSTITIAL_LERP_RATE)
//		POINT_CAM_AT_COORD(thisUIData.tennisCameras.tennisCamTut, thisUIData.tennisCameras.vCamFocusPos)
	ELSE
	
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
	ENDIF
	
	//Below is a little ugly, I'm not gonna lie.
	//Based off the bitmasks we choose an animation, a camera angle, and which player to focus on.	12/17, this just less easy with B*983886, added Amanda dialogue
	//PLR_HAPPY and PLR_SAD are easy, we focus on the player, if the game is over we use a standard camera angle.
	//OPP_HAPPY and OPP_SAD are more complicated. If the opponent is happy and the game is over then the opponent won, we focus on the player being sad.
												//If the opponent is sad and the game is over then they lost, we focus on the player being happy about it.
												//If the opponent is happy and the game isn't over then we focus on the opponent being happy.
												//If the opponent is sad and the game isn't over we focus on the opponent being sad.
	IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_PLR_HAPPY)
		thisUIData.sInterstitialFaceDict = GET_TENNIS_FACIAL_DICT(TRUE, TRUE)
		eActor = TENNIS_ACTOR_PLAYER
		GET_TENNIS_INTERSTITIAL_ANIMATIONS(thisUIData, eActor, TRUE)
		IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
			SET_UP_TENNIS_INTERSTITIAL(thisProps, thisUIData, eSelfID, TRUE)
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PLR_HAPPY)
			bHappy = TRUE
			bStartInterstitial = TRUE
		ENDIF
	ELIF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_PLR_SAD)
		thisUIData.sInterstitialFaceDict = GET_TENNIS_FACIAL_DICT(TRUE, FALSE)
		eActor = TENNIS_ACTOR_PLAYER
		GET_TENNIS_INTERSTITIAL_ANIMATIONS(thisUIData, eActor, FALSE)
		IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
			SET_UP_TENNIS_INTERSTITIAL(thisProps, thisUIData, eSelfID, TRUE)
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PLR_SAD)
			bHappy = FALSE
			bStartInterstitial = TRUE
		ENDIF
	ELIF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_OPP_HAPPY)
		BOOL bAmandaWinAfterDeuce = thisUIData.playerChars[eSelfID] = CHAR_MICHAEL AND thisUIData.playerChars[eOtherID] = CHAR_AMANDA AND GET_TENNIS_DEUCE_COUNT(thisUIData) >= AMANDA_DSTAR_DEUCE_COUNT
		TENNIS_PLAYER_ID ePlayer = INT_TO_ENUM(TENNIS_PLAYER_ID, PICK_INT(IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_GAME_DONE_STISH) AND NOT bAmandaWinAfterDeuce, ENUM_TO_INT(eSelfID), ENUM_TO_INT(eOtherID)))
		IF ePlayer = eSelfID
			eActor = TENNIS_ACTOR_PLAYER
		ELIF IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherID])
			eActor = TENNIS_ACTOR_GENERIC_MALE
		ELSE
			eActor = TENNIS_ACTOR_GENERIC_FEMALE
		ENDIF
		bHappy = (bAmandaWinAfterDeuce OR NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_GAME_DONE_STISH))
		thisUIData.sInterstitialFaceDict = GET_TENNIS_FACIAL_DICT(ePlayer = eSelfID, bHappy, eActor = TENNIS_ACTOR_GENERIC_FEMALE)
		GET_TENNIS_INTERSTITIAL_ANIMATIONS(thisUIData, eActor, bHappy)
		IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
			SET_UP_TENNIS_INTERSTITIAL(thisProps, thisUIData, ePlayer, eActor = TENNIS_ACTOR_PLAYER)
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_OPP_HAPPY)
			bStartInterstitial = TRUE
		ENDIF
	ELIF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_OPP_SAD)
		TENNIS_PLAYER_ID ePlayer = INT_TO_ENUM(TENNIS_PLAYER_ID, PICK_INT(IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_GAME_DONE_STISH), ENUM_TO_INT(eSelfID), ENUM_TO_INT(eOtherID)))
		IF ePlayer = eSelfID
			eActor = TENNIS_ACTOR_PLAYER
		ELIF IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherID])
			eActor = TENNIS_ACTOR_GENERIC_MALE
		ELSE
			eActor = TENNIS_ACTOR_GENERIC_FEMALE
		ENDIF
		bHappy = IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_GAME_DONE_STISH)
		thisUIData.sInterstitialFaceDict = GET_TENNIS_FACIAL_DICT(ePlayer = eSelfID, bHappy, eActor = TENNIS_ACTOR_GENERIC_FEMALE)
		GET_TENNIS_INTERSTITIAL_ANIMATIONS(thisUIData, eActor, bHappy)
		IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[ePlayer])) 
			SET_UP_TENNIS_INTERSTITIAL(thisProps, thisUIData, ePlayer, eActor = TENNIS_ACTOR_PLAYER)
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_OPP_SAD)
			bStartInterstitial = TRUE
		ENDIF
	ENDIF
	
	IF bStartInterstitial
		SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
		CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_GAME_DONE_STISH)
		
		VECTOR vScenePos = thisProps.tennisCourt.vCenterCourt
		VECTOR vSceneRot = <<0,0,GET_HEADING_FROM_VECTOR_2D(thisProps.vForward.x, thisProps.vForward.y)>>
		
		
		IF eActor = TENNIS_ACTOR_PLAYER
			IF thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)].eCourtSide = TENNIS_PLAYER_AWAY
				vSceneRot.z += 180.0
			ENDIF
			
			IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
				vSceneRot.z += PICK_FLOAT(bHappy, 180.0, 270.0)
			ENDIF
		
		ELSE
			IF thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)].eCourtSide = TENNIS_PLAYER_HOME
				vScenePos += 5.0 * thisProps.tennisCourt.vForwardNorm
				vSceneRot.z += 180.0
			ELSE
				vScenePos -= 5.0 * thisProps.tennisCourt.vForwardNorm
			ENDIF
		ENDIF
		
		thisUIData.iSceneID = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneRot)
		CDEBUG2LN(DEBUG_TENNIS, "HANDLE_TENNIS_INTERSTITIALS Stating interstitial at vScenePos=", vScenePos)
		
		IF DOES_CAM_EXIST(thisUIData.tennisCameras.tennisCamTut)
			DESTROY_CAM(thisUIData.tennisCameras.tennisCamTut)
		ENDIF
		thisUIData.tennisCameras.tennisCamTut = CREATE_CAM("DEFAULT_ANIMATED_CAMERA")
		
		PLAY_SYNCHRONIZED_CAM_ANIM(thisUIData.tennisCameras.tennisCamTut, thisUIData.iSceneID, thisUIData.sInterstitialCamAnim, thisUIData.sInterstitialDict)
		SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
		IF eActor = TENNIS_ACTOR_PLAYER
			TASK_SYNCHRONIZED_SCENE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)]), thisUIData.iSceneID, thisUIData.sInterstitialDict, thisUIData.sInterstitialPlrAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		ELSE
			sAnimDict = PICK_STRING(IS_TENNIS_PED_MALE((thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)])), "mini@tennis", "mini@tennis@female")
			//DEBUG_RECORD_STRING("TASK_PLAY_ANIM", thisUIData.sInterstitialPlrAnim)
			//CERRORLN(DEBUG_MISSION, "Playing facial anim: ", thisUIData.sInterstitialFaceAnim)
			TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)]), sAnimDict, thisUIData.sInterstitialPlrAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
			TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)]), thisUIData.sInterstitialFaceDict, thisUIData.sInterstitialFaceAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_SECONDARY)
			//CPRINTLN(DEBUG_TENNIS, "Tasked to play ", thisUIData.sInterstitialPlrAnim, " for Interstitial")
			TENNIS_PLAYER_ID eOtherPlayer = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)))
			TASK_STAND_STILL(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPlayer]), -1)
			#IF IS_TENNIS_MULTIPLAYER
				HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL VALIDATE_USE_BALL_SIMULATOR(TENNIS_GAME_PROPERTIES &thisProps, INT iBounces, INT iUtilFlags, TENNIS_GAME_STATE_ENUM thisTGSE)
	IF thisTGSE < TGSE_PREPARE_SERVE
		RETURN FALSE
	ENDIF
	IF IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
		RETURN FALSE
	ENDIF
	IF iBounces >= 2
		RETURN FALSE
	ENDIF
	IF IS_BITMASK_AS_ENUM_SET(iUtilFlags, TD_ANIM_PLAYING)
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
		RETURN FALSE
	ENDIF
	IF IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
		RETURN FALSE
	ENDIF
	IF NOT IS_BALL_WITHIN_TENNIS_COURT_WALLS(thisProps.tennisBall, thisProps.tennisCourt, thisProps.vForward, thisProps.vRight)
		IF NOT IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_BOUNCED_IN_TIME_SLICE)
			SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_BOUNCED_IN_TIME_SLICE)	// Set as a precaution the ball is way outside the court
		ENDIF
		RETURN FALSE
	ENDIF
	IF IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_PAUSE_UPDATE)
//		CPRINTLN(DEBUG_TENNIS, "VALIDATE_USE_BALL_SIMULATOR failed, TBF_PAUSE_UPDATE is set")
		RETURN FALSE
	ENDIF
	IF IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_HIT_PLAYER)
//		CPRINTLN(DEBUG_TENNIS, "VALIDATE_USE_BALL_SIMULATOR failed, TBF_HIT_PLAYER is set")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_TENNIS_AMBIENT_PREP_TIME_LIMIT(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
	INT iReturn = 0
	IF IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eSelfID], TPB_TENNIS_SWING_EVENT_FLAG)
		iReturn = 5000
		IF GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_DIVE_BACKHAND
		OR GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_DIVE_FOREHAND
			iReturn = 2500
		ENDIF
	ENDIF
	IF IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eOtherID], TPB_TENNIS_SWING_EVENT_FLAG)
		iReturn = 5000
		IF GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eOtherID]) = TSS_DIVE_BACKHAND
		OR GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eOtherID]) = TSS_DIVE_FOREHAND
			iReturn = 2500
		ENDIF
	ENDIF
	RETURN iReturn
ENDFUNC

/// PURPOSE:
///    Returns if the players are close enough to walk back to their starting positions instead of warping.
/// PARAMS:
///    thisProps - 
///    thisStatus - 
///    eSelfID - 
///    eOtherID - 
/// RETURNS:
///    TRUE if each ped is within CUTSCENE_SKIP_DIST_SQUARED (VDIST2 check) of their position. FALSE otherwise
FUNC BOOL SHOULD_TENNIS_PLAYERS_WALK_TO_POSITION(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_GAME_STATUS &thisStatus, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
	IF thisProps.eTennisActivity = TA_AI_VS_AI
		RETURN TRUE
	ENDIF
	CDEBUG2LN(DEBUG_TENNIS, "SHOULD_TENNIS_PLAYERS_WALK_TO_POSITION :: thisUIData.iNumWalksSinceCut=", thisUIData.iNumWalksSinceCut)
	IF thisUIData.iNumWalksSinceCut >= MAX_WALKS_IN_A_ROW
		CDEBUG2LN(DEBUG_TENNIS, "SHOULD_TENNIS_PLAYERS_WALK_TO_POSITION :: thisUIData.iNumWalksSinceCut >= MAX_WALKS_IN_A_ROW (", thisUIData.iNumWalksSinceCut, ")")
		thisUIData.iNumWalksSinceCut = 0
		RETURN FALSE
	ENDIF
	INT iPoints = thisStatus.playerScores[GET_TENNIS_STATUS_SCORED_LAST(thisStatus)].iPointsWon + 1
	IF iPoints >= PICK_INT(IS_TENNIS_SET_ON_TIE_BREAK(thisStatus), 7, 4) AND iPoints - thisStatus.playerScores[1 - ENUM_TO_INT(GET_TENNIS_STATUS_SCORED_LAST(thisStatus))].iPointsWon >= 2
		CDEBUG2LN(DEBUG_TENNIS, "SHOULD_TENNIS_PLAYERS_WALK_TO_POSITION :: Game was won by ", thisProps.tennisPlayers[GET_TENNIS_STATUS_SCORED_LAST(thisStatus)].playerAI.texName, " with points=", iPoints)
		thisUIData.iNumWalksSinceCut = 0
		RETURN FALSE
	ENDIF
//	IF GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_DIVE_BACKHAND OR GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_DIVE_FOREHAND
//		CDEBUG2LN(DEBUG_TENNIS, "SHOULD_TENNIS_PLAYERS_WALK_TO_POSITION :: ", thisProps.tennisPlayers[eSelfID].playerAI.texName, " is diving")
//		thisUIData.iNumWalksSinceCut = 0
//		RETURN FALSE
//	ENDIF
//	IF GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eOtherID]) = TSS_DIVE_BACKHAND OR GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eOtherID]) = TSS_DIVE_FOREHAND
//		CDEBUG2LN(DEBUG_TENNIS, "SHOULD_TENNIS_PLAYERS_WALK_TO_POSITION :: ", thisProps.tennisPlayers[eOtherID].playerAI.texName, " is diving")
//		thisUIData.iNumWalksSinceCut = 0
//		RETURN FALSE
//	ENDIF
	IF GET_TENNIS_RALLY_COUNTER(thisUIData) <= CUTSCENE_SKIP_RALLY_COUNT
		thisUIData.iNumWalksSinceCut++
		RETURN TRUE
	ENDIF
	
	VECTOR vSelfDest = GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, GET_TENNIS_SERVING_SIDE(thisStatus), thisProps.vRight, thisProps.vForward, (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus) = eSelfID), thisProps.tennisPlayers[eSelfID].eCourtSide)
	VECTOR vOtherDest = GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, GET_TENNIS_SERVING_SIDE(thisStatus), thisProps.vRight, thisProps.vForward, (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus) = eOtherID), thisProps.tennisPlayers[eOtherID].eCourtSide)
	FLOAT fSelfDist = VDIST2(vSelfDest, thisProps.tennisPlayers[eSelfID].vPos)
	FLOAT fOtherDist = VDIST2(vOtherDest, thisProps.tennisPlayers[eOtherID].vPos)
	CPRINTLN(DEBUG_TENNIS, "fSelfDist=", fSelfDist, ", fOtherDist=", fOtherDist, ", CUTSCENE_SKIP_DIST_SQUARED=", CUTSCENE_SKIP_DIST_SQUARED)
	IF fSelfDist < CUTSCENE_SKIP_DIST_SQUARED AND fOtherDist < CUTSCENE_SKIP_DIST_SQUARED
		thisUIData.iNumWalksSinceCut++
		RETURN TRUE
	ENDIF
	
	thisUIData.iNumWalksSinceCut = 0
	RETURN FALSE
ENDFUNC

PROC HANDLE_TENNIS_QUIT_INPUT(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATUS &thisStatus, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eSelfID)
	UNUSED_PARAMETER(eSelfID)
	IF VALIDATE_QUIT_BUTTON_PRESSED(GET_TENNIS_GAME_STATE(thisStatus))
	AND GET_TENNIS_GAME_STATE(thisStatus) > TGSE_INTRO
	AND GET_TENNIS_GAME_STATE(thisStatus) < TGSE_OUTRO
	AND GET_TENNIS_GAME_STATE(thisStatus) <> TGSE_QUIT_CONFIRM
	AND GET_TENNIS_GAME_STATE(thisStatus) <> TGSE_QUIT_FROM_OPTION_MENU
	AND thisProps.eTennisActivity <> TA_AI_VS_AI
	AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
		IF GET_TENNIS_GAME_STATE(thisStatus) = TGSE_OPTION_MENU AND IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SCLB)
			IF IS_PLAYER_ONLINE()
				CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SCLB)
			ENDIF
			BOOL bSelectDiff = (thisProps.eTennisActivity <> TA_FRIEND_ACTIVITY AND NOT GET_TENNIS_STATUS_REMATCH(thisStatus) AND NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP))
			BUILD_TENNIS_INTRO_MENU(thisUIData, bSelectDiff)
		ELSE
			CPRINTLN(DEBUG_TENNIS, "Quit Button Press Detected")
			SET_TENNIS_GAME_STATE_QUIT(thisStatus)
			SET_TENNIS_PAUSED(TRUE, thisProps.tennisBall)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the TPB_DROP_OUT_OF_TENNIS_MODE flag from the players thruout the whole game
/// PARAMS:
///    thisProps - 
///    iServerSeqProgress - gives sequence progress back out
///    iReceiverSeqProgress - gives sequence progress back out
PROC UPDATE_TENNIS_MODE_FROM_PLAYER_FLAGS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATUS &thisStatus, INT &iServerSeqProgress, INT &iReceiverSeqProgress)
	TENNIS_PLAYER_ID eServer, eReceiver
	eServer = GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)
	eReceiver = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eServer))
	// Get sequence progress
	IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eServer])) AND GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eServer]), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
		iServerSeqProgress = GET_SEQUENCE_PROGRESS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eServer]))
		//CDEBUG3LN(DEBUG_TENNIS, "UPDATE_TENNIS_MODE_FROM_PLAYER_FLAGS :: iServerSeqProgress=", iServerSeqProgress)
	ENDIF
	IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eReceiver])) AND GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eReceiver]), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
		iReceiverSeqProgress = GET_SEQUENCE_PROGRESS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eReceiver]))
		//CDEBUG3LN(DEBUG_TENNIS, "UPDATE_TENNIS_MODE_FROM_PLAYER_FLAGS :: iReceiverSeqProgress=", iReceiverSeqProgress)
	ENDIF
	// Drop the players out of Tennis Mode if their flags are set
	IF IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eServer], TPB_DROP_OUT_OF_TENNIS_MODE) AND iServerSeqProgress > 0
		CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eServer], TPB_DROP_OUT_OF_TENNIS_MODE)
		SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eServer]), FALSE)
		SET_PED_WEAPON_MOVEMENT_CLIPSET(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eServer]), "weapons@tennis@male")
	ENDIF
	IF IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eReceiver], TPB_DROP_OUT_OF_TENNIS_MODE) AND iReceiverSeqProgress > 0
		CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eReceiver], TPB_DROP_OUT_OF_TENNIS_MODE)
		SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eReceiver]), FALSE)
		SET_PED_WEAPON_MOVEMENT_CLIPSET(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eReceiver]), "weapons@tennis@male")
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisProps - This is the only parameter needed for an ambient game
///    thisStatus - 
///    thisSPData - 
///    iUtilFlags - For an ambient game pass in 0, otherwise pass in the thisUIData flag
///    eSelfID - 
PROC PROCESS_GAME_STATE(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATUS &thisStatus, INT &iUtilFlags, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, FLOAT& fMessageTimer, BOOL bFreemodeIntro=FALSE)
	INT EX, WHY, dummy
	VECTOR vOffset = <<0,0,0>>, vPredictedBallPos, vMarkerPos
	TENNIS_PLAYER_ID ePlayer
	FLOAT fTimeToPrediction
	VECTOR vBallPos
	
	BOOL bIgnoreResetFlags = (thisProps.eTennisActivity = TA_AI_VS_AI) AND GET_TENNIS_GAME_STATE(thisStatus) = TGSE_TENNIS_PLAYERS_WALKING_WAIT
	
	COMMON_EVERY_FRAME_DATA(thisProps, GET_TENNIS_GAME_STATE(thisStatus), iUtilFlags, eSelfID, eOtherID, bIgnoreResetFlags)
			
	//SET_BITMASK_AS_ENUM(iUtilFlags, TD_UPDATE_SCORES)
	IF thisProps.eTennisActivity = TA_AI_VS_AI AND NOT bFreemodeIntro
		FLIP_TENNIS_PREDICTION_FLAG(thisProps)
	ENDIF
	
	#IF TENNIS_DEBUG_RECORDING
	IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[eOtherID].pIndex) AND DOES_ENTITY_EXIST(thisProps.tennisPlayers[eSelfID].pIndex) AND NOT IS_PED_INJURED(thisProps.tennisPlayers[eSelfID].pIndex) AND NOT IS_PED_INJURED(thisProps.tennisPlayers[eOtherID].pIndex)
		DEBUG_RECORD_CLEAR_ENTITIES()
		DEBUG_RECORD_ENTITY(thisProps.tennisPlayers[eOtherID].pIndex)
		DEBUG_RECORD_ENTITY(thisProps.tennisPlayers[eSelfID].pIndex)
		DEBUG_RECORD_ENTITY_VECTOR(thisProps.tennisPlayers[eOtherID].pIndex, "AI Pos", thisProps.tennisPlayers[eOtherID].vPos, TRUE)
		DEBUG_RECORD_ENTITY_VECTOR(thisProps.tennisPlayers[eOtherID].pIndex, "AI Velocity", GET_ENTITY_VELOCITY(thisProps.tennisPlayers[eOtherID].pIndex), FALSE)
		DEBUG_RECORD_ENTITY_VECTOR(thisProps.tennisPlayers[eSelfID].pIndex, "My Pos", thisProps.tennisPlayers[eOtherID].vPos, TRUE)
		DEBUG_RECORD_ENTITY_VECTOR(thisProps.tennisPlayers[eSelfID].pIndex, "My Velocity", GET_ENTITY_VELOCITY(thisProps.tennisPlayers[eOtherID].pIndex), FALSE)
		DEBUG_RECORD_ENTITY_FLOAT(thisProps.tennisPlayers[eSelfID].pIndex, "GET_FRAME_TIME", GET_FRAME_TIME())
	ENDIF
	#ENDIF
	
	IF IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_BOUNCED_IN_TIME_SLICE)
		CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_BOUNCED_IN_TIME_SLICE)
	ENDIF

	INT iBounces = GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
	IF VALIDATE_USE_BALL_SIMULATOR(thisProps, iBounces, iUtilFlags, GET_TENNIS_GAME_STATE(thisStatus))
		CDEBUG3LN(DEBUG_TENNIS, "UPDATE_BALL_SIMULATOR in PROCESS_GAME_STATE")
		BOOL bPlaySound = (GET_TENNIS_GAME_STATE(thisStatus) <> TGSE_PREPARE_SERVE AND GET_TENNIS_GAME_STATE(thisStatus) < TGSE_INTERSTITIAL)
		vBallPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
		TENNIS_NET_BOUNCE_ENUM eBounce = UPDATE_BALL_SIMULATOR(thisProps.tennisCourt, vBallPos, thisProps.tennisBall.vBallVel, thisProps.vForward, thisProps.tennisBall.fFlightTimer, thisProps.tennisBall.eBallSpin, thisProps.tennisBall.fSpinTimer, iBounces, bPlaySound, -1, IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED))
		IF eBounce = TNB_GROUND_BOUNCE
			SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_BOUNCED_IN_TIME_SLICE)
		ELIF eBounce = TNB_RICOCHET OR eBounce = TNB_NORMAL_BOUNCE
			CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[0], TPB_REDUCE_PREDICTION_CALC)
			CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[1], TPB_REDUCE_PREDICTION_CALC)
		ENDIF
		SET_TENNIS_BALL_POS(thisProps.tennisBall, vBallPos)
		SET_ENTITY_COORDS(thisProps.tennisBall.oBall, GET_TENNIS_BALL_POS(thisProps.tennisBall), default, default, default, FALSE)
		SET_ENTITY_VELOCITY(thisProps.tennisBall.oBall, thisProps.tennisBall.vBallVel)
		vBallPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
		#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_SPHERE("BallInSim", vBallPos, 0.05, (<<1,1,0>>))
		#ENDIF
	ELIF GET_TENNIS_GAME_STATE(thisStatus) > TGSE_INTRO
		IF DOES_TENNIS_BALL_TRAIL_EXIST(thisProps.tennisBall)
			STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
		ENDIF
	ENDIF
	// Handle the player's flags for dropping out of tennis mode.
	INT iServerSeqProgress, iReceiverSeqProgress
	UPDATE_TENNIS_MODE_FROM_PLAYER_FLAGS(thisProps, thisStatus, iServerSeqProgress, iReceiverSeqProgress)
	
	SWITCH GET_TENNIS_GAME_STATE(thisStatus)
	
		CASE TGSE_PRE_INIT
			IF IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
				CLEAR_PRINTS()
				SET_ENTITY_COORDS(thisProps.tennisPlayers[eSelfID].pIndex, GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, GET_TENNIS_SERVING_SIDE(thisStatus), thisProps.vRight, thisProps.vForward, eSelfID = (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)), eSelfID),TRUE,TRUE)
				SET_ENTITY_HEADING(GET_PLAYER_PED(GET_PLAYER_INDEX()), GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eSelfID].vMyForward.x,thisProps.tennisPlayers[eSelfID].vMyForward.y))
				NEW_LOAD_SCENE_START_SPHERE(thisProps.tennisCourt.vCenterCourt, 50)
				DEBUG_MESSAGE("Set Network Start Load Scene at thisProps.tennisCourt.vCenterCourt")
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_PRE_INIT_WAIT)
			ENDIF
		BREAK
		
		CASE TGSE_PRE_INIT_WAIT
			IF IS_NEW_LOAD_SCENE_ACTIVE()	//IS_NEW_LOAD_SCENE_LOADED()
				DEBUG_MESSAGE("IS_NEW_LOAD_SCENE_ACTIVE returned TRUE, continue!")
				DO_SCREEN_FADE_IN(250)
				IF thisProps.eTennisActivity <> TA_AI_VS_AI
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
				ENDIF
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_INIT)
			ELSE
				DEBUG_MESSAGE("IS_NEW_LOAD_SCENE_ACTIVE returned FALSE, wait more til everything is loaded")
			ENDIF
		BREAK
		
		CASE TGSE_INIT
			IF HAVE_REQUEST_TENNIS_ASSETS_LOADED(thisProps)
				IF thisProps.eTennisActivity = TA_AI_VS_AI
					VECTOR vStart
					DEBUG_SPAWN_BALL(thisProps, TRUE)
					vStart = GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, SERVING_FROM_RIGHT, thisProps.vRight, thisProps.vForward, TRUE, thisProps.tennisPlayers[eSelfID].eCourtSide)
					SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID], CREATE_PED(PEDTYPE_PLAYER2, thisProps.mThePlayerAIModel, vStart, GET_HEADING_FROM_VECTOR_2D(thisProps.vForward.x, thisProps.vForward.y), FALSE, FALSE))
					TASK_STAND_STILL(thisProps.tennisPlayers[eSelfID].pIndex, -1)
					SET_ENTITY_HEADING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), GET_HEADING_FROM_VECTOR_2D(thisProps.vForward.x, thisProps.vForward.y) + 180)
					vOffset = GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, GET_TENNIS_SERVING_SIDE(thisStatus), thisProps.vRight, thisProps.vForward, eOtherID = (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)), eOtherID)
					SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID], CREATE_PED(PEDTYPE_PLAYER2, thisProps.mTheOpponentModel, vOffset, GET_HEADING_FROM_VECTOR_2D(-thisProps.vForward.x, -thisProps.vForward.y), FALSE, FALSE))
					TASK_STAND_STILL(thisProps.tennisPlayers[eOtherID].pIndex, -1)
					SET_ENTITY_HEADING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), GET_HEADING_FROM_VECTOR_2D(-thisProps.vForward.x, -thisProps.vForward.y))
					
					//this is an AI_VS_AI game
					vOffset = <<0,0,0>>
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_PREPARE_SERVE)
					SET_TENNIS_SERVE_COUNT(thisStatus, 0)
					SET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus, eSelfID)
					thisProps.tennisPlayers[eSelfID].oTennisRacket = CREATE_OBJECT(PROP_TENNIS_RACK_01B, thisProps.tennisCourt.vCourtCorners[3], FALSE, FALSE)
					SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eSelfID].oTennisRacket, FALSE)
					thisProps.tennisPlayers[eOtherID].oTennisRacket = CREATE_OBJECT(PROP_TENNIS_RACK_01B, thisProps.tennisCourt.vCourtCorners[2], FALSE, FALSE)
					ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eSelfID, eOtherID)
					SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eOtherID].oTennisRacket, FALSE)
					SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eSelfID].oTennisRacket, TRUE)
					SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(thisProps.tennisPlayers[eSelfID].oTennisRacket, TRUE)
					SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eOtherID].oTennisRacket, TRUE)
					SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(thisProps.tennisPlayers[eOtherID].oTennisRacket, TRUE)
					SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]),TRUE)
					SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]),TRUE)
					
					//If the game is set at Michael's House it must be Family Tennis because we already know it's TA_AI_VS_AI
					IF thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_MichaelHouse
						SET_FAMILY_TENNIS_PEDS_GROUP_AND_UNTARGETTABLE(thisProps.tennisPlayers)
						SET_PED_COMPONENT_VARIATION(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
						SET_PED_COMPONENT_VARIATION(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
						SET_PED_COMPONENT_VARIATION(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
						SET_PED_COMPONENT_VARIATION(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
					ENDIF
				ELSE //Entity is not dead and this isn't AI vs AI
					//Wait for PROCESS_TENNIS_UI_DATA to move the game state on
				ENDIF
			ELSE
				CPRINTLN(DEBUG_TENNIS, "Waiting on HAVE_REQUEST_TENNIS_ASSETS_LOADED in PROCESS_GAME_STATE")
			ENDIF
		BREAK
			
// ********************************************* Player Serves *******************************************************************
		CASE TGSE_PREPARE_SERVE
			RESET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
			
			IF IS_BITMASK_AS_ENUM_SET(iUtilFlags, TD_FIRST_SERVE)
				ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eSelfID, eOtherID)
				SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eSelfID].oTennisRacket, TRUE)
				SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eOtherID].oTennisRacket, TRUE)
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_FIRST_SERVE)
			ENDIF
			BOOL bDoneDivingReacting
			bDoneDivingReacting = GET_GAME_TIMER() > GET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisProps.tennisPlayers[eSelfID]) AND GET_GAME_TIMER() > GET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisProps.tennisPlayers[eOtherID])
			IF (thisProps.eTennisActivity <> TA_AI_VS_AI AND NOT IS_BITMASK_AS_ENUM_SET(iUtilFlags, TD_ANIM_PLAYING) AND bDoneDivingReacting)
			OR (thisProps.eTennisActivity = TA_AI_VS_AI	AND GET_TENNIS_TIMER_A(thisProps) > GET_TENNIS_AMBIENT_PREP_TIME_LIMIT(thisProps, eSelfID, eOtherID) AND NOT bFreemodeIntro)
			OR (thisProps.eTennisActivity = TA_AI_VS_AI	AND bFreemodeIntro)
				UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), eSelfID)
				UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eOtherID], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), eOtherID)
				SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eSelfID].playerAI, ASE_AI_IDLE)
				SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eOtherID].playerAI, ASE_AI_IDLE)
				SET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID], TSS_NO_SWING)
				SET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eOtherID], TSS_NO_SWING)
				CLEAR_TENNIS_PLAYER_CONTROL_BITS(thisProps.tennisPlayers[eSelfID])
				CLEAR_TENNIS_PLAYER_CONTROL_BITS(thisProps.tennisPlayers[eOtherID])
				CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED)
				CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_TENNIS_SWING_EVENT_FLAG)
				CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eOtherID], TPB_TENNIS_SWING_EVENT_FLAG)
				CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_FORCED_UPDATE)
				CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eOtherID], TPB_FORCED_UPDATE)
				ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, TRUE)
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_DELAY_THE_CUT_DELAY)
				// Clear the conversation bits in case we didn't play a convo last interstitial
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_L_DSTAR)
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_W_DSTAR)
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_FAULT_DSTAR)
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_GAME_LOST_DSTAR)
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_GAME_WON_DSTAR)
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_MATCH_POINT_DSTAR)
				CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_STOP_INTERSTITIAL_CAM_POINTING)
				CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_CALL_SIDE_CHANGE_ONCE)
				CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_SERVE_WAS_POWERFUL)
				IF thisStatus.ePrevGameState <> TGSE_SERVE_RESULT	//If the server did not fault
					//Reset the serve count
					SET_TENNIS_SERVE_COUNT(thisStatus, 0)
					CPRINTLN(DEBUG_TENNIS, "Resetting the serve count.")
				ENDIF
				
				// Reset the players' shot types
				SET_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eSelfID], SHOT_NORMAL)
				SET_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eOtherID], SHOT_NORMAL)
				IF thisProps.eTennisActivity <> TA_AI_VS_AI AND NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
					DEBUG_SPAWN_BALL(thisProps, TRUE)
				ENDIF
				BOOL bWalkToPos
				bWalkToPos = thisProps.eTennisActivity = TA_AI_VS_AI OR IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
				// If the player wasn't tasked in CASE TGSE_POINT_WON, get them back to position now
				IF NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)].playerAI, TAF_TASKED_NEW_RALLY)
					SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), 
						GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), thisProps.eTennisActivity <> TA_AI_VS_AI, IS_BITMASK_AS_ENUM_SET(iUtilFlags, TD_FIRST_SERVE),
						(thisProps.eTennisActivity <> TA_AI_VS_AI AND NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)), bWalkToPos)
				ENDIF
				// If the player wasn't tasked in CASE TGSE_POINT_WON, get them back to position now
				IF NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))].playerAI, TAF_TASKED_NEW_RALLY)
					SET_TENNIS_PLAYER_RECEIVE_SERVE(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), 
						INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))), thisProps.eTennisActivity <> TA_AI_VS_AI, bWalkToPos, default, 
						NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))].playerAI, TAF_TASKED_ANIM))
				ENDIF
				CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_BASELINE_TRACKING_INIT)
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_TENNIS_PLAYERS_WALKING_WAIT)
				// Clear these now that they are no longer needed
				CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_NEW_RALLY)
				CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[eOtherID].playerAI, TAF_TASKED_NEW_RALLY)
				// Reset ball spin and timerA
				RESET_TENNIS_BALL_SPIN(thisProps.tennisBall)
				SET_TENNIS_TIMER_A(thisProps, 0)
				ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, FALSE)
			ELSE
				ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, FALSE)
			ENDIF
		BREAK
		
		CASE TGSE_SERVE
			UPDATE_TENNIS_OPPONENT_WALK_BACK_PROGRESS(thisProps.tennisPlayers, eOtherID, GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), thisProps.eTennisActivity, IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eOtherID].playerAI, TAF_TASKED_NEW_RALLY))
			IF IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_BALL_RESET_IN_HAND)
				CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_BALL_RESET_IN_HAND)
			ENDIF
			IF NOT IS_BITMASK_AS_ENUM_SET(iUtilFlags, TD_ANIM_PLAYING)
				UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), TRUE)
				UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))))
			ENDIF
			
			BOOL bMoveState
			
			UPDATE_TENNIS_PLAYER_SWINGING_AIMLESSLY(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))], thisProps.tennisBall)
			
			IF thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)].controlType <> COMPUTER
				LAUNCH_DETAILS serveGrade
				IF UPDATE_TENNIS_PLAYER_SERVE_TAP_TWICE(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), serveGrade)
					bMoveState = TRUE
				ENDIF
			ELSE
				STRING sAnimDict
				sAnimDict = PICK_STRING(IS_TENNIS_PED_MALE((thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)])), "mini@tennis", "mini@tennis@female")
				IF IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)]), sAnimDict, "serve")
					IF thisProps.eTennisActivity = TA_AI_VS_AI AND DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) 
						FLOAT fAnimTime
						fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)]), sAnimDict, "serve")
						IF fAnimTime > TIME_TO_BALL_IN_HAND AND fAnimTime < (TIME_TO_BALL_IN_HAND * 10.0)
							CPRINTLN(DEBUG_TENNIS, "fAnimTime: ", fAnimTime)
							ATTACH_TENNIS_BALL_TO_PLAYER(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)], thisProps.tennisBall)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_READY_FOR_FM_INTRO)
					SET_TENNIS_GLOBAL_FLAG(TGF_READY_FOR_FM_INTRO)
				ENDIF
					
				IF UPDATE_SERVE_AI(thisProps, thisStatus, thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)], bFreemodeIntro)
					bMoveState = TRUE
				ENDIF
			ENDIF
			IF bMoveState
				SET_BITMASK_AS_ENUM(iUtilFlags, TD_ACE)
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_WAIT_FOR_PREDICTION)
				SET_TENNIS_STATUS_HIT_LAST(thisStatus, GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))
				TASK_TENNIS_PLAYER_WATCH_BALL(thisProps.tennisPlayers[eSelfID], thisProps.tennisBall, TRUE)
				TASK_TENNIS_PLAYER_WATCH_BALL(thisProps.tennisPlayers[eOtherID], thisProps.tennisBall, TRUE)
			ENDIF
		BREAK
		
		CASE TGSE_SERVE_RESULT
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))))
			UPDATE_TENNIS_PLAYER_SWINGING_AIMLESSLY(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)], thisProps.tennisBall)
			IF UPDATE_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))], thisProps, thisStatus.playerScores, GET_TENNIS_STATUS_HIT_LAST(thisStatus), INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))), GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), GET_TENNIS_SERVING_SIDE(thisStatus), IS_TENNIS_SET_ON_TIE_BREAK(thisStatus))
				SET_TENNIS_STATUS_HIT_LAST(thisStatus, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))))
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_ACE)
			ENDIF
			
			IF GET_FRAME_COUNT() > GET_TENNIS_BALL_INTO_PLAYER_FRAME_STAMP(thisProps.tennisBall)
			AND IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))], TPB_HIT_BY_BALL)
				CPRINTLN(DEBUG_TENNIS, "TGSE_SERVE_RESULT: Player was hit by the ball, point won")
				SET_TENNIS_SCORED_LAST(thisStatus, GET_TENNIS_STATUS_HIT_LAST(thisStatus))
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_POINT_WON)
				PAUSE_TENNIS_INTERSTITIAL_AND_DIALOGUE(iUtilFlags)
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_ACE)
			ENDIF
		
			// Wait for 1st collision. Net or 1st bounce
			
			IF HAS_BALL_BOUNCED(thisProps.tennisBall, GET_TENNIS_FRAME_COUNTER(thisProps), thisProps.tennisCourt.vCenterCourt)
				CPRINTLN(DEBUG_TENNIS, "TGSE_SERVE_RESULT ball has bounced")
				IF IS_BALL_INSIDE_SERVICE_AREA(thisProps.tennisCourt, thisProps, thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)].eCourtSide, GET_TENNIS_SERVING_SIDE(thisStatus))
					// Serve Good
					INCREASE_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_RALLY)
				ELSE
					// Serve Out
					// Second Serve?
					PAUSE_TENNIS_INTERSTITIAL_AND_DIALOGUE(iUtilFlags)
					IF GET_TENNIS_SERVE_COUNT(thisStatus) = 0
						IF eSelfID = (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))
							SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_PLR_SAD)
						ELSE
							SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_OPP_SAD)
						ENDIF
						SET_TENNIS_SERVE_COUNT(thisStatus, 1)
						SET_TENNIS_GAME_STATE(thisStatus, TGSE_PREPARE_SERVE)
						SET_BITMASK_AS_ENUM(iUtilFlags, TD_FAULT)
						SET_BITMASK_AS_ENUM(iUtilFlags, TD_FAULT_DSTAR)
					ELSE	// Double Fault
						SET_TENNIS_SERVE_COUNT(thisStatus, 2)
						SET_TENNIS_SCORED_LAST(thisStatus, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))))
						CPRINTLN(DEBUG_TENNIS, "SERVE_RESULT_PLAYER: Point won off a double fault")
						SET_TENNIS_GAME_STATE(thisStatus, TGSE_POINT_WON)
					ENDIF
				ENDIF		
			ELSE
				//If the server is allowed to hit the ball then that means the servee hit it before it bounced. Server gets a point.
				IF GET_TENNIS_STATUS_HIT_LAST(thisStatus) <> GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)
					CPRINTLN(DEBUG_TENNIS, "Serve hit before a bounce, they lose a point!")
					IF eSelfID = (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))
						SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_PLR_HAPPY)
					ELSE
						SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_OPP_HAPPY)
					ENDIF
					SET_TENNIS_SCORED_LAST(thisStatus, GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))
					CPRINTLN(DEBUG_TENNIS, "SERVE_RESULT_PLAYER: Point won because servee hit before the bounce")
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_POINT_WON)
					PAUSE_TENNIS_INTERSTITIAL_AND_DIALOGUE(iUtilFlags)
				ENDIF
			ENDIF
			
			// For some reason I saw the ball go through the ground.
			vBallPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
			IF vBallPos.z < thisProps.tennisCourt.vCourtCorners[0].z-1
			AND (VDIST2(thisProps.tennisCourt.vCenterCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall)) < COURT_RANGE * COURT_RANGE)
				SET_TENNIS_SCORED_LAST(thisStatus, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))))
				CPRINTLN(DEBUG_TENNIS, "SERVE_RESULT_PLAYER: Ball is under the court.")
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_POINT_WON)
			ENDIF
		BREAK

// ******************************************** After Serves *********************************************************				
		
		CASE TGSE_WAIT_FOR_PREDICTION
			ePlayer = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)))
			CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[ePlayer], TPB_REDUCE_PREDICTION_CALC)
			vMarkerPos = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(
				thisProps.tennisCourt,
				GET_TENNIS_BALL_POS(thisProps.tennisBall), 
				thisProps.tennisBall.vBallVel,
				thisProps.vForward,
				GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), 
				GET_TENNIS_BALL_SPIN(thisProps.tennisBall), 
				GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
			SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[ePlayer], vMarkerPos)
			
			vPredictedBallPos = GET_CLOSEST_POINT_FOR_TENNIS_SWING(
				thisProps.tennisPlayers[ePlayer].vPos,
				thisProps.tennisCourt,
				GET_TENNIS_BALL_POS(thisProps.tennisBall), 
				thisProps.tennisBall.vBallVel,
				thisProps.vForward,
				GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), 
				GET_TENNIS_BALL_SPIN(thisProps.tennisBall), 
				GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall), fTimeToPrediction)
			SET_AI_PREDICTION_POINT(thisProps.tennisPlayers[ePlayer].playerAI, GET_BEST_TENNIS_PREDICTION_FROM_OFFSET(
				thisProps.tennisPlayers[ePlayer], 
				thisProps.vSwingStateOffsets, 
				vPredictedBallPos,
				thisProps.tennisCourt.vCenterCourt.z, fTimeToPrediction))
			IF NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[ePlayer].playerAI, TAF_CHECK_REACTS)
				SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[ePlayer].playerAI, TAF_CHECK_REACTS)
			ENDIF
			
			IF bFreemodeIntro
				SET_AI_REACTION_TIME(thisProps.tennisPlayers[ePlayer].playerAI, 0.0)
			ELSE
				SET_RANDOM_AI_REACTION_TIME(thisProps.tennisPlayers[ePlayer].playerAI)
				//Predict where the tennis ball will land and if the ped is an AI tell them not to chase it if it is OOB
				//Do this after the bounce count check to make sure we're not checking where it lands after the bounce.
				IF NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[ePlayer].playerAI, TAF_CHECK_FOR_OOB)
				AND GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall) < 1 
				AND NOT IS_POINT_INSIDE_TENNIS_AREA(vMarkerPos, thisProps.tennisCourt.vCourtCorners[0] + (thisProps.vRight * 0.5), thisProps.tennisCourt.vCourtCorners[3] + (thisProps.vRight * 0.5), GET_TENNIS_COURT_WIDTH(thisProps.tennisCourt))
					CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY_PREDICT: Ball is out of bounds")
					IF GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[ePlayer]) = TSS_NO_SWING
						CPRINTLN(DEBUG_TENNIS, "Telling the AI to stop chasing the ball")
						SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[ePlayer].playerAI, TAF_CHECK_FOR_OOB)
					ENDIF
				ENDIF
			ENDIF
			
			IF thisProps.tennisPlayers[ePlayer].controlType = COMPUTER
				SET_TENNIS_AI_STATE(thisProps.tennisPlayers[ePlayer].playerAI, ASE_AI_REACTING)
			ELSE
				SET_TENNIS_AI_STATE(thisProps.tennisPlayers[ePlayer].playerAI, ASE_AI_MOVING_TO_BALL)
			ENDIF
			SET_TENNIS_AI_STATE(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)].playerAI, ASE_AI_MOVING_TO_IDLE)
			SET_TENNIS_GAME_STATE(thisStatus, TGSE_SERVE_RESULT)
			//X marker should only show when tennisPlayers[1 - ENUM_TO_INT(thisStatus.eWhoIsServing)] is human
			IF thisProps.tennisPlayers[ePlayer].controlType <> COMPUTER
				fMessageTimer = 0
				thisProps.tennisBall.iBounceCount = 0
				SET_BITMASK_AS_ENUM(iUtilFlags, TD_X_MARKER)
				CPRINTLN(DEBUG_TENNIS, "Telling the Bounce Marker to show and resetting gating variables")
			ENDIF
			
			//Figuring out which side the opponent shot the ball to
			IF thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus))].eCourtSide = (TENNIS_PLAYER_AWAY)
				dummy = 0
			ELIF thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus))].eCourtSide = (TENNIS_PLAYER_HOME)
				dummy = 2
			ELSE
				CPRINTLN(DEBUG_TENNIS, "TGSE RALLY: we're using an invalid eHitLast enum! Enum: ", GET_TENNIS_STATUS_HIT_LAST(thisStatus))
			ENDIF
			ADJUST_AI_HOME_POSITIONS_WITH_BUCKETS(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus))], thisProps.tennisCourt.vCourtCorners[dummy], thisProps.tennisCourt.vServiceCorners[dummy])
			ADJUST_AI_SERVE_POSITION_WITH_BUCKETS(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus))], GET_TENNIS_SERVING_SIDE(thisStatus))
		BREAK	
		
// ******************************************** Rally *********************************************************
		CASE TGSE_RALLY
			vBallPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
			IF NOT UPDATE_BOUNCE_COUNT(thisProps, GET_TENNIS_STATUS_HIT_LAST(thisStatus))
				IF GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall) > 1
					// 2 bounces? Person who hit last won
					SET_TENNIS_SCORED_LAST(thisStatus, GET_TENNIS_STATUS_HIT_LAST(thisStatus))
					PAUSE_TENNIS_INTERSTITIAL_AND_DIALOGUE(iUtilFlags)
					CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY: 2 bounces")
				ELSE
					// 1 bounce out of bound? Person who just hit last didn't win
					SET_TENNIS_SCORED_LAST(thisStatus, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus))))
					PAUSE_TENNIS_INTERSTITIAL_AND_DIALOGUE(iUtilFlags)
					CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY: 1 bounce")
					// I didn't score so I must have hit it OOB
					IF GET_TENNIS_STATUS_SCORED_LAST(thisStatus) <> eSelfID AND thisProps.eTennisActivity = TA_AI_VS_AI
						g_savedGlobals.sTennisData.iTotalOOB++
						INCREASE_TENNIS_MP_STAT(TMST_TENNIS_TOTAL_OOB_HITS)
					ENDIF
					IF IS_POINT_JUST_OUT_OF_TENNIS_BOUNDS(thisProps.tennisBall.vCollPos, thisProps.tennisCourt, thisProps.vForward, thisProps.vRight)
						SET_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus)))], TPB_RAGE_ANIM)
					ENDIF
				ENDIF
				CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY: Point won because UPDATE_BOUNCE_COUNT returned FALSE")
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_POINT_WON)
			ELIF vBallPos.z < thisProps.tennisCourt.vCourtCorners[0].z - 1 //1 is a magic debug number
			AND (VDIST2(thisProps.tennisCourt.vCenterCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall)) < COURT_RANGE * COURT_RANGE)
				// For some reason I saw the ball go through the ground.
				CPRINTLN(DEBUG_TENNIS, "TENNIS_GAME_RALLY: Ball is under the court.")
				SET_TENNIS_SCORED_LAST(thisStatus, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus))))
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_POINT_WON)
				// I didn't score so I must have hit it under the court :(
				IF GET_TENNIS_STATUS_SCORED_LAST(thisStatus) <> eSelfID AND thisProps.eTennisActivity <> TA_AI_VS_AI
					g_savedGlobals.sTennisData.iTotalOOB++
					INCREASE_TENNIS_MP_STAT(TMST_TENNIS_TOTAL_OOB_HITS)
				ENDIF
			ENDIF
			
			UPDATE_TENNIS_PLAYER_SWINGING_AIMLESSLY(thisProps.tennisPlayers[GET_TENNIS_STATUS_HIT_LAST(thisStatus)], thisProps.tennisBall)
			IF UPDATE_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus))], thisProps, thisStatus.playerScores, GET_TENNIS_STATUS_HIT_LAST(thisStatus), INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus))), GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), GET_TENNIS_SERVING_SIDE(thisStatus), IS_TENNIS_SET_ON_TIE_BREAK(thisStatus))
				RESET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
				SET_TENNIS_STATUS_HIT_LAST(thisStatus, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus))))
				IF thisProps.tennisPlayers[GET_TENNIS_STATUS_HIT_LAST(thisStatus)].controlType = COMPUTER
					SET_TENNIS_AI_STATE(thisProps.tennisPlayers[GET_TENNIS_STATUS_HIT_LAST(thisStatus)].playerAI, ASE_AI_MOVING_TO_IDLE)
					SET_RANDOM_TENNIS_AI_HOME_POS(thisProps.tennisPlayers[GET_TENNIS_STATUS_HIT_LAST(thisStatus)], thisProps.tennisCourt, thisProps.vRight)
				ELSE
					CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_X_MARKER)
				ENDIF
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_ACE)
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_RALLY_PREDICT)
				
				// Reset the power timers so you can't charge it on the opponent's rally
				RESET_TENNIS_PLAYER_POWER_TIMER(thisProps.tennisPlayers[eSelfID])
				RESET_TENNIS_PLAYER_POWER_TIMER(thisProps.tennisPlayers[eOtherID])
				
			ELIF GET_FRAME_COUNT() > GET_TENNIS_BALL_INTO_PLAYER_FRAME_STAMP(thisProps.tennisBall)
			AND IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus))], TPB_HIT_BY_BALL)
				CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY: Player was hit by the ball, point won")
				SET_TENNIS_SCORED_LAST(thisStatus, GET_TENNIS_STATUS_HIT_LAST(thisStatus))
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_POINT_WON)
				PAUSE_TENNIS_INTERSTITIAL_AND_DIALOGUE(iUtilFlags)
				CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_ACE)
			ENDIF
			
			// Update player mover
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), eSelfID)
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eOtherID], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), eOtherID)
			
			IF thisProps.eTennisActivity <> TA_AI_VS_AI
				HANDLE_TENNIS_PLAYER_OUTSIDE_PLAYING_AREA(thisProps.tennisPlayers[eSelfID], thisProps.tennisCourt)
			ENDIF
			
			IF IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_ANIM) 
			AND NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eOtherID].playerAI, TAF_TASKED_ANIM)
			AND NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eOtherID].playerAI, TAF_TASKED_NEW_RALLY)
				TASK_STAND_STILL(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), -1)
				CPRINTLN(DEBUG_TENNIS, "Tasked Plyr2 to stand still")
			ENDIF
		BREAK
		
		CASE TGSE_RALLY_PREDICT
			CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY_PREDICT: ", 
				PICK_STRING(GET_TENNIS_STATUS_HIT_LAST(thisStatus) = TENNIS_PLAYER_AWAY, "TENNIS_PLAYER_AWAY", "TENNIS_PLAYER_HOME"), " hit last")
			ePlayer = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_HIT_LAST(thisStatus)))
			CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[ePlayer], TPB_REDUCE_PREDICTION_CALC)
			VECTOR vFirstBounce
			vFirstBounce = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(
				thisProps.tennisCourt,
				GET_TENNIS_BALL_POS(thisProps.tennisBall), 
				thisProps.tennisBall.vBallVel,
				thisProps.vForward,
				GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), 
				GET_TENNIS_BALL_SPIN(thisProps.tennisBall), 
				GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
			SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[ePlayer], vFirstBounce)
			IF NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[ePlayer].playerAI, TAF_CHECK_REACTS)
				SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[ePlayer].playerAI, TAF_CHECK_REACTS)
			ENDIF
			
			IF GET_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[GET_TENNIS_STATUS_HIT_LAST(thisStatus)]) = SHOT_LOB		//If the shot it a lob, set the prediction and prep to the same point
				SET_AI_PREDICTION_POINT(thisProps.tennisPlayers[ePlayer].playerAI, vFirstBounce)
				CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY_PREDICT: Predicting Lob for player/AI ", PICK_STRING(ePlayer = TENNIS_PLAYER_AWAY, "TENNIS_PLAYER_AWAY", "TENNIS_PLAYER_HOME"))
			ELSE
				vPredictedBallPos = GET_CLOSEST_POINT_FOR_TENNIS_SWING(thisProps.tennisPlayers[ePlayer].vPos, thisProps.tennisCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall), 
					thisProps.tennisBall.vBallVel, thisProps.vForward, GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), GET_TENNIS_BALL_SPIN(thisProps.tennisBall), 
					GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall), fTimeToPrediction)
				SET_AI_PREDICTION_POINT(thisProps.tennisPlayers[ePlayer].playerAI, GET_BEST_TENNIS_PREDICTION_FROM_OFFSET(thisProps.tennisPlayers[ePlayer], thisProps.vSwingStateOffsets, 
					vPredictedBallPos, thisProps.tennisCourt.vCenterCourt.z, fTimeToPrediction))
				CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY_PREDICT: Predicting normal shot for ", thisProps.tennisPlayers[ePlayer].playerAI.texName)
			ENDIF
//			DEBUG_RECORD_THIS_ENTITY(thisProps.tennisPlayers[ePlayer].pIndex)

			//Predict where the tennis ball will land and if the ped is an AI tell them not to chase it if it is OOB
			//Do this after the bounce count check to make sure we're not checking where it lands after the bounce.
			IF NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[ePlayer].playerAI, TAF_CHECK_FOR_OOB)
			AND GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall) < 1 
			AND NOT IS_POINT_INSIDE_TENNIS_AREA(vFirstBounce, thisProps.tennisCourt.vCourtCorners[0] + (thisProps.vRight * 0.5), thisProps.tennisCourt.vCourtCorners[3] + (thisProps.vRight * 0.5), GET_TENNIS_COURT_WIDTH(thisProps.tennisCourt))
				CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY_PREDICT: Ball is out of bounds")
				IF GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[ePlayer]) = TSS_NO_SWING
					CPRINTLN(DEBUG_TENNIS, "Telling the AI to stop chasing the ball")
					SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[ePlayer].playerAI, TAF_CHECK_FOR_OOB)
				ENDIF
			ENDIF
			
			IF thisProps.tennisPlayers[ePlayer].controlType = COMPUTER
				
				//Adjusting the home positions based on the direction of the shot hit last, only applies for AI, doesn't need to be called for people.
				IF thisProps.tennisPlayers[ePlayer].eCourtSide = (TENNIS_PLAYER_AWAY)
					dummy = 0	// dummy is the corner used to adjust the home buckets
				ELIF thisProps.tennisPlayers[ePlayer].eCourtSide = (TENNIS_PLAYER_HOME)
					dummy = 2
				ELSE
					CPRINTLN(DEBUG_TENNIS, "TGSE RALLY: we're using an invalid eHitLast enum! Enum: ", GET_TENNIS_STATUS_HIT_LAST(thisStatus))
				ENDIF
				ADJUST_AI_HOME_POSITIONS_WITH_BUCKETS(thisProps.tennisPlayers[ePlayer], thisProps.tennisCourt.vCourtCorners[dummy], thisProps.tennisCourt.vServiceCorners[dummy])
			ELSE	//the ped is human so we want to display the x_marker for them.
				fMessageTimer = 0
				thisProps.tennisBall.iBounceCount = 0
				SET_BITMASK_AS_ENUM(iUtilFlags, TD_X_MARKER)
				CPRINTLN(DEBUG_TENNIS, "Telling the Bounce Marker to show and resetting gating variables")
			ENDIF
			//Clear the TAF_JUST_TASKED bitmask so the AI, if AI controlled, can run to the new point
			CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[ePlayer].playerAI, TAF_JUST_TASKED)
			SET_TENNIS_AI_STATE(thisProps.tennisPlayers[ePlayer].playerAI, ASE_AI_MOVING_TO_BALL)
			SET_TENNIS_GAME_STATE(thisStatus, TGSE_RALLY)
			CPRINTLN(DEBUG_TENNIS, "TAF_JUST_TASKED Cleared in TGSE_RALLY_PREDICT")
		BREAK
		
// ******************************************** Winner *********************************************************		
		CASE TGSE_POINT_WON
			TASK_TENNIS_PLAYER_WATCH_BALL(thisProps.tennisPlayers[eSelfID], thisProps.tennisBall, FALSE)
			TASK_TENNIS_PLAYER_WATCH_BALL(thisProps.tennisPlayers[eOtherID], thisProps.tennisBall, FALSE)
			SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eSelfID].playerAI, ASE_AI_IDLE)
			SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eOtherID].playerAI, ASE_AI_IDLE)
		
			CPRINTLN(DEBUG_TENNIS, "Bounce Count: ", GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall))
			INCREASE_TENNIS_POINTS_WON(thisStatus.playerScores[GET_TENNIS_STATUS_SCORED_LAST(thisStatus)], iUtilFlags)
			// Game won?
			IF thisStatus.playerScores[GET_TENNIS_STATUS_SCORED_LAST(thisStatus)].iPointsWon >= PICK_INT(IS_TENNIS_SET_ON_TIE_BREAK(thisStatus), 7, 4)
			AND thisStatus.playerScores[GET_TENNIS_STATUS_SCORED_LAST(thisStatus)].iPointsWon - thisStatus.playerScores[1 - ENUM_TO_INT(GET_TENNIS_STATUS_SCORED_LAST(thisStatus))].iPointsWon >= 2
				RECORD_TENNIS_GAME_WIN(thisStatus.playerScores, GET_TENNIS_STATUS_CURRENT_SET(thisStatus), (GET_TENNIS_STATUS_SCORED_LAST(thisStatus)), INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_STATUS_SCORED_LAST(thisStatus))), iUtilFlags, IS_TENNIS_SET_ON_TIE_BREAK(thisStatus))
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_CHANGE_SERVERS)
				SET_BITMASK_AS_ENUM(iUtilFlags, TD_GAME_DONE_STISH)
				SET_TENNIS_SET_TIE_BREAK(thisStatus, FALSE)
				IF thisProps.eTennisActivity = TA_AI_VS_AI
					SET_TENNIS_TIMER_A(thisProps, 0)
				ENDIF
				
				IF (GET_TENNIS_STATUS_SCORED_LAST(thisStatus)) = eSelfID
					SET_BITMASK_AS_ENUM(iUtilFlags, TD_GAME_WON)
					SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_PLR_HAPPY)
					IF thisProps.tennisPlayers[eSelfID].controlType <> COMPUTER
						TENNIS_INCREMENT_STAT(PS_STRENGTH, TENNIS_WIN_STRENGTH_INC)
						SET_BITMASK_AS_ENUM(iUtilFlags, TD_GAME_WON_DSTAR)
					ENDIF
					IF GET_AI_DIFFICULTY(thisProps.tennisPlayers[eOtherID].playerAI) = AD_HARD
						RESET_TENNIS_AI_BUCKETS(thisProps.tennisPlayers[eOtherID].playerAI)
					ENDIF
				ELSE
					SET_BITMASK_AS_ENUM(iUtilFlags, TD_GAME_LOST)
					SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_OPP_HAPPY)
					IF thisProps.tennisPlayers[eSelfID].controlType <> COMPUTER
						SET_BITMASK_AS_ENUM(iUtilFlags, TD_GAME_LOST_DSTAR)
					ENDIF
				ENDIF
				
			// Keep playing
			ELSE
				INT iPointToCheck, iCurrentSet
				TENNIS_PLAYER_ID eScoredLast, eScoredOn
				eScoredLast = GET_TENNIS_STATUS_SCORED_LAST(thisStatus)
				eScoredOn = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eScoredLast))
				iCurrentSet = GET_TENNIS_STATUS_CURRENT_SET(thisStatus)
				iPointToCheck = PICK_INT(IS_TENNIS_SET_ON_TIE_BREAK(thisStatus), 6, 3)
				CPRINTLN(DEBUG_TENNIS, "TGSE_POINT_WON: iPointToCheck: ", iPointToCheck)
				IF IS_TENNIS_SET_ON_TIE_BREAK(thisStatus)
					SET_TENNIS_PROPS_DELAY_NEXT_UI_TIME_STAMP(thisProps, GET_GAME_TIMER() + DELAY_NEXT_UI_DURATION)
				ENDIF
				
				IF thisStatus.playerScores[eScoredLast].iPointsWon >= iPointToCheck 
				AND thisStatus.playerScores[eScoredLast].iPointsWon = thisStatus.playerScores[eScoredOn].iPointsWon
					SET_BITMASK_AS_ENUM(iUtilFlags, TD_DEUCE)
					SET_TENNIS_INTERSTITIAL_FLAGS(eScoredLast, eSelfID, thisProps.tennisPlayers, iUtilFlags)
				ELIF thisStatus.playerScores[eScoredLast].iPointsWon > iPointToCheck 
				AND thisStatus.playerScores[eScoredLast].iPointsWon = thisStatus.playerScores[eScoredOn].iPointsWon + 1
					// Check for a match or set point here
					IF (GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_SETS
					AND thisStatus.playerScores[eScoredLast].iGamesWon[iCurrentSet] >= 5 
					AND (thisStatus.playerScores[eScoredLast].iGamesWon[iCurrentSet] + 1) - thisStatus.playerScores[eScoredOn].iGamesWon[iCurrentSet] >= 2)
					OR (GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_ONE_GAME)
					OR (GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_THREE_GAMES
					AND thisStatus.playerScores[eScoredLast].iGamesWon[iCurrentSet] >= 1)
					OR (GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_FIVE_GAMES
					AND thisStatus.playerScores[eScoredLast].iGamesWon[iCurrentSet] >= 2)
						// Game Point message
						SET_BITMASK_AS_ENUM(iUtilFlags, TD_GAME_POINT)
					ELSE
						SET_BITMASK_AS_ENUM(iUtilFlags, TD_ADVANTAGE)
					ENDIF
					SET_TENNIS_INTERSTITIAL_FLAGS(eScoredLast, eSelfID, thisProps.tennisPlayers, iUtilFlags)
				ELIF thisStatus.playerScores[eScoredLast].iPointsWon = iPointToCheck AND thisStatus.playerScores[eScoredOn].iPointsWon < iPointToCheck
					SET_BITMASK_AS_ENUM(iUtilFlags, TD_GAME_POINT)
					SET_TENNIS_INTERSTITIAL_FLAGS(eScoredLast, eSelfID, thisProps.tennisPlayers, iUtilFlags)
				ELSE
					IF thisStatus.ePrevGameState = TGSE_SERVE_RESULT	//Either the server faulted or the servee hit before a bounce
						IF thisStatus.eScoredLast <> GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)
							//Double Fault
							
							//These bitmasks are set here to make sure that the splash only comes up when the game isn't won
							SET_BITMASK_AS_ENUM(iUtilFlags, TD_FAULT)
							SET_BITMASK_AS_ENUM(iUtilFlags, TD_FAULT_DSTAR)
							
							IF (GET_TENNIS_STATUS_SCORED_LAST(thisStatus)) = eSelfID
								SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_OPP_SAD)
							ELSE
								SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_PLR_SAD)
							ENDIF
						ELSE
							//Servee hit twice
							CDEBUG1LN(DEBUG_TENNIS, "//Servee hit twice! Notify Rob Pearsall!")
							IF (eScoredLast) = eSelfID
								SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_PLR_HAPPY)
								SET_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_WON)
								SET_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_W_DSTAR)
							ELSE
								SET_TENNIS_ANIM_REACTION_FLAGS(iUtilFlags, TD_OPP_HAPPY)
								SET_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_L_DSTAR)
								SET_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_LOST)
							ENDIF
						ENDIF
					ELIF thisStatus.ePrevGameState = TGSE_RALLY
						IF (eScoredLast) = eSelfID
							CDEBUG2LN(DEBUG_TENNIS, "SET_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_WON)")
							SET_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_WON)
						ELSE
							CDEBUG2LN(DEBUG_TENNIS, "SET_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_LOST)")
							SET_BITMASK_AS_ENUM(iUtilFlags, TD_POINT_LOST)
						ENDIF
						SET_TENNIS_INTERSTITIAL_FLAGS(eScoredLast, eSelfID, thisProps.tennisPlayers, iUtilFlags)
					ELSE
						CPRINTLN(DEBUG_TENNIS, "ePrevGameState: ", thisStatus.ePrevGameState)
						CPRINTLN(DEBUG_TENNIS, "eGameState: ", GET_TENNIS_GAME_STATE(thisStatus))
						SCRIPT_ASSERT("TGSE_POINT_WON: Point was gained, we should have noted it. Contact Rob Pearsall.")
					ENDIF
				ENDIF
				// Clear the Ace bit if the point was from a double fault
				IF thisStatus.ePrevGameState = TGSE_SERVE_RESULT AND thisStatus.eScoredLast <> GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)
					CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_ACE)
					// Count the fault for social club stats
					IF NOT IS_BITMASK_AS_ENUM_SET(iUtilFlags, TD_FAULT)
						// Count the fault because we're not counting it when the UI comes up like normal, we're showing other UI
					ENDIF
				ENDIF
				IF IS_TENNIS_SET_ON_TIE_BREAK(thisStatus)
					IF IS_BIT_SET(thisStatus.playerScores[eSelfID].iPointsWon + thisStatus.playerScores[eOtherID].iPointsWon, 0)	// If the number of points is odd then we switch servers
						SET_TENNIS_GAME_STATE(thisStatus, TGSE_CHANGE_SERVERS)
						CPRINTLN(DEBUG_TENNIS, "Changing servers, composite score is odd")
					ELIF (thisStatus.playerScores[eSelfID].iPointsWon + thisStatus.playerScores[eOtherID].iPointsWon) % 6 = 0		// If a multiple of six points was won then we switch court sides
						SET_TENNIS_GAME_STATE(thisStatus, TGSE_SIDE_CHANGE)
						CPRINTLN(DEBUG_TENNIS, "Changing sides, 6 points have been won since last change")
					ELSE
						SET_TENNIS_SERVING_SIDE(thisStatus, INT_TO_ENUM(SERVING_SIDE_ENUM, 1 - ENUM_TO_INT(GET_TENNIS_SERVING_SIDE(thisStatus))))
						SET_TENNIS_GAME_STATE(thisStatus, TGSE_PREPARE_SERVE)
						CPRINTLN(DEBUG_TENNIS, "preparing next serve in tie break")
					ENDIF
				ELSE
					SET_TENNIS_SERVING_SIDE(thisStatus, INT_TO_ENUM(SERVING_SIDE_ENUM, 1 - ENUM_TO_INT(GET_TENNIS_SERVING_SIDE(thisStatus))))
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_PREPARE_SERVE)
					IF thisProps.eTennisActivity = TA_AI_VS_AI
						SET_TENNIS_TIMER_A(thisProps, 0)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_ANIM) OR IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eOtherID].playerAI, TAF_TASKED_ANIM)
				SET_BITMASK_AS_ENUM(iUtilFlags, TD_DELAY_THE_CUT_DELAY)
				CPRINTLN(DEBUG_TENNIS, "SET_BITMASK_AS_ENUM(iUtilFlags, TD_DELAY_THE_CUT_DELAY)")
			ENDIF
			
			CLEAR_TENNIS_PLAYER_CONTROL_BITS(thisProps.tennisPlayers[eSelfID])
			CLEAR_TENNIS_PLAYER_CONTROL_BITS(thisProps.tennisPlayers[eOtherID])
			
			CPRINTLN(DEBUG_TENNIS, "eSelfID Points Won: ", thisStatus.playerScores[eSelfID].iPointsWon)
			CPRINTLN(DEBUG_TENNIS, "eOtherID Points Won: ", thisStatus.playerScores[eOtherID].iPointsWon)
			CPRINTLN(DEBUG_TENNIS, "eSelfID Games Won: ", thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)])
			CPRINTLN(DEBUG_TENNIS, "eOtherID Games Won: ", thisStatus.playerScores[eOtherID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)])
		BREAK
		
// ******************************************** Switch *********************************************************	
		CASE TGSE_TENNIS_PLAYERS_WALKING_WAIT
			// Track the position of the players to move the state on
			IF ARE_TENNIS_PLAYERS_IN_POSITION(thisProps.tennisPlayers, eSelfID, eOtherID, GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), thisProps.eTennisActivity, IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION), iReceiverSeqProgress)
				ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, TRUE)
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_SERVE)
				CLEAR_TENNIS_AI_FLAGS_ALL(thisProps.tennisPlayers[eSelfID].playerAI)
				CLEAR_TENNIS_AI_FLAGS_ALL(thisProps.tennisPlayers[eOtherID].playerAI)
				IF IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
					CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_WALKING_TO_POSITION)
				ENDIF
				IF IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_SHOWN_SERVE_MESSAGE)
					CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_SHOWN_SERVE_MESSAGE)
				ENDIF
			ENDIF

			IF IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION) AND NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_BALL_RESET_IN_HAND) AND iServerSeqProgress >= 2
				SET_TENNIS_FLOW_FLAG(thisProps, TFF_BALL_RESET_IN_HAND)
				SET_TENNIS_PROPS_BALL_RESET_STAMP(thisProps, GET_GAME_TIMER() + ATTACH_TENNIS_BALL_STAMP)
			ELIF GET_GAME_TIMER() > GET_TENNIS_PROPS_BALL_RESET_STAMP(thisProps) AND IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_BALL_RESET_IN_HAND) AND DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND NOT IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
				DEBUG_SPAWN_BALL(thisProps)
				ATTACH_TENNIS_BALL_TO_PLAYER(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)], thisProps.tennisBall)
				CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_HIT_PLAYER)
			ENDIF
			
			IF GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus) <> eSelfID AND GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
				ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, TRUE)
				UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), eSelfID)
				HANDLE_TENNIS_RESET_FLAGS(TGSE_TENNIS_PLAYERS_WALKING_WAIT, thisProps.tennisPlayers[eSelfID], iUtilFlags, TRUE)
				UPDATE_TENNIS_PLAYER_SWINGING_AIMLESSLY(thisProps.tennisPlayers[eSelfID], thisProps.tennisBall)
			ENDIF
		BREAK
		
		CASE TGSE_CHANGE_SERVERS
		
			IF NOT IS_TENNIS_SET_ON_TIE_BREAK(thisStatus)
				// Was a set won?
				IF thisProps.eTennisActivity <> TA_AI_VS_AI
				AND ((thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] > 5 
						AND (thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)]) - thisStatus.playerScores[eOtherID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] >= 2
						AND thisStatus.playerScores[eSelfID].iPointsWon >= 3)
					OR (thisStatus.playerScores[eOtherID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] > 5
						AND (thisStatus.playerScores[eOtherID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)]) - thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] >= 2
						AND thisStatus.playerScores[eOtherID].iPointsWon >= 3)
					OR (thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] = 7)
					OR (thisStatus.playerScores[eOtherID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] = 7))
				//That was one, moderately big, IF statement
					//If there is more than 1 set then we need to zero out the scores to keep the scoreboard from looking weird.
					IF GET_TENNIS_STATUS_NUM_SETS(thisStatus) > 1
						INCREASE_CURRENT_TENNIS_SET(thisStatus.playerScores, thisStatus.iCurrentSet, iUtilFlags, GET_TENNIS_STATUS_NUM_SETS(thisStatus))
						CPRINTLN(DEBUG_TENNIS, "iCurrentSet increased")
					ENDIF
				ELIF (thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] = 6)
				AND (thisStatus.playerScores[eOtherID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] = 6)
				AND thisProps.eTennisActivity <> TA_AI_VS_AI
					SET_TENNIS_SET_TIE_BREAK(thisStatus, TRUE)
					SET_BITMASK_AS_ENUM(iUtilFlags, TD_SHOW_TIE_BREAK)
					SET_TENNIS_PROPS_DELAY_NEXT_UI_TIME_STAMP(thisProps, GET_GAME_TIMER() + DELAY_NEXT_UI_DURATION)
					CPRINTLN(DEBUG_TENNIS, "Entering Tie Break scenario")
				ENDIF
				
				// Do we have a Match Winner?
				IF thisProps.eTennisActivity <> TA_AI_VS_AI
				AND NOT IS_TENNIS_SET_ON_TIE_BREAK(thisStatus)
				AND (	(GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_SETS AND thisStatus.playerScores[eSelfID].iSetsWon > GET_TENNIS_STATUS_NUM_SETS(thisStatus)/2)
					OR 	(GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_SETS AND thisStatus.playerScores[eOtherID].iSetsWon > GET_TENNIS_STATUS_NUM_SETS(thisStatus)/2)
					OR	(GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_ONE_GAME AND thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] >= 1)
					OR 	(GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_ONE_GAME AND thisStatus.playerScores[eOtherID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] >= 1)
					OR	(GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_THREE_GAMES AND thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] >= 2)
					OR	(GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_THREE_GAMES AND thisStatus.playerScores[eOtherID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] >= 2)
					OR	(GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_FIVE_GAMES AND thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] >= 3)
					OR	(GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_FIVE_GAMES AND thisStatus.playerScores[eOtherID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] >= 3))
				//IF statement over. A very large IF statement.
					IF GET_TENNIS_STATUS_NUM_SETS(thisStatus) > 1
						thisStatus.iCurrentSet--
					ENDIF
					SET_TENNIS_PLAYER_TO_TENNIS_MODE(PLAYER_PED_ID(), FALSE)
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_OUTRO)
					SET_BITMASK_AS_ENUM(iUtilFlags, TD_UPDATE_SCORES)
				ELSE					
					RESET_TENNIS_POINTS_WON(thisStatus.playerScores, iUtilFlags)
					//swap servers
					IF (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)) = eSelfID
						SET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus, eOtherID)
					ELIF (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)) = eOtherID
						SET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus, eSelfID)
					ENDIF
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_PREPARE_SERVE)
					SET_TENNIS_SERVING_SIDE(thisStatus, SERVING_FROM_RIGHT)
				
					//Check for a side change here
					//Check to make sure it's not TA_AI_VS_AI as well
					IF TENNIS_SHOULD_SIDES_CHANGE(thisStatus.playerScores, GET_TENNIS_STATUS_CURRENT_SET(thisStatus)) AND thisProps.eTennisActivity <> TA_AI_VS_AI
						SET_TENNIS_GAME_STATE(thisStatus, TGSE_SIDE_CHANGE)
					ENDIF
				ENDIF
				
			ELIF IS_TENNIS_SET_ON_TIE_BREAK(thisStatus)
				//swap servers
				IF (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)) = eSelfID
					SET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus, eOtherID)
				ELIF (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)) = eOtherID
					SET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus, eSelfID)
				ENDIF
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_PREPARE_SERVE)
				SET_TENNIS_SERVING_SIDE(thisStatus, SERVING_FROM_RIGHT)
			ENDIF
		BREAK
		
		CASE TGSE_INTERSTITIAL
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eOtherID], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), eOtherID)
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), eSelfID)

			IF (thisProps.eTennisActivity <> TA_AI_VS_AI AND NOT IS_BITMASK_AS_ENUM_SET(iUtilFlags, TD_ANIM_PLAYING))
			OR thisProps.eTennisActivity = TA_AI_VS_AI
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_PREPARE_SERVE)
			ENDIF
		BREAK
		
		//This is a test state used for proving out ideas outside the course of a normal game
		//Enter this state with Z on the keyboard, exit with Z as well.
		CASE TGSE_TEST_STATE
			UNUSED_PARAMETER( EX )
			UNUSED_PARAMETER( WHY )
			
			#IF IS_DEBUG_BUILD
//			SET_TENNIS_COURT_VARS_OFF_CORNERS(thisProps.tennisCourt, thisProps, DEBUG_COURT_FRAME_MULTIPLIER, DEBUG_COURT_SERVICE_RATIO)
			IF thisProps.tennisWidgets.bTest_PlayerResetFlagUseCamHeading
				SET_PED_RESET_FLAG(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), PRF_ForcePedToUseScripCamHeading, TRUE)	// we don't use script heading for interstitials?
			ENDIF
			IF thisProps.tennisWidgets.bTest_PlayerResetFlagStrafe
				SET_PED_RESET_FLAG(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), PRF_ForcePedToStrafe, TRUE)
			ENDIF
			IF thisProps.tennisWidgets.bTest_AIResetFlagUseCamHeading
				SET_PED_RESET_FLAG(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), PRF_ForcePedToUseScripCamHeading, TRUE)
			ENDIF
			IF thisProps.tennisWidgets.bTest_AIResetFlagStrafe
				SET_PED_RESET_FLAG(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), PRF_ForcePedToStrafe, TRUE)
			ENDIF
			
			GET_ANALOG_STICK_VALUES(EX, WHY, dummy, dummy)
			
			HANDLE_TENNIS_PLAYER_OUTSIDE_PLAYING_AREA(thisProps.tennisPlayers[eSelfID], thisProps.tennisCourt)
			
			IF GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall) < 3 AND HAS_BALL_BOUNCED(thisProps.tennisBall, GET_TENNIS_FRAME_COUNTER(thisProps), thisProps.tennisCourt.vCenterCourt)
				INCREASE_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
			ENDIF
			
			DRAW_DEBUG_CROSS((<< -1178.0740, -1633.2863, 3.38 >> + thisProps.vRight * TUT_STAGE_SHIFT), 1, 0, 0, 0, 255)
			DRAW_DEBUG_SPHERE((<< TEST_X, TEST_Y, TEST_Z>>), TEST_RADIUS, 0, 255, 255, 155)
//			DRAW_DEBUG_POLY_ANGLED_AREA((<<-2860.7622, 13.3794, 10.6083>>), (<<-2924.0659, 31.5452, 10.6083>>), 30.0)
						
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT)
				CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_TENNIS_SWING_EVENT_FLAG)
				SET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID], INT_TO_ENUM(TENNIS_SWING_STATES, TEST_SWING_STATE))
				SET_TENNIS_PLAYER_DIVE_SELECTION(thisProps.tennisPlayers[eSelfID], TENNIS_DIVE_DiveLeft_TopLeft)
				SET_CONTROL_SHAKE(PLAYER_CONTROL, SHAKE_DURATION_SLIDE, SHAKE_DURATION_FREQUENCY)
			ENDIF
			
//			ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eSelfID, eOtherID, TRUE)
//			VECTOR vAdd, vEnd1, vEnd2
//			FLOAT fDist1, fDist2, fDistDiff	
//			vAdd = (thisProps.tennisCourt.vCourtCorners[1] - thisProps.tennisCourt.vCourtCorners[0]) * 0.5
//			vEnd1 = thisProps.tennisCourt.vCourtCorners[0] + vAdd
//			vEnd2 = thisProps.tennisCourt.vCourtCorners[3] + vAdd
//			fDist1 = VDIST(vEnd1, thisProps.tennisPlayers[eOtherID].vPos)
//			fDist2 = VDIST(vEnd2, thisProps.tennisPlayers[eOtherID].vPos)
//			fDistDiff = ABSF(fDist1 - fDist2)
//			DRAW_DEBUG_SPHERE(vEnd1, 0.5)
//			DRAW_DEBUG_SPHERE(vEnd2, 0.5)
//			CPRINTLN(DEBUG_TENNIS, "fDist1=", fDist1, ", fDist2=", fDist2, ", fDistDiff=", fDistDiff)
			
			BOOL bTestFlag
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RUP)
				TEST_HIT(thisProps.tennisPlayers[eSelfID], thisProps, thisStatus, eSelfID)
				RESET_TENNIS_PLAYER_SWING_TIMER(thisProps.tennisPlayers[eSelfID])
				RESET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
				bTestFlag = TRUE
			ENDIF
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
				TEST_HIT(thisProps.tennisPlayers[eOtherID], thisProps, thisStatus, eOtherID)
				RESET_TENNIS_PLAYER_SWING_TIMER(thisProps.tennisPlayers[eSelfID])
				RESET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
				bTestFlag = TRUE
			ENDIF
			
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), eOtherID)
			UPDATE_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eSelfID], thisProps, thisStatus.playerScores, GET_TENNIS_STATUS_HIT_LAST(thisStatus), eSelfID, GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), GET_TENNIS_SERVING_SIDE(thisStatus), IS_TENNIS_SET_ON_TIE_BREAK(thisStatus))
			HANDLE_TENNIS_ANIMS(thisProps.tennisPlayers[eSelfID], thisProps.phasesArray, thisProps.eTennisActivity)
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eOtherID], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), eOtherID)
			UPDATE_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eOtherID], thisProps, thisStatus.playerScores, GET_TENNIS_STATUS_HIT_LAST(thisStatus), eOtherID, GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), GET_TENNIS_SERVING_SIDE(thisStatus), IS_TENNIS_SET_ON_TIE_BREAK(thisStatus))
			HANDLE_TENNIS_ANIMS(thisProps.tennisPlayers[eOtherID], thisProps.phasesArray, thisProps.eTennisActivity)
			
			//Set Prediction after the ball has been hit.
			IF bTestFlag
				CDEBUG2LN(DEBUG_TENNIS, "TGSE_TEST_STATE :: bTestFlag=TRUE")
				VECTOR vDestTest
				vDestTest = GET_CLOSEST_POINT_FOR_TENNIS_SWING(
					thisProps.tennisPlayers[eOtherID].vPos,
					thisProps.tennisCourt,
					GET_TENNIS_BALL_POS(thisProps.tennisBall), 
					thisProps.tennisBall.vBallVel,
					thisProps.vForward,
					GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), 
					GET_TENNIS_BALL_SPIN(thisProps.tennisBall), 
					GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall), fTimeToPrediction)
				SET_AI_PREDICTION_POINT(thisProps.tennisPlayers[eOtherID].playerAI, GET_BEST_TENNIS_PREDICTION_FROM_OFFSET(
					thisProps.tennisPlayers[eOtherID], 
					thisProps.vSwingStateOffsets, 
					vDestTest,
					thisProps.tennisCourt.vCenterCourt.z, fTimeToPrediction))
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
				// Print out the matrix of swings by bucket in a way that can be copied and pasted into script.
//				INT iSwing, iBucket
//				REPEAT MAX_SWING_BUCKETS iBucket
//					REPEAT ENUM_TO_INT(TSS_MAX_SWINGS) iSwing
//						IF thisProps.vSwingStateOffsets[iSwing].x >= thisProps.swingBucketSteps[iBucket]
//						AND thisProps.vSwingStateOffsets[iSwing].x < thisProps.swingBucketSteps[iBucket + 1]
//							CPRINTLN(DEBUG_TENNIS, "thisProps.swingBuckets[", iBucket, "][] = ", GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, iSwing)))	//, ", x=", thisProps.vSwingStateOffsets[iSwing].x)
//						ENDIF
//					ENDREPEAT
//					CPRINTLN(DEBUG_TENNIS, "")
//				ENDREPEAT
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
				TASK_GO_STRAIGHT_TO_COORD(thisProps.tennisPlayers[eSelfID].pIndex, thisProps.tennisCourt.vCourtCorners[0], PEDMOVE_RUN)
				CDEBUG2LN(DEBUG_TENNIS, "INPUT_SCRIPT_PAD_UP :: TASK_GO_STRAIGHT_TO_COORD called")
//				PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[eOtherID], "FAC_LEAVE_TENNIS_WON", GET_TENNIS_PLAYER_VOICE(thisProps.tennisPlayers[eOtherID]), SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
			ENDIF
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
				PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[eOtherID], "FAC_LEAVE_TENNIS_QUIT", GET_TENNIS_PLAYER_VOICE(thisProps.tennisPlayers[eOtherID]), SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
			ENDIF
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
				PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[eOtherID], "FAC_LEAVE_TENNIS_LOST", GET_TENNIS_PLAYER_VOICE(thisProps.tennisPlayers[eOtherID]), SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
			ENDIF
			
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	IF NOT IS_BITMASK_AS_ENUM_SET(iUtilFlags, TD_ANIM_PLAYING) 
	AND GET_TENNIS_GAME_STATE(thisStatus) > TGSE_OPTION_MENU 
	AND GET_TENNIS_GAME_STATE(thisStatus) < TGSE_QUIT_CONFIRM
		HANDLE_TENNIS_ANIMS(thisProps.tennisPlayers[eSelfID], thisProps.phasesArray, thisProps.eTennisActivity)
		HANDLE_TENNIS_ANIMS(thisProps.tennisPlayers[eOtherID], thisProps.phasesArray, thisProps.eTennisActivity)
	ENDIF
	
	COMMON_TENNIS_END_OF_FRAME_DATA(thisProps)
ENDPROC

/// PURPOSE:
///    Handles things like onscreen UI and running cutscenes
/// PARAMS:
///    thisUIData - 
///    thisProps - 
///    thisStatus - 
///    thisSPData - 
///    eSelfID - 
PROC PROCESS_TENNIS_UI_DATA(TENNIS_GAME_UI_DATA &thisUIData, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATUS &thisStatus, TENNIS_GAME_SP_DATA &thisSPData, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
	VECTOR vTemp
	FLOAT fReactAdd
	STRING sDict
	VECTOR vStartCamPos, vStartCamRot, vOldCamRot
	TEXT_LABEL_63 sSceneDict
	TEXT_LABEL_31 sScenePlrAnim
	TEXT_LABEL_31 sSceneCamAnim
	
	IF thisProps.thisLocation = ALOC_tennis_beachCourt AND GET_TENNIS_GAME_STATE(thisStatus) >= TGSE_PREPARE_SERVE AND GET_TENNIS_GAME_STATE(thisStatus) < TGSE_POINT_WON
		HANDLE_TENNIS_ABUSE_FROM_PEDS(thisSPData, thisProps.tennisPlayers[eSelfID].eCourtSide)
	ENDIF
	
	SWITCH GET_TENNIS_GAME_STATE(thisStatus)
	
		CASE TGSE_FRIEND_INIT_WAIT
			//("Waiting a frame to remove the friend ped")
			SET_TENNIS_GAME_STATE(thisStatus, TGSE_INIT)
		BREAK
	
		CASE TGSE_INIT
			IF HAVE_REQUEST_TENNIS_ASSETS_LOADED(thisProps) //AND HAVE_HUMAN_DATA_ASSETS_LOADED(thisUIData)
				IF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY
				ELIF thisProps.eTennisActivity = TA_PLAYER_VS_AI
					SET_PED_LOD_MULTIPLIER(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), DEBUG_LOD_SETTING)
					
					IF NOT IS_PED_INJURED(thisProps.tennisPlayers[eOtherID].pIndex) AND NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
						STRING sName
						IF IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherID])
							sName = PICK_STRING(IS_PED_COMPONENT_VARIATION_VALID(thisProps.tennisPlayers[eOtherID].pIndex,INT_TO_ENUM(PED_COMPONENT,0),1,0), "OPP_NAME_3", "OPP_NAME_4")
						ELSE
							sName = PICK_STRING(IS_PED_COMPONENT_VARIATION_VALID(thisProps.tennisPlayers[eOtherID].pIndex,INT_TO_ENUM(PED_COMPONENT,0),1,0), "OPP_NAME_2", "OPP_NAME_1")
						ENDIF
						SET_TENNIS_PLAYER_NAME(thisUIData, eOtherID, sName)
					ENDIF
					
					IF IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
						CPRINTLN(DEBUG_TENNIS, "Setting NPC Amanda to player's relgroup")
						PRIVATE_SetDefaultFamilyMemberAttributes(thisProps.tennisPlayers[1].pIndex, RELGROUPHASH_PLAYER)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[0]))
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[0]))
				ENDIF
				IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1]))
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1]))
				ENDIF
				
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_INTRO)
			ENDIF
		BREAK
		
		CASE TGSE_INTRO
			IF thisProps.eTennisActivity <> TA_AI_VS_AI
	
				IF GET_TENNIS_CUTSCENE_STATE(thisUIData.tennisCameras) > CUT_INIT 
				AND (IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR GET_TIMER_IN_SECONDS(thisUIData.tennisCameras.tennisCamTimer) > CUT_TIMEOUT) 
				AND HAVE_HUMAN_DATA_ASSETS_LOADED(thisUIData)
					//KILL_ANY_CONVERSATION()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
												
					SET_TENNIS_CUTSCENE_STATE(thisUIData.tennisCameras, CUT_FINISH)
				ENDIF
				
				FLOAT fSyncPhase
				IF IS_SYNCHRONIZED_SCENE_RUNNING(thisUIData.iSceneID)
					fSyncPhase = GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID)
//					CDEBUG2LN(DEBUG_TENNIS, "frame=", GET_FRAME_COUNT(), ", fSyncPhase=", fSyncPhase)
				ENDIF
				
				IF ( IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
					OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE) )
				AND NOT HAS_TENNIS_CHARACTER_SEEN_TUTORIAL()
				AND GET_TENNIS_TUTORIAL_STATE(thisUIData.tennisCameras) >= TUT_DEMO_HIT_TOWARD_PED
					CDEBUG2LN(DEBUG_TENNIS, "IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RDOWN) :: FINISH_TENNIS_TUTORIAL(thisUIData.tennisCameras)")
					FINISH_TENNIS_TUTORIAL(thisUIData.tennisCameras)
				ENDIF
				
				IF NOT HAS_TENNIS_CHARACTER_SEEN_TUTORIAL()
					IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND NOT IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall) AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
						INT iBounces 
						VECTOR vBallPos
						iBounces = GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
						vBallPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
						UPDATE_BALL_SIMULATOR(
							thisProps.tennisCourt, 
							vBallPos,
							thisProps.tennisBall.vBallVel,
							thisProps.vForward,
							thisProps.tennisBall.fFlightTimer,
							thisProps.tennisBall.eBallSpin,
							thisProps.tennisBall.fSpinTimer,
							iBounces, IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_SOUND_ON_FOR_TUT))
						SET_TENNIS_BALL_POS(thisProps.tennisBall, vBallPos)
						SET_ENTITY_COORDS(thisProps.tennisBall.oBall, GET_TENNIS_BALL_POS(thisProps.tennisBall))
						SET_ENTITY_VELOCITY(thisProps.tennisBall.oBall, thisProps.tennisBall.vBallVel)
					ELSE
						IF DOES_TENNIS_BALL_TRAIL_EXIST(thisProps.tennisBall)
							STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
						ENDIF
					ENDIF
					
					HANDLE_TENNIS_ANIMS(thisProps.tennisPlayers[eOtherID], thisProps.phasesArray, thisProps.eTennisActivity)
					COMMON_EVERY_FRAME_DATA(thisProps, GET_TENNIS_GAME_STATE(thisStatus), thisUIData.iUtilFlags, eSelfID, eOtherID)
					HANDLE_TENNIS_RESET_FLAGS(GET_TENNIS_GAME_STATE(thisStatus), thisProps.tennisPlayers[eOtherID], thisUIData.iUtilFlags, TRUE)
					HANDLE_BITMASK_UI(thisUIData, thisProps, thisStatus.playerScores,
						GET_TENNIS_MATCH_RULESET(thisStatus),
						GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus),
						GET_TENNIS_STATUS_SCORED_LAST(thisStatus),
						GET_TENNIS_SERVE_COUNT(thisStatus),
						GET_TENNIS_STATUS_NUM_SETS(thisStatus),
						GET_TENNIS_STATUS_CURRENT_SET(thisStatus),
						eSelfID, eOtherID)
						
//					IF NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMBIENT_SHUTDOWN)
//						SET_TENNIS_GLOBAL_FLAG(TGF_AMBIENT_SHUTDOWN)
//					ENDIF
				ENDIF
				
				IF (HAS_TENNIS_CHARACTER_SEEN_TUTORIAL() AND fSyncPhase > 0.99) // AND TENNIS_PLAY_INTRO_CUTSCENE(thisUIData.tennisCameras, thisProps, thisUIData, thisSPData, GET_TENNIS_SERVING_SIDE(thisStatus), GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), eSelfID, eOtherID, GET_TENNIS_STATUS_REMATCH(thisStatus)))
				OR (NOT HAS_TENNIS_CHARACTER_SEEN_TUTORIAL() AND NOT IS_TENNIS_PLAYING_TUTORIAL_CUTSCENE(thisUIData.tennisCameras, thisProps, thisUIData, thisSPData, eSelfID, eOtherID))
					IF HAS_TENNIS_CHARACTER_SEEN_TUTORIAL()
						CLEAR_PED_TASKS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
						CLEAR_PED_TASKS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
						
						SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), 
							GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, SERVING_FROM_RIGHT, thisProps.vRight, thisProps.vForward, TRUE, eSelfID))
						thisProps.tennisPlayers[eSelfID].vPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
						SET_ENTITY_HEADING(thisProps.tennisPlayers[eSelfID].pIndex, GET_HEADING_FROM_VECTOR_2D(thisProps.vForward.x,thisProps.vForward.y))
						
						IF DOES_ENTITY_EXIST(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
							SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), 
								GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, SERVING_FROM_RIGHT, thisProps.vRight, thisProps.vForward, FALSE, eOtherID), TRUE, TRUE, TRUE)
							thisProps.tennisPlayers[eOtherID].vPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
							SET_ENTITY_HEADING(thisProps.tennisPlayers[eOtherID].pIndex, GET_HEADING_FROM_VECTOR_2D(-thisProps.vForward.x,-thisProps.vForward.y))
							SET_PED_LOD_MULTIPLIER(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), DEBUG_LOD_SETTING)
							sDict = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherID]), "mini@tennis", "mini@tennis@female")
							IF NOT IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), sDict, "intro_a")
								TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), sDict, "intro_a", default, default, default, AF_LOOPING)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), TRUE)
							ENDIF
							sDict = PICK_STRING(IS_TENNIS_PED_MALE((thisProps.tennisPlayers[eSelfID])), "mini@tennis", "mini@tennis@female")
							IF NOT IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), sDict, "intro_b")
								TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), sDict, "intro_b", default, default, default, AF_LOOPING)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), TRUE)
							ENDIF
						ENDIF
						
						IF NOT DOES_ENTITY_EXIST( thisSPData.oLighting )
							TENNIS_CREATE_LIGHTING_PROP( thisSPData, thisProps )
						ENDIF
						
						SETUP_TENNIS_COURT_SHADOWS()
						
						IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[eSelfID].oTennisRacket)
							DELETE_OBJECT(thisProps.tennisPlayers[eSelfID].oTennisRacket)
						ENDIF
						IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[eOtherID].oTennisRacket)
							DELETE_OBJECT(thisProps.tennisPlayers[eOtherID].oTennisRacket)
						ENDIF
						
						thisProps.tennisPlayers[eSelfID].oTennisRacket = CREATE_OBJECT(PROP_TENNIS_RACK_01B, thisProps.tennisCourt.vCourtCorners[3])
						SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eSelfID].oTennisRacket, TRUE)
						thisProps.tennisPlayers[eOtherID].oTennisRacket = CREATE_OBJECT(PROP_TENNIS_RACK_01B, thisProps.tennisCourt.vCourtCorners[2])
						SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eOtherID].oTennisRacket, TRUE)
						
						ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eSelfID, eOtherID)
						
						IF DOES_CAM_EXIST(thisUIData.tennisCameras.tennisCamTut)
							DESTROY_CAM(thisUIData.tennisCameras.tennisCamTut)
						ENDIF
						
						thisUIData.tennisCameras.tennisCamTut = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
						
						UPDATE_TENNIS_WANDERING_MENU_CAMERA(thisProps, thisUIData)
					ENDIF
					
					SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), FALSE)
					SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), FALSE)
					CPRINTLN(DEBUG_TENNIS, "SET_FORCE_FOOTSTEP_UPDATE FALSE")
					
					ADJUST_TENNIS_CAMERA_DYNAMIC(thisUIData.tennisCameras.tennisCamDyna,	// Prime the dynamic camera after intro
												GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt),
												PICK_VECTOR(thisProps.tennisPlayers[eSelfID].eCourtSide = (TENNIS_PLAYER_AWAY), 
												thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisCourt.vCourtCorners[2]),
												thisProps.tennisPlayers[eSelfID].vMyRight, 
												thisProps.tennisPlayers[eSelfID].vMyForward,
												GET_TENNIS_BALL_POS(thisProps.tennisBall) - thisProps.tennisCourt.vCenterCourt,
												thisUIData.tennisCameras.fDynaCamSwing, DEFAULT_DYNA_DIST,
												thisUIData.tennisCameras.fDynaDolly,
												thisUIData.tennisCameras.fDynaNudgeRSLR, thisUIData.tennisCameras.fDynaNudgeRSUD, FALSE, TRUE)
					
					SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(thisProps.tennisPlayers[eSelfID].oTennisRacket, TRUE)
					SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(thisProps.tennisPlayers[eOtherID].oTennisRacket, TRUE)
					SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_SET_INIT)
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_OPTION_MENU)
					SET_MULTIHEAD_SAFE(FALSE)		//B* 2152527
					RESET_CUTSCENE_STATE(thisUIData.tennisCameras)
					INIT_ALL_TENNIS_UI(thisUIData.tennisUI)
					
				ELIF HAS_TENNIS_CHARACTER_SEEN_TUTORIAL()
					
					REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
					
					IF fSyncPhase > 0.20 AND NOT IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_INTRO_PLYR_RETORT)
						SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_INTRO_PLYR_RETORT)
						STRING sContext
						sContext = PICK_STRING(thisUIData.playerChars[eSelfID] = CHAR_MICHAEL, "MGTN_introQM", "MGTN_introQT")
						CREATE_CONVERSATION(thisUIData.myDialogueStruct, thisUIData.sSubtitleGroupID, sContext, CONV_PRIORITY_HIGH)
						CDEBUG2LN(DEBUG_TENNIS, "CREATE_CONVERSATION seen tutorial dialogue, sContext=", sContext)
					ELIF fSyncPhase > 0.42 AND NOT IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_INTRO_OPP_HECKLE) AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_INTRO_OPP_HECKLE)
						PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[eOtherID], "GAME_HECKLE", thisProps.tennisPlayers[eOtherID].texVoice, SPEECH_PARAMS_FORCE_SHOUTED)
					ENDIF
					
				ENDIF
				
				IF NOT HAS_TENNIS_CHARACTER_SEEN_TUTORIAL()
					COMMON_TENNIS_END_OF_FRAME_DATA(thisProps)
				ENDIF
			ENDIF
		BREAK
		
		CASE TGSE_OPTION_MENU
			HANDLE_TENNIS_QUIT_INPUT(thisProps, thisStatus, thisUIData, eSelfID)
			IF GET_TENNIS_UI_STATE(thisUIData) = TENNIS_UI_HELP_SERVE OR GET_TENNIS_UI_STATE(thisUIData) = TENNIS_UI_STR_INFO
				SETTIMERB(0)
				SET_TENNIS_GAME_STATE(thisStatus, TGSE_OPTION_FINISHED)
			ENDIF
			PROCESS_TENNIS_UI(thisProps, thisStatus, thisUIData, thisProps.tennisPlayers[eSelfID], eSelfID, eOtherID)
		BREAK
		
		CASE TGSE_OPTION_FINISHED
			CPRINTLN(DEBUG_TENNIS, "TGSE_OPTION_FINISHED: Enabling Tennis Mode")
			SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]),TRUE)
			SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]),TRUE)
			IF IS_TENNIS_SAVED_GLOBAL_FLAG_SET(g_savedGlobals.sTennisData, TGF_DEFAULT_CAM_DYNA)
				SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_DYNAMIC)
			ELSE
				SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_FAR)
			ENDIF
			CPRINTLN(DEBUG_TENNIS, "Setting to game cam after options")
			
			#IF IS_TENNIS_MULTIPLAYER
				SET_TENNIS_GAME_FIRST_SERVE(thisStatus, TENNIS_PLAYER_HOME)
				CPRINTLN(DEBUG_TENNIS, "Tennis is Multiplayer and we're sending in TENNIS_PLAYER_HOME to serve")
			#ENDIF
			#IF NOT IS_TENNIS_MULTIPLAYER
				SET_TENNIS_GAME_FIRST_SERVE(thisStatus, INT_TO_ENUM(TENNIS_PLAYER_ID, eSelfID), thisUIData.iUtilFlags)
				CPRINTLN(DEBUG_TENNIS, "Tennis is Singleplayer and we're sending in: ", thisProps.tennisPlayers[eSelfID].playerAI.texName, " to serve")
			#ENDIF
		BREAK
		
		CASE TGSE_SERVE_RESULT
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_UI_POST_SERVE)
				SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_PLAYING)
				SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_UI_POST_SERVE)
			ENDIF
		BREAK
		
		CASE TGSE_SERVE
			HANDLE_TENNIS_QUIT_INPUT(thisProps, thisStatus, thisUIData, eSelfID)
			
			UPDATE_TENNIS_UI_POINTS(thisUIData, 
				GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), 
				thisStatus.playerScores, 
				GET_TENNIS_STATUS_NUM_SETS(thisStatus), 
				GET_TENNIS_STATUS_CURRENT_SET(thisStatus), 
				eSelfID, eOtherID, 
				IS_TENNIS_SET_ON_TIE_BREAK(thisStatus))		
			
			IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SECOND_SERVE)
				SHOW_TENNIS_SECOND_SERVE_MESSAGE(thisUIData.tennisUI)
				CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SECOND_SERVE)
			ENDIF
		BREAK
		
		CASE TGSE_PREPARE_SERVE
			HANDLE_TENNIS_QUIT_INPUT(thisProps, thisStatus, thisUIData, eSelfID)
			UPDATE_TENNIS_UI_POINTS(thisUIData, 
				GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), 
				thisStatus.playerScores, 
				GET_TENNIS_STATUS_NUM_SETS(thisStatus), 
				GET_TENNIS_STATUS_CURRENT_SET(thisStatus), 
				eSelfID, eOtherID, 
				IS_TENNIS_SET_ON_TIE_BREAK(thisStatus))
			
			IF NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
				IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_PAUSE_THE_CUT)
					CPRINTLN(DEBUG_TENNIS, "TD_PAUSE_THE_CUT is set, Setting TD_ANIM_PLAYING")
					IF IS_TIMER_STARTED(thisUIData.tennisUI.sUITimer) OR IS_TIMER_PAUSED(thisUIData.tennisUI.sUITimer)
						RESTART_TIMER_NOW(thisUIData.tennisUI.sUITimer)
						CPRINTLN(DEBUG_TENNIS, "Restarting the timer and pausing the cut")
					ENDIF
					SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)		//We set the anim_playing mask to prevent the game from going onward.
					CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PAUSE_THE_CUT)
					SETUP_TENNIS_PLAYING_CONTROLS(thisUIData.tennisUI, FALSE, VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(thisStatus.playerScores, GET_TENNIS_STATUS_CURRENT_SET(thisStatus), GET_TENNIS_STATUS_REMATCH(thisStatus)), GET_TENNIS_STATUS_NUM_SETS(thisStatus))
				ENDIF
			
				IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SWING_METER_POS_SET)
					CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SWING_METER_POS_SET)
				ENDIF
				
				fReactAdd = PICK_FLOAT(IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_DELAY_THE_CUT_DELAY), CUT_DELAY_ANIM_ADD, 0.0)
				
				IF IS_TIMER_STARTED(thisUIData.tennisUI.sUITimer) AND GET_TIMER_IN_SECONDS(thisUIData.tennisUI.sUITimer) > CUT_DELAY + fReactAdd
					HANDLE_TENNIS_INTERSTITIALS(thisProps.tennisCourt, thisProps, thisUIData, thisStatus, eSelfID, eOtherID)
					CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PAUSE_DIALOGUE)
				ENDIF
			ENDIF
			BOOL bDoneDivingReacting
			bDoneDivingReacting = GET_GAME_TIMER() > GET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisProps.tennisPlayers[eSelfID]) AND GET_GAME_TIMER() > GET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisProps.tennisPlayers[eOtherID])
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING) AND bDoneDivingReacting
				HANDLE_TENNIS_RESET_FLAGS(GET_TENNIS_GAME_STATE(thisStatus), thisProps.tennisPlayers[eSelfID], thisUIData.iUtilFlags, TRUE)
				HANDLE_TENNIS_RESET_FLAGS(GET_TENNIS_GAME_STATE(thisStatus), thisProps.tennisPlayers[eOtherID], thisUIData.iUtilFlags, TRUE)
				UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, SERVING_FROM_RIGHT, eSelfID)
				CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_PLAYER_CARD_SHOULD_SHOW)
				
				IF ((GET_RANDOM_FLOAT_IN_RANGE() <= RALLY_CONVO_RATE OR IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_MATCH_POINT_DSTAR))
				OR thisStatus.ePrevGameState = TGSE_CHANGE_SERVERS)
					IF eSelfID = (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus))
						SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_SERVE_DSTAR)
						CPRINTLN(DEBUG_TENNIS, "Setting the bitmask to play a serve taunt")
					ELIF eOtherID = GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus) AND thisUIData.playerChars[eOtherID] = CHAR_AMANDA
						IF thisUIData.playerChars[eSelfID] = CHAR_MICHAEL
							SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_AMANDA_SERVE_TO_MICHAEL)
						ELIF thisUIData.playerChars[eSelfID] = CHAR_TREVOR
							SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_AMANDA_SERVE_TO_TREVOR)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//If this is the first serve of the game show the serve message
			IF (thisStatus.ePrevGameState = TGSE_CHANGE_SERVERS OR thisStatus.ePrevGameState = TGSE_OPTION_FINISHED OR thisStatus.ePrevGameState = TGSE_SIDE_CHANGE)
				IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
					IF NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
						SHOW_TENNIS_SERVE_MESSAGE(thisUIData.tennisUI, GET_TENNIS_PLAYER_NAME(thisUIData, GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)))
					ELSE
						SET_TENNIS_FLOW_FLAG(thisProps, TFF_HANDLE_NEW_SERVE_MSG)
					ENDIF
					IF NOT IS_TENNIS_SET_ON_TIE_BREAK(thisStatus)
						RESET_TENNIS_DEUCE_COUNT(thisUIData)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				//Show the serve help if no anim is playing and we're serving
				IF (GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)) = eSelfID AND GET_TENNIS_UI_STATE(thisUIData) <> TENNIS_UI_STR_INFO
					SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_HELP_SERVE)
					CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_UI_POST_SERVE)
					SETUP_TENNIS_SERVING_CONTROLS(thisUIData.tennisUI, FALSE, VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(thisStatus.playerScores, GET_TENNIS_STATUS_CURRENT_SET(thisStatus), GET_TENNIS_STATUS_REMATCH(thisStatus)), GET_TENNIS_STATUS_NUM_SETS(thisStatus))
				ENDIF
			
				SET_TENNIS_RALLY_COUNTER(thisUIData, 0)
				//This part makes sure that the timer is reset in the case of a fault. It makes the serve meter start at zero again if we're the serving player
				IF GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus) = eSelfID AND NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
					RESTART_TIMER_NOW(thisUIData.tennisUI.sUITimer)
					TENNIS_PAUSE_TIMER(thisUIData.tennisUI.sUITimer)
				ENDIF
			ENDIF
		BREAK
		
		CASE TGSE_TENNIS_PLAYERS_WALKING_WAIT
			// Handle stuff skipped by side change
			IF IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_HANDLE_UI_AFTER_SIDE_CHANGE)
				CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_HANDLE_UI_AFTER_SIDE_CHANGE)
				SHOW_TENNIS_SERVE_MESSAGE(thisUIData.tennisUI, GET_TENNIS_PLAYER_NAME(thisUIData, GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)))
				SET_TENNIS_RALLY_COUNTER(thisUIData, 0)
				IF NOT IS_TENNIS_SET_ON_TIE_BREAK(thisStatus)
					RESET_TENNIS_DEUCE_COUNT(thisUIData)
				ENDIF
			ENDIF
			// Show serve message when walking back to position after a side change/change server, happens during tiebreak
			IF IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_HANDLE_NEW_SERVE_MSG) 
			AND NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_SHOWN_SERVE_MESSAGE)
			AND GET_GAME_TIMER() > GET_TENNIS_PROPS_DELAY_NEXT_UI_TIME_STAMP(thisProps)
				CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_HANDLE_NEW_SERVE_MSG)
				SET_TENNIS_FLOW_FLAG(thisProps, TFF_SHOWN_SERVE_MESSAGE)
				SHOW_TENNIS_SERVE_MESSAGE(thisUIData.tennisUI, GET_TENNIS_PLAYER_NAME(thisUIData, GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)))
			ENDIF
			
			UPDATE_TENNIS_UI_POINTS(thisUIData, 
				GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), 
				thisStatus.playerScores, 
				GET_TENNIS_STATUS_NUM_SETS(thisStatus), 
				GET_TENNIS_STATUS_CURRENT_SET(thisStatus), 
				eSelfID, eOtherID, 
				IS_TENNIS_SET_ON_TIE_BREAK(thisStatus))		
		
			HANDLE_TENNIS_QUIT_INPUT(thisProps, thisStatus, thisUIData, eSelfID)
			IF NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_BASELINE_TRACKING_INIT)
				UPDATE_BASELINE_CAM_TRACKING(thisProps, thisUIData, eSelfID, thisUIData.tennisCameras.tennisCamFar, thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisCourt.vCourtCorners[1], thisProps.tennisCourt.vCourtCorners[3], 1.0)
				UPDATE_BASELINE_CAM_TRACKING(thisProps, thisUIData, eSelfID, thisUIData.tennisCameras.tennisCamFarOtherSide, thisProps.tennisCourt.vCourtCorners[2], thisProps.tennisCourt.vCourtCorners[3], thisProps.tennisCourt.vCourtCorners[1], 1.0)
				SET_TENNIS_FLOW_FLAG(thisProps, TFF_BASELINE_TRACKING_INIT)
			ENDIF
		BREAK
		
		CASE TGSE_RALLY
			HANDLE_TENNIS_QUIT_INPUT(thisProps, thisStatus, thisUIData, eSelfID)
		BREAK
		
		CASE TGSE_RALLY_PREDICT
			SET_TENNIS_RALLY_COUNTER(thisUIData, GET_TENNIS_RALLY_COUNTER(thisUIData) + 1)
			// Decide if we set the long rally conversation flag
			IF GET_TENNIS_RALLY_COUNTER(thisUIData) > TENNIS_RALLY_CONVO_THRESHOLD AND GET_RANDOM_FLOAT_IN_RANGE() < LONG_RALLY_CONVO_RATE AND GET_GAME_TIMER() > GET_TENNIS_LONG_RALLY_CONVO_STAMP(thisUIData)
				INT iReceivingPlayer
				TENNIS_PLAYER_ID eHitLast
				eHitLast = GET_TENNIS_STATUS_HIT_LAST(thisStatus)
				iReceivingPlayer = 1 - ENUM_TO_INT(eHitLast)
				IF thisUIData.playerChars[iReceivingPlayer] <> CHAR_AMANDA OR thisUIData.playerChars[eHitLast] = CHAR_MICHAEL	// this ensures Amanda lines only fire during Amanda v. Michael matches
					SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_LONG_RALLY)
					SET_TENNIS_LONG_RALLY_CONVO_STAMP(thisUIData, GET_GAME_TIMER() + LONG_RALLY_CONVO_WAIT)
				ENDIF
			ENDIF
		BREAK
		
		CASE TGSE_POINT_WON
			UPDATE_TENNIS_UI_POINTS(thisUIData, 
				GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), 
				thisStatus.playerScores, 
				GET_TENNIS_STATUS_NUM_SETS(thisStatus), 
				GET_TENNIS_STATUS_CURRENT_SET(thisStatus), 
				eSelfID, eOtherID, 
				IS_TENNIS_SET_ON_TIE_BREAK(thisStatus))				
		
			IF thisStatus.ePrevGameState <> TGSE_SERVE_RESULT	// means we didn't have a double fault
			#IF IS_DEBUG_BUILD AND NOT NEVER_WALK_TO_POSITION #ENDIF
				IF SHOULD_TENNIS_PLAYERS_WALK_TO_POSITION(thisProps, thisUIData, thisStatus, eSelfID, eOtherID)
				#IF IS_DEBUG_BUILD OR ALWAYS_WALK_TO_POSITION #ENDIF
					BOOL bServerWalk, bReceiverWalk, bDelayWalk, bSideChange
					TENNIS_PLAYER_ID eServing, eReceiver
					SERVING_SIDE_ENUM eServingSide
					eServing = GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)
					eReceiver = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eServing))
					eServingSide = INT_TO_ENUM(SERVING_SIDE_ENUM, 1 - ENUM_TO_INT(GET_TENNIS_SERVING_SIDE(thisStatus)))
					bSideChange = FALSE
					IF IS_TENNIS_SET_ON_TIE_BREAK(thisStatus)
						bSideChange = (thisStatus.playerScores[eSelfID].iPointsWon + thisStatus.playerScores[eOtherID].iPointsWon + 1) % 6 = 0	// If a multiple of six points was won then we switch court sides
						IF IS_BIT_SET(thisStatus.playerScores[eSelfID].iPointsWon + thisStatus.playerScores[eOtherID].iPointsWon + 1, 0)		// If the number of points is odd then we switch servers
						OR bSideChange
							eServingSide = SERVING_FROM_RIGHT
							eServing = eReceiver
							eReceiver = GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)
							CPRINTLN(DEBUG_TENNIS, "CASE TGSE_POINT_WON Changing Servers and Resetting serve side, bSideChange=", PICK_STRING(bSideChange, "TRUE", "FALSE"))
						ENDIF
					ENDIF
					// Walk back variables, =if player is not tasked with an anim or tasked to walk back already or diving/reacting
					bServerWalk = NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eServing].playerAI, TAF_TASKED_ANIM) AND NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eServing].playerAI, TAF_TASKED_NEW_RALLY) AND GET_GAME_TIMER() > GET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisProps.tennisPlayers[eServing])
					bReceiverWalk = NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eReceiver].playerAI, TAF_TASKED_ANIM) AND NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eReceiver].playerAI, TAF_TASKED_NEW_RALLY) AND GET_GAME_TIMER() > GET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisProps.tennisPlayers[eReceiver])
					bDelayWalk = bServerWalk AND bReceiverWalk
					STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
					SET_TENNIS_FLOW_FLAG(thisProps, TFF_WALKING_TO_POSITION)
					RESTART_TIMER_NOW(thisUIData.tennisUI.sUITimer)
					IF bServerWalk
						CDEBUG2LN(DEBUG_TENNIS, "CASE TGSE_POINT_WON Setting ", thisProps.tennisPlayers[eServing].playerAI.texName, " to serve")
						SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[eServing].playerAI, TAF_TASKED_NEW_RALLY)
						SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[eServing], thisProps, eServingSide, eServing, FALSE, default, FALSE, TRUE, bSideChange)
					ENDIF
					IF bReceiverWalk
						CDEBUG2LN(DEBUG_TENNIS, "CASE TGSE_POINT_WON Setting ", thisProps.tennisPlayers[eReceiver].playerAI.texName, " to receive serve")
						SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[eReceiver].playerAI, TAF_TASKED_NEW_RALLY)
						SET_TENNIS_PLAYER_RECEIVE_SERVE(thisProps.tennisPlayers[eReceiver], thisProps, eServingSide, eReceiver, FALSE, TRUE, bSideChange, bDelayWalk)
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_WALKING_TO_POSITION)
				CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_NEW_RALLY)
				CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[eOtherID].playerAI, TAF_TASKED_NEW_RALLY)
				LOAD_TENNIS_INTERSTITIAL_SCENE_AREAS(thisProps.tennisCourt)
			ENDIF
		BREAK
		
		CASE TGSE_SIDE_CHANGE
			HANDLE_TENNIS_QUIT_INPUT(thisProps, thisStatus, thisUIData, eSelfID)
			UPDATE_TENNIS_UI_POINTS(thisUIData, 
				GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), 
				thisStatus.playerScores, 
				GET_TENNIS_STATUS_NUM_SETS(thisStatus), 
				GET_TENNIS_STATUS_CURRENT_SET(thisStatus), 
				eSelfID, eOtherID, 
				IS_TENNIS_SET_ON_TIE_BREAK(thisStatus))
			
			IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_PAUSE_THE_CUT)
				CPRINTLN(DEBUG_TENNIS, "TD_PAUSE_THE_CUT is set, Setting TD_ANIM_PLAYING")
				IF IS_TIMER_STARTED(thisUIData.tennisUI.sUITimer) OR IS_TIMER_PAUSED(thisUIData.tennisUI.sUITimer)
					RESTART_TIMER_NOW(thisUIData.tennisUI.sUITimer)
					CPRINTLN(DEBUG_TENNIS, "Restarting the timer and pausing the cut")
				ENDIF
				SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)		//We set the anim_playing mask to prevent the game from going onward.
				CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PAUSE_THE_CUT)
//				SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)]),TRUE)
				CDEBUG1LN(DEBUG_TENNIS, "GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)=", thisProps.tennisPlayers[GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)].playerAI.texName)
			ENDIF
			
			fReactAdd = PICK_FLOAT(IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_DELAY_THE_CUT_DELAY), CUT_DELAY_ANIM_ADD, 0.0)
			
			IF IS_TIMER_STARTED(thisUIData.tennisUI.sUITimer) AND GET_TIMER_IN_SECONDS(thisUIData.tennisUI.sUITimer) > CUT_DELAY + fReactAdd
				HANDLE_TENNIS_INTERSTITIALS(thisProps.tennisCourt, thisProps, thisUIData, thisStatus, eSelfID, eOtherID)
				CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PAUSE_DIALOGUE)
			ELSE
				UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, SERVING_FROM_RIGHT, eSelfID)
				UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eOtherID], thisProps, SERVING_FROM_RIGHT, eOtherID)
				HANDLE_TENNIS_RESET_FLAGS(GET_TENNIS_GAME_STATE(thisStatus), thisProps.tennisPlayers[eSelfID], thisUIData.iUtilFlags, TRUE)
				HANDLE_TENNIS_RESET_FLAGS(GET_TENNIS_GAME_STATE(thisStatus), thisProps.tennisPlayers[eOtherID], thisUIData.iUtilFlags, TRUE)
			ENDIF
			
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SIDE_CHANGING)
					CPRINTLN(DEBUG_TENNIS, "Changing side now")
					TENNIS_SIDE_CHANGE_MAKE_PLAYER_WALK(thisProps.tennisCourt, thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)], TRUE)
					
					SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_SIDE_CHANGE, FALSE)
					TENNIS_ACTIVATE_SIDE_CHANGE_CAMERA_SP(thisProps.tennisCourt, thisUIData.tennisCameras, thisProps.tennisPlayers[eSelfID].eCourtSide)
					SHOW_TENNIS_SIDE_CHANGE_MESSAGE(thisUIData.tennisUI)
					SET_TENNIS_FLOW_FLAG(thisProps, TFF_STOP_INTERSTITIAL_CAM_POINTING)
					
					//Prime the dynamic camera to reduce pops
					VECTOR vBallWillBe
					TENNIS_PLAYER_ID eServer, eCourtSide
					SERVING_SIDE_ENUM eServingSide
					eServer = GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus)
					eCourtSide = thisProps.tennisPlayers[eServer].eCourtSide
					eServingSide = GET_TENNIS_SERVING_SIDE(thisStatus)
					IF eCourtSide = TENNIS_PLAYER_AWAY
						vBallWillBe = PICK_VECTOR(eServingSide = SERVING_FROM_RIGHT, thisProps.tennisCourt.vCourtCorners[1], thisProps.tennisCourt.vCourtCorners[0])
					ELSE
						vBallWillBe = PICK_VECTOR(eServingSide = SERVING_FROM_RIGHT, thisProps.tennisCourt.vCourtCorners[3], thisProps.tennisCourt.vCourtCorners[2])
					ENDIF
					// Prime the dynamic camera after a side change
					ADJUST_TENNIS_CAMERA_DYNAMIC(thisUIData.tennisCameras.tennisCamDyna, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt),
						PICK_VECTOR(thisProps.tennisPlayers[eSelfID].eCourtSide = (TENNIS_PLAYER_AWAY), thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisCourt.vCourtCorners[2]),
						thisProps.tennisPlayers[eSelfID].vMyRight, thisProps.tennisPlayers[eSelfID].vMyForward, vBallWillBe - thisProps.tennisCourt.vCenterCourt,
						thisUIData.tennisCameras.fDynaCamSwing, DEFAULT_DYNA_DIST, thisUIData.tennisCameras.fDynaDolly, thisUIData.tennisCameras.fDynaNudgeRSLR, 
						thisUIData.tennisCameras.fDynaNudgeRSUD, FALSE, TRUE)
												
					ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, FALSE)
												
					SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SIDE_CHANGING)
				ENDIF
				IF IS_TIMER_STARTED(thisUIData.tennisCameras.tennisCamTimer)
				AND GET_TIMER_IN_SECONDS(thisUIData.tennisCameras.tennisCamTimer) * 1000 > SIDE_CHANGE_LENGTH
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_PREPARE_SERVE)
					CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_WALKING_TO_POSITION)
					CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SIDE_CHANGING)
					SET_TENNIS_FLOW_FLAG(thisProps, TFF_HANDLE_UI_AFTER_SIDE_CHANGE)
				ENDIF
			ENDIF
		BREAK
		
		CASE TGSE_OUTRO
			IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_QUIT_WAIT)
				CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_QUIT_WAIT)
				DISPLAY_TENNIS_QUIT_UI(thisUIData.tennisUI)
			ENDIF
			IF GET_TENNIS_UI_STATE(thisUIData) <> TENNIS_UI_QUIT
				SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_QUIT)
			ENDIF
			UPDATE_SHARD_BIG_MESSAGE(thisUIData.tennisUI.matchUI)
			SET_TENNIS_BALL_VISIBILITY(thisProps.tennisBall.oBall, FALSE)
			HANDLE_BITMASK_UI(thisUIData, thisProps, thisStatus.playerScores, 
				GET_TENNIS_MATCH_RULESET(thisStatus), 
				GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), 
				GET_TENNIS_STATUS_SCORED_LAST(thisStatus), 0, 
				GET_TENNIS_STATUS_NUM_SETS(thisStatus), 
				GET_TENNIS_STATUS_CURRENT_SET(thisStatus), 
				eSelfID, eOtherID)
			UPDATE_TENNIS_UI_POINTS(thisUIData, 
				GET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus), 
				thisStatus.playerScores, 
				GET_TENNIS_STATUS_NUM_SETS(thisStatus), 
				GET_TENNIS_STATUS_CURRENT_SET(thisStatus), 
				eSelfID, eOtherID, 
				IS_TENNIS_SET_ON_TIE_BREAK(thisStatus), TRUE)
			
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				BOOL bOutroFinished
				bOutroFinished = FALSE
				
				// Checking if player won
				IF (GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_SETS 		AND thisStatus.playerScores[eSelfID].iSetsWon > GET_TENNIS_STATUS_NUM_SETS(thisStatus)/2)
				OR (GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_ONE_GAME 	AND thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] >= 1)
				OR (GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_THREE_GAMES 	AND thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] >= 2)
				OR (GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_FIVE_GAMES 	AND thisStatus.playerScores[eSelfID].iGamesWon[GET_TENNIS_STATUS_CURRENT_SET(thisStatus)] >= 3)
					bOutroFinished = TENNIS_PLAY_OUTRO_CUTSCENE(thisUIData.tennisCameras, thisProps, thisUIData, thisUIData.iUtilFlags, TRUE, eSelfID/*, eOtherID*/)
					HANDLE_TENNIS_COMPLETION_PERCENTAGE()
				//Opponent must have won
				ELSE
					bOutroFinished = TENNIS_PLAY_OUTRO_CUTSCENE(thisUIData.tennisCameras, thisProps, thisUIData,  thisUIData.iUtilFlags, FALSE, eSelfID/*, eOtherID*/)
				ENDIF
				
				IF bOutroFinished
					DISPLAY_TENNIS_SPLASH_UI(thisUIData.tennisUI, FALSE)
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_FINISH)
					CLEAR_RANK_REDICTION_DETAILS()
					TENNIS_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
					IF thisProps.eTennisActivity <> TA_FRIEND_ACTIVITY AND NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
						RESET_CUTSCENE_STATE(thisUIData.tennisCameras)
					ELSE
						SET_TENNIS_CUTSCENE_STATE(thisUIData.tennisCameras, CUT_SET_FRIEND_OUTFIT)
					ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
				DEBUG_RECORD_END()
			#ENDIF
		BREAK
		
		CASE TGSE_OUTRO_FROM_OPTIONS
			IF IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
				IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_QUIT_WAIT)
					CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_QUIT_WAIT)
					SET_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), thisSPData.sVariations)
					CPRINTLN(DEBUG_TENNIS, "Restoring the player's outfit")
					IF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY OR IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
						SET_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), thisSPData.sOppVariations)
					ENDIF
					CPRINTLN(DEBUG_TENNIS, "Quitting Tennis from the option menu")
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_TENNIS_PLAYER_TO_TENNIS_MODE(PLAYER_PED_ID(), FALSE)
					ENDIF
					SET_TENNIS_QUIT_WAIT_STAMP(thisUIData, GET_GAME_TIMER() + 500)	// wait 500ms to allow the player's clothes to switch back
					// restore the player's vehicle
					ODDJOB_RESTORE_SAVED_VEHICLE(thisSPData.playerVehicle)
					MOVE_VEHICLE_TO_SAFE_LOCATION_FOR_COURT(thisProps, thisSPData.playerVehicle.playerVehicle)
				ENDIF
				
				IF GET_GAME_TIMER() > GET_TENNIS_QUIT_WAIT_STAMP(thisUIData)
					TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, thisSPData, AR_playerQuit)
				ENDIF
			ENDIF
		BREAK
		
		CASE TGSE_FINISH
			ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, FALSE)
			
			INT iChoice
			iChoice = PROCESS_RESULTS_SCOREBOARD(thisProps, thisStatus, thisUIData, eSelfID, eOtherID, IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_QUIT_BIT))
			
			IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_QUIT_WAIT)
				// Restore the player's vehicle
				ODDJOB_RESTORE_SAVED_VEHICLE(thisSPData.playerVehicle)
				MOVE_VEHICLE_TO_SAFE_LOCATION_FOR_COURT(thisProps, thisSPData.playerVehicle.playerVehicle)
				SET_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), thisSPData.sVariations)
				CPRINTLN(DEBUG_TENNIS, "CASE TGSE_FINISH Restoring the player's outfit")
				IF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY	// OR IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
					CPRINTLN(DEBUG_TENNIS, "Restoring opponent variations")
					SET_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), thisSPData.sOppVariations)
					WHILE NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID])) AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
						CDEBUG2LN(DEBUG_TENNIS, "Waiting on Friend outfit to load back in")
						WAIT(0)
					ENDWHILE
				ENDIF
				WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
					CDEBUG2LN(DEBUG_TENNIS, "Waiting on Player outfit to load back in")
					WAIT(0)
				ENDWHILE
				
				IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[eSelfID].oTennisRacket)
					DELETE_OBJECT(thisProps.tennisPlayers[eSelfID].oTennisRacket)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(thisSPData.oBottle)
					thisSPData.oBottle = CREATE_OBJECT(PROP_ENERGY_DRINK, thisProps.tennisCourt.vCenterCourt)
				ENDIF
				PED_INDEX ped
				ped = GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID])
				IF DOES_ENTITY_EXIST(thisSPData.oBottle) AND DOES_ENTITY_EXIST(ped)
					ATTACH_ENTITY_TO_ENTITY(thisSPData.oBottle, ped, GET_PED_BONE_INDEX(ped, BONETAG_PH_R_Hand), (<<0,0,0>>), (<<0,0,0>>), TRUE)
				ENDIF
				
				FLOAT fThrowBottlePhase
				FLOAT fAllowBreakPhase	// The phase at which player can break from the outro anim
				sSceneDict = "mini@tennisoutro@"
				sScenePlrAnim = "tennis_outro_"
				IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
					sSceneDict += "michael"
					sScenePlrAnim += "michael"
					fThrowBottlePhase = 0.32
					fAllowBreakPhase = 0.48
				ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
					sSceneDict += "trevor"
					sScenePlrAnim += "trevor"
					fThrowBottlePhase = 0.26
					fAllowBreakPhase = 0.51
				ENDIF
				sSceneCamAnim = sScenePlrAnim
				sSceneCamAnim += "_cam"
				
				// If for some reason fAllowBreakPhase gets changed to a lower number
				// ensure that we cannot break until the bottle is thrown
				IF fAllowBreakPhase < fThrowBottlePhase
					fAllowBreakPhase = fThrowBottlePhase
				ENDIF
				
				IF DOES_CAM_EXIST(thisUIData.tennisCameras.tennisCamTut)
					vOldCamRot = GET_CAM_ROT(thisUIData.tennisCameras.tennisCamTut)
					DESTROY_CAM(thisUIData.tennisCameras.tennisCamTut)
				ENDIF
				thisUIData.tennisCameras.tennisCamTut = CREATE_CAMERA(CAMTYPE_ANIMATED)
				
				IF DOES_CAM_EXIST(thisUIData.tennisCameras.tennisCamDyna)
					DESTROY_CAM(thisUIData.tennisCameras.tennisCamDyna)
				ENDIF
				thisUIData.tennisCameras.tennisCamDyna = CREATE_CAMERA(CAMTYPE_SCRIPTED)
				
				ANIMPOSTFX_STOP("MinigameTransitionIn")
				ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
				
				DISPLAY_RADAR(FALSE)
				
				IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[eOtherID].oTennisRacket)
					DELETE_OBJECT(thisProps.tennisPlayers[eOtherID].oTennisRacket)
				ENDIF
				
				IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
					SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), GET_TENNIS_OPPONENT_OUTRO_COORDS(thisProps.tennisCourt.eTennisCourt))
					SET_ENTITY_HEADING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), GET_TENNIS_OPPONENT_OUTRO_HEADING(thisProps.tennisCourt.eTennisCourt))
				ENDIF
				
				SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), TRUE)
				SET_FORCE_STEP_TYPE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), TRUE, 20, 0)
				CPRINTLN(DEBUG_TENNIS, "SET_FORCE_FOOTSTEP_UPDATE TRUE")
				
				TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), "mini@triathlon", "idle_e", INSTANT_BLEND_IN)
				
				VECTOR vRotation
				vRotation = GET_TENNIS_OUTRO_ROTATION(thisProps.tennisCourt.eTennisCourt)
				IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
					vRotation.z += 90.0
				ENDIF
				
				thisUIData.iSceneID = CREATE_SYNCHRONIZED_SCENE(GET_TENNIS_OUTRO_COORDS(thisProps.tennisCourt.eTennisCourt), vRotation)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), thisUIData.iSceneID, sSceneDict, sScenePlrAnim, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT)
				PLAY_SYNCHRONIZED_CAM_ANIM(thisUIData.tennisCameras.tennisCamTut, thisUIData.iSceneID, sSceneCamAnim, sSceneDict)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(thisUIData.iSceneID, FALSE)
				
				vStartCamPos = GET_CAM_COORD(thisUIData.tennisCameras.tennisCamTut)
				vStartCamPos.z += 60.0
				vStartCamRot = GET_CAM_ROT(thisUIData.tennisCameras.tennisCamTut)
				vStartCamRot.x = vOldCamRot.x
				
				SET_CAM_COORD(thisUIData.tennisCameras.tennisCamDyna, vStartCamPos)
				SET_CAM_ROT(thisUIData.tennisCameras.tennisCamDyna, vStartCamRot)
				SET_CAM_FOV(thisUIData.tennisCameras.tennisCamDyna, GET_CAM_FOV(thisUIData.tennisCameras.tennisCamTut))
				
				CDEBUG2LN(DEBUG_TENNIS, "QUIT_WHOOSH played, second")
				PLAY_SOUND_FRONTEND(-1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
				SET_CAM_ACTIVE_WITH_INTERP(thisUIData.tennisCameras.tennisCamTut, thisUIData.tennisCameras.tennisCamDyna, 666)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				// Reposition the player if we're at Michael's house.
//				IF thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_MichaelHouse
//					IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
//						CPRINTLN(DEBUG_TENNIS, "Setting position at Michaels house so the camera has a place to go.")
//						SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), thisProps.tennisCourt.vCenterCourt + (thisProps.tennisPlayers[eSelfID].vMyForward * -3.0))
//					ENDIF
//				ENDIF
				
				BOOL bExitAnim
				
				MAKE_TENNIS_AUTOSAVE_REQUEST()
				VECTOR vPrevPos, vCurPos, vBottleVel
				WHILE GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID) < 1.0
				AND NOT bExitAnim
				
					REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
					
					// If player has tried to move after the bottle has been thrown but before synch scene is over
					IF (GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) < - DISABLED_INPUT_DEAD_ZONE
					OR GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) > DISABLED_INPUT_DEAD_ZONE
					OR GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < - DISABLED_INPUT_DEAD_ZONE
					OR GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > DISABLED_INPUT_DEAD_ZONE)
						IF GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID) >= fAllowBreakPhase
							bExitAnim = TRUE
							CPRINTLN(DEBUG_TENNIS, "Breaking from end synch scene")
							CDEBUG1LN(DEBUG_TENNIS, "Current synch scene phase is: ", GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID), ". Allow break phase is: ", fAllowBreakPhase )
						ENDIF
					ENDIF
				
//					CDEBUG2LN(DEBUG_TENNIS, "GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID)=", GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID))
					IF DOES_ENTITY_EXIST(thisSPData.oBottle)
						vPrevPos = vCurPos
						vCurPos = GET_ENTITY_COORDS(thisSPData.oBottle)
						vBottleVel = vCurPos - vPrevPos
					ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID) >= fThrowBottlePhase
					OR bExitAnim
						IF DOES_ENTITY_EXIST(thisSPData.oBottle) AND IS_ENTITY_ATTACHED(thisSPData.oBottle)
							vBottleVel *= 1/GET_FRAME_TIME()	// Scale up by 1/FPS
							CDEBUG2LN(DEBUG_TENNIS, "Detaching water bottle, vBottleVel=", vBottleVel)
							DETACH_ENTITY(thisSPData.oBottle)
							SET_ENTITY_VELOCITY(thisSPData.oBottle, vBottleVel)
							SET_OBJECT_AS_NO_LONGER_NEEDED(thisSPData.oBottle)
						ENDIF
					ENDIF
					WAIT(0)
				ENDWHILE
				
				SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), FALSE)
				SET_FORCE_STEP_TYPE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), FALSE, 20, 0)
				CPRINTLN(DEBUG_TENNIS, "SET_FORCE_FOOTSTEP_UPDATE FALSE")
				
				IF NOT bExitAnim
					CPRINTLN(DEBUG_TENNIS, "not bexitanim")
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_PED_RESET_FLAG(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), PRF_SkipOnFootIdleIntro, TRUE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					SET_PED_RESET_FLAG(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), PRF_SkipOnFootIdleIntro, TRUE)
				ELSE
						CPRINTLN(DEBUG_TENNIS, "bexitanim")
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE)
      				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 500)
				ENDIF
				
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				
				//RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE)
				
				IF (GET_TENNIS_MATCH_RULESET(thisStatus) = TMR_SETS AND thisStatus.playerScores[eSelfID].iSetsWon > GET_TENNIS_STATUS_NUM_SETS(thisStatus)/2)
				OR (GET_TENNIS_MATCH_RULESET(thisStatus) <> TMR_SETS AND thisStatus.playerScores[eSelfID].iGamesWon[0] > thisStatus.playerScores[eOtherID].iGamesWon[0])
					CDEBUG1LN(DEBUG_TENNIS, "thisStatus.playerScores[eSelfID].iSetsWon > GET_TENNIS_STATUS_NUM_SETS(thisStatus)/2 :: AR_playerWon")
					IF NOT sclb_rank_predict.bFinishedWrite //emergency write!
						WRITE_TENNIS_SP_SCLB_DATA()
					ENDIF
					SCRIPT_PASSED(thisProps, thisSPData, thisUIData, AR_playerWon, eSelfID, eOtherID, TRUE)
				ELSE
					IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_QUIT_BIT)
						CDEBUG1LN(DEBUG_TENNIS, "IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_QUIT_BIT) :: AR_playerQuit")
						SCRIPT_FAILED(thisProps, thisSPData, thisUIData, AR_playerQuit, eSelfID, eOtherID, TRUE)
					ELSE
						CDEBUG1LN(DEBUG_TENNIS, "NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_QUIT_BIT) :: AR_buddyA_won")
						IF NOT sclb_rank_predict.bFinishedWrite
							WRITE_TENNIS_SP_SCLB_DATA()
						ENDIF
						SCRIPT_FAILED(thisProps, thisSPData, thisUIData, AR_buddyA_won, eSelfID, eOtherID, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF iChoice = 1
				SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_QUIT_WAIT)
				HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
				PLAY_SOUND_FRONTEND(-1, "NO", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				IF DOES_ENTITY_EXIST(thisSPData.oLighting)
					DELETE_OBJECT(thisSPData.oLighting)
				ENDIF
			ELIF iChoice = 2
				HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
				READY_TENNIS_FOR_REMATCH(thisProps, thisUIData, thisStatus, eSelfID, eOtherID)
				CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_SCLB_RANK_PRED_DONE)
				PLAY_SOUND_FRONTEND(-1, "YES", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			ENDIF
		BREAK
		
		CASE TGSE_QUIT_FROM_OPTION_MENU
		CASE TGSE_QUIT_CONFIRM
			DISPLAY_TENNIS_QUIT_UI(thisUIData.tennisUI)
			IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID])) AND IS_TASK_MOVE_NETWORK_ACTIVE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
				SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), "Speed", 0.5)
			ENDIF
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				CLEAR_HELP(TRUE)
				SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_PLAYING)
				SET_TENNIS_INTERSTITIAL_PLAYER(thisUIData, eSelfID)
				
				// Just in case, wipe the ball effects.
				STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
				
				//Clear all UI, set us to the quit scene.
				RESET_CUTSCENE_STATE(thisUIData.tennisCameras)
				DISPLAY_TENNIS_SPLASH_UI(thisUIData.tennisUI, FALSE)
				RETURN_FROM_TENNIS_QUIT_STATE(thisStatus)
				IF thisStatus.ePrevGameState > TGSE_OPTION_FINISHED
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_OUTRO)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_TENNIS_PLAYER_TO_TENNIS_MODE(PLAYER_PED_ID(), FALSE)
					ENDIF
					SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_QUIT_WAIT)
					CPRINTLN(DEBUG_TENNIS, "Quitting Tennis from gameplay")
				ELSE
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_OUTRO_FROM_OPTIONS)
					SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_QUIT_WAIT)
					DO_SCREEN_FADE_OUT(500)
				ENDIF
				SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_UPDATE_SCORES)
				CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				IF IS_TIMER_STARTED(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
					TENNIS_PAUSE_TIMER(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
				ENDIF
				SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_QUIT_BIT)
				SWING_METER_DISPLAY(thisUIData.tennisUI, FALSE, 0)
				
				KILL_ANY_CONVERSATION()
				
				CLEAR_HELP()
				
				RESET_TENNIS_MATCH_UI(thisUIData.tennisUI)
				
				SET_TENNIS_PAUSED(FALSE, thisProps.tennisBall)
			ENDIF
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				// Go Back to the game
				RETURN_FROM_TENNIS_QUIT_STATE(thisStatus)
				// Give control back if we had it
				IF thisStatus.ePrevGameState > TGSE_OPTION_FINISHED
					ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, TRUE)
					CPRINTLN(DEBUG_TENNIS, "Control given back to player")
				ENDIF
				//If the player hit the quit area then set their position before leaving this screen
				IF VDIST2(thisProps.tennisPlayers[eSelfID].vPos, thisSPData.vQuitPoint) < QUIT_PT_RADIUS * QUIT_PT_RADIUS
					vTemp = thisProps.tennisPlayers[eSelfID].vPos - thisSPData.vQuitPoint
					vTemp = vTemp * QUIT_PT_RESET
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), thisProps.tennisPlayers[eSelfID].vPos + vTemp, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
					ENDIF
				ENDIF
				// unpause the game
				SET_TENNIS_PAUSED(FALSE, thisProps.tennisBall)
			ENDIF
		BREAK
		
		CASE TGSE_TEST_STATE
			SHOW_TENNIS_STATE_MESSAGE()
			VECTOR vXMarker
			vXMarker = thisProps.tennisCourt.vCenterCourt - thisProps.tennisPlayers[eSelfID].vMyForward
			vXMarker.z += 0.01
			DISPLAY_TENNIS_X_MARKER(vXMarker, thisProps.tennisBall.eBallSpin, TRUE)			
			PROCESS_TENNIS_UI(thisProps, thisStatus, thisUIData, thisProps.tennisPlayers[eSelfID], eSelfID, eOtherID, FALSE)
			SET_TENNIS_FAR_CAM_MULTS(thisUIData.tennisCameras, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt))
			SET_FAR_CAM(thisProps, thisUIData, eSelfID)
			SET_FAR_CAM_OTHER_SIDE(thisProps, thisUIData, eSelfID)
			
//			DRAW_DEBUG_BOX(thisSPData.vBlockingArea[2], thisSPData.vBlockingArea[3])
			
//			SHOW_TENNIS_SCOREBOARD(thisUIData.tennisUI)
			
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
				CPRINTLN(DEBUG_TENNIS, "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^   Swapping Court Sides   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
				TENNIS_ACTIVATE_SIDE_CHANGE(thisProps, thisUIData.tennisCameras)
				SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[eSelfID], 
										thisProps, 
										GET_TENNIS_SERVING_SIDE(thisStatus), 
										eSelfID,
										FALSE)
				SET_TENNIS_PLAYER_RECEIVE_SERVE(thisProps.tennisPlayers[eOtherID], 
												thisProps, 
												GET_TENNIS_SERVING_SIDE(thisStatus), 
												eOtherID,
												FALSE)
			ENDIF
			
//			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
//				CDEBUG2LN(DEBUG_TENNIS, "PLAY_TENNIS_AMBIENT_SPEECH(TENNIS_WIN")
//				TEXT_LABEL_31 texVoice
//				texVoice = "AMANDA_NORMAL"
//				PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[eOtherID], "TENNIS_WIN", texVoice)
//			ENDIF
//			
//			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
//				CDEBUG2LN(DEBUG_TENNIS, "CREATE_CONVERSATION(thisUIData.FaMa_TnsWn")
//				CREATE_CONVERSATION(thisUIData.myDialogueStruct, "FaMAAU", "FaMa_TnsWn", CONV_PRIORITY_HIGH)
//			ENDIF
//			
//			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
//				CDEBUG2LN(DEBUG_TENNIS, "CREATE_CONVERSATION(thisUIData.FaMA_TnsLs")
//				CREATE_CONVERSATION(thisUIData.myDialogueStruct, "FaMAAU", "FaMA_TnsLs", CONV_PRIORITY_HIGH)
//			ENDIF
			
			/********************************************************************************************************************************************
			***		Serve Meter calls
			********************************************************************************************************************************************/
//			SETUP_TENNIS_SERVE_METER(thisUIData.tennisUI.scorePanelSF)
			FLOAT fScreenX, fScreenY, fApexX, fNumerator, fDenominator, fAlpha
			INT ex, dummy
			GET_ANALOG_STICK_VALUES(ex, dummy, dummy, dummy)
			IF ABSI(ex) <= SERVE_OOB_THRESHOLD
				fNumerator = TO_FLOAT(ex + SERVE_OOB_THRESHOLD)
				fDenominator = TO_FLOAT(SERVE_OOB_THRESHOLD + SERVE_OOB_THRESHOLD)
				fAlpha = fNumerator/fDenominator
				fApexX = LERP_FLOAT(-0.5, 0.5, fAlpha)
//				CPRINTLN(DEBUG_TENNIS, "IN ", "fNumerator: ", fNumerator, " fDenominator: ", fDenominator, " fAlpha: ", fAlpha)
			ELSE
				fApexX = TO_FLOAT(ex / SERVE_OOB_THRESHOLD)
				fApexX = CLAMP(fApexX, -1, 1)
//				CPRINTLN(DEBUG_TENNIS, "OUT ", "fNumerator: ", TO_FLOAT(ex/128), " ex: ", ex)
			ENDIF
			GET_SCREEN_COORD_FROM_WORLD_COORD(thisProps.tennisPlayers[eSelfID].vPos, fScreenX, fScreenY)
			thisUIData.fSwingMeterX = LERP_FLOAT(thisUIData.fSwingMeterX, fScreenX + SERVE_X_OFFSET, 0.5)
			thisUIData.fSwingMeterY = LERP_FLOAT(thisUIData.fSwingMeterY, SERVE_Y_OFFSET, 0.5)
			SWING_METER_SET_APEX_MARKER(thisUIData.tennisUI.scorePanelSF, TRUE, 0.5, TRUE, fApexX)
			SWING_METER_SET_POSITION(thisUIData.tennisUI.scorePanelSF, thisUIData.fSwingMeterX, thisUIData.fSwingMeterY)
//			SWING_METER_SET_TARGET_COLOR(thisUIData.tennisUI.scorePanelSF, ARRR, GEE, BEE, ARVA)
		#IF IS_DEBUG_BUILD
			IF SHOULD_SWING_METER_SHOW
				SWING_METER_DISPLAY(thisUIData.tennisUI, TRUE)
				CPRINTLN(DEBUG_TENNIS, "xoff=", thisUIData.fSwingMeterX, ", yoff=", thisUIData.fSwingMeterY, ", fScreenX=", fScreenX, ", fScreenY=", fScreenY)
			ELSE
				SWING_METER_DISPLAY(thisUIData.tennisUI, FALSE)
			ENDIF
		#ENDIF
		BREAK
	ENDSWITCH
ENDPROC
