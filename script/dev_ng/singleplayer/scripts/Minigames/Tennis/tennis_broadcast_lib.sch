 
//////////////////////////////////////////////////////////////////////
/* tennis_broadcast_lib.sch											*/
/* Authors: DJ Jones & Ryan Paradis*								*/
/* *shamelessly stolen by Rob and DJ								*/
/* Multiplayer functionality of broadcast msgs for tennis.			*/
//////////////////////////////////////////////////////////////////////


USING "tennis_broadcast.sch"
USING "tennis_ui_lib.sch"
USING "tennis_helpers_lib.sch"
USING "tennis_client_lib.sch"
USING "tennis_helpers_lib.sch"
USING "tennis_ball_lib.sch"
USING "tennis_server_lib.sch"


/// PURPOSE:
///    Send a message to all clients. SCTV players are blocked from sending messages
/// PARAMS:
///    sBroadcast - 
///    iPlayerFlags - 
PROC TENNIS_SEND_BROADCAST_EVENT(TENNIS_BROADCAST_EVENT& sBroadcast, INT iPlayerFlags)
	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	TIME_DATATYPE tempTime = GET_NETWORK_TIME_ACCURATE()
	sBroadcast.playerID = PLAYER_ID()
	SET_TENNIS_BROADCAST_TIME_DATATYPE(sBroadcast, tempTime)
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sBroadcast, SIZE_OF(sBroadcast), iPlayerFlags)
	CDEBUG1LN(DEBUG_TENNIS, "TENNIS_SEND_BROADCAST_EVENT netTime = ", GET_TENNIS_BROADCAST_TIME_DATATYPE(sBroadcast))
ENDPROC

PROC TENNIS_MP_BROADCAST_BALL_DESYNC()
	
	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_BALL_DESYNC
	
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event BALL_UPDATE_IMPACT_TYPE_BALL_DESYNC sent")
ENDPROC

PROC TENNIS_MP_BROADCAST_SEQUENCE_DONE()
	
	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SEQ_DONE
	
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event BALL_UPDATE_IMPACT_TYPE_SEQ_DONE sent")
	
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_MAKE_BALL( TENNIS_SERVER_BD &serverBD, TENNIS_PLAYER_ID eSelfID )

	IF GET_TENNIS_MP_WHO_IS_SERVING(serverBD) != eSelfID
		EXIT
	ENDIF
	
	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_CREATE
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_MAKE_BALL sent from ", eventToSend.iLRAngle)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_LAUNCH_BALL( TENNIS_SERVER_BD &serverBD, TENNIS_PLAYER_ID eSelfID, INT iForce, INT iAngle, FLOAT fLeftRight, VECTOR vForward, VECTOR vRight )

	IF GET_TENNIS_MP_WHO_IS_SERVING(serverBD) != eSelfID
		EXIT
	ENDIF
	
	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_LAUNCH
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	eventToSend.iDummy0 = iForce
	eventToSend.iDummy1 = iAngle
	eventToSend.fDummy = fLeftRight
	eventToSend.vVelocity = vForward
	eventToSend.vPosition = vRight
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_LAUNCH_BALL sent from ", eventToSend.iLRAngle)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_REQUEST_UPDATE()

	IF NOT TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_REQUEST_UPDATE
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	
	// Send our event off.
	TIME_DATATYPE tempTime = GET_NETWORK_TIME_ACCURATE()
	SET_TENNIS_BROADCAST_TIME_DATATYPE(eventToSend, tempTime)
	eventToSend.playerID = PLAYER_ID()
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, eventToSend, SIZE_OF(eventToSend), ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_REQUEST_UPDATE sent from ", eventToSend.iLRAngle)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_UI_SHOW_PLAYER_CARD()

	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_PLAYER_CARD
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_UI_SHOW_PLAYER_CARD sent from ", eventToSend.iLRAngle)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SCOREBOARD()

	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SCOREBOARD
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SCOREBOARD sent from ", eventToSend.iLRAngle)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_UI_SET_CAMERA_STATE( CAMERA_STATE_ENUM eEnum, BOOL bSetChosenCam = TRUE )

	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SET_CAMERA_STATE
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	eventToSend.iDummy0 = ENUM_TO_INT( eEnum )
	eventToSend.bDummy = bSetChosenCam
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_UI_SET_CAMERA_STATE sent from ", eventToSend.iLRAngle)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UTIL_FLAGS( TENNIS_GAME_UI_DATA & thisUIData )

	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_UTIL_FLAGS
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	eventToSend.iDummy0 = thisUIData.iUtilFlags
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UTIL_FLAGS sent from ", eventToSend.iLRAngle)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_FLAGS( TENNIS_GAME_UI_DATA & thisUIData )

	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_UI_FLAGS
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	eventToSend.iDummy0 = thisUIData.tennisUI.iUIFlags
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_FLAGS sent from ", eventToSend.iLRAngle)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_POINTS( BOOL bHide )

	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_POINTS
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	eventToSend.bDummy = bHide
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_POINTS sent from ", eventToSend.iLRAngle)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SERVE_MESSAGE( TENNIS_PLAYER_ID ePlayerID )

	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.playerID = PLAYER_ID()
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SERVE_MESSAGE
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	eventToSend.iDummy0 = ENUM_TO_INT( ePlayerID )
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SERVE_MESSAGE sent from ", eventToSend.iLRAngle)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SECOND_SERVE_MESSAGE()

	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SECOND_SERVE_MESSAGE
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SECOND_SERVE_MESSAGE sent from ", eventToSend.iLRAngle)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SIDE_CHANGE()

	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SIDE_CHANGE
	eventToSend.iLRAngle = PARTICIPANT_ID_TO_INT()
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SIDE_CHANGE sent from ", eventToSend.iLRAngle)
ENDPROC


/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_REACT()
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_GENERAL)
	SET_TENNIS_BROADCAST_IMPACT_ID(eventToSend, ENUM_TO_INT(TGEE_REACT_EVENT))
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event 'React' sent")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
///    iServerMessage - pass in 1 if this message comes from the server, 2 for another case, for leaving the game really.
PROC TENNIS_MP_BROADCAST_QUIT(INT iServerMessage=0)
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_QUIT)
	SET_TENNIS_QUIT_EVENT_SERVER_FLAG(eventToSend, iServerMessage)
	IF iServerMessage = 1
		SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
	ELSE
		CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
	ENDIF
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	CPRINTLN(DEBUG_TENNIS, "Broadcast event 'Quit' sent, iServerMessage=", iServerMessage)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    iOffender - 
///    eSelfID - 
///    iUIFlags - 
PROC TENNIS_MP_BROADCAST_GET_OFF_MY_SIDE(INT iOffender, INT &iUIFlags, INT &iUtilFlags)
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	TENNIS_BROADCAST_EVENT eventToSend
	
	//Fill in the event type
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_GET_OFF_MY_SIDE)
	SET_TENNIS_BROADCAST_OFFENDER(eventToSend, iOffender)
	
	//Send the event off
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	//Take care of Util and UI Flags
	SET_BITMASK_AS_ENUM(iUIFlags, TUF_FOULED)
	CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_ACE)
	
	CPRINTLN(DEBUG_TENNIS, "Broadcast event 'Get Off My Side!' sent")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
///    iServeCount - 
PROC TENNIS_MP_BROADCAST_FAULT(INT iServeCount)
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	TENNIS_BROADCAST_EVENT eventToSend
	
	//Fill in the event type
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_FAULT)
	SET_TENNIS_BROADCAST_SERVE_COUNT(eventToSend, iServeCount)
	
	//Send the event off
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, "Broadcast event 'Fault' sent")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_OOB(TENNIS_PLAYER_ID eSelfID)
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	TENNIS_BROADCAST_EVENT eventToSend
	
	//Fill in the event type
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_OOB)
	SET_TENNIS_BROADCAST_POINT_WINNER(eventToSend, eSelfID)
	
	//Send the event off
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, "Broadcast event 'OOB' sent")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_POINT_WON(TENNIS_PLAYER_ID ePointWinner)
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	TENNIS_BROADCAST_EVENT eventToSend
	
	//Fill in the event type
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_POINT_WON)
	SET_TENNIS_BROADCAST_POINT_WINNER(eventToSend, ePointWinner)
	
	//Send the event off
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, "Broadcast event 'Point Won' sent")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_HIT_BEFORE_BOUNCE(INT &iUIFlags)
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	TENNIS_BROADCAST_EVENT eventToSend
	
	// Fill in the event type
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_HIT_BEFORE_BOUNCE)
	
	// Set the UIFlag for a specific point message
	SET_BITMASK_AS_ENUM(iUIFlags, TUF_HIT_BEFORE_BOUNCE)
	
	// Send the event off
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, "Broadcast event 'Ball hit before bounce' sent")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    eSelfID - 
PROC TENNIS_MP_BROADCAST_HIT_BY_BALL()
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	TENNIS_BROADCAST_EVENT eventToSend
	
	//Fill in the event type
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_HIT_BEFORE_BOUNCE)
	SET_TENNIS_BEFORE_BOUNCE_HIT_BY_FLAG(eventToSend)
	
	//Send the event off
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	
	CPRINTLN(DEBUG_TENNIS, "Broadcast event 'TENNIS_MP_BROADCAST_HIT_BY_BALL' sent")
ENDPROC

PROC TENNIS_MP_BROADCAST_SERVE_EVENT(INT iLRAngle, LAUNCH_DETAIL_ENUM eServeGrade, VECTOR vBallPos)
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	TENNIS_BROADCAST_EVENT eventToSend
	CPRINTLN(DEBUG_TENNIS, "eServeGrade: ", GET_STRING_FROM_TENNIS_SERVE_GRADE(eServeGrade))
	CPRINTLN(DEBUG_TENNIS, "iLRAngle: ", iLRAngle)
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	SET_TENNIS_BROADCAST_SERVE_GRADE(eventToSend, eServeGrade)
	SET_TENNIS_BROADCAST_SERVE_ANGLE(eventToSend, iLRAngle)
	SET_TENNIS_BROADCAST_BALL_POSITION(eventToSend, vBallPos)
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SERVE
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	CPRINTLN(DEBUG_TENNIS, "Broadcast event 'TENNIS_MP_BROADCAST_SERVE_EVENT' sent")
ENDPROC

/// PURPOSE:
///    Broadcast a ball update message to all players other than the sender.
/// PARAMS:
///    vBallPos - 
///    vBallVel - 
///    eSelfID - 
///    clientBD - []
///    bServerBroadcast - When true this sends the message to both clients, used mainly for correcting desyncs
PROC TENNIS_MP_BROADCAST_UPDATE_BALL(VECTOR vBallPos, VECTOR vBallVel, TENNIS_BALL_SPIN broadcastSpin, TENNIS_PLAYER_ID eSelfID, TENNIS_CLIENT_BD &clientBD[], TENNIS_BALL_UPDATE_ENUM eUpdate, SHOT_TYPE_ENUM eShot, TENNIS_PARTICIPANT_INFO &info, BOOL bServerBroadcast=FALSE)
	CPRINTLN(DEBUG_TENNIS, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	TENNIS_BROADCAST_EVENT eventToSend
	
	IF eUpdate = TBU_INITIAL_POSITION
		INCREASE_TENNIS_CLIENT_RALLY_COUNTER(clientBD[info.iPlayers[eSelfID]])
	ENDIF
	
	IF SHOULD_TENNIS_CLIENT_GET_RALLY_XP_BONUS(clientBD[info.iPlayers[eSelfID]])
		RESET_TENNIS_CLIENT_RALLY_COUNTER(clientBD[info.iPlayers[eSelfID]])
		AWARD_TENNIS_CLIENT_RALLY_XP_BONUS()
	ENDIF
	
	// Fill in the event.
	SET_TENNIS_BROADCAST_EVENT_TYPE(eventToSend, SCRIPT_EVENT_BALL_UPDATE)
	SET_TENNIS_BROADCAST_BALL_POSITION(eventToSend, vBallPos)
	SET_TENNIS_BROADCAST_BALL_VELOCITY(eventToSend, vBallVel)
	SET_TENNIS_BROADCAST_SPIN(eventToSend, broadcastSpin)
	eventToSend.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_UPDATE	// Lets the game know that this ball update isn't a serve
	eventToSend.iDummy0 = ENUM_TO_INT(eShot)
	SET_TENNIS_BROADCAST_BALL_UPDATE_ENUM(eventToSend, eUpdate)
	CPRINTLN(DEBUG_TENNIS, "Ball Position: ", vBallPos)
	CPRINTLN(DEBUG_TENNIS, "Ball Velocity: ", vBallVel)
	CPRINTLN(DEBUG_TENNIS, "Broadcast event, 'TENNIS_MP_BROADCAST_UPDATE_BALL' sent, ", GET_STRING_FROM_TENNIS_BALL_UPDATE_ENUM(eUpdate))
	#IF TENNIS_DEBUG_RECORDING
	DEBUG_RECORD_ENTITY_VECTOR(PLAYER_PED_ID(), "vBallPosBroadcastOut", vBallPos, TRUE)
	DEBUG_RECORD_ENTITY_VECTOR(PLAYER_PED_ID(), "vBallVelBroadcastOut", vBallVel, FALSE)
	DEBUG_RECORD_ENTITY_FLOAT(PLAYER_PED_ID(), "Frame Time", GET_FRAME_TIME())
	#ENDIF
	
	// Send our event off.
	TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	//bServerBroadcast should only be true when trying to correct a desync
	IF bServerBroadcast
		TENNIS_SEND_BROADCAST_EVENT(eventToSend, ALL_PLAYERS(FALSE))
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle messages being thrown to us.
/// PARAMS:

/// PURPOSE:
///    Handle messages being thrown to us.
/// PARAMS:
///    clientBD - The BroadcastData for the client calling this function
///    thisProps - 
///    thisBall - Used when setting the ball's velocity and position
///    iMyControlFlags - the bitfield of the client calling this function
///    eSelfID - the eSelfID for this client
PROC TENNIS_MP_PROCESS_BROADCAST_EVENTS(TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD[], TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_BALL &thisBall, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, TENNIS_PARTICIPANT_INFO & info )
	TENNIS_BROADCAST_EVENT eventDetails
	INT iCount
	INT iParticipantID = PARTICIPANT_ID_TO_INT()
	LAUNCH_DETAILS sDummy
	INT iSCTVFocusParticipantID = NATIVE_TO_INT( NETWORK_GET_HOST_OF_THIS_SCRIPT() )
	INT iTheirNetTime
	
	UNUSED_PARAMETER( iTheirNetTime )
	
	IF TENNIS_IS_PLAYER_SPECTATING()
		IF IS_ENTITY_A_PED( GET_SPECTATOR_CURRENT_FOCUS_PED() )
			iSCTVFocusParticipantID = NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX( NETWORK_GET_PLAYER_INDEX_FROM_PED( GET_SPECTATOR_CURRENT_FOCUS_PED() ) ) )
			CDEBUG3LN( DEBUG_TENNIS, "TENNIS_MP_PROCESS_BROADCAST_EVENTS - iSCTVFocusParticipantID = GET_SPECTATOR_CURRENT_FOCUS_PED - iSCTVFocusParticipantID = ", iSCTVFocusParticipantID)
		ELIF IS_ENTITY_A_PED( GET_SPECTATOR_DESIRED_FOCUS_PED() )
			iSCTVFocusParticipantID = NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX( NETWORK_GET_PLAYER_INDEX_FROM_PED( GET_SPECTATOR_DESIRED_FOCUS_PED() ) ) )
			CDEBUG3LN( DEBUG_TENNIS, "TENNIS_MP_PROCESS_BROADCAST_EVENTS - iSCTVFocusParticipantID = GET_SPECTATOR_CURRENT_FOCUS_PED - iSCTVFocusParticipantID = ", iSCTVFocusParticipantID)
		ELSE
			CDEBUG3LN( DEBUG_TENNIS, "TENNIS_MP_PROCESS_BROADCAST_EVENTS - iSCTVFocusParticipantID = NATIVE_TO_INT( NETWORK_GET_HOST_OF_THIS_SCRIPT() ) ")
		ENDIF
	ENDIF
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		// We just got an event we may care about.
		IF (GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount) = EVENT_NETWORK_SCRIPT_EVENT)
			GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, eventDetails, SIZE_OF(eventDetails))
			RESET_TENNIS_BALL_BOUNCE_TIMEOUT(thisProps.tennisBall)
			
			// Check for the event that tells us to apply a force to the ball.
			// might need to do spin data stuff later
			IF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_UPDATE
				CPRINTLN(DEBUG_TENNIS, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
				iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
				CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
				IF GET_TENNIS_BROADCAST_BALL_UPDATE_ENUM(eventDetails) = TBU_INITIAL_POSITION
					// Wipe TBF_INCREASE_SIM_SPEED clean on a new volley, we'll see if we want to set it again if we add this to MP (for lob returns)
					IF IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED)
						CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED)
					ENDIF
					SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_INITIAL_BALL_UPDATE_RECEIVED)
					SET_TENNIS_CLIENT_PRED_BALL_POS(clientBD[iParticipantID], GET_TENNIS_BROADCAST_BALL_POSITION(eventDetails))
					SET_TENNIS_CLIENT_PRED_BALL_VEL(clientBD[iParticipantID], GET_TENNIS_BROADCAST_BALL_VELOCITY(eventDetails))
					SET_TENNIS_CLIENT_PRED_BALL_SPIN(clientBD[iParticipantID], GET_TENNIS_BROADCAST_SPIN(eventDetails))
					RESET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
					#IF IS_DEBUG_BUILD
					VECTOR vTemp = GET_TENNIS_CLIENT_PRED_BALL_POS(clientBD[iParticipantID])
					CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_CLIENT_PRED_BALL_POS(clientBD)=", vTemp)
					vTemp = GET_TENNIS_CLIENT_PRED_BALL_VEL(clientBD[iParticipantID])
					CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_CLIENT_PRED_BALL_VEL(clientBD)=", vTemp)
					CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_CLIENT_PRED_BALL_SPIN(clientBD)=", GET_STRING_FROM_TENNIS_BALL_SPIN(GET_TENNIS_CLIENT_PRED_BALL_SPIN(clientBD[iParticipantID])))
					#ENDIF
				ELIF GET_TENNIS_BROADCAST_BALL_UPDATE_ENUM(eventDetails) = TBU_BALL_VELOCITY_VECTOR
					SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_BALL_VELOCITY_UPDATE_RECEIVED)
					iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
					INT iOurNetTime = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
					INT iCatchupTime = iOurNetTime - iTheirNetTime
					SET_TENNIS_CLIENT_BALL_CATCHUP_TIME(clientBD[iParticipantID], iCatchupTime)
					SET_TENNIS_CLIENT_PRED_BALL_POS(clientBD[iParticipantID], GET_TENNIS_BROADCAST_BALL_POSITION(eventDetails))
					SET_TENNIS_CLIENT_PRED_BALL_VEL(clientBD[iParticipantID], GET_TENNIS_BROADCAST_BALL_VELOCITY(eventDetails))
					SET_TENNIS_CLIENT_PRED_BALL_SPIN(clientBD[iParticipantID], GET_TENNIS_BROADCAST_SPIN(eventDetails))
					CPRINTLN(DEBUG_TENNIS, "iCatchupTime=", GET_TENNIS_CLIENT_BALL_CATCHUP_TIME(clientBD[iParticipantID]), ", iTheirNetTime=", iTheirNetTime, ", iOurNetTime=", iOurNetTime)
					#IF IS_DEBUG_BUILD
					VECTOR vTemp = GET_TENNIS_CLIENT_PRED_BALL_POS(clientBD[iParticipantID])
					CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_CLIENT_PRED_BALL_POS(clientBD)=", vTemp)
					vTemp = GET_TENNIS_CLIENT_PRED_BALL_VEL(clientBD[iParticipantID])
					CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_CLIENT_PRED_BALL_VEL(clientBD)=", vTemp)
					CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_CLIENT_PRED_BALL_SPIN(clientBD)=", GET_STRING_FROM_TENNIS_BALL_SPIN(GET_TENNIS_CLIENT_PRED_BALL_SPIN(clientBD[iParticipantID])))
					#ENDIF
				ELIF GET_TENNIS_BROADCAST_BALL_UPDATE_ENUM(eventDetails) = TBU_NET_RICOCHET
					IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
						IF IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
							CDEBUG2LN(DEBUG_TENNIS, "TBU_NET_RICOCHET Detaching entity")
							DETACH_ENTITY(thisProps.tennisBall.oBall)
						ENDIF
						thisProps.tennisBall.vNext = GET_TENNIS_BROADCAST_BALL_POSITION(eventDetails)
						SET_ENTITY_COORDS(thisProps.tennisBall.oBall, GET_TENNIS_BROADCAST_BALL_POSITION(eventDetails))
						SET_ENTITY_VELOCITY(thisProps.tennisBall.oBall, GET_TENNIS_BROADCAST_BALL_VELOCITY(eventDetails))
					ENDIF
					
					SET_TENNIS_BALL_POS(thisProps.tennisBall, GET_TENNIS_BROADCAST_BALL_POSITION(eventDetails))
					thisProps.tennisBall.vBallVel = GET_TENNIS_BROADCAST_BALL_VELOCITY(eventDetails)
					IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iParticipantID], TMC_INITIAL_BALL_UPDATE_RECEIVED)
						CLEAR_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_INITIAL_BALL_UPDATE_RECEIVED)
					ENDIF
					IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iParticipantID], TMC_BALL_VELOCITY_UPDATE_RECEIVED)
						CLEAR_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_BALL_VELOCITY_UPDATE_RECEIVED)
					ENDIF
					CPRINTLN(DEBUG_TENNIS, "thisProps.tennisBall.vNext=", thisProps.tennisBall.vNext)
					CPRINTLN(DEBUG_TENNIS, "thisProps.tennisBall.vBallVel=", thisProps.tennisBall.vBallVel)
				ENDIF
				SET_TENNIS_BALL_INTO_PLAYER_FRAME_STAMP(thisProps.tennisBall, GET_FRAME_COUNT() + COLLIDE_WITH_PLAYER_THRESH)
				CLEAR_TENNIS_BALL_FLAG(thisBall, TBF_HIT_PLAYER)
				SET_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eOtherID], INT_TO_ENUM(SHOT_TYPE_ENUM, eventDetails.iDummy0))
				
				STRING sAnimDict = PICK_STRING(IS_TENNIS_PED_MALE((thisProps.tennisPlayers[eSelfID])), "mini@tennis", "mini@tennis@female")
				// Latency may mean we get the ball hit to us before we're done serving. This should account for all that and let the game continue.
				// We handle it here, immediately, so there's no chance of bitmasks being flipped between now and when TMC_BALL_HIT is processed.
				IF IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), sAnimDict, "serve")
					IF IS_TIMER_STARTED(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
						TENNIS_PAUSE_TIMER(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
					ENDIF
					SET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID], TSS_NO_SWING)
					STOP_ANIM_TASK(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), sAnimDict, "serve")
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_SERVE_GRADE_FLAG)
					// Increase the serve count
					SET_TENNIS_MP_SERVE_COUNT(serverBD, GET_TENNIS_MP_SERVE_COUNT(serverBD)+1)
					// Clear the serve prepared bitmask so we update strokes normally
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_SERVE_PREPARED)
					IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
						IF IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
							CDEBUG2LN(DEBUG_TENNIS, "Detaching entity, ", GET_STRING_FROM_TENNIS_BALL_UPDATE_ENUM(GET_TENNIS_BROADCAST_BALL_UPDATE_ENUM(eventDetails)))
							DETACH_ENTITY(thisProps.tennisBall.oBall)
						ENDIF
					ENDIF
					CPRINTLN(DEBUG_TENNIS, "Stopping the serve anim, hit was broadcast to us.")
				ENDIF
				CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_BROADCAST_BALL_UPDATE_ENUM(eventDetails)=", GET_STRING_FROM_TENNIS_BALL_UPDATE_ENUM(GET_TENNIS_BROADCAST_BALL_UPDATE_ENUM(eventDetails)))
				CPRINTLN(DEBUG_TENNIS, "Broadcast event 'SCRIPT_EVENT_BALL_UPDATE - Normal Hit' processed.")
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_REQUEST_UPDATE
				CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_REQUEST_UPDATE caught from ", eventDetails.iLRAngle )
				iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
				CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
				IF NOT TENNIS_IS_PLAYER_SPECTATING()
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_REQUEST_UPDATE processing from ", eventDetails.iLRAngle )
					TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_FLAGS( thisUIData )
					TENNIS_MP_BROADCAST_SCTV_UI_SET_CAMERA_STATE( GET_TENNIS_CAMERA_STATE( thisUIData.tennisCameras ), thisUIData.tennisCameras.bSetChosenCam  )
					IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_PLAYER_CARD_SHOULD_SHOW)
						TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_POINTS( FALSE )
						TENNIS_MP_BROADCAST_SCTV_UI_SHOW_PLAYER_CARD()
					ELIF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SCOREBOARD_SHOULD_SHOW)
						TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_POINTS( TRUE )
						TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SCOREBOARD()
					ENDIF					
				ENDIF
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_CREATE
				IF TENNIS_IS_PLAYER_SPECTATING()
					iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
					CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_CREATE caught from ", eventDetails.iLRAngle )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_CREATE iSCTVFocusParticipantID = ", iSCTVFocusParticipantID )
					DEBUG_SPAWN_BALL(thisProps)
					ATTACH_TENNIS_BALL_TO_PLAYER(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)], thisProps.tennisBall)
				ENDIF				
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_POINTS
				IF TENNIS_IS_PLAYER_SPECTATING()
					iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
					CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_POINTS caught from ", eventDetails.iLRAngle )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_POINTS iSCTVFocusParticipantID = ", iSCTVFocusParticipantID )
					IF eventDetails.iLRAngle = iSCTVFocusParticipantID
						SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_UPDATE_SCORES)
						UPDATE_TENNIS_UI_POINTS(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), info, eventDetails.bDummy, eventDetails.bDummy)
					ENDIF
				ENDIF		
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_PLAYER_CARD
				IF TENNIS_IS_PLAYER_SPECTATING()
					iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
					CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_PLAYER_CARD caught from ", eventDetails.iLRAngle )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_PLAYER_CARD iSCTVFocusParticipantID = ", iSCTVFocusParticipantID )
					SHOW_PLAYER_CARD_NO_MATTER_WHAT(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), info, FALSE, FALSE)
				ENDIF		
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SCOREBOARD
				IF TENNIS_IS_PLAYER_SPECTATING()
					iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
					CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SCOREBOARD caught from ", eventDetails.iLRAngle )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SCOREBOARD iSCTVFocusParticipantID = ", iSCTVFocusParticipantID )
					SHOW_SCOREBOARD_NO_MATTER_WHAT(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), info, FALSE, TRUE)
				ENDIF		
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SERVE_MESSAGE
				IF TENNIS_IS_PLAYER_SPECTATING()
					iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
					CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SERVE_MESSAGE caught from ", eventDetails.iLRAngle )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SERVE_MESSAGE iSCTVFocusParticipantID = ", iSCTVFocusParticipantID )
					IF eventDetails.iLRAngle = iSCTVFocusParticipantID
						TENNIS_PLAYER_ID ePlayerID = INT_TO_ENUM( TENNIS_PLAYER_ID, eventDetails.iDummy0 )
						SHOW_TENNIS_SERVE_MESSAGE(thisUIData.tennisUI, GET_TENNIS_PLAYER_NAME(thisUIData, ePlayerID), ePlayerID = eSelfID)
					ENDIF
				ENDIF	
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SECOND_SERVE_MESSAGE
				IF TENNIS_IS_PLAYER_SPECTATING()
					iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
					CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SECOND_SERVE_MESSAGE caught from ", eventDetails.iLRAngle )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SECOND_SERVE_MESSAGE iSCTVFocusParticipantID = ", iSCTVFocusParticipantID )
					IF eventDetails.iLRAngle = iSCTVFocusParticipantID
						SHOW_TENNIS_SECOND_SERVE_MESSAGE(thisUIData.tennisUI)
					ENDIF
				ENDIF	
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SIDE_CHANGE
				IF TENNIS_IS_PLAYER_SPECTATING()
					iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
					CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SIDE_CHANGE caught from ", eventDetails.iLRAngle )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SHOW_SIDE_CHANGE iSCTVFocusParticipantID = ", iSCTVFocusParticipantID )
					IF eventDetails.iLRAngle = iSCTVFocusParticipantID
						SHOW_TENNIS_SIDE_CHANGE_MESSAGE(thisUIData.tennisUI)
					ENDIF
				ENDIF	
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_UTIL_FLAGS
				IF TENNIS_IS_PLAYER_SPECTATING()
					iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
					CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_UTIL_FLAGS caught from ", eventDetails.iLRAngle )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_UTIL_FLAGS iSCTVFocusParticipantID = ", iSCTVFocusParticipantID )
					IF eventDetails.iLRAngle = iSCTVFocusParticipantID
						thisUIData.iUtilFlags = eventDetails.iDummy0
						
						IF thisUIData.iUtilFlags != thisUIData.iPrevUtilFlags
							thisUIData.iPrevUtilFlags = thisUIData.iUtilFlags
						ENDIF
						
						HANDLE_BITMASK_UI_FOR_SCTV(thisUIData, 
						  thisProps, 
						  serverBD.playerScores, 
						  GET_TENNIS_MP_RULESET(serverBD),
						  GET_TENNIS_MP_WHO_IS_SERVING(serverBD), 
						  GET_TENNIS_MP_SCORED_LAST(serverBD), 
						  GET_TENNIS_MP_SERVE_COUNT(serverBD), 
						  GET_TENNIS_MP_NUM_SETS(serverBD), 
						  GET_TENNIS_MP_CURRENT_SET(serverBD), 
						  eSelfID, 
						  eOtherID)
						
					ENDIF
				ENDIF
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_UI_FLAGS
				IF TENNIS_IS_PLAYER_SPECTATING()
					iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
					CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_UI_FLAGS caught from ", eventDetails.iLRAngle )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_UPDATE_UI_FLAGS iSCTVFocusParticipantID = ", iSCTVFocusParticipantID )
					IF eventDetails.iLRAngle = iSCTVFocusParticipantID
						thisUIData.tennisUI.iUIFlags = eventDetails.iDummy0
						
						IF thisUIData.tennisUI.iUIFlags != thisUIData.tennisUI.iPrevUIFlags
						
							IF NOT HAS_SCALEFORM_MOVIE_LOADED(thisUIData.tennisUI.scorePanelSF)
								EXIT
							ENDIF
						
							IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_PLAYER_CARD_SHOULD_SHOW)
								AND IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iPrevUIFlags, TUF_PLAYER_CARD_SHOULD_SHOW)
								DISPLAY_PLAYER_CARD(thisUIData.tennisUI.scorepanelSF, FALSE)
							ENDIF
							
							IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_PLAYER_CARD_SHOULD_SHOW)
								AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iPrevUIFlags, TUF_PLAYER_CARD_SHOULD_SHOW)
								SHOW_PLAYER_CARD_NO_MATTER_WHAT(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), info, FALSE, FALSE)
							ENDIF
							
							IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SCOREBOARD_SHOULD_SHOW)
								AND IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iPrevUIFlags, TUF_SCOREBOARD_SHOULD_SHOW)
								DISPLAY_SCOREBOARD(thisUIData.tennisUI.scorepanelSF, FALSE)
							ENDIF
							
							IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SCOREBOARD_SHOULD_SHOW)
								AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iPrevUIFlags, TUF_SCOREBOARD_SHOULD_SHOW)
								SHOW_SCOREBOARD_NO_MATTER_WHAT(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), info, FALSE, TRUE)
							ENDIF
							
							thisUIData.tennisUI.iPrevUIFlags = thisUIData.tennisUI.iUIFlags

						ENDIF
						
						HANDLE_BITMASK_UI_FOR_SCTV(thisUIData, 
						  thisProps, 
						  serverBD.playerScores, 
						  GET_TENNIS_MP_RULESET(serverBD),
						  GET_TENNIS_MP_WHO_IS_SERVING(serverBD), 
						  GET_TENNIS_MP_SCORED_LAST(serverBD), 
						  GET_TENNIS_MP_SERVE_COUNT(serverBD), 
						  GET_TENNIS_MP_NUM_SETS(serverBD), 
						  GET_TENNIS_MP_CURRENT_SET(serverBD), 
						  eSelfID, 
						  eOtherID)
						
					ENDIF
				ENDIF
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_LAUNCH
				IF TENNIS_IS_PLAYER_SPECTATING()
					iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
					CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_LAUNCH caught from ", eventDetails.iLRAngle )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_LAUNCH iSCTVFocusParticipantID = ", iSCTVFocusParticipantID )
					
					iParticipantID = eventDetails.iLRAngle
	
					DETACH_ENTITY(thisProps.tennisBall.oBall,FALSE)
					sDummy.iForce = eventDetails.iDummy0
					sDummy.iAngle = eventDetails.iDummy1
					thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)].fLeftRight = eventDetails.fDummy
					thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)].vMyForward = eventDetails.vVelocity
					thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)].vMyRight = eventDetails.vPosition
					
					LAUNCH_BALL(thisProps, thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)], sDummy)
					
					IF GET_TENNIS_CLIENT_SERVE_GRADE(clientBD[iParticipantID]) = LD_SERVE_S OR GET_TENNIS_CLIENT_SERVE_GRADE(clientBD[iParticipantID]) = LD_SERVE_S_PLUS
						PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SMASH_MASTER", GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING( serverBD )]))
					ELSE
						PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SERVE_MASTER", GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING( serverBD )]))
					ENDIF
					SET_TENNIS_BALL_BOUNCE_FRAME_STAMP(thisProps.tennisBall, GET_TENNIS_FRAME_COUNTER(thisProps))
										
					//Get my prediction here and set the new X-Marker
					VECTOR vDestTest
					vDestTest = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(thisProps.tennisCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall), thisProps.tennisBall.vBallVel, 
						thisProps.vForward, GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), GET_TENNIS_BALL_SPIN(thisProps.tennisBall), GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
					SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)], vDestTest)
					
					// Display the X marker
					IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_X_MARKER)
						SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_X_MARKER)
						CPRINTLN(DEBUG_TENNIS, "Showing where the ball will land")
					ENDIF
				
				ENDIF
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SERVE
				CPRINTLN(DEBUG_TENNIS, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
				iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
				CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
				SET_TENNIS_BALL_INTO_PLAYER_FRAME_STAMP(thisProps.tennisBall, GET_FRAME_COUNT() + COLLIDE_WITH_PLAYER_THRESH)
				SET_TENNIS_CLIENT_SERVE_GRADE(clientBD[iParticipantID], GET_TENNIS_BROADCAST_SERVE_GRADE(eventDetails))
				SET_TENNIS_PLAYER_ANALOG_X_VALUE(thisProps.tennisPlayers[eSelfID], GET_TENNIS_BROADCAST_SERVE_ANGLE(eventDetails))
				SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_SERVE_GRADE_FLAG)
				SET_TENNIS_BALL_BOUNCE_FRAME_STAMP(thisBall, GET_TENNIS_FRAME_COUNTER(thisProps))
				UPDATE_TENNIS_BALL_TIME_SLICE_VARIABLES(thisBall, FALSE)
				SET_TENNIS_BALL_SERVE_SAFETY_POS(thisBall, GET_TENNIS_BROADCAST_BALL_POSITION(eventDetails))
				IF GET_TENNIS_BROADCAST_SERVE_GRADE(eventDetails) = LD_SERVE_S_PLUS
					SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED)
				ELIF IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED)
					CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED)
				ENDIF
				
				CPRINTLN(DEBUG_TENNIS, "iParticipantID: ", iParticipantID)
				CPRINTLN(DEBUG_TENNIS, "eServeGrade: ", GET_STRING_FROM_TENNIS_SERVE_GRADE(GET_TENNIS_BROADCAST_SERVE_GRADE(eventDetails)))
				CPRINTLN(DEBUG_TENNIS, "Broadcast event 'SCRIPT_EVENT_BALL_UPDATE - Serve' processed.")
				
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SEQ_DONE
				CPRINTLN(DEBUG_TENNIS, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
				iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
				CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
				SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_OPP_SEQUENCE_DONE)
				IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID])) AND NOT IS_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
					SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), TRUE)
				ENDIF
				CPRINTLN(DEBUG_TENNIS, "Broadcast event 'SCRIPT_EVENT_BALL_UPDATE - BALL_UPDATE_IMPACT_TYPE_SEQ_DONE' processed.")
				
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_BALL_DESYNC
				CPRINTLN(DEBUG_TENNIS, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
				iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
				CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
				SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, eOtherID)
				IF NOT TENNIS_IS_PLAYER_SPECTATING()
					SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[iParticipantID], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
				ENDIF
				IF IS_TIMER_STARTED(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
					TENNIS_PAUSE_TIMER(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
				ENDIF
				CPRINTLN(DEBUG_TENNIS, "Broadcast event 'SCRIPT_EVENT_BALL_UPDATE - BALL_UPDATE_IMPACT_TYPE_BALL_DESYNC' processed.")
				
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_QUIT
				CPRINTLN(DEBUG_TENNIS, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
				iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
				CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
				IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
					DETACH_ENTITY(thisProps.tennisBall.oBall)
				ENDIF
				
				IF IS_TIMER_STARTED( thisProps.tennisPlayers[eSelfID].meterDelayTimer )
					TENNIS_PAUSE_TIMER(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
				ENDIF
				SWING_METER_DISPLAY(thisUIData.tennisUI, FALSE, 0)
				
				STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
				
				CLEAR_HELP()
				SET_TENNIS_TIMER_A(thisProps, 0)
				IF GET_TENNIS_QUIT_EVENT_SERVER_FLAG(eventDetails) = 0
					CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
					SHOW_TENNIS_QUITTER_UI(thisUIData.tennisUI)
					SET_TENNIS_FLOW_FLAG(thisProps, TFF_CONTINUE_AFTER_VOTE_PASSING)
					IF WAS_TENNIS_PLAYER_WINNING(serverBD, eSelfID, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eSelfID)))
						SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PLAY_TENNIS)
						SET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[iParticipantID], AR_playerWon)
						SET_TENNIS_UI_BETS_DATA(thisUIData.tennisUI, BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID()))
						CREATE_TENNIS_BETTING_SCOREBOARD(thisUIData.tennisUI, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt), TRUE)
					ELIF MPGlobals.g_MPBettingData.iYourTotalBet > 0
						CDEBUG2LN(DEBUG_TENNIS, "BROADCAST_BETTING_MISSION_FINISHED_TIED() SCRIPT_EVENT_QUIT event received")
						BROADCAST_BETTING_MISSION_FINISHED_TIED()
					ENDIF
					DETACH_ENTITY(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)].oTennisRacket)
					IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)]))
						TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)]), "mini@tennis", "ready_2_idle")
						SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)]), FALSE)
					ENDIF
					SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_OPP_OUT_OF_TENNIS_MODE)
					IF NOT TENNIS_IS_PLAYER_SPECTATING()
						SET_TENNIS_CLIENT_MP_STATE(clientBD[iParticipantID], TMPS_QUIT_THE_GAME)
					ENDIF
				ELIF GET_TENNIS_QUIT_EVENT_SERVER_FLAG(eventDetails) = 1
					SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
					SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_SHOW_QUIT_SCOREBOARD)
					HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
					IF NOT TENNIS_IS_PLAYER_SPECTATING()
						SET_TENNIS_CLIENT_MP_STATE(clientBD[iParticipantID], TMPS_NO_REMATCH)
					ENDIF
				ELIF GET_TENNIS_QUIT_EVENT_SERVER_FLAG(eventDetails) = 2
					CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
					HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
					SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_SHOW_QUIT_LEADERBOARD)
					SET_LOADING_ICON_INACTIVE()
					IF NOT TENNIS_IS_PLAYER_SPECTATING()
						SET_TENNIS_CLIENT_MP_STATE(clientBD[iParticipantID], TMPS_WAIT_FOR_PREPARE_REMATCH)
						SET_TENNIS_CLIENT_MP_STATE(clientBD[iParticipantID], TMPS_NO_REMATCH)
					ENDIF
				ENDIF
				CPRINTLN(DEBUG_TENNIS, "Broadcast event 'SCRIPT_EVENT_QUIT' processed, GET_TENNIS_QUIT_EVENT_SERVER_FLAG(eventDetails)=", GET_TENNIS_QUIT_EVENT_SERVER_FLAG(eventDetails))
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_FAULT
				CPRINTLN(DEBUG_TENNIS, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
				iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
				CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
				INT iServeCount = GET_TENNIS_BROADCAST_SERVE_COUNT(eventDetails)
				//SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_FAULT) Commented out for now while scores aren't updating well
				IF iServeCount >= 2
					IF NOT TENNIS_IS_PLAYER_SPECTATING()
						SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[iParticipantID], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
					ENDIF
					SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eSelfID)))
					SHOW_TENNIS_DOUBLE_FAULT_UI(thisUIData.tennisUI, eSelfID = GET_TENNIS_MP_WHO_IS_SERVING(serverBD))
				ELIF iServeCount >= 1
					IF NOT TENNIS_IS_PLAYER_SPECTATING()
						SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[iParticipantID], TGSE_INTERSTITIAL, TMPS_WAIT_FOR_INTERSTITIAL)
					ENDIF
					SET_TENNIS_TIMER_A(thisProps, 0)
					HUD_COLOURS eHudColor = HUD_COLOUR_WHITE
					IF eSelfID = GET_TENNIS_MP_WHO_IS_SERVING(serverBD)
						eHudColor = HUD_COLOUR_RED
					ENDIF
					SHOW_TENNIS_FAULT_UI(thisUIData.tennisUI, eHudColor)
					SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SECOND_SERVE)
					SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_FIRST_FAULT)
				ELSE
					CERRORLN(DEBUG_TENNIS, "Serve Count was less than 1, something broke.")
					SCRIPT_ASSERT("Serve Count was less than 1, Contact Rob Pearsall.")
				ENDIF
				SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_SERVER_FAULTED)
				CPRINTLN(DEBUG_TENNIS, "Broadcast event 'SCRIPT_EVENT_FAULT' processed")
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_OOB
				CPRINTLN(DEBUG_TENNIS, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
				iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
				CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
				SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_OOB)
				INCREASE_TENNIS_MP_STAT(TMST_TENNIS_TOTAL_OOB_HITS)
				SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, GET_TENNIS_BROADCAST_POINT_WINNER(eventDetails))
				IF NOT TENNIS_IS_PLAYER_SPECTATING()
					SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[iParticipantID], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
				ENDIF
				CPRINTLN(DEBUG_TENNIS, "Broadcast event 'SCRIPT_EVENT_OOB' processed")
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_POINT_WON
				CPRINTLN(DEBUG_TENNIS, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
				iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
				CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
				SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, GET_TENNIS_BROADCAST_POINT_WINNER(eventDetails))
				IF NOT TENNIS_IS_PLAYER_SPECTATING()
					SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[iParticipantID], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
				ENDIF
				IF IS_TIMER_STARTED(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
					TENNIS_PAUSE_TIMER(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
				ENDIF
				CPRINTLN(DEBUG_TENNIS, "Broadcast event 'SCRIPT_EVENT_POINT_WON' processed")
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_HIT_BEFORE_BOUNCE
				CPRINTLN(DEBUG_TENNIS, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
				iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
				CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
				IF GET_TENNIS_BEFORE_BOUNCE_HIT_BY_FLAG(eventDetails) = 1
					SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, eSelfID)
					CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ACE)
					IF NOT TENNIS_IS_PLAYER_SPECTATING()
						SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[iParticipantID], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
					ENDIF
					CPRINTLN(DEBUG_TENNIS, "Broadcast event 'TENNIS_MP_BROADCAST_HIT_BY_BALL' processed")
				ELSE
					SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_HIT_BEFORE_BOUNCE)
					SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_HIT_BEFORE_BOUNCE)
					CPRINTLN(DEBUG_TENNIS, "Broadcast event 'SCRIPT_EVENT_HIT_BEFORE_BOUNCE' processed")
				ENDIF
				IF IS_TIMER_STARTED(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
					TENNIS_PAUSE_TIMER(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
				ENDIF
			ELIF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_GET_OFF_MY_SIDE
				CPRINTLN(DEBUG_TENNIS, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
				iTheirNetTime = GET_TENNIS_BROADCAST_TIME_DATATYPE(eventDetails)
				CPRINTLN( DEBUG_TENNIS, "GET_TENNIS_BROADCAST_TIME_DATATYPE in the event is ", iTheirNetTime )
				IF NOT TENNIS_IS_PLAYER_SPECTATING()
					SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[iParticipantID], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
				ENDIF
				SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - GET_TENNIS_BROADCAST_OFFENDER(eventDetails)))
				RESTART_TIMER_NOW(thisUIData.tennisUI.sUITimer)
				SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_FOULED)
				CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ACE)
				RESET_TENNIS_PLAYER_SWING_TIMER(thisProps.tennisPlayers[eSelfID])
				IF IS_TIMER_STARTED(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
					TENNIS_PAUSE_TIMER(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
				ENDIF
				IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
					DETACH_ENTITY(thisProps.tennisBall.oBall)
				ENDIF
				SET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID], TSS_NO_SWING)
				STRING sAnimDict = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eSelfID]), "mini@tennis", "mini@tennis@female")
				STRING sAnim = "react_ball_out"
				#IF TENNIS_DEBUG_RECORDING
				DEBUG_RECORD_STRING("TASK_PLAY_ANIM", "react_ball_out")
				DEBUG_RECORD_STRING("TASK_PLAY_ANIM", sAnim)
				#ENDIF
				IF NOT TENNIS_IS_PLAYER_SPECTATING()
					TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, sAnim)
				ENDIF
				CPRINTLN(DEBUG_TENNIS, "Broadcast event 'SCRIPT_EVENT_GET_OFF_MY_SIDE' processed")
			ENDIF
			IF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) >= SCRIPT_EVENT_BALL_UPDATE AND GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) <= SCRIPT_EVENT_QUIT
				CDEBUG1LN(DEBUG_TENNIS, "netTime = ", eventDetails.iNetworkTime )
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
