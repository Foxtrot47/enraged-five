USING "tennis.sch"
USING "net_mission.sch"
USING "tennis_core_lib.sch"

//Including these so intellisense works, I'm tired of not having intellisense
USING "tennis_mp.sch"
USING "tennis_client_lib.sch"
USING "tennis_server_lib.sch"
USING "tennis_broadcast_lib.sch"
USING "fmmc_mp_setup_mission.sch"
USING "tennis_leaderboard_lib.sch"
USING "achievement_public.sch"

PROC HANDLE_TENNIS_MP_INTERSTITIALS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_CLIENT_BD &clientBD, TENNIS_PLAYER_ID eSelfID, BOOL bHappy, TENNIS_PARTICIPANT_INFO &info)
	TENNIS_INTERSTITIAL_ACTOR eActor
	BOOL bMalePed = IS_TENNIS_PED_MALE((thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)]))
	STRING sAnimDict = PICK_STRING(bMalePed, "mini@tennis", "mini@tennis@female")
	IF bMalePed
		eActor = TENNIS_ACTOR_GENERIC_MALE
		CDEBUG2LN(DEBUG_TENNIS, "HANDLE_TENNIS_MP_INTERSTITIALS :: eActor = TENNIS_ACTOR_GENERIC_MALE")
	ELSE
		eActor = TENNIS_ACTOR_GENERIC_FEMALE
		CDEBUG2LN(DEBUG_TENNIS, "HANDLE_TENNIS_MP_INTERSTITIALS :: eActor = TENNIS_ACTOR_GENERIC_FEMALE")
	ENDIF
	TENNIS_HIDE_CLIENT_CLONE(clientBD, eSelfID, FALSE)
	SET_TENNIS_CLIENT_FLAG(clientBD, TMC_MAKE_PLAYERS_INVISIBLE)
	IF NOT IS_PED_INJURED(clientBD.pedClones[eSelfID])
		ATTACH_ENTITY_TO_ENTITY(thisProps.tennisPlayers[eSelfID].oTennisRacket, clientBD.pedClones[eSelfID], 
			GET_PED_BONE_INDEX(clientBD.pedClones[eSelfID], BONETAG_PH_R_HAND), (<<0,0,0>>), (<<0,0,0>>))	//, RACKET_OFFSET, RACKET_ROTATION)
	ENDIF
	SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( info.iPlayers[0] ) ) )
	SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( info.iPlayers[1] ) ) )
	
	thisUIData.sInterstitialFaceDict = GET_TENNIS_FACIAL_DICT(FALSE, bHappy, !bMalePed)
	GET_TENNIS_INTERSTITIAL_ANIMATIONS(thisUIData, eActor, bHappy)
	IF NOT IS_PED_INJURED(clientBD.pedClones[eSelfID]) 
		SET_TENNIS_INTERSTITIAL_PLAYER(thisUIData, eSelfID)
		SET_FORCE_FOOTSTEP_UPDATE(clientBD.pedClones[eSelfID], TRUE)
		IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
			DETACH_ENTITY(thisProps.tennisBall.oBall)
		ENDIF
		VECTOR vNewPos
		vNewPos = thisProps.tennisCourt.vCenterCourt + (thisProps.tennisPlayers[eSelfID].vMyForward * -5.0)
		IF NOT IS_PED_INJURED(clientBD.pedClones[eSelfID])
			SET_ENTITY_COORDS(clientBD.pedClones[eSelfID], vNewPos)
			SET_ENTITY_HEADING(clientBD.pedClones[eSelfID], GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eSelfID].vMyForward.x, thisProps.tennisPlayers[eSelfID].vMyForward.y))
		ENDIF
		FORCE_PED_AI_AND_ANIMATION_UPDATE(clientBD.pedClones[eSelfID])
	#IF IS_DEBUG_BUILD
	ELSE
		CERRORLN(DEBUG_TENNIS, "HANDLE_TENNIS_MP_INTERSTITIALS :: IS_PED_INJURED(clientBD.pedClones[eSelfID]) = TRUE")
	#ENDIF
	ENDIF
	
//	SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
	
	VECTOR vScenePos = thisProps.tennisCourt.vCenterCourt
	VECTOR vSceneRot = <<0,0,GET_HEADING_FROM_VECTOR_2D(thisProps.vForward.x, thisProps.vForward.y)>>
	
	IF thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)].eCourtSide = TENNIS_PLAYER_HOME
		vScenePos += 5.0 * thisProps.tennisCourt.vForwardNorm
		vSceneRot.z += 180.0
	ELSE
		vScenePos -= 5.0 * thisProps.tennisCourt.vForwardNorm
	ENDIF
		
	thisUIData.iSceneID = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneRot)
	CDEBUG2LN(DEBUG_TENNIS, "HANDLE_TENNIS_MP_INTERSTITIALS Starting interstitial at vScenePos=", vScenePos)
	
	IF DOES_CAM_EXIST(thisUIData.tennisCameras.tennisCamTut)
		DESTROY_CAM(thisUIData.tennisCameras.tennisCamTut)
	ENDIF
	thisUIData.tennisCameras.tennisCamTut = CREATE_CAM("DEFAULT_ANIMATED_CAMERA")
	
	PLAY_SYNCHRONIZED_CAM_ANIM(thisUIData.tennisCameras.tennisCamTut, thisUIData.iSceneID, thisUIData.sInterstitialCamAnim, thisUIData.sInterstitialDict)
	SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	TASK_PLAY_ANIM(clientBD.pedClones[eSelfID], sAnimDict, thisUIData.sInterstitialPlrAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)
	TASK_PLAY_ANIM(clientBD.pedClones[eSelfID], thisUIData.sInterstitialFaceDict, thisUIData.sInterstitialFaceAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_SECONDARY | AF_NOT_INTERRUPTABLE)
	FLOAT fOutroLength = GET_TENNIS_REACT_ANIM_TIME(thisUIData.sInterstitialPlrAnim) * 1000
	SET_TENNIS_CLIENT_OUTRO_STAMP(clientBD, GET_GAME_TIMER() + ROUND(fOutroLength * 0.5))
	SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_QUICK_CUT, FALSE)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisProps - 
///    serverBD - 
///    clientBD - 
///    eTargetID - The ID of the ped we want to put in tennis mode, for players this is just the other guy
///    info - 
/// RETURNS:
///    
FUNC BOOL VALIDATE_TENNIS_MODE_ON_OPPONENT(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eTargetID, TENNIS_PARTICIPANT_INFO &info)
	IF NOT VALIDATE_TENNIS_PLAYERS_INFO(info)
		RETURN FALSE
	ENDIF
	IF IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eTargetID], TPB_FORCED_UPDATE)
//		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT :: TRUE from TPB_FORCED_UPDATE")
		RETURN TRUE
	ENDIF
	IF IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eTargetID].playerAI, TAF_PREVENT_MP_TENNIS_MODE_UPDATE)
//		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT :: IS_TENNIS_AI_FLAG_SET(", thisProps.tennisPlayers[eTargetID].playerAI.texName, ", TAF_PREVENT_MP_TENNIS_MODE_UPDATE)")
		RETURN FALSE
	ENDIF
	IF IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eTargetID]))
//		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT :: IS_PED_INJURED(", thisProps.tennisPlayers[eTargetID].playerAI.texName, ")")
		RETURN FALSE
	ENDIF
	IF NOT IS_ENTITY_DEAD(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eTargetID])) AND IS_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eTargetID]))
//		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT :: IS_TENNIS_MODE(", thisProps.tennisPlayers[eTargetID].playerAI.texName, ")")
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eTargetID]) <> TSS_NO_SWING
//		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT :: GET_TENNIS_PLAYER_SWING_STATE(", thisProps.tennisPlayers[eTargetID].playerAI.texName, ") <> TSS_NO_SWING")
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_SIDE_CHANGE
//		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT :: GET_TENNIS_SERVER_GAME_STATE = TGSE_SIDE_CHANGE")
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_INTERSTITIAL
//		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT :: GET_TENNIS_SERVER_GAME_STATE = TGSE_INTERSTITIAL")
		RETURN FALSE
	ENDIF
//	PRINT_TENNIS_PARTICIPANT_INFO(info)
	CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT :: eTargetID=", GET_STRING_FROM_TENNIS_PLAYER_ID(eTargetID))
	IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eTargetID]], TMC_WALKING_TO_POS)
//		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT :: IS_TENNIS_CLIENT_FLAG_SET(", thisProps.tennisPlayers[eTargetID].playerAI.texName, ", TMC_WALKING_TO_POS)")
		RETURN FALSE
	ENDIF
	IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[1 - ENUM_TO_INT(eTargetID)]], TMC_OPP_OUT_OF_TENNIS_MODE)
//		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT :: IS_TENNIS_CLIENT_FLAG_SET(", thisProps.tennisPlayers[1 - ENUM_TO_INT(eTargetID)].playerAI.texName, ", TMC_OPP_OUT_OF_TENNIS_MODE)")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL VALIDATE_TENNIS_MODE_ON_OPPONENT_FOR_SCTV(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eTargetID, TENNIS_PARTICIPANT_INFO &info, INT iMyParticipantID)
	IF NOT VALIDATE_TENNIS_PLAYERS_INFO(info)
		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT_FOR_SCTV :: NOT VALIDATE_TENNIS_PLAYERS_INFO(info)")
		RETURN FALSE
	ENDIF
	IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_OPP_SEQUENCE_DONE)
		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT_FOR_SCTV :: TMC_OPP_SEQUENCE_DONE returning TRUE!")
		RETURN TRUE
	ENDIF
	IF GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_SIDE_CHANGE
		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT_FOR_SCTV :: GET_TENNIS_SERVER_GAME_STATE = TGSE_SIDE_CHANGE")
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_INTERSTITIAL
		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT_FOR_SCTV :: GET_TENNIS_SERVER_GAME_STATE = TGSE_INTERSTITIAL")
		RETURN FALSE
	ENDIF
//	PRINT_TENNIS_PARTICIPANT_INFO(info)
//	CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT :: eTargetID=", GET_STRING_FROM_TENNIS_PLAYER_ID(eTargetID))
	IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eTargetID]], TMC_WALKING_TO_POS) AND NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eTargetID])) AND GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eTargetID]), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
		IF (IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eTargetID]), "mini@tennis", "ready_2_idle") AND GET_ENTITY_ANIM_CURRENT_TIME(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eTargetID]), "mini@tennis", "ready_2_idle") > DISABLE_TENNIS_MODE_ANIM_PHASE)
		OR NOT IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eTargetID]), "mini@tennis", "ready_2_idle")
			CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT_FOR_SCTV :: IS_TENNIS_CLIENT_FLAG_SET(", thisProps.tennisPlayers[eTargetID].playerAI.texName, ", TMC_WALKING_TO_POS)")
			RETURN FALSE
		ENDIF
	ENDIF
	IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_OPP_OUT_OF_TENNIS_MODE)
		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MODE_ON_OPPONENT_FOR_SCTV :: IS_TENNIS_CLIENT_FLAG_SET(", thisProps.tennisPlayers[1 - ENUM_TO_INT(eTargetID)].playerAI.texName, ", TMC_OPP_OUT_OF_TENNIS_MODE)")
		RETURN FALSE
	ENDIF
	UNUSED_PARAMETER( thisProps )

	RETURN TRUE

ENDFUNC

FUNC TENNIS_SERVE_VALIDATION VALIDATE_TENNIS_MP_SERVE_LAUNCH( TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID )
	UNUSED_PARAMETER(eSelfID)
	
	STRING sAnimDict
	sAnimDict = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherID]), "mini@tennis", "mini@tennis@female")
	
	IF (IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), sAnimDict, "serve")
	AND GET_ENTITY_ANIM_CURRENT_TIME(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), sAnimDict, "serve") >= SERVE_ACTIVE_PHASE)
		CDEBUG1LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MP_SERVE_LAUNCH returning TENNIS_SERVE_SUCCESS, GET_ENTITY_ANIM_CURRENT_TIME >= SERVE_ACTIVE_PHASE")
		CDEBUG2LN(DEBUG_TENNIS, "sAnimDict = ", sAnimDict)
		CDEBUG2LN(DEBUG_TENNIS, "eSelfID   = ", GET_STRING_FROM_TENNIS_PLAYER_ID(eSelfID) )
		CDEBUG2LN(DEBUG_TENNIS, "eOtherID  = ", GET_STRING_FROM_TENNIS_PLAYER_ID(eOtherID) )
		RETURN TENNIS_SERVE_SUCCESS
	ENDIF
	
	IF (NOT IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), sAnimDict, "serve"))
		CDEBUG1LN(DEBUG_TENNIS, "VALIDATE_TENNIS_MP_SERVE_LAUNCH returning TENNIS_SERVE_FAILSAFE, NOT IS_ENTITY_PLAYING_ANIM")
		CDEBUG2LN(DEBUG_TENNIS, "sAnimDict = ", sAnimDict)
		CDEBUG2LN(DEBUG_TENNIS, "eSelfID   = ", GET_STRING_FROM_TENNIS_PLAYER_ID(eSelfID) )
		CDEBUG2LN(DEBUG_TENNIS, "eOtherID  = ", GET_STRING_FROM_TENNIS_PLAYER_ID(eOtherID) )
		RETURN TENNIS_SERVE_FAILSAFE
	ENDIF
	
	RETURN TENNIS_SERVE_INVALID
	
ENDFUNC


/// PURPOSE:
///    init court, ball, player stats and request assets
/// PARAMS:
///    thisProps - 
///    eSelfID - 
PROC INITIALIZE_MP_TENNIS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
	INITIALIZE_TENNIS_COURT(thisProps.tennisCourt, thisProps)
	
	thisProps.texIdleEvent = "Idle1"
	
	VECTOR vTempForward, vTempRight
	IF eSelfID = (TENNIS_PLAYER_AWAY)
		vTempForward = thisProps.vForward
		vTempRight = thisProps.vRight
	ELIF eSelfID = (TENNIS_PLAYER_HOME)
		vTempForward = -thisProps.vForward
		vTempRight = -thisProps.vRight
	ENDIF
	
	thisProps.eTennisActivity = TA_MULTIPLAYER
	
	INITIALIZE_TENNIS_PLAYER(thisProps.tennisPlayers[eSelfID], eSelfID, HUMAN_LOCAL, vTempForward, vTempRight)
	INITIALIZE_TENNIS_PLAYER(thisProps.tennisPlayers[eOtherID], eOtherID, COMPUTER, -vTempForward, -vTempRight)
	SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID], PLAYER_PED_ID())
	INITIALIZE_BALL(thisProps.tennisBall)
	INITIALIZE_TENNIS_LAUNCH_DETAILS(thisProps)
	INITIALIZE_TENNIS_SWING_STATE_OFFSETS(thisProps)
	INITIALIZE_TENNIS_SWING_STATE_PHASES(thisProps)
	
	REQUEST_MODEL(PROP_TENNIS_BALL)
	REQUEST_MODEL(PROP_TENNIS_RACK_01B)
	REQUEST_ANIM_DICT("mini@tennis")
	REQUEST_ANIM_DICT("mini@tennis@female")
	REQUEST_ANIM_DICT("weapons@tennis@male")
	REQUEST_ANIM_DICT("facials@gen_male@base")
	REQUEST_ANIM_DICT("facials@gen_male@variations@happy")
	REQUEST_ANIM_DICT("facials@gen_female@base")
	REQUEST_ANIM_DICT("facials@gen_female@variations@happy")
	REQUEST_STREAMED_TEXTURE_DICT("MPHUD")
	
	DISPLAY_RADAR(FALSE)
ENDPROC

PROC SETUP_TENNIS_MP_WARNING_UI(TENNIS_UI &thisUI)
	SET_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_QUIT_OUTSIDE_COURT)
ENDPROC

PROC RESET_TENNIS_MP_QUIT_UI(TENNIS_UI &thisUI)
	CLEAR_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_QUIT_OUTSIDE_COURT)
ENDPROC

PROC READY_TENNIS_MP_FOR_REMATCH(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_SERVER_BD &serverBD, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, TENNIS_PLAYER_ID ePrevServer)
    CPRINTLN(DEBUG_TENNIS, "***************************** Readying Tennis MP for a rematch *****************************")
	SET_TENNIS_SERVER_FLAG(serverBD, TMS_IS_REMATCH_FLAG)
    thisUIData.iCurrentOption = 1
    RESET_TENNIS_DEUCE_COUNT(thisUIData)
    SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_SET_INIT)
    thisUIData.ePrevUIState = TENNIS_UI_SET_INIT
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_MATCH_POINT_DSTAR)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PLR_SAD)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PLR_HAPPY)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_OPP_SAD)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_OPP_HAPPY)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_GAME_DONE_STISH)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_POINT_LOST)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_POINT_WON)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PAUSE_DIALOGUE)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PAUSE_THE_CUT)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_POINT_W_DSTAR)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PAUSE_THE_CUT)
    SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_UPDATE_SCORES)
    SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_FIRST_SERVE)
	CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_MP_QUIT_BUTTON_SET)
	
	thisUIData.tennisUI.scorePanelSF = REQUEST_SCALEFORM_MOVIE("tennis")
    
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SET_TENNIS_MP_WHO_IS_SERVING(serverBD, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(ePrevServer)))
		SET_TENNIS_MP_SERVING_SIDE(serverBD, SERVING_FROM_RIGHT)
//		SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_PREPARE_SERVE)
		serverBD.iServeCount = 0
		serverBD.iCurrentSet = 0
    
	    INT p=0
	    WHILE p < serverBD.iNumSets
	        serverBD.playerScores[eSelfID].iGamesWon[p] = 0
	        serverBD.playerScores[eOtherID].iGamesWon[p] = 0
	        p++
	    ENDWHILE
	    serverBD.playerScores[eSelfID].iPointsWon = 0
	    serverBD.playerScores[eSelfID].iSetsWon = 0
	    serverBD.playerScores[eOtherID].iPointsWon = 0
	    serverBD.playerScores[eOtherID].iSetsWon = 0
	ENDIF
    
    IF thisProps.tennisPlayers[eSelfID].eCourtSide <> eSelfID
        TENNIS_ACTIVATE_SIDE_CHANGE(thisProps, thisUIData.tennisCameras)
    ENDIF
ENDPROC

// Set up network requests and registry.
// Returns FALSE if net script initialisation fails or the script fails to receive an initial network broadcast.
FUNC BOOL TENNIS_NETWORK_INIT(MP_MISSION_DATA fmmcMissionData, TENNIS_SERVER_BD &serverData, TENNIS_CLIENT_BD &clientBD[NUM_TENNIS_MP_PLAYERS], TENNIS_PARTICIPANT_INFO &info)
	CPRINTLN( DEBUG_TENNIS, "TENNIS_NETWORK_INIT - starting" )
	CLEAR_HELP()
	//If we are in freemode launch diffrently
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	
		//Freemode mission start details
		INT iPlayerMissionToLoad = FMMC_MINI_GAME_CREATOR_ID
		INT iMissionVariation 	 = 0
		INT iNumberOfPlayers 	 = NUM_TENNIS_MP_PLAYERS
		//Call the function that controls the setting up of this script
		FMMC_PROCESS_PRE_GAME_COMMON(fmmcMissionData, iPlayerMissionToLoad, iMissionVariation, iNumberOfPlayers, default, default, FALSE)
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_TENNIS
		
		//Script has started set the global broadcast data up so every body knows
		SET_FM_MISSION_LAUNCHED_SUCESS(fmmcMissionData.mdID.idCreator, fmmcMissionData.mdID.idVariation, fmmcMissionData.iInstanceId)
	
	//If we are not in freemode deal with it normally
	ELSE
		CPRINTLN( DEBUG_TENNIS, "TENNIS_NETWORK_INIT - setting up network script with ", NUM_TENNIS_MP_PLAYERS )
		// Set us to be a net script.
		// This marks the script as a net script, and handles any instancing setup. 
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_TENNIS_MP_PLAYERS, FALSE)
		
		// This marks the script as a net script, and handles any instancing setup. 
	//	IF missionScriptArgs.mission <> eAM_TENNIS
	//		CPRINTLN(DEBUG_TENNIS, "Changing missionScriptArgs.mission to eAM_TENNIS...")
	//		missionScriptArgs.mission = eAM_TENNIS
	//	ENDIF
	//	SETUP_MP_MISSION_FOR_NETWORK( GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_TENNIS) ,  missionScriptArgs )
		
		// Wait until the script is properly running (inline wait).
		IF NOT HANDLE_NET_SCRIPT_INITIALISATION(FALSE, -1, TRUE)
			RETURN FALSE
		ENDIF
		
		// This script will not be paused if another script calls PAUSE_GAME.
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	ENDIF
	
	//This is common to all modes
	// Make code aware of our server and player broadcast data variables.
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverData, SIZE_OF(serverData))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(clientBD, SIZE_OF(clientBD))
	
	// Only the table itself is currently synched.
	RESERVE_NETWORK_MISSION_OBJECTS(TENNIS_OBJECTS)
		
	// Set the server and player states.
	SET_TENNIS_SERVER_MP_STATE(serverData, TMPS_INIT)
	SET_TENNIS_CLIENT_MP_STATE(clientBD[PARTICIPANT_ID_TO_INT()], TMPS_INIT)
	
	INITIALIZE_TENNIS_PARTICIPANT_INFO(info)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Create and attach rackets, ball, place peds on the court
///    Do everything you normally do for the player, don't do anything for the opponent
///    But do this all based on eSelfID
/// PARAMS:
///    thisPlayer - 
///    thisCourt - 
///    oBall - 
///    vCourtRight - 
///    eSelfID - 
PROC SET_UP_MP_TENNIS_PLAYERS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER_ID servingPlayer, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
	//Spawn our own ball, attach it to our hand, and if we're not serving hide it.
	VECTOR vDest = GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, SERVING_FROM_RIGHT, thisProps.vRight, thisProps.vForward, eSelfID = (servingPlayer), eSelfID)
	
	IF NOT TENNIS_IS_PLAYER_SPECTATING()
		SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID], PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(thisProps.tennisPlayers[eSelfID].pIndex)
			SET_ENTITY_COORDS(thisProps.tennisPlayers[eSelfID].pIndex, vDest, TRUE, FALSE)
		ENDIF
	ENDIF
	
	//attach the racket to the ped and make it visible
	thisProps.tennisPlayers[eSelfID].oTennisRacket = CREATE_OBJECT(PROP_TENNIS_RACK_01B, thisProps.tennisCourt.vCourtCorners[0], FALSE, FALSE)
	SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eSelfID].oTennisRacket, TRUE)
	SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(thisProps.tennisPlayers[eSelfID].oTennisRacket, TRUE)
	SET_TENNIS_PLAYER_TO_TENNIS_MODE(thisProps.tennisPlayers[eSelfID].pIndex,TRUE)
	
	// Attach the racket to the opponent ped, this will need to be updated all the time.
	thisProps.tennisPlayers[eOtherID].oTennisRacket = CREATE_OBJECT(PROP_TENNIS_RACK_01B, thisProps.tennisCourt.vCourtCorners[1], FALSE, FALSE)
	SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eOtherID].oTennisRacket, TRUE)
	SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(thisProps.tennisPlayers[eSelfID].oTennisRacket, TRUE)
	
	// Attach the rackets
	ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eSelfID, eOtherID)
	
	IF NOT DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
		//attach the ball and make it invisible
		CDEBUG2LN(DEBUG_TENNIS, "SET_UP_MP_TENNIS_PLAYERS creating ball" )
		thisProps.tennisBall.oBall = CREATE_OBJECT(PROP_TENNIS_BALL,thisProps.tennisCourt.vCenterCourt, FALSE, FALSE)
	ENDIF
	SET_ENTITY_VISIBLE(thisProps.tennisBall.oBall, FALSE)
	SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(thisProps.tennisBall.oBall, FALSE)
	SET_ENTITY_RECORDS_COLLISIONS(thisProps.tennisBall.oBall, TRUE)
	
	IF eSelfID = servingPlayer
		SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, SERVING_FROM_RIGHT, eSelfID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks vPlayerPos against the quit spheres and determines whether to quit the area or not
/// PARAMS:
///    vPlayerPos - 
///    vCenterCourt - 
/// RETURNS:
///    
FUNC BOOL SHOULD_TENNIS_MP_INCREASE_QUIT_TIMER(VECTOR vPlayerPos, VECTOR vCenterCourt)
	VECTOR v1 = (<< -1175.3660, -1582.9221, 3.37 >>)
	VECTOR v2 = (<< -1128.8975, -1649.4512, 3.37 >>)
	VECTOR v3 = (<< -1156.1381, -1661.8712, 3.37 >>)
	VECTOR v4 = (<< -1192.5183, -1609.0835, 3.37 >>)
	VECTOR v5 = (<< -1147.1272, -1635.4512, 3.37 >>)
	VECTOR v6 = (<< -1160.5195, -1644.7035, 3.37 >>)
	
	RETURN IS_POINT_INSIDE_TENNIS_AREA(vPlayerPos, v1, v2, COURT_OOB_WIDTH_1)
		OR IS_POINT_INSIDE_TENNIS_AREA(vPlayerPos, v3, v4, COURT_OOB_WIDTH_2)
		OR IS_POINT_INSIDE_TENNIS_AREA(vPlayerPos, v5, v6, COURT_OOB_WIDTH_3)
		OR VDIST2(vPlayerPos, vCenterCourt) >= (COURT_RANGE * COURT_RANGE)
ENDFUNC

/// PURPOSE:
///    Sets the server's game state and flips the appropriate bitmasks
/// PARAMS:
///    serverBD - 
///    clientBD - 
PROC HANDLE_TENNIS_MP_LOCKSTEP_STATES(TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD)
	SWITCH GET_TENNIS_CLIENT_MP_STATE(clientBD)
		CASE TMPS_WAIT_FOR_PREPARE_SERVE
			IF NOT IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_PREPARE_SERVE)
				SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_PREPARE_SERVE)
				SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_PREPARE_SERVE)
				CPRINTLN(DEBUG_TENNIS, "Moving Server to TGSE_PREPARE_SERVE")
			ENDIF
		BREAK
		
		CASE TMPS_WAIT_FOR_RALLY
			SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_RALLY)
			SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_RALLY)
		
			RESET_NET_TIMER(serverBD.timeServeTimeOut)
			START_NET_TIMER(serverBD.timeServeTimeOut)
			
			CPRINTLN(DEBUG_TENNIS, "Moving Server to TGSE_RALLY")
		BREAK
		
		CASE TMPS_WAIT_FOR_POINT_WON
			SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_POINT_WON)
			SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_POINT_WON)
			CPRINTLN(DEBUG_TENNIS, "Moving Server to TGSE_POINT_WON")
		BREAK
		
		CASE TMPS_WAIT_FOR_POINT_CHECKS
			SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_POINT_CHECKS)
			SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_POINT_CHECKS)
			CPRINTLN(DEBUG_TENNIS, "Moving Server to TMPS_WAIT_FOR_POINT_CHECKS")
		BREAK
		
		CASE TMPS_WAIT_FOR_INTERSTITIAL
			IF NOT IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_INTERSTITIAL)
				SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_INTERSTITIAL)
				SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_INTERSTITIAL)
				CPRINTLN(DEBUG_TENNIS, "Moving Server to TGSE_INTERSTITIAL")
			ENDIF
		BREAK
		
		CASE TMPS_WAIT_FOR_SIDE_CHANGE
			SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_SIDE_CHANGE)
			SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_SIDE_CHANGE)
			CPRINTLN(DEBUG_TENNIS, "Moving Server to TGSE_SIDE_CHANGE")
		BREAK
		
		CASE TMPS_WAIT_FOR_CHANGE_SERVERS
			SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_CHANGE_SERVERS)
			SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_CHANGE_SERVERS)
			CPRINTLN(DEBUG_TENNIS, "Moving Server to TGSE_CHANGE_SERVERS")
		BREAK
		
		CASE TMPS_WAIT_FOR_OUTRO
			SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_OUTRO)
			SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_OUTRO)
			CPRINTLN(DEBUG_TENNIS, "Moving Server to TGSE_OUTRO")
		BREAK
		
		CASE TMPS_WAIT_FOR_FINISH
			SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_FINISH)
			SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_FINISH)
			CPRINTLN(DEBUG_TENNIS, "Moving Server to TGSE_FINISH")
		BREAK
		
		CASE TMPS_WAIT_FOR_PREPARE_REMATCH
			SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_PREPARE_REMATCH)
			SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_PREPARE_REMATCH)
			CPRINTLN(DEBUG_TENNIS, "Moving Server to TGSE_PREPARE_REMATCH")
		BREAK
		
		CASE TMPS_QUIT_THE_GAME
			CPRINTLN(DEBUG_TENNIS, "Server should do nothing for CASE TMPS_QUIT_THE_GAME")
		BREAK
		
		CASE TMPS_NO_REMATCH
			CPRINTLN(DEBUG_TENNIS, "TMPS_NO_REMATCH, clients will be quitting soon")
		BREAK
		
		DEFAULT
			CPRINTLN(DEBUG_TENNIS, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
			CPRINTLN(DEBUG_TENNIS, "Hitting Default Case for HANDLE_TENNIS_MP_LOCKSTEP_STATES, This is not good.")
			CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_CLIENT_MP_STATE(clientBD) is: ", GET_TENNIS_CLIENT_MP_STATE(clientBD))
			CPRINTLN(DEBUG_TENNIS, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Handles the lockstepping system for moving clients from one MPstate to another and the server from one gamestate to another
/// PARAMS:
///    serverBD - 
///    clientBD - 
///    eSelfID - 

/// PURPOSE:
///    
/// PARAMS:
///    serverBD - 
///    clientBD - 
///    eSelfID - 
/// RETURNS:
///    FALSE when there is a desync
FUNC BOOL UPDATE_TENNIS_MP_GAME(TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, TENNIS_PARTICIPANT_INFO &info)
	BOOL bSelfValid = info.iPlayers[eSelfID] <> TENNIS_INVALID_PARTICIPANT_ID
	BOOL bOtherValid = info.iPlayers[eOtherID] <> TENNIS_INVALID_PARTICIPANT_ID
	
	IF bSelfValid AND bOtherValid 
	AND clientBD[info.iPlayers[eSelfID]].eMPState = TMPS_WAIT_POST_MENU
	AND clientBD[info.iPlayers[eOtherID]].eMPState = TMPS_WAIT_POST_MENU
		SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_GAME)
		SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_PREPARE_SERVE)
	ENDIF
	
	IF bSelfValid AND bOtherValid
	AND (GET_TENNIS_CLIENT_MP_STATE(clientBD[info.iPlayers[eSelfID]]) > TMPS_WAIT_POST_GAME)
	AND (GET_TENNIS_CLIENT_MP_STATE(clientBD[info.iPlayers[eOtherID]]) > TMPS_WAIT_POST_GAME)
		//Both Clients are waiting in some lockstep state
		IF GET_TENNIS_CLIENT_MP_STATE(clientBD[info.iPlayers[eSelfID]]) = GET_TENNIS_CLIENT_MP_STATE(clientBD[info.iPlayers[eOtherID]])
			CPRINTLN(DEBUG_TENNIS, "Clients are in the same lockstep state")
			
			//Clients are in sync, reset the timer and turn off the desync prep bitmask
			RESET_TENNIS_DESYNC_TIMER()
			IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_PREPARE_FOR_DESYNC)
				CLEAR_TENNIS_SERVER_FLAG(serverBD, TMS_PREPARE_FOR_DESYNC)
			ENDIF
			
			HANDLE_TENNIS_MP_LOCKSTEP_STATES(serverBD, clientBD[info.iPlayers[eSelfID]])
		ELIF (GET_TENNIS_CLIENT_MP_STATE(clientBD[info.iPlayers[eSelfID]]) = TMPS_WAIT_FOR_FINISH)
		OR   (GET_TENNIS_CLIENT_MP_STATE(clientBD[info.iPlayers[eOtherID]]) = TMPS_WAIT_FOR_FINISH)
			TENNIS_MP_BROADCAST_QUIT(1)
			TENNIS_MP_BROADCAST_QUIT(1)
			CPRINTLN(DEBUG_TENNIS, "UPDATE_TENNIS_MP_GAME: Sending quit events, one player does not want to rematch")
//		ELSE
//			IF NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_ANIM_PLAYING)
//			OR NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eOtherID]], TMC_ANIM_PLAYING)
//				INCREASE_TENNIS_DESYNC_TIMER()
//				SET_TENNIS_SERVER_FLAG(serverBD, TMS_PREPARE_FOR_DESYNC)
//				IF SHOULD_TENNIS_DESYNC_BE_HANDLED()
//					CPRINTLN(DEBUG_TENNIS, "Desync has occured for too long.")
//					CPRINTLN(DEBUG_TENNIS, "Client[eSelfID] MP State: ", GET_TENNIS_CLIENT_MP_STATE(clientBD[info.iPlayers[eSelfID]]))
//					CPRINTLN(DEBUG_TENNIS, "Client[eOtherID] MP State: ", GET_TENNIS_CLIENT_MP_STATE(clientBD[info.iPlayers[eOtherID]]))
//					//Open the floodgates to allow the game to continue. 
//					//We'll probably handle each individual desync as it appears, I can't think of a clean way to handle it well all the time.
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_MOVE_TO_GAME)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_MOVE_TO_RALLY)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_MOVE_TO_PREPARE_SERVE)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_MOVE_TO_OUTRO)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_MOVE_TO_POINT_WON)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_MOVE_TO_INTERSTITIAL)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_MOVE_TO_CHANGE_SERVERS)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_PREPARE_FOR_DESYNC)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_RECOVERING_FROM_DESYNC)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_MOVE_TO_FINISH)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_MOVE_TO_SIDE_CHANGE)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_MOVE_TO_PREPARE_REMATCH)
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_MOVE_TO_POINT_CHECKS)
//					
//					//Set the bitmask that lets us know we recovered from a desync
//					SET_TENNIS_SERVER_FLAG(serverBD, TMS_RECOVERING_FROM_DESYNC)
//					RETURN FALSE
//				ENDIF
//			ENDIF			
		ENDIF
	ENDIF
	
	//Currently the game moves to the state of the the client who hit last, even when the clients disagree.
	IF bSelfValid AND bOtherValid
	AND clientBD[info.iPlayers[eSelfID]].eMPState = TMPS_WAIT_LOCKSTEP 
	AND clientBD[info.iPlayers[eOtherID]].eMPState = TMPS_WAIT_LOCKSTEP
		CPRINTLN(DEBUG_TENNIS, "Both clients are in the lockstep state")
	
		SET_TENNIS_SERVER_FLAG_EXCLUSIVE(serverBD, TMS_MOVE_TO_PREPARE_SERVE)
		IF clientBD[info.iPlayers[eSelfID]].eGameState = clientBD[info.iPlayers[eOtherID]].eGameState
			CPRINTLN(DEBUG_TENNIS, "Server Message:")
			CPRINTLN(DEBUG_TENNIS, "host and client agree, moving state on")
			CPRINTLN(DEBUG_TENNIS, "Host state: ", clientBD[info.iPlayers[eSelfID]].eGameState)
			CPRINTLN(DEBUG_TENNIS, "End Server Message")
			SET_TENNIS_SERVER_GAME_STATE(serverBD, clientBD[info.iPlayers[eSelfID]].eGameState)
		ELSE
			CPRINTLN(DEBUG_TENNIS, "Server Message:")
			CPRINTLN(DEBUG_TENNIS, "host and client disagree, moving to 1 minus eHitLast(", GET_TENNIS_MP_HIT_LAST(serverBD), ") state")
			CPRINTLN(DEBUG_TENNIS, "Host state: ", clientBD[info.iPlayers[eSelfID]].eGameState)
			CPRINTLN(DEBUG_TENNIS, "Client State: ", clientBD[info.iPlayers[eOtherID]].eGameState)
			CPRINTLN(DEBUG_TENNIS, "End Server Message")
			SET_TENNIS_SERVER_GAME_STATE(serverBD, clientBD[info.iPlayers[1 - ENUM_TO_INT(GET_TENNIS_MP_HIT_LAST(serverBD))]].eGameState)
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisUIData - 
///    eSelfID - 
///    clientBD - []
///    playerScores - []
PROC PROCESS_TENNIS_MP_QUIT_UI(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eSelfID, TENNIS_SERVER_BD serverBD, TENNIS_CLIENT_BD &clientBD[], TENNIS_SCORES &playerScores[], INT iCurrentSet, BOOL bRematch, INT iNumSets, TENNIS_PARTICIPANT_INFO &info)
	INT iParticipantID = PARTICIPANT_ID_TO_INT()
	
	SWITCH GET_TENNIS_UI_STATE(thisUIData)
		CASE TENNIS_UI_PLAYING
			// Just let it go and play.
			IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_RETURNING_FROM_QUIT)
				CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_RETURNING_FROM_QUIT)
				RESET_TENNIS_MP_QUIT_UI(thisUIData.tennisUI)
				IF GET_TENNIS_TIMER_A(thisProps) < SPLASH_UI_DURATION
					DISPLAY_TENNIS_SPLASH_UI(thisUIData.tennisUI, TRUE)
				ENDIF
				
				BOOL bLobAvailable
				bLobAvailable = IS_TENNIS_PLAYER_IN_SERVICE_BOXES(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)], thisProps.tennisCourt, thisProps.vRight)
				IF UPDATE_TENNIS_PLAYING_CONTROLS(thisUIData.tennisUI, bLobAvailable)
					SETUP_TENNIS_PLAYING_CONTROLS(thisUIData.tennisUI, bLobAvailable, VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(playerScores, iCurrentSet, bRematch), iNumSets)
				ENDIF
			ENDIF
		BREAK
		
		CASE TENNIS_UI_QUIT
			DISPLAY_TENNIS_QUIT_UI(thisUIData.tennisUI)
			
			IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				CPRINTLN(DEBUG_TENNIS, "Quitting :: TENNIS_UI_QUIT IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)")
				CALL_END_OF_MISSION_QUIT_TELEMETRY()
				SET_TENNIS_GLOBAL_FLAG(TGF_QUIT_FROM_TENNIS_MP)
				TENNIS_MP_BROADCAST_QUIT()
				IF IS_TIMER_STARTED(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
					TENNIS_PAUSE_TIMER(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
				ENDIF
				SWING_METER_DISPLAY(thisUIData.tennisUI, FALSE, 0)
				DISPLAY_TENNIS_SPLASH_UI(thisUIData.tennisUI, FALSE)
				SET_TENNIS_CLIENT_MP_STATE(clientBD[iParticipantID], TMPS_QUIT_THE_GAME)
				SET_TENNIS_CLIENT_FLAG(clientBD[iParticipantID], TMC_SHOW_QUIT_LEADERBOARD)
				ALLOW_TENNIS_PLAYER_CONTROL(TA_MULTIPLAYER, FALSE)
				SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_QUIT_BIT)
				IF WAS_TENNIS_PLAYER_WINNING(serverBD, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eSelfID)), eSelfID)
					SET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[iParticipantID], AR_buddyA_won)
					IF VALIDATE_TENNIS_PLAYERS_INFO( info )
						SET_TENNIS_UI_BETS_DATA(thisUIData.tennisUI, BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iPlayers[1 - ENUM_TO_INT(eSelfID)]))))
					ENDIF
					CREATE_TENNIS_BETTING_SCOREBOARD(thisUIData.tennisUI, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt), FALSE)
				ELSE
					SET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[iParticipantID], AR_playerQuit)
				ENDIF
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_MINI_GAME_SOUNDSET")
				
				CLEAR_HELP()
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_RETURNING_FROM_QUIT)
				SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_PLAYING)
				ALLOW_TENNIS_PLAYER_CONTROL(TA_MULTIPLAYER, TRUE)
				PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_MINI_GAME_SOUNDSET")
			ENDIF
		BREAK
		
		CASE TENNIS_UI_FAR_AWAY
			DISPLAY_TENNIS_QUIT_UI(thisUIData.tennisUI)
			DISPLAY_TENNIS_SPLASH_UI(thisUIData.tennisUI, FALSE)
			CLEAR_TENNIS_HELP_MESSAGES()
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisProps - 
///    thisPlayer - The player we're processing the Serve Meter for
///    bInit - 
///    bShowResults - Show Results when the game is finished
PROC PROCESS_TENNIS_MP_METER_AND_BITMASK_UI(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_SERVER_BD &serverBD, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER &thisPlayer, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	IF IS_TIMER_STARTED(thisPlayer.meterDelayTimer) 
	AND (NOT IS_TIMER_PAUSED(thisPlayer.meterDelayTimer)) 
	AND GET_TIMER_IN_SECONDS(thisPlayer.meterDelayTimer) > UI_ON
	
		PROCESS_SERVE_METER(thisUIData, thisPlayer)
		
	#IF IS_DEBUG_BUILD
	ELSE
		
		TEXT_LABEL_63 texDisplay = "PROCESS_TENNIS_MP_METER_AND_BITMASK_UI"
		VECTOR vDisplay = << 0.75, 0.75, 0.0 >>
		VECTOR vAdd = << 0.0, 0.0125, 0.0 >>
		DRAW_DEBUG_TEXT_2D(texDisplay, vDisplay)
		vDisplay += vAdd
		
		texDisplay = "IS_TIMER_STARTED = "
		texDisplay += PICK_STRING(IS_TIMER_STARTED(thisPlayer.meterDelayTimer), "TRUE", "FALSE")
		DRAW_DEBUG_TEXT_2D(texDisplay, vDisplay)
		vDisplay += vAdd
		
		texDisplay = "NOT IS_TIMER_PAUSED = "
		texDisplay += PICK_STRING(NOT IS_TIMER_PAUSED(thisPlayer.meterDelayTimer), "TRUE", "FALSE")
		DRAW_DEBUG_TEXT_2D(texDisplay, vDisplay)
		vDisplay += vAdd
		
		IF IS_TIMER_STARTED(thisPlayer.meterDelayTimer)
			texDisplay = "GET_TIMER_IN_SECONDS = "
			texDisplay += GET_STRING_FROM_FLOAT(GET_TIMER_IN_SECONDS(thisPlayer.meterDelayTimer))
			DRAW_DEBUG_TEXT_2D(texDisplay, vDisplay)
			vDisplay += vAdd
		ENDIF
		
	#ENDIF
	ENDIF
	
	IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING) AND GET_TENNIS_SERVER_GAME_STATE(serverBD) > TGSE_OPTION_FINISHED
		IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SWING_METER_POS_SET)
			SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SWING_METER_POS_SET)
			FLOAT fXOffset, fYOffset, fOffsetValue
			GET_SCREEN_COORD_FROM_WORLD_COORD(thisProps.tennisPlayers[eSelfID].vPos, fXOffset, fYOffset)
			IF fXOffset = -1 OR fYOffset = -1
				fXOffset = PICK_FLOAT(GET_TENNIS_MP_SERVING_SIDE(serverBD) = SERVING_FROM_RIGHT, SERVE_METER_RIGHT_MAX, SERVE_METER_LEFT_MAX)
				fYOffset = SERVE_Y_OFFSET
			ELIF thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_CountryClub
				fOffsetValue = PICK_FLOAT(GET_IS_WIDESCREEN(), MICHAEL_SERVE_METER_X_OFFSET_STAT, MICHAEL_SERVE_METER_X_OFF_4BY3)
				fXOffset += PICK_FLOAT(GET_TENNIS_MP_SERVING_SIDE(serverBD) = SERVING_FROM_RIGHT, fOffsetValue, -fOffsetValue - MICHAEL_SERVE_METER_X_RIGHT_ADJ)
			ELSE
				fOffsetValue = PICK_FLOAT(GET_IS_WIDESCREEN(), MICHAEL_SERVE_METER_X_OFFSET_STAT, MICHAEL_SERVE_METER_X_OFF_4BY3)
				fXOffset += PICK_FLOAT(GET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras) = TENNIS_CAMERA_DYNAMIC,
							PICK_FLOAT(GET_TENNIS_MP_SERVING_SIDE(serverBD) = SERVING_FROM_RIGHT, MICHAEL_SERVE_METER_X_OFFSET_DYNA, -MICHAEL_SERVE_METER_X_OFFSET_DYNA),	//Dynamic cam variables
							PICK_FLOAT(GET_TENNIS_MP_SERVING_SIDE(serverBD) = SERVING_FROM_RIGHT, fOffsetValue, -fOffsetValue - MICHAEL_SERVE_METER_X_RIGHT_ADJ))	//Static cam variables
			ENDIF
			fYOffset = SERVE_Y_OFFSET	// fYOffset is replaced, we're keeping the Y static, B*911072
			thisUIData.fSwingMeterX = fXOffset
			thisUIData.fSwingMeterY = fYOffset
		ENDIF
		
		IF IS_TIMER_STARTED(thisPlayer.meterDelayTimer) 
		AND (NOT IS_TIMER_PAUSED(thisPlayer.meterDelayTimer))
		AND GET_TIMER_IN_SECONDS(thisPlayer.meterDelayTimer) > UI_ON
			SWING_METER_SET_POSITION(thisUIData.tennisUI.scorePanelSF, thisUIData.fSwingMeterX, thisUIData.fSwingMeterY)
			SWING_METER_DISPLAY(thisUIData.tennisUI, TRUE)
		ELIF serverBD.eGameState <> TGSE_TEST_STATE
			SWING_METER_DISPLAY(thisUIData.tennisUI, FALSE)
			IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SWING_METER_POS_SET)
				CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SWING_METER_POS_SET)
			ENDIF
		ENDIF
	ENDIF
	
	//Draw the movies after getting data for them.
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		DRAW_TENNIS_SCALEFORM_UI(thisUIData.tennisUI.scorePanelSF)
	ENDIF
	
	IF NOT IS_TENNIS_SPLASH_UI_HIDDEN(thisUIData.tennisUI)
//		IF NOT UPDATE_TENNIS_MESSAGE_UI(thisUIData.tennisUI.splashUI)
		IF NOT UPDATE_SHARD_BIG_MESSAGE(thisUIData.tennisUI.uiShardMS)
			DISPLAY_TENNIS_SPLASH_UI(thisUIData.tennisUI, FALSE)
		ENDIF
	ENDIF
	
	HANDLE_BITMASK_UI(thisUIData, 
					  thisProps, 
					  serverBD.playerScores, 
					  GET_TENNIS_MP_RULESET(serverBD),
					  GET_TENNIS_MP_WHO_IS_SERVING(serverBD), 
					  GET_TENNIS_MP_SCORED_LAST(serverBD), 
					  GET_TENNIS_MP_SERVE_COUNT(serverBD), 
					  GET_TENNIS_MP_NUM_SETS(serverBD), 
					  GET_TENNIS_MP_CURRENT_SET(serverBD), 
					  eSelfID, 
					  eOtherID)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisProps - 
///    thisPlayer - The player we're processing the Serve Meter for
///    bInit - 
///    bShowResults - Show Results when the game is finished
PROC PROCESS_TENNIS_MP_METER_AND_BITMASK_UI_FOR_SCTV(TENNIS_GAME_UI_DATA &thisUIData)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	//Draw the movies after getting data for them.
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		CDEBUG3LN( DEBUG_TENNIS, "PROCESS_TENNIS_MP_METER_AND_BITMASK_UI_FOR_SCTV - DRAW_TENNIS_SCALEFORM_UI" )
		DRAW_TENNIS_SCALEFORM_UI(thisUIData.tennisUI.scorePanelSF)
	ENDIF

	IF NOT IS_TENNIS_SPLASH_UI_HIDDEN(thisUIData.tennisUI)
//		IF NOT UPDATE_TENNIS_MESSAGE_UI(thisUIData.tennisUI.splashUI)
		IF NOT UPDATE_SHARD_BIG_MESSAGE(thisUIData.tennisUI.uiShardMS)
			CDEBUG3LN( DEBUG_TENNIS, "PROCESS_TENNIS_MP_METER_AND_BITMASK_UI_FOR_SCTV - DISPLAY_TENNIS_SPLASH_UI" )
			DISPLAY_TENNIS_SPLASH_UI(thisUIData.tennisUI, FALSE)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Asks about a rematch and runs the timer.
/// PARAMS:
///    thisUIData - 
/// RETURNS:
///    0 for no rematch, 1 for a rematch, -1 otherwise
FUNC INT PROCESS_TENNIS_MP_RESULTS_SCOREBOARD(TENNIS_GAME_UI_DATA &thisUIData)
	DISPLAY_TENNIS_BETTING_SCOREBOARD(thisUIData.tennisUI)
	INT iChoice = HANDLE_TENNIS_REMATCH_UI(thisUIData)
	
	thisUIData.fMessageTimer += GET_FRAME_TIME()
	
	IF iChoice <> -1
		RETURN iChoice
	ELIF thisUIData.fMessageTimer > FINAL_MENU_TIMEOUT
		RETURN 0
	ENDIF
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Handles updates that normally occur in PROCESS_MP_CLIENT_STATE without all the messy game update stuff:
///    UPDATE_TENNIS_PLAYER_MOVER
///    UPDATE_TENNIS_PLAYER_STROKE
///    COMMON_EVERY_FRAME_DATA
///    PROCESS_TENNIS_MP_UI_DATA
///    HANDLE_TENNIS_ANIMS
/// PARAMS:
///    thisProps - 
///    thisUIData - 
///    serverBD - 
///    clientBD - []
///    eSelfID - 
///    bServing - 
PROC TENNIS_MP_WAIT_STATE_UPDATES(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_SERVER_BD &serverBD, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, TENNIS_PARTICIPANT_INFO &info, BOOL bServing = FALSE, BOOL bStroke = TRUE, BOOL bIgnoreResetFlags = FALSE)
	INT iBounces = GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
	
	IF GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_RALLY AND NOT IS_TRANSITION_ACTIVE()	//OR GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_PREPARE_SERVE
		UPDATE_TENNIS_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext, FALSE)
		HANDLE_TENNIS_INSTRUCTION_INPUT(thisUIData)	//, serverBD)
	ENDIF
	
	// Drop the player out of tennis mode if they need it.
	IF IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eSelfID], TPB_DROP_OUT_OF_TENNIS_MODE)
		IF IS_ENTITY_PLAYING_ANIM(thisProps.tennisPlayers[eSelfID].pIndex, "mini@tennis", "ready_2_idle")
			IF GET_ENTITY_ANIM_CURRENT_TIME(thisProps.tennisPlayers[eSelfID].pIndex, "mini@tennis", "ready_2_idle") >= DISABLE_TENNIS_MODE_ANIM_PHASE
				CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_DROP_OUT_OF_TENNIS_MODE)
				SET_TENNIS_PLAYER_TO_TENNIS_MODE(thisProps.tennisPlayers[eSelfID].pIndex, FALSE)
				SET_PED_WEAPON_MOVEMENT_CLIPSET(thisProps.tennisPlayers[eSelfID].pIndex, "weapons@tennis@male")
				CDEBUG2LN(DEBUG_TENNIS, "TENNIS_MP_WAIT_STATE_UPDATES :: Dropped out of tennis mode")
			ENDIF
		ENDIF
	ENDIF
	
	STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
	
	IF VALIDATE_USE_BALL_SIMULATOR(thisProps, iBounces, thisUIData.iUtilFlags, GET_TENNIS_SERVER_GAME_STATE(serverBD))
		BOOL bPlaySound = FALSE	//(GET_TENNIS_SERVER_GAME_STATE(serverBD) <> TGSE_PREPARE_SERVE AND GET_TENNIS_SERVER_GAME_STATE(serverBD) < TGSE_INTERSTITIAL)
		#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_ENTITY_VECTOR(thisProps.tennisPlayers[eSelfID].pIndex, "vBallPosIntoSimWSU", GET_TENNIS_BALL_POS(thisProps.tennisBall), TRUE)
		#ENDIF
		IF NOT DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
			DEBUG_SPAWN_BALL(thisProps)
			CDEBUG2LN(DEBUG_TENNIS, "TENNIS_MP_WAIT_STATE_UPDATES :: respawned thisProps.tennisBall")
		ENDIF
		UPDATE_BALL_SIMULATOR_TIME_SLICED(thisProps.tennisCourt, thisProps.tennisBall, thisProps.tennisPlayers,  thisProps.vForward, iBounces, GET_FRAME_TIME(), bPlaySound)
	ELSE
		
		UPDATE_TENNIS_BALL_TIME_SLICE_VARIABLES(thisProps.tennisBall, FALSE)
	ENDIF
	
	IF GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_RALLY
		UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, bServing, IS_PAUSE_MENU_ACTIVE())
	ENDIF
	
	IF GET_TENNIS_SERVER_GAME_STATE(serverBD) < TGSE_OUTRO
		PROCESS_CAMERA_STATE(thisProps, thisUIData, eSelfID, thisProps.tennisPlayers[eSelfID].eCourtSide, GET_TENNIS_MP_HIT_LAST(serverBD))
	ENDIF
	
	IF GET_TENNIS_UI_STATE(thisUIData) = TENNIS_UI_QUIT OR GET_TENNIS_UI_STATE(thisUIData) = TENNIS_UI_FAR_AWAY
		DISPLAY_TENNIS_QUIT_UI(thisUIData.tennisUI)
		
		CLEAR_TENNIS_HELP_MESSAGES()
	ENDIF
	
	IF bStroke
//		UPDATE_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_HIT_LAST(serverBD), eSelfID, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), GET_TENNIS_MP_SERVING_SIDE(serverBD), clientBD)
	ENDIF
	
	COMMON_EVERY_FRAME_DATA(thisProps, GET_TENNIS_SERVER_GAME_STATE(serverBD), thisUIData.iUtilFlags, eSelfID, eOtherID, info, bIgnoreResetFlags)
	
	PROCESS_TENNIS_MP_METER_AND_BITMASK_UI(thisProps,
				serverBD,
				thisUIData,
				thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)],
				eSelfID,
				eOtherID)
	
	IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING) 
	AND GET_TENNIS_SERVER_GAME_STATE(serverBD) > TGSE_OPTION_MENU
	AND GET_TENNIS_SERVER_GAME_STATE(serverBD) < TGSE_QUIT_CONFIRM
	AND GET_TENNIS_SERVER_GAME_STATE(serverBD) <> TGSE_INTERSTITIAL
		HANDLE_TENNIS_ANIMS(thisProps.tennisPlayers[eSelfID], thisProps.phasesArray, TA_MULTIPLAYER)
	ENDIF
	
	IF NOT IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS)
		DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, TRUE)
	ENDIF
ENDPROC

PROC TENNIS_MP_HANDLE_OFFSIDES_PLAYERS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, TENNIS_PARTICIPANT_INFO &info)
	INT iMyCornerCheck	//, iTheirCornerCheck
	
	//if I'm positioned at AWAY then I should stay off corner[3] and they should stay off corner[0]
	IF thisProps.tennisPlayers[eSelfID].eCourtSide = TENNIS_PLAYER_AWAY
		iMyCornerCheck = 3
//		iTheirCornerCheck = 0
	ELSE
		iMyCornerCheck = 0
//		iTheirCornerCheck = 3
	ENDIF
	
	IF IS_POINT_INSIDE_TENNIS_AREA(thisProps.tennisPlayers[eSelfID].vPos, thisProps.tennisCourt.vCenterCourt, thisProps.tennisCourt.vCourtCorners[iMyCornerCheck] + thisProps.vRight * 0.5, OFFSIDES_WIDTH)
		SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, eOtherID)
		SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
		TENNIS_MP_BROADCAST_GET_OFF_MY_SIDE(ENUM_TO_INT(eSelfID), thisUIData.tennisUI.iUIFlags, thisUIData.iUtilFlags)
		IF IS_TIMER_STARTED(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
			TENNIS_PAUSE_TIMER(thisProps.tennisPlayers[eSelfID].meterDelayTimer)
		ENDIF
		IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
			DETACH_ENTITY(thisProps.tennisBall.oBall, FALSE)
		ENDIF
		RESET_TENNIS_PLAYER_SWING_TIMER(thisProps.tennisPlayers[eSelfID])
		SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_PAUSE_UPDATE)
		CPRINTLN(DEBUG_TENNIS, "TENNIS_MP_HANDLE_OFFSIDES_PLAYERS eSelfID made the foul")
	ENDIF
ENDPROC

PROC CHECK_AND_HANDLE_TENNIS_BALL_RECEIVED_FLAG(TENNIS_CLIENT_BD & clientBD, TENNIS_GAME_PROPERTIES & thisProps, TENNIS_PLAYER_ID eSelfID)
	IF IS_TENNIS_CLIENT_FLAG_SET(clientBD, TMC_INITIAL_BALL_UPDATE_RECEIVED) AND HAS_TENNIS_BALL_PASSED_BROADCAST_POS(clientBD, thisProps.tennisBall)
		CLEAR_TENNIS_CLIENT_FLAG(clientBD, TMC_INITIAL_BALL_UPDATE_RECEIVED)
		SET_TENNIS_BALL_BOUNCE_FRAME_STAMP(thisProps.tennisBall, GET_TENNIS_FRAME_COUNTER(thisProps))
		SET_TENNIS_BALL_INTO_PLAYER_FRAME_STAMP(thisProps.tennisBall, GET_FRAME_COUNT() + COLLIDE_WITH_PLAYER_THRESH)
		DETACH_ENTITY(thisProps.tennisBall.oBall, FALSE)
		RESET_TENNIS_BALL_TIMERS(thisProps.tennisBall)
		CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_HIT_PLAYER)
		SET_TENNIS_BALL_POS(thisProps.tennisBall, GET_TENNIS_CLIENT_PRED_BALL_POS(clientBD))
		thisProps.tennisBall.vBallVel = GET_TENNIS_CLIENT_PRED_BALL_VEL(clientBD)
		IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
			VECTOR vBallPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
			#IF TENNIS_DEBUG_RECORDING
			DEBUG_RECORD_ENTITY_VECTOR(thisProps.tennisPlayers[eSelfID].pIndex, "vBallPosBroadcastIn", vBallPos, TRUE)
			DEBUG_RECORD_ENTITY_VECTOR(thisProps.tennisPlayers[eSelfID].pIndex, "vBallVelBroadcastIn", thisProps.tennisBall.vBallVel, FALSE)
			DEBUG_RECORD_ENTITY_FLOAT(thisProps.tennisPlayers[eSelfID].pIndex, "Frame Time", GET_FRAME_TIME())
			#ENDIF
			SET_ENTITY_COORDS(thisProps.tennisBall.oBall, vBallPos)
			SET_ENTITY_VELOCITY(thisProps.tennisBall.oBall, thisProps.tennisBall.vBallVel)
			UPDATE_TENNIS_BALL_TIME_SLICE_VARIABLES(thisProps.tennisBall, TRUE)

			CPRINTLN(DEBUG_TENNIS, "Game Timer: ", GET_GAME_TIMER())
			CPRINTLN(DEBUG_TENNIS, "Ball Position: ", vBallPos)
			CPRINTLN(DEBUG_TENNIS, "Ball Velocity: ", thisProps.tennisBall.vBallVel)
		ENDIF
				
		INCREASE_TENNIS_CLIENT_RALLY_COUNTER(clientBD)
		IF SHOULD_TENNIS_CLIENT_GET_RALLY_XP_BONUS(clientBD)
			RESET_TENNIS_CLIENT_RALLY_COUNTER(clientBD)
			AWARD_TENNIS_CLIENT_RALLY_XP_BONUS()
		ENDIF
		
		RESET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
		SET_TENNIS_CLIENT_FLAG(clientBD, TMC_BALL_HIT)
		CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_REDUCE_PREDICTION_CALC)
		SET_TENNIS_BALL_SPIN(thisProps.tennisBall, GET_TENNIS_CLIENT_PRED_BALL_SPIN(clientBD))
		
		IF GET_TENNIS_CLIENT_PRED_BALL_SPIN(clientBD) = TBS_NO_SPIN
			PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_NPC_FOREARM_MASTER", GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)]))
		ELIF GET_TENNIS_CLIENT_PRED_BALL_SPIN(clientBD) = TBS_SLICE
			PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_NPC_BACKSLICE_MASTER",  GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)]))
		ELIF GET_TENNIS_CLIENT_PRED_BALL_SPIN(clientBD) = TBS_TOPSPIN
			PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_NPC_TOPSPIN_MASTER",  GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)]))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Called when the TMC_BALL_VELOCITY_UPDATE_RECEIVED is set. Sets up the lerp for the ball to get it in line with the velocity update.
/// PARAMS:
///    thisProps - 
///    clientBD - []
///    eSelfID - 
///    fTargetAddTime - 
///    fTimeToPrediction - 
PROC HANDLE_TENNIS_BALL_VELOCITY_UPDATE(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, FLOAT &fTargetAddTime, FLOAT &fTimeToPrediction, INT &iBounces)
	CLEAR_TENNIS_CLIENT_FLAG(clientBD[PARTICIPANT_ID_TO_INT()], TMC_BALL_VELOCITY_UPDATE_RECEIVED)
	// Tell the player to re-predict their vFuturePoints array
	CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_REDUCE_PREDICTION_CALC)
	SET_TENNIS_CLIENT_FLAG(clientBD[PARTICIPANT_ID_TO_INT()], TMC_WATCH_THE_COURT)
	iBounces = 0	// Reset for when we go thru catch up time
	SET_TENNIS_MP_HIT_LAST(serverBD, eOtherID)
	COPY_ONE_TENNIS_BALL_TO_ANOTHER(thisProps.tennisBall, thisProps.lerpBall)	// Store off old ball
	SET_TENNIS_BALL_FLAG(thisProps.lerpBall, TBF_UPDATE_PTFX)
	SET_TENNIS_CLIENT_FLAG(clientBD[PARTICIPANT_ID_TO_INT()], TMC_BALL_LERP_TO_POS)
	SET_TENNIS_BALL_POS(thisProps.tennisBall, GET_TENNIS_CLIENT_PRED_BALL_POS(clientBD[PARTICIPANT_ID_TO_INT()]))
	RESET_TENNIS_BALL_TIMERS(thisProps.tennisBall)
	thisProps.tennisBall.vBallVel = GET_TENNIS_CLIENT_PRED_BALL_VEL(clientBD[PARTICIPANT_ID_TO_INT()])
	fTargetAddTime = TO_FLOAT(GET_TENNIS_CLIENT_BALL_CATCHUP_TIME(clientBD[PARTICIPANT_ID_TO_INT()])) / 1000	// catchup time is in ms
	IF fTargetAddTime < 0
		CWARNINGLN( DEBUG_TENNIS, "HANDLE_TENNIS_BALL_VELOCITY_UPDATE detected '<0' add time, changing to frame time, (was ", fTargetAddTime, ")" )
		fTargetAddTime = GET_FRAME_TIME()
	ENDIF
	UPDATE_TENNIS_BALL_TIME_SLICE_VARIABLES(thisProps.tennisBall, TRUE)
	SET_TENNIS_CLIENT_FRAME_STAMP(clientBD[PARTICIPANT_ID_TO_INT()], GET_GAME_TIMER() + MP_BALL_LERP_DURATION - ROUND(GET_FRAME_TIME()*1000))
	CPRINTLN(DEBUG_TENNIS, "TMC_BALL_VELOCITY_UPDATE_RECEIVED :: fTargetAddTime=", fTargetAddTime)
	// Set the X marker
	VECTOR vActualMarker
	vActualMarker = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(thisProps.tennisCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall), thisProps.tennisBall.vBallVel, 
		thisProps.vForward, GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), GET_TENNIS_BALL_SPIN(thisProps.tennisBall), GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
	// Display the X marker
	SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[eSelfID], vActualMarker)
	// Give the bounce stamp a frame of leeway
	SET_TENNIS_BALL_BOUNCE_FRAME_STAMP( thisProps.tennisBall, GET_TENNIS_FRAME_COUNTER(thisProps) - ( FRAME_THRESHOLD_FOR_BOUNCE ) )
	
	thisUIData.fMessageTimer = 0
	thisProps.tennisBall.iBounceCount = 0
	CPRINTLN(DEBUG_TENNIS, "HANDLE_TENNIS_BALL_VELOCITY_UPDATE: Setting X Marker bit")
	SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_X_MARKER)
	// Find the player's prediction point again off the new path
	VECTOR vDestTest
	vDestTest = GET_CLOSEST_POINT_FOR_TENNIS_SWING(thisProps.tennisPlayers[eSelfID].vPos, thisProps.tennisCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall), 
		thisProps.tennisBall.vBallVel, thisProps.vForward, GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), GET_TENNIS_BALL_SPIN(thisProps.tennisBall), 
		GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall), fTimeToPrediction)
	SET_AI_PREDICTION_POINT(thisProps.tennisPlayers[eSelfID].playerAI, GET_BEST_TENNIS_PREDICTION_FROM_OFFSET(thisProps.tennisPlayers[eSelfID], thisProps.vSwingStateOffsets, 
		vDestTest, thisProps.tennisCourt.vCenterCourt.z, fTimeToPrediction))
ENDPROC

PROC HANDLE_TENNIS_SCTV_QUIT_CONDITIONS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_SERVER_BD &serverBD, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, INT missionVariation, TENNIS_PARTICIPANT_INFO &info)
	BOOL bQuit = FALSE
	IF info.iPlayerCount < ENUM_TO_INT(TENNIS_PLAYERS)
		CDEBUG2LN(DEBUG_TENNIS, "info.iPlayers < ENUM_TO_INT(TENNIS_PLAYERS)")
		bQuit = TRUE
	ENDIF
	IF bQuit
		TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, AR_playerQuit, serverBD.iMatchHistoryID, missionVariation)
	ENDIF
ENDPROC

PROC HANDLE_TENNIS_SCTV_BALL_SIMULATION(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, FLOAT &fTimeToPrediction)
	INT iBounces = GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
	IF VALIDATE_USE_BALL_SIMULATOR(thisProps, iBounces, thisUIData.iUtilFlags, GET_TENNIS_SERVER_GAME_STATE(serverBD))
		BOOL bPlaySound = (GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_RALLY)
		#IF IS_DEBUG_BUILD
			DEBUG_RECORD_ENTITY_VECTOR(thisProps.tennisPlayers[eSelfID].pIndex, "vBallPosIntoSim", GET_TENNIS_BALL_POS(thisProps.tennisBall), TRUE)
		#ENDIF
		FLOAT fTargetAddTime
		IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[PARTICIPANT_ID_TO_INT()], TMC_BALL_VELOCITY_UPDATE_RECEIVED)
			HANDLE_TENNIS_BALL_VELOCITY_UPDATE(thisProps, thisUIData, serverBD, clientBD, eSelfID, eOtherID, fTargetAddTime, fTimeToPrediction, iBounces)
		ELSE
			fTargetAddTime = GET_FRAME_TIME()
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
			DEBUG_SPAWN_BALL(thisProps)
			CDEBUG2LN(DEBUG_TENNIS, "PROCESS_TENNIS_SCTV_GAME_STATE :: respawned thisProps.tennisBall")
		ENDIF
		
		UPDATE_BALL_SIMULATOR_TIME_SLICED(thisProps.tennisCourt, thisProps.tennisBall, thisProps.tennisPlayers, thisProps.vForward, iBounces, fTargetAddTime, bPlaySound, default, default, IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED))
		IF HAS_BALL_BOUNCED(thisProps.tennisBall, GET_TENNIS_FRAME_COUNTER(thisProps), thisProps.tennisCourt.vCenterCourt) 
		AND IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED)
			CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED)
		ENDIF
		
		IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[PARTICIPANT_ID_TO_INT()], TMC_BALL_LERP_TO_POS)
			INT iLerpBounces = GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.lerpBall)
			UPDATE_BALL_SIMULATOR_TIME_SLICED(thisProps.tennisCourt, thisProps.lerpBall, thisProps.tennisPlayers, thisProps.vForward, iLerpBounces, GET_FRAME_TIME(), FALSE, TRUE, default, IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED))
			// Compute Alpha value
			FLOAT fClamped = (CLAMP((GET_TENNIS_CLIENT_FRAME_STAMP(clientBD[PARTICIPANT_ID_TO_INT()]) - GET_GAME_TIMER()) / TO_FLOAT(MP_BALL_LERP_DURATION), 0, 1))
			FLOAT fAlpha = 1 - (fClamped * fClamped)
			VECTOR vBallLerped = LERP_VECTOR(GET_TENNIS_BALL_POS(thisProps.lerpBall), GET_TENNIS_BALL_POS(thisProps.tennisBall), fAlpha)
			CDEBUG3LN(DEBUG_TENNIS, "PROCESS_TENNIS_SCTV_GAME_STATE :: TMC_BALL_LERP_TO_POS active :: fAlpha=", fAlpha, ", GET_TENNIS_CLIENT_FRAME_STAMP=", GET_TENNIS_CLIENT_FRAME_STAMP(clientBD[PARTICIPANT_ID_TO_INT()]), ", GET_GAME_TIMER=", GET_GAME_TIMER(), ", MP_BALL_LERP_DURATION=", MP_BALL_LERP_DURATION)
			IF fAlpha >= 1
				CLEAR_TENNIS_CLIENT_FLAG(clientBD[PARTICIPANT_ID_TO_INT()], TMC_BALL_LERP_TO_POS)
			ENDIF
			SET_ENTITY_COORDS(thisProps.tennisBall.oBall, vBallLerped, default, default, default, FALSE)
			UPDATE_TENNIS_BALL_TRAIL(thisProps.lerpBall, vBallLerped)
			STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
			#IF TENNIS_DEBUG_RECORDING
			DEBUG_RECORD_SPHERE("vBallLerped", vBallLerped, 0.1, (<<255,250,150>>))
			DEBUG_RECORD_SPHERE("lerpBall", GET_TENNIS_BALL_POS(thisProps.lerpBall), 0.1, (<<155,155,155>>))
			#ENDIF
		ELSE
			UPDATE_TENNIS_BALL_TRAIL(thisProps.tennisBall, GET_TENNIS_BALL_POS(thisProps.tennisBall))
			STOP_TENNIS_BALL_TRAIL(thisProps.lerpBall)
		ENDIF
	ELSE
		STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
		STOP_TENNIS_BALL_TRAIL(thisProps.lerpBall)
		UPDATE_TENNIS_BALL_TIME_SLICE_VARIABLES(thisProps.tennisBall, FALSE)
		UPDATE_TENNIS_BALL_TIME_SLICE_VARIABLES(thisProps.lerpBall, FALSE)
	ENDIF
ENDPROC

PROC TENNIS_SCTV_HANDLE_SERVE_FLAG(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD[], TENNIS_PARTICIPANT_INFO & info )
	
	INT iParticipantID = NATIVE_TO_INT( NETWORK_GET_HOST_OF_THIS_SCRIPT() )
	IF GET_TENNIS_MP_WHO_IS_SERVING( serverBD ) = TENNIS_PLAYER_HOME
		iParticipantID = TENNIS_GET_OTHER_PARTICIPANT( iParticipantID, info )
	ENDIF
	
	IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iParticipantID], TMC_SERVE_PREPARED) 
	AND IS_TENNIS_CLIENT_FLAG_SET(clientBD[iParticipantID], TMC_SERVE_GRADE_FLAG)
		STRING sAnimDict
		sAnimDict = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING( serverBD )]), "mini@tennis", "mini@tennis@female")
		IF (IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING( serverBD )]), sAnimDict, "serve")
		AND GET_ENTITY_ANIM_CURRENT_TIME(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING( serverBD )]), sAnimDict, "serve") >= SERVE_ACTIVE_PHASE)
		OR (NOT IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING( serverBD )]), sAnimDict, "serve"))
			CPRINTLN(DEBUG_TENNIS, "TENNIS_SCTV_HANDLE_SERVE_FLAG Turning off TMC_SERVE_PREPARED, turning on TMC_BALL_SERVED, Attempting to increase Serve Count")
		
		
		
			DETACH_ENTITY(thisProps.tennisBall.oBall,FALSE)
			LAUNCH_BALL(thisProps, thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)], thisProps.grades[GET_TENNIS_CLIENT_SERVE_GRADE(clientBD[iParticipantID])])
			
			IF GET_TENNIS_CLIENT_SERVE_GRADE(clientBD[iParticipantID]) = LD_SERVE_S OR GET_TENNIS_CLIENT_SERVE_GRADE(clientBD[iParticipantID]) = LD_SERVE_S_PLUS
				PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SMASH_MASTER", GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING( serverBD )]))
			ELSE
				PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SERVE_MASTER", GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING( serverBD )]))
			ENDIF
			
			
			
			//Get my prediction here and set the new X-Marker
			VECTOR vDestTest
			vDestTest = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(thisProps.tennisCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall), thisProps.tennisBall.vBallVel, 
				thisProps.vForward, GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), GET_TENNIS_BALL_SPIN(thisProps.tennisBall), GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
			SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)], vDestTest)
			
			// Display the X marker
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_X_MARKER)
				SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_X_MARKER)
				CPRINTLN(DEBUG_TENNIS, "TENNIS_SCTV_HANDLE_SERVE_FLAG Showing where the ball will land")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Takes the variables cached by the clients and sets them as the spectator's variables.
/// PARAMS:
///    thisProps - Contains the local variables we're setting
///    clientBD - Contains the cached variables we're setting from
///    eSelfID - The selfID of the spectator, the ID of the spectated client.
PROC TENNIS_SPECTATOR_HANDLE_CACHED_CAMERA_VARIABLES(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eSelfID)
	
	IF NOT TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	INT iSCTVFocusParticipantID = NATIVE_TO_INT( NETWORK_GET_HOST_OF_THIS_SCRIPT() )
	
	IF IS_ENTITY_A_PED( GET_SPECTATOR_CURRENT_FOCUS_PED() )
		iSCTVFocusParticipantID = NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX( NETWORK_GET_PLAYER_INDEX_FROM_PED( GET_SPECTATOR_CURRENT_FOCUS_PED() ) ) )
	ELIF IS_ENTITY_A_PED( GET_SPECTATOR_DESIRED_FOCUS_PED() )
		iSCTVFocusParticipantID = NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX( NETWORK_GET_PLAYER_INDEX_FROM_PED( GET_SPECTATOR_DESIRED_FOCUS_PED() ) ) )
	ENDIF
	
	thisProps.tennisPlayers[eSelfID].vMyForward = clientBD[iSCTVFocusParticipantID].vForward
	thisProps.tennisPlayers[eSelfID].vMyRight   = clientBD[iSCTVFocusParticipantID].vRight
	thisProps.tennisPlayers[eSelfID].eCourtside = clientBD[iSCTVFocusParticipantID].eCourtside
	
ENDPROC

PROC PROCESS_TENNIS_SCTV_GAME_STATE(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, INT missionVariation, TENNIS_PARTICIPANT_INFO &info)
	CDEBUG3LN(DEBUG_TENNIS, "PROCESS_TENNIS_SCTV_GAME_STATE :: called")
//	IF IS_SCREEN_FADED_OUT()
//		DO_SCREEN_FADE_IN(200)
//	ENDIF
	TENNIS_SPECTATOR_HANDLE_CACHED_CAMERA_VARIABLES(thisProps, clientBD, eSelfID)
	FLOAT fTimeToPrediction
	HANDLE_TENNIS_SCTV_QUIT_CONDITIONS(thisProps, thisUIData, serverBD, eSelfID, eOtherID, missionVariation, info)
	PROCESS_TENNIS_MP_METER_AND_BITMASK_UI_FOR_SCTV( thisUIData )
	HANDLE_TENNIS_SCTV_BALL_SIMULATION(thisProps, thisUIData, serverBD, clientBD, eSelfID, eOtherID, fTimeToPrediction)
	IF GET_TENNIS_SERVER_GAME_STATE(serverBD) < TGSE_OUTRO
		PROCESS_CAMERA_STATE(thisProps, thisUIData, eSelfID, thisProps.tennisPlayers[eSelfID].eCourtSide, GET_TENNIS_MP_HIT_LAST(serverBD))
	ENDIF
	IF GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_RALLY
		IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[PARTICIPANT_ID_TO_INT()], TMC_OPP_SEQUENCE_DONE)
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[PARTICIPANT_ID_TO_INT()], TMC_OPP_SEQUENCE_DONE)
		ENDIF
	ENDIF
	COMMON_EVERY_FRAME_DATA(thisProps, GET_TENNIS_CLIENT_GAME_STATE(clientBD[PARTICIPANT_ID_TO_INT()]), thisUIData.iUtilFlags, eSelfID, eOtherID, info, IS_TENNIS_CLIENT_FLAG_SET(clientBD[PARTICIPANT_ID_TO_INT()], TMC_WALKING_TO_POS))
	// Game type broadcast-y stuff
	CHECK_AND_HANDLE_TENNIS_BALL_RECEIVED_FLAG(clientBD[PARTICIPANT_ID_TO_INT()], thisProps, eSelfID)
	//TENNIS_SCTV_HANDLE_SERVE_FLAG(thisProps, thisUIData, serverBD, clientBD, info)
ENDPROC

/// PURPOSE:
///    Caches off the player's current forward and right vector along with their courtside.
/// PARAMS:
///    thisProps - Contains all the vectors for the players we need to cache.
///    clientBD - Stores the cached variables so the spectator can read them.
///    eSelfID - The selfID of the local player.
PROC TENNIS_CLIENT_CACHE_CAMERA_VARIABLES(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eSelfID)
	
	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	INT iParticipantID = PARTICIPANT_ID_TO_INT()
	
	clientBD[iParticipantID].vForward   = thisProps.tennisPlayers[eSelfID].vMyForward
	clientBD[iParticipantID].vRight     = thisProps.tennisPlayers[eSelfID].vMyRight
	clientBD[iParticipantID].eCourtSide = thisProps.tennisPlayers[eSelfID].eCourtSide
	
ENDPROC

/// PURPOSE:
///    Contains the state machine that runs tennis as a game
/// PARAMS:
///    thisProps - 
///    thisUIData - 
///    serverBD - Some server data is taken care of here, like the ball bounces and scores
///    clientBD[] - use the full array
///    eSelfID - 
PROC PROCESS_MP_CLIENT_GAME_STATE(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, BOOL bGamePaused, INT missionVariation, TENNIS_PARTICIPANT_INFO &info)
	FLOAT fTimeToPrediction
	
	TENNIS_CLIENT_CACHE_CAMERA_VARIABLES(thisProps, clientBD, eSelfID)
	
	PROCESS_TENNIS_MP_QUIT_UI(thisProps, thisUIData, eSelfID, serverBD, clientBD, serverBD.playerScores, GET_TENNIS_MP_CURRENT_SET(serverBD), IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_IS_REMATCH_FLAG), GET_TENNIS_MP_NUM_SETS(serverBD), info)
	
	PROCESS_TENNIS_MP_METER_AND_BITMASK_UI(thisProps, serverBD, thisUIData, thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)], eSelfID, eOtherID)
	
	INT iBounces = GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
	
	IF IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_BOUNCED_IN_TIME_SLICE)
		CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_BOUNCED_IN_TIME_SLICE)
	ENDIF
	
	#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_ENTITY(thisProps.tennisPlayers[eOtherID].pIndex)
		DEBUG_RECORD_ENTITY(thisProps.tennisPlayers[eSelfID].pIndex)
	#ENDIF
	
	IF VALIDATE_USE_BALL_SIMULATOR(thisProps, iBounces, thisUIData.iUtilFlags, GET_TENNIS_SERVER_GAME_STATE(serverBD))
	AND VALIDATE_TENNIS_PLAYERS_INFO( info )
		BOOL bPlaySound = (GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_RALLY)
		#IF IS_DEBUG_BUILD
			DEBUG_RECORD_ENTITY_VECTOR(thisProps.tennisPlayers[eSelfID].pIndex, "vBallPosIntoSim", GET_TENNIS_BALL_POS(thisProps.tennisBall), TRUE)
		#ENDIF
		FLOAT fTargetAddTime
		IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_BALL_VELOCITY_UPDATE_RECEIVED)
			HANDLE_TENNIS_BALL_VELOCITY_UPDATE(thisProps, thisUIData, serverBD, clientBD, eSelfID, eOtherID, fTargetAddTime, fTimeToPrediction, iBounces)
		ELSE
			fTargetAddTime = GET_FRAME_TIME()
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
			DEBUG_SPAWN_BALL(thisProps)
			CDEBUG2LN(DEBUG_TENNIS, "PROCESS_MP_CLIENT_GAME_STATE :: respawned thisProps.tennisBall")
		ENDIF
		UPDATE_BALL_SIMULATOR_TIME_SLICED(thisProps.tennisCourt, thisProps.tennisBall, thisProps.tennisPlayers, thisProps.vForward, iBounces, fTargetAddTime, bPlaySound, default, NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_WATCH_THE_COURT), IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED))
		IF IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_RICOCHETED)
			CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_RICOCHETED)
			CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_REDUCE_PREDICTION_CALC)
			TENNIS_MP_BROADCAST_UPDATE_BALL(GET_TENNIS_BALL_POS(thisProps.tennisBall), thisProps.tennisBall.vBallVel, GET_TENNIS_BALL_SPIN(thisProps.tennisBall), eSelfID, clientBD, TBU_NET_RICOCHET, GET_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eSelfID]), info)
		ENDIF
		
		IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_BALL_LERP_TO_POS)
			INT iLerpBounces = GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.lerpBall)
			UPDATE_BALL_SIMULATOR_TIME_SLICED(thisProps.tennisCourt, thisProps.lerpBall, thisProps.tennisPlayers, thisProps.vForward, iLerpBounces, GET_FRAME_TIME(), FALSE, TRUE, default, IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED))
			// Compute Alpha value
			FLOAT fClamped = (CLAMP((GET_TENNIS_CLIENT_FRAME_STAMP(clientBD[info.iPlayers[eSelfID]]) - GET_GAME_TIMER()) / TO_FLOAT(MP_BALL_LERP_DURATION), 0, 1))
			FLOAT fAlpha = 1 - (fClamped * fClamped)
			VECTOR vBallLerped = LERP_VECTOR(GET_TENNIS_BALL_POS(thisProps.lerpBall), GET_TENNIS_BALL_POS(thisProps.tennisBall), fAlpha)
			CDEBUG3LN(DEBUG_TENNIS, "TMC_BALL_LERP_TO_POS active :: fAlpha=", fAlpha, ", GET_TENNIS_CLIENT_FRAME_STAMP=", GET_TENNIS_CLIENT_FRAME_STAMP(clientBD[info.iPlayers[eSelfID]]), ", GET_GAME_TIMER=", GET_GAME_TIMER(), ", MP_BALL_LERP_DURATION=", MP_BALL_LERP_DURATION)
			IF fAlpha >= 1
				CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_BALL_LERP_TO_POS)
			ENDIF
			SET_ENTITY_COORDS(thisProps.tennisBall.oBall, vBallLerped, default, default, default, FALSE)
			UPDATE_TENNIS_BALL_TRAIL(thisProps.lerpBall, vBallLerped)
			STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
			#IF TENNIS_DEBUG_RECORDING
			DEBUG_RECORD_SPHERE("vBallLerped", vBallLerped, 0.1, (<<255,250,150>>))
			DEBUG_RECORD_SPHERE("lerpBall", GET_TENNIS_BALL_POS(thisProps.lerpBall), 0.1, (<<155,155,155>>))
			#ENDIF
		ELSE
			UPDATE_TENNIS_BALL_TRAIL(thisProps.tennisBall, GET_TENNIS_BALL_POS(thisProps.tennisBall))
			STOP_TENNIS_BALL_TRAIL(thisProps.lerpBall)
		ENDIF
	ELSE
		STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
		STOP_TENNIS_BALL_TRAIL(thisProps.lerpBall)
		UPDATE_TENNIS_BALL_TIME_SLICE_VARIABLES(thisProps.tennisBall, FALSE)
		UPDATE_TENNIS_BALL_TIME_SLICE_VARIABLES(thisProps.lerpBall, FALSE)
	ENDIF
							
	IF GET_TENNIS_SERVER_GAME_STATE(serverBD) < TGSE_OUTRO
		PROCESS_CAMERA_STATE(thisProps, thisUIData, eSelfID, thisProps.tennisPlayers[eSelfID].eCourtSide, GET_TENNIS_MP_HIT_LAST(serverBD))
	ENDIF
	IF VALIDATE_TENNIS_PLAYERS_INFO(info)
		COMMON_EVERY_FRAME_DATA(thisProps, GET_TENNIS_CLIENT_GAME_STATE(clientBD[info.iPlayers[eSelfID]]), thisUIData.iUtilFlags, eSelfID, eOtherID, info, IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_WALKING_TO_POS))
		
		IF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
			SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_ANIM_PLAYING)
		ELIF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_ANIM_PLAYING)
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_ANIM_PLAYING)
		ENDIF
	ELIF GET_TENNIS_SERVER_GAME_STATE(serverBD) <> TGSE_OUTRO
		CERRORLN(DEBUG_TENNIS, "VALIDATE_TENNIS_PLAYERS_INFO(info) failed, exiting main loop")
		SET_TENNIS_FLOW_FLAG(thisProps, TFF_CONTINUE_AFTER_VOTE_PASSING)
		SET_TENNIS_CLIENT_MP_STATE(clientBD[PARTICIPANT_ID_TO_INT()], TMPS_QUIT_THE_GAME)
		EXIT
	ENDIF
	
	IF GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_RALLY AND NOT IS_TRANSITION_ACTIVE()	//OR GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_PREPARE_SERVE
		UPDATE_TENNIS_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext, FALSE)
		HANDLE_TENNIS_INSTRUCTION_INPUT(thisUIData)	//, serverBD)
	ENDIF
	
	// Drop the player out of tennis mode if they need it.
	IF IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eSelfID], TPB_DROP_OUT_OF_TENNIS_MODE)
		IF IS_ENTITY_PLAYING_ANIM(thisProps.tennisPlayers[eSelfID].pIndex, "mini@tennis", "ready_2_idle")
			IF GET_ENTITY_ANIM_CURRENT_TIME(thisProps.tennisPlayers[eSelfID].pIndex, "mini@tennis", "ready_2_idle") >= DISABLE_TENNIS_MODE_ANIM_PHASE
				CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_DROP_OUT_OF_TENNIS_MODE)
				SET_TENNIS_PLAYER_TO_TENNIS_MODE(thisProps.tennisPlayers[eSelfID].pIndex, FALSE)
				SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "weapons@tennis@male")
				CDEBUG2LN(DEBUG_TENNIS, "PROCESS_MP_CLIENT_GAME_STATE :: Dropped out of tennis mode")
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH GET_TENNIS_SERVER_GAME_STATE(serverBD)
		CASE TGSE_PREPARE_SERVE
			IF IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_GAME_HAS_SHOWN_SIDE_CHANGE_MSG)
				CPRINTLN(DEBUG_TENNIS, "Clearing side change msg")
				CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_GAME_HAS_SHOWN_SIDE_CHANGE_MSG)
			ENDIF
			
			SHOW_PLAYER_CARD_NO_MATTER_WHAT( thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), info )
			SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_UPDATE_SCORES)
			UPDATE_TENNIS_UI_POINTS(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), info)
			TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_POINTS(FALSE)
			SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eSelfID].playerAI, ASE_AI_IDLE)
			SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eOtherID].playerAI, ASE_AI_IDLE)
			// Reset stroke so the dynamic camera doesn't get stuck rendering at the distance for SHOT_LOB
			SET_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eSelfID], SHOT_NORMAL)
			SET_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eOtherID], SHOT_NORMAL)
			// Reset tennis ball bounce count, it's nutty that this wasn't here before.
			RESET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
			RESET_TENNIS_CLIENT_RALLY_COUNTER(clientBD[info.iPlayers[eSelfID]])
				
			IF GET_TENNIS_SERVER_PREV_GAME_STATE(serverBD) = TGSE_INTERSTITIAL
			OR GET_TENNIS_SERVER_PREV_GAME_STATE(serverBD) = TGSE_CHANGE_SERVERS
			OR GET_TENNIS_SERVER_PREV_GAME_STATE(serverBD) = TGSE_SIDE_CHANGE
			OR GET_TENNIS_SERVER_PREV_GAME_STATE(serverBD) = TGSE_OUTRO
			OR GET_TENNIS_SERVER_PREV_GAME_STATE(serverBD) = TGSE_PREPARE_REMATCH
				CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_FIRST_FAULT)
				SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, thisUIData.tennisCameras.eChosenCam, FALSE)
				TENNIS_MP_BROADCAST_SCTV_UI_SET_CAMERA_STATE( thisUIData.tennisCameras.eChosenCam, FALSE )
			ENDIF
			
			// Clearing the bits from broadcast events to make sure they aren't processed if the event came in after a state change
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_BALL_HIT)
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_OOB)
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_HIT_BEFORE_BOUNCE)
			// Clearing Fouled flag in case we skipped it for other splash UI
			CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_FOULED)
			// Clearing the Game won bit if it was overlooked by a desync
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_GAME_WAS_WON)
			// At this point no one should be watching the court for bounces
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_WATCH_THE_COURT)
			// Clear the bitmask saying the game ended, this is only true if we rematched and now we're back in it!
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_MATCH_IS_OVER)
			// Clear the stop meter bit just in case the opponent fouled
			CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_STOP_METER)
			// Clear the serve grade flag in case of fouls
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_GRADE_FLAG)
			// Clear the ball update received flag, a new rally has begun
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_INITIAL_BALL_UPDATE_RECEIVED)
			// Clear the ball secondary update flag
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_BALL_VELOCITY_UPDATE_RECEIVED)
			// Clear the ball lerp to position flag
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_BALL_LERP_TO_POS)
			// Clear the before bounce bitmask so UI displays correctly
			CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_HIT_BEFORE_BOUNCE)
			// Stop the tennis ball from smoking
			STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
			// Clearing the Ball Served Bit, it hasn't been yet so let's make sure it's off
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_BALL_SERVED)
			// Clear the bit that says the ball hit the player. If set, this bit will cause the VALIDATE_USE_BALL_SIMULATOR func to fail.
			CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_HIT_PLAYER)
			// Clear the ball bounced in time slice bit
			CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_BOUNCED_IN_TIME_SLICE)
			// Allow the ball to get updated again
			CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_PAUSE_UPDATE)
			// Clear the bounce timeout flag, just in case
			CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_NO_BOUNCE_TIMEOUT_OCCURED)
			// Clear all AI Flags and certain player flags
			CLEAR_TENNIS_PLAYER_CONTROL_BITS(thisProps.tennisPlayers[eSelfID])
			// Clear the event flag so we can swing again
			CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_TENNIS_SWING_EVENT_FLAG)
			// Clear the forced update flag for the players
			CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_FORCED_UPDATE)
			CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eOtherID], TPB_FORCED_UPDATE)
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_QUIT_BIT)
			CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SWING_METER_POS_SET)
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_SPINNER_10SEC_UPDATE)
			// Allows the players to be set to walk back between rallies
			CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_NEW_RALLY)
			// Allows the player to hit the ball again.
			CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_ANIM)
			IF IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eSelfID], TPB_SWINGING_AIMLESSLY)
				CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_SWINGING_AIMLESSLY)
			ENDIF
			
			SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ACE)
			
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, eSelfID = GET_TENNIS_MP_WHO_IS_SERVING(serverBD), bGamePaused)
			
			RESET_TENNIS_BALL_SPIN(thisProps.tennisBall)
			
			IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SECOND_SERVE)
				SHOW_TENNIS_SECOND_SERVE_MESSAGE(thisUIData.tennisUI)
				TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SECOND_SERVE_MESSAGE()
				CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SECOND_SERVE)
			ENDIF

			IF NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SERVER_FAULTED)
				//Server Function
				//Reset the serve count
				SET_TENNIS_MP_SERVE_COUNT(serverBD, 0)
				CPRINTLN(DEBUG_TENNIS, "TGSE_PREPARE_SERVE: Resetting the serve count.")
			ELSE
				//There was a fault, clear the fault flag
				CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_SERVER_FAULTED)
				CPRINTLN(DEBUG_TENNIS, "TGSE_PREPARE_SERVE: Fault Flag Cleared")
				IF GET_TENNIS_MP_SERVE_COUNT(serverBD) >= 2
					CPRINTLN(DEBUG_TENNIS, "TGSE_PREPARE_SERVE: Reset Serve Count after Double Fault")
					SET_TENNIS_MP_SERVE_COUNT(serverBD, 0)
				ENDIF
			ENDIF
			
			IF NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_WALKING_TO_POS)
				ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eSelfID, eOtherID)
				IF eSelfID = (GET_TENNIS_MP_WHO_IS_SERVING(serverBD))
					DEBUG_SPAWN_BALL(thisProps, TRUE)
					SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID)
				ELSE
					SET_TENNIS_PLAYER_RECEIVE_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID)
					ATTACH_TENNIS_BALL_TO_PLAYER(thisProps.tennisPlayers[eOtherID], thisProps.tennisBall)
				ENDIF
			ENDIF
			
			IF eSelfID = (GET_TENNIS_MP_WHO_IS_SERVING(serverBD))
				SETUP_TENNIS_SERVING_CONTROLS(
					thisUIData.tennisUI, 
					GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_SERVE_TOSS, 
					VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(serverBD.playerScores, GET_TENNIS_MP_CURRENT_SET(serverBD), IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_IS_REMATCH_FLAG)), GET_TENNIS_MP_NUM_SETS(serverBD))
			ELSE
				SETUP_TENNIS_PLAYING_CONTROLS(thisUIData.tennisUI, FALSE, VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(serverBD.playerScores, GET_TENNIS_MP_CURRENT_SET(serverBD), IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_IS_REMATCH_FLAG)), GET_TENNIS_MP_NUM_SETS(serverBD))
			ENDIF
			
			CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_WALKING_TO_POS)
			
			TASK_TENNIS_PLAYER_WATCH_BALL(thisProps.tennisPlayers[eSelfID], thisProps.tennisBall, TRUE)
			
			SET_TENNIS_BALL_VISIBILITY(thisProps.tennisBall.oBall, TRUE)
			
			SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_RALLY, TMPS_WAIT_FOR_RALLY)
			
			SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_PREPARED)
			CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_FIRST_SERVE)
			
			ALLOW_TENNIS_PLAYER_CONTROL(TA_MULTIPLAYER, TRUE)
			
			CPRINTLN(DEBUG_TENNIS, "TGSE_PREPARE_SERVE: Serve Prepared")
		BREAK
		
		CASE TGSE_RALLY
			IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_OPP_SEQUENCE_DONE)
				CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_OPP_SEQUENCE_DONE)
			ENDIF
			IF GET_TENNIS_UI_STATE(thisUIData) <> TENNIS_UI_QUIT AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_QUIT_BIT)
				ALLOW_TENNIS_PLAYER_CONTROL(TA_MULTIPLAYER, TRUE)
			ENDIF
			CHECK_AND_HANDLE_TENNIS_BALL_RECEIVED_FLAG(clientBD[PARTICIPANT_ID_TO_INT()], thisProps, eSelfID)
			
			IF (IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_PREPARED)/* AND (GET_TENNIS_MP_WHO_IS_SERVING(serverBD)) = eSelfID*/)
			AND (GET_TENNIS_UI_STATE(thisUIData) <> TENNIS_UI_QUIT AND GET_TENNIS_UI_STATE(thisUIData) <> TENNIS_UI_FAR_AWAY)
				UPDATE_TENNIS_SHOT_CLOCK(serverBD.timeServeTimeout, thisUIData.tennisUI)
			ENDIF
			IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SERVE_MESSAGE)
				IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
					SHOW_TENNIS_SERVE_MESSAGE(thisUIData.tennisUI, GET_TENNIS_PLAYER_NAME(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD)), GET_TENNIS_MP_WHO_IS_SERVING(serverBD) = eSelfID)
					TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SERVE_MESSAGE( GET_TENNIS_MP_WHO_IS_SERVING(serverBD) )
				#IF IS_DEBUG_BUILD
				ELSE
					CERRORLN(DEBUG_TENNIS, "IS_ANY_BIG_MESSAGE_BEING_DISPLAYED() :: skipping serve message")
				#ENDIF
				ENDIF
				CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SERVE_MESSAGE)
			ENDIF
			
			IF HAS_TENNIS_AWARD_VIEW_TIME_ELAPSED(thisUIData.tennisUI)
				IF NOT MPGlobals.sMinigameMPGlobals.bHideMPAwards
					MPGlobals.sMinigameMPGlobals.bHideMPAwards = TRUE
					CPRINTLN(DEBUG_TENNIS, "MPGlobals.sMinigameMPGlobals.bHideMPAwards = TRUE :: Awards are hidden.")
				ENDIF
			ELSE
				INCREASE_TENNIS_AWARD_VIEW_TIME(thisUIData.tennisUI)
			ENDIF
			
			TENNIS_MP_HANDLE_OFFSIDES_PLAYERS(thisProps, thisUIData, serverBD, clientBD, eSelfID, eOtherID, info)
			
			BOOL bStroke, bTaskedReact
			
			IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_HIT_BEFORE_BOUNCE)
				//A point has been won
				CPRINTLN(DEBUG_TENNIS, "Processing the TMC_HIT_BEFORE_BOUNCE flag")
				SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, GET_TENNIS_MP_WHO_IS_SERVING(serverBD))
				SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
				CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_HIT_BEFORE_BOUNCE)
			ENDIF
			
			//Handle button presses from the player
			//First look for the serve
			IF eSelfID = (GET_TENNIS_MP_WHO_IS_SERVING(serverBD)) AND IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_PREPARED)
				// Handle the serving UI
				IF UPDATE_TENNIS_SERVING_CONTROLS(thisUIData.tennisUI, GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_SERVE_TOSS)
					SETUP_TENNIS_SERVING_CONTROLS(thisUIData.tennisUI, GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_SERVE_TOSS, 
						VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(serverBD.playerScores, GET_TENNIS_MP_CURRENT_SET(serverBD), IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_IS_REMATCH_FLAG)), GET_TENNIS_MP_NUM_SETS(serverBD))
				ENDIF
				LAUNCH_DETAILS serveGrade
				IF UPDATE_TENNIS_PLAYER_SERVE_TAP_TWICE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), serveGrade, bGamePaused)
					// Set the broadcast event that the ball has been hit
					// If the TMC_SERVE_GRADE_FLAG flag is not set send the FAULT_GRADE
					IF NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_GRADE_FLAG)
						CPRINTLN(DEBUG_TENNIS, "Post:  UPDATE_TENNIS_PLAYER_SERVE_TAP_TWICE" )
						CPRINTLN(DEBUG_TENNIS, "iParticipantID: ", PARTICIPANT_ID_TO_INT())
						CPRINTLN(DEBUG_TENNIS, "info.iPlayers[eSelfID]: ", info.iPlayers[eSelfID])
						CPRINTLN(DEBUG_TENNIS, "eSelfID", ENUM_TO_INT(eSelfID))
						TENNIS_MP_BROADCAST_SERVE_EVENT(0, LD_SERVE_FAULT, GET_TENNIS_BALL_POS(thisProps.tennisBall))
					ENDIF
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_GRADE_FLAG)
					// Server func for setting hit last
					SET_TENNIS_MP_HIT_LAST(serverBD, eSelfID)
					// Clear the bitmask that makes us watch the court for bounces, we're relying on the receiver for that
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_WATCH_THE_COURT)
					// Increase the serve count
					SET_TENNIS_MP_SERVE_COUNT(serverBD, GET_TENNIS_MP_SERVE_COUNT(serverBD)+1)
					// Clear the serve prepared bitmask so we update strokes normally
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_PREPARED)
					// Set the UI so they player can start the help text for swinging
					SETUP_TENNIS_PLAYING_CONTROLS(thisUIData.tennisUI, 
						IS_TENNIS_PLAYER_IN_SERVICE_BOXES(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)], thisProps.tennisCourt, thisProps.vRight), 
						VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(serverBD.playerScores, GET_TENNIS_MP_CURRENT_SET(serverBD), IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_IS_REMATCH_FLAG)),
						GET_TENNIS_MP_NUM_SETS(serverBD))
						
					TENNIS_MP_BROADCAST_SCTV_LAUNCH_BALL( serverBD, eSelfID, serveGrade.iForce, serveGrade.iAngle, thisProps.tennisPlayers[eSelfID].fLeftRight, thisProps.tennisPlayers[eSelfID].vMyForward, thisProps.tennisPlayers[eSelfID].vMyRight )
					CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY: Served the Ball, Turning the Serve Prepared bit and the Watch the Court bit off")
				ELIF IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eSelfID], TPB_STOP_METER) 
				AND NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_GRADE_FLAG)
					INT iAngle
					iAngle = GET_TENNIS_PLAYER_ANALOG_X_VALUE(thisProps.tennisPlayers[eSelfID])
					
					IF IS_USING_CURSOR(FRONTEND_CONTROL)
						iAngle = ROUND(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X) * 256.0 ) - 128
					ENDIF
					
					//All of these serve grades should be fair serves
					IF GET_TENNIS_PLAYER_POWER_TIMER(thisProps.tennisPlayers[eSelfID]) > SERVE_S_PLUS
						TENNIS_MP_BROADCAST_SERVE_EVENT(iAngle, LD_SERVE_S_PLUS, GET_TENNIS_BALL_POS(thisProps.tennisBall))
						SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED)
					ELIF GET_TENNIS_PLAYER_POWER_TIMER(thisProps.tennisPlayers[eSelfID]) > SERVE_S
						TENNIS_MP_BROADCAST_SERVE_EVENT(iAngle, LD_SERVE_S, GET_TENNIS_BALL_POS(thisProps.tennisBall))
					ELIF GET_TENNIS_PLAYER_POWER_TIMER(thisProps.tennisPlayers[eSelfID]) > SERVE_A
						TENNIS_MP_BROADCAST_SERVE_EVENT(iAngle, LD_SERVE_A, GET_TENNIS_BALL_POS(thisProps.tennisBall))
					ELIF GET_TENNIS_PLAYER_POWER_TIMER(thisProps.tennisPlayers[eSelfID]) > SERVE_B
						TENNIS_MP_BROADCAST_SERVE_EVENT(iAngle, LD_SERVE_B, GET_TENNIS_BALL_POS(thisProps.tennisBall))
					ELIF GET_TENNIS_PLAYER_POWER_TIMER(thisProps.tennisPlayers[eSelfID]) > SERVE_C
						TENNIS_MP_BROADCAST_SERVE_EVENT(iAngle, LD_SERVE_C, GET_TENNIS_BALL_POS(thisProps.tennisBall))
					ELIF GET_TENNIS_PLAYER_POWER_TIMER(thisProps.tennisPlayers[eSelfID]) >= SERVE_D
						TENNIS_MP_BROADCAST_SERVE_EVENT(iAngle, LD_SERVE_D, GET_TENNIS_BALL_POS(thisProps.tennisBall))
					ELSE
						CPRINTLN(DEBUG_TENNIS, "TENNIS_MP_BROADCAST_SERVE_EVENT broadcasting default, fPow was too low. Contact Rob Pearsall.")
						TENNIS_MP_BROADCAST_SERVE_EVENT(iAngle, LD_SERVE_D, GET_TENNIS_BALL_POS(thisProps.tennisBall))
					ENDIF
					// Set the server as the player who hit last
					SET_TENNIS_MP_HIT_LAST(serverBD, eSelfID)
					// Set the TMC_SERVE_GRADE_FLAG flag here
					SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_GRADE_FLAG)
				ENDIF
			//Then update as normal
			ELSE
				// Handle the playing UI
				BOOL bLobAvailable
				bLobAvailable = IS_TENNIS_PLAYER_IN_SERVICE_BOXES(thisProps.tennisPlayers[eOtherID], thisProps.tennisCourt, thisProps.vRight)
				IF UPDATE_TENNIS_PLAYING_CONTROLS(thisUIData.tennisUI, bLobAvailable)
					SETUP_TENNIS_PLAYING_CONTROLS(thisUIData.tennisUI, bLobAvailable, VALIDATE_TENNIS_IS_IN_FIRST_TWO_SHOTS(serverBD.playerScores, GET_TENNIS_MP_CURRENT_SET(serverBD), IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_IS_REMATCH_FLAG)), GET_TENNIS_MP_NUM_SETS(serverBD))
				ENDIF
				IF NOT IS_TIMER_STARTED(thisUIData.tennisUI.sUITimer) OR IS_TIMER_PAUSED(thisUIData.tennisUI.sUITimer)
					//Restarts the timer used in interstitials
					RESTART_TIMER_NOW(thisUIData.tennisUI.sUITimer)
					CPRINTLN(DEBUG_TENNIS, "Restarting the sUITimer, this player should not be serving")
				ENDIF
				
				bTaskedReact = IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_ANIM)
				IF NOT bGamePaused
					bStroke = UPDATE_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eSelfID], thisProps, serverBD.playerScores, GET_TENNIS_MP_HIT_LAST(serverBD), eSelfID, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), GET_TENNIS_MP_SERVING_SIDE(serverBD), IS_TENNIS_MP_ON_TIE_BREAK(serverBD), clientBD, info) AND NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_PREPARED)
				ENDIF
				IF bTaskedReact <> IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_ANIM)
//					TENNIS_MP_BROADCAST_REACT(clientBD)
				ENDIF
				IF bStroke
					IF eSelfID <> (GET_TENNIS_MP_HIT_LAST(serverBD))
						//If I hit the ball before it bounces and it was just served I lose a point
						IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_BALL_SERVED) AND GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall) = 0
							CPRINTLN(DEBUG_TENNIS, "I hit the ball before the bounce on a serve, moving to point won.")
							SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, eOtherID)
							SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
							TENNIS_MP_BROADCAST_HIT_BEFORE_BOUNCE(thisUIData.tennisUI.iUIFlags)
							CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ACE)
						ENDIF
						RESET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
						SET_TENNIS_MP_HIT_LAST(serverBD, eSelfID)
					ELSE
						//Lose a point, you hit the ball twice
						CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY: Point won because eHitLast hit the ball again")
						SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, eOtherID)
						SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
						TENNIS_MP_BROADCAST_POINT_WON(eOtherID)
					ENDIF
					
					CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ACE)
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_WATCH_THE_COURT)
					TURN_TENNIS_X_MARKER_OFF(thisUIData)
					
				ELIF IS_TENNIS_PLAYER_FLAG_SET(thisProps.tennisPlayers[eSelfID], TPB_HIT_BY_BALL) 	//AND GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall) = 0
					CPRINTLN(DEBUG_TENNIS, "I got hit by a ball before it bounced, moving to point won.")
					SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, eOtherID)
					SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
					TENNIS_MP_BROADCAST_HIT_BY_BALL()
					CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ACE)
				ELIF GET_TENNIS_MP_HIT_LAST(serverBD) = eSelfID OR (DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall))
					UPDATE_TENNIS_PLAYER_SWINGING_AIMLESSLY(thisProps.tennisPlayers[eSelfID], thisProps.tennisBall)
				ENDIF
			ENDIF
			
			//Update the mover for the player
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, (eSelfID = (GET_TENNIS_MP_WHO_IS_SERVING(serverBD)) AND IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_PREPARED)), bGamePaused)
			
			//Observe the ball and check for bounces
			//Was the ball hit to us?
			IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_BALL_HIT)
				CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_BALL_HIT)
				SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_WATCH_THE_COURT)
				
				CPRINTLN(DEBUG_TENNIS, "The ball has been hit to us")
				RESET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
				CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ACE)
				
				VECTOR vDestTest
				vDestTest = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(thisProps.tennisCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall), thisProps.tennisBall.vBallVel,
					thisProps.vForward, GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), GET_TENNIS_BALL_SPIN(thisProps.tennisBall), GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
				SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[eSelfID], vDestTest)
					
				//Predict where the tennis ball will land and tell the ped not to chase it if it is OOB
				//Do this after the bounce count check to make sure we're not checking where it lands after the bounce.
				IF NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eSelfID].playerAI, TAF_CHECK_FOR_OOB)
				AND GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall) < 1 
				AND IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_BALL_LERP_TO_POS)
				AND NOT IS_POINT_INSIDE_TENNIS_AREA(vDestTest, thisProps.tennisCourt.vCourtCorners[0] + (thisProps.vRight * 0.5), thisProps.tennisCourt.vCourtCorners[3] + (thisProps.vRight * 0.5), GET_TENNIS_COURT_WIDTH(thisProps.tennisCourt))
					CPRINTLN(DEBUG_TENNIS, "TMC_BALL_HIT: Ball is out of bounds")
					IF GET_TENNIS_PLAYER_SWING_STATE(thisProps.tennisPlayers[eSelfID]) = TSS_NO_SWING
						CPRINTLN(DEBUG_TENNIS, "Telling ", thisprops.tennisPlayers[eSelfID].playerAI.texName, " to stop chasing the ball")
						SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[eSelfID].playerAI, TAF_CHECK_FOR_OOB)
					ENDIF
				ENDIF
				
				vDestTest = GET_CLOSEST_POINT_FOR_TENNIS_SWING(thisProps.tennisPlayers[eSelfID].vPos, thisProps.tennisCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall), 
					thisProps.tennisBall.vBallVel, thisProps.vForward, GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), GET_TENNIS_BALL_SPIN(thisProps.tennisBall), 
					GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall), fTimeToPrediction)
				SET_AI_PREDICTION_POINT(thisProps.tennisPlayers[eSelfID].playerAI, GET_BEST_TENNIS_PREDICTION_FROM_OFFSET(thisProps.tennisPlayers[eSelfID], 
					thisProps.vSwingStateOffsets, vDestTest, thisProps.tennisCourt.vCenterCourt.z, fTimeToPrediction))
				CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[eSelfID].playerAI, TAF_JUST_TASKED)
				SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eSelfID].playerAI, ASE_AI_MOVING_TO_BALL)
			// Wait for the server to launch their ball and then launch ours
			ELIF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_PREPARED) 
			AND IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_GRADE_FLAG)
			AND eSelfID <> GET_TENNIS_MP_WHO_IS_SERVING(serverBD)
				TENNIS_SERVE_VALIDATION eServeValid
				eServeValid = VALIDATE_TENNIS_MP_SERVE_LAUNCH(thisProps, eSelfID, eOtherID)
				IF eServeValid <> TENNIS_SERVE_INVALID
					CPRINTLN(DEBUG_TENNIS, "Turning off TMC_SERVE_PREPARED, turning on TMC_BALL_SERVED, Attempting to increase Serve Count")
					IF NOT GET_LAUNCH_DETAILS_LEFT_RIGHT_SERVE(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), GET_TENNIS_PLAYER_ANALOG_X_VALUE(thisProps.tennisPlayers[eSelfID]))
						SET_TENNIS_CLIENT_SERVE_GRADE(clientBD[info.iPlayers[eSelfID]], LD_SERVE_OOB)
					ENDIF
					
					TENNIS_MP_BROADCAST_SCTV_LAUNCH_BALL( serverBD, eSelfID, thisProps.grades[GET_TENNIS_CLIENT_SERVE_GRADE(clientBD[info.iPlayers[eSelfID]])].iForce, thisProps.grades[GET_TENNIS_CLIENT_SERVE_GRADE(clientBD[info.iPlayers[eSelfID]])].iAngle, thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)].fLeftRight, thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)].vMyForward, thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)].vMyRight )
					
					DETACH_ENTITY(thisProps.tennisBall.oBall,FALSE)
					IF eServeValid = TENNIS_SERVE_FAILSAFE
						VECTOR vSafetyPos
						vSafetyPos = GET_TENNIS_BALL_SERVE_SAFETY_POS(thisProps.tennisBall)
						CDEBUG2LN(DEBUG_TENNIS, "TENNIS_SERVE_FAILSAFE being handled")
						CDEBUG2LN(DEBUG_TENNIS, "vSafetyPos = ", vSafetyPos)
						SET_TENNIS_BALL_POS(thisProps.tennisBall, vSafetyPos)
						SET_ENTITY_COORDS(thisProps.tennisBall.oBall, vSafetyPos, FALSE)
					ENDIF
					LAUNCH_BALL(thisProps, thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)], thisProps.grades[GET_TENNIS_CLIENT_SERVE_GRADE(clientBD[info.iPlayers[eSelfID]])])
					IF GET_TENNIS_CLIENT_SERVE_GRADE(clientBD[info.iPlayers[eSelfID]]) = LD_SERVE_S OR GET_TENNIS_CLIENT_SERVE_GRADE(clientBD[info.iPlayers[eSelfID]]) = LD_SERVE_S_PLUS
						PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SMASH_MASTER", GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
					ELSE
						PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SERVE_MASTER", GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
					ENDIF
					
					CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[eSelfID], TPB_REDUCE_PREDICTION_CALC)
					SET_TENNIS_MP_HIT_LAST(serverBD, eOtherID)
					SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_BALL_SERVED)
					SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_WATCH_THE_COURT)
					SET_TENNIS_MP_SERVE_COUNT(serverBD, GET_TENNIS_MP_SERVE_COUNT(serverBD)+1)
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_PREPARED)
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_GRADE_FLAG)
					
					//Get my prediction here and set the new X-Marker
					VECTOR vDestTest
					vDestTest = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(thisProps.tennisCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall), thisProps.tennisBall.vBallVel, 
						thisProps.vForward, GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), GET_TENNIS_BALL_SPIN(thisProps.tennisBall), GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
					SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[eSelfID], vDestTest)
					
					vDestTest = GET_CLOSEST_POINT_FOR_TENNIS_SWING(thisProps.tennisPlayers[eSelfID].vPos, thisProps.tennisCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall), 
						thisProps.tennisBall.vBallVel, thisProps.vForward, GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), GET_TENNIS_BALL_SPIN(thisProps.tennisBall), 
						GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall), fTimeToPrediction)
					SET_AI_PREDICTION_POINT(thisProps.tennisPlayers[eSelfID].playerAI, GET_BEST_TENNIS_PREDICTION_FROM_OFFSET(thisProps.tennisPlayers[eSelfID], 
						thisProps.vSwingStateOffsets, vDestTest, thisProps.tennisCourt.vCenterCourt.z, fTimeToPrediction))
					CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[eSelfID].playerAI, TAF_JUST_TASKED)
					SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eSelfID].playerAI, ASE_AI_MOVING_TO_BALL)
					
					// Display the X marker
					IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_X_MARKER)
						thisUIData.fMessageTimer = 0
						thisProps.tennisBall.iBounceCount = 0
						SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_X_MARKER)
						CPRINTLN(DEBUG_TENNIS, "Showing where the ball will land and resetting gating variables")
					ENDIF
				ENDIF
				
			// Do our normal updates here like looking for bounces only if the ball isn't in someone's hand
			ELIF NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SERVE_PREPARED)
				//Has the ball bounced?
				IF HAS_BALL_BOUNCED(thisProps.tennisBall, GET_TENNIS_FRAME_COUNTER(thisProps), thisProps.tennisCourt.vCenterCourt)
					INCREASE_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
					IF IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_NO_BOUNCE_TIMEOUT_OCCURED)
						CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_NO_BOUNCE_TIMEOUT_OCCURED)
						SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
						SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, eSelfID)
						TENNIS_MP_BROADCAST_BALL_DESYNC()
					ENDIF
					//Was it just served
					IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_BALL_SERVED)
						CPRINTLN(DEBUG_TENNIS, "The ball has been served, checking if it bounces against the ground")
						//Is it in bounds of the serve?
						//Only check this if I am the server.
						IF NOT IS_BALL_INSIDE_SERVICE_AREA(thisProps.tennisCourt, thisProps, thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)].eCourtSide, GET_TENNIS_MP_SERVING_SIDE(serverBD))
						AND (GET_TENNIS_MP_WHO_IS_SERVING(serverBD)) = eOtherID
							//Fault!
							CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY: Fault detected")
							SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_SERVER_FAULTED)
							SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_FAULT)
							CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ACE)
							
							INCREASE_TENNIS_MP_STAT(TMST_TENNIS_TOTAL_FAULTS)
							
							IF GET_TENNIS_MP_SERVE_COUNT(serverBD) >= 2
								//Double Fault
								CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY: Double Fault")
								SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_MP_WHO_IS_SERVING(serverBD))))	//Could use eOtherID in this case.
								SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
								TENNIS_MP_BROADCAST_FAULT(2)
							ELIF GET_TENNIS_MP_SERVE_COUNT(serverBD) >= 1
								//First Fault
								CPRINTLN(DEBUG_TENNIS, "TGSE_RALLY: First Fault")
								SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_INTERSTITIAL, TMPS_WAIT_FOR_INTERSTITIAL)
								SET_TENNIS_TIMER_A(thisProps, 0)
								TENNIS_MP_BROADCAST_FAULT(1)
								SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_FIRST_FAULT)
							ENDIF
						ELSE
							//The ball was in bounds, clear the ball served bitmask
							CPRINTLN(DEBUG_TENNIS, "Clearing TMC_BALL_SERVED bit")
							CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_BALL_SERVED)
						ENDIF
					//I rely on the receiver to tell whether the ball is in or out, the hope is that this accounts a bit for lag
					ELIF (GET_TENNIS_MP_HIT_LAST(serverBD)) <> eSelfID
					AND IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_WATCH_THE_COURT)
						IF GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall) >= 2
							//A point has been won
							CPRINTLN(DEBUG_TENNIS, "Bounce Count is above 2, moving to point won")
							TENNIS_PLAYER_ID ePointWinner
							ePointWinner = eOtherID // Generally we're watching the ball because the other player hit it last
							IF NOT TENNIS_IS_BALL_ON_COURT_SIDE(thisProps.tennisPlayers[eSelfID].eCourtSide, thisProps)
								ePointWinner = eSelfID // If there are two bounces and the ball isn't on our side then most likely the opponent hit the net and it bounced back. We should have caught that before but it's being handled here.
							ENDIF
							CDEBUG2LN(DEBUG_TENNIS, "ePointWinner = ", GET_STRING_FROM_TENNIS_PLAYER_ID(ePointWinner))
							SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, ePointWinner)
							SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
							TENNIS_MP_BROADCAST_POINT_WON(ePointWinner)
						ELIF GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall) >= 1 AND NOT IS_BALL_INSIDE_COURT(GET_TENNIS_MP_HIT_LAST(serverBD), thisProps, default, TRUE)
							//A point has been won
							CPRINTLN(DEBUG_TENNIS, "Bounce Count is >= 1 and ball is not inside the court, moving to point won")
							SET_TENNIS_MP_SCORED_LAST(thisProps, serverBD, eSelfID)
							SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_POINT_WON, TMPS_WAIT_FOR_POINT_WON)
							IF NOT IS_BALL_INSIDE_ENTIRE_COURT(thisProps)
								TENNIS_MP_BROADCAST_OOB(eSelfID)
							ELSE
								TENNIS_MP_BROADCAST_POINT_WON(eSelfID)
							ENDIF
						ELSE
							//Ball only bounced, keep playing
							CDEBUG3LN( DEBUG_TENNIS, "TGSE_RALLY data on normal bounce" )
							CDEBUG3LN( DEBUG_TENNIS, "eSelfID                      = ", GET_STRING_FROM_TENNIS_PLAYER_ID( eSelfID ) )
							CDEBUG3LN( DEBUG_TENNIS, "GET_TENNIS_MP_HIT_LAST       = ", GET_STRING_FROM_TENNIS_PLAYER_ID( GET_TENNIS_MP_HIT_LAST( serverBD ) ) )
							CDEBUG3LN( DEBUG_TENNIS, "GET_TENNIS_BALL_BOUNCE_COUNT = ", GET_TENNIS_BALL_BOUNCE_COUNT( thisProps.tennisBall ) )
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Move the state on forcefully if we experienced a desync
			//This has the added effect of moving to Point Won when a fault is detected by one machine and not the other.
			IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_RECOVERING_FROM_DESYNC)
				SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_POINT_WON, TRUE)
			ENDIF
		BREAK
		
		CASE TGSE_POINT_WON
			CPRINTLN(DEBUG_TENNIS, "TGSE_POINT_WON: Registering a Point Won")
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, FALSE, bGamePaused)
//			CLEAR_TENNIS_PLAYER_CONTROL_BITS(thisProps.tennisPlayers[eSelfID])
			// Get us out of the quit UI if we're not in TENNIS_UI_PLAYING
			IF GET_TENNIS_UI_STATE(thisUIData) <> TENNIS_UI_PLAYING
				SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_PLAYING)
				SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_RETURNING_FROM_QUIT)
			ENDIF
			
			IF (GET_TENNIS_MP_SCORED_LAST(serverBD)) = eSelfID
				SET_TENNIS_ANIM_REACTION_FLAGS(thisUIData.iUtilFlags, TD_PLR_HAPPY)
				INCREASE_TENNIS_MP_STAT(TMST_TENNIS_TOTAL_POINTS)
			ELSE
				SET_TENNIS_ANIM_REACTION_FLAGS(thisUIData.iUtilFlags, TD_PLR_SAD)
			ENDIF
			
			// Register points and game won for the server
			TENNIS_MP_INCREASE_POINTS_WON(serverBD.playerScores[GET_TENNIS_MP_SCORED_LAST(serverBD)], thisUIData.iUtilFlags)
			IF serverBD.playerScores[GET_TENNIS_MP_SCORED_LAST(serverBD)].iPointsWon >= PICK_INT(IS_TENNIS_MP_ON_TIE_BREAK(serverBD), 7, 4)
			AND serverBD.playerScores[GET_TENNIS_MP_SCORED_LAST(serverBD)].iPointsWon - serverBD.playerScores[1 - ENUM_TO_INT(GET_TENNIS_MP_SCORED_LAST(serverBD))].iPointsWon >= 2
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					RECORD_TENNIS_GAME_WIN(serverBD.playerScores, GET_TENNIS_MP_CURRENT_SET(serverBD), (GET_TENNIS_MP_SCORED_LAST(serverBD)), INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_MP_SCORED_LAST(serverBD))), thisUIData.iUtilFlags, IS_TENNIS_MP_ON_TIE_BREAK(serverBD))
				ENDIF
			ENDIF
			
			// Update the scores
			SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_UPDATE_SCORES)
			// Move the state on
			SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_POINT_CHECKS, TMPS_WAIT_FOR_POINT_CHECKS)
		BREAK
		
		CASE TGSE_POINT_CHECKS
			CPRINTLN(DEBUG_TENNIS, "TGSE_POINT_CHECKS: Running Point Checks")
			INT iPointToCheck, iCurrentSet
			TENNIS_PLAYER_ID eScoredLast, eScoredOn
			eScoredLast = GET_TENNIS_MP_SCORED_LAST(serverBD)
			eScoredOn = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eScoredLast))
			iCurrentSet = GET_TENNIS_MP_CURRENT_SET(serverBD)
			iPointToCheck = PICK_INT(IS_TENNIS_MP_ON_TIE_BREAK(serverBD), 7, 4)
			
			CPRINTLN(DEBUG_TENNIS, "Game Timer: ", GET_GAME_TIMER(), " Scores from server below:")
			CPRINTLN(DEBUG_TENNIS, "eSelfID points won: ", serverBD.playerScores[eSelfID].iPointsWon)
			CPRINTLN(DEBUG_TENNIS, "eOtherID points won: ", serverBD.playerScores[eOtherID].iPointsWon)
			CPRINTLN(DEBUG_TENNIS, "eSelfID games won: ", serverBD.playerScores[eSelfID].iGamesWon[GET_TENNIS_MP_CURRENT_SET(serverBD)])
			CPRINTLN(DEBUG_TENNIS, "eOtherID games won: ", serverBD.playerScores[eOtherID].iGamesWon[GET_TENNIS_MP_CURRENT_SET(serverBD)])
			
			// Game won?
			IF serverBD.playerScores[GET_TENNIS_MP_SCORED_LAST(serverBD)].iPointsWon >= iPointToCheck
			AND serverBD.playerScores[GET_TENNIS_MP_SCORED_LAST(serverBD)].iPointsWon - serverBD.playerScores[1 - ENUM_TO_INT(GET_TENNIS_MP_SCORED_LAST(serverBD))].iPointsWon >= 2
				SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_CHANGE_SERVERS, TMPS_WAIT_FOR_CHANGE_SERVERS)
				SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_GAME_DONE_STISH)
				SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_UPDATE_SCORES)
				SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_GAME_WAS_WON)
				
				IF (GET_TENNIS_MP_SCORED_LAST(serverBD)) = eSelfID
					INCREASE_TENNIS_MP_STAT(TMST_TENNIS_GAMES_WON)
				ELSE
					INCREASE_TENNIS_MP_STAT(TMST_TENNIS_GAMES_LOST)
				ENDIF
				
				//Move the state on forcefully if we experienced a desync
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_RECOVERING_FROM_DESYNC)
					SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_CHANGE_SERVERS, TRUE)
				ENDIF
				
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT() AND IS_TENNIS_MP_ON_TIE_BREAK(serverBD)
					SET_TENNIS_MP_TIE_BREAK(serverBD, FALSE)
				ENDIF
				
				// Reset cachedMPScores
				thisProps.cachedMPScores[eSelfID].iPointsWon = 0
				thisProps.cachedMPScores[eOtherID].iPointsWon = 0
				thisProps.cachedMPScores[eSelfID].iGamesWon[GET_TENNIS_MP_CURRENT_SET(serverBD)] = serverBD.playerScores[eSelfID].iGamesWon[GET_TENNIS_MP_CURRENT_SET(serverBD)]
				thisProps.cachedMPScores[eOtherID].iGamesWon[GET_TENNIS_MP_CURRENT_SET(serverBD)] = serverBD.playerScores[eOtherID].iGamesWon[GET_TENNIS_MP_CURRENT_SET(serverBD)]
				
				CPRINTLN(DEBUG_TENNIS, "Game was won")
				
			// Keep playing
			ELSE
				// cache off MP scores
				thisProps.cachedMPScores[eSelfID].iPointsWon = serverBD.playerScores[eSelfID].iPointsWon
				thisProps.cachedMPScores[eOtherID].iPointsWon = serverBD.playerScores[eOtherID].iPointsWon
				
				// begin score checks IG
				iPointToCheck = PICK_INT(IS_TENNIS_MP_ON_TIE_BREAK(serverBD), 6, 3)
				
				IF serverBD.playerScores[eScoredLast].iPointsWon >= iPointToCheck 
				AND serverBD.playerScores[eScoredLast].iPointsWon = serverBD.playerScores[eScoredOn].iPointsWon
					SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_DEUCE)
					CPRINTLN(DEBUG_TENNIS, "TD_DEUCE UI chosen")
				ELIF serverBD.playerScores[eScoredLast].iPointsWon > iPointToCheck 
				AND serverBD.playerScores[eScoredLast].iPointsWon = serverBD.playerScores[eScoredOn].iPointsWon + 1
					IF (GET_TENNIS_MP_RULESET(serverBD) = TMR_SETS
					AND serverBD.playerScores[eScoredLast].iGamesWon[iCurrentSet] >= 5 
					AND (serverBD.playerScores[eScoredLast].iGamesWon[iCurrentSet] + 1) - serverBD.playerScores[eScoredOn].iGamesWon[iCurrentSet] >= 2)
					OR (GET_TENNIS_MP_RULESET(serverBD) = TMR_ONE_GAME)
					OR (GET_TENNIS_MP_RULESET(serverBD) = TMR_THREE_GAMES
					AND serverBD.playerScores[eScoredLast].iGamesWon[iCurrentSet] >= 1)
					OR (GET_TENNIS_MP_RULESET(serverBD) = TMR_FIVE_GAMES
					AND serverBD.playerScores[eScoredLast].iGamesWon[iCurrentSet] >= 2)
						// Game Point Message
						SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_GAME_POINT)
						CPRINTLN(DEBUG_TENNIS, "TD_GAME_POINT UI chosen over TD_ADVANTAGE")
					ELSE
						SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ADVANTAGE)
						CPRINTLN(DEBUG_TENNIS, "TD_ADVANTAGE UI chosen")
					ENDIF
				ELIF serverBD.playerScores[GET_TENNIS_MP_SCORED_LAST(serverBD)].iPointsWon = iPointToCheck AND serverBD.playerScores[1 - ENUM_TO_INT(GET_TENNIS_MP_SCORED_LAST(serverBD))].iPointsWon < 3
					SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_GAME_POINT)
					IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_OOB)
						SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_OOB)
						CPRINTLN(DEBUG_TENNIS, "TD_OOB UI chosen along with TD_GAME_POINT")
					ENDIF
					CPRINTLN(DEBUG_TENNIS, "TD_GAME_POINT UI chosen")
				ELSE
					IF NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SERVER_FAULTED)
						IF (GET_TENNIS_MP_SCORED_LAST(serverBD)) = eSelfID
							SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_POINT_WON)
							CPRINTLN(DEBUG_TENNIS, "TD_POINT_WON UI chosen")
						ELSE
							IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_OOB)
								SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_OOB)
								CPRINTLN(DEBUG_TENNIS, "TD_OOB UI chosen")
							ELSE
								SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_POINT_LOST)
								CPRINTLN(DEBUG_TENNIS, "TD_POINT_LOST UI chosen")
							ENDIF
						ENDIF
					//There's no ELSE because fault UI is taken care of in a different area
					ENDIF
				ENDIF
				SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_INTERSTITIAL, TMPS_WAIT_FOR_INTERSTITIAL)
				SET_TENNIS_TIMER_A(thisProps, 0)
				
				//Move the state on forcefully if we experienced a desync
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_RECOVERING_FROM_DESYNC)
					SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_INTERSTITIAL, TRUE)
				ENDIF
				
				IF IS_TENNIS_MP_ON_TIE_BREAK(serverBD)
					IF IS_BIT_SET(serverBD.playerScores[eSelfID].iPointsWon + serverBD.playerScores[eOtherID].iPointsWon, 0)	// If the number of points is odd then we switch servers
						SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_CHANGE_SERVERS, TMPS_WAIT_FOR_CHANGE_SERVERS)
						CPRINTLN(DEBUG_TENNIS, "Changing servers, composite score is odd")
					ELIF (serverBD.playerScores[eSelfID].iPointsWon + serverBD.playerScores[eOtherID].iPointsWon) % 6 = 0		// If a multiple of six points was won then we switch court sides
						SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_INTERSTITIAL, TMPS_WAIT_FOR_INTERSTITIAL)
						SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_TB_CHANGE_SIDES)
						CPRINTLN(DEBUG_TENNIS, "Changing sides, 6 points have been won since last change")
					ENDIF
				ENDIF
				
				//have the server switch the serving side.
				SET_TENNIS_MP_SERVING_SIDE(serverBD, INT_TO_ENUM(SERVING_SIDE_ENUM, 1 - ENUM_TO_INT(GET_TENNIS_MP_SERVING_SIDE(serverBD))))
			ENDIF
		BREAK
		
		CASE TGSE_CHANGE_SERVERS
			CDEBUG2LN(DEBUG_TENNIS, "CASE TGSE_CHANGE_SERVERS :: GET_TENNIS_MP_CURRENT_SET(serverBD)=", GET_TENNIS_MP_CURRENT_SET(serverBD), ", GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)=", GET_TENNIS_MP_PROPS_CACHED_SET(thisProps))
			
			SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SERVE_MESSAGE)
			
			IF NOT IS_TENNIS_MP_ON_TIE_BREAK(serverBD)
				RESET_TENNIS_DEUCE_COUNT(thisUIData)
			
				TENNIS_MP_RESET_POINTS_WON(serverBD.playerScores, thisUIData.iUtilFlags)
				
				SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_GAME_WON)
				
				// Was a set won?
				IF (serverBD.playerScores[eSelfID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] > 5 
				AND (serverBD.playerScores[eSelfID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)]) - serverBD.playerScores[eOtherID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] >= 2)
				OR (serverBD.playerScores[eOtherID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] > 5
				AND (serverBD.playerScores[eOtherID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)]) - serverBD.playerScores[eSelfID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] >= 2)
				OR (serverBD.playerScores[eSelfID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] >= 7)
				OR (serverBD.playerScores[eOtherID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] >= 7)
				//That was one big IF statement
					IF GET_TENNIS_MP_SCORED_LAST(serverBD) = eSelfID
						// Increase sets won
						INCREASE_TENNIS_MP_STAT(TMST_TENNIS_SETS_WON)
					ELSE
						INCREASE_TENNIS_MP_STAT(TMST_TENNIS_SETS_LOST)
					ENDIF
					//If there is more than 1 set then we need to zero out the scores to keep the scoreboard from looking weird.
					IF GET_TENNIS_MP_NUM_SETS(serverBD) > 1
						SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_INCREASE_SET)
						CPRINTLN(DEBUG_TENNIS, "TMC_INCREASE_SET Flag set")
					ENDIF
					SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SET_WON)
					
				ELIF (serverBD.playerScores[eSelfID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] = 6)
				AND (serverBD.playerScores[eOtherID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] = 6)
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						SET_TENNIS_MP_TIE_BREAK(serverBD, TRUE)
					ENDIF
				
					SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_SHOW_TIE_BREAK)
					CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_GAME_WON)
					CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SET_WON)
					CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_MATCH_WON)
					CPRINTLN(DEBUG_TENNIS, "Entering Tie Break scenario")
				ENDIF

				// Do we have a Match Winner?
				IF (GET_TENNIS_MP_RULESET(serverBD) = TMR_SETS 			AND serverBD.playerScores[eSelfID].iSetsWon > GET_TENNIS_MP_NUM_SETS(serverBD)/2)
				OR (GET_TENNIS_MP_RULESET(serverBD) = TMR_SETS 			AND serverBD.playerScores[eOtherID].iSetsWon > GET_TENNIS_MP_NUM_SETS(serverBD)/2)
				OR (GET_TENNIS_MP_RULESET(serverBD) = TMR_ONE_GAME 		AND serverBD.playerScores[eSelfID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] >= 1)
				OR (GET_TENNIS_MP_RULESET(serverBD) = TMR_ONE_GAME 		AND serverBD.playerScores[eOtherID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] >= 1)
				OR (GET_TENNIS_MP_RULESET(serverBD) = TMR_THREE_GAMES	AND serverBD.playerScores[eSelfID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] >= 2)
				OR (GET_TENNIS_MP_RULESET(serverBD) = TMR_THREE_GAMES 	AND serverBD.playerScores[eOtherID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] >= 2)
				OR (GET_TENNIS_MP_RULESET(serverBD) = TMR_FIVE_GAMES 	AND serverBD.playerScores[eSelfID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] >= 3)
				OR (GET_TENNIS_MP_RULESET(serverBD) = TMR_FIVE_GAMES 	AND serverBD.playerScores[eOtherID].iGamesWon[GET_TENNIS_MP_PROPS_CACHED_SET(thisProps)] >= 3)
				//IF statement over, kinda large. A moderate IF statement.
					SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_INTERSTITIAL, TMPS_WAIT_FOR_INTERSTITIAL)
					SET_TENNIS_TIMER_A(thisProps, 0)
					SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_MATCH_IS_OVER)
					
					IF GET_TENNIS_MP_RULESET(serverBD) = TMR_SETS
					AND GET_TENNIS_MP_NUM_SETS(serverBD) >= 3
						TENNIS_INCREMENT_STAT(PS_STRENGTH, TENNIS_WIN_STRENGTH_INC)
					ENDIF
					
					thisUIData.fMessageTimer = 0
					CPRINTLN(DEBUG_TENNIS, "Game Over, Yeah!")
					CPRINTLN(DEBUG_TENNIS, "Ruleset=", GET_STRING_FROM_TENNIS_RULESET(GET_TENNIS_MP_RULESET(serverBD)))
					CPRINTLN(DEBUG_TENNIS, "[eSelfID].iSetsWon=", serverBD.playerScores[eSelfID].iSetsWon)
					CPRINTLN(DEBUG_TENNIS, "[eOtherID].iSetsWon=", serverBD.playerScores[eOtherID].iSetsWon)
					CPRINTLN(DEBUG_TENNIS, "[eSelfID].iGamesWon[", GET_TENNIS_MP_CURRENT_SET(serverBD), "]=", serverBD.playerScores[eSelfID].iGamesWon[GET_TENNIS_MP_CURRENT_SET(serverBD)])
					CPRINTLN(DEBUG_TENNIS, "[eOtherID].iGamesWon[", GET_TENNIS_MP_CURRENT_SET(serverBD), "]=", serverBD.playerScores[eOtherID].iGamesWon[GET_TENNIS_MP_CURRENT_SET(serverBD)])
					
					INT iXPToGive
					CPRINTLN(DEBUG_TENNIS, "XP Award Calculation (tunable=", g_sMPTunables.fxp_tunable_Minigames_Tennis_Taking_Part, "):")
					iXPToGive = ROUND(TENNIS_XP_PARTICIPATION * g_sMPTunables.fxp_tunable_Minigames_Tennis_Taking_Part)		// for participation
					CPRINTLN(DEBUG_TENNIS, "iXPToGive=", iXPToGive)
					IF serverBD.playerScores[eSelfID].iGamesWon[0] <> -1	iXPToGive += ROUND(TENNIS_XP_GAME_WON * serverBD.playerScores[eSelfID].iGamesWon[0] * g_sMPTunables.fxp_tunable_Minigames_Tennis_Game) ENDIF
					CPRINTLN(DEBUG_TENNIS, "iXPToGive=", iXPToGive)
					IF serverBD.playerScores[eSelfID].iGamesWon[1] <> -1	iXPToGive += ROUND(TENNIS_XP_GAME_WON * serverBD.playerScores[eSelfID].iGamesWon[1] * g_sMPTunables.fxp_tunable_Minigames_Tennis_Game) ENDIF
					CPRINTLN(DEBUG_TENNIS, "iXPToGive=", iXPToGive)
					IF serverBD.playerScores[eSelfID].iGamesWon[2] <> -1	iXPToGive += ROUND(TENNIS_XP_GAME_WON * serverBD.playerScores[eSelfID].iGamesWon[2] * g_sMPTunables.fxp_tunable_Minigames_Tennis_Game) ENDIF
					CPRINTLN(DEBUG_TENNIS, "iXPToGive=", iXPToGive)
					IF serverBD.playerScores[eSelfID].iGamesWon[3] <> -1	iXPToGive += ROUND(TENNIS_XP_GAME_WON * serverBD.playerScores[eSelfID].iGamesWon[3] * g_sMPTunables.fxp_tunable_Minigames_Tennis_Game) ENDIF
					CPRINTLN(DEBUG_TENNIS, "iXPToGive=", iXPToGive)
					IF serverBD.playerScores[eSelfID].iGamesWon[4] <> -1	iXPToGive += ROUND(TENNIS_XP_GAME_WON * serverBD.playerScores[eSelfID].iGamesWon[4] * g_sMPTunables.fxp_tunable_Minigames_Tennis_Game) ENDIF
					CPRINTLN(DEBUG_TENNIS, "iXPToGive=", iXPToGive)
					iXPToGive += ROUND(TENNIS_XP_SET_WON * serverBD.playerScores[eSelfID].iSetsWon * g_sMPTunables.fxp_tunable_Minigames_Tennis_Set)
					CPRINTLN(DEBUG_TENNIS, "iXPToGive=", iXPToGive)
					
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PLAY_TENNIS)
					
					IF (GET_TENNIS_MP_RULESET(serverBD) = TMR_SETS AND serverBD.playerScores[eSelfID].iSetsWon > (GET_TENNIS_MP_NUM_SETS(serverBD)/2))
					OR (GET_TENNIS_MP_RULESET(serverBD) <> TMR_SETS AND serverBD.playerScores[eSelfID].iGamesWon[0] > serverBD.playerScores[eOtherID].iGamesWon[0])
						HANDLE_TENNIS_MP_INTERSTITIALS(thisProps, thisUIData, clientBD[info.iPlayers[eSelfID]], eSelfID, TRUE, info)
						iXPToGive += ROUND(TENNIS_XP_MATCH_WON * g_sMPTunables.fxp_tunable_Minigames_Tennis_Win)
						
						SET_TENNIS_UI_BETS_DATA(thisUIData.tennisUI, BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID()))
						SET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[info.iPlayers[eSelfID]], AR_playerWon)
						// increment our winning stats
						INCREASE_TENNIS_MP_STAT(TMST_TENNIS_MATCHES_WON)
						SET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_WON, GET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_WON) + 1)
						INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_TENNIS_WON, 1)
						
						CPRINTLN(DEBUG_TENNIS, "MP_AWARD_FM_TENNIS_WON Character Award Increased.")
						IF GET_TENNIS_MP_RULESET(serverBD) = TMR_SETS AND GET_TENNIS_MP_NUM_SETS(serverBD) = 5
							SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FM_TENNIS_5_SET_WINS, TRUE)
							CPRINTLN(DEBUG_TENNIS, "MP_AWARD_FM_TENNIS_5_SET_WINS Character Award Increased.")
						ENDIF
						IF GET_TENNIS_MP_RULESET(serverBD) = TMR_SETS AND serverBD.playerScores[eOtherID].iSetsWon = 0 AND GET_TENNIS_MP_NUM_SETS(serverBD) > 1
							SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FM_TENNIS_STASETWIN, TRUE)
							CPRINTLN(DEBUG_TENNIS, "MP_AWARD_FM_TENNIS_STASETWIN (Straight Sets) Character Award Increased.")
						ENDIF
						
					ELIF (GET_TENNIS_MP_RULESET(serverBD) = TMR_SETS AND serverBD.playerScores[eOtherID].iSetsWon > (GET_TENNIS_MP_NUM_SETS(serverBD)/2))
					OR (GET_TENNIS_MP_RULESET(serverBD) <> TMR_SETS AND serverBD.playerScores[eOtherID].iGamesWon[0] > serverBD.playerScores[eSelfID].iGamesWon[0])
						HANDLE_TENNIS_MP_INTERSTITIALS(thisProps, thisUIData, clientBD[info.iPlayers[eSelfID]], eSelfID, FALSE, info)
						SET_TENNIS_UI_BETS_DATA(thisUIData.tennisUI, BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iPlayers[eOtherID]))))
						SET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[info.iPlayers[eSelfID]], AR_buddyA_won)
						// increment our losing stats
						INCREASE_TENNIS_MP_STAT(TMST_TENNIS_MATCHES_LOST)
						SET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_LOST, GET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_LOST) + 1)
					ELSE
						CPRINTLN(DEBUG_TENNIS, "Match is over but we have no winner :(")
						CPRINTLN(DEBUG_TENNIS, "Ruleset: ", GET_STRING_FROM_TENNIS_RULESET(GET_TENNIS_MP_RULESET(serverBD)))
						CPRINTLN(DEBUG_TENNIS, "eSelfID Sets", serverBD.playerScores[eSelfID].iSetsWon)
						CPRINTLN(DEBUG_TENNIS, "eOtherID Sets", serverBD.playerScores[eOtherID].iSetsWon)
						CPRINTLN(DEBUG_TENNIS, "eSelfID Games", serverBD.playerScores[eSelfID].iGamesWon[0])
						CPRINTLN(DEBUG_TENNIS, "eOtherID GAMES", serverBD.playerScores[eOtherID].iGamesWon[0])
					ENDIF
					TRIGGER_CELEBRATION_PRE_LOAD(thisUIData.sCelebData)
					SET_TENNIS_UI_XP_TO_GIVE(thisUIData.tennisUI, iXPToGive)
					SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_MATCH_WON)
					HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
					SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_TENNIS)
					thisUIData.eGenericMGStage = GENERIC_CELEBRATION_WINNER
					//CHECK_FRANKIE_SAYS_ACHIEVEMENT()
				ELSE
					//swap servers
					IF GET_TENNIS_MP_WHO_IS_SERVING(serverBD) = TENNIS_PLAYER_AWAY
						SET_TENNIS_MP_WHO_IS_SERVING(serverBD, TENNIS_PLAYER_HOME)
					ELIF GET_TENNIS_MP_WHO_IS_SERVING(serverBD) = TENNIS_PLAYER_HOME
						SET_TENNIS_MP_WHO_IS_SERVING(serverBD, TENNIS_PLAYER_AWAY)
					ELSE
						CPRINTLN(DEBUG_TENNIS, "Serving player is neither home nor away, something went wrong.")
					ENDIF
					
					IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_GAME_WAS_WON) AND TENNIS_SHOULD_SIDES_CHANGE(serverBD.playerScores, GET_TENNIS_MP_CURRENT_SET(serverBD)) AND NOT IS_TENNIS_MP_ON_TIE_BREAK(serverBD)
						SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_SIDE_CHANGE, TMPS_WAIT_FOR_SIDE_CHANGE)
						CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_GAME_WAS_WON)
						CPRINTLN(DEBUG_TENNIS, "Changing sides of the court")
					ELSE
						SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_INTERSTITIAL, TMPS_WAIT_FOR_INTERSTITIAL)
					ENDIF
					SET_TENNIS_TIMER_A(thisProps, 0)
					SET_TENNIS_MP_SERVING_SIDE(serverBD, SERVING_FROM_RIGHT)
					CPRINTLN(DEBUG_TENNIS, "Points reset, Server changed.")
				ENDIF
			ELIF IS_TENNIS_MP_ON_TIE_BREAK(serverBD)
				//swap servers
				CPRINTLN(DEBUG_TENNIS, "TGSE_CHANGE_SERVERS: Tennis is still on Tie Breaker")
				IF (GET_TENNIS_MP_WHO_IS_SERVING(serverBD)) = eSelfID
					SET_TENNIS_MP_WHO_IS_SERVING(serverBD, eOtherID)
				ELIF (GET_TENNIS_MP_WHO_IS_SERVING(serverBD)) = eOtherID
					SET_TENNIS_MP_WHO_IS_SERVING(serverBD, eSelfID)
				ENDIF
				SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_INTERSTITIAL, TMPS_WAIT_FOR_INTERSTITIAL)
				SET_TENNIS_MP_SERVING_SIDE(serverBD, SERVING_FROM_RIGHT)
			ENDIF
		BREAK
		
		CASE TGSE_INTERSTITIAL
			TASK_TENNIS_PLAYER_WATCH_BALL(thisProps.tennisPlayers[eSelfID], thisProps.tennisBall, FALSE)
			ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, FALSE)
			// Allow awards to show
			RESET_TENNIS_AWARD_VIEW_TIME(thisUIData.tennisUI)
			
			IF GET_TENNIS_UI_STATE(thisUIData) <> TENNIS_UI_PLAYING
				DISPLAY_TENNIS_SPLASH_UI(thisUIData.tennisUI, TRUE)
				SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_PLAYING)
				SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_RETURNING_FROM_QUIT)
			ENDIF
			
			IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_GAME_WAS_WON)				//Check for a side change here
				CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_GAME_WAS_WON)
				
				IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_MATCH_IS_OVER)
					thisUIData.iCurrentOption = 1	// Priming the rematch menu
					SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_OUTRO, TMPS_WAIT_FOR_OUTRO)
					BUILD_TENNIS_REMATCH_MENU(TRUE)
					HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
//					CREATE_TENNIS_BETTING_SCOREBOARD(thisUIData.tennisUI, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt), GET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[info.iPlayers[eSelfID]]) = AR_playerWon)
					SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_INCREASE_SET)
					CPRINTLN(DEBUG_TENNIS, "Match is over, going to outro")
				ELSE
					SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_PREPARE_SERVE, TMPS_WAIT_FOR_PREPARE_SERVE)
					SHOW_TENNIS_SCOREBOARD(thisUIData.tennisUI)
					IF NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_NEW_RALLY)
						SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_NEW_RALLY)
						SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_WALKING_TO_POS)
						IF eSelfID = (GET_TENNIS_MP_WHO_IS_SERVING(serverBD))
							SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, default, default, FALSE, TRUE)
						ELSE
							SET_TENNIS_PLAYER_RECEIVE_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, default, TRUE)
							ATTACH_TENNIS_BALL_TO_PLAYER(thisProps.tennisPlayers[eOtherID], thisProps.tennisBall)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_TB_CHANGE_SIDES)
					SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_SIDE_CHANGE, TMPS_WAIT_FOR_SIDE_CHANGE)
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_TB_CHANGE_SIDES)
					CPRINTLN(DEBUG_TENNIS, "Changing sides of the court during tie breaks")
				ELSE
					SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_PREPARE_SERVE, TMPS_WAIT_FOR_PREPARE_SERVE)
				ENDIF
			ENDIF
			
			CLEAR_TENNIS_HELP_MESSAGES()
			SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_UPDATE_SCORES)
			UPDATE_TENNIS_UI_POINTS(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), 
				eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), info, IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_MATCH_IS_OVER))	//, TRUE)
			TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_POINTS(TRUE)
			SET_TENNIS_TIMER_A(thisProps, 0)
		BREAK
		
		CASE TGSE_SIDE_CHANGE
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SIDE_CHANGING)
				SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SIDE_CHANGING)
				SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_UPDATE_SCORES)
				UPDATE_TENNIS_UI_POINTS(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), 
					eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), info, IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_MATCH_IS_OVER), TRUE)
				TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_POINTS(TRUE)
				HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
				TENNIS_ACTIVATE_SIDE_CHANGE_PART_1(thisProps)
				SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_SIDE_CHANGE, FALSE)	//We only get to this state from a state where the camera was stored. i.e. an interstitial always precedes the side change
				TENNIS_MP_BROADCAST_SCTV_UI_SET_CAMERA_STATE( TENNIS_CAMERA_SIDE_CHANGE, FALSE )
				TENNIS_ACTIVATE_SIDE_CHANGE_CAMERA_SP(thisProps.tennisCourt, thisUIData.tennisCameras, thisProps.tennisPlayers[eSelfID].eCourtSide)
				
				// Set cloned peds to walk, hide our peds, attach rackets to hands
				SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_MAKE_PLAYERS_INVISIBLE)
				SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( info.iPlayers[eSelfID] ) ) )
				SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( info.iPlayers[eOtherID] ) ) )
				TENNIS_SIDE_CHANGE_CLIENT_CLONE(thisProps, clientBD[info.iPlayers[eSelfID]], eSelfID)
				TENNIS_SIDE_CHANGE_CLIENT_CLONE(thisProps, clientBD[info.iPlayers[eSelfID]], eOtherID)
				IF NOT IS_PED_INJURED(clientBD[info.iPlayers[eSelfID]].pedClones[eSelfID])
					ATTACH_ENTITY_TO_ENTITY(thisProps.tennisPlayers[eSelfID].oTennisRacket, clientBD[info.iPlayers[eSelfID]].pedClones[eSelfID], 
						GET_PED_BONE_INDEX(clientBD[info.iPlayers[eSelfID]].pedClones[eSelfID], BONETAG_PH_R_HAND), (<<0,0,0>>), (<<0,0,0>>))	//, RACKET_OFFSET, RACKET_ROTATION)
				ENDIF
				IF NOT IS_PED_INJURED(clientBD[info.iPlayers[eSelfID]].pedClones[eOtherID])
					ATTACH_ENTITY_TO_ENTITY(thisProps.tennisPlayers[eOtherID].oTennisRacket, clientBD[info.iPlayers[eSelfID]].pedClones[eOtherID], 
						GET_PED_BONE_INDEX(clientBD[info.iPlayers[eSelfID]].pedClones[eOtherID], BONETAG_PH_R_HAND), (<<0,0,0>>), (<<0,0,0>>))	//, RACKET_OFFSET, RACKET_ROTATION)
				ENDIF
				
				VECTOR vBallWillBe
				TENNIS_PLAYER_ID eServer, eCourtSide
				SERVING_SIDE_ENUM eServingSide
				eServer = GET_TENNIS_MP_WHO_IS_SERVING(serverBD)
				eCourtSide = thisProps.tennisPlayers[eServer].eCourtSide
				eServingSide = GET_TENNIS_MP_SERVING_SIDE(serverBD)
				IF eCourtSide = TENNIS_PLAYER_AWAY
					vBallWillBe = PICK_VECTOR(eServingSide = SERVING_FROM_RIGHT, thisProps.tennisCourt.vCourtCorners[1], thisProps.tennisCourt.vCourtCorners[0])
				ELSE
					vBallWillBe = PICK_VECTOR(eServingSide = SERVING_FROM_RIGHT, thisProps.tennisCourt.vCourtCorners[3], thisProps.tennisCourt.vCourtCorners[2])
				ENDIF
				// Prime the dynamic camera after a side change, MP
				ADJUST_TENNIS_CAMERA_DYNAMIC(thisUIData.tennisCameras.tennisCamDyna, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt),
					PICK_VECTOR(thisProps.tennisPlayers[eSelfID].eCourtSide = (TENNIS_PLAYER_AWAY), thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisCourt.vCourtCorners[2]),
					thisProps.tennisPlayers[eSelfID].vMyRight, thisProps.tennisPlayers[eSelfID].vMyForward, vBallWillBe - thisProps.tennisCourt.vCenterCourt,
					thisUIData.tennisCameras.fDynaCamSwing, DEFAULT_DYNA_DIST, thisUIData.tennisCameras.fDynaDolly, thisUIData.tennisCameras.fDynaNudgeRSLR, thisUIData.tennisCameras.fDynaNudgeRSUD, FALSE, TRUE)
											
				ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, FALSE)
				
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					IF IS_TENNIS_SERVER_FLAG_SET( serverBD, TMS_SIDES_ARE_CHANGED )
						CLEAR_TENNIS_SERVER_FLAG( serverBD, TMS_SIDES_ARE_CHANGED )
					ELSE
						SET_TENNIS_SERVER_FLAG( serverBD, TMS_SIDES_ARE_CHANGED )
					ENDIF
				ENDIF
				
			ENDIF
			
			IF IS_TIMER_STARTED(thisUIData.tennisCameras.tennisCamTimer)
			AND GET_TIMER_IN_SECONDS(thisUIData.tennisCameras.tennisCamTimer) * 1000 > 2330
			AND NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_GAME_HAS_SHOWN_SIDE_CHANGE_MSG)
				SHOW_TENNIS_SIDE_CHANGE_MESSAGE(thisUIData.tennisUI)
				TENNIS_MP_BROADCAST_SCTV_UI_SHOW_SIDE_CHANGE()
				SET_TENNIS_FLOW_FLAG(thisProps, TFF_GAME_HAS_SHOWN_SIDE_CHANGE_MSG)
				CPRINTLN(DEBUG_TENNIS, "SHOW_TENNIS_SIDE_CHANGE_MESSAGE called in CASE TGSE_SIDE_CHANGE")
			ENDIF
			
			IF IS_TIMER_STARTED(thisUIData.tennisCameras.tennisCamTimer)
			AND GET_TIMER_IN_SECONDS(thisUIData.tennisCameras.tennisCamTimer) * 1000 > SIDE_CHANGE_LENGTH
				SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_PREPARE_SERVE, TMPS_WAIT_FOR_PREPARE_SERVE)
				SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, thisUIData.tennisCameras.eChosenCam, FALSE)
				SHOW_TENNIS_SCOREBOARD(thisUIData.tennisUI)
				SET_TENNIS_TIMER_A(thisProps, 0)
				TENNIS_MP_BROADCAST_SCTV_UI_SET_CAMERA_STATE( thisUIData.tennisCameras.eChosenCam, FALSE )
				CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_MAKE_PLAYERS_INVISIBLE)
			ENDIF
			
			IF IS_TIMER_STARTED(thisUIData.tennisCameras.tennisCamTimer)
			AND GET_TIMER_IN_SECONDS(thisUIData.tennisCameras.tennisCamTimer) * 1000 > 1000
			AND NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_SC_WALK_STARTED)
				SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_SC_WALK_STARTED)
				TENNIS_ACTIVATE_SIDE_CHANGE_PART_2(thisProps, thisUIData.tennisCameras)
				SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), thisProps.tennisCourt.vCenterCourt - (thisProps.tennisPlayers[eSelfID].vMyForward * 8.5), FALSE)
				SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_WALKING_TO_POS)
				IF eSelfID = (GET_TENNIS_MP_WHO_IS_SERVING(serverBD))
					SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, default, default, default, TRUE, default, TRUE)
				ELSE
					SET_TENNIS_PLAYER_RECEIVE_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, default, TRUE, default, default, TRUE)
					ATTACH_TENNIS_BALL_TO_PLAYER(thisProps.tennisPlayers[eOtherID], thisProps.tennisBall)
				ENDIF
			ENDIF
		BREAK
		
		CASE TGSE_OUTRO
			CDEBUG3LN(DEBUG_TENNIS, "CASE TGSE_OUTRO")
			INT iMyParticipantID
			iMyParticipantID = PARTICIPANT_ID_TO_INT()
			ALLOW_TENNIS_PLAYER_CONTROL(TA_MULTIPLAYER, FALSE)
			PROCESS_TENNIS_MP_RESULTS_SCOREBOARD(thisUIData)
			JOB_WIN_STATUS eJobStatus
			PLAYER_INDEX winningPlayer
			IF GET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[iMyParticipantID]) = AR_playerWon
				eJobStatus = JOB_STATUS_WIN
				winningPlayer = PLAYER_ID()
			ELSE
				eJobStatus = JOB_STATUS_LOSE
				winningPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(TENNIS_GET_OTHER_PARTICIPANT(iMyParticipantID, info)))
			ENDIF
			IF GET_GAME_TIMER() > GET_TENNIS_CLIENT_OUTRO_STAMP(clientBD[iMyParticipantID])
				IF NOT HAS_TENNIS_LEADERBOARD_CAM_BEEN_REQUESTED()
					REQUEST_TENNIS_LEADERBOARD_CAM()
				ENDIF
			ENDIF
			IF HAS_GENERIC_CELEBRATION_FINISHED(thisUIData.sCelebData, thisUIData.tennisCameras.tennisCamDyna, "XPT_TENNIS", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_A_TENNIS_GAME, GET_TENNIS_UI_XP_TO_GIVE(thisUIData.tennisUI), 
			eJobStatus, g_iMyBetWinnings, thisUIData.eGenericMGStage, winningPlayer, TRUE)
				SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
				SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SHOW_QUIT_LEADERBOARD)
				SET_TENNIS_FLOW_FLAG(thisProps, TFF_CONTINUE_AFTER_VOTE_PASSING)
			#IF IS_DEBUG_BUILD
			ELSE
				CDEBUG3LN(DEBUG_TENNIS, "HAS_GENERIC_CELEBRATION_FINISHED=FALSE, eJobStatus=", PICK_STRING(eJobStatus=JOB_STATUS_WIN, "JOB_STATUS_WIN", "JOB_STATUS_LOSE"))
			#ENDIF
			ENDIF
		BREAK
		
		CASE TGSE_PREPARE_REMATCH
			// Ensure we keep lockstepping the game
			SET_TENNIS_CLIENT_GAME_AND_MP_STATE(clientBD[info.iPlayers[eSelfID]], TGSE_PREPARE_SERVE, TMPS_WAIT_FOR_PREPARE_SERVE)
			
			IF NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[info.iPlayers[eSelfID]], TMC_WALKING_TO_POS) AND HAS_SCALEFORM_MOVIE_LOADED(thisUIData.tennisUI.scorePanelSF)
				INIT_ALL_TENNIS_UI(thisUIData.tennisUI)
				SET_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_WALKING_TO_POS)
				IF eSelfID = (GET_TENNIS_MP_WHO_IS_SERVING(serverBD))
					SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, default, default, FALSE, TRUE)
				ELSE
					SET_TENNIS_PLAYER_RECEIVE_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, default, TRUE)
					ATTACH_TENNIS_BALL_TO_PLAYER(thisProps.tennisPlayers[eOtherID], thisProps.tennisBall)
				ENDIF
			ENDIF
		BREAK
		
		CASE TGSE_FINISH
			HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
			IF IS_TENNIS_LEADERBOARD_CAM_READY()
//				IF HAS_TENNIS_SCLB_FINISHED(thisUIData.tennisUI.leaderboardUI, thisUIData.tennisUI)
					CPRINTLN(DEBUG_TENNIS, "TGSE_FINISH: Game over, yeeeeaaahhh!")
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					WRITE_TENNIS_MP_SCLB_DATA()
					SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
					TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, GET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[PARTICIPANT_ID_TO_INT()]), serverBD.iMatchHistoryID, missionVariation)
//				ENDIF
			ENDIF
		BREAK
		#IF IS_DEBUG_BUILD
		CASE TGSE_TEST_STATE
			CLIENT_SERVER_DEBUG_DISPLAY()
			SHOW_TENNIS_STATE_MESSAGE()
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, GET_TENNIS_MP_WHO_IS_SERVING(serverBD) = eSelfID, IS_PAUSE_MENU_ACTIVE())
			DRAW_DEBUG_SPHERE(thisProps.tennisCourt.vCenterCourt + (thisProps.tennisPlayers[eSelfID].vMyRight * SIDE_CHANGE_MID_DEST_SCALAR), 0.2)
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RUP)
				TENNIS_GAME_STATUS thisStatus
				TEST_HIT(thisProps.tennisPlayers[eSelfID], thisProps, thisStatus, eSelfID)
				CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eSelfID]], TMC_WATCH_THE_COURT)
				TENNIS_MP_BROADCAST_UPDATE_BALL(GET_TENNIS_BALL_POS(thisProps.tennisBall), thisProps.tennisBall.vBallVel, GET_TENNIS_BALL_SPIN(thisProps.tennisBall), eSelfID, clientBD, TBU_NET_RICOCHET, SHOT_LOB, info)
			ENDIF
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
				TASK_GO_STRAIGHT_TO_COORD(thisProps.tennisPlayers[eSelfID].pIndex, thisProps.tennisCourt.vCenterCourt, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
				CDEBUG2LN(DEBUG_TENNIS, "INPUT_SCRIPT_PAD_UP calling TASK_GO_STRAIGHT_TO_COORD")
			ENDIF
		BREAK
		#ENDIF
	ENDSWITCH
	
//	IF NOT IS_TENNIS_SPLASH_UI_HIDDEN(thisUIData.tennisUI)
//		IF GET_TENNIS_UI_STATE(thisUIData) <> TENNIS_UI_PLAYING
//			SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_PLAYING, FALSE)
//		ENDIF
//	ENDIF

	IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING) 
	AND GET_TENNIS_SERVER_GAME_STATE(serverBD) > TGSE_OPTION_MENU
	AND GET_TENNIS_SERVER_GAME_STATE(serverBD) < TGSE_QUIT_CONFIRM
//	AND GET_TENNIS_SERVER_GAME_STATE(serverBD) <> TGSE_INTERSTITIAL
		HANDLE_TENNIS_ANIMS(thisProps.tennisPlayers[eSelfID], thisProps.phasesArray, TA_MULTIPLAYER)
	ENDIF	
	
	COMMON_TENNIS_END_OF_FRAME_DATA(thisProps)
ENDPROC

/// PURPOSE:
///    used to update the game rules, since they don't get set by SCTV
/// PARAMS:
///    thisUIData - 
///    serverBD - 
PROC REFRESH_SCTV_GAME_RULES( TENNIS_GAME_UI_DATA &thisUIData, TENNIS_SERVER_BD &serverBD )

	SWITCH serverBD.eRuleset
		CASE TMR_ONE_GAME
			thisUIData.iRulesSelection = 0
		BREAK
		CASE TMR_THREE_GAMES
			thisUIData.iRulesSelection = 1
		BREAK
		CASE TMR_FIVE_GAMES
			thisUIData.iRulesSelection = 2
		BREAK
		CASE TMR_SETS
			SWITCH serverBD.iNumSets
				CASE 1
					thisUIData.iRulesSelection = 3
				BREAK
				CASE 3
					thisUIData.iRulesSelection = 4
				BREAK
				CASE 5
					thisUIData.iRulesSelection = 5
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

ENDPROC

