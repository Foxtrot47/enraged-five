USING "minigames_helpers.sch"
USING "minigame_UIInputs.sch"
USING "friendActivity_public.sch"
//USING "family_scene_private.sch"

USING "tennis.sch"
USING "tennis_player.sch"
USING "tennis_player_lib.sch"
USING "tennis_court.sch"
USING "tennis_court_lib.sch"
USING "tennis_ball_lib.sch"
USING "tennis_cameras.sch"
USING "tennis_cameras_lib.sch"
USING "fmmc_mp_setup_mission.sch"
USING "socialclub_leaderboard.sch"
USING "stats_private.sch"
USING "tennis_leaderboard_lib.sch"
USING "family_scene_private.sch"
USING "cheat_controller_public.sch"



/// ************************************************************* PROCEDURES *************************************************************
//#IF IS_DEBUG_BUILD
//  PROC INIT_OFFSETS(TENNIS_GAME_PROPERTIES &thisProps)
//      thisProps.xRacketOff = thisProps.RacketOffX
//      thisProps.yRacketOff = thisProps.RacketOffY
//      thisProps.zRacketOff = thisProps.RacketOffZ
//      thisProps.xRacketRot = thisProps.RacketRotX
//      thisProps.yRacketRot = thisProps.RacketRotY
//      thisProps.zRacketRot = thisProps.RacketRotZ
//  ENDPROC
//#ENDIF

PROC INITIALIZE_TENNIS_SWING_STATE_PHASES(TENNIS_GAME_PROPERTIES &thisProps)
	CPRINTLN(DEBUG_TENNIS, "INITIALIZE_TENNIS_SWING_STATE_PHASES called")
	thisProps.phasesArray[TSS_BACKHAND_BS_HI].fInit 		= 0.0
	thisProps.phasesArray[TSS_BACKHAND_BS_HI].fActive 		= 0.167
	thisProps.phasesArray[TSS_BACKHAND_BS_HI].fEnd 			= 0.633
	thisProps.phasesArray[TSS_BACKHAND_BS_HI].iFrameTot		= 48
	thisProps.phasesArray[TSS_BACKHAND_BS_HI].sAnimName		= "backhand_bs_hi"
	
	thisProps.phasesArray[TSS_BACKHAND_BS_LOW].fInit 		= 0.0
	thisProps.phasesArray[TSS_BACKHAND_BS_LOW].fActive 		= 0.143
	thisProps.phasesArray[TSS_BACKHAND_BS_LOW].fEnd 		= 0.633
	thisProps.phasesArray[TSS_BACKHAND_BS_LOW].iFrameTot	= 56
	thisProps.phasesArray[TSS_BACKHAND_BS_LOW].sAnimName	= "backhand_bs_lo"
	
	thisProps.phasesArray[TSS_BACKHAND_BS_MID].fInit 		= 0.0
	thisProps.phasesArray[TSS_BACKHAND_BS_MID].fActive 		= 0.150
	thisProps.phasesArray[TSS_BACKHAND_BS_MID].fEnd 		= 0.633
	thisProps.phasesArray[TSS_BACKHAND_BS_MID].iFrameTot	= 46
	thisProps.phasesArray[TSS_BACKHAND_BS_MID].sAnimName	= "backhand_bs_md"
	
	thisProps.phasesArray[TSS_BACKHAND_TS_HI].fInit 		= 0.0
	thisProps.phasesArray[TSS_BACKHAND_TS_HI].fActive 		= 0.140
	thisProps.phasesArray[TSS_BACKHAND_TS_HI].fEnd 			= 0.530
	thisProps.phasesArray[TSS_BACKHAND_TS_HI].iFrameTot		= 58
	thisProps.phasesArray[TSS_BACKHAND_TS_HI].sAnimName		= "backhand_ts_hi"
	
	thisProps.phasesArray[TSS_BACKHAND_TS_LOW].fInit 		= 0.0
	thisProps.phasesArray[TSS_BACKHAND_TS_LOW].fActive 		= 0.150
	thisProps.phasesArray[TSS_BACKHAND_TS_LOW].fEnd 		= 0.535
	thisProps.phasesArray[TSS_BACKHAND_TS_LOW].iFrameTot	= 54
	thisProps.phasesArray[TSS_BACKHAND_TS_LOW].sAnimName	= "backhand_ts_lo"
	
	thisProps.phasesArray[TSS_BACKHAND_TS_MID].fInit 		= 0.0
	thisProps.phasesArray[TSS_BACKHAND_TS_MID].fActive	 	= 0.170
	thisProps.phasesArray[TSS_BACKHAND_TS_MID].fEnd 		= 0.665
	thisProps.phasesArray[TSS_BACKHAND_TS_MID].iFrameTot	= 46
	thisProps.phasesArray[TSS_BACKHAND_TS_MID].sAnimName	= "backhand_ts_md"
	
	thisProps.phasesArray[TSS_BACKHAND_BS_HI_SKIP].fInit 		= 0.125
	thisProps.phasesArray[TSS_BACKHAND_BS_HI_SKIP].fActive 		= 0.167
	thisProps.phasesArray[TSS_BACKHAND_BS_HI_SKIP].fEnd 		= 0.633
	thisProps.phasesArray[TSS_BACKHAND_BS_HI_SKIP].iFrameTot	= 48
	thisProps.phasesArray[TSS_BACKHAND_BS_HI_SKIP].sAnimName	= "backhand_bs_hi"
	
	thisProps.phasesArray[TSS_BACKHAND_BS_LOW_SKIP].fInit 		= 0.089
	thisProps.phasesArray[TSS_BACKHAND_BS_LOW_SKIP].fActive 	= 0.143
	thisProps.phasesArray[TSS_BACKHAND_BS_LOW_SKIP].fEnd 		= 0.633
	thisProps.phasesArray[TSS_BACKHAND_BS_LOW_SKIP].iFrameTot	= 56
	thisProps.phasesArray[TSS_BACKHAND_BS_LOW_SKIP].sAnimName	= "backhand_bs_lo"
	
	thisProps.phasesArray[TSS_BACKHAND_BS_MID_SKIP].fInit 		= 0.108
	thisProps.phasesArray[TSS_BACKHAND_BS_MID_SKIP].fActive 	= 0.150
	thisProps.phasesArray[TSS_BACKHAND_BS_MID_SKIP].fEnd 		= 0.633
	thisProps.phasesArray[TSS_BACKHAND_BS_MID_SKIP].iFrameTot	= 46
	thisProps.phasesArray[TSS_BACKHAND_BS_MID_SKIP].sAnimName	= "backhand_bs_md"
	
	thisProps.phasesArray[TSS_BACKHAND_TS_HI_SKIP].fInit 		= 0.086
	thisProps.phasesArray[TSS_BACKHAND_TS_HI_SKIP].fActive 		= 0.120
	thisProps.phasesArray[TSS_BACKHAND_TS_HI_SKIP].fEnd 		= 0.530
	thisProps.phasesArray[TSS_BACKHAND_TS_HI_SKIP].iFrameTot	= 58
	thisProps.phasesArray[TSS_BACKHAND_TS_HI_SKIP].sAnimName	= "backhand_ts_hi"
	
	thisProps.phasesArray[TSS_BACKHAND_TS_LOW_SKIP].fInit 		= 0.0925
	thisProps.phasesArray[TSS_BACKHAND_TS_LOW_SKIP].fActive 	= 0.150
	thisProps.phasesArray[TSS_BACKHAND_TS_LOW_SKIP].fEnd 		= 0.535
	thisProps.phasesArray[TSS_BACKHAND_TS_LOW_SKIP].iFrameTot	= 54
	thisProps.phasesArray[TSS_BACKHAND_TS_LOW_SKIP].sAnimName	= "backhand_ts_lo"
	
	thisProps.phasesArray[TSS_BACKHAND_TS_MID_SKIP].fInit 		= 0.108
	thisProps.phasesArray[TSS_BACKHAND_TS_MID_SKIP].fActive	 	= 0.160
	thisProps.phasesArray[TSS_BACKHAND_TS_MID_SKIP].fEnd 		= 0.665
	thisProps.phasesArray[TSS_BACKHAND_TS_MID_SKIP].iFrameTot	= 46
	thisProps.phasesArray[TSS_BACKHAND_TS_MID_SKIP].sAnimName	= "backhand_ts_md"
	
	thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW].fActive 		= 0.300
	thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW].fEnd 		= 0.580
	thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW].iFrameTot	= 46
	thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW].sAnimName	= "close_bh_bs_lo"
	
	thisProps.phasesArray[TSS_CLOSE_BH_BS_MID].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_BH_BS_MID].fActive 		= 0.300
	thisProps.phasesArray[TSS_CLOSE_BH_BS_MID].fEnd 		= 0.585
	thisProps.phasesArray[TSS_CLOSE_BH_BS_MID].iFrameTot	= 46
	thisProps.phasesArray[TSS_CLOSE_BH_BS_MID].sAnimName	= "close_bh_bs_md"
	
	thisProps.phasesArray[TSS_CLOSE_BH_BS_HI].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_BH_BS_HI].fActive 		= 0.290
	thisProps.phasesArray[TSS_CLOSE_BH_BS_HI].fEnd 			= 0.630
	thisProps.phasesArray[TSS_CLOSE_BH_BS_HI].iFrameTot		= 48
	thisProps.phasesArray[TSS_CLOSE_BH_BS_HI].sAnimName		= "close_bh_bs_hi"
	
	thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW].fActive 		= 0.280
	thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW].fEnd 		= 0.708
	thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW].iFrameTot	= 50
	thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW].sAnimName	= "close_bh_ts_lo"
	
	thisProps.phasesArray[TSS_CLOSE_BH_TS_MID].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_BH_TS_MID].fActive 		= 0.260
	thisProps.phasesArray[TSS_CLOSE_BH_TS_MID].fEnd 		= 0.710
	thisProps.phasesArray[TSS_CLOSE_BH_TS_MID].iFrameTot	= 54
	thisProps.phasesArray[TSS_CLOSE_BH_TS_MID].sAnimName	= "close_bh_ts_md"
	
	thisProps.phasesArray[TSS_CLOSE_BH_TS_HI].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_BH_TS_HI].fActive 		= 0.260
	thisProps.phasesArray[TSS_CLOSE_BH_TS_HI].fEnd 			= 0.643
	thisProps.phasesArray[TSS_CLOSE_BH_TS_HI].iFrameTot		= 54
	thisProps.phasesArray[TSS_CLOSE_BH_TS_HI].sAnimName		= "close_bh_ts_hi"
	
	thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW_SKIP].fInit 		= 0.174
	thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW_SKIP].fActive 	= thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW].fActive 
	thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW].fEnd 	
	thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_BH_BS_LOW_SKIP].sAnimName	= "close_bh_bs_lo"
	
	thisProps.phasesArray[TSS_CLOSE_BH_BS_MID_SKIP].fInit 		= 0.217
	thisProps.phasesArray[TSS_CLOSE_BH_BS_MID_SKIP].fActive 	= thisProps.phasesArray[TSS_CLOSE_BH_BS_MID].fActive 
	thisProps.phasesArray[TSS_CLOSE_BH_BS_MID_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_BH_BS_MID].fEnd 	
	thisProps.phasesArray[TSS_CLOSE_BH_BS_MID_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_BH_BS_MID].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_BH_BS_MID_SKIP].sAnimName	= "close_bh_bs_md"
	
	thisProps.phasesArray[TSS_CLOSE_BH_BS_HI_SKIP].fInit 		= 0.208
	thisProps.phasesArray[TSS_CLOSE_BH_BS_HI_SKIP].fActive 		= thisProps.phasesArray[TSS_CLOSE_BH_BS_HI].fActive 
	thisProps.phasesArray[TSS_CLOSE_BH_BS_HI_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_BH_BS_HI].fEnd 		
	thisProps.phasesArray[TSS_CLOSE_BH_BS_HI_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_BH_BS_HI].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_BH_BS_HI_SKIP].sAnimName	= "close_bh_bs_hi"
	
	thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW_SKIP].fInit 		= 0.120
	thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW_SKIP].fActive 	= thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW].fActive 
	thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW].fEnd 	
	thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_BH_TS_LOW_SKIP].sAnimName	= "close_bh_ts_lo"
	
	thisProps.phasesArray[TSS_CLOSE_BH_TS_MID_SKIP].fInit 		= 0.148
	thisProps.phasesArray[TSS_CLOSE_BH_TS_MID_SKIP].fActive 	= thisProps.phasesArray[TSS_CLOSE_BH_TS_MID].fActive 
	thisProps.phasesArray[TSS_CLOSE_BH_TS_MID_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_BH_TS_MID].fEnd 	
	thisProps.phasesArray[TSS_CLOSE_BH_TS_MID_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_BH_TS_MID].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_BH_TS_MID_SKIP].sAnimName	= "close_bh_ts_md"
	
	thisProps.phasesArray[TSS_CLOSE_BH_TS_HI_SKIP].fInit 		= 0.090
	thisProps.phasesArray[TSS_CLOSE_BH_TS_HI_SKIP].fActive 		= thisProps.phasesArray[TSS_CLOSE_BH_TS_HI].fActive 	
	thisProps.phasesArray[TSS_CLOSE_BH_TS_HI_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_BH_TS_HI].fEnd 	
	thisProps.phasesArray[TSS_CLOSE_BH_TS_HI_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_BH_TS_HI].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_BH_TS_HI_SKIP].sAnimName	= "close_bh_ts_hi"
	
	thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW].fActive 		= 0.270
	thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW].fEnd 		= 0.613
	thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW].iFrameTot	= 56
	thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW].sAnimName	= "close_fh_bs_lo"
	
	thisProps.phasesArray[TSS_CLOSE_FH_BS_MID].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_FH_BS_MID].fActive 		= 0.270
	thisProps.phasesArray[TSS_CLOSE_FH_BS_MID].fEnd 		= 0.533
	thisProps.phasesArray[TSS_CLOSE_FH_BS_MID].iFrameTot	= 57
	thisProps.phasesArray[TSS_CLOSE_FH_BS_MID].sAnimName	= "close_fh_bs_md"
	
	thisProps.phasesArray[TSS_CLOSE_FH_BS_HI].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_FH_BS_HI].fActive 		= 0.280
	thisProps.phasesArray[TSS_CLOSE_FH_BS_HI].fEnd 			= 0.690
	thisProps.phasesArray[TSS_CLOSE_FH_BS_HI].iFrameTot		= 53
	thisProps.phasesArray[TSS_CLOSE_FH_BS_HI].sAnimName		= "close_fh_bs_hi"
	
	thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW].fActive 		= 0.260
	thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW].fEnd 		= 0.665
	thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW].iFrameTot	= 57
	thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW].sAnimName	= "close_fh_ts_lo"
	
	thisProps.phasesArray[TSS_CLOSE_FH_TS_MID].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_FH_TS_MID].fActive 		= 0.280
	thisProps.phasesArray[TSS_CLOSE_FH_TS_MID].fEnd 		= 0.782
	thisProps.phasesArray[TSS_CLOSE_FH_TS_MID].iFrameTot	= 55
	thisProps.phasesArray[TSS_CLOSE_FH_TS_MID].sAnimName	= "close_fh_ts_md"
	
	thisProps.phasesArray[TSS_CLOSE_FH_TS_HI].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_FH_TS_HI].fActive 		= 0.240
	thisProps.phasesArray[TSS_CLOSE_FH_TS_HI].fEnd 			= 0.698
	thisProps.phasesArray[TSS_CLOSE_FH_TS_HI].iFrameTot		= 62
	thisProps.phasesArray[TSS_CLOSE_FH_TS_HI].sAnimName		= "close_fh_ts_hi"
	
	thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW_SKIP].fInit 		= 0.135
	thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW_SKIP].fActive 	= thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW].fActive 
	thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW].fEnd 	
	thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_FH_BS_LOW_SKIP].sAnimName	= "close_fh_bs_lo"
	
	thisProps.phasesArray[TSS_CLOSE_FH_BS_MID_SKIP].fInit 		= 0.173
	thisProps.phasesArray[TSS_CLOSE_FH_BS_MID_SKIP].fActive 	= thisProps.phasesArray[TSS_CLOSE_FH_BS_MID].fActive 
	thisProps.phasesArray[TSS_CLOSE_FH_BS_MID_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_FH_BS_MID].fEnd 	
	thisProps.phasesArray[TSS_CLOSE_FH_BS_MID_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_FH_BS_MID].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_FH_BS_MID_SKIP].sAnimName	= "close_fh_bs_md"
	
	thisProps.phasesArray[TSS_CLOSE_FH_BS_HI_SKIP].fInit 		= 0.120
	thisProps.phasesArray[TSS_CLOSE_FH_BS_HI_SKIP].fActive 		= thisProps.phasesArray[TSS_CLOSE_FH_BS_HI].fActive 	
	thisProps.phasesArray[TSS_CLOSE_FH_BS_HI_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_FH_BS_HI].fEnd 	
	thisProps.phasesArray[TSS_CLOSE_FH_BS_HI_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_FH_BS_HI].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_FH_BS_HI_SKIP].sAnimName	= "close_fh_bs_hi"
	
	thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW_SKIP].fInit 		= 0.148
	thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW_SKIP].fActive 	= thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW].fActive 
	thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW].fEnd 	
	thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_FH_TS_LOW_SKIP].sAnimName	= "close_fh_ts_lo"
	
	thisProps.phasesArray[TSS_CLOSE_FH_TS_MID_SKIP].fInit 		= 0.160
	thisProps.phasesArray[TSS_CLOSE_FH_TS_MID_SKIP].fActive 	= thisProps.phasesArray[TSS_CLOSE_FH_TS_MID].fActive 
	thisProps.phasesArray[TSS_CLOSE_FH_TS_MID_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_FH_TS_MID].fEnd 	
	thisProps.phasesArray[TSS_CLOSE_FH_TS_MID_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_FH_TS_MID].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_FH_TS_MID_SKIP].sAnimName	= "close_fh_ts_md"
	
	thisProps.phasesArray[TSS_CLOSE_FH_TS_HI_SKIP].fInit 		= 0.155
	thisProps.phasesArray[TSS_CLOSE_FH_TS_HI_SKIP].fActive 		= thisProps.phasesArray[TSS_CLOSE_FH_TS_HI].fActive 	
	thisProps.phasesArray[TSS_CLOSE_FH_TS_HI_SKIP].fEnd 		= thisProps.phasesArray[TSS_CLOSE_FH_TS_HI].fEnd 	
	thisProps.phasesArray[TSS_CLOSE_FH_TS_HI_SKIP].iFrameTot	= thisProps.phasesArray[TSS_CLOSE_FH_TS_HI].iFrameTot
	thisProps.phasesArray[TSS_CLOSE_FH_TS_HI_SKIP].sAnimName	= "close_fh_ts_hi"
	
	thisProps.phasesArray[TSS_FOREHAND_BS_HI].fInit 		= 0.0
	thisProps.phasesArray[TSS_FOREHAND_BS_HI].fActive 		= 0.210
	thisProps.phasesArray[TSS_FOREHAND_BS_HI].fEnd 			= 0.700
	thisProps.phasesArray[TSS_FOREHAND_BS_HI].iFrameTot		= 43
	thisProps.phasesArray[TSS_FOREHAND_BS_HI].sAnimName		= "forehand_bs_hi"
	
	thisProps.phasesArray[TSS_FOREHAND_BS_LOW].fInit 		= 0.0
	thisProps.phasesArray[TSS_FOREHAND_BS_LOW].fActive 		= 0.160
	thisProps.phasesArray[TSS_FOREHAND_BS_LOW].fEnd 		= 0.670
	thisProps.phasesArray[TSS_FOREHAND_BS_LOW].iFrameTot	= 46
	thisProps.phasesArray[TSS_FOREHAND_BS_LOW].sAnimName	= "forehand_bs_lo"
	
	thisProps.phasesArray[TSS_FOREHAND_BS_MID].fInit 		= 0.0
	thisProps.phasesArray[TSS_FOREHAND_BS_MID].fActive 		= 0.180
	thisProps.phasesArray[TSS_FOREHAND_BS_MID].fEnd 		= 0.600
	thisProps.phasesArray[TSS_FOREHAND_BS_MID].iFrameTot	= 46
	thisProps.phasesArray[TSS_FOREHAND_BS_MID].sAnimName	= "forehand_bs_md"
	
	thisProps.phasesArray[TSS_FOREHAND_BS_HI_SKIP].fInit 		= 0.136
	thisProps.phasesArray[TSS_FOREHAND_BS_HI_SKIP].fActive 		= 0.210
	thisProps.phasesArray[TSS_FOREHAND_BS_HI_SKIP].fEnd 		= 0.700
	thisProps.phasesArray[TSS_FOREHAND_BS_HI_SKIP].iFrameTot	= 43
	thisProps.phasesArray[TSS_FOREHAND_BS_HI_SKIP].sAnimName	= "forehand_bs_hi"
	
	thisProps.phasesArray[TSS_FOREHAND_BS_LOW_SKIP].fInit 		= 0.108
	thisProps.phasesArray[TSS_FOREHAND_BS_LOW_SKIP].fActive 	= 0.160
	thisProps.phasesArray[TSS_FOREHAND_BS_LOW_SKIP].fEnd 		= 0.670
	thisProps.phasesArray[TSS_FOREHAND_BS_LOW_SKIP].iFrameTot	= 46
	thisProps.phasesArray[TSS_FOREHAND_BS_LOW_SKIP].sAnimName	= "forehand_bs_lo"
	
	thisProps.phasesArray[TSS_FOREHAND_BS_MID_SKIP].fInit 		= 0.108
	thisProps.phasesArray[TSS_FOREHAND_BS_MID_SKIP].fActive 	= 0.180
	thisProps.phasesArray[TSS_FOREHAND_BS_MID_SKIP].fEnd 		= 0.600
	thisProps.phasesArray[TSS_FOREHAND_BS_MID_SKIP].iFrameTot	= 46
	thisProps.phasesArray[TSS_FOREHAND_BS_MID_SKIP].sAnimName	= "forehand_bs_md"
	
	thisProps.phasesArray[TSS_FOREHAND_SMASH].fInit 		= 0.0
	thisProps.phasesArray[TSS_FOREHAND_SMASH].fActive 		= 0.170
	thisProps.phasesArray[TSS_FOREHAND_SMASH].fEnd 			= 0.722
	thisProps.phasesArray[TSS_FOREHAND_SMASH].iFrameTot		= 46
	thisProps.phasesArray[TSS_FOREHAND_SMASH].sAnimName		= "forehand_smash"
	
	thisProps.phasesArray[TSS_FOREHAND_SMASH_LEFT].fInit 		= 0.0
	thisProps.phasesArray[TSS_FOREHAND_SMASH_LEFT].fActive 		= 0.170
	thisProps.phasesArray[TSS_FOREHAND_SMASH_LEFT].fEnd 		= 0.614
	thisProps.phasesArray[TSS_FOREHAND_SMASH_LEFT].iFrameTot	= 46
	thisProps.phasesArray[TSS_FOREHAND_SMASH_LEFT].sAnimName	= "forehand_smash_lt"
	
	thisProps.phasesArray[TSS_FOREHAND_SMASH_RIGHT].fInit 		= 0.0
	thisProps.phasesArray[TSS_FOREHAND_SMASH_RIGHT].fActive 	= 0.170
	thisProps.phasesArray[TSS_FOREHAND_SMASH_RIGHT].fEnd 		= 0.700
	thisProps.phasesArray[TSS_FOREHAND_SMASH_RIGHT].iFrameTot	= 46
	thisProps.phasesArray[TSS_FOREHAND_SMASH_RIGHT].sAnimName	= "forehand_smash_rt"
	
	thisProps.phasesArray[TSS_FOREHAND_SMASH_SKIP].fInit 		= 0.108
	thisProps.phasesArray[TSS_FOREHAND_SMASH_SKIP].fActive 		= 0.170
	thisProps.phasesArray[TSS_FOREHAND_SMASH_SKIP].fEnd 		= 0.722
	thisProps.phasesArray[TSS_FOREHAND_SMASH_SKIP].iFrameTot	= 46
	thisProps.phasesArray[TSS_FOREHAND_SMASH_SKIP].sAnimName	= "forehand_smash"
	
	thisProps.phasesArray[TSS_FOREHAND_SMASH_LEFT_SKIP].fInit 		= 0.108
	thisProps.phasesArray[TSS_FOREHAND_SMASH_LEFT_SKIP].fActive 	= 0.170
	thisProps.phasesArray[TSS_FOREHAND_SMASH_LEFT_SKIP].fEnd 		= 0.614
	thisProps.phasesArray[TSS_FOREHAND_SMASH_LEFT_SKIP].iFrameTot	= 46
	thisProps.phasesArray[TSS_FOREHAND_SMASH_LEFT_SKIP].sAnimName	= "forehand_smash_lt"
	
	thisProps.phasesArray[TSS_FOREHAND_SMASH_RIGHT_SKIP].fInit 		= 0.108
	thisProps.phasesArray[TSS_FOREHAND_SMASH_RIGHT_SKIP].fActive 	= 0.170
	thisProps.phasesArray[TSS_FOREHAND_SMASH_RIGHT_SKIP].fEnd 		= 0.700
	thisProps.phasesArray[TSS_FOREHAND_SMASH_RIGHT_SKIP].iFrameTot	= 46
	thisProps.phasesArray[TSS_FOREHAND_SMASH_RIGHT_SKIP].sAnimName	= "forehand_smash_rt"
	
	thisProps.phasesArray[TSS_FOREHAND_TS_HI].fInit 		= 0.0
	thisProps.phasesArray[TSS_FOREHAND_TS_HI].fActive 		= 0.140
	thisProps.phasesArray[TSS_FOREHAND_TS_HI].fEnd 			= 0.615
	thisProps.phasesArray[TSS_FOREHAND_TS_HI].iFrameTot		= 62
	thisProps.phasesArray[TSS_FOREHAND_TS_HI].sAnimName		= "forehand_ts_hi"
	
	thisProps.phasesArray[TSS_FOREHAND_TS_LOW].fInit 		= 0.0
	thisProps.phasesArray[TSS_FOREHAND_TS_LOW].fActive 		= 0.160
	thisProps.phasesArray[TSS_FOREHAND_TS_LOW].fEnd 		= 0.643
	thisProps.phasesArray[TSS_FOREHAND_TS_LOW].iFrameTot	= 52
	thisProps.phasesArray[TSS_FOREHAND_TS_LOW].sAnimName	= "forehand_ts_lo"
	
	thisProps.phasesArray[TSS_FOREHAND_TS_MID].fInit 		= 0.0
	thisProps.phasesArray[TSS_FOREHAND_TS_MID].fActive 		= 0.180
	thisProps.phasesArray[TSS_FOREHAND_TS_MID].fEnd 		= 0.779
	thisProps.phasesArray[TSS_FOREHAND_TS_MID].iFrameTot	= 45
	thisProps.phasesArray[TSS_FOREHAND_TS_MID].sAnimName	= "forehand_ts_md"
	
	thisProps.phasesArray[TSS_FOREHAND_TS_HI_SKIP].fInit 		= 0.080
	thisProps.phasesArray[TSS_FOREHAND_TS_HI_SKIP].fActive 		= 0.140
	thisProps.phasesArray[TSS_FOREHAND_TS_HI_SKIP].fEnd 		= 0.615
	thisProps.phasesArray[TSS_FOREHAND_TS_HI_SKIP].iFrameTot	= 62
	thisProps.phasesArray[TSS_FOREHAND_TS_HI_SKIP].sAnimName	= "forehand_ts_hi"
	
	thisProps.phasesArray[TSS_FOREHAND_TS_LOW_SKIP].fInit 		= 0.094
	thisProps.phasesArray[TSS_FOREHAND_TS_LOW_SKIP].fActive 	= 0.160
	thisProps.phasesArray[TSS_FOREHAND_TS_LOW_SKIP].fEnd 		= 0.643
	thisProps.phasesArray[TSS_FOREHAND_TS_LOW_SKIP].iFrameTot	= 52
	thisProps.phasesArray[TSS_FOREHAND_TS_LOW_SKIP].sAnimName	= "forehand_ts_lo"
	
	thisProps.phasesArray[TSS_FOREHAND_TS_MID_SKIP].fInit 		= 0.108
	thisProps.phasesArray[TSS_FOREHAND_TS_MID_SKIP].fActive 	= 0.180
	thisProps.phasesArray[TSS_FOREHAND_TS_MID_SKIP].fEnd 		= 0.779
	thisProps.phasesArray[TSS_FOREHAND_TS_MID_SKIP].iFrameTot	= 45
	thisProps.phasesArray[TSS_FOREHAND_TS_MID_SKIP].sAnimName	= "forehand_ts_md"
	
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_HI].fInit 		= 0.280
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_HI].fActive 	= 0.370
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_HI].fEnd 		= 0.708
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_HI].iFrameTot	= 57
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_HI].sAnimName	= "lunge_bh_hi"
	
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_LOW].fInit 	= 0.275
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_LOW].fActive 	= 0.370
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_LOW].fEnd 		= 0.695
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_LOW].iFrameTot	= 58
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_LOW].sAnimName	= "lunge_bh_lo"
	
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_MID].fInit 	= 0.270
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_MID].fActive 	= 0.370
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_MID].fEnd 		= 0.688
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_MID].iFrameTot	= 58
	thisProps.phasesArray[TSS_LUNGE_BACKHAND_MID].sAnimName	= "lunge_bh_mid"
	
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_HI].fInit 		= 0.260
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_HI].fActive 	= 0.410
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_HI].fEnd 		= 0.705
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_HI].iFrameTot	= 54
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_HI].sAnimName	= "lunge_fh_hi"
	
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_LOW].fInit 	= 0.260
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_LOW].fActive 	= 0.420
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_LOW].fEnd 		= 0.738
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_LOW].iFrameTot	= 52
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_LOW].sAnimName	= "lunge_fh_lo"
	
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_MID].fInit 	= 0.263
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_MID].fActive 	= 0.430
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_MID].fEnd 		= 0.695
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_MID].iFrameTot	= 50
	thisProps.phasesArray[TSS_LUNGE_FOREHAND_MID].sAnimName	= "lunge_fh_mid"
	
	thisProps.phasesArray[TSS_DIVE_BACKHAND].fInit 		= 0.073
	thisProps.phasesArray[TSS_DIVE_BACKHAND].fActive 	= 0.170
	thisProps.phasesArray[TSS_DIVE_BACKHAND].fEnd 		= 0.770
	thisProps.phasesArray[TSS_DIVE_BACKHAND].iFrameTot	= 102
	thisProps.phasesArray[TSS_DIVE_BACKHAND].sAnimName	= "dive_bh"
	
	thisProps.phasesArray[TSS_DIVE_FOREHAND].fInit 		= 0.053
	thisProps.phasesArray[TSS_DIVE_FOREHAND].fActive 	= 0.170
	thisProps.phasesArray[TSS_DIVE_FOREHAND].fEnd 		= 0.781
	thisProps.phasesArray[TSS_DIVE_FOREHAND].iFrameTot	= 102
	thisProps.phasesArray[TSS_DIVE_FOREHAND].sAnimName	= "dive_fh"
	
	thisProps.phasesArray[TSS_FOREHAND_TS_LO_FAR].fInit 	= 0.0
	thisProps.phasesArray[TSS_FOREHAND_TS_LO_FAR].fActive 	= 0.15
	thisProps.phasesArray[TSS_FOREHAND_TS_LO_FAR].fEnd 		= 0.95
	thisProps.phasesArray[TSS_FOREHAND_TS_LO_FAR].iFrameTot	= 65
	thisProps.phasesArray[TSS_FOREHAND_TS_LO_FAR].sAnimName	= "forehand_ts_lo_far"
	
	thisProps.phasesArray[TSS_FOREHAND_TS_MID_FAR].fInit 		= 0.0
	thisProps.phasesArray[TSS_FOREHAND_TS_MID_FAR].fActive 		= 0.17
	thisProps.phasesArray[TSS_FOREHAND_TS_MID_FAR].fEnd 		= 0.95
	thisProps.phasesArray[TSS_FOREHAND_TS_MID_FAR].iFrameTot	= 58
	thisProps.phasesArray[TSS_FOREHAND_TS_MID_FAR].sAnimName	= "forehand_ts_md_far"
	
	thisProps.phasesArray[TSS_FOREHAND_TS_HI_FAR].fInit 	= 0.0
	thisProps.phasesArray[TSS_FOREHAND_TS_HI_FAR].fActive 	= 0.13
	thisProps.phasesArray[TSS_FOREHAND_TS_HI_FAR].fEnd 		= 0.95
	thisProps.phasesArray[TSS_FOREHAND_TS_HI_FAR].iFrameTot	= 74
	thisProps.phasesArray[TSS_FOREHAND_TS_HI_FAR].sAnimName	= "forehand_ts_hi_far"
	
	thisProps.phasesArray[TSS_CLOSE_BH_LO].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_BH_LO].fActive 		= 0.2
	thisProps.phasesArray[TSS_CLOSE_BH_LO].fEnd 		= 0.95
	thisProps.phasesArray[TSS_CLOSE_BH_LO].iFrameTot	= 40
	thisProps.phasesArray[TSS_CLOSE_BH_LO].sAnimName	= "close_bh_lo"
	
	thisProps.phasesArray[TSS_CLOSE_BH_MID].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_BH_MID].fActive 	= 0.2
	thisProps.phasesArray[TSS_CLOSE_BH_MID].fEnd 		= 0.95
	thisProps.phasesArray[TSS_CLOSE_BH_MID].iFrameTot	= 40
	thisProps.phasesArray[TSS_CLOSE_BH_MID].sAnimName	= "close_bh_md"
	
	thisProps.phasesArray[TSS_CLOSE_BH_HI].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_BH_HI].fActive 		= 0.2
	thisProps.phasesArray[TSS_CLOSE_BH_HI].fEnd 		= 0.95
	thisProps.phasesArray[TSS_CLOSE_BH_HI].iFrameTot	= 40
	thisProps.phasesArray[TSS_CLOSE_BH_HI].sAnimName	= "close_bh_hi"
	
	thisProps.phasesArray[TSS_CLOSE_FH_LO].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_FH_LO].fActive 		= 0.174
	thisProps.phasesArray[TSS_CLOSE_FH_LO].fEnd 		= 0.95
	thisProps.phasesArray[TSS_CLOSE_FH_LO].iFrameTot	= 46
	thisProps.phasesArray[TSS_CLOSE_FH_LO].sAnimName	= "close_fh_lo"
	
	thisProps.phasesArray[TSS_CLOSE_FH_MID].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_FH_MID].fActive 	= 0.174
	thisProps.phasesArray[TSS_CLOSE_FH_MID].fEnd 		= 0.95
	thisProps.phasesArray[TSS_CLOSE_FH_MID].iFrameTot	= 46
	thisProps.phasesArray[TSS_CLOSE_FH_MID].sAnimName	= "close_fh_md"
	
	thisProps.phasesArray[TSS_CLOSE_FH_HI].fInit 		= 0.0
	thisProps.phasesArray[TSS_CLOSE_FH_HI].fActive 		= 0.174
	thisProps.phasesArray[TSS_CLOSE_FH_HI].fEnd 		= 0.95
	thisProps.phasesArray[TSS_CLOSE_FH_HI].iFrameTot	= 46
	thisProps.phasesArray[TSS_CLOSE_FH_HI].sAnimName	= "close_fh_hi"
	
	thisProps.phasesArray[TSS_MID_BH_LO].fInit 		= 0.0
	thisProps.phasesArray[TSS_MID_BH_LO].fActive 	= 0.250
	thisProps.phasesArray[TSS_MID_BH_LO].fEnd 		= 0.95
	thisProps.phasesArray[TSS_MID_BH_LO].iFrameTot	= 48
	thisProps.phasesArray[TSS_MID_BH_LO].sAnimName	= "mid_bh_lo"
	
	thisProps.phasesArray[TSS_MID_BH_MID].fInit 	= 0.0
	thisProps.phasesArray[TSS_MID_BH_MID].fActive 	= 0.231
	thisProps.phasesArray[TSS_MID_BH_MID].fEnd 		= 0.95
	thisProps.phasesArray[TSS_MID_BH_MID].iFrameTot	= 52
	thisProps.phasesArray[TSS_MID_BH_MID].sAnimName	= "mid_bh_md"
	
	thisProps.phasesArray[TSS_MID_BH_HI].fInit 		= 0.0
	thisProps.phasesArray[TSS_MID_BH_HI].fActive 	= 0.231
	thisProps.phasesArray[TSS_MID_BH_HI].fEnd 		= 0.95
	thisProps.phasesArray[TSS_MID_BH_HI].iFrameTot	= 52
	thisProps.phasesArray[TSS_MID_BH_HI].sAnimName	= "mid_bh_hi"
ENDPROC

/// PURPOSE:
///    Initializes the array offsets given to us by Mark Tennant
/// PARAMS:
///    thisProps - 
PROC INITIALIZE_TENNIS_SWING_STATE_OFFSETS(TENNIS_GAME_PROPERTIES &thisProps)
	CPRINTLN(DEBUG_TENNIS, "INITIALIZE_TENNIS_SWING_STATE_OFFSETS called")
	thisProps.vSwingStateOffsets[TSS_BACKHAND_BS_LOW] 	= <<-0.8036, 0.6726, -0.6097>>
	thisProps.vSwingStateOffsets[TSS_BACKHAND_BS_MID] 	= <<-0.8147, 0.6746, -0.0154>>
	thisProps.vSwingStateOffsets[TSS_BACKHAND_BS_HI] 	= <<-0.8298, 0.6483, 0.615>>
	thisProps.vSwingStateOffsets[TSS_BACKHAND_TS_LOW] 	= <<-0.8254, 0.6648, -0.6352>>
	thisProps.vSwingStateOffsets[TSS_BACKHAND_TS_MID] 	= <<-0.8217, 0.6774, -0.012>>
	thisProps.vSwingStateOffsets[TSS_BACKHAND_TS_HI] 	= <<-0.8072, 0.6576, 0.5741>>

	thisProps.vSwingStateOffsets[TSS_FOREHAND_BS_LOW] 	= <<0.813, 0.6795, -0.5939>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_BS_MID] 	= <<0.8651, 0.6928, 0.0177>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_BS_HI] 	= <<0.8592, 0.6398, 0.585>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_TS_LOW] 	= <<0.8073, 0.6708, -0.5718>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_TS_MID] 	= <<0.831, 0.6645, 0.0059>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_TS_HI] 	= <<0.8462, 0.6823, 0.6137>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_SMASH] 		= <<0.0038, 0.571, 1.4844>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_SMASH_LEFT] 	= <<-0.6106, 0.6003, 1.4926>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_SMASH_RIGHT] 	= <<0.5791, 0.6003, 1.4524>>

	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_BS_LOW] 	= <<-0.1276, 0.6496, -0.6081>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_BS_MID] 	= <<-0.1219, 0.6858, -0.0039>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_BS_HI] 	= <<-0.158, 0.6565, 0.5989>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_TS_LOW] 	= <<-0.1302, 0.6646, -0.6002>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_TS_MID] 	= <<-0.1243, 0.6686, 0.0104>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_TS_HI] 	= <<-0.1614, 0.6659, 0.6349>>
	
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_BS_LOW] 	= <<0.2654, 0.7156, -0.6474>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_BS_MID] 	= <<0.2594, 0.6587, -0.0193>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_BS_HI] 	= <<0.2755, 0.6588, 0.5959>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_TS_LOW] 	= <<0.2617, 0.701, -0.6222>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_TS_MID] 	= <<0.2718, 0.6637, -0.0077>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_TS_HI] 	= <<0.269, 0.6677, 0.6025>>
	
	thisProps.vSwingStateOffsets[TSS_BACKHAND_BS_LOW_SKIP] 	= <<-0.7118, 0.5888, -0.6097>>
	thisProps.vSwingStateOffsets[TSS_BACKHAND_BS_MID_SKIP] 	= <<-0.7944, 0.5983, -0.0154>>
	thisProps.vSwingStateOffsets[TSS_BACKHAND_BS_HI_SKIP] 	= <<-0.7291, 0.5708, 0.615>>
	thisProps.vSwingStateOffsets[TSS_BACKHAND_TS_LOW_SKIP] 	= <<-0.6591, 0.4535, -0.6352>>
	thisProps.vSwingStateOffsets[TSS_BACKHAND_TS_MID_SKIP] 	= <<-0.7722, 0.6042, -0.012>>
	thisProps.vSwingStateOffsets[TSS_BACKHAND_TS_HI_SKIP] 	= <<-0.8057, 0.6484, 0.5741>>
	
	thisProps.vSwingStateOffsets[TSS_FOREHAND_BS_LOW_SKIP] 		= <<0.7679, 0.5793, -0.5939>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_BS_MID_SKIP] 		= <<0.7837, 0.616, 0.0177>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_BS_HI_SKIP] 		= <<0.6994, 0.5786, 0.585>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_TS_LOW_SKIP]	 	= <<0.8024, 0.5107, -0.5718>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_TS_MID_SKIP] 		= <<0.8475, 0.4297, 0.0059>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_TS_HI_SKIP] 		= <<0.8012, 0.6051, 0.6137>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_SMASH_SKIP] 		= <<-0.0045, 0.4354, 1.4844>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_SMASH_LEFT_SKIP] 	= <<-0.5737, 0.4354, 1.4926>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_SMASH_RIGHT_SKIP] = <<0.5691, 0.4354, 1.4524>>
	
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_BS_LOW_SKIP] 	= <<-0.4511, 0.4966, -0.6081>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_BS_MID_SKIP] 	= <<-0.5103, 0.51, -0.0039>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_BS_HI_SKIP] 	= <<-0.4779, 0.3857, 0.5989>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_TS_LOW_SKIP] 	= <<-0.3778, 0.5049, -0.6002>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_TS_MID_SKIP] 	= <<-0.3224, 0.5942, 0.0104>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_TS_HI_SKIP] 	= <<-0.3764, 0.6902, 0.6349>>
	
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_BS_LOW_SKIP] 	= <<0.4805, 0.7156, -0.6474>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_BS_MID_SKIP] 	= <<0.7625, 0.6532, -0.0193>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_BS_HI_SKIP] 	= <<0.6043, 0.6588, 0.5959>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_TS_LOW_SKIP] 	= <<0.473, 0.6992, -0.6222>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_TS_MID_SKIP] 	= <<0.5413, 0.6353, -0.0077>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_TS_HI_SKIP] 	= <<0.714, 0.6593, 0.6025>>
	
	thisProps.vSwingStateOffsets[TSS_LUNGE_BACKHAND_LOW] = <<-1.3525, 0.6005, -0.7221>>
	thisProps.vSwingStateOffsets[TSS_LUNGE_BACKHAND_MID] = <<-1.5755, 0.4699, -0.0379>>
	thisProps.vSwingStateOffsets[TSS_LUNGE_BACKHAND_HI]  = <<-1.318, 0.4631, 0.7024>>
	thisProps.vSwingStateOffsets[TSS_LUNGE_FOREHAND_LOW] = <<1.9622, 0.2122, -0.6432>>
	thisProps.vSwingStateOffsets[TSS_LUNGE_FOREHAND_MID] = <<1.9598, 0.5945, -0.0585>>
	thisProps.vSwingStateOffsets[TSS_LUNGE_FOREHAND_HI]  = <<1.8166, 0.3588, 0.7322>>

	thisProps.vSwingStateOffsets[TSS_DIVE_FOREHAND] = <<2.2269, -0.0119, 0.0417 >>
	thisProps.vSwingStateOffsets[TSS_DIVE_BACKHAND] = <<-2.2644, -0.1467, 0.0997 >>
	
	thisProps.vSwingStateOffsets[TSS_FOREHAND_TS_LO_FAR] 	= <<1.4174, 0.6731, -0.5824>>
	thisProps.vSwingStateOffsets[TSS_FOREHAND_TS_MID_FAR] 	= <<1.4134, 0.6671, 0.0059>> 
	thisProps.vSwingStateOffsets[TSS_FOREHAND_TS_HI_FAR] 	= <<1.4031, 0.6845, 0.6133>>
	
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_LO] 	= <<-0.1726, 0.6642, -0.6095>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_MID] 	= <<-0.2261, 0.7049, 0.0018>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_BH_HI] 	= <<-0.1558, 0.6787, 0.6703>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_LO] 	= <<0.2443, 0.6567, -0.5885>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_MID] 	= <<0.2594, 0.6587, -0.0193>>
	thisProps.vSwingStateOffsets[TSS_CLOSE_FH_HI] 	= <<0.2594, 0.6387, 0.6221>>
	
	thisProps.vSwingStateOffsets[TSS_MID_BH_LO] 	= <<-0.4975, 0.6646, -0.6002>>
	thisProps.vSwingStateOffsets[TSS_MID_BH_MID] 	= <<-0.4998, 0.6686, 0.0104>>
	thisProps.vSwingStateOffsets[TSS_MID_BH_HI] 	= <<-0.4952, 0.6659, 0.6349>>
	
	// Make these offsets very large so we never use them
	thisProps.vSwingStateOffsets[TSS_SERVE_TOSS] = <<1000, 1000, 1000>>
	thisProps.vSwingStateOffsets[TSS_WAITING] = <<1000, 1000, 1000>>
	thisProps.vSwingStateOffsets[TSS_NO_SWING] = <<1000, 1000, 1000>>
	
	thisProps.swingBucketSteps[0] = -2.2644
	thisProps.swingBucketSteps[1] = -0.8260
	thisProps.swingBucketSteps[2] = -0.6969
	thisProps.swingBucketSteps[3] = -0.4494
	thisProps.swingBucketSteps[4] = -0.2019
	thisProps.swingBucketSteps[5] = 0.0456
	thisProps.swingBucketSteps[6] = 0.2931
	thisProps.swingBucketSteps[7] = 0.5406
	thisProps.swingBucketSteps[8] = 0.7881
	thisProps.swingBucketSteps[9] = 1.0356
	thisProps.swingBucketSteps[10] = 2.6669
	
	// Prime the sorted matrix of swings
	INT iBucket, iBucketSwing
	REPEAT MAX_SWING_BUCKETS iBucket
		REPEAT MAX_SWINGS_PER_BUCKET iBucketSwing
			thisProps.swingBuckets[iBucket][iBucketSwing] = TSS_NO_SWING
		ENDREPEAT
	ENDREPEAT
	
	//Populate the sorted matrix of swings
	thisProps.swingBuckets[0][0] = TSS_BACKHAND_BS_HI
	thisProps.swingBuckets[0][1] = TSS_LUNGE_BACKHAND_HI
	thisProps.swingBuckets[0][2] = TSS_LUNGE_BACKHAND_MID
	thisProps.swingBuckets[0][3] = TSS_LUNGE_BACKHAND_LOW
	thisProps.swingBuckets[0][4] = TSS_DIVE_BACKHAND

	thisProps.swingBuckets[1][0] = TSS_BACKHAND_TS_HI
	thisProps.swingBuckets[1][1] = TSS_BACKHAND_TS_MID
	thisProps.swingBuckets[1][2] = TSS_BACKHAND_TS_LOW
	thisProps.swingBuckets[1][3] = TSS_BACKHAND_BS_MID
	thisProps.swingBuckets[1][4] = TSS_BACKHAND_BS_LOW
	thisProps.swingBuckets[1][5] = TSS_BACKHAND_TS_HI_SKIP
	thisProps.swingBuckets[1][6] = TSS_BACKHAND_TS_MID_SKIP
	thisProps.swingBuckets[1][7] = TSS_BACKHAND_BS_HI_SKIP
	thisProps.swingBuckets[1][8] = TSS_BACKHAND_BS_MID_SKIP
	thisProps.swingBuckets[1][9] = TSS_BACKHAND_BS_LOW_SKIP

	thisProps.swingBuckets[2][0] = TSS_BACKHAND_TS_LOW_SKIP
	thisProps.swingBuckets[2][1] = TSS_FOREHAND_SMASH_LEFT
	thisProps.swingBuckets[2][2] = TSS_FOREHAND_SMASH_LEFT_SKIP
	thisProps.swingBuckets[2][3] = TSS_CLOSE_BH_BS_HI_SKIP
	thisProps.swingBuckets[2][4] = TSS_CLOSE_BH_BS_MID_SKIP
	thisProps.swingBuckets[2][5] = TSS_CLOSE_BH_BS_LOW_SKIP
	thisProps.swingBuckets[2][6] = TSS_MID_BH_LO
	thisProps.swingBuckets[2][7] = TSS_MID_BH_MID
	thisProps.swingBuckets[2][8] = TSS_MID_BH_HI

	thisProps.swingBuckets[3][0] = TSS_CLOSE_BH_TS_HI_SKIP
	thisProps.swingBuckets[3][1] = TSS_CLOSE_BH_TS_MID_SKIP
	thisProps.swingBuckets[3][2] = TSS_CLOSE_BH_TS_LOW_SKIP
	thisProps.swingBuckets[3][3] = TSS_CLOSE_BH_MID
	
	thisProps.swingBuckets[4][0] = TSS_FOREHAND_SMASH
	thisProps.swingBuckets[4][1] = TSS_FOREHAND_SMASH_SKIP
	thisProps.swingBuckets[4][2] = TSS_CLOSE_BH_TS_HI
	thisProps.swingBuckets[4][3] = TSS_CLOSE_BH_TS_MID
	thisProps.swingBuckets[4][4] = TSS_CLOSE_BH_TS_LOW
	thisProps.swingBuckets[4][5] = TSS_CLOSE_BH_BS_HI
	thisProps.swingBuckets[4][6] = TSS_CLOSE_BH_BS_MID
	thisProps.swingBuckets[4][7] = TSS_CLOSE_BH_BS_LOW
	thisProps.swingBuckets[4][8] = TSS_CLOSE_BH_LO
	thisProps.swingBuckets[4][9] = TSS_CLOSE_BH_HI

	thisProps.swingBuckets[5][0] = TSS_CLOSE_FH_TS_HI
	thisProps.swingBuckets[5][1] = TSS_CLOSE_FH_TS_MID
	thisProps.swingBuckets[5][2] = TSS_CLOSE_FH_TS_LOW
	thisProps.swingBuckets[5][3] = TSS_CLOSE_FH_BS_HI
	thisProps.swingBuckets[5][4] = TSS_CLOSE_FH_BS_MID
	thisProps.swingBuckets[5][5] = TSS_CLOSE_FH_BS_LOW
	thisProps.swingBuckets[5][6] = TSS_CLOSE_FH_LO
	thisProps.swingBuckets[5][7] = TSS_CLOSE_FH_MID
	thisProps.swingBuckets[5][8] = TSS_CLOSE_FH_HI

	thisProps.swingBuckets[6][0] = TSS_CLOSE_FH_TS_LOW_SKIP
	thisProps.swingBuckets[6][1] = TSS_CLOSE_FH_BS_LOW_SKIP

	thisProps.swingBuckets[7][0] = TSS_FOREHAND_SMASH_RIGHT
	thisProps.swingBuckets[7][1] = TSS_FOREHAND_BS_HI_SKIP
	thisProps.swingBuckets[7][2] = TSS_FOREHAND_BS_MID_SKIP
	thisProps.swingBuckets[7][3] = TSS_FOREHAND_BS_LOW_SKIP
	thisProps.swingBuckets[7][4] = TSS_FOREHAND_SMASH_RIGHT_SKIP
	thisProps.swingBuckets[7][5] = TSS_CLOSE_FH_TS_HI_SKIP
	thisProps.swingBuckets[7][6] = TSS_CLOSE_FH_TS_MID_SKIP
	thisProps.swingBuckets[7][7] = TSS_CLOSE_FH_BS_HI_SKIP
	thisProps.swingBuckets[7][8] = TSS_CLOSE_FH_BS_MID_SKIP

	thisProps.swingBuckets[8][0] = TSS_FOREHAND_TS_HI
	thisProps.swingBuckets[8][1] = TSS_FOREHAND_TS_MID
	thisProps.swingBuckets[8][2] = TSS_FOREHAND_TS_LOW
	thisProps.swingBuckets[8][3] = TSS_FOREHAND_BS_HI
	thisProps.swingBuckets[8][4] = TSS_FOREHAND_BS_MID
	thisProps.swingBuckets[8][5] = TSS_FOREHAND_BS_LOW
	thisProps.swingBuckets[8][6] = TSS_FOREHAND_TS_HI_SKIP
	thisProps.swingBuckets[8][7] = TSS_FOREHAND_TS_MID_SKIP
	thisProps.swingBuckets[8][8] = TSS_FOREHAND_TS_LOW_SKIP

	thisProps.swingBuckets[9][0] = TSS_LUNGE_FOREHAND_HI
	thisProps.swingBuckets[9][1] = TSS_LUNGE_FOREHAND_MID
	thisProps.swingBuckets[9][2] = TSS_LUNGE_FOREHAND_LOW
	thisProps.swingBuckets[9][3] = TSS_FOREHAND_TS_LO_FAR
	thisProps.swingBuckets[9][4] = TSS_FOREHAND_TS_MID_FAR
	thisProps.swingBuckets[9][5] = TSS_FOREHAND_TS_HI_FAR
	thisProps.swingBuckets[9][6] = TSS_DIVE_FOREHAND
ENDPROC

PROC SET_TENNIS_PAUSED(BOOL bTennisPaused, TENNIS_BALL &thisBall)
	SET_GAME_PAUSED(bTennisPaused)
	IF bTennisPaused
		SET_TENNIS_BALL_FLAG(thisBall, TBF_PAUSE_UPDATE)
	ELSE
		CLEAR_TENNIS_BALL_FLAG(thisBall, TBF_PAUSE_UPDATE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Should only be called in TA_PLAYER_VS_AI games, registers completion percentage if it has not yet been registered
/// PARAMS:
///    iNumSets - 
PROC HANDLE_TENNIS_COMPLETION_PERCENTAGE()
	IF NOT IS_TENNIS_SAVED_GLOBAL_FLAG_SET(g_savedGlobals.sTennisData, TGF_ONE_SET_FINISHED)
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_TENNIS)
		SET_TENNIS_SAVED_GLOBAL_FLAG(g_savedGlobals.sTennisData, TGF_ONE_SET_FINISHED)
		CPRINTLN(DEBUG_TENNIS, "Registering Completion Percentage for Tennis")
	ENDIF
ENDPROC

PROC INITIALIZE_TENNIS_LAUNCH_DETAILS(TENNIS_GAME_PROPERTIES &thisProps)
    thisProps.grades[LD_SERVE_S_PLUS].iForce = 45       //S+ shot at 2.929
    thisProps.grades[LD_SERVE_S].iForce = 40            //S
    thisProps.grades[LD_SERVE_A].iForce = 35            //A
    thisProps.grades[LD_SERVE_B].iForce = 30            //B
    thisProps.grades[LD_SERVE_C].iForce = 25            //C
    thisProps.grades[LD_SERVE_D].iForce = 20            //D
    thisProps.grades[LD_SERVE_FAULT].iForce = 30        //Fault
    thisProps.grades[LD_SERVE_OOB].iForce = 35			//Fault
    thisProps.grades[LD_MID].iForce = 24            //Mid Level shot at MID_HEIGHT
    thisProps.grades[LD_OVERHEAD].iForce = 27       //Overhead shot at OVERHEAD_HEIGHT
    
    thisProps.grades[LD_SERVE_S_PLUS].iAngle = 355      //S+ shot at 2.929
    thisProps.grades[LD_SERVE_S].iAngle = 356           //S
    thisProps.grades[LD_SERVE_A].iAngle = 358           //A
    thisProps.grades[LD_SERVE_B].iAngle = 2	            //B
    thisProps.grades[LD_SERVE_C].iAngle = 5             //C
    thisProps.grades[LD_SERVE_D].iAngle = 9             //D
    thisProps.grades[LD_SERVE_FAULT].iAngle = 354       //Fault
    thisProps.grades[LD_SERVE_OOB].iAngle = 2			//Fault
    thisProps.grades[LD_MID].iAngle = 15            //Mid Level shot at MID_HEIGHT
    thisProps.grades[LD_OVERHEAD].iAngle = 4        //Overhead shot at OVERHEAD_HEIGHT
ENDPROC

PROC INITIALIZE_FAMILY_PED_TENNIS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATUS &thisStatus, TENNIS_ACTIVITY thisTA, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
    INITIALIZE_TENNIS_COURT(thisProps.tennisCourt, thisProps)
    INITIALIZE_TENNIS_PLAYER(thisProps.tennisPlayers[eSelfID], eSelfID, COMPUTER, thisProps.vForward, thisProps.vRight)
    INITIALIZE_TENNIS_PLAYER(thisProps.tennisPlayers[eOtherID], eOtherID, COMPUTER, -thisProps.vForward, -thisProps.vRight)
    INITIALIZE_BALL(thisProps.tennisBall)
	
	thisProps.texIdleEvent = "Idle1"
    
    thisStatus.eGameState = TGSE_INIT
    thisProps.eTennisActivity = thisTA
    
    thisProps.tennisPlayers[eSelfID].playerAI.eDifficulty = AD_NORMAL
    thisProps.tennisPlayers[eOtherID].playerAI.eDifficulty = AD_NORMAL
    
    INITIALIZE_TENNIS_LAUNCH_DETAILS(thisProps)
	INITIALIZE_TENNIS_SWING_STATE_OFFSETS(thisProps)
	INITIALIZE_TENNIS_SWING_STATE_PHASES(thisProps)
    
    //SET_WIND(0)   
ENDPROC

PROC MAKE_TENNIS_AUTOSAVE_REQUEST()
	CPRINTLN(DEBUG_TENNIS, "MAKE_TENNIS_AUTOSAVE_REQUEST just called.")
	SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)
	MAKE_AUTOSAVE_REQUEST()
ENDPROC

PROC SET_FAMILY_TENNIS_PEDS_GROUP_AND_UNTARGETTABLE(TENNIS_PLAYER &tennisPlayers[])
  PRIVATE_SetDefaultFamilyMemberAttributes(tennisPlayers[0].pIndex, RELGROUPHASH_PLAYER)
  PRIVATE_SetDefaultFamilyMemberAttributes(tennisPlayers[1].pIndex, RELGROUPHASH_PLAYER)
//    SET_PED_CAN_BE_TARGETTED(tennisPlayers[0].pIndex, FALSE)
//    SET_PED_RELATIONSHIP_GROUP_HASH(tennisPlayers[0].pIndex, RELGROUPHASH_PLAYER)
//    SET_PED_CAN_BE_TARGETTED(tennisPlayers[1].pIndex, FALSE)
//    SET_PED_RELATIONSHIP_GROUP_HASH(tennisPlayers[1].pIndex, RELGROUPHASH_PLAYER)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisProps - 
///    thisTA - 
///    eloc - 
PROC INITIALIZE_AMBIENT_TENNIS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATUS &thisStatus, TENNIS_ACTIVITY thisTA, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, TennisLaunchData &launchData, MODEL_NAMES mOpponent=A_F_Y_Tennis_01, MODEL_NAMES mPlayerAI=A_M_M_TENNIS_01)
    thisProps.eTennisActivity = thisTA
    INITIALIZE_TENNIS_COURT(thisProps.tennisCourt, thisProps, launchData.eCourtToLaunch)
    VECTOR vTempForward, vTempRight
    IF eSelfID = (TENNIS_PLAYER_AWAY)
        vTempForward = thisProps.vForward
        vTempRight = thisProps.vRight
    ELIF eSelfID = (TENNIS_PLAYER_HOME)
        vTempForward = -thisProps.vForward
        vTempRight = -thisProps.vRight
    ENDIF
	
	thisProps.texIdleEvent = "Idle1"
    
    INITIALIZE_TENNIS_PLAYER(thisProps.tennisPlayers[eSelfID], eSelfID, COMPUTER, vTempForward, vTempRight)
    INITIALIZE_TENNIS_PLAYER(thisProps.tennisPlayers[eOtherID], eOtherID, COMPUTER, -vTempForward, -vTempRight)
    INITIALIZE_BALL(thisProps.tennisBall)
    INITIALIZE_TENNIS_LAUNCH_DETAILS(thisProps)
	INITIALIZE_TENNIS_SWING_STATE_OFFSETS(thisProps)
	INITIALIZE_TENNIS_SWING_STATE_PHASES(thisProps)
    
    INITIALIZE_AI_HOME_POSITIONS(thisProps.tennisPlayers[eSelfID].playerAI, thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisPlayers[eSelfID].vMyForward, thisProps.tennisPlayers[eSelfID].vMyRight, thisProps.tennisCourt.vServiceCorners[0])
    INITIALIZE_AI_HOME_POSITIONS(thisProps.tennisPlayers[eOtherID].playerAI, thisProps.tennisCourt.vCourtCorners[2], thisProps.tennisPlayers[eOtherID].vMyForward, thisProps.tennisPlayers[eOtherID].vMyRight, thisProps.tennisCourt.vServiceCorners[2])
    
    SET_TENNIS_STATUS_AI_WAIT(thisStatus, PICK_INT(launchData.bFreemodeIntro, launchData.iStartDelay, GET_RANDOM_INT_IN_RANGE(500,3000)))
    
    SET_TENNIS_GAME_STATE(thisStatus, TGSE_INIT)
    thisProps.mTheOpponentModel = mOpponent
    thisProps.mThePlayerAIModel = mPlayerAI
	AI_DIFFICULTY eDiff
	IF launchData.bFreemodeIntro
		eDiff = AD_EASY
		thisProps.bCheckPredictionFlag = TRUE
	ELSE
		eDiff = AD_NORMAL
	ENDIF
    thisProps.tennisPlayers[eSelfID].playerAI.eDifficulty = eDiff
    thisProps.tennisPlayers[eOtherID].playerAI.eDifficulty = eDiff
    
    REQUEST_MODEL(PROP_TENNIS_BALL)
    REQUEST_MODEL(mOpponent)
    REQUEST_MODEL(mPlayerAI)
    REQUEST_MODEL(PROP_TENNIS_RACK_01B)
    REQUEST_ANIM_DICT("mini@tennis")
	REQUEST_ANIM_DICT("mini@tennis@female")
	REQUEST_ANIM_DICT("weapons@tennis@male")
ENDPROC

PROC INITIALIZE_TENNIS_GAME_SP_DATA(TENNIS_GAME_SP_DATA &thisSPData, TENNIS_GAME_PROPERTIES &thisProps)
	thisSPData.fAbuseTimer = GET_RANDOM_FLOAT_IN_RANGE(120.0, 240.0)
	SET_TENNIS_SP_FLAG(thisSPData, TSP_GUARANTEE_FIRST_ABUSE)
	CDEBUG2LN(DEBUG_TENNIS, "INITIALIZE_TENNIS_GAME_SP_DATA :: thisSPData.fAbuseTimer=", thisSPData.fAbuseTimer)
	
	#IF NOT IS_TENNIS_MULTIPLAYER
	thisSPData.vQuitPoint = <<0,0,0>>
	#ENDIF
	
	IF (thisProps.thisLocation = ALOC_tennis_michaelHouse)
		thisSPData.vBlockingArea[0] = <<-766.24, 167.38, 66.00>>
		thisSPData.vBlockingArea[1] = <<-779.41, 140.22, 70.00>>
		SET_PED_PATHS_IN_AREA(thisSPData.vBlockingArea[0], thisSPData.vBlockingArea[1], FALSE)
//		thisSPData.blockingArea[0] = PREVENT_PEDS_FROM_BEING_IN_AREA(thisSPData.vBlockingArea[0], thisSPData.vBlockingArea[1])
		CPRINTLN(DEBUG_TENNIS, "INITIALIZE_TENNIS_COURT :: Preventing peds from being around ALOC_tennis_michaelHouse")
		
		#IF NOT IS_TENNIS_MULTIPLAYER
		thisSPData.vQuitPoint = <<-785.65, 155.18, 67.28>>
		#ENDIF
	ELIF (thisProps.thisLocation = ALOC_tennis_beachCourt)
		// These calls disable navmesh in small amounts, create small blocking areas, and create small non creation areas
		thisSPData.vBlockingArea[0] = <<-1173.058, -1600.912, 3.000>>
		thisSPData.vBlockingArea[1] = <<-1171.258, -1599.212, 7.100>>
		
		thisSPData.vBlockingArea[2] = <<-1181.387, -1630.644, 3.000>>
		thisSPData.vBlockingArea[3] = <<-1158.462, -1603.669, 7.000>>
		
		// Larger, overlapping, non_creation area
		thisSPData.vNonCreationArea[0] = <<-1229.030, -1597.280, 7.140>>
		thisSPData.vNonCreationArea[1] = <<-1139.230, -1685.360, 3.000>>
		
		SET_PED_PATHS_IN_AREA(thisSPData.vBlockingArea[2], thisSPData.vBlockingArea[3], FALSE)
		
		SET_PED_NON_CREATION_AREA(thisSPData.vBlockingArea[2], thisSPData.vBlockingArea[3])
		
		// Larger, overlapping, blocking area
		thisSPData.blockingArea[3] = ADD_SCENARIO_BLOCKING_AREA(thisSPData.vNonCreationArea[0], thisSPData.vNonCreationArea[1])
		CPRINTLN(DEBUG_TENNIS, "INITIALIZE_TENNIS_COURT :: Preventing peds from being around ALOC_tennis_beachCourt")
	ELSE
		thisSPData.vBlockingArea[0] = thisProps.tennisCourt.vCourtCorners[0]
		thisSPData.vBlockingArea[1] = thisProps.tennisCourt.vCourtCorners[2]
		thisSPData.blockingArea[0] = PREVENT_PEDS_FROM_BEING_IN_AREA(thisSPData.vBlockingArea[0], thisSPData.vBlockingArea[1])
		
		IF thisProps.thisLocation = ALOC_tennis_chumashHotel
			CPRINTLN(DEBUG_TENNIS, "Blocking chumash locations")
			thisSPData.vBlockingArea[2] = <<-2840.66382, -39.25038, 1.88216>>
			thisSPData.vBlockingArea[3] = <<-2842.75024, -42.03827, 2.46897>>
			thisSPData.blockingArea[1] = PREVENT_PEDS_FROM_BEING_IN_AREA(thisSPData.vBlockingArea[2], thisSPData.vBlockingArea[3])
		ENDIF
		IF thisProps.thisLocation = ALOC_tennis_vespucciHotel
			CPRINTLN(DEBUG_TENNIS, "Blocking vespucciHotel locations")
			thisSPData.vBlockingArea[2] = <<-963.646606,-1250.219971,6.967079>>
			thisSPData.vBlockingArea[3] = <<-906.628662,-1278.933105,7.967287>> 
			thisSPData.blockingArea[1] = PREVENT_PEDS_FROM_BEING_IN_AREA(thisSPData.vBlockingArea[2], thisSPData.vBlockingArea[3])
		ENDIF
		
		CPRINTLN(DEBUG_TENNIS, "INITIALIZE_TENNIS_COURT :: Preventing peds from being around ", GET_STRING_FROM_TENNIS_ALOC(thisProps.thisLocation))
		
	ENDIF
ENDPROC

FUNC AI_DIFFICULTY CALCULATE_TENNIS_FRIEND_DIFFICULTY(TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eOtherID)
    SWITCH thisUIData.playerChars[eOtherID]
        CASE CHAR_MICHAEL
            RETURN AD_NORMAL
        BREAK
        CASE CHAR_FRANKLIN
            RETURN AD_HARD
        BREAK
        CASE CHAR_TREVOR
            RETURN AD_EASY
        BREAK
        CASE CHAR_LAMAR
            RETURN AD_NORMAL
        BREAK
        CASE CHAR_JIMMY
            RETURN AD_EASY
        BREAK
        CASE CHAR_AMANDA
            RETURN AD_NORMAL
        BREAK
    ENDSWITCH
	
	RETURN AD_NORMAL
ENDFUNC

PROC REQUEST_PRESTREAM_TENNIS_ASSETS(TENNIS_GAME_PROPERTIES &thisProps, MODEL_NAMES eOpponentModel, BOOL bTutorial)
	CDEBUG2LN(DEBUG_TENNIS, "REQUEST_PRESTREAM_TENNIS_ASSETS called, eOpponentModel=", eOpponentModel)
	REQUEST_MODEL(eOpponentModel)
	thisProps.mTheOpponentModel = eOpponentModel
	
	REQUEST_MODEL(PROP_TENNIS_RACK_01B)
	REQUEST_ANIM_DICT("mini@tennis")
	REQUEST_ADDITIONAL_TEXT("TENNIS", MINIGAME_TEXT_SLOT)
	
	IF bTutorial
		REQUEST_ANIM_DICT("mini@tennisintro")
	ELSE
		REQUEST_ANIM_DICT("mini@tennisintro_alt1")
	ENDIF
ENDPROC

FUNC BOOL HAVE_PRESTREAM_TENNIS_ASSETS_LOADED(TENNIS_GAME_PROPERTIES &thisProps, BOOL bTutorial)
	IF NOT HAS_MODEL_LOADED(thisProps.mTheOpponentModel)
		IF thisProps.eTennisActivity <> TA_AI_VS_AI
			CPRINTLN(DEBUG_TENNIS, "Waiting for mTheOpponentModel")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(PROP_TENNIS_RACK_01B)
		IF thisProps.eTennisActivity <> TA_AI_VS_AI
			CPRINTLN(DEBUG_TENNIS, "Waiting for PROP_TENNIS_RACK_01B")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("mini@tennis")
		IF thisProps.eTennisActivity <> TA_AI_VS_AI
			CPRINTLN(DEBUG_TENNIS, "Waiting for mini@tennisintro")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)						
		CPRINTLN(DEBUG_TENNIS, "Waiting for MINIGAME_TEXT_SLOT")
		RETURN FALSE
	ENDIF
	
	IF bTutorial
		IF NOT HAS_ANIM_DICT_LOADED("mini@tennisintro")
			RETURN FALSE
		ENDIF
	ELSE
		IF NOT HAS_ANIM_DICT_LOADED("mini@tennisintro_alt1")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisProps - The game to initialize
PROC INITIALIZE_TENNIS_GAME(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATUS &thisStatus, TENNIS_ACTIVITY thisTA, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, MODEL_NAMES mOpponent=A_F_Y_Tennis_01, MODEL_NAMES mPlayerAI=A_M_M_TENNIS_01) //A_F_Y_Hipster_02
    //Disable unwanted functions
    DISABLE_SELECTOR()
    
    INITIALIZE_TENNIS_COURT(thisProps.tennisCourt, thisProps)
	
	thisProps.texIdleEvent = "Idle1"
	
	IF thisTA <> TA_FRIEND_ACTIVITY AND thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_MichaelHouse
		CPRINTLN(DEBUG_TENNIS, "Setting Amanda as the opponent")
		SET_TENNIS_GLOBAL_FLAG(TGF_AMANDA_IS_OPP)
	ENDIF
    
    VECTOR vTempForward, vTempRight
    IF eSelfID = TENNIS_PLAYER_AWAY
        vTempForward = thisProps.vForward
        vTempRight = thisProps.vRight
    ELIF eSelfID = TENNIS_PLAYER_HOME
        vTempForward = -thisProps.vForward
        vTempRight = -thisProps.vRight
    ENDIF
    
    INITIALIZE_BALL(thisProps.tennisBall)
    INITIALIZE_TENNIS_LAUNCH_DETAILS(thisProps)
	INITIALIZE_TENNIS_SWING_STATE_OFFSETS(thisProps)
	INITIALIZE_TENNIS_SWING_STATE_PHASES(thisProps)
    INITIALIZE_TENNIS_PLAYER(thisProps.tennisPlayers[eSelfID], eSelfID, HUMAN_LOCAL, vTempForward, vTempRight)
    INITIALIZE_TENNIS_PLAYER(thisProps.tennisPlayers[eOtherID], eOtherID, COMPUTER, -vTempForward, -vTempRight)
    
    thisStatus.eGameState = TGSE_PRE_INIT
	
	IF IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
		mOpponent = GET_NPC_PED_MODEL(CHAR_AMANDA)
		CDEBUG2LN(DEBUG_TENNIS, "TGF_AMANDA_IS_OPP is set, setting model to Amanda, mOpponent=", mOpponent)
	ELIF VALIDATE_PICK_DEFAULT_TENNIS_OPPONENT(thisTA)
		mOpponent = A_F_Y_Tennis_01
	ELIF GET_RANDOM_FLOAT_IN_RANGE() < 0.5
		mOpponent = A_M_M_TENNIS_01
		CDEBUG2LN(DEBUG_TENNIS, "setting model to A_M_M_TENNIS_01, mOpponent=", mOpponent)
	ENDIF
    
    thisProps.mTheOpponentModel = mOpponent
    thisProps.mThePlayerAIModel = mPlayerAI
    thisStatus.iNumSets = 1
	
    INT p=1
    REPEAT MAX_SETS p
        thisStatus.playerScores[0].iGamesWon[p] = -1
        thisStatus.playerScores[1].iGamesWon[p] = -1
    ENDREPEAT
    thisStatus.playerScores[0].iGamesWon[0] = 0
    thisStatus.playerScores[1].iGamesWon[0] = 0
    
    SWITCH thisTA
        CASE TA_PLAYER_VS_AI
            //Tennis is currently set up for this
            //("Tennis starting a Player vs AI game.")
            SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID],PLAYER_PED_ID())
            thisProps.eTennisActivity = thisTA
            DISPLAY_RADAR(FALSE)
        BREAK
        CASE TA_AI_VS_AI
            //Set tennisPlayers[eSelfID] to AI and init their AI
            //("Tennis starting an AI vs AI game.")
            INITIALIZE_TENNIS_PLAYER(thisProps.tennisPlayers[eSelfID], eSelfID, COMPUTER, thisProps.vForward, thisProps.vRight)
            REQUEST_MODEL(thisProps.mThePlayerAIModel)
            thisProps.eTennisActivity = thisTA
            SET_TENNIS_STATUS_AI_WAIT(thisStatus, GET_RANDOM_INT_IN_RANGE(500,3000))
        BREAK
        CASE TA_FRIEND_ACTIVITY
            //No current settings
            //("Tennis starting a Player vs AI Friend Activity game.")
            SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID],PLAYER_PED_ID())
            thisProps.eTennisActivity = thisTA
            DISPLAY_RADAR(FALSE)
        BREAK
        CASE TA_FAMILY_ACTIVITY
            //No Current Settings
            //("Tennis starting a Player vs AI Family Activity game.")
            SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID],PLAYER_PED_ID())
            thisProps.eTennisActivity = thisTA
            DISPLAY_RADAR(FALSE)
        BREAK
        CASE TA_MULTIPLAYER
            //No Current Settings
            //("Tennis starting a Player vs Player game across a network.")
            //SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID],PLAYER_PED_ID())
            thisProps.eTennisActivity = thisTA
            DISPLAY_RADAR(FALSE)
            INITIALIZE_TENNIS_COURT(thisProps.tennisCourt, thisProps, TC_VENICE_1)
        BREAK
        DEFAULT
            SCRIPT_ASSERT("TENNIS STARTED WITHOUT A VALID TA ENUM, PLEASE CONTACT ROB PEARSALL")
        BREAK
    ENDSWITCH
    
    IF GET_TENNIS_GAME_STATE(thisStatus) >= TGSE_INIT
        UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_SERVING_SIDE(thisStatus), eSelfID, FALSE, FALSE)
    ENDIF
        
    //Gets the friend for a friend activity
    IF NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())  
        thisProps.eTennisActivity = TA_FRIEND_ACTIVITY
        PRINTSTRING("starting friend activity\n")
    ELSE
        PRINTSTRING("friend not valid, using normal opponent\n")
        SET_MISSION_FLAG(TRUE)
    ENDIF
    
	// These are high-priority assets for getting the intro sync scene running.
	REQUEST_PRESTREAM_TENNIS_ASSETS(thisProps, thisProps.mTheOpponentModel, NOT HAS_TENNIS_CHARACTER_SEEN_TUTORIAL())
	WHILE NOT HAVE_PRESTREAM_TENNIS_ASSETS_LOADED(thisProps, NOT HAS_TENNIS_CHARACTER_SEEN_TUTORIAL())
		WAIT(0)
	ENDWHILE
	IF IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
		CDEBUG2LN(DEBUG_TENNIS, "Preloading Amanda's Variations")
		VECTOR vOffset = << -1284.62, -126.71, 44.74>>	// random place away from the court.
		SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID], CREATE_PED(PEDTYPE_PLAYER2, thisProps.mTheOpponentModel, vOffset, GET_HEADING_FROM_VECTOR_2D(-thisProps.vForward.x,-thisProps.vForward.y)))
		PED_INDEX ped = GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID])
		SET_PED_PRELOAD_VARIATION_DATA(ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 0) 
		SET_PED_PRELOAD_VARIATION_DATA(ped, INT_TO_ENUM(PED_COMPONENT,1), 0, 0) 
		SET_PED_PRELOAD_VARIATION_DATA(ped, INT_TO_ENUM(PED_COMPONENT,2), 0, 0) 
		SET_PED_PRELOAD_VARIATION_DATA(ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 0) 
		SET_PED_PRELOAD_VARIATION_DATA(ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0) 
		SET_PED_PRELOAD_VARIATION_DATA(ped, INT_TO_ENUM(PED_COMPONENT,5), 0, 0) 
		SET_PED_PRELOAD_VARIATION_DATA(ped, INT_TO_ENUM(PED_COMPONENT,7), 0, 0) 
		SET_PED_PRELOAD_VARIATION_DATA(ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0) 
		SET_PED_PRELOAD_VARIATION_DATA(ped, INT_TO_ENUM(PED_COMPONENT,10), 0, 0)
		
		WHILE NOT IS_PED_INJURED(ped) AND NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(ped)
			CDEBUG2LN(DEBUG_TENNIS, "Waiting for Amanda's Variations to load")
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC SET_TENNIS_DIALOGUE_STRUCT(structPedsForConversation &convos, enumCharacterList char, enumCharacterList char2, TENNIS_PLAYER &player1, TENNIS_PLAYER &player2)
	CPRINTLN(DEBUG_TENNIS, "SET_TENNIS_DIALOGUE_STRUCT called")
    SWITCH char
        CASE CHAR_MICHAEL
            ADD_PED_FOR_DIALOGUE(convos,0,GET_TENNIS_PLAYER_INDEX(player1),"MICHAEL")
			SET_TENNIS_PLAYER_VOICE(player1, "MICHAEL_NORMAL")
        BREAK
        CASE CHAR_FRANKLIN
            ADD_PED_FOR_DIALOGUE(convos,1,GET_TENNIS_PLAYER_INDEX(player1),"FRANKLIN")
			SET_TENNIS_PLAYER_VOICE(player1, "FRANKLIN_NORMAL")
        BREAK
        CASE CHAR_TREVOR
            ADD_PED_FOR_DIALOGUE(convos,2,GET_TENNIS_PLAYER_INDEX(player1),"TREVOR")
			SET_TENNIS_PLAYER_VOICE(player1, "TREVOR_NORMAL")
        BREAK
    ENDSWITCH
    SWITCH char2
        CASE CHAR_MICHAEL
            ADD_PED_FOR_DIALOGUE(convos,0,GET_TENNIS_PLAYER_INDEX(player2),"MICHAEL")
			SET_TENNIS_PLAYER_VOICE(player2, "MICHAEL_NORMAL")
        BREAK
        CASE CHAR_FRANKLIN
            ADD_PED_FOR_DIALOGUE(convos,1,GET_TENNIS_PLAYER_INDEX(player2),"FRANKLIN")
			SET_TENNIS_PLAYER_VOICE(player2, "FRANKLIN_NORMAL")
        BREAK
        CASE CHAR_TREVOR
            ADD_PED_FOR_DIALOGUE(convos,2,GET_TENNIS_PLAYER_INDEX(player2),"TREVOR")
			SET_TENNIS_PLAYER_VOICE(player2, "TREVOR_NORMAL")
        BREAK
        CASE CHAR_LAMAR
            ADD_PED_FOR_DIALOGUE(convos,3,GET_TENNIS_PLAYER_INDEX(player2),"LAMAR")
			SET_TENNIS_PLAYER_VOICE(player2, "LAMAR_NORMAL")
        BREAK
        CASE CHAR_AMANDA
            ADD_PED_FOR_DIALOGUE(convos,4,GET_TENNIS_PLAYER_INDEX(player2),"AMANDA")
			SET_TENNIS_PLAYER_VOICE(player2, "AMANDA_NORMAL")
        BREAK
        CASE CHAR_JIMMY
            ADD_PED_FOR_DIALOGUE(convos,6,GET_TENNIS_PLAYER_INDEX(player2),"JIMMY")
			SET_TENNIS_PLAYER_VOICE(player2, "JIMMY_NORMAL")
        BREAK
        DEFAULT
			STRING sPed
			BOOL bVariationCheck
			IF NOT IS_PED_INJURED( GET_TENNIS_PLAYER_INDEX(player2) )
			bVariationCheck = IS_PED_COMPONENT_VARIATION_VALID(GET_TENNIS_PLAYER_INDEX(player2),INT_TO_ENUM(PED_COMPONENT,0),1,0)
			ENDIF
			IF IS_TENNIS_PED_MALE((player2))
				sPed = PICK_STRING(bVariationCheck, "TENNISPLAYER3", "TENNISPLAYER4")
				SET_TENNIS_PLAYER_VOICE(player2, PICK_STRING(bVariationCheck, "A_M_M_TENNIS_01_BLACK_MINI_01", "A_M_M_TENNIS_01_WHITE_MINI_01"))
			ELSE
				sPed = PICK_STRING(bVariationCheck, "TENNISPLAYER2", "TENNISPLAYER1")
				SET_TENNIS_PLAYER_VOICE(player2, PICK_STRING(bVariationCheck, "A_F_Y_TENNIS_01_BLACK_MINI_01", "A_F_Y_TENNIS_01_WHITE_MINI_01"))
			ENDIF
            ADD_PED_FOR_DIALOGUE(convos,3,GET_TENNIS_PLAYER_INDEX(player2),sPed)
        BREAK
    ENDSWITCH
ENDPROC

PROC INITIALIZE_TENNIS_GAME_UI_DATA(TENNIS_GAME_UI_DATA &thisUIData, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
    INITIALIZE_TENNIS_CAMERAS(thisProps, thisUIData, thisUIData.tennisCameras, eSelfID)
    REQUEST_TENNIS_UI_SF(thisUIData.tennisUI)
    REQUEST_ADDITIONAL_TEXT("TENNIS", MINIGAME_TEXT_SLOT)
    thisUIData.iUtilFlags = 0
    SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_UPDATE_SCORES)
    SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_FIRST_SERVE)
    thisUIData.sSubtitleGroupID = "mgtnaud"
    thisUIData.fMessageTimer = 0
    thisUIData.fInterstitialTime = 0
    SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_SET_INIT)
    
    START_TIMER_NOW(thisUIData.interstitialTimer)
    TENNIS_PAUSE_TIMER(thisUIData.interstitialTimer)
    
    //Set the playerChars[eSelfID] as either
    //    CHAR_MICHAEL,  //0
    //    CHAR_FRANKLIN, //1
    //    CHAR_TREVOR,   //2
    thisUIData.playerChars[eSelfID] = GET_CURRENT_PLAYER_PED_ENUM()
    SET_TENNIS_PLAYER_NAME_OFF_ENUM(thisUIData, eSelfID)
    //Set the playerChars[eOtherID] here too
    
    IF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY
        thisUIData.playerChars[eOtherID] = GET_PLAYER_PED_ENUM(FRIEND_A_PED_ID())
        IF (thisUIData.playerChars[eOtherID] = NO_CHARACTER)
            thisUIData.playerChars[eOtherID] = GET_NPC_PED_ENUM(FRIEND_A_PED_ID())
            CPRINTLN(DEBUG_TENNIS, "Ped Enum was NO_CHARACTER, setting off NPC Ped Enum")
        ENDIF
            
        SET_TENNIS_PLAYER_NAME_OFF_ENUM(thisUIData, eOtherID)
    ELIF thisProps.eTennisActivity = TA_PLAYER_VS_AI AND NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
        thisUIData.playerChars[eOtherID] = NO_CHARACTER
	ELIF IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
		thisUIData.playerChars[eOtherID] = CHAR_AMANDA
        SET_TENNIS_PLAYER_NAME_OFF_ENUM(thisUIData, eOtherID)
    ENDIF
    
    SETTIMERB(0) // Used for UI
	
	REQUEST_PTFX_ASSET()
	REQUEST_STREAMED_TEXTURE_DICT("ShopUI_Title_Tennis")
    
    // Set the player ready for the game as long as it isn't AI vs AI.
    IF thisProps.eTennisActivity <> TA_AI_VS_AI
        IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
            CLEAR_PED_TASKS(PLAYER_PED_ID())
            #IF NOT IS_TENNIS_MULTIPLAYER
                SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE)
            #ENDIF
        ENDIF
    ENDIF
	
	#IF NOT IS_TENNIS_MULTIPLAYER
		SET_PLAYER_PORTRAITS(thisUIData.tennisUI, thisProps, eSelfID, eOtherID)
	#ENDIF
ENDPROC

PROC ADJUST_TENNIS_PLAYER_ON_STATS(enumCharacterList eCharFromUIData, TENNIS_PLAYER &thisPlayer)
	INT iStrength = GET_SP_PLAYER_PED_STAT_VALUE(eCharFromUIData, PS_STRENGTH)
	INT iStamina = GET_SP_PLAYER_PED_STAT_VALUE(eCharFromUIData, PS_STAMINA)
	#IF IS_TENNIS_MULTIPLAYER
		iStrength = GET_MP_INT_CHARACTER_STAT(MP_STAT_STRENGTH)		// MP is only based on strength right now, both values are off the stength stat.
		iStamina = GET_MP_INT_CHARACTER_STAT(MP_STAT_STRENGTH)
	#ENDIF
	FLOAT fAlpha = TO_FLOAT(iStrength + iStamina) / 200
	SET_TENNIS_PLAYER_STRENGTH_BOOST(thisPlayer, LERP_FLOAT(1.0, MAX_STRENGTH_BOOST, fAlpha))
	#IF IS_DEBUG_BUILD
		IF PLAYER_STAT_OVERRIDE_STR
			thisPlayer.fStrengthBoost = MAX_STRENGTH_BOOST
		ENDIF
	#ENDIF
ENDPROC

PROC TENNIS_AMBIENT_CLEANUP(TENNIS_GAME_PROPERTIES &thisProps)
//    DELETE_OBJECT(thisProps.tennisBall.oBall)
//    DELETE_OBJECT(thisProps.tennisPlayers[0].oTennisRacket)
//    DELETE_OBJECT(thisProps.tennisPlayers[1].oTennisRacket) 
	IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[0].oTennisRacket)
		DETACH_ENTITY(thisProps.tennisPlayers[0].oTennisRacket)
		SET_OBJECT_AS_NO_LONGER_NEEDED(thisProps.tennisPlayers[0].oTennisRacket)
	ENDIF
	IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
		DETACH_ENTITY(thisProps.tennisBall.oBall)
		SET_OBJECT_AS_NO_LONGER_NEEDED(thisProps.tennisBall.oBall)
	ENDIF
	IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[1].oTennisRacket)
		DETACH_ENTITY(thisProps.tennisPlayers[1].oTennisRacket)
		SET_OBJECT_AS_NO_LONGER_NEEDED(thisProps.tennisPlayers[1].oTennisRacket)
	ENDIF
    SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TENNIS_BALL)
    SET_MODEL_AS_NO_LONGER_NEEDED(thisProps.mTheOpponentModel)
    SET_MODEL_AS_NO_LONGER_NEEDED(thisProps.mThePlayerAIModel)
    SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TENNIS_RACK_01B)
    REMOVE_ANIM_DICT("mini@tennis")
    REMOVE_ANIM_DICT("mini@tennis@female")
	IF IS_TENNIS_GLOBAL_FLAG_SET(TGF_READY_FOR_FM_INTRO)
		CLEAR_TENNIS_GLOBAL_FLAG(TGF_READY_FOR_FM_INTRO)
	ENDIF
	IF IS_TENNIS_GLOBAL_FLAG_SET(TGF_START_AMBIENT_TENNIS)
		CLEAR_TENNIS_GLOBAL_FLAG(TGF_START_AMBIENT_TENNIS)
	ENDIF
	
	IF NOT TENNIS_IS_PLAYER_SPECTATING()
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
		NET_NL()NET_PRINT("TENNIS_AMBIENT_CLEANUP: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(),TRUE)")	
	ELSE
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
		NET_NL()NET_PRINT("TENNIS_AMBIENT_CLEANUP: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(),FALSE)")	
	ENDIF

    
    IF NOT IS_ENTITY_DEAD(thisProps.tennisPlayers[TENNIS_PLAYER_AWAY].pIndex) AND NOT IS_PED_BEING_STEALTH_KILLED(thisProps.tennisPlayers[TENNIS_PLAYER_AWAY].pIndex)
        CLEAR_TENNIS_PED_TASKS(thisProps.tennisPlayers, TENNIS_PLAYER_AWAY)
    ENDIF
    IF NOT IS_ENTITY_DEAD(thisProps.tennisPlayers[TENNIS_PLAYER_HOME].pIndex) AND NOT IS_PED_BEING_STEALTH_KILLED(thisProps.tennisPlayers[TENNIS_PLAYER_HOME].pIndex)
        CLEAR_TENNIS_PED_TASKS(thisProps.tennisPlayers, TENNIS_PLAYER_HOME)
    ENDIF
    
    CPRINTLN(DEBUG_TENNIS, "******************************** Ambient Tennis Last Post *********************************")
    TERMINATE_THIS_THREAD()
ENDPROC

#IF IS_DEBUG_BUILD
    PROC TENNIS_PRINT_CURRENT_GLOBAL_VALUES()
        CPRINTLN(DEBUG_TENNIS, "Current Tennis Globals below:")
        CPRINTLN(DEBUG_TENNIS, "iTotalAces     : ", g_savedGlobals.sTennisData.iTotalAces)
        CPRINTLN(DEBUG_TENNIS, "iTotalAced     : ", g_savedGlobals.sTennisData.iTotalAced)
        CPRINTLN(DEBUG_TENNIS, "iTotalPoints   : ", g_savedGlobals.sTennisData.iTotalPoints)
        CPRINTLN(DEBUG_TENNIS, "iHighestDeuce  : ", g_savedGlobals.sTennisData.iHighestDeuce)
        CPRINTLN(DEBUG_TENNIS, "iGamesWon      : ", g_savedGlobals.sTennisData.iGamesWon)
        CPRINTLN(DEBUG_TENNIS, "iGamesLost     : ", g_savedGlobals.sTennisData.iGamesLost)
        CPRINTLN(DEBUG_TENNIS, "iSetsWon       : ", g_savedGlobals.sTennisData.iSetsWon)
        CPRINTLN(DEBUG_TENNIS, "iSetsLost      : ", g_savedGlobals.sTennisData.iSetsLost)
        CPRINTLN(DEBUG_TENNIS, "iMatchesWon    : ", g_savedGlobals.sTennisData.iMatchesWon)
        CPRINTLN(DEBUG_TENNIS, "iMatchesLost   : ", g_savedGlobals.sTennisData.iMatchesLost)
        CPRINTLN(DEBUG_TENNIS, "iTotalOOB      : ", g_savedGlobals.sTennisData.iTotalOOB)
        CPRINTLN(DEBUG_TENNIS, "iTotalFaults   : ", g_savedGlobals.sTennisData.iTotalFaults)
    ENDPROC
#ENDIF

PROC TENNIS_SCRIPT_CLEANUP(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID #IF NOT IS_TENNIS_MULTIPLAYER, TENNIS_GAME_SP_DATA &thisSPData #ENDIF , enumActivityResult eResult, #IF IS_TENNIS_MULTIPLAYER INT iMatchHistoryID, INT missionVariation, #ENDIF BOOL bClearHelp=FALSE, BOOL bSeamless=FALSE)
    CPRINTLN(DEBUG_TENNIS, "Tennis Cleanup Initiated!")
	
	IF thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_WeazelCourt1
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			CDEBUG2LN(DEBUG_TENNIS, "TENNIS_SCRIPT_CLEANUP :: Stopping new load scene")
			NEW_LOAD_SCENE_STOP()
		ENDIF
	ENDIF
	
//	STATIC_BLIP_MINIGAME_TENNIS

	CLEAR_RANK_REDICTION_DETAILS()
	TENNIS_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
	
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()

	CLEAR_PED_NON_CREATION_AREA()
	
	CLEAR_WEATHER_TYPE_PERSIST()
	
	STOP_AUDIO_SCENE("TENNIS_SCENE")
	
	DESTROY_TENNIS_BLIP(thisUIData.tennisUI.oppBlip)
	
	CASCADE_SHADOWS_INIT_SESSION()
	
	RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
	
	// Allow Awards to show in other minigames (currently only used in Tennis MP and Shooting Range MP)
	MPGlobals.sMinigameMPGlobals.bHideMPAwards = FALSE
	
	REMOVE_PTFX_ASSET()
	
	IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
		ANIMPOSTFX_STOP("MinigameTransitionIn")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionOut")
		ANIMPOSTFX_STOP("MinigameTransitionOut")
	ENDIF

//	CLEANUP_MG_BIG_MESSAGE()
	
	TRIGGER_MUSIC_EVENT("MGTN_END")
	
    #IF IS_DEBUG_BUILD
        TENNIS_PRINT_CURRENT_GLOBAL_VALUES()
		IF GET_COMMANDLINE_PARAM_EXISTS("DR_tennis")
			DEBUG_RECORD_END()
			CDEBUG1LN(DEBUG_TENNIS, "DEBUG_RECORD_END(DR_tennis)")
		ENDIF
    #ENDIF
	
	#IF IS_TENNIS_MULTIPLAYER
		MAKE_TENNIS_AUTOSAVE_REQUEST()
		//gdisablerankupmessage = FALSE
		SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	#ENDIF
    
    SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE)
	
	// Remove custom PC keyboard and mouse control scheme
	SHUTDOWN_PC_SCRIPTED_CONTROLS()
    
    ENABLE_SELECTOR()
    
    IF g_bResultScreenDisplaying
        SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
    ENDIF
	
    IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[0].oTennisRacket)
        DELETE_OBJECT(thisProps.tennisPlayers[0].oTennisRacket)
    ENDIF
    IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[1].oTennisRacket)
        DELETE_OBJECT(thisProps.tennisPlayers[1].oTennisRacket)
    ENDIF
	
    CPRINTLN(DEBUG_TENNIS, "Cleaning up cams and ending Tennis")
    
    SET_MINIGAME_IN_PROGRESS(FALSE)
	
    DISPLAY_RADAR(TRUE)
    IF NOT bSeamless
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
    DESTROY_CAM(thisUIData.tennisCameras.tennisCamFar)
    DESTROY_CAM(thisUIData.tennisCameras.tennisCamFarOtherSide)
    DESTROY_CAM(thisUIData.tennisCameras.tennisCamDyna)
	DESTROY_CAM(thisUIData.tennisCameras.tennisCamTut, TRUE)
    DELETE_OBJECT(thisProps.tennisBall.oBall)
    DELETE_OBJECT(thisProps.tennisPlayers[0].oTennisRacket)
    DELETE_OBJECT(thisProps.tennisPlayers[1].oTennisRacket)
    CLEANUP_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext)
    REMOVE_ANIM_DICT("mini@tennis")
    REMOVE_ANIM_DICT("mini@tennis@female")
	REMOVE_ANIM_DICT("mini@triathlon")
    SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TENNIS_BALL)
    SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TENNIS_RACK_01B)
    CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT, TRUE)
    
    BEGIN_SCALEFORM_MOVIE_METHOD(thisUIData.tennisUI.scorePanelSF, "SET_SCOREPANEL")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
    END_SCALEFORM_MOVIE_METHOD()
    
    BEGIN_SCALEFORM_MOVIE_METHOD(thisUIData.tennisUI.scorePanelSF, "SET_SCOREBOARD")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
    END_SCALEFORM_MOVIE_METHOD()
    
    SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(thisUIData.tennisUI.scorePanelSF)
    SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(thisUIData.tennisUI.matchUI.siMovie)
//	#IF IS_TENNIS_MULTIPLAYER
    SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(thisUIData.tennisUI.uiShardMS.siMovie)
//	#ENDIF
	CLEANUP_SC_LEADERBOARD_UI(thisUIData.tennisUI.leaderboardUI)
    
	#IF IS_TENNIS_MULTIPLAYER
		CLEAR_OVERRIDE_WEATHER()
	    //Clean up freemode variables
	    IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
			INT iFMMCValue
			IF eResult = AR_playerWon
				iFMMCValue = ciFMMC_END_OF_MISSION_STATUS_PASSED
			ELIF eResult = AR_buddyA_won
				iFMMCValue = ciFMMC_END_OF_MISSION_STATUS_FAILED
			ELIF eResult = AR_playerQuit
				iFMMCValue = ciFMMC_END_OF_MISSION_STATUS_CANCELLED
			ELSE
				CPRINTLN(DEBUG_TENNIS, "TENNIS_SCRIPT_CLEANUP: eResult is unaccounted for enum: ", eResult)
				iFMMCValue = ciFMMC_END_OF_MISSION_STATUS_NOT_SET
			ENDIF
			IF NETWORK_IS_GAME_IN_PROGRESS()
				DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MG_TENNIS, iMatchHistoryID, missionVariation)
			ENDIF
			CPRINTLN(DEBUG_TENNIS, "Resetting Variables with calculated value")
	        RESET_FMMC_MISSION_VARIABLES(TRUE, FALSE, iFMMCValue)
	    ENDIF
	IF NOT IS_PLAYER_SCTV( PLAYER_ID() ) 
		SET_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), sMPVars)
        SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, m_eHeadProp, FALSE) 
		FINALIZE_HEAD_BLEND(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
	ENDIF
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_TENNIS, "PRINT_PED_VARIATIONS below (cleanup):")
			PRINT_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
		#ENDIF
		IF HAS_CELEBRATION_SCREEN_LOADED(thisUIData.sCelebData)
			SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE) 
			CLEANUP_CELEBRATION_SCREEN(thisUIData.sCelebData)
		ENDIF
	#ENDIF

    IF NOT bClearHelp AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_QUIT_BIT)
        CLEAR_HELP()
    ENDIF
        
    IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
        RESET_PED_STRAFE_CLIPSET(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
    ENDIF   
    IF NOT IS_ENTITY_DEAD(thisProps.tennisPlayers[eSelfID].pIndex)
        CLEAR_TENNIS_PED_TASKS(thisProps.tennisPlayers, eSelfID, FALSE)
    ENDIF
    
    #IF NOT IS_TENNIS_MULTIPLAYER
	
	IF DOES_ENTITY_EXIST(thisSPData.oBottle) AND IS_ENTITY_ATTACHED(thisSPData.oBottle)
		CDEBUG2LN(DEBUG_TENNIS, "Detaching water bottle, failsafe cleanup")
		DETACH_ENTITY(thisSPData.oBottle)
		SET_OBJECT_AS_NO_LONGER_NEEDED(thisSPData.oBottle)
	ENDIF
	
		SET_PED_PATHS_BACK_TO_ORIGINAL(thisSPData.vBlockingArea[2], thisSPData.vBlockingArea[3])
		IF thisProps.eTennisActivity <> TA_AI_VS_AI
			CPRINTLN(DEBUG_TENNIS, "Unlocking outfits, reenabling cheats, reenabling special ability, unrequesting audio banks")
			IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), FALSE)
			ENDIF
			IF thisUIData.playerChars[eSelfID] = CHAR_MICHAEL
				SET_PED_COMP_ITEM_ACQUIRED_SP(PLAYER_ZERO, COMP_TYPE_OUTFIT, OUTFIT_P0_TENNIS, TRUE)
				SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_OUTFIT, OUTFIT_P0_TENNIS, TRUE)
			ELIF thisUIData.playerChars[eSelfID] = CHAR_TREVOR
				SET_PED_COMP_ITEM_ACQUIRED_SP(PLAYER_TWO, COMP_TYPE_OUTFIT, OUTFIT_P2_TENNIS, TRUE)
				SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_TWO, COMP_TYPE_OUTFIT, OUTFIT_P2_TENNIS, TRUE)
			ENDIF
			ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
			UNREQUEST_TENNIS_BANKS()
			ODDJOB_EXIT_CUTSCENE()
			IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
				SET_PED_CONFIG_FLAG(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), PCF_ForceNoTimesliceIntelligenceUpdate, FALSE)
			ENDIF
		ENDIF
		
		DELETE_OBJECT(thisSPData.oCollBox[0])
		DELETE_OBJECT(thisSPData.oCollBox[1])
		DELETE_OBJECT(thisSPData.oCollBox[2])
		DELETE_OBJECT(thisSPData.oCollBox[3])
    	IF IS_SCREEN_FADED_OUT()
	        DO_SCREEN_FADE_IN(100)
	    ENDIF
		
		IF DOES_ENTITY_EXIST(thisSPData.pedAbuser)
			SET_PED_AS_NO_LONGER_NEEDED(thisSPData.pedAbuser)
		ENDIF
	    REMOVE_ANIM_DICT("misscommon@response")
		
		IF NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
	        IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
	            RESET_PED_STRAFE_CLIPSET(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
	        ENDIF
	        IF NOT IS_ENTITY_DEAD(thisProps.tennisPlayers[eOtherID].pIndex)
	            CLEAR_TENNIS_PED_TASKS(thisProps.tennisPlayers, eOtherID, FALSE)
	        ENDIF
		ELIF thisProps.eTennisActivity <> TA_FRIEND_ACTIVITY
			IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
				DELETE_PED(thisProps.tennisPlayers[eOtherID].pIndex)
			ENDIF
			IF thisUIData.playerChars[eSelfID] = CHAR_MICHAEL
				RESET_FRIEND_LAST_CONTACT_TIMER(CHAR_MICHAEL, CHAR_AMANDA, FRIEND_CONTACT_FACE)
			ENDIF
			PRIVATE_Set_Current_Family_Member_Event(FM_MICHAEL_WIFE, FE_M7_WIFE_sunbathing)
		ENDIF
		CLEAR_TENNIS_GLOBAL_FLAG(TGF_AMANDA_IS_OPP)
	
		// Resetting last known info to be consistent with missions, B*909265
		ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_MISSION_NONE)
		CLEANUP_MENU_ASSETS()
		IF DOES_ENTITY_EXIST(thisSPData.oLighting)
			DELETE_OBJECT(thisSPData.oLighting)
		ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(Prop_VB_34_tencrt_lighting)
    #ENDIF
    
    SET_DISABLE_AMBIENT_MELEE_MOVE(GET_PLAYER_INDEX(), FALSE)
        
    DISABLE_CELLPHONE(FALSE)
    
	IF NOT bSeamless
    	SET_TENNIS_PLAYER_TO_TENNIS_MODE(PLAYER_PED_ID(),FALSE)
	ENDIF
	
    #IF NOT IS_TENNIS_MULTIPLAYER
        IF thisProps.eTennisActivity <> TA_AI_VS_AI
            SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]),FALSE)
        ENDIF
    
	    ALLOW_PEDS_TO_BE_IN_AREA(thisSPData.blockingArea[0], thisSPData.vBlockingArea[0], thisSPData.vBlockingArea[1])
		
		IF thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_chumashHotel
			ALLOW_PEDS_TO_BE_IN_AREA(thisSPData.blockingArea[1], thisSPData.vBlockingArea[2], thisSPData.vBlockingArea[3])
		ENDIF
		
		IF thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_CountryClub
//		    ALLOW_PEDS_TO_BE_IN_AREA(thisSPData.blockingArea[1], thisSPData.vBlockingArea[2], thisSPData.vBlockingArea[3])
//		    ALLOW_PEDS_TO_BE_IN_AREA(thisSPData.blockingArea[2], thisSPData.vBlockingArea[4], thisSPData.vBlockingArea[5])
			REMOVE_SCENARIO_BLOCKING_AREA(thisSPData.blockingArea[3])
		ENDIF
    #ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
    
    SET_TENNIS_PAUSED(FALSE, thisProps.tennisBall)
	
	FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
    
    CPRINTLN(DEBUG_TENNIS, "************************* Tennis Last Post **************************")
    
    #IF NOT IS_TENNIS_MULTIPLAYER
        
        IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	        SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
	    ENDIF
		
        IF (thisProps.eTennisActivity = TA_FRIEND_ACTIVITY)
			// If peds are ok, put buddy back in players group
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(FRIEND_A_PED_ID()) AND NOT IS_PED_INJURED(FRIEND_A_PED_ID())
					IF NOT IS_PED_GROUP_MEMBER(FRIEND_A_PED_ID(), PLAYER_GROUP_ID())
						SET_PED_AS_GROUP_MEMBER(FRIEND_A_PED_ID(), PLAYER_GROUP_ID())
						SET_PED_GROUP_MEMBER_PASSENGER_INDEX(FRIEND_A_PED_ID(), VS_FRONT_RIGHT)
					ENDIF
				ENDIF
			ENDIF
			
            IF NOT DOES_ENTITY_EXIST(FRIEND_A_PED_ID()) ORELSE IS_ENTITY_DEAD(FRIEND_A_PED_ID()) ORELSE IS_PED_INJURED(FRIEND_A_PED_ID()) 
                finishActivity(thisProps.thisLocation, AR_buddy_injured)
            ELIF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID()) ORELSE IS_ENTITY_DEAD(PLAYER_PED_ID()) ORELSE IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
                finishActivity(thisProps.thisLocation, AR_deatharrest)
            ELSE
                TASK_PAUSE(FRIEND_A_PED_ID(), -1)
                CLEAR_PED_TASKS_IMMEDIATELY(FRIEND_A_PED_ID())
                finishActivity(thisProps.thisLocation, eResult)
				CPRINTLN(DEBUG_TENNIS, "finishing friend activity, eResult=", eResult)
            ENDIF
        ENDIF		
        SET_MODEL_AS_NO_LONGER_NEEDED(thisProps.mTheOpponentModel)
		
		IF IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(200)
		ENDIF
        TERMINATE_THIS_THREAD()
    #ENDIF
    
    #IF IS_TENNIS_MULTIPLAYER
		SET_LOADING_ICON_INACTIVE()
		CPRINTLN(DEBUG_TENNIS, "FMMC_TYPE_MG_TENNIS=", FMMC_TYPE_MG_TENNIS, ", iMatchHistoryID=", iMatchHistoryID, ", missionVariation=", missionVariation)
        SET_TIME_OF_DAY(TIME_OFF, TRUE)
        ENABLE_ALL_MP_HUD()
        DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, FALSE)
        DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
        SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), FALSE)
        TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
    #ENDIF
    	
ENDPROC

/// PURPOSE:
///    
PROC PROCESS_REWARDS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_GAME_SP_DATA &thisSPData, TENNIS_PLAYER_ID eOpponentID)
    FLOAT fMoneyMod = 1.0
    SWITCH thisSPData.eDifficulty
        CASE AD_EASY
            fMoneyMod *= 0.5
        BREAK
        CASE AD_HARD
            fMoneyMod *= 1.5
        BREAK
    ENDSWITCH   
    FLOAT fRewardMoney = fBaseRewardMoney * fMoneyMod
    
    IF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY AND (thisUIData.playerChars[eOpponentID] = CHAR_MICHAEL OR thisUIData.playerChars[eOpponentID] = CHAR_FRANKLIN OR thisUIData.playerChars[eOpponentID] = CHAR_TREVOR)
        DEBIT_BANK_ACCOUNT(thisUIData.playerChars[eOpponentID], BAAC_UNLOGGED_SMALL_ACTION, FLOOR(fRewardMoney))
        fRewardMoney = CLAMP(fRewardMoney, 0, TO_FLOAT(GET_TOTAL_CASH(thisUIData.playerChars[eOpponentID])))
        CPRINTLN(DEBUG_TENNIS, "Should have subtracted $", fRewardMoney, " from friend ped")
    ENDIF
    
    CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, FLOOR(fRewardMoney))
    CPRINTLN(DEBUG_TENNIS, "Processing awards in Tennis, adding $", fRewardMoney)
ENDPROC

/// PURPOSE:
///    
PROC SCRIPT_PASSED(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_SP_DATA &thisSPData, TENNIS_GAME_UI_DATA &thisUIData, enumActivityResult result, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, BOOL bSeamless = FALSE)
    PROCESS_REWARDS(thisProps, thisUIData, thisSPData, eOtherID)
    TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, thisSPData, result, bSeamless)
ENDPROC

/// PURPOSE:
///    
PROC SCRIPT_FAILED(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_SP_DATA &thisSPData, TENNIS_GAME_UI_DATA &thisUIData, enumActivityResult result, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, BOOL bSeamless = FALSE)
    TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, thisSPData, result, bSeamless)
ENDPROC

/// PURPOSE:
///    Turns on the TD_PAUSE_THE_CUT and TD_PAUSE_DIALOGUE
/// PARAMS:
///    bitmask - 
PROC PAUSE_TENNIS_INTERSTITIAL_AND_DIALOGUE(INT &bitmask)
    SET_BITMASK_AS_ENUM(bitmask, TD_PAUSE_THE_CUT)
    SET_BITMASK_AS_ENUM(bitmask, TD_PAUSE_DIALOGUE)
    CPRINTLN(DEBUG_TENNIS, "Pausing dialogue and the interstitial cut")
ENDPROC

PROC TENNIS_ACTIVATE_SIDE_CHANGE_PART_1(TENNIS_GAME_PROPERTIES &thisProps)
	CPRINTLN(DEBUG_TENNIS, "TENNIS_ACTIVATE_SIDE_CHANGE_PART_1 called")
    IF thisProps.tennisPlayers[0].eCourtSide = TENNIS_PLAYER_AWAY
        thisProps.tennisPlayers[0].eCourtside = TENNIS_PLAYER_HOME
        thisProps.tennisPlayers[1].eCourtside = TENNIS_PLAYER_AWAY
        CPRINTLN(DEBUG_TENNIS, "Away player now on eCourtSide TENNIS_PLAYER_HOME")
    ELSE
        thisProps.tennisPlayers[0].eCourtside = TENNIS_PLAYER_AWAY
        thisProps.tennisPlayers[1].eCourtside = TENNIS_PLAYER_HOME
        CPRINTLN(DEBUG_TENNIS, "Away player now on eCourtSide TENNIS_PLAYER_AWAY")
    ENDIF
ENDPROC

PROC TENNIS_ACTIVATE_SIDE_CHANGE_PART_2(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_CAMERAS &thisCams)
    thisProps.tennisPlayers[0].vMyRight = thisProps.tennisPlayers[0].vMyRight * -1.0
    thisProps.tennisPlayers[0].vMyForward = thisProps.tennisPlayers[0].vMyForward * -1.0
    thisProps.tennisPlayers[1].vMyRight = thisProps.tennisPlayers[1].vMyRight * -1.0
    thisProps.tennisPlayers[1].vMyForward = thisProps.tennisPlayers[1].vMyForward * -1.0
	
	CPRINTLN(DEBUG_TENNIS, "TENNIS_ACTIVATE_SIDE_CHANGE_PART_2 called")
    IF thisCams.eChosenCam = TENNIS_CAMERA_FAR
        thisCams.eChosenCam = TENNIS_CAMERA_FAR_OTHER_SIDE
        CPRINTLN(DEBUG_TENNIS, "ePrevCam = TENNIS_CAMERA_FAR_OTHER_SIDE")
    ELIF thisCams.eChosenCam = TENNIS_CAMERA_FAR_OTHER_SIDE
        thisCams.eChosenCam = TENNIS_CAMERA_FAR
        CPRINTLN(DEBUG_TENNIS, "ePrevCam = TENNIS_CAMERA_FAR")
    ELSE
        CPRINTLN(DEBUG_TENNIS, "eChosenCam not changing during side change, eChosenCam = ", thisCams.eChosenCam)
    ENDIF
    
    IF thisCams.eTennisCam = TENNIS_CAMERA_FAR
        thisCams.eTennisCam = TENNIS_CAMERA_FAR_OTHER_SIDE
        CPRINTLN(DEBUG_TENNIS, "eTennisCam = TENNIS_CAMERA_FAR_OTHER_SIDE")
    ELIF thisCams.eTennisCam = TENNIS_CAMERA_FAR_OTHER_SIDE
        thisCams.eTennisCam = TENNIS_CAMERA_FAR
        CPRINTLN(DEBUG_TENNIS, "eTennisCam = TENNIS_CAMERA_FAR")
    ELSE
        CPRINTLN(DEBUG_TENNIS, "eTennisCam not changing during side change, eTennisCam = ", thisCams.eTennisCam)
    ENDIF
ENDPROC

/// PURPOSE:
///    Switching players' forward and right vectors and their eCourtSide
/// PARAMS:
///    thisProps - 
///    thisCams - uses ePrevCam, make sure that this is called after the appropriate SET_TENNIS_CAMERA_ENUM calls
PROC TENNIS_ACTIVATE_SIDE_CHANGE(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_CAMERAS &thisCams)
    CPRINTLN(DEBUG_TENNIS, "Swapping player sides, switching eCourtSide enum and vMyRight/vMyForward vectors. Potentially swapping ePrevCam.")
	TENNIS_ACTIVATE_SIDE_CHANGE_PART_1(thisProps)
	TENNIS_ACTIVATE_SIDE_CHANGE_PART_2(thisProps, thisCams)
ENDPROC

PROC READY_TENNIS_FOR_REMATCH(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_GAME_STATUS &thisStatus, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
    CPRINTLN(DEBUG_TENNIS, "***************************** Readying Tennis for a rematch *****************************")
    thisUIData.iCurrentOption = 0
    RESET_TENNIS_DEUCE_COUNT(thisUIData)
    SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_SET_INIT)
    thisUIData.ePrevUIState = TENNIS_UI_SET_INIT
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_MATCH_POINT_DSTAR)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PLR_SAD)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PLR_HAPPY)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_OPP_SAD)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_OPP_HAPPY)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_GAME_DONE_STISH)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_GAME_LOST)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_GAME_WON)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_POINT_LOST)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_POINT_WON)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PAUSE_DIALOGUE)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PAUSE_THE_CUT)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_POINT_W_DSTAR)
    CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_PAUSE_THE_CUT)
    SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_UPDATE_SCORES)
    SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_FIRST_SERVE)
	CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_WALKING_TO_POSITION)
	SET_TENNIS_CUTSCENE_STATE(thisUIData.tennisCameras, CUT_INIT)
	CLEAR_TENNIS_CONVO_FLAG(thisUIData, TCVF_AMANDA_GETS_LAST_WORD)

    SET_TENNIS_STATUS_WHO_IS_SERVING(thisStatus, eSelfID)
    SET_TENNIS_SERVING_SIDE(thisStatus, SERVING_FROM_RIGHT)
    SET_TENNIS_GAME_STATE(thisStatus, TGSE_OPTION_MENU)
    thisStatus.iServeCount = 0
    thisStatus.iCurrentSet = 0
    
    IF thisProps.tennisPlayers[eSelfID].eCourtSide <> eSelfID
        TENNIS_ACTIVATE_SIDE_CHANGE(thisProps, thisUIData.tennisCameras)
    ENDIF
    
    INT p=0
    WHILE p < GET_TENNIS_STATUS_NUM_SETS(thisStatus)
        thisStatus.playerScores[eSelfID].iGamesWon[p] = 0
        thisStatus.playerScores[eOtherID].iGamesWon[p] = 0
        p++
    ENDWHILE
    thisStatus.playerScores[eSelfID].iPointsWon = 0
    thisStatus.playerScores[eSelfID].iSetsWon = 0
    thisStatus.playerScores[eOtherID].iPointsWon = 0
    thisStatus.playerScores[eOtherID].iSetsWon = 0
	
	IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
		ANIMPOSTFX_STOP("MinigameTransitionIn")
	ENDIF
	ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
	
	SETUP_TENNIS_COURT_SHADOWS()
	ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eSelfID, eOtherID)
	SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), 
		GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, SERVING_FROM_RIGHT, thisProps.vRight, thisProps.vForward, TRUE, eOtherID))
	thisProps.tennisPlayers[eOtherID].vPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
	SET_ENTITY_HEADING(thisProps.tennisPlayers[eOtherID].pIndex, GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eOtherID].vMyForward.x,thisProps.tennisPlayers[eOtherID].vMyForward.y))
	SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), 
		GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, SERVING_FROM_RIGHT, thisProps.vRight, thisProps.vForward, FALSE, eSelfID))	//, TRUE, TRUE, TRUE)
	thisProps.tennisPlayers[eSelfID].vPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
	SET_ENTITY_HEADING(thisProps.tennisPlayers[eSelfID].pIndex, GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eSelfID].vMyForward.x,thisProps.tennisPlayers[eSelfID].vMyForward.y))
	STRING sDict = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eSelfID]), "mini@tennis", "mini@tennis@female")
	IF NOT IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), sDict, "intro_a")
		TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), sDict, "intro_a", default, default, default, AF_LOOPING)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), TRUE)
	ENDIF
	sDict = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherID]), "mini@tennis", "mini@tennis@female")
	IF NOT IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), sDict, "intro_b")
		TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), sDict, "intro_b", default, default, default, AF_LOOPING)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), TRUE)
	ENDIF
	
    SET_TENNIS_STATUS_REMATCH(thisStatus, TRUE)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisScores - 
///    iCurrentSet - 
/// RETURNS:
///    
FUNC BOOL TENNIS_SHOULD_SIDES_CHANGE(TENNIS_SCORES &thisScores[], INT iCurrentSet)
    INT iTotalGamesPlayed = 0
    INT p = 0
    
    WHILE p < iCurrentSet + 1
        iTotalGamesPlayed += thisScores[0].iGamesWon[p]
        iTotalGamesPlayed += thisScores[1].iGamesWon[p]
        p++
    ENDWHILE
    
    IF iTotalGamesPlayed % 2 = 0
        RETURN FALSE
    ELSE
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
ENDFUNC
