CONST_INT	IS_TENNIS_MULTIPLAYER	1
CONST_INT	TENNIS_OBJECTS			2
CONST_INT	NUM_TENNIS_MP_PLAYERS	4

USING "commands_hud.sch"
USING "commands_network.sch"
USING "net_include.sch"

PED_VARIATION_STRUCT sMPVars
PED_COMP_NAME_ENUM m_eHeadProp

USING "tennis_mp_core_lib.sch"

USING "Transition_Common.sch"

TENNIS_SERVER_BD 					serverBD
TENNIS_CLIENT_BD 					clientBD[NUM_TENNIS_MP_PLAYERS]
TENNIS_PARTICIPANT_INFO 			sParticipantInfo

//Game Variables
TENNIS_GAME_PROPERTIES thisProps
TENNIS_GAME_UI_DATA thisUIData

//Desync Variables
//VECTOR vBallPosAtDesync
//VECTOR vBallVelAtDesync
//FLOAT fBallScalarAtDesync
//BOOL bDesyncPrepped = FALSE


SCRIPT(MP_MISSION_DATA missionScriptArgs)
	CPRINTLN(DEBUG_TENNIS, "****************************** TENNIS MP FIRST POST SANITY *******************************")
	
	START_AUDIO_SCENE("TENNIS_SCENE")
	ANIMPOSTFX_STOP_ALL()
	TRIGGER_MUSIC_EVENT("MGTN_START")
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_TENNIS()
	Force_All_Drunk_Peds_To_Become_Sober_And_Quit_Camera()
	
	//gdisablerankupmessage = TRUE
	SET_DISABLE_RANK_UP_MESSAGE(TRUE)
	
	#IF IS_DEBUG_BUILD
//	TRACE_NATIVE_COMMAND("SET_PLAYER_CONTROL")
//	TRACE_NATIVE_COMMAND("ENABLE_TENNIS_MODE")
	#ENDIF
	
	SET_OVERRIDE_WEATHER("EXTRASUNNY")
	
	CLIENT_SET_UP_EVERYONE_CHAT()
	
	IF IS_TENNIS_GLOBAL_FLAG_SET(TGF_QUIT_FROM_TENNIS_MP)
		CLEAR_TENNIS_GLOBAL_FLAG(TGF_QUIT_FROM_TENNIS_MP)
	ENDIF
	
	BOOL bInitFailCleanup = NOT TENNIS_NETWORK_INIT(missionScriptArgs, serverBD, clientBD, sParticipantInfo)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashMac, serverBD.iMatchHistoryID)
		CPRINTLN(DEBUG_TENNIS, "serverBD.iMatchHistoryID=", serverBD.iMatchHistoryID, ", serverBD.iHashMac=", serverBD.iHashMac)
	ENDIF
	INIT_PC_SCRIPTED_CONTROLS("Tennis")
	
	SET_MINIGAME_IN_PROGRESS(TRUE)
	
	// ID to use when referencing the player array.
	// 1 = Host, HOME
	// 0 = Client, AWAY
	TENNIS_PLAYER_ID eSelfID = TENNIS_PLAYER_AWAY
	TENNIS_PLAYER_ID eOtherID = TENNIS_PLAYER_HOME
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		eSelfID = TENNIS_PLAYER_HOME
		eOtherID = TENNIS_PLAYER_AWAY
	ENDIF
	
	IF bInitFailCleanup
		CDEBUG1LN(DEBUG_TENNIS, "Failed to receive an initial network broadcast. Cleaning up.")
		TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, AR_playerQuit, serverBD.iMatchHistoryID, missionScriptArgs.mdID.idVariation, FALSE)
	ENDIF
	
	INT iMyParticipantID = PARTICIPANT_ID_TO_INT()
	
	UPDATE_TENNIS_PARTICIPANT_INFO(sParticipantInfo)
	PRINT_TENNIS_PARTICIPANT_INFO(sParticipantInfo)
	
	IF TENNIS_IS_PLAYER_SPECTATING()
	
		PARTICIPANT_INDEX piFocus
		
		IF IS_ENTITY_A_PED( GET_SPECTATOR_CURRENT_FOCUS_PED() )
		
			piFocus = NETWORK_GET_PARTICIPANT_INDEX( NETWORK_GET_PLAYER_INDEX_FROM_PED( GET_SPECTATOR_CURRENT_FOCUS_PED() ) )
			eSelfID = TENNIS_GET_PARTICIPANT_TENNIS_PLAYER_ID( NATIVE_TO_INT( piFocus ), sParticipantInfo )
			
			CPRINTLN(DEBUG_TENNIS, "Using GET_SPECTATOR_CURRENT_FOCUS_PED for self ID, participant = ", NATIVE_TO_INT( piFocus ))
		ELIF IS_ENTITY_A_PED( GET_SPECTATOR_DESIRED_FOCUS_PED() )
		
			piFocus = NETWORK_GET_PARTICIPANT_INDEX( NETWORK_GET_PLAYER_INDEX_FROM_PED( GET_SPECTATOR_DESIRED_FOCUS_PED() ) )
			eSelfID = TENNIS_GET_PARTICIPANT_TENNIS_PLAYER_ID( NATIVE_TO_INT( piFocus ), sParticipantInfo )
		
			CPRINTLN(DEBUG_TENNIS, "Using GET_SPECTATOR_DESIRED_FOCUS_PED for self ID, participant = ", NATIVE_TO_INT( piFocus ))
		ELSE
		
			eSelfID = TENNIS_PLAYER_HOME
		
			CPRINTLN(DEBUG_TENNIS, "Using GET_SPECTATOR_CURRENT_FOCUS_PED and GET_SPECTATOR_DESIRED_FOCUS_PED are BS, using Home for self ID")
		ENDIF
		
		IF eSelfID = TENNIS_PLAYER_HOME
		
			eOtherID = TENNIS_PLAYER_AWAY

		ELSE
			
			eOtherID = TENNIS_PLAYER_HOME
		
		ENDIF
		
	ENDIF		

	UPDATE_TENNIS_PARTICIPANT_INFO(sParticipantInfo)
	
	CDEBUG2LN(DEBUG_TENNIS, "eSelfID=", GET_STRING_FROM_TENNIS_PLAYER_ID(eSelfID), ", eOtherID=", GET_STRING_FROM_TENNIS_PLAYER_ID(eOtherID))
	// Variables for displaying time in wait messages.
	FLOAT fTimerVal
	
	// Despite being an IF the compiler jumps here when the function is true.
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU)
		CERRORLN(DEBUG_TENNIS, "HAS_FORCE_CLEANUP_OCCURRED: Game over, yeeeeaaahhh!")
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
		TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, AR_playerQuit, serverBD.iMatchHistoryID, missionScriptArgs.mdID.idVariation, FALSE)
	ENDIF
	
	CLEAR_AREA(thisProps.tennisCourt.vCenterCourt, 50.0, TRUE)
	CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vCenterCourt, 50.0)
	CLEAR_AREA_OF_PROJECTILES(thisProps.tennisCourt.vCenterCourt, 50.0)
	REMOVE_DECALS_IN_RANGE(thisProps.tennisCourt.vCenterCourt, 5000.0)
	CPRINTLN(DEBUG_TENNIS, "Clearing Area of bloody messes and resetting wanted level")
	IF NOT TENNIS_IS_PLAYER_SPECTATING()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE,SPC_REMOVE_PROJECTILES)
			SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE,SPC_REMOVE_EXPLOSIONS)
			SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE,SPC_DEACTIVATE_GADGETS)
		ENDIF
	ENDIF
	
//	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	//Load up a loading spinner
	TENNISMP_LOADING_SPINNER loadingSpinner
	//Initialize a bunch of stuff here
	INITIALIZE_MP_TENNIS(thisProps, eSelfID, eOtherID)
	TENNIS_ALIGN_SCTV_NORMALS_WITH_COURTSIDES(serverBD, thisProps)
	INITIALIZE_TENNIS_GAME_UI_DATA(thisUIData, thisProps, eSelfID, eOtherID)
	CPRINTLN(DEBUG_TENNIS, PICK_STRING(NETWORK_IS_HOST_OF_THIS_SCRIPT(), "Randomly Choosing Server", "Serving player is already chosen"))
	TENNIS_PLAYER_ID servingPlayer
	IF GET_RANDOM_FLOAT_IN_RANGE() >= 0.5
		servingPlayer = TENNIS_PLAYER_AWAY
	ELSE
		servingPlayer = TENNIS_PLAYER_HOME
	ENDIF
	SET_TENNIS_MP_WHO_IS_SERVING(serverBD, servingPlayer)	
	#IF IS_DEBUG_BUILD
		TENNIS_MP_LOAD_WIDGETS(serverBD, eSelfID, eOtherID, thisProps)
		SET_PROFILING_OF_THIS_SCRIPT(TRUE)
		BOOL bDebugDraw
	#ENDIF
	
	IF TENNIS_IS_PLAYER_SPECTATING()
		VECTOR vSCTV = thisProps.tennisCourt.vCenterCourt + ( thisProps.tennisCourt.vRightNorm * TENNIS_SPECTATOR_PLACEMENT_OFFSET )
		SET_ENTITY_COORDS( PLAYER_PED_ID(), vSCTV )
		#IF IS_DEBUG_BUILD
		DEBUG_RECORD_SPHERE("SCTV Placed Here", vSCTV, 1.0, (<<255, 255, 255>>))
		#ENDIF
		CDEBUG2LN(DEBUG_TENNIS, "vSCTV placed here = ", vSCTV)
	ENDIF
	
	NETWORK_OVERRIDE_CLOCK_TIME(12, 15, 0)
	DISABLE_ALL_MP_HUD()
	SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)
	FLOAT fWaitTimeOut = 0.0
	// Wait here for a second player.
	WHILE sParticipantInfo.iPlayerCount < ENUM_TO_INT(TENNIS_PLAYERS)
		// Update info until we get two players
		UPDATE_TENNIS_PARTICIPANT_INFO(sParticipantInfo)
		SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
		ENDIF
		
		IF fWaitTimeOut > WAITING_FOR_PLAYERS_TIMEOUT
			CDEBUG1LN(DEBUG_TENNIS, "NETWORK_GET_NUM_PARTICIPANTS() < ENUM_TO_INT(TENNIS_PLAYERS) :: Quitting because our waiting for other players timed out.")
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
			
			TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, AR_playerQuit, serverBD.iMatchHistoryID, missionScriptArgs.mdID.idVariation)
			
		ELIF fWaitTimeOut > TENNIS_WAIT_TIMEOUT__MESSAGE_PROMPT
		
			IF TENNIS_IS_PLAYER_SPECTATING()
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP("MP_QUITTER")
			ENDIF
			
		ENDIF
		
		#IF IS_DEBUG_BUILD			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				CLEAR_TENNIS_CHARACTER_SEEN_TUTORIAL()
				CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
				TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, AR_playerQuit, serverBD.iMatchHistoryID, missionScriptArgs.mdID.idVariation)
			ENDIF
		#ENDIF
		
		fWaitTimeOut += GET_FRAME_TIME()
		WAIT(0)
	ENDWHILE
	// Update info, we're in the game.
		
	PARTICIPANT_INDEX piHomeParticipant = NETWORK_GET_HOST_OF_THIS_SCRIPT()
	PARTICIPANT_INDEX piAwayParticipant = INT_TO_PARTICIPANTINDEX( TENNIS_GET_OTHER_PARTICIPANT( NATIVE_TO_INT( piHomeParticipant ), sParticipantInfo ) )
	
	CDEBUG2LN(DEBUG_TENNIS, "eSelfID=", GET_STRING_FROM_TENNIS_PLAYER_ID(eSelfID), ", eOtherID=", GET_STRING_FROM_TENNIS_PLAYER_ID(eOtherID))
	SET_TENNIS_PLAYER_NAME(thisUIData, TENNIS_PLAYER_HOME, GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX( piHomeParticipant ) ) )
	SET_TENNIS_PLAYER_NAME(thisUIData, TENNIS_PLAYER_AWAY, GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX( piAwayParticipant ) ) )
	SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[TENNIS_PLAYER_HOME], GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX( piHomeParticipant ) ) )
	SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[TENNIS_PLAYER_AWAY], GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX( piAwayParticipant ) ) )
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("DR_tennis")
			DEBUG_RECORD_START()
			CDEBUG1LN(DEBUG_TENNIS, "DEBUG_RECORD_START(DR_tennis)")
		ENDIF
	#ENDIF
	
	//No longer able to join this mission
	SET_FM_MISSION_AS_NOT_JOINABLE()
	
	DO_SCREEN_FADE_OUT(500)
	IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
		SET_PED_LOD_MULTIPLIER(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), DEBUG_LOD_SETTING)
	ENDIF
	IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
		SET_PED_LOD_MULTIPLIER(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), DEBUG_LOD_SETTING)
	ENDIF
	
	CPRINTLN(DEBUG_TENNIS, "Participant Names:")
	CPRINTLN(DEBUG_TENNIS, GET_TENNIS_PLAYER_NAME(thisUIData, eSelfID))
	CPRINTLN(DEBUG_TENNIS, GET_TENNIS_PLAYER_NAME(thisUIData, eOtherID))
	IF TENNIS_IS_PLAYER_SPECTATING()
		CPRINTLN(DEBUG_TENNIS, "I am SCTV/spectating")
		PRINT_TENNIS_PARTICIPANT_INFO( sParticipantInfo )
	ELSE
		CPRINTLN(DEBUG_TENNIS, "I am ", thisProps.tennisPlayers[eSelfID].playerAI.texName)
	ENDIF
	
	IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		SET_DISABLE_AMBIENT_MELEE_MOVE(GET_PLAYER_INDEX(), TRUE)
	ENDIF
	
	CLEAR_AREA_OF_PEDS(thisProps.tennisCourt.vCenterCourt, 100)
	CLEAR_HELP()
	TENNIS_PLAYER_ID ePrevSelfID
	
	#IF IS_DEBUG_BUILD
//		<<-1176.3427, -1621.0055, 4.9480>>, <<-7.3778, 0.0000, -57.4697>>
//		<<-1176.2418, -1620.9414, 4.9325>>, <<-7.3778, 0.0000, -57.4697>>
		FLOAT fHeading = GET_HEADING_FROM_VECTOR_2D(thisProps.vForward.x, thisProps.vForward.y)
		VECTOR vOffset1 = ROTATE_VECTOR_ABOUT_Z(<<-1176.3427, -1621.0055, 4.9480>> - thisProps.tennisCourt.vCenterCourt, -fHeading)
		VECTOR vOffset2 = ROTATE_VECTOR_ABOUT_Z(<<-1176.2418, -1620.9414, 4.9325>> - thisProps.tennisCourt.vCenterCourt, -fHeading)
		FLOAT fFinalHeading = -57.4697 - fHeading
		CDEBUG2LN(DEBUG_TENNIS, "vOffset1=", vOffset1, ", vOffset2=", vOffset2, ", fFinalHeading=", fFinalHeading)
	#ENDIF
	
	BOOL bHeadshotRegistered[TENNIS_PLAYERS]
	
	WHILE(TRUE)
		WAIT(0)
//		CDEBUG3LN(DEBUG_TENNIS, "GET_FRAME_COUNT(", GET_FRAME_COUNT(), ")")
		UPDATE_TENNIS_PARTICIPANT_INFO(sParticipantInfo)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			iMyParticipantID = PARTICIPANT_ID_TO_INT()
		ENDIF
		
		IF NOT IS_RADAR_HIDDEN()
			DISPLAY_RADAR(FALSE)
		ENDIF
		
		IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_MAKE_PLAYERS_INVISIBLE) AND VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
			SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( sParticipantInfo.iPlayers[eSelfID] ) ) )
			SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( sParticipantInfo.iPlayers[eOtherID] ) ) )
		ENDIF
		
		MAINTAIN_CELEBRATION_PRE_LOAD(thisUIData.sCelebData)
		
		IF TENNIS_IS_PLAYER_SPECTATING()
			
			IF IS_A_SPECTATOR_CAM_ACTIVE()
				IF NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX( NETWORK_GET_PLAYER_INDEX_FROM_PED( GET_SPECTATOR_CURRENT_FOCUS_PED() ) ) ) = -1	//VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
					CERRORLN(DEBUG_TENNIS, "Quitting SCTV, VALIDATE_TENNIS_PLAYERS_INFO returned FALSE")
					CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
					TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, AR_playerQuit, serverBD.iMatchHistoryID, missionScriptArgs.mdID.idVariation)
				ENDIF

				PARTICIPANT_INDEX piFocus
				
				IF IS_ENTITY_A_PED( GET_SPECTATOR_CURRENT_FOCUS_PED() )
				
					piFocus = NETWORK_GET_PARTICIPANT_INDEX( NETWORK_GET_PLAYER_INDEX_FROM_PED( GET_SPECTATOR_CURRENT_FOCUS_PED() ) )
					eSelfID = TENNIS_GET_PARTICIPANT_TENNIS_PLAYER_ID( NATIVE_TO_INT( piFocus ), sParticipantInfo )
					IF ePrevSelfID != eSelfID
						TENNIS_MP_BROADCAST_SCTV_REQUEST_UPDATE()
						ePrevSelfID = eSelfID
					ENDIF
					CDEBUG3LN(DEBUG_TENNIS, "Using GET_SPECTATOR_CURRENT_FOCUS_PED for self ID, participant = ", NATIVE_TO_INT( piFocus ))
				ELIF IS_ENTITY_A_PED( GET_SPECTATOR_DESIRED_FOCUS_PED() )
				
					piFocus = NETWORK_GET_PARTICIPANT_INDEX( NETWORK_GET_PLAYER_INDEX_FROM_PED( GET_SPECTATOR_DESIRED_FOCUS_PED() ) )
					eSelfID = TENNIS_GET_PARTICIPANT_TENNIS_PLAYER_ID( NATIVE_TO_INT( piFocus ), sParticipantInfo )
					IF ePrevSelfID != eSelfID
						TENNIS_MP_BROADCAST_SCTV_REQUEST_UPDATE()
						ePrevSelfID = eSelfID
					ENDIF
				
					CDEBUG3LN(DEBUG_TENNIS, "Using GET_SPECTATOR_DESIRED_FOCUS_PED for self ID, participant = ", NATIVE_TO_INT( piFocus ))
				ELSE
				
					eSelfID = TENNIS_PLAYER_HOME
					IF ePrevSelfID != eSelfID
						TENNIS_MP_BROADCAST_SCTV_REQUEST_UPDATE()
						ePrevSelfID = eSelfID
					ENDIF
				
					CDEBUG3LN(DEBUG_TENNIS, "Using GET_SPECTATOR_CURRENT_FOCUS_PED and GET_SPECTATOR_DESIRED_FOCUS_PED are BS, using Home for self ID")
				ENDIF
				
				IF eSelfID = TENNIS_PLAYER_HOME
				
					eOtherID = TENNIS_PLAYER_AWAY

				ELSE
					
					eOtherID = TENNIS_PLAYER_HOME
				
				ENDIF
			ENDIF
		ENDIF		
		
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CPRINTLN(DEBUG_TENNIS, "SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE: Game over, yeeeeaaahhh!")
			IF RECEIVED_EVENT(EVENT_NETWORK_END_MATCH)	// Player quit from pause menu
				SET_TENNIS_GLOBAL_FLAG(TGF_QUIT_FROM_TENNIS_MP)
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
			TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, AR_playerQuit, serverBD.iMatchHistoryID, missionScriptArgs.mdID.idVariation)
		ENDIF
		SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
		SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0.0, -0.0375)
		UPDATE_TENNIS_TIMER_A(thisProps)
		
		IF NOT IS_SELECTOR_DISABLED()	
			// Disable unwanted functions
			DISABLE_SELECTOR()
		ENDIF
		
		IF NOT IS_PED_INJURED( thisProps.tennisPlayers[eSelfID].pIndex )
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eSelfID].vMyForward.x, thisProps.tennisPlayers[eSelfID].vMyForward.y) - GET_ENTITY_HEADING(thisProps.tennisPlayers[eSelfID].pIndex))
		ENDIF
		
		NETWORK_OVERRIDE_CLOCK_TIME(12, 15, 0)
		
		// non-SCTV players have to broadcast their data flags for synchronization to SCTV....
		IF NOT TENNIS_IS_PLAYER_SPECTATING() AND NETWORK_GET_NUM_PARTICIPANTS() > 1
			IF thisUIData.iUtilFlags != thisUIData.iPrevUtilFlags
				CDEBUG3LN(DEBUG_TENNIS, "iPrevUtilFlags checksumming")
				TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UTIL_FLAGS( thisUIData )
				thisUIData.iPrevUtilFlags = thisUIData.iUtilFlags
			ENDIF
						
			IF thisUIData.tennisUI.iUIFlags != thisUIData.tennisUI.iPrevUIFlags
				CDEBUG3LN(DEBUG_TENNIS, "iPrevUIFlags checksumming")
				TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_FLAGS( thisUIData )
				thisUIData.tennisUI.iPrevUIFlags = thisUIData.tennisUI.iUIFlags
			ENDIF
		ENDIF
		
		BOOL bGamePaused = IS_PAUSE_MENU_ACTIVE()
				
		IF TENNIS_IS_PLAYER_SPECTATING()
			IF VALIDATE_TENNIS_MODE_ON_OPPONENT(thisProps, serverBD, clientBD, TENNIS_PLAYER_HOME, sParticipantInfo)
				SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[TENNIS_PLAYER_HOME]),TRUE)
				CDEBUG2LN(DEBUG_TENNIS, "WHILE(TRUE) :: Putting ", thisProps.tennisPlayers[TENNIS_PLAYER_HOME].playerAI.texName, " ped in Tennis Mode, top of WHILE loop")
			ENDIF
			IF VALIDATE_TENNIS_MODE_ON_OPPONENT(thisProps, serverBD, clientBD, TENNIS_PLAYER_AWAY, sParticipantInfo)
				SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[TENNIS_PLAYER_AWAY]),TRUE)
				CDEBUG2LN(DEBUG_TENNIS, "WHILE(TRUE) :: Putting ", thisProps.tennisPlayers[TENNIS_PLAYER_AWAY].playerAI.texName, " ped in Tennis Mode, top of WHILE loop")
			ENDIF
		ELSE
			IF VALIDATE_TENNIS_MODE_ON_OPPONENT_FOR_SCTV(thisProps, serverBD, clientBD, eOtherID, sParticipantInfo, iMyParticipantID)
				IF NOT IS_PED_INJURED(thisProps.tennisPlayers[eOtherID].pIndex) AND NOT IS_TENNIS_MODE( thisProps.tennisPlayers[eOtherID].pIndex )
					SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), TRUE)
					CDEBUG2LN(DEBUG_TENNIS, "WHILE(TRUE) :: Putting ", thisProps.tennisPlayers[eOtherID].playerAI.texName, " ped in Tennis Mode, top of WHILE loop")
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(thisProps.tennisPlayers[eOtherID].pIndex) AND IS_TENNIS_MODE( thisProps.tennisPlayers[eOtherID].pIndex )
					SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), FALSE)
					CDEBUG2LN(DEBUG_TENNIS, "WHILE(TRUE) :: Putting ", thisProps.tennisPlayers[eOtherID].playerAI.texName, " ped out of Tennis Mode, top of WHILE loop")
				ENDIF
			ENDIF
		ENDIF
				
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				WRITE_TENNIS_MP_SCLB_DATA()
				CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
				TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, AR_playerQuit, serverBD.iMatchHistoryID, missionScriptArgs.mdID.idVariation)
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
				CPRINTLN( DEBUG_TENNIS, "IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)" )
				SHOW_PLAYER_CARD_NO_MATTER_WHAT( thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), sParticipantInfo )
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
				bDebugDraw = !bDebugDraw
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bDebugDraw)
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT)
				IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_MAKE_PLAYERS_INVISIBLE)
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_MAKE_PLAYERS_INVISIBLE)
				ELSE
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_MAKE_PLAYERS_INVISIBLE)
				ENDIF
			ENDIF
			IF IS_KEYBOARD_KEY_PRESSED( KEY_RIGHT )
				NETWORK_OVERRIDE_CLOCK_TIME(12, 15, 0)
			ENDIF
			DRAW_DEBUG_SPHERE(thisProps.tennisCourt.vCenterCourt, 0.1, 255, 0, 255, 155)
			DRAW_DEBUG_SPHERE((<< TEST_X, TEST_Y, TEST_Z>>), TEST_RADIUS, 0, 255, 255, 155)
			PRINT_TENNIS_PARTICIPANT_INFO_TO_SCREEN( sParticipantInfo )
			TENNIS_UPDATE_WIDGETS( thisProps )
		#ENDIF
		
		IF GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_RALLY AND NOT TENNIS_IS_PLAYER_SPECTATING()
			DRAW_DEBUG_SPHERE(thisProps.tennisCourt.vCenterCourt, -COURT_RANGE, 0,0,255,50)
			UPDATE_TENNIS_MP_HEADSET_ICONS(GET_TENNIS_SERVER_MP_STATE(serverBD), thisUIData, clientBD, eSelfID, eOtherID, sParticipantInfo)
			
			HANDLE_TENNIS_PLAYER_OUTSIDE_PLAYING_AREA(thisProps.tennisPlayers[eSelfID], thisProps.tennisCourt)
		ENDIF
		
		//Showing the Quit UI
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
		AND GET_TENNIS_UI_STATE(thisUIData) <> TENNIS_UI_FAR_AWAY
		AND (GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_RALLY OR GET_TENNIS_SERVER_GAME_STATE(serverBD) = TGSE_TEST_STATE)
		AND NOT bGamePaused
			ALLOW_TENNIS_PLAYER_CONTROL(TA_MULTIPLAYER, FALSE)
			SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_QUIT)
			CPRINTLN(DEBUG_TENNIS, "Quit Button Press Detected")
		ENDIF
		
		IF NOT TENNIS_IS_PLAYER_SPECTATING()
			TENNIS_EVERY_FRAME_DISABLES()
		ENDIF
		
		// Need to prcess things thrown at us from the other clients.
		TENNIS_MP_PROCESS_BROADCAST_EVENTS(serverBD, clientBD, thisProps, thisUIData, thisProps.tennisBall, eSelfID, eOtherID, sParticipantInfo)
		TENNIS_MP_PROCESS_CAMERA_EVENTS(serverBD, clientBD[iMyParticipantID], thisProps, thisUIData, eSelfID, eOtherID, sParticipantInfo)
		
		IF (sParticipantInfo.iPlayerCount < ENUM_TO_INT(TENNIS_PLAYERS)) AND VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
			IF GET_TENNIS_SERVER_GAME_STATE(serverBD) < TGSE_OUTRO AND GET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID]) <> TMPS_QUIT_THE_GAME
				//SHOW_TENNIS_QUITTER_UI(thisUIData.tennisUI)
				CPRINTLN(DEBUG_TENNIS, "Quitting :: NETWORK_GET_NUM_PARTICIPANTS() < ENUM_TO_INT(TENNIS_PLAYERS)")
				IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[eOtherID].oTennisRacket)
					DETACH_ENTITY(thisProps.tennisPlayers[eOtherID].oTennisRacket)
				ENDIF
				IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
					TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), "mini@tennis", "ready_2_idle")
				ENDIF
				SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), FALSE)
				SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_OPP_OUT_OF_TENNIS_MODE)
				
				IF WAS_TENNIS_PLAYER_WINNING(serverBD, eSelfID, eOtherID)
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PLAY_TENNIS)
					SET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[iMyParticipantID], AR_playerWon)
					SET_TENNIS_UI_BETS_DATA(thisUIData.tennisUI, BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID()))
					CREATE_TENNIS_BETTING_SCOREBOARD(thisUIData.tennisUI, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt), TRUE)
				ELSE
					CDEBUG2LN(DEBUG_TENNIS, "BROADCAST_BETTING_MISSION_FINISHED_TIED() called in .sc loop")
					BROADCAST_BETTING_MISSION_FINISHED_TIED()
				ENDIF
				SET_TENNIS_FLOW_FLAG(thisProps, TFF_CONTINUE_AFTER_VOTE_PASSING)
				SET_TENNIS_TIMER_A(thisProps, SPLASH_UI_DURATION)
				CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
				SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
			ENDIF
		ENDIF
		
		IF GET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID]) <> TMPS_RUNNING AND GET_GAME_TIMER() > GET_TENNIS_CLIENT_WAIT_STAMP(clientBD[iMyParticipantID])
			PRINT_ALL_TENNIS_SERVER_DATA(serverBD)
			SET_TENNIS_CLIENT_WAIT_STAMP(clientBD[iMyParticipantID], GET_GAME_TIMER() + ROUND(DESYNC_THRESHOLD * 1000))
		ENDIF
		
		TENNIS_SUPPRESS_FIRST_PERSON_CAMERA()
	
		//Start loop based on Client MP State
		SWITCH GET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID])
			CASE TMPS_INIT
				// Wait until the server moves beyond the init state.
				IF GET_TENNIS_SERVER_MP_STATE(serverBD) > TMPS_INIT
					IF IS_SCREEN_FADED_OUT()
					
						IF TENNIS_IS_PLAYER_SPECTATING()
							//Set the game play camera along the line of the court to prevent the ped from rotating during interstitials
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eSelfID].vMyForward.x, thisProps.tennisPlayers[eSelfID].vMyForward.y))
							CLEAR_AREA_OF_VEHICLES(thisProps.tennisCourt.vCenterCourt, 20, FALSE, FALSE, TRUE)
					
							SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_STREAMING)
						ELSE
							//Do other things that are done before streaming/asset requests
							IF NOT IS_ENTITY_DEAD(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
								//Teleport the player to their starting position based on who they are.
								SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), thisProps.tennisCourt.vCourtCorners[PICK_INT(eSelfID = (TENNIS_PLAYER_AWAY), 1, 3)])
							ENDIF
							//Set the game play camera along the line of the court to prevent the ped from rotating during interstitials
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eSelfID].vMyForward.x, thisProps.tennisPlayers[eSelfID].vMyForward.y))
	//						NEW_LOAD_SCENE_START_SPHERE(thisProps.tennisCourt.vCenterCourt, 50)
							CLEAR_AREA_OF_VEHICLES(thisProps.tennisCourt.vCenterCourt, 20, FALSE, FALSE, TRUE)
				
							//Remove the weapon from the player
							IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
								SET_CURRENT_PED_WEAPON(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), WEAPONTYPE_UNARMED, TRUE)
							ENDIF
							GET_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), sMPVars)
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_TENNIS, "PRINT_PED_VARIATIONS below:")
								PRINT_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
							#ENDIF
							IF NOT IS_PLAYER_MALE(PLAYER_ID()) AND IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
					            //Set appropiate tennis shoes for female tennis players, no high heels
					            CPRINTLN(DEBUG_TENNIS, "Changing into appropiate tennis shoes") 
					            SET_PED_COMP_ITEM_CURRENT_MP(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), COMP_TYPE_FEET, FEET_FMF_10_1, FALSE) 
								FINALIZE_HEAD_BLEND(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
						    ENDIF
							
							m_eHeadProp = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD))
							SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_HEAD_NONE, FALSE)
							FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
							
							SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_STREAMING)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			// Stream the surrounding area.
			CASE TMPS_STREAMING
//				IF IS_NEW_LOAD_SCENE_ACTIVE()	//IS_NEW_LOAD_SCENE_LOADED()
					TENNIS_MP_BROADCAST_SCTV_REQUEST_UPDATE()
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_SETUP)
					
					SET_TRANSITION_STRING("MP_SPINLOADING")
					REFRESH_SCALEFORM_LOADING_ICON(loadingSpinner.loadingStruct)
					loadingSpinner.loadingStruct.sMainStringSlot = GET_TRANSITION_STRING()
					loadingSpinner.loadingStruct.sPlayerName = GET_TENNIS_PLAYER_NAME(thisUIData, eOtherID)
					loadingSpinner.loadingStruct.IsTextPlayerNameAndDoubleNumber = TRUE
					loadingSpinner.loadingStruct.IsTextDoubleNumber = FALSE
					
					REQUEST_TENNIS_ENDSCREEN(thisUIData.tennisUI)
//				ELSE
//					CPRINTLN(DEBUG_TENNIS, "Waiting on IS_NEW_LOAD_SCENE_ACTIVE()")
//				ENDIF
			BREAK
			
			// Put props/players/cameras into place.
			CASE TMPS_SETUP
								
				BOOL bMyHeadshotLoaded, bTheirHeadshotLoaded
				bMyHeadshotLoaded = REGISTER_HEAD_TEXTURE_MP(sParticipantInfo.iPlayers[eSelfID], bHeadshotRegistered[eSelfID], thisUIData.tennisUI.sHeadshots[eSelfID]) OR thisUIData.tennisUI.sHeadshots[eSelfID].iFailSafe > TENNIS_TEXTURE_LOAD_FAILSAFE
				bTheirHeadshotLoaded = REGISTER_HEAD_TEXTURE_MP(sParticipantInfo.iPlayers[eOtherID], bHeadshotRegistered[eOtherID], thisUIData.tennisUI.sHeadshots[eOtherID]) OR thisUIData.tennisUI.sHeadshots[eOtherID].iFailSafe > TENNIS_TEXTURE_LOAD_FAILSAFE
			
				IF bMyHeadshotLoaded
					bHeadshotRegistered[eSelfID] = FALSE
					CDEBUG1LN(DEBUG_TENNIS,"Frames needed to load headshot ", thisUIData.tennisUI.sHeadshots[eSelfID].iFailSafe)
					thisUIData.tennisUI.sHeadshots[eSelfID].iFailSafe = 0
				ELSE
					thisUIData.tennisUI.sHeadshots[eSelfID].iFailSafe++
				ENDIF
				
				IF bTheirHeadshotLoaded
					bHeadshotRegistered[eOtherID] = FALSE
					CDEBUG1LN(DEBUG_TENNIS,"Frames needed to load headshot ", thisUIData.tennisUI.sHeadshots[eOtherID].iFailSafe)
					thisUIData.tennisUI.sHeadshots[eOtherID].iFailSafe = 0
				ELSE
					thisUIData.tennisUI.sHeadshots[eOtherID].iFailSafe++
				ENDIF
				
				IF HAVE_REQUEST_TENNIS_ASSETS_LOADED(thisProps) AND HAVE_HUMAN_DATA_ASSETS_LOADED(thisUIData) AND bMyHeadshotLoaded AND bTheirHeadshotLoaded
					IF CAN_REGISTER_MISSION_OBJECTS(TENNIS_OBJECTS)
						thisUIData.tennisUI.sHeadTextures[eSelfID] = thisUIData.tennisUI.sHeadshots[eSelfID].headShotTxd
						thisUIData.tennisUI.sHeadTextures[eOtherID] = thisUIData.tennisUI.sHeadshots[eOtherID].headShotTxd
						SET_UP_MP_TENNIS_PLAYERS(thisProps, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), eSelfID, eOtherID)
						// prime the camera
						ADJUST_TENNIS_CAMERA_DYNAMIC(thisUIData.tennisCameras.tennisCamDyna,
													GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt),
													PICK_VECTOR(thisProps.tennisPlayers[eSelfID].eCourtSide = (TENNIS_PLAYER_AWAY), thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisCourt.vCourtCorners[2]),
													thisProps.tennisPlayers[eSelfID].vMyRight,
													thisProps.tennisPlayers[eSelfID].vMyForward,
													GET_TENNIS_BALL_POS(thisProps.tennisBall) - thisProps.tennisCourt.vCenterCourt,
													thisUIData.tennisCameras.fDynaCamSwing, DEFAULT_DYNA_DIST,
													thisUIData.tennisCameras.fDynaDolly,
													thisUIData.tennisCameras.fDynaNudgeRSLR, thisUIData.tennisCameras.fDynaNudgeRSUD, 
													FALSE, TRUE)
						SET_TENNIS_PI_CUR_PLAYER_ID(clientBD[iMyParticipantID], PLAYER_ID())
						
						SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_SYNC_OBJECTS)
						SET_TENNIS_CLIENT_FRAME_STAMP(clientBD[iMyParticipantID], GET_GAME_TIMER() + HEADSHOT_WAIT_LIMIT)
						CPRINTLN(DEBUG_TENNIS, "Set up players, PI_CUR_PLAYER_IDs, and set the client state")
					ELSE
						CPRINTLN(DEBUG_TENNIS, "IF CAN_REGISTER_MISSION_OBJECTS(TENNIS_OBJECTS) :: Returning FALSE")
					ENDIF
				ENDIF
			BREAK
			
			CASE TMPS_SYNC_OBJECTS
				SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SYNC_COMPLETE)
				INIT_ALL_TENNIS_UI(thisUIData.tennisUI)
				SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_SETUP_POST)
			BREAK
			
			// Wait here for server.
			CASE TMPS_SETUP_POST
				IF GET_TENNIS_SERVER_MP_STATE(serverBD) >= TMPS_RUNNING
					IF TENNIS_IS_PLAYER_SPECTATING()
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
					
					DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, TRUE)
					DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, TRUE)
					
					IF eSelfID = (TENNIS_PLAYER_AWAY)
						IF IS_TENNIS_SAVED_GLOBAL_FLAG_SET(g_savedGlobals.sTennisData, TGF_DEFAULT_CAM_DYNA)
							SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_DYNAMIC)
							TENNIS_MP_BROADCAST_SCTV_UI_SET_CAMERA_STATE( TENNIS_CAMERA_DYNAMIC )
						ELSE
							SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_FAR)
							TENNIS_MP_BROADCAST_SCTV_UI_SET_CAMERA_STATE( TENNIS_CAMERA_FAR )
						ENDIF
					ELIF eSelfID = (TENNIS_PLAYER_HOME)
						IF IS_TENNIS_SAVED_GLOBAL_FLAG_SET(g_savedGlobals.sTennisData, TGF_DEFAULT_CAM_DYNA)
							SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_DYNAMIC)
							TENNIS_MP_BROADCAST_SCTV_UI_SET_CAMERA_STATE( TENNIS_CAMERA_DYNAMIC )
						ELSE
							SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, TENNIS_CAMERA_FAR_OTHER_SIDE)
							TENNIS_MP_BROADCAST_SCTV_UI_SET_CAMERA_STATE( TENNIS_CAMERA_FAR_OTHER_SIDE )
						ENDIF
					ELSE
						SCRIPT_ASSERT("incorrect enum used when setting cameras")
					ENDIF
						
					INT iSets
					TENNIS_MATCH_RULESET eRuleset
					SWITCH g_FMMC_STRUCT.iNumOfSets
						CASE 0
							iSets = 1
							eRuleset = TMR_ONE_GAME
							CPRINTLN(DEBUG_TENNIS, "iNumSets as set by menu: 1 game")
						BREAK
						CASE 1
							iSets = 1
							eRuleset = TMR_THREE_GAMES
							CPRINTLN(DEBUG_TENNIS, "iNumSets as set by menu: 3 games")
						BREAK
						CASE 2
							iSets = 1
							eRuleset = TMR_FIVE_GAMES
							CPRINTLN(DEBUG_TENNIS, "iNumSets as set by menu: 5 games")
						BREAK
						CASE 3
							iSets = 1
							eRuleset = TMR_SETS
							CPRINTLN(DEBUG_TENNIS, "iNumSets as set by menu: 1 set")
						BREAK
						CASE 4
							iSets = 3
							eRuleset = TMR_SETS
							CPRINTLN(DEBUG_TENNIS, "iNumSets as set by menu: 3 sets")
						BREAK
						CASE 5
							iSets = 5
							eRuleset = TMR_SETS
							CPRINTLN(DEBUG_TENNIS, "iNumSets as set by menu: 5 sets")
						BREAK
					ENDSWITCH
					thisUIData.iRulesSelection = g_FMMC_STRUCT.iNumOfSets
					SET_TENNIS_MP_RULESET(serverBD, eRuleset)
					SET_TENNIS_MP_NUM_SETS(serverBD, iSets)
					
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SYNC_COMPLETE)
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_INIT_TUTORIAL)	//TMPS_WAIT_POST_MENU)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ELSE
					CPRINTLN(DEBUG_TENNIS, PICK_STRING(GET_TENNIS_SERVER_MP_STATE(serverBD) >= TMPS_RUNNING, "Waiting on New Load Scene", "Server is not in TMPS_RUNNING"))
				ENDIF
			BREAK
			
			CASE TMPS_INIT_TUTORIAL
				// Make a clone ped and hide it
				TENNIS_MAKE_CLIENT_CLONE_PED(clientBD[iMyParticipantID], GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), eSelfID)
				TENNIS_MAKE_CLIENT_CLONE_PED(clientBD[iMyParticipantID], GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), eOtherID)
				STORE_PLAYER_INTERACTION_ANIM_VALUES_FOR_CELEBRATION_SCREEN()
				
				IF NOT TENNIS_IS_PLAYER_SPECTATING()
					IF eSelfID = GET_TENNIS_MP_WHO_IS_SERVING(serverBD)
						SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, SERVING_FROM_RIGHT, eSelfID)
					ELSE
						SET_TENNIS_PLAYER_RECEIVE_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, SERVING_FROM_RIGHT, eSelfID)
					ENDIF
					thisProps.tennisPlayers[eSelfID].controlType = HUMAN_LOCAL_MP
					DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_TENNIS, serverBD.iMatchHistoryID, serverBD.iHashMac)
					IF NOT TENNIS_IS_PLAYER_SPECTATING()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						CDEBUG2LN(DEBUG_TENNIS, "NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)")
					ENDIF
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_WAIT_POST_MENU)
				ELSE
					// Teleport to a corner and stay invisible
					// Go to the state for SCTV
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(200)
						CDEBUG2LN(DEBUG_TENNIS, "CASE TMPS_INIT_TUTORIAL Fading in!")
					ENDIF
					REFRESH_SCTV_GAME_RULES( thisUIData, serverBD )

					CDEBUG2LN(DEBUG_TENNIS, "CASE TMPS_INIT_TUTORIAL :: Setting camera, attaching rackets, attaching ball")
					piHomeParticipant = NETWORK_GET_HOST_OF_THIS_SCRIPT()
					piAwayParticipant = INT_TO_PARTICIPANTINDEX( TENNIS_GET_OTHER_PARTICIPANT( NATIVE_TO_INT( piHomeParticipant ), sParticipantInfo ) )
					
					SET_TENNIS_PLAYER_NAME(thisUIData, TENNIS_PLAYER_HOME, GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX( piHomeParticipant ) ) )
					SET_TENNIS_PLAYER_NAME(thisUIData, TENNIS_PLAYER_AWAY, GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX( piAwayParticipant ) ) )
					SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[TENNIS_PLAYER_HOME], GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX( piHomeParticipant ) ) )
					SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[TENNIS_PLAYER_AWAY], GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX( piAwayParticipant ) ) )
					
					ATTACH_TENNIS_BALL_TO_PLAYER(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)], thisProps.tennisBall)
					TENNIS_MP_BROADCAST_SCTV_MAKE_BALL( serverBD, eSelfID )
					TENNIS_MP_BROADCAST_SCTV_REQUEST_UPDATE()
							
					IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_PLAYER_CARD_SHOULD_SHOW)
						SHOW_PLAYER_CARD_NO_MATTER_WHAT(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), sParticipantInfo, FALSE, FALSE)
					ELIF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SCOREBOARD_SHOULD_SHOW)
						SHOW_SCOREBOARD_NO_MATTER_WHAT(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), sParticipantInfo, FALSE, TRUE)
					ENDIF
							
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_SCTV)
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_POST_MENU
				// Sitting in a waiting state.
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, eSelfID = GET_TENNIS_MP_WHO_IS_SERVING(serverBD), FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				// Currently used post menu, so that the server can kick us into the game.
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_GAME)
					DO_SCREEN_FADE_IN(200)
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SHOW_SERVE_MESSAGE)
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_LOCKSTEP
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, FALSE, FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				//Waiting in this state after trying to move on to the next state
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_GAME)
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SWITCHED_TO_RUNNING)
					CPRINTLN(DEBUG_TENNIS, "Moving from TMPS_WAIT_LOCKSTEP back to running")
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_FOR_PREPARE_SERVE
				IF NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
					CERRORLN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_PREPARE_SERVE :: NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)")
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
				ENDIF
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, eSelfID = (GET_TENNIS_MP_WHO_IS_SERVING(serverBD)), FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				
				IF IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_SIDE_CHANGING)
					CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SIDE_CHANGING)
					TENNIS_HIDE_CLIENT_CLONE(clientBD[iMyParticipantID], eSelfID)
					TENNIS_HIDE_CLIENT_CLONE(clientBD[iMyParticipantID], eOtherID)
					ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eSelfID, eOtherID)
				ENDIF
				
				IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS)
					ALLOW_TENNIS_PLAYER_CONTROL(TA_MULTIPLAYER, FALSE)
				ENDIF
				
				IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB) OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB)) AND IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SERVER_FAULTED)
					SHOW_TENNIS_SCOREBOARD(thisUIData.tennisUI)
				ELIF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SERVER_FAULTED) AND IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_FIRST_FAULT)
					HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
					CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SCOREBOARD_SHOULD_SHOW)
					CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_PLAYER_CARD_SHOULD_SHOW)
				ENDIF
				
				INT iSeqProgress
				IF GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
					iSeqProgress = GET_SEQUENCE_PROGRESS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
					IF iSeqProgress >= (GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisProps.tennisPlayers[eSelfID]) - 1) AND NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SEQUENCE_DONE_SENT)
						SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SEQUENCE_DONE_SENT)
						TENNIS_MP_BROADCAST_SEQUENCE_DONE()
					ENDIF
				ENDIF
				
				IF NOT IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_BALL_RESET_IN_HAND) AND iSeqProgress >= 2
					SET_TENNIS_FLOW_FLAG(thisProps, TFF_BALL_RESET_IN_HAND)
					SET_TENNIS_PROPS_BALL_RESET_STAMP(thisProps, GET_GAME_TIMER() + ATTACH_TENNIS_BALL_STAMP)
				ELIF GET_GAME_TIMER() > GET_TENNIS_PROPS_BALL_RESET_STAMP(thisProps) AND IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_BALL_RESET_IN_HAND) AND DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND NOT IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
					DEBUG_SPAWN_BALL(thisProps, TRUE)
					TENNIS_MP_BROADCAST_SCTV_MAKE_BALL( serverBD, eSelfID )
					ATTACH_TENNIS_BALL_TO_PLAYER(thisProps.tennisPlayers[GET_TENNIS_MP_WHO_IS_SERVING(serverBD)], thisProps.tennisBall)
				ENDIF
				
				IF (IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_PREPARE_SERVE) AND GET_TENNIS_TIMER_A(thisProps) > SCOREBOARD_WAIT_THRESHOLD AND ARE_TENNIS_PLAYERS_IN_POSITION(thisProps.tennisPlayers, eSelfID, eOtherID, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), TA_MULTIPLAYER, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS), 0))
				OR (GET_TENNIS_TIMER_A(thisProps) > DEFAULT_TIME_BEFORE_WARP)
					IF GET_TENNIS_TIMER_A(thisProps) > DEFAULT_TIME_BEFORE_WARP
						CDEBUG2LN(DEBUG_TENNIS, "GET_TENNIS_TIMER_A(thisProps) > DEFAULT_TIME_BEFORE_WARP")
						CLEAR_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_WALKING_TO_POS)
					ENDIF
					IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_MAKE_PLAYERS_INVISIBLE)
						CLEAR_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_MAKE_PLAYERS_INVISIBLE)
					ENDIF
					IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SC_WALK_STARTED)
						CLEAR_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SC_WALK_STARTED)
					ENDIF
					CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[eOtherID].playerAI, TAF_PREVENT_MP_TENNIS_MODE_UPDATE)
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SWITCHED_TO_RUNNING)
					CLEAR_TENNIS_FLOW_FLAG(thisProps, TFF_BALL_RESET_IN_HAND)
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SEQUENCE_DONE_SENT)
					CPRINTLN(DEBUG_TENNIS, "Moving from TMPS_WAIT_FOR_PREPARE_SERVE back to running")
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_FOR_RALLY
				IF NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
					CERRORLN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_RALLY :: NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)")
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
				ENDIF
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, eSelfID = (GET_TENNIS_MP_WHO_IS_SERVING(serverBD)), FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				
				//Waiting in this state after trying to move on to the next state
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_RALLY)
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SWITCHED_TO_RUNNING)
					CPRINTLN(DEBUG_TENNIS, "Moving from TMPS_WAIT_FOR_RALLY back to running")
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_FOR_POINT_WON
				IF NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
					CERRORLN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_POINT_WON :: NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)")
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
				ENDIF
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, FALSE, FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				BOOL bDoneDivingReacting
				bDoneDivingReacting = GET_GAME_TIMER() > GET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisProps.tennisPlayers[eSelfID]) AND GET_GAME_TIMER() > GET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisProps.tennisPlayers[eOtherID])
				
				SET_TENNIS_MP_PROPS_CACHED_SET(thisProps, GET_TENNIS_MP_CURRENT_SET(serverBD))
				IF NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_NEW_RALLY) AND bDoneDivingReacting
					SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_NEW_RALLY)
					// Start players walking back to position.
					INT iScoredLastPoints, iTotalGamesPlayed, iOtherPoints, p
					iScoredLastPoints = thisProps.cachedMPScores[thisProps.eCachedScoredLast].iPointsWon + 1	// use cached values but don't cache them, the current server data may not be fresh
					iOtherPoints = thisProps.cachedMPScores[1 - ENUM_TO_INT(thisProps.eCachedScoredLast)].iPointsWon
					iTotalGamesPlayed = 0
					p = 0
					WHILE p < GET_TENNIS_MP_CURRENT_SET(serverBD) + 1
					    iTotalGamesPlayed += thisProps.cachedMPScores[0].iGamesWon[p]
					    iTotalGamesPlayed += thisProps.cachedMPScores[1].iGamesWon[p]
					    p++
					ENDWHILE
					BOOL bGameWon, bSideChange, bSwitchServeSide
					SERVING_SIDE_ENUM eServeSide
					TENNIS_PLAYER_ID eServingPlayer
					eServingPlayer = GET_TENNIS_MP_WHO_IS_SERVING(serverBD)
					bGameWon = iScoredLastPoints >= PICK_INT(IS_TENNIS_MP_ON_TIE_BREAK(serverBD), 7, 4) AND iScoredLastPoints - iOtherPoints >= 2
					IF bGameWon
						iTotalGamesPlayed++
						eServeSide = SERVING_FROM_RIGHT
						IF iTotalGamesPlayed % 2 <> 0
							bSideChange = TRUE
						ENDIF
						CDEBUG2LN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_POINT_WON :: Game won, iTotalGamesPlayed=", iTotalGamesPlayed, ", bSideChange=", PICK_STRING(bSideChange, "TRUE", "FALSE"))
					ELSE
						bSideChange = FALSE
						bSwitchServeSide = TRUE
						IF IS_TENNIS_MP_ON_TIE_BREAK(serverBD)
							IF IS_BIT_SET(iScoredLastPoints + iOtherPoints, 0)	// If the number of points is odd then we switch servers
								eServeSide = SERVING_FROM_RIGHT
								eServingPlayer = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eServingPlayer))
								bSwitchServeSide = FALSE
								CPRINTLN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_POINT_WON :: Changing servers, composite score is odd")
							ELIF (iScoredLastPoints + iOtherPoints) % 6 = 0		// If a multiple of six points was won then we switch court sides
								bSideChange = TRUE
								CPRINTLN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_POINT_WON :: Changing sides, 6 points have been won since last change")
							ENDIF
						ENDIF
						IF bSwitchServeSide
							IF GET_TENNIS_MP_SERVING_SIDE(serverBD) = SERVING_FROM_RIGHT
								eServeSide = SERVING_FROM_LEFT
							ELSE
								eServeSide = SERVING_FROM_RIGHT
							ENDIF
						ENDIF
						CDEBUG2LN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_POINT_WON :: Game not won")
					ENDIF
					CDEBUG2LN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_POINT_WON :: Checking points for walking back to pos, iScoredLastPoints=", iScoredLastPoints, ", iOtherPoints=", iOtherPoints, ", iTotalGamesPlayed=", iTotalGamesPlayed)
//					IF NOT bSideChange
						SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_WALKING_TO_POS)
						SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[eOtherID].playerAI, TAF_PREVENT_MP_TENNIS_MODE_UPDATE)
						IF eSelfID = (eServingPlayer) ^ bGameWon
							SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, eServeSide, eSelfID, default, default, FALSE, TRUE, bSideChange)
						ELSE
							SET_TENNIS_PLAYER_RECEIVE_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, eServeSide, eSelfID, default, TRUE, bSideChange)
						ENDIF
//					ENDIF
				ENDIF
				//Waiting in this state after trying to move on to the next state
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_POINT_WON) AND bDoneDivingReacting
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SWITCHED_TO_RUNNING)
					CPRINTLN(DEBUG_TENNIS, "Moving from TMPS_WAIT_FOR_POINT_WON back to running")
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_FOR_POINT_CHECKS
				IF NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
					CERRORLN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_POINT_CHECKS :: NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)")
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
				ENDIF
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, FALSE, FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				//Waiting in this state after trying to move on to the next state
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_POINT_CHECKS)
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SWITCHED_TO_RUNNING)
					CPRINTLN(DEBUG_TENNIS, "Moving from TMPS_WAIT_FOR_POINT_CHECKS back to running")
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_FOR_INTERSTITIAL
				IF NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
					CERRORLN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_INTERSTITIAL :: NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)")
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
				ENDIF
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, FALSE, FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				// Turning off player control so we don't run around, causing messages to the networked player have your ped running with reset flags off but not in tennis mode. It stops the walking and aiming like you're holding a gun.
				ALLOW_TENNIS_PLAYER_CONTROL(TA_MULTIPLAYER, FALSE)
				
				IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_INCREASE_SET)
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_INCREASE_SET)
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						INCREASE_CURRENT_TENNIS_SET(serverBD.playerScores, serverBD.iCurrentSet, thisUIData.iUtilFlags, GET_TENNIS_MP_NUM_SETS(serverBD))
					ENDIF
				ENDIF
				IF NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_NEW_RALLY) AND IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_FIRST_FAULT)
					SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[eSelfID].playerAI, TAF_TASKED_NEW_RALLY)
					
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_WALKING_TO_POS)
					IF eSelfID = (GET_TENNIS_MP_WHO_IS_SERVING(serverBD))
						SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, default, default, FALSE, TRUE)
					ELSE
						SET_TENNIS_PLAYER_RECEIVE_SERVE(thisProps.tennisPlayers[eSelfID], thisProps, GET_TENNIS_MP_SERVING_SIDE(serverBD), eSelfID, default, TRUE)
					ENDIF
				ENDIF
				IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_GAME_WAS_WON)
					REQUEST_CELEBRATION_SCREEN(thisUIData.sCelebData)
				ENDIF
				//Waiting in this state after trying to move on to the next state
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_INTERSTITIAL) AND GET_TENNIS_TIMER_A(thisProps) > (SPLASH_UI_DURATION + ROUND(GET_FRAME_TIME()*1000))
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SWITCHED_TO_RUNNING)
					CPRINTLN(DEBUG_TENNIS, "Moving from TMPS_WAIT_FOR_INTERSTITIAL back to running")
				ELSE
//					CDEBUG3LN(DEBUG_TENNIS, "TMPS_WAIT_FOR_INTERSTITIAL:")
//					CDEBUG3LN(DEBUG_TENNIS, "IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_INTERSTITIAL)=", IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_INTERSTITIAL))
//					CDEBUG3LN(DEBUG_TENNIS, "GET_TENNIS_TIMER_A(thisProps) > (SPLASH_UI_DURATION + ROUND(GET_FRAME_TIME()*1000))=", GET_TENNIS_TIMER_A(thisProps) > (SPLASH_UI_DURATION + ROUND(GET_FRAME_TIME()*1000)))
//					CDEBUG3LN(DEBUG_TENNIS, "---------------------------")
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_FOR_SIDE_CHANGE
				IF NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
					CERRORLN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_SIDE_CHANGE :: NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)")
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
				ENDIF
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, FALSE, FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				
				IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_INCREASE_SET)
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_INCREASE_SET)
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						INCREASE_CURRENT_TENNIS_SET(serverBD.playerScores, serverBD.iCurrentSet, thisUIData.iUtilFlags, GET_TENNIS_MP_NUM_SETS(serverBD))
					ENDIF
				ENDIF
				//Waiting in this state after trying to move on to the next state
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_SIDE_CHANGE)
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SWITCHED_TO_RUNNING)
					CPRINTLN(DEBUG_TENNIS, "Moving from TMPS_WAIT_FOR_SIDE_CHANGE back to running")
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_FOR_CHANGE_SERVERS
				IF NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
					CERRORLN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_CHANGE_SERVERS :: NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)")
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
				ENDIF
				//Using "1 - " in the call below because the current server won't be serving when we move into the case
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, eSelfID = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(GET_TENNIS_MP_WHO_IS_SERVING(serverBD))), FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				//Waiting in this state after trying to move on to the next state
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_CHANGE_SERVERS)
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SWITCHED_TO_RUNNING)
					CPRINTLN(DEBUG_TENNIS, "Moving from TMPS_WAIT_FOR_CHANGE_SERVERS back to running")
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_FOR_OUTRO
				IF NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
					CERRORLN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_OUTRO :: NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)")
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
				ENDIF
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, FALSE, FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				
				IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_INCREASE_SET)
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_INCREASE_SET)
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT() AND GET_TENNIS_MP_RULESET(serverBD) = TMR_SETS AND GET_TENNIS_MP_NUM_SETS(serverBD) > 1
						INCREASE_CURRENT_TENNIS_SET(serverBD.playerScores, serverBD.iCurrentSet, thisUIData.iUtilFlags, GET_TENNIS_MP_NUM_SETS(serverBD))
					ENDIF
				ENDIF
				//Waiting in this state after trying to move on to the next state
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_OUTRO) AND HAS_CELEBRATION_SCREEN_LOADED(thisUIData.sCelebData) AND GET_GAME_TIMER() > GET_TENNIS_CLIENT_OUTRO_STAMP(clientBD[iMyParticipantID])
					FLOAT fOutroLength
					fOutroLength = GET_TENNIS_REACT_ANIM_TIME(thisUIData.sInterstitialPlrAnim) * 1000
					SET_TENNIS_CLIENT_OUTRO_STAMP(clientBD[iMyParticipantID], GET_GAME_TIMER() + ROUND(fOutroLength * 0.4))
					UPDATE_TENNIS_UI_POINTS(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), 
						GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), sParticipantInfo, TRUE, TRUE)
					TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_POINTS(TRUE)
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SWITCHED_TO_RUNNING)
					CPRINTLN(DEBUG_TENNIS, "Moving from TMPS_WAIT_FOR_OUTRO back to running, SET_TENNIS_CLIENT_OUTRO_STAMP=fOutroLength * 0.4")
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_FOR_FINISH	// deprecated?
				IF NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
					CERRORLN(DEBUG_TENNIS, "CASE TMPS_WAIT_FOR_FINISH :: NOT VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)")
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
				ENDIF
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, FALSE, FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
				SET_LOADING_ICON_INACTIVE()

				//Waiting in this state after trying to move on to the next state
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_FINISH)
					SET_LOADING_ICON_INACTIVE()
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SWITCHED_TO_RUNNING)
					CPRINTLN(DEBUG_TENNIS, "Moving from TMPS_WAIT_FOR_FINISH back to running")
				ENDIF
			BREAK
			
			CASE TMPS_WAIT_FOR_PREPARE_REMATCH	// deprecated
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, FALSE, FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				
				thisUIData.fMessageTimer += GET_FRAME_TIME()
				fTimerVal = thisUIData.fMessageTimer
				INT iTimerVal
				iTimerVal = CEIL(FINAL_MENU_TIMEOUT - fTimerVal)
				IF (iTimerVal < 10)
					IF NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SPINNER_10SEC_UPDATE)
						// Print the help controls.
						BUILD_TENNIS_QUIT_WITH_SPINNER_MENU(FALSE)
						SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SPINNER_10SEC_UPDATE)
					ENDIF
				ENDIF
				DRAW_TENNIS_HELP_KEYS(iTimerVal)
				
				IF GET_TENNIS_UI_STATE(thisUIData) = TENNIS_UI_QUIT
					PROCESS_TENNIS_MP_QUIT_UI(thisProps, thisUIData, eSelfID, serverBD, clientBD, serverBD.playerScores, GET_TENNIS_MP_CURRENT_SET(serverBD), IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_IS_REMATCH_FLAG), GET_TENNIS_MP_NUM_SETS(serverBD), sParticipantInfo)
				ENDIF
				
				UPDATE_TENNIS_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext)
				IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
				AND NOT bGamePaused
					ALLOW_TENNIS_PLAYER_CONTROL(TA_MULTIPLAYER, FALSE)
					SET_TENNIS_UI_STATE(thisUIData, TENNIS_UI_QUIT)
					CPRINTLN(DEBUG_TENNIS, "Quit Button Press Detected from TMPS_WAIT_FOR_PREPARE_REMATCH")
				ENDIF
				
				//Waiting in this state after trying to move on to the next state
				IF IS_TENNIS_SERVER_FLAG_SET(serverBD, TMS_MOVE_TO_PREPARE_REMATCH)
					SET_LOADING_ICON_INACTIVE()
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_RUNNING)
					UPDATE_TENNIS_UI_POINTS(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), sParticipantInfo)
					TENNIS_MP_BROADCAST_SCTV_UI_UPDATE_UI_POINTS(FALSE)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SWITCHED_TO_RUNNING)
					CLEAR_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SHOW_QUIT_LEADERBOARD)
					READY_TENNIS_MP_FOR_REMATCH(thisProps, thisUIData, serverBD, eSelfID, eOtherID, servingPlayer)
					servingPlayer = GET_TENNIS_MP_WHO_IS_SERVING(serverBD)
					HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
					SET_TENNIS_TIMER_A(thisProps, 0)
					CPRINTLN(DEBUG_TENNIS, "Moving from TMPS_WAIT_FOR_PREPARE_REMATCH back to running")
				ENDIF
			BREAK
			
			CASE TMPS_NO_REMATCH	// deprecated
				TENNIS_MP_WAIT_STATE_UPDATES(thisProps, thisUIData, serverBD, eSelfID, eOtherID, sParticipantInfo, FALSE, FALSE, IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_WALKING_TO_POS))
				
				IF GET_TENNIS_TIMER_A(thisProps) > SPLASH_UI_DURATION
					CPRINTLN(DEBUG_TENNIS, "Quitting :: GET_TENNIS_TIMER_A(thisProps) > SPLASH_UI_DURATION")
					SET_TENNIS_CLIENT_MP_STATE(clientBD[iMyParticipantID], TMPS_QUIT_THE_GAME)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SHOW_QUIT_LEADERBOARD)
				ENDIF
			BREAK
			
			CASE TMPS_QUIT_THE_GAME
				
				IF NOT HAS_TENNIS_LEADERBOARD_CAM_BEEN_REQUESTED()
					REQUEST_TENNIS_LEADERBOARD_CAM()
				ENDIF
				
				// Handle the splash UI
				IF NOT IS_TENNIS_SPLASH_UI_HIDDEN(thisUIData.tennisUI)
					IF HAS_SCALEFORM_MOVIE_LOADED(thisUIData.tennisUI.uiShardMS.siMovie)
						UPDATE_SHARD_BIG_MESSAGE(thisUIData.tennisUI.uiShardMS)
					ENDIF
				ELSE
					CDEBUG2LN(DEBUG_TENNIS, "CASE TMPS_QUIT_THE_GAME IS_TENNIS_SPLASH_UI_HIDDEN(thisUIData.tennisUI) is True")
				ENDIF
				// Get the player out of tennis mode
				IF IS_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID])) AND IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), "mini@tennis", "ready_2_idle")
					SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), FALSE)
					SET_PED_WEAPON_MOVEMENT_CLIPSET(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), "weapons@tennis@male")
				ENDIF
				// Show the scoreboard before quitting
				IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SHOW_QUIT_SCOREBOARD) AND NOT IS_TRANSITION_ACTIVE()
					IF NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SHOW_QUIT_LEADERBOARD)
						UPDATE_TENNIS_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext)
					ENDIF
					DRAW_TENNIS_SCALEFORM_UI(thisUIData.tennisUI.scorePanelSF)
					IF ( IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) )
					AND NOT bGamePaused
						CPRINTLN(DEBUG_TENNIS, "IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SHOW_QUIT_SCOREBOARD) button pressed")
						CLEAR_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SHOW_QUIT_SCOREBOARD)
						SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SHOW_QUIT_LEADERBOARD)
					ENDIF
				ENDIF
				// Show the tennis scoreboard
				IF ((GET_TENNIS_TIMER_A(thisProps) > 4000) OR ( GET_TENNIS_TIMER_A(thisProps) > 2000 AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) AND NOT bGamePaused ) )
				AND NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SHOW_QUIT_LEADERBOARD)
					CPRINTLN(DEBUG_TENNIS, "NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SHOW_QUIT_LEADERBOARD) showing scoreboard")
					DISPLAY_TENNIS_SPLASH_UI(thisUIData.tennisUI, FALSE)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SHOW_QUIT_SCOREBOARD)
					SHOW_TENNIS_SCOREBOARD(thisUIData.tennisUI)
					IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_MP_QUIT_BUTTON_SET)
						SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_MP_QUIT_BUTTON_SET)
						INIT_SIMPLE_USE_CONTEXT(thisUIData.tennisUI.useContext, FALSE, FALSE, FALSE, TRUE)
						ADD_SIMPLE_USE_CONTEXT_INPUT(thisUIData.tennisUI.useContext, "FINISH", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					ENDIF
				ENDIF
				// Request the leaderboard cam and skedaddle
				IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SHOW_QUIT_LEADERBOARD) AND SET_SKYSWOOP_UP()	//IS_SWITCH_TO_MULTI_FIRSTPART_FINISHED()
					IF NOT HAS_TENNIS_LEADERBOARD_CAM_BEEN_REQUESTED()
						REQUEST_TENNIS_LEADERBOARD_CAM()
					ENDIF
					
					// If the screen is done fading out before we quit
					IF IS_TENNIS_LEADERBOARD_CAM_READY()
						HIDE_TENNIS_PLAYER_CARD_AND_SCOREBOARD(thisUIData.tennisUI)
						CPRINTLN(DEBUG_TENNIS, "TMPS_QUIT_THE_GAME: Game over, yeeeeaaahhh!")
						TEXT_LABEL_23 texContentID
						texContentID = ""
						IF GET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[iMyParticipantID]) = AR_playerWon AND NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_MATCH_IS_OVER)
							INCREASE_TENNIS_MP_STAT(TMST_TENNIS_MATCHES_WON)
							SET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_WON, GET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_WON) + 1)
							INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_TENNIS_WON, 1)
							CDEBUG2LN(DEBUG_TENNIS, "Player won but match isn't over, we were quit on. Mark MP_AWARD_FM_TENNIS_WON and MPPLY_TENNIS_MATCHES_WON and TMST_TENNIS_MATCHES_WON")
						ELIF GET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[iMyParticipantID]) = AR_buddyA_won AND NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_MATCH_IS_OVER)
							// increment our losing stats
							INCREASE_TENNIS_MP_STAT(TMST_TENNIS_MATCHES_LOST)
							SET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_LOST, GET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_LOST) + 1)
							CDEBUG2LN(DEBUG_TENNIS, "Match isn't over but we've quit and lost the match.")
						ENDIF
						IF IS_TENNIS_FLOW_FLAG_SET(thisProps, TFF_CONTINUE_AFTER_VOTE_PASSING)
							SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, texContentID)
						ELSE
							SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_QUIT, <<0.0, 0.0, 0.0>>, texContentID)
						ENDIF
						WRITE_TENNIS_MP_SCLB_DATA()
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						SET_LOADING_ICON_INACTIVE()
						TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, GET_TENNIS_CLIENT_ACTIVITY_RESULT(clientBD[iMyParticipantID]), serverBD.iMatchHistoryID, missionScriptArgs.mdID.idVariation)
					ENDIF
				ELIF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SHOW_QUIT_LEADERBOARD) AND NOT IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SKYSWOOP_STARTED)
					SET_TENNIS_CLIENT_FLAG(clientBD[iMyParticipantID], TMC_SKYSWOOP_STARTED)
					SET_SKYSWOOP_UP()
				ENDIF
			BREAK
			
			// Main running state.
			CASE TMPS_RUNNING
				PROCESS_MP_CLIENT_GAME_STATE(thisProps, thisUIData, serverBD, clientBD, eSelfID, eOtherID, bGamePaused, missionScriptArgs.mdID.idVariation, sParticipantInfo)
			BREAK
			
			CASE TMPS_SCTV
			#IF IS_DEBUG_BUILD
				CLIENT_SERVER_DEBUG_DISPLAY()
			#ENDIF
				IF IS_SCREEN_FADED_OUT() AND GET_TENNIS_CLIENT_GAME_STATE(clientBD[PARTICIPANT_ID_TO_INT()]) > TGSE_OPTION_FINISHED
					SHOW_PLAYER_CARD_NO_MATTER_WHAT(thisUIData, GET_TENNIS_MP_WHO_IS_SERVING(serverBD), serverBD.playerScores, GET_TENNIS_MP_NUM_SETS(serverBD), GET_TENNIS_MP_CURRENT_SET(serverBD), eSelfID, eOtherID, IS_TENNIS_MP_ON_TIE_BREAK(serverBD), sParticipantInfo, FALSE, FALSE)
					DO_SCREEN_FADE_IN(500)
				ENDIF
				IF VALIDATE_TENNIS_MODE_ON_OPPONENT_FOR_SCTV(thisProps, serverBD, clientBD, TENNIS_PLAYER_HOME, sParticipantInfo, iMyParticipantID)
					IF NOT IS_PED_INJURED(thisProps.tennisPlayers[TENNIS_PLAYER_HOME].pIndex) AND NOT IS_TENNIS_MODE( thisProps.tennisPlayers[TENNIS_PLAYER_HOME].pIndex )
						SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[TENNIS_PLAYER_HOME]),TRUE)
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(thisProps.tennisPlayers[TENNIS_PLAYER_HOME].pIndex) AND IS_TENNIS_MODE( thisProps.tennisPlayers[TENNIS_PLAYER_HOME].pIndex )
						SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[TENNIS_PLAYER_HOME]),FALSE)
					ENDIF
				ENDIF
				
				IF VALIDATE_TENNIS_MODE_ON_OPPONENT_FOR_SCTV(thisProps, serverBD, clientBD, TENNIS_PLAYER_AWAY, sParticipantInfo, iMyParticipantID)
					IF NOT IS_PED_INJURED( thisProps.tennisPlayers[TENNIS_PLAYER_AWAY].pIndex ) AND NOT IS_TENNIS_MODE( thisProps.tennisPlayers[TENNIS_PLAYER_AWAY].pIndex )
						SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[TENNIS_PLAYER_AWAY]),TRUE)
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED( thisProps.tennisPlayers[TENNIS_PLAYER_AWAY].pIndex ) AND IS_TENNIS_MODE( thisProps.tennisPlayers[TENNIS_PLAYER_AWAY].pIndex )
						SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[TENNIS_PLAYER_AWAY]),FALSE)
					ENDIF
				ENDIF
			
				PROCESS_TENNIS_SCTV_GAME_STATE(thisProps, thisUIData, serverBD, clientBD, eSelfID, eOtherID, missionScriptArgs.mdID.idVariation, sParticipantInfo)
			BREAK
			
		ENDSWITCH
		
		// Server state machine
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()	
			// GENERAL UPDATE
			
			NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
			
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
					IF GET_TENNIS_SERVER_GAME_STATE(serverBD) <> TGSE_TEST_STATE
						SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_TEST_STATE)
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						CPRINTLN(DEBUG_TENNIS, "ENTERING TEST STATE")
					ELSE
						SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_RALLY)
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
						CPRINTLN(DEBUG_TENNIS, "EXITING TEST STATE")
					ENDIF
				ENDIF
			#ENDIF
			
			SWITCH GET_TENNIS_SERVER_MP_STATE(serverBD)
				CASE TMPS_INIT
					// Wait for another participant.
					IF (sParticipantInfo.iPlayerCount >= ENUM_TO_INT(TENNIS_PLAYERS))
						// Update info until we get two players
						UPDATE_TENNIS_PARTICIPANT_INFO(sParticipantInfo)
						SET_TENNIS_SERVER_MP_STATE(serverBD, TMPS_SETUP)
						fWaitTimeOut = 0.0
						CPRINTLN(DEBUG_TENNIS, "s_TMPS_INIT: All Players found")
					ELSE
						CPRINTLN(DEBUG_TENNIS, "s_TMPS_INIT: Server waiting for players")
					ENDIF
				BREAK
				
				CASE TMPS_SETUP
					IF IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SYNC_COMPLETE)
					AND VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
					AND IS_TENNIS_CLIENT_FLAG_SET(clientBD[sParticipantInfo.iPlayers[eOtherID]], TMC_SYNC_COMPLETE)
						SET_TENNIS_SERVER_MP_STATE(serverBD, TMPS_RUNNING)
						SET_TENNIS_SERVER_GAME_STATE(serverBD, TGSE_OPTION_MENU)
					ELIF fWaitTimeOut > WAITING_FOR_PLAYERS_TIMEOUT
						CPRINTLN(DEBUG_TENNIS, "TMPS_SETUP :: Quitting because our waiting for other players timed out.")
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
						TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, AR_playerQuit, serverBD.iMatchHistoryID, missionScriptArgs.mdID.idVariation)
					ENDIF
					
					fWaitTimeOut += GET_FRAME_TIME()
					
					CPRINTLN(DEBUG_TENNIS, PICK_STRING(IS_TENNIS_CLIENT_FLAG_SET(clientBD[iMyParticipantID], TMC_SYNC_COMPLETE),"s_TMPS_SETUP: eSelfID has synced","s_TMPS_SETUP: eSelfID has NOT synced"))
					IF VALIDATE_TENNIS_PLAYERS_INFO(sParticipantInfo)
						CPRINTLN(DEBUG_TENNIS, PICK_STRING(IS_TENNIS_CLIENT_FLAG_SET(clientBD[sParticipantInfo.iPlayers[eOtherID]], TMC_SYNC_COMPLETE),"s_TMPS_SETUP: eOtherID has synced","s_TMPS_SETUP: eOtherID has NOT synced"))
					ENDIF
				BREAK
				
				CASE TMPS_RUNNING
					
					HANDLE_TENNIS_SERVER_BITMASKS(serverBD, clientBD, sParticipantInfo)
										
					IF UPDATE_TENNIS_MP_GAME(serverBD, clientBD, eSelfID, eOtherID, sParticipantInfo)
						//The game is running fine
					ELSE
						//A desync occured for too long
					ENDIF
				BREAK
			ENDSWITCH
			
			//GENERAL UPDATE FINISHED
		ENDIF
		
		//Counts the frames done, hence the last thing we do before moving on
		INCREASE_TENNIS_FRAME_COUNTER(thisProps.iFrameCounter)
		
	ENDWHILE
ENDSCRIPT
