USING "rage_builtins.sch"
USING "globals.sch"
USING "script_player.sch"
USING "script_camera.sch"

USING "tennis_cameras.sch"
USING "tennis.sch"
USING "tennis_helpers_lib.sch"
USING "tennis_player_lib.sch"
USING "tennis_ui.sch"
USING "tennis_ui_lib.sch"

USING "net_betting_common.sch"
	
PROC SET_TENNIS_FAR_CAM_MULTS(TENNIS_CAMERAS &thisCam, PLAYING_COURT thisCourt)
	#IF IS_DEBUG_BUILD
		IF ADJUST_DYNA_CAM_OFF_MICHAEL_VARS
			thisCourt = TENNIS_COURT_MichaelHouse
		ENDIF
	#ENDIF
	// Prime the values as Michael's house vars, some courts don't change some of these
	thisCam.fFarCamUp = FAR_CAM_UP_MICHAEL
	thisCam.fFarCamLookAt = FAR_CAM_LOOK_MICHAEL
	thisCam.fFarCamForward = FAR_CAM_FORWARD_MICHAEL
	thisCam.fFOV = FAR_CAM_FOV_MICHAEL
	
	IF thisCourt = TENNIS_COURT_CountryClub
		thisCam.fFarCamUp = FAR_CAM_UP_VENICE
		thisCam.fFarCamLookAt = FAR_CAM_LOOK_VENICE
		thisCam.fFarCamForward = FAR_CAM_FORWARD_VENICE
		thisCam.fFOV = FAR_CAM_FOV_VENICE
	ELIF thisCourt = TENNIS_COURT_VespucciHotel
		thisCam.fFarCamUp = FAR_CAM_UP_VeH
		thisCam.fFarCamLookAt = FAR_CAM_LOOK_VeH
		thisCam.fFarCamForward = FAR_CAM_FORWARD_VeH
		thisCam.fFOV = FAR_CAM_FOV_VeH
	ELIF thisCourt = TENNIS_COURT_RichmanHotel1
		thisCam.fFarCamUp = FAR_CAM_UP_RICHMAN
		thisCam.fFarCamLookAt = FAR_CAM_LOOK_RICHMAN
		thisCam.fFarCamForward = FAR_CAM_FORWARD_RICHMAN
		thisCam.fFOV = FAR_CAM_FOV_RICHMAN
	ELIF thisCourt = TENNIS_COURT_LSUCourt1
		thisCam.fFarCamUp = FAR_CAM_UP_LSU
		thisCam.fFarCamLookAt = FAR_CAM_LOOK_LSU
		thisCam.fFarCamForward = FAR_CAM_FORWARD_LSU
		thisCam.fFOV = FAR_CAM_FOV_LSU
	ELIF thisCourt = TENNIS_COURT_WeazelCourt1
		thisCam.fFarCamUp = FAR_CAM_UP_WEAZEL
		thisCam.fFarCamLookAt = FAR_CAM_LOOK_WEAZEL
		thisCam.fFarCamForward = FAR_CAM_FORWARD_WEAZEL
		thisCam.fFOV = FAR_CAM_FOV_WEAZEL
	ELIF thisCourt = TENNIS_COURT_chumashHotel
		thisCam.fFarCamUp = FAR_CAM_UP_CHUMASH
		thisCam.fFarCamLookAt = FAR_CAM_LOOK_CHUMASH
		thisCam.fFarCamForward = FAR_CAM_FORWARD_CHUMASH
		thisCam.fFOV = FAR_CAM_FOV_CHUMASH
	ELIF thisCourt = TENNIS_COURT_VinewoodHotel1
		thisCam.fFarCamUp = FAR_CAM_UP_VINE
		thisCam.fFarCamLookAt = FAR_CAM_LOOK_VINE
		thisCam.fFarCamForward = FAR_CAM_FORWARD_VINE
		thisCam.fFOV = FAR_CAM_FOV_VINE
	ENDIF
ENDPROC

PROC TENNIS_SET_GAMEPLAY_CAM_WORLD_HEADING(FLOAT fHeading = 0.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHeading - GET_ENTITY_HEADING(PLAYER_PED_ID()))
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisCam - 
///    vForward - forward vector of the court
///    vCenterCourt - 
PROC SET_TENNIS_CAMERA_MP_CHEAT(TENNIS_CAMERAS &thisCam, VECTOR &vForward, VECTOR &vCenterCourt)
	FLOAT fHeading = GET_HEADING_FROM_VECTOR_2D(vForward.x, vForward.y) + 27
	VECTOR vPos = vCenterCourt + (<<0,0,10>>)
	VECTOR vHeading = (<<-15, 0, fHeading>>)
	SET_CAM_PARAMS(thisCam.tennisCamTut, vPos, vHeading, 45.0)
ENDPROC

PROC TENNIS_ACTIVATE_SIDE_CHANGE_CAMERA_SP(TENNIS_COURT &thisCourt, TENNIS_CAMERAS &thisCam, TENNIS_PLAYER_ID newCourtSide)
	CPRINTLN(DEBUG_TENNIS, "TENNIS_ACTIVATE_SIDE_CHANGE_CAMERA called")
	FLOAT fHeading = GET_HEADING_FROM_VECTOR_2D(thisCourt.vForwardNorm.x, thisCourt.vForwardNorm.y)
	IF newCourtSide = TENNIS_PLAYER_AWAY		fHeading += 180		ENDIF
	VECTOR vStartPos = ROTATE_VECTOR_ABOUT_Z(<< 1.51628, -8.88086, 1.049 >>, fHeading) + thisCourt.vCenterCourt
	VECTOR vEndPos = ROTATE_VECTOR_ABOUT_Z(<< 1.36811, -7.97335, 1.0391 >>, fHeading) + thisCourt.vCenterCourt
	fHeading += -350.722870
	
	VECTOR vRot = (<<-0.6204, -0.0000, fHeading>>)
	
	IF DOES_CAM_EXIST(thisCam.tennisCamTut)
		DESTROY_CAM(thisCam.tennisCamTut)
	ENDIF
	
	thisCam.tennisCamTut = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
	
//	IF IS_CAM_SHAKING(thisCam.tennisCamTut)
//		STOP_CAM_SHAKING(thisCam.tennisCamTut, TRUE)
//	ENDIF
	
//	STOP_CAM_POINTING(thisCam.tennisCamTut)
	//SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
	SET_CAM_PARAMS(thisCam.tennisCamTut, vStartPos, vRot, 30.0)
	SET_CAM_PARAMS(thisCam.tennisCamTut, vEndPos, vRot, 30.0, SIDE_CHANGE_LENGTH)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	IF IS_TIMER_STARTED(thisCam.tennisCamTimer)
		RESTART_TIMER_NOW(thisCam.tennisCamTimer)
	ELSE
		START_TIMER_NOW_SAFE(thisCam.tennisCamTimer)
	ENDIF
ENDPROC

PROC UPDATE_BASELINE_CAM_TRACKING(TENNIS_GAME_PROPERTIES& thisProps, TENNIS_GAME_UI_DATA& thisUIData, TENNIS_PLAYER_ID eSelfID, CAMERA_INDEX camBaseline, VECTOR vLeftNearCorner, VECTOR vRightNearCorner, VECTOR vLeftFarCorner, FLOAT fLerpStrength = 0.5)
	VECTOR vBaselineCenter, vBaselineDir, vBackwards, vCameraRight, vPlayerPos, vOldCamPos, vDesiredCamPos
	FLOAT fPlayerDot
	
	fLerpStrength = 0.0
	
	// If it's been a while since our last update, we should hard-set to the desired position.
//	IF (NOT IS_TIMER_STARTED(thisUIData.trackingTimer)) OR GET_TIMER_IN_SECONDS(thisUIData.trackingTimer) > 0.3
//		fLerpStrength = 1.0
//	ENDIF
	
	IF IS_CAM_ACTIVE(camBaseline)
		RESTART_TIMER_NOW(thisUIData.trackingTimer)
	ENDIF
	
	vBaselineCenter = vLeftNearCorner
	vBaselineDir = vRightNearCorner - vBaselineCenter
	vBackwards = vBaselineCenter - vLeftFarCorner
	
	vBackwards.z = 0.0
	vBaselineDir.z = 0.0
	vBaselineCenter.z = 0.0
	vBaselineCenter += 0.5 * vBaselineDir
	vBackwards = NORMALISE_VECTOR(vBackwards)
	vCameraRight = NORMALISE_VECTOR(vBaselineDir)
	
	vPlayerPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
	vPlayerPos.z = 0.0
	fPlayerDot = DOT_PRODUCT(vPlayerPos, vCameraRight) - DOT_PRODUCT(vBaselineCenter, vCameraRight)
	IF GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt) = TENNIS_COURT_VinewoodHotel1 AND thisProps.tennisPlayers[eSelfID].eCourtSide = TENNIS_PLAYER_HOME
//		CDEBUG2LN(DEBUG_TENNIS, "UPDATE_BASELINE_CAM_TRACKING :: Clamping fPlayerDot, ", thisProps.tennisPlayers[eSelfID].playerAI.texName, " eCourtSide=", GET_STRING_FROM_TENNIS_PLAYER_ID(thisProps.tennisPlayers[eSelfID].eCourtSide))
		fPlayerDot = CLAMP(fPlayerDot, -4.07, 4.07)
	ENDIF
	
	vOldCamPos = GET_CAM_COORD(camBaseline)
	
	vDesiredCamPos = vBaselineCenter + 0.5 * fPlayerDot * vCameraRight + thisUIData.fFarCamBaseOffset * vBackwards
	vDesiredCamPos.z = vOldCamPos.z
	
//	CDEBUG2LN(DEBUG_TENNIS, "UPDATE_BASELINE_CAM_TRACKING :: fPlayerDot=", fPlayerDot)
	
//	IF fLerpStrength = 1.0
//		CERRORLN(DEBUG_MISSION, "fLerpStrength = ", fLerpStrength)
//		CERRORLN(DEBUG_MISSION, "vOldCamPos = ", vOldCamPos)
//		CERRORLN(DEBUG_MISSION, "vDesiredCamPos = ", vDesiredCamPos)
//	ENDIF
	
	vDesiredCamPos = LERP_VECTOR(vOldCamPos, vDesiredCamPos, fLerpStrength)
	
//	SET_CAM_COORD(camBaseline, vDesiredCamPos)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisCourt - 
///    vForward - 
///    vRight - 
///    vUp - 
PROC SET_FAR_CAM(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eSelfID)
	VECTOR vCamCoord = thisProps.tennisCourt.vCourtCorners[0] + thisProps.vRight*0.5 + thisProps.vForward*-thisUIData.tennisCameras.fFarCamForward + thisProps.vUp*thisUIData.tennisCameras.fFarCamUp
	VECTOR vLookCoord = thisProps.tennisCourt.vCourtCorners[0] + thisProps.vRight*0.5 + thisProps.vForward*thisUIData.tennisCameras.fFarCamLookAt
	VECTOR vLookRot
	
	vLookRot.x = ATAN2(vLookCoord.z - vCamCoord.z, GET_DISTANCE_BETWEEN_COORDS(vLookCoord, vCamCoord, FALSE))
	vLookRot.z = ATAN2(vLookCoord.y - vCamCoord.y, vLookCoord.x - vCamCoord.x) - 90.0
	
	SET_CAM_ROT(thisUIData.tennisCameras.tennisCamFar, vLookRot)
	SET_CAM_FOV(thisUIData.tennisCameras.tennisCamFar, thisUIData.tennisCameras.fFOV)
	
	VECTOR vBackwards = NORMALISE_VECTOR(thisProps.tennisCourt.vCourtCorners[0] - thisProps.tennisCourt.vCourtCorners[3])
	thisUIData.fFarCamBaseOffset = DOT_PRODUCT(vCamCoord, vBackwards) - DOT_PRODUCT(thisProps.tennisCourt.vCourtCorners[0], vBackwards)
	
//	DRAW_DEBUG_SPHERE(vCamCoord, 0.5)
	SET_CAM_COORD(thisUIData.tennisCameras.tennisCamFar, vCamCoord)
	UPDATE_BASELINE_CAM_TRACKING(thisProps, thisUIData, eSelfID, thisUIData.tennisCameras.tennisCamFar, thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisCourt.vCourtCorners[1], thisProps.tennisCourt.vCourtCorners[3], 1.0)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisCourt - 
///    vForward - 
///    vRight - 
///    vUp - 
PROC SET_FAR_CAM_OTHER_SIDE(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eSelfID)
	VECTOR vCamCoord = thisProps.tennisCourt.vCourtCorners[2] - thisProps.vRight*0.5 - thisProps.vForward*-thisUIData.tennisCameras.fFarCamForward + thisProps.vUp*thisUIData.tennisCameras.fFarCamUp
	VECTOR vLookCoord = thisProps.tennisCourt.vCourtCorners[2] - thisProps.vRight*0.5 - thisProps.vForward*thisUIData.tennisCameras.fFarCamLookAt
	VECTOR vLookRot
	
	vLookRot.x = ATAN2(vLookCoord.z - vCamCoord.z, GET_DISTANCE_BETWEEN_COORDS(vLookCoord, vCamCoord, FALSE))
	vLookRot.z = ATAN2(vLookCoord.y - vCamCoord.y, vLookCoord.x - vCamCoord.x) - 90.0
	
	SET_CAM_ROT(thisUIData.tennisCameras.tennisCamFarOtherSide, vLookRot)
	SET_CAM_FOV(thisUIData.tennisCameras.tennisCamFarOtherSide, thisUIData.tennisCameras.fFOV)
	
	VECTOR vBackwards = NORMALISE_VECTOR(thisProps.tennisCourt.vCourtCorners[3] - thisProps.tennisCourt.vCourtCorners[0])
	thisUIData.fFarCamOtherSideBaseOffset = DOT_PRODUCT(vCamCoord, vBackwards) - DOT_PRODUCT(thisProps.tennisCourt.vCourtCorners[3], vBackwards)
	
	SET_CAM_COORD(thisUIData.tennisCameras.tennisCamFarOtherSide, vCamCoord)
	UPDATE_BASELINE_CAM_TRACKING(thisProps, thisUIData, eSelfID, thisUIData.tennisCameras.tennisCamFarOtherSide, thisProps.tennisCourt.vCourtCorners[2], thisProps.tennisCourt.vCourtCorners[3], thisProps.tennisCourt.vCourtCorners[1], 1.0)
ENDPROC

PROC GET_BASE_TENNIS_DYNA_CAM_VALUES(PLAYING_COURT thisCourt, FLOAT &zeeMax, FLOAT &whyMax, FLOAT &fFOVMax, FLOAT &fLookAtMax, FLOAT &fMaxDistToNet, FLOAT &fZeeTowardMin, FLOAT &fDynaMoverFOVMin, FLOAT &fLookAtMin)
	// LOOK_AT_TOWARD
	SWITCH thisCourt
		CASE TENNIS_COURT_VespucciHotel		fLookAtMax = LOOK_AT_TOWARD_MAX_VeH		BREAK
		CASE TENNIS_COURT_CountryClub		fLookAtMax = LOOK_AT_TOWARD_VENICE_MAX	BREAK
		CASE TENNIS_COURT_RichmanHotel1		fLookAtMax = LOOK_AT_TOWARD_RICHMAN		BREAK
		CASE TENNIS_COURT_LSUCourt1			fLookAtMax = LOOK_AT_TOWARD_LSU			BREAK	
		CASE TENNIS_COURT_chumashHotel		fLookAtMax = LOOK_AT_TOWARD_CHUMASH		BREAK
		CASE TENNIS_COURT_VinewoodHotel1	fLookAtMax = LOOK_AT_TOWARD_VINE		BREAK
		DEFAULT								fLookAtMax = LOOK_AT_TOWARD_MIKE_MAX	BREAK
	ENDSWITCH
	// WHY_TOWARD_MAX
	SWITCH thisCourt
		CASE TENNIS_COURT_VespucciHotel		whyMax = WHY_TOWARD_MAX_VeH		BREAK
		CASE TENNIS_COURT_CountryClub		whyMax = WHY_TOWARD_MAX_VENICE	BREAK
		CASE TENNIS_COURT_RichmanHotel1		whyMax = WHY_TOWARD_MAX_RICHMAN	BREAK
		CASE TENNIS_COURT_LSUCourt1			whyMax = WHY_TOWARD_MAX_LSU		BREAK
		CASE TENNIS_COURT_chumashHotel		whyMax = WHY_TOWARD_MAX_CHUMASH	BREAK
		CASE TENNIS_COURT_VinewoodHotel1	whyMax = WHY_TOWARD_MAX_VINE	BREAK
		DEFAULT								whyMax = WHY_TOWARD_MAX_MICHAEL	BREAK
	ENDSWITCH
	// ZEE_TOWARD_MAX
	SWITCH thisCourt
		CASE TENNIS_COURT_VespucciHotel		zeeMax = ZEE_TOWARD_MAX_VeH		BREAK
		CASE TENNIS_COURT_CountryClub		zeeMax = ZEE_TOWARD_MAX_VENICE	BREAK
		DEFAULT								zeeMax = ZEE_TOWARD_MAX_MICHAEL	BREAK
	ENDSWITCH
	// ZEE_TOWARD_MIN
	SWITCH thisCourt
		CASE TENNIS_COURT_CountryClub		fZeeTowardMin = ZEE_TOWARD_MIN_VENICE	BREAK
		DEFAULT								fZeeTowardMin = ZEE_TOWARD_MIN			BREAK
	ENDSWITCH
	// DYNAMOVER_FOV_MAX
	SWITCH thisCourt
		CASE TENNIS_COURT_VespucciHotel		fFOVMax = DYNAMOVER_FOV_MAX_VeH		BREAK
		CASE TENNIS_COURT_CountryClub		fFOVMax = DYNAMOVER_FOV_VENICE_MAX	BREAK
		CASE TENNIS_COURT_RichmanHotel1		fFOVMax = DYNAMOVER_FOV_MAX_RICHMAN	BREAK
		CASE TENNIS_COURT_LSUCourt1			fFOVMax = DYNAMOVER_FOV_MAX_LSU		BREAK
		CASE TENNIS_COURT_chumashHotel		fFOVMax = DYNAMOVER_FOV_MAX_CHUMASH	BREAK
		CASE TENNIS_COURT_VinewoodHotel1	fFOVMax = DYNAMOVER_FOV_MAX_VINE	BREAK
		DEFAULT								fFOVMax = DYNAMOVER_FOV_MIKE_MAX	BREAK
	ENDSWITCH
	// MAX_DIST_TO_NET
	SWITCH thisCourt
		CASE TENNIS_COURT_VespucciHotel		fMaxDistToNet = MAX_DIST_TO_NET		BREAK
		DEFAULT								fMaxDistToNet = MAX_DIST_TO_NET		BREAK
	ENDSWITCH
	// DYNAMOVER_FOV_MIN
	fDynaMoverFOVMin = DYNAMOVER_FOV_MIN
	// LOOK_AT_TOWARD_MIN
	fLookAtMin = LOOK_AT_TOWARD_MIN
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisCams - 
///    vAnchor - 
///    vRight - 
///    vForward - 
///    fRight - thisUIData.fDynaCamSwing
///    fForward - fY
///    fUp - fZ
///    fLook - fLook
PROC ADJUST_TENNIS_CAMERA_DYNAMIC(CAMERA_INDEX &dynaCam, PLAYING_COURT thisCourt, VECTOR vAnchor, VECTOR vRight, VECTOR vForward, VECTOR vCenterToBall, FLOAT &fRight, FLOAT fDistToCenterCourt, FLOAT &fDynaDolly, FLOAT &fNudgeRSLR, FLOAT &fNudgeRSUD, BOOL bIsLobIncoming=FALSE, BOOL bDefaults=FALSE)
	#IF IS_DEBUG_BUILD
		IF ADJUST_DYNA_CAM_OFF_MICHAEL_VARS
			thisCourt = TENNIS_COURT_MichaelHouse
		ENDIF
	#ENDIF
//	CDEBUG2LN(DEBUG_TENNIS, "ADJUST_TENNIS_CAMERA_DYNAMIC :: bDefaults=", PICK_STRING(bDefaults, "TRUE", "FALSE"))
	//Used for offsets and values for dynamic camera
	FLOAT fDist, fZ, fLook, fFov, fCurrentFOV, fX, fLerpDest, fNudgeX, fNudgeY, fNudgeDest, fDollyTarget
	FLOAT zeeMax, whyMax, fFOVMax, fLookAtMax, fMaxDistToNet, fZeeTowardMin, fDynaMoverFOVMin, fLookAtMin
	VECTOR vDesiredPos, vCurrentCamPos, vUp
	INT iLR, iUD, iDummy
	GET_BASE_TENNIS_DYNA_CAM_VALUES(thisCourt, zeeMax, whyMax, fFOVMax, fLookAtMax, fMaxDistToNet, fZeeTowardMin, fDynaMoverFOVMin, fLookAtMin)
	
	GET_ANALOG_STICK_VALUES(iDummy, iDummy, iLR, iUD)
	
	// Disable camera movement for PC mouse. It doesn't work well on mouse, causes conflicts with serving, and also the camera movement isn't essential. B* 1882367
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		iLR = 0
		iUD = 0
	ENDIF
	
	IF iLR > 50
		// we want to look right, boost by Const
		fNudgeDest = TO_FLOAT(iLR) / TO_FLOAT(128)
	ELIF iLR < -50
		// we want to look left, boost by -Const
		fNudgeDest = 1 - (TO_FLOAT(iLR) / TO_FLOAT(-128))
	ELSE
		// we want to look center, boost by 0
		fNudgeDest = 0.5
	ENDIF
	fNudgeRSLR = LERP_FLOAT(fNudgeRSLR, fNudgeDest, DYNA_NUDGE_LERP)
	fNudgeX = LERP_FLOAT(-DYNA_NUDGE_RS_LR, DYNA_NUDGE_RS_LR, fNudgeRSLR)
	
	IF iUD > 50
		// we want to look up, boost by -Const
		fNudgeDest = 1 - (TO_FLOAT(iUD) / TO_FLOAT(128))
	ELIF iUD < -50
		// we want to look down, boost by Const
		fNudgeDest = (TO_FLOAT(iUD) / TO_FLOAT(-128))
	ELSE
		// we want to look center, boost by 0
		fNudgeDest = 0.5
	ENDIF
	fNudgeRSUD = LERP_FLOAT(fNudgeRSUD, fNudgeDest, DYNA_NUDGE_LERP)
	fNudgeY = LERP_FLOAT(-DYNA_NUDGE_RS_UD, DYNA_NUDGE_RS_UD, fNudgeRSUD)
	IF IS_LOOK_INVERTED()
		fNudgeY *= -1
	ENDIF
	
	vUp = (<< 0, 0, 1 >>)
	
	fDist = CLAMP(fDistToCenterCourt, MIN_DIST_TO_NET, fMaxDistToNet)	//Clamp the range we want to zoom in and out from
	fDist = (fDist - MIN_DIST_TO_NET) / (fMaxDistToNet - MIN_DIST_TO_NET)	//Convert fDist to an alpha value
	
	IF DOT_PRODUCT(vCenterToBall, vRight) > 0
		//we want to get to 0, swing the camera to the left, looking right
		fLerpDest = 0
	ELSE
		//we want to get to 1, swing the camera to the right, looking left
		fLerpDest = 1
	ENDIF
	//Lerp between our current value and the desired value
	fRight = LERP_FLOAT(fRight, fLerpDest, DYNA_CAM_SWING_LERP)
	fX = LERP_FLOAT(DYNA_CENTER_LINE - DYNA_CAM_SWING_WIDTH, DYNA_CENTER_LINE + DYNA_CAM_SWING_WIDTH, fRight)
	
	IF bIsLobIncoming
		fDynaDolly = LERP_FLOAT(fDynaDolly, 1, DYNA_DOLLY_LERP)
		fDollyTarget = LERP_FLOAT(0, 1, fDynaDolly)
	ELSE
		fDynaDolly= LERP_FLOAT(fDynaDolly, fDist, DYNA_DOLLY_LERP)
		fDollyTarget = LERP_FLOAT(0, 1, fDynaDolly)
	ENDIF
	
	fZ = LERP_FLOAT(fZeeTowardMin, zeeMax, fDollyTarget)
	fLook = LERP_FLOAT(fLookAtMin, fLookAtMax, fDollyTarget)
	fFOV = LERP_FLOAT(fDynaMoverFOVMin, fFOVMax, fDollyTarget)
	
	IF bDefaults
		fRight = fLerpDest
		fX = PICK_FLOAT(fRight = 1, DYNA_CENTER_LINE + DYNA_CAM_SWING_WIDTH, DYNA_CENTER_LINE - DYNA_CAM_SWING_WIDTH)
		fDynaDolly = 1.0
		fDollyTarget = 1.0
		fZ = zeeMax
		fLook = fLookAtMax
		fFOV = fFOVMax
		fNudgeX = 0
		fNudgeY = 0
	ENDIF
	
	vDesiredPos = vAnchor + (vRight * fX) + (vForward * whyMax) + (<<0,0,1>> * fZ)
	vCurrentCamPos = GET_CAM_COORD(dynaCam)
	fCurrentFOV = GET_CAM_FOV(dynaCam)
	
	IF (VDIST2(vDesiredPos, vCurrentCamPos) < DYNA_SNAP_DIST * DYNA_SNAP_DIST AND ABSF(fCurrentFOV - fFOV) < DYNA_SNAP_FOV) AND NOT bDefaults
		SET_CAM_COORD(dynaCam, LERP_VECTOR(vCurrentCamPos, vDesiredPos, DYNA_BOOM_LERP))
		SET_CAM_FOV(dynaCam, LERP_FLOAT(fCurrentFOV, fFOV, DYNA_FOV_LERP))
	ELSE
		SET_CAM_COORD(dynaCam, vDesiredPos)
		SET_CAM_FOV(dynaCam, fFOV)
		CPRINTLN(DEBUG_TENNIS, "Snapping Dynamic Camera in place, ", PICK_STRING(VDIST2(vDesiredPos, vCurrentCamPos) >= DYNA_SNAP_DIST * DYNA_SNAP_DIST, "too far from desired position!", PICK_STRING(ABSF(fCurrentFOV - fFOV) >= DYNA_SNAP_FOV, "difference between FOV was too great!", "bDefaults")))
	ENDIF
	
	POINT_CAM_AT_COORD(dynaCam,	vAnchor + (vRight * (DYNA_CENTER_LINE + fNudgeX)) + (vForward * fLook) + (vUp * fNudgeY))
//	CDEBUG2LN(DEBUG_TENNIS, "vDesiredPos=", vDesiredPos, ", fRight=", fRight, ", fDynaDolly=", fDynaDolly, ", fDollyTarget=", fDollyTarget, ", fZ=", fZ)
//	CDEBUG2LN(DEBUG_TENNIS, "fLook=", fLook, ", fFOV=", fFOV, ", whyMax=", whyMax, ", fNudgeX=", fNudgeX, ", fNudgeY=", fNudgeY, ", vAnchor=", vAnchor)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisCourt - 
///    vForward - 
///    vRight - 
///    vUp - 
///    bCut - 
PROC DO_FAR_CAM(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eSelfID)
	SET_FAR_CAM(thisProps, thisUIData, eSelfID)
	SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamFar,TRUE)
	CPRINTLN(DEBUG_TENNIS, "Setting Far Cam Active")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    bCut - 
PROC DO_FAR_CAM_OTHER_SIDE(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eSelfID)
	SET_FAR_CAM_OTHER_SIDE(thisProps, thisUIData, eSelfID)
	SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamFarOtherSide,TRUE)
	CPRINTLN(DEBUG_TENNIS, "Setting Far Cam Other Side Active")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisProps - 
///    thisCam - 
///    eSelfID - 
PROC SET_DYNA_CAM(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_CAMERAS thisCam, TENNIS_PLAYER_ID eSelfID) 
	INT iAnchor = PICK_INT(eSelfID = (TENNIS_PLAYER_AWAY), 0, 2)
	FLOAT whyMax = PICK_FLOAT(GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt) = TENNIS_COURT_CountryClub, WHY_TOWARD_MAX_VENICE, WHY_TOWARD_MAX_MICHAEL)
	FLOAT zeeMax = PICK_FLOAT(GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt) = TENNIS_COURT_CountryClub, ZEE_TOWARD_MAX_VENICE, ZEE_TOWARD_MAX_MICHAEL)
	
	SET_CAM_COORD(thisCam.tennisCamDyna, (thisProps.tennisCourt.vCourtCorners[iAnchor] + 
										 (thisProps.tennisPlayers[eSelfID].vMyRight * thisProps.tennisPlayers[eSelfID].vMyRight) + 
										 (thisProps.tennisPlayers[eSelfID].vMyForward * whyMax) + 
										 (<<0,0,1>> * zeeMax)))
ENDPROC

/// PURPOSE:
///    
PROC INITIALIZE_TENNIS_CAMERAS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_CAMERAS &thisCam, TENNIS_PLAYER_ID eSelfID)
	SET_TENNIS_FAR_CAM_MULTS(thisUIData.tennisCameras, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt))
	
	thisCam.tennisCamFar = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
	SET_FAR_CAM(thisProps, thisUIData, eSelfID)
	thisCam.tennisCamFarOtherSide = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
	SET_FAR_CAM_OTHER_SIDE(thisProps, thisUIData, eSelfID)
	thisCam.tennisCamDyna = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
	SET_DYNA_CAM(thisProps, thisCam, eSelfID)
	IF NOT DOES_CAM_EXIST(thisCam.tennisCamTut)
		thisCam.tennisCamTut = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
	ENDIF
	
	SET_TENNIS_CAMERA_STATE(thisCam, TENNIS_CAMERA_OPTION, FALSE)
	
	IF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY OR IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
		SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_SET_FRIEND_OUTFIT)
	ENDIF
	
	thisCam.fDynaNudgeRSLR = 0.5	
	thisCam.fDynaNudgeRSUD = 0.5
ENDPROC

/// PURPOSE:
///    Sets a cam active on this frame
/// PARAMS:
///    thisCams - 
///    desiredCam - 
///    bInterp - 
PROC TENNIS_CAMERA_CUT_NOW(TENNIS_CAMERAS &thisCams, CAMERA_STATE_ENUM desiredCam, BOOL bInterp, VECTOR vForward, VECTOR vCenterCourt, BOOL bForceRecreation = FALSE)
	CPRINTLN(DEBUG_TENNIS, "Trying to render script cams now")
	SWITCH desiredCam
		CASE TENNIS_CAMERA_OPTION
			IF bForceRecreation
				IF DOES_CAM_EXIST(thisCams.tennisCamFar)
					DESTROY_CAM(thisCams.tennisCamFar, TRUE)
				ENDIF
				thisCams.tennisCamFar = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			
			ELIF NOT IS_CAM_RENDERING(thisCams.tennisCamFar)
				SET_CAM_ACTIVE(thisCams.tennisCamFar, TRUE)
			ENDIF
		BREAK
		CASE TENNIS_CAMERA_FAR
			IF bForceRecreation
				IF DOES_CAM_EXIST(thisCams.tennisCamFar)
					DESTROY_CAM(thisCams.tennisCamFar, TRUE)
				ENDIF
				thisCams.tennisCamFar = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			
			ELIF NOT IS_CAM_RENDERING(thisCams.tennisCamFar)
				SET_CAM_ACTIVE(thisCams.tennisCamFar, TRUE)
			ENDIF
		BREAK
		CASE TENNIS_CAMERA_FAR_OTHER_SIDE
			IF bForceRecreation
				IF DOES_CAM_EXIST(thisCams.tennisCamFarOtherSide)
					DESTROY_CAM(thisCams.tennisCamFarOtherSide, TRUE)
				ENDIF
				thisCams.tennisCamFarOtherSide = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			
			ELIF NOT IS_CAM_RENDERING(thisCams.tennisCamFarOtherSide)
				SET_CAM_ACTIVE(thisCams.tennisCamFarOtherSide, TRUE)	
			ENDIF
		BREAK
		CASE TENNIS_CAMERA_DYNAMIC
			IF bForceRecreation
				IF DOES_CAM_EXIST(thisCams.tennisCamDyna)
					DESTROY_CAM(thisCams.tennisCamDyna, TRUE)
				ENDIF
				thisCams.tennisCamDyna = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			
			ELIF NOT IS_CAM_RENDERING(thisCams.tennisCamDyna)
				SET_CAM_ACTIVE(thisCams.tennisCamDyna, TRUE)	
			ENDIF
		BREAK
		CASE TENNIS_CAMERA_QUICK_CUT
			IF bForceRecreation
				IF DOES_CAM_EXIST(thisCams.tennisCamTut)
					DESTROY_CAM(thisCams.tennisCamTut, TRUE)
				ENDIF
				thisCams.tennisCamTut = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			
			ELIF NOT IS_CAM_RENDERING(thisCams.tennisCamTut)
				SET_CAM_ACTIVE(thisCams.tennisCamTut, TRUE)
			ENDIF
		BREAK
		CASE TENNIS_CAMERA_SIDE_CHANGE
			IF bForceRecreation
				IF DOES_CAM_EXIST(thisCams.tennisCamTut)
					DESTROY_CAM(thisCams.tennisCamTut, TRUE)
				ENDIF
				thisCams.tennisCamTut = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			
			ELIF NOT IS_CAM_RENDERING(thisCams.tennisCamTut)
				SET_CAM_ACTIVE(thisCams.tennisCamTut, TRUE)
			ENDIF
		BREAK
		CASE TENNIS_CAMERA_MP_CHEAT
			SET_TENNIS_CAMERA_MP_CHEAT(thisCams, vForward, vCenterCourt)
			IF NOT IS_CAM_RENDERING(thisCams.tennisCamTut)
				SET_CAM_ACTIVE(thisCams.tennisCamTut, TRUE)
			ENDIF
		BREAK
		DEFAULT
			CPRINTLN(DEBUG_TENNIS, "Desired Cam: ", desiredCam)
			SCRIPT_ASSERT("Trying to switch to Camera Now and we didn't account for the desired cam! Contact Rob Pearsall.")
		BREAK
	ENDSWITCH
	
	SET_TENNIS_CAMERA_STATE(thisCams, desiredCam, FALSE)
	
	RENDER_SCRIPT_CAMS(TRUE, bInterp)
	
ENDPROC

PROC TENNIS_COMMON_INTRO_INIT_CALLS(TENNIS_CAMERAS &thisCam, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, SERVING_SIDE_ENUM eServeSide, TENNIS_PLAYER_ID eServing, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, BOOL bRematch)
	START_TIMER_NOW_SAFE(thisCam.tennisCamTimer)

	IF TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	//Sets the relative heading of the gameplay camera, prevents the player from rotating during interstitials
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eSelfID].vMyForward.x, thisProps.tennisPlayers[eSelfID].vMyForward.y))
	CPRINTLN(DEBUG_TENNIS, "Setting the Game Cam Heading")
	
	//Put the player and oppponent into the right position
	SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), 
		GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, eServeSide, thisProps.vRight, thisProps.vForward, eSelfID = (eServing), eSelfID))
	thisProps.tennisPlayers[eSelfID].vPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
	SET_ENTITY_HEADING(thisProps.tennisPlayers[eSelfID].pIndex, GET_HEADING_FROM_VECTOR_2D(thisProps.vForward.x,thisProps.vForward.y))
	
	//If it's a rematch we're going to recreate the rackets so let's delete them before that
	IF bRematch
		IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[eSelfID].oTennisRacket)
			DELETE_OBJECT(thisProps.tennisPlayers[eSelfID].oTennisRacket)
		ENDIF
		IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[eOtherID].oTennisRacket)
			DELETE_OBJECT(thisProps.tennisPlayers[eOtherID].oTennisRacket)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[eSelfID].oTennisRacket)
		DELETE_OBJECT(thisProps.tennisPlayers[eSelfID].oTennisRacket)
	ENDIF
	IF DOES_ENTITY_EXIST(thisProps.tennisPlayers[eOtherID].oTennisRacket)
		DELETE_OBJECT(thisProps.tennisPlayers[eOtherID].oTennisRacket)
	ENDIF
	
	thisProps.tennisPlayers[eSelfID].oTennisRacket = CREATE_OBJECT(PROP_TENNIS_RACK_01B, thisProps.tennisCourt.vCourtCorners[3])
	SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eSelfID].oTennisRacket, TRUE)
	thisProps.tennisPlayers[eOtherID].oTennisRacket = CREATE_OBJECT(PROP_TENNIS_RACK_01B, thisProps.tennisCourt.vCourtCorners[2])
	SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eOtherID].oTennisRacket, TRUE)
	
	//Turn gravity off for a ped so she doesn't settle on the teleport
	IF DOES_ENTITY_EXIST(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
		SET_PED_GRAVITY(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), FALSE)
		SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), 
			GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, eServeSide, thisProps.vRight, thisProps.vForward, eOtherID = (eServing), eOtherID), TRUE, TRUE, TRUE)
		thisProps.tennisPlayers[eOtherID].vPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
		SET_ENTITY_HEADING(thisProps.tennisPlayers[eOtherID].pIndex, GET_HEADING_FROM_VECTOR_2D(-thisProps.vForward.x,-thisProps.vForward.y))
		ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eSelfID, eOtherID, DEFAULT, TRUE)
		SET_PED_LOD_MULTIPLIER(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), DEBUG_LOD_SETTING)
	ENDIF
	
	//Remove the weapon from the player
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE,SPC_DEACTIVATE_GADGETS)
//		CLEAR_AREA(thisProps.tennisCourt.vCenterCourt, 50.0, TRUE)
//		CLEAR_AREA_OF_OBJECTS(thisProps.tennisCourt.vCenterCourt, 50.0)
//		CLEAR_AREA_OF_PROJECTILES(thisProps.tennisCourt.vCenterCourt, 50.0)
		REMOVE_DECALS_IN_RANGE(thisProps.tennisCourt.vCenterCourt, 50.0)
		PRINTLN("Clearing Area and resetting wanted level")
	ENDIF
	
	//Sets the peds into their outfits
	SET_TENNIS_OUTFIT(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), thisUIData.playerChars[eSelfID])
	CPRINTLN(DEBUG_TENNIS, "Setting the player's outfit")
	
	//Prime the dynamic camera
	ADJUST_TENNIS_CAMERA_DYNAMIC(thisUIData.tennisCameras.tennisCamDyna, 	// Prime the dynamic camera during intro cutscene
								GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt),
								PICK_VECTOR(thisProps.tennisPlayers[eSelfID].eCourtSide = (TENNIS_PLAYER_AWAY), thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisCourt.vCourtCorners[2]),
								thisProps.tennisPlayers[eSelfID].vMyRight, 
								thisProps.tennisPlayers[eSelfID].vMyForward,
								GET_TENNIS_BALL_POS(thisProps.tennisBall) - thisProps.tennisCourt.vCenterCourt,
								thisUIData.tennisCameras.fDynaCamSwing, DEFAULT_DYNA_DIST,
								thisUIData.tennisCameras.fDynaDolly,
								thisUIData.tennisCameras.fDynaNudgeRSLR, thisUIData.tennisCameras.fDynaNudgeRSUD, FALSE, TRUE)
	SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_NO_DYNA_CAM)
ENDPROC
	
PROC GET_TENNIS_INTRO_CAMERA_VECTORS(PLAYING_COURT eThisCourt, VECTOR &vEstStartPos, VECTOR &vEstStartRot, VECTOR &vEstEndPos, VECTOR &vEstEndRot, VECTOR &vCut1StartPos, VECTOR &vCut1StartRot, VECTOR &vCut1EndPos, VECTOR &vCut1EndRot, VECTOR &vCut2StartPos, VECTOR &vCut2StartRot, VECTOR &vCut2EndPos, VECTOR &vCut2EndRot, VECTOR &vEndCutPos, VECTOR &vEndCutRot)
	IF eThisCourt = TENNIS_COURT_CountryClub
		vEstStartPos	= << -1173.7322, -1613.1334, 14.1354 >>
		vEstStartRot	= << 1.2823, -0.0000, 55.6566 >>
		vEstEndPos		= << -1173.7322, -1613.1334, 14.1354 >>
		vEstEndRot		= << 1.2823, 0.0000, 62.3150 >>
		vCut1StartPos	= << -1161.6476, -1622.2164, 4.8050 >>
		vCut1StartRot	= << -7.4309, 0.0000, -145.3574 >>
		vCut1EndPos		= << -1162.0618, -1622.5024, 4.8050 >>
		vCut1EndRot		= << -7.4309, 0.0000, -145.3574 >>
		vCut2StartPos	= << -1176.4601, -1610.3588, 5.1114 >>
		vCut2StartRot	= << -12.9497, 0.0000, 34.2234 >>
		vCut2EndPos		= << -1176.7467, -1610.5531, 5.1114 >>
		vCut2EndRot		= << -12.9497, 0.0000, 34.2234 >>
		vEndCutPos		= << -1179.1620, -1632.5061, 15.3701 >>
		vEndCutRot		= << -31.7064, -0.0000, -34.0277 >>
	ELIF eThisCourt = TENNIS_COURT_MichaelHouse
		vEstStartPos	= <<-768.9312, 150.2377, 80.6085>>
		vEstStartRot	= <<-1.3848, 0.0000, 111.7951>>
		vEstEndPos		= <<-768.9312, 150.2377, 80.6085>>
		vEstEndRot		= <<-1.3848, 0.0000, 125.2499>>
		vCut1StartPos	= << -769.3076, 143.7017, 67.7890 >>
		vCut1StartRot	= << -4.7504, 0.0000, 179.2149 >>
		vCut1EndPos		= << -769.7811, 143.7082, 67.7890 >>
		vCut1EndRot		= << -4.7504, 0.0000, 179.2149 >>
		vCut2StartPos	= << -774.4316, 162.9025, 67.7334 >>
		vCut2StartRot	= << -4.0931, 0.0000, 0.2899 >>
		vCut2EndPos		= << -774.9694, 162.8998, 67.7334 >>
		vCut2EndRot		= << -4.0931, 0.0000, 0.2899 >>
		vEndCutPos		= << -758.8076, 142.6710, 71.7971 >>
		vEndCutRot		= << -14.3808, -0.0000, 66.8546 >>
	ELIF eThisCourt = TENNIS_COURT_DamVista
		vEstStartPos	= <<-47.8623, 942.2250, 239.2995>>
		vEstStartRot	= <<4.0025, 0.0000, -102.1522>>
		vEstEndPos		= <<-47.8623, 942.2250, 239.2995>>
		vEstEndRot		= <<4.0025, -0.0000, -133.1586>>
		vCut1StartPos	= <<-33.6093, 945.1030, 232.6339>>
		vCut1StartRot	= <<-5.4457, 0.0000, -96.1901>>
		vCut1EndPos		= <<-33.6825, 944.5201, 232.6339>>
		vCut1EndRot		= <<-5.4457, 0.0000, -96.1901>>
		vCut2StartPos	= <<-51.7151, 941.2524, 232.5966>>
		vCut2StartRot	= <<-3.7404, 0.0000, 84.4577>>
		vCut2EndPos		= <<-51.7617, 940.7729, 232.5966>>
		vCut2EndRot		= <<-3.7404, 0.0000, 84.4577>>
		vEndCutPos		= <<-54.6340, 956.9537, 239.0396>>
		vEndCutRot		= <<-19.1303, -0.0000, -149.8481>>
	ELIF eThisCourt = TENNIS_COURT_VinewoodHotel1
		vEstStartPos	= <<495.1683, -217.5374, 65.0420>>
		vEstStartRot	= <<-2.3845, -0.0000, 98.6308>>
		vEstEndPos		= <<495.1683, -217.5374, 65.0420>>
		vEstEndRot		= <<-2.3845, -0.0000, 121.7622>>
		vCut1StartPos	= <<487.4100, -207.2260, 54.1008>>
		vCut1StartRot	= <<-5.6561, -0.0000, -20.7994>>
		vCut1EndPos		= <<487.6892, -207.3320, 54.1008>>
		vCut1EndRot		= <<-5.6561, -0.0000, -20.7994>>
		vCut2StartPos	= <<485.9728, -227.8195, 54.3010>>
		vCut2StartRot	= <<-7.8660, 0.0000, 159.6492>>
		vCut2EndPos		= <<486.2157, -227.9096, 54.3010>>
		vCut2EndRot		= <<-7.8660, 0.0000, 159.6492>>
		vEndCutPos		= <<486.4862, -236.6287, 57.0774>>
		vEndCutRot		= <<-18.1919, 0.0000, 3.4992>>
	ELIF eThisCourt = TENNIS_COURT_RichmanHotel1
		vEstStartPos	= <<-1222.3241, 349.2241, 96.8315>>
		vEstStartRot	= <<-7.9664, -0.0000, 133.2006>>
		vEstEndPos		= <<-1222.3241, 349.2241, 96.8315>>
		vEstEndRot		= <<-7.9664, 0.0000, 144.1445>>
		vCut1StartPos	= <<-1217.1841, 329.9447, 80.3087>>
		vCut1StartRot	= <<-5.4425, -0.0000, -163.0523>>
		vCut1EndPos		= <<-1217.5055, 329.8468, 80.3087>>
		vCut1EndRot		= <<-5.4425, -0.0000, -163.0523>>
		vCut2StartPos	= <<-1228.0419, 347.1952, 80.5893>>
		vCut2StartRot	= <<-10.6986, 0.0000, 14.5021>>
		vCut2EndPos		= <<-1228.4235, 347.0965, 80.5893>>
		vCut2EndRot		= <<-10.6986, 0.0000, 14.5021>>
		vEndCutPos		= <<-1210.9601, 325.6631, 81.7946>>
		vEndCutRot		= <<-7.0604, 0.0000, 52.2813>>
	ELIF eThisCourt = TENNIS_COURT_LSUCourt1
		vEstStartPos	= <<-1615.0161, 255.5728, 61.6524>>
		vEstStartRot	= <<17.6874, -0.0000, 174.0035>>
		vEstEndPos		= <<-1615.0161, 255.5728, 61.6524>>
		vEstEndRot		= <<17.6874, 0.0000, 167.9108>>
		vCut1StartPos	= <<-1618.5062, 255.5526, 59.8491>>
		vCut1StartRot	= <<-5.7741, 0.0000, -156.6808>>
		vCut1EndPos		= <<-1619.1666, 255.2679, 59.8491>>
		vCut1EndRot		= <<-5.7741, 0.0000, -156.6808>>
		vCut2StartPos	= <<-1631.9211, 270.6205, 59.7183>>
		vCut2StartRot	= <<-3.2191, 0.0000, 24.8128>>
		vCut2EndPos		= <<-1632.2443, 270.4711, 59.7183>>
		vCut2EndRot		= <<-3.2191, 0.0000, 24.8128>>
		vEndCutPos		= <<-1640.4214, 273.9286, 64.1802>>
		vEndCutRot		= <<-25.2988, 0.0000, -117.6262>>
	ELIF eThisCourt = TENNIS_COURT_VespucciHotel
		vEstStartPos	= <<-934.9677, -1264.8883, 28.0571>>
		vEstStartRot	= <<10.0324, -0.0000, -167.8753>>
		vEstEndPos		= <<-934.9677, -1264.8883, 28.0571>>
		vEstEndRot		= <<10.0324, 0.0000, -150.1858>>
		vCut1StartPos	= <<-927.7309, -1268.2637, 8.5354>>
		vCut1StartRot	= <<-8.2404, 0.0000, -150.5351>>
		vCut1EndPos		= <<-928.1280, -1268.4879, 8.5354>>
		vCut1EndRot		= <<-8.2404, 0.0000, -150.5351>>
		vCut2StartPos	= <<-942.5503, -1254.5420, 8.5412>>
		vCut2StartRot	= <<-6.4098, -0.0000, 29.1854>>
		vCut2EndPos		= <<-942.9761, -1254.7802, 8.5412>>
		vCut2EndRot		= <<-6.4098, -0.0000, 29.1854>>
		vEndCutPos		= <<-936.0025, -1279.8507, 13.3759>>
		vEndCutRot		= <<-16.0879, -0.0000, -14.1346>>
	ELIF eThisCourt = TENNIS_COURT_WeazelCourt1
		vEstStartPos	= <<-1346.9120, -86.6428, 64.1162>>
		vEstStartRot	= <<1.0235, 0.0000, 164.1050>>
		vEstEndPos		= <<-1346.9120, -86.6428, 64.1162>>
		vEstEndRot		= <<1.0235, -0.0000, 145.4656>>
		vCut1StartPos	= <<-1377.2101, -91.8163, 51.1486>>
		vCut1StartRot	= <<-5.5958, 0.0000, 5.2778>>
		vCut1EndPos		= <<-1376.7970, -91.7782, 51.1486>>
		vCut1EndRot		= <<-5.5958, 0.0000, 5.2778>>
		vCut2StartPos	= <<-1369.3833, -110.9868, 50.8640>>
		vCut2StartRot	= <<-2.4917, -0.0000, -173.9544>>
		vCut2EndPos		= <<-1368.9037, -110.9360, 50.8640>>
		vCut2EndRot		= <<-2.4917, -0.0000, -173.9544>>
		vEndCutPos		= <<-1363.8210, -83.7574, 55.6739>>
		vEndCutRot		= <<-14.2855, -0.0000, 141.0600>>
	ELIF eThisCourt = TENNIS_COURT_chumashHotel
		vEstStartPos	= <<-2867.2266, 16.5983, 19.9156>>
		vEstStartRot	= <<1.7390, 0.0000, 63.3028>>
		vEstEndPos		= <<-2867.2266, 16.5983, 19.9156>>
		vEstEndRot		= <<1.7390, -0.0000, 75.9587>>
		vCut1StartPos	= <<-2868.8862, 25.1403, 11.9536>>
		vCut1StartRot	= <<-5.8176, 0.0000, -17.4057>>
		vCut1EndPos		= <<-2868.5479, 25.0343, 11.9536>>
		vCut1EndRot		= <<-5.8176, 0.0000, -17.4057>>
		vCut2StartPos	= <<-2869.1284, 6.8012, 12.2636>>
		vCut2StartRot	= <<-7.8955, 0.0000, 163.8746>>
		vCut2EndPos		= <<-2869.3667, 6.8700, 12.2636>>
		vCut2EndRot		= <<-7.8955, 0.0000, 163.8746>>
		vEndCutPos		= <<-2855.3826, 27.2468, 20.5089>>
		vEndCutRot		= <<-25.1167, 0.0000, 115.5386>>
	ELSE
		CPRINTLN(DEBUG_TENNIS, "Vectors for cameras not created, Court Enum not recognized!")
	ENDIF
	
	//Adjusting upward angles for new dynamic pos offsets
	vCut1StartRot.x	= 7.1329
	vCut1EndRot.x	= 7.1329
	vCut2StartRot.x	= 7.1329
	vCut2EndRot.x	= 7.1329
	
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisCam - 
///    thisProps - 
///    iUtilFlags - 
/// RETURNS:
///    Returns true when finished
FUNC BOOL TENNIS_PLAY_INTRO_CUTSCENE(TENNIS_CAMERAS &thisCam, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, SERVING_SIDE_ENUM eServeSide, TENNIS_PLAYER_ID eServing, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, BOOL bRematch)
	IF GET_TENNIS_CUTSCENE_STATE(thisCam) > CUT_INIT
	AND (IS_ENTITY_DEAD(thisProps.tennisPlayers[eSelfID].pIndex) OR IS_ENTITY_DEAD(thisProps.tennisPlayers[eOtherID].pIndex))
		PRINTSTRING("TENNIS_PLAY_INTRO_CUTSCENE: Player or Opponent is dead!!!!!\n")
		RETURN FALSE
	ENDIF
	
	IF GET_TENNIS_CUTSCENE_STATE(thisCam) <= CUT_LOAD_HEADSHOTS
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	ENDIF
	
//	IF NOT IS_TENNIS_SPLASH_UI_HIDDEN(thisUIData.tennisUI)
//		IF NOT UPDATE_TENNIS_BIG_MESSAGE(thisUIData.tennisUI.splashUI)
//			DISPLAY_TENNIS_SPLASH_UI(thisUIData.tennisUI, FALSE)
//		ENDIF
//	ENDIF
	
	STRING sConvo, sDict
	VECTOR vCut1StartPos, vCut1EndPos, vCut2StartPos, vCut2EndPos, vEndCutPos
	VECTOR vCut1StartRot, vCut1EndRot, vCut2StartRot, vCut2EndRot, vEndCutRot
	VECTOR vEstStartPos, vEstStartRot, vEstEndPos, vEstEndRot	// Used for the intro establishing pan
	
	GET_TENNIS_INTRO_CAMERA_VECTORS(
		thisProps.tennisCourt.eTennisCourt,
		vEstStartPos, vEstStartRot,
		vEstEndPos, vEstEndRot,
		vCut1StartPos, vCut1StartRot,
		vCut1EndPos, vCut1EndRot,
		vCut2StartPos, vCut2StartRot,
		vCut2EndPos, vCut2EndRot,
		vEndCutPos, vEndCutRot)
	
	VECTOR vOffset1		= << 0.341911, 2.83403, -0.0656705 >>
	VECTOR vOffset2		= << 0.0543287, 2.83269, -0.0656705 >>
	FLOAT fHeading		= GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eOtherID].vMyForward.x, thisProps.tennisPlayers[eOtherID].vMyForward.y)
	vCut1StartPos		= thisProps.tennisPlayers[eOtherID].vPos + ROTATE_VECTOR_ABOUT_Z(vOffset1, fHeading)
	vCut1EndPos			= thisProps.tennisPlayers[eOtherID].vPos + ROTATE_VECTOR_ABOUT_Z(vOffset2, fHeading)
	fHeading			= GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eSelfID].vMyForward.x, thisProps.tennisPlayers[eSelfID].vMyForward.y)
	vCut2StartPos		= thisProps.tennisPlayers[eSelfID].vPos + ROTATE_VECTOR_ABOUT_Z(vOffset2, fHeading)
	vCut2EndPos			= thisProps.tennisPlayers[eSelfID].vPos + ROTATE_VECTOR_ABOUT_Z(vOffset1, fHeading)

	SWITCH GET_TENNIS_CUTSCENE_STATE(thisCam)
		CASE CUT_SET_FRIEND_OUTFIT
			IF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY
//		        startActivity(thisProps.thisLocation)
		        IF IS_PED_IN_ANY_VEHICLE(FRIEND_A_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(FRIEND_A_PED_ID())
		        ENDIF
				IF DOES_ENTITY_EXIST(FRIEND_A_PED_ID()) AND NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())
					SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID], FRIEND_A_PED_ID())
				ENDIF
				IF NOT IS_PED_INJURED(FRIEND_A_PED_ID()) AND IS_PED_GROUP_MEMBER(FRIEND_A_PED_ID(), PLAYER_GROUP_ID())
					REMOVE_PED_FROM_GROUP(FRIEND_A_PED_ID())
				ENDIF
			ENDIF
		
			START_TIMER_NOW_SAFE(thisCam.tennisCamTimer)
			
			
			// We no longer need to wait; this is handled before the intro sync scene.
			SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_INIT)
		BREAK
		
		CASE CUT_WAIT_FOR_OUTFITS
			IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID])) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
				SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_INIT_FRIEND_ACT)
				RESTART_TIMER_NOW(thisCam.tennisCamTimer)
			ENDIF
		BREAK
		
		//Skipped in friend activities, mimicked mostly by CUT_INIT_FRIEND_ACT
		CASE CUT_INIT
			START_TIMER_NOW_SAFE(thisCam.tennisCamTimer)
			//Set up the intro cutscene camera
			STOP_CAM_POINTING(thisCam.tennisCamTut)
			SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
			SET_CAM_PARAMS(thisCam.tennisCamTut, vEstStartPos, vEstStartRot, 45.000)
			SET_CAM_PARAMS(thisCam.tennisCamTut, vEstEndPos, vEstEndRot, 45.000, 5000)
			RENDER_SCRIPT_CAMS(TRUE,FALSE)
			CPRINTLN(DEBUG_TENNIS, "Intro Pan while we place the opponent ped.")
//			SET_SCALEFORM_BIG_MESSAGE(thisUIData.tennisUI.splashUI, "WELCO_TUT", "", 3000)
			TENNIS_COMMON_INTRO_INIT_CALLS(thisCam, thisProps, thisUIData, eServeSide, eServing, eSelfID, eOtherID, bRematch)
			
			// Camera is cut away, clear the court.
			CLEAR_COURT_OF_PROPS(thisProps)

			SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_LOAD_HEADSHOTS)
		BREAK
		
		CASE CUT_LOAD_HEADSHOTS
			SET_PLAYER_PORTRAITS(thisUIData.tennisUI, thisProps, eSelfID, eOtherID)
			RESTART_TIMER_NOW(thisCam.tennisCamTimer)
			IF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY OR IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
				SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_START_FRIEND_CONVO)
			ELSE
				SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_INIT_CAMS_AND_CONVO)
			ENDIF
		BREAK
		
		CASE CUT_INIT_CAMS_AND_CONVO
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 3.0
				//Set up the intro cutscene camera
				STOP_CAM_POINTING(thisCam.tennisCamTut)
				SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vCut1StartPos, vCut1StartRot, 30)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vCut1EndPos, vCut1EndRot, 30, 4000)
				SHAKE_CAM(thisUIData.tennisCameras.tennisCamTut, "HAND_SHAKE", 1.0)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
		
				//Pick Intro dialogue string
				IF NOT bRematch
					sConvo = "MGTN_intro2q"
				ELSE
					sConvo = "MGTN_rematch"
				ENDIF
				CREATE_CONVERSATION(thisUIData.myDialogueStruct, thisUIData.sSubtitleGroupID, sConvo, CONV_PRIORITY_VERY_LOW)
		
				//Move the state on
				SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_SHOW_OPPONENT)
				RESTART_TIMER_NOW(thisCam.tennisCamTimer)
				thisCam.bFlag = FALSE
			ELIF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 2.9 AND NOT thisCam.bFlag
				sDict = PICK_STRING(IS_TENNIS_PED_MALE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID])), "mini@tennis", "mini@tennis@female")
				SEQUENCE_INDEX seqIndex
				OPEN_SEQUENCE_TASK(seqIndex)
				TASK_PLAY_ANIM(NULL, sDict, "intro_a", INSTANT_BLEND_IN)
				TASK_PLAY_ANIM(NULL, sDict, "intro_a", default, default, default, AF_LOOPING)
				CLOSE_SEQUENCE_TASK(seqIndex)
				TASK_PERFORM_SEQUENCE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), seqIndex)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
				thisCam.bFlag = TRUE
				CPRINTLN(DEBUG_TENNIS, "CUT_INIT_CAMS_AND_CONVO :: TASK_PERFORM_SEQUENCE(eOtherID)")
			ENDIF
		BREAK
		
		CASE CUT_INIT_FRIEND_ACT
			TENNIS_COMMON_INTRO_INIT_CALLS(thisCam, thisProps, thisUIData, eServeSide, eServing, eSelfID, eOtherID, bRematch)
	
			//Move the state on
			SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_LOAD_HEADSHOTS)
		BREAK
		
		CASE CUT_START_FRIEND_CONVO
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 3.0
				//Set up camera to look at opponent
				STOP_CAM_POINTING(thisCam.tennisCamTut)
				SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vCut1StartPos, vCut1StartRot, 30)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vCut1EndPos, vCut1EndRot, 30, 4000)
				SHAKE_CAM(thisUIData.tennisCameras.tennisCamTut, "HAND_SHAKE", 1.0)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				SWITCH thisUIData.playerChars[eOtherID]
					CASE CHAR_MICHAEL
						IF NOT bRematch
							sConvo = "MGTN_introQM"
						ELSE
							sConvo = "MGTN_remaQM"
						ENDIF
					BREAK
					CASE CHAR_FRANKLIN
						IF NOT bRematch
							sConvo = "MGTN_introQF"
						ELSE
							sConvo = "MGTN_remaQF"
						ENDIF
					BREAK
					CASE CHAR_TREVOR
						IF NOT bRematch
							sConvo = "MGTN_introQT"
						ELSE
							sConvo = "MGTN_remaQT"
						ENDIF
					BREAK
					CASE CHAR_LAMAR
						IF NOT bRematch
							sConvo = "MGTN_introQL"
						ELSE
							sConvo = "MGTN_remaQL"
						ENDIF
					BREAK
					CASE CHAR_AMANDA
						IF NOT bRematch
							sConvo = "MGTN_introQA"
						ELSE
							sConvo = "MGTN_remaQA"
						ENDIF
					BREAK
					CASE CHAR_JIMMY
						IF NOT bRematch
							sConvo = "MGTN_introQJ"
						ELSE
							sConvo = "MGTN_remaQJ"
						ENDIF
					BREAK
					DEFAULT
						CPRINTLN(DEBUG_TENNIS, "Default friend case hit, this shouldn't happen, contact Rob Pearsall!!!")
					BREAK
				ENDSWITCH
				CREATE_CONVERSATION(thisUIData.myDialogueStruct, thisUIData.sSubtitleGroupID, sConvo, CONV_PRIORITY_VERY_LOW)
				
				//Move the state on
				RESTART_TIMER_NOW(thisCam.tennisCamTimer)
				SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_SHOW_OPPONENT)
				thisCam.bFlag = FALSE
			ELIF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 2.9 AND NOT thisCam.bFlag
				sDict = PICK_STRING(IS_TENNIS_PED_MALE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID])), "mini@tennis", "mini@tennis@female")
				SEQUENCE_INDEX seqIndex
				OPEN_SEQUENCE_TASK(seqIndex)
				TASK_PLAY_ANIM(NULL, sDict, "intro_a", INSTANT_BLEND_IN)
				TASK_PLAY_ANIM(NULL, sDict, "intro_a", default, default, default, AF_LOOPING)
				CLOSE_SEQUENCE_TASK(seqIndex)
				TASK_PERFORM_SEQUENCE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), seqIndex)
				
				CPRINTLN(DEBUG_TENNIS, "CUT_START_FRIEND_CONVO :: TASK_PERFORM_SEQUENCE(eOtherID)")
				thisCam.bFlag = TRUE
			ENDIF
		BREAK
		
		CASE CUT_SHOW_OPPONENT
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 3.0
				SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vCut2StartPos, vCut2StartRot, 30)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vCut2EndPos, vCut2EndRot, 30, 4000)
				SHAKE_CAM(thisUIData.tennisCameras.tennisCamTut, "HAND_SHAKE", 1.0)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
			
				RESTART_TIMER_NOW(thisCam.tennisCamTimer)
				SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_SIDE_SLIDE_1)
				
				ATTACH_ENTITY_TO_ENTITY(
					thisProps.tennisPlayers[eOtherID].oTennisRacket, 
					GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), 
					GET_PED_BONE_INDEX( GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), BONETAG_PH_R_HAND), 
					RACKET_OFFSET, RACKET_ROTATION)
				thisCam.bFlag = FALSE
			ELIF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 2.9 AND NOT thisCam.bFlag
				sDict = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eSelfID]), "mini@tennis", "mini@tennis@female")
				SEQUENCE_INDEX seqIndex
				OPEN_SEQUENCE_TASK(seqIndex)
				TASK_PLAY_ANIM(NULL, sDict, "intro_b", INSTANT_BLEND_IN)
				TASK_PLAY_ANIM(NULL, sDict, "intro_b", default, default, default, AF_LOOPING)
				CLOSE_SEQUENCE_TASK(seqIndex)
				TASK_PERFORM_SEQUENCE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), seqIndex)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), TRUE)
				thisCam.bFlag = TRUE
			ENDIF
		BREAK
		
		CASE CUT_SIDE_SLIDE_1
			SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_SHOW_PLAYER)
			SWITCH thisUIData.playerChars[eSelfID]
				CASE CHAR_MICHAEL
					IF NOT bRematch
						sConvo = "MGTN_intr2aM"
					ELSE
						sConvo = "MGTN_remaQM"
					ENDIF
				BREAK
				CASE CHAR_FRANKLIN
					IF NOT bRematch
						sConvo = "MGTN_intr2af"
					ELSE
						sConvo = "MGTN_remaQF"
					ENDIF
				BREAK
				CASE CHAR_TREVOR
					IF NOT bRematch
						sConvo = "MGTN_intr2at"
					ELSE
						sConvo = "MGTN_remaQT"
					ENDIF
				BREAK
			ENDSWITCH
			CREATE_CONVERSATION(thisUIData.myDialogueStruct, thisUIData.sSubtitleGroupID, sConvo, CONV_PRIORITY_VERY_LOW)
		BREAK
		
		CASE CUT_SHOW_PLAYER
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 3.0 AND HAS_ANIM_DICT_LOADED("mini@tennis@female")
				SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_FINISH)
			ENDIF
		BREAK
		
		CASE CUT_FINISH
//			SET_CAM_PARAMS(thisCam.tennisCamTut, vEndCutPos, vEndCutRot, 45.000)
			sDict = PICK_STRING(IS_TENNIS_PED_MALE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID])), "mini@tennis", "mini@tennis@female")
			SET_PED_GRAVITY(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), TRUE)
			IF NOT IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), sDict, "intro_a")
				TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), sDict, "intro_a", default, default, default, AF_LOOPING)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), TRUE)
			ENDIF
			sDict = PICK_STRING(IS_TENNIS_PED_MALE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID])), "mini@tennis", "mini@tennis@female")
			IF NOT IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), sDict, "intro_b")
				TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), sDict, "intro_b", default, default, default, AF_LOOPING)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), TRUE)
			ENDIF
			SETUP_TENNIS_COURT_SHADOWS()
			ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eSelfID, eOtherID)
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_TENNIS_OUTRO_COORDS(PLAYING_COURT eCourt)
	SWITCH eCourt
		CASE TENNIS_COURT_CountryClub  // Seems to be mapped to Vespucci Beach courts
			RETURN <<-1160.4484, -1627.3651, 4.3739>>
		BREAK
		
		CASE TENNIS_COURT_MichaelHouse
			RETURN <<-778.5258, 157.2043, 67.4746>>
		BREAK
		
		CASE TENNIS_COURT_VinewoodHotel1
			RETURN <<483.1611, -232.2875, 53.7814>>
		BREAK
		
		CASE TENNIS_COURT_RichmanHotel1
			RETURN <<-1228.4156, 335.1815, 80.0072>>
		BREAK
		
		CASE TENNIS_COURT_LSUCourt1
			RETURN <<-1622.7997, 250.2613, 59.5503>>
		BREAK
		
		CASE TENNIS_COURT_VespucciHotel
			RETURN <<-943.4133, -1258.1320, 7.9671>>
		BREAK
		
		CASE TENNIS_COURT_WeazelCourt1
			RETURN <<-1365.3394, -113.9222, 50.7046>>
		BREAK
		
//		CASE TENNIS_COURT_WeazelCourt2
//			RETURN <<-1348.7031, -112.6838, 50.7046>>
//		BREAK
//		
//		CASE TENNIS_COURT_WeazelCourt3
//			RETURN <<-1306.1234, -110.7947, 47.8662>>
//		BREAK
//		
//		CASE TENNIS_COURT_WeazelCourt4
//		CASE TENNIS_COURT_WeazelCourt5
//			RETURN <<-1286.1997, -125.1103, 45.7609>>
//		BREAK
		
		CASE TENNIS_COURT_chumashHotel
			RETURN <<-2874.5032, 5.2701, 11.6033>>
		BREAK
		
		CASE TENNIS_COURT_DamVista
			RETURN <<-48.9073, 947.0613, 232.1739>>
		BREAK
		
		// These don't appear to be referenced for single player
//		CASE TENNIS_COURT_ArchMansion
//		CASE TENNIS_COURT_CrescentDrive
//		CASE TENNIS_COURT_SpireHouse
//		CASE TENNIS_COURT_PagodaFireplace
//		CASE TENNIS_COURT_ChurchAdjacent
		CASE TENNIS_COURT_Ambient
			RETURN <<0,0,0>>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_TENNIS_OUTRO_ROTATION(PLAYING_COURT eCourt)
	SWITCH eCourt
		CASE TENNIS_COURT_CountryClub  // Seems to be mapped to Vespucci Beach courts
			RETURN <<0.0, 0.0, 303.4364>>
		BREAK
		
		CASE TENNIS_COURT_MichaelHouse
			RETURN <<0.0, 0.0, 99.6300>>
		BREAK
		
		CASE TENNIS_COURT_VinewoodHotel1
			RETURN <<0.0, 0.0, 217.9985>>
		BREAK
		
		CASE TENNIS_COURT_RichmanHotel1
			RETURN <<0.0, 0.0, 79.0217>>
		BREAK
		
		CASE TENNIS_COURT_LSUCourt1
			RETURN <<0.0, 0.0, 130.2767>>
		BREAK
		
		CASE TENNIS_COURT_VespucciHotel
			RETURN <<0.0, 0.0, 108.0969>>
		BREAK
		
		CASE TENNIS_COURT_WeazelCourt1
			RETURN <<0.0, 0.0, 252.9845>>
		BREAK
		
//		CASE TENNIS_COURT_WeazelCourt2
//			RETURN <<0.0, 0.0, 103.1033>>
//		BREAK
//		
//		CASE TENNIS_COURT_WeazelCourt3
//			RETURN <<0.0, 0.0, 149.0952>>
//		BREAK
//		
//		CASE TENNIS_COURT_WeazelCourt4
//		CASE TENNIS_COURT_WeazelCourt5
//			RETURN <<0.0, 0.0, 53.9422>>
//		BREAK
		
		CASE TENNIS_COURT_chumashHotel
			RETURN <<0.0, 0.0, 195.4965>>
		BREAK
		
		CASE TENNIS_COURT_DamVista
			RETURN <<0.0, 0.0, 16.4788>>
		BREAK
		
		// These don't appear to be referenced for single player
//		CASE TENNIS_COURT_ArchMansion
//		CASE TENNIS_COURT_CrescentDrive
//		CASE TENNIS_COURT_SpireHouse
//		CASE TENNIS_COURT_PagodaFireplace
//		CASE TENNIS_COURT_ChurchAdjacent
		CASE TENNIS_COURT_Ambient
			RETURN <<0,0,0>>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_TENNIS_OPPONENT_OUTRO_COORDS(PLAYING_COURT eCourt)
	SWITCH eCourt
		CASE TENNIS_COURT_CountryClub  // Seems to be mapped to Vespucci Beach courts
			RETURN <<-1165.8970, -1625.7869, 3.3739>>
		BREAK
		
		CASE TENNIS_COURT_MichaelHouse
			RETURN <<-769.3506, 164.1594, 66.4746>>
		BREAK
		
		CASE TENNIS_COURT_VinewoodHotel1
			RETURN <<482.2044, -225.0880, 52.7864>>
		BREAK
		
		CASE TENNIS_COURT_RichmanHotel1
			RETURN <<-1219.1724, 334.4609, 79.0072>>
		BREAK
		
		CASE TENNIS_COURT_LSUCourt1
			RETURN <<-1629.6028, 259.6511, 58.5553>>
		BREAK
		
		CASE TENNIS_COURT_VespucciHotel
			RETURN <<-938.1701, -1249.2831, 6.9671>>
		BREAK
		
		CASE TENNIS_COURT_WeazelCourt1
			RETURN <<-1369.7644, -103.5999, 49.7046>>
		BREAK
		
//		CASE TENNIS_COURT_WeazelCourt2
//			RETURN <<-1348.7031, -112.6838, 50.7046>>
//		BREAK
//		
//		CASE TENNIS_COURT_WeazelCourt3
//			RETURN <<-1306.1234, -110.7947, 47.8662>>
//		BREAK
//		
//		CASE TENNIS_COURT_WeazelCourt4
//		CASE TENNIS_COURT_WeazelCourt5
//			RETURN <<-1286.1997, -125.1103, 45.7609>>
//		BREAK
		
		CASE TENNIS_COURT_chumashHotel
			RETURN <<-2862.9324, 11.8041, 10.6083>>
		BREAK
		
		CASE TENNIS_COURT_DamVista
			RETURN <<-40.3682, 949.8334, 231.1740>>
		BREAK
		
		// These don't appear to be referenced for single player
//		CASE TENNIS_COURT_ArchMansion
//		CASE TENNIS_COURT_CrescentDrive
//		CASE TENNIS_COURT_SpireHouse
//		CASE TENNIS_COURT_PagodaFireplace
//		CASE TENNIS_COURT_ChurchAdjacent
		CASE TENNIS_COURT_Ambient
			RETURN <<0,0,0>>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT GET_TENNIS_OPPONENT_OUTRO_HEADING(PLAYING_COURT eCourt)
	SWITCH eCourt
		CASE TENNIS_COURT_CountryClub  // Seems to be mapped to Vespucci Beach courts
			RETURN 275.2246
		BREAK
		
		CASE TENNIS_COURT_MichaelHouse
			RETURN 113.7222
		BREAK
		
		CASE TENNIS_COURT_VinewoodHotel1
			RETURN 197.4718
		BREAK
		
		CASE TENNIS_COURT_RichmanHotel1
			RETURN 90.1874 
		BREAK
		
		CASE TENNIS_COURT_LSUCourt1
			RETURN 189.8068
		BREAK
		
		CASE TENNIS_COURT_VespucciHotel
			RETURN 142.6223
		BREAK
		
		CASE TENNIS_COURT_WeazelCourt1
			RETURN 215.3455
		BREAK
		
//		CASE TENNIS_COURT_WeazelCourt2
//			RETURN 103.1033
//		BREAK
//		
//		CASE TENNIS_COURT_WeazelCourt3
//			RETURN 149.0952
//		BREAK
//		
//		CASE TENNIS_COURT_WeazelCourt4
//		CASE TENNIS_COURT_WeazelCourt5
//			RETURN 53.9422
//		BREAK
		
		CASE TENNIS_COURT_chumashHotel
			RETURN 136.2812
		BREAK
		
		CASE TENNIS_COURT_DamVista
			RETURN 86.4911
		BREAK
		
		// These don't appear to be referenced for single player
//		CASE TENNIS_COURT_ArchMansion
//		CASE TENNIS_COURT_CrescentDrive
//		CASE TENNIS_COURT_SpireHouse
//		CASE TENNIS_COURT_PagodaFireplace
//		CASE TENNIS_COURT_ChurchAdjacent
		CASE TENNIS_COURT_Ambient
			RETURN 0.0
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

/// PURPOSE:
///    Plays outro cutscene for Tennis
/// PARAMS:
///    iPlayerIndex - ENTITY_INDEX for the player
///    iOpponentIndex - ENTITY_INDEX for the opponent
///    bWon - 
/// RETURNS:
///    
FUNC BOOL TENNIS_PLAY_OUTRO_CUTSCENE(TENNIS_CAMERAS &thisCam, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, INT &iUtilFlags, BOOL bPlayerWon, TENNIS_PLAYER_ID eSelfID/*, TENNIS_PLAYER_ID eOtherID*/)
	IF IS_ENTITY_DEAD(thisProps.tennisPlayers[0].pIndex) 
	OR IS_ENTITY_DEAD(thisProps.tennisPlayers[1].pIndex)	
		PRINTSTRING("TENNIS_PLAY_INTRO_CUTSCENE: Player or Opponent is dead!!!!!\n")
		RETURN FALSE
	ENDIF
	
	TEXT_LABEL_63 sSceneDict, sScenePlrAnim, sSceneCamAnim
	VECTOR vScenePos
	VECTOR vCamPos, vCamRot, vDestPos, vDestRot
	FLOAT fCamFOV
	FLOAT fSceneHeading
	
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()

	SWITCH GET_TENNIS_CUTSCENE_STATE(thisCam)
		CASE CUT_INIT
			SET_TENNIS_INTERSTITIAL_PLAYER(thisUIData, eSelfID)
			sSceneDict = "mini@tennisexit@"
			sSceneDict += PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eSelfID]), "male", "female")
			sScenePlrAnim = "tennis_outro_"
			sScenePlrAnim += PICK_STRING(bPlayerWon, "win", "lose")
			sSceneCamAnim = sScenePlrAnim
			sSceneCamAnim += "_cam"
			
			fSceneHeading = GET_HEADING_FROM_VECTOR_2D(thisProps.vForward.x, thisProps.vForward.y)
			vScenePos = thisProps.tennisCourt.vCenterCourt
			vScenePos.z += 1.0
			IF thisProps.tennisPlayers[eSelfID].eCourtSide = TENNIS_PLAYER_AWAY
				vScenePos -= 0.6 * thisProps.vForward
			ELSE
				vScenePos += 0.6 * thisProps.vForward
				fSceneHeading += 180.0
			ENDIF
			
			thisUIData.iSceneID = CREATE_SYNCHRONIZED_SCENE(vScenePos, <<0,0,fSceneHeading>>)
			
			IF DOES_CAM_EXIST(thisCam.tennisCamTut)
				DESTROY_CAM(thisCam.tennisCamTut)
			ENDIF
			thisCam.tennisCamTut = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
			PLAY_SYNCHRONIZED_CAM_ANIM(thisCam.tennisCamTut, thisUIData.iSceneID, sSceneCamAnim, sSceneDict)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			CASCADE_SHADOWS_INIT_SESSION()
			
			CLEAR_PED_TASKS_IMMEDIATELY(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
			FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
						
			SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), TRUE)
			CPRINTLN(DEBUG_TENNIS, "SET_FORCE_FOOTSTEP_UPDATE TRUE")
						
			START_TIMER_NOW_SAFE(thisCam.tennisCamTimer)
			//Cause the player to drop the ball so it's not awkwardly attached during the cutscene.
			DETACH_ENTITY(thisProps.tennisBall.oBall)
			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			TASK_SYNCHRONIZED_SCENE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), thisUIData.iSceneID, sSceneDict, sScenePlrAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			
			IF bPlayerWon
				SET_BITMASK_AS_ENUM(iUtilFlags, TD_GAME_WON_DSTAR)
			ELSE
				SET_BITMASK_AS_ENUM(iUtilFlags, TD_GAME_LOST_DSTAR)
			ENDIF
			
			CLEAR_BITMASK_AS_ENUM(iUtilFlags, TD_PAUSE_DIALOGUE)
			CPRINTLN(DEBUG_TENNIS, "Turning on Game Finished bits and clearing the pause dialogue bit")
			
			SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_SHOW_PLAYER)
			// Make sure the buttons get initialized for the results scoreboard
			CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_SCLB_BUTTONS_INITED)
		BREAK
		
		CASE CUT_SHOW_PLAYER
			IF NOT IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_AMANDA_GETS_LAST_WORD) AND NOT IS_AMBIENT_SPEECH_PLAYING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]))
				SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_AMANDA_GETS_LAST_WORD)
				IF IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP) AND thisProps.eTennisActivity <> TA_FRIEND_ACTIVITY AND thisUIData.playerChars[eSelfID] = CHAR_MICHAEL
					STRING sConvo
					IF bPlayerWon
						sConvo = "FAC_LEAVE_TENNIS_WON"
					ELIF IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_QUIT_BIT)
						sConvo = "FAC_LEAVE_TENNIS_QUIT"
					ELSE
						sConvo = "FAC_LEAVE_TENNIS_LOST"
					ENDIF
					PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)], sConvo, GET_TENNIS_PLAYER_VOICE(thisProps.tennisPlayers[1 - ENUM_TO_INT(eSelfID)]), SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
				ENDIF
			ENDIF
			IF NOT IS_AMBIENT_SPEECH_PLAYING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[0])) AND NOT IS_AMBIENT_SPEECH_PLAYING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1]))
				IF (NOT IS_SYNCHRONIZED_SCENE_RUNNING(thisUIData.iSceneID)) OR GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID) > 0.65
					IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(thisUIData.tennisUI.eUISecFlags, TUSF_MISSION_END_STINGER)
						IF thisProps.eTennisActivity <> TA_FRIEND_ACTIVITY
			        		MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(bPlayerWon)
						ENDIF
						SET_BITMASK_ENUM_AS_ENUM(thisUIData.tennisUI.eUISecFlags, TUSF_MISSION_END_STINGER)
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 texDisplay
			FLOAT fDisplayAtY, fDisplayAdd
			fDisplayAtY = 0.2
			fDisplayAdd = 0.0125
			texDisplay = "scene phase: "
			texDisplay += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID))
			DRAW_DEBUG_TEXT_2D(texDisplay, (<< 0.5,fDisplayAtY,0.0 >>))
			fDisplayAtY += fDisplayAdd
			#ENDIF
			
			FLOAT fUIPhase
			fUIPhase = PICK_FLOAT(bPlayerWon, 0.47, 0.65)
			
			IF NOT IS_BITMASK_AS_ENUM_SET(thisUIData.tennisUI.iUIFlags, TUF_MATCH_WON)
			AND (NOT IS_SYNCHRONIZED_SCENE_RUNNING(thisUIData.iSceneID) OR GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID) > fUIPhase)
				SET_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_MATCH_WON)
				IF bPlayerWon
					SHOW_TENNIS_MATCH_WON_UI(thisUIData.tennisUI, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt))
				ELSE
					SHOW_TENNIS_MATCH_LOST_UI(thisUIData.tennisUI, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt))
				ENDIF
			ENDIF
			
			IF (NOT IS_SYNCHRONIZED_SCENE_RUNNING(thisUIData.iSceneID)) OR GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID) > 0.90
				SET_TENNIS_CUTSCENE_STATE(thisCam, CUT_SIDE_SLIDE_1)
				CLEAR_BITMASK_AS_ENUM(thisUIData.tennisUI.iUIFlags, TUF_MATCH_WON)
			ENDIF
		BREAK
		
		CASE CUT_SIDE_SLIDE_1
			IF NOT IS_AMBIENT_SPEECH_PLAYING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[0])) AND NOT IS_AMBIENT_SPEECH_PLAYING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1]))
				IF (NOT IS_SYNCHRONIZED_SCENE_RUNNING(thisUIData.iSceneID)) OR GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID) > 0.65
					IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(thisUIData.tennisUI.eUISecFlags, TUSF_MISSION_END_STINGER)
						IF thisProps.eTennisActivity <> TA_FRIEND_ACTIVITY
			        		MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(bPlayerWon)
						ENDIF
						SET_BITMASK_ENUM_AS_ENUM(thisUIData.tennisUI.eUISecFlags, TUSF_MISSION_END_STINGER)
					ENDIF
				ENDIF
			ENDIF
			vCamPos = GET_CAM_COORD(thisCam.tennisCamTut)
			vCamRot = GET_CAM_ROT(thisCam.tennisCamTut)
			fCamFOV = GET_CAM_FOV(thisCam.tennisCamTut)
			
			vDestPos = vCamPos
			vDestPos.z += 91.6584
			vDestRot = vCamRot
			vDestRot.x += 45.0
			
			IF DOES_CAM_EXIST(thisCam.tennisCamTut)
				DESTROY_CAM(thisCam.tennisCamTut)
			ENDIF
			
			thisCam.tennisCamTut = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vCamPos, vCamRot, fCamFOV, TRUE)
			SET_CAM_PARAMS(thisCam.tennisCamTut, vDestPos, vDestRot, fCamFOV, 666)
			PLAY_SOUND_FRONTEND(-1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
			CDEBUG2LN(DEBUG_TENNIS, "QUIT_WHOOSH played, first")
			RESTART_TIMER_NOW(thisCam.tennisCamTimer)
			SET_TENNIS_CUTSCENE_STATE(thisCam, WAIT_FOR_SLIDE_1)
			
			ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, FALSE) 
		BREAK
		
		CASE WAIT_FOR_SLIDE_1
			BOOL bWaitForConvo
			bWaitForConvo = IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP) AND thisProps.eTennisActivity <> TA_FRIEND_ACTIVITY AND NOT IS_AMBIENT_SPEECH_PLAYING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[1]))
			
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) >= 1.1 AND ((NOT bWaitForConvo) OR (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()))
				SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), FALSE)
				CPRINTLN(DEBUG_TENNIS, "SET_FORCE_FOOTSTEP_UPDATE FALSE")
				IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(thisUIData.tennisUI.eUISecFlags, TUSF_MISSION_END_STINGER)
					IF thisProps.eTennisActivity <> TA_FRIEND_ACTIVITY
		        		MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(bPlayerWon)
					ENDIF
					SET_BITMASK_ENUM_AS_ENUM(thisUIData.tennisUI.eUISecFlags, TUSF_MISSION_END_STINGER)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_TENNIS_WANDERING_MENU_CAMERA(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData)
	VECTOR vStartPos, vStartRot, vEndPos, vEndRot
	VECTOR vCenterCourt = thisProps.tennisCourt.vCenterCourt
	VECTOR vOffset1, vOffset2
	
	IF NOT DOES_CAM_EXIST(thisUIData.tennisCameras.tennisCamTut)
		thisUIData.tennisCameras.tennisCamTut = CREATE_CAMERA(CAMTYPE_SCRIPTED)
	ENDIF
	
	IF IS_CAM_SHAKING(thisUIData.tennisCameras.tennisCamTut)
		STOP_CAM_SHAKING(thisUIData.tennisCameras.tennisCamTut, TRUE)
	ENDIF
	
	FLOAT fCourtHeading = GET_HEADING_FROM_VECTOR_2D(thisProps.tennisCourt.vForwardNorm.x, thisProps.tennisCourt.vForwardNorm.y)
	
//	CPRINTLN(DEBUG_TENNIS, "UPDATE_TENNIS_WANDERING_MENU_CAMERA=", GET_STRING_FROM_TENNIS_MENU_WANDERING_STATE(thisUIData.tennisCameras.eWanderingCam))
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
			thisUIData.tennisCameras.iWanderCamStamp = 0
		ENDIF
	#ENDIF
	
//	<<-54.6967, 945.2090, 232.0078>>
//	VECTOR vCenter, vOffset, vRot
//	vCenter = thisProps.tennisCourt.vCenterCourt
//	vOffset = <<-54.6905, 947.1729, 231.1741>> - vCenter
//	vRot = ROTATE_VECTOR_ABOUT_Z(vOffset, -fCourtHeading)
//	CPRINTLN(DEBUG_TENNIS, "vRot1=", vRot)
//	<< -2.06296, -11.737, 0.833801 >>
	
	SWITCH thisUIData.tennisCameras.eWanderingCam
		CASE TMWS_ORBIT_RIGHT_LEFT
			IF GET_GAME_TIMER() > thisUIData.tennisCameras.iWanderCamStamp
				vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< -6.60958, 29.4857, 7.926 >>), fCourtHeading)
				vOffset2	= ROTATE_VECTOR_ABOUT_Z((<< -1.60898, 31.8844, 8.326 >>), fCourtHeading)
				IF thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_VespucciHotel
					vOffset1 *= WANDER_CAM_ORBIT_SCALAR
					vOffset2 *= WANDER_CAM_ORBIT_SCALAR
				ENDIF
				vStartPos	= vCenterCourt + vOffset1
				vStartRot	= <<-13.2626, -0.1123, -166.50358 + fCourtHeading >>
				vEndPos		= vCenterCourt + vOffset2
				vEndRot		= <<-13.2626, -0.1123, -178.25818 + fCourtHeading >>
				SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisUIData.tennisCameras.tennisCamTut, vStartPos, vStartRot, 34.000, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(thisUIData.tennisCameras.tennisCamTut, vEndPos, vEndRot, 34.000, 10792, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				thisUIData.tennisCameras.iWanderCamStamp = GET_GAME_TIMER() + 10792
				thisUIData.tennisCameras.eWanderingCam = TMWS_SLOW_DOLLY_PLAYER
			ENDIF
		BREAK
		CASE TMWS_SLOW_DOLLY_PLAYER
			IF GET_GAME_TIMER() > thisUIData.tennisCameras.iWanderCamStamp
				vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< 3.97927, -14.717, 1.526 >>), fCourtHeading)
				vOffset2	= ROTATE_VECTOR_ABOUT_Z((<< 3.27921, -14.9168, 1.526 >>), fCourtHeading)
				vStartPos	= vCenterCourt + vOffset1
				vStartRot	= <<-3.6658, 0.0000, -342.18808 + fCourtHeading >>
				vEndPos		= vCenterCourt + vOffset2
				vEndRot		= <<-3.6658, 0.0000, -342.18808 + fCourtHeading >>
				SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisUIData.tennisCameras.tennisCamTut, vStartPos, vStartRot, 34.000, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(thisUIData.tennisCameras.tennisCamTut, vEndPos, vEndRot, 34.000, 8891, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				thisUIData.tennisCameras.iWanderCamStamp = GET_GAME_TIMER() + 8891
				thisUIData.tennisCameras.eWanderingCam = TMWS_SLOW_DOLLY_OPP
			ENDIF
		BREAK
		CASE TMWS_SLOW_DOLLY_OPP
			IF GET_GAME_TIMER() > thisUIData.tennisCameras.iWanderCamStamp
				vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< -5.61444, 10.1854, 1.126 >>), fCourtHeading)
				vOffset2	= ROTATE_VECTOR_ABOUT_Z((<< -5.91428, 10.9855, 1.126 >>), fCourtHeading)
				vStartPos	= vCenterCourt + vOffset1
				vStartRot	= <<-3.4082, 0.0000, -62.99898 + fCourtHeading >>
				vEndPos		= vCenterCourt + vOffset2
				vEndRot		= <<-3.4082, 0.0000, -69.99898 + fCourtHeading >>
				SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisUIData.tennisCameras.tennisCamTut, vStartPos, vStartRot, 34.000, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(thisUIData.tennisCameras.tennisCamTut, vEndPos, vEndRot, 34.000, 11233, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				thisUIData.tennisCameras.iWanderCamStamp = GET_GAME_TIMER() + 11233
				thisUIData.tennisCameras.eWanderingCam = TMWS_SLOW_PUSH_OPP
			ENDIF
		BREAK
		CASE TMWS_SLOW_PUSH_OPP
			IF GET_GAME_TIMER() > thisUIData.tennisCameras.iWanderCamStamp
				IF thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_VespucciHotel 
				OR thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_chumashHotel
					fCourtHeading += WANDER_CAM_PUSH_OPP_ROT
				ENDIF
				vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< -4.93523, 16.192, 1.295 >>), fCourtHeading)
				vOffset2	= ROTATE_VECTOR_ABOUT_Z((<< -4.5825, 15.4108, 1.261 >>), fCourtHeading)
				IF thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_VespucciHotel 
				OR thisProps.tennisCourt.eTennisCourt = TENNIS_COURT_chumashHotel
					vOffset1 *= WANDER_CAM_PUSH_OPP_SCALAR
					vOffset2 *= WANDER_CAM_PUSH_OPP_SCALAR
				ENDIF
				vStartPos	= vCenterCourt + vOffset1
				vStartRot	= <<-2.8891, 0.0000, -155.689484 + fCourtHeading >>
				vEndPos		= vCenterCourt + vOffset2
				vEndRot		= <<-2.8891, 0.0000, -155.689484 + fCourtHeading >>
				SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisUIData.tennisCameras.tennisCamTut, vStartPos, vStartRot, 34.000, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(thisUIData.tennisCameras.tennisCamTut, vEndPos, vEndRot, 34.000, 10660, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				thisUIData.tennisCameras.iWanderCamStamp = GET_GAME_TIMER() + 10660
				thisUIData.tennisCameras.eWanderingCam = TMWS_SLOW_PUSH_PLAYER
			ENDIF
		BREAK
		CASE TMWS_SLOW_PUSH_PLAYER
			IF GET_GAME_TIMER() > thisUIData.tennisCameras.iWanderCamStamp
				vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< -0.0195496, -10.016, 0.826004 >>), fCourtHeading)
				vOffset2	= ROTATE_VECTOR_ABOUT_Z((<< 0.580325, -10.4161, 0.926003 >>), fCourtHeading)
				vStartPos	= vCenterCourt + vOffset1
				vStartRot	= <<9.5480, -0.0000, -133.68988 + fCourtHeading >>
				vEndPos		= vCenterCourt + vOffset2
				vEndRot		= <<9.5480, -0.0000, -133.68988 + fCourtHeading >>
				SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisUIData.tennisCameras.tennisCamTut, vStartPos, vStartRot, 34.000, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(thisUIData.tennisCameras.tennisCamTut, vEndPos, vEndRot, 34.000, 10429, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				thisUIData.tennisCameras.iWanderCamStamp = GET_GAME_TIMER() + 10429
				thisUIData.tennisCameras.eWanderingCam = TMWS_ORBIT_RIGHT_LEFT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC RUN_TENNIS_INTRO_SYNC_SCENE(TENNIS_CAMERAS &thisCam, TENNIS_GAME_PROPERTIES& thisProps, TENNIS_GAME_UI_DATA& thisUIData, TENNIS_GAME_SP_DATA &thisSPData, TENNIS_PLAYER_ID eStarPed, TENNIS_PLAYER_ID eOtherPed)
	CDEBUG2LN(DEBUG_TENNIS, "RUN_TENNIS_INTRO_SYNC_SCENE :: eOtherPed = ", eOtherPed, ", eStarPed = ", eStarPed)
	VECTOR vOffset
	
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
	// Not sure if we can get away with doing this so late, may need to delay the cs start
	GET_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), thisSPData.sVariations)
	//Sets the peds into their outfits
	SET_TENNIS_OUTFIT(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), GET_CURRENT_PLAYER_PED_ENUM())	// Grabbing enum, thisUIData isn't initialized yet.
	SET_PED_SWEAT(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), 0.0)
	
	//Remove the weapon from the player
	IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]))
		SET_CURRENT_PED_WEAPON(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), WEAPONTYPE_UNARMED, TRUE)
				
		IF IS_PED_IN_ANY_VEHICLE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), TRUE)					
			SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), thisProps.tennisCourt.vCenterCourt+thisProps.tennisPlayers[eOtherPed].vMyForward)
			CPRINTLN(DEBUG_TENNIS, "eOtherPed was in a vehicle, moving to center court")
		ENDIF
	ENDIF
	
	IF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY       
        IF NOT IS_PED_INJURED(FRIEND_A_PED_ID())
			SET_CURRENT_PED_WEAPON(FRIEND_A_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			IF IS_PED_IN_ANY_VEHICLE(FRIEND_A_PED_ID(), TRUE)
				SET_ENTITY_COORDS(FRIEND_A_PED_ID(), thisProps.tennisCourt.vCenterCourt+thisProps.tennisPlayers[eStarPed].vMyForward)
				CPRINTLN(DEBUG_TENNIS, "Friend was in a vehicle, moving to center court")
			ENDIF
		ENDIF
	ENDIF
	
	// Save the player's vehicle
	ODDJOB_SAVE_VEHICLE(thisSPData.playerVehicle)
	MOVE_VEHICLE_TO_SAFE_LOCATION_FOR_COURT(thisProps, thisSPData.playerVehicle.playerVehicle)
	// Clear area of vehicles, peds, and in general
	CLEAR_AREA_OF_VEHICLES(thisProps.tennisCourt.vCenterCourt, 20, FALSE, FALSE, TRUE)
	CLEAR_AREA(thisProps.tennisCourt.vCenterCourt, 20, TRUE)
	CLEAR_AREA_OF_PEDS(thisProps.tennisCourt.vCenterCourt, 20)
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(200)
	ENDIF
	
	//Set up the intro cutscene camera
	thisUIData.iSceneID = CREATE_SYNCHRONIZED_SCENE(thisProps.tennisCourt.vCenterCourt, <<0,0,GET_HEADING_FROM_VECTOR_2D(thisProps.vForward.x, thisProps.vForward.y) + 180.0>>)
	IF DOES_CAM_EXIST(thisCam.tennisCamTut)
		DESTROY_CAM(thisCam.tennisCamTut)
	ENDIF
	
	thisCam.tennisCamTut = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
	SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
	
	IF HAS_TENNIS_CHARACTER_SEEN_TUTORIAL()
		PLAY_SYNCHRONIZED_CAM_ANIM(thisCam.tennisCamTut, thisUIData.iSceneID, "tennis_ig_intro_alt1_cam", "mini@tennisintro_alt1")
	ELSE
		PLAY_SYNCHRONIZED_CAM_ANIM(thisCam.tennisCamTut, thisUIData.iSceneID, "tennis_ig_intro_cam", "mini@tennisintro")
	ENDIF
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	Force_All_Drunk_Peds_To_Become_Sober_And_Quit_Camera()
	
	CLEAR_PED_TASKS_IMMEDIATELY(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]))
	FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]))
	
	CLEAR_HELP()
	IF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY
        startActivity(thisProps.thisLocation)
        IF IS_PED_IN_ANY_VEHICLE(FRIEND_A_PED_ID())
			CLEAR_PED_TASKS_IMMEDIATELY(FRIEND_A_PED_ID())
        ENDIF
		IF NOT IS_PED_INJURED(FRIEND_A_PED_ID()) AND IS_PED_GROUP_MEMBER(FRIEND_A_PED_ID(), PLAYER_GROUP_ID())
			SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed], FRIEND_A_PED_ID())
			GET_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), thisSPData.sOppVariations)
			REMOVE_PED_FROM_GROUP(FRIEND_A_PED_ID())
		ENDIF
		//Sets the peds into their outfits
		enumCharacterList ePed = GET_PLAYER_PED_ENUM(FRIEND_A_PED_ID())
        IF (ePed = NO_CHARACTER)
            ePed = GET_NPC_PED_ENUM(FRIEND_A_PED_ID())
        ENDIF
		SET_TENNIS_OUTFIT(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), ePed)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]))
		vOffset = << -1284.62, -126.71, 44.74>>	//GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, eServeSide, thisProps.vRight, thisProps.vForward, eOtherPed = ENUM_TO_INT(eServing), eOtherPed)
		SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed], CREATE_PED(PEDTYPE_PLAYER2,thisProps.mTheOpponentModel,vOffset,GET_HEADING_FROM_VECTOR_2D(-thisProps.vForward.x,-thisProps.vForward.y)))
	ENDIF
	
	IF VALIDATE_PICK_DEFAULT_TENNIS_OPPONENT(thisProps.eTennisActivity) AND NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]))
		SET_PED_COMPONENT_VARIATION(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
		SET_PED_COMPONENT_VARIATION(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), INT_TO_ENUM(PED_COMPONENT,3), 1, 3, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
	ENDIF
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), TRUE)
	
	REQUEST_TENNIS_BANKS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]))
	
	thisProps.tennisPlayers[eOtherPed].oTennisRacket = CREATE_OBJECT(PROP_TENNIS_RACK_01B, thisProps.tennisCourt.vCourtCorners[3])
	SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eOtherPed].oTennisRacket, TRUE)
	thisProps.tennisPlayers[eStarPed].oTennisRacket = CREATE_OBJECT(PROP_TENNIS_RACK_01B, thisProps.tennisCourt.vCourtCorners[2])
	SET_ENTITY_VISIBLE(thisProps.tennisPlayers[eStarPed].oTennisRacket, TRUE)
		
	SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), TRUE)
	SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), TRUE)
	CPRINTLN(DEBUG_TENNIS, "SET_FORCE_FOOTSTEP_UPDATE TRUE")

	CLEAR_PED_TASKS_IMMEDIATELY(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]))
	IF HAS_TENNIS_CHARACTER_SEEN_TUTORIAL()
		STRING sStarPedAnim = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eStarPed]), "tennis_ig_intro_alt1_guy_b", "tennis_ig_intro_alt1_female_b")
		STRING sOtherPedAnim = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherPed]), "tennis_ig_intro_alt1_guy_a", "tennis_ig_intro_alt1_female_a")
		STRING sStarRackAnim = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eStarPed]), "tennis_ig_intro_alt1_rack_b", "tennis_ig_intro_alt1_female_rack_b")
		STRING sOtherRackAnim = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherPed]), "tennis_ig_intro_alt1_rack_a", "tennis_ig_intro_alt1_female_rack_a")
		CDEBUG2LN(DEBUG_TENNIS, "sStarPedAnim=", sStarPedAnim, ", sOtherPedAnim=", sOtherPedAnim, ", sStarRackAnim=", sStarRackAnim, ", sOtherRackAnim=", sOtherRackAnim)
		TASK_SYNCHRONIZED_SCENE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), thisUIData.iSceneID, "mini@tennisintro_alt1", sStarPedAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		TASK_SYNCHRONIZED_SCENE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), thisUIData.iSceneID, "mini@tennisintro_alt1", sOtherPedAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		PLAY_SYNCHRONIZED_ENTITY_ANIM(thisProps.tennisPlayers[eStarPed].oTennisRacket, thisUIData.iSceneID, sStarRackAnim, "mini@tennisintro_alt1", INSTANT_BLEND_IN)
		PLAY_SYNCHRONIZED_ENTITY_ANIM(thisProps.tennisPlayers[eOtherPed].oTennisRacket, thisUIData.iSceneID, sOtherRackAnim, "mini@tennisintro_alt1", INSTANT_BLEND_IN)
	ELSE
		STRING sStarPedAnim = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eStarPed]), "tennis_ig_intro_guy_b", "tennis_ig_intro_female_b")
		STRING sOtherPedAnim = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherPed]), "tennis_ig_intro_guy_a", "tennis_ig_intro_female_a")
		STRING sStarRackAnim = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eStarPed]), "tennis_ig_intro_rack_a", "tennis_ig_intro_female_rack_b")	// intro_rack_a is actually for ped b
		STRING sOtherRackAnim = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherPed]), "tennis_ig_intro_rack_b", "tennis_ig_intro_female_rack_a")	// intro_racl_b is actually for ped a
		CDEBUG2LN(DEBUG_TENNIS, "sStarPedAnim=", sStarPedAnim, ", sOtherPedAnim=", sOtherPedAnim, ", sStarRackAnim=", sStarRackAnim, ", sOtherRackAnim=", sOtherRackAnim)
		TASK_SYNCHRONIZED_SCENE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), thisUIData.iSceneID, "mini@tennisintro", sStarPedAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		TASK_SYNCHRONIZED_SCENE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), thisUIData.iSceneID, "mini@tennisintro", sOtherPedAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		PLAY_SYNCHRONIZED_ENTITY_ANIM(thisProps.tennisPlayers[eStarPed].oTennisRacket, thisUIData.iSceneID, sStarRackAnim, "mini@tennisintro", INSTANT_BLEND_IN)
		PLAY_SYNCHRONIZED_ENTITY_ANIM(thisProps.tennisPlayers[eOtherPed].oTennisRacket, thisUIData.iSceneID, sOtherRackAnim, "mini@tennisintro", INSTANT_BLEND_IN)
	ENDIF
	
	CLEAR_COURT_OF_PROPS(thisProps)
ENDPROC

PROC INCREMENT_TENNIS_TUTORIAL_STATE(TENNIS_CAMERAS &thisCam, BOOL bResetTimer=FALSE)
	SET_TENNIS_TUTORIAL_STATE(thisCam, INT_TO_ENUM(TENNIS_TUTORIAL_ENUM, (ENUM_TO_INT(thisCam.eTutCut) + 1)))
	IF bResetTimer
		RESTART_TIMER_NOW(thisCam.tennisCamTimer)
	ENDIF
ENDPROC

PROC FINISH_TENNIS_TUTORIAL(TENNIS_CAMERAS &thisCam)
	SET_TENNIS_TUTORIAL_STATE(thisCam, TUT_CLEANUP_WAIT)
	
	#IF IS_TENNIS_MULTIPLAYER
		DO_SCREEN_FADE_OUT(500)
	#ENDIF
ENDPROC

/// PURPOSE:
///    Gives a brief overview of how to play Tennis, eStarPed is the star of the tutorial and should be set to controlType COMPUTER.
///    They also need to be set to be the Home player, vectors and all, whether they actually are or not. The camera angles only face one way and the scripted hits only go one way.
///    While running the tutorial you also need to 
///    		-draw splash UI
///    		-handle bitmask UI
///    		-probably handle tennis ball flight
///    		-COMMON_EVERY_FRAME_DATA
/// PARAMS:
///    thisCam - 
///    thisProps - 
///    splashUI - 
///    eOtherPed - The TENNIS_PLAYER_ID of the ped that is not mine
///    eStarPed - The TENNIS_PLAYER_ID of my ped
///    iUtilFlags - 
///    fStageShift - Should be set to TUT_STAGE_SHIFT to offset the tutorial onto another court.
/// RETURNS:
///    TRUE when the movie is over.
FUNC BOOL IS_TENNIS_PLAYING_TUTORIAL_CUTSCENE(TENNIS_CAMERAS &thisCam, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_GAME_SP_DATA& thisSPData, TENNIS_PLAYER_ID eOtherPed, TENNIS_PLAYER_ID eStarPed)
	VECTOR vStartPos, vStartRot, vEndPos, vEndRot, vPrediction
	VECTOR vCenterCourt = thisProps.tennisCourt.vCenterCourt
	VECTOR vOffset1, vOffset2
	STRING sDict
	FLOAT fSyncPhase
	TENNIS_SCORES playerScores[TENNIS_PLAYERS]
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	
	IF NOT DOES_CAM_EXIST(thisCam.tennisCamTut)
		thisCam.tennisCamTut = CREATE_CAMERA(CAMTYPE_SCRIPTED)
	ENDIF
	
	FLOAT fCourtHeading = GET_HEADING_FROM_VECTOR_2D(thisProps.tennisCourt.vForwardNorm.x, thisProps.tennisCourt.vForwardNorm.y)
	
//	#IF IS_DEBUG_BUILD
//		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
//			INCREMENT_TENNIS_TUTORIAL_STATE(thisCam)
//		ENDIF
//	#ENDIF
	
//	CDEBUG2LN(DEBUG_TENNIS, "Timer: ", GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer))
	
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
	SWITCH GET_TENNIS_TUTORIAL_STATE(thisCam)
		CASE TUT_INIT
			IF IS_SYNCHRONIZED_SCENE_RUNNING(thisUIData.iSceneID)
				fSyncPhase = GET_SYNCHRONIZED_SCENE_PHASE(thisUIData.iSceneID)
			ENDIF
//			CDEBUG2LN(DEBUG_TENNIS, "fSyncPhase=", fSyncPhase)
			IF fSyncPhase > 0.99
				CLEAR_AREA_OF_PEDS(thisProps.tennisCourt.vCenterCourt, 15)
				CPRINTLN(DEBUG_TENNIS, "TUT_INIT")
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(200)
				ENDIF
				CPRINTLN(DEBUG_TENNIS, "Base Court Heading=", fCourtHeading)
				START_TIMER_NOW_SAFE(thisCam.tennisCamTimer)
				
				IF DOES_CAM_EXIST(thisCam.tennisCamTut)
					DESTROY_CAM(thisCam.tennisCamTut)
				ENDIF
				
				thisCam.tennisCamTut = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
				
				//Set up the intro cutscene camera
				STOP_CAM_POINTING(thisCam.tennisCamTut)
				
				RESET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
				TENNIS_COMMON_INTRO_INIT_CALLS(thisCam, thisProps, thisUIData, SERVING_FROM_RIGHT, eOtherPed, eOtherPed, eStarPed, FALSE)
				SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_SOUND_ON_FOR_TUT)
				
				CLEAR_COURT_OF_PROPS(thisProps)
					
				CPRINTLN(DEBUG_TENNIS, "Tutorial Initialized")
				IF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY
//			        startActivity(thisProps.thisLocation)
			        IF IS_PED_IN_ANY_VEHICLE(FRIEND_A_PED_ID())
						CLEAR_PED_TASKS_IMMEDIATELY(FRIEND_A_PED_ID())
			        ENDIF
					IF NOT IS_PED_INJURED(FRIEND_A_PED_ID()) AND IS_PED_GROUP_MEMBER(FRIEND_A_PED_ID(), PLAYER_GROUP_ID())
						SET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed], FRIEND_A_PED_ID())
						REMOVE_PED_FROM_GROUP(FRIEND_A_PED_ID())
					ENDIF
				ENDIF
				
				//move the ped into position while camera is panning
				IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]))
					CLEAR_PED_TASKS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]))
					
					SET_PED_GRAVITY(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), TRUE)
					CPRINTLN(DEBUG_TENNIS, "TUT_MOVE_PEDS")
					vOffset1 = ROTATE_VECTOR_ABOUT_Z((<< 0.111025, 8.76049, -0.00380015 >>), fCourtHeading)
					SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), (vCenterCourt + vOffset1))
					CPRINTLN(DEBUG_TENNIS, "vOffset1=", vOffset1)
					SET_ENTITY_HEADING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eStarPed].vMyForward.x, thisProps.tennisPlayers[eStarPed].vMyForward.y))
					vOffset1 = ROTATE_VECTOR_ABOUT_Z((<< -4.01989, -11.9028, 0.000106812 >>), fCourtHeading)
					SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), (vCenterCourt + vOffset1))
					SET_ENTITY_HEADING(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eOtherPed].vMyForward.x, thisProps.tennisPlayers[eOtherPed].vMyForward.y))
					TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), "mini@triathlon", "idle_e", INSTANT_BLEND_IN, default, default, AF_LOOPING)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), TRUE)
					CPRINTLN(DEBUG_TENNIS, "vOffset1=", vOffset1)
					
					ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eOtherPed, eStarPed, DEFAULT, TRUE)
					
					SET_RACKET_ON_GROUND_NEXT_TO_PLAYER(thisProps, eOtherPed)
					
					SET_PED_LOD_MULTIPLIER(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), DEBUG_LOD_SETTING)
					
					SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), TRUE)
					CPRINTLN(DEBUG_TENNIS, "SET_FORCE_FOOTSTEP_UPDATE TRUE")
					
					SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), TRUE)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), TRUE)
				ENDIF
				
				STRING sAnimDict
				IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]))
					sAnimDict = PICK_STRING(IS_TENNIS_PED_MALE((thisProps.tennisPlayers[eStarPed])), "mini@tennis", "mini@tennis@female")
				ENDIF
				IF HAS_ANIM_DICT_LOADED(sAnimDict)
					CPRINTLN(DEBUG_TENNIS, "Second cut, show opponent")
					vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< -2.01333, 14.6845, 2.226 >>), fCourtHeading)
					vOffset2	= ROTATE_VECTOR_ABOUT_Z((<< -2.01331, 14.7845, 1.626 >>), fCourtHeading)
					vStartPos	= vCenterCourt + vOffset1
					vStartRot	= <<-3.1146, 0.0000, -163.88428 + fCourtHeading >>
					vEndPos		= vCenterCourt + vOffset2
					vEndRot		= <<-3.1146, 0.0000, -163.88428 + fCourtHeading >>
					SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
					SET_CAM_PARAMS(thisCam.tennisCamTut, vStartPos, vStartRot, 34)
					SET_CAM_PARAMS(thisCam.tennisCamTut, vEndPos, vEndRot, 34, 5000)
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
					
					TENNIS_CREATE_LIGHTING_PROP( thisSPData, thisProps )
					
					PRINT_HELP_FOREVER("SWINGING_TUT")

					INCREMENT_TENNIS_TUTORIAL_STATE(thisCam, TRUE)
				ENDIF
			
			ELIF NOT IS_TENNIS_SP_FLAG_SET(thisSPData, TSP_PRINT_FIRST_TUT_HELP)
				PRINT_HELP("TUT_EXPL", 4400)
				SET_TENNIS_SP_FLAG(thisSPData, TSP_PRINT_FIRST_TUT_HELP)
			
			ELIF NOT IS_TENNIS_SP_FLAG_SET(thisSPData, TSP_PRINT_SECOND_TUT_HELP)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TUT_EXPL")
				PRINT_HELP_FOREVER("LETSSWING_TUT")
				SET_TENNIS_SP_FLAG(thisSPData, TSP_PRINT_SECOND_TUT_HELP)
			ENDIF
			
			STRING sContext
			IF NOT IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_INTRO_OPP_HECKLE) AND fSyncPhase > 0.24
				SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_INTRO_OPP_HECKLE)
				IF thisUIData.playerChars[eStarPed] = CHAR_AMANDA
					sContext = "MGTN_introQA"
				ELIF thisUIData.playerChars[eStarPed] = CHAR_JIMMY
					sContext = "MGTN_introQJ"
				ELIF thisUIData.playerChars[eStarPed] = CHAR_MICHAEL
					sContext = "MGTN_introQM"
				ELIF thisUIData.playerChars[eStarPed] = CHAR_TREVOR
					sContext = "MGTN_introQT"
				ELSE
					sContext = "MGTN_intro2q"
				ENDIF
				CREATE_CONVERSATION(thisUIData.myDialogueStruct, thisUIData.sSubtitleGroupID, sContext, CONV_PRIORITY_HIGH)
				CDEBUG2LN(DEBUG_TENNIS, "IS_TENNIS_PLAYING_TUTORIAL_CUTSCENE :: intro dialogue, sContext=", sContext)
			ELIF NOT IS_TENNIS_CONVO_FLAG_SET(thisUIData, TCVF_INTRO_PLYR_RETORT) AND fSyncPhase > 0.535 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				SET_TENNIS_CONVO_FLAG(thisUIData, TCVF_INTRO_PLYR_RETORT)
				sContext = PICK_STRING(thisUIData.playerChars[eOtherPed] = CHAR_MICHAEL, "GENERIC_WHATEVER_02", "GENERIC_WHATEVER_05")	// eOtherPed is the player in this case
				PLAY_TENNIS_AMBIENT_SPEECH(thisProps.tennisPlayers[eOtherPed], sContext, thisProps.tennisPlayers[eOtherPed].texVoice)
			ENDIF
		BREAK
		
		CASE TUT_DEMO_HIT_TOWARD_PED
//			CPRINTLN(DEBUG_TENNIS, "TUT_DEMO_HIT_TOWARD_PED")
			// after giving the player time to read hit the ball at the opponent, do prediction for us and them, show shot marker
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 5.0
				CPRINTLN(DEBUG_TENNIS, "hit the ball at the opponent, set their prediction")
				vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< 0.4741, -4.2480, 1.7361 >>), fCourtHeading)
				vOffset2	= ROTATE_VECTOR_ABOUT_Z((<< 0.5346, -3.6707, 1.6871 >>), fCourtHeading)
				vStartPos	= vCenterCourt + vOffset1
				vStartRot	= <<-4.9340, 0.0000, -18.2130 + fCourtHeading >>
				vEndPos		= vCenterCourt + vOffset2
				vEndRot		= <<-4.9340, 0.0000, -18.2130 + fCourtHeading >>
				
				// CALCULATE OFFSET BASED ON WORLD COORDS FOR AUTHORING!
//				vOffset1 = ROTATE_VECTOR_ABOUT_Z(<<486.5146, -222.1390, 54.5231>> - vCenterCourt, -fCourtHeading)
//				vOffset2 = ROTATE_VECTOR_ABOUT_Z(<<486.7694, -221.6174, 54.4741>> - vCenterCourt, -fCourtHeading)
//				CERRORLN(DEBUG_MISSION, "Generic offset for TUT_DEMO_HIT_TOWARD_PED camera 1: ", vOffset1)
//				CERRORLN(DEBUG_MISSION, "Generic offset for TUT_DEMO_HIT_TOWARD_PED camera 2: ", vOffset2)
//				CERRORLN(DEBUG_MISSION, "Generic heading offset for cam 1: ", -38.2687 - fCourtHeading)
//				CERRORLN(DEBUG_MISSION, "Generic heading offset for cam 2: ", -38.2687 - fCourtHeading)
//				
//				vStartPos = <<486.5146, -222.1390, 54.5231>>
//				vEndPos = <<486.7694, -221.6174, 54.4741>>
//				vStartRot = <<-4.9340, 0.0000, -38.2687>>
//				vEndRot = <<-4.9340, 0.0000, -38.2687>>
				
				// <<0.474128, -4.24802, 1.7361>>
				// <<0.534621, -3.67066, 1.6871>>
				// -18.212982
				// -18.212982
				
				SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vStartPos, vStartRot, 34.0)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vEndPos, vEndRot, 34.0, 1700, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				vOffset1 = ROTATE_VECTOR_ABOUT_Z((<< 2.12135, -10.6487, 0.6092 >>), fCourtHeading)
				SCRIPTED_TENNIS_BALL_HIT(thisProps.tennisPlayers[eOtherPed], thisProps, thisProps.grades[LD_MID], SHOT_NORMAL, (vCenterCourt + vOffset1))
				SET_AI_DIFFICULTY(thisProps.tennisPlayers[eStarPed].playerAI, AD_EASY)	//AD_TUTORIAL
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam, TRUE)
				CPRINTLN(DEBUG_TENNIS, "vOffset1=", vOffset1)
			ENDIF
		BREAK
		
		CASE TUT_DEMO_SWING_PREDICTION
//			CPRINTLN(DEBUG_TENNIS, "TUT_DEMO_SWING_PREDICTION")
			// Now that the ball was hit, set prediction points for both playerAI, the AI so it can hit back and the player's so the marker shows correctly
			vPrediction = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(
				thisProps.tennisCourt,
				GET_TENNIS_BALL_POS(thisProps.tennisBall), 
				thisProps.tennisBall.vBallVel,
				thisProps.vForward,
				GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
			SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[eOtherPed], vPrediction)
			SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_X_MARKER)
			vPrediction = vPrediction - (thisProps.tennisPlayers[eStarPed].vMyForward * AI_PREDICTION_SCALAR)
			SET_AI_PREDICTION_POINT(thisProps.tennisPlayers[eStarPed].playerAI, vPrediction)
			SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eStarPed].playerAI, ASE_AI_MOVING_TO_BALL)
			INCREMENT_TENNIS_TUTORIAL_STATE(thisCam)
		BREAK
		
		CASE TUT_DEMO_SWING_CUT_TO_MARKERS
//			CPRINTLN(DEBUG_TENNIS, "TUT_DEMO_SWING_CUT_TO_MARKERS")
			// Give the opponent enough time to chase the ball and hit it back, cut after ball goes offscreen
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eStarPed], thisProps, SERVING_FROM_RIGHT, eStarPed)
			UPDATE_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eStarPed], thisProps, playerScores, eOtherPed, eStarPed, TENNIS_PLAYER_AWAY, SERVING_FROM_RIGHT, FALSE)
			
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 1.7
				CPRINTLN(DEBUG_TENNIS, "Explain Base shot bounces")
				vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< -1.47647, 5.10748, 2.4931 >>), fCourtHeading)
				vOffset2	= ROTATE_VECTOR_ABOUT_Z((<< -1.3262, 4.42321, 2.354 >>), fCourtHeading)
				vStartPos	= vCenterCourt + vOffset1
				vStartRot	= <<-11.2307, 0.0000, -167.621979 + fCourtHeading >>
				vEndPos		= vCenterCourt + vOffset2
				vEndRot		= <<-11.2307, 0.0000, -167.621979 + fCourtHeading >>
				
				// CALCULATE OFFSET BASED ON WORLD COORDS FOR AUTHORING!
//				vOffset1 = ROTATE_VECTOR_ABOUT_Z(<<487.8906, -212.6819, 55.2801>> - vCenterCourt, -fCourtHeading)
//				vOffset2 = ROTATE_VECTOR_ABOUT_Z(<<487.7971, -213.3762, 55.1410>> - vCenterCourt, -fCourtHeading)
//				CERRORLN(DEBUG_MISSION, "Generic offset for TUT_DEMO_SWING_CUT_TO_MARKERS camera 1: ", vOffset1)
//				CERRORLN(DEBUG_MISSION, "Generic offset for TUT_DEMO_SWING_CUT_TO_MARKERS camera 2: ", vOffset2)
//				CERRORLN(DEBUG_MISSION, "Generic heading offset for cam 1: ", 172.3223 - fCourtHeading)
//				CERRORLN(DEBUG_MISSION, "Generic heading offset for cam 2: ", 172.3223 - fCourtHeading)
//				
//				vStartPos = <<487.8906, -212.6819, 55.2801>>
//				vEndPos = <<487.7971, -213.3762, 55.1410>>
//				vStartRot = <<-11.2307, 0.0000, 172.3223>>
//				vEndRot = <<-11.2307, 0.0000, 172.3223>>
				
				SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vStartPos, vStartRot, 34.0)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vEndPos, vEndRot, 34.0, 3660, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				CPRINTLN(DEBUG_TENNIS, "vOffset1=", vOffset1)
				CPRINTLN(DEBUG_TENNIS, "vOffset2=", vOffset2)
				CPRINTLN(DEBUG_TENNIS, "vStartRot=", vStartRot)
				CPRINTLN(DEBUG_TENNIS, "vEndRot=", vEndRot)
				
				vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< 1.98294, 6.55033, 0.6092 >>), fCourtHeading)
				SCRIPTED_TENNIS_BALL_HIT(thisProps.tennisPlayers[eStarPed], thisProps, thisProps.grades[LD_MID], SHOT_NORMAL, (vCenterCourt + vOffset1), FALSE)
				CPRINTLN(DEBUG_TENNIS, "vOffset1=", vOffset1)
				
				SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eStarPed].playerAI, ASE_AI_IDLE)
				
				PRINT_HELP_FOREVER("BASESHOT_TUT")
				
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam, TRUE)
			ENDIF
		BREAK
		
		CASE TUT_SET_GREEN_MARKER
//			CPRINTLN(DEBUG_TENNIS, "TUT_SET_GREEN_MARKER")
			// Set prediction after the hit so our marker shows up correctly
			vPrediction = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(
				thisProps.tennisCourt,
				GET_TENNIS_BALL_POS(thisProps.tennisBall), 
				thisProps.tennisBall.vBallVel,
				thisProps.vForward,
				GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
			SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[eOtherPed], vPrediction)
			SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_X_MARKER)
			INCREMENT_TENNIS_TUTORIAL_STATE(thisCam)
		BREAK
		
		CASE TUT_CUT_TO_RED_MARKER
//			CPRINTLN(DEBUG_TENNIS, "TUT_CUT_TO_RED_MARKER")
			// Let the BASESHOT explanation sink in, then explain a topspin shot, zoom to a better view
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 3.66
				CPRINTLN(DEBUG_TENNIS, "Explain Topspin shot bounces")
				vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< -7.05529, -12.6506, 1.4227 >>), fCourtHeading)
				vOffset2	= ROTATE_VECTOR_ABOUT_Z((<< -6.71516, -12.4364, 1.3693 >>), fCourtHeading)
				vStartPos	= vCenterCourt + vOffset1
				vStartRot	= <<-7.5662, -0.0000, -57.8045 + fCourtHeading >>
				vEndPos		= vCenterCourt + vOffset2
				vEndRot		= <<-7.5662, -0.0000, -57.8045 + fCourtHeading >>
				
				// CALCULATE OFFSET BASED ON WORLD COORDS FOR AUTHORING!
//				vOffset1 = ROTATE_VECTOR_ABOUT_Z(<<476.5602, -227.4500, 54.2097>> - vCenterCourt, -fCourtHeading)
//				vOffset2 = ROTATE_VECTOR_ABOUT_Z(<<476.9532, -227.3654, 54.1563>> - vCenterCourt, -fCourtHeading)
//				CERRORLN(DEBUG_MISSION, "Generic offset for TUT_CUT_TO_RED_MARKER camera 1: ", vOffset1)
//				CERRORLN(DEBUG_MISSION, "Generic offset for TUT_CUT_TO_RED_MARKER camera 2: ", vOffset2)
//				CERRORLN(DEBUG_MISSION, "Generic heading offset for cam 1: ", -77.8602 - fCourtHeading)
//				CERRORLN(DEBUG_MISSION, "Generic heading offset for cam 2: ", -77.8602 - fCourtHeading)
//				
//				vStartPos = <<476.5602, -227.4500, 54.2097>>
//				vEndPos = <<476.9532, -227.3654, 54.1563>>
//				vStartRot = <<-7.5662, -0.0000, -77.8602>>
//				vEndRot = <<-7.5662, -0.0000, -77.8602>>
				
				SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vStartPos, vStartRot, 34.0)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vEndPos, vEndRot, 34.0, 7792, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				CPRINTLN(DEBUG_TENNIS, "vOffset1=", vOffset1)
				CPRINTLN(DEBUG_TENNIS, "vOffset2=", vOffset2)
				CPRINTLN(DEBUG_TENNIS, "vStartRot=", vStartRot)
				CPRINTLN(DEBUG_TENNIS, "vEndRot=", vEndRot)
				
				PRINT_HELP_FOREVER("TOPSPIN_TUT")
				
				DEBUG_SPAWN_BALL(thisProps, TRUE)
				
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam, TRUE)
			ELIF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 2.0 AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_SOUND_ON_FOR_TUT)
			ENDIF
		BREAK
		
		CASE TUT_DEMO_TOPSPIN
//			CPRINTLN(DEBUG_TENNIS, "TUT_DEMO_TOPSPIN")
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 4.0
				CPRINTLN(DEBUG_TENNIS, "TUT_DEMO_TOPSPIN")
				vOffset1 = ROTATE_VECTOR_ABOUT_Z((<< 1.98294, 6.55033, 0.6092 >>), fCourtHeading)
				CPRINTLN(DEBUG_TENNIS, "vOffset1=", vOffset1)
				SCRIPTED_TENNIS_BALL_HIT(thisProps.tennisPlayers[eStarPed], thisProps, thisProps.grades[LD_MID], SHOT_TOPSPIN, (vCenterCourt + vOffset1))
				CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam)
				SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_SOUND_ON_FOR_TUT)
			ENDIF
		BREAK
		
		CASE TUT_PREDICT_TOPSPIN_MARKER
//			CPRINTLN(DEBUG_TENNIS, "TUT_PREDICT_TOPSPIN_MARKER")
			// Set prediction after the hit so our marker shows up correctly
			vPrediction = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(
				thisProps.tennisCourt,
				GET_TENNIS_BALL_POS(thisProps.tennisBall), 
				thisProps.tennisBall.vBallVel,
				thisProps.vForward,
				GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
			SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[eOtherPed], vPrediction)
			SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_X_MARKER)
			INCREMENT_TENNIS_TUTORIAL_STATE(thisCam)
		BREAK
		
		CASE TUT_CUT_TO_BACKSPIN_MARKER
//			CPRINTLN(DEBUG_TENNIS, "TUT_CUT_TO_BACKSPIN_MARKER")
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 7.792
				CPRINTLN(DEBUG_TENNIS, "Explain Backspin shot bounces")
				vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< -2.23175, -3.5811, 2.045 >>), fCourtHeading)
				vOffset2	= ROTATE_VECTOR_ABOUT_Z((<< -1.74695, -4.27046, 1.8589 >>), fCourtHeading)
				vStartPos	= vCenterCourt + vOffset1
				vStartRot	= <<-12.4513, -0.0000, -144.895172 + fCourtHeading>>
				vEndPos		= vCenterCourt + vOffset2
				vEndRot		= <<-12.4513, -0.0000, -144.895172 + fCourtHeading >>
				
				// CALCULATE OFFSET BASED ON WORLD COORDS FOR AUTHORING!
//				vOffset1 = ROTATE_VECTOR_ABOUT_Z(<<484.2015, -220.5846, 54.8320>> - vCenterCourt, -fCourtHeading)
//				vOffset2 = ROTATE_VECTOR_ABOUT_Z(<<484.4205, -221.3984, 54.6459>> - vCenterCourt, -fCourtHeading)
//				CERRORLN(DEBUG_MISSION, "Generic offset for TUT_CUT_TO_BACKSPIN_MARKER camera 1: ", vOffset1)
//				CERRORLN(DEBUG_MISSION, "Generic offset for TUT_CUT_TO_BACKSPIN_MARKER camera 2: ", vOffset2)
//				CERRORLN(DEBUG_MISSION, "Generic heading offset for cam 1: ", -164.9509 - fCourtHeading)
//				CERRORLN(DEBUG_MISSION, "Generic heading offset for cam 2: ", -164.9509 - fCourtHeading)
//				
//				vStartPos = <<484.2015, -220.5846, 54.8320>>
//				vEndPos = <<484.4205, -221.3984, 54.6459>>
//				vStartRot = <<-12.4513, -0.0000, -164.9509>>
//				vEndRot = <<-12.4513, -0.0000, -164.9509>>
				
				SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vStartPos, vStartRot, 34.0)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vEndPos, vEndRot, 34.0, 7231, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				CPRINTLN(DEBUG_TENNIS, "vOffset1=", vOffset1)
				CPRINTLN(DEBUG_TENNIS, "vOffset2=", vOffset2)
				CPRINTLN(DEBUG_TENNIS, "vStartRot=", vStartRot)
				CPRINTLN(DEBUG_TENNIS, "vEndRot=", vEndRot)
				
				PRINT_HELP_FOREVER("BACKSPIN_TUT")
				
				DEBUG_SPAWN_BALL(thisProps, TRUE)
				
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam, TRUE)
			ELIF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 6.0 AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_SOUND_ON_FOR_TUT)
			ENDIF
		BREAK
		
		CASE TUT_DEMO_BACKSPIN
//			CPRINTLN(DEBUG_TENNIS, "TUT_DEMO_BACKSPIN")
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 4.0
				SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_SOUND_ON_FOR_TUT)
				CPRINTLN(DEBUG_TENNIS, "TUT_DEMO_BACKSPIN")
				vOffset1	= ROTATE_VECTOR_ABOUT_Z((<< 1.98294, 6.55033, 0.6092 >>), fCourtHeading)
				CPRINTLN(DEBUG_TENNIS, "vOffset1=", vOffset1)
				SCRIPTED_TENNIS_BALL_HIT(thisProps.tennisPlayers[eStarPed], thisProps, thisProps.grades[LD_MID], SHOT_BACKSPIN, (vCenterCourt + vOffset1))
				CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam)
			ENDIF
		BREAK
		
		CASE TUT_PREDICT_BACKSPIN_MARKER
//			CPRINTLN(DEBUG_TENNIS, "TUT_PREDICT_BACKSPIN_MARKER")
			// Set prediction after the hit so our marker shows up correctly
			vPrediction = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(
				thisProps.tennisCourt,
				GET_TENNIS_BALL_POS(thisProps.tennisBall), 
				thisProps.tennisBall.vBallVel,
				thisProps.vForward,
				GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
			SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[eOtherPed], vPrediction)
			SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_X_MARKER)
			INCREMENT_TENNIS_TUTORIAL_STATE(thisCam)
		BREAK
		
		CASE TUT_UPDATE_BACKSPIN_CUT_STRATS
//			CPRINTLN(DEBUG_TENNIS, "TUT_UPDATE_BACKSPIN_CUT_STRATS")
			// Update the bounce count so the backspin gets applied correctly
			UPDATE_BOUNCE_COUNT(thisProps, eStarPed)
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 7.231
				CPRINTLN(DEBUG_TENNIS, "TUT_UPDATE_BACKSPIN_CUT_STRATS, show opponent")
				
				vOffset1	= ROTATE_VECTOR_ABOUT_Z(<< -4.28439, -14.3705, 1.06731 >>, fCourtHeading)
				vOffset2	= ROTATE_VECTOR_ABOUT_Z(<< -4.08069, -13.9911, 1.0531 >>, fCourtHeading)
				vStartPos	= vCenterCourt + vOffset1
				vStartRot	= <<-1.9039, 0.0000, -28.424164 + fCourtHeading>>
				vEndPos		= vCenterCourt + vOffset2
				vEndRot		= <<-1.9039, 0.0000, -27.146362 + fCourtHeading>>
				
				SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vStartPos, vStartRot, 34.0)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vEndPos, vEndRot, 34.0, 2096, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				PRINT_HELP_FOREVER("STRATS_TUT")
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam, TRUE)
				ATTACH_TENNIS_BALL_TO_PLAYER(thisProps.tennisPlayers[eOtherPed], thisProps.tennisBall)
			ELIF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 5.4 AND NOT IS_BITMASK_AS_ENUM_SET(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
			ELIF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 4.667 AND NOT IS_TENNIS_SP_FLAG_SET(thisSPData, TSP_TUT_SERVE_SET)
				SET_TENNIS_SP_FLAG(thisSPData, TSP_TUT_SERVE_SET)
				vOffset1 = ROTATE_VECTOR_ABOUT_Z((<< -2.22121, -11.8614, 0.0895996 >>), fCourtHeading)
				SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), vCenterCourt + vOffset1)
				ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eOtherPed, eStarPed)
				TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), "mini@tennis", "serve")
			ENDIF
		BREAK
		
		CASE TUT_HIT_STRATS_BALL_1
//			CPRINTLN(DEBUG_TENNIS, "TUT_HIT_STRATS_BALL_1")
			// Give one last tip and some encouragement
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 1.08
				CPRINTLN(DEBUG_TENNIS, "TUT_HIT_STRATS_BALL_1")
				vOffset1 = GET_ENTITY_COORDS(thisProps.tennisBall.oBall)	//ROTATE_VECTOR_ABOUT_Z((<< -2.17613, -8.89807, 1.0923 >>), fCourtHeading)
				CPRINTLN(DEBUG_TENNIS, "vOffset1=", vOffset1)
				SCRIPTED_TENNIS_BALL_HIT(thisProps.tennisPlayers[eOtherPed], thisProps, thisProps.grades[LD_SERVE_B], SHOT_NORMAL, vOffset1)	//(vCenterCourt + vOffset1))
				CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam)
			ENDIF
		BREAK
		
		CASE TUT_PREDICT_STRATS_BALL_1
//			CPRINTLN(DEBUG_TENNIS, "TUT_PREDICT_STRATS_BALL_1")
			// Now that the ball was hit, set prediction points for both playerAI, the AI so it can hit back and the player's so the marker shows correctly
			vPrediction = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(
				thisProps.tennisCourt,
				GET_TENNIS_BALL_POS(thisProps.tennisBall), 
				thisProps.tennisBall.vBallVel,
				thisProps.vForward,
				GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
			SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[eOtherPed], vPrediction)
			SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_X_MARKER)
			vPrediction = vPrediction - (thisProps.tennisPlayers[eStarPed].vMyForward * AI_PREDICTION_SCALAR)
			SET_AI_PREDICTION_POINT(thisProps.tennisPlayers[eStarPed].playerAI, vPrediction)
			SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eStarPed].playerAI, ASE_AI_MOVING_TO_BALL)
			
			INCREMENT_TENNIS_TUTORIAL_STATE(thisCam)
		BREAK
		
		CASE TUT_CUT_BETWEEN_STRATS
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eStarPed], thisProps, SERVING_FROM_RIGHT, eStarPed)
			UPDATE_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eStarPed], thisProps, playerScores, eOtherPed, eStarPed, TENNIS_PLAYER_AWAY, SERVING_FROM_RIGHT, FALSE)
			
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 2.096985
				CPRINTLN(DEBUG_TENNIS, "TUT_CUT_BETWEEN_STRATS")
				vOffset1	= ROTATE_VECTOR_ABOUT_Z(<< 6.04321, -1.55509, 1.70601 >>, fCourtHeading)
				vOffset2	= ROTATE_VECTOR_ABOUT_Z(<< 5.44394, -0.383772, 1.6055 >>, fCourtHeading)
				vStartPos	= vCenterCourt + vOffset1
				vStartRot	= <<-4.3693, 0.0000, -330.279663 + fCourtHeading>>
				vEndPos		= vCenterCourt + vOffset2
				vEndRot		= <<-4.3693, 0.0000, -331.690765 + fCourtHeading>>
				
				SET_CAM_ACTIVE(thisCam.tennisCamTut, TRUE)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vStartPos, vStartRot, 34.0)
				SET_CAM_PARAMS(thisCam.tennisCamTut, vEndPos, vEndRot, 34.0, 6500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam, TRUE)
			ENDIF
		BREAK
		
		CASE TUT_HIT_STRATS_BALL_2
//			CPRINTLN(DEBUG_TENNIS, "TUT_HIT_STRATS_BALL_2")
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eStarPed], thisProps, SERVING_FROM_RIGHT, eStarPed)
			UPDATE_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eStarPed], thisProps, playerScores, eOtherPed, eStarPed, TENNIS_PLAYER_AWAY, SERVING_FROM_RIGHT, FALSE)
			
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 1.6
				CPRINTLN(DEBUG_TENNIS, "TUT_HIT_STRATS_BALL_2")
				vOffset1 = ROTATE_VECTOR_ABOUT_Z((<< 2.41032, -9.10167, 1.9262 >>), fCourtHeading)
				CPRINTLN(DEBUG_TENNIS, "vOffset1=", vOffset1)
				SCRIPTED_TENNIS_BALL_HIT(thisProps.tennisPlayers[eOtherPed], thisProps, thisProps.grades[LD_OVERHEAD], SHOT_TOPSPIN, (vCenterCourt + vOffset1))
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam, TRUE)
			ENDIF
		BREAK
		
		CASE TUT_PREDICT_STRATS_BALL_2
//			CPRINTLN(DEBUG_TENNIS, "TUT_PREDICT_STRATS_BALL_2")
			// Now that the ball was hit, set prediction points for both playerAI, the AI so it can hit back and the player's so the marker shows correctly
			vPrediction = SIMULATE_TENNIS_BALL_FIRST_BOUNCE(
				thisProps.tennisCourt,
				GET_TENNIS_BALL_POS(thisProps.tennisBall), 
				thisProps.tennisBall.vBallVel,
				thisProps.vForward,
				GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN(thisProps.tennisBall),
				GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall))
			SET_TENNIS_PLAYER_MARKER_POS(thisProps.tennisPlayers[eOtherPed], vPrediction)
			SET_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_X_MARKER)
			vPrediction = vPrediction - (thisProps.tennisPlayers[eStarPed].vMyForward * AI_PREDICTION_SCALAR)
			SET_AI_PREDICTION_POINT(thisProps.tennisPlayers[eStarPed].playerAI, vPrediction)
			SET_TENNIS_AI_STATE(thisProps.tennisPlayers[eStarPed].playerAI, ASE_AI_MOVING_TO_BALL)
			
			INCREMENT_TENNIS_TUTORIAL_STATE(thisCam, TRUE)
		BREAK
		
		CASE TUT_CLOSE_UP_RETURN
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eStarPed], thisProps, SERVING_FROM_RIGHT, eStarPed)
			UPDATE_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eStarPed], thisProps, playerScores, eOtherPed, eStarPed, TENNIS_PLAYER_AWAY, SERVING_FROM_RIGHT, FALSE)
			
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 0.364
				CPRINTLN(DEBUG_TENNIS, "TUT_CLOSE_UP_RETURN, show opponent")
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam)
			ENDIF
		BREAK
		
		CASE TUT_FINAL_HIT_GLHF
//			CPRINTLN(DEBUG_TENNIS, "TUT_FINAL_HIT_GLHF")
			UPDATE_TENNIS_PLAYER_MOVER(thisProps.tennisPlayers[eStarPed], thisProps, SERVING_FROM_RIGHT, eStarPed)
			UPDATE_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[eStarPed], thisProps, playerScores, eOtherPed, eStarPed, TENNIS_PLAYER_AWAY, SERVING_FROM_RIGHT, FALSE)
			
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 4.4
				INCREMENT_TENNIS_TUTORIAL_STATE(thisCam, TRUE)
			ELIF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 2.4
				PRINT_HELP_FOREVER("GLHF_TUT")
			ENDIF
		BREAK
		
		CASE TUT_FINISH_INTRO_THEN_WAIT
			IF GET_TIMER_IN_SECONDS(thisCam.tennisCamTimer) > 3.0
				SET_TENNIS_TUTORIAL_STATE(thisCam, TUT_WAIT_FOR_OTHER_PLAYER)
//			ELSE
//				UPDATE_TENNIS_BIG_MESSAGE(splashUI)
			ENDIF
		BREAK
		
		CASE TUT_WAIT_FOR_OTHER_PLAYER
			// Do nothing, just wait, the tutorial will finish because both players bitmasks are set outside this function.
		BREAK
		
		CASE TUT_FINISH
//			CPRINTLN(DEBUG_TENNIS, "TUT_FINISH")
			FINISH_TENNIS_TUTORIAL(thisCam)
		BREAK
		
		// Wait in this state for a frame so we don't make back to back calls of FORCE_PED_AI_AND_ANIMATION_UPDATE
		CASE TUT_CLEANUP_WAIT
			INCREMENT_TENNIS_TUTORIAL_STATE(thisCam, TRUE)
		BREAK
		
		CASE TUT_CLEANUP
//			CPRINTLN(DEBUG_TENNIS, "TUT_CLEANUP")
			IF NOT IS_SCREEN_FADING_OUT()
//				VECTOR vEndCutPos, vEndCutRot, vDummy
//							
//				GET_TENNIS_INTRO_CAMERA_VECTORS(
//					thisProps.tennisCourt.eTennisCourt,
//					vDummy, vDummy,
//					vDummy, vDummy,
//					vDummy, vDummy,
//					vDummy, vDummy,
//					vDummy, vDummy,
//					vDummy, vDummy,
//					vEndCutPos, vEndCutRot)
		
				CLEAR_PRINTS()
				CLEAR_HELP(TRUE)
				
//				SET_CAM_PARAMS(thisCam.tennisCamTut, vEndCutPos, vEndCutRot, 45.000)
				SETUP_TENNIS_COURT_SHADOWS()
				ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eOtherPed, eStarPed)
				SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), 
					GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, SERVING_FROM_RIGHT, thisProps.vRight, thisProps.vForward, TRUE, eOtherPed))
				thisProps.tennisPlayers[eOtherPed].vPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]))
				SET_ENTITY_HEADING(thisProps.tennisPlayers[eOtherPed].pIndex, GET_HEADING_FROM_VECTOR_2D(thisProps.vForward.x,thisProps.vForward.y))
				SET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), 
					GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, SERVING_FROM_RIGHT, thisProps.vRight, thisProps.vForward, FALSE, eStarPed), TRUE, TRUE, TRUE)
				thisProps.tennisPlayers[eStarPed].vPos = GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]))
				SET_ENTITY_HEADING(thisProps.tennisPlayers[eStarPed].pIndex, GET_HEADING_FROM_VECTOR_2D(-thisProps.vForward.x,-thisProps.vForward.y))
				sDict = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eStarPed]), "mini@tennis", "mini@tennis@female")
				IF NOT IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), sDict, "intro_a")
					TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), sDict, "intro_a", INSTANT_BLEND_IN, default, default, AF_LOOPING)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eStarPed]), TRUE)
				ENDIF
				sDict = PICK_STRING(IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherPed]), "mini@tennis", "mini@tennis@female")
				IF NOT IS_ENTITY_PLAYING_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), sDict, "intro_b")
					TASK_PLAY_ANIM(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), sDict, "intro_b", INSTANT_BLEND_IN, default, default, AF_LOOPING)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), TRUE)
				ENDIF
								
				SET_FORCE_FOOTSTEP_UPDATE(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherPed]), FALSE)
				CPRINTLN(DEBUG_TENNIS, "SET_FORCE_FOOTSTEP_UPDATE FALSE")
				
				STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
				DEBUG_SPAWN_BALL(thisProps)
				
				UPDATE_TENNIS_WANDERING_MENU_CAMERA(thisProps, thisUIData)
				
				BOOL bSelectDiff
				bSelectDiff = NOT IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP) AND thisProps.eTennisActivity <> TA_FRIEND_ACTIVITY
				BUILD_TENNIS_INTRO_MENU(thisUIData, bSelectDiff)
				
				CLEAR_BITMASK_AS_ENUM(thisUIData.iUtilFlags, TD_ANIM_PLAYING)
				
//				SET_TENNIS_CHARACTER_SEEN_TUTORIAL()
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

PROC TENNIS_ALIGN_SCTV_NORMALS_WITH_COURTSIDES( TENNIS_SERVER_BD &serverBD, TENNIS_GAME_PROPERTIES & thisProps )
	
	IF NOT TENNIS_IS_PLAYER_SPECTATING()
		EXIT
	ENDIF
	
	IF NOT IS_TENNIS_SERVER_FLAG_SET( serverBD, TMS_SIDES_ARE_CHANGED )
	    thisProps.tennisPlayers[0].vMyRight   = thisProps.tennisCourt.vRightNorm
	    thisProps.tennisPlayers[0].vMyForward = thisProps.tennisCourt.vForwardNorm
	    thisProps.tennisPlayers[0].eCourtSide = TENNIS_PLAYER_AWAY
	    thisProps.tennisPlayers[1].vMyRight   = thisProps.tennisCourt.vRightNorm * -1.0
	    thisProps.tennisPlayers[1].vMyForward = thisProps.tennisCourt.vForwardNorm * -1.0
		thisProps.tennisPlayers[1].eCourtSide = TENNIS_PLAYER_HOME
	ELSE
	    thisProps.tennisPlayers[0].vMyRight   = thisProps.tennisCourt.vRightNorm * -1.0
	    thisProps.tennisPlayers[0].vMyForward = thisProps.tennisCourt.vForwardNorm * -1.0
	    thisProps.tennisPlayers[0].eCourtSide = TENNIS_PLAYER_HOME
	    thisProps.tennisPlayers[1].vMyRight   = thisProps.tennisCourt.vRightNorm
	    thisProps.tennisPlayers[1].vMyForward = thisProps.tennisCourt.vForwardNorm
	    thisProps.tennisPlayers[1].eCourtSide = TENNIS_PLAYER_AWAY
	ENDIF
	
ENDPROC

PROC TENNIS_MP_PROCESS_CAMERA_EVENTS( TENNIS_SERVER_BD &serverBD, TENNIS_CLIENT_BD &clientBD, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_UI_DATA &thisUIData, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, TENNIS_PARTICIPANT_INFO &info )
	TENNIS_BROADCAST_EVENT eventDetails
	INT iCount
	INT iSCTVFocusParticipantID = NATIVE_TO_INT( NETWORK_GET_HOST_OF_THIS_SCRIPT() )
	
	IF TENNIS_IS_PLAYER_SPECTATING()
		IF IS_ENTITY_A_PED( GET_SPECTATOR_CURRENT_FOCUS_PED() )
			iSCTVFocusParticipantID = NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX( NETWORK_GET_PLAYER_INDEX_FROM_PED( GET_SPECTATOR_CURRENT_FOCUS_PED() ) ) )
			CDEBUG3LN( DEBUG_TENNIS, "TENNIS_MP_PROCESS_CAMERA_EVENTS - iSCTVFocusParticipantID = GET_SPECTATOR_CURRENT_FOCUS_PED - iSCTVFocusParticipantID = ", iSCTVFocusParticipantID)
		ELIF IS_ENTITY_A_PED( GET_SPECTATOR_DESIRED_FOCUS_PED() )
			iSCTVFocusParticipantID = NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX( NETWORK_GET_PLAYER_INDEX_FROM_PED( GET_SPECTATOR_DESIRED_FOCUS_PED() ) ) )
			CDEBUG3LN( DEBUG_TENNIS, "TENNIS_MP_PROCESS_CAMERA_EVENTS - iSCTVFocusParticipantID = GET_SPECTATOR_CURRENT_FOCUS_PED - iSCTVFocusParticipantID = ", iSCTVFocusParticipantID)
		ELSE
			CDEBUG3LN( DEBUG_TENNIS, "TENNIS_MP_PROCESS_CAMERA_EVENTS - iSCTVFocusParticipantID = NATIVE_TO_INT( NETWORK_GET_HOST_OF_THIS_SCRIPT() ) ")
		ENDIF
	ENDIF
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		// We just got an event we may care about.
		IF (GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount) = EVENT_NETWORK_SCRIPT_EVENT)
			GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, eventDetails, SIZE_OF(eventDetails))
			
			// Check for the event that tells us to apply a force to the ball.
			// might need to do spin data stuff later
			IF GET_TENNIS_BROADCAST_EVENT_TYPE(eventDetails) = SCRIPT_EVENT_BALL_UPDATE AND eventDetails.eBallUpdateType = BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SET_CAMERA_STATE
				IF TENNIS_IS_PLAYER_SPECTATING()
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SET_CAMERA_STATE caught from ", eventDetails.iLRAngle )
					CPRINTLN(DEBUG_TENNIS, "BALL_UPDATE_IMPACT_TYPE_SCTV_UI_SET_CAMERA_STATE iSCTVFocusParticipantID = ", iSCTVFocusParticipantID )
					IF eventDetails.iLRAngle = iSCTVFocusParticipantID
						SET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras, INT_TO_ENUM( CAMERA_STATE_ENUM, eventDetails.iDummy0 ), eventDetails.bDummy)
						IF INT_TO_ENUM( CAMERA_STATE_ENUM, eventDetails.iDummy0 ) = TENNIS_CAMERA_SIDE_CHANGE
							DISPLAY_PLAYER_CARD(thisUIData.tennisUI.scorepanelSF, FALSE)
							DISPLAY_SCOREBOARD(thisUIData.tennisUI.scorepanelSF, FALSE)
							
							// Locally change the courtside of our ped
							IF thisProps.tennisPlayers[eSelfID].eCourtSide = TENNIS_PLAYER_AWAY
								thisProps.tennisPlayers[eSelfID].eCourtSide = TENNIS_PLAYER_HOME
								thisProps.tennisPlayers[eOtherID].eCourtSide = TENNIS_PLAYER_AWAY
							ELSE
								thisProps.tennisPlayers[eSelfID].eCourtSide = TENNIS_PLAYER_AWAY
								thisProps.tennisPlayers[eotherID].eCourtSide = TENNIS_PLAYER_HOME
							ENDIF
						    thisProps.tennisPlayers[0].vMyRight = thisProps.tennisPlayers[0].vMyRight * -1.0
						    thisProps.tennisPlayers[0].vMyForward = thisProps.tennisPlayers[0].vMyForward * -1.0
						    thisProps.tennisPlayers[1].vMyRight = thisProps.tennisPlayers[1].vMyRight * -1.0
						    thisProps.tennisPlayers[1].vMyForward = thisProps.tennisPlayers[1].vMyForward * -1.0
							
							TENNIS_ACTIVATE_SIDE_CHANGE_CAMERA_SP(thisProps.tennisCourt, thisUIData.tennisCameras, thisProps.tennisPlayers[eSelfID].eCourtSide)
							
							VECTOR vBallWillBe
							TENNIS_PLAYER_ID eServer, eCourtSide
							SERVING_SIDE_ENUM eServingSide
							eServer = GET_TENNIS_MP_WHO_IS_SERVING(serverBD)
							eCourtSide = thisProps.tennisPlayers[eSelfID].eCourtSide
							eServingSide = GET_TENNIS_MP_SERVING_SIDE(serverBD)
							IF eCourtSide = TENNIS_PLAYER_AWAY
								vBallWillBe = PICK_VECTOR(eServingSide = SERVING_FROM_RIGHT, thisProps.tennisCourt.vCourtCorners[1], thisProps.tennisCourt.vCourtCorners[0])
							ELSE
								vBallWillBe = PICK_VECTOR(eServingSide = SERVING_FROM_RIGHT, thisProps.tennisCourt.vCourtCorners[3], thisProps.tennisCourt.vCourtCorners[2])
							ENDIF
							DEBUG_SPAWN_BALL( thisProps )
							ATTACH_TENNIS_BALL_TO_PLAYER(thisProps.tennisPlayers[eServer], thisProps.tennisBall)
							// Prime the dynamic camera after a side change, MP
							ADJUST_TENNIS_CAMERA_DYNAMIC(thisUIData.tennisCameras.tennisCamDyna, GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt),
								PICK_VECTOR(thisProps.tennisPlayers[eSelfID].eCourtSide = (TENNIS_PLAYER_AWAY), thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisCourt.vCourtCorners[2]),
								thisProps.tennisPlayers[eSelfID].vMyRight, thisProps.tennisPlayers[eSelfID].vMyForward, vBallWillBe - thisProps.tennisCourt.vCenterCourt,
								thisUIData.tennisCameras.fDynaCamSwing, DEFAULT_DYNA_DIST, thisUIData.tennisCameras.fDynaDolly, thisUIData.tennisCameras.fDynaNudgeRSLR, thisUIData.tennisCameras.fDynaNudgeRSUD, FALSE, TRUE)
							// Set cloned peds to walk, hide our peds, attach rackets and balls to hands
							SET_TENNIS_CLIENT_FLAG(clientBD, TMC_MAKE_PLAYERS_INVISIBLE)
							SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( info.iPlayers[eSelfID] ) ) )
							SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( info.iPlayers[eOtherID] ) ) )
							TENNIS_SIDE_CHANGE_CLIENT_CLONE(thisProps, clientBD, eSelfID)
							TENNIS_SIDE_CHANGE_CLIENT_CLONE(thisProps, clientBD, eOtherID)
							IF NOT IS_PED_INJURED(clientBD.pedClones[eSelfID])
								ATTACH_ENTITY_TO_ENTITY(thisProps.tennisPlayers[eSelfID].oTennisRacket, clientBD.pedClones[eSelfID], 
									GET_PED_BONE_INDEX(clientBD.pedClones[eSelfID], BONETAG_PH_R_HAND), (<<0,0,0>>), (<<0,0,0>>))	//, RACKET_OFFSET, RACKET_ROTATION)
							ENDIF
							IF NOT IS_PED_INJURED(clientBD.pedClones[eOtherID])
								ATTACH_ENTITY_TO_ENTITY(thisProps.tennisPlayers[eOtherID].oTennisRacket, clientBD.pedClones[eOtherID], 
									GET_PED_BONE_INDEX(clientBD.pedClones[eOtherID], BONETAG_PH_R_HAND), (<<0,0,0>>), (<<0,0,0>>))	//, RACKET_OFFSET, RACKET_ROTATION)
							ENDIF
						ELIF INT_TO_ENUM( CAMERA_STATE_ENUM, eventDetails.iDummy0 ) <> TENNIS_CAMERA_SIDE_CHANGE
							IF IS_TENNIS_CLIENT_FLAG_SET(clientBD, TMC_MAKE_PLAYERS_INVISIBLE)
								CLEAR_TENNIS_CLIENT_FLAG(clientBD, TMC_MAKE_PLAYERS_INVISIBLE)
								SET_PLAYER_VISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( info.iPlayers[eSelfID] ) ) )
								SET_PLAYER_VISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( info.iPlayers[eOtherID] ) ) )
								TENNIS_HIDE_CLIENT_CLONE(clientBD, eSelfID)
								TENNIS_HIDE_CLIENT_CLONE(clientBD, eOtherID)
								ATTACH_TENNIS_RACKETS_TO_PLAYERS(thisProps, eSelfID, eOtherID)
								SWITCH GET_TENNIS_CAMERA_STATE(thisUIData.tennisCameras)
									CASE TENNIS_CAMERA_FAR				SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamFar, TRUE)				BREAK
									CASE TENNIS_CAMERA_FAR_OTHER_SIDE	SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamFarOtherSide, TRUE)	BREAK
									CASE TENNIS_CAMERA_DYNAMIC			SET_CAM_ACTIVE(thisUIData.tennisCameras.tennisCamDyna, TRUE)			BREAK
								ENDSWITCH
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Calls Disable_On_Foot_First_Person_View_This_Update and prints to screen that it's suppressing the camera
PROC TENNIS_SUPPRESS_FIRST_PERSON_CAMERA()
	DRAW_DEBUG_TEXT_2D( "Disabling First Person Cam", (<< 0.5, 0.8, 0.0 >>) )
	Disable_On_Foot_First_Person_View_This_Update()
ENDPROC

