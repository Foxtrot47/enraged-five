USING "minigames_helpers.sch"
USING "minigame_UIInputs.sch"
USING "friendActivity_public.sch"

USING "tennis_ball.sch"
USING "tennis_court.sch"
USING "tennis_helpers_lib.sch"
USING "tennis_player.sch"
USING "tennis.sch"
USING "tennis_mp.sch"

PROC COPY_ONE_TENNIS_BALL_TO_ANOTHER(TENNIS_BALL &sourceBall, TENNIS_BALL &targetBall)
	CPRINTLN(DEBUG_TENNIS, "COPY_ONE_TENNIS_BALL_TO_ANOTHER called, ball_", NATIVE_TO_INT(sourceBall.oBall), " -> ball_", NATIVE_TO_INT(targetBall.oBall))
	targetBall.iBounceCount					= sourceBall.iBounceCount
	targetBall.iBallBounceFrameStamp		= sourceBall.iBallBounceFrameStamp
	targetBall.iBallIntoPlayerFrameStamp	= sourceBall.iBallIntoPlayerFrameStamp
	targetBall.fFlightTimer					= sourceBall.fFlightTimer
	targetBall.fSpinTimer					= sourceBall.fSpinTimer
	targetBall.fNoBounceTimeOut				= sourceBall.fNoBounceTimeOut
	targetBall.vBallPosHidden				= sourceBall.vBallPosHidden
	targetBall.vBallVel						= sourceBall.vBallVel
	targetBall.vCollPos						= sourceBall.vCollPos
	targetBall.vBallLaunchPos				= sourceBall.vBallLaunchPos
	targetBall.eBallSpin					= sourceBall.eBallSpin
	targetBall.iBallFlags					= sourceBall.iBallFlags
	// Time Slice Variables
	targetBall.fTargetTime					= sourceBall.fTargetTime
	targetBall.fSimTime						= sourceBall.fSimTime
	targetBall.vNaught						= sourceBall.vNaught
	targetBall.vNext						= sourceBall.vNext
	targetBall.fPredTimeRemainder			= sourceBall.fPredTimeRemainder
ENDPROC

PROC UPDATE_TENNIS_BALL_TIME_SLICE_VARIABLES(TENNIS_BALL &thisBall, BOOL bUseCached)
	VECTOR vBallPos
	IF DOES_ENTITY_EXIST(thisBall.oBall)
		vBallPos = PICK_VECTOR(bUseCached, GET_TENNIS_BALL_POS(thisBall), GET_ENTITY_COORDS(thisBall.oBall))
	ENDIF
	thisBall.vNaught = vBallPos
	thisBall.vNext = vBallPos
	thisBall.fTargetTime = 0.0
	thisBall.fSimTime = 0.0
ENDPROC

PROC INITIALIZE_BALL(TENNIS_BALL &thisBall)
	//REQUEST_MODEL(mTheBallModel)
	thisBall.iBounceCount = 0
ENDPROC

/// PURPOSE:
///    spawns a new ball
///    Sets old ball as no longer needed unless it's invisible, in which case it's deleted.
/// PARAMS:
///    thisProps - 
///    bDelete - Will delete the old ball if TRUE
PROC DEBUG_SPAWN_BALL(TENNIS_GAME_PROPERTIES &thisProps, BOOL bDelete=TRUE)	// Changed bDelete to TRUE to intentionally always delete the ball
	IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
		IF bDelete OR NOT IS_ENTITY_VISIBLE(thisProps.tennisBall.oBall)
			DELETE_OBJECT(thisProps.tennisBall.oBall)
			CDEBUG2LN(DEBUG_TENNIS, "DEBUG_SPAWN_BALL ball deleted.")
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(thisProps.tennisBall.oBall)
		ENDIF
	ENDIF
	
	VECTOR vSpawnHere
	
	//This could be the center of the court for all I care
	vSpawnHere = thisProps.tennisCourt.vCenterCourt
	
	thisProps.tennisBall.oBall = CREATE_OBJECT(PROP_TENNIS_BALL,vSpawnHere, FALSE, FALSE)
	SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(thisProps.tennisBall.oBall, TRUE)
	SET_ENTITY_RECORDS_COLLISIONS(thisProps.tennisBall.oBall,TRUE)
	SET_TENNIS_BALL_VISIBILITY(thisProps.tennisBall.oBall, FALSE)
	FREEZE_ENTITY_POSITION(thisProps.tennisBall.oBall,FALSE)
	CPRINTLN(DEBUG_TENNIS, "DEBUG_SPAWN_BALL called")
	DEBUG_PRINTCALLSTACK()
	SET_TENNIS_BALL_POS(thisProps.tennisBall, thisProps.tennisCourt.vCenterCourt)
	IF DOES_TENNIS_BALL_TRAIL_EXIST(thisProps.tennisBall)
		STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
	ENDIF
ENDPROC

/// PURPOSE:
///    Calculates acceleration by taking gravity and air friction into account
/// PARAMS:
///    vBallVel - 
/// RETURNS:
///    acceleration to be applied (added) to the velocity
FUNC VECTOR CALCULATE_BALL_ACCEL(VECTOR vBallVel, FLOAT fFrameRate)
	VECTOR gravity = << 0.0, 0.0, -9.8>>
	FLOAT fVelMag = VMAG(vBallVel)
	VECTOR accel = -vBallVel * (fVelMag * AIR_FRICTION)
	accel += gravity
	accel *= fFrameRate
	RETURN accel	
ENDFUNC

/// PURPOSE:
///    Calculates the velocity after it bounces with the ground.
/// PARAMS:
///    vBallVel - 
/// RETURNS:
///    
FUNC VECTOR CALCULATE_BALL_BOUNCE(VECTOR vBallVel)
	vBallVel *= GROUND_BOUNCE_COEFF
	vBallVel.z = -vBallVel.z
	IF vBallVel.z < BOUNCE_MIN_Z
		vBallVel.z = 0.0
	ENDIF
	RETURN vBallVel
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisCourt - 
///    vPos1 - First position, before velocity added
///    vPos2 - Second position, after velocity added
///    vUpCourt - 
///    vNewPos - Passed back changed when function returns TNB_RICOCHET
///    vReflectNorm - Passed back changed when function returns TNB_RICOCHET
/// RETURNS:
///    
FUNC TENNIS_NET_BOUNCE_ENUM WILL_TENNIS_SIMULATION_HIT_THE_NET(TENNIS_COURT &thisCourt, VECTOR &vPos1, VECTOR &vPos2, VECTOR &vUpCourt, VECTOR &vNewPos, VECTOR &vReflectNorm, FLOAT fNetFudge)
	FLOAT tValueBall
	GET_LINE_PLANE_INTERSECTION(vPos1, vPos2, thisCourt.vCenterCourt, vUpCourt, tValueBall)
	IF tValueBall >= 0 AND tValueBall <= 1
		VECTOR vIntersectPlane = vPos1 + ((vPos2 - vPos1) * tValueBall)
		VECTOR vNetAtIntersection
		FLOAT tValueNet1 = GET_RATIO_OF_CLOSEST_POINT_ON_LINE(vIntersectPlane, thisCourt.vNetTop[0], thisCourt.vNetTop[1], FALSE)
		FLOAT tValueNet2 = GET_RATIO_OF_CLOSEST_POINT_ON_LINE(vIntersectPlane, thisCourt.vNetTop[1], thisCourt.vNetTop[2], FALSE)
		
		IF tValueNet1 > 0 AND tValueNet1 < 1
			// Use tValue1
			vNetAtIntersection = thisCourt.vNetTop[0] + ((thisCourt.vNetTop[1] - thisCourt.vNetTop[0]) * tValueNet1)
		ELIF tValueNet2 > 0 AND tValueNet2 < 1 AND tValueNet1 > 0
			// Use tValue2
			vNetAtIntersection = thisCourt.vNetTop[1] + ((thisCourt.vNetTop[2] - thisCourt.vNetTop[1]) * tValueNet2)
		ENDIF
		
		FLOAT fHeightAboveNet = vIntersectPlane.z - vNetAtIntersection.z
		
//		CPRINTLN(DEBUG_TENNIS, "vNetAtIntersection ", vNetAtIntersection, ", vIntersectPlane ", vIntersectPlane, ", tValueBall ", tValueBall, ", tValNet1 ", tValueNet1, ", tValNet2 ", tValueNet2)
		IF fHeightAboveNet <= CLAMP(TENNIS_BALL_RADIUS - fNetFudge, -1, 0)	// "-1" should be negative infinity.
//			CPRINTLN(DEBUG_TENNIS, "WILL_TENNIS_SIMULATION_HIT_THE_NET Returning TNB_NORMAL_BOUNCE")
			RETURN TNB_NORMAL_BOUNCE
		ELIF fHeightAboveNet > 0 AND fHeightAboveNet <= (TENNIS_BALL_RADIUS - fNetFudge)
			CPRINTLN(DEBUG_TENNIS, "WILL_TENNIS_SIMULATION_HIT_THE_NET Returning TNB_RICOCHET and a new vReflectNorm")
			VECTOR vNetNorm = vUpCourt / VMAG(vUpCourt)
			IF DOT_PRODUCT(vPos2 - vPos1, vNetNorm) > 0
				vNetNorm = vNetNorm * -1.0
			ENDIF
			FLOAT fPushback = SQRT((TENNIS_BALL_RADIUS * TENNIS_BALL_RADIUS) - (fHeightAboveNet * fHeightAboveNet))
			vNewPos = vIntersectPlane + vNetNorm * fPushBack
			vReflectNorm = vNewpos - vNetAtIntersection
			vReflectNorm = vReflectNorm / VMAG(vReflectNorm)
			RETURN TNB_RICOCHET
		ENDIF
	ENDIF
	RETURN TNB_NO_BOUNCE
ENDFUNC

/// PURPOSE:
///    Updates the ball simulator, pass in the ball position and velocity by reference, then set the entity's position and velocity to those values.
///    Everything is passed by reference, be very careful that if this isn't the main sim update that all values passed to the function are cached first.
/// PARAMS:
///    vBallPos - 
///    vBallVel - 
///    fCourtZ - 
///    fBallAirTime - 
/// RETURNS:
///    TRUE if the ball bounced during the update
FUNC TENNIS_NET_BOUNCE_ENUM UPDATE_BALL_SIMULATOR(TENNIS_COURT &thisCourt, VECTOR &vBallPos, VECTOR &vBallVel, VECTOR &vUpCourt, FLOAT &fBallAirTime, TENNIS_BALL_SPIN &eSpin, FLOAT &fSpinTimer, INT &iBounces, BOOL bPlaySound=FALSE, FLOAT fLockFrames=-1.0, BOOL bIncreaseSimSpeed=FALSE, FLOAT fNetFudge=0.0, BOOL bIgnoreRicochet=FALSE)
	VECTOR vForce = << 0,0,1 >>
	FLOAT fFrameRate = PICK_FLOAT((fLockFrames = -1), GET_FRAME_TIME(), fLockFrames)	//PICK_FLOAT(bLockFrames, SIXTY_FPS, GET_FRAME_TIME())	//THIRTY_FPS
	TENNIS_NET_BOUNCE_ENUM eBounced = TNB_NO_BOUNCE
	FLOAT fForceAdjust = fFrameRate / THIRTY_FPS
	vForce *= fForceAdjust	// this scales down the added force when we do piecewise updates like lockstepping the frames.
	
	IF (bIncreaseSimSpeed) #IF IS_DEBUG_BUILD OR INCREASE_BALL_SIM_SPEED #ENDIF
		fFrameRate += fFrameRate * BALL_SIM_SPEED_INCREASE
		vForce += vForce * BALL_SIM_SPEED_INCREASE
	ENDIF
	
	// Topspin adds a downward force to the velocity
	IF eSpin = TBS_TOPSPIN AND fBallAirTime > TOPSPIN_APPLY_TIME AND fSpinTimer < TOPSPIN_APPLY_DURATION
		vForce *= -TOPSPIN_MOD
		fSpinTimer += fFrameRate
		vBallVel += vForce
	ELIF eSpin = TBS_SLICE
		// Slice adds an upward lift before the bounce
		IF fBallAirTime > SLICE_APPLY_TIME AND iBounces < 1
			vForce = (vBallVel * fForceAdjust) * SLICE_MOD
			vForce.z = 0
			vBallVel += vForce
		// Slice stunts the bounce after it occurs for a shallow second bounce
		ELIF iBounces >= 1
			vForce = (vBallVel * fForceAdjust) * SLICE_SUBTRACT
			vBallVel += vForce
			eSpin = TBS_NO_SPIN
		ENDIF
	ENDIF
	
	fBallAirTime += fFrameRate
	
	VECTOR vTest = vUpCourt / VMAG(vUpCourt)
	DRAW_DEBUG_LINE(thisCourt.vCenterCourt + (<< 0, 0, 1 >>), thisCourt.vCenterCourt + (<< 0, 0, 1 >>) + vTest)
	DRAW_DEBUG_LINE(thisCourt.vCenterCourt + (<< 0, 0, 1 >>), thisCourt.vCenterCourt + (<< 0, 0, 2 >>), 0, 255, 0)
	
	VECTOR vNext = vBallPos + vBallVel * fFrameRate
	VECTOR vReflectNorm, vNewPos
	//Reflect the ball off the net if it hits
	eBounced = WILL_TENNIS_SIMULATION_HIT_THE_NET(thisCourt, vBallPos, vNext, vUpCourt, vNewPos, vReflectNorm, fNetFudge)
	IF eBounced = TNB_NORMAL_BOUNCE
		IF bPlaySound
			PLAY_SOUND_FROM_COORD(-1, "TENNIS_NET_BALL_MEDIUM_MASTER", vBallPos, default, default, default, TRUE)
			CPRINTLN(DEBUG_TENNIS, "TENNIS_NET_BALL_MEDIUM_MASTER played")
			
			VECTOR vBallPos2 = vBallPos + vBallVel
			APPLY_IMPULSE_TO_CLOTH(vBallPos, vBallPos2, 2.0)			
		ENDIF
		
		#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_SPHERE("NetBounce", vNewPos, 0.06, <<1.0, 0.5, 0.5>>)
		#ENDIF
		VECTOR vNorm = vUpCourt / VMAG(vUpCourt)
		FLOAT fDot = DOT_PRODUCT(vNorm, vBallVel)
		VECTOR vParallel = vNorm * fDot	// Find component of the velocity parallel to the normal
		vBallVel -= vParallel * NET_BOUNCE_HOR_COEFF
		vBallVel.z *= NET_BOUNCE_VERT_COEFF				
		
	ELIF eBounced = TNB_RICOCHET AND NOT bIgnoreRicochet
		#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_SPHERE("NetRicochet", vNewPos, 0.06, <<1.0, 0.5, 0.5>>)
		#ENDIF
		IF bPlaySound
			PLAY_SOUND_FROM_COORD(-1, "TENNIS_NET_BALL_SKIM_MASTER", vBallPos, default, default, default, TRUE)
			CPRINTLN(DEBUG_TENNIS, "TENNIS_NET_BALL_SKIM_MASTER played")
			
			VECTOR vBallPos2 = vBallPos + vBallVel
			APPLY_IMPULSE_TO_CLOTH(vBallPos, vBallPos2, 2.0)
		ENDIF
		vBallPos = vNewPos
		vBallVel = vBallVel - (vReflectNorm * DOT_PRODUCT(vReflectNorm, vBallVel) * NET_BOUNCE_HOR_COEFF)
	ENDIF
	
	VECTOR vPrevBallPos = vBallPos
	vBallPos += vBallVel * fFrameRate
	
	IF vBallPos.z < thisCourt.vCenterCourt.z
		FLOAT tValue
		GET_LINE_PLANE_INTERSECTION(vPrevBallPos, vBallPos, thisCourt.vCenterCourt, (<<0,0,1>>), tValue)
		VECTOR vAdd = vBallVel * fFrameRate * tValue
		vBallPos = vPrevBallPos + vAdd
		vBallPos.z += TENNIS_BALL_BOUNCE_RADIUS
		vBallVel = CALCULATE_BALL_BOUNCE(vBallVel)
		iBounces++
		eBounced = TNB_GROUND_BOUNCE
		IF bPlaySound AND iBounces = 1
			PLAY_SOUND_FROM_COORD(-1, "TENNIS_CLS_BALL_HARD_MASTER", vBallPos, default, default, default, TRUE)
			CPRINTLN(DEBUG_TENNIS, "TENNIS_CLS_BALL_HARD_MASTER played, iBounces=", iBounces)
		ELIF bPlaySound AND iBounces > 1 AND iBounces < 4
			PLAY_SOUND_FROM_COORD(-1, "TENNIS_CLS_BALL_MASTER", vBallPos, default, default, default, TRUE)
			CPRINTLN(DEBUG_TENNIS, "TENNIS_CLS_BALL_MASTER played, iBounces=", iBounces)
		ENDIF
	ENDIF
	
	vBallVel += CALCULATE_BALL_ACCEL(vBallVel, fFrameRate)
	
	DRAW_DEBUG_TEXT("ScriptedMotion", vBallPos)
	
	RETURN eBounced
ENDFUNC

/// PURPOSE:
///    Time slices the ball update and LERPs the final vBallPos at 60FPS
/// PARAMS:
///    thisCourt - 
///    vBallPos - 
///    vBallVel - 
///    vUpCourt - 
///    fBallAirTime - 
///    eSpin - 
///    fSpinTimer - 
///    iBounces - 
///    fTargetTime - 
///    fSimTime - 
///    bPlaySound - 
/// RETURNS:
///   TRUE if the ball bounces
FUNC TENNIS_NET_BOUNCE_ENUM UPDATE_BALL_SIMULATOR_TIME_SLICED(TENNIS_COURT &thisCourt, TENNIS_BALL &thisBall, TENNIS_PLAYER &tennisPlayers[], VECTOR &vUpCourt, INT &iBounces, FLOAT fTargetAddTime, BOOL bPlaySound=FALSE, BOOL bIgnoreBounce=FALSE, BOOL bIgnoreRicochet=FALSE, BOOL bIncreaseSimSpeed=FALSE)
	TENNIS_NET_BOUNCE_ENUM eSimReturn
	thisBall.fTargetTime += fTargetAddTime
	
	// Sanity check if we have the flag set but didn't handle it, clear it now.
	IF IS_TENNIS_BALL_FLAG_SET(thisBall, TBF_RICOCHETED)
		CLEAR_TENNIS_BALL_FLAG(thisBall, TBF_RICOCHETED)
	ENDIF
	
	WHILE thisBall.fSimTime < thisBall.fTargetTime
		thisBall.vNaught = thisBall.vNext
		eSimReturn = UPDATE_BALL_SIMULATOR(thisCourt, thisBall.vNext, thisBall.vBallVel, vUpCourt, thisBall.fFlightTimer, thisBall.eBallSpin, thisBall.fSpinTimer, iBounces, bPlaySound, SIXTY_FPS, bIncreaseSimSpeed, default, bIgnoreRicochet)
		thisBall.fSimTime += SIXTY_FPS
		// If we hit a bounce then set the flag and track it.
		IF eSimReturn = TNB_GROUND_BOUNCE AND NOT bIgnoreBounce
			SET_TENNIS_BALL_FLAG(thisBall, TBF_BOUNCED_IN_TIME_SLICE)
		ELIF eSimReturn = TNB_RICOCHET AND NOT bIgnoreRicochet
			SET_TENNIS_BALL_FLAG(thisBall, TBF_RICOCHETED)
		ENDIF
		#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_SPHERE("updateSlice", thisBall.vNext, 0.1, (<<200,200,200>>))
		#ENDIF
//		CDEBUG3LN(DEBUG_TENNIS, "vNaught=", thisBall.vNaught, ", vNext=", thisBall.vNext, ", fSimTime=", thisBall.fSimTime, ", fTargetTime=", thisBall.fTargetTime)
	ENDWHILE
	
	FLOAT fDiff = thisBall.fTargetTime - (thisBall.fSimTime - SIXTY_FPS)
	FLOAT fAlpha = fDiff/SIXTY_FPS
	VECTOR vFinalBallPos = LERP_VECTOR(thisBall.vNaught, thisBall.vNext, fAlpha)
//	CDEBUG3LN(DEBUG_TENNIS, "fTargetTime=", thisBall.fTargetTime, ", fSimTime=", thisBall.fSimTime, ", fDiff=", fDiff, ", fAlpha=", fAlpha, ", vFinal=", vFinalBallPos)
	
	IF GET_FRAME_COUNT() > GET_TENNIS_BALL_INTO_PLAYER_FRAME_STAMP(thisBall)
		// Dot the ball pos with the player pos. That'll give us a cheap Dist2. (No, it won't. playerPos-ballPos dotted with itself is Dist2)
		BOOL bRequireP2Check = TRUE
		FLOAT fDist2
		IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(tennisPlayers[0]))
			fDist2 = VDIST2(vFinalBallPos, GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(tennisPlayers[0])))
		ENDIF
		IF (fDist2 < 0.25)
		AND (GET_TENNIS_PLAYER_SWING_STATE(tennisPlayers[0]) = TSS_NO_SWING)
		AND NOT IS_TENNIS_PLAYER_FLAG_SET(tennisPlayers[0], TPB_TENNIS_SWING_EVENT_FLAG)
			CPRINTLN(DEBUG_TENNIS, "===== HIT PLAYER 0 =====")
			SET_TENNIS_BALL_FLAG(thisBall, TBF_HIT_PLAYER)
			bRequireP2Check = FALSE
		ENDIF
		IF bRequireP2Check
			IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(tennisPlayers[1]))
				fDist2 = VDIST2(vFinalBallPos, GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(tennisPlayers[1])))
			ENDIF
			IF (fDist2 < 0.25)
			AND (GET_TENNIS_PLAYER_SWING_STATE(tennisPlayers[1]) = TSS_NO_SWING)
			AND NOT IS_TENNIS_PLAYER_FLAG_SET(tennisPlayers[1], TPB_TENNIS_SWING_EVENT_FLAG)
				CPRINTLN(DEBUG_TENNIS, "===== HIT PLAYER 1 =====")
				SET_TENNIS_BALL_FLAG(thisBall, TBF_HIT_PLAYER)
			ENDIF
		ENDIF
	ENDIF
	
	SET_TENNIS_BALL_POS(thisBall, vFinalBallPos)
	IF DOES_ENTITY_EXIST(thisBall.oBall)
		SET_ENTITY_COORDS(thisBall.oBall, vFinalBallPos, default, default, default, FALSE)
		SET_ENTITY_VELOCITY(thisBall.oBall, thisBall.vBallVel)
	ENDIF
	#IF TENNIS_DEBUG_RECORDING
	TEXT_LABEL_31 texBall = "vBallPos"
	texBall += NATIVE_TO_INT(thisBall.oBall)
	DEBUG_RECORD_SPHERE(texBall, vFinalBallPos, 0.1, (<<255,255,0>>))
	#ENDIF
	
	RETURN eSimReturn
ENDFUNC

/// PURPOSE:
///    Intended to be called right after the ball is hit
///    Simulates the ball for MAX_BOUNCE_SIM_ITERATIONS frames or until it bounces
///    90 might be too much...
/// PARAMS:
///    vBallPos - 
///    vBallVel - 
///    fCourtZ - 
///    fBallAirTime - from accessor
///    eSpin - from accessor
///    fSpinTimer - from accessor
FUNC VECTOR SIMULATE_TENNIS_BALL_FIRST_BOUNCE(TENNIS_COURT &thisCourt, VECTOR vBallPos, VECTOR vBallVel, VECTOR vUpCourt, FLOAT fBallAirTime, TENNIS_BALL_SPIN eSpin, FLOAT fSpinTimer)
	CPRINTLN(DEBUG_TENNIS, "SIMULATE_TENNIS_BALL_FIRST_BOUNCE called")
	INT iBounces = 0
	INT p = 0
	WHILE p < MAX_BOUNCE_SIM_ITERATIONS
		UPDATE_BALL_SIMULATOR(thisCourt, vBallPos, vBallVel, vUpCourt, fBallAirTime, eSpin, fSpinTimer, iBounces, default, THIRTY_FPS, default, NET_PREDICTION_FUDGE_VALUE)
		IF iBounces > 0
			RETURN vBallPos
		ENDIF
		p++
	ENDWHILE
	
	RETURN vBallPos
ENDFUNC

/// PURPOSE:
///    Finds the best spot to swing at the ball based on distance and bounces
/// PARAMS:
///    vPlayerPos - 
///    thisCourt - 
///    vBallPos - 
///    vBallVel - 
///    vUpCourt - 
///    fBallAirTime - 
///    eSpin - 
///    fSpinTimer - 
///    fTime - FLOAT return value, frame ahead of the predicted ball position
/// RETURNS:
///    Returns the closest simulation point to vPlayerPos or the apex of the ball after the first bounce.
FUNC VECTOR GET_CLOSEST_POINT_FOR_TENNIS_SWING(VECTOR vPlayerPos, TENNIS_COURT &thisCourt, VECTOR vBallPos, VECTOR vBallVel, VECTOR vUpCourt, FLOAT fBallAirTime, TENNIS_BALL_SPIN eSpin, FLOAT fSpinTimer, FLOAT &fTime)
	CPRINTLN(DEBUG_TENNIS, "GET_CLOSEST_POINT_FOR_TENNIS_SWING called")
	VECTOR vOldPos
	INT iBounces = 0
	INT p = 0
	WHILE p < MAX_BOUNCE_SIM_ITERATIONS	AND iBounces < 2
		vOldPos = vBallPos
		UPDATE_BALL_SIMULATOR(thisCourt, vBallPos, vBallVel, vUpCourt, fBallAirTime, eSpin, fSpinTimer, iBounces)
		// Return the closest point to the player in the simulation
		IF VDIST(vPlayerPos, vOldPos) < VDIST(vPlayerPos, vBallPos)
			CPRINTLN(DEBUG_TENNIS, "Returning closest position of the ball ", vBallPos)
			fTime = p * GET_FRAME_TIME()	//THIRTY_FPS
			RETURN vBallPos
		ENDIF
		// Return the apex of the second bounce
		IF (iBounces > 0) 
		AND (vBallVel.z < 0)
			CPRINTLN(DEBUG_TENNIS, "Returning ideal position of ball after first bounce ", vOldPos)
			fTime = p * GET_FRAME_TIME()	//THIRTY_FPS
			RETURN vOldPos
		ENDIF
		p++
	ENDWHILE
	
	fTime = p * GET_FRAME_TIME()	//THIRTY_FPS
	RETURN vBallPos
ENDFUNC

/// PURPOSE:
///    launches the ball
///    Stores the velocity in the ball structs vBallVel because a velocity check in the same frame returns <<0,0,0>>
/// PARAMS:
///    thisProps - contains the ball, court, and direction vectors required
///    eWhoIsServing - 
///    thisPlayer - which player is hitting the ball
///    launchDetails - The details we're launching the ball with
PROC LAUNCH_BALL(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER &thisPlayer, LAUNCH_DETAILS &launchDetails)
	
	CPRINTLN(DEBUG_TENNIS, "Game Timer: ", GET_GAME_TIMER(), " Ball launched with the following details:")
	CPRINTLN(DEBUG_TENNIS, "iForce: ", launchDetails.iForce)
	CPRINTLN(DEBUG_TENNIS, "iAngle: ", launchDetails.iAngle)
	CPRINTLN(DEBUG_TENNIS, "LeftRight: ", thisPlayer.fLeftRight)
	
	FLOAT fLeftRightAngle
	fLeftRightAngle = thisPlayer.fLeftRight
	
	VECTOR vForce
	vForce = GetForceVector(TO_FLOAT(launchDetails.iForce), TO_FLOAT(launchDetails.iAngle), fLeftRightAngle, thisPlayer.vMyForward, thisPlayer.vMyRight)
	
	IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
		IF thisProps.eTennisActivity <> TA_AI_VS_AI AND NOT DOES_TENNIS_BALL_TRAIL_EXIST(thisProps.tennisBall)
			STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
			START_TENNIS_BALL_TRAIL(thisProps.tennisBall)
		ENDIF
		// Set the ball's velocity to the new vForce
		thisProps.tennisBall.vBallVel = vForce
		#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "BallPosBeforeLaunch", GET_TENNIS_BALL_POS(thisProps.tennisBall), TRUE)
		#ENDIF
		SET_TENNIS_BALL_POS(thisProps.tennisBall, GET_ENTITY_COORDS(thisProps.tennisBall.oBall))
		#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "BallPosPostLaunch", GET_TENNIS_BALL_POS(thisProps.tennisBall), TRUE)
		#ENDIF
		// hold onto where it was launched from, we use this later for some calculations
		thisProps.tennisBall.vBallLaunchPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
		CPRINTLN(DEBUG_TENNIS, "ball position: ", thisProps.tennisBall.vBallLaunchPos, ", vForce ", vForce)
		SET_TENNIS_BALL_INTO_PLAYER_FRAME_STAMP(thisProps.tennisBall, GET_FRAME_COUNT() + COLLIDE_WITH_PLAYER_THRESH)
		RESET_TENNIS_BALL_TIMERS(thisProps.tennisBall)
		// Update time slice variables
		UPDATE_TENNIS_BALL_TIME_SLICE_VARIABLES(thisProps.tennisBall, TRUE)
		SET_TENNIS_BALL_VISIBILITY(thisProps.tennisBall.oBall, TRUE)
	ENDIF
	CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_HIT_PLAYER)
ENDPROC

/// PURPOSE:
///    launches the ball
///    Stores the velocity in the ball structs vBallVel because a velocity check in the same frame returns <<0,0,0>>
/// PARAMS:
///    thisProps - contains the ball, court, and direction vectors required
///    eWhoIsServing - 
///    thisPlayer - which player is hitting the ball
///    launchDetails - The details we're launching the ball with
PROC LAUNCH_BALL_WITH_FORCE_VECTOR(TENNIS_GAME_PROPERTIES &thisProps, VECTOR vForce)
	CPRINTLN(DEBUG_TENNIS, "     LAUNCH_BALL_WITH_FORCE_VECTOR :: Ball launched with the following details:")
	
	IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
		IF thisProps.eTennisActivity <> TA_AI_VS_AI AND NOT DOES_TENNIS_BALL_TRAIL_EXIST(thisProps.tennisBall)
			STOP_TENNIS_BALL_TRAIL(thisProps.tennisBall)
			START_TENNIS_BALL_TRAIL(thisProps.tennisBall)
		ENDIF
		// Set the ball's velocity to the new vForce
		thisProps.tennisBall.vBallVel = vForce
		VECTOR vBallPos = GET_ENTITY_COORDS(thisProps.tennisBall.oBall)
		IF vBallPos.z < thisProps.tennisCourt.vCenterCourt.z
			CPRINTLN(DEBUG_TENNIS, "Adjusting ball position, below ground: ", (vBallPos.z - thisProps.tennisCourt.vCenterCourt.z))
			vBallPos.z = thisProps.tennisCourt.vCenterCourt.z
		ENDIF
		SET_TENNIS_BALL_POS(thisProps.tennisBall, vBallPos)
		// hold onto where it was launched from, we use this later for some calculations
		thisProps.tennisBall.vBallLaunchPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
		CPRINTLN(DEBUG_TENNIS, "ball position: ", thisProps.tennisBall.vBallLaunchPos, ", vForce ", vForce)
		SET_TENNIS_BALL_INTO_PLAYER_FRAME_STAMP(thisProps.tennisBall, GET_FRAME_COUNT() + COLLIDE_WITH_PLAYER_THRESH)
		RESET_TENNIS_BALL_TIMERS(thisProps.tennisBall)
		// Update time slice variables
		UPDATE_TENNIS_BALL_TIME_SLICE_VARIABLES(thisProps.tennisBall, TRUE)
		SET_TENNIS_BALL_BOUNCE_FRAME_STAMP(thisProps.tennisBall, GET_TENNIS_FRAME_COUNTER(thisProps))
		SET_TENNIS_BALL_VISIBILITY(thisProps.tennisBall.oBall, TRUE)
	ENDIF
	CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_HIT_PLAYER)
	CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_BOUNCED_IN_TIME_SLICE)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisProps - 
/// RETURNS:
///    HAS_ENTITY_COLLIDED_WITH_ANYTHING(thisProps.tennisBall.oBall) and checks for what was hit
FUNC BOOL HAS_BALL_BOUNCED(TENNIS_BALL &thisBall, INT iFrameCounter, VECTOR &vCenterCourt)
	IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(thisBall.oBall) OR IS_TENNIS_BALL_FLAG_SET(thisBall, TBF_BOUNCED_IN_TIME_SLICE)
		
		MATERIAL_NAMES mat = GET_LAST_MATERIAL_HIT_BY_ENTITY(thisBall.oBall)
		
		CPRINTLN(DEBUG_TENNIS, "mat hash: ", mat)
		//CREATE_PICKUP(PICKUP_MONEY_WALLET, thisProps.tennisBall.vPrevPos)
		
		IF mat = TENNIS_NET
			CPRINTLN(DEBUG_TENNIS, "Ball hit the net and is in the air")
		ENDIF
		
		IF (mat = TENNIS_TURF
			OR mat = GOLF_PATH1
			OR mat = GOLF_PATH2
			OR mat = GOLF_WATER_HAZARD
			OR mat = TENNIS_FENCE
			OR mat = TENNIS_TURF_2
			OR mat = TENNIS_FENCE_2
			OR mat = TENNIS_FENCE_3
			OR mat = TENNIS_FENCE_4
			OR mat = TENNIS_FENCE_5
			OR mat = TENNIS_FENCE_6
			OR mat = TENNIS_BLEACHERS_SEATS
			OR mat = TENNIS_BLEACHERS_FLOOR
			OR mat = TENNIS_INNER_WALLS
			OR mat = TENNIS_TURF_3
			OR mat = TENNIS_TURF_4
			OR mat = TENNIS_TURF_5
			OR mat = GOLF_GREEN
			OR mat = TENNIS_BLEACHERS_SEATS_2)
		AND iFrameCounter - thisBall.iBallBounceFrameStamp > FRAME_THRESHOLD_FOR_BOUNCE
		AND mat <> INVALID_MATERIAL
			SET_TENNIS_BALL_BOUNCE_FRAME_STAMP(thisBall, iFrameCounter)
			thisBall.vCollPos = GET_TENNIS_BALL_POS(thisBall)
			CPRINTLN(DEBUG_TENNIS, "HAS_BALL_BOUNCED: TRUE, thisBall.iBallBounceFrameStamp=", thisBall.iBallBounceFrameStamp)
			CPRINTLN(DEBUG_TENNIS, "Collision Position: ", thisBall.vCollPos)
			#IF TENNIS_DEBUG_RECORDING
			DEBUG_RECORD_SPHERE("CollPoint", thisBall.vCollPos, 0.1, (<<255, 255, 0>>))
			#ENDIF
			CLEAR_TENNIS_BALL_FLAG(thisBall, TBF_INCREASE_SIM_SPEED)
			CLEAR_TENNIS_BALL_FLAG(thisBall, TBF_BOUNCED_IN_TIME_SLICE)
			//CREATE_PICKUP(PICKUP_MONEY_WALLET, thisProps.tennisBall.vPrevPos)
			RETURN TRUE
		ELSE
			CPRINTLN(DEBUG_TENNIS, PICK_STRING(iFrameCounter - thisBall.iBallBounceFrameStamp > FRAME_THRESHOLD_FOR_BOUNCE, "Bounce threshold passed", "Bounce threshold not passed yet"))
		ENDIF
		
		IF IS_TENNIS_BALL_FLAG_SET(thisBall, TBF_BOUNCED_IN_TIME_SLICE)
		AND iFrameCounter - thisBall.iBallBounceFrameStamp > FRAME_THRESHOLD_FOR_BOUNCE
			CLEAR_TENNIS_BALL_FLAG(thisBall, TBF_BOUNCED_IN_TIME_SLICE)
			SET_TENNIS_BALL_BOUNCE_FRAME_STAMP(thisBall, iFrameCounter)
			thisBall.vCollPos = GET_TENNIS_BALL_POS(thisBall)
			CPRINTLN(DEBUG_TENNIS, "HAS_BALL_BOUNCED: TBF_BOUNCED_IN_TIME_SLICE, thisBall.iBallBounceFrameStamp=", thisBall.iBallBounceFrameStamp)
			CPRINTLN(DEBUG_TENNIS, "Collision Position: ", thisBall.vCollPos)
			#IF TENNIS_DEBUG_RECORDING
			DEBUG_RECORD_SPHERE("CollPoint", thisBall.vCollPos, 0.1, (<<255, 255, 0>>))
			#ENDIF
			CLEAR_TENNIS_BALL_FLAG(thisBall, TBF_INCREASE_SIM_SPEED)
			RETURN TRUE
			// TODO: Timeslice makes the ball go thru a player
		ENDIF
		
		IF (VDIST2(vCenterCourt, GET_TENNIS_BALL_POS(thisBall)) > COURT_RANGE * COURT_RANGE)
			CERRORLN(DEBUG_TENNIS, "Ball has hit nothing for ", NO_BOUNCE_TIMEOUT," seconds, contact Rob Pearsall!")
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	IF GET_TENNIS_BALL_BOUNCE_TIMEOUT(thisBall) > NO_BOUNCE_TIMEOUT
		RESET_TENNIS_BALL_BOUNCE_TIMEOUT(thisBall)
		SET_TENNIS_BALL_FLAG(thisBall, TBF_NO_BOUNCE_TIMEOUT_OCCURED)
		CERRORLN(DEBUG_TENNIS, "Ball has hit nothing for ", NO_BOUNCE_TIMEOUT," seconds, contact Rob Pearsall!")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
