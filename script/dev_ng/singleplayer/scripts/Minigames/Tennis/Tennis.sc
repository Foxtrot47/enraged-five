CONST_INT	IS_TENNIS_MULTIPLAYER	0


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.



USING "tennis_core_lib.sch"
USING "mission_titles_private.sch"

/// ************************************************************* SCRIPT *************************************************************
SCRIPT
	CPRINTLN(DEBUG_TENNIS, "********************* Tennis First Post **********************")
	CPRINTLN(DEBUG_TENNIS, "GET_ALLOCATED_STACK_SIZE(", GET_ALLOCATED_STACK_SIZE(), ")")
	
	TENNIS_GAME_PROPERTIES thisProps
	TENNIS_GAME_STATUS	thisStatus
	TENNIS_GAME_SP_DATA	thisSPData
	TENNIS_GAME_UI_DATA thisUIData
	TENNIS_PLAYER_ID eSelfID = TENNIS_PLAYER_AWAY	//use HOME instead of AWAY to start the game from the other side of the court.
	TENNIS_PLAYER_ID eOtherID = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eSelfID))
	
	// Despite being an IF the compiler jumps here when the function is true.
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		CDEBUG1LN(DEBUG_TENNIS, "HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_PAUSE_MENU_TERMINATED), quitting")
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		SET_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), thisSPData.sVariations)
		TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, thisSPData, AR_playerQuit)
	ENDIF

	#IF IS_DEBUG_BUILD
		BOOL bDebugDraw = FALSE
		TENNIS_PRINT_CURRENT_GLOBAL_VALUES()
		SET_PROFILING_OF_THIS_SCRIPT(TRUE)
//		SET_TENNIS_GLOBAL_FLAG(TGF_AMANDA_IS_OPP)
	#ENDIF
	ODDJOB_ENTER_CUTSCENE()
	SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
	ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
	
	START_AUDIO_SCENE("TENNIS_SCENE")
	
	SET_MINIGAME_IN_PROGRESS(TRUE)
	
	INIT_PC_SCRIPTED_CONTROLS("Tennis")
	
	SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	ENDIF
	
	#IF IS_DEBUG_BUILD
//		TRACE_NATIVE_COMMAND("TASK_GO_STRAIGHT_TO_COORD")
//		TRACE_NATIVE_COMMAND("SET_PLAYER_CONTROL")
//		TRACE_NATIVE_COMMAND("DRAW_SCALEFORM_MOVIE_FULLSCREEN")
//		TRACE_NATIVE_COMMAND("SET_OBJECT_AS_NO_LONGER_NEEDED")
	#ENDIF
	
	TRIGGER_MUSIC_EVENT("MGTN_START")
	
	DISABLE_CELLPHONE(TRUE)

	TEXT_LABEL_23 emptyLabel = "NONE"
	MINIGAME_DISPLAY_MISSION_TITLE(ENUM_TO_INT(MINIGAME_TENNIS))
	
	INITIALIZE_TENNIS_GAME(thisProps, thisStatus, TA_PLAYER_VS_AI, eSelfID, eOtherID) //TA_AI_VS_AI //TA_PLAYER_VS_AI
	
	// Run the intro sync scene.
	RUN_TENNIS_INTRO_SYNC_SCENE(thisUIData.tennisCameras, thisProps, thisUIData, thisSPData, eOtherID, eSelfID)
	
	// Request all assets that used to be requested in init.
    REQUEST_MODEL(PROP_TENNIS_BALL)
    REQUEST_MODEL(thisProps.mTheOpponentModel)
    REQUEST_MODEL(PROP_TENNIS_RACK_01B)
    REQUEST_ANIM_DICT("mini@tennis")
	REQUEST_ANIM_DICT("mini@tennis@female")
    REQUEST_ANIM_DICT("mini@triathlon")
    REQUEST_STREAMED_TEXTURE_DICT("MPHUD")
	REQUEST_MODEL(Prop_VB_34_tencrt_lighting)
	REQUEST_ANIM_DICT("weapons@tennis@male")
	REQUEST_MODEL(PROP_ENERGY_DRINK)
	
	IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
		REQUEST_ANIM_DICT("facials@p_m_zero@base")
		REQUEST_ANIM_DICT("facials@p_m_zero@variations@happy")
		REQUEST_ANIM_DICT("mini@tennisreactions@michael")
		REQUEST_ANIM_DICT("mini@tennisoutro@michael")
	ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
		REQUEST_ANIM_DICT("facials@p_m_two@base")
		REQUEST_ANIM_DICT("facials@p_m_two@variations@happy")
		REQUEST_ANIM_DICT("mini@tennisreactions@trevor")
		REQUEST_ANIM_DICT("mini@tennistrevor")
		REQUEST_ANIM_DICT("mini@tennisoutro@trevor")
	ENDIF
	
	IF IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eSelfID])
		REQUEST_ANIM_DICT("mini@tennisexit@male")
	ELSE
		REQUEST_ANIM_DICT("mini@tennisexit@female")
	ENDIF
	
	IF IS_TENNIS_PED_MALE(thisProps.tennisPlayers[eOtherID])
		REQUEST_ANIM_DICT("facials@gen_male@base")
		REQUEST_ANIM_DICT("facials@gen_male@variations@happy")
	ELSE
		REQUEST_ANIM_DICT("facials@gen_female@base")
		REQUEST_ANIM_DICT("facials@gen_female@variations@happy")
	ENDIF
	IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
		SET_PED_CONFIG_FLAG(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), PCF_ForceNoTimesliceIntelligenceUpdate, TRUE)
	ENDIF
	
	INITIALIZE_TENNIS_GAME_UI_DATA(thisUIData, thisProps, eSelfID, eOtherID)
	INITIALIZE_TENNIS_GAME_SP_DATA(thisSPData, thisProps)
	INITIALIZE_AI_HOME_POSITIONS(thisProps.tennisPlayers[eSelfID].playerAI, thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisPlayers[eSelfID].vMyForward, thisProps.tennisPlayers[eSelfID].vMyRight, thisProps.tennisCourt.vServiceCorners[0])
	INITIALIZE_AI_HOME_POSITIONS(thisProps.tennisPlayers[eOtherID].playerAI, thisProps.tennisCourt.vCourtCorners[2], thisProps.tennisPlayers[eOtherID].vMyForward, thisProps.tennisPlayers[eOtherID].vMyRight, thisProps.tennisCourt.vServiceCorners[2])
	
	ADJUST_TENNIS_PLAYER_ON_STATS(thisUIData.playerChars[eSelfID], thisProps.tennisPlayers[eSelfID])
	
	//Stops primed grenades from exploding during the intro cutscene
	IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE,SPC_REMOVE_PROJECTILES)
		SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE,SPC_REMOVE_EXPLOSIONS)
	ENDIF
			
	#IF IS_DEBUG_BUILD
		LOAD_WIDGETS(thisProps, thisStatus, thisUIData, eSelfID, eOtherID, thisSPData)
	#ENDIF
		
	//Initialize(court)
	IF thisProps.eTennisActivity <> TA_AI_VS_AI
		//("Checking Distance to Court")
		IF VDIST2(GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()), FALSE), thisProps.tennisCourt.vLauncherPoint) > 10000 //100m away
			DEBUG_MESSAGE("Far away from start spot, fading out and going to TGSE_PRE_INIT")
			SET_TENNIS_GAME_STATE(thisStatus, TGSE_PRE_INIT)
			DO_SCREEN_FADE_OUT(500)
		ELIF thisProps.eTennisActivity = TA_FRIEND_ACTIVITY OR IS_TENNIS_GLOBAL_FLAG_SET(TGF_AMANDA_IS_OPP)
			SET_TENNIS_GAME_STATE(thisStatus, TGSE_FRIEND_INIT_WAIT)
		ELSE
			RESET_CUTSCENE_STATE(thisUIData.tennisCameras)
			SET_TENNIS_GAME_STATE(thisStatus, TGSE_INIT)
		ENDIF
	ELSE
		DEBUG_MESSAGE("Starting game as normal, moving to TGSE_INIT")
		SET_TENNIS_GAME_STATE(thisStatus, TGSE_INIT)
	ENDIF
	
	SET_DISABLE_AMBIENT_MELEE_MOVE(GET_PLAYER_INDEX(), TRUE)
	
	SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
	NET_NL()NET_PRINT("tennis: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(),FALSE)")				

	SET_TENNIS_DIALOGUE_STRUCT(thisUIData.myDialogueStruct, thisUIData.playerChars[eSelfID], thisUIData.playerChars[eOtherID], thisProps.tennisPlayers[eSelfID], thisProps.tennisPlayers[eOtherID])
	
	// Get current clock time and store it so we can set it every frame
	INT iHours, iMinutes, iSeconds
	iHours = GET_CLOCK_HOURS()
	iMinutes = GET_CLOCK_MINUTES()
	iSeconds = GET_CLOCK_SECONDS()
	SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 10.0)
	
	WHILE (TRUE)
	
		TENNIS_SUPPRESS_FIRST_PERSON_CAMERA()
		
		UPDATE_TENNIS_TIMER_A(thisProps)
	
		SET_WIND(0)
		SET_CLOCK_TIME(iHours, iMinutes, iSeconds)
		IF NOT IS_SELECTOR_DISABLED()
			DISABLE_SELECTOR()
		ENDIF
		
		IF NOT IS_RADAR_HIDDEN()
			DISPLAY_RADAR(FALSE)
		ENDIF
		
		// Ensure that all subtitles are slightly raised to not impact instructional buttons.
		SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0.0, -0.0375)
		
		IF GET_TENNIS_GAME_STATE(thisStatus) > TGSE_INTRO AND GET_TENNIS_CUTSCENE_STATE(thisUIData.tennisCameras) >= CUT_INIT
			TENNIS_SET_GAMEPLAY_CAM_WORLD_HEADING(GET_HEADING_FROM_VECTOR_2D(thisProps.tennisPlayers[eSelfID].vMyForward.x, thisProps.tennisPlayers[eSelfID].vMyForward.y))
		ENDIF
				
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_ELEMENTS(thisProps, eSelfID, eOtherID, thisSPData)
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				CDEBUG1LN(DEBUG_TENNIS, "Passing thru KEY_S Debug!")
				SCRIPT_PASSED(thisProps, thisSPData, thisUIData, AR_playerWon, eSelfID, eOtherID)
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				CDEBUG1LN(DEBUG_TENNIS, "Quitting thru F_KEY Debug!")
				SET_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), thisSPData.sVariations)
//				SET_PED_VARIATIONS(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), thisSPData.sOppVariations)
				CLEAR_TENNIS_CHARACTER_SEEN_TUTORIAL()
				TENNIS_SCRIPT_CLEANUP(thisProps, thisUIData, eSelfID, eOtherID, thisSPData, AR_playerWon)
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
				IF GET_TENNIS_GAME_STATE(thisStatus) <> TGSE_TEST_STATE
					SET_TENNIS_GAME_STATE(thisStatus, TGSE_TEST_STATE)
					ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, TRUE)
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					CPRINTLN(DEBUG_TENNIS, "Entering Test State")
				ELSE
					thisStatus.eGameState = thisStatus.ePrevGameState
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
					CPRINTLN(DEBUG_TENNIS, "Leaving Test State, ePrevGameState will not be TGSE_TEST_STATE!!!")
				ENDIF
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
				bDebugDraw = !bDebugDraw
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bDebugDraw)
			ENDIF
			IF thisProps.tennisPlayers[eSelfID].vPos.z < thisProps.tennisCourt.vCenterCourt.z - 1
				CPRINTLN(DEBUG_TENNIS, "eSelfID vPos: ", thisProps.tennisPlayers[eSelfID].vPos)
			ENDIF
			IF thisProps.tennisPlayers[eOtherID].vPos.z < thisProps.tennisCourt.vCenterCourt.z - 1
				CPRINTLN(DEBUG_TENNIS, "eOtherID vPos: ", thisProps.tennisPlayers[eOtherID].vPos)
			ENDIF
		#ENDIF
		
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LB)
			CPRINTLN(DEBUG_TENNIS, "TRYING TO QUIT")
		ENDIF
		
		IF GET_TENNIS_GAME_STATE(thisStatus) > TGSE_INTRO AND GET_TENNIS_GAME_STATE(thisStatus) < TGSE_OUTRO AND thisProps.eTennisActivity <> TA_AI_VS_AI
			PROCESS_OPTION_STATE(thisProps, thisStatus, thisSPData, thisUIData, eSelfID, eOtherID)
		ENDIF
		
		IF GET_TENNIS_GAME_STATE(thisStatus) > TGSE_INTRO
//			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			IF GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt) <> TENNIS_COURT_CountryClub
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			ENDIF
		ENDIF
		
		IF (GET_TENNIS_GAME_STATE(thisStatus) > TGSE_OPTION_FINISHED AND GET_TENNIS_GAME_STATE(thisStatus) < TGSE_QUIT_CONFIRM AND thisProps.eTennisActivity <> TA_AI_VS_AI)
		OR GET_TENNIS_GAME_STATE(thisStatus) = TGSE_TEST_STATE
			UPDATE_MISSION_NAME_DISPLAYING(emptyLabel, default, default, default, TRUE)
			PROCESS_CAMERA_STATE(thisProps, thisUIData, eSelfID, thisProps.tennisPlayers[eSelfID].eCourtSide, GET_TENNIS_STATUS_HIT_LAST(thisStatus))
		ELIF GET_TENNIS_GAME_STATE(thisStatus) = TGSE_OPTION_MENU
			UPDATE_TENNIS_WANDERING_MENU_CAMERA(thisProps, thisUIData)
		ENDIF
		
		PROCESS_GAME_STATE(thisProps, thisStatus, thisUIData.iUtilFlags, eSelfID, eOtherID, thisUIData.fMessageTimer)
		
		HANDLE_TENNIS_DIALOGUE(thisProps, thisStatus, thisProps.tennisPlayers[GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData)], thisUIData, eSelfID, eOtherID, GET_TENNIS_INTERSTITIAL_PLAYER(thisUIData))
		
		PROCESS_TENNIS_UI_DATA(thisUIData, thisProps, thisStatus, thisSPData, eSelfID, eOtherID)
		
		IF GET_TENNIS_GAME_STATE(thisStatus) > TGSE_INTRO AND GET_TENNIS_GAME_STATE(thisStatus) < TGSE_QUIT_CONFIRM AND thisProps.eTennisActivity <> TA_AI_VS_AI
			PROCESS_TENNIS_UI(thisProps, thisStatus, thisUIData, thisProps.tennisPlayers[eSelfID], eSelfID, eOtherID)
		ENDIF
		
		INCREASE_TENNIS_FRAME_COUNTER(thisProps.iFrameCounter)
		
		IF GET_TENNIS_GAME_STATE(thisStatus) > TGSE_INTRO AND HANDLE_TENNIS_FAIL_SATES(thisProps, eSelfID, thisUIData.iUtilFlags)
			CDEBUG1LN(DEBUG_TENNIS, "HANDLE_TENNIS_FAIL_SATES :: SCRIPT_FAILED, NO_ACTIVITY_RESULT")
			SCRIPT_FAILED(thisProps, thisSPData, thisUIData, NO_ACTIVITY_RESULT, eSelfID, eOtherID)
		ENDIF
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT


