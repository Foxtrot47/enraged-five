USING "minigames_helpers.sch"
USING "minigame_UIInputs.sch"

USING "tennis_ai.sch"
USING "tennis_player.sch"
USING "tennis.sch"
USING "tennis_ball.sch"
USING "tennis_ball_lib.sch"
USING "tennis_court.sch"
USING "tennis_helpers_lib.sch"

PROC INITIALIZE_TENNIS_AI(TENNIS_AI_PLAYER &thisAI)
	SET_TENNIS_AI_STATE(thisAI, ASE_AI_IDLE)
	SET_AI_DIFFICULTY(thisAI, AD_EASY)
ENDPROC

PROC INITIALIZE_AI_HOME_POSITIONS(TENNIS_AI_PLAYER &thisAI, VECTOR vBottomLeft, VECTOR vForward, VECTOR vRight, VECTOR vServiceLeft)
	thisAI.vHomePos[0] = vBottomLeft + (vForward * MULT_5) + (vRight * MULT_1)		// Back Center
	thisAI.vHomePos[1] = vBottomLeft + (vForward * MULT_2) + (vRight * MULT_3)		// Front Right
	thisAI.vHomePos[2] = vBottomLeft + (vForward * MULT_2) + (vRight * MULT_4)		// Front Left
	thisAI.vHomePos[3] = vServiceLeft + (vForward * MULT_6) + (vRight * MULT_1)		// Middle Center
ENDPROC

/// PURPOSE:
///    Used when setting the launch_details of an AI player
/// PARAMS:
///    thisPlayer - an AI controlled player
///    thisProps - 
///    serveGrade - passed by reference and modified
FUNC TENNIS_BALL_SPIN CALCULATE_TENNIS_AI_BALL_SPIN(AI_DIFFICULTY eDiff)
	SWITCH eDiff	
		CASE AD_EASY
			RETURN TBS_NO_SPIN
		BREAK
		
		CASE AD_NORMAL
			//Determine Chance of Spin
			IF GET_RANDOM_FLOAT_IN_RANGE() < NORMAL_SPIN_RATE
				IF GET_RANDOM_FLOAT_IN_RANGE() < 0.50
					RETURN TBS_TOPSPIN
				ELSE
					RETURN TBS_SLICE
				ENDIF
			ELSE
				RETURN TBS_NO_SPIN
			ENDIF
		BREAK
		
		CASE AD_HARD
			//Determine Chance of Spin
			IF GET_RANDOM_FLOAT_IN_RANGE() < HARD_SPIN_RATE
				IF GET_RANDOM_FLOAT_IN_RANGE() < HARD_TOPSPIN_RATE
					RETURN TBS_TOPSPIN
				ELSE
					 RETURN TBS_SLICE
				ENDIF
			ELSE
				RETURN TBS_NO_SPIN
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TBS_NO_SPIN
ENDFUNC

/// PURPOSE:
///    Should only be used for freemode intro
/// PARAMS:
///    thisAI - 
///    fNewReactionTime - 
PROC SET_AI_REACTION_TIME(TENNIS_AI_PLAYER &thisAI, FLOAT fNewReactionTime)
	thisAI.fReactionTime = fNewReactionTime
	CPRINTLN(DEBUG_TENNIS, "SET_AI_REACTION_TIME :: ", thisAI.texName, " to ", fNewReactionTime)
ENDPROC

/// PURPOSE:
///    Sets a random reaction time based on difficulty of the AI
/// PARAMS:
///    thisAI - 
PROC SET_RANDOM_AI_REACTION_TIME(TENNIS_AI_PLAYER &thisAI)
	IF thisAI.eDifficulty = AD_EASY
		thisAI.fReactionTime = GET_RANDOM_FLOAT_IN_RANGE(0, MAX_EASY_AI_REACTION_TIME)
	ELIF thisAI.eDifficulty = AD_NORMAL
		thisAI.fReactionTime = GET_RANDOM_FLOAT_IN_RANGE(0, MAX_NORMAL_AI_REACTION_TIME)
	ELIF thisAI.eDifficulty = AD_HARD
		thisAI.fReactionTime = GET_RANDOM_FLOAT_IN_RANGE(0, MAX_HARD_AI_REACTION_TIME)
	ENDIF
	CPRINTLN(DEBUG_TENNIS, "SET_RANDOM_AI_REACTION_TIME :: ", thisAI.texName, " set to ", thisAI.fReactionTime)
ENDPROC

PROC UPDATE_TENNIS_AI_REACTION_TIME(TENNIS_AI_PLAYER &thisAI)
	thisAI.fReactionTime -= GET_FRAME_TIME()
ENDPROC

FUNC FLOAT GET_TENNIS_AI_REACTION_TIME(TENNIS_AI_PLAYER &thisAI)
	RETURN thisAI.fReactionTime
ENDFUNC

/// PURPOSE:
///    Returns the value of the serve bucket for the given AI.
///    A positive value means the ball has been consistently hit to the outside of the ped.
///    A negative value means the ball has been hit to the center of the court or the inside of the ped.
/// PARAMS:
///    thisAI - 
/// RETURNS:
///    The value to multiply the peds vMyRight by
FUNC FLOAT GET_TENNIS_AI_SERVE_BUCKET(TENNIS_AI_PLAYER &thisAI)
	RETURN thisAI.fServeBucket
ENDFUNC

PROC RESET_TENNIS_AI_BUCKETS(TENNIS_AI_PLAYER &thisAI)
	thisAI.fServeBucket = 0
	thisAI.fRightBucket = 0
	thisAI.fBackBucket = 0
ENDPROC

/// PURPOSE:
///    Decreases multiplier by SERVE_BUCKET_RATE to a limit of SERVE_BUCKET_MIN
/// PARAMS:
///    thisAI - 
PROC DECREASE_SERVE_BUCKET(TENNIS_AI_PLAYER &thisAI)
	thisAI.fServeBucket -= SERVE_BUCKET_RATE
	thisAI.fServeBucket = CLAMP(thisAI.fServeBucket, SERVE_BUCKET_MIN, SERVE_BUCKET_MAX)
ENDPROC

/// PURPOSE:
///    Increases multiplier by SERVE_BUCKET_RATE to a limit of SERVE_BUCKET_MAX
/// PARAMS:
///    thisAI - 
PROC INCREASE_SERVE_BUCKET(TENNIS_AI_PLAYER &thisAI)
	thisAI.fServeBucket += SERVE_BUCKET_RATE
	thisAI.fServeBucket = CLAMP(thisAI.fServeBucket, SERVE_BUCKET_MIN, SERVE_BUCKET_MAX)
ENDPROC

/// PURPOSE:
///    Adjusts the Serve Bucket based on whether the shot was left or right of the ped
/// PARAMS:
///    thisPlayer - 
PROC ADJUST_AI_SERVE_POSITION_WITH_BUCKETS(TENNIS_PLAYER &thisPlayer, SERVING_SIDE_ENUM eServingSide)
	CPRINTLN(DEBUG_TENNIS, "ADJUST_AI_SERVE_POSITION_WITH_BUCKETS")
	FLOAT dp = DOT_PRODUCT(thisPlayer.vMyRight, GET_AI_PREDICTION_POINT(thisPlayer.playerAI) - thisPlayer.vPos)
	// "inside" and "outside" refer to relations with the center line, toward the center line is inside.
	IF dp > 0 AND eServingSide = SERVING_FROM_RIGHT		// the serve was shot outside of us, increase the bucket
		INCREASE_SERVE_BUCKET(thisPlayer.playerAI)
		CPRINTLN(DEBUG_TENNIS, "dp > 0 AND eServingSide = SERVING_FROM_RIGHT :: ", GET_TENNIS_AI_SERVE_BUCKET(thisPlayer.playerAI))
	ELIF dp <= 0 AND eServingSide = SERVING_FROM_RIGHT	// the serve was shot inside of us, decrease the bucket
		DECREASE_SERVE_BUCKET(thisPlayer.playerAI)
		CPRINTLN(DEBUG_TENNIS, "dp <= 0 AND eServingSide = SERVING_FROM_RIGHT :: ", GET_TENNIS_AI_SERVE_BUCKET(thisPlayer.playerAI))
	ELIF dp > 0 AND eServingSide = SERVING_FROM_LEFT	// the serve was shot inside of us, decrease the bucket
		DECREASE_SERVE_BUCKET(thisPlayer.playerAI)
		CPRINTLN(DEBUG_TENNIS, "dp > 0 AND eServingSide = SERVING_FROM_LEFT :: ", GET_TENNIS_AI_SERVE_BUCKET(thisPlayer.playerAI))
	ELIF dp <= 0 AND eServingSide = SERVING_FROM_LEFT	// the serve was shot outside of us, increase the bucket
		INCREASE_SERVE_BUCKET(thisPlayer.playerAI)
		CPRINTLN(DEBUG_TENNIS, "dp <= 0 AND eServingSide = SERVING_FROM_LEFT :: ", GET_TENNIS_AI_SERVE_BUCKET(thisPlayer.playerAI))
	ENDIF
//	SCRIPT_ASSERT("0")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisAI - 
///    bIncrease - TRUE to move the home position further toward back court
PROC ADJUST_BUCKET_BACK(TENNIS_AI_PLAYER &thisAI, BOOL bIncrease)
	FLOAT fChangeRate
	
	SWITCH thisAI.eDifficulty
		CASE AD_EASY	fChangeRate = BUCKET_CHANGE_RATE_BACK_HARD	BREAK
		CASE AD_NORMAL	fChangeRate = BUCKET_CHANGE_RATE_BACK_HARD	BREAK
		CASE AD_HARD	fChangeRate = BUCKET_CHANGE_RATE_BACK_HARD	BREAK
	ENDSWITCH
	
	IF NOT bIncrease
		fChangeRate *= -1
	ENDIF
	
	thisAI.fBackBucket += fChangeRate
	thisAI.fBackBucket = CLAMP(thisAI.fBackBucket, BUCKET_RIGHT_MIN, BUCKET_RIGHT_MAX)
ENDPROC

/// PURPOSE:
///    Increases how far right the homepostions are pushed from neutral
/// PARAMS:
///    thisAI - 
PROC INCREASE_BUCKET_RIGHT(TENNIS_AI_PLAYER &thisAI)
	FLOAT fChangeRate
	
	SWITCH thisAI.eDifficulty
		CASE AD_EASY	fChangeRate = BUCKET_CHANGE_RATE_EASY	BREAK
		CASE AD_NORMAL	fChangeRate = BUCKET_CHANGE_RATE_NORM	BREAK
		CASE AD_HARD	fChangeRate = BUCKET_CHANGE_RATE_HARD	BREAK
	ENDSWITCH
	
	thisAI.fRightBucket += fChangeRate
	thisAI.fRightBucket = CLAMP(thisAI.fRightBucket, BUCKET_RIGHT_MIN, BUCKET_RIGHT_MAX)
ENDPROC

/// PURPOSE:
///    decreases how far right the homes are pushed from neutral, home positions can be pushed left.
/// PARAMS:
///    thisAI - 
PROC DECREASE_BUCKET_RIGHT(TENNIS_AI_PLAYER &thisAI)
	FLOAT fChangeRate
	
	SWITCH thisAI.eDifficulty
		CASE AD_EASY	fChangeRate = BUCKET_CHANGE_RATE_EASY	BREAK
		CASE AD_NORMAL	fChangeRate = BUCKET_CHANGE_RATE_NORM	BREAK
		CASE AD_HARD	fChangeRate = BUCKET_CHANGE_RATE_HARD	BREAK
	ENDSWITCH
	
	thisAI.fRightBucket -= fChangeRate
	thisAI.fRightBucket = CLAMP(thisAI.fRightBucket, BUCKET_RIGHT_MIN, BUCKET_RIGHT_MAX)
ENDPROC

/// PURPOSE:
///    Checks the backhand bit and then increases or decreases the appropriate bucket
///    Then it adjusts the home positions off the buckets
///    Currently only adjusts left and right, don't know if we need forward/back yet.
/// PARAMS:
///    thisPlayer - 
///    vBottomLeft - 
PROC ADJUST_AI_HOME_POSITIONS_WITH_BUCKETS(TENNIS_PLAYER &thisPlayer, VECTOR vBottomLeft, VECTOR vServiceBoxLeft)
	CPRINTLN(DEBUG_TENNIS, "ADJUST_AI_HOME_POSITIONS_WITH_BUCKETS: ", thisPlayer.playerAI.texName, " called, should only be called once")
	//Checking the dot product of the player's right and the vector between their position and their prediction point
	IF DOT_PRODUCT(thisPlayer.vMyRight, GET_AI_PREDICTION_POINT(thisPlayer.playerAI) - thisPlayer.vPos) > 0
		INCREASE_BUCKET_RIGHT(thisPlayer.playerAI)
	ELSE	
		DECREASE_BUCKET_RIGHT(thisPlayer.playerAI)
	ENDIF
	
	IF DOT_PRODUCT(thisPlayer.vMyForward, GET_AI_PREDICTION_POINT(thisPlayer.playerAI) - thisPlayer.vPos) > 0
		ADJUST_BUCKET_BACK(thisPlayer.playerAI, FALSE)
	ELSE
		ADJUST_BUCKET_BACK(thisPlayer.playerAI, TRUE)
	ENDIF
	
	thisPlayer.playerAI.vHomePos[0] = vBottomLeft + (thisPlayer.vMyForward * MULT_5) + (thisPlayer.vMyForward * thisPlayer.playerAI.fBackBucket) + (thisPlayer.vMyRight * MULT_1) + (thisPlayer.vMyRight * thisPlayer.playerAI.fRightBucket)
	thisPlayer.playerAI.vHomePos[1] = vBottomLeft + (thisPlayer.vMyForward * MULT_2) + (thisPlayer.vMyForward * thisPlayer.playerAI.fBackBucket) + (thisPlayer.vMyRight * MULT_3) + (thisPlayer.vMyRight * thisPlayer.playerAI.fRightBucket)
	thisPlayer.playerAI.vHomePos[2] = vBottomLeft + (thisPlayer.vMyForward * MULT_2) + (thisPlayer.vMyForward * thisPlayer.playerAI.fBackBucket) + (thisPlayer.vMyRight * MULT_4) + (thisPlayer.vMyRight * thisPlayer.playerAI.fRightBucket)
	thisPlayer.playerAI.vHomePos[3] = vServiceBoxLeft + (thisPlayer.vMyForward * MULT_6) + (thisPlayer.vMyForward * thisPlayer.playerAI.fBackBucket) + (thisPlayer.vMyRight * MULT_1) + (thisPlayer.vMyRight * thisPlayer.playerAI.fRightBucket)	
ENDPROC

/// PURPOSE:
///    If the AI passed in is doing anything other than charging a shot we set their state to charging a shot.
/// PARAMS:
///    thisAI - 
PROC TENNIS_AI_CHOOSE_SHOT_NOW(TENNIS_AI_PLAYER &thisAI)
	SWITCH GET_TENNIS_AI_STATE(thisAI)
		CASE ASE_AI_CHARGING_TOPSPIN
		CASE ASE_AI_CHARGING_LOB
//				CPRINTLN(DEBUG_TENNIS, "TENNIS_AI_CHOOSE_SHOT_NOW: Do nothing, shot already chosen")
		BREAK
		DEFAULT
			CPRINTLN(DEBUG_TENNIS, "TENNIS_AI_CHOOSE_SHOT_NOW: ", thisAI.texName)
			FLOAT fTemp, fNormalShotChance
			fTemp = GET_RANDOM_FLOAT_IN_RANGE()
			fNormalShotChance = PICK_FLOAT(thisAI.eDifficulty = AD_HARD, AI_HARD_SHOTTYPE_RATE, AI_AVERAGE_SHOTTYPE_RATE)
			IF fTemp < fNormalShotChance
				SET_TENNIS_AI_STATE(thisAI, ASE_AI_CHARGING_TOPSPIN)
			ELSE
				SET_TENNIS_AI_STATE(thisAI, ASE_AI_CHARGING_LOB)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Determines what "buttons" the ai is using on their shot
/// PARAMS:
///    thisPlayer - 
///    thisProps - 
///    bIsAICross - Normal shot type, passed by reference
///    bIsAITriangle - Lob shot type, passed by reference
PROC GET_AI_BUTTON_FORCES(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, BOOL &bBaseShot, BOOL &bLob)

	bBaseShot = FALSE
	bLob = FALSE
	
	FLOAT fTemp
	
	SWITCH GET_TENNIS_AI_STATE(thisPlayer.playerAI)
		CASE ASE_AI_CHOOSING_SHOT			
			IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
			AND (GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall) > 0)
				fTemp = GET_RANDOM_FLOAT_IN_RANGE()
				FLOAT fNormalShotChance
				fNormalShotChance = PICK_FLOAT(thisPlayer.playerAI.eDifficulty = AD_HARD, AI_HARD_SHOTTYPE_RATE, AI_AVERAGE_SHOTTYPE_RATE)
				IF fTemp < fNormalShotChance
					IF NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_AI_HAS_SWUNG)
						bBaseShot = TRUE
						SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_AI_HAS_SWUNG)
					ENDIF		
					SET_TENNIS_AI_STATE(thisPlayer.playerAI, ASE_AI_CHARGING_TOPSPIN)
				ELSE
					IF NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_AI_HAS_SWUNG)
						bLob = TRUE
						SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_AI_HAS_SWUNG)
					ENDIF
					SET_TENNIS_AI_STATE(thisPlayer.playerAI, ASE_AI_CHARGING_LOB)
				ENDIF
			ENDIF
		BREAK
		CASE ASE_AI_CHARGING_TOPSPIN
			bBaseShot = TRUE
		BREAK
		CASE ASE_AI_CHARGING_LOB
			bLob = TRUE
		BREAK		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Normal Difficulty chooses between 3 hom positions
///    Easy does not use Home positions
///    Hard choose between two centrally located positions
/// PARAMS:
///    thisAI - 
PROC SET_RANDOM_TENNIS_AI_HOME_POS(TENNIS_PLAYER &thisPlayer, TENNIS_COURT &thisCourt, VECTOR vPropsRight)
	INT p = GET_RANDOM_INT_IN_RANGE(0,MAX_HOME_POS-1)
	
	IF thisPlayer.playerAI.eDifficulty = AD_HARD
		p = PICK_INT(GET_RANDOM_FLOAT_IN_RANGE() < 0.5, 0, 3)
		CPRINTLN(DEBUG_TENNIS, "SET_RANDOM_TENNIS_AI_HOME_POS: ", thisPlayer.playerAI.texName, " vHomePos[", p, "]")
		SET_AI_PREDICTION_POINT(thisPlayer.playerAI, thisPlayer.playerAI.vHomePos[p])
	ELIF thisPlayer.playerAI.eDifficulty = AD_EASY
		VECTOR vMid1 = thisCourt.vCourtCorners[0] + (vPropsRight * 0.5)
		VECTOR vMid2 = thisCourt.vCourtCorners[3] + (vPropsRight * 0.5)
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_POLY_ANGLED_AREA(vMid1, vMid2, GET_TENNIS_COURT_WIDTH(thisCourt))
		#ENDIF
		IF NOT IS_POINT_INSIDE_TENNIS_AREA(thisPlayer.vPos, vMid1, vMid2, GET_TENNIS_COURT_WIDTH(thisCourt))
			vMid1 = thisCourt.vCenterCourt - (thisPlayer.vMyForward * 6.0)
			SET_AI_PREDICTION_POINT(thisPlayer.playerAI, vMid1)
			CPRINTLN(DEBUG_TENNIS, "SET_RANDOM_TENNIS_AI_HOME_POS: ", thisPlayer.playerAI.texName, "Easy going back to ", vMid1)
		ELSE
			SET_AI_PREDICTION_POINT(thisPlayer.playerAI, thisPlayer.vPos)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_TENNIS, "SET_RANDOM_TENNIS_AI_HOME_POS: ", thisPlayer.playerAI.texName, " vHomePos[", p, "]")
		SET_AI_PREDICTION_POINT(thisPlayer.playerAI, thisPlayer.playerAI.vHomePos[p])
	ENDIF
ENDPROC

FUNC BOOL VALIDATE_TENNIS_AI_GO_TO_IDLE_POS(TENNIS_PLAYER &thisPlayer, VECTOR &vPointToGoTo, VECTOR &vDimensions)
	IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_TENNIS_SWING_EVENT_FLAG)
		RETURN FALSE
	ENDIF
	IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_TASKED_NEW_RALLY)
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_AI_GO_TO_IDLE_POS :: ", thisPlayer.playerAI.texName, " IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_TASKED_NEW_RALLY)")
		RETURN FALSE
	ENDIF
	IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_JUST_TASKED)
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_AI_GO_TO_IDLE_POS :: ", thisPlayer.playerAI.texName, " IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_JUST_TASKED)")
		RETURN FALSE
	ENDIF
	IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_TASKED_ANIM)
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_AI_GO_TO_IDLE_POS :: ", thisPlayer.playerAI.texName, " IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_TASKED_ANIM)")
		RETURN FALSE
	ENDIF
	IF IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer)) 
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_AI_GO_TO_IDLE_POS :: ", thisPlayer.playerAI.texName, " IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer))")
		RETURN FALSE
	ENDIF
	IF IS_ENTITY_AT_COORD(thisPlayer.pIndex, vPointToGoTo, vDimensions)
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_AI_GO_TO_IDLE_POS :: ", thisPlayer.playerAI.texName, " IS_ENTITY_AT_COORD(thisPlayer.pIndex, vPointToGoTo, vDimensions)")
		RETURN FALSE
	ENDIF
	IF NOT SHOULD_RETASK_MOTION(thisPlayer.playerAI)
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_AI_GO_TO_IDLE_POS :: ", thisPlayer.playerAI.texName, " NOT SHOULD_RETASK_MOTION(thisPlayer.playerAI)")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC UPDATE_HARD_AI_BASED_ON_PLAYER_ACTION(TENNIS_PLAYER &thisPlayer, TENNIS_PLAYER &thatPlayer)
	IF GET_GAME_TIMER() > GET_TENNIS_AI_ACTION_TAKEN_STAMP(thisPlayer.playerAI)	// AND NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_DONT_CHASE)
		SHOT_TYPE_ENUM eShot = GET_TENNIS_PLAYER_STROKE(thatPlayer)
		IF eShot = SHOT_BACKSPIN OR eShot = SHOT_STRONG
			SET_AI_PREDICTION_POINT(thisPlayer.playerAI, thisPlayer.playerAI.vHomePos[0])
			SET_TENNIS_AI_STATE(thisPlayer.playerAI, ASE_AI_MOVING_TO_IDLE)
			CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
			CPRINTLN(DEBUG_TENNIS, thisPlayer.playerAI.texName, " preparing for a Backspin or Strong swing.")
		ENDIF
		SET_TENNIS_AI_ACTION_TAKEN_STAMP(thisPlayer.playerAI, GET_GAME_TIMER() + AI_ACTION_TAKEN_DURATION)
	ENDIF
ENDPROC

/// PURPOSE:
///    Move the AI around the court
/// PARAMS:
///    thisPlayer - 
///    thisProps - 
///    x - 
///    y - 
///    bIsBackhand - 
PROC GET_AI_STICK_FORCE(TENNIS_PLAYER &thisPlayer, INT &x,INT &y, TENNIS_PLAYER &thatPlayer, TENNIS_BALL &thisBall) // 0...128
	VECTOR vCoord
	VECTOR vPointToGoTo = GET_AI_PREDICTION_POINT(thisPlayer.playerAI)
	
	IF GET_TENNIS_PLAYER_DIRECTION_HELD_TIMER(thatPlayer) > AI_HELD_DIRECTION_REACT AND NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.PlayerAI, TAF_PLYR_RUN_LR_HELD)
		SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_PLYR_RUN_LR_HELD)
	ELIF IS_TENNIS_AI_FLAG_SET(thisPlayer.PlayerAI, TAF_PLYR_RUN_LR_HELD)
		CLEAR_TENNIS_AI_FLAG(thisPlayer.PlayerAI, TAF_PLYR_RUN_LR_HELD)
	ENDIF
	
	SWITCH GET_TENNIS_AI_STATE(thisPlayer.playerAI)
		CASE ASE_AI_IDLE
			x = 0
			y = 0
			IF GET_AI_DIFFICULTY(thisPlayer.playerAI) = AD_HARD
				UPDATE_HARD_AI_BASED_ON_PLAYER_ACTION(thisPlayer, thatPlayer)
			ENDIF
		BREAK
		
		CASE ASE_AI_MOVING_TO_IDLE
			IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_NO_SWING	//This IF exists to allow the animation to finish before being tasked with running to a home position
				VECTOR vDimensions
				vDimensions = (<< 0.1, 0.1, 0.5 >>)
				//If they're not at the prediction point and they're not moving to it, move them to it.
				IF VALIDATE_TENNIS_AI_GO_TO_IDLE_POS(thisPlayer, vPointToGoTo, vDimensions)
					CPRINTLN(DEBUG_TENNIS, "GET_AI_STICK_FORCE: ", thisPlayer.playerAI.texName, " Tasked to go to a home position")
					TASK_GO_STRAIGHT_TO_COORD(GET_TENNIS_PLAYER_INDEX(thisPlayer),vPointToGoTo,PEDMOVE_RUN,DEFAULT_TIME_NEVER_WARP)
					SET_AI_DESTINATION_POINT(thisPlayer.playerAI, vPointToGoTo)
					SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
					vCoord = vPointToGoTo - thisPlayer.vPos
					vCoord = vCoord/VMAG(vCoord)
					x = ROUND(vCoord.x * 128)
					y = ROUND(vCoord.y * 128)
					#IF TENNIS_DEBUG_RECORDING
					DEBUG_RECORD_ENTITY_SPHERE(thisPlayer.pIndex, "SetDestination", vPointToGoTo, 0.1, <<1.0, 0.0, 0.0>>)
					#ENDIF
					//Else if they're at the point or is the task is finised go to Idle
				ELIF (IS_ENTITY_AT_COORD(thisPlayer.pIndex, GET_AI_PREDICTION_POINT(thisPlayer.playerAI), vDimensions) 
				OR GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(thisPlayer), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK)
				AND NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_TASKED_NEW_RALLY)
					SET_TENNIS_AI_STATE(thisPlayer.playerAI, ASE_AI_IDLE)
					CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
					CPRINTLN(DEBUG_TENNIS, "GET_AI_STICK_FORCE: ", thisPlayer.playerAI.texName, " TAF_JUST_TASKED Cleared in MOVING_TO_IDLE")
				ELIF thisPlayer.playerAI.eDifficulty = AD_EASY
				AND NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_TASKED_NEW_RALLY)
					SET_TENNIS_AI_STATE(thisPlayer.playerAI, ASE_AI_IDLE)
					CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
					CPRINTLN(DEBUG_TENNIS, "GET_AI_STICK_FORCE: ", thisPlayer.playerAI.texName, " AI is Easy, not moving to a home position")
				ENDIF
				IF GET_AI_DIFFICULTY(thisPlayer.playerAI) = AD_HARD
					UPDATE_HARD_AI_BASED_ON_PLAYER_ACTION(thisPlayer, thatPlayer)
				ENDIF
			ENDIF
		BREAK
		
		CASE ASE_AI_REACTING
			IF GET_TENNIS_AI_REACTION_TIME(thisPlayer.playerAI) <= 0
				CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
				SET_TENNIS_AI_STATE(thisPlayer.playerAI, ASE_AI_MOVING_TO_BALL)
			ENDIF
			UPDATE_TENNIS_AI_REACTION_TIME(thisPlayer.playerAI)
		BREAK
		
		CASE ASE_AI_MOVING_TO_BALL
			IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_NO_SWING
				FLOAT fTValue
				VECTOR vPlayerToPred, vPlayerToBall, vBallAtPlayer, vAdd
				GET_LINE_PLANE_INTERSECTION(GET_TENNIS_BALL_POS(thisBall), GET_TENNIS_BALL_POS(thisBall) + thisBall.vBallVel, thisPlayer.vPos, thisPlayer.vMyForward, fTValue)
				vAdd = (GET_TENNIS_BALL_POS(thisBall) - GET_TENNIS_BALL_POS(thisBall) + thisBall.vBallVel) * fTValue
				vBallAtPlayer = GET_TENNIS_BALL_POS(thisBall) + vAdd
				vPlayerToPred = vPointToGoTo - thisPlayer.vPos
				// check how far I am in front of the prediction position (weight that by ball velocity?)
				FLOAT fDotPlayerPredForward
				fDotPlayerPredForward = DOT_PRODUCT(vPlayerToPred, thisPlayer.vMyForward)
				// check if I've been tasked already
				IF NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_JUST_TASKED)
					// check if this is not a lob
					IF GET_TENNIS_PLAYER_STROKE(thatPlayer) <> SHOT_LOB
						// If it's above a certain value get the line plane intersection
						IF fDotPlayerPredForward < TENNIS_AI_RUN_BACKWARD_CORRECTION
							VECTOR vBallToPlayer, vNewPrediction
							// Set my prediction off that value, plus some scaled version of the distance between
							vBallToPlayer =  thisPlayer.vPos - vBallAtPlayer
							vNewPrediction = vBallAtPlayer + (vBallToPlayer * TENNIS_PREDICTION_PLAYER_TO_BALL_SCALAR)
							// set that as the new prediction position
							SET_AI_PREDICTION_POINT(thisPlayer.playerAI, vNewPrediction)
							SET_AI_DESTINATION_POINT(thisPlayer.playerAI, vNewPrediction)
							fDotPlayerPredForward = 0	// set this perpendicular
						ENDIF
						CDEBUG2LN(DEBUG_TENNIS, "GET_AI_STICK_FORCE :: ", thisPlayer.playerAI.texName, " ASE_AI_MOVING_TO_BALL fDotPlayerPredForward=", fDotPlayerPredForward)
					ENDIF
				ENDIF
				
				IF GET_TENNIS_PLAYER_STROKE(thatPlayer) <> SHOT_LOB
					// Prevent the ped from running to prediction if they're between the ball path and prediction point
					vPlayerToBall = vBallAtPlayer - thisPlayer.vPos
					FLOAT fDotPlayerPredRight, fDotPlayerBallRight
					fDotPlayerPredRight = DOT_PRODUCT(vPlayerToPred, thisPlayer.vMyRight)
					fDotPlayerBallRight = DOT_PRODUCT(vPlayerToBall, thisPlayer.vMyRight)
					BOOL bPredRight, bBallRight
					bPredRight = fDotPlayerPredRight > 0
					bBallRight = fDotPlayerBallRight > 0
					IF VDIST2(thisPlayer.vPos, vPointToGoTo) < AI_STOP_RADIUS_SQUARED AND bPredRight <> bBallRight AND fDotPlayerPredForward < 0
						CDEBUG2LN(DEBUG_TENNIS, "GET_AI_STICK_FORCE :: ", thisPlayer.playerAI.texName, " ASE_AI_MOVING_TO_BALL :: Player-Between Checks, fDotPlayerPredRight=", fDotPlayerPredRight, ", fDotPlayerBallRight=", fDotPlayerBallRight)
						CLEAR_PED_TASKS(GET_TENNIS_PLAYER_INDEX(thisPlayer))
						SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
					ENDIF
				ENDIF
				
				//If we're not at the point and we're not moving to it, move to it.
				IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer))
				AND NOT (VDIST2(thisPlayer.vPos,vPointToGoTo) <= AI_SWING_RADIUS * AI_SWING_RADIUS)
				AND NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_JUST_TASKED)	// OR SHOULD_RETASK_MOTION(thisPlayer.playerAI))
				AND NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_TENNIS_SWING_EVENT_FLAG)
				//AND NOT IS_POINT_INSIDE_TENNIS_AREA(vRunHere, vCorner1, vCorner2, COURT_WIDTH)
					TASK_GO_STRAIGHT_TO_COORD(thisPlayer.pIndex, GET_AI_PREDICTION_POINT(thisPlayer.playerAI), PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
					vPointToGoTo = GET_AI_PREDICTION_POINT(thisPlayer.playerAI)
					SET_AI_DESTINATION_POINT(thisPlayer.playerAI, vPointToGoTo)
					CPRINTLN(DEBUG_TENNIS, "GET_AI_STICK_FORCE: ", thisPlayer.playerAI.texName, " Tasked to go to prediction point: ", vPointToGoTo)
					SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
					vCoord = GET_AI_PREDICTION_POINT(thisPlayer.playerAI) - thisPlayer.vPos
					vCoord = vCoord/VMAG(vCoord)
					x = ROUND(vCoord.x * 128)
					y = ROUND(vCoord.y * 128)
					#IF TENNIS_DEBUG_RECORDING
					DEBUG_RECORD_ENTITY_SPHERE(thisPlayer.pIndex, "SetDestination", vPointToGoTo, 0.1, <<1.0, 0.0, 0.0>>)
					#ENDIF
				//Else if we're there or if the task has finished move to Choosing_Shot
				ELIF (VDIST2(thisPlayer.vPos,vPointToGoTo) <= AI_SWING_RADIUS * AI_SWING_RADIUS)
				OR GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(thisPlayer), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
					SET_TENNIS_AI_STATE(thisPlayer.playerAI, ASE_AI_CHOOSING_SHOT)
					CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
					CPRINTLN(DEBUG_TENNIS, "GET_AI_STICK_FORCE: ", thisPlayer.playerAI.texName, " TAF_JUST_TASKED Cleared in MOVING_TO_BALL, ", PICK_STRING((GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(thisPlayer), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK), "Task was finished", "Task was ended by Radius Check"))
				ENDIF
			ENDIF
		BREAK
		
		CASE ASE_AI_CHOOSING_SHOT
			x = 0
			y = 0
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Easy AI faults occassionally and never hits above a C grade
///    Normal AI fault sometimes and never hits above an A grade
///    HARD AI never faults and never hits below a B grade
/// PARAMS:
///    thisProps - 
///    thisStatus - 
///    thisPlayer - 
/// RETURNS:
///    
FUNC BOOL UPDATE_SERVE_AI(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATUS &thisStatus, TENNIS_PLAYER &thisPlayer, BOOL bFreemodeIntro=FALSE)
	INT iGrade
	LAUNCH_DETAILS serveGrade
	
	IF NOT bFreemodeIntro
		IF (GET_TENNIS_TIMER_A(thisProps) > GET_TENNIS_STATUS_AI_WAIT(thisStatus) AND GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_NO_SWING AND thisProps.eTennisActivity <> TA_AI_VS_AI)
		OR (GET_TENNIS_TIMER_A(thisProps) > GET_TENNIS_STATUS_AI_WAIT(thisStatus) AND GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_NO_SWING AND thisProps.eTennisActivity = TA_AI_VS_AI AND IS_SCREEN_FADED_IN())
			SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, TSS_SERVE_TOSS)
			SET_TENNIS_TIMER_A(thisProps, 0)
		ENDIF
	ELSE
		IF IS_TENNIS_GLOBAL_FLAG_SET(TGF_START_AMBIENT_TENNIS)
			SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, TSS_SERVE_TOSS)
			SET_TENNIS_TIMER_A(thisProps, 0)
		ENDIF
	ENDIF
	
	IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_SERVE_EXECUTING)
		FLOAT fAngleA, fAngleB, fAngleResult
		VECTOR vAim, v1
		
		//Instead of vMyRight this should be scalable so the AI can aim to different spots
		IF GET_TENNIS_SERVING_SIDE(thisStatus) = SERVING_FROM_RIGHT
			IF thisPlayer.eCourtSide = (TENNIS_PLAYER_AWAY)
				vAim = thisProps.tennisCourt.vCourtCorners[3] + thisPlayer.vMyRight
				v1 = thisProps.tennisCourt.vCourtCorners[3]
			ELSE
				vAim = thisProps.tennisCourt.vCourtCorners[1] + thisPlayer.vMyRight
				v1 = thisProps.tennisCourt.vCourtCorners[1]
			ENDIF
		ELSE
			IF thisPlayer.eCourtSide = (TENNIS_PLAYER_AWAY)
				vAim = thisProps.tennisCourt.vCourtCorners[2] - thisPlayer.vMyRight
				v1 = thisProps.tennisCourt.vCourtCorners[2]
			ELSE
				vAim = thisProps.tennisCourt.vCourtCorners[0] - thisPlayer.vMyRight
				v1 = thisProps.tennisCourt.vCourtCorners[0]
			ENDIF
		ENDIF
		
		IF thisProps.eTennisActivity = TA_AI_VS_AI
			iGrade = ENUM_TO_INT(LD_SERVE_A)
		ELSE
			INT iNonFault
			IF thisPlayer.playerAI.eDifficulty = AD_EASY
				iNonFault = GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(LD_SERVE_A), ENUM_TO_INT(LD_SERVE_FAULT))
				iGrade = PICK_INT(GET_RANDOM_FLOAT_IN_RANGE() < AD_EASY_FAULT_PROBABILITY, ENUM_TO_INT(LD_SERVE_FAULT), iNonFault)
				CPRINTLN(DEBUG_TENNIS, "EASY AI grade: ", iGrade)
			ELIF thisPlayer.playerAI.eDifficulty = AD_NORMAL
				iNonFault = GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(LD_SERVE_S), ENUM_TO_INT(LD_SERVE_C))
				iGrade = PICK_INT(GET_RANDOM_FLOAT_IN_RANGE() < AD_NORMAL_FAULT_PROBABILITY, ENUM_TO_INT(LD_SERVE_FAULT), iNonFault)
				CPRINTLN(DEBUG_TENNIS, "NORMAL AI grade: ", iGrade)
			ELIF thisPlayer.playerAI.eDifficulty = AD_HARD
				iGrade = GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(LD_SERVE_S_PLUS), ENUM_TO_INT(LD_SERVE_A))
				CPRINTLN(DEBUG_TENNIS, "HARD AI grade: ", iGrade)
			ENDIF
		ENDIF
		vAim = LERP_VECTOR(v1, vAim, GET_RANDOM_FLOAT_IN_RANGE())
		fAngleA = GET_HEADING_BETWEEN_VECTORS(thisPlayer.vPos, vAim)
		fAngleB = GET_HEADING_BETWEEN_VECTORS(thisPlayer.vPos, thisPlayer.vPos + thisPlayer.vMyForward)
		fAngleResult = fAngleA - fAngleB
		
//		PRINTLN("___vAim: ", vAim)

		IF bFreemodeIntro
			iGrade = ENUM_TO_INT(LD_SERVE_A)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF AI_SERVE_OVERRIDE
				iGrade = DEBUG_SERVE_OVERRIDE
			ENDIF
		#ENDIF
		
		serveGrade = thisProps.grades[iGrade]
		thisPlayer.fLeftRight = fAngleResult
		SET_TENNIS_PLAYER_STROKE(thisPlayer, SERVE)
		IF iGrade = ENUM_TO_INT(LD_SERVE_S) OR iGrade = ENUM_TO_INT(LD_SERVE_S_PLUS)
			PLAY_SOUND_FROM_ENTITY(-1, PICK_STRING(thisProps.eTennisActivity = TA_AI_VS_AI, "TENNIS_AMB_SMASH_MASTER", "TENNIS_NPC_SMASH_MASTER"), GET_TENNIS_PLAYER_INDEX(thisPlayer))
		ELSE
			PLAY_SOUND_FROM_ENTITY(-1, PICK_STRING(thisProps.eTennisActivity = TA_AI_VS_AI, "TENNIS_AMB_SERVE_MASTER", "TENNIS_NPC_SERVE_MASTER"), GET_TENNIS_PLAYER_INDEX(thisPlayer))
		ENDIF
		DETACH_ENTITY(thisProps.tennisBall.oBall,FALSE)
		LAUNCH_BALL(thisProps, thisPlayer, serveGrade)
		CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_SERVE_EXECUTING)
		CPRINTLN(DEBUG_TENNIS, "AI iGrade used: ", GET_STRING_FROM_TENNIS_SERVE_GRADE(INT_TO_ENUM(LAUNCH_DETAIL_ENUM, iGrade)))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

