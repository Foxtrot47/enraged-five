USING "minigames_helpers.sch"
USING "minigame_UIInputs.sch"
USING "script_player.sch"

USING "tennis_player.sch"
USING "tennis.sch"
USING "tennis_ball.sch"
USING "tennis_court.sch"
USING "tennis_court_lib.sch"
USING "tennis_ai.sch"
USING "tennis_ai_lib.sch"
USING "tennis_helpers_lib.sch"
USING "tennis_broadcast_lib.sch"

PROC INITIALIZE_TENNIS_PLAYER(TENNIS_PLAYER &thisPlayer, TENNIS_PLAYER_ID eThisSelfID, PLAYER_CONTROL_TYPE newControlType, VECTOR vNewForward, VECTOR vNewRight)
	thisPlayer.eSwingState = TSS_NO_SWING
	thisPlayer.eCourtSide = eThisSelfID
	thisPlayer.vMyForward = vNewForward/VMAG(vNewForward)
	thisPlayer.vMyRight = vNewRight/VMAG(vNewRight)
	thisPlayer.fStrongSwingTimer = STRONG_SWING_THRESHOLD + 1
	
	SWITCH newControlType
		CASE CONTROL_NONE
			thisPlayer.controlType = newControlType
		BREAK
		CASE HUMAN_LOCAL
			thisPlayer.controlType = newControlType
		BREAK
		CASE HUMAN_LOCAL_MP
			thisPlayer.controlType = newControlType
		BREAK
		CASE HUMAN_NETWORK
			thisPlayer.controlType = newControlType
		BREAK
		CASE COMPUTER
			INITIALIZE_TENNIS_AI(thisPlayer.playerAI)
			thisPlayer.controlType = newControlType
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	STRING sName = PICK_STRING(eThisSelfID = TENNIS_PLAYER_AWAY, "Plyr1", "Plyr2")
	thisPlayer.playerAI.texName = ""
	thisPlayer.playerAI.texName += sName
	#ENDIF
ENDPROC

/// PURPOSE:
///    Used to determine the offset we'll run to from the predicted offset
/// PARAMS:
///    thisPlayer - 
///    vPredictedBallPos - from GET_CLOSEST_POINT_FOR_TENNIS_SWING
/// RETURNS:
///    
FUNC VECTOR GET_TENNIS_PLAYER_POINT_ON_BALL_PLANE(TENNIS_PLAYER &thisPlayer, VECTOR vPredictedBallPos)
	VECTOR vCoolOffset = vPredictedBallPos - (thisPlayer.vPos)	// + GET_ENTITY_VELOCITY(GET_TENNIS_PLAYER_INDEX(thisPlayer)) * 0.5)
	FLOAT fDistToMove = DOT_PRODUCT(vCoolOffset, thisPlayer.vMyForward)
	VECTOR vCoolMove = thisPlayer.vMyForward * fDistToMove
	RETURN thisPlayer.vPos + vCoolMove
ENDFUNC

//Function to estimate the cost to the player of a particular shot
FUNC FLOAT GET_SHOT_EXTRA_COST(TENNIS_SWING_STATES swing)
	SWITCH(swing)
		CASE TSS_CLOSE_BH_TS_HI
		CASE TSS_CLOSE_BH_TS_MID
		CASE TSS_CLOSE_BH_TS_LOW
		CASE TSS_CLOSE_BH_BS_HI
		CASE TSS_CLOSE_BH_BS_MID
		CASE TSS_CLOSE_BH_BS_LOW			
		CASE TSS_CLOSE_FH_TS_HI
		CASE TSS_CLOSE_FH_TS_MID
		CASE TSS_CLOSE_FH_TS_LOW
		CASE TSS_CLOSE_FH_BS_HI
		CASE TSS_CLOSE_FH_BS_MID
		CASE TSS_CLOSE_FH_BS_LOW
			RETURN LOW_COST_SHOT_COST	//Close shots are currently considered less favorably than simple forhand and backhand shots
		CASE TSS_LUNGE_BACKHAND_HI
		CASE TSS_LUNGE_BACKHAND_MID
		CASE TSS_LUNGE_BACKHAND_LOW
		CASE TSS_LUNGE_FOREHAND_HI
		CASE TSS_LUNGE_FOREHAND_MID
		CASE TSS_LUNGE_FOREHAND_LOW
			RETURN LOW_COST_SHOT_COST
		CASE TSS_DIVE_BACKHAND
		CASE TSS_DIVE_FOREHAND
			RETURN HIGH_COST_SHOT_COST	//Least favorable!
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC FLOAT DIST_PLAYER_CAN_TRAVEL(TENNIS_PLAYER &thisPlayer, VECTOR vNavPoint, FLOAT fTimeAvailable, FLOAT &fDistToShot)
	VECTOR toNavPoint = vNavPoint - thisPlayer.vPos
	fDistToShot = VMAG(toNavPoint)
	IF fDistToShot > TENNIS_PLAYER_RUN_VELOCITY
		toNavPoint *= (TENNIS_PLAYER_RUN_VELOCITY / fDistToShot)
	ENDIF
	VECTOR vPlayerVel = GET_ENTITY_VELOCITY(thisPlayer.pIndex)
	FLOAT fAccelNeeded = VDIST(toNavPoint, vPlayerVel) * TENNIS_PLAYER_ACCEL_ALLOW / TENNIS_PLAYER_RUN_VELOCITY
	FLOAT fDistWeCanTravel = (fTimeAvailable * TENNIS_PLAYER_RUN_VELOCITY) - fAccelNeeded
	IF fDistWeCanTravel < 0
		fDistWeCanTravel = 0
	ENDIF
	RETURN fDistWeCanTravel
ENDFUNC

FUNC FLOAT CALCULATE_SHORT_FALL(TENNIS_PLAYER &thisPlayer, VECTOR &vSwingStateOffsets[], TENNIS_SWING_PHASES &phasesArray[], VECTOR &vFuturePoints[])
	FLOAT fClosestPossible = 100.0
	BOOL bFound = FALSE
	INT p = 0
	WHILE p < ENUM_TO_INT(TSS_NO_SWING)
		IF phasesArray[p].fActive >= 0	// Account for uninitialized entries in the array
			VECTOR vAdjustedOffset = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, vSwingStateOffsets[p])
			INT iFrameDelayToSweetSpot = FLOOR((phasesArray[p].fActive - phasesArray[p].fInit) * GET_TOTAL_FRAMES_ADJUSTED_FOR_FRAME_TIME(phasesArray[p].iFrameTot))
			INT iFrameCheck = iFrameDelayToSweetSpot
			WHILE iFrameCheck < COUNT_OF(vFuturePoints)
				FLOAT fTimeToShot = iFrameCheck * GET_FRAME_TIME()
				VECTOR vRequiredPosition = vFuturePoints[iFrameCheck] - vAdjustedOffset
				FLOAT distToTravel=0.0
				FLOAT distCanTravel = DIST_PLAYER_CAN_TRAVEL(thisPlayer, vRequiredPosition, fTimeToShot, distToTravel)
				FLOAT fShortFall = distToTravel - distCanTravel
				IF fShortFall <= 0.0
					RETURN 0.0			//Can't get any shorter than this
				ENDIF
				IF (fShortFall < fClosestPossible)
					fClosestPossible = fShortFall
					bFound = TRUE
				ENDIF
				iFrameCheck++
			ENDWHILE
		ENDIF
		p++
	ENDWHILE	
	IF NOT bFound
		//We're fine
		RETURN 0.0
	ENDIF
	RETURN fClosestPossible
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisPlayer - 
///    vSwingStateOffsets - []
/// RETURNS:
///    
FUNC VECTOR GET_BEST_TENNIS_PREDICTION_FROM_OFFSET(TENNIS_PLAYER &thisPlayer, VECTOR &vSwingStateOffsets[], VECTOR vPredictedBallPos, FLOAT fCourtZ, FLOAT fTimeToPrediction)
	CPRINTLN(DEBUG_TENNIS, "GET_BEST_TENNIS_PREDICTION_FROM_OFFSET called")
	INT p = 0
	VECTOR vAdjustedOffset, vReturn, vStoredOffset
	FLOAT fDistToBall = 100
	FLOAT fFuncDist
	VECTOR vPlayerOnBallPlane = GET_TENNIS_PLAYER_POINT_ON_BALL_PLANE(thisPlayer, vPredictedBallPos)
	FLOAT fDot = DOT_PRODUCT(vPredictedBallPos - thisPlayer.vPos, thisPlayer.vMyRight)
	BOOL bRightSide = fDot > 0
	#IF IS_DEBUG_BUILD
	#IF TENNIS_DEBUG_RECORDING
	TEXT_LABEL_31 texName
	INT iBestStroke
	#ENDIF
	#ENDIF
	CPRINTLN(DEBUG_TENNIS, "fDot ", fDot, ", bRightSide ", PICK_STRING(bRightSide, "RIGHT", "LEFT"))
	WHILE p < ENUM_TO_INT(TSS_NO_SWING)
		#IF IS_DEBUG_BUILD
		#IF TENNIS_DEBUG_RECORDING
			vAdjustedOffset = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, vSwingStateOffsets[p])
			texName = GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, p))
			DEBUG_RECORD_SPHERE(texName, vAdjustedOffset + thisPlayer.vPos, 0.1, <<1.0,0.0,1.0>>)
		#ENDIF
		#ENDIF
		
		BOOL bShotIsRight = vSwingStateOffsets[p].x > 0
		IF (bRightSide = bShotIsRight)
			vAdjustedOffset = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, vSwingStateOffsets[p])
			
			//Height check to see if we can use this shot
			fFuncDist = ABSF((vAdjustedOffset.z + vPlayerOnBallPlane.z) -  vPredictedBallPos.z)
			IF (fFuncDist < GOOD_SHOT_ALLOWANCE)
				fFuncDist = 0	//Shot cost then becomes the dominating factor
			ENDIF
			//Add in the cost of the shot
			fFuncDist += GET_SHOT_EXTRA_COST(INT_TO_ENUM(TENNIS_SWING_STATES, p))

			//Add navigation error
			VECTOR vNavPoint = vPredictedBallPos - vAdjustedOffset
			FLOAT fDistToShot = 0.0
			FLOAT fDistWeCanTravel = DIST_PLAYER_CAN_TRAVEL(thisPlayer, vNavPoint, fTimeToPrediction, fDistToShot)
			IF fDistWeCanTravel < fDistToShot
				fFuncDist += fDistToShot - fDistWeCanTravel
			ENDIF
			
			IF fFuncDist < fDistToBall
				fDistToBall = fFuncDist
				vStoredOffset = vAdjustedOffset
				#IF IS_DEBUG_BUILD
				#IF TENNIS_DEBUG_RECORDING
					iBestStroke = p
				#ENDIF
				#ENDIF
			ENDIF
				
			#IF IS_DEBUG_BUILD
			#IF TENNIS_DEBUG_RECORDING
			VECTOR linePosA = vAdjustedOffset + thisPlayer.vPos
			VECTOR linePosB = linePosA
			linePosB.z += fFuncDist
			DEBUG_RECORD_LINE("BallError", linePosA, linePosB, <<1,1,0>>, <<0,1,0>>)
			IF fDistWeCanTravel < fDistToShot
				linePosB.z = linePosA.z
				linePosB.z -= fDistToShot - fDistWeCanTravel
				DEBUG_RECORD_LINE("fNavShortFall", linePosA, linePosB, (<<1,0,0>>), (<<1,0,0>>))
//				DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fDistWeCanTravel", fDistWeCanTravel)
			ENDIF
			#ENDIF
			#ENDIF
		ENDIF
		p++
	ENDWHILE
	
	vReturn = vPredictedBallPos - vStoredOffset
	vReturn.z = fCourtZ
	
	#IF IS_DEBUG_BUILD
	#IF TENNIS_DEBUG_RECORDING
	DEBUG_RECORD_LINE("PedToPredictedBat", vReturn, vReturn + vStoredOffset, (<<0,1,0>>), (<<1,0,0>>))
//	DEBUG_RECORD_ENTITY_STRING(thisPlayer.pIndex, "ShotPredicted", GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, iBestStroke)))
	DEBUG_RECORD_SPHERE("PredictedPosition", vPredictedBallPos, 0.05, <<1.0,1.0,1.0>>)
	DEBUG_RECORD_SPHERE("Best Predicted Offset", thisPlayer.vPos + vStoredOffset, 0.1, (<<1.0,0,0>>))
	DEBUG_RECORD_SPHERE("vReturn", vReturn, 0.15, <<0,1,0>>)
	CPRINTLN(DEBUG_TENNIS, "iBestStroke ", iBestStroke)
	#ENDIF
	#ENDIF
	
	RETURN vReturn
ENDFUNC

FUNC VECTOR CALCULATE_2D_BARYCENTRIC_COORDS(FLOAT ax, FLOAT ay, FLOAT bx, FLOAT by, FLOAT cx, FLOAT cy, FLOAT vecX, FLOAT vecY)
	VECTOR lambda
	FLOAT den = 1.0 / ((by - cy) * (ax - cx) + (cx - bx) * (ay - cy))
 
	lambda.x = ((by - cy) * (vecX - cx) + (cx - bx) * (vecY - cy)) * den
	lambda.y = ((cy - ay) * (vecX - cx) + (ax - cx) * (vecY - cy)) * den
	lambda.z = 1 - lambda.x - lambda.y
 
	RETURN lambda
ENDFUNC

/// PURPOSE:
///    Used to get the blend for the 4-way blend played thru the MoVE network.
/// PARAMS:
///    thisPlayer - 
///    vPredictedBallPos - 
///    vNextPredictedBallPos - 
///    topLeft - 
///    topRight - 
///    bottomRight - 
///    bottomLeft - 
///    bRightSide - 
///    fVertBlend - 
///    fHorBlend - 
/// RETURNS:
///    
FUNC TENNIS_DIVE_DiveType GET_TENNIS_DIVE_BLEND_TO_PLAY(TENNIS_PLAYER &thisPlayer, VECTOR &vPredictedBallPos, VECTOR &vNextPredictedBallPos, VECTOR &topLeft, VECTOR &bottomRight, VECTOR &bottomLeft, BOOL bRightSide, FLOAT &fVertBlend, FLOAT &fHorBlend, VECTOR &vBallLocOut)

	FLOAT fRatio = 0.0
	GET_LINE_PLANE_INTERSECTION(vPredictedBallPos, vNextPredictedBallPos, thisPlayer.vPos, thisPlayer.vMyForward, fRatio)
	IF fRatio < MIN_DIVE_SEGMENT_RATIO OR fRatio > MAX_DIVE_SEGMENT_RATIO
//		CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_DIVE_BLEND_TO_PLAY :: RETURN TENNIS_DIVE_nodive, fRatio < MIN_DIVE_SEGMENT_RATIO OR fRatio > MAX_DIVE_SEGMENT_RATIO")
		RETURN TENNIS_DIVE_nodive
	ENDIF
	#IF TENNIS_DEBUG_RECORDING
	DEBUG_RECORD_SPHERE("ballNaught", vPredictedBallPos, 0.1, (<<255,255,0>>))
	DEBUG_RECORD_SPHERE("ballPrime", vNextPredictedBallPos, 0.1, (<<255,200,0>>))
	#ENDIF
	VECTOR vBallPosForTest = vPredictedBallPos + (vNextPredictedBallPos - vPredictedBallPos) * fRatio
	#IF IS_DEBUG_BUILD
	#IF TENNIS_DEBUG_RECORDING
	TEXT_LABEL_31 texName = "DiveBallPos_"
	texName += ROUND(fRatio * 100)
	DEBUG_RECORD_SPHERE(texName, vBallPosForTest, 0.1, <<1.0, 0.7, 0.7>>)
	#ENDIF
	#ENDIF
	//Finding the triangle to use, need ball location in local coords to player
	VECTOR vBallR = vBallPosForTest - thisPlayer.vPos
	FLOAT vecX = DOT_PRODUCT(vBallR, thisPlayer.vMyRight)
	IF ABSF(vecX) < DIVE_DIST_TO_SIDE_THRESHOLD
//		CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_DIVE_BLEND_TO_PLAY :: RETURN TENNIS_DIVE_nodive, ABSF(vecX) < DIVE_DIST_TO_SIDE_THRESHOLD")
		RETURN TENNIS_DIVE_nodive
	ENDIF
	FLOAT vecY = vBallR.z
	VECTOR vBallLocal = <<vecX, 0.0, vecY>>
	fVertBlend = GET_RATIO_OF_CLOSEST_POINT_ON_LINE(vBallLocal, bottomLeft, topLeft, FALSE)
	fHorBlend = GET_RATIO_OF_CLOSEST_POINT_ON_LINE(vBallLocal, PICK_VECTOR(bRightSide, bottomLeft, bottomRight), PICK_VECTOR(bRightSide, bottomRight, bottomLeft), FALSE)
	IF fVertBlend < 0 OR fVertBlend > 2.0 OR fHorBlend < 0 OR fHorBlend > 2.0
		RETURN TENNIS_DIVE_nodive
	ENDIF
	vBallLocOut = vBallPosForTest
	
	RETURN TENNIS_DIVE_DiveLeft_BottomRight
ENDFUNC

/// PURPOSE:
///    Gets a dive to play thru barycentric coords, a 3-way blend
/// PARAMS:
///    thisPlayer - 
///    vPredictedBallPos - 
///    vNextPredictedBallPos - 
///    topLeft - 
///    topRight - 
///    bottomRight - 
///    bottomLeft - 
///    bRightSide - 
///    vBaryAnimationOut - 
/// RETURNS:
///    
FUNC TENNIS_DIVE_DiveType GET_DIVE_TO_PLAY(TENNIS_PLAYER &thisPlayer, VECTOR &vPredictedBallPos, VECTOR &vNextPredictedBallPos, VECTOR &topLeft, VECTOR &topRight, VECTOR &bottomRight, VECTOR &bottomLeft, BOOL bRightSide, VECTOR &vBaryAnimationOut)
	FLOAT fRatio = 0.0
	GET_LINE_PLANE_INTERSECTION(vPredictedBallPos, vNextPredictedBallPos, thisPlayer.vPos, thisPlayer.vMyForward, fRatio)
	IF fRatio < MIN_DIVE_SEGMENT_RATIO OR fRatio > MAX_DIVE_SEGMENT_RATIO
//		CPRINTLN(DEBUG_TENNIS, "GET_DIVE_TO_PLAY :: RETURN TENNIS_DIVE_nodive, fRatio < MIN_DIVE_SEGMENT_RATIO OR fRatio > MAX_DIVE_SEGMENT_RATIO")
		RETURN TENNIS_DIVE_nodive
	ENDIF
	#IF TENNIS_DEBUG_RECORDING
	#IF IS_DEBUG_BUILD
	DEBUG_RECORD_SPHERE("ballNaught", vPredictedBallPos, 0.1, (<<255,255,0>>))
	DEBUG_RECORD_SPHERE("ballPrime", vNextPredictedBallPos, 0.1, (<<255,200,0>>))
	#ENDIF
	#ENDIF
	VECTOR vBallPosForTest = vPredictedBallPos + (vNextPredictedBallPos - vPredictedBallPos) * fRatio
	#IF IS_DEBUG_BUILD
	#IF TENNIS_DEBUG_RECORDING
	TEXT_LABEL_31 texName = "DiveBallPos_"
	texName += ROUND(fRatio * 100)
	DEBUG_RECORD_SPHERE(texName, vBallPosForTest, 0.1, <<1.0, 0.7, 0.7>>)
	#ENDIF
	#ENDIF
	//Finding the triangle to use, need ball location in local coords to player
	VECTOR vBallR = vBallPosForTest - thisPlayer.vPos
	FLOAT vecX = DOT_PRODUCT(vBallR, thisPlayer.vMyRight)
	IF ABSF(vecX) < DIVE_DIST_TO_SIDE_THRESHOLD 
//		CPRINTLN(DEBUG_TENNIS, "GET_DIVE_TO_PLAY :: RETURN TENNIS_DIVE_nodive, ABSF(vecX) < DIVE_DIST_TO_SIDE_THRESHOLD")
		RETURN TENNIS_DIVE_nodive
	ENDIF
	FLOAT vecY = vBallR.z
	VECTOR vBallLocal = <<vecX, 0.0, vecY>>
//	FLOAT fVertical = GET_RATIO_OF_CLOSEST_POINT_ON_LINE(vBallLocal, topLeft, bottomLeft, FALSE)
	FLOAT fHorizontal = GET_RATIO_OF_CLOSEST_POINT_ON_LINE(vBallLocal, bottomLeft, bottomRight, FALSE)
	#IF IS_DEBUG_BUILD
	#IF TENNIS_DEBUG_RECORDING
	//	DEBUG_RECORD_SPHERE("vBallLocal", ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, vBallLocal) + thisPlayer.vPos, 0.1, (<<255,255,0>>))
//		IF bRightSide AND thisProps.eTennisActivity <> TA_AI_VS_AI
//			DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "DiveRatioRight", fHorizontal)
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "Dive_vBallLocal", vBallLocal, FALSE)
//		ELIF thisProps.eTennisActivity <> TA_AI_VS_AI
//			DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "DiveRatioLeft", fHorizontal)
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "Dive_vBallLocal", vBallLocal, FALSE)
//		ENDIF
		
		VECTOR vAdjTopLeft = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, topLeft)
		VECTOR vAdjTopRight = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, topRight)
		VECTOR vAdjBotLeft = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, bottomLeft)
		VECTOR vAdjBotRight = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, bottomRight)
		texName = "diveTopLeft_"
		texName += ROUND(vecX * 100)
		texName += "cm"
//		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "xDistToSide", vecX)
		DEBUG_RECORD_SPHERE(texName, thisPlayer.vPos + vAdjTopLeft, 0.05, (<<255,255,255>>))
		DEBUG_RECORD_SPHERE("diveTopRight", thisPlayer.vPos + vAdjTopRight, 0.05, (<<255,255,255>>))
		DEBUG_RECORD_SPHERE("diveBotLeft", thisPlayer.vPos + vAdjBotLeft, 0.05, (<<255,255,255>>))
		DEBUG_RECORD_SPHERE("diveBotRight", thisPlayer.vPos + vAdjBotRight, 0.05, (<<255,255,255>>))
	#ENDIF
	#ENDIF

	//Allow some excess
	IF fHorizontal >= -1.0 AND fHorizontal <= 2.0
		VECTOR vDiagonal = GET_CLOSEST_POINT_ON_LINE(vBallLocal, bottomLeft, topRight, FALSE)
		VECTOR vCross = CROSS_PRODUCT(topRight - bottomLeft, bottomRight - bottomLeft)
		VECTOR vDiagNormal = CROSS_PRODUCT(topRight - bottomLeft, vCross)
		FLOAT fDotDiagBall = DOT_PRODUCT(vBallLocal - vDiagonal, vDiagNormal)
		#IF IS_DEBUG_BUILD
		#IF TENNIS_DEBUG_RECORDING
			VECTOR vAdjDiagonal = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, vDiagonal)
			vAdjDiagonal += thisPlayer.vPos
			VECTOR vAdjCross = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, vCross) + thisPlayer.vPos + vAdjBotLeft
			VECTOR vAdjDiagNormal = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, vDiagNormal) + thisPlayer.vPos + vAdjBotLeft
			TEXT_LABEL_31 texDiagBall = "vDiagBall_"
			texDiagBall += ROUND(fDotDiagBall * 100)
			DEBUG_RECORD_SPHERE("vDiagonal", vAdjDiagonal, 0.05, (<<155,155,155>>))
//			DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "UpOrDown", vDiagonal.z - vBallLocal.z)
			DEBUG_RECORD_LINE("diagonalLine", thisPlayer.vPos + vAdjBotLeft, thisPlayer.vPos + vAdjTopRight, (<<255,255,255>>), (<<255,255,255>>))
			DEBUG_RECORD_LINE("vCrossBlue", thisPlayer.vPos + vAdjBotLeft, vAdjCross, (<<255,255,255>>), (<<0,0,255>>))
			DEBUG_RECORD_LINE("vDiagNormalRed", thisPlayer.vPos + vAdjBotLeft, vAdjDiagNormal, (<<255,255,255>>), (<<255,0,0>>))
			DEBUG_RECORD_LINE(texDiagBall, vAdjDiagonal, ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, vBallLocal) + thisPlayer.vPos, (<<255,255,255>>), (<<255,255,255>>))
		#ENDIF
		#ENDIF
		IF fDotDiagBall > 0
			//Top left triangle
			#IF IS_DEBUG_BUILD
			#IF TENNIS_DEBUG_RECORDING
			VECTOR vUp = vAdjDiagonal
			vUp.z += 0.3
			DEBUG_RECORD_LINE("TopTriangle", vAdjDiagonal, vUp, (<<155,155,155>>), (<<155,155,155>>))
			#ENDIF
			#ENDIF
			//Calculate barycentric coordinates
			vBaryAnimationOut = CALCULATE_2D_BARYCENTRIC_COORDS(topLeft.x, topLeft.z, topRight.x, topRight.z, bottomLeft.x, bottomLeft.z, vecX, vecY)
			
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "vBaryAnimationOut", vBaryAnimationOut, FALSE)
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "vBary-A", <<topLeft.x, topLeft.z, 0.0>>, FALSE)
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "vBary-B", <<topRight.x, topRight.z, 0.0>>, FALSE)
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "vBary-C", <<bottomLeft.x, bottomLeft.z, 0.0>>, FALSE)
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "vBary-Test", <<vecX, vecY, 0.0>>, FALSE)
			//Store animation blend
			IF 		(vBaryAnimationOut.x >= 0.0) AND (vBaryAnimationOut.x <= 1.0)
			AND 	(vBaryAnimationOut.y >= 0.0) AND (vBaryAnimationOut.y <= 1.0)
			AND 	(vBaryAnimationOut.z >= 0.0) AND (vBaryAnimationOut.z <= 1.0)
				//On the triangle
				//Edge detection and dodgy shots to the outside would be cool
				//With better results towards the inside...
				//But for now let's just return the shot
				//And store the blend
				IF bRightSide
					RETURN TENNIS_DIVE_DiveRight_TopLeft
				ELSE
					RETURN TENNIS_DIVE_DiveLeft_TopLeft
				ENDIF
			ELSE
				vBaryAnimationOut.x = CLAMP(vBaryAnimationOut.x, 0.0, 1.0)
				vBaryAnimationOut.y = CLAMP(vBaryAnimationOut.y, 0.0, 1.0)
				vBaryAnimationOut.z = CLAMP(vBaryAnimationOut.z, 0.0, 1.0)
				IF bRightSide
					RETURN TENNIS_DIVE_DiveRight_TopLeft
				ELSE
					RETURN TENNIS_DIVE_DiveLeft_TopLeft
				ENDIF
			ENDIF			
		ELSE
			//Bottom right triangle
			#IF IS_DEBUG_BUILD
			#IF TENNIS_DEBUG_RECORDING
			VECTOR vDown = vAdjDiagonal
			vDown.z -= 0.3
			DEBUG_RECORD_LINE("BotTriangle", vAdjDiagonal, vDown, (<<155,155,155>>), (<<155,155,155>>))
			#ENDIF
			#ENDIF
			//Calculate barycentric coordinates
			vBaryAnimationOut = CALCULATE_2D_BARYCENTRIC_COORDS(bottomLeft.x, bottomLeft.z, topRight.x, topRight.z, bottomRight.x, bottomRight.z, vecX, vecY)
					
			//Store animation blend
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "vBaryAnimationOut", vBaryAnimationOut, FALSE)
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "vBary-A", <<bottomLeft.x, bottomLeft.z, 0.0>>, FALSE)
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "vBary-B", <<topRight.x, topRight.z, 0.0>>, FALSE)
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "vBary-C", <<bottomRight.x, bottomRight.z, 0.0>>, FALSE)
//			DEBUG_RECORD_ENTITY_VECTOR(thisPlayer.pIndex, "vBary-Test", <<vecX, vecY, 0.0>>, FALSE)
			IF 		(vBaryAnimationOut.x >= 0.0) AND (vBaryAnimationOut.x <= 1.0)
			AND 	(vBaryAnimationOut.y >= 0.0) AND (vBaryAnimationOut.y <= 1.0)
			AND 	(vBaryAnimationOut.z >= 0.0) AND (vBaryAnimationOut.z <= 1.0)
				//On the triangle
				//Edge detection and dodgy shots to the outside would be cool
				//With better results towards the inside...
				//But for now let's just return the shot
				//And store the blend
				IF bRightSide
					RETURN TENNIS_DIVE_DiveRight_BottomRight
				ELSE
					RETURN TENNIS_DIVE_DiveLeft_BottomRight
				ENDIF
			ELSE
				vBaryAnimationOut.x = CLAMP(vBaryAnimationOut.x, 0.0, 1.0)
				vBaryAnimationOut.y = CLAMP(vBaryAnimationOut.y, 0.0, 1.0)
				vBaryAnimationOut.z = CLAMP(vBaryAnimationOut.z, 0.0, 1.0)
				IF bRightSide
					RETURN TENNIS_DIVE_DiveRight_BottomRight
				ELSE
					RETURN TENNIS_DIVE_DiveLeft_BottomRight
				ENDIF
			ENDIF		
		ENDIF
	ENDIF
	RETURN TENNIS_DIVE_NoDive
ENDFUNC



FUNC VECTOR IN_PLAYER_COORDS(TENNIS_PLAYER &thisPlayer, VECTOR &a)
	VECTOR mapped = thisPlayer.vMyRight * a.x
	mapped += thisPlayer.vMyForward * a.y
	mapped.z = a.z
	mapped += thisPlayer.vPos
	RETURN mapped
ENDFUNC
							
FUNC VECTOR GET_DIVE_LOCATION_FROM_TRIANGLE(TENNIS_PLAYER &thisPlayer, VECTOR &a, VECTOR &b, VECTOR &c, VECTOR &vBary)
	VECTOR aPlayer = IN_PLAYER_COORDS(thisPlayer, a)
	VECTOR bPlayer = IN_PLAYER_COORDS(thisPlayer, b)
	VECTOR cPlayer = IN_PLAYER_COORDS(thisPlayer, c)

	VECTOR vContactPoint = vBary.x * aPlayer
	vContactPoint += vBary.y * bPlayer
	vContactPoint += vBary.z * cPlayer
	RETURN vContactPoint
ENDFUNC

FUNC VECTOR GET_DIVE_LOCATION_FROM_DATA(TENNIS_PLAYER &thisPlayer, TENNIS_DIVE_DiveType foundDive, VECTOR &vBary)
	SWITCH(foundDive)
	CASE TENNIS_DIVE_DiveLeft_TopLeft
		//AnimationsToBlend: Left_TopLeft, 		Left_TopRight, 		Left_BottomLeft
		RETURN GET_DIVE_LOCATION_FROM_TRIANGLE(thisPlayer, DIVE_Left_TopLeft, DIVE_Left_TopRight, DIVE_Left_BottomLeft, vBary)
	CASE TENNIS_DIVE_DiveLeft_BottomRight		//AnimationsToBlend: Left_BottomLeft, 	Left_TopRight, 		Left_BottomRight
		RETURN GET_DIVE_LOCATION_FROM_TRIANGLE(thisPlayer, DIVE_Left_BottomLeft, DIVE_Left_TopRight, DIVE_Left_BottomRight, vBary)
	CASE TENNIS_DIVE_DiveRight_TopLeft			//AnimationsToBlend: Right_TopLeft, 	Right_TopRight, 	Right_BottomRight
		RETURN GET_DIVE_LOCATION_FROM_TRIANGLE(thisPlayer, DIVE_Right_TopLeft, DIVE_Right_TopRight, DIVE_Right_BottomRight, vBary)
	CASE TENNIS_DIVE_DiveRight_BottomRight		//AnimationsToBlend: Right_TopRight, 	Right_BottomRight, 	Right_BottomLeft	
		RETURN GET_DIVE_LOCATION_FROM_TRIANGLE(thisPlayer, DIVE_Right_TopRight, DIVE_Right_BottomRight, DIVE_Right_BottomLeft, vBary)
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_CENTRE_OF_BAT(TENNIS_PLAYER &thisPlayer)
	VECTOR vObjectCoord
	IF DOES_ENTITY_EXIST(thisPlayer.oTennisRacket)
		PROCESS_ENTITY_ATTACHMENTS(thisPlayer.oTennisRacket)
		VECTOR vx,vy,vz
		GET_ENTITY_MATRIX(thisPlayer.oTennisRacket, vy, vx, vz, vObjectCoord)
		vObjectCoord += vz*0.4
	ENDIF
	return vObjectCoord
ENDFUNC

FUNC BOOL VALIDATE_SWING_TRUNCATION(TENNIS_PLAYER &thisPlayer)	//, INT iSwing)
	IF thisPlayer.controlType = COMPUTER
		RETURN FALSE
	ENDIF
	
//	TENNIS_SWING_STATES eSwing = INT_TO_ENUM(TENNIS_SWING_STATES, iSwing)
//	
//	IF eSwing = TSS_LUNGE_BACKHAND_HI
//		RETURN FALSE
//	ENDIF
//	IF eSwing = TSS_LUNGE_BACKHAND_LOW
//		RETURN FALSE
//	ENDIF
//	IF eSwing = TSS_LUNGE_BACKHAND_MID
//		RETURN FALSE
//	ENDIF
//	IF eSwing = TSS_LUNGE_FOREHAND_HI
//		RETURN FALSE
//	ENDIF
//	IF eSwing = TSS_LUNGE_FOREHAND_LOW
//		RETURN FALSE
//	ENDIF
//	IF eSwing = TSS_LUNGE_FOREHAND_MID
//		RETURN FALSE
//	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_TENNIS_BALL_TO_PLAYER_IN_FRAMES(TENNIS_PLAYER &thisPlayer, VECTOR &vFuturePoints[])
	INT p=0
	WHILE p < COUNT_OF(vFuturePoints) - 1
		FLOAT fDot = DOT_PRODUCT(thisPlayer.vMyForward, vFuturePoints[p] - thisPlayer.vPos)
		IF fDot < 0
			CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_BALL_TO_PLAYER_IN_FRAMES called, p=", p)
			RETURN p
		ENDIF
		p++
	ENDWHILE
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisPlayer - 
///    vSwingStateOffsets - []
///    phasesArray - []
///    vFuturePoints - []
///    vIdealNavPointOut - 
///    iFramesTrunc - 
///    vBestBallOut - Used for the NO_FRILLS version of this function.
/// RETURNS:
///    
FUNC TENNIS_SWING_STATES GET_TENNIS_SWING_STATE_TO_PLAY(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, VECTOR &vSwingStateOffsets[], TENNIS_SWING_PHASES &phasesArray[], VECTOR &vFuturePoints[], VECTOR &vIdealNavPointOut, INT &iFramesTrunc, VECTOR &vBestBallOut)
	INT iBestSwing = ENUM_TO_INT(TSS_NO_SWING)
	FLOAT fLowestCost = SWING_BASE_LOWEST_COST	//TODO: bump this on a flag so we can eliminate NO_FRILLS logic duplication
	FLOAT fLowestWorstCase = 100
	INT iFramesAhead
	VECTOR vAdjustedOffset
	VECTOR vBestBallLocation
	#IF IS_DEBUG_BUILD
		VECTOR vBestBatLocation
		FLOAT fLowestCostDebug = 100.0
	#ENDIF
	
	FLOAT fTValue
	GET_LINE_PLANE_INTERSECTION(vFuturePoints[0], vFuturePoints[1], thisPlayer.vPos, thisPlayer.vMyForward, fTValue)
	VECTOR vAdd = (vFuturePoints[1] - vFuturePoints[0]) * fTValue
	VECTOR vBallAtPlayer = vFuturePoints[0] + vAdd
	VECTOR vPlayerToBall = vBallAtPlayer - thisPlayer.vPos
	FLOAT fDotPlayerToBall = DOT_PRODUCT(vPlayerToBall, thisPlayer.vMyRight)
	INT iLowBucket = -1
	INT iHighBucket = -1
	INT iBucket
	REPEAT MAX_SWING_BUCKETS iBucket
		IF iLowBucket = -1 AND (fDotPlayerToBall - SWING_BUCKET_STEP_SIZE) <= thisProps.swingBucketSteps[iBucket+1]
			iLowBucket = iBucket
		ENDIF
		IF iHighBucket = -1 AND (fDotPlayerToBall + SWING_BUCKET_STEP_SIZE) <= thisProps.swingBucketSteps[iBucket+1]
			iHighBucket = iBucket
		ENDIF
	ENDREPEAT
	IF iLowBucket = -1	iLowBucket = PICK_INT(fDotPlayerToBall > 0, (MAX_SWING_BUCKETS - 1), 0)	ENDIF
	IF iHighBucket = -1	iHighBucket = PICK_INT(fDotPlayerToBall > 0, (MAX_SWING_BUCKETS - 1), 0)	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF TENNIS_DEBUG_RECORDING
//		CPRINTLN(DEBUG_TENNIS, "[", GET_FRAME_COUNT(), "] fTValue=", fTValue, ", fDotPlayerToBall=", fDotPlayerToBall, ", iLowBucket=", iLowBucket, ", iHighBucket=", iHighBucket)
		TEXT_LABEL_31 texBucket = "Bucket_"
		texBucket += iLowBucket
		texBucket += "->"
		texBucket += iHighBucket
		VECTOR vDraw = thisPlayer.vPos + vPlayerToBall
		vDraw.z = thisPlayer.vPos.z
		DEBUG_RECORD_SPHERE(texBucket, vDraw, 0.1, (<<255, 255, 255>>))
		texBucket = "BucketRight_"
		texBucket += ROUND(fDotPlayerToBall * 100)
		texBucket += "cm"
		DEBUG_RECORD_LINE(texBucket, thisPlayer.vPos, thisPlayer.vPos + (thisPlayer.vMyRight * fDotPlayerToBall), (<<255, 255, 255>>), (<<255, 255, 255>>))
	#ENDIF
	#ENDIF
	
	INT iFuturePreview
	VECTOR vPlayerPos = thisPlayer.vPos
	VECTOR vBallLocOut
	FOR iFuturePreview = 0 TO MAX_FRAMES_IN_THE_FUTURE
//		WHILE p < ENUM_TO_INT(TSS_MAX_SWINGS) - 1
		FOR iBucket = iLowBucket TO iHighBucket
			INT iSwingInBucket = 0
			WHILE iSwingInBucket < MAX_SWINGS_PER_BUCKET
				INT p = ENUM_TO_INT(thisProps.swingBuckets[iBucket][iSwingInBucket])
				iSwingInBucket++
				IF 	phasesArray[p].fActive >= 0	// Account for uninitialized entries in the array
					
					FLOAT fTimeToSweetSpot = ((phasesArray[p].fActive - phasesArray[p].fInit) * (phasesArray[p].iFrameTot) * THIRTY_FPS) 
					FLOAT fPreciseIndex = (fTimeToSweetSpot - thisProps.tennisBall.fPredTimeRemainder) / THIRTY_FPS
					INT iLowerIndex = FLOOR(fPreciseIndex)
					FLOAT fAlpha = fPreciseIndex - TO_FLOAT(iLowerIndex)
					// iFramesAhead is the time it takes for the bat to get to the sweet spot, plus our frames in the future check, plus a buffer we can change
					iFramesAhead = iFuturePreview + iLowerIndex + FRAME_TASK_BUFFER
					
					//Make sure we're not predicting too far in the future
					IF iFramesAhead < COUNT_OF(vFuturePoints) - 2 AND iFramesAhead >= 0
					
						//Dives blend many animations and need special treatment. One thing is we don't bother checking them in the future
						IF 	(iFuturePreview = 0) AND ((p = ENUM_TO_INT(TSS_DIVE_BACKHAND)) OR (p = ENUM_TO_INT(TSS_DIVE_FOREHAND)))
							TENNIS_DIVE_DiveType diveType
							FLOAT fVertBlend = -1, fHorBlend = -1
							IF (p = ENUM_TO_INT(TSS_DIVE_BACKHAND))
								diveType = GET_TENNIS_DIVE_BLEND_TO_PLAY(thisPlayer, vFuturePoints[8], vFuturePoints[9], DIVE_Left_TopLeft, DIVE_Left_BottomRight, DIVE_Left_BottomLeft, FALSE, fVertBlend, fHorBlend, vBallLocOut)
							ELSE
								diveType = GET_TENNIS_DIVE_BLEND_TO_PLAY(thisPlayer, vFuturePoints[8], vFuturePoints[9], DIVE_Right_TopLeft, DIVE_Right_BottomRight, DIVE_Right_BottomLeft, TRUE, fVertBlend, fHorBlend, vBallLocOut)
							ENDIF
							
							//Need to calculate the closest point in the dive area to the ball
							IF diveType <> TENNIS_DIVE_NoDive
								//Cost is
								FLOAT fCost = GET_SHOT_EXTRA_COST(INT_TO_ENUM(TENNIS_SWING_STATES, p))
								IF fCost < fLowestCost
									fLowestCost = fCost
									iBestSwing = p
									SET_TENNIS_PLAYER_DIVE_BLEND_VERTICAL(thisPlayer, fVertBlend)
									SET_TENNIS_PLAYER_DIVE_BLEND_HORIZONTAL(thisPlayer, fHorBlend)
									SET_TENNIS_PLAYER_DIVE_SELECTION(thisPlayer, diveType)
									
									vBestBallLocation = vBallLocOut
									//Also need to store off dive
									#IF IS_DEBUG_BUILD
										vBestBatLocation = vBestBallLocation
									#ENDIF
								ENDIF
							ENDIF
						ELIF (p <> ENUM_TO_INT(TSS_DIVE_BACKHAND) AND p <> ENUM_TO_INT(TSS_DIVE_FOREHAND))	//Not a dive
							vAdjustedOffset = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, vSwingStateOffsets[p])
							VECTOR vBatPointToTest = vPlayerPos + vAdjustedOffset
							
							VECTOR vBallPosToTest = LERP_VECTOR(vFuturePoints[iFramesAhead], vFuturePoints[iFramesAhead+1], fAlpha)
							
							// This would be a better nav point for this offset
							VECTOR tmpNavPoint = vBallPosToTest - vAdjustedOffset
							tmpNavPoint.z = thisPlayer.vPos.z
							
							// vDelta is the vector from the ballPos to the batPos
							VECTOR vDelta = vBatPointToTest - vBallPosToTest
							FLOAT fDotUpCourt = DOT_PRODUCT(vDelta, thisPlayer.vMyForward)
							FLOAT fDotBallInFront = DOT_PRODUCT(thisPlayer.vMyForward, GET_TENNIS_BALL_POS(thisProps.tennisBall) - thisPlayer.vPos)		// Distance of the ball in front of the player
							// Runs the simulation back until the ball is in front of the ball and tracks how many frames we'd be truncating
							INT iRewindCounter = 0
							WHILE fDotUpCourt > 0 AND iFramesAhead - iRewindCounter > 2	AND VALIDATE_SWING_TRUNCATION(thisPlayer) AND fDotBallInFront > 0
								iRewindCounter++
								vBallPosToTest = GET_CLOSEST_POINT_ON_LINE(vBatPointToTest, vFuturePoints[iFramesAhead - iRewindCounter], vFuturePoints[iFramesAhead - iRewindCounter - 1], TRUE)
								vDelta = vBatPointToTest - vBallPosToTest
								fDotUpCourt = DOT_PRODUCT(vDelta, thisPlayer.vMyForward)
							ENDWHILE
							
							IF (ABSF(fDotUpCourt) > UPCOURT_ERROR_REDUCTION)
								fDotUpCourt *= UPCOURT_ERROR_REDUCTION / ABSF(fDotUpCourt)
							ENDIF
							
							vDelta -= thisPlayer.vMyForward * fDotUpCourt
							FLOAT fDeltaMag = VMAG(vDelta)
							
							#IF IS_DEBUG_BUILD
								#IF TENNIS_DEBUG_RECORDING
								#IF IS_DEBUG_BUILD
								TEXT_LABEL_63 texBallAndFrame = GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, p))
								texBallAndFrame += iFramesAhead
								IF RECORD_HEAPS_OF_DATA
									DEBUG_RECORD_SPHERE(GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, p)), vBatPointToTest, 0.1, (<<0,255,0>>))
									DEBUG_RECORD_SPHERE(texBallAndFrame, vBallPosToTest, 0.1, (<<0,0,255>>))
									TEXT_LABEL_31 texSwingToBall = "SwingToBall_~"
									texSwingToBall += ROUND(fDeltaMag * 100)
									DEBUG_RECORD_LINE(texSwingToBall, vBatPointToTest, vBallPosToTest, (<<0,255,0>>), (<<0,0,255>>))
								ENDIF
								#ENDIF
								#ENDIF
							#ENDIF

							//Shots near the centre of the racket are considered equal but weighted on "Cost"
							IF fDeltaMag < GOOD_SHOT_ALLOWANCE
								fDeltaMag = 0
							ENDIF
							
							//Add in the cost of the shot
							fDeltaMag += GET_SHOT_EXTRA_COST(INT_TO_ENUM(TENNIS_SWING_STATES, p))
			
							//Bias against shots on the outside of the racket that will miss
							IF fDeltaMag < fLowestCost
								// vDelta is the vector from the ball to the bat
								VECTOR vBallToPed = thisPlayer.vPos - vBallPosToTest
								FLOAT fDotBallBetweenRaq = DOT_PRODUCT(vDelta, vBallToPed)
								IF fDotBallBetweenRaq > 0
									//Ball is beyond centre of bat, is it outside the racquet?
									IF fDeltaMag > RACKET_RADIUS_FOR_CONNECTION
										FLOAT fOutsideWeight = BIAS_AGAINST_OUTSIDE_SHOT * (fDeltaMag - RACKET_RADIUS_FOR_CONNECTION)
										#IF IS_DEBUG_BUILD
										#IF TENNIS_DEBUG_RECORDING
										TEXT_LABEL_63 texOSW = "O.S.W."
										texOSW += ROUND(fOutsideWeight * 100)
										texOSW += GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, p))
										DEBUG_RECORD_LINE(texOSW, vBatPointToTest, vBatPointToTest + thisPlayer.vMyRight*fOutsideWeight, <<0,0,0>>, <<1,0,0>>)									
										#ENDIF
										#ENDIF
										fDeltaMag += fOutsideWeight
									ENDIF
								ENDIF
							ENDIF
							
							IF fDeltaMag < fLowestWorstCase
								fLowestWorstCase = fDeltaMag
								vBestBallLocation = vBallPosToTest
							ENDIF
							#IF IS_DEBUG_BUILD
								IF fDeltaMag < fLowestCostDebug
									fLowestCostDebug = fDeltaMag
									vBestBatLocation = vBatPointToTest
								ENDIF
							#ENDIF
							
							//If this is still considered a good swing then record it as the best one yet
							IF fDeltaMag < fLowestCost
								//Unless this is the future!
								IF iFuturePreview > 0
									#IF IS_DEBUG_BUILD 
									#IF TENNIS_DEBUG_RECORDING
									DEBUG_RECORD_SPHERE("BatLocationForFutureShot", vBatPointToTest, 0.1, <<1.0,0.5,0.25>>)
									DEBUG_RECORD_SPHERE("BallLocationForFutureShot", vBallPosToTest, 0.1, <<0.5,0.5,0.75>>)
									DEBUG_RECORD_SPHERE("ReplacedBatLocation", vBestBallLocation, 0.05, <<1.0,0.5,0.25>>)
									DEBUG_RECORD_SPHERE("ReplacedBallLocation", vBestBallLocation, 0.05, <<0.5,0.5,0.75>>)
									VECTOR vShouldBeHere = vBestBallLocation - vAdjustedOffset
									vShouldBeHere.z = vPlayerPos.z
									DEBUG_RECORD_SPHERE("BestShotLocation", vShouldBeHere, 0.02, <<0.0,0.0,0.2>>)
									#ENDIF
									#ENDIF
								ENDIF
								fLowestCost = fDeltaMag
								iBestSwing = p
								vIdealNavPointOut = tmpNavPoint
								iFramesTrunc = iRewindCounter - iFuturePreview	// Rewind counter moves frames up, iFuturePreview moves them back.
								vBestBallLocation = vBallPosToTest
								CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_SWING_STATE_TO_PLAY :: ", GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, p)), " and fLowestCost is ", fLowestCost, ", iRewindCounter=", iRewindCounter, ", iFuturePreview=", iFuturePreview)
								#IF IS_DEBUG_BUILD
								#IF TENNIS_DEBUG_RECORDING
									TEXT_LABEL_63 texName = GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, p))
									texName += "_"
									texName += ROUND(1/GET_FRAME_TIME())
									texName += "fps"
									DEBUG_RECORD_SPHERE(texName, vBatPointToTest, 0.1, (<<0,255,0>>))
									DEBUG_RECORD_SPHERE(texBallAndFrame, vBallPosToTest, 0.1, (<<255,255,0>>))
									TEXT_LABEL_31 texSwingToBall = "SwingToBall_"
									texSwingToBall += ROUND(fDeltaMag * 100)
									DEBUG_RECORD_LINE(texSwingToBall, vBatPointToTest, vBallPosToTest, (<<0,255,0>>), (<<255,255,0>>))
									vBestBatLocation = vBatPointToTest
									TEXT_LABEL_31 texIdealNav = "vIdealNav_"
									texIdealNav += ROUND(fDeltaMag * 100)
									DEBUG_RECORD_SPHERE(texIdealNav, vIdealNavPointOut, 0.1, (<<255,0,0>>))
								#ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDWHILE
		ENDFOR
		
		vBestBallOut = vBestBallLocation
		
		// Don't need to look ahead if we're seeing a great shot!
		IF fLowestCost <= SWING_DAMN_IT_THRESHOLD
			RETURN INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing)
		ENDIF
		
		IF INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing) <> TSS_NO_SWING
			RETURN INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing)
		ENDIF
		
		IF thisPlayer.controlType = COMPUTER
			RETURN TSS_NO_SWING
		ENDIF
		
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	vBestBatLocation = vBestBatLocation
	#IF TENNIS_DEBUG_RECORDING
	DEBUG_RECORD_SPHERE("BestBallLocationForSwing", vBestBallLocation, 0.1, <<0.8,0.8,0.8>>)
	DEBUG_RECORD_SPHERE("BatLocationForBestSwing", vBestBatLocation, 0.1, <<0.2,0.2,0.2>>)
	DEBUG_RECORD_SPHERE("IdealNavPointForBestSwing", vIdealNavPointOut, 0.1, <<0.2,0.2,0.2>>)
//	DEBUG_RECORD_ENTITY_STRING(thisPlayer.pIndex, "ShotSelected", GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing)))
//	DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "NumFramesToConnection", TO_FLOAT(iNumFrameToBestShotHits))
//	DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "ShotError", fLowestCost)
//	DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "ReducedUpCourtError", fSelectedReductionInError)
	#ENDIF
	#ENDIF
	
	RETURN INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing)
ENDFUNC

/// PURPOSE:
///    Return any swing that looks like it's going for the ball. Accuracy isn't needed if we're using this function.
/// PARAMS:
///    thisPlayer - 
///    vSwingStateOffsets - []
///    phasesArray - []
///    vNaught - Base of the line we're testing
///    vEnd - endpoint of the line we're testing
/// RETURNS:
///    The swing which is closest to its sweet spot along the predicted path of the ball
FUNC TENNIS_SWING_STATES GET_TENNIS_SWING_STATE_TO_PLAY_NO_FRILLS(TENNIS_PLAYER &thisPlayer, VECTOR &vSwingStateOffsets[], TENNIS_SWING_PHASES &phasesArray[], VECTOR &vNaught, VECTOR &vEnd)
	INT iBestSwing = ENUM_TO_INT(TSS_NO_SWING)
	FLOAT fLowestCost = 1000.0
	FLOAT tValue
	VECTOR vAdjustedOffset
	VECTOR vBatPointToTest
	#IF IS_DEBUG_BUILD
	VECTOR vChosen
	vChosen = vChosen
	#ENDIF
	
	GET_LINE_PLANE_INTERSECTION(vNaught, vEnd, thisPlayer.vPos, thisPlayer.vMyForward, tValue)
	CPRINTLN(DEBUG_TENNIS, "tValue from intersection: ", tValue)
	
	FLOAT fNoSwingThreshold = PICK_FLOAT(thisPlayer.controlType <> COMPUTER, WILD_SWING_MAX_DEPTH, WILD_SWING_AI_DIST_THRESHOLD)
	IF tValue <= fNoSwingThreshold
		CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_SWING_STATE_TO_PLAY_NO_FRILLS :: Ball behind player, choosing TSS_NO_SWING")
		RETURN TSS_NO_SWING
	ENDIF
	
	VECTOR vSegment = (vEnd - vNaught) * tValue
	VECTOR vIntersect = vNaught + vSegment
	vIntersect.z = vNaught.z	// Level the point with the ball to prevent exceedingly high shots
	
	FLOAT fHeight = vIntersect.z - thisPlayer.vPos.z
	IF fHeight > WILD_SWING_HEIGHT_THRESHOLD
		CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_SWING_STATE_TO_PLAY_NO_FRILLS :: Ball above player, choosing TSS_NO_SWING")
		RETURN TSS_NO_SWING
	ENDIF
	CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_SWING_STATE_TO_PLAY_NO_FRILLS :: fHeight=", fHeight)
	
	#IF TENNIS_DEBUG_RECORDING
	#IF IS_DEBUG_BUILD
	DEBUG_RECORD_SPHERE("TargetedPos", vIntersect, 0.1, (<<1,0.5,0>>))
	#ENDIF
	#ENDIF
	
	
	INT p = 0
	WHILE p < ENUM_TO_INT(TSS_MAX_SWINGS) - 1
		IF phasesArray[p].fActive >= 0	// Account for uninitialized entries in the array
		AND INT_TO_ENUM(TENNIS_SWING_STATES, p) <> TSS_DIVE_BACKHAND
		AND INT_TO_ENUM(TENNIS_SWING_STATES, p) <> TSS_DIVE_FOREHAND
			vAdjustedOffset = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, vSwingStateOffsets[p])

			vBatPointToTest = thisPlayer.vPos + vAdjustedOffset
			
			#IF TENNIS_DEBUG_RECORDING
			#IF IS_DEBUG_BUILD
			DEBUG_RECORD_SPHERE(GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, p)), vBatPointToTest, 0.1, (<<1,1,0>>))
			DEBUG_RECORD_LINE("RacketToBall", vBatPointToTest, vIntersect, (<<1,1,0>>), (<<1,0.5,0>>))
			#ENDIF
			#ENDIF
			
			// vDelta is the vector from the ballPos to the batPos
			FLOAT fDeltaMag = VDIST2(vBatPointToTest, vIntersect)
			
			//If this is still considered a good swing then record it as the best one yet
			IF fDeltaMag < fLowestCost
				fLowestCost = fDeltaMag
				iBestSwing = p
				#IF IS_DEBUG_BUILD
				vChosen = vBatPointToTest
				#ENDIF
				CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_SWING_STATE_TO_PLAY_NO_FRILLS :: Setting iBestSwing to ", GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing)), " and fLowestCost is ", fLowestCost)
			ENDIF
		ENDIF
		p++
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
	#IF TENNIS_DEBUG_RECORDING
	DEBUG_RECORD_SPHERE(GET_STRING_FROM_TENNIS_SWING_STATE(INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing)), vChosen, 0.15, (<<0,0,1>>))
	#ENDIF
	#ENDIF
	
	TENNIS_DIVE_DiveType diveType
	VECTOR v1, v2, vDummy
	FLOAT fVertBlend, fHorBlend
	v1 = vIntersect + thisPlayer.vMyForward
	v2 = vIntersect - thisPlayer.vMyForward
	IF INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing) = TSS_DIVE_BACKHAND
		CPRINTLN(DEBUG_TENNIS, "NO_FRILLS :: INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing) = TSS_DIVE_BACKHAND")
		diveType = GET_TENNIS_DIVE_BLEND_TO_PLAY(thisPlayer, v1, v2, DIVE_Left_TopLeft, DIVE_Left_BottomRight, DIVE_Left_BottomLeft, FALSE, fVertBlend, fHorBlend, vDummy)
		IF diveType = TENNIS_DIVE_NoDive
			SET_TENNIS_PLAYER_DIVE_BLEND_VERTICAL(thisPlayer, 0.5)
			SET_TENNIS_PLAYER_DIVE_BLEND_HORIZONTAL(thisPlayer, 1.0)
			SET_TENNIS_PLAYER_DIVE_SELECTION(thisPlayer, TENNIS_DIVE_DiveLeft_BottomRight)
			CPRINTLN(DEBUG_TENNIS, "NO_FRILLS :: diveType = FALSE, going with defaults")
		ELSE
			SET_TENNIS_PLAYER_DIVE_BLEND_VERTICAL(thisPlayer, fVertBlend)
			SET_TENNIS_PLAYER_DIVE_BLEND_HORIZONTAL(thisPlayer, fHorBlend)
			SET_TENNIS_PLAYER_DIVE_SELECTION(thisPlayer, diveType)
			iBestSwing = ENUM_TO_INT(TSS_NO_SWING)
			CPRINTLN(DEBUG_TENNIS, "NO_FRILLS :: iBestSwing = ENUM_TO_INT(TSS_NO_SWING)", diveType)
		ENDIF
	ELIF INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing) = TSS_DIVE_FOREHAND
		CPRINTLN(DEBUG_TENNIS, "NO_FRILLS :: INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing) = TSS_DIVE_FOREHAND")
		diveType = GET_TENNIS_DIVE_BLEND_TO_PLAY(thisPlayer, v1, v2, DIVE_Right_TopLeft, DIVE_Right_BottomRight, DIVE_Right_BottomLeft, TRUE, fVertBlend, fHorBlend, vDummy)
		IF diveType = TENNIS_DIVE_NoDive
			SET_TENNIS_PLAYER_DIVE_BLEND_VERTICAL(thisPlayer, 0.5)
			SET_TENNIS_PLAYER_DIVE_BLEND_HORIZONTAL(thisPlayer, 1.0)
			SET_TENNIS_PLAYER_DIVE_SELECTION(thisPlayer, TENNIS_DIVE_DiveLeft_BottomRight)
			CPRINTLN(DEBUG_TENNIS, "NO_FRILLS :: diveType = FALSE, going with defaults")
		ELSE
			SET_TENNIS_PLAYER_DIVE_BLEND_VERTICAL(thisPlayer, fVertBlend)
			SET_TENNIS_PLAYER_DIVE_BLEND_HORIZONTAL(thisPlayer, fHorBlend)
			SET_TENNIS_PLAYER_DIVE_SELECTION(thisPlayer, diveType)
			iBestSwing = ENUM_TO_INT(TSS_NO_SWING)
			CPRINTLN(DEBUG_TENNIS, "NO_FRILLS :: iBestSwing = ENUM_TO_INT(TSS_NO_SWING)", diveType)
		ENDIF
	ENDIF
	
	RETURN INT_TO_ENUM(TENNIS_SWING_STATES, iBestSwing)
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisPlayer - 
///    sNetworkName - 
PROC SET_UP_TENNIS_MOVE_NETWORK(TENNIS_PLAYER &thisPlayer, STRING sNetworkName, FLOAT fBlendTime)
	IF NOT (NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer)) AND IS_TASK_MOVE_NETWORK_ACTIVE(GET_TENNIS_PLAYER_INDEX(thisPlayer)) AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(GET_TENNIS_PLAYER_INDEX(thisPlayer)), "running"))
		CPRINTLN(DEBUG_TENNIS, "Setting player with eCourtSide ", thisPlayer.playerAI.texName, " to the network: ", sNetworkName)
//		STRING sAnimDict = PICK_STRING(IS_TENNIS_PED_MALE(thisPlayer), "mini@tennis", "mini@tennis@female")
		TASK_MOVE_NETWORK_BY_NAME(GET_TENNIS_PLAYER_INDEX(thisPlayer), sNetworkName, fBlendTime, FALSE, "mini@tennis", MOVE_USE_KINEMATIC_PHYSICS)
	ENDIF
ENDPROC

PROC HANDLE_TENNIS_FOOT_SQUEAKS(TENNIS_PLAYER &thisPlayer, INT x, INT y)
	// If it's not rain or drizzle then play some squeaks.
	IF GET_RAIN_LEVEL() <= 0 AND GET_SNOW_LEVEL() <= 0 AND GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_NO_SWING
		//This is all for the audio foot squeaks
		IF ABSF(TO_FLOAT(x)) >= ABSF(TO_FLOAT(y))
			IF x > DEAD_ZONE_THRESHOLD
				IF thisPlayer.running <> RUN_RIGHT AND GET_RANDOM_FLOAT_IN_RANGE() < SQUEAK_RATE_COD
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_FOOT_SQUEAKS_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
				ELIF GET_RANDOM_FLOAT_IN_RANGE() < SQUEAK_RATE_RUNNING
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_FOOT_SQUEAKS_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
				ENDIF
				thisPlayer.running = RUN_RIGHT
			ELIF x < -DEAD_ZONE_THRESHOLD
				IF thisPlayer.running <> RUN_LEFT AND GET_RANDOM_FLOAT_IN_RANGE() < SQUEAK_RATE_COD
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_FOOT_SQUEAKS_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
				ELIF GET_RANDOM_FLOAT_IN_RANGE() < SQUEAK_RATE_RUNNING
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_FOOT_SQUEAKS_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
				ENDIF
				thisPlayer.running = RUN_LEFT
			ELSE
				IF thisPlayer.running <> RUN_NEUTRAL AND GET_RANDOM_FLOAT_IN_RANGE() < SQUEAK_RATE_COD
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_FOOT_SQUEAKS_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
				ENDIF
				thisPlayer.running = RUN_NEUTRAL
			ENDIF
		ELIF ABSF(TO_FLOAT(x)) < ABSF(TO_FLOAT(y))
			IF y > DEAD_ZONE_THRESHOLD
				IF thisPlayer.running <> RUN_BACKWARD AND GET_RANDOM_FLOAT_IN_RANGE() < SQUEAK_RATE_COD
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_FOOT_SQUEAKS_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
				ELIF GET_RANDOM_FLOAT_IN_RANGE() < SQUEAK_RATE_RUNNING
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_FOOT_SQUEAKS_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
				ENDIF
				thisPlayer.running = RUN_BACKWARD
			ELIF y < -DEAD_ZONE_THRESHOLD
				IF thisPlayer.running <> RUN_FORWARD AND GET_RANDOM_FLOAT_IN_RANGE() < SQUEAK_RATE_COD
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_FOOT_SQUEAKS_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
				ELIF GET_RANDOM_FLOAT_IN_RANGE() < SQUEAK_RATE_RUNNING
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_FOOT_SQUEAKS_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
				ENDIF
				thisPlayer.running = RUN_FORWARD
			ELSE
				IF thisPlayer.running <> RUN_NEUTRAL AND GET_RANDOM_FLOAT_IN_RANGE() < SQUEAK_RATE_COD
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_FOOT_SQUEAKS_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
				ENDIF
				thisPlayer.running = RUN_NEUTRAL
			ENDIF
		ELSE
			SCRIPT_ASSERT("We missed a permutation for the player's change of direction, contact Rob Pearsall")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL VALIDATE_TENNIS_AI_TAKEOVER(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps)
	IF thisPlayer.controlType = COMPUTER
		RETURN FALSE
	ENDIF
	
	IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_WANTS_TO_SWING)
//		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_TENNIS_AI_TAKEOVER :: ", thisPlayer.playerAI.texName, " NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_WANTS_TO_SWING) fail")
		RETURN FALSE
	ENDIF
	
	IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_TENNIS_SWING_EVENT_FLAG)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_ALLOW_AI_ASSIST)
//		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_TENNIS_AI_TAKEOVER :: ", thisPlayer.playerAI.texName, " IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_ALLOW_AI_ASSIST) fail")
		RETURN FALSE
	ENDIF
	
	IF VDIST2(GET_AI_PREDICTION_POINT(thisPlayer.playerAI) + (<< 0,0,1 >>), thisPlayer.vPos) > (AI_TAKEOVER_DISTANCE * AI_TAKEOVER_DISTANCE)
		#IF IS_DEBUG_BUILD
		FLOAT fDist = VDIST(GET_AI_PREDICTION_POINT(thisPlayer.playerAI) + (<< 0,0,1 >>), thisPlayer.vPos)
		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_TENNIS_AI_TAKEOVER :: ", thisPlayer.playerAI.texName, " VDIST2 fail=", fDist)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	FLOAT fDot = DOT_PRODUCT(thisPlayer.vMyForward, GET_TENNIS_BALL_POS(thisProps.tennisBall) - thisPlayer.vPos)
	IF fDot < 0
		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_TENNIS_AI_TAKEOVER :: ", thisPlayer.playerAI.texName, " fDot Fail=", fDot)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Manages moving thisPlayer around the court
/// PARAMS:
///    thisPlayer - 
///    thisProps - 
///    bIsServing - use TRUE when thisPlayer is serving
PROC UPDATE_TENNIS_PLAYER_MOVER(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, SERVING_SIDE_ENUM eServingSide, TENNIS_PLAYER_ID eThisPedID, BOOL bIsServing=FALSE, BOOL bGamePaused=FALSE)
	IF thisPlayer.controlType <> HUMAN_NETWORK AND NOT bIsServing AND NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer)) AND NOT IS_PED_RAGDOLL(GET_TENNIS_PLAYER_INDEX(thisPlayer))
		INT x,y,dummy
		// Get the player into tennis mode if they aren't
		IF IS_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisPlayer)) AND NOT IS_TENNIS_PED_MALE(thisPlayer) AND NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_DROP_OUT_OF_TENNIS_MODE)
			SET_PED_WEAPON_MOVEMENT_CLIPSET(GET_TENNIS_PLAYER_INDEX(thisPlayer), "TENNIS_LOCOMOTION_FEMALE")
		ENDIF
		// Returns between 0 and 128
		GET_ANALOG_STICK_VALUES(x,y,dummy,dummy)
		// This is where we'll implement the pseudo-stamina
		#IF TENNIS_DEBUG_RECORDING
		#IF IS_DEBUG_BUILD
		IF thisProps.eTennisActivity <> TA_AI_VS_AI
			DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "PlayerXInput", TO_FLOAT(x))
			DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "PlayerYInput", TO_FLOAT(y))
		ENDIF
		#ENDIF
		#ENDIF
		FLOAT fAbsX = ABSF(TO_FLOAT(x))
		IF (fAbsX > 0 OR ABSF(TO_FLOAT(y)) > 0) AND thisPlayer.controlType <> COMPUTER
			SET_PED_MIN_MOVE_BLEND_RATIO(GET_TENNIS_PLAYER_INDEX(thisPlayer), PEDMOVEBLENDRATIO_RUN)
		ENDIF
		IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) <> TSS_SERVE_TOSS AND thisPlayer.controlType <> COMPUTER AND fAbsX > DEAD_ZONE_THRESHOLD
			SET_TENNIS_PLAYER_DIRECTION_HELD_TIMER(thisPlayer, GET_TENNIS_PLAYER_DIRECTION_HELD_TIMER(thisPlayer) + GET_FRAME_TIME())
		ELSE
			SET_TENNIS_PLAYER_DIRECTION_HELD_TIMER(thisPlayer, 0)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF USE_WALK_MOVE_NETWORK AND thisPlayer.controlType <> COMPUTER
				SET_PED_MIN_MOVE_BLEND_RATIO(GET_TENNIS_PLAYER_INDEX(thisPlayer), PEDMOVEBLENDRATIO_WALK)
			ENDIF
		#ENDIF
		
		IF NOT CAN_TENNIS_PLAYER_USE_STRONG_SWING(thisPlayer)
			UPDATE_TENNIS_PLAYER_STRONG_SWING_COOLDOWN(thisPlayer)
		ENDIF
		// Checks that the player wants to swing and is near to their prediction point
		BOOL bAITakingOver = VALIDATE_TENNIS_AI_TAKEOVER(thisPlayer, thisProps)
		
		#IF IS_DEBUG_BUILD
		#IF TENNIS_DEBUG_RECORDING
		IF bAITakingOver AND thisProps.eTennisActivity <> TA_AI_VS_AI
			DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "bAITakingOver", 1.0)
		ELIF thisProps.eTennisActivity <> TA_AI_VS_AI
			DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "bAITakingOver", 0.0)
		ENDIF
		#ENDIF
		#ENDIF
		IF thisPlayer.controlType <> COMPUTER
		AND NOT bAITakingOver
			// Returns between 0 and 128
			GET_ANALOG_STICK_VALUES(x,y,dummy,dummy)
			#IF NOT IS_TENNIS_MULTIPLAYER
				ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, TRUE)
			#ENDIF
			//CPRINTLN(DEBUG_TENNIS, x, " ", y)
		ELIF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_DIVE_ROLL_FINISHING)
			GET_AI_STICK_FORCE(thisPlayer,x,y, thisProps.tennisPlayers[1 - ENUM_TO_INT(eThisPedID)], thisProps.tennisBall)
			//The GET_AI_STICK_FORCE call above flips this flag if the player has been just tasked with something
			IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_JUST_TASKED) AND IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_AI_HAS_SWUNG)
				SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, TSS_NO_SWING)
				CPRINTLN(DEBUG_TENNIS, thisPlayer.playerAI.texName, "'s AI has been tasked and has swung, setting to TSS_NO_SWING")
			ENDIF
		ENDIF
		
		IF thisProps.eTennisActivity <> TA_AI_VS_AI
			HANDLE_TENNIS_FOOT_SQUEAKS(thisPlayer, x, y)
		ENDIF
		FLOAT fDotForward = DOT_PRODUCT(thisPlayer.vMyForward, thisProps.tennisBall.vBallVel)
		IF IS_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisPlayer)) AND fDotForward < 0
			// Calculate the direction of the ball from the player and pass that to the MoVE network
			FLOAT fDotSide = DOT_PRODUCT(thisPlayer.vMyRight, GET_TENNIS_BALL_POS(thisProps.tennisBall) - thisPlayer.vPos)
			SET_TENNIS_MOVE_NETWORK_SIGNAL_FLOAT(GET_TENNIS_PLAYER_INDEX(thisPlayer), "ForcedStopDirection", fDotSide)
//			CDEBUG3LN(DEBUG_TENNIS, "SET_TENNIS_MOVE_NETWORK_SIGNAL_FLOAT :: ForcedStopDirection, fDotSide=", fDotSide, ", fDotForward=", fDotForward)
		ENDIF
	
	// Player Must be serving. Or a ragdoll. But probably serving.
	ELIF thisPlayer.controlType <> HUMAN_NETWORK
		FLOAT dummy, x
		INT iEx, iDummy
		IF thisPlayer.controlType <> COMPUTER
			GET_ANALOG_STICK_VALUES_MOVE_INPUT(x,dummy,dummy,dummy)
		ELSE
			GET_AI_STICK_FORCE(thisPlayer,iEx,iDummy, thisProps.tennisPlayers[1 - ENUM_TO_INT(eThisPedID)], thisProps.tennisBall)
		ENDIF
		
		IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer)) AND IS_TASK_MOVE_NETWORK_ACTIVE(GET_TENNIS_PLAYER_INDEX(thisPlayer))
			//If we're in the intro, request that we go into the loop
			IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(GET_TENNIS_PLAYER_INDEX(thisPlayer)), "Intro") AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(GET_TENNIS_PLAYER_INDEX(thisPlayer))
				REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(GET_TENNIS_PLAYER_INDEX(thisPlayer), "running")
				CPRINTLN(DEBUG_TENNIS, thisPlayer.playerAI.texName, " requested running state in TennisServeSetMoVE network")
			ENDIF
			
			VECTOR vAnchor
			FLOAT fDistMax, fDistMin
			//start setting the bounds for how far the player can walk during the pre-serve
			IF thisPlayer.eCourtSide = (TENNIS_PLAYER_AWAY) AND eServingSide = SERVING_FROM_RIGHT
				vAnchor = thisProps.tennisCourt.vCourtCorners[1]
				fDistMax = SERVE_WALK_MID - SERVE_WALK_MOD_RIGHT
				fDistMin = SERVE_WALK_MIN
			ELIF thisPlayer.eCourtSide = (TENNIS_PLAYER_AWAY) AND eServingSide = SERVING_FROM_LEFT
				vAnchor = thisProps.tennisCourt.vCourtCorners[1]
				fDistMax = SERVE_WALK_MAX
				fDistMin = SERVE_WALK_MID + (SERVE_WALK_MOD_LEFT)
			ELIF thisPlayer.eCourtSide <> (TENNIS_PLAYER_AWAY) AND eServingSide = SERVING_FROM_RIGHT
				vAnchor = thisProps.tennisCourt.vCourtCorners[3]
				fDistMax = SERVE_WALK_MID - SERVE_WALK_MOD_RIGHT
				fDistMin = SERVE_WALK_MIN
			ELIF thisPlayer.eCourtSide <> (TENNIS_PLAYER_AWAY) AND eServingSide = SERVING_FROM_LEFT
				vAnchor = thisProps.tennisCourt.vCourtCorners[3]
				fDistMax = SERVE_WALK_MAX
				fDistMin = SERVE_WALK_MID + (SERVE_WALK_MOD_LEFT)
			ELSE
				SCRIPT_ASSERT("UPDATE_TENNIS_PLAYER_MOVER missed a permutation of forecourt and serving side")
			ENDIF
			
//			DRAW_DEBUG_SPHERE(vAnchor, fDistMax, 100, 100, 100, 100)
//			DRAW_DEBUG_SPHERE(vAnchor, fDistMin, 200, 200, 200, 200)
			
			// The following set of IFs is to make sure that the player doesn't move too far to the left or right and determines the blend ratio
			// Walk Right
			IF (x > 0.75) AND (VDIST2(thisPlayer.vPos, vAnchor) > fDistMin * fDistMin)
			OR (x < 0.25) AND (VDIST2(thisPlayer.vPos, vAnchor) < fDistMax * fDistMax)
				thisPlayer.fServeBlend = x
			ELIF (x >= 0.25 AND x <= 0.75)
			OR (x > 0.75 AND VDIST2(thisPlayer.vPos, vAnchor) < fDistMin * fDistMin)
			OR (x < 0.25) AND (VDIST2(thisPlayer.vPos, vAnchor) > fDistMax * fDistMax)
			OR bGamePaused
				thisPlayer.fServeBlend = 0.5
			ENDIF
			
			IF GET_TASK_MOVE_NETWORK_EVENT(GET_TENNIS_PLAYER_INDEX(thisPlayer), "IdleStarted")
				FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE()
				thisProps.texIdleEvent = PICK_STRING(fRand < 0.33, "Idle3", PICK_STRING(fRand < 0.66, "Idle4", "Idle1"))
			ENDIF
			SET_TASK_MOVE_NETWORK_SIGNAL_BOOL(GET_TENNIS_PLAYER_INDEX(thisPlayer), thisProps.texIdleEvent, TRUE)
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(GET_TENNIS_PLAYER_INDEX(thisPlayer), "Speed", thisPlayer.fServeBlend)
//			CDEBUG3LN(DEBUG_TENNIS, "UPDATE_TENNIS_PLAYER_MOVER, sending bool signal=", thisProps.texIdleEvent, ", GET_FRAME_COUNT=", GET_FRAME_COUNT())
		ENDIF
	ENDIF
	
	IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_NO_SWING 
	AND DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) 
	AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(thisProps.tennisBall.oBall)
	AND VDIST2(thisPlayer.vPos, GET_TENNIS_BALL_POS(thisProps.tennisBall)) < PLAYER_HIT_BY_BALL_RADIUS * PLAYER_HIT_BY_BALL_RADIUS
		CPRINTLN(DEBUG_TENNIS, "Ball collided with ", thisPlayer.playerAI.texName)
		SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_HIT_BY_BALL)
		SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_HIT_PLAYER)
	ENDIF
ENDPROC


/// PURPOSE:
///    Checks if two vectors are almost equal / are within fTolerance of each other. 
/// PARAMS:
///    v1 - Vector 1
///    v2 - Vector 2
///    fTolerance - Tolerance factor.
/// RETURNS:
///    TRUE if v1 is within fTolerance of v2.
FUNC BOOL ARE_VECTORS_ALMOST_EQUAL_2D(VECTOR v1, VECTOR v2, FLOAT fTolerance = 0.5  )
	
	IF fTolerance < 0
		fTolerance = 0
	ENDIF
	
	IF ABSF(v1.x - v2.x) <= fTolerance
		IF ABSF(v1.y - v2.y) <= fTolerance
			RETURN TRUE
		ENDIF		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    tells us whether or not to use TASK_GO_STRAIGHT_TO_COORD over FOLLOW_NAV_MESH
/// PARAMS:
///    thisProps - 
///    thisPlayer - 
/// RETURNS:
///    
FUNC BOOL VALIDATE_TENNIS_SIDE_CHANGE_USES_STRAIGHT_TO_COORD(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER &thisPlayer)
	// Check the court we're on
	IF GET_TENNIS_PLAYING_COURT(thisProps.tennisCourt) <> TENNIS_COURT_VinewoodHotel1
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_SIDE_CHANGE_USES_STRAIGHT_TO_COORD :: ", thisPlayer.playerAI.texName, " !TENNIS_COURT_VinewoodHotel1")
		RETURN FALSE
	ENDIF
	// Check if the player is on the correct side of the court
	IF thisPlayer.eCourtSide <> TENNIS_PLAYER_HOME
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_SIDE_CHANGE_USES_STRAIGHT_TO_COORD :: ", thisPlayer.playerAI.texName, " !TENNIS_PLAYER_HOME")
		RETURN FALSE
	ENDIF
	// Passed all checks, return TRUE
	CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_TENNIS_SIDE_CHANGE_USES_STRAIGHT_TO_COORD :: ", thisPlayer.playerAI.texName, " returning TRUE")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisPlayer - 
///    thisProps - 
///    eServingSide - ENUM_TO_INT of the tennis.sc enum, SERVING_SIDE_ENUM
///    eSelfID - ID of the player being set to receive the serve. Index of thisPlayer in the player array.
///    bForcePedUpdate - Used when setting the player in position and they are off camera
///    bWalkToPos - 
///    bSideChange - If walking back this will prevent the possible delay of the sequence
///    bAddDelay - this is also contingent on bSideChange being false, adds a random delay to the walk back task sequence
PROC SET_TENNIS_PLAYER_RECEIVE_SERVE(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, SERVING_SIDE_ENUM eServingSide, TENNIS_PLAYER_ID eSelfID, BOOL bForcePedUpdate = TRUE, BOOL bWalkToPos = FALSE, BOOL bSideChange = FALSE, BOOL bAddDelay=FALSE, BOOL bSkipToIdle=FALSE)
	CPRINTLN(DEBUG_TENNIS, "prep_next_rally :: ", thisPlayer.playerAI.texName, " set to receive serve")
	IF thisPlayer.controlType <> COMPUTER AND NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer)) AND NOT bWalkToPos
		ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, TRUE)
	ENDIF
	IF NOT bWalkToPos
		CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_MOVE_ENABLED)
	ENDIF
	CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
	CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_AI_HAS_SWUNG)
	
	SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, TSS_NO_SWING)
	
	IF thisPlayer.controlType = COMPUTER AND NOT bWalkToPos AND thisProps.eTennisActivity <> TA_AI_VS_AI
		SET_TENNIS_AI_STATE(thisPlayer.playerAI, ASE_AI_IDLE)
		SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisPlayer), TRUE)
	ENDIF
	SWITCH eServingSide
		CASE SERVING_FROM_LEFT
			IF thisPlayer.controlType = COMPUTER
				IF thisPlayer.eCourtSide <> (TENNIS_PLAYER_AWAY)
					SET_AI_PREDICTION_POINT(thisPlayer.playerAI, thisProps.tennisCourt.vCourtCorners[2])
				ELSE
					SET_AI_PREDICTION_POINT(thisPlayer.playerAI, thisProps.tennisCourt.vCourtCorners[0])
				ENDIF
			ENDIF
		BREAK
		CASE SERVING_FROM_RIGHT
			IF thisPlayer.controlType = COMPUTER
				IF thisPlayer.eCourtSide <> (TENNIS_PLAYER_AWAY)
					SET_AI_PREDICTION_POINT(thisPlayer.playerAI, thisProps.tennisCourt.vCourtCorners[3])
				ELSE
					SET_AI_PREDICTION_POINT(thisPlayer.playerAI, thisProps.tennisCourt.vCourtCorners[1])
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF NOT IS_PED_INJURED(thisPlayer.pIndex) AND thisPlayer.eSwingState = TSS_NO_SWING
		FLOAT fHeading
		VECTOR vDest
		
		vDest = GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, eServingSide, thisProps.vRight, thisProps.vForward, FALSE, thisPlayer.eCourtSide)
		IF thisPlayer.controlType = COMPUTER
			// Adjust the vDest by the playerAI.fServeBucket amount. Cause them to start closer to where the player has hit before.
			vDest += thisPlayer.vMyRight * GET_TENNIS_AI_SERVE_BUCKET(thisPlayer.playerAI) * PICK_FLOAT(eServingSide = SERVING_FROM_RIGHT, 1.0, -1.0)
		ENDIF
		
		CPRINTLN(DEBUG_TENNIS, "GET_TENNIS_AI_SERVE_BUCKET(thisPlayer.playerAI) is: ", GET_TENNIS_AI_SERVE_BUCKET(thisPlayer.playerAI))
		
		IF thisProps.eTennisActivity <> TA_AI_VS_AI AND NOT bWalkToPos AND NOT IS_PLAYER_SCTV( PLAYER_ID() )
			CLEAR_TENNIS_PED_TASKS(thisProps.tennisPlayers, eSelfID)
			SET_ENTITY_COORDS(thisPlayer.pIndex, vDest, TRUE, FALSE)
			fHeading = GET_HEADING_FROM_VECTOR_2D(-thisProps.vForward.x,-thisProps.vForward.y)
			IF thisPlayer.eCourtSide = (TENNIS_PLAYER_AWAY)
				fHeading += 180
			ENDIF	
			SET_ENTITY_HEADING(thisPlayer.pIndex,fHeading)
		ELSE
			SET_ENTITY_HEADING(thisPlayer.pIndex, GET_ENTITY_HEADING(thisPlayer.pIndex))
			SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_DROP_OUT_OF_TENNIS_MODE)
			FLOAT fFinalHeading = GET_HEADING_FROM_VECTOR_2D(thisPlayer.vMyForward.x, thisPlayer.vMyForward.y) + PICK_FLOAT(bSideChange, 180.0, 0.0)
			VECTOR vPlayerToCenter = (thisProps.tennisCourt.vCenterCourt - GET_ENTITY_COORDS(thisProps.tennisPlayers[eSelfID].pIndex))
			BOOL bOnOppositeSide = DOT_PRODUCT(vPlayerToCenter, thisPlayer.vMyForward) < 0
			BOOL bBetweenIsOnRight = DOT_PRODUCT(vPlayerToCenter, thisPlayer.vMyRight) < 0
			CPRINTLN(DEBUG_TENNIS, "SET_TENNIS_RECEIVE_SERVE: Value of bOnOppositeSide is: ", bOnOppositeSide)
			INT iDelay = 0
			SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, 0)	// Prime sequence threshold to 0, ARE_TENNIS_PLAYERS_IN_POSITION can break out when this AI progress is bigger than GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD
			CLEAR_SEQUENCE_TASK(thisPlayer.seqWalk)
			OPEN_SEQUENCE_TASK(thisPlayer.seqWalk)
				IF NOT bSideChange AND bAddDelay
					SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer) + 1)	// We're adding a task, add one to the sequence threshold
					iDelay = GET_RANDOM_INT_IN_RANGE(800, 1200)
					TASK_PLAY_ANIM(NULL, PICK_STRING(IS_TENNIS_PED_MALE(thisPlayer), "mini@tennis", "mini@tennis@female"), "idle", WALK_BLEND_IN, NORMAL_BLEND_OUT, iDelay)
				ENDIF
				IF NOT bSkipToIdle
					TASK_PLAY_ANIM(NULL, "mini@tennis", "ready_2_idle", WALK_BLEND_IN)
					SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer) + 1)
				ENDIF
				IF ( bSideChange AND NOT bOnOppositeSide ) OR ( NOT bSideChange AND bOnOppositeSide )
					VECTOR vBetween = thisProps.tennisCourt.vCenterCourt + (thisPlayer.vMyRight * SIDE_CHANGE_MID_DEST_SCALAR)
					
					IF NOT bBetweenIsOnRight AND ( NOT bSideChange AND bOnOppositeSide )
						vBetween = thisProps.tennisCourt.vCenterCourt + (thisPlayer.vMyRight * -SIDE_CHANGE_MID_DEST_SCALAR)
					ENDIF
					
					IF VALIDATE_TENNIS_SIDE_CHANGE_USES_STRAIGHT_TO_COORD(thisProps, thisPlayer)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vBetween, PEDMOVE_WALK)
					ELSE
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vBetween, PEDMOVE_WALK, default, default, ENAV_NO_STOPPING)
					ENDIF
					SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer) + 1)
				ENDIF
				
				// If we are really really really close, just achieve heading
				IF ARE_VECTORS_ALMOST_EQUAL_2D(GET_ENTITY_COORDS(thisPlayer.pIndex), vDest, SERVICE_RECEIVE_DIST_THRESHOLD)
					CPRINTLN(DEBUG_TENNIS, "SET_TENNIS_RECEIVE_SERVE: Receiving player is just achieving heading")
					TASK_ACHIEVE_HEADING(NULL, fFinalHeading, 2500)
					SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer) + 1)
				ELSE
					CPRINTLN(DEBUG_TENNIS, "SET_TENNIS_RECEIVE_SERVE: Receiving player is going to service receipt position")
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDest, PEDMOVE_WALK, default, default, default, fFinalHeading)
					SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer) + 1)
				ENDIF
				STRING sDict = PICK_STRING( IS_TENNIS_PED_MALE(thisPlayer), "mini@tennis", "mini@tennis@female" )
				TASK_PLAY_ANIM(NULL, sDict, "idle_2_ready", default, NORMAL_BLEND_OUT)
				SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer) + 1)
			CLOSE_SEQUENCE_TASK(thisPlayer.seqWalk)
			TASK_PERFORM_SEQUENCE(GET_TENNIS_PLAYER_INDEX(thisPlayer), thisPlayer.seqWalk)
			SET_PED_WEAPON_MOVEMENT_CLIPSET(GET_TENNIS_PLAYER_INDEX(thisPlayer), "weapons@tennis@male")
			SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
			CPRINTLN(DEBUG_TENNIS, thisPlayer.playerAI.texName, " tasked to receive serve, iDelay=", iDelay, ", iSeqThreshold=", GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer))
			#IF TENNIS_DEBUG_RECORDING
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_15 texDest = "vDest"
			texDest += thisPlayer.playerAI.texName
			DEBUG_RECORD_SPHERE(texDest, vDest, 0.5, (<<255, 0, 255>>))
			#ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	IF bForcePedUpdate AND DOES_ENTITY_EXIST(GET_TENNIS_PLAYER_INDEX(thisPlayer))
		FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisPlayer))
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets thisPlayer up to serve the ball
/// PARAMS:
///    thisPlayer - 
///    eServingSide - 
///    eSelfID - The spot in the player array of the player passed in as thisPlayer
///    bForcePedUpdate - Used when setting the player in position and they are off camera
PROC SET_TENNIS_PLAYER_SERVE(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, SERVING_SIDE_ENUM eServingSide, TENNIS_PLAYER_ID eSelfID, BOOL bForcePedUpdate = TRUE, BOOL bFirstServe = FALSE, BOOL bAttachBall = TRUE, BOOL bWalkToPos = FALSE, BOOL bSideChange = FALSE, BOOL bSkipToIdle=FALSE)
	CPRINTLN(DEBUG_TENNIS, "prep_next_rally :: ", thisPlayer.playerAI.texName, " set to serve")
	IF thisPlayer.controlType <> COMPUTER AND NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer)) AND NOT bWalkToPos
		ALLOW_TENNIS_PLAYER_CONTROL(thisProps.eTennisActivity, TRUE)
	ENDIF
	SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, TSS_NO_SWING)
	IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_SERVE_EXECUTING)
		CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_SERVE_EXECUTING)
	ENDIF
	CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
	
	//Put the ball in the ped's other hand
	IF bAttachBall
		ATTACH_TENNIS_BALL_TO_PLAYER(thisPlayer, thisProps.tennisBall)
	ENDIF
	
	IF thisPlayer.controlType = COMPUTER
		SET_TENNIS_AI_STATE(thisPlayer.playerAI, ASE_AI_IDLE)
		IF thisPlayer.eCourtSide <> TENNIS_PLAYER_AWAY
			SET_AI_PREDICTION_POINT(thisPlayer.playerAI, (thisProps.tennisCourt.vCourtCorners[2]+thisProps.tennisCourt.vCourtCorners[3])*0.5)
		ELSE
			SET_AI_PREDICTION_POINT(thisPlayer.playerAI, (thisProps.tennisCourt.vCourtCorners[0]+thisProps.tennisCourt.vCourtCorners[1])*0.5)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(thisPlayer.pIndex) AND thisPlayer.eSwingState = TSS_NO_SWING //AND NOT IS_PED_RAGDOLL(GET_TENNIS_PLAYER_INDEX(thisPlayer))
		FLOAT fHeading
		VECTOR vDest
		
		vDest = GET_TENNIS_SERVICE_POSITION(thisProps.tennisCourt, eServingSide, thisProps.vRight, thisProps.vForward, TRUE, thisPlayer.eCourtSide)
		IF thisProps.eTennisActivity <> TA_AI_VS_AI AND NOT bWalkToPos AND NOT IS_PLAYER_SCTV( PLAYER_ID() )
			// Only clear tasks if the player isn't already in the "running" MoVE state
			IF NOT (NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer)) AND IS_TASK_MOVE_NETWORK_ACTIVE(GET_TENNIS_PLAYER_INDEX(thisPlayer)) AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(GET_TENNIS_PLAYER_INDEX(thisPlayer)), "running"))
				CLEAR_TENNIS_PED_TASKS(thisProps.tennisPlayers, eSelfID)
			ENDIF
			SET_ENTITY_COORDS(thisPlayer.pIndex,vDest,TRUE,TRUE)
			fHeading = GET_HEADING_FROM_VECTOR_2D(-thisProps.vForward.x,-thisProps.vForward.y)
			IF thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY
				fHeading += 180
			ENDIF	
			SET_ENTITY_HEADING(thisPlayer.pIndex,fHeading)
		ELSE
			SET_ENTITY_HEADING(thisPlayer.pIndex, GET_ENTITY_HEADING(thisPlayer.pIndex))
			SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_DROP_OUT_OF_TENNIS_MODE)
			FLOAT fFinalHeading = GET_HEADING_FROM_VECTOR_2D(thisPlayer.vMyForward.x, thisPlayer.vMyForward.y) + PICK_FLOAT(bSideChange, 180.0, 0.0)
			VECTOR vPlayerToCenter = (thisProps.tennisCourt.vCenterCourt - GET_ENTITY_COORDS(thisProps.tennisPlayers[eSelfID].pIndex))
			BOOL bOnOppositeSide = DOT_PRODUCT(vPlayerToCenter, thisPlayer.vMyForward) < 0
			BOOL bBetweenIsOnRight = DOT_PRODUCT(vPlayerToCenter, thisPlayer.vMyRight) < 0
			CPRINTLN(DEBUG_TENNIS, "SET_TENNIS_PLAYER_SERVE: Value of bOnOppositeSide is: ", bOnOppositeSide)
			SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, 0)
			CLEAR_SEQUENCE_TASK(thisPlayer.seqWalk)
			OPEN_SEQUENCE_TASK(thisPlayer.seqWalk)
				IF NOT bSkipToIdle
					TASK_PLAY_ANIM(NULL, "mini@tennis", "ready_2_idle", WALK_BLEND_IN)
					SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer) + 1)
				ENDIF
				IF ( bSideChange AND NOT bOnOppositeSide ) OR ( NOT bSideChange AND bOnOppositeSide )
					VECTOR vBetween = thisProps.tennisCourt.vCenterCourt + (thisPlayer.vMyRight * SIDE_CHANGE_MID_DEST_SCALAR)
					
					IF NOT bBetweenIsOnRight AND ( NOT bSideChange AND bOnOppositeSide )
						vBetween = thisProps.tennisCourt.vCenterCourt + (thisPlayer.vMyRight * -SIDE_CHANGE_MID_DEST_SCALAR)
					ENDIF
					
					IF VALIDATE_TENNIS_SIDE_CHANGE_USES_STRAIGHT_TO_COORD(thisProps, thisPlayer)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vBetween, PEDMOVE_WALK)
					ELSE
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vBetween, PEDMOVE_WALK, default, default, ENAV_NO_STOPPING)
					ENDIF
					SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer) + 1)
				ENDIF
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDest, PEDMOVE_WALK, default, default, default, fFinalHeading)
				SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer) + 1)
				#IF IS_TENNIS_MULTIPLAYER		TASK_PLAY_ANIM(NULL, "mini@tennis", "idle_2_serve", default, default, default, AF_HOLD_LAST_FRAME|AF_NOT_INTERRUPTABLE)	#ENDIF
				#IF NOT IS_TENNIS_MULTIPLAYER	TASK_PLAY_ANIM(NULL, "mini@tennis", "idle_2_serve")	#ENDIF
				SET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer, GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer) + 1)
			CLOSE_SEQUENCE_TASK(thisPlayer.seqWalk)
			TASK_PERFORM_SEQUENCE(thisPlayer.pIndex, thisPlayer.seqWalk)
			SET_PED_WEAPON_MOVEMENT_CLIPSET(thisPlayer.pIndex, "weapons@tennis@male")
			SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
			CPRINTLN(DEBUG_TENNIS, PICK_STRING(eSelfID = TENNIS_PLAYER_AWAY, "TENNIS_PLAYER_AWAY", "TENNIS_PLAYER_HOME"), " Tasked to go to serve spot, iSeqThreshold=", GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(thisPlayer))
			#IF TENNIS_DEBUG_RECORDING
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_15 texDest = "vDest"
			texDest += thisPlayer.playerAI.texName
			DEBUG_RECORD_SPHERE(texDest, vDest, 0.5, (<<255, 0, 255>>))
			#ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	IF NOT bWalkToPos AND thisProps.eTennisActivity <> TA_AI_VS_AI
		IF NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_JUST_TASKED)
			BOOL bNoIntro = thisProps.eTennisActivity <> TA_AI_VS_AI AND NOT bWalkToPos
			STRING sNetworkName = PICK_STRING(bNoIntro, "TennisServeSet", "TennisServeSetWithIntro")
			SET_UP_TENNIS_MOVE_NETWORK(thisPlayer, sNetworkName, 0.0)
			bFirstServe = bFirstServe
		ELSE
			SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisPlayer),TRUE)
		ENDIF
	ENDIF
	
	IF bForcePedUpdate AND DOES_ENTITY_EXIST(GET_TENNIS_PLAYER_INDEX(thisPlayer))
		FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(thisPlayer))
	ENDIF
	
	thisPlayer.vPos = GET_ENTITY_COORDS( thisPlayer.pIndex, FALSE )
ENDPROC

/// PURPOSE:
///    Determines if a player is within the box defined by the midcourt service corners.
/// PARAMS:
///    thisPlayer - 
///    thisCourt - 
///    vRight - The right vector for the court. Found in "thisProps"
/// RETURNS:
///    TRUE if the player passed in is close to the net.
FUNC BOOL IS_TENNIS_PLAYER_IN_SERVICE_BOXES(TENNIS_PLAYER &thisPlayer, TENNIS_COURT &thisCourt, VECTOR vRight)
	VECTOR v1, v2
	v1 = thisCourt.vServiceCorners[0] + (vRight * 0.5)
	v2 = thisCourt.vServiceCorners[3] + (vRight * 0.5)
	BOOL bReturn = IS_POINT_INSIDE_TENNIS_AREA(thisPlayer.vPos, v1, v2, GET_TENNIS_COURT_WIDTH(thisCourt))
//	CPRINTLN(DEBUG_TENNIS, "IS_TENNIS_PLAYER_IN_SERVICE_BOXES :: ", PICK_STRING(bReturn, "TRUE", "FALSE"))
	RETURN bReturn
ENDFUNC

/// PURPOSE:
///    Checks if the player can use a POWER_VOLLEY LAUNCH_DETAIL_ENUM
/// PARAMS:
///    thisPlayer - 
///    thisCourt - 
///    vRight - 
///    fBallZ - 
///    iBounces - 
/// RETURNS:
///    The grade of the volley, returns LD_INVALID when no power volley is allowed
FUNC BOOL CAN_TENNIS_PLAYER_USE_POWER_VOLLEY(TENNIS_PLAYER &thisPlayer, TENNIS_COURT &thisCourt, VECTOR &vRight, FLOAT fBallZ, INT iBounces)
	IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_DIVE_BACKHAND OR GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_DIVE_FOREHAND
		RETURN FALSE
	ENDIF
	
	FLOAT fDiff = fBallZ - thisCourt.vCourtCorners[0].z
	BOOL bAICheck = (thisPlayer.controlType <> COMPUTER OR thisPlayer.playerAI.eDifficulty > AD_EASY)
	BOOL bBounceCheck = iBounces < 1
	
	IF NOT bAICheck
		RETURN FALSE
	ENDIF
	
	IF NOT bBounceCheck
		RETURN FALSE
	ENDIF
	
	// we use the swinging player here because we just want the drop shot distance to net check.
	IF NOT IS_TENNIS_PLAYER_IN_SERVICE_BOXES(thisPlayer, thisCourt, vRight)
		CPRINTLN(DEBUG_TENNIS, "CAN_TENNIS_PLAYER_USE_POWER_VOLLEY :: failed IS_POINT_INSIDE_TENNIS_AREA")
		RETURN FALSE
	ENDIF
	
	IF fDiff < POWER_VOLLEY_HEIGHT_FLOOR
		CPRINTLN(DEBUG_TENNIS, "CAN_TENNIS_PLAYER_USE_POWER_VOLLEY :: failed, fDiff < POWER_VOLLEY_HEIGHT_FLOOR (", fDiff, ")")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Used when setting the launch_details of an AI player
/// PARAMS:
///    thisPlayer - an AI controlled player
///    thisProps - 
///    serveGrade - passed by reference and modified
PROC GET_LAUNCH_DETAILS_AI(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, LAUNCH_DETAILS &serveGrade, TENNIS_PLAYER_ID eThisPedID)
	INT iOppWho = 1 - ENUM_TO_INT(eThisPedID)
	VECTOR v2
	FLOAT fAngleA = GET_HEADING_BETWEEN_VECTORS(thisPlayer.vPos, thisProps.tennisPlayers[iOppWho].vPos)
	FLOAT fAngleB = GET_HEADING_BETWEEN_VECTORS(thisPlayer.vPos, thisPlayer.vPos + thisPlayer.vMyForward)
	FLOAT fAngleResult = fAngleA - fAngleB
	
	SWITCH thisPlayer.playerAI.eDifficulty
		CASE AD_TUTORIAL
			VECTOR vEnd
			vEnd = thisPlayer.vPos + thisPlayer.vMyForward
			fAngleResult = GET_ANGLE_BETWEEN_2D_VECTORS(thisPlayer.vPos.x, thisPlayer.vPos.y, vEnd.x, vEnd.y)
			thisPlayer.fLeftRight = fAngleResult
		BREAK
	
		CASE AD_EASY
			fAngleA = GET_HEADING_BETWEEN_VECTORS(thisPlayer.vPos, thisProps.tennisBall.vBallLaunchPos)
			fAngleResult = fAngleA - fAngleB
			serveGrade.iAngle = ROUND(GET_RANDOM_FLOAT_IN_RANGE(serveGrade.iAngle - UP_DOWN_RANGE, serveGrade.iAngle + UP_DOWN_RANGE))
			thisPlayer.fLeftRight = (GET_RANDOM_FLOAT_IN_RANGE(fAngleResult - LEFT_RIGHT_EASY_OFFSET, fAngleResult + LEFT_RIGHT_EASY_OFFSET))
			RESET_TENNIS_BALL_SPIN(thisProps.tennisBall)
		BREAK
		
		CASE AD_NORMAL
			FLOAT fNeg
			serveGrade.iAngle =ROUND(GET_RANDOM_FLOAT_IN_RANGE(serveGrade.iAngle - UP_DOWN_RANGE, serveGrade.iAngle + UP_DOWN_RANGE))
			//Determine which eCourtSide the opponent is on and adjust accordingly
			fNeg = PICK_FLOAT(thisProps.tennisPlayers[iOppWho].eCourtSide = TENNIS_PLAYER_AWAY, 1.0, -1.0)
			//Figure out the relative side of the court the opponent is on, in terms of serving side
			IF VDIST2(thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisPlayers[iOppWho].vPos) > VDIST2(thisProps.tennisCourt.vCourtCorners[1], thisProps.tennisPlayers[iOppWho].vPos) 
				//Hit the ball to the other side of that player
				v2 = thisProps.tennisPlayers[iOppWho].vPos - ((thisProps.tennisPlayers[iOppWho].vMyRight*LEFT_RIGHT_NORM_MULT) * fNeg)
			ELSE
				v2 = thisProps.tennisPlayers[iOppWho].vPos + ((thisProps.tennisPlayers[iOppWho].vMyRight*LEFT_RIGHT_NORM_MULT) * fNeg)
			ENDIF
			fAngleA = GET_HEADING_BETWEEN_VECTORS(thisPlayer.vPos, v2)
			fAngleResult = (fAngleA - fAngleB)
			thisPlayer.fLeftRight = (GET_RANDOM_FLOAT_IN_RANGE(fAngleResult - LEFT_RIGHT_NORM_OFFSET, fAngleResult + LEFT_RIGHT_NORM_OFFSET))
			
			//Determine Chance of Spin
			IF GET_RANDOM_FLOAT_IN_RANGE() < NORMAL_SPIN_RATE
				IF GET_RANDOM_FLOAT_IN_RANGE() < 0.5
					SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_TOPSPIN)
				ELSE
					SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_SLICE)
				ENDIF
			ELSE
				RESET_TENNIS_BALL_SPIN(thisProps.tennisBall)
			ENDIF
		BREAK
		
		CASE AD_HARD
			serveGrade.iAngle = ROUND(GET_RANDOM_FLOAT_IN_RANGE(serveGrade.iAngle - UP_DOWN_RANGE, TO_FLOAT(serveGrade.iAngle)))
			//Figure out the relative side of the court the opponent is on
			IF (VDIST2(thisProps.tennisCourt.vCourtCorners[0], thisProps.tennisPlayers[iOppWho].vPos) > VDIST2(thisProps.tennisCourt.vCourtCorners[1], thisProps.tennisPlayers[iOppWho].vPos))
				//Hit the ball to 0 or 3
				IF thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY
					IF IS_BALL_INSIDE_COURT(INT_TO_ENUM(TENNIS_PLAYER_ID, iOppWho), thisProps, FALSE)
						v2 = thisProps.tennisCourt.vCourtCorners[3] + (thisPlayer.vMyRight * 0.5)
						CPRINTLN(DEBUG_TENNIS, "Choosing corner 3 as first choice")
					ELIF (VDIST2(thisProps.tennisCourt.vCourtCorners[0], thisPlayer.vPos) < VDIST2(thisProps.tennisCourt.vCourtCorners[1], thisPlayer.vPos))
						v2 = thisProps.tennisCourt.vCourtCorners[2]
						CPRINTLN(DEBUG_TENNIS, "Choosing corner 2 as alt")
					ELSE
						v2 = thisProps.tennisCourt.vCourtCorners[3]
						CPRINTLN(DEBUG_TENNIS, "Away Player 3/2 Default Case")
					ENDIF
				ELSE
					IF IS_BALL_INSIDE_COURT(INT_TO_ENUM(TENNIS_PLAYER_ID, iOppWho), thisProps, FALSE)
						v2 = thisProps.tennisCourt.vCourtCorners[0] - (thisPlayer.vMyRight * 0.5)
						CPRINTLN(DEBUG_TENNIS, "Choosing corner 0 as first choice")
					ELIF (VDIST2(thisProps.tennisCourt.vCourtCorners[3], thisPlayer.vPos) < VDIST2(thisProps.tennisCourt.vCourtCorners[2], thisPlayer.vPos))
						v2 = thisProps.tennisCourt.vCourtCorners[1]
						CPRINTLN(DEBUG_TENNIS, "Choosing corner 1 as alt")
					ELSE
						v2 = thisProps.tennisCourt.vCourtCorners[0]
						CPRINTLN(DEBUG_TENNIS, "Home Player 0/1 Default Case")
					ENDIF
				ENDIF
			ELSE
				//Hit the ball to 1 or 2
				IF thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY
					IF IS_BALL_INSIDE_COURT(INT_TO_ENUM(TENNIS_PLAYER_ID, iOppWho), thisProps, FALSE)
						v2 = thisProps.tennisCourt.vCourtCorners[2]	- (thisPlayer.vMyRight * 0.5)
						CPRINTLN(DEBUG_TENNIS, "Choosing corner 2 as first choice")
					ELIF (VDIST2(thisProps.tennisCourt.vCourtCorners[1], thisPlayer.vPos) < VDIST2(thisProps.tennisCourt.vCourtCorners[0], thisPlayer.vPos))
						v2 = thisProps.tennisCourt.vCourtCorners[3]
						CPRINTLN(DEBUG_TENNIS, "Choosing corner 3 as alt")
					ELSE
						v2 = thisProps.tennisCourt.vCourtCorners[2]
						CPRINTLN(DEBUG_TENNIS, "Away Player 2/3 Default Case")
					ENDIF
				ELSE
					IF IS_BALL_INSIDE_COURT(INT_TO_ENUM(TENNIS_PLAYER_ID, iOppWho), thisProps, FALSE)
						v2 = thisProps.tennisCourt.vCourtCorners[1] + (thisPlayer.vMyRight * 0.5)
						CPRINTLN(DEBUG_TENNIS, "Choosing corner 1 as first choice")
					ELIF (VDIST2(thisProps.tennisCourt.vCourtCorners[2], thisPlayer.vPos) < VDIST2(thisProps.tennisCourt.vCourtCorners[3], thisPlayer.vPos))
						v2 = thisProps.tennisCourt.vCourtCorners[0]
						CPRINTLN(DEBUG_TENNIS, "Choosing corner 0 as alt")
					ELSE
						v2 = thisProps.tennisCourt.vCourtCorners[1]
						CPRINTLN(DEBUG_TENNIS, "Away Player 1/0 Default Case")
					ENDIF
				ENDIF
			ENDIF
			
			fAngleA = GET_HEADING_BETWEEN_VECTORS(GET_TENNIS_BALL_POS(thisProps.tennisBall), v2)	// Changed Hard AI to use the ball position in fAngleA to ensure it's more in than out.
			fAngleResult = (fAngleA - fAngleB)
			thisPlayer.fLeftRight = (fAngleResult)
			
			//Determine Chance of Spin
			IF GET_RANDOM_FLOAT_IN_RANGE() < HARD_SPIN_RATE
				IF GET_RANDOM_FLOAT_IN_RANGE() < HARD_TOPSPIN_RATE
					SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_TOPSPIN)
				ELSE
					SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_SLICE)
				ENDIF
			ELSE
				RESET_TENNIS_BALL_SPIN(thisProps.tennisBall)
			ENDIF
		BREAK
	ENDSWITCH
	
	//Scale force with average squared distance to the net
	FLOAT f1 = VDIST2(thisPlayer.vPos, thisProps.tennisCourt.vCenterCourt)
	FLOAT f2 = VDIST2(thisPlayer.vPos, thisProps.tennisCourt.vNetSideline[0])
	FLOAT f3 = VDIST2(thisPlayer.vPos, thisProps.tennisCourt.vNetSideline[1])
	IF GET_TENNIS_PLAYER_STROKE(thisPlayer) = SHOT_LOB
		serveGrade.iForce -= FLOOR(( 1 / ((f1+f2+f3) / 3) ) * NET_SCALAR_LOB)
	ELSE
		serveGrade.iForce -= FLOOR(( 1 / ((f1+f2+f3) / 3) ) * NET_SCALAR)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets thisPlayer.fLeftRight as the angle of the shot
/// PARAMS:
///    thisPlayer - the player performing the serve
///    thisProps - 
///    eServingSide - 
///    x - stored x value of serve angle
/// RETURNS:
///    TRUE if the X controller value is in service bounds, FALSE when guaranteeing a fault
FUNC BOOL GET_LAUNCH_DETAILS_LEFT_RIGHT_SERVE(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, SERVING_SIDE_ENUM eServingSide, INT x)
	FLOAT fAngleResult
	VECTOR vCorner = <<0,0,0>>, vNorm, v1, v2, vRightNorm, vHomePos
	FLOAT fPercent	//128 is added below because x ranges from -256 to 0. This makes the range -128 to 128
	FLOAT fFlip = 1
	FLOAT fOuterCornerMult, fInnerRightMult, fInnerLeftMult
	BOOL bReturn
	
//	CPRINTLN(DEBUG_TENNIS, "GET_LAUNCH_DETAILS_LEFT_RIGHT_SERVE x is ", x)
	
	vHomePos = GET_ENTITY_COORDS(thisProps.tennisBall.oBall)
	
	IF ABSI(x) > SERVE_OOB_THRESHOLD
		fOuterCornerMult = OUTER_CORNER_MULT
		fInnerRightMult = INNER_RIGHT_MULT
		fInnerLeftMult = INNER_LEFT_MULT
		bReturn = FALSE
	ELSE
		fOuterCornerMult = OUTER_IN_BOUNDS
		fInnerRightMult = 0.0
		fInnerLeftMult = 0.0
		bReturn = TRUE
	ENDIF
	
	vRightNorm = NORMALISE_VECTOR(thisProps.vRight)
	fPercent = (TO_FLOAT(x) + 128) / 256
	
	//Determine the point we should aim at
	IF thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY
		IF eServingSide = SERVING_FROM_RIGHT
			//Cross corner is 3
			v1 = thisProps.tennisCourt.vServiceCorners[3] - (vRightNorm * fOuterCornerMult)
			v2 = thisProps.tennisCourt.vServiceCorners[3] + thisProps.vRight * 0.5 + (vRightNorm * fInnerRightMult)
			// We always LERP from left corner to right corner
			vCorner = LERP_VECTOR(v1, v2, fPercent)
			//We flip the result if the vectors face the same direction. (we're aiming to our left)
			IF DOT_PRODUCT(thisPlayer.vMyRight, vCorner - vHomePos) > 0
				fFlip = -1
				CPRINTLN(DEBUG_TENNIS, "Flipping, Away side from Right")
			ENDIF
		ELIF eServingSide = SERVING_FROM_LEFT
			//Cross Corner is 2
			v2 = thisProps.tennisCourt.vServiceCorners[2] + (vRightNorm * fOuterCornerMult)
			v1 = thisProps.tennisCourt.vServiceCorners[2] - thisProps.vRight * 0.5 - (vRightNorm * fInnerLeftMult)
			vCorner = LERP_VECTOR(v1, v2, fPercent)
			//We flip the result if the vectors DO NOT face the same direction. (we're aiming to our right)
			IF DOT_PRODUCT(thisPlayer.vMyRight, vCorner - vHomePos) < 0
				fFlip = -1
				CPRINTLN(DEBUG_TENNIS, "Flipping, Away side from Left")
			ENDIF
		ENDIF
	ELSE
		IF eServingSide = SERVING_FROM_RIGHT
			//Cross Corner is 1
			v1 = thisProps.tennisCourt.vServiceCorners[1] + (vRightNorm * fOuterCornerMult)
			v2 = thisProps.tennisCourt.vServiceCorners[1] - thisProps.vRight * 0.5 - (vRightNorm * fInnerRightMult)
			vCorner = LERP_VECTOR(v1, v2, fPercent)
			//We flip the result if the vectors DO NOT face the same direction. (we're aiming to our left)
			IF DOT_PRODUCT(thisPlayer.vMyRight, vCorner - vHomePos) > 0
				fFlip = -1
				CPRINTLN(DEBUG_TENNIS, "Flipping, Home side from Right")
			ENDIF
		ELIF eServingSide = SERVING_FROM_LEFT
			//Cross Corner is 0
			v2 = thisProps.tennisCourt.vServiceCorners[0] - (vRightNorm * fOuterCornerMult)
			v1 = thisProps.tennisCourt.vServiceCorners[0] + thisProps.vRight * 0.5 + (vRightNorm * fInnerLeftMult)
			vCorner = LERP_VECTOR(v1, v2, fPercent)
			//We flip the result if the vectors DO NOT face the same direction. (we're aiming to our right)
			IF DOT_PRODUCT(thisPlayer.vMyRight, vCorner - vHomePos) < 0
				fFlip = -1
				CPRINTLN(DEBUG_TENNIS, "Flipping, Home side from Left")
			ENDIF
		ENDIF
	ENDIF
	
	vCorner.z = thisPlayer.vPos.z
	vNorm = NORMALISE_VECTOR(vCorner - vHomePos)
	fAngleResult = GET_ANGLE_BETWEEN_2D_VECTORS(thisPlayer.vMyForward.x, thisPlayer.vMyForward.y, vNorm.x, vNorm.y)
	fAngleResult *= fFlip
	thisPlayer.fLeftRight = fAngleResult
	
	IF eServingSide = SERVING_FROM_LEFT
		thisPlayer.fLeftRight *= -1		//Flip the sign for correct direction
	ENDIF
//	CPRINTLN(DEBUG_TENNIS, "GET_LAUNCH_DETAILS_LEFT_RIGHT_SERVE end of func x is ", x, ", fLeftRight is ", thisPlayer.fLeftRight)
	RETURN bReturn
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisPlayer - 
///    thisCourt - 
///    vBallPos - 
///    iPadX - 
/// RETURNS:
///    direction vector to destination from vBallPos
FUNC VECTOR CALCULATE_TENNIS_DESTINATION_VECTOR_HUMAN(TENNIS_PLAYER &thisPlayer, TENNIS_COURT &thisCourt, VECTOR vBallPos, INT iPadX)
	VECTOR vReturn
	VECTOR vLeftAdjust, vRightAdjust
	vLeftAdjust = <<0,0,0>>	//thisPlayer.vMyRight * -1.0
	vRightAdjust = <<0,0,0>>	//thisPlayer.vMyRight
	
	INT iLeftCorner = PICK_INT(thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY, 3, 1)
	INT iRightCorner = PICK_INT(thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY, 2, 0)
	
	VECTOR vHalfRight = (thisCourt.vCourtCorners[1] - thisCourt.vCourtCorners[0]) * 0.5
//	DRAW_DEBUG_POLY_ANGLED_AREA(thisCourt.vCourtCorners[0] + vHalfRight, thisCourt.vCourtCorners[3] + vHalfRight, GET_TENNIS_COURT_WIDTH(thisCourt))
	IF NOT IS_POINT_INSIDE_TENNIS_AREA(vBallPos, thisCourt.vCourtCorners[0] + vHalfRight, thisCourt.vCourtCorners[3] + vHalfRight, GET_TENNIS_COURT_WIDTH(thisCourt))
		FLOAT fOOBAimScalar = LERP_FLOAT(OOB_AIM_IN_SCALAR_MIN, OOB_AIM_IN_SCALAR_MAX, GET_RANDOM_FLOAT_IN_RANGE())
		IF DOT_PRODUCT(thisPlayer.vMyRight, thisPlayer.vPos - thisCourt.vCourtCorners[0]) < 0
			vLeftAdjust = thisPlayer.vMyRight * fOOBAimScalar
		ENDIF
		IF DOT_PRODUCT(thisPlayer.vMyRight, thisPlayer.vPos - thisCourt.vCourtCorners[1]) >= 0
			vRightAdjust = thisPlayer.vMyRight * fOOBAimScalar * -1.0
		ENDIF
	ENDIF
	// Aim left or right
	VECTOR vProjected = GET_CLOSEST_POINT_ON_LINE(thisPlayer.vPos, thisCourt.vCourtCorners[iLeftCorner], thisCourt.vCourtCorners[iRightCorner])
	IF ABSI(iPadX) > DEAD_ZONE_THRESHOLD
		FLOAT fAlpha
		VECTOR vDest
		IF iPadX > 0
			fAlpha = TO_FLOAT(iPadX) / TO_FLOAT(ANALOG_OFFSET)
			vDest = LERP_VECTOR(vProjected, thisCourt.vCourtCorners[iRightCorner] + vRightAdjust, fAlpha)
		ELSE
			fAlpha = TO_FLOAT(iPadX) / TO_FLOAT(-ANALOG_OFFSET)
			vDest = LERP_VECTOR(vProjected, thisCourt.vCourtCorners[iLeftCorner] + vLeftAdjust, fAlpha)
		ENDIF
		vReturn = vDest - vBallPos
		#IF TENNIS_DEBUG_RECORDING
		#IF IS_DEBUG_BUILD
		DEBUG_RECORD_SPHERE("vDest", vDest, 0.3, <<0,255,0>>)
		DEBUG_RECORD_SPHERE("leftCorner", thisCourt.vCourtCorners[iLeftCorner] + vLeftAdjust, 0.2, <<0,255,255>>)
		DEBUG_RECORD_SPHERE("rightCorner", thisCourt.vCourtCorners[iRightCorner] + vRightAdjust, 0.2, <<0,255,255>>)
		#ENDIF
		#ENDIF
	ELSE	// Aim straight
		vReturn = vProjected - vBallPos
		#IF TENNIS_DEBUG_RECORDING
		#IF IS_DEBUG_BUILD
		DEBUG_RECORD_SPHERE("vProjected", vProjected, 0.5, <<0,255,0>>)
		#ENDIF
		#ENDIF
	ENDIF
	
	RETURN vReturn
ENDFUNC

FUNC VECTOR CALCULATE_TENNIS_DESTINATION_VECTOR_AI(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, VECTOR &vBallLaunchPos, VECTOR &vOppPos, TENNIS_PLAYER_ID eThisPedID)
	INT iLeftCorner = PICK_INT(thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY, 3, 1)
	INT iRightCorner = PICK_INT(thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY, 2, 0)
	
	INT iOppWho = 1 - ENUM_TO_INT(eThisPedID)
	VECTOR vReturn
	
	IF thisPlayer.playerAI.eDifficulty = AD_EASY
	OR IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_SERVE_WAS_POWERFUL)
		VECTOR vVariance = GET_RANDOM_FLOAT_IN_RANGE(-1, 1) * thisPlayer.vMyRight
		vReturn = (vBallLaunchPos + vVariance) - thisPlayer.vPos
		IF IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_SERVE_WAS_POWERFUL)
			CPRINTLN(DEBUG_TENNIS, "CALCULATE_TENNIS_DESTINATION_VECTOR_AI: Using easy shot return style")
			CLEAR_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_SERVE_WAS_POWERFUL)
		ENDIF
	ELIF thisPlayer.playerAI.eDifficulty = AD_NORMAL
		FLOAT fVariance = GET_RANDOM_FLOAT_IN_RANGE(-0.15, 1.15)
		VECTOR vLeft, vRight
		VECTOR vHalfway = (thisProps.tennisCourt.vCourtCorners[iRightCorner] - thisProps.tennisCourt.vCourtCorners[iLeftCorner]) * 0.5
		IF VDIST2(vOppPos, thisProps.tennisCourt.vCourtCorners[iLeftCorner]) > VDIST2(vOppPos, thisProps.tennisCourt.vCourtCorners[iRightCorner])
			vLeft = thisProps.tennisCourt.vCourtCorners[iLeftCorner]
			vRight = vLeft + vHalfway
		ELSE
			vLeft = thisProps.tennisCourt.vCourtCorners[iLeftCorner] + vHalfway
			vRight = thisProps.tennisCourt.vCourtCorners[iRightCorner]
		ENDIF
		VECTOR vDest = LERP_VECTOR(vLeft, vRight, fVariance)
		vReturn = vDest - GET_TENNIS_BALL_POS(thisProps.tennisBall)
		
	ELIF thisPlayer.playerAI.eDifficulty = AD_HARD
		VECTOR v2, vHardUpCourt
		BOOL bAICheating = FALSE
		FLOAT fDist1, fDist2, fDistDiff, fCheatThreshold, fCheatRoll
		fDist1 = VDIST2(thisProps.tennisCourt.vCourtCorners[0], vOppPos)
		fDist2 = VDIST2(thisProps.tennisCourt.vCourtCorners[1], vOppPos)
		fDistDiff = ABSF(fDist1 - fDist2)
		fCheatThreshold = LERP_FLOAT(HIGH_AI_CHEAT_CHANCE, LOW_AI_CHEAT_CHANCE, (fDistDiff/AI_CHEAT_DIST_MAX))
		fCheatRoll = GET_RANDOM_FLOAT_IN_RANGE()
		CPRINTLN(DEBUG_TENNIS, "HARD AI CHEATING? fCheatThreshold=", fCheatThreshold, ", fCheatRoll=", fCheatRoll, ", fDistDiff=", fDistDiff, ", fDist1=", fDist1, ", fDist2=", fDist2)
		
		SWITCH GET_TENNIS_PLAYER_STROKE(thisPlayer)
			CASE SHOT_NORMAL		vHardUpCourt = thisPlayer.vMyForward * AI_HARD_UP_NORMAL_SCALAR			BREAK
			CASE SHOT_TOPSPIN		vHardUpCourt = thisPlayer.vMyForward * AI_HARD_UP_TOPSPIN_SCALAR		BREAK
			CASE SHOT_BACKSPIN		vHardUpCourt = thisPlayer.vMyForward * AI_HARD_UP_BACKSPIN_SCALAR		BREAK
		ENDSWITCH
		
		IF fCheatRoll < fCheatThreshold AND IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_PLYR_RUN_LR_HELD)
			//Read inputs, shoot other direction
			bAICheating = TRUE
			INT x, dummy
			GET_ANALOG_STICK_VALUES(x, dummy, dummy, dummy)
			IF x > DEAD_ZONE_THRESHOLD
				dummy = PICK_INT(thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY, 2, 0)
				v2 = thisProps.tennisCourt.vCourtCorners[dummy] + vHardUpCourt
				CPRINTLN(DEBUG_TENNIS, "AI Cheating, player is running right, x=", x, ", corner=", dummy)
			ELIF x < -DEAD_ZONE_THRESHOLD
				dummy = PICK_INT(thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY, 3, 1)
				v2 = thisProps.tennisCourt.vCourtCorners[dummy] + vHardUpCourt
				CPRINTLN(DEBUG_TENNIS, "AI Cheating, player is running left, x=", x, ", corner=", dummy)
			ELSE
				bAICheating = FALSE
				CPRINTLN(DEBUG_TENNIS, "AI Tried to cheat but no left/right input to read, x=", x)
			ENDIF
		ENDIF
		
		//Figure out the relative side of the court the opponent is on
		IF (fDist1 > fDist2) AND NOT bAICheating
			//Hit the ball to 0 or 3
			IF thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY
				fDist1 = VDIST2(thisProps.tennisCourt.vCourtCorners[0], thisPlayer.vPos)
				fDist2 = VDIST2(thisProps.tennisCourt.vCourtCorners[1], thisPlayer.vPos)
				IF IS_BALL_INSIDE_COURT(INT_TO_ENUM(TENNIS_PLAYER_ID, iOppWho), thisProps, FALSE)
					v2 = thisProps.tennisCourt.vCourtCorners[3] + vHardUpCourt	// + (thisPlayer.vMyRight * 0.5)
					CPRINTLN(DEBUG_TENNIS, "Choosing corner 3 as first choice")
				ELIF (fDist1 < fDist2)
					v2 = thisProps.tennisCourt.vCourtCorners[3] + (thisPlayer.vMyRight * 1.5) + vHardUpCourt
					CPRINTLN(DEBUG_TENNIS, "Choosing corner 2 as alt")
				ELSE
					v2 = thisProps.tennisCourt.vCourtCorners[3] + vHardUpCourt
					CPRINTLN(DEBUG_TENNIS, "Away Player 3/2 Default Case")
				ENDIF
			ELSE
				fDist1 = VDIST2(thisProps.tennisCourt.vCourtCorners[3], thisPlayer.vPos)
				fDist2 = VDIST2(thisProps.tennisCourt.vCourtCorners[2], thisPlayer.vPos)
				IF IS_BALL_INSIDE_COURT(INT_TO_ENUM(TENNIS_PLAYER_ID, iOppWho), thisProps, FALSE)
					v2 = thisProps.tennisCourt.vCourtCorners[0] + vHardUpCourt	// - (thisPlayer.vMyRight * 0.5)
					CPRINTLN(DEBUG_TENNIS, "Choosing corner 0 as first choice")
				ELIF (fDist1 < fDist2)
					v2 = thisProps.tennisCourt.vCourtCorners[0] - (thisPlayer.vMyRight * 1.5) + vHardUpCourt	//1]
					CPRINTLN(DEBUG_TENNIS, "Choosing corner 0 - (thisPlayer.vMyRight * 2.0) as alt")
				ELSE
					v2 = thisProps.tennisCourt.vCourtCorners[0] + vHardUpCourt
					CPRINTLN(DEBUG_TENNIS, "Home Player 0/1 Default Case")
				ENDIF
			ENDIF
		ELIF NOT bAICheating
			//Hit the ball to 1 or 2
			IF thisPlayer.eCourtSide = TENNIS_PLAYER_AWAY
				fDist1 = VDIST2(thisProps.tennisCourt.vCourtCorners[1], thisPlayer.vPos)
				fDist2 = VDIST2(thisProps.tennisCourt.vCourtCorners[0], thisPlayer.vPos)
				IF IS_BALL_INSIDE_COURT(INT_TO_ENUM(TENNIS_PLAYER_ID, iOppWho), thisProps, FALSE)
					v2 = thisProps.tennisCourt.vCourtCorners[2] + vHardUpCourt	//	- (thisPlayer.vMyRight * 0.5)
					CPRINTLN(DEBUG_TENNIS, "Choosing corner 2 as first choice")
				ELIF (fDist1 < fDist2)
					v2 = thisProps.tennisCourt.vCourtCorners[2] - (thisPlayer.vMyRight * 1.5) + vHardUpCourt
					CPRINTLN(DEBUG_TENNIS, "Choosing corner 3 as alt")
				ELSE
					v2 = thisProps.tennisCourt.vCourtCorners[2] + vHardUpCourt
					CPRINTLN(DEBUG_TENNIS, "Away Player 2/3 Default Case")
				ENDIF
			ELSE
				fDist1 = VDIST2(thisProps.tennisCourt.vCourtCorners[2], thisPlayer.vPos)
				fDist2 = VDIST2(thisProps.tennisCourt.vCourtCorners[3], thisPlayer.vPos)
				IF IS_BALL_INSIDE_COURT(INT_TO_ENUM(TENNIS_PLAYER_ID, iOppWho), thisProps, FALSE)
					v2 = thisProps.tennisCourt.vCourtCorners[1] + vHardUpCourt	// + (thisPlayer.vMyRight * 0.5)
					CPRINTLN(DEBUG_TENNIS, "Choosing corner 1 as first choice")
				ELIF (fDist1 < fDist2)
					v2 = thisProps.tennisCourt.vCourtCorners[1] + (thisPlayer.vMyRight * 1.5) + vHardUpCourt	//0]
					CPRINTLN(DEBUG_TENNIS, "Choosing corner 0 as alt")
				ELSE
					v2 = thisProps.tennisCourt.vCourtCorners[1] + vHardUpCourt
					CPRINTLN(DEBUG_TENNIS, "Away Player 1/0 Default Case")
				ENDIF
			ENDIF
		ENDIF
		
		#IF TENNIS_DEBUG_RECORDING
		#IF IS_DEBUG_BUILD
		DEBUG_RECORD_SPHERE("HardAIvReturn", vReturn, 0.5, (<<0,0,255>>))
		#ENDIF
		#ENDIF
		
		vReturn = v2 - GET_TENNIS_BALL_POS(thisProps.tennisBall)
		
	ELSE	// AD_TUTORIAL et al.
		vReturn = vBallLaunchPos - thisPlayer.vPos
	ENDIF
	
	RETURN vReturn
ENDFUNC

FUNC VECTOR CREATE_TENNIS_FORCE_VECTOR(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, INT iPadX, TENNIS_PLAYER_ID eThisPedID, VECTOR vBallPosAtHit, BOOL bPowerVolley=FALSE, BOOL bStrongSwing=FALSE, BOOL bLob=FALSE, BOOL bDropShot=FALSE)
	VECTOR vForce
	FLOAT fMinShotPower = MIN_SHOT_POWER
	FLOAT fMaxUpwardZee = MAX_UPWARD_ZEE
	FLOAT fMinUpwardZee = MIN_UPWARD_ZEE
	FLOAT fMaxDownwardZee = MAX_DOWNWARD_ZEE
	FLOAT fMinDownwardZee = MIN_DOWNWARD_ZEE
	FLOAT fMaxShotPower = MAX_SHOT_POWER * GET_TENNIS_PLAYER_STRENGTH_BOOST(thisPlayer)
	FLOAT fMaxShotPowerScaled = fMaxShotPower
	
	VECTOR vBallPos = vBallPosAtHit																					// Get the ball's position
	VECTOR vBallAtNet = GET_CLOSEST_POINT_ON_LINE(vBallPos, thisProps.tennisCourt.vNetSideline[0], thisProps.tennisCourt.vNetSideline[1], FALSE)	// Project the ball to the net.
	FLOAT fBallDistToNet = VDIST(vBallPos, vBallAtNet)																// Get ball distance to net
	FLOAT fMaxDistance = VMAG(thisProps.tennisCourt.vCourtCorners[3] - thisProps.tennisCourt.vCourtCorners[0]) / 2	// Get the max distance to the net
	FLOAT fBallToNetAlpha = fBallDistToNet / fMaxDistance														// Turn distance into an alpha value
	IF IS_TENNIS_BALL_FLAG_SET(thisProps.tennisBall, TBF_SERVE_WAS_POWERFUL)
		bPowerVolley = FALSE
		bStrongSwing = FALSE
		bLob = TRUE
		SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_LOB)
	ENDIF
	IF bPowerVolley
		fMaxShotPower = MAX_SHOT_POWER_PV
		fMinShotPower = MIN_SHOT_POWER_PV
		fMaxUpwardZee = MAX_UPWARD_ZEE_PV
		fMinUpwardZee = MIN_UPWARD_ZEE_PV
		fMaxDownwardZee = MAX_DOWNWARD_ZEE_PV
		CPRINTLN(DEBUG_TENNIS, "Using Power Volley variables for force vector")
	ELIF bStrongSwing
		fMaxShotPower += STRONG_SWING_POWER_ADD
		CPRINTLN(DEBUG_TENNIS, "Using Strong Swing variables for force vector")
	ELIF bLob
		fMaxUpwardZee = MAX_UPWARD_ZEE_LOB
		fMinUpwardZee = MIN_UPWARD_ZEE_LOB
		fMaxDownwardZee = MAX_DOWNWARD_ZEE_LOB
		fMinDownwardZee = LERP_FLOAT(MIN_DOWNWARD_ZEE_LOB_NEAR, MIN_DOWNWARD_ZEE_LOB_FAR, fBallToNetAlpha)
		fMaxShotPowerScaled = MAX_POWER_HIGH_LOB_NEAR_NET
		fMinShotPower = MIN_SHOT_POWER_LOB
		fMaxShotPower = LERP_FLOAT(MAX_SHOT_POWER_LOB_NEAR, MAX_SHOT_POWER_LOB_FAR, fBallToNetAlpha)
		CPRINTLN(DEBUG_TENNIS, "Using Lob variables for force vector")
	ELIF bDropShot
		fMaxShotPower = MAX_POWER_LOW_FAR_DROP
		fMaxShotPowerScaled = MAX_POWER_HIGH_FAR_DROP
		fMaxDownwardZee = MAX_DOWNWARD_ZEE_DROP
		fMinShotPower = MIN_SHOT_POWER_DROP
		fMinDownwardZee = MIN_DOWNWARD_ZEE_DROP
		CPRINTLN(DEBUG_TENNIS, "Using Drop Shot variables for force vector")
	ENDIF
	
	FLOAT fHeightOfBall = vBallPos.z - thisProps.tennisCourt.vCenterCourt.z											// Get the height of the ball
	FLOAT fHeightAlpha = fHeightOfBall / OVERHEAD_HEIGHT															// Get alpha based on height
	FLOAT fMaxPowerOffHeight = LERP_FLOAT(fMaxShotPower, fMaxShotPowerScaled, fHeightAlpha)							// Used mainly for Lobs, scales power down as the height goes up.
	FLOAT fMaxPower = LERP_FLOAT(fMaxPowerOffHeight, fMaxShotPower, fBallToNetAlpha)								// LERP the maximum power off the distance to the net, only Lobs change the min value. No change for other shots.
	FLOAT fMinPower = LERP_FLOAT(fMinShotPower, fMaxPower, fBallToNetAlpha)											// LERP the minimum power off the distance to the net, the further out we are the harder we want to hit at all heights.
	FLOAT fPower = LERP_FLOAT(fMinPower, fMaxPower, fHeightAlpha)													// Get power off height
	
	IF thisPlayer.controlType <> COMPUTER
		vForce = CALCULATE_TENNIS_DESTINATION_VECTOR_HUMAN(thisPlayer, thisProps.tennisCourt, vBallPos, iPadX)
	ELSE
		INT iOppWho = 1 - ENUM_TO_INT(eThisPedID)
		vForce = CALCULATE_TENNIS_DESTINATION_VECTOR_AI(thisPlayer, thisProps, thisProps.tennisBall.vBallLaunchPos, thisProps.tennisPlayers[iOppWho].vPos, eThisPedID)
	ENDIF
	
	// Further from the net means shallower upward angles and shallower downward angles
	// Closer to the net means steeper upward angles and steeper downward angles
	// We select the angle from between the upward and the downward based on height
	FLOAT fUpwardZee = LERP_FLOAT(fMaxUpwardZee, fMinUpwardZee, fBallToNetAlpha)			// Steeper angles near the net for low shots
	FLOAT fDownwardZee = LERP_FLOAT(fMaxDownwardZee, fMinDownwardZee, fBallToNetAlpha)		// Steeper angles near the net for high shots
	vForce.z = LERP_FLOAT(fUpwardZee, fDownwardZee, fHeightAlpha)							// Select upward/downward angle from height
	vForce = vForce/VMAG(vForce)	// create unit vector
	vForce = vForce * fPower		// Multiply by our power
	
	#IF TENNIS_DEBUG_RECORDING
	#IF IS_DEBUG_BUILD
	IF thisProps.eTennisActivity <> TA_AI_VS_AI
		DEBUG_RECORD_SPHERE("vBallAtNet", vBallAtNet, 0.5, <<255,0,0>>)
		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fMaxDistance", fMaxDistance)
		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fBallDistToNet", fBallDistToNet)
		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fBallToNetAlpha", fBallToNetAlpha)
		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fMinPower", fMinPower)
		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fHeightOfBall", fHeightOfBall)
		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fHeightAlpha", fHeightAlpha)
		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fPower", fPower)
		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fMaxShotPower", fMaxShotPower)
		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fMaxShotPowerScaled", fMaxShotPowerScaled)
		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fMaxUpwardZee", fMaxUpwardZee)
		DEBUG_RECORD_ENTITY_FLOAT(thisPlayer.pIndex, "fMaxDownwardZee", fMaxDownwardZee)
	ENDIF
	#ENDIF
	#ENDIF
	
	RETURN vForce
ENDFUNC

PROC HANDLE_TENNIS_BALL_LAUNCH_AUDIO(TENNIS_PLAYER &thisPlayer, TENNIS_BALL &thisBall, BOOL bPowerVolley, BOOL bStrongSwing, BOOL bLobShot, BOOL bDropShot)
	IF GET_PLAYER_PED_ENUM(GET_TENNIS_PLAYER_INDEX(thisPlayer)) = CHAR_AMANDA AND GET_RANDOM_FLOAT_IN_RANGE() < 0.5
		PLAY_TENNIS_AMBIENT_SPEECH(thisPlayer, "TENNIS_EXHERT", GET_TENNIS_PLAYER_VOICE(thisPlayer))
	ENDIF
	
	INT iDuration
	BOOL bLobDrop = (bDropShot OR bLobShot)
	IF bPowerVolley OR bStrongSwing
		IF GET_TENNIS_BALL_SPIN(thisBall) = TBS_NO_SPIN
			PLAY_SOUND_FROM_ENTITY(-1, PICK_STRING(thisPlayer.controlType <> COMPUTER, "TENNIS_PLYR_SMASH_MASTER", "TENNIS_NPC_SMASH_MASTER"), GET_TENNIS_PLAYER_INDEX(thisPlayer))
		ELIF GET_TENNIS_BALL_SPIN(thisBall) = TBS_SLICE
			PLAY_SOUND_FROM_ENTITY(-1, PICK_STRING(thisPlayer.controlType <> COMPUTER, "TENNIS_PLYR_SMASH_BACKSLICE_MASTER", "TENNIS_NPC_SMASH_BACKSLICE_MASTER"),  GET_TENNIS_PLAYER_INDEX(thisPlayer))
		ELIF GET_TENNIS_BALL_SPIN(thisBall) = TBS_TOPSPIN
			PLAY_SOUND_FROM_ENTITY(-1, PICK_STRING(thisPlayer.controlType <> COMPUTER, "TENNIS_PLYR_SMASH_MASTER", "TENNIS_NPC_SMASH_MASTER"), GET_TENNIS_PLAYER_INDEX(thisPlayer))
		ENDIF
		iDuration = POWER_SHAKE_DURATION
	ELIF bLobDrop
		PLAY_SOUND_FROM_ENTITY(-1, PICK_STRING(thisPlayer.controlType <> COMPUTER, "TENNIS_PLYR_LOB_MASTER", "TENNIS_NPC_LOB_MASTER"),  GET_TENNIS_PLAYER_INDEX(thisPlayer))
		iDuration = LOB_SHAKE_DURATION
	ELSE
		IF GET_TENNIS_BALL_SPIN(thisBall) = TBS_NO_SPIN
			PLAY_SOUND_FROM_ENTITY(-1, PICK_STRING(thisPlayer.controlType <> COMPUTER, "TENNIS_PLYR_FOREARM_MASTER", "TENNIS_NPC_FOREARM_MASTER"), GET_TENNIS_PLAYER_INDEX(thisPlayer))
		ELIF GET_TENNIS_BALL_SPIN(thisBall) = TBS_SLICE
			PLAY_SOUND_FROM_ENTITY(-1, PICK_STRING(thisPlayer.controlType <> COMPUTER, "TENNIS_PLYR_BACKSLICE_MASTER", "TENNIS_NPC_BACKSLICE_MASTER"),  GET_TENNIS_PLAYER_INDEX(thisPlayer))
		ELIF GET_TENNIS_BALL_SPIN(thisBall) = TBS_TOPSPIN
			PLAY_SOUND_FROM_ENTITY(-1, PICK_STRING(thisPlayer.controlType <> COMPUTER, "TENNIS_PLYR_TOPSPIN_MASTER", "TENNIS_NPC_TOPSPIN_MASTER"),  GET_TENNIS_PLAYER_INDEX(thisPlayer))
		ENDIF
		iDuration = NORMAL_SHAKE_DURATION
	ENDIF
	
	// Shake the player's controller
	IF thisPlayer.controlType <> COMPUTER
		SET_CONTROL_SHAKE(PLAYER_CONTROL, iDuration, 256)
	ENDIF
ENDPROC

PROC GET_TENNIS_INPUT_AND_LAUNCH_BALL_WITH_FORCE_VECTOR(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER_ID eThisPedID, BOOL bPowerVolley, BOOL bStrongSwing, BOOL bLobShot, BOOL bDropShot #IF IS_TENNIS_MULTIPLAYER, TENNIS_CLIENT_BD &clientBD[], TENNIS_PARTICIPANT_INFO &info #ENDIF )
	INT x,y,dummy
	GET_ANALOG_STICK_VALUES(x,y,dummy,dummy)
	VECTOR vForce
	
	IF thisPlayer.controlType <> COMPUTER
		//Determine the forward/back modification from spin
		IF GET_TENNIS_PLAYER_STROKE(thisPlayer) = SHOT_TOPSPIN
			SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_TOPSPIN)
		ELIF GET_TENNIS_PLAYER_STROKE(thisPlayer) = SHOT_BACKSPIN
			SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_SLICE)
		ELSE
			RESET_TENNIS_BALL_SPIN(thisProps.tennisBall)
		ENDIF
	ELSE
		IF GET_TENNIS_PLAYER_STROKE(thisPlayer) <> SHOT_LOB AND GET_TENNIS_PLAYER_STROKE(thisPlayer) <> SHOT_DROP AND NOT bPowerVolley
			SET_TENNIS_BALL_SPIN(thisProps.tennisBall, CALCULATE_TENNIS_AI_BALL_SPIN(thisPlayer.playerAI.eDifficulty))
		ENDIF
	ENDIF
		
	vForce = CREATE_TENNIS_FORCE_VECTOR(thisPlayer, thisProps, x, eThisPedID, GET_TENNIS_BALL_POS(thisProps.tennisBall), bPowerVolley, bStrongSwing, bLobShot, bDropShot)
	
	CPRINTLN(DEBUG_TENNIS, thisPlayer.playerAI.texName, " Hit the ball")
	
	IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
		DETACH_ENTITY(thisProps.tennisBall.oBall)
	ENDIF
	
	LAUNCH_BALL_WITH_FORCE_VECTOR(thisProps, vForce)
	CLEAR_TENNIS_PLAYER_CONTROL_BITS(thisPlayer)
	CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_JUST_TASKED)
	SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_MUST_RELEASE)
	RESET_TENNIS_PLAYER_SWING_TIMER(thisPlayer)
	HANDLE_TENNIS_BALL_LAUNCH_AUDIO(thisPlayer, thisProps.tennisBall, bPowerVolley, bStrongSwing, bLobShot, bDropShot)
	
	//this resets the TAF_AI_HAS_SWUNG flag for the other player to allow them to hit the ball again.
	dummy = 1 - ENUM_TO_INT(eThisPedID)
	CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[dummy].playerAI, TAF_AI_HAS_SWUNG)
	CLEAR_TENNIS_AI_FLAG(thisProps.tennisPlayers[dummy].playerAI, TAF_PREVENT_DBL_SWING)
	//This resets the ShotType enum for the other player, the enum is used for prediction to indicate when a lob has been used.
	SET_TENNIS_PLAYER_STROKE(thisProps.tennisPlayers[dummy], SHOT_NORMAL)
	SET_TENNIS_PLAYER_TRUNCATE_FRAMES(thisPlayer, 0)
	
	#IF IS_TENNIS_MULTIPLAYER
	TENNIS_MP_BROADCAST_UPDATE_BALL(GET_TENNIS_BALL_POS(thisProps.tennisBall), vForce, GET_TENNIS_BALL_SPIN(thisProps.tennisBall), eThisPedID, clientBD, TBU_BALL_VELOCITY_VECTOR, GET_TENNIS_PLAYER_STROKE(thisPlayer), info)
	#ENDIF
ENDPROC

PROC LAUNCH_TENNIS_BALL_WITH_CLIENT_PRED_DATA(TENNIS_PLAYER & thisPlayer, TENNIS_GAME_PROPERTIES & thisProps, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eThisPedID, BOOL bPowerVolley, BOOL bStrongSwing, BOOL bLobShot, BOOL bDropShot, TENNIS_PARTICIPANT_INFO &info)
	CDEBUG2LN(DEBUG_TENNIS, "LAUNCH_TENNIS_BALL_WITH_CLIENT_PRED_DATA called")
	IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
		DETACH_ENTITY(thisProps.tennisBall.oBall)
	ENDIF
	SET_ENTITY_COORDS(thisProps.tennisBall.oBall, GET_TENNIS_CLIENT_PRED_BALL_POS(clientBD[info.iPlayers[eThisPedID]]))
	LAUNCH_BALL_WITH_FORCE_VECTOR(thisProps, GET_TENNIS_CLIENT_PRED_BALL_VEL(clientBD[info.iPlayers[eThisPedID]]))
	CLEAR_TENNIS_PLAYER_CONTROL_BITS(thisPlayer)
	RESET_TENNIS_PLAYER_SWING_TIMER(thisPlayer)
	HANDLE_TENNIS_BALL_LAUNCH_AUDIO(thisPlayer, thisProps.tennisBall, bPowerVolley, bStrongSwing, bLobShot, bDropShot)
	SET_TENNIS_PLAYER_TRUNCATE_FRAMES(thisPlayer, 0)

	SET_TENNIS_SWING_ANIM_PLAY_RATE(GET_TENNIS_PLAYER_INDEX(thisPlayer), 1.0)
	
	CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_PREVENT_DBL_SWING)
	CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_ALLOW_AI_ASSIST)
//	CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_CAN_CHARGE_STRONG_SWING)
	RESET_TENNIS_PLAYER_STRONG_SWING_HOLD_TIMER(thisPlayer)
	CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_WANTS_TO_SWING)
	IF thisPlayer.controlType <> COMPUTER
		SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_MUST_RELEASE)
	ENDIF
ENDPROC

FUNC BOOL VALIDATE_SWING_IS_NOT_LUNGE(TENNIS_SWING_STATES swing)
	IF swing = TSS_LUNGE_BACKHAND_HI
		RETURN FALSE
	ENDIF
	IF swing = TSS_LUNGE_BACKHAND_LOW
		RETURN FALSE
	ENDIF
	IF swing = TSS_LUNGE_BACKHAND_MID
		RETURN FALSE
	ENDIF
	IF swing = TSS_LUNGE_FOREHAND_HI
		RETURN FALSE
	ENDIF
	IF swing = TSS_LUNGE_FOREHAND_LOW
		RETURN FALSE
	ENDIF
	IF swing = TSS_LUNGE_FOREHAND_MID
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks that the ped want sot 
/// PARAMS:
///    thisPlayer - 
///    vBallPos - 
/// RETURNS:
///    
FUNC BOOL VALIDATE_WILD_SWING(TENNIS_PLAYER &thisPlayer, TENNIS_PLAYER &otherPlayer, TENNIS_BALL &thisBall, VECTOR vCenterCourt, FLOAT &fYDist)
	IF IS_ENTITY_ATTACHED(thisBall.oBall)
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_PLAYER_STROKE(otherPlayer) = SHOT_LOB AND GET_TENNIS_BALL_BOUNCE_COUNT(thisBall) <= 1
		RETURN FALSE
	ENDIF
	
	IF thisPlayer.ControlType <> COMPUTER
		IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_WANTS_TO_SWING)
			RETURN FALSE
		ENDIF
		IF VDIST2(thisPlayer.vPos, GET_TENNIS_BALL_POS(thisBall)) > WILD_SWING_RADIUS_PLAYER * WILD_SWING_RADIUS_PLAYER
			RETURN FALSE
		ENDIF
		IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_PREVENT_DBL_SWING)
//			CPRINTLN(DEBUG_TENNIS, "VALIDATE_WILD_SWING: TAF_PREVENT_DBL_SWING is set.")
			RETURN FALSE
		ENDIF
		fYDist = 0
		fYDist = DOT_PRODUCT(thisPlayer.vMyForward, GET_TENNIS_BALL_POS(thisBall) - thisPlayer.vPos)
		#IF IS_DEBUG_BUILD
		#IF TENNIS_DEBUG_RECORDING
			TEXT_LABEL_31 texDepthCheck = "WildSwingDepthCheck_"
			texDepthCheck += ROUND(fYDist * 100)
			texDepthCheck += "cm"
			VECTOR vLineEnd = thisPlayer.vPos + (thisPlayer.vMyForward * fYDist)
			DEBUG_RECORD_LINE(texDepthCheck, thisPlayer.vPos, vLineEnd, (<<255,255,255>>), (<<0,0,0>>))
		#ENDIF
		#ENDIF
		VECTOR vVel = thisBall.vBallVel
		VECTOR vBallPos = GET_TENNIS_BALL_POS(thisBall)
		vVel.z = 0
		vBallPos.z = vCenterCourt.z
		VECTOR vBallToCenter = vCenterCourt - vBallPos
		FLOAT fDotCourt = DOT_PRODUCT(vVel, vBallToCenter)
//		CPRINTLN(DEBUG_TENNIS, "VALIDATE_WILD_SWING :: fDotCourt=", fDotCourt, ", vVel=", vVel, ", vBallToCenter=", vBallToCenter)
		// Is the ball WILD_SWING_MIN_DIST_FAIL away and on our side of the court? No Wild Swing
		IF fYDist > WILD_SWING_MIN_DIST_FAIL AND fDotCourt < 0
			RETURN FALSE
		ENDIF
		
		// Is the ball on the other side of the court
		IF fYDist > WILD_SWING_MIN_DIST_FAIL AND fYDist < WILD_SWING_MAX_DIST_FAIL
			RETURN FALSE
		ENDIF
		
		// Return TRUE early because if the ball is WILD_SWING_MAX_DEPTH behind us we want to swing
		IF fYDist < WILD_SWING_MAX_DEPTH
			CPRINTLN(DEBUG_TENNIS, "VALIDATE_WILD_SWING: fYDist < WILD_SWING_MAX_DEPTH. fYDist=", fYDist)
			RETURN TRUE
		ENDIF
		
		IF NOT IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SCRIPT_RDOWN)
		AND NOT IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT)
		AND NOT IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
		AND NOT IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SCRIPT_RUP)
			RETURN FALSE
		ENDIF
	ELSE	// We need different criteria for COMPUTER controlled players.
		IF NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_CHECK_REACTS)
//			CPRINTLN(DEBUG_TENNIS, "VALIDATE_WILD_SWING: TAF_CHECK_REACTS is not set.")
			RETURN FALSE
		ENDIF
		IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_PREVENT_DBL_SWING)
//			CPRINTLN(DEBUG_TENNIS, "VALIDATE_WILD_SWING: TAF_PREVENT_DBL_SWING is set.")
			RETURN FALSE
		ENDIF
		IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_TASKED_ANIM)
//			CPRINTLN(DEBUG_TENNIS, "VALIDATE_WILD_SWING: TAF_TASKED_ANIM is set.")
			RETURN FALSE
		ENDIF
		FLOAT fDot = DOT_PRODUCT(thisPlayer.vMyForward, GET_TENNIS_BALL_POS(thisBall) - thisPlayer.vPos)
		#IF IS_DEBUG_BUILD
		#IF TENNIS_DEBUG_RECORDING
			TEXT_LABEL_31 texLine = "ToBall_"
			texLine += ROUND(fDot * 100)
			texLine += "cm"
			DEBUG_RECORD_LINE(texLine, thisPlayer.vPos, thisPlayer.vPos + (thisPlayer.vMyForward * fDot), (<<255,255,255>>), (<<0,0,0>>))
		#ENDIF
		#ENDIF
		IF fDot > WILD_SWING_AI_DIST_THRESHOLD
//			CPRINTLN(DEBUG_TENNIS, "VALIDATE_WILD_SWING: fDot(", fDot, ") > WILD_SWING_AI_DIST_THRESHOLD")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gates the player's ability to launch the ball with a new force vector
/// PARAMS:
///    thisPlayer - the player hitting the ball, thisProps.tennisPlayers[eThisPlayerID]
///    thisBall - the actual ball struct, thisProps.tennisBall
///    vCenterCourt - center of the playing court, thisProps.tennisCourt.vCenterCourt
///    bUnattached - If the ball is attached or not.
/// RETURNS:
///    TRUE if the player can hit the ball
FUNC BOOL VALIDATE_SUCCESSFUL_BALL_HIT(TENNIS_PLAYER &thisPlayer, TENNIS_BALL &thisBall, VECTOR vCenterCourt, BOOL bUnattached)
	IF NOT bUnattached
//		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: NOT bUnattached")
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_NO_SWING
//		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: GET_TENNIS_PLAYER_SWING_STATE(", thisPlayer.playerAI.texName, ") = TSS_NO_SWING")
		RETURN FALSE
	ENDIF
	IF NOT IS_PED_INJURED( GET_TENNIS_PLAYER_INDEX( thisPlayer ) ) AND GET_TENNIS_SWING_ANIM_SWUNG( GET_TENNIS_PLAYER_INDEX( thisPlayer ) )
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 texDisplay = thisPlayer.playerAI.texName
		texDisplay += " SWUNG"
		VECTOR vDisplay = << 0.1, ( ( ENUM_TO_INT( thisPlayer.eCourtSide ) * 0.0125 ) + 0.2 ), 0.0 >>
		DRAW_DEBUG_TEXT( texDisplay, vDisplay )
		#ENDIF
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: ", thisPlayer.playerAI.texName, " GET_TENNIS_SWING_ANIM_SWUNG" )
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_PLAYER_SWING_ANIM_FRAME_STAMP(thisPlayer) > GET_FRAME_COUNT()
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: ", thisPlayer.playerAI.texName, " GET_TENNIS_PLAYER_SWING_ANIM_FRAME_STAMP(", GET_TENNIS_PLAYER_SWING_ANIM_FRAME_STAMP(thisPlayer), ") > GET_FRAME_COUNT")
		RETURN FALSE
	ENDIF
	IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_TENNIS_SWING_EVENT_FLAG)	// This should never hit because of the frame stamp above
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: NOT IS_TENNIS_PLAYER_FLAG_SET(", thisPlayer.playerAI.texName, ", TPB_TENNIS_SWING_EVENT_FLAG)")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_WANTS_TO_SWING)
//		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: NOT IS_TENNIS_PLAYER_FLAG_SET(", thisPlayer.playerAI.texName, "), TPB_WANTS_TO_SWING)")
		RETURN FALSE
	ENDIF
	
	IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_MISSED_THE_BALL)
		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: IS_TENNIS_PLAYER_FLAG_SET(", thisPlayer.playerAI.texName, ", TPB_MISSED_THE_BALL)")
		RETURN FALSE
	ENDIF
	
	IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_TASKED_ANIM)
		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: IS_TENNIS_AI_FLAG_SET(", thisPlayer.playerAI.texName, ", TAF_TASKED_ANIM)")
		RETURN FALSE
	ENDIF
	
	VECTOR vBallPos = GET_TENNIS_BALL_POS(thisBall)
	
	IF DOT_PRODUCT(thisPlayer.vMyForward, vBallPos - vCenterCourt) > 0
		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: DOT_PRODUCT(", thisPlayer.playerAI.texName, ".vMyForward, vBallPos - vCenterCourt) > 0")
		RETURN FALSE
	ENDIF
	
	#IF IS_TENNIS_MULTIPLAYER
	
	IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_SWINGING_AIMLESSLY)
		CDEBUG3LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: IS_TENNIS_PLAYER_FLAG_SET(", thisPlayer.playerAI.texName, ", TPB_SWINGING_AIMLESSLY)")
		RETURN FALSE
	ENDIF
	
	///		Return TRUE to eliminate the false positives we'd get from checking the X, Y, and Z dots.
	///     by the time we're checking this func the other player has already begun the initial ball update so we have to hit it back to avoid a desync of data.
	RETURN TRUE
	
	#ENDIF
	
	FLOAT fRangeAllowance = NORMAL_RACKET_RANGE_ALLOWANCE
	// If we're diving we want less tolerance on the dive so the ball is only hit back when nearer to the racket.
	IF 	GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_DIVE_BACKHAND
	OR	GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_DIVE_FOREHAND
		fRangeAllowance = DIVE_RACKET_RANGE_ALLOWANCE
	// If we're truncating the animation to play a shorter swing then we'll shrink the racket size to accommodate for truncated translation and allow for misses.
	ELIF GET_TENNIS_PLAYER_TRUNCATE_FRAMES(thisPlayer) > TRUNCATED_SHRINK_THRESHOLD	
		fRangeAllowance = TRUNC_RACKET_RANGE_ALLOWANCE
	ENDIF
	FLOAT fDistanceCheck = VDIST2(vBallPos, GET_CENTRE_OF_BAT(thisPlayer))
	IF fDistanceCheck > fRangeAllowance * fRangeAllowance
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: Ball is too far from racket (", fDistanceCheck, " > ", fRangeAllowance * fRangeAllowance, ")")
		RETURN FALSE
	ENDIF
	
//	CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: Bulk checks passed. GET_FRAME_COUNT(", GET_FRAME_COUNT(), ")")
	
	// Do these checks now before we move the ball onto the Y plane of the racket.
	VECTOR vRacket = GET_CENTRE_OF_BAT(thisPlayer)
	VECTOR vRacketToBall = vBallPos - vRacket
	FLOAT fAbsX = DOT_PRODUCT(thisPlayer.vMyRight, vRacketToBall)	// this should just be the length of vBallPos - vRacket along the vMyRight axis
	FLOAT fAbsY = DOT_PRODUCT(thisPlayer.vMyForward, vRacketToBall)	// this should just be the length of vBallPos - vRacket along the vMyForward axis
	FLOAT fAbsZ = DOT_PRODUCT((<<0,0,1>>), vRacketToBall)			// Height of the ball above the racket
	VECTOR vNextRaqFrame = GET_PED_BONE_COORDS(GET_TENNIS_PLAYER_INDEX(thisPlayer), BONETAG_PH_L_HAND, (<< 0,0,0 >>))	// Where the racket head will be next frame.
	VECTOR vNextRaqToBall = vBallPos - vNextRaqFrame
	FLOAT fDotRaqNextX = DOT_PRODUCT(thisPlayer.vMyRight, vNextRaqToBall)
	FLOAT fDotXDiff = ABSF(fAbsX) - ABSF(fDotRaqNextX)
	fDotXDiff *= RACKET_X_DIST_SCALAR
	
	#IF IS_DEBUG_BUILD
	
	TEXT_LABEL_63 texDisplay = thisPlayer.playerAI.texName
	texDisplay += " vRacket"
	DRAW_DEBUG_TEXT( texDisplay, vRacket )
	texDisplay = thisPlayer.playerAI.texName
	texDisplay += " vNextRaqToBall"
	DRAW_DEBUG_TEXT( texDisplay, vNextRaqToBall )
	texDisplay = thisPlayer.playerAI.texName
	texDisplay += " vBallPos"
	DRAW_DEBUG_TEXT( texDisplay, vBallPos )
	
	#IF TENNIS_DEBUG_RECORDING
	
	VECTOR vXVisual = thisPlayer.vMyRight * fAbsX
	VECTOR vYVisual = thisPlayer.vMyForward * fAbsY
	VECTOR vZVisual = (<<0,0,1>>) * fAbsZ
	VECTOR vNextXVis = thisPlayer.vMyRight * fDotRaqNextX
	TEXT_LABEL_31 texName
	texName = "Racket"
	DEBUG_RECORD_SPHERE(texName, vRacket, 0.1, (<<0,255,0>>))
	texName = "RaqXVisual_"
	texName += ROUND(fAbsX * 100)
	texName += "cm"
	DEBUG_RECORD_LINE(texName, vRacket, vRacket + vXVisual, (<<0,255,0>>), (<<0,155,0>>))
	texName = "RaqYVisual_" 
	texName += ROUND(fAbsY * 100)
	texName += "cm"
	DEBUG_RECORD_LINE(texName, vRacket, vRacket + vYVisual, (<<0,255,0>>), (<<0,155,0>>))
	texName = "RaqZVisual_" 
	texName += ROUND(fAbsZ * 100)
	texName += "cm"
	DEBUG_RECORD_LINE(texName, vRacket, vRacket + vZVisual, (<<0,255,0>>), (<<0,155,0>>))
	DEBUG_RECORD_LINE("vRacketToBall", vRacket, vRacket + vRacketToBall, (<<0,255,0>>), (<<0,155,0>>))
	texName = "RaqNextXVis_"
	texName += ROUND(fDotRaqNextX * 100)
	texName += "cm"
	DEBUG_RECORD_LINE(texName, vNextRaqFrame, vNextRaqFrame + vNextXVis, (<<0,255,0>>), (<<0,155,0>>))
	
	#ENDIF
	#ENDIF
	
	IF NOT DOES_LINE_CROSS_BAT_PLANE_2(thisPlayer.oTennisRacket, vNextRaqFrame, thisPlayer.vMyForward, vBallPos, thisBall.vBallVel)
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: ", thisPlayer.playerAI.texName, ", DOES_LINE_CROSS_BAT_PLANE_2 failed ")
		RETURN FALSE
	ENDIF
	IF ABSF(fAbsX) > X_DIST_FROM_RACKET_THRESHOLD + fDotXDiff
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: ", thisPlayer.playerAI.texName, ", Racket, fAbsX > X_DIST_FROM_RACKET_THRESHOLD fAbsX=", fAbsX, ", new threshold=", X_DIST_FROM_RACKET_THRESHOLD + fDotXDiff)
		RETURN FALSE
	ENDIF	
	IF fAbsY < Y_DIST_BEHIND_RACKET_THRESHOLD
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: ", thisPlayer.playerAI.texName, ", Racket, fAbsY < Y_DIST_BEHIND_RACKET_THRESHOLD fAbsY=", fAbsY)
		RETURN FALSE
	ENDIF
	IF fAbsZ > Z_DIST_FROM_RACKET_THRESHOLD
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_SUCCESSFUL_BALL_HIT :: ", thisPlayer.playerAI.texName, ", Racket, fAbsZ > Z_DIST_FROM_RACKET_THRESHOLD fAbsZ=", fAbsZ)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	FLOAT fBallVelDot = DOT_PRODUCT(thisPlayer.vMyForward, thisBall.vBallVel)
	CDEBUG2LN(DEBUG_TENNIS, "fBallVelDot=", fBallVelDot, ", fAbsX=", fAbsX, ", fAbsY=", fAbsY, ", fAbsZ=", fAbsZ)
	#ENDIF
	
	SET_TENNIS_BALL_POS(thisBall, vBallPos)
	SET_ENTITY_COORDS(thisBall.oBall, vBallPos)
	
	RETURN TRUE
ENDFUNC

PROC SET_TENNIS_PLAYER_IN_GAME_REACTION(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER &thisPlayer, TENNIS_SCORES &playerScores[], TENNIS_PLAYER_ID eOtherID, TENNIS_PLAYER_ID eServingPlayer, SERVING_SIDE_ENUM eServingSide, VECTOR vBallPos , BOOL bOnTieBreak)
//	CPRINTLN(DEBUG_TENNIS, "SET_TENNIS_PLAYER_IN_GAME_REACTION called")
	IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_TENNIS_SWING_EVENT_FLAG)
		FLOAT fDot = DOT_PRODUCT(thisPlayer.vMyRight, vBallPos - thisPlayer.vPos)
		PED_INDEX ped = GET_TENNIS_PLAYER_INDEX(thisPlayer)
		STRING sAnimDict = "mini@tennis"
		STRING sAnim = PICK_STRING(fDot > 0, "react_ball_out", "react_ball_out_lt")
		BOOL bSideChange = FALSE
		#IF TENNIS_DEBUG_RECORDING
		#IF IS_DEBUG_BUILD
		DEBUG_RECORD_STRING("PLAY_TENNIS_SWING_ANIM", sAnim)
		#ENDIF
		#ENDIF
		CLEAR_PED_TASKS(ped)
		PLAY_TENNIS_SWING_ANIM(ped, sAnimDict, sAnim, 0, 1.0, TRUE)
		CPRINTLN(DEBUG_TENNIS, "SET_TENNIS_PLAYER_IN_GAME_REACTION :: Setting ", thisPlayer.playerAI.texName, " using PLAY_TENNIS_SWING_ANIM with ", sAnim, ", fDot=", fDot)
		SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_TENNIS_SWING_EVENT_FLAG)
		SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_TASKED_ANIM)
		SET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisPlayer, GET_GAME_TIMER() + TENNIS_PLAYER_REACT_DURATION)
		// Set the other player back to their position, this rally is over.
		IF IS_TENNIS_MULTIPLAYER = 0 AND NOT IS_TENNIS_AI_FLAG_SET(thisProps.tennisPlayers[eOtherID].playerAI, TAF_TASKED_NEW_RALLY) AND GET_GAME_TIMER() > GET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisProps.tennisPlayers[eOtherID])
			eServingSide = INT_TO_ENUM(SERVING_SIDE_ENUM, 1 - ENUM_TO_INT(eServingSide))
			IF bOnTieBreak
				bSideChange = (playerScores[0].iPointsWon + playerScores[1].iPointsWon + 1) % 6 = 0		// If a multiple of six points was won then we switch court sides
				IF IS_BIT_SET(playerScores[0].iPointsWon + playerScores[1].iPointsWon + 1, 0)			// If the number of points is odd then we switch servers
				OR bSideChange
					eServingSide = SERVING_FROM_RIGHT
					eServingPlayer = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eServingPlayer))
					CPRINTLN(DEBUG_TENNIS, "SET_TENNIS_PLAYER_IN_GAME_REACTION Changing Servers and Resetting serve side, bSideChange=", PICK_STRING(bSideChange, "TRUE", "FALSE"))
				ENDIF
			ELIF thisProps.eTennisActivity = TA_AI_VS_AI
				IF ( playerScores[0].iPointsWon >= 4 AND playerScores[0].iPointsWon - playerScores[1].iPointsWon >= 2 )
				OR ( playerScores[1].iPointsWon >= 4 AND playerScores[1].iPointsWon - playerScores[0].iPointsWon >= 2 )
					eServingPlayer = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eServingPlayer))
				ENDIF
			ENDIF
			
			IF eOtherID = eServingPlayer
				CDEBUG2LN(DEBUG_TENNIS, "SET_TENNIS_PLAYER_IN_GAME_REACTION Setting ", thisProps.tennisPlayers[eOtherID].playerAI.texName, " to serve")
				SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[eOtherID].playerAI, TAF_TASKED_NEW_RALLY)
				SET_TENNIS_PLAYER_SERVE(thisProps.tennisPlayers[eOtherID], thisProps, eServingSide, eOtherID, FALSE, default, FALSE, TRUE, bSideChange)
			ELSE
				CDEBUG2LN(DEBUG_TENNIS, "SET_TENNIS_PLAYER_IN_GAME_REACTION Setting ", thisProps.tennisPlayers[eOtherID].playerAI.texName, " to receive serve")
				SET_TENNIS_AI_FLAG(thisProps.tennisPlayers[eOtherID].playerAI, TAF_TASKED_NEW_RALLY)
				SET_TENNIS_PLAYER_RECEIVE_SERVE(thisProps.tennisPlayers[eOtherID], thisProps, eServingSide, eOtherID, FALSE, TRUE, bSideChange)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the ball is OOB and sets the player to the react instead of swinging
///    Does not work for Human players.
/// PARAMS:
///    thisPlayer - 
///    thisBall - 
///    thisCourt - 
///    vBallPos - vfuturepoint we're checking
///    vRight - 
/// RETURNS:
///    TRUE if the ped is set to react, FALSE otherwise
FUNC BOOL CHECK_TENNIS_PLAYER_OOB_REACTIONS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER &thisPlayer, TENNIS_SCORES &playerScores[], TENNIS_PLAYER_ID eOtherID, TENNIS_PLAYER_ID eServingPlayer, SERVING_SIDE_ENUM eServingSide, VECTOR vBallPos, BOOL bOnTieBreak)
//	IF thisPlayer.controlType <> COMPUTER
//		RETURN FALSE
//	ENDIF
	IF NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_CHECK_FOR_OOB)
		RETURN FALSE
	ENDIF
	IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_DONT_CHASE)
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) <> TSS_NO_SWING
		RETURN FALSE
	ENDIF
	//Predict where the tennis ball will land and if the ped is an AI tell them not to chase it if it is OOB
	//Do this after the bounce count check to make sure we're not checking where it lands after the bounce.
	CPRINTLN(DEBUG_TENNIS, "CHECK_TENNIS_PLAYER_OOB_REACTIONS :: Telling the AI to stop chasing the ball")
	SET_TENNIS_PLAYER_IN_GAME_REACTION(thisProps, thisPlayer, playerScores, eOtherID, eServingPlayer, eServingSide, vBallPos, bOnTieBreak)
	SET_TENNIS_AI_STATE(thisPlayer.playerAI, ASE_AI_MOVING_TO_IDLE)
	SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_DONT_CHASE)
	CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_CHECK_FOR_OOB)
	RETURN TRUE
ENDFUNC

FUNC BOOL VALIDATE_THIS_PLAYER_CAN_THINK_ABOUT_SWINGING(TENNIS_PLAYER &thisPlayer, VECTOR vBallPos, BOOL bUnattached)
	IF NOT bUnattached
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) <> TSS_NO_SWING
		RETURN FALSE
	ENDIF
	IF VDIST(thisPlayer.vPos, vBallPos) > 100.0
		RETURN FALSE
	ENDIF
	IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_CHECK_FOR_OOB)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL VALIDATE_BALL_SIM_SPEED_INCREASE(TENNIS_BALL &thisBall, TENNIS_COURT &thisCourt, TENNIS_PLAYER &thisPlayer, TENNIS_PLAYER &otherPlayer)
	IF thisPlayer.controlType = COMPUTER
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_BALL_SIM_SPEED_INCREASE :: thisPlayer.controlType = COMPUTER")
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_PLAYER_STROKE(otherPlayer) <> SHOT_LOB
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_BALL_SIM_SPEED_INCREASE :: ", otherPlayer.playerAI.texName, " GET_TENNIS_PLAYER_STROKE(otherPlayer) <> SHOT_LOB")
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_PLAYER_STROKE(thisPlayer) = SHOT_LOB
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_BALL_SIM_SPEED_INCREASE :: ", thisPlayer.playerAI.texName, " GET_TENNIS_PLAYER_STROKE(thisPlayer) = SHOT_LOB")
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_BALL_BOUNCE_COUNT(thisBall) >= 1
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_BALL_SIM_SPEED_INCREASE :: ", thisPlayer.playerAI.texName, " GET_TENNIS_BALL_BOUNCE_COUNT(thisBall) >= 1")
		RETURN FALSE
	ENDIF
	VECTOR vBallPos = GET_TENNIS_BALL_POS(thisBall)
	FLOAT fDiff = vBallPos.z - thisCourt.vCenterCourt.z
	IF (fDiff < 0.1)
		CDEBUG2LN(DEBUG_TENNIS, "VALIDATE_BALL_SIM_SPEED_INCREASE :: ", thisPlayer.playerAI.texName, ", (fDiff(", fDiff, ") < 0.1)")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_TENNIS_PLAYER_AI_STROKE(TENNIS_PLAYER &thisPlayer, TENNIS_COURT &thisCourt, TENNIS_PLAYER &oppPlayer)
	IF GET_AI_DIFFICULTY(thisPlayer.playerAI) <> AD_HARD
		IF GET_TENNIS_AI_STATE(thisPlayer.playerAI) = ASE_AI_CHARGING_TOPSPIN
			SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_NORMAL)
		ELIF GET_TENNIS_AI_STATE(thisPlayer.playerAI) = ASE_AI_CHARGING_LOB
			SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_LOB)
		ELSE
			CPRINTLN(DEBUG_TENNIS, "SET_TENNIS_PLAYER_AI_STROKE :: Setting default")
			SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_NORMAL)
		ENDIF
	ELSE
		FLOAT fDist1, fDist2, fDistDiff, fDropThreshold, fDropRoll		
		VECTOR vAdd = (thisCourt.vCourtCorners[1] - thisCourt.vCourtCorners[0]) * 0.5
		VECTOR vEnd1 = thisCourt.vCourtCorners[0] + vAdd
		VECTOR vEnd2 = thisCourt.vCourtCorners[3] + vAdd
		fDist1 = VDIST(vEnd1, oppPlayer.vPos)
		fDist2 = VDIST(vEnd2, oppPlayer.vPos)
		fDistDiff = ABSF(fDist1 - fDist2)
		fDropThreshold = LERP_FLOAT(LOW_AI_DROPLOB_CHANCE, HIGH_AI_DROPLOB_CHANCE, (fDistDiff/AI_DROP_MAX))
		fDropRoll = GET_RANDOM_FLOAT_IN_RANGE()
		CPRINTLN(DEBUG_TENNIS, "HARD AI Reading the player? fDropThreshold=", fDropThreshold, ", fDropRoll=", fDropRoll, ", fDistDiff=", fDistDiff, ", fDist1=", fDist1, ", fDist2=", fDist2)
		IF fDropRoll < fDropThreshold
			SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_LOB)
		ELSE
			SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_NORMAL)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF AI_DEBUG_SWING_LOB
			SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_LOB)
		ENDIF
	#ENDIF
ENDPROC

PROC PERFORM_TENNIS_PLAYER_PREDICTION_FULL(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, VECTOR &vBallPos, VECTOR &vBallVel, FLOAT &fBallAirTime, TENNIS_BALL_SPIN &eSpin, FLOAT &fSpinTimer, INT &iBounces)
	INT p = 0
	WHILE p < MAX_FUTURE_POINTS
		TENNIS_NET_BOUNCE_ENUM eBounce = UPDATE_BALL_SIMULATOR(thisProps.tennisCourt, vBallPos, vBallVel, thisProps.vForward, fBallAirTime, eSpin, fSpinTimer, iBounces, FALSE, THIRTY_FPS, default, NET_PREDICTION_FUDGE_VALUE)
		IF eBounce = TNB_GROUND_BOUNCE
			IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_CHECK_FOR_OOB) AND GET_TENNIS_PLAYER_REACT_TIMER(thisPlayer) = 0
				RESET_TENNIS_PLAYER_REACT_TIMER(thisPlayer)
				SET_TENNIS_PLAYER_REACT_LIMIT(thisPlayer, TO_FLOAT(p) * GET_FRAME_TIME())
			ENDIF
		ENDIF
		IF NOT IS_VECTOR_ZERO(vBallPos)
			thisProps.vFuturePoints[p] = vBallPos
		ELSE
			thisProps.vFuturePoints[p] = GET_TENNIS_BALL_POS(thisProps.tennisBall)
			CPRINTLN(DEBUG_TENNIS, "Ball is at origin, vFuturePoints[", p, "]=", thisProps.vFuturePoints[p])
		ENDIF
		p++
	ENDWHILE
	CACHE_TENNIS_PLAYER_PREDICTION_VARIABLES(thisPlayer, vBallVel, fBallAirTime, eSpin, fSpinTimer, iBounces)
	IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_REDUCE_PREDICTION_CALC) #IF IS_DEBUG_BUILD AND OPTIMIZE_PREDICTION #ENDIF
		SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_REDUCE_PREDICTION_CALC)
		thisProps.tennisBall.fPredTimeRemainder = THIRTY_FPS
	ENDIF
ENDPROC

PROC UPDATE_TENNIS_PLAYER_PREDICTION(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, VECTOR &vBallPos, VECTOR &vBallVel, FLOAT &fBallAirTime, TENNIS_BALL_SPIN &eSpin, FLOAT &fSpinTimer, INT &iBounces)
	INT p = 0
	thisProps.tennisBall.fPredTimeRemainder -= GET_FRAME_TIME()
	WHILE (thisProps.tennisBall.fPredTimeRemainder <= 0)
		thisProps.tennisBall.fPredTimeRemainder += THIRTY_FPS
		FOR p=0 TO MAX_FUTURE_POINTS-2
			thisProps.vFuturePoints[p] = thisProps.vFuturePoints[p+1]
		ENDFOR
		vBallPos = thisProps.vFuturePoints[p]
		SET_TENNIS_PLAYER_PREDICTION_VARIABLES_FROM_CACHE(thisPlayer, vBallVel, fBallAirTime, eSpin, fSpinTimer, iBounces)
		TENNIS_NET_BOUNCE_ENUM eBounce = UPDATE_BALL_SIMULATOR(thisProps.tennisCourt, vBallPos, vBallVel, thisProps.vForward, fBallAirTime, eSpin, fSpinTimer, iBounces, FALSE, THIRTY_FPS, default, NET_PREDICTION_FUDGE_VALUE)
		IF eBounce = TNB_GROUND_BOUNCE
			IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_CHECK_FOR_OOB) AND GET_TENNIS_PLAYER_REACT_TIMER(thisPlayer) = 0
				RESET_TENNIS_PLAYER_REACT_TIMER(thisPlayer)
				SET_TENNIS_PLAYER_REACT_LIMIT(thisPlayer, TO_FLOAT(p) * GET_FRAME_TIME())
			ENDIF
		ENDIF
		IF NOT IS_VECTOR_ZERO(vBallPos)
			thisProps.vFuturePoints[p] = vBallPos
		ELSE
			thisProps.vFuturePoints[p] = GET_TENNIS_BALL_POS(thisProps.tennisBall)
			CPRINTLN(DEBUG_TENNIS, "Ball is at origin, vFuturePoints[", p, "]=", thisProps.vFuturePoints[p])
		ENDIF
		CACHE_TENNIS_PLAYER_PREDICTION_VARIABLES(thisPlayer, vBallVel, fBallAirTime, eSpin, fSpinTimer, iBounces)
	ENDWHILE
ENDPROC

FUNC TENNIS_BALL_SPIN TENNIS_PREDICT_BALL_SPIN(TENNIS_PLAYER &thisPlayer)
	SWITCH GET_TENNIS_PLAYER_STROKE(thisPlayer)
		CASE SHOT_BACKSPIN		RETURN TBS_SLICE
		CASE SHOT_TOPSPIN		RETURN TBS_TOPSPIN
		DEFAULT					RETURN TBS_NO_SPIN
	ENDSWITCH
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisPlayer - 
///    thisProps - 
///    clientBD - []
///    eThisPedID - 
///    vBestBallOut - 
PROC TENNIS_PREDICT_SWING_AND_SEND_BROADCAST(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_CLIENT_BD &clientBD[], TENNIS_PLAYER_ID eThisPedID, VECTOR vBestBallOut, TENNIS_PARTICIPANT_INFO &info)
	BOOL bLobShot, bDropShot, bPowerVolley, bStrongSwing
	TENNIS_PLAYER_ID eOppPlayer = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eThisPedID))

	// If the shot is a Lob/Drop shot then we want to reset the spin. If it's a lob then we want to use the lob grade.
	IF GET_TENNIS_PLAYER_STROKE(thisPlayer) = SHOT_LOB
		RESET_TENNIS_BALL_SPIN(thisProps.tennisBall)
		IF IS_TENNIS_PLAYER_IN_SERVICE_BOXES(thisProps.tennisPlayers[eOppPlayer], thisProps.tennisCourt, thisProps.vRight)
			CPRINTLN(DEBUG_TENNIS, "TENNIS_PREDICT_SWING_AND_SEND_BROADCAST: ", thisPlayer.playerAI.texName, " Using LD_LOB")
			bLobShot = TRUE
		ELSE
			CPRINTLN(DEBUG_TENNIS, "TENNIS_PREDICT_SWING_AND_SEND_BROADCAST: ", thisPlayer.playerAI.texName," Using DropShot")
			bDropShot = TRUE
			SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_DROP)	// Change the stroke to Drop to avoid weird camera incoming stuff and sim speed increases
		ENDIF
	ELIF thisPlayer.controlType <> COMPUTER AND HAS_TENNIS_PLAYER_ACTIVATED_STRONG_SWING(thisPlayer) AND CAN_TENNIS_PLAYER_USE_STRONG_SWING(thisPlayer)	//TODO: Predict if the player can use the swing
		// do Strong Swing stuff to the serveGrade here.
		CPRINTLN(DEBUG_TENNIS, "TENNIS_PREDICT_SWING_AND_SEND_BROADCAST: ", thisPlayer.playerAI.texName, " Using Strong Swing")
		bStrongSwing = TRUE
	// If it's not a lob and not an intended Strong Swing check if we get a more powerful swing!
	ELIF CAN_TENNIS_PLAYER_USE_POWER_VOLLEY(thisPlayer, thisProps.tennisCourt, thisProps.vRight, vBestBallOut.z, GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall))	// TODO: predict if the ball will bounce
		bPowerVolley = TRUE
		CPRINTLN(DEBUG_TENNIS, "TENNIS_PREDICT_SWING_AND_SEND_BROADCAST: ", thisPlayer.playerAI.texName, " Using ePowerVolley")
	ENDIF
	
	INT x,dummy
	GET_ANALOG_STICK_VALUES(x,dummy,dummy,dummy)
	VECTOR vPredBallVel = CREATE_TENNIS_FORCE_VECTOR(thisPlayer, thisProps, x, eThisPedID, vBestBallOut, bPowerVolley, bStrongSwing, bLobShot, bDropShot)

	TENNIS_BALL_SPIN ePredSpin = TENNIS_PREDICT_BALL_SPIN(thisPlayer)
	SET_TENNIS_CLIENT_PRED_BALL_POS(clientBD[info.iPlayers[eThisPedID]], vBestBallOut)
	SET_TENNIS_CLIENT_PRED_BALL_VEL(clientBD[info.iPlayers[eThisPedID]], vPredBallVel)
	TENNIS_MP_BROADCAST_UPDATE_BALL(vBestBallOut, vPredBallVel, ePredSpin, eThisPedID, clientBD, TBU_INITIAL_POSITION, GET_TENNIS_PLAYER_STROKE(thisPlayer), info)
	CLEAR_TENNIS_CLIENT_FLAG(clientBD[info.iPlayers[eThisPedID]], TMC_BALL_LERP_TO_POS)
ENDPROC

FUNC BOOL HAS_TENNIS_BALL_PASSED_BROADCAST_POS(TENNIS_CLIENT_BD & clientBD, TENNIS_BALL &thisBall)
	VECTOR vBallPos = GET_TENNIS_BALL_POS(thisBall)
	FLOAT fFrameTime = GET_FRAME_TIME()
	VECTOR vBallVel = thisBall.vBallVel * fFrameTime
//	vBallPos += vBallVel
	VECTOR vPredToBall = vBallPos - GET_TENNIS_CLIENT_PRED_BALL_POS(clientBD)
	FLOAT fDot = DOT_PRODUCT(vBallVel, vPredToBall)
	#IF TENNIS_DEBUG_RECORDING
	VECTOR vPred = GET_TENNIS_CLIENT_PRED_BALL_POS(clientBD)
	DEBUG_RECORD_SPHERE("vPredBallPos", GET_TENNIS_CLIENT_PRED_BALL_POS(clientBD), 0.1, (<<255,0,0>>))
	DEBUG_RECORD_SPHERE("vBallPosNext", vBallPos, 0.1, (<<255,255,255>>))
	TEXT_LABEL_15 texDot = "fDot_"
	texDot += ROUND(fDot * 100)
	texDot += "cm"
	DEBUG_RECORD_LINE(texDot, GET_TENNIS_CLIENT_PRED_BALL_POS(clientBD), vBallPos, (<<255,0,0>>), (<<255,255,255>>))
	CDEBUG2LN(DEBUG_TENNIS, "HAS_TENNIS_BALL_PASSED_BROADCAST_POS :: fDot=", fDot, ", vPredBallPos=", vPred, ", vBallPosNext=", vBallPos)
	#ENDIF
	IF fDot >= 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL VALIDATE_TENNIS_SET_PLAYER_MISSED_FLAG(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER &thisPlayer, BOOL bUnattached)
	IF DOT_PRODUCT(thisPlayer.vMyForward, thisProps.tennisBall.vBallVel) > 0
		RETURN FALSE
	ENDIF
	IF DOT_PRODUCT(thisPlayer.vMyForward, thisProps.vFuturePoints[MISS_CHECK_INDEX] - thisPlayer.vPos) > 0 
		RETURN FALSE
	ENDIF
	IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) <> TSS_NO_SWING 
		RETURN FALSE
	ENDIF
	IF NOT bUnattached
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisPlayer - 
///    thisProps - 
///    eHitLast - 
///    eThisPedID - the ID of the player hitting the ball
/// RETURNS:
///    
FUNC BOOL UPDATE_TENNIS_PLAYER_STROKE(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_SCORES &playerScores[], TENNIS_PLAYER_ID eHitLast, TENNIS_PLAYER_ID eThisPedID, TENNIS_PLAYER_ID eServingPlayer, SERVING_SIDE_ENUM eServingSide, BOOL bOnTieBreak #IF IS_TENNIS_MULTIPLAYER , TENNIS_CLIENT_BD &clientBD[], TENNIS_PARTICIPANT_INFO &info #ENDIF )
	BOOL bBaseShot, bLob, bBackspin, bTopspin	//bStrongSwing
	TENNIS_SWING_STATES swing
	FLOAT fYDist
	BOOL bUnattached = DOES_ENTITY_EXIST(thisProps.tennisBall.oBall) AND NOT IS_ENTITY_ATTACHED(thisProps.tennisBall.oBall)
	// Cache off variables to run thru the simulator
	FLOAT fBallAirTime = GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall)
	FLOAT fSpinTimer = GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall)
	INT iBounces = GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall)
	TENNIS_BALL_SPIN eSpin = GET_TENNIS_BALL_SPIN(thisProps.tennisBall)
	VECTOR vBallPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
	VECTOR vBallVel = thisProps.tennisBall.vBallVel
	// Fill out the prediction array in order
	IF bUnattached AND eHitLast <> eThisPedID
		IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_REDUCE_PREDICTION_CALC)
			PERFORM_TENNIS_PLAYER_PREDICTION_FULL(thisPlayer, thisProps, vBallPos, vBallVel, fBallAirTime, eSpin, fSpinTimer, iBounces)
		ELIF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_REDUCE_PREDICTION_CALC)
			UPDATE_TENNIS_PLAYER_PREDICTION(thisPlayer, thisProps, vBallPos, vBallVel, fBallAirTime, eSpin, fSpinTimer, iBounces)
		ENDIF
	ENDIF	
	// Check AI OOB Reactions
	IF IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_CHECK_FOR_OOB)
		INCREASE_TENNIS_PLAYER_REACT_TIMER(thisPlayer)
		IF GET_TENNIS_PLAYER_REACT_TIMER(thisPlayer) >= GET_TENNIS_PLAYER_REACT_LIMIT(thisPlayer)
			CHECK_TENNIS_PLAYER_OOB_REACTIONS(thisProps, thisPlayer, playerScores, eHitLast, eServingPlayer, eServingSide, GET_TENNIS_BALL_POS(thisProps.tennisBall), bOnTieBreak)
			CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_CHECK_FOR_OOB)
			CLEAR_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_DONT_CHASE)
			RESET_TENNIS_PLAYER_REACT_TIMER(thisPlayer)
		ENDIF
	ENDIF
	//Check for Player Input	
	IF thisPlayer.controlType <> COMPUTER AND GET_GAME_TIMER() > GET_TENNIS_PLAYER_SWING_DELAY_STAMP(thisPlayer)
		bBaseShot =	IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RDOWN)
		bBackspin =	IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT)
		bTopspin =	IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
		bLob =		IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RUP)
		
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RDOWN)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RUP)
			IF CAN_TENNIS_PLAYER_USE_STRONG_SWING(thisPlayer)
				SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_CAN_CHARGE_STRONG_SWING)
			ENDIF
			IF VALIDATE_TENNIS_SET_PLAYER_MISSED_FLAG(thisProps, thisPlayer, bUnattached)
				SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_MISSED_THE_BALL)
				CPRINTLN(DEBUG_TENNIS, "UPDATE_TENNIS_PLAYER_STROKE :: SET_TENNIS_PLAYER_FLAG(", thisPlayer.playerAI.texName, ", TPB_MISSED_THE_BALL)")
				#IF TENNIS_DEBUG_RECORDING
				#IF IS_DEBUG_BUILD
				DEBUG_RECORD_SPHERE("FuturePointMissCheck", thisProps.vFuturePoints[MISS_CHECK_INDEX], 0.1, (<<0,0,0>>))
				#ENDIF
				#ENDIF
			ENDIF
			VECTOR vVelTemp, vForwardTemp
			vVelTemp = vBallVel
			vVelTemp.z = 0
			vForwardTemp = thisPlayer.vMyForward
			vForwardTemp.z = 0
			IF DOT_PRODUCT(vVelTemp, vForwardTemp) < 0
				SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_ALLOW_AI_ASSIST)
			ENDIF
		ENDIF
	ELIF GET_GAME_TIMER() > GET_TENNIS_PLAYER_SWING_DELAY_STAMP(thisPlayer)
		//Get AI Button forces if the ball isn't in the swing range for the AI
		IF VDIST2(GET_TENNIS_BALL_POS(thisProps.tennisBall), thisPlayer.vPos) > AI_SWING_RADIUS * AI_SWING_RADIUS
			GET_AI_BUTTON_FORCES(thisPlayer, thisProps, bBaseShot, bLob)	// AI chooses spin later based on difficulty, we just need whether it's a flat or high shot
		ENDIF
	ENDIF
	
	IF thisPlayer.controlType <> COMPUTER 
	AND IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_WANTS_TO_SWING)
	AND GET_TENNIS_PLAYER_SWING_TIMER(thisPlayer) > SWING_TIMEOUT
	AND GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_NO_SWING
		CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_WANTS_TO_SWING)
		RESET_TENNIS_PLAYER_SWING_TIMER(thisPlayer)
		CPRINTLN(DEBUG_TENNIS, "Swing timed out for ", thisPlayer.playerAI.texName)
		#IF IS_DEBUG_BUILD
		#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_STRING("SwingTimedOut", thisPlayer.playerAI.texName)
		#ENDIF
		#ENDIF
	ELIF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_WANTS_TO_SWING)
		INCREMENT_TENNIS_PLAYER_SWING_TIMER(thisPlayer)	// used for the swing timeout.
		IF bBaseShot OR bBackspin OR bTopspin
			INCREMENT_TENNIS_PLAYER_POWER_TIMER(thisPlayer)
		ELSE
			RESET_TENNIS_PLAYER_POWER_TIMER(thisPlayer)
		ENDIF
	ENDIF
	
	//Store the input if thisPlayer wants to swing
	IF bBaseShot OR bLob OR bBackspin OR bTopspin
		IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_NO_SWING 				// Our swing state indicates we're not swinging
		AND NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_WANTS_TO_SWING)			// We haven't set the TPB_WANTS_TO_SWING bit yet
		AND NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_MUST_RELEASE)				// And we're not currently holding the button from a previous swing
			//If we're not the last hit the ball then say we want to hit it
			SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_WANTS_TO_SWING)
			CPRINTLN(DEBUG_TENNIS, "TPB_WANTS_TO_SWING bit set for ", thisPlayer.playerAI.texName)
			IF bBaseShot
				SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_NORMAL)
			ELIF bLob
				SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_LOB)
			ELIF bBackspin
				SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_BACKSPIN)
			ELIF bTopspin
				SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_TOPSPIN)
			ENDIF
			
			IF thisPlayer.controlType <> COMPUTER
				FLOAT fTimeToPrediction
				VECTOR vDestTest
				vDestTest = GET_CLOSEST_POINT_FOR_TENNIS_SWING(thisPlayer.vPos, thisProps.tennisCourt, GET_TENNIS_BALL_POS(thisProps.tennisBall), thisProps.tennisBall.vBallVel,
					thisProps.vForward, GET_TENNIS_BALL_FLIGHT_TIMER(thisProps.tennisBall), GET_TENNIS_BALL_SPIN(thisProps.tennisBall), GET_TENNIS_BALL_SPIN_TIMER(thisProps.tennisBall), fTimeToPrediction)
				SET_AI_PREDICTION_POINT(thisPlayer.playerAI, GET_BEST_TENNIS_PREDICTION_FROM_OFFSET(thisPlayer, thisProps.vSwingStateOffsets, vDestTest, 
					thisProps.tennisCourt.vCenterCourt.z, fTimeToPrediction))
			ENDIF
					
			// begin debug block
			#IF IS_DEBUG_BUILD
			#IF TENNIS_DEBUG_RECORDING
			DEBUG_RECORD_STRING("SwingPressed", "TPB_WANTS_TO_SWING=ON!")
			#ENDIF
		ELSE
			#IF TENNIS_DEBUG_RECORDING
			#IF IS_DEBUG_BUILD
			IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_WANTS_TO_SWING)
				DEBUG_RECORD_STRING("Discarded", "TPB_WANTS_TO_SWING=TRUE")
			ELSE
				DEBUG_RECORD_STRING("Discarded", "TPB_WANTS_TO_SWING=FALSE")
			ENDIF
			#ENDIF
			#ENDIF
			IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_MUST_RELEASE)
				CDEBUG3LN(DEBUG_TENNIS, thisPlayer.playerAI.texName, " must release the button to swing again!")
			ENDIF
			IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) <> TSS_NO_SWING
				CDEBUG3LN(DEBUG_TENNIS, thisPlayer.playerAI.texName, " swing state is ", GET_STRING_FROM_TENNIS_SWING_STATE(GET_TENNIS_PLAYER_SWING_STATE(thisPlayer)))
			ENDIF
			#ENDIF
		ENDIF
		IF thisPlayer.controlType <> COMPUTER AND bBaseShot
			IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_CAN_CHARGE_STRONG_SWING)
				INCREASE_TENNIS_PLAYER_STRONG_SWING_HOLD_TIMER(thisPlayer)
			ENDIF
		ELIF NOT bBaseShot
			// Reset the strong swing hold timer
			RESET_TENNIS_PLAYER_STRONG_SWING_HOLD_TIMER(thisPlayer)
		ENDIF
		IF HAS_TENNIS_PLAYER_ACTIVATED_STRONG_SWING(thisPlayer)
			CDEBUG3LN(DEBUG_TENNIS, "UPDATE_TENNIS_PLAYER_STROKE: ", thisPlayer.playerAI.texName, " Setting Pad Shake, strong swing active")
			SET_CONTROL_SHAKE(PLAYER_CONTROL, SHAKE_DURATION_SLIDE, STRONG_SWING_SHAKE_FREQ)
		ENDIF
	ELSE
		IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_MUST_RELEASE) AND GET_GAME_TIMER() > GET_TENNIS_PLAYER_SWING_DELAY_STAMP(thisPlayer)
			CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_MUST_RELEASE)
		ENDIF
		// Reset the strong swing hold timer
		RESET_TENNIS_PLAYER_STRONG_SWING_HOLD_TIMER(thisPlayer)
	ENDIF
	
	#IF TENNIS_DEBUG_RECORDING
	#IF IS_DEBUG_BUILD
	IF thisProps.eTennisActivity <> TA_AI_VS_AI
		DEBUG_RECORD_ENTITY_SPHERE(thisPlayer.pIndex, "ReceivingPlayer", thisPlayer.vPos, 0.1, (<<1, 1, 1>>))
		DEBUG_RECORD_SPHERE("DestinationPos", GET_AI_PREDICTION_POINT(thisPlayer.playerAI), 0.1, (<<0, 1, 0>>))
	ENDIF
	#ENDIF
	#ENDIF
	
	IF eHitLast <> eThisPedID AND ((IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_WANTS_TO_SWING) AND thisPlayer.controlType <> COMPUTER) OR thisPlayer.controlType = COMPUTER)
		IF VALIDATE_THIS_PLAYER_CAN_THINK_ABOUT_SWINGING(thisPlayer, vBallPos, bUnattached)
			VECTOR vIdealNavPoint = GET_AI_PREDICTION_POINT(thisPlayer.playerAI)	//TODO: Plug back into navigation
			VECTOR vBestBallOut
			INT iFramesTrunc
			swing = GET_TENNIS_SWING_STATE_TO_PLAY(thisPlayer, thisProps, thisProps.vSwingStateOffsets, thisProps.phasesArray, thisProps.vFuturePoints, vIdealNavPoint, iFramesTrunc, vBestBallOut)
			IF (swing <> TSS_NO_SWING AND swing <> TSS_WAITING) AND (thisPlayer.controlType = COMPUTER OR (thisPlayer.ControlType <> COMPUTER AND IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_WANTS_TO_SWING)))
				IF NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_DONT_CHASE) AND NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_PREVENT_DBL_SWING)
					//Set the AI to want to shoot if they collide with a prediction point
					TENNIS_AI_CHOOSE_SHOT_NOW(thisPlayer.playerAI)

					//If the player wants to shoot then start up a swing based on the collision point
					SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, swing)
					SET_TENNIS_PLAYER_TRUNCATE_FRAMES(thisPlayer, iFramesTrunc)
					SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_PREVENT_DBL_SWING)
					SET_TENNIS_PLAYER_SWING_ANIM_FRAME_STAMP(thisPlayer, GET_FRAME_COUNT() + SWING_ANIM_MIN_DURATION)
					IF thisPlayer.controlType = COMPUTER
						SET_TENNIS_PLAYER_AI_STROKE(thisPlayer, thisProps.tennisCourt, thisProps.tennisPlayers[eHitLast])
						SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_WANTS_TO_SWING)
					ENDIF
					#IF IS_TENNIS_MULTIPLAYER
					IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_MISSED_THE_BALL)
					AND ((swing <> TSS_DIVE_BACKHAND AND swing <> TSS_DIVE_FOREHAND) OR GET_TENNIS_PLAYER_DIVE_SELECTION(thisPlayer) <> TENNIS_DIVE_NoDive)
						TENNIS_PREDICT_SWING_AND_SEND_BROADCAST(thisPlayer, thisProps, clientBD, eThisPedID, vBestBallOut, info)
					ENDIF
					#ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN(DEBUG_TENNIS, thisPlayer.playerAI.texName, ", TAF_DONT_CHASE=", PICK_STRING(IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_DONT_CHASE), "TRUE", "FALSE"), ", TAF_PREVENT_DBL_SWING=", PICK_STRING(IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_PREVENT_DBL_SWING), "TRUE", "FALSE"))
				#ENDIF
				ENDIF
			ELIF swing = TSS_NO_SWING AND VALIDATE_WILD_SWING(thisPlayer, thisProps.tennisPlayers[eHitLast], thisProps.tennisBall, thisProps.tennisCourt.vCenterCourt, fYDist)
				vBallPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
				IF fYDist > 0
					swing = GET_TENNIS_SWING_STATE_TO_PLAY_NO_FRILLS(thisPlayer, thisProps.vSwingStateOffsets, thisProps.phasesArray, vBallPos, vBestBallOut)
					SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, swing)
				ENDIF
				SET_TENNIS_PLAYER_SWING_DELAY_STAMP(thisPlayer, GET_GAME_TIMER() + SWING_DELAY_TIME)
				#IF IS_TENNIS_MULTIPLAYER
					CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_WANTS_TO_SWING)
				#ENDIF
				
				IF swing = TSS_NO_SWING OR fYDist <= 0
					CLEAR_PED_TASKS(GET_TENNIS_PLAYER_INDEX(thisPlayer))
					SET_TENNIS_PLAYER_IN_GAME_REACTION(thisProps, thisPlayer, playerScores, eHitLast, eServingPlayer, eServingSide, vBallPos, bOnTieBreak)
					CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_WANTS_TO_SWING)
				ENDIF
				// If the ball is within MIN_DIST_NO_FRILLS_SPEED_ADJUST meters then do the calculation for speeding it up, otherwise just play the anim
				IF fYDist < MIN_DIST_NO_FRILLS_SPEED_ADJUST
					// Find which prediction crosses the player plane
					INT iFutureCounter = 0
					FLOAT fFutureDot = DOT_PRODUCT(thisProps.vFuturePoints[iFutureCounter] - thisPlayer.vPos, thisPlayer.vMyForward)
					WHILE iFutureCounter < MAX_FUTURE_POINTS - 1 AND fFutureDot > 0
						iFutureCounter++
						fFutureDot = DOT_PRODUCT(thisProps.vFuturePoints[iFutureCounter] - thisPlayer.vPos, thisPlayer.vMyForward)
					ENDWHILE
					// Based on when it crosses speed up or slow down the animation accordingly
					FLOAT fFrameTot = TO_FLOAT(GET_TOTAL_FRAMES_ADJUSTED_FOR_FRAME_TIME(thisProps.phasesArray[swing].iFrameTot))
					INT iTruncation = FLOOR(thisProps.phasesArray[swing].fActive * fFrameTot) - iFutureCounter
					SET_TENNIS_PLAYER_TRUNCATE_FRAMES(thisPlayer, iTruncation)
					// moving this flag inside the check so we can still swing again if the ball is close and our anim as finished.
					SET_TENNIS_AI_FLAG(thisPlayer.playerAI, TAF_PREVENT_DBL_SWING)
				ENDIF
			ENDIF
			//Keep giving the AI new points to get to
			SET_AI_PREDICTION_POINT(thisPlayer.playerAI, vIdealNavPoint)
		ELIF thisPlayer.controlType <> COMPUTER AND GET_TENNIS_PLAYER_REACT_TIMER(thisPlayer) >= (GET_TENNIS_PLAYER_REACT_LIMIT(thisPlayer) / 2)
			IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_WANTS_TO_SWING)
				CHECK_TENNIS_PLAYER_OOB_REACTIONS(thisProps, thisPlayer, playerScores, eHitLast, eServingPlayer, eServingSide, GET_TENNIS_BALL_POS(thisProps.tennisBall), bOnTieBreak)
			ENDIF
		ENDIF
	ENDIF
	
	IF VALIDATE_SUCCESSFUL_BALL_HIT(thisPlayer, thisProps.tennisBall, thisProps.tennisCourt.vCenterCourt, bUnattached)
	#IF IS_TENNIS_MULTIPLAYER
	AND HAS_TENNIS_BALL_PASSED_BROADCAST_POS(clientBD[info.iPlayers[eThisPedID]], thisProps.tennisBall)
	#ENDIF
		BOOL bLobShot, bDropShot, bPowerVolley, bStrongSwing
		TENNIS_PLAYER_ID eOppPlayer = INT_TO_ENUM(TENNIS_PLAYER_ID, 1 - ENUM_TO_INT(eThisPedID))
		vBallPos = GET_TENNIS_BALL_POS(thisProps.tennisBall)
		
		// If the shot is a Lob/Drop shot then we want to reset the spin. If it's a lob then we want to use the lob grade.
		IF GET_TENNIS_PLAYER_STROKE(thisPlayer) = SHOT_LOB
			RESET_TENNIS_BALL_SPIN(thisProps.tennisBall)
			IF IS_TENNIS_PLAYER_IN_SERVICE_BOXES(thisProps.tennisPlayers[eOppPlayer], thisProps.tennisCourt, thisProps.vRight)
				CPRINTLN(DEBUG_TENNIS, "ePed ", thisPlayer.playerAI.texName, " Using LD_LOB")
				bLobShot = TRUE
			ELSE
				CPRINTLN(DEBUG_TENNIS, "ePed ", thisPlayer.playerAI.texName," Using DropShot")
				bDropShot = TRUE
				SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_DROP)	// Change the stroke to Drop to avoid weird camera incoming stuff and sim speed increases
			ENDIF
		ELIF thisPlayer.controlType <> COMPUTER AND HAS_TENNIS_PLAYER_ACTIVATED_STRONG_SWING(thisPlayer) AND CAN_TENNIS_PLAYER_USE_STRONG_SWING(thisPlayer)
			// do Strong Swing stuff to the serveGrade here.
			CPRINTLN(DEBUG_TENNIS, "UPDATE_TENNIS_PLAYER_STROKE: ", thisPlayer.playerAI.texName, " Using Strong Swing")
			RESET_TENNIS_PLAYER_STRONG_SWING_COOLDOWN(thisPlayer)
			CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_CAN_CHARGE_STRONG_SWING)
			RESET_TENNIS_PLAYER_STRONG_SWING_HOLD_TIMER(thisPlayer)
			bStrongSwing = TRUE
		// If it's not a lob and not an intended Strong Swing check if we get a more powerful swing!
		ELIF CAN_TENNIS_PLAYER_USE_POWER_VOLLEY(thisPlayer, thisProps.tennisCourt, thisProps.vRight, vBallPos.z, GET_TENNIS_BALL_BOUNCE_COUNT(thisProps.tennisBall))
			bPowerVolley = TRUE
			CPRINTLN(DEBUG_TENNIS, thisPlayer.playerAI.texName, " Using ePowerVolley")
		ENDIF
		
		#IF NOT IS_TENNIS_MULTIPLAYER
		IF VALIDATE_BALL_SIM_SPEED_INCREASE(thisProps.tennisBall, thisProps.tennisCourt, thisPlayer, thisProps.tennisPlayers[eOppPlayer])
			SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED)
		ENDIF
		#ENDIF
		
		GET_TENNIS_INPUT_AND_LAUNCH_BALL_WITH_FORCE_VECTOR(thisPlayer, thisProps, eThisPedID, bPowerVolley, bStrongSwing, bLobShot, bDropShot #IF IS_TENNIS_MULTIPLAYER , clientBD, info #ENDIF )
		
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		//Draw all the prediction points
		IF DRAW_PREDICTION_BALLS
			INT p = 0
			WHILE p < MAX_FUTURE_POINTS
				DRAW_DEBUG_SPHERE(thisProps.vFuturePoints[p], 0.05)
				p++
			ENDWHILE
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_TENNIS_PLAYER_SWINGING_AIMLESSLY(TENNIS_PLAYER &thisPlayer, TENNIS_BALL &thisBall)
	IF thisPlayer.controlType <> COMPUTER
		IF NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_TASKED_NEW_RALLY) AND NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_TASKED_ANIM)
			IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_NO_SWING
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RDOWN)
				OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT)
				OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
				OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RUP)
					CDEBUG2LN(DEBUG_TENNIS, "UPDATE_TENNIS_PLAYER_SWINGING_AIMLESSLY :: ", thisPlayer.playerAI.texName, " is swinging with ", GET_STRING_FROM_TENNIS_SWING_STATE(TSS_FOREHAND_TS_MID))
					VECTOR vToBall = GET_TENNIS_BALL_POS(thisBall) - thisPlayer.vPos
					FLOAT fDot = DOT_PRODUCT(thisPlayer.vMyRight, vToBall)
					IF fDot >= 0
						SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, TSS_FOREHAND_TS_MID)
					ELSE
						SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, TSS_BACKHAND_TS_MID)
					ENDIF
					SET_TENNIS_PLAYER_TRUNCATE_FRAMES(thisPlayer, 0)
					SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_SWINGING_AIMLESSLY)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisPlayer - 
///    thisProps - 
///    eServingSide - 
///    bMPGamePaused - Used only in MP for when the game is paused, when paused it should accept no input.
/// RETURNS:
///    
FUNC BOOL UPDATE_TENNIS_PLAYER_SERVE_TAP_TWICE(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, SERVING_SIDE_ENUM eServingSide, LAUNCH_DETAILS & serveGrade, BOOL bMPGamePaused=FALSE)
//	CPRINTLN(DEBUG_TENNIS, "UPDATE_TENNIS_PLAYER_SERVE_TAP_TWICE")
	IF DOES_ENTITY_EXIST(thisProps.tennisBall.oBall)
		
		INCREMENT_TENNIS_PLAYER_SWING_TIMER(thisPlayer)
		
		IF (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RDOWN) AND NOT bMPGamePaused) OR (GET_TENNIS_PLAYER_SWING_TIMER(thisPlayer) > AUTOMATIC_SWING AND thisProps.eTennisActivity = TA_MULTIPLAYER)
			CPRINTLN(DEBUG_TENNIS, PICK_STRING(GET_TENNIS_PLAYER_SWING_TIMER(thisPlayer) > AUTOMATIC_SWING, "Swinging Automatically", "Accept Button Pressed"))
			//Don't accept a second button press until the UI is actually ON and being updated.
			IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_SERVE_TOSS 
			AND IS_TIMER_STARTED(thisPlayer.meterDelayTimer) 
			AND (NOT IS_TIMER_PAUSED(thisPlayer.meterDelayTimer)) 
			AND GET_TIMER_IN_SECONDS(thisPlayer.meterDelayTimer) > UI_ON + NO_PRESS_BUFFER
			AND NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_STOP_METER)
				SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_STOP_METER)
				
				// Left Right influence
				INT iLeftX, iDummy
				GET_ANALOG_STICK_VALUES(iLeftX,iDummy,iDummy,iDummy)
				
				IF IS_USING_CURSOR(FRONTEND_CONTROL)
					iLeftX = ROUND(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X) * 256.0 ) - 128
				ENDIF
				
				SET_TENNIS_PLAYER_ANALOG_X_VALUE(thisPlayer, iLeftX)
			ENDIF
			
			//(re)start the timer because this is the first press of the button
			SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, TSS_SERVE_TOSS)
			IF NOT IS_TIMER_STARTED(thisPlayer.meterDelayTimer)
				RESTART_TIMER_NOW(thisPlayer.meterDelayTimer)
				CPRINTLN(DEBUG_TENNIS, "Restart the meter delay timer")
			ENDIF
			IF IS_TIMER_PAUSED(thisPlayer.meterDelayTimer)
				RESTART_TIMER_NOW(thisPlayer.meterDelayTimer)
				UNPAUSE_TIMER(thisPlayer.meterDelayTimer)
				CPRINTLN(DEBUG_TENNIS, "Restart the meter delay timer and unpause it")
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF SHOULD_SET_POWER_SHOT_FLAG
			SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_SERVE_WAS_POWERFUL)
		ENDIF
		#ENDIF
		
		IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_SERVE_EXECUTING)
			IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_STOP_METER)
				//All of these serve grades should be fair serves
				IF GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer) > SERVE_S_PLUS
					serveGrade = thisProps.grades[LD_SERVE_S_PLUS]
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SMASH_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 450, 256)
					IF GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer) >= SERVE_SIM_INCREASE AND IS_TENNIS_MULTIPLAYER = 0
					#IF IS_DEBUG_BUILD OR SHOULD_SET_POWER_SHOT_FLAG #ENDIF
						SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_INCREASE_SIM_SPEED)
						CPRINTLN(DEBUG_TENNIS, "Setting TBF_SERVE_WAS_POWERFUL in GET_TENNIS_PLAYER_POWER_TIMER")
						SET_TENNIS_BALL_FLAG(thisProps.tennisBall, TBF_SERVE_WAS_POWERFUL)
					ENDIF
					CPRINTLN(DEBUG_TENNIS, "Ball served, used LD_SERVE_S_PLUS, value=", GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer))
				ELIF GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer) > SERVE_S
					serveGrade = thisProps.grades[LD_SERVE_S]
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SMASH_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 250, 256)
					CPRINTLN(DEBUG_TENNIS, "Ball served, used LD_SERVE_S, value=", GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer))
				ELIF GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer) > SERVE_A
					serveGrade = thisProps.grades[LD_SERVE_A]
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 200, 256)
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SERVE_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
					CPRINTLN(DEBUG_TENNIS, " Ball served, used LD_SERVE_A, value=", GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer))
				ELIF GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer) > SERVE_B
					serveGrade = thisProps.grades[LD_SERVE_B]
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SERVE_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 150, 256)
					CPRINTLN(DEBUG_TENNIS, "Ball served, used LD_SERVE_B, value=", GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer))
				ELIF GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer) > SERVE_C
					serveGrade = thisProps.grades[LD_SERVE_C]
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SERVE_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 256)
					CPRINTLN(DEBUG_TENNIS, " served, used LD_SERVE_C, value=", GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer))
				ELIF GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer) >= SERVE_D
					serveGrade = thisProps.grades[LD_SERVE_D]
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SERVE_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 70, 256)
					CPRINTLN(DEBUG_TENNIS, "Ball served, used LD_SERVE_D, value=", GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer))
				ELSE
					CPRINTLN(DEBUG_TENNIS, "fPow: ", GET_TENNIS_PLAYER_POWER_TIMER(thisPlayer))
					SCRIPT_ASSERT("Serve Grade was missed, Contact Rob Pearsall")
				ENDIF
			ELSE	//The player didn't stop the meter/press a button, they now fault
				serveGrade = thisProps.grades[LD_SERVE_FAULT]
					PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_SERVE_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 50, 256)
				CPRINTLN(DEBUG_TENNIS, "Ball served, used LD_SERVE_FAULT")
			ENDIF
			
			IF NOT GET_LAUNCH_DETAILS_LEFT_RIGHT_SERVE(thisPlayer, thisProps, eServingSide, GET_TENNIS_PLAYER_ANALOG_X_VALUE(thisPlayer))
				serveGrade = thisProps.grades[LD_SERVE_OOB]
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF PLAYER_SERVE_OVERRIDE
					serveGrade = thisProps.grades[DEBUG_SERVE_OVERRIDE]
					CPRINTLN(DEBUG_TENNIS, "Forcing serve as LAUNCH_DETAIL ", DEBUG_SERVE_OVERRIDE, " for debug purposes")
				ENDIF
			#ENDIF
			
			DETACH_ENTITY(thisProps.tennisBall.oBall,FALSE)
			LAUNCH_BALL(thisProps, thisPlayer, serveGrade)
			
			TENNIS_PAUSE_TIMER(thisPlayer.meterDelayTimer)
			CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_STOP_METER)
			SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_NORMAL)
			RESET_TENNIS_PLAYER_POWER_TIMER(thisPlayer)
			RESET_TENNIS_PLAYER_SWING_TIMER(thisPlayer)
			
			RETURN TRUE
		ENDIF
		
		IF GET_TENNIS_PLAYER_SWING_TIMER(thisPlayer) > AUTOMATIC_SWING OR (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RDOWN) AND NOT bMPGamePaused)
			RESET_TENNIS_PLAYER_SWING_TIMER(thisPlayer)
			//CPRINTLN(DEBUG_TENNIS, "Resetting the fTimer")
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the players have finished their sequence tasks that move them back into position
/// PARAMS:
///    players - []
///    eSelfID - 
///    eOtherID - 
///    eServer - serving player
///    bWalkToPos - should be if this is AI_VS_AI or if the SP game flag is set
/// RETURNS:
///    
FUNC BOOL ARE_TENNIS_PLAYERS_IN_POSITION(TENNIS_PLAYER &players[], TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, TENNIS_PLAYER_ID eServer, TENNIS_ACTIVITY eActivity, BOOL bWalkToPos, INT iReceiverSeqProgress)
	BOOL bReturn = TRUE
	PED_INDEX pedSelf = GET_TENNIS_PLAYER_INDEX(players[eSelfID])
	PED_INDEX pedOther = GET_TENNIS_PLAYER_INDEX(players[eOtherID])
	IF NOT IS_PED_INJURED(pedSelf) 
	#IF NOT IS_TENNIS_MULTIPLAYER	AND GET_SCRIPT_TASK_STATUS(pedSelf, SCRIPT_TASK_PERFORM_SEQUENCE) <> FINISHED_TASK	#ENDIF
	#IF IS_TENNIS_MULTIPLAYER		AND (GET_SCRIPT_TASK_STATUS(pedSelf, SCRIPT_TASK_PERFORM_SEQUENCE) <> FINISHED_TASK ^ (IS_ENTITY_PLAYING_ANIM(pedSelf, "mini@tennis", "idle_2_serve") AND GET_ENTITY_ANIM_CURRENT_TIME(pedSelf, "mini@tennis", "idle_2_serve") >= 1.0) )	#ENDIF
		CDEBUG3LN(DEBUG_TENNIS, "ARE_TENNIS_PLAYERS_IN_POSITION :: IS_PED_INJURED(pedSelf)=", PICK_STRING(IS_PED_INJURED(pedSelf), "TRUE", "FALSE"))
		CDEBUG3LN(DEBUG_TENNIS, "ARE_TENNIS_PLAYERS_IN_POSITION :: GET_SEQUENCE_PROGRESS(pedSelf)=", GET_SEQUENCE_PROGRESS(pedSelf))
		CDEBUG3LN(DEBUG_TENNIS, "IS_ENTITY_PLAYING_ANIM(pedSelf, mini@tennis, idle_2_serve)=", PICK_STRING(IS_ENTITY_PLAYING_ANIM(pedSelf, "mini@tennis", "idle_2_serve"), "TRUE", "FALSE"))
		IF IS_ENTITY_PLAYING_ANIM(pedSelf, "mini@tennis", "idle_2_serve")
			CDEBUG3LN(DEBUG_TENNIS, "GET_ENTITY_ANIM_CURRENT_TIME(pedSelf, mini@tennis, idle_2_serve)=", GET_ENTITY_ANIM_CURRENT_TIME(pedSelf, "mini@tennis", "idle_2_serve"))
		ENDIF
		bReturn = FALSE
		IF IS_TENNIS_MODE( pedSelf )
			SET_TENNIS_PLAYER_FLAG( players[eSelfID], TPB_DROP_OUT_OF_TENNIS_MODE )
			CDEBUG2LN(DEBUG_TENNIS, "ARE_TENNIS_PLAYERS_IN_POSITION tennis mode failsafe hit for ", players[eSelfID].playerAI.texName )
		ENDIF
	ELIF NOT IS_PED_INJURED(pedSelf) AND (bWalkToPos OR eActivity = TA_AI_VS_AI) AND NOT IS_TENNIS_PLAYER_FLAG_SET(players[eSelfID], TPB_FORCED_UPDATE)
		IF eSelfID = eServer
			SET_UP_TENNIS_MOVE_NETWORK(players[eSelfID], "TennisServeSet", 0.0)
		ENDIF
		SET_TENNIS_PLAYER_TO_TENNIS_MODE(pedSelf, TRUE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSelf)
		SET_TENNIS_PLAYER_FLAG(players[eSelfID], TPB_FORCED_UPDATE)
		CDEBUG2LN(DEBUG_TENNIS, "ARE_TENNIS_PLAYERS_IN_POSITION ", players[eSelfID].playerAI.texName, " forced to tennis mode")
	ELIF IS_TENNIS_PLAYER_FLAG_SET(players[eSelfID], TPB_FORCED_UPDATE) AND eSelfID = eServer
		IF NOT IS_PED_INJURED(pedSelf) AND IS_TASK_MOVE_NETWORK_ACTIVE(pedSelf)
			//If we're in the intro, request that we go into the loop
			IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(pedSelf), "Intro") AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(pedSelf)
				REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(pedSelf, "running")
				CPRINTLN(DEBUG_TENNIS, players[eSelfID].playerAI.texName, " requested running state in TennisServeSetMoVE network")
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bReturnEarly = eOtherID <> eServer AND iReceiverSeqProgress >= (GET_TENNIS_PLAYER_SEQUENCE_THRESHOLD(players[eOtherID]) - 1) AND bReturn AND IS_TENNIS_MULTIPLAYER = 0
	IF NOT IS_PED_INJURED(pedOther) 
	#IF NOT IS_TENNIS_MULTIPLAYER	AND (GET_SCRIPT_TASK_STATUS(pedOther, SCRIPT_TASK_PERFORM_SEQUENCE) <> FINISHED_TASK AND NOT bReturnEarly)	#ENDIF
	#IF IS_TENNIS_MULTIPLAYER		AND ((GET_SCRIPT_TASK_STATUS(pedOther, SCRIPT_TASK_PERFORM_SEQUENCE) <> FINISHED_TASK ^ (IS_ENTITY_PLAYING_ANIM(pedOther, "mini@tennis", "idle_2_serve") AND GET_ENTITY_ANIM_CURRENT_TIME(pedOther, "mini@tennis", "idle_2_serve") >= 1.0) ) AND NOT bReturnEarly)	#ENDIF
		CDEBUG3LN(DEBUG_TENNIS, "ARE_TENNIS_PLAYERS_IN_POSITION :: IS_PED_INJURED(pedOther)=", PICK_STRING(IS_PED_INJURED(pedOther), "TRUE", "FALSE"))
		CDEBUG3LN(DEBUG_TENNIS, "ARE_TENNIS_PLAYERS_IN_POSITION :: GET_SEQUENCE_PROGRESS(pedOther)=", GET_SEQUENCE_PROGRESS(pedOther))
		CDEBUG3LN(DEBUG_TENNIS, "IS_ENTITY_PLAYING_ANIM(pedOther, mini@tennis, idle_2_serve)=", PICK_STRING(IS_ENTITY_PLAYING_ANIM(pedOther, "mini@tennis", "idle_2_serve"), "TRUE", "FALSE"))
		IF IS_ENTITY_PLAYING_ANIM(pedOther, "mini@tennis", "idle_2_serve")
			CDEBUG3LN(DEBUG_TENNIS, "GET_ENTITY_ANIM_CURRENT_TIME(pedOther, mini@tennis, idle_2_serve)=", GET_ENTITY_ANIM_CURRENT_TIME(pedOther, "mini@tennis", "idle_2_serve"))
		ENDIF
		IF IS_TENNIS_MODE( pedOther )
			SET_TENNIS_PLAYER_FLAG( players[eOtherID], TPB_DROP_OUT_OF_TENNIS_MODE )
			CDEBUG2LN(DEBUG_TENNIS, "ARE_TENNIS_PLAYERS_IN_POSITION tennis mode failsafe hit for ", players[eOtherID].playerAI.texName )
		ENDIF
		bReturn = FALSE
	#IF NOT IS_TENNIS_MULTIPLAYER
	ELIF NOT IS_PED_INJURED(pedOther) AND (bWalkToPos OR eActivity = TA_MULTIPLAYER OR eActivity = TA_AI_VS_AI) AND NOT IS_TENNIS_PLAYER_FLAG_SET(players[eOtherID], TPB_FORCED_UPDATE)
	AND GET_SCRIPT_TASK_STATUS(pedOther, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
		IF eOtherID = eServer
			SET_UP_TENNIS_MOVE_NETWORK(players[eOtherID], "TennisServeSet", 0.0)
		ENDIF
		SET_TENNIS_PLAYER_TO_TENNIS_MODE(pedOther, TRUE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedOther)
		SET_TENNIS_PLAYER_FLAG(players[eOtherID], TPB_FORCED_UPDATE)
		CDEBUG2LN(DEBUG_TENNIS, "ARE_TENNIS_PLAYERS_IN_POSITION ", players[eOtherID].playerAI.texName, " forced to tennis mode")
	ELIF IS_TENNIS_PLAYER_FLAG_SET(players[eOtherID], TPB_FORCED_UPDATE) AND eOtherID = eServer
		IF NOT IS_PED_INJURED(pedOther) AND IS_TASK_MOVE_NETWORK_ACTIVE(pedOther)
			//If we're in the intro, request that we go into the loop
			IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(pedOther), "Intro") AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(pedOther)
				REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(pedOther, "running")
				CPRINTLN(DEBUG_TENNIS, players[eOtherID].playerAI.texName, " requested running state in TennisServeSetMoVE network")
			ENDIF
		ENDIF
	#ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

/// PURPOSE:
///    Monitor the opponent's sequence progress and put them back in tennis mode when they're in place.
///    DO NOT USE IN TENNIS MP
/// PARAMS:
///    players - 
///    eOtherID - 
///    eServer - 
///    eActivity - 
///    bWalkToPos - 
/// RETURNS:
///    
FUNC BOOL UPDATE_TENNIS_OPPONENT_WALK_BACK_PROGRESS(TENNIS_PLAYER &players[], TENNIS_PLAYER_ID eOtherID, TENNIS_PLAYER_ID eServer, TENNIS_ACTIVITY eActivity, BOOL bWalkToPos)
	IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(players[eOtherID])) 
	AND (GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(players[eOtherID]), SCRIPT_TASK_PERFORM_SEQUENCE) <> FINISHED_TASK)
		CDEBUG3LN(DEBUG_TENNIS, "UPDATE_TENNIS_OPPONENT_WALK_BACK_PROGRESS :: GET_SEQUENCE_PROGRESS=", GET_SEQUENCE_PROGRESS(GET_TENNIS_PLAYER_INDEX(players[eOtherID])))
		RETURN FALSE
	ELIF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(players[eOtherID])) AND (bWalkToPos OR eActivity = TA_MULTIPLAYER OR eActivity = TA_AI_VS_AI) AND NOT IS_TENNIS_PLAYER_FLAG_SET(players[eOtherID], TPB_FORCED_UPDATE)
		IF eOtherID = eServer
			SET_UP_TENNIS_MOVE_NETWORK(players[eOtherID], "TennisServeSet", 0.0)
		ENDIF
		SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(players[eOtherID]), TRUE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_TENNIS_PLAYER_INDEX(players[eOtherID]))
		SET_TENNIS_PLAYER_FLAG(players[eOtherID], TPB_FORCED_UPDATE)
		CDEBUG2LN(DEBUG_TENNIS, "UPDATE_TENNIS_OPPONENT_WALK_BACK_PROGRESS ", players[eOtherID].playerAI.texName, " forced to tennis mode")
	ELIF IS_TENNIS_PLAYER_FLAG_SET(players[eOtherID], TPB_FORCED_UPDATE) AND eOtherID = eServer
		IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(players[eOtherID])) AND IS_TASK_MOVE_NETWORK_ACTIVE(GET_TENNIS_PLAYER_INDEX(players[eOtherID]))
			//If we're in the intro, request that we go into the loop
			IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(GET_TENNIS_PLAYER_INDEX(players[eOtherID])), "Intro") AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(GET_TENNIS_PLAYER_INDEX(players[eOtherID]))
				REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(GET_TENNIS_PLAYER_INDEX(players[eOtherID]), "running")
				CPRINTLN(DEBUG_TENNIS, "UPDATE_TENNIS_OPPONENT_WALK_BACK_PROGRESS ", players[eOtherID].playerAI.texName, " requested running state in TennisServeSetMoVE network")
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisPlayer - 
///    thisProps - 
///    thisStatus - 
///    eSelfID - The ID of the player who is test hitting the ball
PROC SCRIPTED_TENNIS_BALL_HIT(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, LAUNCH_DETAILS &serveGrade, SHOT_TYPE_ENUM eShotType, VECTOR vHitFrom, BOOL bPlaySound=TRUE)
	DEBUG_SPAWN_BALL(thisProps, TRUE)
	DETACH_ENTITY(thisProps.tennisBall.oBall)
	SET_ENTITY_COORDS(thisProps.tennisBall.oBall, vHitFrom)
	
	CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[0], TPB_REDUCE_PREDICTION_CALC)
	CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[1], TPB_REDUCE_PREDICTION_CALC)
	
	IF eShotType = SHOT_TOPSPIN
		SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_TOPSPIN)
	ELIF eShotType = SHOT_BACKSPIN
		SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_SLICE)
	ELSE
		SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_NO_SPIN)
	ENDIF
	
	LAUNCH_BALL(thisProps, thisPlayer, serveGrade)
	
	SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_NORMAL)
	IF bPlaySound
		PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_FOREARM_MASTER", thisProps.tennisBall.oBall)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    
/// PARAMS:
///    thisPlayer - 
///    thisProps - 
///    thisStatus - 
///    eSelfID - The ID of the player who is test hitting the ball
PROC TEST_HIT(TENNIS_PLAYER &thisPlayer, TENNIS_GAME_PROPERTIES &thisProps, TENNIS_GAME_STATUS &thisStatus, TENNIS_PLAYER_ID eSelfID)
	VECTOR vCoord = thisPlayer.vPos + thisPlayer.vMyForward * TEST_FORWARD_SCALAR
	VECTOR vForce
	
	CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[0], TPB_REDUCE_PREDICTION_CALC)
	CLEAR_TENNIS_PLAYER_FLAG(thisProps.tennisPlayers[1], TPB_REDUCE_PREDICTION_CALC)
	
	vCoord.z = thisProps.tennisCourt.vCourtCorners[0].z + TEST_HEIGHT
	DETACH_ENTITY(thisProps.tennisBall.oBall)
	SET_ENTITY_COORDS(thisProps.tennisBall.oBall, vCoord)
	SET_TENNIS_BALL_POS(thisProps.tennisBall, GET_ENTITY_COORDS(thisProps.tennisBall.oBall))
	
	INT x,y,dummy
	GET_ANALOG_STICK_VALUES(x,y,dummy,dummy)
	
	vForce = CREATE_TENNIS_FORCE_VECTOR(thisPlayer, thisProps, x, eSelfID, vCoord, TEST_POWER_VOLLEY, TEST_STRONG_SWING, TEST_LOB, TEST_DROP_SHOT)
	
	IF y < -120
		SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_TOPSPIN)
	ELIF y > 120
		SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_SLICE)
	ELSE
		SET_TENNIS_BALL_SPIN(thisProps.tennisBall, TBS_NO_SPIN)
	ENDIF
	
	SET_TENNIS_STATUS_HIT_LAST(thisStatus, eSelfID)
	
	IF DEBUG_LAUNCH_BALL_FROM_VECTORS
		SET_ENTITY_COORDS(thisProps.tennisBall.oBall, DEBUG_LAUNCH_POS)
		SET_TENNIS_BALL_POS(thisProps.tennisBall, DEBUG_LAUNCH_POS)
		vForce = DEBUG_LAUNCH_VEL
	ENDIF
	
	CPRINTLN(DEBUG_TENNIS, "vCoord: ", vCoord)
//	LAUNCH_BALL(thisProps, thisPlayer, serveGrade)
	LAUNCH_BALL_WITH_FORCE_VECTOR(thisProps, vForce)
	SET_TENNIS_PLAYER_STROKE(thisPlayer, SHOT_NORMAL)
	IF thisPlayer.controlType <> COMPUTER
		PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_FOREARM_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
	ELSE
		PLAY_SOUND_FROM_ENTITY(-1, "TENNIS_PLYR_FOREARM_MASTER", GET_TENNIS_PLAYER_INDEX(thisPlayer))
	ENDIF
ENDPROC
#ENDIF
