USING "minigames_helpers.sch"
USING "minigame_UIInputs.sch"
USING "script_player.sch"
USING "rage_builtins.sch"
USING "globals.sch"

USING "tennis.sch"
USING "tennis_player.sch"
USING "tennis_debug.sch"
USING "tennis_server.sch"


PROC SET_TENNIS_MP_SCORED_LAST(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_SERVER_BD &serverBD, TENNIS_PLAYER_ID eWhoScored)
	SET_TENNIS_MP_SCORED_LAST_private(serverBD, eWhoScored)
	thisProps.eCachedScoredLast = eWhoScored
ENDPROC

FUNC BOOL VALIDATE_AMANDA_IS_OPP_AND_LOSING_OVERALL(TENNIS_GAME_UI_DATA &thisUIData, TENNIS_GAME_STATUS &thisStatus, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID)
	IF thisUIData.playerChars[eOtherID] <> CHAR_AMANDA
		RETURN FALSE
	ENDIF
	
	INT iCurrentSet = GET_TENNIS_STATUS_CURRENT_SET(thisStatus)
	TENNIS_MATCH_RULESET eRules = GET_TENNIS_MATCH_RULESET(thisStatus)
	CPRINTLN(DEBUG_TENNIS, "VALIDATE_AMANDA_IS_OPP_AND_LOSING_OVERALL called:")
	
	// Sets as a defining factor
	IF (eRules = TMR_SETS 			AND thisStatus.playerScores[eOtherID].iSetsWon < thisStatus.playerScores[eSelfID].iSetsWon)
	OR (eRules <> TMR_SETS			AND thisStatus.playerScores[eOtherID].iGamesWon[iCurrentSet] < thisStatus.playerScores[eSelfID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "eRules=", GET_STRING_FROM_TENNIS_RULESET(eRules))
		CPRINTLN(DEBUG_TENNIS, "AmSets=", thisStatus.playerScores[eOtherID].iSetsWon)
		CPRINTLN(DEBUG_TENNIS, "TheirSets=", thisStatus.playerScores[eSelfID].iSetsWon)
		CPRINTLN(DEBUG_TENNIS, "AmGames=", thisStatus.playerScores[eOtherID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "TheirGames=", thisStatus.playerScores[eSelfID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "Returning TRUE")
		RETURN TRUE
	ENDIF
	IF (eRules = TMR_SETS 			AND thisStatus.playerScores[eOtherID].iSetsWon > thisStatus.playerScores[eSelfID].iSetsWon)
	OR (eRules <> TMR_SETS			AND thisStatus.playerScores[eOtherID].iGamesWon[iCurrentSet] > thisStatus.playerScores[eSelfID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "eRules=", GET_STRING_FROM_TENNIS_RULESET(eRules))
		CPRINTLN(DEBUG_TENNIS, "AmSets=", thisStatus.playerScores[eOtherID].iSetsWon)
		CPRINTLN(DEBUG_TENNIS, "TheirSets=", thisStatus.playerScores[eSelfID].iSetsWon)
		CPRINTLN(DEBUG_TENNIS, "AmGames=", thisStatus.playerScores[eOtherID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "TheirGames=", thisStatus.playerScores[eSelfID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "Returning FALSE")
		RETURN FALSE
	ENDIF
	// Games as a defining factor
	IF (thisStatus.playerScores[eOtherID].iGamesWon[iCurrentSet] < thisStatus.playerScores[eSelfID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "eRules=", GET_STRING_FROM_TENNIS_RULESET(eRules))
		CPRINTLN(DEBUG_TENNIS, "AmGames=", thisStatus.playerScores[eOtherID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "TheirGames=", thisStatus.playerScores[eSelfID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "Returning TRUE")
		RETURN TRUE
	ENDIF
	IF (thisStatus.playerScores[eOtherID].iGamesWon[iCurrentSet] > thisStatus.playerScores[eSelfID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "eRules=", GET_STRING_FROM_TENNIS_RULESET(eRules))
		CPRINTLN(DEBUG_TENNIS, "AmGames=", thisStatus.playerScores[eOtherID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "TheirGames=", thisStatus.playerScores[eSelfID].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "Returning FALSE")
		RETURN FALSE
	ENDIF
	// Points as a defining factor
//	IF thisStatus.playerScores[eOtherID].iPointsWon < thisStatus.playerScores[eSelfID].iPointsWon
//		CPRINTLN(DEBUG_TENNIS, "AmPoints=", thisStatus.playerScores[eOtherID].iPointsWon)
//		CPRINTLN(DEBUG_TENNIS, "TheirPoints=", thisStatus.playerScores[eSelfID].iPointsWon)
//		CPRINTLN(DEBUG_TENNIS, "Returning TRUE")
//		RETURN TRUE
//	ENDIF
//	IF thisStatus.playerScores[eOtherID].iPointsWon >= thisStatus.playerScores[eSelfID].iPointsWon
//		CPRINTLN(DEBUG_TENNIS, "AmPoints=", thisStatus.playerScores[eOtherID].iPointsWon)
//		CPRINTLN(DEBUG_TENNIS, "TheirPoints=", thisStatus.playerScores[eSelfID].iPointsWon)
//		CPRINTLN(DEBUG_TENNIS, "Returning FALSE")
//		RETURN FALSE
//	ENDIF
	CPRINTLN(DEBUG_TENNIS, "VALIDATE_AMANDA_IS_OPP_AND_LOSING_OVERALL returning default, FALSE")
	RETURN FALSE
ENDFUNC

PROC TENNIS_INCREMENT_STAT(PLAYER_STATS_ENUM eStat, INT iIncrease)
	#IF NOT IS_TENNIS_MULTIPLAYER
	enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
	INCREMENT_PLAYER_PED_STAT(ePed, eStat, iIncrease)
	CPRINTLN(DEBUG_TENNIS, "TENNIS_INCREMENT_STAT called, ePed=", ePed, ", eStat=", eStat, ", iIncrease=", iIncrease)
	#ENDIF
	
	#IF IS_TENNIS_MULTIPLAYER
	INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, eStat, iIncrease)
	CPRINTLN(DEBUG_TENNIS, "TENNIS_INCREMENT_STAT called, eStat=", eStat, ", iIncrease=", iIncrease)
	#ENDIF
	
ENDPROC

FUNC BOOL IS_TENNIS_PED_MALE(TENNIS_PLAYER &thisPlayer)
	IF thisPlayer.genderPlayer = GENDER_NEUTRAL
		PED_INDEX ped = GET_TENNIS_PLAYER_INDEX(thisPlayer)
		CPRINTLN(DEBUG_TENNIS, "IS_TENNIS_PED_MALE: ped = ", NATIVE_TO_INT(ped))
		
		enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
		CPRINTLN(DEBUG_TENNIS, "IS_TENNIS_PED_MALE: ePed = ", ePed)
		
		IF ePed = NO_CHARACTER
			ePed = GET_NPC_PED_ENUM(ped)
			CPRINTLN(DEBUG_TENNIS, "IS_TENNIS_PED_MALE: ePed = NO_CHARACTER, ePed = ", ePed)
		ENDIF
		
		
		#IF NOT IS_TENNIS_MULTIPLAYER
			PED_INDEX player = PLAYER_PED_ID()		
			CPRINTLN(DEBUG_TENNIS, "IS_TENNIS_PED_MALE: player = ", NATIVE_TO_INT(player))
			
			IF ePed = CHAR_MICHAEL
			OR ePed = CHAR_FRANKLIN
			OR ePed = CHAR_TREVOR
			OR ped = player
				CPRINTLN(DEBUG_TENNIS, "IS_TENNIS_PED_MALE: This tennis ped is a main character and male")
				thisPlayer.genderPlayer = GENDER_MALE
			ELIF IS_PED_MALE(ped) AND ePed <> CHAR_JIMMY
				CPRINTLN(DEBUG_TENNIS, "IS_TENNIS_PED_MALE: This tennis ped is male")
				thisPlayer.genderPlayer = GENDER_MALE
			ELSE
				CPRINTLN(DEBUG_TENNIS, "IS_TENNIS_PED_MALE: This tennis ped is female")
				thisPlayer.genderPlayer = GENDER_FEMALE
			ENDIF
		#ENDIF
		
		#IF IS_TENNIS_MULTIPLAYER
		IF IS_PED_FEMALE(ped)
			CPRINTLN(DEBUG_TENNIS, "IS_TENNIS_PED_MALE: This tennis ped is female")
			thisPlayer.genderPlayer = GENDER_FEMALE
		ELSE
			CPRINTLN(DEBUG_TENNIS, "IS_TENNIS_PED_MALE: This tennis ped is male")
			thisPlayer.genderPlayer = GENDER_MALE
		ENDIF
		#ENDIF
		
	ENDIF
	
	RETURN (thisPlayer.genderPlayer = GENDER_MALE)
ENDFUNC

PROC PLAY_TENNIS_AMBIENT_SPEECH(TENNIS_PLAYER &thisPlayer, STRING sContext, TEXT_LABEL_31 texVoice, SPEECH_PARAMS eParams = SPEECH_PARAMS_FORCE_SHOUTED)
	CDEBUG2LN(DEBUG_TENNIS, "PLAY_TENNIS_AMBIENT_SPEECH :: ", thisPlayer.playerAI.texName, " with sContext=", sContext, ", texVoice=", texVoice, ", IS_PED_INJURED(", PICK_STRING(IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer)), "TRUE", "FALSE"), ")")
	IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer))
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(GET_TENNIS_PLAYER_INDEX(thisPlayer), sContext, texVoice, eParams)
	ENDIF
ENDPROC

PROC LOAD_TENNIS_INTERSTITIAL_SCENE_AREAS(TENNIS_COURT &thisCourt)
	VECTOR vLoad
	SWITCH thisCourt.eTennisCourt
		CASE TENNIS_COURT_WeazelCourt1
			vLoad = (<<-1363.8917, -156.5640, 62.6605>>)
			NEW_LOAD_SCENE_START(vLoad, vLoad - thisCourt.vCenterCourt, 50)
			vLoad = (<<-1408.1664, -133.7219, 64.0504>>)
			NEW_LOAD_SCENE_START(vLoad, vLoad - thisCourt.vCenterCourt, 50)
			CPRINTLN(DEBUG_TENNIS, "LOAD_TENNIS_INTERSTITIAL_SCENE_AREAS called for ", GET_STRING_FROM_TENNIS_PLAYING_COURT(thisCourt.eTennisCourt))
		BREAK
		DEFAULT
			vLoad = thisCourt.vCenterCourt
			CPRINTLN(DEBUG_TENNIS, "LOAD_TENNIS_INTERSTITIAL_SCENE_AREAS defaulted for ", GET_STRING_FROM_TENNIS_PLAYING_COURT(thisCourt.eTennisCourt))
			//Do nothing
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Turns control for the player on an off, can be called every frame without multiple Native calls made.
/// PARAMS:
///    thisActivity - Will work if the activity is anything but TA_AI_VS_AI
///    bControl - 
PROC ALLOW_TENNIS_PLAYER_CONTROL(TENNIS_ACTIVITY thisActivity, BOOL bControl)
	IF thisActivity <> TA_AI_VS_AI
		IF bControl
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
				SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
				#IF TENNIS_DEBUG_RECORDING
				#IF IS_DEBUG_BUILD
				DEBUG_RECORD_STRING("SET_PLAYER_CONTROL", "TRUE")
				#ENDIF
				#ENDIF
				CPRINTLN(DEBUG_TENNIS, "ALLOW_TENNIS_PLAYER_CONTROL: Turning Player Control ON")
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
				SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE)
				#IF TENNIS_DEBUG_RECORDING
				#IF IS_DEBUG_BUILD
				DEBUG_RECORD_STRING("SET_PLAYER_CONTROL", "FALSE")
				#ENDIF
				#ENDIF
				CPRINTLN(DEBUG_TENNIS, "ALLOW_TENNIS_PLAYER_CONTROL: Turning Player Control OFF")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if ePlayer was winning
/// PARAMS:
///    serverBD - 
///    ePlayer - checking this player to see if they were winning.
///    eQuitter - 
/// RETURNS:
///    TRUE if they were winning
FUNC BOOL WAS_TENNIS_PLAYER_WINNING(TENNIS_SERVER_BD &serverBD, TENNIS_PLAYER_ID ePlayer, TENNIS_PLAYER_ID eQuitter)
	INT iCurrentSet = GET_TENNIS_MP_CURRENT_SET(serverBD)
	TENNIS_MATCH_RULESET eRules = GET_TENNIS_MP_RULESET(serverBD)
	CPRINTLN(DEBUG_TENNIS, "WAS_TENNIS_PLAYER_WINNING called:")
	
	INT iSetCount = 0
	INT iGamesWon
	REPEAT MAX_SETS iSetCount
		iGamesWon += serverBD.playerScores[ePlayer].iGamesWon[iSetCount]
	ENDREPEAT
	
	IF iGamesWon < 1
		CPRINTLN(DEBUG_TENNIS, "player has won no games")
		RETURN FALSE
	ENDIF
	
	IF (eRules = TMR_SETS 			AND serverBD.playerScores[ePlayer].iSetsWon < serverBD.playerScores[eQuitter].iSetsWon)
	OR (eRules <> TMR_SETS			AND serverBD.playerScores[ePlayer].iGamesWon[iCurrentSet] < serverBD.playerScores[eQuitter].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "eRules=", GET_STRING_FROM_TENNIS_RULESET(eRules))
		CPRINTLN(DEBUG_TENNIS, "MySets=", serverBD.playerScores[ePlayer].iSetsWon)
		CPRINTLN(DEBUG_TENNIS, "TheirSets=", serverBD.playerScores[eQuitter].iSetsWon)
		CPRINTLN(DEBUG_TENNIS, "MyGames=", serverBD.playerScores[ePlayer].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "TheirGames=", serverBD.playerScores[eQuitter].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "Returning FALSE")
		RETURN FALSE
	ENDIF
	IF (eRules = TMR_SETS 			AND serverBD.playerScores[ePlayer].iSetsWon > serverBD.playerScores[eQuitter].iSetsWon)
	OR (eRules <> TMR_SETS			AND serverBD.playerScores[ePlayer].iGamesWon[iCurrentSet] > serverBD.playerScores[eQuitter].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "eRules=", GET_STRING_FROM_TENNIS_RULESET(eRules))
		CPRINTLN(DEBUG_TENNIS, "MySets=", serverBD.playerScores[ePlayer].iSetsWon)
		CPRINTLN(DEBUG_TENNIS, "TheirSets=", serverBD.playerScores[eQuitter].iSetsWon)
		CPRINTLN(DEBUG_TENNIS, "MyGames=", serverBD.playerScores[ePlayer].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "TheirGames=", serverBD.playerScores[eQuitter].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "Returning TRUE")
		RETURN TRUE
	ENDIF
	IF (eRules = TMR_SETS 			AND serverBD.playerScores[ePlayer].iGamesWon[iCurrentSet] < serverBD.playerScores[eQuitter].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "eRules=", GET_STRING_FROM_TENNIS_RULESET(eRules))
		CPRINTLN(DEBUG_TENNIS, "MyGames=", serverBD.playerScores[ePlayer].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "TheirGames=", serverBD.playerScores[eQuitter].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "Returning FALSE")
		RETURN FALSE
	ENDIF
	IF (eRules = TMR_SETS 			AND serverBD.playerScores[ePlayer].iGamesWon[iCurrentSet] > serverBD.playerScores[eQuitter].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "eRules=", GET_STRING_FROM_TENNIS_RULESET(eRules))
		CPRINTLN(DEBUG_TENNIS, "MyGames=", serverBD.playerScores[ePlayer].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "TheirGames=", serverBD.playerScores[eQuitter].iGamesWon[iCurrentSet])
		CPRINTLN(DEBUG_TENNIS, "Returning TRUE")
		RETURN TRUE
	ENDIF
	IF serverBD.playerScores[ePlayer].iPointsWon < serverBD.playerScores[eQuitter].iPointsWon
		CPRINTLN(DEBUG_TENNIS, "MyPoints=", serverBD.playerScores[ePlayer].iPointsWon)
		CPRINTLN(DEBUG_TENNIS, "TheirPoints=", serverBD.playerScores[eQuitter].iPointsWon)
		CPRINTLN(DEBUG_TENNIS, "Returning FALSE")
		RETURN FALSE
	ENDIF
	IF serverBD.playerScores[ePlayer].iPointsWon >= serverBD.playerScores[eQuitter].iPointsWon
		CPRINTLN(DEBUG_TENNIS, "MyPoints=", serverBD.playerScores[ePlayer].iPointsWon)
		CPRINTLN(DEBUG_TENNIS, "TheirPoints=", serverBD.playerScores[eQuitter].iPointsWon)
		CPRINTLN(DEBUG_TENNIS, "Returning TRUE")
		RETURN TRUE
	ENDIF
	CPRINTLN(DEBUG_TENNIS, "WAS_TENNIS_PLAYER_WINNING returning default, TRUE")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Takes in an offset and orients to the player using their forward and right vectors
///    Was originally intended for swing offsets but has been co-opted for general use of converting offsets to the player's coordinate system
/// PARAMS:
///    thisPlayer - 
///    vSwingStateOffset - can be any offset, just commonly used for swing state offsets
/// RETURNS:
///    
FUNC VECTOR ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(TENNIS_PLAYER &thisPlayer, VECTOR vSwingStateOffset)
	VECTOR vAdjustedOffset = (vSwingStateOffset.x * thisPlayer.vMyRight)
	vAdjustedOffset += (vSwingStateOffset.y * thisPlayer.vMyForward)
	vAdjustedOffset.z += vSwingStateOffset.z
	
	RETURN vAdjustedOffset
ENDFUNC

/// PURPOSE:
///    Intended to convert the total frames of an animation at 30FPS to a number of frames at the current FRAME_TIME()
/// PARAMS:
///    iFrameTot - 
/// RETURNS:
///    the new converted frame total
FUNC INT GET_TOTAL_FRAMES_ADJUSTED_FOR_FRAME_TIME(INT iFrameTot)
	FLOAT fAnimTimeTot = TO_FLOAT(iFrameTot) * THIRTY_FPS
	FLOAT fNewTot = fAnimTimeTot / GET_FRAME_TIME()
	RETURN ROUND (fNewTot)
ENDFUNC

PROC ATTACH_TENNIS_BALL_TO_PLAYER(TENNIS_PLAYER &thisPlayer, TENNIS_BALL &thisBall)
	PED_INDEX ped = GET_TENNIS_PLAYER_INDEX(thisPlayer)
	IF DOES_ENTITY_EXIST(thisBall.oBall) AND DOES_ENTITY_EXIST(ped)
		ATTACH_ENTITY_TO_ENTITY(thisBall.oBall, ped, GET_PED_BONE_INDEX(ped, BONETAG_PH_L_Hand), (<<0,0,0>>), (<<0,0,0>>), TRUE)
		SET_TENNIS_BALL_VISIBILITY(thisBall.oBall, TRUE)
		CDEBUG2LN(DEBUG_TENNIS, "ATTACH_TENNIS_BALL_TO_PLAYER attaching to ", thisPlayer.playerAI.texName)
	ENDIF
ENDPROC

PROC ATTACH_TENNIS_RACKETS_TO_PLAYERS(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER_ID eSelfID, TENNIS_PLAYER_ID eOtherID, BOOL bWithOffset=FALSE, BOOL bSkipPlayer=FALSE)
	IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID])) AND NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]))
		IF NOT bWithOffset
			IF NOT bSkipPlayer
				ATTACH_ENTITY_TO_ENTITY(
					thisProps.tennisPlayers[eSelfID].oTennisRacket, 
					GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), 
					GET_PED_BONE_INDEX( GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), BONETAG_PH_R_HAND), 
					<<0,0,0>>, <<0,0,0>>)
			ENDIF
			ATTACH_ENTITY_TO_ENTITY(
				thisProps.tennisPlayers[eOtherID].oTennisRacket, 
				GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), 
				GET_PED_BONE_INDEX( GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), BONETAG_PH_R_HAND), 
				<<0,0,0>>, <<0,0,0>>)
		ELSE
			IF NOT bSkipPlayer
				ATTACH_ENTITY_TO_ENTITY(
					thisProps.tennisPlayers[eSelfID].oTennisRacket, 
					GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), 
					GET_PED_BONE_INDEX(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eSelfID]), BONETAG_PH_R_HAND), 
					RACKET_OFFSET, RACKET_ROTATION)
			ENDIF
			ATTACH_ENTITY_TO_ENTITY(
				thisProps.tennisPlayers[eOtherID].oTennisRacket, 
				GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), 
				GET_PED_BONE_INDEX(GET_TENNIS_PLAYER_INDEX(thisProps.tennisPlayers[eOtherID]), BONETAG_PH_R_HAND), 
				RACKET_OFFSET, RACKET_ROTATION)
		ENDIF
	ENDIF
ENDPROC

PROC SET_RACKET_ON_GROUND_NEXT_TO_PLAYER(TENNIS_GAME_PROPERTIES &thisProps, TENNIS_PLAYER_ID eSelfID)
	VECTOR vPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisProps.tennisPlayers[eSelfID].pIndex, <<1.3, 0.3, -0.914>>)
	FLOAT fHeading = GET_ENTITY_HEADING(thisProps.tennisPlayers[eSelfID].pIndex) + 15.0
	
	vPosition.z -= 0.072
	
	SET_ENTITY_COORDS(thisProps.tennisPlayers[eSelfID].oTennisRacket, vPosition)
	SET_ENTITY_ROTATION(thisProps.tennisPlayers[eSelfID].oTennisRacket, <<-90.6,0,fHeading>>)
	FREEZE_ENTITY_POSITION(thisProps.tennisPlayers[eSelfID].oTennisRacket, TRUE)
ENDPROC

PROC SET_TENNIS_CASCADE_SHADOWS_DEPTH_MODE(BOOL bEnable)
	CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE(bEnable)
	IF bEnable
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.25)
	ENDIF
ENDPROC

PROC SETUP_TENNIS_COURT_SHADOWS()
	CPRINTLN(DEBUG_TENNIS, "SETUP_TENNIS_COURT_SHADOWS called")
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.3)
	CASCADE_SHADOWS_SET_SPLIT_Z_EXP_WEIGHT(0.5)
	CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
	CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE("CSM_ST_BOX4x4")	// removing this can help GPU
ENDPROC

/// PURPOSE:
///    Creates the lighting prop for all locations other than Michael's Court.
PROC TENNIS_CREATE_LIGHTING_PROP( TENNIS_GAME_SP_DATA & thisSPData, TENNIS_GAME_PROPERTIES & thisProps )
	
	IF thisProps.thisLocation = ALOC_tennis_michaelHouse
		CDEBUG2LN( DEBUG_TENNIS, "TENNIS_CREATE_LIGHTING_PROP not creating light Prop, we're at Michael's house" )
		EXIT
	ENDIF
	
	thisSPData.oLighting = CREATE_OBJECT(PROP_VB_34_TENCRT_LIGHTING, thisProps.tennisCourt.vCenterCourt - (<< 0, 0, 1.415 >>) )
	SET_ENTITY_HEADING(thisSPData.oLighting, -52.5)
	CDEBUG2LN( DEBUG_TENNIS, "TENNIS_CREATE_LIGHTING_PROP lighting prop created." )
	
ENDPROC

PROC TASK_TENNIS_PLAYER_WATCH_BALL(TENNIS_PLAYER &thisPlayer, TENNIS_BALL &thisBall, BOOL bLookAtBall)
	IF DOES_ENTITY_EXIST(thisBall.oBall)
		IF bLookAtBall
			IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer))
				TASK_LOOK_AT_ENTITY(GET_TENNIS_PLAYER_INDEX(thisPlayer), thisBall.oBall, -1, SLF_FAST_TURN_RATE|SLF_EXTEND_YAW_LIMIT)
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(GET_TENNIS_PLAYER_INDEX(thisPlayer))
				TASK_CLEAR_LOOK_AT(GET_TENNIS_PLAYER_INDEX(thisPlayer))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TRIGGER_TENNIS_SCREEN_BLUR(BOOL bBlur)
	IF bBlur
//		TRIGGER_SCREENBLUR_FADE_IN(300)
	ELSE
//		TRIGGER_SCREENBLUR_FADE_OUT(300)
	ENDIF
ENDPROC

/// PURPOSE:
///    Has a timer and a rate of abuse, plays ambient abuse if there's a ped around
///    Widgets under Tennis -> AI
/// PARAMS:
///    thisSPData - 
PROC HANDLE_TENNIS_ABUSE_FROM_PEDS(TENNIS_GAME_SP_DATA &thisSPData, TENNIS_PLAYER_ID eCourtSide)
	INCREASE_TENNIS_ABUSE_TIMER(thisSPData)
	IF GET_TENNIS_ABUSE_TIMER(thisSPData) > TENNIS_PED_ABUSE_TIME
		IF GET_RANDOM_FLOAT_IN_RANGE() <= TENNIS_PED_ABUSE_RATE OR IS_TENNIS_SP_FLAG_SET(thisSPData, TSP_GUARANTEE_FIRST_ABUSE)
			SET_TENNIS_SP_FLAG(thisSPData, TSP_PICK_OUT_ABUSIVE_PED)
			IF IS_TENNIS_SP_FLAG_SET(thisSPData, TSP_GUARANTEE_FIRST_ABUSE)
				CLEAR_TENNIS_SP_FLAG(thisSPData, TSP_GUARANTEE_FIRST_ABUSE)
			ENDIF
			REQUEST_ANIM_DICT("misscommon@response")
		ENDIF
		RESET_TENNIS_ABUSE_TIMER(thisSPData)
	ENDIF
	
	IF IS_TENNIS_SP_FLAG_SET(thisSPData, TSP_PICK_OUT_ABUSIVE_PED) AND HAS_ANIM_DICT_LOADED("misscommon@response")
		CDEBUG2LN(DEBUG_TENNIS, "HANDLE_TENNIS_ABUSE_FROM_PEDS Frame=", GET_FRAME_COUNT())
		IF DOES_ENTITY_EXIST(thisSPData.pedAbuser)	// if we called this before set the last ped as no longer needed. Just covering our bases
			SET_PED_AS_NO_LONGER_NEEDED(thisSPData.pedAbuser)
			CDEBUG2LN(DEBUG_TENNIS, "HANDLE_TENNIS_ABUSE_FROM_PEDS :: Setting previous abuser as no longer needed")
		ENDIF
		
		BOOL bFoundPed
		VECTOR vNearestPoint = <<-1155.4968, -1606.7255, 4.0605>>
		bFoundPed = GET_CLOSEST_PED(vNearestPoint, COURT_RANGE * 2,  TRUE, FALSE, thisSPData.pedAbuser)
		#IF TENNIS_DEBUG_RECORDING
		#IF IS_DEBUG_BUILD
		DEBUG_RECORD_SPHERE("vNearestPoint", vNearestPoint, 1.0, (<<1,1,1>>))
		#ENDIF
		#ENDIF
		IF bFoundPed AND NOT IS_PED_INJURED(thisSPData.pedAbuser)
			VECTOR vPedPos = GET_ENTITY_COORDS(thisSPData.pedAbuser)
			VECTOR vPedToPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) - vPedPos
			VECTOR vDest = PICK_VECTOR(eCourtside = TENNIS_PLAYER_AWAY, (<<-1156.8663, -1617.8341, 3.3276>>), (<<-1167.2797, -1603.9518, 3.3300>>))
			FLOAT fHeading = GET_HEADING_FROM_VECTOR_2D(vPedToPlayer.x, vPedToPlayer.y)
			#IF TENNIS_DEBUG_RECORDING
			DEBUG_RECORD_LINE("LineOfAbuse", vPedPos, vPedPos+vPedToPlayer, (<<0,0,0>>), (<<1,1,1>>))
			DEBUG_RECORD_SPHERE("vDest", vDest, 0.5, (<<0,0,0>>))
			#ENDIF
			
			SEQUENCE_INDEX seqAbuse
			CLEAR_SEQUENCE_TASK(seqAbuse)
			OPEN_SEQUENCE_TASK(seqAbuse)
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
				// task to follow nav mesh to fence
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDest, PEDMOVE_WALK, DEFAULT, DEFAULT, DEFAULT, 123)
				TASK_ACHIEVE_HEADING(NULL, fHeading)
				// task to flip them off
				TASK_PLAY_ANIM(NULL, "misscommon@response", PICK_STRING(GET_RANDOM_FLOAT_IN_RANGE() < 0.5, "screw_you", "bring_it_on"))
				// task to walk away
				//TASK_WANDER_STANDARD(NULL, fHeading+180.0)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1134.4781, -1577.9078, 3.4310>>, PEDMOVE_WALK, DEFAULT, DEFAULT, DEFAULT, fHeading+180.0)
				// close sequence task
			CLOSE_SEQUENCE_TASK(seqAbuse)
			TASK_PERFORM_SEQUENCE(thisSPData.pedAbuser, seqAbuse)
			CLEAR_SEQUENCE_TASK(seqAbuse)
			SET_TENNIS_SP_FLAG(thisSPData, TSP_MONITOR_ABUSE_SEQUENCE)
		ENDIF
		CLEAR_TENNIS_SP_FLAG(thisSPData, TSP_PICK_OUT_ABUSIVE_PED)
	ENDIF
	
	IF IS_TENNIS_SP_FLAG_SET(thisSPData, TSP_MONITOR_ABUSE_SEQUENCE)
//		CDEBUG2LN(DEBUG_TENNIS, "GET_SEQUENCE_PROGRESS(", GET_SEQUENCE_PROGRESS(thisSPData.pedAbuser), ")")
		IF NOT IS_PED_INJURED(thisSPData.pedAbuser) AND GET_SEQUENCE_PROGRESS(thisSPData.pedAbuser) >= 3
			// task to yell at the player
			PLAY_PED_AMBIENT_SPEECH(thisSPData.pedAbuser, PICK_STRING(GET_RANDOM_FLOAT_IN_RANGE() > 0.5, "GENERIC_INSULT_HIGH", "GENERIC_INSULT_MALE"), SPEECH_PARAMS_STANDARD)
			CLEAR_TENNIS_SP_FLAG(thisSPData, TSP_MONITOR_ABUSE_SEQUENCE)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
			SET_TENNIS_SP_FLAG(thisSPData, TSP_PICK_OUT_ABUSIVE_PED)
			REQUEST_ANIM_DICT("misscommon@response")
			CPRINTLN(DEBUG_TENNIS, "SET_TENNIS_SP_FLAG(thisSPData, TSP_PICK_OUT_ABUSIVE_PED) thru debug")
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    Check to see if a line segment crosses the plane of the bat
/// PARAMS:
///    oTheBat - the object index of the bat
///    vA - first point on line
///    vB - second point on line
/// RETURNS:
///    TRUE if vA and vB are on opposite sides of the bat plane
FUNC BOOL DOES_LINE_CROSS_BAT_PLANE(ENTITY_INDEX oTheBat, VECTOR vA, VECTOR vB)
	IF DOES_ENTITY_EXIST(oTheBat)
		VECTOR vObjectCoord
		VECTOR vx,vy,vz
		GET_ENTITY_MATRIX(oTheBat, vy, vx, vz, vObjectCoord)
		VECTOR vA2Bat = vA - vObjectCoord
		VECTOR vB2Bat = vB - vObjectCoord
		RETURN (DOT_PRODUCT(vA2Bat, vy) > 0.0) ^ (DOT_PRODUCT(vB2Bat, vy) > 0.0)
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_LINE_CROSS_BAT_PLANE_2(ENTITY_INDEX oTheBat, VECTOR &vNextRaqFrame, VECTOR &vUpCourt, VECTOR &vBallPos, VECTOR &vBallVel)
	IF DOES_ENTITY_EXIST(oTheBat)
		
		VECTOR vObjectCoord
		VECTOR vx,vy,vz
		GET_ENTITY_MATRIX(oTheBat, vy, vx, vz, vObjectCoord)
//		DEBUG_RECORD_SPHERE("GET_ENTITY_MATRIX(bat)", vObjectCoord, 0.1, (<<0,0,0>>))
		vObjectCoord += vz * 0.4
//		DEBUG_RECORD_SPHERE("GET_ENT_COORD(bat)", GET_ENTITY_COORDS(oTheBat), 0.1, (<<255,255,255>>))
		
		#IF TENNIS_DEBUG_RECORDING
		DRAW_DEBUG_LINE(vObjectCoord, vObjectCoord + vUpCourt, 255, 0, 0)
		DEBUG_RECORD_SPHERE("batPos", vObjectCoord, 0.1, <<0.0,0.0,1.0>>)
		DEBUG_RECORD_LINE("BatX", vObjectCoord, vObjectCoord+vx*0.25, <<0.0,0.0,0.0>>, <<1.0,0.0,0.0>>)
		DEBUG_RECORD_LINE("BatY", vObjectCoord, vObjectCoord+vy*0.25, <<0.0,0.0,0.0>>, <<0.0,1.0,0.0>>)
		DEBUG_RECORD_LINE("BatZ", vObjectCoord, vObjectCoord+vz*0.25, <<0.0,0.0,0.0>>, <<0.0,0.0,1.0>>)
		#ENDIF
		
		// Adding in velocity to see when the ball crosses the racket plane	-RP
		FLOAT fFrameTime = GET_FRAME_TIME()
		vBallPos += vBallVel * fFrameTime
		#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_SPHERE("BP_vBallTest", vBallPos, 0.1, (<<0,255,255>>))
		#ENDIF
		
		//Also the racket is moving
		VECTOR vRacketTestPos = vNextRaqFrame
		#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_SPHERE("BP_vBatTest", vRacketTestPos, 0.1, (<<0,155,155>>))
		#ENDIF
				
		FLOAT dPUp = DOT_PRODUCT(vBallPos - vRacketTestPos, vUpCourt)
		BOOL bReturn = (dPUp < CROSS_BAT_PLANE_THRESHOLD)
		IF bReturn
//			DEBUG_RECORD_SPHERE("vBatPosAtHit", vObjectCoord, 0.15, (<<0,1,1>>))
			FLOAT fVelThroughPlane = DOT_PRODUCT(vBallVel, vUpCourt)
			IF fVelThroughPlane < 0.0
				FLOAT fTimeThroughPlane = dPUp / fVelThroughPlane
				fTimeThroughPlane = CLAMP(fTimeThroughPlane, 0.0, fFrameTime)	//THIRTY_FPS)
				vBallPos -= vBallVel * fTimeThroughPlane
//				DEBUG_RECORD_SPHERE("vBallPosAtHit1", vBallPos, 0.05, (<<0,0,1>>))

				//Another check to see how far we need to push the ball up court
				FLOAT dPUp2 = DOT_PRODUCT(vBallPos - vObjectCoord, vUpCourt)
				IF dPUp2 < -0.1
					vBallPos -= vUpCourt * dPUp2
//					DEBUG_RECORD_SPHERE("vBallPosAtHitAdjusted", vBallPos, 0.05, (<<0,0,1>>))
				ENDIF
			ELSE
				vBallPos -= vUpCourt * dPUp
				#IF TENNIS_DEBUG_RECORDING
				DEBUG_RECORD_SPHERE("vBallPosAtHit2", vBallPos, 0.05, (<<0,0,1>>))
				#ENDIF
			ENDIF
		ENDIF
		
		CPRINTLN(DEBUG_TENNIS, "DOES_LINE_CROSS_BAT_PLANE_2 :: ", PICK_STRING(bReturn, "TRUE", "FALSE"), ": Ballpos ", vBallPos, ", vBallVel, ", vBallVel, ", BatPos ", vObjectCoord, ", Dist ", VDIST(vBallPos, vRacketTestPos), ", Dot ", DOT_PRODUCT(vBallPos - vRacketTestPos, vUpCourt))
		
		RETURN bReturn
	ENDIF	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Checks a flag that is flipped every frame, let us know whether to check prediction spheres in an AI vs AI game.
/// PARAMS:
///    thisProps - 
/// RETURNS:
///    
FUNC BOOL SHOULD_TENNIS_RUN_PREDICTION(TENNIS_GAME_PROPERTIES &thisProps)
	RETURN thisProps.bCheckPredictionFlag
ENDFUNC

/// PURPOSE:
///    Flips the prediction flag
/// PARAMS:
///    thisProps - 
PROC FLIP_TENNIS_PREDICTION_FLAG(TENNIS_GAME_PROPERTIES &thisProps)
	thisProps.bCheckPredictionFlag = !thisProps.bCheckPredictionFlag
ENDPROC

/// PURPOSE:
///    Calls CLEAR_PED_TASKS_IMMEDIATELY on index of the player in players[eSelfID]
/// PARAMS:
///    players - 
///    eSelfID - 
PROC CLEAR_TENNIS_PED_TASKS(TENNIS_PLAYER &players[], TENNIS_PLAYER_ID eSelfID, BOOL bImmediately = TRUE)
	CPRINTLN(DEBUG_TENNIS, "Clearing Tasks for ", players[eSelfID].playerAI.texName)
	
	IF bImmediately
		CLEAR_PED_TASKS_IMMEDIATELY(players[eSelfID].pIndex)
	ELSE
		CLEAR_PED_TASKS(players[eSelfID].pIndex)
	ENDIF
ENDPROC

PROC SET_TENNIS_MP_TRACKING_STAT(TENNIS_MP_STAT_TRACKING_ENUM eStat, INT iIncrease)
	TENNIS_STAT_TRACKING_ARRAY[eStat] = iIncrease
ENDPROC

FUNC INT GET_TENNIS_MP_TRACKING_STAT(TENNIS_MP_STAT_TRACKING_ENUM eStat)
	RETURN TENNIS_STAT_TRACKING_ARRAY[eStat]
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    eStat - 
///    iIncrease - Will increment by stat by 1 if equal to 1, will set the stat to that number otherwise.
PROC INCREASE_TENNIS_MP_STAT(TENNIS_MP_STAT_TRACKING_ENUM eStat, INT iIncrease=1)
	IF iIncrease = 1
		SET_TENNIS_MP_TRACKING_STAT(eStat, GET_TENNIS_MP_TRACKING_STAT(eStat) + iIncrease)
	ELSE
		SET_TENNIS_MP_TRACKING_STAT(eStat, iIncrease)
	ENDIF
	
	CPRINTLN(DEBUG_TENNIS, "INCREASE_TENNIS_MP_STAT :: Increasing ", GET_STRING_FROM_TENNIS_MP_STAT_TRACKING_ENUM( eStat ) )
ENDPROC

/// PURPOSE:
///    Returns the component of a that is parallel to axis
/// PARAMS:
///    a - 
///    axis - 
/// RETURNS:
///    Vector
FUNC VECTOR	Parallel(VECTOR	a, VECTOR axis)
	VECTOR	temp = axis * DOT_PRODUCT(a, axis)
	RETURN	temp
ENDFUNC	

/// PURPOSE:
///    Returns the component of a that is perpendicular to axis
/// PARAMS:
///    a - 
///    axis - 
/// RETURNS:
///    Vector
FUNC VECTOR	Perp(VECTOR	a, VECTOR axis)
	VECTOR temp = a - Parallel(a, axis)
	RETURN temp
ENDFUNC

/// PURPOSE:
///    Rotates vRotateMe by Angle around vRotateAbout
/// PARAMS:
///    vRotateMe - 
///    Angle - 
///    vRotateAbout - 
PROC ROTATE_VECTOR(VECTOR &vRotateMe, FLOAT Angle, VECTOR vRotateAbout)
	vRotateAbout = NORMALISE_VECTOR(vRotateAbout)
	FLOAT cosTheta = COS(Angle)
	FLOAT sinTheta = SIN(Angle)
	vRotateMe = Perp(vRotateMe,vRotateAbout)*cosTheta +
				CROSS_PRODUCT(vRotateAbout,vRotateMe)*sinTheta +
				Parallel(vRotateMe,vRotateAbout)
ENDPROC

/// PURPOSE:
///    Lets the caller know if vPoint is in the angled area between vCenter1 and vCenter2 with fWidth
///    Calls IS_POINT_IN_ANGLED_AREA and handles the change in height needed for the Z
/// PARAMS:
///    vPoint - 
///    vCenter1 - 
///    vCenter2 - 
///    fWidth - 
/// RETURNS:
///    TRUE if the vPoint is in the angled area defined bu vCenter1, vCenter2, and fWidth
///    FALSE otherwise
FUNC BOOL IS_POINT_INSIDE_TENNIS_AREA(VECTOR vPoint, VECTOR vCenter1, VECTOR vCenter2, FLOAT fWidth)
	vCenter1.z += TENNIS_ANGLED_AREA_CHECK_ADD_HEIGHT
	vCenter2.z -= TENNIS_ANGLED_AREA_CHECK_SUB_HEIGHT
	RETURN IS_POINT_IN_ANGLED_AREA(vPoint, vCenter1, vCenter2, fWidth)
ENDFUNC

FUNC BOOL IS_BALL_WITHIN_TENNIS_COURT_WALLS(TENNIS_BALL &thisBall, TENNIS_COURT &thisCourt, VECTOR &vForward, VECTOR &vRight)
	VECTOR vCenter1 = thisCourt.vCourtCorners[0] + (vRight * 0.5)
	VECTOR vCenter2 = thisCourt.vCourtCorners[3] + (vRight * 0.5)
	VECTOR vDepthAdd = vForward * COURT_DEPTH_SCALAR
	FLOAT fWidth = thisCourt.fWidth * COURT_WIDTH_SCALAR
	vCenter1 = vCenter1 - vDepthAdd
	vCenter2 = vCenter2 + vDepthAdd
//	#IF IS_DEBUG_BUILD
//		DRAW_DEBUG_POLY_ANGLED_AREA(vCenter1, vCenter2, fWidth)
//	#ENDIF
	RETURN IS_POINT_INSIDE_TENNIS_AREA(GET_TENNIS_BALL_POS(thisBall), vCenter1, vCenter2, fWidth)
ENDFUNC

FUNC BOOL IS_POINT_WITHIN_TENNIS_COURT_WALLS(VECTOR vPoint, TENNIS_COURT &thisCourt, VECTOR &vForward, VECTOR &vRight)
	VECTOR vCenter1 = thisCourt.vCourtCorners[0] + (vRight * 0.5)
	VECTOR vCenter2 = thisCourt.vCourtCorners[3] + (vRight * 0.5)
	VECTOR vDepthAdd = vForward * COURT_DEPTH_SCALAR
	FLOAT fWidth = thisCourt.fWidth * COURT_WIDTH_SCALAR
	vCenter1 = vCenter1 - vDepthAdd
	vCenter2 = vCenter2 + vDepthAdd
	RETURN IS_POINT_INSIDE_TENNIS_AREA(vPoint, vCenter1, vCenter2, fWidth)
ENDFUNC

FUNC BOOL IS_POINT_JUST_OUT_OF_TENNIS_BOUNDS(VECTOR vPoint, TENNIS_COURT &thisCourt, VECTOR &vForward, VECTOR &vRight)
	VECTOR vCenter1 = thisCourt.vCourtCorners[0] + (vRight * 0.5)
	VECTOR vCenter2 = thisCourt.vCourtCorners[3] + (vRight * 0.5)
	BOOL bInsideCourt = IS_POINT_INSIDE_TENNIS_AREA(vPoint, vCenter1, vCenter2, thisCourt.fWidth)
	
	VECTOR vDepthAdd = vForward * COURT_DEPTH_JUST_OOB
	FLOAT fWidth = thisCourt.fWidth * COURT_WIDTH_JUST_OOB
	vCenter1 = vCenter1 - vDepthAdd
	vCenter2 = vCenter2 + vDepthAdd
	
	RETURN (NOT bInsideCourt AND IS_POINT_INSIDE_TENNIS_AREA(vPoint, vCenter1, vCenter2, fWidth))
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    vPlayerPos - 
///    thisCourt - 
/// RETURNS:
///    
FUNC BOOL IS_TENNIS_PLAYER_OUTSIDE_PLAYING_AREA(VECTOR vPlayerPos, TENNIS_COURT &thisCourt)
	FLOAT fLengthScalar, fWidthScalar
	SWITCH thisCourt.eTennisCourt
		CASE TENNIS_COURT_chumashHotel
			fLengthScalar = 2.031
			fWidthScalar = 2.160
		BREAK
		CASE TENNIS_COURT_CountryClub	// Venice Beach
			fLengthScalar = 6.955
			fWidthScalar = 1.600
		BREAK
		CASE TENNIS_COURT_LSUCourt1
			fLengthScalar = 6.715
			fWidthScalar = 2.043
		BREAK
		CASE TENNIS_COURT_MichaelHouse
			fLengthScalar = 4.800
			fWidthScalar = 2.433
		BREAK
		CASE TENNIS_COURT_RichmanHotel1
			fLengthScalar = 6.100
			fWidthScalar = 2.395
		BREAK
		CASE TENNIS_COURT_VespucciHotel
			fLengthScalar = 2.900
			fWidthScalar = 2.005
		BREAK
		CASE TENNIS_COURT_VinewoodHotel1
			fLengthScalar = 7.000
			fWidthScalar = 2.533
		BREAK
		CASE TENNIS_COURT_WeazelCourt1
			fLengthScalar = 7.700
			fWidthScalar = 2.775
		BREAK
		DEFAULT
			fLengthScalar = 10.0
			fWidthScalar = 3.0
		BREAK
	ENDSWITCH
	VECTOR vMid = (thisCourt.vCourtCorners[1] - thisCourt.vCourtCorners[0]) * 0.5
	VECTOR v1 = thisCourt.vCourtCorners[0] + vMid - (thisCourt.vForwardNorm * fLengthScalar)
	VECTOR v2 = thisCourt.vCourtCorners[3] + vMid + (thisCourt.vForwardNorm * fLengthScalar)
//	#IF IS_DEBUG_BUILD
//		VECTOR vUp = << 0, 0, 0.1 >>
//		DRAW_DEBUG_POLY_ANGLED_AREA(v1 + vUp, v2 + vUp, GET_TENNIS_COURT_WIDTH(thisCourt) * COURT_WIDTH_SCALAR)
//		fWidthScalar = COURT_WIDTH_SCALAR
//	#ENDIF
	RETURN NOT IS_POINT_INSIDE_TENNIS_AREA(vPlayerPos, v1, v2, GET_TENNIS_COURT_WIDTH(thisCourt) * fWidthScalar)
ENDFUNC

/// PURPOSE:
///    checks to see if the player is outside the court area and then tasks them to run back to the court
/// PARAMS:
///    thisPlayer - 
///    thisCourt - 
PROC HANDLE_TENNIS_PLAYER_OUTSIDE_PLAYING_AREA(TENNIS_PLAYER &thisPlayer, TENNIS_COURT &thisCourt)
	IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_TASKED_BACK_TO_COURT) AND IS_TENNIS_PLAYER_OUTSIDE_PLAYING_AREA(thisPlayer.vPos, thisCourt)
		//task to go back to court
		IF GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(thisPlayer), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(thisPlayer), SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
			VECTOR vBack
			vBack = (thisPlayer.vPos - thisCourt.vCenterCourt) * BACK_TO_COURT_SCALAR
			TASK_FOLLOW_NAV_MESH_TO_COORD(GET_TENNIS_PLAYER_INDEX(thisPlayer), thisCourt.vCenterCourt + vBack, PEDMOVE_RUN)
			SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_TASKED_BACK_TO_COURT)
			PRINT_HELP("FAR_FROM_COURT")
		ENDIF
	ELIF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_TASKED_BACK_TO_COURT) AND GET_SCRIPT_TASK_STATUS(GET_TENNIS_PLAYER_INDEX(thisPlayer), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
		CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_TASKED_BACK_TO_COURT)
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    fForce - 
///    fAngle - 
///    fLeftRightAngle - 
///    eCourtSide - 
/// RETURNS:
///    
FUNC VECTOR GetForceVector(FLOAT fForce, FLOAT fAngle, FLOAT fLeftRightAngle, VECTOR &vForward, VECTOR &vRight)
	VECTOR vForceToReturn
	vForceToReturn = NORMALISE_VECTOR(vForward)
	ROTATE_VECTOR(vForceToReturn,fLeftRightAngle,<<0,0,1>>)
	VECTOR vRightNormal = NORMALISE_VECTOR(vRight)
	ROTATE_VECTOR(vForceToReturn,fAngle,vRightNormal)
	vForceToReturn *= fForce	
	RETURN vForceToReturn
ENDFUNC

FUNC BOOL IS_ANALOG_JUST_PRESSED_UP(TENNIS_UI &thisUI, INT iDeadZone)
	INT y, dummy
	GET_ANALOG_STICK_VALUES(dummy,y,dummy,dummy, FALSE)
	
	IF y < iDeadZone AND (NOT IS_BITMASK_AS_ENUM_SET(thisUI.iUIFlags, TUF_ANALOG_UP) OR GET_GAME_TIMER() > GET_TENNIS_UI_MENU_STAMP(thisUI))
		SET_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_UP)
		CLEAR_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_DOWN)
		SET_TENNIS_UI_MENU_STAMP(thisUI, GET_GAME_TIMER() + MENU_STAMP_DELAY)
		RETURN TRUE
	ELIF y > iDeadZone
		CLEAR_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_UP)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANALOG_JUST_PRESSED_DOWN(TENNIS_UI &thisUI, INT iDeadZone)
	INT y, dummy
	GET_ANALOG_STICK_VALUES(dummy,y,dummy,dummy, FALSE)
	
	IF y > iDeadZone AND (NOT IS_BITMASK_AS_ENUM_SET(thisUI.iUIFlags, TUF_ANALOG_DOWN) OR GET_GAME_TIMER() > GET_TENNIS_UI_MENU_STAMP(thisUI))
		SET_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_DOWN)
		CLEAR_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_UP)
		SET_TENNIS_UI_MENU_STAMP(thisUI, GET_GAME_TIMER() + MENU_STAMP_DELAY)
		RETURN TRUE
	ELIF y < iDeadZone
		CLEAR_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_DOWN)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANALOG_JUST_PRESSED_LEFT(TENNIS_UI &thisUI, INT iDeadZone)
	INT x, dummy
	GET_ANALOG_STICK_VALUES(x,dummy,dummy,dummy, FALSE)
	
	IF x < iDeadZone AND (NOT IS_BITMASK_AS_ENUM_SET(thisUI.iUIFlags, TUF_ANALOG_LEFT) OR GET_GAME_TIMER() > GET_TENNIS_UI_MENU_STAMP(thisUI))
		SET_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_LEFT)
		CLEAR_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_RIGHT)
		SET_TENNIS_UI_MENU_STAMP(thisUI, GET_GAME_TIMER() + MENU_STAMP_DELAY)
		RETURN TRUE
	ELIF x > iDeadZone
		CLEAR_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_LEFT)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANALOG_JUST_PRESSED_RIGHT(TENNIS_UI &thisUI, INT iDeadZone)
	INT x, dummy
	GET_ANALOG_STICK_VALUES(x,dummy,dummy,dummy, FALSE)
	
	IF x > iDeadZone AND (NOT IS_BITMASK_AS_ENUM_SET(thisUI.iUIFlags, TUF_ANALOG_RIGHT) OR GET_GAME_TIMER() > GET_TENNIS_UI_MENU_STAMP(thisUI))
		SET_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_RIGHT)
		CLEAR_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_LEFT)
		SET_TENNIS_UI_MENU_STAMP(thisUI, GET_GAME_TIMER() + MENU_STAMP_DELAY)
		RETURN TRUE
	ELIF x < iDeadZone
		CLEAR_BITMASK_AS_ENUM(thisUI.iUIFlags, TUF_ANALOG_RIGHT)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL VALIDATE_SWING_ANIM_TRUNCATION(TENNIS_SWING_STATES eSwing, INT iFrameTrunc)
	IF iFrameTrunc < 0
		RETURN FALSE
	ENDIF
	IF eSwing = TSS_LUNGE_BACKHAND_HI
		RETURN FALSE
	ENDIF
	IF eSwing = TSS_LUNGE_BACKHAND_LOW
		RETURN FALSE
	ENDIF
	IF eSwing = TSS_LUNGE_BACKHAND_MID
		RETURN FALSE
	ENDIF
	IF eSwing = TSS_LUNGE_FOREHAND_HI
		RETURN FALSE
	ENDIF
	IF eSwing = TSS_LUNGE_FOREHAND_LOW
		RETURN FALSE
	ENDIF
	IF eSwing = TSS_LUNGE_FOREHAND_MID
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisPlayer - the player who is swinging
///    ped - the ped index we're tasking
///    thisPhase - the phase/swing state we're using
///    iFrameTrunc - Frames to truncate the beginning the animation by.
PROC CHECK_AND_SET_TENNIS_SWING_ANIM(TENNIS_PLAYER &thisPlayer, PED_INDEX ped, TENNIS_SWING_PHASES &thisPhase, INT iFrameTrunc, TENNIS_ACTIVITY eActivity)
	UNUSED_PARAMETER( eActivity )
	BOOL bIsPedMale = IS_TENNIS_PED_MALE(thisPlayer)
	STRING sAnimDict = PICK_STRING(bIsPedMale, "mini@tennis", "mini@tennis@female")
	TEXT_LABEL_31 sAnim = thisPhase.sAnimName
	
	// Add the truncate frames to the fInit value because we want to skip that many frames. 
	// This value is found in GET_TENNIS_SWING_STATE_TO_PLAY and set when that function returns a valid swing to use.
	INT iFrameTotAdj = GET_TOTAL_FRAMES_ADJUSTED_FOR_FRAME_TIME(thisPhase.iFrameTot)
	FLOAT fTruncPhase = (TO_FLOAT(iFrameTrunc) / TO_FLOAT(iFrameTotAdj))
	FLOAT fStartOfAnim = thisPhase.fInit
	
	IF VALIDATE_SWING_ANIM_TRUNCATION(GET_TENNIS_PLAYER_SWING_STATE(thisPlayer), iFrameTrunc)
		fStartOfAnim = thisPhase.fInit + fTruncPhase
		SET_TENNIS_PLAYER_ANIM_SPEED(thisPlayer, 1.0)
	ELSE
		FLOAT fSpaceToPlay = thisPhase.fActive - fTruncPhase
		FLOAT fSpeed = thisPhase.fActive / fSpaceToPlay	// We have fActive phase to play and only fSpaceToPlay to play it in, divide one by the other for the new rate!
		//Clamp the speed because we don't want to move too fast
		fSpeed = CLAMP(fSpeed, MIN_ANIM_SPEED, MAX_ANIM_SPEED)
		SET_TENNIS_PLAYER_ANIM_SPEED(thisPlayer, fSpeed)
	ENDIF
	
	IF fStartOfAnim > thisPhase.fActive
//		FLOAT fOneFrame = 1/iFrameTotAdj
		fStartOfAnim = thisPhase.fActive	// - fOneFrame
//		SCRIPT_ASSERT("fStartOfAnim > thisPhase.fActive")
	ENDIF
	
	// For Mark's new MoVE network.
	IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_TENNIS_SWING_EVENT_FLAG)
	
		IF NOT IS_TENNIS_MODE( GET_TENNIS_PLAYER_INDEX( thisPlayer ) )
			EXIT
		ENDIF
		
		#IF TENNIS_DEBUG_RECORDING
		DEBUG_RECORD_STRING("TASK_PLAY_ANIM", sAnim)
		DEBUG_RECORD_ENTITY_FLOAT(ped, "fStartOfAnim", fStartOfAnim)
		#ENDIF
		CLEAR_PED_TASKS(ped)
		
		IF (GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_DIVE_BACKHAND OR GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_DIVE_FOREHAND)
			IF GET_TENNIS_PLAYER_DIVE_SELECTION(thisPlayer) <> TENNIS_DIVE_NoDive
				TENNIS_DIVE_DIRECTION eDir
				IF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_DIVE_BACKHAND
					CPRINTLN(DEBUG_TENNIS, "CHECK_AND_SET_TENNIS_SWING_ANIM :: eDir = DIVE_BH")
					eDir = DIVE_BH
				ELIF GET_TENNIS_PLAYER_SWING_STATE(thisPlayer) = TSS_DIVE_FOREHAND
					CPRINTLN(DEBUG_TENNIS, "CHECK_AND_SET_TENNIS_SWING_ANIM :: eDir = DIVE_FH")
					eDir = DIVE_FH
				ENDIF
				PLAY_TENNIS_DIVE_ANIM(ped, eDir, GET_TENNIS_PLAYER_DIVE_BLEND_HORIZONTAL(thisPlayer), GET_TENNIS_PLAYER_DIVE_BLEND_VERTICAL(thisPlayer))
				SET_TENNIS_PLAYER_DIVE_REACT_STAMP(thisPlayer, GET_GAME_TIMER() + TENNIS_PROPS_DIVE_DURATION)
				CPRINTLN(DEBUG_TENNIS, "CHECK_AND_SET_TENNIS_SWING_ANIM :: ", thisPlayer.playerAI.texName, " using PLAY_TENNIS_DIVE_ANIM fHor=", GET_TENNIS_PLAYER_DIVE_BLEND_HORIZONTAL(thisPlayer), ", fVert=", GET_TENNIS_PLAYER_DIVE_BLEND_VERTICAL(thisPlayer))
			#IF IS_DEBUG_BUILD
			ELSE
				CPRINTLN(DEBUG_TENNIS, "CHECK_AND_SET_TENNIS_SWING_ANIM :: Skipping Dive")
			#ENDIF
			ENDIF
		ELSE
			PLAY_TENNIS_SWING_ANIM(ped, sAnimDict, sAnim, fStartOfAnim, GET_TENNIS_PLAYER_ANIM_SPEED(thisPlayer))	//Start the anim
			CPRINTLN(DEBUG_TENNIS, "CHECK_AND_SET_TENNIS_SWING_ANIM :: Setting ", thisPlayer.playerAI.texName, " using PLAY_TENNIS_SWING_ANIM")
		ENDIF
		CPRINTLN(DEBUG_TENNIS, "")
		CPRINTLN(DEBUG_TENNIS, "CHECK_AND_SET_TENNIS_SWING_ANIM :: sAnimName=", sAnim, ", fStartOfAnim=", fStartOfAnim, ", iFrameTrunc=", iFrameTrunc, ", fAnimSpeed=", GET_TENNIS_PLAYER_ANIM_SPEED(thisPlayer))
		SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_TENNIS_SWING_EVENT_FLAG)
		// Increment our iSwingsForStrength, determines strength gained from playing tennis (handled outside the script)
		IF thisPlayer.controlType <> COMPUTER
			g_savedGlobals.sTennisData.iSwingsForStrength++
		ENDIF
	ELIF GET_TENNIS_SWING_ANIM_CAN_BE_INTERRUPTED(ped) OR GET_TENNIS_SWING_ANIM_COMPLETE(ped)
		CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_TENNIS_SWING_EVENT_FLAG)
		CLEAR_PED_TASKS(ped)
		SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, TSS_NO_SWING)
		SET_TENNIS_PLAYER_ANIM_SPEED(thisPlayer, 1.0)
		IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_SWINGING_AIMLESSLY)
			CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_SWINGING_AIMLESSLY)
		ENDIF
		CPRINTLN(DEBUG_TENNIS, "Swing anim came to an end for ", thisPlayer.playerAI.texName, ", Clearing ped tasks, INTERRUPTED=", PICK_STRING(GET_TENNIS_SWING_ANIM_CAN_BE_INTERRUPTED(ped), "TRUE", "FALSE"), ", COMPLETE=", PICK_STRING( GET_TENNIS_SWING_ANIM_COMPLETE(ped), "TRUE", "FALSE" ) )
	ENDIF
ENDPROC

FUNC VECTOR GET_RANDOM_TENNIS_CAMERA_ANGLE(TENNIS_PLAYER &thisPlayer, STRING sInterstitialPlrAnim, VECTOR &vInterstitialOffset)
	INT i = GET_RANDOM_INT_IN_RANGE(0, MAX_STISH_CAM_ANGLES-1)
	
	VECTOR vCameraOffsets[MAX_STISH_CAM_ANGLES]
	vCameraOffsets[0] = << 1.5, 2.00, 0.23>>
	vCameraOffsets[1] = << 0.0, 3.00, 0.23>>
	vCameraOffsets[2] = << 1.0, 2.66, 0.23>>
	vCameraOffsets[3] = <<-2.0, 2.66, 0.23>>
	vCameraOffsets[4] = <<-1.0, 2.66, 0.23>>
	vCameraOffsets[5] = <<-2.0, 3.66, 0.23>>	// reserved for the "walk forward and point" anim
	
	vInterstitialOffset = <<0,0,0>>
	
	IF ARE_STRINGS_EQUAL(sInterstitialPlrAnim, "react_win_05")
		i = 5
		vInterstitialOffset = thisPlayer.vMyForward * INTERSTITIAL_FORWARD_SCALAR
	ENDIF
	
	VECTOR vReturn = ADJUST_TENNIS_SWING_OFFSET_FOR_PLAYER(thisPlayer, vCameraOffsets[i])
	
	CPRINTLN(DEBUG_TENNIS, "GET_RANDOM_TENNIS_CAMERA_ANGLE vCameraOffsets[i]", vCameraOffsets[i], ", vReturn=", vReturn)
	
	RETURN vReturn + GET_ENTITY_COORDS(GET_TENNIS_PLAYER_INDEX(thisPlayer))
ENDFUNC

FUNC TEXT_LABEL_63 GET_TENNIS_FACIAL_DICT(BOOL bPlayer, BOOL bHappy, BOOL bFemale = TRUE)
	TEXT_LABEL_63 sDict = "facials@"
	
	IF bPlayer
		IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
			sDict += "p_m_zero"
		ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
			sDict += "p_m_two"
//		ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
//			sDict += "p_m_one"
		ENDIF
	ELIF bFemale
		sDict+= "gen_female"
	ELSE
		sDict += "gen_male"
	ENDIF
	
	IF bHappy
		sDict += "@variations@happy"
	ELSE
		sDict += "@base"
	ENDIF
	
	RETURN sDict
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    sReturnAnim - The String that we want to check against
///    bHappy - TRUE for celebration anims, FALSE for lose anims
/// RETURNS:
///    
ENUM TENNIS_INTERSTITIAL_ACTOR
	TENNIS_ACTOR_PLAYER,
	TENNIS_ACTOR_GENERIC_FEMALE,
	TENNIS_ACTOR_GENERIC_MALE
ENDENUM
/// PURPOSE:
///    
/// PARAMS:
///    thisUIData - 
///    eActor - 
///    bHappy - 
PROC GET_TENNIS_INTERSTITIAL_ANIMATIONS(TENNIS_GAME_UI_DATA& thisUIData, TENNIS_INTERSTITIAL_ACTOR eActor, BOOL bHappy)
	INT iAnim
	
	IF eActor = TENNIS_ACTOR_PLAYER
		IF bHappy
			iAnim = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(thisUIData.iUsedPlayerHappyAnims, NUM_PLAYER_ANIMS, 1, NUM_PLAYER_ANIMS)
		ELSE
			iAnim = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(thisUIData.iUsedPlayerSadAnims, NUM_PLAYER_ANIMS, 1, NUM_PLAYER_ANIMS)
		ENDIF
		IF iAnim = -1
			INT p = 0
			REPEAT NUM_PLAYER_ANIMS p
				IF bHappy
					thisUIData.iUsedPlayerHappyAnims[p] = 0
				ELSE
					thisUIData.iUsedPlayerSadAnims[p] = 0
				ENDIF
			ENDREPEAT
			IF PICK_INT(bHappy, thisUIData.iLastPlayerHappyAnim, thisUIData.iLastPlayerSadAnim) = NUM_PLAYER_ANIMS - 1
				iAnim = 1
			ELSE
				iAnim = PICK_INT(bHappy, thisUIData.iLastPlayerHappyAnim, thisUIData.iLastPlayerSadAnim) + 1
			ENDIF
			CDEBUG2LN(DEBUG_TENNIS, "GET_TENNIS_INTERSTITIAL_ANIMATIONS :: Resetting thisUIData.", PICK_STRING(bHappy, "iUsedPlayerHappyAnims", "iUsedPlayerSadAnims"))
		ELSE
			IF bHappy
				thisUIData.iUsedPlayerHappyAnims[iAnim] = iAnim
			ELSE
				thisUIData.iUsedPlayerSadAnims[iAnim] = iAnim
			ENDIF
		ENDIF
		IF bHappy
			thisUIData.iLastPlayerHappyAnim = iAnim
		ELSE
			thisUIData.iLastPlayerSadAnim = iAnim
		ENDIF
		
		IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
			thisUIData.sInterstitialDict = "mini@tennisreactions@michael"
			thisUIData.sInterstitialPlrAnim = PICK_STRING(bHappy, "positive", "negative")
			thisUIData.sInterstitialPlrAnim += "_react_0"
			thisUIData.sInterstitialPlrAnim += iAnim
		ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
			IF bHappy
				thisUIData.sInterstitialDict = "mini@tennistrevor"
				thisUIData.sInterstitialPlrAnim = "react_win_trevor_0"
				thisUIData.sInterstitialPlrAnim += iAnim
			ELSE
				thisUIData.sInterstitialDict = "mini@tennisreactions@trevor"
				thisUIData.sInterstitialPlrAnim = "negative_react_0"
				thisUIData.sInterstitialPlrAnim += iAnim
			ENDIF
		ENDIF
		
		thisUIData.sInterstitialCamAnim = thisUIData.sInterstitialPlrAnim
		thisUIData.sInterstitialCamAnim += "_cam"
	
		IF bHappy
			thisUIData.sInterstitialFaceAnim = "mood_happy_"
			thisUIData.sInterstitialFaceAnim += GET_RANDOM_INT_IN_RANGE(1, 4)
		ELSE
			thisUIData.sInterstitialFaceAnim = "mood_angry_1"
		ENDIF
		
		CDEBUG2LN(DEBUG_TENNIS, "GET_TENNIS_INTERSTITIAL_ANIMATIONS :: thisUIData.sInterstitialPlrAnim=", thisUIData.sInterstitialPlrAnim)
		EXIT
	ELSE	// Not a player
		IF eActor = TENNIS_ACTOR_GENERIC_FEMALE
			thisUIData.sInterstitialDict = "mini@tennis@female"
		ELIF eActor = TENNIS_ACTOR_GENERIC_MALE
			thisUIData.sInterstitialDict = "mini@tennis"
		ENDIF
		IF bHappy
			iAnim = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(thisUIData.iUsedAIHappyAnims, NUM_AI_ANIMS, 1, NUM_AI_ANIMS)
		ELSE
			iAnim = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(thisUIData.iUsedAISadAnims, NUM_AI_ANIMS, 1, NUM_AI_ANIMS)
		ENDIF
		IF iAnim = -1
			INT p = 0
			REPEAT NUM_AI_ANIMS p
				IF bHappy
					thisUIData.iUsedAIHappyAnims[p] = 0
				ELSE
					thisUIData.iUsedAISadAnims[p] = 0
				ENDIF
			ENDREPEAT
			IF PICK_INT(bHappy, thisUIData.iLastAIHappyAnim, thisUIData.iLastAISadAnim) = NUM_AI_ANIMS - 1
				iAnim = 1
			ELSE
				iAnim = PICK_INT(bHappy, thisUIData.iLastAIHappyAnim, thisUIData.iLastAISadAnim) + 1
			ENDIF
			CDEBUG2LN(DEBUG_TENNIS, "GET_TENNIS_INTERSTITIAL_ANIMATIONS :: Resetting thisUIData.", PICK_STRING(bHappy, "iUsedAIHappyAnims", "iUsedAISadAnims"))
		ELSE
			IF bHappy
				thisUIData.iUsedAIHappyAnims[iAnim] = iAnim
			ELSE
				thisUIData.iUsedAISadAnims[iAnim] = iAnim
			ENDIF
		ENDIF
		IF bHappy
			thisUIData.iLastAIHappyAnim = iAnim
		ELSE
			thisUIData.iLastAISadAnim = iAnim
		ENDIF

		IF bHappy
			thisUIData.sInterstitialPlrAnim = "react_win_0"
			thisUIData.sInterstitialFaceAnim = "mood_happy_"
			thisUIData.sInterstitialFaceAnim += GET_RANDOM_INT_IN_RANGE(1, 4)
		ELSE
			thisUIData.sInterstitialPlrAnim = "react_lose_0"
			thisUIData.sInterstitialFaceAnim = "mood_angry_1"
		ENDIF

		thisUIData.sInterstitialPlrAnim += iAnim
		thisUIData.sInterstitialCamAnim = thisUIData.sInterstitialPlrAnim
		thisUIData.sInterstitialCamAnim += "_cam"
		CDEBUG2LN(DEBUG_TENNIS, "GET_TENNIS_INTERSTITIAL_ANIMATIONS :: thisUIData.sInterstitialPlrAnim=", thisUIData.sInterstitialPlrAnim)
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisProps - gets its react stamp set when the player dives
///    thisPlayer - the player who's anims we're handing
///    phasesArray - []
///    eActivity - 
PROC HANDLE_TENNIS_ANIMS(TENNIS_PLAYER &thisPlayer, TENNIS_SWING_PHASES &phasesArray[], TENNIS_ACTIVITY eActivity)
	PED_INDEX ped = GET_TENNIS_PLAYER_INDEX(thisPlayer)
	FLOAT fInit, fActive, fEnd, fToss
	STRING sSwingAnim
	TENNIS_SWING_STATES eSwing = GET_TENNIS_PLAYER_SWING_STATE(thisPlayer)
	
	IF NOT IS_PED_INJURED(ped)
		STRING sAnimDict = PICK_STRING(IS_TENNIS_PED_MALE(thisPlayer), "mini@tennis", "mini@tennis@female")
		
		IF eSwing = TSS_NO_SWING
			IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_DIVE_ROLL_FINISHING)
				IF IS_ENTITY_PLAYING_ANIM(ped, sAnimDict, "idle")
					STOP_ANIM_TASK(ped, sAnimDict, "idle")
					CPRINTLN(DEBUG_TENNIS, "Idle cleaned up")
				ENDIF
				CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_DIVE_ROLL_FINISHING)
				CPRINTLN(DEBUG_TENNIS, "TPB_DIVE_ROLL_FINISHING flag cleared")
			ENDIF
			IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_MOVE_ENABLED) AND NOT IS_TENNIS_AI_FLAG_SET(thisPlayer.playerAI, TAF_JUST_TASKED)
				IF NOT IS_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisPlayer))
					SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisPlayer),TRUE)
					CPRINTLN(DEBUG_TENNIS, "eSwing = TSS_NO_SWING, Turning tennis mode on for ", thisPlayer.playerAI.texName)
				ENDIF
				SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_MOVE_ENABLED)
			ENDIF
		ELIF eSwing = TSS_SERVE_TOSS
			//floats are taken from watching the anim and picking phases
			fInit 		= 0.0
			fToss		= 0.567
			fActive 	= SERVE_ACTIVE_PHASE
			fEnd 		= 0.855
			sSwingAnim = "serve"
			IF NOT IS_ENTITY_PLAYING_ANIM(ped, sAnimDict, sSwingAnim)
				#IF TENNIS_DEBUG_RECORDING
				DEBUG_RECORD_ENTITY_STRING(ped, "TASK_PLAY_ANIM", sSwingAnim)
				#ENDIF
				TASK_PLAY_ANIM(ped, sAnimDict, sSwingAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT, fInit)
				SET_TENNIS_PLAYER_STROKE(thisPlayer, TOSS)
				CPRINTLN(DEBUG_TENNIS, "TSS_SERVE_TOSS started for player with eCourtSide ", thisPlayer.playerAI.texName)
			ELSE
				IF GET_ENTITY_ANIM_CURRENT_TIME(ped,sAnimDict,sSwingAnim) > fEnd
					CPRINTLN(DEBUG_TENNIS, "TSS_SERVE_TOSS ended for player with eCourtSide ", thisPlayer.playerAI.texName)
					STOP_ANIM_TASK(ped, sAnimDict, sSwingAnim)	//CLEAR_PED_TASKS_IMMEDIATELY(ped)
					SET_TENNIS_PLAYER_SWING_STATE(thisPlayer, TSS_NO_SWING)
					IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_SERVE_EXECUTING)
						CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_SERVE_EXECUTING)
					ENDIF
					
					IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_MOVE_ENABLED)
						SET_TENNIS_PLAYER_TO_TENNIS_MODE(GET_TENNIS_PLAYER_INDEX(thisPlayer),TRUE)
						SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_MOVE_ENABLED)
						CPRINTLN(DEBUG_TENNIS, "Turning tennis mode on for player with eCourtSide ", thisPlayer.playerAI.texName)
					ENDIF
				ELIF GET_ENTITY_ANIM_CURRENT_TIME(ped,sAnimDict,sSwingAnim) >= fActive //Spot to hit the ball
					IF NOT IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_SERVE_EXECUTING)
						SET_TENNIS_PLAYER_FLAG(thisPlayer, TPB_SERVE_EXECUTING)
						SET_TENNIS_PLAYER_STROKE(thisPlayer, SERVE)
					ENDIF
				ELIF GET_ENTITY_ANIM_CURRENT_TIME(ped,sAnimDict,sSwingAnim) > fToss //Point of release in the serve anim
					IF IS_TENNIS_PLAYER_FLAG_SET(thisPlayer, TPB_SERVE_EXECUTING)
						CLEAR_TENNIS_PLAYER_FLAG(thisPlayer, TPB_SERVE_EXECUTING)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CHECK_AND_SET_TENNIS_SWING_ANIM(thisPlayer, ped, phasesArray[eSwing], GET_TENNIS_PLAYER_TRUNCATE_FRAMES(thisPlayer), eActivity)
		ENDIF
	ENDIF
ENDPROC
