// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	SPTT_Helpers.sch
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Single Player Races - Helper procs/functions file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
USING "globals.sch"
USING "SPTT_Head.sch"
USING "lineactivation.sch"	// Include this so it compiles in release, for using GET_OFFSET_FROM_COORD_IN_WORLD_COORDS
USING "screens_header.sch"
USING "hud_drawing.sch"
USING "UIUtil.sch"
USING "shared_hud_displays.sch"
USING "socialclub_leaderboard.sch"
USING "script_camera.sch"
USING "sptt_sc_leaderboard_lib.sch"

/*
#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID stuntvalues
#ENDIF
*/

ENUM SPTT_STUNT_FINISH_CUT_STATE
	SPTT_STUNT_FINISH_INIT = 0,
	SPTT_STUNT_FINISH_CREATE_CAM,
	SPTT_STUNT_FINISH_AUTOPILOT	
ENDENUM
SPTT_STUNT_FINISH_CUT_STATE eSPRStuntCutFinish = SPTT_STUNT_FINISH_INIT

ENUM SPTT_END_SCREEN_STATE
	SPTT_END_SCREEN_INIT,
	SPTT_END_SCREEN_SETUP,
	SPTT_END_SCREEN_POINT_CAM,
	SPTT_END_DETERMINE_TRACKING,
	SPTT_END_HOLD_SHOT,
	SPTT_END_QUICK_PAN_SETUP,
	SPTT_END_QUICK_PAN_UPDATE,
	SPTT_END_SFX_AND_SPLASH_SETUP,
	SPTT_END_SPLASH_UPDATE,
	SPTT_END_CLEANUP
ENDENUM

SPTT_END_SCREEN_STATE eSPREndScreenState = SPTT_END_SCREEN_INIT

ENUM SPTT_STUNT_BEAT_STATE
	SPTT_STUNT_BEAT_INIT,
	SPTT_STUNT_BEAT_STREAM,
	SPTT_STUNT_BEAT_UPDATE,	
	SPTT_STUNT_BEAT_INACTIVE
ENDENUM
SPTT_STUNT_BEAT_STATE 	eSPRBeatState

ENUM SPTT_STUNT_BEAT_TRIGGERS
	SPTT_STUNT_BEAT_01 = BIT0,		
	SPTT_STUNT_BEAT_02 = BIT1
ENDENUM

ENUM SPTT_STUNT_BEAT_INTS
	SPTT_STUNT_BEAT_INT_01 = 0,
	SPTT_STUNT_BEAT_INT_02
ENDENUM

CONST_INT				STUNT_MAX_BEATS			2
CONST_INT				STUNT_MAX_MODELS		5
CONST_INT				STUNT_MAX_PEDS			5
CONST_INT				STUNT_MAX_VEHICLES		5

BOOL bAudioPlayedLastGate = FALSE

// Stunt Reward Bools
BOOL bMissedGate = FALSE
BOOL bInnerGate = FALSE
BOOL bOutterGate = FALSE
BOOL bInnerKnife = FALSE
BOOL bInnerInverted = FALSE
BOOL bOutterKnife = FALSE
BOOL bOutterInverted = FALSE
BOOL bStuntKniL = FALSE
BOOL bStuntKniLHit = FALSE
BOOL bStuntKniLMiss = FALSE
BOOL bStuntKniR = FALSE
BOOL bStuntKniRHit = FALSE
BOOL bStuntKniRMiss = FALSE
BOOL bStuntInv = FALSE
BOOL bStuntInvHit = FALSE
BOOL bStuntInvMiss = FALSE
BOOL bTransitionUp
BOOL bTransitionOut
// Stunt Reward Timers
structTimer stuntRoll
structTimer stuntInverted
structTimer stuntLoop
structTimer stuntFail
structTimer missedGate
structTimer clearedGate
structTimer centerGate
structTimer clearedKnife
structTimer clearedInvert
structTimer centerKnife
structTimer centerInvert
structTimer SPRStuntCamTimer

CAMERA_INDEX iSPRStuntFinishCam
CAMERA_INDEX iSPRStuntFinishCam1

SCRIPT_SCALEFORM_MEDAL_TOAST SPTT_UI_MedalToast
STREAMED_MODEL			stuntBeatModels[STUNT_MAX_MODELS]

VEHICLE_INDEX 			iBeatVehicles[STUNT_MAX_VEHICLES]
PED_INDEX	 			iBeatPeds[STUNT_MAX_PEDS]
INT						iBeatsTriggered, iBeatsTriggered1

// race hud UI variables
STRING sStuntName = NULL
FLOAT fStuntValue = -1.0
INT iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)

INT iHUDDisplayBonusPrint = 0
INT iHUDDisplayBonusTracker = 0
structTimer tDisplayBonusTimer

FUNC STRING GET_HUD_STUNT_STRING()
	RETURN sStuntName
ENDFUNC

FUNC FLOAT GET_HUD_STUNT_FLOAT()
	RETURN fStuntValue
ENDFUNC

FUNC INT GET_HUD_STUNT_INT()
	RETURN iStuntValueColor
ENDFUNC

ENUM GateMessageType
	message0 = BIT0,
	message1 = BIT1,
	message2 = BIT2,
	message3 = BIT3,
	message4 = BIT4
ENDENUM

INT iSPRAudioBeats
CONST_INT iMAXAUDIOBEATS 2

ENUM STUNTBEATAUDIO
	AudioBeat0 = BIT0,
	AudioBeat1 = BIT1
ENDENUM

structPedsForConversation myDialogueStruct

STRING sRadStation

// -----------------------------------
// Dialogue helpers
// -----------------------------------

// plays audio based on what string and bitmask get passed into it
// iRandLow and iRandHigh are the random int ranges it uses to determine if it plays the line
// setting those to be above or below 5000 will make a line not play or always play when calling this function
// all calls to this are made in spr_racer.sch
PROC Play_Gate_Audio(STRING sLine, INT &iBitmaskInt) //, INT iRandLow, INT iRandHigh)	
	TEXT_LABEL tMisstext = "Gate_Miss"
	// play specified single line		
		
	IF NOT IS_BITMASK_AS_ENUM_SET(iBitmaskInt, message0)
		PRINTLN("Play_Gate_Audio:  message0 bit not set running play audio")
//		TEXT_LABEL tLine = sLine		
//		tLine += "_"
//		tLine += 1
		IF NOT bAudioPlayedLastGate
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", sLine, tLine, CONV_PRIORITY_MEDIUM)
			bAudioPlayedLastGate = TRUE
			SET_BITMASK_AS_ENUM(iBitmaskInt, message0)
			EXIT
		ELSE
			// set no line bitmask
			bAudioPlayedLastGate = FALSE
			IF ARE_STRINGS_EQUAL(sLine, tMisstext)
				PRINT_NOW("SPR_FAIL_GATE", DEFAULT_GOD_TEXT_TIME, 0)				
			ENDIF
			EXIT
		ENDIF
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(iBitmaskInt, message0)
		IF NOT IS_BITMASK_AS_ENUM_SET(iBitmaskInt, message1)
//			TEXT_LABEL tLine = sLine
//			tLine += "_"
//			tLine += 2
			IF NOT bAudioPlayedLastGate 
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", sLine, tLine, CONV_PRIORITY_MEDIUM)
				bAudioPlayedLastGate = TRUE
				SET_BITMASK_AS_ENUM(iBitmaskInt, message1)
				EXIT
			ELSE
				// set no line bitmask
				bAudioPlayedLastGate = FALSE
				IF ARE_STRINGS_EQUAL(sLine, tMisstext)
					PRINT_NOW("SPR_FAIL_GATE", DEFAULT_GOD_TEXT_TIME, 0)					
				ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(iBitmaskInt, message1)
		IF NOT IS_BITMASK_AS_ENUM_SET(iBitmaskInt, message2)
//			TEXT_LABEL tLine = sLine
//			tLine += "_"
//			tLine += 3
			IF NOT bAudioPlayedLastGate 
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", sLine, tLine, CONV_PRIORITY_MEDIUM)
				bAudioPlayedLastGate = TRUE
				SET_BITMASK_AS_ENUM(iBitmaskInt, message2)
				EXIT
			ELSE
				// set no line bitmask
				bAudioPlayedLastGate = FALSE
				IF ARE_STRINGS_EQUAL(sLine, tMisstext)
					PRINT_NOW("SPR_FAIL_GATE", DEFAULT_GOD_TEXT_TIME, 0)
				ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF

	IF IS_BITMASK_AS_ENUM_SET(iBitmaskInt, message2)
		IF NOT IS_BITMASK_AS_ENUM_SET(iBitmaskInt, message3)
//			TEXT_LABEL tLine = sLine
//			tLine += "_"
//			tLine += 4
			IF NOT bAudioPlayedLastGate 
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", sLine, tLine, CONV_PRIORITY_MEDIUM)
				bAudioPlayedLastGate = TRUE
				SET_BITMASK_AS_ENUM(iBitmaskInt, message3)
				EXIT
			ELSE
				// set no line bitmask
				bAudioPlayedLastGate = FALSE
				IF ARE_STRINGS_EQUAL(sLine, tMisstext)
					PRINT_NOW("SPR_FAIL_GATE", DEFAULT_GOD_TEXT_TIME, 0)					
				ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF

	IF IS_BITMASK_AS_ENUM_SET(iBitmaskInt, message3)
		IF NOT IS_BITMASK_AS_ENUM_SET(iBitmaskInt, message4)
//			TEXT_LABEL tLine = sLine
//			tLine += "_"
//			tLine += 5
			IF NOT bAudioPlayedLastGate 
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", sLine, tLine, CONV_PRIORITY_MEDIUM)
				bAudioPlayedLastGate = TRUE
				SET_BITMASK_AS_ENUM(iBitmaskInt, message4)
				EXIT
			ELSE
				// set no line bitmask
				bAudioPlayedLastGate = FALSE
				IF ARE_STRINGS_EQUAL(sLine, tMisstext)
					PRINT_NOW("SPR_FAIL_GATE", DEFAULT_GOD_TEXT_TIME, 0)					
				ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF

ENDPROC


// -----------------------------------
// RACE PROCS/FUNCTIONS
// -----------------------------------

PROC Disable_Airport_Icons()
	IF SPTT_Master.eRaceType = SPTT_RACE_TYPE_PLANE
		SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
		SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_2, FALSE)
		SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_3, FALSE)
		SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_4, FALSE)		
	ENDIF
ENDPROC

/// PURPOSE:
///    Lets the script know if the medal toast is displaying
/// RETURNS:
///    TRUE if displaying, FALSE when finished.
FUNC INT SPTT_ADD_MEDAL_FEED(SPTT_RACE_STRUCT &Race)
	PRINTLN("UPDATE_RANGE_MEDAL_TOAST called")
	
	IF (Race.Racer[0].fClockTime <= SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])
		BEGIN_TEXT_COMMAND_THEFEED_POST(SPTT_Master.szRaceName[SPTT_Master.iRaceCur])
		RETURN END_TEXT_COMMAND_THEFEED_POST_AWARD("MPMedals_FEED", "Feed_Medal_FlightSchool", 0, HUD_COLOUR_GOLD, "HUD_MED_UNLKED")
	ELIF (Race.Racer[0].fClockTime <= ((SPTT_Master.fTimeGold[SPTT_Master.iRaceCur]-SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur])/2)+SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur])
		BEGIN_TEXT_COMMAND_THEFEED_POST(SPTT_Master.szRaceName[SPTT_Master.iRaceCur])
		RETURN END_TEXT_COMMAND_THEFEED_POST_AWARD("MPMedals_FEED", "Feed_Medal_FlightSchool", 0, HUD_COLOUR_SILVER, "HUD_MED_UNLKED")
	ELIF (Race.Racer[0].fClockTime <= SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur])
		BEGIN_TEXT_COMMAND_THEFEED_POST(SPTT_Master.szRaceName[SPTT_Master.iRaceCur])
		RETURN END_TEXT_COMMAND_THEFEED_POST_AWARD("MPMedals_FEED", "Feed_Medal_FlightSchool", 0, HUD_COLOUR_BRONZE, "HUD_MED_UNLKED")
	ENDIF

	RETURN -1
ENDFUNC

PROC SPTT_SETUP_SCORECARD(SPTT_RACE_STRUCT &Race)
	STRING sMethodName = GET_STRING_FROM_BIG_MESSAGE_TYPE_ENUM(MG_BIG_MESSAGE_CENTERED)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(Race.bigMessageUI.siMovie, "RESET_MOVIE")
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(Race.bigMessageUI.siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("SPR_UI_PASS")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(SPTT_Master.szRaceName[SPTT_Master.iRaceCur])
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(100.0) // Alpha
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) // Centered
	END_SCALEFORM_MOVIE_METHOD()
	
	// Store the duration.
	Race.bigMessageUI.iDuration = 5000
	CANCEL_TIMER(Race.bigMessageUI.movieTimer)
	bTransitionUp = FALSE
	bTransitionOut = FALSE
ENDPROC

/// PURPOSE:
///    Draws the scorecard for the SPTT
FUNC BOOL SPTT_NEW_DRAW_SCORECARD(SPTT_RACE_STRUCT &Race)

	FLOAT fTimerVal = 0.0
	IF NOT IS_TIMER_STARTED(Race.bigMessageUI.movieTimer)
		RESTART_TIMER_NOW(Race.bigMessageUI.movieTimer)
	ELSE
		fTimerVal = GET_TIMER_IN_SECONDS(Race.bigMessageUI.movieTimer)
	ENDIF
	
	// Do the transition up here too.
	IF NOT bTransitionUp
		IF fTimerVal > 1.0
//			PRINTLN("SPTT_NEW_DRAW_SCORECARD - Transition up: ", fTimerVal)
//			BEGIN_SCALEFORM_MOVIE_METHOD(Race.bigMessageUI.siMovie, "TRANSITION_UP")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(MG_BIG_MESSAGE_ANIM_TIME_SLOW)
//			END_SCALEFORM_MOVIE_METHOD()
			bTransitionUp = TRUE
		ENDIF
	ENDIF
	
	// Draw victory message. When it's done, move on.
	IF NOT bTransitionOut
		IF fTimerVal > 4.25
			PRINTLN("SPTT_NEW_DRAW_SCORECARD - Transition out: ", fTimerVal)
			BEGIN_SCALEFORM_MOVIE_METHOD(Race.bigMessageUI.siMovie, "TRANSITION_OUT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.5)
			END_SCALEFORM_MOVIE_METHOD()
			bTransitionOut = TRUE
		ENDIF
	ENDIF
	
	// Draw.
	//FLOAT yat = GET_ASPECT_RATIO_MODIFIER()*0.25
	//DRAW_SCALEFORM_MOVIE(Race.bigMessageUI.siMovie,0.5,yat,AR16_9/ GET_SCREEN_ASPECT_RATIO(),1,255,255,255,255)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(Race.bigMessageUI.siMovie, 255, 255, 255, 255)
	
	// Are we done?
	IF fTimerVal > 5.0
		PRINTLN("SPTT_NEW_DRAW_SCORECARD - All done: ", fTimerVal)
		CANCEL_TIMER(Race.bigMessageUI.movieTimer)
		bTransitionUp = FALSE
		bTransitionOut = FALSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VECTOR SPTT_GET_END_SCREEN_CAM_POS()
	SWITCH INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur)
		CASE SPTT_BridgeBinge
			RETURN <<-2591.9653, 2483.1675, 60.8904>>
		BREAK
		CASE SPTT_Vinewood_Flyby
			RETURN <<1827.9561, 585.8948, 287.6289>>
		BREAK
		CASE SPTT_Advanced_Race01
			RETURN <<991.1072, -2389.9829, 83.3701>> 
		BREAK
		CASE SPTT_AirportFlyby
			RETURN <<-1721.0487, -2792.9478, 75.4149>>
		BREAK
		CASE SPTT_Altitude 
			RETURN <<-578.6125, 5436.7212, 150.1292>>
		BREAK
	ENDSWITCH
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC VECTOR SPTT_GET_END_SCREEN_CAM_ROT()
	SWITCH INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur)
		CASE SPTT_BridgeBinge
			RETURN <<-7.5758, 0.0000, 148.5448>>
		BREAK
		CASE SPTT_Vinewood_Flyby
			RETURN <<-1.0194, 0.7478, 23.0628>>
		BREAK
		CASE SPTT_Advanced_Race01
			RETURN <<-5.3368, 0.0006, 158.2570>>
		BREAK
		CASE SPTT_AirportFlyby
			RETURN <<-1.3523, 0.0000, 0.0891>>
		BREAK
		CASE SPTT_Altitude
			RETURN <<18.1009, -0.5992, -103.0214>>
		BREAK
	ENDSWITCH
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC FLOAT SPTT_GET_END_SCREEN_CAM_FOV()
	SWITCH INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur)
		CASE SPTT_BridgeBinge
			RETURN 46.9415
		BREAK
		CASE SPTT_Vinewood_Flyby
			RETURN 37.9558
		BREAK
		CASE SPTT_Advanced_Race01
			RETURN 48.4122
		BREAK
		CASE SPTT_AirportFlyby
			RETURN 35.2464
		BREAK
		CASE SPTT_Altitude
			RETURN 42.5338
		BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

//FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
//	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
//ENDFUNC

FLOAT fLerpValueX = 0.2, fLerpValueZ = 0.2
BOOL bTrackLeft, bTrackUp

PROC SPTT_DETERMINE_PLANE_TRACKING_CAM_DIRECTION(CAMERA_INDEX trackingCam)
	VECTOR vPFwd, vPPos, vCRight, vCUp, vCPos, vCRot, vTemp
	GET_ENTITY_MATRIX(PLAYER_PED_ID(), vPFwd, vTemp, vTemp, vPPos) //unit vectors
	
	vCRot = GET_CAM_ROT(trackingCam)
	vCPos = GET_CAM_COORD(trackingCam)
	vCRight = CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<vCRot.x + 90, vCRot.y, vCRot.z>>)
	vCUp = CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<vCRot.x, vCRot.y, vCRot.z + 90>>)
	
	//init lerp vals
	fLerpValueX = 0.2
	fLerpValueZ = 0.5

	VECTOR vPlaneToCam = (GET_CAM_COORD(trackingCam) - vPPos)
	VECTOR vPProj = vPPos + (DOT_PRODUCT(vPlaneToCam, vPFwd) * vPFwd)
	
	IF vPProj.z > vCPos.z
		bTrackUp = TRUE
		PRINTLN("Plane is going towards a point above the center of the screen")
		PRINTLN("Setting camera to TRACK UP")
	ELSE
		bTrackUp = FALSE
		PRINTLN("Setting camera to TRACK DOWN")
		PRINTLN("Plane is going towards a point below the center of the screen")	
	ENDIF
	
	PRINTLN("vPProj is... ", vPProj)
	PRINTLN("vCPos is... ", vCPos)
		
	FLOAT fDotProductRight, fDotProductUp
	
	fDotProductRight = DOT_PRODUCT(vPProj, vCRight)
	fDotProductUp = DOT_PRODUCT(vPProj, vCUp)
	
	IF fDotProductRight < 0
		bTrackLeft = TRUE
		PRINTLN("Plane is going towards the left of the center of the screen")
		PRINTLN("Setting camera to TRACK LEFT")
	ELSE
		bTrackLeft = FALSE
		PRINTLN("Plane is going towards the right of the center of the screen")	
		PRINTLN("Setting camera to TRACK RIGHT")
	ENDIF
	
	PRINTLN("fDotProductRight is... ", fDotProductRight)
	PRINTLN("fDotProductUp is... ", fDotProductUp)
	
ENDPROC

PROC SPTT_UPDATE_PLANE_TRACKING_CAM(CAMERA_INDEX trackingCam, VECTOR trackPos)

	
	IF NOT DOES_CAM_EXIST(trackingCam)
		PRINTLN("Tracking Camera doesnt exist")
		EXIT
	ENDIF
	IF NOT IS_CAM_ACTIVE(trackingCam)
		PRINTLN("Tracking Camera isn't active")
		EXIT
	ENDIF
	
	FLOAT  fDotProductUp
	VECTOR camRot, camPos, vCamUp, vCamToPlane
	FLOAT fNewPitch, fNewHeading
	
	camRot = GET_CAM_ROT(trackingCam)
	camPos = GET_CAM_COORD(trackingCam)

	vCamUp = CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<camRot.x, camRot.y, camRot.z + 90>>)
	vCamToPlane = trackPos - camPos
	vCamToPlane = NORMALISE_VECTOR(vCamToPlane)
	
	fNewPitch = ATAN2(trackPos.z - camPos.z, GET_DISTANCE_BETWEEN_COORDS(trackPos, camPos, FALSE))
	fNewHeading = GET_HEADING_FROM_VECTOR_2D(trackPos.x - camPos.x, trackPos.y - camPos.y)
	fNewHeading = WRAP(fNewHeading, -180, 180)
//	IF fNewHeading > 180
//		fNewHeading -= 360 //convert the heading from 0 to 360 to -180 to 180
//	ENDIF
	
//	DRAW_DEBUG_SPHERE(trackPos, 1, 255, 0, 0)
//	DRAW_DEBUG_SPHERE(camPos, 1, 0, 255, 0)	
//	DRAW_DEBUG_LINE(trackPos, camPos)
	
	fDotProductUp = DOT_PRODUCT(CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<fNewPitch, camRot.y, camRot.z>>), vCamUp)
	
	FLOAT fNewPitchDiff = fNewPitch - camRot.x

	//because of the jump from Loop HUGE to TINY
	IF fNewPitchDiff > 270.0
		fNewPitchDiff -= 360.0
	ELIF fNewPitchDiff < -270.0
		fNewPitchDiff += 360.0
	ENDIF
	
	fNewPitch = camRot.x + fNewPitchDiff
			
	IF (bTrackUp AND fNewPitch > camRot.x) OR (!bTrackUp AND fNewPitch < camRot.x)
		PRINTLN("Old pitch is... ", camRot.x, " and new pitch is... ", fNewPitch)
		IF fDotProductUp > 0
			PRINTLN("Going up, so pitch should be updating")
		ELIF fDotProductUp < 0
			PRINTLN("Going down, so pitch should be updating")			
		ELSE
			PRINTLN("Dot product is 0. Is the pitch the same??")
		ENDIF
		fLerpValueX *= 0.9
		camRot.x = LERP_FLOAT(camRot.x, fNewPitch, fLerpValueX)
	ELSE
		PRINTLN("Old pitch is... ", camRot.x, " and new pitch is... ", fNewPitch)
		PRINTLN("fDotProductUp is... ", fDotProductUp)
		PRINTLN("Not adjusting pitch")
	ENDIF
	
	
	FLOAT fNewHeadingDiff = fNewHeading - camRot.z

	//because of the jump from Loop HUGE to TINY
	IF fNewHeadingDiff > 270.0
		fNewHeadingDiff -= 360.0
	ELIF fNewHeadingDiff < -270.0
		fNewHeadingDiff += 360.0
	ENDIF
	
	fNewHeading = camRot.z + fNewHeadingDiff
	
	IF (bTrackLeft AND camRot.z - fNewHeading < 0) OR (!bTrackLeft AND camRot.z - fNewHeading > 0)
		PRINTLN("Old heading is... ", camRot.z, " and new heading is... ", fNewHeading)
		
		IF (!bTrackLeft AND camRot.z - fNewHeading > 0)
			PRINTLN("Going right, so heading should be updating")
		ELIF (bTrackLeft AND camRot.z - fNewHeading < 0)
			PRINTLN("Going left, so heading should be updating")
		ELSE
			PRINTLN("Dot product is 0. Is the heading the same??")
		ENDIF
		fLerpValueZ *= 0.9
		camRot.z = LERP_FLOAT(camRot.z, fNewHeading, fLerpValueZ)
	ELSE
		PRINTLN("Old heading is... ", camRot.z, " and new heading is... ", fNewHeading)
		PRINTLN("camRot.z - fNewHeading is... ", camRot.z - fNewHeading)
		PRINTLN("Not adjusting heading")
	ENDIF
	
	
	SET_CAM_ROT(trackingCam, camRot)
	

ENDPROC

FUNC BOOL SPTT_ATTEMPT_LB_WRITE(SPTT_RACE_STRUCT& Race, BOOL bSkipRankPrediction = FALSE)
	#IF IS_DEBUG_BUILD
		IF NOT Race.bPlayerUsedDebugSkip
	#ENDIF		
		IF IS_PLAYER_ONLINE()
			IF NOT bWrittenToLB
				INT iRank = 0
				IF Race.Racer[0].fClockTime > 0
					PRINTLN("Race.Racer[0].fClockTime is ", Race.Racer[0].fClockTime, " and SPTT_Master.fTimeGold[SPTT_Master.iRaceCur] is ", SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])
					IF Race.Racer[0].fClockTime <= SPTT_Master.fTimeGold[SPTT_Master.iRaceCur]
						PRINTLN("gold because Race.Racer[0].fClockTime is ", Race.Racer[0].fClockTime, " and SPTT_Master.fTimeGold[SPTT_Master.iRaceCur] is ", SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])
						iRank = 1
					ELIF Race.Racer[0].fClockTime <= ((SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur]-SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])/2)+SPTT_Master.fTimeGold[SPTT_Master.iRaceCur]
						PRINTLN("silver because Race.Racer[0].fClockTime is ", Race.Racer[0].fClockTime, " and SPTT_Master.fTimeGold[SPTT_Master.iRaceCur] is ", SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])
						iRank = 2
					ELIF Race.Racer[0].fClockTime <= SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur]
						PRINTLN("bronze because Race.Racer[0].fClockTime is ", Race.Racer[0].fClockTime, " and SPTT_Master.fTimeGold[SPTT_Master.iRaceCur] is ", SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])
						iRank = 3
					ENDIF
				ENDIF
				IF iRank = 0
					SCRIPT_ASSERT("rank is 0 for leaderboard!!!!")
					CDEBUG1LN( DEBUG_OR_RACES, "Oops! Trying to do a LB right but the clock time is too low!")				
				ENDIF
				SPTT_LOAD_SOCIAL_CLUB_LEADERBOARD(INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur), SPTT_Master.szRaceFileName[SPTT_Master.iRaceCur])
				IF bSkipRankPrediction
					SPTT_WRITE_TIME_TO_LEADERBOARD(INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur), Race.Racer[0].fClockTime, iRank)
					bWrittenToLB = TRUE
					RETURN TRUE
				ELSE
					IF SPTT_DO_RANK_PREDICTION(INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur), Race.Racer[0].fClockTime, iRank)
						bWrittenToLB = TRUE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF bSkipRankPrediction
				bWrittenToLB = TRUE
			ENDIF
			PRINTLN("Couldn't write to leaderboards because player is not online!!")
		ENDIF

	#IF IS_DEBUG_BUILD
		ENDIF
	#ENDIF
	
	RETURN FALSE
	
ENDFUNC


VECTOR vTrackingPosition, vCamPosition, vCamRotation
FLOAT fCamFOV, fDelayToStartTracking = 0.0



FUNC BOOL SPTT_UPDATING_END_SCREEN(SPTT_RACE_STRUCT& Race, BOOL bEndTracking, BOOL bEndShake)
	STOP_CONTROL_SHAKE(PLAYER_CONTROL)
	
	SWITCH eSPREndScreenState
		CASE SPTT_END_SCREEN_INIT
			#IF IS_DEBUG_BUILD
				DEBUG_MESSAGE("Update_Ending_Screen : ENDING_INIT")
			#ENDIF
			bWrittenToLB = FALSE
			vCamPosition = SPTT_GET_END_SCREEN_CAM_POS()
			vCamRotation = SPTT_GET_END_SCREEN_CAM_ROT()
			fCamFOV = SPTT_GET_END_SCREEN_CAM_FOV()
//			SPTT_UI_MedalToast.siMovie = REQUEST_MG_MEDAL_TOAST()
			DISPLAY_RADAR(FALSE)
			CLEAR_HELP()
			CLEAR_PRINTS()
			eSPREndScreenState = SPTT_END_SCREEN_SETUP
			BREAK
		
		CASE SPTT_END_SCREEN_SETUP
			
			IF IS_PLAYER_PLAYING(PLAYER_ID()) 
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					ANIMPOSTFX_PLAY("MinigameEndMichael", 0, FALSE)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					ANIMPOSTFX_PLAY("MinigameEndFranklin", 0, FALSE)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					ANIMPOSTFX_PLAY("MinigameEndTrevor", 0, FALSE)
				ENDIF

				iSPRStuntFinishCam1 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPosition, vCamRotation, fCamFOV , TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				IF bEndShake
					SHAKE_CAM(iSPRStuntFinishCam1, "HAND_SHAKE", 0.07)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(Race.Racer[0].Vehicle) AND IS_ENTITY_IN_AIR(Race.Racer[0].Vehicle)
					TASK_VEHICLE_MISSION_COORS_TARGET(PLAYER_PED_ID(), Race.Racer[0].Vehicle, <<vCamPosition.x, vCamPosition.y, vCamPosition.z + 15>>, MISSION_GOTO, GET_ENTITY_SPEED(Race.Racer[0].Vehicle), DRIVINGMODE_PLOUGHTHROUGH, 10.0, 10.0)
				ENDIF
			
				POINT_CAM_AT_COORD(iSPRStuntFinishCam1, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				
				eSPREndScreenState = SPTT_END_SCREEN_POINT_CAM
				
				
			ENDIF
			BREAK
			
		CASE SPTT_END_SCREEN_POINT_CAM
			STOP_CAM_POINTING(iSPRStuntFinishCam1)
			SPTT_DETERMINE_PLANE_TRACKING_CAM_DIRECTION(iSPRStuntFinishCam1)
			START_TIMER_NOW_SAFE(SPRStuntCamTimer)
			eSPREndScreenState = SPTT_END_DETERMINE_TRACKING
			BREAK
			
		CASE SPTT_END_DETERMINE_TRACKING
			IF TIMER_DO_ONCE_WHEN_READY(SPRStuntCamTimer, fDelayToStartTracking)
				START_TIMER_NOW_SAFE(SPRStuntCamTimer)
				SPTT_DETERMINE_PLANE_TRACKING_CAM_DIRECTION(iSPRStuntFinishCam1)
				eSPREndScreenState = SPTT_END_HOLD_SHOT
			ENDIF
			BREAK
		
		CASE SPTT_END_HOLD_SHOT
			
			IF bEndTracking
				IF IS_ENTITY_ON_SCREEN(Race.Racer[0].Vehicle) AND IS_ENTITY_IN_AIR(Race.Racer[0].Vehicle)
					vTrackingPosition = GET_ENTITY_COORDS(Race.Racer[0].Vehicle)
					START_TIMER_NOW_SAFE(SPRStuntCamTimer)
				ENDIF				
				SPTT_UPDATE_PLANE_TRACKING_CAM(iSPRStuntFinishCam1, vTrackingPosition)
			ENDIF
			IF GET_TIMER_IN_SECONDS_SAFE(SPRStuntCamTimer) >= 1.0 
				eSPREndScreenState = SPTT_END_QUICK_PAN_SETUP				
			ENDIF
			BREAK
			
		CASE SPTT_END_QUICK_PAN_SETUP
			RESTART_TIMER_NOW(SPRStuntCamTimer)
			IF DOES_CAM_EXIST(iSPRStuntFinishCam)
				DESTROY_CAM(iSPRStuntFinishCam)
			ENDIF
			ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
			STOP_CAM_SHAKING(iSPRStuntFinishCam1)
			vCamPosition = GET_CAM_COORD(iSPRStuntFinishCam1)
			vCamRotation = GET_CAM_ROT(iSPRStuntFinishCam1)
			iSPRStuntFinishCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<vCamPosition.x, vCamPosition.y, vCamPosition.z + 1000>>, <<90, vCamRotation.y, vCamRotation.z>>, fCamFOV)
			SET_CAM_ACTIVE_WITH_INTERP(iSPRStuntFinishCam, iSPRStuntFinishCam1, 500)
			PLAY_SOUND_FRONTEND(-1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")			
			eSPREndScreenState = SPTT_END_QUICK_PAN_UPDATE
			BREAK
			
		CASE SPTT_END_QUICK_PAN_UPDATE
		IF NOT IS_CAM_INTERPOLATING(iSPRStuntFinishCam) AND NOT IS_CAM_INTERPOLATING(iSPRStuntFinishCam1)
				IF (Race.Racer[0].fClockTime <= SPTT_Master.fTimeGold[SPTT_Master.iRaceCur]) 
					PLAY_SOUND_FRONTEND(-1, "MEDAL_GOLD", "HUD_AWARDS")
					PRINTLN("PLAYING: MEDAL_GOLD")
				ELIF (Race.Racer[0].fClockTime <= ((SPTT_Master.fTimeGold[SPTT_Master.iRaceCur]-SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur])/2)+SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur]) 
					PLAY_SOUND_FRONTEND(-1, "MEDAL_SILVER", "HUD_AWARDS")
					PRINTLN("PLAYING: MEDAL_SILVER")
				ELIF (Race.Racer[0].fClockTime <= SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur]) 
					PLAY_SOUND_FRONTEND(-1, "MEDAL_BRONZE", "HUD_AWARDS")
					PRINTLN("PLAYING: MEDAL_BRONZE")
				ELSE
					MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
					PRINTLN("PLAYING: MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC")
				ENDIF
				eSPREndScreenState = SPTT_END_SFX_AND_SPLASH_SETUP
			ENDIF
			BREAK
			
		CASE SPTT_END_SFX_AND_SPLASH_SETUP
			//when the scaleform splash text is done
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			ENDIF
			SPTT_SETUP_SCORECARD(Race)
			MG_INIT_FAIL_FADE_EFFECT(SPTT_Master.failFadeEffect, TRUE)
			RESTART_TIMER_NOW(SPRStuntCamTimer)
			IF IS_VEHICLE_DRIVEABLE(Race.Racer[0].Vehicle)				
				sRadStation = GET_PLAYER_RADIO_STATION_NAME()
				SET_VEHICLE_RADIO_ENABLED(Race.Racer[0].Vehicle, FALSE)				
			ENDIF
			
			eSPREndScreenState = SPTT_END_SPLASH_UPDATE				
			BREAK
			
		CASE SPTT_END_SPLASH_UPDATE
			IF SPTT_NEW_DRAW_SCORECARD(Race)
				eSPREndScreenState = SPTT_END_CLEANUP				
			ENDIF
			BREAK

		CASE SPTT_END_CLEANUP
			//clear
			ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
			CLEANUP_MG_MEDAL_TOAST(SPTT_UI_MedalToast)
			SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)
			SPTT_ADD_MEDAL_FEED(Race)
			MAKE_AUTOSAVE_REQUEST()
			SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE)
			eSPREndScreenState=SPTT_END_SCREEN_INIT
			RETURN FALSE
			BREAK	
	ENDSWITCH
	
	SPTT_ATTEMPT_LB_WRITE(Race)
	
	RETURN TRUE
	
	
ENDFUNC

PROC Cleanup_Finish_Camera()
	IF DOES_CAM_EXIST(iSPRStuntFinishCam)	
		IF IS_CAM_ACTIVE(iSPRStuntFinishCam)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_CAM_ACTIVE(iSPRStuntFinishCam, FALSE)
		ENDIF
		DESTROY_CAM(iSPRStuntFinishCam)	
	ENDIF
ENDPROC

PROC Make_Finish_Camera(VEHICLE_INDEX& iPlayersPlane)
	VECTOR vCamPos, vCamRot
	FLOAT fFov = GET_GAMEPLAY_CAM_FOV()
//	FLOAT fPlayerHeading, fTempHeading, fHeadingDelta
	vCamPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(iPlayersPlane, <<0, -5, 0>>)		
	vCamRot = GET_GAMEPLAY_CAM_ROT()
	vCamPos.z += 25.0 //25m above the player.
	vCamRot.x = 60
	
//	fTempHeading = GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(iPlayersPlane), vCamPos)
//	fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
//	fHeadingDelta = fPlayerHeading - fTempHeading 

//	IF fHeadingDelta > 180
//    	fHeadingDelta -= 360
//	ELIF fHeadingDelta < -180
//    	fHeadingDelta += 360
//	ENDIF

//	IF fHeadingDelta < 0
//    	vCamRot.z -= 90
//	ELSE
//    	vCamRot.z += 90
//	ENDIF
	IF NOT DOES_CAM_EXIST(iSPRStuntFinishCam)
		iSPRStuntFinishCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vCamPos, vCamRot, fFov, FALSE)
	ENDIF
ENDPROC

PROC SPTT_Race_Stunt_Manage_Finish_Camera(VEHICLE_INDEX& iPlayersPlane)
	IF NOT DOES_ENTITY_EXIST(iPlayersPlane)
		PRINTLN("SPTT_Race_Stunt_Manage_Finish_Camera: iPlayersPlane doesnt exist, exiting")
		PRINTNL()
		EXIT
	ENDIF 

	SWITCH eSPRStuntCutFinish
		CASE SPTT_STUNT_FINISH_INIT
			PRINTLN("SPTT_Race_Stunt_Manage_Finish_Camera: inside SPTT_STUNT_FINISH_INIT")
			PRINTNL()
			IF ((NOT DOES_CAM_EXIST(iSPRStuntFinishCam))
			OR (NOT ARE_VECTORS_ALMOST_EQUAL(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(iPlayersPlane, <<0, -5, 0>>), GET_CAM_COORD(iSPRStuntFinishCam), 10.0)))
				PRINTLN("SPTT_Race_Stunt_Manage_Finish_Camera: running Make_Finish_Cam")
				PRINTNL()
				Make_Finish_Camera(iPlayersPlane)
			ENDIF
			IF NOT IS_CAM_ACTIVE(iSPRStuntFinishCam)			
				SET_CAM_ACTIVE(iSPRStuntFinishCam, TRUE)
			ELSE
				RENDER_SCRIPT_CAMS(TRUE, TRUE, 3000)
				SET_ENTITY_CAN_BE_DAMAGED(iPlayersPlane, FALSE)
				START_TIMER_NOW(SPRStuntCamTimer)				
				eSPRStuntCutFinish = SPTT_STUNT_FINISH_AUTOPILOT
			ENDIF
		BREAK
		
		CASE SPTT_STUNT_FINISH_AUTOPILOT	
			PRINTLN("SPTT_Race_Stunt_Manage_Finish_Camera: inside SPTT_STUNT_FINISH_AUTOPILOT")
			PRINTNL()
			IF NOT DOES_CAM_EXIST(iSPRStuntFinishCam)
			OR NOT IS_CAM_ACTIVE(iSPRStuntFinishCam)
				eSPRStuntCutFinish = SPTT_STUNT_FINISH_INIT
			ELSE
				VECTOR vCurPlaneRotation//, vCurCamRotation
//				vCurCamRotation = GET_CAM_ROT(iSPRStuntFinishCam)
				vCurPlaneRotation = GET_ENTITY_ROTATION(iPlayersPlane)		
				IF GET_TIMER_IN_SECONDS(SPRStuntCamTimer) > 0.5																				
					SET_VEHICLE_FORWARD_SPEED(iPlayersPlane, SPTT_PLANE_SPD_MAX)
//					IF vCurPlaneRotation.x < 45 //x is pitch
//						vCurPlaneRotation.x++
//					ENDIF						
//					IF ABSF(vCurPlaneRotation.x - vCurCamRotation.x) < 60
//						vCurPlaneRotation.x++
//					ENDIF
//					IF vCurPlaneRotation.z <= vCurCamRotation.z //z is yaw/heading
//						IF ABSF(vCurPlaneRotation.z - vCurCamRotation.z) < 90
//							vCurPlaneRotation.z--
//						ENDIF
//						IF ABSF(vCurPlaneRotation.y) < 90 //y is roll
//							vCurPlaneRotation.y++
//						ENDIF
//					ELSE
//						IF ABSF(vCurPlaneRotation.z - vCurCamRotation.z) < 90
//							vCurPlaneRotation.z++
//						ENDIF
//						IF ABSF(vCurPlaneRotation.y) < 90 //y is roll
//							vCurPlaneRotation.y--
//						ENDIF
//					ENDIF
//					SET_ENTITY_ROTATION(iPlayersPlane, vCurPlaneRotation)											
					//FREEZE_ENTITY_POSITION(iPlayersPlane, FALSE)						
				ELSE
					PLANE_AUTOPILOT(iPlayersPlane, SPTT_PLANE_SPD_MAX, TRUE, vCurPlaneRotation.x, vCurPlaneRotation.y)				
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH	
	
	IF DOES_CAM_EXIST(iSPRStuntFinishCam)
	AND IS_SCREEN_FADED_OUT()	
		DESTROY_CAM(iSPRStuntFinishCam)		
		RENDER_SCRIPT_CAMS(FALSe, FALSE)
		eSPRStuntCutFinish = SPTT_STUNT_FINISH_INIT
	ENDIF
ENDPROC

PROC SPTT_Racer_AutoPilot(SPTT_RACER_STRUCT& Racer, FLOAT fSpeed, BOOL bRotInterp)
	//DEBUG_MESSAGE("SPTT_Racer_AutoPilot")
	IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
		IF IS_THIS_MODEL_A_PLANE(Racer.eVehicleModel)
			VECTOR vCurPlaneRotation = GET_ENTITY_ROTATION(Racer.Vehicle)
			PLANE_AUTOPILOT(Racer.Vehicle, fSpeed, bRotInterp, vCurPlaneRotation.x)
		ENDIF
	ENDIF
ENDPROC

PROC SPTT_Racer_AutoCircle(SPTT_RACER_STRUCT& Racer, FLOAT fSpeed, BOOL bRotInterp)
	//DEBUG_MESSAGE("SPTT_Racer_AutoCircle")
	IF NOT IS_ENTITY_DEAD(Racer.Vehicle)
		IF IS_THIS_MODEL_A_PLANE(Racer.eVehicleModel)
			// TODO: Decide if this check is even needed...
			IF (GET_ENTITY_HEIGHT_ABOVE_GROUND(Racer.Vehicle) > SPTT_ON_GROUND_DIST)
				PLANE_AUTOCIRCLE(Racer.Vehicle, fSpeed, bRotInterp)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC SPTT_Race_Racer_AutoPilot(SPTT_RACE_STRUCT& Race)
	//DEBUG_MESSAGE("SPTT_Race_Racer_AutoPilot")
	
	// Reset gameplay camera behind player.
//	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	
	// Turn off player control just in case
	IF IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
		SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), FALSE, SPC_ALLOW_PLAYER_DAMAGE)
	ENDIF
		
	// Loop through all racers and auto-pilot them.
	INT i
	REPEAT Race.iRacerCnt i		
		SPTT_Racer_AutoPilot(Race.Racer[i], SPTT_PLANE_SPD_MAX, TRUE)
	ENDREPEAT
	
ENDPROC

PROC SPTT_Race_Racer_AutoCircle(SPTT_RACE_STRUCT& Race)
	//DEBUG_MESSAGE("SPTT_Race_Racer_AutoCircle")

	// Reset gameplay camera behind player.
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	
	// Loop through all racers and auto-circle them.
	INT i
	REPEAT Race.iRacerCnt i
		SPTT_Racer_AutoCircle(Race.Racer[i], SPTT_PLANE_SPD_MAX, TRUE)
	ENDREPEAT
	
ENDPROC


// Stunt Clock adjustments
PROC SPTT_Racer_ClockTime_Bonus(SPTT_RACER_STRUCT& Racer, FLOAT fBonus)
	//DEBUG_MESSAGE("SPTT_Racer_ClockTime_Bonus")
	Racer.fPlsMnsLst = fBonus
	Racer.fPlsMnsTot += Racer.fPlsMnsLst
ENDPROC

PROC SPTT_Racer_ClockTime_Penalty(SPTT_RACER_STRUCT& Racer, FLOAT fPenalty)
	//DEBUG_MESSAGE("SPTT_Racer_ClockTime_Penalty")
	Racer.fPlsMnsLst = -fPenalty
	Racer.fPlsMnsTot += Racer.fPlsMnsLst
ENDPROC


PROC SPTT_Gate_Reward(SPTT_RACER_STRUCT& Racer, SPTT_RACE_GATE_STATUS status)
	SWITCH status
		CASE SPTT_RACE_GATE_STATUS_MISSED_GATE
			SPTT_Racer_ClockTime_Penalty(Racer, SPTT_GATE_MISS_PENALTY)
			//PRINT_NOW("SPTT_FAIL_GATE", SPTT_GATE_MISS_DISP_TIME, 1)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_PASS
			//PRINT_NOW("SPTT_PASS_GATE", SPTT_GATE_MISS_DISP_TIME, 1)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_PASS_INNER
			SPTT_Racer_ClockTime_Bonus(Racer, SPTT_GATE_INNER_PASS_BONUS)
			//PRINT_NOW("SPTT_KNIFE_STUNT", SPTT_GATE_MISS_DISP_TIME, 1)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_PASS_OUTTER
			SPTT_Racer_ClockTime_Bonus(Racer, SPTT_GATE_OUTTER_PASS_BONUS)
			//PRINT_NOW("SPTT_KNIFE_STUNT", SPTT_GATE_MISS_DISP_TIME, 1)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_KNIFE_OUTTER
			SPTT_Racer_ClockTime_Bonus(Racer, SPTT_GATE_OUTTER_KNIFE_BONUS)
			//PRINT_NOW("SPTT_KNIFE_STUNT", SPTT_GATE_MISS_DISP_TIME, 1)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_INVERT_OUTTER
			SPTT_Racer_ClockTime_Bonus(Racer, SPTT_GATE_OUTTER_INVERT_BONUS)
			//PRINT_NOW("SPTT_KNIFE_STUNT", SPTT_GATE_MISS_DISP_TIME, 1)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_KNIFE_INNER
			SPTT_Racer_ClockTime_Bonus(Racer, SPTT_GATE_INNER_KNIFE_BONUS)
			//PRINT_NOW("SPTT_KNIFE_STUNT", SPTT_GATE_MISS_DISP_TIME, 1)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_INVERT_INNER
			SPTT_Racer_ClockTime_Bonus(Racer, SPTT_GATE_INNER_INVERT_BONUS)
			//PRINT_NOW("SPTT_KNIFE_STUNT", SPTT_GATE_MISS_DISP_TIME, 1)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_STUNT_KL
			SPTT_Racer_ClockTime_Bonus(Racer, SPTT_GATE_STUNT_BONUS)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_STUNT_KL_HIT
			SPTT_Racer_ClockTime_Penalty(Racer, SPTT_GATE_STUNT_HIT)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_STUNT_KL_MISS
			SPTT_Racer_ClockTime_Penalty(Racer, SPTT_GATE_MISS_PENALTY)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_STUNT_KR
			SPTT_Racer_ClockTime_Bonus(Racer, SPTT_GATE_STUNT_BONUS)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_STUNT_KR_HIT
			SPTT_Racer_ClockTime_Penalty(Racer, SPTT_GATE_STUNT_HIT)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_STUNT_KR_MISS
			SPTT_Racer_ClockTime_Penalty(Racer, SPTT_GATE_MISS_PENALTY)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_STUNT_INV
			SPTT_Racer_ClockTime_Bonus(Racer, SPTT_GATE_STUNT_BONUS)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_STUNT_INV_HIT
			SPTT_Racer_ClockTime_Penalty(Racer, SPTT_GATE_STUNT_HIT)
		BREAK
		CASE SPTT_RACE_GATE_STATUS_STUNT_INV_MISS
			SPTT_Racer_ClockTime_Penalty(Racer, SPTT_GATE_MISS_PENALTY)
		BREAK

	ENDSWITCH
ENDPROC

PROC runGateTimers(SPTT_RACER_STRUCT& Racer, BOOL bRacerIsPlayer, INT iGateCheck)

	IF bRacerIsPlayer
		IF SPTT_Master.eRaceType = SPTT_RACE_TYPE_PLANE
			IF IS_TIMER_STARTED(missedGate)
				ADJUST_TIMER(missedGate, 3.0)
			ENDIF
			IF IS_TIMER_STARTED(stuntLoop)
				ADJUST_TIMER(stuntLoop, 3.0)
			ENDIF
			IF IS_TIMER_STARTED(stuntRoll)
				ADJUST_TIMER(stuntRoll, 3.0)
			ENDIF
			IF IS_TIMER_STARTED(stuntFail)
				ADJUST_TIMER(stuntFail, 3.0)
			ENDIF
			IF IS_TIMER_STARTED(stuntInverted)
				ADJUST_TIMER(stuntInverted, 3.0)
			ENDIF
			IF IS_TIMER_STARTED(clearedGate)
				ADJUST_TIMER(clearedGate, 3.0)
			ENDIF
			IF IS_TIMER_STARTED(centerGate)
				ADJUST_TIMER(centerGate, 3.0)
			ENDIF
			IF IS_TIMER_STARTED(clearedKnife)
				ADJUST_TIMER(clearedKnife, 3.0)
			ENDIF
			IF IS_TIMER_STARTED(clearedInvert)
				ADJUST_TIMER(clearedInvert, 3.0)
			ENDIF
			IF IS_TIMER_STARTED(centerKnife)
				ADJUST_TIMER(centerKnife, 3.0)
			ENDIF
			IF IS_TIMER_STARTED(centerInvert)
				ADJUST_TIMER(centerInvert, 3.0)
			ENDIF
			SPTT_Gate_Reward(Racer, INT_TO_ENUM(SPTT_RACE_GATE_STATUS, iGateCheck))
		
			SWITCH INT_TO_ENUM(SPTT_RACE_GATE_STATUS, iGateCheck)
				CASE SPTT_RACE_GATE_STATUS_MISSED_GATE
					bMissedGate = TRUE
					RESTART_TIMER_NOW(missedGate)															
				BREAK
					
				CASE SPTT_RACE_GATE_STATUS_PASS_INNER
					bInnerGate = TRUE
					RESTART_TIMER_NOW(centerGate)
				BREAK

				CASE SPTT_RACE_GATE_STATUS_PASS_OUTTER
					bOutterGate = TRUE
					RESTART_TIMER_NOW(clearedGate)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_KNIFE_INNER
					bInnerKnife = TRUE
					RESTART_TIMER_NOW(centerKnife)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_INVERT_INNER
					bInnerInverted = TRUE
					RESTART_TIMER_NOW(centerInvert)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_KNIFE_OUTTER
					bOutterKnife = TRUE
					RESTART_TIMER_NOW(clearedKnife)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_INVERT_OUTTER
					bOutterInverted = TRUE
					RESTART_TIMER_NOW(clearedInvert)
				BREAK	
				
				CASE SPTT_RACE_GATE_STATUS_STUNT_KL
					bStuntKniL = TRUE
					RESTART_TIMER_NOW(stuntRoll)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_STUNT_KL_HIT
					bStuntKniLHit = TRUE
					RESTART_TIMER_NOW(stuntFail)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_STUNT_KL_MISS
					bStuntKniLMiss = TRUE
					RESTART_TIMER_NOW(missedGate)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_STUNT_KR
					bStuntKniR = TRUE
					RESTART_TIMER_NOW(stuntRoll)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_STUNT_KR_HIT
					bStuntKniRHit = TRUE
					RESTART_TIMER_NOW(stuntFail)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_STUNT_KR_MISS
					bStuntKniRMiss = TRUE
					RESTART_TIMER_NOW(missedGate)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_STUNT_INV
					bStuntInv = TRUE
					RESTART_TIMER_NOW(stuntInverted)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_STUNT_INV_HIT
					bStuntInvHit = TRUE
					RESTART_TIMER_NOW(stuntFail)
				BREAK
				
				CASE SPTT_RACE_GATE_STATUS_STUNT_INV_MISS
					bStuntInvMiss = TRUE
					RESTART_TIMER_NOW(missedGate)
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC


PROC rewardPlayer()

// Gates
	// Do Stunt Gate Times HERE
	IF bMissedGate = TRUE
		IF IS_TIMER_STARTED(missedGate)
			IF (GET_TIMER_IN_SECONDS(missedGate) <= 2.5)	
				sStuntName = "GATEMISS"
				fStuntValue = SPTT_GATE_MISS_PENALTY
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bMissedGate = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bInnerGate = TRUE
		IF IS_TIMER_STARTED(centerGate)
			IF (GET_TIMER_IN_SECONDS(centerGate) <= 2.5)			
				sStuntName = "GATEINNER"
				fStuntValue = SPTT_GATE_INNER_PASS_BONUS
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bInnerGate = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bOutterGate = TRUE
		IF IS_TIMER_STARTED(clearedGate)
			IF (GET_TIMER_IN_SECONDS(clearedGate) <= 2.5)			
				sStuntName = "GATEOUTTER"
				fStuntValue = SPTT_GATE_OUTTER_PASS_BONUS
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bOutterGate = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bInnerKnife = TRUE
		IF IS_TIMER_STARTED(centerKnife)
			IF (GET_TIMER_IN_SECONDS(centerKnife) <= 2.5)			
				sStuntName = "GATEKNIFEINNER"
				fStuntValue = SPTT_GATE_INNER_KNIFE_BONUS
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bInnerKnife = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bInnerInverted = TRUE
		IF IS_TIMER_STARTED(centerInvert)
			IF (GET_TIMER_IN_SECONDS(centerInvert) <= 2.5)			
				sStuntName = "GATEINVERTEDIN"
				fStuntValue = SPTT_GATE_INNER_INVERT_BONUS
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bInnerInverted = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bOutterKnife = TRUE
		IF IS_TIMER_STARTED(clearedKnife)
			IF (GET_TIMER_IN_SECONDS(clearedKnife) <= 2.5)			
				sStuntName = "GATEKNIFEOUTTER"
				fStuntValue = SPTT_GATE_OUTTER_KNIFE_BONUS
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bOutterKnife = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bOutterInverted = TRUE
		IF IS_TIMER_STARTED(clearedInvert)
			IF (GET_TIMER_IN_SECONDS(clearedInvert) <= 2.5)			
				sStuntName = "GATEINVERTEDOUT"
				fStuntValue = SPTT_GATE_OUTTER_INVERT_BONUS
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bOutterInverted = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bStuntKniL = TRUE
		IF IS_TIMER_STARTED(stuntRoll)
			IF (GET_TIMER_IN_SECONDS(stuntRoll) <= 2.5)			
				sStuntName = "STUNTKNIFELEFT"
				fStuntValue = SPTT_GATE_STUNT_BONUS
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bStuntKniL = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bStuntKniLHit = TRUE
		IF IS_TIMER_STARTED(stuntRoll)
			IF (GET_TIMER_IN_SECONDS(stuntRoll) <= 2.5)			
				sStuntName = "STUNTKNIFELEFTHIT"
				fStuntValue = SPTT_GATE_STUNT_HIT
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bStuntKniLHit = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bStuntKniLMiss = TRUE
		IF IS_TIMER_STARTED(stuntRoll)
			IF (GET_TIMER_IN_SECONDS(stuntRoll) <= 2.5)			
				sStuntName = "STUNTKNIFELEFTMISS"
				fStuntValue = SPTT_GATE_MISS_PENALTY
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bStuntKniLMiss = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bStuntKniR = TRUE
		IF IS_TIMER_STARTED(stuntRoll)
			IF (GET_TIMER_IN_SECONDS(stuntRoll) <= 2.5)			
				sStuntName = "STUNTKNIFERIGHT"
				fStuntValue = SPTT_GATE_STUNT_BONUS
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bStuntKniR = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bStuntKniRHit = TRUE
		IF IS_TIMER_STARTED(stuntRoll)
			IF (GET_TIMER_IN_SECONDS(stuntRoll) <= 2.5)			
				sStuntName = "STUNTKNIFERIGHTHIT"
				fStuntValue = SPTT_GATE_STUNT_HIT
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bStuntKniRHit = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bStuntKniRMiss = TRUE
		IF IS_TIMER_STARTED(stuntRoll)
			IF (GET_TIMER_IN_SECONDS(stuntRoll) <= 2.5)			
				sStuntName = "STUNTKNIFERIGHTMISS"
				fStuntValue = SPTT_GATE_MISS_PENALTY
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bStuntKniRMiss = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bStuntInv = TRUE
		IF IS_TIMER_STARTED(stuntInverted)
			IF (GET_TIMER_IN_SECONDS(stuntInverted) <= 2.5)			
				sStuntName = "STUNTINVERTED"
				fStuntValue = SPTT_GATE_STUNT_BONUS
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bStuntInv = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bStuntInvHit = TRUE
		IF IS_TIMER_STARTED(stuntInverted)
			IF (GET_TIMER_IN_SECONDS(stuntInverted) <= 2.5)			
				sStuntName = "STUNTINVERTEDHIT"
				fStuntValue = SPTT_GATE_STUNT_HIT
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bStuntInvHit = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF bStuntInvMiss = TRUE
		IF IS_TIMER_STARTED(stuntInverted)
			IF (GET_TIMER_IN_SECONDS(stuntInverted) <= 2.5)			
				sStuntName = "STUNTINVERTEDMISS"
				fStuntValue = SPTT_GATE_MISS_PENALTY
				iStuntValueColor = ENUM_TO_INT(HUD_COLOUR_WHITE)
			ELSE
				sStuntName = NULL
				fStuntValue = -1
				iStuntValueColor = 1
				bStuntInvMiss = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INIT_SCREEN_SPTT_SCORECARD(MEGA_PLACEMENT_TOOLS &thisPlacement)
#IF COMPILE_WIDGET_OUTPUT
	thisPlacement.txtScreenName = "empty"
	thisPlacement.txtFileName = "empty"
	thisPlacement.txtFilePath = "empty"
	thisPlacement.SpritePlacement[SPTT_SC_SOCIALCLUB_IMG].txtSpriteName = "SC_SOCIALCLUB_IMG"
	thisPlacement.SpritePlacement[SPTT_SC_HITS_IMG].txtSpriteName = "SPTT_SC_HITS_IMG"
	thisPlacement.SpritePlacement[SPTT_SC_HITS_YELLOW_IMG].txtSpriteName = "SC_HITS_YELLOW_IMG"
	thisPlacement.SpritePlacement[SPTT_SC_HITS_RED_IMG].txtSpriteName = "SC_HITS_RED_IMG"
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLUE_IMG].txtSpriteName = "SC_HITS_BLUE_IMG"
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLACK_IMG].txtSpriteName = "SC_HITS_BLACK_IMG"
	thisPlacement.SpritePlacement[SPTT_SC_MEDAL_AWARD_IMG].txtSpriteName = "SC_MEDAL_AWARD_IMG"
	thisPlacement.SpritePlacement[SPTT_SC_BRONZE_IMG].txtSpriteName = "SC_BRONZE_IMG"
	thisPlacement.SpritePlacement[SPTT_SC_SILVER_IMG].txtSpriteName = "SC_SILVER_IMG"
	thisPlacement.SpritePlacement[SPTT_SC_GOLD_IMG].txtSpriteName = "SC_GOLD_IMG"
	thisPlacement.TextPlacement[SPTT_SC_SCORE_TITLE].strName = "SC_SCORE_TITLE"
	thisPlacement.TextPlacement[SPTT_SC_HITS_YELLOW_TXT].strName = "SC_HITS_YELLOW_TXT"
	thisPlacement.TextPlacement[SPTT_SC_HITS_RED_TXT].strName = "SC_HITS_RED_TXT"
	thisPlacement.TextPlacement[SPTT_SC_HITS_BLUE_TXT].strName = "SC_HITS_BLUE_TXT"
	thisPlacement.TextPlacement[SPTT_SC_HITS_BLACK_TXT].strName = "SC_HITS_BLACK_TXT"
	thisPlacement.TextPlacement[SPTT_SC_AWARD_TXT].strName = "SC_AWARD_TXT"
	thisPlacement.TextPlacement[SPTT_SC_SCORE_TXT].strName = "SC_SCORE_TXT"
	thisPlacement.TextPlacement[SPTT_SC_HISCORE_TXT].strName = "SC_HISCORE_TXT"
	thisPlacement.TextPlacement[SPTT_SC_WEAPON_TXT].strName = "SC_WEAPON_TXT"
	thisPlacement.TextPlacement[SPTT_SC_FIRED_TXT].strName = "SC_FIRED_TXT"
	thisPlacement.TextPlacement[SPTT_SC_HITS_TXT].strName = "SC_HITS_TXT"
	thisPlacement.TextPlacement[SPTT_SC_ACC_TXT].strName = "SC_ACC_TXT"
	thisPlacement.TextPlacement[SPTT_SC_TIMEREM_TXT].strName = "SC_TIMEREM_TXT"
	thisPlacement.TextPlacement[SPTT_SC_SCOREVAL_TXT].strName = "SPTT_SC_SCOREVAL_TXT"
	thisPlacement.TextPlacement[SPTT_SC_HISCOREVAL_TXT].strName = "SPTT_SC_HISCOREVAL_TXT"
	thisPlacement.TextPlacement[SPTT_SC_TOTALVAL_TXT].strName = "SPTT_SC_TOTALVAL_TXT"
	thisPlacement.TextPlacement[SPTT_SC_WEAPONVAL_TXT].strName = "SPTT_SC_WEAPONV_TXT"
/*	thisPlacement.TextPlacement[SPTT_SC_FIREDVAL_TXT].strName = "SPTT_SC_FIREDVAL_TXT"
	thisPlacement.TextPlacement[SPTT_SC_HITSVAL_TXT].strName = "SPTT_SC_HITSVAL_TXT"
	thisPlacement.TextPlacement[SPTT_SC_ACCVAL_TXT].strName = "SPTT_SC_ACCVAL_TXT"
	thisPlacement.TextPlacement[SPTT_SC_TIMEREMVAL_TXT].strName = "SPTT_SC_TIMEREMVAL_TXT"
*/	thisPlacement.TextPlacement[SPTT_SC_BRONZE_GOAL_TXT].strName = "SC_BRONZE_GOAL_TXT"
	thisPlacement.TextPlacement[SPTT_SC_SILVER_GOAL_TXT].strName = "SC_SILVER_GOAL_TXT"
	thisPlacement.TextPlacement[SPTT_SC_GOLD_GOAL_TXT].strName = "SC_GOLD_GOAL_TXT"
	thisPlacement.RectPlacement[SPTT_SC_SCORE_BG].txtRectName = "SC_SCORE_BG"
	thisPlacement.RectPlacement[SPTT_SC_SCORE_EDGE].txtRectName = "SC_SCORE_EDGE"
	thisPlacement.RectPlacement[SPTT_SC_HIT_BG].txtRectName = "SC_HIT_BG"
	thisPlacement.RectPlacement[SPTT_SC_INFO1_BG].txtRectName = "SC_INFO1_BG"
	thisPlacement.RectPlacement[SPTT_SC_INFO2_BG].txtRectName = "SC_INFO2_BG"
	thisPlacement.RectPlacement[SPTT_SC_INFO3_BG].txtRectName = "SC_INFO3_BG"
	thisPlacement.RectPlacement[SPTT_SC_INFO4_BG].txtRectName = "SC_INFO4_BG"
	thisPlacement.RectPlacement[SPTT_SC_INFO5_BG].txtRectName = "SC_INFO5_BG"
	thisPlacement.RectPlacement[SPTT_SC_INFO6_BG].txtRectName = "SC_INFO6_BG"
	thisPlacement.RectPlacement[SPTT_SC_INFO7_BG].txtRectName = "SC_INFO7_BG"
	thisPlacement.RectPlacement[SPTT_SC_INFO8_BG].txtRectName = "SC_INFO8_BG"
	thisPlacement.RectPlacement[SPTT_SC_BRONZE_BG].txtRectName = "SC_BRONZE_BG"
	thisPlacement.RectPlacement[SPTT_SC_SILVER_BG].txtRectName = "SC_SILVER_BG"
	thisPlacement.RectPlacement[SPTT_SC_GOLD_BG].txtRectName = "SC_GOLD_BG"
	thisPlacement.RectPlacement[SPTT_SC_BRONZE_OVERLAY].txtRectName = "SC_BRONZE_OVERLAY"
	thisPlacement.RectPlacement[SPTT_SC_SILVER_OVERLAY].txtRectName = "SC_SILVER_OVERLAY"
	thisPlacement.RectPlacement[SPTT_SC_GOLD_OVERLAY].txtRectName = "SC_GOLD_OVERLAY"
#ENDIF

	//////////
	// TEXT //
	//////////
	
	// TITLES
	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_MAIN_TITLE], 254.0000, 74.0000)

	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_SCORE_TITLE], 257.0000+FLOAT_X_TO_PIXEL(MINIGAME_X_PADDING_LEFT, 1280, TRUE), 127.0000)
	
	// Labels
	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_AWARD_TXT], 257.0000+FLOAT_X_TO_PIXEL(MINIGAME_X_PADDING_LEFT, 1280, TRUE), 297.0000)

	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_SCORE_TXT], 257.0000+FLOAT_X_TO_PIXEL(MINIGAME_X_PADDING_LEFT, 1280, TRUE), 324.0000)

	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_HISCORE_TXT], 257.0000+FLOAT_X_TO_PIXEL(MINIGAME_X_PADDING_LEFT, 1280, TRUE), 351.0000)

	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_TOTAL_TXT], 257.0000+FLOAT_X_TO_PIXEL(MINIGAME_X_PADDING_LEFT, 1280, TRUE), 378.0000)

	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_WEAPON_TXT], 257.0000+FLOAT_X_TO_PIXEL(MINIGAME_X_PADDING_LEFT, 1280, TRUE), 405.0000)
	

	// Stats
	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_AWARDVAL_TXT], 257.0000, 297.0000)

	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_SCOREVAL_TXT], 257.0000, 324.0000)

	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_HISCOREVAL_TXT], 257.0000, 351.0000)

	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_TOTALVAL_TXT], 257.0000, 378.0000)

	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_WEAPONVAL_TXT], 257.0000, 405.0000)

	//Result Medals
	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_GOLD_GOAL_TXT], 298.0000, 567.0000)

	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_SILVER_GOAL_TXT], 384.0000, 567.0000)

	PIXEL_POSITION_TEXT(thisPlacement.TextPlacement[SPTT_SC_BRONZE_GOAL_TXT], 470.0000, 567.0000)

	/////////////
	// SPRITES //
	/////////////
	///     
	// MAIN BACKGROUND
	PIXEL_POSITION_AND_SIZE_SPRITE(thisPlacement.SpritePlacement[SPTT_SCREEN_MAIN_BACKGROUND], 208.0000, 55.0000, 864.0000, 570.0000, TRUE)
	SPRITE_COLOR(thisPlacement.SpritePlacement[SPTT_SCREEN_MAIN_BACKGROUND],0,0,0)
	
	PIXEL_POSITION_AND_SIZE_SPRITE(thisPlacement.SpritePlacement[SPTT_SC_HITS_IMG], 384.0000, 224.0000, 256.0000, 256.0000)
	SPRITE_COLOR(thisPlacement.SpritePlacement[SPTT_SC_HITS_IMG])
	
//	// grey background behind race image
//	thisPlacement.RectPlacement[SPTT_SC_SCORE_BG].x = 0.3000
//	thisPlacement.RectPlacement[SPTT_SC_SCORE_BG].y = 0.2000
//	thisPlacement.RectPlacement[SPTT_SC_SCORE_BG].w = 0.1990
//	thisPlacement.RectPlacement[SPTT_SC_SCORE_BG].h = 0.0420
//	thisPlacement.RectPlacement[SPTT_SC_SCORE_BG].r = 255
//	thisPlacement.RectPlacement[SPTT_SC_SCORE_BG].g = 255
//	thisPlacement.RectPlacement[SPTT_SC_SCORE_BG].b = 255
//	thisPlacement.RectPlacement[SPTT_SC_SCORE_BG].a = 255
	
	/*
	thisPlacement.SpritePlacement[SPTT_SC_SOCIALCLUB_IMG].x = 0.7850
	thisPlacement.SpritePlacement[SPTT_SC_SOCIALCLUB_IMG].y = 0.3410
	thisPlacement.SpritePlacement[SPTT_SC_SOCIALCLUB_IMG].w = 0.0250
	thisPlacement.SpritePlacement[SPTT_SC_SOCIALCLUB_IMG].h = 0.0350
	thisPlacement.SpritePlacement[SPTT_SC_SOCIALCLUB_IMG].r = 255
	thisPlacement.SpritePlacement[SPTT_SC_SOCIALCLUB_IMG].g = 255
	thisPlacement.SpritePlacement[SPTT_SC_SOCIALCLUB_IMG].b = 255
	thisPlacement.SpritePlacement[SPTT_SC_SOCIALCLUB_IMG].a = 255
	thisPlacement.SpritePlacement[SPTT_SC_SOCIALCLUB_IMG].fRotation = 0.0000

	thisPlacement.SpritePlacement[SPTT_SC_HITS_YELLOW_IMG].x = PIXEL_X_TO_FLOAT(480)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_YELLOW_IMG].y = 0.2520
	thisPlacement.SpritePlacement[SPTT_SC_HITS_YELLOW_IMG].w = PIXEL_X_TO_FLOAT(22)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_YELLOW_IMG].h = PIXEL_Y_TO_FLOAT(22)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_YELLOW_IMG].r = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_YELLOW_IMG].g = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_YELLOW_IMG].b = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_YELLOW_IMG].a = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_YELLOW_IMG].fRotation = 0.0000

	thisPlacement.SpritePlacement[SPTT_SC_HITS_RED_IMG].x = PIXEL_X_TO_FLOAT(480)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_RED_IMG].y = 0.2970
	thisPlacement.SpritePlacement[SPTT_SC_HITS_RED_IMG].w = PIXEL_X_TO_FLOAT(22)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_RED_IMG].h = PIXEL_Y_TO_FLOAT(22)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_RED_IMG].r = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_RED_IMG].g = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_RED_IMG].b = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_RED_IMG].a = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_RED_IMG].fRotation = 0.0000

	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLUE_IMG].x = PIXEL_X_TO_FLOAT(480)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLUE_IMG].y = 0.3420
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLUE_IMG].w = PIXEL_X_TO_FLOAT(22)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLUE_IMG].h = PIXEL_Y_TO_FLOAT(22)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLUE_IMG].r = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLUE_IMG].g = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLUE_IMG].b = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLUE_IMG].a = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLUE_IMG].fRotation = 0.0000

	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLACK_IMG].x = PIXEL_X_TO_FLOAT(480)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLACK_IMG].y = 0.3870
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLACK_IMG].w = PIXEL_X_TO_FLOAT(22)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLACK_IMG].h = PIXEL_Y_TO_FLOAT(22)
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLACK_IMG].r = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLACK_IMG].g = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLACK_IMG].b = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLACK_IMG].a = 255
	thisPlacement.SpritePlacement[SPTT_SC_HITS_BLACK_IMG].fRotation = 0.0000
	*/

	// Earned Medal
	PIXEL_POSITION_AND_SIZE_SPRITE(thisPlacement.SpritePlacement[SPTT_SC_MEDAL_AWARD_IMG], 495.0000, 306.0000, 32.0000, 32.0000)
	SPRITE_COLOR(thisPlacement.SpritePlacement[SPTT_SC_MEDAL_AWARD_IMG])

	// Medals
	PIXEL_POSITION_AND_SIZE_SPRITE(thisPlacement.SpritePlacement[SPTT_SC_GOLD_IMG], 258.5000, 482.5000, 80.0000, 80.0000, TRUE)
	SPRITE_COLOR(thisPlacement.SpritePlacement[SPTT_SC_GOLD_IMG])

	PIXEL_POSITION_AND_SIZE_SPRITE(thisPlacement.SpritePlacement[SPTT_SC_SILVER_IMG], 344.0000, 482.5000, 80.0000, 80.0000, TRUE)
	SPRITE_COLOR(thisPlacement.SpritePlacement[SPTT_SC_SILVER_IMG])

	PIXEL_POSITION_AND_SIZE_SPRITE(thisPlacement.SpritePlacement[SPTT_SC_BRONZE_IMG], 429.5000, 482.5000, 80.0000, 80.0000, TRUE)
	SPRITE_COLOR(thisPlacement.SpritePlacement[SPTT_SC_BRONZE_IMG])

	///////////
	// RECTS //
	///////////
	
	// Score
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_SCORE_EDGE], 257.0000, 119.0000, 254.0000, 5.0000, TRUE)
	RECT_COLOR(thisPlacement.RectPlacement[SPTT_SC_SCORE_EDGE],70,70,130)

	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_SCORE_BG], 257.0000, 124.0000, 254.0000, 25.0000, TRUE)
	RECT_COLOR(thisPlacement.RectPlacement[SPTT_SC_SCORE_BG])

	// Column Image BG (column 1)
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_HIT_BG], 257.0000, 159.0000, 254.0000, 133.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_HIT_BG],HUD_COLOUR_PAUSE_BG, TRUE)

	// Stats (column 1)
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_INFO1_BG], 257.0000, 294.0000, 254.0000, 25.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_INFO1_BG],HUD_COLOUR_PAUSE_BG, TRUE)
	
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_INFO2_BG], 257.0000, 321.0000, 254.0000, 25.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_INFO2_BG],HUD_COLOUR_PAUSE_BG, TRUE)
	
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_INFO3_BG], 257.0000, 348.0000, 254.0000, 25.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_INFO3_BG],HUD_COLOUR_PAUSE_BG, TRUE)
	
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_INFO4_BG], 257.0000, 375.0000, 254.0000, 25.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_INFO4_BG],HUD_COLOUR_PAUSE_BG, TRUE)
	
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_INFO5_BG], 257.0000, 402.0000, 254.0000, 25.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_INFO5_BG],HUD_COLOUR_PAUSE_BG, TRUE)
	
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_INFO6_BG], 257.0000, 429.0000, 254.0000, 52.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_INFO6_BG],HUD_COLOUR_PAUSE_BG, TRUE)
	
//	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_INFO7_BG], 257.0000, 456.0000, 254.0000, 25.0000, TRUE)
//	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_INFO7_BG],HUD_COLOUR_PAUSE_BG, TRUE)
//	
//	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_INFO8_BG], 257.0000, 483.0000, 254.0000, 25.0000, TRUE)
//	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_INFO8_BG],HUD_COLOUR_PAUSE_BG, TRUE)

	// Medals (column 1)
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_BRONZE_BG], 257.0000, 483.0000, 83.0000, 84.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_BRONZE_BG],HUD_COLOUR_PAUSE_BG, TRUE)
	
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_SILVER_BG], 342.0000, 483.0000, 84.0000, 84.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_SILVER_BG],HUD_COLOUR_PAUSE_BG, TRUE)
	
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_GOLD_BG], 428.0000, 483.0000, 83.0000, 84.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_GOLD_BG],HUD_COLOUR_PAUSE_BG, TRUE)
	
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_BRONZE_OVERLAY], 428.0000, 564.0000, 83.0000, 25.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_BRONZE_OVERLAY],HUD_COLOUR_BRONZE, TRUE)

	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_SILVER_OVERLAY], 342.0000, 564.0000, 84.0000, 25.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_SILVER_OVERLAY],HUD_COLOUR_SILVER, TRUE)

	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[SPTT_SC_GOLD_OVERLAY], 257.0000, 564.0000, 83.0000, 25.0000, TRUE)
	SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[SPTT_SC_GOLD_OVERLAY],HUD_COLOUR_GOLD, TRUE)

	thisPlacement.bHudScreenInitialised = TRUE
ENDPROC

/// PURPOSE:
///    Sets up the shooting range's scorecard
PROC SPTT_Init_Scorecard(SPTT_RACE_STRUCT &Race)
	RESET_ALL_SPRITE_PLACEMENT_VALUES(Race.uiScorecard.SpritePlacement)
	SET_STANDARD_INGAME_TEXT_DETAILS(Race.uiScorecard.aStyle)
	
	INIT_SCREEN_SPTT_SCORECARD(Race.uiScorecard)
ENDPROC


PROC SPTT_Draw_Scorecard(SPTT_RACE_STRUCT &Race, INT iPlayerIndex)	
//PROC PROCESS_SCREEN_SPTT_SCORECARD(Range_WeaponRounds &sWeaponRndInfo[], Range_RoundInfo &sRoundInfo, Range_MenuData &sMenuInfo)	
	
	// Draw the BG - Common to the main menu
	DRAW_2D_SPRITE("Shared", "BGGradient_32x1024", Race.uiScorecard.SpritePlacement[SPTT_MAIN_BACKGROUND],FALSE)
		
	// Draw the title - Common to the main menu
//	SET_TEXT_CURSIVE(Race.uiScorecard.aStyle.TS_TITLE)
	SET_TEXT_WHITE(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_TITLE)
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_MAIN_TITLE], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_TITLE, "SPR_TITLE")	
//	SET_TEXT_STANDARD(Race.uiScorecard.aStyle.TS_TITLE)
	
	// Draw the "Score" title bar.
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[SPTT_SC_SCORE_BG])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[SPTT_SC_SCORE_EDGE])
	SET_TEXT_BLACK(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_PRIMARY)
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_SCORE_TITLE], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_PRIMARY, "SPR_AWARD_TITLE")
	
	// Draw the HITS area.
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[SPTT_SC_HIT_BG])	
	TEXT_LABEL sDescriptionImage = "SPR_DESC_"
	sDescriptionImage += ENUM_TO_INT(g_current_selected_SPTT_Race)+1
	DRAW_2D_SPRITE("SPRRaces", sDescriptionImage, Race.uiScorecard.SpritePlacement[SPTT_SC_HITS_IMG])
	
	// Draw the 8 info areas.
	INT index
	REPEAT 5 index
		SPTT_SC_SCREEN_RECT eCurrentRect = SPTT_SC_INFO1_BG + INT_TO_ENUM(SPTT_SC_SCREEN_RECT, index)
		DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[eCurrentRect])
	ENDREPEAT
	
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[SPTT_SC_INFO6_BG])
	
	// Labels.
	// Award medal
	SET_TEXT_WHITE(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)	
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_AWARD_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_MEDALEARN")
	STRING sText = "SC_LB_EMPTY"	
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO1_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)	
	IF (Race.Racer[iPlayerIndex].fClockTime <= SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])
		sText = "SPR_GOLD_MED"
		SET_TEXT_GOLD(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
		SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO1_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT+PIXEL_X_TO_FLOAT(22))	
		SET_SPRITE_GOLD(Race.uiScorecard.SpritePlacement[SPTT_SC_MEDAL_AWARD_IMG])
		DRAW_2D_SPRITE("SPRRaces", "MedalDot_32", Race.uiScorecard.SpritePlacement[SPTT_SC_MEDAL_AWARD_IMG])
	ELIF (Race.Racer[iPlayerIndex].fClockTime <= ((SPTT_Master.fTimeGold[SPTT_Master.iRaceCur]-SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur])/2)+SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur])
		sText = "SPR_SILVER_MED"
		SET_TEXT_SILVER(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
		SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO1_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT+PIXEL_X_TO_FLOAT(22))
		SET_SPRITE_SILVER(Race.uiScorecard.SpritePlacement[SPTT_SC_MEDAL_AWARD_IMG])
		DRAW_2D_SPRITE("SPRRaces", "MedalDot_32", Race.uiScorecard.SpritePlacement[SPTT_SC_MEDAL_AWARD_IMG])
	ELIF (Race.Racer[iPlayerIndex].fClockTime <= SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur])
		sText = "SPR_BRONZE_MED"
		SET_TEXT_BRONZE(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
		SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO1_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT+PIXEL_X_TO_FLOAT(22))
		SET_SPRITE_BRONZE(Race.uiScorecard.SpritePlacement[SPTT_SC_MEDAL_AWARD_IMG])
		DRAW_2D_SPRITE("SPRRaces", "MedalDot_32", Race.uiScorecard.SpritePlacement[SPTT_SC_MEDAL_AWARD_IMG])
	ENDIF		
	DRAW_TEXT_WITH_ALIGNMENT(Race.uiScorecard.TextPlacement[SPTT_SC_AWARDVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, sText, FALSE, TRUE)
	
	CLEAR_TEXT_WRAPPED(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
	SET_TEXT_WHITE(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
	
	//Draw the info for the info areas
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_SCORE_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_TIME")
	IF Race.Racer[iPlayerIndex].fPlsMnsTot < 0
		DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_HISCORE_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "TIMEPENAL_R")
	ELSE
		DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_HISCORE_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "TIMEBONUS_R")
	ENDIF
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_TOTAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_RESULTS")
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_WEAPON_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_TIMEBEST")
/*	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_FIRED_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_STUNT_K")
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_HITS_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_STUNT_I")
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_ACC_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_STUNT_P")
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_TIMEREM_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_STUNT_IT")
*/	
	// Values.		
	// Race Time	
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO2_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
	//DRAW_TEXT_WITH_FLOAT(Race.uiScorecard.TextPlacement[SPTT_SC_SCOREVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", Race.Racer[iPlayerIndex].fClockTime, 2, FONT_RIGHT)
	DRAW_TEXT_TIMER(Race.uiScorecard.TextPlacement[SPTT_SC_SCOREVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, FLOOR((Race.Racer[iPlayerIndex].fClockTime+Race.Racer[iPlayerIndex].fPlsMnsTot) * 1000.0), TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", FALSE, TRUE )
	
	// Time Bonus	
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO3_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
	//DRAW_TEXT_WITH_FLOAT(Race.uiScorecard.TextPlacement[SPTT_SC_HISCOREVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", Race.Racer[iPlayerIndex].fPlsMnsTot, 2, FONT_RIGHT)	
	DRAW_TEXT_TIMER(Race.uiScorecard.TextPlacement[SPTT_SC_HISCOREVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, ABSI(FLOOR(Race.Racer[iPlayerIndex].fPlsMnsTot * 1000.0)), TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", FALSE, TRUE )
	
	// Total Time			
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO4_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
	//DRAW_TEXT_WITH_FLOAT(Race.uiScorecard.TextPlacement[SPTT_SC_WEAPONVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", g_savedGlobals.sSPTTData.CourseData[SPTT_Master.iRaceCur].goalTime, 2, FONT_RIGHT)
	DRAW_TEXT_TIMER(Race.uiScorecard.TextPlacement[SPTT_SC_TOTALVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, FLOOR(Race.Racer[iPlayerIndex].fClockTime * 1000.0), TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", FALSE, TRUE )

	// Best Time			
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO5_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
	//DRAW_TEXT_WITH_FLOAT(Race.uiScorecard.TextPlacement[SPTT_SC_WEAPONVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", g_savedGlobals.sSPTTData.CourseData[SPTT_Master.iRaceCur].goalTime, 2, FONT_RIGHT)
	DRAW_TEXT_TIMER(Race.uiScorecard.TextPlacement[SPTT_SC_WEAPONVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, FLOOR(g_savedGlobals.sSPTTData.fBestTime[SPTT_Master.iRaceCur] * 1000.0), TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", FALSE, TRUE )
/*	
	// Shots fired	
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO6_BG])
	//DRAW_TEXT_WITH_NUMBER(Race.uiScorecard.TextPlacement[SPTT_SC_FIREDVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", 0, FONT_RIGHT)
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_FIREDVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_STUNT_TBD")
	
	// Shots hit
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO6_BG])
	//DRAW_TEXT_WITH_NUMBER(Race.uiScorecard.TextPlacement[SPTT_SC_HITSVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", 0, FONT_RIGHT)
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_HITSVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_STUNT_TBD")
	
	// Accuracy
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO6_BG])
	//DRAW_TEXT_WITH_NUMBER(Race.uiScorecard.TextPlacement[SPTT_SC_ACCVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", 0, FONT_RIGHT)
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_ACCVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_STUNT_TBD")
	
	// Time remaining, if any.
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_INFO6_BG])
	//DRAW_TEXT_WITH_NUMBER(Race.uiScorecard.TextPlacement[SPTT_SC_TIMEREMVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", 0, FONT_RIGHT)
	DRAW_TEXT(Race.uiScorecard.TextPlacement[SPTT_SC_TIMEREMVAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "SPR_STUNT_TBD")
*/	
	// Draw the medal rects, with medals and overlays.
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[SPTT_SC_BRONZE_BG])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[SPTT_SC_SILVER_BG])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[SPTT_SC_GOLD_BG])
	DRAW_2D_SPRITE("SPRRaces", "FlightStunt_Bronze_128", Race.uiScorecard.SpritePlacement[SPTT_SC_BRONZE_IMG])
	DRAW_2D_SPRITE("SPRRaces", "FlightStunt_Silver_128", Race.uiScorecard.SpritePlacement[SPTT_SC_SILVER_IMG])
	DRAW_2D_SPRITE("SPRRaces", "FlightStunt_Gold_128", Race.uiScorecard.SpritePlacement[SPTT_SC_GOLD_IMG])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[SPTT_SC_BRONZE_OVERLAY])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[SPTT_SC_SILVER_OVERLAY])
	DRAW_RECTANGLE(Race.uiScorecard.RectPlacement[SPTT_SC_GOLD_OVERLAY])
	
	// Draw goal scores on the overlays.
	SET_TEXT_BLACK(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
	//INT iGoalScore = 0 //= sWeaponRndInfo[catMenu.iCurElement].sChallenges[chalMenu.iCurElement].iBronzeReq
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_BRONZE_OVERLAY])
	//DRAW_TEXT_WITH_NUMBER(Race.uiScorecard.TextPlacement[SPTT_SC_BRONZE_GOAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", iGoalScore, FONT_CENTRE)
	DRAW_TEXT_TIMER(Race.uiScorecard.TextPlacement[SPTT_SC_BRONZE_GOAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, FLOOR(SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur])*1000, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE )
	
	SET_TEXT_BLACK(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
	//iGoalScore = 0 //= sWeaponRndInfo[catMenu.iCurElement].sChallenges[chalMenu.iCurElement].iSilverReq
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_SILVER_OVERLAY])
	//DRAW_TEXT_WITH_NUMBER(Race.uiScorecard.TextPlacement[SPTT_SC_SILVER_GOAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", iGoalScore, FONT_CENTRE)
	DRAW_TEXT_TIMER(Race.uiScorecard.TextPlacement[SPTT_SC_SILVER_GOAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, FLOOR((((SPTT_Master.fTimeGold[SPTT_Master.iRaceCur]-SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur])/2)+SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur]))*1000, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE)
	
	SET_TEXT_BLACK(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY)
	//iGoalScore = 0 //= sWeaponRndInfo[catMenu.iCurElement].sChallenges[chalMenu.iCurElement].iGoldReq
	SET_TEXT_WRAPPED_TO_RECT(Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, Race.uiScorecard.RectPlacement[SPTT_SC_GOLD_OVERLAY])
	//DRAW_TEXT_WITH_NUMBER(Race.uiScorecard.TextPlacement[SPTT_SC_GOLD_GOAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", iGoalScore, FONT_CENTRE)
	DRAW_TEXT_TIMER(Race.uiScorecard.TextPlacement[SPTT_SC_GOLD_GOAL_TXT], Race.uiScorecard.aStyle.TS_MINIGAME_MENU_SECONDARY, FLOOR(SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])*1000, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE)
ENDPROC

FUNC BOOL SPTT_Master_Choose_Stunt_Race(UI_INPUT_DATA &uiInput, BOOL &raceNotRunning)
	//SPTT_UI_Placement = SPTT_UI_Placement

	IF NOT SPTT_Menu_Init()
		DEBUG_MESSAGE("Waiting on SPTT_Menu_Init")
		RETURN FALSE
	ENDIF
	
/*	#IF IS_DEBUG_BUILD
		SPTT_Menu_Widget = SPTT_Menu_Widget
		#IF SPTT_USE_NEW_MENU
			CREATE_MEGA_PLACEMENT_WIDGETS(SPTT_UI_Placement, SPTT_Menu_Widget)
		#ENDIF
	#ENDIF
*/	
	//Keep going till we either push the button to Launch a challenge or Exit
	IF SPTT_Menu_Get_Input(bIsLaunchingRace, uiInput)
			
		//are we transitioning, or is quit menu is fully on
		IF bIsQuitTrans OR bShowQuitMenu
			//if either, then display UI
			SET_WARNING_MESSAGE_WITH_HEADER("SPR_QT_PLANE_T", "SPR_QUIT_PLANE", FE_WARNING_YES | FE_WARNING_NO)
			
			//find out if we're transitioning or not
			IF NOT raceNotRunning
				IF bIsQuitTrans
					//use timer to keep track of transition
					IF IS_TIMER_STARTED(tSPRQuitUITimer)
						IF TIMER_DO_ONCE_WHEN_READY(tSPRQuitUITimer, TO_FLOAT(SPTT_QUIT_UI_TRANS_TIME/1000))						
							//turn transition off after SPTT_QUIT_UI_TRANS_TIME miliseconds
							bIsQuitTrans = FALSE
							//switch quit menu to fully on or fully off
							bShowQuitMenu = !bShowQuitMenu
						ENDIF
					ELSE
						RESTART_TIMER_NOW(tSPRQuitUITimer)
					ENDIF
				ELSE
					//if we're not transitioning, then draw instructions (and check for input)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
					UPDATE_SIMPLE_USE_CONTEXT(SPTT_Master.uiInput)
				ENDIF
			ENDIF
		ELSE
			raceNotRunning = FALSE
			SPTT_Menu_Update()
		ENDIF
		
		RETURN FALSE
	ENDIF

	// SPR doesnt use stunt tuner
/*	#IF IS_DEBUG_BUILD
		IF bIsLaunchingRace AND bIsLaunchingStuntTuner
			bIsLaunchingRace = FALSE
			Pilot_School_Launch_Stunt_Tuner()
		ENDIF	
	#ENDIF
*/	//Launching or exiting?
	IF bIsLaunchingRace
		SPTT_Launch_Race()		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_ENTITY_CAN_BE_DAMAGED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
		ENDIF
		DEBUG_MESSAGE("Launching race")
	ELSE			
		SPTT_Master.iRaceCur = -1 //infinite loop/fatal crash otherwise..............................................................................................................................................................................
	ENDIF
	SPTT_Menu_Cleanup()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		SET_ENTITY_CAN_BE_DAMAGED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
	ENDIF
	RETURN TRUE	
ENDFUNC

//
//	Audio Beats
//

PROC SPTT_Race_Manage_B_Binge_Audio(INT iGateCur)
	IF iGateCur = 7		
		IF NOT IS_BITMASK_AS_ENUM_SET(iSPRAudioBeats, AudioBeat0)	
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", "Beat_01", "Beat_01_1", CONV_PRIORITY_MEDIUM)
			SET_BITMASK_AS_ENUM(iSPRAudioBeats, AudioBeat0)	
		ENDIF
	ENDIF
	IF iGateCur = 11
		IF NOT IS_BITMASK_AS_ENUM_SET(iSPRAudioBeats, AudioBeat1)	
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", "Beat_01", "Beat_01_2", CONV_PRIORITY_MEDIUM)
			SET_BITMASK_AS_ENUM(iSPRAudioBeats, AudioBeat1)	
		ENDIF
	ENDIF
ENDPROC


PROC SPTT_Race_Manage_Vinewood_Audio(INT iGateCur)
	IF iGateCur = 9
		IF NOT IS_BITMASK_AS_ENUM_SET(iSPRAudioBeats, AudioBeat0)	
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", "Beat_02", "Beat_02_1", CONV_PRIORITY_MEDIUM)
			SET_BITMASK_AS_ENUM(iSPRAudioBeats, AudioBeat0)	
		ENDIF
	ENDIF
	IF iGateCur = 12
		IF NOT IS_BITMASK_AS_ENUM_SET(iSPRAudioBeats, AudioBeat1)	
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", "Beat_02", "Beat_02_2", CONV_PRIORITY_MEDIUM)
			SET_BITMASK_AS_ENUM(iSPRAudioBeats, AudioBeat1)	
		ENDIF
	ENDIF
ENDPROC

PROC SPTT_Race_Manage_Advanced_Audio(INT iGateCur)
	IF iGateCur = 4
		IF NOT IS_BITMASK_AS_ENUM_SET(iSPRAudioBeats, AudioBeat0)
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", "Beat_03", "Beat_03_1", CONV_PRIORITY_MEDIUM)
			SET_BITMASK_AS_ENUM(iSPRAudioBeats, AudioBeat0)	
		ENDIF
	ENDIF
	IF iGateCur = 8
		IF NOT IS_BITMASK_AS_ENUM_SET(iSPRAudioBeats, AudioBeat1)
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", "Beat_03", "Beat_03_2", CONV_PRIORITY_MEDIUM)
			SET_BITMASK_AS_ENUM(iSPRAudioBeats, AudioBeat1)	
		ENDIF
	ENDIF
ENDPROC

PROC SPTT_Race_Manage_Airport_Audio(INT iGateCur)
	IF iGateCur = 6
		IF NOT IS_BITMASK_AS_ENUM_SET(iSPRAudioBeats, AudioBeat0)	
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", "Beat_04", "Beat_04_1", CONV_PRIORITY_MEDIUM)
			SET_BITMASK_AS_ENUM(iSPRAudioBeats, AudioBeat0)	
		ENDIF
	ENDIF
	IF iGateCur = 12
		IF NOT IS_BITMASK_AS_ENUM_SET(iSPRAudioBeats, AudioBeat1)	
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", "Beat_04", "Beat_04_2", CONV_PRIORITY_MEDIUM)
			SET_BITMASK_AS_ENUM(iSPRAudioBeats, AudioBeat1)	
		ENDIF
	ENDIF
ENDPROC

PROC SPTT_Race_Manage_Altitude_Audio(INT iGateCur)
	IF iGateCur = 5
		IF NOT IS_BITMASK_AS_ENUM_SET(iSPRAudioBeats, AudioBeat0)	
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", "Beat_05", "Beat_05_1", CONV_PRIORITY_MEDIUM)
			SET_BITMASK_AS_ENUM(iSPRAudioBeats, AudioBeat0)	
		ENDIF
	ENDIF
	IF iGateCur = 12
		IF NOT IS_BITMASK_AS_ENUM_SET(iSPRAudioBeats, AudioBeat1)	
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(myDialogueStruct, "MGSPAUD", "Beat_05", "Beat_05_2", CONV_PRIORITY_MEDIUM)
			SET_BITMASK_AS_ENUM(iSPRAudioBeats, AudioBeat1)	
		ENDIF
	ENDIF
ENDPROC


PROC SPTT_Race_Cleanup_Beat_Audio()	
	INT iNumAudBeats
	REPEAT iMAXAUDIOBEATS iNumAudBeats
		IF IS_BITMASK_AS_ENUM_SET(iSPRAudioBeats, INT_TO_ENUM(STUNTBEATAUDIO, iNumAudBeats))
			CLEAR_BITMASK_AS_ENUM(iSPRAudioBeats, INT_TO_ENUM(STUNTBEATAUDIO, iNumAudBeats))
		ENDIF
	ENDREPEAT
ENDPROC

//
//	One off Beats
//

PROC SPTT_Race_Request_Stunt_Beat_Models()		
	SWITCH INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur)
		CASE SPTT_AirportFlyby
//			ADD_STREAMED_MODEL(stuntBeatModels, MARQUIS)
			ADD_STREAMED_MODEL(stuntBeatModels, JET)
			ADD_STREAMED_MODEL(stuntBeatModels, S_M_M_HIGHSEC_01)
			REQUEST_VEHICLE_RECORDING(101, "SPRStuntAF")
			DEBUG_MESSAGE("SPTT_Race_Request_Stunt_Beat_Models: REQUESTED AIRPORT FLYBY MODELS")	
		BREAK
		CASE SPTT_BridgeBinge
			ADD_STREAMED_MODEL(stuntBeatModels, JETMAX)
			ADD_STREAMED_MODEL(stuntBeatModels, S_M_M_DOCKWORK_01)
			DEBUG_MESSAGE("SPTT_Race_Request_Stunt_Beat_Models: REQUESTED BRIDGE BINGE MODELS")	
		BREAK
		CASE SPTT_Vinewood_Flyby
			ADD_STREAMED_MODEL(stuntBeatModels, SANCHEZ)
			ADD_STREAMED_MODEL(stuntBeatModels, A_M_Y_MOTOX_01)
			DEBUG_MESSAGE("SPTT_Race_Request_Stunt_Beat_Models: REQUESTED VINEWOOD FLYBY MODELS")	
		BREAK
		CASE SPTT_Advanced_Race01
			ADD_STREAMED_MODEL(stuntBeatModels, HAULER)
			ADD_STREAMED_MODEL(stuntBeatModels, TANKER)
			ADD_STREAMED_MODEL(stuntBeatModels, S_M_M_DOCKWORK_01)
			REQUEST_WAYPOINT_RECORDING("SPR_Fluff_01")
			DEBUG_MESSAGE("SPTT_Race_Request_Stunt_Beat_Models: REQUESTED ENGINEERED BRIDGEWORK MODELS")	
		BREAK
		CASE SPTT_Altitude
		BREAK
	ENDSWITCH
	
	REQUEST_ALL_MODELS(stuntBeatModels)
ENDPROC

FUNC BOOL SPTT_Race_Loaded_Stunt_Beat_Models()
	SWITCH INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur)
		CASE SPTT_AirportFlyby			
			IF ARE_MODELS_STREAMED(stuntBeatModels)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "SPRStuntAF")
				DEBUG_MESSAGE("AIRPORT FLYBY asset requests loaded, returning TRUE")
				RETURN TRUE
			ELSE
				DEBUG_MESSAGE("AIRPORT FLYBY asset requests NOT loaded, returning FALSE")
				RETURN FALSE
			ENDIF
		BREAK
		CASE SPTT_BridgeBinge
			IF ARE_MODELS_STREAMED(stuntBeatModels)
				DEBUG_MESSAGE("BRIDGE BINGE asset requests loaded, returning TRUE")
				RETURN TRUE
			ELSE
				DEBUG_MESSAGE("BRIDGE BINGE asset requests NOT loaded, returning FALSE")
				RETURN FALSE
			ENDIF
		BREAK
		CASE SPTT_Vinewood_Flyby
			IF ARE_MODELS_STREAMED(stuntBeatModels)
				DEBUG_MESSAGE("VINEWOOD FLYBY asset requests loaded, returning TRUE")
				RETURN TRUE
			ELSE
				DEBUG_MESSAGE("VINEWOOD FLYBY asset requests NOT loaded, returning FALSE")
				RETURN FALSE
			ENDIF
		BREAK
		CASE SPTT_Advanced_Race01
			IF ARE_MODELS_STREAMED(stuntBeatModels)
			AND GET_IS_WAYPOINT_RECORDING_LOADED("SPR_Fluff_01")
				DEBUG_MESSAGE("ENGINEERED BRIDGEWORK asset requests loaded, returning TRUE")
				RETURN TRUE
			ELSE
				DEBUG_MESSAGE("ENGINEERED BRIDGEWORK asset requests NOT loaded, returning FALSE")
				RETURN FALSE
			ENDIF
		BREAK
		CASE SPTT_Altitude
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC

PROC SPTT_Race_Manage_Beats_B_Binge(INT iGateCur)		
	
	// Bridge Binge Beat 1
	
	IF iGateCur = 6
		IF NOT DOES_ENTITY_EXIST(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
		AND NOT IS_BITMASK_AS_ENUM_SET(iBeatsTriggered, SPTT_STUNT_BEAT_01)		
			iBeatVehicles[SPTT_STUNT_BEAT_INT_01] = CREATE_VEHICLE(JETMAX, << -1429.1023, 2602.6450, -1.0709 >>, 85.2614)	
			iBeatPeds[SPTT_STUNT_BEAT_INT_01] = CREATE_PED_INSIDE_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_01], PEDTYPE_CIVMALE, S_M_M_DOCKWORK_01)
				
			IF NOT IS_ENTITY_DEAD(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
			AND NOT IS_ENTITY_DEAD(iBeatPeds[SPTT_STUNT_BEAT_INT_01])
				IF NOT IS_ENTITY_AT_COORD(iBeatVehicles[SPTT_STUNT_BEAT_INT_01], << -1552.2813, 2641.6094, -0.8283 >>, <<3.0, 3.0, 3.0>>)
					IF (GET_SCRIPT_TASK_STATUS(iBeatPeds[SPTT_STUNT_BEAT_INT_01], SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK)
						//do nothing
					ELSE
						TASK_VEHICLE_MISSION_COORS_TARGET(iBeatPeds[SPTT_STUNT_BEAT_INT_01], iBeatVehicles[SPTT_STUNT_BEAT_INT_01], (<< -1552.2813, 2641.6094, -0.8283 >>), MISSION_GOTO, GET_VEHICLE_ESTIMATED_MAX_SPEED(iBeatVehicles[SPTT_STUNT_BEAT_INT_01]), DRIVINGMODE_PLOUGHTHROUGH, 3.0, -1, true)
					ENDIF
				ELSE
					DEBUG_MESSAGE("JETMAX at target location")
				ENDIF
			ENDIF
			SET_BITMASK_AS_ENUM(iBeatsTriggered, SPTT_STUNT_BEAT_01)
		ENDIF					
	ELIF iGateCur = 12
		IF IS_BITMASK_AS_ENUM_SET(iBeatsTriggered, SPTT_STUNT_BEAT_01)
			IF NOT IS_ENTITY_DEAD(iBeatPeds[SPTT_STUNT_BEAT_INT_01])
				DELETE_PED(iBeatPeds[SPTT_STUNT_BEAT_INT_01])
			ENDIF
			IF NOT IS_ENTITY_DEAD(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
				DELETE_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
			ENDIF
		ENDIF
	ENDIF	

ENDPROC

PROC SPTT_Race_Manage_Beats_Airport(INT iGateCur)		
	
	// Airport Flyby Beat 1
	IF NOT IS_BITMASK_AS_ENUM_SET(iBeatsTriggered, SPTT_STUNT_BEAT_01)		
		IF iGateCur < 3
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "SPRStuntAF")
				REQUEST_COLLISION_AT_COORD(<< -943.8105, -3173.6924, 12.9445 >>)
				IF NOT DOES_ENTITY_EXIST(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
					iBeatVehicles[SPTT_STUNT_BEAT_INT_01] =  CREATE_VEHICLE(JET, << -943.8105, -3173.6924, 12.9445 >>, 60.5370)					
					SET_VEHICLE_ON_GROUND_PROPERLY(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
					SET_VEHICLE_ENGINE_ON(iBeatVehicles[SPTT_STUNT_BEAT_INT_01], TRUE, TRUE)
					IF SPTT_Master.eRaceType = SPTT_RACE_TYPE_PLANE
						SET_HELI_BLADES_FULL_SPEED(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
					ENDIF
					iBeatPeds[SPTT_STUNT_BEAT_INT_01] = CREATE_PED_INSIDE_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_01], PEDTYPE_CIVMALE, S_M_M_HIGHSEC_01)
				ENDIF
				IF NOT IS_ENTITY_DEAD(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
					IF IS_ENTITY_AT_COORD(iBeatVehicles[SPTT_STUNT_BEAT_INT_01], << -943.8105, -3173.6924, 12.9445 >>, <<20.0, 20.0, 20.0>>)
						DEBUG_MESSAGE("Jet is at starting location")
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
							DEBUG_MESSAGE("Jet is not performing vehicle recording, starting vehicle recording")
							START_PLAYBACK_RECORDED_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_01], 101, "SPRStuntAF")							
						ENDIF
					ELSE
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])	
							DEBUG_MESSAGE("Playback has stopped for JET")
							IF NOT IS_ENTITY_DEAD(iBeatPeds[SPTT_STUNT_BEAT_INT_01])
								IF NOT (GET_SCRIPT_TASK_STATUS(iBeatPeds[SPTT_STUNT_BEAT_INT_01], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK)
									DEBUG_MESSAGE("Tasking JET ped to fly to far away vector")
									TASK_VEHICLE_DRIVE_TO_COORD(iBeatPeds[SPTT_STUNT_BEAT_INT_01], iBeatVehicles[SPTT_STUNT_BEAT_INT_01], << -6793.8760, -557.0385, 781.0172 >>, 50.0, DRIVINGSTYLE_NORMAL, JET, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
					STOP_PLAYBACK_RECORDED_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
				ELSE
					DELETE_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
					SET_BITMASK_AS_ENUM(iBeatsTriggered, SPTT_STUNT_BEAT_01)	
				ENDIF
			ENDIF			
		ENDIF
	ELIF NOT IS_ENTITY_DEAD(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
		REQUEST_COLLISION_AT_COORD(GET_ENTITY_COORDS(iBeatVehicles[SPTT_STUNT_BEAT_INT_01]))		
	ENDIF		
	
ENDPROC

PROC SPTT_Race_Manage_Vinewood(INT iGateCur)	

	// Vinewood Beat 1
	IF iGateCur >= 1
		IF NOT DOES_ENTITY_EXIST(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
			IF NOT IS_BITMASK_AS_ENUM_SET(iBeatsTriggered, SPTT_STUNT_BEAT_01)	
				IF ARE_MODELS_STREAMED(stuntBeatModels)
					iBeatVehicles[SPTT_STUNT_BEAT_INT_01] = CREATE_VEHICLE(SANCHEZ, << -2586.9011, 386.4225, 208.0505 >>, 349.0836)	
					DEBUG_MESSAGE("SPTT_STUNT_BEAT_01 vehicle made")
					SET_BITMASK_AS_ENUM(iBeatsTriggered, SPTT_STUNT_BEAT_01)	
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
			IF NOT DOES_ENTITY_EXIST(iBeatPeds[SPTT_STUNT_BEAT_INT_01])
				IF NOT IS_BITMASK_AS_ENUM_SET(iBeatsTriggered, SPTT_STUNT_BEAT_02)		
					iBeatPeds[SPTT_STUNT_BEAT_INT_01] = CREATE_PED_INSIDE_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_01], PEDTYPE_CIVMALE, A_M_Y_MOTOX_01)
					DEBUG_MESSAGE("SPTT_STUNT_BEAT_02 ped made")
					SET_BITMASK_AS_ENUM(iBeatsTriggered, SPTT_STUNT_BEAT_02)	
				ENDIF
			ENDIF
		ENDIF
		// if rider and bike exist and arent dead, task to point
		IF ((NOT DOES_ENTITY_EXIST(iBeatVehicles[SPTT_STUNT_BEAT_INT_01]))
		AND (NOT DOES_ENTITY_EXIST(iBeatPeds[SPTT_STUNT_BEAT_INT_01])))
			IF ((NOT IS_ENTITY_DEAD(iBeatVehicles[SPTT_STUNT_BEAT_INT_01]))
			AND (NOT IS_ENTITY_DEAD(iBeatPeds[SPTT_STUNT_BEAT_INT_01])))
				IF NOT IS_ENTITY_AT_COORD(iBeatVehicles[SPTT_STUNT_BEAT_INT_01], << -2530.8728, 608.7010, 238.9111 >>, <<3.0, 3.0, 3.0>>)
					IF (GET_SCRIPT_TASK_STATUS(iBeatPeds[SPTT_STUNT_BEAT_INT_01], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK)
					ELSE
						TASK_VEHICLE_DRIVE_TO_COORD(iBeatPeds[SPTT_STUNT_BEAT_INT_01], iBeatVehicles[SPTT_STUNT_BEAT_INT_01], (<< -2530.8728, 608.7010, 238.9111 >>), GET_VEHICLE_ESTIMATED_MAX_SPEED(iBeatVehicles[SPTT_STUNT_BEAT_INT_01]), DRIVINGSTYLE_STRAIGHTLINE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
					ENDIF
				ELSE
					DEBUG_MESSAGE("SPTT_STUNT_BEAT_01 at target location")
				ENDIF
			ELSE
				DEBUG_MESSAGE("SPTT_STUNT_BEAT_01 ped or bike is dead")
			ENDIF	
		ENDIF
	ENDIF	
	// Vinewood Beat 2
	IF iGateCur >= 1
		IF NOT DOES_ENTITY_EXIST(iBeatVehicles[SPTT_STUNT_BEAT_INT_02])
			IF NOT IS_BITMASK_AS_ENUM_SET(iBeatsTriggered1, SPTT_STUNT_BEAT_01)		
				IF ARE_MODELS_STREAMED(stuntBeatModels)
					iBeatVehicles[SPTT_STUNT_BEAT_INT_02] = CREATE_VEHICLE(SANCHEZ, << -2582.9304, 384.9180, 208.5764 >>, 336.8248)	
					DEBUG_MESSAGE("SPTT_STUNT_BEAT_02 stuff made")
					SET_BITMASK_AS_ENUM(iBeatsTriggered1, SPTT_STUNT_BEAT_01)	
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(iBeatVehicles[SPTT_STUNT_BEAT_INT_02])
			IF NOT DOES_ENTITY_EXIST(iBeatPeds[SPTT_STUNT_BEAT_INT_02])
				IF NOT IS_BITMASK_AS_ENUM_SET(iBeatsTriggered1, SPTT_STUNT_BEAT_02)	
					iBeatPeds[SPTT_STUNT_BEAT_INT_02] = CREATE_PED_INSIDE_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_02], PEDTYPE_CIVMALE, A_M_Y_MOTOX_01)
					DEBUG_MESSAGE("SPTT_STUNT_BEAT_02 stuff made")
					SET_BITMASK_AS_ENUM(iBeatsTriggered1, SPTT_STUNT_BEAT_02)	
				ENDIF
			ENDIF
		ENDIF
		IF ((NOT DOES_ENTITY_EXIST(iBeatVehicles[SPTT_STUNT_BEAT_INT_02]))
		AND (NOT DOES_ENTITY_EXIST(iBeatPeds[SPTT_STUNT_BEAT_INT_02])))
			IF ((NOT IS_ENTITY_DEAD(iBeatVehicles[SPTT_STUNT_BEAT_INT_02]))
			AND (NOT IS_ENTITY_DEAD(iBeatPeds[SPTT_STUNT_BEAT_INT_02])))
				IF NOT IS_ENTITY_AT_COORD(iBeatVehicles[SPTT_STUNT_BEAT_INT_02], << -2527.7124, 609.6833, 239.2568 >>, <<3.0, 3.0, 3.0>>)
					IF (GET_SCRIPT_TASK_STATUS(iBeatPeds[SPTT_STUNT_BEAT_INT_02], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK)
					ELSE
						TASK_VEHICLE_DRIVE_TO_COORD(iBeatPeds[SPTT_STUNT_BEAT_INT_02], iBeatVehicles[SPTT_STUNT_BEAT_INT_02], (<< -2527.7124, 609.6833, 239.2568 >>), GET_VEHICLE_ESTIMATED_MAX_SPEED(iBeatVehicles[SPTT_STUNT_BEAT_INT_02]), DRIVINGSTYLE_STRAIGHTLINE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
					ENDIF
				ELSE
					DEBUG_MESSAGE("SPTT_STUNT_BEAT_02 at target location")
				ENDIF
			ELSE
				DEBUG_MESSAGE("SPTT_STUNT_BEAT_02 ped or bike is dead")
			ENDIF	
		ENDIF	
	ENDIF
ENDPROC

PROC SPTT_Race_Manage_Advanced(INT iGateCur)		
	
	// Engineered Bridgework
	IF iGateCur >= 4
		IF NOT DOES_ENTITY_EXIST(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
		AND NOT IS_BITMASK_AS_ENUM_SET(iBeatsTriggered, SPTT_STUNT_BEAT_01)		
			iBeatVehicles[SPTT_STUNT_BEAT_INT_01] = CREATE_VEHICLE(HAULER, << -449.6041, -2271.6929, 6.6086 >>, 267.5199)	
			iBeatVehicles[SPTT_STUNT_BEAT_INT_02] = CREATE_VEHICLE(TANKER, << -449.6041, -2271.6929, 6.6086 >>, 267.5199)
			ATTACH_VEHICLE_TO_TRAILER(iBeatVehicles[SPTT_STUNT_BEAT_INT_01], iBeatVehicles[SPTT_STUNT_BEAT_INT_02])
			iBeatPeds[SPTT_STUNT_BEAT_INT_01] = CREATE_PED_INSIDE_VEHICLE(iBeatVehicles[SPTT_STUNT_BEAT_INT_01], PEDTYPE_CIVMALE, S_M_M_DOCKWORK_01)
			SET_BITMASK_AS_ENUM(iBeatsTriggered, SPTT_STUNT_BEAT_01)	
		ENDIF
		IF GET_IS_WAYPOINT_RECORDING_LOADED("SPR_Fluff_01")
			IF NOT IS_ENTITY_DEAD(iBeatVehicles[SPTT_STUNT_BEAT_INT_01])
			AND NOT IS_ENTITY_DEAD(iBeatPeds[SPTT_STUNT_BEAT_INT_01])
				IF (GET_SCRIPT_TASK_STATUS(iBeatPeds[SPTT_STUNT_BEAT_INT_01], SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) = PERFORMING_TASK)
				ELSE
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(iBeatPeds[SPTT_STUNT_BEAT_INT_01], iBeatVehicles[SPTT_STUNT_BEAT_INT_01], "SPR_Fluff_01", DRIVINGMODE_AVOIDCARS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT, -1, GET_VEHICLE_ESTIMATED_MAX_SPEED(iBeatVehicles[SPTT_STUNT_BEAT_INT_01]))
				ENDIF
			ELSE
				DEBUG_MESSAGE("SPTT_STUNT_BEAT_01 ped or bike is dead")
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC


PROC SPTT_Race_Manage_Stunt_Beats(INT iGateCur)		
	SWITCH INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur)
		CASE SPTT_AirportFlyby
			SPTT_Race_Manage_Airport_Audio(iGateCur)	
			SPTT_Race_Manage_Beats_Airport(iGateCur)			
		BREAK
		CASE SPTT_BridgeBinge
			SPTT_Race_Manage_B_Binge_Audio(iGateCur)
			SPTT_Race_Manage_Beats_B_Binge(iGateCur)			
		BREAK
		CASE SPTT_Vinewood_Flyby
			SPTT_Race_Manage_Vinewood_Audio(iGateCur)
			SPTT_Race_Manage_Vinewood(iGateCur)
		BREAK
		CASE SPTT_Advanced_Race01
			SPTT_Race_Manage_Advanced_Audio(iGateCur)
			SPTT_Race_Manage_Advanced(iGateCur)
		BREAK
		CASE SPTT_Altitude
			SPTT_Race_Manage_Altitude_Audio(iGateCur)
		BREAK
	ENDSWITCH	
ENDPROC

PROC SPTT_Race_Cleanup_Stunt_Beats()		
	INT iBeatIndex
	REPEAT STUNT_MAX_BEATS iBeatIndex
		IF IS_BITMASK_AS_ENUM_SET(iBeatsTriggered, INT_TO_ENUM(SPTT_STUNT_BEAT_TRIGGERS, iBeatIndex))
			CLEAR_BITMASK_AS_ENUM(iBeatsTriggered, INT_TO_ENUM(SPTT_STUNT_BEAT_TRIGGERS, iBeatIndex))
		ENDIF
		IF IS_BITMASK_AS_ENUM_SET(iBeatsTriggered1, INT_TO_ENUM(SPTT_STUNT_BEAT_TRIGGERS, iBeatIndex))
			CLEAR_BITMASK_AS_ENUM(iBeatsTriggered1, INT_TO_ENUM(SPTT_STUNT_BEAT_TRIGGERS, iBeatIndex))
		ENDIF
		IF DOES_ENTITY_EXIST(iBeatVehicles[iBeatIndex])
			DELETE_VEHICLE(iBeatVehicles[iBeatIndex])
		ENDIF
		IF DOES_ENTITY_EXIST(iBeatPeds[iBeatIndex])
			DELETE_PED(iBeatPeds[iBeatIndex])
		ENDIF
	ENDREPEAT
	
	SET_ALL_MODELS_AS_NO_LONGER_NEEDED(stuntBeatModels)
	
	// set all models in array to dummy models so when it inits again it only request models for current race
	REPEAT COUNT_OF(stuntBeatModels) iBeatIndex
		stuntBeatModels[iBeatIndex].thisModel = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT

ENDPROC


PROC SPTT_Race_Manage_Race_Beats(INT iGateCur, BOOL bFailChecking)		

	SWITCH eSPRBeatState
		CASE SPTT_STUNT_BEAT_INIT	
			SPTT_Race_Cleanup_Stunt_Beats()			
			SPTT_Race_Cleanup_Beat_Audio()
			SPTT_Race_Request_Stunt_Beat_Models()
			DEBUG_MESSAGE("SPTT_Race_Manage_Race_Beats: REQUESTED MODELS")	
			
			eSPRBeatState = SPTT_STUNT_BEAT_STREAM
			DEBUG_MESSAGE("SPTT_Race_Manage_Race_Beats: moving to SPTT_STUNT_BEAT_STREAM")		
		BREAK
		
		CASE SPTT_STUNT_BEAT_STREAM
			IF SPTT_Race_Loaded_Stunt_Beat_Models()
				DEBUG_MESSAGE("SPTT_Race_Manage_Race_Beats: MODELS LOADED")	
				eSPRBeatState = SPTT_STUNT_BEAT_UPDATE
				DEBUG_MESSAGE("SPTT_Race_Manage_Race_Beats: moving to SPTT_STUNT_BEAT_UPDATE")		
			ENDIF
		BREAK
		
		CASE SPTT_STUNT_BEAT_UPDATE	
			SPTT_Race_Manage_Stunt_Beats(iGateCur)					
		BREAK
		
		CASE SPTT_STUNT_BEAT_INACTIVE
			IF bFailChecking 
				eSPRBeatState = SPTT_STUNT_BEAT_INIT
				DEBUG_MESSAGE("SPTT_Race_Manage_Race_Beats: moving to SPTT_STUNT_BEAT_INIT")	
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SPTT_Race_Draw_Hud(SPTT_RACE_STRUCT& Race)
	IF IS_RADAR_HIDDEN()
		DISPLAY_RADAR(TRUE)
	ENDIF

	IF NOT IS_TIMER_STARTED(tDisplayBonusTimer)	
		iHUDDisplayBonusPrint = 0
		iHUDDisplayBonusTracker = 0
		START_TIMER_NOW(tDisplayBonusTimer)			
	ENDIF
		
	IF CEIL(Race.Racer[0].fPlsMnsTot*1000) != iHUDDisplayBonusTracker		
		iHUDDisplayBonusPrint = CEIL(Race.Racer[0].fPlsMnsTot*1000) - iHUDDisplayBonusTracker
		iHUDDisplayBonusTracker = CEIL(Race.Racer[0].fPlsMnsTot*1000)			
		RESTART_TIMER_NOW(tDisplayBonusTimer)					
	ENDIF
	
	INT iHUDDisplayTime = FLOOR(GET_TIMER_IN_SECONDS(Race.tClock)*100)*10 - CEIL(Race.Racer[0].fPlsMnsTot*1000)

	INT iBonus = 0
	IF (GET_TIMER_IN_SECONDS(tDisplayBonusTimer) <= 2.0)		
		IF Race.Racer[0].iGateCur > 0
			iBonus = -iHUDDisplayBonusPrint		
		ELSE
			iBonus = 0
		ENDIF
	ENDIF	
	
	
	
	// Check whether or not to display best time.
	INT iBesSPRTime = FLOOR(g_savedGlobals.sSPTTData.fBestTime[SPTT_Master.iRaceCur]*100)*10
	
	IF iBesSPRTime <= 0 //not a valid time.
		iBesSPRTime = -1
	ENDIF
	
	INT iGoalTime
	STRING sGoalTimeLabel
	HUD_COLOURS eGoalColor
	PODIUMPOS ePodiumPosition
	
	IF iHUDDisplayTime <= SPTT_Master.fTimeGold[SPTT_Master.iRaceCur]*1000
		iGoalTime = FLOOR(SPTT_Master.fTimeGold[SPTT_Master.iRaceCur]*1000)
		ePodiumPosition = PODIUMPOS_GOLD
		sGoalTimeLabel = "SPR_GBEST"
		eGoalColor = HUD_COLOUR_GOLD
	ELIF iHUDDisplayTime <= ((((SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur]-SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])/2)+SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])*1000)
		
		ePodiumPosition = PODIUMPOS_SILVER
		iGoalTime = FLOOR((((SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur]-SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])/2)+SPTT_Master.fTimeGold[SPTT_Master.iRaceCur])*1000)
		sGoalTimeLabel = "SPR_SBEST"
		eGoalColor = HUD_COLOUR_SILVER
	
	ELIF iHUDDisplayTime <= SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur]*1000
		ePodiumPosition = PODIUMPOS_BRONZE
		iGoalTime = FLOOR(SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur]*1000)
		sGoalTimeLabel = "SPR_BBEST"
		eGoalColor = HUD_COLOUR_BRONZE
	ELSE 
		iGoalTime = -1
	ENDIF
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	// Draws the Hud used in a race
	DRAW_STUNT_PLANE_HUD(iHUDDisplayTime, // RaceTime - The timer
		"", 	// TimerTitle - The title of the timer. Defaults to TIME with "" passed in
		-1, 	// LapNumber - Number of laps
		-1, 	// LapMaxNumber - Max number of laps
		"", 	// LapTitle - The title of the lap meter. Defaults to LAP with "" Passed in
		-1, 	// PositionNum - The position Number
		-1, 	// PositionMaxNumber - The position maximum number
		"", 	// PositionTitle - The title of the position number. Defaults to POSITION with "" Passed in 
		iBonus, // ExtraTimeGiven - If any extra time is given to the timer, pass this in to display +xs -xs
		HUD_COLOUR_WHITE, //PlacementColour - The position numbers can change colour
		/*Race.Racer[0].iGateCur*/-1, 	// CheckpointNumber - if you have a checkpoint bar the current number
		/*Race.iGateCnt*/-1, 	// CheckpointMaxNum - if you have a checkpoint bar, the maximum number
	    "SPR_GATES", 	// CheckpointTitle - The title of the checkpoint bar, defaults to CHECKPOINT with "" passed in
		HUD_COLOUR_YELLOW, // CheckpointColour - the colour the bar should be
		-1, 	// MeterNumber - if you want a meter displayed pass in the current value
		-1, 	// MeterMaxNum - If you want a meter displayed pass in the max value
		"", 	// MeterTitle - the meters title. Defaults to DAMAGE with "" Passed in
		HUD_COLOUR_YELLOW, // MeterColour - The meter colour
		iGoalTime, // GoalTime
		sGoalTimeLabel, // GoalTimeTitle
		ePodiumPosition, // GoalMedalDisplay
		eGoalColor, // Goal Timer Color
		iBesSPRTime, // BestTime - If you want to show a best time then pass in a millisecond value
		"SPRBEST", 	// BestTimeTitle - Title of the best time timer. Defaults to BEST TIME with "" passed in
		PODIUMPOS_NONE, // MedalDisplay - If you want to show a medal postion next to the Best timer then pass in a position. Used for a target times.
		HUD_COLOUR_WHITE, // Best timer color
		TRUE, 	// DisplayMilliseconds - True if you want the main timer to display milliseconds
		-1, 	// FlashingTime - How long you want the whole hud to flash for.
		"", // GET_HUD_STUNT_STRING(), // FloatTitle - The title the float score will have
		-1, // GET_HUD_STUNT_FLOAT(), // FloatValue = value the float value will have
		INT_TO_ENUM(HUD_COLOURS, GET_HUD_STUNT_INT())) // FloatColour - the colour the float value will have
//		DRAW_RACE_TIMER_HUD(FLOOR(Race.Racer[0].fClockTime*1000), 1, 1, -1, -1, -1, CEIL(Race.Racer[0].fPlsMnsTot*1000), CEIL(SPTT_Global_BestTime_Get(SPTT_Master.iRaceCur)*1000), 0)
ENDPROC

			
// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!

