

//****************************************************************************************************
//
//	Author: 		Asa Dang, modified by TSchram
//	Date: 			6/24/11
//	Description:	Main menu helper functions for stunt plane races. Procs and Funcs here don't deal directly
//					with the menu logic or flow.
//	
//****************************************************************************************************
////
///    

USING "sptt_head.sch"

/// PURPOSE:
///    calculates silver goal based on bronze and hold goals
FUNC FLOAT calculateSilverTime(INT iSelectedRace)
	FLOAT silverTime
	silverTime = ((SPTT_Master.fTimeBronze[iSelectedRace]-SPTT_Master.fTimeGold[iSelectedRace])/2)+SPTT_Master.fTimeGold[iSelectedRace]
	RETURN silverTime
ENDFUNC

/// PURPOSE:
///    Sets the hud colour depending on if the player got gold silver or bronze
/// PARAMS:
///    iValue - the value of their score
PROC Set_Text_Colour_With_Value(INT iValue, INT &iRed, INT &iGreen, INT &iBlue, INT &iAlpha) //, SPTT_STRUCT& thisSPR)
	HUD_COLOURS colour
	IF iValue >= SPTT_Master.fTimeGold[SPTT_Master.iRaceCur]
		//GOLD
		colour=HUD_COLOUR_YELLOWLIGHT
		
	ELIF iValue >= calculateSilverTime() AND iValue < SPTT_Master.fTimeGold[SPTT_Master.iRaceCur]
		//Silver
		colour=HUD_COLOUR_BLUELIGHT

	ELIF iValue >= SPTT_Master.fTimeBronze[SPTT_Master.iRaceCur] AND iValue < calculateSilverTime()
		//BRONZE
		colour=HUD_COLOUR_ORANGELIGHT
	ELSE
		colour=HUD_COLOUR_GREY
	ENDIF
	
	GET_HUD_COLOUR(colour, iRed, iGreen, iBlue, iAlpha)
	
ENDPROC
