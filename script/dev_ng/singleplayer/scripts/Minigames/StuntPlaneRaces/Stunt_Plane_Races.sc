// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	Stunt_Plane_Races.sc
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Red Bull style death defying race against time
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// -----------------------------------
// SPR FILE INCLUDES
// -----------------------------------
USING "screen_placements.sch"
USING "screen_placements_export.sch"
USING "sptt_head.sch"
USING "Stunt_Plane_Races_Menu.sch"
USING "sptt_helpers_stunt.sch"
USING "sptt_helpers.sch"
USING "sptt_camera.sch"
USING "sptt_gate.sch"
USING "sptt_racer.sch"

// SPR Main Data.  //used in Tri/Stunt/Offroad
SPTT_MAIN_STRUCT SPTT_Main

USING "SPTT_Race.sch"

USING "UIUtil.sch"


// -----------------------------------
// VARIABLES
// -----------------------------------

// SPR Race Data.
SPTT_RACE_STRUCT SPTT_Race

// Used to monitor analog stick as DPad
UI_INPUT_DATA	uiInput
structTimer	vehRecordTimer

//SCENARIO_BLOCKING_INDEX sbiAirport
VECTOR vAirportCenter = <<-1497.7083, -3449.5762, 7.3477>>

// -----------------------------------
// RACE FILE INCLUDES
// -----------------------------------
USING "minigames_helpers.sch"

USING "sptt_course_data.sch"
//USING "Races\BridgeBinge.sch"
//USING "Races\Vinewood_Flyby.sch"
//USING "Races\Advanced_Race01.sch"
//USING "Races\AirportFlyby.sch"
//USING "Races\Altitude.sch"

// -----------------------------------
// MAIN PROCS/FUNCTIONS
// -----------------------------------

/// PURPOSE:
///    Setup SPR Race.
PROC SPTT_Setup_Race()
	//PRINTLN("SPTT_Setup_Race")
	
	CLEAR_RANK_REDICTION_DETAILS() 
	SC_LEADERBOARD_CACHE_CLEAR_ALL()
	
	// Setup trick tracking.
	ADD_PED_FOR_DIALOGUE(SPTT_Race.Racer[0].pedConvo, 0, NULL, "PilotDispatch")
	ADD_PED_FOR_DIALOGUE(myDialogueStruct, 0, NULL, "PilotDispatch")

	// If in SCH File Mode, call setup race function.
	SPTT_Race_Init(SPTT_Race)
	
	SPTT_SETUP_COURSE(SPTT_Race, INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur))
	
	IF DOES_ENTITY_EXIST(SPTT_Race.Racer[0].Vehicle)
		SET_VEHICLE_DOORS_LOCKED(SPTT_Race.Racer[0].Vehicle, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Setup SPR Main.
/// RETURNS:
///    TRUE if still setting up SPR Main
FUNC BOOL SPTT_Main_Setup()
	//PRINTLN("SPTT_Main_Setup")

	// Check if player is dead.
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		PRINTLN("SPTT_Main_Setup: Player dead!")
		RETURN FALSE
	ENDIF
	
	DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
	
	MINIGAME_DISPLAY_MISSION_TITLE(ENUM_TO_INT(MINIGAME_STUNT_PLANES))
	
	// Setup SPR Main State Machine.
	SWITCH (SPTT_Main.eSetup)
	
		// Setup (init).
		CASE SPTT_MAIN_SETUP_INIT
			PRINTLN("SPTT_MAIN_SETUP: Inside INIT")
			
			SET_MAX_WANTED_LEVEL(0)
			
//			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 10.0)
			DISABLE_SELECTOR()
	
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE)
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_GOLF_COURSE)
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_BASE)
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_PRISON)
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_BIOTECH)
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS)
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO)
			
			CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10)
			
			CLEAR_AREA_OF_VEHICLES(vAirportCenter, 600)
//			sbiAirport = ADD_SCENARIO_BLOCKING_AREA(<< vAirportCenter.x - 600, vAirportCenter.y - 600, vAirportCenter.z - 600 >>, 
//					<< vAirportCenter.x + 600, vAirportCenter.y + 600, vAirportCenter.z + 600 >>)
			
			SPTT_LOAD_COURSE_DATA(SPTT_Race, INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur))

			ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
			
			// Take away player control.
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_ALLOW_PLAYER_DAMAGE)
			
			// If player is in stunt plane, start loading.
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
				PRINTLN("SPTT_MAIN_SETUP: Going to SETUP_LOAD_INIT")
				SPTT_Main.eSetup = SPTT_MAIN_SETUP_LOAD_INIT
			// Otherwise, player is on foot, fade out first.
			ELSE
				PRINTLN("SPTT_MAIN_SETUP: Going to SETUP_FADE_OUT")
				SPTT_Main.eSetup = SPTT_MAIN_SETUP_FADE_OUT
			ENDIF
		BREAK
		
		// Setup Fade Out.
		CASE SPTT_MAIN_SETUP_FADE_OUT
			IF SPTT_FadeOut_Safe(SPTT_FADE_NORM_TIME)
				PRINTLN("SPTT_MAIN_SETUP: SETUP_FADE_OUT: Going to SETUP_LOAD_INIT")
				SPTT_Main.eSetup = SPTT_MAIN_SETUP_LOAD_INIT
			ENDIF
		BREAK
		
		// Setup Load (init).
		CASE SPTT_MAIN_SETUP_LOAD_INIT
		
			// Request stringtable (minigames).
			REQUEST_ADDITIONAL_TEXT("SP_SPR", MINIGAME_TEXT_SLOT)
			
			// Request texture dictionary (pilot school).
//			REQUEST_STREAMED_TEXTURE_DICT("PilotSchool")
			
			REQUEST_STREAMED_TEXTURE_DICT("Shared")
			WHILE NOT HAS_STREAMED_TEXTURE_DICT_LOADED("Shared")
				PRINTLN("Have not streamed text dict: Shared")
				WAIT(0)
			ENDWHILE
			
			//Veh recordings			
//			REQUEST_VEHICLE_RECORDING(103, "SPRTaxi")
//			REQUEST_VEHICLE_RECORDING(104, "SPRTaxi")
			
			REQUEST_SCRIPT_AUDIO_BANK ("HUD_AWARDS")
			
			// Load script start area.
//			LOAD_SCENE(vScriptStartPos)
			
			PRINTLN("SPTT_MAIN_SETUP: SETUP_LOAD_INIT: Going to SETUP_LOAD_WAIT")
			SPTT_Main.eSetup = SPTT_MAIN_SETUP_LOAD_WAIT
		BREAK
		
		// Setup Load (wait).
		CASE SPTT_MAIN_SETUP_LOAD_WAIT
			IF HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
//			AND HAS_STREAMED_TEXTURE_DICT_LOADED("PilotSchool")			
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(103, "SPRTaxi")
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(104, "SPRTaxi")
				PRINTLN("SPTT_MAIN_SETUP: SETUP_LOAD_WAIT: Going to SETUP_CREATE_INIT")
				SPTT_Main.eSetup = SPTT_MAIN_SETUP_CREATE_INIT
			ENDIF
		BREAK
		
		// Setup Create (init).
		CASE SPTT_MAIN_SETUP_CREATE_INIT
			// TODO: Add anything that need creating here...
			PRINTLN("SPTT_MAIN_SETUP: SETUP_CREATE_WAIT: Going to SETUP_CREATE_WAIT")
			SPTT_Main.eSetup = SPTT_MAIN_SETUP_CREATE_WAIT
			RESTART_TIMER_NOW(SPTT_Main.tSetup)
		BREAK
		
		// Setup Create (wait).
		CASE SPTT_MAIN_SETUP_CREATE_WAIT
			IF TIMER_DO_WHEN_READY(SPTT_Main.tSetup, 0.5)
				PRINTLN("SPTT_MAIN_SETUP: SETUP_CREATE_WAIT: Going to SETUP_PLACE_INIT")
				SPTT_Main.eSetup = SPTT_MAIN_SETUP_PLACE_INIT
			ENDIF
		BREAK
		
		// Setup Place (init).
		CASE SPTT_MAIN_SETUP_PLACE_INIT
		
			// If player is in stunt plane, skip placement.
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				PRINTLN("SPTT_MAIN_SETUP: SETUP_PLACE_INIT: Going to SETUP_WAIT")
				SPTT_Main.eSetup = SPTT_MAIN_SETUP_WAIT
			// Otherwise, player is on foot, do placement.
			ELSE
			
				// Teleport player to script start position/heading.
				//SET_ENTITY_COORDS(PLAYER_PED_ID(), vScriptStartPos)
				//SET_ENTITY_HEADING(PLAYER_PED_ID(), fScriptStartHead)
				
				// Reset gameplay camera behind player.
				//SET_GAMEPLAY_CAM_RELATIVE_HEADING(180.0)
				//SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
				
				PRINTLN("SPTT_MAIN_SETUP: SETUP_PLACE_INIT_ELSE: Going to SETUP_PLACE_WAIT")
				SPTT_Main.eSetup = SPTT_MAIN_SETUP_PLACE_WAIT
				RESTART_TIMER_NOW(SPTT_Main.tSetup)
			ENDIF
			
		BREAK
		
		// Setup Place (wait).
		CASE SPTT_MAIN_SETUP_PLACE_WAIT
			IF TIMER_DO_WHEN_READY(SPTT_Main.tSetup, 0.5)
				PRINTLN("SPTT_MAIN_SETUP: SETUP_PLACE_WAIT: Going to SETUP_FADE_IN")
				SPTT_Main.eSetup = SPTT_MAIN_SETUP_FADE_IN
			ENDIF
		BREAK
		
		// Setup Fade In.
		CASE SPTT_MAIN_SETUP_FADE_IN
			IF SPTT_FadeIn_Safe(SPTT_FADE_LONG_TIME)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				PRINTLN("SPTT_MAIN_SETUP: SETUP_FADE_IN: Going to SETUP_WAIT")
				SPTT_Main.eSetup = SPTT_MAIN_SETUP_WAIT
			ENDIF
		BREAK
		
		// Setup (wait).
		CASE SPTT_MAIN_SETUP_WAIT
			CANCEL_TIMER(SPTT_Main.tSetup)
			RETURN FALSE
		BREAK
		
		// Setup (cleanup).
		CASE SPTT_MAIN_SETUP_CLEANUP
			RETURN FALSE
		BREAK
		
	ENDSWITCH
	
	// Setup SPR Main still running.
	RETURN TRUE
	
ENDFUNC

PROC HANDLE_FIRST_MUSIC_EVENT()
	VEHICLE_INDEX vehTemp
	
	IF NOT bTriggerFirstMusicEvent
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
				IF DOES_ENTITY_EXIST(vehTemp) AND NOT IS_ENTITY_DEAD(vehTemp)
					IF IS_ENTITY_IN_AIR(vehTemp)
						IF NOT IS_SCREEN_FADED_OUT()
							TRIGGER_MUSIC_EVENT("MGSP_START")
							PRINTLN("TRIGGERING MUSIC EVENT - MGSP_START")
							bTriggerFirstMusicEvent = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_LB_BUTTONS(BOOL bViewProfile = FALSE)
	IF bViewProfile
		INIT_SIMPLE_USE_CONTEXT(SPTT_Master.uiInput, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(SPTT_Master.uiInput, "HUD_INPUT53",	FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		ADD_SIMPLE_USE_CONTEXT_INPUT(SPTT_Master.uiInput, "SCLB_PROFILE",	FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
		SET_SIMPLE_USE_CONTEXT_FULLSCREEN(SPTT_Master.uiInput, TRUE)
	ELSE
		INIT_SIMPLE_USE_CONTEXT(SPTT_Master.uiInput, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(SPTT_Master.uiInput, "HUD_INPUT53",	FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		SET_SIMPLE_USE_CONTEXT_FULLSCREEN(SPTT_Master.uiInput, TRUE)
	ENDIF
ENDPROC


/// PURPOSE:
///    Update SPR Main.
/// RETURNS:
///    TRUE if still updating SPR Main.
FUNC BOOL SPTT_Main_Update()
	//PRINTLN("SPTT_Main_Update")
	
	// Check if player is dead.
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		PRINTLN("SPTT_Main_Update: Player dead!")
	ENDIF
	
	// Check if player vehicle exists yet.
	IF DOES_ENTITY_EXIST(SPTT_Master.PlayerVeh)
	
		// Check if player vehicle is alive.
		IF NOT IS_ENTITY_DEAD(SPTT_Master.PlayerVeh)
		
			// Check if player is out of player vehicle.
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SPTT_Master.PlayerVeh)
			
				// If player is in a vehicle, make that player vehicle.
				IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
					SPTT_Master.PlayerVeh = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
				ENDIF
				
			ENDIF
			
		ENDIF
		
	// Otherwise, if player is in player vehicle, get a handle to it.
	ELIF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
		SPTT_Master.PlayerVeh = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
	ELSE
		SPTT_Master.PlayerVeh = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10, STUNT, 0)
	ENDIF
	
	INT iRed, iGreen, iBlue, iAlpha
	
	IF SPTT_Race.fadeCheckpoint <> NULL
		SPTT_Race.iFadeAlpha -= 25
		IF SPTT_Race.iFadeAlpha <= 0
			DELETE_CHECKPOINT(SPTT_Race.fadeCheckpoint)
			SPTT_Race.fadeCheckpoint = NULL
			bMissedStuntGate = FALSE
		ELSE
			HUD_COLOURS eColor
			IF bMissedStuntGate
				eColor = HUD_COLOUR_RED
			ELSE
				eColor = HUD_COLOUR_WHITE
			ENDIF
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(eColor), iRed, iGreen, iBlue, iAlpha)
			SET_CHECKPOINT_RGBA(SPTT_Race.fadeCheckpoint, iRed, iGreen, iBlue, IMIN(CEIL(1.5 * SPTT_Race.iFadeAlpha), 255))
			SET_CHECKPOINT_RGBA2(SPTT_Race.fadeCheckpoint, iRed, iGreen, iBlue, SPTT_Race.iFadeAlpha)
		ENDIF
	ENDIF
	
	HANDLE_FIRST_MUSIC_EVENT()
	
	SET_EXCLUSIVE_SCENARIO_GROUP("AMMUNATION")
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	
	// Rob - 2102677, 2101217
	IF SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_CHOOSE_RACE
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10)
		IF IS_VEHICLE_DRIVEABLE(SPTT_Master.PlayerVeh)
			IF IS_ENTITY_ON_FIRE(SPTT_Master.PlayerVeh)
				STOP_ENTITY_FIRE(SPTT_Master.PlayerVeh)
			ENDIF
		ENDIF
	ENDIF
	
	// Update SPR Main State Machine.
	SWITCH (SPTT_Main.eUpdate)
	
		// Update (init).
		CASE SPTT_MAIN_UPDATE_INIT
			PRINTLN("SPTT_MAIN_UPDATE - INIT")
			
			//kill any existing conversations:
			STOP_SCRIPTED_CONVERSATION(FALSE)
							
			// Set SRP Mode as invalid.
			SPTT_Main.iSPRMode = -1
			
			// Set Current Race to default.
			SPTT_Master.iRaceCur = 0
			
			// AsD TODO:
			//Do we need to change this area for the new location? B*1222593
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1734.8021, 3224.1047, 41.3709>>, <<1762.0674, 3258.6899, 40.5188>>, FALSE)
			// If player is in stunt plane, choose race.
			IF DOES_ENTITY_EXIST(SPTT_Master.PlayerVeh)
				PRINTLN("SPTT_MAIN_UPDATE - CHOOSE_RACE")
				SET_VEHICLE_DOORS_LOCKED(SPTT_Master.PlayerVeh, VEHICLELOCK_UNLOCKED)
				SPTT_Race.bLBToggle = FALSE
				SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_SETUP_MENU_CAM
			// Otherwise, player is on foot, get in first.
			ELSE
				PRINTLN("SPTT_MAIN_UPDATE - GET_IN_INIT")
				SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_GET_IN_INIT
			ENDIF
			
		BREAK
		
		// Update Get In (init).
		CASE SPTT_MAIN_UPDATE_GET_IN_INIT
		
			// Tell player to get in stunt plane.
//			PRINT_NOW("SPR_GET_IN", DEFAULT_GOD_TEXT_TIME, 0)
			PRINTLN("Couldn't get handle to stunt plane")
			// Add blip for stunt plane.
			// TODO: How should I do this?
			//ADD_BLIP_FOR_ENTITY()
			
			PRINTLN("SPTT_MAIN_UPDATE - SPTT_MAIN_UPDATE_CLEANUP")
			SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_CLEANUP
		BREAK
		
		// Update Get In (wait).
		CASE SPTT_MAIN_UPDATE_GET_IN_WAIT
		
			// Check if player is in stunt plane.
			IF DOES_ENTITY_EXIST(SPTT_Master.PlayerVeh)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_ALLOW_PLAYER_DAMAGE)
				SET_VEHICLE_DOORS_LOCKED(SPTT_Master.PlayerVeh, VEHICLELOCK_UNLOCKED)
				PRINTLN("SPTT_MAIN_UPDATE - CHOOSE_RACE")
				SPTT_Race.bLBToggle = FALSE
				SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_SETUP_MENU_CAM
			ENDIF
			
		BREAK
		
		CASE SPTT_MAIN_UPDATE_SETUP_MENU_CAM
			IF SPTT_Race.bRestarting
			ELSE
				PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_SETUP_MENU_CAM: Setting up menu camera.")
				IF DOES_CAM_EXIST(iSPRStuntFinishCam) AND IS_CAM_ACTIVE(iSPRStuntFinishCam) AND IS_SCREEN_FADED_IN()
					PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_SETUP_MENU_CAM: Finish cam exists, it's active, and screen is faded in.")
				
					IF DOES_CAM_EXIST(SPTT_Race.menuCam)
						DESTROY_CAM(SPTT_Race.menuCam)
					ENDIF
					ANIMPOSTFX_STOP("MinigameTransitionOut")
					ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
					SPTT_Race.menuCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, SPTT_MENU_CAM_COORD, SPTT_MENU_CAM_ROT, SPTT_MENU_CAM_FOV, TRUE)					
					SET_CINEMATIC_MODE_ACTIVE(FALSE)	
					SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
					ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
					SET_CAM_COORD(iSPRStuntFinishCam, <<SPTT_MENU_CAM_COORD.x, SPTT_MENU_CAM_COORD.y, SPTT_MENU_CAM_COORD.z + 1000>>)
					SET_CAM_ROT(iSPRStuntFinishCam, <<90, SPTT_MENU_CAM_ROT.y, SPTT_MENU_CAM_ROT.z>>)
					SET_CAM_ACTIVE_WITH_INTERP(SPTT_Race.menuCam, iSPRStuntFinishCam, 500)
					PLAY_SOUND_FRONTEND(-1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ELSE
					PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_SETUP_MENU_CAM: One of these is NOT true: Finish cam exists, it's active, and screen is faded in.")
					
					SPTT_FadeIn_Safe(SPTT_FADE_QUICK_TIME)
					IF DOES_CAM_EXIST(SPTT_Race.menuCam)
						DESTROY_CAM(SPTT_Race.menuCam)
					ENDIF
					
					SPTT_Race.menuCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, SPTT_MENU_CAM_COORD, SPTT_MENU_CAM_ROT, SPTT_MENU_CAM_FOV, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF
			ENDIF

			REQUEST_MINIGAME_COUNTDOWN_UI(SPTT_CountDownUI)
			SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(TRUE)		
			CLEAR_FOCUS()
			SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_CHOOSE_RACE
		BREAK
		
		// Update Choose Race.
		CASE SPTT_MAIN_UPDATE_CHOOSE_RACE
			#IF IS_DEBUG_BUILD
				SPTT_Race.bPlayerUsedDebugSkip = FALSE
			#ENDIF
			IF (NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				AND NOT IS_ENTITY_DEAD(SPTT_Master.PlayerVeh))
				SET_VEHICLE_ENGINE_ON(SPTT_Master.PlayerVeh, TRUE, FALSE)
			ENDIF

			IF IS_PC_VERSION()
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
			ENDIF

			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	// B*2239651 - Disabling pause_alternate during end/menu screens (to avoid clash when exiting leaderboards)

			// Auto-pilot race racers.
			// SPTT_Race_Racer_AutoPilot(SPTT_Race)
			DISPLAY_RADAR(FALSE)
			// Check if race is restarting.
			SPTT_Race.bDidWeRestart = SPTT_Race.bRestarting
			IF SPTT_Race.bRestarting
				IF DOES_ENTITY_EXIST(SPTT_Race.Racer[0].Vehicle)
					SET_VEHICLE_DOORS_LOCKED(SPTT_Race.Racer[0].Vehicle, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
				ENDIF
				eSPRBeatState = SPTT_STUNT_BEAT_INIT
				// No longer restarting.
				SPTT_Race.bRestarting = FALSE
				// Setup race.
				SPTT_Setup_Race()	
				PRINTLN("SPTT_MAIN_UPDATE - FADE_OUT")
				SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_FADE_OUT
				
			// Otherwise, wait for player to choose a race.
			ELSE

				IF SPTT_Race.bLBToggle
					IF IS_PLAYER_ONLINE()
						IF SPTT_Race.bLBToggle AND IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) AND TIMERA() > 500
							// We're in the leaderboard. Go to menu.
							PLAY_SOUND(-1, "CANCEL", "HUD_MINI_GAME_SOUNDSET")
							SPTT_Race.bLBToggle = FALSE
							SPTT_Race.bLBViewProfile = TRUE
							INIT_SPTT_MENU_USE_CONTEXT(SPTT_Master.uiInput)
							SETTIMERA(0)
						ENDIF
						IF NOT SPTT_Race.bLBViewProfile AND SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(spttLBControl)
							SPTT_Race.bLBViewProfile = TRUE
							SETUP_LB_BUTTONS(SPTT_Race.bLBViewProfile)
						ENDIF							
						SPTT_DISPLAY_SOCIAL_CLUB_LEADERBOARD(SPTT_Master.uiLeaderboard, g_current_selected_SPTT_Race, SPTT_Master.szRaceName[g_current_selected_SPTT_Race])
					ELSE	
						IF DO_SIGNED_OUT_WARNING(iBS)
							SPTT_Race.bLBToggle = FALSE
							iBS = 0
							INIT_SPTT_MENU_USE_CONTEXT(SPTT_Master.uiInput)
							SETTIMERA(0)
						ENDIF
					ENDIF
				ELSE
					IF SPTT_ATTEMPT_LB_WRITE(SPTT_Race) AND !bIsQuitTrans AND !bShowQuitMenu 
						INIT_SPTT_MENU_USE_CONTEXT(SPTT_Master.uiInput)
					ENDIF
					
					IF bShowingOfflineLBButton AND IS_PLAYER_ONLINE()
						INIT_SPTT_MENU_USE_CONTEXT(SPTT_Master.uiInput)
						bShowingOfflineLBButton = FALSE
					ENDIF
					
					IF NOT SPTT_Race.bLBToggle 
					AND !bIsQuitTrans 
					AND !bShowQuitMenu 
					AND (bWrittenToLB OR NOT IS_PLAYER_ONLINE())
					AND (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD) OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD))
						// In menu, go to leaderboard.
						SPTT_Race.bLBToggle = TRUE
						PLAY_SOUND(-1, "SELECT", "HUD_MINI_GAME_SOUNDSET")
						SOCIAL_CLUB_CLEAR_CONTROL_STRUCT(spttLBControl)
						SPTT_Race.bLBViewProfile = SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(spttLBControl)
						SETUP_LB_BUTTONS(SPTT_Race.bLBViewProfile)
						SETTIMERA(0)
					ENDIF
				
					IF SPTT_Master_Choose_Stunt_Race(uiInput, SPTT_Race.bFailChecking)
						eSPRBeatState = SPTT_STUNT_BEAT_INIT
						// If current race is still valid, setup race.
						IF (SPTT_Master.iRaceCur <> -1)
							SPTT_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
							
							SPTT_Setup_Race()
							PRINTLN("SPTT_MAIN_UPDATE - FADE_OUT")
							// stunt plane in hangar for first mission select
							// AsD TODO:
							IF IS_ENTITY_AT_COORD(SPTT_Master.PlayerVeh, SPTT_MENU_PLAYER_COORDS, (<<5.0,5.0,5.0>>), FALSE)
								IF NOT IS_ENTITY_DEAD(SPTT_Master.PlayerVeh)
		//							SPTT_FadeOut_Safe(SPTT_FADE_NORM_TIME)		
									START_TIMER_NOW(vehRecordTimer)
									SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_FADE_OUT
								ENDIF
							ELSE  // Transition from sky to the next race start locaiton
								IF NOT IS_ENTITY_DEAD(SPTT_Master.PlayerVeh)
		//							SPTT_FadeOut_Safe(SPTT_FADE_NORM_TIME)							
									SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_FADE_OUT
								ENDIF
							ENDIF
							
						// Otherwise, race isn't valid, cleanup main.
						ELSE
							// End of race quit transition
							
							SPTT_Main.eUpdate = SPTT_MAIN_QUIT_FADE_TELEPORT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE SPTT_MAIN_QUIT_FADE_TELEPORT
			HIDE_HUD_AND_RADAR_THIS_FRAME()				
			// make sure screen is faded
			IF IS_VEHICLE_DRIVEABLE(SPTT_Master.PlayerVeh)
				//player exit vehicle
//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
//						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//					ENDIF
//				ELSE
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_CLEANUP
//				ENDIF
			ELSE
					IF DOES_ENTITY_EXIST(SPTT_Race.Racer[0].Vehicle)
						FREEZE_ENTITY_POSITION(SPTT_Race.Racer[0].Vehicle, FALSE)
					ENDIF
					// teleport player
					SET_ENTITY_COORDS(PLAYER_PED_ID(), SPTT_MENU_PLAYER_COORDS, TRUE)
					SET_ENTITY_ROTATION(PLAYER_PED_ID(), SPTT_MENU_PLAYER_ROTATION)
				
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					ENDIF
					// set came relative to player
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					// if race vehicle exists do a load scene otherwise just go to cleanup (ie teleporting to outside the hangar)
					IF DOES_ENTITY_EXIST(SPTT_Race.Racer[0].Vehicle)
						// make sure world is loaded vv before moving on
						IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			          		// AsD TODO:
							NEW_LOAD_SCENE_START(SPTT_MENU_PLAYER_COORDS, GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()), 500.0)
			                #IF IS_DEBUG_BUILD
			                      PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_START().")
			                #ENDIF
						ENDIF
						IF IS_NEW_LOAD_SCENE_LOADED()
			                #IF IS_DEBUG_BUILD
			                      PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to IS_NEW_LOAD_SCENE_LOADED() returning TRUE.")
			                #ENDIF
			                NEW_LOAD_SCENE_STOP()
							// go to cleanup
							PRINTLN("SPTT_MAIN_UPDATE - CLEANUP")
							SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_CLEANUP
						ENDIF
					ELSE
						// go to cleanup
						PRINTLN("SPTT_MAIN_UPDATE - CLEANUP")
						SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_CLEANUP
					ENDIF
			ENDIF
		BREAK
		
		// Update Fade Out.
		CASE SPTT_MAIN_UPDATE_FADE_OUT
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2177746
			IF INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur) = SPTT_BridgeBinge // AND NOT SPTT_Race.bDidWeRestart
				PRINTLN("SPTT_MAIN_UPDATE - SETUP_RACE_INIT")
				STOP_PLAYBACK_RECORDED_VEHICLE(SPTT_Master.PlayerVeh)
				IF IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, SPTT_FIRST_GATE_ACTIVATION)
					CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, SPTT_FIRST_GATE_ACTIVATION)
				ENDIF
				SET_VEHICLE_ENGINE_ON(SPTT_Master.PlayerVeh, TRUE, TRUE)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), SPTT_Master.PlayerVeh)
				SET_VEHICLE_FIXED(SPTT_Master.PlayerVeh)
				Cleanup_Finish_Camera()
				iSPRStuntFinishCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1702.4866, 3279.5454, 41.9968>>, <<-2.9399, 0.0000, 110.4428>>, 45.0218, TRUE)
				SET_CAM_ACTIVE(SPTT_Race.menuCam, FALSE)
//				SET_CAM_ACTIVE_WITH_INTERP(iSPRStuntFinishCam, SPTT_Race.menuCam, 3000)
				SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_SPECIAL_CAM_PAN
			ELSE
				IF SPTT_FadeOut_Safe(SPTT_FADE_NORM_TIME)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					PRINTLN("SPTT_MAIN_UPDATE - SETUP_RACE_INIT")
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_VEHICLE_FIXED(SPTT_Master.PlayerVeh)
					STOP_PLAYBACK_RECORDED_VEHICLE(SPTT_Master.PlayerVeh)
					IF IS_SCREEN_FADED_OUT()
						Cleanup_Finish_Camera()
						SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_SETUP_RACE_INIT
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SPTT_MAIN_UPDATE_SPECIAL_CAM_PAN
			IF NOT ((IS_CAM_ACTIVE(iSPRStuntFinishCam) AND IS_CAM_INTERPOLATING(iSPRStuntFinishCam))
					OR (IS_CAM_ACTIVE(SPTT_Race.menuCam) AND IS_CAM_INTERPOLATING(SPTT_Race.menuCam)))
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE)
				PRINTLN("SPTT_MAIN_UPDATE - SPECIAL_CAM_PAN")
				Cleanup_Finish_Camera()
				SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_SETUP_RACE_INIT
			ENDIF
			
		BREAK
		
		// Update Setup Race (init).
		CASE SPTT_MAIN_UPDATE_SETUP_RACE_INIT				
			SPTT_Race.eSetup = SPTT_RACE_SETUP_INIT
			PRINTLN("SPTT_MAIN_UPDATE - SETUP_RACE_WAIT")
			SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_SETUP_RACE_WAIT
		BREAK

		// Update Setup Race (wait).
		CASE SPTT_MAIN_UPDATE_SETUP_RACE_WAIT
			IF NOT SPTT_Race_Setup(SPTT_Race) //AND CHECK_RACE_LOADED()
                // CREATE STUFF
				
				SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(TRUE)
				
				IF SPTT_Race.bDidWeRestart			
					NEW_LOAD_SCENE_START(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()), 500.0)
				ENDIF		
								
				PRINTLN("SPTT_MAIN_UPDATE - SPTT_MAIN_UPDATE_SETUP_RACE_WAIT")
				//are we on the first course/not restarting?
				IF INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur) = SPTT_BridgeBinge
					PRINTLN("Setting up bridge binge before the taxi")
					PRINT("SPR_TAXI_GATE", 10000, 1)
					IF NOT IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, SPTT_FIRST_GATE_ACTIVATION)
						// Activate first/second race gates.
						SPTT_Race_Gate_Activate(SPTT_Race, 0, TRUE)
						SET_BITMASK_AS_ENUM(iSPRGeneralBits, SPTT_FIRST_GATE_ACTIVATION)
					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEHICLELOCK_UNLOCKED)
						FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
					// Start race racers.
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_FADE_IN
				ELSE
					SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_FADE_IN				
				ENDIF
				SPTT_Race.bDidWeRestart = FALSE
			ENDIF
		BREAK
		
		// Update Fade Out.
		CASE SPTT_MAIN_UPDATE_FADE_IN
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			OR IS_NEW_LOAD_SCENE_LOADED()
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF		
				SET_CINEMATIC_MODE_ACTIVE(FALSE)
				SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
				FREEZE_ENTITY_POSITION(SPTT_Race.Racer[0].Vehicle, FALSE)
				Cleanup_Finish_Camera()
				IF IS_BITMASK_AS_ENUM_SET(iSPRGeneralBits, SPTT_FIRST_GATE_ACTIVATION)
					CLEAR_BITMASK_AS_ENUM(iSPRGeneralBits, SPTT_FIRST_GATE_ACTIVATION)
				ENDIF
				IF INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur) = SPTT_BridgeBinge
					IF SPTT_FadeIn_Safe(SPTT_FADE_QUICK_TIME)
						SPTT_Race.eUpdate = SPTT_RACE_UPDATE_INIT
						PRINTLN("SPTT_MAIN_UPDATE - WAIT")
						SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_WAIT
					ENDIF
				ELSE
					SPTT_Race_Racer_AutoPilot(SPTT_Race)
	//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					IF SPTT_FadeIn_Safe(SPTT_FADE_QUICK_TIME)
						SPTT_Race.eUpdate = SPTT_RACE_UPDATE_INIT
						PRINTLN("SPTT_MAIN_UPDATE - WAIT")
						SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_WAIT
					ENDIF
				ENDIF
			ELSE
				PRINTLN("SPTT_MAIN_UPDATE_FADE_IN - WAITING ON IS_NEW_LOAD_SCENE_LOADED")
			ENDIF
		BREAK
		
		// Update (wait).
		CASE SPTT_MAIN_UPDATE_WAIT
			// If SPR Mode is in Edit, do not update the SPR Race.
			IF (SPTT_Main.iSPRMode = 1)
				RETURN TRUE
			ENDIF
						
			// Check if SPR Race is still updating.
			IF SPTT_Race_Update(SPTT_Race)
				RETURN TRUE
			ENDIF
			
			//PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_WAIT: SPTT_Race_Update is done updating.")
			
			eSPRBeatState = SPTT_STUNT_BEAT_INACTIVE
			SPTT_RESET_GATE_DIALOGUE_BITS()					
			SPTT_REGISTER_COMPLETION_PERCENTAGE()
			
			// If the player isn't restarting the course, load the scene where the menu will be displayed.  This check should be TRUE if the player quit or won a trial.
			IF NOT SPTT_Race.bRestarting
				PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_WAIT: Loading scene in menu location since we're not restarting.")
				PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_WAIT: Starting stream timer limit.")
				START_TIMER_NOW_SAFE(SPTT_Race.tStreamTimeLimit)
				
				// Ensure the player and the camera are in the same frame when loading new scene.  The camera will be destroyed in the SPTT_MAIN_UPDATE_FADE_IN_TO_MENU state.
				PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_WAIT: Player teleported to ", SPTT_Master.vDefRcrPos," to the main menu location.")
				SPTT_Racer_Teleport(SPTT_Race.Racer[0], SPTT_Master.vDefRcrPos, SPTT_Master.fDefRcrHead, 0.0)
				IF DOES_ENTITY_EXIST(SPTT_Race.Racer[0].Vehicle)
					SET_VEHICLE_ON_GROUND_PROPERLY(SPTT_Race.Racer[0].Vehicle)
				ENDIF
				//SPTT_Race.menuCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, SPTT_MENU_CAM_COORD, SPTT_MENU_CAM_ROT, SPTT_MENU_CAM_FOV, TRUE)
				
				// Load the scene in the main menu.
				IF NEW_LOAD_SCENE_START(SPTT_MENU_CAM_COORD, CONVERT_ROTATION_TO_DIRECTION_VECTOR(SPTT_MENU_CAM_ROT), REPLAY_LOAD_SCENE_SIZE)
					PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_WAIT: New load scene has STARTED.  Not warping the camera.")
					
					// Possibly pause game while scene is loading, and ensure player control is disabled.
//					SET_GAME_PAUSED(TRUE)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					// Wait until the scene has loaded, or until scene has loaded.
					WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND GET_TIMER_IN_SECONDS(SPTT_Race.tStreamTimeLimit) < REPLAY_LOAD_SCENE_TIMEOUT/1000
						PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_WAIT: Waiting for new load scene to be loaded.")
						WAIT(0)
					ENDWHILE
					
					NEW_LOAD_SCENE_STOP()
					SET_FOCUS_POS_AND_VEL(SPTT_MENU_CAM_COORD, CONVERT_ROTATION_TO_DIRECTION_VECTOR(SPTT_MENU_CAM_ROT))
					
					PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_WAIT: New load scene is LOADED, so STOPPING the scene. Timer left after loading was at ", GET_TIMER_IN_SECONDS(SPTT_Race.tStreamTimeLimit))
//					SET_GAME_PAUSED(FALSE)
					
					PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_WAIT: Cancelling stream timer. SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_FADE_IN_TO_MENU.")
					CANCEL_TIMER(SPTT_Race.tStreamTimeLimit)
					SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_FADE_IN_TO_MENU
				ELIF TIMER_DO_WHEN_READY(SPTT_Race.tStreamTimeLimit, REPLAY_LOAD_SCENE_TIMEOUT/1000)
					PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_WAIT: Scene failed to start for 10 seconds. Cancelling timer. SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_FADE_IN_TO_MENU.")
					CANCEL_TIMER(SPTT_Race.tStreamTimeLimit)
					SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_FADE_IN_TO_MENU
				ENDIF
			
			// The player is restarting the course.
			ELSE
				PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_WAIT: Race is restarting.  We're going to SPTT_MAIN_UPDATE_SETUP_MENU_CAM, but not doing anything.")
				SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_SETUP_MENU_CAM
			ENDIF
		BREAK
		
		CASE SPTT_MAIN_UPDATE_FADE_IN_TO_MENU
//			IF SPTT_FadeIn_Safe(SPTT_FADE_QUICK_TIME)
				PRINTLN("[SPTT_Main_Update] SPTT_MAIN_UPDATE_FADE_IN_TO_MENU -> SPTT_MAIN_UPDATE_CHOOSE_RACE")
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEHICLELOCK_UNLOCKED)
				ENDIF
				SPTT_Race.bLBToggle = FALSE
				SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_SETUP_MENU_CAM
//			ENDIF
			BREAK
		
		// Update (cleanup).
		CASE SPTT_MAIN_UPDATE_CLEANUP
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEHICLELOCK_UNLOCKED)
			ENDIF
			
			//last chance if we havent written.
			SPTT_ATTEMPT_LB_WRITE(SPTT_Race, TRUE)
			
			IF IS_SCREEN_FADED_OUT()
				SPTT_FadeIn_Safe(SPTT_FADE_NORM_TIME)
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	// Update SPR Main still running.
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Cleanup SPR Main.
PROC SPTT_Main_Cleanup()
	PRINTLN("[SPTT_Main_Cleanup] Procedure called.")

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	SPTT_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
	
	SC_LEADERBOARD_CACHE_CLEAR_ALL()
	
	CLEAR_RANK_REDICTION_DETAILS() 
	
	DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	
	SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(FALSE)
	
	SET_MAX_WANTED_LEVEL(5)
	
	CLEAR_FOCUS()
	
	//PRINTLN("SPTT_Main_Cleanup")
	
//	REMOVE_SCENARIO_BLOCKING_AREA(SPTT_Main.sbi01)
//	REMOVE_SCENARIO_BLOCKING_AREA(sbiAirport)
//	PRINTLN("REMOVING SCENARIO BLOCKING AREA")
	
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	
	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	SET_MINIGAME_SPLASH_SHOWING(FALSE)
	
	CLEAR_WEATHER_TYPE_PERSIST()
	CANCEL_MUSIC_EVENT("MGSP_START")
	CANCEL_MUSIC_EVENT("MGSP_FAIL")
	CANCEL_MUSIC_EVENT("MGTR_COMPLETE")
	TRIGGER_MUSIC_EVENT("MGSP_END")
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_GOLF_COURSE)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_BASE)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_PRISON)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_BIOTECH)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO)
		
	ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE()
	SPTT_Cleanup_Leaderboard(SPTT_Master.uiLeaderboard)
	
	// Give back player control.
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

	SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)

	
	// Evict stringtable (minigames).
	CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT, TRUE)
	
	// Evict texture dictionary (pilot school).
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("PilotSchool")
	
	// Cleanup race.
	SPTT_Race_Cleanup(SPTT_Race, TRUE)

	REMOVE_VEHICLE_RECORDING(103, "SPRTaxi")
	REMOVE_VEHICLE_RECORDING(104, "SPRTaxi")
	
	CLEAR_HELP(TRUE)
	CLEAR_PRINTS()
	
	ENABLE_SELECTOR()
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	
	SPTT_RACE_CLEANUP_FINISH_STREAMVOL()
	
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	Set_Leave_Area_Flag_For_All_Blipped_Missions()
	
	// Terminate script.
	TERMINATE_THIS_THREAD()
	
ENDPROC

/// PURPOSE:
///    Process SPR Main debug inputs.
PROC SPTT_Main_Debug()
	//PRINTLN("SPTT_Main_Debug")
	#IF IS_DEBUG_BUILD
		// Pass/Fail stunt plane races (exiting for now).
		IF IS_DEBUG_KEY_PRESSED(KEY_1, KEYBOARD_MODIFIER_SHIFT, "")
			SPTT_Race_Racer_Gate_Jump_Last(SPTT_Race, 0)
			ADJUST_TIMER(SPTT_Race.tClock, SPTT_Master.fTimeGold[g_current_selected_SPTT_Race] - 1)
			PAUSE_TIMER(SPTT_Race.tClock)
		ELIF IS_DEBUG_KEY_PRESSED(KEY_2, KEYBOARD_MODIFIER_SHIFT, "")
			SPTT_Race_Racer_Gate_Jump_Last(SPTT_Race, 0)
			ADJUST_TIMER(SPTT_Race.tClock, SPTT_Master.fTimeGold[g_current_selected_SPTT_Race] + (SPTT_Master.fTimeBronze[g_current_selected_SPTT_Race] - SPTT_Master.fTimeGold[g_current_selected_SPTT_Race])/2  - 1)
			PAUSE_TIMER(SPTT_Race.tClock)
		ELIF IS_DEBUG_KEY_PRESSED(KEY_3, KEYBOARD_MODIFIER_SHIFT, "")
			SPTT_Race_Racer_Gate_Jump_Last(SPTT_Race, 0)
			ADJUST_TIMER(SPTT_Race.tClock, SPTT_Master.fTimeBronze[g_current_selected_SPTT_Race] - 1)
			PAUSE_TIMER(SPTT_Race.tClock)
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			SPTT_Race_Racer_Gate_Jump_Last(SPTT_Race, 0)		
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			SPTT_Main_Cleanup()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E)
			IF NOT IS_VEHICLE_FUCKED(SPTT_Race.Racer[0].Vehicle)
				VECTOR tmpVec = GET_ENTITY_ROTATION(SPTT_Race.Racer[0].Vehicle)
				PRINTLN("Vehicle rotation is ... ", tmpVec)
			ENDIF
		ENDIF
		
		// Jump stunt plane race gate (ahead).
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			SPTT_Race_Racer_Gate_Jump_Cur(SPTT_Race, 0)
		// Jump stunt plane race gate (behind).
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			SPTT_Race_Racer_Gate_Jump_Nxt(SPTT_Race, 0)
		ENDIF
	#ENDIF
	
ENDPROC




// -----------------------------------
// STUNT PLANE RACES SCRIPT
// -----------------------------------
SCRIPT

	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances.
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_REPEAT_PLAY | FORCE_CLEANUP_FLAG_DEBUG_MENU | FORCE_CLEANUP_FLAG_MAGDEMO | FORCE_CLEANUP_FLAG_SP_TO_MP))
		PRINTLN("HAS_FORCE_CLEANUP_OCCURRED happened.")
		SPTT_Main_Cleanup()
	ENDIF
	
	// Any initialization (generally, only mission scripts should set
	// the mission flag to TRUE).
	SET_MISSION_FLAG(TRUE)
	
	// Setup SPR Main Run/Setup/Update.
	PRINTLN("SPTT_MAIN_RUN - SETUP")
	SPTT_Main.eRun = SPTT_MAIN_RUN_SETUP
	SPTT_Main.eSetup = SPTT_MAIN_SETUP_INIT
	SPTT_Main.eUpdate = SPTT_MAIN_UPDATE_INIT
	
//	// AsD TODO: 
//	//do we need to change the blocking area? B* 1222593
//	SPTT_Main.sbi01 = ADD_SCENARIO_BLOCKING_AREA((<<1731.5636, 3309.1814, 40.2237>> - <<1200,1200,1200>>), (<<1731.5636, 3309.1814, 40.2237>> + <<1200,1200,1200>>))
//	PRINTLN("ADDING SCENARIO BLOCKING AREA")
	
	// Stunt Plane Races main loop.
	WHILE TRUE
		// Process SPR Main Debug inputs.
		SPTT_Main_Debug()	
	
		// Run SPR Main State Machine.
		SWITCH (SPTT_Main.eRun)
			CASE SPTT_MAIN_RUN_SETUP
				IF NOT SPTT_Main_Setup()
					PRINTLN("SPTT_MAIN_RUN - UPDATE")
					SPTT_Main.eRun = SPTT_MAIN_RUN_UPDATE
				ENDIF
			BREAK
			CASE SPTT_MAIN_RUN_UPDATE
				IF NOT SPTT_Main_Update()
//					PRINT("FALSEUPDATE", DEFAULT_GOD_TEXT_TIME, 1)
					PRINTLN("SPTT_MAIN_RUN - CLEANUP")
					SPTT_Main.eRun = SPTT_MAIN_RUN_CLEANUP
				ENDIF
			BREAK
			CASE SPTT_MAIN_RUN_CLEANUP
//				IF NOT IS_MESSAGE_BEING_DISPLAYED()
					SPTT_Main_Cleanup()
//				ENDIF
			BREAK
		ENDSWITCH
		
		// Wait until next frame.
		WAIT(0)
		
	ENDWHILE
	
	
	// Should never reach here.
	PRINTLN("Should never reach ENDSCRIPT!!!")
	
ENDSCRIPT




// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!
