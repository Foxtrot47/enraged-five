//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			6/24/11
//	Description:	Main menu logic for pilot school. Loads in data to be shown in the menu, then allows 
//					the player to launch a challenge from the UI. Everything here should only deal with 
//					menu flow/logic.
//	
//****************************************************************************************************


CONST_INT						SPTT_USE_NEW_MENU				1

USING "minigame_uiinputs.sch"
USING "globals.sch"
USING "rage_builtins.sch"
USING "stack_sizes.sch"

USING "commands_hud.sch"
USING "commands_debug.sch"
USING "commands_xml.sch"

USING "minigames_helpers.sch"

USING "flow_help_public.sch"
USING "SPTT_Data.sch"
USING "SPTT_Menu_Text.sch"

USING "UIUtil.sch"
USING "script_oddjob_funcs.sch"
USING "menu_cursor_public.sch"

INT iMenuSoundIndex = -1

ENUM SPTT_MENU_INIT_STATE
	SPTT_MENU_INIT_REQUEST = 0,
	SPTT_MENU_INIT_STREAM,
	SPTT_MENU_INIT_INIT,
	SPTT_MENU_INIT_FINISH
ENDENUM

SPTT_MENU_INIT_STATE	eSPRInitState = SPTT_MENU_INIT_REQUEST

CONST_INT						SPTT_QUIT_UI_TRANS_TIME 250
structTimer						tSPRQuitUITimer

BOOL bIsLaunchingRace = FALSE
BOOL bShowQuitMenu = FALSE
BOOL bIsQuitTrans = FALSE
BOOL bIsWaitingToQuit = FALSE
BOOL bWrittenToLB = TRUE
BOOL bShowingOfflineLBButton

INT iChallengeScores[NUMBER_OF_SPTT_COURSES]

///=====
USING "SPTT_Menu_lib.sch"

MEGA_PLACEMENT_TOOLS			SPTT_UI_Placement

///=====
///    
FUNC FLOAT SPTT_RACE_GET_GOLD_TIME(INT iRaceIndex)	
	SWITCH INT_TO_ENUM(SPTT_Races_ENUM, iRaceIndex)
		CASE SPTT_BridgeBinge
			RETURN 70.0
		BREAK
		CASE SPTT_Vinewood_Flyby
			RETURN 75.0
		BREAK
		CASE SPTT_Advanced_Race01
			RETURN 72.0
		BREAK
		CASE SPTT_AirportFlyby
			RETURN 120.0
		BREAK
		CASE SPTT_Altitude
			RETURN 115.0
		BREAK
	ENDSWITCH
	RETURN -1.0
ENDFUNC

FUNC FLOAT SPTT_RACE_GET_BRONZE_TIME(INT iRaceIndex)	
	SWITCH INT_TO_ENUM(SPTT_Races_ENUM, iRaceIndex)
		CASE SPTT_BridgeBinge
			RETURN 130.0
		BREAK
		CASE SPTT_Vinewood_Flyby
			RETURN 120.0
		BREAK
		CASE SPTT_Advanced_Race01
			RETURN 120.0
		BREAK
		CASE SPTT_AirportFlyby
			RETURN 170.0
		BREAK
		CASE SPTT_Altitude
			RETURN 180.0
		BREAK
	ENDSWITCH
	RETURN -1.0
ENDFUNC

FUNC TEXT_LABEL SPTT_RACE_GET_RACE_NAME(INT iRaceIndex)	
	TEXT_LABEL sRaceName = ""
	SWITCH INT_TO_ENUM(SPTT_Races_ENUM, iRaceIndex)
		CASE SPTT_BridgeBinge
			sRaceName = "BRIDGEBINGE"
		BREAK
		CASE SPTT_Vinewood_Flyby
			sRaceName = "VINEWOOD"
		BREAK
		CASE SPTT_Advanced_Race01
			sRaceName = "BRIDGEWORK"
		BREAK
		CASE SPTT_AirportFlyby
			sRaceName = "AIRPORT"
		BREAK
		CASE SPTT_Altitude
			sRaceName = "ALTITUDE"
		BREAK
	ENDSWITCH
	DEBUG_MESSAGE("Returning: ", sRaceName)
	RETURN sRaceName
ENDFUNC

PROC SPTT_REGISTER_COMPLETION_PERCENTAGE()
	PRINTLN("Seeing if we should register completion percentage. Did we get at least bronze in Altitude?")
	IF g_savedGlobals.sSPTTData.CourseData[SPTT_Altitude].status = SPTT_UNLOCKED
		IF g_savedGlobals.sSPTTData.fBestTime[SPTT_Altitude] > 0
			IF g_savedGlobals.sSPTTData.fBestTime[SPTT_Altitude] <= SPTT_RACE_GET_BRONZE_TIME(ENUM_TO_INT(SPTT_Altitude))
				PRINTLN("Registering SPTT as completed!")
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_STUNT_TT)
			ELSE
				PRINTLN("Altitude best time is not good enough for a bronze medal. NOT Registering SPTT as completed")
			ENDIF
		ELSE
			PRINTLN("Altitude does not have a best time. NOT Registering SPTT as completed")
		ENDIF
	ELSE
		PRINTLN("Altitude has not been unlocked. NOT Registering SPTT as completed")
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Resets all of the scores
PROC SPTT_Menu_Reset_Scores()

	g_savedGlobals.sSPTTData.CourseData[SPTT_BridgeBinge].status		= SPTT_UNLOCKED
	//g_savedGlobals.sSPTTData.CourseData[PSC_Inverted].goalTime   = -1
	
	INT iNode_count = 1
	REPEAT SPTT_Master.iRaceCnt iNode_count
			g_savedGlobals.sSPTTData.CourseData[iNode_count].status		= SPTT_UNLOCKED
	ENDREPEAT
	

ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC SPTT_Menu_Load_Vars()	
	INT iRaceIndex = 0
	REPEAT 5 iRaceIndex	
		g_savedGlobals.sSPTTData.CourseData[iRaceIndex].name = SPTT_RACE_GET_RACE_NAME(iRaceIndex)		
		SPTT_Master.szRaceName[iRaceIndex] = g_savedGlobals.sSPTTData.CourseData[iRaceIndex].name		
		SPTT_Master.fTimeGold[iRaceIndex] = SPTT_RACE_GET_GOLD_TIME(iRaceIndex)
		SPTT_Master.fTimeBronze[iRaceIndex]	= SPTT_RACE_GET_BRONZE_TIME(iRaceIndex)										
		IF iRaceIndex < (ENUM_TO_INT(NUMBER_OF_SPTT_COURSES)-1)
			PRINTLN("g_savedGlobals.sSPTTData.fBestTime[", iRaceIndex, "] is ", g_savedGlobals.sSPTTData.fBestTime[iRaceIndex])
				PRINTLN("SPTT_Master.fTimeBronze[", iRaceIndex, "] is ", SPTT_Master.fTimeBronze[iRaceIndex])
			IF g_savedGlobals.sSPTTData.fBestTime[iRaceIndex] > SPTT_Master.fTimeBronze[iRaceIndex]					
			OR g_savedGlobals.sSPTTData.fBestTime[iRaceIndex] <= 0
				PRINTLN("Locking race #", iRaceIndex+1)
				
				g_savedGlobals.sSPTTData.CourseData[iRaceIndex+1].status = SPTT_LOCKED										
			ELSE
				PRINTLN("Unlocking race #", iRaceIndex+1)
				g_savedGlobals.sSPTTData.CourseData[iRaceIndex+1].status = SPTT_UNLOCKED		
			ENDIF
		ENDIF
	ENDREPEAT
	g_savedGlobals.sSPTTData.CourseData[SPTT_BridgeBinge].status = SPTT_UNLOCKED
	
ENDPROC


PROC SPTT_Menu_Cleanup()
	CLEAR_HELP()
	CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT, TRUE)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF	
	IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
		ANIMPOSTFX_STOP("MinigameTransitionIn")
	ENDIF
	ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
	eSPRInitState = SPTT_MENU_INIT_REQUEST
	SET_GAME_PAUSED(FALSE)
	DISABLE_CELLPHONE(FALSE)
	KILL_FACE_TO_FACE_CONVERSATION()
	DEBUG_MESSAGE("******************** END SCRIPT: SPTT_Menu.sc ********************")
ENDPROC

// set the latest unlocked race highlighted when returning to menu
PROC Stunt_Highlight_Latest()

	IF ((g_savedGlobals.sSPTTData.CourseData[SPTT_Altitude].status = SPTT_UNLOCKED)
	AND (g_savedGlobals.sSPTTData.fBestTime[SPTT_Altitude] = 0))
		DEBUG_MESSAGE("Stunt_Highlight_Latest: Altitude unlocked without score, set to default race")
		g_current_selected_SPTT_Race = SPTT_Altitude
		EXIT
	ELIF ((g_savedGlobals.sSPTTData.CourseData[SPTT_AirportFlyby].status = SPTT_UNLOCKED)
	AND (g_savedGlobals.sSPTTData.fBestTime[SPTT_AirportFlyby] = 0))
		g_current_selected_SPTT_Race = SPTT_AirportFlyby
		DEBUG_MESSAGE("Stunt_Highlight_Latest: SPTT_AirportFlyby unlocked without score, set to default race")
		EXIT
	ELIF ((g_savedGlobals.sSPTTData.CourseData[SPTT_Advanced_Race01].status = SPTT_UNLOCKED)
	AND (g_savedGlobals.sSPTTData.fBestTime[SPTT_Advanced_Race01] = 0))
		g_current_selected_SPTT_Race = SPTT_Advanced_Race01
		DEBUG_MESSAGE("Stunt_Highlight_Latest: SPTT_Advanced_Race01 unlocked without score, set to default race")
		EXIT
	ELIF ((g_savedGlobals.sSPTTData.CourseData[SPTT_Vinewood_Flyby].status = SPTT_UNLOCKED)
	AND (g_savedGlobals.sSPTTData.fBestTime[SPTT_Vinewood_Flyby] = 0))
		g_current_selected_SPTT_Race = SPTT_Vinewood_Flyby
		DEBUG_MESSAGE("Stunt_Highlight_Latest: SPTT_Vinewood_Flyby unlocked without score, set to default race")
		EXIT
	ELSE
		DEBUG_MESSAGE("Stunt_Highlight_Latest: Bridge Binge Unlocked by default")
		EXIT
	ENDIF
	
ENDPROC

PROC INIT_SPTT_MENU_USE_CONTEXT(SIMPLE_USE_CONTEXT& uiInput)
	//TODO: add array overrun protection if necessary
	IF g_current_selected_SPTT_Race != SPTT_BridgeBinge
	AND	(g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].status = SPTT_LOCKED)				
		INIT_SIMPLE_USE_CONTEXT(uiInput, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(uiInput, "IB_QUIT", FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF NOT IS_PLAYER_ONLINE()
			ADD_SIMPLE_USE_CONTEXT_INPUT(uiInput, "HUD_INPUT68", FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = TRUE
		ELIF IS_PLAYER_ONLINE() AND bWrittenToLB
			ADD_SIMPLE_USE_CONTEXT_INPUT(uiInput, "HUD_INPUT68", FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = FALSE
		ELSE //player is online and we havent written
			bShowingOfflineLBButton = FALSE
		ENDIF
	ELSE
		INIT_SIMPLE_USE_CONTEXT(uiInput, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(uiInput, "FE_HLP4", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ADD_SIMPLE_USE_CONTEXT_INPUT(uiInput, "IB_QUIT", FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF NOT IS_PLAYER_ONLINE()
			ADD_SIMPLE_USE_CONTEXT_INPUT(uiInput, "HUD_INPUT68", FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = TRUE
		ELIF IS_PLAYER_ONLINE() AND bWrittenToLB
			ADD_SIMPLE_USE_CONTEXT_INPUT(uiInput, "HUD_INPUT68", FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = FALSE
		ELSE //player is online and we havent written
			bShowingOfflineLBButton = FALSE
		ENDIF
	ENDIF
	
	SET_SIMPLE_USE_CONTEXT_FULLSCREEN(SPTT_Master.uiInput, TRUE)
ENDPROC


FUNC BOOL SPTT_Menu_Init()
	
	SWITCH eSPRInitState
		CASE SPTT_MENU_INIT_REQUEST
			DEBUG_MESSAGE("******************** SPTT_Menu_Init ********************")
			CLEAR_HELP()
			REQUEST_ADDITIONAL_TEXT("SP_SPR", MINIGAME_TEXT_SLOT)
			REQUEST_STREAMED_TEXTURE_DICT("SPRRaces")
		
			#IF SPTT_USE_NEW_MENU	
				REQUEST_STREAMED_TEXTURE_DICT("MPHUD")
			#ENDIF
			
			DEBUG_MESSAGE("SPTT_Menu_Init: Moving to SPTT_MENU_INIT_STREAM")
			eSPRInitState = SPTT_MENU_INIT_STREAM
		BREAK
		
		CASE SPTT_MENU_INIT_STREAM
			IF HAS_THIS_ADDITIONAL_TEXT_LOADED("SP_SPR", MINIGAME_TEXT_SLOT)
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("SPRRaces")
					#IF SPTT_USE_NEW_MENU
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPHUD")
					#ENDIF
						DEBUG_MESSAGE("SPTT_Menu_Init: Moving to SPTT_MENU_INIT_INIT")
						eSPRInitState = SPTT_MENU_INIT_INIT
					#IF SPTT_USE_NEW_MENU
					ELSE
						DEBUG_MESSAGE("SPTT_Menu_Init: Waiting on HAS_STREAMED_TEXTURE_DICT_LOADED(MPHUD)")
					ENDIF
					#ENDIF
				ELSE
					DEBUG_MESSAGE("SPTT_Menu_Init: Waiting on HAS_STREAMED_TEXTURE_DICT_LOADED(SP_SPR)")
				ENDIF
			ELSE
				DEBUG_MESSAGE("SPTT_Menu_Init: Waiting on HAS_THIS_ADDITIONAL_TEXT_LOADED")
			ENDIF
		BREAK
		CASE SPTT_MENU_INIT_INIT
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SPTT_Master.uiInput.sfMov)
			SPTT_Master.uiInput.sfMov = NULL
			INIT_SPTT_MENU_USE_CONTEXT(SPTT_Master.uiInput)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_ALLOW_PLAYER_DAMAGE)
			ENDIF
			DISABLE_CELLPHONE(TRUE)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
			SET_VEHICLE_DOORS_LOCKED(SPTT_Master.PlayerVeh, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
			
				
			KILL_FACE_TO_FACE_CONVERSATION()
			
			//init array
			INT iClass
			iClass = 0
			REPEAT SPTT_Master.iRaceCnt iClass
				iChallengeScores[iClass] = -1
			ENDREPEAT
			
			#IF SPTT_USE_NEW_MENU
				INIT_SPTT_MENU(SPTT_UI_Placement)
			#ENDIF
			
			SPTT_Menu_Load_Vars()
			
			//commenting out, since we want the cursor to remain on the race that has just been completed, like other minigames.
			//Stunt_Highlight_Latest()
			
			ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
			
			DEBUG_MESSAGE("SPTT_Menu_Init: Moving to SPTT_MENU_INIT_FINISH")
			eSPRInitState = SPTT_MENU_INIT_FINISH
		BREAK
		CASE SPTT_MENU_INIT_FINISH
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SPTT_Menu_Draw_Debug_Options()
 #IF IS_DEBUG_BUILD

	CONST_FLOAT fDiffForPrint	0.04		//0.03		//0.05
	FLOAT xpos, ypos
	xpos = 0.060//0.3660
	ypos = 0.7500
	
	//RectPieceNum: 0
	DRAW_RECT(xpos + 0.14, ypos + 0.08 , 0.3000, 0.1600, 128, 128, 0, 255)
	
	//TextPieceNum: 0
	SET_TEXT_SCALE(0.5000, 0.5000)
	SET_TEXT_COLOUR(0, 0, 128, 255)
	SET_TEXT_DROP_SHADOW()
//	SET_TEXT_OUTLINE()
	DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*0.0), "STRING", "'W' to unlock all challenges")

	//TextPieceNum: 1
	SET_TEXT_SCALE(0.5000, 0.5000)
	SET_TEXT_COLOUR(0, 0, 128, 255)
	SET_TEXT_DROP_SHADOW()
//	SET_TEXT_OUTLINE()
	DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*1.0), "STRING", "'Shift-W' to reset challenges")

	//TextPieceNum: 2
	SET_TEXT_SCALE(0.5000, 0.5000)
	SET_TEXT_COLOUR(0, 0, 128, 255)
	SET_TEXT_DROP_SHADOW()
//	SET_TEXT_OUTLINE()

	IF g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].status = SPTT_LOCKED
		DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*2.0), "STRING", "'P' unlock this challenge")
	ELSE
		IF g_savedGlobals.sSPTTData.fBestTime[g_current_selected_SPTT_Race] < 0
			DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*2.0), "STRING", "'P' pass this challenge")
		ELSE
			DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*2.0), "STRING", "'P' lock this challenge")
		ENDIF
	ENDIF

	//TextPieceNum: 3
	SET_TEXT_SCALE(0.5000, 0.5000)
	SET_TEXT_COLOUR(0, 0, 128, 255)
	SET_TEXT_DROP_SHADOW()
//	SET_TEXT_OUTLINE()
	DISPLAY_TEXT_WITH_LITERAL_STRING(xpos, ypos + (fDiffForPrint*3.0), "STRING", "'C' to save your local XML file")

#ENDIF
ENDPROC



PROC SPTT_Menu_Draw_Choices()

	DRAW_RECT(0.2000, 0.5000, 0.2750, 0.8500, 0, 0, 0, 192)
	
	
	FLOAT	fScale = 1.0
	INT		iRed, iGreen, iBlue, iAlpha
	
		
	INT iChoices, iSchoolScore
	REPEAT SPTT_Master.iRaceCnt iChoices
	
		fScale = 1.0
		iRed = 255
		iGreen = 255
		iBlue = 255
		iAlpha = 255
		
		iSchoolScore = iChallengeScores[iChoices] 
		
		IF SPTT_Master.iRaceCnt = SPTT_Master.iRaceCur
			fScale = 1.0
						
			SWITCH g_savedGlobals.sSPTTData.CourseData[iChoices].status
				CASE SPTT_LOCKED
					g_savedGlobals.sSPTTData.CourseData[iChoices].status = SPTT_UNLOCKED
					GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
					BREAK
				CASE SPTT_UNLOCKED FALLTHRU
				CASE SPTT_NEW_RECORD
					IF iSchoolScore >= SPTT_Master.fTimeGold[iChoices]
						GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iRed, iGreen, iBlue, iAlpha)
					ELIF iSchoolScore >= calculateSilverTime()
						GET_HUD_COLOUR(HUD_COLOUR_BLUE, iRed, iGreen, iBlue, iAlpha)
					ELIF iSchoolScore >= SPTT_Master.fTimeBronze[iChoices]
						GET_HUD_COLOUR(HUD_COLOUR_ORANGE, iRed, iGreen, iBlue, iAlpha)
					ELSE
						GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, iRed, iGreen, iBlue, iAlpha)
					ENDIF
					BREAK
				
				DEFAULT
					SCRIPT_ASSERT("unknown status???")
					BREAK
			ENDSWITCH
		ELSE
			fScale = 0.7500
			
			SWITCH g_savedGlobals.sSPTTData.CourseData[iChoices].status
				CASE SPTT_LOCKED
					g_savedGlobals.sSPTTData.CourseData[iChoices].status = SPTT_UNLOCKED
					GET_HUD_COLOUR(HUD_COLOUR_REDLIGHT, iRed, iGreen, iBlue, iAlpha)
					BREAK
				CASE SPTT_UNLOCKED FALLTHRU
				CASE SPTT_NEW_RECORD
					IF iSchoolScore >= SPTT_Master.fTimeGold[iChoices]
						GET_HUD_COLOUR(HUD_COLOUR_YELLOWLIGHT, iRed, iGreen, iBlue, iAlpha)
					ELIF iSchoolScore >= calculateSilverTime()
						GET_HUD_COLOUR(HUD_COLOUR_BLUELIGHT, iRed, iGreen, iBlue, iAlpha)
					ELIF iSchoolScore >= SPTT_Master.fTimeBronze[iChoices]
						GET_HUD_COLOUR(HUD_COLOUR_ORANGELIGHT, iRed, iGreen, iBlue, iAlpha)
					ELSE
						GET_HUD_COLOUR(HUD_COLOUR_GREY, iRed, iGreen, iBlue, iAlpha)
					ENDIF
					BREAK
				
				DEFAULT
					SCRIPT_ASSERT("unknown status???")
					BREAK
			ENDSWITCH
			
		ENDIF
		
		FLOAT fAddition
		fAddition = 0.0
		IF INT_TO_ENUM(SPTT_Races_ENUM, iChoices) > g_current_selected_SPTT_Race
			fAddition = 0.05
		ENDIF
		
		DISPLAY_TEXT_LABEL(g_savedGlobals.sSPTTData.CourseData[iChoices].name,
					0.0760, (0.1000*0.8) + (fAddition*0.8) + (0.1000*0.5*TO_FLOAT(iChoices)),
					fScale, 0.7500,
					1.0, 1.0, INT_COLOUR(iRed, iGreen, iBlue, iAlpha))	
		IF INT_TO_ENUM(SPTT_Races_ENUM, iChoices) = g_current_selected_SPTT_Race
			TEXT_LABEL sStatus

			iSchoolScore = iChallengeScores[iChoices]//SPTT_Data_Get_Score(g_savedGlobals.sSPTTData.CourseData[iChoices])
			
			SWITCH g_savedGlobals.sSPTTData.CourseData[iChoices].status
				CASE SPTT_LOCKED
					g_savedGlobals.sSPTTData.CourseData[iChoices].status = SPTT_UNLOCKED
					GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
					BREAK
				CASE SPTT_UNLOCKED FALLTHRU
				CASE SPTT_NEW_RECORD
					IF iSchoolScore >= SPTT_Master.fTimeGold[iChoices]
						IF g_savedGlobals.sSPTTData.CourseData[iChoices].status = SPTT_NEW_RECORD
							sStatus = "SPR_rGOLD"
						ELSE
							sStatus = "SPR_GOLD"
						ENDIF
						GET_HUD_COLOUR(HUD_COLOUR_YELLOWLIGHT, iRed, iGreen, iBlue, iAlpha)
					ELIF iSchoolScore >= calculateSilverTime()
						IF g_savedGlobals.sSPTTData.CourseData[iChoices].status = SPTT_NEW_RECORD
							sStatus = "SPR_rSILVER"
						ELSE
							sStatus = "SPR_SILVER"
						ENDIF
						GET_HUD_COLOUR(HUD_COLOUR_BLUELIGHT, iRed, iGreen, iBlue, iAlpha)
					ELIF iSchoolScore >= SPTT_Master.fTimeBronze[iChoices]
						IF g_savedGlobals.sSPTTData.CourseData[iChoices].status = SPTT_NEW_RECORD
							sStatus = "SPR_rBRONZE"
						ELSE
							sStatus = "SPR_BRONZE"
						ENDIF
						GET_HUD_COLOUR(HUD_COLOUR_ORANGELIGHT, iRed, iGreen, iBlue, iAlpha)
					ELSE
						IF iSchoolScore > 0
							sStatus = "SPR_OPEN"
						ELSE
							sStatus = "SPR_UNLOCKED"
							// set as newly unlocked here
							g_current_selected_SPTT_Race = INT_TO_ENUM(SPTT_Races_ENUM, SPTT_Master.iRaceCur)
						ENDIF
						
						GET_HUD_COLOUR(HUD_COLOUR_GREY, iRed, iGreen, iBlue, iAlpha)
					ENDIF
					BREAK
				
				DEFAULT
					SCRIPT_ASSERT("unknown status???")
					BREAK
			ENDSWITCH
			
			setupDisplayText(0.5, 0.5, 1.0, 1.0, INT_COLOUR(iRed, iGreen, iBlue, iAlpha), 0, 0)
			IF iSchoolScore > 0
				DISPLAY_TEXT_WITH_NUMBER(0.1060, (0.1600*0.8) + (0.1000*0.5*TO_FLOAT(iChoices)), sStatus, iSchoolScore)
			ELSE
				DISPLAY_TEXT(0.1060, (0.1600*0.8) + (0.1000*0.5*TO_FLOAT(iChoices)), sStatus)
			ENDIF
					
		ENDIF
		
	ENDREPEAT
		
ENDPROC


//Returns true as long as we're waiting on or receiving valid input, returns false when launching challenge or exiting
FUNC BOOL SPTT_Menu_Get_Input(BOOL &bIsLaunching, UI_INPUT_DATA &uiInput)
	
	IF bIsQuitTrans = TRUE
		//skip input if we're transitioning to quit menu
		RETURN TRUE
		DEBUG_MESSAGE("bIsQuitTrans has returned TRUE")
	ENDIF
	
	//if we were waiting for the trans to finish in order to quit, do it now
	IF bIsWaitingToQuit
		RETURN FALSE
		DEBUG_MESSAGE("bIsWaitingToQuit has returned FALSE")
	ENDIF
	
	IF TIMERA() < 500
		RETURN TRUE
	ENDIF
	
	IF bShowQuitMenu
		
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			iMenuSoundIndex = GET_SOUND_ID()						
			PLAY_SOUND_FRONTEND(iMenuSoundIndex,"YES","HUD_FRONTEND_DEFAULT_SOUNDSET")
			bIsQuitTrans = TRUE
			bIsWaitingToQuit = TRUE
			bIsLaunching = FALSE
			RETURN TRUE
			DEBUG_MESSAGE("bShowQuitMenu has returned TRUE, after hitting CROSS")
		ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			iMenuSoundIndex = GET_SOUND_ID()			
			PLAY_SOUND_FRONTEND(iMenuSoundIndex,"NO","HUD_FRONTEND_DEFAULT_SOUNDSET")
			DEBUG_MESSAGE("bIsQuitTrans has returned FALSE, after hitting CIRCLE")
			CLEAR_HELP()
			
			INIT_SPTT_MENU_USE_CONTEXT(SPTT_Master.uiInput)
	
			bIsQuitTrans = TRUE	
		ELSE
			RETURN TRUE
		ENDIF

	ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		iMenuSoundIndex = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(iMenuSoundIndex,"QUIT","HUD_FRONTEND_DEFAULT_SOUNDSET")
		DEBUG_MESSAGE("EXIT BUTTON PRESSED - Moving to Quit UI")
		
		//we're exiting the menu without launching
		INIT_SIMPLE_USE_CONTEXT(SPTT_Master.uiInput, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(SPTT_Master.uiInput, "FE_HLP29", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ADD_SIMPLE_USE_CONTEXT_INPUT(SPTT_Master.uiInput, "FE_HLP31", FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		
		CANCEL_TIMER(tSPRQuitUITimer)
		bIsQuitTrans = TRUE
		
		CLEAR_HELP()
		
		RETURN TRUE
	ENDIF
	

	#IF IS_DEBUG_BUILD
//		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
//			DEBUG_MESSAGE("LAUNCHING STUNT TUNER")
//			//make sure we tell the menu to launch
//			bIsLaunching = TRUE
//	//		bIsLaunchingStuntTuner = TRUE
//			
//			RETURN FALSE
//		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
			
			STRING sFilePath = "X:/gta5/titleupdate/dev_ng/common/data/script/xml"
			STRING sFileName = "local_SPRRecords.xml"
			
			OPEN_NAMED_DEBUG_FILE(sFilePath , sFileName)
				SAVE_STRING_TO_DEBUG_FILE("<SPRRecords_xml>")SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	<SPR>")SAVE_NEWLINE_TO_DEBUG_FILE()
				
				INT iSave
				REPEAT NUMBER_OF_SPTT_COURSES iSave
					SAVE_SPTT_STRUCT_TO_NAMED_DEBUG_FILE(g_savedGlobals.sSPTTData.CourseData[iSave], sFilePath, sFileName)
				ENDREPEAT
				
				SAVE_STRING_TO_DEBUG_FILE("	</SPR>")SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("</SPRRecords_xml>")SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
			CLOSE_DEBUG_FILE()
			
			TEXT_LABEL_63 str = "saved "
			str += sFileName
			PRINT_STRING_WITH_LITERAL_STRING("STRING", str, DEFAULT_GOD_TEXT_TIME, 1)
			SAVE_STRING_TO_DEBUG_FILE("saved debug info in \"")SAVE_STRING_TO_DEBUG_FILE(sFilePath)SAVE_STRING_TO_DEBUG_FILE(sFileName)SAVE_STRING_TO_DEBUG_FILE("\"")SAVE_NEWLINE_TO_DEBUG_FILE()
			
		ENDIF
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_W)
			INT iRepeat
			IF IS_KEYBOARD_KEY_PRESSED(KEY_LSHIFT)
			OR IS_KEYBOARD_KEY_PRESSED(KEY_RSHIFT)
				//reset the variables to their xml defaults
				
				REPEAT NUMBER_OF_SPTT_COURSES iRepeat
					g_savedGlobals.sSPTTData.fBestTime[iRepeat] = 0
				ENDREPEAT
				
				SPTT_Menu_Reset_Scores()
			ELSE
				//unlock all schools
				REPEAT NUMBER_OF_SPTT_COURSES iRepeat
					IF g_savedGlobals.sSPTTData.CourseData[iRepeat].status = SPTT_LOCKED
						g_savedGlobals.sSPTTData.CourseData[iRepeat].status = SPTT_UNLOCKED
					ENDIF
				ENDREPEAT
				//set stats
				STAT_SET_INT(SP0_FLYING_ABILITY, 100)
				STAT_SET_INT(SP1_FLYING_ABILITY, 100)
				STAT_SET_INT(SP2_FLYING_ABILITY, 100)
				PRINTLN("Set flying ability to 100 for all characters")
			ENDIF
		ENDIF
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			IF g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].status = SPTT_LOCKED
				g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].status = SPTT_UNLOCKED
				iChallengeScores[g_current_selected_SPTT_Race] = -1
			ELSE
				IF g_savedGlobals.sSPTTData.fBestTime[g_current_selected_SPTT_Race] < 0
					//Set_Random_Values_For_SPRRace(g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race])
					iChallengeScores[g_current_selected_SPTT_Race] = 0 //SPTT_Data_Get_Score(g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race])
				ELSE
					
/*					IF (current_pilot_school_stage <> PERFORM_PILOT_CLASS_ONE)
						g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].status		= SPTT_LOCKED
						
						g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].goalTime	= -1
						g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].goalDamage	= 100
						g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].goalDist	= -1
						g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].goalCheck	= -1
						g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].goalPlace	= -1
						iChallengeScores[g_current_selected_SPTT_Race] = -1
					ENDIF
*/				ENDIF
			ENDIF
			//DEBUG_MESSAGE(g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].name, " SCORE IS ", GET_STRING_FROM_INT(SPTT_Data_Get_Score(g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race])))
		ENDIF
		
	#ENDIF
	
	
	// PC keyboard and mouse

	BOOL bCursorAccept = FALSE
	
	IF IS_PC_VERSION()
	AND bShowQuitMenu = FALSE
	
		IF IS_USING_CURSOR(FRONTEND_CONTROL)
				
			// Get the highlighted menu item
			g_iMenuCursorItem = GET_CURSOR_HIGHLIGHTED_ITEM_IN_CUSTOM_MENU( 0.201, 0.222, 0.198, 0.0375, 0.034, 5, 255)
			
			HANDLE_MENU_CURSOR(FALSE, ENUM_TO_INT(g_current_selected_SPTT_Race))
			
			// Get the menu item index that the mouse is over.
			// -1 means the mouse is outside the menu area.
			
			// We know the mouse is inside the menu now...
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
					
				// Selecting again the currently selected item = buy
				IF INT_TO_ENUM( SPTT_Races_ENUM, g_iMenuCursorItem ) = g_current_selected_SPTT_Race
					bCursorAccept = TRUE
				ELSE
					// Select new item
					iMenuSoundIndex = GET_SOUND_ID()
					PLAY_SOUND(iMenuSoundIndex, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
					g_current_selected_SPTT_Race = INT_TO_ENUM(SPTT_Races_ENUM, g_iMenuCursorItem)
					INIT_SPTT_MENU_USE_CONTEXT(SPTT_Master.uiInput)
				ENDIF
						
			ENDIF
			
		ENDIF
			
	ENDIF

	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR bCursorAccept = TRUE
	// Removing this as it will conflict with the PC UI controls - Steve R LDS
	//#IF IS_DEBUG_BUILD
	//	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
	//#ENDIF	//	IS_DEBUG_BUILD
		bCursorAccept = FALSE
	
		IF g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].status = SPTT_LOCKED
			iMenuSoundIndex = GET_SOUND_ID()
			PLAY_SOUND(iMenuSoundIndex, "CANCEL", "HUD_MINI_GAME_SOUNDSET")
			DEBUG_MESSAGE("SPTT_Menu_Get_Input: Race is locked, not launching")
		ELSE
			iMenuSoundIndex = GET_SOUND_ID()
			PLAY_SOUND(iMenuSoundIndex, "SELECT", "HUD_MINI_GAME_SOUNDSET")
			DEBUG_MESSAGE("LAUNCH BUTTON PRESSED")
			//make sure we tell the menu to launch a challenge
			bIsLaunching = TRUE
			RETURN FALSE
		ENDIF
	ENDIF

	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) OR IS_LEFT_ANALOG_AS_DPAD(LEFT_ANALOG_AS_DPAD_UP, uiInput)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
// Removing this as it will conflict with the PC UI controls - Steve R LDS
//#IF IS_DEBUG_BUILD
//	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
//#ENDIF	//	IS_DEBUG_BUILD
		INT iPrevSelectedClass
		iPrevSelectedClass = ENUM_TO_INT(g_current_selected_SPTT_Race) - 1
		IF iPrevSelectedClass < 0
			// set to 5th race (ie 5 minus 1)vv
			iPrevSelectedClass = 4
		ENDIF
		iMenuSoundIndex = GET_SOUND_ID()
		PLAY_SOUND(iMenuSoundIndex, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
		g_current_selected_SPTT_Race = INT_TO_ENUM(SPTT_Races_ENUM, iPrevSelectedClass)
		INIT_SPTT_MENU_USE_CONTEXT(SPTT_Master.uiInput)		
	ENDIF
		
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) OR IS_LEFT_ANALOG_AS_DPAD(LEFT_ANALOG_AS_DPAD_DOWN, uiInput)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
// Removing this as it will conflict with the PC UI controls - Steve R LDS
//#IF IS_DEBUG_BUILD
//	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
//#ENDIF	//	IS_DEBUG_BUILD
		INT iNextSelectedClass
		iNextSelectedClass = ENUM_TO_INT(g_current_selected_SPTT_Race) + 1
		IF iNextSelectedClass > 4
			// set next race to 0 if its higher than 4; only loob through the 5 races, ignore 2 ghost races
			iNextSelectedClass = 0
		ENDIF
		iMenuSoundIndex = GET_SOUND_ID()
		PLAY_SOUND(iMenuSoundIndex, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
		g_current_selected_SPTT_Race = INT_TO_ENUM(SPTT_Races_ENUM, iNextSelectedClass)
		INIT_SPTT_MENU_USE_CONTEXT(SPTT_Master.uiInput)
	ENDIF
	
	
	RETURN TRUE
	
ENDFUNC


PROC SPTT_Menu_Update_Info()
	DRAW_RECT(0.6500, 0.5000, 0.5750, 0.8500, 0, 0, 0, 192)			
	INT iHudRed, iHudGreen, iHudBlue, iHudAlpha
	IF (g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].status <> SPTT_LOCKED)
		IF (g_savedGlobals.sSPTTData.fBestTime[g_current_selected_SPTT_Race] > 0)			
			INT iGoal, iChoice
			iChoice = 0
			REPEAT NUMBER_OF_SPTT_COURSES iGoal
				//Display_Results(g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race])				
			ENDREPEAT
			
			FLOAT			fLabel_posX, fLabel_posY
			fLabel_posX		= 0.1060+0.30
			fLabel_posY		= 0.20 + (0.1000*0.5*TO_FLOAT(iChoice))
			CONST_FLOAT		fLABEL_SCORE_OFFSET		0.35
			
			
			
			GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, iHudRed, iHudGreen, iHudBlue, iHudAlpha)
			setupDisplayText(0.75, 0.75, 1.0, 1.0, INT_COLOUR(iHudRed, iHudGreen, iHudBlue, iHudAlpha), 0, 0)
			DISPLAY_TEXT(0.65 , 0.15, "PS_PB")
			
			//save off the score so we dont calculate it every frame
			IF iChallengeScores[g_current_selected_SPTT_Race] < 0						
				iChallengeScores[g_current_selected_SPTT_Race] = 0 //Race.Racer[iPlayerIndex].fClockTime // SPTT_Data_Get_Score(g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race])
			ENDIF
			
			INT iChallengeScore
			iChallengeScore = iChallengeScores[g_current_selected_SPTT_Race] 
			IF iChallengeScore >= SPTT_Master.fTimeBronze[g_current_selected_SPTT_Race] AND iChallengeScore < calculateSilverTime()
				//BRONZE
				GET_HUD_COLOUR(HUD_COLOUR_ORANGELIGHT, iHudRed, iHudGreen, iHudBlue, iHudAlpha)
				setupDisplayText(0.75, 0.75, 1.0, 1.0, INT_COLOUR(iHudRed, iHudGreen, iHudBlue, iHudAlpha), 0, 0)
				DISPLAY_TEXT(fLabel_posX + fLABEL_SCORE_OFFSET, fLabel_posY, "SPR_sBRONZE")
			ELIF iChallengeScore >= calculateSilverTime(g_current_selected_SPTT_Race) AND iChallengeScore < SPTT_Master.fTimeGold[g_current_selected_SPTT_Race]
				//Silver
				GET_HUD_COLOUR(HUD_COLOUR_BLUELIGHT, iHudRed, iHudGreen, iHudBlue, iHudAlpha)
				setupDisplayText(0.75, 0.75, 1.0, 1.0, INT_COLOUR(iHudRed, iHudGreen, iHudBlue, iHudAlpha), 0, 0)
				DISPLAY_TEXT(fLabel_posX + fLABEL_SCORE_OFFSET, fLabel_posY, "SPR_sSILVER")
			ELIF iChallengeScore >= SPTT_Master.fTimeGold[g_current_selected_SPTT_Race]
				//GOLD
				GET_HUD_COLOUR(HUD_COLOUR_YELLOWLIGHT, iHudRed, iHudGreen, iHudBlue, iHudAlpha)
				setupDisplayText(0.75, 0.75, 1.0, 1.0, INT_COLOUR(iHudRed, iHudGreen, iHudBlue, iHudAlpha), 0, 0)
				DISPLAY_TEXT(fLabel_posX + fLABEL_SCORE_OFFSET, fLabel_posY, "SPR_sGOLD")
			ENDIF
		
			

		ENDIF
			//first time trying it out...
			
			
			INT iGoal //, iChoice
			//iChoice = 0
			
			GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, iHudRed, iHudGreen, iHudBlue, iHudAlpha)
			setupDisplayText(0.75, 0.75, 1.0, 1.0, INT_COLOUR(iHudRed, iHudGreen, iHudBlue, iHudAlpha), 0, 0)
			DISPLAY_TEXT(0.5 , 0.10, g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].name)
			
			
			
			REPEAT NUMBER_OF_SPTT_GOALS iGoal
			
				IF SPTT_Data_Is_Goal_Bit_Set(g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race], INT_TO_ENUM(SPTT_GOAL_BITS, iGoal))
					//Display_Target_Text_Label(g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race], INT_TO_ENUM(SPTT_GOAL_BITS, iGoal), iChoice)
				ENDIF
			
			ENDREPEAT
		
	ELSE
//		INT iClass, iPreReq
//		iPreReq = 0
//		
//		GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, iHudRed, iHudGreen, iHudBlue, iHudAlpha)
//		setupDisplayText(0.75, 0.75, 1.0, 1.0, INT_COLOUR(iHudRed, iHudGreen, iHudBlue, iHudAlpha), 0, 0)
//		DISPLAY_TEXT(0.5 , 0.10, "PS_PREREQ")
//		
//		REPEAT NUMBER_OF_SPTT_COURSES iClass
//			IF SPTT_Data_Is_PreReq_Bit_Set(g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race], INT_TO_ENUM(SPTT_Races_ENUM, iClass))	
//				Display_PreReq_Text_Label(INT_TO_ENUM(SPTT_Races_ENUM, iClass), iPreReq)
//			ENDIF 
//		ENDREPEAT
		
	ENDIF
	
//	Display_mission_description(g_current_selected_SPTT_Race)
ENDPROC


PROC SPTT_Menu_Update()

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	#IF SPTT_USE_NEW_MENU
		PROCESS_SCREEN_SPTT_MENU(SPTT_UI_Placement)
		#IF COMPILE_WIDGET_OUTPUT
			EXPORT_SCREEN_MEGA_PLACEMENT(SPTT_UI_Placement)
		#ENDIF
	#ENDIF
	#IF NOT SPTT_USE_NEW_MENU
		SPTT_Menu_Draw_Choices()
		SPTT_Menu_Draw_Buttons()
		SPTT_Menu_Update_Info()
		#IF IS_DEBUG_BUILD
			SPTT_Menu_Draw_Debug_Options()
		#ENDIF
	#ENDIF

ENDPROC

PROC SPTT_Launch_Race()
	SWITCH(g_current_selected_SPTT_Race)
		CASE SPTT_BridgeBinge
			SPTT_Master.iRaceCur =  ENUM_TO_INT(SPTT_BridgeBinge)
			BREAK
		CASE SPTT_Vinewood_Flyby
			SPTT_Master.iRaceCur =  ENUM_TO_INT(SPTT_Vinewood_Flyby)
			BREAK
		CASE SPTT_Advanced_Race01
			SPTT_Master.iRaceCur =  ENUM_TO_INT(SPTT_Advanced_Race01)
			BREAK
		CASE SPTT_AirportFlyby
			SPTT_Master.iRaceCur = ENUM_TO_INT(SPTT_AirportFlyby)
			BREAK
		CASE SPTT_Altitude
			SPTT_Master.iRaceCur =  ENUM_TO_INT(SPTT_Altitude)
			BREAK
	ENDSWITCH
ENDPROC
