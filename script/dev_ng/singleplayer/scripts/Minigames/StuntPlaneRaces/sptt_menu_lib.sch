USING "hud_drawing.sch"
USING "screens_header.sch"
USING "sptt_head.sch"
USING "SPTT_Menu.sch"
USING "minigame_uiinputs.sch"
USING "SPTT_Data.sch"
USING "UIUtil.sch"
USING "socialclub_leaderboard.sch"

//PROC INIT_SPTT_MENU_USE_CONTEXT(SIMPLE_USE_CONTEXT& uiInput)
//	//TODO: add array overrun protection if necessary
//	IF (g_savedGlobals.sSPTTData.CourseData[g_current_selected_SPTT_Race].status = SPTT_LOCKED)				
//		INIT_SIMPLE_USE_CONTEXT(uiInput, FALSE, FALSE, FALSE, TRUE)
//		ADD_SIMPLE_USE_CONTEXT_INPUT(uiInput, "IB_QUIT",		FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//		ADD_SIMPLE_USE_CONTEXT_INPUT(uiInput, "HUD_INPUT68",	FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
//	ELSE
//		INIT_SIMPLE_USE_CONTEXT(SPTT_Master.uiInput, FALSE, FALSE, FALSE, TRUE)
//		ADD_SIMPLE_USE_CONTEXT_INPUT(SPTT_Master.uiInput, "FE_HLP4",	FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//		ADD_SIMPLE_USE_CONTEXT_INPUT(uiInput, "IB_QUIT",				FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//		ADD_SIMPLE_USE_CONTEXT_INPUT(uiInput, "HUD_INPUT68",			FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
//	ENDIF
//	
//	SET_SIMPLE_USE_CONTEXT_FULLSCREEN(SPTT_Master.uiInput, TRUE)
//ENDPROC
BOOL bIsWidescreen
PROC INIT_SPTT_MENU(MEGA_PLACEMENT_TOOLS &thisPlacement)
	PRINTLN("Setting up new menu!")	
	SC_LEADERBOARD_CACHE_CLEAR_ALL()
//	INIT_SPTT_MENU_USE_CONTEXT(SPTT_Master.uiInput)
	bIsWidescreen = GET_IS_WIDESCREEN()
	RESET_ALL_SPRITE_PLACEMENT_VALUES(thisPlacement.SpritePlacement)
	SET_STANDARD_INGAME_TEXT_DETAILS(thisPlacement.aStyle)
	INIT_SCREEN_SPTT_MENU(thisPlacement)
ENDPROC

PROC PROCESS_SCREEN_SPTT_MENU(MEGA_PLACEMENT_TOOLS &thisPlacement)
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_W)	
		INT iRaceIndex
		REPEAT NUMBER_OF_SPTT_COURSES iRaceIndex	
			g_savedGlobals.sSPTTData.CourseData[iRaceIndex].status = SPTT_UNLOCKED				
		ENDREPEAT
		//set stats
		STAT_SET_INT(SP0_FLYING_ABILITY, 100)
		STAT_SET_INT(SP1_FLYING_ABILITY, 100)
		STAT_SET_INT(SP2_FLYING_ABILITY, 100)
		PRINTLN("Set flying ability to 100 for all characters")
	ENDIF
	#ENDIF
	
	IF NOT bIsWidescreen
		IF NOT IS_PC_VERSION()
			SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_CENTRE)
		ENDIF
	ENDIF

	//pad instructions
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	UPDATE_SIMPLE_USE_CONTEXT(SPTT_Master.uiInput)	
	
//	SPRITE_PLACEMENT tempoverlay
//	PIXEL_POSITION_AND_SIZE_SPRITE(tempoverlay, 0, 0, 1280, 720, TRUE)
//	SET_SPRITE_WHITE(tempoverlay)
//	tempoverlay.a = 128
//	DRAW_2D_SPRITE("SPRRaces", "menu_test", tempoverlay, FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)	
		
	//background
//	DRAW_2D_SPRITE("MPHUD", "MP_Main_Gradient", thisPlacement.SpritePlacement[SPTT_MAIN_BACKGROUND],FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_BEFORE_HUD)
	DRAW_2D_SPRITE("Shared", "BGGradient_32x1024", thisPlacement.SpritePlacement[SPTT_MAIN_BACKGROUND], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_BEFORE_HUD)

	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_SELECTION_BACKGROUND])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_DESCRIPTION_BACKGROUND])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_BACKGROUND])

	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_SELECTION_EDGING])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_DESCRIPTION_EDGING])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_EDGING])

	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_SELECTION_IMAGE_BACKGROUD])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_DESCRIPTION_IMAGE_BACKGROUND])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_IMAGE_BACKGROUND])
	
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_SELECTION_IMAGE_BACKGROUD])
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	//main title
//	SET_TEXT_CURSIVE(thisPlacement.aStyle.TS_TITLE)
	SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TITLE)
	DRAW_TEXT(thisPlacement.TextPlacement[SPTT_MENU_MAIN_TITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "SPR_TITLE")	
//	SET_TEXT_STANDARD(thisPlacement.aStyle.TS_TITLE)
	//subtitles n such
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)

	SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	// Race column, title row
	SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, thisPlacement.RectPlacement[SPTT_SELECTION_BACKGROUND], 0, 0)
		DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[SPTT_SELECTION_TITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "SPR_CHALLENGES", TRUE, FALSE)	
	CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	// info Column, title row
	SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, thisPlacement.RectPlacement[SPTT_DESCRIPTION_BACKGROUND], 0, 0)
		DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[SPTT_DESCRIPTION_TITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "SPR_INFO", TRUE, FALSE)
	CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	// Medal Column, title row
	SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, thisPlacement.RectPlacement[SPTT_AWARDS_EDGING], 0, 0)
		DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[SPTT_AWARDS_TITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "SPR_AWARD_TITLE", TRUE, FALSE)
	CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
	
		//draw challenge selection
	INT iCurIdx = 0
	SPTT_SCREEN_RECT eCurRectIdx = SPTT_SELECTION_ITEM_BG_1
	SPTT_SCREEN_TEXT eCurTitleIdx = SPTT_SELECTION_ITEM_TITLE_1
	SPTT_SCREEN_SPRITE eCurSpriteIdx = SPTT_SELECTION_ITEM_SPRITE_1

//	STRING sCurMedalSprite// = "Locked_Icon_32"
	//draw score/awards info
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_ITEM_BG_1])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_ITEM_BG_2])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_ITEM_BG_3])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_MEDAL_BG_1])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_MEDAL_BG_2])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_MEDAL_BG_3])	
	
	// Are we currently hovering over an unlocked item?
	BOOL bOnUnlocked = TRUE
	SPRITE_PLACEMENT tempSprPlacement
	TEXT_PLACEMENT tempTxtPlacement
	
	//draw selections
	REPEAT NUMBER_OF_SPTT_COURSES iCurIdx
	
		IF iCurIdx < 5
			tempTxtPlacement = thisPlacement.TextPlacement[eCurTitleIdx]
			//grab scores
			IF (g_savedGlobals.sSPTTData.fBestTime[iCurIdx] > 0)
				//iChallengeScores[iCurIdx] = FLOOR(g_savedGlobals.sSPTTData.fBestTime[iCurIdx])
				iChallengeScores[iCurIdx] = CEIL(g_savedGlobals.sSPTTData.fBestTime[iCurIdx])
//					PRINTLN("Best time for index#", iCurIdx, "is ... ", iChallengeScores[iCurIdx])
			ELSE
				iChallengeScores[iCurIdx] = -1
			ENDIF
			//draw selection
			IF iCurIdx = ENUM_TO_INT(g_current_selected_SPTT_Race)//eCurRectIdx = eCurSelectionIdx
				// Draw our selected rectangle!
				SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[eCurRectIdx], HUD_COLOUR_WHITE, TRUE)
//				SET_RECTANGLE_ACTIVE_AND_SELECTED_WHITE(thisPlacement.RectPlacement[eCurRectIdx])
				DRAW_RECTANGLE(thisPlacement.RectPlacement[eCurRectIdx])
				
				SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
		
				SWITCH g_savedGlobals.sSPTTData.CourseData[iCurIdx].status
					CASE SPTT_NEW_RECORD
						//TODO: change this item's award sprite to something else so it says "NEW"
						//new record
//						DRAW_2D_SPRITE("Shared", "NewStar_32", tempSprPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)	
						BREAK
					CASE SPTT_UNLOCKED
						//dont change sprite color
						IF iCurIdx < 5
							IF g_savedGlobals.sSPTTData.fBestTime[iCurIdx] <= 0
								PIXEL_POSITION_AND_SIZE_SPRITE(tempSprPlacement, 269.0000, 0.0, 32.0000, 32.0000)
								SPRITE_COLOR(tempSprPlacement)
								tempSprPlacement.y = thisPlacement.TextPlacement[eCurTitleIdx].y + NEW_STAR_OFFSET_Y
								tempTxtPlacement.x = tempSprPlacement.x + NEW_STAR_OFFSET_X
								IF NOT bIsWidescreen
									FIXUP_SPRITE_FOR_NON_WIDESCREEN(tempSprPlacement)
								ENDIF
								SET_SPRITE_WHITE(tempSprPlacement)
								DRAW_2D_SPRITE("Shared", "NewStar_32", tempSprPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)	
								
								IF NOT bIsWidescreen
									FIXUP_TEXT_FOR_NON_WIDESCREEN(tempTxtPlacement, thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
								ENDIF
							ENDIF
						ENDIF
						BREAK
					
					DEFAULT
						// We're on a locked item.
						bOnUnlocked = FALSE
					BREAK
				ENDSWITCH
				
							
				IF iCurIdx = ENUM_TO_INT(g_current_selected_SPTT_Race)
					SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				ENDIF
				CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				DRAW_TEXT(tempTxtPlacement, thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, SPTT_Master.szRaceName[iCurIdx])
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
				
				// If the item we're on is locked, color it grey. If not, color it white.
				IF bOnUnlocked
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				ELSE
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				ENDIF
				
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				
				IF ((iChallengeScores[iCurIdx] <= SPTT_Master.fTimeGold[iCurIdx]) 
				AND (iChallengeScores[iCurIdx] > 1))
					//DEBUG_MESSAGE("Current race is Gold: Big medal set to gold")
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[SPTT_AWARDS_IMAGE_SPRITE])
					DRAW_2D_SPRITE("SPRRaces", "FlightStunt_Gold_128", thisPlacement.SpritePlacement[SPTT_AWARDS_IMAGE_SPRITE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_AWARDS_ITEM_BG_1], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
//						SET_TEXT_GOLD(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
						SET_TEXT_RIGHT_JUSTIFY(TRUE)
						DRAW_TEXT(thisPlacement.TextPlacement[SPTT_AWARDS_VALUE], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, "SPR_sGOLD")
						SET_TEXT_RIGHT_JUSTIFY(FALSE)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				ELIF ((iChallengeScores[iCurIdx] <= (((SPTT_Master.fTimeBronze[iCurIdx]-SPTT_Master.fTimeGold[iCurIdx])/2) + SPTT_Master.fTimeGold[iCurIdx]))
				AND (iChallengeScores[iCurIdx] > SPTT_Master.fTimeGold[iCurIdx]))
					//DEBUG_MESSAGE("Current race is Silver: Big medal set to Silver")
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[SPTT_AWARDS_IMAGE_SPRITE])
					DRAW_2D_SPRITE("SPRRaces", "FlightStunt_Silver_128", thisPlacement.SpritePlacement[SPTT_AWARDS_IMAGE_SPRITE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_AWARDS_ITEM_BG_1], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
//						SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
						SET_TEXT_RIGHT_JUSTIFY(TRUE)
						DRAW_TEXT(thisPlacement.TextPlacement[SPTT_AWARDS_VALUE], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, "SPR_sSILVER")
						SET_TEXT_RIGHT_JUSTIFY(FALSE)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				ELIF ((iChallengeScores[iCurIdx] <= SPTT_Master.fTimeBronze[iCurIdx])
				AND (iChallengeScores[iCurIdx] > (((SPTT_Master.fTimeBronze[iCurIdx]-SPTT_Master.fTimeGold[iCurIdx])/2) + SPTT_Master.fTimeGold[iCurIdx])))
					//DEBUG_MESSAGE("Current race is Bronze: Big medal set to Bronze")
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[SPTT_AWARDS_IMAGE_SPRITE])
					DRAW_2D_SPRITE("SPRRaces", "FlightStunt_Bronze_128", thisPlacement.SpritePlacement[SPTT_AWARDS_IMAGE_SPRITE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_AWARDS_ITEM_BG_1], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
//						SET_TEXT_DARK_RED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
						SET_TEXT_RIGHT_JUSTIFY(TRUE)
						DRAW_TEXT(thisPlacement.TextPlacement[SPTT_AWARDS_VALUE], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, "SPR_sBRONZE")
						SET_TEXT_RIGHT_JUSTIFY(FALSE)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				ELIF ((iChallengeScores[iCurIdx] > SPTT_Master.fTimeBronze[iCurIdx])
				OR (iChallengeScores[iCurIdx] = -1))
					//DEBUG_MESSAGE("Current race is blank: Big medal set to blank")
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[SPTT_AWARDS_IMAGE_SPRITE])
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_AWARDS_ITEM_BG_1], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
						SET_TEXT_RIGHT_JUSTIFY(TRUE)
						DRAW_TEXT(thisPlacement.TextPlacement[SPTT_AWARDS_VALUE], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, "SC_LB_EMPTY")
						SET_TEXT_RIGHT_JUSTIFY(FALSE)
					DRAW_2D_SPRITE("SPRRaces", "Empty_128", thisPlacement.SpritePlacement[SPTT_AWARDS_IMAGE_SPRITE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
					//set it gray...
					SET_SPRITE_BLACK(thisPlacement.SpritePlacement[eCurSpriteIdx])
					
				ENDIF
				
				// gold
				
				IF ((iChallengeScores[iCurIdx] <= SPTT_Master.fTimeGold[iCurIdx]) 
				AND (NOT (iChallengeScores[iCurIdx] = -1)))
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[SPTT_AWARDS_MEDAL_SPRITE_3])	
				ELSE
					thisPlacement.SpritePlacement[SPTT_AWARDS_MEDAL_SPRITE_3].a = ROUND(255 * 0.30)
				ENDIF
				// silver
				
				IF ((iChallengeScores[iCurIdx] <= (((SPTT_Master.fTimeBronze[iCurIdx]-SPTT_Master.fTimeGold[iCurIdx])/2) + SPTT_Master.fTimeGold[iCurIdx]))
				AND (NOT (iChallengeScores[iCurIdx] = -1)))
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[SPTT_AWARDS_MEDAL_SPRITE_2])		
				ELSE
					thisPlacement.SpritePlacement[SPTT_AWARDS_MEDAL_SPRITE_2].a = ROUND(255 * 0.30)
				ENDIF
				// bronze
				
				IF ((iChallengeScores[iCurIdx] <= SPTT_Master.fTimeBronze[iCurIdx])
				AND (NOT (iChallengeScores[iCurIdx] = -1)))
					SET_SPRITE_WHITE(thisPlacement.SpritePlacement[SPTT_AWARDS_MEDAL_SPRITE_1])
				ELSE
					thisPlacement.SpritePlacement[SPTT_AWARDS_MEDAL_SPRITE_1].a = ROUND(255 * 0.30)
				ENDIF
				DRAW_2D_SPRITE("SPRRaces", "FlightStunt_Bronze_128", thisPlacement.SpritePlacement[SPTT_AWARDS_MEDAL_SPRITE_1], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("SPRRaces", "FlightStunt_Silver_128", thisPlacement.SpritePlacement[SPTT_AWARDS_MEDAL_SPRITE_2], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				DRAW_2D_SPRITE("SPRRaces", "FlightStunt_Gold_128", thisPlacement.SpritePlacement[SPTT_AWARDS_MEDAL_SPRITE_3], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
				
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				// DISPLAY BEST TIME ::				
				IF g_savedGlobals.sSPTTData.fBestTime[g_current_selected_SPTT_Race] > 0
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_AWARDS_ITEM_BG_2], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					SET_TEXT_RIGHT_JUSTIFY(TRUE)
					DRAW_TEXT_TIMER(thisPlacement.TextPlacement[SPTT_BEST_TIME_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, FLOOR(g_savedGlobals.sSPTTData.fBestTime[g_current_selected_SPTT_Race] * 1000.0),TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", FALSE, TRUE )
					SET_TEXT_RIGHT_JUSTIFY(FALSE)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				ELSE
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_AWARDS_ITEM_BG_2], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					SET_TEXT_RIGHT_JUSTIFY(TRUE)
					DRAW_TEXT(thisPlacement.TextPlacement[SPTT_BEST_TIME_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, "SC_LB_EMPTY")
					SET_TEXT_RIGHT_JUSTIFY(FALSE)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)				
				ENDIF
				DRAW_TEXT(thisPlacement.TextPlacement[SPTT_RECENT_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, "SPR_RTIME")//most recent time
				IF SPTT_Master.fLastTime[g_current_selected_SPTT_Race] > 0
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_AWARDS_ITEM_BG_3], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					SET_TEXT_RIGHT_JUSTIFY(TRUE)
					DRAW_TEXT_TIMER(thisPlacement.TextPlacement[SPTT_RECENT_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, FLOOR(SPTT_Master.fLastTime[g_current_selected_SPTT_Race] * 1000.0), TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", FALSE, TRUE )
					SET_TEXT_RIGHT_JUSTIFY(FALSE)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				ELSE
					SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_AWARDS_ITEM_BG_3], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
					SET_TEXT_RIGHT_JUSTIFY(TRUE)
					DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[SPTT_RECENT_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, "SC_LB_EMPTY", FALSE, TRUE)
					SET_TEXT_RIGHT_JUSTIFY(FALSE)
					CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
					SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				ENDIF

												
			ELSE
				// This item isn't selected. Draw an unselected rect.
				SET_RECT_UNDIMMED(thisPlacement.RectPlacement[eCurRectIdx])
				SET_RECT_TO_THIS_HUD_COLOUR(thisPlacement.RectPlacement[eCurRectIdx], HUD_COLOUR_PAUSE_BG, TRUE)
				DRAW_RECTANGLE(thisPlacement.RectPlacement[eCurRectIdx])
				
				//make sure mini-award or lock is shown
				SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
				//dont highlight rect or text
				SWITCH g_savedGlobals.sSPTTData.CourseData[iCurIdx].status
					CASE SPTT_LOCKED
						SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
						SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
						BREAK
					CASE SPTT_NEW_RECORD
						//TODO: change this item's award sprite to something else so it says "NEW"
//						SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
//						DRAW_2D_SPRITE("Shared", "NewStar_32", tempSprPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)	
//						SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
						BREAK
					CASE SPTT_UNLOCKED
						//dont change sprite color
						IF iCurIdx < 5
							SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
							IF g_savedGlobals.sSPTTData.fBestTime[iCurIdx] <= 0
								PIXEL_POSITION_AND_SIZE_SPRITE(tempSprPlacement, 269.0000, 0.0, 32.0000, 32.0000)
								SPRITE_COLOR(tempSprPlacement)
								tempSprPlacement.y = thisPlacement.TextPlacement[eCurTitleIdx].y + NEW_STAR_OFFSET_Y
								tempTxtPlacement.x = tempSprPlacement.x + NEW_STAR_OFFSET_X
								IF NOT bIsWidescreen
									FIXUP_SPRITE_FOR_NON_WIDESCREEN(tempSprPlacement)
								ENDIF
								SET_SPRITE_WHITE(tempSprPlacement)
								DRAW_2D_SPRITE("Shared", "NewStar_32", tempSprPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)	
								
								IF NOT bIsWidescreen
									FIXUP_TEXT_FOR_NON_WIDESCREEN(tempTxtPlacement, thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
								ENDIF
							ENDIF
						ENDIF
						BREAK
				ENDSWITCH
				IF iCurIdx = ENUM_TO_INT(g_current_selected_SPTT_Race)
					SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				ENDIF
				CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				DRAW_TEXT(tempTxtPlacement, thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, SPTT_Master.szRaceName[iCurIdx])
				SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
				SET_SPRITE_WHITE(thisPlacement.SpritePlacement[eCurSpriteIdx])
				
			ENDIF
			
			//draw small medals
			IF g_savedGlobals.sSPTTData.CourseData[iCurIdx].status = SPTT_LOCKED
				//PRINTLN("Shared loaded: ", HAS_STREAMED_TEXTURE_DICT_LOADED("Shared"))
				DRAW_2D_SPRITE("Shared", "Locked_Icon_32", thisPlacement.SpritePlacement[eCurSpriteIdx], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)	
			ELIF iChallengeScores[iCurIdx] > 0.0
				IF iChallengeScores[iCurIdx] <= SPTT_Master.fTimeGold[iCurIdx] AND iChallengeScores[iCurIdx] <= SPTT_Master.fTimeBronze[iCurIdx]
					SET_SPRITE_GOLD(thisPlacement.SpritePlacement[eCurSpriteIdx])
					DRAW_2D_SPRITE("Shared", "MedalDot_32", thisPlacement.SpritePlacement[eCurSpriteIdx], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)		
				ELIF iChallengeScores[iCurIdx] <= ((SPTT_Master.fTimeBronze[iCurIdx]-SPTT_Master.fTimeGold[iCurIdx])/2)+SPTT_Master.fTimeGold[iCurIdx]
					SET_SPRITE_SILVER(thisPlacement.SpritePlacement[eCurSpriteIdx])
					DRAW_2D_SPRITE("Shared", "MedalDot_32", thisPlacement.SpritePlacement[eCurSpriteIdx], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)		
				ELIF iChallengeScores[iCurIdx] <= SPTT_Master.fTimeBronze[iCurIdx]
					SET_SPRITE_BRONZE(thisPlacement.SpritePlacement[eCurSpriteIdx])
					DRAW_2D_SPRITE("Shared", "MedalDot_32", thisPlacement.SpritePlacement[eCurSpriteIdx], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)		
				ENDIF
			ENDIF
		
		
			eCurRectIdx += INT_TO_ENUM(SPTT_SCREEN_RECT, 1)
			eCurTitleIdx += INT_TO_ENUM(SPTT_SCREEN_TEXT, 1)
			eCurSpriteIdx += INT_TO_ENUM(SPTT_SCREEN_SPRITE, 1)
		ENDIF
		
	ENDREPEAT
	
	
	
	INT thisFlyingAbility = 0
	IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_MICHAEL)
		STAT_GET_INT(SP0_FLYING_ABILITY, thisFlyingAbility)
	ELIF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_FRANKLIN)
		STAT_GET_INT(SP1_FLYING_ABILITY, thisFlyingAbility)
	ELIF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
		STAT_GET_INT(SP2_FLYING_ABILITY, thisFlyingAbility)
	ENDIF
	IF thisFlyingAbility < 40
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_MORE_TRAINING_BG_TEXT], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		SET_TEXT_LEADING(1)

		TEXT_PLACEMENT txtTemp = thisPlacement.TextPlacement[SPTT_MORE_TRAINING_TEXT]
		TEXT_STYLE txtStyleTemp = thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY
		IF NOT bIsWidescreen
			txtStyleTemp.WrapEndX -= 0.08
			FIXUP_TEXT_FOR_NON_WIDESCREEN(txtTemp, txtStyleTemp)
		ENDIF

		IF bIsWidescreen
			SET_RECT_DYNAMIC_TO_STRING(thisPlacement.RectPlacement[SPTT_MORE_TRAINING_BG], "FLY_STAT_WARN", txtTemp, txtStyleTemp, 27.0, 2.0, 5, 159.0)
		ELSE
			SET_RECT_DYNAMIC_TO_STRING(thisPlacement.RectPlacement[SPTT_MORE_TRAINING_BG], "FLY_STAT_WARN", txtTemp, txtStyleTemp, 18.0, 1.55, 5, 106.0)
		ENDIF
		DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_MORE_TRAINING_BG])
		
		DRAW_2D_SPRITE("Shared", "Info_Icon_32", thisPlacement.SpritePlacement[eCurSpriteIdx], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)	
		DRAW_TEXT(thisPlacement.TextPlacement[SPTT_MORE_TRAINING_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, "FLY_STAT_WARN")
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
	ENDIF
	
	// SECOND COLUMN
	// Draw description info
	TEXT_LABEL sDescriptionImage = "SPR_DESC_"
	sDescriptionImage += ENUM_TO_INT(g_current_selected_SPTT_Race)+1
	SET_SPRITE_WHITE(thisPlacement.SpritePlacement[SPTT_DESCRIPTION_IMAGE_SPRITE])
	DRAW_2D_SPRITE("SPRRaces", sDescriptionImage, thisPlacement.SpritePlacement[SPTT_DESCRIPTION_IMAGE_SPRITE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
	
	// DYNAMIC DESCRIPTION TEXT AND RECT.
	SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_DESCRIPTION_1_INFO_BG], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
	SET_TEXT_LEADING(1)
	TEXT_LABEL sDescriptionLabel = "SPR_DESC_"
	IF bOnUnlocked
		sDescriptionLabel += ENUM_TO_INT(g_current_selected_SPTT_Race)+1
	ELSE
		sDescriptionLabel += 0
	ENDIF
	
	TEXT_PLACEMENT txtTemp = thisPlacement.TextPlacement[SPTT_DESCRIPTION_INFO_TEXT]
	TEXT_STYLE txtStyleTemp = thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY
	IF NOT bIsWidescreen
		txtStyleTemp.WrapEndX -= 0.108
		FIXUP_TEXT_FOR_NON_WIDESCREEN(txtTemp, txtStyleTemp)
	ENDIF
	
	// This rectangle needs to be shrunk to be the size of the text that we'll be drawing in it.
	IF bIsWidescreen
		SET_RECT_DYNAMIC_TO_STRING(thisPlacement.RectPlacement[SPTT_DESCRIPTION_1_INFO_BG], sDescriptionLabel, txtTemp, txtStyleTemp, 27.0, 2.0, 5, 159.0)
	ELSE
		SET_RECT_DYNAMIC_TO_STRING(thisPlacement.RectPlacement[SPTT_DESCRIPTION_1_INFO_BG], sDescriptionLabel, txtTemp, txtStyleTemp, 18.0, 1.55, 5, 106.0)
	ENDIF
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_DESCRIPTION_1_INFO_BG])
	
	// Now draw the text.
	DRAW_TEXT(thisPlacement.TextPlacement[SPTT_DESCRIPTION_INFO_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, sDescriptionLabel)	
	CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
	
	// 3RD COLUMN
	// Need to draw these grey when over a locked item: Won column, Best Time Row
	SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
	DRAW_TEXT(thisPlacement.TextPlacement[SPTT_AWARDS_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, "SPR_AWARDS_SUB")
	DRAW_TEXT(thisPlacement.TextPlacement[SPTT_BEST_TIME_SUBTITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, "SPR_BEST")


	SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_MEDAL_SUB_1])
	SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_AWARDS_MEDAL_SUB_1])
		DRAW_TEXT_TIMER(thisPlacement.TextPlacement[SPTT_MEDAL_TIME_TEXT_1], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, FLOOR(SPTT_Master.fTimeBronze[g_current_selected_SPTT_Race]) * 1000 ,TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE)
	CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
	
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_MEDAL_SUB_2])
	SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_AWARDS_MEDAL_SUB_2])
		DRAW_TEXT_TIMER(thisPlacement.TextPlacement[SPTT_MEDAL_TIME_TEXT_2], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, FLOOR((((SPTT_Master.fTimeBronze[g_current_selected_SPTT_Race]-SPTT_Master.fTimeGold[g_current_selected_SPTT_Race])/2) + SPTT_Master.fTimeGold[g_current_selected_SPTT_Race])) * 1000,TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE )
	CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
	
	DRAW_RECTANGLE(thisPlacement.RectPlacement[SPTT_AWARDS_MEDAL_SUB_3])
	SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, thisPlacement.RectPlacement[SPTT_AWARDS_MEDAL_SUB_3])
		DRAW_TEXT_TIMER(thisPlacement.TextPlacement[SPTT_MEDAL_TIME_TEXT_3], thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, FLOOR(SPTT_Master.fTimeGold[g_current_selected_SPTT_Race]) * 1000 ,TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE)
	CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
	SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)

ENDPROC

