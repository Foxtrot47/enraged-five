USING "commands_stats.sch"
USING "socialclub_leaderboard.sch"
USING "net_leaderboards.sch"

SC_LEADERBOARD_CONTROL_STRUCT spttLBControl
INT iBS = 0

PROC SPTT_WRITE_TIME_TO_LEADERBOARD(SPTT_Races_ENUM eCourse, FLOAT fRaceTime, INT iRank)
	PRINTLN("SPTT_WRITE_TIME_TO_LEADERBOARD: Writing leaderboards for ", 
		PICK_STRING(eCourse = SPTT_BridgeBinge, "Bridge Binge", 
		PICK_STRING(eCourse = SPTT_Vinewood_Flyby, "Vinewood Flyby", 
		PICK_STRING(eCourse = SPTT_Advanced_Race01, "Engineered Bridgework",
		PICK_STRING(eCourse = SPTT_AirportFlyby, "Airport Flyby",
		PICK_STRING(eCourse = SPTT_Altitude, "Altitude", "Invalid Enum"))))))
	
	INT numBronzeMedals = 0, numSilverMedals = 0, numGoldMedals = 0
	
	IF iRank = 1
		numGoldMedals++
	ELIF iRank = 2
		numSilverMedals++
	ELIF iRank = 3
		numBronzeMedals++
	ENDIF
	
	
	
	// NOW PASS THE DATA IN FOR SUBMISSION
	TEXT_LABEL_23 sIdentifier[3]
	TEXT_LABEL_31 categoryName[3]
	categoryName[0] = "GameType"
	categoryName[1] = "Location"
	categoryName[2] = "Type"  
	sIdentifier[0] = "SP"
	// Begin setting the leaderboard data types
	SWITCH eCourse
		CASE SPTT_BridgeBinge //Bridge Binge
		 	CDEBUG1LN( DEBUG_OR_RACES, "iRank is ", iRank)
			CDEBUG1LN( DEBUG_OR_RACES, "fRaceTime is ", fRaceTime, " for TRIAL_01")
			sIdentifier[1] = "TRIAL_01"
			BREAK
		CASE SPTT_Vinewood_Flyby //Vinewood Flyby
			CDEBUG1LN( DEBUG_OR_RACES, "iRank is ", iRank)
			CDEBUG1LN( DEBUG_OR_RACES, "fRaceTime is ", fRaceTime, " for TRIAL_02")
			sIdentifier[1] = "TRIAL_02"
			BREAK
		CASE SPTT_Advanced_Race01 //Engineered Bridgework
			CDEBUG1LN( DEBUG_OR_RACES, "iRank is ", iRank)
			CDEBUG1LN( DEBUG_OR_RACES, "fRaceTime is ", fRaceTime, " for TRIAL_03")
			sIdentifier[1] = "TRIAL_03"
			BREAK
		CASE SPTT_AirportFlyby //Airport Flyby
			CDEBUG1LN( DEBUG_OR_RACES, "iRank is ", iRank)
			CDEBUG1LN( DEBUG_OR_RACES, "fRaceTime is ", fRaceTime, " for TRIAL_04")
			sIdentifier[1] = "TRIAL_04"
			BREAK
		CASE SPTT_Altitude //Altitude
			CDEBUG1LN( DEBUG_OR_RACES, "iRank is ", iRank)
			CDEBUG1LN( DEBUG_OR_RACES, "fRaceTime is ", fRaceTime, " for TRIAL_05")
			sIdentifier[1] = "TRIAL_05"
			BREAK
	ENDSWITCH
	sIdentifier[2] = "StuntPlaneRace"
	
	IF INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_RACES, sIdentifier, categoryName, 3, DEFAULT, TRUE)
	
		// Write the best time to the data
//		LEADERBOARDS_WRITE_SET_INT(0,-CEIL(fRaceTime*1000))
		IF iRank <=3 AND iRank > 0
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_SCORE, -CEIL(fRaceTime*100)*10, 0)
			//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, -CEIL(fRaceTime*100)*10, 0)
		ELSE
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_SCORE, 0, 0)
			//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, 0, 0)
		ENDIF
		//medal?
//		LEADERBOARDS_WRITE_SET_INT(1,0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_MEDAL, numBronzeMedals*1 + numSilverMedals*2 + numGoldMedals*3, 0)
		//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_MEDAL, numBronzeMedals*1 + numSilverMedals*2 + numGoldMedals*3, 0)
		//lap time
//		LEADERBOARDS_WRITE_SET_INT(2,0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_BEST_TIME, CEIL(fRaceTime*100)*10, 0)
		//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BEST_TIME, CEIL(fRaceTime*100)*10, 0)
		//total time
//		LEADERBOARDS_WRITE_SET_INT(3, CEIL(fRaceTime*1000))
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_TOTAL_TIME, CEIL(fRaceTime*100)*10, 0)
		//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME, CEIL(fRaceTime*100)*10, 0)
		//vehicle
//		LEADERBOARDS_WRITE_SET_INT(4,0) //ENUM_TO_INT(Model name)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_VEHICLE_ID, 0, 0)
		//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_VEHICLE_ID, 0, 0)
		//vehicle colour dont think we need this
//		LEADERBOARDS_WRITE_SET_INT(5,0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_VEHICLE_COLOR, 0, 0)
		//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_VEHICLE_COLOR, 0, 0)
		//attempts
//		LEADERBOARDS_WRITE_SET_INT(6,1)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_NUM_MATCHES, 1, 0)
		//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES, 1, 0)
		//write 1 if bronze medal
//		LEADERBOARDS_WRITE_SET_INT(7, 0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_BRONZE_MEDALS, numBronzeMedals, 0)
		//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BRONZE_MEDALS, numBronzeMedals, 0)
		//write 1 if silver medal
//		LEADERBOARDS_WRITE_SET_INT(8, 0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_SILVER_MEDALS, numSilverMedals, 0)
		//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SILVER_MEDALS, numSilverMedals, 0)
		//write 1 if gold medal
//		LEADERBOARDS_WRITE_SET_INT(9, 0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_GOLD_MEDALS, numGoldMedals, 0)
		//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_GOLD_MEDALS, numGoldMedals, 0)
	ENDIF
ENDPROC
//
//-----------------------------------------------------
//Name: MINI_GAMES_RACES
//ID: 817 - LEADERBOARD_MINI_GAMES_RACES
//Inputs: 10
//	LB_INPUT_COL_SCORE (long)
//	LB_INPUT_COL_MEDAL (int)
//	LB_INPUT_COL_BEST_TIME (long)
//	LB_INPUT_COL_TOTAL_TIME (long)
//	LB_INPUT_COL_VEHICLE_ID (int)
//	LB_INPUT_COL_VEHICLE_COLOR (int)
//	LB_INPUT_COL_NUM_MATCHES (int)
//	LB_INPUT_COL_BRONZE_MEDALS (int)
//	LB_INPUT_COL_SILVER_MEDALS (int)
//	LB_INPUT_COL_GOLD_MEDALS (int)
//Columns: 10
//	SCORE_COLUMN ( AGG_Max ) - InputId: LB_INPUT_COL_SCORE
//	MEDAL ( AGG_Last ) - InputId: LB_INPUT_COL_MEDAL
//	LAP_TIME ( AGG_Last ) - InputId: LB_INPUT_COL_BEST_TIME
//	TOTAL_TIME ( AGG_Last ) - InputId: LB_INPUT_COL_TOTAL_TIME
//	VEHICLE ( AGG_Last ) - InputId: LB_INPUT_COL_VEHICLE_ID
//	VEHICLE_COLOR ( AGG_Last ) - InputId: LB_INPUT_COL_VEHICLE_COLOR
//	NUM_ATTEMPTS ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_MATCHES
//	NUM_BRONZE_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_BRONZE_MEDALS
//	NUM_SILVER_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_SILVER_MEDALS
//	NUM_GOLD_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_GOLD_MEDALS
//Instances: 3
//	GameType,Location,Type
//	GameType,Location,Type,Challenge
//	GameType,Location,Type,ScEvent

PROC SPTT_LOAD_SOCIAL_CLUB_LEADERBOARD(SPTT_Races_ENUM eCourse, STRING sTitle)
	TEXT_LABEL_23 sUniqueIdentifier
	SWITCH eCourse
		CASE SPTT_BridgeBinge //Bridge Binge
		 	sUniqueIdentifier = "TRIAL_01"
			sTitle = "SPTTLB_1"
			BREAK
		CASE SPTT_Vinewood_Flyby //Vinewood Flyby
			sUniqueIdentifier = "TRIAL_02"
			sTitle = "SPTTLB_2"
			BREAK
		CASE SPTT_Advanced_Race01 //Engineered Bridgework
			sUniqueIdentifier = "TRIAL_03"
			sTitle = "SPTTLB_3"
			BREAK
		CASE SPTT_AirportFlyby //Airport Flyby
			sUniqueIdentifier = "TRIAL_04"
			sTitle = "SPTTLB_4"
			BREAK
		CASE SPTT_Altitude //Altitude
			sUniqueIdentifier = "TRIAL_05"
			sTitle = "SPTTLB_5"
			BREAK
	ENDSWITCH
	SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(spttLBControl, FMMC_TYPE_SP_STUNT_PLANE_RACES, sUniqueIdentifier, sTitle)
ENDPROC


FUNC BOOL SPTT_DO_RANK_PREDICTION(SPTT_Races_ENUM eCourse, FLOAT fRaceTime, INT iRank)

	IF scLB_rank_predict.bFinishedRead
	AND NOT scLB_rank_predict.bFinishedWrite
		
		SPTT_WRITE_TIME_TO_LEADERBOARD(eCourse, fRaceTime, iRank)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("GET_RANK_PREDICTION_DETAILS CURRENT RUN values (JUST AFTER WRITE) " ) 
			PRINT_RANK_PREDICTION_STRUCT(scLB_rank_predict.currentResult)
		#ENDIF
	
		scLB_rank_predict.bFinishedWrite = TRUE
	ENDIF
	IF GET_RANK_PREDICTION_DETAILS(spttLBControl)
		sclb_useRankPrediction = TRUE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SPTT_DISPLAY_SOCIAL_CLUB_LEADERBOARD(SCALEFORM_INDEX &uiLeaderboard, SPTT_Races_ENUM eCourse, STRING sTitle)	
	UPDATE_SIMPLE_USE_CONTEXT(SPTT_Master.uiInput)	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
 	SPTT_LOAD_SOCIAL_CLUB_LEADERBOARD(eCourse, sTitle)
	DRAW_SC_SCALEFORM_LEADERBOARD(uiLeaderboard, spttLBControl)
ENDPROC

PROC SPTT_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(spttLBControl)
ENDPROC

PROC SPTT_Draw_Leaderboard(SCALEFORM_INDEX &uiLeaderboard, SPTT_Races_ENUM eCourse)
	SPTT_LOAD_SOCIAL_CLUB_LEADERBOARD(eCourse)
	DRAW_SC_SCALEFORM_LEADERBOARD(uiLeaderboard, spttLBControl)
ENDPROC

PROC SPTT_Cleanup_Leaderboard(SCALEFORM_INDEX &uiLeaderboard)
	CLEANUP_SC_LEADERBOARD_UI(uiLeaderboard)
ENDPROC
