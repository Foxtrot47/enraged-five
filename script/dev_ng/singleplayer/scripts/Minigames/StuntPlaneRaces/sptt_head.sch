// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	SPTT_Head.sch
//		AUTHOR			:	Nicholas Zippmann
//		DESCRIPTION		:	Single Player Races - Head global data file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "minigames_helpers.sch"
//USING "minigame_uiinputs.sch"
USING "script_usecontext.sch"
USING "commands_xml.sch"
USING "commands_water.sch"
USING "screen_placements.sch"
USING "end_screen.sch"
USING "minigame_big_message.sch"

CONST_INT	SPTT_ENABLE_WIDGETS				0				// 1 to use widgets


#IF NOT DEFINED(SPTT_RACE_IS_TRIATHLON)
	CONST_INT SPTT_RACE_IS_TRIATHLON		0	
#ENDIF


// -----------------------------------
// CONSTANTS
// -----------------------------------



#IF NOT DEFINED(SPTT_GATE_MAX)
CONST_INT 	SPTT_GATE_MAX 					33				// Max race gates.
#ENDIF
CONST_INT	SPTT_RACER_MAX					8				// Max race racers.
CONST_INT	SPTT_FADE_LONG_TIME				5000			// Fade time (long) (ms).
CONST_INT	SPTT_FADE_NORM_TIME				1000			// Fade time (normal) (ms).
CONST_INT	SPTT_FADE_QUICK_TIME			500				// Fade time (quick) (ms).
CONST_FLOAT	SPTT_CLEAR_AREA_RADIUS			30.0			// Clear area radius (m).
CONST_FLOAT	SPTT_ON_GROUND_DIST				10.0			// On ground distance (m).
CONST_FLOAT SPTT_GATE_CHKPNT_SCL			16.0			// Gate checkpoint size.
CONST_FLOAT SPTT_GATE_CHKPNTNXT_SCL			7.0				// Gate next checkpoint size.
CONST_FLOAT SPTT_GATE_BLIPCUR_SCL			1.2				// Gate current blip scale.
CONST_FLOAT SPTT_GATE_BLIPNXT_SCL			0.7				// Gate next blip scale.
CONST_FLOAT SPTT_GATE_MISS_DIST_SCL			5.0				// Gate miss distance scale.
CONST_FLOAT SPTT_GATE_MISS_PENALTY			5.0				// Gate miss penalty (s).
CONST_FLOAT SPTT_GATE_INNER_PASS_BONUS		1.0				// Gate inner pass bonus (s).
CONST_FLOAT SPTT_GATE_OUTTER_PASS_BONUS		0.0				// Gate Outter pass bonus (s).
CONST_FLOAT SPTT_GATE_OUTTER_KNIFE_BONUS	1.5				// Gate Outter Knife bonus (s).
CONST_FLOAT SPTT_GATE_OUTTER_INVERT_BONUS	2.0				// Gate Outter Invert bonus (s).
CONST_FLOAT SPTT_GATE_INNER_KNIFE_BONUS		2.5				// Gate Inner Knife bonus (s).
CONST_FLOAT SPTT_GATE_INNER_INVERT_BONUS	3.0				// Gate Outter pass bonus (s).
CONST_FLOAT	SPTT_GATE_STUNT_BONUS			4.0				// Stunt Gate hit bonus
CONST_FLOAT	SPTT_GATE_STUNT_HIT				1.5				// Stunt Gate failure to do stunt
CONST_FLOAT SPTT_GATE_WRONG_PENALTY			2.5				// Gate miss penalty (s).
CONST_INT	SPTT_GATE_MISS_DISP_TIME		2500 			// Gate miss display time (ms).
CONST_FLOAT SPTT_GATE_HEIGHT_CHECK			10.0			// Gate height check - to fix Bug # 666112 - DS

//CONST_FLOAT SPTT_STUNT_INVERT_MIN	-140.0			// The min amount the player needs to get the invert stunt bonus
//CONST_FLOAT SPTT_STUNT_INVERT_MAX	140.0			// The max amount the player can be inverted to get the invert stunt bonus
CONST_FLOAT SPTT_STUNT_INVERT_THRESHOLD		-0.8660 	// -cos(30)

CONST_INT	SPTT_VEH_STUCK_PLANE_ROOF_TIME	1000	// Vehicle stuck time (roof) (ms).
CONST_INT	SPTT_VEH_STUCK_PLANE_SIDE_TIME	1000	// Vehicle stuck time (side) (ms).
CONST_INT	SPTT_VEH_STUCK_PLANE_HUNG_TIME	10000	// Vehicle stuck time (hung) (ms).
CONST_INT	SPTT_VEH_STUCK_PLANE_JAM_TIME	10000	// Vehicle stuck time (jam) (ms).

CONST_INT	SPTT_VEH_STUCK_BIKE_ROOF_TIME	5000	// Vehicle stuck time (roof) (ms).
CONST_INT	SPTT_VEH_STUCK_BIKE_SIDE_TIME	20000	// Vehicle stuck time (side) (ms).
CONST_INT	SPTT_VEH_STUCK_BIKE_HUNG_TIME	30000	// Vehicle stuck time (hung) (ms).
CONST_INT	SPTT_VEH_STUCK_BIKE_JAM_TIME	60000	// Vehicle stuck time (jam) (ms).

CONST_FLOAT	SPTT_HUD_NUM_WIDTH			0.0165			// HUD number width for offsetting.
CONST_FLOAT	SPTT_HUD_CLN_WIDTH			0.0265			// HUD colon width for offsetting.
CONST_FLOAT	SPTT_HUD_PLS_WIDTH			0.0175			// HUD plus width for offsetting.
CONST_FLOAT	SPTT_HUD_MNS_WIDTH			0.0095			// HUD minus width for offsetting.
CONST_FLOAT	SPTT_HUD_RADAR_POS_X		0.109 			// HUD radar overlay pos (x).
CONST_FLOAT	SPTT_HUD_RADAR_POS_Y		0.850 			// HUD radar overlay pos (y).
CONST_FLOAT	SPTT_HUD_RADAR_WIDTH		0.110			// HUD radar overlay width.
CONST_FLOAT	SPTT_HUD_RADAR_HEIGHT		0.200 			// HUD radar overlay height.
CONST_FLOAT	SPTT_HUD_RADAR_ROT_SCL		-1.0 			// HUD radar overlay rot scale.

CONST_FLOAT SPTT_UI_CR_HEAD_POS_X		0.4				// UI choose race heading pos (x).
CONST_FLOAT SPTT_UI_CR_HEAD_POS_Y		0.05			// UI choose race heading pos (y). (formally .15)
CONST_FLOAT SPTT_UI_CR_HEAD_SCALE		0.8				// UI choose race heading scale (x/y). (formally 1.0)
CONST_FLOAT SPTT_UI_CR_INFO_POS_X		0.325			// UI choose race info pos (x).
CONST_FLOAT SPTT_UI_CR_INFO_POS_Y		0.11			// UI choose race info pos (y). (formally .25)
CONST_FLOAT SPTT_UI_CR_INFO_SCALE		0.5				// UI choose race info scale (x/y).
CONST_FLOAT SPTT_UI_CR_INFO_SPC_X1		0.23			// UI choose race info space (x1).
CONST_FLOAT SPTT_UI_CR_INFO_SPC_X2		0.282			// UI choose race info space (x2).
CONST_FLOAT SPTT_UI_CR_INFO_SPC_Y		0.052			// UI choose race info space (y).
CONST_FLOAT SPTT_UI_CR_SEL_POS_X		0.32			// UI choose race select pos (x).
CONST_FLOAT SPTT_UI_CR_SEL_POS_Y		0.895			// UI choose race select pos (y). (formally .81)
CONST_FLOAT SPTT_UI_CR_SEL_SCALE		0.625			// UI choose race select scale (x/y).
CONST_FLOAT SPTT_UI_CR_ENT_POS_X		0.435			// UI choose race enter pos (x).
CONST_FLOAT SPTT_UI_CR_ENT_POS_Y		0.895			// UI choose race enter pos (y). (formally .81)
CONST_FLOAT SPTT_UI_CR_ENT_SCALE		0.625			// UI choose race enter scale (x/y).
CONST_FLOAT SPTT_UI_CR_EXIT_POS_X		0.614			// UI choose race exit pos (x).
CONST_FLOAT SPTT_UI_CR_EXIT_POS_Y		0.895			// UI choose race exit pos (y). (formally .81)
CONST_FLOAT SPTT_UI_CR_EXIT_SCALE		0.625			// UI choose race exit scale (x/y).
CONST_FLOAT SPTT_UI_CR_RECT_POS_X		0.5				// UI choose race rectangle pos (x).
CONST_FLOAT SPTT_UI_CR_RECT_POS_Y		0.3				// UI choose race rectangle pos (y). (formally .5)
CONST_FLOAT SPTT_UI_CR_RECT_WIDTH		0.385			// UI choose race rectange width.
CONST_FLOAT SPTT_UI_CR_RECT_HEIGHT		1.30			// UI choose race rectangle height.
CONST_INT 	SPTT_UI_CR_RECT_ALPHA		191				// UI choose race rectange alpha.

CONST_FLOAT SPTT_UI_CD_NUM_SCALE		2.0				// UI countdown number scale for both x/y.
CONST_FLOAT SPTT_UI_CD_NUM_POS_X		0.48			// UI countdown number start position (x).
CONST_FLOAT SPTT_UI_CD_NUM_POS_Y		0.4				// UI countdown number start position (y).
CONST_FLOAT SPTT_UI_CD_NUM_TIME			1.25			// UI countdown number display for time (s).
CONST_FLOAT SPTT_UI_CD_GO_SCALE			2.0				// UI countdown "GO!" scale for both x/y.
CONST_FLOAT SPTT_UI_CD_GO_POS_X			0.44			// UI countdown "GO!" start position (x).
CONST_FLOAT SPTT_UI_CD_GO_POS_Y			0.4				// UI countdown "GO!" start position (y).
CONST_FLOAT SPTT_UI_CD_GO_TIME			1.0				// UI countdown "GO!" display for time (s).

CONST_FLOAT SPTT_UI_FINISH_SCALE		2.0				// UI finish scale for both x/y.
CONST_FLOAT SPTT_UI_FINISH_POS_X		0.4				// UI finish start position (x).
CONST_FLOAT SPTT_UI_FINISH_POS_Y		0.4				// UI finish start position (y).
CONST_FLOAT SPTT_UI_FINISH_TIME			3.0				// UI finish display for time (s).

CONST_FLOAT SPTT_UI_LB_HEAD_POS_X		0.4				// UI leaderboard heading pos (x).
CONST_FLOAT SPTT_UI_LB_HEAD_POS_Y		0.15			// UI leaderboard heading pos (y).
CONST_FLOAT SPTT_UI_LB_HEAD_SCALE		1.0				// UI leaderboard heading scale (x/y).
CONST_FLOAT SPTT_UI_LB_INFO_POS_X		0.325			// UI leaderboard info pos (x).
CONST_FLOAT SPTT_UI_LB_INFO_POS_Y		0.21			// UI leaderboard info pos (y).
CONST_FLOAT SPTT_UI_LB_INFO_SCALE		0.5				// UI leaderboard info scale (x/y).
CONST_FLOAT SPTT_UI_LB_INFO_SPC_X1		0.23			// UI leaderboard info space (x1).
CONST_FLOAT SPTT_UI_LB_INFO_SPC_X2		0.282			// UI leaderboard info space (x2).
CONST_FLOAT SPTT_UI_LB_INFO_SPC_Y		0.052			// UI leaderboard info space (y).
CONST_FLOAT SPTT_UI_LB_CONT_POS_X		0.32			// UI leaderboard select pos (x).
CONST_FLOAT SPTT_UI_LB_CONT_POS_Y		0.81			// UI leaderboard select pos (y).
CONST_FLOAT SPTT_UI_LB_CONT_SCALE		0.625			// UI leaderboard select scale (x/y).
CONST_FLOAT SPTT_UI_LB_EXIT_POS_X		0.614			// UI leaderboard exit pos (x).
CONST_FLOAT SPTT_UI_LB_EXIT_POS_Y		0.81			// UI leaderboard exit pos (y).
CONST_FLOAT SPTT_UI_LB_EXIT_SCALE		0.625			// UI leaderboard exit scale (x/y).
CONST_FLOAT SPTT_UI_LB_RECT_POS_X		0.5				// UI leaderboard rectangle pos (x).
CONST_FLOAT SPTT_UI_LB_RECT_POS_Y		0.5				// UI leaderboard rectangle pos (y).
CONST_FLOAT SPTT_UI_LB_RECT_WIDTH		0.375			// UI leaderboard rectange width.
CONST_FLOAT SPTT_UI_LB_RECT_HEIGHT		0.750			// UI leaderboard rectangle height.
CONST_INT 	SPTT_UI_LB_RECT_ALPHA		191				// UI leaderboard rectange alpha.
CONST_INT 	SPTT_UI_LB_RACER_LIMIT		10				// UI leaderboard racer limit.

CONST_INT	SPTT_UI_TIME_THROTTLE		10				// UI throttle for updating the race time

CONST_INT	SPTT_INPUT_STICK_LIMIT		128				// INPUT stick limit for both x/y.
CONST_INT	SPTT_INPUT_STICK_DZ_X		32				// INPUT stick dead zone (x).
CONST_INT	SPTT_INPUT_STICK_DZ_Y		32				// INPUT stick dead zone (x).

CONST_FLOAT SPTT_CAM_INTERP_SPEED		100.0			// CAM interp speed (m/s).
CONST_INT 	SPTT_CAM_INTERP_TIME_MIN	2000			// CAM interp time max (ms).
CONST_INT 	SPTT_CAM_INTERP_TIME_MAX	6000			// CAM interp time max (ms).
CONST_FLOAT	SPTT_CAM_ROTATE_INC			2.0				// CAM rotate increment (deg).
CONST_FLOAT	SPTT_CAM_ROTATE_LIMIT		89.0			// CAM rotate limit (deg).

CONST_FLOAT	SPTT_CAM_ZOOM_INC			0.1				// CAM zoom increment (scalar).
CONST_FLOAT	SPTT_CAM_ZOOM_LIMIT_MIN		0.5				// CAM zoom min limit (scalar).
CONST_FLOAT	SPTT_CAM_ZOOM_LIMIT_MAX		5.0				// CAM zoom max limit (scalar).
CONST_FLOAT SPTT_CAM_GATE_FOCUS_DIST 	50.0			// CAM gate focus distance (m).
CONST_FLOAT SPTT_CAM_RACER_FOCUS_DIST 	10.0			// CAM racer focus distance (m).

CONST_FLOAT	SPTT_PLANE_SPD_MAX			60.0			// Plane speed max(m/s).
CONST_FLOAT	SPTT_TRICK_ROT_MIN			0.0				// Trick rotation min (deg).
CONST_FLOAT	SPTT_TRICK_ROT_MID			180.0			// Trick rotation mid (deg).
CONST_FLOAT	SPTT_TRICK_ROT_MAX			360.0			// Trick rotation max (deg).
CONST_FLOAT	SPTT_TRICK_ROT_JUMP			90.0			// Trick rotation jump (deg).
CONST_FLOAT	SPTT_TRICK_ROT_BUFF			30.0			// Trick rotation buffer (deg).
CONST_FLOAT	SPTT_TRICK_LOOP_MIN			1.0				// Trick loop bonus min (s).
CONST_FLOAT	SPTT_TRICK_LOOP_MAX			15.0			// Trick loop bonus max (s).
CONST_FLOAT	SPTT_TRICK_ROLL_MIN			0.2				// Trick roll bonus min (s).
CONST_FLOAT	SPTT_TRICK_ROLL_MAX			3.0				// Trick roll bonus max (s).
CONST_FLOAT	SPTT_TRICK_INVT_TIME		10.0			// Trick invert time (s).
CONST_FLOAT	SPTT_TRICK_INVT_MIN			0.4				// Trick invert bonus min (s).
CONST_FLOAT	SPTT_TRICK_INVT_MAX			6.0				// Trick invert bonus max (s).
CONST_FLOAT	SPTT_TRICK_DEGRADE			2.0				// Trick bonus degrade (^x).
CONST_INT	SPTT_TRICK_DISP_TIME		2500 			// Trick display time (ms).

CONST_FLOAT	SPTT_HINT_CAM_TIMER_THRESHOLD	1.0			// Threshold for going back from hint cam, like Alwyn's stuff does

CONST_FLOAT SPTT_MENU_CAM_FOV			34.9705
VECTOR		SPTT_MENU_CAM_COORD			= <<1691.2277, 3251.7849, 44.2574>>
VECTOR		SPTT_MENU_CAM_ROT 			= <<2.5652, -0.0000, -4.1481>>


// -----------------------------------
// ENUMERATIONS
// -----------------------------------

// SPR Main Run.
ENUM SPTT_MAIN_RUN_ENUM
	SPTT_MAIN_RUN_SETUP,
	SPTT_MAIN_RUN_UPDATE,
	SPTT_MAIN_RUN_CLEANUP
ENDENUM

// SPR Main Setup.
ENUM SPTT_MAIN_SETUP_ENUM
	SPTT_MAIN_SETUP_INIT,
	SPTT_MAIN_SETUP_FADE_OUT,
	SPTT_MAIN_SETUP_LOAD_INIT,
	SPTT_MAIN_SETUP_LOAD_WAIT,
	SPTT_MAIN_SETUP_CREATE_INIT,
	SPTT_MAIN_SETUP_CREATE_WAIT,
	SPTT_MAIN_SETUP_PLACE_INIT,
	SPTT_MAIN_SETUP_PLACE_WAIT,
	SPTT_MAIN_SETUP_FADE_IN,
	SPTT_MAIN_SETUP_WAIT,
	SPTT_MAIN_SETUP_CLEANUP
ENDENUM

// SPR Main Udpate.
ENUM SPTT_MAIN_UPDATE_ENUM
	SPTT_MAIN_UPDATE_INIT,
	SPTT_MAIN_UPDATE_GET_IN_INIT,
	SPTT_MAIN_UPDATE_GET_IN_WAIT,
	SPTT_MAIN_UPDATE_SETUP_MENU_CAM,
	SPTT_MAIN_UPDATE_CHOOSE_RACE,
	SPTT_MAIN_RESETTING_LOAD,
	SPTT_MAIN_UPDATE_ORCUT_LOAD,
	SPTT_MAIN_QUIT_FADE_TELEPORT,
	SPTT_MAIN_UPDATE_FADE_OUT,
	SPTT_MAIN_UPDATE_SPECIAL_CAM_PAN,
	SPTT_MAIN_UPDATE_SETUP_RACE_INIT,
	SPTT_MAIN_UPDATE_SETUP_RACE_WAIT,
	SPTT_MAIN_UPDATE_TAXI_INTRO,
	SPTT_MAIN_UPDATE_FADE_IN,
	SPTT_MAIN_UPDATE_WAIT,
	SPTT_MAIN_UPDATE_FADE_IN_TO_MENU,
	SPTT_MAIN_UPDATE_PRECLEANUP_DELAY,
	SPTT_MAIN_RESETTING_LOAD_FADE,
	SPTT_MAIN_UPDATE_CLEANUP
ENDENUM

// SPR Camera Mode.
ENUM SPTT_CAMERA_MODE_ENUM
	SPTT_CAMERA_MODE_GAME,
	SPTT_CAMERA_MODE_FREE,
	SPTT_CAMERA_MODE_FLY
ENDENUM

// SPR Camera Focus.
ENUM SPTT_CAMERA_FOCUS_ENUM
	SPTT_CAMERA_FOCUS_GATES,
	SPTT_CAMERA_FOCUS_RACERS
ENDENUM

// SPR Move Mode.
ENUM SPTT_MOVE_MODE_ENUM
	SPTT_MOVE_MODE_CAMERA,
	SPTT_MOVE_MODE_GROUND,
	SPTT_MOVE_MODE_PREV,
	SPTT_MOVE_MODE_NEXT,
	SPTT_MOVE_MODE_SELECT
ENDENUM

// SPR Racer Reset.
ENUM SPTT_RACER_RESET_ENUM
	SPTT_RACER_RESET_FAIL_OVER,
	SPTT_RACER_RESET_FAIL_EFFECTS,
	SPTT_RACER_RESET_FAIL_SELECT_RETRY,
	SPTT_RACER_RESET_FAIL_HINT,
	SPTT_RACER_RESET_INIT,
	SPTT_RACER_RESET_CHOOSE,
	SPTT_RACER_RESET_FADE_OUT,
	SPTT_RACER_QUIT_FADE_OUT,
	SPTT_RACER_RESET_CREATE,
	SPTT_RACER_RESET_PLACE,
	SPTT_RACER_QUIT_PLACE,
	SPTT_RACER_RESET_FADE_IN,
	SPTT_RACER_QUIT_FADE_IN,
	SPTT_RACER_RESET_WAIT,
	SPTT_RACER_QUIT_EXIT
ENDENUM

// SPR Race Type.
ENUM SPTT_RACE_TYPE_ENUM
	SPTT_RACE_TYPE_PLANE,
	SPTT_RACE_TYPE_OFFROAD,
	SPTT_RACE_TYPE_TRIATHLON
ENDENUM

// SPR Race Setup.
ENUM SPTT_RACE_SETUP_ENUM
	SPTT_RACE_SETUP_INIT,
	SPTT_RACE_SETUP_LOAD_INIT,
	SPTT_RACE_SETUP_LOAD_WAIT,
	SPTT_RACE_SETUP_CREATE_INIT,
	SPTT_RACE_SETUP_CREATE_WAIT,
	SPTT_RACE_SETUP_WAIT,
	SPTT_RACE_SETUP_CLEANUP
ENDENUM

// SPR Race Update.
ENUM SPTT_RACE_UPDATE_ENUM
	SPTT_RACE_UPDATE_INIT,
	SPTT_RACE_UPDATE_COUNTDOWN,
	SPTT_RACE_UPDATE_FINISH,
	SPTT_RACE_UPDATE_TAXI_IN,
	SPTT_RACE_UPDATE_LEADERBOARD,
	SPTT_RACE_UPDATE_FADE_OUT_BEFORE_FINISH,
	SPTT_RACE_UPDATE_FADE_IN_BEFORE_FINISH,
	SPTT_RACE_UPDATE_WAIT,
	SPTT_RACE_UPDATE_CLEANUP
ENDENUM

// SPR Race Gate stunt types
ENUM SPTT_RACE_STUNT_GATE_ENUM
	SPTT_RACE_STUNT_GATE_NORMAL,
	SPTT_RACE_STUNT_GATE_INVERTED, 
	SPTT_RACE_STUNT_GATE_SIDE_LEFT,
	SPTT_RACE_STUNT_GATE_SIDE_RIGHT
ENDENUM

//SPR Gate check types
ENUM SPTT_RACE_GATE_STATUS
	SPTT_RACE_GATE_STATUS_INVALID = -1,
	SPTT_RACE_GATE_STATUS_PASS,
	SPTT_RACE_GATE_STATUS_PASS_INNER,
	SPTT_RACE_GATE_STATUS_PASS_OUTTER,
	SPTT_RACE_GATE_STATUS_KNIFE_INNER,
	SPTT_RACE_GATE_STATUS_INVERT_INNER,
	SPTT_RACE_GATE_STATUS_KNIFE_OUTTER,
	SPTT_RACE_GATE_STATUS_INVERT_OUTTER,
	SPTT_RACE_GATE_STATUS_MISSED_GATE,
	SPTT_RACE_GATE_STATUS_STUNT_KL,			// left knife stunt gate
	SPTT_RACE_GATE_STATUS_STUNT_KL_HIT,		// left knife stunt gate hit, no stunt
	SPTT_RACE_GATE_STATUS_STUNT_KL_MISS,		// left knife stunt gate miss
	SPTT_RACE_GATE_STATUS_STUNT_KR,			// right knife stunt gate
	SPTT_RACE_GATE_STATUS_STUNT_KR_HIT,		// right knife stunt gate hit, no stunt
	SPTT_RACE_GATE_STATUS_STUNT_KR_MISS,		// right knife stunt gate miss
	SPTT_RACE_GATE_STATUS_STUNT_INV,			// inverted stunt gate
	SPTT_RACE_GATE_STATUS_STUNT_INV_HIT,		// inverted stunt gate hit, no stunt
	SPTT_RACE_GATE_STATUS_STUNT_INV_MISS,	// inverted stunt gate miss
	SPTT_RACE_GATE_STATUS_INCOMPLETE
ENDENUM

//SPR Gate check types
ENUM SPTT_RACE_CHECKPOINT_TYPE
	SPTT_CHKPT_OFFROAD_DEFAULT = 0,
	SPTT_CHKPT_OFFROAD_FINISH,
	SPTT_CHKPT_STUNT_DEFAULT,
	SPTT_CHKPT_STUNT_STUNT,
	SPTT_CHKPT_STUNT_FINISH,
	SPTT_CHKPT_TRI_SWIM,
	SPTT_CHKPT_TRI_BIKE,
	SPTT_CHKPT_TRI_RUN,
	SPTT_CHKPT_TRI_FINISH,
	SPTT_CHKPT_TRI_FIRST_TRANS,
	SPTT_CHKPT_TRI_SECOND_TRANS
ENDENUM

ENUM SPTT_RACE_GATE_FLAGS
	SPTT_RACE_GATE_FLAG_AP_IGNORE_GROUND 		= BIT0,
	SPTT_RACE_GATE_FLAG_CHECK_VEHICLE 			= BIT1,
	SPTT_RACE_GATE_FLAG_DO_NOT_SNAP_TO_GROUND 	= BIT2
ENDENUM

#IF SPTT_RACE_IS_TRIATHLON
// Determines how a Tri racer competes.
ENUM TRI_RACER_COMPETE_MODE
	TRI_RACER_COMPETE_MODE_DEFAULT,		
	TRI_RACER_COMPETE_MODE_TIRED,		
	TRI_RACER_COMPETE_MODE_AGGRESSIVE,	
	TRI_RACER_COMPETE_MODE_FORCED_TIRED			
ENDENUM
#ENDIF

ENUM SPTT_SC_SCREEN_TEXT
	SPTT_SC_MAIN_TITLE,
	SPTT_SC_SCORE_TITLE,
	SPTT_SC_AWARD_TXT,
	SPTT_SC_SCORE_TXT,
	SPTT_SC_HISCORE_TXT,
	SPTT_SC_TOTAL_TXT,
	SPTT_SC_WEAPON_TXT,
	SPTT_SC_FIRED_TXT,
	SPTT_SC_HITS_TXT,
	SPTT_SC_ACC_TXT,
	SPTT_SC_TIMEREM_TXT,
	SPTT_SC_AWARDVAL_TXT,
	SPTT_SC_SCOREVAL_TXT,
	SPTT_SC_HISCOREVAL_TXT,
	SPTT_SC_TOTALVAL_TXT,
	SPTT_SC_WEAPONVAL_TXT,
	SPTT_SC_BRONZE_GOAL_TXT,
	SPTT_SC_SILVER_GOAL_TXT,
	SPTT_SC_GOLD_GOAL_TXT
ENDENUM

ENUM SPTT_SC_SCREEN_SPRITE
	SPTT_SCREEN_MAIN_BACKGROUND,
	SPTT_SC_SOCIALCLUB_IMG,
	SPTT_SC_HITS_IMG,
	SPTT_SC_MEDAL_AWARD_IMG,
	SPTT_SC_BRONZE_IMG,
	SPTT_SC_SILVER_IMG,
	SPTT_SC_GOLD_IMG
ENDENUM

ENUM SPTT_SC_SCREEN_RECT
	SPTT_SC_SCORE_BG,
	SPTT_SC_SCORE_EDGE,
	SPTT_SC_HIT_BG,
	SPTT_SC_INFO1_BG,
	SPTT_SC_INFO2_BG,
	SPTT_SC_INFO3_BG,
	SPTT_SC_INFO4_BG,
	SPTT_SC_INFO5_BG,
	SPTT_SC_INFO6_BG,
	SPTT_SC_INFO7_BG,
	SPTT_SC_INFO8_BG,
	SPTT_SC_BRONZE_BG,
	SPTT_SC_SILVER_BG,
	SPTT_SC_GOLD_BG,
	SPTT_SC_BRONZE_OVERLAY,
	SPTT_SC_SILVER_OVERLAY,
	SPTT_SC_GOLD_OVERLAY
ENDENUM

// -----------------------------------
// STRUCTURES
// -----------------------------------

// SPR Main.
STRUCT SPTT_MAIN_STRUCT
	INT iSPRMode
	SPTT_MAIN_RUN_ENUM eRun
	StructTimer tSetup
	SPTT_MAIN_SETUP_ENUM eSetup
	SPTT_MAIN_UPDATE_ENUM eUpdate
	SCENARIO_BLOCKING_INDEX sbi01
ENDSTRUCT

// SPR Display.
STRUCT SPTT_DISPLAY_STRUCT	
	StructTimer tCnt
ENDSTRUCT

// SPR Input.
STRUCT SPTT_INPUT_STRUCT
	INT iLS_X, iLS_Y
	INT iRS_X, iRS_Y
ENDSTRUCT

// SPR Camera.
STRUCT SPTT_CAMERA_STRUCT
	CAMERA_INDEX Lst, Cur
	OBJECT_INDEX DummyCam
	FLOAT fZoom
	SPTT_CAMERA_MODE_ENUM eMode
	SPTT_CAMERA_FOCUS_ENUM eFocus
ENDSTRUCT

STRUCT SPTT_HINT_CAM_STRUCT
	BOOL bActive, bQuickTapActivates
//	ENTITY_INDEX Entity
	VECTOR Coord
	FLOAT fCamTimer
//	PS_HINT_CAM_ENUM HintType
	CAMERA_INDEX CustomCam
ENDSTRUCT

// SPR Gate.
STRUCT SPTT_GATE_STRUCT	
	//INT iPosX
	//INT iPosY
	VECTOR vPos
	FLOAT fRadius
	FLOAT fHeightCheck 		// To Fix Bug # 666112 - DS
	BLIP_INDEX Blip
	CHECKPOINT_INDEX Chkpnt
	SPTT_RACE_CHECKPOINT_TYPE eChkpntType
	SPTT_RACE_STUNT_GATE_ENUM eStuntType
	INT iGateFlags //auto pilot should ignore ground check (185332)	
ENDSTRUCT

// SPR Racer.
STRUCT SPTT_RACER_STRUCT
	TEXT_LABEL_31 szName
	PED_INDEX Driver
	VEHICLE_INDEX Vehicle
	BLIP_INDEX Blip
	INT iGateCur, iRank
	FLOAT fClockTime	
	FLOAT fPlsMnsLst
	FLOAT fPlsMnsTot
	VECTOR vStartPos
	FLOAT fStartHead
	PED_TYPE eDriverType
	MODEL_NAMES eDriverModel
	MODEL_NAMES eVehicleModel
	SPTT_RACER_RESET_ENUM eReset
	structPedsForConversation pedConvo
	
	#IF SPTT_RACE_IS_TRIATHLON
		FLOAT fMinMoveBlendRatioOnFoot, 	fMaxMoveBlendRatioOnFoot		
		FLOAT fMinMoveBlendRatioInWater, 	fMaxMoveBlendRatioInWater
		FLOAT fMinBikeSpeed, 				fMaxBikeSpeed
		
		INT iSkillPlacement
		
		TRI_RACER_COMPETE_MODE eCompeteMode
		
		structTimer timerInCurrentCompeteMode
		structTimer timerRacerVehicleIdle
		
		BOOL bHasBeenOnABike
		
		STRING szCurrentBikeRecordingName	// Necessary for Ironman, to switch a racer's waypoint vehicle recoring.
	#ENDIF
	
ENDSTRUCT

// SPR Race.
STRUCT SPTT_RACE_STRUCT	
	BOOL bRestarting, bFailChecking, bDidWeRestart
	StructTimer tClock, tStreamTimeLimit
	FLOAT fBestClockTime
	FLOAT fBestSplitTime
	INT iGateCheck
	INT iGateCnt, iRacerCnt		
	SPTT_DISPLAY_STRUCT Display
	SCRIPT_SCALEFORM_BIG_MESSAGE bigMessageUI
	StructTimer tUpdate
	SPTT_RACE_SETUP_ENUM eSetup
	SPTT_RACE_UPDATE_ENUM eUpdate	
	SPTT_GATE_STRUCT sGate[SPTT_GATE_MAX]
	SPTT_RACER_STRUCT Racer[SPTT_RACER_MAX]
	MEGA_PLACEMENT_TOOLS uiScorecard
	CAMERA_INDEX menuCam
	BOOL bLBToggle, bLBViewProfile
	#IF IS_DEBUG_BUILD
		BOOL bPlayerUsedDebugSkip = FALSE
	#ENDIF
	CHECKPOINT_INDEX fadeCheckpoint
	INT iFadeAlpha
ENDSTRUCT

#IF IS_DEBUG_BUILD

// SPR Widget toggle.
STRUCT SPTT_WIDGET_TOGGLE
	BOOL bCur, bLst
ENDSTRUCT

// SPR Widget button.
STRUCT SPTT_WIDGET_BUTTON
	BOOL bCur
ENDSTRUCT

// SPR Widget int.
STRUCT SPTT_WIDGET_INT
	INT iCur, iLst
ENDSTRUCT

// SPR Widget float.
STRUCT SPTT_WIDGET_FLOAT
	FLOAT fCur, fLst
ENDSTRUCT

// SPR Widget vector.
STRUCT SPTT_WIDGET_VECTOR
	VECTOR vCur, vLst
ENDSTRUCT

// SPR Widget Textbox.
STRUCT SPTT_WIDGET_TEXTBOX
	TEXT_WIDGET_ID WidgetID
	TEXT_LABEL_31 szCur, szLst
ENDSTRUCT

// SPR Widget Listbox.
STRUCT SPTT_WIDGET_LISTBOX
	INT iCur, iLst
ENDSTRUCT

// SPR Widget Gates.
STRUCT SPTT_WIDGET_GATES
	WIDGET_GROUP_ID GroupID
	SPTT_WIDGET_BUTTON Create
	SPTT_WIDGET_BUTTON Delete
	SPTT_WIDGET_TOGGLE ShowAll
	SPTT_WIDGET_BUTTON SnapSelected
	SPTT_WIDGET_INT Selection
	SPTT_WIDGET_VECTOR Position
	SPTT_WIDGET_FLOAT Radius
	SPTT_WIDGET_LISTBOX ChkpntType
	SPTT_WIDGET_LISTBOX StuntType
	SPTT_WIDGET_LISTBOX RelMode
	SPTT_WIDGET_TOGGLE RelAbs
	SPTT_WIDGET_VECTOR RelMove
ENDSTRUCT

// SPR Widget Racers.
STRUCT SPTT_WIDGET_RACERS
	WIDGET_GROUP_ID GroupID
	SPTT_WIDGET_BUTTON Create
	SPTT_WIDGET_BUTTON Delete
	SPTT_WIDGET_TOGGLE ShowAll
	SPTT_WIDGET_INT Selection
	SPTT_WIDGET_TEXTBOX Name
	SPTT_WIDGET_VECTOR Position
	SPTT_WIDGET_FLOAT Heading
	SPTT_WIDGET_LISTBOX DriverType
	SPTT_WIDGET_LISTBOX DriverModel
	SPTT_WIDGET_LISTBOX VehicleModel
	SPTT_WIDGET_LISTBOX RelMode
	SPTT_WIDGET_TOGGLE RelAbs
	SPTT_WIDGET_VECTOR RelMove
	BOOL bRacerQueue[SPTT_RACER_MAX]
ENDSTRUCT

// SPR Widget Race.
STRUCT SPTT_WIDGET_RACE
	WIDGET_GROUP_ID GroupID
	SPTT_WIDGET_TEXTBOX Error
	//SPTT_WIDGET_FLOAT Time
	SPTT_WIDGET_TEXTBOX Name
	SPTT_WIDGET_TEXTBOX FileName
	SPTT_WIDGET_BUTTON Create
	SPTT_WIDGET_LISTBOX Pick
	SPTT_WIDGET_BUTTON Load
	SPTT_WIDGET_BUTTON Save
	SPTT_WIDGET_BUTTON Export
	SPTT_WIDGET_GATES Gates
	SPTT_WIDGET_RACERS Racers
	SPTT_RACE_STRUCT Race
ENDSTRUCT

// SPR Widget Race Mode.
STRUCT SPTT_WIDGET_RACEMODE
	WIDGET_GROUP_ID GroupID
ENDSTRUCT

// SPR Widget Edit Mode.
STRUCT SPTT_WIDGET_EDITMODE
	WIDGET_GROUP_ID GroupID
	SPTT_WIDGET_LISTBOX CamMode
	SPTT_WIDGET_LISTBOX CamFocus
	SPTT_WIDGET_RACE Race
	SPTT_INPUT_STRUCT Input
	SPTT_CAMERA_STRUCT Camera
ENDSTRUCT

// SPR Widget Main.
STRUCT SPTT_WIDGET_MAIN
	WIDGET_GROUP_ID GroupID
	SPTT_WIDGET_LISTBOX SPRMode
	SPTT_WIDGET_RACEMODE RaceMode
	SPTT_WIDGET_EDITMODE EditMode
ENDSTRUCT

#ENDIF	//	IS_DEBUG_BUILD

// SPR Master.
STRUCT SPTT_MASTER_STRUCT
	SPTT_RACE_TYPE_ENUM eRaceType
	VEHICLE_INDEX PlayerVeh	
	SPTT_RACE_CHECKPOINT_TYPE eDefChkPntType
	VECTOR vDefRcrPos
	FLOAT fDefRcrHead
	PED_TYPE eDefDrvType
	MODEL_NAMES eDefDrvModel
	MODEL_NAMES eDefVehModel
	INT iRaceCur, iRaceCnt
	FLOAT fRaceTime[NUMBER_OF_SPTT_COURSES], fLastTime[NUMBER_OF_SPTT_COURSES]
	TEXT_LABEL_31 szRaceName[NUMBER_OF_SPTT_COURSES]
	TEXT_LABEL_31 szRaceFileName[NUMBER_OF_SPTT_COURSES]
	FLOAT fTimeGold[NUMBER_OF_SPTT_COURSES]
	FLOAT fTimeBronze[NUMBER_OF_SPTT_COURSES]
	SCALEFORM_INDEX uiLeaderboard
	SIMPLE_USE_CONTEXT uiInput
	VEHICLE_SETUP_STRUCT vssPlaneSetup
	MG_FAIL_FADE_EFFECT failFadeEffect
	MG_FAIL_SPLASH failSplash
	//SCRIPT_SCALEFORM_UI uiInput
ENDSTRUCT


// -----------------------------------
// VARIABLES
// -----------------------------------

// SPR Master Data (DO NOT MOVE).
SPTT_MASTER_STRUCT SPTT_Master


// -----------------------------------
// PROCS/FUNCTIONS
// -----------------------------------

