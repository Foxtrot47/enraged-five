// Range_Round_lib.sch
USING "Range_Round.sch"
USING "Range_Target.sch"
USING "Range_UI.sch"
USINg "Range_Core_lib.sch"
USING "Range_Range_lib.sch"
USING "minigame_uiinputs.sch"
USING "script_usecontext.sch"
USING "range_public.sch"

PROC INIT_WEAPON_ROUNDS(Range_WeaponRounds & sWeaponRoundData[])
	// Pistol	
	// Challenges
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_1].eRndType 		= RT_PISTOL_CHAL_1
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_1].eUnlockReq 	= RT_INVALID
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_1].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_PISTOL_CHAL_1, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_1].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_PISTOL_CHAL_1, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_1].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_PISTOL_CHAL_1, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_1].eScoringStyle 	= ROUNDSCORE_BULLSEYE
	
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_2].eRndType 		= RT_PISTOL_CHAL_2
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_2].eUnlockReq 	= RT_PISTOL_CHAL_1
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_2].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_PISTOL_CHAL_2, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_2].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_PISTOL_CHAL_2, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_2].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_PISTOL_CHAL_2, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_2].eScoringStyle 	= ROUNDSCORE_BULLSEYE
	
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_3].eRndType 		= RT_PISTOL_CHAL_3
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_3].eUnlockReq 	= RT_PISTOL_CHAL_2
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_3].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_PISTOL_CHAL_3, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_3].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_PISTOL_CHAL_3, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_3].iGoldReq 		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_PISTOL_CHAL_3, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_PISTOL].sChallenges[WEAPCHAL_3].eScoringStyle 	= ROUNDSCORE_BULLSEYE	
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "InitWeaponRounds -- Pistol Initialized.")
	
	
	// SMG
	// Challenges
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_1].eRndType 			= RT_SMG_CHAL_1
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_1].eUnlockReq 		= RT_PISTOL_CHAL_1
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_1].iBronzeReq 		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SMG_CHAL_1, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_1].iSilverReq 		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SMG_CHAL_1, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_1].iGoldReq   		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SMG_CHAL_1, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_1].eScoringStyle 	= ROUNDSCORE_BULLSEYE
	
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_2].eRndType 			= RT_SMG_CHAL_2
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_2].eUnlockReq 		= RT_SMG_CHAL_1
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_2].iBronzeReq 		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SMG_CHAL_2, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_2].iSilverReq 		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SMG_CHAL_2, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_2].iGoldReq   		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SMG_CHAL_2, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_2].eScoringStyle 	= ROUNDSCORE_BULLSEYE
	
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_3].eRndType 			= RT_SMG_CHAL_3
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_3].eUnlockReq 		= RT_SMG_CHAL_2
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_3].iBronzeReq 		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SMG_CHAL_3, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_3].iSilverReq 		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SMG_CHAL_3, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_3].iGoldReq   		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SMG_CHAL_3, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_SMG].sChallenges[WEAPCHAL_3].eScoringStyle 	= ROUNDSCORE_BULLSEYE
		
	CDEBUG2LN(DEBUG_SHOOTRANGE, "InitWeaponRounds -- SMG Initialized.")
	
	// Assault Rifle
	// Challenges
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_1].eRndType 		= RT_AR_CHAL_1
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_1].eUnlockReq 	= RT_SMG_CHAL_1
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_1].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_AR_CHAL_1, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_1].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_AR_CHAL_1, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_1].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_AR_CHAL_1, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_1].eScoringStyle 	= ROUNDSCORE_BULLSEYE

	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_2].eRndType 		= RT_AR_CHAL_2
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_2].eUnlockReq 	= RT_AR_CHAL_1
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_2].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_AR_CHAL_2, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_2].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_AR_CHAL_2, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_2].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_AR_CHAL_2, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_1].eScoringStyle 	= ROUNDSCORE_BULLSEYE

	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_3].eRndType 		= RT_AR_CHAL_3
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_3].eUnlockReq 	= RT_AR_CHAL_2
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_3].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_AR_CHAL_3, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_3].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_AR_CHAL_3, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_3].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_AR_CHAL_3, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_AR].sChallenges[WEAPCHAL_3].eScoringStyle 	= ROUNDSCORE_BULLSEYE
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "InitWeaponRounds -- RIFLE Initialized.")
	
	// Shotgun
	// Challenges
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_1].eRndType 		= RT_SHOTGUN_CHAL_1
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_1].eUnlockReq 	= RT_AR_CHAL_1
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_1].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SHOTGUN_CHAL_1, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_1].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SHOTGUN_CHAL_1, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_1].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SHOTGUN_CHAL_1, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_1].eScoringStyle = ROUNDSCORE_DESTRUCTION_W_BONUS

	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_2].eRndType 		= RT_SHOTGUN_CHAL_2
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_2].eUnlockReq 	= RT_SHOTGUN_CHAL_1
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_2].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SHOTGUN_CHAL_2, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_2].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SHOTGUN_CHAL_2, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_2].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SHOTGUN_CHAL_2, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_2].eScoringStyle = ROUNDSCORE_RANGE_REVERSE

	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_3].eRndType 		= RT_SHOTGUN_CHAL_3
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_3].eUnlockReq 	= RT_SHOTGUN_CHAL_2
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_3].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SHOTGUN_CHAL_3, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_3].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SHOTGUN_CHAL_3, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_3].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_SHOTGUN_CHAL_3, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_SHOTGUN].sChallenges[WEAPCHAL_3].eScoringStyle = ROUNDSCORE_DESTRUCTION_W_BONUS
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "InitWeaponRounds -- Shotgun Initialized.")
	
	// Heavy MG
	// Challenges
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_1].eRndType 		= RT_MINIGUN_CHAL_1
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_1].eUnlockReq 	= RT_SHOTGUN_CHAL_1
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_1].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_MINIGUN_CHAL_1, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_1].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_MINIGUN_CHAL_1, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_1].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_MINIGUN_CHAL_1, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_1].eScoringStyle = ROUNDSCORE_MINIGUN
	
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_2].eRndType 		= RT_MINIGUN_CHAL_2
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_2].eUnlockReq 	= RT_MINIGUN_CHAL_1
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_2].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_MINIGUN_CHAL_2, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_2].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_MINIGUN_CHAL_2, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_2].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_MINIGUN_CHAL_2, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_2].eScoringStyle = ROUNDSCORE_MINIGUN
	
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_3].eRndType 		= RT_MINIGUN_CHAL_3
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_3].eUnlockReq 	= RT_MINIGUN_CHAL_2
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_3].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_MINIGUN_CHAL_3, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_3].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_MINIGUN_CHAL_3, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_3].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_MINIGUN_CHAL_3, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_MINIGUN].sChallenges[WEAPCHAL_3].eScoringStyle = ROUNDSCORE_MINIGUN
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "InitWeaponRounds -- Minigun Initialized.")
	
	// LMG
	// Challenges
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_1].eRndType 	= RT_LMG_CHAL_1
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_1].eUnlockReq 	= RT_SHOTGUN_CHAL_1
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_1].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_LMG_CHAL_1, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_1].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_LMG_CHAL_1, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_1].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_LMG_CHAL_1, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_1].eScoringStyle 	= ROUNDSCORE_BULLSEYE
	
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_2].eRndType 	= RT_LMG_CHAL_2
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_2].eUnlockReq 	= RT_LMG_CHAL_1
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_2].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_LMG_CHAL_2, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_2].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_LMG_CHAL_2, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_2].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_LMG_CHAL_2, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_2].eScoringStyle 	= ROUNDSCORE_BULLSEYE
	
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_3].eRndType 	= RT_LMG_CHAL_3
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_3].eUnlockReq 	= RT_LMG_CHAL_2
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_3].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_LMG_CHAL_3, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_3].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_LMG_CHAL_3, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_3].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_LMG_CHAL_3, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_LIGHT_MG].sChallenges[WEAPCHAL_3].eScoringStyle 	= ROUNDSCORE_BULLSEYE
	CDEBUG2LN(DEBUG_SHOOTRANGE, "InitWeaponRounds -- LMG Initialized.")
	
	
	//NG Current gen owners only
	// Bonus NG to CG Challenges
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_1].eRndType 		= RT_RAILGUN_CHAL_1
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_1].eUnlockReq 	= RT_SHOTGUN_CHAL_1
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_1].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_1, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_1].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_1, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_1].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_1, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_1].eScoringStyle = ROUNDSCORE_DESTRUCTION_W_BONUS
	
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_2].eRndType 		= RT_RAILGUN_CHAL_2
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_2].eUnlockReq 	= RT_RAILGUN_CHAL_1
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_2].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_2, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_2].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_2, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_2].iGoldReq   	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_2, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_2].eScoringStyle = ROUNDSCORE_DESTRUCTION_W_BONUS
	
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_3].eRndType 		= RT_RAILGUN_CHAL_3
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_3].eUnlockReq 	= RT_RAILGUN_CHAL_2
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_3].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_3, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_3].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_3, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_3].iGoldReq 		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_3, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_3].eScoringStyle = ROUNDSCORE_DESTRUCTION_W_BONUS
	
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_4].eRndType 		= RT_RAILGUN_CHAL_4
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_4].eUnlockReq 	= RT_RAILGUN_CHAL_3
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_4].iBronzeReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_4, RRM_BRONZE)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_4].iSilverReq 	= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_4, RRM_SILVER)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_4].iGoldReq 		= RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RT_RAILGUN_CHAL_4, RRM_GOLD)
	sWeaponRoundData[WEAPCAT_RAILGUN].sChallenges[WEAPCHAL_4].eScoringStyle = ROUNDSCORE_DESTRUCTION_W_BONUS
	
ENDPROC


/// PURPOSE:
///    Makes sure everythign we want to be unlocked is unlocked (also sets the defaults to unlocked)
PROC INIT_ROUND_UNLOCKS(Range_WeaponRounds & sWeapRnds[])
	INT iter_0, iter_1, iChallActive
	
	// Lets set what weapons and challenges are unlocked
	REPEAT NUM_WEAPON_CATS iter_0
		iChallActive = 0
		
		REPEAT NUM_WEAPON_CHALLS iter_1
			// Did we pass our challanges requirement?
			// OR are we the first challenge and I have the weapon?
			// OR are we unlocked by some other means and havent passed our req
			IF IS_ROUND_CHALLENGE_REQ_MET(sWeapRnds[iter_0].sChallenges[iter_1].eUnlockReq)
					OR (iter_1 = 0 )
					OR IS_ROUND_UNLOCKED(sWeapRnds[iter_0].sChallenges[iter_1].eRndType)
				SET_ROUND_UNLOCKED(sWeapRnds[iter_0].sChallenges[iter_1].eRndType)
				iChallActive++
			ENDIF
		ENDREPEAT		
		
		// Check if the weapon is unlocked.. multiple ways
		// Does the player have the weapon or do we atleast have one challenge active
		IF iChallActive > 0
		//	sWeapRnds[iter_0].bUnlocked = TRUE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "InitWeaponRndData -- Weapon #", iter_0, " is unlocked (has more then one challenge)")
		ELSE
		//	sWeapRnds[iter_0].bUnlocked = FALSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "InitWeaponRndData -- Weapon #", iter_0, " is unlocked (is Locked less then 1 chall active)")
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Standard things we need to do after a round before we start a new one
PROC ROUND_CLEANUP(GRID_LOCATION & sGridData[], Range_RoundInfo & sCurRndInfo, Range_Targets & sTargetData, SPT_OBJECT & sSPTQueue[],
			Range_WeaponRounds & sWeapRndInfo[], Range_MenuData & sMenuInfo, BOOL bTutorial = FALSE, BOOL bForceLeaderboard = FALSE)
	// Restore the players ammo state, even in tutorial.
	WEAPON_TYPE weapType = GET_WEAPONTYPE_FROM_RANGE_INFO(sCurRndInfo.eWeaponCat, sCurRndInfo.eWeaponType)
	IF NOT IS_ROUND_FLAG_SET(sCurRndInfo, ROUND_PLAYER_OWNS_WEAPON)
		SET_AMMO_IN_CLIP(rangePlayerPed, weapType, 0)	
		REMOVE_WEAPON_FROM_PED(rangePlayerPed, weapType)
		SET_PED_AMMO(rangePlayerPed, weapType, 0)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "ROUND_CLEANUP -- -Removing that round's weapon from ped!")
	ELSE
		// Give us back our ammo
		SET_AMMO_IN_CLIP(rangePlayerPed, weapType, 0)	
		SET_PED_AMMO(rangePlayerPed, weapType, sCurRndInfo.iStoredAmmoCount)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "ROUND_CLEANUP -- Player's Ammo Restored: ", GET_AMMO_IN_PED_WEAPON(rangePlayerPed, weapType))
	ENDIF
	
	// Handle the attachments!
	IF IS_ROUND_FLAG_SET(sCurRndInfo, ROUND_PLAYER_OWNS_HG_FLASHLIGHT)
		GIVE_WEAPON_COMPONENT_TO_PED(rangePlayerPed, weapType, WEAPONCOMPONENT_AT_PI_FLSH)
	ELIF IS_ROUND_FLAG_SET(sCurRndInfo, ROUND_PLAYER_OWNS_AR_FLASHLIGHT)
		GIVE_WEAPON_COMPONENT_TO_PED(rangePlayerPed, weapType, WEAPONCOMPONENT_AT_AR_FLSH)
	ENDIF

	CLEAR_ROUND_FLAG(sCurRndInfo, ROUND_PLAYER_OWNS_AR_FLASHLIGHT)
	CLEAR_ROUND_FLAG(sCurRndInfo, ROUND_PLAYER_OWNS_HG_FLASHLIGHT)
	CLEAR_ROUND_FLAG(sCurRndInfo, ROUND_PLAYER_OWNS_WEAPON)
	
	IF NOT bTutorial
		// Only give leaderboard items if we're not online.
		SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, DEFAULT, (!IS_PLAYER_ONLINE() OR bForceLeaderboard))
	ENDIF
	
	// Destroy all targets
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Round_Cleanup -- Set all grids to not occupied.")
	INT iTer
	REPEAT MAX_GRID_LOCS iTer
		sGridData[iTer].bOccupied = FALSE
	ENDREPEAT
	
	// Clear all of our actions
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Round_Cleanup -- Clear all actions.")
	REPEAT iMAX_ACTIONS iTer
		sTargetData.sQueuedActions[iTer].iTargetIndex	= -1
		sTargetData.sQueuedActions[iTer].eBehavior		= AB_INVALID
		sTargetData.sQueuedActions[iTer].fTime 		= 0.0 	
		sTargetData.sQueuedActions[iTer].bPerformed	= FALSE
	ENDREPEAT
	
	// Clear up all the entities
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Round_Cleanup -- Clean all entities.")
	REPEAT iMAX_ENTITIES iTer
		sTargetData.sTargetEntities[iTer].eGridIndex 		= INVALID_LOC
		sTargetData.sTargetEntities[iTer].iCreateNumber		= -99
		sTargetData.sTargetEntities[iTer].iStatus			= 0
		sTargetData.sTargetEntities[iTer].fTargCreateTime 	= -1.0
		sTargetData.sTargetEntities[iTer].fRespawnAtTime 	= -1.0
		sTargetData.sTargetEntities[iTer].fFlipFrameTime	= 0
		sTargetData.sTargetEntities[iTer].fRotFrameTime		= 0
		DESTROY_TARGET_ENTITY(sTargetData.sTargetEntities[iTer], sGridData)	
	ENDREPEAT
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Round_Cleanup -- Clean SPT")
	REPEAT MAX_NUM_SPT iTer
		sSPTQueue[iTer].bActive = FALSE
		sSPTQueue[iTer].fFrameTime = -1
	ENDREPEAT
	
	// Reset target variables.
	sTargetData.iActionIndex = 0
	
	// Reset score variables.
	sCurRndInfo.sScoreData.iP1Score = 0
	sCurRndInfo.sScoreData.iTimeBonus = 0
	
	// Reset MP variables.
	sCurRndInfo.sMPData.iHighestMP	= 1
	sCurRndInfo.sMPData.bIncrementMP = FALSE
	RESET_MULTIPLIER(sCurRndInfo.sMPData)
	
	// Reset player round variables.
	CLEAR_ROUND_FLAG(sCurRndInfo, ROUND_TIME_IS_UP)
	CLEAR_ROUND_FLAG(sCurRndInfo, ROUND_USE_METAL_TARGETS)
	CLEAR_ROUND_FLAG(sCurRndInfo, ROUND_USE_BACK_RANGE)
	CLEAR_ROUND_FLAG(sCurRndInfo, ROUND_USE_FRAGGABLE_TARGETS)
	sCurRndInfo.iTargetsDestroyed = 0
	sCurRndInfo.iStoredAmmoCount = -1
	CANCEL_TIMER(sCurRndInfo.sTimeLeft)
	
	sCurRndInfo.iZ1Hits = 0
	sCurRndInfo.iZ2Hits = 0
	sCurRndInfo.iZ3Hits = 0
	sCurRndInfo.iZ4Hits = 0
	sCurRndInfo.fTotalTime = 0
	sCurRndInfo.fStartTime = 0.0
	sCurRndInfo.fNoAmmoStamp = 0
	sCurRndInfo.iTotalShots = 0
	sCurRndInfo.fTotalPauseTime = 0
	RESTART_TIMER_AT(sCurRndInfo.sGenTimer, 0.0)
	
	sCurRndInfo.sGraceInfo.bGraceUsed = FALSE
	CANCEL_TIMER(sCurRndInfo.sGraceInfo.tGraceResetTimer)
	
	CLEAR_ALL_FLOATING_HELP()
	
	// Set all of our new unlocks
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Round cleannup going to init round unlocks now...")
	INIT_ROUND_UNLOCKS(sWeapRndInfo)
ENDPROC


/// PURPOSE:
///    Figures out what the time bonus will be.
PROC ROUND_CALCULATE_TIME_BONUS(Range_RoundInfo & sRndInfo)
	sRndInfo.sScoreData.iTimeBonus = 0
	
	// Time taken
	IF NOT IS_ROUND_FLAG_SET(sRndInfo, ROUND_TIME_IS_UP)
	AND NOT IS_ROUND_FLAG_SET(sRndInfo, ROUND_NO_TIME_BONUS)
		// How much did we have left over?
		FLOAT fLeftOverTime = sRndInfo.fCurrentRoundLength - GET_TIMER_IN_SECONDS(sRndInfo.sTimeLeft)
		
		// Get our values
		INT iTotal = ROUND(fLeftOverTime * 1000.0)
		INT iMinutes = (iTotal / 1000) / 60
		INT iSeconds = (iTotal - (iMinutes * 60 * 1000)) / 1000
		INT iMilliSecs = (iTotal - ((iSeconds + (iMinutes * 60)) * 1000))
		
		// 10 points per second left
		sRndInfo.sScoreData.iTimeBonus = iSeconds * 10
		sRndInfo.sScoreData.iTimeBonus += (iMilliSecs / 100)
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "GiveTimeBonus -- Left over time: ", fLeftOverTime)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "GiveTimeBonus -- Seconds Reward: ", iSeconds * 10)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "GiveTimeBonus -- MiliSec Reward: ", iMilliSecs / 100)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "GiveTimeBonus -- Bonus Given: ", sRndInfo.sScoreData.iTimeBonus)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "GiveTimeBonus -- Total points: ", sRndInfo.sScoreData.iP1Score)
	ENDIF
ENDPROC


/// PURPOSE:
///    Add on the time bonus on to the players score
PROC ROUND_GIVE_TIME_BONUS(Range_RoundInfo & sCurRndInfo)
	// Time taken
	IF IS_ROUND_FLAG_SET(sCurRndInfo, ROUND_TIME_IS_UP)
		EXIT
	ELIF NOT IS_ROUND_FLAG_SET(sCurRndInfo, ROUND_NO_TIME_BONUS)
		// Increase our score
		sCurRndInfo.sScoreData.iP1Score += sCurRndInfo.sScoreData.iTimeBonus	
	ENDIF
ENDPROC


/// PURPOSE:
///    All the misc things that need to be done
///    after we pick our weapon, but before the countdown
PROC PRE_START_ROUND(Range_CoreData & sCore, Range_RoundInfo & sRndInfo, Range_RangeData & sRangeInfo)		
	// Back to the opening shot
	IF NOT (sCore.bSawIntro)
		RENDER_SCRIPT_CAMS(FALSE, TRUE)
	ENDIF
	
	// Begin the round
	WEAPON_TYPE toGive = GET_WEAPONTYPE_FROM_RANGE_INFO(sRndInfo.eWeaponCat, sRndInfo.eWeaponType)
	SETUP_PLAYER_WEAPONS(toGive, sRndInfo)
	
	// Set up our coords
	INIT_RANGE_GRID(sRangeInfo, sRndInfo)
	
	// Restart our countdown timer
	RESTART_TIMER_NOW(sRndInfo.sGenTimer)
	
	// Clear the bitmask to play sounds at the end of the round
	CLEAR_BITMASK_AS_ENUM(sRndInfo.iRoundFlags, ROUND_PASS_NOISE_PLAYED)
	
	// Give him the pistol and only allow him to use it
	SET_PED_CAN_SWITCH_WEAPON(rangePlayerPed, FALSE)
	
	// Force the player to focus downrange.
	IF ABSF(GET_ENTITY_HEADING(PLAYER_PED_ID()) - sRangeInfo.fPlayerRootHead) > 20.0
		SET_ENTITY_HEADING(PLAYER_PED_ID(), sRangeInfo.fPlayerRootHead)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	ENDIF
ENDPROC


/// PURPOSE:
///    All the different things that could end the round
/// RETURNS:
///    TRUE if the round is over
FUNC BOOL IS_ROUND_OVER(Range_RoundInfo & sCurRndInfo, TARGET_ENTITY & sTargInfo[])
	#IF IS_DEBUG_BUILD
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "End Currently Running Range Round")
		
			// Uncomment to fake a pass when J skipping.
			//sCurRndInfo.sScoreData.iP1Score = 30000
			
			RESTART_TIMER_AT(sCurRndInfo.sTimeLeft, sCurRndInfo.fCurrentRoundLength)
		ENDIF
	#ENDIF
	
	// Is time up?
	IF IS_ROUND_FLAG_SET(sCurRndInfo, ROUND_TIME_IS_UP)
		STOP_SOUND(sCurRndInfo.iSFX_Warning)
		//PLAY_RANGE_SOUND("ROUND_OVER")
		PLAY_RANGE_SOUND("TIMER_STOP")
		RETURN TRUE
	ENDIF
	
	// Have we destroyed all the targets? -- Only valid if this isn't an infinite round. 
	IF NOT IS_BITMASK_AS_ENUM_SET(sCurRndInfo.iRoundFlags, ROUND_TARGETS_ARE_INFINITE) AND NOT
			IS_BITMASK_AS_ENUM_SET(sCurRndInfo.iRoundFlags, ROUND_ALLOW_TIME_EXPIRY)
		IF (sCurRndInfo.iTargetsDestroyed >= sCurRndInfo.iMaxTargets) 
			PAUSE_TIMER(sCurRndInfo.sTimeLeft)
			IF NOT ARE_ANY_TARGET_ENTITIES_MOVING(sCurRndInfo, sTargInfo)
				STOP_SOUND(sCurRndInfo.iSFX_Warning)
				//PLAY_RANGE_SOUND("ROUND_OVER")
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Did we run out of bullets with our gun type?
	WEAPON_TYPE weapType = GET_WEAPONTYPE_FROM_RANGE_INFO(sCurRndInfo.eWeaponCat, sCurRndInfo.eWeaponType)
	IF GET_AMMO_IN_PED_WEAPON(rangePlayerPed, weapType) <= 0
		IF (sCurRndInfo.fNoAmmoStamp <= 0.0)
			sCurRndInfo.fNoAmmoStamp = GET_GAME_TIMER() / 1000.0
			PAUSE_TIMER(sCurRndInfo.sTimeLeft)
		ELIF ((GET_GAME_TIMER() / 1000.0) - sCurRndInfo.fNoAmmoStamp) >= 1.0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    All of our Round timer logic can be found here
PROC UPDATE_ROUND_TIME_LEFT(Range_RoundInfo & sRndInfo)
	// If our time isnt up, go ahead and start the timer
	IF NOT IS_ROUND_FLAG_SET(sRndInfo, ROUND_TIME_IS_UP)
		IF NOT IS_TIMER_STARTED(sRndInfo.sTimeLeft)
			START_TIMER_AT(sRndInfo.sTimeLeft, 0.0)	
			// Store our time that we began
			sRndInfo.fStartTime = (GET_GAME_TIMER() / 1000.0)
		ENDIF
		
		IF NOT IS_ROUND_FLAG_SET(sRndInfo, ROUND_PLAYED_10_SEC_WARNING)
			FLOAT fTimer = GET_TIMER_IN_SECONDS(sRndInfo.sTimeLeft)
			IF (sRndInfo.fCurrentRoundLength - fTimer) < 1.0
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 1.5
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 2.0
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 2.5
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 3.0
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 3.5
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 4.0
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 4.5
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 5.0
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 6.0
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 7.0
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 8.0
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 9.0
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 10.0
			OR (sRndInfo.fCurrentRoundLength - fTimer) < 11.0
				PLAY_RANGE_SOUND("10_SEC_WARNING")
				SET_ROUND_FLAG(sRndInfo, ROUND_PLAYED_10_SEC_WARNING)
				sRndInfo.iBeepTimeStamp = GET_GAME_TIMER()
			ENDIF
		ELSE
			FLOAT fTimer = GET_TIMER_IN_SECONDS(sRndInfo.sTimeLeft)
			IF ((sRndInfo.fCurrentRoundLength - fTimer) < 5.5 AND GET_GAME_TIMER() - sRndInfo.iBeepTimeStamp > 500)
			OR ((sRndInfo.fCurrentRoundLength - fTimer) < 11.0 AND GET_GAME_TIMER() - sRndInfo.iBeepTimeStamp > 1000)
				CLEAR_ROUND_FLAG(sRndInfo, ROUND_PLAYED_10_SEC_WARNING)
			ENDIF
		ENDIF
	
		// Once time is up, stop the timer
		IF GET_TIMER_IN_SECONDS(sRndInfo.sTimeLeft) + 1.0 >= sRndInfo.fCurrentRoundLength
			CANCEL_TIMER(sRndInfo.sTimeLeft)
			SET_ROUND_FLAG(sRndInfo, ROUND_TIME_IS_UP)
		ENDIF
	ENDIF	
ENDPROC


/// PURPOSE:
///    Pause or unpause the shooting range round.
PROC PAUSE_RANGE_ROUND(Range_CoreData & sCoreData, Range_RoundInfo & sRndInfo, Range_MenuData & sMenuInfo, BOOL bPause = TRUE)
	IF bPause
		IF (sCoreData.ePrevState = SRM_SHOOT)
			// Pause our countdown timer
			IF IS_TIMER_STARTED(sRndInfo.sTimeLeft)
				PAUSE_TIMER(sRndInfo.sTimeLeft)
			ENDIF
			
			// Take away control.
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			// Handle pausing our time
			sRndInfo.fCurrentPauseTime = (GET_GAME_TIMER() / 1000.0)
		ENDIF
		
		CLEAR_HELP()
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Setting up the ROUND Quit Screen!")
		
		INIT_SIMPLE_USE_CONTEXT(sMenuInfo.menuContext, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(sMenuInfo.menuContext, "FE_HLP29", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ADD_SIMPLE_USE_CONTEXT_INPUT(sMenuInfo.menuContext, "FE_HLP31", FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		SET_SIMPLE_USE_CONTEXT_FULLSCREEN(sMenuInfo.menuContext, TRUE)
		
		IF NOT HAS_SOUND_FINISHED(sRndInfo.iSFX_Warning)
			STOP_SOUND(sRndInfo.iSFX_Warning)
		ENDIF
		
		sCoreData.eRangeState = SRM_EXIT
		SET_GAME_PAUSED(TRUE)
	ELSE
		// Unpause the timer
		IF (sCoreData.ePrevState = SRM_SHOOT)
			// Unpause timer.
			IF IS_TIMER_STARTED(sRndInfo.sTimeLeft)
				UNPAUSE_TIMER(sRndInfo.sTimeLeft)
			ENDIF
			
			// Give control back.
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			
			// Need to offset our rnd start time since we paused
			FLOAT tempTime = (GET_GAME_TIMER() / 1000.0) - sRndInfo.fCurrentPauseTime
			sRndInfo.fStartTime += tempTime
			sRndInfo.fTotalPauseTime += tempTime
			INIT_SIMPLE_USE_CONTEXT(sMenuInfo.menuContext, FALSE, FALSE, TRUE, TRUE)
			ADD_SIMPLE_USE_CONTEXT_INPUT(sMenuInfo.menuContext, "IB_QUIT", FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
			SET_SIMPLE_USE_CONTEXT_FULLSCREEN(sMenuInfo.menuContext, TRUE)
		
		ELIF (sCoreData.ePrevState = SRM_COUNT_DWN_INIT 
		OR sCoreData.ePrevState = SRM_COUNT_DWN)
			// Unpause our countdown timer.
			UNPAUSE_TIMER(sRndInfo.sGenTimer)
			INIT_SIMPLE_USE_CONTEXT(sMenuInfo.menuContext, FALSE, FALSE, TRUE, TRUE)
			ADD_SIMPLE_USE_CONTEXT_INPUT(sMenuInfo.menuContext, "IB_QUIT", FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
			SET_SIMPLE_USE_CONTEXT_FULLSCREEN(sMenuInfo.menuContext, TRUE)
		ENDIF
		
		sCoreData.eRangeState = sCoreData.ePrevState
		SET_GAME_PAUSED(FALSE)
	ENDIF
ENDPROC


/// PURPOSE:
///    Gets and saves a medal the player has earned
/// PARAMS:
///    challenge - The challenge he earned the medal in.
FUNC BOOL SAVE_PLAYER_MEDAL(Range_Challenge & sChallenge, Range_RoundInfo & sRndInfo)
	// First, get the medal we already have.
	RANGE_ROUND_MEDAL eCurrentMedal = GET_ROUND_MEDAL(sChallenge.eRndType)
	
	// If we we have a gold medal, don't do anything.
	IF (eCurrentMedal = RRM_GOLD)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "SavePlayerMedal -- Player already has gold medal, don't need to save anything.")
	ELSE
		// Okay, get the medal we earned.
		RANGE_ROUND_MEDAL eMedalEarned = GET_MEDAL_AWARD(sRndInfo.sScoreData.iP1Score, sChallenge)

		// If it's better than what we had stored, store it.
		IF (eMedalEarned > eCurrentMedal)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "SavePlayerMedal -- Player earned a better medal than last time. Save it.")
			SET_ROUND_MEDAL(sChallenge.eRndType, eMedalEarned)

			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates general round items that shouldn't be handled elsewhere.
PROC PROCESS_ROUND_GENERAL(Range_RoundInfo & sRndInfo)
	IF IS_ROUND_FLAG_SET(sRndInfo, ROUND_CAN_DISPLAY_TARGET_HELP)
		IF IS_ROUND_FLAG_SET(sRndInfo, ROUND_HVY3_FLOAT_HELP)
			CLEAR_ROUND_FLAG(sRndInfo, ROUND_HVY3_FLOAT_HELP)
		ENDIF
	ENDIF
ENDPROC
