USING "hud_drawing.sch"
USING "screen_placements.sch"
USING "UIUtil.sch"
USING "script_usecontext.sch"
USING "minigame_uiinputs.sch"
USING "socialclub_leaderboard.sch"
USING "minigame_big_message.sch"
USING "minigame_midsized_message.sch"

// Range_UI.sch
CONST_INT	MAX_WEAPONS_PER_CAT		4
//CONST_INT	STICK_DEAD_ZONE			33
CONST_INT	MAX_NUM_SPT				4
CONST_INT	COUNTDOWN_TIMER_LEN		3

//TWEAK_FLOAT	fScorecardScale			0.8

/// PURPOSE: The main panes of our menus.
ENUM RANGE_MENU_INDEX
	RANGE_MENU_WEAPCAT = 0,
	RANGE_MENU_WEAPONS,
	RANGE_MENU_CHALLENGES
ENDENUM

ENUM RANGE_MENU_FLAGS
	RMF_SHOWING_SCLB				= BIT0,
	RMF_SET_RANGE_SCLB_BUTTONS		= BIT1,
	RMF_SHOWED_MEDAL_SPLASH			= BIT2,
	RMF_EXITED_SIGNIN           	= BIT3,
	RMF_SCLB_PROFILE_BUTTON_SHOWN	= BIT4
ENDENUM

FUNC STRING GET_STRING_FROM_RANGE_MENU_FLAG(RANGE_MENU_FLAGS eFlag)
	SWITCH eFlag
		CASE RMF_SHOWING_SCLB					RETURN "RMF_SHOWING_SCLB"
		CASE RMF_SET_RANGE_SCLB_BUTTONS			RETURN "RMF_SET_RANGE_SCLB_BUTTONS"
		CASE RMF_SHOWED_MEDAL_SPLASH			RETURN "RMF_SHOWED_MEDAL_SPLASH"
		CASE RMF_EXITED_SIGNIN					RETURN "RMF_EXITED_SIGNIN"
		CASE RMF_SCLB_PROFILE_BUTTON_SHOWN		RETURN "RMF_SCLB_PROFILE_BUTTON_SHOWN"
	ENDSWITCH
	RETURN "Unknown RANGE_MENU_FLAGS Flag"
ENDFUNC

STRUCT Range_Menu_Element_NoLabel
	BOOL 	bActive
ENDSTRUCT

/// PURPOSE: OUR WEAPONS CATEGORIES MENU
STRUCT Range_WeaponCat_Menu
	// Sme noLabel magic you'll find below. We don't store it because it's a common string. We simply poll the system for it.
	Range_Menu_Element_NoLabel	menuElement[NUM_WEAPON_CATS]
	INT 						iNumElements
	INT 						iCurElement
ENDSTRUCT

/// PURPOSE: OUR ACTUAL WEAPONS MENU. THIS WILL BE RE-INIT EVERYTIME WE PICK A NEW CATEGORY.
STRUCT Range_Weapons_Menu
	// Bit of magic here. Because the weapon strings are already stored in the shop, we retreive those.
	// Thusly, there's no reason to store weapon strings all over again.
	// Can be confusing, but there's translation functions for it. Namely: GET_WEAPON_STRING_INFO
	// this function uses the position in the cat menu and the position in the weapon menu to tell us what we're on.
	
	Range_Menu_Element_NoLabel	menuElement[MAX_WEAPONS_PER_CAT]
	INT 						iNumElements
	INT 						iCurElement
ENDSTRUCT

/// PURPOSE: OUR CHALLENGES MENU.
STRUCT Range_Challenges_Menu
	Range_Menu_Element_NoLabel	menuElement[NUM_WEAPON_CHALLS]
	TEXT_LABEL_23			challengeDesc[NUM_WEAPON_CHALLS]
	RANGE_ROUND_TYPE		roundType[NUM_WEAPON_CHALLS]
	
	INT 					iNumElements
	INT 					iCurElement = -1
ENDSTRUCT

STRUCT SPT_OBJECT
	FLOAT 			fPosX
	FLOAT 			fPosY
	FLOAT			fFrameTime
	HUD_COLOURS		color
	INT				iAlpha
	INT				iPtValue
	BOOL			bActive
ENDSTRUCT

STRUCT Range_SPTData
	INT iActiveSPT
	SPT_OBJECT	sSptQueue[MAX_NUM_SPT]
ENDSTRUCT

STRUCT Range_MenuData
	RANGE_MENU_INDEX 			curMenu = RANGE_MENU_WEAPCAT
	Range_WeaponCat_Menu		catMenu
	Range_Weapons_Menu			weapMenu
	Range_Challenges_Menu		chalMenu
	BOOL 						bInitMenu
	//SCRIPT_SCALEFORM_UI			uiMenuChoices
	SIMPLE_USE_CONTEXT			menuContext
	
	SCALEFORM_INDEX				uiLeaderboard
	SCRIPT_SHARD_BIG_MESSAGE	uiSplashText
	BOOL						bTransitionOut = FALSE
	BOOL						bTransitionUp = FALSE
	BOOL						bFlashEffect = FALSE
	
	COUNTDOWN_UI 				uiCountdown
	
	MEGA_PLACEMENT_TOOLS		uiPlacement			// Placements
	UI_INPUT_DATA				uiInput				// Replaces bAcceptStickInput
	
	FLOAT 						fXPlacement = 302.0
	FLOAT						fXSpacing = 79.0
	
	// When the player selects something locked, we use this as the red value for the Challenge bar. It will be set to 255, and go down by 1 every
	// frame until it's black again.
	INT 						iMenuHoldDelay
	RANGE_MENU_FLAGS			eFlags
	
	INT 						iMedalFeed
	
	StructTimer					tQuitDelay
	
	INT							iLeaderboardWarningBitset
	
	BOOL 						bTransitionActive = FALSE
	//#IF IS_DEBUG_BUILD
	// RESTORE THIS!
	FLOAT fMenuRightOffset = 0.394
	FLOAT fMenuUpOffset = -0.02
	//#ENDIF
ENDSTRUCT

PROC SET_RANGE_MENU_FLAG(Range_MenuData &thisMenu, RANGE_MENU_FLAGS eFlag)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_RANGE_MENU_FLAG setting ", GET_STRING_FROM_RANGE_MENU_FLAG(eFlag))
	thisMenu.eFlags = thisMenu.eFlags | eFlag
ENDPROC

PROC CLEAR_RANGE_MENU_FLAG(Range_MenuData &thisMenu, RANGE_MENU_FLAGS eFlag)
	thisMenu.eFlags -= thisMenu.eFlags & eFlag
ENDPROC

FUNC BOOL IS_RANGE_MENU_FLAG_SET(Range_MenuData &thisMenu, RANGE_MENU_FLAGS eFlag)
	RETURN (ENUM_TO_INT(thisMenu.eFlags) & ENUM_TO_INT(eFlag)) <> 0
ENDFUNC

/// PURPOSE:
///    Lets the script know if the medal toast is displaying
/// RETURNS:
///    TRUE if displaying, FALSE when finished.
FUNC INT UPDATE_RANGE_MEDAL_TOAST(RANGE_ROUND_MEDAL eMedal, Range_RoundInfo & sRndInfo)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "UPDATE_RANGE_MEDAL_TOAST called")
	
	TEXT_LABEL_15 txtWeapon
	GET_WEAPON_STRING_INFO(sRndInfo.eWeaponCat, sRndInfo.eWeaponType, txtWeapon)
	INT iChallenge = ENUM_TO_INT(sRndInfo.eChallengeType) + 1
	
	IF (eMedal = RRM_BRONZE)
		PLAY_SOUND_FRONTEND(-1, "MEDAL_BRONZE", "HUD_AWARDS")
		BEGIN_TEXT_COMMAND_THEFEED_POST("SHR_CHALL_NAME")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(txtWeapon)
			ADD_TEXT_COMPONENT_INTEGER(iChallenge)
		RETURN END_TEXT_COMMAND_THEFEED_POST_AWARD("MPMedals_FEED", "Feed_Medal_ShootingRange", 0, HUD_COLOUR_BRONZE, "HUD_MED_UNLKED")
	ELIF (eMedal = RRM_SILVER)
		PLAY_SOUND_FRONTEND(-1, "MEDAL_SILVER", "HUD_AWARDS")
		BEGIN_TEXT_COMMAND_THEFEED_POST("SHR_CHALL_NAME")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(txtWeapon)
			ADD_TEXT_COMPONENT_INTEGER(iChallenge)
		RETURN END_TEXT_COMMAND_THEFEED_POST_AWARD("MPMedals_FEED", "Feed_Medal_ShootingRange", 0, HUD_COLOUR_SILVER, "HUD_MED_UNLKED")
	ELIF (eMedal = RRM_GOLD)
		PLAY_SOUND_FRONTEND(-1, "MEDAL_GOLD", "HUD_AWARDS")
		BEGIN_TEXT_COMMAND_THEFEED_POST("SHR_CHALL_NAME")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(txtWeapon)
			ADD_TEXT_COMPONENT_INTEGER(iChallenge)
		RETURN END_TEXT_COMMAND_THEFEED_POST_AWARD("MPMedals_FEED", "Feed_Medal_ShootingRange", 0, HUD_COLOUR_GOLD, "HUD_MED_UNLKED")
	ENDIF

	RETURN -1
ENDFUNC

PROC SET_RANGE_SP_MENU_HOLD_DELAY(Range_MenuData & sMenuInfo, INT iNewHoldDelay)
//	CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_RANGE_SP_MENU_HOLD_DELAY :: iNewHoldDelay=", iNewHoldDelay)
	sMenuInfo.iMenuHoldDelay = iNewHoldDelay
ENDPROC

FUNC INT GET_RANGE_SP_MENU_HOLD_DELAY(Range_MenuData & sMenuInfo)
	RETURN sMenuInfo.iMenuHoldDelay
ENDFUNC

PROC RANGE_CLEAN_UI(Range_MenuData & sMenuInfo)
	CLEANUP_SC_LEADERBOARD_UI(sMenuInfo.uiLeaderboard)
ENDPROC



