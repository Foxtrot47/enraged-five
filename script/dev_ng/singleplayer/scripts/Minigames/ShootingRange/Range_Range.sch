// Range_Range.sch
// I know the name on this sucks, but this stores the data about the actual range.
// Grid Struct
STRUCT GRID_LOCATION	
	VECTOR			vLoc			// Should probably turn this into a constant!!
	BOOL			bOccupied
ENDSTRUCT

ENUM RANGE_CORNER_INDEXES
	RANGE_FRONT_LEFT = 0,
	RANGE_BACK_LEFT,
	RANGE_FRONT_RIGHT,
	RANGE_BACK_RIGHT,
	RANGE_FRONT_LEFT_FAR,
	RANGE_BACK_LEFT_FAR,
	RANGE_FRONT_RIGHT_FAR,
	
	NUM_RANGE_CORNERS
ENDENUM

STRUCT Range_RangeData
	GRID_LOCATION	sRangeGrid[MAX_GRID_LOCS]
	VECTOR 			vRangeForward
	VECTOR			vRangeRight
	VECTOR			vRangeCorners[NUM_RANGE_CORNERS]
	VECTOR			vDoorPos
	
	RANGE_LOCATION  eLocation
	
	VECTOR 			vPlayerRootPos				// Player's position at the coutner
	FLOAT 			fPlayerRootHead				// Player's heading at the counter
	
	VECTOR			vAngledAreaChk1				// Box which the palyer cannot fire inside
	VECTOR			vAngledAreaChk2
	
	VECTOR 			vRemoveGlassesFacing		// Coord player will face when taking off glasses (180, basically)
	
	VECTOR 			vRedressPos					// Warp during final redress cut, w/ headergear on
	FLOAT 			fRedressHead				
	VECTOR			vRedressGoto				// Coord player will walk to after warp, to exit range
	VECTOR			vRedressLeaveRangeGoto		// Walking away from counter as part of headgear cutscene
	
	VECTOR 			vRedressCamStart
	VECTOR			vRedressCamStartOrient
	
	VECTOR			vRedressCamInterp
	VECTOR			vRedressCamInterpOrient
	
	VECTOR			vRedressCamEnd
	VECTOR			vRedressCamEndOrient
	
	VECTOR			vQuitCamPos					// Where cam snaps to player's side when removing headgera
	VECTOR			vQuitCamOrient
	
	FLOAT			fTargetEndZRot				// Te facing all targets should have when they enter.
ENDSTRUCT



