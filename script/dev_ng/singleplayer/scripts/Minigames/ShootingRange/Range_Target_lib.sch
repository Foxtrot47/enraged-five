// Range_Target_lib.sch
USING "Range_Target.sch"
USING "Range_Score_lib.sch"
USING "Range_UI_lib.sch"

/// PURPOSE:
///    Clears all decals and number of times a target has been hit.
/// PARAMS:
///    targetEntity - 
PROC RESET_TARGET_DECALS(TARGET_ENTITY & targetEntity)
	//REMOVE_DECALS_FROM_OBJECT(targetEntity.target.objBacking)
	//REMOVE_DECALS_FROM_OBJECT(targetEntity.target.objTarget)
	VECTOR vFwd
	
	IF DOES_ENTITY_EXIST(targetEntity.target.objBacking)
		vFwd = GET_ENTITY_FORWARD_VECTOR(targetEntity.target.objBacking)
		//vFwd.y *= -1.0
		REMOVE_DECALS_FROM_OBJECT_FACING(targetEntity.target.objBacking, vFwd)
	ENDIF
	
	vFwd = GET_ENTITY_FORWARD_VECTOR(targetEntity.target.objTarget)
	//vFwd.y *= -1.0
	REMOVE_DECALS_FROM_OBJECT_FACING(targetEntity.target.objTarget, vFwd)
ENDPROC


/// PURPOSE:
///    Takes a coordinate and returns the zone in the target that the coord is located in
/// PARAMS:
///    targToCheck - The target we are checking to see where we hit
///    vHitCoord - Where our projectile Hit
///    bAllowBackingHit - This will even count a backing hit (used in shotgun challenges)
/// RETURNS:
///    Which zone we hit (1-4), -1 if we didnt hit a zone. If backing hits are enabled, 0 can be returned.
FUNC INT GET_TARGET_HIT_ZONE(MODERN_TARGET & stTargToCheck, VECTOR vCoordsHit, BOOL bAllowBackingHit = FALSE)
	VECTOR vTargPos = GET_ENTITY_COORDS(stTargToCheck.objTarget)
	
	//If backing object doesn't exist it means we are using fraggable objects so move check coord down
	IF NOT DOES_ENTITY_EXIST(stTargToCheck.objBacking)
		vTargPos.z = vTargPos.z - 0.4 
	ENDIF	
	
	FLOAT fHitDistFromCenter = VDIST2(vTargPos, vCoordsHit)
	
	IF (fHitDistFromCenter <= fTgtBullseyeRadius_SQ)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Hit zone 1")
		RETURN 1
	ELIF (fHitDistFromCenter <= fTgtRing1Radius_SQ)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Hit zone 2")
		RETURN 2
	ELIF (fHitDistFromCenter <= fTgtRing2Radius_SQ)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Hit zone 3")
		RETURN 3
	ELIF (fHitDistFromCenter <= fTgtRing3Radius_SQ)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Hit zone 4")
		RETURN 4
	ENDIF
	
	IF bAllowBackingHit AND DOES_ENTITY_EXIST(stTargToCheck.objBacking)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(stTargToCheck.objBacking, PLAYER_PED_ID())
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Hit backing!")
			RETURN 0
		ENDIF
	ENDIF
	
	RETURN -1	
ENDFUNC

/// PURPOSE:
///    Just checks to see if a target was hit at all.
/// RETURNS:
///    TRUE if so.
FUNC BOOL IS_TARGET_HIT(MODERN_TARGET & stTargToCheck, BOOL bAllowBackingHit = FALSE, BOOL bCheckIfBroken = FALSE)
	// Check any part of the target.
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(stTargToCheck.objTarget, PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	IF bCheckIfBroken
		IF HAS_OBJECT_BEEN_BROKEN(stTargToCheck.objTarget)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Check the backing if we're supposed to.
	IF bAllowBackingHit AND DOES_ENTITY_EXIST(stTargToCheck.objBacking)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(stTargToCheck.objBacking, PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Checks what zone we hit in, handles all things that should happen 
///    after target hit or missed
/// PARAMS:
///    stTargToCheck - 
PROC CHECK_TARGET_HIT(TARGET_ENTITY & sTargetEntity, Range_RoundInfo & sRndInfo, Range_SPTData & sSPTInfo)
	VECTOR vCoordHit
	INT iHitZone

	// Did you hit the target
	IF GET_PED_LAST_WEAPON_IMPACT_COORD(PLAYER_PED_ID(), vCoordHit)
		// If we have already shot a target, we don't want to keep checking other targets for this shot so exit out
		IF IS_ROUND_FLAG_SET(sRndInfo, ROUND_TARGET_HIT)
			DEBUG_MESSAGE("CheckTargetHit -- WE already hit a target this iteration...")
			EXIT
		ENDIF
		
		// We took a shot
		SET_ROUND_FLAG(sRndInfo, ROUND_SHOT_FIRED)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "CHECK_TARGET_HIT: Setting the ROUND_SHOT_FIRED Flag")
							
		// Check if we are flipped -- We might want to move this in at some point, but for now don't do anything
		IF (CHECK_TARGET_STATUS(sTargetEntity, TS_FLIPPED) AND NOT CHECK_TARGET_STATUS(sTargetEntity, TS_FLIPPING)) OR CHECK_TARGET_STATUS(sTargetEntity, TS_EXITING) // also cant shoot while entering/exiting.. 
			// Clear all the hit detection
			CLEAR_ENTITY_LAST_WEAPON_DAMAGE(sTargetEntity.target.objTarget)
			EXIT
		ENDIF
		
		// Check which zone we hit
		iHitZone = GET_TARGET_HIT_ZONE(sTargetEntity.target, vCoordHit, TRUE)
		
		IF (iHitZone > 0)
			SCORE_ZONE_HIT(1, iHitZone, sRndInfo, sSPTInfo)			
		ELIF (iHitZone = 0)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "JUST HIT THE BACKING!")
		ENDIF
		
		// Clear all the hit detection
		CLEAR_ENTITY_LAST_WEAPON_DAMAGE(sTargetEntity.target.objTarget)

		// Did we hit something?
		IF iHitZone <= 4 and iHitZone >= 1 AND NOT IS_ROUND_FLAG_SET(sRndInfo, ROUND_TARGET_HIT)
			sTargetEntity.fHp -= sRndInfo.fCurrentBulletDamage
			sRndInfo.sMPData.bIncrementMP = TRUE
			SET_ROUND_FLAG(sRndInfo, ROUND_TARGET_HIT)
		ENDIF					
	ENDIF	
ENDPROC


/// PURPOSE:
///    
/// PARAMS:
///    targetEntity - 
/// RETURNS:
///    TRUE if we hit this target. FALSE if not.
FUNC BOOL CHECK_TARGET_HIT_SHOTGUN(TARGET_ENTITY & sTargetEntity, Range_RoundInfo & sRndInfo, GRID_LOCATION & sGridInfo[], Range_SPTData & sSPTInfo)
	VECTOR coordHit
	
	IF GET_PED_LAST_WEAPON_IMPACT_COORD(PLAYER_PED_ID(), coordHit)
		// We took a shot
		SET_ROUND_FLAG(sRndInfo, ROUND_SHOT_FIRED)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "CHECK_TARGET_HIT_SHOTGUN: Setting ROUND_SHOT_FIRED flag")
							
		// Check if we are flipped -- We might want to move this in at some point, but for now don't do anything
		IF CHECK_TARGET_STATUS(sTargetEntity, TS_FLIPPED) OR CHECK_TARGET_STATUS(sTargetEntity, TS_EXITING) // also cant shoot while entering/exiting.. 
			//DEBUG_MESSAGE("CheckTargetHit -- Target is flipped, dont do anything..")
			// Clear all the hit detection
			CLEAR_ENTITY_LAST_WEAPON_DAMAGE(sTargetEntity.target.objTarget)
			
			IF DOES_ENTITY_EXIST(sTargetEntity.target.objBacking)
				// This clears backing hits, if we want to keep them.
				CLEAR_ENTITY_LAST_WEAPON_DAMAGE(sTargetEntity.target.objBacking)
			ENDIF
			RETURN FALSE
		ENDIF

		// Check which zone we hit
		BOOL bTargetHit = IS_TARGET_HIT(sTargetEntity.target, TRUE)
		//iHitZone = GET_TARGET_HIT_ZONE(sTargetEntity.target, coordHit, TRUE)
		
		// Clear all the hit detection
		CLEAR_ENTITY_LAST_WEAPON_DAMAGE(sTargetEntity.target.objTarget)
		
		IF DOES_ENTITY_EXIST(sTargetEntity.target.objBacking)
			// This clears backing hits, if we want to keep them.
			CLEAR_ENTITY_LAST_WEAPON_DAMAGE(sTargetEntity.target.objBacking)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sTargetEntity.target.objBacking)
		ENDIF
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sTargetEntity.target.objTarget)
		
		// Did we hit something?
		IF bTargetHit			
			IF (sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_DESTRUCTION)
				// If we're in destruction mode, each hit is worth one point.
				sRndInfo.sScoreData.iP1Score += 1

			ELIF (sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_RANGE_REVERSE)
				// If we're in reverse-range mode, each target is worth more points based on how close it was.
				GRID_INDEX gridLoc = GET_RANGE_GRID_FROM_VECTOR(sTargetEntity.target.vPos, sGridInfo)
				PROCESS_SCORE_REVDEST(gridLoc, sRndInfo, sSPTInfo)
			ENDIF
			
			// Use zone1 hits for shotgun this will be a problem if we end up saving off these stats
			//sRndInfo.iZ1Hits++
			CDEBUG2LN(DEBUG_SHOOTRANGE, "ScoreZoneHit -- TARGET HIT!!")
		
			sTargetEntity.fHp -= sRndInfo.fCurrentBulletDamage
			sRndInfo.sMPData.bIncrementMP = TRUE
			SET_ROUND_FLAG(sRndInfo, ROUND_TARGET_HIT)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	
	
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_TARGET_HIT_WITH_EXPLOSION(TARGET_ENTITY & sTargetEntity, Range_RoundInfo & sRndInfo)

	IF CHECK_TARGET_STATUS(sTargetEntity, TS_EXITING)
	OR CHECK_TARGET_STATUS(sTargetEntity, TS_ENTERING)// also cant shoot while entering/exiting..
		SET_ENTITY_INVINCIBLE(sTargetEntity.target.objTarget, TRUE)
		RETURN FALSE
	ELSE
		SET_ENTITY_INVINCIBLE(sTargetEntity.target.objTarget, FALSE)
	ENDIF
	
	BOOL bTargetHit = IS_TARGET_HIT(sTargetEntity.target, FALSE, TRUE)
	
	// Clear all the hit detection
	CLEAR_ENTITY_LAST_WEAPON_DAMAGE(sTargetEntity.target.objTarget)
	CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sTargetEntity.target.objTarget)
	
	// Did we hit something?
	IF bTargetHit			
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "ScoreZoneHit -- TARGET HIT!!")
	
		sTargetEntity.fHp -= sRndInfo.fCurrentBulletDamage
		sRndInfo.sMPData.bIncrementMP = TRUE
		SET_ROUND_FLAG(sRndInfo, ROUND_TARGET_HIT)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Basic target function that all targets use. This checks to see
///    if we need to update the position or heading
/// PARAMS:
///    stTarget - The target we are checking
PROC UPDATE_TARGET_POS_ORI(TARGET_ENTITY &targetEntity)
	// Since everything is attached, we should just have to move the bullseye
	IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(targetEntity.target.objArm),targetEntity.target.vPos,0.01)		
		SET_ENTITY_COORDS(targetEntity.target.objArm,targetEntity.target.vPos)
		targetEntity.target.vCenter = targetEntity.target.vPos
		targetEntity.target.vCenter.z -= CONST_CENTER_Z_OFFSET
	ENDIF
	
	//IF Backing exists it means we aren't using fraggable targets.
	IF DOES_ENTITY_EXIST(targetEntity.target.objBacking)
		// Update the BULLSEYE heading if it changes (don't need to flip the whole arm)
		IF GET_ENTITY_HEADING(targetEntity.target.objBacking) <= (targetEntity.target.fHeading - 0.15)
		OR GET_ENTITY_HEADING(targetEntity.target.objBacking) >= (targetEntity.target.fHeading + 0.15)	
			//SET_ENTITY_HEADING(targetEntity.target.objBull,targetEntity.target.fHeading)
			IF CHECK_TARGET_STATUS(targetEntity, TS_HEIGHT_HIGH)
				// Short height variant.
				ATTACH_ENTITY_TO_ENTITY(targetEntity.target.objBacking,targetEntity.target.objArm,0,<<0.0,0.007,-0.450>>,<<0.0,0.0,targetEntity.target.fHeading>>)
			ELIF CHECK_TARGET_STATUS(targetEntity, TS_HEIGHT_LOW)
				// Long height variant.
				ATTACH_ENTITY_TO_ENTITY(targetEntity.target.objBacking,targetEntity.target.objArm,0,<<0.0,0.007,-1.117>>,<<0.0,0.0,targetEntity.target.fHeading>>)
			ELSE
				// Medium height variant.
				ATTACH_ENTITY_TO_ENTITY(targetEntity.target.objBacking,targetEntity.target.objArm,0,<<0.0,0.007,-0.760>>,<<0.0,0.0,targetEntity.target.fHeading>>)
			ENDIF
		ENDIF
	ELSE
		// Update the BULLSEYE heading if it changes (don't need to flip the whole arm)
		IF GET_ENTITY_HEADING(targetEntity.target.objTarget) <= (targetEntity.target.fHeading - 0.15)
		OR GET_ENTITY_HEADING(targetEntity.target.objTarget) >= (targetEntity.target.fHeading + 0.15)	
			//SET_ENTITY_HEADING(targetEntity.target.objBull,targetEntity.target.fHeading)
			IF CHECK_TARGET_STATUS(targetEntity, TS_HEIGHT_HIGH)
				// Short height variant.
				ATTACH_ENTITY_TO_ENTITY(targetEntity.target.objTarget,targetEntity.target.objArm,0,<<0.0,0.007,-0.450>>,<<0.0,0.0,targetEntity.target.fHeading>>, DEFAULT, DEFAULT, TRUE)
			ELIF CHECK_TARGET_STATUS(targetEntity, TS_HEIGHT_LOW)
				// Long height variant.
				ATTACH_ENTITY_TO_ENTITY(targetEntity.target.objTarget,targetEntity.target.objArm,0,<<0.0,0.007,-1.117>>,<<0.0,0.0,targetEntity.target.fHeading>>, DEFAULT, DEFAULT, TRUE)
			ELSE
				// Medium height variant.
				ATTACH_ENTITY_TO_ENTITY(targetEntity.target.objTarget,targetEntity.target.objArm,0,<<0.0,0.007,-0.760>>,<<0.0,0.0,targetEntity.target.fHeading>>, DEFAULT, DEFAULT, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	// Check our Rotation
	IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_ROTATION(targetEntity.target.objArm),targetEntity.target.vRot,0.01)
		SET_ENTITY_ROTATION(targetEntity.target.objArm,targetEntity.target.vRot)
	ENDIF
ENDPROC


/// PURPOSE:
///    Flips the target AROUND THE Z. It will face away/toward the player.
/// RETURNS:
///    TRUE when done.
FUNC BOOL FLIP_TARGET(TARGET_ENTITY &tEntityToFlip)
	FLOAT fStart,fEnd
	FLOAT fInterpTime
	
	// Increment our frame count
	tEntityToFlip.fFlipFrameTime += GET_FRAME_TIME()
	
	// Find out how much we should be flipping
	fInterpTime = tEntityToFlip.fFlipFrameTime / CONST_FLIP_TIME
	
	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		
		// Are we coming in or going out..
		IF CHECK_TARGET_STATUS(tEntityToFlip, TS_FLIPPED)
			fStart = 0.0
			fEnd = 180.0
		ELSE
			fStart = 180.0
			fEnd = 0.0
		ENDIF
		
		tEntityToFlip.target.fHeading = fStart + (fEnd - fStart) * fInterpTime
		//DEBUG_MESSAGE("FlipBullseye -- Our heading while flipping: ",GET_STRING_FROM_FLOAT(tEntityToFlip.target.fHeading))	
		RETURN FALSE
	ELSE	
		// Reset our frame time
		tEntityToFlip.fFlipFrameTime = 0.0
		
		// Pop it just in case
		IF CHECK_TARGET_STATUS(tEntityToFlip, TS_FLIPPED)
			tEntityToFlip.target.fHeading = 180.0
		ELSE
			tEntityToFlip.target.fHeading = 0.0
		ENDIF
		
		// Set our entityi to not entering
		DEBUG_MESSAGE("FlipBullseye -- Finished Flipping!!")		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC


/// PURPOSE:
///    Rotates the target arm AROUND THE X. This, in turn, will rotate the target.
/// RETURNS:
///    TRUE when done rotating.
FUNC BOOL ROTATE_TARGET(TARGET_ENTITY & sEntityToRot, Range_RangeData & sRangeInfo, BOOL bEntering = TRUE)
	VECTOR vStart,vEnd
	FLOAT fInterpTime
	
	// If we're a respawning target that has a respawn time, and it's earlier than that, do nothing.
	IF CHECK_TARGET_STATUS(sEntityToRot, TS_RESPAWNS) OR CHECK_TARGET_STATUS(sEntityToRot, TS_RESPAWN_LONG) OR 
			CHECK_TARGET_STATUS(sEntityToRot, TS_RESPAWN_SHORT)
		IF (sEntityToRot.fRespawnAtTime > 0.0)
			IF (sEntityToRot.fRespawnAtTime > GET_GAME_TIMER() / 1000.0)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Not re-entering target. Respawns at: ", sEntityToRot.fRespawnAtTime)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Current time is: ", GET_GAME_TIMER() / 1000.0)
				RETURN FALSE
			ENDIF
		ENDIF
		
		sEntityToRot.fHP = 100.0
	ENDIF
		
	// Find out how much we should be moving
	IF CHECK_TARGET_STATUS(sEntityToRot, TS_ENTERING)
		// Increment our frame count
		sEntityToRot.fRotFrameTime += (GET_FRAME_TIME() * sEntityToRot.fRotateCoeff)
		sEntityToRot.fRotateCoeff += fRotateCoeffInc		// was 0.12
		
		fInterpTime = sEntityToRot.fRotFrameTime / CONST_ROT_TIME_ENTER
	ELSE
		sEntityToRot.fRotFrameTime += GET_FRAME_TIME()
		fInterpTime = sEntityToRot.fRotFrameTime / CONST_ROT_TIME_EXIT
	ENDIF
	
	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		// Are we coming in or going out..
		IF bEntering
			vStart = <<-90.0,0.0, sRangeInfo.fTargetEndZRot>>
			vEnd = <<0.0,0.0, sRangeInfo.fTargetEndZRot>>
		ELSE
			vStart = <<0.0,0.0, sRangeInfo.fTargetEndZRot>>	
			vEnd = <<-90.0,0.0, sRangeInfo.fTargetEndZRot>>
		ENDIF
		sEntityToRot.target.vRot = vStart + (vEnd - vStart) * fInterpTime
				
		RETURN FALSE
	ELSE	
		// Pop em
		IF bEntering
			sEntityToRot.target.vRot = <<0.0,0.0, sRangeInfo.fTargetEndZRot>>	
			//CDEBUG2LN(DEBUG_SHOOTRANGE, "RotateArm -- (ENTERING) Finished Rotating!!")	
		ELSE
			sEntityToRot.target.vRot = <<-90.0,0.0, sRangeInfo.fTargetEndZRot>>
			//CDEBUG2LN(DEBUG_SHOOTRANGE, "RotateArm -- (EXITING) Finished Rotating!!")	
		ENDIF
		
		// Reset our frame time
		sEntityToRot.fRotFrameTime = 0.0		
		sEntityToRot.fRotateCoeff = 0.5
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC


/// PURPOSE:
///    Forces the target to wobble.
/// RETURNS:
///    TRUE when done.
FUNC BOOL WOBBLE_TARGET(TARGET_ENTITY & tEntityToWobble)
	IF NOT (CHECK_TARGET_STATUS(tEntityToWobble, TS_EXITING) OR CHECK_TARGET_STATUS(tEntityToWobble, TS_FLIPPING))
		// We need to give the target a little back and forth for like half a second. Let's go ahead and do that.
		tEntityToWobble.fWobbleFrameTime += GET_FRAME_TIME()
		tEntityToWobble.target.vRot.x = SIN(tEntityToWobble.fWobbleFrameTime * 600.0) * tEntityToWobble.fWobbleCoeff 
		
		IF (tEntityToWobble.fWobbleCoeff > WOBBLE_COEFF_DONE)
			tEntityToWobble.fWobbleCoeff -= tEntityToWobble.fWobbleChange
		ELSE
			tEntityToWobble.fWobbleFrameTime = 0.0
			RETURN TRUE
		ENDIF
			
		SET_ENTITY_ROTATION(tEntityToWobble.target.objArm, tEntityToWobble.target.vRot)
		
		RETURN FALSE
	ELSE
		// We're exiting or flipping. No more wobble.
		tEntityToWobble.fWobbleFrameTime = 0.0
		RETURN TRUE
	ENDIF
ENDFUNC


/// PURPOSE:
///    Lerp function for moving targets
/// RETURNS:
///    TRUE once we are finished moving
FUNC BOOL MOVE_TARGET(TARGET_ENTITY & tEntityToMove, GRID_LOCATION & sGridData[])
	VECTOR vStart, vEnd
	FLOAT fInterpTime
	
	// Increment our frame count
	tEntityToMove.fMoveFrame += GET_FRAME_TIME()
	
	// Find out how much we should be moving
	fInterpTime = tEntityToMove.fMoveFrame / tEntityToMove.fMoveLength
	
	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		
		vStart = tEntityToMove.vMoveStart//m_vMoveStart[tEntityToMove.iMoveIndex]
		vEnd = sGridData[tEntityToMove.eMoveDest].vLoc	
		tEntityToMove.target.vPos = vStart + (vEnd - vStart) * fInterpTime
				
		RETURN FALSE
	ELSE	
		// Set our entityi to not moving, and reset all of its paramaters
		DEBUG_MESSAGE("MoveTarget -- Finished Moving!!")
		// pop it
		tEntityToMove.target.vPos = sGridData[tEntityToMove.eMoveDest].vLoc	
		//tEntityToMove.fTargCreateTime = (GET_GAME_TIMER() / 1000.0)	
		
		// Old grid loc is no longer occupied
		sGridData[tEntityToMove.eGridIndex].bOccupied = FALSE
		
		// New grid loc is occupied
		tEntityToMove.eGridIndex = tEntityToMove.eMoveDest
		sGridData[tEntityToMove.eGridIndex].bOccupied = TRUE
		
		// STOP PLAYING SOUND: Moving
		IF (tEntityToMove.iMovingSound != -1)
			STOP_SOUND(tEntityToMove.iMovingSound)
			RELEASE_SOUND_ID(tEntityToMove.iMovingSound)
			tEntityToMove.iMovingSound = -1
		ENDIF
		
		// PLAY SOUND: Stopped moving
		PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_STOP_MASTER", tEntityToMove.target.objArm)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC


/// PURPOSE:
///    Creates a target for the shooting range. 
/// RETURNS:
///    TRUE if the target created successfully.
FUNC BOOL CREATE_TARGET(TARGET_ENTITY & targetEntity, Range_RoundInfo & sRndInfo, Range_RangeData & sRangeInfo, GRID_INDEX eGridLocation, BOOL bRotated = TRUE)
	MODEL_NAMES tempArm
	MODEL_NAMES tempBacking
	MODEL_NAMES tempTarget

	// First make sure we don't already have a target in this location
	IF (eGridLocation = INVALID_LOC)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Invalid Entity Index: ", ENUM_TO_INT(eGridLocation))
		RETURN FALSE
	ENDIF
		
	// First make sure we don't already have a target in this location
	IF (sRangeInfo.sRangeGrid[eGridLocation].bOccupied)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "CREATE_TARGET -- Location is already Occupied (translate this int to GRID_INDEX): ", ENUM_TO_INT(eGridLocation))
		RETURN FALSE
	ENDIF
	
	MODERN_TARGET	newTarget
	
	IF IS_ROUND_FLAG_SET(sRndInfo, ROUND_USE_METAL_TARGETS)
		// Metal Targets
		tempBacking 	= PROP_TARGET_BACKBOARD_B
		tempArm 		= PROP_TARGET_ARM_B
		tempTarget		= PROP_TARGET_COMP_METAL
	ELIF IS_ROUND_FLAG_SET(sRndInfo, ROUND_USE_FRAGGABLE_TARGETS)
		tempArm 		= PROP_TARGET_ARM_B
		tempTarget		= INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("PROP_TARGET_FRAG_BOARD"))
	ELSE
		// Regular Targets
		tempBacking 	= PROP_TARGET_BACKBOARD
		tempArm 		= PROP_TARGET_ARM
		tempTarget		= PROP_TARGET_COMP_WOOD
	ENDIF
	
	// If we were given a flag to pick a random height, pick one. Needed for the arm and target height.
	IF IS_BITMASK_AS_ENUM_SET(targetEntity.iStatus, TS_HEIGHT_RANDOM)
		CLEAR_BITMASK_AS_ENUM(targetEntity.iStatus, TS_HEIGHT_RANDOM)
		CLEAR_BITMASK_AS_ENUM(targetEntity.iStatus, TS_HEIGHT_HIGH)
		CLEAR_BITMASK_AS_ENUM(targetEntity.iStatus, TS_HEIGHT_LOW)
		CLEAR_BITMASK_AS_ENUM(targetEntity.iStatus, TS_HEIGHT_MID)
		
		INT iHgt = GET_RANDOM_INT_IN_RANGE(0, 2)
		IF (iHgt = 0)
			SET_BITMASK_AS_ENUM(targetEntity.iStatus, TS_HEIGHT_MID)
		ELIF (iHgt = 1)
			SET_BITMASK_AS_ENUM(targetEntity.iStatus, TS_HEIGHT_LOW)
		ELSE
			SET_BITMASK_AS_ENUM(targetEntity.iStatus, TS_HEIGHT_HIGH)
		ENDIF
	ENDIF
	
	// Random height variation to target.
	IF IS_BITMASK_AS_ENUM_SET(targetEntity.iStatus, TS_HEIGHT_LOW)
		tempArm = PROP_TARGET_ARM_LONG
		
	ELIF IS_BITMASK_AS_ENUM_SET(targetEntity.iStatus, TS_HEIGHT_HIGH)
		tempArm = PROP_TARGET_ARM_SM
	ENDIF
	
	// Now then, lets create the target!
	newTarget.vPos = sRangeInfo.sRangeGrid[eGridLocation].vLoc
	
	newTarget.fHeading = 180.0
	newTarget.vCenter = newTarget.vPos
	newTarget.vCenter.z -= CONST_CENTER_Z_OFFSET			// This value seems to be highly ineffectual.
	
	IF NOT IS_ROUND_FLAG_SET(sRndInfo, ROUND_USE_FRAGGABLE_TARGETS)
		// Create the backing of the target
		newTarget.objBacking = CREATE_OBJECT_NO_OFFSET(tempBacking, newTarget.vPos)

		// Create the target itself.
		newTarget.objTarget = CREATE_OBJECT_NO_OFFSET(tempTarget, newTarget.vPos)
		ATTACH_ENTITY_TO_ENTITY(newTarget.objTarget, newTarget.objBacking,0,<<0.0,-0.04,-0.410>>,<<0.0,0.0,0.0>>)
		
		// Create the Arm
		newTarget.objArm = CREATE_OBJECT_NO_OFFSET(tempArm, newTarget.vPos)

		// Attach the backing to the arm. This affects only creation, not final resting point!
		IF IS_BITMASK_AS_ENUM_SET(targetEntity.iStatus, TS_HEIGHT_HIGH)
			// Short height variant.
			ATTACH_ENTITY_TO_ENTITY(newTarget.objBacking, newTarget.objArm,0,<<0.0,0.007,-0.450>>,<<0.0,0.0,180.0>>)
		ELIF IS_BITMASK_AS_ENUM_SET(targetEntity.iStatus, TS_HEIGHT_LOW)
			// Long height variant.
			ATTACH_ENTITY_TO_ENTITY(newTarget.objBacking, newTarget.objArm,0,<<0.0,0.007,-1.117>>,<<0.0,0.0,180.0>>)
		ELSE
			// Medium height variant.
			ATTACH_ENTITY_TO_ENTITY(newTarget.objBacking, newTarget.objArm,0,<<0.0,0.007,-0.760>>,<<0.0,0.0,180.0>>)
		ENDIF
	ELSE
		newTarget.objTarget = CREATE_OBJECT_NO_OFFSET(tempTarget, newTarget.vPos)
		
		// Create the Arm
		newTarget.objArm = CREATE_OBJECT_NO_OFFSET(tempArm, newTarget.vPos)
		
		// Attach the backing to the arm. This affects only creation, not final resting point!
		IF IS_BITMASK_AS_ENUM_SET(targetEntity.iStatus, TS_HEIGHT_HIGH)
			// Short height variant.
			ATTACH_ENTITY_TO_ENTITY(newTarget.objTarget, newTarget.objArm,0,<<0.0,0.007,-0.450>>,<<0.0,0.0,180.0>>, DEFAULT, DEFAULT, TRUE)
		ELIF IS_BITMASK_AS_ENUM_SET(targetEntity.iStatus, TS_HEIGHT_LOW)
			// Long height variant.
			ATTACH_ENTITY_TO_ENTITY(newTarget.objTarget, newTarget.objArm,0,<<0.0,0.007,-1.117>>,<<0.0,0.0,180.0>>, DEFAULT, DEFAULT, TRUE)
		ELSE
			// Medium height variant.
			ATTACH_ENTITY_TO_ENTITY(newTarget.objTarget, newTarget.objArm,0,<<0.0,0.007,-0.760>>,<<0.0,0.0,180.0>>, DEFAULT, DEFAULT, TRUE)
		ENDIF
	ENDIF
	
	// Rotate the arm
	IF bRotated
		SET_ENTITY_ROTATION(newTarget.objArm,<<-90.0,0.0,sRangeInfo.fTargetEndZRot>>)
	ELSE
		SET_ENTITY_ROTATION(newTarget.objArm,<<0.0,0.0,sRangeInfo.fTargetEndZRot>>)
	ENDIF
	FREEZE_ENTITY_POSITION(newTarget.objArm,TRUE)

	// We're not flipped
	targetEntity.fHp	  = CONST_TARGET_HP
	targetEntity.target = newTarget
	targetEntity.eGridIndex = eGridLocation
	targetEntity.fTargCreateTime = (GET_GAME_TIMER() / 1000.0)
	ADD_TARGET_STATUS(targetEntity, TS_ENTERING)
	targetEntity.fRotateCoeff = 0.5
	
	// Mark this grid location as occupied
	sRangeInfo.sRangeGrid[eGridLocation].bOccupied = TRUE
	
	// PLAY SOUND: Creation -- Not sure if this is a real thing.
	IF NOT IS_BITMASK_AS_ENUM_SET(targetEntity.iStatus, TS_DONT_PLAY_ENTRY_SOUND)
		PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", targetEntity.target.objArm)
	ENDIF
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Handles creation of targets
PROC PROCESS_TARGET_CREATION(Range_RoundInfo & sRndInfo, TARGET_ENTITY & sTargetEnts[], Range_RangeData & sRangeInfo)//GRID_LOCATION & sGridInfo[])
	INT iTer
	// Don't worry if our time is up
	IF IS_ROUND_FLAG_SET(sRndInfo, ROUND_TIME_IS_UP)
		EXIT
	ENDIF
	
	// Loop through all of our targets, create accordingly
	REPEAT sRndInfo.iMaxTargets iTer
		// Has this target already been created?
		IF NOT CHECK_TARGET_STATUS(sTargetEnts[iTer], TS_CREATED) AND sTargetEnts[iTer].eGridIndex <> INVALID_LOC
			// Is our goal number -1? If so, create if the target behind us has been destroyed
			IF sTargetEnts[iTer].iCreateNumber = -1
				// If we are the first index, then just create, other wise, see if the one behind us is destroyed
				IF iTer = 0
					// Create the target
					IF CREATE_TARGET(sTargetEnts[iTer], sRndInfo, sRangeInfo, sTargetEnts[iTer].eGridIndex)
						// We've created the target
						ADD_TARGET_STATUS(sTargetEnts[iTer],TS_CREATED)
					ENDIF					
				
				ELIF (iTer <= sRndInfo.iTargetsDestroyed)
					// Create the target
					IF CREATE_TARGET(sTargetEnts[iTer], sRndInfo, sRangeInfo, sTargetEnts[iTer].eGridIndex)
						// We've created the target
						ADD_TARGET_STATUS(sTargetEnts[iTer],TS_CREATED)
						CDEBUG2LN(DEBUG_SHOOTRANGE, "ProcessTargetCreation -- Creating Entity #", iTer, " Destroyed Target: ", sRndInfo.iTargetsDestroyed)
					ENDIF	
				ENDIF
			
			// Have our goal number of targets been destroyed
			ELIF (sTargetEnts[iTer].iCreateNumber <= sRndInfo.iTargetsDestroyed)
				// Create the target
				IF CREATE_TARGET(sTargetEnts[iTer], sRndInfo, sRangeInfo, sTargetEnts[iTer].eGridIndex)
					// We've created the target
					ADD_TARGET_STATUS(sTargetEnts[iTer],TS_CREATED)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "ProcessTargetCreation -- Creating Entity #", iTer, " Destroyed Target: ", sRndInfo.iTargetsDestroyed)
				ENDIF				
			ENDIF

		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Constructor for the targets. Sets them up to be parsed through and created. Once created
///    'CreateTarget' will fill in the rest of the data
/// PARAMS:
///    Target - The target we want to create
///    eGridIndex - Which location it will be created at
///    iCreateNumber - How many targets need to be destroyed before this can be made. -1 means it will use its index
///    and detect when the target set up before it was destroyed
PROC SETUP_TARGET_ENTITY(TARGET_ENTITY & targetEntity, GRID_INDEX eGridIndex, INT iCreateNumber = -1, TARGET_STATUS tgtStatus = TS_HEIGHT_MID)
	IF targetEntity.eGridIndex <> INVALID_LOC
		DEBUG_MESSAGE("SetupTargetEntity -- Trying to setup an entity that already has been setup, please look over your setup..")
	ENDIF
	
	targetEntity.eGridIndex		= eGridIndex
	targetEntity.iCreateNumber	= iCreateNumber
	targetEntity.iStatus		= 0
	
	// Set height if we've gotten an appropriate flag passed in.
	// Also, set any other creation flags.
	SET_BITMASK_AS_ENUM(targetEntity.iStatus, tgtStatus)
ENDPROC


/// PURPOSE:
///    Updates all the specifics of a target. This is the main update for a target
///    the specifics of the behaviors are handled in other functions called by this one
/// PARAMS:
///    stTarget - The target we wa
/// RETURNS:
///    TRUE if we destroyed this target. FALSE if not.
FUNC BOOL UPDATE_TARGET_ENTITY(TARGET_ENTITY & sTargetEntity, GRID_LOCATION & sGridData[], Range_RoundInfo & sRndInfo, Range_SPTData & sSPTInfo, Range_RangeData & sRangeInfo, BOOL bIsTutorial = FALSE)
	// Ensure that the target never actually dies... (# 386651)
	IF NOT IS_ENTITY_DEAD(sTargetEntity.target.objTarget)
		SET_ENTITY_HEALTH(sTargetEntity.target.objTarget, 700)
	ENDIF
	IF NOT IS_ENTITY_DEAD(sTargetEntity.target.objBacking)
		SET_ENTITY_HEALTH(sTargetEntity.target.objBacking, 700)
	ENDIF
	IF NOT IS_ENTITY_DEAD(sTargetEntity.target.objArm)
		SET_ENTITY_HEALTH(sTargetEntity.target.objArm, 700)
	ENDIF	
	
	// If were not created.. exit out of this..
	IF NOT CHECK_TARGET_STATUS(sTargetEntity, TS_CREATED)
	OR CHECK_TARGET_STATUS(sTargetEntity, TS_DESTROYED)
		//DEBUG_MESSAGE("UpdateTargetEntity -- Trying to update a target entity that hasnt been created yet @ Loc: '",GetGridLocName(targetEntity.eGridIndex),"' .. exiting..")
		RETURN FALSE
	ENDIF
	
	// Do we need to enter in?
	IF CHECK_TARGET_STATUS(sTargetEntity, TS_ENTERING)
		IF ROTATE_TARGET(sTargetEntity, sRangeInfo)
			REMOVE_TARGET_STATUS(sTargetEntity, TS_ENTERING)
			
			// Set us to wobble.
			ADD_TARGET_STATUS(sTargetEntity, TS_WOBBLING)
			
			sTargetEntity.fWobbleCoeff = GET_RANDOM_FLOAT_IN_RANGE(WOBBLE_COEFF_MIN, WOBBLE_COEFF_MAX)
			sTargetEntity.fWobbleChange = GET_RANDOM_FLOAT_IN_RANGE(WOBBLE_COEFF_DEC_MIN, WOBBLE_COEFF_DEC_MAX)
		ENDIF
	// Well are we exiting?
	ELIF CHECK_TARGET_STATUS(sTargetEntity, TS_EXITING)
		IF ROTATE_TARGET(sTargetEntity, sRangeInfo, FALSE)
			IF NOT (CHECK_TARGET_STATUS(sTargetEntity, TS_RESPAWNS) OR CHECK_TARGET_STATUS(sTargetEntity, TS_RESPAWN_LONG) OR CHECK_TARGET_STATUS(sTargetEntity, TS_RESPAWN_SHORT))
				// Destroy the target
				DESTROY_TARGET_ENTITY(sTargetEntity, sGridData)
				// Mark this grid as un occupided
				sGridData[sTargetEntity.eGridIndex].bOccupied = FALSE
				
				// Don't clear help during the tutorial!!
				IF NOT bIsTutorial
					CLEAR_ALL_FLOATING_HELP()
				ENDIF
				
				RETURN FALSE
			ELSE
				// This target respawns! Clear our status, and set us back to entering.
				//sTargetEntity.iStatus = 0
				REMOVE_TARGET_STATUS(sTargetEntity, TS_ENTERING)
				REMOVE_TARGET_STATUS(sTargetEntity, TS_WOBBLING)
				REMOVE_TARGET_STATUS(sTargetEntity, TS_FLIPPED)
				REMOVE_TARGET_STATUS(sTargetEntity, TS_FLIPPING)
				REMOVE_TARGET_STATUS(sTargetEntity, TS_EXITING)
				
				ADD_TARGET_STATUS(sTargetEntity, TS_ENTERING)
				ADD_TARGET_STATUS(sTargetEntity, TS_CREATED)
				
				IF DOES_ENTITY_EXIST(sTargetEntity.target.objBacking)
					CLEAR_ENTITY_LAST_WEAPON_DAMAGE(sTargetEntity.target.objBacking)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sTargetEntity.target.objBacking)
				ENDIF
				CLEAR_ENTITY_LAST_WEAPON_DAMAGE(sTargetEntity.target.objTarget)
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sTargetEntity.target.objTarget)
			
				RESET_TARGET_DECALS(sTargetEntity)
				
				IF IS_ROUND_FLAG_SET(sRndInfo, ROUND_USE_FRAGGABLE_TARGETS)
					FIX_OBJECT_FRAGMENT(sTargetEntity.target.objTarget)
				ENDIF
				
				sTargetEntity.fTargCreateTime = (GET_GAME_TIMER() / 1000.0)	
				sTargetEntity.fHP = CONST_TARGET_HP
				sTargetEntity.fRotFrameTime = 0.0	
				sTargetEntity.fFlipFrameTime = 0.0	
				sTargetEntity.fWobbleCoeff = GET_RANDOM_FLOAT_IN_RANGE(WOBBLE_COEFF_MIN, WOBBLE_COEFF_MAX)
				sTargetEntity.fWobbleChange = GET_RANDOM_FLOAT_IN_RANGE(WOBBLE_COEFF_DEC_MIN, WOBBLE_COEFF_DEC_MAX)
				
				// Handle respawn time.
				IF CHECK_TARGET_STATUS(sTargetEntity, TS_RESPAWN_SHORT)
					sTargetEntity.fRespawnAtTime = (GET_GAME_TIMER() / 1000.0) + TARGET_RESPAWN_SHORT
				ELIF CHECK_TARGET_STATUS(sTargetEntity, TS_RESPAWN_LONG)
					sTargetEntity.fRespawnAtTime = (GET_GAME_TIMER() / 1000.0) + TARGET_RESPAWN_LONG
				ELSE
					sTargetEntity.fRespawnAtTime = (GET_GAME_TIMER() / 1000.0) + TARGET_RESPAWN_NORMAL
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// If we're wobbling, wobble.
	IF CHECK_TARGET_STATUS(sTargetEntity, TS_WOBBLING)
		IF WOBBLE_TARGET(sTargetEntity)
			//CDEBUG2LN(DEBUG_SHOOTRANGE, "UpdateTargetEntity == Done wobbling...")
			REMOVE_TARGET_STATUS(sTargetEntity, TS_WOBBLING)
		ENDIF
	ENDIF
	
	// Move as long as were not entering and we've bene told to
	IF CHECK_TARGET_STATUS(sTargetEntity, TS_MOVING) AND NOT CHECK_TARGET_STATUS(sTargetEntity, TS_ENTERING)
		IF MOVE_TARGET(sTargetEntity, sGridData)
			REMOVE_TARGET_STATUS(sTargetEntity, TS_MOVING)
		ENDIF
	ENDIF
	
	// Check if we need to be flipping
	IF CHECK_TARGET_STATUS(sTargetEntity, TS_FLIPPING)
		IF FLIP_TARGET(sTargetEntity)
			// Check if were flipped or not
			IF CHECK_TARGET_STATUS(sTargetEntity, TS_FLIPPED)
				REMOVE_TARGET_STATUS(sTargetEntity, TS_FLIPPED)
			ELSE
				ADD_TARGET_STATUS(sTargetEntity, TS_FLIPPED)
				
				// We just flipped, wipe all of our decals.
				RESET_TARGET_DECALS(sTargetEntity)
			ENDIF

			REMOVE_TARGET_STATUS(sTargetEntity, TS_FLIPPING)
		ENDIF
	ENDIF
	
	// Update the targets position/heading
	UPDATE_TARGET_POS_ORI(sTargetEntity)

	// Check if we've been hit.
	// Are we using the shotgun?
	CLEAR_ROUND_FLAG(sRndInfo, ROUND_TARGET_HIT)
	SWITCH (sRndInfo.SScoreData.eScoreStyle)
		CASE ROUNDSCORE_RANGE_REVERSE
			// Temp debug display to draw the score of a target next to it...
			//DRAW_TARGET_SCORE(sTargetEntity, sGridData)
		FALLTHRU
		
		CASE ROUNDSCORE_DESTRUCTION
		CASE ROUNDSCORE_DESTRUCTION_W_BONUS
			//DRAW_TARGET_SCORE(sTargetEntity, sGridData, FALSE)
			
			// Shotgun has both destruction and reverse range score styles.
			IF IS_ROUND_FLAG_SET(sRndInfo, ROUND_USE_FRAGGABLE_TARGETS)
				IF CHECK_TARGET_HIT_WITH_EXPLOSION(sTargetEntity, sRndInfo)
					SET_ROUND_FLAG(sRndInfo, ROUND_TARGET_HIT)
				ENDIF
			ELSE
				IF CHECK_TARGET_HIT_SHOTGUN(sTargetEntity, sRndInfo, sGridData, sSPTInfo)
					SET_ROUND_FLAG(sRndInfo, ROUND_TARGET_HIT)
				ENDIF
			ENDIF
			
		BREAK
		
		DEFAULT
			//DRAW_TARGET_SCORE(sTargetEntity, sGridData, FALSE)
			CHECK_TARGET_HIT(sTargetEntity, sRndInfo, sSPTInfo)
		BREAK
	ENDSWITCH
		
	// Destroy target once it's health is gone
	IF sTargetEntity.fHp <= 0.0 AND NOT CHECK_TARGET_STATUS(sTargetEntity, TS_EXITING)		
		// Increment our counter
		sRndInfo.iTargetsDestroyed++	
		
		// Play the sound of us exiting.
		PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetEntity.target.objArm)
		
		// If we're playing a moving sound, stop that.
		IF (sTargetEntity.iMovingSound != -1)
			STOP_SOUND(sTargetEntity.iMovingSound)
			RELEASE_SOUND_ID(sTargetEntity.iMovingSound)
			sTargetEntity.iMovingSound = -1
		ENDIF

		// We are exiting
		ADD_TARGET_STATUS(sTargetEntity, TS_EXITING)		
	ENDIF
	
	RETURN IS_ROUND_FLAG_SET(sRndInfo, ROUND_TARGET_HIT)
ENDFUNC


PROC EXIT_ALL_TARGETS(TARGET_ENTITY &sTargetEnts[])
	INT i = 0
	REPEAT iMAX_ENTITIES i
		REMOVE_TARGET_STATUS(sTargetEnts[i], TS_RESPAWNS)
		REMOVE_TARGET_STATUS(sTargetEnts[i], TS_RESPAWN_SHORT)
		REMOVE_TARGET_STATUS(sTargetEnts[i], TS_RESPAWN_LONG)
		IF CHECK_TARGET_STATUS(sTargetEnts[i], TS_CREATED) AND NOT CHECK_TARGET_STATUS(sTargetEnts[i], TS_DESTROYED)
			ADD_TARGET_STATUS(sTargetEnts[i], TS_EXITING)
			PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetEnts[i].target.objArm)
			// If we're playing a moving sound, stop that.
			IF (sTargetEnts[i].iMovingSound != -1)
				STOP_SOUND(sTargetEnts[i].iMovingSound)
				RELEASE_SOUND_ID(sTargetEnts[i].iMovingSound)
				sTargetEnts[i].iMovingSound = -1
			ENDIF
		ENDIF

	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Creates all the targets used in the tutorial.
///    Should probably add this to the Range Round Lib.
PROC CREATE_TUTORIAL_TARGETS(TARGET_ENTITY & sTargetEnts[], Range_RoundInfo & sRndInfo)
	// 10 Targets
	sRndInfo.iMaxTargets = 10
	sRndInfo.fCurrentRoundLength = 99	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP
	
	//single target
	SETUP_TARGET_ENTITY(sTargetEnts[0], gridA2)
	SETUP_TARGET_ENTITY(sTargetEnts[1], gridA4)
	SETUP_TARGET_ENTITY(sTargetEnts[2], gridA1)
	SETUP_TARGET_ENTITY(sTargetEnts[3], gridA5)
	
	// I will hack it so this one is created when I want it to be
	SETUP_TARGET_ENTITY(sTargetEnts[4], gridA3, 5)
	
	// Combo meter targets
	SETUP_TARGET_ENTITY(sTargetEnts[5], gridA1)
	SETUP_TARGET_ENTITY(sTargetEnts[6], gridA2)
	SETUP_TARGET_ENTITY(sTargetEnts[7], gridA4)
	SETUP_TARGET_ENTITY(sTargetEnts[8], gridA5)
	SETUP_TARGET_ENTITY(sTargetEnts[9], gridB3)
ENDPROC


/// PURPOSE:
///    Update the tutorial target entities.
/// PARAMS:
///    sRndInfo - 
///    sTargetEnts - 
///    bCombo - 
PROC UPDATE_TUTORIAL_TARGET_ENTITIES(Range_RoundInfo & sRndInfo, TARGET_ENTITY & sTargetEnts[], GRID_LOCATION & sGridInfo[], Range_SPTData & sSPTInfo, BOOL bCombo = FALSE)
	// The shooting tutorial section
	IF NOT bCombo
		// Create the targets when they need to be created...
		PROCESS_TARGET_CREATION(sRndInfo, sTargetEnts, sGridInfo)
		
		// update our two targets
		UPDATE_TARGET_ENTITY(sTargetEnts[0], sGridInfo, sRndInfo, sSPTInfo, TRUE)
		UPDATE_TARGET_ENTITY(sTargetEnts[1], sGridInfo, sRndInfo, sSPTInfo, TRUE)
		UPDATE_TARGET_ENTITY(sTargetEnts[2], sGridInfo, sRndInfo, sSPTInfo, TRUE)
		UPDATE_TARGET_ENTITY(sTargetEnts[3], sGridInfo, sRndInfo, sSPTInfo, TRUE)

	ELSE
		// Update the combo tutorial section
		UPDATE_TARGET_ENTITY(sTargetEnts[5], sGridInfo, sRndInfo, sSPTInfo, TRUE)
		UPDATE_TARGET_ENTITY(sTargetEnts[6], sGridInfo, sRndInfo, sSPTInfo, TRUE)
		UPDATE_TARGET_ENTITY(sTargetEnts[7], sGridInfo, sRndInfo, sSPTInfo, TRUE)
		UPDATE_TARGET_ENTITY(sTargetEnts[8], sGridInfo, sRndInfo, sSPTInfo, TRUE)
		UPDATE_TARGET_ENTITY(sTargetEnts[9], sGridInfo, sRndInfo, sSPTInfo, TRUE)
	ENDIF
ENDPROC


/// PURPOSE:
///   There was an issue where when a round ended, the target slide sound would continue to play.
///    This function fixes that. (Bug # 1349975)
PROC STOP_TARGET_SOUNDS(TARGET_ENTITY & sTargEnts[], INT iMaxTargets)
	INT index
	REPEAT iMaxTargets index
		IF (sTargEnts[index].iMovingSound != -1)
			STOP_SOUND(sTargEnts[index].iMovingSound)
			RELEASE_SOUND_ID(sTargEnts[index].iMovingSound)
			sTargEnts[index].iMovingSound = -1
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///   Loops through all of the queued up actions and check's if it needs
///    to perform the message or not.
PROC PROCESS_TARGET_ACTIONS(Range_RoundInfo & sRndInfo, Range_Targets & sTargetData, Range_RangeData & sRangeInfo)
	INT	iTer
	FLOAT fCurrentTime
	TARGET_ENTITY tempEntity
	
	// Don't worry if our time is up
	IF IS_ROUND_FLAG_SET(sRndInfo, ROUND_TIME_IS_UP)
		EXIT
	ENDIF
	
	//&***** OLD STUFF!
	// What time are we at
	// Current time = (Round Start time + Round Length) - How long we've been in this round
	// - GET_TIMER_IN_SECONDS(m_tTimeLeft)//m_fCurrRndLength - ( - m_fRndStartTime) //(m_fRndStartTime + m_fCurrRndLength) -  GET_TIMER_IN_SECONDS(m_tTimeLeft)
	//&***** OLD STUFF!

	// Get our time
	fCurrentTime = (GET_GAME_TIMER() / 1000.0)
	
	// Loop through all of our actions, see if
	// we need to act on something
	REPEAT sRndInfo.iMaxActions iTer
		// Have we performed this action yet?
		IF NOT sTargetData.sQueuedActions[iTer].bPerformed
			
			tempEntity = sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex]
			
			// What does this action want us to do
			SWITCH sTargetData.sQueuedActions[iTer].eBehavior
				// Flip the target
				CASE AB_FLIP
					// Has the target even been created yet?
					IF CHECK_TARGET_STATUS(tempEntity, TS_CREATED) AND NOT CHECK_TARGET_STATUS(tempEntity, TS_DESTROYED)
						// Has our target been around long enough?
						IF (fCurrentTime - (tempEntity.fTargCreateTime + sRndInfo.fTotalPauseTime)) >= sTargetData.sQueuedActions[iTer].fTime						
							// You can't flip if your already flipping
							IF NOT CHECK_TARGET_STATUS(tempEntity, TS_FLIPPING)
								// Were Flipping now
								ADD_TARGET_STATUS(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex], TS_FLIPPING)
								
								// PLAY SOUND: Flipping (this is rotating facing/not facing)
								PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", tempEntity.target.objArm)
								
								// If we are already flipped, this is up flipping back to face the player... clear decals
								IF CHECK_TARGET_STATUS(tempEntity, TS_FLIPPED)
									RESET_TARGET_DECALS(tempEntity)
									//REMOVE_DECALS_FROM_OBJECT(tempEntity.target.objBacking)
									//REMOVE_DECALS_FROM_OBJECT(tempEntity.target.objTarget)
								ENDIF
								
								// We have done this 
								sTargetData.sQueuedActions[iTer].bPerformed = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Move the target from one grid loc
				// to another
				CASE AB_MOVE
					// Has the target even been created yet? (-1 means no target was created)
					IF CHECK_TARGET_STATUS(tempEntity, TS_CREATED) AND NOT CHECK_TARGET_STATUS(tempEntity, TS_DESTROYED)
						// You can't move if your already moving
						IF NOT CHECK_TARGET_STATUS(tempEntity, TS_MOVING)						
							// Were moving now
							ADD_TARGET_STATUS(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex], TS_MOVING)
							
							// PLAY SOUND: Moving
							sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].iMovingSound = GET_SOUND_ID()
							PLAY_SOUND_FROM_ENTITY(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].iMovingSound, "TARGET_PRACTICE_SLIDE_MASTER", tempEntity.target.objArm)
		
							// Will use an incrmental index when we move multiple targets
							sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].fMoveFrame = 0.0 
							sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].vMoveStart = sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].target.vPos
							
							// Assign our move index
							//m_TargetEntities[m_QueuedActions[iTer].targetIndex].iMoveIndex = m_iMoveIndex
							
							// Set where we are moving too
							sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].eMoveDest = sTargetData.sQueuedActions[iter].eDestIndex
							
							// Set how long we will take to move
							sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].fMoveLength = sTargetData.sQueuedActions[iter].fTime
							
							// We have done this 
							sTargetData.sQueuedActions[iTer].bPerformed = TRUE	
						ENDIF
					ENDIF
				BREAK
				
				// Move a target from one grid loc
				// to another grid loc at a certain time
				CASE AB_MOVE_AT_TIME_LUDICROUS
				CASE AB_MOVE_AT_TIME_VERYFAST
				CASE AB_MOVE_AT_TIME_FAST
				CASE AB_MOVE_AT_TIME_NORMAL
				CASE AB_MOVE_AT_TIME_SLOW
				CASE AB_MOVE_AT_TIME_VERY_SLOW
					// Has the target even been created yet?
					IF CHECK_TARGET_STATUS(tempEntity, TS_CREATED) AND NOT CHECK_TARGET_STATUS(tempEntity, TS_DESTROYED)				
						// Has our target been around long enough?
						IF (fCurrentTime - (tempEntity.fTargCreateTime + sRndInfo.fTotalPauseTime)) >= sTargetData.sQueuedActions[iTer].fTime
							// You can't move if your already moving
							IF NOT CHECK_TARGET_STATUS(tempEntity, TS_MOVING)					
								// Were moving now
								ADD_TARGET_STATUS(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex], TS_MOVING)
								
								// PLAY SOUND: Moving
								IF NOT CHECK_TARGET_STATUS(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex], TS_DONT_PLAY_SLIDE_SOUND)
									sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].iMovingSound = GET_SOUND_ID()
									PLAY_SOUND_FROM_ENTITY(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].iMovingSound, "TARGET_PRACTICE_SLIDE_MASTER", tempEntity.target.objArm)
								ENDIF
								
								// Will use an incrmental index when we move multiple targets
								sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].fMoveFrame = 0.0 
								sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].vMoveStart = sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].target.vPos
								
								// Set where we are moving too
								sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].eMoveDest = sTargetData.sQueuedActions[iter].eDestIndex
								
								// Set how long we will take to move
								IF sTargetData.sQueuedActions[iTer].eBehavior = AB_MOVE_AT_TIME_VERYFAST
									sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].fMoveLength = MOVE_VERY_FAST_SPEED
									
								ELIF sTargetData.sQueuedActions[iTer].eBehavior = AB_MOVE_AT_TIME_FAST
									sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].fMoveLength = MOVE_FAST_SPEED
									
								ELIF sTargetData.sQueuedActions[iTer].eBehavior = AB_MOVE_AT_TIME_LUDICROUS
									sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].fMoveLength = MOVE_LUDICROUS_SPEED
									
								ELIF sTargetData.sQueuedActions[iTer].eBehavior = AB_MOVE_AT_TIME_SLOW
									sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].fMoveLength = MOVE_SLOW_SPEED
								
								ELIF sTargetData.sQueuedActions[iTer].eBehavior = AB_MOVE_AT_TIME_VERY_SLOW
									sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].fMoveLength = MOVE_VERY_SLOW_SPEED
										
								ELIF sTargetData.sQueuedActions[iTer].eBehavior = AB_MOVE_AT_TIME_NORMAL
									sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].fMoveLength = MOVE_NORMAL_SPEED
								ENDIF
								
								CDEBUG2LN(DEBUG_SHOOTRANGE, "ProcessActions -- MOVE_AT_TIME_*** -- Moving to : ", ENUM_TO_INT(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].eMoveDest))
								
								// We have done this 
								sTargetData.sQueuedActions[iTer].bPerformed = TRUE								
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Destroy the target when it reaches a certain location
				CASE AB_DESTROY_WHEN_AT_LOC
					// Has the target even been created yet?
					IF CHECK_TARGET_STATUS(tempEntity, TS_CREATED) AND NOT CHECK_TARGET_STATUS(tempEntity, TS_DESTROYED)
						//DEBUG_MESSAGE("ProcessActions -- AB_DESTROY_WHEN_AT_LOC -- Target is created and not destroyed..")
						// Wait till the target is rested before we destroy it
						IF NOT CHECK_TARGET_STATUS(tempEntity, TS_MOVING) AND NOT CHECK_TARGET_STATUS(tempEntity, TS_FLIPPING) AND NOT
								CHECK_TARGET_STATUS(tempEntity, TS_EXITING)
							//DEBUG_MESSAGE("ProcessActions -- AB_DESTROY_WHEN_AT_LOC -- Target is not moving or flipping..")
							// Check to see if we are at our grid loc
							IF (tempEntity.eGridIndex = sTargetData.sQueuedActions[iter].eDestIndex)
								// Increment our counter
								sRndInfo.iTargetsDestroyed++	
								
								// We are exiting
								ADD_TARGET_STATUS(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex], TS_EXITING)	
								
								// Play sound.
								PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].target.objArm)
								
								CDEBUG2LN(DEBUG_SHOOTRANGE, "ProcessActions -- AB_DESTROY_WHEN_AT_LOC -- Destroying target.")
								// We have done this 
								sTargetData.sQueuedActions[iTer].bPerformed = TRUE	
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Destroy the target after X seconds have passed in the round.
				CASE AB_DESTROY_AT_TIME
					IF CHECK_TARGET_STATUS(tempEntity, TS_CREATED) AND NOT CHECK_TARGET_STATUS(tempEntity, TS_DESTROYED) AND NOT
								CHECK_TARGET_STATUS(tempEntity, TS_EXITING)
						// If time's up...
						IF (fCurrentTime - sRndInfo.fStartTime) >= sTargetData.sQueuedActions[iTer].fTime
							// Increment our counter
							sRndInfo.iTargetsDestroyed++
							
							// We are exiting
							ADD_TARGET_STATUS(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex], TS_EXITING)	
							
							// Play sound.
							PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].target.objArm)
							
							CDEBUG2LN(DEBUG_SHOOTRANGE, "ProcessActions -- AB_DESTROY_AT_TIME -- Destroying target.")
							
							// We have done this 
							sTargetData.sQueuedActions[iTer].bPerformed = TRUE	
						ENDIF
					ENDIF
				BREAK
				
				// Destroys a target a certain amount of time after it's been created, as opposed to X time into the round.
				CASE AB_DESTROY_AFTER_TIME_CREATED
					// Has the target even been created yet?
					IF CHECK_TARGET_STATUS(tempEntity, TS_CREATED) AND NOT CHECK_TARGET_STATUS(tempEntity, TS_DESTROYED) AND NOT
								CHECK_TARGET_STATUS(tempEntity, TS_EXITING)				
						// Has our target been around long enough?
						IF (fCurrentTime - (tempEntity.fTargCreateTime + sRndInfo.fTotalPauseTime)) >= sTargetData.sQueuedActions[iTer].fTime
							// Increment our counter
							sRndInfo.iTargetsDestroyed++
							
							// We are exiting
							ADD_TARGET_STATUS(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex], TS_EXITING)	
							
							// Play sound.
							PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].target.objArm)
							CDEBUG2LN(DEBUG_SHOOTRANGE, "ProcessActions -- AB_DESTROY_AFTER_TIME_CREATED -- Destroying target.")
							
							// We have done this 
							sTargetData.sQueuedActions[iTer].bPerformed = TRUE	
						ENDIF
					ENDIF
				BREAK
				
				// Create a target at specific time after the round has begun
				CASE AB_CREATE_AT_TIME
					// Has the target even been created yet?
					IF NOT CHECK_TARGET_STATUS(tempEntity,TS_CREATED)
						// Has it been long enough to create the target?
						IF (fCurrentTime - sRndInfo.fStartTime) >= sTargetData.sQueuedActions[iTer].fTime
							// Create the target
							IF CREATE_TARGET(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex], sRndInfo, sRangeInfo, sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex].eGridIndex)
								// We've created the target
								ADD_TARGET_STATUS(sTargetData.sTargetEntities[sTargetData.sQueuedActions[iTer].iTargetIndex], TS_CREATED)
								CDEBUG2LN(DEBUG_SHOOTRANGE, "ProcessActions -- AB_CREATE_AT_TIME -- Target created.")
								
								// We have done this 
								sTargetData.sQueuedActions[iTer].bPerformed = TRUE	
							ENDIF								
						ENDIF						
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Adds an action to the queue that will be performed on a target once the target is created
/// PARAMS:
///    targetIndex - Which index in the target array should have this action performed on
///    fPerformTime - What time we want to perform the action (NOTE: If you use the move behavior, this will be how long you want
///    					it to take before it reaches it's destination. Delayed moving is not supported 'yet')
PROC TARGETACTION_CREATE_ACTION(Range_Targets & sTargData,  Range_RoundInfo & sRndData, INT iTargetIndex, FLOAT fPerformTime, ACTION_BEHAVIOR eBehavior, GRID_INDEX eGridDest = INVALID_LOC)
	// Can only have 
	IF (sTargData.iActionIndex >= iMAX_ACTIONS)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "ACTION_CreateAction -- Reached Action Limit. Speak to Ryan Paradis to increase limit.")
		EXIT
	ENDIF
	
	// Check if we are trying to add a perform time in the past
	IF (fPerformTime > sRndData.fCurrentRoundLength)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "ACTION_CreateAction -- Trying to add an action in the past. You may not see it!")
	ENDIF
	
	// Add our action to the queue
	sTargData.sQueuedActions[sTargData.iActionIndex].iTargetIndex 	= iTargetIndex
	sTargData.sQueuedActions[sTargData.iActionIndex].eBehavior		= eBehavior
	sTargData.sQueuedActions[sTargData.iActionIndex].fTime 			= fPerformTime 	

	// If we are doing a move action, grab the destination
	IF (eGridDest <> INVALID_LOC)
		sTargData.sQueuedActions[sTargData.iActionIndex].eDestIndex = eGridDest
	ENDIF

	// Increment our index
	sTargData.iActionIndex++
ENDPROC

