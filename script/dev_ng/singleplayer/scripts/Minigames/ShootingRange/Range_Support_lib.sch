// Range_Support.sch
USING "Range_Public.sch"
USING "Range_Range.sch"
USING "Range_Round.sch"
USING "Range_Core.sch"
USING "hud_drawing.sch"
USING "range_tutorial.sch"

/// PURPOSE:
///    Set the palyer's coords and heading.
PROC TELEPORT_PLAYER(VECTOR vCoords, FLOAT fHeading)
	// Teleport the ped to the following coords and pos
	SET_ENTITY_COORDS(PLAYER_PED_ID(), vCoords)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
ENDPROC


/// PURPOSE:
///    Gives the scripter the GRID_INDEX from the location passed in.
///    Does not take into acct height.
///    Relatively expensive function to call as it does, at most, 16 VDIST2() calls. 
///    It does not look at the sqrt'd distance. That'd be ludicrous.
/// RETURNS:
///    The GRID_INDEX of the vector. Can return INVALID_LOC!!
FUNC GRID_INDEX GET_RANGE_GRID_FROM_VECTOR(VECTOR vLocation, GRID_LOCATION & sRangeGrid[])
	vLocation.z = 0.0		// Don't want to take height into acct. 
	VECTOR vGridLoc			// Make a copy of the grid location so that we don't overwrite the height.
	
	INT index 
	FOR index = 0 TO (ENUM_TO_INT(MAX_GRID_LOCS) - 1)
		GRID_INDEX tempGridIndex = INT_TO_ENUM(GRID_INDEX, index)
		
		// Store the grid loc and zero out the height. No need to worry about that.
		vGridLoc = sRangeGrid[tempGridIndex].vLoc
		vGridLoc.z = 0.0
		
		// 1.0m seems to be about the sweet spot for this... may be a bit too big.
		IF VDIST2(vLocation, vGridLoc) <= 1.5
			RETURN tempGridIndex
		ENDIF
	ENDFOR
	
	RETURN INVALID_LOC
ENDFUNC


/// PURPOSE:
///    Given the round type enum, returns the string for the header.
/// RETURNS:
///    The string describing that round.
FUNC STRING GET_WEAPON_NAME_FROM_ROUND(RANGE_ROUND_TYPE eRndChoice)	
	SWITCH eRndChoice
		CASE RT_PISTOL_CHAL_1
		CASE RT_PISTOL_CHAL_2
		CASE RT_PISTOL_CHAL_3
			RETURN "SHR_CAT_HG"	
		BREAK 
		
		CASE RT_SMG_CHAL_1
		CASE RT_SMG_CHAL_2
		CASE RT_SMG_CHAL_3
			RETURN "SHR_CAT_SMG"
		BREAK 
		
		CASE RT_AR_CHAL_1
		CASE RT_AR_CHAL_2
		CASE RT_AR_CHAL_3
			RETURN "SHR_CAT_AR"
		BREAK
		
		CASE RT_SHOTGUN_CHAL_1
		CASE RT_SHOTGUN_CHAL_2
		CASE RT_SHOTGUN_CHAL_3
			RETURN "SHR_CAT_SG"
		BREAK
		
		CASE RT_LMG_CHAL_1
		CASE RT_LMG_CHAL_2
	 	CASE RT_LMG_CHAL_3
			RETURN "SHR_CAT_LMG"
		BREAK
		
		CASE RT_MINIGUN_CHAL_1
		CASE RT_MINIGUN_CHAL_2
		CASE RT_MINIGUN_CHAL_3
			RETURN "SHR_CAT_HVY"
		BREAK
		
		//NG Current Gen owners only.
		CASE RT_RAILGUN_CHAL_1
		CASE RT_RAILGUN_CHAL_2
		CASE RT_RAILGUN_CHAL_3
		CASE RT_RAILGUN_CHAL_4
			RETURN "SHR_CAT_RAILGUN"	
		BREAK 
		
	ENDSWITCH	
	
	RETURN "WEP STRING MISSING"
ENDFUNC


/// PURPOSE:
///    Checks to see if the player's gotten too far from the starting point. 
///    If so, he's left the range.
/// RETURNS:
///    TRUE when it's time to quit.
FUNC BOOL CHECK_RANGE_PLAYER_FAIL_CONDITIONS(Range_CoreData & sCoreInfo, Range_RangeData & sRangeInfo)
	// If we're actually in the game...
	IF (sCoreInfo.eRangeState >= SRM_PICK_WEAPON)
		UNUSED_PARAMETER(sRangeInfo)
		// See if we've tried to leave the range.
		// Commenting out, we're locking the door instead. B*1986623
//		IF NOT sCoreInfo.bWarnedAboutLeaving
//			IF GET_ENTITY_DISTANCE_FROM_LOCATION(rangePlayerPed, sRangeInfo.vDoorPos) < 4.5
//				sCoreInfo.bWarnedAboutLeaving = TRUE
//				PRINT_HELP("SHR_H_LEAVE")
//			ENDIF
//		ELSE
//			IF GET_ENTITY_DISTANCE_FROM_LOCATION(rangePlayerPed, sRangeInfo.vDoorPos) < 1.0
//				RETURN TRUE
//			ENDIF
//		ENDIF
		
		// See if we're just firing the wrong way.
		IF (sCoreInfo.eRangeState <= SRM_COMPLETE_MSG) AND (sCoreInfo.eRangeState > SRM_PICK_WEAPON)
			BOOL bMisfire
			IF (sCoreInfo.eRangeLoc = RANGELOC_PILLBOX_HILL)
				bMisfire = IS_BULLET_IN_ANGLED_AREA(<<6.488925,-1097.952881,32.773098>>, <<7.870516,-1094.140015,28.797018>>, 0.75, TRUE)
					OR IS_BULLET_IN_ANGLED_AREA(<<18.330799,-1102.255493,32.786949>>, <<19.692461,-1098.621948,28.797018>>, 0.5, TRUE)
					OR IS_BULLET_IN_ANGLED_AREA(<<6.473230,-1097.685669,32.782070>>, <<18.608915,-1102.111694,28.797018>>, 0.5, TRUE)
					OR IS_BULLET_IN_ANGLED_AREA(<<7.100925,-1095.961060,32.779907>>, <<19.176600,-1100.579590,32.119560>>, 4.25, TRUE)
					OR IS_BULLET_IN_ANGLED_AREA(<<6.869850,-1096.625366,28.797018>>, <<18.985115,-1101.054199,29.092524>>, 2.75, TRUE)
				
//				#IF IS_DEBUG_BUILD
//					DRAW_DEBUG_ANGLED_AREA(<<6.488925,-1097.952881,32.773098>>, <<7.870516,-1094.140015,28.797018>>, 0.75, 255, 0, 0, 255)
//					DRAW_DEBUG_ANGLED_AREA(<<18.330799,-1102.255493,32.786949>>, <<19.692461,-1098.621948,28.797018>>, 0.5, 0, 255, 0, 255)
//					DRAW_DEBUG_ANGLED_AREA(<<6.473230,-1097.685669,32.782070>>, <<18.608915,-1102.111694,28.797018>>, 0.5, 0, 0, 255, 255)
//					DRAW_DEBUG_ANGLED_AREA(<<7.100925,-1095.961060,32.779907>>, <<19.176600,-1100.579590,32.119560>>, 4.25, 0, 0, 0, 255)
//					DRAW_DEBUG_ANGLED_AREA(<<6.869850,-1096.625366,28.797018>>, <<18.985115,-1101.054199,29.092524>>, 2.75, 255, 255, 255, 255)
//				#ENDIF
			ELSE
				bMisfire = IS_BULLET_IN_ANGLED_AREA(<<829.218, -2160.619, 24.657>>, <<814.218, -2160.619, 34.657>>, 0.75, TRUE)
					OR IS_BULLET_IN_ANGLED_AREA(<<828.275, -2160.619, 24.657>>, <<827.525, -2160.619, 34.657>>, 5.0, TRUE)
					OR IS_BULLET_IN_ANGLED_AREA(<<815.575,  -2160.619, 24.657>>, <<814.825, -2160.619, 34.657>>, 5.0, TRUE)
					OR IS_BULLET_IN_ANGLED_AREA(<<829.375, -2161.619, 32.5>>, <<813.625, -2161.619, 33.7>>, 3.0, TRUE)
					OR IS_BULLET_IN_ANGLED_AREA(<<829.375, -2161.619, 27.5>>, <<813.625, -2161.619, 29.1>>, 3.0, TRUE)
					
//				#IF IS_DEBUG_BUILD
//					DRAW_DEBUG_ANGLED_AREA(<<829.218, -2160.619, 24.657>>, <<814.218, -2160.619, 34.657>>, 0.75, 255, 0, 0, 255)
//					DRAW_DEBUG_ANGLED_AREA(<<828.275, -2160.619, 24.657>>, <<827.525, -2160.619, 34.657>>, 5.0, 0, 255, 0, 255)
//					DRAW_DEBUG_ANGLED_AREA(<<815.575,  -2160.619, 24.657>>, <<814.825, -2160.619, 34.657>>, 5.0, 0, 0, 255, 255)
//					DRAW_DEBUG_ANGLED_AREA(<<829.375, -2161.619, 32.5>>, <<813.625, -2161.619, 33.7>>, 3.0, 0, 0, 0, 255)
//					DRAW_DEBUG_ANGLED_AREA(<<829.375, -2161.619, 27.5>>, <<813.625, -2161.619, 29.1>>, 3.0, 255, 255, 255, 255)
//				#ENDIF
			ENDIF

			IF bMisfire
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Player has fired into a restricted area! Printing help, and quitting!")
				sCoreInfo.bDelayShutdown = TRUE
				CLEAR_HELP()
				PRINT_HELP("SHR_H_MISFIRE")
				SET_MAX_WANTED_LEVEL(6)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				RANGE_SET_HAS_PLAYER_KICKED_OFF_IN_RANGE( sCoreInfo, TRUE )
				
				RETURN TRUE
			ENDIF
		ENDIF
		
		// Make sure player is alive.
		IF IS_ENTITY_DEAD(rangePlayerPed)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Returns the string information about the passed in range weapon.
///    Returns these items by reference.
PROC GET_WEAPON_STRING_INFO(RANGE_WEAPON_CATEGORY eWeaponCategory, RANGE_WEAPON_TYPE eWeapon, TEXT_LABEL_15 & sWeaponName)
	SWITCH (eWeaponCategory)
		CASE WEAPCAT_PISTOL
			IF (eWeapon = RANGE_PISTOL)
				sWeaponName = "WT_PIST"
			ELIF (eWeapon = RANGE_DLC_PISTOL50)
				sWeaponName = "WT_PIST_50"
			ELIF (eWeapon = RANGE_COMBATPISTOL)
				sWeaponName = "WT_PIST_CBT"
			ELIF (eWeapon = RANGE_APPISTOL)
				sWeaponName = "WT_PIST_AP"
			ENDIF
		BREAK
		
		CASE WEAPCAT_SMG
			IF (eWeapon = RANGE_MICROSMG)
				sWeaponName = "WT_SMG_MCR"
			ELIF (eWeapon = RANGE_SMG)
				sWeaponName = "WT_SMG"
			ELIF (eWeapon = RANGE_DLC_ASSAULTSMG)
				sWeaponName = "WT_SMG_ASL"		
			ENDIF
		BREAK
		
		CASE WEAPCAT_AR
			IF (eWeapon = RANGE_ASSAULTRIFLE)
				sWeaponName = "WT_RIFLE_ASL"
			ELIF (eWeapon = RANGE_CARBINERIFLE)
				sWeaponName = "WT_RIFLE_CBN"
			ELIF (eWeapon = RANGE_DLC_HEAVYRIFLE)
				sWeaponName = "WT_RIFLE_HVY"
			ELIF (eWeapon = RANGE_ADVANCEDRIFLE)
				sWeaponName = "WT_RIFLE_ADV"
			ENDIF
		BREAK
		
		CASE WEAPCAT_SHOTGUN
			IF (eWeapon = RANGE_PUMPSHOTGUN)
				sWeaponName = "WT_SG_PMP"
			ELIF (eWeapon = RANGE_SAWNOFFSHOTGUN)
				sWeaponName = "WT_SG_SOF"
			ELIF (eWeapon = RANGE_DLC_BULLPUPSHOTGUN)
				sWeaponName = "WT_SG_BLP"
			ELIF (eWeapon = RANGE_ASSAULTSHOTGUN)
				sWeaponName = "WT_SG_ASL"
			ENDIF
		BREAK
		
		CASE WEAPCAT_LIGHT_MG
			IF (eWeapon = RANGE_MG)
				sWeaponName = "WT_MG"
			ELIF (eWeapon = RANGE_COMBATMG)
				sWeaponName = "WT_MG_CBT"	
			ELIF (eWeapon = RANGE_DLC_ASSAULTMG)
				sWeaponName = "WT_MG_ASL"
			ENDIF
		BREAK
		
		CASE WEAPCAT_MINIGUN
			IF (eWeapon = RANGE_MINIGUN)
				sWeaponName = "WT_MINIGUN"
			
			ENDIF
		BREAK
		
		//NG Current gen owners only
		CASE WEAPCAT_RAILGUN
			IF (eWeapon = RANGE_RAILGUN)
				sWeaponName = "WT_RAILGUN"
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC STRING GET_WEAPON_UNLOCK_INFO(RANGE_WEAPON_CATEGORY eWeaponCategory, RANGE_WEAPON_TYPE eWeapon, BOOL bCGtoNG = FALSE)
	SWITCH (eWeaponCategory)
		CASE WEAPCAT_PISTOL
			IF (eWeapon = RANGE_PISTOL)
				RETURN ""
				
			ELIF (eWeapon = RANGE_DLC_PISTOL50)
				RETURN "SHR_WEPLK_P50"
				
			ELIF (eWeapon = RANGE_COMBATPISTOL)
				RETURN "SHR_WEPLK_PCB"
				
			ELIF (eWeapon = RANGE_APPISTOL)
				RETURN "SHR_WEPLK_PAP"
				
			ENDIF
			
		BREAK
		
		CASE WEAPCAT_SMG
			IF (eWeapon = RANGE_MICROSMG)
				RETURN "SHR_WEPLK_SMGM"
				
			ELIF (eWeapon = RANGE_SMG)
				RETURN "SHR_WEPLK_SMGS"
				
			ELIF (eWeapon = RANGE_DLC_ASSAULTSMG)
				RETURN "SHR_WEPLK_SMGA"

			ENDIF
		BREAK
		
		CASE WEAPCAT_AR
			IF (eWeapon = RANGE_ASSAULTRIFLE)
				RETURN "SHR_WEPLK_AR"
				
			ELIF (eWeapon = RANGE_CARBINERIFLE)
				RETURN "SHR_WEPLK_ARCR"
				
			ELIF (eWeapon = RANGE_DLC_HEAVYRIFLE)
				RETURN "SHR_WEPLK_ARHR"
				
			ELIF (eWeapon = RANGE_ADVANCEDRIFLE)
				RETURN "SHR_WEPLK_ARAR"

			ENDIF
		BREAK
		
		CASE WEAPCAT_SHOTGUN
			IF (eWeapon = RANGE_PUMPSHOTGUN)
				RETURN "SHR_WEPLK_SGPM"
				
			ELIF (eWeapon = RANGE_SAWNOFFSHOTGUN)
				RETURN "SHR_WEPLK_SGSW"
				
			ELIF (eWeapon = RANGE_DLC_BULLPUPSHOTGUN)
				RETURN "SHR_WEPLK_SGBP"
				
			ELIF (eWeapon = RANGE_ASSAULTSHOTGUN)
				RETURN "SHR_WEPLK_SGAS"
				
			ENDIF
		BREAK
		
		CASE WEAPCAT_LIGHT_MG
			IF (eWeapon = RANGE_MG)
				RETURN "SHR_WEPLK_MG"
				
			ELIF (eWeapon = RANGE_COMBATMG)
				RETURN "SHR_WEPLK_MGC"
				
			ELIF (eWeapon = RANGE_DLC_ASSAULTMG)
				RETURN "SHR_WEPLK_MGA"
			ENDIF
		BREAK
		
		CASE WEAPCAT_MINIGUN
			IF (eWeapon = RANGE_MINIGUN)
				IF bCGtoNG
					RETURN "SHR_WEPLK_MINI2"
				ELSE
					RETURN "SHR_WEPLK_MINI"
				ENDIF
			ENDIF	
		BREAK
		
		CASE WEAPCAT_RAILGUN
			//NG Current gen owners only
			IF (eWeapon = RANGE_RAILGUN)
				RETURN "SHR_WEPLK_RAIL"
			ENDIF
		BREAK
		
	ENDSWITCH
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Currently a stub with comments, Waiting on IS_WEAPON_VALID function to be created.
/// PARAMS:
PROC RANGE_CHECK_DLC(Range_CoreData &sCoreData)
	sCoreData.bPistolDLC	= IS_WEAPON_VALID(WEAPONTYPE_DLC_PISTOL50) 			AND IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_PISTOL50)
	sCoreData.bSMGDLC		= IS_WEAPON_VALID(WEAPONTYPE_DLC_ASSAULTSMG)		AND IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_ASSAULTSMG)
	sCoreData.bARDLC		= IS_WEAPON_VALID(WEAPONTYPE_DLC_HEAVYRIFLE)		AND IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_HEAVYRIFLE)
	sCoreData.bShotgunDLC	= IS_WEAPON_VALID(WEAPONTYPE_DLC_BULLPUPSHOTGUN)	AND IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_BULLPUPSHOTGUN)
	sCoreData.bLMGDLC		= IS_WEAPON_VALID(WEAPONTYPE_DLC_ASSAULTMG)			AND IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_ASSAULTMG)
	sCoreData.bCGtoNG		= IS_LAST_GEN_PLAYER()
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Checking DLC:")
	CDEBUG2LN(DEBUG_SHOOTRANGE, "sCoreData.bPistolDLC	= ", PICK_STRING(sCoreData.bPistolDLC, "TRUE", "FALSE"))
	CDEBUG2LN(DEBUG_SHOOTRANGE, "sCoreData.bSMGDLC		= ", PICK_STRING(sCoreData.bSMGDLC, "TRUE", "FALSE"))
	CDEBUG2LN(DEBUG_SHOOTRANGE, "sCoreData.bARDLC		= ", PICK_STRING(sCoreData.bARDLC, "TRUE", "FALSE"))
	CDEBUG2LN(DEBUG_SHOOTRANGE, "sCoreData.bShotgunDLC	= ", PICK_STRING(sCoreData.bShotgunDLC, "TRUE", "FALSE"))
	CDEBUG2LN(DEBUG_SHOOTRANGE, "sCoreData.bLMGDLC		= ", PICK_STRING(sCoreData.bLMGDLC, "TRUE", "FALSE"))
	CDEBUG2LN(DEBUG_SHOOTRANGE, "sCoreData.bCGtoNG		= ", PICK_STRING(sCoreData.bCGtoNG, "TRUE", "FALSE"))
ENDPROC

FUNC BOOL IS_RANGE_WEAPON_UNLOCKED(RANGE_WEAPON_CATEGORY eCategory, RANGE_WEAPON_TYPE eWeapon)
	BOOL bAcquired1, bAcquired2, bAcquired3
	INT iMichael, iFranklin, iTrevor
	
	//CDEBUG2LN(DEBUG_SHOOTRANGE, "IS_RANGE_WEAPON_UNLOCKED :: eCategory=", eCategory, ", eWeapon=", eWeapon)
	
	SWITCH (eCategory)
		CASE WEAPCAT_PISTOL
			IF (eWeapon = RANGE_PISTOL)
				RETURN TRUE
			
			ELIF (eWeapon = RANGE_DLC_PISTOL50)
				RETURN IS_WEAPON_VALID(WEAPONTYPE_DLC_PISTOL50) AND IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_PISTOL50)
				
			ELIF (eWeapon = RANGE_COMBATPISTOL)
				// If we've passed Challenge 2 in pistol, yes this is unlocked.
				IF IS_ROUND_PASSED(RT_PISTOL_CHAL_2)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* COMBATPISTOL UNLOCK: Pistol chal 2 passed.")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_CMBTPISTOL_HELDTIME, iMichael)
				STAT_GET_INT(SP1_CMBTPISTOL_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_CMBTPISTOL_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0

				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_PISTOL, eWeapon)))
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* COMBATPISTOL UNLOCK: Has been aquired")
					RETURN TRUE
				ENDIF
				
			ELIF (eWeapon = RANGE_APPISTOL)
				// If any of the pistol challenges are golded, yes this is unlocked.
				IF WAS_CHALLENGE_GOLDED(RT_PISTOL_CHAL_1) OR WAS_CHALLENGE_GOLDED(RT_PISTOL_CHAL_2) OR WAS_CHALLENGE_GOLDED(RT_PISTOL_CHAL_3)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* APPISTOL UNLOCK: Pistol challenge golded")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_APPISTOL_HELDTIME, iMichael)
				STAT_GET_INT(SP1_APPISTOL_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_APPISTOL_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0

				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_PISTOL, eWeapon)))
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* APPISTOL UNLOCK: Has been aquired")
					RETURN TRUE
				ENDIF

			ENDIF
			
		BREAK
		
		CASE WEAPCAT_SMG
			IF (eWeapon = RANGE_MICROSMG)
				// If we've passed Challenge 1 in pistol, yes this is unlocked.
				IF IS_ROUND_PASSED(RT_PISTOL_CHAL_1)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* MICROSMG UNLOCK: Pistol chal 1 passed.")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_MICROSMG_HELDTIME, iMichael)
				STAT_GET_INT(SP1_MICROSMG_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_MICROSMG_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0
				
//				CDEBUG2LN(DEBUG_SHOOTRANGE, "GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), WEAPONSLOT_MICROSMG)=", GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), WEAPONSLOT_MICROSMG))

				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_PISTOL, eWeapon)))
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* MICROSMG UNLOCK: Has been aquired")
					RETURN TRUE
				ENDIF	

			ELIF (eWeapon = RANGE_SMG)
				// If we've passed Challenge 2 in smg, yes this is unlocked.
				IF IS_ROUND_PASSED(RT_SMG_CHAL_2)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* SMG UNLOCK: SMG chal 2 passed.")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_SMG_HELDTIME, iMichael)
				STAT_GET_INT(SP1_SMG_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_SMG_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0
				
//				CDEBUG2LN(DEBUG_SHOOTRANGE, "GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), WEAPONSLOT_SMG)=", GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), WEAPONSLOT_SMG))

				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), WEAPONSLOT_SMG) = WEAPONTYPE_SMG)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* SMG UNLOCK: Has been aquired")
					RETURN TRUE
				ENDIF	
				
			ELIF (eWeapon = RANGE_DLC_ASSAULTSMG)
				RETURN IS_WEAPON_VALID(WEAPONTYPE_DLC_ASSAULTSMG) AND IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_ASSAULTSMG)
							

			ENDIF
		BREAK
		
		CASE WEAPCAT_AR
			IF (eWeapon = RANGE_ASSAULTRIFLE)
				// If we've passed Challenge 1 in SMG, yes this is unlocked.
				IF IS_ROUND_PASSED(RT_SMG_CHAL_1)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* ASSAULTRIFLE UNLOCK: SMG chal 1 passed.")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_ASLTRIFLE_HELDTIME, iMichael)
				STAT_GET_INT(SP1_ASLTRIFLE_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_ASLTRIFLE_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0
				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_AR, eWeapon)))
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* ASSAULTRIFLE UNLOCK: Has been  aquired")
					RETURN TRUE
				ENDIF
				
			ELIF (eWeapon = RANGE_CARBINERIFLE)
				// If we've passed Challenge 2 in AR, yes this is unlocked.
				IF IS_ROUND_PASSED(RT_AR_CHAL_2)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* CARBINERIFLE UNLOCK: AR chal 2 passed.")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_CRBNRIFLE_HELDTIME, iMichael)
				STAT_GET_INT(SP1_CRBNRIFLE_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_CRBNRIFLE_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0
				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_AR, eWeapon)))
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* CARBINERIFLE UNLOCK: Has been aquired")
					RETURN TRUE
				ENDIF
				
			ELIF (eWeapon = RANGE_DLC_HEAVYRIFLE)
				RETURN IS_WEAPON_VALID(WEAPONTYPE_DLC_HEAVYRIFLE) AND IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_HEAVYRIFLE)
				
			ELIF (eWeapon = RANGE_ADVANCEDRIFLE)
				// If any of the AR challenges are golded, yes this is unlocked.
				IF WAS_CHALLENGE_GOLDED(RT_AR_CHAL_1) OR WAS_CHALLENGE_GOLDED(RT_AR_CHAL_2) OR WAS_CHALLENGE_GOLDED(RT_AR_CHAL_3)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* ADVANCEDRIFLE UNLOCK: AR chal golded")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_ADVRIFLE_HELDTIME, iMichael)
				STAT_GET_INT(SP1_ADVRIFLE_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_ADVRIFLE_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0
				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_AR, eWeapon)))
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* ADVANCEDRIFLE UNLOCK: Has been aquired")
					RETURN TRUE
				ENDIF
			
			ENDIF
		BREAK
		
		CASE WEAPCAT_SHOTGUN
			IF (eWeapon = RANGE_PUMPSHOTGUN)
				// If we've passed Challenge 1 in AR, yes this is unlocked.
				IF IS_ROUND_PASSED(RT_AR_CHAL_1)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* PUMP UNLOCK: AR chal 1 passed.")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_PUMP_HELDTIME, iMichael)
				STAT_GET_INT(SP1_PUMP_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_PUMP_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0
				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_SHOTGUN, eWeapon)))
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* PUMP UNLOCK: Has been aquired")
					RETURN TRUE
				ENDIF
				
			ELIF (eWeapon = RANGE_SAWNOFFSHOTGUN)
				// If we've passed Challenge 2 in SG, yes this is unlocked.
				IF IS_ROUND_PASSED(RT_SHOTGUN_CHAL_2)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* SAWNOFFSHOTGUN UNLOCK: Shotgun chal 2 passed.")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_SAWNOFF_HELDTIME, iMichael)
				STAT_GET_INT(SP1_SAWNOFF_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_SAWNOFF_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0
				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_SHOTGUN, eWeapon)))
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* SAWNOFFSHOTGUN UNLOCK: Has been aquired")
					RETURN TRUE
				ENDIF
				
			ELIF (eWeapon = RANGE_DLC_BULLPUPSHOTGUN)
				RETURN IS_WEAPON_VALID(WEAPONTYPE_DLC_BULLPUPSHOTGUN) AND IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_BULLPUPSHOTGUN)
				
			ELIF (eWeapon = RANGE_ASSAULTSHOTGUN)
				// If any of the SG challenges are golded, yes this is unlocked.
				IF WAS_CHALLENGE_GOLDED(RT_SHOTGUN_CHAL_1) OR WAS_CHALLENGE_GOLDED(RT_SHOTGUN_CHAL_2) OR WAS_CHALLENGE_GOLDED(RT_SHOTGUN_CHAL_3)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* ASSAULTSHOTGUN UNLOCK: Shotgun chal golded")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_ASLTSHTGN_HELDTIME, iMichael)
				STAT_GET_INT(SP1_ASLTSHTGN_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_ASLTSHTGN_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0
				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_SHOTGUN, eWeapon)))
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* ASSAULTSHOTGUN UNLOCK: Has been aquired")
					RETURN TRUE
				ENDIF
			
			ENDIF
		BREAK
		
		CASE WEAPCAT_LIGHT_MG
			IF (eWeapon = RANGE_MG)
				// If we've passed Challenge 1 in shotgun, yes this is unlocked.
				IF IS_ROUND_PASSED(RT_SHOTGUN_CHAL_1)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* MG UNLOCK: Shotgun chal 1 passed.")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_MG_HELDTIME, iMichael)
				STAT_GET_INT(SP1_MG_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_MG_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0
				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_LIGHT_MG, eWeapon)))
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* MG UNLOCK: Has been aquired")
					RETURN TRUE
				ENDIF	
				
			ELIF (eWeapon = RANGE_COMBATMG)
				// If we've passed Challenge 2 in lmg, yes this is unlocked.
				IF IS_ROUND_PASSED(RT_LMG_CHAL_2)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* COMBATMG UNLOCK: LMG chal 2 passed.")
					RETURN TRUE
				ENDIF
				
				// Have we ever owned it?
				STAT_GET_INT(SP0_CMBTMG_HELDTIME, iMichael)
				STAT_GET_INT(SP1_CMBTMG_HELDTIME, iFranklin)
				STAT_GET_INT(SP2_CMBTMG_HELDTIME, iTrevor)
				bAcquired1 = iMichael > 0
				bAcquired2 = iFranklin > 0
				bAcquired3 = iTrevor > 0
				IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_LIGHT_MG, eWeapon)))
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "* COMBATMG UNLOCK: Has been aquired")
					RETURN TRUE
				ENDIF	
				
			ELIF (eWeapon = RANGE_DLC_ASSAULTMG)
				RETURN IS_WEAPON_VALID(WEAPONTYPE_DLC_ASSAULTMG) AND IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_ASSAULTMG)
			ENDIF
		BREAK
		
		CASE WEAPCAT_MINIGUN
			// Only one weapon to check for in here, really.
			IF IS_ROUND_PASSED(RT_PISTOL_CHAL_1) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_2) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_3)
					AND IS_ROUND_PASSED(RT_SMG_CHAL_1) AND IS_ROUND_PASSED(RT_SMG_CHAL_2) AND IS_ROUND_PASSED(RT_SMG_CHAL_3)
					AND IS_ROUND_PASSED(RT_AR_CHAL_1) AND IS_ROUND_PASSED(RT_AR_CHAL_2) AND IS_ROUND_PASSED(RT_AR_CHAL_3)
					AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_1) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_2) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_3)
					AND IS_ROUND_PASSED(RT_LMG_CHAL_1) AND IS_ROUND_PASSED(RT_LMG_CHAL_2) AND IS_ROUND_PASSED(RT_LMG_CHAL_3)
				RETURN TRUE
			ENDIF
			
			// Have we ever owned it?
			STAT_GET_INT(SP0_MINIGUNS_HELDTIME, iMichael)
			STAT_GET_INT(SP1_MINIGUNS_HELDTIME, iFranklin)
			STAT_GET_INT(SP2_MINIGUNS_HELDTIME, iTrevor)
			bAcquired1 = iMichael > 0
			bAcquired2 = iFranklin > 0
			bAcquired3 = iTrevor > 0
			IF (bAcquired1 OR bAcquired2 OR bAcquired3 OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_MINIGUN, eWeapon)))
				RETURN TRUE
			ENDIF
		BREAK
		
		//NG Current gen owners only
		CASE WEAPCAT_RAILGUN
//			IF IS_ROUND_PASSED(RT_PISTOL_CHAL_1) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_2) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_3)
//			AND IS_ROUND_PASSED(RT_SMG_CHAL_1) AND IS_ROUND_PASSED(RT_SMG_CHAL_2) AND IS_ROUND_PASSED(RT_SMG_CHAL_3)
//			AND IS_ROUND_PASSED(RT_AR_CHAL_1) AND IS_ROUND_PASSED(RT_AR_CHAL_2) AND IS_ROUND_PASSED(RT_AR_CHAL_3)
//			AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_1) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_2) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_3)
//			AND IS_ROUND_PASSED(RT_LMG_CHAL_1) AND IS_ROUND_PASSED(RT_LMG_CHAL_2) AND IS_ROUND_PASSED(RT_LMG_CHAL_3) 
//				RETURN TRUE
//			ENDIF
//			
//			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_RANGE_INFO(WEAPCAT_RAILGUN, eWeapon))
				RETURN TRUE
//			ENDIF
			
		BREAK
		
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC RANGE_ROUND_TYPE GET_RANGE_ROUND_FROM_CAT_AND_CHAL(RANGE_WEAPON_CATEGORY eCategory, RANGE_WEAPON_CHALLENGES eChallenge)
	RANGE_ROUND_TYPE eRetVal = RT_INVALID
	
	SWITCH (eCategory)
		CASE WEAPCAT_PISTOL
			IF (eChallenge = WEAPCHAL_1)
				eRetVal = RT_PISTOL_CHAL_1
			ELIF (eChallenge = WEAPCHAL_2)
				eRetVal = RT_PISTOL_CHAL_2
			ELIF (eChallenge = WEAPCHAL_3)
				eRetVal = RT_PISTOL_CHAL_3
			ENDIF
		BREAK
		
		CASE WEAPCAT_SMG
			IF (eChallenge = WEAPCHAL_1)
				eRetVal = RT_SMG_CHAL_1
			ELIF (eChallenge = WEAPCHAL_2)
				eRetVal = RT_SMG_CHAL_2
			ELIF (eChallenge = WEAPCHAL_3)
				eRetVal = RT_SMG_CHAL_3
			ENDIF
		BREAK
		
		CASE WEAPCAT_AR
			IF (eChallenge = WEAPCHAL_1)
				eRetVal = RT_AR_CHAL_1
			ELIF (eChallenge = WEAPCHAL_2)
				eRetVal = RT_AR_CHAL_2
			ELIF (eChallenge = WEAPCHAL_3)
				eRetVal = RT_AR_CHAL_3
			ENDIF
		BREAK
		
		CASE WEAPCAT_SHOTGUN
			IF (eChallenge = WEAPCHAL_1)
				eRetVal = RT_SHOTGUN_CHAL_1
			ELIF (eChallenge = WEAPCHAL_2)
				eRetVal = RT_SHOTGUN_CHAL_2
			ELIF (eChallenge = WEAPCHAL_3)
				eRetVal = RT_SHOTGUN_CHAL_3
			ENDIF
		BREAK
		
		CASE WEAPCAT_LIGHT_MG
			IF (eChallenge = WEAPCHAL_1)
				eRetVal = RT_LMG_CHAL_1
			ELIF (eChallenge = WEAPCHAL_2)
				eRetVal = RT_LMG_CHAL_2
			ELIF (eChallenge = WEAPCHAL_3)
				eRetVal = RT_LMG_CHAL_3
			ENDIF
		BREAK
		
		CASE WEAPCAT_MINIGUN
			IF (eChallenge = WEAPCHAL_1)
				eRetVal = RT_MINIGUN_CHAL_1
			ELIF (eChallenge = WEAPCHAL_2)
				eRetVal = RT_MINIGUN_CHAL_2
			ELIF (eChallenge = WEAPCHAL_3)
				eRetVal = RT_MINIGUN_CHAL_3
			ENDIF
		BREAK
		
		//NG Current gen owners only
		CASE WEAPCAT_RAILGUN
			IF (eChallenge = WEAPCHAL_1)
				eRetVal = RT_RAILGUN_CHAL_1
			ELIF (eChallenge = WEAPCHAL_2)
				eRetVal = RT_RAILGUN_CHAL_2
			ELIF (eChallenge = WEAPCHAL_3)
				eRetVal = RT_RAILGUN_CHAL_3
			ELIF (eChallenge = WEAPCHAL_4)
				eRetVal = RT_RAILGUN_CHAL_4
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN eRetVal
ENDFUNC


// Set the camera heading to look at the range.
PROC RANGE_SET_PLAYER_CAM_DOWNRANGE(Range_RangeData & sRangeInfo)
	IF NOT IS_ENTITY_DEAD(rangePlayerPed)
		FLOAT fPlayerHead = GET_ENTITY_HEADING(rangePlayerPed)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(sRangeInfo.fPlayerRootHead - fPlayerHead)
	ENDIF
ENDPROC

USING "player_ped_public.sch"

/// PURPOSE:
///    Wrapper for REMOVE_ALL_PED_WEAPONS
PROC RANGE_REMOVE_ALL_PED_WEAPONS()
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_REMOVE_ALL_PED_WEAPONS called")
	REMOVE_ALL_PED_WEAPONS(rangePlayerPed)
ENDPROC

/// PURPOSE:
///    Stores all the weapons that the player currently has.
/// PARAMS:
///    bRemove - Should we also remove the weapons?
PROC RANGE_STORE_PLAYER_WEAPONS(PED_WEAPONS_STRUCT &sWeapons)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STORE_PLAYER_WEAPONS called")
	// Store every weapon slot.
	IF NOT IS_ENTITY_DEAD(rangePlayerPed)
		GET_PED_WEAPONS(rangePlayerPed, sWeapons)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives the player back all of the weapons he had when he started the range, complete with that ammo state as well.
PROC RANGE_RESTORE_PLAYER_WEAPONS(PED_WEAPONS_STRUCT &sWeapons)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_RESTORE_PLAYER_WEAPONS called")
	IF NOT IS_ENTITY_DEAD(rangePlayerPed)
		RANGE_REMOVE_ALL_PED_WEAPONS()
		
		SET_PED_WEAPONS(rangePlayerPed, sWeapons)
	ENDIF
ENDPROC

PROC RANGE_ACCURACY_CLAMP(Range_RoundInfo & sRndInfo)
	INT iTotalHits = sRndInfo.iZ1Hits + sRndInfo.iZ2Hits + sRndInfo.iZ3Hits + sRndInfo.iZ4Hits
	IF (iTotalHits > sRndInfo.iTotalShots)
		sRndInfo.iTotalShots = iTotalHits
		CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_ACCURACY_CLAMP: Clamping total shots to iTotalHits :: ", sRndInfo.iTotalShots)
	ENDIF
ENDPROC

/// PURPOSE:
///    Runs through all weapons in a category to see if any are "new"
FUNC BOOL DOES_WEAPON_CATEGORY_CONTAIN_ANY_NEW_WEAPONS(RANGE_WEAPON_CATEGORY eCategory)
	INT iWeaponsInCat
	SWITCH eCategory
		CASE WEAPCAT_PISTOL		iWeaponsInCat = 4	BREAK
		CASE WEAPCAT_SMG		iWeaponsInCat = 3	BREAK
		CASE WEAPCAT_AR			iWeaponsInCat = 4	BREAK
		CASE WEAPCAT_SHOTGUN	iWeaponsInCat = 4	BREAK
		CASE WEAPCAT_LIGHT_MG	iWeaponsInCat = 3	BREAK
		CASE WEAPCAT_MINIGUN	iWeaponsInCat = 1	BREAK
		//NG Current gen owners only
		CASE WEAPCAT_RAILGUN	iWeaponsInCat = 1	BREAK
	ENDSWITCH
	
	INT iWeaponIdx
	REPEAT iWeaponsInCat iWeaponIdx
		INT iWeaponBit = (ENUM_TO_INT(eCategory) * 4) + iWeaponIdx
		
		// If we've never selected it before, we need to draw the star.
//		CDEBUG2LN(DEBUG_SHOOTRANGE, "Checking Cat: ", eCategory, " Weapon: ", iWeaponIdx, " Bit: ", iWeaponBit)
		IF NOT IS_BIT_SET(g_savedGlobals.sRangeData[ePlayerChar].iWeaponLockStatus, iWeaponBit)
//			CDEBUG2LN(DEBUG_SHOOTRANGE, "Bit wasn't set, you should see a star next to weapon: ", iWeaponIdx)
			
			// If this weapon is unlocked, and the bit isn't set, that meanst it's new. Return 
			IF IS_RANGE_WEAPON_UNLOCKED(eCategory, INT_TO_ENUM(RANGE_WEAPON_TYPE, iWeaponIdx))
//				CDEBUG2LN(DEBUG_SHOOTRANGE, "There was an unlocked weapon in cat: ", eCategory, " that hadn't had a weapon selected. Star this cat.")
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    
///    After everythign is streamed in, do these things
PROC RANGE_SET_PLAYER_PED_COMPONENTS(Range_CoreData & sCoreInfo)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "PostStreamInit :: Putting Safety Gear on ped")
	STORE_PLAYER_PED_VARIATIONS(rangePlayerPed, TRUE)
	
	// Store specifically what's on his head.
	sCoreInfo.iStartHeadPropIdx = GET_PED_PROP_INDEX(rangePlayerPed, ANCHOR_HEAD)
	sCoreInfo.iStartFacePropIdx = GET_PED_PROP_INDEX(rangePlayerPed, ANCHOR_EYES)
	
	CLEAR_PED_PROP(rangePlayerPed, ANCHOR_HEAD)
	CLEAR_PED_PROP(rangePlayerPed, ANCHOR_EYES)
	
	sCoreInfo.oEarmuffs = CREATE_OBJECT(PROP_EAR_DEFENDERS_01, GET_ENTITY_COORDS(rangePlayerPed))
	ATTACH_ENTITY_TO_ENTITY(sCoreInfo.oEarmuffs, rangePlayerPed, GET_PED_BONE_INDEX(rangePlayerPed, BONETAG_HEAD), 
		EARMUFFS_OFFSET, EARMUFFS_ROT)
		
	IF DOES_ENTITY_EXIST(sCoreInfo.oEarmuffs)
		SET_ENTITY_VISIBLE(sCoreInfo.oEarmuffs, FALSE)
	ENDIF
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			SET_PED_PROP_INDEX(rangePlayerPed, ANCHOR_HEAD, 3, 0)
			SET_PED_PROP_INDEX(rangePlayerPed, ANCHOR_EYES, 10, 0)
		BREAK
		CASE CHAR_FRANKLIN
			SET_PED_PROP_INDEX(rangePlayerPed, ANCHOR_HEAD, 1, 0)
			SET_PED_PROP_INDEX(rangePlayerPed, ANCHOR_EYES, 11, 0)
		BREAK
		CASE CHAR_TREVOR
			SET_PED_PROP_INDEX(rangePlayerPed, ANCHOR_HEAD, 3, 0)
			SET_PED_PROP_INDEX(rangePlayerPed, ANCHOR_EYES, 11, 0)
		BREAK
	ENDSWITCH
ENDPROC
