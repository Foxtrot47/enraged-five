USING "Range_Score.sch"
USING "Range_Round.sch"
USING "commands_stats.sch"
USING "net_prints.sch"
USING "socialclub_leaderboard.sch"
USING "net_leaderboards.sch"

SC_LEADERBOARD_CONTROL_STRUCT rangeLBControl
CONST_INT	k_RANGE_SCLB_ERROR_CHECK_MULT		10

FUNC BOOL RANGE_SHOULD_SCLB_SHOW_PROFILE_BUTTON()
	RETURN SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(rangeLBControl)
ENDFUNC

FUNC INT GET_RANGE_SP_FMMC_TYPE_OFF_ROUND_TYPE(RANGE_ROUND_TYPE eRoundType)	
	SWITCH eRoundType
		CASE RT_PISTOL_CHAL_1
			RETURN (FMMC_TYPE_SP_RANGE_PISTOL_1)	
		BREAK																	 
		CASE RT_PISTOL_CHAL_2													 
			RETURN (FMMC_TYPE_SP_RANGE_PISTOL_2)	
		BREAK																	 
		CASE RT_PISTOL_CHAL_3													 
			RETURN (FMMC_TYPE_SP_RANGE_PISTOL_3)	
		BREAK																	 
		CASE RT_SMG_CHAL_1														 
			RETURN (FMMC_TYPE_SP_RANGE_SMG_1)		
		BREAK																	 
		CASE RT_SMG_CHAL_2																
			RETURN (FMMC_TYPE_SP_RANGE_SMG_2)		
		BREAK																			
		CASE RT_SMG_CHAL_3																
			RETURN (FMMC_TYPE_SP_RANGE_SMG_3)		
		BREAK																			
		CASE RT_SHOTGUN_CHAL_1															
			RETURN (FMMC_TYPE_SP_RANGE_SHOTGUN_1)	
		BREAK																			
		CASE RT_SHOTGUN_CHAL_2															
			RETURN (FMMC_TYPE_SP_RANGE_SHOTGUN_2)	
		BREAK																			
		CASE RT_SHOTGUN_CHAL_3													
			RETURN (FMMC_TYPE_SP_RANGE_SHOTGUN_3)			
		BREAK																	
		CASE RT_AR_CHAL_1														
			RETURN (FMMC_TYPE_SP_RANGE_AR_1)			
		BREAK																	
		CASE RT_AR_CHAL_2														
			RETURN (FMMC_TYPE_SP_RANGE_AR_2)			
		BREAK																	
		CASE RT_AR_CHAL_3														
			RETURN (FMMC_TYPE_SP_RANGE_AR_3)			
		BREAK																	
		CASE RT_LMG_CHAL_1
			RETURN (FMMC_TYPE_SP_RANGE_LMG_1)
		BREAK
		CASE RT_LMG_CHAL_2
			RETURN (FMMC_TYPE_SP_RANGE_LMG_2)
		BREAK
		CASE RT_LMG_CHAL_3
			RETURN (FMMC_TYPE_SP_RANGE_LMG_3)
		BREAK
		CASE RT_MINIGUN_CHAL_1
			RETURN (FMMC_TYPE_SP_RANGE_HEAVY_1)
		BREAK
		CASE RT_MINIGUN_CHAL_2
			RETURN (FMMC_TYPE_SP_RANGE_HEAVY_2)
		BREAK
		CASE RT_MINIGUN_CHAL_3
			RETURN (FMMC_TYPE_SP_RANGE_HEAVY_3)
		BREAK
		CASE RT_RAILGUN_CHAL_1
			RETURN (FMMC_TYPE_SP_RANGE_RAILGUN_1)
		BREAK
		CASE RT_RAILGUN_CHAL_2
			RETURN (FMMC_TYPE_SP_RANGE_RAILGUN_2)
		BREAK
		CASE RT_RAILGUN_CHAL_3
			RETURN (FMMC_TYPE_SP_RANGE_RAILGUN_3)
		BREAK
		CASE RT_RAILGUN_CHAL_4
			RETURN (FMMC_TYPE_SP_RANGE_RAILGUN_4)
		BREAK
	ENDSWITCH
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Default case hit for leaderboard, contact Rob Pearsall!!!")
	RETURN (FMMC_TYPE_SP_RANGE_PISTOL_1)
ENDFUNC

PROC READ_RANGE_SP_SOCIAL_CLUB_LEADERBOARD(RANGE_ROUND_TYPE eRoundType)
	SC_LEADERBOARD_CACHE_CLEAR_ALL()

	INT iFMMCType = GET_RANGE_SP_FMMC_TYPE_OFF_ROUND_TYPE(eRoundType)
	TEXT_LABEL_31 sIdentifiers = "RoundType"
	sIdentifiers += ENUM_TO_INT(eRoundType)
	SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(rangeLBControl, iFMMCType, sIdentifiers, "", -1, -1, FALSE, TRUE)
ENDPROC

PROC RANGE_SP_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(rangeLBControl)
	SC_LEADERBOARD_CACHE_CLEAR_ALL()
ENDPROC

PROC SP_RANGE_DISPLAY_SOCIAL_CLUB_LEADERBOARD(SCALEFORM_INDEX &uiLeaderboard, RANGE_ROUND_TYPE eRoundType, RANGE_LBD_WRITE_DATA & lbdWrite, BOOL bPredicted)
	CDEBUG1LN(DEBUG_SHOOTRANGE, "SP_RANGE_DISPLAY_SOCIAL_CLUB_LEADERBOARD - ", eRoundType, " :: ", lbdWrite.eRoundType)
	
	// If this isn't the one we read, read it now.
	IF (eRoundType != lbdWrite.eRoundType) OR (lbdWrite.eRoundType = RT_INVALID)
		RANGE_SP_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
		
		CDEBUG1LN(DEBUG_SHOOTRANGE, "Last read leaderboard and this one don't match! Triggering read for: ", GET_STRING_FROM_RANGE_ROUND_TYPE(eRoundType) )
		READ_RANGE_SP_SOCIAL_CLUB_LEADERBOARD(eRoundType)
		lbdWrite.eRoundType = eRoundType
		EXIT
	ENDIF
	
	sclb_useRankPrediction = bPredicted
	DRAW_SC_SCALEFORM_LEADERBOARD(uiLeaderboard, rangeLBControl)
ENDPROC

/// PURPOSE:
///    Writes data to the leaderboard based on the roundtype in the lbdWrite data
/// PARAMS:
///    lbdWrite - The write struct for the leaderboard.
///    weaponRounds - Used for error checking the scores against gold requirements
PROC WRITE_TO_RANGE_SP_HIGH_SCORE_LEADERBOARD(RANGE_LBD_WRITE_DATA & lbdWrite)
	TEXT_LABEL_31 categoryName[1]
	categoryName[0] = "Type"
	TEXT_LABEL_23 sIdentifier[1]
	sIdentifier[0] = "RoundType"
	sIdentifier[0] += ENUM_TO_INT(lbdWrite.eRoundType)
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "WRITE_TO_RANGE_SP_HIGH_SCORE_LEADERBOARD Calling Init for ", sIdentifier[0] )
	
	IF INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_SRANGE, sIdentifier, categoryName, 1, DEFAULT, TRUE)
		
		//Give each weapontype a unique number off the round and it's own weaponType
		INT iWeaponTypeAdd
		IF lbdWrite.eRoundType > RT_MINIGUN_CHAL_3
			iWeaponTypeAdd = 600	// 600 for railguns
		ELIF lbdWrite.eRoundType > RT_LMG_CHAL_3
			iWeaponTypeAdd = 500	// 500 for miniguns
		ELIF lbdWrite.eRoundType > RT_AR_CHAL_3
			iWeaponTypeAdd = 400	// 400 for LMGs
		ELIF lbdWrite.eRoundType > RT_SHOTGUN_CHAL_3
			iWeaponTypeAdd = 300	// 300 for ARs
		ELIF lbdWrite.eRoundType > RT_SMG_CHAL_3
			iWeaponTypeAdd = 200	// 200 for Shotguns
		ELIF lbdWrite.eRoundType > RT_PISTOL_CHAL_3
			iWeaponTypeAdd = 100	// 100 for SMGs
		ENDIF
		
		INT iWeaponTypeUsed = ENUM_TO_INT(g_savedGlobals.sRangeData[ePlayerChar].sRounds[lbdWrite.eRoundType].eWeaponType) + iWeaponTypeAdd
		
		INT iHighScore = GET_RANGE_SP_HIGH_SCORE(ePlayerChar, lbdWrite.eRoundType)
		INT iGoldReq = RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(lbdWrite.eRoundType, RRM_GOLD)
		
		IF iHighScore > (iGoldReq * k_RANGE_SCLB_ERROR_CHECK_MULT)
			
			CPRINTLN(DEBUG_SHOOTRANGE, "WRITE_TO_RANGE_SP_HIGH_SCORE_LEADERBOARD m_Id <> sIdentifier[0]!")
			CPRINTLN(DEBUG_SHOOTRANGE, "lbdWrite.eRoundType  = ", GET_STRING_FROM_RANGE_ROUND_TYPE(lbdWrite.eRoundType) )
			CPRINTLN(DEBUG_SHOOTRANGE, "ePlayerChar          = ", ePlayerChar )
			CPRINTLN(DEBUG_SHOOTRANGE, "eWeaponType          = ", g_savedGlobals.sRangeData[ePlayerChar].sRounds[lbdWrite.eRoundType].eWeaponType )
			CPRINTLN(DEBUG_SHOOTRANGE, "iWeaponTypeAdd       = ", iWeaponTypeAdd )
			CPRINTLN(DEBUG_SHOOTRANGE, "iWeaponTypeUsed      = ", iWeaponTypeUsed )
			CPRINTLN(DEBUG_SHOOTRANGE, "iHighScore           = ", iHighScore )
			CPRINTLN(DEBUG_SHOOTRANGE, "iGoldReq             = ", iGoldReq )
			CPRINTLN(DEBUG_SHOOTRANGE, "lbdWrite.iShotsFired = ", lbdWrite.iShotsFired )
			CPRINTLN(DEBUG_SHOOTRANGE, "lbdWrite.iShotsHit   = ", lbdWrite.iShotsHit )
			CPRINTLN(DEBUG_SHOOTRANGE, "lbdWrite.eMedal      = ", GET_STRING_FROM_RANGE_ROUND_MEDAL(lbdWrite.eMedal) )
			CPRINTLN(DEBUG_SHOOTRANGE, "categoryName[0]      = ", categoryName[0] )
			CPRINTLN(DEBUG_SHOOTRANGE, "sIdentifier[0]       = ", sIdentifier[0] )
			CPRINTLN(DEBUG_SHOOTRANGE, "m_Id                 = ", sclb_rank_predict.LBWriteDetails.m_GroupSelector.m_Group[0].m_Id )
			CASSERTLN(DEBUG_SHOOTRANGE, "WRITE_TO_RANGE_SP_HIGH_SCORE_LEADERBOARD m_Id <> sIdentifier[0]. Bug this to Default MP Levels and attach logs!")
			
		ENDIF

		// Write the high score to the data
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE), LB_INPUT_COL_SCORE, iHighScore, 0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE), LB_INPUT_COL_SHOTS_FIRED, lbdWrite.iShotsFired, 0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE), LB_INPUT_COL_SHOTS_HIT, lbdWrite.iShotsHit,0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE), LB_INPUT_COL_TOTAL_SHOTS_FIRED, lbdWrite.iShotsFired, 0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE), LB_INPUT_COL_TOTAL_SHOTS_HIT, lbdWrite.iShotsHit,0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE), LB_INPUT_COL_WEAPON_USED, iWeaponTypeUsed, 0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE), LB_INPUT_COL_MEDAL, ENUM_TO_INT(lbdWrite.eMedal), 0)
		
		scLB_rank_predict.bFinishedWrite = TRUE
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "WRITE_TO_RANGE_SP_HIGH_SCORE_LEADERBOARD written, values below:")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "lbdWrite.eRoundType  = ", GET_STRING_FROM_RANGE_ROUND_TYPE(lbdWrite.eRoundType) )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "ePlayerChar          = ", ePlayerChar )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "eWeaponType          = ", g_savedGlobals.sRangeData[ePlayerChar].sRounds[lbdWrite.eRoundType].eWeaponType )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "iWeaponTypeAdd       = ", iWeaponTypeAdd )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "iWeaponTypeUsed      = ", iWeaponTypeUsed )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "iHighScore           = ", iHighScore )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "iGoldReq             = ", iGoldReq )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "lbdWrite.iShotsFired = ", lbdWrite.iShotsFired )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "lbdWrite.iShotsHit   = ", lbdWrite.iShotsHit )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "lbdWrite.eMedal      = ", GET_STRING_FROM_RANGE_ROUND_MEDAL(lbdWrite.eMedal) )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "categoryName[0]      = ", categoryName[0] )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "sIdentifier[0]       = ", sIdentifier[0] )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "m_Id                 = ", sclb_rank_predict.LBWriteDetails.m_GroupSelector.m_Group[0].m_Id )
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC



/// PURPOSE:
///    Triggers the single pre-read for a range round before displaying leaderboard.
///    Begins the prediction process
PROC RANGE_SP_PRE_READ_LBD(RANGE_LBD_WRITE_DATA & lbdWrite, Range_RoundInfo & sRndInfo, Range_Challenge & sChal)
	lbdWrite.eRoundType = sRndInfo.eRoundType
	lbdWrite.iShotsFired = sRndInfo.iTotalShots
	lbdWrite.iShotsHit = sRndInfo.iZ1Hits + sRndInfo.iZ2Hits + sRndInfo.iZ3Hits + sRndInfo.iZ4Hits
	lbdWrite.eMedal = GET_MEDAL_FOR_SCORE(sChal, sRndInfo.sScoreData.iP1Score)
	
	READ_RANGE_SP_SOCIAL_CLUB_LEADERBOARD(sRndInfo.eRoundType)
ENDPROC


