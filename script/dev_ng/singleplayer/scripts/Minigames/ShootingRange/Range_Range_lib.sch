// Range_Range_lib.sch
USING "Range_Range.sch"

/// PURPOSE:
///    Set up all the details of the grid 
PROC INIT_RANGE_GRID(Range_RangeData & sRangeData, Range_RoundInfo & sRndInfo)//GRID_LOCATION & sRangeGridData[],)
	FLOAT rowA_Y = 0.20
	FLOAT rowB_Y = 0.40
	FLOAT rowC_Y = 0.60
	FLOAT rowD_Y = 0.80
	
	FLOAT col1_X = 0.05
	FLOAT col2_X = 0.275
	FLOAT col3_X = 0.50
	FLOAT col4_X = 0.725
	FLOAT col5_X = 0.95
	
	INT rfLIndex
	
	IF NOT IS_ROUND_FLAG_SET(sRndInfo, ROUND_USE_BACK_RANGE)
		rfLIndex = ENUM_TO_INT(RANGE_FRONT_LEFT)
		sRangeData.vRangeForward = sRangeData.vRangeCorners[RANGE_BACK_LEFT] - sRangeData.vRangeCorners[RANGE_FRONT_LEFT]
	    sRangeData.vRangeRight = sRangeData.vRangeCorners[RANGE_FRONT_RIGHT] - sRangeData.vRangeCorners[RANGE_FRONT_LEFT]
	ELSE
		rfLIndex = ENUM_TO_INT(RANGE_FRONT_LEFT_FAR)
		sRangeData.vRangeForward = sRangeData.vRangeCorners[RANGE_BACK_LEFT_FAR] - sRangeData.vRangeCorners[RANGE_FRONT_LEFT_FAR]
	    sRangeData.vRangeRight = sRangeData.vRangeCorners[RANGE_FRONT_RIGHT_FAR] - sRangeData.vRangeCorners[RANGE_FRONT_LEFT_FAR]
	ENDIF

	// Temp variables for below. We calc these before each row so it only has to be done once. Saves a lot of math later.
	VECTOR vFwdRow
	VECTOR vRowCol1X = sRangeData.vRangeCorners[rfLIndex] + sRangeData.vRangeRight * col1_X
	VECTOR vRowCol2X = sRangeData.vRangeCorners[rfLIndex] + sRangeData.vRangeRight * col2_X
	VECTOR vRowCol3X = sRangeData.vRangeCorners[rfLIndex] + sRangeData.vRangeRight * col3_X
	VECTOR vRowCol4X = sRangeData.vRangeCorners[rfLIndex] + sRangeData.vRangeRight * col4_X
	VECTOR vRowCol5X = sRangeData.vRangeCorners[rfLIndex] + sRangeData.vRangeRight * col5_X
	
	// Set all non-occupied
	GRID_INDEX index
	FOR index = gridA1 TO gridD5
		// Trying something here...
		INT iMod = (ENUM_TO_INT(index) % 5)
		
		IF (index < gridB1)
			vFwdRow = sRangeData.vRangeForward * rowA_Y
		ELIF (index < gridC1)
			vFwdRow = sRangeData.vRangeForward * rowB_Y
		ELIF (index < gridD1)
			vFwdRow = sRangeData.vRangeForward * rowC_Y
		ELSE
			vFwdRow = sRangeData.vRangeForward * rowD_Y
		ENDIF
		
		IF (iMod = 0)	// We're in a 1-Col
			sRangeData.sRangeGrid[index].vLoc = vRowCol1X + vFwdRow
		ELIF (iMod = 1)	// We're in a 2-Col
			sRangeData.sRangeGrid[index].vLoc = vRowCol2X + vFwdRow
		ELIF (iMod = 2)	// We're in a 3-Col
			sRangeData.sRangeGrid[index].vLoc = vRowCol3X + vFwdRow
		ELIF (iMod = 3)	// We're in a 4-Col
			sRangeData.sRangeGrid[index].vLoc = vRowCol4X + vFwdRow
		ELIF (iMod = 4)	// We're in a 5-Col
			sRangeData.sRangeGrid[index].vLoc = vRowCol5X + vFwdRow
		ENDIF
		
		sRangeData.sRangeGrid[index].bOccupied = FALSE
	ENDFOR
ENDPROC
