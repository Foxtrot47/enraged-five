// Range_Tutorial.sch

ENUM RANGE_TUTORIAL_BITFLAGS
	eRangeTutorial_Displayed_Hit4 = BIT0,
	eRangeTutorial_Displayed_Hit3 = BIT1,
	eRangeTutorial_Displayed_Hit2 = BIT2,
	eRangeTutorial_Displayed_Hit1 = BIT3,
	eRangeTutorial_weaponTaken    = BIT4
ENDENUM

ENUM RANGE_TUTORIAL_SYNC_SCENE_STATE
	eRangeSyncScene_Init = 0,
	eRangeSyncScene_Update,
	eRangeSyncScene_Cleanup
ENDENUM

STRUCT Range_TutorialData
	// Tutorial info.
	INT						iTutorialTimer
	INT 					iTutorialState
	RANGE_TUTORIAL_BITFLAGS	eBitflags

	RANGE_TUTORIAL_SYNC_SCENE_STATE 	eSceneState = eRangeSyncScene_Init
	
	INT						iSceneID
	CAMERA_INDEX			sceneCamera
	
	PED_INDEX				pedGuy1
	PED_INDEX				pedGuy2
	PED_INDEX				pedGuy3
	
	OBJECT_INDEX			objDoor
	
	OBJECT_INDEX			objEarmuff1
	OBJECT_INDEX			objEarmuff2
	OBJECT_INDEX			objEarmuff3
	
	OBJECT_INDEX			objGlasses1
	OBJECT_INDEX			objGlasses2
	OBJECT_INDEX			objGlasses3
	
	VECTOR					vSceneRootPos				// Tutorial scene root
	VECTOR					vSceneRootOrient			
	
	VECTOR					vScoreTargetPos				// Where exmaple target will be created in tutorial
	
	VECTOR 					vNonTutorialCamPos			// When playing non-tutorial, this data describes the cam
	VECTOR 					vNonTutorialCamOrient
	VECTOR 					vNonTutorialCamEndPos
	VECTOR 					vNonTutorialCamEndOrient
ENDSTRUCT




