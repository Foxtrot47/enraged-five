// range_debug.sch
USING "commands_network.sch"
#IF IS_DEBUG_BUILD
	USING "range_round.sch"
	USING "range_public.sch"
	USING "commands_debug.sch"
	USING "commands_player.sch"
	
	FUNC STRING RANGE_GET_DEBUG_WEAPON_NAME(RANGE_WEAPON_CATEGORY eCategory, RANGE_WEAPON_TYPE eWeapon)
		SWITCH (eCategory)
			CASE WEAPCAT_PISTOL
				IF (eWeapon = RANGE_PISTOL)
					RETURN "Pistol"
				ELIF (eWeapon = RANGE_DLC_PISTOL50)
					RETURN "Pistol .50"
				ELIF (eWeapon = RANGE_COMBATPISTOL)
					RETURN "Combat Pistol"
				ELIF (eWeapon = RANGE_APPISTOL)
					RETURN "AP Pistol"
				ENDIF
			BREAK
			
			CASE WEAPCAT_SMG
				IF (eWeapon = RANGE_MICROSMG)
					RETURN "Micro SMG"
				ELIF (eWeapon = RANGE_SMG)
					RETURN "SGM"
				ELIF (eWeapon = RANGE_DLC_ASSAULTSMG)
					RETURN "Assault SMG"
				ENDIF
			BREAK
			
			CASE WEAPCAT_AR
				IF (eWeapon = RANGE_ASSAULTRIFLE)
					RETURN "Assault Rifle"
				ELIF (eWeapon = RANGE_CARBINERIFLE)
					RETURN "Carbine Rifle"
				ELIF (eWeapon = RANGE_DLC_SPECIALCARBINE)
					RETURN "Special Carbine"
				ELIF (eWeapon = RANGE_DLC_HEAVYRIFLE)
					RETURN "Heavy Rifle"
				ELIF (eWeapon = RANGE_ADVANCEDRIFLE)
					RETURN "Advanced Rifle"
				ENDIF
			BREAK
			
			CASE WEAPCAT_SHOTGUN
				IF (eWeapon = RANGE_PUMPSHOTGUN)
					RETURN "Pump Shotgun"
				ELIF (eWeapon = RANGE_SAWNOFFSHOTGUN)
					RETURN "Sawn-off Shotgun"
				ELIF (eWeapon = RANGE_DLC_BULLPUPSHOTGUN)
					RETURN "Bullpup Shotgun"
				ELIF (eWeapon = RANGE_ASSAULTSHOTGUN)
					RETURN "Assault Shotgun"
				ENDIF
			BREAK
			
			CASE WEAPCAT_LIGHT_MG
				IF (eWeapon = RANGE_MG)
					RETURN "MG"
				ELIF (eWeapon = RANGE_COMBATMG)
					RETURN "Combat MG"
				ELIF (eWeapon = RANGE_DLC_ASSAULTMG)
					RETURN "Assault MG"
				ENDIF
			BREAK
			
			CASE WEAPCAT_MINIGUN
				RETURN "Minigun"
			BREAK
		ENDSWITCH
		
		RETURN "NOT HANDLED!"
	ENDFUNC
	

	FUNC STRING RANGE_GET_ROUND_DEBUG_NAME_FROM_TYPE(RANGE_ROUND_TYPE eRndType)
		SWITCH (eRndType)	
			CASE RT_PISTOL_CHAL_1
				RETURN "PISTOL 1"
			CASE RT_PISTOL_CHAL_2
				RETURN "PISTOL 2"
			CASE RT_PISTOL_CHAL_3
				RETURN "PISTOL 3"
				
			CASE RT_SMG_CHAL_1
				RETURN "SMG 1"
			CASE RT_SMG_CHAL_2
				RETURN "SMG 2"
			CASE RT_SMG_CHAL_3
				RETURN "SMG 3"
				
			CASE RT_AR_CHAL_1
				RETURN "AR 1"
			CASE RT_AR_CHAL_2
				RETURN "AR 2"
			CASE RT_AR_CHAL_3
				RETURN "AR 3"
				
			CASE RT_SHOTGUN_CHAL_1
				RETURN "SHOTGUN 1"
			CASE RT_SHOTGUN_CHAL_2
				RETURN "SHOTGUN 2"
			CASE RT_SHOTGUN_CHAL_3
				RETURN "SHOTGUN 3"
				
			CASE RT_LMG_CHAL_1
				RETURN "LMG 1"
			CASE RT_LMG_CHAL_2
				RETURN "LMG 2"
			CASE RT_LMG_CHAL_3
				RETURN "LMG 3"
				
			CASE RT_MINIGUN_CHAL_1
				RETURN "HEAVY 1"
			CASE RT_MINIGUN_CHAL_2
				RETURN "HEAVY 2"
			CASE RT_MINIGUN_CHAL_3
				RETURN "HEAVY 3"
			BREAK
		ENDSWITCH
		
		RETURN "NOT HANDLED!"
	ENDFUNC
	
	FUNC STRING RANGE_GET_MEDAL_DEBUG_NAME(RANGE_ROUND_MEDAL eMedal)
		SWITCH eMedal
			CASE RRM_GOLD
				RETURN "GOLD"
			CASE RRM_SILVER
				RETURN "SILVER"
			CASE RRM_BRONZE
				RETURN "BRONZE"
			DEFAULT
				RETURN "NO MEDAL!"
		ENDSWITCH
		RETURN "NOT HANDLED!"
	ENDFUNC
	
	PROC EXPORT_ROUND_DATA_TO_NETWORK_DEBUG_FILE(Range_RoundInfo & sRndInfo, Range_Challenge & sRoundChallenge)	
		CDEBUG2LN(DEBUG_SHOOTRANGE, "EXPORT_ROUND_DATA_TO_NETWORK_DEBUG_FILE called")
		// Set our path.
		TEXT_LABEL_63 txtFilePath = "N:/RSGSAN/RDR2/usr/rparadis/Exports/"
		
		// Construct our file name.
		TEXT_LABEL_63 txtFileName = GET_PLAYER_NAME(GET_PLAYER_INDEX())
		IF ARE_STRINGS_EQUAL(GET_PLAYER_NAME(GET_PLAYER_INDEX()), "User")
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Seriously? Your username is 'user'? Appending game time onto the end to make you unique. You should fix that, though.")
			txtFileName += GET_GAME_TIMER()
		ENDIF
		txtFileName += ".log"
		
		IF OPEN_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			// Write some shit! (63 characters at a time)
			TEXT_LABEL_63 txtTemp
			
			txtTemp = "--------------------"
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			
			// Challenge:
			txtTemp = "Challenge: "
			txtTemp += RANGE_GET_ROUND_DEBUG_NAME_FROM_TYPE(sRndInfo.eRoundType)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			
			// User:
			txtTemp = "User: "
			txtTemp += GET_PLAYER_NAME(GET_PLAYER_INDEX())
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			
			// Weapon used:
			txtTemp = "Weapon Used: "
			txtTemp += RANGE_GET_DEBUG_WEAPON_NAME(sRndInfo.eWeaponCat, sRndInfo.eWeaponType)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			
			// Score earned:
			txtTemp = "Score Earned: "
			txtTemp += sRndInfo.sScoreData.iP1Score
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			
			// Hits per zone:
			txtTemp = "Hits Per Zone (bullseye first): "
			txtTemp += sRndInfo.iZ1Hits
			txtTemp += " / "
			txtTemp += sRndInfo.iZ2Hits
			txtTemp += " / "
			txtTemp += sRndInfo.iZ3Hits
			txtTemp += " / "
			txtTemp += sRndInfo.iZ4Hits
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			
			// Misses:
			txtTemp = "Number of Misses: "
			INT iNumMisses = sRndInfo.iTotalShots - (sRndInfo.iZ1Hits + sRndInfo.iZ2Hits + sRndInfo.iZ3Hits + sRndInfo.iZ4Hits)
			txtTemp += iNumMisses
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			
			// Accuracy:
			txtTemp = "Accuracy: "
			INT iAcc = sRndInfo.iZ1Hits + sRndInfo.iZ2Hits + sRndInfo.iZ3Hits + sRndInfo.iZ4Hits
			iAcc = ROUND((TO_FLOAT(iAcc) / TO_FLOAT(sRndInfo.iTotalShots)) * 100.0)
			txtTemp += iAcc
			txtTemp += "% "
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			
			// Goals for this challenge:
			txtTemp = "GOALS"
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			txtTemp = "Bronze: "
			txtTemp += sRoundChallenge.iBronzeReq
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			txtTemp = "Silver: "
			txtTemp += sRoundChallenge.iSilverReq
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			txtTemp = "Gold: "
			txtTemp += sRoundChallenge.iGoldReq
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)

			// Medal earned:
			txtTemp = "Medal Earned: "
			txtTemp += RANGE_GET_MEDAL_DEBUG_NAME(GET_MEDAL_FOR_SCORE(sRoundChallenge, sRndInfo.sScoreData.iP1Score))
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			
			txtTemp = "===================="
			SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTemp, txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(txtFilePath, txtFileName)
			CLOSE_DEBUG_FILE()
			
			CDEBUG2LN(DEBUG_SHOOTRANGE, "")
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Dear Unwitting Tester,")
			CDEBUG2LN(DEBUG_SHOOTRANGE, "I am tracking you. Your round results have been written to a network debug file.")
			CDEBUG2LN(DEBUG_SHOOTRANGE, "I appreciate your cooperation.")
			CDEBUG2LN(DEBUG_SHOOTRANGE, "                                                  Sincerely,")
			CDEBUG2LN(DEBUG_SHOOTRANGE, "                                                 Ryan Paradis")
			CDEBUG2LN(DEBUG_SHOOTRANGE, "")
		ELSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Couldn't access netwrok location. Not tracking you. Sorry.")
		ENDIF
	ENDPROC
#ENDIF


