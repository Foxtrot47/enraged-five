//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	Range_round_library.sch										//
//		AUTHOR			:	Ryan Paradis												//
//		DESCRIPTION		:	library for all of the gun range rounds				        //
//																						//
////////////////////////////////////////////////////////////////////////////////////////// 
USING "Range_Round.sch"
USING "range_target_lib.sch"

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_PistolChallenge_1(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 10 Targets
	sRndInfo.iMaxTargets = 15
	sRndInfo.fCurrentRoundLength = 40.0
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP
	sRndInfo.eRoundType = RT_PISTOL_CHAL_1
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	// Single targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA3)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridA2)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridA4)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridC3)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridC2)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridC1)
	
	// Drop down together. Only play 2 entry sounds.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridD2, 6, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridD3, 6)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridD4, 6)
	
	// Singular targets again.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridB4)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridB3)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridB2)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridC3)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridB3)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridA3)

	// Our Actions
	sRndInfo.iMaxActions = 0
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_PistolChallenge_2(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 10 Targets
	sRndInfo.iMaxTargets = 6
	sRndInfo.fCurrentRoundLength = 24.0
	
	sRndInfo.fCurrentBulletDamage = 0
	sRndInfo.eRoundType = RT_PISTOL_CHAL_2
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	SET_ROUND_FLAG(sRndInfo, ROUND_ALLOW_TIME_EXPIRY)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	//single target
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0],gridD2, 0, TS_HEIGHT_MID | TS_GIVE_FLOAT_HELP)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1],gridD3, 999)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2],gridD4, 999)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3],gridA3, 999)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4],gridA2, 999)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5],gridD3, 999)
	
	// Our Actions
	sRndInfo.iMaxActions = 17
	
	// First Target - 17 actions
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,4.0,AB_MOVE,gridA2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,-1.0,AB_DESTROY_WHEN_AT_LOC,gridA2)
	
	// Second Target
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,3.75,AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,4.0,AB_MOVE,gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,-1.0,AB_DESTROY_WHEN_AT_LOC,gridA3)
	
	// Third Target
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,7.5,AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,4.0,AB_MOVE,gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,-1.0,AB_DESTROY_WHEN_AT_LOC,gridA4)
	
	// Fourth Target
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,10.25,AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,4.0,AB_MOVE,gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,-1.0,AB_DESTROY_WHEN_AT_LOC,gridD3)
	
	// Fifth Target
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4,14.0,AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4,4.0,AB_MOVE,gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4,-1.0,AB_DESTROY_WHEN_AT_LOC,gridD2)
	
	// Sixth Target
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5,17.75,AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5,4.0,AB_MOVE,gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5,-1.0,AB_DESTROY_WHEN_AT_LOC,gridA3)
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_PistolChallenge_3(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 10 Targets
	sRndInfo.iMaxTargets = 1
	sRndInfo.fCurrentRoundLength = 33.0
	
	sRndInfo.fCurrentBulletDamage = 0.0
	sRndInfo.eRoundType = RT_PISTOL_CHAL_3
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	//single target
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA5, 0, TS_HEIGHT_MID | TS_GIVE_FLOAT_HELP)
	
	// Our Actions
	sRndInfo.iMaxActions = 16
	
	// Fast move is 3 seconds.
	
	// Initial move
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 1.0, AB_MOVE_AT_TIME_FAST, gridA1)
	
	// Flip then move back... x4 times
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,4.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,5.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,5.5,AB_MOVE_AT_TIME_FAST, gridA5)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,8.6,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,10.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,10.5,AB_MOVE_AT_TIME_FAST, gridA1)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,13.6,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,15.6,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,16.0,AB_MOVE_AT_TIME_FAST, gridA5)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,19.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,21.6,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,22.0,AB_MOVE_AT_TIME_FAST, gridA1)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,25.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,28.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,28.5,AB_MOVE_AT_TIME_FAST, gridA5)
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_SMGChallenge_1(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)	
		// 5 Targets
	sRndInfo.iMaxTargets = 5
	sRndInfo.fCurrentRoundLength = 29.0
	
	sRndInfo.fCurrentBulletDamage = 0.0
	sRndInfo.eRoundType = RT_SMG_CHAL_1
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	//the outsides. Only play 2 entry sounds.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0],gridB1,0)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1],gridB2,0)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2],gridB3,0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND | TS_GIVE_FLOAT_HELP)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3],gridB4,0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4],gridB5,0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
		
	// Our Actions
	sRndInfo.iMaxActions = 24
	
	/// Line of targets all flipped
	
	// x-x-o-x-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,0.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,0.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,0.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4,0.0,AB_FLIP)

	// x-o-x-x-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,2.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,2.0,AB_FLIP)
	
	// o-x-x-x-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,4.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,4.0,AB_FLIP)
	
	// x-o-x-x-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,6.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,6.0,AB_FLIP)
	
	// x-x-x-x-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,8.0,AB_FLIP)
	// FIRST RELOAD
	
	// x-x-o-x-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,11.0,AB_FLIP)
	
	// x-x-x-o-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,13.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,13.0,AB_FLIP)
	
	// x-x-x-x-o
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,15.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4,15.0,AB_FLIP)
	
	// x-x-x-o-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4,17.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,17.0,AB_FLIP)
	
	// x-x-x-x-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,19.0,AB_FLIP)
	// SECOND RELOAD
	
	// x-x-x-o-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,22.0,AB_FLIP)
	
	// x-x-o-x-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,24.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,24.0,AB_FLIP)
	
	// x-o-x-x-x
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,26.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,26.0,AB_FLIP)
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_SMGChallenge_2(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
		// 5 Targets
	sRndInfo.iMaxTargets = 4
	sRndInfo.fCurrentRoundLength = 26.5
	
	sRndInfo.fCurrentBulletDamage = 0.0
	sRndInfo.eRoundType = RT_SMG_CHAL_2
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	//the outsides. Play 2 entry sounds.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA3, 0, TS_HEIGHT_MID | TS_GIVE_FLOAT_HELP)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridB3, 0)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridC3, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridD3, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
		
	// Our Actions
	sRndInfo.iMaxActions = 40
	
	/// Line of targets all flipped
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,0.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,0.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,0.0,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,0.0,AB_FLIP)
	
	
	// gridA3->A1 
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,0.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,0.1,AB_MOVE_AT_TIME_FAST,gridA1)	// Will take 3.0 seconds to get there.
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,3.1,AB_FLIP)
	
	// gridB3->B1
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,1.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,1.1,AB_MOVE_AT_TIME_FAST,gridB1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,4.1,AB_FLIP)
	
	// gridC3->C1
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,2.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,2.1,AB_MOVE_AT_TIME_FAST,gridC1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,5.1,AB_FLIP)
	
	// gridD3->D1
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,3.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,3.1,AB_MOVE_AT_TIME_FAST,gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,6.1,AB_FLIP)
	
	// Everything is in column 1 now.
	
	// GridA1->A5
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,9.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,9.1,AB_MOVE_AT_TIME_NORMAL,gridA5)	// Will take 4.0 seconds to get there.
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,13.1,AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,10.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,10.1,AB_MOVE_AT_TIME_NORMAL,gridB5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,14.1,AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,11.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,11.1,AB_MOVE_AT_TIME_NORMAL,gridC5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,15.1,AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,12.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,12.1,AB_MOVE_AT_TIME_NORMAL,gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,16.1,AB_FLIP)
	
	// Everything is in column 5 now.
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,19.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,19.1,AB_MOVE_AT_TIME_FAST,gridA3)	// Will take 3.0 seconds to get there.
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0,22.1,AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,20.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,20.1,AB_MOVE_AT_TIME_FAST,gridB3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1,23.1,AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,21.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,21.1,AB_MOVE_AT_TIME_FAST,gridC3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2,24.1,AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,22.1,AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,22.1,AB_MOVE_AT_TIME_FAST,gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3,25.1,AB_FLIP)
ENDPROC


/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_SMGChallenge_3(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 1 target, 2 actions.
	sRndInfo.iMaxTargets = 10
	sRndInfo.iMaxActions = 30
	
	sRndInfo.fCurrentRoundLength = 33.0
	sRndInfo.fCurrentBulletDamage = 0.0
	sRndInfo.eRoundType = RT_SMG_CHAL_3
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	SET_ROUND_FLAG(sRndInfo, ROUND_ALLOW_TIME_EXPIRY)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	// 2.5 second targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridB3, 0, TS_HEIGHT_MID | TS_GIVE_FLOAT_HELP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 2.5, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridC2, 999, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 2.5, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 5.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridD4, 999, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 5.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 7.5, AB_DESTROY_AT_TIME)
		
	// 2 second targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridD1, 999, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 13.5, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 0.1, AB_MOVE_AT_TIME_FAST, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 17.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridB3, 999, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 10.5, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 13.5, AB_DESTROY_AT_TIME)
		
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridC5, 999, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 7.5, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 0.1, AB_MOVE_AT_TIME_FAST, gridC1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 11.0, AB_DESTROY_AT_TIME)
	
	// 2 second targets. Two entry sounds.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridD5, 6, TS_HEIGHT_LOW)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridD4, 6, TS_HEIGHT_MID)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridD3, 6, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridD2, 6, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND)
	//SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridD1, 6)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 0.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 0.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 0.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 0.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 0.0, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 0.1, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 0.1, AB_MOVE_AT_TIME_FAST, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, -1.0, AB_DESTROY_WHEN_AT_LOC, gridA5)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 3.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 3.0, AB_MOVE_AT_TIME_FAST, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, -1.0, AB_DESTROY_WHEN_AT_LOC, gridA4)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 8.5, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 8.5, AB_MOVE_AT_TIME_FAST, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, -1.0, AB_DESTROY_WHEN_AT_LOC, gridA3)

	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 11.4, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 11.4, AB_MOVE_AT_TIME_FAST, gridA2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, -1.0, AB_DESTROY_WHEN_AT_LOC, gridA2)
	
	//TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 10.9, AB_FLIP)
	//TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 10.9, AB_MOVE_AT_TIME_FAST, gridA1)
	//TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, -1.0, AB_DESTROY_WHEN_AT_LOC, gridA1)
ENDPROC


/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_AssaultRifleChallenge_1(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 16 Targets
	sRndInfo.iMaxTargets = 17
	sRndInfo.fCurrentRoundLength = 37.0
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP / 2.9
	sRndInfo.eRoundType = RT_AR_CHAL_1
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_BACK_RANGE)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	//the outsides
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridC2, 0, TS_HEIGHT_MID | TS_GIVE_FLOAT_HELP)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridC3)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridC4)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridD4)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridD3)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridD2)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridB1)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridB2)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridB3)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridB4)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridB5)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridC1, 11)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridC3, 11)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridC5, 11, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridA2, 14)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[15], gridA3, 14)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[16], gridA4, 14, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)

	// Our Actions
	sRndInfo.iMaxActions = 0
ENDPROC


/// PURPOSE:
///    Constructor for shooting range rounds
///    Want to demonstrate that it can punch through targets.
PROC Setup_AssaultRifleChallenge_2(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)			
	// 5 Targets
	sRndInfo.iMaxTargets = 4
	sRndInfo.fCurrentRoundLength = 58.0
	
	sRndInfo.fCurrentBulletDamage = 0.0
	sRndInfo.eRoundType = RT_AR_CHAL_2
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_BACK_RANGE)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	// Line - Left side, all flipped.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA5, 0, TS_HEIGHT_MID | TS_GIVE_FLOAT_HELP)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridB5, 0)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridC5, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridD5, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 0.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 0.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 0.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 0.0, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 1.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 2.0, AB_MOVE_AT_TIME_SLOW, gridA1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 7.5, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 8.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 9.0, AB_MOVE_AT_TIME_SLOW, gridB1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 14.5, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 15.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 16.0, AB_MOVE_AT_TIME_SLOW, gridC1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 21.5, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 22.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 23.0, AB_MOVE_AT_TIME_SLOW, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 28.5, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 29.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 30.0, AB_MOVE_AT_TIME_SLOW, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 35.5, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 36.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 37.0, AB_MOVE_AT_TIME_SLOW, gridB4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 42.5, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 43.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 44.0, AB_MOVE_AT_TIME_SLOW, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 49.5, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 50.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 51.0, AB_MOVE_AT_TIME_SLOW, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 56.5, AB_FLIP)
	
	// Our Actions
	sRndInfo.iMaxActions = 28
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
///    Want to demonstrate that it can punch through targets.
PROC Setup_AssaultRifleChallenge_3(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)			
	// 16 Targets
	sRndInfo.iMaxTargets = 10
	sRndInfo.fCurrentRoundLength = 30.0
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP / 2.9
	sRndInfo.eRoundType = RT_AR_CHAL_3
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_BACK_RANGE)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	// Our Actions
	sRndInfo.iMaxActions = 35
	
	// Front target L->R
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA5, 0, TS_HEIGHT_MID | TS_GIVE_FLOAT_HELP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 0.75, AB_MOVE_AT_TIME_SLOW, gridA1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, -1.0, AB_DESTROY_WHEN_AT_LOC, gridA1)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridB1, 1, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 0.75, AB_MOVE_AT_TIME_SLOW, gridB5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, -1.0, AB_DESTROY_WHEN_AT_LOC, gridB5)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridC5, 2, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 0.75, AB_MOVE_AT_TIME_SLOW, gridC1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, -1.0, AB_DESTROY_WHEN_AT_LOC, gridC1)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridD1, 3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 0.75, AB_MOVE_AT_TIME_SLOW, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, -1.0, AB_DESTROY_WHEN_AT_LOC, gridD5)
	
	// 3 targets,   x
	//			  x   x	
	//				 
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridD1, 4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 1.0, AB_MOVE_AT_TIME_NORMAL, gridA1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 5.0, AB_MOVE_AT_TIME_NORMAL, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 9.0, AB_MOVE_AT_TIME_NORMAL, gridA1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 13.0, AB_MOVE_AT_TIME_NORMAL, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 29.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridD3, 4, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 1.0, AB_MOVE_AT_TIME_NORMAL, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 5.0, AB_MOVE_AT_TIME_NORMAL, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 9.0, AB_MOVE_AT_TIME_NORMAL, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 13.0, AB_MOVE_AT_TIME_NORMAL, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 29.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridD5, 4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 1.0, AB_MOVE_AT_TIME_NORMAL, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 5.0, AB_MOVE_AT_TIME_NORMAL, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 9.0, AB_MOVE_AT_TIME_NORMAL, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 13.0, AB_MOVE_AT_TIME_NORMAL, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 29.0, AB_DESTROY_AT_TIME)
	
	// 3 targets, x x
	//				
	//			   x
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridA2, 7, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 1.0, AB_MOVE_AT_TIME_NORMAL, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 5.0, AB_MOVE_AT_TIME_NORMAL, gridA2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 9.0, AB_MOVE_AT_TIME_NORMAL, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 13.0, AB_MOVE_AT_TIME_NORMAL, gridA2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 30.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridA3, 7, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 1.0, AB_MOVE_AT_TIME_NORMAL, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 5.0, AB_MOVE_AT_TIME_NORMAL, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 9.0, AB_MOVE_AT_TIME_NORMAL, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 13.0, AB_MOVE_AT_TIME_NORMAL, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 30.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridA4, 7, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 1.0, AB_MOVE_AT_TIME_NORMAL, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 5.0, AB_MOVE_AT_TIME_NORMAL, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 9.0, AB_MOVE_AT_TIME_NORMAL, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 13.0, AB_MOVE_AT_TIME_NORMAL, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 30.0, AB_DESTROY_AT_TIME)
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_ShotgunChallenge_1(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 1 Targets
	sRndInfo.iMaxTargets = 32
	sRndInfo.fCurrentRoundLength = 30.0
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP + 99.0
	sRndInfo.eRoundType = RT_SHOTGUN_CHAL_1
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_DESTRUCTION_W_BONUS
	
	// Setup targets. Since they're all being created at the same time, only play 3 entry noises.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA1, 0)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridA2, 0)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridA3, 0)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridA4, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridB1, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridB2, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridB3, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridB4, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridC1, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridC2, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridC3, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridC4, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridD1, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridD2, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridD3, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[15], gridD4, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[16], gridA1, 16)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[17], gridA2, 16)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[18], gridA3, 16)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[19], gridA4, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[20], gridB1, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[21], gridB2, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[22], gridB3, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[23], gridB4, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[24], gridC1, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[25], gridC2, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[26], gridC3, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[27], gridC4, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[28], gridD1, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[29], gridD2, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[30], gridD3, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[31], gridD4, 16, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	
	// Our Actions
	sRndInfo.iMaxActions = 0
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_ShotgunChallenge_2(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 12 Targets
	sRndInfo.iMaxTargets = 12
	sRndInfo.fCurrentRoundLength = 43.5
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP + 100.0
	sRndInfo.eRoundType = RT_SHOTGUN_CHAL_2
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_RANGE_REVERSE
	
	// Our Actions
	sRndInfo.iMaxActions = 46
	
	// Setup targets & actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridD5, 0)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 1.0, AB_MOVE_AT_TIME_FAST, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 3.0, AB_MOVE_AT_TIME_FAST, gridA1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 7.0, AB_MOVE_AT_TIME_FAST, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 11.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridD3, 1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 1.0, AB_MOVE_AT_TIME_FAST, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 4.0, AB_MOVE_AT_TIME_FAST, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 7.0, AB_MOVE_AT_TIME_FAST, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 22.0, AB_DESTROY_AT_TIME)
	
	// Setup targets & actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridD2, 2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 1.0, AB_MOVE_AT_TIME_VERYFAST, gridA2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 3.0, AB_MOVE_AT_TIME_VERYFAST, gridA1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 5.0, AB_MOVE_AT_TIME_VERYFAST, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 30.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridD4, 2, TS_HEIGHT_MID | TS_DONT_PLAY_SLIDE_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 1.0, AB_MOVE_AT_TIME_VERYFAST, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 3.0, AB_MOVE_AT_TIME_VERYFAST, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 5.0, AB_MOVE_AT_TIME_VERYFAST, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 30.0, AB_DESTROY_AT_TIME)
	
	// Setup targets & actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridD1, 4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 1.0, AB_MOVE_AT_TIME_VERYFAST, gridA1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 3.0, AB_MOVE_AT_TIME_VERYFAST, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 36.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridD2, 4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 1.0, AB_MOVE_AT_TIME_VERYFAST, gridA2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 3.0, AB_MOVE_AT_TIME_VERYFAST, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 36.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridD3, 4, TS_HEIGHT_MID | TS_DONT_PLAY_SLIDE_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 1.0, AB_MOVE_AT_TIME_VERYFAST, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 3.0, AB_MOVE_AT_TIME_VERYFAST, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 36.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridD4, 4, TS_HEIGHT_MID | TS_DONT_PLAY_SLIDE_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 1.0, AB_MOVE_AT_TIME_VERYFAST, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 3.0, AB_MOVE_AT_TIME_VERYFAST, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 36.0, AB_DESTROY_AT_TIME)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridD5, 4, TS_HEIGHT_MID | TS_DONT_PLAY_SLIDE_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 1.0, AB_MOVE_AT_TIME_VERYFAST, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 3.0, AB_MOVE_AT_TIME_VERYFAST, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 36.0, AB_DESTROY_AT_TIME)
	
	// Last set.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridA2, 9)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 1.0, AB_MOVE_AT_TIME_VERYFAST, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 3.0, AB_MOVE_AT_TIME_VERYFAST, gridA1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 5.0, AB_MOVE_AT_TIME_VERYFAST, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 7.0, AB_MOVE_AT_TIME_VERYFAST, gridA1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 9.0, AB_MOVE_AT_TIME_VERYFAST, gridD2)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridA3, 9, TS_HEIGHT_MID | TS_DONT_PLAY_SLIDE_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 1.0, AB_MOVE_AT_TIME_VERYFAST, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 3.0, AB_MOVE_AT_TIME_VERYFAST, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 5.0, AB_MOVE_AT_TIME_VERYFAST, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 7.0, AB_MOVE_AT_TIME_VERYFAST, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 9.0, AB_MOVE_AT_TIME_VERYFAST, gridD3)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridA4, 9, TS_HEIGHT_MID | TS_DONT_PLAY_SLIDE_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 1.0, AB_MOVE_AT_TIME_VERYFAST, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 3.0, AB_MOVE_AT_TIME_VERYFAST, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 5.0, AB_MOVE_AT_TIME_VERYFAST, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 7.0, AB_MOVE_AT_TIME_VERYFAST, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 9.0, AB_MOVE_AT_TIME_VERYFAST, gridD4)
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_ShotgunChallenge_3(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 10 Targets
	sRndInfo.iMaxTargets = 20
	sRndInfo.fCurrentRoundLength = 29.2
	
	//SET_ROUND_FLAG(sRndInfo, ROUND_ALLOW_TIME_EXPIRY)
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP
	sRndInfo.eRoundType = RT_SHOTGUN_CHAL_3
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_DESTRUCTION_W_BONUS
	
	// Our Actions
	sRndInfo.iMaxActions = 47
	
	// 3 targets crossing paths. -- 6 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA1, 0)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 0.5, AB_MOVE_AT_TIME_VERYFAST, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 3.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridB5, 0)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 0.5, AB_MOVE_AT_TIME_SLOW, gridB1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridA3, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 0.5, AB_MOVE_AT_TIME_FAST, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 4.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// 3 targets crossing paths. -- 6 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridC1, 3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 0.5, AB_MOVE_AT_TIME_FAST, gridC5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 4.0, AB_DESTROY_AFTER_TIME_CREATED, gridC5)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridB5, 3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 0.5, AB_MOVE_AT_TIME_FAST, gridB1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 4.0, AB_DESTROY_AFTER_TIME_CREATED, gridB1)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridA2, 3, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 0.5, AB_MOVE_AT_TIME_FAST, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 4.0, AB_DESTROY_AFTER_TIME_CREATED, gridD2)
	
	// 3 targets crossing paths. -- 6 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridD1, 6)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 0.5, AB_MOVE_AT_TIME_FAST, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 4.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridC5, 6)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 0.5, AB_MOVE_AT_TIME_FAST, gridC1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 4.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridA4, 6, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 0.5, AB_MOVE_AT_TIME_FAST, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 4.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// 4 targets crossing paths. -- 8 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridA2, 9)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 0.5, AB_MOVE_AT_TIME_SLOW, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridA3, 9)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 0.5, AB_MOVE_AT_TIME_SLOW, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridC1, 9, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 0.5, AB_MOVE_AT_TIME_VERYFAST, gridC5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 3.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridD5, 9, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 0.5, AB_MOVE_AT_TIME_VERYFAST, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 3.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// 4 targets crossing paths. -- 12 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridA3, 13)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 0.5, AB_MOVE_AT_TIME_SLOW, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 5.5, AB_MOVE_AT_TIME_SLOW, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridA4, 13)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 0.5, AB_MOVE_AT_TIME_SLOW, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 5.5, AB_MOVE_AT_TIME_SLOW, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[15], gridD1, 13, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 0.5, AB_MOVE_AT_TIME_VERYFAST, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 5.5, AB_MOVE_AT_TIME_VERYFAST, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 8.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[16], gridC5, 13, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 16, 0.5, AB_MOVE_AT_TIME_VERYFAST, gridC1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 16, 5.5, AB_MOVE_AT_TIME_VERYFAST, gridC5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 16, 8.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// 3 Condensing targets. -- 9 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[17], gridA1, 17)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 17, 0.5, AB_MOVE_AT_TIME_SLOW, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 17, 5.5, AB_MOVE_AT_TIME_SLOW, gridA1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 17, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[18], gridA5, 17)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 18, 0.5, AB_MOVE_AT_TIME_NORMAL, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 18, 5.5, AB_MOVE_AT_TIME_NORMAL, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 18, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[19], gridA3, 17, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 19, 0.5, AB_MOVE_AT_TIME_FAST, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 19, 5.5, AB_MOVE_AT_TIME_FAST, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 19, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_MinigunChallenge_1(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 1 Targets
	sRndInfo.iMaxTargets = 5
	sRndInfo.fCurrentRoundLength = 35.1
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP / 5.0
	sRndInfo.eRoundType = RT_MINIGUN_CHAL_1

	SET_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_TARGETS_ARE_INFINITE)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_MINIGUN
	
	// Setup targets.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridC1, 0, TS_RESPAWNS | TS_RESPAWN_SHORT | TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridC2, 0, TS_RESPAWNS | TS_RESPAWN_SHORT | TS_HEIGHT_MID)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridC3, 0, TS_RESPAWNS | TS_RESPAWN_SHORT | TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridC4, 0, TS_RESPAWNS | TS_RESPAWN_SHORT | TS_HEIGHT_MID)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridC5, 0, TS_RESPAWNS | TS_RESPAWN_SHORT | TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)

	// Our Actions
	sRndInfo.iMaxActions = 0
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_MinigunChallenge_2(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 12 Targets
	sRndInfo.iMaxTargets = 47
	sRndInfo.fCurrentRoundLength = 33.5
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP / 3.0
	sRndInfo.eRoundType = RT_MINIGUN_CHAL_2
	
	//SET_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_MINIGUN
	
	// Our Actions
	sRndInfo.iMaxActions = 0
	
	// One column at a time.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA3, 0, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridB3, 0)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridC3, 0)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridD3, 0, TS_DONT_PLAY_ENTRY_SOUND)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridA2, 4, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridB2, 4)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridC2, 4)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridD2, 4, TS_DONT_PLAY_ENTRY_SOUND)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridA5, 8, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridB5, 8)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridC5, 8)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridD5, 8, TS_DONT_PLAY_ENTRY_SOUND)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridA1, 12, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridB1, 12)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridC1, 12)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[15], gridD1, 12, TS_DONT_PLAY_ENTRY_SOUND)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[16], gridA4, 16, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[17], gridB4, 16)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[18], gridC4, 16)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[19], gridD4, 16, TS_DONT_PLAY_ENTRY_SOUND)
	
	
	// Middle 3 columns
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[20], gridA2, 20, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[21], gridB2, 20) 
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[22], gridC2, 20, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[23], gridD2, 20, TS_DONT_PLAY_ENTRY_SOUND)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[24], gridA3, 20, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[25], gridB3, 20)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[26], gridC3, 20, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[27], gridD3, 20, TS_DONT_PLAY_ENTRY_SOUND)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[28], gridA4, 20, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[29], gridB4, 20)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[30], gridC4, 20, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[31], gridD4, 20, TS_DONT_PLAY_ENTRY_SOUND)
	
	// Front Row
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[32], gridA1, 32, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[33], gridA2, 32)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[34], gridA3, 32, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[35], gridA4, 32)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[36], gridA5, 32, TS_DONT_PLAY_ENTRY_SOUND)
	
	// Middle Row
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[37], gridA1, 37, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[38], gridA2, 37)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[39], gridA3, 37, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[40], gridA4, 37)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[41], gridA5, 37, TS_DONT_PLAY_ENTRY_SOUND)
	
	// Back Row
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[42], gridA1, 42, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[43], gridA2, 42)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[44], gridA3, 42, TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[45], gridA4, 42)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[46], gridA5, 42, TS_DONT_PLAY_ENTRY_SOUND)
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_MinigunChallenge_3(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 10 Targets
	sRndInfo.iMaxTargets = 20
	sRndInfo.fCurrentRoundLength = 40.0
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP / 3.0
	sRndInfo.eRoundType = RT_MINIGUN_CHAL_3
	
	//SET_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_MINIGUN
	SET_ROUND_FLAG(sRndInfo, ROUND_TARGETS_ARE_INFINITE)
	SET_ROUND_FLAG(sRndInfo, ROUND_HVY3_FLOAT_HELP)
	
	// Our Actions
	sRndInfo.iMaxActions = 0

	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA1, 0, TS_HEIGHT_HIGH | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridA2, 0, TS_HEIGHT_HIGH | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridA3, 0, TS_HEIGHT_HIGH | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridA4, 0, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridA5, 0, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridB1, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridB2, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridB3, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridB4, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridB5, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridC1, 0, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridC2, 0, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridC3, 0, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridC4, 0, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridC5, 0, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[15], gridD1, 0, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[16], gridD2, 0, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[17], gridD3, 0, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[18], gridD4, 0, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[19], gridD5, 0, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND | TS_RESPAWNS)
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_LMGChallenge_1(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	sRndInfo.eRoundType = RT_LMG_CHAL_1
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP / 5.0
	sRndInfo.iMaxTargets = 16
	sRndInfo.iMaxActions = 30
	sRndInfo.fCurrentRoundLength = 33.5
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_TARGETS_ARE_INFINITE)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	// Single target.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA2, 0, TS_HEIGHT_MID | TS_GIVE_FLOAT_HELP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 4.5, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridA4, 0, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 4.5, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Two targets.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridB3, 999, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 6.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 4.5, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridC3, 999, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 6.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 4.5, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Three targets.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridA2, 999, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 12.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 4.5, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridA3, 999, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 12.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 4.5, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridA4, 999, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 12.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 4.5, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Four targets.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridB2, 999, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 18.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridB3, 999, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 18.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridC3, 999, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 18.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridB4, 999, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 18.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Five Targets.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridA2, 999, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 26.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridC2, 999, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 26.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridA3, 999, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 26.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridA4, 999, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 26.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[15], gridC4, 999, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 26.0, AB_CREATE_AT_TIME)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 6.0, AB_DESTROY_AFTER_TIME_CREATED)
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_LMGChallenge_2(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	sRndInfo.eRoundType = RT_LMG_CHAL_2
	
	sRndInfo.fCurrentBulletDamage = 0.0
	sRndInfo.iMaxTargets = 5
	sRndInfo.fCurrentRoundLength = 64.5
	sRndInfo.iMaxActions = 45
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_BACK_RANGE)
	SET_ROUND_FLAG(sRndInfo, ROUND_TARGETS_ARE_INFINITE)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	
	// 5 targets in a row that all start off flipped.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA1, 0, TS_HEIGHT_HIGH | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridB2, 0, TS_HEIGHT_LOW | TS_RESPAWNS)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridC3, 0, TS_HEIGHT_HIGH | TS_RESPAWNS | TS_DONT_PLAY_ENTRY_SOUND | TS_GIVE_FLOAT_HELP)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridB4, 0, TS_HEIGHT_MID | TS_RESPAWNS | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridA5, 0, TS_HEIGHT_LOW | TS_RESPAWNS | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 0.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 0.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 0.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 0.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 0.0, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 1.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 4.0, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 7.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 10.0, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 13.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 16.0, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 19.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 22.0, AB_FLIP)
	
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 25.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 28.0, AB_FLIP)
	
	// Show all targets in a sequence.
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 31.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 32.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 33.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 34.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 35.0, AB_FLIP)
	
	// Wait a sec, then flip them all back, in the same sequence
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 38.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 39.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 40.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 41.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 42.0, AB_FLIP)
	
	// Wait 2 secs, then show them again in the reverse sequence.
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 45.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 46.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 47.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 48.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 49.0, AB_FLIP)
	
	// Wait a sec, then flip them back.
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 52.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 53.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 54.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 55.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 56.0, AB_FLIP)
	
	// Show them all to the player.
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 59.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 59.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 59.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 59.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 59.0, AB_FLIP)
	
	// End the round. Hide them all.
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 64.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 64.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 64.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 64.0, AB_FLIP)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 64.0, AB_FLIP)
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_LMGChallenge_3(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	sRndInfo.eRoundType = RT_LMG_CHAL_3
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP / 7.0
	sRndInfo.iMaxTargets = 15
	sRndInfo.fCurrentRoundLength = 75.0
	sRndInfo.iMaxActions = 0
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_BACK_RANGE)
	SET_ROUND_FLAG(sRndInfo, ROUND_TARGETS_ARE_INFINITE)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridB1, 0, TS_HEIGHT_MID | TS_RESPAWN_LONG | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridB3, 0, TS_HEIGHT_LOW | TS_RESPAWN_LONG | TS_GIVE_FLOAT_HELP)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridB5, 0, TS_HEIGHT_MID | TS_RESPAWN_LONG | TS_DONT_PLAY_ENTRY_SOUND)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridA1, 3, TS_HEIGHT_HIGH | TS_RESPAWN_LONG)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridA2, 3, TS_HEIGHT_LOW | TS_RESPAWN_LONG | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridA4, 3, TS_HEIGHT_LOW | TS_RESPAWN_LONG)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridA5, 3, TS_HEIGHT_HIGH | TS_RESPAWN_LONG | TS_DONT_PLAY_ENTRY_SOUND)

	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridB2, 7, TS_HEIGHT_MID | TS_RESPAWN_LONG)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridC2, 7, TS_HEIGHT_HIGH | TS_RESPAWN_LONG | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridC4, 7, TS_HEIGHT_HIGH | TS_RESPAWN_LONG)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridB4, 7, TS_HEIGHT_MID | TS_RESPAWN_LONG | TS_DONT_PLAY_ENTRY_SOUND)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridC1, 11, TS_HEIGHT_LOW | TS_RESPAWN_LONG)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridC5, 11, TS_HEIGHT_LOW | TS_RESPAWN_LONG)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridA3, 11, TS_HEIGHT_HIGH | TS_RESPAWN_LONG | TS_DONT_PLAY_ENTRY_SOUND)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridC3, 11, TS_HEIGHT_MID | TS_RESPAWN_LONG | TS_DONT_PLAY_ENTRY_SOUND)
	
	// D1-D5
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_RailgunChallenge_1(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 10 Targets
	sRndInfo.iMaxTargets = 18
	sRndInfo.fCurrentRoundLength = 55.0
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP + 500
	sRndInfo.eRoundType = RT_RAILGUN_CHAL_1
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_FRAGGABLE_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_DESTRUCTION_W_BONUS
	
	// One at a time
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA3, 0)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 8.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridD3, 1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 8.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridD2, 2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 3.0, AB_MOVE_AT_TIME_SLOW, gridC4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridD4, 3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 3.0, AB_MOVE_AT_TIME_SLOW, gridC2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Two at a time
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridB2, 4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 3, AB_MOVE_AT_TIME_SLOW, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 12.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridC1, 4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 4.5, AB_MOVE_AT_TIME_SLOW, gridC3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 12.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Two at a time
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridB4, 6)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 3, AB_MOVE_AT_TIME_SLOW, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridC5, 6)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 4.5, AB_MOVE_AT_TIME_SLOW, gridC3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Three at a time
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridB2, 8)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 2.2, AB_MOVE_AT_TIME_SLOW, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridB3, 8)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 2.0, AB_MOVE_AT_TIME_SLOW, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridB4, 8)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 2.7, AB_MOVE_AT_TIME_SLOW, gridA2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Three at a time
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridD4, 11)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 2.2, AB_MOVE_AT_TIME_SLOW, gridC3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridD3, 11)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 2.0, AB_MOVE_AT_TIME_SLOW, gridC2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridD2, 11)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 2.7, AB_MOVE_AT_TIME_SLOW, gridC4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Four at a time
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridD2, 14)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 2.0, AB_MOVE_AT_TIME_SLOW, gridC4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 9.0, AB_MOVE_AT_TIME_SLOW, gridB2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 16.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[15], gridD4, 14)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 2.5, AB_MOVE_AT_TIME_SLOW, gridB3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 9.0, AB_MOVE_AT_TIME_SLOW, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 16.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[16], gridB4, 14)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 16, 3.0, AB_MOVE_AT_TIME_SLOW, gridC2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 16, 9.0, AB_MOVE_AT_TIME_SLOW, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 16, 16.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[17], gridB2, 14)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 17, 3.5, AB_MOVE_AT_TIME_SLOW, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 17, 9.0, AB_MOVE_AT_TIME_SLOW, gridB4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 17, 16.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Our Actions
	sRndInfo.iMaxActions = 38
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_RailgunChallenge_2(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 16 Targets
	sRndInfo.iMaxTargets = 16
	sRndInfo.fCurrentRoundLength = 45.0
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP + 300
	sRndInfo.eRoundType = RT_RAILGUN_CHAL_2
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_BACK_RANGE)
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_FRAGGABLE_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_DESTRUCTION_W_BONUS
	
	// Our Actions
	sRndInfo.iMaxActions = 32
	
	// 2 targets crossing paths. -- 4 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridA4, 0)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 1, AB_MOVE_AT_TIME_SLOW, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 7.5, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridA2, 0)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 1.25, AB_MOVE_AT_TIME_SLOW, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 7.5, AB_DESTROY_AFTER_TIME_CREATED)	
		
	// 3 targets crossing paths. -- 6 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridA4, 2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 2.0, AB_MOVE_AT_TIME_NORMAL, gridB4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 7.5, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridA5, 2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 3.0, AB_MOVE_AT_TIME_NORMAL, gridB3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 7.5, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridB3, 2, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 2.4, AB_MOVE_AT_TIME_NORMAL, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 7.5, AB_DESTROY_AFTER_TIME_CREATED)
	
	
	// 2 targets crossing paths. -- 4 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridC1, 5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 2.0, AB_MOVE_AT_TIME_NORMAL, gridA3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 7.5, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridB4, 5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 2.0, AB_MOVE_AT_TIME_NORMAL, gridB2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 7.5, AB_DESTROY_AFTER_TIME_CREATED)
	
	// 3 targets crossing paths. -- 6 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridC2, 7)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 2.0, AB_MOVE_AT_TIME_NORMAL, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 7.5, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridC1, 7)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 3.0, AB_MOVE_AT_TIME_NORMAL, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 7.5, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridD3, 7, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 2.4, AB_MOVE_AT_TIME_NORMAL, gridC2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 7.5, AB_DESTROY_AFTER_TIME_CREATED)
	
	// 4 targets crossing paths. -- 8 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridA5, 10)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 2.0, AB_MOVE_AT_TIME_SLOW, gridC4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridA4, 10)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 2.25, AB_MOVE_AT_TIME_SLOW, gridC5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridD4, 10, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 2.0, AB_MOVE_AT_TIME_VERY_SLOW, gridB1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridD1, 10, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 2.25, AB_MOVE_AT_TIME_VERY_SLOW, gridB4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 11.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// 2 targets crossing paths. -- 4 actions.
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridC2, 14)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 2.5, AB_MOVE_AT_TIME_NORMAL, gridC4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 7.5, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[15], gridC4, 14)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 2.75, AB_MOVE_AT_TIME_NORMAL, gridC2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 7.5, AB_DESTROY_AFTER_TIME_CREATED)
	
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_RailgunChallenge_3(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 15 Targets
	sRndInfo.iMaxTargets = 20
	sRndInfo.fCurrentRoundLength = 45.0
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP + 200
	sRndInfo.eRoundType = RT_RAILGUN_CHAL_3
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_FRAGGABLE_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_DESTRUCTION_W_BONUS
	

	
	// Two targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridC3, 0, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 0.5, AB_MOVE_AT_TIME_FAST, gridC5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridC5, 0, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 1.5, AB_MOVE_AT_TIME_NORMAL, gridB1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Three targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridB3, 2, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 1.7, AB_MOVE_AT_TIME_NORMAL, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridB1, 2, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 2.0, AB_MOVE_AT_TIME_NORMAL, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridB5, 2, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 2.3, AB_MOVE_AT_TIME_NORMAL, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Two targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridA1, 5, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 3.3, AB_MOVE_AT_TIME_FAST, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridD1, 5, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 3.3, AB_MOVE_AT_TIME_FAST, gridB2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Three targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridC3, 7, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 1.7, AB_MOVE_AT_TIME_FAST, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridC4, 7, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 2.0, AB_MOVE_AT_TIME_FAST, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridC2, 7, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 2.3, AB_MOVE_AT_TIME_FAST, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Four targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridB3, 10, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 2.7, AB_MOVE_AT_TIME_NORMAL, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridB5, 10, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 2.1, AB_MOVE_AT_TIME_NORMAL, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridB1, 10, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 2.4, AB_MOVE_AT_TIME_NORMAL, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridA5, 10, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 1.6, AB_MOVE_AT_TIME_SLOW, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Two targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridD1, 14, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 2.8, AB_MOVE_AT_TIME_NORMAL, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[15], gridD5, 14, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 2.9, AB_MOVE_AT_TIME_NORMAL, gridA1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Four targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[16], gridC2, 16, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 16, 2.7, AB_MOVE_AT_TIME_FAST, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 16, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[17], gridC1, 16, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 17, 2.1, AB_MOVE_AT_TIME_FAST, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 17, 9.0, AB_DESTROY_WHEN_AT_LOC, gridD3)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[18], gridC3, 16, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 18, 2.4, AB_MOVE_AT_TIME_FAST, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 18, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[19], gridB1, 16, TS_HEIGHT_HIGH | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 19, 1.6, AB_MOVE_AT_TIME_NORMAL, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 19, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Our Actions
	sRndInfo.iMaxActions = 40
ENDPROC

/// PURPOSE:
///    Constructor for shooting range rounds
PROC Setup_RailgunChallenge_4(Range_RoundInfo & sRndInfo, Range_Targets & sTargData)
	// 15 Targets
	sRndInfo.iMaxTargets = 23
	sRndInfo.fCurrentRoundLength = 60.0
	
	sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP + 200
	sRndInfo.eRoundType = RT_RAILGUN_CHAL_4
	
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_FRAGGABLE_TARGETS)
	SET_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	SET_ROUND_FLAG(sRndInfo, ROUND_USE_BACK_RANGE)
	
	sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_DESTRUCTION_W_BONUS
	
	
	
	//Three targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[0], gridD1, 0, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 1.1, AB_MOVE_AT_TIME_NORMAL, gridB5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 0, 1.0, AB_DESTROY_WHEN_AT_LOC, gridB5)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[1], gridD4, 0, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 1.5, AB_MOVE_AT_TIME_NORMAL, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 1, 0.0, AB_DESTROY_WHEN_AT_LOC, gridA4)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[2], gridD5, 0, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 1.3, AB_MOVE_AT_TIME_NORMAL, gridB2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 2, 0.0, AB_DESTROY_WHEN_AT_LOC, gridB2)
	
	// Three targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[3], gridA3, 3, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 1.9, AB_MOVE_AT_TIME_NORMAL, gridC1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 3, 9.0, AB_DESTROY_WHEN_AT_LOC, gridC1)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[4], gridB1, 3, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 3.5, AB_MOVE_AT_TIME_FAST, gridC5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 4, 9.0, AB_DESTROY_WHEN_AT_LOC, gridC5)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[5], gridD5, 3, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 3.2, AB_MOVE_AT_TIME_SLOW, gridB5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 5, 9.0, AB_DESTROY_AFTER_TIME_CREATED, gridB5)
	
	
	//Two targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[6], gridD5, 6, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 1.6, AB_MOVE_AT_TIME_FAST, gridB4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 6, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[7], gridC5, 6, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 3.0, AB_MOVE_AT_TIME_FAST, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 7, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	//Two targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[8], gridD1, 8, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 1.6, AB_MOVE_AT_TIME_NORMAL, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 8, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[9], gridC1, 8, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 1.6, AB_MOVE_AT_TIME_NORMAL, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 9, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Three targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[10], gridA2, 10, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 3.0, AB_MOVE_AT_TIME_FAST, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 10, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[11], gridA3, 10, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 2.3, AB_MOVE_AT_TIME_FAST, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 11, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[12], gridA1, 10, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 2.6, AB_MOVE_AT_TIME_FAST, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 12, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Four targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[13], gridB3, 13, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 3.0, AB_MOVE_AT_TIME_NORMAL, gridD3)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 13, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[14], gridB5, 13, TS_HEIGHT_LOW | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 2.4, AB_MOVE_AT_TIME_NORMAL, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 14, 9.0, AB_DESTROY_WHEN_AT_LOC, gridD1)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[15], gridB1, 13, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 2.7, AB_MOVE_AT_TIME_NORMAL, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 15, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[16], gridA5, 13, TS_HEIGHT_MID | TS_DONT_PLAY_ENTRY_SOUND)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 16, 1.9, AB_MOVE_AT_TIME_SLOW, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 16, 9.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Two targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[17], gridA2, 17, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 17, 3.5, AB_MOVE_AT_TIME_NORMAL, gridD5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 17, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[18], gridA4, 17, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 18, 3.6, AB_MOVE_AT_TIME_NORMAL, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 18, 10.0, AB_DESTROY_AFTER_TIME_CREATED)
	
	// Two targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[19], gridA1, 19, TS_HEIGHT_MID)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 19, 1.4, AB_MOVE_AT_TIME_VERYFAST, gridD2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 19, 3.4, AB_MOVE_AT_TIME_VERYFAST, gridA4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 19, 11.0, AB_DESTROY_WHEN_AT_LOC, gridA4)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[20], gridA5, 19, TS_HEIGHT_LOW)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 20, 1.6, AB_MOVE_AT_TIME_VERYFAST, gridD4)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 20, 3.6, AB_MOVE_AT_TIME_VERYFAST, gridA2)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 20, 11.0, AB_DESTROY_WHEN_AT_LOC, gridA2)

	//Two targets
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[21], gridA1, 21, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 21, 1.7, AB_MOVE_AT_TIME_FAST, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 21, 4.7, AB_MOVE_AT_TIME_FAST, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 21, 11.0, AB_DESTROY_WHEN_AT_LOC, gridD1)
	SETUP_TARGET_ENTITY(sTargData.sTargetEntities[22], gridD5, 21, TS_HEIGHT_HIGH)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 22, 1.5, AB_MOVE_AT_TIME_FAST, gridD1)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 22, 4.5, AB_MOVE_AT_TIME_FAST, gridA5)
	TARGETACTION_CREATE_ACTION(sTargData, sRndInfo, 22, 9.0, AB_DESTROY_WHEN_AT_LOC, gridA5)
	
	// Our Actions
	sRndInfo.iMaxActions = 50
ENDPROC

PROC RANGE_SetupRound(Range_CoreData & sCore, Range_RoundInfo & sRndInfo, Range_Targets & sTargData, Range_RangeData & sRangeInfo)
	// Reset all round flags we need to.
	CLEAR_ROUND_FLAG(sRndInfo, ROUND_USE_METAL_TARGETS)
	CLEAR_ROUND_FLAG(sRndInfo, ROUND_USE_FRAGGABLE_TARGETS)
	CLEAR_ROUND_FLAG(sRndInfo, ROUND_USE_BACK_RANGE)
	CLEAR_ROUND_FLAG(sRndInfo, ROUND_TIME_IS_UP)
	CLEAR_ROUND_FLAG(sRndInfo, ROUND_PLAYED_10_SEC_WARNING)
	CLEAR_ROUND_FLAG(sRndInfo, ROUND_PLAYED_5_SEC_WARNING)
	CLEAR_ROUND_FLAG(sRndInfo, ROUND_TARGETS_ARE_INFINITE)
	CLEAR_ROUND_FLAG(sRndInfo, ROUND_NO_TIME_BONUS)
	CLEAR_ROUND_FLAG(sRndInfo, ROUND_HVY3_FLOAT_HELP)
	CLEAR_ROUND_FLAG(sRndInfo, ROUND_ALLOW_TIME_EXPIRY)

	
	// Which did we select?
	SWITCH (sRndInfo.eChallengeType)
		CASE WEAPCHAL_1
			// Which weapon?
			SWITCH (sRndInfo.eWeaponCat)
				CASE WEAPCAT_PISTOL
					Setup_PistolChallenge_1(sRndInfo, sTargData)
				BREAK

				CASE WEAPCAT_SMG
					Setup_SMGChallenge_1(sRndInfo, sTargData)
				BREAK

				CASE WEAPCAT_AR
					Setup_AssaultRifleChallenge_1(sRndInfo, sTargData)
				BREAK

				CASE WEAPCAT_SHOTGUN
					Setup_ShotgunChallenge_1(sRndInfo, sTargData)
				BREAK
				
				CASE WEAPCAT_MINIGUN
					Setup_MinigunChallenge_1(sRndInfo, sTargData)
				BREAK
				
				CASE WEAPCAT_LIGHT_MG
					Setup_LMGChallenge_1(sRndInfo, sTargData)
				BREAK
				
				//NG Current gen owners only
				CASE WEAPCAT_RAILGUN
					Setup_RailgunChallenge_1(sRndInfo, sTargData)
				BREAK

			ENDSWITCH	
		BREAK
		
		CASE WEAPCHAL_2
			// Which weapon?
			SWITCH (sRndInfo.eWeaponCat)
				CASE WEAPCAT_PISTOL
					Setup_PistolChallenge_2(sRndInfo, sTargData)
				BREAK

				CASE WEAPCAT_SMG
					Setup_SMGChallenge_2(sRndInfo, sTargData)
				BREAK

				CASE WEAPCAT_AR
					Setup_AssaultRifleChallenge_2(sRndInfo, sTargData)
				BREAK

				CASE WEAPCAT_SHOTGUN
					Setup_ShotgunChallenge_2(sRndInfo, sTargData)
				BREAK
				
				CASE WEAPCAT_MINIGUN
					Setup_MinigunChallenge_2(sRndInfo, sTargData)
				BREAK
				
				CASE WEAPCAT_LIGHT_MG
					Setup_LMGChallenge_2(sRndInfo, sTargData)
				BREAK
				
				//NG Current gen owners only
				CASE WEAPCAT_RAILGUN
					Setup_RailgunChallenge_2(sRndInfo, sTargData)
				BREAK
				
			ENDSWITCH	
		BREAK
		
		CASE WEAPCHAL_3
			// Which weapon?
			SWITCH (sRndInfo.eWeaponCat)
				CASE WEAPCAT_PISTOL
					Setup_PistolChallenge_3(sRndInfo, sTargData)
				BREAK

				CASE WEAPCAT_SMG
					Setup_SMGChallenge_3(sRndInfo, sTargData)
				BREAK

				CASE WEAPCAT_AR
					Setup_AssaultRifleChallenge_3(sRndInfo, sTargData)
				BREAK

				CASE WEAPCAT_SHOTGUN
					Setup_ShotgunChallenge_3(sRndInfo, sTargData)
				BREAK
				
				CASE WEAPCAT_MINIGUN
					Setup_MinigunChallenge_3(sRndInfo, sTargData)
				BREAK
				
				CASE WEAPCAT_LIGHT_MG
					Setup_LMGChallenge_3(sRndInfo, sTargData)
				BREAK
				
				//NG Current gen owners only
				CASE WEAPCAT_RAILGUN
					Setup_RailgunChallenge_3(sRndInfo, sTargData)
				BREAK
				
			ENDSWITCH	
		BREAK
		
		CASE WEAPCHAL_4
			// Which weapon?
			SWITCH (sRndInfo.eWeaponCat)

				CASE WEAPCAT_RAILGUN
					Setup_RailgunChallenge_4(sRndInfo, sTargData)
				BREAK
				
			ENDSWITCH	
		BREAK
		
	ENDSWITCH
	
	PRE_START_ROUND(sCore, sRndInfo, sRangeInfo)
ENDPROC
