// Range_Round.sch
USING "Range_Score.sch"

CONST_FLOAT			CONST_GraceResetTime  1.0
TWEAK_INT			k_RANGE_TARGET_HIT_BUFFER	30 // in milliseconds

/// PURPOSE: Used to store data related to our grace period for auto weapons.
STRUCT Range_Grace
	BOOL			bGraceUsed			// Single shot grace period when firing automatic weapons, so he doens't lose multiplier right away.
	structTimer		tGraceResetTimer	// Eventually reset the grace period timer.	
ENDSTRUCT

ENUM ROUND_INFO_FLAGS
	ROUND_SHOT_FIRED			= BIT0,
	ROUND_TARGET_HIT			= BIT1,
	
	ROUND_PASS_NOISE_PLAYED		= BIT5,
	ROUND_WIPE_CAM				= BIT6,
	
	ROUND_CAN_DISPLAY_TARGET_HELP = BIT7,
	
	// You set these.
	ROUND_USE_FRAGGABLE_TARGETS = BIT9,
	ROUND_USE_METAL_TARGETS 	= BIT10,
	ROUND_USE_BACK_RANGE 		= BIT11,
	
	ROUND_NO_TIME_BONUS			= BIT16,
	
	ROUND_TARGETS_ARE_INFINITE 	= BIT20,
	ROUND_ALLOW_TIME_EXPIRY		= BIT21,
	ROUND_AWARDED_PERCENTAGE	= BIT22,
	
	// Don't set these.
	ROUND_PLAYER_OWNS_WEAPON 	= BIT12,
	ROUND_TIME_IS_UP			= BIT13,
	ROUND_PLAYED_10_SEC_WARNING = BIT14,
	ROUND_PLAYED_5_SEC_WARNING	= BIT15,
	ROUND_FAIL_TARGET_CLEANUP	= BIT17,
	
	ROUND_PLAYER_OWNS_HG_FLASHLIGHT = BIT18,
	ROUND_PLAYER_OWNS_AR_FLASHLIGHT = BIT19,

	// Or this.
	ROUND_HVY3_FLOAT_HELP		= BIT30
ENDENUM

/// PURPOSE: This struct tends to track things on a per-loop basis.
///    There's a few things in here that aren't, like total shots.
STRUCT Range_RoundInfo
	INT iTotalShots
	INT iZ1Hits
	INT iZ2Hits
	INT iZ3Hits
	INT iZ4Hits
	INT iTargetsDestroyed
	INT iMaxTargets
	INT iStoredAmmoCount
	INT iMaxActions
	
	INT iTargetsHitThisExplosion = 0
	
	INT iRoundFlags
	INT iSFX_Warning = -1
	INT iBeepTimeStamp
	
	RANGE_ROUND_TYPE		eRoundType
	RANGE_WEAPON_CATEGORY	eWeaponCat
	RANGE_WEAPON_TYPE		eWeaponType
	RANGE_WEAPON_CHALLENGES eChallengeType
	
	FLOAT fCurrentBulletDamage
	FLOAT fCurrentRoundLength
	FLOAT fTotalTime			// I tihnk this is the total time taken...
	FLOAT fStartTime
	FLOAT fNoAmmoStamp			// No clue what this is.
	FLOAT fTotalPauseTime
	FLOAT fCurrentPauseTime
	
	structTimer sTimeLeft
	structTimer	sGenTimer		// "Use to control misc things."
	
	Range_Grace 			sGraceInfo
	Range_MultiplierData 	sMPData
	Range_ScoreData			sScoreData
	
	INT iTargetHitsCached
	INT iTargetHitsBuffer
ENDSTRUCT

/// PURPOSE: Contains info on a specific challenge.
STRUCT Range_Challenge
	RANGE_ROUND_TYPE	eRndType
	RANGE_ROUND_TYPE	eUnlockReq
	ROUND_SCORING_STYLE	eScoringStyle
	
	INT iZ1Hits
	INT iZ2Hits
	INT iZ3Hits
	INT iZ4Hits
	
	INT					iBronzeReq
	INT					iSilverReq
	INT					iGoldReq
	
ENDSTRUCT

/// PURPOSE: This is the info associated with all rounds:
///    - If it's unlocked
///    - Round type
///    - Unlock Req
///    - Medal thresholds
///    This is meant to be init once, and then only the bUnlock will really change.
STRUCT Range_WeaponRounds
	Range_Challenge		sChallenges[NUM_WEAPON_CHALLS]
ENDSTRUCT

PROC PLAY_RANGE_SOUND(STRING sSound, BOOL bHUDDefaultSet = FALSE)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "PLAY_RANGE_SOUND :: called, playing ", sSound)
	
	IF bHUDDefaultSet
		PLAY_SOUND_FRONTEND(-1, sSound, "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ELSE
		PLAY_SOUND_FRONTEND(-1, sSound, "HUD_MINI_GAME_SOUNDSET")
	ENDIF
ENDPROC

PROC RANGE_INCREMENT_ZONE_HITS(Range_RoundInfo & sRndInfo, INT iZone)
	enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
	STATSENUM eStat
	IF ePed = CHAR_MICHAEL
		eStat = SP0_HITS_PEDS_VEHICLES
	ELIF ePed = CHAR_FRANKLIN
		eStat = SP1_HITS_PEDS_VEHICLES
	ELIF ePed = CHAR_TREVOR
		eStat = SP2_HITS_PEDS_VEHICLES
	ENDIF
	SWITCH iZone
		CASE 1
			sRndInfo.iZ1Hits++
			STAT_INCREMENT(eStat, 1.0)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_INCREMENT_ZONE_HITS: Incrementing zone ", iZone, " hits to ", sRndInfo.iZ1Hits, ", ePed=", ePed, ", eStat=", eStat)
		BREAK
		CASE 2
			sRndInfo.iZ2Hits++
			STAT_INCREMENT(eStat, 1.0)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_INCREMENT_ZONE_HITS: Incrementing zone ", iZone, " hits to ", sRndInfo.iZ2Hits, ", ePed=", ePed, ", eStat=", eStat)
		BREAK
		CASE 3
			sRndInfo.iZ3Hits++
			STAT_INCREMENT(eStat, 1.0)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_INCREMENT_ZONE_HITS: Incrementing zone ", iZone, " hits to ", sRndInfo.iZ3Hits, ", ePed=", ePed, ", eStat=", eStat)
		BREAK
		CASE 4
			sRndInfo.iZ4Hits++
			STAT_INCREMENT(eStat, 1.0)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_INCREMENT_ZONE_HITS: Incrementing zone ", iZone, " hits to ", sRndInfo.iZ4Hits, ", ePed=", ePed, ", eStat=", eStat)
		BREAK
		DEFAULT
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Passing an invalid Zone to RANGE_INCREMENT_ZONE_HITS :: ", iZone)
		BREAK
	ENDSWITCH
ENDPROC

FUNC ROUND_SCORING_STYLE GET_ROUND_SCORE_STYLE(Range_RoundInfo & sCurRoundInfo)
	RETURN sCurRoundInfo.sScoreData.eScoreStyle
ENDFUNC


/// PURPOSE:
///    Sets a round type to unlocked
/// PARAMS:
///    eRoundType - The round type to be unlocked
PROC SET_ROUND_UNLOCKED(RANGE_ROUND_TYPE eRoundType, BOOL bUnlocked = TRUE)
	g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].bUnlocked	= bUnlocked
ENDPROC

/// PURPOSE:
///    Sets a round type to unlocked
/// PARAMS:
///    eRoundType - The round type to be unlocked
PROC SET_ROUND_PASSED(RANGE_ROUND_TYPE eRoundType, BOOL bPassed = TRUE)
	g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].bPassed = bPassed
ENDPROC

/// PURPOSE:
///    STores zone hits for a round, but only if they surpass the zone hits from a previous round.
PROC SET_ROUND_ZONE_HITS(RANGE_ROUND_TYPE eRoundType, INT iScore, INT iZ1, INT iZ2, INT iZ3, INT iZ4)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_ROUND_ZONE_HITS :: Called")
	IF (iScore > GET_RANGE_SP_HIGH_SCORE(ePlayerChar, eRoundType))
		CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_ROUND_ZONE_HITS :: Higher Score")
		g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].iZone1Hit = iZ1
		g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].iZone2Hit = iZ2
		g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].iZone3Hit = iZ3
		g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].iZone4Hit = iZ4
		CDEBUG2LN(DEBUG_SHOOTRANGE, "g_savedGlobals.sRangeData[ePlayerChar].sRounds[", eRoundType, "].iZone1Hit= ", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].iZone1Hit, ", iZ1=", iZ1)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "g_savedGlobals.sRangeData[ePlayerChar].sRounds[", eRoundType, "].iZone2Hit= ", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].iZone2Hit, ", iZ2=", iZ2)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "g_savedGlobals.sRangeData[ePlayerChar].sRounds[", eRoundType, "].iZone3Hit= ", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].iZone3Hit, ", iZ3=", iZ3)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "g_savedGlobals.sRangeData[ePlayerChar].sRounds[", eRoundType, "].iZone4Hit= ", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].iZone4Hit, ", iZ4=", iZ4)
	ENDIF
ENDPROC


/// PURPOSE:
///		Checks if the certain round is passed (CheckRoundPassed)
/// RETURNS:
///	    True if this round is passed
FUNC BOOL IS_ROUND_PASSED(RANGE_ROUND_TYPE eRoundType)
	// Check our saved globals
	RETURN g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].bPassed	
ENDFUNC

/// PURPOSE:
///		Checks if the certain round is unlocked
/// RETURNS:
///	    True if this round is unlocked (met previous requirements)
FUNC BOOL IS_ROUND_UNLOCKED(RANGE_ROUND_TYPE eRoundType)
	// Check our saved globals
	RETURN g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].bUnlocked	
ENDFUNC

/// PURPOSE:
///    Checks if all the range rounds have been passed
/// RETURNS:
///    
FUNC BOOL HANDLE_RANGE_ROUND_COMPLETION_PERCENTAGES()
	BOOL bPistolPassed		= IS_ROUND_PASSED(RT_PISTOL_CHAL_1) 	AND IS_ROUND_PASSED(RT_PISTOL_CHAL_2) 	AND IS_ROUND_PASSED(RT_PISTOL_CHAL_3)
	BOOL bSMGPassed 		= IS_ROUND_PASSED(RT_SMG_CHAL_1) 		AND IS_ROUND_PASSED(RT_SMG_CHAL_2) 		AND IS_ROUND_PASSED(RT_SMG_CHAL_3)
	BOOL bARPassed 			= IS_ROUND_PASSED(RT_AR_CHAL_1) 		AND IS_ROUND_PASSED(RT_AR_CHAL_2) 		AND IS_ROUND_PASSED(RT_AR_CHAL_3)
	BOOL bShotgunPassed 	= IS_ROUND_PASSED(RT_SHOTGUN_CHAL_1) 	AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_2) 	AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_3)
	BOOL bLMGPassed 		= IS_ROUND_PASSED(RT_LMG_CHAL_1) 		AND IS_ROUND_PASSED(RT_LMG_CHAL_2) 		AND IS_ROUND_PASSED(RT_LMG_CHAL_3)
	BOOL bHeavyPassed 		= IS_ROUND_PASSED(RT_MINIGUN_CHAL_1) 	AND IS_ROUND_PASSED(RT_MINIGUN_CHAL_2) 	AND IS_ROUND_PASSED(RT_MINIGUN_CHAL_3)
	
	IF bPistolPassed	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_MG_SHHAN)	ENDIF
	IF bSMGPassed		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_MG_SHSUB)	ENDIF
	IF bARPassed		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_MG_SHRIF)	ENDIF
	IF bShotgunPassed	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_MG_SHSHO)	ENDIF
	IF bLMGPassed		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_MG_SHLMG)	ENDIF
	IF bHeavyPassed		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_MG_SHHVY)	ENDIF
	
	RETURN bPistolPassed AND bSMGPassed AND bARPassed AND bShotgunPassed AND bLMGPassed AND bHeavyPassed
ENDFUNC

/// PURPOSE:
///    Returns the current highest medal for the specified round
/// PARAMS:
///    eRoundType - Which round type we are inquiring about
/// RETURNS:
///    The highest medal the player has earned in this round
FUNC RANGE_ROUND_MEDAL GET_ROUND_MEDAL(RANGE_ROUND_TYPE eRoundType)
	RETURN g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].eTopMedal	
ENDFUNC

/// PURPOSE:
///    Sets our save data to store the medal for the specified round
/// PARAMS:
///    eRoundType - The round type
///    eMedal - The medal we are awarding
PROC SET_ROUND_MEDAL(RANGE_ROUND_TYPE eRoundType, RANGE_ROUND_MEDAL eMedal)
	g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].eTopMedal	= eMedal
	SET_ROUND_UNLOCKED(eRoundType, TRUE)
	SET_ROUND_PASSED(eRoundType, TRUE)
	
	PLAY_RANGE_SOUND("MEDAL_UP")
	
	CPRINTLN( DEBUG_SHOOTRANGE, "SET_ROUND_MEDAL -- Awarding ", GET_STRING_FROM_RANGE_ROUND_MEDAL( eMedal ), " medal, ePlayerChar=", ePlayerChar, ", eRoundType=", GET_STRING_FROM_RANGE_ROUND_TYPE( eRoundType ) )
ENDPROC

/// PURPOSE:
///    Returns whether or not the player earned a bronze on the challenge passed in.
/// RETURNS:
///    TRUE if the player earned a bronze or better.
FUNC BOOL WAS_CHALLENGE_BRONZED(RANGE_ROUND_TYPE eRoundType)
	RETURN (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].eTopMedal = RRM_GOLD)
	OR (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].eTopMedal = RRM_SILVER)
	OR(g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].eTopMedal = RRM_BRONZE)
ENDFUNC

/// PURPOSE:
///    Returns whether or not the player earned a silver on the challenge passed in.
/// RETURNS:
///    TRUE if the player earned a silver or better.
FUNC BOOL WAS_CHALLENGE_SILVERED(RANGE_ROUND_TYPE eRoundType)
	RETURN (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].eTopMedal = RRM_GOLD)
	OR (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].eTopMedal = RRM_SILVER)
ENDFUNC

/// PURPOSE:
///    Returns whether or not the player earned a gold on the challenge passed in.
/// RETURNS:
///    TRUE if the player earned a gold.
FUNC BOOL WAS_CHALLENGE_GOLDED(RANGE_ROUND_TYPE eRoundType)
	RETURN (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].eTopMedal = RRM_GOLD)
ENDFUNC

/// PURPOSE:
///    Checks if all the range rounds have bronze medals or better
/// RETURNS:
///    
FUNC BOOL ARE_ALL_RANGE_ROUNDS_BRONZED()
	BOOL bPistol = WAS_CHALLENGE_BRONZED(RT_PISTOL_CHAL_1) 		AND WAS_CHALLENGE_BRONZED(RT_PISTOL_CHAL_2) 	AND WAS_CHALLENGE_BRONZED(RT_PISTOL_CHAL_3)
	BOOL bSMG = WAS_CHALLENGE_BRONZED(RT_SMG_CHAL_1) 			AND WAS_CHALLENGE_BRONZED(RT_SMG_CHAL_2) 		AND WAS_CHALLENGE_BRONZED(RT_SMG_CHAL_3)
	BOOL bAR = WAS_CHALLENGE_BRONZED(RT_AR_CHAL_1) 				AND WAS_CHALLENGE_BRONZED(RT_AR_CHAL_2)			AND WAS_CHALLENGE_BRONZED(RT_AR_CHAL_3)
	BOOL bShotgun = WAS_CHALLENGE_BRONZED(RT_SHOTGUN_CHAL_1)	AND WAS_CHALLENGE_BRONZED(RT_SHOTGUN_CHAL_2)	AND WAS_CHALLENGE_BRONZED(RT_SHOTGUN_CHAL_3)
	BOOL bLMG = WAS_CHALLENGE_BRONZED(RT_LMG_CHAL_1) 			AND WAS_CHALLENGE_BRONZED(RT_LMG_CHAL_2) 		AND WAS_CHALLENGE_BRONZED(RT_LMG_CHAL_3)
	BOOL bHeavy = WAS_CHALLENGE_BRONZED(RT_MINIGUN_CHAL_1)		AND WAS_CHALLENGE_BRONZED(RT_MINIGUN_CHAL_2) 	AND WAS_CHALLENGE_BRONZED(RT_MINIGUN_CHAL_3)
	RETURN bPistol AND bSMG AND bAR AND bShotgun AND bLMG AND bHeavy
ENDFUNC

/// PURPOSE:
///    Checks if all the range rounds have been silver medals or better
/// RETURNS:
///    
FUNC BOOL ARE_ALL_RANGE_ROUNDS_SILVERED()
	BOOL bPistol = WAS_CHALLENGE_SILVERED(RT_PISTOL_CHAL_1) 	AND WAS_CHALLENGE_SILVERED(RT_PISTOL_CHAL_2) 	AND WAS_CHALLENGE_SILVERED(RT_PISTOL_CHAL_3)
	BOOL bSMG = WAS_CHALLENGE_SILVERED(RT_SMG_CHAL_1) 			AND WAS_CHALLENGE_SILVERED(RT_SMG_CHAL_2) 		AND WAS_CHALLENGE_SILVERED(RT_SMG_CHAL_3)
	BOOL bAR = WAS_CHALLENGE_SILVERED(RT_AR_CHAL_1) 			AND WAS_CHALLENGE_SILVERED(RT_AR_CHAL_2) 		AND WAS_CHALLENGE_SILVERED(RT_AR_CHAL_3)
	BOOL bShotgun = WAS_CHALLENGE_SILVERED(RT_SHOTGUN_CHAL_1) 	AND WAS_CHALLENGE_SILVERED(RT_SHOTGUN_CHAL_2)	AND WAS_CHALLENGE_SILVERED(RT_SHOTGUN_CHAL_3)
	BOOL bLMG = WAS_CHALLENGE_SILVERED(RT_LMG_CHAL_1) 			AND WAS_CHALLENGE_SILVERED(RT_LMG_CHAL_2)		AND WAS_CHALLENGE_SILVERED(RT_LMG_CHAL_3)
	BOOL bHeavy = WAS_CHALLENGE_SILVERED(RT_MINIGUN_CHAL_1) 	AND WAS_CHALLENGE_SILVERED(RT_MINIGUN_CHAL_2) 	AND WAS_CHALLENGE_SILVERED(RT_MINIGUN_CHAL_3)
	RETURN bPistol AND bSMG AND bAR AND bShotgun AND bLMG AND bHeavy
ENDFUNC

/// PURPOSE:
///    Checks if all the range rounds have been passed
/// RETURNS:
///    
FUNC BOOL ARE_ALL_RANGE_ROUNDS_GOLDED()
	BOOL bPistol = WAS_CHALLENGE_GOLDED(RT_PISTOL_CHAL_1) 	AND WAS_CHALLENGE_GOLDED(RT_PISTOL_CHAL_2) 	AND WAS_CHALLENGE_GOLDED(RT_PISTOL_CHAL_3)
	BOOL bSMG = WAS_CHALLENGE_GOLDED(RT_SMG_CHAL_1) 		AND WAS_CHALLENGE_GOLDED(RT_SMG_CHAL_2) 	AND WAS_CHALLENGE_GOLDED(RT_SMG_CHAL_3)
	BOOL bAR = WAS_CHALLENGE_GOLDED(RT_AR_CHAL_1) 			AND WAS_CHALLENGE_GOLDED(RT_AR_CHAL_2) 		AND WAS_CHALLENGE_GOLDED(RT_AR_CHAL_3)
	BOOL bShotgun = WAS_CHALLENGE_GOLDED(RT_SHOTGUN_CHAL_1) AND WAS_CHALLENGE_GOLDED(RT_SHOTGUN_CHAL_2) AND WAS_CHALLENGE_GOLDED(RT_SHOTGUN_CHAL_3)
	BOOL bLMG = WAS_CHALLENGE_GOLDED(RT_LMG_CHAL_1) 		AND WAS_CHALLENGE_GOLDED(RT_LMG_CHAL_2) 	AND WAS_CHALLENGE_GOLDED(RT_LMG_CHAL_3)
	BOOL bHeavy = WAS_CHALLENGE_GOLDED(RT_MINIGUN_CHAL_1) 	AND WAS_CHALLENGE_GOLDED(RT_MINIGUN_CHAL_2) AND WAS_CHALLENGE_GOLDED(RT_MINIGUN_CHAL_3)
	RETURN bPistol AND bSMG AND bAR AND bShotgun AND bLMG AND bHeavy
ENDFUNC

/// PURPOSE:
///    Set all range rounds to a specific medal
/// RETURNS:
///    
PROC SET_ALL_RANGE_ROUNDS_MEDAL(RANGE_ROUND_MEDAL newMedal)
	SET_ROUND_MEDAL(RT_PISTOL_CHAL_1, newMedal)
	SET_ROUND_MEDAL(RT_PISTOL_CHAL_2, newMedal)
	SET_ROUND_MEDAL(RT_PISTOL_CHAL_3, newMedal)
	SET_ROUND_MEDAL(RT_SMG_CHAL_1, newMedal)
	SET_ROUND_MEDAL(RT_SMG_CHAL_2, newMedal)
	SET_ROUND_MEDAL(RT_SMG_CHAL_3, newMedal)
	SET_ROUND_MEDAL(RT_AR_CHAL_1, newMedal)
	SET_ROUND_MEDAL(RT_AR_CHAL_2, newMedal)
	SET_ROUND_MEDAL(RT_AR_CHAL_3, newMedal)
	SET_ROUND_MEDAL(RT_SHOTGUN_CHAL_1, newMedal)
	SET_ROUND_MEDAL(RT_SHOTGUN_CHAL_2, newMedal)
	SET_ROUND_MEDAL(RT_SHOTGUN_CHAL_3, newMedal)
	SET_ROUND_MEDAL(RT_LMG_CHAL_1, newMedal)
	SET_ROUND_MEDAL(RT_LMG_CHAL_2, newMedal)
	SET_ROUND_MEDAL(RT_LMG_CHAL_3, newMedal)
	SET_ROUND_MEDAL(RT_MINIGUN_CHAL_1, newMedal)
	SET_ROUND_MEDAL(RT_MINIGUN_CHAL_2, newMedal)
	SET_ROUND_MEDAL(RT_MINIGUN_CHAL_3, newMedal)
	//NG Current gen owners only
	SET_ROUND_MEDAL(RT_RAILGUN_CHAL_1, newMedal)
	SET_ROUND_MEDAL(RT_RAILGUN_CHAL_2, newMedal)
	SET_ROUND_MEDAL(RT_RAILGUN_CHAL_3, newMedal)
	SET_ROUND_MEDAL(RT_RAILGUN_CHAL_4, newMedal)
ENDPROC

PROC SET_ALL_RANGE_ROUNDS_SPECIFIC()
//	SET_ROUND_MEDAL(RT_PISTOL_CHAL_1, newMedal)
	SET_ROUND_MEDAL(RT_PISTOL_CHAL_2, RRM_SILVER)
	SET_ROUND_MEDAL(RT_PISTOL_CHAL_3, RRM_GOLD)
	SET_ROUND_MEDAL(RT_SMG_CHAL_1, RRM_GOLD)
	SET_ROUND_MEDAL(RT_SMG_CHAL_2, RRM_GOLD)
	SET_ROUND_MEDAL(RT_SMG_CHAL_3, RRM_GOLD)
	SET_ROUND_MEDAL(RT_AR_CHAL_1, RRM_GOLD)
	SET_ROUND_MEDAL(RT_AR_CHAL_2, RRM_GOLD)
	SET_ROUND_MEDAL(RT_AR_CHAL_3, RRM_GOLD)	
	SET_ROUND_MEDAL(RT_SHOTGUN_CHAL_1, RRM_GOLD)
	SET_ROUND_MEDAL(RT_SHOTGUN_CHAL_2, RRM_GOLD)
	SET_ROUND_MEDAL(RT_SHOTGUN_CHAL_3, RRM_GOLD)
	SET_ROUND_MEDAL(RT_LMG_CHAL_1, RRM_GOLD)
	SET_ROUND_MEDAL(RT_LMG_CHAL_2, RRM_GOLD)
	SET_ROUND_MEDAL(RT_LMG_CHAL_3, RRM_GOLD)
	SET_ROUND_MEDAL(RT_MINIGUN_CHAL_1, RRM_GOLD)
	SET_ROUND_MEDAL(RT_MINIGUN_CHAL_2, RRM_GOLD)
	SET_ROUND_MEDAL(RT_MINIGUN_CHAL_3, RRM_GOLD)
	//NG Current gen owners only
	SET_ROUND_MEDAL(RT_RAILGUN_CHAL_1, RRM_GOLD)
	SET_ROUND_MEDAL(RT_RAILGUN_CHAL_2, RRM_GOLD)
	SET_ROUND_MEDAL(RT_RAILGUN_CHAL_3, RRM_GOLD)
	SET_ROUND_MEDAL(RT_RAILGUN_CHAL_4, RRM_GOLD)
ENDPROC


/// PURPOSE:
///    Sets all range rounds to bronze
/// RETURNS:
///    
PROC SET_ALL_RANGE_ROUNDS_BRONZED()
	SET_ALL_RANGE_ROUNDS_MEDAL(RRM_BRONZE)
ENDPROC

/// PURPOSE:
///    Sets all range rounds to silver
/// RETURNS:
///    
PROC SET_ALL_RANGE_ROUNDS_SILVERED()
	SET_ALL_RANGE_ROUNDS_MEDAL(RRM_SILVER)
ENDPROC

/// PURPOSE:
///    Sets all range rounds to gold
/// RETURNS:
///    
PROC SET_ALL_RANGE_ROUNDS_GOLDED()
	SET_ALL_RANGE_ROUNDS_MEDAL(RRM_GOLD)
ENDPROC

/// PURPOSE:
///    Quick wrapper to let us know if the player passed or not.
///    If his score is higher then the goal score, then he has
/// RETURNS:
///    TRUE if the players score is higher then the goal score
FUNC BOOL IS_ROUND_CLEARED(Range_RoundInfo & sCurRoundInfo, Range_Challenge & sRoundChallenge)
	RETURN (sCurRoundInfo.sScoreData.iP1Score >= sRoundChallenge.iBronzeReq)
ENDFUNC

/// PURPOSE:
///    Checks if we passed this round, and if we need to unlock the next round
PROC CHECK_UNLOCK_NEXT_ROUND(Range_RoundInfo & sCurRoundInfo, Range_Challenge & sRoundChallenge)
	// Did we clear the round
	IF IS_ROUND_CLEARED(sCurRoundInfo, sRoundChallenge)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "CheckUnlockNextRound -- Round passed.")	
		SET_ROUND_PASSED(sCurRoundInfo.eRoundType)
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "m_RoundChoice is: ", ENUM_TO_INT(sCurRoundInfo.eRoundType))
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the challenge requirement for this round is met. (IsChallengeReqMet)
/// RETURNS:
///    TRUE if it is.
FUNC BOOL IS_ROUND_CHALLENGE_REQ_MET(RANGE_ROUND_TYPE eUnlockReq, BOOL bIgnoreUnlockReq = FALSE)
	IF (eUnlockReq = RT_INVALID) AND NOT bIgnoreUnlockReq
		RETURN TRUE
		
	ELIF IS_ROUND_PASSED(eUnlockReq)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the high score for a certain round
/// PARAMS:
///    roundType - Which round we want to get our high score
/// RETURNS:
///    Returns the INT value of the high score for the passed in round
FUNC INT GET_ROUND_HIGH_SCORE(RANGE_ROUND_TYPE roundType)
	RETURN GET_RANGE_SP_HIGH_SCORE(ePlayerChar, roundType)
ENDFUNC

/// PURPOSE:
///    Returns the high score for a certain round
/// PARAMS:
///    roundType - Which round we want to get our high score
/// RETURNS:
///    Returns the INT value of the high score for the passed in round
FUNC INT GET_ROUND_LAST_SCORE(RANGE_ROUND_TYPE roundType)
	RETURN g_savedGlobals.sRangeData[ePlayerChar].sRounds[roundType].iLastScore
ENDFUNC

/// PURPOSE:
///    Given a challenge, returns the medal requirement.
/// RETURNS:
///    Score value needed for the medal.
FUNC INT GET_MEDAL_SCORE_REQUIREMENT(Range_Challenge & sTheChallenge, RANGE_ROUND_MEDAL eForMedal)
	IF (eForMedal = RRM_GOLD)
		RETURN sTheChallenge.iGoldReq
	ELIF (eForMedal = RRM_SILVER)
		RETURN sTheChallenge.iSilverReq
	ENDIF
	
	RETURN sTheChallenge.iBronzeReq
ENDFUNC

/// PURPOSE:
///    Given a score, returns the medal earned with that score.
FUNC RANGE_ROUND_MEDAL GET_MEDAL_FOR_SCORE(Range_Challenge & sTheChallenge, INT iScore)
	IF (iScore >= sTheChallenge.iGoldReq)
		RETURN RRM_GOLD
	ELIF (iScore >= sTheChallenge.iSilverReq)
		RETURN RRM_SILVER
	ELIF (iScore >= sTheChallenge.iBronzeReq)
		RETURN RRM_BRONZE
	ENDIF
	
	RETURN RRM_NONE
ENDFUNC

/// PURPOSE:
///    Sets a flag for the given round.
PROC SET_ROUND_FLAG(Range_RoundInfo & sCurRoundInfo, ROUND_INFO_FLAGS eFlagToSet)
	sCurRoundInfo.iRoundFlags |= ENUM_TO_INT(eFlagToSet)
ENDPROC

/// PURPOSE:
///    Clears a flag in the given round's data.
PROC CLEAR_ROUND_FLAG(Range_RoundInfo & sCurRoundInfo, ROUND_INFO_FLAGS eFlagToClear)
	CLEAR_BITMASK_AS_ENUM(sCurRoundInfo.iRoundFlags, eFlagToClear)
ENDPROC

/// PURPOSE:
///    Checks to see if a flag is set for the given round.
FUNC BOOL IS_ROUND_FLAG_SET(Range_RoundInfo & sCurRoundInfo, ROUND_INFO_FLAGS eFlagToChk)
	RETURN (sCurRoundInfo.iRoundFlags & ENUM_TO_INT(eFlagToChk)) <> 0
ENDFUNC

/// PURPOSE:
///    Finds the medal requirement for a round. Takes in a round and medal.
/// PARAMS:
///    eRoundType - The round type you're inquiring about
///    eMedal - The Medal you're checking the requirement for
/// RETURNS:
///    The medal requirement of eMedal. -1 if it fails to find an entry.
FUNC INT RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE(RANGE_ROUND_TYPE eRoundType, RANGE_ROUND_MEDAL eMedal)

	CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE called with ", GET_STRING_FROM_RANGE_ROUND_TYPE(eRoundType), " and ", GET_STRING_FROM_RANGE_ROUND_MEDAL(eMedal) )
	
	SWITCH eRoundType
		CASE RT_PISTOL_CHAL_1
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 1400
				CASE RRM_SILVER		RETURN 2000
				CASE RRM_GOLD		RETURN 2800
			ENDSWITCH
		BREAK
		CASE RT_PISTOL_CHAL_2
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 2000
				CASE RRM_SILVER		RETURN 10000
				CASE RRM_GOLD		RETURN 20000
			ENDSWITCH
		BREAK
		CASE RT_PISTOL_CHAL_3
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 650
				CASE RRM_SILVER		RETURN 3000
				CASE RRM_GOLD		RETURN 15000
			ENDSWITCH
		BREAK
		CASE RT_SMG_CHAL_1
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 4000
				CASE RRM_SILVER		RETURN 6000
				CASE RRM_GOLD		RETURN 12000
			ENDSWITCH
		BREAK
		CASE RT_SMG_CHAL_2
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 1250
				CASE RRM_SILVER		RETURN 2500
				CASE RRM_GOLD		RETURN 7500
			ENDSWITCH
		BREAK
		CASE RT_SMG_CHAL_3
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 3000
				CASE RRM_SILVER		RETURN 5000
				CASE RRM_GOLD		RETURN 10000
			ENDSWITCH
		BREAK
		CASE RT_SHOTGUN_CHAL_1
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 22
				CASE RRM_SILVER		RETURN 32
				CASE RRM_GOLD		RETURN 46
			ENDSWITCH
		BREAK
		CASE RT_SHOTGUN_CHAL_2
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 290
				CASE RRM_SILVER		RETURN 590
				CASE RRM_GOLD		RETURN 700
			ENDSWITCH
		BREAK
		CASE RT_SHOTGUN_CHAL_3
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 14
				CASE RRM_SILVER		RETURN 18
				CASE RRM_GOLD		RETURN 23
			ENDSWITCH
		BREAK
		CASE RT_AR_CHAL_1
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 2500
				CASE RRM_SILVER		RETURN 3550
				CASE RRM_GOLD		RETURN 7550
			ENDSWITCH
		BREAK
		CASE RT_AR_CHAL_2
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 4750 
				CASE RRM_SILVER		RETURN 7500
				CASE RRM_GOLD		RETURN 17500
			ENDSWITCH
		BREAK
		CASE RT_AR_CHAL_3
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 1000
				CASE RRM_SILVER		RETURN 3000
				CASE RRM_GOLD		RETURN 5000
			ENDSWITCH
		BREAK
		CASE RT_LMG_CHAL_1
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 4500
				CASE RRM_SILVER		RETURN 5500
				CASE RRM_GOLD		RETURN 7500
			ENDSWITCH
		BREAK
		CASE RT_LMG_CHAL_2
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 5500
				CASE RRM_SILVER		RETURN 12000
				CASE RRM_GOLD		RETURN 40000
			ENDSWITCH
		BREAK
		CASE RT_LMG_CHAL_3
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 7500
				CASE RRM_SILVER		RETURN 19000
				CASE RRM_GOLD		RETURN 32000
			ENDSWITCH
		BREAK
		CASE RT_MINIGUN_CHAL_1
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 10000
				CASE RRM_SILVER		RETURN 25000
				CASE RRM_GOLD		RETURN 50000
			ENDSWITCH
		BREAK
		CASE RT_MINIGUN_CHAL_2
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 15000
				CASE RRM_SILVER		RETURN 30000
				CASE RRM_GOLD		RETURN 45000
			ENDSWITCH
		BREAK
		CASE RT_MINIGUN_CHAL_3
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 20000
				CASE RRM_SILVER		RETURN 40000
				CASE RRM_GOLD		RETURN 70000
			ENDSWITCH
		BREAK
		CASE RT_RAILGUN_CHAL_1
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 20
				CASE RRM_SILVER		RETURN 22
				CASE RRM_GOLD		RETURN 24
			ENDSWITCH
		BREAK
		CASE RT_RAILGUN_CHAL_2
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 16
				CASE RRM_SILVER		RETURN 20
				CASE RRM_GOLD		RETURN 24
			ENDSWITCH
		BREAK
		CASE RT_RAILGUN_CHAL_3
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 20
				CASE RRM_SILVER		RETURN 26
				CASE RRM_GOLD		RETURN 32
			ENDSWITCH
		BREAK
		CASE RT_RAILGUN_CHAL_4
			SWITCH eMedal
				CASE RRM_BRONZE		RETURN 26
				CASE RRM_SILVER		RETURN 31
				CASE RRM_GOLD		RETURN 35
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	CASSERTLN(DEBUG_SHOOTRANGE, "RANGE_GET_MEDAL_REQUIREMENT_OFF_ROUND_TYPE no case created! eRoundType=", GET_STRING_FROM_RANGE_ROUND_TYPE(eRoundType), " and eMedal=", GET_STRING_FROM_RANGE_ROUND_MEDAL(eMedal) )
	
	RETURN -1
	
ENDFUNC


