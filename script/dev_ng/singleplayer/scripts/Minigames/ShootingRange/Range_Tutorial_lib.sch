// Range_Tutorial_lib.sch
USING "Range_Tutorial.sch"
USING "commands_hud.sch"
USING "shared_hud_displays.sch"


/// PURPOSE:
///    Displays the scores in the tutorial. 
PROC TUTORIAL_DRAW_SCORE_ZONE(INT iNumZones)	
	SWITCH (iNumZones)
		CASE 1
			// Zone 1 reward
			PRINT_HELP("SHR_TUT_Z1PTS", DEFAULT_GOD_TEXT_TIME)
		BREAK
		
		CASE 2
			// Zone 2 reward
			PRINT_HELP("SHR_TUT_Z2PTS", DEFAULT_GOD_TEXT_TIME)
		BREAK
		
		CASE 3
			// Draw Zone 3 reward
			PRINT_HELP("SHR_TUT_Z3PTS", DEFAULT_GOD_TEXT_TIME)
		BREAK
		
		CASE 4
			// Draw Zone 4 reward
			PRINT_HELP("SHR_TUT_Z4PTS", DEFAULT_GOD_TEXT_TIME)
		BREAK
	ENDSWITCH
ENDPROC

PROC CREATE_TUTORIAL_SCORING_TARGET(TARGET_ENTITY & sTargetEnt, Range_TutorialData & sTutorialInfo, Range_CoreData & sCoreInfo)
	MODEL_NAMES tempArm
	MODEL_NAMES tempBacking
	MODEL_NAMES tempTarget

	// Regular Targets
	tempBacking 	= PROP_TARGET_BACKBOARD
	tempArm 		= PROP_TARGET_ARM
	tempTarget		= PROP_TARGET_COMP_WOOD
	MODERN_TARGET	newTarget

	SET_BITMASK_AS_ENUM(sTargetEnt.iStatus, TS_HEIGHT_MID)

	// Now then, lets create the target!
	newTarget.vPos = sTutorialInfo.vScoreTargetPos
	
	newTarget.fHeading = 180.0
	newTarget.vCenter = newTarget.vPos
	newTarget.vCenter.z -= CONST_CENTER_Z_OFFSET			// This value seems to be highly ineffectual.
	
	// Create the backing of the target
	newTarget.objBacking = CREATE_OBJECT_NO_OFFSET(tempBacking, newTarget.vPos)

	// Create the target itself.
	newTarget.objTarget = CREATE_OBJECT_NO_OFFSET(tempTarget, newTarget.vPos)
	ATTACH_ENTITY_TO_ENTITY(newTarget.objTarget, newTarget.objBacking,0,<<0.0,-0.04,-0.410>>,<<0.0,0.0,0.0>>)

	// Create the Arm
	newTarget.objArm = CREATE_OBJECT_NO_OFFSET(tempArm, newTarget.vPos)

	// Attach the backing to the arm. This affects only creation, not final resting point!
	ATTACH_ENTITY_TO_ENTITY(newTarget.objBacking, newTarget.objArm,0,<<0.0,0.007,-0.760>>,<<0.0,0.0,180.0>>)

	// Rotate the arm
	IF (sCoreInfo.eRangeLoc = RANGELOC_PILLBOX_HILL)
		SET_ENTITY_ROTATION(newTarget.objArm,<<-90.0,0.0,160.0>>)
	ELSE
		SET_ENTITY_ROTATION(newTarget.objArm,<<-90.0,0.0,179.0>>)
	ENDIF
	sTargetEnt.iStatus		= 0
	SET_BITMASK_AS_ENUM(sTargetEnt.iStatus, TS_HEIGHT_MID)

	// We're not flipped
	sTargetEnt.fHp	  = CONST_TARGET_HP
	sTargetEnt.target = newTarget
	sTargetEnt.eGridIndex = gridA1
	sTargetEnt.fTargCreateTime = (GET_GAME_TIMER() / 1000.0)
	ADD_TARGET_STATUS(sTargetEnt, TS_ENTERING)
	sTargetEnt.fRotateCoeff = 0.5
	
	// PLAY SOUND: Creation -- Not sure if this is a real thing.
	PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetEnt.target.objArm)
ENDPROC


/// PURPOSE:
///    Updates and explains the combo MP meter in the shooting range.
PROC UPDATE_TUTORIAL_COMBO_METER(Range_RoundInfo & sRndInfo)	
	// Draw the Multiplier
	// Draw the HUD. Most is already store, the rest was calculated above:	
	Range_HUD_Data sDrawData
	sDrawData.iMultiplier = sRndInfo.sMPData.iCurrMP
	sDrawData.iCurMultiplierBlocks = sRndInfo.sMPData.iMPBlocks
	sDrawData.iMaxMultiplierBlocks = iMULTIPLIER_BLOCKS
	
	RUN_SHOOTING_RANGE_MULTIPLYER(sDrawData)
ENDPROC


/// PURPOSE:
///    Fires a single bullet from a ped to a coord, using the ped's weapon.
///    Intended for use in the tutorial.
PROC RANGE_FIRE_TUTORIAL_BULLET_AT_COORD(PED_INDEX pedTutorial, VECTOR vCoords, FLOAT fRadius = 0.2)
	WEAPON_TYPE eWeapon
	IF DOES_ENTITY_EXIST(pedTutorial) AND NOT IS_ENTITY_DEAD(pedTutorial)
		IF GET_CURRENT_PED_WEAPON(pedTutorial, eWeapon)
			ENTITY_INDEX entWeapon = GET_CURRENT_PED_WEAPON_ENTITY_INDEX(pedTutorial)
			VECTOR vFinalCoords = << vCoords.x + GET_RANDOM_FLOAT_IN_RANGE(-fRadius, fRadius), vCoords.y + GET_RANDOM_FLOAT_IN_RANGE(-fRadius, fRadius), vCoords.z + GET_RANDOM_FLOAT_IN_RANGE(-fRadius, fRadius) >>
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_ENTITY_COORDS(entWeapon), vFinalCoords, 1, TRUE, eWeapon, pedTutorial, TRUE)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Destroys all shooting range tutorial peds and props.
PROC RANGE_TUTORIAL_CLEAR_PEDS_AND_PROPS(Range_TutorialData & sTutorialInfo)
	IF DOES_ENTITY_EXIST(sTutorialInfo.pedGuy1)
		DELETE_PED(sTutorialInfo.pedGuy1)
	ENDIF
	IF DOES_ENTITY_EXIST(sTutorialInfo.pedGuy2)
		DELETE_PED(sTutorialInfo.pedGuy2)
	ENDIF
	IF DOES_ENTITY_EXIST(sTutorialInfo.pedGuy3)
		DELETE_PED(sTutorialInfo.pedGuy3)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sTutorialInfo.objEarmuff1)
		SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.objEarmuff1, DEFAULT, TRUE)
		DELETE_OBJECT(sTutorialInfo.objEarmuff1)
	ENDIF
	IF DOES_ENTITY_EXIST(sTutorialInfo.objEarmuff2)
		SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.objEarmuff2, DEFAULT, TRUE)
		DELETE_OBJECT(sTutorialInfo.objEarmuff2)
	ENDIF
	IF DOES_ENTITY_EXIST(sTutorialInfo.objEarmuff3)
		SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.objEarmuff3, DEFAULT, TRUE)
		DELETE_OBJECT(sTutorialInfo.objEarmuff3)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sTutorialInfo.objGlasses1)
		SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.objGlasses1, DEFAULT, TRUE)
		DELETE_OBJECT(sTutorialInfo.objGlasses1)
	ENDIF
	IF DOES_ENTITY_EXIST(sTutorialInfo.objGlasses2)
		SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.objGlasses2, DEFAULT, TRUE)
		DELETE_OBJECT(sTutorialInfo.objGlasses2)
	ENDIF
	IF DOES_ENTITY_EXIST(sTutorialInfo.objGlasses3)
		SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.objGlasses3, DEFAULT, TRUE)
		DELETE_OBJECT(sTutorialInfo.objGlasses3)
	ENDIF
	CLEAR_AREA(sTutorialInfo.vSceneRootPos, 35.0, TRUE)
ENDPROC


PROC UPDATE_TUTORIAL_CONTENT(FLOAT fScenePercent, Range_TutorialData & sTutorialInfo, Range_CoreData & sCoreInfo, TARGET_ENTITY & sTargetEnts[], Range_RoundInfo & sRndInfo, GRID_LOCATION & sGridInfo[],
			Range_SPTData & sSPTInfo, Range_RangeData &sRangeInfo)
	UNUSED_PARAMETER(sCoreInfo)		
	
	UPDATE_TARGET_ENTITY(sTargetEnts[0], sGridInfo, sRndInfo, sSPTInfo, sRangeInfo, TRUE)
	UPDATE_TARGET_ENTITY(sTargetEnts[1], sGridInfo, sRndInfo, sSPTInfo, sRangeInfo, TRUE)
	UPDATE_TARGET_ENTITY(sTargetEnts[2], sGridInfo, sRndInfo, sSPTInfo, sRangeInfo, TRUE)
	UPDATE_TARGET_ENTITY(sTargetEnts[3], sGridInfo, sRndInfo, sSPTInfo, sRangeInfo, TRUE)
	UPDATE_TARGET_ENTITY(sTargetEnts[4], sGridInfo, sRndInfo, sSPTInfo, sRangeInfo, TRUE)


	SWITCH sTutorialInfo.iTutorialState
		CASE 0
			// At start: "Welcome to the Ammu-Nation shooting range. This tutorial can be skipped..."
			CLEAR_HELP()
			PRINT_HELP_FOREVER("SC_TUT_1")
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Showing SC_TUT_1")

			sTutorialInfo.iTutorialState++
		BREAK
		
		CASE 1
			// (0.1121): "The shooting range has various challenges:..."
			IF (fScenePercent >= 0.1121)
				//CLEAR_HELP()
				PRINT_HELP_FOREVER("SC_TUT_2")

				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		CASE 2
			// (0.19431): << Drop targets >>
			IF (fScenePercent >= 0.19431)
				SETUP_TARGET_ENTITY(sTargetEnts[0], gridC2)
				CREATE_TARGET(sTargetEnts[0], sRndInfo, sRangeInfo, sTargetEnts[0].eGridIndex)
				ADD_TARGET_STATUS(sTargetEnts[0], TS_CREATED)
				
				SETUP_TARGET_ENTITY(sTargetEnts[1], gridB3)
				CREATE_TARGET(sTargetEnts[1], sRndInfo, sRangeInfo, sTargetEnts[1].eGridIndex)
				ADD_TARGET_STATUS(sTargetEnts[1], TS_CREATED)

				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		CASE 3
			// (0.215): << Recoil first target >>
			IF (fScenePercent >= 0.215)
				RANGE_FIRE_TUTORIAL_BULLET_AT_COORD(sTutorialInfo.pedGuy3, sTargetEnts[0].target.vCenter)
				
				sTargetEnts[0].iStatus |= ENUM_TO_INT(TS_EXITING)
				PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetEnts[0].target.objArm)

				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		CASE 4
			// (0.2275): << Recoil second target >>
			IF (fScenePercent >= 0.2275)
				RANGE_FIRE_TUTORIAL_BULLET_AT_COORD(sTutorialInfo.pedGuy3, sTargetEnts[1].target.vCenter)
				
				sTargetEnts[1].iStatus |= ENUM_TO_INT(TS_EXITING)
				PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetEnts[1].target.objArm)

				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		CASE 5
			IF (fScenePercent >= 0.26756)
				//CLEAR_HELP()
				PRINT_HELP_FOREVER("SC_TUT_3")

				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		CASE 6
			IF (fScenePercent >= 0.39760)
				//CLEAR_HELP()
				PRINT_HELP_FOREVER("SC_TUT_4")
				
				DESTROY_TARGET_ENTITY(sTargetEnts[0], sGridInfo)
				
				// Create a target to demo this.
				CREATE_TUTORIAL_SCORING_TARGET(sTargetEnts[0], sTutorialInfo, sCoreInfo)
				ADD_TARGET_STATUS(sTargetEnts[0], TS_CREATED)

				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		// ZONE PRINTS
		CASE 7
			IF (fScenePercent >= 0.50)
				sTutorialInfo.iTutorialState++
				
				// DISPLAY ZONE 4
				TUTORIAL_DRAW_SCORE_ZONE(4)
			ENDIF
		BREAK
		
		CASE 8
			IF (fScenePercent >= 0.542)
				sTutorialInfo.iTutorialState++
				
				// DISPLAY ZONE 3
				TUTORIAL_DRAW_SCORE_ZONE(3)
			ENDIF
		BREAK
		
		CASE 9
			IF (fScenePercent >= 0.584)
				sTutorialInfo.iTutorialState++
				
				// DISPLAY ZONE 2
				TUTORIAL_DRAW_SCORE_ZONE(2)
			ENDIF
		BREAK
		
		CASE 10
			
			IF (fScenePercent >= 0.6258)
				sTutorialInfo.iTutorialState++
				
				// DISPLAY ZONE 1
				TUTORIAL_DRAW_SCORE_ZONE(1)
			ENDIF
		BREAK
		
		CASE 11
			IF (fScenePercent >= 0.66741)
				CLEAR_PRINTS()			// Also clear the zone 1 text.
				//CLEAR_HELP()
				
				DESTROY_TARGET_ENTITY(sTargetEnts[0], sGridInfo)
				
				// Drop in 5 targets...
				sRndInfo.iMaxTargets = 10
				sRndInfo.fCurrentRoundLength = 99	
				sRndInfo.fCurrentBulletDamage = CONST_TARGET_HP
				
				SETUP_TARGET_ENTITY(sTargetEnts[0], gridB2)
				CREATE_TARGET(sTargetEnts[0], sRndInfo, sRangeInfo, sTargetEnts[0].eGridIndex)
				ADD_TARGET_STATUS(sTargetEnts[0], TS_CREATED)
				
				SETUP_TARGET_ENTITY(sTargetEnts[1], gridB3)
				CREATE_TARGET(sTargetEnts[1], sRndInfo, sRangeInfo, sTargetEnts[1].eGridIndex)
				ADD_TARGET_STATUS(sTargetEnts[1], TS_CREATED)
				
				SETUP_TARGET_ENTITY(sTargetEnts[2], gridB4)
				CREATE_TARGET(sTargetEnts[2], sRndInfo, sRangeInfo, sTargetEnts[2].eGridIndex)
				ADD_TARGET_STATUS(sTargetEnts[2], TS_CREATED)
				
				SETUP_TARGET_ENTITY(sTargetEnts[3], gridB5)
				CREATE_TARGET(sTargetEnts[3], sRndInfo, sRangeInfo, sTargetEnts[3].eGridIndex)
				ADD_TARGET_STATUS(sTargetEnts[3], TS_CREATED)
				
				PRINT_HELP_FOREVER("SC_TUT_5")
				sRndInfo.sMPData.iMPBlocks = 2
				
				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		CASE 12
			// Update the combo meter.
			UPDATE_TUTORIAL_COMBO_METER(sRndInfo)

			// (0.69731): << Recoil target 1 >>
			IF (fScenePercent >= 0.69731)
				RANGE_FIRE_TUTORIAL_BULLET_AT_COORD(sTutorialInfo.pedGuy3, sTargetEnts[0].target.vCenter)
				sTargetEnts[0].iStatus |= ENUM_TO_INT(TS_EXITING)
				PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetEnts[0].target.objArm)
				INCREMENT_MULTIPLIER(sRndInfo.sMPData)

				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		CASE 13
			// Update the combo meter.
			UPDATE_TUTORIAL_COMBO_METER(sRndInfo)

			// (0.70702): << Recoil target 2 >>
			IF (fScenePercent >= 0.70702)
				RANGE_FIRE_TUTORIAL_BULLET_AT_COORD(sTutorialInfo.pedGuy3, sTargetEnts[1].target.vCenter)
				sTargetEnts[1].iStatus |= ENUM_TO_INT(TS_EXITING)
				PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetEnts[1].target.objArm)
				INCREMENT_MULTIPLIER(sRndInfo.sMPData)
				
				// Need to swap the tutorial meter to 2x, 
				
				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		CASE 14
			// Update the combo meter.
			UPDATE_TUTORIAL_COMBO_METER(sRndInfo)

			// (0.7272): << Recoil target 4 >>
			IF (fScenePercent >= 0.72)
				RANGE_FIRE_TUTORIAL_BULLET_AT_COORD(sTutorialInfo.pedGuy3, sTargetEnts[3].target.vCenter)
				sTargetEnts[3].iStatus |= ENUM_TO_INT(TS_EXITING)
				PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetEnts[3].target.objArm)
				INCREMENT_MULTIPLIER(sRndInfo.sMPData)

				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		CASE 15
			// Update the combo meter.
			UPDATE_TUTORIAL_COMBO_METER(sRndInfo)

			// (0.7414): << Recoil target 3 >>
			IF (fScenePercent >= 0.7414)
				RANGE_FIRE_TUTORIAL_BULLET_AT_COORD(sTutorialInfo.pedGuy3, sTargetEnts[2].target.vCenter)
				sTargetEnts[2].iStatus |= ENUM_TO_INT(TS_EXITING)
				PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sTargetEnts[2].target.objArm)
				INCREMENT_MULTIPLIER(sRndInfo.sMPData)

				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK

		CASE 16
			// Update the combo meter.
			UPDATE_TUTORIAL_COMBO_METER(sRndInfo)
			
			IF (fScenePercent >= 0.83183)
				//CLEAR_HELP()
				PRINT_HELP_FOREVER("SC_TUT_6")
				
				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		CASE 17
			// Done
			IF (fScenePercent >= 0.96)
				CLEAR_HELP()
				CLEAR_PRINTS()
				CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_2)

				SET_PED_FIRING_PATTERN(rangePlayerPed, FIRING_PATTERN_FULL_AUTO)
				
				// Clear the flag that says we hit a target, must be cleared before the rounds start otherwise it screws our accuracy
				CLEAR_ROUND_FLAG(sRndInfo, ROUND_SHOT_FIRED)
				
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Cutscene done. Destroying cam! Leaving!")
				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC



FUNC BOOL UPDATE_TUTORIAL(Range_TutorialData & sTutorialInfo, Range_CoreData & sCoreInfo, TARGET_ENTITY & sTargetEnts[], Range_RoundInfo & sRndInfo, GRID_LOCATION & sGridInfo[],
			Range_SPTData & sSPTInfo, Range_RangeData & sRangeInfo)
	// Don't draw any HUD: reticule, RADAR, etc.
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SET_WIDESCREEN_BORDERS(TRUE, 100)
	
	// Having an issue keeping the room streamed in.
	SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("V_7_ShootRm")
				
	// Check tutorial skip
	IF (sTutorialInfo.iTutorialState > 0 AND sTutorialInfo.iTutorialState != 99)
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_RELEASED()
			sTutorialInfo.iTutorialState = 99
			CLEAR_HELP()
			CLEAR_PRINTS()
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			RANGE_TUTORIAL_CLEAR_PEDS_AND_PROPS(sTutorialInfo)
			STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(sRangeInfo.vDoorPos, 3.0, V_ILEV_GC_DOOR01, NORMAL_BLEND_OUT)
			
			TELEPORT_PLAYER(sRangeInfo.vPlayerRootPos, sRangeInfo.fPlayerRootHead)
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(sTutorialInfo.sceneCamera)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "UpdateTutorial -- Skipping the cutscene")
			
			RETURN TRUE
		ENDIF
	ENDIF

	SWITCH (sTutorialInfo.eSceneState)
		CASE eRangeSyncScene_Init
			CDEBUG2LN(DEBUG_SHOOTRANGE, "TUTORIAL STATE eRangeSyncScene_Init")
			
			sTutorialInfo.iSceneID = CREATE_SYNCHRONIZED_SCENE(sTutorialInfo.vSceneRootPos, sTutorialInfo.vSceneRootOrient)
			
			sTutorialInfo.sceneCamera = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
			PLAY_SYNCHRONIZED_CAM_ANIM(sTutorialInfo.sceneCamera, sTutorialInfo.iSceneID, "shoot_range_tutorial_cam", "mini@shoot_range")
			SET_CAM_ACTIVE(sTutorialInfo.sceneCamera, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			TASK_SYNCHRONIZED_SCENE(rangePlayerPed, sTutorialInfo.iSceneID, "mini@shoot_range", "shoot_range_tutorial_plyr", INSTANT_BLEND_IN, WALK_BLEND_OUT)
			SET_FORCE_FOOTSTEP_UPDATE(rangePlayerPed, TRUE)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sTutorialInfo.iSceneID, FALSE)
			
			// Get the door. 
			OBJECT_INDEX objDoor
			objDoor = GET_CLOSEST_OBJECT_OF_TYPE(sRangeInfo.vDoorPos, 3.0, V_ILEV_GC_DOOR01, TRUE)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(objDoor, sTutorialInfo.iSceneID, "shoot_range_tutorial_door", "mini@shoot_range", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)

			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objDoor)
			
			// Task all peds.
			IF DOES_ENTITY_EXIST(sTutorialInfo.pedGuy1)	AND NOT IS_ENTITY_DEAD(sTutorialInfo.pedGuy1)
				SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.pedGuy1, TRUE, TRUE)
				GIVE_WEAPON_TO_PED(sTutorialInfo.pedGuy1, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
				TASK_SYNCHRONIZED_SCENE(sTutorialInfo.pedGuy1, sTutorialInfo.iSceneID, "mini@shoot_range", "shoot_range_tutorial_npc1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				SET_FORCE_FOOTSTEP_UPDATE(sTutorialInfo.pedGuy1, TRUE)
			ENDIF
			IF DOES_ENTITY_EXIST(sTutorialInfo.pedGuy2)	AND NOT IS_ENTITY_DEAD(sTutorialInfo.pedGuy2)
				SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.pedGuy2, TRUE, TRUE)
				GIVE_WEAPON_TO_PED(sTutorialInfo.pedGuy2, WEAPONTYPE_COMBATPISTOL, INFINITE_AMMO, TRUE)
				TASK_SYNCHRONIZED_SCENE(sTutorialInfo.pedGuy2, sTutorialInfo.iSceneID, "mini@shoot_range", "shoot_range_tutorial_npc2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				SET_FORCE_FOOTSTEP_UPDATE(sTutorialInfo.pedGuy2, TRUE)
			ENDIF
			IF DOES_ENTITY_EXIST(sTutorialInfo.pedGuy3)	AND NOT IS_ENTITY_DEAD(sTutorialInfo.pedGuy3)
				SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.pedGuy3, TRUE, TRUE)
				GIVE_WEAPON_TO_PED(sTutorialInfo.pedGuy3, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
				TASK_SYNCHRONIZED_SCENE(sTutorialInfo.pedGuy3, sTutorialInfo.iSceneID, "mini@shoot_range", "shoot_range_tutorial_npc3", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				SET_FORCE_FOOTSTEP_UPDATE(sTutorialInfo.pedGuy3, TRUE)
			ENDIF
			
			IF (sCoreInfo.eRangeLoc = RANGELOC_PILLBOX_HILL)
				SET_STATIC_EMITTER_ENABLED("LOS_SANTOS_AMMUNATION_GUN_RANGE", TRUE)
			ELSE
				SET_STATIC_EMITTER_ENABLED("SE_AMMUNATION_CYPRESS_FLATS_GUN_RANGE", TRUE)
			ENDIF

			// Add the earmuffs to the player...
			RANGE_SET_PLAYER_PED_COMPONENTS(sCoreInfo)

			sTutorialInfo.eSceneState = eRangeSyncScene_Update
		BREAK
		
		CASE eRangeSyncScene_Update
			//CDEBUG2LN(DEBUG_SHOOTRANGE, "TUTORIAL STATE eRangeSyncScene_Update")
			
			// Update the tutorial display.
			UPDATE_TUTORIAL_CONTENT(GET_SYNCHRONIZED_SCENE_PHASE(sTutorialInfo.iSceneID), sTutorialInfo, sCoreInfo, sTargetEnts, sRndInfo, sGridInfo, sSPTInfo, sRangeInfo)
			
			TEXT_LABEL_63 texTime
			texTime = "sceneTime = "
			texTime += ROUND( GET_SYNCHRONIZED_SCENE_PHASE(sTutorialInfo.iSceneID) * 100 )
			DRAW_DEBUG_TEXT_2D( texTime, (<< 0.5, 0.8, 0.0 >>) )
			
			DRAW_DEBUG_TEXT( "pedGuy1", GET_ENTITY_COORDS( sTutorialInfo.pedGuy1, FALSE ) )
			DRAW_DEBUG_TEXT( "pedGuy2", GET_ENTITY_COORDS( sTutorialInfo.pedGuy2, FALSE ) )
			DRAW_DEBUG_TEXT( "pedGuy3", GET_ENTITY_COORDS( sTutorialInfo.pedGuy3, FALSE ) )
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sTutorialInfo.iSceneID)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sTutorialInfo.iSceneID) > 0.98
					sTutorialInfo.eSceneState = eRangeSyncScene_Cleanup
				ELIF GET_SYNCHRONIZED_SCENE_PHASE(sTutorialInfo.iSceneID) > 0.88
					IF NOT IS_PED_INJURED( sTutorialInfo.pedGuy3 )
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET( sTutorialInfo.eBitflags, eRangeTutorial_weaponTaken )
							SET_BITMASK_ENUM_AS_ENUM( sTutorialInfo.eBitflags, eRangeTutorial_weaponTaken )
							GIVE_WEAPON_TO_PED(sTutorialInfo.pedGuy3, WEAPONTYPE_UNARMED, INFINITE_AMMO, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				sTutorialInfo.eSceneState = eRangeSyncScene_Cleanup
			ENDIF
		BREAK
		
		CASE eRangeSyncScene_Cleanup
			CDEBUG2LN(DEBUG_SHOOTRANGE, "TUTORIAL STATE eRangeSyncScene_Cleanup")
			
			RANGE_TUTORIAL_CLEAR_PEDS_AND_PROPS(sTutorialInfo)
			STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(sRangeInfo.vDoorPos, 3.0, V_ILEV_GC_DOOR01, NORMAL_BLEND_OUT)
			
			IF (sCoreInfo.eRangeLoc = RANGELOC_PILLBOX_HILL)
				SET_STATIC_EMITTER_ENABLED("LOS_SANTOS_AMMUNATION_GUN_RANGE", FALSE)
			ELSE
				SET_STATIC_EMITTER_ENABLED("SE_AMMUNATION_CYPRESS_FLATS_GUN_RANGE", FALSE)
			ENDIF

			RETURN TRUE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Handles the slow camera pan for the range intro.
/// RETURNS:
///    TRUE when done.
FUNC BOOL RANGE_RUN_NON_TUTORIAL_INTRO(Range_TutorialData & sTutorialInfo, Range_RoundInfo & sRndInfo, Range_CoreData & sCoreInfo, Range_RangeData & sRangeInfo)
	// Update the intro for the range, but there's no tutorial.
	TEXT_LABEL_63 texDisplay = "nonTutState = "
	texDisplay += sTutorialInfo.iTutorialState
	DRAW_DEBUG_TEXT_2D(texDisplay, (<< 0.9, 0.3, 0.0 >>))
	SWITCH (sTutorialInfo.iTutorialState)
		CASE 0
			RANGE_SET_PLAYER_PED_COMPONENTS(sCoreInfo)
			TELEPORT_PLAYER(sRangeInfo.vPlayerRootPos, sRangeInfo.fPlayerRootHead)
			
			sTutorialInfo.sceneCamera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
			SET_CAM_COORD(sTutorialInfo.sceneCamera, sTutorialInfo.vNonTutorialCamPos)
			SET_CAM_ROT(sTutorialInfo.sceneCamera, sTutorialInfo.vNonTutorialCamOrient)
			SET_CAM_FOV(sTutorialInfo.sceneCamera, 50.0)
			SET_CAM_ACTIVE(sTutorialInfo.sceneCamera, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SET_CAM_PARAMS(sTutorialInfo.sceneCamera, sTutorialInfo.vNonTutorialCamEndPos, sTutorialInfo.vNonTutorialCamEndOrient, 50.0, 4000)
			
			// Trigger player to run the end of the sync scene anim.
			sTutorialInfo.iSceneID = CREATE_SYNCHRONIZED_SCENE(sTutorialInfo.vSceneRootPos, sTutorialInfo.vSceneRootOrient)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sTutorialInfo.iSceneID, FALSE)
			TASK_SYNCHRONIZED_SCENE(rangePlayerPed, sTutorialInfo.iSceneID, "mini@shoot_range", "shoot_range_tutorial_plyr", INSTANT_BLEND_IN, WALK_BLEND_OUT)
			SET_FORCE_FOOTSTEP_UPDATE(rangePlayerPed, TRUE)
			SET_SYNCHRONIZED_SCENE_PHASE(sTutorialInfo.iSceneID, 0.92)
			
			
			// If we grabbed peds, need to kill them now. There's no intro, so we don't need em.
			IF DOES_ENTITY_EXIST(sTutorialInfo.pedGuy1)
			AND NOT IS_ENTITY_DEAD(sTutorialInfo.pedGuy1)
				SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.pedGuy1, TRUE, TRUE)
				CLEAR_PED_PROP(sTutorialInfo.pedGuy1, ANCHOR_HEAD)
				DELETE_PED(sTutorialInfo.pedGuy1)
			ENDIF
			IF DOES_ENTITY_EXIST(sTutorialInfo.pedGuy2)
			AND NOT IS_ENTITY_DEAD(sTutorialInfo.pedGuy2)
				SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.pedGuy2, TRUE, TRUE)
				CLEAR_PED_PROP(sTutorialInfo.pedGuy2, ANCHOR_HEAD)
				DELETE_PED(sTutorialInfo.pedGuy2)
			ENDIF
			IF DOES_ENTITY_EXIST(sTutorialInfo.pedGuy3)
			AND NOT IS_ENTITY_DEAD(sTutorialInfo.pedGuy3)
				SET_ENTITY_AS_MISSION_ENTITY(sTutorialInfo.pedGuy3, TRUE, TRUE)
				CLEAR_PED_PROP(sTutorialInfo.pedGuy3, ANCHOR_HEAD)
				DELETE_PED(sTutorialInfo.pedGuy3)
			ENDIF
			
			CLEAR_AREA(sRangeInfo.vPlayerRootPos, 10.0, TRUE)
			
			RESTART_TIMER_AT(sRndInfo.sGenTimer, 0.0)
			sTutorialInfo.iTutorialState++
		BREAK
		
		CASE 1
			IF GET_SYNCHRONIZED_SCENE_PHASE(sTutorialInfo.iSceneID) > 0.99	
				sTutorialInfo.iTutorialState++
			ENDIF
		BREAK
		
		CASE 2
			IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME)
				sTutorialInfo.iTutorialTimer = GET_GAME_TIMER() + DEFAULT_INTERP_TO_FROM_GAME
				sTutorialInfo.iTutorialState++
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 3
			IF GET_GAME_TIMER() > sTutorialInfo.iTutorialTimer
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Stores off all the tutorial peds and props we'll be needing.
PROC RANGE_STORE_TUTORIAL_INFO(SHOOTING_RANGE_LAUNCH_DATA & launchData, Range_TutorialData & sTutorialInfo)
	sTutorialInfo.pedGuy1 = launchData.pedParticipant1
	sTutorialInfo.pedGuy2 = launchData.pedParticipant2
	sTutorialInfo.pedGuy3 = launchData.pedParticipant3
	
	sTutorialInfo.objEarmuff1 = launchData.objEarmuffs1
	sTutorialInfo.objEarmuff2 = launchData.objEarmuffs2
	sTutorialInfo.objEarmuff3 = launchData.objEarmuffs3
	
	sTutorialInfo.objGlasses1 = launchData.objGlasses1
	sTutorialInfo.objGlasses2 = launchData.objGlasses2
	sTutorialInfo.objGlasses3 = launchData.objGlasses3
ENDPROC


