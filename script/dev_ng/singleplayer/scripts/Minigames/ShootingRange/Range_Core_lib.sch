// Range_Core_lib.sch
USING "Range_Core.sch"
USING "Range_Round.sch"
USING "Range_UI.sch"

/// PURPOSE:
///    Handles giving the player the weapon type, and enough ammo for the round.
/// PARAMS:
///    weapType - 
PROC SETUP_PLAYER_WEAPONS(WEAPON_TYPE weapType, Range_RoundInfo & sRndInfo)
	IF (weapType <> WEAPONTYPE_UNARMED) AND (weapType <> WEAPONTYPE_INVALID)		
		// Store whether or not we own the weapon, as well as the weapon type.
		IF HAS_PED_GOT_WEAPON(rangePlayerPed, weapType)
			SET_ROUND_FLAG(sRndInfo, ROUND_PLAYER_OWNS_WEAPON)
			
			// Store how many bullets we had in the weapon when we started. We'll give this back to the palyer at the end.
			GET_AMMO_IN_CLIP(rangePlayerPed, weapType, sRndInfo.iStoredAmmoCount)
			sRndInfo.iStoredAmmoCount += GET_AMMO_IN_PED_WEAPON(rangePlayerPed, weapType)
			
			CDEBUG2LN(DEBUG_SHOOTRANGE, "HandleWeapons -- Player's Ammo Before: ", sRndInfo.iStoredAmmoCount, " WeapType: ", weapType)
		ELSE
			CLEAR_ROUND_FLAG(sRndInfo, ROUND_PLAYER_OWNS_WEAPON)
			
			// Don't have the weapon, so give it to him.	
			GIVE_WEAPON_TO_PED(rangePlayerPed, weapType, 0, FALSE)
			
			CDEBUG2LN(DEBUG_SHOOTRANGE, "HandleWeapons -- Player did not have weapon before now. WeapType: ", weapType)
		ENDIF
		
		// If the palyer has the flashlight attachment, remove it...
		IF HAS_PED_GOT_WEAPON_COMPONENT(rangePlayerPed, weapType, WEAPONCOMPONENT_AT_PI_FLSH)
			SET_ROUND_FLAG(sRndInfo, ROUND_PLAYER_OWNS_HG_FLASHLIGHT)
			REMOVE_WEAPON_COMPONENT_FROM_PED(rangePlayerPed, weapType, WEAPONCOMPONENT_AT_PI_FLSH)
			
		ELIF HAS_PED_GOT_WEAPON_COMPONENT(rangePlayerPed, weapType, WEAPONCOMPONENT_AT_AR_FLSH)
			SET_ROUND_FLAG(sRndInfo, ROUND_PLAYER_OWNS_AR_FLASHLIGHT)
			REMOVE_WEAPON_COMPONENT_FROM_PED(rangePlayerPed, weapType, WEAPONCOMPONENT_AT_AR_FLSH)
		ENDIF

		// No matter what, give them the constant ammo.
		IF (weapType = WEAPONTYPE_MINIGUN)
			SET_PED_AMMO(rangePlayerPed, weapType, 15000)
			SET_AMMO_IN_CLIP(rangePlayerPed, weapType, 15000)
		ELSE
			SET_PED_AMMO(rangePlayerPed, weapType, 600)
			SET_AMMO_IN_CLIP(rangePlayerPed, weapType, GET_MAX_AMMO_IN_CLIP(rangePlayerPed, weapType))
		ENDIF
		
		REFILL_AMMO_INSTANTLY(PLAYER_PED_ID())
		
		// How much do we have now?
		CDEBUG2LN(DEBUG_SHOOTRANGE, "HandleWeapons -- Player's Ammo After: ", GET_AMMO_IN_PED_WEAPON(rangePlayerPed, weapType))
		
		// Put this weapon in the players hand
		SET_CURRENT_PED_WEAPON(rangePlayerPed, weapType, FALSE)
	ENDIF
ENDPROC


/// PURPOSE:
///    Request assets that are needed as soon as the range is engaged.
PROC RANGE_REQUEST_FIRST_STAGE_ASSETS()
	// Needed for sync scene.
	REQUEST_ANIM_DICT("mini@shoot_range")
	REQUEST_MODEL(PROP_EAR_DEFENDERS_01)
	REQUEST_MODEL(PROP_SAFETY_GLASSES)
	REQUEST_MODEL(PROP_TARGET_BACKBOARD)
	REQUEST_MODEL(PROP_TARGET_ARM)
	REQUEST_MODEL(PROP_TARGET_COMP_WOOD)
	REQUEST_SCRIPT_AUDIO_BANK ("HUD_AWARDS")	
ENDPROC

/// PURPOSE:
///    Returns whether or not assets that are needed immediately on range start have loaded.
FUNC BOOL RANGE_HAVE_FIRST_STAGE_ASSETS_LOADED()
	// check the sync scene...
	IF NOT HAS_ANIM_DICT_LOADED("mini@shoot_range")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(PROP_EAR_DEFENDERS_01)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(PROP_SAFETY_GLASSES)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(PROP_TARGET_BACKBOARD)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(PROP_TARGET_ARM)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(PROP_TARGET_COMP_WOOD)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Better way of streaming all of our assets. Better than what? Better than not streaming them, of course.
PROC RANGE_STREAM_ALL_ASSETS(INT & iAudioStreamCount, Range_MenuData & sMenuInfo)	
	// Remaining target items.
	REQUEST_MODEL(PROP_TARGET_ARM_B)
	REQUEST_MODEL(PROP_TARGET_ARM_LONG)
	REQUEST_MODEL(PROP_TARGET_ARM_SM)
	REQUEST_MODEL(PROP_TARGET_BACKBOARD_B)
	REQUEST_MODEL(PROP_TARGET_COMP_METAL)
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("PROP_TARGET_FRAG_BOARD")))
	// Request earmuff anim
	REQUEST_ANIM_DICT("mini@ears_defenders")
	
	// Request the bank, set our counter to 0. this counter is there in case audio takes forever to load.
	REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\TARGET_PRACTICE")
	REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\HUD_321_GO")
	
	sMenuInfo.uiLeaderboard = REQUEST_SC_LEADERBOARD_UI()
	REQUEST_MINIGAME_COUNTDOWN_UI(sMenuInfo.uiCountdown)
	sMenuInfo.uiSplashText.siMovie = REQUEST_MG_BIG_MESSAGE()
//	REQUEST_RANGE_MEDAL_TOAST()
	
	// New Menu 
	//REQUEST_STREAMED_TEXTURE_DICT("MPHUD")		-- Appears that this isn't being used.
	REQUEST_STREAMED_TEXTURE_DICT("SRange_Gen")
	REQUEST_STREAMED_TEXTURE_DICT("SRange_Chal")
	REQUEST_STREAMED_TEXTURE_DICT("SRange_Chal2")
	REQUEST_STREAMED_TEXTURE_DICT("SRange_Weap")
	REQUEST_STREAMED_TEXTURE_DICT("SRange_Weap2")
	REQUEST_STREAMED_TEXTURE_DICT("MPMedals_FEED")
	
	// TODO: REMOVE THIS TEMP TEXTURE DICTIONARY -TAYLOR
	// I don't see why we need to. A shared text dict is a good idea. -Ryan
	REQUEST_STREAMED_TEXTURE_DICT("Shared")
	REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")
	
	iAudioStreamCount = 0
ENDPROC


/// PURPOSE:
///    Checks to see if all requested assets are streamed.
/// RETURNS:
///    TRUE when we're done streaming.
FUNC BOOL RANGE_HAVE_ALL_ASSETS_STREAMED(INT & iAudioStreamCount, Range_MenuData & sMenuInfo)	
	IF NOT HAS_MODEL_LOADED(PROP_TARGET_ARM)
	OR NOT HAS_MODEL_LOADED(PROP_TARGET_ARM_B)
	OR NOT HAS_MODEL_LOADED(PROP_TARGET_ARM_LONG)
	OR NOT HAS_MODEL_LOADED(PROP_TARGET_ARM_SM)
	OR NOT HAS_MODEL_LOADED(PROP_TARGET_BACKBOARD)
	OR NOT HAS_MODEL_LOADED(PROP_TARGET_BACKBOARD_B)
	OR NOT HAS_MODEL_LOADED(PROP_TARGET_COMP_WOOD)
	OR NOT HAS_MODEL_LOADED(PROP_TARGET_COMP_METAL)
	OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("PROP_TARGET_FRAG_BOARD")))
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Waiting on shooting range model")
	ENDIF

	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sMenuInfo.uiSplashText.siMovie)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "The Splash Text UI scaleform hasn't loaded.")
		RETURN FALSE
	ENDIF
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sMenuInfo.uiLeaderboard)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Leaderboard UI has not yet loaded.")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MINIGAME_COUNTDOWN_UI_LOADED(sMenuInfo.uiCountdown)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "The countdown UI scaleform hasn't loaded.")
		RETURN FALSE
	ENDIF

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("SRange_Gen")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: SRange_Gen")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("SRange_Chal")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: SRange_Chal")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("SRange_Chal2")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: SRange_Chal2")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("SRange_Weap")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: SRange_Weap")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("SRange_Weap2")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: SRange_Weap2")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPMedals_FEED")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: MPMedals_FEED")
		RETURN FALSE
	ENDIF

	// TODO: REMOVE THIS TEMP TEXTURE DICTIONARY -TAYLOR
	// I don't see why we need to. A shared text dict is a good idea. -Ryan
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("Shared")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: Shared")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: CommonMenu")
		RETURN FALSE
	ENDIF

	// Check the sound bank:
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\TARGET_PRACTICE") OR NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\HUD_321_GO")
		iAudioStreamCount += 1
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Shooting range audio bank not loaded. Counter at:", iAudioStreamCount)
		
		IF (iAudioStreamCount > 150)
			// Tries for 10 seconds to stream the audio. If that fails, just return TRUE.
			CDEBUG2LN(DEBUG_SHOOTRANGE, "We've tried to stream audio for 15 seconds... continuing on without audio streamed.")
			
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Helper function to accept button input (the accept button) but make sure the next frame
///    it doesnt read it as well
/// RETURNS:
///    TRUE if the player has Pressed and Released the Accept button
FUNC BOOL IS_ACCEPT_BUTTON_PRESSED(Range_CoreData & sCrData)
	// Have we pressed the skip button?
	IF NOT (sCrData.bSkipPressed)
		IF IS_MENU_ACCEPT_BUTTON_PRESSED()
			sCrData.bSkipPressed = TRUE
			RETURN TRUE
		ENDIF
	// The skip button has been pressed
	ELSE
		// Check if we have released the button
		// after pressing it
		IF NOT IS_MENU_ACCEPT_BUTTON_PRESSED()
			IF (sCrData.bSkipPressed)
				sCrData.bSkipPressed = FALSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    We need to block certain things every frame in the range, such as jumping and climbing. This function handles that.
PROC RANGE_UPDATE_PED_ABILITIES()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
ENDPROC

/// PURPOSE:
///    Disables shooting for a frame.
PROC RANGE_DISABLE_SHOOTING()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
ENDPROC







