// Round_Medal_lib.sch

/// PURPOSE:
///    Returns the medal earned with our current score.
/// RETURNS:
///    The RANGE_ROUND_MEDAL earned, if any.
FUNC RANGE_ROUND_MEDAL GET_MEDAL_AWARD(INT iScoreToCheck, Range_Challenge & sChallenge)
	// If we've made any medals, award them.
	IF (iScoreToCheck >= sChallenge.iGoldReq)
		RETURN RRM_GOLD
		
	ELIF (iScoreToCheck >= sChallenge.iSilverReq)
		RETURN RRM_SILVER
		
	ELIF (iScoreToCheck >= sChallenge.iBronzeReq)
		RETURN RRM_BRONZE
	ENDIF

	// Got nothing.
	RETURN RRM_NONE
ENDFUNC
