USING "hud_drawing.sch"
USING "screens_header.sch"
USING "screen_placements.sch"
USING "screen_placements_export.sch"
USING "range_menu.sch"
USING "range_public.sch"
USING "range_ui.sch"
USING "range_round.sch"
USING "range_support_lib.sch"

CONST_INT 	TEXT_TERTIARY_LEADING 	0
BOOL bIsWidescreen

INT iNumMenuElements

/// PURPOSE:
///    Sets up the Shooting Range's main menu.
PROC INIT_RANGE_MENU(MEGA_PLACEMENT_TOOLS &thisPlacement)
CDEBUG2LN(DEBUG_SHOOTRANGE, "INIT_RANGE_MENU :: called")

	RESET_ALL_SPRITE_PLACEMENT_VALUES(thisPlacement.SpritePlacement)
	SET_STANDARD_INGAME_TEXT_DETAILS(thisPlacement.aStyle)
	bIsWidescreen = GET_IS_WIDESCREEN()

	INIT_SCREEN_RANGE_MENU(thisPlacement)
ENDPROC

PROC RANGE_MENU_DRAW_TARGET_RESULTS(Range_MenuData &sMenuInfo, RANGE_ROUND_TYPE eRndTemp)
	// Draw the HITS area.
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_MEDAL_TITLE_IMAGE].x = 0.742
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_MEDAL_TITLE_IMAGE].y = 0.308
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_MEDAL_TITLE_IMAGE].w = 0.100
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_MEDAL_TITLE_IMAGE].h = 0.178
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_MEDAL_TITLE_IMAGE])
	ENDIF
	DRAW_2D_SPRITE("SRange_Gen", "Hits_Target", sMenuInfo.uiPlacement.SpritePlacement[RANGE_MEDAL_TITLE_IMAGE])
	SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	
	// Draw hits text:
	IF (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
		SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, HUD_COLOUR_WHITE)	
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT].x = 0.629
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT].y = 0.228
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_2_NUMBERS(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_X_HITS", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone1Hit, 100) 
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT].x = 0.629
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT].y = 0.272
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_2_NUMBERS(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_X_HITS", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone2Hit, 50) 
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT].x = 0.629
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT].y = 0.316
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_2_NUMBERS(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_X_HITS", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone3Hit, 25) 
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT].x = 0.629
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT].y = 0.360
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_2_NUMBERS(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_X_HITS", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone4Hit, 10)
		
		// Draw some X's, if we've hit the area.
		SPRITE_PLACEMENT plcDrawX
		plcDrawX.w = 0.0100
		plcDrawX.h = 0.0180
		GET_HUD_COLOUR(HUD_COLOUR_WHITE, plcDrawX.r, plcDrawX.g, plcDrawX.b, plcDrawX.a)
		plcDrawX.fRotation = 0.0000
		
		// Z1
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone1Hit >= 1)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3430 + sMenuInfo.fMenuRightOffset					  
			plcDrawX.y = 0.3250 + sMenuInfo.fMenuUpOffset
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)					  
		ENDIF																	  
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone1Hit >= 2)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3510 + sMenuInfo.fMenuRightOffset					  
			plcDrawX.y = 0.3190 + sMenuInfo.fMenuUpOffset
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)					  
		ENDIF																	  
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone1Hit >= 3)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3490 + sMenuInfo.fMenuRightOffset					  
			plcDrawX.y = 0.3380 + sMenuInfo.fMenuUpOffset						  
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)					  
		ENDIF																	  
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone1Hit >= 4)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3540 + sMenuInfo.fMenuRightOffset
			plcDrawX.y = 0.3320 + sMenuInfo.fMenuUpOffset
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)
		ENDIF
		
		// Z2
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone2Hit >= 1)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3340 + sMenuInfo.fMenuRightOffset					  
			plcDrawX.y = 0.3480 + sMenuInfo.fMenuUpOffset			
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)					  
		ENDIF																	  
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone2Hit >= 2)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3480 + sMenuInfo.fMenuRightOffset					  
			plcDrawX.y = 0.3060 + sMenuInfo.fMenuUpOffset				
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)					  
		ENDIF																	  
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone2Hit >= 3)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3660 + sMenuInfo.fMenuRightOffset					  
			plcDrawX.y = 0.3210 + sMenuInfo.fMenuUpOffset			
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)					  
		ENDIF																	  
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone2Hit >= 4)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3500 + sMenuInfo.fMenuRightOffset
			plcDrawX.y = 0.3540 + sMenuInfo.fMenuUpOffset
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)
		ENDIF
		
		// Z3
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone3Hit >= 1)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3270 + sMenuInfo.fMenuRightOffset					  
			plcDrawX.y = 0.2980 + sMenuInfo.fMenuUpOffset
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)					  
		ENDIF																	  
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone3Hit >= 2)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3380 + sMenuInfo.fMenuRightOffset					  
			plcDrawX.y = 0.2810 + sMenuInfo.fMenuUpOffset	
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)					  
		ENDIF																	  
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone3Hit >= 3)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3750 + sMenuInfo.fMenuRightOffset					  
			plcDrawX.y = 0.3460 + sMenuInfo.fMenuUpOffset		
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)					  
		ENDIF																	  
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone3Hit >= 4)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3440 + sMenuInfo.fMenuRightOffset
			plcDrawX.y = 0.3700 + sMenuInfo.fMenuUpOffset
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)
		ENDIF

		// Z4
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone4Hit >= 1)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3730 + sMenuInfo.fMenuRightOffset
			plcDrawX.y = 0.2730 + sMenuInfo.fMenuUpOffset
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)
		ENDIF
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone4Hit >= 2)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3100 + sMenuInfo.fMenuRightOffset
			plcDrawX.y = 0.3370 + sMenuInfo.fMenuUpOffset
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)
		ENDIF
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone4Hit >= 3)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3280 + sMenuInfo.fMenuRightOffset
			plcDrawX.y = 0.3860 + sMenuInfo.fMenuUpOffset
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)
		ENDIF
		IF (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone4Hit >= 4)
			plcDrawX.w = 0.0100
			plcDrawX.h = 0.0180
			plcDrawX.x = 0.3770 + sMenuInfo.fMenuRightOffset
			plcDrawX.y = 0.3700 + sMenuInfo.fMenuUpOffset
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(plcDrawX)
			ENDIF
			DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", plcDrawX)
		ENDIF
	ELSE
		// Draw 0'ed out hit areas.
		SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, HUD_COLOUR_WHITE)	
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT].x = 0.629
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT].y = 0.228
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_2_NUMBERS(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_X_HITS", 0, 100) 
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT].x = 0.629
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT].y = 0.272
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_2_NUMBERS(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_X_HITS", 0, 50) 
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT].x = 0.629
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT].y = 0.316
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_2_NUMBERS(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_X_HITS", 0, 25) 
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT].x = 0.629
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT].y = 0.360
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_2_NUMBERS(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_X_HITS", 0, 10)
	ENDIF
	
		
	// Always draw hit sprites.
	// Colors set in Range_Scorecard
	SPRITE_COLOR(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1],151,124,19)
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].x = 0.617
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].y = 0.242
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].h = 0.0444
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1])
	ENDIF
	DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1])
	
	SPRITE_COLOR(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2],112,33,35)
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].x = 0.617
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].y = 0.286
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].h = 0.0444
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	ENDIF
	DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	
	SPRITE_COLOR(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3],49,113,107)
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].x = 0.617
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].y = 0.330
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].h = 0.0444
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	ENDIF
	DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	
	SPRITE_COLOR(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4],48,48,48)
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].x = 0.617
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].y = 0.374
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].h = 0.0444
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4])
	ENDIF
	DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4])
ENDPROC


PROC RANGE_MENU_DRAW_SHOTGUN_RESULTS(Range_MenuData &sMenuInfo, RANGE_ROUND_TYPE eRndTemp)
	SPRITE_ROTATION(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1], 0.0)
	SPRITE_ROTATION(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2], 0.0)
	SPRITE_ROTATION(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3], 0.0)
	SPRITE_ROTATION(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4], 0.0)
	
	// Draw the shotgun hit icons.
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].x = 0.6158
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].y = 0.2477
	SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1])
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1])
	ENDIF
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Mini_Target_64", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1])
	
	
	// Draw 2 at red - Reverse order.
	SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].x = 0.6158
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].y = 0.2905
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	ENDIF
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Mini_Target_64", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].x = 0.6228
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].y = 0.2905
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	ENDIF
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Mini_Target_64", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	
	
	// Draw 3 at blue - Reverse order.
	SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].x = 0.6158
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].y = 0.3323
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	ENDIF
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Mini_Target_64", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].x = 0.6228
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].y = 0.3323
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	ENDIF
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Mini_Target_64", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].x = 0.6298
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].y = 0.3323
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	ENDIF
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Mini_Target_64", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	
	
	// Draw 4 at black - Reverse order.
	SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4])
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].x = 0.6158
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].y = 0.3764
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4])
	ENDIF
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Mini_Target_64", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4])
	
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].x = 0.6228
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].y = 0.3764
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4])
	ENDIF
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Mini_Target_64", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4])
	
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].x = 0.6298
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].y = 0.3764
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4])
	ENDIF
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Mini_Target_64", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4])
	
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].x = 0.6368
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4].y = 0.3764
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4])
	ENDIF
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Mini_Target_64", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_4])
	
	//Chal_Mini_Target_64
	
	// ZONE TEXT
	// Need to draw text for what the targets mean beneath each of them:
	// Position the test first!!!
	SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, HUD_COLOUR_WHITE)	
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT].x = 0.627
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT].y = 0.2367
	IF NOT bIsWidescreen
		FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	ENDIF
	DRAW_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_1PT")
	
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT].x = 0.634
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT].y = 0.2795
	IF NOT bIsWidescreen
		FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	ENDIF
	DRAW_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_3PT")
	
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT].x = 0.641
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT].y = 0.3213
	IF NOT bIsWidescreen
		FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	ENDIF
	DRAW_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_5PT")
	
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT].x = 0.648
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT].y = 0.3654
	IF NOT bIsWidescreen
		FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	ENDIF
	DRAW_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_4_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_7PT")
	
	// Draw the plus over the last one.
	SPRITE_PLACEMENT tempPlus
	tempPlus.x = 0.649
	tempPlus.y = 0.366
	tempPlus.w = 0.025
	tempPlus.h = 0.044
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(tempPlus)
	ENDIF
	SET_SPRITE_BLACK(tempPlus)
	DRAW_2D_SPRITE("Shared", "MenuPlus_32", tempPlus)
	
	tempPlus.x = 0.648
	tempPlus.y = 0.3645
	tempPlus.w = 0.025
	tempPlus.h = 0.044
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(tempPlus)
	ENDIF
	SET_SPRITE_WHITE(tempPlus)
	DRAW_2D_SPRITE("Shared", "MenuPlus_32", tempPlus)

	// SCORE TEXT
	SET_TEXT_WRAPPED_TO_RECT(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_IMAGE_BACKGROUND], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
	
	IF (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
		SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, HUD_COLOUR_WHITE)
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT].x = 0.80
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT].y = 0.227
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "RNG_XSCORE", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone4Hit, FONT_RIGHT) 
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT].x = 0.80
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT].y = 0.2698
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "RNG_XSCORE", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone3Hit, FONT_RIGHT) 
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT].x = 0.80
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT].y = 0.3116
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "RNG_XSCORE", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone2Hit, FONT_RIGHT) 
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_4_TXT].x = 0.80
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_4_TXT].y = 0.3557
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_4_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_4_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "RNG_XSCORE", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone1Hit, FONT_RIGHT)
	ELSE
				SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, HUD_COLOUR_WHITE)
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT].x = 0.80
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT].y = 0.227
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "RNG_XSCORE", 0, FONT_RIGHT) 		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT].x = 0.80
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT].y = 0.2698
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "RNG_XSCORE", 0, FONT_RIGHT) 
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT].x = 0.80
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT].y = 0.3116
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "RNG_XSCORE", 0, FONT_RIGHT) 
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_4_TXT].x = 0.80
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_4_TXT].y = 0.3557
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_4_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_4_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "RNG_XSCORE", 0, FONT_RIGHT)
	ENDIF
ENDPROC


PROC RANGE_MENU_DRAW_MINIGUN_RESULTS(Range_MenuData &sMenuInfo, RANGE_ROUND_TYPE eRndTemp)	
	SPRITE_ROTATION(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1], 345.0)
	SPRITE_ROTATION(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2], 345.0)
	SPRITE_ROTATION(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3], 345.0)
	
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].w = sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].w
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].h = sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].h 
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].w = sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].w
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].h = sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].h
	
	// Draw the shotgun hit icons.
	SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1])
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].x = 0.6415
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1].y = 0.254	
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1])
	ENDIF
	DRAW_2D_SPRITE("SRange_Gen", "Icon_Target_32", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_1])
	
	
	// Draw 2 at red - Reverse order.
	SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].x = 0.7025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].y = 0.254
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	ENDIF
	DRAW_2D_SPRITE("SRange_Gen", "Icon_Target_32", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].x = 0.6975
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2].y = 0.254	
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	ENDIF
	DRAW_2D_SPRITE("SRange_Gen", "Icon_Target_32", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_2])
	
	
	// Draw 3 at blue - Reverse order.
	SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].x = 0.7635
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].y = 0.254	
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	ENDIF
	DRAW_2D_SPRITE("SRange_Gen", "Icon_Target_32", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].x = 0.7585
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].y = 0.254	
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	ENDIF
	DRAW_2D_SPRITE("SRange_Gen", "Icon_Target_32", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].w = 0.025
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].h = 0.044
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].x = 0.7535
	sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3].y = 0.254	
	IF NOT bIsWidescreen
		FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
	ENDIF
	DRAW_2D_SPRITE("SRange_Gen", "Icon_Target_32", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_TARGET_3])
		
	// ZONE TEXT
	// Need to draw text for what the targets mean beneath each of them:
	// Position the test first!!!
	SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, HUD_COLOUR_WHITE)
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT].x = 0.642
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT].y = 0.280
	IF NOT bIsWidescreen
		FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	ENDIF
	DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "HUD_MULTSMAL", 100, FONT_CENTRE)
	
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT].x = 0.701
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT].y = 0.280
	IF NOT bIsWidescreen
		FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	ENDIF
	DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "HUD_MULTSMAL", 200, FONT_CENTRE)
	
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT].x = 0.760
	sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT].y = 0.280
	IF NOT bIsWidescreen
		FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	ENDIF
	DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "HUD_MULTSMAL", 300, FONT_CENTRE)


	// SCORE TEXT
	IF (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
		SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, HUD_COLOUR_WHITE)
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT].x = 0.642
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT].y = 0.3116
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "NUMBER", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone3Hit, FONT_CENTRE)
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT].x = 0.701
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT].y = 0.3116
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "NUMBER", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone2Hit, FONT_CENTRE)
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT].x = 0.760
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT].y = 0.3116
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "NUMBER", g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].iZone1Hit, FONT_CENTRE)
	ELSE
		SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, HUD_COLOUR_WHITE)
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT].x = 0.642
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT].y = 0.3116
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_1_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "NUMBER", 0, FONT_CENTRE)
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT].x = 0.701
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT].y = 0.3116
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_2_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "NUMBER", 0, FONT_CENTRE)
		
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT].x = 0.760
		sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT].y = 0.3116
		IF NOT bIsWidescreen
			FIXUP_TEXT_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TARGET_HITS_3_TXT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "NUMBER", 0, FONT_CENTRE)
	ENDIF
ENDPROC



/// PURPOSE:
///    Draws the main menu for the shooting range.
PROC PROCESS_SCREEN_RANGE_MENU(Range_WeaponRounds &sWeaponRndInfo[], Range_MenuData &sMenuInfo, Range_CoreData & sCoreInfo)

	#IF IS_DEBUG_BUILD
	STRING sResolution = PICK_STRING( bIsWidescreen, "16:9", "4:3" )
	DRAW_DEBUG_TEXT_2D( sResolution, (<< 0.9, 0.1, 0.0 >>) )
	#ENDIF
	
	// When displaying an unlock icon, we ned to draw it at the end of the text, not at the normal placement spot. This will handle that.
	SPRITE_PLACEMENT	sprPlaceTemp
	TEXT_PLACEMENT		texPlaceTemp

	// Draw everything! Basically, want to remove this later...
	// STATIC MENU DRAW
	// TITLE
	// Draw the BG.
	DRAW_2D_SPRITE("Shared", "BGGradient_32x1024", sMenuInfo.uiPlacement.SpritePlacement[RANGE_MAIN_BACKGROUND], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_BEFORE_HUD)
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	DRAW_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGE_MENU_MAIN_TITLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "SHR_TITLE")

	SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)

	// WEAPON COLUMN TITLE
	// =========================================================================================================
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_WEAPON_BACKGROUD])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_WEAPON_EDGING])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_WEAPON_IMAGE_BACKGROUD])
	DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_WEAPON_TITLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "SHR_CHOOSE_WEAP", TRUE, FALSE)
	
	// Draw an image depending on what category we have highlighted.
	SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE])
	IF (sMenuInfo.curMenu = RANGE_MENU_WEAPCAT)		
		sMenuInfo.weapMenu.iCurElement = 0
		RANGE_WEAPON_CATEGORY eTempCat = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement)	
		TEXT_LABEL_15 szWeapcatSprite = "Weapcat_"
		TEXT_LABEL_15 szWeapcatFolder = "SRange_Gen"
		SWITCH (eTempCat)
			CASE WEAPCAT_PISTOL		
				szWeapcatSprite += "HG"		
				IF sCoreInfo.bPistolDLC
					szWeapcatSprite += "_2"
				ENDIF
			BREAK
			CASE WEAPCAT_SMG		
				szWeapcatSprite += "SMG"	
				IF sCoreInfo.bSMGDLC
					szWeapcatSprite += "_2"
				ENDIF
			BREAK
			CASE WEAPCAT_AR			
				szWeapcatSprite += "AR"		
				IF sCoreInfo.bARDLC
					szWeapcatSprite += "_2"
				ENDIF
			BREAK
			CASE WEAPCAT_SHOTGUN
				szWeapcatSprite += "SG"		
				IF sCoreInfo.bShotgunDLC
					szWeapcatSprite += "_2"
				ENDIF
			BREAK
			CASE WEAPCAT_LIGHT_MG	
				szWeapcatSprite += "LMG"		
				IF sCoreInfo.bLMGDLC
					szWeapcatSprite += "_2"
				ENDIF
			BREAK
			CASE WEAPCAT_MINIGUN	
				szWeapcatSprite += "HVY"			
			BREAK
			CASE WEAPCAT_RAILGUN
				szWeapcatSprite = "Weap_RG_1"
				szWeapcatFolder = "SRange_Weap"
			BREAK
		ENDSWITCH
		PIXEL_POSITION_AND_SIZE_SPRITE(sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE], 384.0000, 224.0000, 256.0000, 256.0000)
		IF NOT bIsWidescreen
			FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE])
		ENDIF
		DRAW_2D_SPRITE(szWeapcatFolder, szWeapcatSprite, sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE])
		DRAW_DEBUG_TEXT_2D( szWeapcatSprite, (<< sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE].x, sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE].y, 0.0 >>) )
		
	ELIF (sMenuInfo.curMenu = RANGE_MENU_WEAPONS) OR (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
		// If we're on a locked weapon, dim the weapon image. If we're on the challenge menu, don't.
		RANGE_WEAPON_CATEGORY eTempCat = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement)
		RANGE_WEAPON_TYPE eTempWeap = INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.weapMenu.iCurElement)
		TEXT_LABEL_15 szDirName
		TEXT_LABEL_15 szWeapName = GET_WEAPON_SPRITE_NAME(eTempCat, eTempWeap, szDirName)
		
		// Pistols need to be downsized 10%.
		IF (sMenuInfo.curMenu > RANGE_MENU_WEAPCAT)
		AND (eTempCat = WEAPCAT_PISTOL)
			PIXEL_POSITION_AND_SIZE_SPRITE(sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE], 384.0000, 224.0000, 230.4, 230.4)
		ELSE
			PIXEL_POSITION_AND_SIZE_SPRITE(sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE], 384.0000, 224.0000, 256.0000, 256.0000)			
		ENDIF
		IF NOT bIsWidescreen
			FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE])
		ENDIF
		DRAW_2D_SPRITE(szDirName, szWeapName, sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE])
		DRAW_DEBUG_TEXT_2D( szDirName, (<< sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE].x, sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE].y, 0.0 >>) )
		DRAW_DEBUG_TEXT_2D( szWeapName, (<< sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE].x, sMenuInfo.uiPlacement.SpritePlacement[RANGE_WEAPON_TITLE_IMAGE].y + 0.0125, 0.0 >>) )
	ENDIF
	
	// CHALLENGE COLUMN TITLE
	// ==========================================================================================================
	// This draw the actual title at the very top of the screen.
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_CHALLENGE_BACKGROUND])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_CHALLENGE_EDGING])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_CHALLENGE_IMAGE_BACKGROUND])
	DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_CHALLENGE_TITLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "SHR_CHALLENGES", TRUE, FALSE)
	
	// Here, we get the image name of what we'll be drawing at the top of the screen.
	// We also flag it as dimmed if need be (challenge locked or generic image because we aren't on that selection)
	TEXT_LABEL_15 szName = "Chal_Blank"
	TEXT_LABEL_15 szFolderName = "SRange_Chal"
	IF (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
		szName = GET_CHALLENGE_SPRITE_NAME(INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), sMenuInfo.chalMenu.iCurElement, szFolderName)
	ENDIF
	DRAW_2D_SPRITE(szFolderName, szName, sMenuInfo.uiPlacement.SpritePlacement[RANGE_CHALLENGE_TITLE_IMAGE])	

	// AWARD COLUMN TITLE
	// ==================================================================================================================
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_BACKGROUND])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_EDGING])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_IMAGE_BACKGROUND])
	DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_TITLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "SHR_RESULTS", TRUE, FALSE)
	
	// DRAW DEPENDING ON THE ROUND HIGHLIGHTED
	// ==================================================================================================================
	IF sMenuInfo.chalMenu.iCurElement < 0
		sMenuInfo.chalMenu.iCurElement = 0		//B* 2707679: Failsafe to have a valid array index
	ENDIF
	
	INT iElement = PICK_INT(sMenuInfo.curMenu = RANGE_MENU_CHALLENGES, sMenuInfo.chalMenu.iCurElement, 0)

	RANGE_ROUND_TYPE eRndTemp = sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[iElement].eRndType
	IF (sMenuInfo.curMenu != RANGE_MENU_CHALLENGES)
		IF (sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[0].eScoringStyle = ROUNDSCORE_DESTRUCTION_W_BONUS)
			RANGE_MENU_DRAW_SHOTGUN_RESULTS(sMenuInfo, eRndTemp)
		ELIF (sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[0].eScoringStyle = ROUNDSCORE_MINIGUN)
			RANGE_MENU_DRAW_MINIGUN_RESULTS(sMenuInfo, eRndTemp)
		ELSE
			RANGE_MENU_DRAW_TARGET_RESULTS(sMenuInfo, eRndTemp)
		ENDIF
	ELSE
		// Display the scoring type of the challenge...
		IF (sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[sMenuInfo.chalMenu.iCurElement].eScoringStyle = ROUNDSCORE_DESTRUCTION_W_BONUS)
			RANGE_MENU_DRAW_SHOTGUN_RESULTS(sMenuInfo, eRndTemp)
		ELIF (sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[sMenuInfo.chalMenu.iCurElement].eScoringStyle = ROUNDSCORE_MINIGUN)
			RANGE_MENU_DRAW_MINIGUN_RESULTS(sMenuInfo, eRndTemp)
		ELSE
			RANGE_MENU_DRAW_TARGET_RESULTS(sMenuInfo, eRndTemp)
		ENDIF
	ENDIF

	SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)

	// AWARD COLUMN MEDAL SCORE BG
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_BRONZE_MEDAL_BG])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_SILVER_MEDAL_BG])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_GOLD_MEDAL_BG])
	
	
	// DYNAMIC MENU DRAW
	// ==================================================================================================================
	// ==================================================================================================================
	// WEAPON COLUMN -- Handles drawing of categories/weapon submenu
	INT iCurrentMenuSlot = 0
	RANGE_WEAPON_CATEGORY iCategoryIndex
	RANGE_WEAPON_TYPE iWeaponIndex
	RANGE_SCREEN_RECT eCurrentRect
	RANGE_SCREEN_SPRITE eCurrentSprite
	RANGE_SCREEN_TEXT eCurrentText
	
	FLOAT fDisplayAtX = k_MENU_DEBUG_OUTPUT_X
	FLOAT fDisplayAtY = k_MENU_DEBUG_OUTPUT_Y
	FLOAT fDisplayAdd = k_MENU_DEBUG_OUTPUT_SPACING
	TEXT_LABEL_63 texDisplay = "Debug Menu Output"
	DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>) )
	fDisplayAtY += fDisplayAdd
	
	REPEAT NUM_WEAPON_CATS iCategoryIndex
	
		IF NOT (iCategoryIndex = WEAPCAT_RAILGUN AND sCoreInfo.bCGtoNG = FALSE)
			
			#IF IS_DEBUG_BUILD
			texDisplay = "iCatIndex = "
			texDisplay += GET_STRING_FROM_RANGE_WEAPON_CATEGORY(iCategoryIndex)
			DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>) )
			fDisplayAtY += fDisplayAdd
			texDisplay = "iCurrentMenuSlot = "
			texDisplay += iCurrentMenuSlot
			DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>) )
			fDisplayAtY += fDisplayAdd
			#ENDIF
	
			eCurrentRect = RANGE_WEAPON_ITEM_BG_1 + INT_TO_ENUM(RANGE_SCREEN_RECT, iCurrentMenuSlot)
			eCurrentSprite = RANGE_WEAPON_ITEM_INFO_1 + INT_TO_ENUM(RANGE_SCREEN_SPRITE, iCurrentMenuSlot)
			eCurrentText = RANGE_WEAPON_ITEM_TEXT_1 + INT_TO_ENUM(RANGE_SCREEN_TEXT, iCurrentMenuSlot)
			
			// If we're in the weapCat menu, all text is white. Selected item is white with black text.
			IF (sMenuInfo.curMenu = RANGE_MENU_WEAPCAT)
				IF (iCategoryIndex = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement))
					SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_WHITE, TRUE)				
					SET_SPRITE_BLACK(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
					SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				ELSE
					SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
					SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
					SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				ENDIF
			ELSE
				// Not in weapcat menu. The highlighted item is Red w/ black text. Everything else is white.
				IF (iCategoryIndex = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement))
					SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_WHITE, TRUE)
					SET_SPRITE_BLACK(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
					SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				ELSE
					SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
					SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
					SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
				ENDIF
			ENDIF
			DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect])
			texPlaceTemp = sMenuInfo.uiPlacement.TextPlacement[eCurrentText]
			IF NOT sMenuInfo.catMenu.menuElement[iCategoryIndex].bActive
				DRAW_2D_SPRITE("Shared", "Locked_Icon_32", sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
			ELSE
				// It's active. If we're in the weapcat menu, draw a '+'. 
				// If we're not, and our cur weapcat menu item is this, draw a down arrow. If our weapcat menu is something else, draw the '+'.
				IF (iCategoryIndex = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement))
					SET_SPRITE_HUD_COLOUR(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite], HUD_COLOUR_BLACK)
					DRAW_2D_SPRITE("Shared", PICK_STRING((sMenuInfo.curMenu = RANGE_MENU_WEAPCAT), "MenuPlus_32", "MenuArrow_32"), sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
				ENDIF

				// If there's new weapons in the cat, mark it as new.
				IF DOES_WEAPON_CATEGORY_CONTAIN_ANY_NEW_WEAPONS(iCategoryIndex)	
					// Get the new sprite placement:
					PIXEL_POSITION_AND_SIZE_SPRITE(sprPlaceTemp, 269.0000, 0.0, 32.0000, 32.0000)
					SPRITE_COLOR(sprPlaceTemp)
					sprPlaceTemp.y = sMenuInfo.uiPlacement.TextPlacement[eCurrentText].y + NEW_STAR_OFFSET_Y
					texPlaceTemp.x = sprPlaceTemp.x + NEW_STAR_OFFSET_X
					SET_SPRITE_WHITE(sprPlaceTemp)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprPlaceTemp)
					ENDIF
					DRAW_2D_SPRITE("CommonMenu", "shop_NEW_Star", sprPlaceTemp)
					
					// Only fixup if we're shifting the text.
					IF NOT bIsWidescreen
						FIXUP_TEXT_FOR_NON_WIDESCREEN(texPlaceTemp, sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					ENDIF
				ENDIF
			ENDIF
			DRAW_TEXT(texPlaceTemp, sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, GET_CATEGORY_STRING_INFO(iCategoryIndex)) 

			// Reset things that might have been adjusted
			SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
			SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
			SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
			iCurrentMenuSlot++

			// Responsible for drawing the weapons in the category.
			IF iCategoryIndex = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement)
				
				texDisplay = "iCurElement = "
				texDisplay += sMenuInfo.catMenu.iCurElement
				DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>) )
				fDisplayAtY += fDisplayAdd
				texDisplay = "iNumElements = "
				texDisplay += sMenuInfo.weapMenu.iNumElements
				DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>) )
				fDisplayAtY += fDisplayAdd
				
				REPEAT sMenuInfo.weapMenu.iNumElements iWeaponIndex
				
					texDisplay = "iWeaponIndex = "
					texDisplay += ENUM_TO_INT(iWeaponIndex)
					DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>) )
					fDisplayAtY += fDisplayAdd
					texDisplay = "iCurrentMenuSlot = "
					texDisplay += iCurrentMenuSlot
					DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>) )
					fDisplayAtY += fDisplayAdd
				
					eCurrentRect = RANGE_WEAPON_ITEM_BG_1 + INT_TO_ENUM(RANGE_SCREEN_RECT, iCurrentMenuSlot)
					eCurrentSprite = RANGE_WEAPON_ITEM_INFO_1 + INT_TO_ENUM(RANGE_SCREEN_SPRITE, iCurrentMenuSlot)
					eCurrentText = RANGE_WEAPON_ITEM_TEXT_1 + INT_TO_ENUM(RANGE_SCREEN_TEXT, iCurrentMenuSlot)
					
					// If we're in the weapons menu, all text is white. Selected item is white with black text.
					IF (sMenuInfo.curMenu = RANGE_MENU_WEAPONS)
						IF (iWeaponIndex = INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.weapMenu.iCurElement))
							SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_WHITE, TRUE)					
							SET_SPRITE_BLACK(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
							SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
						ELSE
							SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
							SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
							SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
						ENDIF
					ELSE
						// We're not in the weapons menu. Highlighted item is red w/ black text. Everything else is white.
						IF (iWeaponIndex = INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.weapMenu.iCurElement))
							SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_WHITE, TRUE)
							SET_SPRITE_BLACK(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
							SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
						ELSE
							SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
							SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
							SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
						ENDIF
					ENDIF
					DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect])
					SET_TEXT_TABBED(sMenuInfo.uiPlacement.TextPlacement[eCurrentText])
					
					INT iWeaponBit
					iWeaponBit = (ENUM_TO_INT(iCategoryIndex) * 4) + ENUM_TO_INT(iWeaponIndex)
					texPlaceTemp = sMenuInfo.uiPlacement.TextPlacement[eCurrentText]
					IF NOT sMenuInfo.weapMenu.menuElement[iWeaponIndex].bActive
						DRAW_2D_SPRITE("Shared", "Locked_Icon_32", sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
						
					ELIF IS_BIT_SET(g_savedGlobals.sRangeData[ePlayerChar].iWeaponUsedStatus, iWeaponBit)
						IF (iWeaponIndex = INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.weapMenu.iCurElement))
							SET_SPRITE_BLACK(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
						ELSE
							SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
						ENDIF
						DRAW_2D_SPRITE("CommonMenu", "Shop_Tick_Icon", sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
					ELSE 
						// If we've never highlighted it before, set it as 
						IF NOT IS_BIT_SET(g_savedGlobals.sRangeData[ePlayerChar].iWeaponLockStatus, iWeaponBit)
							// Get the new sprite placement:
							PIXEL_POSITION_AND_SIZE_SPRITE(sprPlaceTemp, 285.0000, 0.0, 32.0000, 32.0000)
							sprPlaceTemp.y = sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite].y
							texPlaceTemp.x = sprPlaceTemp.x + NEW_STAR_OFFSET_X
							
							SET_SPRITE_WHITE(sprPlaceTemp)
							IF NOT bIsWidescreen
								FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprPlaceTemp)
							ENDIF
							DRAW_2D_SPRITE("CommonMenu", "shop_NEW_Star", sprPlaceTemp)
							
							// Only fixup the text again if we shifted it because of the star.
							IF NOT bIsWidescreen
								FIXUP_TEXT_FOR_NON_WIDESCREEN(texPlaceTemp, sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
							ENDIF
						ENDIF
					ENDIF
					// Always draw weapon name. Unlock info moves to challenge column.
					IF iCategoryIndex = WEAPCAT_RAILGUN AND iWeaponIndex = RANGE_RAILGUN
						DRAW_TEXT(texPlaceTemp, sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "WT_RAILGUN") 
					ELSE
						DRAW_TEXT(texPlaceTemp, sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, GET_WEAPON_NAME(GET_WEAPONTYPE_FROM_RANGE_INFO(iCategoryIndex, iWeaponIndex))) 	
					ENDIF
					
					// Reset things that might have been adjusted
					SET_TEXT_UNTABBED(sMenuInfo.uiPlacement.TextPlacement[eCurrentText])
					SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
					SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
					SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
					iCurrentMenuSlot++
				ENDREPEAT
			ENDIF
		
		ENDIF
	ENDREPEAT
	
	iNumMenuElements = iCurrentMenuSlot // Needed for mouse menu support
	
	
	// CHALLENGE COLUMN DYNAMIC
	// ==================================================================================================================
	INT iChallengeIndex
	RANGE_SCREEN_RECT eCurrentInfoRect
	RANGE_SCREEN_TEXT eCurrentInfoText
	TEXT_LABEL_15 challengeTitle, txtLockInfo
	REPEAT sMenuInfo.chalMenu.iNumElements iChallengeIndex
		eCurrentRect = RANGE_CHALLENGE_1_TITLE_BG + INT_TO_ENUM(RANGE_SCREEN_RECT, iChallengeIndex)
		eCurrentSprite = RANGE_CHALLENGE_CATEGORY_MEDAL_1 + INT_TO_ENUM(RANGE_SCREEN_SPRITE, iChallengeIndex)
		eCurrentText = RANGE_CHALLENGE_CATEGORY_TITLE_1 + INT_TO_ENUM(RANGE_SCREEN_TEXT, iChallengeIndex)
		eCurrentInfoRect = RANGE_CHALLENGE_1_INFO_BG + INT_TO_ENUM(RANGE_SCREEN_RECT, iChallengeIndex)
		eCurrentInfoText = RANGE_CHALLENGE_CATEGORY_INFO_1 + INT_TO_ENUM(RANGE_SCREEN_TEXT, iChallengeIndex)
		challengeTitle = "SHR_CHALL_"
		challengeTitle += iChallengeIndex + 1

		// If we're highlighting the item, the text is black, otherwise it's white.
		IF (iChallengeIndex = sMenuInfo.chalMenu.iCurElement) AND (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
			SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ELSE
			SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		
		// If we're in the challenges menu, we can highlight things.
		IF (iChallengeIndex = sMenuInfo.chalMenu.iCurElement) AND (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
			SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_WHITE, TRUE)
		ELSE
			SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
		ENDIF
		SET_TEXT_WRAPPED_TO_RECT(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, sMenuInfo.uiPlacement.RectPlacement[eCurrentInfoRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		
		// We need to see if we're over a locked item.
		BOOL bCurItemUnlocked = FALSE
		IF (sMenuInfo.curMenu = RANGE_MENU_WEAPCAT)
			// In the category menu
			IF (sMenuInfo.catMenu.menuElement[sMenuInfo.catMenu.iCurElement].bActive)
				bCurItemUnlocked = TRUE
			ENDIF
			
		ELIF (sMenuInfo.curMenu = RANGE_MENU_WEAPONS)
			// Okay, so we're in the weapons. Is it locked?
			IF (sMenuInfo.weapMenu.menuElement[sMenuInfo.weapMenu.iCurElement].bActive)
				bCurItemUnlocked = TRUE
			ENDIF
		ELSE
			bCurItemUnlocked = TRUE
		ENDIF
		
		// This draws: Challenge #_                   (Medal image)                  and the info below it in the center column
		// Will always at least draw the first Challenge title + Info boxes.
		// This is drawing the challenge title rect.
		IF (bCurItemUnlocked OR iChallengeIndex = 0)
			DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect])
			
			// This is drawing the detail rect.
			SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
			DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[eCurrentInfoRect])
		ENDIF
		
		// This attempts to give the challenge a description.
		txtLockInfo = ""
		TEXT_LABEL_15 txtSubstring = ""

		// If the current item is unlocked, display info for it.
		IF bCurItemUnlocked
			IF (sMenuInfo.chalMenu.menuElement[iChallengeIndex].bActive)
				// The challenge is unlocked. Draw its description.
				txtLockInfo = sMenuInfo.chalMenu.challengeDesc[iChallengeIndex]

				// If it's a challenge that's passed, put a medal up for it.
				IF IS_ROUND_PASSED(sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[iChallengeIndex].eRndType)
					SWITCH GET_ROUND_MEDAL(sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[iChallengeIndex].eRndType)
						CASE RRM_BRONZE
							SET_SPRITE_HUD_COLOUR(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite], HUD_COLOUR_BRONZE)
							DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
						BREAK
						CASE RRM_SILVER
							SET_SPRITE_HUD_COLOUR(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite], HUD_COLOUR_SILVER)
							DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
						BREAK
						CASE RRM_GOLD
							SET_SPRITE_HUD_COLOUR(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite], HUD_COLOUR_GOLD)
							DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
						BREAK
					ENDSWITCH
				ENDIF
			ELSE
				// The challenge is locked. Draw its unlock requirement.
				IF (iChallengeIndex = sMenuInfo.chalMenu.iCurElement) AND (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
					SET_SPRITE_BLACK(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
				ELSE
					SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
				ENDIF
				DRAW_2D_SPRITE("Shared", "Locked_Icon_32", sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
				txtLockInfo = "SHR_C"
				txtLockInfo += iChallengeIndex+1
				txtLockInfo += "_REQ"
			ENDIF
		ELSE
			// Current thing was locked. Display info for it.
			IF (iChallengeIndex = 0)
				// Draw a lock on the top item.
				SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
				DRAW_2D_SPRITE("Shared", "Locked_Icon_32", sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
				
				challengeTitle = "SHR_CHALL_1"
				
				IF (sMenuInfo.curMenu = RANGE_MENU_WEAPCAT)
					txtLockInfo = GET_WEAPON_UNLOCK_INFO(INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), INT_TO_ENUM(RANGE_WEAPON_TYPE, 0), sCoreInfo.bCGtoNG)
					txtSubstring = GET_CATEGORY_STRING_INFO(INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), TRUE)
				ELIF (sMenuInfo.curMenu = RANGE_MENU_WEAPONS)
					txtLockInfo = GET_WEAPON_UNLOCK_INFO(INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.weapMenu.iCurElement), sCoreInfo.bCGtoNG)
					txtSubstring = GET_THE_WEAPON_NAME(GET_WEAPONTYPE_FROM_RANGE_INFO(INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.weapMenu.iCurElement)))
				ENDIF
			ENDIF
		ENDIF

		IF GET_LENGTH_OF_LITERAL_STRING(txtLockInfo) != 0
			SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
			
			// Draw the title only if we're also going to draw the description. However, the title could have change to directions
			// on how to unlock.
			DRAW_TEXT(sMenuInfo.uiPlacement.TextPlacement[eCurrentText], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, challengeTitle)
			
			SET_TEXT_LEADING(TEXT_TERTIARY_LEADING)
			IF GET_LENGTH_OF_LITERAL_STRING(txtSubstring) != 0
				// Had a substring, draw that.
				DRAW_TEXT_WITH_TEXT(sMenuInfo.uiPlacement.TextPlacement[eCurrentInfoText], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, 
						txtLockInfo, txtSubstring)
			ELSE
				DRAW_TEXT(sMenuInfo.uiPlacement.TextPlacement[eCurrentInfoText], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, txtLockInfo)
			ENDIF
		ENDIF
				
		// Reset things that might have been adjusted
		SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
		SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
		SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
		CLEAR_TEXT_WRAPPED(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
	ENDREPEAT
	

	// AWARD COLUMN	DYNAMIC
	// ==================================================================================================================
	SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, HUD_COLOUR_WHITE)

	// DRAW ALL LABELS ON THE LEFT OF THE COLUMN:
	// Medal Label
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_1])
	DRAW_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_TEXT_1], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_M_BEST")
	
	// Score Label
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_2])
	DRAW_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_TEXT_2], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_SCORE_REC")
	
	// Acuracy Label
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_3])
	DRAW_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_TEXT_3], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ACC_REC")
	
	// High Score Label
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_4])
	DRAW_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_TEXT_4], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_HI_SCORE")
	
	
	INT iHighScore = 0
	IF (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
		eRndTemp = sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[sMenuInfo.chalMenu.iCurElement].eRndType
		iHighScore = GET_ROUND_HIGH_SCORE(eRndTemp)
	ENDIF
	
	// DRAW ALL VALUES ON THE RIGHT OF THE COLUMN -- IF THERE'S NO HIGH SCORE, DRAW THE DASHES:
	IF (iHighScore = 0)
		// Empty medal, score, best score and accuracy %.
		SET_TEXT_WRAPPED_TO_RECT(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_1], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_STAT_1], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH", FALSE, TRUE)
		
		SET_TEXT_WRAPPED_TO_RECT(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_2], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_STAT_2], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH", FALSE, TRUE)

		SET_TEXT_WRAPPED_TO_RECT(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_3], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_STAT_3], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH", FALSE, TRUE)
		
		SET_TEXT_WRAPPED_TO_RECT(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_4], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_STAT_4], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH", FALSE, TRUE)

		// Empty medals.
		sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_BRONZE_GOAL_IMAGE].a = 76
		DRAW_2D_SPRITE("SRange_Gen", "Shooting_Bronze_128", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_BRONZE_GOAL_IMAGE], FALSE)
		sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_SILVER_GOAL_IMAGE].a = 76
		DRAW_2D_SPRITE("SRange_Gen", "Shooting_Silver_128", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_SILVER_GOAL_IMAGE], FALSE)
		sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_GOLD_GOAL_IMAGE].a = 76
		DRAW_2D_SPRITE("SRange_Gen", "Shooting_Gold_128", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_GOLD_GOAL_IMAGE], FALSE)
	ELSE
		RANGE_ROUND_MEDAL eMedalTemp = GET_ROUND_MEDAL(sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[sMenuInfo.chalMenu.iCurElement].eRndType)
		
		// Medal, Score, High Score, Accuracy % Value
		SET_TEXT_WRAPPED_TO_RECT(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_1], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		IF (eMedalTemp = RRM_GOLD)
			SET_TEXT_GOLD(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_STAT_1], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_M_GOLD", FALSE, TRUE)
		ELIF (eMedalTemp = RRM_SILVER)
			SET_TEXT_SILVER(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_STAT_1], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_M_SILVER", FALSE, TRUE)
		ELIF (eMedalTemp = RRM_BRONZE)
			SET_TEXT_BRONZE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_STAT_1], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_M_BRONZE", FALSE, TRUE)
		ELSE
			SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_STAT_1], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH", FALSE, TRUE)
		ENDIF
		SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		
		SET_TEXT_WRAPPED_TO_RECT(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_2], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_STAT_2], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", GET_ROUND_LAST_SCORE(eRndTemp), FONT_RIGHT)
		
		SET_TEXT_WRAPPED_TO_RECT(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_3], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT_WITH_FLOAT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_STAT_3], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "HUD_PERCENT", 
				g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRndTemp].fAccuracy, 1, FONT_RIGHT)

		SET_TEXT_WRAPPED_TO_RECT(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_ITEM_BG_4], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_ITEM_STAT_4], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", GET_ROUND_HIGH_SCORE(eRndTemp), FONT_RIGHT)

		// Draw medals based on what's eanred.
		IF (eMedalTemp >= RRM_BRONZE)
			sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_BRONZE_GOAL_IMAGE].a = 255
		ELSE
			sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_BRONZE_GOAL_IMAGE].a = 76
		ENDIF
		DRAW_2D_SPRITE("SRange_Gen", "Shooting_Bronze_128", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_BRONZE_GOAL_IMAGE], FALSE)
		
		IF (eMedalTemp >= RRM_SILVER)
			sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_SILVER_GOAL_IMAGE].a = 255
		ELSE
			sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_SILVER_GOAL_IMAGE].a = 76
		ENDIF
		DRAW_2D_SPRITE("SRange_Gen", "Shooting_Silver_128", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_SILVER_GOAL_IMAGE], FALSE)
		
		IF (eMedalTemp >= RRM_GOLD)
			sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_GOLD_GOAL_IMAGE].a = 255
		ELSE
			sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_GOLD_GOAL_IMAGE].a = 76
		ENDIF
		DRAW_2D_SPRITE("SRange_Gen", "Shooting_Gold_128", sMenuInfo.uiPlacement.SpritePlacement[RANGE_AWARDS_GOLD_GOAL_IMAGE], FALSE)
	ENDIF
	
	SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_BRONZE, TRUE)
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_BRONZE_SCORE_BG])
	
	SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_SILVER, TRUE)
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_SILVER_SCORE_BG])
	
	SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_GOLD, TRUE)
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGE_AWARDS_GOLD_SCORE_BG])
	
	SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	IF sMenuInfo.curMenu = RANGE_MENU_CHALLENGES AND sMenuInfo.chalMenu.menuElement[sMenuInfo.chalMenu.iCurElement].bActive
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_BRONZE_GOAL_SCORE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", GET_MEDAL_SCORE_REQUIREMENT(sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[sMenuInfo.chalMenu.iCurElement], RRM_BRONZE), FONT_CENTRE)	
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_SILVER_GOAL_SCORE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", GET_MEDAL_SCORE_REQUIREMENT(sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[sMenuInfo.chalMenu.iCurElement], RRM_SILVER), FONT_CENTRE)	
		DRAW_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_GOLD_GOAL_SCORE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", GET_MEDAL_SCORE_REQUIREMENT(sWeaponRndInfo[sMenuInfo.catMenu.iCurElement].sChallenges[sMenuInfo.chalMenu.iCurElement], RRM_GOLD), FONT_CENTRE)	
	ELSE
		DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_BRONZE_GOAL_SCORE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH", TRUE, FALSE)	
		DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_SILVER_GOAL_SCORE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH", TRUE, FALSE)	
		DRAW_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGE_AWARDS_GOLD_GOAL_SCORE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH", TRUE, FALSE)	
	ENDIF
	SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
ENDPROC



