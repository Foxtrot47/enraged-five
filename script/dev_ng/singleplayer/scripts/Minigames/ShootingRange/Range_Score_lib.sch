// Range_Score_lib.sch
USING "Range_Score.sch"
USING "Range_Round.sch"
USING "Range_UI_lib.sch"
USING "CompletionPercentage_Public.sch"
USING "Range_Leaderboard_lib.sch"

PROC UPDATE_RANGE_MINIGUN_COMBO_TIMERS(Range_MultiplierData & sMPData)
	INT iTime = ROUND( 1000 * GET_FRAME_TIME() )
	sMPData.iMinigunFrameTimer -= iTime
	sMPData.iMinigunFrameTimer = CLAMP_INT(sMPData.iMinigunFrameTimer, 0, MAX_MINIGUN_COMBO_FRAMES)
	sMPData.iMinigunBlockTimer -= iTime
	sMPData.iMinigunBlockTimer = CLAMP_INT(sMPData.iMinigunBlockTimer, 0, MAX_MINIGUN_BLOCK_FRAMES)
	//CDEBUG2LN(DEBUG_SHOOTRANGE, "UPDATE_RANGE_MINIGUN_COMBO_TIMER: Post Clamp sMPData.iMinigunFrameTimer -> ", sMPData.iMinigunFrameTimer)
ENDPROC

/// PURPOSE:
///    Scores a hit for the player, increments his score, then adds it to the queue.
PROC SCORE_ZONE_HIT(INT iNumHits, INT iHitZone, Range_RoundInfo & sRndInfo, Range_SPTData & sSPTInfo)
	INT iTer
	IF iNumHits = 0 OR iHitZone = -1
		EXIT
	ENDIF
	
	INT iTempScore
	
	IF (sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_BULLSEYE)
		REPEAT iNumHits iTer
			// Do special logic per zone
			SWITCH iHitZone
				CASE 1					
					iTempScore = CONST_ZONE_1_REWARD * sRndInfo.sMPData.iCurrMP
					sRndInfo.sScoreData.iP1Score += iTempScore
					
					// Scroll the text
					ADD_SCORE_TO_SPT(iTempScore, HUD_COLOUR_YELLOW, sSPTInfo)
					RANGE_INCREMENT_ZONE_HITS(sRndInfo, 1)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "ScoreZoneHit -- Zone 1 Hit!")
				BREAK
				
				CASE 2
					iTempScore = CONST_ZONE_2_REWARD * sRndInfo.sMPData.iCurrMP
					sRndInfo.sScoreData.iP1Score += iTempScore
					
					// Scroll the text
					ADD_SCORE_TO_SPT(iTempScore, HUD_COLOUR_RED, sSPTInfo)
					RANGE_INCREMENT_ZONE_HITS(sRndInfo, 2)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "ScoreZoneHit -- Zone 2  Hit!")
				BREAK
				
				CASE 3
					iTempScore = CONST_ZONE_3_REWARD * sRndInfo.sMPData.iCurrMP
					sRndInfo.sScoreData.iP1Score += iTempScore
					
					// Scroll the text
					ADD_SCORE_TO_SPT(iTempScore, HUD_COLOUR_BLUE, sSPTInfo)
					RANGE_INCREMENT_ZONE_HITS(sRndInfo, 3)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "ScoreZoneHit -- Zone 3 Hit!")
				BREAK
				
				CASE 4
					iTempScore = CONST_ZONE_4_REWARD * sRndInfo.sMPData.iCurrMP
					sRndInfo.sScoreData.iP1Score += iTempScore
					
					// Scroll the text
					ADD_SCORE_TO_SPT(iTempScore, HUD_COLOUR_GREY, sSPTInfo)
					RANGE_INCREMENT_ZONE_HITS(sRndInfo, 4)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "ScoreZoneHit -- Zone 4 Hit!")
				BREAK
			ENDSWITCH	
		ENDREPEAT
	ELIF (sRndInfo.sScoreData.eScoreStyle = ROUNDSCORE_MINIGUN)
		HUD_COLOURS eHudColor
		// Calculate score from multiplier
		iTempScore = CONST_ZONE_1_REWARD * sRndInfo.sMPData.iCurrMP
		sRndInfo.sScoreData.iP1Score += iTempScore
		
		// Determine zone hit and color of text from multiplier
		IF sRndInfo.sMPData.iCurrMP = 1
			eHudColor = HUD_COLOUR_GREY
			RANGE_INCREMENT_ZONE_HITS(sRndInfo, 3)
		ELIF sRndInfo.sMPData.iCurrMP = 2
			eHudColor = HUD_COLOUR_BLUE
			RANGE_INCREMENT_ZONE_HITS(sRndInfo, 2)
		ELIF sRndInfo.sMPData.iCurrMP = 3
			eHudColor = HUD_COLOUR_YELLOW
			RANGE_INCREMENT_ZONE_HITS(sRndInfo, 1)
		ELSE
			eHudColor = HUD_COLOUR_GREY
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Default HUD color hit in ROUNDSCORE_MINIGUN, contact Rob Pearsall")
		ENDIF
		
		// Scroll the text
		ADD_SCORE_TO_SPT(iTempScore, eHudColor, sSPTInfo)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "ScoreZoneHit!")
	ENDIF
ENDPROC

/// PURPOSE:
///    Dropped this in a function instead of muddy up the target handler.
///    Takes the grid loc that was hit, and processes the score for it.
PROC PROCESS_SCORE_REVDEST(GRID_INDEX shotIndex, Range_RoundInfo & sRndInfo, Range_SPTData & sSPTInfo)
	INT iScoreToGive = 0
	HUD_COLOURS printColor = HUD_COLOUR_GREY
	
	SWITCH (shotIndex)
		CASE gridA1
		CASE gridA2
		CASE gridA3
		CASE gridA4
		CASE gridA5
			RANGE_INCREMENT_ZONE_HITS(sRndInfo, 1)
			iScoreToGive = 100
			printColor = HUD_COLOUR_YELLOW
		BREAK
		
		CASE gridB1
		CASE gridB2
		CASE gridB3
		CASE gridB4
		CASE gridB5
			RANGE_INCREMENT_ZONE_HITS(sRndInfo, 2)
			iScoreToGive = 50
			printColor = HUD_COLOUR_RED
		BREAK
		
		CASE gridC1
		CASE gridC2
		CASE gridC3
		CASE gridC4
		CASE gridC5
			RANGE_INCREMENT_ZONE_HITS(sRndInfo, 3)
			iScoreToGive = 25
			printColor = HUD_COLOUR_BLUE
		BREAK
		
		CASE gridD1
		CASE gridD2
		CASE gridD3
		CASE gridD4
		CASE gridD5
			IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sRangeData[ePlayerChar].iBools, SRB_PrintedMinScore_ReverseRange)
				SET_BITMASK_AS_ENUM(g_savedGlobals.sRangeData[ePlayerChar].iBools, SRB_PrintedMinScore_ReverseRange)
				
				// First time we ever got a low-score hit, print help to the player.
				PRINT_HELP("SHR_DEST_LOWHIT")
			ENDIF
			RANGE_INCREMENT_ZONE_HITS(sRndInfo, 4)
			iScoreToGive = 10
			printColor = HUD_COLOUR_GREY
		BREAK
	ENDSWITCH
	
	// Add that score for the player
	IF (iScoreToGive <> 0)
		sRndInfo.sScoreData.iP1Score += iScoreToGive
		ADD_SCORE_TO_SPT(iScoreToGive, printColor, sSPTInfo)
	ENDIF
ENDPROC


/// PURPOSE:
///    Safe increment for the multiplier. Safer than what? Not wearing multiplier incrementation protection, of course!
PROC INCREMENT_MULTIPLIER(Range_MultiplierData & sMPData)
	// Increment our blocks
	IF (sMPData.iMPBlocks < iMULTIPLIER_BLOCKS)
		// Increase the number of blocks we have towards the next multiplier.
		sMPData.iMPBlocks++
	ELIF (sMPData.iMPBlocks >= iMULTIPLIER_BLOCKS)
		IF (sMPData.iCurrMP < CONST_MAX_MULTIP)
			// Play a sound here.
			CDEBUG2LN(DEBUG_SHOOTRANGE, "PLAYING THE MULTIPLIER INCREASE HERE!!!")
			PLAY_RANGE_SOUND("MULTIPLIER_INCREASE")
			
			// Increase our multiplier, and reset the number of blocks we have.
			sMPData.iCurrMP++
			sMPData.iMPBlocks = 0
		ENDIF
	ENDIF

	CDEBUG2LN(DEBUG_SHOOTRANGE, "IncrementMultiplier -- Multiplier: ", sMPData.iCurrMP, " Blocks: ", sMPData.iMPBlocks)
	
	// Is this our new highest mp?
	IF (sMPData.iCurrMP > sMPData.iHighestMP)
		sMPData.iHighestMP = sMPData.iCurrMP
	ENDIF
ENDPROC


/// PURPOSE:
///    Checks to see if we actually need to reset the multipler for a missed shot.
/// RETURNS:
///    TRUE if we should reset the multipler.
FUNC BOOL NEED_RESET_MULTIPLIER(Range_RoundInfo & sRndInfo)
	// If we're a pistol or shotgun, a missed shot always resets.
	IF (sRndInfo.eWeaponCat = WEAPCAT_PISTOL) OR (sRndInfo.eWeaponCat = WEAPCAT_SHOTGUN)
		DEBUG_MESSAGE("Weapon was a pistol or shotty... resetting...")
		RETURN TRUE
	ENDIF
		
	// So here, it's assumed we're a rifle or SMG.
	// If we haven't already consumed our grace period, do so.
	IF (NOT sRndInfo.sGraceInfo.bGraceUsed)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Never used a grace! GRACE START!")
		
		// Awesome. This was a free shot.
		sRndInfo.sGraceInfo.bGraceUsed = TRUE
		RESTART_TIMER_NOW(sRndInfo.sGraceInfo.tGraceResetTimer)
		RETURN FALSE
	ELSE
		// Shit, grace period was used... we first need to check our timer, see if we're still in a grace period...
		// Update the timer. If it's past a certain point, we're allowed to un-set our grace used.
		IF (GET_TIMER_IN_SECONDS(sRndInfo.sGraceInfo.tGraceResetTimer) > CONST_GraceResetTime)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Timer has eclipsed! GRACE START!")
			
			// Okay, we're actually fine. Just need to reset that timer, and set us to have used the grace, and report that we're okay.
			sRndInfo.sGraceInfo.bGraceUsed = TRUE
			RESTART_TIMER_NOW(sRndInfo.sGraceInfo.tGraceResetTimer)
			RETURN FALSE
		ELSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Timer hasn't eclipsed... resetting...")
			
			// Nope, still in grace. Due for an MP reset. Also reset the timer.
			RESTART_TIMER_NOW(sRndInfo.sGraceInfo.tGraceResetTimer)
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Checks our score against the high score table, and let's us know if we have the highest score.
///    If we do, it will assign the new high score as well as set a flag letting the sciprt know 
///    its happened
/// RETURNS:
///    TRUE if we broke the high score
FUNC BOOL CHECK_OUR_SCORE_AGAINST_HIGH_SCORE(Range_ScoreData & sScrData, Range_RoundInfo & sRndInfo)

	// Store this in our last score, no matter what....
	g_savedGlobals.sRangeData[ePlayerChar].sRounds[sRndInfo.eRoundType].iLastScore = sScrData.iP1Score
	
	// Changing how this works. Save the high score no matter what.
	INT currHighScore = GET_RANGE_SP_HIGH_SCORE(ePlayerChar, sRndInfo.eRoundType)
	
	// Output our high score..
	CDEBUG2LN(DEBUG_SHOOTRANGE, "CheckHighScore -- Current High Score: ", currHighScore)
	
	INT iTempHits = sRndInfo.iZ1Hits + sRndInfo.iZ2Hits + sRndInfo.iZ3Hits + sRndInfo.iZ4Hits
	FLOAT tempAcc = TO_FLOAT(iTempHits)
	FLOAT tempShots = TO_FLOAT(sRndInfo.iTotalShots)
	tempAcc = (tempAcc / tempShots) * 100.0
	
	// Cap accuracy at 100% -- #401261
	IF (tempAcc > 100.0)
		tempAcc = 100.0
	ENDIF
	
	//Set accuracy no matter what.
	g_savedGlobals.sRangeData[ePlayerChar].sRounds[sRndInfo.eRoundType].fAccuracy = tempAcc
	
	// Check if we beat it
	IF (sScrData.iP1Score > currHighScore)

		// Store high score, weapon used, shots fired, shots hit, and accuracy.
		SET_RANGE_SP_HIGH_SCORE(ePlayerChar, sRndInfo.eRoundType, sScrData.iP1Score)
		g_savedGlobals.sRangeData[ePlayerChar].sRounds[sRndInfo.eRoundType].eWeaponType = sRndInfo.eWeaponType
		g_savedGlobals.sRangeData[ePlayerChar].sRounds[sRndInfo.eRoundType].iShotsFired = sRndInfo.iTotalShots
		g_savedGlobals.sRangeData[ePlayerChar].sRounds[sRndInfo.eRoundType].iShotsHit = iTempHits
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "CheckHighScore -- NEW!!! High Score: ", GET_RANGE_SP_HIGH_SCORE(ePlayerChar, sRndInfo.eRoundType))
		CDEBUG2LN(DEBUG_SHOOTRANGE, "For round type: ", ENUM_TO_INT(sRndInfo.eRoundType))

		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_RANGE_SCORE_AWARDS()
	// award a percentage here
	HANDLE_RANGE_ROUND_COMPLETION_PERCENTAGES()
	// figure out shop discount here
	IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sRangeData[ePlayerChar].iBools, SRB_ShownFloatingHelp_ShopDiscountGold)
		IF ARE_ALL_RANGE_ROUNDS_GOLDED()
			CPRINTLN( DEBUG_SHOOTRANGE, "Discount! :: IF ARE_ALL_RANGE_ROUNDS_GOLDED() returned TRUE")
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
			
			PRINT_HELP("SHR_DISCO_G")
			SET_BITMASK_AS_ENUM(g_savedGlobals.sRangeData[ePlayerChar].iBools, SRB_ShownFloatingHelp_ShopDiscountGold)
			SET_BITMASK_AS_ENUM(g_savedGlobals.sRangeData[ePlayerChar].iBools, SRB_ShownFloatingHelp_ShopDiscountSilver)
			SET_BITMASK_AS_ENUM(g_savedGlobals.sRangeData[ePlayerChar].iBools, SRB_ShownFloatingHelp_ShopDiscountBronze)
		ELIF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sRangeData[ePlayerChar].iBools, SRB_ShownFloatingHelp_ShopDiscountSilver)
			IF ARE_ALL_RANGE_ROUNDS_SILVERED()
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				CPRINTLN( DEBUG_SHOOTRANGE, "Discount! :: IF ARE_ALL_RANGE_ROUNDS_SILVERED() returned TRUE")
				PRINT_HELP("SHR_DISCO_S")
				SET_BITMASK_AS_ENUM(g_savedGlobals.sRangeData[ePlayerChar].iBools, SRB_ShownFloatingHelp_ShopDiscountSilver)
				SET_BITMASK_AS_ENUM(g_savedGlobals.sRangeData[ePlayerChar].iBools, SRB_ShownFloatingHelp_ShopDiscountBronze)
			ELIF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sRangeData[ePlayerChar].iBools, SRB_ShownFloatingHelp_ShopDiscountBronze)
				IF ARE_ALL_RANGE_ROUNDS_BRONZED()
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					CPRINTLN( DEBUG_SHOOTRANGE, "Discount! :: IF ARE_ALL_RANGE_ROUNDS_BRONZED() returned TRUE")
					PRINT_HELP("SHR_DISCO_B")
					SET_BITMASK_AS_ENUM(g_savedGlobals.sRangeData[ePlayerChar].iBools, SRB_ShownFloatingHelp_ShopDiscountBronze)
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

