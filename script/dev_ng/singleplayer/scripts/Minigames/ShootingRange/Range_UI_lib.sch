// Range_UI_lib.sch
USING "Range_UI.sch"
USING "Range_Target.sch"
USING "Range_Support_lib.sch"
USING "Range_Medal_lib.sch"
//USING "Range_Round_lib.sch"
USING "shared_hud_displays.sch"
USING "range_menu_lib.sch"
USING "UIUtil.sch"
USING "menu_cursor_public.sch"

CONST_FLOAT fSPTTypeScale 0.73333 // 22px/30px (desired/default)
CONST_FLOAT fSPTLineSpacing 0.03472 // 25px / 720px

FORWARD ENUM RANGE_STATE
PROC HIDE_UI_DURING_RANGE(RANGE_STATE eRangeState)
	IF eRangeState != SRM_SHOOT
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
	ENDIF
	
	HIDE_ALL_BOTTOM_RIGHT_HUD()
ENDPROC


/// PURPOSE:
///    Triggers the flash transition effect, and the screenblur for rnge applications.
PROC RANGE_UI_TRIGGER_TRANSITION(Range_MenuData & sMenuInfo, BOOL bActivate)
	// Don't process this transition while active.
	IF (bActivate AND sMenuInfo.bTransitionActive)
		EXIT
	ELIF (NOT bActivate AND NOT sMenuInfo.bTransitionActive)
		EXIT
	ENDIF
	
	// Effect is different for each player.
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	TEXT_LABEL_31 txtTransition = "MenuMG"
	IF (ePlayer = CHAR_TREVOR)
		txtTransition += "Trevor"
	ELIF (ePlayer = CHAR_MICHAEL)
		txtTransition += "Michael"
	ELIF (ePlayer = CHAR_FRANKLIN)
		txtTransition += "Franklin"
	ENDIF
	
	IF bActivate
		// Turn the effect on.
		txtTransition += "In"
		//TRIGGER_SCREENBLUR_FADE_IN(250)
		ANIMPOSTFX_PLAY(txtTransition, 0, TRUE)
	ELSE
		// Turn the effect off.
		TEXT_LABEL_31 txtTransIn = txtTransition
		txtTransIn += "In"
		txtTransition += "Out"
		ANIMPOSTFX_STOP(txtTransIn)
		ANIMPOSTFX_PLAY(txtTransition, 0, TRUE)
		//TRIGGER_SCREENBLUR_FADE_OUT(250)
	ENDIF
	
	sMenuInfo.bTransitionActive = bActivate
ENDPROC

PROC SETUP_RANGE_MENU_CONTROLS(SIMPLE_USE_CONTEXT &useContext, BOOL bUseQuitButton, BOOL bHasSelectOption = TRUE, BOOL bHasLeaderboard = FALSE)
CDEBUG2LN(DEBUG_SHOOTRANGE, "SETUP_RANGE_MENU_CONTROLS called with bUseQuitButton=", PICK_STRING(bUseQuitButton, "TRUE", "FALSE"))
	IF bHasSelectOption
		INIT_SIMPLE_USE_CONTEXT(useContext, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(useContext, "FE_HLP4",											FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ADD_SIMPLE_USE_CONTEXT_INPUT(useContext, PICK_STRING(bUseQuitButton, "IB_QUIT", "FE_HLP3"), FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	ELSE
		INIT_SIMPLE_USE_CONTEXT(useContext, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(useContext, PICK_STRING(bUseQuitButton, "IB_QUIT", "FE_HLP3"), FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	ENDIF
	IF bHasLeaderboard
		ADD_SIMPLE_USE_CONTEXT_INPUT(useContext, "SCLB",											FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
	ENDIF
	SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(useContext, TRUE)
	SET_SIMPLE_USE_CONTEXT_FULLSCREEN(useContext, TRUE)
ENDPROC

/// PURPOSE:
///    Set up the splash message for display after a round
///    With the change to use Shards the colors are now all white.
/// PARAMS:
///    siStruct - 
///    sMsg - The Title Message
///    sWeapon - The weapon used, passed into SHR_CHALL_NAME
///    iChal - The number of the challenge, passed into SHR_CHALL_NAME
///    eEndFlash - The color to exit with.
PROC SET_RANGE_BIG_MESSAGE(SCRIPT_SHARD_BIG_MESSAGE &siStruct, STRING sMsg, STRING sWeapon, INT iChal, HUD_COLOURS eEndFlash = HUD_COLOUR_WHITE)
	STRING sMethodName = "SHOW_SHARD_CENTERED_MP_MESSAGE"
	siStruct.eEndFlash = HUD_COLOUR_BLACK
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siStruct.siMovie, "RESET_MOVIE")
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siStruct.siMovie, sMethodName)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eEndFlash)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sMsg)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SHR_CHALL_NAME")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sWeapon)
		ADD_TEXT_COMPONENT_INTEGER(iChal)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
	END_SCALEFORM_MOVIE_METHOD()
	
	// Store the duration.
	siStruct.iDuration = 5000
ENDPROC

FUNC BOOL UPDATE_RANGE_SPLASH(Range_MenuData & sMenuInfo)
	// Maybe need to flash
	// Trigger the screen effect.
	IF NOT sMenuInfo.bFlashEffect
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
	        CASE CHAR_MICHAEL		ANIMPOSTFX_PLAY("SuccessMichael", 500, FALSE)		BREAK
	        CASE CHAR_FRANKLIN		ANIMPOSTFX_PLAY("SuccessFranklin", 500, FALSE)		BREAK
	        CASE CHAR_TREVOR		ANIMPOSTFX_PLAY("SuccessTrevor", 500, FALSE)		BREAK
	    ENDSWITCH
		sMenuInfo.bFlashEffect = TRUE
	ENDIF
	
	// Make sure our timer is active.
	FLOAT fTimerVal = 0.0
	IF NOT IS_TIMER_STARTED(sMenuInfo.uiSplashText.movieTimer)
		RESTART_TIMER_NOW(sMenuInfo.uiSplashText.movieTimer)
	ELSE
		fTimerVal = GET_TIMER_IN_SECONDS(sMenuInfo.uiSplashText.movieTimer)
	ENDIF
	
	// Do the transition up here too.
//	IF NOT sMenuInfo.bTransitionUp
//		IF fTimerVal > 1.0
//			CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_RANGE_SPLASH - Transition up: ", fTimerVal)
//			BEGIN_SCALEFORM_MOVIE_METHOD(sMenuInfo.uiSplashText.siMovie, "TRANSITION_UP")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(MG_BIG_MESSAGE_ANIM_TIME_SLOW)
//			END_SCALEFORM_MOVIE_METHOD()
//			sMenuInfo.bTransitionUp = TRUE
//		ENDIF
//	ENDIF
	
	// Draw victory message. When it's done, move on.
	IF NOT sMenuInfo.bTransitionOut
		IF fTimerVal > 4.25
			CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_RANGE_SPLASH - Transition out: ", fTimerVal)
			END_SHARD_BIG_MESSAGE(sMenuInfo.uiSplashText)
			sMenuInfo.bTransitionOut = TRUE
		ENDIF
	ENDIF
	
	// Draw.
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(sMenuInfo.uiSplashText.siMovie, 255, 255, 255, 255)
	
	// Are we done?
	IF fTimerVal > 5.0
		CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_RANGE_SPLASH - All done: ", fTimerVal)
		CANCEL_TIMER(sMenuInfo.uiSplashText.movieTimer)
		sMenuInfo.bTransitionUp = FALSE
		sMenuInfo.bTransitionOut = FALSE
		sMenuInfo.bFlashEffect = FALSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets up all the basics for the queue.. 
PROC SETUP_SPT_QUEUE(SPT_OBJECT & sSPTData[])
	INT iIter
	REPEAT MAX_NUM_SPT iIter
		sSPTData[iIter].fPosX = 0.5 + 0.00234 // middle of screen plus small offset for centering
		sSPTData[iIter].fPosY = PIXEL_Y_TO_FLOAT(300)
		sSPTData[iIter].iAlpha = 255
		sSPTData[iIter].iPtValue = 50
		sSPTData[iIter].fFrameTime = -1
	ENDREPEAT	
ENDPROC

PROC SORT_SPT (SPT_OBJECT & sSPTData[])
	INT iI, iJ
	SPT_OBJECT sTemp
	
//CDEBUG2LN(DEBUG_SHOOTRANGE, "PRE SORT")
//	REPEAT MAX_NUM_SPT iI
//	CDEBUG2LN(DEBUG_SHOOTRANGE, iI," : ", sSPTData[iI].fFrameTime)
//	ENDREPEAT
	
	iI = 1
	WHILE  iI < MAX_NUM_SPT
		sTemp = sSPTData[iI]
		
		// To change Sort ordering
		// > for Big to Small
		// < for Small to Big
		iJ = iI
		WHILE iJ > 0 AND sTemp.fFrameTime < sSPTData[iJ-1].fFrameTime
			sSPTData[iJ] = sSPTData[iJ-1]
			iJ--
		ENDWHILE
		
		sSPTData[iJ] = sTemp
		
		iI++
	ENDWHILE
		
//CDEBUG2LN(DEBUG_SHOOTRANGE, "POST SORT")
//	REPEAT MAX_NUM_SPT iI
//	CDEBUG2LN(DEBUG_SHOOTRANGE, iI," : ", sSPTData[iI].fFrameTime)
//	ENDREPEAT
ENDPROC

FUNC INT GET_NEXT_SPT (SPT_OBJECT & sSPTData[])
	INT iI, iActiveCount
	
	REPEAT MAX_NUM_SPT iI
		IF sSPTData[iI].bActive
			iActiveCount++
		ENDIF
	ENDREPEAT
	
	// None active, return 0
	IF iActiveCount = 0
		RETURN 0
	ENDIF
	
	SORT_SPT(sSPTData)
		
	REPEAT MAX_NUM_SPT iI
		IF NOT sSPTData[iI].bActive
			RETURN iI
		ENDIF
	ENDREPEAT
	
	RETURN MAX_NUM_SPT - 1
ENDFUNC

/// PURPOSE:
///    Tells our SPT queue to start scrolling
PROC ADD_SCORE_TO_SPT(INT iValue, HUD_COLOURS txtColor, Range_SPTData & sSPTInfo)
	sSPTInfo.iActiveSPT = GET_NEXT_SPT(sSPTInfo.sSPTQueue)
//CDEBUG2LN(DEBUG_SHOOTRANGE, "GET_NEXT_SPT: ", sSPTInfo.iActiveSPT)
	
	// Create the new object:
	sSPTInfo.sSptQueue[sSPTInfo.iActiveSPT].fPosY = PIXEL_Y_TO_FLOAT(300)
	sSPTInfo.sSptQueue[sSPTInfo.iActiveSPT].iAlpha = 255
	sSPTInfo.sSptQueue[sSPTInfo.iActiveSPT].iPtValue = iValue
	sSPTInfo.sSptQueue[sSPTInfo.iActiveSPT].bActive = TRUE
	sSPTInfo.sSptQueue[sSPTInfo.iActiveSPT].fFrameTime = 0
	sSPTInfo.sSptQueue[sSPTInfo.iActiveSPT].color = txtColor
	
	// We've added, run a quick sort.
	SORT_SPT(sSPTInfo.sSptQueue)
ENDPROC


/// PURPOSE:
///    Scrolls one specific SPT object.
/// RETURNS:
///    TRUE when done.
PROC SPT_SCROLL(SPT_OBJECT & sSPTObj)
	FLOAT fStart,fEnd
	FLOAT fInterpTime
	
	FLOAT fAlphaStartFadeOut = 0.5
	
	// Increment our frame count
	sSPTObj.fFrameTime += GET_FRAME_TIME()
	
	// Find out how much we should be flipping
	fInterpTime = sSPTObj.fFrameTime / CONST_SPT_LENGTH

	
	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		// Position
		fStart = PIXEL_Y_TO_FLOAT(300)
		fEnd = PIXEL_Y_TO_FLOAT(260)
		
		// Linear
//		sSPTObj.fPosY = (fEnd-fStart)*(fInterpTime)+fStart
		
		// Quad OUT
		sSPTObj.fPosY = -(fEnd-fStart)*(fInterpTime/CONST_SPT_LENGTH)*(fInterpTime-2)+fStart
		
		// Quad IN
//		sSPTObj.fPosY = (fEnd-fStart)*(fInterpTime/CONST_SPT_LENGTH)*fInterpTime+fStart
		
		// Alpha
		IF fInterpTime < fAlphaStartFadeOut
			sSPTObj.iAlpha = 255
		ELSE
			sSPTObj.iAlpha = 255 - ROUND((fInterpTime - fAlphaStartFadeOut)/fAlphaStartFadeOut * 255)
		ENDIF
		
		sSPTObj.bActive = TRUE
	ELSE			
		// Dissapear!
		sSPTObj.iAlpha = 0
		sSPTObj.bActive = FALSE
		sSPTObj.fFrameTime = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates our Scrolling Point Text.
PROC UPDATE_SPT(SPT_OBJECT & sSPTData[])
	INT iI, iActiveCount
	BOOL bNeedSort = FALSE
		
	// Scroll the text up after its been displayed
	REPEAT MAX_NUM_SPT iI
		// Is this active
		IF sSPTData[iI].bActive
			SPT_SCROLL(sSPTData[iI])
			iActiveCount++
			
			// Sort SPTs when SPT becomes inactive
			IF NOT sSPTData[iI].bActive
				bNeedSort = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iActiveCount > 0
		IF bNeedSort
			SORT_SPT(sSPTData)
		ENDIF
		
		// Reposition based on latest entry
		FOR iI = 0 TO MAX_NUM_SPT - 2
			IF sSPTData[iI].bActive AND sSPTData[iI + 1].bActive AND sSPTData[iI+1].fPosY > sSPTData[iI].fPosY - fSPTLineSpacing
				sSPTData[iI+1].fPosY = sSPTData[iI].fPosY - fSPTLineSpacing
			ENDIF
		ENDFOR
	ELSE
		EXIT		
	ENDIF
ENDPROC


/// PURPOSE:
///    Adds an to a menu.
PROC SETUP_MENU_ELEMENT(Range_CoreData & sCoreInfo, RANGE_MENU_INDEX eMenu, BOOL bActive, Range_MenuData & sMenuInfo, 
		STRING optLabel = NULL)
	DEBUG_MESSAGE("Adding a menu element with label: ", optLabel)
	
	IF (eMenu = RANGE_MENU_WEAPCAT)
		sMenuInfo.catMenu.menuElement[sMenuInfo.catMenu.iNumElements].bActive = bActive OR CHEAT_ARE_ALL_ROUNDS_UNLOCKED(sCoreInfo)
		sMenuInfo.catMenu.iNumElements += 1
	
	ELIF (eMenu = RANGE_MENU_WEAPONS)
		sMenuInfo.weapMenu.menuElement[sMenuInfo.weapMenu.iNumElements].bActive = bActive OR CHEAT_ARE_ALL_ROUNDS_UNLOCKED(sCoreInfo)
		sMenuInfo.weapMenu.iNumElements += 1
		
	ELIF (eMenu = RANGE_MENU_CHALLENGES)
		sMenuInfo.chalMenu.menuElement[sMenuInfo.chalMenu.iNumElements].bActive = bActive OR CHEAT_ARE_ALL_ROUNDS_UNLOCKED(sCoreInfo)
		sMenuInfo.chalMenu.iNumElements += 1
	ENDIF
	
	DEBUG_MESSAGE("Element added!")
ENDPROC


/// PURPOSE:
///    Lock/unlock items on the category menu based on 
PROC INIT_CATEGORY_MENU(Range_CoreData & sCoreInfo, Range_MenuData & sMenuInfo)	
	// If we cheat, everything is open. If not, then check it.
	IF CHEAT_ARE_ALL_ROUNDS_UNLOCKED(sCoreInfo)
		sMenuInfo.catMenu.menuElement[WEAPCAT_PISTOL].bActive = TRUE
		sMenuInfo.catMenu.menuElement[WEAPCAT_SMG].bActive = TRUE
		sMenuInfo.catMenu.menuElement[WEAPCAT_AR].bActive = TRUE
		sMenuInfo.catMenu.menuElement[WEAPCAT_SHOTGUN].bActive = TRUE
		sMenuInfo.catMenu.menuElement[WEAPCAT_LIGHT_MG].bActive = TRUE
		sMenuInfo.catMenu.menuElement[WEAPCAT_MINIGUN].bActive = TRUE
		//NG Current gen owners only
		IF sCoreInfo.bCGtoNG
			sMenuInfo.catMenu.menuElement[WEAPCAT_RAILGUN].bActive = TRUE
		ELSE
			sMenuInfo.catMenu.menuElement[WEAPCAT_RAILGUN].bActive = FALSE
		ENDIF
	ELSE
		// Pistol cat always open.
		sMenuInfo.catMenu.menuElement[WEAPCAT_PISTOL].bActive = TRUE
		
		// SMG cat open if a weapon is.
		sMenuInfo.catMenu.menuElement[WEAPCAT_SMG].bActive = IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SMG, RANGE_MICROSMG) OR 
																IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SMG, RANGE_SMG) OR
																IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SMG, RANGE_DLC_ASSAULTSMG)	
		// AR cat open if a weapon is.
		sMenuInfo.catMenu.menuElement[WEAPCAT_AR].bActive = IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_AR, RANGE_ASSAULTRIFLE) OR 
																IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_AR, RANGE_CARBINERIFLE) OR
																IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_AR, RANGE_DLC_HEAVYRIFLE) OR
																IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_AR, RANGE_ADVANCEDRIFLE)
		// Shotgun cat open if a weapon is.
		sMenuInfo.catMenu.menuElement[WEAPCAT_SHOTGUN].bActive = IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SHOTGUN, RANGE_PUMPSHOTGUN) OR 
																IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SHOTGUN, RANGE_SAWNOFFSHOTGUN) OR
																IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SHOTGUN, RANGE_DLC_BULLPUPSHOTGUN) OR
																IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SHOTGUN, RANGE_ASSAULTSHOTGUN)	
		// Light MG cat open if a weapon is.
		sMenuInfo.catMenu.menuElement[WEAPCAT_LIGHT_MG].bActive = IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_LIGHT_MG, RANGE_MG) OR 
																IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_LIGHT_MG, RANGE_COMBATMG) OR
																IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_LIGHT_MG, RANGE_DLC_ASSAULTMG)	
		
		// Heavy cat open if all other rounds passed.
		BOOL bUnlockMini_Pistol = IS_ROUND_PASSED(RT_PISTOL_CHAL_1) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_2) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_3)
		BOOL bUnlockMini_SMG = IS_ROUND_PASSED(RT_SMG_CHAL_1) AND IS_ROUND_PASSED(RT_SMG_CHAL_2) AND IS_ROUND_PASSED(RT_SMG_CHAL_3)
		BOOL bUnlockMini_AR = IS_ROUND_PASSED(RT_AR_CHAL_1) AND IS_ROUND_PASSED(RT_AR_CHAL_2) AND IS_ROUND_PASSED(RT_AR_CHAL_3)
		BOOL bUnlockMini_Shot = IS_ROUND_PASSED(RT_SHOTGUN_CHAL_1) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_2) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_3)
		BOOL bUnlockMini_LMG = IS_ROUND_PASSED(RT_LMG_CHAL_1) AND IS_ROUND_PASSED(RT_LMG_CHAL_2) AND IS_ROUND_PASSED(RT_LMG_CHAL_3)
		sMenuInfo.catMenu.menuElement[WEAPCAT_MINIGUN].bActive = bUnlockMini_Pistol AND bUnlockMini_SMG AND bUnlockMini_AR AND bUnlockMini_Shot AND bUnlockMini_LMG
		
		//NG Current gen owners only
		IF sCoreInfo.bCGtoNG
			BOOL bUnlockRail_Pistol = TRUE//IS_ROUND_PASSED(RT_PISTOL_CHAL_1) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_2) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_3)
			BOOL bUnlockRail_SMG = TRUE//IS_ROUND_PASSED(RT_SMG_CHAL_1) AND IS_ROUND_PASSED(RT_SMG_CHAL_2) AND IS_ROUND_PASSED(RT_SMG_CHAL_3)
			BOOL bUnlockRail_AR = TRUE//IS_ROUND_PASSED(RT_AR_CHAL_1) AND IS_ROUND_PASSED(RT_AR_CHAL_2) AND IS_ROUND_PASSED(RT_AR_CHAL_3)
			BOOL bUnlockRail_Shot = TRUE//IS_ROUND_PASSED(RT_SHOTGUN_CHAL_1) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_2) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_3)
			BOOL bUnlockRail_LMG = TRUE//IS_ROUND_PASSED(RT_LMG_CHAL_1) AND IS_ROUND_PASSED(RT_LMG_CHAL_2) AND IS_ROUND_PASSED(RT_LMG_CHAL_3)
			BOOL bUnlockRail_HVY = TRUE//IS_ROUND_PASSED(RT_MINIGUN_CHAL_1) AND IS_ROUND_PASSED(RT_MINIGUN_CHAL_2) AND IS_ROUND_PASSED(RT_MINIGUN_CHAL_3)
			sMenuInfo.catMenu.menuElement[WEAPCAT_RAILGUN].bActive = bUnlockRail_Pistol AND bUnlockRail_SMG AND bUnlockRail_AR AND bUnlockRail_Shot AND bUnlockRail_LMG AND bUnlockRail_HVY
		ELSE
			sMenuInfo.catMenu.menuElement[WEAPCAT_RAILGUN].bActive = FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
			// Debug print out all of our unlocks:
		CDEBUG2LN(DEBUG_SHOOTRANGE, "** Pistol category: ACTIVE.")
			
			IF sMenuInfo.catMenu.menuElement[WEAPCAT_SMG].bActive
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** SMG category: ACTIVE.")
			ELSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** SMG category: INACTIVE.")
			ENDIF
			
			IF sMenuInfo.catMenu.menuElement[WEAPCAT_AR].bActive
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** AR category: ACTIVE.")
			ELSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** AR category: INACTIVE.")
			ENDIF
			
			IF sMenuInfo.catMenu.menuElement[WEAPCAT_SHOTGUN].bActive
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** Shotgun category: ACTIVE.")
			ELSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** Shotgun category: INACTIVE.")
			ENDIF
			
			IF sMenuInfo.catMenu.menuElement[WEAPCAT_LIGHT_MG].bActive
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** LMG category: ACTIVE.")
			ELSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** LMG category: INACTIVE.")
			ENDIF		
			
			IF sMenuInfo.catMenu.menuElement[WEAPCAT_MINIGUN].bActive
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** Heavy category: ACTIVE.")
			ELSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** Heavy category: INACTIVE.")
			ENDIF
			
			//NG Current gen owners only
			IF sMenuInfo.catMenu.menuElement[WEAPCAT_RAILGUN].bActive
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** Bonus category: ACTIVE.")
			ELSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "** Bonus category: INACTIVE.")
			ENDIF
			
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Setup the weapons menu as described by weaponCat
PROC INIT_WEAPONS_MENU(Range_CoreData & sCoreInfo, RANGE_WEAPON_CATEGORY weaponCat, Range_MenuData & sMenuInfo)	
	// Store previous weapon selection. We will be restoring that. -- #253796
	INT iStoredWeap = -100
	IF (sMenuInfo.weapMenu.iCurElement <> 0)
		iStoredWeap = sMenuInfo.weapMenu.iCurElement
	ENDIF
	
	// Wipe what was in the global data previously.
	Range_Weapons_Menu newMenu
	sMenuInfo.weapMenu = newMenu
	
	BOOL bCheatEntered = CHEAT_ARE_ALL_ROUNDS_UNLOCKED(sCoreInfo)
	
	IF (weaponCat = WEAPCAT_PISTOL)
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, TRUE, sMenuInfo)
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_PISTOL, RANGE_COMBATPISTOL), sMenuInfo)
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_PISTOL, RANGE_APPISTOL), sMenuInfo)
		IF sCoreInfo.bPistolDLC
			SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_PISTOL, RANGE_DLC_PISTOL50), sMenuInfo)
		ENDIF

	ELIF (weaponCat = WEAPCAT_SMG)
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SMG, RANGE_MICROSMG), sMenuInfo)
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SMG, RANGE_SMG), sMenuInfo)
		
		IF sCoreInfo.bSMGDLC
			SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SMG, RANGE_DLC_ASSAULTSMG), sMenuInfo)
		ENDIF

		
	ELIF (weaponCat = WEAPCAT_AR)
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_AR, RANGE_ASSAULTRIFLE), sMenuInfo) 	
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_AR, RANGE_CARBINERIFLE), sMenuInfo)
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_AR, RANGE_ADVANCEDRIFLE), sMenuInfo)
		
		IF sCoreInfo.bARDLC
			SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_AR, RANGE_DLC_HEAVYRIFLE), sMenuInfo)
		ENDIF

		
	ELIF (weaponCat = WEAPCAT_SHOTGUN)
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SHOTGUN, RANGE_PUMPSHOTGUN), sMenuInfo) 	
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SHOTGUN, RANGE_SAWNOFFSHOTGUN), sMenuInfo)
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SHOTGUN, RANGE_ASSAULTSHOTGUN), sMenuInfo)
		
		IF sCoreInfo.bShotgunDLC
			SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_SHOTGUN, RANGE_DLC_BULLPUPSHOTGUN), sMenuInfo)
		ENDIF

		
	ELIF (weaponCat = WEAPCAT_LIGHT_MG)
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_LIGHT_MG, RANGE_MG), sMenuInfo) 	
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_LIGHT_MG, RANGE_COMBATMG), sMenuInfo)
		
		IF sCoreInfo.bLMGDLC
			SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, bCheatEntered OR IS_RANGE_WEAPON_UNLOCKED(WEAPCAT_LIGHT_MG, RANGE_DLC_ASSAULTMG), sMenuInfo)
		ENDIF

	ELIF (weaponCat = WEAPCAT_MINIGUN)
		// Minigun is only unlocked if you have at least a bronze in everything.
		BOOL bUnlockMini_Pistol = IS_ROUND_PASSED(RT_PISTOL_CHAL_1) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_2) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_3)
		BOOL bUnlockMini_SMG = IS_ROUND_PASSED(RT_SMG_CHAL_1) AND IS_ROUND_PASSED(RT_SMG_CHAL_2) AND IS_ROUND_PASSED(RT_SMG_CHAL_3)
		BOOL bUnlockMini_AR = IS_ROUND_PASSED(RT_AR_CHAL_1) AND IS_ROUND_PASSED(RT_AR_CHAL_2) AND IS_ROUND_PASSED(RT_AR_CHAL_3)
		BOOL bUnlockMini_Shot = IS_ROUND_PASSED(RT_SHOTGUN_CHAL_1) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_2) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_3)
		BOOL bUnlockMini_LMG = IS_ROUND_PASSED(RT_LMG_CHAL_1) AND IS_ROUND_PASSED(RT_LMG_CHAL_2) AND IS_ROUND_PASSED(RT_LMG_CHAL_3)
		
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, 
			(bUnlockMini_Pistol AND bUnlockMini_SMG AND bUnlockMini_AR AND bUnlockMini_Shot AND bUnlockMini_LMG) OR bCheatEntered, 
			sMenuInfo)
	
	//NG Current gen owners only	
	ELIF (weaponCat = WEAPCAT_RAILGUN)
		IF sCoreInfo.bCGtoNG
			BOOL bUnlockRail_Pistol = TRUE// IS_ROUND_PASSED(RT_PISTOL_CHAL_1) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_2) AND IS_ROUND_PASSED(RT_PISTOL_CHAL_3)
			BOOL bUnlockRail_SMG = TRUE//IS_ROUND_PASSED(RT_SMG_CHAL_1) AND IS_ROUND_PASSED(RT_SMG_CHAL_2) AND IS_ROUND_PASSED(RT_SMG_CHAL_3)
			BOOL bUnlockRail_AR = TRUE//IS_ROUND_PASSED(RT_AR_CHAL_1) AND IS_ROUND_PASSED(RT_AR_CHAL_2) AND IS_ROUND_PASSED(RT_AR_CHAL_3)
			BOOL bUnlockRail_Shot = TRUE//IS_ROUND_PASSED(RT_SHOTGUN_CHAL_1) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_2) AND IS_ROUND_PASSED(RT_SHOTGUN_CHAL_3)
			BOOL bUnlockRail_LMG = TRUE//IS_ROUND_PASSED(RT_LMG_CHAL_1) AND IS_ROUND_PASSED(RT_LMG_CHAL_2) AND IS_ROUND_PASSED(RT_LMG_CHAL_3)
			BOOL bUnlockRail_HVY = TRUE//IS_ROUND_PASSED(RT_MINIGUN_CHAL_1) AND IS_ROUND_PASSED(RT_MINIGUN_CHAL_2) AND IS_ROUND_PASSED(RT_MINIGUN_CHAL_3)
			SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPONS, (bUnlockRail_Pistol AND bUnlockRail_SMG AND bUnlockRail_AR AND bUnlockRail_Shot AND bUnlockRail_LMG AND bUnlockRail_HVY) OR bCheatEntered, sMenuInfo)
		ENDIF
	ENDIF
	
	// Restore the last selected weapon. -- #253796
	IF (iStoredWeap <> -100)
		sMenuInfo.weapMenu.iCurElement = iStoredWeap
	ENDIF
ENDPROC


/// PURPOSE:
///    Setup the challenges menu as described by weapon category.
PROC INIT_CHALLENGES_MENU(Range_WeaponRounds & sWeaponRndInfo[], Range_CoreData & sCoreInfo, RANGE_WEAPON_CATEGORY weaponCat, Range_MenuData & sMenuInfo)
	// First element = always active.
	// Second element active only if we've passed te first. third element active only if we've passed the second.
	// Also, set our stored challenges to the global enum.
	sMenuInfo.chalMenu.menuElement[WEAPCHAL_1].bActive = sMenuInfo.catMenu.menuElement[weaponCat].bActive OR CHEAT_ARE_ALL_ROUNDS_UNLOCKED(sCoreInfo)
	sMenuInfo.chalMenu.menuElement[WEAPCHAL_2].bActive = IS_ROUND_CHALLENGE_REQ_MET(sWeaponRndInfo[weaponCat].sChallenges[WEAPCHAL_2].eUnlockReq) OR CHEAT_ARE_ALL_ROUNDS_UNLOCKED(sCoreInfo)
	sMenuInfo.chalMenu.menuElement[WEAPCHAL_3].bActive = IS_ROUND_CHALLENGE_REQ_MET(sWeaponRndInfo[weaponCat].sChallenges[WEAPCHAL_3].eUnlockReq) OR CHEAT_ARE_ALL_ROUNDS_UNLOCKED(sCoreInfo)
	
	IF INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement) = WEAPCAT_RAILGUN
	AND sCoreInfo.bCGtoNG
		sMenuInfo.chalMenu.menuElement[WEAPCHAL_4].bActive = IS_ROUND_CHALLENGE_REQ_MET(sWeaponRndInfo[weaponCat].sChallenges[WEAPCHAL_4].eUnlockReq) OR CHEAT_ARE_ALL_ROUNDS_UNLOCKED(sCoreInfo)
		sMenuInfo.chalMenu.iNumElements = 4
	ELSE
		sMenuInfo.chalMenu.menuElement[WEAPCHAL_4].bActive = FALSE
		sMenuInfo.chalMenu.iNumElements = 3
	ENDIF
	
	TEXT_LABEL_15 txtChallengDesc =  "SHR_"

	SWITCH weaponCat
		CASE WEAPCAT_PISTOL
			sMenuInfo.chalMenu.roundType[WEAPCHAL_1] = RT_PISTOL_CHAL_1
			sMenuInfo.chalMenu.roundType[WEAPCHAL_2] = RT_PISTOL_CHAL_2
			sMenuInfo.chalMenu.roundType[WEAPCHAL_3] = RT_PISTOL_CHAL_3
			txtChallengDesc += "PIS"
		BREAK
		CASE WEAPCAT_SMG
			sMenuInfo.chalMenu.roundType[WEAPCHAL_1] = RT_SMG_CHAL_1
			sMenuInfo.chalMenu.roundType[WEAPCHAL_2] = RT_SMG_CHAL_2
			sMenuInfo.chalMenu.roundType[WEAPCHAL_3] = RT_SMG_CHAL_3
			txtChallengDesc += "SMG"
		BREAK
		CASE WEAPCAT_AR
			sMenuInfo.chalMenu.roundType[WEAPCHAL_1] = RT_AR_CHAL_1
			sMenuInfo.chalMenu.roundType[WEAPCHAL_2] = RT_AR_CHAL_2
			sMenuInfo.chalMenu.roundType[WEAPCHAL_3] = RT_AR_CHAL_3
			txtChallengDesc += "AR"
		BREAK
		CASE WEAPCAT_SHOTGUN
			sMenuInfo.chalMenu.roundType[WEAPCHAL_1] = RT_SHOTGUN_CHAL_1
			sMenuInfo.chalMenu.roundType[WEAPCHAL_2] = RT_SHOTGUN_CHAL_2
			sMenuInfo.chalMenu.roundType[WEAPCHAL_3] = RT_SHOTGUN_CHAL_3
			txtChallengDesc += "SHO"
		BREAK
		CASE WEAPCAT_LIGHT_MG
			sMenuInfo.chalMenu.roundType[WEAPCHAL_1] = RT_LMG_CHAL_1
			sMenuInfo.chalMenu.roundType[WEAPCHAL_2] = RT_LMG_CHAL_2
			sMenuInfo.chalMenu.roundType[WEAPCHAL_3] = RT_LMG_CHAL_3
			txtChallengDesc += "LMG"
		BREAK
		CASE WEAPCAT_MINIGUN
			sMenuInfo.chalMenu.roundType[WEAPCHAL_1] = RT_MINIGUN_CHAL_1
			sMenuInfo.chalMenu.roundType[WEAPCHAL_2] = RT_MINIGUN_CHAL_2
			sMenuInfo.chalMenu.roundType[WEAPCHAL_3] = RT_MINIGUN_CHAL_3
			txtChallengDesc += "MG"
		BREAK	
		//NG Current gen owners only
		CASE WEAPCAT_RAILGUN
			sMenuInfo.chalMenu.roundType[WEAPCHAL_1] = RT_RAILGUN_CHAL_1
			sMenuInfo.chalMenu.roundType[WEAPCHAL_2] = RT_RAILGUN_CHAL_2
			sMenuInfo.chalMenu.roundType[WEAPCHAL_3] = RT_RAILGUN_CHAL_3
			sMenuInfo.chalMenu.roundType[WEAPCHAL_4] = RT_RAILGUN_CHAL_4
			txtChallengDesc += "RG"
		BREAK
	ENDSWITCH

	sMenuInfo.chalMenu.challengeDesc[WEAPCHAL_1] = txtChallengDesc
	sMenuInfo.chalMenu.challengeDesc[WEAPCHAL_2] = txtChallengDesc
	sMenuInfo.chalMenu.challengeDesc[WEAPCHAL_3] = txtChallengDesc
	sMenuInfo.chalMenu.challengeDesc[WEAPCHAL_4] = txtChallengDesc
	sMenuInfo.chalMenu.challengeDesc[WEAPCHAL_1] += "_C1_DESC"
	sMenuInfo.chalMenu.challengeDesc[WEAPCHAL_2] += "_C2_DESC"
	sMenuInfo.chalMenu.challengeDesc[WEAPCHAL_3] += "_C3_DESC"
	sMenuInfo.chalMenu.challengeDesc[WEAPCHAL_4] += "_C4_DESC"
	
ENDPROC

PROC INIT_RANGE_UI_LEADERBOARD_SF(SCALEFORM_INDEX &uiLeaderboard)
CDEBUG2LN(DEBUG_SHOOTRANGE, "INIT_RANGE_UI_LEADERBOARD_SF: Setting up headers")	
	SET_SC_LEADERBOARD_TITLE(uiLeaderboard, "SCLB_TITLE")
	SET_SC_LEADERBOARD_HEADER(uiLeaderboard, SECTION_WORLD, 	0, SLOT_LAYOUT_3_STATS, "", "", "", "", ICON_POINT, ICON_GUN, ICON_POSITION)
	SET_SC_LEADERBOARD_HEADER(uiLeaderboard, SECTION_FRIEND,	1, SLOT_LAYOUT_3_STATS, "", "", "", "", ICON_POINT, ICON_GUN, ICON_POSITION)
	SET_SC_LEADERBOARD_HEADER(uiLeaderboard, SECTION_CREW, 		2, SLOT_LAYOUT_3_STATS, "", "", "", "", ICON_POINT, ICON_GUN, ICON_POSITION)
ENDPROC


/// PURPOSE:
///    Sets up the menu elements for drawing and updating.
PROC INIT_MAIN_MENU(Range_CoreData & sCoreInfo, Range_MenuData &sMenuInfo, Range_WeaponRounds & sWeapRnds[])
	//Going to try and reserve some old selections
	RANGE_MENU_INDEX eTempMenu = sMenuInfo.curMenu
	
//	INIT_RANGE_UI_LEADERBOARD_SF(sMenuInfo.uiLeaderboard)

	INIT_RANGE_MENU(sMenuInfo.uiPlacement)
	
	// Add the weapon categories:
	SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPCAT, TRUE, sMenuInfo)
	SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPCAT, TRUE, sMenuInfo, "SHR_GEN_REQ")
	SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPCAT, TRUE, sMenuInfo, "SHR_GEN_REQ")
	SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPCAT, TRUE, sMenuInfo, "SHR_GEN_REQ")
	SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPCAT, TRUE, sMenuInfo, "SHR_GEN_REQ")
	SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPCAT, TRUE, sMenuInfo, "SHR_HVYMG_REQ")
//	//NG Current gen owners only
	IF sCoreInfo.bCGtoNG
		SETUP_MENU_ELEMENT(sCoreInfo, RANGE_MENU_WEAPCAT, TRUE, sMenuInfo, "SHR_HVYMG_REQ")
	ENDIF
	// Tell which weapons menu element we're on.
	sMenuInfo.catMenu.iCurElement = 0
	INIT_CHALLENGES_MENU(sWeapRnds, sCoreInfo, INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), sMenuInfo)
	
	// Tell which challenge menu element we're on.
	sMenuInfo.chalMenu.iCurElement = -1
	
	INIT_CATEGORY_MENU(sCoreInfo, sMenuInfo)
		
	// Okay, if we weren't on the category menu, need to rebuild some options here.
	SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, eTempMenu = RANGE_MENU_WEAPCAT, DEFAULT, TRUE)
ENDPROC

/// PURPOSE:
///    Returns the item value for the weapon selected by a cursor rather than a gamepad.
/// PARAMS:
///    iCursorValue - The menu cursor item currently selected
///    iCatValue - The weapon category currently selected.
///    iMaxValueInCat - The number of weapons in the category
/// RETURNS:
///    
FUNC INT GET_CURSOR_SELECTED_WEAPON_ITEM( INT iCursorValue, INT iCatValue, INT iMaxValueInCat )

	INT iMinValue = iCatValue + 1
	INT iMaxValue = iMinValue + iMaxValueInCat - 1
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "GET_CURSOR_SELECTED_WEAPON_ITEM Output")
	CDEBUG2LN(DEBUG_SHOOTRANGE, "iCursorValue = ", iCursorValue)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "iCatValue    = ", iCatValue)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "iMinValue    = ", iMinValue)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "iMaxValue    = ", iMaxValue)
	
	IF iCursorValue < iMinValue
	OR iCursorValue > iMaxValue
		CDEBUG2LN(DEBUG_SHOOTRANGE, "MENU_CURSOR_NO_ITEM")
		RETURN MENU_CURSOR_NO_ITEM
	ENDIF
	
	INT iReturnVal = iCursorValue - iMinValue
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "iReturnVal   = ", iReturnVal)
	
	RETURN iReturnVal

ENDFUNC


/// PURPOSE:
///    Returns a weapon category from the categories outside of the currently selected weapon menu.
/// PARAMS:
///    iCursorValue - The menu cursor item currently selected
///    iCatValue - The weapon category currently selected.
///    iNumWeapons - The number of weapons in the category
/// RETURNS:
///    MENU_CURSOR_NO_ITEM if the cursor is inside the current category, or the category index if outside
///    
FUNC INT GET_CURSOR_SELECTED_WEAPON_CATEGORY_FROM_WEAPON_MENU( INT iCursorValue, INT iCatValue, INT iNumWeapons )

	// Below the category item
	
	IF iCursorValue > MENU_CURSOR_NO_ITEM
	AND iCursorValue < iCatValue
		RETURN iCursorValue
	ENDIF
	
	IF iCursorValue > iCatValue + iNumWeapons
		RETURN iCursorValue - iNumWeapons
	ENDIF
	
	IF iCursorValue = iCatValue
		RETURN -2
	ENDIF

	RETURN MENU_CURSOR_NO_ITEM

ENDFUNC


/// PURPOSE:
///    Handles all input on the main menu. (RANGE_ProcessMenuInput)
/// RETURNS:
///    The Retval for when this is done. (NONE, if there's no update required, FALSE to quit, TRUE when done.)
FUNC THREE_STATE_RETVAL PROCESS_MENU_INPUT(Range_RoundInfo & sRndInfo, Range_WeaponRounds & sWeapRnds[], Range_CoreData & sCoreInfo, Range_MenuData & sMenuInfo)
	
	
	////////
	///     PC Mouse support
	///


	BOOL bCursorWeaponCatSelect = FALSE
	BOOL bCursorWeaponSelect = 	  FALSE
	BOOL bCursorChallengeAccept = FALSE
	BOOL bCursorBack = 			  FALSE
	
	INT iCursorWeaponMenuItem	 = 		MENU_CURSOR_NO_ITEM
	INT iCursorChallengeMenuItem = 		MENU_CURSOR_NO_ITEM
	INT iCursorWeaponSelection = 		MENU_CURSOR_NO_ITEM
	INT iCursorNewWeaponCategoryItem = 	MENU_CURSOR_NO_ITEM
		
	IF IS_PC_VERSION() AND IS_USING_CURSOR(FRONTEND_CONTROL)
		
		IF NOT IS_RANGE_MENU_FLAG_SET(sMenuInfo, RMF_SHOWING_SCLB)
		
			//PRINTLN("SP RANGE MOUSE - DEBUG 1")

			iCursorWeaponMenuItem = GET_CURSOR_HIGHLIGHTED_ITEM_IN_CUSTOM_MENU( 0.201, 0.409, 0.198, 0.0375, 0.034, iNumMenuElements, 32 ) // Main Menu
			
			IF sMenuInfo.curMenu = RANGE_MENU_CHALLENGES
				iCursorChallengeMenuItem = GET_CURSOR_HIGHLIGHTED_ITEM_IN_CUSTOM_MENU( 0.401, 0.408, 0.198, 0.111, 0.11, sMenuInfo.chalMenu.iNumElements, 32)  // Challenge Menu
				//PRINTLN("SP RANGE MOUSE - DEBUG 2")
			ENDIF
				
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			AND NOT IS_MOUSE_ROLLED_OVER_INSTRUCTIONAL_BUTTONS()
			AND NOT IS_RANGE_MENU_FLAG_SET(sMenuInfo, RMF_EXITED_SIGNIN)
			
				PRINTLN("SP RANGE MOUSE - DEBUG 3")
				
				//////////////////////////////////////////////////////////////////////////////////////
				// Weapon category menu
				//////////////////////////////////////////////////////////////////////////////////////
		
				IF  sMenuInfo.curMenu = RANGE_MENU_WEAPCAT
				
					PRINTLN("SP RANGE MOUSE - DEBUG 4")
								
					IF iCursorWeaponMenuItem > MENU_CURSOR_NO_ITEM
					
						PRINTLN("SP RANGE MOUSE - DEBUG 5")
								
						// Select new item
						IF iCursorWeaponMenuItem != sMenuInfo.catMenu.iCurElement
						
							PRINTLN("SP RANGE MOUSE - DEBUG 6")
				
							sMenuInfo.catMenu.iCurElement = iCursorWeaponMenuItem
						
							INIT_CHALLENGES_MENU(sWeapRnds, sCoreInfo, INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), sMenuInfo)
							CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU WEAPCAT CURSOR SELECT - CUR POS = ", sMenuInfo.catMenu.iCurElement)
						
							PLAY_RANGE_SOUND("NAV_UP_DOWN", TRUE)
						
							// Re-setup our controls.
							SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, TRUE, sMenuInfo.catMenu.menuElement[sMenuInfo.catMenu.iCurElement].bActive, !sCoreInfo.ProcessPrediction)
						ELSE
						
							// Confirm current item
							
							PRINTLN("SP RANGE MOUSE - DEBUG 7")
						
							// Confirm current item
							bCursorWeaponCatSelect = TRUE
							
						ENDIF
						
					ENDIF
					
					
				//////////////////////////////////////////////////////////////////////////////////////
				// Weapon selection menu
				//////////////////////////////////////////////////////////////////////////////////////
				ELIF sMenuInfo.curMenu = RANGE_MENU_WEAPONS
				OR (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES AND iCursorChallengeMenuItem = MENU_CURSOR_NO_ITEM) // This is so we can select stuff from this menu when in the challeng menu too.
				
					PRINTLN("SP RANGE MOUSE - DEBUG 8")
				
					sMenuInfo.curMenu = RANGE_MENU_WEAPONS	
					
					// A weapon menu item is being selected.
					IF iCursorWeaponMenuItem > MENU_CURSOR_NO_ITEM
						
						PRINTLN("SP RANGE MOUSE - DEBUG 9")
						
						// Get the weapon item being selected.
						iCursorWeaponSelection = GET_CURSOR_SELECTED_WEAPON_ITEM(iCursorWeaponMenuItem, sMenuInfo.catMenu.iCurElement, sMenuInfo.weapMenu.iNumElements)
						
						//The player is selecting outside the weapons menu, so try to handle this and select the new category
						IF iCursorWeaponSelection = MENU_CURSOR_NO_ITEM
						
							PRINTLN("SP RANGE MOUSE - DEBUG 10")
							CDEBUG2LN(DEBUG_SHOOTRANGE, "PROCESS_MENU_INPUT menu info ***************" )
							CDEBUG2LN(DEBUG_SHOOTRANGE, "iCursorWeaponMenuItem                     = ", iCursorWeaponMenuItem )
							CDEBUG2LN(DEBUG_SHOOTRANGE, "sMenuInfo.catMenu.iCurElement             = ", sMenuInfo.catMenu.iCurElement )
							CDEBUG2LN(DEBUG_SHOOTRANGE, "sMenuInfo.weapMenu.iNumElements           = ", sMenuInfo.weapMenu.iNumElements )
							
							// Get the new weapon category being selected.
							iCursorNewWeaponCategoryItem = GET_CURSOR_SELECTED_WEAPON_CATEGORY_FROM_WEAPON_MENU(iCursorWeaponMenuItem, sMenuInfo.catMenu.iCurElement, sMenuInfo.weapMenu.iNumElements)
							
							// Weapon category tab closes down when the player clicks on it.
							IF iCursorNewWeaponCategoryItem = -2 
								
								PRINTLN("SP RANGE MOUSE - DEBUG 11")
								bCursorBack = TRUE
							
							// Player selects 
							ELIF iCursorNewWeaponCategoryItem != MENU_CURSOR_NO_ITEM
							
								PRINTLN("SP RANGE MOUSE - DEBUG 12")
								
								SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, sMenuInfo.weapMenu.menuElement[sMenuInfo.weapMenu.iCurElement].bActive, !sCoreInfo.ProcessPrediction)
								
								// Only allow them to select unlocked items - causes bugs otherwise
								IF sMenuInfo.catMenu.menuElement[iCursorNewWeaponCategoryItem].bActive
								
									PRINTLN("SP RANGE MOUSE - DEBUG 13")
								
									sMenuInfo.catMenu.iCurElement =  iCursorNewWeaponCategoryItem // Switch weapon category
									sMenuInfo.weapMenu.iCurElement = 0 // Select first item
									
									sRndInfo.eWeaponType = INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.weapMenu.iCurElement)
									sRndInfo.eWeaponCat = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement)	// Fix for b*2197105. If user changes category while in weapon menu, the category data will be updated here, not in the usual non-mouse handling below. So need to set the round weapon-category here too.

									sMenuInfo.curMenu = INT_TO_ENUM(RANGE_MENU_INDEX, ENUM_TO_INT(sMenuInfo.curMenu) + 1)
														
									INIT_CHALLENGES_MENU(sWeapRnds, sCoreInfo, INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), sMenuInfo)
									CDEBUG2LN(DEBUG_SHOOTRANGE, "PROCESS_MENU_INPUT, sMenuInfo.catMenu.iCurElement = ", GET_STRING_FROM_RANGE_WEAPON_CATEGORY(INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement)) )
									INIT_WEAPONS_MENU(sCoreInfo, INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), sMenuInfo)
									
									// Re-setup our controls.
									
									bCursorWeaponSelect = TRUE
							
								ELSE
								
									// The item is locked, so we need to close down the weapon menu and return to the weapon category menu
									// with the relevant item selected.
									
									PRINTLN("SP RANGE MOUSE - DEBUG 13A New weapon category =  ", iCursorNewWeaponCategoryItem )
									
									sMenuInfo.catMenu.iCurElement = iCursorNewWeaponCategoryItem // Switch weapon category
									
									bCursorBack = TRUE							
									
								ENDIF
								
							ENDIF
						
						ELSE
						
									
							// Select new item
							//IF iCursorWeaponSelection != sMenuInfo.weapMenu.iCurElement
							
							
							PRINTLN("SP RANGE MOUSE - DEBUG 14")
							
							sMenuInfo.weapMenu.iCurElement = iCursorWeaponSelection
							
							// Item is unlocked
							IF sMenuInfo.weapMenu.menuElement[iCursorWeaponSelection].bActive
							
								PRINTLN("SP RANGE MOUSE - DEBUG 15")
							
								INT iWeaponBit
								iWeaponBit = (sMenuInfo.catMenu.iCurElement * 4) + sMenuInfo.weapMenu.iCurElement
								IF (iWeaponBit > -1) AND (iWeaponBit < 32)
									IF NOT IS_BIT_SET(g_savedGlobals.sRangeData[ePlayerChar].iWeaponLockStatus, iWeaponBit)
										SET_BIT(g_savedGlobals.sRangeData[ePlayerChar].iWeaponLockStatus, iWeaponBit)
									ENDIF
								ENDIF
								
								CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU WEAPONS CURSOR SELECT - CUR POS = ", sMenuInfo.weapMenu.iCurElement)
						
								PLAY_RANGE_SOUND("NAV_UP_DOWN", TRUE)
								
								sRndInfo.eWeaponType = INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.weapMenu.iCurElement)
								sMenuInfo.curMenu = INT_TO_ENUM(RANGE_MENU_INDEX, ENUM_TO_INT(sMenuInfo.curMenu) + 1)
								
								// B*2255267 - Initialise challenge menu item here (if needed)
								IF sMenuInfo.chalMenu.iCurElement = -1
									sMenuInfo.chalMenu.iCurElement = 0
								ENDIF
					
								INIT_CHALLENGES_MENU(sWeapRnds, sCoreInfo, INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), sMenuInfo)
						
								// Re-setup our controls.
								SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, sMenuInfo.weapMenu.menuElement[sMenuInfo.weapMenu.iCurElement].bActive, !sCoreInfo.ProcessPrediction)
							
							ELSE
							
								// Item is locked
								PRINTLN("SP RANGE MOUSE - DEBUG 14A")
								
								sRndInfo.eWeaponType = INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.weapMenu.iCurElement)
								INIT_CHALLENGES_MENU(sWeapRnds, sCoreInfo, INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), sMenuInfo)
								// Re-setup our controls.
								SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, sMenuInfo.weapMenu.menuElement[sMenuInfo.weapMenu.iCurElement].bActive, !sCoreInfo.ProcessPrediction)
								
								PLAY_RANGE_SOUND("NAV_UP_DOWN", TRUE)
								
							ENDIF
							
						ENDIF
						

					ENDIF
					
					
				//////////////////////////////////////////////////////////////////////////////////////
				// Challenge menu
				//////////////////////////////////////////////////////////////////////////////////////
				
				ELIF sMenuInfo.curMenu = RANGE_MENU_CHALLENGES
							
					IF iCursorChallengeMenuItem > MENU_CURSOR_NO_ITEM
					
						// Select new item
						IF iCursorChallengeMenuItem != sMenuInfo.chalMenu.iCurElement
						
							sMenuInfo.chalMenu.iCurElement = iCursorChallengeMenuItem
						
							CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU CHALLENGES CURSOR SELECT - CUR POS = ", sMenuInfo.chalMenu.iCurElement)
				
							// We went over it. Not new.
							IF NOT IS_BIT_SET(g_savedGlobals.sRangeData[ePlayerChar].iCatChalLockStatus, ENUM_TO_INT(R_CAT_UNLOCK_HG) + sMenuInfo.catMenu.iCurElement)
								SET_BIT(g_savedGlobals.sRangeData[ePlayerChar].iCatChalLockStatus, ENUM_TO_INT(R_CAT_UNLOCK_HG) + sMenuInfo.catMenu.iCurElement)
							ENDIF

							PLAY_RANGE_SOUND("NAV_UP_DOWN", TRUE)
							
							// Re-setup our controls.
							SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, sMenuInfo.chalMenu.menuElement[sMenuInfo.chalMenu.iCurElement].bActive, !sCoreInfo.ProcessPrediction)
					
						ELSE
							// Accept item
							
							// The following has been removed to fix B* 2197105
							// --
							// Set values of weapon category and weapon here, as these might not get set if we directly click on the challenge. Fixes B* 1878816
//							sRndInfo.eWeaponType = INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.weapMenu.iCurElement)
//							sRndInfo.eWeaponCat = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement)
							// --
							
							bCursorChallengeAccept = TRUE
						ENDIF
						
					ENDIF
				ENDIF
			
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
				bCursorBack = TRUE
			ENDIF
			
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW) // Dimmed otherwise
			
		ENDIF
		
	ENDIF
	
	//////////////////////////////////////////////////////////////////////////////////////
	// End of mouse specific code
	//////////////////////////////////////////////////////////////////////////////////////
		
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD) AND NOT IS_RANGE_MENU_FLAG_SET(sMenuInfo, RMF_SHOWING_SCLB)
		IF IS_SIMPLE_USE_CONTEXT_INPUT_ADDED(sMenuInfo.menuContext, FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			PLAY_RANGE_SOUND("LEADER_BOARD", TRUE)
			INT iElement = 0
			IF (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
				iElement = sMenuInfo.chalMenu.iCurElement
			ENDIF
			RANGE_ROUND_TYPE eRndTemp = sWeapRnds[sMenuInfo.catMenu.iCurElement].sChallenges[iElement].eRndType
			SET_RANGE_MENU_FLAG(sMenuInfo, RMF_SHOWING_SCLB)
			SET_RANGE_MENU_FLAG(sMenuInfo, RMF_SET_RANGE_SCLB_BUTTONS)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "SP_RANGE_DISPLAY_SOCIAL_CLUB_LEADERBOARD(sMenuInfo.uiLeaderboard, ", eRndTemp, ")")
		ENDIF
		
	ELIF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR bCursorBack = TRUE ) AND IS_RANGE_MENU_FLAG_SET(sMenuInfo, RMF_SHOWING_SCLB)
		CLEAR_RANGE_MENU_FLAG(sMenuInfo, RMF_SHOWING_SCLB)
		//RANGE_SP_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
		SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, sMenuInfo.curMenu = RANGE_MENU_WEAPCAT, DEFAULT, !sCoreInfo.ProcessPrediction OR NOT IS_PLAYER_ONLINE())
		RETURN RETVAL_NONE
	ENDIF
	
	//CDEBUG2LN(DEBUG_SHOOTRANGE, "Drawing Menu")
	SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
	
	UPDATE_SIMPLE_USE_CONTEXT(sMenuInfo.menuContext)
	
	IF IS_RANGE_MENU_FLAG_SET(sMenuInfo, RMF_SHOWING_SCLB)
		RETURN RETVAL_NONE
	ENDIF
	
	// Okay, handle the button we've gotten.
	IF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR bCursorBack = TRUE )
	AND sMenuInfo.curMenu = RANGE_MENU_WEAPCAT
		PLAY_RANGE_SOUND("BACK", TRUE)
		SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
		RETURN RETVAL_FALSE
	ENDIF	//BREAK
	
		
	IF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
		OR bCursorWeaponCatSelect = TRUE
		OR bCursorWeaponSelect = TRUE
		OR bCursorChallengeAccept = TRUE)
	AND NOT IS_RANGE_MENU_FLAG_SET(sMenuInfo, RMF_EXITED_SIGNIN)
		// A button is handled different depending on which menu we're on.
						
		IF (sMenuInfo.curMenu = RANGE_MENU_WEAPCAT)
		
			BOOL bWasTriggeredByMouseClick = bCursorWeaponCatSelect
			bCursorWeaponCatSelect = FALSE
			
			IF sMenuInfo.catMenu.menuElement[sMenuInfo.catMenu.iCurElement].bActive
				sRndInfo.eWeaponCat = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement)
				sMenuInfo.curMenu = RANGE_MENU_WEAPONS
				INIT_WEAPONS_MENU(sCoreInfo, sRndInfo.eWeaponCat, sMenuInfo)
				
				SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, DEFAULT, TRUE)
				
				// We went over a weapon. Not new.
				INT iWeaponBit
				iWeaponBit = (sMenuInfo.catMenu.iCurElement * 4) + sMenuInfo.weapMenu.iCurElement
				IF (iWeaponBit > -1) AND (iWeaponBit < 32)
					IF NOT IS_BIT_SET(g_savedGlobals.sRangeData[ePlayerChar].iWeaponLockStatus, iWeaponBit)
						SET_BIT(g_savedGlobals.sRangeData[ePlayerChar].iWeaponLockStatus, iWeaponBit)
					ENDIF
				ENDIF
				
				PLAY_RANGE_SOUND("SELECT", TRUE)
				
				
				// This is needed so when using mouse the challenge menu gets selected automatically
				// when you click on a weapon.
				
				IF bWasTriggeredByMouseClick

					sMenuInfo.curMenu = RANGE_MENU_WEAPONS

				ELSE
													
					RETURN RETVAL_NONE
				
				ENDIF
				
			ELSE
				PLAY_RANGE_SOUND("BACK", TRUE)
			ENDIF
			
		ENDIF
		
		IF (sMenuInfo.curMenu = RANGE_MENU_WEAPONS) 
		
			bCursorWeaponSelect = FALSE
		
			IF sMenuInfo.weapMenu.menuElement[sMenuInfo.weapMenu.iCurElement].bActive
				sRndInfo.eWeaponType = INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.weapMenu.iCurElement)
				sMenuInfo.curMenu = INT_TO_ENUM(RANGE_MENU_INDEX, ENUM_TO_INT(sMenuInfo.curMenu) + 1)
				INIT_CHALLENGES_MENU(sWeapRnds, sCoreInfo, sRndInfo.eWeaponCat, sMenuInfo)
				
				SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, DEFAULT, TRUE)
				
				// If we're going into the challenge menu, select the highest available challenge.
				// this is as per #287682 - Select the highest available challenge when going into the challenges menu.
				IF sMenuInfo.chalMenu.menuElement[3].bActive AND 
					IS_ROUND_PASSED(GET_RANGE_ROUND_FROM_CAT_AND_CHAL(sRndInfo.eWeaponCat, INT_TO_ENUM(RANGE_WEAPON_CHALLENGES, 2))) //Challenge 4
					sMenuInfo.chalMenu.iCurElement = 3
				ELIF sMenuInfo.chalMenu.menuElement[2].bActive AND 
					IS_ROUND_PASSED(GET_RANGE_ROUND_FROM_CAT_AND_CHAL(sRndInfo.eWeaponCat, INT_TO_ENUM(RANGE_WEAPON_CHALLENGES, 1))) //Challenge 3
					sMenuInfo.chalMenu.iCurElement = 2
				ELIF sMenuInfo.chalMenu.menuElement[1].bActive AND 
					IS_ROUND_PASSED(GET_RANGE_ROUND_FROM_CAT_AND_CHAL(sRndInfo.eWeaponCat, INT_TO_ENUM(RANGE_WEAPON_CHALLENGES, 0))) //Challenge 2
					sMenuInfo.chalMenu.iCurElement = 1
				ELSE
					sMenuInfo.chalMenu.iCurElement = 0
				ENDIF

				PLAY_RANGE_SOUND("SELECT", TRUE)

				RETURN RETVAL_NONE
			ELSE
				PLAY_RANGE_SOUND("BACK", TRUE)
			ENDIF
			
		ELIF (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES) 
		AND bCursorWeaponSelect = FALSE
				
			bCursorChallengeAccept = FALSE
			
			IF (sMenuInfo.chalMenu.iCurElement = -1)
				sMenuInfo.chalMenu.iCurElement = 0
			ENDIF
			
			IF (sMenuInfo.chalMenu.menuElement[sMenuInfo.chalMenu.iCurElement].bActive)
				
				PLAY_RANGE_SOUND("SELECT", TRUE)
				
				// We are using a weapon. MArk it as used.
				INT iWeaponBit
				iWeaponBit = (sMenuInfo.catMenu.iCurElement * 4) + sMenuInfo.weapMenu.iCurElement
				IF (iWeaponBit > -1) AND (iWeaponBit < 32)
					IF NOT IS_BIT_SET(g_savedGlobals.sRangeData[ePlayerChar].iWeaponUsedStatus, iWeaponBit)
						SET_BIT(g_savedGlobals.sRangeData[ePlayerChar].iWeaponUsedStatus, iWeaponBit)
					ENDIF
				ENDIF
			
				// We just selected a challenge, we're done.
				sRndInfo.eChallengeType = INT_TO_ENUM(RANGE_WEAPON_CHALLENGES, sMenuInfo.chalMenu.iCurElement)
				SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
				RETURN RETVAL_TRUE
			ELSE
				PLAY_RANGE_SOUND("BACK", TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF ((IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR bCursorBack = TRUE) AND sMenuInfo.curMenu > RANGE_MENU_WEAPCAT)
	AND NOT IS_RANGE_MENU_FLAG_SET(sMenuInfo, RMF_EXITED_SIGNIN)
		
		bCursorBack = FALSE
		
		// We've hit back. Different depending on the menu we're on. We can't go back on the main menu.
		IF (sMenuInfo.curMenu <> RANGE_MENU_WEAPCAT)	
			PLAY_RANGE_SOUND("BACK", TRUE)
			sMenuInfo.curMenu = INT_TO_ENUM(RANGE_MENU_INDEX, ENUM_TO_INT(sMenuInfo.curMenu) - 1)
			
			IF (sMenuInfo.curMenu = RANGE_MENU_WEAPCAT)
				sMenuInfo.weapMenu.iNumElements = 0
				
				SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, TRUE, DEFAULT, TRUE)
			ELSE
				SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, DEFAULT, TRUE)
			ENDIF
			
			RETURN RETVAL_NONE
		ENDIF
	ENDIF	//BREAK
	
	CLEAR_RANGE_MENU_FLAG(sMenuInfo, RMF_EXITED_SIGNIN)
	
	// Maybe need to handle some pad/stick input.
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) OR IS_LEFT_ANALOG_AS_DPAD(LEFT_ANALOG_AS_DPAD_UP, sMenuInfo.uiInput))
	OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND GET_GAME_TIMER() > GET_RANGE_SP_MENU_HOLD_DELAY(sMenuInfo))
		IF (sMenuInfo.curMenu = RANGE_MENU_WEAPCAT)
			sMenuInfo.catMenu.iCurElement -= 1
			IF (sMenuInfo.catMenu.iCurElement < 0)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU WEAPCAT CYCLE UP")
				sMenuInfo.catMenu.iCurElement = sMenuInfo.catMenu.iNumElements - 1
			ENDIF
			INIT_CHALLENGES_MENU(sWeapRnds, sCoreInfo, INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), sMenuInfo)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU WEAPCAT - CUR POS = ", sMenuInfo.catMenu.iCurElement)
			
			PLAY_RANGE_SOUND("NAV_UP_DOWN", TRUE)
			
			// Re-setup our controls.
			SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, TRUE, sMenuInfo.catMenu.menuElement[sMenuInfo.catMenu.iCurElement].bActive, !sCoreInfo.ProcessPrediction)
			
		ELIF (sMenuInfo.curMenu = RANGE_MENU_WEAPONS)
			INT iOld = sMenuInfo.weapMenu.iCurElement
			sMenuInfo.weapMenu.iCurElement -= 1
			IF (sMenuInfo.weapMenu.iCurElement < 0)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU WEAPONS CYCLE UP")
				sMenuInfo.weapMenu.iCurElement = sMenuInfo.weapMenu.iNumElements - 1
			ENDIF
		CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU WEAPONS - CUR POS = ", sMenuInfo.weapMenu.iCurElement)
			
			// We went over a weapon. Not new.
			INT iWeaponBit
			iWeaponBit = (sMenuInfo.catMenu.iCurElement * 4) + sMenuInfo.weapMenu.iCurElement
			IF (iWeaponBit > -1) AND (iWeaponBit < 32)
				IF NOT IS_BIT_SET(g_savedGlobals.sRangeData[ePlayerChar].iWeaponLockStatus, iWeaponBit)
					SET_BIT(g_savedGlobals.sRangeData[ePlayerChar].iWeaponLockStatus, iWeaponBit)
				ENDIF
			ENDIF
			
			IF (iOld != sMenuInfo.weapMenu.iCurElement)
				PLAY_RANGE_SOUND("NAV_UP_DOWN", TRUE)
			ENDIF
			
			// Re-setup our controls.
			SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, sMenuInfo.weapMenu.menuElement[sMenuInfo.weapMenu.iCurElement].bActive, !sCoreInfo.ProcessPrediction)
			
		ELIF (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
			sMenuInfo.chalMenu.iCurElement -= 1
			IF (sMenuInfo.chalMenu.iCurElement < 0)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU CHALLENGES CYCLE UP")
				sMenuInfo.chalMenu.iCurElement = sMenuInfo.chalMenu.iNumElements - 1
			ENDIF
		CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU CHALLENGES - CUR POS = ", sMenuInfo.chalMenu.iCurElement)
			
			// We went over it. Not new.
			IF NOT IS_BIT_SET(g_savedGlobals.sRangeData[ePlayerChar].iCatChalLockStatus, ENUM_TO_INT(R_CAT_UNLOCK_HG) + sMenuInfo.catMenu.iCurElement)
				SET_BIT(g_savedGlobals.sRangeData[ePlayerChar].iCatChalLockStatus, ENUM_TO_INT(R_CAT_UNLOCK_HG) + sMenuInfo.catMenu.iCurElement)
			ENDIF
		
			PLAY_RANGE_SOUND("NAV_UP_DOWN", TRUE)
			
			// Re-setup our controls.
			SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, sMenuInfo.chalMenu.menuElement[sMenuInfo.chalMenu.iCurElement].bActive, !sCoreInfo.ProcessPrediction)
		ENDIF
		SET_RANGE_SP_MENU_HOLD_DELAY(sMenuInfo, GET_GAME_TIMER() + DELAY_BEFORE_REACTIVATED)
	
		RETURN RETVAL_NONE
	ENDIF
	
	// Move Down
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) OR IS_LEFT_ANALOG_AS_DPAD(LEFT_ANALOG_AS_DPAD_DOWN, sMenuInfo.uiInput))
	OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND GET_GAME_TIMER() > GET_RANGE_SP_MENU_HOLD_DELAY(sMenuInfo))
		IF (sMenuInfo.curMenu = RANGE_MENU_WEAPCAT)
			sMenuInfo.catMenu.iCurElement += 1
			IF (sMenuInfo.catMenu.iCurElement >= sMenuInfo.catMenu.iNumElements)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU WEAPCAT CYCLE DOWN")
				sMenuInfo.catMenu.iCurElement = 0
			ENDIF
			INIT_CHALLENGES_MENU(sWeapRnds, sCoreInfo, INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.catMenu.iCurElement), sMenuInfo)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU WEAPCAT - CUR POS = ", sMenuInfo.catMenu.iCurElement)
			
			PLAY_RANGE_SOUND("NAV_UP_DOWN", TRUE)
			
			// Re-setup our controls.
			SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, TRUE, sMenuInfo.catMenu.menuElement[sMenuInfo.catMenu.iCurElement].bActive, !sCoreInfo.ProcessPrediction)
			
		ELIF (sMenuInfo.curMenu = RANGE_MENU_WEAPONS)
			INT iOld = sMenuInfo.weapMenu.iCurElement
			sMenuInfo.weapMenu.iCurElement += 1
			IF (sMenuInfo.weapMenu.iCurElement >= sMenuInfo.weapMenu.iNumElements)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU WEAPONS CYCLE DOWN")
				sMenuInfo.weapMenu.iCurElement = 0
			ENDIF
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU WEAPONS - CUR POS = ", sMenuInfo.weapMenu.iCurElement)
			
			// We went over a weapon. Not new.
			INT iWeaponBit
			iWeaponBit = (sMenuInfo.catMenu.iCurElement * 4) + sMenuInfo.weapMenu.iCurElement
			IF (iWeaponBit > -1) AND (iWeaponBit < 32)
				IF NOT IS_BIT_SET(g_savedGlobals.sRangeData[ePlayerChar].iWeaponLockStatus, iWeaponBit)
					SET_BIT(g_savedGlobals.sRangeData[ePlayerChar].iWeaponLockStatus, iWeaponBit)
				ENDIF
			ENDIF
			
			IF (iOld != sMenuInfo.weapMenu.iCurElement)
				PLAY_RANGE_SOUND("NAV_UP_DOWN", TRUE)
			ENDIF
			
			// Re-setup our controls.
			SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, sMenuInfo.weapMenu.menuElement[sMenuInfo.weapMenu.iCurElement].bActive, !sCoreInfo.ProcessPrediction)
			
		ELIF (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
			sMenuInfo.chalMenu.iCurElement += 1
			IF (sMenuInfo.chalMenu.iCurElement >= sMenuInfo.chalMenu.iNumElements)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU CHALLENGES CYCLE DOWN")
				sMenuInfo.chalMenu.iCurElement = 0
			ENDIF
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU CHALLENGES - CUR POS = ", sMenuInfo.chalMenu.iCurElement)
			
			// We went over it. Not new.
			IF NOT IS_BIT_SET(g_savedGlobals.sRangeData[ePlayerChar].iCatChalLockStatus, ENUM_TO_INT(R_CAT_UNLOCK_HG) + sMenuInfo.catMenu.iCurElement)
				SET_BIT(g_savedGlobals.sRangeData[ePlayerChar].iCatChalLockStatus, ENUM_TO_INT(R_CAT_UNLOCK_HG) + sMenuInfo.catMenu.iCurElement)
			ENDIF

			PLAY_RANGE_SOUND("NAV_UP_DOWN", TRUE)
			
			// Re-setup our controls.
			SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, FALSE, sMenuInfo.chalMenu.menuElement[sMenuInfo.chalMenu.iCurElement].bActive, !sCoreInfo.ProcessPrediction)
		ENDIF
		SET_RANGE_SP_MENU_HOLD_DELAY(sMenuInfo, GET_GAME_TIMER() + DELAY_BEFORE_REACTIVATED)

		RETURN RETVAL_NONE
	ENDIF
	
// Mouse debug stuff
//	DISPLAY_TEXT_WITH_NUMBER( 0.05,0.2, "NUMBER", sMenuInfo.catMenu.iCurElement)
//	DISPLAY_TEXT_WITH_NUMBER( 0.15,0.2, "NUMBER", iCursorWeaponMenuItem)
//	DISPLAY_TEXT_WITH_NUMBER( 0.05,0.3, "NUMBER", sMenuInfo.weapMenu.iCurElement)
//	DISPLAY_TEXT_WITH_NUMBER( 0.15,0.3, "NUMBER", iCursorWeaponMenuItem)
//	DISPLAY_TEXT_WITH_NUMBER( 0.05,0.4, "NUMBER", sMenuInfo.chalMenu.iCurElement)
//	DISPLAY_TEXT_WITH_NUMBER( 0.15,0.4, "NUMBER", iCursorChallengeMenuItem)
//	DISPLAY_TEXT_WITH_NUMBER( 0.1,0.4,"NUMBER", GET_CURSOR_SELECTED_WEAPON_ITEM(iCursorWeaponMenuItem, sMenuInfo.catMenu.iCurElement))
//	DISPLAY_TEXT_WITH_NUMBER( 0.1,0.5, "NUMBER", iCursorNewWeaponCategoryItem)

	RETURN RETVAL_NONE
ENDFUNC


/// PURPOSE:
///    Draws and handles the updating of the main menu.
/// RETURNS:
///    TRUE when we're returning something in retRndDesc
FUNC THREE_STATE_RETVAL UPDATE_MAIN_MENU(Range_CoreData & sCrData, Range_RoundInfo & sRndInfo, Range_WeaponRounds & sWeapRnds[], Range_MenuData & sMenuInfo)
	IF NOT g_bResultScreenDisplaying
		SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
	ENDIF	
	// If we're flagged as showing the social club leaderboard, then show that. Otherwise, draw the normal menu.
	IF IS_RANGE_MENU_FLAG_SET(sMenuInfo, RMF_SHOWING_SCLB)
		IF NOT IS_PLAYER_ONLINE()
			// Not online? Just display the warning.
			IF DO_SIGNED_OUT_WARNING(sMenuInfo.iLeaderboardWarningBitset)
				CLEAR_RANGE_MENU_FLAG(sMenuInfo, RMF_SHOWING_SCLB)
				SET_RANGE_MENU_FLAG(sMenuInfo, RMF_EXITED_SIGNIN)
				CLEAR_RANGE_MENU_FLAG(sMenuInfo, RMF_SCLB_PROFILE_BUTTON_SHOWN)
				SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, TRUE,  sMenuInfo.catMenu.menuElement[sMenuInfo.catMenu.iCurElement].bActive, FALSE)
			ENDIF
		ELSE
			INT iElement = 0
			IF (sMenuInfo.curMenu = RANGE_MENU_CHALLENGES)
				iElement = sMenuInfo.chalMenu.iCurElement
			ENDIF
			
			RANGE_ROUND_TYPE eRndTemp = sWeapRnds[sMenuInfo.catMenu.iCurElement].sChallenges[iElement].eRndType
			SP_RANGE_DISPLAY_SOCIAL_CLUB_LEADERBOARD(sMenuInfo.uiLeaderboard, eRndTemp, sCrData.lbdWrite, (sCrData.ePredictedRnd = eRndTemp))
			
			IF IS_RANGE_MENU_FLAG_SET(sMenuInfo, RMF_SET_RANGE_SCLB_BUTTONS)
				CLEAR_RANGE_MENU_FLAG(sMenuInfo, RMF_SET_RANGE_SCLB_BUTTONS)
				INIT_SIMPLE_USE_CONTEXT(sMenuInfo.menuContext, FALSE, FALSE, TRUE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(sMenuInfo.menuContext, "FE_HLP3",			FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				IF RANGE_SHOULD_SCLB_SHOW_PROFILE_BUTTON()
					SET_RANGE_MENU_FLAG(sMenuInfo, RMF_SCLB_PROFILE_BUTTON_SHOWN)
					ADD_SIMPLE_USE_CONTEXT_INPUT(sMenuInfo.menuContext, "SCLB_PROFILE",	FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
				ENDIF
			ENDIF
			IF RANGE_SHOULD_SCLB_SHOW_PROFILE_BUTTON() AND NOT IS_RANGE_MENU_FLAG_SET(sMenuInfo, RMF_SCLB_PROFILE_BUTTON_SHOWN)
				SET_RANGE_MENU_FLAG(sMenuInfo, RMF_SCLB_PROFILE_BUTTON_SHOWN)
				INIT_SIMPLE_USE_CONTEXT(sMenuInfo.menuContext, FALSE, FALSE, TRUE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(sMenuInfo.menuContext, "FE_HLP3",			FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				ADD_SIMPLE_USE_CONTEXT_INPUT(sMenuInfo.menuContext, "SCLB_PROFILE",		FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
			ELIF NOT RANGE_SHOULD_SCLB_SHOW_PROFILE_BUTTON() AND IS_RANGE_MENU_FLAG_SET(sMenuInfo, RMF_SCLB_PROFILE_BUTTON_SHOWN)
				CLEAR_RANGE_MENU_FLAG(sMenuInfo, RMF_SCLB_PROFILE_BUTTON_SHOWN)
				INIT_SIMPLE_USE_CONTEXT(sMenuInfo.menuContext, FALSE, FALSE, TRUE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(sMenuInfo.menuContext, "FE_HLP3",			FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			ENDIF
		ENDIF
	ELSE
		// If we're still making predictions, don't add the leaderboard prompt.
		IF IS_PLAYER_ONLINE()
			// If the player wasn't online, we need to force the read right now. 
			// The player got here by finishing a round, getting back to the menu, then logging online.
			IF NOT sCrData.bOnlineForPrediction
				CDEBUG1LN(DEBUG_SHOOTRANGE, "Player logged online after posting a score while offline! Trigger emergency read/write!")
				sCrData.bOnlineForPrediction = TRUE
				sCrData.ProcessPrediction = TRUE
				
				SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, TRUE,  sMenuInfo.catMenu.menuElement[sMenuInfo.catMenu.iCurElement].bActive, FALSE)

				RANGE_SP_PRE_READ_LBD(sCrData.lbdWrite, sRndInfo, sWeapRnds[sRndInfo.eWeaponCat].sChallenges[sRndInfo.eChallengeType])
				RETURN RETVAL_NONE
			ENDIF
			
			IF sCrData.ProcessPrediction
				IF scLB_rank_predict.bFinishedRead
				AND NOT scLB_rank_predict.bFinishedWrite
					CDEBUG1LN(DEBUG_SHOOTRANGE, "Doing predictive write.")
					WRITE_TO_RANGE_SP_HIGH_SCORE_LEADERBOARD(sCrData.lbdWrite)
				ENDIF
				IF GET_RANK_PREDICTION_DETAILS(rangeLBControl)
					CDEBUG1LN(DEBUG_SHOOTRANGE, "Got prediction details! All done!")
					
					// Setup new prompts.
					SETUP_RANGE_MENU_CONTROLS(sMenuInfo.menuContext, (sMenuInfo.curMenu = RANGE_MENU_WEAPCAT), DEFAULT, TRUE)
					
					sclb_useRankPrediction = TRUE
					sCrData.ProcessPrediction = FALSE
				ENDIF
			ENDIF
		ELSE
			// Never process prediction if we're not online.
			sCrData.ProcessPrediction = FALSE
		ENDIF

		PROCESS_SCREEN_RANGE_MENU(sWeapRnds, sMenuInfo, sCrData)
	ENDIF
	
	#IF COMPILE_WIDGET_OUTPUT
		EXPORT_SCREEN_MEGA_PLACEMENT(sMenuInfo.uiPlacement)
	#ENDIF
	
#IF IS_DEBUG_BUILD
		IF IS_DEBUG_KEY_PRESSED(KEY_1, KEYBOARD_MODIFIER_SHIFT, "")
			SET_ALL_RANGE_ROUNDS_GOLDED()
//			SET_ALL_RANGE_ROUNDS_SPECIFIC()
		ELIF IS_DEBUG_KEY_PRESSED(KEY_2, KEYBOARD_MODIFIER_SHIFT, "")
			SET_ALL_RANGE_ROUNDS_SILVERED()
		ELIF IS_DEBUG_KEY_PRESSED(KEY_3, KEYBOARD_MODIFIER_SHIFT, "")
			SET_ALL_RANGE_ROUNDS_BRONZED()
		ENDIF
#ENDIF

// First off, catch a flag that says we need to re-init.
	IF (sCrData.bNeedMenuReinit)
		INIT_CATEGORY_MENU(sCrData, sMenuInfo)
		sCrData.bNeedMenuReinit = FALSE
		sMenuInfo.curMenu = RANGE_MENU_WEAPCAT
	ENDIF

	// Okay update the input: =============================================================
	RETURN PROCESS_MENU_INPUT(sRndInfo, sWeapRnds, sCrData, sMenuInfo)
	// ====================================================================================
ENDFUNC


/// PURPOSE:
///    Draws the players points he is rewarded when he hits a target
PROC DRAW_SPT(SPT_OBJECT & sSPTObj[])
	INT iI
	
	// Update all of the text
	UPDATE_SPT(sSPTObj)
	
	SET_TEXT_OUTLINE()
	
	iI = MAX_NUM_SPT - 1
	WHILE iI >= 0
		IF sSPTObj[iI].bActive
			SET_TEXT_OUTLINE()
			DISPLAY_TEXT_LABEL_WITH_NUMBER(	"SHR_SPT_POINTS",
											sSPTObj[iI].fPosX - GET_STRING_WIDTH_WITH_NUMBER("SHR_SPT_POINTS", sSPTObj[iI].iPtValue) * fSPTTypeScale/2,
											sSPTObj[iI].fPosY - GET_RENDERED_CHARACTER_HEIGHT(fSPTTypeScale) * fSPTTypeScale/2,
											0.73333,
											0.73333,
											1.0,
											1.0,
											GetTextColor(sSPTObj[iI].color, sSPTObj[iI].iAlpha),
											sSPTObj[iI].iPtValue,
											0,
											0,
											FONT_STANDARD,2)
		ENDIF
		iI--
	ENDWHILE
ENDPROC


/// PURPOSE:
///    Main hud function
PROC DRAW_RANGE_HUD(Range_RoundInfo & sRndInfo, Range_WeaponRounds & sWeaponRoundInfo[], Range_SPTData & sSPTInfo, BOOL bForCompleteMsg = FALSE)
	// Get the time:
	IF IS_TIMER_STARTED(sRndInfo.sTimeLeft)
		sRndInfo.fTotalTime = sRndInfo.fCurrentRoundLength - GET_TIMER_IN_SECONDS(sRndInfo.sTimeLeft)
		IF (sRndInfo.fTotalTime < 0.0)
			sRndInfo.fTotalTime = 0.0
		ENDIF
	ELSE
		sRndInfo.fTotalTime = 0.0
	ENDIF
	INT iTotalTime = ROUND(sRndInfo.fTotalTime * 1000.0)
	
	// Get the goal score and string:
	INT iGoalToDisp
	STRING sGoalString 
	HUD_COLOURS eGoalColor
	INT iBronzeReq = GET_MEDAL_SCORE_REQUIREMENT(sWeaponRoundInfo[sRndInfo.eWeaponCat].sChallenges[sRndInfo.eChallengeType], RRM_BRONZE)
	INT iSilverReq = GET_MEDAL_SCORE_REQUIREMENT(sWeaponRoundInfo[sRndInfo.eWeaponCat].sChallenges[sRndInfo.eChallengeType], RRM_SILVER)
	IF (sRndInfo.sScoreData.iP1Score < iBronzeReq)
		iGoalToDisp = iBronzeReq
		sGoalString = "SHR_GOAL_T_BNZE"
		eGoalColor = HUD_COLOUR_BRONZE
	ELIF (sRndInfo.sScoreData.iP1Score < iSilverReq)
		iGoalToDisp = iSilverReq
		sGoalString = "SHR_GOAL_T_SILV"
		eGoalColor = HUD_COLOUR_SILVER
	ELSE
		iGoalToDisp = GET_MEDAL_SCORE_REQUIREMENT(sWeaponRoundInfo[sRndInfo.eWeaponCat].sChallenges[sRndInfo.eChallengeType], RRM_GOLD)
		sGoalString = "SHR_GOAL_T_GOLD"
		eGoalColor = HUD_COLOUR_GOLD
	ENDIF
	
	// Get the current num targets, and total targets:
	INT iTargets = -1, iTotalTargets = -1
	IF NOT IS_ROUND_FLAG_SET(sRndInfo, ROUND_USE_METAL_TARGETS) AND NOT IS_ROUND_FLAG_SET(sRndInfo, ROUND_TARGETS_ARE_INFINITE)
		iTargets = sRndInfo.iTargetsDestroyed
		iTotalTargets = sRndInfo.iMaxTargets
	ENDIF
	
	// Setup the MP, make it non-existant if we're in a shotty or minigun round:
	IF (sRndInfo.eWeaponCat = WEAPCAT_SHOTGUN)
	OR (sRndInfo.eWeaponCat = WEAPCAT_RAILGUN)// OR (sRndInfo.eWeaponCat = WEAPCAT_MINIGUN)
		sRndInfo.sMPData.iCurrMP = -1
	ENDIF
	
	// Set the color we're to draw the timer as.
	HUD_COLOURS eColorToDraw = HUD_COLOUR_WHITE
	IF (iTotalTime <= 6000)
		eColorToDraw = HUD_COLOUR_RED
	ENDIF
		
	// Draw the HUD. Most is already store, the rest was calculated above:
	Range_HUD_Data sDrawData
	
	IF bForCompleteMsg 
	AND sRndInfo.sScoreData.iTimeBonus != 0 
	AND NOT IS_ROUND_FLAG_SET(sRndInfo, ROUND_NO_TIME_BONUS)
		// Here, draw the time bonus the player earned, instead of time.
		sDrawData.iOpponentScore = sRndInfo.sScoreData.iTimeBonus
		sDrawData.sOpponentScoreLabel = "SHR_TIME_BONUS"
		sDrawData.eOpponentScoreColor = HUD_COLOUR_GREEN
	ELSE
		sDrawData.iTimerVal = iTotalTime
		sDrawData.eTimerColor = eColorToDraw
	ENDIF
	
	sDrawData.iCurScore = sRndInfo.sScoreData.iP1Score
	
	sDrawData.sGoalScoreLabel = sGoalString
	sDrawData.iGoalScore = iGoalToDisp
	sDrawData.eGoalScoreColor = eGoalColor
	
	sDrawData.iCurTargets = iTargets
	sDrawData.iMaxTargets = iTotalTargets
	
	sDrawData.iMultiplier = sRndInfo.sMPData.iCurrMP
	sDrawData.iCurMultiplierBlocks = sRndInfo.sMPData.iMPBlocks
	sDrawData.iMaxMultiplierBlocks = iMULTIPLIER_BLOCKS
	
	sDrawData.iBestScore = GET_ROUND_HIGH_SCORE(sRndInfo.eRoundType) 
	
	DRAW_SHOOTING_RANGE_HUD(sDrawData)
	
	// Draw our scrolling point text:
	DRAW_SPT(sSPTInfo.sSptQueue)
ENDPROC


/// PURPOSE:
///    Updates the "are you sure you want to exit" screen.
PROC UPDATE_UI_EXIT(BOOL bRangeExit = FALSE)
	IF bRangeExit
		SET_WARNING_MESSAGE_WITH_HEADER("SHR_QUIT_RANGE", "SHR_QUIT_DET", FE_WARNING_NO | FE_WARNING_YES)
	ELSE
		SET_WARNING_MESSAGE_WITH_HEADER("SHR_QUIT_RANGE", "SHR_QUIT_RNDDET", FE_WARNING_NO | FE_WARNING_YES)
	ENDIF
	
	SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
ENDPROC

/// PURPOSE:
///    Sets what the scaleform movie should draw on the round voer message.
PROC SET_ROUND_OVER_MESSAGE(Range_RoundInfo & sRndInfo, Range_MenuData & sMenuInfo, BOOL bPassed)
	TEXT_LABEL_15 txtWeapon
	GET_WEAPON_STRING_INFO(sRndInfo.eWeaponCat, sRndInfo.eWeaponType, txtWeapon)
	INT iChallenge = ENUM_TO_INT(sRndInfo.eChallengeType) + 1
	
	STRING sMsg = PICK_STRING(bPassed, "SC_PASS", "SC_FAIL")
	HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW
	IF NOT bPassed
		eHudColor = HUD_COLOUR_RED
	ENDIF
	
	IF (sRndInfo.iTargetsDestroyed >= sRndInfo.iMaxTargets) AND NOT IS_BITMASK_AS_ENUM_SET(sRndInfo.iRoundFlags, ROUND_TARGETS_ARE_INFINITE)
		SET_RANGE_BIG_MESSAGE(sMenuInfo.uiSplashText, sMsg, txtWeapon, iChallenge, eHudColor)
		
	// Is time up?
	ELIF IS_ROUND_FLAG_SET(sRndInfo, ROUND_TIME_IS_UP)
		SET_RANGE_BIG_MESSAGE(sMenuInfo.uiSplashText, sMsg, txtWeapon, iChallenge, eHudColor)

	// did we run out of ammo?
	ELIF GET_AMMO_IN_PED_WEAPON(rangePlayerPed, GET_WEAPONTYPE_FROM_RANGE_INFO(sRndInfo.eWeaponCat, sRndInfo.eWeaponType)) <= 0
		SET_RANGE_BIG_MESSAGE(sMenuInfo.uiSplashText, "SC_COM_AMMO", txtWeapon, iChallenge, eHudColor)
	ENDIF
ENDPROC






