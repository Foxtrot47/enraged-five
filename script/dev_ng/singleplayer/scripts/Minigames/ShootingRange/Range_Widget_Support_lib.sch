// Range_Widget_Support_lib.sch
USING "screen_placements.sch"
USING "hud_drawing.sch"

// Main Data.
WIDGET_GROUP_ID	ShootingRangeWidgets = NULL

// Widget Modes
BOOL bDrawTarget = FALSE

// Target Widget Variables
WIDGET_GROUP_ID	targetWidgets = NULL
BOOL bDrawTargetZones = FALSE
ENTITY_INDEX oDebugTarget
VECTOR vTargetPos

/// PURPOSE:
///    Provides debugging support for the target.
PROC UPDATE_TARGET_WIDGET()
	IF (targetWidgets = NULL)
		SET_CURRENT_WIDGET_GROUP(ShootingRangeWidgets)
			targetWidgets = START_WIDGET_GROUP("Target Debug")
				ADD_WIDGET_BOOL("Draw Target Zones", bDrawTargetZones)
				ADD_WIDGET_FLOAT_SLIDER("Bullseye Radius", fTgtBullseyeRadius, 0.0, 1.5, 0.0025)
				ADD_WIDGET_FLOAT_SLIDER("Ring 1 Radius", fTgtRing1Radius, 0.0, 1.5, 0.0025)
				ADD_WIDGET_FLOAT_SLIDER("Ring 2 Radius", fTgtRing2Radius, 0.0, 1.5, 0.0025)
				ADD_WIDGET_FLOAT_SLIDER("Ring 3 Radius", fTgtRing3Radius, 0.0, 1.5, 0.0025)
			STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(ShootingRangeWidgets)
		
		// Create the target in front of the player. Freeze it there.
		vTargetPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.5, 0.75, 0.5>>)
		oDebugTarget = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT(PROP_TARGET_COMP_WOOD, vTargetPos))
		FREEZE_ENTITY_POSITION(oDebugTarget, TRUE)
		
		// Just in case the target has moved...
		vTargetPos = GET_ENTITY_COORDS(oDebugTarget)
	ELSE
		// Are we to be rendering the target zones?
		IF (bDrawTargetZones)
			DRAW_DEBUG_SPHERE(vTargetPos, fTgtBullseyeRadius, 255, 0, 0, 128)
			DRAW_DEBUG_SPHERE(vTargetPos, fTgtRing1Radius, 0, 255, 0, 128)
			DRAW_DEBUG_SPHERE(vTargetPos, fTgtRing2Radius, 0, 0, 255, 128)
			DRAW_DEBUG_SPHERE(vTargetPos, fTgtRing3Radius, 92, 92, 92, 128)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Handles all of the shooting range widgets.
FUNC BOOL UPDATE_RANGE_WIDGETS(Range_MenuData &sMenuInfo, RANGE_STATE eState)
eState = eState
	//IF (eState = SRM_SCORE)	// This gives us the widgets when the scorecard come up.
		IF (ShootingRangeWidgets = NULL) AND sMenuInfo.uiPlacement.bHudScreenInitialised
			// Set up the group.
			ShootingRangeWidgets = START_WIDGET_GROUP("Shooting Range")
				ADD_WIDGET_FLOAT_SLIDER("Target Rotate Coeff Increase", fRotateCoeffInc, 0.01, 1.0, 0.001)
				ADD_WIDGET_BOOL("Draw Target", bDrawTarget)
				ADD_WIDGET_BOOL("PRINT_RANGE_MEDAL_VALUES", PRINT_RANGE_MEDAL_VALUES)
				ADD_WIDGET_FLOAT_SLIDER("Menu Output X", k_MENU_DEBUG_OUTPUT_X, -10, 10, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Menu Output Y", k_MENU_DEBUG_OUTPUT_Y, -10, 10, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Menu Output Spacing", k_MENU_DEBUG_OUTPUT_SPACING, -10, 10, 0.001)
				ADD_WIDGET_INT_SLIDER("Shotgun hit buffer (ms)", k_RANGE_TARGET_HIT_BUFFER, 0, 10000, 10 )
			//	ADD_WIDGET_FLOAT_SLIDER("Lock scale amt", fScaleWidget, 0.001, 1.0, 0.001)
			STOP_WIDGET_GROUP()
			
			// Can't submit with this. It causes A-Bug asserts.
			#IF IS_DEBUG_BUILD
				#IF COMPILE_WIDGET_OUTPUT
					CREATE_MEGA_PLACEMENT_WIDGETS(sMenuInfo.uiPlacement, ShootingRangeWidgets)
				#ENDIF
			#ENDIF
		ELSE
			// If we're debugging the target, update that.
			IF bDrawTarget
				UPDATE_TARGET_WIDGET()
				RETURN TRUE
			ELSE
				IF DOES_WIDGET_GROUP_EXIST(targetWidgets)
					DELETE_ENTITY(oDebugTarget)
					DELETE_WIDGET_GROUP(targetWidgets)
				ENDIF
				targetWidgets = NULL
			ENDIF
		ENDIF
	//ENDIF
	
	IF IS_DEBUG_KEY_JUST_RELEASED( KEY_1, KEYBOARD_MODIFIER_NONE, "printing medals" )
		INT iPed = 0
		INT iRound = 0
		
		FOR iPed = 0 TO 2
			
			FOR iRound = 0 TO ENUM_TO_INT( RT_MAX_ROUND_TYPES ) - 1
				CPRINTLN( DEBUG_SHOOTRANGE, "PlayerPed_", iped, " has ", GET_STRING_FROM_RANGE_ROUND_MEDAL( g_savedGlobals.sRangeData[iPed].sRounds[iRound].eTopMedal ), " in ", GET_STRING_FROM_RANGE_ROUND_TYPE( INT_TO_ENUM( RANGE_ROUND_TYPE, iRound ) ) )
			ENDFOR
			
			CPRINTLN(DEBUG_SHOOTRANGE, "Gold Discount: ", PICK_STRING( IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sRangeData[iPed].iBools, SRB_ShownFloatingHelp_ShopDiscountGold), "unlocked", "locked" ) )
			CPRINTLN(DEBUG_SHOOTRANGE, "Silver Discount: ", PICK_STRING( IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sRangeData[iPed].iBools, SRB_ShownFloatingHelp_ShopDiscountSilver), "unlocked", "locked" ) )
			CPRINTLN(DEBUG_SHOOTRANGE, "Bronze Discount: ", PICK_STRING( IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sRangeData[iPed].iBools, SRB_ShownFloatingHelp_ShopDiscountBronze), "unlocked", "locked" ) )
		
		ENDFOR
		
	ENDIF
	
	IF PRINT_RANGE_MEDAL_VALUES
		INT iPed = 0
		INT iRound = 0
		TEXT_LABEL_63 texDisplay
		FLOAT fDisplayX, fDisplayY
		FOR iPed = 0 TO 2
			fDisplayX = 0.1 + ( iPed * 0.25 )
			FOR iRound = 0 TO ENUM_TO_INT( RT_MAX_ROUND_TYPES ) - 1
				fDisplayY = 0.2 + ( iRound * 0.0125 )
				texDisplay = "ped_"
				texDisplay += iPed
				texDisplay += " has "
				texDisplay += GET_STRING_FROM_RANGE_ROUND_MEDAL( g_savedGlobals.sRangeData[iPed].sRounds[iRound].eTopMedal )
				texDisplay += " in "
				texDisplay += GET_STRING_FROM_RANGE_ROUND_TYPE( INT_TO_ENUM( RANGE_ROUND_TYPE, iRound ) )
				DRAW_DEBUG_TEXT_2D( texDisplay, (<< fDisplayX, fDisplayY, 0.0>>) )
			ENDFOR
			
			fDisplayY += 0.0125		
			texDisplay = "Gold Disount: "
			texDisplay += PICK_STRING( IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sRangeData[iPed].iBools, SRB_ShownFloatingHelp_ShopDiscountGold), "unlocked", "locked" )
			DRAW_DEBUG_TEXT_2D( texDisplay, (<< fDisplayX, fDisplayY, 0.0>>) )
			
			fDisplayY += 0.0125		
			texDisplay = "Silver Disount: "
			texDisplay += PICK_STRING( IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sRangeData[iPed].iBools, SRB_ShownFloatingHelp_ShopDiscountSilver), "unlocked", "locked" )
			DRAW_DEBUG_TEXT_2D( texDisplay, (<< fDisplayX, fDisplayY, 0.0>>) )
			
			fDisplayY += 0.0125		
			texDisplay = "Bronze Disount: "
			texDisplay += PICK_STRING( IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sRangeData[iPed].iBools, SRB_ShownFloatingHelp_ShopDiscountBronze), "unlocked", "locked" )
			DRAW_DEBUG_TEXT_2D( texDisplay, (<< fDisplayX, fDisplayY, 0.0>>) )
		
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Destroy the widgets and everything that they create.
PROC CLEANUP_RANGE_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(targetWidgets)
		DELETE_ENTITY(oDebugTarget)
		DELETE_WIDGET_GROUP(targetWidgets)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(ShootingRangeWidgets)
		DELETE_WIDGET_GROUP(ShootingRangeWidgets)
	ENDIF			
	
	targetWidgets = NULL
	ShootingRangeWidgets = NULL
ENDPROC
