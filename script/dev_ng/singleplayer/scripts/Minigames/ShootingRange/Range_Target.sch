// Range_Target.sch
CONST_INT		iMAX_ENTITIES				50
	
CONST_FLOAT		CONST_TARGET_HP				100.0
CONST_FLOAT		CONST_ROT_TIME_ENTER		0.6		// How long it should take to enter
CONST_FLOAT		CONST_ROT_TIME_EXIT			0.5		// How long it should take to exit
CONST_FLOAT		CONST_FLIP_TIME				0.5		// How long it should take to flip the target
CONST_FLOAT		CONST_CENTER_Z_OFFSET		1.155

CONST_INT		CONST_SHOTTY_2TARGS			3
CONST_INT		CONST_SHOTTY_3TARGS			5
CONST_INT		CONST_SHOTTY_4TARGS			7
CONST_INT		CONST_SHOTTY_5TARGS			7
CONST_INT		CONST_SHOTTY_6TARGS			7

CONST_INT		CONST_MINI_3RD_ROW			2
CONST_INT		CONST_MINI_4TH_ROW			4

CONST_INT		CONST_ZONE_1_REWARD			100			
CONST_INT		CONST_ZONE_2_REWARD			50
CONST_INT		CONST_ZONE_3_REWARD			25
CONST_INT		CONST_ZONE_4_REWARD			10

CONST_INT		CONST_TARG_DESTRUCT			100
CONST_INT		CONST_REWARD_HIT1			25		
CONST_INT		CONST_REWARD_HIT2			10
CONST_INT		CONST_REWARD_HIT3			5
CONST_INT		CONST_REWARD_HIT_ELSE		1

// Used for Moving targets
CONST_FLOAT		MOVE_VERY_SLOW_SPEED		6.0			// 6 seconds to destination
CONST_FLOAT		MOVE_SLOW_SPEED				5.0			// 5 seconds to destination
CONST_FLOAT		MOVE_NORMAL_SPEED			4.0			// 4 seconds to destination
CONST_FLOAT		MOVE_FAST_SPEED				3.0			// 3 seconds to destination
CONST_FLOAT		MOVE_VERY_FAST_SPEED		2.0			// 2 seconds to destination
CONST_FLOAT		MOVE_LUDICROUS_SPEED		1.0			// 1 second to destination

// Used to time respawns
CONST_FLOAT 	TARGET_RESPAWN_SHORT		1.0
CONST_FLOAT 	TARGET_RESPAWN_NORMAL		1.75
CONST_FLOAT 	TARGET_RESPAWN_LONG			3.5

TWEAK_FLOAT	fTgtBullseyeRadius 		0.075
TWEAK_FLOAT	fTgtRing1Radius 		0.145
TWEAK_FLOAT	fTgtRing2Radius 		0.215
TWEAK_FLOAT	fTgtRing3Radius 		0.285

// IF YOU MESS WITH THE ABOVE, YOU MUST FIX THESE.
TWEAK_FLOAT	fTgtBullseyeRadius_SQ 	0.005625
TWEAK_FLOAT	fTgtRing1Radius_SQ 		0.021025
TWEAK_FLOAT	fTgtRing2Radius_SQ 		0.046225
TWEAK_FLOAT	fTgtRing3Radius_SQ 		0.081225

TWEAK_FLOAT fRotateCoeffInc			0.32

CONST_FLOAT		WOBBLE_COEFF_MAX	4.0
CONST_FLOAT		WOBBLE_COEFF_MIN	1.0

CONST_FLOAT 	WOBBLE_COEFF_DONE		0.5
CONST_FLOAT		WOBBLE_COEFF_DEC_MAX	0.15
CONST_FLOAT		WOBBLE_COEFF_DEC_MIN	0.05

ENUM TARGET_MOVE_BEHAVIOR
	TMB_STATIC,						// Static target, does'nt flip
	TMB_STATIC_FLIP,				// Static target, flips after specified time
	TMB_NULL
ENDENUM

ENUM TARGET_STATUS
	TS_NO_STATUS		= BIT0,
	TS_MOVING			= BIT1,
	TS_FLIPPED 			= BIT2,
	TS_FLIPPING			= BIT3,
	TS_ENTERING			= BIT4,
	TS_EXITING			= BIT5,
	TS_ASSIGNED			= BIT6,
	TS_CREATED 			= BIT7,
	TS_DESTROYED		= BIT8,
	
	TS_WOBBLING = BIT10,
	
	// Flags to set on creation.
	TS_DONT_PLAY_ENTRY_SOUND = BIT11,
	TS_DONT_PLAY_SLIDE_SOUND = BIT12,
	
	TS_RESPAWNS 		= BIT20,
	TS_RESPAWN_SHORT 	= BIT21,
	TS_RESPAWN_LONG 	= BIT22,
	
	TS_GIVE_FLOAT_HELP = BIT23,
	
	TS_HEIGHT_RANDOM = BIT27,
	TS_HEIGHT_MID 	= BIT28,
	TS_HEIGHT_HIGH 	= BIT29,
	TS_HEIGHT_LOW 	= BIT30
ENDENUM

// The actual physical target
STRUCT MODERN_TARGET
	VECTOR 					vPos			// The postion of the target, all models share this
	VECTOR					vCenter			// The center of the bullseye
	VECTOR					vRot			// The rotation of the target
	FLOAT 					fHeading		// The orientation of the BULLSEYE
	
	OBJECT_INDEX 			objArm			// The object handle of the targets arm
	OBJECT_INDEX 			objBacking		// The object handle of the targets backing
	OBJECT_INDEX 			objTarget		// The object handle of the targets
ENDSTRUCT

// Different behaviors these actions can do
ENUM ACTION_BEHAVIOR
	AB_MOVE,
	AB_MOVE_AT_TIME_VERY_SLOW,
	AB_MOVE_AT_TIME_SLOW,
	AB_MOVE_AT_TIME_NORMAL,
	AB_MOVE_AT_TIME_FAST,
	AB_MOVE_AT_TIME_VERYFAST,
	AB_MOVE_AT_TIME_LUDICROUS,
	AB_FLIP,
	AB_DESTROY_WHEN_AT_LOC,
	AB_DESTROY_AT_TIME, 
	AB_DESTROY_AFTER_TIME_CREATED,
	AB_CREATE_AT_TIME,
	AB_INVALID
ENDENUM

// Action Struct
STRUCT TARGET_ACTION
	ACTION_BEHAVIOR	eBehavior = AB_INVALID
	FLOAT			fTime
	BOOL			bPerformed
	INT				iTargetIndex
	GRID_INDEX		eDestIndex				// Used for move commands
ENDSTRUCT

/// PURPOSE: 
STRUCT TARGET_ENTITY
	GRID_INDEX 			eGridIndex = INVALID_LOC	// Which grid this entity is in
	GRID_INDEX			eMoveDest = INVALID_LOC		// When we are moving, this is where we will move too
	MODERN_TARGET		target						// The actual physical target
	
	INT					iCreateNumber				// The amount of targets that need to be destroyed before it will be created
	INT					iStatus						// Bitfield used to check our status
	INT					iMovingSound = -1				// Sound ID to store the moving sound.
	
	FLOAT				fTargCreateTime = -1.0		// What time we were created
	FLOAT				fHp							// The targets HP
	FLOAT				fMoveLength					// How long when moving, should we take
	FLOAT				fRotFrameTime				// Used for rotating the arm down
	FLOAT				fWobbleFrameTime			// Used to wobble the target.
	FLOAT				fFlipFrameTime				// Used for flipping the bullseye around
	FLOAT				fMoveFrame
	FLOAT				fWobbleCoeff				// Used to make the target wobble on entry.
	FLOAT				fWobbleChange
	
	FLOAT				fRespawnAtTime = -1.0		// Used for respawning targets.
	FLOAT				fRotateCoeff				// USed to accelerate the rortate in as a target enters.
		
	VECTOR				vMoveStart					// Where our old start position was
	
	BOOL				bHitThisRound = FALSE
	
ENDSTRUCT

STRUCT Range_Targets
	TARGET_ENTITY		sTargetEntities[iMAX_ENTITIES]
	TARGET_ACTION 		sQueuedActions[iMAX_ACTIONS]
	
	INT 				iActionIndex
ENDSTRUCT



/// PURPOSE:
///    Checks a target for any combination of statuses, if they are set, then we return true
/// PARAMS:
///    entityToCheck - Which targets status we want to check
///    checkStatus - Which status we want to check
/// RETURNS:
///    TRUE if the status exists on the target.
FUNC BOOL CHECK_TARGET_STATUS(TARGET_ENTITY entityToCheck, TARGET_STATUS checkStatus)
	RETURN IS_BITMASK_AS_ENUM_SET(entityToCheck.iStatus, checkStatus)
ENDFUNC


/// PURPOSE:
///    Sets a specific status to an entity
/// PARAMS:
///    entityToSet - The target we want to set the status on
///    statusToSet - What status do we we want to set
PROC ADD_TARGET_STATUS(TARGET_ENTITY &entityToSet, TARGET_STATUS statusToSet)	
	SET_BITMASK_AS_ENUM(entityToSet.iStatus, statusToSet)
ENDPROC


/// PURPOSE:
///    Removes a specific status from a target
/// PARAMS:
///    entity - The entity that we want to remove the status from
///    statusToRem - Which status do we want to remove
PROC REMOVE_TARGET_STATUS(TARGET_ENTITY &entity, TARGET_STATUS statusToRem)
	CLEAR_BITMASK_AS_ENUM(entity.iStatus, statusToRem)
ENDPROC


/// PURPOSE:
///    Checks to see if an entity is fully entered (created and all the way down)
/// PARAMS:
///    entity - The entity we want to check
/// RETURNS:
///    TRUE once fully down and shootable
FUNC BOOL IS_TARGET_ENTERED(TARGET_ENTITY entity)
	RETURN (NOT CHECK_TARGET_STATUS(entity, TS_ENTERING) AND CHECK_TARGET_STATUS(entity, TS_CREATED))
ENDFUNC


/// PURPOSE:
///    Cleans up a target
/// PARAMS:
///    stTarget - The target we want to clean up
PROC DESTROY_TARGET(MODERN_TARGET & stTarget)
	IF DOES_ENTITY_EXIST(stTarget.objTarget)
		DELETE_OBJECT(stTarget.objArm)
		DELETE_OBJECT(stTarget.objTarget)
	ENDIF
	IF DOES_ENTITY_EXIST(stTarget.objBacking)
		DELETE_OBJECT(stTarget.objBacking)
	ENDIF
ENDPROC


/// PURPOSE:
///    Destroys the target at the passed in grid location.. if there is one
/// PARAMS:
///    eGridLoc - The targets grid location that we want to destroy
FUNC BOOL DESTROY_TARGET_ENTITY(TARGET_ENTITY & tEntity, GRID_LOCATION & sGridData[])
	// Destroy the target
	DESTROY_TARGET(tEntity.target)
	
	// Mark this grid as un occupided - eGridIndex is -1 for some reason...
	IF (tEntity.eGridIndex <> INVALID_LOC)
		sGridData[tEntity.eGridIndex].bOccupied = FALSE
	ENDIF
	
	tEntity.fTargCreateTime = -1.0
	tEntity.iStatus = 0
	ADD_TARGET_STATUS(tEntity, TS_DESTROYED)
	ADD_TARGET_STATUS(tEntity, TS_CREATED)
	tEntity.fHp	= CONST_TARGET_HP
	tEntity.fRespawnAtTime = -1.0
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    General check to see if a target is Entering, Moving, Exiting, Rotating, Flipping....
/// RETURNS:
///    TRUE if any of these are true
FUNC BOOL ARE_ANY_TARGET_ENTITIES_MOVING(Range_RoundInfo & sRndInfo, TARGET_ENTITY & sTargetEnts[])
	INT iTer
	
	REPEAT sRndInfo.iMaxTargets iTer
		IF CHECK_TARGET_STATUS(sTargetEnts[iTer], TS_MOVING)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "AreAnyTargetsMobile -- Entity #", iTer, " is still moving..")
			RETURN TRUE
		ENDIF
		
		IF CHECK_TARGET_STATUS(sTargetEnts[iTer], TS_ENTERING)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "AreAnyTargetsMobile -- Entity #", iTer, " is still entering..")
			RETURN TRUE
		ENDIF
		
		IF CHECK_TARGET_STATUS(sTargetEnts[iTer], TS_EXITING)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "AreAnyTargetsMobile -- Entity #", iTer, " is still exiting..")
			RETURN TRUE
		ENDIF
		
		IF CHECK_TARGET_STATUS(sTargetEnts[iTer], TS_FLIPPING)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "AreAnyTargetsMobile -- Entity #", iTer, " is still flipping..")
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC	


