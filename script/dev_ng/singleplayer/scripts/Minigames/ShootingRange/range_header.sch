USING "script_maths.sch"

CONST_INT		iMAX_ACTIONS				50
CONST_INT		iMAX_MOVEABLE_TARGS			3

CONST_INT		CONST_MAX_MULTIP			3
CONST_INT		iMULTIPLIER_BLOCKS			4

// Ammo given values
CONST_INT		CONST_FLAT_AMMO				300


CONST_FLOAT		CONST_SPT_LENGTH			0.75
CONST_FLOAT		CONST_MP_CD					3.5			// How long until the multiplier resets

/// PURPOSE: How many challenges we have.
ENUM RANGE_WEAPON_CHALLENGES
	WEAPCHAL_1 = 0,
	WEAPCHAL_2,
	WEAPCHAL_3,
	WEAPCHAL_4,
	NUM_WEAPON_CHALLS
ENDENUM

ENUM RANGE_STATE
	SRM_INIT,
//	SRM_LOAD_AREA_TELE,
	SRM_LOAD_AREA_INTRO,
	SRM_INIT_DONE,
	SRM_TUTORIAL,
	SRM_NON_TUTORIAL_INTRO,	
	SRM_PICK_WEAPON,
	SRM_WAIT_FOR_START,
	SRM_COUNT_DWN_INIT,
	SRM_COUNT_DWN,
	SRM_SHOOT,
	SRM_COMPLETE_MSG,
	SRM_MEDAL_TOAST,
	SRM_READ_SCLB,
	SRM_EXIT,
	SRM_EXIT_CONF,
	SRM_START_EXIT_CUTSCENE,
	
	SRM_TRIGGER_EXIT_REDRESS,
	SRM_REDRESS_STAGE_2,
	SRM_REDRESS_EXIT
ENDENUM

#IF IS_DEBUG_BUILD
FUNC STRING GET_STRING_FROM_RANGE_STATE(RANGE_STATE eState)
	SWITCH eState
		CASE SRM_INIT                 RETURN "SRM_INIT"
		CASE SRM_LOAD_AREA_INTRO      RETURN "SRM_LOAD_AREA_INTRO"
		CASE SRM_INIT_DONE            RETURN "SRM_INIT_DONE"
		CASE SRM_TUTORIAL             RETURN "SRM_TUTORIAL"
		CASE SRM_NON_TUTORIAL_INTRO   RETURN "SRM_NON_TUTORIAL_INTRO"	
		CASE SRM_PICK_WEAPON          RETURN "SRM_PICK_WEAPON"
		CASE SRM_WAIT_FOR_START       RETURN "SRM_WAIT_FOR_START"
		CASE SRM_COUNT_DWN_INIT       RETURN "SRM_COUNT_DWN_INIT"
		CASE SRM_COUNT_DWN            RETURN "SRM_COUNT_DWN"
		CASE SRM_SHOOT                RETURN "SRM_SHOOT"
		CASE SRM_COMPLETE_MSG         RETURN "SRM_COMPLETE_MSG"
		CASE SRM_MEDAL_TOAST          RETURN "SRM_MEDAL_TOAST"
		CASE SRM_READ_SCLB            RETURN "SRM_READ_SCLB"
		CASE SRM_EXIT                 RETURN "SRM_EXIT"
		CASE SRM_EXIT_CONF            RETURN "SRM_EXIT_CONF"
		CASE SRM_START_EXIT_CUTSCENE  RETURN "SRM_START_EXIT_CUTSCENE"
		CASE SRM_TRIGGER_EXIT_REDRESS RETURN "SRM_TRIGGER_EXIT_REDRESS"
		CASE SRM_REDRESS_STAGE_2      RETURN "SRM_REDRESS_STAGE_2"
		CASE SRM_REDRESS_EXIT         RETURN "SRM_REDRESS_EXIT"
	ENDSWITCH
	RETURN "unknown RANGE_STATE"
ENDFUNC
#ENDIF

ENUM THREE_STATE_RETVAL
	RETVAL_NONE = 0,
	RETVAL_TRUE,
	RETVAL_FALSE
ENDENUM

STRUCT RANGE_LBD_WRITE_DATA
	RANGE_ROUND_TYPE eRoundType = RT_INVALID
	INT iShotsFired
	INT iShotsHit
	RANGE_ROUND_MEDAL eMedal	
ENDSTRUCT

USING "Range_Support_lib.sch"
USING "Range_Range.sch"
USING "Range_Round.sch"
USING "Range_Target_lib.sch"
USING "Range_Score.sch"



