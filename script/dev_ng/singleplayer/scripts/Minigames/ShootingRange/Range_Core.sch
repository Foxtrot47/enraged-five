// Range_Core.sch

CONST_INT MAX_ASSETS 	10

USING "Range_Leaderboard_lib.sch"

STRUCT Range_CoreData
	// Intro info.
	BOOL 			bSawIntro
	BOOL 			bFailed
	INT 			iIntroShot
	
	BOOL 			bTriggeredOutro = FALSE
	
	// State Data.
	RANGE_STATE 	eRangeState = SRM_INIT
	RANGE_STATE		ePrevState
	
	// Forced update info.
	INT				iAudioStreamTimeout
	BOOL 			bNeedMenuReinit
	BOOL 			bSkipPressed
	BOOL 			bWarnedAboutLeaving
	
	BOOL			bRestoredWeapons = FALSE
	
	BOOL 			bDelayShutdown
	
	// DLC Unlocks
	BOOL 			bPistolDLC
	BOOL 			bSMGDLC
	BOOL 			bARDLC
	BOOL 			bShotgunDLC
	BOOL 			bLMGDLC
	
	BOOL			bCGtoNG = FALSE

	// Cheats.
	BOOL 			bCheat_UnlockAllWeapons
	
	// Other.
	BOOL			bKickedOffInRange = FALSE
	
	INTERIOR_INSTANCE_INDEX	RangeInterior = NULL
	
	OBJECT_INDEX oEarmuffs
	
	INT 			iEarsRemoveStart
	INT 			iEarsRemoveEnd
	INT				iAnimStartTime
	
	INT 			iStartHeadPropIdx = -1
	INT 			iStartFacePropIdx = -1
	
	STREAMED_MODEL 	rangeAssets[MAX_ASSETS]
	
	RANGE_LOCATION	eRangeLoc
	PED_WEAPONS_STRUCT sWeapons
	
	RANGE_LBD_WRITE_DATA lbdWrite
	BOOL ProcessPrediction = FALSE
	BOOL bOnlineForPrediction = TRUE
	RANGE_ROUND_TYPE ePredictedRnd = RT_INVALID
ENDSTRUCT



/// PURPOSE:
///    Unlocks all weapons in the range.
PROC CHEAT_UNLOCK_ALL_WEAPONS(Range_CoreData & sCore)
	#IF IS_DEBUG_BUILD
		sCore.bCheat_UnlockAllWeapons = TRUE
		sCore.bNeedMenuReinit = TRUE
	#ENDIF
	
	// Prevent compile errors in release.
	sCore.bCheat_UnlockAllWeapons = sCore.bCheat_UnlockAllWeapons
ENDPROC

/// PURPOSE:
///    Lets us know if all the rounds have been unlocked due to a cheat.
/// RETURNS:
///    TRUE if we've cheated. FALSE in release/if we haven't.
FUNC BOOL CHEAT_ARE_ALL_ROUNDS_UNLOCKED(Range_CoreData & sCore)
	#IF IS_DEBUG_BUILD
		RETURN sCore.bCheat_UnlockAllWeapons
	#ENDIF
	
	// Prevent compile errors in release.
	sCore.bCheat_UnlockAllWeapons = sCore.bCheat_UnlockAllWeapons
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Accessor
PROC RANGE_SET_HAS_PLAYER_KICKED_OFF_IN_RANGE( Range_CoreData & sCoreInfo, BOOL bKickedOff )
	CDEBUG2LN( DEBUG_SHOOTRANGE, "RANGE_SET_HAS_PLAYER_KICKED_OFF_IN_RANGE setting to ", PICK_STRING( bKickedOff, "TRUE", "FALSE" ) )
	sCoreInfo.bKickedOffInRange = bKickedOff
ENDPROC

/// PURPOSE:
///    Accessor
FUNC BOOL RANGE_GET_HAS_PLAYER_KICKED_OFF_IN_RANGE( Range_CoreData & sCoreInfo )
	RETURN sCoreInfo.bKickedOffInRange
ENDFUNC












