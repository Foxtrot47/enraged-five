USING "golf_clubs.sch"
USING "golf_course.sch"
USING "golf_foursome.sch"
//USING "golf_helpers.sch"
USING "golf_ui.sch"

FUNC OBJECT_INDEX CREATE_GOLF_CLUB(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_BAG &golfBag)
	OBJECT_INDEX newClub
	
	#IF IS_DEBUG_BUILD
		PRINTLN("Making a nwe golf club")
	//	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	CLEANUP_GOLF_CLUB(thisPlayerCore)
	newClub = CREATE_OBJECT_NO_OFFSET(GET_GOLF_CLUB_MODEL(golfBag, GET_GOLF_PLAYER_CURRENT_CLUB(thisPlayerCore)), GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore), FALSE)
	RETURN newClub
ENDFUNC

PROC GOLF_DETACH_CLUB(GOLF_FOURSOME &thisFoursome, INT iPlayerIndex)

	OBJECT_INDEX objClub = GET_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(thisFoursome, iPlayerIndex)
	
	IF DOES_ENTITY_EXIST(objClub)
		IF IS_ENTITY_ATTACHED(objClub)
			CDEBUG1LN(DEBUG_GOLF,"Detaching club for the player ", iPlayerIndex)
			DETACH_ENTITY(objClub)
		ENDIF
	ENDIF
	
ENDPROC

PROC DEBUG_DRAW_RANGE_OF_CLUBS(GOLF_FOURSOME &thisFoursome, GOLF_GAME &thisGame)

 	eCLUB_TYPE eMinClubType, eMaxClubType
 	FLOAT clubEffectiveRange = 0
 	eMinClubType = GET_GOLF_CLUB_TYPE( thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
	eMaxClubType = GET_GOLF_CLUB_TYPE( thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
	FLOAT fSwingHeading = GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome)
	VECTOR vPlayerAim = NORMALISE_VECTOR( <<COS(fSwingHeading), SIN(fSwingHeading), 0>>)
	INT clubIndex
	VECTOR vDrawPos
	
	REPEAT COUNT_OF(thisGame.golfBag.clubs) clubIndex
		eCLUB_TYPE indexType = GET_GOLF_CLUB_TYPE(thisGame.golfBag, clubIndex)
		IF indexType >= eMinClubType
		AND indexType <= eMaxClubType
			clubEffectiveRange = GET_GOLF_CLUB_PREDITED_CLUB_RANGE(thisGame.golfBag, clubIndex)
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_APPROACH OR GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_PUNCH
				clubEffectiveRange *= GET_APPROACH_SHOT_PREDICTION_MODFIER()
			ENDIF

			vDrawPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome) - vPlayerAim*clubEffectiveRange
			GET_GROUND_Z_FOR_3D_COORD(vDrawPos+<<0,0,50>>, vDrawPos.z)
			
			DRAW_DEBUG_SPHERE(vDrawPos, 0.25)
			
		ENDIF
	ENDREPEAT

ENDPROC

PROC GOLF_GET_CLUB_RANGE_FOR_SITUATION(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, eCLUB_TYPE &eMinClubType, eCLUB_TYPE &eMaxClubType, BOOL bAllowPutterWhenNotOnGreen)
	IF IS_BALL_ON_GREEN(thisCourse, thisFoursome) // Player limited to putter
		eMinClubType = eCLUB_WEDGE_PITCH
		eMaxClubType = eCLUB_PUTTER
	ELIF IS_BALL_IN_BUNKER(thisFoursome) // Player limited to iron/wedges
		eMinClubType = eCLUB_IRON_3
		eMaxClubType = eCLUB_WEDGE_LOB
	ELIF IS_BALL_IN_RANGE_OF_GREEN(thisCourse, thisFoursome) // Player limited to iron/wedges
		eMinClubType = eCLUB_IRON_3
		
		IF bAllowPutterWhenNotOnGreen
			eMaxClubType = eCLUB_PUTTER
		ELSE
			eMaxClubType = eCLUB_WEDGE_LOB
		ENDIF
	ELIF IS_BALL_IN_ROUGH(thisFoursome) // Player limited to iron/wedges
		eMinClubType = eCLUB_IRON_3
		eMaxClubType = eCLUB_WEDGE_LOB
	ELIF IS_TEE_SHOT(thisCourse, thisFoursome) // Limited to all but putter
		eMinClubType = eCLUB_WOOD_1
		eMaxClubType = eCLUB_WEDGE_LOB
	ELIF IS_BALL_ON_FAIRWAY(thisFoursome) // Limited to all woods but driver, irons, wedges
		eMinClubType = eCLUB_WOOD_3
		eMaxClubType = eCLUB_WEDGE_LOB
	ELSE // Unknown situation, Player limited to iron sand wedges
		eMinClubType = eCLUB_IRON_3
		eMaxClubType = eCLUB_WEDGE_LOB
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_ON_PUTTING_GREEN()
	#IF PUTTING_GREEN
		IF bDebugSpew	DEBUG_MESSAGE("GOLF: This player is on the putting green. Should be forcing putter")	ENDIF
		RETURN TRUE
	#ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Picks the ideal club and shot for a given ball location on a whole. 
/// PARAMS:
///    thisGame - 
///    thisCourse - 
///    thisFoursome - 
PROC PICK_IDEAL_CLUB_AND_SHOT( GOLF_GAME &thisGame,GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome #IF NOT GOLF_IS_AI_ONLY , GOLF_HELPERS &thisHelpers #ENDIF, BOOL bUseEntityPos = TRUE )
	FLOAT holeLength = GET_GOLF_FOURSOME_CURRENT_PLAYER_DISTANCE_TO_HOLE(thisFoursome)
	INT clubIndex, nearestClub
	FLOAT nearestClubDist = -9999
	FLOAT clubEffectiveRange = -1

	IF IS_BALL_ON_GREEN(thisCourse, thisFoursome) // Player limited to putter
	OR IS_PLAYER_ON_PUTTING_GREEN()
		IF IS_SHOT_A_GIMME(thisCourse, thisFoursome)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, SWING_STYLE_GREEN_TAP)
		ELIF holeLength < 5.0
			SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, SWING_STYLE_GREEN_SHORT)
		ELIF holeLength < 9.5
			SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, SWING_STYLE_GREEN)
		ELSE
			SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, SWING_STYLE_GREEN_LONG)
		ENDIF
		SET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome, GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eCLUB_PUTTER))
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome) != -1
			EXIT
		ENDIF
	ELIF IS_BALL_IN_BUNKER(thisFoursome) // Player limited to sand wedge
		SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, SWING_STYLE_NORMAL)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome, GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eCLUB_WEDGE_SAND))
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome) != -1
			EXIT
		ENDIF
	ELIF IS_BALL_IN_RANGE_OF_GREEN(thisCourse, thisFoursome, bUseEntityPos) // Player limited to wedges
		SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, SWING_STYLE_APPROACH)
		#IF NOT GOLF_IS_AI_ONLY 
			GOLF_PRINT_CHANGED_SHOT_TYPE_HELP(thisGame, thisFoursome, thisHelpers, FALSE)
		#ENDIF
	ELSE
		SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, SWING_STYLE_NORMAL)
	ENDIF
	eCLUB_TYPE eMinClubType, eMaxClubType
	GOLF_GET_CLUB_RANGE_FOR_SITUATION(thisCourse, thisFoursome, eMinClubType, eMaxClubType, FALSE)

	// If our biggest usable club is still short of the hole, use it
//	clubEffectiveRange = GET_GOLF_CLUB_PREDITED_CLUB_RANGE(thisGame.golfBag, GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eMinClubType))
//	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_APPROACH OR GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_PUNCH
//		clubEffectiveRange *= GET_APPROACH_SHOT_PREDICTION_MODFIER()
//	ENDIF
//	IF clubEffectiveRange < holeLength
//		SET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome, GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eMinClubType))
//		EXIT
//	ENDIF

	// If our shortest usable club is long for the hole, use it
	clubEffectiveRange = GET_GOLF_CLUB_PREDITED_CLUB_RANGE(thisGame.golfBag, GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eMaxClubType))
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_APPROACH OR GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_PUNCH
		clubEffectiveRange *= GET_APPROACH_SHOT_PREDICTION_MODFIER()
	ENDIF
	IF clubEffectiveRange > holeLength
		SET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome, GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eMaxClubType))
		EXIT
	ENDIF
	
	// Otherwise, lets find our longest usable club that is short of the hole
	nearestClub = -1
	REPEAT COUNT_OF(thisGame.golfBag.clubs) clubIndex
		eCLUB_TYPE indexType = GET_GOLF_CLUB_TYPE(thisGame.golfBag, clubIndex)
		IF indexType >= eMinClubType
		AND indexType <= eMaxClubType
			clubEffectiveRange = GET_GOLF_CLUB_PREDITED_CLUB_RANGE(thisGame.golfBag, clubIndex)
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_APPROACH OR GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_PUNCH
				clubEffectiveRange *= GET_APPROACH_SHOT_PREDICTION_MODFIER()
			ENDIF
			
			IF thisGame.golfBag.clubs[clubIndex].kClubType >= eCLUB_WEDGE_PITCH
				//Check if the wedges at 75% power is good enough, don't go to a lower wedge otherwise
				clubEffectiveRange *=0.75
			ENDIF
			
			IF 	IS_TEE_SHOT(thisCourse, thisFoursome) //Tee shots need tweaking to be perfect
				IF GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) = 2
					clubEffectiveRange *= 1.1
				ELIF GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) = 3
					clubEffectiveRange *= 2.25
				ENDIF
			ENDIF
			clubEffectiveRange -= holeLength
			IF clubEffectiveRange < 0 AND clubEffectiveRange > nearestClubDist
				nearestClub = clubIndex
				nearestClubDist = clubEffectiveRange
			ENDIF
		ENDIF
	ENDREPEAT
	IF nearestClub != -1
		SET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome, nearestClub)
		EXIT
	ENDIF
	SET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome, GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eMinClubType))
ENDPROC


//Attachs a club to a ped that aligns with the walking animation
PROC GOLF_ATTACH_CLUB_FOR_WALKING(GOLF_FOURSOME &thisFoursome, GOLF_GAME &thisGame)

	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome))
		eCLUB_TYPE clubType =  GET_GOLF_CLUB_TYPE( thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
		ATTACH_ENTITY_TO_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome),  GET_PED_BONE_INDEX( GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
		UNUSED_PARAMETER(clubType)
	ENDIF

ENDPROC

FUNC BOOL IS_GOLFER_PLAYING_IDLE_ANIM_FOR_CLUB(PED_INDEX pedGolfer, eCLUB_TYPE clubType)

	IF clubType >= eCLUB_WOOD_1 AND clubType <= eCLUB_WOOD_5
		RETURN IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), "wood_idle_a")
			OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE),  "wood_idle_b")
			OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE),  "wood_idle_c")
	ELIF clubType >= eCLUB_IRON_2 AND clubType <= eCLUB_IRON_9
		RETURN IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), "iron_idle_a")
			OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE),  "iron_idle_b")
			OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE),  "iron_idle_c")
	ELIF clubType >= eCLUB_WEDGE_PITCH AND clubType <= eCLUB_WEDGE_LOB_ULTRA
		RETURN IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), "wedge_idle_a")
			OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE),  "wedge_idle_b")
			OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE),  "wedge_idle_c")
	ELIF clubType = eCLUB_PUTTER
		RETURN IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), "putt_idle_a")
			OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE),  "putt_idle_b")
			OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE),  "putt_idle_c")	
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GOLF_ATTACH_CLUB_TO_PLAYER(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_BAG &golfBag, BOOL bAttachOnlyIfRecreate = FALSE, BOOL bCreateOnlyIfPlayingAnim = FALSE)
	eCLUB_TYPE clubType = GET_GOLF_CLUB_TYPE(golfBag,  GET_GOLF_PLAYER_CURRENT_CLUB(thisPlayerCore))
	MODEL_NAMES clubModel = GET_GOLF_CLUB_MODEL(golfBag,  GET_GOLF_PLAYER_CURRENT_CLUB(thisPlayerCore))
	
	IF bCreateOnlyIfPlayingAnim
		IF NOT IS_GOLFER_PLAYING_IDLE_ANIM_FOR_CLUB(GET_GOLF_PLAYER_PED(thisPlayerCore), clubType)
			EXIT // only make club if the player is playing the adress idle for that club
		ENDIF
	ENDIF
	
	BOOL bRecreateClub = TRUE
	
	IF DOES_ENTITY_EXIST(GET_GOLF_PLAYER_CLUB(thisPlayerCore))
	
		IF clubModel = GET_ENTITY_MODEL(GET_GOLF_PLAYER_CLUB(thisPlayerCore))
			bRecreateClub = FALSE //don't make club if it already exsist and is the correct model
		ENDIF
	ENDIF
	
	IF bRecreateClub
		SET_GOLF_PLAYER_CLUB(thisPlayerCore, CREATE_GOLF_CLUB(thisPlayerCore, golfBag))
	ENDIF
		
	IF NOT bAttachOnlyIfRecreate OR bRecreateClub
		PED_INDEX pedIndex = GET_GOLF_PLAYER_PED(thisPlayerCore)
		IF NOT IS_PED_INJURED(pedIndex)
			ATTACH_ENTITY_TO_ENTITY(GET_GOLF_PLAYER_CLUB(thisPlayerCore), pedIndex, GET_PED_BONE_INDEX( pedIndex, BONETAG_PH_R_HAND), GET_GOLF_CLUB_ATTACH_OFFSET(pedIndex, clubType), GET_GOLF_CLUB_ATTACH_ROTATION(pedIndex, clubType))
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_GOLF_CLUB_WEAPON(PED_INDEX pedIndex, BOOL bForceInHand, BOOL bUnequipClub = FALSE)
	SET_CURRENT_PED_WEAPON(pedIndex, WEAPONTYPE_UNARMED, bForceInHand)

	IF HAS_PED_GOT_WEAPON(pedIndex, WEAPONTYPE_GOLFCLUB) AND bUnequipClub
		REMOVE_WEAPON_FROM_PED(pedIndex, WEAPONTYPE_GOLFCLUB)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_GOLFER_HAVE_CLUB_WHEN_NAVIGATING(GOLF_FOURSOME &thisFoursome, INT iGolferIndex)

	PED_INDEX golferPed = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iGolferIndex)
//	VEHICLE_INDEX golferCart = GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(thisFoursome, iGolferIndex)
	OBJECT_INDEX golferClub = GET_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(thisFoursome, iGolferIndex)
	
	IF IS_PED_IN_ANY_VEHICLE(golferPed, TRUE)
	OR IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(golferPed)
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_IN_WATER(golferPed)
		RETURN FALSE
	ENDIF
	
	IF golferPed = PLAYER_PED_ID()
		RETURN TRUE
	ENDIF
		
	IF NOT DOES_ENTITY_EXIST(golferClub)
		IF  GET_ENTITY_SPEED(golferPed) != 0
		AND NOT IS_PED_WALKING(golferPed)
			RETURN FALSE //only create club when still
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

PROC GOLF_MONITOR_CLUB_WEAPONS(GOLF_FOURSOME &thisFoursome, GOLF_GAME &thisGame, BOOL bForceIntoHand)
	INT iGolferIndex
	
	WEAPON_TYPE weapon = WEAPONTYPE_GOLFCLUB
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iGolferIndex
	
		PED_INDEX pedIndex = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iGolferIndex)
		IF NOT IS_PED_INJURED(pedIndex)
			IF iGolferIndex != GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
			AND (IS_PED_RAGDOLL(pedIndex) OR IS_PED_RESPONDING_TO_EVENT(pedIndex, EVENT_PED_COLLISION_WITH_PLAYER))
				
				GOLF_DETACH_CLUB(thisFoursome, iGolferIndex)
					
			ELIF SHOULD_GOLFER_HAVE_CLUB_WHEN_NAVIGATING(thisFoursome, iGolferIndex)
				IF iGolferIndex = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
					IF HAS_PED_GOT_WEAPON(pedIndex, weapon)  
						IF GET_PEDS_CURRENT_WEAPON(pedIndex) != weapon
							CLEANUP_GOLF_CLUB(thisFoursome.playerCore[iGolferIndex])
							SET_CURRENT_PED_WEAPON(pedIndex, weapon, bForceIntoHand)
						ENDIF
					ELSE
						CLEANUP_GOLF_CLUB(thisFoursome.playerCore[iGolferIndex])
						GIVE_WEAPON_TO_PED(pedIndex, weapon, -1, bForceIntoHand, TRUE)
					ENDIF
				ELSE
					GOLF_ATTACH_CLUB_TO_PLAYER(thisFoursome.playerCore[iGolferIndex], thisGame.golfbag, TRUE)
				ENDIF
			ELSE
				IF iGolferIndex = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
					IF NOT HAS_PED_GOT_WEAPON(pedIndex, weapon)
						GIVE_WEAPON_TO_PED(pedIndex, weapon, -1, FALSE, FALSE)
					ELIF GET_PEDS_CURRENT_WEAPON(pedIndex) = weapon
						SET_CURRENT_PED_WEAPON(pedIndex, WEAPONTYPE_UNARMED, bForceIntoHand)	
					ENDIF
				ELSE
					CLEANUP_GOLF_CLUB(thisFoursome.playerCore[iGolferIndex])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC
