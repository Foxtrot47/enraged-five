USING "minigames_helpers.sch"

ENUM GOLF_LIE_TYPE
	LIE_UNKNOWN = -1,
	LIE_BUNKER,
	LIE_CART_PATH,
	LIE_FAIRWAY,
	LIE_GREEN,
	LIE_ROUGH,
	LIE_TEE,
	LIE_UNKNOWN_UI, //for comunicating with UI
	LIE_WATER,
	
	LIE_CUP,
	LIE_BUSH
ENDENUM

/// PURPOSE: Data representation of a single hole of golf
STRUCT GOLF_HOLE
#IF NOT COMPILE_LOOKUPS	
	VECTOR			vTeePosition
	VECTOR 			vTeeSpectate[4]
	VECTOR			vFairwayPosition
	VECTOR 			vHolePosition
	VECTOR			vGreenSpectate[4]
#ENDIF
	FLOAT			fHoleLength
	INT				iPar
	OBJECT_INDEX	objFlag
ENDSTRUCT

/// PURPOSE: Data representation of a single golf course
STRUCT GOLF_COURSE
	GOLF_HOLE	golfHole[9]
	INT			iNumHoles
	INT			iTotalPar
	VECTOR 		vPlayerStartPosition[4]
	VECTOR		vCartStartPosition[4]
	
	#IF NOT GOLF_IS_AI_ONLY
	BLIP_INDEX blipFlag
	#ENDIF
	
ENDSTRUCT


FUNC VECTOR GET_GOLF_HOLE_TEE_POSITION_STATIC(INT holeIndex)
	#IF NOT PUTTING_GREEN
		SWITCH holeIndex
			CASE 0
				RETURN <<-1370.93, 173.98, 57.01 >>
			BREAK
			CASE 1
				RETURN << -1107.26, 157.15, 62.04>>
			BREAK
			CASE 2
				RETURN  << -1312.97, 125.64, 56.39 >>
			BREAK
			CASE 3
				RETURN  << -1218.56, 107.48, 57.04 >>
			BREAK
			CASE 4
				RETURN << -1098.15, 69.50, 53.09 >> 
			BREAK
			CASE 5
				RETURN << -987.70, -105.42, 39.59 >>
			BREAK
			CASE 6
				RETURN << -1117.793, -104.069, 40.8406 >>
			BREAK
			CASE 7
				RETURN << -1272.63, 38.40, 48.75 >>
			BREAK
			CASE 8
				RETURN << -1138.38123, 0.60467, 47.98225>>
			BREAK
		ENDSWITCH
	#ENDIF
	#IF PUTTING_GREEN
		SWITCH holeIndex
			CASE 0
				RETURN << -1329.875,                        52.557,                   52.677>>
			BREAK
			CASE 1
				RETURN << -1329.26,                      35.458,                   52.677 >>
			BREAK
			CASE 2
				RETURN  << -1330.135,                      95.877,                   54.98 >>
			BREAK
			CASE 3
				RETURN << -1334.394,                      103.347,                 55.392 >>
			BREAK
			CASE 4
				RETURN << -1332.059,                      68.521,                   52.736 >>
			BREAK
			CASE 5
				RETURN << -1336.215,                      82.867,                   53.703 >>
			BREAK
		ENDSWITCH
	#ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Accessor for hole tee positition
/// PARAMS:
///    golfCourse - 
///    holeIndex - 
/// RETURNS:
///    
FUNC VECTOR GET_GOLF_HOLE_TEE_POSITION(GOLF_COURSE &golfCourse, INT holeIndex)
	IF holeIndex < 0 OR holeIndex >= golfCourse.iNumHoles
		RETURN <<0,0,0>>
	ENDIF
#IF NOT COMPILE_LOOKUPS	
	RETURN golfCourse.golfHole[holeIndex].vTeePosition
#ENDIF
#IF COMPILE_LOOKUPS
	RETURN GET_GOLF_HOLE_TEE_POSITION_STATIC(holeIndex)
#ENDIF
ENDFUNC


/// PURPOSE:
///    Accessor for hole tee spectator positition
/// PARAMS:
///    golfCourse - 
///    holeIndex - 
///    spectateIndex - 
/// RETURNS:
///    
FUNC VECTOR GET_GOLF_HOLE_TEE_SPECTATE_POSITION(GOLF_COURSE &golfCourse, INT holeIndex, INT spectateIndex)
	IF holeIndex < 0 OR holeIndex >= golfCourse.iNumHoles
		RETURN <<0,0,0>>
	ENDIF
#IF NOT COMPILE_LOOKUPS	
	IF spectateIndex < 0 OR spectateIndex >= COUNT_OF(golfCourse.golfHole[].vTeeSpectate)
		RETURN <<0,0,0>>
	ENDIF
	
	RETURN golfCourse.golfHole[holeIndex].vTeeSpectate[spectateIndex]
#ENDIF
#IF COMPILE_LOOKUPS
	SWITCH holeIndex
		CASE 0
			SWITCH spectateIndex
				CASE 0
					RETURN <<-1369.2075, 166.1965, 57.0130>>
				BREAK
				CASE 1
					RETURN <<-1367.2594, 166.6778, 57.0130>>
				BREAK
				CASE 2
					RETURN <<-1370.8029, 167.6664, 57.0130>>
				BREAK
				CASE 3
					RETURN <<-1371.5073, 169.5609, 57.0130>>
				BREAK
				CASE 4
					RETURN <<-1370.8456, 164.1834, 56.8900>>
				BREAK
				CASE 5
					RETURN <<-1367.9016, 164.2965, 56.8690>>
				BREAK
			ENDSWITCH

		BREAK
		CASE 1
			SWITCH spectateIndex
				CASE 0
					RETURN << -1101.0491, 156.1904, 62.0401 >> 
				BREAK
				CASE 1
					RETURN << -1100.9050, 159.2561, 62.0415 >>
				BREAK
				CASE 2
					RETURN << -1102.7826, 161.6288, 62.0412 >>
				BREAK
				CASE 3
					RETURN << -1105.9637, 161.2863, 62.0406 >>
				BREAK
				CASE 4
					RETURN <<-1104.4509, 163.5161, 62.0095>>
				BREAK
				CASE 5
					RETURN <<-1099.9340, 161.6773, 62.0185>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH spectateIndex
				CASE 0
					RETURN <<-1317.0281, 128.0565, 56.4377>>
				BREAK
				CASE 1
					RETURN <<-1315.4363, 129.9425, 56.6243>>
				BREAK
				CASE 2
					RETURN <<-1313.4515, 131.9924, 56.8265>>
				BREAK
				CASE 3
					RETURN <<-1317.2489, 133.3213, 56.7050>>
				BREAK
				CASE 4
					RETURN <<-1318.7860, 131.5965, 56.4503>>
				BREAK
				CASE 5
					RETURN <<-1320.1383, 129.2562, 56.3240>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH spectateIndex
				CASE 0
					RETURN << -1218.8939, 110.6482, 57.08 >> 
				BREAK
				CASE 1
					RETURN  << -1222.2432, 110.2088, 57.08 >> 
				BREAK
				CASE 2
					RETURN << -1220.0, 111.91, 58.0703 >>
				BREAK
				CASE 3
					RETURN << -1221.2565, 101.3278, 57.08 >> 
				BREAK
				CASE 4
					RETURN <<-1223.2966, 103.1185, 56.8130>>
				BREAK
				CASE 5
					RETURN <<-1216.3892, 115.3967, 57.1354>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH spectateIndex
				CASE 0
					RETURN << -1104.6074, 70.6124, 53.2120 >>
				BREAK
				CASE 1
					RETURN << -1101.6980, 73.7137, 53.1993 >>
				BREAK
				CASE 2
					RETURN << -1103.90, 72.917, 54.30 >>
				BREAK
				CASE 3
					RETURN << -1100.4252, 75.0875, 54.3712 >>
				BREAK
				CASE 4
					RETURN <<-1108.5879, 72.7163, 53.4783>>
				BREAK
				CASE 5
					RETURN <<-1107.1752, 68.8603, 53.2257>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH spectateIndex
				CASE 0
					RETURN << -984.8632, -108.5439, 39.5642 >> 
				BREAK
				CASE 1
					RETURN << -982.4098, -106.4736, 39.5732 >> 
				BREAK
				CASE 2
					RETURN << -981.2261, -103.0422, 39.5779 >>
				BREAK
				CASE 3
					RETURN << -981.8594, -100.6231, 39.5813 >>
				BREAK
				CASE 4
					RETURN <<-978.5359, -100.5075, 39.5193>>
				BREAK
				CASE 5
					RETURN <<-981.4874, -109.4747, 39.2195>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			SWITCH spectateIndex
				CASE 0
					RETURN << -1113.8646, -100.3123, 40.9050 >>
				BREAK
				CASE 1
					RETURN << -1111.5592, -104.7822, 40.8405 >>
				BREAK
				CASE 2
					RETURN << -1113.2805, -107.0443, 40.8405 >>
				BREAK
				CASE 3
					RETURN << -1116.9398, -109.7583, 40.8608 >>
				BREAK
				CASE 4
					RETURN <<-1110.0200, -108.1524, 40.7427>>
				BREAK
				CASE 5
					RETURN <<-1112.8149, -103.1259, 40.8406>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			SWITCH spectateIndex
				CASE 0
					RETURN << -1277.2773, 36.1405, 48.9194 >>
				BREAK
				CASE 1
					RETURN << -1277.3438, 39.2424, 49.1028 >>
				BREAK
				CASE 2
					RETURN << -1275.5933, 41.3619, 49.0876 >> 
				BREAK
				CASE 3
					RETURN << -1271.2444, 43.9149, 48.9679 >> 
				BREAK
				CASE 4
					RETURN <<-1279.0214, 42.0418, 49.3157>>
				BREAK
				CASE 5
					RETURN <<-1281.1838, 37.6356, 49.3165>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			SWITCH spectateIndex
				CASE 0
					RETURN <<-1138.5895, -5.6756, 47.9822>>
				BREAK
				CASE 1
					RETURN <<-1136.4796, -5.8462, 47.9822>>
				BREAK
				CASE 2
					RETURN  <<-1134.6447, -4.3631, 47.9822>>
				BREAK
				CASE 3
					RETURN <<-1133.7120, -2.4897, 47.9822>>
				BREAK
				CASE 4
					RETURN <<-1133.8031, -7.8430, 47.9822>>
				BREAK
				CASE 5
					RETURN <<-1137.6029, -9.0347, 47.8107>>
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
#ENDIF
ENDFUNC


FUNC VECTOR GET_GOLF_HOLE_PIN_POSITION_STATIC(INT holeIndex)
	#IF NOT PUTTING_GREEN
		SWITCH holeIndex
			CASE 0
				RETURN  << -1114.121, 220.789, 63.78 >>
			BREAK
			CASE 1
				RETURN << -1322.07, 158.77, 56.69 >>
			BREAK
			CASE 2
				RETURN << -1237.419, 112.988, 56.086 >> 
			BREAK
			CASE 3
				RETURN << -1096.541, 7.848, 49.63 >>
			BREAK
			CASE 4
				RETURN << -957.386, -90.412, 39.161 >>
			BREAK
			CASE 5
				RETURN << -1103.516, -115.163, 40.444 >>
			BREAK
			CASE 6
				RETURN << -1290.632, 2.754, 49.217 >> 
			BREAK
			CASE 7
				RETURN << -1034.944, -83.144, 42.919>>
			BREAK
			CASE 8
				RETURN << -1294.775, 83.51, 53.804 >>
			BREAK
		ENDSWITCH
	#ENDIF
	#IF PUTTING_GREEN
		SWITCH holeIndex
			CASE 0
				RETURN << -1329.875,                      35.458,                   52.677 >>
			BREAK
			CASE 1
				RETURN << -1329.26,                        52.557,                   52.677>>
			BREAK
			CASE 2
				RETURN << -1330.135,                      68.521,                   52.736 >>
			BREAK
			CASE 3
				RETURN << -1334.394,                      82.867,                   53.703 >>
			BREAK
			CASE 4
				RETURN << -1332.059,                      95.877,                   54.98 >>
			BREAK
			CASE 5
				RETURN << -1336.215,                      103.347,                 55.392 >> 
			BREAK
		ENDSWITCH
	#ENDIF
	
	
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Accessor for hole pin positition
/// PARAMS:
///    golfCourse - 
///    holeIndex - 
/// RETURNS:
///    
FUNC VECTOR GET_GOLF_HOLE_PIN_POSITION(GOLF_COURSE &golfCourse, INT holeIndex)
	IF holeIndex < 0 OR holeIndex >= golfCourse.iNumHoles
		RETURN <<0,0,0>>
	ENDIF
#IF NOT COMPILE_LOOKUPS	
	RETURN golfCourse.golfHole[holeIndex].vHolePosition
#ENDIF
#IF COMPILE_LOOKUPS
	#IF NOT PUTTING_GREEN
		SWITCH holeIndex
			CASE 0
				RETURN  << -1114.121, 220.789, 63.78 >>
			BREAK
			CASE 1
				RETURN << -1322.07, 158.77, 56.69 >>
			BREAK
			CASE 2
				RETURN << -1237.419, 112.988, 56.086 >> 
			BREAK
			CASE 3
				RETURN << -1096.541, 7.848, 49.63 >>
			BREAK
			CASE 4
				RETURN << -957.386, -90.412, 39.161 >>
			BREAK
			CASE 5
				RETURN << -1103.516, -115.163, 40.444 >>
			BREAK
			CASE 6
				RETURN << -1290.633, 2.771, 49.219 >> 
			BREAK
			CASE 7
				RETURN << -1034.944, -83.144, 42.919>>
			BREAK
			CASE 8
				RETURN << -1294.775, 83.51, 53.804 >>
			BREAK
		ENDSWITCH
	#ENDIF
	#IF PUTTING_GREEN
		SWITCH holeIndex
			CASE 0
				RETURN << -1329.875,                      35.458,                   52.677 >>
			BREAK
			CASE 1
				RETURN << -1329.26,                        52.557,                   52.677>>
			BREAK
			CASE 2
				RETURN << -1330.135,                      68.521,                   52.736 >>
			BREAK
			CASE 3
				RETURN << -1334.394,                      82.867,                   53.703 >>
			BREAK
			CASE 4
				RETURN << -1332.059,                      95.877,                   54.98 >>
			BREAK
			CASE 5
				RETURN << -1336.215,                      103.347,                 55.392 >> 
			BREAK
		ENDSWITCH
	#ENDIF
	
	
	RETURN <<0,0,0>>
#ENDIF
ENDFUNC

/// PURPOSE:
///    Accessor for hole fairway positition
/// PARAMS:
///    golfCourse - 
///    holeIndex - 
/// RETURNS:
///    
FUNC VECTOR GET_GOLF_HOLE_FAIRWAY_POSITION(GOLF_COURSE &golfCourse, INT holeIndex)
	IF holeIndex < 0 OR holeIndex >= golfCourse.iNumHoles
		RETURN <<0,0,0>>
	ENDIF
#IF NOT COMPILE_LOOKUPS	
	RETURN golfCourse.golfHole[holeIndex].vFairwayPosition
#ENDIF
#IF COMPILE_LOOKUPS
	#IF NOT PUTTING_GREEN
		SWITCH holeIndex
			CASE 0
				RETURN << -1252.9742, 182.4325, 61.3071 >>
			BREAK
			CASE 1
				RETURN << -1222.2045, 150.2919, 58.7062 >>
			BREAK
			CASE 2
				RETURN  << -1240.0823, 105.7823, 55.6871 >>
			BREAK
			CASE 3
				RETURN << -1132.48730, 74.15947, 55.23262 >>
			BREAK
			CASE 4
				RETURN << -1022.11084, -34.77494, 44.37743 >>
			BREAK
			CASE 5
				RETURN << -1100.05701, -114.27702, 40.53680 >>
			BREAK
			CASE 6
				RETURN << -1225.2073, -54.2714, 44.1932 >>
			BREAK
			CASE 7
				RETURN << -1159.3220, -26.5465, 44.7971 >>
			BREAK
			CASE 8
				RETURN <<-1177.1937, 34.2190, 50.8363>>
			BREAK
		ENDSWITCH
	#ENDIF
	#IF PUTTING_GREEN
		SWITCH holeIndex
			CASE 0
				RETURN << -1329.875,                      35.458,                   52.677 >>
			BREAK
			CASE 1
				RETURN << -1329.26,                        52.557,                   52.677>>
			BREAK
			CASE 2
				RETURN << -1330.135,                      68.521,                   52.736 >>
			BREAK
			CASE 3
				RETURN << -1334.394,                      82.867,                   53.703 >>
			BREAK
			CASE 4
				RETURN << -1332.059,                      95.877,                   54.98 >>
			BREAK
			CASE 5
				RETURN << -1336.215,                      103.347,                 55.392 >> 
			BREAK
		ENDSWITCH
	#ENDIF
	RETURN <<0,0,0>>
#ENDIF
ENDFUNC

/// PURPOSE:
///    Accessor for green specatate positition
/// PARAMS:
///    golfCourse - 
///    holeIndex - 
///    spectateIndex - 
/// RETURNS:
///    
FUNC VECTOR GET_GOLF_HOLE_GREEN_SPECTATE_POSITION(GOLF_COURSE &golfCourse, INT holeIndex, INT spectateIndex)
	IF holeIndex < 0 OR holeIndex >= golfCourse.iNumHoles
		RETURN <<0,0,0>>
	ENDIF
#IF NOT COMPILE_LOOKUPS	
	IF spectateIndex < 0 OR spectateIndex >= COUNT_OF(golfCourse.golfHole[].vGreenSpectate)
		RETURN <<0,0,0>>
	ENDIF
	
	RETURN golfCourse.golfHole[holeIndex].vGreenSpectate[spectateIndex]
#ENDIF
#IF COMPILE_LOOKUPS
	SWITCH holeIndex
		CASE 0
			SWITCH spectateIndex
				CASE 0
					RETURN  << -1121.4823, 210.4423, 63.9292 >>
				BREAK
				CASE 1
					RETURN << -1111.8832, 211.2035, 63.8440 >>
				BREAK
				CASE 2
					RETURN << -1109.5812, 220.7810, 63.9314 >>
				BREAK
				CASE 3
					RETURN << -1117.7329, 231.4756, 64.5959 >>
				BREAK
				CASE 4
					RETURN <<-1124.8193, 232.4564, 65.1060>>
				BREAK
				CASE 5
					RETURN <<-1114.6022, 228.6924, 64.1294>>
				BREAK
				CASE 6
					RETURN <<-1124.3363, 220.1641, 64.0189>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH spectateIndex
				CASE 0
					RETURN << -1324.6469, 150.2405, 56.9512 >> 
				BREAK
				CASE 1
					RETURN << -1332.1470, 152.7218, 56.9449 >>
				BREAK
				CASE 2
					RETURN << -1340.3395, 165.9850, 57.0015 >>
				BREAK
				CASE 3
					RETURN << -1330.3406, 172.5728, 57.0822 >>
				BREAK
				CASE 4
					RETURN <<-1325.2750, 173.1006, 57.1633>>
				BREAK
				CASE 5
					RETURN <<-1337.1770, 158.5159, 56.9107>>
				BREAK
				CASE 6
					RETURN <<-1320.5304, 169.4781, 56.8531>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH spectateIndex
				CASE 0
					RETURN << -1230.4810, 103.0481, 55.7491 >>
				BREAK
				CASE 1
					RETURN << -1234.8417, 95.7965, 55.6671 >>
				BREAK
				CASE 2
					RETURN << -1243.4578, 95.5196, 55.5717 >>
				BREAK
				CASE 3
					RETURN << -1245.1464, 112.5895, 55.9974 >>
				BREAK
				CASE 4
					RETURN <<-1239.0730, 119.9953, 56.4275>>
				BREAK
				CASE 5
					RETURN <<-1232.4279, 116.2665, 56.6504>>
				BREAK
				CASE 6
					RETURN <<-1235.9989, 119.7500, 56.4250>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH spectateIndex
				CASE 0
					RETURN << -1101.0415, 27.2703, 50.1697 >>
				BREAK
				CASE 1
					RETURN << -1094.1313, 23.7850, 50.0870 >>
				BREAK
				CASE 2
					RETURN << -1087.6356, 20.9966, 50.0321 >>
				BREAK
				CASE 3
					RETURN << -1080.5242, 13.8964, 49.7340 >>
				BREAK
				CASE 4
					RETURN << -1086.1053,  4.0804, 49.7967 >>
				BREAK
				CASE 5
					RETURN << -1097.6541, -1.5694, 50.0125 >>
				BREAK
				CASE 6
					RETURN << -1108.6144, -4.9012, 49.6852 >>
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH spectateIndex
				CASE 0
					RETURN << -949.3773, -93.7449, 39.5250 >>
				BREAK
				CASE 1
					RETURN << -965.4772, -92.9853, 39.3605 >>
				BREAK
				CASE 2
					RETURN << -965.3192, -101.4310, 39.4042 >>
				BREAK
				CASE 3
					RETURN << -952.4808, -99.1808, 39.5487 >>
				BREAK
				CASE 4
					RETURN <<-958.3450, -103.7673, 39.3340>>
				BREAK
				CASE 5
					RETURN <<-949.2680, -87.9874, 39.3694>>
				BREAK
				CASE 6
					RETURN <<-951.5580, -85.9542, 39.2469>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH spectateIndex
				CASE 0
					RETURN << -1098.2764, -107.6579, 40.5369 >>
				BREAK
				CASE 1
					RETURN << -1106.5730, -106.9375, 40.6960 >>
				BREAK
				CASE 2
					RETURN << -1111.6543, -121.3032, 40.7039 >>
				BREAK
				CASE 3
					RETURN << -1102.0128, -127.7622, 40.6900 >>
				BREAK
				CASE 4
					RETURN <<-1092.9447, -115.8245, 40.5376>>
				BREAK
				CASE 5
					RETURN <<-1094.2567, -122.7798, 40.5520>>
				BREAK
				CASE 6
					RETURN <<-1092.9913, -118.4603, 40.5422>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			SWITCH spectateIndex
				CASE 0
					RETURN << -1294.8875, 10.1593, 50.3758 >>
				BREAK
				CASE 1
					RETURN << -1288.8715, 14.8418, 49.8751 >>
				BREAK
				CASE 2
					RETURN  << -1276.2704, 11.8301, 48.5562 >>
				BREAK
				CASE 3
					RETURN << -1283.1622, -6.8256, 48.6238 >>
				BREAK
				CASE 4
					RETURN <<-1275.6063, -2.0290, 48.0408>>
				BREAK
				CASE 5
					RETURN <<-1273.7120, 5.2094, 48.1840>>
				BREAK
				CASE 6
					RETURN <<-1293.3835, 0.3131, 49.4842>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			SWITCH spectateIndex
				CASE 0
					RETURN << -1041.6849, -75.4766, 43.0439 >>
				BREAK
				CASE 1
					RETURN << -1030.3097, -76.6724, 43.2806 >>
				BREAK
				CASE 2
					RETURN << -1029.7913, -88.4011, 43.1511 >>
				BREAK
				CASE 3
					RETURN << -1041.5754, -92.4546, 42.8253 >>
				BREAK
				CASE 4
					RETURN <<-1050.4659, -93.7612, 42.5099>>
				BREAK
				CASE 5
					RETURN <<-1050.8630, -84.3568, 42.5056>>
				BREAK
				CASE 6
					RETURN <<-1048.3430, -82.4877, 42.5625>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			SWITCH spectateIndex
				CASE 0
					RETURN << -1284.5513, 76.0288, 53.9062 >>
				BREAK
				CASE 1
					RETURN << -1282.6757, 86.4323, 53.9098 >>
				BREAK
				CASE 2
					RETURN <<-1299.2700, 80.8423, 53.9110>>
				BREAK
				CASE 3
					RETURN <<-1299.1049, 87.1486, 53.9145>>
				BREAK
				CASE 4
					RETURN <<-1290.0995, 74.7491, 53.9426>>
				BREAK
				CASE 5
					RETURN <<-1292.3124, 90.6955, 53.9123>>
				BREAK
				CASE 6
					RETURN  <<-1291.1971, 89.2305, 53.9061>>
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	RETURN <<0,0,0>>
#ENDIF
ENDFUNC

FUNC BOOL SHOULD_USE_GREEN_SPECATE_FOR_COORD(VECTOR vShooterCoord, INT iHole)

	SWITCH iHole
		CASE 0
			RETURN VDIST2(vShooterCoord, <<-1118.974365,216.739594,62.148014>>  ) < (8.75 * 8.75)
		BREAK
		CASE 1
			RETURN FALSE
		BREAK
		CASE 2
			RETURN FALSE
		BREAK
		CASE 3
			RETURN VDIST2(vShooterCoord, <<-1098.52, 10.75, 49.73>> ) < (25.00 * 25.00)
		BREAK
		CASE 4
			RETURN FALSE
		BREAK
		CASE 5
			RETURN FALSE
		BREAK
		CASE 6
			RETURN FALSE
		BREAK
		CASE 7
			RETURN FALSE
		BREAK
		CASE 8
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_GOLF_HOLE_TEE_RADIUS(INT iHole)

	SWITCH iHole
		CASE 0
			RETURN 11.26
		BREAK
		CASE 1
			RETURN 10.38
		BREAK
		CASE 2
			RETURN 13.40
		BREAK
		CASE 3
			RETURN 8.94
		BREAK
		CASE 4
			RETURN 12.80
		BREAK
		CASE 5
			RETURN 9.42
		BREAK
		CASE 6
			RETURN 9.42
		BREAK
		CASE 7
			RETURN 15.00
		BREAK
		CASE 8
			RETURN 11.69
		BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC BOOL IS_POINT_NEAR_TEE_BOX(VECTOR vWorldPoint)

	INT iHole
	
	REPEAT 9 iHole
		IF VDIST2(GET_GOLF_HOLE_TEE_POSITION_STATIC(iHole), vWorldPoint) < (GET_GOLF_HOLE_TEE_RADIUS(iHole) * GET_GOLF_HOLE_TEE_RADIUS(iHole))
			RETURN TRUE
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_POINT_BAD_LIE(VECTOR vWorldPos)
	
	RETURN IS_POINT_NEAR_POINT( vWorldPos, <<-1215.93, -15.72, 45.21>>, 2.500)
	
ENDFUNC

/// PURPOSE:
///    Accessor for golf hole length
/// PARAMS:
///    golfCourse - 
///    holeIndex - 
/// RETURNS:
///    
FUNC FLOAT GET_GOLF_HOLE_LENGTH(GOLF_COURSE &golfCourse, INT holeIndex)
	IF holeIndex < 0 OR holeIndex >= golfCourse.iNumHoles
		RETURN 0.0
	ENDIF
	RETURN golfCourse.golfHole[holeIndex].fHoleLength
ENDFUNC

/// PURPOSE:
///    Accessor for golf hole par
/// PARAMS:
///    golfCourse - 
///    holeIndex - 
/// RETURNS:
///    
FUNC INT GET_GOLF_HOLE_PAR(GOLF_COURSE &golfCourse, INT holeIndex)
	IF holeIndex < 0 OR holeIndex >= golfCourse.iNumHoles
		RETURN 0
	ENDIF
	RETURN golfCourse.golfHole[holeIndex].iPar
ENDFUNC

FUNC INT GET_GOLF_HOLE_PAR_STATIC(INT holeIndex)
	SWITCH holeIndex
		CASE 0
			RETURN 5
		BREAK
		CASE 1
			RETURN 4
		BREAK
		CASE 2
			RETURN 3
		BREAK
		CASE 3
			RETURN 4
		BREAK
		CASE 4
			RETURN 4
		BREAK
		CASE 5
			RETURN 3
		BREAK
		CASE 6
			RETURN 4
		BREAK
		CASE 7
			RETURN 5
		BREAK
		CASE 8
			RETURN 4
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Accessor for golf hole flag object
/// PARAMS:
///    golfCourse - 
///    holeIndex - 
/// RETURNS:
///    
FUNC OBJECT_INDEX GET_GOLF_HOLE_FLAG(GOLF_COURSE &golfCourse, INT holeIndex)
	IF holeIndex < 0 OR holeIndex >= golfCourse.iNumHoles
		RETURN NULL
	ENDIF
	RETURN golfCourse.golfHole[holeIndex].objFlag
ENDFUNC

/// PURPOSE:
///    Accessor for golf hole flag object
/// PARAMS:
///    golfCourse - 
///    holeIndex - 
///    objFlag - 
PROC SET_GOLF_HOLE_FLAG(GOLF_COURSE &golfCourse, INT holeIndex, OBJECT_INDEX objFlag)
	IF holeIndex < 0 OR holeIndex >= golfCourse.iNumHoles
		EXIT
	ENDIF
	golfCourse.golfHole[holeIndex].objFlag = objFlag
ENDPROC

/// PURPOSE:
///    Accessor for golf hole flag object
/// PARAMS:
///    golfCourse - 
///    holeIndex - 
PROC CLEANUP_GOLF_HOLE_FLAG(GOLF_COURSE &golfCourse, INT holeIndex)
	IF holeIndex < 0 OR holeIndex >= golfCourse.iNumHoles
		EXIT
	ENDIF
	IF DOES_ENTITY_EXIST(golfCourse.golfHole[holeIndex].objFlag)
		DELETE_OBJECT(golfCourse.golfHole[holeIndex].objFlag)
	ENDIF
ENDPROC

/// PURPOSE:
///    Accessor for golf course number of holes
/// PARAMS:
///    golfCourse - 
/// RETURNS:
///    
FUNC INT GET_GOLF_COURSE_NUM_HOLES(GOLF_COURSE &golfCourse)
	RETURN golfCourse.iNumHoles
ENDFUNC

/// PURPOSE:
///    Accessor for golf course start position
/// PARAMS:
///    golfCourse - 
///    playerIndex - 
/// RETURNS:
///    
FUNC VECTOR GET_GOLF_COURSE_START_POSITION(GOLF_COURSE &golfCourse, INT playerIndex)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(golfCourse.vPlayerStartPosition)
		RETURN <<0,0,0>>
	ENDIF
	RETURN golfCourse.vPlayerStartPosition[playerIndex]
ENDFUNC

/// PURPOSE:
///    Accessor for golf course cart position
/// PARAMS:
///    golfCourse - 
///    playerIndex - 
/// RETURNS:
///    
FUNC VECTOR GET_GOLF_COURSE_CART_POSITION(GOLF_COURSE &golfCourse, INT playerIndex)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(golfCourse.vCartStartPosition)
		RETURN <<0,0,0>>
	ENDIF
	RETURN golfCourse.vCartStartPosition[playerIndex]
ENDFUNC	


/// PURPOSE:
///    Accessor for golf course current player foursome hole
/// PARAMS:
///    golfCourse - 
/// RETURNS:
///    
FUNC INT GET_GOLF_COURSE_PLAYER_HOLE()
	RETURN g_sGolfGlobals.CurrrentPlayerHole
ENDFUNC	

/// PURPOSE:
///    Accessor for golf course current player foursome hole
/// PARAMS:
///    golfCourse - 
///    iCurrentPlayerHole - 
PROC SET_GOLF_COURSE_PLAYER_HOLE(INT iCurrentPlayerHole)
	g_sGolfGlobals.CurrrentPlayerHole = iCurrentPlayerHole
ENDPROC	 

/// PURPOSE:
///    Accessor for golf course
/// PARAMS:
///    golfCourse - 
FUNC INT GET_GOLF_COURSE_TOTAL_PAR(GOLF_COURSE &golfCourse)
	RETURN golfCourse.iTotalPar
ENDFUNC	

/// PURPOSE:
///    Accessor for golf course
/// PARAMS:
///    golfCourse - 
///    iTotalPar - 
PROC SET_GOLF_COURSE_TOTAL_PAR(GOLF_COURSE &golfCourse, INT iTotalPar)
	golfCourse.iTotalPar = iTotalPar
ENDPROC	 

FUNC BOOL IS_GOLF_LIE_UNPLAYABLE(GOLF_LIE_TYPE lieType)
	RETURN lieType = LIE_BUSH OR lieType = LIE_CART_PATH
ENDFUNC

FUNC GOLF_LIE_TYPE GET_GOLF_LIE_TYPE_FROM_MATERIAL(MATERIAL_NAMES lastMatHit)
	SWITCH lastMatHit
		CASE GOLF_ROUGH
		CASE GOLF_ROUGH2
			RETURN LIE_ROUGH
		CASE GOLF_HARD_VEG3
		CASE GOLF_SOFT_VEG
		CASE GOLF_HARD_VEG1
		CASE GOLF_HARD_VEG2
		CASE GOLF_DIRT_TRACK
		CASE GOLF_HARD_VEG4
			RETURN LIE_BUSH
		BREAK
		CASE GOLF_MISC
		CASE GOLF_FLAG_POLE
			RETURN LIE_CUP
		BREAK
		CASE GOLF_PATH1
		CASE GOLF_PATH2
		CASE GOLF_CART_PATH
		CASE GOLF_ROCKS
		CASE GOLF_SAND //golf path can be compact sand
			RETURN LIE_CART_PATH
		BREAK
		CASE GOLF_FAIRWAY
			RETURN LIE_FAIRWAY
		BREAK
		CASE GOLF_GREEN
			RETURN LIE_GREEN
		BREAK
		CASE GOLF_BUNKER
			RETURN LIE_BUNKER
		BREAK
		CASE GOLF_WATER_HAZARD
			RETURN LIE_WATER
		BREAK
	ENDSWITCH
	
	#IF NOT GOLF_IS_AI_ONLY
		CDEBUG1LN(DEBUG_GOLF,"Material not an enum ", lastMatHit)
	#ENDIF
	RETURN LIE_UNKNOWN
ENDFUNC

FUNC TEXT_LABEL_23 GET_LIE_STRING_FROM_ENUM(GOLF_LIE_TYPE eLie)
	TEXT_LABEL_23 txtLie = "LIE_"

	SWITCH eLie
		CASE LIE_TEE
			txtLie += "TEE"
		BREAK
		CASE LIE_FAIRWAY
			txtLie += "FAIRWAY"
		BREAK
		CASE LIE_ROUGH
			txtLie += "ROUGH"
		BREAK
		CASE LIE_BUNKER
			txtLie += "BUNKER"
		BREAK
		CASE LIE_GREEN
			txtLie += "GREEN"
		BREAK
		CASE LIE_CART_PATH
			txtLie += "PATH"
		BREAK
		CASE LIE_WATER
			txtLie += "WATER"
		BREAK
		DEFAULT
			txtLie += "UNKNOWN"
		BREAK
	ENDSWITCH
	
	RETURN txtLie
ENDFUNC

//Gets the center of the course, not complete
FUNC VECTOR GET_GOLF_COURSE_CENTER(GOLF_COURSE &thisCourse)
	UNUSED_PARAMETER(thisCourse)

	RETURN <<-1175, 40, 57>>
ENDFUNC
