USING "minigames_helpers.sch"
USING "minigame_UIInputs.sch"
USING "socialclub_leaderboard.sch"
USING "script_conversion.sch"
USING "cheat_controller_public.sch"
USING "golf_buddies.sch"
USING "script_usecontext.sch"
USING "golf_splash.sch"

#IF NOT DEFINED(COMPILE_LOOKUPS)
	CONST_INT COMPILE_LOOKUPS 0
#ENDIF
#IF NOT DEFINED(PUTTING_GREEN)
	CONST_INT PUTTING_GREEN 0
#ENDIF
#IF NOT DEFINED(GOLF_IS_AI_ONLY)
	CONST_INT GOLF_IS_AI_ONLY 0
#ENDIF

#IF NOT DEFINED(GOLF_USE_FALLTHROUGH_WORLD_CHECK)
	CONST_INT GOLF_USE_FALLTHROUGH_WORLD_CHECK 0
#ENDIF


#IF IS_DEBUG_BUILD

VECTOR vRingScale = <<0, 0, 1>>
VECTOR vRingColour = <<0.037, 0.185, 0>>
Float fRingAlpha = 0.0

VECTOR vDebugBounds1 = <<-1274.15, 186.79, 61.970>> 
VECTOR vDebugBounds2 = << -1255.380, 185.870, 0.0>>
VECTOR vDebugBounds3 = << -1326.063, 192.648, 61.760>>
VECTOR vDebugBounds4 = <<-1325.440, 183.78, 0.0>> 
VECTOR vDebugBounds5 = << -1159.35, 226.630, 70>>
VECTOR vDebugBounds6 = <<-1152.920, 203.93, 0.0>>

BOOL bDisableNaturalClubSpinEffects = FALSE
#ENDIF

BOOL bPCGolfControlsSetup = FALSE

/// PURPOSE:
///    Initialises the PC controls in a safe way
PROC SETUP_PC_GOLF_CONTROLS()

	IF bPCGolfControlsSetup = FALSE
		INIT_PC_SCRIPTED_CONTROLS("Golf")
		bPCGolfControlsSetup = TRUE
	ENDIF

ENDPROC

// Cleans up the golf controls in a safe way
PROC CLEANUP_PC_GOLF_CONTROLS()
	
	IF bPCGolfControlsSetup
		SHUTDOWN_PC_SCRIPTED_CONTROLS()
		bPCGolfControlsSetup = FALSE
	ENDIF

ENDPROC

/// PURPOSE:
///    Meters to yards conversion, since the game runs on meters, but golf is traditionally represented in Yards/Feet
/// PARAMS:
///    fMeters - Meters to convert to yards
/// RETURNS:
///    Number of yards 
FUNC FLOAT METERS_TO_YARDS(FLOAT fMeters)
	RETURN fMeters * 1.0936133
ENDFUNC

FUNC FLOAT YARDS_TO_METERS(FLOAT fYards)
	RETURN fYards/1.0936133
ENDFUNC

FUNC FLOAT FEET_TO_METERS(FLOAT fFeet)
	RETURN fFeet*0.3048
ENDFUNC

FUNC FLOAT INCH_TO_CM(FLOAT fInch)
	RETURN fInch * 2.54
ENDFUNC

/// PURPOSE:
///    Meters to feet conversion, since the game runs on meters, but golf is traditionally represented in Yards/Feet
/// PARAMS:
///    fMeters - Meters to convert to feet
/// RETURNS:
///    Number of feet 
//FUNC FLOAT METERS_TO_FEET(FLOAT fMeters)
//	RETURN fMeters * 3.2808
//ENDFUNC

TWEAK_FLOAT	MAGIC_DISTANCE_CONVERSION	1.85 // This is so we can make the existing hole seem like the right length. Displayed distances are multiplied by this, and club tunings are divided by

//TWEAK_FLOAT predictWood1 1.0
//TWEAK_FLOAT predictWood3 1.0
//TWEAK_FLOAT predictWood5 70.0
//TWEAK_FLOAT predictIron3 65.0
//TWEAK_FLOAT predictIron4 60.0
//TWEAK_FLOAT predictIron5 55.0
//TWEAK_FLOAT predictIron6 50.0
//TWEAK_FLOAT predictIron7 45.0
//TWEAK_FLOAT predictIron8 40.0
//TWEAK_FLOAT predictIron9 35.0
//TWEAK_FLOAT predictPitch 30.0
//TWEAK_FLOAT predictSand  25.0
//TWEAK_FLOAT predictLob   20.0
TWEAK_INT	CONST_TEE_PREVIEW_0	1000
TWEAK_INT	CONST_TEE_PREVIEW_1	1300
TWEAK_INT	CONST_TEE_PREVIEW_2	2500
TWEAK_INT	CONST_TEE_PREVIEW_3	1200
TWEAK_INT	CONST_TEE_PREVIEW_4	2000
TWEAK_INT	CONST_TEE_PREVIEW_5	1800
TWEAK_INT	CONST_TEE_PREVIEW_6	2000
TWEAK_INT	CONST_TEE_PREVIEW_7	1800
TWEAK_INT	CONST_TEE_PREVIEW_8	2100

TWEAK_INT	CONST_GRAPH_TYPE	1

/// PURPOSE: Bitfield enum for controlling help and objective prints
ENUM GOLF_STREAMING_FLAGS
	GSF_NO_STREAM_FLAG = 0,
	GSF_STREAM_CUTBACK_AFTER_SHOT = BIT0,
	GSF_STREAM_NEXT_SHOT_FOR_SKIPPING = BIT1,
	GSF_END_GAME_XP_CALCULATED = BIT2,
	
	GSF_SET_HELPER_BLIP_ADDRESS_NAME = BIT10,
	GSF_SET_HELPER_BLIP_IN_FLIGHT_NAME = BIT11,
	
	GSF_SKIP_AI_PRESSED = BIT12,
	GSF_REACTION_CAMERA_SET_UP = BIT13,
	
	GSF_SUCCESSFUL_LEADERBOARD_WRITE = BIT17,
	GSF_DONT_ALLOW_SOCIAL_CLUB = BIT18,
	GSF_SCORECARD_SOUND_PLAYED = BIT19,
	GSF_SCORECARD_STREAMVOL_LOADED = BIT20,
	GSF_SCORECARD_STREAMVOL_LOADINGNEXT	= BIT21,
	GSF_SCORECARD_INITIAL_LOAD_DONE	= BIT22,
	GSF_FORCE_SCORECARD_VOL_LOAD = BIT23,
	
	GSF_TURNED_OFF_EXPANDED_MAP = BIT24
ENDENUM


/// PURPOSE: Enumeration for the state machine for the golf scripts
ENUM GOLF_MINIGAME_STATE
	GMS_UNDEFINED = -1,
	GMS_PRE_INIT,
	GMS_PRE_INIT_WAIT,
	GMS_INIT,
	GMS_INIT_STREAMING_DONE,
	GMS_INIT_POST_STREAMING,
	GMS_INIT_DONE,
	GMS_PLAY_GOLF,
	GMS_EXIT_OK,
	GMS_SCOREBOARD_OVERRIDE,
	GMS_CLEANUP,
	GMS_OUTRO_QUIT,
	GMS_OUTRO_INIT,
	GMS_OUTRO,
	GMS_REMATCH,
	GMS_FAIL,
	GMS_RESET
ENDENUM
CONST_INT BIT31 (BIT30 * 2)  // -2147483648 for 32-bit signed int, this definition works with code.

/// PURPOSE: Bitfield enumeration for human player control to commmunicate with AI foursomes
ENUM GOLF_CONTROL_FLAGS
	GCF_NO_FLAGS						= 0,
	GCF_PLAYER_SKIP						= BIT0,
	GCF_PLAYER_DONE_WITH_HOLE			= BIT1,
	GCF_PLAYER_DONE_WITH_SHOT			= BIT2,
	GCF_AI_HIT_BALL_THIS_FRAME			= BIT3,
	GCF_WAS_PERFECT_HIT					= BIT4,
	GCF_BALL_JUST_HIT           		= BIT5,
	GCF_BALL_JUST_LAND                  = BIT6,
	GCF_BALL_OOB						= BIT7,
	GCF_STOP_INPUT						= BIT8,
	GCF_TELPORT_CART                    = BIT9,
	GCF_MOVE_TO_REMATCH_SCREEN			= BIT10,
	GCF_MP_START_QUIT_MENU				= BIT11,
	GCF_MP_SYNC_SPLASH_TEXT             = BIT12,
	GCF_MOVE_TO_NEXT_SHOT_MP			= BIT13, 
	GCF_MOVE_TO_NEXT_HOLE_MP			= BIT14, 
	GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE 	= BIT15, 
	GCF_OPPONENT_DONE_WITH_SHOT			= BIT16, 
	GCF_DONE_WITH_GAME					= BIT17,
	GCF_PLAYED_HIT_PARTICLE_EFFECT_MP   = BIT18,
	GCF_PLAYED_LAND_PARTICLE_EFFECT_MP  = BIT19, 
	GCF_PLAYED_HIT_SOUND_EFFECT_MP      = BIT20,
	GCF_PLAYED_LAND_SOUND_EFFECT_MP     = BIT21,
	GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE  = BIT22,
	GCF_MP_OUTRO						= BIT23, //send game to outro scene
	GCF_MP_PLAYED_BALL_IN_HOLE_SOUND    = BIT24,
	GCF_BALL_IN_CUP_SOUND				= BIT25,
	GCF_DISPLAY_QUITTER					= BIT26,
	GCF_USING_CUTSCENE_SHADOW			= BIT27,
	GCF_BALL_POP_SOUND					= BIT28,
	GCF_BALL_IN_FOLIAGE					= BIT29,
	
	GCF_IS_FRIEND_ACTIVITY				= BIT30,
	GCF_PLAYER_NO_SKIP					= BIT31 // This is so dangerous
	
ENDENUM

//Flags for the server
ENUM GOLF_CONTROL_FLAGS2
	GCF2_NO_FLAG = 0,
	GCF2_NEW_GOLFER_IS_SET 			= BIT1,
	GCF2_SYNC_BALL_IN_WATER_SOUND 	= BIT2,
	
	
	GCF2_BALL_CONTACT_SYNCED 		= BIT5,
	GCF2_GO_TO_REMATCH 				= BIT6,
	GCF2_PLAYER_HAS_TELEPORTED 		= BIT7,
	GCF2_DETACH_GOLF_CLUB			= BIT8,
	GFC2_DISPLAYING_QUITING_PLAYER  = BIT9,
	GFC2_AT_GOLF_SCOREBOARD		    = BIT10,
	
	GCF2_KICK_PLAYER				= BIT12,
	
	GCF2_VIEWING_ADDRESS_BALL = BIT20
ENDENUM

ENUM GOLF_UI_CONTROL
	GUC_NONE							 = 0,
	GUC_REFRESH_PERMANENT				 = BIT0,
	GUC_REFRESH_INPUT					 = BIT1,
	GUC_REFRESH_PUTT					 = BIT2,
	GUC_REFRESH_NAVIGATE 				 = BIT3,
	GUC_REFRESH_ADDRESS					 = BIT4,
	GUC_REFRESH_SWING					 = BIT5,
	GUC_REFRESH_FLIGHT					 = BIT6,
	GUC_REFRESH_AT_REST					 = BIT7,
	GUC_REFRESH_IN_HOLE					 = BIT8,
	GUC_REFRESH_FLOATING				 = BIT10,
	GUC_REFRESH_GOLF_TRAIL  		     = BIT11,
	
	GUC_MP_PLAYERS_ADDED				 = BIT12,
	GUC_USE_FILL_QUALITY    		     = BIT13,
	GUC_DISABLE_UI          		     = BIT14,
	GUC_END_GOLF_SCOREBOARD 		     = BIT15,
	GUC_DISABLE_FADE_IN     		     = BIT16,
	
	GUC_DISPLAY_NEW_BEST_SCORE           = BIT17,
	GUC_DISPLAY_FIR						 = BIT18,
	GUC_DISPLAY_GIR						 = BIT19,
	GUC_DISPLAY_LONGEST_DRIVE   		 = BIT20,
	GUC_DISPLAY_LONGEST_HOLE    		 = BIT21,
	GUC_DISPLAY_CLOSEST_TO_PIN 			 = BIT22,
	GUC_SPLASH_TEXT_DISPLAYED_THIS_TURN  = BIT23,
	
	GUC_SOCIAL_CLUB_BOARD_DISPLAYED		 = BIT24,
	GUC_SOCIAL_CLUB_UI_DISPLAYED		 = BIT25,
	GUC_SWING_METER_DISPLAYED   		 = BIT26,

	GUC_END_GAME_SPLASH_DISPLAYED		 = BIT27,
	GUC_STYLE_HELP_DISPLAYED   			 = BIT28,
	GUC_SELECT_PLAYERS_DISPLAYED		 = BIT29,
	GUC_SCOREBOARD_DISPLAYED			 = BIT30
ENDENUM

ENUM GOLF_DIALOGUE_FLAG
	GDF_NONE = 0,
	GDF_PARTNER_SAID_AIM_WARNING = BIT0,
	GDF_PLAYER_GET_IN_CART		 = BIT1

ENDENUM

ENUM GOLF_HELP_DISPLAYED_FLAG
	GHD_NONE                     = 0,
	GHD_WOOD_DISPLAYED           = BIT1,
	GHD_IRON_DISPLAYED           = BIT2,
	GHD_WEDGE_DISPLAYED          = BIT3,
	GHD_PUTTER_DISPLAYED         = BIT4,
	GHD_STYLE_POWER_DISPLAYED    = BIT5,
	GHD_STYLE_APPROACH_DISPLAYED = BIT6,
	GHD_STLYE_PUNCH_DISPLAYED    = BIT7,
	GHD_WARNING_UNPLAYABLE_LIE   = BIT8,
	GHD_WARNING_NEAR_LIMIT   	 = BIT9,
	GHD_WARNING_HAZARD_ROUGH   	 = BIT10,
	GHD_WARNING_HAZARD_SAND  	 = BIT11,
	GHD_WARNING_KICK_FOR_TIME	 = BIT12,

	GHD_DISPLAY_UNPLAYABLE_LIE_HELP = BIT15,
	
	GHD_DISPLAY_SCORECARD_HELP      = BIT26,
	GHD_START_TIE					= BIT27,
	GHD_APPROCH_BALL_FOOT_DISPLAYED = BIT28,
	
	GHD_GOLF_TERMINATED_DISPLAYED =    BIT30
ENDENUM

ENUM GOLF_KEYS_IN_USE_FLAG
	GOLF_KEY_NONE                = 0,
	GOLF_KEY_SOCIAL_CLUB         = BIT1,
	GOLF_KEY_CONTINUE            = BIT2,
	GOLF_KEY_SCROLL              = BIT3,
	GOLF_KEY_QUIT                = BIT4, 
	GOLF_KEY_REPLAY				 = BIT5,
	GOLF_KEY_PROFILE			 = BIT6,
	GOLF_KEY_ON_TEE				 = BIT7
ENDENUM

ENUM GOLF_OOB_REASON
	OOB_NONE = 0,
	OOB_WATER,
	OOB_UKNOWN_LIE,
	OOB_ON_WRONG_GREEN,
	OOB_UNPLAYABLE_LIE,
	OOB_OUTSIDE_BOUNDING_PLANES,
	OOB_OUTSIDE_HOLE_BOUNDING_SPHERES,
	OOB_RESTRICTIVE_POSITION
ENDENUM

ENUM GOLF_STATS_FLAG
	GSF_NONE							= 0, 
	GSF_UPDATED_STAT_SCORE 				= BIT1,	//Updated Birdie Stat recently, so we should avoid calling it until the next hole
	GSF_UPDATE_SOME_OTHER_STAT 			= BIT2
ENDENUM

//List of splash text, used for syncing splash text over network
ENUM GOLF_SPLASH_TEXT_ENUM
	GOLF_SPLASH_NONE = 0,
	GOLF_SPLASH_HOLE_IN_ONE,
	GOLF_SPLASH_EAGLE2,
	GOLF_SPLASH_EAGLE,
	GOLF_SPLASH_BIRDIE,
	GOLF_SPLASH_PAR,
	GOLF_SPLASH_BOGEY,
	GOLF_SPLASH_BOGEY2,
	GOLF_SPLASH_BOGEY3,
	GOLF_SPLASH_BOGEY4,
	GOLF_SPLASH_BOGEY5,
	GOLF_SPLASH_FIR,
	GOLF_SPLASH_GIR,
	GOLF_SPLASH_LONGEST_DIVE,
	GOLF_SPLASH_LONGEST_HOLE,
	GOLF_SPLASH_CLOSEST_TO_PIN,
	GOLF_SPLASH_OOB,
	GOLF_SPLASH_WATER,
	GOLF_SPLASH_UNPLAYABLE,
	GOLF_SPLASH_OBSTRUCTED,
	GOLF_SPLASH_SCORE_LIMIT, 
	GOLF_SPLASH_PERFECT_HIT
ENDENUM

ENUM GOLF_UI_DISPLAY_STATE
	GOLF_DISPLAY_NONE = 0,
	GOLF_DISPLAY_HOLE = BIT0,
	GOLF_DISPLAY_SHOT = BIT1,
	GOLF_DISPLAY_METER = BIT2,
	GOLF_DISPLAY_PLAYERCARD = BIT3,
	GOLF_DISPLAY_SCOREBOARD = BIT4,
	GOLF_DISPLAY_ALL = -1
ENDENUM

ENUM SHOT_DISPLAY_STATE
	SHOT_DISPLAY_NONE = 0,
	SHOT_DISPLAY_LIE = BIT0,
	SHOT_DISPLAY_WIND = BIT1,
	SHOT_DISPLAY_CLUB = BIT2,
	SHOT_DISPLAY_SWING = BIT3,
	SHOT_DISPLAY_SPIN = BIT4,
	SHOT_DISPLAY_SHOT_NUM = BIT5,
	SHOT_DISPLAY_ALL = -1
ENDENUM

ENUM GOLF_DISPLAYED_CONTROLS_STATE
	GDCS_NONE = 0,
	GDCS_SETUP_INTRO_KEYS,
	GDCS_SETUP_SCOREBOARD_KEYS,
	GDCS_SETUP_SOCIAL_CLUB_KEYS,
	GDCS_SETUP_GOLF_SCOREBOARD_CONTROLS,
	GDCS_SETUP_GOLF_NAV_CONTROLS,
	GDCS_SETUP_GOLF_ADRESS_CONTROLS,
	GDCS_SETUP_GOLF_SPECTATE_COMPUTER_CONTROLS,
	GDCS_SETUP_GOLF_SPECTATE_MP_CONTROLS,
	GDCS_SETUP_GOLF_BALL_IN_FLIGHT_CONTROLS,
	GDCS_SETUP_GOLF_FAIL_CONTROLS
ENDENUM

USING "golf_clubs.sch"

/// PURPOSE: Single data representation for the golf game - contains global data used by all foursomes
STRUCT GOLF_GAME
	GOLF_BAG				golfBag
	VECTOR					vWindDirection
	FLOAT					fOldWindHeading
	FLOAT					fWindStrength
	FLOAT					fOldWindStrength
	GOLF_CONTROL_FLAGS		golfControlFlags
	BOOL 					bPlayoffGame = FALSE
	INT						iPlayoffRound = 0
ENDSTRUCT 

USING "golf_course.sch"
USING "golf_swing_meter.sch"
USING "golf_player.sch"
USING "golf_foursome.sch"
/// PURPOSE: State machine for intro cutscene
ENUM GOLF_INTRO_STATE
	GIS_INTRO_FLYOVER_INIT,
	GIS_INTRO_FLYOVER_CAMERA1,
	GIS_INTRO_FLYOVER_CAMERA2,
	GIS_INTRO_FLYOVER_CAMERA3,
	GIS_INTRO_FLYOVER_CLEANUP,
	GIS_INTRO_FLYOVER_OVER
ENDENUM

//the type of camera the spectating player is using
ENUM SPECTATOR_CAMERA_STATE
	GOLF_SPECTATOR_ADDRESS_CAM = 0,
	GOLF_SPECTATOR_CINEMATIC_CAM,
	GOLF_SPECTATOR_POV_CAM
ENDENUM

STRUCT GOLF_TRAIL
	VECTOR position0, velocity0, checkPointPos
	FLOAT velocityScale, z1
	VECTOR radius, colorStart, colorMid, colorEnd, vAlpha
	INT numControlPoints, tessellation
	BOOL ascend
	FLOAT pixelThickness, pixelExpansion, fadeOpacity, fadeExponentBias, textureFill
ENDSTRUCT

STRUCT GOLF_INSTRUCTIONS
	FLOAT fInstrRectX
	FLOAT fInstrRectY
	FLOAT fInstrRectWidth
	FLOAT fInstrRectHeight
	FLOAT fInstrTextX
	FLOAT fInstrTextY
ENDSTRUCT

/// PURPOSE: Utility and helper data used by the local machine player for display of his foursome and thier play
STRUCT GOLF_HELPERS
	SCALEFORM_INDEX				golfUI
	SCALEFORM_INDEX				floatingUI
	SCALEFORM_INDEX				leaderboardUI
	
	COMPLEX_USE_CONTEXT useContext
	
	GOLF_UI_DISPLAY_STATE	eCurrentGolfUIDisplay
	GOLF_UI_DISPLAY_STATE	ePreviousGolfUIDisplay
	SHOT_DISPLAY_STATE      eShotDisplayState
	
	GOLF_UI_CONTROL				golfUIControl
	GOLF_DIALOGUE_FLAG		    golfDialogueFlag
	
	GOLF_HELP_DISPLAYED_FLAG	golfHelpDisplayed  //the help text was displayed, display no longer
	GOLF_HELP_DISPLAYED_FLAG	golfHelpDisplaying //the help text is currently displaying
	INT golfKeysInUse
	
	GOLF_STATS_FLAG				golfStatsFlag
	
	GOLF_TRAIL					golfTrail
	GOLF_INSTRUCTIONS			golfInstructionsBox
	GOLF_BUDDIES				currentBuddies
	REL_GROUP_HASH				golfersRelationshipHash
	GROUP_INDEX					golfGroupIndex
	
	PED_VARIATION_STRUCT	sPedVariations
	PED_VARIATION_STRUCT	sFriendVariation
	PED_VARIATION_STRUCT	sFriendVariationB
	VEHICLE_INDEX 			playerLastVehicle
	MODEL_NAMES 			vehicleName
	INT 					iVehicleColor
	INT						iTimesPlayedGolf
	structPedsForConversation 	golfConversation
	//CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
	
	GOLF_STREAMING_FLAGS golfStreaming
	
	INT soundRolling = -1
	
	CAMERA_INDEX camNavigate
	CAMERA_INDEX camNavigate2
	CAMERA_INDEX camFlight
	CAMERA_INDEX camAddress
	CAMERA_INDEX camPreview
	CAMERA_INDEX camPreviewSplineForward
	CAMERA_INDEX camLookAtGreen
	CAMERA_INDEX camReaction
	CAMERA_INDEX camMPApproach
	CAMERA_INDEX camUpStart
	CAMERA_INDEX camUpEnd
	CAMERA_INDEX camPauseAnimCamera
	CAMERA_INDEX camFlyover1, camFlyover2, camFlyover2End, camFlyover3
		
//	VECTOR vMPApproachStartPos
//	VECTOR vMPApproachStartRot
	FLOAT fPreviewSplinePhase
	FLOAT fHoleSplinePhase, fHoleSplineStartPhase
	VECTOR vPreviewCamFinalPos
	VECTOR vAdressCamStartPos
	VECTOR vSpectateCamOffset
	
	INT		iNumTimesFallThroughWorldCheckFailed
	VECTOR	vLastSafeBallPosition
	VECTOR 	vLastGoodAddressCamPos
	FLOAT   fCosShotAngleDevation
	FLOAT   fPlayerHeadingAtShot
	GOLF_INTRO_STATE	introState
	SPECTATOR_CAMERA_STATE spectatorCamState

	
	BOOL		bTransitionToShotCamOver
	BOOL		bShotPreviewCam
	INT			bGreenPreviewCam
		
	BOOL		bJustSwitchedSpectatorCam 
	BOOL		bIntroSkipped
	
	INT			iNumAutoSwings
	INT			iBlockNavMeshIndex = -1
	INT 		iLastEndCameraIndex
	INT			iMatchHistoryID
	BOOL		bJustExitQuit
	BOOL		bGameIsReplay
	BOOL		bPlayerInVehicle
	BOOL		bAllowPlayerViewCam
	BOOL		bWantsTeleport
	
	BOOL		bKeepPlayerInvisible[MP_GOLF_MAX_PLAYERS]
	
	STRUCTTIMER uiTimer			// Generic UI timer (used for scoreboards, etc
	STRUCTTIMER navTimer	    // Generic gameplay timer
	STRUCTTIMER inputTimer		// Generic input timer
	STRUCTTIMER cameraTimer		// Generic camera timer
	STRUCTTIMER resetTimer
	STRUCTTIMER	taskTimer
	STRUCTTIMER idleSpeechTimer
	STRUCTTIMER idleTimer
	STRUCTTIMER quitTimer       //when player quits, gives us a little delay to display stuff
	STRUCTTIMER skipTimer		//Timeout for skiping to the next player shot
	
	GOLF_SPLASH_TEXT_ENUM eLastSplashText
	SWING_STYLE			eLastPuttStyle
	SWING_STYLE			eLastSwingStyle
	
	// Moved from PLAYER struct
	OBJECT_INDEX		objLongestDriveMarker
	BLIP_INDEX			blipCenterOfCourse
	
	BLIP_INDEX			blipPlayersBalls[MP_GOLF_MAX_PLAYERS]

	BLIP_INDEX 			blipCurrentHole
	BLIP_INDEX 			blipCurrentEstimate
	BOOL 				bInputCleared
	BOOL				bWin = FALSE
	BOOL				bEndResetScriptCam = TRUE
	
	INT iStartingHole
	INT iEndingHole
	INT iXPGained
	
	
	VEHICLE_SETUP_STRUCT sPlayerVehicle
	STREAMVOL_ID steamVolNextShot
	STREAMVOL_ID streamVolScorecard
	
	TEXT_LABEL_31 sAudioContext
	TEXT_LABEL_23 sFailReason
	PED_INDEX pedAudio
	INT iCastroIndex = -1
	
	GOLF_DISPLAYED_CONTROLS_STATE golfDisplayedControls
	
	#IF GOLF_IS_MP
		PED_INDEX pedSpectating
		SCENARIO_BLOCKING_INDEX golfScenarioBlockingIndex
		PED_COMP_NAME_ENUM eFeetProp = DUMMY_PED_COMP
	#ENDIF

ENDSTRUCT

CONST_INT MAX_NUMBER_OF_GOLF_TRAIL_PROBES 13

#IF NOT GOLF_IS_AI_ONLY

SHAPETEST_INDEX golfTrailShapeTest[MAX_NUMBER_OF_GOLF_TRAIL_PROBES]
//INT iFramesForResults
INT iGolfShapeTestFlags
INT iLastSectionCompleted
VECTOR vLastValidIntersect
GOLF_LIE_TYPE eIntersectLie
INT iGolfResetSceneID = -1

BOOL 	bAllowLeaderboardUpload = TRUE
#ENDIF

#IF IS_DEBUG_BUILD

BOOL displayLines = FALSE
BOOL 		bDebugDrawForces = FALSE // DEBUG
BOOL		bReattachClub
BOOL		bDebugDrawAI
BOOL		bRepositionPlayer
BOOL		bOcclusionCheck
BOOL		bInfiniteShots
BOOL		bDisplayStats
BOOL		bMaxSpin
BOOL		bTestReplay
#IF GOLF_IS_AI_ONLY
BOOL		bAmbientGolfersOff = FALSE
#ENDIF
#ENDIF


TWEAK_FLOAT	DIST_TO_GREEN_HACK		55.0

#IF NOT GOLF_IS_AI_ONLY
TWEAK_FLOAT ACCEPTABLE_DRIVE_DIST   100.0
#ENDIF

TWEAK_FLOAT WIND_MULTIPLIER  0.5

TWEAK_FLOAT SPIN_MULTIPLIER_GROUND_DURATTION  25.0

TWEAK_FLOAT SPIN_MULTIPLIER_V_AIR  0.05
TWEAK_FLOAT SPIN_MULTIPLIER_H_AIR  0.2

TWEAK_FLOAT VELOCITY_DAMPEN_GREEN_C  2.960
TWEAK_FLOAT VELOCITY_DAMPEN_GREEN_V  0.0
TWEAK_FLOAT VELOCITY_DAMPEN_GREEN_A  0.0

TWEAK_FLOAT VELOCITY_DAMPEN_GREEN_APROACH 0.85
TWEAK_FLOAT VELOCITY_DAMPEN_FAIRWAY  0.85
TWEAK_FLOAT VELOCITY_DAMPEN_ROUGH  0.85
TWEAK_FLOAT VELOCITY_DAMPEN_BUNKER 0.35
TWEAK_FLOAT VELOCITY_DAMPEN_WATER  0.5
TWEAK_FLOAT	VELOCITY_DAMPEN_CART_PATH	0.89
TWEAK_FLOAT VELOCITY_DAMPEN_FOLIAGE -5.0

TWEAK_FLOAT CART_PATH_BOUNCE_MULTIPLIER 0.75
TWEAK_FLOAT FAIRWAY_BOUNCE_MULTIPLIER   0.5
TWEAK_FLOAT GREEN_BOUNCE_MULTIPLIER     0.3
TWEAK_FLOAT ROUGH_BOUNCE_MULTIPLIER     0.25
TWEAK_FLOAT SAND_BOUNCE_MULTIPLIER      0.0

TWEAK_FLOAT CART_BOUNCE_SPEED_OVER_GROUND     0.2
TWEAK_FLOAT FAIRWAY_BOUNCE_SPEED_OVER_GROUND  0.55
TWEAK_FLOAT GREEN_BOUNCE_SPEED_OVER_GROUND    0.55
TWEAK_FLOAT ROUGH_BOUNCE_SPEED_OVER_GROUND    0.5
TWEAK_FLOAT SAND_BOUNCE_SPEED_OVER_GROUND     0.2

TWEAK_FLOAT BALL_VELOCITY_THRESHOLD_BOUNCE  9.5
TWEAK_FLOAT BALL_VELOCITY_THRESHOLD_NORMAL  0.5
TWEAK_FLOAT BALL_VELOCITY_THRESHOLD_GREEN   0.25
TWEAK_FLOAT BALL_VELOCITY_THRESHOLD_HOLE  6.0
TWEAK_FLOAT BALL_RADIUS  4.0 // m
TWEAK_FLOAT	BALL_IN_VEHICLE_RADIUS	12.0
TWEAK_FLOAT FAR_AWAY_THRESHOLD		150.0
TWEAK_FLOAT DIST_FOR_GIMMIE	0.7
TWEAK_FLOAT MAX_ANGLE_DEVIATION 15.0
TWEAK_FLOAT GIMMIE_FORCE_RADIUS 0.1601 //0.1625 is too big, 1.60 is too small
TWEAK_FLOAT BALL_IN_HOLE_RADIUS 0.1
//FLOAT	NEAR_SHOT_RADIUS	= 50.0 //m
TWEAK_FLOAT DIST_FOR_AI_GIMMIE_EASY	1.0
TWEAK_FLOAT DIST_FOR_AI_GIMMIE_MEDIUM	1.5
TWEAK_FLOAT DIST_FOR_AI_GIMMIE_HARD 2.5

TWEAK_FLOAT SWING_METER_POS_X 0.632
TWEAK_FLOAT SWING_METER_POS_Y 0.55

FLOAT MINIMAP_POS_X = -1213.0
FLOAT MINIMAP_POS_Y = 118.179
FLOAT MINIMAP_ZOOM_RATIO = 1.0
INT MINIMAP_ANGLE = 0

TWEAK_FLOAT 		fWoodlHitTime  0.160
TWEAK_FLOAT 		fIronHitTime  0.134
TWEAK_FLOAT 		fWedgeHitTime  0.119
TWEAK_FLOAT 		fPuttHitTime  0.159

TWEAK_FLOAT	fPowerAdjustPreviewLineApproach 1.060
TWEAK_FLOAT fPowerAdjustPreviewLinePunch 1.08
TWEAK_FLOAT fLoftAdjustPreviewLine 0.04

VECTOR vApproachBallOffset = <<0.57, 0.030, 0>>
FLOAT  fApproachBallHeadingOffset = 50.0

CONST_FLOAT PREVIEWCAM_LERP         0.3
CONST_FLOAT CLOSE_PED_DIST2			1.0	//1*1

// Sand trap values
CONST_FLOAT	SAND_STEP_DISTANCE		0.5
CONST_FLOAT SAND_STEP_GAP			0.25
CONST_FLOAT	SAND_STEP_HEIGHT		1.0
CONST_FLOAT SAND_STEP_WIDTH			1.0

CONST_FLOAT GOLF_BALL_BLIP_SCALE	0.67

#IF GOLF_IS_MP
TWEAK_FLOAT fAutoSwingTime 60.0 // number of seconds before auto swing
#ENDIF

CONST_INT			iMaxNumOfAutoSwingsBeforeKick 3

BOOL		bDrivingRangeMode = FALSE// Debug
BOOL		bPerfectAccuracyMode = FALSE// Debug
BOOL		bDisableWindEffects = FALSE// Debug
BOOL		bDisableSpinEffects = FALSE


#IF NOT GOLF_IS_AI_ONLY
#IF IS_DEBUG_BUILD 
	VECTOR vDebugShotPosition
	INT iDebugShotHole
	INT iDebugShotNumber
	INT iDebugShotLie
	BOOL bDebugShotUse = FALSE
	BOOL bDebugHardCodedShotUse = FALSE
	BOOL bMakePutt = FALSE
	BOOL bStopMPTimer = FALSE
	BOOL bHitOpponentBall = FALSE
	BOOL bUseDebugWind = FALSE
	INT iSetScore
	BOOL bSetPlayerScore[4]
	BOOL bGolfPause
	
	FLOAT fDebugBlendWeight
	BOOL bUseDebugBlendWeight
#ENDIF
#ENDIF


BOOL	bFlagPassed = FALSE


CONST_INT NUM_ITERATIONS_IN_ESTIMATE 50
CONST_FLOAT MAX_EXPECTED_FLIGHT_TIME 5.0
CONST_FLOAT f_Vertical_decel -9.81
CONST_FLOAT f_forward_decel -4.0


//// ACCESSORS FOR HELPER STRUCT
///    
///    
/// PURPOSE:
///    Accessor for GOLF_HELPER long drive marker
FUNC OBJECT_INDEX GET_GOLF_HELPER_LONG_DRIVE_MARKER(GOLF_HELPERS &thisHelpers)
	RETURN thisHelpers.objLongestDriveMarker
ENDFUNC


/// PURPOSE:
///    Accessor for GOLF_HELPER shot blip
FUNC BLIP_INDEX GET_GOLF_HELPER_BALL_BLIP(GOLF_HELPERS &thisHelpers, INT iPlayer)
	RETURN thisHelpers.blipPlayersBalls[iPlayer]
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_HELPER shot blip
PROC SET_GOLF_HELPER_BALL_BLIP(GOLF_HELPERS &thisHelpers, BLIP_INDEX blipCurrentShot, INT iPlayer)
	thisHelpers.blipPlayersBalls[iPlayer] = blipCurrentShot
ENDPROC

PROC CLEANUP_GOLF_HELPER_BALL_BLIP(GOLF_HELPERS &thisHelpers, INT iPlayer)
	IF DOES_BLIP_EXIST(thisHelpers.blipPlayersBalls[iPlayer])
		REMOVE_BLIP(thisHelpers.blipPlayersBalls[iPlayer])
	ENDIF
ENDPROC

PROC CLEANUP_GOLF_HELPER_ALL_BALL_BLIPS(GOLF_HELPERS &thisHelpers)
	INT index
	FOR index = 0 TO COUNT_OF(thisHelpers.blipPlayersBalls)-1
		CLEANUP_GOLF_HELPER_BALL_BLIP(thisHelpers, index)
	ENDFOR
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_HELPER hole blip
FUNC BLIP_INDEX GET_GOLF_HELPER_HOLE_BLIP(GOLF_HELPERS &thisHelpers)
	RETURN thisHelpers.blipCurrentHole
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_HELPER hole blip
PROC SET_GOLF_HELPER_HOLE_BLIP(GOLF_HELPERS &thisHelpers, BLIP_INDEX blipCurrentHole)
	thisHelpers.blipCurrentHole = blipCurrentHole
ENDPROC



/// PURPOSE:
///    Accessor for GOLF_HELPER hole blip
PROC CLEANUP_GOLF_HELPER_HOLE_BLIP(GOLF_HELPERS &thisHelpers)
	IF DOES_BLIP_EXIST(thisHelpers.blipCurrentHole)
		REMOVE_BLIP(thisHelpers.blipCurrentHole)
	ENDIF
ENDPROC



/// PURPOSE:
///    Accessor for GOLF_HELPER hole blip
FUNC BLIP_INDEX GET_GOLF_HELPER_ESTIMATE_BLIP(GOLF_HELPERS &thisHelpers)
	RETURN thisHelpers.blipCurrentEstimate
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_HELPER hole blip
PROC SET_GOLF_HELPER_ESTIMATE_BLIP(GOLF_HELPERS &thisHelpers, BLIP_INDEX blipCurrentEstimate)
	thisHelpers.blipCurrentEstimate = blipCurrentEstimate
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_HELPER hole blip
PROC CLEANUP_GOLF_HELPER_ESTIMATE_BLIP(GOLF_HELPERS &thisHelpers)
	IF DOES_BLIP_EXIST(thisHelpers.blipCurrentEstimate)
		REMOVE_BLIP(thisHelpers.blipCurrentEstimate)
	ENDIF
ENDPROC

/* cart no longer has blip
/// PURPOSE:
///    Accessor for GOLF_HELPER hole blip
FUNC BLIP_INDEX GET_GOLF_HELPER_VEHICLE_BLIP(GOLF_HELPERS &thisHelpers)
	RETURN thisHelpers.blipCurrentVehicle
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_HELPER hole blip
PROC SET_GOLF_HELPER_VEHICLE_BLIP(GOLF_HELPERS &thisHelpers, BLIP_INDEX blipCurrentVehicle)
	thisHelpers.blipCurrentVehicle = blipCurrentVehicle
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_HELPER hole blip
PROC CLEANUP_GOLF_HELPER_VEHICLE_BLIP(GOLF_HELPERS &thisHelpers)
	REMOVE_BLIP(thisHelpers.blipCurrentVehicle)
ENDPROC
//*/

/// PURPOSE:
///    Accessor for GOLF_HELPER input cleared bool
FUNC BOOL GET_GOLF_HELPER_INPUT_CLEARED(GOLF_HELPERS &thisHelpers)
	RETURN thisHelpers.bInputCleared
ENDFUNC
	
/// PURPOSE:
///    Accessor for GOLF_HELPER input cleared bool
PROC SET_GOLF_HELPER_INPUT_CLEARED(GOLF_HELPERS &thisHelpers, BOOL bInputCleared)
	thisHelpers.bInputCleared = bInputCleared
ENDPROC	

/// PURPOSE: Helper function to get the servers game/mission state
/// RETURNS: The server game state.
FUNC GOLF_CONTROL_FLAGS GET_GOLF_CONTROL_FLAGS(GOLF_GAME &golfGame)
	RETURN golfGame.golfControlFlags
ENDFUNC

PROC OVERWRITE_GOLF_CONTROL_FLAGS(GOLF_GAME &golfGame, GOLF_CONTROL_FLAGS golfControlFlags)
	golfGame.golfControlFlags = golfControlFlags
ENDPROC

PROC SET_GOLF_CONTROL_FLAG(GOLF_GAME &golfGame, GOLF_CONTROL_FLAGS golfControlFlags)
	golfGame.golfControlFlags = golfGame.golfControlFlags | golfControlFlags
ENDPROC

PROC CLEAR_GOLF_CONTROL_FLAG(GOLF_GAME &golfGame, GOLF_CONTROL_FLAGS golfControlFlags)
	golfGame.golfControlFlags -= golfGame.golfControlFlags & golfControlFlags
ENDPROC

FUNC BOOL IS_GOLF_CONTROL_FLAG_SET(GOLF_GAME &golfGame, GOLF_CONTROL_FLAGS golfControlFlags)
	RETURN (golfGame.golfControlFlags & golfControlFlags) != GCF_NO_FLAGS
ENDFUNC

PROC RESET_GLOBAL_GOLF_DATA()
	g_sGolfGlobals.CurrrentPlayerHole = -1
	g_sGolfGlobals.GlobalGolfControlFlag = ENUM_TO_INT(GGCF_NO_FLAGS)
ENDPROC
FUNC GOLF_UI_CONTROL GET_GOLF_UI_FLAGS(GOLF_HELPERS &golfHelpers)
	RETURN golfHelpers.golfUIControl
ENDFUNC

PROC OVERWRITE_GOLF_UI_FLAGS(GOLF_HELPERS &golfHelpers, GOLF_UI_CONTROL golfUIControl)
	golfHelpers.golfUIControl = golfUIControl
ENDPROC

PROC SET_GOLF_UI_FLAG(GOLF_HELPERS &golfHelpers, GOLF_UI_CONTROL golfUIControl)
	golfHelpers.golfUIControl = golfHelpers.golfUIControl | golfUIControl
ENDPROC

PROC CLEAR_GOLF_UI_FLAG(GOLF_HELPERS &golfHelpers, GOLF_UI_CONTROL golfUIControl)
	golfHelpers.golfUIControl -= golfHelpers.golfUIControl & golfUIControl
ENDPROC

FUNC BOOL IS_GOLF_UI_FLAG_SET(GOLF_HELPERS &golfHelpers, GOLF_UI_CONTROL golfUIControl)
	RETURN (golfHelpers.golfUIControl & golfUIControl) != GUC_NONE
ENDFUNC

//Keep track is a ui element has ever been displayed
FUNC GOLF_HELP_DISPLAYED_FLAG GET_GOLF_HELP_DISPLAYED_FLAG(GOLF_HELPERS &golfHelpers)
	RETURN golfHelpers.golfHelpDisplayed
ENDFUNC

PROC OVERWRITE_GOLF_HELP_DISPLAYED_FLAG(GOLF_HELPERS &golfHelpers, GOLF_HELP_DISPLAYED_FLAG  golfHelpDisplayed)
	golfHelpers.golfHelpDisplayed = golfHelpDisplayed
ENDPROC

PROC SET_GOLF_HELP_DISPLAYED_FLAG(GOLF_HELPERS &golfHelpers, GOLF_HELP_DISPLAYED_FLAG  golfHelpDisplayed)
	golfHelpers.golfHelpDisplayed = golfHelpers.golfHelpDisplayed| golfHelpDisplayed
ENDPROC

PROC CLEAR_GOLF_HELP_DISPLAYED_FLAG(GOLF_HELPERS &golfHelpers, GOLF_HELP_DISPLAYED_FLAG  golfHelpDisplayed)
	golfHelpers.golfHelpDisplayed -= golfHelpers.golfHelpDisplayed & golfHelpDisplayed
ENDPROC

FUNC BOOL IS_GOLF_HELP_DISPLAYED_FLAG_SET(GOLF_HELPERS &golfHelpers, GOLF_HELP_DISPLAYED_FLAG  golfHelpDisplayed)
	RETURN (golfHelpers.golfHelpDisplayed & golfHelpDisplayed) != GHD_NONE
ENDFUNC

PROC SET_GOLF_HELP_CURRENTLY_DISPLAYING(GOLF_HELPERS &golfHelpers, GOLF_HELP_DISPLAYED_FLAG  golfHelpDisplaying)
	golfHelpers.golfHelpDisplaying = golfHelpers.golfHelpDisplaying| golfHelpDisplaying
ENDPROC

PROC CLEAR_GOLF_HELP_CURRENTLY_DISPLAYING(GOLF_HELPERS &golfHelpers, GOLF_HELP_DISPLAYED_FLAG  golfHelpDisplaying)
	golfHelpers.golfHelpDisplaying -= golfHelpers.golfHelpDisplaying & golfHelpDisplaying
ENDPROC

FUNC BOOL IS_GOLF_HELP_CURRENTLY_DISPLAYING(GOLF_HELPERS &golfHelpers, GOLF_HELP_DISPLAYED_FLAG  golfHelpDisplaying)
	RETURN (golfHelpers.golfHelpDisplaying & golfHelpDisplaying) != GHD_NONE
ENDFUNC

//*/
//Dialogue flag
FUNC GOLF_DIALOGUE_FLAG GET_GOLF_HELP_GOLF_DIALOGUE_FLAG(GOLF_HELPERS &golfHelpers)
	RETURN golfHelpers.golfDialogueFlag
ENDFUNC

PROC OVERWRITE_GOLF_DIALOGUE_FLAG(GOLF_HELPERS &golfHelpers, GOLF_DIALOGUE_FLAG  golfDialogueFlag)
	golfHelpers.golfDialogueFlag = golfDialogueFlag
ENDPROC

PROC SET_GOLF_DIALOGUE_FLAG(GOLF_HELPERS &golfHelpers, GOLF_DIALOGUE_FLAG  golfDialogueFlag)
	golfHelpers.golfDialogueFlag = golfHelpers.golfDialogueFlag| golfDialogueFlag
ENDPROC

PROC CLEAR_GOLF_DIALOGUE_FLAG(GOLF_HELPERS &golfHelpers, GOLF_DIALOGUE_FLAG  golfDialogueFlag)
	golfHelpers.golfDialogueFlag -= golfHelpers.golfDialogueFlag & golfDialogueFlag
ENDPROC

FUNC BOOL IS_GOLF_DIALOGUE_FLAG_SET(GOLF_HELPERS &golfHelpers, GOLF_DIALOGUE_FLAG  golfDialogueFlag)
	RETURN (golfHelpers.golfDialogueFlag & golfDialogueFlag) != GDF_NONE
ENDFUNC
//*/

FUNC GOLF_STREAMING_FLAGS GET_GOLF_STREAMING_FLAGS(GOLF_HELPERS &golfHelpers)
	RETURN golfHelpers.golfStreaming
ENDFUNC

PROC OVERWRITE_GOLF_STREAMING_FLAGS(GOLF_HELPERS &golfHelpers, GOLF_STREAMING_FLAGS golfStreaming)
	golfHelpers.golfStreaming = golfStreaming
ENDPROC

PROC SET_GOLF_STREAMING_FLAG(GOLF_HELPERS &golfHelpers, GOLF_STREAMING_FLAGS golfStreaming)
	golfHelpers.golfStreaming = golfHelpers.golfStreaming | golfStreaming
ENDPROC

PROC CLEAR_GOLF_STREAMING_FLAG(GOLF_HELPERS &golfHelpers, GOLF_STREAMING_FLAGS golfStreaming)
	golfHelpers.golfStreaming -= golfHelpers.golfStreaming & golfStreaming
ENDPROC

FUNC BOOL IS_GOLF_STREAMING_FLAG_SET(GOLF_HELPERS &golfHelpers, GOLF_STREAMING_FLAGS golfStreaming)
	RETURN (golfHelpers.golfStreaming & golfStreaming) != GSF_NO_STREAM_FLAG
ENDFUNC

PROC SET_GOLF_STATS_FLAG(GOLF_HELPERS &golfHelpers, GOLF_STATS_FLAG golfStatsFlag)
	golfHelpers.golfStatsFlag = golfHelpers.golfStatsFlag | golfStatsFlag
ENDPROC

PROC CLEAR_GOLF_STATS_FLAG(GOLF_HELPERS &golfHelpers, GOLF_STATS_FLAG golfStatsFlag)
	golfHelpers.golfStatsFlag -= golfHelpers.golfStatsFlag & golfStatsFlag
ENDPROC

FUNC BOOL IS_GOLF_STATS_FLAG_SET(GOLF_HELPERS &golfHelpers, GOLF_STATS_FLAG golfStatsFlag)
	RETURN (golfHelpers.golfStatsFlag & golfStatsFlag) != GSF_NONE
ENDFUNC

FUNC STRING GET_GOLF_TEXTURE_DICTIONARY_NAME()
	RETURN "GolfPutting"
ENDFUNC

FUNC BOOL IS_GOLF_FOURSOME_MP()
	#IF GOLF_IS_MP
		RETURN TRUE
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GOLF_PRINT_HELP(STRING txtHelp, BOOL bForever = FALSE, BOOL bNoSound = FALSE)
	IF IS_GOLF_SPLASH_DISPLAYING() AND IS_GOLF_FOURSOME_MP()
		EXIT 
	ENDIF

	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(txtHelp)
		//CDEBUG1LN(DEBUG_GOLF,"Displaying help ", txtHelp)
		bForever = bForever
		IF bForever
			IF bNoSound
				//this will probably assert
				PRINT_HELP_FOREVER_WITH_STRING_NO_SOUND (txtHelp, "")
			ELSE
				PRINT_HELP_FOREVER(txtHelp)
			ENDIF
		ELSE
			IF bNoSound
				PRINT_HELP_NO_SOUND(txtHelp)
			ELSE
				PRINT_HELP(txtHelp)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GOLF_PRINT_GOD_TEXT(STRING txtHelp)
	//IF NOT IS_THIS_PRINT_BEING_DISPLAYED(txtHelp)
		PRINT_NOW(txtHelp, DEFAULT_GOD_TEXT_TIME, 1)
	//ENDIF
ENDPROC

PROC SET_GOLF_TRAIL_INTERSECT_FLAG(BOOL bSet)
	bSet = bSet
#IF NOT GOLF_IS_AI_ONLY
	IF bSet
		SET_BIT(iGolfShapeTestFlags, 13)
	ELSE
		CLEAR_BIT(iGolfShapeTestFlags, 13)
	ENDIF
#ENDIF
ENDPROC

PROC SET_GOLF_TRAIL_STYLE_CHANGE_FLAG(BOOL bSet)
	bSet = bSet
#IF NOT GOLF_IS_AI_ONLY
	IF bSet
		SET_BIT(iGolfShapeTestFlags, 14)
	ELSE
		CLEAR_BIT(iGolfShapeTestFlags, 14)
	ENDIF
#ENDIF
ENDPROC

PROC SET_GOLF_SHAPE_TEST_CREATE_FLAG(INT index, BOOl bSet)
	bSet = bSet
	index = index
	
#IF NOT GOLF_IS_AI_ONLY
	IF bSet
		SET_BIT(iGolfShapeTestFlags, index)
	ELSE
		CLEAR_BIT(iGolfShapeTestFlags, index)
	ENDIF
#ENDIF
ENDPROC

FUNC BOOL IS_GOLF_TRAIL_INTERSECT_FLAG_SET()
#IF NOT GOLF_IS_AI_ONLY
	RETURN IS_BIT_SET(iGolfShapeTestFlags, 13)
#ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GOLF_TRAIL_STYLE_CHANGE_FLAG_SET()
#IF NOT GOLF_IS_AI_ONLY
	RETURN IS_BIT_SET(iGolfShapeTestFlags, 14)
#ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GOLF_SHAPE_TEST_CREATE_FLAG_SET(INT index)
	index = index
#IF NOT GOLF_IS_AI_ONLY
	RETURN IS_BIT_SET(iGolfShapeTestFlags, index)
#ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_GOLF_SHPAE_TEST_LAST_COMPLETED_SECTION(INT iSection)
iSection = iSection
#IF NOT GOLF_IS_AI_ONLY
	iLastSectionCompleted = iSection
#ENDIF
ENDPROC

FUNC INT GET_GOLF_SHAPE_TEST_LAST_COMPLETED_SECTION()
#IF NOT GOLF_IS_AI_ONLY
	RETURN iLastSectionCompleted
#ENDIF
	RETURN 0
ENDFUNC

PROC SET_GOLF_SHAPE_TEST_LAST_VALID_INTERSECT(VECTOR vIntersect)
vIntersect.z = vIntersect.z
#IF NOT GOLF_IS_AI_ONLY
	vLastValidIntersect =  vIntersect
#ENDIF
ENDPROC

FUNC VECTOR GET_GOLF_SHAPE_TEST_LAST_VALID_INTERSECT()
#IF NOT GOLF_IS_AI_ONLY
	RETURN vLastValidIntersect
#ENDIF

	RETURN <<0,0,0>>
ENDFUNC

PROC SET_GOLF_INTERSECT_LIE(GOLF_LIE_TYPE eLieType)
eLieType = eLieType
#IF NOT GOLF_IS_AI_ONLY
	eIntersectLie = eLieType
#ENDIF
ENDPROC

FUNC GOLF_LIE_TYPE GET_GOLF_INTERSECT_LIE()

#IF NOT GOLF_IS_AI_ONLY
	RETURN eIntersectLie
#ENDIF

	RETURN LIE_UNKNOWN
ENDFUNC

FUNC BOOL IS_GOLF_LIE_SAFE(GOLF_LIE_TYPE lieType)
//	CDEBUG1LN(DEBUG_GOLF,"Intersect lie is ", GET_GOLF_INTERSECT_LIE())

	IF lieType = LIE_FAIRWAY
	OR lieType = LIE_ROUGH
	OR lieType = LIE_GREEN
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GOLF_LIE_SAFE_FOR_TELEPORT(GOLF_LIE_TYPE lieType)

	IF lieType = LIE_FAIRWAY
	OR lieType = LIE_ROUGH
	OR lieType = LIE_GREEN
	OR lieType = LIE_CART_PATH
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_GOLF_LINE_FLAGS()
	CDEBUG1LN(DEBUG_GOLF,"Clearing line flags")
	INT i
	
	//CDEBUG1LN(DEBUG_GOLF,"Clearing golf line flags")
	REPEAT MAX_NUMBER_OF_GOLF_TRAIL_PROBES i 
		SET_GOLF_SHAPE_TEST_CREATE_FLAG(i, FALSE)
	ENDREPEAT
	
	SET_GOLF_SHPAE_TEST_LAST_COMPLETED_SECTION(0)
	SET_GOLF_TRAIL_INTERSECT_FLAG(FALSE)
	 
	 //DEBUG_PRINTCALLSTACK()
ENDPROC

FUNC VECTOR CONVERT_GOLF_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
      RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

//Finds the z value and checks if the change is too dramatic so we don't end up in a tree or something
FUNC BOOL GOLF_FIND_GROUND_Z(VECTOR &retVector, FLOAT fTolerance = 2.0, FLOAT fZOffset = 1.0)

	FLOAT fHeight
	IF GET_GROUND_Z_FOR_3D_COORD(retVector+<<0,0,fZOffset>>, fHeight)
		IF ABSF(retVector.z - fHeight) < fTolerance //make sure the change in z position isn't too dramatic
			retVector.z = fHeight
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MAKE_PUTT_SET()
	#IF IS_DEBUG_BUILD 
	#IF NOT GOLF_IS_AI_ONLY
		RETURN bMakePutt
	#ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GOLF_CAN_DISPLAY_SOCAIL_CLUB_LEADERBOARD()
	RETURN NETWORK_IS_SIGNED_ONLINE() AND NETWORK_IS_SIGNED_IN() AND SCRIPT_IS_CLOUD_AVAILABLE() AND IS_PLAYER_ONLINE()
ENDFUNC

FUNC STRING GET_GOLF_BASIC_ANIMATION_DICTIONARY_NAME()
	RETURN "MINI@GOLFAI"
ENDFUNC

FUNC STRING GET_GOLF_EXTRA_ANIMATION_DICTIONARY_NAME()
	RETURN "MINI@GOLF"
ENDFUNC

FUNC STRING GET_GOLF_ANIMATION_DICTIONARY_NAME(BOOL bUseExtraAnimations = FALSE)

	#IF GOLF_IS_AI_ONLY
		RETURN GET_GOLF_BASIC_ANIMATION_DICTIONARY_NAME()
	#ENDIF

	IF NOT bUseExtraAnimations
		RETURN GET_GOLF_BASIC_ANIMATION_DICTIONARY_NAME()
	ELSE
		RETURN GET_GOLF_EXTRA_ANIMATION_DICTIONARY_NAME()
	ENDIF
ENDFUNC

FUNC BOOL IS_GOLF_FULL_ROUND(GOLF_HELPERS &thisHelpers)
	RETURN thisHelpers.iStartingHole = 0 AND thisHelpers.iEndingHole = 8
ENDFUNC

FUNC BOOL IS_HOLE_IN_PLAY(GOLF_HELPERS &thisHelpers, INT iHole)
	RETURN thisHelpers.iStartingHole <= iHole AND iHole <= thisHelpers.iEndingHole
ENDFUNC

FUNC BOOL IS_GOLF_CAM_ACTIVE(CAMERA_INDEX &golfCam)
	IF DOES_CAM_EXIST(golfCam)
		IF IS_CAM_ACTIVE(golfCam)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GOLF_CAM_RENDERING(CAMERA_INDEX &golfCam)
	IF IS_GOLF_CAM_ACTIVE(golfCam)
		RETURN IS_CAM_RENDERING(golfCam)
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC enumCharacterList GET_GOLF_PED_ENUM(PED_INDEX pedIndex)

	IF GET_PLAYER_PED_ENUM(pedIndex) = NO_CHARACTER
		RETURN GET_NPC_PED_ENUM(pedIndex)
	ENDIF
	
	RETURN GET_PLAYER_PED_ENUM(pedIndex)
ENDFUNC

FUNC BOOL GOLF_SHOULD_USE_FOREIGN_DISTANCE()

	RETURN DOES_CURRENT_LANGUAGE_USE_METRIC_SYSTEM() 
	AND GET_CURRENT_LANGUAGE() != LANGUAGE_JAPANESE AND GET_CURRENT_LANGUAGE() != LANGUAGE_CHINESE AND GET_CURRENT_LANGUAGE() != LANGUAGE_KOREAN
	AND GET_CURRENT_LANGUAGE() != LANGUAGE_CHINESE_SIMPLIFIED
	//RETURN TRUE
ENDFUNC


PROC FREEZE_GOLF_BALL(OBJECT_INDEX golfBall, BOOL bFreeze)
	FREEZE_ENTITY_POSITION(golfBall, bFreeze)
ENDPROC

#IF NOT GOLF_IS_AI_ONLY
USING "golf_stats.sch"
#ENDIF

