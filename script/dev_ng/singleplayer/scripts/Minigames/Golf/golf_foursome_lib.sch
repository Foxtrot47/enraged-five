USING "golf_foursome.sch"
USING "golf_ai.sch"
USING "golf_input.sch"
USING "golf_ui.sch"
USING "golf_player_lib.sch"
USING "golf_dialogue.sch"
USING "chase_hint_cam.sch"

/// PURPOSE:
///    Creates and maintains any required blips for the local player, as well as managing the flag for the current hole, and whichever in-world checkpoints are needed
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
///    thisPlayer - 
PROC GOLF_MANAGE_HOLE_BLIPS_AND_OBJECTS(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	
//	IF IS_TEE_SHOT(thisCourse, thisFoursome) AND GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_NAVIGATE_TO_SHOT
//		DRAW_CHECKPOINT(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), 1.0, 255, 255, 0)
//	ENDIF
	IF NOT DOES_BLIP_EXIST(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, 0))
		SET_GOLF_HELPER_BALL_BLIP(thisHelpers, CREATE_BLIP_FOR_COORD(GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL_POSITION(thisFoursome)), 0)
		SET_BLIP_COLOUR(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, 0), BLIP_COLOUR_BLUE)
		SET_BLIP_SCALE(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, 0), GOLF_BALL_BLIP_SCALE)
		TEXT_LABEL_23 blipName = "YOUR_BALL"
		
		IF IS_TEE_SHOT(thisCourse, thisFoursome)
			blipName += "_T"
		ENDIF
		SET_BLIP_NAME_FROM_TEXT_FILE(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, 0), blipName)
	ENDIF
	CLEANUP_GOLF_HELPER_BALL_BLIP(thisHelpers, 1)
	CLEANUP_GOLF_HELPER_BALL_BLIP(thisHelpers, 2)
	CLEANUP_GOLF_HELPER_BALL_BLIP(thisHelpers, 3)
	
	BLIP_INDEX blipShotEstimate = GET_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers)
	IF DOES_BLIP_EXIST(blipShotEstimate)
		REMOVE_BLIP(blipShotEstimate)
	ENDIF
	
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		INT iHole
		VEHICLE_INDEX playerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(playerVehicle) AND NOT IS_ENTITY_DEAD(playerVehicle)
			REPEAT GET_GOLF_COURSE_NUM_HOLES(thisCourse) iHole
				IF DOES_ENTITY_EXIST(GET_GOLF_HOLE_FLAG(thisCourse, iHole)) AND NOT IS_ENTITY_DEAD(GET_GOLF_HOLE_FLAG(thisCourse, iHole))
					IF IS_ENTITY_TOUCHING_ENTITY(GET_GOLF_HOLE_FLAG(thisCourse, iHole), playerVehicle)
						CDEBUG1LN(DEBUG_GOLF,"Player touching flag ", iHole)
						FREEZE_ENTITY_POSITION(GET_GOLF_HOLE_FLAG(thisCourse, iHole), FALSE)
						SET_ENTITY_DYNAMIC(GET_GOLF_HOLE_FLAG(thisCourse, iHole), TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL SHOULD_DO_NEXT_SHOT(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	PED_INDEX pedGolfer = GET_GOLF_FOURSOME_LOCAL_PLAYER_PED(thisFoursome) 
	IF IS_PED_IN_ANY_VEHICLE(pedGolfer)
		VEHICLE_INDEX vehGolfer = GET_VEHICLE_PED_IS_IN(pedGolfer)
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		SET_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_WAS_IN_CART)
		IF NOT IS_VEHICLE_STOPPED(vehGolfer)
			IF GET_SCRIPT_TASK_STATUS( pedGolfer, SCRIPT_TASK_VEHICLE_TEMP_ACTION) != PERFORMING_TASK
				TASK_VEHICLE_TEMP_ACTION(pedGolfer, vehGolfer, TEMPACT_BRAKE, 1000)
			ENDIF
		ELSE
			IF GET_SCRIPT_TASK_STATUS( pedGolfer, SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
				TASK_LEAVE_ANY_VEHICLE(pedGolfer)
			ENDIF
		ENDIF
	ELSE
		IF IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_WAS_IN_CART)
			IF NOT IS_TIMER_STARTED(thisHelpers.navTimer)
				START_TIMER_NOW_SAFE(thisHelpers.navTimer)
				TASK_GO_TO_COORD_ANY_MEANS(pedGolfer, GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL_POSITION(thisFoursome),  PEDMOVE_WALK, NULL)
			ENDIF
		ENDIF
		IF TIMER_DO_ONCE_WHEN_READY( thisHelpers.navTimer, 0.75) OR NOT IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_WAS_IN_CART)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC INC_GOLF_STAT_SCORE_COUNT(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_PLAYER &thisPlayer, GOLF_HELPERS &thisHelpers, BOOL bEndGame)
	
	INT iCurrentHole = PICK_INT(bEndGame, thisHelpers.iEndingHole, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) - 1)
	INT shotDiff = GET_GOLF_PLAYER_HOLE_SCORE(thisPlayer, iCurrentHole) - GET_GOLF_HOLE_PAR(thisCourse, iCurrentHole)
	INT holePar = GET_GOLF_HOLE_PAR(thisCourse, iCurrentHole)
	
	UNUSED_PARAMETER(thisGame)
	
	CDEBUG1LN(DEBUG_GOLF,"Checking to increase score stats after playing hole ", iCurrentHole)
	
	SWITCH shotDiff
		CASE -4		
			IF holePar = 5		// Like this will ever happen
				SET_GOLF_STAT_GOT_HOLE_IN_ONE()
			ENDIF
		BREAK
		CASE -3
			IF holePar = 4
				SET_GOLF_STAT_GOT_HOLE_IN_ONE()
			ELSE
				//Eagle
			ENDIF
		BREAK
		CASE -2
			IF holePar = 3
				SET_GOLF_STAT_GOT_HOLE_IN_ONE()
			ELSE
				//Eagle
			ENDIF
		BREAK
		CASE -1
			INC_GOLF_STAT_NUM_BIRDIES()
		BREAK
		CASE 0
			//Par
		BREAK
		CASE 1
			//Bogey
		BREAK
		CASE 2
			//Bogey2
		BREAK
		CASE 3
			//Bogey3
		BREAK
		CASE 4
			//Bogey4
		BREAK
		CASE 5
			//Bogey5
		BREAK
		DEFAULT
			EXIT
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Manages the game of golf itself for a foursome. 
/// PARAMS:
///    thisGame - 
///    thisCourse - 
///    thisFoursome - 
///    allPlayers - 
///    thisHelpers - 
/// RETURNS:
///    Returns GMS_CLEANUP if the foursome is in a state that requires script cleanup (exit, finish) otherwise returns GMS_PLAY_GOLF
FUNC GOLF_MINIGAME_STATE GOLF_PLAY_GOLF(GOLF_GAME &thisGame,GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &allPlayers[], GOLF_HELPERS &thisHelpers)
	INT playerIndex
	BOOL bDoRematch
	
	SWITCH GET_GOLF_FOURSOME_STATE(thisFoursome)
		CASE GGS_NAVIGATE_TO_SHOT
			
			IF bDebugSpew DEBUG_MESSAGE("GMS_NAVIGATE_TO_SHOT") ENDIF
			
			IF IS_GOLF_FOURSOME_MP()
				SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_TAKING_SHOT)
				RETURN GMS_PLAY_GOLF
			ENDIF
			
			CLEAR_GOLF_SPLASH()
			
			GOLF_MONITOR_CLUB_WEAPONS(thisFoursome, thisGame, FALSE)
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iGolfResetSceneID)
				ADD_GOLF_FOURSOME_TO_GROUP(thisFoursome)
			ENDIF
			
			IF IS_TEE_SHOT(thisCourse, thisFoursome)
				IF DOES_BLIP_EXIST(thisCourse.blipFlag)
					REMOVE_BLIP(thisCourse.blipFlag)
				ENDIF
			ELSE
				// If we aren't on a tee shot, change the ball blip to blue. This will be the objective location for player
				IF DOES_BLIP_EXIST(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, 0))
					IF GET_BLIP_COLOUR(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, 0)) = GET_GOLF_PLAYER_BLIP_COLOUR(0)
						//CDEBUG1LN(DEBUG_GOLF, "GOLF_PLAY_GOLF: Setting the player blip blue for navigation")  
						SET_BLIP_COLOUR(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, 0), BLIP_COLOUR_BLUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_GAMEPLAY_CAM_RENDERING()
				IF NOT IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), TRUE)
					IF GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) > 1
						CDEBUG1LN(DEBUG_GOLF,"Shake%% - 1")
						SET_CONTROL_SHAKE(PLAYER_CONTROL, 150, 250)
					ENDIF
				ENDIF
			ELSE
				IF IS_ANY_GOLF_CAM_RENDERING(thisHelpers)
					GOLF_END_CUTSCENE_TO_FIRST_PERSON_CAMERA_EFFECT()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ENDIF
			ENDIF
			
			IF IS_RADAR_HIDDEN()
				DISPLAY_RADAR(TRUE)
			ENDIF
			
			GOLF_MANAGE_CART_DIALOGUE(thisFoursome, thisCourse, thisHelpers, allPlayers)
			
			IF NOT IS_GOLF_FOURSOME_MP() AND NOT IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_APPROCH_BALL_FOOT_DISPLAYED)
				IF IS_TIMER_PAUSED(thisHelpers.uiTimer)
					UNPAUSE_TIMER(thisHelpers.uiTimer)
				ENDIF
				IF IS_TEE_SHOT(thisCourse, thisFoursome) AND GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome) = 0
					START_TIMER_NOW(thisHelpers.uiTimer)
					IF GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) = 0
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GOLF_STAT_HELP")
							
						ELSE
							IF NOT TIMER_DO_WHEN_READY(thisHelpers.uiTimer, 0.1)
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FLY_INTRO2")
								AND GET_GOLF_STAT_ROUNDS_PLAYED() = 0
									ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
									GOLF_PRINT_HELP("GOLF_STAT_HELP")
									RESTART_TIMER_NOW(thisHelpers.uiTimer)
								ENDIF
								PRINT_NOW("NAV1", DEFAULT_GOD_TEXT_TIME, 0)
							ELIF TIMER_DO_WHEN_READY(thisHelpers.uiTimer, 7.5)		
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FLY_INTRO2")
								AND GET_GOLF_STAT_ROUNDS_PLAYED() = 0
									ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
									GOLF_PRINT_HELP("GOLF_STAT_HELP")
									RESTART_TIMER_NOW(thisHelpers.uiTimer)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT TIMER_DO_WHEN_READY(thisHelpers.uiTimer, 0.1)	
							PRINT_NOW("NAV2", DEFAULT_GOD_TEXT_TIME, 0 )
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PLAYER_TASKED)
				//FORCE_PED_MOTION_STATE(GET_GOLF_FOURSOME_LOCAL_PLAYER_PED(thisFoursome), MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT )
				//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				START_TIMER_NOW_SAFE(thisHelpers.taskTimer)
				CLEAR_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_PLAYER_TASKED)
			ENDIF
			IF TIMER_DO_ONCE_WHEN_READY( thisHelpers.taskTimer, 5.0)
				CLEAR_PED_TASKS(GET_GOLF_FOURSOME_LOCAL_PLAYER_PED(thisFoursome))
			ENDIF
			IF thisHelpers.bPlayerInVehicle AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				thisHelpers.bPlayerInVehicle = FALSE
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_NAVIGATE)
			ELIF NOT thisHelpers.bPlayerInVehicle AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				thisHelpers.bPlayerInVehicle = TRUE
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_NAVIGATE)		
			ENDIF
			
			GOLF_MANAGE_HOLE_BLIPS_AND_OBJECTS(thisCourse, thisFoursome, thisHelpers)
			GOLF_MANAGE_NAVIGATE_INPUT(thisGame, thisFoursome.golfState, thisFoursome, thisCourse, thisHelpers) // NOTE THIS CAN CHANGE GOLF STATE
			//GOLF_MANAGE_NAVIGATE_AI(thisCourse, thisFoursome)
			GOLF_MANAGE_NAVIAGTE_AI_GROUP(thisGame, thisFoursome, thisCourse)
			GOLF_MANAGE_NAVIGATE_UI(thisHelpers, thisCourse, thisFoursome)
			GOLF_MANAGE_AMBIENT_SPEECH(thisGame, thisHelpers, thisCourse, thisFoursome, FALSE, FALSE, FALSE)
			GOLF_DISPLAY_CONTROLS(thisHelpers, FALSE)

			IF IS_GOLF_COORD_OUTSIDE_BOUNDARY_PLANES(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			AND IS_GOLF_COORD_OUTSIDE_HOLE_ONE_BOUNDING_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				IF bDebugSpew DEBUG_MESSAGE("GMS_NAVIGATE_TO_SHOT - player not in inner course bounds, moving to GMS_NAVIGATE_OUT_OF_BOUNDS") ENDIF
				SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_NAVIGATE_OUT_OF_BOUNDS)
				REMOVE_GOLF_CLUB_WEAPON(PLAYER_PED_ID(), FALSE, FALSE)
				SET_BLIP_ALPHA(thisHelpers.blipPlayersBalls[0], 0)
				thisHelpers.blipCenterOfCourse = CREATE_BLIP_FOR_COORD(GET_GOLF_COURSE_CENTER(thisCourse))
				SET_BLIP_COLOUR(thisHelpers.blipCenterOfCourse, ENUM_TO_INT(BLIP_COLOUR_YELLOW))
				RESTART_TIMER_NOW(thisHelpers.resetTimer)
				PRINT_NOW("OOB", DEFAULT_GOD_TEXT_TIME, 0 )
			ELIF GOLF_IS_PLAYER_APPROACHING_BALL(thisFoursome.playerCore[0])
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_PED_ON_MOUNT(PLAYER_PED_ID())
					IF bDebugSpew DEBUG_MESSAGE("GMS_NAVIGATE_TO_SHOT - player approaches shot") ENDIF
					IF SHOULD_DO_NEXT_SHOT(thisFoursome, thisHelpers)
						STOP_GAMEPLAY_HINT()
						SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_TAKING_SHOT)
						CLEAR_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_PLAYER_NAV_HEADING | GOLF_WAS_IN_CART)
						CLEAR_GOLF_HELP_DISPLAYED_FLAG(thisHelpers, GHD_APPROCH_BALL_FOOT_DISPLAYED)
						SET_GOLF_CONTROL_FLAG(thisGame, GCF_PLAYER_SKIP)
						IF NOT IS_TEE_SHOT(thisCourse, thisFoursome)
							SET_GOLF_CONTROL_FLAG(thisGame, GCF_PLAYER_NO_SKIP) // We walked to the shot - signal to hide the player
							CDEBUG1LN(DEBUG_GOLF,"Player walked to shot")
							SET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome, 0)
							SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome, GPS_APPROACH_BALL)
						ENDIF
						SET_GOLF_MINIMAP(thisFoursome, thisCourse)
						IF NOT IS_PED_WALKING(PLAYER_PED_ID()) AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
						AND NOT IS_PED_SPRINTING(PLAYER_PED_ID())
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						ENDIF
						IF IS_TIMER_STARTED(thisHelpers.uiTimer)
							CANCEL_TIMER(thisHelpers.uiTimer)
						ENDIF
						REMOVE_GOLF_FOURSOME_FROM_GROUP(thisFoursome)
						//GOLF_ATTACH_CLUB_TO_PLAYER(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag)
					ENDIF
				ELIF NOT IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_APPROCH_BALL_FOOT_DISPLAYED)
				
					TEXT_LABEL_23 txtLeaveCart
					txtLeaveCart = "LEAVE_CART"
					
					IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_MARKER(thisFoursome, 0))
						txtLeaveCart += "_M"
					ELIF IS_TEE_SHOT(thisCourse, thisFoursome)
						txtLeaveCart += "_T"
					ENDIF
						
					SET_GOLF_HELP_DISPLAYED_FLAG(thisHelpers, GHD_APPROCH_BALL_FOOT_DISPLAYED)
					GOLF_PRINT_GOD_TEXT(txtLeaveCart)
				ENDIF
			ENDIF
			
//			IF NOT thisHelpers.bJustExitQuit AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(thisHelpers.localChaseHintCamStruct, GET_GOLF_PLAYER_BALL_POSITION(thisFoursome.playerCore[GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)]))
//			ENDIF
			
		BREAK
		
		CASE GGS_NAVIGATE_OUT_OF_BOUNDS
			IF bDebugSpew DEBUG_MESSAGE("GMS_NAVIGATE_OUT_OF_BOUNDS") ENDIF
			//GOLF_MONITOR_DISTANCE_TO_SHOT()
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
			IF IS_TIMER_STARTED(thisHelpers.uiTimer)
				PAUSE_TIMER(thisHelpers.uiTimer)
			ENDIF
			
			//GOLF_MANAGE_NAVIGATE_INPUT(thisGame, thisFoursome.golfState, allPlayers[0], thisFoursome, thisHelpers)
			IF NOT IS_GOLF_COORD_OUTSIDE_BOUNDARY_PLANES(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				SET_BLIP_ALPHA(thisHelpers.blipPlayersBalls[0], 255)
				REMOVE_BLIP(thisHelpers.blipCenterOfCourse)
				SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_NAVIGATE_TO_SHOT)
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_NAVIGATE)
				CANCEL_TIMER(thisHelpers.resetTimer)
				CLEAR_PRINTS()
			ELIF TIMER_DO_ONCE_WHEN_READY(thisHelpers.resetTimer, 15.0)
				
				#IF NOT GOLF_IS_MP
					thisHelpers.sFailReason = "QUIT_NAV"
					MG_INIT_FAIL_SPLASH_SCREEN(splashFail)
				#ENDIF
				
				REMOVE_BLIP(thisHelpers.blipCenterOfCourse)
				SET_GOLF_HELP_DISPLAYED_FLAG(thisHelpers, GHD_GOLF_TERMINATED_DISPLAYED)
				
				IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_IS_FRIEND_ACTIVITY)
					RESET_FRIEND_FOR_CLEANUP(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 1))
					RESET_FRIEND_FOR_CLEANUP(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 2))
				ENDIF
				
				RETURN GMS_FAIL
			ENDIF
		BREAK
		
		CASE GGS_TAKING_SHOT
			IF bDebugSpew DEBUG_MESSAGE("GMS_TAKING_SHOT") ENDIF
			
			// If we are taking the shot, then let's change the player's ball back to the appropriate ball color
			IF DOES_BLIP_EXIST(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, 0))
				IF GET_BLIP_COLOUR(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, 0)) != GET_GOLF_PLAYER_BLIP_COLOUR(0)
					//CDEBUG1LN(DEBUG_GOLF, "GOLF_PLAY_GOLF: Setting the player blip white for shot duration")  
					SET_BLIP_COLOUR(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, 0), GET_GOLF_PLAYER_BLIP_COLOUR(0))
				ENDIF
			ENDIF
			
			GOLF_MANAGE_CART_DIALOGUE(thisFoursome, thisCourse, thisHelpers, allPlayers)
			
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) != GPS_BALL_IN_FLIGHT // when ball is in flght, load collision in direction it will move
			AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) != GPS_BALL_AT_REST
			AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) > GPS_APPROACH_BALL
				IF STREAMVOL_IS_VALID(thisHelpers.steamVolNextShot)
					STREAMVOL_DELETE(thisHelpers.steamVolNextShot)
				ENDIF
			ENDIF
			
			GOLF_PLAYER_TAKE_SHOT(thisGame, thisCourse, allPlayers[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisFoursome, thisHelpers, allPlayers)
			
			IF ARE_ALL_PLAYERS_DONE_WITH_HOLE(thisFoursome)
			AND NOT IS_GOLF_FOURSOME_MP() //in MP moving to another hole is handled elsewhere
				DEBUG_MESSAGE("ARE_ALL_PLAYERS_DONE_WITH_HOLE")
				CLEAR_FOCUS()
				IF GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) < 8
					SET_END_OF_HOLE_STREAMVOL(thisHelpers, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
				ENDIF
				SET_FOURSOME_NEXT_GOLFER_WEIGHT_BASED_ON_WHO_WON_HOLE(thisFoursome, allPlayers, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
				SET_GOLF_CONTROL_FLAG(thisGame, GCF_PLAYER_DONE_WITH_HOLE)
				GOLF_CLEANUP_CURRENT_HOLE(thisCourse, thisFoursome)
				
				REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) playerIndex 
					SET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, playerIndex,  GPS_PLACE_BALL)
					SET_GOLF_FOURSOME_INDEXED_PLAYER_LIE(thisFoursome, playerIndex, <<0,0,0>>,  LIE_TEE)
					SET_GOLF_FOURSOME_INDEXED_PLAYER_SHOT_NORMAL(thisFoursome, playerIndex, <<0,0,-1>>)
				ENDREPEAT
				
				IF GOLF_MOVE_TO_NEXT_HOLE(thisGame, thisCourse, thisFoursome, thisHelpers.iEndingHole, allPlayers, TRUE)					
					//GAME ENDED
					GOLF_SETUP_SCOREBOARD_KEYS(thisHelpers, TRUE, FALSE, TRUE, TRUE, TRUE)
					SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_SCORECARD_END_GAME)
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), FALSE)
					INC_GOLF_PLAYER_STRENGTH()
					
					thisHelpers.eCurrentGolfUIDisplay = GOLF_DISPLAY_NONE
					IF GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) > 1
						IF DID_PLAYER_WIN_GOLF(thisFoursome, allPlayers)
							IF IS_GOLF_GAME_TIE(thisFoursome, allPlayers) 
								PLAY_GOLF_SPLASH("GOLF_TIE", "FLY_TITLE", GOLF_SPLASH_CENTER, HUD_COLOUR_WHITE, DEFAULT, TRUE)
							ELSE // Player won, alone
								thisHelpers.bWin = TRUE
								INC_GOLF_STAT_NUM_WINS() //you beat the ai
								PLAY_GOLF_SPLASH("GOLF_YOU_WIN", "FLY_TITLE", GOLF_SPLASH_CENTER, HUD_COLOUR_YELLOW, DEFAULT, TRUE)
								MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
							ENDIF
						ELSE //someone else won
							PLAY_GOLF_SPLASH("GOLF_YOU_LOST", "FLY_TITLE", GOLF_SPLASH_CENTER, HUD_COLOUR_RED, DEFAULT, TRUE)
						ENDIF
					ELSE
						INT iOverPar 
						iOverPar = GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)]) - GET_GOLF_COURSE_TOTAL_PAR(thisCourse)
						MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
						IF iOverPar <= 0
							//you beat par when playing by yourself
							PLAY_GOLF_SPLASH("GOLF_DID_WELL", "FLY_TITLE", GOLF_SPLASH_CENTER, HUD_COLOUR_YELLOW, DEFAULT, TRUE)
							thisHelpers.bWin = TRUE
						ELSE
							PLAY_GOLF_SPLASH_WITH_INT("GOLF_OVER_PAR", iOverPar, "FLY_TITLE", GOLF_SPLASH_CENTER, HUD_COLOUR_RED, DEFAULT, TRUE)
						ENDIF
					ENDIF
					
					DO_END_OF_GOLF_GAME_BAG_CUTSCENE(thisFoursome, thisHelpers, thisGame, thisCourse)
				ELSE
					//HOLE ENDED
					GOLF_SETUP_SCOREBOARD_KEYS(thisHelpers, TRUE, TRUE, FALSE, FALSE, FALSE)
					SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_SCORECARD_END_HOLE)
					GOLF_INIT_CURRENT_HOLE(thisCourse, thisFoursome)
					
					SET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome, FIND_NEXT_SHOOTER(thisFoursome, thisCourse))
					
					SET_END_OF_HOLE_CAMERA(thisHelpers, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) - 1)
					REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) playerIndex
						CLEAR_GOLF_PLAYER_CONTROL_FLAG(thisFoursome.playerCore[playerIndex], GOLF_CART_ARRIVED)
						CLEAR_GOLF_PLAYER_CONTROL_FLAG(thisFoursome.playerCore[playerIndex], GOLF_STOP_USING_CART)
					ENDREPEAT
				ENDIF
				
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
			ELIF ARE_ALL_PLAYERS_RECENTLY_DONE_WITH_TEE_SHOT(thisFoursome, thisCourse)
			AND IS_PLAYER_DONE_WITH_SHOT(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
				IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_LOCAL OR GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER
					CLEAR_FOCUS()
					CLEANUP_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome)
					RESET_FOURSOME_NEXT_GOLFER_WEIGTHS(thisFoursome)
					SET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome, FIND_NEXT_SHOOTER(thisFoursome, thisCourse))
					SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_APPROACH_BALL)
					SET_GOLF_PLAYER_CARD_CURRENT_PLAYER(thisFoursome, thisHelpers, allPlayers)
					SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_NAVIGATE_TO_SHOT)
					SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_NAVIGATE | GUC_REFRESH_PERMANENT)
					CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_PLAYER_NO_SKIP) // Clear this flag when moving to navigate
					SET_ENTITY_VISIBLE( GET_GOLF_PLAYER_PED(thisFoursome.playerCore[0]), TRUE)
					CDEBUG1LN(DEBUG_GOLF,"Player walked to shot - moving to navigate 1 - SHOW PLAYER")

					IF ARE_ALL_PLAYERS_ON_THE_GREEN(thisFoursome, thisCourse)
						// Delete the flag as everyone is on the green
						CLEANUP_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
					ENDIF
				ENDIF
			ELIF IS_PLAYER_DONE_WITH_SHOT(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
				CANCEL_TIMER(thisHelpers.uiTimer)
				CLEAR_FOCUS()
				
				IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) != LIE_TEE
					thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)].iNextGolferWeight = 0
				ENDIF
				
				IF NOT IS_GOLF_FOURSOME_MP()
					GRAB_WIND_FOR_HOLE(thisGame, thisFoursome, ciGOLF_OPTION_SUNNY)
				ENDIF
				
				IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_LOCAL OR GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER
					CDEBUG1LN(DEBUG_GOLF,"IS_PLAYER_DONE_WITH_SHOT - FIND_NEXT_SHOOTER")
					
					IF  GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = 0
						CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_PLAYER_SKIP)
					ENDIF
					
					//IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_OOB)
						SET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome, FIND_NEXT_SHOOTER(thisFoursome, thisCourse))
						SET_GOLF_PLAYER_CARD_CURRENT_PLAYER(thisFoursome, thisHelpers, allPlayers)
					//ENDIF
					
					IF IS_TEE_SHOT(thisCourse, thisFoursome)
						SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_PLACE_BALL)
					ELSE
						RESET_FOURSOME_NEXT_GOLFER_WEIGTHS(thisFoursome)
						SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_APPROACH_BALL)
					ENDIF
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = 0 AND NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_PLAYER_SKIP)
						SET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome, 0)				
						SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_NAVIGATE)

						REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) playerIndex
							CLEAR_GOLF_PLAYER_CONTROL_FLAG(thisFoursome.playerCore[playerIndex], GOLF_CART_ARRIVED)
						ENDREPEAT
						
						SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_NAVIGATE_TO_SHOT)
						CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_PLAYER_NO_SKIP) // Clear this flag when moving to navigate
						SET_ENTITY_VISIBLE( GET_GOLF_PLAYER_PED(thisFoursome.playerCore[0]), TRUE)
						CDEBUG1LN(DEBUG_GOLF,"Player walked to shot - moving to navigate 2 - SHOW PLAYER")

					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE GGS_SCORECARD_END_HOLE
			IF NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SCORECARD_INITIAL_LOAD_DONE)
				CDEBUG1LN(DEBUG_GOLF,"Check initial load for scorecard camera")
				IF STREAMVOL_IS_VALID(thisHelpers.streamVolScorecard)
//					IF STREAMVOL_HAS_LOADED(thisHelpers.streamVolScorecard)
						CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_END_GOLF_SCOREBOARD)
						DISPLAY_SCOREBOARD(thisHelpers, TRUE)
						
						CDEBUG1LN(DEBUG_GOLF, "GSF_SCORECARD_INITIAL_LOAD_DONE set")
						STREAMVOL_DELETE(thisHelpers.streamVolScorecard)
						SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_SCORECARD_INITIAL_LOAD_DONE)
						SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_SCORECARD_STREAMVOL_LOADED)
//					ENDIF
				ELSE
					SET_END_OF_HOLE_STREAMVOL(thisHelpers, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
				ENDIF
			ENDIF
			IF bDebugSpew DEBUG_MESSAGE("GMS_SCORECARD_END_HOLE") ENDIF
			
			IF IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SCORECARD_INITIAL_LOAD_DONE)
				IF NOT IS_GOLF_STATS_FLAG_SET(thisHelpers, GSF_UPDATED_STAT_SCORE)
					SET_GOLF_STATS_FLAG(thisHelpers, GSF_UPDATED_STAT_SCORE)
					INC_GOLF_STAT_SCORE_COUNT(thisGame, thisFoursome, thisCourse, allPlayers[GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)], thisHelpers, FALSE)
					PLAY_GOLF_SCORECARD_SOUND_EFFECT(thisGame, thisFoursome, thisCourse, allPlayers[GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)], FALSE)
					CLEANUP_GOLF_FOURSOME_CLUBS(thisFoursome)
					DISABLE_GOLF_MINIMAP()
				ENDIF
				
				IF DOES_CAM_EXIST(thisHelpers.camFlight)
					DESTROY_CAM(thisHelpers.camFlight)
				ENDIF
				CLEANUP_GOLF_HELPER_ALL_BALL_BLIPS(thisHelpers)
				
				SET_END_OF_HOLE_CAMERA(thisHelpers, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) - 1)
				
				IF IS_GOLF_CAM_RENDERING(thisHelpers.camNavigate2)
					CREATE_GOLF_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) - 1)
				ENDIF
				
				IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SOCIAL_CLUB_BOARD_DISPLAYED)
					GOLF_MANAGE_SCORECARD_INPUT(thisFoursome.golfState, thisGame, thisHelpers, bDoRematch)
				ENDIF
				
				IF NOT IS_RADAR_HIDDEN()
					DISPLAY_RADAR(FALSE)
				ENDIF
				
				IF GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_HOLE
					IF STREAMVOL_IS_VALID(thisHelpers.streamVolScorecard)
						STREAMVOL_DELETE(thisHelpers.streamVolScorecard)
					ENDIF
					CLEAR_GOLF_STREAMING_FLAG(thisHelpers, GSF_SCORECARD_INITIAL_LOAD_DONE)
					CLEAR_GOLF_STREAMING_FLAG(thisHelpers, GSF_SCORECARD_STREAMVOL_LOADED)
					CDEBUG1LN(DEBUG_GOLF,"Golf foursome state does not equal end hole")
					
					IF NOT IS_GOLF_FOURSOME_MP()
						SET_GOLF_PLAYER_CARD_CURRENT_PLAYER(thisFoursome, thisHelpers, allPlayers, -1, TRUE)
						GOLF_END_CUTSCENE_TO_FIRST_PERSON_CAMERA_EFFECT()
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						GOLF_MONITOR_CLUB_WEAPONS(thisFoursome, thisGame, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_GLOBAL_GOLF_CONTROL_FLAG(GGCF_PLAYER_DONE_WITH_HOLE)
						SETUP_GOLF_NAV_CONTROLS(thisHelpers, TRUE, FALSE)
					ENDIF
					
					
					CLEAR_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_HOLE)
					GOLF_UPDATE_END_OF_HOLE_STATS(IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome), GOLF_SUCCESSFUL_DRIVE), TRUE)
					CLEAR_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG(thisFoursome,0, GOLF_SUCCESSFUL_DRIVE)
					CLEAR_GOLF_STATS_FLAG(thisHelpers, GSF_UPDATED_STAT_SCORE)
					DISPLAY_SCOREBOARD(thisHelpers, FALSE)
					CLEAR_FOCUS()
					SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_NAVIGATE_TO_SHOT)
					CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_PLAYER_NO_SKIP) // Clear this flag when moving to navigate
					CLEAR_GOLF_DIALOGUE_FLAG(thisHelpers, GDF_PLAYER_GET_IN_CART)
					
					#IF NOT GOLF_IS_MP
						SET_ENTITY_VISIBLE( GET_GOLF_PLAYER_PED(thisFoursome.playerCore[0]), TRUE)
						DISPLAY_RADAR(TRUE)
					#ENDIF
					CDEBUG1LN(DEBUG_GOLF,"Player walked to shot - moving to navigate 3 - SHOW PLAYER")
					
					SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_NAVIGATE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
					RESTART_TIMER_NOW(thisHelpers.uiTimer)
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					GOLF_DISPLAY_CONTROLS(thisHelpers, FALSE)
				ELSE
					IF NOT IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PLAYER_REMOVED)
						GOLF_DISPLAY_CONTROLS(thisHelpers, TRUE)
					ENDIF
				ENDIF
				
				#IF NOT GOLF_IS_MP
					GOLF_MANAGE_NAVIAGTE_AI_GROUP(thisGame, thisFoursome, thisCourse)
				#ENDIF
			ENDIF
		BREAK
		CASE GGS_SCORECARD_END_GAME
			IF bDebugSpew DEBUG_MESSAGE("GMS_SCORECARD_END_GAME") ENDIF		
			
			IF NOT IS_GOLF_STATS_FLAG_SET(thisHelpers, GSF_UPDATED_STAT_SCORE)
				
				SET_GOLF_STATS_FLAG(thisHelpers, GSF_UPDATED_STAT_SCORE)
				INC_GOLF_STAT_SCORE_COUNT(thisGame, thisFoursome, thisCourse, allPlayers[GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)], thisHelpers, TRUE)
				
				SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)
				MAKE_AUTOSAVE_REQUEST()
				
				#IF NOT GOLF_IS_MP
				CDEBUG1LN(DEBUG_GOLF,"Players Score ", GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)]), " Par ", GET_GOLF_COURSE_TOTAL_PAR(thisCourse))
				IF GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)]) <= GET_GOLF_COURSE_TOTAL_PAR(thisCourse)
					REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_GOLF)
				ENDIF
				#ENDIF
				
				CDEBUG1LN(DEBUG_GOLF,"Update Socailclub stats!")
				SET_GOLF_STAT_ROUNDS_PLAYED(thisHelpers.iTimesPlayedGolf+1)
				INC_GOLF_STAT_ROUND_PLAYED_WITH_CHARACTER()
				 
				IF GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)]) < GET_GOLF_STAT_LOWEST_ROUND()
				AND IS_GOLF_FULL_ROUND(thisHelpers) AND NOT thisGame.bPlayoffGame
					CDEBUG1LN(DEBUG_GOLF,"New best score. Old ", GET_GOLF_STAT_LOWEST_ROUND(), " new ", GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)])) 
					
					SET_GOLF_STAT_LOWEST_ROUND(GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)]))
					SET_GOLF_UI_FLAG(thisHelpers, GUC_DISPLAY_NEW_BEST_SCORE)
				ENDIF
				
				#IF GOLF_IS_MP
					CLEANUP_GOLF_FOURSOME_CLUBS(thisFoursome)
				#ENDIF
				
				CLEAR_RANK_REDICTION_DETAILS()
				SOCIAL_CLUB_CLEAR_DISPLAY_STRUCT()
				CLEANUP_SOCIAL_CLUB_LEADERBOARD(golfLB_control)
			ENDIF
			
			IF NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SUCCESSFUL_LEADERBOARD_WRITE)
				IF GOLF_UPDATE_SOCIAL_CLUB_LEADERBOARD(FALSE)
					SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_SUCCESSFUL_LEADERBOARD_WRITE)
				ENDIF
			ENDIF
			
			#IF NOT GOLF_IS_MP
				IF NOT DO_END_OF_GOLF_GAME_BAG_CUTSCENE(thisFoursome, thisHelpers, thisGame, thisCourse)
					CDEBUG1LN(DEBUG_GOLF,"Splash is on screen")
					RETURN GMS_PLAY_GOLF
				ENDIF
			#ENDIF
			
			#IF GOLF_IS_MP
			
				IF NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SCORECARD_INITIAL_LOAD_DONE)
					CDEBUG1LN(DEBUG_GOLF,"Check initial load for scorecard camera")
					IF STREAMVOL_IS_VALID(thisHelpers.streamVolScorecard)
						IF STREAMVOL_HAS_LOADED(thisHelpers.streamVolScorecard)
						OR IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_FORCE_SCORECARD_VOL_LOAD)
						
							CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_END_GOLF_SCOREBOARD)
							DISPLAY_SCOREBOARD(thisHelpers, TRUE)
						
							CDEBUG1LN(DEBUG_GOLF, "GSF_SCORECARD_INITIAL_LOAD_DONE set")
							STREAMVOL_DELETE(thisHelpers.streamVolScorecard)
							SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_SCORECARD_INITIAL_LOAD_DONE)
							SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_SCORECARD_STREAMVOL_LOADED)
							
						ENDIF
					ELSE
						SET_END_OF_HOLE_STREAMVOL(thisHelpers, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
					ENDIF
					
					RETURN GMS_PLAY_GOLF
				ENDIF
				
				SET_END_OF_HOLE_CAMERA(thisHelpers, 7, TRUE, TRUE)
			#ENDIF
			
			IF NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SCORECARD_SOUND_PLAYED)
				SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_SCORECARD_SOUND_PLAYED)
				PLAY_GOLF_SCORECARD_SOUND_EFFECT(thisGame, thisFoursome, thisCourse, allPlayers[GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)], TRUE)
			ENDIF
			
			IF NOT GOLF_CAN_DISPLAY_SOCAIL_CLUB_LEADERBOARD() //if you can't access the leaderboard, allow the player to press the button so they can see the alert
				CLEAR_GOLF_STREAMING_FLAG(thisHelpers, GSF_DONT_ALLOW_SOCIAL_CLUB)
			ELSE
				//can access the leaderboard
				IF IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SUCCESSFUL_LEADERBOARD_WRITE)
					CLEAR_GOLF_STREAMING_FLAG(thisHelpers, GSF_DONT_ALLOW_SOCIAL_CLUB) //write over, allow leaderboard
				ELSE
					SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_DONT_ALLOW_SOCIAL_CLUB) //still writing, block leadboard
				ENDIF
			ENDIF
			
			CLEANUP_GOLF_HELPER_ALL_BALL_BLIPS(thisHelpers)
			
			IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SOCIAL_CLUB_BOARD_DISPLAYED)
				IF GOLF_CAN_DISPLAY_SOCAIL_CLUB_LEADERBOARD() // if you can write to the leaderboard
					IF NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SUCCESSFUL_LEADERBOARD_WRITE) //if the write isn't over
						IF IS_BITMASK_AS_ENUM_SET(thisHelpers.golfKeysInUse, GOLF_KEY_SOCIAL_CLUB)
							GOLF_SETUP_SCOREBOARD_KEYS(thisHelpers, TRUE, FALSE, FALSE, !IS_GOLF_FOURSOME_MP(), FALSE) //don't show button
						ENDIF
					ELIF NOT IS_BITMASK_AS_ENUM_SET(thisHelpers.golfKeysInUse, GOLF_KEY_SOCIAL_CLUB) //if the button is not up and the write is over
						GOLF_SETUP_SCOREBOARD_KEYS(thisHelpers, TRUE, FALSE, FALSE, !IS_GOLF_FOURSOME_MP(), TRUE) //show button
					ENDIF
				ENDIF
				
				GOLF_MANAGE_SCORECARD_INPUT(thisFoursome.golfState, thisGame, thisHelpers, bDoRematch)
			ENDIF
			
			IF NOT IS_RADAR_HIDDEN()
				DISPLAY_RADAR(FALSE)
			ENDIF
			
			IF GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_GAME
				bFlagPassed = TRUE
				CDEBUG1LN(DEBUG_GOLF,"Golf foursome state doesn't equal end game")
				
				GOLF_UPDATE_END_OF_HOLE_STATS(IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome), GOLF_SUCCESSFUL_DRIVE), TRUE)
				//GET_GOLF_PLAYER_CURRENT_SCORE starts at 0 and goes up, a par game is 36

				SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_END_GAME)
				CLEAR_GOLF_STATS_FLAG(thisHelpers, GSF_UPDATED_STAT_SCORE)
				CLEANUP_GOLF_FOURSOME_CLUBS(thisFoursome)

				#IF GOLF_IS_MP
					CLEAR_HELP()
				#ENDIF
				IF NOT IS_GOLF_FOURSOME_MP()
					DISPLAY_SCOREBOARD(thisHelpers, FALSE)
				
					SWITCH GET_GOLF_PED_ENUM(PLAYER_PED_ID()) //unlock golf outfit
						CASE CHAR_MICHAEL
							SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_GOLF, TRUE)
						BREAK
						CASE CHAR_FRANKLIN
							SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), COMP_TYPE_OUTFIT, OUTFIT_P1_GOLF, TRUE)
						BREAK
						CASE CHAR_TREVOR
							SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF, TRUE)
						BREAK
					ENDSWITCH
				ENDIF
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				IF IS_GOLF_FOURSOME_MP()
					RETURN GMS_OUTRO_INIT
				ELSE
					IF bDoRematch
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						RETURN GMS_REMATCH
					ELSE
						RETURN GMS_OUTRO_INIT
					ENDIF
				ENDIF
			ELSE
				DISPLAY_SCOREBOARD(thisHelpers, TRUE)
			ENDIF
			IF NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
			OR IS_GOLF_FOURSOME_MP()
				GOLF_DISPLAY_CONTROLS(thisHelpers, TRUE)
			ENDIF
		BREAK
		
		CASE GGS_GET_IN_CART
		FALLTHRU
		CASE GGS_DRIVE_TO_SHOT
			PRINTSTRING("Player foursome is in an incorrect state.\n")
			SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_NAVIGATE_TO_SHOT)
		BREAK
		CASE GGS_END_GAME
			CDEBUG1LN(DEBUG_GOLF,"Foursome in GGS_END_GAME")
			RETURN GMS_OUTRO_INIT
		BREAK
	ENDSWITCH
	RETURN GMS_PLAY_GOLF
ENDFUNC
