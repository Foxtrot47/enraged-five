USING "golf.sch"
USING "golf_course.sch"
USING "golf_foursome.sch"
USING "golf_ui.sch"
USING "commands_shapetest.sch"


/// PURPOSE:
///    Sets up the an ai at the current hole
/// PARAMS:
///    thisCourse - 
///    playerCore - 
PROC GOLF_INIT_PLAYER_AT_CURRENT_HOLE(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER_CORE &playerCore)
		SET_GOLF_PLAYER_LIE(playerCore, <<0,0,0>>, LIE_TEE)
		SET_GOLF_PLAYER_SHOT_NORMAL(playerCore,  <<0,0,-1>>)
		SET_GOLF_PLAYER_SHOT_ESTIMATE(playerCore, <<0,0,0>>)
		SET_GOLF_PLAYER_BALL_POSITION(playerCore, GET_GOLF_HOLE_TEE_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
		SET_GOLF_PLAYER_DISTANCE_TO_HOLE(playerCore, GET_GOLF_HOLE_LENGTH(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
		SET_GOLF_PLAYER_SHOTS_ON_HOLE(playerCore, 0)
ENDPROC

FUNC BOOL IS_GOLF_COORD_IN_RANGE_OF_GREEN(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, VECTOR vPos)
	IF VDIST2(vPos, GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))) > (DIST_TO_GREEN_HACK*DIST_TO_GREEN_HACK)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_INDEXED_BALL_IN_RANGE_OF_GREEN(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, INT playerIndex)
	OBJECT_INDEX golfBall = GET_GOLF_PLAYER_BALL(thisFoursome.playerCore[playerIndex])
	IF NOT DOES_ENTITY_EXIST(golfBall)
		RETURN FALSE
	ENDIF
	VECTOR ballLoc = GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisFoursome.playerCore[playerIndex]), FALSE)
	IF VDIST2(ballLoc, GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))) > (DIST_TO_GREEN_HACK*DIST_TO_GREEN_HACK)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_BALL_IN_RANGE_OF_GREEN(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, BOOL bUseEntityPos = TRUE)
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) < 0
		RETURN FALSE
	ENDIf

	OBJECT_INDEX golfBall = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)
	VECTOR ballLoc
	
	IF bUseEntityPos
		IF NOT DOES_ENTITY_EXIST(golfBall)
			RETURN FALSE
		ENDIF
		ballLoc = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), FALSE)
	ELSE
		ballLoc = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
	ENDIF
	
	IF VDIST2(ballLoc, GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))) > (DIST_TO_GREEN_HACK*DIST_TO_GREEN_HACK)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC DISABLE_GOLF_MINIMAP()
	SET_MINIMAP_GOLF_COURSE_OFF()
	UNLOCK_MINIMAP_ANGLE()
	UNLOCK_MINIMAP_POSITION()
	SET_RADAR_ZOOM(0)
	
	IF NOT IS_GOLF_FOURSOME_MP()
		TOGGLE_STEALTH_RADAR(TRUE)
	ENDIF
ENDPROC

PROC SET_GOLF_MINIMAP_DATA(INT iHole, BOOL bUseGreenMap)
	SWITCH iHole
		CASE 0
			IF NOT bUseGreenMap
				MINIMAP_ZOOM_RATIO = 0.81 MINIMAP_POS_X = -1222 MINIMAP_POS_Y = 83
			ELSE
				MINIMAP_ZOOM_RATIO = 0.003 MINIMAP_POS_X = -1142 MINIMAP_POS_Y = 156
			ENDIF
				MINIMAP_ANGLE = 280
			BREAK
		CASE 1
			IF NOT bUseGreenMap
				MINIMAP_ZOOM_RATIO = 0.75 MINIMAP_POS_X = -1216 MINIMAP_POS_Y = 247
			ELSE
				MINIMAP_ZOOM_RATIO = 0.001 MINIMAP_POS_X = -1294 MINIMAP_POS_Y = 195
			ENDIF
			MINIMAP_ANGLE = 89
			BREAK
		CASE 2
			IF NOT bUseGreenMap
				MINIMAP_ZOOM_RATIO = 0.1 MINIMAP_POS_X = -1274.5 MINIMAP_POS_Y = 65
			ELSE
				MINIMAP_ZOOM_RATIO = 0.1 MINIMAP_POS_X = -1274.5 MINIMAP_POS_Y = 65
			ENDIF
			MINIMAP_ANGLE = 264
			BREAK
		CASE 3
			IF NOT bUseGreenMap
				MINIMAP_ZOOM_RATIO = 0.55 MINIMAP_POS_X = -1197 MINIMAP_POS_Y = 1
			ELSE
				MINIMAP_ZOOM_RATIO = 0.001 MINIMAP_POS_X = -1145 MINIMAP_POS_Y = 0
			ENDIF
			MINIMAP_ANGLE = 232
			BREAK
		CASE 4
			IF NOT bUseGreenMap
				MINIMAP_ZOOM_RATIO = 0.75 MINIMAP_POS_X = -1090 MINIMAP_POS_Y = -70
			ELSE
				MINIMAP_ZOOM_RATIO = 0.001 MINIMAP_POS_X = -1004 MINIMAP_POS_Y = -92
			ENDIF
			MINIMAP_ANGLE = 220
			BREAK
		CASE 5
			IF NOT bUseGreenMap
				MINIMAP_ZOOM_RATIO = 0.4 MINIMAP_POS_X = -1051 MINIMAP_POS_Y = -55
			ELSE
				MINIMAP_ZOOM_RATIO = 0.001 MINIMAP_POS_X = -1076 MINIMAP_POS_Y = -76
			ENDIF
			MINIMAP_ANGLE = 90
			BREAK
		CASE 6
			IF NOT bUseGreenMap
				MINIMAP_ZOOM_RATIO = 0.75 MINIMAP_POS_X = -1164 MINIMAP_POS_Y = 40
			ELSE
				MINIMAP_ZOOM_RATIO = 0.085 MINIMAP_POS_X = -1250 MINIMAP_POS_Y = 34
			ENDIF
			MINIMAP_ANGLE = 57
			BREAK
		CASE 7
			IF NOT bUseGreenMap
				MINIMAP_ZOOM_RATIO = 0.825 MINIMAP_POS_X = -1212 MINIMAP_POS_Y = -120
			ELSE
				MINIMAP_ZOOM_RATIO = 0.001 MINIMAP_POS_X = -1097 MINIMAP_POS_Y = -109
			ENDIF
			MINIMAP_ANGLE = 240
			BREAK
		CASE 8
			IF NOT bUseGreenMap
				MINIMAP_ZOOM_RATIO = 0.675 MINIMAP_POS_X = -1173 MINIMAP_POS_Y = 117
			ELSE
				MINIMAP_ZOOM_RATIO = 0.001 MINIMAP_POS_X = -1242 MINIMAP_POS_Y = 106
			ENDIF
			MINIMAP_ANGLE = 63
			BREAK
	ENDSWITCH
ENDPROC

//init golf minimap
PROC SET_GOLF_MINIMAP(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse)
	
	INT hole =  GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)
	BOOL bUseGreenMap = IS_BALL_IN_RANGE_OF_GREEN(thisCourse, thisFoursome)
	eMINIMAP_GOLF_COURSE_HOLE eHole = INT_TO_ENUM(eMINIMAP_GOLF_COURSE_HOLE ,hole+1)
	SET_MINIMAP_GOLF_COURSE(eHole)
	UNUSED_PARAMETER(thisCourse)
	TOGGLE_STEALTH_RADAR(FALSE)
	
	/* I was told to use hard coded values
	VECTOR teePos =  GET_GOLF_HOLE_TEE_POSITION(thisCourse, hole)
	VECTOR pinPos = GET_GOLF_HOLE_PIN_POSITION(thisCourse,hole)
	
	LOCK_MINIMAP_ANGLE(FLOOR(GET_HEADING_BETWEEN_VECTORS(teePos, pinPos)))

	//center of hole
	VECTOR center = teePos + 0.5*(pinPos-teePos)
	LOCK_MINIMAP_POSITION(center.x, center.y)
	zoomValue = GET_GOLF_MINIMAP_RATIO(hole)
	//*/
	#IF NOT GOLF_IS_AI_ONLY
	IF DOES_BLIP_EXIST(thisCourse.blipFlag)
		REMOVE_BLIP(thisCourse.blipFlag)
	ENDIF
	thisCourse.blipFlag = CREATE_BLIP_FOR_COORD(GET_GOLF_HOLE_PIN_POSITION(thisCourse, hole)) 
	SET_BLIP_SPRITE(thisCourse.blipFlag, RADAR_TRACE_GOLF_FLAG)
	#ENDIF

	SET_GOLF_MINIMAP_DATA(hole, bUseGreenMap)
	
	SET_RADAR_ZOOM(CEIL(1100*MINIMAP_ZOOM_RATIO))
	LOCK_MINIMAP_POSITION(MINIMAP_POS_X, MINIMAP_POS_Y)
	LOCK_MINIMAP_ANGLE(MINIMAP_ANGLE)
ENDPROC

/// PURPOSE:
///    Sets up the foursome's current hole
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
PROC GOLF_INIT_CURRENT_HOLE(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)
	INT iCurrentPlayer
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iCurrentPlayer
		SET_GOLF_PLAYER_LIE(thisFoursome.playerCore[iCurrentPlayer], <<0,0,0>>, LIE_TEE)
		SET_GOLF_PLAYER_SHOT_NORMAL(thisFoursome.playerCore[iCurrentPlayer],  <<0,0,-1>>)
		SET_GOLF_PLAYER_SHOT_ESTIMATE(thisFoursome.playerCore[iCurrentPlayer], <<0,0,0>>)
		SET_GOLF_PLAYER_BALL_POSITION(thisFoursome.playerCore[iCurrentPlayer], GET_GOLF_HOLE_TEE_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
		SET_GOLF_PLAYER_DISTANCE_TO_HOLE(thisFoursome.playerCore[iCurrentPlayer], GET_GOLF_HOLE_LENGTH(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
		SET_GOLF_PLAYER_SHOTS_ON_HOLE(thisFoursome.playerCore[iCurrentPlayer], 0)
	ENDREPEAT
	
ENDPROC

FUNC BOOL DID_GOLF_INDEXED_PLAYER_WIN(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &allPlayers[], INT playerIndex)
	INT oppIndex
	INT score = GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[playerIndex])
	
	IF IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, playerIndex, GOLF_PLAYER_REMOVED)
		RETURN FALSE
	ENDIF
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) oppIndex
		CDEBUG1LN(DEBUG_GOLF,"Player ", oppIndex , " score ", GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[oppIndex]))
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_GOLF,"Done Printing scores when looking for tie game")
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) oppIndex
		IF GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[oppIndex]) < score
		AND NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, oppIndex, GOLF_PLAYER_REMOVED)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GOLF_GAME_TIE(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &allPlayers[])
	INT playerIndex, playersWon
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) playerIndex	
		IF DID_GOLF_INDEXED_PLAYER_WIN(thisFoursome, allPlayers, playerIndex) //will return true if they have the best score
			playersWon++ //inc the amount of players with the best score
		ENDIF
	ENDREPEAT

	CDEBUG1LN(DEBUG_GOLF,"Number of players won ", playersWon)
	RETURN playersWon > 1 //if only one person has the best score, it is not a tie game
ENDFUNC

//functions assumes game is a tie game
FUNC BOOL GOLFER_IS_PART_OF_TIE(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &allPlayers[], INT iCheckPlayerIndex)
	//if the player is part of the tie he will have one of the best score
	RETURN DID_GOLF_INDEXED_PLAYER_WIN(thisFoursome, allPlayers, iCheckPlayerIndex)
ENDFUNC

FUNC INT GET_GOLF_WINNER(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &allPlayers[])
	INT playerIndex
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) playerIndex	
		IF DID_GOLF_INDEXED_PLAYER_WIN(thisFoursome, allPlayers, playerIndex)
			RETURN playerIndex
		ENDIF
	ENDREPEAT

	RETURN 0
ENDFUNC

FUNC INT GET_GOLFER_PLACEMENT(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &allPlayers[], BOOL &bDidTie)

	bDidTie = FALSE
	
	INT iLocalPlayer = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
	IF IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iLocalPlayer, GOLF_PLAYER_REMOVED)
		RETURN 0
	ENDIF
	
	INT oppIndex
	INT iPlacement = 1
	INT score = GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[iLocalPlayer])
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) oppIndex	
		IF oppIndex != iLocalPlayer
		AND NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, oppIndex, GOLF_PLAYER_REMOVED)
		
			IF GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[oppIndex]) < score			
				iPlacement++
			ELIF GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[oppIndex]) = score
				bDidTie = TRUE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN iPlacement
ENDFUNC

FUNC BOOL DID_PLAYER_WIN_GOLF(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &allPlayers[])
	RETURN DID_GOLF_INDEXED_PLAYER_WIN(thisFoursome, allPlayers, 0)
ENDFUNC

/// PURPOSE:
///    Moves a foursome to the next hole. Wraps around if we are at the end of a course
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
FUNC BOOL GOLF_MOVE_TO_NEXT_HOLE(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, INT iEndHole, #IF NOT GOLF_IS_AI_ONLY GOLF_PLAYER &allPlayers[], #ENDIF BOOL bPlayerFoursome = FALSE )

	#IF NOT GOLF_IS_AI_ONLY
		CDEBUG1LN(DEBUG_GOLF,"Add one to current hole")
	#ENDIF
	
	thisFoursome.iCurrentHole++
	IF bPlayerFoursome
		SET_GOLF_COURSE_PLAYER_HOLE(GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
	ENDIF
	
	#IF NOT GOLF_IS_AI_ONLY	
		allPlayers[0].iCurrentScore = allPlayers[0].iCurrentScore //surpress errors
	#ENDIF
	
	BOOl bAllowEndGame = TRUE
	
	IF GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) >= (iEndHole+1)
	OR thisGame.bPlayoffGame
		#IF NOT GOLF_IS_AI_ONLY	
		#IF GOLF_IS_MP
			IF thisGame.bPlayoffGame //if you are in sudden death mode, check if you should end game
				CDEBUG1LN(DEBUG_GOLF,"Checking Tie Game")
				bAllowEndGame = !IS_GOLF_GAME_TIE(thisFoursome, allPlayers)
			ENDIF
		#ENDIF
		#ENDIF
	
		IF bPlayerFoursome AND bAllowEndGame
			CDEBUG1LN(DEBUG_GOLF,"End Game!")
			SET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome, iEndHole)
			thisGame.bPlayoffGame = FALSE
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_GOLF,"Loop!")
			IF GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) >= GET_GOLF_COURSE_NUM_HOLES(thisCourse)
				SET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome, 0)
			ENDIF
			#IF NOT GOLF_IS_AI_ONLY
				CDEBUG1LN(DEBUG_GOLF,"Mark Playoff Game")
				thisGame.bPlayoffGame = TRUE
				thisGame.iPlayoffRound++
				RETURN TRUE
			#ENDIF
			
			
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CREATE_GOLF_FLAG(GOLF_COURSE &thisCourse, INT iHole)
	IF iHole >=0 AND iHole < thisCourse.iNumHoles
		IF NOT DOES_ENTITY_EXIST(GET_GOLF_HOLE_FLAG(thisCourse, iHole))
			
			CDEBUG1LN(DEBUG_GOLF, "Creating golf flag at hole ", iHole)
			
			SET_GOLF_HOLE_FLAG(thisCourse, iHole, CREATE_OBJECT_NO_OFFSET(PROP_GOLFFLAG, GET_GOLF_HOLE_PIN_POSITION(thisCourse, iHole), FALSE))
			FREEZE_ENTITY_POSITION(GET_GOLF_HOLE_FLAG(thisCourse, iHole), TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up a foursome's current hole (before moving on to next hole
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
PROC GOLF_CLEANUP_CURRENT_HOLE(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)
	UNUSED_PARAMETER(thisCourse)

	INT iCurrentPlayer
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iCurrentPlayer
		IF GET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL(thisFoursome, iCurrentPlayer) != HUMAN_NETWORK
			#IF NOT GOLF_IS_MP
			CLEANUP_GOLF_FOURSOME_INDEXED_GOLF_BALL(thisFoursome, iCurrentPlayer)
			#ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Checks to see if the current player's shot is a tee shot 
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
/// RETURNS:
///    
FUNC BOOL IS_TEE_SHOT(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)
	IF (GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) < 0) OR (GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) >= GET_GOLF_COURSE_NUM_HOLES(thisCourse))
		// Current hole is invalid
		RETURN FALSE
	ENDIF
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) != LIE_TEE // This is the first shot on this hole ie TEE SHOT
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to see if the current player's shot was a tee shot (valid until it switches to the next player) 
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
/// RETURNS:
///    
FUNC BOOL WAS_TEE_SHOT(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)
	IF (GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) < 0) OR (GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) >= GET_GOLF_COURSE_NUM_HOLES(thisCourse))
		// Current hole is invalid
		RETURN FALSE
	ENDIF
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LAST_LIE(thisFoursome) != LIE_TEE // This is the first shot on this hole ie TEE SHOT
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to see if the ball for the current player is in the hole on the current green. Note: Currently a fairly forgiving dist check. Will change to the trigger check once hole geometry is in
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
/// RETURNS:
///    
FUNC BOOL IS_BALL_IN_HOLE(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)
	IF NOT DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		RETURN FALSE
	ENDIF
	VECTOR ballLoc = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), FALSE)
	VECTOR pinPos = GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
	
	IF VDIST2(ballLoc, pinPos) > (0.25*0.25)
		RETURN FALSE
	ENDIF
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_CUP
		IF ballLoc.z < pinPos.z + 0.11// This accounts for the fact that the hole is down a bit off the green
			RETURN TRUE                  
		ENDIF
	ENDIF
	
	IF VDIST2(ballLoc, pinPos) < (BALL_IN_HOLE_RADIUS*BALL_IN_HOLE_RADIUS)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, <<0,0,0>>, LIE_CUP)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the ball for the current player is on the green of the current hole. Note: Currently just a large radius check. Will change to a material check once the tech comes online
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
/// RETURNS:
///    
FUNC BOOL IS_BALL_ON_GREEN(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)
	OBJECT_INDEX golfBall = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)
	
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_MARKER(thisFoursome)) AND GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN
		RETURN TRUE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(golfBall)
		RETURN FALSE
	ENDIF
	
	VECTOR ballLoc = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), FALSE)
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) != LIE_GREEN 
	AND VDIST2(ballLoc, GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))) >(DIST_FOR_GIMMIE*DIST_FOR_GIMMIE)
		RETURN FALSE
	ENDIF
	
	IF VDIST2(ballLoc, GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))) > (DIST_TO_GREEN_HACK*DIST_TO_GREEN_HACK)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

//GOLF_COURSE &thisCourse was being passed to the following five funcions and they didn't do anything
//Sometimes this would cause a stack overflow when in the launcher so I am just going to remove them for now
//I will put them back in if it is required
FUNC BOOL IS_BALL_ON_FAIRWAY(GOLF_FOURSOME &thisFoursome)
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) != LIE_FAIRWAY
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


FUNC BOOL IS_BALL_IN_BUNKER(GOLF_FOURSOME &thisFoursome)
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) != LIE_BUNKER
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_BALL_IN_ROUGH(GOLF_FOURSOME &thisFoursome)
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) != LIE_ROUGH
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_BALL_IN_WATER_HAZARD(GOLF_FOURSOME &thisFoursome)
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) != LIE_WATER
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_BALL_ON_CART_PATH(GOLF_FOURSOME &thisFoursome)
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) != LIE_CART_PATH
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

//GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome
FUNC BOOL IS_BALL_LIE_UNKOWN(GOLF_FOURSOME &thisFoursome)
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) != LIE_UNKNOWN
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SHOT_A_GIMME(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, BOOL bUseEntityCoord = FALSE)
	IF NOT IS_BALL_ON_GREEN(thisCourse, thisFoursome)
		RETURN FALSE
	ENDIF
	
	VECTOR ballPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
	IF bUseEntityCoord
		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
			ballPos = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		ENDIF
	ENDIF
	
	IF VDIST2(ballPos, GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))) > (DIST_FOR_GIMMIE*DIST_FOR_GIMMIE)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SHOT_AN_AI_GIMME(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)//, BOOL bUseEntityCoord = FALSE)
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != COMPUTER
		RETURN FALSE
	ENDIF
	IF NOT IS_BALL_ON_GREEN(thisCourse, thisFoursome)
		RETURN FALSE
	ENDIF

	VECTOR ballPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
//	IF bUseEntityCoord
//		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
//			ballPos = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
//		ENDIF
//	ENDIF
	
	
	FLOAT fGimmeDist = DIST_FOR_GIMMIE
	SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_AI_DIFFICULTY(thisFoursome) 
		CASE GOLF_AI_EASY
			fGimmeDist = DIST_FOR_AI_GIMMIE_EASY
		BREAK
		CASE GOLF_AI_NORMAL
			fGimmeDist = DIST_FOR_AI_GIMMIE_MEDIUM
		BREAK
		CASE GOLF_AI_HARD
			fGimmeDist = DIST_FOR_AI_GIMMIE_HARD
		BREAK
	ENDSWITCH
	
	IF VDIST(ballPos, GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))) > (fGimmeDist)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Checks to see if the player is on the course proper, and not leaving. Not really used as the check is currently non-trivial.
/// PARAMS:
///    thisPlayerCore - 
///    thisCourse - 
/// RETURNS:
///    
FUNC BOOL GOLF_IS_PLAYER_ON_COURSE_INNER(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_COURSE &thisCourse)
	UNUSED_PARAMETER(thisPlayerCore)
	UNUSED_PARAMETER(thisCourse)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to see if the player is within the larger hysteresis around the course. Leaving this exits the game. Not currently used as the check is currently non-trivial
/// PARAMS:
///    thisPlayerCore - 
///    thisCourse - 
/// RETURNS:
///    
FUNC BOOL GOLF_IS_PLAYER_ON_COURSE_OUTER(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_COURSE &thisCourse)
	UNUSED_PARAMETER(thisPlayerCore)
	UNUSED_PARAMETER(thisCourse)
	RETURN TRUE
ENDFUNC


FUNC BOOL IS_GOLF_PLAYER_PUTTING(GOLF_PLAYER_CORE &thisPlayerCore)
 	SWING_STYLE currentSwingStyle = GET_GOLF_PLAYER_SWING_STYLE(thisPlayerCore)
	IF currentSwingStyle = SWING_STYLE_GREEN
	OR currentSwingStyle = SWING_STYLE_GREEN_SHORT
	OR currentSwingStyle = SWING_STYLE_GREEN_LONG
	OR currentSwingStyle = SWING_STYLE_GREEN_TAP
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DRAW_GOLF_BOUND_PLANE(VECTOR p0, VECTOR p1, FLOAT fWidth, INT R, INT G, INT B)
p1.z = p0.z
	VECTOR vNormal =  NORMALISE_VECTOR(p0 - p1)
	
	vNormal = ROTATE_VECTOR_ABOUT_Z_ORTHO(vNormal, ROTSTEP_90)
	
	DRAW_DEBUG_SPHERE(p0, 0.5, 255,255,255)
	DRAW_DEBUG_SPHERE(p1, 0.5, 0,0,0)
	DRAW_DEBUG_LINE(p0, p0+(fWidth*vNormal), R, G, B)
	DRAW_DEBUG_LINE(p0, p0-(fWidth*vNormal), R, G, B)

ENDPROC

FUNC BOOL IS_GOLF_COORD_OUTSIDE_BOUNDARY_PLANES(VECTOR ballPos )
	
	//plane 1, in front of club house
	VECTOR planeNormal = NORMALISE_VECTOR(<< -1405.69, 61.69, 51.66 >> - <<-1403.44, 61.61, 51.76>>)
	FLOAT D = DOT_PRODUCT(planeNormal, << -1405.69, 61.69, 51.66 >>)
	IF DOT_PRODUCT(ballPos, planeNormal)  > D
		RETURN TRUE
	ENDIF
	
	//plane 2, spliting seven hole tee and fairway
	planeNormal = NORMALISE_VECTOR(<<-1167.90, -85.86, 0>> - <<-1162.01, -89.1, 0>>)
	D = DOT_PRODUCT(planeNormal, <<-1162.01, -89.1, 0>>- 9.92*planeNormal)
	
	IF DOT_PRODUCT(ballPos, planeNormal) > D
		//plane bordering west wall, hole seven fairway
		planeNormal = NORMALISE_VECTOR(<<-1193.70, -89.76, 0>> - <<-1192.45, -87.41, 0>>)
		D = DOT_PRODUCT(planeNormal, << -1193.70, -89.76, 39.97>> - 2.778*planeNormal)
		
		IF DOT_PRODUCT(ballPos, planeNormal) > D
			RETURN TRUE
		ENDIF
		
		IF IS_POINT_IN_ANGLED_AREA(ballPos, <<-1227.90869, -68.76888, 43.06714>>, <<-1260.59949, -50.60131, 44.63058>>, 2.740, FALSE, FALSE)
			CDEBUG1LN(DEBUG_GOLF,"In that strange area near hole 7 fairway")
			RETURN TRUE
		ENDIF
	ELSE
		//plane near hole seven te
		planeNormal = NORMALISE_VECTOR(<<-1193.70, -89.76, 0>> - <<-1192.45, -87.41, 0>>)
		D = DOT_PRODUCT(planeNormal, << -1193.70, -89.76, 39.97>> - 1.03*planeNormal)
		
		IF DOT_PRODUCT(ballPos, planeNormal) > D
			RETURN TRUE
		ENDIF
	ENDIF
	
	//plane 3, splits south wall bording the 5th hole green
	planeNormal = NORMALISE_VECTOR(<< -970.31, -113.24, 0.0>> - <<-973.91, -115.19, 0.0>>)
	D = DOT_PRODUCT(planeNormal, << -970.31, -113.24, 38.28>>)
	
	IF DOT_PRODUCT(ballPos, planeNormal) > D
		//plane 4, on hole 5 green side
			planeNormal = NORMALISE_VECTOR(<< -948.52, -106.46, 0.0>> - <<-951.06, -102.20, 0.0>>)
			D = DOT_PRODUCT(planeNormal, << -948.52, -106.46, 42.39>>)

			IF DOT_PRODUCT(ballPos, planeNormal) > D
				RETURN TRUE
			ENDIF
	ELSE
		//plane 5, splits hole 6 tee and green
		planeNormal = NORMALISE_VECTOR(<< -1028.62, -127.38, 0.0>> - <<-1034.37, -129.55, 0.0>>)
		D = DOT_PRODUCT(planeNormal, << -1028.62, -127.38, 39.51>>)
	
		IF DOT_PRODUCT(ballPos, planeNormal) > D
			//plane 6, on hole 6 tee
			planeNormal = NORMALISE_VECTOR(<< -994.29, -125.95, 0.0>> - <<-1001.81, -94.44, 0.0>>)
			D = DOT_PRODUCT(planeNormal, << -994.29, -125.95, 40.12>>)

			IF DOT_PRODUCT(ballPos, planeNormal) > D
				RETURN TRUE
			ENDIF
			
		ELSE
			//plane 7, hole 6 green
			planeNormal = NORMALISE_VECTOR(<< -1037.99, -140.83, 0.0>> - <<-1043.54, -131.81, 0.0>>)
			D = DOT_PRODUCT(planeNormal, << -1037.99, -140.83, 42.99>>)

			IF DOT_PRODUCT(ballPos, planeNormal) > D	
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//plane 8, splits hole 5 and hole 1 green 
	planeNormal = NORMALISE_VECTOR(<< -1047.59, 96.74, 0.0 >> - <<-1041.82, 87.45, 0.0>>)
	D = DOT_PRODUCT(planeNormal, << -1047.59, 96.74, 52.25 >>)
	
	IF DOT_PRODUCT(ballPos, planeNormal) > D
		//plane 9, split hole 1 green and hole 2 tee
		planeNormal = NORMALISE_VECTOR(<< -1094.08, 180.44, 0.0>> - <<-1086.17, 167.17, 0.0>>)
		D = DOT_PRODUCT(planeNormal, << -1094.08, 180.44, 61.49>>)

		IF DOT_PRODUCT(ballPos, planeNormal) > D
			//plane 10, on hole 1 green
			planeNormal = NORMALISE_VECTOR(<< -1092.00, 196.78, 0.0>> - <<-1163.07, 179.38, 0.0>>)
			D = DOT_PRODUCT(planeNormal, (<< -1092.00, 196.78, 60.09>>-3.75*planeNormal))
			IF DOT_PRODUCT(ballPos, planeNormal) > D
				RETURN TRUE
			ENDIF
			
		ELSE
			//plane 11, hole 2 tee
			planeNormal = NORMALISE_VECTOR(<< -1069.28, 145.22, 0.0>> - <<-1152.37, 98.02, 0.0>>)
			D = DOT_PRODUCT(planeNormal, << -1069.28, 145.22, 61.60>>)

			IF DOT_PRODUCT(ballPos, planeNormal) > D	
				RETURN TRUE
			ENDIF
		ENDIF
		
	ELSE
		//plane 12, on hole 5 side
		planeNormal = NORMALISE_VECTOR(<< -967.03, -19.35, 0.0>> - <<-1035.28, -63.01, 0.0>>)
		D = DOT_PRODUCT(planeNormal, << -967.03, -19.35, 48.28>>)

		IF DOT_PRODUCT(ballPos, planeNormal) > D	
			RETURN TRUE
		ENDIF
	ENDIF
	
//	DRAW_GOLF_BOUND_PLANE(<<-1274.15, 186.79, 61.970>>, << -1255.380, 185.870, 61.970>>, 100, 0, 0, 255)
//	DRAW_GOLF_BOUND_PLANE(<< -1326.063, 192.648, 61.760>>, <<-1325.440, 183.78, 0.0>>, 150, 0, 255, 0)
//	DRAW_GOLF_BOUND_PLANE(<< -1159.35, 226.630, 70>>, <<-1152.920, 203.93, 0.0>>, 150, 255, 0, 0)
	
	//plane 13, spilts hole 1 into two halfs
	planeNormal = NORMALISE_VECTOR(<<-1274.15, 186.79, 0.0>> - << -1255.380, 185.870, 0.0>>)
	D = DOT_PRODUCT(planeNormal, << -1274.15, 186.79, 61.970>>)

	IF DOT_PRODUCT(ballPos, planeNormal) > D
		//plane 13, on tee side of hole one
		planeNormal = NORMALISE_VECTOR(<< -1326.063, 192.648, 0.0>>  - <<-1325.440, 183.78, 0.0>> )
		D = DOT_PRODUCT(planeNormal, << -1326.063, 192.648, 61.760>> )

		IF DOT_PRODUCT(ballPos, planeNormal) > D
			RETURN TRUE
		ENDIF
		
	ELSE
		//plane 14, on green side of hole one
		planeNormal = NORMALISE_VECTOR(<< -1159.35, 226.630, 0.0>> - <<-1152.920, 203.93, 0.0>>)
		D = DOT_PRODUCT(planeNormal, << -1159.35, 226.630, 70>>)

		IF DOT_PRODUCT(ballPos, planeNormal) > D	
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE 
ENDFUNC

FUNC BOOL IS_GOLF_COURSE_OBSTRUCTING_BETWEEN_POSTIONS(VECTOR vPos, VECTOR vEnd, BOOL bConsiderMaterial, ENTITY_INDEX excludeEntity = NULL)
	
	DRAW_DEBUG_LINE(vPos, vEnd, 255, 0, 0)
	MATERIAL_NAMES retMaterial
	
	VECTOR vDir = vEnd - vPos
	INT inc
	FLOAT fInc
	REPEAT 20 inc
		fInc = (TO_FLOAT(inc)/20.0)
		DRAW_DEBUG_SPHERE(vPos+ vDir*fInc, 0.35, 0, 0, 255, 10)
	ENDREPEAT
	
	IF excludeEntity = NULL
		excludeEntity = PLAYER_PED_ID()
	ENDIF
	
	SHAPETEST_INDEX capsuleTest = START_SHAPE_TEST_CAPSULE(vPos, vEnd, 0.35, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_RIVER, excludeEntity)
	INT bHitResult
	VECTOR retPos, vRetNormal
	ENTITY_INDEX hitEntity

	//WHILE 
	IF GET_SHAPE_TEST_RESULT_INCLUDING_MATERIAL(capsuleTest, bHitResult, retPos, vRetNormal, retMaterial, hitEntity) != SHAPETEST_STATUS_RESULTS_READY
		CDEBUG1LN(DEBUG_GOLF,"Capsule result not ready?")
	ENDIF
	//ENDWHILE
	
	
	IF bConsiderMaterial
		GOLF_LIE_TYPE retLie = GET_GOLF_LIE_TYPE_FROM_MATERIAL(retMaterial)
		//these lies are okay to cross with
		IF retLie = LIE_BUNKER OR retLie = LIE_CART_PATH OR retLie = LIE_FAIRWAY 
		OR retLie = LIE_GREEN OR retLie = LIE_ROUGH
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF bHitResult != 0
		CDEBUG1LN(DEBUG_GOLF,"Capsule is hitting something")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GOLF_COURSE_OBSTRUCTING_AT_POSTION(VECTOR vPos, FLOAT fCameraAngle, VECTOR vStartOffset)
	
	VECTOR vCheckPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fCameraAngle, vStartOffset)
	VECTOR vCheckDim = <<0.5, 0.5, 1.0>>
	
	DRAW_DEBUG_BOX(vCheckPos - vCheckDim*0.5, vCheckPos + vCheckDim*0.5, 0, 255, 10)
	
	SHAPETEST_INDEX capsuleTest = START_SHAPE_TEST_BOX(vCheckPos, vCheckDim, <<0,0,0>>, EULER_YXZ, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_PED, PLAYER_PED_ID())

	INT bHitResult
	VECTOR retPos, vRetNormal
	ENTITY_INDEX hitEntity
	MATERIAL_NAMES retMaterial
	
	//WHILE 
	IF GET_SHAPE_TEST_RESULT_INCLUDING_MATERIAL(capsuleTest, bHitResult, retPos, vRetNormal, retMaterial, hitEntity) != SHAPETEST_STATUS_RESULTS_READY
		CDEBUG1LN(DEBUG_GOLF,"Box result not ready?")
	ENDIF
	//ENDWHILE
	
	IF bHitResult != 0
		CDEBUG1LN(DEBUG_GOLF,"Box is hitting something")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL GET_GOLF_LIE_FROM_WORLD_POSITION(VECTOR worldPos, GOLF_LIE_TYPE &retLie, GOLF_LIE_TYPE defaultLie, ENTITY_INDEX ExcludeIndex)

	INT bHitResult
	VECTOR vHitPos, vHitNormal
	MATERIAL_NAMES material
	ENTITY_INDEX hitEntity
	SHAPETEST_INDEX lineShapeTest = START_SHAPE_TEST_CAPSULE(worldPos+<<0,0,0.25>>, worldPos -<<0,0,0.25>>, 0.05, SCRIPT_INCLUDE_MOVER, ExcludeIndex)
			
	IF GET_SHAPE_TEST_RESULT_INCLUDING_MATERIAL(lineShapeTest, bHitResult, vHitPos, vHitNormal, material, hitEntity) != SHAPETEST_STATUS_RESULTS_READY
		CDEBUG1LN(DEBUG_GOLF,"Capsule result not ready?")
	ENDIF

			
	IF bHitResult != 0
		retLie = GET_GOLF_LIE_TYPE_FROM_MATERIAL(material)
		
		//Tee boxes need to use same materail as green, Check that the green lie is really a green
		//Email Evan Lawson if you intend to chamge this block of code.
		IF retLie = LIE_GREEN
			CDEBUG1LN(DEBUG_GOLF,"!!!!!!!!!! Found lie is green !!!!!!!!!!!!!!!")
			IF IS_POINT_NEAR_TEE_BOX(worldPos)
				CDEBUG1LN(DEBUG_GOLF,"it's on a TEE BOX, chagning find lie to FAIRWAY")
				retLie = LIE_FAIRWAY
			ENDIF
		ENDIF
		IF IS_POINT_BAD_LIE(worldPos)
			CDEBUG1LN(DEBUG_GOLF, "Lie is hard coded bad position")
			retLie = LIE_BUSH
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_GOLF,"Get Golf Lie From World Postion, lie returned ", retLie)
//			DEBUG_PRINTCALLSTACK()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_GOLF,"Lie from world position failed?!")
	retLie = defaultLie 
	RETURN FALSE
ENDFUNC 

FUNC BOOL GOLF_PROBE_COURSE(VECTOR startPos, VECTOR endPos, INT index, GOLF_LIE_TYPE &retLie, VECTOR &retPos)

startPos.z = startPos.z
endPos.z = endPos.z
UNUSED_PARAMETER(retLie)
retPos.z = retPos.z
index = index
#IF NOT GOLF_IS_AI_ONLY
	INT bHitResult
	VECTOR vHitNormal
	ENTITY_INDEX hitEntity
	MATERIAL_NAMES material
	
	IF NOT IS_GOLF_SHAPE_TEST_CREATE_FLAG_SET(index)
		
		golfTrailShapeTest[index] = START_SHAPE_TEST_LOS_PROBE(startPos, endPos, SCRIPT_INCLUDE_FOLIAGE | SCRIPT_INCLUDE_MOVER)
		
		SET_GOLF_SHPAE_TEST_LAST_COMPLETED_SECTION(0)
//		iFramesForResults = 0
		SET_GOLF_SHAPE_TEST_CREATE_FLAG(index, TRUE)
		IF index = 0
		//	CDEBUG1LN(DEBUG_GOLF,"Creating shape tests ")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_LINE(startPos, endPos, 255, 0, 0)
	#ENDIF
	
	IF GET_SHAPE_TEST_RESULT_INCLUDING_MATERIAL(golfTrailShapeTest[index], bHitResult, retPos, vHitNormal, material, hitEntity) = SHAPETEST_STATUS_RESULTS_READY
		SET_GOLF_SHPAE_TEST_LAST_COMPLETED_SECTION(index)
		
		IF bHitResult != 0
			retLie = GET_GOLF_LIE_TYPE_FROM_MATERIAL(material)
			RETURN TRUE
		ELSE
//			CDEBUG1LN(DEBUG_GOLF,"Still looking for line intersection, part ", index)
		ENDIF
	ENDIF
		
	retLie = LIE_ROUGH
#ENDIF
	RETURN FALSE

ENDFUNC

FUNC BOOL WOULD_CAMERA_AT_POSITION_BE_OCCLUDED(VECTOR vObjPosition, FLOAT fCameraAngle, VECTOR vCamOffset, VECTOR &vRetPos)
	
	VECTOR vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vObjPosition,fCameraAngle, vCamOffset)
	VECTOR vCamToObj = vObjPosition - vCamPos
	VECTOR vToObjNorm = NORMALISE_VECTOR(vCamToObj)
	VECTOR vCollisionPos
	vCamToObj *= 0.75
	vObjPosition = vCamPos + vCamToObj //don't start right on top of object
	
	DRAW_DEBUG_LINE(vCamPos, vObjPosition, 255, 0, 0)
	INT inc
	FLOAT fMag = VDIST(vCamPos, vObjPosition)
	REPEAT 20 inc
		DRAW_DEBUG_SPHERE(vCamPos + ((TO_FLOAT(inc)*fMag)/20.0)*vToObjNorm, 0.75, 0, 0, 255, 10)
	ENDREPEAT
	
	SHAPETEST_INDEX capsuleTest = START_SHAPE_TEST_CAPSULE(vObjPosition, vCamPos,  0.25, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT, PLAYER_PED_ID())
	INT bHitResult
	VECTOR vRetNormal
	ENTITY_INDEX hitEntity
	
	//WHILE 
	IF GET_SHAPE_TEST_RESULT(capsuleTest, bHitResult, vCollisionPos, vRetNormal, hitEntity) != SHAPETEST_STATUS_RESULTS_READY
		CDEBUG1LN(DEBUG_GOLF,"Camera occlusion Capsule result not ready??")
	ENDIF
	//ENDWHILE
	
	IF bHitResult != 0
		CDEBUG1LN(DEBUG_GOLF,"Camera at pos capsule is hitting something at ", vCollisionPos)
		vRetPos = vCollisionPos
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GOLF_OBJECT_IN_FOLIAGE(OBJECT_INDEX objIndex)

	SHAPETEST_INDEX objectTest = START_SHAPE_TEST_BOUND(objIndex, SCRIPT_INCLUDE_FOLIAGE)
	INT bHitResult
	VECTOR vRetPos, vRetNormal
	ENTITY_INDEX hitEntity

	IF GET_SHAPE_TEST_RESULT(objectTest, bHitResult, vRetPos, vRetNormal, hitEntity) != SHAPETEST_STATUS_RESULTS_READY
		CDEBUG1LN(DEBUG_GOLF,"Foliage result not ready??")
	ENDIF
	
	IF bHitResult != 0
		CDEBUG1LN(DEBUG_GOLF,"Object is hitting foliage")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


//Returns if the location of the ball would restrict the goflers swing
FUNC BOOL IS_GOLF_POSITION_RESTRICTIVE(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, VECTOR vPos)
	
	#IF GOLF_IS_AI_ONLY
		RETURN FALSE
	#ENDIF

	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_BALL_IN_FLIGHT
		CDEBUG1LN(DEBUG_GOLF,"Dont call IS_GOLF_POSITION_RESTRICTIVE when ball is in flight")
//		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN//some times it would consider near a cup a restrictive position
		RETURN FALSE
	ENDIF
	
	FLOAT fHeading = GET_HEADING_BETWEEN_VECTORS(GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), vPos)
	
	VECTOR vEndPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, <<1.0, 0.0, 1.0>>)
	VECTOR vDir = NORMALISE_VECTOR(vEndPos - vPos) * 0.5 //so the prob doesn't start right on the ball
	
	IF IS_GOLF_COURSE_OBSTRUCTING_BETWEEN_POSTIONS(vPos + vDir, vEndPos, TRUE)
		CDEBUG1LN(DEBUG_GOLF,"Ball pos is restrictive for player at ", vPos)
		RETURN TRUE
	ENDIF
	
	vEndPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading+80, <<1.0, 0.0, 1.0>>)
	vDir = NORMALISE_VECTOR(vEndPos - vPos) * 0.5
	
	IF IS_GOLF_COURSE_OBSTRUCTING_BETWEEN_POSTIONS(vPos+vDir, vEndPos, TRUE)
		CDEBUG1LN(DEBUG_GOLF,"Ball pos is restrictive for back swing at ", vPos)
		RETURN TRUE
	ENDIF
	
	vEndPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading-80, <<1.0, 0.0, 1.0>>)
	vDir = NORMALISE_VECTOR(vEndPos - vPos) * 0.5
	
	IF IS_GOLF_COURSE_OBSTRUCTING_BETWEEN_POSTIONS(vPos+vDir, vEndPos, TRUE)
		CDEBUG1LN(DEBUG_GOLF,"Ball pos is restrictive for forward swing at ", vPos)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_GOLF_COORD_OUTSIDE_HOLE_ONE_BOUNDING_SPHERE(VECTOR vGolfCoord)
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE( <<-1365.727905,174.315369,57.013119>>, 20.75, 0, 255, 255, 50 ) 
		DRAW_DEBUG_SPHERE( <<-1351.805176,179.003036,57.291412>>, 25.75, 0, 255, 255, 50 ) 
		DRAW_DEBUG_SPHERE( <<-1338.616699,182.510620,57.515053>>, 25.75, 0, 255, 255, 50 ) 
		DRAW_DEBUG_SPHERE( <<-1320.937378,184.857407,57.820129>>, 25.75, 0, 255, 255, 50 )
		DRAW_DEBUG_SPHERE( <<-1303.497437,185.037949,58.282097>>, 25.75, 0, 255, 255, 50 )
		DRAW_DEBUG_SPHERE( <<-1287.777954,186.679718,58.927990>>, 25.75, 0, 255, 255, 50 )
		DRAW_DEBUG_SPHERE( <<-1276.092407,187.600403,59.527382>>, 25.75, 0, 255, 255, 50 )
		DRAW_DEBUG_SPHERE( <<-1256.294678,188.985962,61.289848>>, 25.00, 0, 255, 255, 50 )
		DRAW_DEBUG_SPHERE( <<-1235.771240,189.366776,62.402626>>, 29.50, 0, 255, 255, 50 )
		DRAW_DEBUG_SPHERE( <<-1217.616089,191.289215,63.778843>>, 33.50, 0, 255, 255, 50 )
		DRAW_DEBUG_SPHERE( <<-1189.676880,198.544662,64.960938>>, 45.00, 0, 255, 255, 50 )
		DRAW_DEBUG_SPHERE( <<-1147.465820, 215.924240, 64.5190>>, 49.75, 0, 255, 255, 50 )
	#ENDIF
	
	IF VDIST2(vGolfCoord, <<-1365.727905,174.315369,57.013119>> ) < (20.75 * 20.75)
	OR VDIST2(vGolfCoord, <<-1351.805176,179.003036,57.291412>> ) < (25.75 * 25.75)
	OR VDIST2(vGolfCoord, <<-1338.616699,182.510620,57.515053>> ) < (25.75 * 25.75)
	OR VDIST2(vGolfCoord, <<-1320.937378,184.857407,57.820129>> ) < (25.75 * 25.75)
	OR VDIST2(vGolfCoord, <<-1303.497437,185.037949,58.282097>> ) < (25.75 * 25.75)	
	OR VDIST2(vGolfCoord, <<-1287.777954,186.679718,58.927990>> ) < (25.75 * 25.75)
	OR VDIST2(vGolfCoord, <<-1276.092407,187.600403,59.527382>> ) < (25.75 * 25.75)
	OR VDIST2(vGolfCoord, <<-1256.294678,188.985962,61.289848>> ) < (25.00 * 25.00)
	OR VDIST2(vGolfCoord, <<-1235.771240,189.366776,62.402626>> ) < (29.50 * 29.50)
	OR VDIST2(vGolfCoord, <<-1217.616089,191.289215,63.778843>> ) < (33.50 * 33.50)	
	OR VDIST2(vGolfCoord, <<-1189.676880,198.544662,64.960938>> ) < (45.00 * 45.00)
	OR VDIST2(vGolfCoord, <<-1147.465820, 215.924240, 64.5190>> ) < (49.75 * 49.75)	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GOLF_COORD_OUTSIDE_HOLE_TWO_BOUNDING_SPHERE(VECTOR vGolfCoord)
		
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE( <<-1327.736816,159.325668,56.804813>>, 20.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1313.003052,151.442810,57.082485>>, 20.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1300.225464,150.735046,57.934685>>, 18.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1314.335693,159.902603,56.821346>>, 18.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1291.734009,162.252930,57.521557>>, 18.75, 0, 255, 255, 50)
		
		DRAW_DEBUG_SPHERE( <<-1273.538452,163.715561,57.991531>>, 18.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1255.357544,159.830231,57.953476>>, 22.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1260.908691,144.870560,57.691547>>, 22.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1244.690674,145.638657,57.714031>>, 22.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1222.357056,145.155884,58.442333>>, 33.50, 0, 255, 255, 50)
		
		DRAW_DEBUG_SPHERE( <<-1195.714478,138.888123,59.483425>>, 38.50, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1161.011597,149.753998,61.720253>>, 38.50, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1118.562012,155.971573,61.498318>>, 38.50, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1234.879639,162.849533,59.078590>>, 15.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1304.960571,167.753250,57.582531>> , 10.25, 0, 255, 255, 50)
	#ENDIF
	
	IF VDIST2(vGolfCoord, <<-1327.736816,159.325668,56.804813>> ) < (20.00 * 20.00)
	OR VDIST2(vGolfCoord, <<-1313.003052,151.442810,57.082485>> ) < (20.00 * 20.00)
	OR VDIST2(vGolfCoord, <<-1300.225464,150.735046,57.934685>> ) < (18.75 * 18.75)
	OR VDIST2(vGolfCoord, <<-1314.335693,159.902603,56.821346>> ) < (18.75 * 18.75)
	OR VDIST2(vGolfCoord, <<-1291.734009,162.252930,57.521557>> ) < (18.75 * 18.75)
	
	OR VDIST2(vGolfCoord, <<-1273.538452,163.715561,57.991531>> ) < (18.75 * 18.75)
	OR VDIST2(vGolfCoord, <<-1255.357544,159.830231,57.953476>> ) < (22.75 * 22.75)
	OR VDIST2(vGolfCoord, <<-1260.908691,144.870560,57.691547>> ) < (22.75 * 22.75)
	OR VDIST2(vGolfCoord, <<-1244.690674,145.638657,57.714031>> ) < (22.75 * 22.75)
	OR VDIST2(vGolfCoord, <<-1222.357056,145.155884,58.442333>> ) < (33.50 * 33.50)
	
	OR VDIST2(vGolfCoord, <<-1195.714478,138.888123,59.483425>> ) < (38.50 * 38.50)
	OR VDIST2(vGolfCoord, <<-1161.011597,149.753998,61.720253>> ) < (38.50 * 38.50)
	OR VDIST2(vGolfCoord, <<-1118.562012,155.971573,61.498318>> ) < (38.50 * 38.50)
	OR VDIST2(vGolfCoord, <<-1234.879639,162.849533,59.078590>> ) < (15.00 * 15.00)
	OR VDIST2(vGolfCoord, <<-1304.960571,167.753250,57.582531>> ) < (10.25 * 10.25)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GOLF_COORD_OUTSIDE_HOLE_THREE_BOUNDING_SPHERE(VECTOR vGolfCoord)
		
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE( <<-1304.960571,167.753250,57.582531>>, 6.25, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1234.879639,162.849533,59.078590>>, 11.0, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1241.930786,103.196625,55.612572>>, 36.0, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1255.233398,110.289917,55.627853>>, 36.0, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1274.278687,119.107964,56.414837>>, 28.0, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1293.003174,121.777184,56.083878>>, 23.0, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1302.705200,124.269310,56.271282>>, 23.0, 0, 255, 255, 50)
	#ENDIF
	
	IF VDIST2(vGolfCoord, <<-1304.960571,167.753250,57.582531>> ) < (6.25 * 6.25)
	OR VDIST2(vGolfCoord, <<-1234.879639,162.849533,59.078590>> ) < (11.0 * 11.0)
	OR VDIST2(vGolfCoord, <<-1241.930786,103.196625,55.612572>> ) < (36.0 * 36.0)
	OR VDIST2(vGolfCoord, <<-1255.233398,110.289917,55.627853>> ) < (36.0 * 36.0)
	OR VDIST2(vGolfCoord, <<-1274.278687,119.107964,56.414837>> ) < (28.0 * 28.0)
	OR VDIST2(vGolfCoord, <<-1293.003174,121.777184,56.083878>> ) < (23.0 * 23.0)
	OR VDIST2(vGolfCoord, <<-1302.705200,124.269310,56.271282>> ) < (23.0 * 23.0)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GOLF_COORD_OUTSIDE_HOLE_FOUR_BOUNDING_SPHERE(VECTOR vGolfCoord)
		
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE( << -1221.06, 98.5996, 57.883600000 >>, 20.900000, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1200.08, 92.5423, 57.651700000 >>, 27.650000, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1178.72, 83.8707, 60.610100000 >>, 35.525000, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1149.67, 68.5744, 57.316500000 >>, 36.62, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1138.01, 55.2482, 54.432500000 >>, 33.52, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1125.44, 41.863, 51.9212000000 >>, 29.825, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1121.19, 35.1585, 51.582100000 >>, 30.425, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1101.69, 12.5718, 50.153500000 >>, 34.025, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1116.93, 0.466945, 49.06270000 >>, 18.575, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1155.4375, 92.815178,56.988285 >>, 14.50, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1095.9377, 42.5799, 50.3450000 >>, 11.12, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1105.00488, 50.75577, 51.66250 >>, 14.7, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1093.25110, 36.75171, 49.51097 >>, 11.49, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( << -1112.37891, 61.12305, 52.97316 >>, 10.85, 0, 255, 255, 50)
		
	#ENDIF
	
	IF VDIST2(vGolfCoord, << -1221.06, 98.5996, 57.883600000 >>) < (20.900 * 20.9)
	OR VDIST2(vGolfCoord, << -1200.08, 92.5423, 57.651700000 >>) < (27.650 * 27.65)
	OR VDIST2(vGolfCoord, << -1178.72, 83.8707, 60.610100000 >>) < (32.525 * 32.525)
	OR VDIST2(vGolfCoord, << -1149.67, 68.5744, 57.316500000 >>) < (36.620 * 36.62)
	OR VDIST2(vGolfCoord, << -1138.01, 55.2482, 54.432500000 >>) < (33.520 * 33.52)
	OR VDIST2(vGolfCoord, << -1125.44, 41.863, 51.9212000000 >>) < (29.825 * 29.825)
	OR VDIST2(vGolfCoord, << -1121.19, 35.1585, 51.582100000 >>) < (30.425 * 30.425)
	OR VDIST2(vGolfCoord, << -1101.69, 12.5718, 50.153500000 >>) < (34.025 * 34.025)
	OR VDIST2(vGolfCoord, << -1116.93, 0.466945, 49.06270000 >>) < (18.575 * 18.575)
	OR VDIST2(vGolfCoord, << -1155.4375, 92.815, 56.99000000 >>) < (14.500 * 14.50)
	OR VDIST2(vGolfCoord, << -1095.9377, 42.5799, 50.3450000 >>) < (11.120 * 11.12)
	OR VDIST2(vGolfCoord, << -1105.00488, 50.75577, 51.66250 >>) < (14.700 * 14.7)
	OR VDIST2(vGolfCoord, << -1093.25110, 36.75171, 49.51097 >>) < (11.490 * 11.49)
	OR VDIST2(vGolfCoord, << -1112.37891, 61.12305, 52.97316 >>) < (10.850 * 10.85)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GOLF_COORD_OUTSIDE_HOLE_FIVE_BOUNDING_SPHERE(VECTOR vGolfCoord)
		
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE( <<-946.038025,-84.437508,39.088943>>, 29.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-969.936890,-67.453644,40.620064>>, 29.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-990.433167,-51.773438,41.655811>>, 35.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1006.639526,-20.229515,45.67778>>, 48.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1030.741699,10.294682,49.258698>>, 48.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1074.044678,49.566372,50.122120>>, 29.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1081.729858,57.717678,50.813744>>, 29.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1059.091309,48.760387,49.809143>>, 29.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1037.404297,37.183304,45.012669>>, 29.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-987.9270630,12.216481,51.479408>>, 58.25, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-923.314270,-55.920582,38.376068>>, 58.25, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-939.216064,-106.79802,41.728218>>, 30.75, 0, 255, 255, 50) 
	#ENDIF
		
	IF VDIST2(vGolfCoord, <<-946.038025,-84.437508,39.088943>>) < (29.75 * 29.75)
	OR VDIST2(vGolfCoord, <<-969.936890,-67.453644,40.620064>>) < (29.75 * 29.75)
	OR VDIST2(vGolfCoord, <<-990.433167,-51.773438,41.655811>>) < (35.75 * 29.75)
	OR VDIST2(vGolfCoord, <<-1006.639526,-20.229515,45.67778>>) < (48.00 * 48.00)
	OR VDIST2(vGolfCoord, <<-1030.741699,10.294682,49.258698>>) < (48.00 * 48.00)
	OR VDIST2(vGolfCoord, <<-1074.044678,49.566372,50.122120>>) < (29.75 * 29.75)
	OR VDIST2(vGolfCoord, <<-1081.729858,57.717678,50.813744>>) < (29.75 * 29.75)
	OR VDIST2(vGolfCoord, <<-1059.091309,48.760387,49.809143>>) < (29.75 * 29.75)
	OR VDIST2(vGolfCoord, <<-1037.404297,37.183304,45.012669>>) < (29.75 * 29.75)
	OR VDIST2(vGolfCoord, <<-987.9270630,12.216481,51.479408>>) < (58.25 * 58.25)
	OR VDIST2(vGolfCoord, <<-923.314270,-55.920582,38.376068>>) < (58.25 * 58.25)
	OR VDIST2(vGolfCoord, <<-939.216064,-106.79802,41.728218>>) < (30.75 * 30.75)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GOLF_COORD_OUTSIDE_HOLE_SIX_BOUNDING_SPHERE(VECTOR vGolfCoord)
		
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE( <<-989.449097,-122.8805770,39.029953>>, 25.25, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1018.170471,-117.239906,40.538963>>, 25.25, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1045.524170,-123.058960,40.773052>>, 28.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1072.829346,-123.255363,40.280102>>, 42.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1094.701538,-125.870667,40.663219>>, 42.00, 0, 255, 255, 50)
	#ENDIF
	
	IF VDIST2(vGolfCoord, <<-989.449097,-122.8805770,39.029953>> ) < ( 25.25 * 25.25)
	OR VDIST2(vGolfCoord, <<-1018.170471,-117.239906,40.538963>> ) < ( 25.25 * 25.25)
	OR VDIST2(vGolfCoord, <<-1045.524170,-123.058960,40.773052>> ) < ( 28.00 * 28.00)
	OR VDIST2(vGolfCoord, <<-1072.829346,-123.255363,40.280102>> ) < ( 42.00 * 42.00)
	OR VDIST2(vGolfCoord, <<-1094.701538,-125.870667,40.663219>> ) < ( 42.00 * 42.00)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_GOLF_COORD_OUTSIDE_HOLE_SEVEN_BOUNDING_SPHERE(VECTOR vGolfCoord)
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE( <<-1136.241699,-95.068581,41.302517>>, 28.25, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1165.434692,-81.748619,44.069450>>, 37.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1191.798340,-60.593716,43.362129>>, 37.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1209.493042,-45.690117,43.190418>>, 37.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1222.780640,-37.272449,44.954025>>, 37.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1233.681152,-29.527641,42.596130>>, 40.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1254.531860,-18.875807,46.362934>>, 40.00, 0, 255, 255, 50) 
		DRAW_DEBUG_SPHERE( <<-1291.964355,-1.4287470,49.296162>>, 40.00, 0, 255, 255, 50) 
	#ENDIF
	
	IF VDIST2(vGolfCoord, <<-1136.241699,-95.068581,41.302517>>) < (28.25 * 28.25)
	OR VDIST2(vGolfCoord, <<-1165.434692,-81.748619,44.069450>>) < (37.00 * 37.00)
	OR VDIST2(vGolfCoord, <<-1191.798340,-60.593716,43.362129>>) < (37.00 * 37.00)
	OR VDIST2(vGolfCoord, <<-1209.493042,-45.690117,43.190418>>) < (37.00 * 37.00)
	OR VDIST2(vGolfCoord, <<-1222.780640,-37.272449,44.954025>>) < (37.00 * 37.00)
	OR VDIST2(vGolfCoord, <<-1233.681152,-29.527641,42.596130>>) < (40.00 * 40.00)
	OR VDIST2(vGolfCoord, <<-1254.531860,-18.875807,46.362934>>) < (40.00 * 40.00) 
	OR VDIST2(vGolfCoord, <<-1291.964355,-1.4287470,49.296162>>) < (40.00 * 40.00)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GOLF_COORD_OUTSIDE_HOLE_EIGHT_BOUNDING_SPHERE(VECTOR vGolfCoord)
		
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE( <<-1262.265259,30.905869,47.800354>>, 23.50, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1238.014404,20.013653,46.685284>>, 31.50, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1207.611450,-1.210448,46.655750>>, 36.25, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1176.875732,-18.25381,45.277035>>, 36.25, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1159.923706,-21.481136,45.18527>>, 25.25, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1150.805542,-33.96546,44.683826>>, 26.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1137.149658,-43.70882,44.634342>>, 26.75, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1142.32666,-47.365517,44.363075>>, 30.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1139.133423,-47.338657,44.39388>>, 32.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1123.010254,-54.36808,43.903271>>, 32.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1105.071045,-64.81659,43.047184>>, 32.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1088.105347,-68.641586,42.17559>>, 32.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1077.96338,-71.280106,42.958477>>, 32.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1067.076294,-77.98500,42.574677>>, 32.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1053.74231,-83.321754,42.588959>>, 32.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1041.197876,-87.25570,42.836494>>, 30.00, 0, 255, 255, 50)
	#ENDIF
	
	IF VDIST2(vGolfCoord, <<-1262.265259,30.905869,47.800354>>) < (23.50 * 23.50)
	OR VDIST2(vGolfCoord, <<-1238.014404,20.013653,46.685284>>) < (31.50 * 31.50)
	OR VDIST2(vGolfCoord, <<-1207.611450,-1.210448,46.655750>>) < (36.25 * 36.25)
	OR VDIST2(vGolfCoord, <<-1176.875732,-18.25381,45.277035>>) < (36.25 * 36.25)
	OR VDIST2(vGolfCoord, <<-1159.923706,-21.481136,45.18527>>) < (25.25 * 25.25)
	OR VDIST2(vGolfCoord, <<-1150.805542,-33.96546,44.683826>>) < (26.75 * 26.75)
	OR VDIST2(vGolfCoord, <<-1137.149658,-43.70882,44.634342>>) < (26.75 * 26.75)
	OR VDIST2(vGolfCoord, <<-1142.32666,-47.365517,44.363075>>) < (30.00 * 30.00)
	OR VDIST2(vGolfCoord, <<-1139.133423,-47.338657,44.39388>>) < (32.00 * 32.00)
	OR VDIST2(vGolfCoord, <<-1123.010254,-54.36808,43.903271>>) < (32.00 * 32.00)
	OR VDIST2(vGolfCoord, <<-1105.071045,-64.81659,43.047184>>) < (32.00 * 32.00)
	OR VDIST2(vGolfCoord, <<-1088.105347,-68.641586,42.17559>>) < (32.00 * 32.00)
	OR VDIST2(vGolfCoord, <<-1077.96338,-71.280106,42.958477>>) < (32.00 * 32.00)
	OR VDIST2(vGolfCoord, <<-1067.076294,-77.98500,42.574677>>) < (32.00 * 32.00)
	OR VDIST2(vGolfCoord, <<-1053.74231,-83.321754,42.588959>>) < (32.00 * 32.00)
	OR VDIST2(vGolfCoord, <<-1041.197876,-87.25570,42.836494>>) < (30.00 * 30.00) 
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GOLF_COORD_OUTSIDE_HOLE_NINE_BOUNDING_SPHERE(VECTOR vGolfCoord)
		
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE( <<-1283.381104,80.552170,53.905674>>, 30.50, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1252.595459,70.568245,51.245884>>, 31.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1229.660645,60.067318,51.890144>>, 31.00, 0, 255, 255, 50) 
		DRAW_DEBUG_SPHERE( <<-1215.065430,50.102516,51.799274>>, 33.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1181.437500,28.372972,50.825546>>, 37.50, 0, 255, 255, 50) 
		DRAW_DEBUG_SPHERE( <<-1167.599121,15.932876,49.163261>>, 37.50, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1151.077637,9.428267,48.3015520>>, 35.00, 0, 255, 255, 50)
		DRAW_DEBUG_SPHERE( <<-1187.174683,60.732788,53.639885>>,  9.00, 0, 255, 255, 50)
	#ENDIF
	
	IF VDIST2(vGolfCoord, <<-1283.381104,80.552170,53.905674>> ) < (30.50 * 30.50)
	OR VDIST2(vGolfCoord, <<-1252.595459,70.568245,51.245884>> ) < (31.00 * 31.00)
	OR VDIST2(vGolfCoord, <<-1229.660645,60.067318,51.890144>> ) < (31.00 * 31.00) 
	OR VDIST2(vGolfCoord, <<-1215.065430,50.102516,51.799274>> ) < (33.00 * 33.00)
	OR VDIST2(vGolfCoord, <<-1181.437500,28.372972,50.825546>> ) < (37.50 * 37.50) 
	OR VDIST2(vGolfCoord, <<-1167.599121,15.932876,49.163261>> ) < (37.50 * 37.50)
	OR VDIST2(vGolfCoord, <<-1151.077637,9.428267,48.3015520>> ) < (35.00 * 35.00)
	OR VDIST2(vGolfCoord, <<-1187.174683,60.732788,53.639885>> ) < (9.000 * 9.000)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GOLF_COORD_OUTSIDE_HOLE_BOUNDING_SPHERES(VECTOR vGolfCoord, GOLF_FOURSOME &thisFoursome)

	SWITCH GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)
	
		CASE 0
			RETURN IS_GOLF_COORD_OUTSIDE_HOLE_ONE_BOUNDING_SPHERE(vGolfCoord)
		BREAK
		CASE 1
			RETURN IS_GOLF_COORD_OUTSIDE_HOLE_TWO_BOUNDING_SPHERE(vGolfCoord)
		BREAK
		CASE 2
			RETURN IS_GOLF_COORD_OUTSIDE_HOLE_THREE_BOUNDING_SPHERE(vGolfCoord)
		BREAK
		CASE 3
			RETURN IS_GOLF_COORD_OUTSIDE_HOLE_FOUR_BOUNDING_SPHERE(vGolfCoord)
		BREAK
		CASE 4
			RETURN IS_GOLF_COORD_OUTSIDE_HOLE_FIVE_BOUNDING_SPHERE(vGolfCoord)
		BREAK
		CASE 5
			RETURN IS_GOLF_COORD_OUTSIDE_HOLE_SIX_BOUNDING_SPHERE(vGolfCoord)
		BREAK
		CASE 6
			RETURN IS_GOLF_COORD_OUTSIDE_HOLE_SEVEN_BOUNDING_SPHERE(vGolfCoord)
		BREAK
		CASE 7
			RETURN IS_GOLF_COORD_OUTSIDE_HOLE_EIGHT_BOUNDING_SPHERE(vGolfCoord)
		BREAK
		CASE 8
			RETURN IS_GOLF_COORD_OUTSIDE_HOLE_NINE_BOUNDING_SPHERE(vGolfCoord)
		BREAK
		
	ENDSWITCH
	

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GOLF_BALL_OOB(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_OOB_REASON &OOBReason, BOOL bDoRestrictiveTest = TRUE, BOOL bDoUnplayableLieTest = TRUE)

	IF IS_BALL_IN_WATER_HAZARD(thisFoursome) 
		#IF NOT GOLF_IS_AI_ONLY	CDEBUG1LN(DEBUG_GOLF,"OOB WATER") #ENDIF
		OOBReason = OOB_WATER
		RETURN TRUE
	ELIF IS_BALL_LIE_UNKOWN(thisFoursome)
		#IF NOT GOLF_IS_AI_ONLY	CDEBUG1LN(DEBUG_GOLF,"OOB UKNOWN LIE") #ENDIF
		OOBReason = OOB_UKNOWN_LIE
		RETURN TRUE
	//if you hit the ball onto a green that belongs to a different hole, thats oob
	ELIF (GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN AND NOT IS_BALL_IN_RANGE_OF_GREEN(thisCourse, thisFoursome) )
		#IF NOT GOLF_IS_AI_ONLY	CDEBUG1LN(DEBUG_GOLF,"OOB ON WRONG GREEN") #ENDIF
		OOBReason = OOB_ON_WRONG_GREEN
		RETURN TRUE
	//bush and cart path are going to count as OOB for now
	ELIF IS_GOLF_LIE_UNPLAYABLE(GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)) AND bDoUnplayableLieTest
		#IF NOT GOLF_IS_AI_ONLY	CDEBUG1LN(DEBUG_GOLF,"OOB CART PATH OR BUSH") #ENDIF
		OOBReason = OOB_UNPLAYABLE_LIE
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		VECTOR vBallPos = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
	
		IF IS_GOLF_COORD_OUTSIDE_BOUNDARY_PLANES(vBallPos)
			#IF NOT GOLF_IS_AI_ONLY	CDEBUG1LN(DEBUG_GOLF,"OOB OUTSIDE BOUNDING PLANES") #ENDIF
			OOBReason = OOB_OUTSIDE_BOUNDING_PLANES
			RETURN TRUE
		ELIF IS_GOLF_COORD_OUTSIDE_HOLE_BOUNDING_SPHERES(vBallPos, thisFoursome)
			#IF NOT GOLF_IS_AI_ONLY	CDEBUG1LN(DEBUG_GOLF,"OOB OUTSIDE HOLE BOUNDNIG SPHERES") #ENDIF
			OOBReason = OOB_OUTSIDE_HOLE_BOUNDING_SPHERES
			RETURN TRUE
		ENDIF
		IF bDoRestrictiveTest
			IF IS_GOLF_POSITION_RESTRICTIVE(thisCourse, thisFoursome, vBallPos)
				#IF NOT GOLF_IS_AI_ONLY	CDEBUG1LN(DEBUG_GOLF,"OOB RESTRICTIVE POSITION") #ENDIF	
				OOBReason = OOB_RESTRICTIVE_POSITION
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_BALL_OUTSIDE_BOUNDARY_PLANE_OR_BOUNDING_SPHERES(GOLF_FOURSOME &thisFoursome)
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		IF IS_GOLF_COORD_OUTSIDE_BOUNDARY_PLANES(GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)))
			RETURN TRUE
		ELIF IS_GOLF_COORD_OUTSIDE_HOLE_BOUNDING_SPHERES(GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), thisFoursome)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GOLF_COORD_INSIDE_FAIRWAY_OF_INDEXED_HOLE(VECTOR vCoord, INT iHole)

	SWITCH iHole
		CASE 0
			RETURN IS_POINT_IN_ANGLED_AREA(vCoord, <<-1343.414673,181.182388,57.333588>>, <<-1251.276123,187.565048,63.270870>>, 20.50, FALSE, FALSE)
				OR IS_POINT_IN_ANGLED_AREA(vCoord, <<-1250.130371,183.416595,61.826935>>, <<-1135.730469,210.237213,65.005524>>, 34.25, FALSE, FALSE)
		BREAK
		CASE 1
			RETURN IS_POINT_IN_ANGLED_AREA(vCoord, <<-1158.625977,148.445343,61.671444>>, <<-1252.306152,148.070114,58.765175>>, 43.75, FALSE, FALSE)
				OR IS_POINT_IN_ANGLED_AREA(vCoord, <<-1252.342896,162.886703,66.231209>>, <<-1312.435059,159.272552,57.933022>>, 27.25, FALSE, FALSE)
		BREAK
		CASE 2
			RETURN IS_POINT_IN_ANGLED_AREA(vCoord, <<-1306.300659,126.595154,64.511719>>, <<-1248.896606,105.227753,56.552639>>, 27.25, FALSE, FALSE)
		BREAK
		CASE 3
			RETURN IS_POINT_IN_ANGLED_AREA(vCoord, <<-1228.795166,98.256592,64.046806>>, <<-1127.404785,70.777428,55.554138>>, 33.25, FALSE, FALSE)
				OR IS_POINT_IN_ANGLED_AREA(vCoord, <<-1139.193848,78.430031,55.897522>>, <<-1089.521729,-8.462549,47.662827>>, 50.00, FALSE, FALSE)
		BREAK
		CASE 4
			RETURN IS_POINT_IN_ANGLED_AREA(vCoord, <<-1056.761108,4.600485,56.479935>>, <<-972.462341,-79.602837,40.800972>>, 33.25, FALSE, FALSE)
		BREAK
		CASE 5
			RETURN IS_POINT_IN_ANGLED_AREA(vCoord, <<-1001.106934,-113.409019,48.136395>>, <<-1082.220337,-116.038490,41.546932>>, 33.25, FALSE, FALSE)
		BREAK
		CASE 6
			RETURN IS_POINT_IN_ANGLED_AREA(vCoord, <<-1123.556030,-97.212242,48.866890>>, <<-1283.789551,-21.737469,48.840378>>, 33.25, FALSE, FALSE)
		BREAK
		CASE 7
			RETURN IS_POINT_IN_ANGLED_AREA(vCoord, <<-1272.027222,36.362148,56.681969>>, <<-1059.007202,-81.266525,43.529655>>, 39.50, FALSE, FALSE)
		BREAK
		CASE 8
			RETURN IS_POINT_IN_ANGLED_AREA(vCoord, <<-1128.409058,-3.124314,56.171017>>, <<-1252.343872,79.264534,52.995228>>, 37.00, FALSE, FALSE)
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SHOT_LIMIT_EXCEEDED(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, INT iPlayerIndex = -1, INT iOffset = 0)

	IF iPlayerIndex = -1
		iPlayerIndex = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
	ENDIF

	RETURN (GET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(thisFoursome, iPlayerIndex) >= GET_GOLF_HOLE_PAR(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) + (5+iOffset)) #IF IS_DEBUG_BUILD AND NOT bInfiniteShots #ENDIF
ENDFUNC


FUNC INT GET_NUM_OF_GREEN_SPLINE_CAMERAS_FOR_HOLE(INT iHole)
	
	SWITCH iHole
		CASE 0
			RETURN 4
		BREAK
		CASE 1
			RETURN 4
		BREAK
		CASE 2
			RETURN 2
		BREAK
		CASE 3
			RETURN 2
		BREAK
		CASE 4
			RETURN 4
		BREAK
		CASE 5
			RETURN 2
		BREAK
		CASE 6
			RETURN 5
		BREAK
		CASE 7
			RETURN 5
		BREAK
		CASE 8
			RETURN 4
		BREAK
	ENDSWITCH

	RETURN 0
ENDFUNC

FUNC INT GET_GREEN_SPLINE_CAMERA_TIME_FOR_HOLE(INT iHole)
	
	SWITCH iHole
		CASE 0
			RETURN 2000
		BREAK
		CASE 1
			RETURN 2000
		BREAK
		CASE 2
			RETURN 1000
		BREAK
		CASE 3
			RETURN 1500
		BREAK
		CASE 4
			RETURN 2500
		BREAK
		CASE 5
			RETURN 1000
		BREAK
		CASE 6
			RETURN 2500
		BREAK
		CASE 7
			RETURN 2500
		BREAK
		CASE 8
			RETURN 2000
		BREAK
	ENDSWITCH

	RETURN 0
ENDFUNC


PROC GET_GOLF_HOLE_ONE_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(INT index, VECTOR &vRetPos, VECTOR &vRetRot)
	
	SWITCH index
		CASE 0
			vRetPos = << -1320.7644, 183.6616, 67.9775 >>
			vRetRot = << -23.3201, 0.0000, -89.2365 >>
		BREAK
		CASE 1
			vRetPos = << -1260.7570, 181.4989, 73.9749 >>
			vRetRot = << -18.9391, 0.0000, -83.2668 >>
		BREAK
		CASE 2
			vRetPos = << -1197.6821, 177.2393, 72.5978 >>
			vRetRot = << -11.5560, -0.0000, -55.3467 >>
		BREAK
		CASE 3
			vRetPos = << -1134.2712, 212.9660, 73.2606 >>
			vRetRot = << -37.0222, -0.0000, -55.6838 >>
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid green camera index for hole 1")
	ENDSWITCH
ENDPROC

PROC GET_GOLF_HOLE_TWO_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(INT index, VECTOR &vRetPos, VECTOR &vRetRot)
	SWITCH index
		CASE 0
			vRetPos = << -1151.7957, 140.7695, 72.0727 >>
			vRetRot =  << -20.6667, 0.0000, 72.5055 >>
		BREAK
		CASE 1
			vRetPos = << -1214.8385, 154.1587, 65.2884 >>
			vRetRot = << -4.3005, -0.0000, 77.3166 >>
		BREAK
		CASE 2
			vRetPos = << -1247.2153, 161.2281, 67.3770 >>
			vRetRot =  << -12.6665, 0.0000, 78.4986 >>
		BREAK
		CASE 3
			vRetPos = << -1301.7051, 167.4967, 70.6909 >>
			vRetRot = << -34.7268, 0.0000, 101.4268 >>
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid green camera index for hole 2")
	ENDSWITCH
ENDPROC

PROC GET_GOLF_HOLE_THREE_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(INT index, VECTOR &vRetPos, VECTOR &vRetRot)
	SWITCH index
		CASE 0
			vRetPos = << -1291.3287, 106.4161, 63.9952 >>
			vRetRot =  << -16.6060, 0.0000, -74.3135 >>
		BREAK
		CASE 1
			vRetPos = << -1264.4171, 94.7818, 68.7728 >> 
			vRetRot = << -25.2876, 0.0000, -67.0479 >>
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid green camera index for hole 3")
	ENDSWITCH
ENDPROC

PROC GET_GOLF_HOLE_FOUR_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(INT index, VECTOR &vRetPos, VECTOR &vRetRot)
	SWITCH index
		CASE 0
			vRetPos = << -1174.7363, 103.7762, 65.5594 >>
			vRetRot = << -8.6982, 0.0000, -137.0540 >>
		BREAK
		CASE 1
			vRetPos = << -1119.5093, 42.8915, 65.5874 >>
			vRetRot = << -23.2281, -0.0000, -148.3643 >>
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid green camera index for hole 4")
	ENDSWITCH
ENDPROC

PROC GET_GOLF_HOLE_FIVE_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(INT index, VECTOR &vRetPos, VECTOR &vRetRot)
	SWITCH index
		CASE 0
			vRetPos = << -1054.8104, 41.1086, 69.7104 >>
			vRetRot = << -24.8931, -0.0000, -157.7285 >>
		BREAK
		CASE 1
			vRetPos =  << -1025.4761, 7.3602, 65.0285 >>
			vRetRot =  << -22.6380, 0.0000, 179.0858 >>
		BREAK
		CASE 2
			vRetPos = << -1041.0343, -45.9046, 62.6625 >>
			vRetRot = << -27.7410, -0.0000, -103.5129 >>
		BREAK
		CASE 3
			vRetPos = << -999.8635, -66.2741, 65.9639 >>
			vRetRot = << -38.8941, 0.0000, -110.9679 >>
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid green camera index for hole 5")
	ENDSWITCH
ENDPROC

PROC GET_GOLF_HOLE_SIX_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(INT index, VECTOR &vRetPos, VECTOR &vRetRot)
	SWITCH index
		CASE 0
			vRetPos = << -1018.8165, -117.0517, 52.8287 >>
			vRetRot = << -29.9027, -0.0000, 86.7974 >>
		BREAK
		CASE 1
			vRetPos = << -1069.7557, -113.5879, 60.8817 >>
			vRetRot = << -36.7612, 0.0000, 82.3421 >>
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid green camera index for hole 6")
	ENDSWITCH
ENDPROC

PROC GET_GOLF_HOLE_SEVEN_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(INT index, VECTOR &vRetPos, VECTOR &vRetRot)
	SWITCH index
		CASE 0
			vRetPos = << -1136.5591, -94.7377, 45.8988 >>
			vRetRot = << -3.2122, 0.0000, 63.8833 >>
		BREAK
		CASE 1
			vRetPos = << -1171.7223, -79.0620, 52.6703 >>
			vRetRot = << -15.8985, 0.0000, 53.0819 >>
		BREAK
		CASE 2
			vRetPos = <<-1201.0748, -73.5546, 56.2329>>
			vRetRot = <<-12.3795, -0.0000, 33.9216>>	
		BREAK
		CASE 3
			vRetPos = << -1227.6796, -47.7722, 61.0299 >>
			vRetRot = << -25.4619, -0.0000, 51.7201 >>
		BREAK
		CASE 4
			vRetPos = << -1261.4871, -21.2802, 60.1234 >>
			vRetRot = << -20.4131, 0.0000, 38.3490 >>
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid green camera index for hole 7")
	ENDSWITCH
ENDPROC

PROC GET_GOLF_HOLE_EIGHT_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(INT index, VECTOR &vRetPos, VECTOR &vRetRot)
	SWITCH index
		CASE 0
			vRetPos = << -1241.2260, 18.3065, 60.5586 >>
			vRetRot = << -11.2652, 0.0000, -117.0898 >>
		BREAK
		CASE 1
			vRetPos = << -1202.9443, -15.9930, 55.8415 >>
			vRetRot = << -8.8412, 0.0000, -107.6249 >>
		BREAK
		CASE 2
			vRetPos = << -1156.3354, -28.2037, 57.1487 >>
			vRetRot = << -20.3684, -0.0070, -127.7787 >>
		BREAK
		CASE 3
			vRetPos = << -1113.6414, -82.6089, 56.2070 >>
			vRetRot = << -19.9821, -0.0000, -99.4635 >>
		BREAK
		CASE 4
			vRetPos = << -1057.5558, -98.8819, 62.6970 >>
			vRetRot = << -43.8620, -0.0000, -53.4940 >>
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid green camera index for hole 8")
	ENDSWITCH
ENDPROC

PROC GET_GOLF_HOLE_NINE_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(INT index, VECTOR &vRetPos, VECTOR &vRetRot)
	SWITCH index
		CASE 0
			vRetPos = << -1127.2665, -20.2238, 65.8451 >>
			vRetRot = << -21.5872, -0.0000, 34.5271 >>
		BREAK
		CASE 1
			vRetPos = << -1172.8208, 33.7858, 62.5583 >>
			vRetRot = << -6.6738, 0.0000, 56.4890 >>
		BREAK
		CASE 2
			vRetPos = << -1212.7775, 62.3151, 72.1999 >>
			vRetRot = << -26.5597, -0.0000, 70.5980 >>
		BREAK
		CASE 3
			vRetPos = << -1258.0530, 92.9510, 71.2632 >>
			vRetRot = << -22.4006, -0.0000, 100.3488 >>
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid green camera index for hole 9")
	ENDSWITCH
ENDPROC

PROC GET_GOLF_PREVIEW_GREEN_CAMERA_SPLINE_AT_HOLE_AND_INDEX(INT iHole, INT index, VECTOR &vRetPos, VECTOR &vRetRot)
	SWITCH iHole
		CASE 0
			GET_GOLF_HOLE_ONE_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(index, vRetPos, vRetRot)
		BREAK
		CASE 1
			GET_GOLF_HOLE_TWO_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(index, vRetPos, vRetRot)
		BREAK
		CASE 2
			GET_GOLF_HOLE_THREE_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(index, vRetPos, vRetRot)
		BREAK
		CASE 3
			GET_GOLF_HOLE_FOUR_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(index, vRetPos, vRetRot)
		BREAK
		CASE 4
			GET_GOLF_HOLE_FIVE_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(index, vRetPos, vRetRot)
		BREAK
		CASE 5
			GET_GOLF_HOLE_SIX_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(index, vRetPos, vRetRot)
		BREAK
		CASE 6
			GET_GOLF_HOLE_SEVEN_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(index, vRetPos, vRetRot)
		BREAK
		CASE 7
			GET_GOLF_HOLE_EIGHT_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(index, vRetPos, vRetRot)
		BREAK
		CASE 8
			GET_GOLF_HOLE_NINE_PREVIEW_GREEN_CAMERA_SPLINE_AT_INDEX(index, vRetPos, vRetRot)
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GOLF_HOLE_CAMERA_START_AND_END(INT iHole, VECTOR &vStartPos, VECTOR &vEndPos)

	SWITCH iHole
		CASE 0
			vStartPos = <<-1374.0, 172.6, 0>>
			vEndPos   = <<-1141.4, 208.2, 0>>
		BREAK
		CASE 1
			vStartPos = <<-1102.9, 157.2, 0>>
			vEndPos   = <<-1304.2, 168.9, 0>>
		BREAK
		CASE 2
			vStartPos = <<-1313.6, 128.3, 0>>
			vEndPos   = <<-1238.7,  83.6, 0>>
		BREAK
		CASE 3
			vStartPos = <<-1219.8, 107.9, 0>>
			vEndPos   = <<-1109.8,  28.7, 0>>
		BREAK
		CASE 4
			vStartPos = <<-1101.3,  70.3, 0>> //yes this is right, starts from positve 70 and goes to negative 70
			vEndPos   = << -974.4, -73.3, 0>>
		BREAK
		CASE 5
			vStartPos = << -983.8, -103.7, 0>>
			vEndPos   = <<-1075.9, -109.8, 0>>
		BREAK
		CASE 6
			vStartPos = <<-1115.6, -104.6, 0>>
			vEndPos   = <<-1268.7,   -7.2, 0>>
		BREAK
		CASE 7
			vStartPos = <<-1275.3,  41.2, 0>>
			vEndPos   = <<-1059.0, -88.8, 0>>
		BREAK
		CASE 8
			vStartPos = <<-1137.0, -1.5, 0>>
			vEndPos   = <<-1276.7, 91.8, 0>>
		BREAK	
	
	ENDSWITCH

ENDPROC

FUNC BOOL IS_GOLF_COORD_ON_LEFT_OF_FAIRWAY(VECTOR vCoord, INT iHole)

	VECTOR vStartPoint, vEndPoint
	
	SWITCH iHole
		CASE 0
			vStartPoint = << -1368.52124, 172.21158, 57.01312 >>
			vEndPoint 	= <<-1125.14551, 199.77325, 63.89479>>
		BREAK
		CASE 1
			vStartPoint = <<-1134.17688, 149.05234, 62.00032>>
			vEndPoint 	= <<-1319.26160, 166.38510, 56.82042>>
		BREAK
		CASE 2
			vStartPoint = <<-1307.92554, 127.87812, 56.64053>>
			vEndPoint 	= <<-1247.85815, 102.13271, 55.64892>>
		BREAK
		CASE 3
			vStartPoint = <<-1207.61719, 104.42262, 57.04683>>
			vEndPoint 	= <<-1103.42798, 36.12697, 50.59733>>
		BREAK
		CASE 4
			vStartPoint = <<-1093.37866, 62.70148, 52.66673>>
			vEndPoint 	= <<-959.64417, -101.13708, 39.40543>>
		BREAK
		CASE 5
			vStartPoint = <<-989.71198, -110.51187, 39.60591>>
			vEndPoint 	= <<-1092.82458, -116.18317, 40.53841>>
		BREAK
		CASE 6
			vStartPoint = <<-1126.40894, -101.06005, 40.84630>>
			vEndPoint	= <<-1283.58594, -21.47935, 47.85569>>
		BREAK
		CASE 7
			vStartPoint = <<-1266.40979, 35.08315, 48.37233>>
			vEndPoint 	= <<-1047.60498, -93.48099, 42.56837>>
		BREAK
		CASE 8
			vStartPoint = <<-1137.12952, 7.77768, 48.13606>>
			vEndPoint	= <<-1276.85278, 94.21320, 53.77030>>
		BREAK
	ENDSWITCH
	
	RETURN ((vEndPoint.x - vStartPoint.x)* (vCoord.y - vStartPoint.y) - (vEndPoint.y - vStartPoint.y)*(vCoord.x - vStartPoint.x)) < 0
ENDFUNC

FUNC VECTOR GET_END_OF_HOLE_CAMERA_POS(INT iHole, INT index, VECTOR &vCamRot, FLOAT &FOV)
	SWITCH iHole
		CASE 0
			IF index = 0
				vCamRot = <<4.6807, -0.0000, 98.5813>>
				FOV = 40.5269
				RETURN <<-1094.3793, 158.8722, 63.4781>>
			ELIF index = 1
				vCamRot = <<3.2454, -0.0000, 98.5813>>
				FOV = 40.5269
				RETURN <<-1095.3110, 158.7316, 63.5552>>
			ELIF index = 2
				vCamRot = <<-3.3433, 0.0000, 78.1312>>
				FOV = 37.5666
				RETURN <<-1165.5881, 136.7941, 71.5705>>
			ELIF index = 3
				vCamRot = <<-3.4177, -0.0000, 89.8306>>
				FOV = 37.5666
				RETURN <<-1168.8252, 147.0191, 71.3499>>
			ELIF index = 4
				vCamRot = <<3.7196, -0.0000, -113.3335>>
				FOV = 35.4314
				RETURN <<-1335.8735, 169.6272, 59.6483>>
			ELSE
				vCamRot = <<3.7196, -0.0000, -113.3335>>
				FOV = 35.4314
				RETURN <<-1333.8400, 168.8565, 59.7896>>
			ENDIF
		BREAK
		CASE 1
			IF index = 0
				vCamRot = <<4.4568, -0.0000, -87.8237>>
				FOV = 45.9116
				RETURN <<-1322.8126, 126.5154, 57.9224>>
			ELIF index = 1
				vCamRot = <<4.4568, -0.0000, -87.8237>>
				FOV = 45.9116
				RETURN <<-1321.8717, 126.5511, 57.9958>>
			ELIF index = 2
				vCamRot = <<5.1447, -0.0000, 33.2004>>
				FOV = 45.9116
				RETURN <<-1251.6779, 94.2465, 57.9286>>
			ELIF index = 3
				vCamRot = <<5.1447, -0.0000, 33.2004>>
				FOV = 45.9116
				RETURN <<-1249.6210, 95.5924, 57.9286>>
			ELIF index = 4
				vCamRot =  <<-0.4914, -0.0000, -174.7261>>
				FOV = 45.9116
				RETURN <<-1241.1838, 120.3595, 58.2548>>
			ELSE
				vCamRot = <<-0.4914, -0.0000, -174.7261>>
				FOV = 45.9116
				RETURN <<-1241.0734, 119.1704, 58.2447>>
			ENDIF
		BREAK
		CASE 2
			IF index = 0
				vCamRot = <<-0.7562, -0.0000, -124.5854>>
				FOV = 38.4834
				RETURN <<-1228.7203, 114.3806, 58.9171>>
			ELIF index = 1
				vCamRot = <<-0.7562, -0.0000, -124.5854>>
				FOV = 38.4834
				RETURN <<-1227.9462, 113.8469, 58.9047>>
			ELIF index = 2
				vCamRot = <<-0.0200, -0.0000, 64.9655>>
				FOV = 34.9358
				RETURN <<-1097.9763, 46.7093, 59.8762>>
			ELIF index = 3
				vCamRot = <<0.3494, -0.0000, 73.9477>>
				FOV = 34.9358
				RETURN <<-1097.3260, 52.7992, 59.8767>>
			ELIF index = 4
				vCamRot = <<-0.8136, 0.0000, -81.5381>>
				FOV = 34.9358
				RETURN <<-1104.9528, 4.2624, 51.5391>>
			ELSE
				vCamRot = <<-0.8136, 0.0000, -81.5381>>
				FOV = 34.9358
				RETURN <<-1103.7354, 4.4436, 51.5216>>
			ENDIF
		BREAK
		CASE 3
			IF index = 0
				vCamRot = <<-0.6096, 0.0000, -170.0010>>
				FOV = 44.7168
				RETURN <<-1095.6362, 80.3156, 55.6221>>
			ELIF index = 1
				vCamRot = <<-0.6096, 0.0000, -170.0010>>
				FOV = 44.7168
				RETURN <<-1095.5004, 79.5601, 55.6139>>
			ELIF index = 2
				vCamRot = <<-3.0396, -0.0000, -138.2854>>
				FOV = 39.9005
				RETURN <<-1082.9509, 28.7458, 55.7276>>
			ELIF index = 3
				vCamRot = <<-3.0396, -0.0000, -138.2854>>
				FOV = 39.9005
				RETURN <<-1081.2046, 26.7871, 55.5883>>
			ELIF index = 4
				vCamRot = <<4.9468, -0.0000, 30.4095>>
				FOV = 39.9005
				RETURN <<-952.7022, -109.9683, 42.3249>>
			ELSE
				vCamRot = <<4.9468, -0.0000, 30.4095>>
				FOV = 39.9005
				RETURN <<-951.5651, -109.3009, 42.3249>>
			ENDIF
		BREAK
		CASE 4
			IF index = 0
				vCamRot = <<2.4594, 0.0000, 87.7383>>
				FOV = 50.8481
				RETURN <<-977.3425, -103.2089, 40.9894>>
			ELIF index = 1
				vCamRot = <<2.4594, 0.0000, 87.7383>>
				FOV = 50.8481
				RETURN <<-978.0359, -103.1816, 41.0192>>
			ELIF index = 2
				vCamRot = <<-2.1647, -0.0000, -65.6143>>
				FOV = 41.7968
				RETURN <<-1078.7642, -131.8416, 45.7591>>
			ELIF index = 3
				vCamRot = <<-2.1647, -0.0000, -65.6143>>
				FOV = 41.7968
				RETURN <<-1079.3359, -130.5815, 45.7591>>
			ELIF index = 4
				vCamRot = <<1.3729, 0.0000, -119.3426>>
				FOV = 41.7968
				RETURN <<-1108.5283, -109.6968, 42.3521>>
			ELSE
				vCamRot = <<1.3729, 0.0000, -119.3426>>
				FOV = 41.7968
				RETURN <<-1107.8715, -110.0659, 42.3702>>
			ENDIF
		BREAK
		CASE 5
			IF index = 0
				vCamRot = <<1.8288, 0.0000, 55.9156>>
				FOV = 49.0568
				RETURN <<-1107.5090, -109.7506, 43.3216>>
			ELIF index = 1
				vCamRot = <<1.8288, 0.0000, 55.9156>>
				FOV = 49.0568
				RETURN <<-1108.1251, -109.3343, 43.3453>>
			ELIF index = 2
				vCamRot = <<2.7417, -0.0000, 64.1222>>
				FOV = 49.0568
				RETURN <<-1154.3723, -73.2358, 48.3704>>
			ELIF index = 3
				vCamRot = <<2.7417, -0.0000, 64.1222>>
				FOV = 49.0568
				RETURN <<-1155.0842, -74.7036, 48.3704>>
			ELIF index = 4
				vCamRot = <<-2.1787, 0.0000, -142.5463>>
				FOV = 43.7116
				RETURN <<-1297.5459, 19.3382, 52.5940>>
			ELSE
				vCamRot = <<-2.1787, 0.0000, -142.5463>>
				FOV = 43.7116
				RETURN <<-1296.7295, 18.2717, 52.5428>>
			ENDIF
		BREAK
		CASE 6
			IF index = 0
				vCamRot = <<-3.8509, -0.0000, -129.3497>>
				FOV = 48.3305
				RETURN <<-1281.2493, 45.1062, 52.0434>>
			ELIF index = 1
				vCamRot = <<-3.8509, -0.0000, -129.3497>>
				FOV = 48.3305
				RETURN <<-1280.5311, 44.5174, 51.9809>>
			ELIF index = 2
				vCamRot = <<4.4008, -0.0000, 67.0328>>
				FOV = 39.9305
				RETURN <<-1085.5990, -62.8286, 48.9953>>
			ELIF index = 3
				vCamRot = <<4.4008, 0.0000, 63.0801>>
				FOV = 39.9305
				RETURN <<-1087.0726, -66.1035, 48.9953>>
			ELIF index = 4
				vCamRot = <<1.5572, -0.0000, 47.0931>>
				FOV = 39.9305
				RETURN <<-1032.4058, -86.9101, 43.9735>>
			ELSE
				vCamRot = <<1.5572, -0.0000, 47.0931>>
				FOV = 39.9305
				RETURN <<-1032.9202, -86.4627, 43.9919>>
			ENDIF
		BREAK
		CASE 7
			IF index = 0
				vCamRot = <<-0.9458, 0.0000, 63.0482>>
				FOV = 43.6572
				RETURN <<-1125.9897, -5.9281, 50.1341>>
			ELIF index = 1
				vCamRot = <<-0.9458, 0.0000, 63.0482>>
				FOV = 43.6572
				RETURN <<-1126.6157, -5.6096, 50.1225>>
			ELIF index = 2
				vCamRot = <<-0.6997, 0.0000, -117.3206>>
				FOV = 46.3961
				RETURN <<-1268.4615, 86.2135, 60.3628>>
			ELIF index = 3
				vCamRot = <<-0.6997, 0.0000, -117.3206>>
				FOV = 46.3961
				RETURN <<-1264.1373, 83.9794, 60.3033>>
			ELIF index = 4
				vCamRot = <<0.5610, 0.0000, 110.8630>>
				FOV = 37.8122
				RETURN <<-1274.6261, 85.7493, 55.9815>>
			ELSE
				vCamRot = <<0.5610, 0.0000, 110.8630>>
				FOV = 37.8122
				RETURN <<-1276.2941, 85.1135, 55.9990>>
			ENDIF
		BREAK
		DEFAULT
			CDEBUG1LN(DEBUG_GOLF,"End Hole Camera is ,", iHole)
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

PROC SET_END_OF_HOLE_STREAMVOL(GOLF_HELPERS &thisHelpers, INT iHole)
	VECTOR vStartCamPos, vStartCamDir, vStartCamRot
	FLOAT FOV
	
	IF STREAMVOL_IS_VALID(thisHelpers.streamVolScorecard)
		STREAMVOL_DELETE(thisHelpers.streamVolScorecard)
	ENDIF
	
	vStartCamPos = GET_END_OF_HOLE_CAMERA_POS(iHole, thisHelpers.iLastEndCameraIndex, vStartCamRot, FOV)
	vStartCamDir = CONVERT_GOLF_ROTATION_TO_DIRECTION_VECTOR(vStartCamRot)
		
	TEXT_LABEL_31 strPos = V2STR(vStartCamPos)	
	TEXT_LABEL_31 strDir = V2STR(vStartCamDir)	
	CDEBUG1LN(DEBUG_GOLF, "SET_END_OF_HOLE_STREAMVOL: Creating frustum at <<", strPos, ">>, <<", strDir, ">> with hole: ", iHole, " and index: ", thisHelpers.iLastEndCameraIndex)	
	thisHelpers.streamVolScorecard = STREAMVOL_CREATE_FRUSTUM(vStartCamPos, vStartCamDir, 100, FLAG_MAPDATA, STREAMVOL_LOD_FLAG_ALL)
ENDPROC

PROC SET_END_OF_HOLE_CAMERA(GOLF_HELPERS &thisHelpers, INT iHole, BOOL bAllowDisplayScoreboard = TRUE, BOOL bEndGameCamera = FALSE)

	VECTOR vStartCamPos, vStartCamRot, vEndCamPos, vEndCamRot
	FLOAT FOV
	INT iInterpTime
	
	IF NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SCORECARD_STREAMVOL_LOADED)	
		//CDEBUG1LN(DEBUG_GOLF, "SET_END_OF_HOLE_CAMERA: Spinning in first check")
		IF IS_STREAMVOL_ACTIVE()
			IF STREAMVOL_IS_VALID(thisHelpers.streamVolScorecard)
				IF STREAMVOL_HAS_LOADED(thisHelpers.streamVolScorecard)
				OR IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_FORCE_SCORECARD_VOL_LOAD)
					CDEBUG1LN(DEBUG_GOLF, "SET_END_OF_HOLE_CAMERA: Setting GSF_SCORECARD_STREAMVOL_LOADED. Delete streamvol")
					SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_SCORECARD_STREAMVOL_LOADED)
					STREAMVOL_DELETE(thisHelpers.streamVolScorecard)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SCORECARD_STREAMVOL_LOADED)
		//CDEBUG1LN(DEBUG_GOLF, "SET_END_OF_HOLE_CAMERA: Spinning in second check")
		IF NOT IS_GOLF_CAM_ACTIVE(thisHelpers.camNavigate2) 
		OR NOT IS_CAM_INTERPOLATING(thisHelpers.camNavigate2)
			
			IF DOES_CAM_EXIST(thisHelpers.camNavigate)
				DESTROY_CAM(thisHelpers.camNavigate)
			ENDIF
			IF DOES_CAM_EXIST(thisHelpers.camNavigate2)
				DESTROY_CAM(thisHelpers.camNavigate2)
			ENDIF
			
			IF bEndGameCamera
				vStartCamPos = <<-1309.448853,120.340935,83.777374>>
				vStartCamRot = <<2.624787,0.000000,-123.091408>>
				
				vEndCamPos = <<-1294.196899,110.403435,84.611801>>
				vEndCamRot = <<2.624787,0.000000,-123.091408>>
				
				FOV = 46.396099
				thisHelpers.iLastEndCameraIndex = 2
				iInterpTime = 32000
			ELSE
				vStartCamPos = GET_END_OF_HOLE_CAMERA_POS(iHole, thisHelpers.iLastEndCameraIndex,   vStartCamRot, FOV)
				vEndCamPos   = GET_END_OF_HOLE_CAMERA_POS(iHole, thisHelpers.iLastEndCameraIndex+1, vEndCamRot, FOV)
				iInterpTime = 6000
			ENDIF
					
			thisHelpers.camNavigate =  CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vStartCamPos, vStartCamRot)
			thisHelpers.camNavigate2 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vEndCamPos,   vEndCamRot)
				
			SET_CAM_FOV(thisHelpers.camNavigate, FOV)
			SET_CAM_FOV(thisHelpers.camNavigate2, FOV)
			
			CAMERA_GRAPH_TYPE camGraph = GRAPH_TYPE_LINEAR
			IF thisHelpers.iLastEndCameraIndex = 0
				camGraph = GRAPH_TYPE_ACCEL
			ELIF thisHelpers.iLastEndCameraIndex = 4
				camGraph = GRAPH_TYPE_DECEL
			ENDIF
			SET_CAM_ACTIVE_WITH_INTERP(thisHelpers.camNavigate2, thisHelpers.camNavigate, iInterpTime, camGraph, camGraph)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SET_FOCUS_POS_AND_VEL(vStartCamPos, <<0,0,0>>)
			
			IF bAllowDisplayScoreboard
				DISPLAY_SCOREBOARD(thisHelpers, TRUE)
			ENDIF
			
			thisHelpers.iLastEndCameraIndex = PICK_INT(thisHelpers.iLastEndCameraIndex + 2 > 5, 0, thisHelpers.iLastEndCameraIndex+2)
			CDEBUG1LN(DEBUG_GOLF, "SET_END_OF_HOLE_CAMERA: Clearing GSF_SCORECARD_STREAMVOL_LOADED")
			SET_END_OF_HOLE_STREAMVOL(thisHelpers, iHole)
			CLEAR_GOLF_STREAMING_FLAG(thisHelpers, GSF_SCORECARD_STREAMVOL_LOADED)
		ENDIF
	ENDIF
ENDPROC


PROC GET_TERRAIN_GRID_DATA(INT iHole, VECTOR &vCenterPos, VECTOR &vForward, FLOAT &fGridRes, FLOAT &fGridWidth, FLOAT &fGridHeight)

	SWITCH iHole
		CASE 0
			vCenterPos  = <<-1119.991, 222.374, 64.650>>
			vForward    = <<-0.314, 0.948, 0.058>>
			fGridRes    = 35.000
			fGridWidth  = 19.460
			fGridHeight = 23.020
		BREAK
	
	ENDSWITCH
	
ENDPROC

FUNC BOOL IS_GOLF_COORD_NEAR_BUNKER(VECTOR vCoord, INT iHole, VECTOR &vOutOfBunkerCoord, INT iOutofBunkerIndex)
	vOutOfBunkerCoord = <<0,0,0>>

	SWITCH iHole
		CASE 0
			IF IS_POINT_IN_ANGLED_AREA(vCoord, <<-1249.828247,188.866852,63.771088>>, <<-1234.003540,191.454483,63.600731>>, 15.5, FALSE, FALSE)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1245.3962, 177.2120, 60.7943>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1248.5626, 177.2773, 60.7550>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1241.0245, 174.8023, 60.7391>>
				ELSE
					vOutOfBunkerCoord = <<-1244.9492, 174.9718, 60.4628>>
				ENDIF
				
			ELIF IS_POINT_IN_ANGLED_AREA(vCoord, <<-1218.842651,195.196243,65.025368>>, <<-1236.062012,192.200455,63.549568>> , 15.5, FALSE, FALSE)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1226.7131, 180.4805, 62.4568>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1229.9747, 180.3678, 62.4090>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord =  <<-1228.1005, 178.8995, 62.2322>>
				ELSE
					vOutOfBunkerCoord = <<-1224.8120, 179.1062, 62.3527>>
				ENDIF
			
			ELIF IS_POINT_IN_ANGLED_AREA(vCoord, <<-1128.345581,200.679260,63.879005>>, <<-1124.463867,218.241745,64.954010>>, 8.5, FALSE, FALSE)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1133.4523, 214.3222, 63.9877>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1133.9468, 212.6187, 63.9865>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1135.0956, 214.6792, 64.0555>>
				ELSE
					vOutOfBunkerCoord = <<-1134.0245, 216.4510, 64.0271>>
				ENDIF
			ELIF IS_POINT_IN_ANGLED_AREA(vCoord, <<-1124.228638,206.006500,63.737614>>, <<-1112.910767,205.865417,64.855797>>, 13.0, FALSE, FALSE)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1117.0522, 197.3494, 63.5884>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1119.3030, 197.4900, 63.5835>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1118.4226, 195.8079, 63.5530>>
				ELSE
					vOutOfBunkerCoord = <<-1116.7025, 195.9991, 63.6064>>
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
		
			IF IS_POINT_NEAR_POINT(vCoord, <<-1221.480347,136.972229,57.883053>> , 12.25)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1210.6311, 145.2679, 58.9165>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1209.8438, 143.8164, 58.8644>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1208.9712, 142.6966, 58.8439>>
				ELSE
					vOutOfBunkerCoord = <<-1208.7344, 144.4290, 58.9932>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1307.994385,148.265793,57.013622>>  , 12.25)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1298.0403, 158.6725, 57.4900>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1296.9418, 160.0557, 57.4344>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1300.7107, 161.1427, 57.3667>>
				ELSE
					vOutOfBunkerCoord = <<-1299.6464, 163.0880, 57.4339>>
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 2
			
			IF IS_POINT_NEAR_POINT(vCoord, <<-1249.797241,120.948471,55.954487>>, 10.25)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1261.0046, 112.5053, 55.7922>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1259.0416, 111.0428, 55.7076>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1261.3955, 110.6866, 55.7104>>
				ELSE
					vOutOfBunkerCoord = <<-1262.2742, 114.0666, 55.8884>>
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 3
			IF IS_POINT_NEAR_POINT(vCoord, <<-1148.604980,90.545326,56.792549>>, 9.0)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1160.4265, 89.4942, 57.1402>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1160.5393, 91.2892, 57.1851>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1160.3037, 93.5336, 57.3266>>
				ELSE
					vOutOfBunkerCoord = <<-1159.3896, 95.3945, 57.3836>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1123.917236,62.398174,54.097630>>, 12.25)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1128.7260, 74.4978, 55.1694>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1132.3936, 74.8003, 55.3063>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1135.1388, 73.6844, 55.3300>>
				ELSE
					vOutOfBunkerCoord = <<-1135.5647, 71.0212, 55.0845>>
				ENDIF
			ENDIF	
		BREAK
	
		CASE 4
			IF IS_POINT_NEAR_POINT(vCoord, <<-1022.973145,9.365685,48.729454>>, 10.0)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1029.7274, 20.6269, 49.6834>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1027.7196, 21.4991, 49.6488>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1031.9130, 18.7379, 49.6796>>
				ELSE
					vOutOfBunkerCoord = <<-1033.0242, 16.8601, 49.6274>>
				ENDIF
			ELIF IS_POINT_IN_ANGLED_AREA(vCoord, <<-990.976929,-12.224630,45.475269>>, <<-1014.560608,-2.282516,48.512466>> , 18.25, FALSE, FALSE)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-992.1171, -0.4806, 47.2586>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-990.9562, -3.1426, 46.9676>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-990.7891, -4.6779, 46.7283>>
				ELSE
					vOutOfBunkerCoord = <<-990.6757, -1.6019, 47.2180>>
				ENDIF
			ELIF IS_POINT_IN_ANGLED_AREA(vCoord, <<-1002.702698,-19.814455,45.336033>>, <<-1013.572632,-35.536903,45.560959>>, 16.25, FALSE, FALSE)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1014.7323, -16.9885, 46.0129>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1016.0983, -18.4531, 45.9076>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1017.3691, -20.0257, 45.7855>>
				ELSE
					vOutOfBunkerCoord = <<-1018.2857, -21.7339, 45.6093>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1014.070129,-38.888248,44.127777>> , 3.75)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1019.7245, -32.4241, 44.6345>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1021.8109, -34.9325, 44.3734>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1022.8051, -37.8995, 44.0943>>
				ELSE
					vOutOfBunkerCoord = <<-1023.6850, -36.2095, 44.2471>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-974.335449,-87.085876,39.365379>>  , 12.5)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-986.5154, -79.0639, 40.3237>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-987.2600, -81.1786, 40.4433>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-987.3561, -83.2422, 40.5134>>
				ELSE
					vOutOfBunkerCoord = <<-987.2036, -85.4018, 40.5124>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-956.215027,-73.618683,39.467106>>, 11.25)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-965.3108, -62.4259, 40.8340>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-966.5059, -63.5912, 40.8414>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-967.5871, -64.9728, 40.7888>>
				ELSE
					vOutOfBunkerCoord = <<-968.6852, -62.2530, 41.0284>>
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF IS_POINT_NEAR_POINT(vCoord, <<-1047.703857,-99.853600,41.880566>> , 7.5)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1037.8843, -103.3558, 41.6942>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1037.3390, -101.2052, 41.9289>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1037.3920, -98.6590, 42.1714>>
				ELSE
					vOutOfBunkerCoord = <<-1038.0197, -105.0445, 41.4403>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1088.469604,-108.750587,40.327614>>, 10.0)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1077.0906, -102.6780, 40.9184>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1076.3594, -105.3632, 40.8033>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1075.4891, -107.4368, 40.7636>>
				ELSE
					vOutOfBunkerCoord = <<-1075.1426, -109.1178, 40.7142>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1087.139038,-125.955559,39.887203>> , 9.5)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1075.6034, -127.5685, 40.1276>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1076.0428, -128.7151, 40.0900>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1076.6813, -130.3401, 40.0376>>
				ELSE
					vOutOfBunkerCoord = <<-1077.3766, -132.3562, 39.9749>>
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF IS_POINT_NEAR_POINT(vCoord, <<-1275.230347,-13.947482,47.105656>>, 13.0)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1265.8893, -27.2585, 46.6329>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1269.1022, -28.6319, 46.7600>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1272.0457, -29.1311, 46.8971>>
				ELSE
					vOutOfBunkerCoord = <<-1274.3591, -29.0810, 47.0307>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1300.141846,16.414881,50.347145>> , 11.0)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1303.5238, 29.1151, 51.6491>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1306.3669, 28.4745, 51.8943>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1309.0829, 27.1428, 52.1360>>
				ELSE
					vOutOfBunkerCoord = <<-1311.1244, 25.4314, 52.3217>>
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			IF IS_POINT_IN_ANGLED_AREA(vCoord, <<-1153.759155,5.592854,49.258270>>, <<-1193.293335,-3.027822,47.537586>> , 16.0, FALSE, FALSE)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1174.0311, 13.0789, 48.9210>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1176.1425, 12.0404, 48.8421>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1177.7793, 12.7224, 48.8837>>
				ELSE
					vOutOfBunkerCoord = <<-1177.7153, 11.1655, 48.7797>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1096.560547,-79.871956,43.651833>>, 11.0)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1109.0933, -76.4043, 42.4111>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1108.5151, -74.3624, 42.3498>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1107.7117, -72.2814, 42.2795>>
				ELSE
					vOutOfBunkerCoord = <<-1106.5951, -70.5346, 42.2694>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1055.672729,-77.093796,43.748905>> , 13.25)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1060.0410, -65.1439, 43.2142>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1057.8276, -64.7564, 43.1777>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1055.5619, -63.9114, 43.1779>>
				ELSE
					vOutOfBunkerCoord = <<-1065.8571, -67.2540, 43.2841>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1047.474365,-100.369911,43.101807>>, 7.25)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1054.9695, -106.0665, 41.2801>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1053.6267, -106.6474, 41.3730>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1052.3851, -107.0745, 41.4528>>
				ELSE
					vOutOfBunkerCoord = <<-1051.1041, -107.5534, 41.4978>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1024.672974,-84.070450,43.897423>> , 6.5)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1019.6564, -91.2034, 42.1369>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1018.5802, -89.0888, 42.2369>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1017.7621, -86.8312, 42.2233>>
				ELSE
					vOutOfBunkerCoord = <<-1017.4727, -84.2646, 42.2985>>
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF IS_POINT_IN_ANGLED_AREA(vCoord, <<-1153.759155,5.592854,49.258270>>, <<-1193.293335,-3.027822,47.537586>> , 16.0, FALSE, FALSE)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1169.0970, -7.1960, 46.4323>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1167.2222, -7.1338, 46.4319>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1164.8500, -7.2801, 46.4345>>
				ELSE
					vOutOfBunkerCoord = <<-1162.7856, -7.8826, 46.4761>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1267.601196,67.514320,52.510513>> , 6.75)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1261.2919, 61.7125, 50.4284>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1259.6598, 63.7194, 50.5575>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1258.2268, 65.9119, 50.6718>>
				ELSE
					vOutOfBunkerCoord = <<-1258.2637, 68.0789, 50.8772>>
				ENDIF
			ELIF IS_POINT_IN_ANGLED_AREA(vCoord,<<-1273.795288,67.980339,54.251511>>, <<-1287.217651,66.198929,53.769260>>, 16.0, FALSE, FALSE)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1269.3099, 73.8897, 52.1591>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1269.9092, 75.9058, 52.3567>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1267.6621, 73.8826, 52.0383>>
				ELSE
					vOutOfBunkerCoord = <<-1268.6478, 76.1384, 52.2743>>
				ENDIF
			ELIF IS_POINT_IN_ANGLED_AREA(vCoord,<<-1287.997559,72.037735,55.717316>>, <<-1299.049683,67.997520,54.094429>> , 9.5, FALSE, FALSE)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1304.0128, 71.0417, 53.2779>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1303.5636, 69.7113, 53.2013>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1303.6740, 67.6263, 52.9456>>
				ELSE
					vOutOfBunkerCoord = <<-1303.1334, 65.3647, 52.5448>>
				ENDIF
			ELIF IS_POINT_NEAR_POINT(vCoord, <<-1283.160034,90.426811,55.650764>>, 8.0)
				IF iOutofBunkerIndex = 0
					vOutOfBunkerCoord = <<-1292.8694, 99.5730, 54.4191>>
				ELIF iOutofBunkerIndex = 1
					vOutOfBunkerCoord = <<-1290.1041, 100.9091, 54.5348>>
				ELIF iOutofBunkerIndex = 2
					vOutOfBunkerCoord = <<-1288.1364, 101.3769, 54.5255>>
				ELSE
					vOutOfBunkerCoord = <<-1285.9658, 101.4354, 54.5018>>
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN NOT IS_VECTOR_ZERO(vOutOfBunkerCoord)

ENDFUNC

FUNC BOOL IS_GOLF_COORD_NEAR_LAKE(VECTOR vCoord, INT iHole, VECTOR &vOutCoord, INT iOutIndex)

	vOutCoord = <<0, 0, 0>>
	
	SWITCH iHole
		CASE 4
			IF IS_POINT_IN_ANGLED_AREA(vCoord, <<-1025.054077,35.634628,49.516659>>, <<-1063.490967,-0.983391,49.584190>>, 14.000000, FALSE, FALSE)
				SWITCH iOutIndex
					CASE 0	vOutCoord = <<-1053.8479,  6.4804, 50.0762>> BREAK
					CASE 1	vOutCoord = <<-1013.1949, 34.3117, 49.6766>> BREAK
					CASE 2	vOutCoord = <<-1010.7953, 30.7865, 49.5420>> BREAK
					CASE 3	vOutCoord = <<-1009.6174, 28.4031, 49.4536>> BREAK
					CASE 4	vOutCoord = <<-1007.7808, 31.3775, 49.5029>> BREAK
					CASE 5	vOutCoord = <<-1008.8806, 33.5182, 49.5793>> BREAK
				ENDSWITCH
			ENDIF
	ENDSWITCH
	
	
	RETURN NOT IS_VECTOR_ZERO(vOutCoord)
ENDFUNC
