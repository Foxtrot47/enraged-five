CONST_INT GOLF_IS_MP 0
CONST_INT COMPILE_LOOKUPS 1
CONST_INT GOLF_USE_FALLTHROUGH_WORLD_CHECK 1

//TWEAK_FLOAT	RADS_0	20.0
//TWEAK_FLOAT	RADS_1	20.0
//TWEAK_FLOAT	RADS_2	20.0
//TWEAK_FLOAT	RADS_3	20.0
//TWEAK_FLOAT	RADS_4	20.0
//TWEAK_FLOAT	RADS_5	20.0
//TWEAK_FLOAT	RADS_6	20.0
//TWEAK_FLOAT	RADS_7	20.0
//TWEAK_FLOAT	RADS_8	20.0
//TWEAK_FLOAT	RADS_9	20.0
//#IF IS_DEBUG_BUILD
//VECTOR vTempSpheres[10]
//INT iSphereTracker
//#ENDIF

USING "minigame_big_message.sch"

BOOL bDebugSpew = FALSE
MG_FAIL_SPLASH splashFail

USING "rgeneral_include.sch"
//INT tempTimer
USING "golf.sch"
USING "golf_core.sch"
USING "socialclub_leaderboard.sch"

///    Feature Schedule
///    -2. Shot estimates use new shot direction algorithm
///	   0. Collision normal - effects spin force applied on impact
///    1. Ground Materials - effects on spin force applied
///    4. Physics tuning
///    5. Cameras Polish
///    
///	   Bugs
///    
///    Ball still falls through ground sometimes - added a timeout
///    
///    TODO / minor points
///    hide other balls during putt
///    now we have 1 frame where characters are disappearing on green
///    
///    3click/stick swing - Spin meter needs to be frame independant (currently you cna spin better with faster framerate  (swing)
///    AI - putting at extreme distances
///    
///    
///    Later
///    Detecting Water/OOB (physics - materials)
///    
///    Clean up walking / running into zone transitions (animation)
///    material / ball tunings - fairway and green (physics)
///    
///    shot estimate - rough, could use better (Swing) - TODO in with code to get an in-game simulator that matches physics
///    save data: Course LowestScoreFront (other)
///    save data: Course LowestScoreBack (other)
///    save data: Course LowestScoreTotal (other)
///    save data: Course AveragePutts % (other)
///    save data: Course FairwayDrives % (other)
///    save data: Overall LongestPutt (other)
///    save data: Overall AveragePutts % (other)
///    save data: Overall FairwayDrives % (other)
///
///    
///	   Golf notes
///	   quick fade to warp player on skip - lets see if we can't get it working as is right now
///
///	   Long term thoughts
///	   Radio needs to stay on after you leave the golfcart
///    
///    Stuff from Tiger Woods 12 to incorporate
///    Need to draw stick path for player
///    Ball first hit and final resting spot
///    Need green slope indicators
///    Score screen should track symbols for eagle, birdie
///    



SCRIPT
	
	STREAMED_MODEL	golfAssets[13]
	GOLF_PLAYER		currentPlayers[4]
	GOLF_FOURSOME	currentPlayerFoursome
	GOLF_COURSE		currentCourse
	GOLF_GAME		currentGame
	GOLF_HELPERS 	currentHelpers
	GOLF_MINIGAME_STATE		golfState, prevState
//	VECTOR vPreviousTime = <<GET_CLOCK_HOURS(), GET_CLOCK_MINUTES(), GET_CLOCK_SECONDS()>>
	BOOL bPlayerControlWasActiveBeforePause = FALSE
	BOOL bIsHeadTexturedRegistered = FALSE
	INT iSignedOutWarningBits
	
	SCRIPT_SCALEFORM_BIG_MESSAGE scaleformFail
	
	PRINTLN("Start Golf Script")
//	bDrivingRangeMode = TRUE
//	bInfiniteShots = TRUE
//	bPerfectAccuracyMode = TRUE
//	bDisableWindEffects = TRUE
//	iDebugShotHole = 1
//	iDebugShotNumber = 1
//	iDebugShotLie = 2
//	

//	TRACE_NATIVE_COMMAND("CLEAR_PRINTS")

	IF VDIST2(GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()), FALSE),<< -1371.9086, 56.2860, 52.6365 >>) > 100
		PRINTLN("Far away from start spot, fading out and going to GMS_PRE_INIT")
		golfState = GMS_PRE_INIT
		DO_SCREEN_FADE_OUT(500)
	ELSE
		golfState = GMS_INIT
	ENDIF
	INT iCurrentPlayer
	SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_GOLF, FALSE)
	
	currentHelpers.iStartingHole = 0
	currentHelpers.iEndingHole = 8
	
	SETUP_PC_GOLF_CONTROLS()
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)	//Don't enable multihead blinders
	TOGGLE_STEALTH_RADAR(FALSE)
	SET_MINIGAME_IN_PROGRESS(TRUE)
	CLEAR_HELP(TRUE)
	CLEAR_PRINTS()
	PAUSE_CLOCK(TRUE)
	//g_bPauseCommsQueues = TRUE
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		IF IS_PED_DUCKING(PLAYER_PED_ID()) // forces player to not crouch
			SET_PED_DUCKING(PLAYER_PED_ID(), FALSE)
		ENDIF
	ENDIF
	STOP_FIRE_IN_RANGE(<< -1371.9086, 56.2860, 52.6365 >>, 10.0)
	
	SET_TEXT_INPUT_BOX_ENABLED(FALSE)	//B* 2184339: Disable cheats in Golf
	
	currentHelpers.vehicleName = DUMMY_MODEL_FOR_SCRIPT
	currentHelpers.playerLastVehicle = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(currentHelpers.playerLastVehicle )
		IF NOT IS_ENTITY_DEAD(currentHelpers.playerLastVehicle)
			IF GET_ENTITY_MODEL(GET_ENTITY_FROM_PED_OR_VEHICLE(currentHelpers.playerLastVehicle)) = TAXI
			AND NOT IS_VEHICLE_SEAT_FREE(currentHelpers.playerLastVehicle, VS_DRIVER)
				PRINTLN("GOLF: The player was brought to the course in a taxi. There should be no vehicle for him in the lot")
				currentHelpers.playerLastVehicle = NULL
			ELSE
				currentHelpers.vehicleName = GET_ENTITY_MODEL(GET_ENTITY_FROM_PED_OR_VEHICLE(currentHelpers.playerLastVehicle))
				currentHelpers.iVehicleColor = GET_VEHICLE_COLOUR_COMBINATION(currentHelpers.playerLastVehicle)
				REQUEST_MODEL(currentHelpers.vehicleName)
				
				//B* - 1958121
				IF GET_IS_VEHICLE_ENGINE_RUNNING(currentHelpers.playerLastVehicle)
					SET_VEHICLE_ENGINE_ON(currentHelpers.playerLastVehicle, FALSE, TRUE)
				ENDIF
				
				GET_VEHICLE_SETUP(currentHelpers.playerLastVehicle, currentHelpers.sPlayerVehicle)
				
				PRINTLN("Start of golf car model ", currentHelpers.vehicleName, " car colour ", currentHelpers.iVehicleColor)
			ENDIF
			
			IF NOT IS_ENTITY_A_MISSION_ENTITY(currentHelpers.playerLastVehicle)
				SET_ENTITY_AS_MISSION_ENTITY(currentHelpers.playerLastVehicle)
			ENDIF
		ELSE
			PRINTLN("GOLF: Player's vehicle is dead. Don't save last vehicle")
			currentHelpers.playerLastVehicle = NULL
		ENDIF
	ELSE
		PRINTLN("GOLF: Player's vehicle does not exist. Don't save last vehicle")
		currentHelpers.playerLastVehicle = NULL
	ENDIF
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		SCRIPT_GOLF_CLEANUP(currentGame, currentCourse, currentPlayerFoursome, currentPlayers, currentHelpers)
	ENDIF
	SCRIPT_SETUP(currentGame)
	currentHelpers.iTimesPlayedGolf = GET_GOLF_STAT_ROUNDS_PLAYED()
	
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	#ENDIF
	CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_LeaveEngineOnWhenExitingVehicles, TRUE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_AllowLockonToFriendlyPlayers, TRUE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_CanAttackFriendly, TRUE)
		
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
	
//	int hole, iGenIndex
//	VECTOR vDebugVector0, vDebugVector1
//	#IF IS_DEBUG_BUILD
//	iSphereTracker = 0
//	#ENDIF
	
	DISABLE_SELECTOR()
	INITAILIZE_GOLF_STATS()
	
	WHILE (TRUE)
//		CDEBUG1LN(DEBUG_GOLF, "TEST")
//		PRINTLN("Test")
		#IF IS_DEBUG_BUILD
			
			GOLF_MANAGE_DEBUG_INPUT(golfState, currentPlayerFoursome, currentPlayers, currentHelpers) // NOTE THIS CAN CHANGE GOLF STATE
			GOLF_DEBUG_DRAW(currentGame, currentCourse, currentPlayerFoursome)
			
			IF bDebugShotUse
				SET_GOLF_DEBUG_SHOT(currentPlayerFoursome, currentCourse)
			ENDIF
			IF bDebugHardCodedShotUse
				SET_GOLF_DEBUG_HARD_CODED_SHOT(currentPlayerFoursome, currentCourse)
			ENDIF
			IF bSetPlayerScore[0] OR bSetPlayerScore[1] OR bSetPlayerScore[2] OR bSetPlayerScore[3]
				SET_GOLF_DEBUG_PLAYER_SCORE(currentPlayerFoursome, currentHelpers, currentPlayers)
			ENDIF
			
//			IS_GOLF_COORD_OUTSIDE_HOLE_BOUNDING_SPHERES(<<0,0,0>>, currentPlayerFoursome)
//			IF IS_GOLF_COORD_OUTSIDE_BOUNDARY_PLANES(GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS())
//				PRINTLN("Outside")
//			ENDIF
//			REPEAT 9 hole
//				REPEAT 4 iGenIndex
//					DRAW_DEBUG_SPHERE(GET_GOLF_HOLE_GREEN_SPECTATE_POSITION(currentCourse, hole, iGenIndex), 0.5)
//				ENDREPEAT
//				DRAW_DEBUG_SPHERE(GET_GOLF_HOLE_PIN_POSITION(currentCourse, hole), DIST_TO_GREEN_HACK)
//				DRAW_DEBUG_SPHERE(GET_GOLF_HOLE_PIN_POSITION(currentCourse, hole), GIMMIE_FORCE_RADIUS, 0, 0, 255, 50)
//				DRAW_DEBUG_SPHERE(GET_GOLF_HOLE_PIN_POSITION(currentCourse, hole), BALL_IN_HOLE_RADIUS, 255, 0, 0, 50)
//			ENDREPEAT
//			hole = GET_GOLF_FOURSOME_CURRENT_HOLE(currentPlayerFoursome)
//			REPEAT GET_NUM_OF_GREEN_SPLINE_CAMERAS_FOR_HOLE(hole) iGenIndex
//				GET_GOLF_PREVIEW_GREEN_CAMERA_SPLINE_AT_HOLE_AND_INDEX(hole, iGenIndex, vDebugVector0, vDebugVector1)
//				DRAW_DEBUG_SPHERE(vDebugVector0, 5.0)
//			ENDREPEAT
//			draw_debug_screen_grid()
		#ENDIF
		
		DISABLE_GOLF_CONTROLS(FALSE, GOLF_SHOULD_ALLOW_ALT_CONTROLS(currentPlayerFoursome))
		IF golfState != GMS_PLAY_GOLF
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		ENDIF
		
		IF golfState != GMS_CLEANUP
			DISABLE_GOLF_HUD(golfState >= GMS_PLAY_GOLF)
		ENDIF
		
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		
		IF IS_ENTITY_DEAD(PLAYER_PED_ID())
			SCRIPT_GOLF_CLEANUP(currentGame, currentCourse,currentPlayerFoursome, currentPlayers, currentHelpers)
		ENDIF
		
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0) AND NOT IS_GOLF_HELP_DISPLAYED_FLAG_SET(currentHelpers, GHD_GOLF_TERMINATED_DISPLAYED)
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ENDIF
		
//		SET_CLOCK_TIME(FLOOR(vPreviousTime.x), FLOOR(vPreviousTime.y), FLOOR(vPreviousTime.z))
		
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_31 vPos
	#ENDIF
		SWITCH golfState
			CASE GMS_PRE_INIT
				IF IS_SCREEN_FADED_OUT()
					CLEAR_PRINTS()
					SET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()),<< -1371.9086, 56.2860, 52.6365 >>)
					SET_ENTITY_HEADING(GET_PLAYER_PED(GET_PLAYER_INDEX()), GET_HEADING_BETWEEN_VECTORS(<< -1371.9086, 56.2860, 52.6365 >>, << -1341.6656, 57.7221, 54.0804 >>))
					DEBUG_MESSAGE("Set Network Start Load Scene at << -1371.9086, 56.2860, 52.6365 >>")
					golfState = GMS_PRE_INIT_WAIT
				ENDIF
			BREAK
			CASE GMS_PRE_INIT_WAIT
				IF NOT IS_STREAMVOL_ACTIVE()
					IF NEW_LOAD_SCENE_START(<<-1218.0029, 115.7468, 59.0221>>, CONVERT_GOLF_ROTATION_TO_DIRECTION_VECTOR(<<-7.7232, -0.0000, 116.6793>>), 152.0)
						DEBUG_MESSAGE("NETWORK_UPDATE_LOAD_SCENE returned TRUE, continue!")
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						DO_SCREEN_FADE_IN(250)
						REQUEST_WAYPOINT_RECORDING("golf_intro")
						golfState = GMS_INIT
					ELSE
						DEBUG_MESSAGE("NETWORK_UPDATE_LOAD_SCENE returned FALSE, wait more til everything is loaded")
					ENDIF
				ELSE
					PRINTLN("Stream volume active")
				ENDIF
			BREAK
			CASE GMS_INIT
				IF bDebugSpew DEBUG_MESSAGE("GMS_INIT") ENDIF
				REQUEST_WAYPOINT_RECORDING("golf_intro")
				REQUEST_STREAMED_TEXTURE_DICT("ShopUI_Title_GolfShop")
				IF GET_IS_WAYPOINT_RECORDING_LOADED("golf_intro")
				AND HAS_STREAMED_TEXTURE_DICT_LOADED("ShopUI_Title_GolfShop")
					GOLF_SETUP_CURRENT_COURSE(currentCourse)
					GOLF_STREAM_ASSETS(golfAssets)
					GOLF_STREAM_UI(currentHelpers)
					// Game first initting. We need the skip to hole menu
					REQUEST_ADDITIONAL_TEXT("SP_GOLF", MINIGAME_TEXT_SLOT)
					REQUEST_ANIM_DICT("mini@golfclubhouse")
					
					#IF IS_DEBUG_BUILD
						GOLF_SETUP_DEBUG_WIDGETS(currentGame#IF NOT COMPILE_LOOKUPS, currentCourse#ENDIF , "Golf")
					#ENDIF
					SET_GOLF_FOURSOME_NUM_PLAYERS(currentPlayerFoursome, 2)
					GOLF_MANAGE_INTRO_FLYOVER(currentGame, golfState, currentPlayerFoursome, currentHelpers)
					golfState = GMS_INIT_STREAMING_DONE
				ENDIF
			BREAK
			
			CASE GMS_INIT_STREAMING_DONE
				IF bDebugSpew DEBUG_MESSAGE("GMS_INIT_STREAMING_DONE") ENDIF
				GOLF_MANAGE_INTRO_FLYOVER(currentGame, golfState, currentPlayerFoursome, currentHelpers)
				IF GOLF_ASSETS_ARE_STREAMED(currentHelpers, golfAssets) AND HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
				AND HAS_ANIM_DICT_LOADED("mini@golfclubhouse")
					SET_SCOREBOARD_TITLE(currentHelpers)
					SET_COURSE_PAR(currentHelpers)
					golfState = GMS_INIT_POST_STREAMING
				ENDIF
			BREAK
			CASE GMS_INIT_POST_STREAMING
				IF bDebugSpew DEBUG_MESSAGE("GMS_INIT_POST_STREAMING") ENDIF
				GOLF_MANAGE_INTRO_FLYOVER(currentGame, golfState, currentPlayerFoursome, currentHelpers)
				IF GOLF_MANAGE_QUIT_INPUT(currentPlayerFoursome, golfState, currentHelpers)
					prevState = golfState
					golfState = GMS_EXIT_OK
				ELIF currentHelpers.introState >= GIS_INTRO_FLYOVER_CAMERA2
				//SET_SCENE_STREAMING(FALSE)
					GOLF_CLEAR_AREA_OF_PEDS_FOR_INTRO(currentCourse)
					
					GOLF_SETUP_PLAYERS(currentGame, currentPlayers, currentPlayerFoursome, currentHelpers)
					GOLF_SETUP_CLUBS (currentGame.golfBag)
					DEBUG_MESSAGE("GMS_INIT_PLACE - screen faded, moving player")
					#IF IS_DEBUG_BUILD
					vPos = V2STR(GET_GOLF_COURSE_START_POSITION(currentCourse,0))
					#ENDIF
					DEBUG_MESSAGE("CLEAR PLAYER TASK FOR TELEPORT")
					CLEAR_PED_TASKS(GET_GOLF_PLAYER_PED(currentPlayerFoursome.playerCore[0]))
					DEBUG_MESSAGE("Moving player to ", vPos)
					SET_GOLF_COURSE_PLAYER_HOLE(0)
					SET_ENTITY_COORDS(GET_GOLF_PLAYER_PED(currentPlayerFoursome.playerCore[0]), GET_GOLF_COURSE_START_POSITION(currentCourse,0), FALSE, FALSE)
					SET_ENTITY_HEADING(GET_GOLF_PLAYER_PED(currentPlayerFoursome.playerCore[0]), GET_HEADING_BETWEEN_VECTORS(GET_GOLF_COURSE_START_POSITION(currentCourse,0), GET_GOLF_COURSE_CART_POSITION( currentCourse, 0)))
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					START_AUDIO_SCENE("GOLF_TRANQUIL")
					SET_PLAYER_GOLF_OUTFIT(currentHelpers)
					SET_GOLF_TRAIL_STATIC_DATA(currentHelpers)
					currentHelpers.eLastPuttStyle = SWING_STYLE_GREEN
					//SET_GOLF_VOICE_NAME(currentPlayerFoursome)
					
					CREATE_GOLF_PLAYER_GOLF_CARTS(currentPlayerFoursome, currentCourse)
					
					CREATE_GOLF_BUDDIES_PEDS(currentPlayerFoursome, currentHelpers, currentCourse)
					//REGISTER_HEAD_TEXTURE(currentPlayerFoursome, currentHelpers)
					
					SET_GOLF_FOURSOME_CART_DRIVER(currentPlayerFoursome, 0,0)
					SET_GOLF_FOURSOME_CART_DRIVER(currentPlayerFoursome, 2,1)
					REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(currentPlayerFoursome) iCurrentPlayer
						IF iCurrentPlayer < 2
							SET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(currentPlayerFoursome, iCurrentPlayer, 0)
						ELSE
							SET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(currentPlayerFoursome, iCurrentPlayer, 1)
						ENDIF
						
						IF IS_GOLF_CONTROL_FLAG_SET(currentGame, GCF_IS_FRIEND_ACTIVITY) AND iCurrentPlayer > 0
							IF DOES_ENTITY_EXIST(GET_GOLF_PLAYER_PED(currentPlayerFoursome.playerCore[iCurrentPlayer]))
								SET_ENTITY_COORDS(GET_GOLF_PLAYER_PED(currentPlayerFoursome.playerCore[iCurrentPlayer]), GET_GOLF_COURSE_START_POSITION(currentCourse, iCurrentPlayer), FALSE)
								SETUP_GOLF_PED_ANCILLARY_DATA(GET_GOLF_PLAYER_PED(currentPlayerFoursome.playerCore[iCurrentPlayer]))
							ENDIF
						ENDIF
												
						IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CART(currentPlayerFoursome, GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(currentPlayerFoursome, iCurrentPlayer)))
						AND NOT IS_ENTITY_DEAD(GET_GOLF_FOURSOME_CART(currentPlayerFoursome, GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(currentPlayerFoursome, iCurrentPlayer)))
						AND DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, iCurrentPlayer)) AND NOT IS_ENTITY_DEAD(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, iCurrentPlayer))
							IF iCurrentPlayer > 1
								SET_PED_INTO_VEHICLE(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, iCurrentPlayer), GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(currentPlayerFoursome, iCurrentPlayer), GET_GOLF_FOURSOME_INDEXED_PLAYER_SEAT(iCurrentPlayer))
							ELIF iCurrentPlayer = 1
								SET_ENTITY_COORDS(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, iCurrentPlayer), <<-1326.9670, 62.4055, 52.5295>>)
								SET_ENTITY_HEADING(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, iCurrentPlayer), 170.3523)
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF NOT DOES_PLAYER_OWN_GOLF_HOUSE()
						GOLF_SPEND_CASH(GET_PRICE_OF_GOLF_GAME(IS_GOLF_CONTROL_FLAG_SET(currentGame, GCF_IS_FRIEND_ACTIVITY)))
					ENDIF
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					
					IF IS_GOLF_CONTROL_FLAG_SET(currentGame, GCF_IS_FRIEND_ACTIVITY)
						IF DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
							IF NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())
								REMOVE_PED_FROM_GROUP(FRIEND_A_PED_ID())
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(FRIEND_B_PED_ID())
							IF NOT IS_ENTITY_DEAD(FRIEND_B_PED_ID())
								REMOVE_PED_FROM_GROUP(FRIEND_B_PED_ID())
							ENDIF
						ENDIF
					ENDIF
					
					REMOVE_AMBIENT_GOLF_FLAGS()
					REPEAT GET_GOLF_COURSE_NUM_HOLES(currentCourse) iCurrentPlayer
						CREATE_GOLF_FLAG(currentCourse, iCurrentPlayer)
					ENDREPEAT
					golfState = GMS_INIT_DONE
					SET_CURRENT_PED_WEAPON(GET_GOLF_PLAYER_PED(currentPlayerFoursome.playerCore[0]), WEAPONTYPE_UNARMED, TRUE)
				ENDIF
			BREAK
			CASE GMS_INIT_DONE
				IF bDebugSpew DEBUG_MESSAGE("GMS_INIT_DONE") ENDIF
				CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_AI_HIT_BALL_THIS_FRAME)
				GOLF_MANAGE_INTRO_FLYOVER(currentGame, golfState, currentPlayerFoursome, currentHelpers)
				
				IF NOT bIsHeadTexturedRegistered
					REGISTER_HEAD_TEXTURE(currentPlayerFoursome)
					bIsHeadTexturedRegistered = TRUE
				ENDIF
				
				IF currentHelpers.introState >= GIS_INTRO_FLYOVER_CLEANUP
					GOLF_INIT_PLAYER_CARD(currentPlayerFoursome, currentHelpers, currentCourse, currentPlayers)
					GOLF_MANAGE_INTRO_FLYOVER(currentGame, golfState, currentPlayerFoursome, currentHelpers) // To make sure the cleanup happens
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					GOLF_INIT_CURRENT_HOLE(currentCourse, currentPlayerFoursome)
					iCurrentPlayer = 0
					
					SET_GOLF_FOURSOME_STATE(currentPlayerFoursome, GGS_NAVIGATE_TO_SHOT)
					SET_GOLF_UI_FLAG(currentHelpers, GUC_REFRESH_NAVIGATE | GUC_REFRESH_PERMANENT)
					IF IS_RADAR_HIDDEN()
						DISPLAY_RADAR(TRUE)
					ENDIF
					DISPLAY_AREA_NAME(FALSE)
					SET_GOLF_FOURSOME_RELATIONSHIP_HASH(currentPlayerFoursome, currentHelpers) //so buddies wont car jack another buddy
					
					TRIGGER_MUSIC_EVENT("MGGF_START")
					REMOVE_WAYPOINT_RECORDING("golf_intro")
					START_TIMER_AT(currentHelpers.idleSpeechTimer, GOLF_PARTNER_SPEAK_DELAY - 5.0)
					GRAB_WIND_FOR_HOLE(currentGame, currentPlayerFoursome, ciGOLF_OPTION_SUNNY)
					golfState = GMS_PLAY_GOLF
					
					currentHelpers.eShotDisplayState = SHOT_DISPLAY_LIE | SHOT_DISPLAY_WIND | SHOT_DISPLAY_CLUB | SHOT_DISPLAY_SWING | SHOT_DISPLAY_SHOT_NUM
					UPDATE_SHOT_DISPLAY(currentHelpers, currentPlayerFoursome, currentGame, currentPlayerFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(currentPlayerFoursome)], currentPlayerFoursome.swingMeter)
					ADD_GOLF_FOURSOME_TO_GROUP(currentPlayerFoursome)
					REMOVE_ANIM_DICT("mini@golfclubhouse")
					SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("ShopUI_Title_GolfShop")
				ELSE
					IF currentHelpers.introState >= GIS_INTRO_FLYOVER_CAMERA3
						GOLF_MANAGE_NAVIGATE_STARTUP_AI(currentPlayerFoursome)
					ENDIF
				ENDIF
				
			BREAK
			CASE GMS_PLAY_GOLF
				IF bDebugSpew DEBUG_MESSAGE("GMS_PLAY_GOLF") ENDIF
				IF GOLF_MANAGE_QUIT_INPUT(currentPlayerFoursome, golfState, currentHelpers)
					prevState = golfState
					golfState = GMS_EXIT_OK
				ELSE
					
					//returns true if a buddy is killed
					IF UPDATE_GOLF_BUDDIES_AI(currentPlayerFoursome)
					OR IS_PED_SHOOTING(PLAYER_PED_ID())
					OR HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
						PRINTLN("Failed because of violence")
						IF GET_GOLF_FOURSOME_STATE(currentPlayerFoursome) = GGS_TAKING_SHOT
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_LOCAL_PLAYER_CLUB(currentPlayerFoursome))
							AND IS_ENTITY_ATTACHED(GET_GOLF_FOURSOME_LOCAL_PLAYER_CLUB(currentPlayerFoursome))
								DETACH_ENTITY(GET_GOLF_FOURSOME_LOCAL_PLAYER_CLUB(currentPlayerFoursome))
							ENDIF
							GOLF_END_CUTSCENE_TO_FIRST_PERSON_CAMERA_EFFECT()
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							SET_PUTTMETER(currentHelpers, currentHelpers.golfUI)
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							DISABLE_GOLF_MINIMAP()
							CLEAR_GOLF_UI_DISPLAY(currentHelpers, GOLF_DISPLAY_HOLE)
							SET_GOLF_FOURSOME_STATE(currentPlayerFoursome, GGS_NAVIGATE_TO_SHOT)
						ENDIF
						
						CLEAR_HELP()
						CLEANUP_GOLF_HELPER_ALL_BALL_BLIPS(currentHelpers)
						CLEANUP_GOLF_HELPER_HOLE_BLIP(currentHelpers)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						TASK_GOLF_FOURSOME_FLEE(currentPlayerFoursome, currentGame)
						
						SET_GOLF_HELP_DISPLAYED_FLAG(currentHelpers, GHD_GOLF_TERMINATED_DISPLAYED)
						
						IF IS_GOLF_CONTROL_FLAG_SET(currentGame, GCF_IS_FRIEND_ACTIVITY)
							GOLF_PRINT_GOD_TEXT("QUIT_ATTACK")
							
							RESET_FRIEND_FOR_CLEANUP(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, 1))
							RESET_FRIEND_FOR_CLEANUP(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, 2))
							
							golfState = GMS_CLEANUP
						ELSE
							
							currentHelpers.sFailReason = "QUIT_ATTACK"
							MG_INIT_FAIL_SPLASH_SCREEN(splashFail)
							golfState = GMS_FAIL
						ENDIF
						
					ELSE
						GOLF_MANAGE_PRE_UPDATE_UI(currentGame, currentHelpers)
						GOLF_MANAGE_CART_OWNER(currentPlayerFoursome)
						golfState = GOLF_PLAY_GOLF(currentGame, currentCourse, currentPlayerFoursome, currentPlayers, currentHelpers)
						IF IS_GOLF_CONTROL_FLAG_SET(currentGame, GCF_PLAYER_DONE_WITH_HOLE)
							CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_PLAYER_DONE_WITH_HOLE)
						ENDIF
						GOLF_MANAGE_PERMANENT_UI(currentGame, currentCourse, currentPlayerFoursome, currentHelpers)
						
	//					DRAW_GOLF_ESTIMATE_RING(GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(currentPlayerFoursome), GET_GOLF_HOLE_PIN_POSITION(currentCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(currentPlayerFoursome)))
						
						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
						AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
							currentHelpers.bJustExitQuit= FALSE
						ELIF currentHelpers.bJustExitQuit
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
						ENDIF
						
						GOLF_MANAGE_SCOREBOARD_UI(currentHelpers, currentPlayerFoursome, currentCourse, golfState)
					ENDIF
				ENDIF
			BREAK
			
			CASE GMS_REMATCH
				PRINTLN("GMS_REMATCH")
				
				ANIMPOSTFX_STOP("MinigameTransitionIn")
				ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
				RENDER_SCRIPT_CAMS(FALSE, TRUE)
				
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iGolfBagSyncScene, FALSE)
				
				IF IS_ENTITY_OK(GET_GOLF_FOURSOME_INDEXED_PLAYER_GOLFBAG(currentPlayerFoursome, 0) )  
				AND IS_ENTITY_OK(GET_GOLF_FOURSOME_CART(currentPlayerFoursome, GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(currentPlayerFoursome, 0)))
					// url:bugstar:2105756
					STOP_SYNCHRONIZED_ENTITY_ANIM(GET_GOLF_FOURSOME_INDEXED_PLAYER_GOLFBAG(currentPlayerFoursome, 0), INSTANT_BLEND_OUT, FALSE)
					
					ATTACH_ENTITY_TO_ENTITY( GET_GOLF_FOURSOME_INDEXED_PLAYER_GOLFBAG(currentPlayerFoursome, 0), GET_GOLF_FOURSOME_CART(currentPlayerFoursome, GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(currentPlayerFoursome, 0)), 0, <<-0.225 - (-0.444), -1.111, 0.570>>, <<0,0,-87.000 + 180.0>>)
					SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CART(currentPlayerFoursome, GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(currentPlayerFoursome, 0)), <<-1322.3052, 29.3257, 52.5880>>)
				ENDIF
				
				RESET_EVERYTHING_FOR_REMATCH_SP(currentGame, currentCourse, currentPlayerFoursome, currentPlayers, currentHelpers)
				
				SETUP_PC_GOLF_CONTROLS() // Re-set these up as we're turning off the golf controls during the leaderboard to prevent conflicts.
				
				golfState = GMS_PLAY_GOLF
			BREAK
			
			CASE GMS_FAIL
				PRINTLN("GMS_FAIL")
				scaleformFail.siMovie = siGolfSplash[0]
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				
				BOOL bShouldRetry 
				bShouldRetry = FALSE
				IF MG_UPDATE_FAIL_SPLASH_SCREEN(scaleformFail, splashFail, "FAILED_GOLF", currentHelpers.sFailReason, bShouldRetry)
					IF bShouldRetry
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
						DO_SCREEN_FADE_OUT(500)
						PLAY_SOUND_FRONTEND(-1,"YES", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						golfState = GMS_RESET
					ELSE
						PLAY_SOUND_FRONTEND(-1,"NO", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						golfState = GMS_CLEANUP
					ENDIF
				ENDIF
			BREAK
			
			CASE GMS_RESET
				PRINTLN("GMS_RESET")
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_TIMER_STARTED(currentHelpers.quitTimer)
						REQUEST_ANIM_DICT("mini@golfclubhouse")
						
						IF HAS_ANIM_DICT_LOADED("mini@golfclubhouse")
							
							REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(currentPlayerFoursome) iCurrentPlayer
								PED_INDEX resetPed 
								resetPed = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, iCurrentPlayer)
								IF DOES_ENTITY_EXIST(resetPed) AND resetPed != PLAYER_PED_ID()
									DELETE_PED(resetPed)
								ENDIF
							ENDREPEAT
							
							CREATE_GOLF_BUDDIES_PEDS(currentPlayerFoursome, currentHelpers, currentCourse)
							CREATE_GOLF_PLAYER_GOLF_CARTS(currentPlayerFoursome, currentCourse)
							
							REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(currentPlayerFoursome) iCurrentPlayer
								PED_INDEX resetPed 
								resetPed = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, iCurrentPlayer)
								IF DOES_ENTITY_EXIST(resetPed)
									VEHICLE_INDEX golfersCart 
									golfersCart = GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(currentPlayerFoursome, iCurrentPlayer)
									
									IF iCurrentPlayer > 1
										IF DOES_ENTITY_EXIST(golfersCart) AND NOT IS_ENTITY_DEAD(golfersCart)
											SET_PED_INTO_VEHICLE(resetPed, golfersCart, GET_GOLF_FOURSOME_INDEXED_PLAYER_SEAT(iCurrentPlayer))
										ENDIF
									ELIF iCurrentPlayer = 1
										SET_ENTITY_COORDS(resetPed, <<-1326.9670, 62.4055, 52.5295>>)
										SET_ENTITY_HEADING(resetPed, 170.3523)
									ENDIF
									
									FORCE_PED_AI_AND_ANIMATION_UPDATE(resetPed)
									SET_ENTITY_HEALTH(resetPed, GET_ENTITY_MAX_HEALTH(resetPed))
								ENDIF
							ENDREPEAT
							
							SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
							CLEAR_AREA_OF_COPS(GET_GOLF_COURSE_CENTER(currentCourse), 400)
							CLEAR_AREA_OF_PEDS(GET_GOLF_COURSE_CENTER(currentCourse), 400)
							CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
							SET_CURRENT_PED_WEAPON(GET_GOLF_PLAYER_PED(currentPlayerFoursome.playerCore[0]), WEAPONTYPE_UNARMED, TRUE)
							CLEANUP_COMPLEX_USE_CONTEXT(currentHelpers.useContext)
							REMOVE_GOLF_FOURSOME_FROM_GROUP(currentPlayerFoursome)
							
							iGolfResetSceneID = CREATE_SYNCHRONIZED_SCENE(<<-1337.0842, 55.5505, 58.957>>, <<0,0,0>>)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iGolfResetSceneID, "mini@golfclubhouse", "clubhouse_exit_plyr", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iGolfResetSceneID, FALSE)
							
							SET_BITMASK_ENUM_AS_ENUM(g_AmbientAreaData[eAMBAREA_GOLF].ambAreaFlags, eAmbientArea_ForceDeactivate)
							SET_BITMASK_ENUM_AS_ENUM(g_AmbientAreaData[eAMBAREA_PUTTING].ambAreaFlags, eAmbientArea_ForceDeactivate)
							
							RESET_EVERYTHING_FOR_REMATCH_SP(currentGame, currentCourse, currentPlayerFoursome, currentPlayers, currentHelpers)
							
							RESTART_TIMER_NOW(currentHelpers.quitTimer)
						ENDIF
					ELIF TIMER_DO_WHEN_READY(currentHelpers.quitTimer, 2.0)
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					GOLF_CLEAR_AREA_OF_PEDS_FOR_INTRO(currentCourse)
				ENDIF
				
				IF IS_SCREEN_FADED_IN()
					PRINTLN("Going back to golf after reseting")
					CANCEL_TIMER(currentHelpers.quitTimer) 
					GOLF_CLEAR_AREA_OF_PEDS_FOR_INTRO(currentCourse)
					REMOVE_ANIM_DICT("mini@golfclubhouse")
					golfState = GMS_PLAY_GOLF
				ENDIF
			
			BREAK
			
			CASE GMS_EXIT_OK
				// SET WHAT SET UP HERE TO HAVE SETUP THE QUIT MENU -- DONE.
				SET_WARNING_MESSAGE_WITH_HEADER("QUIT", "QUIT_DET", FE_WARNING_YES | FE_WARNING_NO)
				//KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				IF IS_MESSAGE_BEING_DISPLAYED()
					CLEAR_PRINTS()
				ENDIF
				IF NOT IS_RADAR_HIDDEN()
					DISPLAY_RADAR(FALSE)
				ENDIF
				
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					bPlayerControlWasActiveBeforePause = TRUE
				ENDIF
				
				GOLF_TRAIL_SET_ENABLED(FALSE)
				
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					currentHelpers.eCurrentGolfUIDisplay = GOLF_DISPLAY_NONE
					SET_PUTTMETER(currentHelpers, currentHelpers.golfUI)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE) // Just in case player has been hidden

					IF IS_THIS_MODEL_A_CAR(currentHelpers.vehicleName) OR IS_THIS_MODEL_A_BIKE(currentHelpers.vehicleName)
						IF NOT DOES_ENTITY_EXIST(currentHelpers.playerLastVehicle)
							currentHelpers.playerLastVehicle = CREATE_VEHICLE(currentHelpers.vehicleName,<<-1389.4039, 48.9430, 52.6259>>, 308.7762)
							IF currentHelpers.iVehicleColor > -1
								SET_VEHICLE_COLOUR_COMBINATION(currentHelpers.playerLastVehicle, currentHelpers.iVehicleColor)
							ENDIF
							
							PRINTLN("End of golf car model ", currentHelpers.vehicleName, " car colour ", currentHelpers.iVehicleColor)
							SET_VEHICLE_SETUP(currentHelpers.playerLastVehicle, currentHelpers.sPlayerVehicle)
						ELIF NOT IS_ENTITY_DEAD(currentHelpers.playerLastVehicle)
							SET_ENTITY_COORDS(currentHelpers.playerLastVehicle,<<-1389.4039, 48.9430, 52.6259>>)
							SET_ENTITY_HEADING(currentHelpers.playerLastVehicle, 308.7762 )
						ENDIF
						
						SET_VEHICLE_DOORS_LOCKED(currentHelpers.playerLastVehicle, VEHICLELOCK_UNLOCKED)
					ENDIF
					IF currentHelpers.introState =GIS_INTRO_FLYOVER_OVER
						REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(currentPlayerFoursome) iCurrentPlayer
							CLEANUP_GOLF_CLUB(currentPlayerFoursome.playerCore[iCurrentPlayer])
						ENDREPEAT
						RESTART_TIMER_AT(currentHelpers.navTimer, 3.5)
						CANCEL_TIMER(currentHelpers.resetTimer)
						golfState = GMS_OUTRO_QUIT
					ELSE
						golfState = GMS_CLEANUP
					ENDIF
				ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					currentHelpers.bJustExitQuit = TRUE
					golfState = prevState
					IF prevState = GMS_SCOREBOARD_OVERRIDE
						golfState = GMS_PLAY_GOLF
					ENDIF
					
					IF bPlayerControlWasActiveBeforePause
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						bPlayerControlWasActiveBeforePause = FALSE
					ENDIF
					
					IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(currentPlayerFoursome)
						SET_GOLF_UI_FLAG(currentHelpers, GUC_REFRESH_GOLF_TRAIL)
					ENDIF
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
					DISPLAY_RADAR(TRUE)
				ENDIF
			BREAK
			
			CASE GMS_SCOREBOARD_OVERRIDE
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				IF NOT IS_RADAR_HIDDEN()
					DISPLAY_RADAR(FALSE)
				ENDIF
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
				CLEAR_GOLF_UI_FLAG(currentHelpers, GUC_STYLE_HELP_DISPLAYED)
				IF IS_TIMER_STARTED(currentHelpers.uiTimer)
					PAUSE_TIMER(currentHelpers.uiTimer)
				ENDIF
				GOLF_MANAGE_SCOREBOARD_UI(currentHelpers, currentPlayerFoursome, currentCourse, golfState)
				GOLF_DISPLAY_CONTROLS(currentHelpers, TRUE)
			BREAK
			
			CASE GMS_CLEANUP
				IF bDebugSpew DEBUG_MESSAGE("GMS_CLEANUP") ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				IF currentHelpers.introState  != GIS_INTRO_FLYOVER_OVER
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1378.6844, 55.8883, 52.6896>>, FALSE)
				ENDIF
				
				IF bFlagPassed
					SCRIPT_PASSED(currentGame, currentCourse, currentPlayerFoursome, currentPlayers, currentHelpers)
				ELSE
					SCRIPT_FAILED(currentGame, currentCourse, currentPlayerFoursome, currentPlayers, currentHelpers)
				ENDIF
			BREAK
			CASE GMS_OUTRO_QUIT
				
				IF DO_GOLF_PAN_UP_CAM(currentHelpers)
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
						DISPLAY_SCOREBOARD(currentHelpers, FALSE)
						
						IF STREAMVOL_IS_VALID(currentHelpers.steamVolNextShot)
							STREAMVOL_DELETE(currentHelpers.steamVolNextShot)
						ENDIF
						
						golfState = GMS_OUTRO_INIT
					ELSE
						INIT_COMPLEX_USE_CONTEXT(currentHelpers.useContext, "GOLF_CONTINUE", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), FALSE)
						DISPLAY_SCOREBOARD(currentHelpers, TRUE)
						GOLF_DISPLAY_CONTROLS(currentHelpers, TRUE)
					ENDIF
				ENDIF
				
			BREAK
			CASE GMS_OUTRO_INIT
				
				IF NOT STREAMVOL_IS_VALID(currentHelpers.steamVolNextShot)
					RESET_GOLF_PLAYER_OUTFIT(currentHelpers)
					IF IS_GOLF_CONTROL_FLAG_SET(currentGame, GCF_IS_FRIEND_ACTIVITY)
						RESET_FRIEND_FOR_GOLF_OUTRO(currentPlayerFoursome, currentHelpers.sFriendVariation,  1, <<-1372.9261, 60.8806, 52.7017>>, 259.8690)
						RESET_FRIEND_FOR_GOLF_OUTRO(currentPlayerFoursome, currentHelpers.sFriendVariationB, 2, <<-1373.0626, 53.0383, 52.7020>>, 310.3635 ) 
					ENDIF
					currentHelpers.steamVolNextShot = STREAMVOL_CREATE_SPHERE(<< -1370.3688, 55.8800, 52.7057 >>, 10.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
				ENDIF
				
				IF STREAMVOL_HAS_LOADED(currentHelpers.steamVolNextShot)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())

					DESTROY_ALL_CAMS()
					RESTART_TIMER_NOW(currentHelpers.navTimer)
					CANCEL_TIMER(currentHelpers.resetTimer)
					CLEAR_HELP()
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					
					CLEAR_AREA_OF_PEDS(<< -1370.3688, 55.8800, 52.7057 >>, 10.0)
					CLEAR_AREA_OF_OBJECTS(<< -1370.3688, 55.8800, 52.7057 >>, 10.0)
					
					IF IS_THIS_MODEL_A_CAR(currentHelpers.vehicleName) OR IS_THIS_MODEL_A_BIKE(currentHelpers.vehicleName)
						IF NOT DOES_ENTITY_EXIST(currentHelpers.playerLastVehicle)
							currentHelpers.playerLastVehicle = CREATE_VEHICLE(currentHelpers.vehicleName,<<-1389.4039, 48.9430, 52.6259>>, 308.7762)
							IF currentHelpers.iVehicleColor > -1
								SET_VEHICLE_COLOUR_COMBINATION(currentHelpers.playerLastVehicle, currentHelpers.iVehicleColor)
							ENDIF
							PRINTLN("End of golf car model ", currentHelpers.vehicleName, " car colour ", currentHelpers.iVehicleColor)
							SET_VEHICLE_SETUP(currentHelpers.playerLastVehicle, currentHelpers.sPlayerVehicle)
						ELIF NOT IS_ENTITY_DEAD(currentHelpers.playerLastVehicle)
							SET_ENTITY_COORDS(currentHelpers.playerLastVehicle,<<-1389.4039, 48.9430, 52.6259>>)
							SET_ENTITY_HEADING(currentHelpers.playerLastVehicle, 308.7762 )
						ENDIF
					ENDIF
					

					
					CLEAR_FOCUS()
					REMOVE_GOLF_CLUB_WEAPON(PLAYER_PED_ID(), TRUE)
					
					DISABLE_GOLF_MINIMAP()
					SET_GOLF_UI_FLAG(currentHelpers, GUC_DISABLE_UI) //stops the golf ui from drawing
					DO_END_OF_GOLF_GAME_CUTSCENE(currentHelpers)
					
					golfState = GMS_OUTRO
				ENDIF
			BREAK
			
			CASE GMS_OUTRO
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				IF DO_END_OF_GOLF_GAME_CUTSCENE(currentHelpers)
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(-15.7166)
//					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-4.9961)
					
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
					currentHelpers.bEndResetScriptCam = FALSE
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), TRUE)
					CLEANUP_GOLF_CART(currentPlayerFoursome, TRUE)
					golfState = GMS_CLEANUP
				ENDIF
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("golfState should never hit case default - please contact Alan Blaine with repro information")
			BREAK
		ENDSWITCH
		
		MANAGE_GOLF_UI_DISPLAY(currentHelpers)
		
		IF NOT IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_DISABLE_UI)
			IF golfState != GMS_EXIT_OK AND golfState > GMS_INIT_DONE
			OR golfState = GMS_SCOREBOARD_OVERRIDE
				
				IF SHOULD_GOLF_SC_LEADERBOARD_DSIPLAY(currentPlayerFoursome, currentHelpers)
					IF GOLF_CAN_DISPLAY_SOCAIL_CLUB_LEADERBOARD()
						GOLF_DISPLAY_SOCIAL_CLUB_LEADERBOARD(currentHelpers.leaderboardUI)
						MANAGE_GOLF_SC_LEADERBOARD_INPUT(currentPlayerFoursome, currentHelpers)
					ELSE
						SET_GOLF_UI_FLAG(currentHelpers, GUC_SOCIAL_CLUB_BOARD_DISPLAYED)
						IF DO_SIGNED_OUT_WARNING(iSignedOutWarningBits)
							CLEAR_GOLF_UI_FLAG(currentHelpers, GUC_SOCIAL_CLUB_BOARD_DISPLAYED)
							GOLF_RESET_SOCIAL_CLUB_KEYS(currentHelpers)
							iSignedOutWarningBits = 0
						ENDIF
					ENDIF
				ELSE
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(currentHelpers.golfUI,255,255,255,0)
				ENDIF
				
				UPDATE_GOLF_SPLASH()
			ENDIF
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
ENDSCRIPT
