USING "net_celebration_screen.sch"

SCALEFORM_INDEX siGolfSplash[2]
INT iGolfSplashDuration
INT iGolfEndSplashDuration
INT iCurrentGolfScaleformIndex
INT iGolfSplashFlags
HUD_COLOURS eGolfSplashEndColour
INT iGolfEndScreenWaitTime
INT iGolfSkycamTime
BOOL bRepositionedCamForSwoop

CELEB_SERVER_DATA sCelebServer

STRUCT GOLF_XP_TRACKER

	INT iStartRP
	INT iStartLvl
	INT iNextLvl
	INT iRPToReachCurrentLvl
	INT iRPToReachNextLvl

ENDSTRUCT

ENUM GOLF_SPLASH_TYPE

	GOLF_SPLASH_DEFAULT,
	GOLF_SPLASH_CENTER,
	GOLF_SPLASH_SHARD_CENTER,
	GOLF_SPLASH_MIDSIZED,
	GOLF_SPLASH_SHARD_MIDSIZED

ENDENUM

ENUM GOLF_SPLASH_FLAGS
	
	GOLF_SPLASH_BIT_PLAY_SHARD_END_ANIM = BIT1

ENDENUM


PROC REQUEST_GOLF_SPLASH_SCALEFORM()
	siGolfSplash[0] = REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
	siGolfSplash[1] = REQUEST_SCALEFORM_MOVIE("MIDSIZED_MESSAGE")
ENDPROC

PROC CLEAN_UP_GOLF_SPLASH()
	INT index
	
	REPEAT COUNT_OF(siGolfSplash) index
		IF HAS_SCALEFORM_MOVIE_LOADED(siGolfSplash[index])
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(siGolfSplash[index])
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_GOLF_SPLASH_LOADED()
	RETURN HAS_SCALEFORM_MOVIE_LOADED(siGolfSplash[0]) AND HAS_SCALEFORM_MOVIE_LOADED(siGolfSplash[1])
ENDFUNC

PROC GET_GOLF_SPLASH_METHOD_NAME_AND_INDEX(GOLF_SPLASH_TYPE splashType, TEXT_LABEL_31 &sMethodName, INT &iSplashIndex)
	
	//In single player do not use the shard big messages
//	#IF NOT GOLF_IS_MP
//		IF splashType = GOLF_SPLASH_SHARD_MIDSIZED
//			splashType = GOLF_SPLASH_MIDSIZED
//		ENDIF
//	#ENDIF
	
	iSplashIndex = 0
	
	IF splashType = GOLF_SPLASH_CENTER
		sMethodName = "SHOW_CENTERED_MP_MESSAGE"
	ELIF splashType = GOLF_SPLASH_SHARD_CENTER
		sMethodName = "SHOW_SHARD_CENTERED_MP_MESSAGE"
		SET_BITMASK_AS_ENUM(iGolfSplashFlags, GOLF_SPLASH_BIT_PLAY_SHARD_END_ANIM)
	ELIF splashType = GOLF_SPLASH_MIDSIZED
		sMethodName = "SHOW_MIDSIZED_MESSAGE"
		iSplashIndex = 1
	ELIF splashType = GOLF_SPLASH_SHARD_MIDSIZED
		IF NETWORK_IS_GAME_IN_PROGRESS()
			sMethodName = "SHOW_COND_SHARD_MESSAGE"
		ELSE
			sMethodName = "SHOW_SHARD_MIDSIZED_MESSAGE"
		ENDIF
		iSplashIndex = 1
		SET_BITMASK_AS_ENUM(iGolfSplashFlags, GOLF_SPLASH_BIT_PLAY_SHARD_END_ANIM)
	ELSE
		sMethodName = "SHOW_MISSION_PASSED_MESSAGE"
	ENDIF
	
ENDPROC

PROC MANAGE_GOLF_MP_SPLASH_END_COLOUR(HUD_COLOURS &eHudColor, BOOL bOverwriteHudColour = TRUE)
	
	IF eHudColor = HUD_COLOUR_YELLOW
		eGolfSplashEndColour =  HUD_COLOUR_WHITE
	ELIF eHudColor = HUD_COLOUR_RED
		eGolfSplashEndColour =  HUD_COLOUR_RED
	ELSE
		eGolfSplashEndColour =  HUD_COLOUR_BLACK
	ENDIF
	
	IF bOverwriteHudColour
		eHudColor = HUD_COLOUR_WHITE
	ENDIF
ENDPROC

PROC PLAY_GOLF_SPLASH(STRING sSplashLabel, STRING strapLine = NULL, GOLF_SPLASH_TYPE splashType = GOLF_SPLASH_DEFAULT, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW, INT iDuration = 4000, BOOL bAnimated = FALSE, HUD_COLOURS eEndHudColour = -1)
	TEXT_LABEL_31 sMethodName
	GET_GOLF_SPLASH_METHOD_NAME_AND_INDEX(splashType, sMethodName, iCurrentGolfScaleformIndex)
	MANAGE_GOLF_MP_SPLASH_END_COLOUR(eHudColor, splashType != GOLF_SPLASH_CENTER)
	IF eEndHudColour != INT_TO_ENUM(HUD_COLOURS, -1)
		eGolfSplashEndColour = eEndHudColour
	ENDIF
	
	iGolfSplashDuration = GET_GAME_TIMER() + iDuration
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siGolfSplash[iCurrentGolfScaleformIndex], sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sSplashLabel)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		IF NOT IS_STRING_NULL_OR_EMPTY(strapline)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strapline)
		ENDIF
		IF bAnimated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	IF bAnimated
		BEGIN_SCALEFORM_MOVIE_METHOD(siGolfSplash[iCurrentGolfScaleformIndex], "TRANSITION_UP")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.15)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


PROC PLAY_GOLF_SPLASH_WITH_INT(STRING sSplashLabel, INT iNum, STRING strapLine = NULL, GOLF_SPLASH_TYPE splashType = GOLF_SPLASH_DEFAULT, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW, INT iDuration = 4000, BOOL bAnimated = FALSE)
	TEXT_LABEL_31 sMethodName
	GET_GOLF_SPLASH_METHOD_NAME_AND_INDEX(splashType, sMethodName, iCurrentGolfScaleformIndex)
	MANAGE_GOLF_MP_SPLASH_END_COLOUR(eHudColor, splashType != GOLF_SPLASH_CENTER)
	
	iGolfSplashDuration = GET_GAME_TIMER() + iDuration
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siGolfSplash[iCurrentGolfScaleformIndex], sMethodName)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sSplashLabel)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_INTEGER(iNum)
		END_TEXT_COMMAND_SCALEFORM_STRING()
			
		IF NOT IS_STRING_NULL_OR_EMPTY(strapline)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strapline)
		ENDIF
		IF bAnimated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC PLAY_GOLF_SPLASH_WITH_INT_IN_STRAP(STRING sSplashLabel, INT iNum, STRING strapLine, GOLF_SPLASH_TYPE splashType = GOLF_SPLASH_DEFAULT, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW, INT iDuration = 4000)
	TEXT_LABEL_31 sMethodName
	GET_GOLF_SPLASH_METHOD_NAME_AND_INDEX(splashType, sMethodName, iCurrentGolfScaleformIndex)
	MANAGE_GOLF_MP_SPLASH_END_COLOUR(eHudColor, splashType != GOLF_SPLASH_CENTER)
	
	iGolfSplashDuration = GET_GAME_TIMER() + iDuration
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siGolfSplash[iCurrentGolfScaleformIndex], sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sSplashLabel)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapLine)
			ADD_TEXT_COMPONENT_INTEGER(iNum)
		END_TEXT_COMMAND_SCALEFORM_STRING()

	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC PLAY_GOLF_SPLASH_WITH_NAME_IN_STRAPLINE(STRING sSplashLabel, STRING strapLine, STRING sPlayerName, GOLF_SPLASH_TYPE splashType = GOLF_SPLASH_DEFAULT, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW)
	TEXT_LABEL_31 sMethodName
	GET_GOLF_SPLASH_METHOD_NAME_AND_INDEX(splashType, sMethodName, iCurrentGolfScaleformIndex)
	MANAGE_GOLF_MP_SPLASH_END_COLOUR(eHudColor, splashType != GOLF_SPLASH_CENTER)
	
	iGolfSplashDuration = GET_GAME_TIMER() + 4000
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siGolfSplash[iCurrentGolfScaleformIndex], sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sSplashLabel)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapLine)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sPlayerName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
			
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC BOOL IS_GOLF_END_SCREEN_READY(CELEBRATION_SCREEN_DATA &golfCelebrationData)
	KILL_UI_FOR_CELEBRATION_SCREEN()
	
	//Make sure we don't load too many scaleform movies at the same time
	CLEAN_UP_GOLF_SPLASH()	
	REQUEST_CELEBRATION_SCREEN(golfCelebrationData)
	
	RETURN HAS_CELEBRATION_SCREEN_LOADED(golfCelebrationData)
ENDFUNC

PROC INIT_GOLF_XP_TRACKER(GOLF_XP_TRACKER &golfXPTracker)

	golfXPTracker.iStartRP = GET_PLAYER_FM_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID()))
	golfXPTracker.iStartLvl = GET_FM_RANK_FROM_XP_VALUE(golfXPTracker.iStartRP)
	golfXPTracker.iNextLvl = golfXPTracker.iStartLvl + 1
	golfXPTracker.iRPToReachCurrentLvl = GET_XP_NEEDED_FOR_FM_RANK(golfXPTracker.iStartLvl)
	golfXPTracker.iRPToReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(golfXPTracker.iNextLvl)

ENDPROC

FUNC BOOL ADD_GOLF_WINNER_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &golfCelebrationData, STRING golfEndScreenWinnerID, PLAYER_INDEX playerIndexWinner,
											 JOB_WIN_STATUS eWinStatus, STRING strBackgroundColour, STRING sWinnerName, STRING sCrewName, INT iBetWinings, INT iAnimToPlay)
	INT i
	
	PLAYER_INDEX playerWinner[8]
	
	
	
	REPEAT COUNT_OF(playerWinner) i
		playerWinner[i] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	playerWinner[0] = playerIndexWinner
	
	IF CREATE_WINNER_SCENE_ENTITIES(sCelebServer, golfCelebrationData, playerWinner, NULL, FALSE, NULL, FALSE, FALSE, FALSE, FALSE, DUMMY_MODEL_FOR_SCRIPT, iAnimToPlay)
		
		PRINTLN("[NETCELEBRATION] - DISPLAY_GOLF_END_SCREEN - ADD_GOLF_WINNER_CELEBRATION_SCREEN - LOAD_WINNER_SCENE_INTERIOR = TRUE.")
		
		CREATE_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenWinnerID, strBackgroundColour)
		golfCelebrationData.iEstimatedScreenDuration = GET_GAME_TIMER() + 3500
		
		ADD_WINNER_TO_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenWinnerID, eWinStatus, sWinnerName, sCrewName,  "", iBetWinings)
		golfCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
		
		//extra time to see betting screen
		CDEBUG1LN(DEBUG_GOLF,"PAssed in bet winnings ", iBetWinings)
		IF ABSI(iBetWinings) > 0
			golfCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
		ENDIF
		
		ADD_BACKGROUND_TO_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenWinnerID)
		SHOW_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenWinnerID)
		
		START_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
		PRINTLN("[WJK] - called START_AUDIO_SCENE(MP_JOB_CHANGE_RADIO_MUTE).")
		
		golfCelebrationData.iWinnerPlayerID = NATIVE_TO_INT(playerIndexWinner)
		
		PRINTLN("[NETCELEBRATION] - DISPLAY_GOLF_END_SCREEN - ADD_GOLF_WINNER_CELEBRATION_SCREEN - returning TRUE.")
		
		RETURN TRUE
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PLAY_GOLF_ENDSCREEN_POST_EFFECT(INT iPlacement, BOOL bIntro)
	TEXT_LABEL_31 txtEffect
	
	IF iPlacement = 1
		IF bIntro
			txtEffect = "MP_Celeb_Win"
		ELSE
			txtEffect = "MP_Celeb_Win_Out"
		ENDIF
	ELSE
		IF bIntro
			txtEffect = "MP_Celeb_Lose"
		ELSE
			txtEffect = "MP_Celeb_Lose_Out"
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(txtEffect)
		IF NOT ANIMPOSTFX_IS_RUNNING(txtEffect)
			ANIMPOSTFX_STOP_ALL()
			
			CDEBUG1LN(DEBUG_GOLF, "Play golf end screen effect ", txtEffect)
			ANIMPOSTFX_PLAY(txtEffect, 0, TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DISPLAY_GOLF_END_SCREEN(CELEBRATION_SCREEN_DATA &golfCelebrationData, CAMERA_INDEX &camEndScreen, GOLF_XP_TRACKER &golfXPTracker, INT iPlacement, BOOL bTied, INT iRPGain,
								  BOOL bCreateWinnerScreen, BOOL bDraw, INT iWinnerNetIndex, INT iBetWinings, BOOL bSpectator, DPAD_VARS &dpadVars)
								  
	CDEBUG1LN(DEBUG_GOLF,"Displaying golf end screen")
	
	HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
	STRING golfEndScreenID = "GOLF_END_SCREEN"
	STRING golfEndScreenWinnerID = "GOLF_END_WINNER_SCREEN"
	STRING strBackgroundColour = "HUD_COLOUR_BLACK"
	BOOL bSuccefullyPlaceCamera = FALSE
	TEXT_LABEL_63 sWinnerName = "" 
	TEXT_LABEL_63 sCrewName = ""
	JOB_WIN_STATUS eWinStatus
	PLAYER_INDEX playerIndexWinner = NULL
	PED_INDEX pedWinner
	TEXT_LABEL_63 tl63_IdleAnimDict, tl63_IdleAnimName
	
	IF IS_MINIGAME_IN_PROGRESS()
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	
	SET_FRONTEND_ACTIVE(FALSE)

	IF iWinnerNetIndex > -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iWinnerNetIndex))
			playerIndexWinner = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iWinnerNetIndex))
			pedWinner = GET_PLAYER_PED(playerIndexWinner)
			sWinnerName = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iWinnerNetIndex)))
			sCrewName = GET_CREW_NAME_FOR_CELEBRATION_SCREEN(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iWinnerNetIndex)))
		ENDIF
	ENDIF
				
	GET_CELEBRATION_IDLE_ANIM_TO_USE(golfCelebrationData, tl63_IdleAnimDict, tl63_IdleAnimName, IS_PED_MALE(pedWinner))				
	REQUEST_ANIM_DICT(tl63_IdleAnimDict)
	
	SWITCH golfCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
			// Setup first celebration screen.
			IF bSpectator
				IF LOAD_WINNER_SCENE_INTERIOR(golfCelebrationData)
					START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					FLASH_CELEBRATION_SCREEN(golfCelebrationData, 0.25, 0.25, 0.25)
					golfCelebrationData.iEstimatedScreenDuration = GET_GAME_TIMER() + 300
					golfCelebrationData.eCurrentStage = CELEBRATION_STAGE_DOING_FLASH
				ENDIF
			ELSE
				// Place camera in front of player.
				IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(golfCelebrationData, camEndScreen, bSuccefullyPlaceCamera)
					
					// Set the celebration cam active. 
					IF bSuccefullyPlaceCamera
						SET_CAM_ACTIVE(camEndScreen, TRUE)
					ENDIF
					
					// Create the celebration movie.
					CREATE_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenID, strBackgroundColour)
					golfCelebrationData.iEstimatedScreenDuration = GET_GAME_TIMER() + 6000
					
					iGolfEndScreenWaitTime = GET_GAME_TIMER() + 1000
					PLAY_GOLF_ENDSCREEN_POST_EFFECT(iPlacement, TRUE)
					
					// Add finish position to screen.
					IF bTied
						ADD_FINISH_POSITION_TO_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenID, iPlacement, "CELEB_YOU_TIED")
					ELSE
						ADD_FINISH_POSITION_TO_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenID, iPlacement)
					ENDIF
					
//					IF iScore > 0
//						ADD_SCORE_TO_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenID, iScore)
//						golfCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
//					ENDIF
					
					// Add RP to celebration screen.
					IF iRPGain > 0
						
						ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenID, iRPGain, golfXPTracker.iStartRP, 
																		  golfXPTracker.iRPToReachCurrentLvl, golfXPTracker.iRPToReachNextLvl, golfXPTracker.iStartLvl, golfXPTracker.iNextLvl)
						golfCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
						
						IF golfXPTracker.iStartRP + iRPGain > golfXPTracker.iRPToReachNextLvl
							golfCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME //time for level up screen
						ENDIF
					ENDIF
					
					// Add background to celebration movie.
					ADD_BACKGROUND_TO_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenID)
					PRINTLN("[NETCELEBRATION] - DISPLAY_GOLF_END_SCREEN - going to stage CELEBRATION_STAGE_TRANSITIONING.")
					
					// Go to next stage.
					golfCelebrationData.eCurrentStage = CELEBRATION_STAGE_TRANSITIONING
					
				ENDIF
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_TRANSITIONING
			
			IF LOAD_WINNER_SCENE_INTERIOR(golfCelebrationData)
			AND HAS_ANIM_DICT_LOADED(tl63_IdleAnimDict)
				
				// Wait 1 second and then move on.
				IF GET_GAME_TIMER() > iGolfEndScreenWaitTime
					
					SHOW_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenID)
						
					START_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
					PRINTLN("[WJK] - called START_AUDIO_SCENE(MP_JOB_CHANGE_RADIO_MUTE).")
					
					STORE_PLAYER_INTERACTION_ANIM_VALUES_FOR_CELEBRATION_SCREEN()
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
					
					PRINTLN("[NETCELEBRATION] - DISPLAY_GOLF_END_SCREEN - GET_GAME_TIMER() > iGolfEndScreenWaitTime.")
					PRINTLN("[NETCELEBRATION] - DISPLAY_GOLF_END_SCREEN - going to stage CELEBRATION_STAGE_END_TRANSITION.")
					golfCelebrationData.eCurrentStage = CELEBRATION_STAGE_END_TRANSITION
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE CELEBRATION_STAGE_END_TRANSITION
			
//			CDEBUG1LN(DEBUG_GOLF, "End screen time ", golfCelebrationData.iEstimatedScreenDuration - GET_GAME_TIMER())
			
			// Create scene for winner scene and wait for timer to expire.
			IF GET_GAME_TIMER() > golfCelebrationData.iEstimatedScreenDuration
				
				PRINTLN("[NETCELEBRATION] - DISPLAY_GOLF_END_SCREEN - GET_GAME_TIMER() > golfCelebrationData.iEstimatedScreenDuration.")
				IF bCreateWinnerScreen
					DRAW_CELEBRATION_SCREEN_THIS_FRAME(golfCelebrationData)
					FLASH_CELEBRATION_SCREEN(golfCelebrationData, 0.25, 0.75, 0.25)
					golfCelebrationData.iEstimatedScreenDuration = GET_GAME_TIMER() + 250
					PRINTLN("[NETCELEBRATION] - DISPLAY_GOLF_END_SCREEN - going to stage CELEBRATION_STAGE_DOING_FLASH.")
					golfCelebrationData.eCurrentStage = CELEBRATION_STAGE_DOING_FLASH
				ELSE
					SET_SKYSWOOP_UP()
					STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
					IF IS_SWITCH_TO_MULTI_FIRSTPART_FINISHED()
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				DRAW_CELEBRATION_SCREEN_THIS_FRAME(golfCelebrationData)
			ENDIF
			
		BREAK
			
		CASE CELEBRATION_STAGE_DOING_FLASH
		
			CDEBUG1LN(DEBUG_GOLF, "CELEBRATION_STAGE_DOING_FLASH ", golfCelebrationData.iEstimatedScreenDuration - GET_GAME_TIMER())
			
			IF GET_GAME_TIMER() > (golfCelebrationData.iEstimatedScreenDuration - 100)
				SET_HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(golfCelebrationData)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE) // url:bugstar:2244903 - remove helmet mid-flash.
				ENDIF
			ENDIF
				
			IF GET_GAME_TIMER() > golfCelebrationData.iEstimatedScreenDuration
				
				IF ADD_GOLF_WINNER_CELEBRATION_SCREEN(golfCelebrationData, golfEndScreenWinnerID, playerIndexWinner, eWinStatus,  strBackgroundColour, sCrewName, sWinnerName, iBetWinings, GlobalplayerBD[NATIVE_TO_INT(playerIndexWinner)].iInteractionAnim)
					
					PRINTLN("[NETCELEBRATION] - DISPLAY_GOLF_END_SCREEN - GET_GAME_TIMER() > golfCelebrationData.iEstimatedScreenDuration.")
					
					// Destory current cam in preperation for new shot.
					IF IS_CAM_RENDERING(camEndScreen)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ENDIF
					IF IS_CAM_ACTIVE(camEndScreen)
						SET_CAM_ACTIVE(camEndScreen, FALSE)
					ENDIF
					IF DOES_CAM_EXIST(camEndScreen)
						DESTROY_CAM(camEndScreen, TRUE)
					ENDIF
					
					// Recreate cam and place until new shot.
					PLACE_WINNER_SCENE_CAMERA(sCelebServer, golfCelebrationData, camEndScreen, DEFAULT, NATIVE_TO_INT(playerIndexWinner))
						
	//				PLAY_GOLF_ENDSCREEN_POST_EFFECT(iPlacement, TRUE)
					
					IF bDraw
						iWinnerNetIndex = -1
						eWinStatus = JOB_STATUS_DRAW
						strBackgroundColour = "HUD_COLOUR_GREY"
					ELIF iWinnerNetIndex = PARTICIPANT_ID_TO_INT()
						eWinStatus = JOB_STATUS_WIN
						strBackgroundColour = "HUD_COLOUR_BLUEDARK"
					ELSE
						eWinStatus = JOB_STATUS_WIN //the camea is going to focus on the winner
						strBackgroundColour = "HUD_COLOUR_RED"
					ENDIF
					
					SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
					RESET_CELEBRATION_PRE_LOAD(golfCelebrationData)
					
					ADJUST_ESTIMATED_TIME_BASED_ON_ANIM_LENGTH(golfCelebrationData)
					golfCelebrationData.iEstimatedScreenDuration = GET_GAME_TIMER() + 6000
					iGolfEndScreenWaitTime = GET_GAME_TIMER() + 1000
					
					MP_TEXT_CHAT_DISABLE(TRUE)
					
					PRINTLN("[NETCELEBRATION] - DISPLAY_GOLF_END_SCREEN - going to stage CELEBRATION_STAGE_PLAYING.")
					golfCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
					
				ENDIF
			ENDIF
							
			// Keep drawing flash.
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(golfCelebrationData)
			
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING
			IF iWinnerNetIndex > -1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iWinnerNetIndex))
					playerIndexWinner = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iWinnerNetIndex))
				ENDIF
			ENDIF
			
			CDEBUG1LN(DEBUG_GOLF, "CELEBRATION_STAGE_PLAYING ", golfCelebrationData.iEstimatedScreenDuration - GET_GAME_TIMER())
			
			IF golfCelebrationData.bAllowPlayerNameToggles
				
				CELEB_SET_PLAYERS_INVISIBLE_THIS_FRAME()
				DRAW_THE_PLAYER_LIST(dpadVars)
				MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS(golfCelebrationData)
			
				IF IS_XBOX360_VERSION()
				OR IS_PS3_VERSION()
					DISPLAY_CELEBRATION_PLAYER_NAMES(golfCelebrationData.pedWinnerClones, golfCelebrationData.tl31_pedWinnerClonesNames, golfCelebrationData.eCurrentStage, golfCelebrationData.sfCelebration)
				ELSE
					DRAW_NG_CELEBRATION_PLAYER_NAMES(	golfCelebrationData, golfCelebrationData.iDrawNamesStage, golfCelebrationData.iNumNamesToDisplay, 
														golfCelebrationData.pedWinnerClones, golfCelebrationData.tl31_pedWinnerClonesNames, 
														golfCelebrationData.eCurrentStage, golfCelebrationData.sfCelebration,
														golfCelebrationData.playerNameMovies, golfCelebrationData.bToggleNames)
				ENDIF
			
			ENDIF
			
			HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(golfCelebrationData) // Fix for B* 2278693 - stop player models being seen during winner scene, only want ot see ped clones.
			
//			IF GET_GAME_TIMER() > golfCelebrationData.iEstimatedScreenDuration
//				STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
//				SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
//				golfCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
//			ENDIF

			IF GET_GAME_TIMER() >= iGolfEndScreenWaitTime
				DRAW_CELEBRATION_SCREEN_USING_3D_BACKGROUND(golfCelebrationData, <<0.0, -0.5, 0.0>>, <<0.0, 0.0, 0.0>>, <<10.0, 5.0, 5.0>>)
				DRAW_BLACK_RECT_FOR_WINNER_SCREEN_END_TRANSITION(GET_GAME_TIMER(), golfCelebrationData.iEstimatedScreenDuration - 400)
			ENDIF
			
			IF GET_GAME_TIMER() > golfCelebrationData.iEstimatedScreenDuration
				CDEBUG1LN(DEBUG_GOLF, "[NETCELEBRATION] - DISPLAY_GOLF_END_SCREEN - GET_GAME_TIMER() > golfCelebrationData.iEstimatedScreenDuration.")
				IF NOT bRepositionedCamForSwoop
//					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
//						VECTOR vCamCoords
//						vCamCoords = GET_CAM_COORD(camEndScreen)
//						vCamCoords.z = 1000.0
//						SET_CAM_COORD(camEndScreen, vCamCoords)
//						SET_ENTITY_COORDS(PLAYER_PED_ID(), vCamCoords, FALSE)
//						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
//						PRINTLN("[NETCELEBRATION] - put winner cam and player ped at ", vCamCoords)
//					ENDIF
				ENDIF
				SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
				STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
				
				IF iGolfSkycamTime = 0
					IF NOT bSpectator
						SET_SKYSWOOP_UP(TRUE, TRUE, TRUE, SWITCH_TYPE_LONG)
					ELSE
						SET_SKYSWOOP_UP()
					ENDIF
					iGolfSkycamTime = GET_GAME_TIMER() + 3000
				ENDIF
				
				CDEBUG1LN(DEBUG_GOLF, "[NETCELEBRATION] - Skycam time: ", iGolfSkycamTime - GET_GAME_TIMER())
				
				IF GET_GAME_TIMER() > iGolfSkycamTime
					golfCelebrationData.bAllowPlayerNameToggles = FALSE
					IF IS_PLAYER_SCTV(PLAYER_ID())
						CLEAR_SPECTATOR_OVERRIDE_COORDS()
					ENDIF
					MP_TEXT_CHAT_DISABLE(FALSE)
					RETURN TRUE
				ENDIF
			ELSE
				golfCelebrationData.bAllowPlayerNameToggles = TRUE
			ENDIF
		
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED
			IF IS_PLAYER_SCTV(PLAYER_ID())
				CLEAR_SPECTATOR_OVERRIDE_COORDS()
			ENDIF
			MP_TEXT_CHAT_DISABLE(FALSE)
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	IF (golfCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING)
		IF DOES_ENTITY_EXIST(golfCelebrationData.pedWinnerClones[0])
			IF NOT IS_ENTITY_DEAD(golfCelebrationData.pedWinnerClones[0])
				IF DOES_CAM_EXIST(camEndScreen)
					IF IS_CAM_RENDERING(camEndScreen)
						UPDATE_CELEBRATION_WINNER_ANIMS(golfCelebrationData, golfCelebrationData.pedWinnerClones[0], golfCelebrationData.iWinnerPlayerID, 0, FALSE, FALSE, FALSE, PAIRED_CELEBRATION_GENDER_COMBO_NOT_SET)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
				
	RETURN FALSE
ENDFUNC

PROC DO_GOLF_SHARD_END()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siGolfSplash[iCurrentGolfScaleformIndex], "SHARD_ANIM_OUT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eGolfSplashEndColour)) 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.33)
	END_SCALEFORM_MOVIE_METHOD()

	iGolfEndSplashDuration = GET_GAME_TIMER() + 330
ENDPROC

PROC CLEAR_GOLF_SPLASH()
	IF IS_BITMASK_AS_ENUM_SET(iGolfSplashFlags, GOLF_SPLASH_BIT_PLAY_SHARD_END_ANIM)
		DO_GOLF_SHARD_END()
		CLEAR_BITMASK_AS_ENUM(iGolfSplashFlags, GOLF_SPLASH_BIT_PLAY_SHARD_END_ANIM)
	ENDIF
	
	iGolfSplashDuration = 0
	
ENDPROC

FUNC BOOL IS_GOLF_SPLASH_DISPLAYING()
	RETURN iGolfSplashDuration > GET_GAME_TIMER()
ENDFUNC

PROC UPDATE_GOLF_SPLASH()
		
	IF iGolfSplashDuration > GET_GAME_TIMER()
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(siGolfSplash[iCurrentGolfScaleformIndex], 255, 255, 255, 255)
		//DRAW_SCALEFORM_MOVIE(siGolfSplash[iCurrentGolfScaleformIndex], vGolfSplashPosition.x, vGolfSplashPosition.y, 1.0, 1.0, 255, 255, 255, 255)
	ELSE
		IF IS_BITMASK_AS_ENUM_SET(iGolfSplashFlags, GOLF_SPLASH_BIT_PLAY_SHARD_END_ANIM)
			DO_GOLF_SHARD_END()
			CLEAR_BITMASK_AS_ENUM(iGolfSplashFlags, GOLF_SPLASH_BIT_PLAY_SHARD_END_ANIM)
		ENDIF
	ENDIF
	
	IF iGolfEndSplashDuration > GET_GAME_TIMER()
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(siGolfSplash[iCurrentGolfScaleformIndex], 255, 255, 255, 255)
	ENDIF

ENDPROC
