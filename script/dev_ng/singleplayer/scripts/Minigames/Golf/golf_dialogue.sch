
USING "golf.sch"
USING "golf_foursome.sch"
USING "golf_buddies_lib.sch"
USING "golf_course_lib.sch"

TWEAK_FLOAT GOLF_PARTNER_SPEAK_DELAY 60.0

FUNC INT GET_GOLF_PARTNER_INDEX(GOLF_FOURSOME &thisFoursome)

	//you are driving the first cart, the second player is your partner
	IF GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(thisFoursome, thisFoursome.iLocalPlayerIndex) = 0
		RETURN 1
	ENDIF
	
	//you are driving the 2nd cart, the last player is your partner
   RETURN 3
ENDFUNC


FUNC BOOL GOLF_CART_IS_READY_FOR_DIALOGUE(GOLF_FOURSOME &thisFoursome, INT iPartnerIndex)

	VEHICLE_INDEX cartIndex = GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(thisFoursome, thisFoursome.iLocalPlayerIndex)
	PED_INDEX partnerPed = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPartnerIndex)

	IF NOT IS_ENTITY_DEAD(partnerPed) AND NOT IS_ENTITY_DEAD(cartIndex)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), cartIndex)
		AND IS_PED_IN_VEHICLE(partnerPed, cartIndex)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC 

FUNC BOOL IS_GOLF_PARTNER_NICE(GOLF_HELPERS &thisHelpers, INT iPartnerIndex)

	GOLF_BUDDIES selectedBuddy = GET_GOLF_INDEXED_SELECTED_BUDDY(thisHelpers, iPartnerIndex)
	
	IF selectedBuddy = GB_MALE1
		RETURN FALSE
	ELIF selectedBuddy = GB_DOMESTIC
		RETURN FALSE
	ELIF selectedBuddy = GB_MALE3
		RETURN FALSE
	ELIF selectedBuddy = GB_MALE4
		RETURN FALSE	
	ELIF selectedBuddy = GB_MALE5
		RETURN TRUE
//	ELIF selectedBuddy = GB_CAR_THEFT_VICTIM
//		RETURN TRUE
//	ELIF selectedBuddy = GB_FEMALE2
//		RETURN TRUE
//	ELIF selectedBuddy = GB_FEMALE3
//		RETURN TRUE
//	ELIF selectedBuddy = GB_FEMALE4
//		RETURN TRUE
//	ELIF selectedBuddy = GB_FEMALE5
//		RETURN TRUE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC STRING GET_GOLF_PARTNER_VOICE_NAME(GOLF_HELPERS &thisHelpers, INT iPartnerIndex)

	GOLF_BUDDIES selectedBuddy = GET_GOLF_INDEXED_SELECTED_BUDDY(thisHelpers, iPartnerIndex)
	
	IF selectedBuddy = GB_MALE1
		RETURN "BDownGolfer"
	ELIF selectedBuddy = GB_DOMESTIC
		RETURN "BDownGolfer"
	ELIF selectedBuddy = GB_MALE3
		RETURN "BDownGolfer"
	ELIF selectedBuddy = GB_MALE4
		RETURN "BDownGolfer"	
	ELIF selectedBuddy = GB_MALE5
		RETURN "BDownGolfer"
//	ELIF selectedBuddy = GB_CAR_THEFT_VICTIM
//		RETURN "CThiefGolfer"
//	ELIF selectedBuddy = GB_FEMALE2
//		RETURN "CThiefGolfer"
//	ELIF selectedBuddy = GB_FEMALE3
//		RETURN "CThiefGolfer"
//	ELIF selectedBuddy = GB_FEMALE4
//		RETURN "CThiefGolfer"
//	ELIF selectedBuddy = GB_FEMALE5
//		RETURN "CThiefGolfer"
	ENDIF

	RETURN ""
ENDFUNC 

FUNC BOOL IS_GOLF_PLAYER_BEATING_PARNTER(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &allPlayers[], INT iPartnerIndex)
	
	IF GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[thisFoursome.iLocalPlayerIndex]) < GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[iPartnerIndex])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_GOLF_PLAYER_DOING_BAD_DAILOGUE(GOLF_FOURSOME &thisFoursome)
	RETURN IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, thisFoursome.iLocalPlayerIndex, GOLF_STROKE_LIMIT)
ENDFUNC

PROC GOLF_MANAGE_ADDRESS_BALL_DIALOGUE(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)

	FLOAT fSwingHeading = GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome)
	VECTOR vPlayerAim = -1.0*NORMALISE_VECTOR(<<COS(fSwingHeading), SIN(fSwingHeading), 0>>)
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	INT iPartnerIndex
	
	IF NOT IS_GOLF_DIALOGUE_FLAG_SET(thisHelpers, GDF_PARTNER_SAID_AIM_WARNING)
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) > 1
		REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPartnerIndex	
			IF iPartnerIndex != GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
			AND NOT IS_PED_INJURED(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPartnerIndex))
			
				VECTOR vPartnerPos = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPartnerIndex))
				VECTOR vToPartner = vPartnerPos - vPlayerPos
				vToPartner = NORMALISE_VECTOR(<<vToPartner.x, vToPartner.y, 0>>)
				
				IF DOT_PRODUCT_XY(vToPartner, vPlayerAim) > 0.95
				AND VDIST2(vPartnerPos, vPlayerPos) < 10.0*10.0
				AND NOT IS_GOLF_DIALOGUE_FLAG_SET(thisHelpers, GDF_PARTNER_SAID_AIM_WARNING)
					PRINTLN("Player aiming at partner, play conversation")
					PLAY_PED_AMBIENT_SPEECH(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPartnerIndex), "GENERIC_INSULT_MED", SPEECH_PARAMS_FORCE_NORMAL)
				
					SET_GOLF_DIALOGUE_FLAG(thisHelpers, GDF_PARTNER_SAID_AIM_WARNING)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC GOLF_MANAGE_CART_DIALOGUE(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &allPlayers[])
	
	#IF GOLF_IS_MP
		EXIT
	#ENDIF
	
	UNUSED_PARAMETER(thisCourse)
	UNUSED_PARAMETER(allPlayers)
	
	IF GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) = 1
		EXIT
	ENDIF
	
	//PRINTLN("Idle speach time ", GET_TIMER_IN_SECONDS(thisHelpers.idleSpeechTimer))
	
	INT iPartnerIndex = GET_GOLF_PARTNER_INDEX(thisFoursome)
	
	IF NOT IS_TIMER_STARTED(thisHelpers.idleSpeechTimer)
		RESTART_TIMER_NOW(thisHelpers.idleSpeechTimer)
	ENDIF
	IF GOLF_CART_IS_READY_FOR_DIALOGUE(thisFoursome, iPartnerIndex)
	AND NOT IS_AMBIENT_SPEECH_PLAYING(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPartnerIndex))
		IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID()) AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND TIMER_DO_WHEN_READY(thisHelpers.idleSpeechTimer, 2.0)
			CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			PRINTLN("Player hit someone with cart, play line ")
			PLAY_PED_AMBIENT_SPEECH(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPartnerIndex), "CAR_HIT_PED_DRIVEN", SPEECH_PARAMS_FORCE_NORMAL)
			
			RESTART_TIMER_NOW(thisHelpers.idleSpeechTimer)
		ELIF TIMER_DO_WHEN_READY(thisHelpers.idleSpeechTimer, GOLF_PARTNER_SPEAK_DELAY)
			TEXT_LABEL_31	chatText = "GOLF_CHAT"
			PED_INDEX pedSpeaker = PLAYER_PED_ID()
			BOOL bPlayerSpeaks = GET_RANDOM_INT_IN_RANGE(0, 10) < 5
			//PRINTLN("Time for buddy to say somthing")
			SWITCH GET_ENTITY_MODEL(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPartnerIndex))
				CASE PLAYER_ZERO
					IF bPlayerSpeaks
						chatText = "GOLF_CHAT_WITH_MICHAEL"
					ELSE
						pedSpeaker = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPartnerIndex)
						SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
							CASE PLAYER_ONE
								chatText = "GOLF_CHAT_WITH_FRANKLIN"
							BREAK
							CASE PLAYER_TWO
								chatText = "GOLF_CHAT_WITH_TREVOR"
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				CASE PLAYER_ONE
					IF bPlayerSpeaks
						chatText = "GOLF_CHAT_WITH_FRANKLIN"
					ELSE
						pedSpeaker = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPartnerIndex)
						SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
							CASE PLAYER_ZERO
								chatText = "GOLF_CHAT_WITH_MICHAEL"
							BREAK
							CASE PLAYER_TWO
								chatText = "GOLF_CHAT_WITH_TREVOR"
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				CASE PLAYER_TWO
					IF bPlayerSpeaks
						chatText = "GOLF_CHAT_WITH_TREVOR"
					ELSE
						pedSpeaker = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPartnerIndex)
						SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
							CASE PLAYER_ZERO
								chatText = "GOLF_CHAT_WITH_MICHAEL"
							BREAK
							CASE PLAYER_ONE
								chatText = "GOLF_CHAT_WITH_FRANKLIN"
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
			
			PLAY_PED_AMBIENT_SPEECH(pedSpeaker, chatText, SPEECH_PARAMS_FORCE_NORMAL)
			PRINTLN("Cart dialogue - Play ", chatText, " on ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(pedSpeaker)))
			RESTART_TIMER_NOW(thisHelpers.idleSpeechTimer)
		ENDIF
	ELIF NOT IS_GOLF_DIALOGUE_FLAG_SET(thisHelpers, GDF_PLAYER_GET_IN_CART) 
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(thisFoursome, thisFoursome.iLocalPlayerIndex))
		AND NOT IS_PED_IN_VEHICLE(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPartnerIndex), GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(thisFoursome, thisFoursome.iLocalPlayerIndex))
			PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GET_IN_VEHICLE", SPEECH_PARAMS_FORCE_NORMAL)
			SET_GOLF_DIALOGUE_FLAG(thisHelpers, GDF_PLAYER_GET_IN_CART)
			PRINTLN("Get in Cart dialogue")
		ENDIF

	ENDIF

ENDPROC


