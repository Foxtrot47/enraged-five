USING "minigames_helpers.sch"
USING "golf.sch"
USING "golf_ui.sch"
USING "golf_buddies_lib.sch"
USING "golf_ui_lib.sch"
USING "net_player_headshots.sch"

/// PURPOSE:
///    Once all hole data is setup, sets the hole lengths and pars.
/// PARAMS:
///    newCourse - 
PROC GOLF_SETUP_HOLE_LENGTHS_AND_PARS(GOLF_COURSE &newCourse)
	INT holeIndex, totalPar
	REPEAT GET_GOLF_COURSE_NUM_HOLES(newCourse) holeIndex
		newCourse.golfHole[holeIndex].fHoleLength = VDIST(GET_GOLF_HOLE_TEE_POSITION(newCourse,holeIndex), GET_GOLF_HOLE_FAIRWAY_POSITION(newCourse, holeIndex)) + VDIST(GET_GOLF_HOLE_FAIRWAY_POSITION(newCourse, holeIndex), GET_GOLF_HOLE_PIN_POSITION(newCourse, holeIndex))
		IF newCourse.golfHole[holeIndex].fHoleLength < 35
			DEBUG_MESSAGE("PAR 1 - putting green!")
			newCourse.golfHole[holeIndex].iPar = 1
		ELIF newCourse.golfHole[holeIndex].fHoleLength < 125
			DEBUG_MESSAGE("PAR 3")
			newCourse.golfHole[holeIndex].iPar = 3
		ELIF newCourse.golfHole[holeIndex].fHoleLength < 260
			DEBUG_MESSAGE("PAR 4")
			newCourse.golfHole[holeIndex].iPar = 4
		ELSE
			DEBUG_MESSAGE("PAR 5")
			newCourse.golfHole[holeIndex].iPar = 5
		ENDIF
		totalPar += newCourse.golfHole[holeIndex].iPar
		SET_GOLF_COURSE_TOTAL_PAR(newCourse, totalPar)
		//DEBUG_MESSAGE()
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Sets up all hole data (Start and cart positions, Tee, pin, tee spectate, green spectate positions for each hole)
/// PARAMS:
///    newCourse - 
PROC GOLF_SETUP_CURRENT_COURSE(GOLF_COURSE &newCourse)
	//Golf Trigger Position
	//newCourse.vPlayerStartPosition[0] = << -1369.7603, 56.9614, 52.6364 >> //  // heading: 263.8103   room: NONE
	
	//Game Start Position
	newCourse.vPlayerStartPosition[0] = << -1339.2357, 60.7393, 54.0802 >> // // heading: 272.0953   room: NONE
	newCourse.vPlayerStartPosition[1] = << -1339.2349, 59.1891, 54.0802 >> // // heading: 272.0953   room: NONE
	newCourse.vPlayerStartPosition[2] = << -1337.0663, 56.5560, 54.0804 >>  // // heading: 272.0953   room: NONE
	newCourse.vPlayerStartPosition[3] = << -1342.3146, 59.0051, 54.2456 >> // // heading: 272.0953   room: NONE
	
	//Cart Position
	newCourse.vCartStartPosition[0]  = << -1324.3918, 59.1355, 52.5704 >> // // heading: 272.0202   room: NONE
	newCourse.vCartStartPosition[1]  = << -1324.4554, 55.1355, 52.5710 >> // // heading: 272.0202   room: NONE
	newCourse.vCartStartPosition[2]  = << -1324.4554, 51.1355, 52.5710 >> // // heading: 272.0202   room: NONE
	newCourse.vCartStartPosition[3]  = << -1324.4554, 47.1355, 52.5710 >> // // heading: 272.0202   room: NONE
	
#IF NOT COMPILE_LOOKUPS
	
//  TEE POSITIONS	
	//	Hole 1: Tee
	newCourse.golfHole[0].vTeePosition = << -1370.93, 173.98, 57.01>>   //heading: 192.7659   room: NONE
	newCourse.golfHole[1].vTeePosition = << -1107.26, 157.15, 62.04 >>   //heading: 98.5704   room: NONE
	newCourse.golfHole[2].vTeePosition = << -1312.97, 125.64, 56.39 >>   //heading: 213.8021   room: NONE
	newCourse.golfHole[3].vTeePosition = << -1218.56, 107.48, 57.04 >>   //heading: 199.1169   room: NONE
	newCourse.golfHole[4].vTeePosition = << -1098.15, 69.50, 53.09 >>   //heading: 122.8670   room: NONE
	newCourse.golfHole[5].vTeePosition = << -987.70, -105.42, 39.59 >>   //heading: 29.3958   room: NONE
	newCourse.golfHole[6].vTeePosition = << -1117.793, -104.069, 40.8406 >>   //heading: 323.8197   room: NONE
	newCourse.golfHole[7].vTeePosition = << -1272.63, 38.40, 48.75 >>   //heading: 176.6545   room: NONE
	newCourse.golfHole[8].vTeePosition = << -1138.38123, 0.60467, 47.98225>>   //heading: 59.4948   room: NONE
	
// TEE SPECTATE
	newCourse.golfHole[0].vTeeSpectate[0] = << -1373.4279, 168.3476, 57.0130 >>
	newCourse.golfHole[0].vTeeSpectate[1] = << -1371.4246, 166.0890, 57.0130 >>
	newCourse.golfHole[0].vTeeSpectate[2] = << -1368.3165, 165.7858, 56.9920 >>
	newCourse.golfHole[0].vTeeSpectate[3] = << -1365.8618, 166.4386, 57.0063 >>

	newCourse.golfHole[1].vTeeSpectate[0]  = << -1101.0491, 156.1904, 62.0401 >>  // heading: 163.6937   room: NONE
	newCourse.golfHole[1].vTeeSpectate[1]  = << -1100.9050, 159.2561, 62.0415 >>  // heading: 11.1509   room: NONE
	newCourse.golfHole[1].vTeeSpectate[2]  = << -1102.7826, 161.6288, 62.0412 >>  // heading: 12.6381   room: NONE
	newCourse.golfHole[1].vTeeSpectate[3]  = << -1105.9637, 161.2863, 62.0406 >> // heading: 83.9452   room: NONE
	
	newCourse.golfHole[2].vTeeSpectate[0] = <<-1317.0281, 128.0565, 56.4377>>  // heading: 308.3324   room: NONE
	newCourse.golfHole[2].vTeeSpectate[1] = <<-1315.4363, 129.9425, 56.6243>> // heading: 107.5934   room: NONE
	newCourse.golfHole[2].vTeeSpectate[2] = <<-1313.4515, 131.9924, 56.8265>> // heading: 159.5085   room: NONE
	newCourse.golfHole[2].vTeeSpectate[3] = <<-1317.2489, 133.3213, 56.7050>> // heading: 243.6013   room: NONE
	
	newCourse.golfHole[3].vTeeSpectate[0] = << -1218.8939, 110.6482, 57.08 >>  // heading: 311.1057   room: NONE
	newCourse.golfHole[3].vTeeSpectate[1] = << -1222.2432, 110.2088, 57.08 >>  // heading: 121.9438   room: NONE
	newCourse.golfHole[3].vTeeSpectate[2] = << -1224.1785, 107.4351, 57.0703 >>  // heading: 191.2770   room: NONE
	newCourse.golfHole[3].vTeeSpectate[3] = << -1221.2565, 101.3278, 57.08 >>  // heading: 249.2166   room: NONE

	newCourse.golfHole[4].vTeeSpectate[0] = << -1104.6074, 70.6124, 53.2120 >>  // heading: 297.0445   room: NONE
	newCourse.golfHole[4].vTeeSpectate[1] = << -1101.6980, 73.7137, 53.1993 >>  // heading: 19.2756   room: NONE
	newCourse.golfHole[4].vTeeSpectate[2] = << -1098.7460, 75.2217, 53.1970 >> //  heading: 116.4343   room: NONE
	newCourse.golfHole[4].vTeeSpectate[3] = << -1095.6252, 74.6875, 53.1712 >>  // heading: 197.5942   room: NONE

	newCourse.golfHole[5].vTeeSpectate[0] = << -984.8632, -108.5439, 39.5642 >>  // heading: 55.8206   room: NONE
	newCourse.golfHole[5].vTeeSpectate[1] = << -982.4098, -106.4736, 39.5732 >>  // heading: 275.4746   room: NONE
	newCourse.golfHole[5].vTeeSpectate[2] = << -981.2261, -103.0422, 39.5779 >>  // heading: 200.7771   room: NONE
	newCourse.golfHole[5].vTeeSpectate[3] = << -981.8594, -100.6231, 39.5813 >> // heading: 109.8125   room: NONE

	newCourse.golfHole[6].vTeeSpectate[0] = << -1113.8646, -100.3123, 40.9050 >>  // heading: 145.5899   room: NONE
	newCourse.golfHole[6].vTeeSpectate[1] = << -1111.5592, -104.7822, 40.8405 >>  // heading: 245.5676   room: NONE
	newCourse.golfHole[6].vTeeSpectate[2] = << -1113.2805, -107.0443, 40.8405 >>  // heading: 146.3829   room: NONE
	newCourse.golfHole[6].vTeeSpectate[3] = << -1116.9398, -109.7583, 40.8608 >>  // heading: 113.1883   room: NONE

	newCourse.golfHole[7].vTeeSpectate[0] = << -1277.2773, 36.1405, 48.9194 >>  // heading: 180.6270   room: NONE
	newCourse.golfHole[7].vTeeSpectate[1] = << -1277.3438, 39.2424, 49.1028 >>  // heading: 97.2724   room: NONE
	newCourse.golfHole[7].vTeeSpectate[2] = << -1275.5933, 41.3619, 49.0876 >>  // heading: 333.7548   room: NONE
	newCourse.golfHole[7].vTeeSpectate[3] = << -1271.2444, 43.9149, 48.9679 >>  // heading: 264.6365   room: NONE

	newCourse.golfHole[8].vTeeSpectate[0] = <<-1138.5895, -5.6756, 47.9822>>  // heading: 209.1611   room: NONE
	newCourse.golfHole[8].vTeeSpectate[1] = <<-1136.4796, -5.8462, 47.9822>>  // heading: 259.7154   room: NONE
	newCourse.golfHole[8].vTeeSpectate[2] =  <<-1134.6447, -4.3631, 47.9822>>  // heading: 178.9254   room: NONE
	newCourse.golfHole[8].vTeeSpectate[3] = <<-1133.7120, -2.4897, 47.9822>>)  // heading: 71.9609   room: NONE


// FAIRWAY POSITIONS	
	newCourse.golfHole[0].vFairwayPosition = << -1252.9742, 182.4325, 61.3071 >>  // heading: 86.8004   room: NONE
	newCourse.golfHole[1].vFairwayPosition =  << -1222.2045, 150.2919, 58.7062 >>   //heading: 121.9376   room: NONE
	newCourse.golfHole[2].vFairwayPosition =  << -1240.0823, 105.7823, 55.6871 >>    //heading: 258.7050   room: NONE
	newCourse.golfHole[3].vFairwayPosition = << -1120.4330, 42.9609, 51.9460 >>   //heading: 36.2419   room: NONE
	newCourse.golfHole[4].vFairwayPosition =  << -1031.1995, -17.2876, 46.5155 >>   //heading: 221.5941   room: NONE
	newCourse.golfHole[5].vFairwayPosition = << -1100.05701, -114.27702, 40.53680 >>    //heading: 73.1453   room: NONE
	newCourse.golfHole[6].vFairwayPosition = << -1225.2073, -54.2714, 44.1932 >>    //heading: 128.3728   room: NONE
	newCourse.golfHole[7].vFairwayPosition = << -1159.3220, -26.5465, 44.7971 >>   //heading: 243.6702   room: NONE
	newCourse.golfHole[8].vFairwayPosition = << -1171.8191, 41.7586, 51.2172 >>   //heading: 29.4339   room: NONE


// PIN POSITIONS
	//	Hole 1: Green
	newCourse.golfHole[0].vHolePosition = << -1114.121, 220.789, 63.78 >>   //heading: 286.5638   room: NONE
	newCourse.golfHole[1].vHolePosition = << -1322.07, 158.77, 56.69 >>   //heading: 121.9376   room: NONE
	newCourse.golfHole[2].vHolePosition = << -1237.419, 112.988, 56.086 >>   //heading: 258.7050   room: NONE
	newCourse.golfHole[3].vHolePosition = << -1096.541, 7.848, 49.63 >>   //heading: 36.2419   room: NONE
	newCourse.golfHole[4].vHolePosition = << -957.386, -90.412, 39.161 >>   //heading: 221.5941   room: NONE
	newCourse.golfHole[5].vHolePosition = << -1103.516, -115.163, 40.444 >>   //heading: 73.1453   room: NONE
	newCourse.golfHole[6].vHolePosition = << -1290.632, 2.754, 49.217 >>   //heading: 128.3728   room: NONE
	newCourse.golfHole[7].vHolePosition = << -1034.944, -83.144, 42.919>>   //heading: 243.6702   room: NONE
	newCourse.golfHole[8].vHolePosition = << -1294.775, 83.51, 53.804 >>   //heading: 29.4339   room: NONE

	
// GREEN SPECTATE	
	newCourse.golfHole[0].vGreenSpectate[0] = << -1121.4823, 210.4423, 63.9292 >>
	newCourse.golfHole[0].vGreenSpectate[1] = << -1111.8832, 211.2035, 63.8440 >> 
	newCourse.golfHole[0].vGreenSpectate[2] = << -1109.5812, 220.7810, 63.9314 >>
	newCourse.golfHole[0].vGreenSpectate[3] = << -1117.7329, 231.4756, 64.5959 >>

	newCourse.golfHole[1].vGreenSpectate[0] = << -1324.6469, 150.2405, 56.9512 >>  // heading: 172.8204   room: NONE
	newCourse.golfHole[1].vGreenSpectate[1] = << -1332.1470, 152.7218, 56.9449 >>  // heading: 47.8838   room: NONE
	newCourse.golfHole[1].vGreenSpectate[2] = << -1340.3395, 165.9850, 57.0015 >>  // heading: 214.6889   room: NONE
	newCourse.golfHole[1].vGreenSpectate[3] = << -1330.3406, 172.5728, 57.0822 >>  // heading: 245.8232   room: NONE
	
	newCourse.golfHole[2].vGreenSpectate[0] = << -1230.4810, 103.0481, 55.7491 >>  // heading: 261.1602   room: NONE
	newCourse.golfHole[2].vGreenSpectate[1] = << -1234.8417, 95.7965, 55.6671 >>  // heading: 119.5674   room: NONE
	newCourse.golfHole[2].vGreenSpectate[2] = << -1243.4578, 95.5196, 55.5717 >>  // heading: 94.8704   room: NONE
	newCourse.golfHole[2].vGreenSpectate[3] = << -1245.1464, 112.5895, 55.9974 >>  // heading: 218.9134   room: NONE

	newCourse.golfHole[3].vGreenSpectate[0] = << -1092.2098, 1.2052, 49.9349 >>  // heading: 145.4737   room: NONE
	newCourse.golfHole[3].vGreenSpectate[1] = << -1108.3335, -4.3454, 49.7427 >>  // heading: 79.2235   room: NONE
	newCourse.golfHole[3].vGreenSpectate[2] = << -1113.0176, 15.5241, 49.4243 >>  // heading: 143.6452   room: NONE
	newCourse.golfHole[3].vGreenSpectate[3]= << -1103.5475, 27.2816, 50.2327 >>  // heading: 299.7788   room: NONE

	newCourse.golfHole[4].vGreenSpectate[0] = << -949.3773, -93.7449, 39.5250 >>  // heading: 54.6365   room: NONE
	newCourse.golfHole[4].vGreenSpectate[1] = << -965.4772, -92.9853, 39.3605 >>  // heading: 179.8926   room: NONE
	newCourse.golfHole[4].vGreenSpectate[2] = << -965.3192, -101.4310, 39.4042 >>  // heading: 248.5290   room: NONE
	newCourse.golfHole[4].vGreenSpectate[3] = << -952.4808, -99.1808, 39.5487 >>  // heading: 303.1429   room: NONE
	
	newCourse.golfHole[5].vGreenSpectate[0] = << -1098.2764, -107.6579, 40.5369 >>  // heading: 332.8230   room: NONE
	newCourse.golfHole[5].vGreenSpectate[1] = << -1106.5730, -106.9375, 40.6960 >>  // heading: 115.5258   room: NONE
	newCourse.golfHole[5].vGreenSpectate[2] = << -1111.6543, -121.3032, 40.7039 >>  // heading: 219.0944   room: NONE
	newCourse.golfHole[5].vGreenSpectate[3] = << -1102.0128, -127.7622, 40.6900 >>  // heading: 278.1586   room: NONE
	
	newCourse.golfHole[6].vGreenSpectate[0] = << -1294.8875, 10.1593, 50.3758 >>  // heading: 90.8053   room: NONE
	newCourse.golfHole[6].vGreenSpectate[1] = << -1288.8715, 14.8418, 49.8751 >>  // heading: 304.1386   room: NONE
	newCourse.golfHole[6].vGreenSpectate[2] = << -1276.2704, 11.8301, 48.5562 >>  // heading: 166.6862   room: NONE
	newCourse.golfHole[6].vGreenSpectate[3] = << -1283.1622, -6.8256, 48.6238 >>  // heading: 131.6282   room: NONE

	newCourse.golfHole[7].vGreenSpectate[0] = << -1041.6849, -75.4766, 43.0439 >>  // heading: 48.9067   room: NONE
	newCourse.golfHole[7].vGreenSpectate[1] = << -1030.3097, -76.6724, 43.2806 >>  // heading: 239.2213   room: NONE
	newCourse.golfHole[7].vGreenSpectate[2] = << -1029.7913, -88.4011, 43.1511 >>  // heading: 128.1387   room: NONE
	newCourse.golfHole[7].vGreenSpectate[3] = << -1041.5754, -92.4546, 42.8253 >>  // heading: 104.6235   room: NONE

	newCourse.golfHole[8].vGreenSpectate[0] = << -1284.5513, 76.0288, 53.9062 >>  // heading: 319.7612   room: NONE
	newCourse.golfHole[8].vGreenSpectate[1] = << -1282.6757, 86.4323, 53.9098 >>  // heading: 134.0081   room: NONE
	newCourse.golfHole[8].vGreenSpectate[2] = << -1289.7227, 75.8181, 53.9091 >>  // heading: 87.3882   room: NONE
	newCourse.golfHole[8].vGreenSpectate[3] = << -1292.6644, 75.2440, 53.9058 >>  // heading: 49.7862   room: NONE

#ENDIF
	newCourse.iNumHoles = 9
	GOLF_SETUP_HOLE_LENGTHS_AND_PARS(newCourse)
ENDPROC
PROC GOLF_SETUP_PUTTING_GREEN(GOLF_COURSE &newCourse)

#IF NOT COMPILE_LOOKUPS	
	newCourse.golfHole[0].vTeePosition = << -1329.875,                        52.557,                   52.677>>   //heading: 192.7659   room: NONE
	newCourse.golfHole[1].vTeePosition = << -1329.26,                      35.458,                   52.677 >>   //heading: 98.5704   room: NONE
	newCourse.golfHole[2].vTeePosition =  << -1330.135,                      95.877,                   54.98 >>  //heading: 213.8021   room: NONE
	newCourse.golfHole[3].vTeePosition =  << -1334.394,                      103.347,                 55.392 >>   //heading: 199.1169   room: NONE
	newCourse.golfHole[4].vTeePosition =  << -1332.059,                      68.521,                   52.736 >>   //heading: 122.8670   room: NONE
	newCourse.golfHole[5].vTeePosition = << -1336.215,                      82.867,                   53.703 >>   //heading: 29.3958   room: NONE

	newCourse.golfHole[0].vFairwayPosition = << -1329.875,                      35.458,                   52.677 >>   //heading: 286.5638   room: NONE
	newCourse.golfHole[1].vFairwayPosition = << -1329.26,                        52.557,                   52.677>>   //heading: 121.9376   room: NONE
	newCourse.golfHole[2].vFairwayPosition = << -1330.135,                      68.521,                   52.736 >>   //heading: 258.7050   room: NONE
	newCourse.golfHole[3].vFairwayPosition = << -1334.394,                      82.867,                   53.703 >>   //heading: 36.2419   room: NONE
	newCourse.golfHole[4].vFairwayPosition = << -1332.059,                      95.877,                   54.98 >>   //heading: 221.5941   room: NONE
	newCourse.golfHole[5].vFairwayPosition = << -1336.215,                      103.347,                 55.392 >>   //heading: 73.1453   room: NONE

	
	newCourse.golfHole[0].vHolePosition = << -1329.875,                      35.458,                   52.677 >>   //heading: 286.5638   room: NONE
	newCourse.golfHole[1].vHolePosition = << -1329.26,                        52.557,                   52.677>>   //heading: 121.9376   room: NONE
	newCourse.golfHole[2].vHolePosition = << -1330.135,                      68.521,                   52.736 >>   //heading: 258.7050   room: NONE
	newCourse.golfHole[3].vHolePosition = << -1334.394,                      82.867,                   53.703 >>   //heading: 36.2419   room: NONE
	newCourse.golfHole[4].vHolePosition = << -1332.059,                      95.877,                   54.98 >>   //heading: 221.5941   room: NONE
	newCourse.golfHole[5].vHolePosition = << -1336.215,                      103.347,                 55.392 >>   //heading: 73.1453   room: NONE
	
#ENDIF

	newCourse.iNumHoles = 6
	GOLF_SETUP_HOLE_LENGTHS_AND_PARS(newCourse)
ENDPROC


/// PURPOSE:
///    Sets up the data for a specific club type
/// PARAMS:
///    thisClub - 
///    clubToAdd - 
PROC SETUP_CLUB_OF_TYPE(CLUB_STRUCT &thisClub, eCLUB_TYPE clubToAdd)
	thisClub.kClubType = clubToAdd
#IF NOT COMPILE_LOOKUPS
	SWITCH clubToAdd
		CASE eCLUB_WOOD_1	// Driver				// 7
			thisClub.fClubHeadLoft = 12.5
			thisClub.fClubRange = 220 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.25
			thisClub.fClubAccuracyModifier = 0.75
			thisClub.oClubProp = PROP_GOLF_WOOD_01
			thisClub.txtName = "Driver"
		BREAK
		CASE eCLUB_WOOD_2	// Brassie				// 12
			thisClub.fClubHeadLoft = 13.75
			thisClub.fClubRange = 210 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.25
			thisClub.fClubAccuracyModifier = 0.75
			thisClub.oClubProp = PROP_GOLF_WOOD_01
			thisClub.txtName = "2 Wood"
		BREAK
		CASE eCLUB_WOOD_3							// 15
			thisClub.fClubHeadLoft = 16
			thisClub.fClubRange = 200 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.25
			thisClub.fClubAccuracyModifier = 0.75
			thisClub.oClubProp = PROP_GOLF_WOOD_01
			thisClub.txtName = "3 Wood"
		BREAK
		CASE eCLUB_WOOD_4							// 18
			thisClub.fClubHeadLoft = 18
			thisClub.fClubRange = 190 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.25
			thisClub.fClubAccuracyModifier = 0.75
			thisClub.oClubProp = PROP_GOLF_WOOD_01
			thisClub.txtName = "4 Wood"
		BREAK
		CASE eCLUB_WOOD_5	// Baffing Spoon ??		// 21
			thisClub.fClubHeadLoft = 21
			thisClub.fClubRange = 180 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.25
			thisClub.fClubAccuracyModifier = 0.75
			thisClub.oClubProp = PROP_GOLF_WOOD_01
			thisClub.txtName = "5 Wood"
		BREAK
		CASE eCLUB_IRON_2	// Cleek				// 17
			thisClub.fClubHeadLoft = 17
			thisClub.fClubRange = 180 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.5
			thisClub.fClubAccuracyModifier = 0.875
			thisClub.oClubProp = PROP_GOLF_IRON_01
			thisClub.txtName = "2 Iron"
		BREAK
		CASE eCLUB_IRON_3	// Mid Mashie			// 20
			thisClub.fClubHeadLoft = 20
			thisClub.fClubRange = 170 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.5
			thisClub.fClubAccuracyModifier = 0.875
			thisClub.oClubProp = PROP_GOLF_IRON_01
			thisClub.txtName = "3 Iron"
		BREAK
		CASE eCLUB_IRON_4	// Mashie Iron			// 23
			thisClub.fClubHeadLoft = 23
			thisClub.fClubRange = 160 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.5
			thisClub.fClubAccuracyModifier = 0.875
			thisClub.oClubProp = PROP_GOLF_IRON_01
			thisClub.txtName = "4 Iron"
		BREAK
		CASE eCLUB_IRON_5	// Mashie				// 26
			thisClub.fClubHeadLoft = 26
			thisClub.fClubRange = 150 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.6
			thisClub.fClubAccuracyModifier = 0.875
			thisClub.oClubProp = PROP_GOLF_IRON_01
			thisClub.txtName = "5 Iron"
		BREAK
		CASE eCLUB_IRON_6	// Spade Mashie			// 29
			thisClub.fClubHeadLoft = 29
			thisClub.fClubRange = 140 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.6
			thisClub.fClubAccuracyModifier = 0.875
			thisClub.oClubProp = PROP_GOLF_IRON_01
			thisClub.txtName = "6 Iron"
		BREAK
		CASE eCLUB_IRON_7	// Mashie Niblick		// 30
			thisClub.fClubHeadLoft = 30
			thisClub.fClubRange = 130 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.7
			thisClub.fClubAccuracyModifier = 0.875
			thisClub.oClubProp = PROP_GOLF_IRON_01
			thisClub.txtName = "7 Iron"
		BREAK
		CASE eCLUB_IRON_8	// Pitching Niblick		// 37
			thisClub.fClubHeadLoft = 37
			thisClub.fClubRange = 120 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.7
			thisClub.fClubAccuracyModifier = 0.875
			thisClub.oClubProp = PROP_GOLF_IRON_01
			thisClub.txtName = "8 Iron"
		BREAK
		CASE eCLUB_IRON_9	// Niblick				// 41	
			thisClub.fClubHeadLoft = 41
			thisClub.fClubRange = 110 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.8
			thisClub.fClubAccuracyModifier = 0.875
			thisClub.oClubProp = PROP_GOLF_IRON_01
			thisClub.txtName = "9 Iron"
		BREAK
		CASE eCLUB_WEDGE_PITCH						// 45
			thisClub.fClubHeadLoft = 45
			thisClub.fClubRange = 100 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 1.0
			thisClub.fClubAccuracyModifier = 1.0
			thisClub.oClubProp = Prop_Golf_Pitcher_01
			thisClub.txtName = "P Wedge"
		BREAK
		CASE eCLUB_WEDGE_APPROACH					// 50
			thisClub.fClubHeadLoft = 50
			thisClub.fClubRange = 85 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 1.0
			thisClub.fClubAccuracyModifier = 1.0
			thisClub.oClubProp = Prop_Golf_Pitcher_01
			thisClub.txtName = "A Wedge"
		BREAK
		CASE eCLUB_WEDGE_SAND						// 55
			thisClub.fClubHeadLoft = 55
			thisClub.fClubRange = 75 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 1.0
			thisClub.fClubAccuracyModifier = 1.0
			thisClub.oClubProp = Prop_Golf_Pitcher_01
			thisClub.txtName = "S Wedge"
		BREAK
		CASE eCLUB_WEDGE_LOB						// 60
			thisClub.fClubHeadLoft = 60
			thisClub.fClubRange = 65 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 1.0
			thisClub.fClubAccuracyModifier = 1.0
			thisClub.oClubProp = Prop_Golf_Pitcher_01
			thisClub.txtName = "L Wedge"
		BREAK
		CASE eCLUB_WEDGE_LOB_ULTRA					// 64
			thisClub.fClubHeadLoft = 64
			thisClub.fClubRange = 40 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 1.0
			thisClub.fClubAccuracyModifier = 1.0
			thisClub.oClubProp = Prop_Golf_Pitcher_01
			thisClub.txtName = "U Wedge"
		BREAK
		CASE eCLUB_PUTTER
			thisClub.fClubHeadLoft = 5
			thisClub.fClubRange = 30 / MAGIC_DISTANCE_CONVERSION
			thisClub.fClubSpinMagModifier = 0.1
			thisClub.fClubAccuracyModifier = 1.0
			thisClub.oClubProp = PROP_GOLF_PUTTER_01
			thisClub.txtName = "Putter"
		BREAK
	ENDSWITCH
#ENDIF
ENDPROC

/// PURPOSE:
///    Adds a specific club to the global bag
/// PARAMS:
///    golfBag - 
///    clubToAdd - 
///    powerMod - 
///    accMod - 
///    spinMod - 
/// RETURNS:
///    
FUNC BOOL GOLF_ADD_CLUB( GOLF_BAG &golfBag, eCLUB_TYPE clubToAdd)
	INT clubIndex
	INT golfBagSize = COUNT_OF(golfBag.clubs)
	REPEAT golfBagSize clubIndex
		IF golfBag.clubs[clubIndex].kClubType = eCLUB_INVALID // We have an unititialized club
			SETUP_CLUB_OF_TYPE(golfBag.clubs[clubIndex], clubToAdd)
//			golfBag.clubs[clubIndex].fClubAccuracyModifier += accMod
//			golfBag.clubs[clubIndex].fClubPowerModifier = powerMod
//			golfBag.clubs[clubIndex].fClubSpinMagModifier += spinMod
			golfBag.iNumClubs++
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC STRING GET_GOLF_MP_PLAYER_NAME(GOLF_PLAYER &thisPlayers[], INT currentPlayer)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(GET_GOLF_PLAYER_NET_INDEX_BY_INDEX(thisPlayers, currentPlayer))
		RETURN GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(GET_GOLF_PLAYER_NET_INDEX_BY_INDEX(thisPlayers, currentPlayer)))
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_GOLF_PLAYER_NAME(PED_INDEX pedIndex, GOLF_HELPERS &thisHelpers, INT playerIndex)
		
	SWITCH GET_GOLF_PED_ENUM(pedIndex)
	CASE CHAR_MICHAEL
		RETURN "BLIP_MICHAEL"
	CASE CHAR_FRANKLIN
		RETURN "BLIP_FRANKLIN"
	CASE CHAR_TREVOR
		RETURN "BLIP_170"
	CASE CHAR_LAMAR
		RETURN "BLIP_172"
	CASE CHAR_JIMMY
		RETURN "BLIP_33"
	DEFAULT
		RETURN GET_GOLF_BUDDY_STRING_TABLE_FIRST_NAME(GET_GOLF_INDEXED_SELECTED_BUDDY(thisHelpers, playerIndex))
	ENDSWITCH
ENDFUNC

FUNC TEXT_LABEL_15 GET_GOLF_PLAYER_CREW_TAG(PLAYER_INDEX playerIndex)
	
	GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(playerIndex)
  //  NETWORK_CLAN_DESC netClan = GET_GAMER_CREW(gamerHandle) 
	TEXT_LABEL_15 CrewTag 
	GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(gamerHandle, CrewTag)

	CDEBUG1LN(DEBUG_GOLF,"GET GOLF CREW TAG , ", CrewTag)
	
    RETURN CrewTag
    
ENDFUNC

PROC SET_GOLF_OUTFIT(PED_INDEX pedIndex)

	SWITCH GET_GOLF_PED_ENUM(pedIndex)
		CASE CHAR_MICHAEL
			/*
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_TORSO, 16, 0)
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_LEG, 15, 0)
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_FEET, 7, 0)
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_HAND, 3, 0)
			//*/	
			SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P0_GOLF, FALSE)
		BREAK
		CASE CHAR_FRANKLIN
			/*
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_TORSO, 6, 0)
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_LEG, 6, 0)
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_FEET, 5, 0)
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_HAND, 2, 0)
			//*/
			SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P1_GOLF, FALSE)
		BREAK
		CASE CHAR_TREVOR
			/*
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_TORSO, 11, 0)
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_LEG, 11, 0)
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_FEET, 1, 0)
			SET_PED_COMPONENT_VARIATION(playerPed, PED_COMP_HAND, 0, 0)
			//*/
			SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF, FALSE)
	
			BREAK
		CASE CHAR_LAMAR
			IF DOES_ENTITY_EXIST(pedIndex) AND NOT IS_ENTITY_DEAD(pedIndex)
				SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_TORSO, 1, 0)
				SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_LEG, 1, 0)
				SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_HAND, 1, 0)
			ENDIF
			BREAK
	ENDSWITCH
	
	REMOVE_PED_HELMET(pedIndex, TRUE)

ENDPROC

#IF NOT GOLF_IS_AI_ONLY

PROC GOLF_INIT_PLAYER_CARD(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse, GOLF_PLAYER &thisPlayers[])

INT currentPlayer
	TEXT_LABEL playerName
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) currentPlayer
		IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, currentPlayer, GOLF_PLAYER_REMOVED)
			playerName = GET_GOLF_PLAYER_NAME(GET_GOLF_PLAYER_PED(thisFoursome.playerCore[currentPlayer]), thisHelpers, currentPlayer)
			
			thisFoursome.playerCore[currentPlayer].playerCard.ID = currentPlayer
			thisFoursome.playerCore[currentPlayer].playerCard.sPlayerName = playerName
			thisFoursome.playerCore[currentPlayer].playerCard.crewTag = " "
			thisFoursome.playerCore[currentPlayer].playerCard.eBallColour = GET_GOLF_PLAYER_HUD_COLOUR(currentPlayer)
			
			CDEBUG1LN(DEBUG_GOLF,"Adding player at index ", currentPlayer)
			
			SET_PLAYERCARD_SLOT(thisHelpers, thisFoursome, thisFoursome.playerCore[currentPlayer].playerCard, 0, FALSE, FALSE)
			SET_SCOREBOARD_SLOT(thisHelpers, thisCourse, thisFoursome, thisPlayers[currentPlayer], thisFoursome.playerCore[currentPlayer].playerCard, FALSE, FALSE)
		ENDIF
	ENDREPEAT

ENDPROC

/// PURPOSE:
///    Sets up players for SP golf
/// PARAMS:
///    thisPlayers - 
///    thisFoursome - 
PROC GOLF_SETUP_PLAYERS( GOLF_GAME &thisGame, GOLF_PLAYER &thisPlayers[], GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	INT currentPlayer
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) currentPlayer
		IF currentPlayer = 0
			SET_GOLF_PLAYER_CONTROL(thisFoursome.playerCore[currentPlayer], HUMAN_LOCAL)
			SET_GOLF_PLAYER_PED(thisFoursome.playerCore[currentPlayer],PLAYER_PED_ID())
		//	thisPlayers[0].iCurrentHole = 0
			SET_GOLF_PLAYER_PAD_NUMBER_BY_INDEX(thisPlayers,currentPlayer, PAD1)
			SET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome, currentPlayer)
		ELSE
			IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_IS_FRIEND_ACTIVITY)
				IF currentPlayer = 1
					SET_GOLF_PLAYER_PED(thisFoursome.playerCore[currentPlayer],FRIEND_A_PED_ID())
					CDEBUG1LN(DEBUG_GOLF, "GOLF_SETUP_PLAYERS: weapon switch disabled on friend A")
					SET_PED_CAN_SWITCH_WEAPON(thisFoursome.playerCore[currentPlayer].pedGolfPlayer, FALSE)
					GET_PED_VARIATIONS(GET_GOLF_PLAYER_PED(thisFoursome.playerCore[currentPlayer]), thisHelpers.sFriendVariation)
					SET_GOLF_OUTFIT(GET_GOLF_PLAYER_PED(thisFoursome.playerCore[currentPlayer]))
				ENDIF
				
				IF DOES_ENTITY_EXIST(FRIEND_B_PED_ID()) AND currentPlayer = 2
					SET_GOLF_PLAYER_PED(thisFoursome.playerCore[currentPlayer],FRIEND_B_PED_ID())
					CDEBUG1LN(DEBUG_GOLF, "GOLF_SETUP_PLAYERS: weapon switch disabled on friend B")
					SET_PED_CAN_SWITCH_WEAPON(thisFoursome.playerCore[currentPlayer].pedGolfPlayer, FALSE)
					GET_PED_VARIATIONS(GET_GOLF_PLAYER_PED(thisFoursome.playerCore[currentPlayer]), thisHelpers.sFriendVariationB)
					SET_GOLF_OUTFIT(GET_GOLF_PLAYER_PED(thisFoursome.playerCore[currentPlayer]))
				ENDIF
				
			ENDIF
			SET_GOLF_PLAYER_CONTROL(thisFoursome.playerCore[currentPlayer], COMPUTER)
		ENDIF
	ENDREPEAT
ENDPROC


#IF GOLF_IS_MP
/// PURPOSE:
///    Sets up players for MP golf
/// PARAMS:
///    thisPlayers - 
///    thisFoursome - 
FUNC BOOL GOLF_SETUP_NETWORK_PLAYERS(GOLF_PLAYER &thisPlayers[], GOLF_FOURSOME &thisFoursome)
	INT currentPlayer
	
	CDEBUG1LN(DEBUG_GOLF,"Setup Golf Network Players.")
	
	//assume no one is playing golf
	REPEAT COUNT_OF(thisFoursome.playerCore) currentPlayer
		CDEBUG1LN(DEBUG_GOLF,"Seting ", currentPlayer, " as removed")
		SET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG(thisFoursome, currentPlayer, GOLF_PLAYER_REMOVED)
		iGolfPlayingParticipantID[currentPlayer] = -1
	ENDREPEAT
	
	thisFoursome.iNumPlayers = 0
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() currentPlayer
		INT iLocalPlayerIndex = GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(currentPlayer))
			CDEBUG1LN(DEBUG_GOLF,"Setting player ped ", GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome))
			SET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iLocalPlayerIndex, GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(currentPlayer))))
			IF IS_PED_INJURED(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iLocalPlayerIndex))
				CDEBUG1LN(DEBUG_GOLF,"Player ", currentPlayer, "setting ped didnt work!")
			ELSE
				thisPlayers[iLocalPlayerIndex].iParticipantIndex = currentPlayer
				iGolfPlayingParticipantID[iLocalPlayerIndex] = currentPlayer
				IF PLAYER_PED_ID() = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iLocalPlayerIndex)                                                                                                                                                                                                                                                                                                      
					SET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL(thisFoursome, iLocalPlayerIndex, HUMAN_LOCAL_MP)
					SET_GOLF_PLAYER_PAD_NUMBER_BY_INDEX(thisPlayers, iLocalPlayerIndex, PAD1)
					SET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome, iLocalPlayerIndex)
				ELSE
					SET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL(thisFoursome, iLocalPlayerIndex, HUMAN_NETWORK)
					
					SET_PED_LEG_IK_MODE(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iLocalPlayerIndex), LEG_IK_FULL)
					IF NOT IS_ENTITY_DEAD(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iLocalPlayerIndex))
						CDEBUG1LN(DEBUG_GOLF,"Setting entity can't blend, id ", iLocalPlayerIndex)
						NETWORK_SET_ENTITY_CAN_BLEND(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iLocalPlayerIndex), FALSE)
					ENDIF
				ENDIF
				
				IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(currentPlayer)))
				OR IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(currentPlayer)))
					CDEBUG1LN(DEBUG_GOLF,"Player ", currentPlayer, " has joined as spectator")
				ELSE
					CDEBUG1LN(DEBUG_GOLF,"Player ", currentPlayer, " joined normaly")
					CLEAR_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG(thisFoursome, iLocalPlayerIndex, GOLF_PLAYER_REMOVED)
				ENDIF
				
				INCREMENT_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome)
			ENDIF
		ELSE
			
		ENDIF
	ENDREPEAT
	
	IF NETWORK_GET_NUM_PARTICIPANTS() != GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome)
		DEBUG_MESSAGE("	GOLF_SETUP_NETWORK_PLAYERS got less peds that participants. Why? Who knows!")	
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

#ENDIF

PROC MANAGE_GOLF_MP_PLAYER_INDEXES(GOLF_PLAYER &thisPlayers[])
	INT iNetIndex, index
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iNetIndex
		INT iPlayersIndex = GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, iNetIndex)
	
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iNetIndex))
			IF iPlayersIndex < 0 //a new player joined
				
				REPEAT COUNT_OF(thisPlayers) index
					IF thisPlayers[index].iParticipantIndex = -1
						CDEBUG1LN(DEBUG_GOLF,"New player added to slot ", index, " with net index ", iNetIndex)
						thisPlayers[index].iParticipantIndex = iNetIndex
					ENDIF
				ENDREPEAT
				
			ENDIF 
		ELSE
			
			IF iPlayersIndex >= 0
				CDEBUG1LN(DEBUG_GOLF,"Valid player ", iPlayersIndex, " left, clear their spot")
				thisPlayers[iPlayersIndex].iParticipantIndex = -1
			ENDIF
			
		ENDIF
	ENDREPEAT
ENDPROC

PROC REGISTER_HEAD_TEXTURE( GOLF_FOURSOME &thisFoursome)
	PED_INDEX playerPed 
	STRING headShotTxd
	PEDHEADSHOT_ID myPedHeadshot
	INT iPlayerIndex, iFailSafe
	BOOL bHeadShotReady

	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		CDEBUG1LN(DEBUG_GOLF,"Loading headshot of player ", iPlayerIndex)
		iFailSafe = 0
		bHeadShotReady = FALSE
		
		playerPed = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayerIndex)
		IF NOT IS_PED_INJURED(playerPed)
			
			CDEBUG1LN(DEBUG_GOLF,"Register ped ", iPlayerIndex, " headshot")
			myPedHeadshot = REGISTER_PEDHEADSHOT(playerPed)		
			
			WHILE NOT bHeadShotReady
			AND iFailSafe < 50
				CDEBUG1LN(DEBUG_GOLF,"Headshot not ready, attempt ", iFailSafe)
				IF NOT IS_PEDHEADSHOT_VALID(myPedHeadshot)
					CDEBUG1LN(DEBUG_GOLF,"Headshot not valid? Re-register")
					myPedHeadshot = REGISTER_PEDHEADSHOT(playerPed)
				ELSE
					IF IS_PEDHEADSHOT_READY(myPedHeadshot)
						bHeadShotReady = TRUE
					ELSE
						CDEBUG1LN(DEBUG_GOLF,"Not ready?")
					ENDIF
				ENDIF
				
				iFailSafe++
				IF iFailSafe = 50 AND NOT bHeadShotReady
					CDEBUG1LN(DEBUG_GOLF,"Failed to load head textures")
					EXIT
				ENDIF
				WAIT(0)
			ENDWHILE
		ENDIF
		
		headShotTxd = GET_PEDHEADSHOT_TXD_STRING(myPedHeadshot)
		
		CDEBUG1LN(DEBUG_GOLF,"Head Texture Name is ", headShotTxd)
		
		thisFoursome.playerCore[iPlayerIndex].playerCard.mugShotDic = headShotTxd
		thisFoursome.playerCore[iPlayerIndex].playerCard.mugShotName = headShotTxd
	ENDREPEAT
ENDPROC

//returns true if the headshot has been loaded OR the headshot can't be loaded
//returns false if the headshot is loading
FUNC BOOL REGISTER_HEAD_TEXTURE_MP(INT iPlayerIndex, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], PEDHEADSHOT_ID &myPedHeadshot, BOOL &bIsRegistered)
	PED_INDEX playerPed 
	STRING headShotTxd
	
	INT iNetIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, iPlayerIndex)
	
	IF iNetIndex < 0
		CDEBUG1LN(DEBUG_GOLF,"Invlaid net index when registering head texture")
		RETURN TRUE
	ENDIF
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iNetIndex))
		CDEBUG1LN(DEBUG_GOLF,"Player ",iPlayerIndex , " with net index", iNetIndex, " does not exsist, thus he is valid.")
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_GOLF,"Loading headshot of player ", iPlayerIndex)
	IF NOT bIsRegistered
		playerPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iNetIndex)))
		IF NOT IS_PED_INJURED(playerPed)
			CDEBUG1LN(DEBUG_GOLF,"Regiester player ", iPlayerIndex, " headshot")
		    myPedHeadshot = Get_HeadshotID_For_Player(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iNetIndex)))
		ELSE
			CDEBUG1LN(DEBUG_GOLF,"Player is injured!? Can't register head shot")
			RETURN TRUE
		ENDIF

		bIsRegistered = TRUE
	ELSE
		IF NOT IS_PEDHEADSHOT_VALID(myPedHeadshot)
			CDEBUG1LN(DEBUG_GOLF,"Headshot not valid")
			RETURN FALSE
		ENDIF
		
		IF NOT IS_PEDHEADSHOT_READY(myPedHeadshot)
			CDEBUG1LN(DEBUG_GOLF,"Headshot not ready")
			RETURN FALSE
		ENDIF
		
		headShotTxd = GET_PEDHEADSHOT_TXD_STRING(myPedHeadshot)
		
		CDEBUG1LN(DEBUG_GOLF,"Head Texture Name is ", headShotTxd)
		
		thisFoursome.playerCore[iPlayerIndex].playerCard.mugShotDic = headShotTxd
		thisFoursome.playerCore[iPlayerIndex].playerCard.mugShotName = headShotTxd
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GOLF_ADD_NETWORK_PLAYERS_TO_UI(TEXT_LABEL_63 &sPlayerNames[], GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], BOOL bSaveNames, BOOL bUseCurrentScores = FALSE)
	INT currentPlayer, iBallColourIndex
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) currentPlayer
		IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, currentPlayer, GOLF_PLAYER_REMOVED)
			IF bSaveNames
				sPlayerNames[currentPlayer] = GET_GOLF_MP_PLAYER_NAME(thisPlayers, currentPlayer)
				CDEBUG1LN(DEBUG_GOLF,"Index, ", currentPlayer, " function ", GET_GOLF_MP_PLAYER_NAME(thisPlayers, currentPlayer), " saved in serverBD ", sPlayerNames[currentPlayer])
			ENDIF
			
			thisFoursome.playerCore[currentPlayer].playerCard.ID = currentPlayer
			thisFoursome.playerCore[currentPlayer].playerCard.sPlayerName = sPlayerNames[currentPlayer]
			thisFoursome.playerCore[currentPlayer].playerCard.crewTag = GET_GOLF_PLAYER_CREW_TAG(NETWORK_GET_PLAYER_INDEX(GET_GOLF_PLAYER_NET_INDEX_BY_INDEX(thisPlayers, currentPlayer)))
			thisFoursome.playerCore[currentPlayer].playerCard.eBallColour = GET_GOLF_PLAYER_HUD_COLOUR(iBallColourIndex)
			iBallColourIndex++
		ENDIF
	ENDREPEAT
	
	INT iScore
	INITIALISE_PLAYERCARD_SCORES(thisFoursome)
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) currentPlayer
		IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, currentPlayer, GOLF_PLAYER_REMOVED)
			iScore = PICK_INT(bUseCurrentScores, GET_GOLF_PLAYER_CURRENT_SCORE(thisPlayers[currentPlayer]), 0)
		
			SET_PLAYERCARD_SLOT(thisHelpers, thisFoursome, thisFoursome.playerCore[currentPlayer].playerCard, iScore, TRUE, FALSE, GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, currentPlayer)  )
			SET_SCOREBOARD_SLOT(thisHelpers, thisCourse, thisFoursome, thisPlayers[currentPlayer], thisFoursome.playerCore[currentPlayer].playerCard, TRUE, bUseCurrentScores)
		ENDIF
	ENDREPEAT

ENDPROC

#ENDIF //golf is ai only
/// PURPOSE:
///    Sets up all clubs in the global bag
/// PARAMS:
///    golfBag - 
PROC GOLF_SETUP_CLUBS( GOLF_BAG &golfBag)
	GOLF_ADD_CLUB(golfBag, eCLUB_WOOD_1)
	GOLF_ADD_CLUB(golfBag, eCLUB_WOOD_3)
	GOLF_ADD_CLUB(golfBag, eCLUB_WOOD_5)
	GOLF_ADD_CLUB(golfBag, eCLUB_IRON_3)
	GOLF_ADD_CLUB(golfBag, eCLUB_IRON_4)
	GOLF_ADD_CLUB(golfBag, eCLUB_IRON_5)
	GOLF_ADD_CLUB(golfBag, eCLUB_IRON_6)
	GOLF_ADD_CLUB(golfBag, eCLUB_IRON_7)
	GOLF_ADD_CLUB(golfBag, eCLUB_IRON_8)
	GOLF_ADD_CLUB(golfBag, eCLUB_IRON_9)
	GOLF_ADD_CLUB(golfBag, eCLUB_WEDGE_PITCH)
	GOLF_ADD_CLUB(golfBag, eCLUB_WEDGE_SAND)
	GOLF_ADD_CLUB(golfBag, eCLUB_WEDGE_LOB)
	GOLF_ADD_CLUB(golfBag, eCLUB_PUTTER)
ENDPROC

PROC GET_GOLF_ANALOG_STICK_VALUES(INT &iLeftX, INT &iLeftY, INT &iRightX, INT &iRightY)

	iLeftX  = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) * 128.0)
	iLeftY  = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) * 128.0)
	
	// More sensitive on PC keyboard and mouse, and using unbound inputs.
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
		iRightX = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) * 400.0)
		iRightY = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) * 400.0)
		// We need to un-invert the PC mouse because unlike the gamepad the values are inverted by code and not script.
		IF IS_MOUSE_LOOK_INVERTED()
			iRightY *= -1
		ENDIF
		
		// Also need to univert if normal look is inverted
		IF IS_LOOK_INVERTED()
			iRightY *= -1
		ENDIF
		
	ELSE
		iRightX = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) * 128.0)
		iRightY = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) * 128.0)
	ENDIF

	//CDEBUG1LN(DEBUG_GOLF,"LeftX ", iLeftX, " LeftY ", iLeftY, " RightX ", iRightX, " RightY ", iRightY)
ENDPROC

