//Golf stat managment
USING "minigames_helpers.sch"
USING "net_stat_system.sch"
USING "socialclub_leaderboard.sch"
USING "net_leaderboards.sch"
USING "achievement_public.sch"

#IF NOT GOLF_IS_AI_ONLY
SC_LEADERBOARD_CONTROL_STRUCT golfLB_control
INT iLocalRoundsPlayed
INT iLocalNumShots
INT iLocalShotsOnHole[9]
BOOL bWonGolfStat
BOOL bUpdateLeaderBoard = FALSE
#ENDIF

FUNC BOOL IS_WON_GOLF_STAT_SET()
	RETURN bWonGolfStat
ENDFUNC

FUNC INT GET_GOLF_STAT_LOWEST_ROUND(INT iIsMP = GOLF_IS_MP)
	INT ret
	
	IF iIsMP != 0
		ret = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iBestRound
	ELSE
		ret = g_savedGlobals.sGolfData.iBestRound
	ENDIF
	
	RETURN ret
ENDFUNC


FUNC FLOAT GET_GOLF_STAT_LONGEST_DRIVE(INT iIsMP = GOLF_IS_MP)
	FLOAT ret
	
	IF iIsMP != 0
		ret = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.fLongestDrive
	ELSE
		ret = g_savedGlobals.sGolfData.fLongestDrive
	ENDIF
	
	RETURN ret
ENDFUNC

FUNC FLOAT GET_GOLF_STAT_LONGEST_PUTT(INT iIsMP = GOLF_IS_MP)
	FLOAT ret
	
	IF iIsMP != 0
		ret =  g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.fLongestPutt
	ELSE
		ret = g_savedGlobals.sGolfData.fLongestPutt
	ENDIF
	
	RETURN ret
ENDFUNC

FUNC INT GET_GOLF_STAT_ROUNDS_PLAYED(INT iIsMP = GOLF_IS_MP)
	INT ret
	
	IF iIsMP != 0
		ret =  g_savedGlobals.sGolfData.iNumRoundsPlayedMP
	ELSE
		ret =  g_savedGlobals.sGolfData.iNumRoundsPlayedSP
	ENDIF
	
	RETURN ret
ENDFUNC

FUNC INT GET_GOLF_STAT_NUM_WINS(INT iIsMP = GOLF_IS_MP)

	IF iIsMP != 0
		RETURN GET_MP_INT_PLAYER_STAT(MPPLY_GOLF_WINS)
	ELSE
		RETURN g_savedGlobals.sGolfData.iNumGolfWins
	ENDIF

ENDFUNC

FUNC INT GET_GOLF_STAT_NUM_SHOTS()
	RETURN iLocalNumShots
ENDFUNC

FUNC FLOAT GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(INT iHole, INT iIsMP = GOLF_IS_MP)
	FLOAT ret
	
	IF iIsMP != 0
		ret =  g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.fLongestDriveHole[iHole]
	ELSE
		ret =  g_savedGlobals.sGolfData.sCourse[0].sHole[iHole].fLongestDriveHole
	ENDIF
	
	RETURN ret
ENDFUNC

FUNC FLOAT GET_GOLF_STAT_LONGEST_PUTT_ON_HOLE(INT iHole, INT iIsMP = GOLF_IS_MP)
	FLOAT ret
	
	IF iIsMP != 0
		ret =  g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.fLongestPuttHole[iHole]
	ELSE
		ret =  g_savedGlobals.sGolfData.sCourse[0].sHole[iHole].fLongestPuttHole
	ENDIF
	
	RETURN ret 
ENDFUNC

FUNC INT GET_GOLF_STAT_SHOTS_ON_HOLE(INT iHole)
	RETURN iLocalShotsOnHole[iHole]
ENDFUNC

FUNC TEXT_LABEL_15 APPEND_YARDS_TO_NUMBER(INT iNum)
	
	TEXT_LABEL_15 txtRet = SC_LEADERBOARD_MAKE_INT_PRETTY(iNum)
	txtRet += " yards"
	
	RETURN txtRet

ENDFUNC

PROC GOLF_WRITE_TO_LEADERBOARD(INT iIsMp = GOLF_IS_MP)
//	SCORE_COLUMN ( AGG_Max ) - InputId: LB_INPUT_COL_SCORE
//	LONGEST_DRIVE ( AGG_Max ) - InputId: LB_INPUT_COL_NUM_DRIVES
//	LONGEST_PUTT ( AGG_Max ) - InputId: LB_INPUT_COL_NUM_PUTT
//	NUM_MATCHES ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_MATCHES
//	NUM_WINS ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_WINS
//	NUM_SHOTS ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_SHOTS_GOLF
//	HOLE_1_DRIVE ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_1_DRIVE
//	HOLE_1_PUTT ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_1_PUTT
//	HOLE_1_SHOTS ( AGG_Sum ) - InputId: LB_INPUT_COL_HOLE_1_SHOTS
//	HOLE_2_DRIVE ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_2_DRIVE
//	HOLE_2_PUTT ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_2_PUTT
//	HOLE_2_SHOTS ( AGG_Sum ) - InputId: LB_INPUT_COL_HOLE_2_SHOTS
//	HOLE_3_DRIVE ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_3_DRIVE
//	HOLE_3_PUTT ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_3_PUTT
//	HOLE_3_SHOTS ( AGG_Sum ) - InputId: LB_INPUT_COL_HOLE_3_SHOTS
//	HOLE_4_DRIVE ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_4_DRIVE
//	HOLE_4_PUTT ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_4_PUTT
//	HOLE_4_SHOTS ( AGG_Sum ) - InputId: LB_INPUT_COL_HOLE_4_SHOTS
//	HOLE_5_DRIVE ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_5_DRIVE
//	HOLE_5_PUTT ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_5_PUTT
//	HOLE_5_SHOTS ( AGG_Sum ) - InputId: LB_INPUT_COL_HOLE_5_SHOTS
//	HOLE_6_DRIVE ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_6_DRIVE
//	HOLE_6_PUTT ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_6_PUTT
//	HOLE_6_SHOTS ( AGG_Sum ) - InputId: LB_INPUT_COL_HOLE_6_SHOTS
//	HOLE_7_DRIVE ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_7_DRIVE
//	HOLE_7_PUTT ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_7_PUTT
//	HOLE_7_SHOTS ( AGG_Sum ) - InputId: LB_INPUT_COL_HOLE_7_SHOTS
//	HOLE_8_DRIVE ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_8_DRIVE
//	HOLE_8_PUTT ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_8_PUTT
//	HOLE_8_SHOTS ( AGG_Sum ) - InputId: LB_INPUT_COL_HOLE_8_SHOTS
//	HOLE_9_DRIVE ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_9_DRIVE
//	HOLE_9_PUTT ( AGG_Max ) - InputId: LB_INPUT_COL_HOLE_9_PUTT
//	HOLE_9_SHOTS ( AGG_Sum ) - InputId: LB_INPUT_COL_HOLE_9_SHOTS
	//	Instances: 3
	//	GameType,Location,Type
	//	GameType,Location,Type,Challenge
	//	GameType,Location,Type,ScEvent
		
	//Why 3 categories? Do we not just want 1 MP or SP??
	TEXT_LABEL_31 categoryNames[1] 
    TEXT_LABEL_23 uniqueIdentifiers[1]
    categoryNames[0] = "GameType" 
    
	// Begin setting the leaderboard data types
	IF iIsMp != 0
		uniqueIdentifiers[0] = "MP"
	ELSE
		uniqueIdentifiers[0] = "SP"
	ENDIF
	
	IF NOT bUpdateLeaderBoard
		CDEBUG1LN(DEBUG_GOLF,"Didn't do anything, don't update leaderboard")
		EXIT
	ENDIF
	
	IF NOT bAllowLeaderboardUpload
		CDEBUG1LN(DEBUG_GOLF, "Disabled leaderboard update")
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_GOLF,"Writing golf stats ", uniqueIdentifiers[0])
	
	IF INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_GOLF, uniqueIdentifiers,categoryNames, 1,-1,TRUE)
	
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_GOLF), LB_INPUT_COL_SCORE, 			-GET_GOLF_STAT_LOWEST_ROUND(),			0.0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_GOLF), LB_INPUT_COL_NUM_DRIVES,  	FLOOR(GET_GOLF_STAT_LONGEST_DRIVE()), 	0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_NUM_PUTT, 		FLOOR(GET_GOLF_STAT_LONGEST_PUTT()),  	0.0)
		
		//Sum data
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_GOLF), LB_INPUT_COL_NUM_MATCHES, 	iLocalRoundsPlayed, 		 			0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_NUM_WINS, 		PICK_INT(IS_WON_GOLF_STAT_SET(), 1, 0), 0.0)
		
		//Hole records
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_1_DRIVE, 	FLOOR(GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(0)), 0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_1_PUTT, 	FLOOR(GET_GOLF_STAT_LONGEST_PUTT_ON_HOLE(0)),  0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_1_SHOTS, 	GET_GOLF_STAT_SHOTS_ON_HOLE(0)				,  0.0)
		
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_2_DRIVE, 	FLOOR(GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(1)), 0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_2_PUTT, 	FLOOR(GET_GOLF_STAT_LONGEST_PUTT_ON_HOLE(1)),  0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_2_SHOTS, 	GET_GOLF_STAT_SHOTS_ON_HOLE(1)				,  0.0)
		
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_3_DRIVE, 	-FLOOR(GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(2)), 0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_3_PUTT, 	FLOOR(GET_GOLF_STAT_LONGEST_PUTT_ON_HOLE(2)),  0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_3_SHOTS, 	GET_GOLF_STAT_SHOTS_ON_HOLE(2)				,  0.0)
		
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_4_DRIVE, 	FLOOR(GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(3)), 0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_4_PUTT, 	FLOOR(GET_GOLF_STAT_LONGEST_PUTT_ON_HOLE(3)),  0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_4_SHOTS, 	GET_GOLF_STAT_SHOTS_ON_HOLE(3)				,  0.0)
		
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_5_DRIVE, 	FLOOR(GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(4)), 0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_HOLE_5_PUTT, 	FLOOR(GET_GOLF_STAT_LONGEST_PUTT_ON_HOLE(4)),  0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_5_SHOTS, 	GET_GOLF_STAT_SHOTS_ON_HOLE(4)				,  0.0)
		
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_6_DRIVE, 	-FLOOR(GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(5)), 0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_6_PUTT, 	FLOOR(GET_GOLF_STAT_LONGEST_PUTT_ON_HOLE(5)),  0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_6_SHOTS, 	GET_GOLF_STAT_SHOTS_ON_HOLE(5)				,  0.0)
		
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_7_DRIVE, 	FLOOR(GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(6)), 0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_7_PUTT, 	FLOOR(GET_GOLF_STAT_LONGEST_PUTT_ON_HOLE(6)),  0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_7_SHOTS, 	GET_GOLF_STAT_SHOTS_ON_HOLE(6)				,  0.0)
		
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_8_DRIVE, 	FLOOR(GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(7)), 0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_8_PUTT, 	FLOOR(GET_GOLF_STAT_LONGEST_PUTT_ON_HOLE(7)),  0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_8_SHOTS, 	GET_GOLF_STAT_SHOTS_ON_HOLE(7)				,  0.0)
		
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_9_DRIVE, 	FLOOR(GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(8)), 0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_9_PUTT, 	FLOOR(GET_GOLF_STAT_LONGEST_PUTT_ON_HOLE(8)),  0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN( LB_INPUT_COL_HOLE_9_SHOTS, 	GET_GOLF_STAT_SHOTS_ON_HOLE(8)				,  0.0)
	ENDIF
	
	bUpdateLeaderBoard = FALSE // won't update again unless more stats are made
	
	//Reset sum data
	iLocalNumShots = 0
	bWonGolfStat = FALSE
	iLocalRoundsPlayed = 0
	INT inc
	REPEAT COUNT_OF(iLocalShotsOnHole) inc
		iLocalShotsOnHole[inc] = 0
	ENDREPEAT
	
ENDPROC


PROC GOLF_LOAD_SOCIAL_CLUB_LEADERBOARD()
#IF NOT GOLF_IS_AI_ONLY
	//TEXT_LABEL_23 sPlayerName = GET_PLAYER_NAME(PLAYER_ID())
	#IF GOLF_IS_MP
		SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(golfLB_control,FMMC_TYPE_MG_GOLF,"","")
	#ENDIF
	
	#IF NOT GOLF_IS_MP
		SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(golfLB_control,FMMC_TYPE_MG_GOLF_SP,"","")
	#ENDIF
#ENDIF
ENDPROC

FUNC BOOL GOLF_UPDATE_SOCIAL_CLUB_LEADERBOARD(BOOL bFastWrite)
	//IF bAllowLeaderboardUpload
	IF GOLF_CAN_DISPLAY_SOCAIL_CLUB_LEADERBOARD() //make sure player is online
		IF bFastWrite
			GOLF_WRITE_TO_LEADERBOARD()
			RETURN TRUE
		ENDIF
	
		CDEBUG1LN(DEBUG_GOLF, "Starting golf leaderboat update")
		GOLF_LOAD_SOCIAL_CLUB_LEADERBOARD()
	
		IF scLB_rank_predict.bFinishedRead AND NOT scLB_rank_predict.bFinishedWrite
			CDEBUG1LN(DEBUG_GOLF, "Read finished, write to leaderboard")
			GOLF_WRITE_TO_LEADERBOARD()
			scLB_rank_predict.bFinishedWrite = TRUE
		ENDIF
		IF GET_RANK_PREDICTION_DETAILS(golfLB_control)
			CDEBUG1LN(DEBUG_GOLF, "Prediction ready, set use prediction flag")
			sclb_useRankPrediction = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GOLF_DISPLAY_SOCIAL_CLUB_LEADERBOARD(#IF NOT GOLF_IS_AI_ONLY SCALEFORM_INDEX& scaleFormID #ENDIF)
#IF NOT GOLF_IS_AI_ONLY
 	GOLF_LOAD_SOCIAL_CLUB_LEADERBOARD()
	DRAW_SC_SCALEFORM_LEADERBOARD(scaleFormID,golfLB_control)
#ENDIF
ENDPROC

PROC SET_GOLF_STAT_LONGEST_DRIVE_FOR_HOLE(INT iHole, FLOAT fDist, INT iIsMP = GOLF_IS_MP)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_GOLF,"Setting longest drive on hole ", iHole, " to ", fDist)
	IF iIsMP = 0
		g_savedGlobals.sGolfData.sCourse[0].sHole[iHole].fLongestDriveHole = fDist
	ELSE
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.fLongestDriveHole[iHole] = fDist
	ENDIF
	bUpdateLeaderBoard = TRUE
ENDPROC

PROC SET_GOLF_STAT_LONGEST_PUTT_FOR_HOLE(INT iHole, FLOAT fDist, INT iIsMP = GOLF_IS_MP)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	IF iIsMP = 0
		g_savedGlobals.sGolfData.sCourse[0].sHole[iHole].fLongestPuttHole = fDist
	ELSE
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.fLongestPuttHole[iHole] = fDist
	ENDIF
	bUpdateLeaderBoard = TRUE
ENDPROC

#IF NOT GOLF_IS_AI_ONLY
PROC INC_GOLF_STAT_NUM_SHOTS(INT iHole)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	iLocalNumShots++
	iLocalShotsOnHole[iHole]++
	bUpdateLeaderBoard = TRUE
ENDPROC
#ENDIF

PROC INC_GOLF_STAT_NUM_BIRDIES(INT iIsMP = GOLF_IS_MP)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	IF iIsMP != 0
		CDEBUG1LN(DEBUG_GOLF,"Inc birdie stat")
		INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_GOLF_BIRDIES, 1)
	ENDIF
	bUpdateLeaderBoard = TRUE
ENDPROC

PROC SET_GOLF_STAT_GOT_HOLE_IN_ONE(INT iIsMP = GOLF_IS_MP)

	REQUEST_SYSTEM_ACTIVITY_TYPE_HOLE_IN_ONE()
	
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	IF iIsMP != 0
		CDEBUG1LN(DEBUG_GOLF,"Set hole in one stat")
		SET_MP_BOOL_CHARACTER_AWARD( MP_AWARD_FM_GOLF_HOLE_IN_1, TRUE)
	ENDIF
	IF NETWORK_IS_GAME_IN_PROGRESS()
		INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_GOLF_HOLE_IN_ONES)
	ENDIF
	bUpdateLeaderBoard = TRUE
ENDPROC

PROC INC_GOLF_STAT_NUM_WINS(INT iIsMP = GOLF_IS_MP)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	IF iIsMP != 0
		INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_GOLF_WON, 1)
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_GOLF_WINS)
		SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_GOLF)
		//CHECK_FRANKIE_SAYS_ACHIEVEMENT()
	ELSE
		g_savedGlobals.sGolfData.iNumGolfWins++
	ENDIF
	bWonGolfStat = TRUE
	bUpdateLeaderBoard = TRUE
ENDPROC

PROC INC_GOLF_STAT_NUM_LOSSES(INT iIsMP = GOLF_IS_MP)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	IF iIsMP != 0
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_GOLF_LOSSES)
		SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_GOLF)
		//CHECK_FRANKIE_SAYS_ACHIEVEMENT()
	ENDIF

ENDPROC


//set the lowest score stat
PROC SET_GOLF_STAT_LOWEST_ROUND(INT roundScore, INT iIsMP = GOLF_IS_MP)
	IF bAllowLeaderboardUpload
		IF iIsMP = 0
			g_savedGlobals.sGolfData.iBestRound = roundScore
		ELSE
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iBestRound = roundScore
		ENDIF
		bUpdateLeaderBoard = TRUE
	ENDIF
ENDPROC

//set roudns played stat
PROC SET_GOLF_STAT_ROUNDS_PLAYED(INT roundsPlayed, INT iIsMP = GOLF_IS_MP)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	IF iIsMP != 0
		g_savedGlobals.sGolfData.iNumRoundsPlayedMP = roundsPlayed
		SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_GOLF)
	ELSE
		g_savedGlobals.sGolfData.iNumRoundsPlayedSP = roundsPlayed
	ENDIF
	
	bUpdateLeaderBoard = TRUE
ENDPROC

PROC INC_GOLF_STAT_ROUND_PLAYED_WITH_CHARACTER()
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		g_savedGlobals.sGolfData.iNumRoundsWithCharacter[0]++
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		g_savedGlobals.sGolfData.iNumRoundsWithCharacter[1]++
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		g_savedGlobals.sGolfData.iNumRoundsWithCharacter[2]++
	ENDIF
	iLocalRoundsPlayed++
	bUpdateLeaderBoard = TRUE
ENDPROC

//sets successful fairway drives per hole
PROC SET_GOLF_STAT_FAIRWAY_DRIVE_PERCENT(FLOAT drivePercent)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	g_savedGlobals.sGolfData.fAverageFairwayDrives = drivePercent
ENDPROC

FUNC FLOAT GET_GOLF_STAT_FAIRWAY_DRIVE_PERCENT()
	FLOAT ret
	ret = g_savedGlobals.sGolfData.fAverageFairwayDrives
	RETURN ret
ENDFUNC

//set number of putts per hole
PROC SET_GOLF_STAT_AVERAGE_PUTTS(FLOAT puttsPerHole)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF

	g_savedGlobals.sGolfData.fAveragePutts = puttsPerHole
ENDPROC

FUNC FLOAT GET_GOLF_STAT_AVERAGE_PUTTS()
	FLOAT ret
	ret = g_savedGlobals.sGolfData.fAveragePutts 
	RETURN ret
ENDFUNC

//set longest putt that ended in the cup
PROC SET_GOLF_STAT_LONGEST_PUTT(FLOAT longestPutt, INT iIsMP = GOLF_IS_MP)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	IF iIsMP = 0
		g_savedGlobals.sGolfData.fLongestPutt = longestPutt
	ELSE
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.fLongestPutt = longestPutt
	ENDIF
	bUpdateLeaderBoard = TRUE
ENDPROC

//sets longest tee shot that landed on the fairway
PROC SET_GOLF_STAT_LONGEST_DRIVE(FLOAT longestDrive, INT iIsMP = GOLF_IS_MP)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	IF iIsMP = 0
		g_savedGlobals.sGolfData.fLongestDrive = longestDrive
	ELSE
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.fLongestDrive = longestDrive
	ENDIF
	bUpdateLeaderBoard = TRUE
ENDPROC

//Some stats should only be update at the end of the hole
PROC GOLF_UPDATE_END_OF_HOLE_STATS(BOOL updateDrivePercent, BOOL updatePuttPercent)
	IF NOT bAllowLeaderboardUpload
		EXIT
	ENDIF
	
	g_savedGlobals.sGolfData.iNumHolesPlayed++
	IF updateDrivePercent
		g_savedGlobals.sGolfData.iNumFairwayDrivesTotal++
		SET_GOLF_STAT_FAIRWAY_DRIVE_PERCENT(TO_FLOAT(g_savedGlobals.sGolfData.iNumFairwayDrivesTotal)/TO_FLOAT(g_savedGlobals.sGolfData.iNumHolesPlayed))
	ENDIF
	
	IF updatePuttPercent
		SET_GOLF_STAT_AVERAGE_PUTTS(TO_FLOAT(g_savedGlobals.sGolfData.iNumPuttsTotal)/TO_FLOAT(g_savedGlobals.sGolfData.iNumHolesPlayed))
	ENDIF
ENDPROC

PROC INITAILIZE_GOLF_STATS()
	IF GET_GOLF_STAT_LOWEST_ROUND() < 9 //It's impossible to get a score this low, dirty cheaters
		SET_GOLF_STAT_LOWEST_ROUND(82) //set to worst possible score
	ENDIF
	IF GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(2) = 0 //on par 3, we are tracking closest to pin
		SET_GOLF_STAT_LONGEST_DRIVE_FOR_HOLE(2, 999.0)
	ENDIF
	IF GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(5) = 0 //on par 3, we are tracking closest to pin
		SET_GOLF_STAT_LONGEST_DRIVE_FOR_HOLE(5, 999.0)
	ENDIF
ENDPROC
