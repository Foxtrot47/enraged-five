USING "golf_mp.sch"
USING "golf_core.sch"
USING "golf_mp_client.sch"
USING "golf_mp_support.sch"


PROC CANCEL_GOLF_MP_TIMER(GOLF_MP_SERVER_BROADCAST_DATA &serverBD)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF serverBD.iMPTimer != 0
			CDEBUG1LN(DEBUG_GOLF,"Cancel mp timer")
			serverBD.iMPTimer = 0
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(INT piPlayer)
	
	IF piPlayer > -1
		RETURN NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(piPlayer))
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC INT GET_REMANING_GOLF_MP_TIME(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, INT iStartAmount, BOOL bStartClock = TRUE)
	INT iTimer = iStartAmount
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF serverBD.iMPTimer = 0 AND bStartClock
			CDEBUG1LN(DEBUG_GOLF,"Starting mo timer")
			serverBD.iMPTimer = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
		ENDIF
	ENDIF
	IF serverBD.iMPTimer > 0
		iTimer = CEIL((iStartAmount- ((NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) - serverBD.iMPTimer)/1000.0)))
	ENDIF
	
	IF iTimer < 0
		iTimer = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bStopMPTimer
			iTimer = iStartAmount
		ENDIF
	#ENDIF
	
	RETURN iTimer
ENDFUNC

PROC SYNC_WIND_FOR_HOLE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_GAME &thisGame)
	thisGame.fWindStrength = serverBD.fWindStrength
	thisGame.vWindDirection = serverBD.vWindDirection
ENDPROC

PROC SERVER_GET_WIND_FOR_HOLE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_GAME &thisGame, GOLF_FOURSOME& thisFoursome, INT iWeather)
	GRAB_WIND_FOR_HOLE(thisGame, thisFoursome, iWeather)
	
	serverBD.fWindStrength = thisGame.fWindStrength
	serverBD.vWindDirection = thisGame.vWindDirection
ENDPROC

FUNC BOOL SHOULD_GOLFER_BE_INVISIBLE(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_HELPERS &thisHelpers, GOLF_FOURSOME &thisFoursome, INT iPlayerIndex, INT piIndex)

	UNUSED_PARAMETER(thisFoursome)
	
	PARTICIPANT_INDEX partIndex = INT_TO_PARTICIPANTINDEX(piIndex)
	//Spectators are always invisible
	IF NETWORK_IS_PARTICIPANT_ACTIVE(partIndex)
		IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(partIndex))
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayerIndex))
		VECTOR vGolferPos = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayerIndex))
		
		IF IS_GOLF_FOURSOME_STATE_AT_END(thisFoursome)
		AND IS_GOLF_CAM_INTERPOLATING(thisHelpers.camNavigate2, TRUE)
			RETURN TRUE
		ENDIF
		
		BOOL bHasPlayerbeenPorted = IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, piIndex, GCF2_PLAYER_HAS_TELEPORTED)
		
		IF IS_GOLF_MP_APPROACH_CAM_RENDERING(thisHelpers)			
			IF NOT bHasPlayerbeenPorted
				thisHelpers.bKeepPlayerInvisible[iPlayerIndex] = TRUE //player is invisible until they port
			ELIF thisHelpers.bKeepPlayerInvisible[iPlayerIndex] // player is currenly invisible
				IF NOT IS_SPHERE_VISIBLE(vGolferPos, 1.0) //they are not on screen, okay to turn visible
				OR (iPlayerIndex = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) )
					thisHelpers.bKeepPlayerInvisible[iPlayerIndex] = FALSE
				ENDIF
			ENDIF
				
			RETURN thisHelpers.bKeepPlayerInvisible[iPlayerIndex]
		ELSE
			IF iPlayerIndex = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) //current player always visible
				thisHelpers.bKeepPlayerInvisible[iPlayerIndex] = FALSE
			ENDIF
		
			RETURN thisHelpers.bKeepPlayerInvisible[iPlayerIndex]
		ENDIF
		
	ENDIF
	
	thisHelpers.bKeepPlayerInvisible[iPlayerIndex] = FALSE
	RETURN FALSE
ENDFUNC

PROC MANAGE_GOLFER_INVISIBLE(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[], BOOL bForceInvis = FALSE)

	INT currentPlayer
	REPEAT COUNT_OF(playerBD) currentPlayer
	
		INT piIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, currentPlayer)
		
		IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piIndex)
			IF SHOULD_GOLFER_BE_INVISIBLE(playerBD, thisHelpers, thisFoursome, currentPlayer, piIndex)
			OR bForceInvis
				//CDEBUG1LN(DEBUG_GOLF,"Turn player ", currentPlayer, " invisible")
				SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(piIndex)))
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_GOLF_MP_ALL_CLIENTS_AT_GOLF_STATE(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_PLAYER &thisPlayers[], GOLF_FOURSOME &thisFoursome, GOLF_MINIGAME_STATE gmState)
	
	INT currentPlayer
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) currentPlayer
		INT piPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, currentPlayer)
		
		CDEBUG1LN(DEBUG_GOLF,"Checking for player ", piPlayer)
		IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piPlayer)
			CDEBUG1LN(DEBUG_GOLF,"Player ", piPlayer, " is active, state is ", GET_GOLF_MP_CLIENT_GOLF_STATE(playerBD, piPlayer) , " checking against ", gmState)
			IF GET_GOLF_MP_CLIENT_GOLF_STATE(playerBD, piPlayer) != gmState
			AND NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, currentPlayer, GOLF_PLAYER_REMOVED)
				CDEBUG1LN(DEBUG_GOLF,"That player isn't at golf state")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, currentPlayer, GOLF_PLAYER_REMOVED)
			CDEBUG1LN(DEBUG_GOLF,"player was marked as removed and not checked for state sync")
		ENDIF
	ENDREPEAT

	CDEBUG1LN(DEBUG_GOLF,"All Players at golf state ", gmState)
	RETURN TRUE
ENDFUNC

FUNC INT GET_NUM_GOLF_CLIENTS_WITH_CONTROL_FLAG_SET(GOLF_FOURSOME &thisFoursome, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_PLAYER &thisPlayers[],  GOLF_CONTROL_FLAGS gcFlag)
	
	INT iPlayerIndex
	INT iNumSet
	BOOL bIsSet
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		
		INT piIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, iPlayerIndex)
		
		IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piIndex)
			
			bIsSet = IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, piIndex, gcFlag)
			
			IF bIsSet
				iNumSet++
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iNumSet
ENDFUNC

FUNC BOOL ARE_ALL_GOLFER_SPECTATORS(GOLF_FOURSOME &thisFoursome, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_PLAYER &thisPlayers[])
	INT iPlayerIndex
	
	UNUSED_PARAMETER(playerBD)
	
	REPEAT COUNT_OF(playerBD) iPlayerIndex
	
		INT piIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, iPlayerIndex)
		IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piIndex)
			IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED) //and player isn't a spectator
				//	CDEBUG1LN(DEBUG_GOLF,"Participant ", piIndex, " does not have flag ", gcFlag2, " set")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	CDEBUG1LN(DEBUG_GOLF,"Everyone is a spectator or not in game")
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ALL_GOLF_CLIENT_CONTROL_FLAGS_SET2(GOLF_FOURSOME &thisFoursome, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_PLAYER &thisPlayers[], 
											    GOLF_CONTROL_FLAGS2 gcFlag2 = GCF2_NO_FLAG)
	INT iPlayerIndex
	UNUSED_PARAMETER(thisFoursome)
	
	REPEAT COUNT_OF(playerBD) iPlayerIndex
	
		INT piIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, iPlayerIndex)
		IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piIndex)
			IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, piIndex, gcFlag2)
				//	CDEBUG1LN(DEBUG_GOLF,"Participant ", piIndex, " does not have flag ", gcFlag2, " set")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ALL_GOLF_CLIENT_CONTROL_FLAGS_SET(GOLF_FOURSOME &thisFoursome, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_PLAYER &thisPlayers[], 
											   GOLF_CONTROL_FLAGS gcFlag)
	INT iPlayerIndex
	UNUSED_PARAMETER(thisFoursome)
	
	REPEAT COUNT_OF(playerBD) iPlayerIndex
	
		INT piIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, iPlayerIndex)
		IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piIndex)
			IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, piIndex, gcFlag)
				//	CDEBUG1LN(DEBUG_GOLF,"Participant ", piIndex, " does not have flag ", gcFlag, " set")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ANY_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(GOLF_FOURSOME &thisFoursome, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_PLAYER &thisPlayers[], GOLF_CONTROL_FLAGS2 gcFlag)

	INT iPlayerIndex
	
	UNUSED_PARAMETER(thisFoursome)
	
	REPEAT COUNT_OF(playerBD) iPlayerIndex
		
		INT piIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, iPlayerIndex)
		IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piIndex)
			IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, piIndex, gcFlag)
				CDEBUG1LN(DEBUG_GOLF,"Player ", iPlayerIndex, " has flag2 set")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_ANY_GOLF_MP_CLIENT_CONTROL_FLAG_SET(GOLF_FOURSOME &thisFoursome, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_PLAYER &thisPlayers[], GOLF_CONTROL_FLAGS gcFlag)

	INT iPlayerIndex
	
	UNUSED_PARAMETER(thisFoursome)
	
	REPEAT COUNT_OF(playerBD) iPlayerIndex
		
		INT piIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, iPlayerIndex)
		IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piIndex)
			IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, piIndex, gcFlag)
				CDEBUG1LN(DEBUG_GOLF,"Player ", iPlayerIndex, " has flag set")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_ANY_GOLF_MP_CLIENT_AT_PLAYER_STATE(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_PLAYER &thisPlayers[], GOLF_FOURSOME &thisFoursome, GOLF_PLAYER_STATE gmState)
	INT currentPlayer

	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) currentPlayer
		INT iNetIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, currentPlayer)
		
		CDEBUG1LN(DEBUG_GOLF, "Checking for player ", iNetIndex)
		IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(iNetIndex)
			CDEBUG1LN(DEBUG_GOLF, "Player ", iNetIndex, " is active, shot state is ", GET_GOLF_MP_CLIENT_PLAYER_STATE(playerBD, iNetIndex) , " checking against ", gmState)
			IF GET_GOLF_MP_CLIENT_PLAYER_STATE(playerBD, iNetIndex) = gmState
			AND NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, currentPlayer, GOLF_PLAYER_REMOVED)
				CDEBUG1LN(DEBUG_GOLF, "That player at shot state")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC GOLF_MP_TELEPORT_TO_END_GAME_LOCATION(GOLF_FOURSOME &thisFoursome)
	
	INT iPlayerIndex = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
	VECTOR vTeleportLoc
	FLOAT fHeading
	
	SWITCH iPlayerIndex
		CASE 0
			vTeleportLoc = <<-1377.9247, 65.5731, 52.6963>>
			fHeading = 257.0720
		BREAK
		CASE 1
			vTeleportLoc = <<-1378.2631, 63.0162, 52.6943>>
			fHeading = 259.0627
		BREAK
		CASE 2
			vTeleportLoc = <<-1378.1993, 48.6754, 52.6868>>
			fHeading = 263.1791
		BREAK
		CASE 3
			vTeleportLoc = <<-1377.7411, 45.1968, 52.6852>>
			fHeading = 293.7572
		BREAK
	ENDSWITCH
	
	//NET_WARP_TO_COORD(vTeleportLoc, fHeading, FALSE, FALSE)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vTeleportLoc)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
	ENDIF									
ENDPROC

PROC GOLF_MP_SET_PLAYERCARDS(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], GOLF_HELPERS &thisHelpers, BOOL bUseNextHolePlayercard, BOOL bUseNextHoleScorecard)

	INT iNetPlayerIndex
	
	UNUSED_PARAMETER(playerBD)
	
	REPEAT COUNT_OF(playerBD) iNetPlayerIndex
		REMOVE_PLAYER(thisHelpers.golfUI, iNetPlayerIndex)
	ENDREPEAT
	
	REPEAT COUNT_OF(playerBD) iNetPlayerIndex
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX( iNetPlayerIndex))
			INT iPlayerIndex = GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, iNetPlayerIndex)
			IF iPlayerIndex > -1
				IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
					SET_PLAYERCARD_SLOT(thisHelpers, thisFoursome, thisFoursome.playerCore[iPlayerIndex].playerCard, GET_GOLF_PLAYER_CURRENT_SCORE(thisPlayers[iPlayerIndex]), TRUE, bUseNextHolePlayercard, iNetPlayerIndex)
					SET_SCOREBOARD_SLOT(thisHelpers, thisCourse, thisFoursome, thisPlayers[iPlayerIndex], thisFoursome.playerCore[iPlayerIndex].playerCard, TRUE, bUseNextHoleScorecard)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC GOLF_MP_CLIENT_MOVE_TO_NEXT_HOLE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], GOLF_HELPERS &thisHelpers, GOLF_GAME &thisGame)
	CDEBUG1LN(DEBUG_GOLF,"Got message from server to move to next hole, we are going to go to the scorecard!")
		INT iNetPlayerIndex
	// Sync server total and hole score
		REPEAT COUNT_OF(playerBD) iNetPlayerIndex
			CLEANUP_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(thisFoursome, iNetPlayerIndex)
		
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX( iNetPlayerIndex))
				INT iPlayerIndex = GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, iNetPlayerIndex)
				INT iCurrentHole = GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)
				IF iPlayerIndex > -1
					SET_GOLF_PLAYER_HOLE_SCORE_BY_INDEX(thisPlayers, iPlayerIndex, iCurrentHole, GET_GOLF_MP_SERVER_PLAYER_HOLE_SCORE(serverBD, iNetPlayerIndex, iCurrentHole))	
					SET_GOLF_PLAYER_CURRENT_SCORE_BY_INDEX(thisPlayers,iPlayerIndex, GET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(serverBD, iNetPlayerIndex))
					SET_PLAYERCARD_SCORES(thisFoursome, thisHelpers, iPlayerIndex, GET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(serverBD, iNetPlayerIndex), TRUE)
				ENDIF
			ENDIF
		ENDREPEAT

	BOOL bEndGame = GOLF_MOVE_TO_NEXT_HOLE(thisGame, thisCourse, thisFoursome, thisHelpers.iEndingHole, thisPlayers, TRUE)

	SET_END_OF_HOLE_STREAMVOL(thisHelpers, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))	
	
	IF NOT bEndGame
		SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_SCORECARD_END_HOLE)
		SET_END_OF_HOLE_CAMERA(thisHelpers, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) - 1)
		GOLF_SETUP_SCOREBOARD_KEYS(thisHelpers, TRUE, TRUE, FALSE, FALSE, FALSE)
	ELSE
		SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_SCORECARD_END_GAME)
		SET_END_OF_HOLE_CAMERA(thisHelpers, 7, TRUE, TRUE)
//		IF NOT thisGame.bPlayoffGame
//			GOLF_MP_TELEPORT_TO_END_GAME_LOCATION(thisFoursome)
//		ENDIF
		GOLF_SETUP_SCOREBOARD_KEYS(thisHelpers, TRUE, FALSE, FALSE, FALSE, TRUE)
	ENDIF
	
	CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_SOCIAL_CLUB_BOARD_DISPLAYED)
	
	
//	CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_END_GOLF_SCOREBOARD)
//	DISPLAY_SCOREBOARD(thisHelpers, TRUE)
	
	CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_SHOT | GCF_PLAYER_DONE_WITH_HOLE | GCF_BALL_JUST_HIT)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		CLEAR_GOLF_MP_SERVER_CONTROL_FLAG(serverBD, GCF_PLAYER_DONE_WITH_HOLE)
	ENDIF
	
	SET_GOLF_FOURSOME_LOCAL_PLAYER_STATE(thisFoursome, GPS_PLACE_BALL)
	GOLF_CLEANUP_CURRENT_HOLE(thisCourse, thisFoursome)
	
	IF IS_PAUSE_MENU_ACTIVE()
		SET_FRONTEND_ACTIVE(FALSE)
	ENDIF
	
	SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE)
	//GOLF_MOVE_TO_NEXT_HOLE returns if the game should end
	IF NOT bEndGame
	OR thisGame.bPlayoffGame
		
		CDEBUG1LN(DEBUG_GOLF,"Plyoff game or not the end")
		GOLF_INIT_CURRENT_HOLE(thisCourse, thisFoursome)
		GOLF_FOURSOME_DELETE_ALL_BALLS(thisFoursome)
		
		SET_GOLF_FOURSOME_CURRENT_PLAYER( thisFoursome, GET_PLAYER_INDEX_FROM_NET_INDEX( thisPlayers, GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD)))
		SET_GOLF_MP_CLIENT_LOCAL_AIM (playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_AIM( thisFoursome ))
		SET_GOLF_MP_CLIENT_LOCAL_PLAYER_STATE (playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_STATE (thisFoursome )) 
		SET_GOLF_MP_CLIENT_LOCAL_BALL_POSITION (playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL_POSITION( thisFoursome ))
		SET_GOLF_MP_CLIENT_LOCAL_DISTANCE_TO_HOLE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_DISTANCE_TO_HOLE(thisFoursome))
		SET_GOLF_MP_CLIENT_LOCAL_LIE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_LIE(thisFoursome))
		SET_GOLF_MP_CLIENT_LOCAL_SHOTS_ON_CURRENT_HOLE(playerBD, 0)
		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL (thisFoursome))
			SET_ENTITY_COORDS(GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL (thisFoursome), GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL_POSITION(thisFoursome))
		ENDIF

		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
		CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_INPUT)
		RESET_GOLF_EFFECTS_FLAGS(playerBD)
		CANCEL_TIMER(thisHelpers.navTimer)
		CANCEL_GOLF_MP_TIMER(serverBD)
		RESET_GOLF_MP_APPROACH_CAM(thisHelpers)
	ELSE
		
	ENDIF
	
	GOLF_MP_SET_PLAYERCARDS(playerBD, thisCourse, thisFoursome, thisPlayers, thisHelpers, bEndGame, TRUE)
ENDPROC


PROC GOLF_MP_CLIENT_MOVE_TO_NEXT_SHOOTER(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[])
	CDEBUG1LN(DEBUG_GOLF,"Got message from server to move to next shooter!")
	
	INT iNewCurrentPlayer = GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD))
	IF iNewCurrentPlayer < 0
		CDEBUG1LN(DEBUG_GOLF,"Current player is less than 0?")
		EXIT
	ENDIF
	
	SET_GOLF_FOURSOME_CURRENT_PLAYER (thisFoursome, iNewCurrentPlayer)
	
	INT iNetPlayerIndex //make sure scores are synced
	INT iScore[MP_GOLF_MAX_PLAYERS]
	BOOL bNextHole[MP_GOLF_MAX_PLAYERS]
	REPEAT COUNT_OF(playerBD) iNetPlayerIndex
//		CLEANUP_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(thisFoursome, iNetPlayerIndex) //with this uncommented you can see the club disapear on reactions animations
	
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX( iNetPlayerIndex))
			INT iPlayerIndex = GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, iNetPlayerIndex)
			
			IF iPlayerIndex > -1
				IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
					CDEBUG1LN(DEBUG_GOLF,"Updaing player card of player ", iPlayerIndex)
					IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, iNetPlayerIndex, GCF_PLAYER_DONE_WITH_HOLE)
					//sometimes the server can send his sever data before his broadcast data
					OR ( NETWORK_GET_HOST_OF_THIS_SCRIPT() = INT_TO_PARTICIPANTINDEX(iNetPlayerIndex) 
						AND IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_PLAYER_DONE_WITH_HOLE)
						AND INT_TO_PARTICIPANTINDEX(iNetPlayerIndex) != PARTICIPANT_ID() ) //the host data will always be synced on it's own script
						
						CDEBUG1LN(DEBUG_GOLF,"Player is done with hole, update score on playercard")
						iScore[iNetPlayerIndex] = GET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(serverBD, iNetPlayerIndex) + GET_GOLF_MP_CLIENT_SHOTS_ON_CURRENT_HOLE(playerBD, iNetPlayerIndex)
						bNextHole[iNetPlayerIndex] = TRUE
						
						IF NETWORK_GET_HOST_OF_THIS_SCRIPT() = INT_TO_PARTICIPANTINDEX(iNetPlayerIndex) 
						AND IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_PLAYER_DONE_WITH_HOLE)
						AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, iNetPlayerIndex, GCF_PLAYER_DONE_WITH_HOLE)
							CDEBUG1LN(DEBUG_GOLF,"Server sent it's serverBD but not it's playerBD")
							iScore[iNetPlayerIndex]++
						ENDIF
					ELSE
						iScore[iNetPlayerIndex] = GET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(serverBD, iNetPlayerIndex)
						bNextHole[iNetPlayerIndex] = FALSE
					ENDIF
					
					SET_PLAYERCARD_SCORES(thisFoursome, thisHelpers, iPlayerIndex, iScore[iNetPlayerIndex], bNextHole[iNetPlayerIndex])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(playerBD) iNetPlayerIndex
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX( iNetPlayerIndex))
			INT iPlayerIndex = GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, iNetPlayerIndex)
			
			IF iPlayerIndex > -1
				IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
					SET_PLAYERCARD_SLOT(thisHelpers, thisFoursome, thisFoursome.playerCore[iPlayerIndex].playerCard, iScore[iNetPlayerIndex], TRUE, bNextHole[iNetPlayerIndex], iNetPlayerIndex)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	IF IS_TEE_SHOT(thisCourse, thisFoursome)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_PLACE_BALL)
	ELSE
		SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_APPROACH_BALL)
	ENDIF
	
	MANAGE_GOLF_BALL_BLIPS(thisFoursome, thisHelpers)
	SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
	CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_INPUT)
	RESET_GOLF_EFFECTS_FLAGS(playerBD)
	CANCEL_TIMER(thisHelpers.navTimer)
	CANCEL_GOLF_MP_TIMER(serverBD)
ENDPROC

PROC GOLF_MP_CLIENT_MOVE_TO_NEXT_SHOT(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_HELPERS &thisHelpers, GOLF_FOURSOME &thisFoursome)
	CDEBUG1LN(DEBUG_GOLF,"Got message from server to move to next shot!")
	CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_SHOT)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome, GPS_APPROACH_BALL)
	SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
	CANCEL_TIMER(thisHelpers.navTimer)
	CANCEL_GOLF_MP_TIMER(serverBD)
ENDPROC


PROC GOLF_MP_CLIENT_UPDATE_LOCAL_PLAYER_SHOT(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_HELPERS &thisHelpers, GOLF_FOURSOME &thisFoursome, GOLF_GAME &thisGame)
	
	IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_BALL_JUST_HIT) AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_BALL_JUST_HIT)
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_BALL_JUST_HIT)
	ENDIF
	
	IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_BALL_JUST_LAND)
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_BALL_JUST_LAND)
	ELSE
		CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD,PARTICIPANT_ID_TO_INT(), GCF_BALL_JUST_LAND)
	ENDIF
	
	IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_BALL_IN_CUP_SOUND) AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_BALL_IN_CUP_SOUND)
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_BALL_IN_CUP_SOUND)
	ENDIF
	
	IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_SHOT) 
	AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_DONE_WITH_SHOT
		CDEBUG1LN(DEBUG_GOLF,"GOT LOCAL SHOT OVER, CLEARING AND SENDING TO SERVER")
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD,PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_SHOT)
		CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD,PARTICIPANT_ID_TO_INT(), GCF_BALL_JUST_HIT)
		CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD,PARTICIPANT_ID_TO_INT(), GCF_BALL_JUST_LAND)
		CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD,PARTICIPANT_ID_TO_INT(), GCF_BALL_IN_CUP_SOUND)
		SET_GOLF_MP_CLIENT_LOCAL_BALL_POSITION(playerBD,  GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL_POSITION (thisFoursome))
		SET_GOLF_MP_CLIENT_LOCAL_DISTANCE_TO_HOLE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_DISTANCE_TO_HOLE(thisFoursome))
		SET_GOLF_MP_CLIENT_LOCAL_LIE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_LIE(thisFoursome))
		SET_GOLF_MP_CLIENT_LOCAL_SHOTS_ON_CURRENT_HOLE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_SHOTS_ON_HOLE(thisFoursome))
		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
	    CDEBUG1LN(DEBUG_GOLF,"Local player done with shot")
	ENDIF

	IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD,PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_HOLE) AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_DONE_WITH_HOLE
		CDEBUG1LN(DEBUG_GOLF,"GOT LOCAL HOLE OVER, CLEARING AND SENDING TO SERVER")
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD,PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_HOLE | GCF_PLAYER_DONE_WITH_SHOT)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SET_GOLF_MP_SERVER_CONTROL_FLAG(serverBD, GCF_PLAYER_DONE_WITH_HOLE)
		ENDIF
		
		CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD,PARTICIPANT_ID_TO_INT(), GCF_BALL_JUST_HIT)
		CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD,PARTICIPANT_ID_TO_INT(), GCF_BALL_JUST_LAND)
		CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD,PARTICIPANT_ID_TO_INT(), GCF_BALL_IN_CUP_SOUND)
		SET_GOLF_MP_CLIENT_LOCAL_BALL_POSITION(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL_POSITION (thisFoursome))
		SET_GOLF_MP_CLIENT_LOCAL_DISTANCE_TO_HOLE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_DISTANCE_TO_HOLE(thisFoursome))
		SET_GOLF_MP_CLIENT_LOCAL_LIE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_LIE(thisFoursome))
		SET_GOLF_MP_CLIENT_LOCAL_SHOTS_ON_CURRENT_HOLE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_SHOTS_ON_HOLE(thisFoursome))
		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
	//CDEBUG1LN(DEBUG_GOLF,"New ball position: ", GET_GOLF_MP_CLIENT_BALL_POSITION (playerBD, PARTICIPANT_ID()))
	ENDIF

ENDPROC

PROC GOLF_MP_SYNC_HEADSETS(GOLF_HEADSET_STATE &golfHeadsetStates[], GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[])
	
	INT iNetIndex
	
	REPEAT COUNT_OF(golfHeadsetStates) iNetIndex
		
		PARTICIPANT_INDEX partIndex = INT_TO_PARTICIPANTINDEX(iNetIndex)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(partIndex)
			IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(partIndex))
				GAMER_HANDLE gamer = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(partIndex))
				
				INT iPlayerIndex = GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, iNetIndex)
				IF iPlayerIndex > -1
					INT iPlayerCardIndex = thisFoursome.playerCore[iPlayerIndex].playerCard.iCurrentSlot
					IF IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED)
					OR IS_GOLF_FOURSOME_STATE_AT_END(thisFoursome)
						iPlayerCardIndex = thisFoursome.playerCore[iPlayerIndex].playerCard.iScoreboardSlot
					ENDIF
					
					IF NETWORK_PLAYER_HAS_HEADSET(NETWORK_GET_PLAYER_INDEX(partIndex))
						
						IF NETWORK_IS_PLAYER_MUTED_BY_ME(NETWORK_GET_PLAYER_INDEX(partIndex))
							IF golfHeadsetStates[iPlayerIndex] != GOLF_HEADSET_MUTE // I just mutted the player
								SET_GOLF_HEADSET_STATE(thisHelpers, iPlayerCardIndex, GOLF_HEADSET_MUTE)
								golfHeadsetStates[iPlayerIndex] = GOLF_HEADSET_MUTE
							ENDIF
						ELSE
							IF NETWORK_IS_PLAYER_TALKING(NETWORK_GET_PLAYER_INDEX(partIndex)) 
							OR NETWORK_IS_CHATTING_IN_PLATFORM_PARTY(gamer) OR NETWORK_IS_GAMER_TALKING(gamer)
								IF golfHeadsetStates[iPlayerIndex] != GOLF_HEADSET_ACTIVE // player is talking
									SET_GOLF_HEADSET_STATE(thisHelpers, iPlayerCardIndex, GOLF_HEADSET_ACTIVE)
									golfHeadsetStates[iPlayerIndex] = GOLF_HEADSET_ACTIVE
								ENDIF
							ELIF golfHeadsetStates[iPlayerIndex] != GOLF_HEADSET_INACTIVE //player has stopped talking
								SET_GOLF_HEADSET_STATE(thisHelpers, iPlayerCardIndex, GOLF_HEADSET_INACTIVE)
								golfHeadsetStates[iPlayerIndex] = GOLF_HEADSET_INACTIVE 
							ENDIF
						ENDIF
						
					ELIF golfHeadsetStates[iPlayerIndex] != GOLF_HEADSET_NONE //player unpluged his headset
						SET_GOLF_HEADSET_STATE(thisHelpers, iPlayerCardIndex, GOLF_HEADSET_NONE)
						golfHeadsetStates[iPlayerIndex] = GOLF_HEADSET_NONE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC GOLF_MP_CLIENT_SYNCHRONIZE_PARTICLE_EFFECTS(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_PLAYER &thisPlayers[])
	
	INT currentPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT( thisPlayers, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
	INT localPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome))
	
	IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, currentPlayer, GCF_BALL_JUST_HIT) 
	AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, localPlayer, GCF_PLAYED_HIT_PARTICLE_EFFECT_MP) 
		CDEBUG1LN(DEBUG_GOLF,"Play Remote Hit Particle Effect")
		
		GOLF_PLAY_CLUB_IMPACT_PTFX(thisFoursome, thisCourse, thisGame)
		
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_TEE
		AND thisFoursome.swingMeter.meterState != SWING_METER_SHANK 
		AND GET_GOLF_HOLE_PAR(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) > 3
			//if you are going to play the tee partcile effect, remove the tee object
			DELETE_TEE(thisFoursome)
		ENDIF
		
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, localPlayer, GCF_PLAYED_HIT_PARTICLE_EFFECT_MP) 
	ENDIF
	
	IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, currentPlayer, GCF_BALL_JUST_LAND) 
	AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, localPlayer, GCF_PLAYED_LAND_PARTICLE_EFFECT_MP) 
	AND DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		CDEBUG1LN(DEBUG_GOLF,"Play Remote Land Particle Effect")

		GOLF_PLAY_GROUND_IMPACT_PTFX(thisFoursome)
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, localPlayer, GCF_PLAYED_LAND_PARTICLE_EFFECT_MP) 
	ENDIF
	
	IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, localPlayer, GCF2_SYNC_BALL_IN_WATER_SOUND)
		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
			IF IS_ENTITY_IN_WATER(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
				CDEBUG1LN(DEBUG_GOLF,"Remote player hit the ball into water")
				PLAY_GOLF_WATER_IMPACT_SOUND_AND_PTFX(thisFoursome)
				SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, localPlayer, GCF2_SYNC_BALL_IN_WATER_SOUND)
			ENDIF
		ENDIF
	ENDIF
	
	GOLF_TRAIL_SET_ENABLED(FALSE)	
ENDPROC

PROC GOLF_MP_CLIENT_SYNCHRONIZE_SOUNDS(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_GAME &thisGame, GOLF_PLAYER &thisPlayers[], GOLF_COURSE &thisCourse)
	
	INT currentPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT( thisPlayers, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
	INT localPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome))
	
	IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, currentPlayer, GCF_BALL_JUST_HIT) 
	AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, localPlayer, GCF_PLAYED_HIT_SOUND_EFFECT_MP) 
		CDEBUG1LN(DEBUG_GOLF, "Play Remote Hit Sound Effect")
		
		GOLF_PLAY_CLUB_IMPACT_SOUND(thisGame, thisFoursome, TRUE, IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, currentPlayer, GCF_WAS_PERFECT_HIT), FALSE)
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, localPlayer, GCF_PLAYED_HIT_SOUND_EFFECT_MP) 
	ENDIF

	IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, currentPlayer, GCF_BALL_JUST_LAND)
		IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, localPlayer, GCF_PLAYED_LAND_SOUND_EFFECT_MP)
		AND DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
			CDEBUG1LN(DEBUG_GOLF, "Play Remote Land Sound Effect")
			
			GOLF_PLAY_GROUND_IMPACT_SOUND(thisFoursome, TRUE, IS_BALL_LOW_IMPACT_SPEED(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)))
			SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, localPlayer, GCF_PLAYED_LAND_SOUND_EFFECT_MP)
		ENDIF
	ELSE
		CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, localPlayer, GCF_PLAYED_LAND_SOUND_EFFECT_MP) //ball is in air again, possible to player sound again
	ENDIF

	IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, currentPlayer, GCF_BALL_IN_CUP_SOUND)
	AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, localPlayer, GCF_MP_PLAYED_BALL_IN_HOLE_SOUND)
	AND currentPlayer != localPlayer
		CDEBUG1LN(DEBUG_GOLF, "Play Remote Ball In Hole Sound Effect")

		GOLF_PLAY_BALL_IN_HOLE_SOUND(thisFoursome, thisCourse, thisGame, FALSE)
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, localPlayer, GCF_MP_PLAYED_BALL_IN_HOLE_SOUND)
	ENDIF

ENDPROC


PROC GOLF_MP_CLIENT_SYNCHRONIZE_BALL_CONTACT(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[])

	INT piPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT( thisPlayers, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
	NETWORK_INDEX niBall = GET_GOLF_MP_CLIENT_BALL(playerBD, piPlayer)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niBall)
		IF (GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_SWING_METER OR GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_BALL_IN_FLIGHT)
		AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, piPlayer, GCF2_BALL_CONTACT_SYNCED)
//			//If the animation of the current player has not reached the ball on my machine. Don't allow the ball to update for a given amount of time
//			IF NOT HAS_SWING_ANIM_REACHED_BALL(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
//			AND (IS_ANY_SWING_ANIM_PLAYING(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
//			OR 	 IS_ANY_BACKSWING_ANIM_PLAYING(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome)))
//				NETWORK_OVERRIDE_BLENDER_FOR_TIME(niBall, 10)
//			ELSE
//				NETWORK_OVERRIDE_BLENDER_FOR_TIME(niBall, 0)
//				SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, piPlayer, GCF2_BALL_CONTACT_SYNCED)
//			ENDIF
		ELSE
//			//player isn't swinging, let network sync ball
//			NETWORK_OVERRIDE_BLENDER_FOR_TIME(niBall, 0)
		ENDIF
	ENDIF
ENDPROC

//We don't always want to play the exact same message as our oppenents. This functions managaes that
PROC GOLF_MP_CLIENT_PLAY_SPLASH_TEXT(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_SPLASH_TEXT_ENUM eSplashText)
	
	TEXT_LABEL_23 sRemoteSplashText, sRemoteStrapLine 
	
	IF eSplashText = GOLF_SPLASH_CLOSEST_TO_PIN
		eSplashText = GOLF_SPLASH_GIR
	ENDIF
	IF eSplashText = GOLF_SPLASH_LONGEST_DIVE OR eSplashText = GOLF_SPLASH_LONGEST_HOLE
		eSplashText = GOLF_SPLASH_FIR
	ENDIF
	
	sRemoteSplashText = GET_GOLF_SPLASH_TEXT_FROM_ENUM(eSplashText, sRemoteStrapLine)
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) < GPS_ADDRESS_BALL
		CDEBUG1LN(DEBUG_GOLF,"Trying to sync end of shot splash at begining of shot?")
		EXIT
	ENDIF
	
	HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW
	
	IF (eSplashText >= GOLF_SPLASH_BOGEY AND eSplashText <= GOLF_SPLASH_BOGEY5)
	OR (eSplashText >= GOLF_SPLASH_OOB AND eSplashText <= GOLF_SPLASH_SCORE_LIMIT)
		eHudColor = HUD_COLOUR_RED
	ENDIF
	
	CDEBUG1LN(DEBUG_GOLF,"Golf MP splash text sync - ", sRemoteSplashText, " strap line ", sRemoteStrapLine)
	thisHelpers.eLastSplashText = eSplashText
	
	IF eSplashText = GOLF_SPLASH_HOLE_IN_ONE OR eSplashText = GOLF_SPLASH_EAGLE2
	OR eSplashText = GOLF_SPLASH_EAGLE 		 OR eSplashText = GOLF_SPLASH_BIRDIE
		HUD_COLOURS eEndHudColor = HUD_COLOUR_BLUE
		PLAY_GOLF_SPLASH(sRemoteSplashText, sRemoteStrapLine, GOLF_SPLASH_SHARD_MIDSIZED, eHudColor, 4000, FALSE, eEndHudColor)
	ELSE
		PLAY_GOLF_SPLASH(sRemoteSplashText, sRemoteStrapLine, GOLF_SPLASH_SHARD_MIDSIZED, eHudColor)
	ENDIF
	
	PLAY_GOLF_SPLASH_SOUND(eSplashText)
	
ENDPROC

PROC GOLF_MP_SYNC_SPLASH_TEXT(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_PLAYER &thisPlayers[])
	
	INT currentPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT( thisPlayers, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
	INT localPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome))

	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
		//local player has splash text
		IF IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SPLASH_TEXT_DISPLAYED_THIS_TURN)
			SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, currentPlayer, GCF_MP_SYNC_SPLASH_TEXT)
			playerBD[currentPlayer].eLastSplashText = thisHelpers.eLastSplashText
		ENDIF
	ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_NETWORK
		//if currnet player is displaying splash, and I have not displayed splash yet
		IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, currentPlayer, GCF_MP_SYNC_SPLASH_TEXT)
		AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, localPlayer, GCF_MP_SYNC_SPLASH_TEXT)
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) > GPS_ADDRESS_BALL
			AND GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_GAME AND GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_HOLE
				CDEBUG1LN(DEBUG_GOLF,"Play Splash Text")
				
				GOLF_MP_CLIENT_PLAY_SPLASH_TEXT(thisFoursome, thisHelpers, playerBD[currentPlayer].eLastSplashText)
				SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, localPlayer, GCF_MP_SYNC_SPLASH_TEXT)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC GOLF_MP_CLIENT_MANAGE_BLIPS(GOLF_FOURSOME &thisFoursome)


	INT iCurrentPlayer = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
	
	IF GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome) != iCurrentPlayer
	
		IF GET_BLIP_INFO_ID_DISPLAY(GET_MAIN_PLAYER_BLIP_ID()) != DISPLAY_NOTHING AND NOT IS_PAUSE_MENU_ACTIVE()
			SET_BLIP_DISPLAY(GET_MAIN_PLAYER_BLIP_ID(), DISPLAY_NOTHING)
		ELIF GET_BLIP_INFO_ID_DISPLAY(GET_MAIN_PLAYER_BLIP_ID()) = DISPLAY_NOTHING AND IS_PAUSE_MENU_ACTIVE()
			SET_BLIP_DISPLAY(GET_MAIN_PLAYER_BLIP_ID(), DISPLAY_BLIP)
		ENDIF
	ELSE
		IF GET_BLIP_INFO_ID_DISPLAY(GET_MAIN_PLAYER_BLIP_ID()) != DISPLAY_BLIP
			SET_BLIP_DISPLAY(GET_MAIN_PLAYER_BLIP_ID(), DISPLAY_BLIP)
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_CURRENT_GOLFER_NEAR_HIS_BALL(GOLF_FOURSOME &thisFoursome)
	
	OBJECT_INDEX objBall = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)
	PED_INDEX pedGolfer = GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome)
	
	IF DOES_ENTITY_EXIST(pedGolfer) AND DOES_ENTITY_EXIST(objBall)
		VECTOR vGolferPos = GET_ENTITY_COORDS(pedGolfer, FALSE)
		vGolferPos.z = 0
		VECTOR vBallPos = GET_ENTITY_COORDS(objBall, FALSE)
		vBallPos.z = 0
		
		IF VDIST2(vGolferPos, vBallPos) < (1.2*1.2)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC GOLF_MP_CLIENT_MANAGE_GOLF_OBJECTS(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_PLAYER &thisPlayers[])
	
	INT iPlayerIndex
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		INT piPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, iPlayerIndex)
		
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_ADDRESS_BALL
		
			IF thisHelpers.bKeepPlayerInvisible[iPlayerIndex] //if the player is invisable, don't have golf club
				CLEANUP_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(thisFoursome, iPlayerIndex)
			ELSE
				IF iPlayerIndex = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
					IF iPlayerIndex != GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome) 
						GOLF_ATTACH_CLUB_TO_PLAYER(thisFoursome.playerCore[iPlayerIndex], thisGame.golfBag, TRUE) //create the networked players clubs
					ENDIF
				ELSE
					IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piPlayer)
					AND NOT IS_GOLF_PLAYER_CONTROL_FLAG_SET(thisFoursome.playerCore[iPlayerIndex], GOLF_PLAYER_REMOVED)
						//create spectator clubs
						SET_GOLF_FOURSOME_INDEXED_PLAYER_CURRENT_CLUB(thisFoursome, iPlayerIndex, GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eCLUB_IRON_3))
						GOLF_ATTACH_CLUB_TO_PLAYER(thisFoursome.playerCore[iPlayerIndex], thisGame.golfBag, TRUE)
					ELSE
						//player is no longer in game, don't make club
						CLEANUP_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(thisFoursome, iPlayerIndex)
						
						IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex))
							IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex))
								CLEANUP_GOLF_FOURSOME_INDEXED_GOLF_BALL(thisFoursome, iPlayerIndex)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_GOLF_FOURSOME_INDEXED_PLAYER_LIE(thisFoursome, iPlayerIndex) = LIE_CUP //in hole get rid of ball now
			OR GET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, iPlayerIndex) = GPS_DONE_WITH_HOLE
				IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex))
					CDEBUG1LN(DEBUG_GOLF,"Removing unneeded ball")
					IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex))
						CLEANUP_GOLF_BALL(thisFoursome.playerCore[iPlayerIndex])
					ENDIF
				ENDIF
			ENDIF
		
		ELSE
			IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piPlayer)
				IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, piPlayer, GCF2_DETACH_GOLF_CLUB)
					GOLF_DETACH_CLUB(thisFoursome, iPlayerIndex)
				ENDIF
			ENDIF
			
		ENDIF
		
		//Local player has to control all the balls
		
		IF ARE_ALL_PLAYERS_ON_THE_GREEN(thisFoursome, thisCourse) 
		AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_ADDRESS_BALL	
			
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) != iPlayerIndex 
				IF GET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, iPlayerIndex) != GPS_DONE_WITH_HOLE
				AND GET_GOLF_PLAYER_LIE(thisFoursome.playerCore[iPlayerIndex]) != LIE_CUP
				AND NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
					//iPlayerIndex isn't current player, create marker
					//CDEBUG1LN(DEBUG_GOLF,"Set marker for specating player ", iPlayerIndex)
					IF NOT DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_MARKER(thisFoursome, iPlayerIndex))
						OBJECT_INDEX newMarker = CREATE_GOLF_BALL_MARKER(thisFoursome, iPlayerIndex)
						SET_GOLF_FOURSOME_INDEXED_PLAYER_MARKER(thisFoursome, newMarker, iPlayerIndex)
					ELSE
						SET_GOLF_MARKER_FLUSH_WITH_GROUND(GET_GOLF_FOURSOME_INDEXED_PLAYER_MARKER(thisFoursome, iPlayerIndex))
					ENDIF
					
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
						CLEANUP_GOLF_FOURSOME_INDEXED_GOLF_BALL(thisFoursome, iPlayerIndex)
					ENDIF
				ENDIF
			ELSE
				//CDEBUG1LN(DEBUG_GOLF,"Remove marker for player ", iPlayerIndex)
				CLEANUP_GOLF_FOURSOME_INDEXED_PLAYER_MARKER(thisFoursome, iPlayerIndex)
			ENDIF
		ENDIF
		
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_BALL_IN_FLIGHT
			OR GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_BALL_AT_REST
				IF GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)	!= iPlayerIndex
					IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex))
					AND DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL(thisFoursome))
						IF IS_ENTITY_TOUCHING_ENTITY(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex), GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL(thisFoursome))
							//unfreeze oppoents ball incase your ball knocks into it
							FREEZE_GOLF_BALL(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex), FALSE)
						ENDIF
					ENDIF
				ENDIF
			ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_ADDRESS_BALL
				IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex))
					//freeze ball when not in flight
					FREEZE_GOLF_BALL(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex), TRUE)
				ENDIF	
			ENDIF
		ENDIF

	ENDREPEAT
	
	IF GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome) != GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)

		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) > GPS_ADDRESS_BALL
		AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) < GPS_POST_SHOT_REACTION
		
			IF IS_ENTITY_AT_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), PLAYER_PED_ID(), <<0.75, 0.75, 3>>)
			AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_DETACH_GOLF_CLUB)
				CDEBUG1LN(DEBUG_GOLF,"Ball is touching me")
				GOLF_KNOCK_OVER_PED(PLAYER_PED_ID(), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
				SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_DETACH_GOLF_CLUB)
			ENDIF
		ENDIF
		
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) > GPS_SWING_METER
			//don't mess with the tee after the player hit the ball
		ELIF IS_TEE_SHOT(thisCourse, thisFoursome)
			CREATE_TEE(thisFoursome, thisCourse)
		ELSE
			DELETE_TEE(thisFoursome)
		ENDIF
	ENDIF

	
	IF ARE_ALL_PLAYERS_ON_THE_GREEN(thisFoursome, thisCourse)
		IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"ALL PLAYERS ON GREEN") ENDIF
		IF DOES_ENTITY_EXIST(GET_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
		AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_ADDRESS_BALL
			// Delete the flag as everyone is on the green
			CDEBUG1LN(DEBUG_GOLF,"DELETE FLAG")
			CLEANUP_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
		ENDIF
	ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) < GPS_SWING_METER
	AND NOT IS_GOLF_CAM_RENDERING(thisHelpers.camReaction)
		CREATE_GOLF_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
	ENDIF

ENDPROC

PROC GOLF_MP_CLIENT_MANAGE_QUIT_REQUEST(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_PLAYER &thisPlayers[])
	
	INT localPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome))
	UNUSED_PARAMETER(thisHelpers)
	
	// B* 2309115
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	//You pressed the quit button and the quit menu isn't up yet
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT) AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, localPlayer, GCF_MP_START_QUIT_MENU)
	//The current player can only quit if he is in address mode
	AND ( GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) != GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome) OR GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) < GPS_SWING_METER)
	//No quiting at the very end of the game
	AND GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_GAME //AND GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_HOLE 
	AND NOT IS_PAUSE_MENU_ACTIVE() AND NOT IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, localPlayer, GCF_MP_START_QUIT_MENU)
		SET_GOLF_CONTROL_FLAG(thisGame, GCF_MP_START_QUIT_MENU)
		SET_GOLF_CONTROL_FLAG(thisGame, GCF_STOP_INPUT)
	ENDIF

ENDPROC

PROC GOLF_MP_CLIENT_UPDATE_LOCAL_PLAYER_TELEPORT(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[])
	// On skips between shots, make sure to grab set the proper teleports for the local player
	INT iLocalPlayer = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome) //index in thisFoursome
	INT piCurrentShooter = GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD) // net index
	
	IF IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iLocalPlayer, GOLF_PLAYER_REMOVED)
		thisHelpers.bAllowPlayerViewCam = FALSE
		EXIT //you are a spectator, don't teleport
	ENDIF
	
	CDEBUG1LN(DEBUG_GOLF,"Teleporting player mp")
	IF GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, iLocalPlayer) != piCurrentShooter
		TELEPORT_SPECTATOR_TO_POSITION_IN_GOLF(thisFoursome, thisHelpers, thisCourse, iLocalPlayer)
	ELSE
		CDEBUG1LN(DEBUG_GOLF,"Teleporting to ball")
	//	thisHelpers.fPreviewSplinePhase = thisHelpers.fPreviewSplinePhase //supress errors
		//SET_ENTITY_COORDS(GET_GOLF_FOURSOME_LOCAL_PLAYER_PED(thisFoursome), GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL_POSITION(thisFoursome), FALSE, FALSE)

		//Note: Use GOLF_ATTACH_CLUB_TO_PLAYER in lieu of recreateclub option as it causes clubs to disappear and reappear on remotes - lavalley
		//IF NOT DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome))
			PICK_IDEAL_CLUB_AND_SHOT(thisGame, thisCourse,thisFoursome, thisHelpers)
			GOLF_ATTACH_CLUB_TO_PLAYER(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag)
		//ENDIF

		PLACE_PLAYER_AT_BALL(thisCourse, thisFoursome, thisGame.golfBag, FALSE, TRUE, IS_TEE_SHOT(thisCourse, thisFoursome), FALSE, TRUE)
	ENDIF
	
	CANCEL_TIMER(thisHelpers.navTimer)
	CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_PLAYER_SKIP)
ENDPROC

PROC SET_GOLF_CLIENT_CONTROL_FLAG_IF_PLAYER_FLAG_IS_SET(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_CONTROL_FLAGS gcFlag, GOLF_PLAYER_STATUS_BITS gpsFlag)
	IF IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(thisFoursome, gpsFlag)
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), gcFlag)
	ELIF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), gcFlag)
		CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), gcFlag)
	ENDIF
ENDPROC

/// PURPOSE:
///    Syncs the remote players data on the local players machine
/// PARAMS:
///    serverBD - 
///    playerBD - 
///    thisGame - 
///    thisFoursome - 
///    thisHelpers - 
///    thisPlayers - 
///    iPlayerIndex - 
PROC GOLF_MP_CLIENT_SYNC_REMOTE_PLAYER_DATA(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[], INT iPlayerIndex)
	// Sync remote player broadcast data to local proxy
	INT piPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT( thisPlayers, iPlayerIndex)
	
	VECTOR vRemoteBallPosition = GET_GOLF_MP_CLIENT_BALL_POSITION( playerBD, piPlayer)
	INT iRemoteShotsOnHole = GET_GOLF_MP_CLIENT_SHOTS_ON_CURRENT_HOLE(playerBD, piPlayer)
	
	IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL_POSITION(thisFoursome, iPlayerIndex), vRemoteBallPosition)
	OR GET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(thisFoursome, iPlayerIndex) != iRemoteShotsOnHole
		IF NOT ARE_VECTORS_ALMOST_EQUAL(vRemoteBallPosition, <<0,0,0>>)
			CDEBUG1LN(DEBUG_GOLF,"Remote player done with shot, update info")
			SET_GOLF_FOURSOME_INDEXED_PLAYER_BALL_POSITION(thisFoursome, iPlayerIndex, vRemoteBallPosition)
			SET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(thisFoursome, iPlayerIndex, GET_GOLF_MP_CLIENT_SHOTS_ON_CURRENT_HOLE(playerBD, piPlayer))			
			SET_GOLF_CONTROL_FLAG(thisGame, GCF_OPPONENT_DONE_WITH_SHOT)
			SET_GOLF_PLAYER_HOLE_SCORE_BY_INDEX (thisPlayers, iPlayerIndex, GET_GOLF_MP_SERVER_CURRENT_HOLE(serverBD),  GET_GOLF_MP_SERVER_PLAYER_CURRENT_HOLE_SCORE(serverBD,piPlayer))
			SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
		ENDIF
	ENDIF
	SET_GOLF_FOURSOME_INDEXED_PLAYER_LIE (thisFoursome, iPlayerIndex, <<0,0,0>>, GET_GOLF_MP_CLIENT_LIE(playerBD, piPlayer), FALSE)
	
	SET_GOLF_FOURSOME_INDEXED_PLAYER_STATE( thisFoursome, iPlayerIndex, GET_GOLF_MP_CLIENT_PLAYER_STATE(playerBD, piPlayer))
	IF NETWORK_DOES_NETWORK_ID_EXIST(GET_GOLF_MP_CLIENT_BALL(playerBD, piPlayer))
		SET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex, NET_TO_OBJ(GET_GOLF_MP_CLIENT_BALL(playerBD, piPlayer)))
	ENDIF
	
	IF NOT IS_PED_INJURED(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayerIndex))
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = iPlayerIndex
		AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_ADDRESS_BALL
		AND IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, piPlayer, GCF2_PLAYER_HAS_TELEPORTED)
		AND NOT IS_GOLF_MP_APPROACH_CAM_RENDERING(thisHelpers)
			//allow blending when it's this players turn
			NETWORK_SET_ENTITY_CAN_BLEND(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayerIndex), TRUE)
			NETWORK_USE_LOGARITHMIC_BLENDING_THIS_FRAME(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayerIndex))
		ELSE
			NETWORK_SET_ENTITY_CAN_BLEND(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayerIndex), FALSE)
		ENDIF
	ENDIF
	
	SET_GOLF_FOURSOME_INDEXED_PLAYER_DISTANCE_TO_HOLE (thisFoursome, iPlayerIndex, GET_GOLF_MP_CLIENT_DISTANCE_TO_HOLE(playerBD, piPlayer))
	IF GET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, iPlayerIndex) = GPS_ADDRESS_BALL
		SET_GOLF_FOURSOME_INDEXED_PLAYER_AIM( thisFoursome, iPlayerIndex, GET_GOLF_MP_CLIENT_AIM(playerBD, piPlayer))
		IF GET_GOLF_FOURSOME_INDEXED_PLAYER_CURRENT_CLUB(thisFoursome, iPlayerIndex) != GET_GOLF_MP_CLIENT_CLUB_INDEX(playerBD, piPlayer)
			SET_GOLF_FOURSOME_INDEXED_PLAYER_CURRENT_CLUB (thisFoursome, iPlayerIndex, GET_GOLF_MP_CLIENT_CLUB_INDEX(playerBD, piPlayer))
			SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_ADDRESS)
		ENDIF
		IF GET_GOLF_FOURSOME_INDEXED_PLAYER_SWING_STYLE (thisFoursome, iPlayerIndex) != GET_GOLF_MP_CLIENT_SWING_STYLE (playerBD, piPlayer)
			SET_GOLF_FOURSOME_INDEXED_PLAYER_SWING_STYLE (thisFoursome, iPlayerIndex, GET_GOLF_MP_CLIENT_SWING_STYLE(playerBD, piPlayer))
			SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_ADDRESS)
		ENDIF
	ENDIF
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = iPlayerIndex
		thisFoursome.swingMeter.fCurrentPowerRel = GET_GOLF_MP_CLIENT_SHOT_STRENGTH(playerBD, piPlayer)
	ENDIF
	
ENDPROC

PROC MANAGE_MP_GOLF_BALL_SYNCING(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_FOURSOME &thisFoursome)

	INT index
	
	REPEAT MP_GOLF_MAX_PLAYERS index
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			OBJECT_INDEX objSyncBall = GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, index)
			
			IF DOES_ENTITY_EXIST(objSyncBall) AND serverBD.hostGolfBallIndex[index] != OBJ_TO_NET(objSyncBall)
				serverBD.hostGolfBallIndex[index] = OBJ_TO_NET(objSyncBall)
			ENDIF
		ELSE
			IF index = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.hostGolfBallIndex[index])
					SET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, index, NET_TO_OBJ(serverBD.hostGolfBallIndex[index]))
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC GOLF_MP_CLIENT_SYNC_LOCAL_PLAYER_DATA(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_GAME &thisGame)
	// Sync local player data to broadcast data
//	IF GET_GOLF_FOURSOME_LOCAL_PLAYER_CURRENT_CLUB(thisFoursome) != GET_GOLF_MP_CLIENT_CLUB_INDEX(playerBD, PARTICIPANT_ID_TO_INT())
//		CDEBUG1LN(DEBUG_GOLF,"Sending CHANGE CLUB to remote machines: ", 	 GET_GOLF_FOURSOME_LOCAL_PLAYER_CURRENT_CLUB(thisFoursome))
//	ENDIF
	SET_GOLF_MP_CLIENT_LOCAL_PLAYER_STATE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_STATE(thisFoursome))
	SET_GOLF_MP_CLIENT_LOCAL_LIE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_LIE(thisFoursome)) //always update your lie
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL(thisFoursome))
	
		IF NOT IS_ENTITY_A_MISSION_ENTITY(GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL(thisFoursome))
			SET_ENTITY_AS_MISSION_ENTITY(GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL(thisFoursome))
		ENDIF
		SET_GOLF_MP_CLIENT_LOCAL_BALL(playerBD, OBJ_TO_NET(GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL(thisFoursome)))
	ENDIF
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_LOCAL_PLAYER_CLUB(thisFoursome))
		IF NOT IS_ENTITY_A_MISSION_ENTITY(GET_GOLF_FOURSOME_LOCAL_PLAYER_CLUB(thisFoursome))
			SET_ENTITY_AS_MISSION_ENTITY(GET_GOLF_FOURSOME_LOCAL_PLAYER_CLUB(thisFoursome))
		ENDIF
	ENDIF
	IF GET_GOLF_FOURSOME_LOCAL_PLAYER_STATE(thisFoursome) = GPS_ADDRESS_BALL
		SET_GOLF_MP_CLIENT_LOCAL_CLUB_INDEX(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_CURRENT_CLUB(thisFoursome))
		SET_GOLF_MP_CLIENT_LOCAL_AIM (playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_AIM(thisFoursome))
		SET_GOLF_MP_CLIENT_LOCAL_SWING_STYLE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_SWING_STYLE(thisFoursome))
	ENDIF
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
		SET_GOLF_MP_CLIENT_LOCAL_SHOT_STRENGTH(playerBD, thisFoursome.swingMeter.fCurrentPowerRel)
	ENDIF
	
	IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
		SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
	ELIF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
		CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
	ENDIF
	
	SET_GOLF_CLIENT_CONTROL_FLAG_IF_PLAYER_FLAG_IS_SET(playerBD, thisFoursome, GCF_BALL_OOB, GOLF_BALL_IS_OOB)
	 
ENDPROC

PROC GOLF_CLIENT_MANAGE_MOVE_TO_NEXT_HOLE_UI(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[])
	
	INT iPlayerIndex
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex,  GOLF_WAITING_FOR_HOLE)
		//Do not check for GOLF_PLAYER_REMOVED here
		
			INT piIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, iPlayerIndex)
			IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piIndex)
				IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, piIndex, GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
				//maybe check for GOLF_PLAYER_REMOVED, but only if it's not for rematch
					CDEBUG1LN(DEBUG_GOLF,"Player ", iPlayerIndex, " is marking his check bos to move to next hole" )
					//update menu
					IF GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(thisFoursome) > 1
					AND NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
						SET_SCOREBOARD_SLOT(thisHelpers, thisCourse, thisFoursome, thisPlayers[iPlayerIndex], thisFoursome.playerCore[iPlayerIndex].playerCard, TRUE, TRUE, TRUE)
					ENDIF
					SET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG(thisFoursome, iPlayerIndex, GOLF_WAITING_FOR_HOLE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC GOLF_MP_MANAGE_END_OF_HOLE_SCORECARD(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[])
	
	UNUSED_PARAMETER(thisGame)
	
	
	//checks to see if players want to move on
	IF (GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_SCORECARD_END_HOLE OR GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_SCORECARD_END_GAME)
		IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, PARTICIPANT_ID_TO_INT(), GFC2_AT_GOLF_SCOREBOARD)
			SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GFC2_AT_GOLF_SCOREBOARD)
		ENDIF
		
		INT iTimer = GET_REMANING_GOLF_MP_TIME(serverBD, 30)
		CDEBUG1LN(DEBUG_GOLF,"iTimer is at ", iTimer)
		GOLF_NETWORK_RELEASE_CONTROL_OF_ALL_BALLS(thisFoursome)
		
		//When at end hole scoreboard auto-ready spectating players
		IF IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PLAYER_REMOVED)
			SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
		ENDIF
		
		//if there is move the one person playing golf show timer, or if it's the end of the game
		IF GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(thisFoursome) > 1
		OR GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_SCORECARD_END_GAME
			
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				FLOAT fNumberReady = TO_FLOAT(GET_NUM_GOLF_CLIENTS_WITH_CONTROL_FLAG_SET(thisFoursome, playerBD, thisPlayers, GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE))
				
				IF (fNumberReady/GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(thisFoursome)) > 0.51 //more then half want to move to next hole
				AND iTimer > 6.0
					serverBD.iMPTimer = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) - 25000
				ENDIF
				
			ENDIF
			
			IF IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED)
			AND NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_DISABLE_UI) AND NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_END_GOLF_SCOREBOARD)
				IF serverBD.iMPTimer != 0
					GOLF_MANAGE_END_OF_HOLE_TIMER(thisFoursome, iTimer)
				ENDIF
			ENDIF
		ENDIF
		
		BOOL bForceMove = (iTimer <= 0)
		IF IS_ANY_GOLF_MP_CLIENT_AT_PLAYER_STATE(playerBD, thisPlayers, thisFoursome, GPS_ADDRESS_BALL)
		AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_GO_TO_REMATCH)
			CDEBUG1LN(DEBUG_GOLF,"A player is at address ball state and you are still at the scoreboard")
			bForceMove = TRUE
		ENDIF
		IF bForceMove
			CDEBUG1LN(DEBUG_GOLF,"Force player past scorecard")
		ENDIF
		
		IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
		OR bForceMove
		
			//all players want to move to next hole, lets do it
			IF IS_ALL_GOLF_CLIENT_CONTROL_FLAGS_SET(thisFoursome, playerBD, thisPlayers, GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
			OR bForceMove
				CDEBUG1LN(DEBUG_GOLF,"All players want to move to next hole")
				
				SET_GOLF_MINIMAP(thisFoursome, thisCourse)
				SET_GOLF_UI_FLAG(thisHelpers, GUC_DISABLE_FADE_IN)
				SET_GOLF_UI_FLAG(thisHelpers, GUC_END_GOLF_SCOREBOARD)
				
				INT iPlayerIndex
				REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
					CLEAR_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG(thisFoursome, iPlayerIndex, GOLF_WAITING_FOR_HOLE)
					IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
						SET_SCOREBOARD_SLOT(thisHelpers, thisCourse, thisFoursome, thisPlayers[iPlayerIndex], thisFoursome.playerCore[iPlayerIndex].playerCard, TRUE, TRUE, FALSE) //remove ready
					ENDIF
				ENDREPEAT
	//		ELSE
			ENDIF
		ENDIF
		
		IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_END_GOLF_SCOREBOARD)
		AND iTimer > 0
			GOLF_CLIENT_MANAGE_MOVE_TO_NEXT_HOLE_UI(playerBD, thisFoursome, thisCourse, thisHelpers, thisPlayers)
		ENDIF
	ELSE
		IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, PARTICIPANT_ID_TO_INT(), GFC2_AT_GOLF_SCOREBOARD)
			CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GFC2_AT_GOLF_SCOREBOARD)
		ENDIF
	ENDIF
ENDPROC

PROC GOLF_MP_CLIENT_MANAGE_AUTO_SWING_TIMER(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[])

	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) >= GPS_ADDRESS_BALL 
	AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) < GPS_BALL_IN_FLIGHT
	AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PLAYER_TIME_OUT)
	ANd NOT IS_SHOT_A_GIMME(thisCourse, thisFoursome)
	AND GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(thisFoursome) > 1
	AND NOT IS_GOLF_MP_APPROACH_CAM_RENDERING(thisHelpers)
		
		INT iTimer
		INT iLocalPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome))
		
		IF thisHelpers.iNumAutoSwings < (iMaxNumOfAutoSwingsBeforeKick - 1)
			iTimer = GET_REMANING_GOLF_MP_TIME(serverBD, FLOOR(fAutoSwingTime))
			
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) < GPS_SWING_METER
			AND GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_LOCAL_MP
				IF iTimer = 0
					CDEBUG1LN(DEBUG_GOLF,"Auto swing")
					thisHelpers.iNumAutoSwings++
					SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_PLAYER_TIME_OUT)
				ENDIF
			ENDIF
		ELSE
			//player is taking too long to golf, use kick timer
			iTimer = GET_REMANING_GOLF_MP_TIME(serverBD, FLOOR(fAutoSwingTime))
			
			IF iTimer = 0 AND GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_LOCAL_MP
				CDEBUG1LN(DEBUG_GOLF, "Kick for time")
				SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, iLocalPlayer, GCF2_KICK_PLAYER)
			ENDIF
		ENDIF
		
		//Dont display timer when quit menu is up
		IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, iLocalPlayer, GCF_MP_START_QUIT_MENU)
			IF iTimer < 16
//				IF thisHelpers.iNumAutoSwings < (iMaxNumOfAutoSwingsBeforeKick - 1)
					DRAW_GENERIC_TIMER(iTimer*1000, "GOLF_SHOT_CLOCK", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
//				ELSE
//					DRAW_GENERIC_TIMER(iTimer*1000, "GOLF_KICK_CLOCK", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)				
//				ENDIF
			ELSE
				DRAW_GENERIC_TIMER(iTimer*1000, "GOLF_SHOT_CLOCK")
			ENDIF
		ENDIF

	ELIF GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_GAME AND GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_HOLE
	AND (GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) > GPS_BALL_IN_FLIGHT OR GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) < GPS_ADDRESS_BALL)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF serverBD.iMPTimer != 0
				serverBD.iMPTimer = 0
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_GOLF_MP_SCOREBOARD_FORCED_CLOSED(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_NETWORK
	AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) >= GPS_BALL_IN_FLIGHT
		//CDEBUG1LN(DEBUG_GOLF,"Remote player can't view scorebaord when ball is in flight")
		RETURN TRUE
	ENDIF
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) >= GPS_SWING_METER
	OR GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) < GPS_ADDRESS_BALL
		//CDEBUG1LN(DEBUG_GOLF,"Player can't use scoreboard after starting swing")
		RETURN TRUE
	ENDIF
	
	IF IS_GOLF_MP_APPROACH_CAM_RENDERING(thisHelpers)
		RETURN TRUE
	ENDIF
	
	IF GET_REMANING_GOLF_MP_TIME(serverBD, FLOOR(fAutoSwingTime), FALSE) < 5.0 
	AND GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(thisFoursome) > 1
		//CDEBUG1LN(DEBUG_GOLF,"Timer is to close to end, don't use scoreboard")
		RETURN TRUE
	ENDIF
	
	IF GET_GOLF_PLAYER_SWING_ANIM(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)]) != SAS_IDLE
		//CDEBUG1LN(DEBUG_GOLF,"Not in swing idle state")
		RETURN TRUE
	ENDIF
		
	IF thisHelpers.bShotPreviewCam OR thisHelpers.bGreenPreviewCam != 0
		//CDEBUG1LN(DEBUG_GOLF,"shots cameras are in use")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Returns the number of people who won
FUNC INT GET_GOLF_BETTING_DATA(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], PLAYER_INDEX &winnerID[])
	INT playerIndex, playersWon
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) playerIndex	
		IF DID_GOLF_INDEXED_PLAYER_WIN(thisFoursome, thisPlayers, playerIndex) //will return true if they have the best score
			winnerID[playersWon] =  NETWORK_GET_PLAYER_INDEX(GET_GOLF_PLAYER_NET_INDEX_BY_INDEX(thisPlayers, playerIndex))
			playersWon++ //inc the amount of players with the best score
		ENDIF
	ENDREPEAT

	RETURN playersWon
ENDFUNC


PROC HANDLE_GOLF_BETTING_WINS(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], INT &iBetWinnings, INT iPlacement)
	
	PLAYER_INDEX winnerID
	PLAYER_INDEX winnerID4[4]
	
	INT iNumWinners = GET_GOLF_BETTING_DATA(thisFoursome, thisPlayers, winnerID4)
	
	IF iNumWinners = 1
		winnerID = winnerID4[0]
		IF MPGlobals.g_MPBettingData.iYourTotalBet > 0
			iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(winnerID)
			CDEBUG1LN(DEBUG_GOLF,"GOlf Bet winings ", iBetWinnings)
		ENDIF
	ELIF iNumWinners > 1 AND iPlacement = 1
		IF MPGlobals.g_MPBettingData.iYourTotalBet > 0
			iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED_TIED()
			CDEBUG1LN(DEBUG_GOLF,"GOlf Tied for first")
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL GOLF_SHOULD_HANDLE_DISCONNECTED_PLAYER(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_FOURSOME &thisFoursome)
	
	//Someone quit
	IF serverBD.iPlayerWhoQuitIndex <= -1
		RETURN FALSE
	ENDIF

	//it's an apropaite time to quit
	IF IS_GOLF_FOURSOME_STATE_AT_END(thisFoursome)
	OR GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) <= GPS_ADDRESS_BALL
		RETURN TRUE
	ENDIF
	
	INT iLocalPLayerQuitIndex = -1
	INT iFindLocal
	REPEAT COUNT_OF(iGolfPlayingParticipantID) iFindLocal
		IF iGolfPlayingParticipantID[iFindLocal] = serverBD.iPlayerWhoQuitIndex
			iLocalPLayerQuitIndex = iFindLocal
		ENDIF
	ENDREPEAT
	
	//The person who quit is not the current player
	IF iLocalPLayerQuitIndex != GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_GOLF, "The person who disconected had their ball in flight, wait until done")
	RETURN FALSE
ENDFUNC

PROC GOLF_MP_HANDLE_PLAYER_QUITING(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[],
									INT &iDisplayQuitterIndex, BOOL &bRecreatePlayercard, BOOL &bLocalQuit)
	
	INT iLocalPLayerQuitIndex = -1
	PED_INDEX tempPed = NULL
	
	INT iFindLocal
	REPEAT COUNT_OF(iGolfPlayingParticipantID) iFindLocal
		IF iGolfPlayingParticipantID[iFindLocal] = serverBD.iPlayerWhoQuitIndex
			iLocalPLayerQuitIndex = iFindLocal
		ENDIF
	ENDREPEAT
	
	IF NOT ARE_ALL_GOLFER_SPECTATORS(thisFoursome, playerBD, thisPlayers) // if only spectators are left, quit
		//some one quit, display it
		
		IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, PARTICIPANT_ID_TO_INT(), GFC2_DISPLAYING_QUITING_PLAYER)
			
			IF iLocalPlayerQuitIndex  <= -1// make sure valid player quit
				CDEBUG1LN(DEBUG_GOLF,"Player ", iLocalPlayerQuitIndex, " has left the game?")
				SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GFC2_DISPLAYING_QUITING_PLAYER)
			ELSE  
				CDEBUG1LN(DEBUG_GOLF,"Player ", iLocalPlayerQuitIndex, " has left the game!!!!!!!!")
				
				SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GFC2_DISPLAYING_QUITING_PLAYER)
				SET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG(thisFoursome, iLocalPlayerQuitIndex, GOLF_PLAYER_REMOVED)
				
				IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = iLocalPlayerQuitIndex
					IF NETWORK_IS_IN_SPECTATOR_MODE()
						SET_IN_SPECTATOR_MODE(FALSE, tempPed)
					ENDIF
					IF NOT IS_GOLF_FOURSOME_STATE_AT_END(thisFoursome)
	//					DO_SCREEN_FADE_OUT(250)
						RESET_GOLF_MP_APPROACH_CAM(thisHelpers)
						IF DOES_CAM_EXIST(thisHelpers.camAddress)
							DESTROY_CAM(thisHelpers.camAddress)
						ENDIF
					ENDIF
				ENDIF
				
				iDisplayQuitterIndex = iLocalPlayerQuitIndex
				SET_GOLF_CONTROL_FLAG(thisGame, GCF_DISPLAY_QUITTER)
				
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_GOLF_TRAIL)
				
				bRecreatePlayercard = TRUE
			ENDIF
		ENDIF
					
		OVERWRITE_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_NONE)
	ELSE
		CDEBUG1LN(DEBUG_GOLF,"only spectators left, leave golf")
		bLocalQuit = TRUE
	ENDIF
	
ENDPROC



PROC PRINT_GOLF_MP_PLAYER_WHO_QUIT_MATCH(TEXT_LABEL_63 &sPlayerNames[], INT iPlayerQuitIndex)//, GOLF_PLAYER &thisPlayers[])

	CDEBUG1LN(DEBUG_GOLF,"Server index of quiter ", iPlayerQuitIndex)
	CDEBUG1LN(DEBUG_GOLF,"PLayer who quit is ", sPlayerNames[iPlayerQuitIndex])

	TEXT_LABEL_63 texShardPlayerName = ""
    texShardPlayerName = "<C>"
    texShardPlayerName += "~HUD_COLOUR_WHITE~"
    texShardPlayerName += sPlayerNames[iPlayerQuitIndex]
    texShardPlayerName += "</C>"
    texShardPlayerName += "~s~"
	
	PLAY_GOLF_SPLASH_WITH_NAME_IN_STRAPLINE("PARTNER_QUIT_MP", "PARTNER_QUIT_ST", texShardPlayerName, GOLF_SPLASH_SHARD_CENTER, HUD_COLOUR_WHITE)
	//PRINT_HELP_WITH_PLAYER_NAME("PARTNER_QUIT_ST", sPlayerNames[iPlayerQuitIndex], HUD_COLOUR_PURE_WHITE)
ENDPROC

PROC DEBUG_GOLF_MP_CLIENT_FLAGS(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_CONTROL_FLAGS &mySavedGCF, GOLF_CONTROL_FLAGS &savedServerGCF)
	IF GET_GOLF_MP_CLIENT_CONTROL_FLAGS(playerBD, PARTICIPANT_ID_TO_INT()) != mySavedGCF
		CDEBUG1LN(DEBUG_GOLF,"Client has updated his control flags")
		PRINTBITS(ENUM_TO_INT(GET_GOLF_MP_CLIENT_CONTROL_FLAGS(playerBD, PARTICIPANT_ID_TO_INT()))) PRINTLN()
		mySavedGCF = GET_GOLF_MP_CLIENT_CONTROL_FLAGS(playerBD, PARTICIPANT_ID_TO_INT())
	ENDIF
	IF GET_GOLF_MP_SERVER_CONTROL_FLAGS(serverBD) != savedServerGCF
		CDEBUG1LN(DEBUG_GOLF,"Server has updated his control flags")
		PRINTBITS(ENUM_TO_INT(GET_GOLF_MP_SERVER_CONTROL_FLAGS(serverBD)) ) PRINTLN()
		savedServerGCF = GET_GOLF_MP_SERVER_CONTROL_FLAGS(serverBD)
	ENDIF
ENDPROC
