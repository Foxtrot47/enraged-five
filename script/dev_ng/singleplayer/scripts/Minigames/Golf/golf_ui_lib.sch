USING "shared_hud_displays.sch"
USING "golf_ui.sch"
USING "golf_buddies_lib.sch"
USING "golf_course_lib.sch"


PROC GOLF_MANAGE_PRE_UPDATE_UI(GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers)
	CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_AI_HIT_BALL_THIS_FRAME)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_USING_CUTSCENE_SHADOW)
	AND NOT IS_GOLF_CAM_RENDERING(thisHelpers.camFlight)
		CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
		CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE(FALSE)
		CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_USING_CUTSCENE_SHADOW)
	ENDIF
ENDPROC

/// PURPOSE:
///    Manages display of the permanent UI - Wind Strength and direction, current hole info, current shot number for active player, and distance to hole, and current foursome scores
/// PARAMS:
///    thisGame - 
///    thisCourse - 
///    thisPlayers - 
///    thisFoursome - 
PROC GOLF_MANAGE_PERMANENT_UI(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	
	// Update wind every frame
	IF bDebugSpew DEBUG_MESSAGE("UPDATE PERMANENT UI") ENDIF
	
	VECTOR camRot
	BOOL bUpdateWind = TRUE
	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
		camRot = GET_CAM_ROT(GET_RENDERING_CAM())
	ELSE
		bUpdateWind = FALSE
	ENDIF

	FLOAT windRelativeHeading = camRot.z - GET_HEADING_FROM_VECTOR_2D(thisGame.vWindDirection.x, thisGame.vWindDirection.y)
	WHILE windRelativeHeading < 0
		windRelativeHeading+=360.0
	ENDWHILE
	
//	PRINTLN("Wind Calc: cameraDirection = ", camRot, ", WindDirection = ", thisGame.vWindDirection)
//	PRINTLN("Wind Calc: cameraHeading = ", camRot.z, ", WindHeading = ", GET_HEADING_FROM_VECTOR_2D(thisGame.vWindDirection.x, thisGame.vWindDirection.y))
//	PRINTLN("Wind Calc: WindRelativeToCam = ", windRelativeHeading)
//	PRINTLN("Final wind speed is ", windSpeed)
	IF GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_TAKING_SHOT
	AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) < GPS_POST_SHOT_REACTION
		IF ABSF(windRelativeHeading - thisGame.fOldWindHeading) > 0.1 
		OR ABSF(thisGame.fOldWindStrength - thisGame.fWindStrength) > 0.1 
			//PRINTLN("Set WIND UI ", ABSF(thisGame.vOldWindDirection.x - thisGame.vWindDirection.x), " ", ABSF(thisGame.vOldWindDirection.y - thisGame.vWindDirection.y), " ", ABSF(thisGame.fOldWindStrength - thisGame.fWindStrength))
			IF bUpdateWind
				thisGame.fOldWindStrength = thisGame.fWindStrength
				thisGame.fOldWindHeading = windRelativeHeading
			ENDIF
			UPDATE_SHOT_DISPLAY(thisHelpers, thisFoursome, thisGame, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisFoursome.swingMeter)
		ENDIF
	ENDIF
	FLOAT fSetHeading = GET_HEADING_FROM_VECTOR_2D(thisGame.vWindDirection.x, thisGame.vWindDirection.y)
	fSetHeading = DEG_TO_RAD(fSetHeading)
	
//	PRINTLN("Setting wind speed to ", thisGame.fWindStrength, " direction to ", fSetHeading)
	SET_WIND_SPEED(thisGame.fWindStrength)
	SET_WIND_DIRECTION(fSetHeading)
	
	MANAGE_SPLASH_TEXT(thisHelpers, thisFoursome)
	MANAGE_PLAYERCARD(thisHelpers, thisGame)
	
	// Only update the rest of the permanent UI when needed
	IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_REFRESH_PERMANENT)
		EXIT
	ENDIF
	
	INT iCurrentHole = GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)
	INT iDist = ROUND(MAGIC_DISTANCE_CONVERSION*METERS_TO_YARDS(GET_GOLF_HOLE_LENGTH(thisCourse, iCurrentHole)))
	//PRINTLN("Displaying hole info for player " ,GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
	SET_HOLE_DISPLAY(thisHelpers, iCurrentHole+1, GET_GOLF_HOLE_PAR(thisCourse, iCurrentHole), iDist)
	
	CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
ENDPROC

PROC GOLF_MANAGE_NAVIGATE_UI(GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)
	
	IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_REFRESH_NAVIGATE)
		EXIT
	ENDIF
	
	IF bDebugSpew DEBUG_MESSAGE("UPDATE NAVIGATE UI") ENDIF
	
	CLEAR_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_SHOT | GOLF_DISPLAY_HOLE)
	SET_PUTTMETER (thisHelpers, thisHelpers.golfUI)
	SET_DISTANCE (thisHelpers.golfUI)
	CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_NAVIGATE)
	SETUP_GOLF_NAV_CONTROLS(thisHelpers, IS_TEE_SHOT(thisCourse, thisFoursome), IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
ENDPROC

PROC GOLF_PRINT_IN_HAZARD_HELP(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_HELPERS &thisHelpers)
	
	IF NOT IS_SHOT_A_GIMME(thisCourse, thisFoursome)
	AND IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
		
		IF thisHelpers.iNumAutoSwings = (iMaxNumOfAutoSwingsBeforeKick - 1)
		AND GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(thisFoursome) > 1
			CANCEL_TIMER(thisHelpers.uiTimer)
			START_TIMER_NOW(thisHelpers.uiTimer)
			GOLF_PRINT_HELP("GOLF_KICK")
			SET_GOLF_UI_FLAG(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
			SET_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WARNING_KICK_FOR_TIME)
			
		ELIF IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_DISPLAY_UNPLAYABLE_LIE_HELP)
			CANCEL_TIMER(thisHelpers.uiTimer)
			START_TIMER_NOW(thisHelpers.uiTimer)
			GOLF_PRINT_HELP("HAZARD_UNPLAYABLE")
			SET_GOLF_UI_FLAG(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
			CLEAR_GOLF_HELP_DISPLAYED_FLAG(thisHelpers, GHD_DISPLAY_UNPLAYABLE_LIE_HELP)
			SET_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WARNING_UNPLAYABLE_LIE)
		ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome) = GET_GOLF_HOLE_PAR(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) + 2
			CANCEL_TIMER(thisHelpers.uiTimer)
			START_TIMER_NOW(thisHelpers.uiTimer)
			GOLF_PRINT_HELP("NEAR_LIMIT")
			SET_GOLF_UI_FLAG(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
			SET_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WARNING_NEAR_LIMIT)
		ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_ROUGH
			CANCEL_TIMER(thisHelpers.uiTimer)
			START_TIMER_NOW(thisHelpers.uiTimer)
			GOLF_PRINT_HELP("HAZARD_ROUGH")
			SET_GOLF_UI_FLAG(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
			SET_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WARNING_HAZARD_ROUGH)
		ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_BUNKER
			CANCEL_TIMER(thisHelpers.uiTimer)
			START_TIMER_NOW(thisHelpers.uiTimer)
			GOLF_PRINT_HELP("HAZARD_SAND")
			SET_GOLF_UI_FLAG(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
			SET_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WARNING_HAZARD_SAND)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_ANY_GOLF_HAZARD_HELP_DISPLAYED()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HAZARD_UNPLAYABLE")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("NEAR_LIMIT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HAZARD_ROUGH")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HAZARD_SAND")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GOLF_KICK")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_GOLF_CLUB_CHANGE_HELP_DISPLAYED_WITH_EXCEPTION(GOLF_HELP_DISPLAYED_FLAG exceptionFlag)
	IF GHD_NONE = (exceptionFlag & GHD_WOOD_DISPLAYED)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLUB_WOOD")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF GHD_NONE = (exceptionFlag & GHD_IRON_DISPLAYED)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLUB_IRON")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF GHD_NONE = (exceptionFlag & GHD_WEDGE_DISPLAYED)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLUB_WEDGE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF GHD_NONE = (exceptionFlag & GHD_PUTTER_DISPLAYED)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLUB_PUTTER")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_GOLF_STYLE_HELP_DISPLPLAYED_WITH_EXCEPTION(GOLF_HELP_DISPLAYED_FLAG exceptionFlag)
	
	IF GHD_NONE = (exceptionFlag & GHD_STYLE_POWER_DISPLAYED)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STYLE_POWER")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF GHD_NONE = (exceptionFlag & GHD_STYLE_APPROACH_DISPLAYED)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STYLE_APPROACH")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF GHD_NONE = (exceptionFlag & GHD_STLYE_PUNCH_DISPLAYED)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STYLE_PUNCH")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GOLF_PRINT_CHANGED_SHOT_TYPE_HELP(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, BOOL clubChange)
	IF GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) = 0 AND GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome) = 0
	OR NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
		//do not show change shot type help for first shot on first hole OR for other players
		EXIT
	ENDIF
	
	
	CANCEL_TIMER(thisHelpers.uiTimer)
	START_TIMER_NOW(thisHelpers.uiTimer)
	STRING helpText = ""
	GOLF_HELP_DISPLAYED_FLAG displayFlag
	
	IF clubChange
		SWITCH  GET_GOLF_CLUB_TYPE(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
			CASE eCLUB_WOOD_1
			CASE eCLUB_WOOD_2
			CASE eCLUB_WOOD_3
			CASE eCLUB_WOOD_4
			CASE eCLUB_WOOD_5
				displayFlag = GHD_WOOD_DISPLAYED
				helpText = "CLUB_WOOD"
			BREAK
			CASE eCLUB_IRON_2
			CASE eCLUB_IRON_3
			CASE eCLUB_IRON_4
			CASE eCLUB_IRON_5
			CASE eCLUB_IRON_6
			CASE eCLUB_IRON_7
			CASE eCLUB_IRON_8
			CASE eCLUB_IRON_9
				displayFlag = GHD_IRON_DISPLAYED
				helpText = "CLUB_IRON"
			BREAK
			CASE eCLUB_WEDGE_PITCH			
			CASE eCLUB_WEDGE_APPROACH		
			CASE eCLUB_WEDGE_SAND				
			CASE eCLUB_WEDGE_LOB					
			CASE eCLUB_WEDGE_LOB_ULTRA	
				displayFlag = GHD_WEDGE_DISPLAYED
				helpText = "CLUB_WEDGE"
			BREAK
			CASE eCLUB_PUTTER
				displayFlag = GHD_PUTTER_DISPLAYED
				helpText = "CLUB_PUTTER"
			BREAK
		ENDSWITCH
	ELSE
		SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome)
			CASE SWING_STYLE_POWER
				displayFlag = GHD_STYLE_POWER_DISPLAYED
				helpText = "STYLE_POWER"
			BREAK
			CASE SWING_STYLE_APPROACH
				displayFlag = GHD_STYLE_APPROACH_DISPLAYED
				helpText = "STYLE_APPROACH"
			BREAK
			CASE SWING_STYLE_PUNCH
				displayFlag = GHD_STLYE_PUNCH_DISPLAYED
				helpText = "STYLE_PUNCH"
			BREAK
		ENDSWITCH
	ENDIF
	
	//Player has switched to style normal or the style we are changing to has already been displayed
	IF (IS_STRING_EMPTY(helpText) OR IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, displayFlag)) 
	AND ((IS_ANY_GOLF_STYLE_HELP_DISPLPLAYED_WITH_EXCEPTION(GHD_NONE) AND NOT clubChange //a style change is up and we are changing are style
		OR IS_ANY_GOLF_CLUB_CHANGE_HELP_DISPLAYED_WITH_EXCEPTION(GHD_NONE) AND clubChange)) //a club change was displaying and we are switching clubs
		CLEAR_GOLF_UI_FLAG (thisHelpers, GUC_STYLE_HELP_DISPLAYED)
	ELIF NOT IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, displayFlag) //the help you want to display has never been up before
		AND NOT IS_STRING_EMPTY(helpText)
		SET_GOLF_UI_FLAG(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
		IF NOT IS_STRING_EMPTY(helpText)
			GOLF_PRINT_HELP(helpText, FALSE, TRUE)
			thisHelpers.golfHelpDisplaying = GHD_NONE
			SET_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, displayFlag)
		ENDIF
	ELIF NOT clubChange
		CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
	ENDIF
	
ENDPROC

PROC SET_GOLF_HELP_AS_DISPLAYED(GOLF_HELPERS &thisHelpers)
	GOLF_HELP_DISPLAYED_FLAG ghdFlag = GHD_NONE
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLUB_WOOD")
		ghdFlag = GHD_WOOD_DISPLAYED
	ENDIF

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLUB_IRON")
		ghdFlag = GHD_IRON_DISPLAYED
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLUB_WEDGE")
		ghdFlag = GHD_WEDGE_DISPLAYED
	ENDIF

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLUB_PUTTER")
		ghdFlag = GHD_PUTTER_DISPLAYED
	ENDIF

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STYLE_POWER")
		ghdFlag = GHD_STYLE_POWER_DISPLAYED
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STYLE_APPROACH")
		ghdFlag = GHD_STYLE_APPROACH_DISPLAYED
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STYLE_PUNCH")
		ghdFlag = GHD_STLYE_PUNCH_DISPLAYED
	ENDIF
		
	SET_GOLF_HELP_DISPLAYED_FLAG(thisHelpers, ghdFlag)

ENDPROC
	 
PROC RESTART_GOLF_HELP_DISPLAYING(GOLF_HELPERS &thisHelpers)

	IF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WOOD_DISPLAYED)
		GOLF_PRINT_HELP("CLUB_WOOD")
	ELIF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_IRON_DISPLAYED)
		GOLF_PRINT_HELP("CLUB_IRON")
	ELIF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WEDGE_DISPLAYED)
		GOLF_PRINT_HELP("CLUB_WEDGE")
	ELIF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_PUTTER_DISPLAYED)
		GOLF_PRINT_HELP("CLUB_PUTTER")
	ELIF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_STYLE_POWER_DISPLAYED)
		GOLF_PRINT_HELP("STYLE_POWER")
	ELIF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_STYLE_APPROACH_DISPLAYED)
		GOLF_PRINT_HELP("STYLE_APPROACH")
	ELIF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_STLYE_PUNCH_DISPLAYED)
		GOLF_PRINT_HELP("STYLE_PUNCH")
	ELIF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WARNING_UNPLAYABLE_LIE)
		GOLF_PRINT_HELP("HAZARD_UNPLAYABLE")
	ELIF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WARNING_NEAR_LIMIT)
		GOLF_PRINT_HELP("NEAR_LIMIT")
	ELIF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WARNING_HAZARD_ROUGH)
		GOLF_PRINT_HELP("HAZARD_ROUGH")
	ELIF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WARNING_HAZARD_SAND)
		GOLF_PRINT_HELP("HAZARD_SAND")	
	ELIF IS_GOLF_HELP_CURRENTLY_DISPLAYING(thisHelpers, GHD_WARNING_KICK_FOR_TIME)
		GOLF_PRINT_HELP("GOLF_KICK")
	ENDIF
	
ENDPROC

PROC GOLF_RESET_STYLE_HELP(GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers)
	
	IF IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
	AND thisHelpers.bGreenPreviewCam = 0
	AND NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED)
	AND NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_MP_START_QUIT_MENU)
		IF NOT IS_ANY_GOLF_HAZARD_HELP_DISPLAYED() 
		AND NOT IS_ANY_GOLF_STYLE_HELP_DISPLPLAYED_WITH_EXCEPTION(GHD_NONE)
		AND NOT IS_ANY_GOLF_CLUB_CHANGE_HELP_DISPLAYED_WITH_EXCEPTION(GHD_NONE)
			//PRINTLN("Style help should be displayed but isn't")
			
			RESTART_GOLF_HELP_DISPLAYING(thisHelpers)
			RESTART_TIMER_NOW(thisHelpers.uiTimer)
		ENDIF
		
		IF TIMER_DO_WHEN_READY(thisHelpers.uiTimer, 2.5)
			SET_GOLF_HELP_AS_DISPLAYED(thisHelpers)
		ENDIF
	
		IF TIMER_DO_ONCE_WHEN_READY(thisHelpers.uiTimer, 7.5)
			PRINTLN("reset timer")
			CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Manages the display of the UI specific to addressing the ball - current club and shot style
/// PARAMS:
///    thisPlayerCore - 
///    golfBag - 
PROC GOLF_MANAGE_ADDRESS_UI(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	FLOAT fXPos, fYPos
	UNUSED_PARAMETER(thisCourse)
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_ADDRESS_BALL
		GOLF_RESET_STYLE_HELP(thisGame, thisHelpers)
	ENDIF

	fXPos = 0.65
	fYPos = 0.411
	
	IF IS_PHONE_ONSCREEN()
		HANG_UP_AND_PUT_AWAY_PHONE()
	ENDIF
	 
	IF NOT IS_SHOT_A_GIMME(thisCourse, thisFoursome) AND NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED)
	AND thisHelpers.bGreenPreviewCam = 0 //AND NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_DISPLAY_QUITTER)
		IF NOT ((GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_LOCAL AND GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_LOCAL_MP) 
		   AND (GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_ADDRESS_BALL OR GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_SWING_METER))
		   #IF GOLF_IS_MP AND thisHelpers.bTransitionToShotCamOver #ENDIF
			DRAW_SCALEFORM_MOVIE(thisHelpers.floatingUI, fXPos,fYPos,1.0,1.0,255,255,255,0)
		ENDIF
	ENDIF
	
	IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_REFRESH_ADDRESS)
	OR IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED)
		EXIT
	ENDIF
	
	IF bDebugSpew DEBUG_MESSAGE("UPDATE ADDRESS UI") ENDIF
	SET_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_HOLE | GOLF_DISPLAY_SHOT | GOLF_DISPLAY_METER)
	DISPLAY_SCOREBOARD(thisHelpers, FALSE)

	thisHelpers.eShotDisplayState = SHOT_DISPLAY_LIE | SHOT_DISPLAY_WIND | SHOT_DISPLAY_CLUB | SHOT_DISPLAY_SWING | SHOT_DISPLAY_SHOT_NUM
	UPDATE_SHOT_DISPLAY(thisHelpers, thisFoursome, thisGame, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisFoursome.swingMeter)
	
	CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_ADDRESS)
ENDPROC



/// PURPOSE:
///    Manages the display of the UI specific to taking a swing - spin meter, and if 3 click, the swing meter itself
/// PARAMS:
///    swingMeter - 
///    thisPlayer - 
///    thisPlayerCore - 
///    golfBag - 
PROC GOLF_MANAGE_SWING_UI(GOLF_COURSE &thisCourse, GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers, SWING_METER &swingMeter, GOLF_PLAYER &thisPlayer, GOLF_FOURSOME &thisFoursome)
	IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_REFRESH_SWING)
	OR NOT IS_SCREEN_FADED_IN()
		EXIT
	ENDIF
	IF bDebugSpew DEBUG_MESSAGE("UPDATE SWING UI") ENDIF
	UNUSED_PARAMETER(thisPlayer)
	FLOAT flagPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_DISTANCE_TO_HOLE(thisFoursome) / swingMeter.fMaxDistance
	IF flagPos > 1.0
		flagPos = 1.0
	ENDIF

	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)  
	AND NOT IS_SHOT_A_GIMME(thisCourse, thisFoursome) AND thisHelpers.bGreenPreviewCam = 0 // AND NOT thisHelpers.bShotPreviewCam
	AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PLAYER_TIME_OUT)
		IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
			DRAW_PUTT_METER(TRUE, thisHelpers, swingMeter, GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL( thisFoursome))
		ELSE
			DRAW_SWING_METER(TRUE, thisHelpers, swingMeter,GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL( thisFoursome))
		ENDIF
	ELSE
		DRAW_PUTT_METER(FALSE, thisHelpers, swingMeter, GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL( thisFoursome))
	ENDIF
	
	thisHelpers.eShotDisplayState = SHOT_DISPLAY_LIE | SHOT_DISPLAY_WIND | SHOT_DISPLAY_CLUB | SHOT_DISPLAY_SWING | SHOT_DISPLAY_SHOT_NUM
	UPDATE_SHOT_DISPLAY(thisHelpers, thisFoursome, thisGame, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisFoursome.swingMeter)
	CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_SWING)
ENDPROC


/// PURPOSE:
///    Manages the display of the UI while the ball is in flight - current shot distance, carry distance (for tee shots), records, and spin meter (if using stick swing)
/// PARAMS:
///    thisGame - 
///    thisCourse - 
///    thisPlayer - 
///    thisFoursome - 
PROC GOLF_MANAGE_FLIGHT_UI(GOLF_HELPERS &thisHelpers, GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)
	IF NOT DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)) 
	OR GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_POST_SHOT_REACTION
		EXIT
	ENDIF
	GOLFUI_FLOATING_UPDATE_HIT_DISTANCE(thisHelpers.floatingUI, thisFoursome)

	IF IS_TEE_SHOT(thisCourse, thisFoursome) AND GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_CARRY(thisFoursome) > 0
		GOLFUI_FLOATING_INIT_CARRY_DISTANCE(thisHelpers.floatingUI, thisFoursome)
	ENDIF
	FLOAT fXPos, fYPos
	FLOAT fMeterGoal = 8.85 //GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome)

	fXPos = 0.65
	fYPos = 0.415 + ((0.25) * fMeterGoal/100.0)

	IF NOT IS_SHOT_A_GIMME(thisCourse, thisFoursome)
		DRAW_SCALEFORM_MOVIE(thisHelpers.floatingUI, fXPos,fYPos,1.0,1.0,255,255,255,0)
	ENDIF
	
	IF bDebugSpew DEBUG_MESSAGE("UPDATE FLIGHT UI") ENDIF
	
//	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_BALL_IN_FLIGHT
//		CLEAR_GOLF_SPLASH()
//	ENDIF

	thisHelpers.eShotDisplayState = SHOT_DISPLAY_WIND | SHOT_DISPLAY_SPIN
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)  AND NOT IS_SHOT_A_GIMME(thisCourse, thisFoursome)
		SET_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_SHOT)
		UPDATE_SHOT_DISPLAY(thisHelpers, thisFoursome, thisGame, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisFoursome.swingMeter)
		DRAW_PUTT_METER(FALSE, thisHelpers, thisFoursome.swingMeter, GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL( thisFoursome))

	ELSE
		CLEAR_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_SHOT)
		//DRAW_SPIN_METER(thisHelpers, thisFoursome.swingMeter)
		//DRAW_PUTT_METER(FALSE, thisHelpers, thisFoursome.swingMeter, GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL( thisFoursome))
	ENDIF

ENDPROC

PROC GOLF_DO_BALL_IN_HOLE_SPLASH_AND_SOUND(GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse, GOLF_PLAYER &thisPlayer, GOLF_FOURSOME &thisFoursome, BOOL bPreScore = TRUE)
	INT shotDiff = GET_GOLF_PLAYER_HOLE_SCORE(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) - GET_GOLF_HOLE_PAR(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
	INT holePar = GET_GOLF_HOLE_PAR(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
	UNUSED_PARAMETER(thisGame)
	INT iXPBonus = 0
	
	TEXT_LABEL txtShot = "SCORE_"
	TEXT_LABEL txtXP = "XPT_ENDHOLE"
	
	GOLF_SPLASH_TEXT_ENUM eSplashEnum
	IF bPreScore
		shotDiff++
	ENDIF
	SWITCH shotDiff
		CASE -4		
			IF holePar = 5 // Like this will ever happen
				txtShot += "HOLEINONE"
				txtXP += "_1"
				eSplashEnum = GOLF_SPLASH_HOLE_IN_ONE
				iXPBonus = 1000
			ENDIF
		BREAK
		CASE -3
			IF holePar = 4
				txtShot += "HOLEINONE"
				txtXP += "_1"
				eSplashEnum = GOLF_SPLASH_HOLE_IN_ONE
				iXPBonus = 1000
			ELSE
				txtShot += "EAGLE2"
				txtXP += "_E2"
				eSplashEnum = GOLF_SPLASH_EAGLE2
				iXPBonus = 400
			ENDIF
		BREAK
		CASE -2
			IF holePar = 3
				txtShot += "HOLEINONE"
				txtXP += "_1"
				eSplashEnum = GOLF_SPLASH_HOLE_IN_ONE
				iXPBonus = 1000
			ELSE
				txtShot += "EAGLE"
				txtXP += "_E"
				eSplashEnum = GOLF_SPLASH_EAGLE
				iXPBonus = 300
			ENDIF
		BREAK
		CASE -1
			txtShot += "BIRDIE"
			txtXP += "_B"
			eSplashEnum = GOLF_SPLASH_BIRDIE
			iXPBonus = 200
		BREAK
		CASE 0
			txtShot += "PAR"
			txtXP += "_P"
			eSplashEnum = GOLF_SPLASH_PAR
			iXPBonus = 100
		BREAK
		CASE 1
			txtShot += "BOGEY"
			txtXP += "_BO"
			eSplashEnum = GOLF_SPLASH_BOGEY
			iXPBonus = 50
		BREAK
		CASE 2
			txtShot += "BOGEY2"
			eSplashEnum = GOLF_SPLASH_BOGEY2
		BREAK
		CASE 3
			txtShot += "BOGEY3"
			eSplashEnum = GOLF_SPLASH_BOGEY3
		BREAK
		CASE 4
			txtShot += "BOGEY4"
			eSplashEnum = GOLF_SPLASH_BOGEY4
		BREAK
		CASE 5
			txtShot += "BOGEY5"
			eSplashEnum = GOLF_SPLASH_BOGEY5
		BREAK
		DEFAULT
//			txtShot = "CARD_SCORE"
//			SET_SPLASH( thisHelpers.golfUI, 1500, txtShot)
			EXIT
		BREAK
	ENDSWITCH
	
	IF IS_GOLF_FOURSOME_MP()
		INT iFinalXp =  ROUND(g_sMPTunables.fxp_tunable_Minigames_ALL_Golf_Bonuses  * (100 + iXPBonus))
		PRINTLN("Giving player ", 100 + iFinalXp, " XP ", txtXP  )
		thisHelpers.iXPGained += GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, txtXP, XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_A_GOLF_HOLE,  iFinalXp)
	ENDIF
	INT iDuration = 2000
	
	SET_SPLASH_TEXT(thisHelpers, eSplashEnum, txtShot, "", -1, iDuration)
ENDPROC


PROC GOLF_DISPLAY_SCORE_LIMIT_SPLASH(GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)

	TEXT_LABEL_23 sStripLine = ""
	GOLF_OOB_REASON OOBReason
	
	IF IS_GOLF_BALL_OOB(thisFoursome, thisCourse, OOBReason)
		IF IS_BALL_IN_WATER_HAZARD(thisFoursome) 
			sStripLine = "LIE_WATER"
		ELIF IS_GOLF_LIE_UNPLAYABLE(GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome))
			sStripLine = "LIE_UNPLAYABLE"
		ELSE
			sStripLine = "LIE_UNKNOWN"
		ENDIF
	ENDIF

	SET_SPLASH_TEXT(thisHelpers, GOLF_SPLASH_SCORE_LIMIT, "SCORE_LIMIT", sStripLine)
ENDPROC

/// PURPOSE:
///    Manages the display of the UI when the ball goes in the hole - lists the overall score for the hole
/// PARAMS:
///    thisCourse - 
///    thisPlayer - 
///    thisFoursome - 
PROC GOLF_MANAGE_BALL_IN_HOLE_UI(GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse, GOLF_PLAYER &thisPlayer, GOLF_FOURSOME &thisFoursome)
	IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_REFRESH_IN_HOLE)
		EXIT
	ENDIF
	//PRINTLN("UPDATE IN HOLE UI")
	IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_STROKE_LIMIT)
		GOLF_DO_BALL_IN_HOLE_SPLASH_AND_SOUND(thisGame, thisHelpers, thisCourse, thisPlayer, thisFoursome, TRUE)
	ELSE
		GOLF_DISPLAY_SCORE_LIMIT_SPLASH(thisHelpers, thisCourse, thisFoursome)
	ENDIF

	IF DOES_BLIP_EXIST(GET_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers))
		CLEANUP_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers)
	ENDIF
	
	SET_PUTTMETER (thisHelpers, thisHelpers.golfUI)
	CLEAR_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_SHOT)
	SET_DISTANCE (thisHelpers.golfUI)
	CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_IN_HOLE)
ENDPROC




/// PURPOSE:
///    Manages the display of the UI for the quit confirmation screen - a box with 'quit?' message
PROC GOLF_MANAGE_QUIT_UI(GOLF_HELPERS &thisHelpers)

	IF bDebugSpew DEBUG_MESSAGE("UPDATE QUIT UI") ENDIF

	DRAW_RECT_FROM_CORNER(0.1, 0.3, 0.8, 0.4, 0,0,0,128) // Shot marker

	SET_TEXT_CENTRE(TRUE)
	SET_TEXT_SCALE(0.6, 0.6)
	DISPLAY_TEXT(0.5, 0.45, "QUIT")
	SET_PUTTMETER (thisHelpers.golfUI)
	SET_DISTANCE (thisHelpers.golfUI)
ENDPROC

PROC GOLF_MANAGE_SCOREBOARD_UI(GOLF_HELPERS &thisHelpers, GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_MINIGAME_STATE &golfState)

	BOOL bExitScoreCard = FALSE
	
	// Fix for B* 2309115
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF

	IF golfState = GMS_PLAY_GOLF 
	AND (GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_NAVIGATE_TO_SHOT OR GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_TAKING_SHOT)
	AND	IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB)
	AND NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED) 
	AND (GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) < GPS_SWING_METER AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) > GPS_APPROACH_ANIM OR GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_TAKING_SHOT)
	AND NOT thisHelpers.bShotPreviewCam AND thisHelpers.bGreenPreviewCam = 0
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	
		PRINTLN("Go To scoreboard state")
		DISPLAY_SCOREBOARD(thisHelpers, TRUE)
		SETUP_GOLF_SCOREBOARD_CONTROLS(thisHelpers)
		
		PLAY_SOUND_FRONTEND(-1, "HIGHLIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		GOLF_TRAIL_SET_ENABLED(FALSE)
		IF golfState != GMS_EXIT_OK
			golfState = GMS_SCOREBOARD_OVERRIDE
		ENDIF
		EXIT
	ENDIF
	
	// Have to use a different input for PC as RRIGHT conflicts with the reset shot key on PC.
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		bExitScoreCard = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB)
	ELSE
		bExitScoreCard = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
		#IF IS_JAPANESE_BUILD		
			IF IS_PLAYSTATION_PLATFORM()  bExitScoreCard = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN) ENDIF
		#ENDIF
	ENDIF
	
	BOOL bOutofBounds = FALSE
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF IS_GOLF_COORD_OUTSIDE_BOUNDARY_PLANES(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
		AND IS_GOLF_COORD_OUTSIDE_HOLE_ONE_BOUNDING_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
			bOutofBounds = TRUE
		ENDIF
	ENDIF
	
	IF (bExitScoreCard OR bOutofBounds)
	AND IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED) 
	AND GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_GAME AND GET_GOLF_FOURSOME_STATE(thisFoursome) !=GGS_SCORECARD_END_HOLE
		
		PRINTLN("Exit scoreboard")
		DISPLAY_SCOREBOARD(thisHelpers, FALSE)
		IF thisHelpers.bGreenPreviewCam = 0 //dont draw trail if you are in the middle of the green preview
			SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_GOLF_TRAIL)
		ENDIF
		
		IF IS_TIMER_PAUSED(thisHelpers.uiTimer)
			UNPAUSE_TIMER(thisHelpers.uiTimer)
		ENDIF
		DISPLAY_RADAR(TRUE)
		CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_INPUT)
		
		IF GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_NAVIGATE_TO_SHOT
			SETUP_GOLF_NAV_CONTROLS(thisHelpers, IS_TEE_SHOT(thisCourse, thisFoursome), IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
		ENDIF
		
		IF golfState != GMS_EXIT_OK
			golfState = GMS_PLAY_GOLF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ANY_GOLF_CLUB_HELP_BEEN_DISPLAYED(GOLF_HELPERS &thisHelpers)

	RETURN (IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_WOOD_DISPLAYED)
			OR IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_IRON_DISPLAYED)
			OR IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_WEDGE_DISPLAYED)
			OR IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_PUTTER_DISPLAYED))
ENDFUNC

FUNC BOOL HAS_ANY_GOLF_SWING_STYLE_HELP_BEEN_DISPLAYED(GOLF_HELPERS &thisHelpers)
	
	RETURN (IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_STYLE_POWER_DISPLAYED)
			OR IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_STYLE_APPROACH_DISPLAYED)
			OR IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_STLYE_PUNCH_DISPLAYED) )

ENDFUNC

#IF NOT GOLF_IS_AI_ONLY
PROC SET_GOLF_PLAYER_CARD_CURRENT_PLAYER(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[], INT iPlayerIndex= -1, BOOL bForceThisHole = FALSE)

	IF iPlayerIndex = -1
		iPlayerIndex= GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
	ENDIF
	INT index
	BOOL bNextHole = FALSE
	INT iNetIndex = -1
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) index
		IF GET_GOLF_PLAYER_STATE(thisFoursome.playerCore[index]) = GPS_DONE_WITH_HOLE
		AND NOT bForceThisHole
			bNextHole = TRUE
		ELSE
			bNextHole = FALSE
		ENDIF
		
		IF IS_GOLF_FOURSOME_MP()
			iNetIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, index)
		ENDIF

		IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, index, GOLF_PLAYER_REMOVED)
			SET_PLAYERCARD_SLOT(thisHelpers, thisFoursome, thisFoursome.playerCore[index].playerCard, GET_GOLF_PLAYER_CURRENT_SCORE(thisPlayers[index]), IS_GOLF_FOURSOME_MP(), bNextHole, iNetIndex)
		ENDIF
	ENDREPEAT

ENDPROC

FUNC BLIP_SPRITE GET_BLIP_SPRITE_FOR_SHOT_ESTIMATE(GOLF_FOURSOME &thisFoursome, VECTOR vCoordPos)

	IF IS_GOLF_LIE_SAFE(GET_GOLF_INTERSECT_LIE())
	AND NOT IS_GOLF_COORD_OUTSIDE_BOUNDARY_PLANES(vCoordPos)
	AND NOT IS_GOLF_COORD_OUTSIDE_HOLE_BOUNDING_SPHERES(vCoordPos, thisFoursome)
		RETURN RADAR_TRACE_AIM
	ENDIF
	
	RETURN RADAR_TRACE_DEAD
ENDFUNC

#ENDIF
