

INT iGolfReaction
VECTOR vGolfReactScenePosition
FLOAT fGoflReactSceneHeaing

PROC GOLF_END_REACTION_SYNC_SCENE()

	IF iGolfReaction = -1
		EXIT //invalid scene id
	ENDIF

	#IF GOLF_IS_MP
		NETWORK_STOP_SYNCHRONISED_SCENE(iGolfReaction)
	#ENDIF
	
	iGolfReaction = -1
ENDPROC

PROC GOLF_CREATE_REACTION_SYNC_SCENE(BOOL bHoldLastFrame)
	
	#IF GOLF_IS_MP
		iGolfReaction = NETWORK_CREATE_SYNCHRONISED_SCENE(vGolfReactScenePosition, <<0, 0, fGoflReactSceneHeaing>>, EULER_YXZ, bHoldLastFrame)
	#ENDIF
	
	#IF NOT GOLF_IS_MP
		iGolfReaction = CREATE_SYNCHRONIZED_SCENE(vGolfReactScenePosition, <<0, 0, fGoflReactSceneHeaing>>)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iGolfReaction, bHoldLastFrame)
	#ENDIF

ENDPROC

PROC GOLF_TASK_REACTION_SYNC_SCENE(PED_INDEX pedGolfer, STRING sAnimDic, STRING sAnimName,  FLOAT blendInDelta, FLOAT blendOutDelta, SYNCED_SCENE_PLAYBACK_FLAGS flags)
	IF NOT DOES_ENTITY_EXIST(pedGolfer) OR IS_ENTITY_DEAD(pedGolfer)
		EXIT
	ENDIF
	
	#IF GOLF_IS_MP
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedGolfer, iGolfReaction, sAnimDic, sAnimName, blendInDelta, blendOutDelta, flags)
	#ENDIF
	
	#IF NOT GOLF_IS_MP
		TASK_SYNCHRONIZED_SCENE(pedGolfer, iGolfReaction, sAnimDic, sAnimName, blendInDelta, blendOutDelta, flags)
	#ENDIF
	
ENDPROC

PROC GOLF_SET_REACTION_SYNC_SCENE_CAM(CAMERA_INDEX reactionCam, STRING sAnimDic, STRING sAnimName)
	
	UNUSED_PARAMETER(reactionCam)
	UNUSED_PARAMETER(sAnimDic)
	UNUSED_PARAMETER(sAnimName)
	#IF NOT GOLF_IS_MP
		PLAY_SYNCHRONIZED_CAM_ANIM(reactionCam, iGolfReaction, sAnimName, sAnimDic) 
		SET_CAM_ACTIVE(reactionCam, TRUE)
	#ENDIF
	
ENDPROC

PROC GOLF_START_REACTION_SYNC_SCENE()

	#IF GOLF_IS_MP
		NETWORK_START_SYNCHRONISED_SCENE(iGolfReaction)
	#ENDIF

ENDPROC

FUNC BOOL IS_GOLF_REACTION_SCENE_RUNNING()
	INT iLocalSceneID = iGolfReaction
	
	#IF GOLF_IS_MP
		iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iGolfReaction)
	#ENDIF
	
	RETURN IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)			
ENDFUNC

FUNC FLOAT GET_GOLF_REACTION_SCENE_PHASE()
	
	INT iLocalSceneID = iGolfReaction
	
	#IF GOLF_IS_MP
		iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iGolfReaction)
	#ENDIF
	
	IF IS_GOLF_REACTION_SCENE_RUNNING()
		RETURN GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID)
	ENDIF
	
	RETURN -1.0
ENDFUNC



FUNC STRING GET_GOLF_REACTION_CAMERA_FROM_PED(PED_INDEX pedGolfer, TEXT_LABEL_63 &sAnimDict, BOOL bIsPutting, FLOAT &AdjustHeading)
	
	IF GET_GOLF_PED_ENUM(pedGolfer) = CHAR_MICHAEL
		IF bIsPutting
			IF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_putter_01_michael")
				RETURN "react_lose_putter_01_michael_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_putter_02_michael")
				RETURN "react_lose_putter_02_michael_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_putter_03_michael")
				RETURN "react_lose_putter_03_michael_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_putter_01_michael")
				RETURN "react_win_putter_01_michael_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_putter_02_michael")
				RETURN "react_win_putter_02_michael_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_putter_03_michael")
				RETURN "react_win_putter_03_michael_cam"
			ENDIF
		ELSE
			IF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_club_01_michael")
				RETURN "react_lose_club_01_michael_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_club_02_michael")
				RETURN "react_lose_club_02_michael_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_club_03_michael")
				RETURN "react_lose_club_03_michael_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_01_michael")
				RETURN "react_win_club_01_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_02_michael")
				RETURN "react_win_club_02_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_03_michael")
				RETURN "react_win_club_03_cam"
			ENDIF
		ENDIF
	ELIF GET_GOLF_PED_ENUM(pedGolfer) = CHAR_FRANKLIN
		IF bIsPutting
			IF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_putter_01_franklin_frank")
				RETURN "react_lose_putter_01_franklin_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_putter_02_franklin_frank")
				RETURN "react_lose_putter_02_franklin_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_putter_03_franklin_frank")
				RETURN "react_lose_putter_03_franklin_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_putter_01_franklin_frank")
				RETURN "react_win_putter_01_franklin_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_putter_02_franklin_frank")
				RETURN "react_win_putter_02_franklin_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_putter_03_franklin_frank")
				RETURN "react_win_putter_03_franklin_cam"
			ENDIF
		ELSE
			IF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_club_01_franklin")
				RETURN "react_lose_club_01_franklin_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_club_02_franklin")
				RETURN "react_lose_club_02_franklin_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_club_03_franklin")
				RETURN "react_lose_club_03_franklin_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_01_franklin")
				RETURN "react_win_club_01_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_02_franklin")
				RETURN "react_win_club_02_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_03_franklin")
				RETURN "react_win_club_03_cam"
			ENDIF
		ENDIF
	ELIF GET_GOLF_PED_ENUM(pedGolfer) = CHAR_TREVOR
		IF bIsPutting
			IF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_putter_01_trevor")
				RETURN "react_lose_putter_01_trevor_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_putter_02_trevor")
				RETURN "react_lose_putter_02_trevor_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_putter_03_trevor")
				RETURN "react_lose_putter_03_trevor_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_putter_01_trevor")
				RETURN "react_win_putter_01_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_putter_02_trevor")
				RETURN "react_win_putter_02_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_putter_03_trevor")
				RETURN "react_win_putter_03_cam"
			ENDIF
		ELSE
			IF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_club_01_trevor")
				RETURN "react_lose_club_01_trevor_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_club_02_trevor")
				RETURN "react_lose_club_02_trevor_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_lose_club_03_trevor")
				RETURN "react_lose_club_03_trevor_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_01_trevor")
				RETURN "react_win_club_01_trevor_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_02_trevor")
				RETURN "react_win_club_02_trevor_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_03_trevor")
				RETURN "react_win_club_03_trevor_cam"
			ENDIF
		ENDIF
	ELSE
	
		IF bIsPutting
			IF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "putt_react_bad_01")
				RETURN "putt_react_bad_01_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "putt_react_bad_02")
				RETURN "putt_react_bad_02_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "putt_react_bad_03")
				RETURN "putt_react_bad_03_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "putt_react_good_01")
				RETURN "putt_react_good_01_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "putt_react_good_02")
				RETURN "putt_react_good_02_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "putt_react_good_03")
				RETURN "putt_react_good_03_cam"
			ENDIF
		ELSE
			AdjustHeading -= 90
			IF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "swing_react_bad_01")
				RETURN "swing_react_bad_01_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "swing_react_bad_02")
				RETURN "swing_react_bad_02_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "swing_react_good_01")
				RETURN "swing_react_good_01_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "swing_react_good_02")
				RETURN "swing_react_good_01_cam" // there is no swing react good two cam
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "wedge_react_bad_01")
				RETURN "wedge_react_bad_01_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "wedge_react_bad_02")
				RETURN "wedge_react_bad_02_cam"
			ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "wedge_swing_good_01")
				RETURN "wedge_react_good_01_cam"
			ENDIF
		ENDIF
		
		IF bIsPutting
			RETURN "putt_react_good_01_cam"
		ELSE
			RETURN "swing_react_good_01_cam"
		ENDIF
		
	ENDIF
	
	RETURN ""
ENDFUNC




FUNC STRING GET_GOLF_REACTION_CAMERA_FROM_STRING(TEXT_LABEL_63 &sAnimDict, STRING sAnimName)
	
	IF ARE_STRINGS_EQUAL(sAnimDict, "mini@golfreactions@michael@putter")
		IF ARE_STRINGS_EQUAL(sAnimName, "react_lose_putter_01_michael")
			RETURN "react_lose_putter_01_michael_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_lose_putter_02_michael")
			RETURN "react_lose_putter_02_michael_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName,"react_lose_putter_03_michael")
			RETURN "react_lose_putter_03_michael_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_putter_01_michael")
			RETURN "react_win_putter_01_michael_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_putter_02_michael")
			RETURN "react_win_putter_02_michael_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_putter_03_michael")
			RETURN "react_win_putter_03_michael_cam"
		ENDIF
	ELIF ARE_STRINGS_EQUAL(sAnimDict, "mini@golfreactions@michael@clubs")
		IF ARE_STRINGS_EQUAL(sAnimName, "react_lose_club_01_michael")
			RETURN "react_lose_club_01_michael_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_lose_club_02_michael")
			RETURN "react_lose_club_02_michael_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_lose_club_03_michael")
			RETURN "react_lose_club_03_michael_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_club_01_michael")
			RETURN "react_win_club_01_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_club_02_michael")
			RETURN "react_win_club_02_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_club_03_michael")
			RETURN "react_win_club_03_cam"
		ENDIF
	ELIF ARE_STRINGS_EQUAL(sAnimDict, "mini@golfreactions@franklin@putter")

		IF ARE_STRINGS_EQUAL(sAnimName, "react_lose_putter_01_franklin_frank")
			RETURN "react_lose_putter_01_franklin_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_lose_putter_02_franklin_frank")
			RETURN "react_lose_putter_02_franklin_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_lose_putter_03_franklin_frank")
			RETURN "react_lose_putter_03_franklin_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_putter_01_franklin_frank")
			RETURN "react_win_putter_01_franklin_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_putter_02_franklin_frank")
			RETURN "react_win_putter_02_franklin_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_putter_03_franklin_frank")
			RETURN "react_win_putter_03_franklin_cam"
		ENDIF
	ELIF ARE_STRINGS_EQUAL(sAnimDict, "mini@golfreactions@franklin@clubs")
		IF ARE_STRINGS_EQUAL(sAnimName, "react_lose_club_01_franklin")
			RETURN "react_lose_club_01_franklin_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_lose_club_02_franklin")
			RETURN "react_lose_club_02_franklin_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_lose_club_03_franklin")
			RETURN "react_lose_club_03_franklin_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_club_01_franklin")
			RETURN "react_win_club_01_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_club_02_franklin")
			RETURN "react_win_club_02_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_club_03_franklin")
			RETURN "react_win_club_03_cam"
		ENDIF
	ELIF ARE_STRINGS_EQUAL(sAnimDict, "mini@golfreactions@trevor@clubs")

		IF ARE_STRINGS_EQUAL(sAnimName, "react_lose_putter_01_trevor")
			RETURN "react_lose_putter_01_trevor_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_lose_putter_02_trevor")
			RETURN "react_lose_putter_02_trevor_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_lose_putter_03_trevor")
			RETURN "react_lose_putter_03_trevor_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_putter_01_trevor")
			RETURN "react_win_putter_01_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_putter_02_trevor")
			RETURN "react_win_putter_02_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_putter_03_trevor")
			RETURN "react_win_putter_03_cam"
		ENDIF
	ELIF ARE_STRINGS_EQUAL(sAnimDict, "mini@golfreactions@trevor@clubs")
		IF ARE_STRINGS_EQUAL(sAnimName, "react_lose_club_01_trevor")
			RETURN "react_lose_club_01_trevor_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_lose_club_02_trevor")
			RETURN "react_lose_club_02_trevor_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_lose_club_03_trevor")
			RETURN "react_lose_club_03_trevor_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_club_01_trevor")
			RETURN "react_win_club_01_trevor_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_club_02_trevor")
			RETURN "react_win_club_02_trevor_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "react_win_club_03_trevor")
			RETURN "react_win_club_03_trevor_cam"
		ENDIF
	ELIF ARE_STRINGS_EQUAL(sAnimDict, "mini@golfreactions@generic@")
	
		IF ARE_STRINGS_EQUAL(sAnimName, "putt_react_bad_01")
			RETURN "putt_react_bad_01_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "putt_react_bad_02")
			RETURN "putt_react_bad_02_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "putt_react_bad_03")
			RETURN "putt_react_bad_03_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "putt_react_good_01")
			RETURN "putt_react_good_01_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "putt_react_good_02")
			RETURN "putt_react_good_02_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "putt_react_good_03")
			RETURN "putt_react_good_03_cam"	
		ELIF ARE_STRINGS_EQUAL(sAnimName, "swing_react_bad_01")
			RETURN "swing_react_bad_01_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "swing_react_bad_02")
			RETURN "swing_react_bad_02_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "swing_react_good_01")
			RETURN "swing_react_good_01_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "swing_react_good_02")
			RETURN "swing_react_good_01_cam" // there is no swing react good two cam
		ELIF ARE_STRINGS_EQUAL(sAnimName, "wedge_react_bad_01")
			RETURN "wedge_react_bad_01_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "wedge_react_bad_02")
			RETURN "wedge_react_bad_02_cam"
		ELIF ARE_STRINGS_EQUAL(sAnimName, "wedge_swing_good_01")
			RETURN "wedge_react_good_01_cam"
		ENDIF
		
	ENDIF
	
	RETURN ""
ENDFUNC



