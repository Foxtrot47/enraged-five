CONST_INT	GOLF_IS_MP 0
BOOL bDebugSpew = FALSE

CONST_INT COMPILE_LOOKUPS 1
CONST_INT PUTTING_GREEN 1
CONST_INT GOLF_IS_AI_ONLY	1

USING "golf.sch"
USING "golf_core.sch"

SCRIPT


	GOLF_FOURSOME	puttingGreenFoursomes[2]
	GOLF_COURSE		puttingGreenCourse
	GOLF_GAME		currentGame
	GOLF_MINIGAME_STATE		golfState
	STREAMED_MODEL	golfAssets[11]
	
    PRINTSTRING("ENTER AI FOURSOME SCRIPT\n")
	golfState = GMS_INIT
	INT iCurrentPlayer
	INT	foursomeIndex
	INT updateIndex
	bFlagPassed = bFlagPassed
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		//SCRIPT_GOLF_CLEANUP(currentGame, currentCourse, currentPlayerFoursome, aiFoursomes, puttingGreenCourse, puttingGreenFoursomes, currentHelpers)
		SCRIPT_GOLF_CLEANUP_COURSE(puttingGreenCourse)
		SCRIPT_GOLF_CLEANUP_AI_FOURSOME(puttingGreenFoursomes)
		
		// DO NOT REMOVE THIS LINE. THE THREAD WILL STAY ACTIVE FOREVER IF YOU DO.
		TERMINATE_THIS_THREAD()
	ENDIF
	
	//SCRIPT_SETUP(currentGame)
	
	WHILE (TRUE)	
		//VECTOR golfCourseLoc <<-1328, 60, 53>>  // near putting greeen and golf launcher
		//VECTOR golfCourseLoc = <<-1175, 40, 57>> // near center of course
		
		//Terminate script if you are far enough away from the course
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			// Handled by ambient area controller.
			//IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), golfCourseLoc) > 120000
			//	PRINTLN("Distance Squared from Course ", VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), golfCourseLoc), "\n")
			//	golfState = GMS_CLEANUP
			//ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD 
			IF bDebugDrawAI OR bDebugDrawForces
				GOLF_DEBUG_DRAW_FOURSOMES(currentGame, puttingGreenCourse, puttingGreenFoursomes)
			ENDIF
			
						
			IF bAmbientGolfersOff OR g_bTriggerSceneActive
				golfState = GMS_CLEANUP
			ENDIF
		#ENDIF
		
		SWITCH golfState
			CASE GMS_INIT
				IF bDebugSpew DEBUG_MESSAGE("GMS_INIT") ENDIF
				GOLF_SETUP_PUTTING_GREEN(puttingGreenCourse)
				REQUEST_ANIM_DICT(GET_GOLF_BASIC_ANIMATION_DICTIONARY_NAME())
				REQUEST_ANIM_SET("move_m@golfer@")
				
				GOLF_STREAM_ASSETS(golfAssets)
				#IF IS_DEBUG_BUILD
					GOLF_SETUP_DEBUG_WIDGETS(currentGame, "Golf Ambient Putting")
				#ENDIF
				
				golfState = GMS_INIT_STREAMING_DONE
			BREAK

			CASE GMS_INIT_STREAMING_DONE

				IF HAS_ANIM_DICT_LOADED(GET_GOLF_BASIC_ANIMATION_DICTIONARY_NAME()) AND ARE_MODELS_STREAMED(golfAssets)
				AND HAS_ANIM_SET_LOADED("move_m@golfer@")
					GOLF_SETUP_CLUBS (currentGame.golfBag)
					golfState = GMS_INIT_POST_STREAMING  
				ENDIF
			BREAK
			CASE GMS_INIT_POST_STREAMING
				IF IS_GOLF_CLUB_OPEN_AT_TIME_OF_DAY(GET_CLOCK_HOURS()) //golf club is open
				
					IF NOT IS_SPHERE_VISIBLE(GET_GOLF_HOLE_TEE_POSITION(puttingGreenCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(puttingGreenFoursomes[foursomeIndex])), 10)
					OR IS_GLOBAL_GOLF_CONTROL_FLAG_SET(GGCF_START_GOLF_GAME)
						GOLF_SETUP_CLUBS (currentGame.golfBag)
						
						golfState = GMS_INIT_DONE
						REPEAT COUNT_OF(puttingGreenFoursomes) foursomeIndex
							// If there are more than two foursomes, 3 must be lower to prevent out of bounds exception
							GOLF_CREATE_AI_FOURSOME(puttingGreenCourse, puttingGreenFoursomes[foursomeIndex], 1, foursomeIndex * 4)
							REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(puttingGreenFoursomes[foursomeIndex]) iCurrentPlayer
								SET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG(puttingGreenFoursomes[foursomeIndex], iCurrentPlayer, GOLF_PUTTING_GREEN_AI)
								SET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB( puttingGreenFoursomes[foursomeIndex], GOLF_FIND_SPECIFIC_CLUB_IN_BAG(currentGame.golfBag, eCLUB_PUTTER))
							ENDREPEAT
						ENDREPEAT
					ENDIF
				ENDIF
			BREAK
			CASE GMS_INIT_DONE
//				DEBUG_MESSAGE("GMS_INIT_DONE")
				CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_AI_HIT_BALL_THIS_FRAME)

				golfState = GMS_PLAY_GOLF
			BREAK
			CASE GMS_PLAY_GOLF
				CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_AI_HIT_BALL_THIS_FRAME)

				GOLF_MANAGE_AI_FOURSOME_PUTTING(currentGame, puttingGreenCourse, puttingGreenFoursomes[updateIndex])
				
				updateIndex++
				IF updateIndex >= COUNT_OF(puttingGreenFoursomes)
					updateIndex = 0
				ENDIF

			BREAK
					
			CASE GMS_CLEANUP
				SCRIPT_GOLF_CLEANUP_COURSE(puttingGreenCourse)
				SCRIPT_GOLF_CLEANUP_AI_FOURSOME(puttingGreenFoursomes)
				REMOVE_ANIM_DICT(GET_GOLF_BASIC_ANIMATION_DICTIONARY_NAME())

				PRINTSTRING("Terminate Golf Putting AI Script\n")
				TERMINATE_THIS_THREAD()

			BREAK
			DEFAULT
				SCRIPT_ASSERT("golfState should never hit case default - please contact Alan Blaine with repro information")
			BREAK
		ENDSWITCH
		
		WAIT(0)
	ENDWHILE
//*/
ENDSCRIPT