USING "minigames_helpers.sch"
USING "golf_course.sch"
/// PURPOSE: Enumeration for a single player taking a shot
ENUM GOLF_PLAYER_STATE
	GPS_UNDEFINED = -1,
	GPS_PLACE_BALL,
	GPS_APPROACH_BALL,
	GPS_APPROACH_ANIM,
	GPS_ADDRESS_BALL,
	GPS_SWING_METER,
	GPS_BALL_IN_FLIGHT,
	GPS_BALL_AT_REST,
	GPS_POST_SHOT_REACTION,
	GPS_BALL_IN_HOLE_CELEBRATION,
	GPS_DONE_WITH_SHOT,
	GPS_DONE_WITH_HOLE
ENDENUM


/// PURPOSE: Enumeration of the various swing styles
ENUM SWING_STYLE
	SWING_STYLE_NORMAL,	// Normal swing, no power adjustments
	SWING_STYLE_POWER,  // Power swing, slight power increase for loss of accuracy
	SWING_STYLE_PUNCH, // Low power, low trajectory shot with strong backspin
	SWING_STYLE_APPROACH, // Low power swing with increased spin and accuracy
	SWING_STYLE_GREEN_SHORT,  // Putting 
	SWING_STYLE_GREEN,  // Putting
	SWING_STYLE_GREEN_LONG,
	SWING_STYLE_GREEN_TAP
ENDENUM 

/// PURPOSE: Enumeration for the animation state machine for a player swing
ENUM SWING_ANIM_STATES
	SAS_IDLE,
	SAS_SWINGING_BACK,
	SAS_HOLDING_POSITION,
	SAS_RETURNING_TO_IDLE,
	SAS_SWINGING_FORWARD,
	SAS_OUTRO
ENDENUM


/// PURPOSE: Bitfield enumeration for each player and thier shots
ENUM GOLF_PLAYER_STATUS_BITS
	GOLF_STATUS_NONE				= 0,
	GOLF_BALL_FIRST_COLLISION		= BIT1,
	GOLF_BALL_AT_REST				= BIT2,
	GOLF_BALL_DRIVE_CARRY			= BIT3,
	GOLF_PLAYER_NAV_HEADING			= BIT4,
	GOLF_PLAYER_TASKED				= BIT5,
	GOLF_BALL_NEEDS_SNAP			= BIT6,
	GOLF_BALL_IN_FOCUS          	= BIT7,
	GOLF_SHOT_IS_GIMMIE         	= BIT8,
	GOLF_WAS_IN_CART				= BIT9,
	GOLF_SHOT_STARTED				= BIT10,
	GOLF_SHOT_OVER					= BIT11,
	GOLF_HOLE_STARTED				= BIT12,
	GOLF_HOLE_OVER					= BIT13,
	GOLF_STROKE_LIMIT           	= BIT14,
	GOLF_FORCE_UPDATE           	= BIT15,
	GOLF_USE_HARD_AI				= BIT16,
	GOLF_PLAYER_TIME_OUT            = BIT17,
	GOLF_BALL_FAR_AWAY				= BIT18,
	GOLF_AI_FAR_AWAY				= BIT19,
	GOLF_FAR_AWAY					= BIT20,
	GOLF_SUCCESSFUL_DRIVE       	= BIT21,
	GOLF_PUTTING_GREEN_AI			= BIT22,
	GOLF_WAITING_FOR_HOLE 			= BIT23,
	GOLF_PLAYER_REMOVED         	= BIT24,
	GOLF_CART_ARRIVED				= BIT25,
	GOLF_STOP_USING_CART        	= BIT26,
	GOLF_PLAYER_REPOSITIONED    	= BIT27,
	GOLF_HIT_FLAG					= BIT28,
	GOLF_BALL_IS_OOB               	= BIT29,
	GOLF_STARTED_SHOT_ON_GREEN      = BIT30,
	GOLF_MISS_PUTT_DIALOG		    = BIT31
ENDENUM

//the more difficult ai will use more sophisticated algorithms to determine shot strength and direction
ENUM GOLF_AI_DIFFICULTY
	GOLF_AI_EASY = 0,
	GOLF_AI_NORMAL,
	GOLF_AI_HARD
ENDENUM

STRUCT GOLF_PLAYER_SCORECARD
	INT ID = -1
	TEXT_LABEL_63 sPlayerName
	TEXT_LABEL_15 crewTag
	TEXT_LABEL_23 mugShotDic, mugShotName
	HUD_COLOURS eBallColour
	INT iCardScore
	INT iCurrentSlot
	INT iScoreboardSlot
ENDSTRUCT

/// PURPOSE: Core struct representing a single golf player, no matter how controlled (human or AI)
STRUCT GOLF_PLAYER_CORE
	GOLF_PLAYER_STATE	playerState
	PLAYER_CONTROL_TYPE playerControl
	PED_INDEX			pedGolfPlayer
	OBJECT_INDEX		objBall
	OBJECT_INDEX		objClub
	OBJECT_INDEX		objGolfbag
	OBJECT_INDEX        objMarker
	VECTOR				vBallPosition
	VECTOR 				vShotEstimate
	FLOAT 				fDistToHole
	FLOAT				fAimHeading
	FLOAT				fPowerMeterGoal
	FLOAT				fShotCarry
	INT 				iCurrentClub
	INT					iCurrentHoleShots
	SWING_STYLE			swingStyle
	SWING_ANIM_STATES 	swingAnim
	SEQUENCE_INDEX 		seq_swing_and_outro
//	INT					iTaskTime
	GOLF_PLAYER_STATUS_BITS	eStatusBits
	GOLF_LIE_TYPE		lieType
	GOLF_LIE_TYPE		lieLastType
	VECTOR				vShotNormal
	VECTOR				vPreviousNormal
	INT					iCartIndex
	INT					iNextGolferWeight
	GOLF_AI_DIFFICULTY  aiDifficulty
	
	#IF NOT GOLF_IS_AI_ONLY
		GOLF_PLAYER_SCORECARD playerCard
		VECTOR vAddressBallPos
	#ENDIF
ENDSTRUCT


STRUCT GOLF_PLAYER_COURSE_SCORE
	INT iHoleScore
	BOOL bFairwayInRegulation
	BOOL bGreenInRegulation
	INT iHolePutts
ENDSTRUCT


/// PURPOSE: Struct for all golf players in the foursome for a local human player
STRUCT GOLF_PLAYER
	PAD_NUMBER			padPlayer
	GOLF_PLAYER_COURSE_SCORE iCourseScore[9]
	
	INT					iCurrentScore
	INT					iParticipantIndex = -1
ENDSTRUCT


/// PURPOSE: Helper function to get the servers game/mission state
/// RETURNS: The server game state.
FUNC GOLF_PLAYER_STATUS_BITS GET_GOLF_PLAYER_CONTROL_FLAGS(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.eStatusBits
ENDFUNC

PROC OVERWRITE_GOLF_PLAYER_CONTROL_FLAGS(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_PLAYER_STATUS_BITS eStatusBits)
	thisPlayerCore.eStatusBits = eStatusBits
ENDPROC

PROC SET_GOLF_PLAYER_CONTROL_FLAG(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_PLAYER_STATUS_BITS eStatusBits)
	thisPlayerCore.eStatusBits = thisPlayerCore.eStatusBits | eStatusBits
ENDPROC

PROC CLEAR_GOLF_PLAYER_CONTROL_FLAG(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_PLAYER_STATUS_BITS eStatusBits)
	thisPlayerCore.eStatusBits -= thisPlayerCore.eStatusBits & eStatusBits
ENDPROC

FUNC BOOL IS_GOLF_PLAYER_CONTROL_FLAG_SET(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_PLAYER_STATUS_BITS eStatusBits)
	RETURN (thisPlayerCore.eStatusBits & eStatusBits) != GOLF_STATUS_NONE
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE state
FUNC GOLF_PLAYER_STATE GET_GOLF_PLAYER_STATE(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.playerState
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE state
PROC SET_GOLF_PLAYER_STATE(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_PLAYER_STATE playerState, BOOL displayDebug = FALSE)

	IF displayDebug
		CDEBUG1LN(DEBUG_GOLF,"Setting to state ", playerState)
		#IF IS_DEBUG_BUILD
//			DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	thisPlayerCore.playerState = playerState
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE control method
FUNC PLAYER_CONTROL_TYPE GET_GOLF_PLAYER_CONTROL(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.playerControl
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE control method
PROC SET_GOLF_PLAYER_CONTROL(GOLF_PLAYER_CORE &thisPlayerCore, PLAYER_CONTROL_TYPE playerControl)
	thisPlayerCore.playerControl = playerControl
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE ped
FUNC PED_INDEX GET_GOLF_PLAYER_PED(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.pedGolfPlayer
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE ped
PROC SET_GOLF_PLAYER_PED(GOLF_PLAYER_CORE &thisPlayerCore, PED_INDEX pedGolfPlayer)
	thisPlayerCore.pedGolfPlayer = pedGolfPlayer
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE ball
FUNC OBJECT_INDEX GET_GOLF_PLAYER_BALL(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.objBall
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE ball
PROC SET_GOLF_PLAYER_BALL(GOLF_PLAYER_CORE &thisPlayerCore, OBJECT_INDEX objBall)
	thisPlayerCore.objBall = objBall
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE ball
PROC SET_GOLF_PLAYER_MARKER(GOLF_PLAYER_CORE &thisPlayerCore, OBJECT_INDEX objMarker)
	thisPlayerCore.objMarker = objMarker
ENDPROC

FUNC OBJECT_INDEX GET_GOLF_PLAYER_MARKER(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.objMarker
ENDFUNC

PROC CLEANUP_GOLF_MARKER(GOLF_PLAYER_CORE &thisPlayerCore)

	IF DOES_ENTITY_EXIST(thisPlayerCore.objMarker)
		DELETE_OBJECT(thisPlayerCore.objMarker)
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleanup for GOLF_PLAYER_CORE ball
PROC CLEANUP_GOLF_BALL(GOLF_PLAYER_CORE &thisPlayerCore)

	IF IS_GOLF_PLAYER_CONTROL_FLAG_SET(thisPlayerCore, GOLF_BALL_IN_FOCUS)
		CDEBUG1LN(DEBUG_GOLF, "Removing ball focus")
		CLEAR_FOCUS()
		CLEAR_GOLF_PLAYER_CONTROL_FLAG(thisPlayerCore, GOLF_BALL_IN_FOCUS)
	ENDIF
	
	IF DOES_ENTITY_EXIST(thisPlayerCore.objBall)
		IF IS_ENTITY_FOCUS(thisPlayerCore.objBall)
			CDEBUG1LN(DEBUG_GOLF, "Removing ball focus")
			CLEAR_FOCUS()
		ENDIF
		
		IF IS_ENTITY_A_MISSION_ENTITY(thisPlayerCore.objBall)
			DELETE_OBJECT(thisPlayerCore.objBall)
		ENDIF
	ENDIF
	thisPlayerCore.objBall = NULL
ENDPROC

PROC SET_GOLF_BALL_AS_NO_LONGER_NEEDED(GOLF_PLAYER_CORE &thisPlayerCore)
	IF DOES_ENTITY_EXIST(thisPlayerCore.objBall)
		SET_OBJECT_AS_NO_LONGER_NEEDED(thisPlayerCore.objBall)
	ENDIF
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE club
FUNC OBJECT_INDEX GET_GOLF_PLAYER_CLUB(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.objClub
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE club
PROC SET_GOLF_PLAYER_CLUB(GOLF_PLAYER_CORE &thisPlayerCore, OBJECT_INDEX objClub)
	IF thisPlayerCore.objClub = objClub
		CDEBUG1LN(DEBUG_GOLF,"Setting a golf club to itself")
		EXIT
	ENDIF

	IF DOES_ENTITY_EXIST(thisPlayerCore.objClub)
		#IF GOLF_IS_MP #IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_GOLF,"Creating new club when old one already exist")
			DEBUG_PRINTCALLSTACK()
		#ENDIF #ENDIF
		DELETE_OBJECT(thisPlayerCore.objClub)
	ENDIF
	thisPlayerCore.objClub = objClub
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE club
PROC CLEANUP_GOLF_CLUB(GOLF_PLAYER_CORE &thisPlayerCore)
	IF DOES_ENTITY_EXIST(thisPlayerCore.objClub)
		DETACH_ENTITY(thisPlayerCore.objClub)
		DELETE_OBJECT(thisPlayerCore.objClub)
		
		#IF GOLF_IS_MP #IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_GOLF,"Removing club")
			DEBUG_PRINTCALLSTACK()
		#ENDIF #ENDIF
	ENDIF
ENDPROC

PROC SET_GOLF_CLUB_NO_LONGER_NEEDED(GOLF_PLAYER_CORE &thisPlayerCore)
	IF DOES_ENTITY_EXIST(thisPlayerCore.objClub)
		DETACH_ENTITY(thisPlayerCore.objClub)
		SET_OBJECT_AS_NO_LONGER_NEEDED(thisPlayerCore.objClub)
	ENDIF
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE club
FUNC OBJECT_INDEX GET_GOLF_PLAYER_GOLFBAG(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.objGolfbag
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE club
PROC SET_GOLF_PLAYER_GOLFBAG(GOLF_PLAYER_CORE &thisPlayerCore, OBJECT_INDEX objGolfbag)
	thisPlayerCore.objGolfbag = objGolfbag
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE club
PROC CLEANUP_GOLF_GOLFBAG(GOLF_PLAYER_CORE &thisPlayerCore)
	IF DOES_ENTITY_EXIST(thisPlayerCore.objGolfbag)
		//DETACH_ENTITY(thisPlayerCore.objGolfbag)
		SET_OBJECT_AS_NO_LONGER_NEEDED(thisPlayerCore.objGolfbag)
	ENDIF
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE ball position
FUNC VECTOR GET_GOLF_PLAYER_BALL_POSITION(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.vBallPosition
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE ball position
PROC SET_GOLF_PLAYER_BALL_POSITION(GOLF_PLAYER_CORE &thisPlayerCore, VECTOR vBallPosition)
	thisPlayerCore.vBallPosition = vBallPosition
ENDPROC
	
/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE shot estimate
FUNC VECTOR GET_GOLF_PLAYER_SHOT_ESTIMATE(GOLF_PLAYER_CORE &thisPlayerCore)
//	PRINTSTRING("GET_GOLF_PLAYER_SHOT_ESTIMATE: ")PRINTVECTOR(thisPlayerCore.vShotEstimate)PRINTNL()
	RETURN thisPlayerCore.vShotEstimate
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE shot estimate
PROC SET_GOLF_PLAYER_SHOT_ESTIMATE(GOLF_PLAYER_CORE &thisPlayerCore, VECTOR vShotEstimate)
	thisPlayerCore.vShotEstimate = vShotEstimate
//	PRINTSTRING("SET_GOLF_PLAYER_SHOT_ESTIMATE: ")PRINTVECTOR(thisPlayerCore.vShotEstimate)PRINTNL()
ENDPROC
	

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE distance to hole
FUNC FLOAT GET_GOLF_PLAYER_DISTANCE_TO_HOLE(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.fDistToHole
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE distance to hole
PROC SET_GOLF_PLAYER_DISTANCE_TO_HOLE(GOLF_PLAYER_CORE &thisPlayerCore, FLOAT fDistToHole)
	thisPlayerCore.fDistToHole = fDistToHole
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE sim
FUNC FLOAT GET_GOLF_PLAYER_AIM(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.fAimHeading
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE sim
PROC SET_GOLF_PLAYER_AIM(GOLF_PLAYER_CORE &thisPlayerCore, FLOAT fAimHeading)
	thisPlayerCore.fAimHeading = fAimHeading
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE sim
PROC INCREMENT_GOLF_PLAYER_AIM(GOLF_PLAYER_CORE &thisPlayerCore, FLOAT fAimAdd)
	thisPlayerCore.fAimHeading += fAimAdd
//	PRINTSTRING("INCREMENT_GOLF_PLAYER_AIM: ")PRINTFLOAT(thisPlayerCore.fAimHeading)PRINTNL()
ENDPROC

PROC INC_GOLF_PLAYER_STRENGTH(INT iAmount = 3)
	
	#IF NOT GOLF_IS_MP
		INCREMENT_PLAYER_PED_STAT(GET_CURRENT_PLAYER_PED_ENUM(), PS_STRENGTH, iAmount)
	#ENDIF
	
	#IF GOLF_IS_MP
		INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STRENGTH, iAmount)
	#ENDIF
	
ENDPROC

//Gets players strength as a float from 0.0 to 1.0. With 0.0 being the max
FUNC FLOAT GET_GOLF_PLAYER_STRENGTH()

	INT iStatValue
	FLOAT fRetValue
	
	#IF NOT GOLF_IS_MP
		STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_STRENGTH), iStatValue)
	#ENDIF
	
	#IF GOLF_IS_MP
		iStatValue = GET_MP_INT_CHARACTER_STAT(MP_STAT_STRENGTH)
	#ENDIF
	
	fRetValue = iStatValue/100.0
	fRetValue = 1.0 - fRetValue
	
	//CDEBUG1LN(DEBUG_GOLF,"Players strength ", iStatValue ," Modified for golf ", fRetValue)
	
	RETURN fRetValue
ENDFUNC

//returns move aim sensitvity
FUNC FLOAT GET_GOLF_PLAYER_ACCURACY()

	#IF GOLF_IS_MP
		RETURN 0.0
	#ENDIF
	INT iPlayerIndex
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		iPlayerIndex = 0
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		iPlayerIndex = 1
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		iPlayerIndex = 2
	ENDIF

	IF g_savedGlobals.sGolfData.iNumRoundsWithCharacter[iPlayerIndex] >= 10
		RETURN 0.0
	ENDIF

//	CDEBUG1LN(DEBUG_GOLF,"Return acc ", 1.0 - TO_FLOAT(g_savedGlobals.sGolfData.iNumRoundsWithCharacter[iPlayerIndex])/10.0)
	RETURN 1.0 -TO_FLOAT(g_savedGlobals.sGolfData.iNumRoundsWithCharacter[iPlayerIndex])/10.0
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE power meter goal
FUNC FLOAT GET_GOLF_PLAYER_METER_GOAL(GOLF_PLAYER_CORE &thisPlayerCore, BOOL bUseStrengthStat)

	FLOAT fMeterGoal = thisPlayerCore.fPowerMeterGoal

	IF bUseStrengthStat AND 
	(GET_GOLF_PLAYER_CONTROL(thisPlayerCore) = HUMAN_LOCAL OR GET_GOLF_PLAYER_CONTROL(thisPlayerCore) = HUMAN_LOCAL_MP)
		fMeterGoal -= GET_GOLF_PLAYER_STRENGTH()*10 //Based on players strength stat, he can lose up to 10% power
		fMeterGoal = PICK_FLOAT(fMeterGoal < 5.0, 5.0, fMeterGoal)
	ENDIF

	RETURN fMeterGoal
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE power meter goal
PROC SET_GOLF_PLAYER_METER_GOAL(GOLF_PLAYER_CORE &thisPlayerCore, FLOAT fPowerMeterGoal)
	thisPlayerCore.fPowerMeterGoal = fPowerMeterGoal
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE drive carry
FUNC FLOAT GET_GOLF_PLAYER_SHOT_CARRY(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.fShotCarry
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE drive carry
PROC SET_GOLF_PLAYER_SHOT_CARRY(GOLF_PLAYER_CORE &thisPlayerCore, FLOAT fShotCarry)
	thisPlayerCore.fShotCarry = fShotCarry
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE current club index
FUNC INT GET_GOLF_PLAYER_CURRENT_CLUB(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.iCurrentClub
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE current club index
PROC SET_GOLF_PLAYER_CURRENT_CLUB(GOLF_PLAYER_CORE &thisPlayerCore, INT iCurrentClub)
	thisPlayerCore.iCurrentClub = iCurrentClub
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE current club index
PROC INCREMENT_GOLF_PLAYER_CURRENT_CLUB(GOLF_PLAYER_CORE &thisPlayerCore, INT iAddClub)
	thisPlayerCore.iCurrentClub += iAddClub
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE shots on current hole
FUNC INT GET_GOLF_PLAYER_SHOTS_ON_HOLE(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.iCurrentHoleShots
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE shots on current hole
PROC SET_GOLF_PLAYER_SHOTS_ON_HOLE(GOLF_PLAYER_CORE &thisPlayerCore, INT iCurrentHoleShots)
	thisPlayerCore.iCurrentHoleShots = iCurrentHoleShots
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE swing style
FUNC SWING_STYLE GET_GOLF_PLAYER_SWING_STYLE(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.swingStyle
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE swing style
PROC SET_GOLF_PLAYER_SWING_STYLE(GOLF_PLAYER_CORE &thisPlayerCore, SWING_STYLE swingStyle)
	thisPlayerCore.swingStyle = swingStyle
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE swing style
PROC INCREMENT_GOLF_PLAYER_SWING_STYLE(GOLF_PLAYER_CORE &thisPlayerCore, INT iAddValue)
	thisPlayerCore.swingStyle += INT_TO_ENUM(SWING_STYLE, iAddValue)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE swing anim
FUNC SWING_ANIM_STATES GET_GOLF_PLAYER_SWING_ANIM(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.swingAnim
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE swing anim
PROC SET_GOLF_PLAYER_SWING_ANIM(GOLF_PLAYER_CORE &thisPlayerCore, SWING_ANIM_STATES swingAnim)
	thisPlayerCore.swingAnim = swingAnim
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE last shot time
//FUNC INT GET_GOLF_PLAYER_TASK_TIME(GOLF_PLAYER_CORE &thisPlayerCore)
//	RETURN thisPlayerCore.iTaskTime
//ENDFUNC	
//
///// PURPOSE:
/////    Accessor for GOLF_PLAYER_CORE last shot time
//PROC SET_GOLF_PLAYER_TASK_TIME(GOLF_PLAYER_CORE &thisPlayerCore, INT iTaskTime)
//	thisPlayerCore.iTaskTime = iTaskTime
//ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE last material hit by ball
FUNC GOLF_LIE_TYPE GET_GOLF_PLAYER_LIE(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.lieType
ENDFUNC

FUNC GOLF_LIE_TYPE GET_GOLF_PLAYER_LAST_LIE(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.lieLastType
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE last material hit by ball
PROC SET_GOLF_PLAYER_LIE(GOLF_PLAYER_CORE &thisPlayerCore, VECTOR vWorldPos, GOLF_LIE_TYPE lieType, BOOL bPrintDebug = TRUE)

	IF bPrintDebug
		#IF NOT GOLF_IS_AI_ONLY
			CDEBUG1LN(DEBUG_GOLF,"Lie set to ")
			IF lieType= LIE_GREEN CDEBUG1LN(DEBUG_GOLF,"Lie Green")
			ELIF lieType= LIE_UNKNOWN CDEBUG1LN(DEBUG_GOLF,"Lie Unknown")
			ELIF lieType= LIE_WATER CDEBUG1LN(DEBUG_GOLF,"Lie Water")
			ELIF lieType= LIE_CUP CDEBUG1LN(DEBUG_GOLF,"Lie Cup")
			ELIF lieType= LIE_BUNKER CDEBUG1LN(DEBUG_GOLF,"Lie Bunker")
			ELIF lieType= LIE_CART_PATH CDEBUG1LN(DEBUG_GOLF,"Lie Cart Path")
			ELIF lieType= LIE_ROUGH CDEBUG1LN(DEBUG_GOLF,"Lie Rough")
			ELIF lieType= LIE_FAIRWAY CDEBUG1LN(DEBUG_GOLF,"Lie Fairway")
			ELIF lieType= LIE_TEE CDEBUG1LN(DEBUG_GOLF,"Lie Tee")
			ELIF lieType= LIE_BUSH CDEBUG1LN(DEBUG_GOLF,"Lie Bush")
			ELSE CDEBUG1LN(DEBUG_GOLF," ", lieType)
			ENDIF
//			DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
	//Tee boxes need to use same materail as green, Check that the green lie is really a green
	IF NOT IS_VECTOR_ZERO(vWorldPos)
		IF lieType = LIE_GREEN
			IF IS_POINT_NEAR_TEE_BOX(vWorldPos)
				#IF NOT GOLF_IS_AI_ONLY
					CDEBUG1LN(DEBUG_GOLF, "LIE is GREEN but it's on a TEE BOX, chagning lie to FAIRWAY")
				#ENDIF
				lieType = LIE_FAIRWAY
			ENDIF
		ENDIF
		IF IS_POINT_BAD_LIE(vWorldPos)
			#IF NOT GOLF_IS_AI_ONLY
				CDEBUG1LN(DEBUG_GOLF, "LIE is on a bad spot that was not marked with the correct material, chagning lie to BUSH")
			#ENDIF
			lieType = LIE_BUSH
		ENDIF
	ENDIF
	
	thisPlayerCore.lieType = lieType
ENDPROC

PROC SET_GOLF_PLAYER_LAST_LIE(GOLF_PLAYER_CORE &thisPlayerCore)
	#IF NOT GOLF_IS_AI_ONLY
		CDEBUG1LN(DEBUG_GOLF,"Setting previous lie to ")
		IF thisPlayerCore.lieType= LIE_GREEN CDEBUG1LN(DEBUG_GOLF,"Lie Green")
		ELIF thisPlayerCore.lieType= LIE_UNKNOWN CDEBUG1LN(DEBUG_GOLF,"Lie Unknown")
		ELIF thisPlayerCore.lieType= LIE_WATER CDEBUG1LN(DEBUG_GOLF,"Lie Water")
		ELIF thisPlayerCore.lieType= LIE_CUP CDEBUG1LN(DEBUG_GOLF,"Lie Cup")
		ELIF thisPlayerCore.lieType= LIE_BUNKER CDEBUG1LN(DEBUG_GOLF,"Lie Bunker")
		ELIF thisPlayerCore.lieType= LIE_CART_PATH CDEBUG1LN(DEBUG_GOLF,"Lie Cart Path")
		ELIF thisPlayerCore.lieType= LIE_ROUGH CDEBUG1LN(DEBUG_GOLF,"Lie Rough")
		ELIF thisPlayerCore.lieType= LIE_FAIRWAY CDEBUG1LN(DEBUG_GOLF,"Lie Fairway")
		ELIF thisPlayerCore.lieType= LIE_TEE CDEBUG1LN(DEBUG_GOLF,"Lie Tee")
		ELIF thisPlayerCore.lieType= LIE_BUSH CDEBUG1LN(DEBUG_GOLF,"Lie Bush")
		ELSE CDEBUG1LN(DEBUG_GOLF," ", thisPlayerCore.lieType)
		ENDIF
	#ENDIF
  thisPlayerCore.lieLastType = thisPlayerCore.lieType
ENDPROC

PROC RESET_GOLF_PLAYER_LAST_LIE(GOLF_PLAYER_CORE &thisPlayerCore)
	#IF NOT GOLF_IS_AI_ONLY
		CDEBUG1LN(DEBUG_GOLF,"Resetting lie to previous lie")
		CDEBUG1LN(DEBUG_GOLF,"Resseting lie to ")
		IF thisPlayerCore.lieLastType= LIE_GREEN CDEBUG1LN(DEBUG_GOLF,"Lie Green")
		ELIF thisPlayerCore.lieLastType= LIE_UNKNOWN CDEBUG1LN(DEBUG_GOLF,"Lie Unknown")
		ELIF thisPlayerCore.lieLastType= LIE_WATER CDEBUG1LN(DEBUG_GOLF,"Lie Water")
		ELIF thisPlayerCore.lieLastType= LIE_CUP CDEBUG1LN(DEBUG_GOLF,"Lie Cup")
		ELIF thisPlayerCore.lieLastType= LIE_BUNKER CDEBUG1LN(DEBUG_GOLF,"Lie Bunker")
		ELIF thisPlayerCore.lieLastType= LIE_CART_PATH CDEBUG1LN(DEBUG_GOLF,"Lie Cart Path")
		ELIF thisPlayerCore.lieLastType= LIE_ROUGH CDEBUG1LN(DEBUG_GOLF,"Lie Rough")
		ELIF thisPlayerCore.lieLastType= LIE_FAIRWAY CDEBUG1LN(DEBUG_GOLF,"Lie Fairway")
		ELIF thisPlayerCore.lieLastType= LIE_TEE CDEBUG1LN(DEBUG_GOLF,"Lie Tee")
		ELIF thisPlayerCore.lieLastType= LIE_BUSH CDEBUG1LN(DEBUG_GOLF,"Lie Bush")
		ELSE CDEBUG1LN(DEBUG_GOLF,thisPlayerCore.lieLastType)
		ENDIF
	#ENDIF
	thisPlayerCore.lieType = thisPlayerCore.lieLastType
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER pad index
FUNC PAD_NUMBER GET_GOLF_PLAYER_PAD_NUMBER(GOLF_PLAYER &thisPlayer)
	RETURN thisPlayer.padPlayer
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER pad index
FUNC PAD_NUMBER GET_GOLF_PLAYER_PAD_NUMBER_BY_INDEX(GOLF_PLAYER &thisPlayers[], INT playerIndex)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(thisPlayers)
		RETURN PAD1
	ENDIF
	RETURN thisPlayers[playerIndex].padPlayer
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER pad index
PROC SET_GOLF_PLAYER_PAD_NUMBER_BY_INDEX(GOLF_PLAYER &thisPlayers[], INT playerIndex, PAD_NUMBER padNumber)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(thisPlayers)
		EXIT
	ENDIF
	thisPlayers[playerIndex].padPlayer = padNumber
ENDPROC



/// PURPOSE:
///    Accessor for GOLF_PLAYER hole score
FUNC INT GET_GOLF_PLAYER_HOLE_SCORE(GOLF_PLAYER &thisPlayer, INT iHole)
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayer.iCourseScore)
		CDEBUG1LN(DEBUG_GOLF,"Invalid hole passed to GET_GOLF_PLAYER_HOLE_SCORE")
		RETURN 0
	ENDIF
	
	RETURN thisPlayer.iCourseScore[iHole].iHoleScore
ENDFUNC
PROC INCREMENT_GOLF_PLAYER_HOLE_SCORE(GOLF_PLAYER &thisPlayer, INT iHole, INT iValue)
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayer.iCourseScore)
		EXIT
	ENDIF
	
	thisPlayer.iCourseScore[iHole].iHoleScore += iValue
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER hole score
FUNC INT GET_GOLF_PLAYER_HOLE_SCORE_BY_INDEX(GOLF_PLAYER &thisPlayers[], INT playerIndex, INT iHole)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(thisPlayers)
		RETURN 0
	ENDIF
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayers[].iCourseScore)
		RETURN 0
	ENDIF
	RETURN thisPlayers[playerIndex].iCourseScore[iHole].iHoleScore
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER hole score
PROC INCREMENT_GOLF_PLAYER_HOLE_SCORE_BY_INDEX(GOLF_PLAYER &thisPlayers[], INT playerIndex, INT iHole, INT iValue)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(thisPlayers)
		EXIT
	ENDIF
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayers[].iCourseScore)
		EXIT
	ENDIF
	thisPlayers[playerIndex].iCourseScore[iHole].iHoleScore += iValue
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_PLAYER hole score
PROC SET_GOLF_PLAYER_HOLE_SCORE_BY_INDEX(GOLF_PLAYER &thisPlayers[], INT playerIndex, INT iHole, INT iHoleScore)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(thisPlayers)
		EXIT
	ENDIF
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayers[].iCourseScore)
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_GOLF,"Setting hole ", iHole," score to ", iHoleScore)
	thisPlayers[playerIndex].iCourseScore[iHole].iHoleScore = iHoleScore
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_PLAYER hole score
FUNC BOOL GET_GOLF_PLAYER_HOLE_FIR(GOLF_PLAYER &thisPlayer, INT iHole)
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayer.iCourseScore)
		RETURN FALSE
	ENDIF
	RETURN thisPlayer.iCourseScore[iHole].bFairwayInRegulation
ENDFUNC
PROC SET_GOLF_PLAYER_HOLE_FIR(GOLF_PLAYER &thisPlayer, INT iHole)
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayer.iCourseScore)
		EXIT
	ENDIF
	thisPlayer.iCourseScore[iHole].bFairwayInRegulation = TRUE
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER hole score
FUNC BOOL GET_GOLF_PLAYER_HOLE_GIR(GOLF_PLAYER &thisPlayer, INT iHole)
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayer.iCourseScore)
		RETURN FALSE
	ENDIF
	RETURN thisPlayer.iCourseScore[iHole].bGreenInRegulation
ENDFUNC
PROC SET_GOLF_PLAYER_HOLE_GIR(GOLF_PLAYER &thisPlayer, INT iHole)
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayer.iCourseScore)
		EXIT
	ENDIF
	thisPlayer.iCourseScore[iHole].bGreenInRegulation = TRUE
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER hole score
FUNC INT GET_GOLF_PLAYER_HOLE_PUTTS(GOLF_PLAYER &thisPlayer, INT iHole)
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayer.iCourseScore)
		RETURN 0
	ENDIF
	
	RETURN thisPlayer.iCourseScore[iHole].iHolePutts
ENDFUNC
PROC INCREMENT_GOLF_PLAYER_HOLE_PUTTS(GOLF_PLAYER &thisPlayer, INT iHole, INT iValue)
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayer.iCourseScore)
		EXIT
	ENDIF
	
	thisPlayer.iCourseScore[iHole].iHolePutts += iValue
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER hole score
FUNC INT GET_GOLF_PLAYER_HOLE_PUTTS_BY_INDEX(GOLF_PLAYER &thisPlayers[], INT playerIndex, INT iHole)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(thisPlayers)
		RETURN 0
	ENDIF
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayers[].iCourseScore)
		RETURN 0
	ENDIF
	RETURN thisPlayers[playerIndex].iCourseScore[iHole].iHolePutts
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER hole score
PROC INCREMENT_GOLF_PLAYER_HOLE_PUTTS_BY_INDEX(GOLF_PLAYER &thisPlayers[], INT playerIndex, INT iHole, INT iValue)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(thisPlayers)
		EXIT
	ENDIF
	IF iHole < 0 OR iHole >= COUNT_OF(thisPlayers[].iCourseScore)
		EXIT
	ENDIF
	thisPlayers[playerIndex].iCourseScore[iHole].iHolePutts += iValue
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_PLAYER current score
FUNC INT GET_GOLF_PLAYER_CURRENT_SCORE(GOLF_PLAYER &thisPlayer)
	RETURN thisPlayer.iCurrentScore
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER current score
FUNC INT GET_GOLF_PLAYER_CURRENT_SCORE_BY_INDEX(GOLF_PLAYER &thisPlayers[], INT playerIndex)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(thisPlayers)
		RETURN 0
	ENDIF
	RETURN thisPlayers[playerIndex].iCurrentScore
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER current score
PROC INCREMENT_GOLF_PLAYER_CURRENT_SCORE(GOLF_PLAYER &thisPlayer, INT iValue)
	thisPlayer.iCurrentScore += iValue
ENDPROC

PROC SET_GOLF_PLAYER_CURRENT_SCORE(GOLF_PLAYER &thisPlayer, INT iCurrentScore)
	thisPlayer.iCurrentScore = iCurrentScore
ENDPROC

PROC SET_GOLF_PLAYER_CURRENT_SCORE_BY_INDEX(GOLF_PLAYER &thisPlayers[], INT playerIndex, INT iCurrentScore)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(thisPlayers)
		EXIT
	ENDIF
	thisPlayers[playerIndex].iCurrentScore = iCurrentScore
ENDPROC
/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE last normal
FUNC VECTOR GET_GOLF_PLAYER_SHOT_NORMAL(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.vShotNormal
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE last normal
PROC SET_GOLF_PLAYER_SHOT_NORMAL(GOLF_PLAYER_CORE &thisPlayerCore, VECTOR vShotNormal)
	thisPlayerCore.vShotNormal = vShotNormal
ENDPROC

PROC SET_GOLF_PLAYER_PREVIOUS_NORMAL(GOLF_PLAYER_CORE &thisPlayerCore)
	thisPlayerCore.vPreviousNormal = thisPlayerCore.vShotNormal
ENDPROC

PROC RESET_GOLF_PLAYER_SHOT_NORMAL(GOLF_PLAYER_CORE &thisPlayerCore)
	thisPlayerCore.vShotNormal = thisPlayerCore.vPreviousNormal
ENDPROC

PROC SET_GOLF_PLAYER_AI_DIFFICULTY(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_AI_DIFFICULTY aiDifficulty)
	thisPlayerCore.aiDifficulty = aiDifficulty
ENDPROC

FUNC GOLF_AI_DIFFICULTY GET_GOLF_PLAYER_AI_DIFFICULTY(GOLF_PLAYER_CORE &thisPlayerCore)
	RETURN thisPlayerCore.aiDifficulty
ENDFUNC

FUNC PARTICIPANT_INDEX GET_GOLF_PLAYER_NET_INDEX_BY_INDEX(GOLF_PLAYER &thisPlayers[], INT playerIndex)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(thisPlayers)
		RETURN NULL
	ENDIF
	RETURN INT_TO_PARTICIPANTINDEX(thisPlayers[playerIndex].iParticipantIndex)
ENDFUNC

FUNC INT GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(GOLF_PLAYER &thisPlayers[], INT playerIndex)
	IF playerIndex < 0 OR playerIndex >= COUNT_OF(thisPlayers)
		RETURN -1
	ENDIF
	RETURN thisPlayers[playerIndex].iParticipantIndex
ENDFUNC

FUNC INT GET_PLAYER_INDEX_FROM_NET_INDEX(GOLF_PLAYER &thisPlayers[], INT iNetParticipantIndex)
	INT iPlayerIndex
	REPEAT COUNT_OF(thisPlayers) iPlayerIndex
		IF thisPlayers[iPlayerIndex].iParticipantIndex = iNetParticipantIndex
			RETURN iPlayerIndex
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

PROC INIT_PLAYER_PLAYOFF_SCORE(GOLF_PLAYER &thisPlayer, INT iHole)
	thisPlayer.iCourseScore[iHole].bFairwayInRegulation = FALSE
	thisPlayer.iCourseScore[iHole].bGreenInRegulation = FALSE
	thisPlayer.iCourseScore[iHole].iHolePutts = 0
	thisPlayer.iCourseScore[iHole].iHoleScore = 0
ENDPROC

FUNC TEXT_LABEL_23 GET_STYLE_STRING_FROM_ENUM(SWING_STYLE eStyle)
	
	TEXT_LABEL_23 txtSwing = "SWING_"
	SWITCH eStyle
		CASE SWING_STYLE_NORMAL
			txtSwing += "NORMAL"
		BREAK
		CASE SWING_STYLE_POWER
			txtSwing += "POWER"
		BREAK
		CASE SWING_STYLE_APPROACH
			txtSwing += "APPROACH"
		BREAK
		CASE SWING_STYLE_PUNCH
			txtSwing += "PUNCH"
		BREAK
		CASE SWING_STYLE_GREEN
		CASE SWING_STYLE_GREEN_TAP
			txtSwing += "GREEN"
		BREAK
		CASE SWING_STYLE_GREEN_SHORT
			txtSwing += "GREEN_S"
		BREAK
		CASE SWING_STYLE_GREEN_LONG
			txtSwing += "GREEN_L"
		BREAK
	ENDSWITCH

	RETURN txtSwing
ENDFUNC

FUNC INT GET_GOLF_PLAYER_BLIP_COLOUR(INT iPlayer)
	IF iPlayer = 0
		RETURN BLIP_COLOUR_GOLF_P1
	ELIF iPlayer = 1
		RETURN BLIP_COLOUR_GOLF_P2
	ELIF iPlayer = 2
		RETURN BLIP_COLOUR_GOLF_P3
	ELSE
		RETURN BLIP_COLOUR_GOLF_P4
	ENDIF
ENDFUNC


FUNC HUD_COLOURS GET_GOLF_PLAYER_HUD_COLOUR(INT iPlayer)
	IF iPlayer = 0
		RETURN HUD_COLOUR_GOLF_P1
	ELIF iPlayer = 1
		RETURN HUD_COLOUR_GOLF_P2
	ELIF iPlayer = 2
		RETURN HUD_COLOUR_GOLF_P3
	ELSE
		RETURN HUD_COLOUR_GOLF_P4
	ENDIF
ENDFUNC
