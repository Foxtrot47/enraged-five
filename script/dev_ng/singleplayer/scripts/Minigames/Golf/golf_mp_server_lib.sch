USING "golf_mp.sch"
USING "golf_mp_client.sch"
USING "golf_helpers.sch"

FUNC BOOL GOLF_SHOULD_MAKE_PLAYER_HOST()
	//Loop through all the players and find the one I want to be host. This stops the spectators from becoming host
	INT iDesiredHostID, iPlayerIndex
	REPEAT MP_GOLF_MAX_PLAYERS iPlayerIndex
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerIndex))
			IF NOT (DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerIndex)))
					OR IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerIndex))))
				iDesiredHostID = iPlayerIndex
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF PARTICIPANT_ID_TO_INT() = iDesiredHostID
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			//Already host, don't need to make them the host
			RETURN FALSE
		ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC GOLF_MP_GAME_STATE GET_CLIENT_STATE_SYNC( GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[])
	GOLF_MP_GAME_STATE syncState = GAME_STATE_UNDEFINED
	INT playerIndex
	REPEAT COUNT_OF(playerBD) playerIndex
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(playerIndex))
			IF syncState = GAME_STATE_UNDEFINED
				syncState = GET_GOLF_MP_CLIENT_MISSION_STATE(playerBD, playerIndex)
			ENDIF
			IF syncState != GET_GOLF_MP_CLIENT_MISSION_STATE(playerBD, playerIndex)
				RETURN GAME_STATE_UNDEFINED
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN syncState
ENDFUNC

FUNC GOLF_MINIGAME_STATE GET_CLIENT_GOLF_STATE_SYNC(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[])
	GOLF_MINIGAME_STATE syncState = GMS_UNDEFINED
	INT playerIndex
	REPEAT COUNT_OF(playerBD) playerIndex
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(playerIndex))
			
			IF GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) < GMS_PLAY_GOLF
			AND (DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(playerIndex)))
				OR IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(playerIndex))))
				//spectator initilaisation is handled else where, do not worry about what state they are in
			ELSE
				IF syncState = GMS_UNDEFINED
					syncState = GET_GOLF_MP_CLIENT_GOLF_STATE(playerBD, playerIndex)
				ENDIF
				IF syncState != GET_GOLF_MP_CLIENT_GOLF_STATE(playerBD, playerIndex)
					RETURN GMS_UNDEFINED
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN syncState
ENDFUNC


FUNC INT FIND_NEXT_NET_SHOOTER(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], GOLF_COURSE &thisCourse, BOOL bAllowDoneWithHole, BOOL bStartOfHole = FALSE)
	FLOAT fFarthestShot = 0.0
	INT iFarthestPlayer = -1
	INT iCurrentPlayer
	FLOAT distToHole, fDistToHoleNoWieght3D
	DEBUG_MESSAGE("FIND_NEXT_NET_SHOOTER starting!")
	
	//make sure not to switch playes after they hit the ball OOB
	//IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD), GCF_BALL_OOB)
	//AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD), GCF_PLAYER_DONE_WITH_HOLE)
		//RETURN GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD)
	// ENDIF
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iCurrentPlayer
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != CONTROL_NONE
			INT piCurrentPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers,iCurrentPlayer)
			
			IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(piCurrentPlayer)
			AND NOT IS_GOLF_NETWORK_PARTICIPANT_SPECTATOR(piCurrentPlayer)
				IF (NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, piCurrentPlayer, GCF_PLAYER_DONE_WITH_HOLE)
				OR bAllowDoneWithHole OR bStartOfHole)
				AND NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iCurrentPlayer, GOLF_PLAYER_REMOVED)
				
					VECTOR vStartPos, vEndPos
					IF NOT bStartOfHole
						vStartPos = GET_GOLF_MP_CLIENT_BALL_POSITION(playerBD, piCurrentPlayer)
						vEndPos = GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
					ELSE
						vStartPos = GET_GOLF_HOLE_TEE_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
						vEndPos = GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
					ENDIF
					
					fDistToHoleNoWieght3D = VDIST(vStartPos, vEndPos)
					
					vStartPos.z = 0
					vEndPos.z = 0 //only 2d distance
					
					distToHole = VDIST(vStartPos, vEndPos)
					
					CDEBUG1LN(DEBUG_GOLF,"Distance to hole with out weights, ", distToHole)
					
					distToHole *= PICK_FLOAT(GET_GOLF_MP_CLIENT_LIE(playerBD, piCurrentPlayer) = LIE_GREEN, 1, 99999)
					distToHole *= PICK_FLOAT(GET_GOLF_MP_CLIENT_LIE(playerBD, piCurrentPlayer) = LIE_TEE, 100, 1)
					distToHole	*= ((thisFoursome.playerCore[iCurrentPlayer].iNextGolferWeight*0.1)+1)
					
					IF fDistToHoleNoWieght3D < DIST_FOR_GIMMIE AND ARE_ALL_PLAYERS_ON_THE_GREEN(thisFoursome, thisCourse)
						CDEBUG1LN(DEBUG_GOLF,"Using near hole wieght")
						distToHole *= 100
					ENDIF
					
	//				IF NOT bStartOfHole AND NOT ARE_ALL_PLAYERS_DONE_WITH_TEE_SHOT(thisFoursome)
	//					CDEBUG1LN(DEBUG_GOLF,"Shots on hole wieght used")
	//					distToHole *= PICK_FLOAT(GET_GOLF_MP_CLIENT_SHOTS_ON_CURRENT_HOLE(playerBD, piCurrentPlayer) != 0, 1, 100)
	//				ELSE
	//					CDEBUG1LN(DEBUG_GOLF,"Shots on hole wieght not used")
	//				ENDIF
					
					CDEBUG1LN(DEBUG_GOLF,"Golfer ",iCurrentPlayer," dist to hole ",distToHole," against farthest shot ",fFarthestShot, " wieght from position in previous hole ", thisFoursome.playerCore[iCurrentPlayer].iNextGolferWeight*0.1)
					CDEBUG1LN(DEBUG_GOLF,"On green weight ", PICK_FLOAT(GET_GOLF_MP_CLIENT_LIE(playerBD, piCurrentPlayer) = LIE_GREEN, 1, 100), " shots on hole weight ", PICK_FLOAT(GET_GOLF_MP_CLIENT_SHOTS_ON_CURRENT_HOLE(playerBD, piCurrentPlayer) != 0, 1, 100))
					
					IF distToHole  > fFarthestShot
						iFarthestPlayer = iCurrentPlayer
						fFarthestShot = distToHole
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iFarthestPlayer = -1
		iFarthestPlayer = 0
	ENDIF
	CDEBUG1LN(DEBUG_GOLF,"Next player is ", iFarthestPlayer,  " net index ", GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers,iFarthestPlayer))
	//DEBUG_PRINTCALLSTACK()
	RETURN GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers,iFarthestPlayer)
ENDFUNC

PROC GOLF_MP_SERVER_UPDATE_SHOT_STATUS(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], GOLF_COURSE &thisCourse)
	BOOL bOneIsDoneWithShot= FALSE
	BOOL bAllDoneWithHole = TRUE
	BOOL bNoneDoneWithHole= TRUE
	BOOL bAllMovedToNextHole = TRUE //check that everyone went to the end hole camera
	INT iPlayerIndex
	INT iPlayerDoneWithShot = -1
	REPEAT COUNT_OF(playerBD) iPlayerIndex
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX( iPlayerIndex))
		
			IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET( thisFoursome, GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, iPlayerIndex), GOLF_PLAYER_REMOVED)
				IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, iPlayerIndex, GCF_PLAYER_DONE_WITH_SHOT)
					bOneIsDoneWithShot = TRUE
					iPlayerDoneWithShot = iPlayerIndex
				ENDIF
				
				IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, iPlayerIndex, GCF_PLAYER_DONE_WITH_HOLE)
					bNoneDoneWithHole = FALSE
					bAllMovedToNextHole = FALSE
				ELSE
					bAllDoneWithHole = FALSE
				ENDIF
			ELSE
				//player is spectating
				IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, iPlayerIndex, GCF_PLAYER_DONE_WITH_HOLE)
					bAllMovedToNextHole = FALSE
				ENDIF
			ENDIF
			
		ELSE
			IF GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD) = iPlayerIndex //current player used to be this guy but he left, he is done with shot
				CDEBUG1LN(DEBUG_GOLF,"Player ", iPlayerIndex, " quit when he was the current player")
				IF NOT IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_MOVE_TO_NEXT_SHOT_MP)
				AND GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_HOLE
					CDEBUG1LN(DEBUG_GOLF,"Haven't told player to move to next shot yet")
					bOneIsDoneWithShot = TRUE
					iPlayerDoneWithShot = iPlayerIndex
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD)) < 0
	OR IS_PED_INJURED(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
		//Some one disconnected at a strange time, re-get current player
//		IF NOT IS_SCREEN_FADED_OUT()
//		AND NOT IS_SCREEN_FADING_OUT()
//			DO_SCREEN_FADE_OUT(250)
//		ENDIF
		SET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD, FIND_NEXT_NET_SHOOTER(playerBD, thisFoursome,thisPlayers, thisCourse, bNoneDoneWithHole))
		
	ELIF bAllDoneWithHole AND NOT IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_MOVE_TO_NEXT_HOLE_MP)
		CDEBUG1LN(DEBUG_GOLF,"SERVER SENDING MOVE TO NEXT HOLE - moving to hole ", GET_GOLF_MP_SERVER_CURRENT_HOLE(serverBD)+1)
		IF iPlayerDoneWithShot != -1
			//INCREMENT_GOLF_MP_SERVER_PLAYER_CURRENT_HOLE_SCORE(serverBD, iPlayerDoneWithShot)
		ENDIF
		REPEAT COUNT_OF(playerBD) iPlayerIndex
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX( iPlayerIndex))
				SET_GOLF_MP_SERVER_PLAYER_CURRENT_HOLE_SCORE(serverBD, iPlayerIndex,  GET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(thisFoursome, GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, iPlayerIndex)))	
				ADD_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(serverBD, iPlayerIndex,  GET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(thisFoursome, GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, iPlayerIndex)))	
			ENDIF
		ENDREPEAT
		
		SET_GOLF_MP_SERVER_CONTROL_FLAG(serverBD, GCF_MOVE_TO_NEXT_HOLE_MP)
		SET_FOURSOME_NEXT_GOLFER_WEIGHT_BASED_ON_WHO_WON_HOLE(thisFoursome, thisPlayers, GET_GOLF_MP_SERVER_CURRENT_HOLE(serverBD))
		SET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD, FIND_NEXT_NET_SHOOTER(playerBD, thisFoursome,thisPlayers, thisCourse, TRUE, TRUE))
		INCREMENT_GOLF_MP_SERVER_CURRENT_HOLE(serverBD)
		
		SET_BITMASK_ENUM_AS_ENUM(serverBD.golfServerControlFlags2, GCF2_NEW_GOLFER_IS_SET)
	ELIF NOT bAllDoneWithHole
		IF bAllMovedToNextHole AND IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_MOVE_TO_NEXT_HOLE_MP)
			CLEAR_GOLF_MP_SERVER_CONTROL_FLAG(serverBD, GCF_MOVE_TO_NEXT_HOLE_MP)
		ENDIF
		IF bOneIsDoneWithShot AND NOT IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_MOVE_TO_NEXT_SHOT_MP)
			CDEBUG1LN(DEBUG_GOLF,"SERVER SENDING MOVE TO NEXT SHOT")
			IF iPlayerDoneWithShot != -1
				//INCREMENT_GOLF_MP_SERVER_PLAYER_CURRENT_HOLE_SCORE(serverBD, iPlayerDoneWithShot)
			ENDIF
			
			IF ARE_ALL_PLAYERS_DONE_WITH_TEE_SHOT(thisFoursome)
				RESET_FOURSOME_NEXT_GOLFER_WEIGTHS(thisFoursome)
			ENDIF
			
			IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(serverBD.golfServerControlFlags2, GCF2_NEW_GOLFER_IS_SET)
				SET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD, FIND_NEXT_NET_SHOOTER(playerBD, thisFoursome,thisPlayers, thisCourse, bNoneDoneWithHole))
			ENDIF
			
			CLEAR_BITMASK_ENUM_AS_ENUM(serverBD.golfServerControlFlags2, GCF2_NEW_GOLFER_IS_SET)
			SET_BITMASK_ENUM_AS_ENUM(serverBD.golfServerControlFlags, GCF_MOVE_TO_NEXT_SHOT_MP)
			
		ELIF NOT bOneIsDoneWithShot AND IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_MOVE_TO_NEXT_SHOT_MP)
			CDEBUG1LN(DEBUG_GOLF,"Checking if all moved to next shoe")
			IF IS_ALL_GOLF_CLIENT_CONTROL_FLAGS_SET(thisFoursome, playerBD, thisPlayers, GCF_MOVE_TO_NEXT_SHOT_MP)
				CDEBUG1LN(DEBUG_GOLF,"Every one moved to next shot")
				CLEAR_GOLF_MP_SERVER_CONTROL_FLAG(serverBD, GCF_MOVE_TO_NEXT_SHOT_MP)
			ELSE
				CDEBUG1LN(DEBUG_GOLF,"Someone is still moving to next shot")
			ENDIF
		ELIF bOneIsDoneWithShot
			CDEBUG1LN(DEBUG_GOLF,"Some one finished their shot and move to next shot was already set")
		ENDIF
	ENDIF
ENDPROC


PROC DEBUG_GOLF_MP_SERVER_FLAGS(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_CONTROL_FLAGS &savedGCF[], VECTOR &savedBallPositions[])
	INT iPlayerIndex
	REPEAT COUNT_OF(playerBD) iPlayerIndex
		//	PARTICIPANT_INDEX piPlayer = INT_TO_PARTICIPANTINDEX(iPlayerIndex)
		IF GET_GOLF_MP_CLIENT_CONTROL_FLAGS(playerBD,iPlayerIndex )!= savedGCF[iPlayerIndex]
			CDEBUG1LN(DEBUG_GOLF,"Server got player ", iPlayerIndex, " control flag update")
			PRINTBITS(ENUM_TO_INT(GET_GOLF_MP_CLIENT_CONTROL_FLAGS(playerBD, iPlayerIndex))) PRINTLN()
			savedGCF[iPlayerIndex] = GET_GOLF_MP_CLIENT_CONTROL_FLAGS(playerBD, iPlayerIndex)
		ENDIF
	
		IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_GOLF_MP_CLIENT_BALL_POSITION( playerBD, iPlayerIndex),savedBallPositions[iPlayerIndex])
			//CDEBUG1LN(DEBUG_GOLF,"Server got player ", iPlayerIndex, " ball position update: ", playerBD[iPlayerIndex].vBallPosition)
			savedBallPositions[iPlayerIndex] = GET_GOLF_MP_CLIENT_BALL_POSITION( playerBD, iPlayerIndex)
		ENDIF
	ENDREPEAT
ENDPROC


