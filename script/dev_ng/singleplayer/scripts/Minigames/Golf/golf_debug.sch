USING "golf.sch"
#IF GOLF_IS_MP
USING "golf_mp_client.sch"
USING "golf_mp.sch"
#ENDIF
USING "golf_fx_lib.sch"
USING "golf_ui.sch"

WIDGET_GROUP_ID		golfTuning
WIDGET_GROUP_ID		clubTuning
WIDGET_GROUP_ID		pinTuning
WIDGET_GROUP_ID		teeTuning
WIDGET_GROUP_ID		shotTuning
WIDGET_GROUP_ID		scoreTuning

PROC GOLF_SETUP_DEBUG_WIDGETS(GOLF_GAME &thisGame #IF NOT COMPILE_LOOKUPS,  GOLF_COURSE &thisCourse#ENDIF , STRING name)
	golfTuning = START_WIDGET_GROUP(name)
	
	START_WIDGET_GROUP("Rob's Mucking About")
//		ADD_WIDGET_FLOAT_SLIDER("RADS_0", 	RADS_0, 0, 75, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("RADS_1", 	RADS_1, 0, 75, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("RADS_2", 	RADS_2, 0, 75, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("RADS_3", 	RADS_3, 0, 75, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("RADS_4", 	RADS_4, 0, 75, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("RADS_5", 	RADS_5, 0, 75, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("RADS_6", 	RADS_6, 0, 75, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("RADS_7", 	RADS_7, 0, 75, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("RADS_8", 	RADS_8, 0, 75, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("RADS_9", 	RADS_9, 0, 75, 0.01)
//		ADD_WIDGET_INT_SLIDER("GOLFER_ONE_THIS_SCORE", GOLFER_ONE_THIS_SCORE, 0, 10, 1)
//		ADD_WIDGET_INT_SLIDER("GOLFER_TWO_THIS_SCORE", GOLFER_TWO_THIS_SCORE, 0, 10, 1)
//		ADD_WIDGET_INT_SLIDER("GOLFER_THREE_THIS_SCORE", GOLFER_THREE_THIS_SCORE, 0, 10, 1)
//		ADD_WIDGET_INT_SLIDER("GOLFER_FOUR_THIS_SCORE", GOLFER_FOUR_THIS_SCORE, 0, 10, 1)
//		ADD_WIDGET_INT_SLIDER("GOLFER_ONE_LAST_SCORE", GOLFER_ONE_LAST_SCORE, 0, 10, 1)
//		ADD_WIDGET_INT_SLIDER("GOLFER_TWO_LAST_SCORE", GOLFER_TWO_LAST_SCORE, 0, 10, 1)
//		ADD_WIDGET_INT_SLIDER("GOLFER_THREE_LAST_SCORE", GOLFER_THREE_LAST_SCORE, 0, 10, 1)
//		ADD_WIDGET_INT_SLIDER("GOLFER_FOUR_LAST_SCORE", GOLFER_FOUR_LAST_SCORE, 0, 10, 1)
//		ADD_WIDGET_INT_SLIDER("GOLFER_ONE_THIS_SCORE", GOLFER_ONE_THIS_SCORE, 0, 10, 1)
	STOP_WIDGET_GROUP()
	
	ADD_WIDGET_BOOL("Debug Spew", bDebugSpew)
	ADD_WIDGET_BOOL("Do Occlusion Checks on Address Cam", bOcclusionCheck)


#IF GOLF_IS_AI_ONLY

	ADD_WIDGET_BOOL("Kill Thread", bAmbientGolfersOff)

#ENDIF	
	
	
	ADD_WIDGET_FLOAT_SLIDER("Swing Meter time (s)", SWING_METER_TIME_SWING, 0.0, 5.0, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Putt Meter time (s)", SWING_METER_TIME_PUTT, 0.0, 5.0, 0.1)
	
	ADD_WIDGET_FLOAT_SLIDER("Normal - Swing Force Multiplier", POWER_MAGIC_NUMBER_NORMAL, 0, 5.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Power - Swing Force Multiplier", POWER_MAGIC_NUMBER_POWER, 0, 5.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Approach - Swing Force Multiplier", POWER_MAGIC_NUMBER_APPROACH, 0, 5.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Punch - Swing Force Multiplier", POWER_MAGIC_NUMBER_PUNCH, 0, 5.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Putt - Swing Force Multiplier", POWER_MAGIC_NUMBER_GREEN, 0, 5.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Putt Short - Swing Force Multiplier", POWER_MAGIC_NUMBER_GREEN_SHORT, 0, 5.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Putt Long - Swing Force Multiplier", POWER_MAGIC_NUMBER_GREEN_LONG, 0, 5.0, 0.01)
	
	ADD_WIDGET_FLOAT_SLIDER("Swing Meter Accuracy - Max Heading Deviation", SWING_METER_HEADING_MAX_DEVIATION, -90, 90, 1.0)
	
	ADD_WIDGET_FLOAT_SLIDER("Ball stop velocity - Bouncing", BALL_VELOCITY_THRESHOLD_BOUNCE, 0, 20.0, 0.01)
	
	ADD_WIDGET_FLOAT_SLIDER("Ball stop velocity - Fairway", BALL_VELOCITY_THRESHOLD_NORMAL, 0, 2.5, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Ball stop velocity - Green", BALL_VELOCITY_THRESHOLD_GREEN, 0, 2.5, 0.005)
	ADD_WIDGET_FLOAT_SLIDER("Hole Radius", BALL_IN_HOLE_RADIUS, 0, 1.0, 0.05)
	
	ADD_WIDGET_FLOAT_SLIDER("Approach offset X", vApproachBallOffset.x, -10.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Approach offset Y", vApproachBallOffset.y, -10.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Approach offset Z", vApproachBallOffset.z, -10.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Approach offset heading", fApproachBallHeadingOffset, -360.0, 260.0, 0.01)
	
	ADD_WIDGET_FLOAT_SLIDER("Golf trail max dist", GOLF_TRAIL_MAX_DIST, 15.0, 200.0, 1.0)
	
	ADD_WIDGET_BOOL("Playoff Game", thisGame.bPlayoffGame)
	ADD_WIDGET_BOOL("Go To Rematch", bTestReplay)

	#IF NOT GOLF_IS_AI_ONLY
	#IF IS_DEBUG_BUILD
		ADD_WIDGET_BOOL("Bug 585354", bDebugHardCodedShotUse)
		ADD_WIDGET_BOOL("Make Putt", bMakePutt)
		ADD_WIDGET_BOOL("Hit Opponents Ball", bHitOpponentBall)
		
		ADD_WIDGET_BOOL("Stop MP Timer", bStopMPTimer)
		ADD_WIDGET_BOOL("Pause after shot", bGolfPause)
	#ENDIF
	#ENDIF
	
	ADD_WIDGET_BOOL("Debug Draw Forces", bDebugDrawForces)
	ADD_WIDGET_BOOL("Debug Draw AI", bDebugDrawAI)
	
	ADD_WIDGET_BOOL("Driving Range (Resets ball after it stops)", bDrivingRangeMode)
	ADD_WIDGET_BOOL("Infinite Shots", bInfiniteShots)
	ADD_WIDGET_BOOL("Max Spin", bMaxSpin)
	ADD_WIDGET_BOOL("Display Stats", bDisplayStats)
	ADD_WIDGET_BOOL("Perfect Hit Accuracy", bPerfectAccuracyMode)

	ADD_WIDGET_BOOL("Disable Wind Effects", bDisableWindEffects)
	ADD_WIDGET_BOOL("Disable Spin Effects", bDisableSpinEffects)
	ADD_WIDGET_BOOL("Disable Natural Club Spin", bDisableNaturalClubSpinEffects)
	
	#IF NOT GOLF_IS_AI_ONLY
	#IF IS_DEBUG_BUILD
		ADD_WIDGET_BOOL("Use debug height blend", bUseDebugBlendWeight)
		ADD_WIDGET_FLOAT_SLIDER("Debug height blend", fDebugBlendWeight, 0, 0.9999, 0.01)
	#ENDIF
	#ENDIF
		
	ADD_WIDGET_FLOAT_SLIDER("Preview line power Approach", fPowerAdjustPreviewLineApproach, 0.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Preview line power Punch", fPowerAdjustPreviewLinePunch, 0.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Preview line loft", fLoftAdjustPreviewLine, 0.0, 1.0, 0.001)
	
	ADD_WIDGET_VECTOR_SLIDER("Debug Vector 1", vDebugBounds1, -9999, 9999, 0.01)
	ADD_WIDGET_VECTOR_SLIDER("Debug Vector 2", vDebugBounds2, -9999, 9999, 0.01)
	ADD_WIDGET_VECTOR_SLIDER("Debug Vector 3", vDebugBounds3, -9999, 9999, 0.01)
	ADD_WIDGET_VECTOR_SLIDER("Debug Vector 4", vDebugBounds4, -9999, 9999, 0.01)
	ADD_WIDGET_VECTOR_SLIDER("Debug Vector 5", vDebugBounds5, -9999, 9999, 0.01)
	ADD_WIDGET_VECTOR_SLIDER("Debug Vector 6", vDebugBounds6, -9999, 9999, 0.01)
	
	ADD_WIDGET_FLOAT_SLIDER("X", vRingScale.x, -360.0, 360.0, 0.001)
	ADD_WIDGET_FLOAT_SLIDER("Y", vRingScale.y, -360.0, 360.0, 0.001)
	ADD_WIDGET_FLOAT_SLIDER("Z", vRingScale.z, -360.0, 360.0, 0.001)
	ADD_WIDGET_FLOAT_SLIDER("R", vRingColour.x, -360.0, 360.0, 0.001)
	ADD_WIDGET_FLOAT_SLIDER("G", vRingColour.y, -360.0, 360.0, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("B", vRingColour.z, -360.0, 360.0, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("A", fRingAlpha, -360.0, 360.0, 0.001)
	
	ADD_WIDGET_FLOAT_SLIDER("Swing Meter Pos X", SWING_METER_POS_X, 0.0, 1.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Swing Meter Pos Y", SWING_METER_POS_Y, 0.0, 1.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Minimap Zoom", MINIMAP_ZOOM_RATIO, 0.0,  1.0, 0.001)
	ADD_WIDGET_FLOAT_SLIDER("Minimap Pos X", MINIMAP_POS_X, -2000.0,  2000.0, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Minimap Pos Y", MINIMAP_POS_Y, -2000.0,  2000.0, 0.1)

	
	ADD_WIDGET_INT_SLIDER("PREVIEW GRAPH TYPE", CONST_GRAPH_TYPE, 0, 4, 1)
	ADD_WIDGET_INT_SLIDER("CONST_TEE_PREVIEW_0", CONST_TEE_PREVIEW_0, 0, 5000, 1)
	ADD_WIDGET_INT_SLIDER("CONST_TEE_PREVIEW_1", CONST_TEE_PREVIEW_1, 0, 5000, 1)
	ADD_WIDGET_INT_SLIDER("CONST_TEE_PREVIEW_2", CONST_TEE_PREVIEW_2, 0, 5000, 1)
	ADD_WIDGET_INT_SLIDER("CONST_TEE_PREVIEW_3", CONST_TEE_PREVIEW_3, 0, 5000, 1)
	ADD_WIDGET_INT_SLIDER("CONST_TEE_PREVIEW_4", CONST_TEE_PREVIEW_4, 0, 5000, 1)
	ADD_WIDGET_INT_SLIDER("CONST_TEE_PREVIEW_5", CONST_TEE_PREVIEW_5, 0, 5000, 1)
	ADD_WIDGET_INT_SLIDER("CONST_TEE_PREVIEW_6", CONST_TEE_PREVIEW_6, 0, 5000, 1)
	ADD_WIDGET_INT_SLIDER("CONST_TEE_PREVIEW_7", CONST_TEE_PREVIEW_7, 0, 5000, 1)
	ADD_WIDGET_INT_SLIDER("CONST_TEE_PREVIEW_8", CONST_TEE_PREVIEW_8, 0, 5000, 1)
	
	#IF IS_DEBUG_BUILD
	#IF NOT GOLF_IS_AI_ONLY
	ADD_WIDGET_BOOL("Use debug wind", bUseDebugWind)
	ADD_WIDGET_FLOAT_SLIDER("Wind Strength", thisGame.fWindStrength, 0.0, 12.0, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Wind Dir X", thisGame.vWindDirection.x, -1.0, 1.0, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Wind Dir Y", thisGame.vWindDirection.y,  -1.0, 1.0, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Wind Dir Z", thisGame.vWindDirection.z,  -1.0, 1.0, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Wind Multiplier", WIND_MULTIPLIER, 0.0, 10.0, 0.1)
	#ENDIF
	#ENDIF
	
	ADD_WIDGET_FLOAT_SLIDER("Ground spin duration modify", SPIN_MULTIPLIER_GROUND_DURATTION, 0.0, 100.0, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Hook/Slice Air Multiplier", SPIN_MULTIPLIER_H_AIR, 0.0, 100.0, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Top/Back Spin Air Multiplier", SPIN_MULTIPLIER_V_AIR, 0.0, 100.0, 0.1)

	ADD_WIDGET_FLOAT_SLIDER("Velocity Dampen Multiplier - Green C", VELOCITY_DAMPEN_GREEN_C, 0.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Velocity Dampen Multiplier - Green V", VELOCITY_DAMPEN_GREEN_V, 0.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Velocity Dampen Multiplier - Green A", VELOCITY_DAMPEN_GREEN_A, 0.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Velocity Dampen Multiplier - Rough", VELOCITY_DAMPEN_ROUGH, 0.0, 1.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Velocity Dampen Multiplier - Bunker", VELOCITY_DAMPEN_BUNKER, 0.0, 1.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Velocity Dampen Multiplier - Water", VELOCITY_DAMPEN_WATER, 0.0, 1.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Velocity Dampen Multiplier - Fairway", VELOCITY_DAMPEN_FAIRWAY, 0.0, 1.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Velocity Dampen Multiplier - Green", VELOCITY_DAMPEN_GREEN_APROACH, 0.0, 1.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Velocity Dampen Multiplier - Foliage", VELOCITY_DAMPEN_FOLIAGE, -10.0, 1.0, 0.01)
	
	ADD_WIDGET_FLOAT_SLIDER("Bounce - Cart Path", CART_PATH_BOUNCE_MULTIPLIER, -10.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Bounce - Fairwayy", FAIRWAY_BOUNCE_MULTIPLIER, -10.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Bounce - Green", GREEN_BOUNCE_MULTIPLIER, -10.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Bounce - Rough", ROUGH_BOUNCE_MULTIPLIER, -10.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Bounce - Sand", SAND_BOUNCE_MULTIPLIER, -10.0, 10.0, 0.01)
	
	ADD_WIDGET_FLOAT_SLIDER("Bounce SpeedOverGround - Cart Path", CART_BOUNCE_SPEED_OVER_GROUND, -10.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Bounce SpeedOverGround - Fairway", FAIRWAY_BOUNCE_SPEED_OVER_GROUND, -10.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Bounce SpeedOverGround - Green", GREEN_BOUNCE_SPEED_OVER_GROUND, -10.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Bounce SpeedOverGround - Rough", ROUGH_BOUNCE_SPEED_OVER_GROUND, -10.0, 10.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Bounce SpeedOverGround - Sand", SAND_BOUNCE_SPEED_OVER_GROUND, -10.0, 10.0, 0.01)
	
	
//	ADD_WIDGET_FLOAT_SLIDER("predictWood1", predictWood1, 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictWood3", predictWood3, 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictWood5", predictWood5, 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictIron3", predictIron3, 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictIron4", predictIron4, 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictIron5", predictIron5, 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictIron6", predictIron6, 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictIron7", predictIron7, 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictIron8", predictIron8, 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictIron9", predictIron9, 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictPitch", predictPitch, 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictSand" , predictSand , 0.0, 300.0, 0.1)
//	ADD_WIDGET_FLOAT_SLIDER("predictLob"  , predictLob  , 0.0, 300.0, 0.1)
//*
#IF NOT COMPILE_LOOKUPS
	ADD_WIDGET_FLOAT_SLIDER("Wood Attach Offset X", vClubAttachOffsetWood.x, 0, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Wood Attach Offset Y", vClubAttachOffsetWood.y, 0, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Wood Attach Offset Z", vClubAttachOffsetWood.z, 0, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Wood Attach Rot X", vClubAttachRotWood.x, 0, 360, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Wood Attach Rot Y", vClubAttachRotWood.y, 0, 360, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Wood Attach Rot Z", vClubAttachRotWood.z, 0, 360, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Iron Attach Offset X", vClubAttachOffsetIron.x, 0, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Iron Attach Offset Y", vClubAttachOffsetIron.y, 0, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Iron Attach Offset Z", vClubAttachOffsetIron.z, 0, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Iron Attach Rot X", vClubAttachRotIron.x, 0, 360, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Iron Attach Rot Y", vClubAttachRotIron.y, 0, 360, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Iron Attach Rot Z", vClubAttachRotIron.z, 0, 360, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Putter Attach Offset X", vClubAttachOffsetPutter.x, -5, 5, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Putter Attach Offset Y", vClubAttachOffsetPutter.y, -5, 5, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Putter Attach Offset Z", vClubAttachOffsetPutter.z, -5, 5, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Putter Attach Rot X", vClubAttachRotPutter.x, 0, 360, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Putter Attach Rot Y", vClubAttachRotPutter.y, 0, 360, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Putter Attach Rot Z", vClubAttachRotPutter.z, 0, 360, 0.1)
	ADD_WIDGET_BOOL("Apply new club rot", bReattachClub)
	
	ADD_WIDGET_FLOAT_SLIDER("Wood Offset Y", vAddressOffsetWood.y, -2, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Wood Offset X", vAddressOffsetWood.x, -2, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Iron Offset Y", vAddressOffsetIron.y,  -2, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Iron Offset X", vAddressOffsetIron.x,  -2, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Wedge Offset Y", vAddressOffsetWedge.y, -2, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Wedge Offset X", vAddressOffsetWedge.x, -2, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Putter Offset Y", vAddressOffsetPutter.y, -2, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("Putter Offset X", vAddressOffsetPutter.x, -2, 2, 0.01)
	ADD_WIDGET_BOOL("Apply new player offset", bRepositionPlayer)
#ENDIF
//*/


#IF NOT GOLF_IS_AI_ONLY
#IF IS_DEBUG_BUILD 
	shotTuning = START_WIDGET_GROUP("Set Shot")
		ADD_WIDGET_VECTOR_SLIDER("Shot Position", vDebugShotPosition, -9999, 9999, 0.01)
		ADD_WIDGET_INT_SLIDER("Hole", iDebugShotHole, 0, 8, 1)
		ADD_WIDGET_INT_SLIDER("Current Shot", iDebugShotNumber, 0, 10, 1)
		ADD_WIDGET_INT_SLIDER("Lie", iDebugShotLie, -1, 7, 1)
		ADD_WIDGET_BOOL("Use Shot", bDebugShotUse)
	STOP_WIDGET_GROUP()

	scoreTuning = START_WIDGET_GROUP("Set Score")
		ADD_WIDGET_INT_SLIDER("Add to score", iSetScore, 0, 10, 1)
		ADD_WIDGET_BOOL("Add to player 0 score", bSetPlayerScore[0])
		ADD_WIDGET_BOOL("Add to player 1 score", bSetPlayerScore[1])
		ADD_WIDGET_BOOL("Add to player 2 score", bSetPlayerScore[2])
		ADD_WIDGET_BOOL("Add to player 3 score", bSetPlayerScore[3])
	STOP_WIDGET_GROUP()
#ENDIF
#ENDIF
		/*	
		LIE_UNKNOWN = -1,
		LIE_TEE,
		LIE_FAIRWAY,
		LIE_GREEN,
		LIE_ROUGH,
		LIE_BUNKER,
		LIE_CART_PATH,
		LIE_WATER,
		LIE_CUP
		//*/
#IF NOT COMPILE_LOOKUPS
	clubTuning = START_WIDGET_GROUP("Clubs")
	INT clubIndex
	REPEAT thisGame.golfBag.iNumClubs clubIndex
		TEXT_LABEL txtClubRange = thisGame.golfBag.clubs[clubIndex].txtName
		txtClubRange += " Range"
		TEXT_LABEL txtClubLoft = thisGame.golfBag.clubs[clubIndex].txtName
		txtClubLoft += " Loft"
		TEXT_LABEL txtClubSpin = thisGame.golfBag.clubs[clubIndex].txtName
		txtClubSpin += " Spin"
		ADD_WIDGET_FLOAT_SLIDER(txtClubRange,  thisGame.golfBag.clubs[clubIndex].fClubRange, 0, 300, 1)
		ADD_WIDGET_FLOAT_SLIDER(txtClubLoft, thisGame.golfBag.clubs[clubIndex].fClubHeadLoft, 0, 90, 1)
		ADD_WIDGET_FLOAT_SLIDER(txtClubSpin, thisGame.golfBag.clubs[clubIndex].fClubSpinMagModifier, 0, 1, 0.01)
		
	ENDREPEAT
	STOP_WIDGET_GROUP()
		
		pinTuning = START_WIDGET_GROUP("Hole Positions")
		INT holeIndex
		REPEAT COUNT_OF(thisCourse.golfHole) holeIndex
			TEXT_LABEL txtPinX = "Hole "
			txtPinX += holeIndex+1
			txtPinX += ": X"
			TEXT_LABEL txtPinY = "Hole "
			txtPinY += holeIndex+1
			txtPinY += ": Y"
			TEXT_LABEL txtPinZ = "Hole "
			txtPinZ += holeIndex+1
			txtPinZ += ": Z"
			ADD_WIDGET_FLOAT_SLIDER(txtPinX, thisCourse.golfHole[holeIndex].vHolePosition.x, -2000, 2000, 0.001)
			ADD_WIDGET_FLOAT_SLIDER(txtPinY, thisCourse.golfHole[holeIndex].vHolePosition.y, -2000, 2000, 0.001)
			ADD_WIDGET_FLOAT_SLIDER(txtPinZ, thisCourse.golfHole[holeIndex].vHolePosition.z, -2000, 2000, 0.001)
			
		ENDREPEAT
		STOP_WIDGET_GROUP()

		teeTuning = START_WIDGET_GROUP("Tee Positions")
		REPEAT COUNT_OF(thisCourse.golfHole) holeIndex
			TEXT_LABEL txtPinX = "Hole "
			txtPinX += holeIndex+1
			txtPinX += ": X"
			TEXT_LABEL txtPinY = "Hole "
			txtPinY += holeIndex+1
			txtPinY += ": Y"
			TEXT_LABEL txtPinZ = "Hole "
			txtPinZ += holeIndex+1
			txtPinZ += ": Z"
			ADD_WIDGET_FLOAT_SLIDER(txtPinX, thisCourse.golfHole[holeIndex].vTeePosition.x, -2000, 2000, 0.001)
			ADD_WIDGET_FLOAT_SLIDER(txtPinY, thisCourse.golfHole[holeIndex].vTeePosition.y, -2000, 2000, 0.001)
			ADD_WIDGET_FLOAT_SLIDER(txtPinZ, thisCourse.golfHole[holeIndex].vTeePosition.z, -2000, 2000, 0.001)
			
		ENDREPEAT
		STOP_WIDGET_GROUP()
#ENDIF
	STOP_WIDGET_GROUP()

ENDPROC
#IF GOLF_IS_MP
PROC GOLF_SETUP_MP_DEBUG_WIDGETS()

ENDPROC

PROC DEBUG_MP_PLAYER_DATA_SYNC_DISPLAY(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[])
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	INT iNetPlayerIndex
	VECTOR tempVec
	
	DRAW_DEBUG_TEXT_2D("Player Broadcast Data", <<0.05, 0.05, 0>>)
	DRAW_DEBUG_TEXT_2D("PartID", <<0.05, 0.1, 0>>)
	DRAW_DEBUG_TEXT_2D("GameState", <<0.11, 0.1, 0>>) 				
	DRAW_DEBUG_TEXT_2D("GolfState", <<0.17, 0.1, 0>>) 				
	DRAW_DEBUG_TEXT_2D("PState", <<0.22, 0.1, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Flags", <<0.27, 0.1, 0>>) 				
	DRAW_DEBUG_TEXT_2D("BallID", <<0.32, 0.1, 0>>) 				
	DRAW_DEBUG_TEXT_2D("ClubID", <<0.37, 0.1, 0>>) 				
	DRAW_DEBUG_TEXT_2D("BallPos", <<0.42, 0.1, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Aim", <<0.5, 0.1, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Dist", <<0.53, 0.1, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Shot", <<0.56, 0.1, 0>>)					
	DRAW_DEBUG_TEXT_2D("Lie", <<0.59, 0.1, 0>>)		
	DRAW_DEBUG_TEXT_2D("Club", <<0.62, 0.1, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Swing", <<0.65, 0.1, 0>>)			
	DRAW_DEBUG_TEXT_2D("playerID", <<0.69, 0.1, 0>>)			
	DRAW_DEBUG_TEXT_2D("Local?", <<0.75, 0.1, 0>>)			
	TEXT_LABEL holder
	REPEAT COUNT_OF(playerBD) iNetPlayerIndex
		holder = iNetPlayerIndex
		DRAW_DEBUG_TEXT_2D(holder, <<0.05, 0.125 + (0.015 *iNetPlayerIndex) , 0>>)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iNetPlayerIndex))
			holder = ENUM_TO_INT( GET_GOLF_MP_CLIENT_MISSION_STATE( playerBD, iNetPlayerIndex))
			DRAW_DEBUG_TEXT_2D(holder, <<0.11,0.125 + (0.015 *iNetPlayerIndex), 0>>) 				
			holder = ENUM_TO_INT( GET_GOLF_MP_CLIENT_GOLF_STATE( playerBD, iNetPlayerIndex))
			DRAW_DEBUG_TEXT_2D(holder, <<0.17,0.125 + (0.015 *iNetPlayerIndex), 0>>) 				
			holder = ENUM_TO_INT( GET_GOLF_MP_CLIENT_PLAYER_STATE( playerBD, iNetPlayerIndex))
			DRAW_DEBUG_TEXT_2D(holder, <<0.22,0.125 + (0.015 *iNetPlayerIndex), 0>>) 				
			holder = ENUM_TO_INT( GET_GOLF_MP_CLIENT_CONTROL_FLAGS( playerBD, iNetPlayerIndex))
			DRAW_DEBUG_TEXT_2D(holder, <<0.27,0.125 + (0.015 *iNetPlayerIndex), 0>>) 				
			holder = NATIVE_TO_INT(GET_GOLF_MP_CLIENT_BALL( playerBD, iNetPlayerIndex))
			DRAW_DEBUG_TEXT_2D(holder, <<0.32,0.125 + (0.015 *iNetPlayerIndex), 0>>) 							
			tempVec = GET_GOLF_MP_CLIENT_BALL_POSITION( playerBD, iNetPlayerIndex)
			holder = ROUND( tempVec.x)
			holder += ","
			holder += ROUND(tempVec.y)
			holder += ","
			holder += ROUND( tempVec.z)
			DRAW_DEBUG_TEXT_2D(holder, <<0.42,0.125 + (0.015 *iNetPlayerIndex), 0>>) 				
			holder = ROUND(  GET_GOLF_MP_CLIENT_AIM( playerBD, iNetPlayerIndex))
			DRAW_DEBUG_TEXT_2D(holder, <<0.5,0.125 + (0.015 *iNetPlayerIndex), 0>>) 				
			holder = ROUND(  GET_GOLF_MP_CLIENT_DISTANCE_TO_HOLE( playerBD, iNetPlayerIndex))
			DRAW_DEBUG_TEXT_2D(holder, <<0.53,0.125 + (0.015 *iNetPlayerIndex), 0>>) 				
			holder =  GET_GOLF_MP_CLIENT_SHOTS_ON_CURRENT_HOLE( playerBD, iNetPlayerIndex)
			DRAW_DEBUG_TEXT_2D(holder, <<0.56, 0.125 + (0.015 *iNetPlayerIndex), 0>>)					
			holder = ENUM_TO_INT( GET_GOLF_MP_CLIENT_LIE( playerBD, iNetPlayerIndex))
			DRAW_DEBUG_TEXT_2D(holder, <<0.59, 0.125 + (0.015 *iNetPlayerIndex), 0>>)		
			holder =  GET_GOLF_MP_CLIENT_CLUB_INDEX( playerBD, iNetPlayerIndex)
			DRAW_DEBUG_TEXT_2D(holder, <<0.62, 0.125 + (0.015 *iNetPlayerIndex), 0>>) 				
			holder = ENUM_TO_INT( GET_GOLF_MP_CLIENT_SWING_STYLE( playerBD, iNetPlayerIndex))
			DRAW_DEBUG_TEXT_2D(holder, <<0.65, 0.125 + (0.015 *iNetPlayerIndex), 0>>)			
			INT iPlayerIndex = GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, iNetPlayerIndex)
			holder = iPlayerIndex
			DRAW_DEBUG_TEXT_2D(holder, <<0.69, 0.125 + (0.015 *iNetPlayerIndex), 0>>)			
			IF GET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL(thisFoursome, iPlayerIndex) = HUMAN_LOCAL_MP
				holder = "Local"
			ELSE
				holder = "Remote"
			ENDIF
			DRAW_DEBUG_TEXT_2D(holder, <<0.75, 0.125 + (0.015 *iNetPlayerIndex), 0>>)			
		ELSE
			DRAW_DEBUG_TEXT_2D("Inactive", <<0.11, 0.125 + (0.015 *iNetPlayerIndex) , 0>>)
		ENDIF
	ENDREPEAT


	DRAW_DEBUG_TEXT_2D("Server Broadcast Data", <<0.05, 0.2, 0>>)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		DRAW_DEBUG_TEXT_2D("HOST", <<0.22, 0.2, 0>>) 				
	ELSE
		DRAW_DEBUG_TEXT_2D("CLIENT", <<0.22, 0.2, 0>>) 				
	ENDIF
	DRAW_DEBUG_TEXT_2D("GameState", <<0.05, 0.225, 0>>)
	DRAW_DEBUG_TEXT_2D("GolfState", <<0.11, 0.225, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Flags", <<0.17, 0.225, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Shooter", <<0.23, 0.225, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Hole", <<0.26, 0.225, 0>>) 				

	holder = ENUM_TO_INT( GET_GOLF_MP_SERVER_MISSION_STATE( serverBD ))
	DRAW_DEBUG_TEXT_2D(holder, <<0.05, 0.25, 0>>)
	holder = ENUM_TO_INT( GET_GOLF_MP_SERVER_GOLF_STATE( serverBD ))
	DRAW_DEBUG_TEXT_2D(holder, <<0.11, 0.25, 0>>) 				
	holder = ENUM_TO_INT( GET_GOLF_MP_SERVER_CONTROL_FLAGS( serverBD ))
	DRAW_DEBUG_TEXT_2D(holder, <<0.17, 0.25, 0>>) 				
	holder = GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX( serverBD )
	DRAW_DEBUG_TEXT_2D(holder, <<0.23, 0.25, 0>>) 				
	holder = GET_GOLF_MP_SERVER_CURRENT_HOLE( serverBD )
	DRAW_DEBUG_TEXT_2D(holder, <<0.26, 0.25, 0>>) 				

	


	DRAW_DEBUG_TEXT_2D("Local Foursome Player Data", <<0.05, 0.35, 0>>)
	DRAW_DEBUG_TEXT_2D("PartID", <<0.05, 0.375, 0>>)
	DRAW_DEBUG_TEXT_2D("PState", <<0.11, 0.375, 0>>) 				
	DRAW_DEBUG_TEXT_2D("BallPos", <<0.16, 0.375, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Aim", <<0.24, 0.375, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Dist", <<0.27, 0.375, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Shot", <<0.3, 0.375, 0>>)					
	DRAW_DEBUG_TEXT_2D("Lie", <<0.33, 0.375, 0>>)		
	DRAW_DEBUG_TEXT_2D("Club", <<0.36, 0.375, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Swing", <<0.39, 0.375, 0>>)			
	DRAW_DEBUG_TEXT_2D("netID", <<0.42,  0.375, 0>>)			
	DRAW_DEBUG_TEXT_2D("Current?", <<0.45,  0.375, 0>>)			
	
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iNetPlayerIndex
		holder = iNetPlayerIndex
		DRAW_DEBUG_TEXT_2D(holder, <<0.05, 0.4 + (0.015 *iNetPlayerIndex) , 0>>)
		holder = ENUM_TO_INT(GET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, iNetPlayerIndex))
		DRAW_DEBUG_TEXT_2D(holder, <<0.11, 0.4 + (0.015 *iNetPlayerIndex), 0>>) 				
		tempVec = GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL_POSITION( thisFoursome, iNetPlayerIndex)
		holder = ROUND( tempVec.x)
		holder += ","
		holder += ROUND(tempVec.y)
		holder += ","
		holder += ROUND( tempVec.z)
		DRAW_DEBUG_TEXT_2D(holder, <<0.16, 0.4 + (0.015 *iNetPlayerIndex), 0>>) 				
		holder = ROUND( GET_GOLF_FOURSOME_INDEXED_PLAYER_AIM( thisFoursome, iNetPlayerIndex))
		DRAW_DEBUG_TEXT_2D(holder, <<0.24, 0.4 + (0.015 *iNetPlayerIndex), 0>>) 				
		holder = ROUND( GET_GOLF_FOURSOME_INDEXED_PLAYER_DISTANCE_TO_HOLE( thisFoursome, iNetPlayerIndex))
		DRAW_DEBUG_TEXT_2D(holder, <<0.27, 0.4 + (0.015 *iNetPlayerIndex), 0>>) 				
		holder = GET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE( thisFoursome, iNetPlayerIndex)
		DRAW_DEBUG_TEXT_2D(holder, <<0.3, 0.4 + (0.015 *iNetPlayerIndex), 0>>)					
		holder = ENUM_TO_INT( GET_GOLF_FOURSOME_INDEXED_PLAYER_LIE( thisFoursome, iNetPlayerIndex))
		DRAW_DEBUG_TEXT_2D(holder, <<0.33, 0.4 + (0.015 *iNetPlayerIndex), 0>>)		
		holder = GET_GOLF_FOURSOME_INDEXED_PLAYER_CURRENT_CLUB( thisFoursome, iNetPlayerIndex)
		DRAW_DEBUG_TEXT_2D(holder, <<0.36, 0.4 + (0.015 *iNetPlayerIndex), 0>>) 				
		holder = ENUM_TO_INT(GET_GOLF_FOURSOME_INDEXED_PLAYER_SWING_STYLE( thisFoursome, iNetPlayerIndex))
		DRAW_DEBUG_TEXT_2D(holder, <<0.39, 0.4 + (0.015 *iNetPlayerIndex), 0>>)			
		holder = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT( thisPlayers, iNetPlayerIndex)
		DRAW_DEBUG_TEXT_2D(holder, <<0.42,  0.4 + (0.015 *iNetPlayerIndex), 0>>)			
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = iNetPlayerIndex
			holder = "Current"
			DRAW_DEBUG_TEXT_2D(holder, <<0.45,  0.4 + (0.015 *iNetPlayerIndex), 0>>)			
		ENDIF
	ENDREPEAT
	
	DRAW_DEBUG_TEXT_2D("Local Foursome/General Data", <<0.05, 0.5, 0>>)
	DRAW_DEBUG_TEXT_2D("State", <<0.05, 0.525, 0>>)
	DRAW_DEBUG_TEXT_2D("Flags", <<0.10, 0.525, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Hole", <<0.15, 0.525, 0>>) 				
	DRAW_DEBUG_TEXT_2D("CPlayer", <<0.18, 0.525, 0>>) 				
	DRAW_DEBUG_TEXT_2D("LPlayer", <<0.24, 0.525, 0>>) 				
	holder = ENUM_TO_INT( GET_GOLF_FOURSOME_STATE(thisFoursome))
	DRAW_DEBUG_TEXT_2D(holder, <<0.05, 0.55, 0>>)
	holder = ENUM_TO_INT( GET_GOLF_CONTROL_FLAGS(thisGame))
	DRAW_DEBUG_TEXT_2D(holder, <<0.10, 0.55, 0>>) 				
	holder = GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)
	DRAW_DEBUG_TEXT_2D(holder, <<0.15, 0.55, 0>>) 				
	holder = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
	DRAW_DEBUG_TEXT_2D(holder, <<0.18, 0.55, 0>>) 				
	holder = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
	DRAW_DEBUG_TEXT_2D(holder, <<0.24, 0.55, 0>>) 				


	DRAW_DEBUG_TEXT_2D("Server Score Data", <<0.05, 0.6, 0>>)
	DRAW_DEBUG_TEXT_2D("PartID", <<0.05, 0.625, 0>>) 				
	DRAW_DEBUG_TEXT_2D("Total Score", <<0.11, 0.625, 0>>) 				
	DRAW_DEBUG_TEXT_2D("1", <<0.17, 0.625, 0>>) 				
	DRAW_DEBUG_TEXT_2D("2", <<0.20, 0.625, 0>>) 				
	DRAW_DEBUG_TEXT_2D("3", <<0.23, 0.625, 0>>) 				
	DRAW_DEBUG_TEXT_2D("4", <<0.26, 0.625, 0>>) 				
	DRAW_DEBUG_TEXT_2D("5", <<0.29, 0.625, 0>>) 				
	DRAW_DEBUG_TEXT_2D("6", <<0.32, 0.625, 0>>) 				
	DRAW_DEBUG_TEXT_2D("7", <<0.35, 0.625, 0>>) 				
	DRAW_DEBUG_TEXT_2D("8", <<0.38, 0.625, 0>>) 				
	DRAW_DEBUG_TEXT_2D("9", <<0.41, 0.625, 0>>) 				

	INT iHoleIndex
	REPEAT COUNT_OF(playerBD) iNetPlayerIndex
		holder = iNetPlayerIndex
		DRAW_DEBUG_TEXT_2D(holder, <<0.05, 0.65 + (0.015 *iNetPlayerIndex) , 0>>)
		holder = GET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE (serverBD, iNetPlayerIndex)
		DRAW_DEBUG_TEXT_2D(holder, <<0.11,  0.65 + (0.015 *iNetPlayerIndex), 0>>) 				
		REPEAT 9 iHoleIndex
			holder = GET_GOLF_MP_SERVER_PLAYER_HOLE_SCORE(serverBD, iNetPlayerIndex, iHoleIndex)
			DRAW_DEBUG_TEXT_2D(holder, <<0.17 + (0.03 * iHoleIndex),  0.65 + (0.015 *iNetPlayerIndex), 0>>) 				
		ENDREPEAT
	ENDREPEAT
	
ENDPROC


#ENDIF

PROC GOLF_CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(clubTuning)
		DELETE_WIDGET_GROUP(clubTuning)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(pinTuning)
		DELETE_WIDGET_GROUP(pinTuning)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(teeTuning)
		DELETE_WIDGET_GROUP(teeTuning)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(golfTuning)
		DELETE_WIDGET_GROUP(golfTuning)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(shotTuning)
		DELETE_WIDGET_GROUP(shotTuning)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(scoreTuning)
		DELETE_WIDGET_GROUP(scoreTuning)
	ENDIF

ENDPROC

FUNC STRING GET_PLAYER_STATE_NAME(GOLF_PLAYER_STATE playerState)
	SWITCH playerState
		CASE GPS_PLACE_BALL
			RETURN "PLACE BALL"
		CASE GPS_APPROACH_BALL
			RETURN "APPROACH BALL"
		CASE GPS_ADDRESS_BALL
			RETURN "ADDRESS BALL"
		CASE GPS_SWING_METER
			RETURN "SWING METER"
		CASE GPS_BALL_IN_FLIGHT
			RETURN "BALL IN FLIGHT"
		CASE GPS_BALL_AT_REST
			RETURN "BALL AT RESET"
		CASE GPS_BALL_IN_HOLE_CELEBRATION
			RETURN "BALL IN HOLE"
		CASE GPS_DONE_WITH_SHOT
			RETURN "DONE WITH SHOT"
		CASE GPS_DONE_WITH_HOLE
			RETURN "DONE WITH HOLE"
		DEFAULT
			RETURN "DEFAULT"
	ENDSWITCH
	RETURN "UNKNOWN"
ENDFUNC

FUNC STRING GET_FOURSOME_STATE_NAME(GOLF_GAME_STATE golfState)
	SWITCH golfState
		CASE GGS_NAVIGATE_TO_SHOT
			RETURN "NAVIGATE TO SHOT"
		CASE GGS_NAVIGATE_OUT_OF_BOUNDS
			RETURN "NAVIGATE OOB"
		CASE GGS_TAKING_SHOT
			RETURN "TAKING SHOT"
		CASE GGS_SCORECARD_END_HOLE
			RETURN "SCORECARD END HOLE"
		CASE GGS_SCORECARD_END_GAME
			RETURN "SCORECARD END GAME"
		CASE GGS_GET_IN_CART
			RETURN "GET_IN_CART"
		CASE GGS_DRIVE_TO_SHOT
			RETURN "DRIVE_TO_SHOT"
		DEFAULT
			RETURN "DEFAULT"
	ENDSWITCH
	RETURN "UNKNOWN"
ENDFUNC



PROC GOLF_DEBUG_DRAW_FOURSOMES(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome[])
	
	INT foursomeIndex, playerIndex
	VECTOR currentHoleLoc
	UNUSED_PARAMETER(thisGame)
	
	IF displayLines != (bDebugDrawForces | bDebugDrawAI)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bDebugDrawForces | bDebugDrawAI)
		displayLines = (bDebugDrawForces | bDebugDrawAI)
	ENDIF
	
	REPEAT COUNT_OF(thisFoursome) foursomeIndex
	    currentHoleLoc = GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome[foursomeIndex]))
		DRAW_DEBUG_LINE(currentHoleLoc, currentHoleLoc + <<0,0,1>>, 255, 0, 0)
		currentHoleLoc = GET_GOLF_HOLE_TEE_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome[foursomeIndex]))
		DRAW_DEBUG_LINE(currentHoleLoc, currentHoleLoc + <<0,0,1>>, 255, 0, 0)
	ENDREPEAT
	
	IF bDebugDrawAI

		REPEAT COUNT_OF(thisFoursome) foursomeIndex
			REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS( thisFoursome[foursomeIndex] ) playerIndex
				IF DOES_ENTITY_EXIST(thisFoursome[foursomeIndex].playerCore[playerIndex].pedGolfPlayer)
					IF NOT IS_ENTITY_DEAD(thisFoursome[foursomeIndex].playerCore[playerIndex].pedGolfPlayer)
						DRAW_DEBUG_TEXT( GET_PLAYER_STATE_NAME(thisFoursome[foursomeIndex].playerCore[playerIndex].playerState), GET_ENTITY_COORDS(thisFoursome[foursomeIndex].playerCore[playerIndex].pedGolfPlayer) + <<0,0,2>>)
						DRAW_DEBUG_TEXT( GET_FOURSOME_STATE_NAME(thisFoursome[foursomeIndex].golfState), GET_ENTITY_COORDS(thisFoursome[foursomeIndex].playerCore[playerIndex].pedGolfPlayer) + <<0,0,2.5>>)
						DRAW_DEBUG_SPHERE( thisFoursome[foursomeIndex].playerCore[playerIndex].vBallPosition, 0.5)
						DRAW_DEBUG_LINE(thisFoursome[foursomeIndex].playerCore[playerIndex].vBallPosition, GET_ENTITY_COORDS(thisFoursome[foursomeIndex].playerCore[playerIndex].pedGolfPlayer))
						
						TEXT_LABEL golfHoleStr = "Hole: "
						golfHoleStr += GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome[foursomeIndex])
						DRAW_DEBUG_TEXT(golfHoleStr, GET_ENTITY_COORDS(thisFoursome[foursomeIndex].playerCore[playerIndex].pedGolfPlayer) + <<0,0,2.75>>)
						
						golfHoleStr = "Shots: "
						golfHoleStr += GET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(thisFoursome[foursomeIndex], playerIndex)
						DRAW_DEBUG_TEXT(golfHoleStr, GET_ENTITY_COORDS(thisFoursome[foursomeIndex].playerCore[playerIndex].pedGolfPlayer) + <<0,0,1.75>>)
						
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ENDIF
	
	IF bReattachClub
		REPEAT COUNT_OF(thisFoursome) foursomeIndex
			IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome[foursomeIndex]))
				eCLUB_TYPE clubType =  GET_GOLF_CLUB_TYPE( thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome[foursomeIndex]))
				PED_INDEX pedIndex = GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome[foursomeIndex])
				ATTACH_ENTITY_TO_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome[foursomeIndex]), GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome[foursomeIndex]),  GET_PED_BONE_INDEX( GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome[foursomeIndex]), BONETAG_L_HAND), GET_GOLF_CLUB_ATTACH_OFFSET(pedIndex, clubType), GET_GOLF_CLUB_ATTACH_ROTATION(pedIndex, clubType) )//, <<0.03, 0.017, 0.0>>,  <<4.75, 3.0, 1.25>>, FALSE)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC GOLF_DEBUG_DRAW(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)

	UNUSED_PARAMETER(thisGame)
	
	IF displayLines != (bDebugDrawForces | bDebugDrawAI | bDisplayStats)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bDebugDrawForces | bDebugDrawAI | bDisplayStats)
		displayLines = (bDebugDrawForces | bDebugDrawAI | bDisplayStats)
	ENDIF
	
	VECTOR currentHoleLoc = GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
	DRAW_DEBUG_LINE(currentHoleLoc, currentHoleLoc + <<0,0,1>>, 255, 0, 0)
	currentHoleLoc = GET_GOLF_HOLE_TEE_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
	DRAW_DEBUG_LINE(currentHoleLoc, currentHoleLoc + <<0,0,1>>, 255, 0, 0)
	IF bDebugDrawForces
		DRAW_DEBUG_LINE(GET_GOLF_HOLE_PIN_POSITION( thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), GET_GOLF_HOLE_PIN_POSITION( thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) + <<0,0, 10>>, 255, 255, 0, 255)
	ENDIF
	IF bDebugDrawAI
		INT playerIndex
		REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS( thisFoursome ) playerIndex
			IF DOES_ENTITY_EXIST(thisFoursome.playerCore[playerIndex].pedGolfPlayer)
				IF NOT IS_ENTITY_DEAD(thisFoursome.playerCore[playerIndex].pedGolfPlayer)
					DRAW_DEBUG_TEXT( GET_PLAYER_STATE_NAME(thisFoursome.playerCore[playerIndex].playerState), GET_ENTITY_COORDS(thisFoursome.playerCore[playerIndex].pedGolfPlayer) + <<0,0,2>>)
					DRAW_DEBUG_TEXT( GET_FOURSOME_STATE_NAME(thisFoursome.golfState), GET_ENTITY_COORDS(thisFoursome.playerCore[playerIndex].pedGolfPlayer) + <<0,0,2.5>>)
					DRAW_DEBUG_SPHERE( thisFoursome.playerCore[playerIndex].vBallPosition, 0.5)
					DRAW_DEBUG_LINE( thisFoursome.playerCore[playerIndex].vBallPosition, GET_ENTITY_COORDS(thisFoursome.playerCore[playerIndex].pedGolfPlayer))
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF bReattachClub
		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome))
			eCLUB_TYPE clubType =  GET_GOLF_CLUB_TYPE( thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
			PED_INDEX pedIndex = GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome)
			ATTACH_ENTITY_TO_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome),  GET_PED_BONE_INDEX( GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), BONETAG_R_HAND), GET_GOLF_CLUB_ATTACH_OFFSET(pedIndex, clubType), GET_GOLF_CLUB_ATTACH_ROTATION(pedIndex, clubType) )//, <<0.03, 0.017, 0.0>>,  <<4.75, 3.0, 1.25>>, FALSE)
		ENDIF
	ENDIF
	#IF NOT GOLF_IS_AI_ONLY
	IF bDisplayStats
		TEXT_LABEL_63 text = "Number of rounds played "
		text+= GET_GOLF_STAT_ROUNDS_PLAYED()
		DRAW_DEBUG_TEXT_2D(text, <<0.7, 0.15, 0.0>>)
		text = "Fairway Drive % "
		text+=FLOOR(GET_GOLF_STAT_FAIRWAY_DRIVE_PERCENT() *100.0)
		DRAW_DEBUG_TEXT_2D(text, <<0.7, 0.2, 0.0>>)
		text = "Average Putts per Hole "
		text+=FLOOR(GET_GOLF_STAT_AVERAGE_PUTTS() *100.0)
		DRAW_DEBUG_TEXT_2D(text, <<0.7, 0.25, 0.0>>)
		text = "Lowest Score "
		text+= GET_GOLF_STAT_LOWEST_ROUND()
		DRAW_DEBUG_TEXT_2D(text, <<0.7, 0.3, 0.0>>)
		text = "Longet Made Putt "
		text+= FLOOR(GET_GOLF_STAT_LONGEST_PUTT() *100.0)
		DRAW_DEBUG_TEXT_2D(text, <<0.7, 0.35, 0.0>>)
		text = "Longest Drive "
		text+= FLOOR(GET_GOLF_STAT_LONGEST_DRIVE() *100.0)
		DRAW_DEBUG_TEXT_2D(text, <<0.7, 0.4, 0.0>>)
		
		text = "Holes played "
		text += g_savedGlobals.sGolfData.iNumHolesPlayed
		DRAW_DEBUG_TEXT_2D(text, <<0.7, 0.45, 0.0>>)
		
		text = "Total Putts  "
		text += g_savedGlobals.sGolfData.iNumPuttsTotal
		DRAW_DEBUG_TEXT_2D(text, <<0.7, 0.5, 0.0>>)
		
		text = "Total fairway drives "
		text += g_savedGlobals.sGolfData.iNumFairwayDrivesTotal
		DRAW_DEBUG_TEXT_2D(text, <<0.7, 0.55, 0.0>>)
	ENDIF
	#ENDIF
ENDPROC

#IF NOT GOLF_IS_AI_ONLY


PROC SET_GOLF_DEBUG_PLAYER_SCORE(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[])

	INT iPlayer
	INT iCurrentHole = GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)
	
	IF bSetPlayerScore[0]
		iPlayer = 0
	ENDIF

	IF bSetPlayerScore[1]
		iPlayer = 1
	ENDIF
	
	IF bSetPlayerScore[2]
		iPlayer = 2
	ENDIF

	IF bSetPlayerScore[3]
		iPlayer = 3
	ENDIF
	
	SET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(thisFoursome, iPlayer, iSetScore)
	SET_GOLF_PLAYER_HOLE_SCORE_BY_INDEX(thisPlayers, iPlayer, iCurrentHole, iSetScore)
	INCREMENT_GOLF_PLAYER_CURRENT_SCORE(thisPlayers[iPlayer], GET_GOLF_PLAYER_HOLE_SCORE_BY_INDEX(thisPlayers, iPlayer, iCurrentHole))
	SET_PLAYERCARD_SLOT(thisHelpers, thisFoursome, thisFoursome.playerCore[iPlayer].playerCard, GET_GOLF_PLAYER_CURRENT_SCORE(thisPlayers[iPlayer]), FALSE, FALSE)

	bSetPlayerScore[iPlayer] = FALSE

ENDPROC
PROC SET_GOLF_DEBUG_SHOT(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse)

	bDebugShotUse = FALSE
				
	SET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome, 0)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome, vDebugShotPosition)
	SET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome, iDebugShotHole)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, <<0,0,0>>, INT_TO_ENUM(GOLF_LIE_TYPE, iDebugShotLie))
	SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome, iDebugShotNumber)
	
	SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_NAVIGATE_TO_SHOT)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome, GPS_APPROACH_BALL)
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
//		OBJECT_INDEX ball = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)
//		DELETE_OBJECT(ball)
	ENDIF
	
	SET_GOLF_FOURSOME_CURRENT_PLAYER_DISTANCE_TO_HOLE(thisFoursome, VDIST(vDebugShotPosition, GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))))
ENDPROC

PROC SET_GOLF_DEBUG_HARD_CODED_SHOT(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse)

	bDebugHardCodedShotUse = FALSE
	VECTOR vHardCodedPos = <<-1086.955, 5.933, 49.792>>
	
	SET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, 1, GPS_DONE_WITH_HOLE)
	SET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome, 0)
	
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 1)) AND NOT IS_ENTITY_DEAD(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 1))
		SET_ENTITY_COORDS(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 1), <<-1087.7296, 3.2469, 49.8298>>)
		SET_ENTITY_HEADING(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 1), 123.1206)
	ENDIF
	
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_CART(thisFoursome)) AND NOT IS_ENTITY_DEAD(GET_GOLF_FOURSOME_CURRENT_PLAYER_CART(thisFoursome))
		SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_CART(thisFoursome), <<-1090.5536, -26.0716, 49.3109>>)
		SET_ENTITY_HEADING(GET_GOLF_FOURSOME_CURRENT_PLAYER_CART(thisFoursome), 248.4617)
	ENDIF

	SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome, vHardCodedPos)
	SET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome, 3)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, <<0,0,0>>, LIE_GREEN)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome, 1)
	
	SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_NAVIGATE_TO_SHOT)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome, GPS_APPROACH_BALL)
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		OBJECT_INDEX myBall = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)
		DELETE_OBJECT(myBall)
	ENDIF
	
	SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome, CREATE_OBJECT_NO_OFFSET(PROP_GOLF_BALL, vHardCodedPos))
	SET_GOLF_FOURSOME_CURRENT_PLAYER_DISTANCE_TO_HOLE(thisFoursome, VDIST(vHardCodedPos, GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))))
ENDPROC
#ENDIF
