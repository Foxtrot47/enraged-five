USING "golf.sch"

ENUM GOLF_MP_SPECTATOR_STATE

	GOLF_SPECTATE_INIT,
	GOLF_SPECTATE_STREAM,
	GOLF_SPECTATE_SETUP,
	GOLF_SPECTATE_WAIT_FOR_END,
	GOLF_SPECATATE_END

ENDENUM

GOLF_MP_SPECTATOR_STATE localSpectateState


PROC GOLF_MANAGE_SPECTATOR(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], TEXT_LABEL_63 &sPlayerNames[], GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[])
	
	STREAMED_MODEL	golfAssets[11]
	INT iHeadTextureLoadIndex = 0, iHeadTextureLoadFailSafe
	BOOL bIsHeadTexturedRegistered = FALSE
	PEDHEADSHOT_ID myPedHeadshot
	
	bAllowLeaderboardUpload = FALSE
	
	WHILE localSpectateState != GOLF_SPECATATE_END
		
		IF GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) >= GMS_OUTRO_INIT
			//joined as everyone was exiting, quit golf
			CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(FALSE, thisGame, thisCourse, thisFoursome, thisPlayers,  thisHelpers)
		ELSE
			SWITCH localSpectateState
			
				CASE GOLF_SPECTATE_INIT
					GOLF_SETUP_CURRENT_COURSE(thisCourse)
					GOLF_STREAM_ASSETS(golfAssets)
					GOLF_STREAM_UI(thisHelpers)
					
					REQUEST_ADDITIONAL_TEXT("SP_GOLF", MINIGAME_TEXT_SLOT)
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					REMOVE_AMBIENT_GOLF_FLAGS()
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1378.6844, 55.8883, 52.6896>>)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					
					localSpectateState = GOLF_SPECTATE_STREAM
				BREAK
				
				CASE GOLF_SPECTATE_STREAM
					CDEBUG1LN(DEBUG_GOLF, "Loading spectator in golf")
					IF GOLF_ASSETS_ARE_STREAMED(thisHelpers, golfAssets)
						IF HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
						AND GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) >= GMS_PLAY_GOLF
							CDEBUG1LN(DEBUG_GOLF, "Done loading spectate")
							
							thisHelpers.iStartingHole = serverBD.iStartHole
							thisHelpers.iEndingHole = serverBD.iEndHole
							
							GOLF_SETUP_CLUBS (thisGame.golfBag)
							
							SET_SCOREBOARD_TITLE(thisHelpers)
							SET_COURSE_PAR(thisHelpers)
							
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
							
							IF NOT DOES_ENTITY_EXIST(GET_GOLF_HOLE_FLAG(thisCourse, 0))
								PRINTLN("Create Golf Flags spectate")
								INT iHoleIndex
								REPEAT GET_GOLF_COURSE_NUM_HOLES(thisCourse) iHoleIndex
									CREATE_GOLF_FLAG(thisCourse, iHoleIndex)
								ENDREPEAT
							ENDIF
							
							SET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome, GET_GOLF_MP_SERVER_CURRENT_HOLE(serverBD))
							SET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome, GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD)))
							GOLF_SETUP_NETWORK_PLAYERS(thisPlayers, thisFoursome)
							
							localSpectateState = GOLF_SPECTATE_SETUP
						ENDIF
					ENDIF
				BREAK
				
				CASE GOLF_SPECTATE_SETUP
				
					IF iHeadTextureLoadIndex < MP_GOLF_MAX_PLAYERS
						IF REGISTER_HEAD_TEXTURE_MP(iHeadTextureLoadIndex, thisFoursome, thisPlayers, myPedHeadshot, bIsHeadTexturedRegistered)
						OR iHeadTextureLoadFailSafe > 10
							iHeadTextureLoadIndex++
							bIsHeadTexturedRegistered = FALSE
							PRINTLN("Frames needed to load headshot ", iHeadTextureLoadFailSafe)
							iHeadTextureLoadFailSafe = 0
						ELSE
							iHeadTextureLoadFailSafe++
						ENDIF
					ELSE
						SET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome, GET_GOLF_MP_SERVER_CURRENT_HOLE(serverBD))
						GOLF_INIT_CURRENT_HOLE(thisCourse, thisFoursome) //this function has shitty name, has nothing to do with 'current' hole
						
						//save everyones current scores
						INT iNetPlayerIndex, iHole
						REPEAT COUNT_OF(playerBD) iNetPlayerIndex
							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX( iNetPlayerIndex))
								INT iPlayerIndex 
								iPlayerIndex = GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, iNetPlayerIndex)
								
								CDEBUG1LN(DEBUG_GOLF, "Setting scores for player at player index ", iPlayerIndex)
								IF iPlayerIndex > -1
									REPEAT GET_GOLF_COURSE_NUM_HOLES(thisCourse) iHole
										SET_GOLF_PLAYER_HOLE_SCORE_BY_INDEX(thisPlayers, iPlayerIndex, iHole, GET_GOLF_MP_SERVER_PLAYER_HOLE_SCORE(serverBD, iNetPlayerIndex, iHole))
									ENDREPEAT
									
									SET_GOLF_PLAYER_CURRENT_SCORE_BY_INDEX(thisPlayers, iPlayerIndex, GET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(serverBD, iNetPlayerIndex))
								ENDIF
							ENDIF
						ENDREPEAT
						
						GOLF_ADD_NETWORK_PLAYERS_TO_UI(sPlayerNames, thisHelpers, thisCourse, thisFoursome, thisPlayers, TRUE, TRUE)
						
						SET_GOLF_MINIMAP(thisFoursome, thisCourse)
						SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_NAVIGATE | GUC_REFRESH_PERMANENT)
						
						TRIGGER_MUSIC_EVENT("MGGF_START")
						HIDE_ALL_PLAYER_BLIPS(TRUE)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						
						thisHelpers.iMatchHistoryID = serverBD.iPosixTime
						DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_GOLF, serverBD.iPosixTime, serverBD.iHashMac)
						
						IF serverBD.iWeather = ciGOLF_OPTION_SUNNY
							SET_OVERRIDE_WEATHER("EXTRASUNNY")
						ELIF  serverBD.iWeather = ciGOLF_OPTION_RAINING
							SET_OVERRIDE_WEATHER("RAIN")
						ENDIF
						
						CLEAR_HELP()
						localSpectateState = GOLF_SPECTATE_WAIT_FOR_END
					ENDIF
					
				BREAK
				
				CASE GOLF_SPECTATE_WAIT_FOR_END
					CDEBUG1LN(DEBUG_GOLF, "Wait till player is adressing ball or at scoreboard")
					
					SET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome, GET_PLAYER_INDEX_FROM_NET_INDEX(thisPlayers, GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD)))
					
					INT piPlayer
					piPlayer = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT( thisPlayers, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
					
					IF ARE_ALL_GOLFER_SPECTATORS(thisFoursome, playerBD, thisPlayers)
						CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(TRUE, thisGame, thisCourse, thisFoursome, thisPlayers, thisHelpers, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
					ENDIF
					
					//Let other clients know you are good to move to next shot
					IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MOVE_TO_NEXT_SHOT_MP)
						SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MOVE_TO_NEXT_SHOT_MP)
						SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_VIEWING_ADDRESS_BALL)
					ENDIF
					
					IF GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) = GMS_PLAY_GOLF
					AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						IF GET_GOLF_MP_CLIENT_PLAYER_STATE(playerBD, piPlayer) = GPS_ADDRESS_BALL
							SET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome, GET_GOLF_MP_SERVER_CURRENT_HOLE(serverBD))
							localSpectateState = GOLF_SPECATATE_END
						ENDIF
						
						IF IS_ANY_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(thisFoursome, playerBD, thisPlayers, GFC2_AT_GOLF_SCOREBOARD)
							SET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome, GET_GOLF_MP_SERVER_CURRENT_HOLE(serverBD)-1)
							GOLF_MP_CLIENT_MOVE_TO_NEXT_HOLE(serverBD, playerBD, thisCourse, thisFoursome, thisPlayers, thisHelpers, thisGame)
							localSpectateState = GOLF_SPECATATE_END
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
		ENDIF
	
		WAIT(0)
	ENDWHILE

ENDPROC
