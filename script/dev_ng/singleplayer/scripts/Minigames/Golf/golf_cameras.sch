USING "minigames_helpers.sch"
USING "golf.sch"
USING "golf_foursome.sch"
USING "golf_helpers.sch"
USING "golf_ai.sch"
USING "golf_buddies_lib.sch"

USING "script_camera.sch"

PROC GOLF_END_CUTSCENE_TO_FIRST_PERSON_CAMERA_EFFECT()
	#IF NOT GOLF_IS_MP
	
	IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
		CDEBUG1LN(DEBUG_GOLF, "Cut to firstperson Golf")
		DEBUG_PRINTCALLSTACK()
		ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
		PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
		WAIT(300)
	ENDIF
	
	#ENDIF
ENDPROC

FUNC BOOL IS_ANY_GOLF_CAM_RENDERING(GOLF_HELPERS &thisHelpers)
	IF IS_GOLF_CAM_RENDERING(thisHelpers.camNavigate)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camNavigate2)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camFlight)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camAddress)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camPreview)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camPreviewSplineForward)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camLookAtGreen)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camReaction)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camMPApproach)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camUpStart)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camUpEnd)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camPauseAnimCamera)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camFlyover1)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camFlyover2)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camFlyover3)
	OR IS_GOLF_CAM_RENDERING(thisHelpers.camFlyover2End)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GOLF_CAM_INTERPOLATING(CAMERA_INDEX golfCam, BOOL bCheckRendering)

	IF DOES_CAM_EXIST(golfCam)
		IF IS_CAM_INTERPOLATING(golfCam)
			RETURN TRUE
		ENDIF
		
		IF bCheckRendering AND IS_CAM_RENDERING(golfCam)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_ROT_FOR_CAM_TO_POINT_AT_COORD(VECTOR camPos, VECTOR pointAt, VECTOR& vRot)
	VECTOR dirVector

	dirVector = pointAt - camPos
	vRot.x = ATAN(dirVector.z/SQRT(dirVector.x*dirVector.x + dirVector.y*dirVector.y))
	vRot.z = -ATAN(dirVector.x/dirVector.y)
	IF dirVector.y < 0
		IF dirVector.x < 0
			vRot.z += 180
		ELSE
			vRot.z -= 180
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_TEE_PREVIEW_INTERP_TIME_FOR_HOLE(INT iHole)
	SWITCH iHole
		CASE 9 RETURN CONST_TEE_PREVIEW_0		BREAK
		CASE 1 RETURN CONST_TEE_PREVIEW_1		BREAK
		CASE 2 RETURN CONST_TEE_PREVIEW_2		BREAK
		CASE 3 RETURN CONST_TEE_PREVIEW_3		BREAK
		CASE 4 RETURN CONST_TEE_PREVIEW_4		BREAK
		CASE 5 RETURN CONST_TEE_PREVIEW_5		BREAK
		CASE 6 RETURN CONST_TEE_PREVIEW_6		BREAK
		CASE 7 RETURN CONST_TEE_PREVIEW_7		BREAK
		CASE 8 RETURN CONST_TEE_PREVIEW_8		BREAK
	ENDSWITCH
	RETURN CONST_TEE_PREVIEW_0 
ENDFUNC

//VECTOR vTempHoldingVector
//Camera when holding the "look at green" button
PROC GOLF_SETUP_LOOK_AT_GREEN_CAMERA(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_HELPERS &thisHelpers)
//
//	VECTOR pinPos = GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
//	FLOAT fHeading = GET_HEADING_BETWEEN_VECTORS(pinPos, GET_GOLF_HOLE_TEE_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
//	VECTOR camPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(pinPos, fHeading+90, <<10.0, 0, 10.0>>)
//	
//	IF NOT DOES_CAM_EXIST(thisHelpers.camLookAtGreen)
//		thisHelpers.camLookAtGreen = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, camPos, <<0,0,0>>, 65, TRUE)
//		POINT_CAM_AT_COORD(thisHelpers.camLookAtGreen , pinPos)
//		SET_CAM_ACTIVE_WITH_INTERP(thisHelpers.camLookAtGreen, GET_RENDERING_CAM(),  DEFAULT_INTERP_TO_FROM_GAME)
//	ENDIF
	FLOAT fCameraStartPhase, fLengthOfPan
	INT iHole = GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)
	VECTOR vCameraPanStart, vCameraPanEnd
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fCamDist
	VECTOR vCurCamPos
	
	UNUSED_PARAMETER(fCamDist)
	//Care about 2D distance
	vPlayerPos.z = 0.0
	GET_GOLF_HOLE_CAMERA_START_AND_END(GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), vCameraPanStart, vCameraPanEnd)
	
	IF DOES_CAM_EXIST(thisHelpers.camAddress) AND NOT DOES_CAM_EXIST(thisHelpers.camLookAtGreen)
	AND HAS_ANIM_DICT_LOADED(GET_HOLE_PREVIEW_ANIM_DICT())
	
		thisHelpers.camLookAtGreen =  CREATE_CAMERA(CAMTYPE_ANIMATED)
		PLAY_CAM_ANIM(thisHelpers.camLookAtGreen , GET_GOLF_GREEN_PREVIEW_CAM_ANIM_NAME(iHole), GET_HOLE_PREVIEW_ANIM_DICT(),<<-1317.170, 60.494, 53.560>>, <<0,0,0>>)
		
		IF IS_TEE_SHOT(thisCourse, thisFoursome)
			fCameraStartPhase = 0.0
			SET_CAM_ACTIVE_WITH_INTERP(thisHelpers.camLookAtGreen, thisHelpers.camAddress, GET_TEE_PREVIEW_INTERP_TIME_FOR_HOLE(iHole), INT_TO_ENUM(CAMERA_GRAPH_TYPE, CONST_GRAPH_TYPE), INT_TO_ENUM(CAMERA_GRAPH_TYPE, CONST_GRAPH_TYPE))
		ELSE
			fLengthOfPan = VDIST(vCameraPanStart, vCameraPanEnd)
			
			fCameraStartPhase = (fLengthOfPan - VDIST(vPlayerPos, vCameraPanEnd))/fLengthOfPan
			
			IF fCameraStartPhase < 0.0
				fCameraStartPhase = 0.0
			ELIF fCameraStartPhase > 1.0 //don't think this is even possible, but just incase
				fCameraStartPhase = 1.0
			ENDIF
	//		fCameraStartPhase = 0.0
			CDEBUG1LN(DEBUG_GOLF,"Starting green camera at phase, ", fCameraStartPhase, " length of pan ", fLengthOfPan)
			
			SET_CAM_ANIM_CURRENT_PHASE(thisHelpers.camLookAtGreen, fCameraStartPhase)
			thisHelpers.fHoleSplineStartPhase = fCameraStartPhase
			
			vCurCamPos =  GET_CAM_COORD(thisHelpers.camAddress)
			VECTOR vNewCamPos =  GET_CAM_COORD(thisHelpers.camLookAtGreen)
			fCamDist = VDIST(vCurCamPos, vNewCamPos)
			CDEBUG1LN(DEBUG_GOLF,"Trying to figure out how to get from ", vCurCamPos, " to ", vNewCamPos, " smoothly over dist = ", fCamDist)
			//7666 - hole 6
			//9000 - hole 9
			SET_CAM_ACTIVE_WITH_INTERP(thisHelpers.camLookAtGreen, thisHelpers.camAddress, GET_TEE_PREVIEW_INTERP_TIME_FOR_HOLE(iHole), INT_TO_ENUM(CAMERA_GRAPH_TYPE, CONST_GRAPH_TYPE), INT_TO_ENUM(CAMERA_GRAPH_TYPE, CONST_GRAPH_TYPE))
//		SET_CAM_ACTIVE(thisHelpers.camLookAtGreen, TRUE)
		ENDIF
	ENDIF
//	
//	REPEAT GET_NUM_OF_GREEN_SPLINE_CAMERAS_FOR_HOLE(iHole) iCamIndex
//		GET_GOLF_PREVIEW_GREEN_CAMERA_SPLINE_AT_HOLE_AND_INDEX(iHole, iCamIndex, vCamPosition, vCamRot)
//
//		DRAW_DEBUG_SPHERE(vCamPosition, 2)
//		DRAW_DEBUG_LINE(vCamPosition, vCamPosition + 3.0*NORMALISE_VECTOR(vCamRot))
//	ENDREPEAT
	
//	vCurCamPos = GET_FINAL_RENDERED_CAM_COORD()
//	fCamDist = VDIST(vCurCamPos, vTempHoldingVector)
//	CDEBUG1LN(DEBUG_GOLF,"Preview cam velocity = ", fCamDist)
//	vTempHoldingVector = vCurCamPos
	
	
	IF DOES_CAM_EXIST(thisHelpers.camLookAtGreen)
		SET_FOCUS_POS_AND_VEL(GET_CAM_COORD(thisHelpers.camLookAtGreen), <<0,0,0>>)
	ENDIF

ENDPROC

/// PURPOSE:
///    Gets the camera offsets for a good green camera
/// PARAMS:
///    thisPlayerCore - 
///    fAimHeading - 
/// RETURNS:
///    
FUNC VECTOR GET_GREEN_CAMERA_OFFSETS(GOLF_PLAYER_CORE &thisPlayerCore, FLOAT &fAimHeading)
	IF GET_GOLF_PLAYER_DISTANCE_TO_HOLE(thisPlayerCore) > 25.0
		fAimHeading = GET_GOLF_PLAYER_AIM(thisPlayerCore) + GET_RANDOM_FLOAT_IN_RANGE(-30, 30)
		RETURN <<-5.0,1.0,1.25>>
	ELIF GET_GOLF_PLAYER_DISTANCE_TO_HOLE(thisPlayerCore) > 10.0
		fAimHeading = GET_GOLF_PLAYER_AIM(thisPlayerCore) + GET_RANDOM_FLOAT_IN_RANGE(-20, 20)
		RETURN <<-2.5,0.5,0.75>>
	ENDIF
	
	fAimHeading = GET_GOLF_PLAYER_AIM(thisPlayerCore)
	RETURN <<-1.5,0.25,0.25>> // Hole Camera
ENDFUNC

/// PURPOSE:
///    Creates the green camera for a given shot
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
///    thisHelpers - 
PROC CREATE_GREEN_CAMERA(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	VECTOR camPosition
	FLOAT camHeading
	VECTOR camOffset = GET_GREEN_CAMERA_OFFSETS(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], camHeading)
	camPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) + <<0,0,0.13>>, camHeading, camOffset)
//	PRINTSTRING("Setting new cam pos: ")PRINTVECTOR(camPosition)PRINTNL()
	IF NOT DOES_CAM_EXIST(thisHelpers.camFlight)
		thisHelpers.camFlight = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, camPosition, <<0,0,0>>, 40, TRUE)
	ELSE
		SET_CAM_COORD(	thisHelpers.camFlight, camPosition)
	ENDIF
	POINT_CAM_AT_COORD(thisHelpers.camFlight,GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))+ <<0,0,camOffset.z/2>> + <<0,0,0.410>>)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
ENDPROC


/// PURPOSE:
///    Manages the green camera for a golf putt or chip
/// PARAMS:
///    thisHelpers - 
///    thisCourse - 
///    thisFoursome - 
PROC GOLF_MANAGE_GREEN_CAMERA(GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers,GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)
	IF NOT DOES_CAM_EXIST(thisHelpers.camFlight)
		IF bDebugSpew DEBUG_MESSAGE("GMS_BALL_IN_FLIGHT - in flight, tracking ball") ENDIF
		CREATE_GREEN_CAMERA( thisCourse, thisFoursome, thisHelpers)
		
		SET_CAM_FOV(thisHelpers.camFlight, 45.0)
		SHAKE_CAM(thisHelpers.camFlight, "HAND_SHAKE", 0.5)
		
		CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
		CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE(TRUE)
		SET_GOLF_CONTROL_FLAG(thisGame, GCF_USING_CUTSCENE_SHADOW)
		
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ELIF NOT IS_ENTITY_ON_SCREEN(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		
		SET_CAM_COORD(thisHelpers.camFlight,GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), GET_RANDOM_FLOAT_IN_RANGE(0, 360), <<6.5, -1.0, 2.5>>))
		POINT_CAM_AT_ENTITY(thisHelpers.camFlight, GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), <<0.0, 0.0, 0.0>>, TRUE)
	ENDIF
ENDPROC

FUNC BOOL GET_SAFE_CAMERA_POSITION(VECTOR &vRetPos, VECTOR vStartPos, FLOAT fCameraAngle, VECTOR vCamOffset, VECTOR vCollisionOfffset, BOOL bCheckMagnitudeIsToClose)
	FLOAT fMagSafe, fMagDefault
	VECTOR vSafeCamPos
	
	IF NOT WOULD_CAMERA_AT_POSITION_BE_OCCLUDED(vStartPos, fCameraAngle, vCamOffset + vCollisionOfffset, vSafeCamPos)
		vRetPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fCameraAngle, vCamOffset)
		RETURN TRUE
	ELSE
		fMagSafe = VMAG(vStartPos - vSafeCamPos)
		fMagSafe *= 0.8 //move up the camera a little bit from the intersection point
		
		IF fMagSafe > 1.0 OR NOT bCheckMagnitudeIsToClose
			fMagDefault = VMAG(vCamOffset)
			vRetPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fCameraAngle, <<(fMagSafe/fMagDefault) * vCamOffset.x, (fMagSafe/fMagDefault) * vCamOffset.y, vCamOffset.z>>)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC VECTOR FIND_ACCEPTABLE_ADDRESS_CAMERA_POSITION_AND_FOV(GOLF_PLAYER_CORE &thisPlayerCore, FLOAT fAimHeading, FLOAT &fFOV, GOLF_HELPERS &thisHelpers, BOOL bUseSavedBallPos = FALSE) 
	VECTOR vBallPosition
	VECTOR vDefaultOffset = <<3.0,0,1.25>>
	VECTOR vRetPosition
	
	IF DOES_ENTITY_EXIST(GET_GOLF_PLAYER_BALL(thisPlayerCore)) AND NOT bUseSavedBallPos
		vBallPosition = GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore))
	ELSE
		vBallPosition = GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore)
	ENDIF
	
	IF GET_SAFE_CAMERA_POSITION(vRetPosition, vBallPosition, fAimHeading, vDefaultOffset, <<0,0,0.5>>, TRUE)
		vRetPosition = vRetPosition
	ELIF VDIST2(thisHelpers.vLastGoodAddressCamPos, <<0,0,0>>) != 0
		CDEBUG1LN(DEBUG_GOLF,"Use previous pos")
		vRetPosition = thisHelpers.vLastGoodAddressCamPos
	ELSE
		vRetPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBallPosition,fAimHeading, vDefaultOffset)
	ENDIF
	
	fFOV = 60
	thisHelpers.vLastGoodAddressCamPos = vRetPosition
	RETURN vRetPosition
ENDFUNC

PROC GOLF_MANAGE_REMOTE_PLAYER_ADDRESS_CAM(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_HELPERS &thisHelpers, FLOAT fAimHeading, INT iHole)
	FLOAT fFov
	VECTOR camPosition
	FLOAT fHeadingToPin
				
//	//when you first make the camera it is going to be address cam
	IF NOT DOES_CAM_EXIST(thisHelpers.camAddress)
		thisHelpers.spectatorCamState = GOLF_SPECTATOR_ADDRESS_CAM
	ENDIF
	
	SWITCH thisHelpers.spectatorCamState
		CASE GOLF_SPECTATOR_ADDRESS_CAM
			IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED)
				DISPLAY_RADAR(TRUE)
				SET_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_HOLE)
			ENDIF
			
			camPosition = FIND_ACCEPTABLE_ADDRESS_CAMERA_POSITION_AND_FOV(thisPlayerCore, fAimHeading, fFov, thisHelpers, FALSE) 
			
			IF NOT DOES_CAM_EXIST(thisHelpers.camAddress)
				thisHelpers.camAddress = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, camPosition, <<0,0,0>>, 60, TRUE)
				thisHelpers.vAdressCamStartPos = camPosition
			ELSE
				BOOL bForceCamUpdate
				VECTOR vBallPos
				vBallPos = GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore)
				bForceCamUpdate = FALSE
				
				IF VDIST(thisHelpers.vAdressCamStartPos, vBallPos) > 5.0
					CDEBUG1LN(DEBUG_GOLF,"Recalculate vAdressCamStartPos ", vBallPos)
					//dont use passed in heading, it might have changed from the default
					fHeadingToPin = GET_HEADING_BETWEEN_VECTORS(GET_GOLF_HOLE_PIN_POSITION_STATIC(iHole), vBallPos) + 90
					thisHelpers.vAdressCamStartPos = FIND_ACCEPTABLE_ADDRESS_CAMERA_POSITION_AND_FOV(thisPlayerCore, fHeadingToPin, fFov, thisHelpers, FALSE)
					bForceCamUpdate = TRUE
				ENDIF
				
				IF thisHelpers.bJustSwitchedSpectatorCam OR bForceCamUpdate
					SET_CAM_COORD(thisHelpers.camAddress, thisHelpers.vAdressCamStartPos)
				ENDIF
			ENDIF
			
			SET_CAM_FOV(thisHelpers.camAddress, fFov)
			POINT_CAM_AT_COORD(thisHelpers.camAddress,GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore) + <<0.0, 0.0, 1.0>> )		
		BREAK
		CASE GOLF_SPECTATOR_CINEMATIC_CAM
			IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED)
				DISPLAY_RADAR(TRUE)
				SET_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_HOLE)
			ENDIF
			
			fHeadingToPin = GET_HEADING_BETWEEN_VECTORS(GET_GOLF_HOLE_PIN_POSITION_STATIC(iHole), GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore)) - 90
			
			GET_SAFE_CAMERA_POSITION(camPosition, GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore), fHeadingToPin, <<-4.0, -0.8, 1.4>>, <<0,0,0>>, FALSE)
			
			SET_CAM_COORD(thisHelpers.camAddress, camPosition)
			SET_CAM_FOV(thisHelpers.camAddress, 60.0)
			POINT_CAM_AT_COORD(thisHelpers.camAddress, GET_GOLF_HOLE_PIN_POSITION_STATIC(iHole))
		BREAK
		CASE GOLF_SPECTATOR_POV_CAM
			FLOAT fHeadingToGolfer
			DISPLAY_RADAR(FALSE)
			CLEAR_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_HOLE)
			
			fHeadingToGolfer = GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore) )
			camPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), fHeadingToGolfer, << 0.73, -1.6, 0.85>>)
			IF NOT DOES_CAM_EXIST(thisHelpers.camAddress)
				thisHelpers.camAddress = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, camPosition, <<0,0,0>>, 60, TRUE)
			ELSE
				SET_CAM_COORD(thisHelpers.camAddress, camPosition)
			ENDIF
			
			SET_CAM_ACTIVE(thisHelpers.camAddress, TRUE)
			SET_CAM_FOV(thisHelpers.camAddress, 35)
			POINT_CAM_AT_COORD(thisHelpers.camAddress, GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore) + <<0.0, 0.0, 0.5>> )
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid spectator camera state")
		BREAK
		
	ENDSWITCH

	thisHelpers.bJustSwitchedSpectatorCam = FALSE
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
ENDPROC

/// PURPOSE:
///    Manages the adress camera (when the player is preparing to hit a shot)
/// PARAMS:
///    thisPlayerCore - 
///    thisHelpers - 
///    fAimHeading - 
PROC ADJUST_ADDRESS_CAMERA(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER_CORE &thisPlayerCore, GOLF_HELPERS &thisHelpers, FLOAT fAimHeading)
	FLOAT fFov

	#IF GOLF_IS_MP
		PED_INDEX tempPed
		IF GET_GOLF_PLAYER_CONTROL(thisPlayerCore) = HUMAN_NETWORK
			IF NOT NETWORK_IS_IN_SPECTATOR_MODE()
			OR thisHelpers.pedSpectating != GET_GOLF_PLAYER_PED(thisPlayerCore)
				thisHelpers.pedSpectating = GET_GOLF_PLAYER_PED(thisPlayerCore)
				SET_IN_SPECTATOR_MODE(TRUE, GET_GOLF_PLAYER_PED(thisPlayerCore))
			ENDIF
		ELSE
			IF NETWORK_IS_IN_SPECTATOR_MODE()
				SET_IN_SPECTATOR_MODE(FALSE, tempPed)
			ENDIF
		ENDIF
	#ENDIF
	
	IF GET_GOLF_PLAYER_CONTROL(thisPlayerCore) = HUMAN_NETWORK
		GOLF_MANAGE_REMOTE_PLAYER_ADDRESS_CAM(thisPlayerCore, thisHelpers, fAimHeading, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
	ELSE
		VECTOR camPosition = FIND_ACCEPTABLE_ADDRESS_CAMERA_POSITION_AND_FOV(thisPlayerCore, fAimHeading, fFov, thisHelpers, FALSE) 
		
		IF NOT DOES_CAM_EXIST(thisHelpers.camAddress)
			thisHelpers.camAddress = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, camPosition, <<0,0,0>>, 30, TRUE)
		ELSE
			SET_CAM_COORD(	thisHelpers.camAddress, camPosition)
		ENDIF
		
		SET_CAM_FOV(thisHelpers.camAddress, fFov)
		POINT_CAM_AT_COORD(thisHelpers.camAddress,GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore) + <<0.0, 0.0, 1.0>> )

		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ENDIF

ENDPROC

PROC CREATE_GOLF_SPLINE_CAM(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_HELPERS &thisHelpers, CAMERA_INDEX &splineCam, FLOAT fAimHeading)
	VECTOR camPosition, camOffset
	VECTOR shotEstimate
	INT nextNode, loopInc
	VECTOR vRot
	INT splineTime
	FLOAT distanceCamTravels, distanceRatio, fFov
	VECTOR cameraYPosOffset = <<0, 1.5, 0>>
	
	//smothly transistion from offset to nig offset
	FLOAT fLateralOffset = 1.0 + ABSF(ABSF(thisHelpers.fCosShotAngleDevation) - 1.0)*25.0
	cameraYPosOffset = <<0, 1.5, 0>> * fLateralOffset

	
	distanceCamTravels = (MAGIC_DISTANCE_CONVERSION * METERS_TO_YARDS(VDIST2(GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore), GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(thisHelpers.golfTrail.numControlPoints-1))))
	distanceRatio = distanceCamTravels/(50*50)
	
	IF distanceCamTravels < 50*50
		splineTime = 100
	ELSE
		splineTime = FLOOR(100.0 * PICK_FLOAT(disTanceRatio < 2.0, distanceRatio, 2.0))
	ENDIF
	
	WHILE loopInc < thisHelpers.golfTrail.numControlPoints 
		
		//get next node index based on direction
		nextNode = loopInc //PICK_INT(bBackwards, (thisHelpers.golfTrail.numControlPoints-1)- loopInc, loopInc)
		
		//dont follow the trail anymore after you get past the top of the arc
		IF nextNode > thisHelpers.golfTrail.numControlPoints/2
			nextNode = thisHelpers.golfTrail.numControlPoints-1
			loopInc = PICK_INT(FALSE, (thisHelpers.golfTrail.numControlPoints/2)-2, thisHelpers.golfTrail.numControlPoints)
		ELIF nextNode = 1
			nextNode = PICK_INT(FALSE, 0, 2)
			loopInc = PICK_INT(FALSE, thisHelpers.golfTrail.numControlPoints-1, 2)
		ENDIF
		
		shotEstimate =  GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(nextNode)
		
		IF nextNode < (thisHelpers.golfTrail.numControlPoints-1)
			//data used while moving across the trail
			camPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(shotEstimate, fAimHeading, <<3.5,0.0,1.25>> + (cameraYPosOffset * 0.33))
			camOffset = <<0.0, 0.0, 0.75>>
		ELSE
			//data used for ending point			
			IF distanceCamTravels < 50*50
				//smallest it will ever be
				camPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(shotEstimate,fAimHeading, <<6.0,0.0,3.5>> + cameraYPosOffset)
				camOffset = <<0.0, 0.0, 0.75>>
			ELSE
				IF disTanceRatio < 3.0
					//in between smallest and largest
					camPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(shotEstimate,fAimHeading, (<<6.0,0.0, 3.5>> * distanceRatio) + cameraYPosOffset)
					camOffset = <<0.0, 0.0, 0.75>> * distanceRatio
				ELSE
					//largest it will ever be
					camPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(shotEstimate,fAimHeading, (<<6.0,0.0,3.5>> * 3.0) + cameraYPosOffset)
					camOffset = <<0.0, 0.0, 0.75>> * 3.0
				ENDIF
			ENDIF
		ENDIF
		IF NOT DOES_CAM_EXIST(splineCam)
			splineCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SPLINE_DEFAULT, camPosition, <<0,0,0>>, 60, TRUE)
			SET_CAM_SPLINE_DURATION(splineCam , splineTime*6 + 500)
			//SET_CAM_SPLINE_SMOOTHING_STYLE(splineCam, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
			IF DOES_CAM_EXIST(thisHelpers.camPreview)
				DESTROY_CAM(thisHelpers.camPreview)
			ENDIF
			
			thisHelpers.camPreview = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, camPosition, <<0,0,0>>, 60, TRUE)
			
				//create copy of address cam first if traveling forward
				camPosition = FIND_ACCEPTABLE_ADDRESS_CAMERA_POSITION_AND_FOV(thisPlayerCore, fAimHeading, fFov, thisHelpers) 
				SET_CAM_COORD(	thisHelpers.camPreview, camPosition)
				SET_ROT_FOR_CAM_TO_POINT_AT_COORD(camPosition, (GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore) + <<0.0, 0.0, 1.0>>), vRot)
				SET_CAM_ROT(thisHelpers.camPreview, vRot)
				ADD_CAM_SPLINE_NODE_USING_CAMERA_FRAME(splineCam,thisHelpers.camPreview, splineTime)
		ENDIF
		
		IF nextNode != 0
			SET_CAM_COORD(thisHelpers.camPreview, camPosition)
			SET_ROT_FOR_CAM_TO_POINT_AT_COORD(camPosition,  (shotEstimate+camOffset), vRot)
			//POINT_CAM_AT_COORD(thisHelpers.camPreview,shotEstimate)
			
			IF nextNode = thisHelpers.golfTrail.numControlPoints-1
				splineTime += 500
			ELSE
				vRot.x = 0
			ENDIF
			SET_CAM_ROT(thisHelpers.camPreview, vRot)
						
			//CDEBUG1LN(DEBUG_GOLF,nextNode, " Pos ", camPosition, " Rot ", vRot, " DirVector ", dirVector, " ShotEst ",  shotEstimate)
			ADD_CAM_SPLINE_NODE_USING_CAMERA_FRAME(splineCam,thisHelpers.camPreview, splineTime)
			IF nextNode = thisHelpers.golfTrail.numControlPoints-1
				splineTime -= 500
			ENDIF
		ENDIF
		
		loopInc++
	ENDWHILE
	
ENDPROC

PROC TRANSITION_PREVIEW_CAM( GOLF_HELPERS &thisHelpers)

	IF DOES_CAM_EXIST(thisHelpers.camPreview) AND IS_CAM_RENDERING(thisHelpers.camPreview)
	AND VDIST2(thisHelpers.vPreviewCamFinalPos, <<0,0,0>>) != 0

		VECTOR vCamCurrentPos = GET_CAM_COORD(thisHelpers.camPreview)
		FLOAT fDistToNewPos = VDIST2(vCamCurrentPos, thisHelpers.vPreviewCamFinalPos) 
		
		IF fDistToNewPos > 4.0
			VECTOR vDirection = NORMALISE_VECTOR(thisHelpers.vPreviewCamFinalPos - vCamCurrentPos)
			SET_CAM_COORD(thisHelpers.camPreview, vCamCurrentPos + vDirection * PICK_FLOAT(1.5*(fDistToNewPos/30) > 1.5, 1.5, 1.5*(fDistToNewPos/30)) )
		ELSE
			SET_CAM_COORD(thisHelpers.camPreview, thisHelpers.vPreviewCamFinalPos)
		ENDIF
	
	ENDIF

ENDPROC

/// PURPOSE:
///    Manages the preview camera (zoomed in on shot marker) for a shot
/// PARAMS:
///    thisPlayerCore - 
///    thisHelpers - 
///    fAimHeading - 
PROC ADJUST_PREVIEW_CAMERA(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_HELPERS &thisHelpers, FLOAT fAimHeading, VECTOR vHolePos, BOOL bContinuous = FALSE, BOOL bSnap = FALSE)
	VECTOR camPosition, camOffset
	VECTOR shotEstimate = GET_GOLF_PLAYER_SHOT_ESTIMATE(thisPlayerCore)
	VECTOR playerPos = GET_ENTITY_COORDS((GET_GOLF_PLAYER_PED(thisPlayerCore)))
	FLOAT distanceRatio
	VECTOR cameraPosOffset = <<6.0,0.0,3.5>>
	VECTOR cameraYPosOffset = <<0, 1.5, 0>> 
	VECTOR cameraMove, cameraRot

	//smothly transistion from offset to nig offset
	FLOAT fLateralOffset = 1.0 + ABSF(ABSF(thisHelpers.fCosShotAngleDevation) - 1.0)*25.0
	//CDEBUG1LN(DEBUG_GOLF,"using lateral offset ", 1.5*fLateralOffset)
	cameraYPosOffset = <<0, 1.5, 0>> * fLateralOffset

	
	IF NOT IS_GOLF_PLAYER_PUTTING(thisPlayerCore) 
		IF bContinuous
			CREATE_GOLF_SPLINE_CAM(thisPlayerCore, thisHelpers, thisHelpers.camPreviewSplineForward, fAimHeading)
		ELSE
			shotEstimate = GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(thisHelpers.golfTrail.numControlPoints-1)

			IF (MAGIC_DISTANCE_CONVERSION * METERS_TO_YARDS(VDIST2(GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore), shotEstimate))) < 50*50
				camPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(shotEstimate,fAimHeading, cameraPosOffset+cameraYPosOffset)
				camOffset = <<0.0, 0.0, 0.75>>
			ELSE
				distanceRatio = (MAGIC_DISTANCE_CONVERSION * METERS_TO_YARDS(VDIST2(GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore), shotEstimate)))/(50*50)
				IF disTanceRatio < 3.0
					camPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(shotEstimate,fAimHeading, (cameraPosOffset * distanceRatio) +cameraYPosOffset)
					camOffset = <<0.0, 0.0, 0.75>> * distanceRatio
				ELSE
					camPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(shotEstimate,fAimHeading, (cameraPosOffset* 3.0) + cameraYPosOffset)
					camOffset = <<0.0, 0.0, 0.75>> * 3.0
				ENDIF
			ENDIF
		ENDIF
	ELSE
		distanceRatio = VDIST2(shotEstimate, playerPos)/YARDS_TO_METERS(5*5)
		
		IF distanceRatio < 1.0 //if the shot estimate is less then 15 meters from the player
			cameraPosOffset.x -= (1-distanceRatio)*6	//smoothly move the camera pos up so the player doesn't get in the way
		ENDIF
		
		IF VDIST2(shotEstimate, vHolePos) < (0.15*0.15)
			shotEstimate.z = vHolePos.z + 0.0875
		ENDIF
		
		camPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(shotEstimate, fAimHeading, cameraPosOffset)
		camOffset = <<0.0, 0.0, 0.75>>
	ENDIF
	
	IF NOT bContinuous OR IS_GOLF_PLAYER_PUTTING(thisPlayerCore) 
		IF NOT DOES_CAM_EXIST(thisHelpers.camPreview)
			thisHelpers.camPreview = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, camPosition, <<0,0,0>>, 60, TRUE)
			SET_CAM_COORD(thisHelpers.camPreview, camPosition)
			POINT_CAM_AT_COORD(thisHelpers.camPreview,shotEstimate + camOffset)
		ELSE
			IF IS_GOLF_PLAYER_PUTTING(thisPlayerCore) OR bSnap
				SET_CAM_COORD(thisHelpers.camPreview, camPosition)
				POINT_CAM_AT_COORD(thisHelpers.camPreview,shotEstimate + camOffset)
				SET_CAM_ACTIVE(thisHelpers.camPreview, TRUE)
			ELSE
				cameraMove = PREVIEWCAM_LERP * (camPosition - GET_CAM_COORD(thisHelpers.camPreview))
				IF VMAG2(cameraMove) < 1000000.0
	//				CERRORLN(DEBUG_MISSION, "Moving golf preview cam by ", cameraMove)
	//				CERRORLN(DEBUG_MISSION, "New cam pos: ", GET_CAM_COORD(thisHelpers.camPreview) + cameraMove)
					STOP_CAM_POINTING(thisHelpers.camPreview)
					SET_CAM_COORD(thisHelpers.camPreview, GET_CAM_COORD(thisHelpers.camPreview) + cameraMove)
					
					cameraRot = GET_CAM_ROT(thisHelpers.camPreview)
					cameraMove = shotEstimate + camOffset - camPosition
					FLOAT fVertDist = cameraMove.z
					cameraMove.z = 0.0
					FLOAT fHorzDist = VMAG(cameraMove)
					
					cameraMove = <<ATAN(fVertDist / fHorzDist), 0.0, GET_HEADING_BETWEEN_VECTORS(camPosition, shotEstimate + camOffset)>>
					cameraMove -= GET_CAM_ROT(thisHelpers.camPreview)
					cameraMove.z = WRAP(cameraMove.z, -180.0, 180.0)
					cameraMove *= PREVIEWCAM_LERP
					SET_CAM_ROT(thisHelpers.camPreview, cameraRot + cameraMove)
				ELSE
					CERRORLN(DEBUG_MINIGAME, "Huge camera change detected! Skipping preview cam update this frame...")
				ENDIF
			ENDIF
		ENDIF
		
		//thisHelpers.vPreviewCamFinalPos = camPosition //SET_CAM_COORD(	thisHelpers.camPreview, camPosition)

		SET_CAM_ACTIVE(thisHelpers.camPreview, TRUE)
	ENDIF
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	IF DOES_CAM_EXIST(thisHelpers.camFlight)
		DESTROY_CAM(thisHelpers.camFlight)
	ENDIF
ENDPROC

PROC BUILD_INTRO_MENU(GOLF_HELPERS &thisHelpers)
	CLEAR_MENU_DATA()
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT, MENU_ITEM_ICON)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT, FONT_RIGHT)
	SET_MENU_ITEM_X_OFFSET(0.0, 0.0, 0.247)
	IF GET_GOLF_NUMBER_OF_UNLOCKED_BUDDIES() > 0
		SET_MENU_TITLE("FLY_PARTY")
		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_GOLF, iR, iG, iB, iA)
		SET_MENU_HEADER_COLOUR(iR, iG, iB, iA, TRUE)
		SET_GOLF_INTRO_SELECT_BUDDIES(thisHelpers)
	ELSE
		SET_MENU_TITLE("FLY_NO_BUD")
		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_GOLF, iR, iG, iB, iA)
		SET_MENU_HEADER_COLOUR(iR, iG, iB, iA, TRUE)
	ENDIF
	SET_MENU_USES_HEADER_GRAPHIC(TRUE, "ShopUI_Title_GolfShop", "ShopUI_Title_GolfShop")
	
ENDPROC

/// PURPOSE:
///    Manages the input and UI for the intro cutscene
/// PARAMS:
///    thisFoursome - 
///    thisPlayer - 
///    thisHelpers - 
/// RETURNS:
///    
FUNC BOOL MANAGE_FLYOVER_UI_INPUT(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	IF NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
		RETURN FALSE
	ENDIF
	IF NOT DOES_CAM_EXIST(GET_RENDERING_CAM())
		RETURN FALSE
	ENDIF
	
	INT check, currentSelection

	INT iLeftX, iLeftY, iRightX, iRightY
	BOOL bSelectPartner = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	BOOL bStartGame = FALSE //IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RLEFT)
	BOOL bDUPress = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) 
	BOOL bDDPress = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	
	BOOL bRDownPressed = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
	GET_GOLF_ANALOG_STICK_VALUES(iLeftX, iLeftY, iRightX, iRightY)
	
	// PC MOUSE SUPPORT
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		// Zero analogue inputs as they can interfere with the M&KB menu control
		// due to the custom control scheme loaded for PC.
		iLeftX = 0
		iLeftY = 0
		iRightX = 0
		iRightY = 0
	
		// Only want this to happen in the first part of the intro.
		IF thisHelpers.introState = GIS_INTRO_FLYOVER_CAMERA1
	
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	// B*2239651 - Disabling pause_alternate during end screens (to avoid clash when exiting leaderboards)

			INT iNumBuddies = GET_GOLF_NUMBER_OF_UNLOCKED_BUDDIES()
		
			HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
			HANDLE_MENU_CURSOR(FALSE)
			
//			// Custom cursor handling here for confirming a game.
//			IF g_iMenuCursorItem = 0 AND iNumBuddies = 0
//				SET_MOUSE_CURSOR_STYLE( MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER )
//			ENDIF
//			
//			IF g_iMenuCursorItem = g_sMenuData.iCurrentItem
//				IF g_sMenuData.iCurrentItem = iNumBuddies
//					SET_MOUSE_CURSOR_STYLE( MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER )
//				ENDIF
//			ENDIF
				
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
			
				// No buddies, just start game.
				IF iNumBuddies = 0
							
					thisFoursome.iNumPlayers = 0
					bSelectPartner = TRUE
				
				ELIF g_iMenuCursorItem = g_sMenuData.iCurrentItem

					// Start selected
					IF g_sMenuData.iCurrentItem = iNumBuddies
						bStartGame = TRUE
					ELSE
						// Toggle buddy on/off
						thisFoursome.iNumPlayers = g_sMenuData.iCurrentItem
						SET_CURRENT_MENU_ITEM(thisFoursome.iNumPlayers)
						bSelectPartner = TRUE
					ENDIF
				
				
				ELSE
					// Select new item
					currentSelection = g_iMenuCursorItem
					SET_CURRENT_MENU_ITEM(currentSelection)
					PLAY_SOUND(-1,"NAV_UP_DOWN", GET_GOLF_HUD_MINIGAME_SOUNDSET())
				ENDIF
	
			// Mousewheel scroll down
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				bDDPress = TRUE
			
			// Mousewheel scroll up
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
				bDUpress = TRUE
			ENDIF
			
		ENDIF

	ENDIF

	IF NOT IS_TIMER_STARTED(thisHelpers.inputTimer)
		START_TIMER_NOW(thisHelpers.inputTimer)
	ENDIF
	
	IF NOT GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers)
		IF NOT bSelectPartner AND NOT bDUPress  AND NOT bDDPress
			SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, TRUE)
		ENDIF
	ENDIF
	GOLF_BUDDIES currentBuddy
	TEXT_LABEL_15 sMenuItemDescription
	
	//SCALEFORM_RETURN_INDEX scaleformRetIndex
	SWITCH thisHelpers.introState
		CASE GIS_INTRO_FLYOVER_INIT
			IF GET_GOLF_NUMBER_OF_UNLOCKED_BUDDIES() != 0 AND NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_IS_FRIEND_ACTIVITY)
				thisFoursome.iNumPlayers = 0
				BUILD_INTRO_MENU(thisHelpers)
				SET_CURRENT_MENU_ITEM(0)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("GOLF_INTRO_HELP", 1000)
				
				SET_CURSOR_POSITION_FOR_MENU()				
				GOLF_SETUP_INTRO_KEYS(thisHelpers)
			ELIF NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_IS_FRIEND_ACTIVITY)
				thisFoursome.iNumPlayers = 0
				BUILD_INTRO_MENU(thisHelpers)
				SET_CURRENT_MENU_ITEM(-1)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("NO_BUDDY", 1000)
				
				SET_CURSOR_POSITION_FOR_MENU()				
				GOLF_SETUP_INTRO_KEYS(thisHelpers)
			ELSE
				CLEAR_MENU_DATA()
			ENDIF
			RETURN TRUE
		BREAK
		CASE GIS_INTRO_FLYOVER_CAMERA1
			IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_IS_FRIEND_ACTIVITY)
				IF NOT IS_TIMER_STARTED(thisHelpers.cameraTimer)
					START_TIMER_NOW_SAFE(thisHelpers.cameraTimer)
				ELIF TIMER_DO_WHEN_READY(thisHelpers.cameraTimer, 2.0)
					GOLF_PRINT_HELP("FLY_INTRO1", TRUE)
					SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
					IF DOES_ENTITY_EXIST(FRIEND_B_PED_ID())
						SET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome, 3)
					ELSE
						SET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome, 2)
					ENDIF
					RETURN TRUE
				ENDIF
			ELIF GET_GOLF_NUMBER_OF_UNLOCKED_BUDDIES() = 0
				
				IF bSelectPartner
					SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_MINI_GAME_SOUNDSET")
					//This is the actual place were the number of golfer is set
					SET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome, 1)
					CANCEL_TIMER(thisHelpers.inputTimer)
					RETURN TRUE
				ENDIF
				
				SET_CURRENT_MENU_ITEM_DESCRIPTION("NO_BUDDY", 1000)
				DRAW_MENU(TRUE, -1, TRUE)
				GOLF_DISPLAY_CONTROLS(thisHelpers, TRUE)
			
			ELSE
				GET_CURRENT_MENU_ITEM_DESCRIPTION(sMenuItemDescription)
				IF bDDPress OR (iLeftY > 64 AND TIMER_DO_WHEN_READY(thisHelpers.inputTimer, 0.25))
					RESTART_TIMER_NOW(thisHelpers.inputTimer)
					PLAY_SOUND(-1,"NAV_UP_DOWN", GET_GOLF_HUD_MINIGAME_SOUNDSET())
					IF thisFoursome.iNumPlayers < (GET_GOLF_NUMBER_OF_UNLOCKED_BUDDIES())
						thisFoursome.iNumPlayers++ //num player is used to keep track of what buddy I am currently selecting, it is not representing the actual number of players yet
					ELIF thisFoursome.iNumPlayers = (GET_GOLF_NUMBER_OF_UNLOCKED_BUDDIES())
						thisFoursome.iNumPlayers = 0
					ENDIF
					
					SET_CURRENT_MENU_ITEM(thisFoursome.iNumPlayers)
				ELIF bDUPress OR (iLeftY < -64 AND TIMER_DO_WHEN_READY(thisHelpers.inputTimer, 0.25))
					RESTART_TIMER_NOW(thisHelpers.inputTimer)
					PLAY_SOUND(-1,"NAV_UP_DOWN", GET_GOLF_HUD_MINIGAME_SOUNDSET())
					IF thisFoursome.iNumPlayers > 0
						thisFoursome.iNumPlayers-- //num player is used to keep track of what buddy I am currently selecting, it is not representing the actual number of players yet
					ELIF thisFoursome.iNumPlayers = 0
						thisFoursome.iNumPlayers = (GET_GOLF_NUMBER_OF_UNLOCKED_BUDDIES())
					ENDIF
					
					SET_CURRENT_MENU_ITEM(thisFoursome.iNumPlayers)
				ELIF ABSI(iLeftY) < 64
					RESTART_TIMER_AT(thisHelpers.inputTimer, 1.1)
				ENDIF

				currentSelection = thisFoursome.iNumPlayers
				
				IF bSelectPartner
					SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
					
					IF currentSelection = GET_GOLF_NUMBER_OF_UNLOCKED_BUDDIES()
						bStartGame = TRUE
					ELSE
						currentBuddy =  GET_GOLF_INDEXED_UNLOCKED_BUDDY(currentSelection)
						
						FLIP_GOLF_BUDDY(thisHelpers, currentBuddy)
						
						IF GET_GOLF_NUMBER_OF_BUDDIES_SELECTED(thisHelpers) < 4
							
							check = PICK_INT(IS_GOLF_BUDDY_SELECTED(thisHelpers, currentBuddy), 1, 0)
							
							//You have to rebuild the entire fucking menu to change one thing :(
							BUILD_INTRO_MENU(thisHelpers)
							SET_CURRENT_MENU_ITEM(currentSelection)
							IF check != 0
								PLAY_SOUND(-1,"SELECT", GET_GOLF_HUD_MINIGAME_SOUNDSET())
							ELSE
								PLAY_SOUND(-1,"CANCEL", GET_GOLF_HUD_MINIGAME_SOUNDSET())
							ENDIF
							SET_CURRENT_MENU_ITEM_DESCRIPTION("GOLF_INTRO_HELP", 1000)
							GET_CURRENT_MENU_ITEM_DESCRIPTION(sMenuItemDescription)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("FLY_PARTY_FULL", 1000)
							GET_CURRENT_MENU_ITEM_DESCRIPTION(sMenuItemDescription)
							PLAY_SOUND(-1,"CANCEL", GET_GOLF_HUD_MINIGAME_SOUNDSET())
							FLIP_GOLF_BUDDY(thisHelpers, currentBuddy)
						ENDIF
					ENDIF
				ENDIF
				
				IF bStartGame
					SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_MINI_GAME_SOUNDSET")
					//This is the actual place were the number of golfer is set
					SET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome, GET_GOLF_NUMBER_OF_BUDDIES_SELECTED(thisHelpers)+1)
					CANCEL_TIMER(thisHelpers.inputTimer)
					RETURN TRUE
				ENDIF
				
				//cursor over 'Tee off'
				IF currentSelection = GET_GOLF_NUMBER_OF_UNLOCKED_BUDDIES()
					SET_MENU_SELECTION_BAR_COLOUR(HUD_COLOUR_GOLF, TRUE)
				ELSE
					SET_MENU_SELECTION_BAR_COLOUR(HUD_COLOUR_GOLF, FALSE)
				ENDIF
				
				SET_CURRENT_MENU_ITEM_DESCRIPTION(sMenuItemDescription, 1000)
				DRAW_MENU(TRUE, -1, TRUE)
				GOLF_DISPLAY_CONTROLS(thisHelpers, TRUE)
			ENDIF
			
		BREAK
		CASE GIS_INTRO_FLYOVER_CAMERA2
			IF bRDownPressed AND GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers)
				SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
				RETURN TRUE
			ENDIF
		BREAK
		CASE GIS_INTRO_FLYOVER_CAMERA3
			IF bRDownPressed AND GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers)
				DEBUG_MESSAGE("Clearing help at end of intro")
				CLEAR_HELP()
				SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
				RETURN TRUE
			ENDIF
		BREAK
		CASE GIS_INTRO_FLYOVER_OVER
		CASE GIS_INTRO_FLYOVER_CLEANUP
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC


PROC GOLF_CLEAR_AREA_OF_PEDS_FOR_INTRO(GOLF_COURSE &thisCourse)
	CLEAR_AREA_OF_VEHICLES(GET_GOLF_COURSE_CENTER(thisCourse), 400)
	CLEAR_AREA_OF_PEDS(<< -1336.4012, 58.9336, 54.2456 >>, 12.0)
	CLEAR_AREA_OF_PEDS(<< -1324.3918, 59.1363, 52.5427 >>, 10.0)
	CLEAR_AREA_OF_PEDS(<< -1339.3778, 58.2594, 54.2457 >>, 10.0)
ENDPROC

/// PURPOSE:
///    Manages the cameras and flow for the intro cutscene
/// PARAMS:
///    thisFoursome - 
///    thisPlayer - 
///    thisHelpers - 
PROC GOLF_MANAGE_INTRO_FLYOVER(GOLF_GAME &thisGame, GOLF_MINIGAME_STATE golfState, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	BOOL bSkipCutscene
	
	INT iPlayer, iGolfSceneID
	
	SET_AMBIENT_PED_RANGE_MULTIPLIER_THIS_FRAME(2.0)
		
	SWITCH thisHelpers.introState
		CASE GIS_INTRO_FLYOVER_INIT
			IF bDebugSpew DEBUG_MESSAGE("GIS_INTRO_FLYOVER_INIT") ENDIF
			
			IF (NOT IS_SCREEN_FADING_IN() OR IS_SCREEN_FADED_IN()) AND LOAD_MENU_ASSETS()
				IF NOT DOES_CAM_EXIST(thisHelpers.camFlyover1)
								
					CDEBUG1LN(DEBUG_GOLF,"Setting global control flag for starting game")
					SET_GLOBAL_GOLF_CONTROL_FLAG(GGCF_START_GOLF_GAME)
				
					thisHelpers.camNavigate =  CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1411.1077, 53.3846, 71.2537>>, <<-14.4288, 0.0000, -85.0531>>, 45)
					
					thisHelpers.camFlyover1 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1389.9817, 54.0818, 91.5779>>, <<-16.3621, 0.0000, -84.5232>>, 45)
					
					thisHelpers.camFlyover2    = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1216.5541, 115.4266, 59.0888>>, <<-2.0149, -0.0000, 115.1002>>, 32.5102)
					thisHelpers.camFlyover2End = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1217.6342, 114.9239, 59.0472>>, <<-2.0149, -0.0000, 115.1002>>, 32.5102)
					
					thisHelpers.camFlyover3 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1316.9739, 41.6803, 54.3193>>, <<4.1443, -0.0000, 35.7702>>, 35.5473)
					
				ENDIF
				IF NOT IS_CAM_ACTIVE(thisHelpers.camFlyover1) 
					//SET_CAM_ACTIVE_WITH_INTERP(thisHelpers.camFlyover1, thisHelpers.camNavigate, 5000)
					SET_CAM_ACTIVE_WITH_INTERP(thisHelpers.camFlyover1, thisHelpers.camNavigate, 11 * 1000)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), FALSE)
					
					VECTOR vFrontDoor, vSign, vSignToDoor, vSignToPlayer
					vFrontDoor = <<-1366.00745, 56.48715, 53.10376>>
					vSign = <<-1376.56860, 55.78519, 53.27305>>
					vSignToDoor = vFrontDoor - vSign
					vSignToPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) - vSign
					
					//Is player closer to the door then then the sign
					IF DOT_PRODUCT_XY(vSignToDoor, vSignToPlayer) > 0.0
						//go straight to door
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vFrontDoor, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
					ELSE 
						//walk around bush waypoint recording
						TASK_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(), "golf_intro")
					ENDIF
					
					SET_PED_NON_CREATION_AREA(<<-1327.4082, 33.2401, 52.5668>>, <<-1320.6265, 56.7003, 52.5064>>)
				ENDIF
				IF MANAGE_FLYOVER_UI_INPUT(thisGame, thisFoursome, thisHelpers)
					CANCEL_TIMER(thisHelpers.cameraTimer)
					thisHelpers.steamVolNextShot = STREAMVOL_CREATE_FRUSTUM(<<-1218.0029, 115.7468, 59.0221>>, CONVERT_GOLF_ROTATION_TO_DIRECTION_VECTOR(<<-7.7232, 0.0000, 116.6793>>), 50, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)
					thisHelpers.introState = GIS_INTRO_FLYOVER_CAMERA1
				ENDIF
			ENDIF
		BREAK
		CASE GIS_INTRO_FLYOVER_CAMERA1
			IF bDebugSpew DEBUG_MESSAGE("GIS_INTRO_FLYOVER_CAMERA1") ENDIF
			IF NOT DOES_CAM_EXIST(thisHelpers.camFlyover1)
				thisHelpers.introState = GIS_INTRO_FLYOVER_CAMERA2
			ENDIF
			IF MANAGE_FLYOVER_UI_INPUT(thisGame, thisFoursome, thisHelpers) AND  golfState > GMS_INIT_STREAMING_DONE
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FLY_INTRO1")
					DEBUG_MESSAGE("FLY_INTRO1 printing")
					GOLF_PRINT_HELP("FLY_INTRO1", TRUE)
				ENDIF
				
				CANCEL_TIMER(thisHelpers.cameraTimer)
				
				thisHelpers.introState = GIS_INTRO_FLYOVER_CAMERA2
				SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
				
				
				WHILE NOT STREAMVOL_HAS_LOADED(thisHelpers.steamVolNextShot)
					CDEBUG1LN(DEBUG_GOLF,"Loading intro scene")
					WAIT(0)
				ENDWHILE
				
				WAIT(600) //wait a bit so the ambient golfer have time to react
				SET_CAM_ACTIVE_WITH_INTERP(thisHelpers.camFlyover2End, thisHelpers.camFlyover2, 7500)
			ENDIF
		BREAK
		CASE GIS_INTRO_FLYOVER_CAMERA2
			IF bDebugSpew DEBUG_MESSAGE("GIS_INTRO_FLYOVER_CAMERA2") ENDIF
			IF DOES_CAM_EXIST(thisHelpers.camFlyover2)
				IF NOT IS_TIMER_STARTED(thisHelpers.cameraTimer)
					START_TIMER_NOW_SAFE(thisHelpers.cameraTimer)
				ENDIF
				
			ELSE
				thisHelpers.introState = GIS_INTRO_FLYOVER_CAMERA3
			ENDIF
			
			CLEAR_FOCUS()
			
			bSkipCutscene = MANAGE_FLYOVER_UI_INPUT(thisGame, thisFoursome, thisHelpers)
			IF (bSkipCutscene OR TIMER_DO_ONCE_WHEN_READY(thisHelpers.cameraTimer, 7.5))
			AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID()) //make sure cloths are streamed in
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FLY_INTRO2")
					DEBUG_MESSAGE("FLY_INTRO2 printing")
					GOLF_PRINT_HELP("FLY_INTRO2", TRUE)
				ENDIF

				CANCEL_TIMER(thisHelpers.cameraTimer)
				IF bSkipCutscene
					thisHelpers.bIntroSkipped = TRUE
					//place all players in car
					REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayer
						IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(thisFoursome, iPlayer))   AND DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayer))
						AND NOT IS_ENTITY_DEAD(GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(thisFoursome, iPlayer)) AND NOT IS_ENTITY_DEAD(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayer))
							SET_PED_INTO_VEHICLE(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayer), GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(thisFoursome, iPlayer), GET_GOLF_FOURSOME_INDEXED_PLAYER_SEAT(iPlayer))
						ENDIF
					ENDREPEAT
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(65+284.8931)
					thisHelpers.introState = GIS_INTRO_FLYOVER_CAMERA3
					DESTROY_CAM(thisHelpers.camFlyover3)
				ELSE
					SET_CAM_ACTIVE(thisHelpers.camFlyover3, TRUE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(90 + GET_ENTITY_HEADING(PLAYER_PED_ID()))
					
					iGolfSceneID = CREATE_SYNCHRONIZED_SCENE(<<-1337.0842, 55.5505, 58.957>>, <<0,0,0>>)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iGolfSceneID, "mini@golfclubhouse", "clubhouse_exit_plyr", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iGolfSceneID, FALSE)
					
					thisHelpers.introState = GIS_INTRO_FLYOVER_CAMERA3
				ENDIF
				SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
			ENDIF
		BREAK
		CASE GIS_INTRO_FLYOVER_CAMERA3
			IF bDebugSpew DEBUG_MESSAGE("GIS_INTRO_FLYOVER_CAMERA3") ENDIF
//			DEBUG_MESSAGE("GOLF_MANAGE_INTRO_FLYOVER: GIS_INTRO_FLYOVER_CAMERA3")
			IF DOES_CAM_EXIST(thisHelpers.camFlyover3)
//				IF NOT IS_CAM_INTERPOLATING(thisHelpers.camFlyover3) 
					IF NOT IS_TIMER_STARTED(thisHelpers.cameraTimer)
						START_TIMER_NOW_SAFE(thisHelpers.cameraTimer)
					ENDIF
//				ENDIF
				SET_CAM_COORD(thisHelpers.camFlyover3, GET_CAM_COORD(thisHelpers.camFlyover3) + 0.0075*CONVERT_ROTATION_TO_DIRECTION_VECTOR(GET_CAM_ROT(thisHelpers.camFlyover3)))
			ELSE
				thisHelpers.introState = GIS_INTRO_FLYOVER_CLEANUP
			ENDIF
			
			IF STREAMVOL_IS_VALID(thisHelpers.steamVolNextShot)
				STREAMVOL_DELETE(thisHelpers.steamVolNextShot)
			ENDIF
			
			bSkipCutscene = MANAGE_FLYOVER_UI_INPUT(thisGame, thisFoursome, thisHelpers)
			
			
			IF bSkipCutscene
			OR TIMER_DO_ONCE_WHEN_READY(thisHelpers.cameraTimer, 7.5)
				CANCEL_TIMER(thisHelpers.cameraTimer)
				thisHelpers.introState = GIS_INTRO_FLYOVER_CLEANUP
				SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
				CLEAR_FOCUS()
				
				IF bSkipCutscene
					thisHelpers.bIntroSkipped = TRUE
					//place all players in car
					REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayer
						IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(thisFoursome, iPlayer))   AND DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayer))
						AND NOT IS_ENTITY_DEAD(GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(thisFoursome, iPlayer)) AND NOT IS_ENTITY_DEAD(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayer))
							SET_PED_INTO_VEHICLE(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayer), GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(thisFoursome, iPlayer), GET_GOLF_FOURSOME_INDEXED_PLAYER_SEAT(iPlayer))
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		BREAK
		CASE GIS_INTRO_FLYOVER_CLEANUP
			
			/*
			SET_CAM_COORD(thisHelpers.camNavigate, << -1324.4429, 55.2141, 54.2773 >>)
			SET_CAM_ROT(thisHelpers.camNavigate, << -3.9482, 0.0000, 21.6545 >>)
			SET_CAM_ACTIVE(thisHelpers.camNavigate, TRUE)
			//*/
			IF NOT thisHelpers.bIntroSkipped
				GOLF_END_CUTSCENE_TO_FIRST_PERSON_CAMERA_EFFECT()
			ENDIF
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			
//			DEBUG_MESSAGE("GOLF_CLEANUP_INTRO_FLYOVER")
			IF DOES_CAM_EXIST(thisHelpers.camFlyover1)
				DESTROY_CAM(thisHelpers.camFlyover1)
			ENDIF
			IF DOES_CAM_EXIST(thisHelpers.camFlyover2)
				DESTROY_CAM(thisHelpers.camFlyover2)
			ENDIF
			IF DOES_CAM_EXIST(thisHelpers.camFlyover3)
				DESTROY_CAM(thisHelpers.camFlyover3)
			ENDIF
			
			CLEAR_PED_NON_CREATION_AREA()
			
			CDEBUG1LN(DEBUG_GOLF, "Castro Index ", thisHelpers.iCastroIndex)
			CDEBUG1LN(DEBUG_GOLF, "Dometic unlock flag ", g_savedGlobals.sGolfData.golfSavedFlags)
			
			IF thisHelpers.iCastroIndex > -1 AND GOLF_DID_CURRENT_PLAYER_UNLOCK_DOMESTIC()
				PED_INDEX domesticPed 
				domesticPed = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, thisHelpers.iCastroIndex)
			
				IF NOT IS_PED_INJURED(domesticPed)
					CDEBUG1LN(DEBUG_GOLF, "Play castro greet line!")
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						PLAY_PED_AMBIENT_SPEECH(domesticPed, "CUSTOM_GOLF_GREET_MICHAEL", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						PLAY_PED_AMBIENT_SPEECH(domesticPed, "CUSTOM_GOLF_GREET_FRANKLIN", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						PLAY_PED_AMBIENT_SPEECH(domesticPed, "CUSTOM_GOLF_GREET_TREVOR", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
					ENDIF
				ENDIF

			ELSE
				PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RACE_REACH_START", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
			ENDIF
			
			CDEBUG1LN(DEBUG_GOLF,"Golf opening dialogue")
			thisHelpers.introState = GIS_INTRO_FLYOVER_OVER
		BREAK
		CASE GIS_INTRO_FLYOVER_OVER
			// Do nothing
		BREAK
	ENDSWITCH


ENDPROC

PROC INTERP_CAM_ROT_TO_POINT_AT_COORD(CAMERA_INDEX camIndex, VECTOR vCoord)
	VECTOR vDesiredCamPointRot, vCamPointChange, vCurrentCamRot, vFinalCamRot
	
	vCurrentCamRot = GET_CAM_ROT(camIndex)
	//slowly change view to look at ball
	SET_ROT_FOR_CAM_TO_POINT_AT_COORD(GET_CAM_COORD(camIndex), vCoord, vDesiredCamPointRot)
	
	vCamPointChange = 0.05*(vDesiredCamPointRot - vCurrentCamRot)
	
	vFinalCamRot = vCurrentCamRot+vCamPointChange
	vFinalCamRot.x = vDesiredCamPointRot.x
	vFinalCamRot.y = vDesiredCamPointRot.y
	
//	CDEBUG1LN(DEBUG_GOLF,"Desired rot ", vDesiredCamPointRot, " Current Rot ", vCurrentCamRot)
//	CDEBUG1LN(DEBUG_GOLF,"Change in rot", vCamPointChange, " Final Rot ", vFinalCamRot)
	
	SET_CAM_ROT(camIndex, vFinalCamRot)
ENDPROC

BOOL bUseStandardFlightCam

FUNC BOOL SET_GOLF_FLIGHT_CAMERA_TO_SAFE_NODE( GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, VECTOR ballCurrentPos)

	INT iHole = GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)
	INT iCamIndex
	VECTOR vCamPos, vCamRot
	BOOL bFoundNode = FALSE
	
	REPEAT GET_NUM_OF_GREEN_SPLINE_CAMERAS_FOR_HOLE(iHole) iCamIndex
	
		GET_GOLF_PREVIEW_GREEN_CAMERA_SPLINE_AT_HOLE_AND_INDEX(iHole, iCamIndex, vCamPos, vCamRot)
		
		IF VDIST2(ballCurrentPos, vCamPos) < (50*50) // find cam node near ball
			DETACH_CAM(thisHelpers.camFlight)
			SET_CAM_COORD(thisHelpers.camFlight, vCamPos)
			bFoundNode = TRUE
		ENDIF

	ENDREPEAT

	RETURN bFoundNode
ENDFUNC


FUNC BOOL IS_GOLF_COORD_UNDER_EARTH_OR_WATER(VECTOR vCheckPos)
	FLOAT fHeight
	
	IF NOT GET_GROUND_Z_FOR_3D_COORD(vCheckPos, fHeight)
		RETURN TRUE
	ENDIF
	
	IF GET_WATER_HEIGHT(vCheckPos + <<0, 0, 50>>, fHeight)
		RETURN fHeight > vCheckPos.z
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Manages the flight camera for a golf shot
/// PARAMS:
///    thisHelpers - 
///    thisPlayerCore - 
PROC GOLF_MANAGE_FLIGHT_CAMERA(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_HELPERS &thisHelpers, GOLF_PLAYER_CORE &thisPlayerCore)
	VECTOR ballCurrentPos
	FLOAT fNewCamHeading, fHeight
	VECTOR vNewCamOffset, vNewCamCoord, vDesiredCamCoord, vChangeCamPos
	
	UNUSED_PARAMETER(thisCourse)
		
	IF NOT DOES_CAM_EXIST(thisHelpers.camFlight) OR IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_OPPONENT_DONE_WITH_SHOT)
		IF DOES_CAM_EXIST(thisHelpers.camFlight)
			DESTROY_CAM(thisHelpers.camFlight)
			CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_OPPONENT_DONE_WITH_SHOT)
		ENDIF
		IF DOES_ENTITY_EXIST(GET_GOLF_PLAYER_BALL(thisPlayerCore))
			IF bDebugSpew DEBUG_MESSAGE("GMS_BALL_IN_FLIGHT - in flight, tracking ball") ENDIF
			thisHelpers.camFlight = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			
			FLOAT cameraHeading = GET_ENTITY_HEADING(GET_GOLF_PLAYER_PED(thisPlayerCore))
			
			IF VDIST2( GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore)), GET_ENTITY_COORDS(GET_GOLF_PLAYER_PED(thisPlayerCore), FALSE)) < 10.0*10.0
				//ball too close to player switch angle 180
				CDEBUG1LN(DEBUG_GOLF,"Distance squaareed from ball ", VDIST2( GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore)), GET_ENTITY_COORDS(GET_GOLF_PLAYER_PED(thisPlayerCore), FALSE)))
				cameraHeading = GET_GOLF_PLAYER_AIM(thisPlayerCore) + 180
				bUseStandardFlightCam = FALSE
				
				SET_CAM_COORD(thisHelpers.camFlight,GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore)), cameraHeading, <<13.0, -2.0, 5.0>>))
				POINT_CAM_AT_ENTITY(thisHelpers.camFlight, GET_GOLF_PLAYER_BALL(thisPlayerCore), <<0.0, 0.0, 0.0>>, TRUE)
			ELSE
				IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
				OR IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SKIP_AI_PRESSED)
					vNewCamOffset = <<4.0, 0.5, 1.0>>
				ELSE
					vNewCamOffset = <<-2.0, 1.5, -1.5>>
				ENDIF
				thisHelpers.vSpectateCamOffset = vNewCamOffset
				ballCurrentPos = GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore))
				
				vNewCamCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(ballCurrentPos, cameraHeading, vNewCamOffset)
				SET_CAM_COORD(thisHelpers.camFlight, vNewCamCoord)
				
				VECTOR vBallToCam = NORMALISE_VECTOR(vNewCamCoord - ballCurrentPos)*2.0
				IF IS_GOLF_COURSE_OBSTRUCTING_BETWEEN_POSTIONS(ballCurrentPos+vBallToCam, vNewCamCoord, FALSE, GET_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
				OR IS_GOLF_COORD_UNDER_EARTH_OR_WATER(vNewCamCoord)
					SET_GOLF_FLIGHT_CAMERA_TO_SAFE_NODE(thisFoursome, thisHelpers, ballCurrentPos)
					bUseStandardFlightCam = TRUE
				ELSE
					bUseStandardFlightCam = TRUE
				ENDIF
				
				POINT_CAM_AT_ENTITY(thisHelpers.camFlight, GET_GOLF_PLAYER_BALL(thisPlayerCore), <<0.0, 0.0, 0.0>>, TRUE)
			ENDIF
			SET_CAM_FOV(thisHelpers.camFlight, 45.0)
			SHAKE_CAM(thisHelpers.camFlight, "HAND_SHAKE", 0.5)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		ENDIF
		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT) 
	ELIF GET_GOLF_PLAYER_CONTROL(thisPlayerCore) = HUMAN_LOCAL OR GET_GOLF_PLAYER_CONTROL(thisPlayerCore) = HUMAN_LOCAL_MP
	AND DOES_ENTITY_EXIST(GET_GOLF_PLAYER_BALL(thisPlayerCore))
		
		ballCurrentPos = GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore))
		VECTOR vCamPos
		vCamPos = GET_CAM_COORD(thisHelpers.camFlight)
		VECTOR vBallToCam = NORMALISE_VECTOR(vCamPos - ballCurrentPos)*2.0
		
		IF (IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_BALL_IN_FOLIAGE) 
		AND IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_FIRST_COLLISION))
		OR IS_GOLF_COURSE_OBSTRUCTING_BETWEEN_POSTIONS(ballCurrentPos+vBallToCam, vCamPos, FALSE, GET_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
		OR IS_GOLF_COORD_UNDER_EARTH_OR_WATER(vCamPos)
			//ball landed in some bushes, use safe camera
				
			SET_GOLF_FLIGHT_CAMERA_TO_SAFE_NODE(thisFoursome, thisHelpers, ballCurrentPos)
			POINT_CAM_AT_ENTITY(thisHelpers.camFlight, GET_GOLF_PLAYER_BALL(thisPlayerCore), <<0.0, 0.0, 0.0>>, TRUE)
			bUseStandardFlightCam = FALSE
		
		ELIF bUseStandardFlightCam
			DETACH_CAM(thisHelpers.camFlight)
			IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_FIRST_COLLISION)
				STOP_CAM_POINTING(thisHelpers.camFlight)	
			ENDIF
			
			ballCurrentPos = GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore))
			#IF NOT GOLF_IS_AI_ONLY
			fNewCamHeading = thisFoursome.fHeaingBeforeReactionCamera
			#ENDIF
//			fCamBallDist = VDIST2( GET_CAM_COORD(thisHelpers.camFlight), GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore)))
//			
//			fNewCamBallDist = (SQRT(fCamBallDist)/VMAG(<<13.0, -2.0, 5.0>>)) + 0.0005
			
			vNewCamOffset = <<4.0, 0.5, 1.0>>
			
			vDesiredCamCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore)), fNewCamHeading, vNewCamOffset)
			
//			DRAW_DEBUG_SPHERE(vDesiredCamCoord, 0.25)
			
			vChangeCamPos = (vDesiredCamCoord - GET_CAM_COORD(thisHelpers.camFlight))
			
			vChangeCamPos *= <<1.0, 1.0, 1.0>>
			
			vNewCamCoord = GET_CAM_COORD(thisHelpers.camFlight) + vChangeCamPos
			
			SET_CAM_COORD(thisHelpers.camFlight, vNewCamCoord)
			
			
//			IF fCamBallDist > 25*25
//				//ball far from camera, switch angle
//				CDEBUG1LN(DEBUG_GOLF,"Switch angle")
//				fNewCamHeading = GET_RANDOM_FLOAT_IN_RANGE(0, 360)
//				
//				SET_CAM_COORD(thisHelpers.camFlight,GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore)), fNewCamHeading, <<13.0, -2.0, 5.0>>))
//				POINT_CAM_AT_ENTITY(thisHelpers.camFlight, GET_GOLF_PLAYER_BALL(thisPlayerCore), <<0.0, 0.0, 0.0>>, TRUE)
//				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
//				
////				SET_ROT_FOR_CAM_TO_POINT_AT_COORD(GET_CAM_COORD(thisHelpers.camFlight), ballCurrentPos, vDesiredCamPointRot)
////				SET_CAM_ROT(thisHelpers.camFlight, vDesiredCamPointRot)
//			ENDIF
//			
//			INTERP_CAM_ROT_TO_POINT_AT_COORD(thisHelpers.camFlight, GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore)))
		ENDIF
	ELIF (GET_GOLF_PLAYER_CONTROL(thisPlayerCore) = HUMAN_NETWORK OR GET_GOLF_PLAYER_CONTROL(thisPlayerCore) = COMPUTER)
	AND DOES_ENTITY_EXIST(GET_GOLF_PLAYER_BALL(thisPlayerCore)) AND bUseStandardFlightCam
	AND NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SKIP_AI_PRESSED)
		ballCurrentPos = GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore))
		
		DETACH_CAM(thisHelpers.camFlight)
		//STOP_CAM_POINTING(thisHelpers.camFlight)
		
//		IF IS_CAM_RENDERING(thisHelpers.camFlight)
			FLOAT cameraHeading = GET_GOLF_PLAYER_AIM(thisPlayerCore)
			vNewCamCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(ballCurrentPos, cameraHeading, thisHelpers.vSpectateCamOffset)
			
			IF VDIST2(vNewCamCoord, ballCurrentPos) < (10.0*10.0)
			AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_FIRST_COLLISION)
				CDEBUG1LN(DEBUG_GOLF,"Offset from camera to ball ", thisHelpers.vSpectateCamOffset)
				thisHelpers.vSpectateCamOffset = thisHelpers.vSpectateCamOffset + NORMALISE_VECTOR(<<thisHelpers.vSpectateCamOffset.x, thisHelpers.vSpectateCamOffset.y,  0>>)*0.25
				CDEBUG1LN(DEBUG_GOLF,"Offset after transform ", thisHelpers.vSpectateCamOffset)
				vNewCamCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(ballCurrentPos, cameraHeading, thisHelpers.vSpectateCamOffset)
			ENDIF
			
			IF GET_GROUND_Z_FOR_3D_COORD(vNewCamCoord, fHeight)
				IF (vNewCamCoord.z - fHeight) < 3.0
					vNewCamCoord.z = fHeight + 3.0
				ENDIF
			ELSE
				//new pos is underground
				GOLF_FIND_GROUND_Z(vNewCamCoord, 100, 50)
				vNewCamCoord.z += 3.0
			ENDIF
		    VECTOR vBallToCam = NORMALISE_VECTOR(vNewCamCoord - ballCurrentPos)*2.0
			
			//ran into somthing?
			IF IS_GOLF_COURSE_OBSTRUCTING_BETWEEN_POSTIONS(ballCurrentPos+vBallToCam, vNewCamCoord, FALSE)
			OR IS_GOLF_COORD_UNDER_EARTH_OR_WATER(vNewCamCoord)
				SET_GOLF_FLIGHT_CAMERA_TO_SAFE_NODE(thisFoursome, thisHelpers, ballCurrentPos)
				POINT_CAM_AT_ENTITY(thisHelpers.camFlight, GET_GOLF_PLAYER_BALL(thisPlayerCore), <<0.0, 0.0, 0.0>>, TRUE)
				bUseStandardFlightCam = FALSE
			ELSE
				IF IS_GOLF_FOURSOME_MP()
					POINT_CAM_AT_ENTITY(thisHelpers.camFlight, GET_GOLF_PLAYER_BALL(thisPlayerCore), <<0.0, 0.0, 0.0>>, TRUE)
				ELSE
					INTERP_CAM_ROT_TO_POINT_AT_COORD(thisHelpers.camFlight, ballCurrentPos)
				ENDIF
				SET_CAM_COORD(thisHelpers.camFlight, vNewCamCoord)
			ENDIF
//		ENDIF
	ENDIF
	
	END_GOLF_BALL_TRAIL_PTFX(thisFoursome)
	
//	IF DOES_CAM_EXIST(thisHelpers.camFlight) AND IS_CAM_RENDERING(thisHelpers.camFlight) AND NOT IS_TIMER_STARTED(thisHelpers.resetTimer)
//	AND GET_GOLF_PLAYER_CONTROL(thisPlayerCore) != HUMAN_NETWORK
//		VECTOR camCoords = GET_CAM_COORD(thisHelpers.camFlight)
//		VECTOR ballCoords = GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore))
//		FLOAT fHeading = GET_HEADING_BETWEEN_VECTORS(camCoords,ballCoords)-90
//		VECTOR vRetPos
//	
//		IF WOULD_CAMERA_AT_POSITION_BE_OCCLUDED(ballCoords, fHeading, -1.0*(camCoords - ballCoords), vRetPos)
//			START_TIMER_NOW(thisHelpers.resetTimer)
//			//IF IS_ENTITY_OCCLUDED(GET_GOLF_PLAYER_BALL(thisPlayerCore)) // no good, the trees do not count as occluders
//			CDEBUG1LN(DEBUG_GOLF,"Occluded")
//			FLOAT fFov
//			VECTOR camPosition = FIND_ACCEPTABLE_ADDRESS_CAMERA_POSITION_AND_FOV(thisPlayerCore, fHeading, fFov, thisHelpers) 
//			
//			SET_CAM_COORD(thisHelpers.camFlight, camPosition)
//			SET_CAM_FOV(thisHelpers.camFlight, fFov)
//			POINT_CAM_AT_ENTITY(thisHelpers.camFlight,GET_GOLF_PLAYER_BALL(thisPlayerCore), <<0,0,0>>)
//			/*Rotate camera 180 around ball
//			camCoords = ROTATE_VECTOR_ABOUT_Z_ORTHO(camCoords - ballCoords, ROTSTEP_180)
//			camCoords += ballCoords
//			
//			SET_CAM_COORD(thisHelpers.camFlight, camCoords)//GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_GOLF_PLAYER_BALL(thisPlayerCore)), GET_RANDOM_FLOAT_IN_RANGE(0, 360), <<4.0, -2.0, 1.0>>))
//			POINT_CAM_AT_ENTITY(thisHelpers.camFlight, GET_GOLF_PLAYER_BALL(thisPlayerCore), <<0.0, 0.0, 0.0>>, TRUE)
//			//*/
//		ENDIF
//	ENDIF
	
	TIMER_DO_ONCE_WHEN_READY(thisHelpers.resetTimer, 2.0)
	
ENDPROC


PROC RESET_GOLF_MP_APPROACH_CAM(GOLF_HELPERS &thisHelpers)
	CDEBUG1LN(DEBUG_GOLF,"Resting golf mp approach cam")

	thisHelpers.bTransitionToShotCamOver = FALSE
	
	IF DOES_CAM_EXIST(thisHelpers.camMPApproach)
		DESTROY_CAM(thisHelpers.camMPApproach)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camUpStart)
		DESTROY_CAM(thisHelpers.camUpStart)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camUpEnd)
		DESTROY_CAM(thisHelpers.camUpEnd)
	ENDIF
ENDPROC

FUNC BOOL IS_GOLF_MP_APPROACH_CAM_RENDERING(GOLF_HELPERS &thisHelpers)
	IF DOES_CAM_EXIST(thisHelpers.camMPApproach)
		RETURN IS_CAM_RENDERING(thisHelpers.camMPApproach) OR GET_CAM_SPLINE_PHASE(thisHelpers.camMPApproach) < 1.0
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL MANAGE_GOLF_MP_APPROACH_CAM(GOLF_FOURSOME &thisFoursome, GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_HELPERS &thisHelpers)
	FLOAT fFov
	VECTOR vRot
	VECTOR vBallPos
	INT iRandomVariation
	
	IF thisHelpers.bTransitionToShotCamOver
//		thisHelpers.vMPApproachStartPos = <<0,0,0>>
//		thisHelpers.vMPApproachStartRot = <<0,0,0>>
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_CAM_EXIST(thisHelpers.camMPApproach)
		CDEBUG1LN(DEBUG_GOLF, "Creating mp approach cam")
		thisHelpers.camMPApproach = CREATE_CAMERA(CAMTYPE_SPLINE_TIMED)
	ENDIF
	
	INT iCurrentHole = GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)
	INT iCurrentPlayer = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
	vBallPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
	FLOAT fAim
	
	IF IS_TEE_SHOT(thisCourse, thisFoursome)
		fAim = GET_HEADING_BETWEEN_VECTORS(GET_GOLF_HOLE_FAIRWAY_POSITION(thisCourse, iCurrentHole), GET_GOLF_HOLE_TEE_POSITION(thisCourse, iCurrentHole))+90
	ELSE
		fAim = GET_HEADING_BETWEEN_VECTORS(GET_GOLF_HOLE_PIN_POSITION(thisCourse, iCurrentHole), vBallPos)+90
	ENDIF
	
	IF NOT IS_CAM_ACTIVE(thisHelpers.camMPApproach)
		CDEBUG1LN(DEBUG_GOLF,"Camera not active.")
		IF NOT DOES_CAM_EXIST(thisHelpers.camUpStart)
			thisHelpers.camUpStart = CREATE_CAMERA(CAMTYPE_SCRIPTED)
		ENDIF
		IF NOT DOES_CAM_EXIST(thisHelpers.camUpEnd)
			thisHelpers.camUpEnd = CREATE_CAMERA(CAMTYPE_SCRIPTED)
		ENDIF
		
		SET_CAM_SPLINE_DURATION(thisHelpers.camMPApproach, 3500)
		
		VECTOR addressCamPosition = FIND_ACCEPTABLE_ADDRESS_CAMERA_POSITION_AND_FOV(thisFoursome.playerCore[iCurrentPlayer], fAim, fFov, thisHelpers, FALSE)
		
		SET_CAM_COORD(thisHelpers.camUpStart, addressCamPosition)
		SET_CAM_FOV(thisHelpers.camUpStart, fFov)
		SET_ROT_FOR_CAM_TO_POINT_AT_COORD(addressCamPosition, vBallPos + <<0.0, 0.0, 1.0>> , vRot)
		SET_CAM_ROT(thisHelpers.camUpStart, vRot)
		
		iRandomVariation = GET_RANDOM_INT_IN_RANGE(0, 4)
		IF iRandomVariation = 0
		OR IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
			SET_ROT_FOR_CAM_TO_POINT_AT_COORD(addressCamPosition, (vBallPos + <<0.0, 0.0, 4.0>>), vRot)
		ELIF iRandomVariation = 1
			SET_ROT_FOR_CAM_TO_POINT_AT_COORD(addressCamPosition, (vBallPos + <<0.0, 0.0, 1.0>> ), vRot)
			addressCamPosition += -2.0*NORMALISE_VECTOR(<<COS(fAim), SIN(fAim), 0.0>>)
			addressCamPosition.z += 2.0
		ELIF iRandomVariation = 2
			SET_ROT_FOR_CAM_TO_POINT_AT_COORD(addressCamPosition, (vBallPos + <<0.0, 0.0, 1.0>> ), vRot)
			addressCamPosition += -3.0*NORMALISE_VECTOR(<<COS(fAim-15), SIN(fAim-15), 0.0>>)
		ELSE
			VECTOR vPointDir = NORMALISE_VECTOR(<<COS(fAim+90), SIN(fAim+90), 0>>)
			
			SET_ROT_FOR_CAM_TO_POINT_AT_COORD(addressCamPosition, (vBallPos + <<vPointDir.x, vPointDir.y, 1.0>>  ), vRot)
			addressCamPosition += -2.5*NORMALISE_VECTOR(<<COS(fAim-15), SIN(fAim-15), 0.0>>)
			addressCamPosition.z += 1.0
		ENDIF
		
		SET_CAM_COORD(thisHelpers.camUpEnd, addressCamPosition)
		SET_CAM_FOV(thisHelpers.camUpEnd, fFov)
		SET_CAM_ROT(thisHelpers.camUpEnd, vRot)
		
		ADD_CAM_SPLINE_NODE_USING_CAMERA_FRAME(thisHelpers.camMPApproach, thisHelpers.camUpEnd, 3500)
		ADD_CAM_SPLINE_NODE_USING_CAMERA_FRAME(thisHelpers.camMPApproach,  thisHelpers.camUpStart, 3500)
		
		SET_CAM_SPLINE_PHASE(thisHelpers.camMPApproach, 0.0)
		
		SET_CAM_ACTIVE(thisHelpers.camMPApproach, TRUE)
		
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ENDIF
	
	CDEBUG1LN(DEBUG_GOLF, "Approach cam phase ", GET_CAM_SPLINE_PHASE(thisHelpers.camMPApproach))
	
	IF GET_CAM_SPLINE_PHASE(thisHelpers.camMPApproach) < 1.0
		CLEAR_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_HOLE)
		DISPLAY_RADAR(FALSE)
		RETURN TRUE
	ENDIF
	
	CLEAR_GOLF_LINE_FLAGS()
	GOLF_SET_GOLF_TRAIL_EFFECT(thisGame, thisFoursome, thisHelpers)
	GOLF_DRAW_GOLF_TRAIL_EFFECT(thisHelpers, thisFoursome)
	SET_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_HOLE)
	DISPLAY_RADAR(TRUE)
	ADJUST_ADDRESS_CAMERA(thisFoursome, thisFoursome.playerCore[iCurrentPlayer], thisHelpers, fAim)
	thisHelpers.bTransitionToShotCamOver = TRUE
	
	CDEBUG1LN(DEBUG_GOLF,"Done with approach cam")
	RETURN FALSE

ENDFUNC

INT iGolfBagSyncScene = -1
INT iGolfSceneID = -1

FUNC BOOL DO_GOLF_PAN_UP_CAM(GOLF_HELPERS &thisHelpers)

	IF NOT DOES_CAM_EXIST(thisHelpers.camUpStart)
	AND NOT DOES_CAM_EXIST(thisHelpers.camUpEnd)
		CDEBUG1LN(DEBUG_GOLF,"Start Pan up Cam")
		thisHelpers.camUpStart = CREATE_CAMERA(CAMTYPE_SCRIPTED)
		thisHelpers.camUpEnd = CREATE_CAMERA(CAMTYPE_SCRIPTED)
		
		CAMERA_INDEX renderingCam = GET_RENDERING_CAM()
		VECTOR vStartPos, vStartRot, vEndCamPos, vEndCamRot
		FLOAT fFOV
		IF NOT DOES_CAM_EXIST(renderingCam)
			CDEBUG1LN(DEBUG_GOLF,"Total fucking bullshit!")
			vStartPos = GET_GAMEPLAY_CAM_COORD()
			vStartRot = GET_GAMEPLAY_CAM_ROT()
			fFOV = GET_GAMEPLAY_CAM_FOV()
		ELSE
			vStartPos = GET_CAM_COORD(renderingCam)
			vStartRot = GET_CAM_ROT(renderingCam)
			fFOV = GET_CAM_FOV(renderingCam)
		ENDIF
		
		vEndCamRot = vStartRot + <<45.0, 0.0, 0.0>>
		vEndCamPos = vStartPos
		vEndCamPos.z = 91.6584
		
		SET_CAM_COORD(thisHelpers.camUpStart, vStartPos)
		SET_CAM_COORD(thisHelpers.camUpEnd, vEndCamPos)

		SET_CAM_ROT(thisHelpers.camUpStart, vStartRot)
		SET_CAM_ROT(thisHelpers.camUpEnd, vEndCamRot)
		
		SET_CAM_FOV(thisHelpers.camUpStart, fFOV)
		SET_CAM_FOV(thisHelpers.camUpEnd,   fFOV)

		SET_CAM_ACTIVE_WITH_INTERP(thisHelpers.camUpEnd, thisHelpers.camUpStart, 1000)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		PLAY_SOUND_FRONTEND( -1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
		//SET_TIMECYCLE_MODIFIER("MinigameTransitionIn") 
		ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, FALSE) 
	ELSE
		
		IF NOT IS_CAM_INTERPOLATING(thisHelpers.camUpEnd)
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_GOLF,"Camera is panning up")
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL DO_END_OF_GOLF_GAME_CUTSCENE(GOLF_HELPERS &thisHelpers)
	
	IF IS_END_OF_GOLF_CUTSCENE_LOADED()
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iGolfSceneID)
		
			CLEAR_PED_WETNESS(PLAYER_PED_ID())
		
			IF DOES_CAM_EXIST(thisHelpers.camNavigate)
				DESTROY_CAM(thisHelpers.camNavigate)
			ENDIF
			thisHelpers.camNavigate = CREATE_CAMERA(CAMTYPE_ANIMATED)
			
			IF NOT DOES_CAM_EXIST(thisHelpers.camUpStart)
				thisHelpers.camUpStart = CREATE_CAMERA(CAMTYPE_SCRIPTED)
			ENDIF
			
			SET_CAM_COORD(thisHelpers.camUpStart,<<-1386.7378, 37.2216, 91.6584>>)
			SET_CAM_ROT(thisHelpers.camUpStart,  <<30.9617, 0.7363, -41.6516>>)
			
			ANIMPOSTFX_STOP("MinigameTransitionIn")
			ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
			
			DISPLAY_RADAR(FALSE)
			iGolfSceneID = CREATE_SYNCHRONIZED_SCENE(<<-1337.0842, 55.5505, 58.957>>, <<0,0,0>>)
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iGolfSceneID, "mini@golfclubhouse", GET_GOLF_ENDING_ANIM(thisHelpers.bWin), INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			PLAY_SYNCHRONIZED_CAM_ANIM(thisHelpers.camNavigate, iGolfSceneID, GET_GOLF_ENDING_CAM_ANIM(thisHelpers.bWin), "mini@golfclubhouse")
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iGolfSceneID, FALSE)
			
			SET_CAM_FOV(thisHelpers.camUpStart, GET_CAM_FOV(thisHelpers.camNavigate))
			
			SET_CAM_ACTIVE_WITH_INTERP(thisHelpers.camNavigate, thisHelpers.camUpStart, 666)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			IF thisHelpers.bWin
				TASK_PLAY_ANIM(PLAYER_PED_ID(), GET_GOLF_END_FACE_ANIM(), "mood_happy_1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_SECONDARY)
				PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GAME_GOOD_SELF", SPEECH_PARAMS_FORCE_NORMAL)
				UNLOCK_GOLF_BUDDY_AFTER_GOLF_WIN(thisHelpers)
				PLAY_SOUND_FRONTEND( -1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
			ELSE
				PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GAME_BAD_SELF", SPEECH_PARAMS_FORCE_NORMAL)
				PLAY_SOUND_FRONTEND( -1, "LOSER", "HUD_AWARDS")
			ENDIF
		ELSE
			IF GET_SYNCHRONIZED_SCENE_PHASE(iGolfSceneID) > 0.95
				RETURN TRUE
			ELSE
			
				CDEBUG1LN(DEBUG_GOLF,"sync scene is running, pahse ", GET_SYNCHRONIZED_SCENE_PHASE(iGolfSceneID))
				CDEBUG1LN(DEBUG_GOLF,"Camera phase ", GET_CAM_ANIM_CURRENT_PHASE(thisHelpers.camNavigate))		
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_END_OF_GOLF_GAME_BAG_CUTSCENE(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_GAME &thisGame, GOLF_COURSE &thisCourse)
	TEXT_LABEL_23 sAnimDict = "mini@golfclub_bag"
	TEXT_LABEL_31 sAnimName, sAnimPed, sAnimBag, sAnimCart, sAnimClub, sAnimCam
	
	IF IS_END_OF_GOLF_CUTSCENE_LOADED()
	
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iGolfBagSyncScene)
			IF thisHelpers.bWin
				sAnimName = "clubbag_round_end_hap_"
			ELSE
				sAnimName = "clubbag_round_end_mad_"
			ENDIF
			
			sAnimPed  = sAnimName
			sAnimBag  = sAnimName
			sAnimCart = sAnimName
			sAnimClub = sAnimName
			sAnimCam  = sAnimName
			
			sAnimPed  += "plyr"
			sAnimBag  += "bag"
			sAnimCart += "cart"
			sAnimClub += "club"
			sAnimCam  += "cam"
			
			INT iLocalPlayer = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
			PED_INDEX     golferPed   = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iLocalPlayer)
			OBJECT_INDEX  golferBag   = GET_GOLF_FOURSOME_INDEXED_PLAYER_GOLFBAG(thisFoursome, iLocalPlayer)
			VEHICLE_INDEX golferCart  = GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(thisFoursome, iLocalPlayer)
			OBJECT_INDEX  golferClub  = GET_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(thisFoursome, iLocalPlayer)

			IF NOT DOES_ENTITY_EXIST(golferClub)
				golferClub = CREATE_GOLF_CLUB(thisFoursome.playerCore[iLocalPlayer], thisGame.golfBag)
			ENDIF
			SET_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(thisFoursome, iLocalPlayer, golferClub)
			DETACH_ENTITY(golferClub)
			
			IF NOT DOES_ENTITY_EXIST(golferBag)
				golferBag = CREATE_OBJECT_NO_OFFSET(PROP_GOLF_BAG_01B, GET_GOLF_COURSE_START_POSITION(thisCourse, 0))
			ENDIF
			DETACH_ENTITY(golferBag)
			
			IF IS_ENTITY_DEAD(golferCart)
				DELETE_VEHICLE(golferCart)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(golferCart)
				golferCart = CREATE_VEHICLE(CADDY, GET_GOLF_COURSE_CART_POSITION(thisCourse, 0))
			ENDIF
			IF NOT IS_ENTITY_DEAD(golferCart)
				SET_VEHICLE_FIXED(golferCart)
			ENDIF
			
			SET_CURRENT_PED_WEAPON(golferPed, WEAPONTYPE_UNARMED, TRUE)
			thisHelpers.camNavigate = CREATE_CAMERA(CAMTYPE_ANIMATED)
			IF DOES_ENTITY_EXIST(golferCart)
				iGolfBagSyncScene = CREATE_SYNCHRONIZED_SCENE(<<-1322.191, 30.327, 52.960>>, <<0,0,-178.000>>)
				TASK_SYNCHRONIZED_SCENE(golferPed,        iGolfBagSyncScene, sAnimDict, sAnimPed, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(golferBag,  iGolfBagSyncScene, sAnimBag,  sAnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(golferCart, iGolfBagSyncScene, sAnimCart, sAnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(golferClub, iGolfBagSyncScene, sAnimClub, sAnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				PLAY_SYNCHRONIZED_CAM_ANIM(thisHelpers.camNavigate, iGolfBagSyncScene, sAnimCam, sAnimDict)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iGolfBagSyncScene, TRUE)
				
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(golferCart)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(golferBag)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(golferClub)
			ENDIF
			
			INT playerCount
			OBJECT_INDEX  updateGolferBag
			REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) playerCount
				updateGolferBag = GET_GOLF_FOURSOME_INDEXED_PLAYER_GOLFBAG(thisFoursome, playerCount)
				IF DOES_ENTITY_EXIST(updateGolferBag) AND updateGolferBag != golferBag
					DELETE_OBJECT(updateGolferBag)
				ENDIF
			ENDREPEAT
			
			PLAY_SOUND_FRONTEND( -1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
			DISPLAY_RADAR(FALSE)
			SET_ENTITY_VISIBLE( GET_GOLF_PLAYER_PED(thisFoursome.playerCore[0]), TRUE) // Just in case player has been hidden			
			SET_CAM_ACTIVE(thisHelpers.camNavigate, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		ELSE
		
			IF GET_SYNCHRONIZED_SCENE_PHASE(iGolfBagSyncScene) > 0.99
				CDEBUG1LN(DEBUG_GOLF,"Club in bag cutscene over")
				DO_GOLF_PAN_UP_CAM(thisHelpers)
				RETURN TRUE
			ENDIF
		
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC GOLF_CREATE_ANIMATED_CAMERA_PAUSE_CAMERA(GOLF_HELPERS &thisHelpers, CAMERA_INDEX &camToPause)

	IF DOES_CAM_EXIST(thisHelpers.camPauseAnimCamera)
		DESTROY_CAM(thisHelpers.camPauseAnimCamera)
	ENDIF
	
	VECTOR vCamPos = GET_CAM_COORD(camToPause)
	VECTOR vCamRot = GET_CAM_ROT(camToPause)
	FLOAT fFov = GET_CAM_FOV(camToPause)
	
	thisHelpers.camPauseAnimCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos, vCamRot, fFov, TRUE)

ENDPROC
