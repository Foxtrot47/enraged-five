USING "golf_player.sch"
USING "golf_ai.sch"
USING "golf_input.sch"
USING "golf_ui.sch"
USING "golf_anims_lib.sch"
USING "golf_physics_lib.sch"
USING "golf_dialogue.sch"


PROC SET_GOLFER_NAV_HEADING(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, BOOL bInterp)
	
	bInterp = FALSE //do not interprolate from scripted camerra to game camera
	IF NOT bInterp
		GOLF_END_CUTSCENE_TO_FIRST_PERSON_CAMERA_EFFECT()
	ENDIF
	RENDER_SCRIPT_CAMS(FALSE, bInterp, DEFAULT_INTERP_TO_FROM_GAME, FALSE)


	IF NOT IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PLAYER_NAV_HEADING) 
		IF NOT (IS_TEE_SHOT(thisCourse, thisFoursome) AND GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) = 0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(180 + GET_HEADING_BETWEEN_VECTORS( GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL_POSITION(thisFoursome), GET_ENTITY_COORDS(GET_GOLF_FOURSOME_LOCAL_PLAYER_PED(thisFoursome))) - GET_ENTITY_HEADING(GET_GOLF_FOURSOME_LOCAL_PLAYER_PED(thisFoursome)))
			SET_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_PLAYER_NAV_HEADING)	
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Sets the name of the blip on the pause screen for opposing net players, or the player
PROC SET_BLIP_NAME_WITH_PLAYER_ID(GOLF_FOURSOME &thisFoursome, BLIP_INDEX blipIndex, INT iPlayerIndex)
	IF iPlayerIndex = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
		SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "YOUR_BALL")
	ELSE
		STRING pTextLabel
		pTextLabel = "OPP_BALL"
		
		BEGIN_TEXT_COMMAND_SET_BLIP_NAME(pTextLabel)
			IF IS_GOLF_FOURSOME_MP()
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(thisFoursome.playerCore[iPlayerIndex].playerCard.sPlayerName)
			ELSE
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(thisFoursome.playerCore[iPlayerIndex].playerCard.sPlayerName)
			ENDIF
		END_TEXT_COMMAND_SET_BLIP_NAME(blipIndex)
		//SET_BLIP_NAME_FROM_ASCII(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, iPlayerIndex), pTextLabel)
	ENDIF
ENDPROC

PROC MANAGE_GOLF_BALL_BLIPS(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	INT iPlayerIndex
	BOOL bIsCurrentPlayer
	
	//CDEBUG1LN(DEBUG_GOLF,"Curretn player is ", GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		
		bIsCurrentPlayer = FALSE
		IF iPlayerIndex = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
			bIsCurrentPlayer = TRUE
		ENDIF
		
		VECTOR vNewBlipPos = GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL_POSITION(thisFoursome, iPlayerIndex)
		
		//Follow the current players ball as it travels through the air
		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex)) AND bIsCurrentPlayer
			vNewBlipPos = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex))
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(vNewBlipPos)
			//don't show the blip for tee shot, unless they are the current player
			IF (GET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(thisFoursome, iPlayerIndex) > 0 OR bIsCurrentPlayer)
			//don't show blip for local player, if it's their turn
			AND (iPlayerIndex != GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome) OR NOT bIsCurrentPlayer)
			//only show blip if player isn't done with hole
			AND GET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, iPlayerIndex) != GPS_DONE_WITH_HOLE
			//Don't blip inactive players
			AND NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
				IF NOT DOES_BLIP_EXIST(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, iPlayerIndex))
					SET_GOLF_HELPER_BALL_BLIP(thisHelpers, CREATE_BLIP_FOR_COORD(vNewBlipPos), iPlayerIndex)
					SET_BLIP_NAME_WITH_PLAYER_ID(thisFoursome, GET_GOLF_HELPER_BALL_BLIP(thisHelpers, iPlayerIndex), iPlayerIndex)
					SET_BLIP_COLOUR(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, iPlayerIndex), GET_GOLF_PLAYER_BLIP_COLOUR(iPlayerIndex))
					SET_BLIP_SCALE(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, iPlayerIndex), GOLF_BALL_BLIP_SCALE)
					SHOW_HEIGHT_ON_BLIP(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, iPlayerIndex), FALSE)
				ELIF NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, iPlayerIndex)), vNewBlipPos)
					SET_BLIP_COORDS(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, iPlayerIndex), vNewBlipPos)
					SET_BLIP_SCALE(GET_GOLF_HELPER_BALL_BLIP(thisHelpers, iPlayerIndex), GOLF_BALL_BLIP_SCALE)
				ENDIF
			ELSE
				//CDEBUG1LN(DEBUG_GOLF,"Cleaning up player ", iPlayerIndex) 
				CLEANUP_GOLF_HELPER_BALL_BLIP(thisHelpers, iPlayerIndex)
			ENDIF
		ELSE
			CLEANUP_GOLF_HELPER_BALL_BLIP(thisHelpers, iPlayerIndex)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL GOLF_CUT_BACK_TO_PLAYER(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_GAME &thisGame)

	IF IS_SHOT_A_GIMME(thisCourse, thisFoursome, TRUE) 
		CDEBUG1LN(DEBUG_GOLF, "GOLF_CUT_BACK_TO_PLAYER: The shot is a gimme")
		RETURN FALSE
	ENDIF

	IF GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) = 1
		CDEBUG1LN(DEBUG_GOLF,"Only one player, return true")
		RETURN TRUE
	ENDIF

	IF NOT (FIND_NEXT_SHOOTER(thisFoursome, thisCourse) = 0 OR ARE_ALL_PLAYERS_RECENTLY_DONE_WITH_TEE_SHOT(thisFoursome, thisCourse))
		CDEBUG1LN(DEBUG_GOLF,"Next shooter is not the player or we are not done with the tee shots")
		RETURN FALSE
	ENDIF
	
	IF IS_TEE_SHOT(thisCourse, thisFoursome)
		CDEBUG1LN(DEBUG_GOLF,"is tee shot")
		RETURN FALSE
	ENDIF
	
	IF (GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) != 0 AND IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_PLAYER_SKIP))
		CDEBUG1LN(DEBUG_GOLF,"Player skiped but the current player is someone else")	
		RETURN FALSE
	ENDIF

	CDEBUG1LN(DEBUG_GOLF,"Cut back to player")
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_ADDRESS_HELP_DISPLAY(GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers)
 RETURN NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
		AND NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_DISPLAY_QUITTER)
		AND NOT IS_GOLF_SPLASH_DISPLAYING()
ENDFUNC

PROC MANAGE_DISPLAY_TIE_MESSAGE(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_GAME &thisGame)

	IF NOT IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_START_TIE)
	AND GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) = 0
	AND IS_TEE_SHOT(thisCourse, thisFoursome) AND thisGame.bPlayoffGame
		IF NOT IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PLAYER_REMOVED)
			GOLF_PRINT_HELP("START_PLAYOFF")
		ELSE
			GOLF_PRINT_HELP("START_PLAYOFF_SPEC")
		ENDIF
		SET_GOLF_HELP_DISPLAYED_FLAG(thisHelpers, GHD_START_TIE)
	ENDIF

ENDPROC
PROC GOLF_MANAGE_ADDRESS_HELP(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_GAME &thisGame, GOLF_PLAYER &thisPlayer)
	
	ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
	
	IF thisHelpers.bGreenPreviewCam = 1
	OR IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED)
	OR IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_MP_START_QUIT_MENU)
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF
		EXIT
	ENDIF
	
	IF SHOULD_ADDRESS_HELP_DISPLAY(thisGame, thisHelpers)
		IF GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) = thisHelpers.iStartingHole
			IF IS_TEE_SHOT(thisCourse, thisFoursome)
			AND NOT thisGame.bPlayoffGame AND NOT thisHelpers.bGameIsReplay
				// Have to use different help text on PC keyboard and mouse as the script inputs don't support single up/down directions.
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					GOLF_PRINT_HELP("SWING_INSTR_S_KM", TRUE)
				ELSE
					GOLF_PRINT_HELP("SWING_INSTR_S", TRUE)
				ENDIF
			ELIF GET_GOLF_FOURSOME_LOCAL_PLAYER_SHOTS_ON_HOLE(thisFoursome) = 1
			AND NOT thisGame.bPlayoffGame AND NOT thisHelpers.bGameIsReplay
			AND GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) != LIE_GREEN
				GOLF_PRINT_HELP("SPIN_INSTR", TRUE)
			ELIF GET_GOLF_PLAYER_HOLE_PUTTS(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) = 0
			AND GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN
				GOLF_PRINT_HELP("PUTT_INSTR", TRUE)
			ELIF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
		ELIF GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) = thisHelpers.iStartingHole+1
			IF IS_TEE_SHOT(thisCourse, thisFoursome)
			AND NOT thisGame.bPlayoffGame AND NOT thisHelpers.bGameIsReplay
				IF NOT IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_DISPLAY_SCORECARD_HELP)
					SET_GOLF_HELP_DISPLAYED_FLAG(thisHelpers, GHD_DISPLAY_SCORECARD_HELP)
					GOLF_PRINT_HELP("GOLF_SCORE_HELP", FALSE)
				ENDIF
			ELIF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
		ELSE
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_ADRESS_INPUT_DISPLAY(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse)

	IF IS_SHOT_A_GIMME(thisCourse, thisFoursome)
	OR thisHelpers.bGreenPreviewCam != 0
	OR IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED)
		EXIT
	ENDIF
	
	IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_REFRESH_INPUT)
		CDEBUG1LN(DEBUG_GOLF,"Update input display")
		SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome)
			CASE HUMAN_LOCAL
			CASE HUMAN_LOCAL_MP
				SETUP_GOLF_ADRESS_CONTROLS(thisHelpers, IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome), 
											NOT IS_BALL_IN_RANGE_OF_GREEN(thisCourse, thisFoursome), thisHelpers.bShotPreviewCam)
			BREAK
			CASE COMPUTER
				SETUP_GOLF_SPECTATE_COMPUTER_CONTROLS(thisHelpers)
			BREAK
			CASE HUMAN_NETWORK
				SETUP_GOLF_SPECTATE_MP_CONTROLS(thisHelpers)
			BREAK
		ENDSWITCH
		
		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_INPUT)
	ENDIF
	
	GOLF_DISPLAY_CONTROLS(thisHelpers, TRUE)

ENDPROC

/// PURPOSE:
///    Manages player flags for the current player, and takes appropriate action when they are set. This is split off so that non-player peds can play
/// PARAMS:
///    thisFoursome - 
///    thisPlayer - 
///    thisHelpers - 
PROC GOLF_MANAGE_PLAYER_FLAGS(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayer, GOLF_HELPERS &thisHelpers)
	
	UNUSED_PARAMETER(thisGame)
	
	GOLF_LIE_TYPE lieCurrent = GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)
	BOOL bGreenOrFairway = (lieCurrent = LIE_GREEN) OR (lieCurrent = LIE_FAIRWAY) OR (lieCurrent = LIE_CUP)
	GOLF_OOB_REASON OOBReason
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK 
		IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_SHOT_OVER)
			CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_SHOT_OVER)
			IF NOT IS_GOLF_BALL_OOB(thisFoursome, thisCourse, OOBReason)
				IF bDebugSpew DEBUG_MESSAGE("GOT SHOT OVER SP, CLEARING AND ADDING SCORE") ENDIF
				// SET Fairways in Regulation
				IF NOT GET_GOLF_PLAYER_HOLE_FIR(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
					FLOAT fDriveDist = VDIST(GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome) = 0 AND GET_GOLF_HOLE_PAR(thisCourse,  GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) != 3 // We are checking against 0 because the splash check happens before the score increment
					AND fDriveDist > ACCEPTABLE_DRIVE_DIST //make sure the player actually hit the ball a reasonable distance
					AND IS_GOLF_COORD_INSIDE_FAIRWAY_OF_INDEXED_HOLE(GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) )
						IF bGreenOrFairway
							SET_GOLF_PLAYER_HOLE_FIR(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
							SET_GOLF_UI_FLAG(thisHelpers, GUC_DISPLAY_FIR)
						ENDIF
					ENDIF
				ENDIF
				
				// SET Greens in Regulation
				IF NOT GET_GOLF_PLAYER_HOLE_GIR(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome) <= GET_GOLF_HOLE_PAR(thisCourse,  GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) - 3 // We are doing this at -3 because the splash check happens before the score is incremented
						IF lieCurrent = LIE_GREEN
							SET_GOLF_PLAYER_HOLE_GIR(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
							SET_GOLF_UI_FLAG(thisHelpers, GUC_DISPLAY_GIR)
						ENDIF
					ENDIF
				ENDIF
				IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
					INCREMENT_GOLF_PLAYER_HOLE_PUTTS(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), 1)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_DRIVE_CARRY) AND bAllowLeaderboardUpload
		CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_DRIVE_CARRY)
		IF NOT IS_GOLF_BALL_OOB(thisFoursome, thisCourse, OOBReason)
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != COMPUTER
			AND GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome) = 0
				IF GET_GOLF_HOLE_PAR(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) != 3 // We don't track longest drive on Par 3
					IF bGreenOrFairway
						SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_SUCCESSFUL_DRIVE)//the golfers drive this hole was good was good
						FLOAT fDriveDist = VDIST(GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))
						fDriveDist = MAGIC_DISTANCE_CONVERSION* METERS_TO_YARDS(fDriveDist) 
						fDriveDist = TO_FLOAT(FLOOR(fDriveDist)) //truncate
												
						IF GOLF_SHOULD_USE_FOREIGN_DISTANCE()
							fDriveDist = YARDS_TO_METERS(fDriveDist)
						ENDIF
						
						IF fDriveDist > GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
						AND fDriveDist > ACCEPTABLE_DRIVE_DIST  //make sure the player actually hit the ball a reasonable distance
						AND IS_GOLF_COORD_INSIDE_FAIRWAY_OF_INDEXED_HOLE(GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) )
							// New longest drive for this hole
							SET_GOLF_STAT_LONGEST_DRIVE_FOR_HOLE(GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), fDriveDist)
							SET_GOLF_UI_FLAG(thisHelpers, GUC_DISPLAY_LONGEST_HOLE)
						ENDIF
						IF fDriveDist > GET_GOLF_STAT_LONGEST_DRIVE()
						AND fDriveDist > 30.0 //make sure the player actually hit the ball a reasonable distance
							// New longest drive ever
							CDEBUG1LN(DEBUG_GOLF,"New longest drive ever. Old value ", GET_GOLF_STAT_LONGEST_DRIVE(), " New value ", fDriveDist)
							SET_GOLF_STAT_LONGEST_DRIVE(fDriveDist)
							SET_GOLF_UI_FLAG(thisHelpers, GUC_DISPLAY_LONGEST_DRIVE)
						ENDIF
					ENDIF
				ELSE	// We track closest to pin, instead
				
					IF lieCurrent = LIE_GREEN AND GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome) = 0
						g_savedGlobals.sGolfData.iNumFairwayDrivesTotal++ // inc total number of succesfull drives
						FLOAT toPinDist = VDIST(GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
						toPinDist = MAGIC_DISTANCE_CONVERSION* METERS_TO_YARDS(toPinDist)//*3.0 don't convert to feet, it ruins the illusion
						
						IF GOLF_SHOULD_USE_FOREIGN_DISTANCE()
							toPinDist = FEET_TO_METERS(toPinDist)
						ENDIF
							
						IF  toPinDist < GET_GOLF_STAT_LONGEST_DRIVE_ON_HOLE(GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
							
							// New closest to pin
							SET_GOLF_STAT_LONGEST_DRIVE_FOR_HOLE(GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), toPinDist)
							SET_GOLF_UI_FLAG(thisHelpers, GUC_DISPLAY_CLOSEST_TO_PIN)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_HOLE_OVER)
		CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_HOLE_OVER)
		RESTART_TIMER_NOW(thisHelpers.cameraTimer)
	ENDIF
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_SHOT_STARTED)
		CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_SHOT_STARTED)
		IF DOES_CAM_EXIST(thisHelpers.camAddress)
			POINT_CAM_AT_ENTITY(thisHelpers.camAddress,GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), <<0.0, 0.0, 1.0>>)
		ENDIF
	ENDIF
ENDPROC

PROC GOLF_MANAGE_PREVIEW_LINE_UPDATE(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)

	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
		IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
			IF NOT IS_GOLF_TRAIL_INTERSECT_FLAG_SET()
				GOLF_SET_GOLF_TRAIL_EFFECT(thisGame, thisFoursome, thisHelpers)
			ELSE
				IF thisHelpers.bShotPreviewCam
					IF NOT IS_GOLF_CAM_INTERPOLATING(thisHelpers.camPreviewSplineForward, TRUE)
					OR IS_GOLF_TRAIL_STYLE_CHANGE_FLAG_SET()
						//CDEBUG1LN(DEBUG_GOLF,"Adjust preview in player_lib")
						ADJUST_PREVIEW_CAMERA(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
						SET_GOLF_TRAIL_STYLE_CHANGE_FLAG(FALSE)
					ENDIF
				ELSE
					SET_GOLF_TRAIL_STYLE_CHANGE_FLAG(FALSE)
					//ADJUST_ADDRESS_CAMERA(thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL GOLF_CHECK_PLAYER_NEAR_BALL(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome)
	
	PED_INDEX golferPedIndex = GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome)
	
	//Guarantee position at ball
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
	AND NOT IS_PED_INJURED(golferPedIndex)
		IF NOT IS_PED_RAGDOLL(golferPedIndex)
			VECTOR vPlayer2DPos, vBall2DPos
			vPlayer2DPos = GET_ENTITY_COORDS(golferPedIndex, FALSE)
			vPlayer2DPos.z = 0
			vBall2DPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
			vBall2DPos.z = 0
			
			IF VDIST2(vPlayer2DPos, vBall2DPos) > (1.2*1.2)
				//Too far from ball teleport
				CDEBUG1LN(DEBUG_GOLF,"Too far from ball, teleport to it.")
				CDEBUG1LN(DEBUG_GOLF,"2D distance from ball ", VDIST(vPlayer2DPos, vBall2DPos) )
				PLACE_PLAYER_AT_BALL_WITH_HEADING(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), FALSE, FALSE, TRUE, FALSE, TRUE)
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL GOLF_NETWORK_REQUEST_CONTROL_OF_ALL_BALLS(GOLF_FOURSOME &thisFoursome)

	INT iPlayerIndex
	BOOL bOwnsAllBalls = TRUE
	
	PED_INDEX tempPed
	IF NETWORK_IS_IN_SPECTATOR_MODE()
		SET_IN_SPECTATOR_MODE(FALSE, tempPed)
	ENDIF
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		OBJECT_INDEX objBallID = GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex)
		
		IF DOES_ENTITY_EXIST(objBallID)
			IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(objBallID)
				NETWORK_INDEX netBallID = NETWORK_GET_NETWORK_ID_FROM_ENTITY(objBallID)
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netBallID)	
					IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(objBallID)
						CDEBUG1LN(DEBUG_GOLF,"Doesn't have control of ball, ", iPlayerIndex)
						bOwnsAllBalls = FALSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(objBallID)
					ELSE
//						CDEBUG1LN(DEBUG_GOLF,"Set ball can't migrate ", iPlayerIndex)
						SET_NETWORK_ID_CAN_MIGRATE(netBallID, FALSE)
					ENDIF
				ELSE
					//The ball exist but it doesn't have a network id? This is bad, get rid of ball.
					CDEBUG1LN(DEBUG_GOLF, "Ball doesn't have network id ", iPlayerIndex)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(objBallID)
						//I am the owner, get rid of it
						CDEBUG1LN(DEBUG_GOLF, "Delete Ball")
						SET_ENTITY_AS_MISSION_ENTITY(objBallID, TRUE, TRUE) //I tried this function and it didn't stop the delete_ball assert
						CLEANUP_GOLF_FOURSOME_INDEXED_GOLF_BALL(thisFoursome, iPlayerIndex)
					ENDIF
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_GOLF, "Ball isn't registed with thread ", iPlayerIndex)
			ENDIF
		ELSE
			IF iPlayerIndex = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
				CDEBUG1LN(DEBUG_GOLF, "Current players ", iPlayerIndex, " ball does not exist?! Wait for it to be created.")
				bOwnsAllBalls = FALSE
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	RETURN bOwnsAllBalls
ENDFUNC

FUNC BOOL GOLF_NETWORK_RELEASE_CONTROL_OF_ALL_BALLS(GOLF_FOURSOME &thisFoursome)

	INT iPlayerIndex
	BOOL bOwnsNoBalls = TRUE
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		
		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex))
			OBJECT_INDEX objBall = GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex)
		
			IF NETWORK_HAS_CONTROL_OF_ENTITY(objBall)
				bOwnsNoBalls = FALSE
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(NETWORK_GET_NETWORK_ID_FROM_ENTITY(objBall))
					CDEBUG1LN(DEBUG_GOLF, "Release control of networked golf ball ", iPlayerIndex)
					SET_NETWORK_ID_CAN_MIGRATE(NETWORK_GET_NETWORK_ID_FROM_ENTITY(objBall), TRUE)
				ELSE
					CLEANUP_GOLF_FOURSOME_INDEXED_GOLF_BALL(thisFoursome, iPlayerIndex)
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT

	RETURN bOwnsNoBalls
ENDFUNC

FUNC BOOL SHOULD_PLAYER_CREATE_GOLF_BALL()
	#IF GOLF_IS_MP
		RETURN NETWORK_IS_HOST_OF_THIS_SCRIPT()
	#ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Manages the entire shot from adddress to ball stopping for a golf player (human or AI)
/// PARAMS:
///    thisGame - 
///    thisCourse - 
///    thisPlayer - 
///    thisFoursome - 
///    thisHelpers - 
PROC GOLF_PLAYER_TAKE_SHOT(GOLF_GAME &thisGame,GOLF_COURSE &thisCourse, GOLF_PLAYER &thisPlayer, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &allPlayers[])
	
	// Need a better recovery mechanism here!
	IF NOT DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
		CDEBUG1LN(DEBUG_GOLF,"Current player doesn't exsist ", GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
		EXIT
	ENDIF
	IF IS_ENTITY_DEAD(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
		CDEBUG1LN(DEBUG_GOLF,"Current player is dead")
		EXIT
	ENDIF
	IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_PLAYER_TAKE_SHOT: Player ",thisFoursome.iCurrentPlayer," with netindex ",thisPlayer.iParticipantIndex) ENDIF
	
	VECTOR vSafeSpotAim, vCartPointSpot
	BOOL goodShot = TRUE, bLieUnplayable = FALSE, bInWater = FALSE
	GOLF_OOB_REASON OOBReason
	FLOAT fSafeAimHeading
	BOOL bControlsBall
	
	UNUSED_PARAMETER(allPlayers)
	
	SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome)
		CASE GPS_PLACE_BALL
			bControlsBall = TRUE
			thisHelpers.vLastSafeBallPosition = <<0, 0, 0>>
			
			IF NOT DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
				IF SHOULD_PLAYER_CREATE_GOLF_BALL()
					SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome, CREATE_GOLF_BALL(GET_GOLF_HOLE_TEE_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) + <<0,0, 0.05>>, GET_HEADING_BETWEEN_VECTORS(GET_GOLF_HOLE_FAIRWAY_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))))
				ENDIF
			ENDIF
			
			#IF GOLF_IS_MP
				IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
					bControlsBall = GOLF_NETWORK_REQUEST_CONTROL_OF_ALL_BALLS(thisFoursome)	
				ELSE
					GOLF_NETWORK_RELEASE_CONTROL_OF_ALL_BALLS(thisFoursome)
				ENDIF
				
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
			#ENDIF
			#IF NOT GOLF_IS_MP
				IF STREAMVOL_IS_VALID(thisHelpers.steamVolNextShot)
					IF NOT STREAMVOL_HAS_LOADED(thisHelpers.steamVolNextShot)
						CDEBUG1LN(DEBUG_GOLF,"Golf next shot stream vol not ready yet")
						bControlsBall = FALSE
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_GOLF,"Golf next shot stream vol not active")
				ENDIF
				
			#ENDIF
			
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
			AND bControlsBall
				CLEAR_NEARBY_GOLF_BALLS(thisFoursome)
			
				IF bDebugSpew DEBUG_MESSAGE("GPS_PLACE_BALL") ENDIF

				IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_NEEDS_SNAP)
					CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_NEEDS_SNAP)
				ENDIF
				IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_MARKER(thisFoursome))
					CLEANUP_GOLF_FOURSOME_CURRENT_PLAYER_MARKER(thisFoursome)
				ENDIF
				REMOVE_GOLF_CLUB_WEAPON(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), TRUE)
				IF NOT bDrivingRangeMode
					PICK_IDEAL_CLUB_AND_SHOT(thisGame, thisCourse,thisFoursome, thisHelpers)
				ENDIF
				
				vSafeSpotAim = GET_GOLF_HOLE_FAIRWAY_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
				fSafeAimHeading = GET_HEADING_BETWEEN_VECTORS(vSafeSpotAim, GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))+90
				
				SET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome, fSafeAimHeading)
				#IF NOT GOLF_IS_MP
				PLACE_PLAYER_AT_BALL_WITH_HEADING(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag, fSafeAimHeading, TRUE, FALSE, TRUE, TRUE, TRUE)
				#ENDIF
				
				IF NOT HAS_ANY_GOLFER_TAKEN_A_SHOT_THIS_HOLE(thisFoursome)
					IF IS_CART_BLOCKING_PLAYERS_SHOT(thisFoursome, thisCourse, GET_GOLF_FOURSOME_CART(thisFoursome, 0)) OR IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_TELPORT_CART)
					OR IS_CART_FAR_FROM_BALL(thisFoursome, GET_GOLF_FOURSOME_CART(thisFoursome, 0))
						MOVE_GOLF_PLAYER_CART_TO_SAFE_LOCATION(thisFoursome, thisCourse, vSafeSpotAim, GET_GOLF_FOURSOME_CART(thisFoursome, 0), 0)
					ENDIF
					IF IS_CART_BLOCKING_PLAYERS_SHOT(thisFoursome, thisCourse,  GET_GOLF_FOURSOME_CART(thisFoursome, 1)) OR IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_TELPORT_CART)
					OR IS_CART_FAR_FROM_BALL(thisFoursome, GET_GOLF_FOURSOME_CART(thisFoursome, 1))
						MOVE_GOLF_PLAYER_CART_TO_SAFE_LOCATION(thisFoursome, thisCourse, vSafeSpotAim, GET_GOLF_FOURSOME_CART(thisFoursome, 1), 1)
					ENDIF
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = 0
						CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_TELPORT_CART)
					ENDIF
				ENDIF
				
				TELEPORT_GOLF_FOURSOME_TO_TEE_SPECTATE(thisFoursome, thisCourse, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)) //guarantees no one is near the golfer when he lines up for his shot
				MOVE_ENTITIES_AROUND_CURRENT_PLAYER(thisFoursome, thisCourse) //guarantees no one is near the golfer when he lines up for his shot
				CLEAR_AREA_AROUND_BALL(thisCourse, thisFoursome)
				
				SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, <<0,0,0>>, LIE_TEE)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome, <<0,0,-1>>)
				
				//Set focus so ball area around ball updates
				IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
					SET_FOCUS_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
					SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), GET_GOLF_HOLE_TEE_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) + <<0, 0, 0.05>>)
				ENDIF
				
				CREATE_TEE(thisFoursome, thisCourse)
				SET_ENTITY_RECORDS_COLLISIONS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), FALSE)
				FREEZE_GOLF_BALL(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), TRUE)
				SET_CURRENT_PED_WEAPON(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), WEAPONTYPE_UNARMED, TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != COMPUTER
//					CREATE_PROJECTION_LINE(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
//				ENDIF
				SET_METER_MAX_DISTANCE(thisGame, thisCourse, thisFoursome)
				SET_METER_CURRENT_DISTANCE(thisFoursome.swingMeter, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome, GOLF_FIND_SHOT_ESTIMATE(thisFoursome, thisGame.golfBag))
				CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_STARTED_SHOT_ON_GREEN)
				CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_FIRST_COLLISION)
				CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_IS_OOB)
				CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_SHOT_IS_GIMMIE)
				CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_FORCE_UPDATE)
				CLEAR_GLOBAL_GOLF_CONTROL_FLAG(GGCF_PLAYER_HOLE_IN_ONE)
				START_TIMER_NOW_SAFE(thisHelpers.inputTimer)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_ADDRESS_BALL)
				
				SET_GOLF_PLAYER_CONTROL_FLAG(thisFoursome.playercore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], GOLF_BALL_IN_FOCUS)
				CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
				GOLF_PRINT_IN_HAZARD_HELP(thisFoursome, thisCourse,  thisHelpers)
				SET_GOLF_PLAYER_LAST_LIE(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
				SET_GOLF_PLAYER_PREVIOUS_NORMAL(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
				
				CLEAR_PRINTS()
				GOLF_FREEZE_ALL_BALL_POSITIONS(thisFoursome)
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_ADDRESS | GUC_REFRESH_SWING | GUC_REFRESH_FLOATING)
				CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_USE_FILL_QUALITY | GUC_REFRESH_INPUT)
				GOLF_CLEAR_SWING_METER(thisFoursome.swingMeter)
				GOLFUI_FLOATING_INIT_STRENGTH(thisHelpers.floatingUI, thisFoursome)
				GOLFUI_FLOATING_CLEAR_SWING_DISTANCE(thisHelpers.floatingUI)
				GOLFUI_FLOATING_CLEAR_HEIGHT(thisHelpers.floatingUI)
				GOLFUI_FLOATING_SET_SLOT_TWO(thisHelpers.floatingUI, "", 0, FALSE)
				
				IF DOES_CAM_EXIST(thisHelpers.camAddress)
					DESTROY_CAM(thisHelpers.camAddress)
				ENDIF
				IF DOES_CAM_EXIST(thisHelpers.camFlight)
					DESTROY_CAM(thisHelpers.camFlight)
				ENDIF
				thisHelpers.vLastGoodAddressCamPos = <<0,0,0>>
				SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome, SAS_IDLE)
				CANCEL_TIMER(thisHelpers.uiTimer)
				CANCEL_TIMER(thisHelpers.taskTimer)
				CANCEL_TIMER(thisHelpers.idleSpeechTimer)
				
				SET_GOLF_TRAIL_INTERSECT_FLAG(FALSE)
				GOLF_SET_GOLF_TRAIL_EFFECT(thisGame, thisFoursome, thisHelpers)
				
				IF NOT IS_GOLF_FOURSOME_MP()
					SET_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_HOLE)
					SETUP_GOLF_SPECTATOR_IDLE_ANIM(thisGame, thisFoursome, thisCourse)
				ENDIF
				
				IF thisGame.bPlayoffGame
					INIT_PLAYER_PLAYOFF_SCORE(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
				ENDIF
				
				MANAGE_GOLF_BALL_BLIPS(thisFoursome, thisHelpers)
				SET_GOLF_MINIMAP(thisFoursome, thisCourse)
				DISPLAY_RADAR(TRUE)
				
				IF NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_DISPLAY_QUITTER)
					SET_SPLASH_TEXT(thisHelpers) //clear splash text
				ENDIF
				
				IF NOT IS_GOLF_FOURSOME_MP()
					ADJUST_ADDRESS_CAMERA(thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers , GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome))
					SET_CAM_ACTIVE(thisHelpers.camAddress, TRUE)
				ELSE
					RESET_GOLF_MP_APPROACH_CAM(thisHelpers)
					MANAGE_GOLF_MP_APPROACH_CAM(thisFoursome, thisGame, thisCourse, thisHelpers)
				ENDIF
				
				IF DOES_ENTITY_EXIST(GET_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
					SET_ENTITY_COLLISION(GET_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), TRUE)
				ENDIF
				
				IF SHOULD_GOLFER_DO_APPROACH_ANIMATION(thisFoursome)
					PLAY_APPROACH_BALL_ANIM(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame)
					SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_APPROACH_ANIM)
				ENDIF
			ENDIF
			#IF NOT GOLF_IS_AI_ONLY
				CDEBUG1LN(DEBUG_GOLF,"Done GPS_PLACE_BALL")
			#ENDIF
		BREAK
		
		CASE GPS_APPROACH_BALL
			IF bDebugSpew DEBUG_MESSAGE("GPS_APPROACH_BALL") ENDIF
			bControlsBall = TRUE
			CLEAR_FOCUS()
			thisHelpers.vLastSafeBallPosition = <<0, 0, 0>>
			
			IF NOT DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
				IF SHOULD_PLAYER_CREATE_GOLF_BALL()
					SNAP_BALL_TO_GROUND(thisFoursome, thisHelpers)
					SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome, CREATE_GOLF_BALL(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), GET_HEADING_BETWEEN_VECTORS(GET_GOLF_HOLE_FAIRWAY_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))))
				ENDIF
			ENDIF
			
			#IF GOLF_IS_MP
				IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
					bControlsBall = GOLF_NETWORK_REQUEST_CONTROL_OF_ALL_BALLS(thisFoursome)	
				ELSE
					GOLF_NETWORK_RELEASE_CONTROL_OF_ALL_BALLS(thisFoursome)
				ENDIF
				
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
			#ENDIF
			
			#IF NOT GOLF_IS_MP
				IF STREAMVOL_IS_VALID(thisHelpers.steamVolNextShot)
					IF NOT STREAMVOL_HAS_LOADED(thisHelpers.steamVolNextShot)
						CDEBUG1LN(DEBUG_GOLF,"Golf next shot stream vol not ready yet")
						bControlsBall = FALSE
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_GOLF,"Golf next shot stream vol not active")
				ENDIF
				IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
					IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_PLAYER_NO_SKIP)
						IF NOT IS_GOLF_PLAYER_PUTTING(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
							CDEBUG1LN(DEBUG_GOLF,"Player walked to shot - next player - HIDE PLAYER")
							SET_ENTITY_VISIBLE( GET_GOLF_PLAYER_PED(thisFoursome.playerCore[0]), FALSE)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF
						
			vSafeSpotAim = GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
			vCartPointSpot = vSafeSpotAim
			
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN
			AND GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) < 8
				vCartPointSpot = GET_GOLF_HOLE_TEE_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)+1) 
			ENDIF
			
			fSafeAimHeading = GET_HEADING_BETWEEN_VECTORS(vSafeSpotAim, GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))+90

			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
			AND bControlsBall
			IF FIND_SAFE_SHOT_AIM(fSafeAimHeading, thisFoursome, thisCourse, thisGame, thisHelpers)
				CLEAR_NEARBY_GOLF_BALLS(thisFoursome)
				CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_NEEDS_SNAP)

				REMOVE_GOLF_CLUB_WEAPON(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), TRUE)

				//move player to ball and find good spots for the carts and golf buddies
				#IF NOT GOLF_IS_MP
				PLACE_PLAYER_AT_BALL_WITH_HEADING(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), TRUE, FALSE, TRUE, TRUE, TRUE)
				#ENDIF
				
				//only do distance teleport if not local players cart
				INT iLocalPlayerCart 
				iLocalPlayerCart = GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(thisFoursome, GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome))
				
				IF IS_CART_BLOCKING_PLAYERS_SHOT(thisFoursome, thisCourse, GET_GOLF_FOURSOME_CART(thisFoursome, 0)) 
				OR IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_TELPORT_CART)
				OR (IS_CART_FAR_FROM_BALL(thisFoursome, GET_GOLF_FOURSOME_CART(thisFoursome, 0)) AND iLocalPlayerCart != 0)
					MOVE_GOLF_PLAYER_CART_TO_SAFE_LOCATION(thisFoursome, thisCourse, vCartPointSpot, GET_GOLF_FOURSOME_CART(thisFoursome, 0), 0)
				ENDIF
				IF IS_CART_BLOCKING_PLAYERS_SHOT(thisFoursome, thisCourse, GET_GOLF_FOURSOME_CART(thisFoursome, 1)) 
				OR IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_TELPORT_CART)
				OR (IS_CART_FAR_FROM_BALL(thisFoursome, GET_GOLF_FOURSOME_CART(thisFoursome, 1)) AND iLocalPlayerCart != 1)
					MOVE_GOLF_PLAYER_CART_TO_SAFE_LOCATION(thisFoursome, thisCourse, vCartPointSpot, GET_GOLF_FOURSOME_CART(thisFoursome, 1), 1)
				ENDIF
				IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = 0
					CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_TELPORT_CART)
				ENDIF
				
				//MOVE_GOLFERS_OFF_COURSE(thisFoursome, thisCourse)
				MOVE_ENTITIES_AROUND_CURRENT_PLAYER(thisFoursome, thisCourse) //guarantees no one is near the golfer when he lines up for his shot
				CLEAR_AREA_AROUND_BALL(thisCourse, thisFoursome)
				
				IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_MARKER(thisFoursome))
					CLEANUP_GOLF_FOURSOME_CURRENT_PLAYER_MARKER(thisFoursome)
				ENDIF
				
				CLEAR_PRINTS()
				CLEAR_HELP()
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_CURRENT_PED_WEAPON(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), WEAPONTYPE_UNARMED, TRUE)
//				IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != COMPUTER
//					CREATE_PROJECTION_LINE(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
//				ENDIF

				CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_IS_OOB | GOLF_BALL_FIRST_COLLISION | GOLF_SHOT_IS_GIMMIE
																			  | GOLF_STARTED_SHOT_ON_GREEN | GOLF_BALL_IS_OOB)
				
				TELEPORT_GOLFERS_TO_SPECTATE(thisFoursome, thisHelpers, thisCourse, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
				
				IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
					SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome, GOLF_FIND_SHOT_ESTIMATE(thisFoursome, thisGame.golfBag)) //GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
					SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_STARTED_SHOT_ON_GREEN)
				ELSE
					SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome, GOLF_FIND_SHOT_ESTIMATE(thisFoursome, thisGame.golfBag))
				ENDIF
				START_TIMER_NOW_SAFE(thisHelpers.inputTimer)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_ADDRESS_BALL)
				CANCEL_TIMER(thisHelpers.uiTimer)
				CANCEL_TIMER(thisHelpers.taskTimer)
				CANCEL_TIMER(thisHelpers.idleSpeechTimer)
				
				//Set focus so ball area around ball updates
				IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
					SET_FOCUS_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
					SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))
				ENDIF
				SET_GOLF_PLAYER_CONTROL_FLAG(thisFoursome.playercore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], GOLF_BALL_IN_FOCUS)
				CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_STYLE_HELP_DISPLAYED)
				GOLF_PRINT_IN_HAZARD_HELP(thisFoursome, thisCourse, thisHelpers)
				SET_GOLF_PLAYER_LAST_LIE(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
				SET_GOLF_PLAYER_PREVIOUS_NORMAL(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
				
				CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_FORCE_UPDATE)
				GOLF_FREEZE_ALL_BALL_POSITIONS(thisFoursome)
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_ADDRESS | GUC_REFRESH_SWING | GUC_REFRESH_FLOATING)
				CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_USE_FILL_QUALITY | GUC_REFRESH_INPUT)
				GOLF_CLEAR_SWING_METER(thisFoursome.swingMeter)
				
				GOLFUI_FLOATING_INIT_STRENGTH(thisHelpers.floatingUI, thisFoursome)
				GOLFUI_FLOATING_CLEAR_SWING_DISTANCE(thisHelpers.floatingUI)
				GOLFUI_FLOATING_CLEAR_HEIGHT(thisHelpers.floatingUI)
				GOLFUI_FLOATING_SET_SLOT_TWO(thisHelpers.floatingUI, "", 0, FALSE)
				
				IF DOES_CAM_EXIST(thisHelpers.camAddress)
					DESTROY_CAM(thisHelpers.camAddress)
				ENDIF
				IF DOES_CAM_EXIST(thisHelpers.camFlight)
					DESTROY_CAM(thisHelpers.camFlight)
				ENDIF
				thisHelpers.vLastGoodAddressCamPos = <<0,0,0>>
				SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome, SAS_IDLE)
				SET_GOLF_TRAIL_INTERSECT_FLAG(FALSE)
				GOLF_SET_GOLF_TRAIL_EFFECT(thisGame, thisFoursome, thisHelpers)
				
				IF NOT IS_GOLF_FOURSOME_MP()
					SET_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_HOLE)
					SETUP_GOLF_SPECTATOR_IDLE_ANIM(thisGame, thisFoursome, thisCourse)
				ENDIF
				
				MANAGE_GOLF_BALL_BLIPS(thisFoursome, thisHelpers)
				SET_GOLF_MINIMAP(thisFoursome, thisCourse)
				DISPLAY_RADAR(TRUE)
				
				IF NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_DISPLAY_QUITTER)
					SET_SPLASH_TEXT(thisHelpers) //clear splash text
				ENDIF
				
				IF NOT IS_GOLF_FOURSOME_MP()
					ADJUST_ADDRESS_CAMERA(thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers , GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome))
					SET_CAM_ACTIVE(thisHelpers.camAddress, TRUE)
				ELSE
					RESET_GOLF_MP_APPROACH_CAM(thisHelpers)
					MANAGE_GOLF_MP_APPROACH_CAM(thisFoursome, thisGame, thisCourse, thisHelpers)
				ENDIF
				
				//Replace the other balls with marker
				#IF NOT GOLF_IS_MP
					IF IS_BALL_ON_GREEN(thisCourse, thisFoursome)
						GOLF_REPLACE_BALL_WITH_MARKER(thisFoursome)
					ENDIF
					
					IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
						// Delete the flag as everyone is on the green
						CLEANUP_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))	
					ELIF NOT DOES_ENTITY_EXIST(GET_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
						//player moved off green, remake flag
						CREATE_GOLF_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
					ENDIF
				#ENDIF
				
				IF DOES_ENTITY_EXIST(GET_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
					SET_ENTITY_COLLISION(GET_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), TRUE)
				ENDIF
				
				IF SHOULD_GOLFER_DO_APPROACH_ANIMATION(thisFoursome)
					PLAY_APPROACH_BALL_ANIM(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame)
					SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_APPROACH_ANIM)
				ENDIF
			
			ENDIF
			ENDIF
		BREAK
		
		CASE GPS_APPROACH_ANIM
			GOLF_SET_GOLF_TRAIL_EFFECT(thisGame, thisFoursome, thisHelpers)
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_ANY_GOLF_HAZARD_HELP_DISPLAYED()
				CLEAR_HELP()
			ENDIF
			IF IS_MESSAGE_BEING_DISPLAYED()
			//AND NOT IS_ANY_GOLF_HAZARD_HELP_DISPLAYED()
				CLEAR_PRINTS()
			ENDIF
			
			IF IS_GOLF_APPROACH_ANIM_OVER(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame, 0.775)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_ADDRESS_BALL)
			ENDIF
		BREAK
		
		CASE GPS_ADDRESS_BALL
				
			IF thisFoursome.swingMeter.meterState = SWING_METER_FALSE_START
				thisFoursome.swingMeter.meterState = SWING_METER_INACTIVE
			ENDIF
									
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF
			
			SET_GLOBAL_GOLF_CONTROL_FLAG(GGCF_PLAYER_ADDRESSING_BALL)

//			uncomment when you want to adjust map position
			#IF GOLF_IS_MP
				SET_GOLF_MINIMAP_DATA(GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), IS_BALL_IN_RANGE_OF_GREEN(thisCourse, thisFoursome))
				SET_RADAR_ZOOM(CEIL(1100*MINIMAP_ZOOM_RATIO))
				LOCK_MINIMAP_POSITION(MINIMAP_POS_X, MINIMAP_POS_Y)
				LOCK_MINIMAP_ANGLE(MINIMAP_ANGLE)
				
				IF NOT DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
					CDEBUG1LN(DEBUG_GOLF,"In adress mode and current ball isn't created?")
					IF SHOULD_PLAYER_CREATE_GOLF_BALL()
						CDEBUG1LN(DEBUG_GOLF,"Try to create it")
						IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome) = 0
							SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome, CREATE_GOLF_BALL(GET_GOLF_HOLE_TEE_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) + <<0,0, 0.05>>, GET_HEADING_BETWEEN_VECTORS(GET_GOLF_HOLE_FAIRWAY_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))))
						ELSE
							SNAP_BALL_TO_GROUND(thisFoursome, thisHelpers)
							SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome, CREATE_GOLF_BALL(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), GET_HEADING_BETWEEN_VECTORS(GET_GOLF_HOLE_FAIRWAY_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))))
						ENDIF
					ENDIF
				ENDIF
			#ENDIF
			
			IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
			AND DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
				FREEZE_GOLF_BALL(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), TRUE)
			ENDIF
			
			GOLF_MANAGE_PREVIEW_LINE_UPDATE(thisGame, thisCourse, thisFoursome, thisHelpers)
			MANAGE_GOLF_BALL_BLIPS(thisFoursome, thisHelpers)
			
			#IF NOT GOLF_IS_MP
				GOLF_CHECK_PLAYER_NEAR_BALL(thisGame, thisFoursome)
			#ENDIF
				
			thisFoursome.vPositionBeforeReactionCam = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
			thisFoursome.fHeaingBeforeReactionCamera = GET_ENTITY_HEADING(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
			
			GOLF_MANAGE_ADDRESS_UI(thisGame, thisCourse, thisFoursome, thisHelpers)
			IF NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_DISPLAY_QUITTER)
				CLEAR_GOLF_SPLASH()
			ENDIF
			
			//make sure everyone, even spectators clear there turn off end scoreboard flag
			CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
			CLEAR_GOLF_STREAMING_FLAG(thisHelpers, GSF_STREAM_CUTBACK_AFTER_SHOT)
			CLEAR_GOLF_STREAMING_FLAG(thisHelpers, GSF_REACTION_CAMERA_SET_UP)
			
			#IF GOLF_IS_MP
			
				IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_NETWORK
					CANCEL_TIMER(thisHelpers.cameraTimer)
					SET_GOLF_PED_IDLE_ANIM(thisFoursome, GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome))
				ENDIF
				
				//Make club
				IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_LOCAL_MP
				AND NOT DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome))
					CDEBUG1LN(DEBUG_GOLF,"No Club? Create one")
					GOLF_ATTACH_CLUB_TO_PLAYER(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag)
				ENDIF
				
				IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
					bControlsBall = GOLF_NETWORK_REQUEST_CONTROL_OF_ALL_BALLS(thisFoursome)
					IF NOT bControlsBall
						CDEBUG1LN(DEBUG_GOLF, "In address mode but don't own all balls? Wait.")
						EXIT
					ENDIF
				ELSE
					GOLF_NETWORK_RELEASE_CONTROL_OF_ALL_BALLS(thisFoursome)
				ENDIF
				
				IF MANAGE_GOLF_MP_APPROACH_CAM(thisFoursome, thisGame, thisCourse, thisHelpers)
					CLEANUP_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers)
					
					EXIT
				ENDIF
				
				GOLF_END_REACTION_SYNC_SCENE()
			#ENDIF
			
			IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_IN(250)
			ENDIF
			
			MANAGE_DISPLAY_TIE_MESSAGE(thisCourse, thisFoursome, thisHelpers, thisGame)

			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
				IF bDebugSpew DEBUG_MESSAGE("GPS_ADDRESS_BALL") ENDIF

				SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome)
					CASE HUMAN_LOCAL
					CASE HUMAN_LOCAL_MP
						START_TIMER_NOW(thisHelpers.uiTimer)
						IF IS_SHOT_A_GIMME(thisCourse, thisFoursome) // Auto-shot within 1m
						OR IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PLAYER_TIME_OUT)
							CLEAR_HELP()
							SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GOLF_MANAGE_ADDRESS_AI()) // The input manager here will change state - be careful!
						ELSE
							#IF NOT GOLF_IS_MP
								GOLF_MANAGE_ADDRESS_BALL_DIALOGUE(thisFoursome, thisHelpers)
							#ENDIF
						
							IF IS_GOLF_TRAIL_INTERSECT_FLAG_SET() OR IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
								INITIALIZE_FLOATING_HELP_FOR_SHOT(thisFoursome, thisHelpers, thisCourse)
							ELSE
								SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_FLOATING)
							ENDIF
							
							GOLF_MANAGE_ADDRESS_HELP(thisCourse, thisFoursome, thisHelpers, thisGame, thisPlayer)
							SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GOLF_MANAGE_ADDRESS_INPUT(thisGame, thisCourse, thisFoursome, thisHelpers)) // The input manager here will change state - be careful!
						ENDIF
					BREAK
					CASE COMPUTER
						START_TIMER_NOW(thisHelpers.taskTimer)
						
						IF TIMER_DO_ONCE_WHEN_READY(thisHelpers.taskTimer, 1.0) //give the ai a second before it stats its swing
							SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GOLF_MANAGE_ADDRESS_AI()) // The input manager here will change state - be careful!
						ENDIF
												
						IF TIMER_DO_WHEN_READY(thisHelpers.taskTimer, 0.5) //lag before player is allowed to skip shot
							GOLF_MANAGE_SKIP_AI_INPUT(thisGame, thisFoursome, thisCourse, thisHelpers)
						ENDIF
					BREAK
				ENDSWITCH

				GOLF_MANAGE_SWING_ANIMS(thisGame, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisFoursome.swingMeter, thisHelpers)
				IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome) //IS_BALL_ON_GREEN(thisCourse, thisFoursome)
					//SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome, GOLF_FIND_SHOT_ESTIMATE(thisFoursome, thisGame.golfBag))
					IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
						IF NOT ARE_VECTORS_ALMOST_EQUAL( GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome), <<0,0,0>>)
						AND NOT IS_SHOT_A_GIMME(thisCourse, thisFoursome)
							//DRAW_GOLF_ESTIMATE_RING(GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
							DRAW_GOLF_ARROW(GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)),
											GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), thisHelpers.bShotPreviewCam)
							DRAW_GOLF_TERRAIN_GRID(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
						ENDIF
						GOLF_TRAIL_SET_ENABLED(FALSE)
					ELSE
						IF DOES_BLIP_EXIST(GET_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers))
							CLEANUP_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers)
						ENDIF
					ENDIF
				ELSE
					IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
						TERRAINGRID_ACTIVATE(FALSE)
						IF NOT ARE_VECTORS_ALMOST_EQUAL( GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome), <<0,0,0>>)
							GOLF_DRAW_GOLF_TRAIL_EFFECT(thisHelpers, thisFoursome)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(GET_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers))
							CLEANUP_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers)
						ENDIF
					ENDIF
				ENDIF
			ELSE //player = HUMAN_NETWORK
				GOLF_MANAGE_ADDRESS_SPECTATOR_INPUT(thisHelpers)
				ADJUST_ADDRESS_CAMERA(thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers , GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome))
				IF NOT IS_CAM_RENDERING(thisHelpers.camAddress)
					SET_CAM_ACTIVE(thisHelpers.camAddress, TRUE)
				ENDIF
				IF DOES_CAM_EXIST(thisHelpers.camFlight)
					DESTROY_CAM(thisHelpers.camFlight)
				ENDIF
			ENDIF
			thisHelpers.fPlayerHeadingAtShot = GET_ENTITY_HEADING(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
			
			IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
				IF IS_GOLF_TRAIL_INTERSECT_FLAG_SET() OR GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers)
				OR IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
					VECTOR vEstBlipPoint
					vEstBlipPoint = PICK_VECTOR(IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome), thisHelpers.golfTrail.checkPointPos)
					MANAGE_CURRENT_ESTIMATE_BLIP(thisHelpers, vEstBlipPoint, 
												GET_GOLF_PLAYER_BLIP_COLOUR(GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)), GET_BLIP_SPRITE_FOR_SHOT_ESTIMATE(thisFoursome, vEstBlipPoint))		
					
					
					IF NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SET_HELPER_BLIP_ADDRESS_NAME)
						SET_BLIP_NAME_FROM_TEXT_FILE(GET_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers), "SHOT_EST")
						SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_SET_HELPER_BLIP_ADDRESS_NAME)
					ENDIF
				ENDIF
			ELSE
				//CDEBUG1LN(DEBUG_GOLF,"Cleaning up estimate")
				CLEANUP_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers)
			ENDIF
			
			GOLF_MANAGE_SWING_UI(thisCourse, thisGame, thisHelpers, thisFoursome.swingMeter, thisPlayer, thisFoursome)
			GOLF_MANAGE_AMBIENT_SPEECH(thisGame, thisHelpers, thisCourse, thisFoursome, FALSE, FALSE, FALSE)
			
			IF NOT IS_GOLF_FOURSOME_MP()
				MANAGE_GOLF_SPECTATOR_IDLE_ANIM(thisFoursome)
			ENDIF
			
			MANAGE_ADRESS_INPUT_DISPLAY(thisFoursome, thisHelpers, thisCourse)
			
//			//debug for testing approach animation			
//			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LB)
//				PLAY_APPROACH_BALL_ANIM(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame)
//				SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_APPROACH_ANIM)
//			ENDIF
		BREAK
		
		CASE GPS_SWING_METER

			IF NOT bPerfectAccuracyMode
				GOLF_TRAIL_SET_ENABLED(FALSE)
				TERRAINGRID_ACTIVATE(FALSE)
			ENDIF
			SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_GOLF_TRAIL)
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
				IF bDebugSpew DEBUG_MESSAGE("GPS_SWING_METER") ENDIF
				IF DOES_BLIP_EXIST(GET_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers))
					CLEANUP_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers)
				ENDIF
				SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome)
					CASE HUMAN_LOCAL
					CASE HUMAN_LOCAL_MP
						IF IS_SHOT_A_GIMME(thisCourse, thisFoursome) // Auto-shot within 1m
						OR IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PLAYER_TIME_OUT)
							IF IS_SHOT_A_GIMME(thisCourse, thisFoursome)
								SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_SHOT_IS_GIMMIE)
							ELIF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PLAYER_TIME_OUT)
								SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_SWING)
							ENDIF

							GOLF_MANAGE_SWING_AI(thisFoursome, thisGame, thisFoursome.swingMeter, IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_SHOT_IS_GIMMIE))
						ELSE
							GOLF_MANAGE_SWING_INPUT(thisGame, thisFoursome, thisHelpers)
						ENDIF
					BREAK
					CASE COMPUTER
						//hard ai always hits perfect shots, normal hits perfect shots on green
						GOLF_MANAGE_SWING_AI(thisFoursome, thisGame, thisFoursome.swingMeter, 
											 IS_GOLF_CURRENT_PLAYER_HARD_AI(thisFoursome) OR 
											 (GET_GOLF_FOURSOME_CURRENT_PLAYER_AI_DIFFICULTY(thisFoursome) = GOLF_AI_NORMAL AND IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)), 
											 IS_GOLF_CURRENT_PLAYER_HARD_AI(thisFoursome) AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome))
						
						#IF NOT GOLF_IS_AI_ONLY //ai golfers in the players foursome get to use the special 'guarantee gimmie shot' algorithm
							IF IS_SHOT_A_GIMME(thisCourse, thisFoursome) 
								SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_SHOT_IS_GIMMIE)
							ENDIF
						#ENDIF
						
						GOLF_MANAGE_SKIP_AI_INPUT(thisGame, thisFoursome, thisCourse, thisHelpers)
						SETUP_GOLF_SPECTATE_COMPUTER_CONTROLS(thisHelpers)
					BREAK
				ENDSWITCH
				GOLF_MANAGE_SWING_ANIMS(thisGame, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisFoursome.swingMeter, thisHelpers)
				
				IF TIMER_DO_ONCE_WHEN_READY(thisHelpers.resetTimer, 8.0)
					CDEBUG1LN(DEBUG_GOLF,"Stuck in GPS_SWING_METER")
					thisFoursome.swingMeter.meterState = SWING_METER_FALSE_START 
				ENDIF
				
				IF thisFoursome.swingMeter.meterState = SWING_METER_FINISHED OR thisFoursome.swingMeter.meterState = SWING_METER_SHANK
					IF NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_AI_HIT_BALL_THIS_FRAME)
						IF HAS_SWING_ANIM_REACHED_BALL(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome)) 
						AND DOES_ENTITY_HAVE_PHYSICS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
							IF GOLF_HIT_BALL(thisGame, thisCourse, thisFoursome, thisHelpers)
								SET_GOLF_UI_FLAG(thisHelpers, GUC_USE_FILL_QUALITY)
								INITIALIZE_FLOATING_HELP_FOR_FLIGHT(thisFoursome, thisHelpers)
								START_TIMER_NOW_SAFE(thisHelpers.cameraTimer)
								CANCEL_TIMER(thisHelpers.resetTimer)
								thisFoursome.swingMeter.meterState = SWING_METER_INACTIVE
								SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
								CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_INPUT)
								CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_FLIGHT)
								DELETE_TEE(thisFoursome)
								SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_BALL_IN_FLIGHT)
								CLEAR_GLOBAL_GOLF_CONTROL_FLAG(GGCF_PLAYER_ADDRESSING_BALL)
								thisHelpers.iNumTimesFallThroughWorldCheckFailed = 0
									
								IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
									START_AUDIO_SCENE("GOLF_FLY_CAM")
								ENDIF
								
								IF GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome) = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
									INC_GOLF_STAT_NUM_SHOTS(GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
								ENDIF
								
								IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_WAS_PERFECT_HIT)
									PLAY_GOLF_SPLASH("PERFECT_HIT", "", GOLF_SPLASH_SHARD_MIDSIZED, HUD_COLOUR_YELLOW, 1500)
									PLAY_GOLF_SPLASH_SOUND(GOLF_SPLASH_PERFECT_HIT)
								ENDIF
							ELSE
								// We didnt hit the ball (force <<0,0,0>>) so lets false start instead
								thisFoursome.swingMeter.meterState = SWING_METER_INACTIVE
								GOLF_CLEAR_SWING_METER(thisFoursome.swingMeter)
								CDEBUG1LN(DEBUG_GOLF, "Player has missed golf ball. Setting foursome state to GPS_ADDRESS_BALL")
								SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_ADDRESS_BALL)
							ENDIF
							SET_GOLF_CONTROL_FLAG( thisGame, GCF_AI_HIT_BALL_THIS_FRAME)
						ENDIF
					ENDIF
				ELIF thisFoursome.swingMeter.meterState = SWING_METER_FALSE_START 
//					IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != COMPUTER
//						CREATE_PROJECTION_LINE(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
//					ENDIF
					thisFoursome.swingMeter.meterState = SWING_METER_INACTIVE
					SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_ADDRESS_BALL)
				ENDIF
			ELSE
				CANCEL_TIMER(thisHelpers.cameraTimer)
				GOLF_MANAGE_ADDRESS_SPECTATOR_INPUT(thisHelpers)
				ADJUST_ADDRESS_CAMERA(thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers , GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome))
				IF NOT IS_CAM_RENDERING(thisHelpers.camAddress)
					SET_CAM_ACTIVE(thisHelpers.camAddress, TRUE)
				ENDIF
			ENDIF
			GOLF_MANAGE_SWING_UI(thisCourse, thisGame, thisHelpers, thisFoursome.swingMeter, thisPlayer, thisFoursome)
			GOLF_MANAGE_ADDRESS_UI(thisGame, thisCourse, thisFoursome, thisHelpers)
		BREAK
		
		CASE GPS_BALL_IN_FLIGHT
			
			IF bDebugSpew DEBUG_MESSAGE("GPS_BALL_IN_FLIGHT") ENDIF
			
			MANAGE_GOLF_BALL_BLIPS(thisFoursome, thisHelpers)
			
			IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
				IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
					GOLF_MANAGE_BALL_IN_FLIGHT(thisGame, thisCourse, thisFoursome, thisHelpers)
					SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome)
						CASE HUMAN_LOCAL
						CASE HUMAN_LOCAL_MP
							GOLF_MANAGE_FLIGHT_INPUT(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisFoursome.swingMeter, thisGame, thisHelpers)
							GOLF_MANAGE_PLAYER_FLAGS(thisGame, thisCourse, thisFoursome, thisPlayer, thisHelpers)
						BREAK
						CASE COMPUTER
							GOLF_MANAGE_FLIGHT_AI(thisFoursome.swingMeter)
						BREAK
					ENDSWITCH
				ENDIF
				IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET( thisFoursome, GOLF_BALL_AT_REST)
					FLOAT fCameraDelayTime //delay the flight camera a bit
					fCameraDelayTime = PICK_FLOAT( IS_BALL_IN_RANGE_OF_GREEN(thisCourse, thisFoursome), 0.65, 1.0 )
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_NETWORK
						IF NOT IS_TIMER_STARTED(thisHelpers.cameraTimer)
							START_TIMER_AT(thisHelpers.cameraTimer, 1.1)
						ENDIF
						IF NOT TIMER_DO_WHEN_READY(thisHelpers.cameraTimer, 2.0)
						AND TIMER_DO_WHEN_READY(thisHelpers.cameraTimer, 1.0)
							GOLFUI_FLOATING_INIT_SHOT_STRENGTH(thisHelpers.floatingUI, thisFoursome)
							GOLF_MANAGE_SWING_UI(thisCourse, thisGame, thisHelpers, thisFoursome.swingMeter, thisPlayer, thisFoursome)
							GOLF_MANAGE_ADDRESS_UI(thisGame, thisCourse, thisFoursome, thisHelpers)
						ELSE
							DISPLAY_RADAR(TRUE)
							SET_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_HOLE)
							RESTART_TIMER_NOW(thisHelpers.cameraTimer)
							GOLF_MANAGE_FLIGHT_UI(thisHelpers, thisGame, thisCourse,thisFoursome)
							GOLF_MANAGE_FLIGHT_CAMERA(thisGame, thisFoursome, thisCourse, thisHelpers, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
						ENDIF
					ELIF NOT TIMER_DO_WHEN_READY(thisHelpers.cameraTimer, fCameraDelayTime) 
					AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET( thisFoursome, GOLF_BALL_FIRST_COLLISION) // We don't want to go to the real flight scene for a bit
						//DEBUG_MESSAGE("GPS_BALL_IN_FLIGHT - first bit, end swing cutscene?")
						IF NOT TIMER_DO_WHEN_READY(thisHelpers.cameraTimer, fCameraDelayTime*0.95) 
							GOLF_MANAGE_SWING_UI(thisCourse, thisGame, thisHelpers, thisFoursome.swingMeter, thisPlayer, thisFoursome)
							GOLF_MANAGE_ADDRESS_UI(thisGame, thisCourse, thisFoursome, thisHelpers)
							GOLFUI_FLOATING_UPDATE_HIT_DISTANCE(thisHelpers.floatingUI, thisFoursome)
						ELSE
							GOLF_MANAGE_FLIGHT_UI(thisHelpers, thisGame, thisCourse,thisFoursome) // start using the flight ui a little before cutting to the flight camera
						ENDIF
					ELSE 
						GOLF_MANAGE_FLIGHT_UI(thisHelpers, thisGame, thisCourse,thisFoursome)
						GOLF_MANAGE_FLIGHT_CAMERA(thisGame, thisFoursome, thisCourse, thisHelpers,thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
					ENDIF
				ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
					GOLF_MANAGE_FLIGHT_UI(thisHelpers, thisGame, thisCourse,thisFoursome)
					//GOLF_MANAGE_FLIGHT_CAMERA(thisGame, thisHelpers,thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
					START_TIMER_NOW_SAFE(thisHelpers.cameraTimer)
					GOLF_CLEAR_SWING_METER(thisFoursome.swingMeter)
					//DO_NAVIGATION_TO_NEXT_SHOT(thisCourse, thisFoursome, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome), FALSE)
				ENDIF
			ELSE
				IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
					GOLF_MANAGE_BALL_ON_GREEN(thisGame, thisCourse, thisFoursome, thisHelpers)
					GOLF_MANAGE_PLAYER_FLAGS(thisGame, thisCourse, thisFoursome, thisPlayer, thisHelpers)
					IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET( thisFoursome, GOLF_BALL_AT_REST)
						START_TIMER_NOW_SAFE(thisHelpers.cameraTimer)
						GOLF_CLEAR_SWING_METER(thisFoursome.swingMeter)
						//DO_NAVIGATION_TO_NEXT_SHOT(thisCourse, thisFoursome, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome), FALSE)
					ENDIF
					
					GOLF_MANAGE_PLAYER_FLAGS(thisGame, thisCourse, thisFoursome, thisPlayer, thisHelpers)
					GOLF_MANAGE_FLIGHT_UI(thisHelpers, thisGame, thisCourse,thisFoursome)
					GOLF_MANAGE_GREEN_CAMERA(thisGame, thisHelpers, thisCourse, thisFoursome)	
				ELSE
					GOLFUI_FLOATING_INIT_SHOT_STRENGTH(thisHelpers.floatingUI, thisFoursome)
					IF NOT IS_TIMER_STARTED(thisHelpers.cameraTimer)
						START_TIMER_NOW_SAFE(thisHelpers.cameraTimer)
					ENDIF
					
					GOLF_MANAGE_PLAYER_FLAGS(thisGame, thisCourse, thisFoursome, thisPlayer, thisHelpers)
					GOLF_MANAGE_FLIGHT_UI(thisHelpers, thisGame, thisCourse,thisFoursome)
					IF NOT IS_SHOT_A_GIMME(thisCourse, thisFoursome)
						GOLF_MANAGE_GREEN_CAMERA(thisGame, thisHelpers, thisCourse, thisFoursome)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
			AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_FIRST_COLLISION)
				IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_REFRESH_INPUT)
					SETUP_GOLF_BALL_IN_FLIGHT_CONTROLS(thisHelpers)
					SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_INPUT)
				ENDIF
				GOLF_DISPLAY_CONTROLS(thisHelpers, TRUE)
			ENDIF
			
			IF IS_MESSAGE_BEING_DISPLAYED()
				CLEAR_PRINTS()
			ENDIF
			
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
				
			IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
			
					IF NOT STREAMVOL_IS_VALID(thisHelpers.steamVolNextShot)
						thisHelpers.steamVolNextShot = STREAMVOL_CREATE_LINE(GET_GOLF_HOLE_TEE_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), 
															    			 GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), FLAG_COLLISIONS_MOVER)
					ENDIF
				
				SET_FOCUS_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
				SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_IN_FOCUS)
			ENDIF
			
			IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
			AND IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
				MANAGE_CURRENT_ESTIMATE_BLIP(thisHelpers, GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), 
											GET_GOLF_PLAYER_BLIP_COLOUR(GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)))
				
				IF NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_SET_HELPER_BLIP_IN_FLIGHT_NAME)
					SET_BLIP_NAME_WITH_PLAYER_ID(thisFoursome, GET_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers), GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
					SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_SET_HELPER_BLIP_IN_FLIGHT_NAME)
				ENDIF
			ENDIF
			
			GOLF_PLAY_BALL_ROLLING_SOUND(thisFoursome, thisHelpers)
			GOLF_MANAGE_BALL_COLLISION(thisFoursome)
			GOLF_MANAGE_BALL_PLAYER_COLLISION(thisFoursome)
			
		BREAK
		CASE GPS_BALL_AT_REST
//			#IF NOT GOLF_IS_AI_ONLY #IF IS_DEBUG_BUILD 
//			SET_GAME_PAUSED(bGolfPause)
//			#ENDIF #ENDIF
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
				IF bDebugSpew DEBUG_MESSAGE("GPS_BALL_AT_REST") ENDIF
				
				//DO_NAVIGATION_TO_NEXT_SHOT(thisCourse, thisFoursome, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome), FALSE)
				//CLEANUP_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome)
				CLEAR_CONTROL_FLAGS_BALL_AT_REST(thisFoursome, thisGame,  thisHelpers)
				GOLF_MANAGE_POST_SHOT_ANIMATIONS(thisGame, thisHelpers, thisCourse, thisFoursome)
				
				IF NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_STREAM_CUTBACK_AFTER_SHOT)
				AND NOT IS_PED_INJURED(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
					SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_STREAM_CUTBACK_AFTER_SHOT)
					
					IF STREAMVOL_IS_VALID(thisHelpers.steamVolNextShot)
						STREAMVOL_DELETE(thisHelpers.steamVolNextShot)
					ENDIF
					
					thisHelpers.steamVolNextShot = STREAMVOL_CREATE_SPHERE(GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome)), 10.0, FLAG_MAPDATA)
				ENDIF
				
				IF (TIMER_DO_WHEN_READY(thisHelpers.cameraTimer, 0.75)
				AND STREAMVOL_HAS_LOADED(thisHelpers.steamVolNextShot))
				OR TIMER_DO_WHEN_READY(thisHelpers.cameraTimer, 2.0)
					
					STOP_CAM_POINTING(thisHelpers.camFlight)
					INCREMENT_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome)
					INCREMENT_GOLF_PLAYER_HOLE_SCORE(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), 1)
					#IF NOT GOLF_IS_MP
						SET_PED_GRAVITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), TRUE)
					#ENDIF
					
					//don't worry about this if you are over the shot limit
					IF NOT IS_SHOT_LIMIT_EXCEEDED(thisFoursome, thisCourse)
						IF IS_GOLF_BALL_OOB(thisFoursome, thisCourse, OOBReason)
							CDEBUG1LN(DEBUG_GOLF,"Golf ball is out of bounds! Reason enum : ", OOBReason)
						
							goodShot = FALSE
							bLieUnplayable = FALSE
							bInWater = IS_BALL_IN_WATER_HAZARD(thisFoursome) //this function will get reset before I do the check for what splash text to use
							INCREMENT_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome)
							INCREMENT_GOLF_PLAYER_HOLE_SCORE(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), 1)
							
							//in case going oob causes you to go over the shot limit
							IF NOT IS_SHOT_LIMIT_EXCEEDED(thisFoursome, thisCourse)
								//Ball is on uplayable lie, Move ball from its bad position 
								IF (IS_GOLF_LIE_UNPLAYABLE(GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)) OR OOBReason = OOB_RESTRICTIVE_POSITION)
								AND NOT IS_BALL_OUTSIDE_BOUNDARY_PLANE_OR_BOUNDING_SPHERES(thisFoursome) // if you are outside the boundaries it don't allow unplayable lie rule
									IF NOT GOLF_FIND_SAFE_SPOT_TO_DROP_BALL(thisFoursome, thisCourse, GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)))
										GOLF_RESET_BALL_AT_REST_HAZARD(thisFoursome)
									ELSE
										bLieUnplayable = TRUE
										IF GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome) = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
											SET_GOLF_HELP_DISPLAYED_FLAG(thisHelpers, GHD_DISPLAY_UNPLAYABLE_LIE_HELP)
										ENDIF
										IF OOBReason = OOB_RESTRICTIVE_POSITION
											SET_SPLASH_TEXT(thisHelpers, GOLF_SPLASH_OBSTRUCTED, "LIE_UNPLAYABLE", "LIE_OBSTRUCT")
										ELSE
											SET_SPLASH_TEXT(thisHelpers, GOLF_SPLASH_UNPLAYABLE, "LIE_UNPLAYABLE", "")
										ENDIF
									ENDIF
								ELSE
									GOLF_RESET_BALL_AT_REST_HAZARD(thisFoursome)
								ENDIF
							ENDIF
							
							IF NOT bLieUnplayable
								IF bInWater
									SET_SPLASH_TEXT(thisHelpers, GOLF_SPLASH_WATER, "LIE_WATER", "")
								ELSE
									SET_SPLASH_TEXT(thisHelpers, GOLF_SPLASH_OOB, "LIE_UNKNOWN", "")
								ENDIF
							ENDIF
							
							SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_USE_HARD_AI)
						ELSE
							goodShot = TRUE
							GOLF_RESET_BALL_AT_REST(thisCourse, thisFoursome)
						ENDIF
					ENDIF
					
					//over shot limit, end hole
					IF  IS_SHOT_LIMIT_EXCEEDED(thisFoursome, thisCourse)
						CLEAR_FOCUS()
						goodShot = FALSE
						SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_IN_HOLE)
						
						SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_BALL_IN_HOLE_CELEBRATION)
						SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_STROKE_LIMIT)
						CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_FIRST_COLLISION)
					ELSE
						SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_POST_SHOT_REACTION)
					ENDIF
					
					//the lie is cup when the ball isn't in the hole. The ball probably skimed the cup, change lie back to green
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_CUP
						SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, <<0,0,0>>, LIE_GREEN)
					ENDIF
					
					GOLF_MANAGE_AMBIENT_SPEECH(thisGame, thisHelpers, thisCourse, thisFoursome, TRUE, !goodShot, FALSE)
                                                                           
					IF NOT IS_GOLF_REACTION_ALLOWED(thisFoursome, thisHelpers, thisCourse)
						SET_CAM_ACTIVE(thisHelpers.camFlight, TRUE)
					ELSE
						IF GOLF_MANAGE_REACTION_CAMERA(thisHelpers, thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
							SET_CAM_ACTIVE(thisHelpers.camReaction, TRUE)
						ENDIF
					ENDIF
				ENDIF
				GOLF_MANAGE_FLIGHT_UI(thisHelpers, thisGame, thisCourse,thisFoursome)
			ELSE
				IF NOT IS_BALL_ON_GREEN(thisCourse, thisFoursome)
					GOLF_MANAGE_FLIGHT_CAMERA(thisGame, thisFoursome, thisCourse, thisHelpers,thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
				ELSE
					GOLF_MANAGE_GREEN_CAMERA(thisGame, thisHelpers, thisCourse, thisFoursome)
				ENDIF
				
				STOP_CAM_POINTING(thisHelpers.camFlight)
				
				//CDEBUG1LN(DEBUG_GOLF,"Camera time ", GET_TIMER_IN_SECONDS(thisHelpers.cameraTimer))
				IF TIMER_DO_ONCE_WHEN_READY(thisHelpers.cameraTimer, 0.50)// OR IS_CUTSCENE_SKIP_BUTTON_PRESSED()
//					GOLF_MANAGE_REACTION_CAMERA(thisHelpers, thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
				ELIF IS_TIMER_STARTED(thisHelpers.cameraTimer)
					GOLF_MANAGE_FLIGHT_UI(thisHelpers, thisGame, thisCourse,thisFoursome)
				ENDIF
				CLEAR_FOCUS()
			ENDIF
			
			IF DOES_CAM_EXIST(thisHelpers.camAddress)
				DESTROY_CAM(thisHelpers.camAddress)
			ENDIF
			
			#IF GOLF_IS_MP
				CANCEL_TIMER(thisHelpers.navTimer)
				
				RESET_GOLF_MP_APPROACH_CAM(thisHelpers)
			#ENDIF
			
			IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
			AND IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
				MANAGE_CURRENT_ESTIMATE_BLIP(thisHelpers, GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), 
											GET_GOLF_PLAYER_BLIP_COLOUR(GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)))
			ENDIF
				
			GOLF_MANAGE_BALL_COLLISION(thisFoursome)
		BREAK
		CASE GPS_POST_SHOT_REACTION
		IF bDebugSpew DEBUG_MESSAGE("GPS_POST_SHOT_REACTION") ENDIF	
			
			BOOL bIsReactionAllowed
			bIsReactionAllowed = IS_GOLF_REACTION_ALLOWED(thisFoursome, thisHelpers, thisCourse)

			#IF GOLF_IS_MP
				RESET_GOLF_MP_APPROACH_CAM(thisHelpers)
				
//				IF NOT bIsReactionAllowed //no reaction animation is playing
//				AND DOES_CAM_EXIST(GET_RENDERING_CAM())
//					thisHelpers.vMPApproachStartPos = GET_CAM_COORD(GET_RENDERING_CAM())
//					thisHelpers.vMPApproachStartRot  = GET_CAM_ROT(GET_RENDERING_CAM())
//				ENDIF
			#ENDIF
						
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
				//wait until reaction animation or splash text is over beforw monving to next shot
				IF bIsReactionAllowed OR IS_GOLF_SPLASH_DISPLAYING()
				OR IS_GOLF_CAM_RENDERING(thisHelpers.camReaction)
				
					//returns true if playing animation
					IF GOLF_MANAGE_POST_SHOT_ANIMATIONS(thisGame, thisHelpers, thisCourse, thisFoursome) // AND NOT IS_CUTSCENE_SKIP_BUTTON_PRESSED()
					OR (IS_GOLF_SPLASH_DISPLAYING() AND NOT IS_GOLF_CAM_ACTIVE(thisHelpers.camReaction))
						//fade out putt meter if its still up
						SET_PUTTMETER (thisHelpers, thisHelpers.golfUI, FALSE, TRUE)
						EXIT
					ENDIF
				ENDIF

				thisFoursome.sCurrentReactionAnimation = ""
				IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_IS_OOB)
				OR GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_CUP //in hole get rid of ball now
				OR IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_STROKE_LIMIT) //over shot limit
					#IF NOT GOLF_IS_MP
						CLEANUP_GOLF_BALL(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
					#ENDIF
					CLEANUP_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers)
				ENDIF
				
				IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_CUP OR IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_STROKE_LIMIT)
					SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_DONE_WITH_HOLE)
					CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_STROKE_LIMIT)
				ELSE
					SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_DONE_WITH_SHOT)
				ENDIF
				
				IF NOT IS_GOLF_FOURSOME_MP()
					IF ARE_ALL_PLAYERS_DONE_WITH_HOLE(thisFoursome) //load view for end of hole camera
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							NEW_LOAD_SCENE_STOP()
						ENDIF
						//VECTOR vLoadScenePos, vLoadSceneRot
						//FLOAT FOV
					//	vLoadScenePos = GET_END_OF_HOLE_CAMERA_POS(GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), thisHelpers.iLastEndCameraIndex, vLoadSceneRot, FOV)
						//vLoadSceneRot = CONVERT_GOLF_ROTATION_TO_DIRECTION_VECTOR(vLoadSceneRot)
						
						//NEW_LOAD_SCENE_START(vLoadScenePos, vLoadSceneRot, 100)
					ENDIF
					
					//only cut back to the player under certain conditions
					IF GOLF_CUT_BACK_TO_PLAYER(thisFoursome, thisCourse, thisGame)
						CLEAR_FOCUS()
						
						IF NOT ARE_ALL_PLAYERS_DONE_WITH_HOLE(thisFoursome)
							IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_IS_OOB)
							OR  (GOLF_IS_PLAYER_APPROACHING_BALL(thisFoursome.playerCore[0]) AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) != GPS_DONE_WITH_HOLE)
								SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_APPROACH_BALL)
							ELSE
								IF SHOULD_PLAYER_WALK_AFTER_SHOT(PLAYER_PED_ID())
								AND GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
								AND GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome) = 1
									CDEBUG1LN(DEBUG_GOLF,"Force Player Walk")
									SET_GOLFER_NAV_HEADING(thisFoursome, thisCourse, IS_GOLF_REACTION_ALLOWED(thisFoursome, thisHelpers, thisCourse))
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
									SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK)
								ELSE
									SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)))
									SET_GOLFER_NAV_HEADING(thisFoursome, thisCourse, IS_GOLF_REACTION_ALLOWED(thisFoursome, thisHelpers, thisCourse))
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								ENDIF
								
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
								FORCE_INSTANT_LEG_IK_SETUP(PLAYER_PED_ID())
								CDEBUG1LN(DEBUG_GOLF,"Cuting back to player")
								
								INT iPlayerIndex
								REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
									//dont clear the local players task, unless he wasn't suppose to be playing reaction
									IF GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome) != iPlayerIndex
										CLEAR_PED_TASKS_IMMEDIATELY(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayerIndex))
										CDEBUG1LN(DEBUG_GOLF,"Golf Force Ped Update Cut To Player")
										FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iPlayerIndex), TRUE)
									ENDIF
								ENDREPEAT
								
								DISABLE_GOLF_MINIMAP()
								CLEAR_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_SHOT | GOLF_DISPLAY_HOLE)
								CLEAR_GOLF_SPLASH()
								GOLF_MONITOR_CLUB_WEAPONS(thisFoursome, thisGame, TRUE)
								
								SETUP_GOLF_NAV_CONTROLS(thisHelpers, FALSE, FALSE)
								GOLF_DISPLAY_CONTROLS(thisHelpers, FALSE)
							ENDIF
						ENDIF
					ELIF IS_SHOT_A_GIMME(thisCourse, thisFoursome, TRUE) AND ARE_ALL_PLAYERS_ON_THE_GREEN(thisFoursome, thisCourse)
					OR IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_IS_OOB)
						CDEBUG1LN(DEBUG_GOLF, "Changing back to GPS_APPROACH_BALL, with a gimme")
						SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_APPROACH_BALL)
					ENDIF
				ENDIF
				
				CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_SPLASH_TEXT_DISPLAYED_THIS_TURN)
			ELSE
				//Remote players is done with hole, play animated camera
				IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_CUP
					IF GOLF_MANAGE_REACTION_CAMERA(thisHelpers, thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
						SET_CAM_ACTIVE(thisHelpers.camReaction, TRUE)
					ENDIF
				ENDIF
				
				CLEAR_FOCUS()
			ENDIF
			
			IF DOES_CAM_EXIST(thisHelpers.camAddress)
				DESTROY_CAM(thisHelpers.camAddress)
			ENDIF
		BREAK
		CASE GPS_BALL_IN_HOLE_CELEBRATION
			GOLF_MANAGE_BALL_IN_HOLE_UI(thisGame, thisHelpers, thisCourse, thisPlayer, thisFoursome)
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
				IF bDebugSpew DEBUG_MESSAGE("GPS_BALL_IN_HOLE_CELEBRATION") ENDIF
				
					IF GET_GOLF_PLAYER_HOLE_SCORE(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) = 0
					AND IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
						SET_GLOBAL_GOLF_CONTROL_FLAG(GGCF_PLAYER_HOLE_IN_ONE)
					ENDIF
				
					INT iCurrentPlayer 
					iCurrentPlayer = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
					iCurrentPlayer = iCurrentPlayer
					
					IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_STROKE_LIMIT)
						//update stats
						IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LAST_LIE(thisFoursome) = LIE_GREEN AND GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != COMPUTER
						   	FLOAT fDist
							fDist = MAGIC_DISTANCE_CONVERSION* METERS_TO_YARDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_DISTANCE_TO_HOLE(thisFoursome))
							IF  fDist > GET_GOLF_STAT_LONGEST_PUTT()
								CDEBUG1LN(DEBUG_GOLF,"New Longest Made Putt ", GET_GOLF_FOURSOME_CURRENT_PLAYER_DISTANCE_TO_HOLE(thisFoursome) )
								SET_GOLF_STAT_LONGEST_PUTT(fDist)
							ENDIF
							
							IF fDist > GET_GOLF_STAT_LONGEST_PUTT_ON_HOLE(GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
								CDEBUG1LN(DEBUG_GOLF,"New Longest Made Putt ", GET_GOLF_FOURSOME_CURRENT_PLAYER_DISTANCE_TO_HOLE(thisFoursome), " on hole ", GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
								SET_GOLF_STAT_LONGEST_PUTT_FOR_HOLE(GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), fDist)
							ENDIF
							
						ENDIF
						
						INCREMENT_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome)
						INCREMENT_GOLF_PLAYER_HOLE_SCORE(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), 1)
						SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, <<0,0,0>>, LIE_CUP)
						GOLF_RESET_BALL_IN_HOLE(thisGame, thisPlayer, thisFoursome, thisHelpers)
						
						INT iCardIndex
						BOOL bDoneWithHole, bAllDoneWithHole
						bAllDoneWithHole = TRUE
						IF NOT IS_GOLF_FOURSOME_MP()//in mp the score is updated by the server
							REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iCardIndex
								bDoneWithHole = GET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, iCardIndex) = GPS_DONE_WITH_HOLE OR iCurrentPlayer = iCardIndex
								IF NOT bDoneWithHole
									bAllDoneWithHole = FALSE
								ENDIF
								SET_PLAYERCARD_SCORES(thisFoursome, thisHelpers, iCardIndex, GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[iCardIndex]), bDoneWithHole)
							ENDREPEAT
							REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iCardIndex
								bDoneWithHole = GET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, iCardIndex) = GPS_DONE_WITH_HOLE OR iCurrentPlayer = iCardIndex
								SET_PLAYERCARD_SLOT(thisHelpers, thisFoursome, thisFoursome.playerCore[iCardIndex].playerCard, GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[iCardIndex]), FALSE, bDoneWithHole)
								SET_SCOREBOARD_SLOT(thisHelpers, thisCourse, thisFoursome, allPlayers[iCardIndex], thisFoursome.playerCore[iCardIndex].playerCard, FALSE, bDoneWithHole)
							ENDREPEAT
						ELIF GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(thisFoursome) = 1
							//is mp and you are the only player, it's okay to update scorecard now
							iCardIndex = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
							bDoneWithHole = TRUE
							SET_PLAYERCARD_SCORES(thisFoursome, thisHelpers, iCardIndex, GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[iCardIndex]), bDoneWithHole)
							SET_PLAYERCARD_SLOT(thisHelpers, thisFoursome, thisFoursome.playerCore[iCardIndex].playerCard, GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[iCardIndex]), TRUE, bDoneWithHole, PARTICIPANT_ID_TO_INT())
							SET_SCOREBOARD_SLOT(thisHelpers, thisCourse, thisFoursome, allPlayers[iCardIndex], thisFoursome.playerCore[iCardIndex].playerCard, TRUE, bDoneWithHole)
						ELSE
							REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iCardIndex
								bDoneWithHole = GET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, iCardIndex) = GPS_DONE_WITH_HOLE OR iCurrentPlayer = iCardIndex
								IF NOT bDoneWithHole
									bAllDoneWithHole = FALSE
								ENDIF
							ENDREPEAT
						ENDIF
						GOLF_MANAGE_AMBIENT_SPEECH(thisGame, thisHelpers, thisCourse, thisFoursome, TRUE, FALSE, TRUE)
						
						IF NOT IS_GOLF_REACTION_ALLOWED(thisFoursome, thisHelpers, thisCourse)
							SET_CAM_ACTIVE(thisHelpers.camFlight, TRUE)
						ELSE
							IF GOLF_MANAGE_REACTION_CAMERA(thisHelpers, thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
								SET_CAM_ACTIVE(thisHelpers.camReaction, TRUE)
							ENDIF
						ENDIF
						IF bAllDoneWithHole
							CDEBUG1LN(DEBUG_GOLF,"All done with hole, setting up scorecard streamvol")
							SET_END_OF_HOLE_STREAMVOL(thisHelpers, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
						ENDIF
					ELSE
						GOLF_RESET_BALL_IN_HOLE(thisGame, thisPlayer, thisFoursome, thisHelpers)
						#IF NOT GOLF_IS_MP //in mp the score is updated by the server
							INT iCardIndex
							BOOL bDoneWithHole
							REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iCardIndex
								bDoneWithHole = GET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, iCardIndex) = GPS_DONE_WITH_HOLE OR iCurrentPlayer = iCardIndex
								SET_PLAYERCARD_SCORES(thisFoursome, thisHelpers, iCardIndex, GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[iCardIndex]), bDoneWithHole)
							ENDREPEAT
							REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iCardIndex
								bDoneWithHole = GET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, iCardIndex) = GPS_DONE_WITH_HOLE OR iCurrentPlayer = iCardIndex
								SET_PLAYERCARD_SLOT(thisHelpers, thisFoursome, thisFoursome.playerCore[iCardIndex].playerCard, GET_GOLF_PLAYER_CURRENT_SCORE(allPlayers[iCardIndex]), FALSE, bDoneWithHole)
								SET_SCOREBOARD_SLOT(thisHelpers, thisCourse, thisFoursome, allPlayers[iCardIndex], thisFoursome.playerCore[iCardIndex].playerCard, FALSE, bDoneWithHole)
							ENDREPEAT
						#ENDIF
					ENDIF
					SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome, GPS_POST_SHOT_REACTION)
					
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != COMPUTER
						g_savedGlobals.sGolfData.iNumPuttsTotal += GET_GOLF_PLAYER_HOLE_PUTTS(thisPlayer, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
					ENDIF
					
					CLEAR_CONTROL_FLAGS_BALL_AT_REST(thisFoursome,thisGame, thisHelpers)
					IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_MARKER(thisFoursome))
						CLEANUP_GOLF_FOURSOME_CURRENT_PLAYER_MARKER(thisFoursome)
					ENDIF
					CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_FIRST_COLLISION)
					#IF NOT GOLF_IS_MP
						SET_PED_GRAVITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), TRUE)
					#ENDIF
//				ENDIF
			ELSE //current player is over the network
				
				CLEAR_FOCUS()
			ENDIF
			
			#IF GOLF_IS_MP
				CANCEL_TIMER(thisHelpers.navTimer)
				
				RESET_GOLF_MP_APPROACH_CAM(thisHelpers)
			#ENDIF
			
			IF DOES_CAM_EXIST(thisHelpers.camAddress)
				DESTROY_CAM(thisHelpers.camAddress)
			ENDIF
		BREAK
		CASE GPS_DONE_WITH_SHOT
			IF bDebugSpew DEBUG_MESSAGE("GPS_DONE_WITH_SHOT") ENDIF
		BREAK
		CASE GPS_DONE_WITH_HOLE
			IF bDebugSpew DEBUG_MESSAGE("GPS_DONE_WITH_HOLE") ENDIF
		BREAK
	ENDSWITCH
ENDPROC



