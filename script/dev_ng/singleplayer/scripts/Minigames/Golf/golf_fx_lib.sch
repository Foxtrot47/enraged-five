USING "golf.sch"
USING "golf_foursome.sch"
USING "golf_course_lib.sch"

PROC SET_GOLF_VOICE_NAME(GOLF_FOURSOME &thisFoursome)

	UNUSED_PARAMETER(thisFoursome)
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID()) OR IS_ENTITY_DEAD(PLAYER_PED_ID())
		EXIT
	ENDIF

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			SET_AMBIENT_VOICE_NAME(PLAYER_PED_ID(), "MICHAEL_NORMAL")
		BREAK
		CASE CHAR_FRANKLIN
			SET_AMBIENT_VOICE_NAME(PLAYER_PED_ID(), "FRANKLIN_NORMAL")
		BREAK
		CASE CHAR_TREVOR
			SET_AMBIENT_VOICE_NAME(PLAYER_PED_ID(), "TREVOR_NORMAL")
		BREAK
	ENDSWITCH
	
	/* rob said no place holder speech 
	INT playerIndex
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) playerIndex
		PED_INDEX ped = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, playerIndex)
		IF DOES_ENTITY_EXIST(ped) AND NOT IS_ENTITY_DEAD(ped)
			IF ped != PLAYER_PED_ID()
				SET_AMBIENT_VOICE_NAME(ped, "TREVOR_NORMAL")
			ENDIF
		ENDIF
	ENDREPEAT
	//*/
ENDPROC

/// PURPOSE: Peds say an appropriate reaction after a shot in golf. isHazard is for when the ball is shot so bad it needs repositioning, like OOB or in water
PROC GOLF_PLAY_AMBIENT_PED_REACTION_SPEECH(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, BOOL golfSpecific, BOOL isHazard, BOOL inHole, BOOL bFour)
	
	TEXT_LABEL_15 talker = "_SELF"
	TEXT_LABEL_23 context = "_GOOD"
	TEXT_LABEL_15 game = "GAME"
	BOOL bSelf = TRUE
	BOOL bPlayerPedTalking = TRUE
	
	IF isHazard
		context = "_BAD"
	ENDIF
		
	INT pedTalkingIndex
	IF GET_RANDOM_INT_IN_RANGE(0, 10) < 8 // 80% chance of self comment
		pedTalkingIndex = GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
	ELSE
		IF WAS_TEE_SHOT(thisCourse, thisFoursome) OR inHole OR IS_BALL_ON_GREEN(thisCourse, thisFoursome)
			pedTalkingIndex = GET_RANDOM_INT_IN_RANGE(0, GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome))
		ELSE
			pedTalkingIndex = GET_GOLF_FOURSOME_NEAREST_PLAYER_TO_CURRENT_PLAYER(thisFoursome)
		ENDIF
	ENDIF
	PED_INDEX pedTalking = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, pedTalkingIndex)
	
	IF NOT DOES_ENTITY_EXIST(pedTalking) OR IS_ENTITY_DEAD(pedTalking)
		EXIT
	ENDIF
	
	IF pedTalkingIndex != GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
		talker = "_OTHER"
		bSelf = FALSE
	ENDIF
	
	IF pedTalkingIndex != 0 // Player isn't talking 
		SWITCH GET_ENTITY_MODEL(pedTalking)
			CASE A_M_Y_GOLFER_01
			CASE A_M_M_GOLFER_01
				bPlayerPedTalking = FALSE
			BREAK
		ENDSWITCH
	ENDIF
	
	IF golfSpecific
		game = "GOLF"
		
		IF isHazard
			IF bSelf // Shooter is talking 
				CDEBUG1LN(DEBUG_GOLF, "SHOT BAD HIT, hazard = true")
				context = "_SHOT_BAD_HIT"
			ELSE
				CDEBUG1LN(DEBUG_GOLF, "isHazard BAD")
				context = "_BAD"
				game = "GAME"
			ENDIF	
		//if area is occupied by a ped
		ELIF bFour 
			context = "_FORE"
			talker = "_SELF"
		ELSE
			GOLF_LIE_TYPE lie = GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)
			IF inHole
				IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_MISS_PUTT_DIALOG)
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome) <= GET_GOLF_HOLE_PAR_STATIC( GET_GOLF_COURSE_PLAYER_HOLE())
						CDEBUG1LN(DEBUG_GOLF, "SINK PUTT")
						context = "_SINK_PUTT"
					ELSE
						CDEBUG1LN(DEBUG_GOLF, "inHole BAD")
						context = "_BAD"
						game = "GAME"
					ENDIF
				ELSE
					CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_MISS_PUTT_DIALOG)
				ENDIF
			ELIF lie = LIE_GREEN 
				
				IF IS_GOLF_FOURSOME_CURRENT_PLAYER_JUST_HIT_BALL_ON_GREEN(thisFoursome) // just hit it onto green
					IF bPlayerPedTalking // Player isn't talking 
						CDEBUG1LN(DEBUG_GOLF, "GOOD LIE")
						context = "_GOOD_LIE"
					ELSE
						CDEBUG1LN(DEBUG_GOLF, "GOOD")
						context = "_GOOD"
						game = "GAME"
					ENDIF
				ELIF  IS_SHOT_A_GIMME(thisCourse, thisFoursome) // very close to hole but a miss
					PRINTLN("MISS PUTT")
					context = "_MISS_PUTT"
					SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_MISS_PUTT_DIALOG)
				ENDIF
				
			ELIF lie = LIE_BUNKER
				IF bPlayerPedTalking // Player isn't talking 
					CDEBUG1LN(DEBUG_GOLF, "LIE BUNKER speech")
					context = "_BUNKER"
				ELSE
					CDEBUG1LN(DEBUG_GOLF, "lie = LIE_BUNKER BAD")
					context = "_BAD"
					game = "GAME"
				ENDIF	
			ELIF lie = LIE_FAIRWAY
				
				IF thisFoursome.swingMeter.meterState = SWING_METER_SHANK
					IF bSelf // Shooter is talking 
						CDEBUG1LN(DEBUG_GOLF, "SHOT BAD HIT, shank")
						context = "_SHOT_BAD_HIT"
					ELSE
						CDEBUG1LN(DEBUG_GOLF, "lie = LIE_FAIRWAY BAD")
						context = "_BAD"
						game = "GAME"
					ENDIF	
				ELIF thisFoursome.swingMeter.fCurrentPowerRel > 90.0
					IF bSelf // Shooter is talking 
						CDEBUG1LN(DEBUG_GOLF, "SHOT GOOD HIT")
						context = "_SHOT_GOOD_HIT"
					ELSE
						CDEBUG1LN(DEBUG_GOLF, "GOOD")
						context = "_GOOD"
						game = "GAME"
					ENDIF	
				ELSE
					IF bPlayerPedTalking // Player isn't talking 
						CDEBUG1LN(DEBUG_GOLF, "GOOD LIE")
						context = "_GOOD_LIE"
					ELSE
						CDEBUG1LN(DEBUG_GOLF, "GOOD")
						context = "_GOOD"
						game = "GAME"
					ENDIF
				ENDIF
			ELSE
				IF bPlayerPedTalking // Player isn't talking 
					CDEBUG1LN(DEBUG_GOLF, "BAD LIE")
					context = "_BAD_LIE"
				ELSE
					CDEBUG1LN(DEBUG_GOLF, "ELSE BAD")
					context = "_BAD"
					game = "GAME"
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	TEXT_LABEL_31 speech = game
	speech += context
	speech += talker
	
	SPEECH_PARAMS speechParams = SPEECH_PARAMS_FORCE_NORMAL_CRITICAL
	FLOAT fDistToCurrent = VDIST(GET_CAM_COORD(GET_RENDERING_CAM()), GET_ENTITY_COORDS(pedTalking))
	
	IF fDistToCurrent > 25.0 
	AND NOT IS_BALL_ON_GREEN(thisCourse, thisFoursome)
	AND NOT inHole
	AND NOT WAS_TEE_SHOT(thisCourse, thisFoursome)
	AND (NOT bSelf OR GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER) 
		// Set this up in an audio packet
		CDEBUG1LN(DEBUG_GOLF, "Ped Speach Packet: ", speech)
		thisHelpers.sAudioContext = speech
		thisHelpers.pedAudio = pedTalking
	ELSE
		CDEBUG1LN(DEBUG_GOLF, "Ped Speach: ", speech)
		IF NOT bSelf
			fDistToCurrent = VDIST( GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome)), GET_ENTITY_COORDS(pedTalking))
			IF fDistToCurrent > 10.0
				speechParams = SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL
			ENDIF
		ENDIF
		CDEBUG1LN(DEBUG_GOLF, "Play ", speech, " on ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(pedTalking)), " with ", speechParams)
		PLAY_PED_AMBIENT_SPEECH(pedTalking, speech, speechParams)
	ENDIF
ENDPROC


/// PURPOSE: Makes the decision to call an ambient speech or not
PROC GOLF_MANAGE_AMBIENT_SPEECH(GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, BOOL golfSpecific, BOOL isHazard, BOOL inHole)
	//code for buddy golfers speaking is in golf_dialogue.sch
	//This is for main characters
	#IF GOLF_IS_MP
		EXIT
	#ENDIF
	
//	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER
//		EXIT
//	ENDIF
	
	UNUSED_PARAMETER(thisGame)
	
	GOLF_PLAYER_STATE playerState = GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome)
	IF GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_NAVIGATE_TO_SHOT
		IF DOES_ENTITY_EXIST(thisHelpers.pedAudio) AND NOT IS_ENTITY_DEAD(thisHelpers.pedAudio)
			CDEBUG1LN(DEBUG_GOLF, "PLAY Ped Speach Packet in navigate: ", thisHelpers.sAudioContext)
			SPEECH_PARAMS speechParams = SPEECH_PARAMS_FORCE_NORMAL_CRITICAL
			IF DOES_CAM_EXIST(GET_RENDERING_CAM()) //GET_RENDERING_CAM can return -1
				IF VDIST(GET_CAM_COORD(GET_RENDERING_CAM()), GET_ENTITY_COORDS(thisHelpers.pedAudio)) > 10.0
					speechParams = SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL
				ENDIF
			ENDIf
			PLAY_PED_AMBIENT_SPEECH(thisHelpers.pedAudio, thisHelpers.sAudioContext, speechParams)
			thisHelpers.pedAudio = NULL
			thisHelpers.sAudioContext = ""
		ENDIF	
	ELIF playerState = GPS_ADDRESS_BALL
		IF DOES_ENTITY_EXIST(thisHelpers.pedAudio) AND NOT IS_ENTITY_DEAD(thisHelpers.pedAudio)
			CDEBUG1LN(DEBUG_GOLF, "PLAY Ped Speach Packet in address: ", thisHelpers.sAudioContext)
			SPEECH_PARAMS speechParams = SPEECH_PARAMS_FORCE_NORMAL_CRITICAL
			IF DOES_CAM_EXIST(GET_RENDERING_CAM())
				IF VDIST(GET_CAM_COORD(GET_RENDERING_CAM()), GET_ENTITY_COORDS(thisHelpers.pedAudio)) > 10.0
					speechParams = SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL
				ENDIF
			ENDIF
			PLAY_PED_AMBIENT_SPEECH(thisHelpers.pedAudio, thisHelpers.sAudioContext, speechParams)
			thisHelpers.pedAudio = NULL
			thisHelpers.sAudioContext = ""
		ENDIF
		// will bitch when you take a long time. 
		IF GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) > 1
			IF NOT IS_TIMER_STARTED(thisHelpers.idleSpeechTimer)
				RESTART_TIMER_NOW(thisHelpers.idleSpeechTimer)
			ENDIF
			//PRINTLN("Idle complain time ", GET_TIMER_IN_SECONDS(thisHelpers.idleSpeechTimer))
			
			IF TIMER_DO_ONCE_WHEN_READY(thisHelpers.idleSpeechTimer, 30.0)
				INT pedTalkingIndex = GET_RANDOM_INT_IN_RANGE(1, GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome))
				PRINTLN("Golfer ", pedTalkingIndex, " comments on you being idle.")
				PED_INDEX pedTalking = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, pedTalkingIndex)
	
				IF DOES_ENTITY_EXIST(pedTalking) AND NOT IS_ENTITY_DEAD(pedTalking)
					PLAY_PED_AMBIENT_SPEECH(pedTalking, "GOLF_IDLE_OTHER", SPEECH_PARAMS_FORCE_NORMAL)
				ENDIF
				CANCEL_TIMER(thisHelpers.idleSpeechTimer)
			ENDIF
		ENDIF
	ELIF playerState = GPS_BALL_AT_REST OR playerState = GPS_BALL_IN_HOLE_CELEBRATION OR playerState = GPS_POST_SHOT_REACTION
		IF IS_TIMER_STARTED(thisHelpers.idleSpeechTimer)
			CANCEL_TIMER(thisHelpers.idleSpeechTimer)
		ENDIF
		//determine speach based on quality of shot
		BOOL playSpeech = FALSE
		BOOL four = FALSE
		GOLF_LIE_TYPE lie = GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)

		//Always coment on the tee shot				//always comment on bad shots    //always comment when hitting it in hole
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = 0
		AND (WAS_TEE_SHOT(thisCourse, thisFoursome)
		OR (lie = LIE_GREEN AND IS_GOLF_FOURSOME_CURRENT_PLAYER_JUST_HIT_BALL_ON_GREEN(thisFoursome))) //comment when first hitting on green
			playSpeech = TRUE
		ENDIF
		
		IF isHazard 
		OR lie = LIE_BUNKER 
		OR inHole
		OR IS_SHOT_A_GIMME(thisCourse, thisFoursome)
			playSpeech = TRUE
		ENDIF
		
		//play if you hit the ball near someone
		VECTOR ballPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
		VECTOR dir = 10.0*NORMALISE_VECTOR(<<1.0, 1.0, 1.0>>)
		IF IS_AREA_OCCUPIED(ballPos - dir, ballPos + dir, FALSE, FALSE, TRUE, FALSE, FALSE, PLAYER_PED_ID()) 
			AND NOT IS_BALL_IN_RANGE_OF_GREEN(thisCourse, thisFoursome)
			four = TRUE
			playSpeech = TRUE
		ENDIF
		
		//10% chance to play sound regaurdless
		IF GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0) < (0.1 * GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome))
			playSpeech = TRUE
		ENDIF		

		IF playSpeech
			GOLF_PLAY_AMBIENT_PED_REACTION_SPEECH(thisCourse, thisFoursome, thisHelpers, golfSpecific, isHazard, inHole, four)
		ENDIF
	ENDIF
ENDPROC

PROC GOLF_PLAY_BACKSWING_SOUND(GOLF_FOURSOME &thisFoursome)
	SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome)
		CASE 	SWING_STYLE_NORMAL	// Normal swing, no power adjustments
		CASE 	SWING_STYLE_POWER  // Power swing, slight power increase for loss of accuracy
			PLAY_SOUND_FROM_ENTITY(-1, "GOLF_BACK_SWING_HARD_MASTER", GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome))
		BREAK
		CASE 	SWING_STYLE_APPROACH // Low power swing with increased spin and accuracy
		CASE 	SWING_STYLE_PUNCH // Low power, low trajectory shot with strong backspin
			PLAY_SOUND_FROM_ENTITY(-1, "GOLF_BACK_SWING_HARD_MASTER", GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome))
		BREAK
		CASE 	SWING_STYLE_GREEN_SHORT  // Putting 
		CASE 	SWING_STYLE_GREEN  // Putting
		CASE 	SWING_STYLE_GREEN_LONG
		CASE 	SWING_STYLE_GREEN_TAP
			PLAY_SOUND_FROM_ENTITY(-1, "GOLF_BACK_SWING_HARD_MASTER", GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome))
		BREAK
	ENDSWITCH
ENDPROC

#IF NOT GOLF_IS_AI_ONLY
INT swingSoundID
#ENDIF

PROC GOLF_PLAY_SWING_SOUND(GOLF_FOURSOME &thisFoursome, GOLF_GAME &thisGame, BOOL bPerfectHit, FLOAT fSwingAmount)
	bPerfectHit = bPerfectHit
	fSwingAmount = fSwingAmount
	UNUSED_PARAMETER(thisGame)
	
	SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome)
		CASE 	SWING_STYLE_NORMAL	// Normal swing, no power adjustments
		CASE 	SWING_STYLE_POWER  // Power swing, slight power increase for loss of accuracy
		CASE 	SWING_STYLE_APPROACH // Low power swing with increased spin and accuracy
		CASE 	SWING_STYLE_PUNCH // Low power, low trajectory shot with strong backspin
			#IF NOT GOLF_IS_AI_ONLY
				eCLUB_TYPE eClub 
				eClub = GET_GOLF_CLUB_TYPE(thisGame.golfBag,  GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
				
				//only play swing sound when using woods and irons
				IF eClub <= eCLUB_IRON_9
					PRINTLN("Back swing phase is ", fSwingAmount)
					
					IF swingSoundID < 0
						swingSoundID = GET_SOUND_ID()
					ENDIF
					
					IF bPerfectHit
						PLAY_SOUND_FROM_ENTITY(swingSoundID, "GOLF_FORWARD_SWING_PERFECT_VB_MASTER", GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome))
						SET_VARIABLE_ON_SOUND(swingSoundID, "SwingSpeed", fSwingAmount)
					ELSE
						PLAY_SOUND_FROM_ENTITY(swingSoundID, "GOLF_FORWARD_SWING_VB_MASTER", GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome))
						SET_VARIABLE_ON_SOUND(swingSoundID, "SwingSpeed", fSwingAmount)
					ENDIF
				ENDIF
			#ENDIF
			
			#IF GOLF_IS_AI_ONLY
				PLAY_SOUND_FROM_ENTITY(-1, "GOLF_FORWARD_SWING_HARD_MASTER", GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome))
			#ENDIF
		BREAK
		CASE 	SWING_STYLE_GREEN_SHORT  // Putting 
		CASE 	SWING_STYLE_GREEN  // Putting
		CASE 	SWING_STYLE_GREEN_LONG
		CASE 	SWING_STYLE_GREEN_TAP
		BREAK
	ENDSWITCH
	

ENDPROC


PROC GOLF_PLAY_CLUB_IMPACT_SOUND(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, BOOL bFromCoord = FALSE, BOOL bPerfectHit = FALSE, BOOL bLight = FALSE)
	STRING soundHit = ""
	
	//the lie won't be valid if the golfer is far away from the player
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_FAR_AWAY)
		EXIT
	ENDIF
	
	SWITCH GET_GOLF_CLUB_TYPE(thisGame.golfBag,  GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
		CASE eCLUB_WOOD_1
		CASE eCLUB_WOOD_2
		CASE eCLUB_WOOD_3
		CASE eCLUB_WOOD_4
		CASE eCLUB_WOOD_5
			SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)
				CASE LIE_FAIRWAY
					IF bLight
						soundHit = "GOLF_SWING_GRASS_LIGHT_MASTER"
					ELIF bPerfectHit
						soundHit = "GOLF_SWING_GRASS_PERFECT_MASTER"
					ELSE
						soundHit = "GOLF_SWING_GRASS_MASTER"
					ENDIF
				BREAK
				CASE LIE_TEE
					IF bLight
						soundHit = "GOLF_SWING_TEE_LIGHT_MASTER"
					ELIF bPerfectHit
						soundHit = "GOLF_SWING_TEE_PERFECT_MASTER"
					ELSE
						soundHit = "GOLF_SWING_TEE_MASTER"
					ENDIF
				BREAK
				DEFAULT

					PRINTLN("Lie/Club mismatch at hole ", GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
						PRINTLN("Club ",  GET_GOLF_CLUB_TYPE(thisGame.golfBag,  GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)))
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN PRINTLN("Lie Green")
					ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_UNKNOWN PRINTLN("Lie Unknown")
					ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_WATER PRINTLN("Lie Water")
					ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_CUP PRINTLN("Lie Cup")
					ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_BUNKER PRINTLN("Lie Bunker")
					ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_CART_PATH PRINTLN("Lie Cart Path")
					ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_ROUGH PRINTLN("Lie Rough")
					ENDIF
					#IF NOT GOLF_IS_AI_ONLY
						SCRIPT_ASSERT("Lie/Club mismatch for sound - using an wood on non-tee/fairway lie")
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE eCLUB_IRON_2
		CASE eCLUB_IRON_3
		CASE eCLUB_IRON_4
		CASE eCLUB_IRON_5
		CASE eCLUB_IRON_6
		CASE eCLUB_IRON_7
		CASE eCLUB_IRON_8
		CASE eCLUB_IRON_9
			SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)
				CASE LIE_TEE
					IF bLight
						soundHit = "GOLF_SWING_TEE_IRON_LIGHT_MASTER"
					ELIF bPerfectHit
						soundHit = "GOLF_SWING_TEE_IRON_PERFECT_MASTER"
					ELSE
						soundHit = "GOLF_SWING_TEE_IRON_MASTER"
					ENDIF
				BREAK
				CASE LIE_FAIRWAY
					IF bLight
						soundHit = "GOLF_SWING_FAIRWAY_IRON_LIGHT_MASTER"
					ELIF bPerfectHit
						soundHit = "GOLF_SWING_FAIRWAY_IRON_PERFECT_MASTER"
					ELSE
						soundHit = "GOLF_SWING_FAIRWAY_IRON_MASTER"
					ENDIF
				BREAK
				CASE LIE_ROUGH
					IF bLight
						soundHit = "GOLF_SWING_ROUGH_IRON_LIGHT_MASTER"
					ELIF bPerfectHit
						soundHit = "GOLF_SWING_ROUGH_IRON_PERFECT_MASTER"
					ELSE
						soundHit = "GOLF_SWING_ROUGH_IRON_MASTER"
					ENDIF
				BREAK
				CASE LIE_BUNKER
					IF bLight
						soundHit = "GOLF_SWING_SAND_IRON_LIGHT_MASTER"
					ELIF bPerfectHit
						soundHit = "GOLF_SWING_SAND_IRON_PERFECT_MASTER"
					ELSE
						soundHit = "GOLF_SWING_SAND_IRON_MASTER"
					ENDIF
				BREAK
				CASE LIE_CART_PATH
					soundHit = "GOLF_SWING_TEE_IRON_MASTER"
				BREAK
				DEFAULT
				
					PRINTLN("Lie/Club mismatch at hole ", GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
					PRINTLN("Club ",  GET_GOLF_CLUB_TYPE(thisGame.golfBag,  GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)))
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN 
						PRINTLN("Lie Green")
					ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_UNKNOWN 
						PRINTLN("Lie Unknown")
					ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_WATER 
						PRINTLN("Lie Water")
					ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_CUP 
						PRINTLN("Lie Cup")
					ENDIF
					#IF NOT GOLF_IS_AI_ONLY
						SCRIPT_ASSERT("Lie/Club mismatch for sound - using an iron/wedge on inappropriate lie")
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK

		CASE eCLUB_WEDGE_APPROACH
		CASE eCLUB_WEDGE_LOB
		CASE eCLUB_WEDGE_LOB_ULTRA
		CASE eCLUB_WEDGE_PITCH
		CASE eCLUB_WEDGE_SAND
				SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)
				CASE LIE_TEE
				CASE LIE_FAIRWAY
				CASE LIE_GREEN
					IF bLight
						soundHit = "GOLF_SWING_CHIP_LIGHT_MASTER"
					ELIF bPerfectHit
						soundHit = "GOLF_SWING_CHIP_PERFECT_MASTER"
					ELSE
						soundHit = "GOLF_SWING_CHIP_MASTER"
					ENDIF				
				BREAK
				CASE LIE_ROUGH
					IF bLight
						soundHit = "GOLF_SWING_CHIP_GRASS_LIGHT_MASTER"
					ELIF bPerfectHit
						soundHit = "GOLF_SWING_CHIP_PERFECT_MASTER" //"GOLF_SWING_CHIP_GRASS_PERFECT_MASTER"
					ELSE
						soundHit = "GOLF_SWING_CHIP_GRASS_MASTER"
					ENDIF
				BREAK
				CASE LIE_BUNKER
					IF bLight
						soundHit = "GOLF_SWING_CHIP_SAND_LIGHT_MASTER"
					ELIF bPerfectHit
						soundHit = "GOLF_SWING_CHIP_SAND_PERFECT_MASTER"
					ELSE
						soundHit = "GOLF_SWING_CHIP_SAND_MASTER"
					ENDIF
				BREAK
				CASE LIE_CART_PATH
					soundHit = "GOLF_SWING_CHIP_MASTER"
				BREAK
				DEFAULT
				
					#IF NOT GOLF_IS_AI_ONLY
						PRINTLN("Lie/Club mismatch at hole ", GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
						PRINTLN("Club ",  GET_GOLF_CLUB_TYPE(thisGame.golfBag,  GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)))
						IF  GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_UNKNOWN 
							PRINTLN("Lie Unknown")
						ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_WATER 
							PRINTLN("Lie Water")
						ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_CUP 
							PRINTLN("Lie Cup")
						ENDIF
					
						SCRIPT_ASSERT("Lie/Club mismatch for sound - using an iron/wedge on inappropriate lie")
					#ENDIF
				BREAK
			ENDSWITCH
		
		BREAK
		
		CASE eCLUB_PUTTER
			soundHit = "GOLF_SWING_PUTT_MASTER"
		BREAK
	ENDSWITCH
	
	IF NOT IS_STRING_EMPTY(soundHit)
		#IF NOT GOLF_IS_AI_ONLY
			PRINTLN("Playing hit sound ", soundHit)
		#ENDIF
		
		IF bFromCoord OR NOT DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
			PLAY_SOUND_FROM_COORD(-1, soundHit, GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))
		ELSE
			PLAY_SOUND_FROM_ENTITY(-1, soundHit, GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_GOLF_WATER_IMPACT_SOUND_AND_PTFX(GOLF_FOURSOME &thisFoursome)
	PLAY_SOUND_FROM_ENTITY(-1, "GOLF_BALL_IN_WATER_MASTER", GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
	
	#IF GOLF_IS_AI_ONLY
		EXIT
	#ENDIF
	
	START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_golf_landing_water", GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), <<0,0,GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)) >>)
ENDPROC

PROC GOLF_PLAY_GROUND_IMPACT_SOUND(GOLF_FOURSOME &thisFoursome, BOOL bFromCoord, BOOL bLowImpact)
	STRING soundHit = ""
	SWITCH GET_LAST_MATERIAL_HIT_BY_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		CASE GOLF_FAIRWAY                                           // grass
		CASE GOLF_GREEN                                              // grass_short
			IF bLowImpact
				soundHit = "GOLF_BALL_IMPACT_FAIRWAY_LIGHT_MASTER"
			ELSE
				soundHit = "GOLF_BALL_IMPACT_FAIRWAY_MASTER"
			ENDIF
		BREAK
		CASE GOLF_ROUGH                                               // grass_long
		CASE GOLF_ROUGH2
			IF bLowImpact
				soundHit = "GOLF_BALL_IMPACT_GRASS_LIGHT_MASTER"
			ELSE
				soundHit = "GOLF_BALL_IMPACT_GRASS_MASTER"
			ENDIF
		BREAK
		CASE GOLF_BUNKER                                             // sand_loose
			IF bLowImpact
				soundHit = "GOLF_BALL_IMPACT_SAND_LIGHT_MASTER"
			ELSE
				soundHit = "GOLF_BALL_IMPACT_SAND_MASTER"
			ENDIF
		BREAK
		CASE GOLF_CART_PATH                                           // gravel_small
		CASE GOLF_PATH1                                               // concrete
		CASE GOLF_PATH2                                               // tarmac
		CASE GOLF_ROCKS												  //rock
			IF bLowImpact
				soundHit = "GOLF_BALL_IMPACT_CONCRETE_LIGHT_MASTER"
			ELSE
				soundHit = "GOLF_BALL_IMPACT_CONCRETE_MASTER"
			ENDIF
		BREAK
		CASE GOLF_HARD_VEG1                                          // woodchips
		CASE GOLF_HARD_VEG2                                          // wood_solid_medium
		CASE GOLF_HARD_VEG3                                       	// bushes
		CASE GOLF_HARD_VEG4
			IF bLowImpact
				soundHit = "GOLF_BALL_IMPACT_TREE_SOFT_MASTER"
			ELSE
				soundHit = "GOLF_BALL_IMPACT_TREE_MASTER"
			ENDIF
		BREAK
//		CASE GOLF_MISC=-1049967444,                                               // plastic
//		CASE GOLF_SOFT_VEG=1474783169,                                            // leaves
		DEFAULT
			IF bLowImpact
				soundHit = "GOLF_BALL_IMPACT_TREE_SOFT_MASTER"
			ELSE
				soundHit = "GOLF_BALL_IMPACT_TREE_MASTER"
			ENDIF
		BREAK
	ENDSWITCH
	IF NOT IS_STRING_EMPTY(soundHit)
		IF bFromCoord
			PLAY_SOUND_FROM_COORD(-1, soundHit, GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)))
		ELSE
			PLAY_SOUND_FROM_ENTITY(-1, soundHit, GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		ENDIF
	ENDIF
ENDPROC

PROC GOLF_PLAY_BALL_ROLLING_SOUND(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	OBJECT_INDEX objBall = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)
	IF NOT DOES_ENTITY_EXIST(objBall)
		EXIT
	ENDIF
	
	BOOL bPlayRollingSound = GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN AND NOT IS_ENTITY_IN_AIR(objBall)
	
	IF bPlayRollingSound
		IF thisHelpers.soundRolling < 0
			thisHelpers.soundRolling = GET_SOUND_ID()
		ENDIF
	
		IF HAS_SOUND_FINISHED(thisHelpers.soundRolling)
			PLAY_SOUND_FROM_ENTITY(thisHelpers.soundRolling, "GOLF_BALL_ROLL_PUTT_MASTER", objBall)
		ENDIF
	ENDIF
ENDPROC

PROC GOLF_PLAY_BALL_FOLIAGE_SOUND(GOLF_FOURSOME &thisFoursome)
	
	OBJECT_INDEX objBall = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)
	IF NOT DOES_ENTITY_EXIST(objBall)
		EXIT
	ENDIF

	PLAY_SOUND_FROM_ENTITY(-1, "GOLF_BALL_IMPACT_LEAVES_MASTER", objBall)
ENDPROC

PROC GOLF_PLAY_BALL_IN_HOLE_SOUND(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_GAME &thisGame, BOOL bSetFlag)
	
	IF NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_BALL_IN_CUP_SOUND)
		PRINTLN("PLAY GOLF BALL IN HOLE SOUND, ", GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
		PLAY_SOUND_FROM_COORD(-1, "GOLF_BALL_CUP_MASTER", GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
		
		IF bSetFlag
			SET_GOLF_CONTROL_FLAG(thisGame, GCF_BALL_IN_CUP_SOUND)
		ENDIF
	ENDIF
	
ENDPROC

PROC PLAY_GOLF_SCORECARD_SOUND_EFFECT(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_PLAYER &thisPlayer, BOOL bEndGame)

	INT iCurrentHole = PICK_INT(bEndGame, 8, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) - 1)
	INT iStrokes = GET_GOLF_PLAYER_HOLE_SCORE(thisPlayer, iCurrentHole)
	INT holePar = GET_GOLF_HOLE_PAR(thisCourse, iCurrentHole)
	
	UNUSED_PARAMETER(iStrokes)
	UNUSED_PARAMETER(holePar)
	UNUSED_PARAMETER(thisGame)

	PLAY_SOUND_FRONTEND(-1, "GOLF_HUD_SCORECARD_MASTER")
//	IF iStrokes < holePar
//		PLAY_SOUND_FRONTEND(-1, "CLAPS_EXCITE", "GOLF_CLAPS_SOUNDSET")
//	ELIF iStrokes = holePar
//		PLAY_SOUND_FRONTEND(-1, "CLAPS_PAR", "GOLF_CLAPS_SOUNDSET")
//	ELSE
//		PLAY_SOUND_FRONTEND(-1, "CLAPS_TEPID", "GOLF_CLAPS_SOUNDSET")
//	ENDIF
ENDPROC

//PTFX_ID
PROC PLAY_GOLF_BALL_TRAIL_PTFX(GOLF_FOURSOME &thisFoursome, BOOL bPerfect)
	#IF GOLF_IS_AI_ONLY
		EXIT
	#ENDIF
	
	IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
	AND DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		PRINTLN("Play golf trail effect")
		PTFX_ID ptfxId = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_golf_ball_trail", GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), <<0,0,0>>, <<0,0,0>>)
		IF bPerfect
			SET_PARTICLE_FX_LOOPED_COLOUR(ptfxId, 240.0/255.0, 200/255.0, 80/255.0)
		ENDIF
	ENDIF

ENDPROC

PROC END_GOLF_BALL_TRAIL_PTFX(GOLF_FOURSOME &thisFoursome)
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		REMOVE_PARTICLE_FX_FROM_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
	ENDIF
ENDPROC

PROC GOLF_PLAY_CLUB_IMPACT_PTFX(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_GAME &thisGame)
	
	#IF GOLF_IS_AI_ONLY
		EXIT
	#ENDIF
	
	STRING ptfxHit = ""
	
	SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)
		CASE LIE_FAIRWAY
			IF ABSF(thisFoursome.swingMeter.fCurrentAccurRel) < 10
				ptfxHit = "scr_golf_strike_fairway" 
			ELSE
				ptfxHit = "scr_golf_strike_fairway_bad" 
			ENDIF
		BREAK
		CASE LIE_ROUGH
			ptfxHit = "scr_golf_strike_thick_grass"
		BREAK
		CASE LIE_BUNKER
			ptfxHit = "scr_golf_strike_bunker"
		BREAK
		CASE LIE_GREEN
		BREAK
		CASE LIE_TEE
			//Pin the tee if you are at a tee shot, it is not a par 3 and you did not mess up the shot
			IF thisFoursome.swingMeter.meterState != SWING_METER_SHANK 
			AND GET_GOLF_HOLE_PAR(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)) > 3
				ptfxHit = "scr_golf_tee_perfect"
			ENDIF
		BREAK
		CASE LIE_CART_PATH
		CASE LIE_WATER
		CASE LIE_UNKNOWN
		BREAK
	ENDSWITCH
	
	PLAY_GOLF_BALL_TRAIL_PTFX(thisFoursome, IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_WAS_PERFECT_HIT))
	IF NOT IS_STRING_EMPTY(ptfxHit)
		START_PARTICLE_FX_NON_LOOPED_AT_COORD(ptfxHit, GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), <<0,0,GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome) >>)
	ENDIF
ENDPROC

PROC GOLF_PLAY_GROUND_IMPACT_PTFX(GOLF_FOURSOME &thisFoursome)
	#IF GOLF_IS_AI_ONLY
		EXIT
	#ENDIF
	
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
		EXIT
	ENDIF

	STRING ptfxHit = ""
	SWITCH GET_LAST_MATERIAL_HIT_BY_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		CASE GOLF_ROUGH                                             // grass_long_36
		CASE GOLF_ROUGH2											// grass_long_38180
			ptfxHit = "scr_golf_landing_thick_grass"
			BREAK
		CASE GOLF_HARD_VEG1                                          // woodchips
		CASE GOLF_HARD_VEG2                                          // wood_solid_medium
		CASE GOLF_HARD_VEG3                                           // bushes
		CASE GOLF_SOFT_VEG                                           // leaves
		CASE GOLF_HARD_VEG4
			ptfxHit = "scr_golf_hit_branches"
		BREAK
		CASE GOLF_BUNKER                                             // sand_loose
			ptfxHit = "scr_golf_landing_bunker"
		BREAK
		CASE GOLF_FAIRWAY                                           // grass
		CASE GOLF_GREEN                                              // grass_short
		CASE GOLF_CART_PATH                                           // gravel_small
		CASE GOLF_PATH1                                               // concrete
		CASE GOLF_PATH2                                               // tarmac
		CASE GOLF_MISC                                              // plastic
		BREAK
		DEFAULT
		BREAK
	ENDSWITCH
	IF NOT IS_STRING_EMPTY(ptfxHit)
		START_PARTICLE_FX_NON_LOOPED_AT_COORD(ptfxHit, GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), <<0,0,GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)) >>)
	ENDIF
ENDPROC

//Values for the golf trail that never change
PROC SET_GOLF_TRAIL_STATIC_DATA(GOLF_HELPERS &thisHelpers)

	thisHelpers.golfTrail.radius = <<0.025, 0.3, 0.025>>
	thisHelpers.golfTrail.colorStart = <<255.0,255.0,255.0>>
	thisHelpers.golfTrail.colorMid = <<255.0,255.0,255.0>>
	thisHelpers.golfTrail.colorEnd = <<255.0,255.0,255.0>>
	thisHelpers.golfTrail.vAlpha = <<100.0, 100.0, 100.0>>
	thisHelpers.golfTrail.numControlPoints = 8
	thisHelpers.golfTrail.tessellation = 10
	thisHelpers.golfTrail.pixelThickness = 1.0
	thisHelpers.golfTrail.pixelExpansion = 1.0
	thisHelpers.golfTrail.fadeOpacity = 1.0
	thisHelpers.golfTrail.fadeExponentBias  = 1.0
	thisHelpers.golfTrail.textureFill = 0.3
	
	
	GOLF_TRAIL_SET_RADIUS(thisHelpers.golfTrail.radius.x, thisHelpers.golfTrail.radius.y, thisHelpers.golfTrail.radius.z)
	GOLF_TRAIL_SET_COLOUR(FLOOR(thisHelpers.golfTrail.colorStart.x), FLOOR(thisHelpers.golfTrail.colorStart.y), FLOOR(thisHelpers.golfTrail.colorStart.z), FLOOR(thisHelpers.golfTrail.vAlpha.x), 
						  FLOOR(thisHelpers.golfTrail.colorMid.x),   FLOOR(thisHelpers.golfTrail.colorMid.y),   FLOOR(thisHelpers.golfTrail.colorMid.z),   FLOOR(thisHelpers.golfTrail.vAlpha.y),
						  FLOOR(thisHelpers.golfTrail.colorEnd.x),   FLOOR(thisHelpers.golfTrail.colorEnd.y),   FLOOR(thisHelpers.golfTrail.colorEnd.z),   FLOOR(thisHelpers.golfTrail.vAlpha.z))

	
	
	GOLF_TRAIL_SET_SHADER_PARAMS(thisHelpers.golfTrail.pixelThickness, thisHelpers.golfTrail.pixelExpansion,thisHelpers.golfTrail.fadeOpacity, thisHelpers.golfTrail.fadeExponentBias, thisHelpers.golfTrail.textureFill)

ENDPROC


FUNC BOOL PROBE_GOLF_TRAIL(GOLF_HELPERS &thisHelpers, INT startIndex, BOOL &ascend, FLOAT &returnHeight)
	
	VECTOR currentPos
	VECTOR nextPos
	
	IF thisHelpers.golfTrail.numControlPoints-2 < startIndex
		currentPos = GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(thisHelpers.golfTrail.numControlPoints-2)
		nextPos = GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(thisHelpers.golfTrail.numControlPoints-1)
	ELSE
		currentPos = GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(startIndex)
		nextPos = GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(startIndex+1)
	ENDIF
	
	VECTOR direction = nextPos-currentPos
	
	IF thisHelpers.golfTrail.numControlPoints-2 < startIndex
		currentPos = currentPos + direction * TO_FLOAT(startIndex - (thisHelpers.golfTrail.numControlPoints-2))
		nextPos    = nextPos    + direction * TO_FLOAT(startIndex - (thisHelpers.golfTrail.numControlPoints-2))
	ELIF startIndex = 0
		currentPos = currentPos + NORMALISE_VECTOR(direction)*0.15 //move a litte bit on the first probe so it doesn't intersect with the ball
	ENDIF
	
	VECTOR returnPos = <<0,0,0>>
	GOLF_LIE_TYPE lieType = LIE_ROUGH
	
	IF GOLF_PROBE_COURSE(currentPos, nextPos, startIndex, lieType, returnPos)
		returnHeight = returnPos.z
		thisHelpers.golfTrail.checkPointPos = returnPos
		SET_GOLF_SHAPE_TEST_LAST_VALID_INTERSECT(returnPos)
		
		PRINTLN("Lie from intersection ", lieType)
		SET_GOLF_INTERSECT_LIE(lieType)
		
		DRAW_DEBUG_SPHERE(returnPos, 0.25, 0, 255, 0)
		//PRINTLN("Index of probe that intersected ", startIndex)
		IF startIndex >= 3
			//PRINTLN("trail decending")
			ascend = FALSE
		ELSE
			//PRINTLN("trail accending")
			ascend = TRUE
		ENDIF
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL FIND_GOLF_TRAIL_OBSTRUCTION(GOLF_HELPERS &thisHelpers, BOOL &ascend, FLOAT &returnHeight)

	INT inc1
	INT startIndex = 0
	INT iNumOfProbes = MAX_NUMBER_OF_GOLF_TRAIL_PROBES-1
	
	REPEAT iNumOfProbes inc1 //(thisHelpers.golfTrail.numControlPoints-2) inc1
		//the line intersects with something
		IF PROBE_GOLF_TRAIL(thisHelpers, startIndex, ascend, returnHeight)
			//Intersection
			//PRINTLN("Found obstruction at check ", inc1)
			RETURN TRUE
		ENDIF
		
		startIndex++
		//startIndex = PICK_INT(startIndex>=(thisHelpers.golfTrail.numControlPoints-1), 0, startIndex)
	ENDREPEAT
	
	//After iNumOfProbes atempts the line did not intersect with the ground.
	RETURN FALSE
ENDFUNC

FUNC FLOAT FIND_GOLF_TRAIL_HEIGHT(GOLF_HELPERS &thisHelpers, VECTOR startPos)

	VECTOR endPoint = startPos
	INT i
//	iFramesForResults++
	
	//Check if there is a wall before you reach the top of the arc
	IF FIND_GOLF_TRAIL_OBSTRUCTION(thisHelpers, thisHelpers.golfTrail.ascend, endPoint.z)
		REPEAT MAX_NUMBER_OF_GOLF_TRAIL_PROBES i 
			SET_GOLF_SHAPE_TEST_CREATE_FLAG(i, FALSE)
		ENDREPEAT
		PRINTLN("Intersecting something")
		SET_GOLF_TRAIL_INTERSECT_FLAG(TRUE)
		RETURN endPoint.z
	ELIF GET_GOLF_SHAPE_TEST_LAST_COMPLETED_SECTION() = MAX_NUMBER_OF_GOLF_TRAIL_PROBES-1 //all probes turned up false
		PRINTLN("All probes turned up false")
		SET_GOLF_TRAIL_INTERSECT_FLAG(TRUE)
		//no obstructions for the trail, expand trail to find correct spot, use binary search
		PRINTLN("Using binary search meathod of finding line endpoint")
		thisHelpers.golfTrail.ascend = FALSE
		thisHelpers.golfTrail.velocity0 *= 0.98 //going down hill, reduce the power of the line a bit to match
		endPoint = GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(thisHelpers.golfTrail.numControlPoints-1) 
		FLOAT high = endPoint.z
		FLOAT low = 30.0
		FLOAT currentZ
		INT loopCount = 0
			
		WHILE(loopCount < 20)
			currentZ = low+(high-low)/2.0
			GOLF_TRAIL_SET_PATH(thisHelpers.golfTrail.position0, thisHelpers.golfTrail.velocity0,thisHelpers.golfTrail.velocityScale, currentZ, FALSE)
			endPoint = GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(thisHelpers.golfTrail.numControlPoints-1) 
			GET_GROUND_Z_FOR_3D_COORD(endPoint + <<0,0,100>>, endPoint.z)
			
			IF ABSF(endPoint.z-currentZ) < 0.1
				thisHelpers.golfTrail.checkPointPos = endPoint
				RETURN currentZ
			ELSE
				IF currentZ > endPoint.z
					high = currentZ
				ELSE
					low = currentZ
				ENDIF
			ENDIF
			loopCount++
		ENDWHILE
				
			REPEAT MAX_NUMBER_OF_GOLF_TRAIL_PROBES i 
				SET_GOLF_SHAPE_TEST_CREATE_FLAG(i, FALSE)
			ENDREPEAT
			
		RETURN -1.0
	ELSE
		//PRINTLN("Waiting for valid intersection")
		SET_GOLF_TRAIL_INTERSECT_FLAG(FALSE)
		VECTOR vLastIntersection = GET_GOLF_SHAPE_TEST_LAST_VALID_INTERSECT()
		IF VMAG2(vLastIntersection) != 0
			//PRINTLN("Returning last valid intersection ", vLastIntersection)
			RETURN vLastIntersection.z
		ELSE
			//PRINTLN("No valid last intersection, returning ", startPos.z) 
			RETURN startPos.z //return old z
		ENDIF
	ENDIF

ENDFUNC

PROC GET_PREVIEW_LINE_POWER_ADJUSTMENT_FOR_CLUB(eCLUB_TYPE eClub, FLOAT &fMaxAdjust, FLOAT &fMinAdjust, FLOAT &LoftAdjust)
	//RETURN fPowerAdjustPreviewLine

	SWITCH eClub
		CASE eCLUB_WOOD_1
			LoftAdjust = 0.070
			fMaxAdjust = 1.000
			fMinAdjust = 1.585
		BREAK
		CASE eCLUB_WOOD_3
			LoftAdjust = 0.080
			fMaxAdjust = 1.000
			fMinAdjust = 1.585
		BREAK
		CASE eCLUB_WOOD_5
			LoftAdjust = 0.080
			fMaxAdjust = 1.035
			fMinAdjust = 1.591
		BREAK
		CASE eCLUB_IRON_3
			LoftAdjust = 0.080
			fMaxAdjust = 1.075
			fMinAdjust = 1.650
		BREAK
		CASE eCLUB_IRON_4
			LoftAdjust = 0.085
			fMaxAdjust = 1.075
			fMinAdjust = 1.660
		BREAK
		CASE eCLUB_IRON_5
			LoftAdjust = 0.085
			fMaxAdjust = 1.120
			fMinAdjust = 1.670
		BREAK
		CASE eCLUB_IRON_6
			LoftAdjust = 0.085
			fMaxAdjust = 1.135
			fMinAdjust = 1.674
		BREAK
		CASE eCLUB_IRON_7
			LoftAdjust = 0.090
			fMaxAdjust = 1.174
			fMinAdjust = 1.690
		BREAK
		CASE eCLUB_IRON_8
			LoftAdjust = 0.095
			fMaxAdjust = 1.215
			fMinAdjust = 1.720
		BREAK
		CASE eCLUB_IRON_9
			LoftAdjust = 0.097
			fMaxAdjust = 1.245
			fMinAdjust = 1.730
		BREAK
		CASE eCLUB_WEDGE_PITCH
			LoftAdjust = 0.100
			fMaxAdjust = 1.265
			fMinAdjust = 1.730
		BREAK
		CASE eCLUB_WEDGE_SAND
			LoftAdjust = 0.100
			fMaxAdjust = 1.414
			fMinAdjust = 1.730
		BREAK
		CASE eCLUB_WEDGE_LOB
			LoftAdjust = 0.085
			fMaxAdjust = 1.485
			fMinAdjust = 1.730
		BREAK
		CASE eCLUB_PUTTER
			LoftAdjust = 0
			fMaxAdjust = 1.0
			fMinAdjust = 1.0
		BREAK
		
		DEFAULT
			PRINTLN("Invalid club ", eClub)
			SCRIPT_ASSERT("Invlaid club type when calculating preview line")
		BREAK
	ENDSWITCH
	
ENDPROC

//Sets all the data in thisHelpers.golfTrail
PROC GOLF_SET_GOLF_TRAIL_EFFECT(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, BOOL useMaxPower = FALSE)
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER //OR GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN
	OR GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_NETWORK
	OR IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
		EXIT
	ENDIF
	
	FLOAT swingHeading = GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome)
	//VECTOR shotEstimate = GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome)
	FLOAT swingLoft = GET_GOLF_CLUB_LOFT(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)) / 90.0
	
	INT currentClub = GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)
	eCLUB_TYPE eClub = GET_GOLF_CLUB_TYPE(thisGame.golfBag, currentClub)
	FLOAT swingPower = -GET_SWING_POWER(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag, thisFoursome.swingMeter, FALSE, FALSE, useMaxPower)
	VECTOR vLieNormal = -GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome)
	//VECTOR startPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)

	FLOAT fMeterGoal = GET_GOLF_PLAYER_METER_GOAL(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], TRUE)
	//The trail is innacurate, the weaker the club the more extra power I have to put into the trail to be accurate
	FLOAT t = PICK_FLOAT(useMaxPower, 1, fMeterGoal/100.0)
	FLOAT fMaxAdjust, fMinAdjust, fLoftAdjust
	
	GET_PREVIEW_LINE_POWER_ADJUSTMENT_FOR_CLUB(eClub, fMaxAdjust, fMinAdjust, fLoftAdjust)
	
	swingPower *= (fMaxAdjust*t + (1-t)*fMinAdjust)
	swingLoft += fLoftAdjust //every club needs a little bit extra loft
	
	//I also need extra power when doing an approach shot to make the line accurate
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_APPROACH
		IF eClub <= eCLUB_WEDGE_PITCH
			swingPower *= fPowerAdjustPreviewLineApproach //normal club and pitching wedge need extra power on approach
		ENDIF
		IF eClub >= eCLUB_WEDGE_PITCH //if the club is a wedge reduce the loft a bit
			swingLoft -= 0.06
		ENDIF
	ENDIF
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_PUNCH
		swingLoft /= 1.9
		IF eClub = eCLUB_WEDGE_LOB //smaller clubs need less power adjustment
			swingPower *= fPowerAdjustPreviewLinePunch - 0.08
		ELIF eClub >= eCLUB_IRON_8
			swingPower *= fPowerAdjustPreviewLinePunch - 0.03
		ELSE
			swingPower *= fPowerAdjustPreviewLinePunch
		ENDIF
	ENDIF
	
	//PRINTLN("Swing Power ", swingPower, " Swing Loft ", swingLoft, " t ", t, " swing meter pos ", fMeterGoal)
	
	VECTOR vPlayerAim,  vShotNormal, vLieOrthogonal
	FLOAT invLoft = 1 - swingLoft
	FLOAT x = 2 * invLoft - (invLoft*invLoft)
	FLOAT y = 2 * swingLoft - (swingLoft*swingLoft)
	vPlayerAim = NORMALISE_VECTOR( <<COS(swingHeading), SIN(swingHeading), 0>>)
	
	// Find the player aim, projected onto a normal vector that is orthogonal to the lie normal (S - ((S.L/L^2) * L))
	vLieOrthogonal = -NORMALISE_VECTOR(vPlayerAim - (DOT_PRODUCT(vPlayerAim, vLieNormal) * vLieNormal))

	// Rotate the projection toward the lie normal based on the swing loft angle
	vShotNormal = NORMALISE_VECTOR((-vPlayerAim * x) + (vLieNormal* y))
	VECTOR vShotDirection = NORMALISE_VECTOR((vLieOrthogonal * x) + (vLieNormal * y))
	
		
	FLOAT fCosAngleBetweenAims = DOT_PRODUCT(vPlayerAim, NORMALISE_VECTOR(<<vShotDirection.x, vShotDirection.y, 0>>))
	thisHelpers.fCosShotAngleDevation = fCosAngleBetweenAims

	FLOAT fClockwise = PICK_FLOAT(((vPlayerAim.x*vShotDirection.y) - (vPlayerAim.y*vShotDirection.x)) < 0, -1, 1)
	IF ABSF(fCosAngleBetweenAims) < COS(MAX_ANGLE_DEVIATION)
		thisHelpers.fCosShotAngleDevation = COS(MAX_ANGLE_DEVIATION)
		vShotDirection = ROTATE_VECTOR_ABOUT_Z(vShotDirection, fClockwise*(ACOS(ABSF(fCosAngleBetweenAims))-MAX_ANGLE_DEVIATION))
	ENDIF
	
	//the z from vShotNormal(V1) is the z I want, the direction from vShotDirection(V2) is the direction I want
	//the below is an equation to get a normalized vector (V3) where V3.z = V1.z
	// and the normalized 2D vectors (x,y) of V3 and V2 are equal
	vShotNormal.y = (vShotDirection.y/ABSF(vShotDirection.y)) * SQRT(ABSF((vShotNormal.z*vShotNormal.z - 1)/((vShotDirection.x*vShotDirection.x)/(vShotDirection.y*vShotDirection.y)+1)))
	vShotNormal.x = ((vShotDirection.x*vShotNormal.y)/vShotDirection.y)

	/*
	VECTOR startPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
	DRAW_DEBUG_LINE(startPos, startPos+vPlayerAim, 0,255,255)
	DRAW_DEBUG_LINE(startPos, startPos+vLieNormal, 0,0,255)
	DRAW_DEBUG_LINE(startPos, startPos+vLieOrthogonal, 255, 0, 0)
	DRAW_DEBUG_LINE(startPos, startPos+vShotNormal, 0, 255, 0)
	//*/
	
	VECTOR endPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
	//*
	SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)
		CASE LIE_TEE
		CASE LIE_GREEN
		CASE LIE_FAIRWAY
			swingPower *=0.85
		BREAK
		CASE LIE_ROUGH
		CASE LIE_CART_PATH
			swingPower *= 0.825
		BREAK
		CASE LIE_BUNKER
		CASE LIE_WATER
		CASE LIE_UNKNOWN
			swingPower *= 0.8
		BREAK
	ENDSWITCH
	//*/
	
	thisHelpers.golfTrail.position0 = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		thisHelpers.golfTrail.position0 = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
	ENDIF
	
//	DRAW_DEBUG_SPHERE(thisHelpers.golfTrail.position0, 0.25, 255, 255, 255)
	
	thisHelpers.golfTrail.velocity0 = vShotNormal
	thisHelpers.golfTrail.velocityScale = swingPower
	thisHelpers.golfTrail.z1 = endPos.z
		
	//Set a prediction line to get some information about the curve
	//GOLF_TRAIL_SET_TESSELLATION(7, thisHelpers.golfTrail.tessellation)
	//GOLF_TRAIL_SET_PATH(thisHelpers.golfTrail.position0, thisHelpers.golfTrail.velocity0, thisHelpers.golfTrail.velocityScale, thisHelpers.golfTrail.z1, FALSE)
	
	//The highest Z value this shot can go
	//thisHelpers.golfTrail.topOfArc = GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(3)
	IF NOT IS_GOLF_SHAPE_TEST_CREATE_FLAG_SET(1)
		GOLF_TRAIL_SET_TESSELLATION(thisHelpers.golfTrail.numControlPoints, thisHelpers.golfTrail.tessellation)
		GOLF_TRAIL_SET_PATH(thisHelpers.golfTrail.position0, thisHelpers.golfTrail.velocity0, thisHelpers.golfTrail.velocityScale, thisHelpers.golfTrail.z1, FALSE)
	ENDIF
	
	IF NOT useMaxPower
		thisHelpers.golfTrail.z1 = FIND_GOLF_TRAIL_HEIGHT(thisHelpers, endPos)
	ELSE
		thisHelpers.golfTrail.checkPointPos = GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(thisHelpers.golfTrail.numControlPoints-1)
	ENDIF
	
	//("thisHelpers.golfTrail.checkPointPos ", thisHelpers.golfTrail.checkPointPos )
	IF thisHelpers.golfTrail.z1 < 0
		thisHelpers.golfTrail.z1 = endPos.z
	ENDIF
	
	IF thisHelpers.golfTrail.z1 > GOLF_TRAIL_GET_MAX_HEIGHT()
		//PRINTLN("End point is too fucking high")
		thisHelpers.golfTrail.z1 = GOLF_TRAIL_GET_MAX_HEIGHT() - 0.01
	ENDIF
	
	IF IS_GOLF_TRAIL_INTERSECT_FLAG_SET()
//		PRINTLN("Frames to find intersection ", iFramesForResults)
//		PRINTLN("Intersect point ", vLastValidIntersect)
	ELSE
//		PRINTLN("Strill doing intersection test after frame ", iFramesForResults)
	ENDIF
	
	SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome, thisHelpers.golfTrail.checkPointPos)
	
	SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_GOLF_TRAIL)

ENDPROC

//Draws the golf trail
PROC GOLF_DRAW_GOLF_TRAIL_EFFECT(GOLF_HELPERS &thisHelpers, GOLF_FOURSOME &thisFoursome)

	IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_REFRESH_GOLF_TRAIL)
		EXIT
	ENDIF

	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER //OR GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN
	OR GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_NETWORK
	OR IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
		EXIT
	ENDIF
	
	CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_GOLF_TRAIL)
	
	PRINTLN("Draw golf trail")
	
	//Set path
	//PRINTLN("Drawing line with end point at height ", thisHelpers.golfTrail.z1)
	GOLF_TRAIL_SET_PATH(thisHelpers.golfTrail.position0, thisHelpers.golfTrail.velocity0, thisHelpers.golfTrail.velocityScale, thisHelpers.golfTrail.z1, thisHelpers.golfTrail.ascend)
	GOLF_TRAIL_SET_ENABLED(TRUE)
	
	VECTOR endPos = GOLF_TRAIL_GET_VISUAL_CONTROL_POINT(thisHelpers.golfTrail.numControlPoints-1)
	FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(thisHelpers.golfTrail.position0, endPos)
	FLOAT fMaxMinDiff = GOLF_TRAIL_MAX_DIST - GOLF_TRAIL_MIN_DIST
	
	IF fDist > GOLF_TRAIL_MAX_DIST
		fDist = GOLF_TRAIL_MAX_DIST
	ELIF fDist < GOLF_TRAIL_MIN_DIST
		fDist = GOLF_TRAIL_MIN_DIST
	ENDIF
	
	FLOAT fInterpValue = (fDist - GOLF_TRAIL_MIN_DIST) / (fMaxMinDiff)
	
	VECTOR vMinRadius = <<0.010, 0.025, 0.010>>
	
	VECTOR vNewRadius = vMinRadius + ((thisHelpers.golfTrail.radius - vMinRadius) * fInterpValue)
	GOLF_TRAIL_SET_RADIUS(vNewRadius.x, vNewRadius.y, vNewRadius.z)
	
	//GET_GROUND_Z_FOR_3D_COORD(endPos+<<0,0,50.0>>, endPos.z)
	//DRAW_CHECKPOINT(endPos, 10, 255, 255, 255)

	thisHelpers.golfTrail.checkPointPos = endPos
	SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome, thisHelpers.golfTrail.checkPointPos)

ENDPROC

FUNC STRING GET_GOLF_HUD_MINIGAME_SOUNDSET()
	RETURN "HUD_MINI_GAME_SOUNDSET"
ENDFUNC
