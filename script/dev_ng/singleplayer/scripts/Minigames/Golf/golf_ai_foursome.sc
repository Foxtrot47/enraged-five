CONST_INT	GOLF_IS_MP 0
BOOL bDebugSpew = FALSE

CONST_INT COMPILE_LOOKUPS 1
CONST_INT GOLF_IS_AI_ONLY	1
USING "golf.sch"
USING "golf_core.sch"
	
PROC GOLF_AI_CLEANUP(GOLF_COURSE &currentCourse, GOLF_FOURSOME &aiFoursomes[])
	SCRIPT_GOLF_CLEANUP_COURSE(currentCourse)
	SCRIPT_GOLF_CLEANUP_AI_FOURSOME(aiFoursomes)
	RESET_GLOBAL_GOLF_DATA()
	REMOVE_ANIM_DICT(GET_GOLF_BASIC_ANIMATION_DICTIONARY_NAME())
	
	// DO NOT REMOVE THIS LINE. THE THREAD WILL STAY ACTIVE FOREVER IF YOU DO.
	TERMINATE_THIS_THREAD()
ENDPROC

PROC SETUP_AI_FOURSOME_FOR_INTRO_CUTSCENE(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse)
	PRINTLN("Setting up foursome for intro cutscene")
	
	GOLF_CLEANUP_CURRENT_HOLE(thisCourse, thisFoursome)
	SET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome,0)
	SET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome, 2)
	GOLF_INIT_CURRENT_HOLE(thisCourse, thisFoursome)
	
	VECTOR vBallPos
	OBJECT_INDEX objGolfBall
	
	IF NOT IS_PED_INJURED(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 0))
		vBallPos = <<-1238.81750, 109.39026, 55.91227>>
		objGolfBall = GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, 0)
		
		SET_GOLF_FOURSOME_INDEXED_PLAYER_BALL_POSITION(thisFoursome, 0, vBallPos)
		
		IF DOES_ENTITY_EXIST(objGolfBall)
			DELETE_OBJECT(objGolfBall)
		ENDIF
		
		SET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, 0, CREATE_GOLF_BALL(vBallPos, 0))
		SET_GOLF_FOURSOME_INDEXED_PLAYER_LIE(thisFoursome, 0, vBallPos, LIE_GREEN, FALSE)
		SET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(thisFoursome, 0, 2)
		SET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, 0, GPS_APPROACH_BALL)
		SET_GOLF_FOURSOME_INDEXED_PLAYER_SWING_STYLE(thisFoursome, 0, SWING_STYLE_GREEN_SHORT)
		SET_GOLF_FOURSOME_INDEXED_PLAYER_DISTANCE_TO_HOLE(thisFoursome, 0, 4.0)
		
		SET_ENTITY_COORDS(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 0), <<-1241.8021, 107.3439, 55.7186>>)
		SET_ENTITY_HEADING(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 0), 342.8433)
	ENDIF
	
	IF NOT IS_PED_INJURED(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 1))
		vBallPos = <<-1236.20117, 112.78288, 56.23418>>
		objGolfBall = GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, 1)
		CLEAR_PED_TASKS_IMMEDIATELY(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 1))
		
		IF DOES_ENTITY_EXIST(objGolfBall)
			DELETE_OBJECT(objGolfBall)
		ENDIF	
		
		SET_GOLF_FOURSOME_INDEXED_PLAYER_BALL_POSITION(thisFoursome, 1, vBallPos)
		SET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, 1, CREATE_GOLF_BALL(vBallPos, 0))
		SET_GOLF_FOURSOME_INDEXED_PLAYER_LIE(thisFoursome, 1, vBallPos, LIE_CUP, FALSE)
		SET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(thisFoursome, 1, 2)
		SET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, 1, GPS_DONE_WITH_HOLE)
		
		SET_ENTITY_COORDS(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 1), <<-1243.4652, 104.3183, 55.5857>>)
		SET_ENTITY_HEADING(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, 1), 306.0930)
	ENDIF
	
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CART(thisFoursome, 0))
	AND IS_VEHICLE_DRIVEABLE(GET_GOLF_FOURSOME_CART(thisFoursome, 0))
		SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CART(thisFoursome, 0), <<-1257.2341, 94.0251, 54.7840>>, FALSE)
		SET_ENTITY_HEADING(GET_GOLF_FOURSOME_CART(thisFoursome, 0), 240.9284)
	ENDIF
	
	SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_TAKING_SHOT)
ENDPROC

SCRIPT

	GOLF_FOURSOME	aiFoursomes[1]
	GOLF_COURSE		currentCourse
	GOLF_MINIGAME_STATE		golfState
	GOLF_GAME		currentGame
	STREAMED_MODEL	golfAssets[11]
	
    PRINTSTRING("ENTER AI FOURSOME SCRIPT\n")
	golfState = GMS_INIT
	INT updateIndex, playerIndex
	bFlagPassed = bFlagPassed

	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		PRINTLN("Force cleanup golf ambient ai")
		GOLF_AI_CLEANUP(currentCourse, aiFoursomes)
	ENDIF
	
	CLEAR_GLOBAL_GOLF_CONTROL_FLAG(GGCF_AMBIENT_GOLFERS_STREAMED_IN)
	
	//SCRIPT_SETUP(currentGame)
	
	WHILE (TRUE)		
		//VECTOR golfCourseLoc <<-1328, 60, 53>>  // near putting greeen and golf launcher
		//VECTOR golfCourseLoc = <<-1175, 40, 57>> // near center of course
		//Terminate script if you are far enough away from the course
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			// Handled by ambient area controller
			//IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), golfCourseLoc) > 120000
			//	PRINTLN("Distance Squared from Course ", VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), golfCourseLoc), "\n")
			//	golfState = GMS_CLEANUP
			//ENDIF
		ENDIF
		
		
		#IF IS_DEBUG_BUILD 
			IF bDebugDrawAI OR bDebugDrawForces
				GOLF_DEBUG_DRAW_FOURSOMES(currentGame, currentCourse, aiFoursomes)
			ENDIF
			
			IF bAmbientGolfersOff OR g_bTriggerSceneActive
				golfState = GMS_CLEANUP
			ENDIF
		#ENDIF
		
		SWITCH golfState
			CASE GMS_INIT
				IF bDebugSpew DEBUG_MESSAGE("GMS_INIT") ENDIF
				GOLF_SETUP_CURRENT_COURSE(currentCourse)
				//SET_PED_NON_CREATION_AREA(<< -1057.3892, -180.0265, 30 >>, << -1099.5076, 252.0553, 65 >>)
				REQUEST_ANIM_DICT(GET_GOLF_BASIC_ANIMATION_DICTIONARY_NAME())
				REQUEST_ANIM_SET("move_m@golfer@")
				
				GOLF_STREAM_ASSETS(golfAssets)
				#IF IS_DEBUG_BUILD
					GOLF_SETUP_DEBUG_WIDGETS(currentGame, "Golf Ambient")
				#ENDIF
				
				golfState = GMS_INIT_STREAMING_DONE
			BREAK

			CASE GMS_INIT_STREAMING_DONE

				//IF GOLF_ASSETS_ARE_STREAMED(currentGame, golfAssets)
				IF HAS_ANIM_DICT_LOADED(GET_GOLF_BASIC_ANIMATION_DICTIONARY_NAME())
				AND REQUEST_SCRIPT_AUDIO_BANK("GOLF_I")
				AND HAS_ANIM_SET_LOADED("move_m@golfer@")
				AND ARE_MODELS_STREAMED(golfAssets)
					GOLF_SETUP_CLUBS (currentGame.golfBag)
					golfState = GMS_INIT_POST_STREAMING  
				ENDIF
			BREAK
			CASE GMS_INIT_POST_STREAMING
				PRINTLN("Is possible to create ai foursome?")
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND IS_GOLF_CLUB_OPEN_AT_TIME_OF_DAY(GET_CLOCK_HOURS()) //golf club is open
				
					//if player can't see tee
					IF NOT IS_SPHERE_VISIBLE(GET_GOLF_HOLE_TEE_POSITION(currentCourse, 2+updateIndex*2), 10)
					AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_GOLF_HOLE_TEE_POSITION(currentCourse, 2+updateIndex*2), <<10, 10, 10>>)
					OR (Is_Player_Timetable_Scene_In_Progress() OR IS_PLAYER_SWITCH_IN_PROGRESS() OR IS_SCREEN_FADED_OUT())
					OR IS_GLOBAL_GOLF_CONTROL_FLAG_SET(GGCF_START_GOLF_GAME)
						PRINTLN("Create golf ai forusome")
						//create
						IF playerIndex = 0
							GOLF_INIT_FOURSOME_DATA(currentCourse, aiFoursomes[updateIndex],  2+updateIndex*2, GET_RANDOM_INT_IN_RANGE(1, 3))
						ENDIF
						
						IF playerIndex < GET_GOLF_FOURSOME_NUM_PLAYERS(aiFoursomes[updateIndex])
							GOLF_CREATE_AI_GOLFER(currentCourse, aiFoursomes[updateIndex], playerIndex)
						ENDIF
						
						playerIndex++
				
						IF playerIndex > GET_GOLF_FOURSOME_NUM_PLAYERS(aiFoursomes[updateIndex])
							playerIndex = 0			
							GOLF_CREATE_AI_CARTS(currentCourse, aiFoursomes[updateIndex], GET_GOLF_FOURSOME_CURRENT_HOLE(aiFoursomes[updateIndex]), GET_GOLF_FOURSOME_NUM_PLAYERS(aiFoursomes[updateIndex]))
							
							updateIndex++
						
							IF updateIndex >= COUNT_OF(aiFoursomes)
								golfState = GMS_INIT_DONE
								updateIndex = 0
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					
			BREAK
			CASE GMS_INIT_DONE
//				DEBUG_MESSAGE("GMS_INIT_DONE")
				CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_AI_HIT_BALL_THIS_FRAME)
				SET_GLOBAL_GOLF_CONTROL_FLAG(GGCF_AMBIENT_GOLFERS_STREAMED_IN)
				golfState = GMS_PLAY_GOLF

			BREAK
			CASE GMS_PLAY_GOLF
				CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_AI_HIT_BALL_THIS_FRAME)

				//REPEAT COUNT_OF(aiFoursomes) tempIndex
//					PRINTSTRING("Manage AI group ")PRINTNL()
					IF IS_GLOBAL_GOLF_CONTROL_FLAG_SET(GGCF_START_GOLF_GAME)
						//player started game of golf, place everyone in correct starting spot
						
						SETUP_AI_FOURSOME_FOR_INTRO_CUTSCENE(aiFoursomes[0], currentCourse)
						CLEAR_GLOBAL_GOLF_CONTROL_FLAG(GGCF_START_GOLF_GAME)
					ENDIF
					
					GOLF_MANAGE_AI_FOURSOME(currentGame, currentCourse, aiFoursomes[updateIndex], aiFoursomes, updateIndex)
					updateIndex++
					IF updateIndex >= COUNT_OF(aiFoursomes)
						updateIndex = 0
					ENDIF
				//ENDREPEAT
				
				IF IS_GLOBAL_GOLF_CONTROL_FLAG_SET(GGCF_PLAYER_SKIP)
					CLEAR_GLOBAL_GOLF_CONTROL_FLAG(GGCF_PLAYER_SKIP)
				ENDIF
				IF IS_GLOBAL_GOLF_CONTROL_FLAG_SET(GGCF_PLAYER_DONE_WITH_HOLE)
					CLEAR_GLOBAL_GOLF_CONTROL_FLAG(GGCF_PLAYER_DONE_WITH_HOLE)
				ENDIF
				
			BREAK
					
			CASE GMS_CLEANUP
				PRINTSTRING("Terminate Golf AI Script\n")
				GOLF_AI_CLEANUP(currentCourse, aiFoursomes)
			BREAK
			DEFAULT
				SCRIPT_ASSERT("golfState should never hit case default - please contact Alan Blaine with repro information")
			BREAK
		ENDSWITCH
		
		WAIT(0)
	ENDWHILE
//*/
ENDSCRIPT

