CONST_INT	GOLF_IS_MP 1
CONST_INT	COMPILE_LOOKUPS 1
CONST_INT GOLF_USE_FALLTHROUGH_WORLD_CHECK 1
BOOL bDebugSpew
USING "net_blips.sch"
USING "golf.sch"
INT	iGolfPlayingParticipantID[MP_GOLF_MAX_PLAYERS] // save the participant id of the people actualy playing the game
USING "net_include.sch"
USING "golf_mp.sch"
USING "golf_mp_support.sch"
USING "golf_mp_client_lib.sch"
USING "golf_mp_server_lib.sch"
USING "cheat_handler.sch"
USING "golf_mp_spectate.sch"

DPAD_VARS dPadVars

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SCRIPT(MP_MISSION_DATA fmmcMissionData)
#IF IS_DEBUG_BUILD
	GOLF_CONTROL_FLAGS savedGCF[MP_GOLF_MAX_PLAYERS]
	VECTOR savedBallPositions[MP_GOLF_MAX_PLAYERS]
	//GOLF_CONTROL_FLAGS mySavedGCF
	//GOLF_CONTROL_FLAGS savedServerGCF
#ENDIF
	
	GOLF_MP_SERVER_BROADCAST_DATA serverBD
	GOLF_MP_PLAYER_BROADCAST_DATA playerBD[MP_GOLF_MAX_PLAYERS]
	
	STREAMED_MODEL	golfAssets[11]
	GOLF_PLAYER		currentPlayers[MP_GOLF_MAX_PLAYERS]
	GOLF_FOURSOME	currentPlayerFoursome
	
	GOLF_COURSE		currentCourse
	GOLF_GAME		currentGame
	GOLF_HELPERS 	currentHelpers
	TEXT_LABEL_63 	sPlayerNames[MP_GOLF_MAX_PLAYERS]
	GOLF_HEADSET_STATE golfHeadsetStates[MP_GOLF_MAX_PLAYERS]
	GOLF_XP_TRACKER golfXPTracker
	
	structTimer		timerPreInitWait
	structTimer		timerFailToLoad
	structTimer		timeForLoadSceneToFinish
	
	
	TEXT_LABEL_23 contentID
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_PLAYER_KILLED_OR_ARRESTED)
		CDEBUG1LN(DEBUG_GOLF,"Force golf_mp cleanup")
		CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(FALSE, currentGame, currentCourse, currentPlayerFoursome, currentPlayers, currentHelpers)
	ENDIF
	
	INT playerIndex, iDisplayQuitterIndex = -1
	PED_INDEX tempPed = NULL
	
//	FMMC_EOM_DETAILS sEndOfMission
//	PLAYER_CURRENT_STATUS_COUNTS sPlayerCounts
//	BOOL bRematchInit = FALSE
	
	CELEBRATION_SCREEN_DATA golfCelebrationData	
	INT iWinnerIndex, iWinnerNetIndex, iPlacement, iBetWinnings
	CAMERA_INDEX camGolfEndScreen
	BOOL bDrawGame, bTiedPlacement = FALSE
	
	INT iHeadTextureLoadIndex = 0, iHeadTextureLoadFailSafe
	BOOL bIsHeadTexturedRegistered = FALSE
	PEDHEADSHOT_ID myPedHeadshot

	BOOL bHereForInit = FALSE
	BOOL bAllowOverideClock = TRUE
	BOOL bGolfersAddedToGameLocally = FALSE
	BOOL bDidFMGameStartCall = FALSE

	BOOL bHaveSavedNames = FALSE
	BOOL bRecreatePlayercard = FALSE
	BOOL bDoLocalQuit = FALSE //quit without entering the speical quit state, used for spectators
	
	//PRINT_HELP("MP_GOLF_HELP")	
	bFlagPassed = bFlagPassed
	
	SETUP_PC_GOLF_CONTROLS()
	
	// Carry out all the initial game starting duties
	IF NOT PROCESS_PRE_GAME(fmmcMissionData, serverBD, playerBD)
		CDEBUG1LN(DEBUG_GOLF,"Golf MP failed to receive an initial network broadcast. Cleaning up.")
		CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(FALSE, currentGame, currentCourse, currentPlayerFoursome, currentPlayers, currentHelpers)
	ENDIF
	
	CDEBUG1LN(DEBUG_GOLF,"Start MP Golf! Dev Network")
	
	currentHelpers.iStartingHole = g_FMMC_STRUCT.iStartingHole
	currentHelpers.iEndingHole = g_FMMC_STRUCT.iNumberOfHoles-1
	
	CDEBUG1LN(DEBUG_GOLF,"Starting Hole is ", currentHelpers.iStartingHole, " ending hole is ", currentHelpers.iEndingHole)
	IF currentHelpers.iEndingHole > 8
		currentHelpers.iEndingHole  = 8
	ENDIF
	
	IF currentHelpers.iStartingHole > currentHelpers.iEndingHole
		CDEBUG1LN(DEBUG_GOLF,"Invalid options")
		currentHelpers.iStartingHole = 0
		currentHelpers.iEndingHole = 8
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.iStartHole = currentHelpers.iStartingHole
		serverBD.iEndHole = currentHelpers.iEndingHole
		serverBD.iWeather = g_FMMC_STRUCT.iWeather
	ENDIF
	
	IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		CDEBUG1LN(DEBUG_GOLF,"Golf Weather option ", g_FMMC_STRUCT.iWeather)
		//EXTRASUNNY CLEAR SMOG CLOUDY RAIN NEUTRAL	SNOW
		IF g_FMMC_STRUCT.iWeather = ciGOLF_OPTION_SUNNY
			SET_OVERRIDE_WEATHER("EXTRASUNNY")
		ELIF  g_FMMC_STRUCT.iWeather = ciGOLF_OPTION_RAINING
			SET_OVERRIDE_WEATHER("RAIN")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
//		bStopMPTimer = TRUE
	#ENDIF
	
	IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
		//Set appropiate golf shoes for female golfers, no high heels
		CDEBUG1LN(DEBUG_GOLF,"Changing into appropiate golf shoes")
		currentHelpers.eFeetProp = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_FEET)
		SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_FEET, FEET_FMF_10_1, FALSE)
		FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
	ENDIF
	
	// Setup widgets
	INT iNumMissionPlayers = NETWORK_GET_NUM_PARTICIPANTS() //NETWORK_GET_NUM_PARTICIPANTS()
	DUMMY_REFERENCE_INT(iNumMissionPlayers)
	#IF IS_DEBUG_BUILD
		GOLF_SETUP_DEBUG_WIDGETS(currentGame#IF NOT COMPILE_LOOKUPS, currentCourse#ENDIF , "Golf MP")
//		GOLF_SETUP_MP_DEBUG_WIDGETS(currentGame)
	#ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		IF IS_PED_DUCKING(PLAYER_PED_ID()) // forces player to not crouch
			SET_PED_DUCKING(PLAYER_PED_ID(), FALSE)
		ENDIF
	ENDIF
	
	IF GET_PACKED_STAT_BOOL(PACKED_MP_TOGGLE_EXPANDED_RADAR)
		CDEBUG1LN(DEBUG_GOLF,"Golf Minimap was expanded")
		SET_GOLF_STREAMING_FLAG(currentHelpers, GSF_TURNED_OFF_EXPANDED_MAP)
		SET_PACKED_STAT_BOOL(PACKED_MP_TOGGLE_EXPANDED_RADAR, FALSE)
		RUN_RADAR_MAP()
	ENDIF
	
	SET_GOLF_TRAIL_STATIC_DATA(currentHelpers)
	currentHelpers.eLastPuttStyle = SWING_STYLE_GREEN
	
	//gDisableRankupMessage = TRUE
	ANIMPOSTFX_STOP_ALL()
	SET_DPADDOWN_ACTIVE(FALSE)
	SET_MINIGAME_IN_PROGRESS(TRUE)
	NETWORK_SET_LOOK_AT_TALKERS(FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
	NETWORK_SET_TALKER_PROXIMITY(0)
	DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
	SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
	NET_NL()NET_PRINT("golf_mp: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(),FALSE)")				
	TOGGLE_STEALTH_RADAR(FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, TRUE)
	CLEAR_GLOBAL_GOLF_CONTROL_FLAG(GGCF_PLAYER_QUIT_MP_GOLF)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(CADDY, TRUE)
	INITAILIZE_GOLF_STATS()
	currentHelpers.iTimesPlayedGolf = GET_GOLF_STAT_ROUNDS_PLAYED()
	GOLF_TOGGLE_LOADING_SPINNER(TRUE)
	CDEBUG1LN(DEBUG_GOLF,"Number times played golf before this ", currentHelpers.iTimesPlayedGolf)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		WHILE NOT PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashMac, serverBD.iPosixTime)
			WAIT(0)
		ENDWHILE
	ELSE
		WHILE serverBD.iPosixTime = -1 //waiting for server to get info
			WAIT(0)
		ENDWHILE
	ENDIF
	
	DO_SCREEN_FADE_OUT(250)
	
	IF DID_I_JOIN_MISSION_AS_SPECTATOR()
	OR IS_PLAYER_SCTV(PLAYER_ID())
		CDEBUG1LN(DEBUG_GOLF,"I am a fucking spectator")
		bAllowLeaderboardUpload = FALSE
		bGolfersAddedToGameLocally = TRUE
		GOLF_MANAGE_SPECTATOR(serverBD, playerBD, sPlayerNames, currentGame, currentPlayerFoursome, currentCourse, currentHelpers, currentPlayers)
		IF IS_TRANSITION_ACTIVE()
			TAKE_CONTROL_OF_TRANSITION(FALSE, FALSE)
		ENDIF
		SET_MG_READY_FOR_TRANSITION_STATE_FM_SWOOP_DOWN()
		GOLF_TOGGLE_LOADING_SPINNER(FALSE)
	ELSE
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
	ENDIF
	
	INIT_GOLF_XP_TRACKER(golfXPTracker)
	
	CLEAR_AREA_OF_PEDS(GET_GOLF_COURSE_CENTER(currentCourse), 400)
	currentHelpers.golfScenarioBlockingIndex = ADD_SCENARIO_BLOCKING_AREA(<<-1383.30615, 195.57402, 57.64289>>, <<-886.40259, -117.18439, 36.96122>>)
//	SET_PED_NON_CREATION_AREA(<<-1383.30615, 195.57402, 57.64289>>, <<-886.40259, -117.18439, 36.96122>>)
	
	WHILE TRUE
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		WAIT(0)
		
		IF GOLF_SHOULD_MAKE_PLAYER_HOST()
			CDEBUG1LN(DEBUG_GOLF, "Someone took host from desired, reset it")
			NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
		ENDIF
		
		IF (DID_I_JOIN_MISSION_AS_SPECTATOR()OR IS_PLAYER_SCTV(PLAYER_ID()))
		AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
			CDEBUG1LN(DEBUG_GOLF, "Spectator is host!!! Very bad don't do anything until a player takes it from him")
			
			IF NETWORK_GET_NUM_PARTICIPANTS() <= 1
				CDEBUG1LN(DEBUG_GOLF, "Only local spectator is left on script. Kill golf.")
				CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(FALSE, currentGame, currentCourse, currentPlayerFoursome, currentPlayers,  currentHelpers, DEFAULT, DEFAULT, FALSE)
			ENDIF
		ELSE
		
		MAINTAIN_CELEBRATION_PRE_LOAD(golfCelebrationData)
		
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() OR IS_SC_COMMUNITY_PLAYLIST_LAUNCHING(ciEVENT_TOURNAMENT_PLAYLIST) OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		OR ((IS_TRANSITION_ACTIVE() OR IS_SKYSWOOP_MOVING() OR IS_SKYSWOOP_IN_SKY()) AND golfCelebrationData.eCurrentStage = CELEBRATION_STAGE_SETUP)
			CDEBUG1LN(DEBUG_GOLF, "Golf Mp - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(FALSE, currentGame, currentCourse, currentPlayerFoursome, currentPlayers,  currentHelpers, DEFAULT, DEFAULT, FALSE)
		ELSE
		
		IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR() AND NOT IS_PLAYER_SCTV(PLAYER_ID())
			//Golf game has started and the plaeyer skipped the loading phase, kick player
			IF (GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) > GMS_PRE_INIT_WAIT AND GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) != GMS_REMATCH
				AND bHereForInit = FALSE)
				CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(FALSE, currentGame, currentCourse, currentPlayerFoursome, currentPlayers,  currentHelpers)
			ENDIF
		ENDIF
		
		IF NOT IS_SELECTOR_DISABLED()
			DISABLE_SELECTOR()
		ENDIF
		DISABLE_GOLF_CONTROLS(GOLF_SHOULD_MP_MENUS_BE_DISABLED(GET_GOLF_MP_SERVER_GOLF_STATE(serverBD), currentPlayerFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(currentPlayerFoursome) != GPS_BALL_IN_FLIGHT)
		DISABLE_GOLF_HUD(TRUE)
		IF IS_WAYPOINT_ACTIVE()
			SET_WAYPOINT_OFF()
		ENDIF
		IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		ENDIF
		IF IS_SCREEN_FADED_OUT()
			RESET_ADAPTATION()
		ENDIF
		
		SET_IDLE_KICK_DISABLED_THIS_FRAME()
		
		//GOLF_MP_HANDLE_NET_EVENTS(serverBD)

//			REPEAT 9 hole
//				DRAW_DEBUG_SPHERE(GET_GOLF_HOLE_PIN_POSITION(currentCourse, hole), GIMMIE_FORCE_RADIUS, 0, 0, 255, 50)
//				DRAW_DEBUG_SPHERE(GET_GOLF_HOLE_PIN_POSITION(currentCourse, hole), BALL_IN_HOLE_RADIUS, 255, 0, 0, 50)
//				DRAW_DEBUG_SPHERE(GET_GOLF_HOLE_PIN_POSITION(currentCourse, hole), 0.02, 0, 0, 0, 255)
//				DRAW_DEBUG_SPHERE(GET_GOLF_HOLE_PIN_POSITION(currentCourse, hole) + <<0,0,0.11>>, 0.02, 0, 255, 255, 255)
//			ENDREPEAT

		#IF IS_DEBUG_BUILD
			IF 	NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF bTestReplay
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
					SET_GOLF_MP_SERVER_CONTROL_FLAG2(serverBD, GCF2_GO_TO_REMATCH)
					
					INT iOppIndex
					REPEAT MP_GOLF_MAX_PLAYERS iOppIndex
//						IF iOppIndex = 0 OR iOppIndex = 1 OR iOppIndex = 2
//							SET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(serverBD, iOppIndex, 15)
//						ELSE
//							SET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(serverBD, iOppIndex, 15)
//						ENDIF
						SET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(serverBD, iOppIndex, 42+iOppIndex)
					ENDREPEAT
					
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
					CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(FALSE, currentGame, currentCourse, currentPlayerFoursome, currentPlayers,  currentHelpers, ciFMMC_END_OF_MISSION_STATUS_PASSED, TRUE)
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_R)
					GOLF_KNOCK_OVER_PED(PLAYER_PED_ID(), NULL)
				ENDIF
			ENDIF
			
			IF IS_GOLF_MP_SERVER_CONTROL_FLAG_SET2(serverBD, GCF2_GO_TO_REMATCH)
				//CDEBUG1LN(DEBUG_GOLF,"Server go to rematch override is set")
				IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_GO_TO_REMATCH)
					CDEBUG1LN(DEBUG_GOLF,"Setup variables to go to rematch screen")
					SET_GOLF_FOURSOME_CURRENT_HOLE(currentPlayerFoursome, 8)
					
					SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_GO_TO_REMATCH)
					bTestReplay = FALSE
				ENDIF
			ENDIF
		#ENDIF

		
		IF GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) >= GMS_PLAY_GOLF
		AND GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) != GMS_OUTRO_INIT
		AND GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) != GMS_OUTRO
		AND GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) != GMS_REMATCH
			
			IF 	NETWORK_IS_HOST_OF_THIS_SCRIPT()
				//in the middle of removing player
				IF serverBD.iPlayerWhoQuitIndex > -1
					//all players have started removing player
					IF IS_ALL_GOLF_CLIENT_CONTROL_FLAGS_SET2(currentPlayerFoursome, playerBD, currentPlayers, GFC2_DISPLAYING_QUITING_PLAYER)
						serverBD.iPlayerWhoQuitIndex  = -1
					ENDIF
				ELSE
					//no players are in the middle of removing players
					IF NOT IS_ANY_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(currentPlayerFoursome, playerBD, currentPlayers, GFC2_DISPLAYING_QUITING_PLAYER)
						serverBD.iPlayerWhoQuitIndex = GET_GOLF_MISSING_PLAYER(currentPlayerFoursome, currentPlayers)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, PARTICIPANT_ID_TO_INT(), GFC2_DISPLAYING_QUITING_PLAYER)
			AND serverBD.iPlayerWhoQuitIndex < 0 //everyone handled quiter, clear flag
				CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GFC2_DISPLAYING_QUITING_PLAYER)
			ENDIF
		ENDIF

		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_GOLF_MP_SERVER_MISSION_STATE(serverBD)
						
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				DEBUG_MESSAGE("CLIENT GAME_STATE_INI")
			BREAK
			
			// Main gameplay state
			CASE GAME_STATE_RUNNING			
				IF bDebugSpew DEBUG_MESSAGE("CLIENT GAME_STATE_RUNNING") ENDIF
				
				IF GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) > GMS_PRE_INIT_WAIT
				AND GET_GOLF_MP_SERVER_GOLF_STATE(serverBD)  != GMS_REMATCH
				AND NETWORK_GET_NUM_PARTICIPANTS() != GET_GOLF_FOURSOME_NUM_PLAYERS(currentPlayerFoursome)
				AND NOT bGolfersAddedToGameLocally
					GOLF_SETUP_NETWORK_PLAYERS(currentPlayers, currentPlayerFoursome)
					bGolfersAddedToGameLocally = TRUE
					CDEBUG1LN(DEBUG_GOLF,"Setup golf for ", GET_GOLF_FOURSOME_NUM_PLAYERS(currentPlayerFoursome), " players")
				ENDIF
				
				IF bGolfersAddedToGameLocally AND bDebugSpew
					INT iCheckPlayerExistIndex
					REPEAT 4 iCheckPlayerExistIndex
						IF NOT DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, iCheckPlayerExistIndex))
							CDEBUG1LN(DEBUG_GOLF,"Player ", iCheckPlayerExistIndex, " doesn't exsist ", NATIVE_TO_INT(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, iCheckPlayerExistIndex)) )
						ELSE
							CDEBUG1LN(DEBUG_GOLF,"Player ", iCheckPlayerExistIndex, " exsist! ", NATIVE_TO_INT(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(currentPlayerFoursome, iCheckPlayerExistIndex)) )
						ENDIF
					ENDREPEAT
				ENDIF

				IF GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) = GMS_EXIT_OK OR GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) = GMS_PLAY_GOLF
					//golf clients request to the server to enter quit state
					GOLF_MP_CLIENT_MANAGE_QUIT_REQUEST(currentGame, currentPlayerFoursome, currentHelpers, playerBD, currentPlayers)
					
					//Host script is in charage of changing to golf quit state
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						//When switching to different hole there is a syncing issue
						//This flags stops that from happening
						IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(currentPlayerFoursome) = GPS_ADDRESS_BALL
							CLEAR_BITMASK_ENUM_AS_ENUM(serverBD.golfServerControlFlags2, GCF2_NEW_GOLFER_IS_SET)
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_GOLF_MP_SERVER_GOLF_STATE(serverBD) < GMS_INIT_STREAMING_DONE
					START_TIMER_NOW(timerFailToLoad)
					IF TIMER_DO_ONCE_WHEN_READY(timerFailToLoad, 120.0)
					AND NOT IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_MP_PLAYERS_ADDED)
						CDEBUG1LN(DEBUG_GOLF,"Participant ", PARTICIPANT_ID_TO_INT(), " failed during load." )
						CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(FALSE, currentGame, currentCourse, currentPlayerFoursome, currentPlayers,  currentHelpers)
					ENDIF
				ENDIF
				
				SWITCH GET_GOLF_MP_SERVER_GOLF_STATE(serverBD)
				
					CASE GMS_PRE_INIT
						DEBUG_MESSAGE("GMS_PRE_INIT")
						// Wait here til we have everyone
						IF NOT IS_SCREEN_FADING_OUT() AND NOT IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_OUT(250)
						ENDIF
						IF currentHelpers.bGameIsReplay
							SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GMS_PRE_INIT_WAIT)
						ENDIF
					BREAK
					CASE GMS_PRE_INIT_WAIT
						CDEBUG1LN(DEBUG_GOLF, "GMS_PRE_INIT_WAIT")
						START_TIMER_NOW(timerPreInitWait)
						
						bHereForInit = TRUE
						IF IS_SCREEN_FADED_OUT()
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1370.668, 172.609, 56.895 >>)
							ENDIF
							
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							CLEAR_PRINTS()
							
							IF TIMER_DO_WHEN_READY(timerPreInitWait, 2.0)
								SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GMS_INIT)
							ENDIF
						ENDIF
					BREAK
					
					CASE GMS_INIT
						DEBUG_MESSAGE("GMS_INIT")
						GOLF_SETUP_CURRENT_COURSE(currentCourse)
						GOLF_STREAM_ASSETS(golfAssets)
						GOLF_STREAM_UI(currentHelpers)
						
						REQUEST_ADDITIONAL_TEXT("SP_GOLF", MINIGAME_TEXT_SLOT)
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							NEW_LOAD_SCENE_STOP()
						ENDIF
						REMOVE_AMBIENT_GOLF_FLAGS()
						SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GMS_INIT_STREAMING_DONE)
					BREAK

					CASE GMS_INIT_STREAMING_DONE
						DEBUG_MESSAGE("GMS_INIT_STREAMING_DONE")
						
						START_TIMER_NOW(timeForLoadSceneToFinish)
						
						//IF NOT IS_STREAMVOL_ACTIVE()
							IF GOLF_ASSETS_ARE_STREAMED(currentHelpers, golfAssets) 
								IF HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
									IF 	NEW_LOAD_SCENE_START_SPHERE(<< -1370.668, 172.609, 56.895 >>, 30, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
									OR TIMER_DO_WHEN_READY(timeForLoadSceneToFinish, 10) //shit is taking to long, just go ahead
										IF IS_TIMER_STARTED(timeForLoadSceneToFinish)
											CDEBUG1LN(DEBUG_GOLF,"NETWORK_UPDATE_LOAD_SCENE has returned true. Time to load is ", GET_TIMER_IN_SECONDS(timeForLoadSceneToFinish))
											CANCEL_TIMER(timeForLoadSceneToFinish)
										ENDIF
										IF ARE_ALL_PLAYERS_ALIVE(currentPlayerFoursome, currentPlayers)
											CDEBUG1LN(DEBUG_GOLF,"Done loading")
											SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GMS_INIT_DONE)
											CANCEL_TIMER(timerFailToLoad)
											IF NOT IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_MP_PLAYERS_ADDED)
												REMOVE_DECALS_IN_RANGE(GET_GOLF_COURSE_CENTER(currentCourse), 400)
												CLEAR_AREA_OF_VEHICLES(GET_GOLF_COURSE_CENTER(currentCourse), 400)
												
												GOLF_SETUP_CLUBS (currentGame.golfBag)
												
												SET_SCOREBOARD_TITLE(currentHelpers)
												SET_COURSE_PAR(currentHelpers)
												
												SET_GOLF_UI_FLAG(currentHelpers, GUC_MP_PLAYERS_ADDED)
												
												DEBUG_MESSAGE("GMS_INIT_PLACE - screen faded, moving player")
												DEBUG_MESSAGE("CLEAR PLAYER TASK FOR TELEPORT")
												SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
												SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
												
												SET_CURRENT_PED_WEAPON(GET_GOLF_FOURSOME_LOCAL_PLAYER_PED(currentPlayerFoursome), WEAPONTYPE_UNARMED, TRUE)
												SET_ENTITY_LOD_DIST(GET_GOLF_FOURSOME_LOCAL_PLAYER_PED(currentPlayerFoursome), 500)
												
												IF NOT DOES_ENTITY_EXIST(GET_GOLF_HOLE_FLAG(currentCourse, 0))
													CDEBUG1LN(DEBUG_GOLF,"Create Golf Flags")
													INT iHoleIndex
													REPEAT GET_GOLF_COURSE_NUM_HOLES(currentCourse) iHoleIndex
														CREATE_GOLF_FLAG(currentCourse, iHoleIndex)
													ENDREPEAT
												ENDIF
												
												bAllowOverideClock = TRUE
												SET_GOLF_FOURSOME_CURRENT_HOLE(currentPlayerFoursome, currentHelpers.iStartingHole)
											ENDIF
										ELSE
											CDEBUG1LN(DEBUG_GOLF,"Not all players are alive?!")
										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_GOLF,"Network update load scene is returning false")
									ENDIF
								ELSE
									CDEBUG1LN(DEBUG_GOLF,"Loading in golf text")
								ENDIF
							ENDIF
//						ELSE
//							CDEBUG1LN(DEBUG_GOLF,"Stream volume active")
//						ENDIF
					BREAK
					CASE GMS_INIT_DONE
						DEBUG_MESSAGE("GMS_INIT_DONE")
						IF ARE_ALL_PLAYERS_ALIVE(currentPlayerFoursome, currentPlayers)
						
							IF iHeadTextureLoadIndex < MP_GOLF_MAX_PLAYERS
								IF REGISTER_HEAD_TEXTURE_MP(iHeadTextureLoadIndex, currentPlayerFoursome, currentPlayers, myPedHeadshot, bIsHeadTexturedRegistered)
								OR iHeadTextureLoadFailSafe > 10
									iHeadTextureLoadIndex++
									bIsHeadTexturedRegistered = FALSE
									CDEBUG1LN(DEBUG_GOLF,"Frames needed to load headshot ", iHeadTextureLoadFailSafe)
									iHeadTextureLoadFailSafe = 0
								ELSE
									iHeadTextureLoadFailSafe++
								ENDIF
							ELSE
								GOLF_MANAGE_NAVIGATE_AI(currentCourse, currentPlayerFoursome)
								GOLF_INIT_CURRENT_HOLE(currentCourse, currentPlayerFoursome)
								
								// Sync some initial data rather than doing GOLF_MP_CLIENT_MOVE_TO_NEXT_HOLE()
								SET_GOLF_MP_CLIENT_LOCAL_BALL_POSITION (playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL_POSITION( currentPlayerFoursome ))
								SET_GOLF_MP_CLIENT_LOCAL_DISTANCE_TO_HOLE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_DISTANCE_TO_HOLE(currentPlayerFoursome))
								SET_GOLF_MP_CLIENT_LOCAL_LIE(playerBD, GET_GOLF_FOURSOME_LOCAL_PLAYER_LIE(currentPlayerFoursome))
								
								GOLF_ADD_NETWORK_PLAYERS_TO_UI(sPlayerNames, currentHelpers, currentCourse, currentPlayerFoursome, currentPlayers, !bHaveSavedNames)
								SET_GOLF_FOURSOME_STATE(currentPlayerFoursome, GGS_NAVIGATE_TO_SHOT)
								SET_GOLF_MINIMAP(currentPlayerFoursome, currentCourse)
								SET_GOLF_UI_FLAG(currentHelpers, GUC_REFRESH_NAVIGATE | GUC_REFRESH_PERMANENT | GUC_REFRESH_ADDRESS)
								SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GMS_PLAY_GOLF)
								TRIGGER_MUSIC_EVENT("MGGF_START")
								HIDE_ALL_PLAYER_BLIPS(TRUE)
								SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
								SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
								currentHelpers.bWantsTeleport = TRUE
								SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_PLAYER_HAS_TELEPORTED)
//								bRematchInit = FALSE
								bHaveSavedNames = TRUE
								GOLF_TOGGLE_LOADING_SPINNER(FALSE)
								
								IF NOT bDidFMGameStartCall
									IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
										SERVER_GET_WIND_FOR_HOLE(serverBD, currentGame, currentPlayerFoursome, g_FMMC_STRUCT.iWeather)
									ENDIF
									
									CLEAR_HELP()
									NEW_LOAD_SCENE_STOP()
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									currentHelpers.iMatchHistoryID = serverBD.iPosixTime
									DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_GOLF, serverBD.iPosixTime, serverBD.iHashMac)
									REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_GOLF()
									bDidFMGameStartCall = TRUE
								ENDIF
								
							ENDIF
						ENDIF

					BREAK
					CASE GMS_PLAY_GOLF
						SYNC_WIND_FOR_HOLE(serverBD, currentGame)
						MANAGE_GOLF_MP_PLAYER_INDEXES(currentPlayers)
						GOLF_MANAGE_PRE_UPDATE_UI(currentGame, currentHelpers)
						GOLF_MANAGE_PERMANENT_UI(currentGame, currentCourse, currentPlayerFoursome, currentHelpers)
						#IF IS_DEBUG_BUILD
							//DEBUG_MP_PLAYER_DATA_SYNC_DISPLAY(serverBD, playerBD, currentGame, currentPlayerFoursome, currentPlayers)
							//GOLF_DEBUG_DRAW(currentGame, currentCourse, currentPlayerFoursome)
							//SET_GOLF_DEBUG_PLAYER_SCORE(currentPlayerFoursome, currentHelpers, currentPlayers)
						#ENDIF
						
						DISPLAY_AREA_NAME(FALSE)
						SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GMS_PLAY_GOLF)
						
						IF bDoLocalQuit
						OR IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_KICK_PLAYER)
							IF NOT IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(currentPlayerFoursome, GOLF_PLAYER_REMOVED)
								//you are the last person to quit, in a game that started with more then one player
								IF GET_GOLF_FOURSOME_NUM_PLAYERS(currentPlayerFoursome) > 1
								AND GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(currentPlayerFoursome) = 1
									IF MPGlobals.g_MPBettingData.iYourTotalBet > 0
										BROADCAST_BETTING_MISSION_FINISHED_TIED()
									ENDIF
								ENDIF
							ENDIF
							
							SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_QUIT, <<0.0, 0.0, 0.0>>, contentID)
							SET_GOLF_UI_FLAG(currentHelpers, GUC_DISABLE_UI)
							
							CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(TRUE, currentGame, currentCourse, currentPlayerFoursome, currentPlayers,  currentHelpers, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
							
						ELIF ARE_ALL_PLAYERS_ALIVE(currentPlayerFoursome, currentPlayers)
						
							IF GOLF_SHOULD_HANDLE_DISCONNECTED_PLAYER(serverBD, currentPlayerFoursome)
								//remove players who quit
								CDEBUG1LN(DEBUG_GOLF, "Golfer ", serverBD.iPlayerWhoQuitIndex, " has left golf!!!")
								GOLF_MP_HANDLE_PLAYER_QUITING(serverBD, playerBD, currentPlayerFoursome, currentGame, currentHelpers, currentPlayers,
															  iDisplayQuitterIndex, bRecreatePlayercard, bDoLocalQuit)
							ENDIF
							
							CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_AI_HIT_BALL_THIS_FRAME)
							CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_BALL_JUST_HIT)
							
							IF bRecreatePlayercard
								GOLF_MP_SET_PLAYERCARDS(playerBD, currentCourse, currentPlayerFoursome, currentPlayers, currentHelpers, FALSE, IS_GOLF_FOURSOME_STATE_AT_END(currentPlayerFoursome))
								bRecreatePlayercard = FALSE
							ENDIF
							
							 //server wants to move to next hole for legit reasons
							IF (IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_MOVE_TO_NEXT_HOLE_MP)
							 //and the player hasn't cleared their 'done with hole flag' yet. Note: spectating players set their done with hole flag at the start of the hole
							AND IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_HOLE))
							//Or server wants to debug move
							OR ((IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE) OR IS_GOLF_MP_SERVER_CONTROL_FLAG_SET2(serverBD, GCF2_GO_TO_REMATCH))
							//And the player hasn't debug moved yet
							AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE))
								GOLF_MP_CLIENT_MOVE_TO_NEXT_HOLE(serverBD, playerBD, currentCourse, currentPlayerFoursome, currentPlayers, currentHelpers, currentGame)
								currentHelpers.bWantsTeleport = TRUE
							ENDIF
							
							#IF IS_DEBUG_BUILD
								//dont upload stats if debug skipping holes
								IF IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE) 
								OR IS_GOLF_MP_SERVER_CONTROL_FLAG_SET2(serverBD, GCF2_GO_TO_REMATCH)
									bAllowLeaderboardUpload = FALSE
								ENDIF
							#ENDIF
							
							//a litte extra for mp in address ball mode					
							IF (GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(currentPlayerFoursome) = GPS_ADDRESS_BALL)
								RESET_GOLF_EFFECTS_FLAGS(playerBD)
								CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MP_SYNC_SPLASH_TEXT)
								CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_BALL_JUST_LAND)
								CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_BALL_IN_CUP_SOUND)
								CLEAR_GOLF_UI_FLAG(currentHelpers, GUC_END_GAME_SPLASH_DISPLAYED)
								IF IS_GOLF_MP_APPROACH_CAM_RENDERING(currentHelpers) OR IS_GOLF_CAM_RENDERING(currentHelpers.camAddress)
									SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_VIEWING_ADDRESS_BALL)
								ENDIF
								
								//spectaing player is done with hole
								IF IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(currentPlayerFoursome, GOLF_PLAYER_REMOVED)
									SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_HOLE)
								ENDIF
								
								INT iCurrentPlayerParticipantIndex
								iCurrentPlayerParticipantIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(currentPlayers, GET_GOLF_FOURSOME_CURRENT_PLAYER(currentPlayerFoursome))
								IF IS_GOLF_NETWORK_PARTICIPANT_ACTIVE(iCurrentPlayerParticipantIndex)
									IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET2(playerBD, iCurrentPlayerParticipantIndex, GCF2_PLAYER_HAS_TELEPORTED)
									OR NOT IS_CURRENT_GOLFER_NEAR_HIS_BALL(currentPlayerFoursome)
										IF IS_GOLF_MP_APPROACH_CAM_RENDERING(currentHelpers)
											CDEBUG1LN(DEBUG_GOLF, "the player has not teleported yet and you are rendering the pan camera")
											SET_CAM_SPLINE_PHASE(currentHelpers.camMPApproach, 0.0)
										ENDIF
									ENDIF
								ENDIF
								
								IF GET_GOLF_FOURSOME_LOCAL_PLAYER(currentPlayerFoursome) = GET_GOLF_FOURSOME_CURRENT_PLAYER(currentPlayerFoursome)
									
									IF IS_ALL_GOLF_CLIENT_CONTROL_FLAGS_SET2(currentPlayerFoursome, playerBD, currentPlayers, GCF2_VIEWING_ADDRESS_BALL)
										IF GOLF_CHECK_PLAYER_NEAR_BALL(currentGame, currentPlayerFoursome)
											SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_PLAYER_HAS_TELEPORTED)
										ENDIF
									ENDIF
									
									IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(currentPlayerFoursome) = LIE_CUP
										CDEBUG1LN(DEBUG_GOLF, "Player is in adress state with LIE_CUP! Bad news. End hole for player for safety.")
										SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_SHOT)
										SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_HOLE)
									ENDIF
								ENDIF
							ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(currentPlayerFoursome) > GPS_BALL_IN_FLIGHT
								CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_PLAYER_HAS_TELEPORTED)
							ELSE
								CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_VIEWING_ADDRESS_BALL)
							ENDIF
							
							// Move to next shooter?
							IF GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD) != GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(currentPlayers, GET_GOLF_FOURSOME_CURRENT_PLAYER( currentPlayerFoursome ))
							AND GET_GOLF_FOURSOME_STATE(currentPlayerFoursome) != GGS_SCORECARD_END_GAME
								GOLF_MP_CLIENT_MOVE_TO_NEXT_SHOOTER(serverBD, playerBD, currentHelpers, currentCourse, currentPlayerFoursome, currentPlayers)
								
								currentHelpers.bWantsTeleport = TRUE
								IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
									SERVER_GET_WIND_FOR_HOLE(serverBD, currentGame, currentPlayerFoursome, g_FMMC_STRUCT.iWeather)
								ENDIF
							ENDIF
							
							// Move to next shot?
							IF IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_MOVE_TO_NEXT_SHOT_MP)
								CDEBUG1LN(DEBUG_GOLF,"Server move to next shot is set")
								IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MOVE_TO_NEXT_SHOT_MP)	
									CDEBUG1LN(DEBUG_GOLF,"Move to next shot")
									IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_SHOT)
										GOLF_MP_CLIENT_MOVE_TO_NEXT_SHOT(serverBD, playerBD, currentHelpers, currentPlayerFoursome)
										CLEAR_AREA_OF_PEDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(currentPlayerFoursome), 20)
									ENDIF
									
									IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
										SERVER_GET_WIND_FOR_HOLE(serverBD, currentGame, currentPlayerFoursome, g_FMMC_STRUCT.iWeather)
									ENDIF
									currentHelpers.bWantsTeleport = TRUE
									
									SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MOVE_TO_NEXT_SHOT_MP)
								ENDIF
								CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYER_DONE_WITH_SHOT)
							ELSE
								CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MOVE_TO_NEXT_SHOT_MP)
							ENDIF
							
							IF currentHelpers.bWantsTeleport AND GET_GOLF_FOURSOME_STATE(currentPlayerFoursome) != GGS_SCORECARD_END_HOLE 
							AND GET_GOLF_FOURSOME_STATE(currentPlayerFoursome) != GGS_SCORECARD_END_GAME
							AND IS_ALL_GOLF_CLIENT_CONTROL_FLAGS_SET2(currentPlayerFoursome, playerBD, currentPlayers, GCF2_VIEWING_ADDRESS_BALL)
								currentHelpers.bWantsTeleport = FALSE
								IF NOT IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(currentPlayerFoursome, GOLF_PLAYER_REMOVED)
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE) //make sure isn't forzen before teleport
								ENDIF
								GOLF_MP_CLIENT_UPDATE_LOCAL_PLAYER_TELEPORT(serverBD, currentGame, currentCourse, currentPlayerFoursome, currentHelpers, currentPlayers)
								CDEBUG1LN(DEBUG_GOLF,"Set has teleported flag")
								SET_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_PLAYER_HAS_TELEPORTED)
							ELIF currentHelpers.bWantsTeleport
								//you want to teleport but havn't yet, don't allow camera switching
								currentHelpers.bAllowPlayerViewCam = FALSE
							ENDIF
							
							// Play Golf!
							IF (NOT IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_SCOREBOARD_DISPLAYED) //scorecard is up, don't update
							OR GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(currentPlayerFoursome) != HUMAN_LOCAL) //well its not your turn, okay to update
							OR IS_GOLF_FOURSOME_STATE_AT_END(currentPlayerFoursome) //always update end of game
								SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GOLF_PLAY_GOLF(currentGame, currentCourse, currentPlayerFoursome, currentPlayers, currentHelpers))
							ENDIF
							
							// Check to see if we should end
							IF GET_GOLF_MP_CLIENT_GOLF_STATE(playerBD, PARTICIPANT_ID_TO_INT()) = GMS_CLEANUP
							OR GET_GOLF_MP_CLIENT_GOLF_STATE(playerBD, PARTICIPANT_ID_TO_INT()) = GMS_OUTRO_INIT
								//tell sever to go to outro scene
								CDEBUG1LN(DEBUG_GOLF,"Golf play golf returned GMS_OUTRO_INIT OR GMS_CLEANUP ", GET_GOLF_MP_CLIENT_GOLF_STATE(playerBD, PARTICIPANT_ID_TO_INT()))
								SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(currentPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(currentPlayerFoursome)), GCF_MP_OUTRO)
							ENDIF
							
							// Check to see if local / current player is done with shot or hole
							IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(currentPlayerFoursome) = HUMAN_LOCAL_MP
								GOLF_MP_CLIENT_UPDATE_LOCAL_PLAYER_SHOT(serverBD, playerBD, currentHelpers, currentPlayerFoursome, currentGame)
							ENDIF
							
							GOLF_MP_CLIENT_MANAGE_AUTO_SWING_TIMER(serverBD, playerBD, currentPlayerFoursome, currentCourse, currentHelpers, currentPlayers)
							
							//Sometimes we want players to be invisible
							MANAGE_GOLFER_INVISIBLE(playerBD, currentPlayerFoursome, currentHelpers, currentPlayers)
							
							//Manage golf objects so the are consistent
							GOLF_MP_CLIENT_MANAGE_GOLF_OBJECTS(playerBD, currentPlayerFoursome, currentHelpers, currentGame, currentCourse, currentPlayers)
							
							MANAGE_MP_GOLF_BALL_SYNCING(serverBD, currentPlayerFoursome)
							
							// Synchronize data
							REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS( currentPlayerFoursome ) playerIndex
								IF GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT( currentPlayers, playerIndex) != PARTICIPANT_ID_TO_INT()
								AND NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(currentPlayerFoursome, playerIndex, GOLF_PLAYER_REMOVED)
									GOLF_MP_CLIENT_SYNC_REMOTE_PLAYER_DATA(serverBD, playerBD, currentGame, currentPlayerFoursome, currentHelpers, currentPlayers, playerIndex)
								ELSE
									GOLF_MP_CLIENT_SYNC_LOCAL_PLAYER_DATA(playerBD, currentPlayerFoursome, currentGame)
								ENDIF
							ENDREPEAT
							
							//Syc headset display on player card
							GOLF_MP_SYNC_HEADSETS(golfHeadsetStates, currentPlayerFoursome, currentHelpers, currentPlayers)
							
							//At end of hole, wait for player to put away scorecard.
							GOLF_MP_MANAGE_END_OF_HOLE_SCORECARD(serverBD, playerBD, currentPlayerFoursome, currentCourse, currentGame, currentHelpers, currentPlayers)
							
							//Synchronize blips
							GOLF_MP_CLIENT_MANAGE_BLIPS(currentPlayerFoursome)
							
							//Synchronize particle effects
							IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(currentPlayerFoursome) = HUMAN_NETWORK
								GOLF_MP_CLIENT_SYNCHRONIZE_PARTICLE_EFFECTS(playerBD, currentGame, currentPlayerFoursome, currentCourse, currentPlayers)
								GOLF_MP_CLIENT_SYNCHRONIZE_SOUNDS(playerBD, currentPlayerFoursome, currentGame, currentPlayers, currentCourse)
								GOLF_MP_CLIENT_SYNCHRONIZE_BALL_CONTACT(playerBD, currentPlayerFoursome, currentPlayers)
							ENDIF
							
							//Synchronize splash text
							GOLF_MP_SYNC_SPLASH_TEXT(currentPlayerFoursome, currentHelpers, playerBD, currentPlayers)
							
							// Error checking
							#IF IS_DEBUG_BUILD
								//DEBUG_GOLF_MP_CLIENT_FLAGS(serverBD, playerBD, mySavedGCF, savedServerGCF)
								
								IF NOT (IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE) OR IS_GOLF_MP_SERVER_CONTROL_FLAG_SET2(serverBD, GCF2_GO_TO_REMATCH))
							 	AND IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE)
									CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE)
								ENDIF
							#ENDIF
							
//							//Manage pause menu
//							IF IS_PAUSE_MENU_ACTIVE()
//								IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(currentPlayerFoursome) >= GPS_BALL_IN_FLIGHT
//									CDEBUG1LN(DEBUG_GOLF,"Pause menu is active. State is ball in flight or greater")
//									SET_FRONTEND_ACTIVE(FALSE)
//									DISABLE_FRONTEND_THIS_FRAME() //don't allow pause menu when ball is in air, like in sp
//								ENDIF
//							ENDIF
							
							// Manage scoreboard display
							IF NOT IS_GOLF_FOURSOME_STATE_AT_END(currentPlayerFoursome)
							AND NOT IS_SCREEN_FADED_OUT() AND NOT IS_PAUSE_MENU_ACTIVE()
							AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MP_OUTRO)
							
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB)
								AND NOT IS_GOLF_MP_SCOREBOARD_FORCED_CLOSED(serverBD, currentPlayerFoursome, currentHelpers)
								AND NOT IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_SCOREBOARD_DISPLAYED)
								AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
								
									CLEAR_HELP()
									DISPLAY_RADAR(FALSE)
									DISPLAY_SCOREBOARD(currentHelpers, TRUE)
									GOLF_TRAIL_SET_ENABLED(FALSE)
									PLAY_SOUND_FRONTEND(-1, "HIGHLIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
									SET_PUTTMETER (currentHelpers, currentHelpers.golfUI)
									SETUP_GOLF_SCOREBOARD_CONTROLS(currentHelpers)
									
								ELIF IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_SCOREBOARD_DISPLAYED)
																	
									IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(currentPlayerFoursome) = HUMAN_NETWORK 
									AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(currentPlayerFoursome) <= GPS_SWING_METER
										//Make sure adress camera is still updating when player is spectating others
										ADJUST_ADDRESS_CAMERA(currentPlayerFoursome, currentPlayerFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(currentPlayerFoursome)], currentHelpers , GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(currentPlayerFoursome))
									ENDIF
									
									GOLF_TRAIL_SET_ENABLED(FALSE)
									
									IF IS_GOLF_MP_SCORECARD_QUIT_PRESSED()
									OR IS_GOLF_MP_SCOREBOARD_FORCED_CLOSED(serverBD, currentPlayerFoursome, currentHelpers)
										IF currentHelpers.spectatorCamState != GOLF_SPECTATOR_POV_CAM
										OR IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(currentPlayerFoursome)
											DISPLAY_RADAR(TRUE)
										ENDIF
										DISPLAY_SCOREBOARD(currentHelpers, FALSE)
										CLEAR_GOLF_UI_FLAG(currentHelpers, GUC_REFRESH_INPUT)
										IF currentHelpers.bGreenPreviewCam = 0
											SET_GOLF_UI_FLAG(currentHelpers, GUC_REFRESH_SWING)
											SET_GOLF_UI_FLAG(currentHelpers, GUC_REFRESH_GOLF_TRAIL)
										ENDIF
									ENDIF
									
									IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(currentPlayerFoursome) <= GPS_SWING_METER
										//If you are quit out of the scoreboard, display adress help text right away
										GOLF_MANAGE_ADDRESS_HELP(currentCourse, currentPlayerFoursome, currentHelpers, currentGame, currentPlayers[GET_GOLF_FOURSOME_CURRENT_PLAYER(currentPlayerFoursome)])
										MANAGE_PLAYERCARD(currentHelpers, currentGame)
									ENDIF
									
									GOLF_DISPLAY_CONTROLS(currentHelpers, TRUE)
								ENDIF
							ENDIF
							
							//if client requested to see the quit menu
							IF IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(currentPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(currentPlayerFoursome)), GCF_MP_START_QUIT_MENU)
								SET_WARNING_MESSAGE_WITH_HEADER("QUIT", "QUIT_DET", FE_WARNING_YES | FE_WARNING_NO)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT) // dont allow map to pop up
								//KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
								IF IS_HELP_MESSAGE_BEING_DISPLAYED()
									CLEAR_HELP()
								ENDIF
								IF IS_MESSAGE_BEING_DISPLAYED()
									CLEAR_PRINTS()
								ENDIF
								IF NOT IS_RADAR_HIDDEN()
									DISPLAY_RADAR(FALSE)
								ENDIF
								GOLF_TRAIL_SET_ENABLED(FALSE)
								
								IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
									//END GOLF
									SET_FRONTEND_ACTIVE(FALSE)
									bDoLocalQuit = TRUE
									CALL_END_OF_MISSION_QUIT_TELEMETRY()
									
								ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
								OR GET_GOLF_FOURSOME_STATE(currentPlayerFoursome) = GGS_SCORECARD_END_GAME //OR GET_GOLF_FOURSOME_STATE(currentPlayerFoursome) = GGS_SCORECARD_END_HOLE
								OR IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_END_GOLF_SCOREBOARD)
								//If its the local players turn, turn off the scoreboard if he is going to auto swing, or if it is the start of his turn
								OR (IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(currentPlayerFoursome) 
									AND (IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(currentPlayerFoursome, GOLF_PLAYER_TIME_OUT)))
									
									IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(currentPlayerFoursome)
										SET_GOLF_UI_FLAG(currentHelpers, GUC_REFRESH_GOLF_TRAIL)
									ENDIF
									
									CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(currentPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(currentPlayerFoursome)), GCF_MP_START_QUIT_MENU)
									CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_MP_START_QUIT_MENU)
									CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_STOP_INPUT)
									
									IF NOT IS_GOLF_FOURSOME_STATE_AT_END(currentPlayerFoursome)
									AND NOT IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_SCOREBOARD_DISPLAYED)
										DISPLAY_RADAR(TRUE)
									ENDIF
								ENDIF
							ENDIF
						
						ELSE 
							CDEBUG1LN(DEBUG_GOLF,"Some one is dead?")
							
						ENDIF
						
						//Wait until the player is lined up with the ball before fading back in
						IF IS_SCREEN_FADED_OUT()
						AND ((GET_GOLF_FOURSOME_STATE(currentPlayerFoursome) = GGS_SCORECARD_END_HOLE OR GET_GOLF_FOURSOME_STATE(currentPlayerFoursome) =  GGS_SCORECARD_END_GAME
							AND NOT IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_DISABLE_FADE_IN))
						OR (GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(currentPlayerFoursome) > GPS_ADDRESS_BALL 
							AND (GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(currentPlayerFoursome) < GPS_BALL_AT_REST OR GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(currentPlayerFoursome) = GPS_BALL_IN_HOLE_CELEBRATION)))
							START_TIMER_NOW(currentHelpers.navTimer)
							
							CDEBUG1LN(DEBUG_GOLF,"Fading in from golf_mp.sc timer is at ", GET_TIMER_IN_SECONDS(currentHelpers.navTimer))
							IF TIMER_DO_ONCE_WHEN_READY(currentHelpers.navTimer, 0.75)
								CDEBUG1LN(DEBUG_GOLF,"Fading in from golf_mp.sc")
								DO_SCREEN_FADE_IN(250)
								SET_SPLASH_TEXT(currentHelpers)
								CLEAR_GOLF_UI_FLAG(currentHelpers, GUC_DISABLE_FADE_IN)
							ENDIF
						ENDIF
					BREAK
					
					CASE GMS_OUTRO_INIT

						CDEBUG1LN(DEBUG_GOLF,"Outro Init")
						
						SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GMS_OUTRO_INIT)
						RESTART_TIMER_NOW(currentHelpers.uiTimer)
						CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(currentPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(currentPlayerFoursome)), GCF_MP_OUTRO)
						CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(currentPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(currentPlayerFoursome)), GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
						CLEAR_GOLF_UI_DISPLAY(currentHelpers, GOLF_DISPLAY_PLAYERCARD)
						SET_GOLF_FOURSOME_LOCAL_PLAYER_STATE(currentPlayerFoursome, GPS_PLACE_BALL)
						SET_GOLF_FOURSOME_STATE(currentPlayerFoursome, GGS_TAKING_SHOT)
						DISABLE_GOLF_MINIMAP()
						
						SET_END_OF_HOLE_CAMERA(currentHelpers, 7, FALSE, TRUE)
						MANAGE_GOLFER_INVISIBLE(playerBD, currentPlayerFoursome, currentHelpers, currentPlayers, TRUE)
						
						IF NETWORK_IS_IN_SPECTATOR_MODE()
							SET_IN_SPECTATOR_MODE(FALSE, tempPed)
						ENDIF
						
						//Display winner
						IF NOT IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_END_GAME_SPLASH_DISPLAYED)
						AND IS_GOLF_END_SCREEN_READY(golfCelebrationData)
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							
							SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
							TRIGGER_CELEBRATION_PRE_LOAD(golfCelebrationData)
							
							IF NOT IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(currentPlayerFoursome, GOLF_PLAYER_REMOVED)
								
								iBetWinnings = 0
//								iPlayersScore = GET_GOLF_PLAYER_CURRENT_SCORE(currentPlayers[GET_GOLF_FOURSOME_LOCAL_PLAYER(currentPlayerFoursome)])
								iPlacement = GET_GOLFER_PLACEMENT(currentPlayerFoursome, currentPlayers, bTiedPlacement)
								iWinnerIndex = GET_GOLF_WINNER(currentPlayerFoursome, currentPlayers)
								iWinnerNetIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(currentPlayers, iWinnerIndex)
								
								IF NOT IS_GOLF_STREAMING_FLAG_SET(currentHelpers, GSF_END_GAME_XP_CALCULATED)
									IF GET_GOLF_FOURSOME_NUM_PLAYERS(currentPlayerFoursome) > 1
										HANDLE_GOLF_BETTING_WINS(currentPlayerFoursome, currentPlayers, iBetWinnings, iPlacement)
									ENDIF
								
									IF GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(currentPlayerFoursome) = 1
										IF GET_GOLF_FOURSOME_NUM_PLAYERS(currentPlayerFoursome) > 1
											CDEBUG1LN(DEBUG_GOLF,"Every one else quit golf, i guess you won")
											currentHelpers.iXPGained += GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_GOLF",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_GOLF_WON, ROUND(g_sMPTunables.fxp_tunable_Minigames_Golf  * 500))
										ENDIF
									ELIF IS_GOLF_GAME_TIE(currentPlayerFoursome, currentPlayers)
										CDEBUG1LN(DEBUG_GOLF,"No extra xp if tie")
										
									ELIF iWinnerIndex = GET_GOLF_FOURSOME_LOCAL_PLAYER(currentPlayerFoursome)
										currentHelpers.iXPGained += GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_GOLF",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_GOLF_WON, ROUND(g_sMPTunables.fxp_tunable_Minigames_Golf  * 500))
									ENDIF
									
									SET_GOLF_STREAMING_FLAG(currentHelpers, GSF_END_GAME_XP_CALCULATED)
								ENDIF
								
								SET_GOLF_UI_FLAG(currentHelpers, GUC_END_GAME_SPLASH_DISPLAYED)
								
								INT iNumHolesPlayed, IncStrength
								iNumHolesPlayed = currentHelpers.iEndingHole - currentHelpers.iStartingHole
								IF iNumHolesPlayed <= 3
									IncStrength = 1
								ELIF iNumHolesPlayed <= 7
									IncStrength = 2
								ELSE //all 8 holes
									IncStrength = 3
								ENDIF
								
								//Increase streangth based on number of holes played
								INC_GOLF_PLAYER_STRENGTH(IncStrength)
								SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PLAY_GOLF)
								
								IF GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(currentPlayerFoursome) = 1
//									IF IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_DISPLAY_NEW_BEST_SCORE)
//										PLAY_GOLF_SPLASH("GOLF_NEW_HIGH")
//									ENDIF
								ELIF IS_GOLF_GAME_TIE(currentPlayerFoursome, currentPlayers)
									CDEBUG1LN(DEBUG_GOLF,"Tie game")
									bDrawGame = TRUE
								ELIF iWinnerIndex = GET_GOLF_FOURSOME_LOCAL_PLAYER(currentPlayerFoursome) //if the player won
									CDEBUG1LN(DEBUG_GOLF,"This player won")
									INC_GOLF_STAT_NUM_WINS()
								ELSE //if someone else won
									CDEBUG1LN(DEBUG_GOLF,"Player who won is ", sPlayerNames[iWinnerIndex])
									INC_GOLF_STAT_NUM_LOSSES()
								ENDIF
								SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_DONE_WITH_GAME)
								CLEAR_GOLF_SPLASH()
							ELSE //player is spectator
								
								iBetWinnings = 0
								iPlacement = 0
								bTiedPlacement = FALSE
								iWinnerIndex = GET_GOLF_WINNER(currentPlayerFoursome, currentPlayers)
								iWinnerNetIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(currentPlayers, iWinnerIndex)
								bDrawGame = IS_GOLF_GAME_TIE(currentPlayerFoursome, currentPlayers)
								
								SET_GOLF_UI_FLAG(currentHelpers, GUC_END_GAME_SPLASH_DISPLAYED)
								SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_DONE_WITH_GAME)
								CLEAR_GOLF_SPLASH()
							ENDIF
						ENDIF
						
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							IF IS_ALL_GOLF_CLIENT_CONTROL_FLAGS_SET(currentPlayerFoursome, playerBD, currentPlayers, GCF_DONE_WITH_GAME)
								SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GMS_OUTRO)
								SET_GOLF_MP_SERVER_GOLF_STATE(serverBD, GMS_OUTRO)
							ENDIF
						ENDIF
					BREAK
					
					CASE GMS_OUTRO
						CDEBUG1LN(DEBUG_GOLF,"GMS_OUTRO")
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP()
						ENDIF
						SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GMS_OUTRO)
						SET_GOLF_UI_FLAG(currentHelpers, GUC_DISABLE_UI)

//						MANAGE_GOLFER_INVISIBLE(playerBD, currentPlayerFoursome, currentHelpers, currentPlayers, TRUE)
						
						IF IS_GOLF_MP_ALL_CLIENTS_AT_GOLF_STATE(playerBD, currentPlayers, currentPlayerFoursome, GMS_OUTRO)
							IF DISPLAY_GOLF_END_SCREEN(golfCelebrationData, camGolfEndScreen, golfXPTracker, iPlacement, bTiedPlacement, currentHelpers.iXPGained,
														GET_GOLF_FOURSOME_NUM_PLAYERS(currentPlayerFoursome) > 1, bDrawGame, iWinnerNetIndex, iBetWinnings,
														IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(currentPlayerFoursome, GOLF_PLAYER_REMOVED), dPadVars)
								
								SET_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MOVE_TO_REMATCH_SCREEN)
								
								SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, contentID)
								
								CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(FALSE, currentGame, currentCourse, currentPlayerFoursome, currentPlayers,  currentHelpers, ciFMMC_END_OF_MISSION_STATUS_PASSED, FALSE, FALSE)
//								IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//									IF IS_ALL_GOLF_CLIENT_CONTROL_FLAGS_SET(currentPlayerFoursome, playerBD, currentPlayers, GCF_MOVE_TO_REMATCH_SCREEN)
//										SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GMS_REMATCH)
//										SET_GOLF_MP_SERVER_GOLF_STATE(serverBD, GMS_REMATCH)
//									ENDIF
//								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE GMS_REMATCH
						CDEBUG1LN(DEBUG_GOLF,"GMS_REMATCH")
//						SET_GOLF_MP_CLIENT_LOCAL_GOLF_STATE(playerBD, GMS_REMATCH)
//						
//						bAllowOverideClock = FALSE
//						IF NOT bRematchInit
//							GOLF_CLEANUP_ALL_CAMERA(currentHelpers)
//							RESTART_TIMER_NOW(timerFailToLoad)
//							bRematchInit = TRUE
//						ENDIF
//						
//						IF IS_NEW_LOAD_SCENE_ACTIVE()
//							NEW_LOAD_SCENE_STOP()
//						ENDIF
//						IF DID_I_JOIN_MISSION_AS_SPECTATOR()
//							CLEAR_I_JOIN_MISSION_AS_SPECTATOR()
//						ENDIF
//						
//						IF bDoLocalQuit
//						OR IS_PLAYER_SCTV(PLAYER_ID())
//							CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(TRUE, currentGame, currentCourse, currentPlayerFoursome, currentPlayers,  currentHelpers, ciFMMC_END_OF_MISSION_STATUS_PASSED)
//						ELSE
//							IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//								IF NOT HAS_NET_TIMER_STARTED(serverBD.timeLeaderboardTimeOut)
//									START_NET_TIMER(serverBD.timeLeaderboardTimeOut)
//								ENDIF
//							
//								FMMC_EOM_SERVER_RESET_PLAYER_COUNTS(sPlayerCounts)
//								INT iRematchLoop
//								REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iRematchLoop
//									IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iRematchLoop))
//										FMMC_EOM_SERVER_PLAYER_PROCESSING(sPlayerCounts, iRematchLoop)
//									ENDIF
//								ENDREPEAT
//								
//								FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING(sPlayerCounts, serverBD.sServerFMMC_EOM, FMMC_TYPE_MG_GOLF, 0)
//							ENDIF
//							
//							IF HAS_NET_TIMER_STARTED(serverBD.timeLeaderboardTimeOut)
//								//IF MAINTAIN_MINI_GAME_END_OF_MISSION_SCREEN(sEndOfMission, serverBD.sServerFMMC_EOM, serverBD.timeLeaderboardTimeOut.timer, VIEW_LEADERBOARD_TIME, "LBD_TIMEOUT")
//								IF MAINTAIN_END_OF_MISSION_SCREEN(sEndOfMission, serverBD.sServerFMMC_EOM,
//															GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),serverBD.timeLeaderboardTimeOut.timer) >= VIEW_LEADERBOARD_TIME,
//															FALSE, FALSE, FALSE, FALSE, FALSE, serverBD.timeLeaderboardTimeOut.timer, VIEW_LEADERBOARD_TIME, "END_LBD_CONT", TRUE)
//									//Go to cleanup state
//									bDoLocalQuit = TRUE
//								ELSE
//									
//								ENDIF
//							ENDIF
//							
//							SET_END_OF_HOLE_CAMERA(currentHelpers, 7, FALSE, TRUE)
//							MANAGE_GOLFER_INVISIBLE(playerBD, currentPlayerFoursome, currentHelpers, currentPlayers, TRUE)
//						ENDIF
						
					BREAK
					
					CASE GMS_EXIT_OK
						CDEBUG1LN(DEBUG_GOLF,"GMS_EXIT_OK")
					BREAK
					
					CASE GMS_CLEANUP
						CDEBUG1LN(DEBUG_GOLF, "GMS_CLEANUP")
					BREAK
					
				ENDSWITCH
				
				IF IS_GOLF_SPLASH_DISPLAYING() AND IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				
				MANAGE_GOLF_UI_DISPLAY(currentHelpers)
				IF GET_GOLF_MP_CLIENT_GOLF_STATE(playerBD, PARTICIPANT_ID_TO_INT()) >= GMS_PLAY_GOLF
				AND NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(currentPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(currentPlayerFoursome)), GCF_MP_START_QUIT_MENU)
				AND (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN())
					
					IF IS_GOLF_CONTROL_FLAG_SET(currentGame, GCF_DISPLAY_QUITTER) AND iDisplayQuitterIndex > -1
						PRINT_GOLF_MP_PLAYER_WHO_QUIT_MATCH(sPlayerNames, iDisplayQuitterIndex)
						iDisplayQuitterIndex = -1
						GOLF_TRAIL_SET_ENABLED(FALSE)
						DISPLAY_RADAR(FALSE)
					ENDIF
					
					IF NOT IS_GOLF_UI_FLAG_SET(currentHelpers, GUC_DISABLE_UI)
					AND NOT IS_PAUSE_MENU_ACTIVE() AND NOT IS_TRANSITION_ACTIVE()
						IF SHOULD_GOLF_SC_LEADERBOARD_DSIPLAY(currentPlayerFoursome, currentHelpers)
							GOLF_DISPLAY_SOCIAL_CLUB_LEADERBOARD(currentHelpers.leaderboardUI)
							MANAGE_GOLF_SC_LEADERBOARD_INPUT(currentPlayerFoursome, currentHelpers)
						ELSE
							DRAW_SCALEFORM_MOVIE_FULLSCREEN(currentHelpers.golfUI,255,255,255,0)
						ENDIF
					ENDIF
					
					IF IS_PAUSE_MENU_ACTIVE()
						CLEAR_GOLF_SPLASH()
					ENDIF
					
					UPDATE_GOLF_SPLASH()
					IF IS_GOLF_CONTROL_FLAG_SET(currentGame, GCF_DISPLAY_QUITTER) AND NOT IS_GOLF_SPLASH_DISPLAYING()
						CDEBUG1LN(DEBUG_GOLF,"No longer displaying quitter")
						SET_GOLF_UI_FLAG(currentHelpers, GUC_REFRESH_GOLF_TRAIL)
						SET_GOLF_UI_FLAG(currentHelpers, GUC_REFRESH_ADDRESS)
						CLEAR_GOLF_CONTROL_FLAG(currentGame, GCF_DISPLAY_QUITTER)
						DISPLAY_RADAR(TRUE)
					ENDIF
				ENDIF
				
				INT iLinesToMoveUp 
				iLinesToMoveUp = GOLF_INSTRUCTIONS_BUTTONS_GET_LINES(currentPlayerFoursome)
				SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(iLinesToMoveUp)
				
			BREAK
		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

			GOLF_MP_GAME_STATE newServerState = GAME_STATE_UNDEFINED
			GOLF_MINIGAME_STATE newServerGolfState = GMS_UNDEFINED
			
			#IF IS_DEBUG_BUILD
			DEBUG_GOLF_MP_SERVER_FLAGS(playerBD, savedGCF, savedBallPositions)
			GOLF_MANAGE_MP_DEBUG_INPUT(serverBD, playerBD, currentPlayerFoursome, currentGame, currentCourse, currentHelpers, currentPlayers)
			#ENDIF
			
			// Do Lockstep
			// Check if all clients are on a different state than the server
			newServerState = GET_CLIENT_STATE_SYNC(playerBD)
			IF newServerState != GAME_STATE_UNDEFINED AND newServerState > GET_GOLF_MP_SERVER_MISSION_STATE(serverBD)
				IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"Server State moving to ",newServerState) ENDIF
				SET_GOLF_MP_SERVER_MISSION_STATE(serverBD, newServerState)
			ENDIF
			
			newServerGolfState = GET_CLIENT_GOLF_STATE_SYNC(serverBD, playerBD)
			IF newServerGolfState != GMS_UNDEFINED AND newServerGolfState > GET_GOLF_MP_SERVER_GOLF_STATE(serverBD)
				IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"Server Golf State moving to ",newServerGolfState) ENDIF
				SET_GOLF_MP_SERVER_GOLF_STATE(serverBD, newServerGolfState)
			ENDIF
			
			
			SWITCH GET_GOLF_MP_SERVER_MISSION_STATE(serverBD)
				CASE GAME_STATE_INI
					DEBUG_MESSAGE("SERVER GAME_STATE_INI")
					SET_GOLF_MP_SERVER_CURRENT_HOLE(serverBD, currentHelpers.iStartingHole)
					
					serverBD.iPlayerWhoQuitIndex = -1 //no one has quit
					iNumMissionPlayers = NETWORK_GET_NUM_PARTICIPANTS() //NETWORK_GET_NUM_PARTICIPANTS()

					CDEBUG1LN(DEBUG_GOLF,"Num participating in this script: ",iNumMissionPlayers)
//					IF iNumMissionPlayers >= 1
						START_TIMER_NOW(currentHelpers.navTimer)	
						
						CDEBUG1LN(DEBUG_GOLF,"Time waiting on init ", GET_TIMER_IN_SECONDS(currentHelpers.navTimer))
						IF TIMER_DO_ONCE_WHEN_READY(currentHelpers.navTimer, 7.5)
							SET_FM_MISSION_AS_NOT_JOINABLE() //you've had plenty of time to join, no more
							SET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD, GET_FIRST_ACTIVE_PLAYER())
							SET_GOLF_MP_SERVER_MISSION_STATE(serverBD, GAME_STATE_RUNNING)
							SET_GOLF_MP_SERVER_GOLF_STATE(serverBD, GMS_PRE_INIT_WAIT)
						ENDIF
//					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					IF bDebugSpew DEBUG_MESSAGE("SERVER GAME_STATE_RUNNING") ENDIF
					// Handle any game logic needed
					SWITCH GET_GOLF_MP_SERVER_GOLF_STATE(serverBD)
						CASE GMS_PRE_INIT
						BREAK
						CASE GMS_PLAY_GOLF
							IF bDebugSpew DEBUG_MESSAGE("SERVER GMS_PLAY_GOLF") ENDIF
							
							//SET_CLOCK_TIME(FLOOR(vPreviousTime.x), FLOOR(vPreviousTime.y), FLOOR(vPreviousTime.z)) //only the server can set the time
							GOLF_MP_SERVER_UPDATE_SHOT_STATUS(serverBD, playerBD, currentPlayerFoursome, currentPlayers, currentCourse)
							
							IF IS_ANY_GOLF_MP_CLIENT_CONTROL_FLAG_SET(currentPlayerFoursome, playerBD, currentPlayers, GCF_MP_OUTRO)
								SET_GOLF_MP_SERVER_GOLF_STATE(serverBD, GMS_OUTRO_INIT)
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		ENDIF
		
		IF bAllowOverideClock
			NETWORK_OVERRIDE_CLOCK_TIME(12, 0, 0)
		ENDIF
				
		//dont give the player control in mp golf
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
		ENDIF
		
		/*INT iParticpantIDForHeadTexture	//Old style of headshot management, calling this every frame should no longer be neccessary DaveyG
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticpantIDForHeadTexture
			IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX(iParticpantIDForHeadTexture) )
				Keep_Player_Headshot_Active(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticpantIDForHeadTexture)))
			ENDIF
		ENDREPEAT*/
		
		ENDIF // SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()	
		ENDIF //is spectator host
	ENDWHILE
	
// End of Mission
ENDSCRIPT

