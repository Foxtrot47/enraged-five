USING "minigames_helpers.sch"
USING "golf_player.sch"
USING "golf_swing_meter.sch"
CONST_INT MP_GOLF_MAX_PLAYERS	6

/// PURPOSE: Enumeration for state machine for the entire game of golf for a foursome
ENUM GOLF_GAME_STATE
	GGS_NAVIGATE_TO_SHOT,
	GGS_NAVIGATE_OUT_OF_BOUNDS,
	GGS_TAKING_SHOT,
	GGS_SCORECARD_END_HOLE,
	GGS_SCORECARD_END_GAME,
	GGS_GET_IN_CART,
	GGS_DRIVE_TO_SHOT,
	GGS_END_GAME
ENDENUM

/// PURPOSE: Structu for a given foursome. Does not necessarily contain a human player
STRUCT GOLF_FOURSOME
	GOLF_GAME_STATE		golfState
	SWING_METER			swingMeter
	
	#IF NOT GOLF_IS_MP
		GOLF_PLAYER_CORE	playerCore[4]
	#ENDIF
	#IF GOLF_IS_MP
		GOLF_PLAYER_CORE	playerCore[MP_GOLF_MAX_PLAYERS]
	#ENDIF
	
	VEHICLE_INDEX		vehCart[2]
	OBJECT_INDEX		objTee
	
	INT					iNumPlayers
	INT					iCurrentPlayer
	INT 				iCurrentHole
	STRUCTTIMER			physicsTimer
	STRUCTTIMER			physicsTimeout
	INT 				iLocalPlayerIndex
	INT					iPrevHoleWinner
	INT					iCartDriver[2]
	
	#IF NOT GOLF_IS_AI_ONLY
		TEXT_LABEL_63		sCurrentReactionAnimation
		VECTOR vPositionBeforeReactionCam
		FLOAT fHeaingBeforeReactionCamera
		INT iBadReactionIndex
		INT iGoodReactionIndex
	#ENDIF
	
ENDSTRUCT

/// PURPOSE:
///    Accessor for GOLF_FOURSOME state
FUNC GOLF_GAME_STATE GET_GOLF_FOURSOME_STATE(GOLF_FOURSOME &golfFoursome)
	RETURN golfFoursome.golfState
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME state
PROC SET_GOLF_FOURSOME_STATE(GOLF_FOURSOME &golfFoursome, GOLF_GAME_STATE golfState)
	golfFoursome.golfState = golfState
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME number of players
FUNC INT GET_GOLF_FOURSOME_NUM_PLAYERS(GOLF_FOURSOME &golfFoursome)
	RETURN golfFoursome.iNumPlayers
ENDFUNC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME number of players
PROC SET_GOLF_FOURSOME_NUM_PLAYERS(GOLF_FOURSOME &golfFoursome, INT iNumPlayers)
	golfFoursome.iNumPlayers = iNumPlayers
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME number of players
PROC INCREMENT_GOLF_FOURSOME_NUM_PLAYERS(GOLF_FOURSOME &golfFoursome)
	golfFoursome.iNumPlayers++
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current player index
FUNC INT GET_GOLF_FOURSOME_CURRENT_PLAYER(GOLF_FOURSOME &golfFoursome)
	RETURN golfFoursome.iCurrentPlayer
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current player index
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER(GOLF_FOURSOME &golfFoursome, INT iCurrentPlayer)
	#IF IS_DEBUG_BUILD
		IF iCurrentPlayer < 0
			CDEBUG1LN(DEBUG_GOLF, "Current player is invalid: ", iCurrentPlayer)
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	
	golfFoursome.iCurrentPlayer = iCurrentPlayer
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current hole index
FUNC INT GET_GOLF_FOURSOME_CURRENT_HOLE(GOLF_FOURSOME &golfFoursome)
	RETURN golfFoursome.iCurrentHole
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current player index
PROC SET_GOLF_FOURSOME_CURRENT_HOLE(GOLF_FOURSOME &golfFoursome, INT iCurrentHole)
	CDEBUG1LN(DEBUG_GOLF,"Set foursome hole: ", iCurrentHole)
	golfFoursome.iCurrentHole = iCurrentHole
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current player index
FUNC INT GET_GOLF_FOURSOME_LOCAL_PLAYER(GOLF_FOURSOME &golfFoursome)
	RETURN golfFoursome.iLocalPlayerIndex
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current player index
PROC SET_GOLF_FOURSOME_LOCAL_PLAYER(GOLF_FOURSOME &golfFoursome, INT iLocalPlayerIndex)
	golfFoursome.iLocalPlayerIndex = iLocalPlayerIndex
ENDPROC

FUNC PED_INDEX GET_GOLF_FOURSOME_LOCAL_PLAYER_PED(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN NULL
	ENDIF
	RETURN GET_GOLF_PLAYER_PED(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDFUNC



/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE state
FUNC GOLF_PLAYER_STATE GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iCurrentPlayer < 0
		RETURN GPS_UNDEFINED
	ENDIF

	RETURN GET_GOLF_PLAYER_STATE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

FUNC GOLF_PLAYER_STATE GET_GOLF_FOURSOME_LOCAL_PLAYER_STATE(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN GPS_UNDEFINED
	ENDIF
	RETURN GET_GOLF_PLAYER_STATE(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDFUNC

FUNC GOLF_PLAYER_STATE GET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN GPS_UNDEFINED
	ENDIF
	RETURN GET_GOLF_PLAYER_STATE(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC
/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE state
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(GOLF_FOURSOME &golfFoursome, GOLF_PLAYER_STATE playerState)
	 SET_GOLF_PLAYER_STATE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], playerState)
ENDPROC

PROC SET_GOLF_FOURSOME_LOCAL_PLAYER_STATE(GOLF_FOURSOME &golfFoursome, GOLF_PLAYER_STATE playerState)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_STATE(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex], playerState)
ENDPROC

PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, GOLF_PLAYER_STATE playerState)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	 SET_GOLF_PLAYER_STATE(golfFoursome.playerCore[iPlayerIndex], playerState)
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE control method
FUNC PLAYER_CONTROL_TYPE GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_CONTROL(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

FUNC BOOL IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(golfFoursome) = HUMAN_LOCAL OR GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(golfFoursome) = HUMAN_LOCAL_MP
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE control method
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(GOLF_FOURSOME &golfFoursome, PLAYER_CONTROL_TYPE playerControl)
	 SET_GOLF_PLAYER_CONTROL(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], playerControl)
ENDPROC

FUNC PLAYER_CONTROL_TYPE GET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN CONTROL_NONE
	ENDIF
	RETURN GET_GOLF_PLAYER_CONTROL(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC



PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, PLAYER_CONTROL_TYPE playerControl)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_CONTROL(golfFoursome.playerCore[iPlayerIndex], playerControl)
ENDPROC/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ped
FUNC PED_INDEX GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iCurrentPlayer < 0
		RETURN NULL
	ENDIF

	RETURN GET_GOLF_PLAYER_PED(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ped
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_PED(GOLF_FOURSOME &golfFoursome, PED_INDEX pedGolfPlayer)
	 SET_GOLF_PLAYER_PED(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], pedGolfPlayer)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ped
FUNC PED_INDEX GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN NULL
	ENDIF
	RETURN GET_GOLF_PLAYER_PED(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ped
PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_PED(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, PED_INDEX pedGolfPlayer)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_PED(golfFoursome.playerCore[iPlayerIndex], pedGolfPlayer)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ball
FUNC OBJECT_INDEX GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_BALL(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ball
FUNC OBJECT_INDEX GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN NULL
	ENDIF
	RETURN GET_GOLF_PLAYER_BALL(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDFUNC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ball
FUNC OBJECT_INDEX GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN NULL
	ENDIF
	RETURN GET_GOLF_PLAYER_BALL(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ball
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(GOLF_FOURSOME &golfFoursome, OBJECT_INDEX objBall)
	 SET_GOLF_PLAYER_BALL(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], objBall)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ball
PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, OBJECT_INDEX objBall)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_BALL(golfFoursome.playerCore[iPlayerIndex], objBall)
ENDPROC

FUNC OBJECT_INDEX GET_GOLF_FOURSOME_CURRENT_PLAYER_MARKER(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_MARKER(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_MARKER(GOLF_FOURSOME &golfFoursome, OBJECT_INDEX objMarker)
	SET_GOLF_PLAYER_MARKER(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], objMarker)
ENDPROC

FUNC OBJECT_INDEX GET_GOLF_FOURSOME_INDEXED_PLAYER_MARKER(GOLF_FOURSOME &golfFoursome, INT playerIndex)
	RETURN GET_GOLF_PLAYER_MARKER(golfFoursome.playerCore[playerIndex])
ENDFUNC

PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_MARKER(GOLF_FOURSOME &golfFoursome, OBJECT_INDEX objMarker, INT playerIndex)
	SET_GOLF_PLAYER_MARKER(golfFoursome.playerCore[playerIndex], objMarker)
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE club
FUNC OBJECT_INDEX GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_CLUB(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE club
FUNC OBJECT_INDEX GET_GOLF_FOURSOME_LOCAL_PLAYER_CLUB(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN NULL
	ENDIF
	RETURN GET_GOLF_PLAYER_CLUB(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE club
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(GOLF_FOURSOME &golfFoursome, OBJECT_INDEX objClub)
	SET_GOLF_PLAYER_CLUB(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], objClub)
ENDPROC

FUNC OBJECT_INDEX GET_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		RETURN NULL
	ENDIF

	RETURN GET_GOLF_PLAYER_CLUB(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE club
PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, OBJECT_INDEX objClub)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_CLUB(golfFoursome.playerCore[iPlayerIndex], objClub)
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE club
PROC CLEANUP_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(GOLF_FOURSOME &golfFoursome)
	CLEANUP_GOLF_CLUB(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDPROC

///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE club
PROC CLEANUP_GOLF_FOURSOME_LOCAL_PLAYER_CLUB(GOLF_FOURSOME &golfFoursome)
	CLEANUP_GOLF_CLUB(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDPROC

PROC CLEANUP_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		EXIT
	ENDIF

	CLEANUP_GOLF_CLUB(golfFoursome.playerCore[iPlayerIndex])
ENDPROC

PROC CLEANUP_GOLF_FOURSOME_CLUBS(GOLF_FOURSOME &golfFoursome, INT iExcludeIndex  =-1)
	INT playerIndex
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(golfFoursome) playerIndex
		IF playerIndex != iExcludeIndex
			CLEANUP_GOLF_CLUB(golfFoursome.playerCore[playerIndex])
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ball position
FUNC VECTOR GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_BALL_POSITION(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ball position
FUNC VECTOR GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL_POSITION(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN GET_GOLF_PLAYER_BALL_POSITION(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
	ENDIF
	RETURN GET_GOLF_PLAYER_BALL_POSITION(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ball position
FUNC VECTOR GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL_POSITION(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN <<0,0,0>>
	ENDIF
	RETURN GET_GOLF_PLAYER_BALL_POSITION(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC
/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ball position
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(GOLF_FOURSOME &golfFoursome, VECTOR vBallPosition)
	SET_GOLF_PLAYER_BALL_POSITION(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], vBallPosition)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE ball position
PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_BALL_POSITION(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, VECTOR vBallPosition)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_BALL_POSITION(golfFoursome.playerCore[iPlayerIndex], vBallPosition)
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE shot estimate
FUNC VECTOR GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(GOLF_FOURSOME &golfFoursome)
//	PRINTSTRING("GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE: Player")PRINTINT(golfFoursome.iCurrentPlayer)PRINTNL()
	RETURN GET_GOLF_PLAYER_SHOT_ESTIMATE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE shot estimate
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(GOLF_FOURSOME &golfFoursome, VECTOR vShotEstimate)
	SET_GOLF_PLAYER_SHOT_ESTIMATE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], vShotEstimate)
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE distance to hole
FUNC FLOAT GET_GOLF_FOURSOME_CURRENT_PLAYER_DISTANCE_TO_HOLE(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_DISTANCE_TO_HOLE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

FUNC FLOAT GET_GOLF_FOURSOME_LOCAL_PLAYER_DISTANCE_TO_HOLE(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN 0.0
	ENDIF
	RETURN GET_GOLF_PLAYER_DISTANCE_TO_HOLE(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDFUNC
FUNC FLOAT GET_GOLF_FOURSOME_INDEXED_PLAYER_DISTANCE_TO_HOLE(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN 0.0
	ENDIF
	RETURN GET_GOLF_PLAYER_DISTANCE_TO_HOLE(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE distance to hole
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_DISTANCE_TO_HOLE(GOLF_FOURSOME &golfFoursome, FLOAT fDistToHole)
	SET_GOLF_PLAYER_DISTANCE_TO_HOLE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], fDistToHole)
ENDPROC

PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_DISTANCE_TO_HOLE(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, FLOAT fDistToHole)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_DISTANCE_TO_HOLE(golfFoursome.playerCore[iPlayerIndex], fDistToHole)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE aim
FUNC FLOAT GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_AIM(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE aim
FUNC FLOAT GET_GOLF_FOURSOME_LOCAL_PLAYER_AIM(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN 0.0
	ENDIF
	RETURN GET_GOLF_PLAYER_AIM(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE aim
FUNC FLOAT GET_GOLF_FOURSOME_INDEXED_PLAYER_AIM(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN 0.0
	ENDIF
	RETURN GET_GOLF_PLAYER_AIM(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE aim
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(GOLF_FOURSOME &golfFoursome, FLOAT fAimHeading)
//	PRINTSTRING("SET_GOLF_FOURSOME_CURRENT_PLAYER_AIM: Player")PRINTINT(golfFoursome.iCurrentPlayer)PRINTNL()
	SET_GOLF_PLAYER_AIM(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], fAimHeading)
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE aim
PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_AIM(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, FLOAT fAimHeading)
//	PRINTSTRING("SET_GOLF_FOURSOME_CURRENT_PLAYER_AIM: Player")PRINTINT(golfFoursome.iCurrentPlayer)PRINTNL()
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_AIM(golfFoursome.playerCore[iPlayerIndex], fAimHeading)
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE aim
PROC INCREMENT_GOLF_FOURSOME_CURRENT_PLAYER_AIM(GOLF_FOURSOME &golfFoursome, FLOAT fAimAdd)
//	PRINTSTRING("INCREMENT_GOLF_FOURSOME_CURRENT_PLAYER_AIM: Player")PRINTINT(golfFoursome.iCurrentPlayer)PRINTNL()
	INCREMENT_GOLF_PLAYER_AIM(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], fAimAdd)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE meter goal
FUNC FLOAT GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(GOLF_FOURSOME &golfFoursome, BOOL bUseStrengthStat = FALSE)
	RETURN GET_GOLF_PLAYER_METER_GOAL(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], bUseStrengthStat)
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE meter goal
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(GOLF_FOURSOME &golfFoursome, FLOAT fPowerMeterGoal)

	//Make sure that the value is appropriate
	IF fPowerMeterGoal > 100
		fPowerMeterGoal = 100
	ELIF fPowerMeterGoal < 5.0
		fPowerMeterGoal = 5.0
	ENDIF
	
	SET_GOLF_PLAYER_METER_GOAL(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], fPowerMeterGoal)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE drive carry
FUNC FLOAT GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_CARRY(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_SHOT_CARRY(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE drive carry
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_CARRY(GOLF_FOURSOME &golfFoursome, FLOAT fShotCarry)
	SET_GOLF_PLAYER_SHOT_CARRY(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], fShotCarry)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE club index
FUNC INT GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_CURRENT_CLUB(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE club index
FUNC INT GET_GOLF_FOURSOME_LOCAL_PLAYER_CURRENT_CLUB(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN 0
	ENDIF
	RETURN GET_GOLF_PLAYER_CURRENT_CLUB(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDFUNC

FUNC INT GET_GOLF_FOURSOME_INDEXED_PLAYER_CURRENT_CLUB(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN 0
	ENDIF
	RETURN GET_GOLF_PLAYER_CURRENT_CLUB(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC
/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE club index
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(GOLF_FOURSOME &golfFoursome, INT iCurrentClub)
	SET_GOLF_PLAYER_CURRENT_CLUB(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], iCurrentClub)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE club index
PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_CURRENT_CLUB(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, INT iCurrentClub)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_CURRENT_CLUB(golfFoursome.playerCore[iPlayerIndex], iCurrentClub)
ENDPROC
/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE club index
PROC INCREMENT_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(GOLF_FOURSOME &golfFoursome, INT iAddClub)
	INCREMENT_GOLF_PLAYER_CURRENT_CLUB(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], iAddClub)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE shots on current hole
FUNC INT GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_SHOTS_ON_HOLE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE shots on current hole
FUNC INT GET_GOLF_FOURSOME_LOCAL_PLAYER_SHOTS_ON_HOLE(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN 0
	ENDIF
	RETURN GET_GOLF_PLAYER_SHOTS_ON_HOLE(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDFUNC

FUNC INT GET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN 0
	ENDIF
	RETURN GET_GOLF_PLAYER_SHOTS_ON_HOLE(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE shots on current hole
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(GOLF_FOURSOME &golfFoursome, INT iCurrentHoleShots)
	SET_GOLF_PLAYER_SHOTS_ON_HOLE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], iCurrentHoleShots)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE shots on current hole
PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_SHOTS_ON_HOLE(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, INT iCurrentHoleShots)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_SHOTS_ON_HOLE(golfFoursome.playerCore[iPlayerIndex], iCurrentHoleShots)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE shots on current hole
PROC INCREMENT_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(GOLF_FOURSOME &golfFoursome, INT iAddShots = 1)
	SET_GOLF_PLAYER_SHOTS_ON_HOLE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], GET_GOLF_PLAYER_SHOTS_ON_HOLE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer]) + iAddShots)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE swing style
FUNC SWING_STYLE GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_SWING_STYLE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

FUNC SWING_STYLE GET_GOLF_FOURSOME_LOCAL_PLAYER_SWING_STYLE(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN SWING_STYLE_NORMAL
	ENDIF
	RETURN GET_GOLF_PLAYER_SWING_STYLE(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDFUNC

FUNC SWING_STYLE GET_GOLF_FOURSOME_INDEXED_PLAYER_SWING_STYLE(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN SWING_STYLE_NORMAL
	ENDIF
	RETURN GET_GOLF_PLAYER_SWING_STYLE(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE swing style
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(GOLF_FOURSOME &golfFoursome, SWING_STYLE swingStyle)
	SET_GOLF_PLAYER_SWING_STYLE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], swingStyle)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE swing style
PROC INCREMENT_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(GOLF_FOURSOME &golfFoursome, INT iAddValue)
	INCREMENT_GOLF_PLAYER_SWING_STYLE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], iAddValue)
ENDPROC

PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_SWING_STYLE(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, SWING_STYLE swingStyle)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_SWING_STYLE(golfFoursome.playerCore[iPlayerIndex], swingStyle)
ENDPROC
/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE swing state
FUNC SWING_ANIM_STATES GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_SWING_ANIM(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE swing anim
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(GOLF_FOURSOME &golfFoursome, SWING_ANIM_STATES swingAnim)
	SET_GOLF_PLAYER_SWING_ANIM(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], swingAnim)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE shot time
FUNC GOLF_LIE_TYPE GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_LIE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

FUNC GOLF_LIE_TYPE GET_GOLF_FOURSOME_CURRENT_PLAYER_LAST_LIE(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_LAST_LIE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC


FUNC GOLF_LIE_TYPE GET_GOLF_FOURSOME_LOCAL_PLAYER_LIE(GOLF_FOURSOME &golfFoursome)
	IF golfFoursome.iLocalPlayerIndex < 0 OR golfFoursome.iLocalPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN LIE_UNKNOWN
	ENDIF
	RETURN GET_GOLF_PLAYER_LIE(golfFoursome.playerCore[golfFoursome.iLocalPlayerIndex])
ENDFUNC


FUNC GOLF_LIE_TYPE GET_GOLF_FOURSOME_INDEXED_PLAYER_LIE(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN LIE_UNKNOWN
	ENDIF
	RETURN GET_GOLF_PLAYER_LIE(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC
/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE shot time
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(GOLF_FOURSOME &golfFoursome, VECTOR vWorldPos, GOLF_LIE_TYPE lieType)
	SET_GOLF_PLAYER_LIE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], vWorldPos, lieType)
ENDPROC

PROC RESET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(GOLF_FOURSOME &golfFoursome)
	RESET_GOLF_PLAYER_LAST_LIE(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDPROC


PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_LIE(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, VECTOR vWorldPos, GOLF_LIE_TYPE lieType, BOOL bPrintDebug = TRUE)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_LIE(golfFoursome.playerCore[iPlayerIndex], vWorldPos, lieType, bPrintDebug)
ENDPROC
/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE last normal value
FUNC VECTOR GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_SHOT_NORMAL(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE last normal value
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(GOLF_FOURSOME &golfFoursome, VECTOR vShotNormal)
	SET_GOLF_PLAYER_SHOT_NORMAL(golfFoursome.playerCore[golfFoursome.iCurrentPlayer], vShotNormal)
ENDPROC

PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_SHOT_NORMAL(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, VECTOR vShotNormal)
	SET_GOLF_PLAYER_SHOT_NORMAL(golfFoursome.playerCore[iPlayerIndex], vShotNormal)
ENDPROC

PROC RESET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(GOLF_FOURSOME &golfFoursome)
	RESET_GOLF_PLAYER_SHOT_NORMAL(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDPROC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE cart
FUNC VEHICLE_INDEX GET_GOLF_FOURSOME_CART(GOLF_FOURSOME &golfFoursome, INT cartIndex = 0)
	RETURN golfFoursome.vehCart[cartIndex]
ENDFUNC


/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE cart
PROC SET_GOLF_FOURSOME_CART(GOLF_FOURSOME &golfFoursome, VEHICLE_INDEX vehCart, INT cartIndex = 0)
	golfFoursome.vehCart[cartIndex] = vehCart
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_PLAYER_CORE cart
PROC CLEANUP_GOLF_CART(GOLF_FOURSOME &golfFoursome, BOOL bForceDelete = FALSE)
	INT vehIndex
	
	REPEAT COUNT_OF(golfFoursome.vehCart) vehIndex
		IF DOES_ENTITY_EXIST(golfFoursome.vehCart[vehIndex]) AND NOT IS_ENTITY_DEAD(golfFoursome.vehCart[vehIndex])
			IF bForceDelete
				DELETE_VEHICLE(golfFoursome.vehCart[vehIndex])
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED(golfFoursome.vehCart[vehIndex])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE cart
FUNC OBJECT_INDEX GET_GOLF_FOURSOME_CURRENT_PLAYER_GOLFBAG(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_PLAYER_GOLFBAG(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE cart
FUNC OBJECT_INDEX GET_GOLF_FOURSOME_INDEXED_PLAYER_GOLFBAG(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		RETURN NULL
	ENDIF
	RETURN GET_GOLF_PLAYER_GOLFBAG(golfFoursome.playerCore[iPlayerIndex])
ENDFUNC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE cart
PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_GOLFBAG(GOLF_FOURSOME &golfFoursome, OBJECT_INDEX objGolfBag)
	SET_GOLF_PLAYER_GOLFBAG( golfFoursome.playerCore[golfFoursome.iCurrentPlayer], objGolfBag)
ENDPROC

/// PURPOSE:
///    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE cart
PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_GOLFBAG(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, OBJECT_INDEX objGolfBag)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_GOLFBAG( golfFoursome.playerCore[iPlayerIndex], objGolfBag)
ENDPROC


///// PURPOSE:
/////    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE cart
//FUNC INT GET_GOLF_FOURSOME_CURRENT_PLAYER_TASK_TIME(GOLF_FOURSOME &golfFoursome)
//	RETURN GET_GOLF_PLAYER_TASK_TIME(golfFoursome.playerCore[golfFoursome.iCurrentPlayer])
//ENDFUNC
//
///// PURPOSE:
/////    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE cart
//FUNC INT GET_GOLF_FOURSOME_INDEXED_PLAYER_TASK_TIME(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex)
//	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
//		RETURN -1
//	ENDIF
//	RETURN GET_GOLF_PLAYER_TASK_TIME(golfFoursome.playerCore[iPlayerIndex])
//ENDFUNC
//
///// PURPOSE:
/////    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE cart
//PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_TASK_TIME(GOLF_FOURSOME &golfFoursome, INT iTaskTime)
//	SET_GOLF_PLAYER_TASK_TIME( golfFoursome.playerCore[golfFoursome.iCurrentPlayer], iTaskTime)
//ENDPROC
//
///// PURPOSE:
/////    Accessor for GOLF_FOURSOME current GOLF_PLAYER_CORE cart
//PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_TASK_TIME(GOLF_FOURSOME &golfFoursome, INT iPlayerIndex, INT iTaskTime)
//	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(golfFoursome.playerCore)
//		EXIT
//	ENDIF
//	SET_GOLF_PLAYER_TASK_TIME( golfFoursome.playerCore[iPlayerIndex], iTaskTime)
//ENDPROC


PROC CLEANUP_GOLF_FOURSOME_INDEXED_GOLF_BALL(GOLF_FOURSOME &thisFoursome, INT iPlayerIndex)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(thisFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	CLEANUP_GOLF_BALL(thisFoursome.playerCore[iPlayerIndex])
ENDPROC

PROC GOLF_FOURSOME_DELETE_ALL_BALLS(GOLF_FOURSOME &thisFoursome)

	INT iPlayerIndex
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
	
		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex))
			#IF GOLF_IS_MP IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, iPlayerIndex)) #ENDIF
			
				CLEANUP_GOLF_BALL(thisFoursome.playerCore[iPlayerIndex])
				
			#IF GOLF_IS_MP ENDIF #ENDIF
		ENDIF
	
	ENDREPEAT

ENDPROC


PROC CLEANUP_GOLF_FOURSOME_CURRENT_GOLF_BALL(GOLF_FOURSOME &thisFoursome)
	CLEANUP_GOLF_BALL( thisFoursome.playerCore[thisFoursome.iCurrentPlayer])
ENDPROC

PROC CLEANUP_GOLF_FOURSOME_INDEXED_PLAYER_MARKER(GOLF_FOURSOME &thisFoursome, INT iPlayerIndex)
	CLEANUP_GOLF_MARKER(thisFoursome.playerCore[iPlayerIndex])
ENDPROC

PROC CLEANUP_GOLF_FOURSOME_CURRENT_PLAYER_MARKER(GOLF_FOURSOME &thisFoursome)
	CLEANUP_GOLF_MARKER(thisFoursome.playerCore[thisFoursome.iCurrentPlayer])
ENDPROC

/// PURPOSE: Helper function to get the servers game/mission state
/// RETURNS: The server game state.
FUNC GOLF_PLAYER_STATUS_BITS GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAGS(GOLF_FOURSOME &thisFoursome)
	RETURN GET_GOLF_PLAYER_CONTROL_FLAGS(thisFoursome.playerCore[thisFoursome.iCurrentPlayer])
ENDFUNC

PROC OVERWRITE_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAGS(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER_STATUS_BITS eStatusBits)
	OVERWRITE_GOLF_PLAYER_CONTROL_FLAGS(thisFoursome.playerCore[thisFoursome.iCurrentPlayer], eStatusBits)
ENDPROC

PROC SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER_STATUS_BITS eStatusBits)
	SET_GOLF_PLAYER_CONTROL_FLAG(thisFoursome.playerCore[thisFoursome.iCurrentPlayer], eStatusBits)
ENDPROC

PROC CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER_STATUS_BITS eStatusBits)
	CLEAR_GOLF_PLAYER_CONTROL_FLAG(thisFoursome.playerCore[thisFoursome.iCurrentPlayer], eStatusBits)
ENDPROC

FUNC BOOL IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER_STATUS_BITS eStatusBits)
	RETURN IS_GOLF_PLAYER_CONTROL_FLAG_SET(thisFoursome.playerCore[thisFoursome.iCurrentPlayer], eStatusBits)
ENDFUNC

PROC SET_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER_STATUS_BITS eStatusBits)
	IF thisFoursome.iLocalPlayerIndex < 0 OR thisFoursome.iLocalPlayerIndex >= COUNT_OF(thisFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_CONTROL_FLAG(thisFoursome.playerCore[thisFoursome.iLocalPlayerIndex], eStatusBits)
ENDPROC

PROC CLEAR_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER_STATUS_BITS eStatusBits)
	IF thisFoursome.iLocalPlayerIndex < 0 OR thisFoursome.iLocalPlayerIndex >= COUNT_OF(thisFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		EXIT
	ENDIF
	CLEAR_GOLF_PLAYER_CONTROL_FLAG(thisFoursome.playerCore[thisFoursome.iLocalPlayerIndex], eStatusBits)
ENDPROC

FUNC BOOL IS_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG_SET(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER_STATUS_BITS eStatusBits)
	IF thisFoursome.iLocalPlayerIndex < 0 OR thisFoursome.iLocalPlayerIndex >= COUNT_OF(thisFoursome.playerCore)
		SCRIPT_ASSERT("golfFoursome.iLocalPlayerIndex out of range")
		RETURN FALSE
	ENDIF
	RETURN IS_GOLF_PLAYER_CONTROL_FLAG_SET(thisFoursome.playerCore[thisFoursome.iLocalPlayerIndex], eStatusBits)
ENDFUNC

PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG(GOLF_FOURSOME &thisFoursome, INT iPlayerIndex, GOLF_PLAYER_STATUS_BITS eStatusBits)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(thisFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	SET_GOLF_PLAYER_CONTROL_FLAG(thisFoursome.playerCore[iPlayerIndex], eStatusBits)
ENDPROC

PROC CLEAR_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG(GOLF_FOURSOME &thisFoursome, INT iPlayerIndex, GOLF_PLAYER_STATUS_BITS eStatusBits)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(thisFoursome.playerCore)
		SCRIPT_ASSERT("iPlayerIndex out of range")
		EXIT
	ENDIF
	CLEAR_GOLF_PLAYER_CONTROL_FLAG(thisFoursome.playerCore[iPlayerIndex], eStatusBits)
ENDPROC

FUNC BOOL IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(GOLF_FOURSOME &thisFoursome, INT iPlayerIndex, GOLF_PLAYER_STATUS_BITS eStatusBits)
	IF iPlayerIndex < 0 OR iPlayerIndex >= COUNT_OF(thisFoursome.playerCore)
		CDEBUG1LN(DEBUG_GOLF, "iPlayerIndex out of range ", iPlayerIndex)
		RETURN FALSE
	ENDIF
	RETURN IS_GOLF_PLAYER_CONTROL_FLAG_SET(thisFoursome.playerCore[iPlayerIndex], eStatusBits)
ENDFUNC

PROC SET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(GOLF_FOURSOME &thisFoursome, INT playerIndex, INT cartIndex)
	thisFoursome.playerCore[playerIndex].iCartIndex = cartIndex
ENDPROC

FUNC INT GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(GOLF_FOURSOME &thisFoursome, INT playerIndex)
	RETURN thisFoursome.playerCore[playerIndex].iCartIndex
ENDFUNC

FUNC INT GET_GOLF_FOURSOME_CURRENT_PLAYER_CART_INDEX(GOLF_FOURSOME &thisFoursome)
	RETURN GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(thisFoursome, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
ENDFUNC

PROC SET_GOLF_FOURSOME_CART_DRIVER(GOLF_FOURSOME &thisFoursome, INT driverIndex, INT cartIndex)
	thisFoursome.iCartDriver[cartIndex] = driverIndex
ENDPROC

FUNC INT GET_GOLF_FOURSOME_CART_DRIVER(GOLF_FOURSOME &thisFoursome, INT cartIndex)
	RETURN thisFoursome.iCartDriver[cartIndex]
ENDFUNC

FUNC INT GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_DRIVER(GOLF_FOURSOME &thisFoursome, INT  playerIndex)
	RETURN GET_GOLF_FOURSOME_CART_DRIVER(thisFoursome, GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(thisFoursome, playerIndex))
ENDFUNC

FUNC INT GET_GOLF_FOURSOME_CURRENT_PLAYER_CART_DRIVER(GOLF_FOURSOME &thisFoursome)
	RETURN GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_DRIVER(thisFoursome, GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
ENDFUNC

FUNC VEHICLE_INDEX GET_GOLF_FOURSOME_CURRENT_PLAYER_CART(GOLF_FOURSOME &golfFoursome)
	RETURN GET_GOLF_FOURSOME_CART(golfFoursome, GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(golfFoursome, GET_GOLF_FOURSOME_CURRENT_PLAYER(golfFoursome)))
ENDFUNC

FUNC VEHICLE_INDEX GET_GOLF_FOURSOME_INDEXED_PLAYER_CART(GOLF_FOURSOME &golfFoursome, INT playerIndex)
	RETURN GET_GOLF_FOURSOME_CART(golfFoursome, GET_GOLF_FOURSOME_INDEXED_PLAYER_CART_INDEX(golfFoursome, playerIndex))
ENDFUNC

FUNC VEHICLE_SEAT GET_GOLF_FOURSOME_INDEXED_PLAYER_SEAT(INT playerIndex)
	IF playerIndex = 0 OR playerIndex = 2
		RETURN VS_DRIVER
	ELSE
		RETURN VS_FRONT_RIGHT
	ENDIF
ENDFUNC

FUNC VEHICLE_SEAT GET_GOLF_FOURSOME_CURRENT_PLAYER_SEAT(GOLF_FOURSOME &thisFoursome)
	RETURN GET_GOLF_FOURSOME_INDEXED_PLAYER_SEAT(GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome))
ENDFUNC

FUNC GOLF_AI_DIFFICULTY GET_GOLF_FOURSOME_CURRENT_PLAYER_AI_DIFFICULTY(GOLF_FOURSOME &thisFoursome)
	RETURN GET_GOLF_PLAYER_AI_DIFFICULTY(thisFoursome.playerCore[thisFoursome.iCurrentPlayer])
ENDFUNC


FUNC BOOL IS_GOLF_FOURSOME_CURRENT_PLAYER_JUST_HIT_BALL_ON_GREEN(GOLF_FOURSOME &thisFoursome)
	RETURN (GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN)
		    AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_STARTED_SHOT_ON_GREEN)
ENDFUNC

PROC GOLF_FIND_NEW_CART_DRIVER(GOLF_FOURSOME &thisFoursome, INT cartIndex)

	INT playerIndex
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) playerIndex
		IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, playerIndex, GOLF_PLAYER_REMOVED)
			SET_GOLF_FOURSOME_CART_DRIVER(thisFoursome, playerIndex, cartIndex)
			EXIT
		ENDIF
	ENDREPEAT

ENDPROC

FUNC BOOL IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(GOLF_FOURSOME &thisFoursome)
 	SWING_STYLE currentSwingStyle = GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome)
	IF currentSwingStyle = SWING_STYLE_GREEN
	OR currentSwingStyle = SWING_STYLE_GREEN_SHORT
	OR currentSwingStyle = SWING_STYLE_GREEN_LONG
	OR currentSwingStyle = SWING_STYLE_GREEN_TAP
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_GOLF_NUMBER_OF_PLAYERS_REMOVED(GOLF_FOURSOME &thisFoursome)
	INT iPlayerIndex
	INT iRet = 0
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		IF IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
			iRet++
		ENDIF
	ENDREPEAT
	RETURN iRet
ENDFUNC

FUNC BOOL IS_GOLF_FOURSOME_STATE_AT_END(GOLF_FOURSOME &thisFoursome)
	RETURN GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_SCORECARD_END_HOLE 
	      OR GET_GOLF_FOURSOME_STATE(thisFoursome) =  GGS_SCORECARD_END_GAME
ENDFUNC

FUNC INT GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(GOLF_FOURSOME &thisFoursome)
	RETURN (GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) - GET_GOLF_NUMBER_OF_PLAYERS_REMOVED(thisFoursome))
ENDFUNC

/// PURPOSE:
///    Checks to see if all players in a foursome have completed the current hole
/// PARAMS:
///    thisFoursome - Foursome to check
///    iExcludeIndex - optional exclusion index - don't check this player
///    bHumanOnly - only check human players
/// RETURNS:
///    TRUE if all players aside from iExclusionInde have completed the current hole
FUNC BOOL ARE_ALL_PLAYERS_DONE_WITH_HOLE(GOLF_FOURSOME &thisFoursome, INT iExcludeIndex = -1, BOOL bHumanOnly = FALSE)
	INT iCurrentPlayer
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iCurrentPlayer
		IF iExcludeIndex < 0 OR iExcludeIndex != iCurrentPlayer
			IF GET_GOLF_PLAYER_STATE(thisFoursome.playerCore[iCurrentPlayer]) != GPS_DONE_WITH_HOLE AND (GET_GOLF_PLAYER_CONTROL(thisFoursome.playerCore[iCurrentPlayer]) != COMPUTER OR NOT bHumanOnly)
			AND NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iCurrentPlayer, GOLF_PLAYER_REMOVED)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC INT GET_GOLF_FOURSOME_NEAREST_PLAYER_TO_CURRENT_PLAYER(GOLF_FOURSOME &thisFoursome)
	INT iPlayerIndex
	INT iNearestIndex = -1
	FLOAT fDist
	FLOAT fClosestDist = 999999999.0
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		IF iPlayerIndex != thisFoursome.iCurrentPlayer
			fDist = VDIST(GET_ENTITY_COORDS(thisFoursome.playerCore[iPlayerIndex].pedGolfPlayer), GET_ENTITY_COORDS(thisFoursome.playerCore[thisFoursome.iCurrentPlayer].pedGolfPlayer)) 
			IF fDist < fClosestDist
				fClosestDist = fDist
				iNearestIndex = iPlayerIndex
			ENDIF
		ENDIF
	ENDREPEAT
	IF iNearestIndex < 0 OR iNearestIndex >= GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome)
		RETURN thisFoursome.iCurrentPlayer
	ENDIF
	RETURN iNearestIndex
ENDFUNC

