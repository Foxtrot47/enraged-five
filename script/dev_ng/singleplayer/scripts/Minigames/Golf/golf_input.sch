USING "minigames_helpers.sch"
USING "golf.sch"
USING "golf_helpers.sch"
USING "golf_cameras.sch"
USING "golf_swing_meter_lib.sch"
USING "golf_ui.sch"
USING "golf_clubs_lib.sch"
USING "golf_physics_lib.sch"


INT GOLF_MAX_PAD_SHAKE_FREQUENCY = 200

FUNC BOOL GOLF_WOULD_MOVING_CLIP_INTO_OBJECT(VECTOR vBallPos, FLOAT fAim, FLOAT fRotationStep, eCLUB_TYPE clubType)
	FLOAT fCameraAngle, fHieght
	
	VECTOR vCurrentPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBallPos, fAim - 90, <<VMAG(GET_GOLF_CLUB_PLACEMENT_OFFSET(clubType)), 0.0, 0.0>>)
	VECTOR vNewPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBallPos, fAim - 90 -fRotationStep, <<VMAG(GET_GOLF_CLUB_PLACEMENT_OFFSET(clubType)), 0.0, 0.0>>)
	
	IF GET_GROUND_Z_FOR_3D_COORD(vCurrentPos +<<0,0,2.0>>, vCurrentPos.z)
		DRAW_DEBUG_SPHERE(vCurrentPos, 0.15, 0, 125, 125)
		
		IF GET_GROUND_Z_FOR_3D_COORD(vNewPos+<<0,0,2.0>>, fHieght)
			DRAW_DEBUG_SPHERE(<<vNewPos.x, vNewPos.y, fHieght>>, 0.15, 125, 125, 0)
			
			IF ABSF(vCurrentPos.z - fHieght) > 0.4
				CDEBUG1LN(DEBUG_GOLF,"No move to hieght change ", ABSF(vCurrentPos.z - fHieght))
				CDEBUG1LN(DEBUG_GOLF,"Too big a difference in new position")
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF fRotationStep > 0.0
		fCameraAngle = fAim - 120 - fRotationStep
	ELSE
		fCameraAngle = fAim - 60 - fRotationStep
	ENDIF
	
	vBallPos.z = vCurrentPos.z + 1.0
	
	RETURN IS_GOLF_COURSE_OBSTRUCTING_AT_POSTION(vBallPos, fCameraAngle, <<1.0, 0.0, 0.0>>)

ENDFUNC
//
//FUNC FLOAT GET_DISTANCE_FOR_STREAM_FRUSTUM(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, VECTOR vStreamPos)
//	FLOAT fDist = VDIST(GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), vStreamPos)
//	FLOAT fReturnVal
//
//	IF fDist >= 75
//		fReturnVal = 75.0
//	ELSE
//		fReturnVal = 75.0	
//	ENDIF
//	
//	RETURN fReturnVal
//ENDFUNC

PROC GOLF_HANDLE_SKIP_SHOT_STREAMING(GOLF_COURSE& thisCourse, GOLF_FOURSOME& thisFoursome, GOLF_HELPERS& thisHelpers)
	FLOAT fAim
	VECTOR vLoadScenePos, vLoadSceneDir, vEndNavBlockingPos
	FLOAT fDist
	
	fAim = GET_HEADING_BETWEEN_VECTORS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))) + 90
	vLoadScenePos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
	vLoadSceneDir = NORMALISE_VECTOR(<<COS(fAim), SIN(fAim), 0>>)
	vEndNavBlockingPos = 50.0*vLoadSceneDir
	DRAW_DEBUG_SPHERE(vLoadScenePos, 0.25, 255, 0, 0)
	DRAW_DEBUG_SPHERE(vLoadScenePos + vEndNavBlockingPos, 0.25, 255, 255, 255)
	DRAW_DEBUG_LINE(vLoadScenePos, vLoadScenePos + vEndNavBlockingPos, 0, 255, 0)

	IF STREAMVOL_IS_VALID(thisHelpers.steamVolNextShot)
		STREAMVOL_DELETE(thisHelpers.steamVolNextShot)
	ENDIF

	IF NOT STREAMVOL_IS_VALID(thisHelpers.steamVolNextShot)
		CDEBUG1LN(DEBUG_GOLF,"Create next shot stream vol")
		IF DOES_GOLF_NAV_MESH_BLOCKING_OBJECT_EXIST(thisHelpers)
			REMOVE_NAVMESH_BLOCKING_OBJECT(thisHelpers.iBlockNavMeshIndex)
		ENDIF
		fDist = 75 //GET_DISTANCE_FOR_STREAM_FRUSTUM(thisCourse, thisFoursome, vLoadScenePos) 
		thisHelpers.iBlockNavMeshIndex = ADD_NAVMESH_BLOCKING_OBJECT( vLoadScenePos + vEndNavBlockingPos, <<2, 100, 10>>, DEG_TO_RAD(fAim - 90))
		thisHelpers.steamVolNextShot = STREAMVOL_CREATE_FRUSTUM(vLoadScenePos, vLoadSceneDir, fDist, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)
	ENDIF

ENDPROC


/// PURPOSE:
///    Manages all input for free navigation mode
/// PARAMS:
///    thisGame - 
///    thisGameState - 
///    thisPlayer - 
///    thisPlayerCore - 
PROC GOLF_MANAGE_NAVIGATE_INPUT(GOLF_GAME &thisGame, GOLF_GAME_STATE &thisGameState, GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_HELPERS &thisHelpers)
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // Fix for B* 1870409
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_SCRIPT_LB)
	ENDIF
	
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LB)
		DEBUG_MESSAGE("GMS_NAVIGATE_TO_SHOT - player pressing skip")
		
		IF NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_STREAM_NEXT_SHOT_FOR_SKIPPING)
			//DISABLE_GOLF_MINIMAP()
			SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_STREAM_NEXT_SHOT_FOR_SKIPPING)
			GOLF_HANDLE_SKIP_SHOT_STREAMING(thisCourse, thisFoursome, thisHelpers)
			PLAY_SOUND_FRONTEND(-1, "HIGHLIGHT_NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			RESTART_TIMER_NOW(thisHelpers.skipTimer)
		ENDIF
	
	ENDIF
	
	IF IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_STREAM_NEXT_SHOT_FOR_SKIPPING)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
		
		IF IS_STREAMVOL_ACTIVE()
		AND STREAMVOL_HAS_LOADED(thisHelpers.steamVolNextShot)
		OR TIMER_DO_WHEN_READY(thisHelpers.skipTimer, 5.0)
			SET_GOLF_MINIMAP(thisFoursome, thisCourse)
			SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
			SET_GOLF_CONTROL_FLAG(thisGame, GCF_PLAYER_SKIP)
			SET_GLOBAL_GOLF_CONTROL_FLAG(GGCF_PLAYER_SKIP)
			SET_GOLF_CONTROL_FLAG(thisGame, GCF_TELPORT_CART)
			CLEAR_GOLF_FOURSOME_LOCAL_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_PLAYER_NAV_HEADING)
			CLEAR_GOLF_HELP_DISPLAYED_FLAG(thisHelpers, GHD_APPROCH_BALL_FOOT_DISPLAYED)
			REMOVE_GOLF_FOURSOME_FROM_GROUP(thisFoursome)
			REMOVE_PARTICLE_FX_FROM_ENTITY(PLAYER_PED_ID())
			thisGameState = GGS_TAKING_SHOT
			
			CANCEL_TIMER(thisHelpers.skipTimer)
			CLEAR_GOLF_STREAMING_FLAG(thisHelpers, GSF_STREAM_NEXT_SHOT_FOR_SKIPPING)
			//GOLF_ATTACH_CLUB_TO_PLAYER(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag)
		ENDIF
	ENDIF
	
	//*
	INT iLeftX, iLeftY, iRightX, iRightY
	GET_GOLF_ANALOG_STICK_VALUES(iLeftX, iLeftY, iRightX, iRightY)
	//IF iRightX != 0
	//	SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
	//ENDIF
	
	IF GET_SCRIPT_TASK_STATUS( PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS( PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK
		IF (iLeftX != 0 OR iLeftY !=0) AND NOT IS_PED_DUCKING(PLAYER_PED_ID()) AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CLEAR_PED_TASKS(GET_GOLF_FOURSOME_LOCAL_PLAYER_PED(thisFoursome))
	    //SET_GOLF_PLAYER_CONTROL_FLAG(thisPlayerCore, GOLF_TASK_STOPPED)
		ENDIF
	ENDIF
	//*/
ENDPROC

/// PURPOSE:
///    Manages input for address mode
/// PARAMS:
///    thisGame - 
///    thisCourse - 
///    thisPlayer - 
///    thisFoursome - 
///    thisHelpers - 
/// RETURNS:
///    New golf player state if the player has started his swing, otherwise returns ADDRESS
FUNC GOLF_PLAYER_STATE GOLF_MANAGE_ADDRESS_INPUT(GOLF_GAME &thisGame,GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	BOOL bXPress  = IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
	BOOL bOPress  = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
//	BOOL bSquarePressed = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
	BOOL bLTPress = IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT)
	BOOL bRTPress = IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
	BOOL bDLPress = FALSE
	BOOL bDRPress = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LB)
	BOOL bDUPress = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP)
	BOOL bDDPress = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN)
			
//	BOOL bDLPress = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
//	BOOL bDRPress = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)

	INT iLeftX, iLeftY, iRightX, iRightY
	GET_GOLF_ANALOG_STICK_VALUES(iLeftX, iLeftY, iRightX, iRightY) //GET_POSITION_OF_ANALOGUE_STICKS(GET_GOLF_PLAYER_PAD_NUMBER(thisPlayer), iLeftX, iLeftY, iRightX, iRightY)
	
	// Fix for B* 2309115
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		RETURN GPS_ADDRESS_BALL
	ENDIF
	
	// B*2191771 PC controls need to use left mouse button to control the start of the swing so if pressed ensured we'll start the swing
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_ATTACK)
		iLeftY = 128
	ENDIF
	
	IF NOT GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers)
		IF NOT bXPress AND NOT bLTPress AND NOT bDLPress  AND NOT bDRPress AND NOT bDUPress  AND NOT bDDPress
		AND (ABSI(iLeftX) + ABSI(iLeftY)< 1) // ABSI(iRightX) + ABSI(iRightY) 
			SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, TRUE)
		ENDIF
	ENDIF
	
	#IF GOLF_IS_MP
		//When game is paused we need to pause the cameras
		IF IS_PAUSE_MENU_ACTIVE() OR IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_MP_START_QUIT_MENU)
			IF DOES_CAM_EXIST(thisHelpers.camPreviewSplineForward)
				SET_CAM_SPLINE_PHASE(thisHelpers.camPreviewSplineForward, thisHelpers.fPreviewSplinePhase)
				
				IF IS_CAM_RENDERING(thisHelpers.camPreviewSplineForward)
					GOLF_CREATE_ANIMATED_CAMERA_PAUSE_CAMERA(thisHelpers, thisHelpers.camPreviewSplineForward)
				ENDIF
			ENDIF
			IF DOES_CAM_EXIST(thisHelpers.camLookAtGreen)
				IF thisHelpers.fHoleSplinePhase < 0
					thisHelpers.fHoleSplinePhase = 0
				ENDIF
				
				SET_CAM_ANIM_CURRENT_PHASE(thisHelpers.camLookAtGreen, thisHelpers.fHoleSplinePhase)
				IF IS_CAM_RENDERING(thisHelpers.camLookAtGreen)
					GOLF_CREATE_ANIMATED_CAMERA_PAUSE_CAMERA(thisHelpers, thisHelpers.camLookAtGreen)
				ENDIF
			ENDIF
			
			RETURN GPS_ADDRESS_BALL
		ELIF IS_GOLF_CAM_RENDERING(thisHelpers.camPauseAnimCamera)
			DESTROY_CAM(thisHelpers.camPauseAnimCamera)
			
			RETURN GPS_ADDRESS_BALL
		ENDIF
	#ENDIF
	
	IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_STOP_INPUT)
	OR IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SCOREBOARD_DISPLAYED)
	OR GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome) != SAS_IDLE
	OR (IS_GOLFER_PLAYING_CLUB_SWITCH_ANIM(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
		AND NOT thisHelpers.bShotPreviewCam)
		RETURN GPS_ADDRESS_BALL
	ENDIF

	IF bOPress AND GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers) AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PLAYER_REPOSITIONED)
	   AND NOT thisHelpers.bJustExitQuit AND thisHelpers.bGreenPreviewCam = 0 AND
	   (NOT thisHelpers.bShotPreviewCam OR (DOES_CAM_EXIST(thisHelpers.camPreview) AND IS_CAM_RENDERING(thisHelpers.camPreview)))
		PICK_IDEAL_CLUB_AND_SHOT(thisGame, thisCourse,thisFoursome, thisHelpers)
		PLACE_PLAYER_AT_BALL(thisCourse, thisFoursome, thisGame.golfBag, FALSE, TRUE, IS_TEE_SHOT(thisCourse, thisFoursome), TRUE, FALSE)
		SET_METER_MAX_DISTANCE(thisGame, thisCourse, thisFoursome)
		SET_METER_CURRENT_DISTANCE(thisFoursome.swingMeter, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame)
		IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome) //IS_BALL_ON_GREEN(thisCourse, thisFoursome)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome, GOLF_FIND_SHOT_ESTIMATE(thisFoursome, thisGame.golfBag) )
		ELSE
			GOLF_SET_GOLF_TRAIL_EFFECT(thisGame, thisFoursome, thisHelpers)
			GOLF_DRAW_GOLF_TRAIL_EFFECT(thisHelpers, thisFoursome)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome, thisHelpers.golfTrail.checkPointPos)
		ENDIF
		IF thisHelpers.bShotPreviewCam
			ADJUST_PREVIEW_CAMERA(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
		ELSE
			ADJUST_ADDRESS_CAMERA(thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome))
		ENDIF
		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_FLOATING)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_PLAYER_REPOSITIONED)
		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_ADDRESS | GUC_REFRESH_SWING)
		//SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
	ENDIF
	
	IF bRTPress AND thisHelpers.bGreenPreviewCam = 0
		IF NOT thisHelpers.bShotPreviewCam
		AND (IS_GOLF_TRAIL_INTERSECT_FLAG_SET() AND NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_REFRESH_GOLF_TRAIL) OR IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome))
			IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
				ADJUST_PREVIEW_CAMERA(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), TRUE)
				SET_CAM_ACTIVE_WITH_INTERP(thisHelpers.camPreview, thisHelpers.camAddress, 500)
			ELSE
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_SWING)
				IF DOES_CAM_EXIST(thisHelpers.camPreviewSplineForward)
					DESTROY_CAM(thisHelpers.camPreviewSplineForward)
				ENDIF
				
				ADJUST_PREVIEW_CAMERA(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), TRUE)
				
				SET_CAM_ACTIVE(thisHelpers.camPreviewSplineForward, TRUE)
				SET_FOCUS_POS_AND_VEL(GET_CAM_COORD(thisHelpers.camPreviewSplineForward), <<0,0,0>>)
				START_AUDIO_SCENE("GOLF_FLY_CAM")
			ENDIF
			
			CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_INPUT)
			thisHelpers.bShotPreviewCam = TRUE
		ENDIF
		
		IF DOES_CAM_EXIST(thisHelpers.camPreviewSplineForward)
			IF IS_CAM_RENDERING(thisHelpers.camPreviewSplineForward)
				thisHelpers.fPreviewSplinePhase = GET_CAM_SPLINE_PHASE(thisHelpers.camPreviewSplineForward)
				CDEBUG1LN(DEBUG_GOLF,"GEtting phase ", thisHelpers.fPreviewSplinePhase)
			ENDIF
		ENDIF
	ELSE
		IF thisHelpers.bShotPreviewCam 
			IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
				ADJUST_ADDRESS_CAMERA(thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome))
				SET_CAM_ACTIVE_WITH_INTERP(thisHelpers.camAddress, thisHelpers.camPreview, 500)
				CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_INPUT)
				thisHelpers.bShotPreviewCam = FALSE
			ELSE
				IF NOT IS_CAM_RENDERING(thisHelpers.camPreviewSplineForward)
					CDEBUG1LN(DEBUG_GOLF,"Make preview cam")
					IF DOES_CAM_EXIST(thisHelpers.camPreviewSplineForward)
						DESTROY_CAM(thisHelpers.camPreviewSplineForward)
					ENDIF
					ADJUST_ADDRESS_CAMERA(thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome))
					ADJUST_PREVIEW_CAMERA(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)), TRUE)
					thisHelpers.fPreviewSplinePhase = 1.0
					SET_CAM_ACTIVE(thisHelpers.camPreviewSplineForward, TRUE)
				ENDIF
				CDEBUG1LN(DEBUG_GOLF,"Setting phase to ", thisHelpers.fPreviewSplinePhase)
				SET_CAM_SPLINE_PHASE(thisHelpers.camPreviewSplineForward,thisHelpers.fPreviewSplinePhase)
				
				thisHelpers.fPreviewSplinePhase -= 0.045
				IF thisHelpers.fPreviewSplinePhase <= 0
					CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_INPUT)
					STOP_AUDIO_SCENE("GOLF_FLY_CAM")
					thisHelpers.bShotPreviewCam = FALSE
					thisHelpers.fPreviewSplinePhase = 0
					SET_FOCUS_POS_AND_VEL(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), <<0,0,0>>)
					
					IF DOES_CAM_EXIST(thisHelpers.camAddress)
						IF NOT IS_CAM_RENDERING(thisHelpers.camAddress) AND thisHelpers.bGreenPreviewCam = 0
							SET_CAM_ACTIVE(thisHelpers.camAddress, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_CAM_EXIST(thisHelpers.camAddress)
				IF NOT IS_CAM_RENDERING(thisHelpers.camAddress) AND thisHelpers.bGreenPreviewCam = 0
					SET_CAM_ACTIVE(thisHelpers.camAddress, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(thisHelpers.camPreview) AND DOES_CAM_EXIST(thisHelpers.camAddress)
		IF IS_CAM_INTERPOLATING(thisHelpers.camPreview) OR IS_CAM_INTERPOLATING(thisHelpers.camAddress)
			RETURN GPS_ADDRESS_BALL
		ENDIF
	ENDIF
	
	IF IS_GOLF_CAM_INTERPOLATING(thisHelpers.camPreviewSplineForward, FALSE) AND thisHelpers.bShotPreviewCam 
		RETURN GPS_ADDRESS_BALL
	ENDIF
	
	IF bLTPress AND GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers) AND NOT thisHelpers.bShotPreviewCam
		
		IF NOT IS_BALL_IN_RANGE_OF_GREEN(thisCourse, thisFoursome) 
			GOLF_SETUP_LOOK_AT_GREEN_CAMERA(thisFoursome, thisCourse, thisHelpers)
			SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_SWING)
			GOLF_TRAIL_SET_ENABLED(FALSE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE) //the ground the player is standing on might load out of memory and they will fall
			thisHelpers.bGreenPreviewCam = 1
			IF DOES_CAM_EXIST(thisHelpers.camLookAtGreen)
				IF IS_CAM_RENDERING(thisHelpers.camLookAtGreen)
					thisHelpers.fHoleSplinePhase = GET_CAM_ANIM_CURRENT_PHASE(thisHelpers.camLookAtGreen)
					CDEBUG1LN(DEBUG_GOLF,"GEtting phase ", thisHelpers.fHoleSplinePhase)
				ENDIF
			ENDIF
		ENDIF
	ELSE
	
		IF thisHelpers.bGreenPreviewCam = 1
			IF DOES_CAM_EXIST(thisHelpers.camLookAtGreen)
				
				IF thisHelpers.fHoleSplinePhase < 0
					thisHelpers.fHoleSplinePhase = 0
				ENDIF
				
				SET_CAM_ANIM_CURRENT_PHASE(thisHelpers.camLookAtGreen,thisHelpers.fHoleSplinePhase)
		
				thisHelpers.fHoleSplinePhase -= 0.045
				IF thisHelpers.fHoleSplinePhase <= 0
				OR thisHelpers.fHoleSplinePhase <= thisHelpers.fHoleSplineStartPhase
					thisHelpers.bGreenPreviewCam = 0
					SET_CAM_ACTIVE(thisHelpers.camAddress, TRUE)
					DESTROY_CAM(thisHelpers.camLookAtGreen )
					GOLF_TRAIL_SET_ENABLED(TRUE)
					SET_FOCUS_POS_AND_VEL(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), <<0,0,0>>)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_SWING)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Set default dead-zone value
	INT iRightDeadZone = 20
	
	// No dead-zone when using mouse
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		iRightDeadZone = 0
	ENDIF
	
	IF ((iLeftY > 1 AND GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers) AND IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome))
	OR (iLeftY > 60 AND GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers) AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)))
	AND NOT thisHelpers.bShotPreviewCam AND thisHelpers.bGreenPreviewCam = 0 //AND NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SPLASH_TEXT_DISPLAYED)
		IF bDebugSpew DEBUG_MESSAGE("GMS_ADDRESS_BALL - X is down, player is starting swing meter") ENDIF
		SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
		GOLF_PLAY_BACKSWING_SOUND(thisFoursome)
		thisFoursome.swingMeter.fCurrentMeterPosition = 0.0
		//thisPlayers[0].vShotEstimate = <<0,0,0>>
		ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)

		CANCEL_TIMER(thisHelpers.uiTimer)
//		DELETE_PROJECTION_LINE()
		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_ADDRESS | GUC_REFRESH_SWING)
		RESTART_TIMER_NOW(thisHelpers.resetTimer) //in the swing meter state, reset timer is a fail safe to make sure the player isn't stuck in that mode
		RETURN GPS_SWING_METER
	ELIF ((ABSI(iRightX) > iRightDeadZone OR ABSi(iRightY) > iRightDeadZone) AND GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers)) AND thisHelpers.bGreenPreviewCam = 0
	#IF IS_DEBUG_BUILD
	OR bRepositionPlayer
	#ENDIF
		CLEAR_GOLF_LINE_FLAGS()
		// Shot prediction adjustments
		FLOAT fSwapInputs = 1.0
		FLOAT rotationDelta = PICK_FLOAT(IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome), 0.0015, 0.003)
		rotationDelta = (rotationDelta*(1.0-GET_GOLF_PLAYER_ACCURACY()) + (2.0*rotationDelta)*GET_GOLF_PLAYER_ACCURACY())
		FLOAT powerDelta = 0.0085
		rotationDelta *= SQRT(GET_GOLF_CLUB_RANGE(thisGame.golfBag, ENUM_TO_INT(eCLUB_WOOD_1))/VDIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), GOLF_FIND_SHOT_ESTIMATE(thisFoursome, thisGame.golfBag)))
		
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_GREEN_LONG
			powerDelta = 0.0055
		ENDIF
		
		SET_METER_MAX_DISTANCE(thisGame, thisCourse, thisFoursome)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome, GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome) - (TO_FLOAT(iRightY) * powerDelta * fSwapInputs))
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome) > 100.0
			SET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome, 100.0)
		ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome) < 5.0 AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome) 
			SET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome, 5.0)
		ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome) < 0.0 
			SET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome, 0.0)
		ENDIF

		VECTOR vBallPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
		FLOAT fAim = GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome)
		
		eCLUB_TYPE eMoveClubType = GET_GOLF_CLUB_TYPE(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
		//Check to see if the player will move inside of an object
		IF GOLF_WOULD_MOVING_CLIP_INTO_OBJECT(vBallPos, fAim, (TO_FLOAT(iRightX) * rotationDelta * fSwapInputs), eMoveClubType)
			CDEBUG1LN(DEBUG_GOLF,"hitting thing dont move")
			fSwapInputs = 0
		ENDIF
		
		IF ABSI(iRightX) > 20
			PLACE_PLAYER_AT_BALL_WITH_HEADING(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome) - (TO_FLOAT(iRightX) * rotationDelta * fSwapInputs), FALSE, FALSE)
			GOLF_BEGIN_SHUFFLE_ANIMATION(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag)
		ENDIF
				
		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_ADDRESS | GUC_REFRESH_PERMANENT | GUC_REFRESH_SWING)
		IF  IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome, GOLF_FIND_SHOT_ESTIMATE(thisFoursome, thisGame.golfBag))
		ENDIF
	
		IF thisHelpers.bShotPreviewCam
			ADJUST_PREVIEW_CAMERA(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
		ELSE
			ADJUST_ADDRESS_CAMERA(thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome))
		ENDIF
		
		GOLF_SET_GOLF_TRAIL_EFFECT(thisGame, thisFoursome, thisHelpers)
		GOLF_DRAW_GOLF_TRAIL_EFFECT(thisHelpers, thisFoursome)
		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_FLOATING)
		INITIALIZE_FLOATING_HELP_FOR_SHOT(thisFoursome, thisHelpers, thisCourse)
		
		CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_PLAYER_REPOSITIONED)
	ENDIF
	
	
	IF TIMER_DO_WHEN_READY(thisHelpers.inputTimer, 0.1)
		eCLUB_TYPE eMinClubType, eMaxClubType, eNextClubType, ePrevClubType
		// We are changing clubs or swing style
		IF (bDRPress OR bDLPress OR bDDPress OR bDUPress) //AND GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers)
		AND thisHelpers.bGreenPreviewCam = 0
			START_TIMER_NOW_SAFE(thisHelpers.inputTimer)
			CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_PLAYER_REPOSITIONED)
			SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
			BOOL bChangedClubStyle = FALSE
			// We are changing club
			//IF NOT IS_BALL_ON_GREEN(thisCourse, thisFoursome) //don't change from putter when on green
				IF bDDPress OR bDUPress
					bChangedClubStyle = TRUE
					MODEL_NAMES oldClubModel = GET_GOLF_CLUB_MODEL(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
					ePrevClubType =  GET_GOLF_CLUB_TYPE(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
			
					IF bDDPress
						INCREMENT_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome, 1)
						GOLF_GET_CLUB_RANGE_FOR_SITUATION(thisCourse, thisFoursome, eMinClubType, eMaxClubType, TRUE)
						eNextClubType = GET_GOLF_CLUB_TYPE(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
						
						IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome) >= thisGame.golfBag.iNumClubs OR eNextClubType > eMaxClubType //(GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome) = GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eCLUB_PUTTER))
							IF eNextClubType > eMaxClubType
								CDEBUG1LN(DEBUG_GOLF,"Golf club incrememted out of range for situation, setting to min club for context")
								eNextClubType = eMinClubType
							ELSE
								CDEBUG1LN(DEBUG_GOLF,"Golf club incrememted out of range of bag, setting to max club for context")
							ENDIF
							SET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome, GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eMinClubType))
						ENDIF
					ELIF bDUPress
						INCREMENT_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome, -1)
						GOLF_GET_CLUB_RANGE_FOR_SITUATION(thisCourse, thisFoursome, eMinClubType, eMaxClubType, TRUE)
						eNextClubType = GET_GOLF_CLUB_TYPE(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
					
						IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome) < 0 OR eNextClubType < eMinClubType //(GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome) = GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eCLUB_PUTTER))
							IF eNextClubType < eMinClubType
								CDEBUG1LN(DEBUG_GOLF,"Golf club decremented out of range for situation, setting to max club for context")
								eNextClubType = eMaxClubType
							ELSE
								CDEBUG1LN(DEBUG_GOLF,"Golf club decremented out of range of bag, setting to max club for context")
							ENDIF
							SET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome, GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eMaxClubType))
						ENDIF
					ENDIF
					
					PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					IF GET_GOLF_CLUB_MODEL(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))!= oldClubModel
					OR (ePrevClubType = eCLUB_WEDGE_PITCH AND eNextClubType = eCLUB_IRON_9) OR (ePrevClubType = eCLUB_IRON_9  AND eNextClubType = eCLUB_WEDGE_PITCH)
						eCLUB_TYPE clubType = GET_GOLF_CLUB_TYPE(thisGame.golfBag,  GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
						PED_INDEX pedIndex = GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome)
						CLEANUP_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome)
						SET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome, CREATE_GOLF_CLUB(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag))
						ATTACH_ENTITY_TO_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_CLUB(thisFoursome), pedIndex, GET_PED_BONE_INDEX( pedIndex, BONETAG_PH_R_HAND), GET_GOLF_CLUB_ATTACH_OFFSET(pedIndex, clubType), GET_GOLF_CLUB_ATTACH_ROTATION(pedIndex, clubType)) //, <<0.03, 0.017, 0.0>>,  <<4.75, 3.0, 1.25>>, FALSE)
						
						VECTOR newClubCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), GET_GOLF_CLUB_PLACEMENT_OFFSET(clubType))
						GET_GROUND_Z_FOR_3D_COORD(newClubCoord+<<0,0,1.0>>, newClubCoord.z)
						newClubCoord.z += 1.0
						
//						SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), newClubCoord, FALSE, TRUE, TRUE)
//						SET_ENTITY_HEADING(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome))
						
						CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_FORCE_UPDATE)
						GOLF_PLAY_CLUB_SWITCH_ANIM(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag, ePrevClubType, newClubCoord) 
						//GOLF_FORCE_IDLE_ANIMATION(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)] , thisGame.golfBag)
						
						IF eNextClubType = eCLUB_PUTTER
							//switching to the last selected putt style
							thisHelpers.eLastSwingStyle = GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome)
							SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, thisHelpers.eLastPuttStyle)
						ELIF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
							//switching to the last selected swing style
							thisHelpers.eLastPuttStyle = GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome)
							SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, thisHelpers.eLastSwingStyle)
						ENDIF
					ENDIF
				ENDIF
			//ENDIF
			
			IF bDLPress OR bDRPress
				bChangedClubStyle = TRUE
				// We are changing swing style
				INCREMENT_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, PICK_INT( bDLPress, -1, 1))
				
				IF GET_GOLF_CLUB_TYPE(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)) != eCLUB_PUTTER
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) < SWING_STYLE_NORMAL
						SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, SWING_STYLE_APPROACH)
					ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) > SWING_STYLE_APPROACH
						SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, SWING_STYLE_NORMAL)
					ENDIF
				ELSE
					IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) < SWING_STYLE_GREEN_SHORT
						SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, SWING_STYLE_GREEN_LONG)
					ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) > SWING_STYLE_GREEN_LONG
						SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome, SWING_STYLE_GREEN_SHORT)
					ENDIF
				ENDIF
				
				PLAY_SOUND_FRONTEND(-1,"NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			ENDIF
			
			IF bChangedClubStyle
				GOLF_PRINT_CHANGED_SHOT_TYPE_HELP(thisGame, thisFoursome, thisHelpers, bDUPress OR bDDPress)
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_ADDRESS | GUC_REFRESH_SWING)
				SET_GOLF_TRAIL_STYLE_CHANGE_FLAG(TRUE)
				CLEAR_GOLF_LINE_FLAGS()
				SET_METER_MAX_DISTANCE(thisGame, thisCourse, thisFoursome)
				IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)//IS_BALL_ON_GREEN(thisCourse, thisFoursome)
					GOLF_SET_GOLF_TRAIL_EFFECT(thisGame, thisFoursome, thisHelpers)
					GOLF_DRAW_GOLF_TRAIL_EFFECT(thisHelpers, thisFoursome)
				ELSE
					SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome, GOLF_FIND_SHOT_ESTIMATE(thisFoursome, thisGame.golfBag))
					IF thisHelpers.bShotPreviewCam
						ADJUST_PREVIEW_CAMERA(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
					ELSE
						ADJUST_ADDRESS_CAMERA(thisFoursome, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisHelpers, GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome))
					ENDIF
					
				ENDIF
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_FLOATING)
				IF thisHelpers.bShotPreviewCam
					SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE) // Would like to get a slower repeat here, but for now requires a discrete press
					SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN GPS_ADDRESS_BALL
ENDFUNC

/// PURPOSE:
///    Manages input for stick shots
/// PARAMS:
///    thisPlayer - 
///    thisFoursome - 
///    golfBag - 
PROC GOLF_MANAGE_SWING_INPUT_STICK_SWING(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_BAG &golfBag, GOLF_HELPERS &thisHelpers)
	// Cache all buttons we are interested in. If none are pressed, we can reset input
	INT iLeftX, iLeftY, iRightX, iRightY
	GET_GOLF_ANALOG_STICK_VALUES(iLeftX, iLeftY, iRightX, iRightY) //GET_POSITION_OF_ANALOGUE_STICKS(GET_GOLF_PLAYER_PAD_NUMBER(thisPlayer), iLeftX, iLeftY, iRightX, iRightY)
	
	// Fix for B* 2309115
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	// B*2191771 PC controls need to use left mouse button to control the start of the swing and top of the swing
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		iLeftY = 0 // Nullify whatever GET_GOLF_ANALOG_STICK_VALUES returned
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_ATTACK)
			iLeftY = -128 // Only set iLeftY if the player presses left mouse button
		ENDIF
	ENDIF
		
	FLOAT fMeterSpeedModifier = PICK_FLOAT(IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome), SWING_METER_TIME_PUTT, SWING_METER_TIME_SWING) 
	//the meter needs to move slower when at low percentages
	IF thisFoursome.swingMeter.fCurrentMeterPosition < 30.0
		fMeterSpeedModifier *= 1.0 + (30.0-thisFoursome.swingMeter.fCurrentMeterPosition)/30.0
	ENDIF
	
	FLOAT meterDelta = GET_FRAME_TIME() * 100.0 / fMeterSpeedModifier
	
//	PRINTSTRING("iLeftY=")PRINTINT(iLeftY)PRINTNL()
//	PRINTSTRING("iLeftX=")PRINTINT(iLeftX)PRINTNL()
//	PRINTSTRING("iRightY=")PRINTINT(iRightY)PRINTNL()
//	PRINTSTRING("iRightX=")PRINTINT(iRightX)PRINTNL()
//	PRINTSTRING("iLastY=")PRINTINT(thisFoursome.swingMeter.iLastY)PRINTNL()
//	PRINTSTRING("iLastX=")PRINTINT(thisFoursome.swingMeter.iLastX)PRINTNL()
	
	IF thisFoursome.swingMeter.meterState != SWING_METER_INACTIVE
		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_SWING)
	ENDIF
	
	IF thisFoursome.swingMeter.iBackswingDelta > 0
		thisFoursome.swingMeter.iBackswingDelta += FLOOR(GET_FRAME_TIME()*1000)
	ENDIF
	
	SWITCH thisFoursome.swingMeter.meterState
		CASE SWING_METER_INACTIVE
//			CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_SWING_INPUT_STICK_SWING : SWING_METER_INACTIVE")
		
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome) = SAS_IDLE
				thisFoursome.swingMeter.meterState = SWING_METER_RISING
				SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome, SAS_SWINGING_BACK)
			ENDIF
//			CDEBUG1LN(DEBUG_GOLF,"SWING_METER_INACTIVE: Moving to SWING_METER_INACTIVE")
		BREAK
		CASE SWING_METER_RISING
			CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_SWING_INPUT_STICK_SWING : SWING_METER_RISING")
			IF thisFoursome.swingMeter.fCurrentMeterPosition > 100.0
				thisFoursome.swingMeter.fCurrentMeterPosition = 100.0
				thisFoursome.swingMeter.meterState = SWING_METER_FALLING_POWER
				CDEBUG1LN(DEBUG_GOLF,"SWING_METER_RISING: Moving to SWING_METER_FALLING_POWER")
			ELIF iLeftY < -80
				CDEBUG1LN(DEBUG_GOLF,"iLeftY < swingMeter.iLastY AND iLeftY > 20")
				thisFoursome.swingMeter.iBackswingTime = GET_GAME_TIMER()
				thisFoursome.swingMeter.iBackswingDelta = thisFoursome.swingMeter.iBackswingTime
			    // moved to SWING_METER_FAILLING state	SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome, SAS_SWINGING_FORWARD)
				GOLF_PLAY_BACKSWING_SOUND(thisFoursome)
				thisFoursome.swingMeter.fCurrentPowerRel = GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome)* 100.0 * TO_FLOAT(thisFoursome.swingMeter.iLastY) / 127.0
				thisFoursome.swingMeter.iBackswingX = thisFoursome.swingMeter.iLastX
				thisFoursome.swingMeter.meterState = SWING_METER_FALLING
				SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
				CDEBUG1LN(DEBUG_GOLF,"SWING_METER_RISING: Moving to SWING_METER_FALLING")
			ELSE
				IF thisFoursome.swingMeter.iLastY > 100
					IF thisFoursome.swingMeter.iBackswingTime = 0
						thisFoursome.swingMeter.iBackswingStartTime = GET_GAME_TIMER()
					ENDIF
				ENDIF
				thisFoursome.swingMeter.fCurrentMeterPosition += meterDelta
				IF ABSF(thisFoursome.swingMeter.fCurrentMeterPosition - GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome)) < 5
					CDEBUG1LN(DEBUG_GOLF,"Shake%% - 2")
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 50, GOLF_MAX_PAD_SHAKE_FREQUENCY )
				ENDIF
			ENDIF
		BREAK
		
		CASE SWING_METER_FALLING_POWER
			CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_SWING_INPUT_STICK_SWING : SWING_METER_FALLING_POWER")
			IF thisFoursome.swingMeter.fCurrentMeterPosition < 0.0
				SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome, SAS_RETURNING_TO_IDLE)
				thisFoursome.swingMeter.meterState = SWING_METER_FALSE_START
				thisFoursome.swingMeter.iBackswingTime = 0
				thisFoursome.swingMeter.iBackswingStartTime = 0
				thisFoursome.swingMeter.iBackswingDelta = 0
				thisFoursome.swingMeter.iBackswingHoldTime = 0
				CDEBUG1LN(DEBUG_GOLF,"FALSE START")
			ELIF iLeftY < -80
				CDEBUG1LN(DEBUG_GOLF,"SWING_METER_FALLING_POWER: iLeftY < swingMeter.iLastY AND iLeftY > 20")
				thisFoursome.swingMeter.iBackswingTime = GET_GAME_TIMER()
				thisFoursome.swingMeter.iBackswingDelta = thisFoursome.swingMeter.iBackswingTime
				// moved to SWING_METER_FAILLING state //SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome, SAS_SWINGING_FORWARD)
				GOLF_PLAY_BACKSWING_SOUND(thisFoursome)
				thisFoursome.swingMeter.fCurrentPowerRel = GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome)* 100.0 * TO_FLOAT(thisFoursome.swingMeter.iLastY) / 127.0
				thisFoursome.swingMeter.iBackswingX = thisFoursome.swingMeter.iLastX
				thisFoursome.swingMeter.meterState = SWING_METER_FALLING
				SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
				CDEBUG1LN(DEBUG_GOLF,"SWING_METER_FALLING_POWER: Moving to SWING_METER_FALLING")
			ELSE
				thisFoursome.swingMeter.fCurrentMeterPosition -= meterDelta * 2
				IF ABSF(thisFoursome.swingMeter.fCurrentMeterPosition - GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome)) < 5
					CDEBUG1LN(DEBUG_GOLF,"Shake%% - 3")
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 50, GOLF_MAX_PAD_SHAKE_FREQUENCY )
				ENDIF
			ENDIF
		BREAK
		
		CASE SWING_METER_FALLING
			CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_SWING_INPUT_STICK_SWING : SWING_METER_FALLING")
			IF thisFoursome.swingMeter.fCurrentMeterPosition < 0.0
				SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome, SAS_RETURNING_TO_IDLE)
				thisFoursome.swingMeter.meterState = SWING_METER_FALSE_START
				thisFoursome.swingMeter.iBackswingTime = 0
				thisFoursome.swingMeter.iBackswingStartTime = 0
				thisFoursome.swingMeter.iBackswingDelta = 0
				thisFoursome.swingMeter.iBackswingHoldTime = 0
				CDEBUG1LN(DEBUG_GOLF,"SWING_METER_FALLING: FALSE START")
			ELIF iLeftY < -80
			OR IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B*2191771 When it reaches here on PC the user has already pressed left mouse button to swing
				IF  TO_FLOAT((thisFoursome.swingMeter.iBackswingDelta  - thisFoursome.swingMeter.iBackswingTime)) < 1000
					IF ABSI(iLeftX) < 100 AND ABSI(thisFoursome.swingMeter.iBackswingX) < 100
						RESTART_TIMER_NOW(thisFoursome.physicsTimeout)
						thisFoursome.swingMeter.meterState = SWING_METER_FINISHED
						thisFoursome.swingMeter.fCurrentPowerRel = thisFoursome.swingMeter.fCurrentMeterPosition
																//GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome)
																//* 150.0 / TO_FLOAT(CLAMP_INT((GET_GAME_TIMER() - thisFoursome.swingMeter.iBackswingTime), 150, 1000))
																//* (CLAMP_INT((2000 - (thisFoursome.swingMeter.iBackswingHoldTime - thisFoursome.swingMeter.iBackswingStartTime)), 500, 2000) / 2000)
						IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"swingMeter.fCurrentPowerRel=",thisFoursome.swingMeter.fCurrentPowerRel) ENDIF
						
						thisFoursome.swingMeter.fCurrentAccurRel = TO_FLOAT(iLeftX)/100
						
						CDEBUG1LN(DEBUG_GOLF,"swingMeter.fCurrentAccurRel=",thisFoursome.swingMeter.fCurrentAccurRel) //ENDIF
						CDEBUG1LN(DEBUG_GOLF,"HIT", iLeftX," - ",thisFoursome.swingMeter.iBackswingX)
						
						
						CDEBUG1LN(DEBUG_GOLF,"Actual meter pos ", thisFoursome.swingMeter.fCurrentMeterPosition)
						CDEBUG1LN(DEBUG_GOLF,"Desired meter pos ", GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome))
						//Actual power is within % of desired power
						IF ABSF(thisFoursome.swingMeter.fCurrentMeterPosition - GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome)) <= 1.0
						AND ABSI(iLeftX) < 50
						#IF IS_DEBUG_BUILD #IF NOT GOLF_IS_AI_ONLY OR bPerfectAccuracyMode #ENDIF #ENDIF
							CDEBUG1LN(DEBUG_GOLF,"Perfect Hit!")
							SET_GOLF_CONTROL_FLAG(thisGame, GCF_WAS_PERFECT_HIT)
						ENDIF
					ELSE
						RESTART_TIMER_NOW(thisFoursome.physicsTimeout)
						thisFoursome.swingMeter.fCurrentPowerRel = GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome)
																* (CLAMP_INT((2000 - (thisFoursome.swingMeter.iBackswingHoldTime - thisFoursome.swingMeter.iBackswingStartTime)), 500, 2000) / 2000)
						thisFoursome.swingMeter.fCurrentAccurRel= (20.0 * GET_SWING_ACCURACY_RANGE(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], golfBag))
						IF iLeftX < 0
							thisFoursome.swingMeter.fCurrentAccurRel = -thisFoursome.swingMeter.fCurrentAccurRel
						ENDIF
						
						thisFoursome.swingMeter.meterState = SWING_METER_SHANK
						CDEBUG1LN(DEBUG_GOLF,"SHANK",iLeftX," - ",thisFoursome.swingMeter.iBackswingX)
					ENDIF
					
					FLOAT fBackSwingPhase 
					fBackSwingPhase = GET_GOLF_PLAYER_BACK_SWING_PHASE(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
					
					GOLF_PLAY_SWING_SOUND(thisFoursome, thisGame, IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_WAS_PERFECT_HIT), fBackSwingPhase)
					SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome, SAS_SWINGING_FORWARD)
				ELSE
					SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome, SAS_RETURNING_TO_IDLE)
					thisFoursome.swingMeter.meterState = SWING_METER_FALSE_START
					thisFoursome.swingMeter.iBackswingTime = 0
					thisFoursome.swingMeter.iBackswingStartTime = 0
					thisFoursome.swingMeter.iBackswingHoldTime = 0
					CDEBUG1LN(DEBUG_GOLF,"SWING_METER_FALLING: FALSE START")
				ENDIF
			ELSE
				IF TO_FLOAT((thisFoursome.swingMeter.iBackswingDelta  - thisFoursome.swingMeter.iBackswingTime)) < 1000
					thisFoursome.swingMeter.fCurrentMeterPosition -= meterDelta * 2
				ELSE
					SET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_ANIM(thisFoursome, SAS_RETURNING_TO_IDLE)
					thisFoursome.swingMeter.meterState = SWING_METER_FALSE_START
					thisFoursome.swingMeter.iBackswingTime = 0
					thisFoursome.swingMeter.iBackswingStartTime = 0
					thisFoursome.swingMeter.iBackswingHoldTime = 0
					CDEBUG1LN(DEBUG_GOLF,"SWING_METER_FALLING: FALSE START")
				ENDIF
			ENDIF
		BREAK
		CASE SWING_METER_SHANK
			CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_SWING_INPUT_STICK_SWING : SWING_METER_SHANK")
			// Do nothing, we are waiting for the ball to be hit
		BREAK
		CASE SWING_METER_FINISHED
			CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_SWING_INPUT_STICK_SWING : SWING_METER_FINISHED")
			// Do nothing, we are waiting for the ball to be hit
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Swing Meter UI in an invalid state")
		BREAK
	ENDSWITCH
	thisFoursome.swingMeter.iLastX = iLeftX
	thisFoursome.swingMeter.iLastY = iLeftY
ENDPROC


/// PURPOSE:
///    Manages swing input - forks depending on 3 click on stick swing shots
/// PARAMS:
///    thisFoursome - 
///    thisPlayer - 
///    golfBag - 
///    thisHelpers - 
PROC GOLF_MANAGE_SWING_INPUT(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome) 
		//GOLF_MANAGE_SWING_INPUT_PUTT( thisFoursome, thisGame.golfBag, thisHelpers)
		GOLF_MANAGE_SWING_INPUT_STICK_SWING(thisGame, thisFoursome, thisGame.golfBag, thisHelpers)
	ELSE //ELIF GET_GOLF_HELPER_STICK_SWING(thisHelpers)
		GOLF_MANAGE_SWING_INPUT_STICK_SWING(thisGame, thisFoursome, thisGame.golfBag, thisHelpers)
//	ELSE
		//GOLF_MANAGE_SWING_INPUT_3_CLICK(thisGame, thisPlayer, thisFoursome, thisHelpers)
	ENDIF
ENDPROC

/// PURPOSE:
///    Manages input during ball flight (ie spin meter)
/// PARAMS:
///    thisPlayer - 
///    thisplayerCore - 
///    swingMeter - 
PROC GOLF_MANAGE_FLIGHT_INPUT(GOLF_PLAYER_CORE &thisPlayerCore, SWING_METER &swingMeter, GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers)
	IF IS_GOLF_PLAYER_CONTROL_FLAG_SET(thisPlayerCore, GOLF_BALL_FIRST_COLLISION)
		EXIT
	ENDIF
	INT iLeftX, iLeftY, iRightX, iRightY
	GET_GOLF_ANALOG_STICK_VALUES(iLeftX, iLeftY, iRightX, iRightY)
	
	// Due to the way the PC control scheme works we're having to force the game
	// to use the move inputs here, as the script inputs have been re-directed to the mouse buttons for the golf club swing.
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		iLeftX  = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_MOVE_LR) * 128.0)
		iLeftY  = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_MOVE_UD) * 128.0)
	ENDIF
	
	
	BOOL bXPress = IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
	IF NOT GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers)
		IF NOT bXPress 
			SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, TRUE)
		ENDIF
	ENDIF
	FLOAT scalar = 3.0
	eCLUB_TYPE clubType = GET_GOLF_CLUB_TYPE(thisGame.golfBag, GET_GOLF_PLAYER_CURRENT_CLUB(thisPlayerCore))
	
	IF clubType <= eCLUB_WOOD_5
		scalar = 2.0
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bMaxSpin
		scalar = 100.0
	ENDIF
	#ENDIF
	IF bXPress AND GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers)
		IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"Spin update: ",iLeftX,",",iLeftY) ENDIF
		swingMeter.fXSpin -= (TO_FLOAT(iLeftY) / 128.0) * scalar
		swingMeter.fYSpin += (TO_FLOAT(iLeftX) / 128.0) * scalar
		
		IF swingMeter.fXSpin > 100
			swingMeter.fXSpin = 100
		ELIF swingMeter.fXSpin < -100
			swingMeter.fXSpin = -100
		ENDIF
		
		IF swingMeter.fYSpin > 100
			swingMeter.fYSpin = 100
		ELIF swingMeter.fYSpin < -100
			swingMeter.fYSpin = -100
		ENDIF	

		SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, FALSE)
	ENDIF
		
ENDPROC

PROC GOLF_MANAGE_SKIP_AI_INPUT(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse,  GOLF_HELPERS &thisHelpers)

	BOOL bXPress = IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
	
	IF bXPress
		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		AND NOT IS_SHOT_A_GIMME(thisCourse, thisFoursome)
		AND DOES_ENTITY_HAVE_PHYSICS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		
			SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_SKIP_AI_PRESSED)
			
			//set physical properties
			FREEZE_GOLF_BALL(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), FALSE)
			SET_ENTITY_RECORDS_COLLISIONS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), TRUE)
			SET_OBJECT_PHYSICS_PARAMS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), -1.0, -1.0, <<0.0, 0.0, 0.01>>, <<-1.0, -1.0, -1.0>>, -1.0, -1.0)
			
			thisFoursome.swingMeter.meterState = SWING_METER_INACTIVE
			thisFoursome.swingMeter.fCurrentPowerRel = GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome)
			
			INITIALIZE_FLOATING_HELP_FOR_FLIGHT(thisFoursome, thisHelpers)
			
			RESTART_TIMER_AT(thisHelpers.cameraTimer, 1.0)
			RESTART_TIMER_NOW(thisFoursome.physicsTimeout)
			
			SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_PERMANENT)
			SET_GOLF_UI_FLAG(thisHelpers, GUC_USE_FILL_QUALITY)
			CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_FLIGHT)
			CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_AT_REST)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_BALL_IN_FLIGHT)
			
			IF DOES_CAM_EXIST(thisHelpers.camFlight)
				DESTROY_CAM(thisHelpers.camFlight)
			ENDIF
			
			VECTOR vShotEstimate = GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome)
			VECTOR vBallPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
			VECTOR vHitDirection = NORMALISE_VECTOR(<<vShotEstimate.x - vBallPos.x, vShotEstimate.y - vBallPos.y, 0.0 >> )
			vShotEstimate -= vHitDirection //skiped shot goes a little to far
			
			IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
				vShotEstimate = GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
				IF NOT IS_GOLF_CURRENT_PLAYER_HARD_AI(thisFoursome)
				AND VDIST2(vShotEstimate, GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)) > (2.0*2.0)
					vShotEstimate.x += GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0)
					vShotEstimate.y += GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0)
				ENDIF
			ELSE
				vShotEstimate.x += GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0)
				vShotEstimate.y += GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0)
			ENDIF
			
			IF NOT GOLF_FIND_GROUND_Z(vShotEstimate, 1000.0, 5.0)
				IF NOT GOLF_FIND_GROUND_Z(vShotEstimate, 1000.0, 10.0)
					GOLF_FIND_GROUND_Z(vShotEstimate, 1000.0, 50.0)
				ENDIF
			ENDIF
			
			VECTOR vPinPos = GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
			
			//Players shot is near hole
			IF VDIST2(vShotEstimate, vPinPos) < (GIMMIE_FORCE_RADIUS*GIMMIE_FORCE_RADIUS)
				SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), vPinPos + <<0.0, 0.0, 0.05>>, FALSE)
				APPLY_FORCE_TO_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), APPLY_TYPE_IMPULSE, <<0.001, 0.001, 0>>, <<0,0,0>>, 0, FALSE, FALSE, TRUE)
				thisHelpers.vLastSafeBallPosition = vPinPos + <<0.0, 0.0, 0.05>> //if it falls through world go back to this spot
			// If the AI is putting, don't modify where we start ball position, and provide a small amount of impulse
			ELIF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
				SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), vShotEstimate + <<0.0, 0.0, 0.05>>, FALSE)
				APPLY_FORCE_TO_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), APPLY_TYPE_IMPULSE, vHitDirection, <<0,0,0>>, 0, FALSE, FALSE, TRUE)		
			ELSE // If swinging, offset position before the landing position and increase height. Increase force so that a significant forward force is generated
				// TODO: Speak with Evan to determine if a more robust solution is needed that takes into account shot magnitude
				// to determine an appropriate level of force and lerp alpha. Skipped a significant number of shots and it looked pretty good.
				SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), LERP_VECTOR(vBallPos, vShotEstimate, 0.95) + <<0.0, 0.0, 0.5>>, FALSE)
				APPLY_FORCE_TO_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), APPLY_TYPE_IMPULSE, <<5*vHitDirection.x, 5*vHitDirection.y, vHitDirection.z >>, <<0,0,0>>, 0, FALSE, FALSE, TRUE)		
			ENDIF
			
			STRING clubAnimation = GET_SWING_ANIM_CLUB(thisGame.golfBag, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
			TEXT_LABEL_31 animSwingAction = clubAnimation
			
			IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
				GOLF_MANAGE_FLIGHT_CAMERA(thisGame, thisFoursome, thisCourse, thisHelpers,thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])
				animSwingAction += "Swing_Action"
			ELSE
				animSwingAction += "Action"
				GOLF_MANAGE_GREEN_CAMERA(thisGame, thisHelpers, thisCourse, thisFoursome)
			ENDIF
			
			IF NOT IS_PED_INJURED(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
				CDEBUG1LN(DEBUG_GOLF,"Skip pressed, use end of swing animation.")
				PLAY_BLENDED_ANIMATION(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), animSwingAction, animSwingAction, FALSE, 0.0, 1.0, AF_HOLD_LAST_FRAME, 0.99)
				
				CDEBUG1LN(DEBUG_GOLF,"Golf Force Ped Update AI Skip")
				FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), TRUE)
			ENDIF
			
		
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Manages input during the scorecard at the end of holes/game
/// PARAMS:
///    thisGameState - 
///    thisPlayer - 
PROC GOLF_MANAGE_SCORECARD_INPUT(GOLF_GAME_STATE &thisGameState, GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers, BOOL &bDoRematch)
	BOOL bXPress = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN) AND NOT IS_PAUSE_MENU_ACTIVE()
	BOOL bOPress = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT) AND NOT IS_PAUSE_MENU_ACTIVE()
	
	// Fix for B* 2309115
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	#IF NOT IS_JAPANESE_BUILD
		UNUSED_PARAMETER(bOPress)
	#ENDIF
	
	BOOL bSquarePress = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT) AND NOT IS_PAUSE_MENU_ACTIVE()
	BOOL bRematchButtonPressed = FALSE
	BOOL bEndOfGame = FALSE
	
	IF thisGameState = GGS_SCORECARD_END_GAME
		bEndOfGame = TRUE
	ENDIF
	
	IF NOT GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers)
		IF NOT bXPress
			SET_GOLF_HELPER_INPUT_CLEARED(thisHelpers, TRUE)
		ENDIF
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	
	IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_MP_START_QUIT_MENU)
		EXIT
	ENDIF
	
	IF bSquarePress AND thisGameState = GGS_SCORECARD_END_GAME 
		IF NOT IS_GOLF_FOURSOME_MP() //rematch flags set elsewhere in mp
			bRematchButtonPressed = TRUE
			bDoRematch = TRUE
		ENDIF
	ENDIF
	
	//X and O are swapped for Japanese, so this manual check must test both cases
	BOOL bContinuePressed = bXPress
	#IF IS_JAPANESE_BUILD IF IS_PLAYSTATION_PLATFORM() bContinuePressed = bOPress ENDIF #ENDIF
	
	IF ((bContinuePressed OR bRematchButtonPressed) AND GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers))
	AND NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SOCIAL_CLUB_BOARD_DISPLAYED)
	AND NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
		CDEBUG1LN(DEBUG_GOLF, "Player pressed confirm or time ran out at end scoreboard")
		SET_GOLF_CONTROL_FLAG(thisGame, GCF_TURN_OFF_SCOREBOARD_MP_OVERRIDE)
		IF NOT IS_GOLF_FOURSOME_MP()
			SET_GOLF_UI_FLAG(thisHelpers, GUC_END_GOLF_SCOREBOARD)
		ELSE
			GOLF_SETUP_SCOREBOARD_KEYS(thisHelpers, FALSE, !bEndOfGame, FALSE, FALSE, bEndOfGame)
		ENDIF
		
		PLAY_SOUND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
	
	IF IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_END_GOLF_SCOREBOARD)
		CDEBUG1LN(DEBUG_GOLF,"End golf scoreboard flag is set")
		CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_END_GOLF_SCOREBOARD)
		thisGameState = GGS_NAVIGATE_TO_SHOT
		SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_NAVIGATE)
		//thisPlayers[0].vShotEstimate = <<0,0,0>>
	ENDIF
ENDPROC

FUNC BOOL SHOULD_GOLF_SC_LEADERBOARD_DSIPLAY(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	
	//IF NETWORK_TEXT_CHAT_IS_TYPING()
	//	RETURN FALSE
	//ENDIF
	
	RETURN  (GET_GOLF_FOURSOME_STATE(thisFoursome) =  GGS_SCORECARD_END_GAME)
			AND NOT IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_DONT_ALLOW_SOCIAL_CLUB)
			AND (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB) OR IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SOCIAL_CLUB_BOARD_DISPLAYED))
ENDFUNC

#IF NOT GOLF_IS_AI_ONLY
PROC MANAGE_GOLF_SC_LEADERBOARD_INPUT(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	BOOL bShowProfile = SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(golfLB_control)
	
	//CLEANUP_PC_GOLF_CONTROLS() // Shutdown PC scripted control scheme so it doesn't interfere with the leaderboard controls.
	
	SET_GOLF_UI_FLAG(thisHelpers, GUC_SOCIAL_CLUB_BOARD_DISPLAYED)
		
	IF NOT IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SOCIAL_CLUB_UI_DISPLAYED)	
		GOLF_SETUP_SOCIAL_CLUB_KEYS(thisHelpers, GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_GAME, bShowProfile)
		SET_GOLF_UI_FLAG(thisHelpers, GUC_SOCIAL_CLUB_UI_DISPLAYED)
	ENDIF
	
	IF bShowProfile != IS_BITMASK_AS_ENUM_SET(thisHelpers.golfKeysInUse, GOLF_KEY_PROFILE) //profile button state doesn't match
		GOLF_SETUP_SOCIAL_CLUB_KEYS(thisHelpers, GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_GAME, bShowProfile)
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		CLEAR_GOLF_UI_FLAG(thisHelpers, GUC_SOCIAL_CLUB_BOARD_DISPLAYED)
		GOLF_RESET_SOCIAL_CLUB_KEYS(thisHelpers)
	ENDIF
ENDPROC
#ENDIF

//PURPOSE: determines if quit button was pressed
FUNC BOOL GOLF_MANAGE_QUIT_INPUT(GOLF_FOURSOME &thisFoursome, GOLF_MINIGAME_STATE golfState, GOLF_HELPERS &thisHelpers )
	
	// Needed to prevent pause menu activating when quitting.
	IF GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_SCORECARD_END_GAME
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
	
	// Fix for B* 2309115
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		RETURN FALSE
	ENDIF
	
	IF ((IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)) AND golfState = GMS_PLAY_GOLF)
	OR ((IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)) AND golfState < GMS_PLAY_GOLF)
	OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD) AND IS_GOLF_UI_FLAG_SET(thisHelpers, GUC_SOCIAL_CLUB_BOARD_DISPLAYED)) // Fixes control issue on Leaderboard screen on PC mouse and keyboard
	
		CDEBUG1LN(DEBUG_GOLF,"Pressed quit button")
		IF NOT (golfState = GMS_CLEANUP) AND NOT (golfState = GMS_EXIT_OK)  AND golfState > GMS_INIT_STREAMING_DONE 
		AND GET_GOLF_FOURSOME_STATE(thisFoursome) != GGS_SCORECARD_END_GAME
		AND (GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) < GPS_SWING_METER  OR GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) > GPS_BALL_IN_HOLE_CELEBRATION)
		AND NOT IS_GOLF_COORD_OUTSIDE_BOUNDARY_PLANES(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		AND IS_SCREEN_FADED_IN()
			CDEBUG1LN(DEBUG_GOLF,"Go to quit")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GOLF_MANAGE_ADDRESS_SPECTATOR_INPUT(GOLF_HELPERS &thisHelpers)

	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_SELECT)
	AND NOT IS_PAUSE_MENU_ACTIVE()
		CDEBUG1LN(DEBUG_GOLF, "Pressed switched camera button")
	
		PLAY_SOUND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		thisHelpers.spectatorCamState = INT_TO_ENUM(SPECTATOR_CAMERA_STATE, ENUM_TO_INT(thisHelpers.spectatorCamState) + 1)
		thisHelpers.bJustSwitchedSpectatorCam = TRUE
		
		//is pov cam disabled?
		IF thisHelpers.spectatorCamState = GOLF_SPECTATOR_POV_CAM AND NOT thisHelpers.bAllowPlayerViewCam
			//move to next cam
			thisHelpers.spectatorCamState = INT_TO_ENUM(SPECTATOR_CAMERA_STATE, ENUM_TO_INT(thisHelpers.spectatorCamState) + 1)
		ENDIF
		
		IF ENUM_TO_INT(thisHelpers.spectatorCamState) >= COUNT_OF(SPECTATOR_CAMERA_STATE)
			thisHelpers.spectatorCamState = GOLF_SPECTATOR_ADDRESS_CAM
		ENDIF
	ENDIF

ENDPROC
