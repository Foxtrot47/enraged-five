USING "golf.sch"

/*

A_M_Y_GOLFER_01_WHITE_FULL_01 - Todd Rosenweig - Buddy car broken down
A_M_M_GOLFER_BLACK_FULL_01 - Eddie Douglas      - Buddy 2
A_M_M_GOLFER_01_WHITE_MINI_01 - Michael Ingram  - Buddy 3
A_M_M_GOLFER_01_WHITE_FULL_01 - Garry Mather    - Buddy 4
A_M_Y_GOLFER_01_WHITE_MINI_01 - Jeff Miller    - Buddy 5

A_F_Y_GOLFER_01_WHITE - Melanie Lomax          - Buddy car theif
A_F_Y_GOLFER_01_WHITE_FULL_01 - Anna Victor    - Budy 7
A_F_Y_GOLFER_01_WHITE_MINI_01 - Kayley Fisher  - Buddy 8

//*/

FUNC BOOL GOLF_DID_CURRENT_PLAYER_UNLOCK_DOMESTIC()

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		RETURN IS_BITMASK_ENUM_AS_ENUM_SET(g_savedGlobals.sGolfData.golfSavedFlags, GSAVE_MICHAEL_UNLOCKED_DOMESTIC)
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		RETURN IS_BITMASK_ENUM_AS_ENUM_SET(g_savedGlobals.sGolfData.golfSavedFlags, GSAVE_FRANKLIN_UNLOCKED_DOMESTIC)
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		RETURN IS_BITMASK_ENUM_AS_ENUM_SET(g_savedGlobals.sGolfData.golfSavedFlags, GSAVE_TREVOR_UNLOCKED_DOMESTIC)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SELECT_GOLF_BUDDY(GOLF_HELPERS &thisHelpers, GOLF_BUDDIES selectedBuddy)
	IF IS_GOLF_BUDDY_UNLOCKED(selectedBuddy)
		thisHelpers.currentBuddies = thisHelpers.currentBuddies | selectedBuddy
	ENDIF
ENDPROC

PROC CLEAR_GOLF_BUDDY(GOLF_HELPERS &thisHelpers, GOLF_BUDDIES selectedBuddy)
	thisHelpers.currentBuddies  -= (thisHelpers.currentBuddies & selectedBuddy) 
ENDPROC

FUNC BOOL IS_GOLF_BUDDY_SELECTED(GOLF_HELPERS &thisHelpers, GOLF_BUDDIES golfBuddy)
	RETURN (thisHelpers.currentBuddies & golfBuddy) !=  GB_NONE
ENDFUNC

PROC FLIP_GOLF_BUDDY(GOLF_HELPERS &thisHelpers, GOLF_BUDDIES selectedBuddy)
	IF IS_GOLF_BUDDY_UNLOCKED(g_savedGlobals.sGolfData, selectedBuddy)
		thisHelpers.currentBuddies = thisHelpers.currentBuddies ^ selectedBuddy
	ENDIF
ENDPROC

//Get an unlocked buddie, index is the bit you want to get amongst the set bits
//For example with an unlocked flag l101 and an index of 3, the function will return 
//the buddy corisponding to the last bit because it is the 3rd set bit 
FUNC GOLF_BUDDIES GET_GOLF_INDEXED_SELECTED_BUDDY(GOLF_HELPERS &thisHelpers, INT index)

	GOLF_BUDDIES golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, BIT0)
	
	index--
	
	WHILE golfBuddy < GB_MAX_BUDDIES
		
		IF IS_GOLF_BUDDY_SELECTED(thisHelpers, golfBuddy)
			IF index = 0
				RETURN golfBuddy
			ENDIF
			index--
		ENDIF
		
		golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, SHIFT_LEFT(ENUM_TO_INT(golfBuddy), 1))
	ENDWHILE
	
	RETURN GB_NONE
ENDFUNC

FUNC MODEL_NAMES GET_GOLF_BUDDY_MODEL_NAME(GOLF_BUDDIES selectedBuddy, PED_TYPE &pedType)
	
	IF selectedBuddy = GB_NONE
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDIF

	IF selectedBuddy = GB_MALE1
		pedType = PEDTYPE_CIVMALE
		RETURN A_M_Y_GOLFER_01
	ELIF selectedBuddy = GB_DOMESTIC
		pedType = PEDTYPE_CIVMALE
		RETURN A_M_Y_GOLFER_01
	ELIF selectedBuddy = GB_MALE3
		pedType = PEDTYPE_CIVMALE
		RETURN A_M_M_GOLFER_01
	ELIF selectedBuddy = GB_MALE4
		pedType = PEDTYPE_CIVMALE
		RETURN A_M_M_GOLFER_01	
	ELIF selectedBuddy = GB_MALE5
		pedType = PEDTYPE_CIVMALE
		RETURN A_M_Y_GOLFER_01
//	ELIF selectedBuddy = GB_CAR_THEFT_VICTIM
//		pedType = PEDTYPE_CIVFEMALE
//		RETURN A_F_Y_GOLFER_01
//	ELIF selectedBuddy = GB_FEMALE2
//		pedType = PEDTYPE_CIVFEMALE
//		RETURN A_F_Y_GOLFER_01
//	ELIF selectedBuddy = GB_FEMALE3
//		pedType = PEDTYPE_CIVFEMALE
//		RETURN A_F_Y_GOLFER_01
//	ELIF selectedBuddy = GB_FEMALE4
//		pedType = PEDTYPE_CIVFEMALE
//		RETURN A_F_Y_GOLFER_01
//	ELIF selectedBuddy = GB_FEMALE5
//		pedType = PEDTYPE_CIVFEMALE
//		RETURN A_F_Y_GOLFER_01
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC SET_GOLF_BUDDY_VOICE_NAME(GOLF_BUDDIES selectedBuddy, PED_INDEX pedIndex)

	IF selectedBuddy = GB_DOMESTIC
		SET_AMBIENT_VOICE_NAME(pedIndex, "REDOCastro")
	ENDIF

ENDPROC

PROC SET_GOLF_BUDDY_VARIATION(GOLF_BUDDIES selectedBuddy, PED_INDEX pedIndex)

	INT headDrawable, headTexture, torsoDrawable, torsoTexture, handDrawable, handTexture
	INT hairDrawable, hairTexture, legDrawable,   legTexture, specialDrawable, specialTexture
	
	IF selectedBuddy = GB_MALE1
		headDrawable = 1 headTexture = 2 torsoDrawable = 1 torsoTexture = 1 handDrawable    = 1 handTexture    = 0
		hairDrawable = 1 hairTexture = 2 legDrawable   = 0 legTexture   = 2 specialDrawable = 0 specialTexture = 0
	ELIF selectedBuddy = GB_DOMESTIC
		headDrawable = 0 headTexture = 0 torsoDrawable = 1 torsoTexture = 2 handDrawable    = 1 handTexture    = 0
		hairDrawable = 1 hairTexture = 0 legDrawable   = 1 legTexture   = 1 specialDrawable = 0 specialTexture = 1
	ELIF selectedBuddy = GB_MALE3
		headDrawable = 0 headTexture = 1 torsoDrawable = 0 torsoTexture = 2 handDrawable    = -1 handTexture    = -1
		hairDrawable = 1 hairTexture = 0 legDrawable   = 1 legTexture   = 0 specialDrawable = -1 specialTexture = -1
	ELIF selectedBuddy = GB_MALE4
		headDrawable = 0 headTexture = 0 torsoDrawable = 0 torsoTexture = 1 handDrawable    = -1 handTexture    = -1
		hairDrawable = 0 hairTexture = 1 legDrawable   = 1 legTexture   = 1 specialDrawable = -1 specialTexture = -1
	ELIF selectedBuddy = GB_MALE5
		headDrawable = 0 headTexture = 1 torsoDrawable = 1 torsoTexture = 0 handDrawable    = -1 handTexture    = -1
		hairDrawable = 0 hairTexture = 0 legDrawable   = 1 legTexture   = 2 specialDrawable = -1 specialTexture = -1
//	ELIF selectedBuddy = GB_CAR_THEFT_VICTIM
//		headDrawable = 0 headTexture = 0 torsoDrawable = 1 torsoTexture = 2  
//		hairDrawable = 1 hairTexture = 0 legDrawable   = 1 legTexture   = 2 specialDrawable = -1 specialTexture = -1
//	ELIF selectedBuddy = GB_FEMALE2
//		headDrawable = 0 headTexture = 0 torsoDrawable = 1 torsoTexture = 2  
//		hairDrawable = 0 hairTexture = 1 legDrawable   = 1 legTexture   = 2 specialDrawable = -1 specialTexture = -1
//	ELIF selectedBuddy = GB_FEMALE3
//		headDrawable = 0 headTexture = 0 torsoDrawable = 0 torsoTexture = 1 
//		hairDrawable = 1 hairTexture = 0 legDrawable   = 1 legTexture   = 1 specialDrawable = -1 specialTexture = -1
//	ELIF selectedBuddy = GB_FEMALE4
//		headDrawable = 0 headTexture = 0 torsoDrawable = 0 torsoTexture = 0 legDrawable = 0 legTexture = 2 hairDrawable = 1 hairTexture = 1
//	ELIF selectedBuddy = GB_FEMALE5
//		headDrawable = 0 headTexture = 0 torsoDrawable = 1 torsoTexture = 0 legDrawable = 1 legTexture = 0 hairDrawable = 0 hairTexture = 0
	ENDIF

	SET_PED_COMPONENT_VARIATION(pedIndex,  PED_COMP_HEAD,    headDrawable, headTexture)
	SET_PED_COMPONENT_VARIATION(pedIndex,  PED_COMP_TORSO,   torsoDrawable, torsoTexture)
	SET_PED_COMPONENT_VARIATION(pedIndex,  PED_COMP_LEG,     legDrawable, legTexture)
	SET_PED_COMPONENT_VARIATION(pedIndex,  PED_COMP_HAIR,    hairDrawable, hairTexture)
	
	IF specialDrawable > -1 AND specialTexture > -1
		SET_PED_COMPONENT_VARIATION(pedIndex,  PED_COMP_HAND, handDrawable, handTexture)
		SET_PED_COMPONENT_VARIATION(pedIndex,  PED_COMP_SPECIAL, specialDrawable, specialTexture)
	ENDIF

ENDPROC

FUNC STRING GET_GOLF_BUDDY_STRING_TABLE_NAME(GOLF_BUDDIES selectedBuddy)

	IF selectedBuddy = GB_MALE1 				RETURN "BUDDY_0"
	ELIF selectedBuddy = GB_DOMESTIC         	RETURN "BUDDY_1"
	ELIF selectedBuddy = GB_MALE3         		RETURN "BUDDY_2"
	ELIF selectedBuddy = GB_MALE4		 		RETURN "BUDDY_3"
	ELIF selectedBuddy = GB_MALE5		 		RETURN "BUDDY_4"
//	ELIF selectedBuddy = GB_CAR_THEFT_VICTIM	RETURN "BUDDY_5"
//	ELIF selectedBuddy = GB_FEMALE2				RETURN "BUDDY_6"
//	ELIF selectedBuddy = GB_FEMALE3				RETURN "BUDDY_7"
//	ELIF selectedBuddy = GB_FEMALE4				RETURN "BUDDY_8"
//	ELIF selectedBuddy = GB_FEMALE5				RETURN "BUDDY_9"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_GOLF_BUDDY_STRING_TABLE_FIRST_NAME(GOLF_BUDDIES selectedBuddy)

	IF selectedBuddy = GB_MALE1 				RETURN "BUDDY_FIRST_0"
	ELIF selectedBuddy = GB_DOMESTIC         	RETURN "BUDDY_FIRST_1"
	ELIF selectedBuddy = GB_MALE3         		RETURN "BUDDY_FIRST_2"
	ELIF selectedBuddy = GB_MALE4		 		RETURN "BUDDY_FIRST_3"
	ELIF selectedBuddy = GB_MALE5		 		RETURN "BUDDY_FIRST_4"
//	ELIF selectedBuddy = GB_CAR_THEFT_VICTIM	RETURN "BUDDY_FIRST_5"
//	ELIF selectedBuddy = GB_FEMALE2				RETURN "BUDDY_FIRST_6"
//	ELIF selectedBuddy = GB_FEMALE3				RETURN "BUDDY_FIRST_7"
//	ELIF selectedBuddy = GB_FEMALE4				RETURN "BUDDY_8"
//	ELIF selectedBuddy = GB_FEMALE5				RETURN "BUDDY_9"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC GOLF_AI_DIFFICULTY GET_GOLF_BUDDY_DIFFICULTY(GOLF_BUDDIES selectedBuddy)
	IF selectedBuddy = GB_MALE1					RETURN GOLF_AI_EASY
	ELIF selectedBuddy = GB_DOMESTIC			RETURN GOLF_AI_HARD
	ELIF selectedBuddy = GB_MALE3				RETURN GOLF_AI_NORMAL
	ELIF selectedBuddy = GB_MALE4				RETURN GOLF_AI_NORMAL
	ELIF selectedBuddy = GB_MALE5				RETURN GOLF_AI_HARD
//	ELIF selectedBuddy = GB_CAR_THEFT_VICTIM	RETURN GOLF_AI_EASY
//	ELIF selectedBuddy = GB_FEMALE2				RETURN GOLF_AI_NORMAL
//	ELIF selectedBuddy = GB_FEMALE3				RETURN GOLF_AI_HARD
//	ELIF selectedBuddy = GB_FEMALE4				RETURN GOLF_AI_NORMAL
//	ELIF selectedBuddy = GB_FEMALE5				RETURN GOLF_AI_HARD
	ENDIF
	
	RETURN GOLF_AI_EASY
ENDFUNC

FUNC STRING GET_GOLF_BUDDY_STRING_TABLE_DIFFICULTY(GOLF_BUDDIES selectedBuddy)
	
	IF GET_GOLF_BUDDY_DIFFICULTY(selectedBuddy) = GOLF_AI_EASY
		RETURN "GOLF_EASY"
	ELIF GET_GOLF_BUDDY_DIFFICULTY(selectedBuddy) = GOLF_AI_NORMAL
		RETURN "GOLF_MED"
	ELIF GET_GOLF_BUDDY_DIFFICULTY(selectedBuddy) = GOLF_AI_HARD
		RETURN "GOLF_HARD"
	ENDIF
	
	RETURN "TENNIS_EASY"
ENDFUNC

FUNC INT GET_GOLF_NUMBER_OF_BUDDIES_SELECTED(GOLF_HELPERS &thisHelpers)
	GOLF_BUDDIES golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, BIT0)
	INT total = 0
	
	WHILE golfBuddy < GB_MAX_BUDDIES
		IF IS_GOLF_BUDDY_SELECTED(thisHelpers, golfBuddy)
			total++
		ENDIF
			golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, SHIFT_LEFT(ENUM_TO_INT(golfBuddy), 1))
	ENDWHILE

	RETURN total
ENDFUNC

FUNC GOLF_BUDDIES REMOVE_RESTRICTED_BUDDIES(GOLF_BUDDIES golfBuddies)

	GOLF_BUDDIES toRemove = GB_NONE
	
	RETURN golfBuddies - (golfBuddies&toRemove)

ENDFUNC
//Sets up the menu to select the buddies from at the start of the minigame
PROC SET_GOLF_INTRO_SELECT_BUDDIES(GOLF_HELPERS &thisHelpers)

	//if we ever decided to limit the times certain buddies will be available
	GOLF_BUDDIES availableBuddies = REMOVE_RESTRICTED_BUDDIES(g_savedGlobals.sGolfData.unlockedBuddies)
	
	GOLF_BUDDIES golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, BIT0)
	INT slot = 0
	
	WHILE golfBuddy < GB_MAX_BUDDIES
		
		//if golfBuddy is set in availableBuddies
		IF (availableBuddies & golfBuddy) !=  GB_NONE
			STRING tableName = GET_GOLF_BUDDY_STRING_TABLE_NAME(golfBuddy)
			STRING tableDifficulty = GET_GOLF_BUDDY_STRING_TABLE_DIFFICULTY(golfBuddy)
			IF NOT IS_STRING_EMPTY(tableName)
				ADD_MENU_ITEM_TEXT(slot, tableName)
				ADD_MENU_ITEM_TEXT(slot, tableDifficulty)
				IF IS_GOLF_BUDDY_SELECTED(thisHelpers, golfBuddy)
					ADD_MENU_ITEM_ICON(slot, MENU_ICON_BOX_TICK)
				ELSE
					ADD_MENU_ITEM_ICON(slot, MENU_ICON_BOX_EMPTY)
				ENDIF
				slot++
			ENDIF
		ENDIF
		//shift flag to next buddy
		golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, SHIFT_LEFT(ENUM_TO_INT(golfBuddy), 1))
	ENDWHILE
	
	ADD_MENU_ITEM_TEXT(slot, "FLY_TEE_OFF") //start game slot is last
	SET_MENU_ROW_TEXT_COLOUR(slot, HUD_COLOUR_BLACK, HUD_COLOUR_GOLF, TRUE)
ENDPROC

PROC UNLOCK_GOLF_BUDDY_AFTER_GOLF_WIN(GOLF_HELPERS &thisHelpers)

	UNLOCK_GOLF_BUDDY(g_savedGlobals.sGolfData, GB_MALE1)
	IF IS_GOLF_BUDDY_SELECTED(thisHelpers, GB_MALE1)
		UNLOCK_GOLF_BUDDY(g_savedGlobals.sGolfData, GB_MALE3)
	ENDIF
	
	IF IS_GOLF_BUDDY_SELECTED(thisHelpers, GB_MALE3)
		UNLOCK_GOLF_BUDDY(g_savedGlobals.sGolfData, GB_MALE4)
	ENDIF
	
	IF IS_GOLF_BUDDY_SELECTED(thisHelpers, GB_MALE4)
		UNLOCK_GOLF_BUDDY(g_savedGlobals.sGolfData, GB_MALE5)
	ENDIF

ENDPROC

PROC SETUP_GOLF_PED_ANCILLARY_DATA(PED_INDEX pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex) AND NOT IS_ENTITY_DEAD(pedIndex)
//		SET_PED_CAN_BE_TARGETTED(pedIndex, FALSE)
		SET_ENTITY_LOD_DIST(pedIndex, 500)
		SET_PED_DIES_IN_WATER(pedIndex, FALSE)
		SET_PED_WEAPON_MOVEMENT_CLIPSET(pedIndex, "move_m@golfer@")
		SET_PED_CONFIG_FLAG(pedIndex, PCF_IgnoreLegIkRestrictions, TRUE)
	ENDIF
ENDPROC

//From the selected buddies, create and intialize the foursomes
PROC CREATE_GOLF_BUDDIES_PEDS(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse)
	
	PED_TYPE pedType = PEDTYPE_PLAYER_UNUSED
	
	GOLF_BUDDIES golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, BIT0)
	INT iCurrentPlayer = 1
	
	WHILE golfBuddy < GB_MAX_BUDDIES
		IF IS_GOLF_BUDDY_SELECTED(thisHelpers,  golfBuddy)
			MODEL_NAMES pedModel = GET_GOLF_BUDDY_MODEL_NAME(INT_TO_ENUM(GOLF_BUDDIES, golfBuddy), pedType)
			
			IF pedModel != DUMMY_MODEL_FOR_SCRIPT AND NOT DOES_ENTITY_EXIST(GET_GOLF_PLAYER_PED(thisFoursome.playerCore[iCurrentPlayer]))
				SET_GOLF_PLAYER_PED(thisFoursome.playerCore[iCurrentPlayer], CREATE_PED(pedType, pedModel, GET_GOLF_COURSE_START_POSITION(thisCourse, iCurrentPlayer)))
				SET_ENTITY_HEADING(GET_GOLF_PLAYER_PED(thisFoursome.playerCore[iCurrentPlayer]), GET_HEADING_BETWEEN_VECTORS(GET_GOLF_COURSE_START_POSITION(thisCourse,iCurrentPlayer), GET_GOLF_COURSE_CART_POSITION(thisCourse, 0)))
				//SET_PED_CAN_RAGDOLL(GET_GOLF_PLAYER_PED(thisFoursome.playerCore[iCurrentPlayer]), FALSE)
				SET_GOLF_BUDDY_VARIATION(INT_TO_ENUM(GOLF_BUDDIES, golfBuddy), GET_GOLF_PLAYER_PED(thisFoursome.playerCore[iCurrentPlayer]))
				SET_GOLF_BUDDY_VOICE_NAME(INT_TO_ENUM(GOLF_BUDDIES, golfBuddy), GET_GOLF_PLAYER_PED(thisFoursome.playerCore[iCurrentPlayer]))
				SETUP_GOLF_PED_ANCILLARY_DATA(GET_GOLF_PLAYER_PED(thisFoursome.playerCore[iCurrentPlayer]))
				SET_GOLF_PLAYER_AI_DIFFICULTY(thisFoursome.playerCore[iCurrentPlayer], GET_GOLF_BUDDY_DIFFICULTY(INT_TO_ENUM(GOLF_BUDDIES, golfBuddy)))
				
				IF golfBuddy = GB_DOMESTIC
					thisHelpers.iCastroIndex = iCurrentPlayer
				ENDIF
				
				iCurrentPlayer++
			ENDIF
		ENDIF
		//shift flag to next buddy
		golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, SHIFT_LEFT(ENUM_TO_INT(golfBuddy), 1))
	ENDWHILE

ENDPROC
