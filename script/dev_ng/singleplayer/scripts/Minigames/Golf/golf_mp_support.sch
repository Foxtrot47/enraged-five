USING "golf_mp.sch"
USING "golf_core.sch"
USING "net_mission.sch"
USING "fmmc_mp_setup_mission.sch"

/// PURPOSE:
///    Do necessary pre game start ini.
/// RETURNS:
///    FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA fmmcMissionData, GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[MP_GOLF_MAX_PLAYERS] )
	
	//If we are in freemode launch diffrently
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	
		//Freemode mission start details
		INT iPlayerMissionToLoad = FMMC_MINI_GAME_CREATOR_ID
		INT iMissionVariation 	 = 0
		
		//Call the function that controls the setting up of this script
		FMMC_PROCESS_PRE_GAME_COMMON(fmmcMissionData, iPlayerMissionToLoad, iMissionVariation, MP_GOLF_MAX_PLAYERS)
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_GOLF
		
		//Script has started set the global broadcast data up so every body knows
		SET_FM_MISSION_LAUNCHED_SUCESS(fmmcMissionData.mdID.idCreator, fmmcMissionData.mdID.idVariation, fmmcMissionData.iInstanceId)
	
	//If we are not in freemode deal with it normally
	ELSE
		// This marks the script as a net script, and handles any instancing setup. 
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(MP_GOLF_MAX_PLAYERS, FALSE)
		
		// This makes sure the net script is active, waits untull it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
	ENDIF
	
	//Common
	RESERVE_NETWORK_MISSION_OBJECTS(10)
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC



/// PURPOSE:
///    Process server only gameplay.
PROC PROCESS_GAME_SERVER()
//	INT iPlayerIndex
//	REPEAT COUNT_OF(playerBD) iPlayerIndex
//	ENDREPEAT
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("PROCESS_GAME_SERVER")
ENDPROC

//Checks if all players are waiting to finish the mission.
FUNC BOOL ARE_ALL_PLAYERS_ALIVE(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[])
	
	INT iPlayerIndex
	
	REPEAT COUNT_OF(thisFoursome.playerCore) iPlayerIndex
		PARTICIPANT_INDEX piIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX(thisPlayers, iPlayerIndex)
	
		IF NATIVE_TO_INT(piIndex) > -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(piIndex)
				IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED( thisFoursome, iPlayerIndex))
					IF IS_ENTITY_DEAD(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED( thisFoursome, iPlayerIndex))
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
	
ENDFUNC

FUNC INT GET_GOLF_MISSING_PLAYER(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[])

	INT iPlayerIndex
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		PARTICIPANT_INDEX piIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX(thisPlayers, iPlayerIndex)
		
		IF NATIVE_TO_INT(piIndex) > -1
			IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piIndex)
				IF IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
//					CDEBUG1LN(DEBUG_GOLF,"Player ", iPlayerIndex, " is disconnected but has already been removed from golf game")
				ELSE
					CDEBUG1LN(DEBUG_GOLF,"Not active, ", iPlayerIndex, "  is the one who disconnected")
					RETURN iGolfPlayingParticipantID[iPlayerIndex] //get the participant id of the quiting player
				ENDIF
			ELSE
//				CDEBUG1LN(DEBUG_GOLF,"Player is still in game")
			ENDIF
		ELSE
			IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
				CDEBUG1LN(DEBUG_GOLF, "Player ", iPlayerIndex, " was net index variable was removed")
				RETURN iGolfPlayingParticipantID[iPlayerIndex]
			ENDIF
		ENDIF
	ENDREPEAT
	
//	CDEBUG1LN(DEBUG_GOLF,"No one left?")
	RETURN -1
ENDFUNC

FUNC BOOL ARE_ALL_INACTIVE_GOLFERS_REMOVED(GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[])

	INT iPlayerIndex
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		PARTICIPANT_INDEX piIndex = GET_GOLF_PLAYER_NET_INDEX_BY_INDEX(thisPlayers, iPlayerIndex)
		
		IF NATIVE_TO_INT(piIndex) > -1
			IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piIndex)
				IF IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
					CDEBUG1LN(DEBUG_GOLF,"Player ", iPlayerIndex, " has already been removed from golf game")
				ELSE
					CDEBUG1LN(DEBUG_GOLF,iPlayerIndex, "  is disconnected and has not been removed from golf game")
					RETURN FALSE
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_GOLF,"Player is still in game")
			ENDIF
		ENDIF
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_GOLF,"All inactive players have been removed")
	RETURN TRUE
ENDFUNC

// Server only function that checks to see if the game mode should now end. For example if someone disconnects
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET(GOLF_FOURSOME &thisFoursome)
	
	//CDEBUG1LN(DEBUG_GOLF,"Number of active participants ", NETWORK_GET_NUM_PARTICIPANTS())
	//CDEBUG1LN(DEBUG_GOLF,"Number of players in foursome ", GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome))
	IF NETWORK_GET_NUM_PARTICIPANTS() < GET_GOLF_FOURSOME_NUMBER_OF_ACTIVE_PLAYERS(thisFoursome)
	
		CDEBUG1LN(DEBUG_GOLF,"someone disconnected. Golf Foursome num players ", GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome), 
				" players removed ", GET_GOLF_NUMBER_OF_PLAYERS_REMOVED(thisFoursome), " num partcipants ", NETWORK_GET_NUM_PARTICIPANTS() )
		RETURN TRUE
	ENDIF
	
	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GOLF_NETWORK_PARTICIPANT_SPECTATOR(INT piPlayer)
	
	IF piPlayer > -1
		RETURN DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(piPlayer)))
			OR  IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(piPlayer)))
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_FIRST_ACTIVE_PLAYER()
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			//no spectators
			IF NOT IS_GOLF_NETWORK_PARTICIPANT_SPECTATOR(iParticipant)
				RETURN iParticipant
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

PROC RESET_GOLF_EFFECTS_FLAGS(GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[])
	CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_BALL_JUST_HIT)
	CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYED_HIT_PARTICLE_EFFECT_MP)
	CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYED_LAND_PARTICLE_EFFECT_MP) 
	CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYED_LAND_SOUND_EFFECT_MP) 
	CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_PLAYED_HIT_SOUND_EFFECT_MP) 
	CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_MP_PLAYED_BALL_IN_HOLE_SOUND)
	CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG(playerBD, PARTICIPANT_ID_TO_INT(), GCF_DONE_WITH_GAME)
	CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_DETACH_GOLF_CLUB)
	CLEAR_GOLF_MP_CLIENT_CONTROL_FLAG2(playerBD, PARTICIPANT_ID_TO_INT(), GCF2_SYNC_BALL_IN_WATER_SOUND)
ENDPROC

PROC CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM(BOOL bUseLeaderboardCam, GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], GOLF_HELPERS &thisHelpers, INT iPassFailStatus = ciFMMC_END_OF_MISSION_STATUS_PASSED, BOOL bClearTask = FALSE, BOOL bUseSkyCam = TRUE)
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR DID_I_JOIN_MISSION_AS_SPECTATOR()
		// 1819210 //////////////////
		IF NOT IS_ENTERED_GAME_AS_SCTV() 
		AND NOT IS_PAUSE_MENU_SELECT_TO_SCTV()
		AND NOT IS_INVITED_TO_SCTV()
		AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		/////////////////////////////
			IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_IN(500)
				PRINTLN("=== CAM === CLEANUP_GOLF_MP_WITH_LEADERBOARD_CAM, DO_SCREEN_FADE_IN")
			ENDIF
		ENDIF
		SCRIPT_GOLF_CLEANUP(thisGame, thisCourse, thisFoursome, thisPlayers,  thisHelpers, iPassFailStatus, bClearTask)
	ELSE
		//destory your own ball if you own it
		OBJECT_INDEX objBallID = GET_GOLF_FOURSOME_LOCAL_PLAYER_BALL(thisFoursome)
		IF DOES_ENTITY_EXIST(objBallID)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(objBallID)
				CLEANUP_GOLF_FOURSOME_INDEXED_GOLF_BALL(thisFoursome, GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome))
			ENDIF
		ENDIF
		
		BOOL bDoneWithSkyTransition = TRUE
		IF bUseSkyCam
			SET_SKYSWOOP_UP()
			bDoneWithSkyTransition = IS_SWITCH_TO_MULTI_FIRSTPART_FINISHED()
		ENDIF
		
		IF bDoneWithSkyTransition
			IF bUseLeaderboardCam
				REQUEST_LEADERBOARD_CAM()
				
				IF IS_LEADERBOARD_CAM_READY()
					SCRIPT_GOLF_CLEANUP_COURSE(thisCourse)
					SCRIPT_GOLF_CLEANUP(thisGame, thisCourse, thisFoursome, thisPlayers,  thisHelpers, iPassFailStatus, bClearTask)
				ENDIF
			ELSE
				SCRIPT_GOLF_CLEANUP_COURSE(thisCourse)
				SCRIPT_GOLF_CLEANUP(thisGame, thisCourse, thisFoursome, thisPlayers,  thisHelpers, iPassFailStatus, bClearTask)		
			ENDIF
		ENDIF
	ENDIF

ENDPROC
