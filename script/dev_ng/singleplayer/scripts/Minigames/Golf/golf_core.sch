USING "friendActivity_public.sch"     
USING "cutscene_public.sch"
USING "golf_data.sch"
USING "golf_helpers.sch"
USING "golf_input.sch"
USING "golf_ui.sch"
USING "golf_ai.sch"
USING "golf_cameras.sch"
USING "golf_player.sch"
#IF NOT GOLF_IS_AI_ONLY
USING "golf_foursome_lib.sch"
#ENDIF
USING "golf_clubs_lib.sch"
USING "fmmc_mp_setup_mission.sch"
#IF IS_DEBUG_BUILD
USING "golf_debug.sch"
#ENDIF

// Any presetup that needs to be done for the script
PROC SCRIPT_SETUP(GOLF_GAME &thisGame)
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	IF DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
		IF NOT IS_PED_INJURED(FRIEND_A_PED_ID()) AND NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())
			SET_GOLF_CONTROL_FLAG(thisGame, GCF_IS_FRIEND_ACTIVITY)
			startActivity(ALOC_golf_countryClub)
		ENDIF
	ELSE
		SET_MISSION_FLAG(TRUE)
	ENDIF
	DISPLAY_RADAR(FALSE)
	//DISPLAY_HUD(FALSE)
	CLEAR_HELP()
	CLEAR_PRINTS()
ENDPROC

PROC SCRIPT_GOLF_CLEANUP_COURSE(GOLF_COURSE &thisCourse)
	
	INT holeCount
	OBJECT_INDEX holeFlag
	REPEAT COUNT_OF(thisCourse.golfHole) holeCount
		holeFlag = GET_GOLF_HOLE_FLAG(thisCourse, holeCount)
		IF DOES_ENTITY_EXIST(holeFlag)
		AND IS_ENTITY_A_MISSION_ENTITY(holeFlag)
			SET_OBJECT_AS_NO_LONGER_NEEDED(holeFlag)
			DELETE_OBJECT(holeFlag)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC SCRIPT_GOLF_CLEANUP_AI_FOURSOME(GOLF_FOURSOME &thisFoursome[])
	INT foursomeCount, playerCount
	REPEAT COUNT_OF(thisFoursome) foursomeCount
		REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome[foursomeCount]) playerCount
			CLEANUP_GOLF_FOURSOME_INDEXED_GOLF_BALL(thisFoursome[foursomeCount], playerCount)
			CLEANUP_GOLF_CLUB(thisFoursome[foursomeCount].playerCore[playerCount])
			CLEANUP_GOLF_GOLFBAG(thisFoursome[foursomeCount].playerCore[playerCount])
			
			IF DOES_ENTITY_EXIST(thisFoursome[foursomeCount].playerCore[playerCount].pedGolfPlayer)
				SET_PED_AS_NO_LONGER_NEEDED(thisFoursome[foursomeCount].playerCore[playerCount].pedGolfPlayer)
			ENDIF
		ENDREPEAT
		
		IF DOES_ENTITY_EXIST(thisFoursome[foursomeCount].vehCart[0])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(thisFoursome[foursomeCount].vehCart[0])
		ENDIF
		
		IF DOES_ENTITY_EXIST(thisFoursome[foursomeCount].vehCart[1])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(thisFoursome[foursomeCount].vehCart[1])
		ENDIF
	ENDREPEAT
	
ENDPROC


PROC REMOVE_AMBIENT_GOLF_FLAGS()

	IF IS_IPL_ACTIVE("GolfFlags")
		REMOVE_IPL("GolfFlags")
	ENDIF

ENDPROC

PROC ADD_AMBIENT_GOLF_FLAGS()
	
	IF NOT IS_IPL_ACTIVE("GolfFlags")
		REQUEST_IPL("GolfFlags")
	ENDIF

ENDPROC

PROC GOLF_CLEANUP_ALL_CAMERA(GOLF_HELPERS &thisHelpers)
	IF DOES_CAM_EXIST(thisHelpers.camNavigate)
		DESTROY_CAM(thisHelpers.camNavigate)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camFlight)
		DESTROY_CAM(thisHelpers.camFlight)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camAddress)
		DESTROY_CAM(thisHelpers.camAddress)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camPreview)
		DESTROY_CAM(thisHelpers.camPreview)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camPreviewSplineForward)
		DESTROY_CAM(thisHelpers.camPreviewSplineForward)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camLookAtGreen)
		DESTROY_CAM(thisHelpers.camLookAtGreen)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camReaction)
		DESTROY_CAM(thisHelpers.camReaction)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camUpStart)
		DESTROY_CAM(thisHelpers.camUpStart)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camUpEnd)
		DESTROY_CAM(thisHelpers.camUpEnd)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camMPApproach)
		DESTROY_CAM(thisHelpers.camMPApproach)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camNavigate)
		DESTROY_CAM(thisHelpers.camNavigate)
	ENDIF
	IF DOES_CAM_EXIST(thisHelpers.camPauseAnimCamera)
		DESTROY_CAM(thisHelpers.camPauseAnimCamera)
	ENDIF

ENDPROC

// Cleanup the script on termination – must not include any WAITs
PROC SCRIPT_GOLF_CLEANUP(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], GOLF_HELPERS &thisHelpers, INT iPassFailStatus = ciFMMC_END_OF_MISSION_STATUS_PASSED, BOOL bClearTask = FALSE)
	
	INT playerCount
	
	CDEBUG1LN(DEBUG_GOLF,"Start golf cleanup")
	DEBUG_PRINTCALLSTACK()
	
	UNUSED_PARAMETER(thisPlayers)
	UNUSED_PARAMETER(thisGame)
	
	#IF NOT GOLF_IS_AI_ONLY
	GOLF_UPDATE_SOCIAL_CLUB_LEADERBOARD(TRUE)
	#ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		
		ENDIF
	ENDIF
	
	TRIGGER_MUSIC_EVENT("MGGF_STOP")
	
	CLEAR_FOCUS()
	//KILL_CHASE_HINT_CAM(thisHelpers.localChaseHintCamStruct)
	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	IF NOT IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_GOLF_TERMINATED_DISPLAYED)
		CLEAR_PRINTS()
	ENDIF
	
	#IF NOT GOLF_IS_AI_ONLY
		CLEANUP_SOCIAL_CLUB_LEADERBOARD(golfLB_control)
		ENABLE_SELECTOR()
		
		#IF NOT GOLF_IS_MP
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_LeaveEngineOnWhenExitingVehicles, FALSE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_AllowLockonToFriendlyPlayers, FALSE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_CanAttackFriendly, FALSE)
			SET_ENTITY_VISIBLE( PLAYER_PED_ID(), TRUE) // Just in case player has been hidden
		#ENDIF
	#ENDIF
	
	IF thisHelpers.soundRolling > -1
		RELEASE_SOUND_ID(thisHelpers.soundRolling)
	ENDIF
	
	CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT, TRUE)
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	TERRAINGRID_ACTIVATE(FALSE)
	ADD_AMBIENT_GOLF_FLAGS()
	SET_BIGMAP_ACTIVE(FALSE)
	DISPLAY_RADAR(TRUE)
	DISPLAY_AREA_NAME(TRUE)
	DISPLAY_HUD(TRUE)
	DISABLE_CELLPHONE(FALSE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
	RELEASE_SCRIPT_AUDIO_BANK()
	ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
	GOLF_TRAIL_SET_ENABLED(FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	DISABLE_GOLF_MINIMAP()
	CLEAN_UP_GOLF_SPLASH()
	
	IF IS_MINIGAME_IN_PROGRESS()
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	
	IF STREAMVOL_IS_VALID(thisHelpers.steamVolNextShot)
		STREAMVOL_DELETE(thisHelpers.steamVolNextShot)
	ENDIF
	
	IF STREAMVOL_IS_VALID(thisHelpers.streamVolScorecard)
		STREAMVOL_DELETE(thisHelpers.streamVolScorecard)
	ENDIF
	//REMOVE_GOLF_CLUB_WEAPON(PLAYER_PED_ID(), FALSE)
	//g_bPauseCommsQueues = FALSE
	
	REMOVE_RELATIONSHIP_GROUP(thisHelpers.golfersRelationshipHash)
	
	#IF NOT GOLF_IS_MP
		PAUSE_CLOCK(FALSE)
		
		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
			//I get an ignorable assert if i don't do a dead check, even though it doesn't matter because I can turn gravity off/on for dead peds
			IF IS_ENTITY_DEAD(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome)) OR NOT IS_ENTITY_DEAD(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome))
				SET_PED_GRAVITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome), TRUE)
			ENDIF
		ENDIF
		
		IF NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_IS_FRIEND_ACTIVITY)
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_GOLF, TRUE)
		ELSE
			IF DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
			AND NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())
				CDEBUG1LN(DEBUG_GOLF, "Cleanup: Enabling weapon switch on friend A")
				SET_PED_CAN_SWITCH_WEAPON(FRIEND_A_PED_ID(), TRUE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(FRIEND_B_PED_ID())
			AND NOT IS_ENTITY_DEAD(FRIEND_B_PED_ID())
				CDEBUG1LN(DEBUG_GOLF, "Cleanup: Enabling weapon switch on friend B")
				SET_PED_CAN_SWITCH_WEAPON(FRIEND_B_PED_ID(), TRUE)
			ENDIF
		ENDIF
	#ENDIF
	
	#IF GOLF_IS_MP
		GOLF_TOGGLE_LOADING_SPINNER(FALSE)
		
		IF thisHelpers.eFeetProp != DUMMY_PED_COMP
			SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_FEET, thisHelpers.eFeetProp, FALSE)
		ENDIF
		
		REMOVE_SCENARIO_BLOCKING_AREA(thisHelpers.golfScenarioBlockingIndex)
		CLEAR_I_JOIN_MISSION_AS_SPECTATOR()
		GOLF_NETWORK_RELEASE_CONTROL_OF_ALL_BALLS(thisFoursome)
		SET_TIME_OF_DAY(TIME_OFF, TRUE)
		SET_DPADDOWN_ACTIVE(TRUE)
		HIDE_ALL_PLAYER_BLIPS(FALSE)
		TOGGLE_STEALTH_RADAR(TRUE)
		DISABLE_SCRIPT_HUD (HUDPART_ALL_OVERHEADS, FALSE)
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		SET_VEHICLE_MODEL_IS_SUPPRESSED(CADDY, FALSE)
		NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE)
		//gDisableRankupMessage = FALSE
		SET_DISABLE_RANK_UP_MESSAGE(FALSE)
		SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY)
		ENDIF
		
		IF IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_TURNED_OFF_EXPANDED_MAP)
			SET_PACKED_STAT_BOOL(PACKED_MP_TOGGLE_EXPANDED_RADAR, TRUE)
		ENDIF
		
		IF GET_BLIP_INFO_ID_DISPLAY(GET_MAIN_PLAYER_BLIP_ID()) != DISPLAY_BLIP
			SET_BLIP_DISPLAY(GET_MAIN_PLAYER_BLIP_ID(), DISPLAY_BLIP)
		ENDIF
		
		CLEAR_OVERRIDE_WEATHER()
		
		PED_INDEX tempPed
		IF NETWORK_IS_IN_SPECTATOR_MODE()
			SET_IN_SPECTATOR_MODE(FALSE, tempPed)
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NETWORK_SET_LOOK_AT_TALKERS(TRUE)
		ENDIF
		
		REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) playerCount
			
			IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, playerCount))
			AND NOT IS_ENTITY_DEAD(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, playerCount))
			AND playerCount != GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
				NETWORK_SET_ENTITY_CAN_BLEND(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, playerCount), TRUE)
			ENDIF
		ENDREPEAT
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		GOLF_CLEANUP_DEBUG_WIDGETS()
	#ENDIF
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
	
		#IF NOT GOLF_IS_AI_ONLY
			CDEBUG1LN(DEBUG_GOLF,"Resesting player control")
		#ENDIF
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		#IF NOT GOLF_IS_MP
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), FALSE)
		#ENDIF
		
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		
		IF thisHelpers.bEndResetScriptCam
			GOLF_END_CUTSCENE_TO_FIRST_PERSON_CAMERA_EFFECT()
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
	ENDIF
	
	IF bClearTask
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) playerCount
		IF playerCount < 4
			CLEANUP_GOLF_FOURSOME_INDEXED_PLAYER_MARKER(thisFoursome, playerCount)
			
			IF IS_GOLF_HELP_DISPLAYED_FLAG_SET(thisHelpers, GHD_GOLF_TERMINATED_DISPLAYED)
				SET_GOLF_CLUB_NO_LONGER_NEEDED(thisFoursome.playerCore[playerCount])
			ELSE
				CLEANUP_GOLF_CLUB(thisFoursome.playerCore[playerCount])
			ENDIF
			
			#IF NOT GOLF_IS_MP			
			
				IF NOT IS_PED_INJURED(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, playerCount))
					SET_PED_CAN_BE_TARGETTED(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, playerCount), TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, playerCount))
					SET_GOLF_BALL_AS_NO_LONGER_NEEDED(thisFoursome.playerCore[playerCount])
				ENDIF
			#ENDIF
			
			#IF GOLF_IS_MP 
			IF thisFoursome.iLocalPlayerIndex != playerCount
				IF NOT IS_PED_INJURED(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, playerCount))
					SET_PED_LEG_IK_MODE(GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, playerCount), LEG_IK_OFF)
				ENDIF
			ENDIF
			#ENDIF
			
			CLEANUP_GOLF_GOLFBAG(thisFoursome.playerCore[playerCount])
			#IF NOT GOLF_IS_AI_ONLY
			REMOVE_PLAYER(thisHelpers.golfUI, playerCount)
			#ENDIF
			
		ENDIF
	ENDREPEAT
	DELETE_TEE(thisFoursome)
	CLEANUP_GOLF_CART(thisFoursome)
	SCRIPT_GOLF_CLEANUP_COURSE(thisCourse)
	
	//Need to set this to -1 or the AI golfers won't be able to play on the last hole the player was on
	g_sGolfGlobals.CurrrentPlayerHole = -1

	IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_IS_FRIEND_ACTIVITY)
		enumActivityResult friendResult = NO_ACTIVITY_RESULT
		
		IF IS_PED_INJURED(PLAYER_PED_ID()) OR IS_PED_BEING_ARRESTED(PLAYER_PED_ID())
			friendResult = AR_deatharrest
		
		ELIF DOES_ENTITY_EXIST(FRIEND_A_PED_ID()) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(FRIEND_A_PED_ID(), PLAYER_PED_ID())
		AND  DOES_ENTITY_EXIST(FRIEND_B_PED_ID()) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(FRIEND_B_PED_ID(), PLAYER_PED_ID())
			friendResult = AR_buddyAll_attacked
		
		ELIF DOES_ENTITY_EXIST(FRIEND_A_PED_ID()) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(FRIEND_A_PED_ID(), PLAYER_PED_ID())
			friendResult = AR_buddyA_attacked
		ELIF DOES_ENTITY_EXIST(FRIEND_B_PED_ID()) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(FRIEND_B_PED_ID(), PLAYER_PED_ID())
			friendResult = AR_buddyB_attacked

		ELIF IS_PED_INJURED(FRIEND_A_PED_ID())
			friendResult = AR_buddy_injured
		ELIF DOES_ENTITY_EXIST(FRIEND_B_PED_ID()) AND IS_PED_INJURED(FRIEND_B_PED_ID())
			friendResult = AR_buddy_injured

		ELIF NOT bFlagPassed
			friendResult = AR_playerQuit

		ELSE
			INT playerScore = GET_GOLF_PLAYER_CURRENT_SCORE_BY_INDEX(thisPlayers, 0)
			INT buddyScoreA = GET_GOLF_PLAYER_CURRENT_SCORE_BY_INDEX(thisPlayers, 1)
			INT buddyScoreB = 99
			BOOL bBuddyBExist = FALSE
			IF DOES_ENTITY_EXIST(FRIEND_B_PED_ID())
				bBuddyBExist = TRUE
				buddyScoreB = GET_GOLF_PLAYER_CURRENT_SCORE_BY_INDEX(thisPlayers, 2)
			ENDIF
			
			IF playerScore < buddyScoreA AND playerScore < buddyScoreB
				friendResult = AR_playerWon //player has the lowest score, he wins
			ELSE
				IF buddyScoreA < playerScore OR buddyScoreB < playerScore //someone beat the player
					IF buddyScoreA < buddyScoreB //buddy A beat buddy B
						friendResult = AR_buddyA_won
					ELIF bBuddyBExist //make sure buddy b was actaully playing
						friendResult =  AR_buddyB_won
					ELSE
						friendResult = AR_playerDraw
					ENDIF
				ELSE //player didn't beat both buddies, and no one beat the player
					friendResult = AR_playerDraw 
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_PED_IN_GROUP(FRIEND_A_PED_ID())
			SET_PED_AS_GROUP_MEMBER(FRIEND_A_PED_ID(), PLAYER_GROUP_ID())
		ENDIF
		
		IF DOES_ENTITY_EXIST(FRIEND_B_PED_ID())
			IF NOT IS_PED_IN_GROUP(FRIEND_B_PED_ID())
				SET_PED_AS_GROUP_MEMBER(FRIEND_B_PED_ID(), PLAYER_GROUP_ID())
			ENDIF
		ENDIF
		finishActivity(ALOC_golf_countryClub, friendResult)
	ENDIF
	IF DOES_BLIP_EXIST(GET_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers))
		CLEANUP_GOLF_HELPER_ESTIMATE_BLIP(thisHelpers)
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("GOLF_TRANQUIL")
		STOP_AUDIO_SCENE("GOLF_TRANQUIL")
	ENDIF
	
	REMOVE_ANIM_DICT(GET_GOLF_BASIC_ANIMATION_DICTIONARY_NAME())
	REMOVE_ANIM_DICT(GET_GOLF_EXTRA_ANIMATION_DICTIONARY_NAME())
	REMOVE_ANIM_DICT(GET_GOLF_IDLE_ANIMATION_DICTIONARY(TRUE))
	REMOVE_ANIM_DICT(GET_GOLF_IDLE_ANIMATION_DICTIONARY(FALSE))
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(thisHelpers.golfUI)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(thisHelpers.floatingUI)
	CLEANUP_SC_LEADERBOARD_UI(thisHelpers.leaderboardUI)
	CLEANUP_MENU_ASSETS()
	RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
	g_sMenuData.bSubtitlesMoved = FALSE
	
	REMOVE_PTFX_ASSET()
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_GOLF_TEXTURE_DICTIONARY_NAME())
	
	// Reseting this here, in case MG_UPDATE_FAIL_SPLASH_SCREEN failed to cleanup. (Very important this gets reset)
	SET_NO_LOADING_SCREEN(FALSE)

	#IF NOT GOLF_IS_MP
	IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_IN(250)
	ENDIF
	#ENDIF
	#IF NOT GOLF_IS_AI_ONLY
		SOCIAL_CLUB_CLEAR_DISPLAY_STRUCT()
	#ENDIF
	
	CLEANUP_PC_GOLF_CONTROLS()
	
	CDEBUG1LN(DEBUG_GOLF,"Golf cleaup complete!")

	//Clean up freemode variables
	IF IS_FREEMODE()
		DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MG_GOLF, thisHelpers.iMatchHistoryID)
		RESET_FMMC_MISSION_VARIABLES(TRUE, FALSE, iPassFailStatus)	
		
		IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
			IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_IN(250)
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ELSE
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ENDIF
	ENDIF

	SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE)
	#IF NOT GOLF_IS_MP
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
		NET_NL()NET_PRINT("SCRIPT_GOLF_CLEANUP: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE) ")
		
	#ENDIF
	
	SET_TEXT_INPUT_BOX_ENABLED(TRUE)	//B* 2184339: Enable cheats on cleanup
	
	TERMINATE_THIS_THREAD()
ENDPROC

#IF NOT GOLF_IS_AI_ONLY
PROC RESET_EVERYTHING_FOR_REMATCH_SP(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], GOLF_HELPERS &thisHelpers)
	INT iScoreIndex, iPlayerIndex
	
	thisHelpers.bGameIsReplay = TRUE
	thisHelpers.bTransitionToShotCamOver = FALSE
	
	thisGame.golfControlFlags = GCF_NO_FLAGS
	thisGame.bPlayoffGame = FALSE
	thisGame.iPlayoffRound = 0
	
	IF NOT IS_PED_INJURED(FRIEND_A_PED_ID())
		SET_GOLF_CONTROL_FLAG(thisGame, GCF_IS_FRIEND_ACTIVITY)
	ENDIF
	
	thisHelpers.golfStreaming = GSF_NO_STREAM_FLAG
	thisHelpers.golfUIControl = GUC_REFRESH_NAVIGATE
	thisHelpers.golfHelpDisplayed = GHD_NONE
	thisHelpers.golfDialogueFlag = GDF_NONE
	thisHelpers.eCurrentGolfUIDisplay = GOLF_DISPLAY_PLAYERCARD
	
	CANCEL_TIMER(thisHelpers.uiTimer)
	CANCEL_TIMER(thisHelpers.navTimer)
	CANCEL_TIMER(thisHelpers.inputTimer)
	CANCEL_TIMER(thisHelpers.cameraTimer)
	CANCEL_TIMER(thisHelpers.resetTimer)
	CANCEL_TIMER(thisHelpers.taskTimer)
	CANCEL_TIMER(thisHelpers.idleSpeechTimer)
	CANCEL_TIMER(thisHelpers.idleTimer)
	
	CLEAR_GOLF_SPLASH()
	CLEANUP_GOLF_HELPER_ALL_BALL_BLIPS(thisHelpers)
	GOLF_CLEANUP_ALL_CAMERA(thisHelpers)
	DISABLE_GOLF_MINIMAP()
	
	SET_GOLF_FOURSOME_STATE(thisFoursome, GGS_NAVIGATE_TO_SHOT)
	CANCEL_TIMER(thisFoursome.physicsTimer)
	CANCEL_TIMER(thisFoursome.physicsTimeout)
	thisFoursome.iCurrentHole = 0
	
	OBJECT_INDEX objBalll, objClub
	VECTOR vNewBallPos = GET_GOLF_HOLE_TEE_POSITION(thisCourse, 0)
	
	REPEAT COUNT_OF(thisFoursome.playerCore) iPlayerIndex
		SET_GOLF_FOURSOME_INDEXED_PLAYER_STATE(thisFoursome, iPlayerIndex, GPS_PLACE_BALL)
		thisFoursome.playerCore[iPlayerIndex].eStatusBits = GOLF_STATUS_NONE
		thisFoursome.playerCore[iPlayerIndex].iCurrentHoleShots = 0
		objBalll = GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome,iPlayerIndex)
		objClub = GET_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(thisFoursome,iPlayerIndex)
		IF DOES_ENTITY_EXIST(objBalll)
			DELETE_OBJECT(objBalll)
		ENDIF
		
		//delleting club to remove the one that was used for the golf in bag cutscene
		IF DOES_ENTITY_EXIST(objClub)
			DELETE_OBJECT(objClub)
		ENDIF
		
		SET_GOLF_FOURSOME_INDEXED_PLAYER_BALL_POSITION(thisFoursome, iPlayerIndex, vNewBallPos)
		SET_GOLF_FOURSOME_INDEXED_PLAYER_DISTANCE_TO_HOLE(thisFoursome, iPlayerIndex, VDIST(vNewBallPos, GET_GOLF_HOLE_PIN_POSITION(thisCourse, 0)))
	ENDREPEAT

	REPEAT COUNT_OF(thisPlayers) iPlayerIndex
		
		thisPlayers[iPlayerIndex].iCurrentScore = 0
		
		REPEAT 9 iScoreIndex
			thisPlayers[iPlayerIndex].iCourseScore[iScoreIndex].iHoleScore = 0
			thisPlayers[iPlayerIndex].iCourseScore[iScoreIndex].bFairwayInRegulation = FALSE
			thisPlayers[iPlayerIndex].iCourseScore[iScoreIndex].bGreenInRegulation = FALSE 
			thisPlayers[iPlayerIndex].iCourseScore[iScoreIndex].iHoleScore = 0
		ENDREPEAT
	ENDREPEAT
	
	INITIALISE_PLAYERCARD_SCORES(thisFoursome)
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iPlayerIndex
		IF NOT IS_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL_FLAG_SET(thisFoursome, iPlayerIndex, GOLF_PLAYER_REMOVED)
			SET_PLAYERCARD_SLOT(thisHelpers, thisFoursome, thisFoursome.playerCore[iPlayerIndex].playerCard, 0, FALSE, FALSE)
			SET_SCOREBOARD_SLOT(thisHelpers, thisCourse, thisFoursome, thisPlayers[iPlayerIndex], thisFoursome.playerCore[iPlayerIndex].playerCard, FALSE, FALSE)
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF

// Perform any special commands if the script passes
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC SCRIPT_PASSED(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], GOLF_HELPERS &thisHelpers)
	SCRIPT_GOLF_CLEANUP(thisGame, thisCourse,thisFoursome, thisPlayers, thisHelpers)
ENDPROC

// Perform any special commands if the script fails
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC SCRIPT_FAILED(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], GOLF_HELPERS &thisHelpers)
	SCRIPT_GOLF_CLEANUP(thisGame, thisCourse,thisFoursome, thisPlayers,  thisHelpers)
ENDPROC

FUNC BOOL GOLF_SHOULD_ALLOW_ALT_CONTROLS(GOLF_FOURSOME &thisFoursome)

	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) >= GPS_BALL_IN_FLIGHT
	OR GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_NAVIGATE_TO_SHOT
	OR GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_SCORECARD_END_HOLE
	OR GET_GOLF_FOURSOME_STATE(thisFoursome) = GGS_NAVIGATE_OUT_OF_BOUNDS
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC DISABLE_GOLF_CONTROLS(BOOL bDisbaleMenu, BOOL bAllowAltControls)
	IF bAllowAltControls
		ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ENDIF

	//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	
	#IF GOLF_IS_MP
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)
	#ENDIF
	
	IF bDisbaleMenu
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	ENDIF
ENDPROC

PROC DISABLE_GOLF_HUD(BOOL bDisableCash)
	HIDE_ALL_BOTTOM_RIGHT_HUD()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	
	IF bDisableCash
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
	ENDIF
	
	SET_WAYPOINT_OFF()
	
	//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
ENDPROC

#IF IS_DEBUG_BUILD
#IF NOT GOLF_IS_AI_ONLY
/// PURPOSE:
///    Manages debug input (finish hole, pass, fail, add/remove shots from current hole)
/// PARAMS:
///    golfState - 
///    thisFoursome - 
///    thisPlayers - 
PROC GOLF_MANAGE_DEBUG_INPUT(GOLF_MINIGAME_STATE &golfState, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER &thisPlayers[], GOLF_HELPERS& golfHelpers)
	
	INT iPlayerIndex
	
	IF golfState = GMS_PLAY_GOLF
		// Debug Key: Check for Pass
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			bFlagPassed = TRUE
			golfHelpers.bWin = TRUE
			golfState = GMS_OUTRO_INIT
		
		// Debug Key: Check for Fail
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			bFlagPassed = FALSE
			golfHelpers.bWin = FALSE
			golfState = GMS_OUTRO_INIT
		ENDIF
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		golfState = GMS_CLEANUP
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
		IF GET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL(thisFoursome, 0) = HUMAN_LOCAL
			SET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL(thisFoursome, 0, COMPUTER)
		ELSE
			SET_GOLF_FOURSOME_INDEXED_PLAYER_CONTROL(thisFoursome, 0, HUMAN_LOCAL)
		ENDIF
	ENDIF
	
	// Finish the current hole
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) != GPS_APPROACH_ANIM
		bAllowLeaderboardUpload = FALSE
		REPEAT thisFoursome.iNumPlayers iPlayerIndex
			SET_GOLF_PLAYER_STATE(thisFoursome.playerCore[iPlayerIndex], GPS_DONE_WITH_HOLE)
		ENDREPEAT
	ENDIF
	
	// Add a shot to the current hole
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
		bAllowLeaderboardUpload = FALSE
		INCREMENT_GOLF_PLAYER_HOLE_SCORE_BY_INDEX(thisPlayers,0,GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), 1)
	ENDIF
	//Remove a shot from the current hole
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
		bAllowLeaderboardUpload = FALSE
		INCREMENT_GOLF_PLAYER_HOLE_SCORE_BY_INDEX(thisPlayers,0,GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), -1)
	ENDIF


	#IF NOT GOLF_IS_AI_ONLY
		vDebugShotPosition = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
	#ENDIF

ENDPROC

#IF GOLF_IS_MP
PROC GOLF_MANAGE_MP_DEBUG_INPUT(GOLF_MP_SERVER_BROADCAST_DATA &serverBD,GOLF_MP_PLAYER_BROADCAST_DATA &playerBD[], GOLF_FOURSOME &thisFoursome, GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_HELPERS &thisHelpers, GOLF_PLAYER &thisPlayers[])
	
	// Debug Key: Check for Pass
//	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
//		bFlagPassed = TRUE
//		golfState = GMS_CLEANUP
//	ENDIF
//
	// Debug Key: Check for Fail
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		bAllowLeaderboardUpload = FALSE
		serverBD.eServerGolfState = GMS_CLEANUP
	ENDIF

	INT playerIndex
	BOOL clearNextHoleFlag = TRUE
	
	REPEAT MP_GOLF_MAX_PLAYERS playerIndex
		IF NOT IS_GOLF_MP_CLIENT_CONTROL_FLAG_SET(playerBD, playerIndex, GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE)
		AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(playerIndex))
			clearNextHoleFlag = FALSE
		ENDIF
	ENDREPEAT
	
	IF clearNextHoleFlag
		CLEAR_GOLF_MP_SERVER_CONTROL_FLAG(serverBD, GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE)
	ENDIF

	// Finish the current hole
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) OR IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_SHIFT, "Next hole"))
	AND thisHelpers.bTransitionToShotCamOver AND IS_SCREEN_FADED_IN() AND NOT IS_GOLF_FOURSOME_STATE_AT_END(thisFoursome)
	AND GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) = GPS_ADDRESS_BALL
	AND NOT IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(serverBD, GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE)
	
		INT iCurrentHole = GET_GOLF_MP_SERVER_CURRENT_HOLE(serverBD)
		INT iHoleScore
		INT iOppIndex
		REPEAT MP_GOLF_MAX_PLAYERS iOppIndex
			IF IS_KEYBOARD_KEY_PRESSED(KEY_LSHIFT)
				iHoleScore  = GET_GOLF_HOLE_PAR(thisCourse, iCurrentHole) + (5 - iOppIndex)
			ELIF IS_KEYBOARD_KEY_PRESSED(KEY_LCONTROL)
				IF iOppIndex = 0 OR iOppIndex = 1
					iHoleScore  = GET_GOLF_HOLE_PAR(thisCourse, iCurrentHole)
				ELSE
					iHoleScore  = GET_GOLF_HOLE_PAR(thisCourse, iCurrentHole) + 5
				ENDIF
			ELSE
				iHoleScore = GET_GOLF_HOLE_PAR(thisCourse, iCurrentHole)
			ENDIF
			
			SET_GOLF_MP_SERVER_PLAYER_HOLE_SCORE(serverBD, iOppIndex, iCurrentHole, iHoleScore)
			SET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(serverBD, iOppIndex, iHoleScore)
		ENDREPEAT
		
		RESET_GOLF_MP_APPROACH_CAM(thisHelpers)
		CLEAR_CONTROL_FLAGS_BALL_AT_REST(thisFoursome, thisGame, thisHelpers)
		SET_GOLF_MP_SERVER_CONTROL_FLAG(serverBD, GCF_MOVE_TO_NEXT_HOLE_MP_OVERRIDE)
		SET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(serverBD, GET_GOLF_PLAYER_NET_INDEX_BY_INDEX_AS_INT(thisPlayers, GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)))
		INCREMENT_GOLF_MP_SERVER_CURRENT_HOLE(serverBD)
		RESET_GOLF_MP_APPROACH_CAM(thisHelpers)
	ENDIF
	// Add a shot to the current hole
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
//		INCREMENT_GOLF_PLAYER_HOLE_SCORE_BY_INDEX(thisPlayers,0,GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), 1)
//	ENDIF
//	//Remove a shot from the current hole
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
//		INCREMENT_GOLF_PLAYER_HOLE_SCORE_BY_INDEX(thisPlayers,0,GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome), -1)
//	ENDIF
ENDPROC
#ENDIF
#ENDIF
#ENDIF

