USING "golf.sch"
USING "golf_helpers.sch"

/// PURPOSE:
///    Finds a shot estimate for a player, using simple projection
/// PARAMS:
///    thisPlayerCore - 
///    golfBag - 
/// RETURNS:
///    VECTOR estimate for the given shot
FUNC VECTOR GOLF_FIND_SHOT_ESTIMATE(GOLF_FOURSOME &thisFoursome, GOLF_BAG &golfBag)
	VECTOR shotEstimate 
	FLOAT randAim = GET_RANDOM_FLOAT_POLAR(0, 8.0)
	FLOAT randPower = GET_RANDOM_FLOAT_POLAR(1.0, 0.2)
	
	//no randomness for hard ai
	IF IS_GOLF_CURRENT_PLAYER_HARD_AI(thisFoursome)
		randAim = GET_RANDOM_FLOAT_POLAR(0, 4.0)
		randPower = 1.0
	ENDIF
	
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_FAR_AWAY) OR GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER
		shotEstimate = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome) + randAim, <<-GET_GOLF_CLUB_RANGE(golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)) * GET_SWING_STYLE_POWER_MULTIPLIER(GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome)) * GET_GOLF_LIE_MULTIPLIER(GET_GOLF_PLAYER_LIE(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)]))*((GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome, TRUE) * randPower)/100),0.0,0.0>>)
	ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) != LIE_GREEN
		shotEstimate = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome),GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), <<-GET_GOLF_CLUB_RANGE(golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)) * GET_SWING_STYLE_POWER_MULTIPLIER(GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome)) * GET_GOLF_LIE_MULTIPLIER(GET_GOLF_PLAYER_LIE(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)])) *(GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome, TRUE)/100),0.0,0.0>>)
	ELSE
		shotEstimate = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome),GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome), -thisFoursome.swingMeter.fMaxDistance * <<(GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome, TRUE)/100.0),0.0,0.0>> )
	ENDIF
	
	FLOAT fHeight
	IF GET_GROUND_Z_FOR_3D_COORD (shotEstimate, fHeight) 
		shotEstimate.z = fHeight
	ELSE
		shotEstimate.z += 50.0
		IF GET_GROUND_Z_FOR_3D_COORD (shotEstimate, fHeight) 
			shotEstimate.z = fHeight
		ELSE
			VECTOR curBallPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
			shotEstimate.z = curBallPos.z
		ENDIF
	ENDIF
	RETURN shotEstimate
ENDFUNC

FUNC FLOAT GET_MAX_BALL_HEIGHT(GOLF_FOURSOME &thisFoursome)

	OBJECT_INDEX playerBall = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)
	
	IF NOT DOES_ENTITY_EXIST(playerBall)
		RETURN 0.0
	ENDIf

	VECTOR vBallVel = GET_ENTITY_VELOCITY(playerBall)
	VECTOR vBallpos = GET_ENTITY_COORDS(playerBall)
	FLOAT fHeightAboveGround, fGroundHeight
	FLOAT fBounceCoef = 0.5
	
	IF GET_GROUND_Z_FOR_3D_COORD(vBallpos + <<0,0,0.1>>, fGroundHeight)
	
		fHeightAboveGround = vBallpos.z - fGroundHeight
	
		FLOAT currentPE = fHeightAboveGround*9.8
		FLOAT currentKE = 0.5 * vBallVel.z * vBallVel.z
		FLOAT fCurrentBounceMaxHeight = (currentPE + currentKE/9.8)
        FLOAT fPostBounceMaxHeight = SQRT(2.0 * 9.8 * fHeightAboveGround)
        fPostBounceMaxHeight += ABSF(vBallVel.z)
        fPostBounceMaxHeight *= fBounceCoef
        fPostBounceMaxHeight *= fPostBounceMaxHeight
        fPostBounceMaxHeight /= (2.0 * 9.8)
		
		FLOAT fOffsetInCurrentBounce = 0.0
		IF vBallVel.z > 0.0
		   //Going up
			fOffsetInCurrentBounce = currentPE * 0.5 / (currentPE + currentKE)
		ELSE
			//Coming down
		     fOffsetInCurrentBounce = 0.5 + (currentKE - currentPE) * 0.5 / (currentPE + currentKE)
		ENDIF

		FLOAT fMaxBallHeightRightNow = fCurrentBounceMaxHeight + (fPostBounceMaxHeight - fCurrentBounceMaxHeight) * fOffsetInCurrentBounce

#IF IS_DEBUG_BUILD
		FLOAT PEHeight = currentPE / 9.8
		FLOAT KEHeight = currentKE / 9.8
		FLOAT TotalHeight = PEHeight + KEHeight
		FLOAT NextBounceHeight = 2 * TotalHeight * fBounceCoef * fBounceCoef

//		FLOAT fDistanceToBallXY = CONST_OFFSET_XY + fMaxBallHeightRightNow * kfFRAMING_CONSTANT    //Tweaks, perhaps start with 4.0, 2.0
//		FLOAT fDesiredCameraHeight = CONST_OFFSET_Z + fMaxBallHeightRightNow                       //Tweak, perhaps 
		VECTOR vBallPosDebugOut = vBallpos
		vBallPosDebugOut.z = fMaxBallHeightRightNow + fGroundHeight
		
		DEBUG_RECORD_SPHERE("Max ball pos", vBallPosDebugOut, 0.25, <<255,255,255>>)
		vBallPosDebugOut.z = fPostBounceMaxHeight + fGroundHeight
		DEBUG_RECORD_SPHERE("Post bounce max", vBallPosDebugOut, 0.25, <<255,0,255>>)
		DEBUG_RECORD_SPHERE("Curr ball pos", vBallpos, 0.25, <<0,0,255>>)
		
		vBallPosDebugOut.z = NextBounceHeight + fGroundHeight
		DEBUG_RECORD_SPHERE("NextBounceHeight", vBallPosDebugOut, 0.25, <<0,0,255>>)
		
		vBallPosDebugOut.x = PEHeight + fGroundHeight
		vBallPosDebugOut.y = KEHeight + fGroundHeight
		vBallPosDebugOut.z = TotalHeight + fGroundHeight
		
		DEBUG_RECORD_SPHERE("PEHeight\KEHeight\TotalHeight", vBallPosDebugOut, 0.25, <<0,0,0>>)
		
		vBallPosDebugOut.x = currentPE
		vBallPosDebugOut.y = currentKE
		vBallPosDebugOut.z = currentPE + currentKE
		DEBUG_RECORD_SPHERE("Energy(PotentialKineticTotal)", vBallPosDebugOut, 0.25, <<0,0,255>>)
#ENDIF
		
		RETURN fMaxBallHeightRightNow + fGroundHeight
	ENDIF

	RETURN vBallpos.z + vBallVel.z*vBallVel.z*0.5
ENDFUNC

FUNC BOOL IS_BALL_LOW_IMPACT_SPEED(OBJECT_INDEX objBall)

	#IF NOT GOLF_IS_AI_ONLY
		CDEBUG1LN(DEBUG_GOLF,"Impact speed of ball ", GET_ENTITY_SPEED(objBall))
	#ENDIF
	RETURN GET_ENTITY_SPEED(objBall) < 6.0
ENDFUNC

FUNC BOOL GOLF_SHOULD_USE_BALL_BOUNCE(GOLF_FOURSOME &thisFoursome, GOLF_GAME &thisGame)

	eCLUB_TYPE clubType = GET_GOLF_CLUB_TYPE(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))

	IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_FIRST_COLLISION)
	AND GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) != SWING_STYLE_PUNCH 
	AND clubType < eCLUB_WEDGE_PITCH AND NOT IS_ENTITY_IN_WATER(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///   Changes player aim based on green slope
/// PARAMS:
///    lieNormal - 
///    playerAim - 
/// RETURNS:
///    FLOAT scalar to putting power based on green slope
FUNC FLOAT GOLF_MODIFY_PUTT_FROM_GREEN_LIE(GOLF_FOURSOME &thisFoursome, VECTOR lieNormal, VECTOR &playerAim, BOOL isShotGimmie)

	VECTOR xyNormal = NORMALISE_VECTOR(<<lieNormal.x, lieNormal.y, 0>>)
	VECTOR xyAim =   NORMALISE_VECTOR(<<playerAim.x, playerAim.y, 0>>)
	FLOAT cosAngle = DOT_PRODUCT(xyNormal, xyAim)
	
	CDEBUG1LN(DEBUG_GOLF,"Lie normal ", lieNormal, " xyNormal ", xyNormal)
	CDEBUG1LN(DEBUG_GOLF,"Player aim ", playerAim, " xyAim ", xyAim)
	
	VECTOR ballPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
	VECTOR aimPos = ballPos + xyAim
	VECTOR liePos = ballPos + xyNormal
	FLOAT degree = 1.0
	FLOAT modify = PICK_FLOAT(isShotGimmie, 10.0, 30.0)
    IF ((aimPos.x - ballPos.x)* (liePos.y - ballPos.y) - (aimPos.y - ballPos.y)*(liePos.x - ballPos.x)) < 0
		degree = -degree
	ENDIF
	
	IF cosAngle < -0.5 // the grounds normal points away from the players aim, so you are going up hill
		//-1 is flat ground, so this would return one so it would not change, as z grows so does the slope of the hill and increase in putt strength
		CDEBUG1LN(DEBUG_GOLF,"Power modify up hill ", 1.0 + (1.0 + lieNormal.z) * modify)
		RETURN  1.0 + (1.0 + lieNormal.z) * modify
	ELIF cosAngle > 0.5 //the groudns normal points in the same general direction as the players aim, you are going down hill
		//as the slope of the hill grows the power decreases
		CDEBUG1LN(DEBUG_GOLF,"Power modify down hill ", 1.0 - (1.0 + lieNormal.z) )
		RETURN 1.0 - (1.0 + lieNormal.z) 
	ELSE //aim is roughly perpendicular to the slope of the green
		//modify aim to account for the hill, rotate aim to the negative of the lie normal
		IF lieNormal.z > -0.999//dont rotate if the ground is generally flat
			CDEBUG1LN(DEBUG_GOLF,"ROATATE AIM by ", degree * ((1.0 + lieNormal.z)*5.0))
			playerAim = ROTATE_VECTOR_ABOUT_Z(playerAim, degree * ((1.0 + lieNormal.z)*5.0))
		ENDIF
		
		modify = PICK_FLOAT(isShotGimmie, 1.0, 10.0)
		CDEBUG1LN(DEBUG_GOLF,"Power modify perpendicular ", 1.0 + (1.0 + lieNormal.z)*modify)
		RETURN 1.0 + (1.0 + lieNormal.z)*modify
	ENDIF
ENDFUNC

/// PURPOSE:
///    Hits the golf ball by applying a physical force based on the players swing meter
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
///    golfBag - 
FUNC BOOL GOLF_HIT_BALL(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome #IF NOT GOLF_IS_AI_ONLY , GOLF_HELPERS &thisHelpers #ENDIF )
	
	#IF NOT GOLF_IS_AI_ONLY
	UNUSED_PARAMETER(thisHelpers)
	#ENDIF
	
	FREEZE_GOLF_BALL(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), FALSE)
	SET_ENTITY_RECORDS_COLLISIONS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), TRUE)
	SET_OBJECT_PHYSICS_PARAMS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), -1.0, -1.0, <<0.0, 0.0, 0.01>>, <<-1.0, -1.0, -1.0>>, -1.0, -1.0)

	BOOL isComputerSwing  =  GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER
	FLOAT swingPower = GET_SWING_POWER(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame.golfBag, thisFoursome.swingMeter, isComputerSwing,  !bPerfectAccuracyMode)
	
	// Randomize power based on lie, a bit
	FLOAT fRandomPower = 1.0
	SWITCH GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)

		//CASE LIE_TEE   no power reduction when on tee
		//CASE LIE_GREEN no randomness in green putting power
		CASE LIE_FAIRWAY
			fRandomPower = 0.95 //GET_RANDOM_FLOAT_POLAR(0.95, 0.05)
		BREAK
		CASE LIE_ROUGH
		CASE LIE_CART_PATH
			fRandomPower = GET_RANDOM_FLOAT_POLAR(0.9, 0.075)
		BREAK
		CASE LIE_BUNKER
		CASE LIE_WATER
		CASE LIE_UNKNOWN
			fRandomPower= GET_RANDOM_FLOAT_POLAR(0.85, 0.15)
			IF fRandomPower > 0.85
				fRandomPower = 0.85 //cap power for bunker shots
			ENDIF
		BREAK
	ENDSWITCH
	
	swingPower *= fRandomPower
			
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER
	AND GET_GOLF_CLUB_TYPE(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)) = eCLUB_WEDGE_LOB 
		//ai is overshooting with lob
		swingPower *= 0.85
	ENDIF
	
	FLOAT swingLoft = GET_GOLF_CLUB_LOFT(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)) / 90.0
	swingLoft += 0.05
	
	FLOAT swingHeading = GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome)
	FLOAT clubSpinMod = GET_GOLF_CLUB_SPIN(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))
//	FLOAT clubNaturalSpin = (25 - GET_GOLF_CLUB_LOFT(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome))) / 2  // Each club has a natural back/top spin it imparts based on the loft
//	IF bDisableNaturalClubSpinEffects
//	OR IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
//		clubNaturalSpin = 0
//	ENDIF
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFOursome) = SWING_STYLE_PUNCH
		swingLoft = GET_GOLF_CLUB_LOFT(thisGame.golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)) / 150.0
	ENDIF
	
	SWITCH thisFoursome.swingMeter.meterState
		CASE SWING_METER_FINISHED
			swingHeading = GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome)
			IF isComputerSwing AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
				swingHeading = GET_HEADING_BETWEEN_VECTORS(GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))+90
				#IF NOT GOLF_IS_AI_ONLY
					CDEBUG1LN(DEBUG_GOLF,"Ai swing heading it ", swingHeading)
				#ENDIF
			ENDIF
			
			//bad acuracy when not using sand wedge in bunker
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_BUNKER
			AND GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome) != GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eCLUB_WEDGE_SAND)
				swingHeading += ( 25.0 * GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0))
			ENDIF
			
			IF NOT isComputerSwing
				thisFoursome.swingMeter.fXSpin = 0//clubNaturalSpin * clubSpinMod
				thisFoursome.swingMeter.fYSpin =  45.0*thisFoursome.swingMeter.fCurrentAccurRel
			ENDIF
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_POWER
				swingHeading += GET_RANDOM_FLOAT_POLAR(0, 5)
				thisFoursome.swingMeter.fXSpin *= 2
				thisFoursome.swingMeter.fYSpin *= 2
			ENDIF
		BREAK
		CASE SWING_METER_SHANK
			
			IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
				swingPower *= GET_RANDOM_FLOAT_IN_RANGE(0.15, 0.25)
				swingLoft *= GET_RANDOM_FLOAT_IN_RANGE(0.25, 1.25)
			ENDIF

			IF thisFoursome.swingMeter.iLastX < 0
				swingHeading = GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome) + (SWING_METER_HEADING_MAX_SHANK * GET_RANDOM_FLOAT_IN_RANGE(0, 1.0))
				thisFoursome.swingMeter.fXSpin = GET_RANDOM_FLOAT_IN_RANGE(0, 20.0) * clubSpinMod
			ELSE
				swingHeading = GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome) + (SWING_METER_HEADING_MAX_SHANK * GET_RANDOM_FLOAT_IN_RANGE(-1.0, 0))
				thisFoursome.swingMeter.fXSpin = GET_RANDOM_FLOAT_IN_RANGE(-20.0, 0) * clubSpinMod
			ENDIF
			thisFoursome.swingMeter.fYSpin =  GET_RANDOM_FLOAT_IN_RANGE(-20.0, 20.0) * clubSpinMod
		BREAK
		DEFAULT
			SCRIPT_ASSERT("GOLF_HIT_BALL has an invalid state - please contact Alan Blaine with repro information")
		BREAK
	ENDSWITCH
	VECTOR swingForce
	VECTOR vLieNormal, vPlayerAim, vLieOrthogonal, vShotNormal
	
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
		#IF PUTTING_GREEN
			swingPower *= 0.95 //putting green golfers are hitting ball too hard
		#ENDIF
	
		swingForce = <<COS(swingHeading) * swingPower, SIN(swingHeading) * swingPower, 0>>
		
		#IF NOT GOLF_IS_AI_ONLY
			//normal and hard golfers modify their putt based on slope of green
			IF (GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER AND GET_GOLF_FOURSOME_CURRENT_PLAYER_AI_DIFFICULTY(thisFoursome) != GOLF_AI_EASY)
				OR IS_SHOT_A_GIMME(thisCourse, thisFoursome)
				vLieNormal = GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome)
				vPlayerAim = NORMALISE_VECTOR( <<COS(swingHeading), SIN(swingHeading), 0>>)
				swingPower *= GOLF_MODIFY_PUTT_FROM_GREEN_LIE(thisFoursome, vLieNormal, vPlayerAim, IS_SHOT_A_GIMME(thisCourse, thisFoursome))
				swingForce = swingPower * vPlayerAim
			ENDIF
		#ENDIF
	ELSE

		FLOAT invLoft = 1 - swingLoft
		FLOAT x = 2 * invLoft - (invLoft*invLoft)
		FLOAT y = 2 * swingLoft - (swingLoft*swingLoft)
		// New version of shot force, incorporating lie normal
		
		vLieNormal = PICK_VECTOR(GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER ,<<0,0,1>>, -GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome))
		vPlayerAim = NORMALISE_VECTOR( <<COS(swingHeading), SIN(swingHeading), 0>>)

		// Find the player aim, projected onto a normal vector that is orthogonal to the lie normal (S - ((S.L/L^2) * L))
		vLieOrthogonal = -NORMALISE_VECTOR(vPlayerAim - (DOT_PRODUCT(vPlayerAim, vLieNormal) * vLieNormal))

		vShotNormal = NORMALISE_VECTOR((-vPlayerAim * x) + (vLieNormal* y))
		// Rotate the projection toward the lie normal based on the swing loft angle
		VECTOR vShotDirection  = NORMALISE_VECTOR((vLieOrthogonal * x) + (vLieNormal * y))
			
	//	CDEBUG1LN(DEBUG_GOLF,"Lie normal ", vLieNormal, " vPlayerAim ", vPlayerAim, " shot dir ", vShotDirection, " lie orth ", vLieOrthogonal, " heading ", swingHeading)
		
		//Cap the amount you can rotate the vector 
		FLOAT fCosAngleBetweenAims = DOT_PRODUCT(vPlayerAim, NORMALISE_VECTOR(<<vShotDirection.x, vShotDirection.y, 0>>))
		FLOAT fClockwise = PICK_FLOAT(((vPlayerAim.x*vShotDirection.y) - (vPlayerAim.y*vShotDirection.x)) < 0, -1, 1)
		IF ABSF(fCosAngleBetweenAims) < COS(MAX_ANGLE_DEVIATION)
			vShotDirection = ROTATE_VECTOR_ABOUT_Z(vShotDirection, fClockwise*(ACOS(ABSF(fCosAngleBetweenAims))-MAX_ANGLE_DEVIATION))
		ENDIF
		
		//the z from vShotNormal(V1) is the z I want, the direction from vShotDirection(V2) is the direction I want
		//the below is an equation to get a normalized vector (V3) where V3.z = V1.z
		// and the normalized 2D vectors (x,y) of V3 and V2 are equal
		vShotNormal.y = (vShotDirection.y/ABSF(vShotDirection.y)) * SQRT(ABSF((vShotNormal.z*vShotNormal.z - 1)/((vShotDirection.x*vShotDirection.x)/(vShotDirection.y*vShotDirection.y)+1)))
		vShotNormal.x = ((vShotDirection.x*vShotNormal.y)/vShotDirection.y)
		
		#IF NOT GOLF_IS_AI_ONLY
			IF isComputerSwing AND IS_GOLF_CURRENT_PLAYER_HARD_AI(thisFoursome)
				VECTOR vBallPos = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
				VECTOR vShotEst = GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome)
				
				IF vShotEst.z < (vBallPos.z - 4.0)
					CDEBUG1LN(DEBUG_GOLF,"Shooting down hill, adjust power a bit")
					swingPower *= 0.9
				ENDIF
			ENDIF
		#ENDIF
		
			/*
			VECTOR startPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
			DRAW_DEBUG_LINE(startPos, startPos+vPlayerAim, 0,255,255)
			DRAW_DEBUG_LINE(startPos, startPos+vLieNormal, 0,0,255)
			DRAW_DEBUG_LINE(startPos, startPos+vLieOrthogonal, 255, 0, 0)
			DRAW_DEBUG_LINE(startPos, startPos+vShotNormal, 0, 255, 0)
			//*/
			
		// Scale the shot direction normal vector by the swing power to get the force vector to apply to the ball
		swingForce = -1.0*swingPower * vShotNormal
	ENDIF

	#IF NOT GOLF_IS_AI_ONLY
		CDEBUG1LN(DEBUG_GOLF,"Power :",swingPower," Loft:",swingLoft," Heading:",swingHeading)
		CDEBUG1LN(DEBUG_GOLF,"Swing Force ", swingForce)
		CDEBUG1LN(DEBUG_GOLF,"Lie normal ", vLieNormal)
	#ENDIF
			
	IF bDebugSpew 
		CDEBUG1LN(DEBUG_GOLF,"Power Meter: ",thisFoursome.swingMeter.fCurrentPowerRel," Accuracy Meter:",thisFoursome.swingMeter.fCurrentAccurRel)
		CDEBUG1LN(DEBUG_GOLF,"Power Meter Goal:",GET_GOLF_FOURSOME_CURRENT_PLAYER_METER_GOAL(thisFoursome)," Max Distance:",thisFoursome.swingMeter.fMaxDistance)
		IF ARE_VECTORS_ALMOST_EQUAL(swingForce, <<0,0,0>>, 0.001)
			IF bDebugSpew DEBUG_MESSAGE("Trying to hit ball with force 0,0,0") ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_GREEN
	AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
		CDEBUG1LN(DEBUG_GOLF,"Player on green but is not putting")
		//set lie to something else so the 'on green' physics don't kick in
		SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, <<0,0,0>>, LIE_FAIRWAY)
	ENDIF

	// Apply force to golf ball
	//APPLY_FORCE_TO_ENTITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), APPLY_TYPE_IMPULSE, swingForce, <<0,0,0>>, 0, FALSE, FALSE, TRUE)
	SET_ENTITY_VELOCITY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), swingForce)
	SET_ENTITY_MAX_SPEED(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), 150.0)
	SET_GOLF_CONTROL_FLAG(thisGame, GCF_BALL_JUST_HIT)
	
	// Play sound
	#IF NOT GOLF_IS_AI_ONLY
		CDEBUG1LN(DEBUG_GOLF,"Playing hit sound, acutal meter pos ", thisFoursome.swingMeter.fCurrentMeterPosition)
	#ENDIF
	GOLF_PLAY_CLUB_IMPACT_SOUND(thisGame, thisFoursome, FALSE, IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_WAS_PERFECT_HIT), thisFoursome.swingMeter.fCurrentMeterPosition < 50.0 )
	
	// Play strike vfx
	GOLF_PLAY_CLUB_IMPACT_PTFX(thisFoursome, thisCourse, thisGame)
	
	CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_AT_REST)
	IF NOT IS_BALL_ON_GREEN(thisCourse, thisFoursome)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_SHOT_STARTED)
	ENDIF
	RESTART_TIMER_NOW(thisFoursome.physicsTimeout)
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
		IF NOT IS_BALL_ON_GREEN(thisCourse, thisFoursome)
			// Would like to get 'this player' in here, but doubtful!
			CDEBUG1LN(DEBUG_GOLF,"Shake%% - 4")
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 256)
			CDEBUG1LN(DEBUG_GOLF,"GOLF_HIT_BALL: RUMBLE!")
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
	IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"Hitting ball with force ", swingForce, ": swing has power ", swingPower, " and loft ", swingLoft * 100) ENDIF
	#ENDIF
	RETURN TRUE
ENDFUNC


FUNC BOOL GOLF_HANDLE_BALL_IN_WATER(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome #IF NOT GOLF_IS_AI_ONLY , GOLF_HELPERS & thisHelpers #ENDIF)

	OBJECT_INDEX playerBall = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)
	UNUSED_PARAMETER(thisGame)
	UNUSED_PARAMETER(thisCourse)
	
	IF IS_ENTITY_IN_WATER(playerBall)
		CDEBUG1LN(DEBUG_GOLF,"Ball fell in water")
		#IF NOT GOLF_IS_AI_ONLY
			IF DOES_CAM_EXIST(thisHelpers.camFlight)
				DETACH_CAM(thisHelpers.camFlight)
				STOP_CAM_POINTING(thisHelpers.camFlight)
			ENDIF
		#ENDIF
		SET_ENTITY_RECORDS_COLLISIONS(playerBall, FALSE)
		FREEZE_GOLF_BALL(playerBall, TRUE)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, <<0,0,0>>, LIE_WATER)
		PLAY_GOLF_WATER_IMPACT_SOUND_AND_PTFX(thisFoursome)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_BALL_AT_REST)
		//SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_SHOT_OVER)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_AT_REST)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_FIRST_COLLISION)
//		#IF NOT GOLF_IS_AI_ONLY
//			GOLF_MANAGE_POST_SHOT_ANIMATIONS(thisGame, thisHelpers, thisCourse, thisFoursome)
//		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

#IF NOT GOLF_IS_AI_ONLY
FUNC BOOL BALL_FALL_THROUGH_WORLD_CHECK(OBJECT_INDEX playerBall, GOLF_HELPERS &thisHelpers)

	FLOAT fHeight = 0.0 
	BOOl bBallFellThrough = FALSE
	
	IF NOT GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(playerBall), fHeight)
		VECTOR vBallPos = GET_ENTITY_COORDS(playerBall)
		thisHelpers.iNumTimesFallThroughWorldCheckFailed++
		
		CDEBUG1LN(DEBUG_GOLF,"Ball fell through. Ball pos was ", vBallPos)
		CDEBUG1LN(DEBUG_GOLF,"Number of frames checked failed in a row ", thisHelpers.iNumTimesFallThroughWorldCheckFailed)
		
		//Check has to fail twice in a row, helps against false negatives
		IF thisHelpers.iNumTimesFallThroughWorldCheckFailed > 1
			CDEBUG1LN(DEBUG_GOLF,"Check if close to surface.")
			
			bBallFellThrough = TRUE
			IF GET_GROUND_Z_FOR_3D_COORD(vBallPos+<<0.1, 0.1, 0.0>>, fHeight)
				IF (vBallPos.z - fHeight) > 1.0
					CDEBUG1LN(DEBUG_GOLF,"There is a huge drop right next to the balls current position.")
					CDEBUG1LN(DEBUG_GOLF,"Get ground Z is returning a false negative at the balls current position")
					
					bBallFellThrough = FALSE
				ENDIF
			ENDIF
			
			IF GET_GROUND_Z_FOR_3D_COORD(vBallPos+<<0,0,0.05>>, vBallPos.z) //pretty close to the surface, move it up
			AND bBallFellThrough
				CDEBUG1LN(DEBUG_GOLF,"Its close enough, just move it")
				SET_ENTITY_COORDS(playerBall, vBallPos)
				bBallFellThrough = FALSE
			ELIF bBallFellThrough
				CDEBUG1LN(DEBUG_GOLF,"It's too deep. End shot and fix ball location")
			ENDIF
		ENDIF
	ELSE
		thisHelpers.iNumTimesFallThroughWorldCheckFailed = 0
		thisHelpers.vLastSafeBallPosition = GET_ENTITY_COORDS(playerBall)
	ENDIF
	
	RETURN bBallFellThrough
ENDFUNC

PROC RESOLVE_FALL_THROUGH_WORLD(OBJECT_INDEX playerBall, GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, BOOL bAllowCupProbe = FALSE)
	CDEBUG1LN(DEBUG_GOLF,"Ball fell through world!")
	GOLF_LIE_TYPE findLie = LIE_ROUGH
	VECTOR vRetNormal
	VECTOR vBallPos = thisHelpers.vLastSafeBallPosition 
	FLOAT fHeight
	
	IF IS_VECTOR_ZERO(vBallPos)
		vBallPos = GET_ENTITY_COORDS(playerBall, FALSE)
	ENDIF
	
	IF GET_GROUND_Z_AND_NORMAL_FOR_3D_COORD(vBallPos+<<0,0,1.0>>, fHeight, vRetNormal)
		vBallPos.z = fHeight
		SET_ENTITY_COORDS_NO_OFFSET(playerBall, vBallPos + <<0,0,0.05>>)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome, -vRetNormal)
	ELSE
		SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome, <<0,0,-1.0>>)
	ENDIF
	GET_GOLF_LIE_FROM_WORLD_POSITION(vBallPos, findLie, GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome), playerBall)
	IF findLie = LIE_CUP //accedently probed cup, probe different area
	AND NOT bAllowCupProbe
		findLie = LIE_GREEN
	ENDIF
	SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, vBallPos, findLie)
ENDPROC

#ENDIF

PROC REDUCE_GOLF_BALL_SPIN(SWING_METER &swingMeter)
	//CDEBUG1LN(DEBUG_GOLF,"Current Spin ", swingMeter.fXSpin, " ", swingMeter.fYSpin )
	IF swingMeter.fXSpin = 0 AND swingMeter.fYSpin = 0
		EXIT
	ENDIF
	
	VECTOR vSpinDir = NORMALISE_VECTOR(<<swingMeter.fXSpin, swingMeter.fYSpin, 0.0>>)*SPIN_MULTIPLIER_GROUND_DURATTION*GET_FRAME_TIME()
	VECTOR vOpostiveDir = -vSpinDir
	
	IF ABSF(swingMeter.fXSpin) < ABSF(vSpinDir.x)
		swingMeter.fXSpin = 0
	ELSE
		swingMeter.fXSpin += vOpostiveDir.x
	ENDIF
	
	IF ABSF(swingMeter.fYSpin) < ABSF(vSpinDir.Y)
		swingMeter.fYSpin = 0
	ELSE
		swingMeter.fYSpin += vOpostiveDir.y
	ENDIF
	
ENDPROC

PROC GOLF_APPLY_GIMMIE_FORCE(OBJECT_INDEX playerBall, VECTOR ballPos, VECTOR pinPos)
	VECTOR vAttractorForce  = NORMALISE_VECTOR(<<pinPos.x, pinPos.y, 0>> - <<ballPos.x, ballPos.y, 0>>)*9.0
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_GOLF,"Apply gimmie force, ", GET_FRAME_COUNT())
		CDEBUG1LN(DEBUG_GOLF,"Distance to cup ", VDIST2(ballPos, pinPos))
		CDEBUG1LN(DEBUG_GOLF,"Ball Pos ", ballPos)
		CDEBUG1LN(DEBUG_GOLF,"Pin Pos ", pinPos)
	#ENDIF
	
//	VECTOR vNewpos = ballPos + vAttractorForce*GET_FRAME_TIME()
//	GOLF_FIND_GROUND_Z(vNewpos)	
//	SET_ENTITY_COORDS(playerBall, vNewpos+<<0,0, 0.025>)
	
	//SET_ENTITY_VELOCITY(playerBall, vAttractorForce)
	APPLY_FORCE_TO_ENTITY(playerBall, APPLY_TYPE_FORCE, vAttractorForce, <<0,0,0>>, 0, FALSE, FALSE, TRUE)
ENDPROC

VECTOR vBallVelocityLastFrame

/// PURPOSE:
///    Manages the golf ball in flight, and applies wind and spin forces to the ball. Determines when the ball has landed and is at rest
/// PARAMS:
///    thisGame - 
///    thisCourse - 
///    thisFoursome - 
PROC GOLF_MANAGE_BALL_IN_FLIGHT(GOLF_GAME &thisGame,GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome #IF NOT GOLF_IS_AI_ONLY , GOLF_HELPERS & thisHelpers #ENDIF)
	OBJECT_INDEX playerBall = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)
	OBJECT_INDEX currentFlag =  GET_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
	VECTOR vBallVelocity = GET_ENTITY_VELOCITY( playerBall)
	FLOAT ballVel = VMAG( vBallVelocity )
	FLOAT fHeight
	
	#IF NOT GOLF_IS_AI_ONLY
	GOLF_LIE_TYPE findLie = LIE_ROUGH
	#ENDIF
	
	VECTOR vSpinMeter = <<thisFoursome.swingMeter.fXSpin, thisFoursome.swingMeter.fYSpin, 0>>
	FLOAT swingHeading = GET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome)
	VECTOR vPlayersAim = NORMALISE_VECTOR( <<COS(swingHeading), SIN(swingHeading), 0>>)
	VECTOR vPlayerAimPerp = ROTATE_VECTOR_ABOUT_Z_ORTHO(vPlayersAim, ROTSTEP_90)
	
	VECTOR	vSpinForce = vPlayersAim*-vSpinMeter.x*SPIN_MULTIPLIER_V_AIR + vPlayerAimPerp*vSpinMeter.y*SPIN_MULTIPLIER_H_AIR
	
	DRAW_DEBUG_LINE(GET_ENTITY_COORDS(playerBall), GET_ENTITY_COORDS(playerBall) + NORMALISE_VECTOR(vSpinForce), 0, 0, 0)
//	CDEBUG1LN(DEBUG_GOLF, "Spin force :", vSpinForce)
//	CDEBUG1LN(DEBUG_GOLF, "Spin meter :", vSpinMeter)
	
	IF GOLF_HANDLE_BALL_IN_WATER(thisGame, thisCourse, thisFoursome #IF NOT GOLF_IS_AI_ONLY , thisHelpers #ENDIF)
		
		EXIT
	ENDIF
	
	#IF NOT GOLF_IS_AI_ONLY
	#IF IS_DEBUG_BUILD
//		GET_MAX_BALL_HEIGHT(thisFoursome)
		
		IF bHitOpponentBall
			INT opponetIndex = PICK_INT(GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)= 0, 1, 0)
			OBJECT_INDEX opponentBall = GET_GOLF_FOURSOME_INDEXED_PLAYER_BALL(thisFoursome, opponetIndex)
			
			IF DOES_ENTITY_EXIST(opponentBall)
				CDEBUG1LN(DEBUG_GOLF,"moving to ball")
				APPLY_FORCE_TO_ENTITY(playerBall, APPLY_TYPE_IMPULSE, NORMALISE_VECTOR(GET_ENTITY_COORDS(opponentBall) - GET_ENTITY_COORDS(playerBall)), <<0,0,0>>, 0, FALSe, FALSE, TRUE)
			ENDIF
		ENDIF
	#ENDIF
	#ENDIF
				
	#IF GOLF_USE_FALLTHROUGH_WORLD_CHECK 
		BOOl bBallFellThrough = BALL_FALL_THROUGH_WORLD_CHECK(playerBall, thisHelpers)
	#ENDIF
	
	// Spin/velocity checks happen every frame
	IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(playerBall)
		
		IF DOES_ENTITY_EXIST(currentFlag)
			IF IS_ENTITY_TOUCHING_ENTITY(playerBall, currentFlag) 
				IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_HIT_FLAG)
					CDEBUG1LN(DEBUG_GOLF,"Hit Flag")
					PLAY_SOUND_FROM_ENTITY(-1, "GOLF_BALL_IMPACT_FLAG_MASTER", GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
					SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_HIT_FLAG)
				ENDIF
				
				IF NOT IS_ENTITY_IN_AIR(playerBall)
					SET_ENTITY_COLLISION(currentFlag, FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		FLOAT fVelMultiplier = 1.0
		FLOAT fBounceMultiplier = 0.0
		FLOAT fSpinLiePower = 0.0
		FLOAT fBounceSpeedOverGround  = 0.0
		
		SWITCH GET_GOLF_LIE_TYPE_FROM_MATERIAL(GET_LAST_MATERIAL_HIT_BY_ENTITY(playerBall))
			CASE LIE_CART_PATH
				fVelMultiplier = VELOCITY_DAMPEN_CART_PATH
				fBounceMultiplier = CART_PATH_BOUNCE_MULTIPLIER
				fBounceSpeedOverGround = CART_BOUNCE_SPEED_OVER_GROUND
				fSpinLiePower = 0.30
			BREAK
			CASE LIE_ROUGH
			CASE LIE_BUSH
				fBounceMultiplier = ROUGH_BOUNCE_MULTIPLIER
				fVelMultiplier = VELOCITY_DAMPEN_ROUGH
				fBounceSpeedOverGround = ROUGH_BOUNCE_SPEED_OVER_GROUND
				fSpinLiePower = 0.15
			BREAK
			CASE LIE_BUNKER
				fVelMultiplier = VELOCITY_DAMPEN_BUNKER
				fBounceMultiplier = SAND_BOUNCE_MULTIPLIER
				fBounceSpeedOverGround = SAND_BOUNCE_SPEED_OVER_GROUND
				fSpinLiePower = 0.0
			BREAK
			CASE LIE_WATER
				fVelMultiplier = VELOCITY_DAMPEN_WATER
			BREAK
			CASE LIE_FAIRWAY
				fVelMultiplier = VELOCITY_DAMPEN_FAIRWAY
				fBounceMultiplier = FAIRWAY_BOUNCE_MULTIPLIER
				fBounceSpeedOverGround = FAIRWAY_BOUNCE_SPEED_OVER_GROUND
				fSpinLiePower = 0.30
			BREAK
			CASE LIE_GREEN
				fVelMultiplier = VELOCITY_DAMPEN_GREEN_APROACH
				fBounceMultiplier = GREEN_BOUNCE_MULTIPLIER
				fBounceSpeedOverGround = GREEN_BOUNCE_SPEED_OVER_GROUND
				fSpinLiePower = 1.2
			BREAK
			CASE LIE_UNKNOWN
				#IF NOT GOLF_IS_AI_ONLY
					CDEBUG1LN(DEBUG_GOLF,"UNKNOWN MATERIAL HIT!")
				#ENDIF
			BREAK
		ENDSWITCH
			
		// If we are touching something, apply contact spin effects, apply velocity dampening based on material, and apply spin dampening
		
		IF NOT bDisableSpinEffects
			//spin force on ground is different
			vSpinForce = vPlayersAim*-vSpinMeter.x*SPIN_MULTIPLIER_H_AIR + vPlayerAimPerp*vSpinMeter.y*SPIN_MULTIPLIER_H_AIR
			vSpinForce *= fSpinLiePower
			
			APPLY_FORCE_TO_ENTITY(playerBall, APPLY_TYPE_FORCE, vSpinForce, <<0,0,0>>, 0, FALSE, FALSE, TRUE)
		ENDIF
		REDUCE_GOLF_BALL_SPIN(thisFoursome.swingMeter)
		
		
		IF NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_BALL_JUST_LAND)
			//have ball bounce
			VECTOR vVelocityAfterBounce, vGroundNormal, vBounceVelocity, vVelocityAlongGround
			#IF NOT GOLF_IS_AI_ONLY
			CDEBUG1LN(DEBUG_GOLF,"Balls current velocity")
			#ENDIF
			IF fBounceMultiplier != 0 AND VMAG2(vBallVelocity) > BALL_VELOCITY_THRESHOLD_BOUNCE//GOLF_SHOULD_USE_BALL_BOUNCE(thisFoursome, thisGame)
				
				GET_GROUND_Z_AND_NORMAL_FOR_3D_COORD(GET_ENTITY_COORDS(playerBall)+<<0,0,1>>, fHeight, vGroundNormal)
//				vImpactNormal = GET_COLLISION_NORMAL_OF_LAST_HIT_FOR_ENTITY(playerBall)
				
				vBounceVelocity = DOT_PRODUCT(vBallVelocityLastFrame, vGroundNormal)*vGroundNormal
				
				vVelocityAfterBounce = -(1.0 + fBounceMultiplier)*vBounceVelocity + vBallVelocityLastFrame
				
				vVelocityAlongGround = vBallVelocityLastFrame - vBounceVelocity
				
				vVelocityAfterBounce -= vVelocityAlongGround * fBounceSpeedOverGround
				
				//APPLY_FORCE_TO_ENTITY(playerBall, APPLY_TYPE_IMPULSE, vBounceImpulse, <<0,0,0>>, 0, FALSE, FALSE, TRUE)
				SET_ENTITY_VELOCITY(playerBall, vVelocityAfterBounce)
				
				#IF NOT GOLF_IS_AI_ONLY
				CDEBUG1LN(DEBUG_GOLF, "Prev vel ", vBallVelocityLastFrame)
				CDEBUG1LN(DEBUG_GOLF, "Set velocity to ", vVelocityAfterBounce)
				CDEBUG1LN(DEBUG_GOLF, "Magnitude is ", VMAG(vVelocityAfterBounce))
				#ENDIF
							
				#IF NOT GOLF_IS_AI_ONLY
					CDEBUG1LN(DEBUG_GOLF,"Bounce impluse ", vVelocityAfterBounce)	
					DRAW_DEBUG_LINE(GET_ENTITY_COORDS(playerBall), GET_ENTITY_COORDS(playerBall) + vVelocityAfterBounce)
				#ENDIF
			ENDIF
		
		
			SET_GOLF_CONTROL_FLAG(thisGame, GCF_BALL_JUST_LAND) //the ball just hit the ground
			GOLF_PLAY_GROUND_IMPACT_SOUND(thisFoursome, FALSE, IS_BALL_LOW_IMPACT_SPEED(playerBall))
		ELSE
			VECTOR vDampenImpulse = GET_ENTITY_VELOCITY(playerBall)
			IF fVelMultiplier != 1.0
				vDampenImpulse *=  fVelMultiplier
				SET_ENTITY_VELOCITY(playerBall, vDampenImpulse) 
			ENDIF
		ENDIF
		
	ELSE	// If we are not touching something, apply wind and air spin effects
		
		IF IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_BALL_JUST_LAND)
			CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_BALL_JUST_LAND) //ball isn't hitting the ground anymore
		ENDIF
	
		IF NOT bDisableWindEffects AND NOT IS_GOLF_CURRENT_PLAYER_HARD_AI(thisFoursome)
			VECTOR	windForce = NORMALISE_VECTOR( <<thisGame.vWindDirection.x,thisGame.vWindDirection.y, 0.0>> ) * thisGame.fWindStrength * WIND_MULTIPLIER
			
			APPLY_FORCE_TO_ENTITY(playerBall, APPLY_TYPE_FORCE, windForce, <<0,0,0>>, 0, FALSE, FALSE, TRUE)
		ENDIF
		IF NOT bDisableSpinEffects
			APPLY_FORCE_TO_ENTITY(playerBall, APPLY_TYPE_FORCE, vSpinForce, <<0,0,0>>, 0, FALSE, FALSE, TRUE)
		ENDIF
		
		#IF NOT GOLF_IS_AI_ONLY //kind of expensive, only do for actual golf game.
		IF IS_GOLF_OBJECT_IN_FOLIAGE(playerBall)
			VECTOR vFoliageImpulse = GET_ENTITY_VELOCITY(playerBall)
			vFoliageImpulse.z = 0
			vFoliageImpulse *= -1 * (1 - VELOCITY_DAMPEN_FOLIAGE)
			APPLY_FORCE_TO_ENTITY(playerBall, APPLY_TYPE_FORCE, vFoliageImpulse, <<0,0,0>>, 0, FALSE, FALSE, TRUE)
			GOLF_PLAY_BALL_FOLIAGE_SOUND(thisFoursome)
			SET_GOLF_CONTROL_FLAG(thisGame, GCF_BALL_IN_FOLIAGE)
		ELSE
			CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_BALL_IN_FOLIAGE)
		ENDIF
		#ENDIF

	ENDIF
	
	// Spin rumble applied every frame
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
		CDEBUG1LN(DEBUG_GOLF,"Shake%% - 5")
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 50, IMAX (0, IMIN( 256, ABSI(ROUND(thisFoursome.swingMeter.fYSpin)) + ABSI(ROUND(thisFoursome.swingMeter.fXSpin)) + 30)))
	ENDIF
	
	#IF NOT GOLF_IS_AI_ONLY 
		IF IS_BALL_IN_HOLE(thisCourse, thisFoursome)
			GOLF_PLAY_BALL_IN_HOLE_SOUND(thisFoursome, thisCourse, thisGame, TRUE)
		ENDIF
	#ENDIF
	
	IF NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_FIRST_COLLISION)
		// Some things we only want to do the first time we hit something
		
		IF NOT IS_TIMER_STARTED(thisFoursome.physicsTimer) AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_FIRST_COLLISION)
			START_TIMER_NOW(thisFoursome.physicsTimer)
		ENDIF
	
		IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(playerBall) AND TIMER_DO_WHEN_READY(thisFoursome.physicsTimer, 0.2) //this way the ball does not count its intial location as a collision
		#IF GOLF_USE_FALLTHROUGH_WORLD_CHECK OR bBallFellThrough #ENDIF
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != COMPUTER
				CLEAR_HELP()
			ENDIF
			IF bDebugSpew DEBUG_MESSAGE("GMS_BALL_IN_FLIGHT - first collision, stop following, still need to track") ENDIF
			SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_CARRY(thisFoursome, VDIST(GET_ENTITY_COORDS(playerBall), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)))
			SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_FIRST_COLLISION)
			CANCEL_TIMER(thisFoursome.physicsTimer)

			GOLF_PLAY_GROUND_IMPACT_PTFX(thisFoursome)
			
			#IF GOLF_USE_FALLTHROUGH_WORLD_CHECK
				IF bBallFellThrough
					CDEBUG1LN(DEBUG_GOLF,"Ball fell through right away")
				ENDIF
			#ENDIF
			
		ENDIF
	ELIF IS_MAKE_PUTT_SET() AND NOT IS_BALL_IN_HOLE(thisCourse, thisFoursome)
		
		GOLF_APPLY_GIMMIE_FORCE(playerBall, GET_ENTITY_COORDS(playerBall), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome)))
			
	ELIF (ballVel < BALL_VELOCITY_THRESHOLD_NORMAL AND (ABSF(thisFoursome.swingMeter.fXSpin)+ABSF(thisFoursome.swingMeter.fYSpin)) < 0.05)
	OR (ballVel < BALL_VELOCITY_THRESHOLD_HOLE AND IS_BALL_IN_HOLE(thisCourse, thisFoursome))
	#IF NOT GOLF_IS_AI_ONLY 					  OR TIMER_DO_WHEN_READY(thisFoursome.physicsTimeout, 25) #ENDIF
	#IF GOLF_USE_FALLTHROUGH_WORLD_CHECK		  OR bBallFellThrough 									  #ENDIF
		// Check for slow-down velocity once we have made contact with something
		IF NOT IS_TIMER_STARTED(thisFoursome.physicsTimer)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, GET_ENTITY_COORDS(playerBall), GET_GOLF_LIE_TYPE_FROM_MATERIAL(GET_LAST_MATERIAL_HIT_BY_ENTITY(playerBall)))
			
			SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome, GET_COLLISION_NORMAL_OF_LAST_HIT_FOR_ENTITY(playerBall)) 
			#IF NOT GOLF_IS_AI_ONLY
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_AT_REST)
			#ENDIF

			START_TIMER_NOW(thisFoursome.physicsTimer)
			IF IS_BALL_IN_HOLE(thisCourse, thisFoursome)
				#IF NOT GOLF_IS_AI_ONLY
					GOLF_MANAGE_POST_SHOT_ANIMATIONS(thisGame, thisHelpers, thisCourse, thisFoursome)
				#ENDIF
			ENDIF
		ENDIF
//		
//		#IF NOT GOLF_IS_AI_ONLY
//			IF TIMER_DO_WHEN_READY(thisFoursome.physicsTimer, 0.75)
//				GOLF_MANAGE_POST_SHOT_ANIMATIONS(thisGame, thisHelpers, thisCourse, thisFoursome)
//			ENDIF
//		#ENDIF

		IF TIMER_DO_ONCE_WHEN_READY(thisFoursome.physicsTimer, 1.0)// OR IS_BALL_IN_HOLE(thisCourse, thisFoursome)
		#IF GOLF_USE_FALLTHROUGH_WORLD_CHECK OR bBallFellThrough #ENDIF
			SET_ENTITY_RECORDS_COLLISIONS(playerBall, FALSE)
			thisFoursome.swingMeter.fXSpin = 0.0
			thisFoursome.swingMeter.fYSpin = 0.0
			
			//CDEBUG1LN(DEBUG_GOLF,"SHOT OVER")
			IF IS_BALL_IN_HOLE(thisCourse, thisFoursome)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_HOLE_OVER)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_BALL_IN_HOLE_CELEBRATION)
				#IF NOT GOLF_IS_AI_ONLY
					SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_IN_HOLE)
				#ENDIF
			ELSE
				SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_BALL_AT_REST)
				IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
					CDEBUG1LN(DEBUG_GOLF,"Shake%% - 6")
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 10, 10)
				ENDIF
				#IF NOT GOLF_IS_AI_ONLY
					IF GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(playerBall), fHeight) //ball didnt fall through world
						GET_GOLF_LIE_FROM_WORLD_POSITION(GET_ENTITY_COORDS(playerBall)+<<0.0, 0.0,0.1>>, findLie, GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome), playerBall)
						SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, GET_ENTITY_COORDS(playerBall), findLie)
						CDEBUG1LN(DEBUG_GOLF,"Found lie after shot is ", findLie)
					ENDIF
					
					VECTOR vGroundNormal = GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome)
					IF VMAG2(vGroundNormal) = 0.0 OR ABSF(vGroundNormal.z) < 0.9 //something went wrong when getting the ground normal, set to default
						SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome, <<0,0,-1.0>>)
					ENDIF
				#ENDIF
			ENDIF
			SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_AT_REST)
			#IF GOLF_USE_FALLTHROUGH_WORLD_CHECK
				IF bBallFellThrough
					RESOLVE_FALL_THROUGH_WORLD(playerBall, thisFoursome, thisHelpers)
				ENDIF
			#ENDIF
			
			FREEZE_GOLF_BALL(playerBall, TRUE)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_SHOT_OVER)
			IF WAS_TEE_SHOT(thisCourse, thisFoursome)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_DRIVE_CARRY)
			ENDIF
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(thisFoursome.physicsTimer)
			CANCEL_TIMER(thisFoursome.physicsTimer)
		ENDIF
	ENDIF
	
	#IF GOLF_IS_AI_ONLY
		IF TIMER_DO_ONCE_WHEN_READY(thisFoursome.physicsTimeout, 14)
		AND NOT IS_MAKE_PUTT_SET()

			SET_ENTITY_RECORDS_COLLISIONS(playerBall, FALSE)
			FREEZE_GOLF_BALL(playerBall, TRUE)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_BALL_AT_REST)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_AT_REST | GOLF_BALL_NEEDS_SNAP)
			
			IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_PUTTING_GREEN_AI)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, <<0,0,0>>, LIE_GREEN)
			ELSE
				SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, <<0,0,0>>, LIE_ROUGH)
			ENDIF
		ENDIF
	#ENDIF
	
	IF DOES_ENTITY_EXIST(playerBall)
		vBallVelocityLastFrame = GET_ENTITY_VELOCITY(playerBall)
	ENDIF
	
	CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_BALL_JUST_HIT)
ENDPROC

/// PURPOSE:
///    Manages a putted ball on the green and determines when the ball is at rest
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
PROC GOLF_MANAGE_BALL_ON_GREEN(GOLF_GAME &thisGame, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome #IF NOT GOLF_IS_AI_ONLY , GOLF_HELPERS &thisHelpers#ENDIF)
	OBJECT_INDEX playerBall = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)
	VECTOR vBallVelocity = GET_ENTITY_VELOCITY( playerBall)
	FLOAT  fBallSpeed = VMAG(vBallVelocity)
	VECTOR pinPos = GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
	VECTOR ballPos = GET_ENTITY_COORDS(playerBall)
	//BOOL isBallNearHole = VDIST2(ballPos,pinPos) < (DIST_FOR_GIMMIE*DIST_FOR_GIMMIE)
	SET_DAMPING(playerBall, PHYSICS_DAMPING_LINEAR_C ,   VELOCITY_DAMPEN_GREEN_C)
	SET_DAMPING(playerBall, PHYSICS_DAMPING_ANGULAR_C ,  VELOCITY_DAMPEN_GREEN_C)
	OBJECT_INDEX currentFlag =  GET_GOLF_HOLE_FLAG(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
		
	UNUSED_PARAMETER(thisGame)
	#IF NOT GOLF_IS_AI_ONLY
		FLOAT fHieght
		VECTOR vRetNormal
		GOLF_LIE_TYPE findLie = LIE_GREEN
	#ENDIF
	
	IF GOLF_HANDLE_BALL_IN_WATER(thisGame, thisCourse, thisFoursome #IF NOT GOLF_IS_AI_ONLY , thisHelpers #ENDIF)
		EXIT
	ENDIF
		
	#IF GOLF_USE_FALLTHROUGH_WORLD_CHECK 
		BOOl bBallFellThrough = BALL_FALL_THROUGH_WORLD_CHECK(playerBall, thisHelpers)
	#ENDIF
	
	#IF NOT GOLF_IS_AI_ONLY 
		IF IS_BALL_IN_HOLE(thisCourse, thisFoursome)
			GOLF_PLAY_BALL_IN_HOLE_SOUND(thisFoursome, thisCourse, thisGame, TRUE)
		ENDIF
		
		CDEBUG1LN(DEBUG_GOLF, "Ball velocity z ", vBallVelocity.z, " Last frame z ", vBallVelocityLastFrame.z)
		IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_FIRST_COLLISION)
			IF vBallVelocity.z > 0.25 //ball is moving up a lot
				IF vBallVelocity.z - vBallVelocityLastFrame.z > 0.3 //ball is gaining velocity a large amount of velocity in one frame
					IF  VDIST(<<ballPos.x, ballPos.y, 0>>, <<pinPos.x, pinPos.y, 0>>) > (GIMMIE_FORCE_RADIUS-0.05) //you are not close to hole, so you didn't hit the rim
						CDEBUG1LN(DEBUG_GOLF, "bug in physics where the ball pops up randomly, stop the madness")
						vBallVelocity.z = 0
						SET_ENTITY_VELOCITY(playerBall, vBallVelocity)
						GOLF_FIND_GROUND_Z(ballPos)
						SET_ENTITY_COORDS_NO_OFFSET(playerBall, ballPos + <<0,0,0.05>>)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		vBallVelocityLastFrame = GET_ENTITY_VELOCITY(playerBall)
	#ENDIF
	
	thisFoursome.swingMeter.fXSpin = 0.0
	thisFoursome.swingMeter.fYSpin = 0.0
	
	IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(playerBall)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, GET_ENTITY_COORDS(playerBall), GET_GOLF_LIE_TYPE_FROM_MATERIAL(GET_LAST_MATERIAL_HIT_BY_ENTITY(playerBall)))
		SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_FIRST_COLLISION)
		CLEAR_GOLF_CONTROL_FLAG(thisGame, GCF_BALL_POP_SOUND)
		
		IF DOES_ENTITY_EXIST(currentFlag)
			IF IS_ENTITY_TOUCHING_ENTITY(playerBall, currentFlag) 
			AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_HIT_FLAG)
				CDEBUG1LN(DEBUG_GOLF, "Hit Flag")
				PLAY_SOUND_FROM_ENTITY(-1, "GOLF_BALL_IMPACT_FLAG_MASTER", GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
				
				SET_ENTITY_COLLISION(currentFlag, FALSE)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_HIT_FLAG)
			ENDIF
		ENDIF
	ELSE
		//When putting did the golf ball pop up near the hole?
		IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_BALL_FIRST_COLLISION)
		AND IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
			IF NOT IS_GOLF_CONTROL_FLAG_SET(thisGame, GCF_BALL_POP_SOUND)
			AND VDIST2(ballPos, pinPos) < (0.3*0.3)
				PLAY_SOUND_FROM_ENTITY(-1, "GOLF_BALL_CUP_MISS_MASTER", GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
				SET_GOLF_CONTROL_FLAG(thisGame, GCF_BALL_POP_SOUND)
			ENDIF
		ENDIF
	ENDIF
	
	IF (fBallSpeed < BALL_VELOCITY_THRESHOLD_GREEN)
	OR (fBallSpeed < BALL_VELOCITY_THRESHOLD_HOLE AND IS_BALL_IN_HOLE(thisCourse, thisFoursome))
	OR IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_SHOT_IS_GIMMIE)
	#IF GOLF_USE_FALLTHROUGH_WORLD_CHECK OR bBallFellThrough #ENDIF  OR IS_MAKE_PUTT_SET()
		
		#IF GOLF_USE_FALLTHROUGH_WORLD_CHECK
			IF bBallFellThrough
				RESOLVE_FALL_THROUGH_WORLD(playerBall, thisFoursome, thisHelpers, IS_BALL_IN_HOLE(thisCourse, thisFoursome))
			ENDIF
		#ENDIF
		
		IF NOT IS_TIMER_STARTED(thisFoursome.physicsTimer)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, GET_ENTITY_COORDS(playerBall), GET_GOLF_LIE_TYPE_FROM_MATERIAL(GET_LAST_MATERIAL_HIT_BY_ENTITY(playerBall)))
			SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome, GET_COLLISION_NORMAL_OF_LAST_HIT_FOR_ENTITY(playerBall))
			
			#IF NOT GOLF_IS_AI_ONLY
				SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_AT_REST)
			#ENDIF
			
			IF IS_BALL_IN_HOLE(thisCourse, thisFoursome)
				#IF NOT GOLF_IS_AI_ONLY
					GOLF_MANAGE_POST_SHOT_ANIMATIONS(thisGame, thisHelpers, thisCourse, thisFoursome)
				#ENDIF
			ENDIF
			
			START_TIMER_NOW(thisFoursome.physicsTimer)
		ENDIF
		
		IF NOT IS_BALL_IN_HOLE(thisCourse, thisFoursome)  AND  //You are not in the hole AND
		(IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_SHOT_IS_GIMMIE)  //the shot is a gimmie OR
		OR VDIST2(ballPos, pinPos) < (GIMMIE_FORCE_RADIUS*GIMMIE_FORCE_RADIUS)//so close it is hanging off the edge, give it a nudge
		OR IS_MAKE_PUTT_SET() //make putt flag is set
		OR IS_SHOT_AN_AI_GIMME(thisCourse, thisFoursome))
		#IF GOLF_IS_AI_ONLY AND FALSE #ENDIF
		
			GOLF_APPLY_GIMMIE_FORCE(playerBall, ballPos, pinPos)
			
			SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_SHOT_IS_GIMMIE)
			#IF NOT GOLF_IS_AI_ONLY
				IF NOT IS_MAKE_PUTT_SET()
					GOLF_MANAGE_POST_SHOT_ANIMATIONS(thisGame, thisHelpers, thisCourse, thisFoursome)
				ENDIF
			#ENDIF
		ELIF TIMER_DO_ONCE_WHEN_READY(thisFoursome.physicsTimer, 0.5)
			IF bDebugSpew DEBUG_MESSAGE("SHOT OVER 2") ENDIF
			SET_ENTITY_RECORDS_COLLISIONS(playerBall, FALSE)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_SHOT_OVER)
			thisFoursome.swingMeter.fXSpin = 0.0
			thisFoursome.swingMeter.fYSpin = 0.0
			
			#IF NOT GOLF_IS_AI_ONLY
				GOLF_MANAGE_POST_SHOT_ANIMATIONS(thisGame, thisHelpers, thisCourse, thisFoursome)
			#ENDIF
			IF IS_BALL_IN_HOLE(thisCourse, thisFoursome)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_HOLE_OVER)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_BALL_IN_HOLE_CELEBRATION)
				FREEZE_GOLF_BALL(playerBall, FALSE)

				#IF NOT GOLF_IS_AI_ONLY
					SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_IN_HOLE)
				#ENDIF
			ELSE
				#IF NOT GOLF_IS_AI_ONLY
					GET_GOLF_LIE_FROM_WORLD_POSITION(GET_ENTITY_COORDS(playerBall)+<<0.0, 0.0,0.1>>, findLie, GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome), playerBall)
					
					IF NOT GET_GROUND_Z_AND_NORMAL_FOR_3D_COORD(GET_ENTITY_COORDS(playerBall)+<<0.05, 0.05,0.1>>, fHieght, vRetNormal)
						vRetNormal = <<0, 0, 1.0>>
					ENDIF
					
					IF findLie = LIE_CUP //accedently probed cup, probe different area
						IF NOT GET_GROUND_Z_AND_NORMAL_FOR_3D_COORD(GET_ENTITY_COORDS(playerBall)-<<0.05, 0.05,0.1>>, fHieght, vRetNormal)
							vRetNormal = <<0, 0, 1.0>>
						ENDIF
						findLie = LIE_GREEN
					ENDIF
					
					CDEBUG1LN(DEBUG_GOLF,"Setting lie normal on green to ", -vRetNormal)
					SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome, -vRetNormal)
					CDEBUG1LN(DEBUG_GOLF,"Setting lie on green to ", findLie)
					SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, GET_ENTITY_COORDS(playerBall), findLie)
				#ENDIF
				
				FREEZE_GOLF_BALL(playerBall, TRUE)
				SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_BALL_AT_REST)
			ENDIF
			SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_AT_REST)
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(thisFoursome.physicsTimer)
			CANCEL_TIMER(thisFoursome.physicsTimer)
		ENDIF
	ENDIF
	IF TIMER_DO_ONCE_WHEN_READY(thisFoursome.physicsTimeout, 30)
	AND NOT IS_MAKE_PUTT_SET()
		SET_ENTITY_COORDS(playerBall, GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_ESTIMATE(thisFoursome))
		SET_ENTITY_RECORDS_COLLISIONS(playerBall, FALSE)
		FREEZE_GOLF_BALL(playerBall, TRUE)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome,  GPS_BALL_AT_REST)
		SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_AT_REST | GOLF_BALL_NEEDS_SNAP)
		CDEBUG1LN(DEBUG_GOLF,"We timed out, resetting golf ball to shot estimate and continuing")
	ENDIF
ENDPROC


/// PURPOSE:
///    Resets a golf ball (ie destroys and recreates) that is at rest
/// PARAMS:
///    thisCourse - 
///    thisFoursome - 
PROC GOLF_RESET_BALL_AT_REST(GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)
	
	IF NOT bDrivingRangeMode
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != COMPUTER
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF
		SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome, GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome)))
		SET_GOLF_FOURSOME_CURRENT_PLAYER_DISTANCE_TO_HOLE(thisFoursome, VDIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))))
	ELSE
		SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)+<<0.0,0.0,0.05>>)
		RESET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)
		RESET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome)
	ENDIF
	SET_ENTITY_MAX_SPEED(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), 150.0)
	FREEZE_GOLF_BALL( GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), TRUE)
	CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_FIRST_COLLISION)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_CARRY(thisFoursome, 0.0)
ENDPROC


PROC GOLF_RESET_BALL_AT_REST_HAZARD(GOLF_FOURSOME &thisFoursome)

	//SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))
	//CLEANUP_GOLF_BALL(thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)]) //I will remove the ball later when I can guarantee the camera isn't on it
	RESET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)
	RESET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome)
	CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_FIRST_COLLISION)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_NEEDS_SNAP)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_CARRY(thisFoursome, 0.0)

	SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_IS_OOB)
ENDPROC

FUNC BOOL GOLF_FIND_SAFE_SPOT_TO_DROP_BALL(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, VECTOR startPos, INT calls = 0)

	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE(startPos, 0.5, 255, 0, 0)
		CDEBUG1LN(DEBUG_GOLF,"Checking safe to drop call number ", calls, " at ", startPos)
	#ENDIF
	//shot is somewhere crazy, probably under the world, too many calls avoid stack overflow
	IF (calls > 2) OR IS_GOLF_COORD_OUTSIDE_BOUNDARY_PLANES(startPos)
		CDEBUG1LN(DEBUG_GOLF,"Call greater then one, or point outside boundry plane")
		RETURN FALSE
	ENDIF

	VECTOR pinPos = GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
	VECTOR lieNormal = <<0,0,-1>>
	FLOAT degree
	GOLF_LIE_TYPE findLie = LIE_ROUGH

	IF VDIST2(startPos, pinPos) > (DIST_TO_GREEN_HACK*DIST_TO_GREEN_HACK)
		degree = 3
	ELSE
		degree = 4
	ENDIF
	
	//rotate to the fairway
    IF IS_GOLF_COORD_ON_LEFT_OF_FAIRWAY(startPos, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
		degree = -degree
	ENDIF
	
//	//Move the ball back a little bit, this stops the ball from bouncing back and forth between two spots.
//	startPos -= NORMALISE_VECTOR(pinPos - startPos) * 0.5
//	
	//translate to new position
	
	//translate off the cart path or bush
	startPos = ROTATE_VECTOR_ABOUT_Z(startPos - pinPos, degree)
	startPos += pinPos
	GET_GROUND_Z_AND_NORMAL_FOR_3D_COORD(startPos + <<0,0,4>>, startPos.z, lieNormal)
		
	GET_GOLF_LIE_FROM_WORLD_POSITION(startPos, findLie, LIE_UNKNOWN, GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
	
	//not a good spot, rotate again
	IF IS_GOLF_POSITION_RESTRICTIVE(thisCourse, thisFoursome, startPos)
	OR findLie = LIE_BUSH OR findLie = LIE_GREEN OR findLie = LIE_CART_PATH OR findLie = LIE_UNKNOWN
		 // move the ball to the pin a little bit so the ball doesn't skim across the harazard and ends up really far from the balls original location 
		startPos += 0.5*NORMALISE_VECTOR(pinPos-startPos)

		CDEBUG1LN(DEBUG_GOLF,"startPos ", startPos, " Lie ", findLie)
		//SCRIPT_ASSERT("Moved ball not in good position")
		RETURN GOLF_FIND_SAFE_SPOT_TO_DROP_BALL(thisFoursome, thisCourse, startPos, calls+1)
	ENDIF
	
	IF IS_GOLF_COORD_OUTSIDE_BOUNDARY_PLANES(startPos)
		RETURN FALSE
	ENDIF
	
	//Set all the data after finding a good spot
	CDEBUG1LN(DEBUG_GOLF,"Drop ball at safe location!")
	SET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome, startPos, findLie)
	SET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), startPos)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome, startPos)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_NORMAL(thisFoursome, -lieNormal)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_DISTANCE_TO_HOLE(thisFoursome, VDIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), GET_GOLF_HOLE_PIN_POSITION(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))))
	SET_ENTITY_MAX_SPEED(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), 150.0)
	FREEZE_GOLF_BALL( GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome), TRUE)
	CLEAR_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_FIRST_COLLISION)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG(thisFoursome, GOLF_BALL_NEEDS_SNAP)
	SET_GOLF_FOURSOME_CURRENT_PLAYER_SHOT_CARRY(thisFoursome, 0.0)
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SNAP_BALL_TO_GROUND(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers)
	CDEBUG1LN(DEBUG_GOLF,"GPS_ADDRESS_BALL: Attempt snap!")
	VECTOR vBallPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
	FLOAT fTempHeight
	
	IF GET_GROUND_Z_FOR_3D_COORD(vBallPos + <<0,0,1>>, fTempHeight)
		IF ABSF(vBallPos.z - fTempHeight) < 0.1
			CDEBUG1LN(DEBUG_GOLF,"Snap z from ", vBallPos.z, " to ", fTempHeight)
			vBallPos.z = fTempHeight
			SET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome, vBallPos)
			
			SET_GOLF_UI_FLAG(thisHelpers, GUC_REFRESH_ADDRESS | GUC_REFRESH_SWING)
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_GOLF,"Snap is too large ",  ABSF(vBallPos.z - fTempHeight))
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT FIND_MAX_METER_DISTANCE(GOLF_FOURSOME &thisFoursome, GOLF_BAG &golfBag)
	FLOAT fMaxRange = GET_GOLF_CLUB_PREDITED_CLUB_RANGE(golfBag, GET_GOLF_FOURSOME_CURRENT_PLAYER_CURRENT_CLUB(thisFoursome)) 
//	
//	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_APPROACH 
//	OR GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_PUNCH
//		fMaxRange *= GET_APPROACH_SHOT_PREDICTION_MODFIER()
//	ENDIF
	
	RETURN fMaxRange
ENDFUNC

/// PURPOSE:
///    Sets swing meter max distance based on the current club and shot style
/// PARAMS:
///    thisGame - 
///    thisCourse - 
///    thisFoursome - 
PROC SET_METER_MAX_DISTANCE(GOLF_GAME &thisGame ,GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)
	IF NOT IS_BALL_ON_GREEN(thisCourse, thisFoursome)
	AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
		thisFoursome.swingMeter.fMaxDistance = FIND_MAX_METER_DISTANCE(thisFoursome, thisGame.golfBag)
		#IF NOT GOLF_IS_AI_ONLY
			CDEBUG1LN(DEBUG_GOLF,"Calculated max distance is ", thisFoursome.swingMeter.fMaxDistance)
		#ENDIF
	ELSE
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_GREEN_SHORT
			thisFoursome.swingMeter.fMaxDistance = 5
		ELIF GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_GREEN
		OR GET_GOLF_FOURSOME_CURRENT_PLAYER_SWING_STYLE(thisFoursome) = SWING_STYLE_GREEN_TAP
			thisFoursome.swingMeter.fMaxDistance = 10
		ELSE
			thisFoursome.swingMeter.fMaxDistance = 20
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the current meter power goal based on a given distance
/// PARAMS:
///    swingMeter - 
///    thisPlayerCore - 
PROC SET_METER_CURRENT_DISTANCE(SWING_METER &swingMeter, GOLF_PLAYER_CORE &thisPlayerCore, GOLF_GAME &thisGame)
	FLOAT fPlayerDistance = GET_GOLF_PLAYER_DISTANCE_TO_HOLE(thisPlayerCore)
	FLOAT fMaxDist = swingMeter.fMaxDistance
	FLOAT fSetMetereGoal = (100 * fPlayerDistance / fMaxDist)
	
	eCLUB_TYPE eClub = GET_GOLF_CLUB_TYPE( thisGame.golfBag, GET_GOLF_PLAYER_CURRENT_CLUB(thisPlayerCore))
	
	//The wedges get weird
	IF  eClub = eCLUB_WEDGE_LOB OR eClub = eCLUB_WEDGE_SAND
	
		CDEBUG1LN(DEBUG_GOLF,"Setting default meter distance for wedges")
		CDEBUG1LN(DEBUG_GOLF,"Distance to hole is ", fPlayerDistance, " fSetMetereGoal ", fSetMetereGoal)

		FLOAT fMeterGoalModify, fInverseMetereModify, fScaleGoal, fMagicNumber
		
		fMeterGoalModify = (fPlayerDistance / fMaxDist)
		fInverseMetereModify = (1.0 - fMeterGoalModify) 
		
		IF eClub = eCLUB_WEDGE_SAND
			fScaleGoal = 2.5
		ELSE
			IF GET_GOLF_PLAYER_CONTROL(thisPlayerCore) != COMPUTER
				IF fSetMetereGoal > 40
					fScaleGoal = 1.5
				ELIF fSetMetereGoal > 30
					fScaleGoal = 2.0
				ELSE
					fScaleGoal = 2.5
				ENDIF
			ENDIF
		ENDIF
		
		fMagicNumber = fScaleGoal*fInverseMetereModify*fInverseMetereModify+1.0 // y = A*(x-1)^2+1
		
		fMeterGoalModify *= fMagicNumber
		
		fSetMetereGoal = fMeterGoalModify*100.0
			
	ELIF eClub = eCLUB_PUTTER AND GET_GOLF_PLAYER_CONTROL(thisPlayerCore) != COMPUTER
		//when putting, consider the players streangth when setting the meter goal
		fSetMetereGoal += GET_GOLF_PLAYER_STRENGTH()*10
	ENDIF
	
	#IF NOT GOLF_IS_AI_ONLY
		CDEBUG1LN(DEBUG_GOLF,"Players distance to hole, ", fPlayerDistance, " max distance ", fMaxDist, " setting meter goal to ", fSetMetereGoal)
	#ENDIF
	
	SET_GOLF_PLAYER_METER_GOAL(thisPlayerCore, fSetMetereGoal)
	IF GET_GOLF_PLAYER_METER_GOAL(thisPlayerCore, FALSE) > 100.0
		SET_GOLF_PLAYER_METER_GOAL(thisPlayerCore, 100.0)
	ELIF GET_GOLF_PLAYER_METER_GOAL(thisPlayerCore, FALSE) < 5.0
		SET_GOLF_PLAYER_METER_GOAL(thisPlayerCore, 5.0)
	ENDIF
ENDPROC

INT iCheckCount, iIntersectFailSafe
BOOL bInitCheckAim = TRUE

FUNC BOOL FIND_SAFE_SHOT_AIM(FLOAT InitialAim, GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse, GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers)
	
	FLOAT fCheckAim = InitialAim
	
	IF bInitCheckAim
		CDEBUG1LN(DEBUG_GOLF,"Initialize find safe spot ")
		bInitCheckAim = FALSE
		iCheckCount = 0
		iIntersectFailSafe = 0
			
		PICK_IDEAL_CLUB_AND_SHOT(thisGame, thisCourse,thisFoursome, thisHelpers, FALSE)
		SET_METER_MAX_DISTANCE(thisGame, thisCourse, thisFoursome)
		SET_METER_CURRENT_DISTANCE(thisFoursome.swingMeter, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], thisGame)
		SET_FOCUS_POS_AND_VEL(GOLF_FIND_SHOT_ESTIMATE(thisFoursome, thisGame.golfBag), <<0,0,0>>)
		
		CLEAR_GOLF_LINE_FLAGS()
		SET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome, fCheckAim)
	ENDIF
	
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
	OR IS_GOLF_COORD_IN_RANGE_OF_GREEN(thisCourse, thisFoursome, GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome))
	OR GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER
		bInitCheckAim = TRUE
		RETURN TRUE
	ENDIF
	
	FLOAT degree = 5.5
	BOOL bCheckLeft
	
	IF IS_GOLF_TRAIL_INTERSECT_FLAG_SET()
		iIntersectFailSafe = 0
		CDEBUG1LN(DEBUG_GOLF,"Interect is found, determine if line ends in safe spot")
		IF NOT IS_GOLF_LIE_SAFE(GET_GOLF_INTERSECT_LIE())
			//rotate to the fairway
			bCheckLeft = IS_GOLF_COORD_ON_LEFT_OF_FAIRWAY(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome), GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
			
			IF iCheckCount%2 = 1
				bCheckLeft = !bCheckLeft
			ENDIF
			degree *= (iCheckCount + 1)
			
		    IF bCheckLeft
				degree = -degree
			ENDIF
			
			fCheckAim -= degree
			iCheckCount++
			
			IF iCheckCount >= 7
				CDEBUG1LN(DEBUG_GOLF,"Can't find a good spot to aim to within a resonable change of angle.")
				fCheckAim = InitialAim
			ENDIF
			CDEBUG1LN(DEBUG_GOLF,"Check for angle ", fCheckAim)
			SET_GOLF_FOURSOME_CURRENT_PLAYER_AIM(thisFoursome, fCheckAim)
			CLEAR_GOLF_LINE_FLAGS()
			GOLF_SET_GOLF_TRAIL_EFFECT(thisGame, thisFoursome, thisHelpers)
			
			IF iCheckCount < 7
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_GOLF,"New line isn't found, continue calculating ", iIntersectFailSafe)
		GOLF_SET_GOLF_TRAIL_EFFECT(thisGame, thisFoursome, thisHelpers)
		
		IF iIntersectFailSafe <= 3
			iIntersectFailSafe++
			RETURN FALSE
		ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_GOLF,"Found line that ends in safe spot")
	//set back to default for next time
	SET_GOLF_INTERSECT_LIE(LIE_UNKNOWN)
	bInitCheckAim = TRUE
	RETURN TRUE
ENDFUNC
