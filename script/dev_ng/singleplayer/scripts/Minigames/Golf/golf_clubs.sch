USING "minigames_helpers.sch"


/// PURPOSE: Enumeration of all supported club types
ENUM eCLUB_TYPE
	eCLUB_INVALID,
	eCLUB_WOOD_1,	// Driver				// 7
	eCLUB_WOOD_2,	// Brassie				// 12
	eCLUB_WOOD_3,							// 15
	eCLUB_WOOD_4,							// 18
	eCLUB_WOOD_5,	// Baffing Spoon ??		// 21
	eCLUB_IRON_2,	// Cleek				// 17
	eCLUB_IRON_3,	// Mid Mashie			// 20
	eCLUB_IRON_4,	// Mashie Iron			// 23
	eCLUB_IRON_5,	// Mashie				// 26
	eCLUB_IRON_6,	// Spade Mashie			// 29
	eCLUB_IRON_7,	// Mashie Niblick		// 30
	eCLUB_IRON_8,	// Pitching Niblick		// 37
	eCLUB_IRON_9,	// Niblick				// 41	
	eCLUB_WEDGE_PITCH,						// 45
	eCLUB_WEDGE_APPROACH,					// 50
	eCLUB_WEDGE_SAND,						// 55
	eCLUB_WEDGE_LOB,						// 60
	eCLUB_WEDGE_LOB_ULTRA,					// 64
	eCLUB_PUTTER
ENDENUM

/// PURPOSE: Struct that represents all data needed to represent a single golf club
STRUCT CLUB_STRUCT
	eCLUB_TYPE	kClubType
#IF NOT COMPILE_LOOKUPS
	FLOAT		fClubHeadLoft // Angle
	FLOAT		fClubRange
	FLOAT		fClubAccuracyModifier
	FLOAT		fClubPowerModifier
	FLOAT		fClubSpinMagModifier
	MODEL_NAMES	oClubProp
	TEXT_LABEL_7 txtName
#ENDIF
ENDSTRUCT

/// PURPOSE: Struct for a golf bag. Currently there is a single global golf bag defined for all players. This will need to be moved into the player structs if we decide to allow the player to customize or upgrade clubs 
STRUCT GOLF_BAG
	CLUB_STRUCT			clubs[14]
	INT					iNumClubs
ENDSTRUCT

//*
#IF NOT COMPILE_LOOKUPS
VECTOR 		vClubAttachOffsetWood =  <<0,0,0>>
VECTOR 		vClubAttachRotWood =  <<0,0,0>>
VECTOR 		vClubAttachOffsetIron =  <<0,0,0>>
VECTOR 		vClubAttachRotIron =  <<0,0,0>>
VECTOR 		vClubAttachOffsetPutter =  <<0,0,0.03>>
VECTOR 		vClubAttachRotPutter = <<0,0,0>>
VECTOR		vAddressOffsetWood = <<0.11,-1.08,0>>
VECTOR		vAddressOffsetIron = <<0.11,-0.81,0>>
VECTOR 		vAddressOffsetPutter = <<0.10,-0.55,0>>
VECTOR		vAddressOffsetWedge = <<0.03, -0.75, 0>>
#ENDIF
//*/

/// PURPOSE:
///    Accessor for club type
FUNC eCLUB_TYPE GET_GOLF_CLUB_TYPE(GOLF_BAG &golfBag, INT clubIndex)
	IF clubIndex < 0 OR clubIndex >= golfBag.iNumClubs
		RETURN eCLUB_WOOD_1
	ENDIF
	RETURN golfBag.clubs[clubIndex].kClubType
ENDFUNC


/// PURPOSE:
///    Accessor for club loft
FUNC FLOAT GET_GOLF_CLUB_LOFT(GOLF_BAG &golfBag, INT clubIndex)
	IF clubIndex < 0 OR clubIndex >= golfBag.iNumClubs
		RETURN 0.0
	ENDIF
#IF NOT COMPILE_LOOKUPS
	RETURN golfBag.clubs[clubIndex].fClubHeadLoft
#ENDIF
#IF COMPILE_LOOKUPS
	SWITCH golfBag.clubs[clubIndex].kClubType
		CASE eCLUB_WOOD_1	// Driver				// 7
			RETURN 13.5
		BREAK
		CASE eCLUB_WOOD_2	// Brassie				// 12
			RETURN 13.75
		BREAK
		CASE eCLUB_WOOD_3							// 15
			RETURN 16.0
		BREAK
		CASE eCLUB_WOOD_4							// 18
			RETURN 18.0
		BREAK
		CASE eCLUB_WOOD_5	// Baffing Spoon ??		// 21
			RETURN 21.0
		BREAK
		CASE eCLUB_IRON_2	// Cleek				// 17
			RETURN 17.0
		BREAK
		CASE eCLUB_IRON_3	// Mid Mashie			// 20
			RETURN 20.0
		BREAK
		CASE eCLUB_IRON_4	// Mashie Iron			// 23
			RETURN 23.0
		BREAK
		CASE eCLUB_IRON_5	// Mashie				// 26
			RETURN 26.0
		BREAK
		CASE eCLUB_IRON_6	// Spade Mashie			// 29
			RETURN 29.0
		BREAK
		CASE eCLUB_IRON_7	// Mashie Niblick		// 30
			RETURN 30.0
		BREAK
		CASE eCLUB_IRON_8	// Pitching Niblick		// 37
			RETURN 37.0
		BREAK
		CASE eCLUB_IRON_9	// Niblick				// 41	
			RETURN 41.0
		BREAK
		CASE eCLUB_WEDGE_PITCH						// 45
			RETURN 45.0
		BREAK
		CASE eCLUB_WEDGE_APPROACH					// 50
			RETURN 50.0
		BREAK
		CASE eCLUB_WEDGE_SAND						// 55
			RETURN 55.0
		BREAK
		CASE eCLUB_WEDGE_LOB						// 60
			RETURN 60.0
		BREAK
		CASE eCLUB_WEDGE_LOB_ULTRA					// 64
			RETURN 64.0
		BREAK
		CASE eCLUB_PUTTER
			RETURN 5.0
		BREAK
	ENDSWITCH
	RETURN 0.0
#ENDIF
ENDFUNC	

/// PURPOSE:
///    Accessor for club range
FUNC FLOAT GET_GOLF_CLUB_RANGE(GOLF_BAG &golfBag, INT clubIndex)
	IF clubIndex < 0 OR clubIndex >= golfBag.iNumClubs
		RETURN 0.0
	ENDIF
#IF NOT COMPILE_LOOKUPS
	RETURN golfBag.clubs[clubIndex].fClubRange
#ENDIF
#IF COMPILE_LOOKUPS
	SWITCH golfBag.clubs[clubIndex].kClubType
		CASE eCLUB_WOOD_1	// Driver				// 7
			RETURN  220 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_WOOD_2	// Brassie				// 12
			RETURN  210 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_WOOD_3							// 15
			RETURN  200 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_WOOD_4							// 18
			RETURN  190 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_WOOD_5	// Baffing Spoon ??		// 21
			RETURN  180 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_IRON_2	// Cleek				// 17
			RETURN  180 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_IRON_3	// Mid Mashie			// 20
			RETURN  170 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_IRON_4	// Mashie Iron			// 23
			RETURN  160 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_IRON_5	// Mashie				// 26
			RETURN  150 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_IRON_6	// Spade Mashie			// 29
			RETURN  140 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_IRON_7	// Mashie Niblick		// 30
			RETURN  130 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_IRON_8	// Pitching Niblick		// 37
			RETURN  120 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_IRON_9	// Niblick				// 41	
			RETURN  110 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_WEDGE_PITCH						// 45
			RETURN  100 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_WEDGE_APPROACH					// 50
			RETURN  85 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_WEDGE_SAND						// 55
			RETURN  75 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_WEDGE_LOB						// 60
			RETURN  65 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_WEDGE_LOB_ULTRA					// 64
			RETURN  40 / MAGIC_DISTANCE_CONVERSION
		BREAK
		CASE eCLUB_PUTTER
			RETURN  30 / MAGIC_DISTANCE_CONVERSION
		BREAK
	ENDSWITCH
	RETURN 0.0
#ENDIF
ENDFUNC	

/// PURPOSE:
///    Accessor for club accuracy modifier
FUNC FLOAT GET_GOLF_CLUB_ACCURACY(GOLF_BAG &golfBag, INT clubIndex)
	IF clubIndex < 0 OR clubIndex >= golfBag.iNumClubs
		RETURN 0.0
	ENDIF
#IF NOT COMPILE_LOOKUPS
	RETURN golfBag.clubs[clubIndex].fClubAccuracyModifier
#ENDIF
#IF COMPILE_LOOKUPS
	SWITCH golfBag.clubs[clubIndex].kClubType
		CASE eCLUB_WOOD_1	// Driver				// 7
			RETURN  0.75
		BREAK
		CASE eCLUB_WOOD_2	// Brassie				// 12
			RETURN  0.75
		BREAK
		CASE eCLUB_WOOD_3							// 15
			RETURN  0.75
		BREAK
		CASE eCLUB_WOOD_4							// 18
			RETURN  0.75
		BREAK
		CASE eCLUB_WOOD_5	// Baffing Spoon ??		// 21
			RETURN  0.75
		BREAK
		CASE eCLUB_IRON_2	// Cleek				// 17
			RETURN  0.875
		BREAK
		CASE eCLUB_IRON_3	// Mid Mashie			// 20
			RETURN  0.875
		BREAK
		CASE eCLUB_IRON_4	// Mashie Iron			// 23
			RETURN  0.875
		BREAK
		CASE eCLUB_IRON_5	// Mashie				// 26
			RETURN  0.875
		BREAK
		CASE eCLUB_IRON_6	// Spade Mashie			// 29
			RETURN  0.875
		BREAK
		CASE eCLUB_IRON_7	// Mashie Niblick		// 30
			RETURN  0.875
		BREAK
		CASE eCLUB_IRON_8	// Pitching Niblick		// 37
			RETURN  0.875
		BREAK
		CASE eCLUB_IRON_9	// Niblick				// 41	
			RETURN  0.875
		BREAK
		CASE eCLUB_WEDGE_PITCH						// 45
			RETURN  1.0
		BREAK
		CASE eCLUB_WEDGE_APPROACH					// 50
			RETURN  1.0
		BREAK
		CASE eCLUB_WEDGE_SAND						// 55
			RETURN  1.0
		BREAK
		CASE eCLUB_WEDGE_LOB						// 60
			RETURN  1.0
		BREAK
		CASE eCLUB_WEDGE_LOB_ULTRA					// 64
			RETURN  1.0
		BREAK
		CASE eCLUB_PUTTER
			RETURN  1.0
		BREAK
	ENDSWITCH
	RETURN 0.0
#ENDIF
ENDFUNC	



/// PURPOSE:
///    Accessor for club spin modifier
FUNC FLOAT GET_GOLF_CLUB_SPIN(GOLF_BAG &golfBag, INT clubIndex)
	IF clubIndex < 0 OR clubIndex >= golfBag.iNumClubs
		RETURN 0.0
	ENDIF
#IF NOT COMPILE_LOOKUPS
	RETURN golfBag.clubs[clubIndex].fClubSpinMagModifier
#ENDIF
#IF COMPILE_LOOKUPS
	SWITCH golfBag.clubs[clubIndex].kClubType
		CASE eCLUB_WOOD_1	// Driver				// 7
			RETURN  0.25
		BREAK
		CASE eCLUB_WOOD_2	// Brassie				// 12
			RETURN  0.25
		BREAK
		CASE eCLUB_WOOD_3							// 15
			RETURN  0.25
		BREAK
		CASE eCLUB_WOOD_4							// 18
			RETURN  0.25
		BREAK
		CASE eCLUB_WOOD_5	// Baffing Spoon ??		// 21
			RETURN  0.25
		BREAK
		CASE eCLUB_IRON_2	// Cleek				// 17
			RETURN  0.5
		BREAK
		CASE eCLUB_IRON_3	// Mid Mashie			// 20
			RETURN  0.5
		BREAK
		CASE eCLUB_IRON_4	// Mashie Iron			// 23
			RETURN  0.5
		BREAK
		CASE eCLUB_IRON_5	// Mashie				// 26
			RETURN  0.6
		BREAK
		CASE eCLUB_IRON_6	// Spade Mashie			// 29
			RETURN  0.6
		BREAK
		CASE eCLUB_IRON_7	// Mashie Niblick		// 30
			RETURN  0.7
		BREAK
		CASE eCLUB_IRON_8	// Pitching Niblick		// 37
			RETURN  0.7
		BREAK
		CASE eCLUB_IRON_9	// Niblick				// 41	
			RETURN  0.8
		BREAK
		CASE eCLUB_WEDGE_PITCH						// 45
			RETURN  1.0
		BREAK
		CASE eCLUB_WEDGE_APPROACH					// 50
			RETURN  1.0
		BREAK
		CASE eCLUB_WEDGE_SAND						// 55
			RETURN  1.0
		BREAK
		CASE eCLUB_WEDGE_LOB						// 60
			RETURN  1.0
		BREAK
		CASE eCLUB_WEDGE_LOB_ULTRA					// 64
			RETURN  1.0
		BREAK
		CASE eCLUB_PUTTER
			RETURN  0.1
		BREAK
	ENDSWITCH
	RETURN 0.0
#ENDIF
ENDFUNC	

/// PURPOSE:
///    Accessor for club model name 
FUNC MODEL_NAMES GET_GOLF_CLUB_MODEL(GOLF_BAG &golfBag, INT clubIndex)
	IF clubIndex < 0 OR clubIndex >= golfBag.iNumClubs
		RETURN PROP_GOLF_DRIVER
	ENDIF
#IF NOT COMPILE_LOOKUPS
	RETURN golfBag.clubs[clubIndex].oClubProp
#ENDIF
#IF COMPILE_LOOKUPS
	SWITCH golfBag.clubs[clubIndex].kClubType
		CASE eCLUB_WOOD_1	// Driver				// 7
			RETURN  PROP_GOLF_WOOD_01
		BREAK
		CASE eCLUB_WOOD_2	// Brassie				// 12
			RETURN  PROP_GOLF_WOOD_01
		BREAK
		CASE eCLUB_WOOD_3							// 15
			RETURN  PROP_GOLF_WOOD_01
		BREAK
		CASE eCLUB_WOOD_4							// 18
			RETURN  PROP_GOLF_WOOD_01
		BREAK
		CASE eCLUB_WOOD_5	// Baffing Spoon ??		// 21
			RETURN  PROP_GOLF_WOOD_01
		BREAK
		CASE eCLUB_IRON_2	// Cleek				// 17
			RETURN  PROP_GOLF_IRON_01
		BREAK
		CASE eCLUB_IRON_3	// Mid Mashie			// 20
			RETURN  PROP_GOLF_IRON_01
		BREAK
		CASE eCLUB_IRON_4	// Mashie Iron			// 23
			RETURN  PROP_GOLF_IRON_01
		BREAK
		CASE eCLUB_IRON_5	// Mashie				// 26
			RETURN  PROP_GOLF_IRON_01
		BREAK
		CASE eCLUB_IRON_6	// Spade Mashie			// 29
			RETURN  PROP_GOLF_IRON_01
		BREAK
		CASE eCLUB_IRON_7	// Mashie Niblick		// 30
			RETURN  PROP_GOLF_IRON_01
		BREAK
		CASE eCLUB_IRON_8	// Pitching Niblick		// 37
			RETURN  PROP_GOLF_IRON_01
		BREAK
		CASE eCLUB_IRON_9	// Niblick				// 41	
			RETURN  PROP_GOLF_IRON_01
		BREAK
		CASE eCLUB_WEDGE_PITCH						// 45
			RETURN  Prop_Golf_Pitcher_01
		BREAK
		CASE eCLUB_WEDGE_APPROACH					// 50
			RETURN  Prop_Golf_Pitcher_01
		BREAK
		CASE eCLUB_WEDGE_SAND						// 55
			RETURN  Prop_Golf_Pitcher_01
		BREAK
		CASE eCLUB_WEDGE_LOB						// 60
			RETURN  Prop_Golf_Pitcher_01
		BREAK
		CASE eCLUB_WEDGE_LOB_ULTRA					// 64
			RETURN  Prop_Golf_Pitcher_01
		BREAK
		CASE eCLUB_PUTTER
			RETURN  PROP_GOLF_PUTTER_01
		BREAK
	ENDSWITCH
	RETURN PROP_GOLF_DRIVER
#ENDIF
ENDFUNC

FUNC FLOAT GET_APPROACH_SHOT_PREDICTION_MODFIER()
	RETURN 0.935
ENDFUNC
FUNC FLOAT GET_GOLF_CLUB_PREDITED_CLUB_RANGE(GOLF_BAG &golfBag, INT clubIndex)

	SWITCH golfBag.clubs[clubIndex].kClubType
		CASE eCLUB_WOOD_1
			RETURN 105.8 //predictWood1
		BREAK
		CASE eCLUB_WOOD_3
			RETURN 100.5 //predictWood3
		BREAK
		CASE eCLUB_WOOD_5
			RETURN 97.3 //predictWood5
		BREAK
		CASE eCLUB_IRON_3
			RETURN 90.2 //predictIron3
		BREAK
		CASE eCLUB_IRON_4
			RETURN 87.0 //predictIron4
		BREAK
		CASE eCLUB_IRON_5
			RETURN 83.0 //predictIron5
		BREAK
		CASE eCLUB_IRON_6
			RETURN 79.9 //predictIron6
		BREAK
		CASE eCLUB_IRON_7
			RETURN 73.5 //predictIron7
		BREAK
		CASE eCLUB_IRON_8
			RETURN 68.0 // predictIron8
		BREAK
		CASE eCLUB_IRON_9
			RETURN 59.1 //predictIron9
		BREAK
		CASE eCLUB_WEDGE_PITCH
			RETURN 52.5 //predictPitch
		BREAK
		CASE eCLUB_WEDGE_SAND
			RETURN 30.9 //predictSand
		BREAK
		CASE eCLUB_WEDGE_LOB
			RETURN 21.8 //predictLob
		BREAK
		DEFAULT
			PRINTLN("Club type ", golfBag.clubs[clubIndex].kClubType, " not valid club when looking for predicted club range")
			SCRIPT_ASSERT("Club type not valid club when looking for predicted club range")
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC	


FUNC TEXT_LABEL_23 GET_CLUB_STRING_FROM_ENUM(eCLUB_TYPE eClub)
	TEXT_LABEL_23 txtClub = "CLUB_"
	txtClub += ENUM_TO_INT(eClub)
	
	RETURN txtClub
ENDFUNC

/// PURPOSE:
///    Finds the index of a given club type within a given golf bag
/// PARAMS:
///    golfBag - Bag to search
///    clubType - Club type to find
/// RETURNS:
///    Index within the bag for the given club type
FUNC INT GOLF_FIND_SPECIFIC_CLUB_IN_BAG(GOLF_BAG &golfBag, eCLUB_TYPE clubType)
	INT clubIndex
	REPEAT golfBag.iNumClubs clubIndex
		IF golfBag.clubs[clubIndex].kClubType = clubType
			RETURN clubIndex
		ENDIF
	ENDREPEAT	
	RETURN -1
ENDFUNC


FUNC VECTOR GET_GOLF_CLUB_PLACEMENT_OFFSET(eCLUB_TYPE clubType)
	#IF GOLF_IS_MP
		//Shiity mp offsets to avoid clipping
		IF clubType <= eCLUB_WOOD_5
			RETURN <<0.15,-1.08,0>>
		ELIF clubType <= eCLUB_IRON_9
			RETURN <<0.16,-0.81,0>>
		ELIF clubType !=eCLUB_PUTTER
			RETURN <<0.03, -0.75, 0>>
		ELSE
			RETURN <<0.10,-0.55,0>>
		ENDIF
	#ENDIF
	
	#IF NOT GOLF_IS_MP
		//these numbers are fucking perfect. But we can't use them in mp because it is too close to the ball, 
		//and the remote peds clubs will clip into it.
		IF clubType <= eCLUB_WOOD_5
			RETURN <<0.11,-1.08,0>>
		ELIF clubType <= eCLUB_IRON_9
			RETURN <<0.11,-0.81,0>>
		ELIF clubType !=eCLUB_PUTTER
			RETURN <<0.03, -0.75, 0>>
		ELSE
			RETURN <<0.10,-0.55,0>>
		ENDIF
	#ENDIF

ENDFUNC

FUNC VECTOR GET_GOLF_CLUB_ATTACH_OFFSET(PED_INDEX pedIndex, eCLUB_TYPE clubType)
	UNUSED_PARAMETER(pedIndex)
	UNUSED_PARAMETER(clubType)
#IF COMPILE_LOOKUPS
	RETURN <<0,0,0>>
#ENDIF
#IF NOT COMPILE_LOOKUPS
	IF clubType <= eCLUB_WOOD_5
		RETURN vClubAttachOffsetWood
	ELIF clubType !=eCLUB_PUTTER
		RETURN vClubAttachOffsetIron
	ELSE
		RETURN vClubAttachOffsetPutter
	ENDIF
	
	pedIndex = pedIndex
	RETURN vClubAttachOffsetWood
#ENDIF
ENDFUNC

FUNC VECTOR GET_GOLF_CLUB_ATTACH_ROTATION(PED_INDEX pedIndex, eCLUB_TYPE clubType)
pedIndex = pedIndex
clubType = clubType
#IF COMPILE_LOOKUPS
	RETURN <<0,0,0>>
#ENDIF
#IF NOT COMPILE_LOOKUPS
	IF clubType <= eCLUB_WOOD_5
		RETURN vClubAttachRotWood
	ELIF clubType !=eCLUB_PUTTER
		RETURN vClubAttachRotIron
	ELSE
		RETURN vClubAttachRotPutter
	ENDIF
	RETURN vClubAttachRotWood
#ENDIF
ENDFUNC
