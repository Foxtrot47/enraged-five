USING "minigames_helpers.sch"
USING "golf_player.sch"
USING "golf_clubs.sch"

/// PURPOSE: Enumeration for state machine for the swing meter
ENUM SWING_METER_STATE
	SWING_METER_INACTIVE,
	SWING_METER_RISING,
	SWING_METER_FALLING_POWER,
	SWING_METER_FALLING,
	SWING_METER_FINISHED,
	SWING_METER_FALSE_START,
	SWING_METER_SHANK
ENDENUM

/// PURPOSE: Struct for running a swing meter. There is a single swing meter for every foursome (since only one player can be taking a shot at once
///    The Swing Meter is used no matter how the player is being controlled
STRUCT SWING_METER
	// These are used for all versions of the swing meter
	SWING_METER_STATE meterState
	FLOAT fMaxDistance

	// These are used for 3-click swings
	FLOAT fCurrentMeterPosition
	FLOAT fCurrentPowerRel
	FLOAT fCurrentAccurRel
	FLOAT fXSpin
	FLOAT fYSpin
	
	// These are used for stick swings
	INT   iLastX
	INT   iLastY
	INT	  iBackswingTime
	INT	  iBackswingHoldTime
	INT	  iBackswingStartTime
	INT   iBackswingDelta
	INT	  iBackswingX
ENDSTRUCT

TWEAK_FLOAT	SWING_METER_TIME_SWING  1.2
TWEAK_FLOAT	SWING_METER_TIME_PUTT   2.0

TWEAK_FLOAT	POWER_MAGIC_NUMBER_NORMAL 0.5575
TWEAK_FLOAT	POWER_MAGIC_NUMBER_POWER 0.5750
TWEAK_FLOAT	POWER_MAGIC_NUMBER_APPROACH 0.5075
TWEAK_FLOAT	POWER_MAGIC_NUMBER_PUNCH 0.558
TWEAK_FLOAT	POWER_MAGIC_NUMBER_GREEN 0.530
TWEAK_FLOAT	POWER_MAGIC_NUMBER_GREEN_LONG 0.790
TWEAK_FLOAT	POWER_MAGIC_NUMBER_GREEN_SHORT 0.360

TWEAK_FLOAT	POWER_MULTIPLIER_FAIRWAY 1.0
TWEAK_FLOAT	POWER_MULTIPLIER_ROUGH 0.95
TWEAK_FLOAT	POWER_MULTIPLIER_BUNKER 0.75
TWEAK_FLOAT	POWER_MULTIPLIER_GREEN 1.0

//TWEAK_FLOAT	SWING_METER_STICK_DEAD_ZONE_SIZE 5.0
TWEAK_FLOAT	SWING_METER_HEADING_MAX_DEVIATION  45.0
TWEAK_FLOAT	SWING_METER_HEADING_MAX_SHANK  90.0

TWEAK_FLOAT GOLF_TRAIL_MAX_DIST 66.25
TWEAK_FLOAT GOLF_TRAIL_MIN_DIST 0.1


/// PURPOSE:
///    Get magic number power multipliers for a shot style 
/// PARAMS:
///    swingStyle - 
/// RETURNS:
///    
FUNC FLOAT GET_SWING_STYLE_POWER_MULTIPLIER(SWING_STYLE swingStyle)
 	SWITCH swingStyle
		CASE  SWING_STYLE_NORMAL
			RETURN 1.0
		BREAK
		CASE  SWING_STYLE_APPROACH
			RETURN POWER_MAGIC_NUMBER_APPROACH / POWER_MAGIC_NUMBER_NORMAL
		BREAK
		CASE  SWING_STYLE_PUNCH
			RETURN POWER_MAGIC_NUMBER_PUNCH / POWER_MAGIC_NUMBER_NORMAL
		BREAK
		CASE SWING_STYLE_GREEN
			RETURN POWER_MAGIC_NUMBER_GREEN / POWER_MAGIC_NUMBER_NORMAL
		BREAK
		CASE SWING_STYLE_GREEN_TAP
		CASE SWING_STYLE_GREEN_SHORT
			RETURN POWER_MAGIC_NUMBER_GREEN_SHORT / POWER_MAGIC_NUMBER_NORMAL
		BREAK
		CASE SWING_STYLE_GREEN_LONG
			RETURN POWER_MAGIC_NUMBER_GREEN_LONG / POWER_MAGIC_NUMBER_NORMAL
		BREAK
		CASE SWING_STYLE_POWER
			RETURN POWER_MAGIC_NUMBER_POWER / POWER_MAGIC_NUMBER_NORMAL
		BREAK
	ENDSWITCH
	RETURN 1.0
ENDFUNC

/// PURPOSE:
/// 	Get magic number accuracy multipliers for a shot style   
/// PARAMS:
///    swingStyle - 
/// RETURNS:
///    
FUNC FLOAT GET_SWING_STYLE_ACCURACY_MULTIPLIER(SWING_STYLE swingStyle)
 	SWITCH swingStyle
		CASE  SWING_STYLE_NORMAL
			RETURN 0.75
		BREAK
		CASE  SWING_STYLE_APPROACH
			RETURN 1.0
		BREAK
		CASE  SWING_STYLE_PUNCH
			RETURN 1.0
		BREAK
		CASE SWING_STYLE_GREEN
		CASE SWING_STYLE_GREEN_SHORT
		CASE SWING_STYLE_GREEN_LONG
		CASE SWING_STYLE_GREEN_TAP
			RETURN 1.0
		BREAK
		CASE SWING_STYLE_POWER
			RETURN 0.5
		BREAK
	ENDSWITCH
	RETURN 1.0
ENDFUNC

/// PURPOSE:
///    Get swing accuracy range for a players current club
/// PARAMS:
///    thisPlayerCore - 
///    golfBag - 
/// RETURNS:
///    
FUNC FLOAT GET_SWING_ACCURACY_RANGE(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_BAG &golfBag)
	RETURN GET_GOLF_CLUB_RANGE(golfBag, GET_GOLF_PLAYER_CURRENT_CLUB(thisPlayerCore)) * GET_SWING_STYLE_ACCURACY_MULTIPLIER(GET_GOLF_PLAYER_SWING_STYLE(thisPlayerCore))
ENDFUNC

/// PURPOSE:
///    Clear the swing meter values between shots
/// PARAMS:
///    swingMeter - 
PROC GOLF_CLEAR_SWING_METER(SWING_METER &swingMeter)
	swingMeter.meterState = SWING_METER_INACTIVE
	swingMeter.fCurrentMeterPosition = 0
	swingMeter.fCurrentPowerRel = 0
	swingMeter.fCurrentAccurRel = 0
	swingMeter.fXSpin = 0
	swingMeter.fYSpin = 0
	swingMeter.iLastX = 0
	swingMeter.iLastY = 0
	swingMeter.iBackswingTime = 0
	swingMeter.iBackswingHoldTime = 0
	swingMeter.iBackswingStartTime = 0
	swingMeter.iBackswingDelta = 0
	swingMeter.iBackswingX = 0
ENDPROC

FUNC FLOAT GET_GOLF_LIE_MULTIPLIER(GOLF_LIE_TYPE lieType)
	SWITCH lieType
		CASE LIE_FAIRWAY
		CASE LIE_TEE
			RETURN POWER_MULTIPLIER_FAIRWAY
		BREAK
		CASE LIE_ROUGH
		CASE LIE_CART_PATH
			RETURN POWER_MULTIPLIER_ROUGH
		BREAK
		CASE LIE_BUNKER
		CASE LIE_WATER
		CASE LIE_UNKNOWN
			RETURN POWER_MULTIPLIER_BUNKER
		BREAK
		CASE LIE_GREEN
			RETURN POWER_MULTIPLIER_GREEN
		BREAK
		DEFAULT 
			RETURN POWER_MULTIPLIER_GREEN
		BREAK
	ENDSWITCH
	RETURN 1.0
ENDFUNC

/// PURPOSE:
///    Gets the current swing power from the swing meter
/// PARAMS:
///    thisPlayerCore - 
///    golfBag - 
///    swingMeter - 
///    bReal - 
///    bMax - 
/// RETURNS:
///    
FUNC FLOAT GET_SWING_POWER(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_BAG &golfBag, SWING_METER &swingMeter, BOOL bUsePowerAdd, BOOL bReal = TRUE, BOOL bMax = FALSE)
	FLOAT powerMeter = swingMeter.fCurrentPowerRel

	IF bMax
		powerMeter = 100
	ELIF NOT bReal // We need to use the red aim line, instead of the power
		powerMeter = GET_GOLF_PLAYER_METER_GOAL(thisPlayerCore, TRUE)
	ENDIF
	FLOAT magicNumber, powerAdd
	SWITCH GET_GOLF_PLAYER_SWING_STYLE(thisPlayerCore)
		CASE SWING_STYLE_NORMAL
			magicNumber = POWER_MAGIC_NUMBER_NORMAL 
			powerAdd = 0
		BREAK
		CASE SWING_STYLE_POWER
			magicNumber = POWER_MAGIC_NUMBER_POWER
			powerAdd = 15 * (1 - SQRT(powerMeter / 100.0))// Extra power at low power to account for differences in estimation and physics
		BREAK
		CASE SWING_STYLE_APPROACH
			magicNumber = POWER_MAGIC_NUMBER_APPROACH
			powerAdd = PICK_FLOAT(powerMeter > 25.0, 25.0, 15.0)
			powerAdd *= (1 - SQRT(powerMeter / 100.0))// Extra power at low power to account for differences in estimation and physics
		BREAK
		CASE SWING_STYLE_PUNCH
			magicNumber = POWER_MAGIC_NUMBER_PUNCH
			powerAdd = 15 * (1 - SQRT(powerMeter / 100.0)) // Extra power at low power to account for differences in estimation and physics
		BREAK
		
		//Putting needs to be really accurate, so accurate that I have to be very specific when modifying the power
		CASE SWING_STYLE_GREEN
			magicNumber = POWER_MAGIC_NUMBER_GREEN
			magicNumber *= (1+ (1-SQRT(powerMeter / 100)))

			IF powerMeter <= 63
				magicNumber *= 1.020
			ENDIF
			IF powerMeter <= 55
				magicNumber *= 1.040
			ENDIF
			IF powerMeter <= 46
				magicNumber *= 1.038
			ENDIF
			IF powerMeter <= 38
				magicNumber *= 1.040
			ENDIF
			IF powerMeter <= 34
				magicNumber *= 1.045
			ENDIF
			IF powerMeter <= 28
				magicNumber *= 1.045
			ENDIF
			
			powerAdd = 0.1
		BREAK
		CASE SWING_STYLE_GREEN_TAP
			RETURN -1.0
		BREAK
		CASE SWING_STYLE_GREEN_SHORT
			magicNumber = POWER_MAGIC_NUMBER_GREEN_SHORT
			magicNumber *= (1+ (1-SQRT(powerMeter / 100)))
			IF powerMeter <= 62
				magicNumber *= 1.035
			ELIF powerMeter <= 75
				magicNumber *= 1.015
			ENDIF
			
			IF powerMeter <= 49
				magicNumber *= 1.035
			ELIF powerMeter <= 58
				magicNumber *= 1.025
			ENDIF
			
			IF powerMeter <= 39
				magicNumber *= 1.075
			ELIF powerMeter <= 43
				magicNumber *= 1.055
			ENDIF
			IF powerMeter <= 30
				magicNumber *= 1.075
			ENDIF
			powerAdd = 0.1
		BREAK
		CASE SWING_STYLE_GREEN_LONG
			magicNumber = POWER_MAGIC_NUMBER_GREEN_LONG
			magicNumber *= (1+ (1-SQRT(powerMeter / 100)))
			
			IF powerMeter < 65
				magicNumber *= 1.015
			ENDIF
			
			IF powerMeter < 56
				magicNumber *= 1.010
			ENDIF
			
			IF powerMeter < 51
				magicNumber *= 1.020
			ENDIF
			
			IF powerMeter < 45
				magicNumber *= 1.020
			ENDIF
			
			IF powerMeter < 35
				magicNumber *= 1.070
			ENDIF
			
			IF powerMeter < 25
				magicNumber *= 1.100
			ENDIF
			
			powerAdd = 0.1
		BREAK
	ENDSWITCH
	
	IF NOT bUsePowerAdd
		powerAdd = 0
	ENDIF
	
	FLOAT ret = -1 * ((powerMeter / 100.0) * GET_GOLF_CLUB_RANGE(golfBag, GET_GOLF_PLAYER_CURRENT_CLUB(thisPlayerCore)) * GET_GOLF_LIE_MULTIPLIER(GET_GOLF_PLAYER_LIE(thisPlayerCore))* magicNumber + powerAdd)

	//PRINTLN("return ", ret)
	
	RETURN ret
ENDFUNC
