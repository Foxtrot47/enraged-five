
USING "golf.sch"
USING "golf_course_lib.sch"
USING "golf_clubs_lib.sch"
USING "golf_sync_scene.sch"

FUNC STRING GET_HOLE_PREVIEW_ANIM_DICT()
	RETURN "mini@golfhole_preview"
ENDFUNC

FUNC STRING GET_GOLF_GREEN_PREVIEW_CAM_ANIM_NAME(INT iHole)
	SWITCH iHole
		CASE 0
			RETURN "hole_01_cam"
		BREAK
		CASE 1
			RETURN "hole_02_cam"
		BREAK
		CASE 2
			RETURN "hole_03_cam"
		BREAK
		CASE 3
			RETURN "hole_04_cam"
		BREAK
		CASE 4
			RETURN "hole_05_cam"
		BREAK
		CASE 5
			RETURN "hole_06_cam"
		BREAK
		CASE 6
			RETURN "hole_07_cam"
		BREAK
		CASE 7
			RETURN "hole_08_cam"
		BREAK
		CASE 8
			RETURN "hole_09_cam"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_GOLF_IDLE_ANIMATION_DICTIONARY(BOOL bBase)
	IF bBase
		RETURN "amb@world_human_golf_player@male@base"
	ELSE
		RETURN "amb@world_human_golf_player@male@idle_a"
	ENDIF
ENDFUNC

FUNC STRING GET_GOLF_REACTION_ANIM_DICT(PED_INDEX pedGolfer, BOOL bIsPutting, BOOL bForceGeneric = FALSE)

	IF pedGolfer = PLAYER_PED_ID() AND NOT bForceGeneric
		IF GET_GOLF_PED_ENUM(pedGolfer) = CHAR_MICHAEL
			IF bIsPutting
				RETURN "mini@golfreactions@michael@putter"
			ELSE
				RETURN "mini@golfreactions@michael@clubs"
			ENDIF
		ELIF GET_GOLF_PED_ENUM(pedGolfer) = CHAR_FRANKLIN
			IF bIsPutting
				RETURN "mini@golfreactions@franklin@putter"
			ELSE
				RETURN "mini@golfreactions@franklin@clubs"
			ENDIF
		ELIF GET_GOLF_PED_ENUM(pedGolfer) = CHAR_TREVOR
			IF bIsPutting
				RETURN "mini@golfreactions@trevor@putter"
			ELSE
				RETURN "mini@golfreactions@trevor@clubs"
			ENDIF
		ENDIF
	ENDIF

	RETURN "mini@golfreactions@generic@"
ENDFUNC


PROC GET_GOLF_FACE_REACTION(BOOL bGeneric, BOOL bGood, BOOL bIsPutting, INT iAnimIndex, TEXT_LABEL_63 &sAnimDict, TEXT_LABEL_63 &txtReturnAnim)
	sAnimDict = ""
	txtReturnAnim = ""
		
	IF bGeneric
		sAnimDict = "mini@golf"
		txtReturnAnim += PICK_STRING(bIsPutting, "putt_react_", "swing_react_")
		txtReturnAnim += PICK_STRING(bGood, "good_0", "bad_0")
		txtReturnAnim += iAnimIndex
		txtReturnAnim += "_face"
		EXIT
	ENDIF
	
	IF GET_GOLF_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
		IF bIsPutting
			sAnimDict = "mini@golf"
			txtReturnAnim = "putt_react_"
			txtReturnAnim += PICK_STRING(bGood, "good_0", "bad_0")
			txtReturnAnim += iAnimIndex
			txtReturnAnim += "_face_player0"
			EXIT
		ELSE
			IF bGood
				sAnimDict = "mini@golfreactions@michael@clubs"
				txtReturnAnim = "react_win_club_0"
				txtReturnAnim += iAnimIndex
				txtReturnAnim += "_michael_facial"
				EXIT
			ELSE
				iAnimIndex = CLAMP_INT(iAnimIndex, 1, 2) //ony 2 variations of generic swing face anims
				sAnimDict = "mini@golf"
				txtReturnAnim = "swing_react_bad_0"
				txtReturnAnim += iAnimIndex
				txtReturnAnim += "_face_player0"
				EXIT
			ENDIF
		ENDIF
	ELIF GET_GOLF_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
		IF bIsPutting
			sAnimDict = "mini@golfreactions@franklin@putter"
			txtReturnAnim = "react_"
			txtReturnAnim += PICK_STRING(bGood, "win_putter_0", "lose_putter_0")
			txtReturnAnim += iAnimIndex
			txtReturnAnim += "_franklin_frank_facial"
			EXIT
		ELSE
			IF bGood
				sAnimDict = "mini@golfreactions@franklin@clubs"
				txtReturnAnim = "react_win_club_0"
				txtReturnAnim += iAnimIndex
				txtReturnAnim += "_franklin_facial"
				EXIT
			ELSE
				iAnimIndex = CLAMP_INT(iAnimIndex, 1, 2) //ony 2 variations of generic swing face anims
				sAnimDict = "mini@golf"
				txtReturnAnim = "swing_react_bad_0"
				txtReturnAnim += iAnimIndex
				txtReturnAnim += "_face_player1"
				EXIT
			ENDIF
		ENDIF
	ELIF GET_GOLF_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
		IF bIsPutting
			IF bGood
				sAnimDict = "mini@golfreactions@trevor@putter"
				txtReturnAnim = "react_win_putter_0"
				txtReturnAnim += iAnimIndex
				txtReturnAnim += "_trevor_facial"
				EXIT
			ELSE
				sAnimDict = "mini@golf"
				txtReturnAnim = "putt_react_bad_0"
				txtReturnAnim += iAnimIndex
				txtReturnAnim += "_face_player2"
				EXIT
			ENDIF
		ELSE
			IF bGood
				sAnimDict = "mini@golfreactions@trevor@clubs"
				txtReturnAnim = "react_win_club_0"
				txtReturnAnim += iAnimIndex
				txtReturnAnim += "_trevor_facial"
				EXIT
			ELSE
				iAnimIndex = CLAMP_INT(iAnimIndex, 1, 2) //ony 2 variations of generic swing face anims
				sAnimDict = "mini@golf"
				txtReturnAnim = "swing_react_bad_0"
				txtReturnAnim += iAnimIndex
				txtReturnAnim += "_face_player2"
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//anims ends in such a way that the player should be walking after
FUNC BOOL SHOULD_PLAYER_WALK_AFTER_SHOT(PED_INDEX pedGolfer)

	TEXT_LABEL_63 sAnimDict = GET_GOLF_REACTION_ANIM_DICT(pedGolfer, FALSE)
	
	IF GET_GOLF_PED_ENUM(pedGolfer) = CHAR_MICHAEL
		IF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_01_michael")
			RETURN TRUE
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_02_michael")
			RETURN TRUE
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_03_michael")
			RETURN TRUE
		ENDIF
	ELIF GET_GOLF_PED_ENUM(pedGolfer) = CHAR_FRANKLIN
		IF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_01_franklin")
			RETURN TRUE
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_02_franklin")
			RETURN TRUE
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_03_franklin")
			RETURN TRUE
		ENDIF
	ELIF GET_GOLF_PED_ENUM(pedGolfer) = CHAR_TREVOR
		IF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_01_trevor")
			RETURN TRUE
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, "react_win_club_02_trevor")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_GOLF_SINGLE_PLAYERS_NAME_FOR_REACTION(BOOL bIsPutting)

	IF GET_GOLF_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
		RETURN "_TREVOR"
	ELIF GET_GOLF_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
		RETURN "_MICHAEL"
	ELIF GET_GOLF_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
		IF bIsPutting
			RETURN "_FRANKLIN_frank"
		ELSE
			RETURN "_FRANKLIN"
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_GOLF_END_FACE_ANIM()
	IF GET_GOLF_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
		RETURN "facials@p_m_zero@variations@happy"
	ELIF GET_GOLF_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
		RETURN "facials@p_m_one@variations@happy"
	ELIF GET_GOLF_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
		RETURN "facials@p_m_two@variations@happy"
	ENDIF
	
	RETURN "facials@p_m_zero@variations@happy"
ENDFUNC

FUNC STRING GET_GOLF_ENDING_ANIM(BOOL bWin)
	
	IF bWin
		RETURN "clubhouse_leave_win_plyr"
	ELSE
		RETURN "clubhouse_leave_lose_alt_plyr"
	ENDIF

ENDFUNC

FUNC STRING GET_GOLF_ENDING_CAM_ANIM(BOOL bWin)
	IF bWin
		RETURN "clubhouse_leave_win_cam"
	ELSE
		RETURN "clubhouse_leave_lose_alt_cam"
	ENDIF
ENDFUNC

FUNC BOOL IS_END_OF_GOLF_CUTSCENE_LOADED()

	REQUEST_ANIM_DICT("mini@golfclubhouse")
	REQUEST_ANIM_DICT(GET_GOLF_END_FACE_ANIM())
	REQUEST_ANIM_DICT("mini@golfclub_bag")
	
	RETURN HAS_ANIM_DICT_LOADED("mini@golfclub_bag")
		   AND HAS_ANIM_DICT_LOADED(GET_GOLF_END_FACE_ANIM()) AND HAS_ANIM_DICT_LOADED("mini@golfclubhouse")
ENDFUNC

FUNC STRING GET_ANIM_CLUB_NAME(eCLUB_TYPE eClub)
	IF eClub <= eCLUB_WOOD_5
		RETURN "Wood"
	ELIF eClub > eCLUB_WOOD_5 AND eCLUB <= eCLUB_IRON_9 
		RETURN "Iron"
	ELIF eClub > eCLUB_IRON_9 AND eClub <= eCLUB_WEDGE_LOB_ULTRA
		RETURN "Wedge"
	ELIF eClub = eCLUB_PUTTER
		RETURN "Putt"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_SWING_ANIM_CLUB(GOLF_BAG &thisBag, GOLF_PLAYER_CORE &thisPlayerCore, BOOL bPostShot = FALSE, BOOL bAllowWedge = TRUE)

	INT currentClub = GET_GOLF_PLAYER_CURRENT_CLUB(thisPlayerCore)
	eCLUB_TYPE eClub = GET_GOLF_CLUB_TYPE(thisBag, currentClub)
	
	IF eClub <= eCLUB_WOOD_5 AND NOT bPostShot
		RETURN "Wood_"
	ELIF eClub > eCLUB_WOOD_5 AND eCLUB <= eCLUB_IRON_9  AND NOT bPostShot
		RETURN "Iron_"
	ELIF eClub > eCLUB_IRON_9 AND eClub <= eCLUB_WEDGE_LOB_ULTRA AND bAllowWedge
		RETURN "Wedge_"
	ELIF eClub = eCLUB_PUTTER
		RETURN "Putt_"
	ENDIF
	
	IF bPostShot
		RETURN "Swing_"
	ENDIF
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Returns the component of a that is parallel to axis
/// PARAMS:
///    a - 
///    axis - 
/// RETURNS:
///    Vector
FUNC VECTOR	ParallelGolf(VECTOR	a, VECTOR axis)
	VECTOR	temp = axis * DOT_PRODUCT(a, axis)
	RETURN	temp
ENDFUNC	

/// PURPOSE:
///    Returns the component of a that is perpendicular to axis
/// PARAMS:
///    a - 
///    axis - 
/// RETURNS:
///    Vector
FUNC VECTOR	PerpGolf(VECTOR	a, VECTOR axis)
	VECTOR temp = a - ParallelGolf(a, axis)
	RETURN temp
ENDFUNC

PROC ROTATE_VECTOR_GOLF(VECTOR &vRotateMe, FLOAT Angle, VECTOR vRotateAbout)
	vRotateAbout = NORMALISE_VECTOR(vRotateAbout)
	FLOAT cosTheta = COS(Angle)
	FLOAT sinTheta = SIN(Angle)
	vRotateMe = PerpGolf(vRotateMe,vRotateAbout)*cosTheta +
				CROSS_PRODUCT(vRotateAbout,vRotateMe)*sinTheta +
				ParallelGolf(vRotateMe,vRotateAbout)
ENDPROC

PROC GET_MAGIC_BLEND_NUMBER_FOR_CLUB(eCLUB_TYPE eClub, FLOAT &fDefaultAngleToClub, FLOAT &fAngleToBlendMagicNumber, FLOAT &fZoffset)
	//GET_GOLF_CLUB_PLACEMENT_OFFSET exaplins why these values are different for mp and sp
	
	IF IS_GOLF_FOURSOME_MP()
		IF eClub <= eCLUB_WOOD_5
			fDefaultAngleToClub = 47.240
			fAngleToBlendMagicNumber = 0.2
			fZoffset = 0.09
		ELIF eClub > eCLUB_WOOD_5 AND eCLUB <= eCLUB_IRON_9
			fDefaultAngleToClub = 53.367
			fAngleToBlendMagicNumber = 0.2
			fZoffset = 0
		ELIF eClub > eCLUB_IRON_9 AND eClub <= eCLUB_WEDGE_LOB_ULTRA
			fDefaultAngleToClub = 52.199
			fAngleToBlendMagicNumber = 0.2
			fZoffset = -0.07
		ELSE //puttiing
			fDefaultAngleToClub = 70.530
			fAngleToBlendMagicNumber = 0.350
			fZoffset = 0.500
		ENDIF
	ELSE
		IF eClub <= eCLUB_WOOD_5
			fDefaultAngleToClub = 46.230
			fAngleToBlendMagicNumber = 0.200
			fZoffset = 0.090
		ELIF eClub > eCLUB_WOOD_5 AND eCLUB <= eCLUB_IRON_9
			fDefaultAngleToClub = 52.25
			fAngleToBlendMagicNumber = 0.2
			fZoffset = 0
		ELIF eClub > eCLUB_IRON_9 AND eClub <= eCLUB_WEDGE_LOB_ULTRA
			fDefaultAngleToClub = 51.910
			fAngleToBlendMagicNumber = 0.15
			fZoffset = -0.07
		ELSE //puttiing
			fDefaultAngleToClub = 70.530
			fAngleToBlendMagicNumber = 0.350
			fZoffset = 0.500
		ENDIF
	ENDIF
	
ENDPROC

FUNC STRING GET_SWING_HEIGHT_MODIFER(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_BAG &thisBag, FLOAT &fBlendAmount, 
									BOOL bOverridePlayerPos = FALSE, FLOAT fOverrideX = 0.0, FLOAT fOverrideY = 0.0, FLOAT fOverrideZ = 0.0)

	#IF GOLF_IS_AI_ONLY
		RETURN ""
	#ENDIF
	
	INT currentClub = GET_GOLF_PLAYER_CURRENT_CLUB(thisPlayerCore)
	eCLUB_TYPE eClub = GET_GOLF_CLUB_TYPE(thisBag, currentClub)
	
	VECTOR vPlayerCoord =  GET_ENTITY_COORDS(GET_GOLF_PLAYER_PED(thisPlayerCore))
	
	IF bOverridePlayerPos
		vPlayerCoord = <<fOverrideX, fOverrideY, fOverrideZ>>
	ENDIF
	
	VECTOR ballPos = GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore)
	FLOAT fDefaultAngleToClub = 52.25
	FLOAT fAngleToBlendMagicNumber
	FLOAT fZoffset
	
	GET_MAGIC_BLEND_NUMBER_FOR_CLUB(eClub, fDefaultAngleToClub, fAngleToBlendMagicNumber, fZoffset)
	vPlayerCoord.z += fZoffset
	
	VECTOR vForwardVector = GET_ENTITY_FORWARD_VECTOR(GET_GOLF_PLAYER_PED(thisPlayerCore))
	VECTOR vToBall = NORMALISE_VECTOR(ballPos - vPlayerCoord)
	VECTOR vVectorToRotateAround = ROTATE_VECTOR_ABOUT_Z(vForwardVector, 90)
	
	VECTOR vToEndOfClub = vForwardVector
	ROTATE_VECTOR_GOLF(vToEndOfClub, fDefaultAngleToClub, vVectorToRotateAround)
	
	FLOAT fAngleBetweenForward =  ACOS(DOT_PRODUCT(vForwardVector, vToBall))
	FLOAT fAngleToRotateClubToBall = fAngleBetweenForward - fDefaultAngleToClub
	
	VECTOR vClubPointingAtBall = NORMALISE_VECTOR(vToEndOfClub)
	ROTATE_VECTOR_GOLF(vClubPointingAtBall, fAngleToRotateClubToBall, vVectorToRotateAround)
	
//	CDEBUG1LN(DEBUG_GOLF, "Forward ", vForwardVector)
//	CDEBUG1LN(DEBUG_GOLF, "Forwarf Flat ", vForwardVectorFlat)
	
	//when debuging, it looks best if you club is a little bit above the white line
	DRAW_DEBUG_LINE(vPlayerCoord, vPlayerCoord+vToBall, 255, 0, 0)
	DRAW_DEBUG_LINE(vPlayerCoord, vPlayerCoord+vForwardVector, 0, 0, 255)
	DRAW_DEBUG_LINE(vPlayerCoord, vPlayerCoord+vClubPointingAtBall, 255, 255, 255)
	DRAW_DEBUG_LINE(vPlayerCoord, vPlayerCoord+vToEndOfClub, 0, 0, 0)
	
//	CDEBUG1LN(DEBUG_GOLF, "Angle between forward and ball ", fAngleBetweenForward)
//	CDEBUG1LN(DEBUG_GOLF, "Angle to roate club to ball ", fAngleToRotateClubToBall)
//	CDEBUG1LN(DEBUG_GOLF, "Magic num blend ", fAngleToBlendMagicNumber)
	
	IF fAngleBetweenForward < fDefaultAngleToClub // roatate the club up to match the angle to the ball

	   fBlendAmount = ABSF(fAngleToRotateClubToBall) * fAngleToBlendMagicNumber

		#IF NOT GOLF_IS_AI_ONLY
		#IF IS_DEBUG_BUILD 
			IF bUseDebugBlendWeight
				fBlendAmount = fDebugBlendWeight
			ENDIF
		#ENDIF
		#ENDIF

		IF fBlendAmount > 0.9999
			fBlendAmount = 0.9999
		ENDIF
//		CDEBUG1LN(DEBUG_GOLF,"High")
//		CDEBUG1LN(DEBUG_GOLF,"Blend ", fBlendAmount)
		RETURN "_High"
	ELSE // roatate the club down to match the angle to the ball
		
		fBlendAmount = ABSF(fAngleToRotateClubToBall) * fAngleToBlendMagicNumber
		
		#IF NOT GOLF_IS_AI_ONLY
		#IF IS_DEBUG_BUILD 
			IF bUseDebugBlendWeight
				fBlendAmount = fDebugBlendWeight
			ENDIF
		#ENDIF
		#ENDIF
		
		IF fBlendAmount > 0.9999
			fBlendAmount = 0.9999
		ENDIF
		
//		CDEBUG1LN(DEBUG_GOLF,"Low")
//		CDEBUG1LN(DEBUG_GOLF,"Blend ", fBlendAmount)
		RETURN "_Low"
	ENDIF
	
	fBlendAmount = 0.0
	RETURN "_High"
ENDFUNC

PROC PLAY_BLENDED_ANIMATION(PED_INDEX pedIndex, TEXT_LABEL_31 &baseAnim, TEXT_LABEL_31 &blendAnim, 
							BOOL bBlendAnim, FLOAT fBlendAmount, FLOAT fRate, ANIMATION_FLAGS animFlags, FLOAT fPhase,
							FLOAT fInTime = NORMAL_BLEND_DURATION, FLOAT fOutTime = NORMAL_BLEND_DURATION, BOOL bUseExtraDictionary = FALSE)

	 ANIM_DATA none
	 ANIM_DATA animDataBlend
	 
	 IF bBlendAnim
	 	animDataBlend.type = APT_3_WAY_BLEND
		animDataBlend.weight0 = 1.0 - fBlendAmount
		animDataBlend.weight1 = fBlendAmount
		
		//set this to whatever to avoid asserts, just make sure weight is 0 so it doesnt blend in
		animDataBlend.weight2 = 0.0
		animDataBlend.anim2 = TEXT_LABEL_TO_STRING(baseAnim)
		animDataBlend.dictionary2 = GET_GOLF_ANIMATION_DICTIONARY_NAME(bUseExtraDictionary)
	ELSE
	 	animDataBlend.type = APT_SINGLE_ANIM
	ENDIF
	
	
	animDataBlend.anim0 = TEXT_LABEL_TO_STRING(baseAnim)
	animDataBlend.dictionary0 = GET_GOLF_ANIMATION_DICTIONARY_NAME(bUseExtraDictionary)
	animDataBlend.phase0 = fPhase
	animDataBlend.rate0 = fRate
	
	IF bBlendAnim
		animDataBlend.anim1 = TEXT_LABEL_TO_STRING(blendAnim)
		animDataBlend.dictionary1 = GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE)
		animDataBlend.phase1 = fPhase
		animDataBlend.rate1 = fRate
	ENDIF
	
	animDataBlend.flags = animFlags
//	
//	#IF IS_DEBUG_BUILD
//	#IF NOT GOLF_IS_AI_ONLY
//		CDEBUG1LN(DEBUG_GOLF,"Playing scripted anim ", baseAnim)
//		DEBUG_PRINTCALLSTACK()
//	#ENDIF
//	#ENDIF

	TASK_SCRIPTED_ANIMATION(pedIndex, animDataBlend, none, none, fInTime, fOutTime)
ENDPROC



FUNC BOOL IS_GOLF_PED_PLAYING_ANY_IDLE_ANIM(PED_INDEX pedGolfer)
	RETURN IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(TRUE), "base")
		OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(FALSE), "idle_a")
		OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(FALSE), "idle_b")
		OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(FALSE), "idle_c")

ENDFUNC

FUNC BOOL IS_GOLF_PED_AT_END_OF_IDLE_ANIM(PED_INDEX pedGolfer)
	
	IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(FALSE), "idle_a")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(FALSE), "idle_a") > 0.99
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(FALSE), "idle_b")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(FALSE), "idle_b") > 0.99
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(FALSE), "idle_c")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(FALSE), "idle_c") > 0.99
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PLAY_APPROACH_BALL_ANIM(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_GAME &thisGame)
	
	FLOAT fBlendAmount
	BOOL bToBlend

	TEXT_LABEL_31 sAnimBlend
	TEXT_LABEL_31 sAnimName =  GET_SWING_ANIM_CLUB(thisGame.golfBag, thisPlayerCore)
	TEXT_LABEL_31 sHeight = GET_SWING_HEIGHT_MODIFER(thisPlayerCore, thisGame.golfBag, fBlendAmount)
	sAnimName += "approach_no_ball"
	sAnimBlend = sAnimName
	sAnimBlend += sHeight
	bToBlend = (fBlendAmount > 0.0)
	
	VECTOR vFinalOffset	
	vFinalOffset = ROTATE_VECTOR_ABOUT_Z(vApproachBallOffset, GET_GOLF_PLAYER_AIM(thisPlayerCore))
	
	#IF NOT GOLF_IS_AI_ONLY
		vFinalOffset = thisPlayercore.vAddressBallPos + vFinalOffset
	#ENDIF
	
	SET_ENTITY_COORDS( GET_GOLF_PLAYER_PED(thisPlayerCore), vFinalOffset, FALSE, TRUE)
	SET_ENTITY_HEADING( GET_GOLF_PLAYER_PED(thisPlayerCore), GET_GOLF_PLAYER_AIM(thisPlayerCore) + fApproachBallHeadingOffset)
	//TASK_PLAY_ANIM(GET_GOLF_PLAYER_PED(thisPlayerCore), GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), sAnimName, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)
	PLAY_BLENDED_ANIMATION(GET_GOLF_PLAYER_PED(thisPlayerCore), sAnimName, sAnimBlend, bToBlend, fBlendAmount, 1.0, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE,  0.0, INSTANT_BLEND_DURATION, INSTANT_BLEND_DURATION, TRUE)
ENDPROC


FUNC BOOL IS_GOLF_APPROACH_ANIM_OVER(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_GAME &thisGame, FLOAT fEndPhase)
	TEXT_LABEL_23 sAnimName =  GET_SWING_ANIM_CLUB(thisGame.golfBag, thisPlayerCore)
	sAnimName += "approach_no_ball"
	
	IF IS_ENTITY_PLAYING_ANIM(GET_GOLF_PLAYER_PED(thisPlayerCore), GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), sAnimName)
		IF GET_ENTITY_ANIM_CURRENT_TIME(GET_GOLF_PLAYER_PED(thisPlayerCore), GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), sAnimName) >= fEndPhase
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_SWING_PUTT_HEIGHT_MODIFER(GOLF_PLAYER_CORE &thisPlayerCore)

	VECTOR playersFeet = GET_ENTITY_COORDS(GET_GOLF_PLAYER_PED(thisPlayerCore)) - <<0.0, 0.0, 1.0>>
	VECTOR ballPos =GET_GOLF_PLAYER_BALL_POSITION(thisPlayerCore)
	FLOAT dist = ABSF(playersFeet.z - ballPos.z)
	
	IF playersFeet.z < ballPos.z AND dist > (0.1)
		//CDEBUG1LN(DEBUG_GOLF,"High ", dist)
		RETURN "_High"
	ELIF dist > (0.18)
		//CDEBUG1LN(DEBUG_GOLF,"Low ", dist)
		RETURN "_Low"
	ENDIF
	
	//No modifer
	RETURN ""
ENDFUNC

FUNC FLOAT GET_SWING_CLUB_RATE(GOLF_BAG &thisBag, GOLF_PLAYER_CORE &thisPlayerCore)

	INT currentClub = GET_GOLF_PLAYER_CURRENT_CLUB(thisPlayerCore)
	eCLUB_TYPE eClub = GET_GOLF_CLUB_TYPE(thisBag, currentClub)
	
	IF eClub <= eCLUB_WOOD_5 
		RETURN 1.0
	ELIF eClub > eCLUB_WOOD_5 AND eCLUB <= eCLUB_IRON_9
		RETURN 1.0
	ELIF eClub > eCLUB_IRON_9 AND eClub <= eCLUB_WEDGE_LOB_ULTRA
		RETURN 0.95
	ELIF eClub = eCLUB_PUTTER
		RETURN 1.0
	ENDIF
	
	RETURN 1.0
ENDFUNC

FUNC BOOL IS_ANY_SWING_ANIM_PLAYING(PED_INDEX pedGolfer)
	IF NOT IS_PED_INJURED(pedGolfer)
		TEXT_LABEL_31 animWoodAction   = "Wood_swing_action"
		TEXT_LABEL_31 animIronAction   = "Iron_swing_action"
		TEXT_LABEL_31 animWedgeAction  = "Wedge_swing_action"
		TEXT_LABEL_31 animPuttAction   = "Putt_action"
		
		IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWoodAction)
			RETURN(TRUE)
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animIronAction)
			RETURN(TRUE)
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWedgeAction)
			RETURN(TRUE)
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animPuttAction)
			RETURN(TRUE)
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_BACKSWING_ANIM_PLAYING(PED_INDEX pedGolfer)
	IF NOT IS_PED_INJURED(pedGolfer)
		TEXT_LABEL_31 animWoodAction   = "Wood_swing_intro"
		TEXT_LABEL_31 animIronAction   = "Iron_swing_intro"
		TEXT_LABEL_31 animWedgeAction  = "Wedge_swing_intro"
		TEXT_LABEL_31 animPuttAction   = "Putt_intro"
		
		IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWoodAction)
			RETURN(TRUE)
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animIronAction)
			RETURN(TRUE)
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWedgeAction)
			RETURN(TRUE)
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animPuttAction)
			RETURN(TRUE)
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_GOLF_PLAYER_BACK_SWING_PHASE(PED_INDEX pedGolfer)

	IF NOT IS_PED_INJURED(pedGolfer)
		TEXT_LABEL_31 animWoodAction   = "Wood_swing_intro"
		TEXT_LABEL_31 animIronAction   = "Iron_swing_intro"
		TEXT_LABEL_31 animWedgeAction  = "Wedge_swing_intro"
		TEXT_LABEL_31 animPuttAction   = "Putt_intro"
		
		IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWoodAction)
			RETURN GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWoodAction)
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animIronAction)
			RETURN GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animIronAction)
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWedgeAction)
			RETURN GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWedgeAction)
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animPuttAction)
			RETURN GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animPuttAction)
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "Wood_swing_idle")
			RETURN 1.0
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "Iron_swing_idle")
			RETURN 1.0
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "Wedge_swing_idle")
			RETURN 1.0
		ENDIF	
		
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC BOOL HAS_SWING_ANIM_REACHED_BALL(PED_INDEX pedGolfer)
	IF NOT IS_PED_INJURED(pedGolfer)
		TEXT_LABEL_31 animWoodAction   = "Wood_swing_action"
		TEXT_LABEL_31 animIronAction   = "Iron_swing_action"
		TEXT_LABEL_31 animWedgeAction  = "Wedge_swing_action"
		TEXT_LABEL_31 animPuttAction   = "Putt_action"
		
		IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWoodAction)
			IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWoodAction) > fWoodlHitTime
				RETURN(TRUE)
			ENDIF
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animIronAction)
			IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animIronAction) > fIronHitTime
				RETURN(TRUE)
			ENDIF	
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWedgeAction)
			IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWedgeAction) > fWedgeHitTime
				RETURN(TRUE)
			ENDIF
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animPuttAction)
			IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animPuttAction) > fPuttHitTime
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL HAS_SWING_ANIM_REACHED_TIME(PED_INDEX pedGolfer, FLOAT fTime)
	IF NOT IS_PED_INJURED(pedGolfer)
		TEXT_LABEL_31 animWoodAction   = "Wood_swing_action"
		TEXT_LABEL_31 animIronAction   = "Iron_swing_action"
		TEXT_LABEL_31 animWedgeAction  = "Wedge_swing_action"
		TEXT_LABEL_31 animPuttAction   = "Putt_action"

		IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWoodAction)
			IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWoodAction) >= fTime
				RETURN(TRUE)
			ENDIF
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animIronAction)
			IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animIronAction) >= fTime
				RETURN(TRUE)
			ENDIF	
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWedgeAction)
			IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animWedgeAction) >= fTime
				RETURN(TRUE)
			ENDIF
		ELIF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animPuttAction)
			IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), animPuttAction) >= fTime
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_GOLFER_PLAYING_CLUB_SWITCH_ANIM(PED_INDEX pedIndex)
	IF NOT DOES_ENTITY_EXIST(pedIndex) OR IS_ENTITY_DEAD(pedIndex)
		RETURN FALSE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "putt_2_wedge")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "putt_2_wedge") < 0.99
			RETURN(TRUE)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "wedge_2_putt")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "wedge_2_putt") < 0.99
			RETURN(TRUE)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "putt_2_iron")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "putt_2_iron") < 0.99
			RETURN(TRUE)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "iron_2_putt")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "iron_2_putt") < 0.99
			RETURN(TRUE)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "wedge_2_iron")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "wedge_2_iron") < 0.99
			RETURN(TRUE)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "iron_2_wedge")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "iron_2_wedge") < 0.99
			RETURN(TRUE)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "iron_2_wood")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "iron_2_wood") < 0.99
			RETURN(TRUE)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "wood_2_iron")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "wood_2_iron") < 0.99
			RETURN(TRUE)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "wood_2_wedge")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "wood_2_wedge") < 0.99
			RETURN(TRUE)
		ENDIF	
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "wedge_2_wood")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "wedge_2_wood") < 0.99
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GOLF_PLAY_CLUB_SWITCH_ANIM(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_BAG &golfBag, eCLUB_TYPE ePrevClubType, VECTOR vFinalPosAfterSwitch)
	FLOAT fVariableClubHeight
	STRING heightModifier = GET_SWING_HEIGHT_MODIFER(thisPlayerCore, golfBag, fVariableClubHeight, TRUE, vFinalPosAfterSwitch.x, vFinalPosAfterSwitch.y, vFinalPosAfterSwitch.z)
													
	INT currentClub = GET_GOLF_PLAYER_CURRENT_CLUB(thisPlayerCore)
	eCLUB_TYPE eClub = GET_GOLF_CLUB_TYPE(golfBag, currentClub)
	
	BOOL bUseBlend = NOT IS_STRING_EMPTY(heightModifier)
	TEXT_LABEL_31 animBase = GET_ANIM_CLUB_NAME(ePrevClubType)
	TEXT_LABEL_31 animBlend = animBase
	TEXT_LABEL_31 txtNextClub = GET_ANIM_CLUB_NAME(eClub)
	
	animBase += "_2_"
	animBlend += "_2_"
	
	animBase += txtNextClub
	animBlend += txtNextClub	
	
	IF bUseBlend
		animBlend += heightModifier
	ENDIF

	CDEBUG1LN(DEBUG_GOLF,"Start club switch anim ", animBase, " Blend with ", animBlend)
	
	//Use the slow blend duration so I get a nice smooth transition from one height to the next
	PLAY_BLENDED_ANIMATION(GET_GOLF_PLAYER_PED(thisPlayerCore), animBase, animBlend, bUseBlend, fVariableClubHeight, 1.0,
						   AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, 0.0, NORMAL_BLEND_DURATION, NORMAL_BLEND_DURATION, TRUE)
						   
	FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_GOLF_PLAYER_PED(thisPlayerCore), TRUE)
ENDPROC

//If it is okay to go back to idle anim from shuffle
FUNC BOOL IS_GOLFER_PLAYING_SHUFFLE_ANIM(PED_INDEX pedIndex)

	IF NOT DOES_ENTITY_EXIST(pedIndex) OR IS_ENTITY_DEAD(pedIndex)
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "Wood_shuffle")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "Wood_shuffle") < 0.90
			RETURN(TRUE)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "Iron_shuffle")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "Iron_shuffle") < 0.90
			RETURN(TRUE)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "Wedge_shuffle")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "Wedge_shuffle") < 0.99
			RETURN(TRUE)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "Putt_shuffle")
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), "Putt_shuffle") < 0.99
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GOLF_BEGIN_SHUFFLE_ANIMATION(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_BAG &golfBag)

	FLOAT fVariableClubHeight
	STRING heightModifier = GET_SWING_HEIGHT_MODIFER(thisPlayerCore, golfBag, fVariableClubHeight)
	
	BOOL bUseBlend = NOT IS_STRING_EMPTY(heightModifier)
	TEXT_LABEL_31 animBase = GET_SWING_ANIM_CLUB(golfBag, thisPlayerCore)
	TEXT_LABEL_31 animBlend = animBase
	animBase += "shuffle"
	animBlend += "shuffle"
	
	IF bUseBlend
		animBlend += heightModifier
		//CDEBUG1LN(DEBUG_GOLF,"Blend with ", animBlend)
	ENDIF	
	
	IF IS_ENTITY_PLAYING_ANIM(GET_GOLF_PLAYER_PED(thisPlayerCore), GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animBlend)
		IF GET_ENTITY_ANIM_CURRENT_TIME(GET_GOLF_PLAYER_PED(thisPlayerCore), GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animBlend) < 0.9
			IF bUseBlend
				CDEBUG1LN(DEBUG_GOLF,"New blend ammount ", fVariableClubHeight)
				SET_ANIM_WEIGHT(PLAYER_PED_ID(), 1.0-fVariableClubHeight, AF_PRIORITY_LOW, 0)
				SET_ANIM_WEIGHT(PLAYER_PED_ID(), fVariableClubHeight, AF_PRIORITY_LOW, 1)
			ENDIF
		ENDIF
		EXIT
	ENDIF

	//CDEBUG1LN(DEBUG_GOLF,"Start shuffle anim ", animBase)

	
	//Use the slow blend duration so I get a nice smooth transition from one height to the next
	PLAY_BLENDED_ANIMATION(GET_GOLF_PLAYER_PED(thisPlayerCore), animBase, animBlend, bUseBlend, fVariableClubHeight, 1.0,
						   AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, 0.0, NORMAL_BLEND_DURATION, NORMAL_BLEND_DURATION, TRUE)


ENDPROC

PROC GOLF_FORCE_IDLE_ANIMATION(GOLF_PLAYER_CORE &thisPlayerCore, GOLF_BAG &golfBag)

	FLOAT fVariableClubHeight
	STRING heightModifier = GET_SWING_HEIGHT_MODIFER(thisPlayerCore, golfBag, fVariableClubHeight)
	
	BOOL bUseBlend = NOT IS_STRING_EMPTY(heightModifier)
	TEXT_LABEL_31 animIdle = GET_SWING_ANIM_CLUB(golfBag, thisPlayerCore)
	animIdle += "idle"
	TEXT_LABEL_31 animIdleBlend = animIdle

	animIdleBlend +=heightModifier
	animIdleBlend += "_a"
	
	animIdle += "_a"

	CDEBUG1LN(DEBUG_GOLF,"Force Idle anim ", animIdle)
	
	IF bUseBlend
		CDEBUG1LN(DEBUG_GOLF,"Blending idle with anim ",animIdleBlend, " blend amount ",  fVariableClubHeight)
	ENDIF
	
	PLAY_BLENDED_ANIMATION(GET_GOLF_PLAYER_PED(thisPlayerCore), animIdle, animIdleBlend, bUseBlend, fVariableClubHeight, 1.0,
												AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, 0.0, INSTANT_BLEND_DURATION, INSTANT_BLEND_DURATION)
	
	IF NOT IS_GOLF_PLAYER_CONTROL_FLAG_SET(thisPlayerCore, GOLF_FORCE_UPDATE)
		CDEBUG1LN(DEBUG_GOLF,"Forcing anim")
		FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_GOLF_PLAYER_PED(thisPlayerCore), TRUE)
		FORCE_INSTANT_LEG_IK_SETUP(GET_GOLF_PLAYER_PED(thisPlayerCore))
		SET_GOLF_PLAYER_CONTROL_FLAG(thisPlayerCore, GOLF_FORCE_UPDATE) //only allow one force update per lineup with ball
	ENDIF

ENDPROC


FUNC BOOL IS_GOLF_IDLE_ANIMATION_PLAYING(PED_INDEX pedIndex, TEXT_LABEL_31 &animIdle, BOOL bUseExtraDictionary)
	
	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(bUseExtraDictionary), animIdle)
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(bUseExtraDictionary), animIdle) < 0.99 
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

//Returns if animIdle0 is the animation currently being played
PROC GOLF_SWITCH_IDLE_ANIM(PED_INDEX pedIndex,  TEXT_LABEL_31 &animIdle0, TEXT_LABEL_31 &animIdle1, TEXT_LABEL_31 &animIdle2, 
												TEXT_LABEL_31 &animBlend0, TEXT_LABEL_31 &animBlend1, TEXT_LABEL_31 &animBlend2, 
												BOOL bBlendAnim, FLOAT fBlendAmount)
		
	FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE()
	
	//If the golfer is still playing one of the other anims, dont switch
	IF IS_GOLF_IDLE_ANIMATION_PLAYING(pedIndex, animIdle0, FALSE) OR IS_GOLF_IDLE_ANIMATION_PLAYING(pedIndex, animIdle1, TRUE) 
	OR IS_GOLF_IDLE_ANIMATION_PLAYING(pedIndex, animIdle2, TRUE)
	OR IS_GOLFER_PLAYING_SHUFFLE_ANIM(pedIndex) OR IS_GOLFER_PLAYING_CLUB_SWITCH_ANIM(pedIndex)
	 	EXIT
	 ENDIF

	IF fRand < 0.33
		CDEBUG1LN(DEBUG_GOLF,"Switch to idle anim ", animIdle0)
		PLAY_BLENDED_ANIMATION(pedIndex, animIdle0, animBlend0, bBlendAnim, fBlendAmount, 1.0, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, 
								0.0, INSTANT_BLEND_DURATION, INSTANT_BLEND_DURATION,  FALSE)
	ELIF fRand <0.66
		CDEBUG1LN(DEBUG_GOLF,"Switch to idle anim ", animIdle1)
		PLAY_BLENDED_ANIMATION(pedIndex, animIdle1, animBlend1, bBlendAnim, fBlendAmount, 1.0, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME,
								0.0, INSTANT_BLEND_DURATION, INSTANT_BLEND_DURATION, TRUE)
	ELSE
		CDEBUG1LN(DEBUG_GOLF,"Switch to idle anim ", animIdle2)
		PLAY_BLENDED_ANIMATION(pedIndex, animIdle2, animBlend2, bBlendAnim, fBlendAmount, 1.0, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME,
								0.0, INSTANT_BLEND_DURATION, INSTANT_BLEND_DURATION, TRUE)
	ENDIF

ENDPROC

PROC GOLF_MANAGE_IDLE_ANIMS(PED_INDEX pedIndex, TEXT_LABEL_31 &idleAnim, TEXT_LABEL_31 &blendAnim, GOLF_HELPERS &thisHelpers, BOOL bBlendAnim, FLOAT bBlendAmount)
		
	TEXT_LABEL_31 animIdleA = idleAnim
	TEXT_LABEL_31 animIdleB = idleAnim
	TEXT_LABEL_31 animIdleC = idleAnim
	TEXT_LABEL_31 blendIdleA = blendAnim
	TEXT_LABEL_31 blendIdleB = blendAnim
	TEXT_LABEL_31 blendIdleC = blendAnim
	
	animIdleA += "_A"
	animIdleB += "_B"
	animIdleC += "_C"
	
	blendIdleA += "_A"
	blendIdleB += "_B"
	blendIdleC += "_C"
	
	IF NOT IS_TIMER_STARTED(thisHelpers.idleTimer)
		START_TIMER_NOW(thisHelpers.idleTimer)
	ENDIF
	
	IF TIMER_DO_WHEN_READY(thisHelpers.idleTimer, 0.1)
	AND GET_GOLF_HELPER_INPUT_CLEARED(thisHelpers)
		GOLF_SWITCH_IDLE_ANIM(pedIndex, animIdleA, animIdleB, animIdleC, blendIdleA, blendIdleB, blendIdleC, bBlendAnim, bBlendAmount)
		RESTART_TIMER_NOW(thisHelpers.idleTimer)
	ENDIF

ENDPROC

PROC GOLF_MANAGE_PUTT_SWING_ANIMS(GOLF_GAME &thisGame, GOLF_PLAYER_CORE &thisPlayerCore, SWING_METER &swingMeter #IF NOT GOLF_IS_AI_ONLY, GOLF_HELPERS &thisHelpers #ENDIF)
	PED_INDEX pedGolfer = GET_GOLF_PLAYER_PED(thisPlayerCore)
	// These, or whatever replace them, will be constants in golf.sch
	FLOAT fBackswingAnimTime		= 1.0
	FLOAT fBackswingStartTime 	= 0.1
	FLOAT fReturnAnimSpeed 		= 0.05			//Controls the speed of the return to idle anim, which is the back-swing anim played backwards.
	#IF NOT GOLF_IS_AI_ONLY
	FLOAT fSwingForwardRate = GET_SWING_CLUB_RATE(thisGame.golfBag, thisPlayerCore)
	#ENDIF
	fBackswingAnimTime =fBackswingAnimTime
	
	IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REQULAR_SWING_ANIMS --") ENDIF

	IF NOT IS_PED_INJURED(pedGolfer)
		FLOAT fAnimTime, fVariableClubHeight
		STRING heightModifier = GET_SWING_HEIGHT_MODIFER(thisPlayerCore, thisGame.golfBag, fVariableClubHeight)
		//BOOL bUseExtraDictionary = NOT IS_STRING_EMPTY(heightModifier)
		BOOl bUseBlendedAnim = NOT IS_STRING_EMPTY(heightModifier)
		STRING clubAnimation = GET_SWING_ANIM_CLUB(thisGame.golfBag, thisPlayerCore)
		
		TEXT_LABEL_31 animIdle = clubAnimation
		TEXT_LABEL_31 animSwingIntro = clubAnimation
		TEXT_LABEL_31 animSwingAction = clubAnimation
		//TEXT_LABEL_31 animSwingOutro = "Swing_Outro" //nothing else, just swing_outro
		
		animIdle += "Idle"

		animSwingIntro += "Intro"
		animSwingAction += "Action"

		heightModifier = heightModifier
		bUseBlendedAnim = bUseBlendedAnim
		#IF NOT GOLF_IS_AI_ONLY
			TEXT_LABEL_31 animIdleBlend = animIdle
			animIdleBlend += heightModifier
			
			TEXT_LABEL_31 animSwingIntroBlend = animSwingIntro 
			animSwingIntroBlend += heightModifier
			
			TEXT_LABEL_31 animSwingActionBlend = animSwingAction
			animSwingActionBlend += heightModifier
		#ENDIF
		
		SWITCH GET_GOLF_PLAYER_SWING_ANIM(thisPlayerCore)
			CASE SAS_IDLE
				IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_IDLE") ENDIF
				#IF NOT GOLF_IS_AI_ONLY
					IF IS_GOLF_APPROACH_ANIM_OVER(thisPlayerCore, thisGame, 0.99)
 						GOLF_MANAGE_IDLE_ANIMS(pedGolfer, animIdle, animIdleBlend, thisHelpers, bUseBlendedAnim, fVariableClubHeight)
					ENDIF
				#ENDIF
			BREAK
			
			CASE SAS_SWINGING_BACK
				IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_BACK") ENDIF
				IF NOT IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
					
					#IF NOT GOLF_IS_AI_ONLY
						PLAY_BLENDED_ANIMATION(pedGolfer, animSwingIntro, animSwingIntroBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_HOLD_LAST_FRAME, 0.0)
					#ENDIF
					
					#IF GOLF_IS_AI_ONLY
						TASK_PLAY_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
					#ENDIF
					IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_BACK - TASK_PLAY_ANIM - Swing_Intro") ENDIF
				ELSE
					fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
					IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro) >=0.975
				    	IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_BACK - Setting speed of anim to 0 - HOLD") ENDIF
						SET_ENTITY_ANIM_SPEED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, 0.0)
						IF swingMeter.iBackswingHoldTime = 0
							swingMeter.iBackswingHoldTime = GET_GAME_TIMER()
						ENDIF
					ELSE
						SET_ENTITY_ANIM_SPEED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, 0.6)
					ENDIF
					
					IF fAnimTime < fBackswingStartTime
						SET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, fBackswingStartTime)
						SET_ENTITY_ANIM_SPEED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, 0.8)
					ENDIF
				ENDIF
			BREAK
			
			CASE SAS_RETURNING_TO_IDLE	
				//If player is playing the swing anim, play in reverse until it reaches idle position
				IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_RETURNING_TO_IDLE") ENDIF
				animIdle+= "_a"
				#IF NOT GOLF_IS_AI_ONLY
					animIdleBlend += "_a"
				#ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
				#IF NOT GOLF_IS_AI_ONLY OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIntroBlend) #ENDIF
				
					SET_ENTITY_ANIM_SPEED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, 0.0)
					fAnimTime =GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
					
					#IF NOT GOLF_IS_AI_ONLY
					IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIntroBlend)
						SET_ENTITY_ANIM_SPEED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIntroBlend, 0.0)
						fAnimTime =GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIntroBlend)
					ENDIF
					#ENDIF

					IF fAnimTime < fBackswingStartTime
						#IF NOT GOLF_IS_AI_ONLY
							PLAY_BLENDED_ANIMATION(pedGolfer, animIdle, animIdleBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, 0.0, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
						#ENDIF
						#IF  GOLF_IS_AI_ONLY
							TASK_PLAY_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(),  animIdle, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
						#ENDIF
						
						SET_GOLF_PLAYER_SWING_ANIM(thisPlayerCore, SAS_IDLE)
						//CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_RETURNING_TO_IDLE - is finished returning from Swing_Intro - TASK_PLAY_ANIM - Idle")
					ELSE
						
						SET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(),animSwingIntro, fAnimTime - fReturnAnimSpeed)
						#IF NOT GOLF_IS_AI_ONLY
							IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIntroBlend)
								//fAnimTime =GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIntroBlend)
								SET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE),animSwingIntroBlend, fAnimTime - fReturnAnimSpeed)
							ENDIF
						#ENDIF
						
						IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_RETURNING_TO_IDLE - is returning from Swing_Intro") ENDIF
					ENDIF
				ELSE
					IF NOT IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animIdle)
						
						#IF NOT GOLF_IS_AI_ONLY
							PLAY_BLENDED_ANIMATION(pedGolfer, animIdle, animIdleBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, SLOW_BLEND_DURATION, 0.0)
						#ENDIF
						#IF  GOLF_IS_AI_ONLY
							TASK_PLAY_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animIdle, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
						#ENDIF
						IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_RETURNING_TO_IDLE - TASK_PLAY_ANIM - Idle") ENDIF
					ENDIF
				
					SET_GOLF_PLAYER_SWING_ANIM(thisPlayerCore, SAS_IDLE)
				ENDIF
			BREAK
			
			CASE SAS_SWINGING_FORWARD				
				IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_FORWARD") ENDIF
				IF NOT IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingAction) 
					IF HAS_ENTITY_ANIM_FINISHED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
						
						#IF NOT GOLF_IS_AI_ONLY
							PLAY_BLENDED_ANIMATION(pedGolfer, animSwingAction, animSwingActionBlend, bUseBlendedAnim, fVariableClubHeight, fSwingForwardRate, AF_HOLD_LAST_FRAME, 0.0)
						#ENDIF
						#IF  GOLF_IS_AI_ONLY
							TASK_PLAY_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(),animSwingAction, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
						#ENDIF
						
						IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_FORWARD - finished Swing_Intro - TASK_PLAY_ANIM sequence - Swing_Action then Swing_Outro") ENDIF
					ELSE
						IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
							fBackswingAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
							
							#IF NOT GOLF_IS_AI_ONLY
								FLOAT fPhase
								fPhase = 0.16 - 0.16*fBackswingAnimTime
								PLAY_BLENDED_ANIMATION(pedGolfer, animSwingAction, animSwingActionBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_HOLD_LAST_FRAME, fPhase)
							#ENDIF
							#IF  GOLF_IS_AI_ONLY
								TASK_PLAY_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingAction, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
							#ENDIF
							
							IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_FORWARD - in the middle of Swing_Intro - TASK_PLAY_ANIM sequence - Swing_Action then Swing_Outro") ENDIF
						ELSE
							SET_GOLF_PLAYER_SWING_ANIM(thisPlayerCore, SAS_IDLE)
						ENDIF
					ENDIF
				ELSE
					#IF NOT GOLF_IS_AI_ONLY
					CDEBUG1LN(DEBUG_GOLF,"Check if swing forward is done")
					IF HAS_ENTITY_ANIM_FINISHED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingAction)
						CDEBUG1LN(DEBUG_GOLF,"Swing forward is over, go back to idle")
						animIdle += "_a"
						animIdleBlend += "_a"
						PLAY_BLENDED_ANIMATION(pedGolfer, animIdle, animIdleBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, 0.0, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
						SET_GOLF_PLAYER_SWING_ANIM(thisPlayerCore, SAS_IDLE)
					ENDIF
					#ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

ENDPROC

// GTA5 anims are in
PROC GOLF_MANAGE_REGULAR_SWING_ANIMS( GOLF_GAME &thisGame, GOLF_PLAYER_CORE &thisPlayerCore, SWING_METER &swingMeter #IF NOT GOLF_IS_AI_ONLY, GOLF_HELPERS &thisHelpers #ENDIF)
	
	IF IS_GOLF_PLAYER_PUTTING(thisPlayerCore)
		GOLF_MANAGE_PUTT_SWING_ANIMS(thisGame, thisPlayerCore, swingMeter #IF NOT GOLF_IS_AI_ONLY, thisHelpers #ENDIF)
		EXIT
	ENDIF
	
	
	PED_INDEX pedGolfer = GET_GOLF_PLAYER_PED(thisPlayerCore)
	// These, or whatever replace them, will be constants in golf.sch
	FLOAT fBackswingAnimTime		= 1.0
	FLOAT fBackswingStartTime 	= 0.1
	FLOAT fReturnAnimSpeed 		= 0.05			//Controls the speed of the return to idle anim, which is the back-swing anim played backwards.
	#IF NOT GOLF_IS_AI_ONLY
	FLOAT fSwingForwardRate = GET_SWING_CLUB_RATE(thisGame.golfBag, thisPlayerCore)
	#ENDIF
	fBackswingAnimTime =fBackswingAnimTime
	
	IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REQULAR_SWING_ANIMS --") ENDIF

	IF NOT IS_PED_INJURED(pedGolfer)
		FLOAT fAnimTime, fVariableClubHeight
		STRING heightModifier = GET_SWING_HEIGHT_MODIFER(thisPlayerCore, thisGame.golfBag, fVariableClubHeight)
		//BOOL bUseExtraDictionary = NOT IS_STRING_EMPTY(heightModifier)
		BOOl bUseBlendedAnim = NOT IS_STRING_EMPTY(heightModifier)
		STRING clubAnimation = GET_SWING_ANIM_CLUB(thisGame.golfBag, thisPlayerCore)
		
		TEXT_LABEL_31 animIdle = clubAnimation
		TEXT_LABEL_31 animSwingIntro = clubAnimation
		TEXT_LABEL_31 animSwingAction = clubAnimation
		TEXT_LABEL_31 animSwingIdle = clubAnimation
		UNUSED_PARAMETER(animSwingIdle)
		//TEXT_LABEL_31 animSwingOutro = "Swing_Outro" //nothing else, just swing_outro
		
		animIdle += "Idle"
		
		animSwingIntro += "Swing_Intro"
		animSwingAction += "Swing_Action"
		animSwingIdle += "Swing_idle"
		
		heightModifier = heightModifier
		bUseBlendedAnim = bUseBlendedAnim
		#IF NOT GOLF_IS_AI_ONLY
			TEXT_LABEL_31 animIdleBlend = animIdle
			animIdleBlend += heightModifier
			
			TEXT_LABEL_31 animSwingIntroBlend = animSwingIntro 
			animSwingIntroBlend += heightModifier
			
			TEXT_LABEL_31 animSwingActionBlend = animSwingAction
			animSwingActionBlend += heightModifier
			
			TEXT_LABEL_31 animSwingIdleBlend = animSwingIdle
			animSwingIdleBlend += heightModifier
		#ENDIF
		
		SWITCH GET_GOLF_PLAYER_SWING_ANIM(thisPlayerCore)
			CASE SAS_IDLE
				IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_IDLE") ENDIF
				#IF NOT GOLF_IS_AI_ONLY
					IF IS_GOLF_APPROACH_ANIM_OVER(thisPlayerCore, thisGame, 0.99)
 						GOLF_MANAGE_IDLE_ANIMS(pedGolfer, animIdle, animIdleBlend, thisHelpers, bUseBlendedAnim, fVariableClubHeight)
					ENDIF
				#ENDIF
			BREAK
			
			CASE SAS_SWINGING_BACK
				IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_BACK") ENDIF
				IF NOT IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
				#IF NOT GOLF_IS_AI_ONLY AND NOT IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIdle) #ENDIF
					#IF NOT GOLF_IS_AI_ONLY
						PLAY_BLENDED_ANIMATION(pedGolfer, animSwingIntro, animSwingIntroBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_HOLD_LAST_FRAME, 0.0)
					#ENDIF
					
					#IF GOLF_IS_AI_ONLY
						TASK_PLAY_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
					#ENDIF
					IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_BACK - TASK_PLAY_ANIM - Swing_Intro") ENDIF
				ELSE
					#IF NOT GOLF_IS_AI_ONLY IF NOT IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIdle) #ENDIF
						fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro) >=0.975
					    	IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_BACK - Setting speed of anim to 0 - HOLD") ENDIF						
							
							#IF NOT GOLF_IS_AI_ONLY
								PLAY_BLENDED_ANIMATION(pedGolfer, animSwingIdle, animSwingIdleBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_LOOPING, 0.0, INSTANT_BLEND_DURATION, INSTANT_BLEND_DURATION, TRUE)
							#ENDIF
							
							#IF GOLF_IS_AI_ONLY
								SET_ENTITY_ANIM_SPEED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, 0.0)
							#ENDIF							

							IF swingMeter.iBackswingHoldTime = 0
								swingMeter.iBackswingHoldTime = GET_GAME_TIMER()
							ENDIF
						ELSE
							SET_ENTITY_ANIM_SPEED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, 0.6)
						ENDIF
						
						IF fAnimTime < fBackswingStartTime
							SET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, fBackswingStartTime)
							SET_ENTITY_ANIM_SPEED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, 0.8)
						ENDIF
					#IF NOT GOLF_IS_AI_ONLY ENDIF #ENDIF
				ENDIF
			BREAK
			
			CASE SAS_RETURNING_TO_IDLE	
				//If player is playing the swing anim, play in reverse until it reaches idle position
				IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_RETURNING_TO_IDLE") ENDIF
				animIdle+= "_A"
				#IF NOT GOLF_IS_AI_ONLY
					animIdleBlend += "_a"
				#ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
				
					SET_ENTITY_ANIM_SPEED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro, 0.0)
					fAnimTime =GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
					
					#IF NOT GOLF_IS_AI_ONLY
					IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIntroBlend)
						SET_ENTITY_ANIM_SPEED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIntroBlend, 0.0)
						fAnimTime =GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIntroBlend)
					ENDIF
					#ENDIF

					IF fAnimTime < fBackswingStartTime
						#IF NOT GOLF_IS_AI_ONLY
							PLAY_BLENDED_ANIMATION(pedGolfer, animIdle, animIdleBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, 0.0, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
						#ENDIF
						#IF  GOLF_IS_AI_ONLY
							TASK_PLAY_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(),  animIdle, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
						#ENDIF
						
						SET_GOLF_PLAYER_SWING_ANIM(thisPlayerCore, SAS_IDLE)
						//CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_RETURNING_TO_IDLE - is finished returning from Swing_Intro - TASK_PLAY_ANIM - Idle")
					ELSE
						
						SET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(),animSwingIntro, fAnimTime - fReturnAnimSpeed)
						#IF NOT GOLF_IS_AI_ONLY
							IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIntroBlend)
								//fAnimTime =GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIntroBlend)
								SET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE),animSwingIntroBlend, fAnimTime - fReturnAnimSpeed)
							ENDIF
						#ENDIF
						
						IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_RETURNING_TO_IDLE - is returning from Swing_Intro") ENDIF
					ENDIF
				ELSE
					#IF NOT GOLF_IS_AI_ONLY
					IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIdle)
					
						PLAY_BLENDED_ANIMATION(pedGolfer, animSwingIntro, animSwingIntroBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_HOLD_LAST_FRAME, 1.0, INSTANT_BLEND_DURATION, INSTANT_BLEND_DURATION)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGolfer, TRUE)
					ELSE
					#ENDIF
						IF NOT IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animIdle)
							
							#IF NOT GOLF_IS_AI_ONLY
								PLAY_BLENDED_ANIMATION(pedGolfer, animIdle, animIdleBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, SLOW_BLEND_DURATION, 0.0)
							#ENDIF
							#IF  GOLF_IS_AI_ONLY
								TASK_PLAY_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animIdle, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
							#ENDIF
							IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_RETURNING_TO_IDLE - TASK_PLAY_ANIM - Idle") ENDIF
						ENDIF
					
						SET_GOLF_PLAYER_SWING_ANIM(thisPlayerCore, SAS_IDLE)
					#IF NOT GOLF_IS_AI_ONLY ENDIF #ENDIF
				ENDIF
			BREAK
			
			CASE SAS_SWINGING_FORWARD				
				IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_FORWARD") ENDIF
				IF NOT IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingAction) 
					IF HAS_ENTITY_ANIM_FINISHED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
					#IF NOT GOLF_IS_AI_ONLY
					OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animSwingIdle)
					#ENDIF
						#IF NOT GOLF_IS_AI_ONLY
							PLAY_BLENDED_ANIMATION(pedGolfer, animSwingAction, animSwingActionBlend, bUseBlendedAnim, fVariableClubHeight, fSwingForwardRate, AF_HOLD_LAST_FRAME, 0.0)
						#ENDIF
						#IF  GOLF_IS_AI_ONLY
							TASK_PLAY_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(),animSwingAction, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
						#ENDIF
						
						IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_FORWARD - finished Swing_Intro - TASK_PLAY_ANIM sequence - Swing_Action then Swing_Outro") ENDIF
					ELSE
						IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
							fBackswingAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingIntro)
							
							#IF NOT GOLF_IS_AI_ONLY
								FLOAT fPhase
								fPhase = 0.16 - 0.16*fBackswingAnimTime
								PLAY_BLENDED_ANIMATION(pedGolfer, animSwingAction, animSwingActionBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_HOLD_LAST_FRAME, fPhase)
							#ENDIF
							#IF  GOLF_IS_AI_ONLY
								TASK_PLAY_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingAction, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
							#ENDIF
							
							IF bDebugSpew CDEBUG1LN(DEBUG_GOLF,"GOLF_MANAGE_REGULAR_SWING_ANIMS : SAS_SWINGING_FORWARD - in the middle of Swing_Intro - TASK_PLAY_ANIM sequence - Swing_Action then Swing_Outro") ENDIF
						ELSE
							SET_GOLF_PLAYER_SWING_ANIM(thisPlayerCore, SAS_IDLE)
						ENDIF
					ENDIF
				ELSE
					#IF NOT GOLF_IS_AI_ONLY
					CDEBUG1LN(DEBUG_GOLF,"Check if swing forward is done")
					IF HAS_ENTITY_ANIM_FINISHED(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(), animSwingAction)
						CDEBUG1LN(DEBUG_GOLF,"Swing forward is over, go back to idle")
						animIdle += "_a"
						animIdleBlend += "_a"
						PLAY_BLENDED_ANIMATION(pedGolfer, animIdle, animIdleBlend, bUseBlendedAnim, fVariableClubHeight, 1.0, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, 0.0, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
						SET_GOLF_PLAYER_SWING_ANIM(thisPlayerCore, SAS_IDLE)
					ENDIF
					#ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//Returns if animIdle0 is the animation currently being played
PROC GOLF_SWITCH_PUTT_IDLE_ANIM(PED_INDEX pedIndex, TEXT_LABEL_31 &animIdle0, TEXT_LABEL_31 &animIdle1, TEXT_LABEL_31 &animIdle2, BOOL bUseExtraAnimations)
		
	FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE()
	
	//If the golfer is still playing one of the other anims, dont switch
	IF IS_GOLF_IDLE_ANIMATION_PLAYING(pedIndex, animIdle0, bUseExtraAnimations) OR IS_GOLF_IDLE_ANIMATION_PLAYING(pedIndex, animIdle1, TRUE) 
	OR IS_GOLF_IDLE_ANIMATION_PLAYING(pedIndex, animIdle2, TRUE)
	 	EXIT
	 ENDIF
	
	IF fRand < 0.33
		//CDEBUG1LN(DEBUG_GOLF,"Switch to idle anim ", animIdle0)
		TASK_PLAY_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(bUseExtraAnimations), animIdle0, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
	ELIF fRand <0.66
		//CDEBUG1LN(DEBUG_GOLF,"Switch to idle anim ", animIdle1)
		TASK_PLAY_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animIdle1, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
	ELSE
		//CDEBUG1LN(DEBUG_GOLF,"Switch to idle anim ", animIdle2)
		TASK_PLAY_ANIM(pedIndex, GET_GOLF_ANIMATION_DICTIONARY_NAME(TRUE), animIdle2, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
	ENDIF

ENDPROC

PROC GOLF_MANAGE_PUTT_IDLE_ANIMS(PED_INDEX pedIndex, TEXT_LABEL_31 idleAnim, GOLF_HELPERS &thisHelpers, BOOL bUseExtraAnimations)
		
	TEXT_LABEL_31 animIdleA = idleAnim
	TEXT_LABEL_31 animIdleB = idleAnim
	TEXT_LABEL_31 animIdleC = idleAnim
	
	animIdleA += "_A"
	animIdleB += "_B"
	animIdleC += "_C"
	
	IF NOT IS_TIMER_STARTED(thisHelpers.idleTimer)
		START_TIMER_NOW(thisHelpers.idleTimer)
	ENDIF
	
	IF TIMER_DO_WHEN_READY(thisHelpers.idleTimer, 6.5)
		GOLF_SWITCH_PUTT_IDLE_ANIM(pedIndex, animIdleA, animIdleB, animIdleC, bUseExtraAnimations)
		RESTART_TIMER_NOW(thisHelpers.idleTimer)
	ENDIF

ENDPROC

FUNC BOOL IS_GOLF_REACTION_ALLOWED(GOLF_FOURSOME &thisFoursome, GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse, INT iShotExceededOffset = 0)

	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != COMPUTER
			
		IF IS_GOLF_SPLASH_DISPLAYING()
		AND thisHelpers.eLastSplashText != GOLF_SPLASH_GIR
			RETURN TRUE
		ENDIF
		
		IF  IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome) AND IS_SHOT_A_GIMME(thisCourse, thisFoursome, TRUE)
			RETURN TRUE
		ENDIF
		
		IF GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome) = LIE_CUP
			RETURN TRUE
		ENDIF
		
		#IF GOLF_IS_MP
			IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_NETWORK //shot is goint to be one behind
				iShotExceededOffset = -1
			ENDIF
		#ENDIF
		
		IF IS_SHOT_LIMIT_EXCEEDED(thisFoursome, thisCourse, -1 , iShotExceededOffset)
			RETURN TRUE
		ENDIF
		GOLF_OOB_REASON OOBReason
		IF IS_GOLF_BALL_OOB(thisFoursome, thisCourse, OOBReason)
			RETURN TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
			IF IS_ENTITY_IN_WATER(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
				RETURN TRUE
			ENDIF
		ENDIF
			
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC FLOAT GET_GOLF_REACTION_ANIMATION_PLAYING_TIME(STRING anim)

	IF ARE_STRINGS_EQUAL(anim, "react_win_club_02_michael")
		RETURN 0.841605
	ENDIF
	
	IF ARE_STRINGS_EQUAL(anim, "react_win_club_03_michael")
		RETURN 0.629133
	ENDIF
		
	IF ARE_STRINGS_EQUAL(anim, "react_win_club_03_trevor")
		RETURN 0.9
	ENDIF
	
	IF IS_GOLF_FOURSOME_MP()
		RETURN 0.6
	ENDIF
	
	RETURN 0.92
ENDFUNC

FUNC BOOL IS_GOLF_ANIMATION_NUETRAL_REACTION(STRING golfAnimation)

	IF ARE_STRINGS_EQUAL(golfAnimation, "swing_react_nuetral_01")
	OR ARE_STRINGS_EQUAL(golfAnimation, "wedge_react_nuetral_01")
	OR ARE_STRINGS_EQUAL(golfAnimation, "putt_react_nuetral_01")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GOLF_MANAGE_AI_POST_SHOT_ANIMATIONS(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, BOOL bAllowStart)

	PED_INDEX pedGolfer = GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome)
	STRING animClub = GET_SWING_ANIM_CLUB(thisGame.golfBag, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], TRUE)
	TEXT_LABEL_31 reactionAnim = animClub
	reactionAnim += "react_"
	reactionAnim += "nuetral_01"
	
	//is animation over
	IF IS_ENTITY_PLAYING_ANIM(pedGolfer,GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), reactionAnim)
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), reactionAnim ) > 0.95
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF bAllowStart
		IF NOT IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), reactionAnim )
		AND (IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), "iron_swing_action")
			OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), "putt_action")
			OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), "wedge_swing_action")
			OR IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), "wood_swing_action"))
			
			TASK_PLAY_ANIM(pedGolfer,GET_GOLF_ANIMATION_DICTIONARY_NAME(FALSE), reactionAnim, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GOLF_MANAGE_REACTION_CAMERA(GOLF_HELPERS &thisHelpers, GOLF_FOURSOME &thisFoursome, GOLF_PLAYER_CORE &thisPlayerCore, STRING sPassedAnimName = NULL)

	IF IS_GOLF_STREAMING_FLAG_SET(thisHelpers, GSF_REACTION_CAMERA_SET_UP)
		RETURN TRUE //camera already running
	ENDIF

	IF DOES_CAM_EXIST(thisHelpers.camReaction)
		DESTROY_CAM(thisHelpers.camReaction)
	ENDIF
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = COMPUTER
		CDEBUG1LN(DEBUG_GOLF,"Don't play reaction camera for computer")
		RETURN FALSE
	ENDIF
	
	
	thisHelpers.camReaction = CREATE_CAMERA(CAMTYPE_ANIMATED)

	BOOL bIsPutting = IS_GOLF_PLAYER_PUTTING(thisPlayerCore)
	PED_INDEX pedGolfer = GET_GOLF_PLAYER_PED(thisPlayerCore)
	VECTOR vGolferPos
	FLOAT fGolferHeading
	#IF NOT GOLF_IS_AI_ONLY
	vGolferPos = thisFoursome.vPositionBeforeReactionCam
	fGolferHeading = thisFoursome.fHeaingBeforeReactionCamera
	#ENDIF
	
	TEXT_LABEL_63 sAnimDict = GET_GOLF_REACTION_ANIM_DICT(pedGolfer, bIsPutting)
	TEXT_LABEL_63 sAnimName
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sPassedAnimName)
		CDEBUG1LN(DEBUG_GOLF,"Get anim cam from string")
		sAnimName = GET_GOLF_REACTION_CAMERA_FROM_STRING(sAnimDict, sPassedAnimName)
	ELSE
		sAnimName = GET_GOLF_REACTION_CAMERA_FROM_PED(pedGolfer, sAnimDict, bIsPutting, fGolferHeading)
	ENDIF
		
	CDEBUG1LN(DEBUG_GOLF,"Playing animated reaction camera, ", sAnimName, " dict ", sAnimDict)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sAnimName)
		IF IS_GOLF_FOURSOME_MP()
			IF NOT bIsPutting
				vGolferPos -= <<0, 0, 1>>
			ENDIF
		
			PLAY_CAM_ANIM(thisHelpers.camReaction , sAnimName, sAnimDict, vGolferPos, <<0, 0, fGolferHeading>>)
		ELSE
			GOLF_SET_REACTION_SYNC_SCENE_CAM(thisHelpers.camReaction, sAnimDict, sAnimName)
		ENDIF
		
		SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_REACTION_CAMERA_SET_UP)
		
		CLEAR_GOLF_UI_DISPLAY(thisHelpers, GOLF_DISPLAY_HOLE | GOLF_DISPLAY_SHOT)
		DISPLAY_HUD(FALSE)
		DISPLAY_RADAR(FALSE)
		RETURN TRUE
	ENDIF
//	SHAKE_CAM(thisHelpers.camReaction, "HAND_SHAKE", 0.2)

	RETURN FALSE
ENDFUNC

#IF NOT GOLF_IS_AI_ONLY
FUNC INT GET_REACTION_ANIM(GOLF_FOURSOME &thisFoursome, BOOL bGood, BOOL bGeneric, INT iDeafult)

	IF bGeneric
		RETURN iDeafult
	ENDIF

	IF thisFoursome.iBadReactionIndex <= 0
		thisFoursome.iBadReactionIndex = 1
	ENDIF
	IF thisFoursome.iGoodReactionIndex <= 0
		thisFoursome.iGoodReactionIndex = 1
	ENDIF
	
	INT iReturnIndex
	
	IF bGood
		iReturnIndex = thisFoursome.iGoodReactionIndex
		thisFoursome.iGoodReactionIndex++
		IF thisFoursome.iGoodReactionIndex > 3
			thisFoursome.iGoodReactionIndex = 1
		ENDIF
	ELSE
		iReturnIndex = thisFoursome.iBadReactionIndex
		thisFoursome.iBadReactionIndex++
		IF thisFoursome.iBadReactionIndex > 3
			thisFoursome.iBadReactionIndex = 1
		ENDIF
	ENDIF
	
	RETURN iReturnIndex
ENDFUNC

FUNC INT GET_REACTION_ANIM_MP(GOLF_FOURSOME &thisFoursome, BOOL bGood, BOOL bIsPutting)
	IF bGood AND NOT bIsPutting
		RETURN 1 //only acceptable animation for good not putting
	ENDIF

	IF thisFoursome.iBadReactionIndex <= 0
		thisFoursome.iBadReactionIndex = 1
	ENDIF
	
	IF thisFoursome.iGoodReactionIndex <= 0
		thisFoursome.iGoodReactionIndex = 1
	ENDIF
	
	INT iReturnIndex
	
	IF bGood
		iReturnIndex = thisFoursome.iGoodReactionIndex
		thisFoursome.iGoodReactionIndex++
		IF thisFoursome.iGoodReactionIndex > 3
			thisFoursome.iGoodReactionIndex = 1
		ENDIF
	ELSE
		iReturnIndex = thisFoursome.iBadReactionIndex
		thisFoursome.iBadReactionIndex++
		IF thisFoursome.iBadReactionIndex > 3
			thisFoursome.iBadReactionIndex = 1
		ENDIF
	ENDIF
	
	//only two variaitions for mp swings
	IF NOT bIsPutting AND iReturnIndex >= 3
		iReturnIndex = GET_RANDOM_INT_IN_RANGE(1, 3)
	ENDIF
	
	RETURN iReturnIndex
ENDFUNC

PROC ADD_REACT_ANIMATION_TO_TEXT_LABEL(TEXT_LABEL_63 &reactionAnim, GOLF_FOURSOME &thisFoursome, BOOL bGood, BOOL bIsPutting, BOOL bGeneric, INT iReactIndex, TEXT_LABEL_63 &sFaceAnimDict, TEXT_LABEL_63 &sFaceAnimName)

	IF IS_GOLF_FOURSOME_MP()
		iReactIndex = GET_REACTION_ANIM_MP(thisFoursome, bGood, bIsPutting)
	ELSE
		iReactIndex = GET_REACTION_ANIM(thisFoursome, bGood, bGeneric, iReactIndex)
	ENDIF
	
	GET_GOLF_FACE_REACTION(bGeneric, bGood, bIsPutting, iReactIndex, sFaceAnimDict, sFaceAnimName)
	
	IF bGood
		IF bGeneric
			reactionAnim += "good_0"
			reactionAnim += iReactIndex
		ELSE 
			IF NOT bIsPutting
				reactionAnim += "win_club_0"
			ELSE
				reactionAnim += "win_putter_0"
			ENDIF
			reactionAnim += iReactIndex
			reactionAnim += GET_GOLF_SINGLE_PLAYERS_NAME_FOR_REACTION(bIsPutting)
		ENDIF
	ELSE
		IF bGeneric
			reactionAnim += "bad_0"
			reactionAnim += iReactIndex
		ELSE 
			IF NOT bIsPutting
				reactionAnim += "lose_club_0"
			ELSE
				reactionAnim += "lose_putter_0"
			ENDIF
			reactionAnim += iReactIndex
			reactionAnim += GET_GOLF_SINGLE_PLAYERS_NAME_FOR_REACTION(bIsPutting)
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL GOLF_REACTION_SHOULD_HOLD_LAST_FRAME(GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse)
	
	//we are the player and it's the last hole
	IF IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_HUMAN_LOCAL(thisFoursome)
	AND GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome) = 8
		//The ball is going in the hole
		IF IS_BALL_IN_HOLE(thisCourse, thisFoursome) OR IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_SHOT_IS_GIMMIE)
			//every one but the player is done with the hole
			IF ARE_ALL_PLAYERS_DONE_WITH_HOLE(thisFoursome, 0)
			OR GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) = 1
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

//Returns TRUE if animation is still playing
FUNC BOOL GOLF_MANAGE_POST_SHOT_ANIMATIONS(GOLF_GAME &thisGame, GOLF_HELPERS &thisHelpers, GOLF_COURSE &thisCourse, GOLF_FOURSOME &thisFoursome)

	BOOL bIsPutting = IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
	PED_INDEX pedGolfer = GET_GOLF_FOURSOME_CURRENT_PLAYER_PED(thisFoursome)
	
	BOOL bGeneric = FALSE
	IF GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome) != GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
	OR IS_GOLF_FOURSOME_MP()
	OR (GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) = HUMAN_LOCAL AND NOT IS_GOLF_REACTION_ALLOWED(thisFoursome, thisHelpers, thisCourse, -1)) // Use generic reactions if we aren't going to show a camera closeup so we aren't stuck in swing on-camera
		bGeneric = TRUE
	ENDIF
	
	TEXT_LABEL_63 sAnimDict = GET_GOLF_REACTION_ANIM_DICT(pedGolfer, bIsPutting, bGeneric)
	
	GOLF_LIE_TYPE playerLie = GET_GOLF_FOURSOME_CURRENT_PLAYER_LIE(thisFoursome)
	
	STRING animClub = GET_SWING_ANIM_CLUB(thisGame.golfBag, thisFoursome.playerCore[GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)], TRUE, NOT IS_GOLF_FOURSOME_MP()) //No wedge reaction in mp
					
	TEXT_LABEL_63 reactionAnim
	
	TEXT_LABEL_63 sFaceAnimDict, sFaceAnimName
	
	IF NOT bGeneric
		reactionAnim = "react_"
	ELSE
		reactionAnim = animClub
		reactionAnim += "react_"
	ENDIF
	
	VECTOR vBallPos = GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL_POSITION(thisFoursome)
	IF DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
		vBallPos = GET_ENTITY_COORDS(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
	ENDIF
	
	IF NOT IS_STRING_EMPTY(thisFoursome.sCurrentReactionAnimation)
	
		//entity is not playing the anim, something went wrong, get out
		IF NOT IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, thisFoursome.sCurrentReactionAnimation)
			thisFoursome.sCurrentReactionAnimation = ""
			RETURN FALSE
		ENDIF
		FLOAT fReactionPhase
		IF IS_GOLF_FOURSOME_MP()
			fReactionPhase = GET_ENTITY_ANIM_CURRENT_TIME(pedGolfer, sAnimDict, thisFoursome.sCurrentReactionAnimation)
		ELSE
			fReactionPhase = GET_GOLF_REACTION_SCENE_PHASE()
		ENDIF
		
		CDEBUG1LN(DEBUG_GOLF, "Reaction phase, ", fReactionPhase)
		#IF GOLF_IS_MP
			IF fReactionPhase > 0.975
				SET_GOLF_STREAMING_FLAG(thisHelpers, GSF_FORCE_SCORECARD_VOL_LOAD)
			ENDIF
		#ENDIF
		
		//is animation over
		IF fReactionPhase > GET_GOLF_REACTION_ANIMATION_PLAYING_TIME(thisFoursome.sCurrentReactionAnimation)
			thisFoursome.sCurrentReactionAnimation = ""
			RETURN FALSE
		ENDIF
		RETURN TRUE
	ENDIF
	
	GOLF_OOB_REASON OOBReason
	
	IF IS_BALL_IN_HOLE(thisCourse, thisFoursome) OR IS_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL_FLAG_SET(thisFoursome, GOLF_SHOT_IS_GIMMIE)
		INT iShotsOnHole = GET_GOLF_FOURSOME_CURRENT_PLAYER_SHOTS_ON_HOLE(thisFoursome)+1
		INT iPar =  GET_GOLF_HOLE_PAR(thisCourse, GET_GOLF_FOURSOME_CURRENT_HOLE(thisFoursome))
		IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
			IF iShotsOnHole < iPar
				ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, TRUE, bIsPutting, bGeneric, 3, sFaceAnimDict, sFaceAnimName)
			ELIF iShotsOnHole = iPar
				ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, TRUE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
			ELSE
				ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, FALSE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
			ENDIF
		ELSE //chip in
//			IF iShotsOnHole <= iPar
//				txtQuality = "good_02"
//			ELSE
				ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, TRUE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
//			ENDIF
		ENDIF
	ELIF IS_SHOT_LIMIT_EXCEEDED(thisFoursome, thisCourse, -1, -1) //you didn't make it in on your last shot, you are now over the limit
		ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, FALSE, bIsPutting, bGeneric, 2, sFaceAnimDict, sFaceAnimName)
	ELIF IS_GOLF_BALL_OOB(thisFoursome, thisCourse, OOBReason)
		ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, FALSE, bIsPutting, bGeneric, 2, sFaceAnimDict, sFaceAnimName)
	ELIF playerLie = LIE_GREEN 
		IF IS_GOLF_FOURSOME_CURRENT_PLAYER_JUST_HIT_BALL_ON_GREEN(thisFoursome) // just hit it onto green
			ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, TRUE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
		ELIF  IS_SHOT_A_GIMME(thisCourse, thisFoursome, TRUE) // very close to hole but a miss
			IF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome) 
				ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, FALSE, bIsPutting, bGeneric, 3, sFaceAnimDict, sFaceAnimName)
			ELSE
				ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, FALSE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
			ENDIF
		ELSE
			ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, TRUE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
		ENDIF
	ELIF playerLie = LIE_FAIRWAY 
		IF thisFoursome.swingMeter.meterState = SWING_METER_SHANK
			ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, FALSE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
		ELIF VDIST2(GET_ENTITY_COORDS(pedGolfer), vBallPos) <(ACCEPTABLE_DRIVE_DIST*ACCEPTABLE_DRIVE_DIST)
		AND DOES_ENTITY_EXIST(GET_GOLF_FOURSOME_CURRENT_PLAYER_BALL(thisFoursome))
			CDEBUG1LN(DEBUG_GOLF,"didnt hit ball very far ", VDIST2(GET_ENTITY_COORDS(pedGolfer), vBallPos))
			ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, FALSE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
		ELIF WAS_TEE_SHOT(thisCourse, thisFoursome)
			ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, TRUE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
		ELSE
			ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, TRUE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
		ENDIF	
	ELIF playerLie = LIE_BUNKER OR playerLie = LIE_ROUGH
		ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, FALSE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
	ELSE
		ADD_REACT_ANIMATION_TO_TEXT_LABEL(reactionAnim, thisFoursome, TRUE, bIsPutting, bGeneric, 1, sFaceAnimDict, sFaceAnimName)
	ENDIF
	
	VECTOR vScenePos = GET_ENTITY_COORDS(pedGolfer)
	FLOAT fSceneHeading = GET_ENTITY_HEADING(pedGolfer)
	
//	#IF NOT GOLF_IS_AI_ONLY
//	vScenePos = thisFoursome.vPositionBeforeReactionCam - <<0,0,1>>
//	fSceneHeading = thisFoursome.fHeaingBeforeReactionCamera + fRingAlpha
//	#ENDIF
	
	IF bGeneric AND NOT bIsPutting
		vScenePos -= <<0,0,1>>
		fSceneHeading -= 90
	ELSE
		IF bIsPutting
		
		ELSE
			vScenePos -= <<0,0,1>>
		ENDIF
	ENDIF
	
	ANIMATION_FLAGS animFlags = AF_DEFAULT
	
	IF IS_GOLF_FOURSOME_MP()
		animFlags = AF_HOLD_LAST_FRAME
	ENDIF
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
		IF NOT IS_ENTITY_PLAYING_ANIM(pedGolfer, sAnimDict, reactionAnim )
			//SET_ENTITY_HEADING(pedGolfer, GET_ENTITY_HEADING(pedGolfer) - 90)
			#IF NOT GOLF_IS_AI_ONLY CDEBUG1LN(DEBUG_GOLF,"play reaction anim ", reactionAnim, " dict ", sAnimDict) #ENDIF
//			TASK_PLAY_ANIM(pedGolfer, sAnimDict, reactionAnim, REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, animFlags)
			CDEBUG1LN(DEBUG_GOLF,"Playing reaction scene from! ", vScenePos)
			vGolfReactScenePosition = vScenePos
			fGoflReactSceneHeaing = fSceneHeading

			IF NOT bGeneric
				GOLF_CREATE_REACTION_SYNC_SCENE(IS_GOLF_REACTION_ALLOWED(thisFoursome, thisHelpers, thisCourse))
				GOLF_TASK_REACTION_SYNC_SCENE(pedGolfer, sAnimDict, reactionAnim, REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
				GOLF_START_REACTION_SYNC_SCENE()
			ELSE
				TASK_PLAY_ANIM(pedGolfer, sAnimDict, reactionAnim, REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, animFlags)
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sFaceAnimDict) AND NOT IS_STRING_NULL_OR_EMPTY(sFaceAnimName)
				CDEBUG1LN(DEBUG_GOLF, "Playing face anim! ", sFaceAnimDict, " ", sFaceAnimName)
				TASK_PLAY_ANIM(pedGolfer, sFaceAnimDict, sFaceAnimName, NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
			ENDIF
			
			thisFoursome.sCurrentReactionAnimation = reactionAnim
		ELSE
			//player is playing anim but sCurrentReactionAnimation is not set?
			thisFoursome.sCurrentReactionAnimation = reactionAnim
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

#ENDIF

// New golf animation
PROC GOLF_MANAGE_SWING_ANIMS( GOLF_GAME &thisGame, GOLF_PLAYER_CORE &thisPlayerCore, SWING_METER &swingMeter #IF NOT GOLF_IS_AI_ONLY,  GOLF_HELPERS &thisHelpers #ENDIF)

	IF IS_GOLF_PLAYER_CONTROL_FLAG_SET(thisPlayerCore, GOLF_FAR_AWAY)
	OR GET_GOLF_PLAYER_STATE(thisPlayerCore) = GPS_BALL_IN_FLIGHT
		EXIT
	ENDIF

	SWITCH GET_GOLF_PLAYER_SWING_STYLE(thisPlayerCore)
		CASE SWING_STYLE_NORMAL	// Normal swing, no power adjustments
		CASE SWING_STYLE_POWER  // Power swing, slight power increase for loss of accuracy
		CASE SWING_STYLE_APPROACH // Low power swing with increased spin and accuracy
		CASE SWING_STYLE_PUNCH // Low power, low trajectory shot with strong backspin
			GOLF_MANAGE_REGULAR_SWING_ANIMS(thisGame, thisPlayerCore, swingMeter #IF NOT GOLF_IS_AI_ONLY, thisHelpers #ENDIF)
		BREAK
		CASE SWING_STYLE_GREEN_SHORT  // Putting 
		CASE SWING_STYLE_GREEN  // Putting
		CASE SWING_STYLE_GREEN_LONG
		CASE SWING_STYLE_GREEN_TAP
			GOLF_MANAGE_REGULAR_SWING_ANIMS(thisGame, thisPlayerCore, swingMeter #IF NOT GOLF_IS_AI_ONLY, thisHelpers #ENDIF)
		BREAK
	ENDSWITCH

ENDPROC

FUNC BOOL SHOULD_GOLFER_DO_APPROACH_ANIMATION(GOLF_FOURSOME &thisFoursome)

	#IF GOLF_IS_AI_ONLY
		RETURN FALSE
	#ENDIF

	IF GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome) = GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
	AND NOT IS_GOLF_FOURSOME_MP() AND NOT IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SETUP_GOLF_SPECTATOR_IDLE_ANIM(GOLF_GAME &thisGame, GOLF_FOURSOME &thisFoursome, GOLF_COURSE &thisCourse)

	INT iGolferIndex
	eCLUB_TYPE eClub
	
	IF thisCourse.iTotalPar = 0
	//supress errors
	ENDIF
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iGolferIndex
		PED_INDEX pedGolfer = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iGolferIndex)
		
		IF iGolferIndex != GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
		AND NOT IS_PED_INJURED(pedGolfer)
//			IF IS_TEE_SHOT(thisCourse, thisFoursome)
//				eClub = eCLUB_WOOD_1
//			ELIF IS_GOLF_FOURSOME_CURRENT_PLAYER_PUTTING(thisFoursome)
//				eClub = eCLUB_PUTTER
//			ELSE
				eClub = eCLUB_IRON_3
//			ENDIF
			IF iGolferIndex != GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
			AND NOT IS_PED_IN_ANY_VEHICLE(pedGolfer)
				SET_GOLF_FOURSOME_INDEXED_PLAYER_CURRENT_CLUB(thisFoursome, iGolferIndex, GOLF_FIND_SPECIFIC_CLUB_IN_BAG(thisGame.golfBag, eClub))
				GOLF_ATTACH_CLUB_TO_PLAYER(thisFoursome.playerCore[iGolferIndex], thisGame.golfBag)
				
				TASK_PLAY_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(TRUE), "base", REALLY_SLOW_BLEND_IN,  REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
			ELSE
				CLEANUP_GOLF_FOURSOME_INDEXED_PLAYER_CLUB(thisFoursome, iGolferIndex)
			ENDIF
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGolfer)
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC SET_GOLF_PED_IDLE_ANIM(GOLF_FOURSOME &thisFoursome, INT iGolferIndex)
	TEXT_LABEL_7 sAnimName
	INT iRandomInt = GET_RANDOM_INT_IN_RANGE(0, 1000)
	
	PED_INDEX pedGolfer = GET_GOLF_FOURSOME_INDEXED_PLAYER_PED(thisFoursome, iGolferIndex)
	
	IF NOT IS_PED_INJURED(pedGolfer)
		IF NOT IS_PED_IN_ANY_VEHICLE(pedGolfer)
			IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(TRUE), "base")
			OR NOT IS_GOLF_PED_PLAYING_ANY_IDLE_ANIM(pedGolfer)
			OR IS_GOLF_PED_AT_END_OF_IDLE_ANIM(pedGolfer)
				IF iRandomInt < 10 //1% chance per frame of playing idle anim
				OR NOT IS_GOLF_PED_PLAYING_ANY_IDLE_ANIM(pedGolfer)
				OR IS_GOLF_PED_AT_END_OF_IDLE_ANIM(pedGolfer)
				
					IF IS_ENTITY_PLAYING_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(TRUE), "base")
						sAnimName = "idle_"
						IF iRandomInt%3 = 0
							sAnimName += "a"
						ELIF iRandomInt%3 = 1
							sAnimName += "b"
						ELSE
							sAnimName += "c"
						ENDIF
							
						TASK_PLAY_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(FALSE), sAnimName, REALLY_SLOW_BLEND_IN,  REALLY_SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
					ELSE
						TASK_PLAY_ANIM(pedGolfer, GET_GOLF_IDLE_ANIMATION_DICTIONARY(TRUE), "base", REALLY_SLOW_BLEND_IN,  REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_GOLF_SPECTATOR_IDLE_ANIM(GOLF_FOURSOME &thisFoursome)

	INT iGolferIndex
	
	REPEAT GET_GOLF_FOURSOME_NUM_PLAYERS(thisFoursome) iGolferIndex
	
		IF iGolferIndex != GET_GOLF_FOURSOME_CURRENT_PLAYER(thisFoursome)
		AND iGolferIndex != GET_GOLF_FOURSOME_LOCAL_PLAYER(thisFoursome)
			SET_GOLF_PED_IDLE_ANIM(thisFoursome, iGolferIndex)
		ENDIF
	ENDREPEAT

ENDPROC
