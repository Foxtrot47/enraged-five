USING "minigames_helpers.sch"


FUNC INT GET_GOLF_NUMBER_OF_UNLOCKED_BUDDIES()
	GOLF_BUDDIES golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, BIT0)
	INT total = 0
	
	WHILE golfBuddy < GB_MAX_BUDDIES
		IF IS_GOLF_BUDDY_UNLOCKED(g_savedGlobals.sGolfData, golfBuddy)
			total++
		ENDIF
		golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, SHIFT_LEFT(ENUM_TO_INT(golfBuddy), 1))
	ENDWHILE

	RETURN total
ENDFUNC

//Get an unlocked buddie, index is the bit you want to get amongst the set bits
//For example with an unlocked flag l101 and an index of 3, the function will return 
//the buddy corisponding to the last bit because it is the 3rd set bit 
FUNC GOLF_BUDDIES GET_GOLF_INDEXED_UNLOCKED_BUDDY(INT index)
	
	GOLF_BUDDIES golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, BIT0)
	
	WHILE golfBuddy < GB_MAX_BUDDIES
		
		IF IS_GOLF_BUDDY_UNLOCKED(g_savedGlobals.sGolfData, golfBuddy)
			IF index = 0
				RETURN golfBuddy
			ENDIF
			index--
		ENDIF
		
		golfBuddy = INT_TO_ENUM(GOLF_BUDDIES, SHIFT_LEFT(ENUM_TO_INT(golfBuddy), 1))
	ENDWHILE
	
	RETURN GB_NONE
ENDFUNC
