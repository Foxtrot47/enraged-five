USING "golf.sch"
USING "Net_Script_Timers.sch"
USING "fmmc_restart_header.sch"


ENUM GOLF_MP_GAME_STATE
	GAME_STATE_UNDEFINED = -1,
	GAME_STATE_INI,
	GAME_STATE_INI_SPAWN,
	GAME_STATE_RUNNING
ENDENUM

STRUCT	GOLF_MP_PLAYER_SCORESHEET
	GOLF_PLAYER_COURSE_SCORE 	iCourseScore[18]
	INT							iCurrentScore
ENDSTRUCT


// The server broadcast data
// Everyone can read this data, only the server can update it
STRUCT GOLF_MP_SERVER_BROADCAST_DATA
	GOLF_MP_GAME_STATE 			eServerGameState
	GOLF_MINIGAME_STATE 		eServerGolfState
	GOLF_CONTROL_FLAGS			golfServerControlFlags
	GOLF_CONTROL_FLAGS2         golfServerControlFlags2
	GOLF_MP_PLAYER_SCORESHEET	serverScore[MP_GOLF_MAX_PLAYERS]
	NETWORK_INDEX				hostGolfBallIndex[MP_GOLF_MAX_PLAYERS]
	INT 						iCurrentShooterNetIndex			
	INT 						iCurrentHole
	INT							iPlayerWhoQuitIndex
	INT							iWeather
	FLOAT						fWindStrength
	VECTOR 						vWindDirection
	INT							iHashMac, iPosixTime = -1
	INT							iMPTimer, iStartHole, iEndHole
	SERVER_EOM_VARS  			sServerFMMC_EOM
	SCRIPT_TIMER				timeLeaderboardTimeOut
ENDSTRUCT

/// PURPOSE:
///    Checks scorecard quit button - different on PC.
/// RETURNS:
///    TRUE if quit button is pressed
FUNC BOOL IS_GOLF_MP_SCORECARD_QUIT_PRESSED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)

		IF NETWORK_TEXT_CHAT_IS_TYPING()
			RETURN FALSE
		ENDIF
	
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB)
			RETURN TRUE
		ENDIF
		
	ELSE
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

// SERVER BD ACCESSORS
/// PURPOSE: Helper function to get the servers game/mission state
/// RETURNS: The server game state.
FUNC GOLF_MP_GAME_STATE GET_GOLF_MP_SERVER_MISSION_STATE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD)
	RETURN serverBD.eServerGameState
ENDFUNC

PROC SET_GOLF_MP_SERVER_MISSION_STATE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MP_GAME_STATE eServerGameState)
	serverBD.eServerGameState = eServerGameState
ENDPROC

/// PURPOSE: Helper function to get the servers game/mission state
/// RETURNS: The server game state.
FUNC GOLF_MINIGAME_STATE GET_GOLF_MP_SERVER_GOLF_STATE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD)
	RETURN serverBD.eServerGolfState
ENDFUNC

PROC SET_GOLF_MP_SERVER_GOLF_STATE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_MINIGAME_STATE eServerGolfState)
	serverBD.eServerGolfState = eServerGolfState
ENDPROC

/// PURPOSE: Helper function to get the servers game/mission state
/// RETURNS: The server game state.
FUNC GOLF_CONTROL_FLAGS GET_GOLF_MP_SERVER_CONTROL_FLAGS(GOLF_MP_SERVER_BROADCAST_DATA &serverBD)
	RETURN serverBD.golfServerControlFlags
ENDFUNC

PROC OVERWRITE_GOLF_MP_SERVER_CONTROL_FLAGS(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_CONTROL_FLAGS golfServerControlFlags)
	serverBD.golfServerControlFlags = golfServerControlFlags
ENDPROC

PROC SET_GOLF_MP_SERVER_CONTROL_FLAG(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_CONTROL_FLAGS golfServerControlFlags)
	serverBD.golfServerControlFlags = serverBD.golfServerControlFlags | golfServerControlFlags
ENDPROC

PROC CLEAR_GOLF_MP_SERVER_CONTROL_FLAG(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_CONTROL_FLAGS golfServerControlFlags)
	serverBD.golfServerControlFlags -= serverBD.golfServerControlFlags & golfServerControlFlags
ENDPROC

FUNC BOOL IS_GOLF_MP_SERVER_CONTROL_FLAG_SET(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_CONTROL_FLAGS golfServerControlFlags)
	RETURN (serverBD.golfServerControlFlags & golfServerControlFlags) != GCF_NO_FLAGS
ENDFUNC

//Control flag 2

PROC SET_GOLF_MP_SERVER_CONTROL_FLAG2(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_CONTROL_FLAGS2 golfServerControlFlags2)
	serverBD.golfServerControlFlags2 = serverBD.golfServerControlFlags2 | golfServerControlFlags2
ENDPROC

PROC CLEAR_GOLF_MP_SERVER_CONTROL_FLAG2(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_CONTROL_FLAGS2 golfServerControlFlags2)
	serverBD.golfServerControlFlags2 -= serverBD.golfServerControlFlags2 & golfServerControlFlags2
ENDPROC

FUNC BOOL IS_GOLF_MP_SERVER_CONTROL_FLAG_SET2(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, GOLF_CONTROL_FLAGS2 golfServerControlFlags2)
	RETURN (serverBD.golfServerControlFlags2 & golfServerControlFlags2) != GCF2_NO_FLAG
ENDFUNC


/// PURPOSE: Helper function to get the servers game/mission state
/// RETURNS: The server game state.
FUNC INT GET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(GOLF_MP_SERVER_BROADCAST_DATA &serverBD)
	RETURN serverBD.iCurrentShooterNetIndex
ENDFUNC

PROC SET_GOLF_MP_SERVER_CURRENT_SHOOTER_NET_INDEX(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, INT iCurrentShooterNetIndex)
	CDEBUG1LN(DEBUG_GOLF, "Setting current player to ", iCurrentShooterNetIndex)
	serverBD.iCurrentShooterNetIndex = iCurrentShooterNetIndex
ENDPROC

/// PURPOSE: Helper function to get the servers game/mission state
/// RETURNS: The server game state.
FUNC INT GET_GOLF_MP_SERVER_CURRENT_HOLE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD)
	RETURN serverBD.iCurrentHole
ENDFUNC

PROC SET_GOLF_MP_SERVER_CURRENT_HOLE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, INT iCurrentHole)
	serverBD.iCurrentHole = iCurrentHole
ENDPROC

PROC INCREMENT_GOLF_MP_SERVER_CURRENT_HOLE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD)
	serverBD.iCurrentHole++
	
	IF serverBD.iCurrentHole > 8
		serverBD.iCurrentHole = 0
	ENDIF
ENDPROC

FUNC INT GET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, INT iPlayer)
	IF iPlayer < 0 OR iPlayer >= COUNT_OF(serverBD.serverScore)
		RETURN 0
	ENDIF
	RETURN serverBD.serverScore[iPlayer].iCurrentScore
ENDFUNC

PROC SET_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, INT iPlayer, INT iCurrentScore)
	IF iPlayer < 0 OR iPlayer >= COUNT_OF(serverBD.serverScore)
		EXIT
	ENDIF
	serverBD.serverScore[iPlayer].iCurrentScore = iCurrentScore
ENDPROC


PROC ADD_GOLF_MP_SERVER_PLAYER_CURRENT_SCORE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, INT iPlayer, INT iAddScore)
	IF iPlayer < 0 OR iPlayer >= COUNT_OF(serverBD.serverScore)
		EXIT
	ENDIF
	serverBD.serverScore[iPlayer].iCurrentScore += iAddScore
ENDPROC


FUNC INT GET_GOLF_MP_SERVER_PLAYER_HOLE_SCORE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, INT iPlayer, INT iHole)
	IF iPlayer < 0 OR iPlayer >= COUNT_OF(serverBD.serverScore)
		RETURN 0
	ENDIF
	IF iHole < 0 OR iHole >= COUNT_OF(serverBD.serverScore[].iCourseScore)
		RETURN 0
	ENDIF
	RETURN serverBD.serverScore[iPlayer].iCourseScore[iHole].iHoleScore
ENDFUNC

PROC SET_GOLF_MP_SERVER_PLAYER_HOLE_SCORE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, INT iPlayer, INT iHole, INT iHoleScore)
	IF iPlayer < 0 OR iPlayer >= COUNT_OF(serverBD.serverScore)
		EXIT
	ENDIF
	IF iHole < 0 OR iHole >= COUNT_OF(serverBD.serverScore[].iCourseScore)
		EXIT
	ENDIF
	serverBD.serverScore[iPlayer].iCourseScore[iHole].iHoleScore = iHoleScore
ENDPROC


FUNC INT GET_GOLF_MP_SERVER_PLAYER_CURRENT_HOLE_SCORE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, INT iPlayer)
	IF iPlayer < 0 OR iPlayer >= COUNT_OF(serverBD.serverScore)
		RETURN 0
	ENDIF
	IF serverBD.iCurrentHole < 0 OR serverBD.iCurrentHole >= COUNT_OF(serverBD.serverScore[].iCourseScore)
		RETURN 0
	ENDIF
	RETURN serverBD.serverScore[iPlayer].iCourseScore[serverBD.iCurrentHole].iHoleScore
ENDFUNC

PROC SET_GOLF_MP_SERVER_PLAYER_CURRENT_HOLE_SCORE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, INT iPlayer, INT iHoleScore)
	IF iPlayer < 0 OR iPlayer >= COUNT_OF(serverBD.serverScore)
		EXIT
	ENDIF
	IF serverBD.iCurrentHole < 0 OR serverBD.iCurrentHole >= COUNT_OF(serverBD.serverScore[].iCourseScore)
		EXIT
	ENDIF
	serverBD.serverScore[iPlayer].iCourseScore[serverBD.iCurrentHole].iHoleScore = iHoleScore
ENDPROC

PROC INCREMENT_GOLF_MP_SERVER_PLAYER_CURRENT_HOLE_SCORE(GOLF_MP_SERVER_BROADCAST_DATA &serverBD, INT iPlayer)
	IF iPlayer < 0 OR iPlayer >= COUNT_OF(serverBD.serverScore)
		EXIT
	ENDIF
	IF serverBD.iCurrentHole < 0 OR serverBD.iCurrentHole >= COUNT_OF(serverBD.serverScore[].iCourseScore)
		EXIT
	ENDIF
	serverBD.serverScore[iPlayer].iCourseScore[serverBD.iCurrentHole].iHoleScore++
ENDPROC

FUNC BOOL GOLF_SHOULD_MP_MENUS_BE_DISABLED(GOLF_MINIGAME_STATE eGameState, GOLF_FOURSOME &thisFoursome)

	IF IS_GOLF_FOURSOME_STATE_AT_END(thisFoursome)
		RETURN FALSE
	ENDIF
	
	IF eGameState = GMS_OUTRO
		RETURN TRUE
	ENDIF
	
	IF GET_GOLF_FOURSOME_CURRENT_PLAYER_STATE(thisFoursome) >= GPS_BALL_IN_FLIGHT
	AND GET_GOLF_FOURSOME_CURRENT_PLAYER_CONTROL(thisFoursome) != HUMAN_NETWORK
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC
