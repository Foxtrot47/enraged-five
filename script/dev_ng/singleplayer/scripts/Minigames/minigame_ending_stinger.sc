

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// minigame_ending_stinger.sc
USING "commands_player.sch"
USING "commands_script.sch"
USING "player_ped_public.sch"

PROC SCRIPT_CLEANUP()
	STOP_STREAM()
	TERMINATE_THIS_THREAD()
ENDPROC


SCRIPT
	PRINTLN("***** MINIGAME ENDING STINGER *****")
	
	INT iTimePlayed = -1
	BOOL bPlayed = FALSE
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		SCRIPT_CLEANUP()
	ENDIF
	
	WHILE TRUE
		IF NOT bPlayed
			// We haven't played the stream yet. Wait for it to load and play it.
			SWITCH (GET_PLAYER_PED_ENUM(PLAYER_PED_ID()))
				CASE CHAR_FRANKLIN
					IF LOAD_STREAM("MISSION_COMPLETE_FRANKLIN_SMALL")
						PLAY_STREAM_FRONTEND()
						bPlayed = TRUE
					ENDIF
				BREAK
				
		    	CASE CHAR_TREVOR
					IF LOAD_STREAM("MISSION_COMPLETE_TREVOR_SMALL")
						PLAY_STREAM_FRONTEND()
						bPlayed = TRUE
					ENDIF
				BREAK
				
				DEFAULT
					// Putting Michael in the default case, because this will also be the case for all MP minigames.
					IF LOAD_STREAM("MISSION_COMPLETE_MICHAEL_SMALL")
						PLAY_STREAM_FRONTEND()
						bPlayed = TRUE		
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			// We've played the stream.
			IF (iTimePlayed = -1)
				// If we've never marked when we played it, store that.
				iTimePlayed = GET_GAME_TIMER()
			ELSE
				// After playing for 6 seconds, we assume it's done. Kill the thread.
				IF (GET_GAME_TIMER() > iTimePlayed + 8000)
					SCRIPT_CLEANUP()
				ENDIF
			ENDIF
		ENDIF
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT
