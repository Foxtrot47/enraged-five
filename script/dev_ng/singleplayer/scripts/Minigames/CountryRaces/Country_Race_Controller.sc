USING "script_launch_control.sch"
USING "candidate_public.sch"
USING "country_race_header.sch"

CONST_INT STREAM_IN_DIST 		290 * 290
CONST_INT STREAM_OUT_DIST 		300 * 300

ENUM COUNTRY_RACE_CONTROLLER_STATE
	STATE_IDLE,
	STATE_INITIALISE_RACE,
	STATE_RACE_ACTIVE,
	STATE_RESETTING
ENDENUM

COUNTRY_RACE_CONTROLLER_STATE e_ControllerState = STATE_IDLE
m_enumMissionCandidateReturnValue 	eCandidateReturn

VEHICLE_INDEX ve_TriggerCar
PED_INDEX p_TriggerDriver
//BOOL bDisrupted = FALSE
//BOOL bRaceJustCompleted = FALSE
INT iRaceSpeechTimer
BOOL b_HintCamReady = TRUE

FUNC TEXT_LABEL GET_RACE_HELP_TEXT()
	TEXT_LABEL text = "CRACEHELP"
	text += (g_savedGlobals.sCountryRaceData.iCurrentRace + 1)
	RETURN text
ENDFUNC

/// PURPOSE:
///    Terminates the thread in case of emergency.
PROC CONTROLLER_CLEANUP(BOOL bForce = FALSE)
	Mission_Over(g_iRaceMissionCandidateID)
	
	IF IS_ENTITY_ALIVE(p_TriggerDriver)
		IF IS_SCRIPT_TASK_RUNNING_OR_STARTING( p_TriggerDriver, SCRIPT_TASK_START_SCENARIO_AT_POSITION )
		OR IS_SCRIPT_TASK_RUNNING_OR_STARTING( p_TriggerDriver, SCRIPT_TASK_START_SCENARIO_IN_PLACE )
			TASK_WANDER_STANDARD( p_TriggerDriver )		
		ENDIF
	ENDIF
	REMOVE_PED( p_TriggerDriver, bForce )
	REMOVE_VEHICLE( ve_TriggerCar, bForce )
	IF bForce
		CLEAR_AREA(GET_RACE_TRIGGER_LOCATION(), 100, TRUE)
	ENDIF
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	b_HintCamReady = TRUE
	TEXT_LABEL text = GET_RACE_HELP_TEXT()
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("C_RACE", MINIGAME_TEXT_SLOT)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRACEDISRUPT")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRACECAR")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(text)
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	SET_SCRIPT_AS_NO_LONGER_NEEDED("Country_Race")
	TERMINATE_THIS_THREAD()
ENDPROC
//
///// PURPOSE:
/////    Creates the racer at the race trigger.
//PROC CREATE_TRIGGER_DRIVER(BOOL bWait = FALSE)
//
//	IF NOT DOES_ENTITY_EXIST( p_TriggerDriver )
//
//		REQUEST_MODEL( A_M_Y_MotoX_02 )
//		
//		IF bWait
//			WHILE NOT HAS_MODEL_LOADED(A_M_Y_MotoX_02)
//				WAIT(0)
//			ENDWHILE
//		ENDIF
//		
//		IF HAS_MODEL_LOADED( A_M_Y_MotoX_02 )
//			p_TriggerDriver = CREATE_PED( PEDTYPE_MISSION, A_M_Y_MotoX_02, GET_TRIGGER_PED_COORDS(), GET_TRIGGER_PED_HEADING() )
//			SET_TRIGGER_PED_COMPONENTS(p_TriggerDriver)
//			SET_AMBIENT_VOICE_NAME(p_TriggerDriver, "A_M_Y_RACER_01_WHITE_MINI_01")	
//			TASK_START_SCENARIO_IN_PLACE( p_TriggerDriver, "WORLD_HUMAN_STAND_IMPATIENT", -1, TRUE )
//			SET_MODEL_AS_NO_LONGER_NEEDED( A_M_Y_MotoX_02 )
//		ENDIF
//	
//	ENDIF
//
//ENDPROC

/// PURPOSE:
///    Creates the race car at the race trigger.
PROC CREATE_TRIGGER_CAR_AND_DRIVER(BOOL bWait = FALSE)
	
	IF NOT DOES_ENTITY_EXIST( ve_TriggerCar )
	AND NOT DOES_ENTITY_EXIST( p_TriggerDriver )
		
		REQUEST_MODEL( GET_TRIGGER_CAR_MODEL() )
		REQUEST_MODEL( A_M_Y_MotoX_02 )
		
		IF bWait
			WHILE NOT HAS_MODEL_LOADED(GET_TRIGGER_CAR_MODEL())
				WAIT(0)
			ENDWHILE
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_MotoX_02)
				WAIT(0)
			ENDWHILE
		ENDIF

		IF HAS_MODEL_LOADED( GET_TRIGGER_CAR_MODEL() )
		AND HAS_MODEL_LOADED( A_M_Y_MotoX_02 )
			ve_TriggerCar = CREATE_VEHICLE( GET_TRIGGER_CAR_MODEL(), GET_TRIGGER_CAR_COORDS(), GET_TRIGGER_CAR_HEADING() )
			SET_VEHICLE_ON_GROUND_PROPERLY( ve_TriggerCar )
			IF g_savedGlobals.sCountryRaceData.iCurrentRace = COUNTRY_RACE_5
				SET_VEHICLE_LIVERY(ve_TriggerCar, 0)
				SET_VEHICLE_COLOURS(ve_TriggerCar, 44, 83)
				SET_VEHICLE_EXTRA_COLOURS(ve_TriggerCar, 111, 111)				
			ENDIF
			SET_VEHICLE_DOORS_LOCKED( ve_TriggerCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY )
			SET_VEHICLE_AUTOMATICALLY_ATTACHES( ve_TriggerCar, FALSE ) 
			SET_VEHICLE_DISABLE_TOWING( ve_TriggerCar, TRUE )
			SET_MODEL_AS_NO_LONGER_NEEDED( GET_TRIGGER_CAR_MODEL() )
			p_TriggerDriver = CREATE_PED( PEDTYPE_MISSION, A_M_Y_MotoX_02, GET_TRIGGER_PED_COORDS(), GET_TRIGGER_PED_HEADING() )
			SET_TRIGGER_PED_COMPONENTS(p_TriggerDriver)
			SET_AMBIENT_VOICE_NAME(p_TriggerDriver, "A_M_Y_RACER_01_WHITE_MINI_01")	
			TASK_START_SCENARIO_IN_PLACE( p_TriggerDriver, "WORLD_HUMAN_STAND_IMPATIENT", -1, TRUE )
			SET_MODEL_AS_NO_LONGER_NEEDED( A_M_Y_MotoX_02 )
		ENDIF
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if the player has disrupted the trigger by destroying the car or startling the trigger ped.
/// RETURNS:
///    If the trigger has been disrupted.
FUNC BOOL IS_PLAYER_FANNYING_ABOUT_AT_TRIGGER()
	
	IF DOES_ENTITY_EXIST(p_TriggerDriver)	
		IF IS_ENTITY_DEAD( p_TriggerDriver )
			RETURN TRUE
		ELSE
			IF IS_PED_FLEEING( p_TriggerDriver )	
				RETURN TRUE	
			ELIF IS_PED_IN_COMBAT( p_TriggerDriver )	
				RETURN TRUE	
			ELIF IS_PED_RAGDOLL( p_TriggerDriver )
				RETURN TRUE
			ELIF IS_PED_SHOOTING(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(ve_TriggerCar)
		IF IS_ENTITY_DEAD( ve_TriggerCar )
			RETURN TRUE
		ELSE	
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ve_TriggerCar, GET_TRIGGER_CAR_COORDS()) > 2
				RETURN TRUE
			ENDIF
			IF HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(PLAYER_PED_ID(), ve_TriggerCar)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FLOAT f_LastVehSpeed

//=============================================== 
// Check if player has rammed into a vehicle 
//=============================================== 
FUNC BOOL HAS_PLAYER_RAMMED_VEHICLE(VEHICLE_INDEX veh) 
	VEHICLE_INDEX playerVeh = GET_PLAYERS_LAST_VEHICLE() 

	IF IS_ENTITY_ALIVE(veh) 
		IF IS_ENTITY_ALIVE(playerVeh) 
		AND f_LastVehSpeed != 0 
		AND f_LastVehSpeed >= 9 
			FLOAT fCurrentSpeed = GET_ENTITY_SPEED(playerVeh) 

			IF IS_ENTITY_TOUCHING_ENTITY(veh, playerVeh) 
			AND ( fCurrentSpeed <= (f_LastVehSpeed * 0.5) ) 
				RETURN TRUE 
			ENDIF
		ENDIF
			
		IF GET_ENTITY_HEALTH(veh) + 100 < GET_ENTITY_MAX_HEALTH(veh)
			RETURN TRUE 
		ENDIF
	ENDIF


	RETURN FALSE 
ENDFUNC



/// PURPOSE:
///    Handles the trigger ped AI.
PROC HANDLE_TRIGGER_DRIVER()
	
	IF IS_ENTITY_ALIVE(p_TriggerDriver)
		IF NOT IS_PED_FLEEING( p_TriggerDriver ) AND NOT IS_PED_IN_COMBAT( p_TriggerDriver ) AND NOT IS_PED_RAGDOLL( p_TriggerDriver )	
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(p_TriggerDriver, SCRIPT_TASK_START_SCENARIO_IN_PLACE)		
				TASK_START_SCENARIO_IN_PLACE( p_TriggerDriver, "WORLD_HUMAN_STAND_IMPATIENT", -1, TRUE )
			ENDIF
			IF HAS_PLAYER_RAMMED_VEHICLE(ve_TriggerCar) OR g_savedGlobals.sCountryRaceData.bDisrupted
				SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(p_TriggerDriver)
				TASK_COMBAT_PED( p_TriggerDriver, PLAYER_PED_ID() )
			ENDIF
			f_LastVehSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID()) 
		ELIF IS_PED_RAGDOLL( p_TriggerDriver ) 
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(p_TriggerDriver, SCRIPT_TASK_COMBAT)
				SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(p_TriggerDriver)
				TASK_COMBAT_PED( p_TriggerDriver, PLAYER_PED_ID() )
			ENDIF	
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Handles the trigger streaming and disruption checks.
/// RETURNS:
///    Whether the trigger is safe to progress to detailed checks.
FUNC BOOL STREAM_TRIGGER_ASSETS()	
	
	IF NOT g_savedGlobals.sCountryRaceData.bDisrupted
	AND NOT g_savedGlobals.sCountryRaceData.bRaceJustCompleted
	AND VDIST2( GET_ENTITY_COORDS( PLAYER_PED_ID() ), GET_RACE_TRIGGER_LOCATION() ) < STREAM_IN_DIST 

		g_savedGlobals.sCountryRaceData.bDisrupted = IS_PLAYER_FANNYING_ABOUT_AT_TRIGGER()

		HANDLE_TRIGGER_DRIVER()

		IF NOT g_savedGlobals.sCountryRaceData.bDisrupted		
			IF IS_ENTITY_ALIVE( p_TriggerDriver ) AND IS_ENTITY_ALIVE( ve_TriggerCar )	
				IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("C_RACE", MINIGAME_TEXT_SLOT)
				//B* 2061672: Don't overwrite minigame text block if the mission stat watcher is running
				AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) <=0
					REQUEST_ADDITIONAL_TEXT("C_RACE", MINIGAME_TEXT_SLOT )
				ENDIF
				// If both the vehicle and the ped exist and we are within range, allow checks to progress to the trigger checks.
				RETURN TRUE 
			ELSE
				CREATE_TRIGGER_CAR_AND_DRIVER()
			ENDIF
		ENDIF
		
	ELSE
		
		IF VDIST2( GET_ENTITY_COORDS( PLAYER_PED_ID() ), GET_RACE_TRIGGER_LOCATION() ) > STREAM_OUT_DIST
			REMOVE_PED( p_TriggerDriver, TRUE )
			REMOVE_VEHICLE( ve_TriggerCar, TRUE )
			
			IF g_savedGlobals.sCountryRaceData.bDisrupted
				g_savedGlobals.sCountryRaceData.bDisrupted = FALSE //Reset the disrupted flag if the player leaves the area.
			ENDIF
			IF g_savedGlobals.sCountryRaceData.bRaceJustCompleted
				g_savedGlobals.sCountryRaceData.bRaceJustCompleted = FALSE
			ENDIF
		ENDIF
		
		IF g_savedGlobals.sCountryRaceData.bDisrupted		
			REMOVE_PED( p_TriggerDriver )
			REMOVE_VEHICLE( ve_TriggerCar )
			
			IF IS_ENTITY_AT_COORD( PLAYER_PED_ID(), GET_RACE_TRIGGER_LOCATION(), <<5,5,5>>, TRUE )
				
				IF HAS_THIS_ADDITIONAL_TEXT_LOADED("C_RACE", MINIGAME_TEXT_SLOT)
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRACEDISRUPT")
					AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appinternet")) = 0
						CLEAR_HELP()
						PRINT_HELP_CUSTOM("CRACEDISRUPT")
					ENDIF
				ENDIF
					
			ELIF HAS_THIS_ADDITIONAL_TEXT_LOADED("C_RACE", MINIGAME_TEXT_SLOT) AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRACEDISRUPT")
				CLEAR_HELP()
			ENDIF
		ELIF HAS_THIS_ADDITIONAL_TEXT_LOADED("C_RACE", MINIGAME_TEXT_SLOT) AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRACEDISRUPT")
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Checks the global skip value handled by the debug menu, if this changes from -1 adjust the race flow accordingly
/// PARAMS:
///    iSkipTo - The race index we want to skip to.
PROC CHECK_DEBUG_WARP(INT &iSkipTo)

	IF iSkipTo <> -1	
		IF iSkipTo >= 0 AND iSkipTo <= 4
			g_savedGlobals.sCountryRaceData.iCurrentRace = iSkipto
			SAFE_FADE_SCREEN_OUT_TO_BLACK()
			REMOVE_PED( p_TriggerDriver, TRUE )
			REMOVE_VEHICLE( ve_TriggerCar, TRUE )
			CLEAR_AREA(GET_RACE_TRIGGER_LOCATION(), 100, TRUE)
			IF NOT IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
				SAFE_TELEPORT_ENTITY( PLAYER_PED_ID(), GET_RACE_TRIGGER_LOCATION(), 0, TRUE )
			ELSE
				SAFE_TELEPORT_ENTITY( GET_PLAYERS_LAST_VEHICLE(), GET_RACE_TRIGGER_LOCATION(), 0, TRUE )
			ENDIF
			g_savedGlobals.sCountryRaceData.bDisrupted = FALSE
			g_savedGlobals.sCountryRaceData.bRaceJustCompleted = FALSE
			CREATE_TRIGGER_CAR_AND_DRIVER(TRUE)
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID()) WAIT_FOR_WORLD_TO_LOAD( GET_ENTITY_COORDS( PLAYER_PED_ID() ) ) ENDIF
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_COUNTRY_RACE, TRUE)
			CPRINTLN(DEBUG_MISSION, "Country Race Controller - Warping to Race ", iSkipto)
			iSkipTo = -1
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Prints the appropriate prize help text.
PROC DO_CONGRATULATION_HELP()
	CLEAR_HELP(TRUE)
	TEXT_LABEL sCongratulationText = "CRACEWIN"
	sCongratulationText += (g_savedGlobals.sCountryRaceData.iCurrentRace + 1)
	CPRINTLN(DEBUG_MISSION, sCongratulationText)
	PRINT_HELP(sCongratulationText)
ENDPROC

/// PURPOSE:
///    Handles the hint camera.
PROC DO_HINT_CAM()

	IF  GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_CINEMATIC
	AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
	AND NOT IS_GAMEPLAY_CAM_LOOKING_BEHIND()
	AND IS_PLAYER_PLAYING(PLAYER_ID())
	AND IS_PED_UNINJURED(p_TriggerDriver)
	AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 20
		IF b_HintCamReady
			IF NOT IS_GAMEPLAY_HINT_ACTIVE()
				SET_GAMEPLAY_ENTITY_HINT(p_TriggerDriver, <<0,0,0>>)//, TRUE, -1, DEFAULT_INTERP_TO_FROM_GAME)
				b_HintCamReady = FALSE
			ENDIF
		ENDIF
	ELSE
		IF IS_GAMEPLAY_HINT_ACTIVE()
			STOP_GAMEPLAY_HINT()
		ENDIF
		b_HintCamReady = TRUE
	ENDIF

ENDPROC

/// PURPOSE:
///    Handles the ped speech.
PROC DO_PED_SPEECH()
	IF GET_GAME_TIMER() > iRaceSpeechTimer + 5000
		PLAY_PED_AMBIENT_SPEECH(p_TriggerDriver, "PRERACE_CHAT", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
		iRaceSpeechTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC


/// PURPOSE:
///    Do the entry help.
PROC DO_ENTRY_HELP_TEXT()
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("C_RACE", MINIGAME_TEXT_SLOT)
		TEXT_LABEL text = GET_RACE_HELP_TEXT()
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(text)
		AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appinternet")) = 0
			CLEAR_HELP()
			PRINT_HELP_CUSTOM(text)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Do the unsuitable car help.
PROC DO_GET_CAR_HELP_TEXT()
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("C_RACE", MINIGAME_TEXT_SLOT)
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRACECAR")
		AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appinternet")) = 0
			CLEAR_HELP()
			PRINT_HELP_CUSTOM("CRACECAR")
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Clear any triggered trigger help.
PROC CLEAR_TRIGGER_HELP()
	TEXT_LABEL text = GET_RACE_HELP_TEXT()
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("C_RACE", MINIGAME_TEXT_SLOT)
	AND	( IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(text) OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRACECAR") )
		CLEAR_HELP()
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops active hints.
PROC STOP_TRIGGER_HINT()
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	b_HintCamReady = TRUE
ENDPROC

/// PURPOSE:
///    Checks if the player is the only ped in a vehicle.
/// RETURNS:
///    TRUE if the player is not with a prostitute and if the vehicle has no passengers.
FUNC BOOL IS_PLAYER_ONLY_ONE_IN_VEHICLE()
	
	IF IS_PLAYER_WITH_A_PROSTITUTE()
		RETURN FALSE	
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("chop")) <> 0
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX TempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF GET_VEHICLE_NUMBER_OF_PASSENGERS(TempVeh) = 0
			RETURN TRUE
		ENDIF	
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Streams the trigger assets in and out, performs suitable checks and handles the players entry to the race.
PROC HANDLE_RACE_ENTRY()
	IF STREAM_TRIGGER_ASSETS()
		IF IS_ENTITY_AT_COORD( PLAYER_PED_ID(), GET_RACE_TRIGGER_LOCATION(), <<5,5,5>>, TRUE )
			
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_HEADLIGHT )

			SET_IK_TARGET(p_TriggerDriver, IK_PART_HEAD, PLAYER_PED_ID(), 0, <<0,0,0>>, ITF_DEFAULT, 150, 400)
			
			IF IS_PLAYER_ONLY_ONE_IN_VEHICLE()			
				IF IS_PLAYER_IN_SUITABLE_RACE_CAR()
					
					DO_PED_SPEECH()	
					DO_HINT_CAM()
					DO_ENTRY_HELP_TEXT()
					
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					AND NOT IS_SELECTOR_ONSCREEN()
					AND NOT IS_PHONE_ONSCREEN()
					AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appinternet")) = 0
						IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CONTEXT )
							CPRINTLN(DEBUG_MISSION, "Controller - Launching race with type: ", GET_RACE_NAME_FROM_INT(g_savedGlobals.sCountryRaceData.iCurrentRace) )
							PLAY_SOUND_FRONTEND(-1,"YES","HUD_FRONTEND_DEFAULT_SOUNDSET")
							CLEAR_HELP()
							CLEAR_AREA_OF_PROJECTILES(GET_RACE_TRIGGER_LOCATION(), 20)
							SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
							SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
							HIDE_HELP_TEXT_THIS_FRAME()
							e_ControllerState = STATE_INITIALISE_RACE
						ENDIF
					ENDIF
				ELSE
					DO_GET_CAR_HELP_TEXT()
					STOP_TRIGGER_HINT()
				ENDIF
			ELSE
				CLEAR_TRIGGER_HELP()
				STOP_TRIGGER_HINT()
			ENDIF
		ELSE
			CLEAR_TRIGGER_HELP()
			STOP_TRIGGER_HINT()
		ENDIF
	ELSE
		CLEAR_TRIGGER_HELP()
		STOP_TRIGGER_HINT()
	ENDIF
ENDPROC

/// PURPOSE:
///    Requests and launches the race script, also checks with the candidate controller if it can launch.
PROC REQUEST_AND_LAUNCH_RACE()

	HIDE_HELP_TEXT_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	REQUEST_SCRIPT("Country_Race")
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_HEADLIGHT )
	CLEAR_AREA_OF_PROJECTILES(GET_RACE_TRIGGER_LOCATION(), 20)
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Country_Race")) = 0

		IF HAS_SCRIPT_LOADED("Country_Race")
			IF eCandidateReturn = MCRET_PROCESSING
			    eCandidateReturn = Request_Mission_Launch(g_iRaceMissionCandidateID, MCTID_SELECTED_BY_PLAYER, MISSION_TYPE_MINIGAME)
				CPRINTLN(DEBUG_MISSION, "Country Race Controller - MCRET_PROCESSING.")	
			ELIF eCandidateReturn = MCRET_ACCEPTED
				REMOVE_PED( p_TriggerDriver )
				REMOVE_VEHICLE( ve_TriggerCar )
				START_NEW_SCRIPT("Country_Race", MULTIPLAYER_MISSION_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED("Country_Race")
				e_ControllerState = STATE_RACE_ACTIVE
				CPRINTLN(DEBUG_MISSION, "Country Race Controller - MCRET_ACCEPTED - Starting script.")
			ELIF eCandidateReturn = MCRET_DENIED
				CONTROLLER_CLEANUP()
				CPRINTLN(DEBUG_MISSION, "Country Race Controller - MCRET_DENIED - Cleaning up.")	
			ENDIF
		ENDIF
		
	ENDIF
					
ENDPROC

/// PURPOSE:
///    Monitors if the race has finished.
PROC CHECK_RACE_FINISHED()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Country_Race")) = 0
		CPRINTLN(DEBUG_MISSION, "Country Race Controller - ", GET_RACE_NAME_FROM_INT(g_savedGlobals.sCountryRaceData.iCurrentRace), PICK_STRING(g_bCountryRacePassed, " PASSED", " FAILED" ) )
		Mission_Over(g_iRaceMissionCandidateID)
		g_iRaceMissionCandidateID = NO_CANDIDATE_ID
		eCandidateReturn = MCRET_PROCESSING
		e_ControllerState = STATE_RESETTING
	ENDIF
ENDPROC

/// PURPOSE:
///    Advances race if appropriate, and resets the race trigger.
PROC RESET_RACE()
	IF IS_COUNTRY_RACE_PASSED()
		DO_CONGRATULATION_HELP()
		ADVANCE_ACTIVE_RACE( g_savedGlobals.sCountryRaceData.iCurrentRace )
	ENDIF
	g_savedGlobals.sCountryRaceData.bRaceJustCompleted = TRUE	// Flag trigger as race has just happened so it doesn't activate until player leaves the area.
	SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_COUNTRY_RACE, FALSE)
	SET_COUNTRY_RACE_FAILED() //reset the passed global bool
	MAKE_AUTOSAVE_REQUEST()
	e_ControllerState = STATE_IDLE
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Handles controller debug procedures.
PROC DEBUG_UPDATE()

	IF e_ControllerState <> STATE_RACE_ACTIVE
		CHECK_DEBUG_WARP(g_iDebugRaceWarp)
	ENDIF	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Country_Race")) = 0
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			 CONTROLLER_CLEANUP(TRUE)
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
			CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20)
		ENDIF
		
	ENDIF
	
ENDPROC
#ENDIF

/// PURPOSE:
///    Checks whether the controller needs to clean up.
PROC DO_CLEANUP_CHECKS()
	IF NOT GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_COUNTRY_RACE)) 
		CPRINTLN(DEBUG_MISSION, "Controller launched before it is unlocked, cleaning up Country Race Controller.")
		g_savedGlobals.sCountryRaceData.bRaceJustCompleted = TRUE //Setting the just completed flag so that the player must leave the area before the trigger reactivates.
	    CONTROLLER_CLEANUP()
	ENDIF
	
	// If a script or mission is running that isn't this one. Cleanup the script.
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
	AND e_ControllerState = STATE_IDLE
		CPRINTLN(DEBUG_MISSION, "Player is on mission, cleaning up Country Race Controller.")
		g_savedGlobals.sCountryRaceData.bRaceJustCompleted = TRUE //Setting the just completed flag so that the player must leave the area before the trigger reactivates.
	    CONTROLLER_CLEANUP()
	ENDIF
	
	// Chinese 2 mission trigger hammers streaming. We can't have the race trigger active near
	// it at the same time. (2037298)
	IF IS_MISSION_AVAILABLE(SP_MISSION_CHINESE_2)
		CPRINTLN(DEBUG_MISSION, "Chinese 2 trigger active nearby, cleaning up Country Race Controller.")
		g_savedGlobals.sCountryRaceData.bRaceJustCompleted = TRUE //Setting the just completed flag so that the player must leave the area before the trigger reactivates.
	    CONTROLLER_CLEANUP()
	ENDIF
	
	IF NOT IS_LAST_GEN_PLAYER()
		CPRINTLN(DEBUG_MISSION, "Player is no longer last gen player, cleaning up Country Race Controller.")
		g_savedGlobals.sCountryRaceData.bRaceJustCompleted = TRUE //Setting the just completed flag so that the player must leave the area before the trigger reactivates.
	    CONTROLLER_CLEANUP()
	ENDIF
	
	// url:bugstar:2110626 - Only do this when race isn't active.
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Country_Race")) = 0 
		IF VDIST2(GET_RACE_TRIGGER_LOCATION(), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 160000 //Over 400m away
			CPRINTLN(DEBUG_MISSION, "Player is 400m away, cleaning up Country Race Controller.")
			CONTROLLER_CLEANUP()
		ENDIF
	ENDIF
	
ENDPROC

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED( FORCE_CLEANUP_FLAG_DEBUG_MENU | FORCE_CLEANUP_FLAG_SP_TO_MP )
		CPRINTLN(DEBUG_MISSION, "Country Race Controller - Force cleanup has occured.")
		CONTROLLER_CLEANUP(TRUE)
	ENDIF

	CPRINTLN(DEBUG_MISSION, "Country Race Controller - script initiating.")

	eCandidateReturn = MCRET_PROCESSING
	
	//Within 200m when trigger activates and screen is faded in and player is in control
	IF IS_ENTITY_ALIVE( PLAYER_PED_ID() )
		IF VDIST2(GET_RACE_TRIGGER_LOCATION(), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 40000
			CPRINTLN(DEBUG_MISSION, "Player is within 200m.")
			IF IS_SCREEN_FADED_IN()
				CPRINTLN(DEBUG_MISSION, "Screen is faded in.")
				IF IS_PLAYER_CONTROL_ON( PLAYER_ID() )
					CPRINTLN(DEBUG_MISSION, "Player control is on.")
					CPRINTLN(DEBUG_MISSION, "Setting trigger so that player must leave area.")
					g_savedGlobals.sCountryRaceData.bRaceJustCompleted = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)
	
	WHILE (TRUE)
		
		IF NOT IS_ENTITY_ALIVE( PLAYER_PED_ID() )
			EXIT
		ENDIF
		
		DO_CLEANUP_CHECKS()
		
		SWITCH e_ControllerState
		
			CASE STATE_IDLE				HANDLE_RACE_ENTRY()	 		BREAK		
			CASE STATE_INITIALISE_RACE 	REQUEST_AND_LAUNCH_RACE() 	BREAK		
			CASE STATE_RACE_ACTIVE		CHECK_RACE_FINISHED() 		BREAK	
			CASE STATE_RESETTING 		RESET_RACE() 				BREAK
		
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD DEBUG_UPDATE() #ENDIF
		
		WAIT(0)
		
	ENDWHILE
	
ENDSCRIPT
