
USING "country_race_header.sch"
USING "minigame_big_message.sch"

//═══════════════════════════════════════╡ Constants ╞══════════════════════════════════════\\

//---------------------------------STAGE-RELATED-CONSTANTS----------------------------------\\

// Stage warps
VECTOR VECTOR_STAGE_SETUP_WARP 						= <<1967.0420, 3116.0051, 45.8901>>
VECTOR VECTOR_STAGE_RACE_WARP						= <<1967.0420, 3116.0051, 45.8901>>
VECTOR VECTOR_STAGE_COMPLETE_WARP					= <<1967.0420, 3116.0051, 45.8901>>

// Stage warp headings
CONST_FLOAT FLOAT_STAGE_SETUP_HEADING				103.4
CONST_FLOAT FLOAT_STAGE_RACE_HEADING				103.4
CONST_FLOAT FLOAT_STAGE_COMPLETE_HEADING			103.4

// Replay stages
CONST_INT INT_REPLAY_STAGE_SETUP 					0
CONST_INT INT_REPLAY_STAGE_RACE 					1
CONST_INT INT_REPLAY_STAGE_COMPLETE					2

// Total number of stages
CONST_INT INT_NUMBER_OF_STAGES						3

// Stage names
STRING STAGE_SETUP_NAME 							= "Stage Setup"
STRING STAGE_RACE_NAME								= "Race"
STRING STAGE_COMPLETE_NAME							= "Complete"

// Mission text
STRING MISSION_TEXT 								= ""

//-------------------------------------OTHER-CONSTANTS--------------------------------------\\

//VECTOR VECTOR_ZERO 									= <<0,0,0>>
CONST_FLOAT	PER_FRAME_FORCE_MULTIPLIER 				30.0
CONST_INT MAX_LOS_CHECK_ENTITIES 					10

//═══════════════════════════════════════╡ Variables ╞══════════════════════════════════════\\

//-------------------------------------------Bools------------------------------------------\\
#IF IS_DEBUG_BUILD
	BOOL bShowDebugCheckpointProgress
#ENDIF
BOOL b_PlayerBoosting = FALSE
BOOL b_ShouldRetry
//------------------------------------------Floats------------------------------------------\\
//-----------------------------------------Integers-----------------------------------------\\

INT i_TriggeredTextHashes[30]
INT i_MissionBitFlags = 0
INT i_StageProgress = 0
INT i_LOSCheckIndex
INT i_BoostTimer = 0
INT i_QuitTimer
INT i_TimeLapseProgress = 0
INT i_DNFTImer = 0
//------------------------------------------Strings-----------------------------------------\\
STRING st_FailString
//------------------------------------------Vectors-----------------------------------------\\
VECTOR v_DriveToStartlineOffset = <<8.669, -5.9084, 0.0428>>	
VECTOR v_PosInitial, v_RotInitial
VECTOR v_PosDestination, v_RotDestination
//-------------------------------------------Misc-------------------------------------------\\

#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct	stageMenu[INT_NUMBER_OF_STAGES]
	WIDGET_GROUP_ID widgetDebug
#ENDIF

PED_STRUCT 					s_MissionPed		[COUNT_OF(MISSION_PED_FLAGS)]
PED_STRUCT					s_AmbientPed		[COUNT_OF(AMBIENT_PED_FLAGS)]
VEHICLE_STRUCT				s_MissionVehicle	[COUNT_OF(MISSION_VEHICLE_FLAGS)]
OBJECT_STRUCT				s_MissionObject		[COUNT_OF(MISSION_OBJECT_FLAGS)]
LOCATES_HEADER_DATA 		s_LocatesData
SCRIPT_ENTITY_LOS_STRUCT 	s_LOSChecks[MAX_LOS_CHECK_ENTITIES]
COUNTRY_RACER_DATA			s_PlayerRacerData
STRUCTTIMELAPSE				s_Timelapse
MISSION_STAGE 	e_CurrentMissionStage
REL_GROUP_HASH 	e_RelEnemy
REL_GROUP_HASH	e_RelBuddy

BIG_TEXT s_LapText
//BIG_TEXT s_FailText
MG_FAIL_SPLASH s_FailSplash
SCRIPT_SCALEFORM_BIG_MESSAGE s_BigMessage
//════════════════════════════════════════╡ Indeces ╞═══════════════════════════════════════\\

//-------------------------------------------Blips------------------------------------------\\
//------------------------------------------Cameras-----------------------------------------\\
CAMERA_INDEX cam_Initial
CAMERA_INDEX cam_Destination
//----------------------------------------Coverpoints---------------------------------------\\
//-----------------------------------------Entities-----------------------------------------\\
//------------------------------------------Objects-----------------------------------------\\
//--------------------------------------Particle-Effects------------------------------------\\
//-------------------------------------------Peds-------------------------------------------\\
PED_INDEX PLAYER = PLAYER_PED_ID()
//------------------------------------------Pickups-----------------------------------------\\
//-------------------------------------------Rope-------------------------------------------\\
//--------------------------------------Scaleform-Index-------------------------------------\\
//-----------------------------------------Sequences----------------------------------------\\
//----------------------------------------Shape-Tests---------------------------------------\\
//-------------------------------------Streaming-Volumes------------------------------------\\
//-----------------------------------------Vehicles-----------------------------------------\\
VEHICLE_INDEX ve_CheckpointVehicle
VEHICLE_INDEX ve_PlayersCar
VEHICLE_INDEX ve_RewardCar

/// PURPOSE:
///    Progresses the stage and resets any variables needed.
/// PARAMS:
///    eNextStage - The stage we need to advance to.
PROC PROGRESS_STAGE(MISSION_STAGE eNextStage)

	i_StageProgress = 0
	e_CurrentMissionStage = eNextStage
	
ENDPROC

/// PURPOSE:
///    Removes all peds.
/// PARAMS:
///    bForceClean - Do we want to force clean?
PROC REMOVE_ALL_PEDS(BOOL bForceClean = FALSE)
	
	INT i
	
	REPEAT COUNT_OF(s_MissionPed) i
		REMOVE_PED( s_MissionPed[i].PedIndex, bForceClean )
	ENDREPEAT
	REPEAT COUNT_OF(s_AmbientPed) i
		REMOVE_PED( s_AmbientPed[i].PedIndex, TRUE )
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Removes all vehicles.
/// PARAMS:
///    bForceClean - Do we want to force clean?
PROC REMOVE_ALL_VEHICLES(BOOL bForceClean = FALSE, BOOL bRemovePlayersCar = TRUE)

	IF bRemovePlayersCar AND bForceClean
		IF IS_ENTITY_ALIVE(PLAYER) AND IS_PED_IN_ANY_VEHICLE(PLAYER)
			SET_PLAYER_OUT_OF_ANY_VEHICLE()
		ENDIF
		REMOVE_VEHICLE( ve_PlayersCar, bForceClean )
	ENDIF
	
	INT i
	
	REPEAT COUNT_OF(s_MissionPed) i
		REMOVE_VEHICLE( s_MissionPed[i].sVehicle.VehicleIndex, bForceClean )
	ENDREPEAT
	
	REPEAT COUNT_OF(s_MissionVehicle) i
		REMOVE_VEHICLE( s_MissionVehicle[i].VehicleIndex, bForceClean )
	ENDREPEAT
	
	REMOVE_VEHICLE( ve_CheckpointVehicle, bForceClean )
	REMOVE_VEHICLE( ve_RewardCar, bForceClean )

	
ENDPROC

/// PURPOSE:
///    Removes all objects.
/// PARAMS:
///    bForceClean - Do we want to force clean?
PROC REMOVE_ALL_OBJECTS(BOOL bForceClean = FALSE)
	INT i
	
	REPEAT COUNT_OF(s_MissionObject) i
		REMOVE_OBJECT( s_MissionObject[i].ObjectIndex, bForceClean )
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Removes all blips.
PROC REMOVE_ALL_BLIPS()
	
	INT i
	
	REPEAT COUNT_OF(s_MissionPed) i
		SAFE_REMOVE_BLIP( s_MissionPed[i].BlipIndex )
	ENDREPEAT
	
	REPEAT COUNT_OF(s_MissionVehicle) i
		SAFE_REMOVE_BLIP( s_MissionVehicle[i].BlipIndex )
	ENDREPEAT
	
	REPEAT COUNT_OF(s_MissionObject) i
		SAFE_REMOVE_BLIP( s_MissionObject[i].BlipIndex )
	ENDREPEAT
	
	SAFE_REMOVE_BLIP(s_PlayerRacerData.blCurrentCheckpoint )
	SAFE_REMOVE_BLIP(s_PlayerRacerData.blNextCheckpoint )
ENDPROC

/// PURPOSE:
///    Removes all relationship groups.
PROC REMOVE_ALL_RELATIONSHIP_GROUPS()

	REMOVE_RELATIONSHIP_GROUP(e_RelEnemy)
	REMOVE_RELATIONSHIP_GROUP(e_RelBuddy)
	REMOVE_RELATIONSHIP_GROUP(e_RelRacer)
	
ENDPROC

/// PURPOSE:
///    Removes all cover points.
PROC REMOVE_ALL_COVER_POINTS()

ENDPROC

/// PURPOSE:
///    Removes all door hashes.
PROC REMOVE_ALL_DOORS()

ENDPROC

/// PURPOSE:
///    Cleans up the mission and all mission assets.
/// PARAMS:
///    bTerminate - Whether we want to terminate the script.
///    bForceCleanup - Whether to force the cleanup (delete assets).
PROC MISSION_CLEANUP( BOOL bTerminate = TRUE, BOOL bForceCleanup = FALSE, BOOL bRemovePlayersCar = TRUE, BOOL bDontRemoveIPL = FALSE)
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	REMOVE_ALL_PEDS(bForceCleanup)
	REMOVE_ALL_VEHICLES(bForceCleanup, bRemovePlayersCar)
	REMOVE_ALL_OBJECTS(bForceCleanup)
	REMOVE_ALL_BLIPS()
	REMOVE_ALL_COVER_POINTS()
	REMOVE_ALL_DOORS()
	
	IF s_PlayerRacerData.cpCurrentCheckpoint != NULL
	DELETE_CHECKPOINT(s_PlayerRacerData.cpCurrentCheckpoint)
	ENDIF
	IF s_PlayerRacerData.cpPreviousCheckpoint != NULL
	DELETE_CHECKPOINT(s_PlayerRacerData.cpPreviousCheckpoint)
	ENDIF

	CLEAR_TRIGGERED_LABELS(i_TriggeredTextHashes)
	
	CLEAR_MISSION_LOCATE_STUFF(s_LocatesData)
	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
	KILL_ANY_CONVERSATION()
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	SET_TIME_SCALE(1)
	SET_CUTSCENE_RUNNING(FALSE)
	CLEAR_TIMECYCLE_MODIFIER()
	SET_ENTITY_VISIBLE(PLAYER, TRUE)
	
	RELEASE_MINIGAME_COUNTDOWN_UI(s_RaceData.sCountDown)
	
	CLEANUP_RACE_CHECKPOINTS(s_PlayerRacerData)
	
	IF bForceCleanup
		CLEAR_AREA(GET_ENTITY_COORDS(PLAYER), 1000, TRUE)
	ENDIF
	
	SET_FRONTEND_RADIO_ACTIVE(TRUE)
	
	IF IS_ENTITY_ALIVE(ve_PlayersCar)
		SET_VEHICLE_HANDBRAKE(ve_PlayersCar, FALSE)
	ENDIF
	
	ANIMPOSTFX_STOP_ALL()	
	
	STOP_AUDIO_SCENES()
	STOP_STREAM()
	
	IF bTerminate
		
		// Turn off the slipstream effect
		SET_ENABLE_VEHICLE_SLIPSTREAMING(FALSE)
		
		REMOVE_ALL_RELATIONSHIP_GROUPS()
		SET_MAX_WANTED_LEVEL(5)	
			
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE()
		
		// Disable all peds...
		SET_PED_POPULATION_BUDGET(2)	
		SET_REDUCE_PED_MODEL_BUDGET(FALSE)
		SET_SCENARIO_TYPE_ENABLED( "WORLD_VEHICLE_BIKER", TRUE )
		SET_SCENARIO_TYPE_ENABLED( "DRIVE", TRUE )
		SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", TRUE)
		SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_PASSENGERS", TRUE)
		// Traffic and cops
		SET_VEHICLE_POPULATION_BUDGET(2)
		SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
		SET_CREATE_RANDOM_COPS(TRUE)
		
		SET_WANTED_LEVEL_MULTIPLIER(1)
		SET_ROADS_IN_ANGLED_AREA( <<2565.207275,2896.661621,29.940796>>, <<1654.530151,3420.822021,63.940063>>, 470.000000, FALSE, TRUE)
	
		// Turn on the slipstream effect
		SET_ENABLE_VEHICLE_SLIPSTREAMING(FALSE)
		
		IF IS_ENTITY_ALIVE( GET_PLAYERS_LAST_VEHICLE() )
			SET_VEHICLE_DOORS_LOCKED( GET_PLAYERS_LAST_VEHICLE(), VEHICLELOCK_UNLOCKED )
			SET_VEHICLE_HANDBRAKE( GET_PLAYERS_LAST_VEHICLE(), FALSE )
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF DOES_WIDGET_GROUP_EXIST(widgetDebug)
				DELETE_WIDGET_GROUP(widgetDebug)
			ENDIF
		#ENDIF
		
		PAUSE_CLOCK(FALSE)

		// Reseting this here, in case MG_UPDATE_FAIL_SPLASH_SCREEN failed to cleanup. (Very important this gets reset)
		SET_NO_LOADING_SCREEN(FALSE)

		IF NOT bDontRemoveIPL
			SET_BUILDING_STATE(BUILDINGNAME_IPL_COUNTRY_RACETRACK, BUILDINGSTATE_NORMAL, FALSE)
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
		ELSE
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		SAFE_FADE_SCREEN_IN_FROM_BLACK_SUPPRESS_TRAFFIC()
		
		MISSION_OVER(g_iRaceMissionCandidateID)

		CPRINTLN(DEBUG_MISSION, "Cleaning up races.")
		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE)
		
		SET_CINEMATIC_BUTTON_ACTIVE( TRUE )
		
		
		
		
		TERMINATE_THIS_THREAD()
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Initialises the big text structs.
PROC INITIALISE_BIG_TEXT

	s_LapText.eBigTextState = BTS_READY
	s_LapText.iBigTextTimer = 0
//
//	s_FailText.eBigTextState = BTS_READY
//	s_FailText.iBigTextTimer = 0
	
ENDPROC

/// PURPOSE:
///    Sets up the missions relationship groups.
///    Sets a like - hate relationship between enemies and buddies/player.
PROC SETUP_RELATIONSHIP_GROUPS()
	
	ADD_RELATIONSHIP_GROUP("ENEMIES", e_RelEnemy)
	ADD_RELATIONSHIP_GROUP("BUDDIES", e_RelBuddy)
	ADD_RELATIONSHIP_GROUP("RACERS", e_RelRacer)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, e_RelEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, e_RelBuddy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, e_RelRacer)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, e_RelBuddy, e_RelEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, e_RelBuddy, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, e_RelBuddy, e_RelRacer)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, e_RelEnemy, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, e_RelEnemy, e_RelBuddy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, e_RelEnemy, e_RelRacer)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, e_RelRacer, e_RelEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, e_RelRacer, e_RelBuddy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, e_RelRacer, RELGROUPHASH_PLAYER)
	
ENDPROC

/// PURPOSE:
///    Initialises mission variables.
PROC INITIALISE_MISSION_VARIABLES()
	
	i_MissionBitFlags = 0
	i_StageProgress = 0
	i_DNFTImer = 0
	i_BoostTimer = 0
	b_PlayerBoosting = FALSE
	i_TimeLapseProgress = 0
	
	INITIALISE_COUNTRY_TRACK_DATA( s_TrackData )
	INITIALISE_MISSION_VEHICLES( s_MissionVehicle )
	INITIALISE_MISSION_PEDS( s_MissionPed, s_MissionVehicle )
	INITIALISE_AMBIENT_PEDS( s_AmbientPed )
	INITIALISE_MISSION_OBJECTS( s_MissionObject )
	INITIALISE_RACER_DATA( s_PlayerRacerData )
	INITIALISE_RACE_DATA( s_RaceData )
	INITIALISE_BIG_TEXT()
	
ENDPROC

/// PURPOSE:
///    Sets up the mission.
PROC MISSION_SETUP()

	IF IS_REPLAY_IN_PROGRESS()				
		SAFE_FADE_SCREEN_OUT_TO_BLACK_SUPPRESS_TRAFFIC()
	ENDIF
	
	REGISTER_SCRIPT_WITH_AUDIO(TRUE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	SET_TIME_SCALE(1)
	CLEAR_PRINTS()
	CLEAR_HELP()
	
	SETUP_RELATIONSHIP_GROUPS()
	
	CLEAR_MISSION_LOCATE_STUFF(s_LocatesData)
	
	e_CurrentMissionStage = STAGE_SETUP
	
	PLAYER = PLAYER_PED_ID()
	
	REQUEST_TEXT(MISSION_TEXT, DLC_TEXT_SLOT0, FALSE)				
	
	ADD_SCENARIO_BLOCKING_AREA_FROM_POSITION_AND_RADIUS( s_TrackData.vCheckpoint[0], 1000 )
	ADD_SCENARIO_BLOCKING_AREA( <<2270.0408, 3006.9998, 44.1942>>, <<2276.2146, 3010.5435, 48.0713>> )
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE( TRUE )

	// Remove vehicle gens
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-7000.0, -7000.0, -100.0>>, <<7000.0, 7000.0, 100.0>>, FALSE)
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<-7000.0, -7000.0, -100.0>>, <<7000.0, 7000.0, 100.0>>)
	DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
	SET_ROADS_IN_ANGLED_AREA( <<2565.207275,2896.661621,29.940796>>, <<1654.530151,3420.822021,63.940063>>, 470.000000, FALSE, FALSE)
	SET_SCENARIO_TYPE_ENABLED( "WORLD_VEHICLE_BIKER", FALSE )
	SET_SCENARIO_TYPE_ENABLED( "DRIVE", FALSE )
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_PASSENGERS", FALSE)
	// Disable all peds...
	SET_PED_POPULATION_BUDGET(0)	
	SET_REDUCE_PED_MODEL_BUDGET(TRUE)
	

	// Traffic and cops
	SET_VEHICLE_POPULATION_BUDGET(0)
	SET_REDUCE_VEHICLE_MODEL_BUDGET(TRUE)
	SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
	SET_CREATE_RANDOM_COPS(FALSE)
	
	SET_WANTED_LEVEL_MULTIPLIER(0)
	
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	
	// Turn on the slipstream effect
	SET_ENABLE_VEHICLE_SLIPSTREAMING(TRUE)
	
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	
	CPRINTLN(DEBUG_MISSION, "Mission Setup Complete.")
	
ENDPROC


/// PURPOSE:
///    Loads the setup stage assets.
PROC LOAD_STAGE_SETUP()
	
	IF NOT IS_ENTITY_ALIVE(ve_PlayersCar)
		IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
			ve_PlayersCar = GET_PLAYERS_LAST_VEHICLE()
			SET_ENTITY_AS_MISSION_ENTITY(ve_PlayersCar)
		ELSE
			REQUEST_MODEL(GAUNTLET)
			WHILE NOT HAS_MODEL_LOADED(GAUNTLET)
				wait(0)
			ENDWHILE
			ve_PlayersCar = CREATE_VEHICLE(GAUNTLET, s_TrackData.vStartingGrid[MAX_RACERS - 1] + v_DriveToStartlineOffset, s_TrackData.fStartingGrid[MAX_RACERS - 1])
			SET_MODEL_AS_NO_LONGER_NEEDED(GAUNTLET)
			SET_PED_INTO_VEHICLE( PLAYER_PED_ID(), ve_PlayersCar )
		ENDIF
	ELSE
		SAFE_TELEPORT_ENTITY(ve_PlayersCar, s_TrackData.vStartingGrid[MAX_RACERS - 1] + v_DriveToStartlineOffset, s_TrackData.fStartingGrid[MAX_RACERS - 1])
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), ve_PlayersCar)
	ENDIF
	
	SAFE_TELEPORT_ENTITY(ve_PlayersCar, s_TrackData.vStartingGrid[MAX_RACERS - 1] + v_DriveToStartlineOffset, s_TrackData.fStartingGrid[MAX_RACERS - 1])
	
	e_CurrentMissionStage = STAGE_SETUP
	
ENDPROC

/// PURPOSE:
///    Loads the race stage.
PROC LOAD_STAGE_RACE()
	
	REQUEST_MINIGAME_COUNTDOWN_UI( s_RaceData.sCountDown )
	WHILE NOT HAS_MINIGAME_COUNTDOWN_UI_LOADED( s_RaceData.sCountDown )
		wait(0)
	ENDWHILE
		
	SET_BUILDING_STATE(BUILDINGNAME_IPL_COUNTRY_RACETRACK, BUILDINGSTATE_DESTROYED, FALSE)
	
	INT i
	FOR i = ENUM_TO_INT(MPF_RACER1 ) TO ENUM_TO_INT( MPF_RACER8 )
		CREATE_MISSION_PED( s_MissionPed[i], TRUE )
	ENDFOR
	
	IF NOT IS_ENTITY_ALIVE(ve_PlayersCar)
		IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
			ve_PlayersCar = GET_PLAYERS_LAST_VEHICLE()
			SET_ENTITY_AS_MISSION_ENTITY(ve_PlayersCar)
		ELSE
			REQUEST_MODEL(GAUNTLET)
			WHILE NOT HAS_MODEL_LOADED(GAUNTLET)
				wait(0)
			ENDWHILE
			ve_PlayersCar = CREATE_VEHICLE(GAUNTLET, s_TrackData.vStartingGrid[MAX_RACERS - 1], s_TrackData.fStartingGrid[MAX_RACERS - 1])
			SET_PED_INTO_VEHICLE( PLAYER_PED_ID(), ve_PlayersCar )
		ENDIF
	ELSE
		SAFE_TELEPORT_ENTITY(ve_PlayersCar, s_TrackData.vStartingGrid[MAX_RACERS - 1], s_TrackData.fStartingGrid[MAX_RACERS - 1])
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), ve_PlayersCar)
	ENDIF
	
	SET_VEHICLE_FIXED(ve_PlayersCar)
	
	SET_VEHICLE_DOORS_LOCKED(ve_PlayersCar, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
	SET_VEHICLE_HANDBRAKE(ve_PlayersCar, TRUE)
	
	REQUEST_WAYPOINT_RECORDING( s_TrackData.stTrackWaypoint )
	WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED( s_TrackData.stTrackWaypoint )
		wait(0)
	ENDWHILE
	
	SAFE_TELEPORT_ENTITY(ve_PlayersCar, s_TrackData.vStartingGrid[MAX_RACERS - 1], s_TrackData.fStartingGrid[MAX_RACERS - 1])
	
	e_CurrentMissionStage = STAGE_RACE

ENDPROC

/// PURPOSE:
///    Loads the complete stage.
PROC LOAD_STAGE_COMPLETE()

	s_RaceData.bRaceOver = TRUE
	
	REQUEST_WAYPOINT_RECORDING( s_TrackData.stTrackWaypoint )
	WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED( s_TrackData.stTrackWaypoint )
		wait(0)
	ENDWHILE
	
	IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
		ve_PlayersCar = GET_PLAYERS_LAST_VEHICLE()
		SET_ENTITY_AS_MISSION_ENTITY(ve_PlayersCar)
	ELSE
		REQUEST_MODEL(GAUNTLET)
		WHILE NOT HAS_MODEL_LOADED(GAUNTLET)
			wait(0)
		ENDWHILE
		ve_PlayersCar = CREATE_VEHICLE(GAUNTLET, s_TrackData.vCheckpoint[FINISH_LINE], s_TrackData.fStartingGrid[MAX_RACERS - 1])
		SET_MODEL_AS_NO_LONGER_NEEDED(GAUNTLET)
		SET_PED_INTO_VEHICLE( PLAYER_PED_ID(), ve_PlayersCar )
	ENDIF
	
	SAFE_TELEPORT_ENTITY(GET_PLAYERS_LAST_VEHICLE(), s_TrackData.vCheckpoint[FINISH_LINE], s_TrackData.fStartingGrid[1] )
	
	SET_VEHICLE_FORWARD_SPEED(GET_PLAYERS_LAST_VEHICLE(), 30)
	
	
	CPRINTLN(DEBUG_MISSION, "Debug skipping to race complete.")
	e_CurrentMissionStage = STAGE_COMPLETE
	
ENDPROC

//╒════════════════════════════════════════════════════════════════════════════════════════╕\\
//│																						   │\\
//│										Debug Procedures								   │\\
//│																						   │\\
//╘════════════════════════════════════════════════════════════════════════════════════════╛\\

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Creates widgets.
PROC CREATE_MISSION_WIDGETS()
	widgetDebug = START_WIDGET_GROUP("Country Race")
	ADD_WIDGET_BOOL("Show Checkpoint Progress: ", bShowDebugCheckpointProgress)
	INT i
	FOR i = ENUM_TO_INT(MPF_RACER1) TO ENUM_TO_INT(MPF_RACER8)
		TEXT_LABEL LapString, CheckpointString, ProgressString
		LapString = s_MissionPed[i].Name
		LapString += " Laps "
		ADD_WIDGET_INT_READ_ONLY (LapString, s_MissionPed[i].sRacerData.iLap)
		CheckpointString = s_MissionPed[i].Name
		CheckpointString += " CP "
		ADD_WIDGET_INT_READ_ONLY (CheckpointString, s_MissionPed[i].sRacerData.iCurrentCheckpoint)
		ProgressString = s_MissionPed[i].Name
		ProgressString += " RP "
		ADD_WIDGET_INT_READ_ONLY (ProgressString, s_MissionPed[i].sRacerData.iRaceProgress)
	ENDFOR
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///    Sets up the the debug menu.
PROC SETUP_MISSION_DEBUG_MENU()

	stageMenu[INT_REPLAY_STAGE_SETUP].sTxtLabel = STAGE_SETUP_NAME
	stageMenu[INT_REPLAY_STAGE_RACE].sTxtLabel = STAGE_RACE_NAME
	stageMenu[INT_REPLAY_STAGE_COMPLETE].sTxtLabel = STAGE_COMPLETE_NAME

ENDPROC

#ENDIF

/// PURPOSE:
///    Skips the mission for menu skip.
/// PARAMS:
///    iReturnValue - The stage selected in the debug menu.
PROC MENU_SKIP(INT iReturnValue, BOOL bRemovePlayersCar = TRUE)

	DO_SCREEN_FADE_OUT(500)
	WHILE NOT IS_SCREEN_FADED_OUT()
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
		WAIT(0)
	ENDWHILE


	SET_PLAYER_OUT_OF_ANY_VEHICLE()

	MISSION_CLEANUP(FALSE, TRUE, bRemovePlayersCar)

	INITIALISE_MISSION_VARIABLES()

	SWITCH iReturnValue
		
			CASE INT_REPLAY_STAGE_SETUP
				
				SAFE_TELEPORT_ENTITY( PLAYER, VECTOR_STAGE_SETUP_WARP, FLOAT_STAGE_SETUP_HEADING, TRUE )
				LOAD_STAGE_SETUP()
				
			BREAK
			
			CASE INT_REPLAY_STAGE_RACE
			
				SAFE_TELEPORT_ENTITY( PLAYER, VECTOR_STAGE_RACE_WARP, FLOAT_STAGE_RACE_HEADING, TRUE )
				LOAD_STAGE_RACE()
				
			BREAK
			
			CASE INT_REPLAY_STAGE_COMPLETE
							
				SAFE_TELEPORT_ENTITY( PLAYER, VECTOR_STAGE_COMPLETE_WARP, FLOAT_STAGE_COMPLETE_HEADING, TRUE )
				LOAD_STAGE_COMPLETE()
				
			BREAK

		ENDSWITCH
	
	WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER))
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		
	SAFE_FADE_SCREEN_IN_FROM_BLACK_SUPPRESS_TRAFFIC()

ENDPROC

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Skips forwards to the next stage.
/// PARAMS:
///    iSkip - The int we are using for the stage.
PROC J_SKIP(INT &iSkip)
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
	KILL_ANY_CONVERSATION()
	IF ENUM_TO_INT(e_CurrentMissionStage) = (INT_NUMBER_OF_STAGES - 1)
		MISSION_FLOW_MISSION_PASSED()
		MISSION_CLEANUP()
	ELSE
		iSkip = ENUM_TO_INT(e_CurrentMissionStage) + 1
		MENU_SKIP(iSkip)
	ENDIF
ENDPROC

/// PURPOSE:
///    Skips backwards to the previous stage.
/// PARAMS:
///    iSkip - The int we are using for the stage.
PROC P_SKIP(INT &iSkip)
	IF ENUM_TO_INT(e_CurrentMissionStage) = 0
		iSkip = 0
	ELSE
		iSkip = ENUM_TO_INT(e_CurrentMissionStage) - 1
	ENDIF
	MENU_SKIP(iSkip)
ENDPROC

/// PURPOSE:
///    Debug fails the mission
PROC DEBUG_FAIL()
	SET_COUNTRY_RACE_FAILED()
	CLEAR_PED_TASKS(PLAYER_PED_ID())
	Mission_Cleanup(TRUE, TRUE)
ENDPROC

/// PURPOSE:
///    Debug passes the mission.
PROC DEBUG_PASS()
	SET_COUNTRY_RACE_PASSED()
	Mission_Cleanup()
ENDPROC

/// PURPOSE:
///    Handle all debug procedures.
PROC DEBUG_UPDATE()

	INT iSkip
	
	// Z-Skip
	IF LAUNCH_MISSION_STAGE_MENU(stageMenu, iSkip)
		MENU_SKIP(iSkip, FALSE)
	ENDIF
	
	// J-Skip
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		IF s_RaceData.bRaceStarted AND NOT s_RaceData.bRaceOver
			IF IS_ENTITY_ALIVE(ve_PlayersCar)
				SET_ENTITY_COORDS(ve_PlayersCar, s_TrackData.vCheckpoint[s_PlayerRacerData.iRaceProgress])
			ENDIF
		ELSE
			J_SKIP(iSkip)
		ENDIF
	ENDIF
	
	// P-Skip
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		P_SKIP(iSkip)
	ENDIF
	
	INT iDebugPassRace = 0
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1) iDebugPassRace = 1 ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2) iDebugPassRace = 2 ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD3) iDebugPassRace = 3 ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4) iDebugPassRace = 4 ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5) iDebugPassRace = 5 ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6) iDebugPassRace = 6 ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD7) iDebugPassRace = 7 ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD8) iDebugPassRace = 8 ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD9) iDebugPassRace = 9 ENDIF
	
	IF iDebugPassRace <> 0
		e_CurrentMissionStage = STAGE_COMPLETE
		i_StageProgress = 0
		s_PlayerRacerData.iPosition = iDebugPassRace
		s_PlayerRacerData.iFinalPosition = iDebugPassRace
		s_RaceData.bRaceOver = TRUE
		s_PlayerRacerData.iTotalTime = GET_GAME_TIMER() - s_RaceData.iRaceStartTime
		IF s_PlayerRacerData.iTotalTime < g_savedGlobals.sCountryRaceData.iBestTime OR g_savedGlobals.sCountryRaceData.iBestTime = 0
			g_savedGlobals.sCountryRaceData.iBestTime = s_PlayerRacerData.iTotalTime
		ENDIF
		CLEANUP_RACE_CHECKPOINTS(s_PlayerRacerData)
		CPRINTLN(DEBUG_MISSION, "iDebugPassRace = ", iDebugPassRace, " passing race with position: ", iDebugPassRace)
	ENDIF
	
	// S-Pass
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		e_CurrentMissionStage = STAGE_COMPLETE
		i_StageProgress = 0
		s_PlayerRacerData.iPosition = 1
		s_PlayerRacerData.iFinalPosition = 1
		s_RaceData.bRaceOver = TRUE
		s_PlayerRacerData.iTotalTime = GET_GAME_TIMER() - s_RaceData.iRaceStartTime
		IF s_PlayerRacerData.iTotalTime < g_savedGlobals.sCountryRaceData.iBestTime OR g_savedGlobals.sCountryRaceData.iBestTime = 0
			g_savedGlobals.sCountryRaceData.iBestTime = s_PlayerRacerData.iTotalTime
		ENDIF
		CLEANUP_RACE_CHECKPOINTS(s_PlayerRacerData)
		CPRINTLN(DEBUG_MISSION, "S PASS Passing race in 1st position.")
	ENDIF

	// F-Fail
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
	OR g_iDebugRaceWarp <> -1
		DEBUG_FAIL()
	ENDIF
	
	IF bShowDebugCheckpointProgress
		INT i
		FOR i = ENUM_TO_INT(MPF_RACER1) TO ENUM_TO_INT(MPF_RACER8)
			IF IS_ENTITY_ALIVE( s_MissionPed[i].sVehicle.VehicleIndex )
				DRAW_DEBUG_TEXT( GET_STRING_FROM_INT(s_MissionPed[i].sRacerData.iLap), GET_ENTITY_COORDS( s_MissionPed[i].sVehicle.VehicleIndex ) + <<0,0,1.44>> )
				DRAW_DEBUG_TEXT( GET_STRING_FROM_INT(s_MissionPed[i].sRacerData.iCurrentCheckpoint), GET_ENTITY_COORDS( s_MissionPed[i].sVehicle.VehicleIndex ) + <<0,0,1.38>> )
				DRAW_DEBUG_TEXT( GET_STRING_FROM_FLOAT(s_MissionPed[i].sRacerData.fDistanceToNextCheckpoint), GET_ENTITY_COORDS( s_MissionPed[i].sVehicle.VehicleIndex ) + <<0,0,1.32>> )
				STRING RSL
				IF s_MissionPed[i].sRacerData.eRacerSkillLevel 		= RSL_POOR 		RSL = "Poor"
				ELIF s_MissionPed[i].sRacerData.eRacerSkillLevel	= RSL_AVERAGE 	RSL = "Average"
				ELSE																RSL = "Professional"
				ENDIF
				DRAW_DEBUG_TEXT( RSL, GET_ENTITY_COORDS( s_MissionPed[i].sVehicle.VehicleIndex ) + <<0,0,1.26>> )
				DRAW_DEBUG_TEXT( GET_STRING_FROM_FLOAT(s_MissionPed[i].sRacerData.fSpeed), GET_ENTITY_COORDS( s_MissionPed[i].sVehicle.VehicleIndex ) + <<0,0,1.2>> )
				DRAW_DEBUG_TEXT( GET_STRING_FROM_FLOAT(GET_ENTITY_SPEED(s_MissionPed[i].sVehicle.VehicleIndex)),  GET_ENTITY_COORDS( s_MissionPed[i].sVehicle.VehicleIndex ) + <<0,0,1.14>> )
			ENDIF
		ENDFOR
		DRAW_DEBUG_TEXT( GET_STRING_FROM_INT(s_PlayerRacerData.iLap), GET_ENTITY_COORDS( PLAYER ) + <<0,0,1.6>> )
		DRAW_DEBUG_TEXT( GET_STRING_FROM_INT(s_PlayerRacerData.iCurrentCheckpoint), GET_ENTITY_COORDS( PLAYER ) + <<0,0,1.5>> )
		DRAW_DEBUG_TEXT( GET_STRING_FROM_FLOAT(s_PlayerRacerData.fDistanceToNextCheckpoint), GET_ENTITY_COORDS( PLAYER ) + <<0,0,1.4>> )
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Initialises the debug widgets and menu.
PROC DEBUG_SETUP()
	CREATE_MISSION_WIDGETS()
	SETUP_MISSION_DEBUG_MENU()
ENDPROC

#ENDIF

INT iPrevClosestRacer
INT iRaceMixTimer
/// PURPOSE:
///     Adds the closest racer to the audio mixer and removes the previous closest
PROC ADD_CLOSEST_RACER_TO_MIX()
	IF GET_GAME_TIMER() > iRaceMixTimer
		FLOAT fDist
		FLOAT fClosestDist = 9999999.9
		INT iClosestRacer
		INT i
		
		FOR i = Enum_TO_INT(MPF_RACER1) TO ENUM_TO_INT(MPF_RACER8)
			IF IS_VEHICLE_OK(s_MissionPed[i].sVehicle.VehicleIndex)
				fDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), s_MissionPed[i].sVehicle.VehicleIndex)
				IF fDist < fClosestDist
					fDist = fClosestDist
					iClosestRacer = i
				ENDIF
			ENDIF
		ENDFOR
		
		IF iPrevClosestRacer > -1 AND  IS_VEHICLE_OK(s_MissionPed[iPrevClosestRacer].sVehicle.VehicleIndex)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(s_MissionPed[iPrevClosestRacer].sVehicle.VehicleIndex)
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(s_MissionPed[iPrevClosestRacer].sVehicle.VehicleIndex,"STREET_RACE_NPC_GENERAL")
		ENDIF
		IF IS_VEHICLE_OK(s_MissionPed[iClosestRacer].sVehicle.VehicleIndex)
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(s_MissionPed[iClosestRacer].sVehicle.VehicleIndex,"STREET_RACE_NPC_CLOSEST")
		ENDIF
		
		iPrevClosestRacer = iClosestRacer
		iRaceMixTimer = GET_GAME_TIMER() + 2000
	ENDIF
ENDPROC


/// PURPOSE:
///    Updates the racers.
PROC UPDATE_RACERS()

	INT i
	
	FOR i = ENUM_TO_INT( MPF_RACER1 ) TO ENUM_TO_INT( MPF_RACER8 )
		
		IF NOT s_MissionPed[i].bKilled
		
			IF s_MissionPed[i].bCreated 
			
				IF IS_ENTITY_ALIVE( s_MissionPed[i].PedIndex )
					
					BOOL bCanRevEngine = TRUE
					
					IF i_TimeLapseProgress < 3 bCanRevEngine = FALSE ENDIF // Stop AI cars revving during timelapse.

					HANDLE_RACER( s_MissionPed[i], s_PlayerRacerData, bCanRevEngine)
					
					ADD_CLOSEST_RACER_TO_MIX()
					
					IF IS_ENTITY_ALIVE( s_MissionPed[i].sVehicle.VehicleIndex )
						
						IF NOT DOES_BLIP_EXIST(s_MissionPed[i].BlipIndex)
							MANAGE_SLIDY_BLIP_FOR_ENTITY( s_MissionPed[i].BlipIndex, s_MissionPed[i].sVehicle.VehicleIndex, TRUE, 10, TRUE )
							SET_BLIP_NAME_FROM_TEXT_FILE(s_MissionPed[i].BlipIndex, "CRACEBLIP")
						ELSE
							MANAGE_SLIDY_BLIP_FOR_ENTITY( s_MissionPed[i].BlipIndex, s_MissionPed[i].sVehicle.VehicleIndex, TRUE, 10, FALSE )
						ENDIF
						
					ENDIF
					
				ELSE
					
					REMOVE_PED( s_MissionPed[i].PedIndex )
					SAFE_REMOVE_BLIP( s_MissionPed[i].BlipIndex )
					s_MissionPed[i].bKilled = TRUE
					
				ENDIF
			
			ELSE
				
				IF NOT s_RaceData.bRaceOver AND IS_MISSION_BIT_FLAG_SET( MBF_TIMELAPSE_INITIATED, i_MissionBitFlags )

					CREATE_MISSION_PED( s_MissionPed[i], FALSE, TRUE )
				
				ENDIF
				
			ENDIF
		
		ENDIF

	ENDFOR

ENDPROC

CONST_INT AMBIENT_PED_STREAM_IN_DIST 150 * 150
CONST_INT AMBIENT_PED_STREAM_OUT_DIST 200 * 200

FUNC STRING GET_AMBIENT_PED_SCENARIO(INT iPedIndex)
	
	SWITCH INT_TO_ENUM( AMBIENT_PED_FLAGS, iPedIndex)
	
		CASE APF_STARTLINE_1
		CASE APF_STARTLINE_2
		CASE APF_STARTLINE_4
		CASE APF_CHECKPOINT0_1
		CASE APF_CHECKPOINT2_1
		CASE APF_CHECKPOINT2_2
		CASE APF_CHECKPOINT3_1
		CASE APF_STAND1_1
		CASE APF_STAND1_3
		CASE APF_STAND1_4
		CASE APF_STAND2_1
		CASE APF_STAND2_2
		CASE APF_STAND2_3
		CASE APF_FINISHLINE_1
		CASE APF_FINISHLINE_2
		CASE APF_FINISHLINE_3
		CASE APF_FINISHLINE_4
			RETURN "WORLD_HUMAN_CHEERING"
		BREAK
		
		CASE APF_STARTLINE_3
		CASE APF_CHECKPOINT0_2
		CASE APF_CHECKPOINT3_2
		CASE APF_STAND1_2
		CASE APF_STAND2_4
			RETURN "WORLD_HUMAN_MOBILE_FILM_SHOCKING"
		BREAK
		
		CASE APF_HOTDOGSTAND
			RETURN "WORLD_HUMAN_STAND_IMPATIENT"
		BREAK
	
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

/// PURPOSE:
///    Handles the AI logic of the ambient peds.
/// PARAMS:
///    PedData - The ped array we want to handle.
PROC HANDLE_AMBIENT_PEDS(PED_STRUCT &PedData[])
	
	INT i
	
	REPEAT COUNT_OF(PedData) i
		
		IF IS_ENTITY_ALIVE(PedData[i].PedIndex)
		
			IF NOT IS_PED_IN_COMBAT(PedData[i].PedIndex)
			AND NOT IS_PED_FLEEING(PedData[i].PedIndex)
			AND NOT IS_PED_RAGDOLL(PedData[i].PedIndex)
				
				SET_IK_TARGET(PedData[i].PedIndex, IK_PART_HEAD, PLAYER_PED_ID(), 0, <<0,0,0>>, ITF_DEFAULT)				
				
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(PedData[i].PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE )
					OPEN_SEQUENCE_TASK(PedData[i].SequenceIndex)
						TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID(), 2000)
						TASK_START_SCENARIO_IN_PLACE(null, GET_AMBIENT_PED_SCENARIO(i), -1, TRUE)
					CLOSE_SEQUENCE_TASK(PedData[i].SequenceIndex)
					TASK_PERFORM_SEQUENCE(PedData[i].PedIndex, PedData[i].SequenceIndex)
					CLEAR_SEQUENCE_TASK(PedData[i].SequenceIndex)
					PedData[i].iTaskTimer = GET_GAME_TIMER()
				ELSE
					
					IF INT_TO_ENUM(AMBIENT_PED_FLAGS, i) != APF_HOTDOGSTAND // If the ped isn't the hotdog guy turn to face players car.
					
						//If they aren't facing the player clear tasks so that they start the sequence again
						IF GET_ENTITY_HEADING(PedData[i].PedIndex) - GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PedData[i].PedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 60
						OR GET_ENTITY_HEADING(PedData[i].PedIndex) - GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PedData[i].PedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < -60
							IF GET_GAME_TIMER() > PedData[i].iTaskTimer + 5000
								CLEAR_PED_TASKS(PedData[i].PedIndex)
							ENDIF
						ELSE
							IF GET_SEQUENCE_PROGRESS(PedData[i].PedIndex) > 0
								IF GET_GAME_TIMER() > PedData[i].iTaskTimer + 4000
									IF INT_TO_ENUM(AMBIENT_PED_FLAGS, i) = APF_STARTLINE_3
									OR INT_TO_ENUM(AMBIENT_PED_FLAGS, i) = APF_CHECKPOINT0_2
									OR INT_TO_ENUM(AMBIENT_PED_FLAGS, i) =  APF_CHECKPOINT3_2
									OR INT_TO_ENUM(AMBIENT_PED_FLAGS, i) =  APF_STAND1_2
									OR INT_TO_ENUM(AMBIENT_PED_FLAGS, i) =  APF_STAND2_4
										SET_IK_TARGET(PedData[i].PedIndex, IK_PART_ARM_RIGHT, PLAYER_PED_ID(), 0, <<0,0,0>>, ITF_DEFAULT, 500, 300)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
	
			ELSE
				IF IS_PED_RAGDOLL(PedData[i].PedIndex)
					SET_ENTITY_HEALTH(PedData[i].PedIndex, 0)
				ELSE
					REMOVE_PED(PedData[i].PedIndex)
				ENDIF
			ENDIF
		
		ENDIF
		
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Streams ambient peds in and out as the player travels around the track.
PROC UPDATE_AMBIENT_PEDS()

	INT i
	
	REPEAT COUNT_OF( s_AmbientPed ) i
	
		IF NOT s_AmbientPed[i].bKilled
			
			IF s_AmbientPed[i].bCreated
			
				IF IS_ENTITY_ALIVE(s_AmbientPed[i].PedIndex)

					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_AmbientPed[i].PedIndex)) > AMBIENT_PED_STREAM_OUT_DIST
				
						REMOVE_PED(s_AmbientPed[i].PedIndex, TRUE)
						s_AmbientPed[i].bCreated = FALSE
					
					ENDIF
				
				ELSE
					
					REMOVE_PED(s_AmbientPed[i].PedIndex)
					s_AmbientPed[i].bKilled = TRUE
					
				ENDIF
			
			ELSE
				
				IF IS_MISSION_BIT_FLAG_SET(MBF_TIMELAPSE_INITIATED, i_MissionBitFlags)
				AND NOT s_RaceData.bRaceOver
				
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), s_AmbientPed[i].vPosition) < AMBIENT_PED_STREAM_IN_DIST
					
						CREATE_AMBIENT_PED( s_AmbientPed[i], FALSE )
					
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDIF
	
	ENDREPEAT
	
						
	HANDLE_AMBIENT_PEDS(s_AmbientPed)
					

ENDPROC

/// PURPOSE:
///    Updates which checkpoint the player is currently on.
PROC HANDLE_PLAYER_RACE_PROGRESS()

	IF s_RaceData.bRaceStarted AND NOT s_RaceData.bRaceOver

		IF s_PlayerRacerData.iRaceProgress < MAX_CHECKPOINTS
			s_PlayerRacerData.fDistanceToNextCheckpoint = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER, s_TrackData.vCheckpoint[s_PlayerRacerData.iRaceProgress])
		ENDIF
		
		SWITCH s_PlayerRacerData.iRaceProgress
				
				//Race to finish line.
				CASE 5 //This case needs to be the index of the finish line.
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), s_TrackData.vFinishLineMin, s_TrackData.vFinishLineMax, s_TrackData.fFinishLineWidth)
					
						s_PlayerRacerData.iLap++
						s_PlayerRacerData.iCurrentCheckpoint++
						IF s_PlayerRacerData.iLap < s_TrackData.iNumLaps
							s_PlayerRacerData.iCurrentLapTime = GET_GAME_TIMER() - s_PlayerRacerData.iLapTimer
							IF s_PlayerRacerData.iCurrentLapTime < s_PlayerRacerData.iBestLapTime s_PlayerRacerData.iBestLapTime = s_PlayerRacerData.iCurrentLapTime ENDIF
							s_PlayerRacerData.iLapTimer = GET_GAME_TIMER()
							s_PlayerRacerData.iRaceProgress = 0
							s_LapText.eBigTextState = BTS_LOAD
						ELSE
							s_PlayerRacerData.iTotalTime = GET_GAME_TIMER() - s_RaceData.iRaceStartTime
							IF s_PlayerRacerData.iTotalTime < g_savedGlobals.sCountryRaceData.iBestTime OR g_savedGlobals.sCountryRaceData.iBestTime = 0
								g_savedGlobals.sCountryRaceData.iBestTime = s_PlayerRacerData.iTotalTime
							ENDIF
							s_PlayerRacerData.bFinished = TRUE
							CPRINTLN(DEBUG_MISSION, "Player Finished.")
							s_RaceData.bRaceOver = TRUE
						ENDIF
						PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
						CLEANUP_RACE_CHECKPOINTS(s_PlayerRacerData)
					ENDIF
				BREAK
				
				DEFAULT
					IF s_PlayerRacerData.fDistanceToNextCheckpoint < 20
						s_PlayerRacerData.iCurrentCheckpoint++
						s_PlayerRacerData.iRaceProgress++
						PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
						CLEANUP_RACE_CHECKPOINTS(s_PlayerRacerData)
					ENDIF
				BREAK
				
		ENDSWITCH
		
		IF NOT s_RaceData.bRaceOver
			
			IF NOT b_PlayerBoosting
				IF GET_GAME_TIMER() < s_RaceData.iRaceStartTime + 500
					IF CAN_VEHICLE_BOOST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
							IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
								SET_VEHICLE_BOOST_ACTIVE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
							ENDIF
							i_BoostTimer = GET_GAME_TIMER() + 2000
							ANIMPOSTFX_PLAY("RaceTurbo", 0, FALSE)
							b_PlayerBoosting = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF GET_GAME_TIMER() > i_BoostTimer OR NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
						SET_VEHICLE_BOOST_ACTIVE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
					ENDIF
					b_PlayerBoosting = FALSE
				ELSE
					APPLY_FORCE_TO_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), APPLY_TYPE_FORCE, <<0.0, 20.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
				ENDIF
			ENDIF
			
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
					
			DISPLAY_LAPS_BIG_TEXT(s_LapText, s_PlayerRacerData)
			
//			DRAW_LOOP_RACE_HUD(GET_GAME_TIMER() - s_RaceData.iRaceStartTime, 0, "", s_PlayerRacerData.iLap+1,s_TrackData.iNumLaps,"",s_PlayerRacerData.iPosition,MAX_RACERS,"",0,HUD_COLOUR_WHITE,-1,-1,"",DEFAULT,DEFAULT,DEFAULT,DEFAULT,
//			DEFAULT,DEFAULT,DEFAULT,GET_GAME_TIMER() - s_PlayerRacerData.iLapTimer,"")
			
			
			DRAW_LOOP_RACE_HUD(GET_GAME_TIMER() - s_RaceData.iRaceStartTime, 	//Race Time
			0, 																	//iComparison
			"", 																//Timer Title "Time"
			s_PlayerRacerData.iLap+1, 											//Current Lap
			s_TrackData.iNumLaps, 												//Total Laps
			"", 																//Lap Title "Lap"
			s_PlayerRacerData.iPosition, 										//Current Position
			MAX_RACERS, 														//Total Racers
			"", 																//Position title "Position"
			0,																	//Extra Time
			HUD_COLOUR_WHITE, 													//Placement Colour
			s_PlayerRacerData.iCurrentCheckpoint + 1, 							//Current Checkpoint
			30, 																//Total Checkpoints
			"", 																//Checkpoint Title
			HUD_COLOUR_WHITE, 													//Checkpoint Colour
			-1, 																//Meters
			-1, 																//Max Meters
			"", 																//Meters Title
			HUD_COLOUR_RED, 													//Meter Colour
			g_SavedGlobals.sCountryRaceData.iBestTime, 							//Best Time
			"CRACEBTIME",														//Best Time Title
			GET_GAME_TIMER() - s_PlayerRacerData.iLapTimer,						//Lap Time
			"", 																//Lap Time Title
			PODIUMPOS_NONE, 													//Podium Position
			TRUE, 																//Display Milliseconds
			-1, 																//Flashing
			"", 																//Float Title
			-1.0, 																//Float Value
			HUD_COLOUR_WHITE, 													//Float Colour
			HUD_COLOUR_WHITE, 													//Best Time Colour
			-1.0,																//Distance Check
			-1,										 							//Worlds Best Time
			FALSE, 																//Time Trial
			-1, 																//Checkpoint Time
			-1,																	//Challenge Time
			"",																	//World Name
			TRUE)																//Use Personal Best instead of World Best
			
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MULTIPLAYER_INFO)
				IF NOT g_Show_Lap_Dpad
					g_Show_Lap_Dpad = TRUE
				ENDIF
			ELSE
				IF g_Show_Lap_Dpad
					g_Show_Lap_Dpad = FALSE
				ENDIF
			ENDIF
			
			DRAW_RACE_CHECKPOINTS(s_PlayerRacerData)
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC FAIL_RACE(STRING sFailText)
	
	PROGRESS_STAGE(STAGE_FAIL)
	st_FailString = sFailText

ENDPROC

PROC CHECK_FAIL()

	IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1688.298950,3336.850586,24.684418>>, <<2475.162842,2991.082275,120.054871>>, 480.000000)
//		FAIL_RACE("CRACEFAIL1")
		PROGRESS_STAGE(STAGE_FAIL)
		st_FailString = "CRACEFAIL1"
	ENDIF
	
ENDPROC


PROC QUIT_RACE()
	
	SAFE_FADE_SCREEN_OUT_TO_BLACK_SUPPRESS_TRAFFIC()
	SAFE_TELEPORT_ENTITY(ve_PlayersCar, <<1948.2572, 3142.9358, 45.8642>>, 51.7219, TRUE)
	SET_COUNTRY_RACE_FAILED()
	MISSION_CLEANUP(TRUE, TRUE, FALSE)
	
ENDPROC

PROC HANDLE_RACE_QUITTING()
	IF s_RaceData.bRaceStarted AND NOT s_RaceData.bRaceOver	
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			IF GET_GAME_TIMER() > i_QuitTimer + 500
				DRAW_GENERIC_METER(GET_GAME_TIMER() - i_QuitTimer - 500, 3000, "CRACEQUIT", HUD_COLOUR_RED, 0, HUDORDER_EIGHTHBOTTOM, -1, -1, FALSE, TRUE)
				IF GET_GAME_TIMER() > i_QuitTimer + 3500
					CPRINTLN(DEBUG_MISSION, "Race quit by player.")
					FAIL_RACE("CRACEFAIL1")
				ENDIF
			ENDIF
		ELSE
			i_QuitTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

INT i_StoppedTimer, i_StoppedSound

PROC INIT_STOPPED_SOUND()
	i_StoppedSound = GET_SOUND_ID()
	i_StoppedTimer = -1
ENDPROC

FUNC INT GET_CLOSEST_RACER()
	INT i_index
	FLOAT fDistance = 0
	FLOAT fClosestDistance = 9999999999.9
	INT iClosest
	
	FOR i_index = ENUM_TO_INT(MPF_RACER1) TO ENUM_TO_INT(MPF_RACER8)
		IF IS_VEHICLE_OK(s_MissionPed[i_index].sVehicle.VehicleIndex)
			fDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), s_MissionPed[i_index].sVehicle.VehicleIndex)
			
			IF fDistance < fClosestDistance
				fClosestDistance = fDistance
				iClosest = i_index
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iClosest
ENDFUNC

/// PURPOSE:
///    Main update for the mission, anything that needs to happen independant of stage goes in here.
PROC MAIN_UPDATE()
	
	CHECK_FAIL()
	
	HANDLE_PLAYER_RACE_PROGRESS()
	UPDATE_RACERS()
	UPDATE_AMBIENT_PEDS()			
	UPDATE_PLAYER_POSITION(s_MissionPed, s_PlayerRacerData)	
	UPDATE_SCRIPT_ENTITY_LOS_DISTRIBUTED_CHECKS( s_LOSChecks, i_LOSCheckIndex )
	DISABLE_SELECTOR_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()

	
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_ATTACK )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_AIM )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_AIM ) //url:bugstar:2059984
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_ATTACK2 )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON )

	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)

ENDPROC

/// PURPOSE:
///    Checks if the player is driving a large vehicle
/// RETURNS:
///    true if the player is driving a large vehicle
FUNC BOOL PLAYERS_CAR_IS_BIG()
	IF IS_VEHICLE_OK(ve_PlayersCar)
		MODEL_NAMES mnCar = GET_ENTITY_MODEL(ve_PlayersCar)
		
		IF mnCar = BALLER
		OR mnCar = BALLER2
		OR mnCar = BISON
		OR mnCar = BISON2
		OR mnCar = BISON3
		OR mnCar = BOBCATXL
		OR mnCar = CAVALCADE
		OR mnCar = CAVALCADE2
		OR mnCar = CRUSADER
		OR mnCar = DUBSTA
		OR mnCar = DUBSTA2
		OR mnCar = FQ2
		OR mnCar = GRANGER
		OR mnCar = GRESLEY
		OR mnCar = LANDSTALKER
		OR mnCar = MESA
		OR mnCar = MESA2
		OR mnCar = MESA3
		OR mnCar = PATRIOT
		OR mnCar = RADI
		OR mnCar = RANCHERXL
		OR mnCar = RANCHERXL2
		OR mnCar = REBEL
		OR mnCar = ROCOTO
		OR mnCar = SADLER
		OR mnCar = SADLER2
		OR mnCar = SANDKING
		OR mnCar = SANDKING2
		OR mnCar = SEMINOLE
		OR mnCar = SUPERD
		OR mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("huntley"))
		OR mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("DUBSTA3"))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player is driving a very large vehicle
/// RETURNS:
///    TRUE if the player is driving a very large vehicle
FUNC BOOL PLAYERS_CAR_IS_VERY_BIG()
	IF IS_VEHICLE_OK(ve_PlayersCar)
		MODEL_NAMES mnCar = GET_ENTITY_MODEL(ve_PlayersCar)
		
		IF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("MONSTER"))
		OR mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("MARSHALL"))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs a timelapse to a desired time.
/// RETURNS:
///    Whethe the timelapse is completed.
FUNC BOOL DO_RACE_INTRO_CUTSCENE()

	BOOL bAssetsCreated
	INT i
	
	SWITCH i_TimeLapseProgress
		
		CASE 0			
			IF REQUEST_AMBIENT_AUDIO_BANK("TIME_LAPSE")

                s_Timelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")

				ADD_CAM_SPLINE_NODE(s_Timelapse.splineCamera, <<2002.6934, 3097.8193, 50.8232>>, <<5.4256, 0.0276, -170.9332>>, 10000, CAM_SPLINE_NODE_NO_FLAGS)
				ADD_CAM_SPLINE_NODE(s_Timelapse.splineCamera, <<2002.8185, 3097.4880, 48.0966>>, <<7.3944, 0.0276, -159.5117>>, 10000, CAM_SPLINE_NODE_NO_FLAGS)
				
				SET_CAM_FOV(s_Timelapse.splineCamera, 40)
                SET_CAM_ACTIVE(s_Timelapse.splineCamera, TRUE)
                PLAY_SOUND_FRONTEND(-1, "TIME_LAPSE_MASTER")
                SET_TODS_CUTSCENE_RUNNING(s_Timelapse, TRUE, FALSE, DEFAULT, DEFAULT, FALSE)
				CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000, TRUE)
			   	DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				SETTIMERA(0)
				SET_MISSION_BIT_FLAG( MBF_TIMELAPSE_INITIATED, i_MissionBitFlags )
				SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					CASE CHAR_MICHAEL
						ANIMPOSTFX_PLAY( "MinigameEndMichael", 5000, FALSE )
					BREAK
					CASE CHAR_FRANKLIN
						ANIMPOSTFX_PLAY( "MinigameEndFranklin", 5000, FALSE )
					BREAK
					CASE CHAR_TREVOR
						ANIMPOSTFX_PLAY( "MinigameEndTrevor", 5000, FALSE )
					BREAK
				ENDSWITCH
           		i_TimeLapseProgress++
            ENDIF
		BREAK
		CASE 1
			 IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(GET_RACE_TIMEOFDAY(), 0, "", "", s_Timelapse)
                RELEASE_AMBIENT_AUDIO_BANK()
                SETTIMERA(0)				
				i_TimeLapseProgress++
            ENDIF
		BREAK
		CASE 2
			IF TIMERA() > 1500
				IF IS_ENTITY_ALIVE(ve_PlayersCar)			
					TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), ve_PlayersCar, s_TrackData.vStartingGrid[MAX_CHECKPOINTS -1], 0.95, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(ve_PlayersCar), DRIVINGMODE_AVOIDCARS, 3.5, -1)
               		SET_RADIO_TO_STATION_NAME("RADIO_01_CLASS_ROCK")
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("TOD_SHIFT_SCENE") 
					STOP_SOUND(s_Timelapse.iSplineStageSound)
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
					STOP_AUDIO_SCENE("TOD_SHIFT_SCENE")
				ENDIF
				CPRINTLN(DEBUG_MISSION, "Intro Cutscene - Panning cam to players bonnet.")
				SET_RADIO_STATION_MUSIC_ONLY("RADIO_01_CLASS_ROCK", TRUE)

				FOR i = ENUM_TO_INT(MPF_RACER1) TO ENUM_TO_INT(MPF_RACER8)
					IF IS_ENTITY_ALIVE(s_MissionPed[i].sVehicle.VehicleIndex)
						SET_VEHICLE_HEADLIGHT_SHADOWS(s_MissionPed[i].sVehicle.VehicleIndex, HEADLIGHTS_CAST_FULL_SHADOWS)
					ENDIF
				ENDFOR
				
				IF PLAYERS_CAR_IS_BIG()
					v_PosInitial = <<-0.7054, 6.3148, 2.0821>>
					v_RotInitial = <<0.9495, 3.9011, 2.7419>>
					v_PosDestination = <<-0.5670, 2.6724, 1.2077>>
					v_RotDestination = <<0.0940, -0.2423, 0.7483>>
				ELIF PLAYERS_CAR_IS_VERY_BIG()
					v_PosInitial = <<-1.6831, 6.0271, 4.2228>>
					v_RotInitial = <<-0.0245, 3.5705, 4.6855>>
					v_PosDestination = <<-1.0281, 4.6872, 1.1868>>
					v_RotDestination = <<-0.1850, 1.8112, 1.1221>>
				ELSE
					v_PosInitial = <<-1.5802, 5.8398, 2.2274>>
					v_RotInitial = <<0.1970, 3.5006, 2.8356>>
					v_PosDestination = <<-0.6678, 1.9740, 0.8876>>
					v_RotDestination = <<0.1440, -0.9013, 0.4170>>
				ENDIF
				
				cam_Initial = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
				SET_CAM_FOV(cam_Initial, 40.0)
				ATTACH_CAM_TO_ENTITY(cam_Initial, ve_PlayersCar, v_PosInitial)
				POINT_CAM_AT_ENTITY(cam_Initial, ve_PlayersCar, v_RotInitial)
				
				cam_Destination = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
				SET_CAM_FOV(cam_Destination, 40.0)
				ATTACH_CAM_TO_ENTITY(cam_Destination, ve_PlayersCar, v_PosDestination)
				POINT_CAM_AT_ENTITY(cam_Destination, ve_PlayersCar, v_RotDestination)
				
				SET_CAM_ACTIVE(cam_Initial, TRUE)
				SET_CAM_ACTIVE_WITH_INTERP(cam_Destination,cam_Initial,4000,GRAPH_TYPE_SIN_ACCEL_DECEL)
				RENDER_SCRIPT_CAMS(TRUE, FALSE, 0)
				SHAKE_CAM(cam_Initial,"ROAD_VIBRATION_SHAKE",0.5)
				SHAKE_CAM(cam_Destination,"ROAD_VIBRATION_SHAKE",0.5)

				SETTIMERA(0)
				
				START_AUDIO_SCENE("RACE_INTRO_GENERIC")
				IF LOAD_STREAM("STOCK_RACE_INTRO","ROAD_RACE_SOUNDSET")
					PLAY_STREAM_FRONTEND()
				ENDIF
				i_TimeLapseProgress++
			ENDIF
		BREAK
		
		CASE 3
			IF TIMERA() > 6000
				DESTROY_ALL_CAMS()
				CPRINTLN(DEBUG_MISSION, "Intro Cutscene - Panning across race front.")
				cam_Initial = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1960.1542, 3125.9194, 47.8242>>, <<-2.4987, 0.0002, -95.2875>>, 25.0)
				cam_Destination = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1963.0167, 3129.4304, 47.4810>>, <<-3.0689, 0.0002, -114.3019>>, 25.0)
				SET_CAM_ACTIVE(cam_Initial, TRUE)
				SET_CAM_ACTIVE_WITH_INTERP(cam_Destination,cam_Initial,7000,GRAPH_TYPE_LINEAR)
				SHAKE_CAM(cam_Initial,"HAND_SHAKE", 0.1)
				SHAKE_CAM(cam_Destination,"HAND_SHAKE", 0.1)
				SETTIMERA(0)
				i_TimeLapseProgress++
			ENDIF
		BREAK
		
		CASE 4
			IF TIMERA() > 4900
				IF IS_SCRIPT_TASK_RUNNING_OR_STARTING( PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					IF IS_ENTITY_ALIVE(ve_PlayersCar)
						SET_VEHICLE_HANDBRAKE(ve_PlayersCar, TRUE)
					ENDIF
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF	
			ENDIF
			
			IF TIMERA() > 7000
				DESTROY_ALL_CAMS()
				CPRINTLN(DEBUG_MISSION, "Intro Cutscene - Easing back to gameplay cam.")
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				cam_Initial = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,  <<1990.6644, 3113.4822, 47.1558>>, <<0.8047, 0.0484, 56.7722>>, 50.0, TRUE)
				cam_Destination = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), GET_GAMEPLAY_CAM_FOV())
				SET_CAM_ACTIVE_WITH_INTERP(cam_Destination, cam_Initial, 4000, GRAPH_TYPE_DECEL)
				SETTIMERA(0)
				i_TimeLapseProgress++
			ENDIF
		
		BREAK
		
		CASE 5
			RETURN TRUE
		BREAK
		
		CASE 6
			IF TIMERA() > 3000
				bAssetsCreated = TRUE
				
				FOR i = ENUM_TO_INT(MPF_RACER1) TO ENUM_TO_INT(MPF_RACER8)
					IF NOT DOES_ENTITY_EXIST(s_MissionPed[i].PedIndex)
						bAssetsCreated = FALSE
					ENDIF
				ENDFOR
				
				IF NOT DOES_ENTITY_EXIST(ve_PlayersCar)
					bAssetsCreated = FALSE
				ENDIF
				
				IF bAssetsCreated
					SET_TODS_CUTSCENE_RUNNING( s_Timelapse, FALSE, DEFAULT, DEFAULT, DEFAULT, FALSE)
					IF IS_AUDIO_SCENE_ACTIVE("TOD_SHIFT_SCENE") 
						STOP_SOUND(s_Timelapse.iSplineStageSound)
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
						STOP_AUDIO_SCENE("TOD_SHIFT_SCENE")
					ENDIF
					SET_RADIO_TO_STATION_NAME("RADIO_01_CLASS_ROCK")
					SET_RADIO_STATION_MUSIC_ONLY("RADIO_01_CLASS_ROCK", TRUE)
					START_AUDIO_SCENE("RACE_INTRO_GENERIC")
					DESTROY_ALL_CAMS()
					STOP_STREAM()
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK

	ENDSWITCH
	
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
        SAFE_FADE_SCREEN_OUT_TO_BLACK_SUPPRESS_TRAFFIC()
		SET_CLOCK_TIME(GET_RACE_TIMEOFDAY(),0,0)
		SETTIMERA(0)
		IF IS_ENTITY_ALIVE(ve_PlayersCar)
			SET_VEHICLE_HANDBRAKE(ve_PlayersCar, TRUE)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SAFE_TELEPORT_ENTITY(ve_PlayersCar, s_TrackData.vStartingGrid[MAX_RACERS - 1], s_TrackData.fStartingGrid[MAX_RACERS - 1], TRUE)
			SET_VEHICLE_ON_GROUND_PROPERLY(ve_PlayersCar)
		ENDIF
		i_TimeLapseProgress = 6
    ENDIF
	
	RETURN FALSE

ENDFUNC

   
/// PURPOSE:
///    Does the logic for the setup stage.
PROC DO_STAGE_SETUP()

	SWITCH i_StageProgress
	
		CASE 0
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(INT_REPLAY_STAGE_SETUP, STAGE_SETUP_NAME, FALSE, TRUE)
			i_TimeLapseProgress = 0
			i_StageProgress++
			
		BREAK
		
		CASE 1
			
			MISSION_PED_FLAGS ePed
			
			IF DO_RACE_INTRO_CUTSCENE()
			
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
				SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)			
				
				FOR ePed = MPF_RACER1 TO MPF_RACER8
					IF IS_ENTITY_ALIVE(s_MissionPed[ePed].PedIndex)
						SET_ENTITY_INVINCIBLE(s_MissionPed[ePed].PedIndex, FALSE)
					ENDIF
					IF IS_ENTITY_ALIVE(s_MissionPed[ePed].sVehicle.VehicleIndex)
						SET_ENTITY_INVINCIBLE(s_MissionPed[ePed].sVehicle.VehicleIndex, FALSE)
					ENDIF
				ENDFOR
				
				SAFE_FADE_SCREEN_IN_FROM_BLACK_SUPPRESS_TRAFFIC()

				i_StageProgress++
			
			ELSE
				
				IF IS_MISSION_BIT_FLAG_SET( MBF_TIMELAPSE_INITIATED, i_MissionBitFlags )

					REQUEST_MINIGAME_COUNTDOWN_UI( s_RaceData.sCountDown )	
					REQUEST_WAYPOINT_RECORDING( s_TrackData.stTrackWaypoint )
					SET_BUILDING_STATE(BUILDINGNAME_IPL_COUNTRY_RACETRACK, BUILDINGSTATE_DESTROYED, FALSE)
					LOAD_STREAM("STOCK_RACE_INTRO","ROAD_RACE_SOUNDSET")
					
					FOR ePed = MPF_RACER1 TO MPF_RACER8
						IF IS_ENTITY_ALIVE(s_MissionPed[ePed].PedIndex)
							SET_ENTITY_INVINCIBLE(s_MissionPed[ePed].PedIndex, TRUE)
						ENDIF
						IF IS_ENTITY_ALIVE(s_MissionPed[ePed].sVehicle.VehicleIndex)
							SET_ENTITY_INVINCIBLE(s_MissionPed[ePed].sVehicle.VehicleIndex, TRUE)
						ENDIF
					ENDFOR
					
					IF NOT DOES_ENTITY_EXIST(ve_PlayersCar)
											
						IF NOT IS_SPHERE_VISIBLE( GET_ENTITY_COORDS(PLAYER_PED_ID()), 2 )
						AND NOT IS_SPHERE_VISIBLE( s_TrackData.vStartingGrid[MAX_RACERS - 1], 2 )
							
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								ve_PlayersCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								SET_ENTITY_AS_MISSION_ENTITY(ve_PlayersCar)
								SAFE_TELEPORT_ENTITY(ve_PlayersCar, s_TrackData.vStartingGrid[MAX_RACERS - 1] + v_DriveToStartlineOffset, s_TrackData.fStartingGrid[MAX_RACERS - 1])
							ELSE
								REQUEST_MODEL(GAUNTLET)
								IF HAS_MODEL_LOADED(GAUNTLET)
									ve_PlayersCar = CREATE_VEHICLE(GAUNTLET, s_TrackData.vStartingGrid[MAX_RACERS - 1] + v_DriveToStartlineOffset, s_TrackData.fStartingGrid[MAX_RACERS - 1])
								ENDIF
							ENDIF
							
							IF IS_ENTITY_ALIVE(ve_PlayersCar)
								SET_VEHICLE_FIXED(ve_PlayersCar)
							ENDIF

						ENDIF
						
					ELSE
						
						IF IS_ENTITY_ALIVE(ve_PlayersCar)

							IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), ve_PlayersCar)
								SET_PED_INTO_VEHICLE( PLAYER_PED_ID(), ve_PlayersCar )
							ENDIF
							IF GET_VEHICLE_DOOR_LOCK_STATUS(ve_PlayersCar) != VEHICLELOCK_LOCKED_PLAYER_INSIDE
								SET_VEHICLE_DOORS_LOCKED(ve_PlayersCar, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
							ENDIF
							
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						
						ENDIF
					
					ENDIF
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 2

			PROGRESS_STAGE( STAGE_RACE )
			
		BREAK
	
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Does the logic of the race stage.
PROC DO_STAGE_RACE()
					
	INT i
	
	SWITCH i_StageProgress
	
		CASE 0
		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(INT_REPLAY_STAGE_RACE, STAGE_RACE_NAME, FALSE, TRUE)
			CPRINTLN(DEBUG_MISSION, "Stage Do Race.")
			SAFE_FADE_SCREEN_IN_FROM_BLACK_SUPPRESS_TRAFFIC()
			REQUEST_SCRIPT_AUDIO_BANK("HUD_321_GO")
			CLEAR_BITMASK_AS_ENUM(s_RaceData.sCountDown.iBitFlags, CNTDWN_UI_Played_Go)
			CLEAR_BITMASK_AS_ENUM(s_RaceData.sCountDown.iBitFlags, CNTDWN_UI_Played_1)
			CLEAR_BITMASK_AS_ENUM(s_RaceData.sCountDown.iBitFlags, CNTDWN_UI_Played_2)
			CLEAR_BITMASK_AS_ENUM(s_RaceData.sCountDown.iBitFlags, CNTDWN_UI_Played_3)
			INIT_STOPPED_SOUND()
			s_BigMessage.siMovie = REQUEST_MG_BIG_MESSAGE()
			MG_INIT_FAIL_SPLASH_SCREEN(s_FailSplash)
			IF g_savedGlobals.sCountryRaceData.iSlipStreamHelpCount < 2
				PRINT_HELP("FM_IHELP_SLP") // You can slipstream by driving close behind another racer. This will give you a speed boost.
				g_savedGlobals.sCountryRaceData.iSlipStreamHelpCount++
			ENDIF
			
			i_StageProgress++
			
		BREAK
		
		CASE 1
			
			DRAW_SCALEFORM_MOVIE( s_RaceData.sCountDown.uiCountdown, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 100 )
			
			IF UPDATE_MINIGAME_COUNTDOWN_UI( s_RaceData.sCountDown, TRUE, FALSE, TRUE, 3, TRUE)
				
				STOP_STREAM()
				RELEASE_MINIGAME_COUNTDOWN_UI(s_RaceData.sCountDown)
				i_QuitTimer = GET_GAME_TIMER()
				i_StageProgress++
				
			ENDIF
			
			IF NOT s_RaceData.bRaceStarted
				IF IS_BITMASK_AS_ENUM_SET( s_RaceData.sCountDown.iBitFlags, CNTDWN_UI_Played_Go)
					s_RaceData.bRaceStarted = TRUE
					CPRINTLN(DEBUG_MISSION, "Camera cut on timer: ", TIMERA())
					IF IS_ENTITY_ALIVE(ve_PlayersCar)
						SET_FIRST_PERSON_FLASH_EFFECT_VEHICLE_MODEL_HASH(ENUM_TO_INT(GET_ENTITY_MODEL(ve_PlayersCar)))
					ENDIF
					SET_TODS_CUTSCENE_RUNNING( s_Timelapse, FALSE, TRUE, DEFAULT, TRUE, FALSE)
					DESTROY_ALL_CAMS()
					IF IS_ENTITY_ALIVE( GET_PLAYERS_LAST_VEHICLE() ) 
						SET_VEHICLE_HANDBRAKE( GET_PLAYERS_LAST_VEHICLE(), FALSE )
					ENDIF
					DISPLAY_HUD( TRUE )
					DISPLAY_RADAR( TRUE )
					CPRINTLN( DEBUG_MISSION, "Race Started!" )
					STOP_AUDIO_SCENE("RACE_INTRO_GENERIC")				
					SET_FRONTEND_RADIO_ACTIVE(TRUE)
					s_RaceData.iRaceStartTime = GET_GAME_TIMER()
					s_PlayerRacerData.iLapTimer = s_RaceData.iRaceStartTime
					START_AUDIO_SCENE("STREET_RACE_DURING_RACE")
					FOR i = ENUM_TO_INT(MPF_RACER1) TO ENUM_TO_INT(MPF_RACER8)
						s_MissionPed[i].bCanBeBlipped = TRUE
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(s_MissionPed[i].sVehicle.VehicleIndex,"STREET_RACE_NPC_GENERAL")
					ENDFOR
				ELSE
					IF TIMERA() > 2800 AND i_TimeLapseProgress = 5
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
							IF NOT IS_MISSION_BIT_FLAG_SET(MBF_FLASH1_TRIGGERED, i_MissionBitFlags)
								IF IS_ENTITY_ALIVE(ve_PlayersCar)
									SET_FIRST_PERSON_FLASH_EFFECT_VEHICLE_MODEL_HASH(ENUM_TO_INT(GET_ENTITY_MODEL(ve_PlayersCar)))
								ENDIF
								ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
								PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
								CPRINTLN(DEBUG_MISSION, "Doing push in flash.")
								SET_MISSION_BIT_FLAG(MBF_FLASH1_TRIGGERED, i_MissionBitFlags)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		BREAK
		
		CASE 2
		
			INT iNumAIFinished
			iNumAIFinished = 0
			FOR i = ENUM_TO_INT(MPF_RACER1) TO ENUM_TO_INT(MPF_RACER8)
				IF s_MissionPed[i].sRacerData.bFinished
					iNumAIFinished++
				ENDIF
			ENDFOR
			
			IF iNumAIFinished = MAX_RACERS - 1
				IF i_DNFTImer = 0
					CPRINTLN(DEBUG_MISSION, "All other racers finished, starting DNF timer. Player will fail in 30 seconds.")
					i_DNFTImer = GET_GAME_TIMER()
				ENDIF
			ENDIF
			
			IF s_PlayerRacerData.bFinished
				s_RaceData.bRaceOver = TRUE
				s_PlayerRacerData.iFinalPosition = s_PlayerRacerData.iPosition
				CLEANUP_RACE_CHECKPOINTS(s_PlayerRacerData)
				i_StageProgress++
			ENDIF
			
			IF (i_DNFTImer <> 0 AND GET_GAME_TIMER() > i_DNFTImer + 30000)	//Player has 30 seconds to finish after the last racer finishes.
				PROGRESS_STAGE(STAGE_FAIL)
				CPRINTLN(DEBUG_MISSION, "Player DNF, Progressing to STAGE_FAIL")
				st_FailString = "CRACEFAIL1"
			ENDIF
			
			IF IS_CHAR_ALMOST_STOPPED(PLAYER_PED_ID())
				IF i_StoppedTimer < 0
					i_StoppedTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2500, 5000)
				ELIF GET_GAME_TIMER() > i_StoppedTimer
					PLAY_SOUND_FROM_ENTITY(i_StoppedSound, "DISTANT_RACERS", s_MissionPed[GET_CLOSEST_RACER()].sVehicle.VehicleIndex, "ROAD_RACE_SOUNDSET")
					CPRINTLN(DEBUG_MISSION, "Playing sound - DISTANT_RACERS")
					i_StoppedTimer = -1
				ENDIF
			ELSE
				IF i_StoppedTimer > 0
					i_StoppedTimer = -1
				ENDIF
			ENDIF
			
			UPDATE_PLAYER_VEHICLE_RESPAWNING(s_PlayerRacerData)
			HANDLE_RACE_QUITTING()
			
		BREAK
		
		CASE 3
			STOP_AUDIO_SCENE("STREET_RACE_DURING_RACE")
			CPRINTLN(DEBUG_MISSION, "s_PlayerRacerData.bFinished = ", s_PlayerRacerData.bFinished,", Race Passed.")
			PROGRESS_STAGE( STAGE_COMPLETE )
		BREAK
	
	ENDSWITCH
	
ENDPROC


END_SCREEN_DATASET s_EndScreen
SIMPLE_USE_CONTEXT s_UseContext
BOOL bRetry


PROC DO_STAGE_COMPLETE()

	INT i
	
	SWITCH i_StageProgress
	
		CASE 0
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(INT_REPLAY_STAGE_COMPLETE, STAGE_COMPLETE_NAME, TRUE, TRUE)
			START_AUDIO_SCENE("STREET_RACE_OUTRO")
			i_StageProgress++
		BREAK
		
		// INITIAL FINISH LINE CAMERA - Slow ease back.
		CASE 1
			cam_Initial = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1954.7765, 3149.1460, 47.0701>>, <<4.9054, -0.0416, -158.4057>>, 52.4461)
			cam_Destination = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1954.7765, 3150.1460, 47.0701>>, <<4.9054, -0.0416, -158.4057>>, 52.4461)
			SET_CAM_ACTIVE(cam_Initial, TRUE)
			SET_CAM_ACTIVE_WITH_INTERP(cam_Destination, cam_Initial, 2000, GRAPH_TYPE_LINEAR)
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL
					ANIMPOSTFX_PLAY( "MinigameEndMichael", 0, FALSE )
				BREAK
				CASE CHAR_FRANKLIN
					ANIMPOSTFX_PLAY( "MinigameEndFranklin", 0, FALSE )
				BREAK
				CASE CHAR_TREVOR
					ANIMPOSTFX_PLAY( "MinigameEndTrevor", 0, FALSE )
				BREAK
			ENDSWITCH
			
			PLAY_SOUND_FRONTEND(-1,"Hit_In","PLAYER_SWITCH_CUSTOM_SOUNDSET")
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_CUTSCENE_RUNNING(TRUE)
			SET_CINEMATIC_MODE_ACTIVE( FALSE )
			SHAKE_CAM(cam_Destination,"HAND_SHAKE",0.3)
			TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING( PLAYER, GET_VEHICLE_PED_IS_IN( PLAYER ), s_TrackData.stTrackWaypoint, DRIVINGMODE_AVOIDCARS_RECKLESS, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE, -1, 30, TRUE )
			IF s_PlayerRacerData.iFinalPosition = 1
				REQUEST_MODEL(GET_TRIGGER_CAR_MODEL())
			ENDIF
			
			i_StageProgress++
		BREAK
		
		//WHIP CAMERA UP TO SKY
		CASE 2		
			IF DOES_CAM_EXIST(cam_Destination)
				IF NOT IS_CAM_INTERPOLATING(cam_Destination)
					DESTROY_CAM(cam_Initial)
					cam_Initial = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1954.7765, 3149.1460, 47.0701>>, <<89.5011, -0.0416, -158.4057>>, 30)
					SET_CAM_ACTIVE_WITH_INTERP(cam_Initial,cam_Destination,600,GRAPH_TYPE_SIN_ACCEL_DECEL)
					SET_CAM_MOTION_BLUR_STRENGTH(cam_Initial,1.0)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN( PLAYER ), 10, 1)
					SWITCH GET_CURRENT_PLAYER_PED_ENUM()
						CASE CHAR_MICHAEL
							ANIMPOSTFX_STOP( "MinigameEndMichael" )
						BREAK
						CASE CHAR_FRANKLIN
							ANIMPOSTFX_STOP( "MinigameEndFranklin" )
						BREAK
						CASE CHAR_TREVOR
							ANIMPOSTFX_STOP( "MinigameEndTrevor" )
						BREAK
					ENDSWITCH
					ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
					PLAY_SOUND_FRONTEND(-1,"Short_Transition_In","PLAYER_SWITCH_CUSTOM_SOUNDSET")
					DESTROY_CAM(cam_Destination)
					e_EndScreenState = END_SCREEN_INITIATE
					i_StageProgress++
				ENDIF
			ENDIF	
		BREAK
		
		CASE 3
			IF DOES_CAM_EXIST(cam_Initial)
				IF NOT IS_CAM_INTERPOLATING(cam_Initial)
					FOR i = Enum_TO_INT(MPF_RACER1) TO ENUM_TO_INT(MPF_RACER8)
						SAFE_REMOVE_BLIP(s_MissionPed[i].BlipIndex)
						REMOVE_PED(s_MissionPed[i].PedIndex, TRUE)
						REMOVE_VEHICLE(s_MissionPed[i].sVehicle.VehicleIndex, TRUE)
					ENDFOR
					SET_BUILDING_STATE(BUILDINGNAME_IPL_COUNTRY_RACETRACK, BUILDINGSTATE_NORMAL, FALSE)
					REPEAT COUNT_OF(s_AmbientPed) i
						REMOVE_PED( s_AmbientPed[i].PedIndex, TRUE )
					ENDREPEAT
				ENDIF
			ENDIF
			IF DISPLAY_RACE_RESULTS(s_PlayerRacerData, s_EndScreen, s_UseContext, bRetry)
				IF !bRetry
					IF s_PlayerRacerData.iFinalPosition > 1
						i_StageProgress = 10
					ELSE
						ve_RewardCar = CREATE_VEHICLE(GET_TRIGGER_CAR_MODEL(), GET_TRIGGER_CAR_COORDS(), GET_TRIGGER_CAR_HEADING())
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_TRIGGER_CAR_MODEL())
						IF g_savedGlobals.sCountryRaceData.iCurrentRace = COUNTRY_RACE_5
							SET_VEHICLE_LIVERY(ve_RewardCar, 0)
							SET_VEHICLE_COLOURS(ve_RewardCar, 44, 83)
							SET_VEHICLE_EXTRA_COLOURS(ve_RewardCar, 111, 111)				
						ENDIF
						i_StageProgress ++
					ENDIF
				ELSE
					MENU_SKIP(INT_REPLAY_STAGE_RACE, FALSE)
				ENDIF
			ENDIF
		BREAK
		
		// WHIP CAMERA DOWN TO SHOT ON PRIZE CAR
		CASE 4
			DESTROY_ALL_CAMS()
			cam_Initial = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1971.3821, 3108.1348, 47.2013>>, <<89.4758, 0.0478, 53.1639>>, 53.374201)
			SET_CAM_ACTIVE(cam_Initial, TRUE)
			cam_Destination = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1971.3821, 3108.1348, 47.2013>>, <<0.4758, 0.0478, 53.1639>>, 53.374201)
			SET_CAM_ACTIVE_WITH_INTERP(cam_Destination,cam_Initial,600,GRAPH_TYPE_SIN_ACCEL_DECEL)
			ANIMPOSTFX_STOP("MinigameTransitionIn")
			PLAY_SOUND_FRONTEND(-1,"Short_Transition_Out","PLAYER_SWITCH_CUSTOM_SOUNDSET")
			SET_CAM_MOTION_BLUR_STRENGTH(cam_Initial,1.0)
			SHAKE_CAM(cam_Initial,"HAND_SHAKE",0.3)
			SET_CAM_MOTION_BLUR_STRENGTH(cam_Destination,1.0)
			SHAKE_CAM(cam_Destination,"HAND_SHAKE",0.3)
			SAFE_TELEPORT_ENTITY(ve_PlayersCar, <<1967.4711, 3124.5825, 46.0038>>, 186.2056, TRUE)
			TASK_VEHICLE_MISSION_COORS_TARGET(PLAYER_PED_ID(), ve_PlayersCar, <<1968.5, 3113.2, 45.9001>>, MISSION_GOTO, 2, DRIVINGMODE_AVOIDCARS, 1, -1)
			ANIMPOSTFX_STOP_ALL()
			ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
			CLEAR_AREA(GET_RACE_TRIGGER_LOCATION(), 100, TRUE)
			i_StageProgress ++	
		BREAK
		
		// PAN CAMERA ACROSS AS PLAYER DRIVES IN
		CASE 5
			IF DOES_CAM_EXIST(cam_Destination)
				IF NOT IS_CAM_INTERPOLATING(cam_Destination)
					DESTROY_CAM(cam_Initial)
					cam_Initial = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1971.6366, 3108.4292, 47.2010>>, <<0.1839, 0.0478, 40.5043>>, 40)
					SET_CAM_ACTIVE_WITH_INTERP(cam_Initial,cam_Destination, 5000, GRAPH_TYPE_SIN_ACCEL_DECEL)
					SHAKE_CAM(cam_Initial,"HAND_SHAKE")
					SETTIMERA(0)
					i_StageProgress ++	
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF TIMERA() > 4700
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
					PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
				ENDIF
				i_StageProgress ++
			ENDIF
		BREAK
		
		CASE 7
			IF DOES_CAM_EXIST(cam_Initial)
				IF NOT IS_CAM_INTERPOLATING(cam_Initial)
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON
					AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) <> CAM_VIEW_MODE_FIRST_PERSON
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(-69.3229)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.5361)
					ENDIF
					SET_CUTSCENE_RUNNING(FALSE)
					SETTIMERA(0)
					i_StageProgress ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			ANIMPOSTFX_STOP_ALL()
			TRACK_VEHICLE_FOR_IMPOUND(ve_RewardCar)
			SET_COUNTRY_RACE_PASSED()
			CPRINTLN(DEBUG_MISSION, "Race Passed")
			STOP_AUDIO_SCENE("STREET_RACE_OUTRO")
			MISSION_CLEANUP()
		BREAK
		
		// PLAYER FAILED SO WHIP CAMERA DOWN TO THEIR CAR.
		CASE 10
			DESTROY_ALL_CAMS()
			cam_Initial = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1953.3099, 3139.0049, 48.1762>>, <<89.5003, -0.0032, 52.2429>>, 25.374201)
			SET_CAM_ACTIVE(cam_Initial, TRUE)
			cam_Destination = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1953.3099, 3139.0049, 48.1762>>, <<-7.3963, -0.0058, 52.1179>>, 45)
			SET_CAM_ACTIVE_WITH_INTERP(cam_Destination,cam_Initial,800,GRAPH_TYPE_SIN_ACCEL_DECEL)
			ANIMPOSTFX_STOP("MinigameTransitionIn")
			PLAY_SOUND_FRONTEND(-1,"Short_Transition_Out","PLAYER_SWITCH_CUSTOM_SOUNDSET")
			SET_CAM_MOTION_BLUR_STRENGTH(cam_Initial,1.0)
			SHAKE_CAM(cam_Initial,"HAND_SHAKE",0.3)
			SET_CAM_MOTION_BLUR_STRENGTH(cam_Destination,1.0)
			SHAKE_CAM(cam_Destination,"HAND_SHAKE",0.3)
			SAFE_TELEPORT_ENTITY(ve_PlayersCar, <<1948.2572, 3142.9358, 45.8642>>, 51.7219, TRUE)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
			CLEAR_AREA(GET_RACE_TRIGGER_LOCATION(), 100, TRUE)
			SETTIMERA(0)
			i_StageProgress ++	
		BREAK
		
		CASE 11
			IF TIMERA() > 500
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					IF IS_ENTITY_ALIVE(ve_PlayersCar)
						SET_FIRST_PERSON_FLASH_EFFECT_VEHICLE_MODEL_HASH(ENUM_TO_INT(GET_ENTITY_MODEL(ve_PlayersCar)))
					ENDIF
					ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
					PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					CPRINTLN(DEBUG_MISSION, "Doing push in flash.")
				ENDIF
				i_StageProgress ++	
			ENDIF
		BREAK
		
		CASE 12
			IF TIMERA() > 790
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				IF IS_ENTITY_ALIVE(ve_PlayersCar)
					SET_FIRST_PERSON_FLASH_EFFECT_VEHICLE_MODEL_HASH(ENUM_TO_INT(GET_ENTITY_MODEL(ve_PlayersCar)))
				ENDIF
				SET_CUTSCENE_RUNNING(FALSE, FALSE, TRUE, DEFAULT)
				SETTIMERA(0)
				i_StageProgress++
			ENDIF
		BREAK
		
		CASE 13
			IF TIMERA() > 2000
				ANIMPOSTFX_STOP_ALL()
				SET_COUNTRY_RACE_FAILED()
				CPRINTLN(DEBUG_MISSION, "Race Failed")
				STOP_AUDIO_SCENE("STREET_RACE_OUTRO")
				MISSION_CLEANUP()
			ENDIF
		BREAK
		
	
	ENDSWITCH

ENDPROC

PROC DO_FAIL()
	
//	IF NOT IS_MISSION_BIT_FLAG_SET(MBF_FAIL_CLEANUP, i_MissionBitFlags)
//		MISSION_CLEANUP(FALSE, FALSE, FALSE, TRUE)
//		SET_MISSION_BIT_FLAG(MBF_FAIL_CLEANUP, i_MissionBitFlags)
//	ENDIF
	
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_CIN_CAM )
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()

	SWITCH i_StageProgress
		
		CASE 0
			SET_CINEMATIC_BUTTON_ACTIVE( FALSE )
			i_StageProgress ++
		BREAK
		
		CASE 1
			IF MG_UPDATE_FAIL_SPLASH_SCREEN(s_BigMessage, s_FailSplash, "CRACEFAIL", st_FailString, b_ShouldRetry)
				IF b_ShouldRetry
					PLAY_SOUND_FRONTEND(-1,"YES","HUD_FRONTEND_DEFAULT_SOUNDSET")
					MENU_SKIP(INT_REPLAY_STAGE_RACE, FALSE)
				ELSE
					// Terminate script
					PLAY_SOUND_FRONTEND(-1,"NO","HUD_FRONTEND_DEFAULT_SOUNDSET")
					DO_SCREEN_FADE_OUT(500)
					SETTIMERA(0)
					i_StageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF TIMERA() > 1500
				MISSION_CLEANUP(TRUE, TRUE, FALSE)
			ENDIF
		BREAK
		
	ENDSWITCH
	

	
ENDPROC

SCRIPT

	SET_MISSION_FLAG(TRUE)

	IF HAS_FORCE_CLEANUP_OCCURRED()
		CPRINTLN(DEBUG_MISSION, "FORCE CLEANUP - Cleaning up races.")
		MISSION_CLEANUP()
	ENDIF

	MISSION_SETUP()
	
	INITIALISE_MISSION_VARIABLES()
	
	#IF IS_DEBUG_BUILD	DEBUG_SETUP()	#ENDIF
	
	WHILE TRUE

		IF e_CurrentMissionStage <> STAGE_FAIL MAIN_UPDATE() ENDIF

		SWITCH e_CurrentMissionStage
			
			CASE STAGE_SETUP
				DO_STAGE_SETUP() // Intro cutscene and setup.
			BREAK
			
			CASE STAGE_RACE
				DO_STAGE_RACE()	// Handle the race.
			BREAK
			
			CASE STAGE_COMPLETE
				DO_STAGE_COMPLETE() // Outro cutscene and cleanup.
			BREAK
			
			CASE STAGE_FAIL
				DO_FAIL()
			BREAK
			
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD	DEBUG_UPDATE()	#ENDIF
		
		wait(0)
	ENDWHILE
	
ENDSCRIPT