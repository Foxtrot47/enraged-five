USING "rage_builtins.sch"
USING "globals.sch"

USING "distributed_los.sch"
USING "rc_helper_functions.sch"
USING "minigames_helpers.sch"

CONST_INT MAX_CHECKPOINTS 6
CONST_INT FINISH_LINE  (MAX_CHECKPOINTS - 1)
CONST_INT MAX_RACERS 9

CONST_INT	COUNTRY_RACE_1		0
CONST_INT	COUNTRY_RACE_2		1
CONST_INT	COUNTRY_RACE_3		2
CONST_INT	COUNTRY_RACE_4		3
CONST_INT	COUNTRY_RACE_5		4

ENUM RACER_SKILL_LEVEL

	RSL_POOR,
	RSL_AVERAGE,
	RSL_PROFESSIONAL,
	RSL_RIVAL

ENDENUM

ENUM BIG_TEXT_STATE

	BTS_READY,
	BTS_LOAD,
	BTS_INIT,
	BTS_DISPLAY,
	BTS_CLEANUP

ENDENUM

ENUM MISSION_STAGE

	STAGE_SETUP,
	STAGE_RACE,
	STAGE_COMPLETE,
	STAGE_FAIL
ENDENUM

ENUM MISSION_BIT_FLAGS
	
	MBF_TIMELAPSE_INITIATED,
	MBF_FAIL_CLEANUP,
	MBF_FLASH1_TRIGGERED,
	MBF_EXAMPLE

ENDENUM

ENUM MISSION_OBJECT_FLAGS

	MOF_MAX_NUMBER_OF_OBJECTS

ENDENUM

ENUM MISSION_PED_FLAGS

	MPF_RACER1,
	MPF_RACER2,
	MPF_RACER3,
	MPF_RACER4,
	MPF_RACER5,
	MPF_RACER6,
	MPF_RACER7,
	MPF_RACER8
	
ENDENUM

ENUM AMBIENT_PED_FLAGS
	
	APF_STARTLINE_1,
	APF_STARTLINE_2,
	APF_STARTLINE_3,
	APF_STARTLINE_4,
	
	APF_CHECKPOINT0_1,
	APF_CHECKPOINT0_2,
	
	APF_STAND1_1,
	APF_STAND1_2,
	APF_STAND1_3,
	APF_STAND1_4,
	
	APF_HOTDOGSTAND,
	
	APF_STAND2_1,
	APF_STAND2_2,
	APF_STAND2_3,
	APF_STAND2_4,
	
	APF_CHECKPOINT2_1,
	APF_CHECKPOINT2_2,
	
	APF_CHECKPOINT3_1,
	APF_CHECKPOINT3_2,
	
	APF_FINISHLINE_1,
	APF_FINISHLINE_2,
	APF_FINISHLINE_3,
	APF_FINISHLINE_4
	
ENDENUM

ENUM MISSION_VEHICLE_FLAGS

	MVF_RACER1,
	MVF_RACER2,
	MVF_RACER3,
	MVF_RACER4,
	MVF_RACER5,
	MVF_RACER6,
	MVF_RACER7,
	MVF_RACER8

ENDENUM

STRUCT COUNTRY_RACE_DATA

	COUNTDOWN_UI sCountdown
	BOOL bRaceStarted
	BOOL bRaceOver
	INT iRaceStartTime
	
ENDSTRUCT
COUNTRY_RACE_DATA s_RaceData


STRUCT COUNTRY_TRACK_DATA
	VECTOR vCheckpoint[MAX_CHECKPOINTS]
	VECTOR vFinishLineMin
	VECTOR vFinishLineMax
	FLOAT fFinishLineWidth
	VECTOR vStartingGrid[MAX_RACERS]
	FLOAT  fStartingGrid[MAX_RACERS]
	STRING stTrackWaypoint
	INT	iNumLaps

ENDSTRUCT
COUNTRY_TRACK_DATA s_TrackData

STRUCT COUNTRY_RACER_DATA

	FLOAT 	fDistanceToNextCheckpoint
	FLOAT	fSpeed
	
	INT		iCurrentCheckpoint
	INT		iLap
	INT		iRaceProgress
	INT		iOldRaceProgress
	INT		iPosition
	INT		iFinalPosition
	INT		iBestLapTime
	INT		iCurrentLapTime
	INT		iLapTimer
	INT 	iRacerIndex
	INT 	iTotalTime
	
	BOOL	bFinished
	
	BLIP_INDEX blCurrentCheckpoint
	BLIP_INDEX blNextCheckpoint
	CHECKPOINT_INDEX cpCurrentCheckpoint
	CHECKPOINT_INDEX cpPreviousCheckpoint
	INT iPrevCheckpointAlpha
	
	RACER_SKILL_LEVEL 	eRacerSkillLevel

ENDSTRUCT

STRUCT BIG_TEXT

	BIG_TEXT_STATE	eBigTextState = BTS_READY
	INT iBigTextTimer
	SCALEFORM_INDEX sfBigText
	
ENDSTRUCT

STRUCT VEHICLE_STRUCT

	VEHICLE_INDEX 			VehicleIndex
	BLIP_INDEX				BlipIndex
	
	MODEL_NAMES 			eModel
	
	VECTOR					vPosition
	FLOAT					fHeading
	BOOL					bCreated
	BOOL					bDestroyed
	INT 					iCleanupTimer
	INT						iStuckTimer
	INT						iWarpTimer
	
ENDSTRUCT

STRUCT PED_STRUCT

	PED_INDEX			PedIndex
	BLIP_INDEX			BlipIndex
	SEQUENCE_INDEX		SequenceIndex

	MODEL_NAMES			eModel
	REL_GROUP_HASH		eRelGroupHash
	
	VEHICLE_STRUCT		sVehicle
	COUNTRY_RACER_DATA	sRacerData
	
	VECTOR				vPosition
	FLOAT				fHeading
	BOOL				bCreated
	BOOL				bKilled
	BOOL				bCanBeBlipped
	INT					iAIStage
	INT 				iTaskTimer
	
	TEXT_LABEL			Name
	
ENDSTRUCT

STRUCT OBJECT_STRUCT

	OBJECT_INDEX	ObjectIndex
	BLIP_INDEX		BlipIndex
	
	MODEL_NAMES		eModel
	
	VECTOR			vPosition
	VECTOR			vRotation
	
	BOOL			bCreated
	BOOL			bDestroyed

ENDSTRUCT

REL_GROUP_HASH	e_RelRacer

FUNC INT GET_RACE_TIMEOFDAY()
	
	SWITCH g_savedGlobals.sCountryRaceData.iCurrentRace
		
		CASE COUNTRY_RACE_1 		RETURN 	20	BREAK
		CASE COUNTRY_RACE_2 		RETURN	17	BREAK
		CASE COUNTRY_RACE_3 		RETURN	12	BREAK
		CASE COUNTRY_RACE_4 		RETURN	21	BREAK
		CASE COUNTRY_RACE_5			RETURN	12	BREAK
		
	ENDSWITCH
	
	RETURN 18
	
ENDFUNC

/// PURPOSE:
///    Gets the name of the race using the int
/// PARAMS:
///    iRace - The race index we want to get the name of.
/// RETURNS:
///    The name of the race.
FUNC STRING GET_RACE_NAME_FROM_INT(INT iRace)

	SWITCH iRace
		
		CASE COUNTRY_RACE_1		RETURN "COUNTRY_RACE_1"		BREAK
		CASE COUNTRY_RACE_2		RETURN "COUNTRY_RACE_2"		BREAK
		CASE COUNTRY_RACE_3		RETURN "COUNTRY_RACE_3"		BREAK
		CASE COUNTRY_RACE_4		RETURN "COUNTRY_RACE_4"		BREAK
		CASE COUNTRY_RACE_5		RETURN "COUNTRY_RACE_5"		BREAK

	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

/// PURPOSE:
///    Updates the 'number of completed races' stat 
/// PARAMS:
///    iPreviousRace - The race that was just completed
PROC UPDATE_RACE_COMPLETED_STAT(INT iPreviousRace)
	
	INT iCurrentValue = 0
	STAT_GET_INT(NUM_STOCK_RACES_COMPLETED, iCurrentValue)
	IF iCurrentValue < iPreviousRace + 1
		CPRINTLN(DEBUG_MISSION_STATS,"Increasing the Completed Stock Races stat value from ",iCurrentValue," to ", iPreviousRace +1)
		STAT_SET_INT(NUM_STOCK_RACES_COMPLETED, iPreviousRace +1)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Advances the race if successfully completed.
/// PARAMS:
///    eCurrentRace - The race that was just completed.
PROC ADVANCE_ACTIVE_RACE(INT iPreviousRace)
	
	UPDATE_RACE_COMPLETED_STAT(iPreviousRace)
	
	SWITCH iPreviousRace
		
		CASE COUNTRY_RACE_1		
			g_savedGlobals.sCountryRaceData.iCurrentRace = COUNTRY_RACE_2		
			g_savedGlobals.sCountryRaceData.bStallionUnlocked = TRUE
		BREAK
		CASE COUNTRY_RACE_2		
			g_savedGlobals.sCountryRaceData.iCurrentRace = COUNTRY_RACE_3	
			g_savedGlobals.sCountryRaceData.bGauntletUnlocked = TRUE
		BREAK
		CASE COUNTRY_RACE_3		
			g_savedGlobals.sCountryRaceData.iCurrentRace = COUNTRY_RACE_4
			g_savedGlobals.sCountryRaceData.bDominatorUnlocked = TRUE
		BREAK
		CASE COUNTRY_RACE_4		
			g_savedGlobals.sCountryRaceData.iCurrentRace = COUNTRY_RACE_5
			g_savedGlobals.sCountryRaceData.bBuffaloUnlocked = TRUE
		BREAK
		CASE COUNTRY_RACE_5
			g_savedGlobals.sCountryRaceData.iCurrentRace = COUNTRY_RACE_1
			g_savedGlobals.sCountryRaceData.bMarshallUnlocked = TRUE
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Gets the coords we want to create the trigger ped at.
/// RETURNS:
///    The coords we want.
FUNC VECTOR GET_TRIGGER_PED_COORDS()

	RETURN <<1965.2484, 3114.8860, 46.1663>>

ENDFUNC

/// PURPOSE:
///    Gets the coords we want to create the trigger car at.
/// RETURNS:
///    The coords we want.
FUNC VECTOR GET_TRIGGER_CAR_COORDS()

	RETURN <<1964.0383, 3114.6345, 46.1319>>

ENDFUNC

/// PURPOSE:
///    Gets the heading we want to create the trigger ped at.
/// RETURNS:
///    The heading we want.
FUNC FLOAT GET_TRIGGER_PED_HEADING()

	RETURN 294.2507

ENDFUNC

/// PURPOSE:
///    Gets the heading we want to create the trigger car at.
/// RETURNS:
///    The heading we want.
FUNC FLOAT GET_TRIGGER_CAR_HEADING()

	RETURN 194.8257

ENDFUNC

/// PURPOSE:
///    Gets the appropriate car model for the rivals/prize car.
/// RETURNS:
///    The prize car model.
FUNC MODEL_NAMES GET_TRIGGER_CAR_MODEL()

	SWITCH g_savedGlobals.sCountryRaceData.iCurrentRace
		
		CASE COUNTRY_RACE_1 		RETURN 	STALION2	BREAK
		CASE COUNTRY_RACE_2 		RETURN	GAUNTLET2	BREAK
		CASE COUNTRY_RACE_3 		RETURN	DOMINATOR2	BREAK
		CASE COUNTRY_RACE_4 		RETURN	BUFFALO3	BREAK
		CASE COUNTRY_RACE_5			RETURN	MARSHALL	BREAK
		
	ENDSWITCH
	
	RETURN BUFFALO3

ENDFUNC

/// PURPOSE:
///    Determines which trigger location to use.
/// RETURNS:
///    The location vector.
FUNC VECTOR GET_RACE_TRIGGER_LOCATION()

	RETURN <<1967.0420, 3116.0051, 45.8901>>
	
ENDFUNC

/// PURPOSE:
///    Sets the components of the trigger ped for each race.
/// PARAMS:
///    PedIndex - The ped whose components we want to set.
PROC SET_TRIGGER_PED_COMPONENTS(PED_INDEX PedIndex)

	SWITCH g_savedGlobals.sCountryRaceData.iCurrentRace
	
		CASE COUNTRY_RACE_1
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)	
		BREAK
		CASE COUNTRY_RACE_2
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 7, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,6), 0, 4, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 0, 1, 0) //(decl)
		BREAK
		CASE COUNTRY_RACE_3
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 5, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,6), 0, 1, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 0, 2, 0) //(decl)
		BREAK
		CASE COUNTRY_RACE_4
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,6), 0, 3, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
		BREAK
		CASE COUNTRY_RACE_5
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 6, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,6), 0, 3, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Initialises the data for the current race.
/// PARAMS:
///    sRaceData - The race data.
///    sTrackData - The track data we want to set.
PROC INITIALISE_COUNTRY_TRACK_DATA( COUNTRY_TRACK_DATA &sTrackData )
	
	CPRINTLN(DEBUG_MISSION, "Initialising race: ", GET_RACE_NAME_FROM_INT(g_savedGlobals.sCountryRaceData.iCurrentRace))

	sTrackData.stTrackWaypoint = "CountryTrack2Backwards"
	
	sTrackData.iNumLaps = 5

	sTrackData.vFinishLineMin = <<1952.026733,3120.290771,45.155823>>
	sTrackData.vFinishLineMax = <<1967.818848,3145.206543,50.280045>>
	sTrackData.fFinishLineWidth = 20.000000

	sTrackData.vCheckpoint[0] = <<1874.0, 3219.9, 45.4>>
	sTrackData.vCheckpoint[1] = <<2003.4, 3300.5, 45.7>>
	sTrackData.vCheckpoint[2] = <<2238.1, 3239.8, 48.1>>
	sTrackData.vCheckpoint[3] = <<2280.9, 3007.0, 46.0>>
	sTrackData.vCheckpoint[4] = <<2144.7, 3034.1, 45.4>>
	sTrackData.vCheckpoint[FINISH_LINE] = <<1968.9, 3127.1, 46.9>>
	
	//Row 1
	sTrackData.vStartingGrid[0] = <<1969.2513, 3122.5647, 46.0076>>
	sTrackData.fStartingGrid[0] = 53.7102
	sTrackData.vStartingGrid[1] = <<1971.0004, 3125.0637, 46.0360>>
	sTrackData.fStartingGrid[1] = 52.6994 
	sTrackData.vStartingGrid[2] = <<1972.9858, 3128.5405, 46.0069>>
	sTrackData.fStartingGrid[2] = 54.9611
	
	
	//Row 2
	sTrackData.vStartingGrid[3] = <<1975.5011, 3117.3798, 46.0271>>
	sTrackData.fStartingGrid[3] = 56.1329
	sTrackData.vStartingGrid[4] = <<1977.6095, 3120.3054, 46.0576>>
	sTrackData.fStartingGrid[4] = 52.8064
	sTrackData.vStartingGrid[5] = <<1979.8716, 3123.5132, 46.0285>>
	sTrackData.fStartingGrid[5] = 54.0832
	
	
	//Row 3
	sTrackData.vStartingGrid[6] = <<1981.7540, 3112.1300, 46.0491>>
	sTrackData.fStartingGrid[6] = 47.3411
	sTrackData.vStartingGrid[7] = <<1984.2336, 3115.1635, 46.0766>>
	sTrackData.fStartingGrid[7] = 58.6868
	sTrackData.vStartingGrid[8] = <<1986.3481, 3118.1844, 46.0478>>
	sTrackData.fStartingGrid[8] = 55.8988
	
ENDPROC

/// PURPOSE:
///    Applies modkits to a specified vehicle.
/// PARAMS:
///    veh - The vehicle we want to mod.
PROC SETUP_VEHICLE_MODS(VEHICLE_INDEX &veh)
	IF DOES_ENTITY_EXIST(veh)
	AND NOT IS_ENTITY_DEAD(veh)
		SET_VEHICLE_COLOUR_COMBINATION(veh, GET_RANDOM_INT_IN_RANGE(0, GET_NUMBER_OF_VEHICLE_COLOURS(veh)))
		IF GET_NUM_MOD_KITS(veh) > 0
			SET_VEHICLE_MOD_KIT(veh, 0)
			IF GET_NUM_VEHICLE_MODS(veh, MOD_SPOILER) > 0
				SET_VEHICLE_MOD(veh, MOD_SPOILER, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_SPOILER)))
			ENDIF
			IF GET_NUM_VEHICLE_MODS(veh, MOD_BUMPER_F) > 0
				SET_VEHICLE_MOD(veh, MOD_BUMPER_F, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_BUMPER_F)))
			ENDIF
			IF GET_NUM_VEHICLE_MODS(veh, MOD_BUMPER_R) > 0
				SET_VEHICLE_MOD(veh, MOD_BUMPER_R, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_BUMPER_R)))
			ENDIF
			IF GET_NUM_VEHICLE_MODS(veh, MOD_SKIRT) > 0
				SET_VEHICLE_MOD(veh, MOD_SKIRT, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_SKIRT)))
			ENDIF
			IF GET_NUM_VEHICLE_MODS(veh, MOD_EXHAUST) > 0
				SET_VEHICLE_MOD(veh, MOD_EXHAUST, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_EXHAUST)))
			ENDIF
			IF GET_NUM_VEHICLE_MODS(veh, MOD_CHASSIS) > 0
				SET_VEHICLE_MOD(veh, MOD_CHASSIS, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_CHASSIS)))
			ENDIF
			IF GET_NUM_VEHICLE_MODS(veh, MOD_GRILL) > 0
				SET_VEHICLE_MOD(veh, MOD_GRILL, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_GRILL)))
			ENDIF
			IF GET_NUM_VEHICLE_MODS(veh, MOD_WING_L) > 0
				SET_VEHICLE_MOD(veh, MOD_WING_L, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_WING_L)))
			ENDIF
			IF GET_NUM_VEHICLE_MODS(veh, MOD_WING_R) > 0
				SET_VEHICLE_MOD(veh, MOD_WING_R, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_WING_R)))
			ENDIF
			IF GET_NUM_VEHICLE_MODS(veh, MOD_ENGINE) > 0
				SET_VEHICLE_MOD(veh, MOD_ENGINE, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_ENGINE)))
			ENDIF
			IF GET_NUM_VEHICLE_MODS(veh, MOD_BRAKES) > 0
				SET_VEHICLE_MOD(veh, MOD_BRAKES, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_BRAKES)))
			ENDIF
			IF GET_NUM_VEHICLE_MODS(veh, MOD_GEARBOX) > 0
				SET_VEHICLE_MOD(veh, MOD_GEARBOX, GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(veh, MOD_GEARBOX)))
			ENDIF
			TOGGLE_VEHICLE_MOD(veh, MOD_TOGGLE_TURBO, TRUE)
			TOGGLE_VEHICLE_MOD(veh, MOD_TOGGLE_NITROUS, TRUE)
			TOGGLE_VEHICLE_MOD(veh, MOD_TOGGLE_XENON_LIGHTS, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Applies modkits to a specified racers vehicle.
/// PARAMS:
///    Racer - The racer whose vehicle want to mod.
PROC SETUP_VEHICLE_MOD_KIT(PED_STRUCT &Racer)
	
	INT iColourCombo	= -1
	INT iWindowTint		= -1
	INT iSpoiler		= -1
	INT iBumperFront	= -1
	INT iBumperRear		= -1
	INT iSkirt			= -1
	INT iExhaust		= -1
	INT iChassis		= -1
	INT iGrill			= -1
	INT iBonnet			= -1
	INT iWingLeft		= -1
	INT iWingRight		= -1
	INT iRoof			= -1
	INT iEngine			= 3
	INT iBrakes			= 2
	INT iGearbox		= 2
	INT iSuspension		= 3
	INT iWheel			= -1
	INT iColor1 = -1
	INT iColor2 = -1
	INT iColor3 = -1
	INT iColor4 = -1
	
//	iColourCombo	= -1
//	iWindowTint		= -1
//	iSpoiler		= -1
//	iBumperFront	= -1
//	iBumperRear		= -1
//	iSkirt			= -1
//	iExhaust		= -1
//	iChassis		= -1
//	iGrill			= -1
//	iBonnet			= -1
//	iWingLeft		= -1
//	iWingRight		= -1
//	iRoof			= -1
//	iEngine			= 3
//	iBrakes			= 2
//	iGearbox		= 2
//	iSuspension		= 3
//	iWheel			= -1
//	iColor1 		= -1
//	iColor2 		= -1
//	iColor3 		= -1
//	iColor4 		= -1	
	
		SWITCH g_savedGlobals.sCountryRaceData.iCurrentRace

		CASE COUNTRY_RACE_1
		
			SWITCH INT_TO_ENUM(MISSION_VEHICLE_FLAGS, Racer.sRacerData.iRacerIndex)
				
				CASE MVF_RACER1
					iColourCombo	= 6
					iWindowTint		= 1
					iSpoiler		= -1
					iBumperFront	= 0
					iBumperRear		= 1
					iSkirt			= -1
					iExhaust		= -1
					iChassis		= -1
					iGrill			= 0
					iBonnet			= 3
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= 0
					iEngine			= 3
					iBrakes			= 2
					iGearbox		= 2
					iSuspension		= 3
					iWheel			= 6
					iColor1 		= 0
					iColor2 		= 27
					iColor3 		= 0
					iColor4 		= 120
				BREAK
				CASE MVF_RACER3
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= -1
					iBumperFront	= 2
					iBumperRear		= 0
					iSkirt			= -1
					iExhaust		= 0
					iChassis		= 1
					iGrill			= 0
					iBonnet			= -1
					iWingLeft		= 0
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 3
					iBrakes			= 2
					iGearbox		= 2
					iSuspension		= -1
					iWheel			= 1
					iColor1 		= 24
					iColor2 		= 0
					iColor3 		= 0
					iColor4 		= 132
					SET_CONVERTIBLE_ROOF( Racer.sVehicle.VehicleIndex, FALSE )
				BREAK
				CASE MVF_RACER4
					iColourCombo	= 0
					iWindowTint		= 1
					iSpoiler		= -1
					iBumperFront	= 0
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= 0
					iChassis		= -1
					iGrill			= -1
					iBonnet			= 0
					iWingLeft		= 1
					iWingRight		= 0
					iRoof			= -1
					iEngine			= 2
					iBrakes			= 2
					iGearbox		= 2
					iSuspension		= -1
					iWheel			= 10
					iColor1 		= 147
					iColor2 		= 16
					iColor3 		= 20
					iColor4 		= 119
				BREAK
				CASE MVF_RACER5
					iColourCombo	= 0
					iWindowTint		= 0
					iSpoiler		= 1
					iBumperFront	= 0
					iBumperRear		= 0
					iSkirt			= -1
					iExhaust		= 0
					iChassis		= 0
					iGrill			= 0
					iBonnet			= 3
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 2
					iBrakes			= 1
					iGearbox		= 1
					iSuspension		= -1
					iWheel			= 8
					iColor1 		= 60
					iColor2 		= 113
					iColor3 		= 19
					iColor4 		= 113
				BREAK
				CASE MVF_RACER6
					iColourCombo	= 10
					iWindowTint		= 2
					iSpoiler		= 1
					iBumperFront	= -1
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= 0
					iChassis		= -1
					iGrill			= -1
					iBonnet			= 0
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= 0
					iEngine			= 2
					iBrakes			= 1
					iGearbox		= 1
					iSuspension		= 2
					iWheel			= 11
					iColor1 		= 138
					iColor2 		= 0
					iColor3 		= 1
					iColor4 		= 138
				BREAK
				CASE MVF_RACER7
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= 3
					iBumperFront	= 1
					iBumperRear		= 0
					iSkirt			= 1
					iExhaust		= 3
					iChassis		= 0
					iGrill			= 2
					iBonnet			= 1
					iWingLeft		= 1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 2
					iBrakes			= 0
					iGearbox		= 2
					iSuspension		= 1
					iWheel			= 23
					iColor1 		= 64
					iColor2 		= 13
					iColor3 		= 13
					iColor4 		= 67
				BREAK
				CASE MVF_RACER8
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= -1
					iBumperFront	= -1
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= 0
					iChassis		= 0
					iGrill			= 2
					iBonnet			= 2
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 2
					iBrakes			= 1
					iGearbox		= 2
					iSuspension		= 1
					iWheel			= 11
					iColor1 		= 39
					iColor2 		= 107
					iColor3 		= 0
					iColor4 		= 156
				BREAK

				
			ENDSWITCH
		
		BREAK
		
		CASE COUNTRY_RACE_2
			
			SWITCH INT_TO_ENUM(MISSION_VEHICLE_FLAGS, Racer.sRacerData.iRacerIndex )
				
				CASE MVF_RACER1
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= 0
					iBumperFront	= 0
					iBumperRear		= 0
					iSkirt			= 0
					iExhaust		= 0
					iChassis		= 0
					iGrill			= -1
					iBonnet			= 0
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= 2
					iEngine			= 1
					iBrakes			= 0
					iGearbox		= 0
					iSuspension		= -1
					iWheel			= 14
					iColor1 		= 11
					iColor2 		= 36
					iColor3 		= 0
					iColor4 		= 36	
				BREAK
				CASE MVF_RACER3
					iColourCombo	= 0
					iWindowTint		= 4
					iSpoiler		= 0
					iBumperFront	= 3
					iBumperRear		= 0
					iSkirt			= -1
					iExhaust		= 0
					iChassis		= 0
					iGrill			= 1
					iBonnet			= 1
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 2
					iBrakes			= 2
					iGearbox		= 0
					iSuspension		= 3
					iWheel			= 12
					iColor1 		= 24
					iColor2 		= 64
					iColor3 		= 0
					iColor4 		= 24
				BREAK
				CASE MVF_RACER4
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= 0
					iBumperFront	= 0
					iBumperRear		= 0
					iSkirt			= 0
					iExhaust		= 0
					iChassis		= -1
					iGrill			= -1
					iBonnet			= -1
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 3
					iBrakes			= 2
					iGearbox		= 0
					iSuspension		= 0
					iWheel			= 23
					iColor1 		= 120
					iColor2 		= 120
					iColor3 		= 120
					iColor4 		= 120
				BREAK
				CASE MVF_RACER5
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= 3
					iBumperFront	= -1
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= 3
					iChassis		= -1
					iGrill			= -1
					iBonnet			= 1
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 2
					iBrakes			= 1
					iGearbox		= 2
					iSuspension		= 2
					iWheel			= 3
					iColor1 		= 40
					iColor2 		= 42
					iColor3 		= 7
					iColor4 		= 156
				BREAK
				CASE MVF_RACER6
					iColourCombo	= 7
					iWindowTint		= 0
					iSpoiler		= -1
					iBumperFront	= 0
					iBumperRear		= 1
					iSkirt			= -1
					iExhaust		= 1
					iChassis		= 0
					iGrill			= -1
					iBonnet			= 0
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 1
					iBrakes			= 2
					iGearbox		= 0
					iSuspension		= -1
					iWheel			= 22
					iColor1 		= 62
					iColor2 		= 91
					iColor3 		= 6
					iColor4 		= 156
					SET_CONVERTIBLE_ROOF( Racer.sVehicle.VehicleIndex, TRUE )
				BREAK
				CASE MVF_RACER7
					iColourCombo	= 0
					iWindowTint		= 1
					iSpoiler		= 0
					iBumperFront	= 0
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= 1
					iChassis		= 1
					iGrill			= -1
					iBonnet			= 2
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= 0
					iEngine			= 2
					iBrakes			= 1
					iGearbox		= 0
					iSuspension		= 1
					iWheel			= 11
					iColor1 		= 50
					iColor2 		= 120
					iColor3 		= 134
					iColor4 		= 156
				BREAK
				CASE MVF_RACER8
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= 1
					iBumperFront	= 1
					iBumperRear		= -1
					iSkirt			= 0
					iExhaust		= 0
					iChassis		= -1
					iGrill			= 0
					iBonnet			= 1
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 3
					iBrakes			= 2
					iGearbox		= 0
					iSuspension		= 3
					iWheel			= 8
					iColor1 		= 36
					iColor2 		= 25
					iColor3 		= 6
					iColor4 		= 156
				BREAK
				
			ENDSWITCH
			
		BREAK
		
		CASE COUNTRY_RACE_3
		
			SWITCH INT_TO_ENUM(MISSION_VEHICLE_FLAGS, Racer.sRacerData.iRacerIndex)
				
				CASE MVF_RACER1
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= 0
					iBumperFront	= -1
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= 1
					iChassis		= -1
					iGrill			= -1
					iBonnet			= 0
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 3
					iBrakes			= 2
					iGearbox		= 0
					iSuspension		= 0
					iWheel			= 7
					iColor1 		= 64
					iColor2 		= 2
					iColor3 		= 6
					iColor4 		= 2
				BREAK
				CASE MVF_RACER3
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= 2
					iBumperFront	= 1
					iBumperRear		= 2
					iSkirt			= 0
					iExhaust		= 2
					iChassis		= 0
					iGrill			= 1
					iBonnet			= -1
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= 0
					iEngine			= 2
					iBrakes			= 2
					iGearbox		= 1
					iSuspension		= 3
					iWheel			= 5
					iColor1 		= 52
					iColor2 		= 5
					iColor3 		= 26
					iColor4 		= 106
				BREAK
				CASE MVF_RACER4
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= 0
					iBumperFront	= -1
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= -1
					iChassis		= 0
					iGrill			= -1
					iBonnet			= 0
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 2
					iBrakes			= 0
					iGearbox		= 0
					iSuspension		= 1
					iWheel			= 15
					iColor1 		= 72
					iColor2 		= 38
					iColor3 		= 46
					iColor4 		= 156
				BREAK
				CASE MVF_RACER5
					iColourCombo	= 0
					iWindowTint		= 1
					iSpoiler		= 1
					iBumperFront	= 1
					iBumperRear		= 0
					iSkirt			= 0
					iExhaust		= 0
					iChassis		= 0
					iGrill			= -1
					iBonnet			= 0
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= 0
					iEngine			= 0
					iBrakes			= 0
					iGearbox		= 0
					iSuspension		= 0
					iWheel			= 10
					iColor1 		= 62
					iColor2 		= 126
					iColor3 		= 15
					iColor4 		= 126
				BREAK
				CASE MVF_RACER6
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= 0
					iBumperFront	= 0
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= 1
					iChassis		= -1
					iGrill			= -1
					iBonnet			= 2
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= 0
					iEngine			= 1
					iBrakes			= 1
					iGearbox		= 0
					iSuspension		= 0
					iWheel			= 11
					iColor1 		= 29
					iColor2 		= 9
					iColor3 		= 6
					iColor4 		= 9
				BREAK
				CASE MVF_RACER7
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= 0
					iBumperFront	= -1
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= 0
					iChassis		= -1
					iGrill			= -1
					iBonnet			= 3
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 1
					iBrakes			= 0
					iGearbox		= 0
					iSuspension		= 0
					iWheel			= 15
					iColor1 		= 143
					iColor2 		= 8
					iColor3 		= 1
					iColor4 		= 143
				BREAK
				CASE MVF_RACER8
					iColourCombo	= 0
					iWindowTint		= -1
					iSpoiler		= 0
					iBumperFront	= 2
					iBumperRear		= 0
					iSkirt			= -1
					iExhaust		= 1
					iChassis		= 0
					iGrill			= -1
					iBonnet			= 0
					iWingLeft		= -1
					iWingRight		= -1
					iRoof			= -1
					iEngine			= 1
					iBrakes			= 0
					iGearbox		= 0
					iSuspension		= 3
					iWheel			= 4
					iColor1 		= 15
					iColor2 		= 120
					iColor3 		= 120
					iColor4 		= 120
				BREAK

				
			ENDSWITCH
		
		BREAK
		
		CASE COUNTRY_RACE_4
		
			SWITCH INT_TO_ENUM(MISSION_VEHICLE_FLAGS, Racer.sRacerData.iRacerIndex)
				
				CASE MVF_RACER1
					iColourCombo	= -1
					iSpoiler		= -1
					iBumperFront	= -1
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= -1
					iChassis		= -1
					iGrill			= -1
					iRoof			= -1
					iBonnet			= -1
					iEngine			= -1
					iBrakes			= -1
					iGearbox		= -1
					iSuspension		= -1
					iColor1 		= 140
					iColor2 		= 122
					iColor3 		= 58
					iColor4 		= 17
				BREAK
				CASE MVF_RACER3
					iColourCombo	= -1
					iSpoiler		= -1
					iBumperFront	= -1
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= -1
					iChassis		= -1
					iGrill			= -1
					iRoof			= -1
					iBonnet			= -1
					iEngine			= -1
					iBrakes			= -1
					iGearbox		= -1
					iSuspension		= -1
					iColor1 		= 149
					iColor2 		= 122
					iColor3 		= 114
					iColor4 		= 92
				BREAK
				CASE MVF_RACER4
					iColourCombo	= -1
					iSpoiler		= 1
					iBumperFront	= 0
					iBumperRear		= 0
					iSkirt			= 0
					iExhaust		= 0
					iChassis		= 0
					iGrill			= 0
					iRoof			= 2
					iBonnet			= 0
					iEngine			= -1
					iBrakes			= -1
					iGearbox		= -1
					iSuspension		= -1
					iColor1 		= 27
					iColor2 		= 134
					iColor3 		= 28
					iColor4 		= 126
				BREAK
				CASE MVF_RACER5
					iColourCombo	= -1
					iSpoiler		= 0
					iBumperFront	= 0
					iBumperRear		= 0
					iSkirt			= 0
					iExhaust		= 0
					iChassis		= -1
					iGrill			= -1
					iRoof			= 0
					iBonnet			= -1
					iEngine			= -1
					iBrakes			= -1
					iGearbox		= -1
					iSuspension		= -1
				BREAK
				CASE MVF_RACER6
					iColourCombo	= -1
					iSpoiler		= -1
					iBumperFront	= -1
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= -1
					iChassis		= -1
					iGrill			= -1
					iRoof			= -1
					iBonnet			= -1
					iEngine			= -1
					iBrakes			= -1
					iGearbox		= -1
					iSuspension		= -1
					iColor1 		= 88
					iColor2 		= 40
					iColor3 		= 14
					iColor4 		= 123
				BREAK
				CASE MVF_RACER7
					iColourCombo	= -1
					iSpoiler		= -1
					iBumperFront	= -1
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= -1
					iChassis		= -1
					iGrill			= -1
					iRoof			= -1
					iBonnet			= -1
					iEngine			= -1
					iBrakes			= -1
					iGearbox		= -1
					iSuspension		= -1
					iColor1 		= 55
					iColor2 		= 40
					iColor3 		= 14
					iColor4 		= 0
				BREAK
				CASE MVF_RACER8
					iColourCombo	= -1
					iSpoiler		= -1
					iBumperFront	= -1
					iBumperRear		= -1
					iSkirt			= -1
					iExhaust		= -1
					iChassis		= -1
					iGrill			= -1
					iRoof			= -1
					iBonnet			= -1
					iColor1 		= 38
					iColor2 		= 121
					iColor3 		= 123
					iColor4 		= 132
				BREAK
				
			ENDSWITCH
		
		BREAK
		
		CASE COUNTRY_RACE_5
		
			SWITCH INT_TO_ENUM(MISSION_VEHICLE_FLAGS, Racer.sRacerData.iRacerIndex)
				
				CASE MVF_RACER1
					iColor1 = 140
					iColor2 = 124
					iColor3 = 134
					iColor4 = 89
				BREAK
				
				CASE MVF_RACER2
					icolor1 = 135
					iColor2 = 134
					iColor3 = 134
					iColor4 = 121
				BREAK
				
				CASE MVF_RACER3
					
				BREAK
				
				CASE MVF_RACER4
					iColor1 = 0
					iColor2 = 134
					iColor3 = 134
					iColor4 = 0
				BREAK
				
				CASE MVF_RACER5
					iColor1 = 57
					iColor2 = 134
					iColor3 = 134
					iColor4 = 67
				BREAK
				
				CASE MVF_RACER6
					
				BREAK
				
				CASE MVF_RACER7
					iColor1 = 141
					iColor2 = 134
					iColor3 = 134
					iColor4 = 127
				BREAK
				
				CASE MVF_RACER8
					iColor1 = 39
					iColor2 = 95
					iColor3 = 124
					iColor4 = 88
				BREAK
				
			ENDSWITCH
			
		BREAK
		
	ENDSWITCH
	
	IF Racer.sRacerData.iRacerIndex = 1
		iSuspension		= 3
	ENDIF
	
	iEngine			= GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_ENGINE ) - 1
	iBrakes			= GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_BRAKES ) - 1
	iGearbox		= GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_GEARBOX ) - 1

	IF IS_ENTITY_ALIVE( Racer.sVehicle.VehicleIndex )
	
		IF GET_NUM_MOD_KITS( Racer.sVehicle.VehicleIndex ) > 0
		
			SET_VEHICLE_MOD_KIT( Racer.sVehicle.VehicleIndex, 0 )
						
			IF iColourCombo <> -1 
				IF iColourCombo < GET_NUMBER_OF_VEHICLE_COLOURS( Racer.sVehicle.VehicleIndex )
					SET_VEHICLE_COLOUR_COMBINATION(Racer.sVehicle.VehicleIndex, iColourCombo)
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. Colour Combination - ", iColourCombo )
				ENDIF
			ENDIF
			
			IF iColor1 <> -1 OR iColor2 <> -1
				SET_VEHICLE_COLOURS(Racer.sVehicle.VehicleIndex, iColor1, iColor2 )
			ENDIF
			
			IF iColor3 <> -1 OR iColor4 <> -1
				SET_VEHICLE_EXTRA_COLOURS(Racer.sVehicle.VehicleIndex, iColor3, iColor4 )
			ENDIF
			
			IF iWindowTint <> -1
				SET_VEHICLE_WINDOW_TINT( Racer.sVehicle.VehicleIndex, iWindowTint )			
			ENDIF
			
			IF iSpoiler <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_SPOILER) > iSpoiler
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_SPOILER, iSpoiler )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_SPOILER - ", iSpoiler )
				ENDIF			
			ENDIF
			
			IF iBumperFront <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_BUMPER_F) > iBumperFront
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_BUMPER_F, iBumperFront )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_BUMPER_F - ", iBumperFront )
				ENDIF
			ENDIF
			
			IF iBumperRear <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_BUMPER_R) > iBumperRear
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_BUMPER_R, iBumperRear )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_BUMPER_R - ", iBumperRear )
				ENDIF
			ENDIF
			
			IF iSkirt <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_SKIRT) > iSkirt
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_SKIRT, iSkirt )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_SKIRT - ", iSkirt )
				ENDIF
			ENDIF
			
			IF iExhaust <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_EXHAUST) > iExhaust
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_EXHAUST, iExhaust )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_EXHAUST - ", iExhaust )
				ENDIF
			ENDIF
			
			IF iChassis <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_CHASSIS) > iChassis
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_CHASSIS, iChassis )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_CHASSIS - ", iChassis )
				ENDIF
			ENDIF
			
			IF iGrill <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_GRILL) > iGrill
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_GRILL, iGrill )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_GRILL - ", iGrill )
				ENDIF
			ENDIF
			
			IF iBonnet <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_BONNET) > iBonnet
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_BONNET, iBonnet )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_BONNET - ", iBonnet )
				ENDIF
			ENDIF
			
			IF iWingLeft <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_WING_L) > iWingLeft
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_WING_L, iWingLeft )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_WING_L - ", iWingLeft )
				ENDIF				
			ENDIF
			
			IF iWingRight <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_WING_R) > iWingRight
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_WING_R, iWingRight )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_WING_R - ", iWingRight )
				ENDIF				
			ENDIF
			
			IF iRoof <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_ROOF) > iRoof
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_ROOF, iRoof )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_ROOF - ", iRoof )
				ENDIF
			ENDIF
			
			IF iEngine <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_ENGINE) > iEngine
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_ENGINE, iEngine )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_ENGINE - ", iEngine )
				ENDIF
			ENDIF
			
			IF iBrakes <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_BRAKES) > iBrakes
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_BRAKES, iBrakes )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_BRAKES - ", iBrakes )
				ENDIF
			ENDIF
			
			IF iGearbox <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_GEARBOX) > iGearbox
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_GEARBOX, iGearbox )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_GEARBOX - ", iGearbox )
				ENDIF
			ENDIF

			IF iSuspension <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_SUSPENSION) > iSuspension
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_SUSPENSION, iSuspension )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_SUSPENSION - ", iSuspension )
				ENDIF
			ENDIF
			
			IF iWheel <> -1
				IF GET_NUM_VEHICLE_MODS( Racer.sVehicle.VehicleIndex, MOD_WHEELS ) > iWheel
					SET_VEHICLE_MOD( Racer.sVehicle.VehicleIndex, MOD_WHEELS, iWheel )
				ELSE
					CASSERTLN(DEBUG_MISSION, Racer.Name, " has invalid mod. MOD_WHEELS - ", iWheel )
				ENDIF
			ENDIF
		
			TOGGLE_VEHICLE_MOD(Racer.sVehicle.VehicleIndex, MOD_TOGGLE_TURBO, TRUE)
			TOGGLE_VEHICLE_MOD(Racer.sVehicle.VehicleIndex, MOD_TOGGLE_NITROUS, TRUE)
			TOGGLE_VEHICLE_MOD(Racer.sVehicle.VehicleIndex, MOD_TOGGLE_XENON_LIGHTS, TRUE)
		
		ELSE
			
			CASSERTLN(DEBUG_MISSION, Racer.Name, " has no mod kit. " )
			
		ENDIF
		
		IF IS_VEHICLE_A_CONVERTIBLE( Racer.sVehicle.VehicleIndex )
			SET_CONVERTIBLE_ROOF( Racer.sVehicle.VehicleIndex, TRUE )
		ENDIF
	ENDIF
	
ENDPROC


PROC SAFE_FADE_SCREEN_OUT_TO_BLACK_SUPPRESS_TRAFFIC(INT iTime = 500)
	IF IS_SCREEN_FADED_IN()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(iTime)
			WHILE NOT IS_SCREEN_FADED_OUT()
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
				WAIT(0)
			ENDWHILE
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_FADE_SCREEN_IN_FROM_BLACK_SUPPRESS_TRAFFIC(INT iTime = 500)
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		IF NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(iTime)
		ENDIF
	ENDIF
	WHILE NOT IS_SCREEN_FADED_IN()
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
		WAIT(0)
	ENDWHILE
ENDPROC

/// PURPOSE:
///    Sets a mission bit flag.
/// PARAMS:
///    eFlag - The bit flat we want to set. 
PROC SET_MISSION_BIT_FLAG( MISSION_BIT_FLAGS eFlag, INT &iMissionBitFlags )
	SET_BIT( iMissionBitFlags, ENUM_TO_INT(eFlag) )
ENDPROC

/// PURPOSE:
///    Clears a mission bit flag.
/// PARAMS:
///    eFlag - The bit flag we want to clear.
PROC CLEAR_MISSION_BIT_FLAG( MISSION_BIT_FLAGS eFlag, INT &iMissionBitFlags )
	CLEAR_BIT( iMissionBitFlags, ENUM_TO_INT(eFlag) )
ENDPROC

/// PURPOSE:
///    Returns whether a bit flag has been set.
/// PARAMS:
///    eFlag - The bit flag we want to check.
/// RETURNS:
///    Whether eFlag has been set.
FUNC BOOL IS_MISSION_BIT_FLAG_SET( MISSION_BIT_FLAGS eFlag, INT &iMissionBitFlags )
	RETURN IS_BIT_SET( iMissionBitFlags, ENUM_TO_INT( eFlag ) )
ENDFUNC

/// PURPOSE:
///    Initialises a cutscene.
/// PARAMS:
///    bIsRunning - Set the cutscene to be running?
///    bKeepPlayerControl - Should the player retain control?
///    bDoGameCamInterp - Do we want to interp the camera?
///    iDurationFromInterp - Duration of camera interp.
///    bHideWeapon - Do we want to hide the player weapon?
PROC SET_CUTSCENE_RUNNING(BOOL bIsRunning, BOOL bKeepPlayerControl = FALSE, BOOL bDoGameCamInterp = FALSE, INT iDurationFromInterp = 2000, BOOL bRenderScriptCams = TRUE, BOOL bHideWeapon = FALSE)


	IF bRenderScriptCams
		SET_WIDESCREEN_BORDERS(bIsRunning,0)
		RENDER_SCRIPT_CAMS(bIsRunning, bDoGameCamInterp, iDurationFromInterp)
	ENDIF
	
	CLEAR_HELP(TRUE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(bIsRunning)
		
	DISABLE_CELLPHONE(bIsRunning)
	DISPLAY_HUD(NOT bIsRunning)
	DISPLAY_RADAR(NOT bIsRunning)
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF bHideWeapon
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), bIsRunning)
		ELSE
			IF NOT bIsRunning
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bKeepPlayerControl
		SET_PLAYER_CONTROL(PLAYER_ID(), NOT bIsRunning)
	ELSE
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sorts the triggered label array such that all the empty elements are at the end.
PROC REMOVE_LABEL_ARRAY_SPACES(INT &iTriggeredTextHashes[])
	INT i = 0
	REPEAT (COUNT_OF(iTriggeredTextHashes) - 1) i
	    IF iTriggeredTextHashes[i] = 0
			IF iTriggeredTextHashes[i+1] != 0
	        	iTriggeredTextHashes[i] = iTriggeredTextHashes[i+1]
	            iTriggeredTextHashes[i+1] = 0
	        ENDIF
	    ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Finds the array index of a particular triggered label, or -1 if the label hasn't been added.
/// PARAMS:
///    iLabelHash - The hash of the label.
/// RETURNS:
///    The labels index in the hash array.
FUNC INT GET_LABEL_INDEX(INT iLabelHash, INT &iTriggeredTextHashes[])
	INT i = 0
	REPEAT COUNT_OF(iTriggeredTextHashes) i
	    IF iTriggeredTextHashes[i] = 0 //We've reached the end of the filled section of the array, no need to continue.
	    	RETURN -1
	    ELIF iTriggeredTextHashes[i] = iLabelHash
	    	RETURN i
	    ENDIF
	ENDREPEAT

	RETURN -1
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the given label has been triggered.  
/// PARAMS:
///    strLabel - The string label we want to check.
/// RETURNS:
///    Whether the label has been triggered.
FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING strLabel, INT &iTriggeredTextHashes)
	RETURN (GET_LABEL_INDEX(GET_HASH_KEY(strLabel), iTriggeredTextHashes) != -1)
ENDFUNC

/// PURPOSE:
///    Adds/removes a text label to/from the list of labels that have been triggered.
/// PARAMS:
///    strLabel - String label we want to set.
///    bTrigger - Do we want to set label as triggered?
PROC SET_LABEL_AS_TRIGGERED(STRING strLabel, BOOL bTrigger, INT &iTriggeredTextHashes[])
	
	INT iHash = GET_HASH_KEY(strLabel)
	INT i = 0

	IF bTrigger
	    BOOL bAdded = FALSE
	    
	    WHILE NOT bAdded AND i < COUNT_OF(iTriggeredTextHashes)
			
			IF iTriggeredTextHashes[i] = iHash //The label is already in the array, don't add it again.
            	bAdded = TRUE 
         	ELIF iTriggeredTextHashes[i] = 0
            	iTriggeredTextHashes[i] = iHash
                bAdded = TRUE
          	ENDIF
          
         	 i++
	    ENDWHILE

	    #IF IS_DEBUG_BUILD
	    	IF NOT bAdded
		        SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Failed to add label, array is full.")
		  ENDIF
	    #ENDIF
	
	ELSE
	   
		INT iIndex = GET_LABEL_INDEX(iHash, iTriggeredTextHashes)
	    IF iIndex != -1
	          iTriggeredTextHashes[iIndex] = 0
	          REMOVE_LABEL_ARRAY_SPACES(iTriggeredTextHashes)
	    ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Clear all triggered text labels.
PROC CLEAR_TRIGGERED_LABELS(INT &iTriggeredTextHashes[])
	INT i = 0
	REPEAT COUNT_OF(iTriggeredTextHashes) i
		iTriggeredTextHashes[i] = 0
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Requests and loads text.
/// PARAMS:
///    text - Text we want to use for the mission.
///    slot - Slow we want to load text into.
///    bWaitForLoad - Do we want to wait for text to load?
PROC REQUEST_TEXT(STRING text, TEXT_BLOCK_SLOTS slot,  BOOL bWaitForLoad)
	REQUEST_ADDITIONAL_TEXT_FOR_DLC(text, slot)
	IF bWaitForLoad
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(slot)
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

/// PURPOSE:
///    Prints help with additional customization.
/// PARAMS:
///    strTextLabel - The help string we want to print.
///    bForever - Do we want to print forever?
///    bSound - Do we want to make a sound when displaying?
///    iOverrideTime - Do we want to specify a duration?
PROC PRINT_HELP_CUSTOM(STRING strTextLabel, BOOL bForever = TRUE, BOOL bSound = TRUE, INT iOverrideTime = -1)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(strTextLabel)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, bForever, bSound, iOverrideTime)
ENDPROC

/// PURPOSE:
///    Checks whether the current time is between two specified hours.
/// PARAMS:
///    iStartHour - The first hour we want to check between.
///    iEndHour - The second hour we want to check between.
/// RETURNS:
///    Whether or not the time is between two hours.
FUNC BOOL IS_TIME_BETWEEN_HOURS(INT iStartHour, INT iEndHour)
	INT iCurrentHour
	TIMEOFDAY time
	
	time = GET_CURRENT_TIMEOFDAY()
	iCurrentHour = GET_TIMEOFDAY_HOUR(time)
	
	IF iStartHour = 24
		iStartHour = 0
	ENDIF
	
	IF iEndHour = 24
		iEndHour = 0
	ENDIF
	
	IF iEndHour < iStartHour
	
		IF iCurrentHour < iEndHour OR iCurrentHour >= iStartHour
			RETURN TRUE
		ENDIF
	
	ELSE
		
		IF iCurrentHour >= iStartHour AND iCurrentHour < iEndHour
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Checks whether the current time of day is near a specified time.
/// PARAMS:
///    iDesiredHour - The hour we want to see if the current time is near.
///    iTimeEitherSide - The time either side of our desired our we want to check.
/// RETURNS:
///    Whether or not the time is near the desired our.
FUNC BOOL IS_TIME_CLOSE_TO_DESIRED_TIME(INT iDesiredHour, INT iTimeEitherSide)

	INT iTimeStart, iTimeEnd
	
	IF iDesiredHour < iTimeEitherSide
		iTimeStart = 24 - (iTimeEitherSide - iDesiredHour)
		iTimeEnd = iDesiredHour + iTimeEitherSide
	ELIF iDesiredHour + iTimeEitherSide > 24
		iTimeStart = iDesiredHour - iTimeEitherSide
		iTimeEnd = (iDesiredHour - 24) + iTimeEitherSide
	ELSE
		iTimeStart = iDesiredHour - iTimeEitherSide
		iTimeEnd = iDesiredHour + iTimeEitherSide 
	ENDIF
	
	IF iTimeStart = 24
		iTimeStart = 0
	ENDIF
	
	IF iTimeEnd = 24
		iTimeEnd = 0
	ENDIF

	RETURN IS_TIME_BETWEEN_HOURS(iTimeStart, iTimeEnd)
	
ENDFUNC

/// PURPOSE:
///    Creates a mission vehicle.
/// PARAMS:
///    veh - The data for the vehicle we want to create.
///    bWithWait - Do we want to wait for the vehicle to be created?
PROC CREATE_MISSION_VEHICLE(VEHICLE_STRUCT &veh, BOOL bWithWait = FALSE, BOOL bAddMods = FALSE)
	
	IF NOT veh.bCreated
	
		REQUEST_MODEL( veh.eModel )
		
		IF bWithWait
			WHILE NOT HAS_MODEL_LOADED( veh.eModel )
				wait(0)
			ENDWHILE			
		ENDIF
		
		IF HAS_MODEL_LOADED( veh.eModel )
		
			veh.VehicleIndex = CREATE_VEHICLE( veh.eModel, veh.vPosition, veh.fHeading )
			veh.bCreated = TRUE
			IF bAddMods
				SETUP_VEHICLE_MODS( veh.VehicleIndex )
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED( veh.eModel )
			
		ENDIF
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Creates a mission vehicle.
/// PARAMS:
///    obj - The data for the object we want to create. 
///    bWithWait - Do we want to wait for the object to be created?
PROC CREATE_MISSION_OBJECT(OBJECT_STRUCT &obj, BOOL bWithWait = FALSE)
	
	IF NOT obj.bCreated
	
		REQUEST_MODEL(obj.eModel)
		
		IF bWithWait
			WHILE NOT HAS_MODEL_LOADED(obj.eModel)
				wait(0)
			ENDWHILE			
		ENDIF
		
		IF HAS_MODEL_LOADED(obj.eModel)
		
			obj.ObjectIndex = CREATE_OBJECT(obj.eModel, obj.vPosition)
			
			SET_ENTITY_ROTATION(obj.ObjectIndex, obj.vRotation)

			obj.bCreated = TRUE
			
			SET_MODEL_AS_NO_LONGER_NEEDED(obj.eModel)
			
		ENDIF
	
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Creates a mission Ped.
/// PARAMS:
///    ped - The data for the ped we want to create.
///    bWait - Do we want to wait for the ped to be created?
PROC CREATE_MISSION_PED(PED_STRUCT &PedData, BOOL bWait, BOOL bDoVisibilityCheck = FALSE)

	IF NOT PedData.bCreated
	
		REQUEST_MODEL( PedData.eModel )
		IF bWait
			WHILE NOT HAS_MODEL_LOADED( PedData.eModel )
				wait(0)
			ENDWHILE
		ENDIF

		REQUEST_MODEL( PedData.sVehicle.eModel )
		IF bWait
			WHILE NOT HAS_MODEL_LOADED( PedData.sVehicle.eModel )
				wait(0)
			ENDWHILE
		ENDIF
		
		BOOL bModelsLoaded = FALSE
		
		IF  HAS_MODEL_LOADED( PedData.eModel )
		AND HAS_MODEL_LOADED( PedData.sVehicle.eModel )
			bModelsLoaded = TRUE
		ENDIF
		
		IF bModelsLoaded 
		AND ( ( bDoVisibilityCheck = FALSE ) OR ( bDoVisibilityCheck = TRUE AND NOT IS_SPHERE_VISIBLE( PedData.vPosition, 2 ) ) )
		
			
			PedData.sVehicle.vPosition = PedData.vPosition
			PedData.sVehicle.fHeading = PedData.fHeading
			PedData.sVehicle.VehicleIndex = CREATE_VEHICLE(PedData.sVehicle.eModel, PedData.vPosition, PedData.fHeading)
			SET_MODEL_AS_NO_LONGER_NEEDED( PedData.sVehicle.eModel )
			SET_VEHICLE_ENGINE_ON(PedData.sVehicle.VehicleIndex, TRUE, TRUE)
			PedData.sVehicle.bCreated = TRUE
			PedData.PedIndex = CREATE_PED_INSIDE_VEHICLE(PedData.sVehicle.VehicleIndex, PEDTYPE_MISSION, PedData.eModel, VS_DRIVER)	
			SETUP_VEHICLE_MOD_KIT(PedData)
			SET_VEHICLE_HAS_UNBREAKABLE_LIGHTS(PedData.sVehicle.VehicleIndex, TRUE)
			IF DOES_ENTITY_EXIST(PedData.PedIndex)
			
				SET_PED_ACCURACY( PedData.PedIndex, 20 )
				
				SET_PED_ALERTNESS( PedData.PedIndex, AS_MUST_GO_TO_COMBAT )
				
				SET_PED_COMBAT_ABILITY( PedData.PedIndex, CAL_PROFESSIONAL )
				
				SET_PED_COMBAT_ATTRIBUTES( PedData.PedIndex, CA_CAN_FLANK, TRUE )
				SET_PED_COMBAT_ATTRIBUTES( PedData.PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE )
				SET_PED_COMBAT_ATTRIBUTES( PedData.PedIndex, CA_USE_VEHICLE_ATTACK, TRUE )	
				SET_PED_COMBAT_ATTRIBUTES( PedData.PedIndex, CA_DISABLE_PINNED_DOWN, TRUE )
				SET_PED_COMBAT_ATTRIBUTES( PedData.PedIndex, CA_CAN_INVESTIGATE, TRUE )
				SET_PED_COMBAT_ATTRIBUTES( PedData.PedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE )
				
				SET_PED_COMBAT_MOVEMENT( PedData.PedIndex, CM_DEFENSIVE )
				
				SET_PED_CONFIG_FLAG( PedData.PedIndex, PCF_DisableGoToWritheWhenInjured, TRUE )
				SET_PED_CONFIG_FLAG( PedData.PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, TRUE )
				SET_PED_CONFIG_FLAG( PedData.PedIndex, PCF_DisableHurt, TRUE )
				
				SET_PED_DIES_WHEN_INJURED( PedData.PedIndex, TRUE )
				
				SET_PED_KEEP_TASK( PedData.PedIndex, TRUE )
				
				SET_PED_NAME_DEBUG( PedData.PedIndex, PedData.Name ) 
				
				SET_PED_RELATIONSHIP_GROUP_HASH( PedData.PedIndex, PedData.eRelGroupHash )
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( PedData.PedIndex, TRUE )
				
				PedData.bKilled = FALSE
				PedData.bCreated = TRUE
				PedData.iAIStage = 0
				
				IF PedData.sRacerData.iRacerIndex = 1 // If it is the rival set components
					SET_TRIGGER_PED_COMPONENTS(PedData.PedIndex)
				ENDIF
				
				CPRINTLN( DEBUG_MISSION, "Ped ", PedData.Name, " created." )
				SET_MODEL_AS_NO_LONGER_NEEDED( PedData.eModel )
			
			ELSE
			
				CPRINTLN( DEBUG_MISSION, "Ped ", PedData.Name, " failed to be created." )
				
			ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Creates an ambient ped.
/// PARAMS:
///    PedData - The struct of the ped we want to create.
///    bWait - Whether we want to wait for creation.
PROC CREATE_AMBIENT_PED(PED_STRUCT &PedData, BOOL bWait = FALSE)
	
	IF NOT PedData.bCreated
	
		REQUEST_MODEL(PedData.eModel)
		
		IF bWait
			WHILE NOT HAS_MODEL_LOADED(PedData.eModel)
				wait(0)
			ENDWHILE	
		ENDIF
		
		IF HAS_MODEL_LOADED(PedData.eModel)
		
			PedData.PedIndex = CREATE_PED( PEDTYPE_MISSION, PedData.eModel, PedData.vPosition, PedData.fHeading)
			
			IF DOES_ENTITY_EXIST(PedData.PedIndex)

				SET_PED_NAME_DEBUG( PedData.PedIndex, PedData.Name )

				PedData.bCreated = TRUE
				PedData.iAIStage = 0
				
				CPRINTLN( DEBUG_MISSION, "Ambient Ped ", PedData.Name, " created." )
				SET_MODEL_AS_NO_LONGER_NEEDED( PedData.eModel )
			ELSE
			
				CPRINTLN( DEBUG_MISSION, "Ambient Ped ", PedData.Name, " failed to be created." )
				
			ENDIF
		
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets player out of a vehicle ready for removal.
PROC SET_PLAYER_OUT_OF_ANY_VEHICLE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VECTOR vCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<vCoords.x, vCoords.y, vCoords.z + 1>>)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Does help text and sets respective help to have been triggered.
/// PARAMS:
///    sHelpText - Help text string we want to display.
///    bSupercedeHelp - Do we want to supercede any help already displaying?
///    iOverrideTime - Do we want to specify a diplay duration?
/// RETURNS:
///    Whether the help was successfully displayed.
FUNC BOOL DO_MISSION_HELP_TEXT(STRING sHelpText, BOOL bSupercedeHelp = TRUE, INT iOverrideTime = -1)
	IF bSupercedeHelp OR NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		PRINT_HELP(sHelpText, iOverrideTime)
		SET_LABEL_AS_TRIGGERED(sHelpText, TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Does god text and sets respective text to have been triggered.
/// PARAMS:
///    sGodText - The string we want to display.
///    bSupercedeText - Do we want to supercede any text already displaying?
///    bSupercedeSpeech - Do we ant to supercede and dialogue already displaying?
///    iTime - How long do we want to display for?
/// RETURNS:
///    Whether the text was successfully printed.
FUNC BOOL DO_MISSION_GOD_TEXT(STRING sGodText, BOOL bSupercedeText = TRUE, BOOL bSupercedeSpeech = FALSE, INT iTime = DEFAULT_GOD_TEXT_TIME)
	IF bSupercedeText OR NOT IS_MESSAGE_BEING_DISPLAYED()
		IF bSupercedeSpeech 
		OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
		OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
			PRINT_NOW(sGodText, iTime, 1)
			SET_LABEL_AS_TRIGGERED(sGodText, TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Safely removes a vehicle.
/// PARAMS:
///    veh - The vehicle index that needs to be cleaned up.
///    bForceDelete - Do we want to delete the vehicle?
PROC REMOVE_VEHICLE(VEHICLE_INDEX &veh, BOOL bForceDelete = FALSE)

	IF DOES_ENTITY_EXIST(veh)
	
		IF NOT IS_ENTITY_DEAD(veh)
		
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(veh)
			ENDIF
			
			STOP_SYNCHRONIZED_ENTITY_ANIM(veh, NORMAL_BLEND_OUT, TRUE)		
			
			IF IS_ENTITY_ATTACHED(veh)
				DETACH_ENTITY(veh)
			ENDIF
			
		ENDIF

		IF bForceDelete
			DELETE_VEHICLE(veh)
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Safely removes an object.
/// PARAMS:
///    obj - The object index that needs to be cleaned up.
///    bForceDelete - Do we want to delete the object?
PROC REMOVE_OBJECT(OBJECT_INDEX &obj, BOOL bForceDelete = FALSE)

	IF DOES_ENTITY_EXIST(obj)
	
		IF IS_ENTITY_ATTACHED(obj)
			DETACH_ENTITY(obj)
		ENDIF

		IF bForceDelete
			DELETE_OBJECT(obj)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
		ENDIF
		
	ENDIF
	
ENDPROC
	
/// PURPOSE:
///    Safely removes a ped.
/// PARAMS:
///    ped - The ped index that needs to be cleaned up.
///    bForceDelete - Do we want to delete the ped?
PROC REMOVE_PED(PED_INDEX &ped, BOOL bForceDelete = FALSE)

	IF DOES_ENTITY_EXIST(ped)
		IF NOT IS_PED_INJURED(ped)
			
			IF NOT IS_PED_IN_ANY_VEHICLE(ped)
			AND NOT IS_PED_GETTING_INTO_A_VEHICLE(ped)
				IF IS_ENTITY_ATTACHED_TO_ANY_OBJECT(ped)
				OR IS_ENTITY_ATTACHED_TO_ANY_PED(ped)
				OR IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(ped)
					DETACH_ENTITY(ped)
				ENDIF
				
				FREEZE_ENTITY_POSITION(ped, FALSE)
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(ped)
				SET_ENTITY_COLLISION(ped, TRUE)
			ENDIF
			
			IF IS_PED_GROUP_MEMBER(ped, PLAYER_GROUP_ID())
				REMOVE_PED_FROM_GROUP(ped)
			ENDIF
			
			IF NOT bForceDelete
				SET_PED_KEEP_TASK(ped, TRUE)
			ENDIF
			
		ENDIF

		IF bForceDelete
			DELETE_PED(ped)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(ped)
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Initialises the variables of the race data.
/// PARAMS:
///    sRaceData - The race data we want to set.
PROC INITIALISE_RACE_DATA( COUNTRY_RACE_DATA &sRaceData )
	
	sRaceData.bRaceStarted = FALSE
	sRaceData.bRaceOver = FALSE
	sRaceData.iRaceStartTime = 0
	
ENDPROC

/// PURPOSE:
///    Initialises mission vehicles.
PROC INITIALISE_MISSION_VEHICLES( VEHICLE_STRUCT &sMissionVehicle[] )
	
	INT i
	
	REPEAT COUNT_OF(MISSION_VEHICLE_FLAGS) i
		
		sMissionVehicle[i].bCreated 				= FALSE
		sMissionVehicle[i].bDestroyed  				= FALSE
			
			SWITCH g_savedGlobals.sCountryRaceData.iCurrentRace
			
				CASE COUNTRY_RACE_1
				
					SWITCH INT_TO_ENUM(MISSION_VEHICLE_FLAGS, i)

						CASE MVF_RACER1	sMissionVehicle[i].eModel = BUCCANEER	BREAK
						CASE MVF_RACER2	sMissionVehicle[i].eModel = GET_TRIGGER_CAR_MODEL()	BREAK
						CASE MVF_RACER3	sMissionVehicle[i].eModel = MANANA		BREAK
						CASE MVF_RACER4	sMissionVehicle[i].eModel = TORNADO		BREAK
						CASE MVF_RACER5	sMissionVehicle[i].eModel = BLADE		BREAK
						CASE MVF_RACER6	sMissionVehicle[i].eModel = VIGERO		BREAK
						CASE MVF_RACER7	sMissionVehicle[i].eModel = WARRENER	BREAK
						CASE MVF_RACER8	sMissionVehicle[i].eModel = PEYOTE		BREAK
						
					ENDSWITCH
				
				BREAK
				
				CASE COUNTRY_RACE_2
					
					SWITCH INT_TO_ENUM(MISSION_VEHICLE_FLAGS, i)
						
						CASE MVF_RACER1	sMissionVehicle[i].eModel = DOMINATOR	BREAK
						CASE MVF_RACER2	sMissionVehicle[i].eModel = GET_TRIGGER_CAR_MODEL()	BREAK
						CASE MVF_RACER3	sMissionVehicle[i].eModel = SABREGT		BREAK
						CASE MVF_RACER4	sMissionVehicle[i].eModel = BUFFALO		BREAK
						CASE MVF_RACER5	sMissionVehicle[i].eModel = RUINER		BREAK
						CASE MVF_RACER6	sMissionVehicle[i].eModel = COQUETTE2	BREAK	
						CASE MVF_RACER7	sMissionVehicle[i].eModel = DUKES		BREAK
						CASE MVF_RACER8	sMissionVehicle[i].eModel = PHOENIX		BREAK
						
					ENDSWITCH
					
				BREAK

				CASE COUNTRY_RACE_3
				
					SWITCH INT_TO_ENUM(MISSION_VEHICLE_FLAGS, i)
						
						CASE MVF_RACER1	sMissionVehicle[i].eModel = PIGALLE		BREAK
						CASE MVF_RACER2	sMissionVehicle[i].eModel = GET_TRIGGER_CAR_MODEL()	BREAK
						CASE MVF_RACER3	sMissionVehicle[i].eModel = FUTO		BREAK
						CASE MVF_RACER4	sMissionVehicle[i].eModel = RAPIDGT		BREAK
						CASE MVF_RACER5	sMissionVehicle[i].eModel = SULTAN		BREAK
						CASE MVF_RACER6	sMissionVehicle[i].eModel = PHOENIX		BREAK
						CASE MVF_RACER7	sMissionVehicle[i].eModel = VIGERO		BREAK
						CASE MVF_RACER8	sMissionVehicle[i].eModel = SABREGT		BREAK
						
					ENDSWITCH
				
				BREAK
				
				CASE COUNTRY_RACE_4
				
					SWITCH INT_TO_ENUM(MISSION_VEHICLE_FLAGS, i)

						CASE MVF_RACER1	sMissionVehicle[i].eModel = GAUNTLET2		BREAK
						CASE MVF_RACER2	sMissionVehicle[i].eModel = GET_TRIGGER_CAR_MODEL()	BREAK
						CASE MVF_RACER3	sMissionVehicle[i].eModel = GAUNTLET2		BREAK
						CASE MVF_RACER4	sMissionVehicle[i].eModel = DOMINATOR2		BREAK
						CASE MVF_RACER5	sMissionVehicle[i].eModel = DOMINATOR2		BREAK
						CASE MVF_RACER6	sMissionVehicle[i].eModel = STALION2		BREAK
						CASE MVF_RACER7	sMissionVehicle[i].eModel = STALION2		BREAK
						CASE MVF_RACER8	sMissionVehicle[i].eModel = DOMINATOR2		BREAK
						
					ENDSWITCH
				
				BREAK
				
				CASE COUNTRY_RACE_5
				
					SWITCH INT_TO_ENUM(MISSION_VEHICLE_FLAGS, i)

						CASE MVF_RACER1	sMissionVehicle[i].eModel = GAUNTLET2		BREAK
						CASE MVF_RACER2	sMissionVehicle[i].eModel = DOMINATOR2		BREAK
						CASE MVF_RACER3	sMissionVehicle[i].eModel = STALION2		BREAK
						CASE MVF_RACER4	sMissionVehicle[i].eModel = BUFFALO3		BREAK
						CASE MVF_RACER5	sMissionVehicle[i].eModel = GAUNTLET2		BREAK
						CASE MVF_RACER6	sMissionVehicle[i].eModel = BUFFALO3		BREAK
						CASE MVF_RACER7	sMissionVehicle[i].eModel = DOMINATOR2		BREAK
						CASE MVF_RACER8	sMissionVehicle[i].eModel = BUFFALO3		BREAK
						
					ENDSWITCH
				
				BREAK
				
			ENDSWITCH

		
	ENDREPEAT
	
ENDPROC



/// PURPOSE:
///    Initialises the data of a racer.
/// PARAMS:
///    Racer - The racer whos data we want to initialise.
PROC INITIALISE_RACER_DATA( COUNTRY_RACER_DATA &sRacer )
	
	sRacer.fDistanceToNextCheckpoint = 0
	sRacer.fSpeed = 0
	sRacer.iCurrentCheckpoint = 0
	sRacer.iLap = 0
	sRacer.iRaceProgress = 0
	sRacer.iOldRaceProgress = -1
	sRacer.iPosition = 0
	sRacer.iBestLapTime = 0
	sRacer.iCurrentLapTime = 0
	sRacer.iLapTimer = 0
	sRacer.iTotalTime = 0
	sRacer.bFinished = FALSE
	
	
	INT r
	IF g_savedGlobals.sCountryRaceData.iCurrentRace <> COUNTRY_RACE_5
		r = GET_RANDOM_INT_IN_RANGE(0,3)
		IF 		r = 0 	sRacer.eRacerSkillLevel = RSL_AVERAGE
		ELIF 	r = 1  	sRacer.eRacerSkillLevel = RSL_POOR
		ELIF  	r = 2 	sRacer.eRacerSkillLevel = RSL_PROFESSIONAL
		ENDIF
	ELSE
		r = GET_RANDOM_INT_IN_RANGE(0,6)
		IF 		r = 0 					sRacer.eRacerSkillLevel = RSL_AVERAGE
		ELIF 	r = 1 OR r = 2  		sRacer.eRacerSkillLevel = RSL_POOR
		ELIF  	r = 3 OR r = 4 OR r = 5 sRacer.eRacerSkillLevel = RSL_PROFESSIONAL
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Initialises the ambient ped data.
/// PARAMS:
///    sAmbientPed - The array of ambient peds whose data we want to initialise.
PROC INITIALISE_AMBIENT_PEDS( PED_STRUCT &sAmbientPed[] )
	
	INT i, r
	
	REPEAT COUNT_OF(AMBIENT_PED_FLAGS) i
	
		sAmbientPed[i].bCreated 		= FALSE
		sAmbientPed[i].bKilled  		= FALSE
		sAmbientPed[i].iAIStage			= 0
		sAmbientPed[i].iTaskTimer		= 0
		sAmbientPed[i].bCanBeBlipped	= FALSE
		
		r = GET_RANDOM_INT_IN_RANGE(0,4)
		IF 		r = 0 	sAmbientPed[i].eModel = A_F_Y_Tourist_01
		ELIF 	r = 1 	sAmbientPed[i].eModel = A_F_Y_Tourist_02
		ELIF 	r = 2	sAmbientPed[i].eModel = A_M_M_Tourist_01
		ELIF	r = 3	sAmbientPed[i].eModel = A_M_M_Hillbilly_02
		ENDIF
		
		sAmbientPed[i].Name					= "Ambient Ped "
		sAmbientPed[i].Name += i
		
		SWITCH INT_TO_ENUM(AMBIENT_PED_FLAGS, i)
		
			CASE APF_STARTLINE_1 	sAmbientPed[i].vPosition = <<1977.8430, 3129.4118, 45.9792>> sAmbientPed[i].fHeading = 122.9084	BREAK
			CASE APF_STARTLINE_2	sAmbientPed[i].vPosition = <<1969.5947, 3115.7012, 45.8988>> sAmbientPed[i].fHeading = -32.152	BREAK
			CASE APF_STARTLINE_3	sAmbientPed[i].vPosition = <<1986.6224, 3123.3468, 46.0496>> sAmbientPed[i].fHeading = 143.293	BREAK
			CASE APF_STARTLINE_4	sAmbientPed[i].vPosition = <<1968.1232, 3117.7219, 45.9001>> sAmbientPed[i].fHeading = 182.6841	BREAK
			
			CASE APF_CHECKPOINT0_1	sAmbientPed[i].vPosition = <<1866.5016, 3220.5420, 44.2959>> sAmbientPed[i].fHeading = 283.4628	BREAK
			CASE APF_CHECKPOINT0_2	sAmbientPed[i].vPosition = <<1866.7335, 3226.2192, 44.2235>> sAmbientPed[i].fHeading = 253.3484	BREAK
			
			CASE APF_STAND1_1		sAmbientPed[i].vPosition = <<1932.0637, 3262.2092, 44.7989>> sAmbientPed[i].fHeading = 184.9414	BREAK
			CASE APF_STAND1_2		sAmbientPed[i].vPosition = <<1939.2964, 3269.1479, 45.2262>> sAmbientPed[i].fHeading = 196.2922	BREAK
			CASE APF_STAND1_3		sAmbientPed[i].vPosition = <<1936.0403, 3264.8064, 44.8089>> sAmbientPed[i].fHeading = 179.3959	BREAK
			CASE APF_STAND1_4		sAmbientPed[i].vPosition = <<1938.1400, 3266.3291, 44.8089>> sAmbientPed[i].fHeading = 208.0957	BREAK
			
			CASE APF_HOTDOGSTAND	sAmbientPed[i].vPosition = <<1969.9380, 3262.4102, 44.5866>> sAmbientPed[i].fHeading = 135.3289	BREAK
			
			CASE APF_STAND2_1		sAmbientPed[i].vPosition = <<2129.3357, 3274.5962, 45.0930>> sAmbientPed[i].fHeading = 158.6073	BREAK
			CASE APF_STAND2_2		sAmbientPed[i].vPosition = <<2131.7813, 3273.9558, 45.0930>> sAmbientPed[i].fHeading = 144.4747	BREAK
			CASE APF_STAND2_3		sAmbientPed[i].vPosition = <<2135.2849, 3273.0071, 45.1832>> sAmbientPed[i].fHeading = 164.2929	BREAK
			CASE APF_STAND2_4		sAmbientPed[i].vPosition = <<2135.5605, 3274.7410, 45.6104>> sAmbientPed[i].fHeading = 166.3804	BREAK
			
			CASE APF_CHECKPOINT2_1	sAmbientPed[i].vPosition = <<2243.9265, 3247.5959, 47.1535>> sAmbientPed[i].fHeading = 113.5077	BREAK
			CASE APF_CHECKPOINT2_2	sAmbientPed[i].vPosition = <<2242.9182, 3248.8616, 47.1472>> sAmbientPed[i].fHeading = 94.3813	BREAK
			
			CASE APF_CHECKPOINT3_1	sAmbientPed[i].vPosition = <<2276.5378, 2995.1807, 44.9018>> sAmbientPed[i].fHeading = 343.2186	BREAK
			CASE APF_CHECKPOINT3_2	sAmbientPed[i].vPosition = <<2272.3389, 2995.2170, 44.7997>> sAmbientPed[i].fHeading = 354.5168	BREAK
			
			CASE APF_FINISHLINE_1	sAmbientPed[i].vPosition = <<2008.7729, 3110.6416, 45.9644>> sAmbientPed[i].fHeading = 212.1216	BREAK
			CASE APF_FINISHLINE_2	sAmbientPed[i].vPosition = <<1993.8628, 3099.1978, 45.9635>> sAmbientPed[i].fHeading = 336.9819	BREAK
			CASE APF_FINISHLINE_3	sAmbientPed[i].vPosition = <<1991.9924, 3100.3875, 45.9819>> sAmbientPed[i].fHeading = 332.3810	BREAK
			CASE APF_FINISHLINE_4	sAmbientPed[i].vPosition = <<1987.6431, 3102.6697, 46.0614>> sAmbientPed[i].fHeading = 327.4696	BREAK
		
		ENDSWITCH
		
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Initialises mission peds.
PROC INITIALISE_MISSION_PEDS( PED_STRUCT &sMissionPed[], VEHICLE_STRUCT &sMissionVehicle[] )
	
	INT i, r
	
	REPEAT COUNT_OF(MISSION_PED_FLAGS) i
		
		sMissionPed[i].bCreated 		= FALSE
		sMissionPed[i].bKilled  		= FALSE
		sMissionPed[i].iAIStage			= 0
		sMissionPed[i].iTaskTimer		= 0
		sMissionPed[i].bCanBeBlipped	= TRUE
		sMissionPed[i].eRelGroupHash	= e_RelRacer
		
		r = GET_RANDOM_INT_IN_RANGE(0,2)
		IF 		r = 0 	sMissionPed[i].eModel = A_M_Y_MotoX_01 
		ELIF 	r = 1	sMissionPed[i].eModel = A_M_Y_MOTOX_02
		ENDIF

		r = GET_RANDOM_INT_IN_RANGE(0,6)
		IF 		r = 0 					sMissionPed[i].sRacerData.eRacerSkillLevel = RSL_AVERAGE
		ELIF 	r = 1 OR r = 2 			sMissionPed[i].sRacerData.eRacerSkillLevel = RSL_POOR
		ELIF  	r = 3 OR r = 4 OR r = 5 sMissionPed[i].sRacerData.eRacerSkillLevel = RSL_PROFESSIONAL
		ENDIF

		sMissionPed[i].Name					= "Racer "
		sMissionPed[i].Name += i
		sMissionPed[i].sVehicle				= sMissionVehicle[i]
		sMissionPed[i].bCanBeBlipped		= FALSE
		
		INITIALISE_RACER_DATA( sMissionPed[i].sRacerData )
		sMissionPed[i].sRacerData.iRacerIndex = i
		
		sMissionPed[i].vPosition = s_TrackData.vStartingGrid[i] 
		sMissionPed[i].fHeading = s_TrackData.fStartingGrid[i] 
		
		IF i = ENUM_TO_INT(MPF_RACER2)
			sMissionPed[i].sRacerData.eRacerSkillLevel = RSL_RIVAL
			sMissionPed[i].eModel = A_M_Y_MOTOX_02
		ENDIF
		
		CPRINTLN( DEBUG_MISSION, sMissionPed[i].Name, " Initialised. " )
		
	ENDREPEAT	
	
ENDPROC

/// PURPOSE:
///    Initialises mission objects.
PROC INITIALISE_MISSION_OBJECTS( OBJECT_STRUCT &sMissionObject[] )

	INT i
	
	REPEAT COUNT_OF( MISSION_OBJECT_FLAGS ) i
	
		sMissionObject[i].bCreated 	= FALSE
		sMissionObject[i].bDestroyed  	= FALSE

	ENDREPEAT

ENDPROC

/// PURPOSE:
///    Gets the base speed of a racer depending on their skill level.
/// PARAMS:
///    PedData - The ped whos speed we want to get.
/// RETURNS:
///    The base speed of the ped.
FUNC FLOAT GET_BASE_SPEED(PED_STRUCT PedData)
	
	FLOAT fSpeed
	
	SWITCH PedData.sRacerData.eRacerSkillLevel
		CASE RSL_POOR			fSpeed = 32.00	BREAK
		CASE RSL_AVERAGE		fSpeed = 36.00	BREAK
		CASE RSL_PROFESSIONAL	fSpeed = 40.00	BREAK
		CASE RSL_RIVAL			fSpeed = 44.00	BREAK
	ENDSWITCH	
	
	fSpeed += ( 3 * g_savedGlobals.sCountryRaceData.iCurrentRace )

	RETURN fSpeed
	
ENDFUNC

/// PURPOSE:
///    Checks if the racer has fucked up so needs to be warped back to the track.
/// PARAMS:
///    PedData - The ped who we are checking.
/// RETURNS:
///    Whether the ped is upside down; on it's side; in water; or 
FUNC BOOL DOES_RACER_NEED_RESCUING(PED_STRUCT &PedData)
	
	IF IS_ENTITY_UPSIDEDOWN(PedData.sVehicle.VehicleIndex)
	OR IS_VEHICLE_STUCK_TIMER_UP(PedData.sVehicle.VehicleIndex, VEH_STUCK_ON_ROOF, ROOF_TIME)
	OR IS_VEHICLE_STUCK_TIMER_UP(PedData.sVehicle.VehicleIndex, VEH_STUCK_ON_SIDE, SIDE_TIME)
		CPRINTLN(DEBUG_MISSION, PedData.Name, " is upsidedown so needs recovering.")
		RETURN TRUE
	ENDIF
		
	IF IS_ENTITY_IN_WATER(PedData.sVehicle.VehicleIndex)
		CPRINTLN(DEBUG_MISSION, PedData.Name, " is in water so needs recovering.")
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_STUCK_TIMER_UP(PedData.sVehicle.VehicleIndex, VEH_STUCK_HUNG_UP, ROOF_TIME)
	OR IS_VEHICLE_STUCK_TIMER_UP(PedData.sVehicle.VehicleIndex, VEH_STUCK_JAMMED, ROOF_TIME)
		CPRINTLN(DEBUG_MISSION, PedData.Name, " is jammed so needs recovering.")
		RETURN TRUE
	ENDIF
	
	IF PedData.sRacerData.iRaceProgress = PedData.sRacerData.iOldRaceProgress
		IF GET_GAME_TIMER() > PedData.sVehicle.iStuckTimer + 45000
			CPRINTLN(DEBUG_MISSION, PedData.Name, " has been on the same checkpoint for 45 seconds so probably needs recovering.")
			RETURN TRUE
		ENDIF
	ELSE	
		PedData.sVehicle.iStuckTimer = GET_GAME_TIMER()
		PedData.sRacerData.iOldRaceProgress = PedData.sRacerData.iRaceProgress
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

INT DISABLE_CAMERA_MOVEMENT_TIMER	//Fix for bug 2212866

/// PURPOSE:
///    Warps the racer back to the nearest waypoint of the track.
/// PARAMS:
///    PedData - The ped we want to warp.
PROC RESCUE_DRIVER(PED_STRUCT &PedData)
	INT iNearestWaypoint
	VECTOR vNearestWaypoint, vNextWaypoint
	
	WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(s_TrackData.stTrackWaypoint, GET_ENTITY_COORDS(PedData.sVehicle.VehicleIndex), iNearestWaypoint)
	WAYPOINT_RECORDING_GET_COORD(s_TrackData.stTrackWaypoint, iNearestWaypoint, vNearestWaypoint)
	WAYPOINT_RECORDING_GET_COORD(s_TrackData.stTrackWaypoint, iNearestWaypoint + 1, vNextWaypoint)

	IF NOT IS_SPHERE_VISIBLE(vNearestWaypoint, 3)
		IF NOT IS_ANY_VEHICLE_NEAR_POINT(vNearestWaypoint, 5)
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PedData.sVehicle.VehicleIndex, vNearestWaypoint) < 5

			SAFE_TELEPORT_ENTITY(PedData.sVehicle.VehicleIndex, vNearestWaypoint, GET_HEADING_BETWEEN_VECTORS_2D(vNearestWaypoint, vNextWaypoint))
			IF NOT IS_PED_IN_VEHICLE(PedData.PedIndex, PedData.sVehicle.VehicleIndex)
				SET_PED_INTO_VEHICLE(PedData.PedIndex, PedData.sVehicle.VehicleIndex)
			ENDIF
			
			DISABLE_CAMERA_MOVEMENT_TIMER = GET_GAME_TIMER() + 1000
			
		ENDIF
	ENDIF
ENDPROC

BOOL b_PlayerVehicleNeedsRecovering
INT i_StuckHelpTimer, i_PlayerWantsToRecoverVehicleTimer
ENUM RACES_RESPAWN_STATE
	RACES_RESPAWN_SET,
	RACES_RESPAWN_INIT,
	RACES_RESPAWN_CHECK,
	RACES_RESPAWN_ACTIVE,
	RACES_RESPAWN_WAIT
ENDENUM
RACES_RESPAWN_STATE e_RaceRespawnState

/// PURPOSE:
///    Allows the player to respawn his car back on the track at the last checkpoint
/// PARAMS:
///    sPlayerRacerData - The players racer data.
PROC UPDATE_PLAYER_VEHICLE_RESPAWNING(COUNTRY_RACER_DATA sPlayerRacerData)

	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OnlyExitVehicleOnButtonRelease, TRUE)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			INT iClosestWaypoint
			VECTOR vClosestWaypoint
			WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(s_TrackData.stTrackWaypoint, GET_ENTITY_COORDS(PLAYER_PED_ID()), iClosestWaypoint)
			WAYPOINT_RECORDING_GET_COORD(s_TrackData.stTrackWaypoint, iClosestWaypoint, vClosestWaypoint)
			IF NOT IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
				b_PlayerVehicleNeedsRecovering = TRUE
				e_RaceRespawnState = RACES_RESPAWN_ACTIVE
				CPRINTLN(DEBUG_MISSION, "Mission Race: Player vehicle is undriveable")
			ENDIF
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRACESTUCK")
				IF IS_ENTITY_IN_WATER(GET_PLAYERS_LAST_VEHICLE())
					PRINT_HELP_FOREVER("CRACESTUCK") // Press and hold ~INPUT_VEH_EXIT~ to return to the race.
					i_StuckHelpTimer = GET_GAME_TIMER() + 1000
				ENDIF
				IF IS_VEHICLE_STUCK_TIMER_UP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEH_STUCK_HUNG_UP, 15000)
				OR IS_VEHICLE_STUCK_TIMER_UP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEH_STUCK_JAMMED, 30000)
				OR IS_VEHICLE_STUCK_TIMER_UP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEH_STUCK_ON_ROOF, 10000)
				OR IS_VEHICLE_STUCK_TIMER_UP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEH_STUCK_ON_SIDE, 3000)
					PRINT_HELP_FOREVER("CRACESTUCK") // Press and hold ~INPUT_VEH_EXIT~ to return to the race.
					i_StuckHelpTimer = GET_GAME_TIMER() + 1000
					CPRINTLN(DEBUG_MISSION, "Mission Race: Player vehicle is stuck")
				ENDIF
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vClosestWaypoint) > 900
					PRINT_HELP_FOREVER("CRACESTUCK") // Press and hold ~INPUT_VEH_EXIT~ to return to the race.
					i_StuckHelpTimer = GET_GAME_TIMER() + 1000
					CPRINTLN(DEBUG_MISSION, "Mission Race: Player is more than 30m from a waypoint.")
				ENDIF
			ELIF GET_GAME_TIMER() > i_StuckHelpTimer
				IF NOT IS_ENTITY_IN_WATER(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				AND NOT IS_VEHICLE_STUCK_TIMER_UP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEH_STUCK_HUNG_UP, 15000)
				AND NOT IS_VEHICLE_STUCK_TIMER_UP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEH_STUCK_JAMMED, 30000)
				AND NOT IS_VEHICLE_STUCK_TIMER_UP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEH_STUCK_ON_ROOF, 10000)
				AND NOT IS_VEHICLE_STUCK_TIMER_UP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VEH_STUCK_ON_SIDE, 3000)
				AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vClosestWaypoint) < 400
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
	OR b_PlayerVehicleNeedsRecovering = TRUE
		INT i_waypoint_to_warp_to, i_waypoint_to_point_to
		VECTOR v_waypoint_to_warp_to, v_next_waypoint
		SWITCH e_RaceRespawnState
			CASE RACES_RESPAWN_SET
				i_PlayerWantsToRecoverVehicleTimer = GET_GAME_TIMER()
				e_RaceRespawnState = RACES_RESPAWN_INIT
			BREAK
			CASE RACES_RESPAWN_INIT
				IF (GET_GAME_TIMER() - i_PlayerWantsToRecoverVehicleTimer) > 500
					i_PlayerWantsToRecoverVehicleTimer = GET_GAME_TIMER() // Set again
					e_RaceRespawnState = RACES_RESPAWN_CHECK
				ENDIF
			BREAK
			CASE RACES_RESPAWN_CHECK
				DRAW_GENERIC_METER(GET_GAME_TIMER() - i_PlayerWantsToRecoverVehicleTimer, 1500, "RACES_RMETER", HUD_COLOUR_RED, 0, HUDORDER_EIGHTHBOTTOM, -1, -1, FALSE, TRUE)
				IF (GET_GAME_TIMER() - i_PlayerWantsToRecoverVehicleTimer) >= 1500
					CPRINTLN(DEBUG_MISSION, "Mission Race: Player has held triangle for long enough, so begin respawning at last checkpoint")
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					e_RaceRespawnState = RACES_RESPAWN_ACTIVE	
				ENDIF
			BREAK
			CASE RACES_RESPAWN_ACTIVE
				IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
						VECTOR vCheckpointToWarpTo
						INT iNumPoints
						IF sPlayerRacerData.iRaceProgress > 0
							vCheckpointToWarpTo = s_TrackData.vCheckPoint[sPlayerRacerData.iRaceProgress - 1]
						ELSE
							vCheckpointToWarpTo = s_TrackData.vCheckPoint[MAX_CHECKPOINTS - 1]
						ENDIF
						WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(s_TrackData.stTrackWaypoint, vCheckpointToWarpTo, i_waypoint_to_warp_to)
						i_waypoint_to_point_to = i_waypoint_to_warp_to + 1
						WAYPOINT_RECORDING_GET_NUM_POINTS(s_TrackData.stTrackWaypoint, iNumPoints)
						IF i_waypoint_to_point_to >= iNumPoints
							i_waypoint_to_point_to = 0
						ENDIF
						WAYPOINT_RECORDING_GET_COORD(s_TrackData.stTrackWaypoint, i_waypoint_to_warp_to, v_waypoint_to_warp_to)
						WAYPOINT_RECORDING_GET_COORD(s_TrackData.stTrackWaypoint, i_waypoint_to_point_to, v_next_waypoint)
					//ENDIF
					IF NOT IS_ANY_VEHICLE_NEAR_POINT(v_waypoint_to_warp_to, 5) // Make sure the new checkpoint we're warping to is clear
					OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(GET_PLAYERS_LAST_VEHICLE(), v_waypoint_to_warp_to) < 5 // In case it's this vehicle within the 5 metres
						SAFE_FADE_SCREEN_OUT_TO_BLACK_SUPPRESS_TRAFFIC()
						SAFE_TELEPORT_ENTITY(GET_PLAYERS_LAST_VEHICLE(), v_waypoint_to_warp_to, GET_HEADING_BETWEEN_VECTORS_2D(v_waypoint_to_warp_to, v_next_waypoint))
						SET_VEHICLE_FIXED(GET_PLAYERS_LAST_VEHICLE())
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
						ENDIF
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRACESTUCK")
							CLEAR_HELP()
						ENDIF
						CPRINTLN(DEBUG_MISSION, "Mission Race: Successfully repositioned player's vehicle")
						SAFE_FADE_SCREEN_IN_FROM_BLACK_SUPPRESS_TRAFFIC()
						e_RaceRespawnState = RACES_RESPAWN_WAIT
					ENDIF
				ENDIF
			BREAK
			CASE RACES_RESPAWN_WAIT
				IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
				AND NOT IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
					CPRINTLN(DEBUG_MISSION, "Mission Race: Player released triangle")
					ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					i_PlayerWantsToRecoverVehicleTimer = -1
					b_PlayerVehicleNeedsRecovering = FALSE
					e_RaceRespawnState = RACES_RESPAWN_SET
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		i_PlayerWantsToRecoverVehicleTimer = -1
		b_PlayerVehicleNeedsRecovering = FALSE
		e_RaceRespawnState = RACES_RESPAWN_SET
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a driver needs a warp to catch up to the player.
/// PARAMS:
///    PedData - The ped who we need to check.
///    sPlayerRacerData - The players race data.
/// RETURNS:
///    Whether the driver needs a warp.
FUNC BOOL DOES_DRIVER_NEED_WARPING(PED_STRUCT &PedData, COUNTRY_RACER_DATA sPlayerRacerData)

	IF PedData.sRacerData.iCurrentCheckpoint < sPlayerRacerData.iCurrentCheckpoint - 1
	AND (PedData.sRacerData.iLap <> s_TrackData.iNumLaps - 1 OR (PedData.sRacerData.iLap = s_TrackData.iNumLaps - 1 AND sPlayerRacerData.iRaceProgress < 4))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Gets the correct heading at each checkpoint the vehicles can warp to.
/// PARAMS:
///    iCheckpointIndex - The checkpoint we want to get the heading for.
/// RETURNS:
///    The heading we want.
FUNC FLOAT GET_CHECKPOINT_WARP_HEADING(INT iCheckpointIndex)
	
	SWITCH iCheckPointIndex
	
		CASE 0	RETURN 51.1		BREAK	
		CASE 1	RETURN -61.8 	BREAK	
		CASE 2	RETURN -115.6	BREAK			
		CASE 3	RETURN -169.6	BREAK		
		CASE 4	RETURN 76.7		BREAK		
		CASE 5	RETURN 69.6		BREAK				
		
	ENDSWITCH
	
	RETURN 0.0
	
ENDFUNC

/// PURPOSE:
///    Warps the racer to the checkpoint behind the player if they get too far away.
/// PARAMS:
///    PedData - The drivers data.
///    sPlayerRacerData - The players racer data.
///    fSpeed - The speed we want the driver to drive at after warp.
PROC WARP_DRIVER(PED_STRUCT &PedData, COUNTRY_RACER_DATA sPlayerRacerData)

	VECTOR vTargetVector
	
	IF sPlayerRacerData.iRaceProgress = 0	//Player is on way to first checkpoint
		vTargetVector = s_TrackData.vCheckpoint[FINISH_LINE - 1]
	ELIF sPlayerRacerData.iRaceProgress = 1
		vTargetVector = s_TrackData.vCheckpoint[FINISH_LINE]
	ELSE
		vTargetVector = s_TrackData.vCheckpoint[sPlayerRacerData.iRaceProgress - 2] //Warp to previous checkpoint.
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vTargetVector)
		IF NOT IS_SPHERE_VISIBLE(vTargetVector, 3)
		AND NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(PedData.sVehicle.VehicleIndex), 3)
			IF GET_GAME_TIMER() > PedData.sVehicle.iWarpTimer + 2000 AND PedData.sVehicle.iWarpTimer <> 0
				IF NOT IS_ANY_VEHICLE_NEAR_POINT(vTargetVector, 4)//NOT IS_AREA_OCCUPIED(vTargetVector - <<1.5,1.5,1.5>>, vTargetVector + <<1.5,1.5,1.5>>, FALSE, TRUE, FALSE, FALSE, FALSE)
					IF sPlayerRacerData.iRaceProgress <> 0
						PedData.sRacerData.iLap = sPlayerRacerData.iLap
						PedData.sRacerData.iRaceProgress = sPlayerRacerData.iRaceProgress - 1
					ELSE
						PedData.sRacerData.iLap = sPlayerRacerData.iLap - 1
						PedData.sRacerData.iRaceProgress = FINISH_LINE
					ENDIF
					
					PedData.sRacerData.iCurrentCheckpoint = sPlayerRacerData.iCurrentCheckpoint - 1
					
					SAFE_TELEPORT_ENTITY( PedData.sVehicle.VehicleIndex, vTargetVector, GET_HEADING_BETWEEN_VECTORS_2D(vTargetVector, s_TrackData.vCheckpoint[PedData.sRacerData.iRaceProgress]), TRUE)
					SET_VEHICLE_FORWARD_SPEED( PedData.sVehicle.VehicleIndex, 20 )
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING( PedData.PedIndex, PedData.sVehicle.VehicleIndex, s_TrackData.stTrackWaypoint, DRIVINGMODE_AVOIDCARS,  0, EWAYPOINT_USE_TIGHTER_TURN_SETTINGS | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE, -1, PedData.sRacerData.fSpeed, TRUE )
					
					DISABLE_CAMERA_MOVEMENT_TIMER = GET_GAME_TIMER() + 1000
				ENDIF
			ENDIF
		ELSE
			PedData.sVehicle.iWarpTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF



ENDPROC

/// PURPOSE:
///    Checks whether the racer is in the area of a sharp corner.
/// PARAMS:
///    PedData - The struct of the racer we want to check.
/// RETURNS:
///    Wether or not the racer is near the corner.
FUNC BOOL IS_RACER_NEAR_SHARP_CORNER(PED_STRUCT PedData )

	IF	IS_ENTITY_IN_ANGLED_AREA( PedData.PedIndex, <<1863.932007,3234.403320,42.031586>>, <<1907.052490,3184.801514,50.189426>>, 25.000000)
	OR	IS_ENTITY_IN_ANGLED_AREA( PedData.PedIndex, <<2249.526367,3235.602783,45.011398>>, <<2191.436279,3247.168945,51.672554>>, 25.000000)
	OR 	IS_ENTITY_IN_ANGLED_AREA( PedData.PedIndex, <<2276.411621,2998.594482,42.964531>>, <<2299.795410,3059.602783,50.520164>>, 25.000000)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

CONST_INT AI_START_BOOST_DURATION 7500
CONST_INT AI_START_BOOST_DURATION_HIGH 1000

/// PURPOSE:
///    Checks if a vehicle is headed along the track waypoint correctly.
/// PARAMS:
///    VehicleIndex - The vehicle we want to check.
/// RETURNS:
///    Whether the vehicle is within a 20 degree arc of the waypoint heading and has able to boost. 
FUNC BOOL CAN_VEHICLE_BOOST(VEHICLE_INDEX VehicleIndex)
	
	IF IS_VEHICLE_OK( VehicleIndex ) 
	
		INT iClosestNode
		VECTOR vClosestNode
		INT iNumPoints
		VECTOR vNextNode
		BOOL bNodesGrabbed = FALSE
		BOOL bPointingCorrectDirection = FALSE
		
		IF WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT( s_TrackData.stTrackWaypoint, GET_ENTITY_COORDS( VehicleIndex ), iClosestNode )
			IF WAYPOINT_RECORDING_GET_COORD( s_TrackData.stTrackWaypoint, iClosestNode, vClosestNode )
				IF WAYPOINT_RECORDING_GET_NUM_POINTS( s_TrackData.stTrackWaypoint, iNumPoints )
					IF iClosestNode < iNumPoints
						bNodesGrabbed = WAYPOINT_RECORDING_GET_COORD( s_TrackData.stTrackWaypoint, iClosestNode + 1, vNextNode )
					ELSE
						bNodesGrabbed = WAYPOINT_RECORDING_GET_COORD( s_TrackData.stTrackWaypoint, 0, vNextNode )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bNodesGrabbed
			//If the vehicle is pointing approximately the same direction as the waypoint route.
			IF  GET_HEADING_BETWEEN_VECTORS_2D( vClosestNode, vNextNode ) - GET_ENTITY_HEADING( VehicleIndex ) < 10
			AND GET_HEADING_BETWEEN_VECTORS_2D( vClosestNode, vNextNode ) - GET_ENTITY_HEADING( VehicleIndex ) > -10
				bPointingCorrectDirection = TRUE
			ENDIF	
		ENDIF
		
		IF IS_VEHICLE_ON_ALL_WHEELS( VehicleIndex ) AND bPointingCorrectDirection AND NOT IS_ENTITY_IN_AIR( VehicleIndex ) AND VDIST2(GET_ENTITY_COORDS(VehicleIndex), vClosestNode) < 900
			RETURN TRUE
		ENDIF
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Handles racing AI of enemies. Wait for race start flag to be set. Once set they will begin their route around a specified track.
///    Will monitor number of laps completed.
/// PARAMS:
///    PedData - The ped data.
///    sPlayerRacerData - The players racer data.
///    bCanRevEngine - Will stop the AI racer from revving car engine before race starts.
PROC HANDLE_RACER(PED_STRUCT &PedData, COUNTRY_RACER_DATA sPlayerRacerData, BOOL bCanRevEngine = FALSE)

	IF DOES_ENTITY_EXIST( PedData.sVehicle.VehicleIndex ) AND NOT IS_ENTITY_DEAD( PedData.sVehicle.VehicleIndex )
		
		BOOL bCheatBoost = FALSE
		VECTOR vBoostForce
		FLOAT fNewSpeed = GET_BASE_SPEED(PedData)
		FLOAT fDistanceDifference
		
		IF sPlayerRacerData.iRaceProgress < MAX_CHECKPOINTS

			PedData.sRacerData.fDistanceToNextCheckpoint = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PedData.PedIndex, s_TrackData.vCheckpoint[PedData.sRacerData.iRaceProgress] )
					
			IF GET_GAME_TIMER() > s_RaceData.iRaceStartTime + AI_START_BOOST_DURATION
					
				// AI is more than one checkpoint ahead of the player.
				IF PedData.sRacerData.iCurrentCheckpoint > sPlayerRacerData.iCurrentCheckpoint	+ 1
					IF VDIST2(GET_ENTITY_COORDS(PedData.sVehicle.VehicleIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 2500
						fNewSpeed -= ( fNewSpeed*0.5 )	
					ELIF VDIST2(GET_ENTITY_COORDS(PedData.sVehicle.VehicleIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 625
						fNewSpeed -= ( fNewSpeed*0.4 )	
					ELSE
						fNewSpeed -= ( fNewSpeed*0.2 )	
					ENDIF
					
				// AI is one checkpoint ahead of the player.
				ELIF PedData.sRacerData.iCurrentCheckpoint = sPlayerRacerData.iCurrentCheckpoint + 1
					FLOAT fDistanceToLeadersCheckpoint = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(), s_TrackData.vCheckpoint[PedData.sRacerData.iRaceProgress] )
					fDistanceDifference = fDistanceToLeadersCheckpoint - PedData.sRacerData.fDistanceToNextCheckpoint
					// AI is over 50m ahead - Reduce speed a lot.
					IF fDistanceDifference > 50	
						fNewSpeed -= (fNewSpeed*0.40) 														
					// AI is over 25m ahead	- Reduce speed a bit.
					ELIF fDistanceDifference > 25	
						fNewSpeed -= (fNewSpeed*0.20) 
					ENDIF
					
				// AI and Player are on the same checkpoint.
				ELIF PedData.sRacerData.iCurrentCheckpoint = sPlayerRacerData.iCurrentCheckpoint
					// AI is closer to the checkpoint.
					IF sPlayerRacerData.fDistanceToNextCheckpoint > PedData.sRacerData.fDistanceToNextCheckpoint
						fDistanceDifference = sPlayerRacerData.fDistanceToNextCheckpoint - PedData.sRacerData.fDistanceToNextCheckpoint
						// AI is over 30m ahead - Reduce speed a lot.
						IF 	 fDistanceDifference > 50	
							fNewSpeed -= (fNewSpeed*0.40)														
						// AI is over 15m ahead - Reduce speed a bit.
						ELIF fDistanceDifference > 25	
							fNewSpeed -= (fNewSpeed*0.20) 
						ENDIF													
					// Player is closer to the checkpoint.
					ELIF sPlayerRacerData.fDistanceToNextCheckpoint < PedData.sRacerData.fDistanceToNextCheckpoint																											
						fDistanceDifference = PedData.sRacerData.fDistanceToNextCheckpoint - sPlayerRacerData.fDistanceToNextCheckpoint
						// Player is over 50m ahead - Increase speed a lot and apply large boost.
						IF 	 fDistanceDifference > 50	
							fNewSpeed += (fNewSpeed*1.50) 
							bCheatBoost = TRUE	
							vBoostForce = <<0.0, 3.0, 0.0>>
						// Player is over 25m ahead	- Increase speed a fair bit and apply small boost.
						ELIF fDistanceDifference > 25	
							fNewSpeed += (fNewSpeed*1.0) 
							bCheatBoost = TRUE	
							vBoostForce = <<0.0, 1.5, 0.0>>	
						// Player is ahead slightly	- Increase speed a bit.
						ELIF fDistanceDifference > 0							
							fNewSpeed += (fNewSpeed*0.50) 
						ENDIF													
					ENDIF
					
				// AI is one checkpoint behind the player.
				ELIF PedData.sRacerData.iCurrentCheckpoint = sPlayerRacerData.iCurrentCheckpoint - 1
					FLOAT fDistanceToPlayersCheckpoint = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PedData.PedIndex, s_TrackData.vCheckpoint[sPlayerRacerData.iRaceProgress] )
					fDistanceDifference = fDistanceToPlayersCheckpoint - sPlayerRacerData.fDistanceToNextCheckpoint
					// Player is over 50m ahead - Increase speed a lot.
					IF fDistanceDifference > 50	
						fNewSpeed += (fNewSpeed*1.50) 
						bCheatBoost = TRUE 
						vBoostForce = <<0.0, 3.0, 0.0>>	
					// Player is over 25m ahead	- Increase speed a fair bit.
					ELIF fDistanceDifference > 25	
						fNewSpeed += (fNewSpeed*1.00) 
						bCheatBoost = TRUE 
						vBoostForce = <<0.0, 1.5, 0.0>>	
					// Player is ahead slightly	- Increase speed a bit.
					ELIF fDistanceDifference > 0
						fNewSpeed += (fNewSpeed*0.50) 
					ENDIF													
				
				// AI is more than one checkpoint behind the player.
				ELSE																														
					fNewSpeed += (fNewSpeed*1.70)
					bCheatBoost = TRUE 
					vBoostForce = <<0.0, 4.0, 0.0>>
				ENDIF

			ELSE
			
				fNewSpeed = 40
				IF GET_GAME_TIMER() < s_RaceData.iRaceStartTime + AI_START_BOOST_DURATION_HIGH
					vBoostForce = <<0.0, 10.0, 0.0>>
				ELSE
					vBoostForce = <<0.0, 6.0, 0.0>>	
				ENDIF
				bCheatBoost = TRUE 
			ENDIF			
			
			PedData.sRacerData.fSpeed = INTERP_FLOAT(PedData.sRacerData.fSpeed, fNewSpeed, 0.1)
			
			SET_DRIVE_TASK_CRUISE_SPEED(PedData.PedIndex, PedData.sRacerData.fSpeed)
			
			SET_DRIVE_TASK_MAX_CRUISE_SPEED(PedData.PedIndex, fNewSpeed)
			
		ENDIF
		
		SWITCH PedData.iAIStage
			
			//Wait for race start.
			CASE 0
				
				IF IS_TIMER_STARTED( s_RaceData.sCountDown.CountdownTimer )
					
					PedData.iAIStage++
					
				ELSE
					
					IF bCanRevEngine
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(PedData.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
						
							OPEN_SEQUENCE_TASK( PedData.SequenceIndex )
								TASK_PAUSE( null, GET_RANDOM_INT_IN_RANGE(200, 500))
								TASK_VEHICLE_TEMP_ACTION( null, PedData.sVehicle.VehicleIndex, TEMPACT_REV_ENGINE, GET_RANDOM_INT_IN_RANGE(500, 2000) )
							CLOSE_SEQUENCE_TASK( PedData.SequenceIndex )
							TASK_PERFORM_SEQUENCE( PedData.PedIndex, PedData.SequenceIndex )
							CLEAR_SEQUENCE_TASK( PedData.SequenceIndex )
						
						ENDIF
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE 1

				SET_VEHICLE_IS_RACING( PedData.sVehicle.VehicleIndex, TRUE )
				SET_VEHICLE_DOORS_LOCKED( PedData.sVehicle.VehicleIndex, VEHICLELOCK_LOCKED )
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( PedData.PedIndex, TRUE )
				SET_VEHICLE_STRONG( PedData.sVehicle.VehicleIndex, TRUE )
				SET_VEHICLE_HAS_STRONG_AXLES( PedData.sVehicle.VehicleIndex, TRUE )
				SET_ENTITY_LOAD_COLLISION_FLAG( PedData.sVehicle.VehicleIndex, TRUE )
				SET_VEHICLE_ENGINE_ON( PedData.sVehicle.VehicleIndex, TRUE, FALSE )
				SET_VEHICLE_BLIP_THROTTLE_RANDOMLY(PedData.sVehicle.VehicleIndex, FALSE)

				OPEN_SEQUENCE_TASK( PedData.SequenceIndex )
					INT r
					r = GET_RANDOM_INT_IN_RANGE( 1100, 1300)
					TASK_PAUSE(null, r)
					r = GET_RANDOM_INT_IN_RANGE( 100, 250)
					IF PedData.sRacerData.iRacerIndex = 1
						TASK_VEHICLE_TEMP_ACTION( null, PedData.sVehicle.VehicleIndex, TEMPACT_BURNOUT, 600 )	// Rival takes off early
					ELIF PedData.sRacerData.iRacerIndex = 2
						TASK_VEHICLE_TEMP_ACTION( null, PedData.sVehicle.VehicleIndex, TEMPACT_BURNOUT, 1100 )	// Players column take off quicker than others
					ELIF PedData.sRacerData.iRacerIndex = 5
						TASK_VEHICLE_TEMP_ACTION( null, PedData.sVehicle.VehicleIndex, TEMPACT_BURNOUT, 1200 )	// Players column take off quicker than others
					ELSE
						TASK_VEHICLE_TEMP_ACTION( null, PedData.sVehicle.VehicleIndex, TEMPACT_BURNOUT, 1300 + ((PedData.sRacerData.iRacerIndex + 1) * r) )
					ENDIF
				CLOSE_SEQUENCE_TASK( PedData.SequenceIndex )
				TASK_PERFORM_SEQUENCE( PedData.PedIndex, PedData.SequenceIndex )
				CLEAR_SEQUENCE_TASK( PedData.SequenceIndex )
					
				PedData.sRacerData.fSpeed = GET_BASE_SPEED(PedData)
				
				PedData.iAIStage++

			BREAK
			
			CASE 2
			
				SWITCH PedData.sRacerData.iRaceProgress

					CASE 5 // This needs to be the index of the finish line. 
					
						IF IS_ENTITY_IN_ANGLED_AREA( PedData.PedIndex,  s_TrackData.vFinishLineMin, s_TrackData.vFinishLineMax, s_TrackData.fFinishLineWidth)
					
							PedData.sRacerData.iLap++
							PedData.sRacerData.iCurrentCheckpoint++
							PedData.sRacerData.iRaceProgress = 0
							
							IF PedData.sRacerData.iLap = s_TrackData.iNumLaps
								
								CPRINTLN(DEBUG_MISSION, PedData.Name, " Finished.")
								PedData.sRacerData.bFinished = TRUE
								
							ENDIF
							
						ENDIF
						
					BREAK
					
					DEFAULT
					
						IF s_RaceData.bRaceStarted

							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( PedData.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE )
								OPEN_SEQUENCE_TASK( PedData.SequenceIndex )
									TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING( NULL, PedData.sVehicle.VehicleIndex, s_TrackData.stTrackWaypoint, DRIVINGMODE_AVOIDCARS_RECKLESS,  0, EWAYPOINT_USE_TIGHTER_TURN_SETTINGS | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE, -1, PedData.sRacerData.fSpeed, TRUE )
								CLOSE_SEQUENCE_TASK( PedData.SequenceIndex )
								TASK_PERFORM_SEQUENCE( PedData.PedIndex, PedData.SequenceIndex )
								CLEAR_SEQUENCE_TASK( PedData.SequenceIndex )
							ENDIF
							
							BOOL bHitCheckpoint
							INT iClosestWaypoint
							VECTOR vClosestWaypoint
							bHitCheckpoint = FALSE
							IF PedData.sRacerData.fDistanceToNextCheckpoint < 20
								bHitCheckpoint = TRUE
							ENDIF
							WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(s_TrackData.stTrackWaypoint, GET_ENTITY_COORDS(PedData.sVehicle.VehicleIndex), iClosestWaypoint)
							WAYPOINT_RECORDING_GET_COORD(s_TrackData.stTrackWaypoint, iClosestWaypoint, vClosestWaypoint)
							IF VDIST2(vClosestWaypoint, s_TrackData.vCheckpoint[PedData.sRacerData.iRaceProgress]) < 400
								bHitCheckpoint = TRUE
							ENDIF
							IF bHitCheckpoint
								PedData.sRacerData.iCurrentCheckpoint++
								PedData.sRacerData.iRaceProgress++
							ENDIF
							
						ELSE
							
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( PedData.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE )
							AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( PedData.PedIndex, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD )
								TASK_VEHICLE_DRIVE_TO_COORD( PedData.PedIndex, PedData.sVehicle.VehicleIndex, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PedData.PedIndex, <<0,100,0>>), 40, DRIVINGSTYLE_STRAIGHTLINE, PedData.sVehicle.eModel, DRIVINGMODE_AVOIDCARS_RECKLESS, 20, 9999)
							ENDIF
						ENDIF
					
					BREAK
					
				ENDSWITCH
				
				IF DOES_RACER_NEED_RESCUING(PedData)
					IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(PedData.sVehicle.VehicleIndex), 3)
						RESCUE_DRIVER(PedData)
					ENDIF
				ENDIF	
				
				//Only warp the rival in the stock car.
				IF PedData.sRacerData.iRacerIndex = 1
					IF DOES_DRIVER_NEED_WARPING(PedData, sPlayerRacerData)
						WARP_DRIVER(PedData, sPlayerRacerData)
					ENDIF
				ENDIF
				
				IF DISABLE_CAMERA_MOVEMENT_TIMER > GET_GAME_TIMER()
					REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME() //Fix for bug 2193182
				ENDIF
				
			BREAK

			CASE 999
				//Racer eliminated from race.
			BREAK

		ENDSWITCH
		
		IF  s_RaceData.bRaceStarted AND NOT s_RaceData.bRaceOver

			IF bCheatBoost AND NOT IS_VECTOR_ZERO( vBoostForce )
			
				IF CAN_VEHICLE_BOOST(PedData.sVehicle.VehicleIndex)
					IF GET_GAME_TIMER() > s_RaceData.iRaceStartTime + AI_START_BOOST_DURATION_HIGH
						vBoostForce *= ( ( g_savedGlobals.sCountryRaceData.iCurrentRace + 1 ) * 0.75 )// += ( <<0,2,0>> * TO_FLOAT( g_savedGlobals.sCountryRaceData.iCurrentRace ) )
					ENDIF
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS( PedData.sVehicle.VehicleIndex, APPLY_TYPE_FORCE, vBoostForce, 0, TRUE, TRUE, TRUE)
					
				ENDIF
				
			ENDIF
			
			//Apply downward force to help cars grip on the corners.
			IF IS_RACER_NEAR_SHARP_CORNER( PedData )
			AND IS_VEHICLE_ON_ALL_WHEELS( PedData.sVehicle.VehicleIndex ) 
			AND NOT IS_ENTITY_IN_AIR( PedData.sVehicle.VehicleIndex )
				APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS( PedData.sVehicle.VehicleIndex, APPLY_TYPE_FORCE, <<0,0,-10>>, 0, TRUE, TRUE, TRUE)
				SET_VEHICLE_FRICTION_OVERRIDE( PedData.sVehicle.VehicleIndex, 2.0 )
			ENDIF

		ENDIF
		
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Checks if a peds vehicle is stuck.
/// PARAMS:
///    PedData - The owner of the vehicle we want to check.
/// RETURNS:
///    Whether the vehicle is trapped or stuck.
FUNC BOOL IS_PEDS_VEHICLE_STUCK_OR_TRAPPED(PED_STRUCT &PedData)

	IF IS_THIS_MODEL_A_CAR( PedData.sVehicle.eModel )
		IF IS_ENTITY_UPSIDEDOWN( PedData.sVehicle.VehicleIndex )
		OR IS_VEHICLE_STUCK_TIMER_UP( PedData.sVehicle.VehicleIndex, VEH_STUCK_ON_ROOF, ROOF_TIME)
		OR IS_VEHICLE_STUCK_TIMER_UP( PedData.sVehicle.VehicleIndex, VEH_STUCK_ON_SIDE, SIDE_TIME)
			CPRINTLN(DEBUG_MISSION, PedData.Name, " is stuck upsidedown.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_WATER( PedData.sVehicle.VehicleIndex )
		IF IS_THIS_MODEL_A_CAR( PedData.sVehicle.eModel )
		OR IS_THIS_MODEL_A_BIKE( PedData.sVehicle.eModel )
			CPRINTLN(DEBUG_MISSION, PedData.Name, " is stuck in water.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_STUCK_TIMER_UP( PedData.sVehicle.VehicleIndex, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
	OR IS_VEHICLE_STUCK_TIMER_UP( PedData.sVehicle.VehicleIndex, VEH_STUCK_JAMMED, JAMMED_TIME)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Handles the crashing and cleanup logic of peds with vehicles.
/// PARAMS:
///    PedData - The owner of the vehicle.
PROC CLEANUP_EMPTY_AND_DEAD_VEHICLES(PED_STRUCT PedData)
	
	IF PedData.bCreated
	
		IF DOES_ENTITY_EXIST( PedData.sVehicle.VehicleIndex )

			IF NOT IS_ENTITY_ALIVE( PedData.sVehicle.VehicleIndex )
			OR IS_VEHICLE_EMPTY( PedData.sVehicle.VehicleIndex, TRUE)
				
				SET_VEHICLE_AS_NO_LONGER_NEEDED( PedData.sVehicle.VehicleIndex )
				CPRINTLN(DEBUG_MISSION, PedData.name, "'s vehicle is no longer needed. " )
				
			ENDIF
		
		ENDIF
	
	ENDIF

ENDPROC

/// PURPOSE:
///    Updates ped blips.
PROC UPDATE_BLIPS(PED_STRUCT &sMissionPed[] )
	INT i
	REPEAT COUNT_OF(MISSION_PED_FLAGS) i
		
		IF DOES_BLIP_EXIST(sMissionPed[i].BlipIndex)
		
			IF IS_ENTITY_ALIVE(sMissionPed[i].PedIndex) AND IS_PED_IN_ANY_VEHICLE(sMissionPed[i].PedIndex)	
			
				IF GET_SEAT_PED_IS_IN(sMissionPed[i].PedIndex) = VS_DRIVER
				
					SET_BLIP_ALPHA(sMissionPed[i].BlipIndex, 255)
					SET_BLIP_SCALE(sMissionPed[i].BlipIndex, BLIP_SIZE_VEHICLE)
					SET_BLIP_HIDDEN_ON_LEGEND(sMissionPed[i].BlipIndex, FALSE)
					
				ELSE
				
					SET_BLIP_ALPHA(sMissionPed[i].BlipIndex, 0)
					SET_BLIP_SCALE(sMissionPed[i].BlipIndex, BLIP_SIZE_PED)
					SET_BLIP_HIDDEN_ON_LEGEND(sMissionPed[i].BlipIndex, TRUE)
					
				ENDIF
				
			ELSE
			
				SET_BLIP_ALPHA(sMissionPed[i].BlipIndex, 255)
				SET_BLIP_SCALE(sMissionPed[i].BlipIndex, BLIP_SIZE_PED)
				SET_BLIP_HIDDEN_ON_LEGEND(sMissionPed[i].BlipIndex, FALSE)
				
			ENDIF
			
		ELSE
			
			IF IS_ENTITY_ALIVE(sMissionPed[i].PedIndex) AND sMissionPed[i].bCanBeBlipped
			
				sMissionPed[i].BlipIndex = CREATE_BLIP_FOR_PED(sMissionPed[i].PedIndex, FALSE)
				SET_BLIP_NAME_FROM_TEXT_FILE(sMissionPed[i].BlipIndex, "CRACEBLIP")
			
			ENDIF
			
		ENDIF
	
	ENDREPEAT

ENDPROC

/// PURPOSE:
///    Updates the players position in the race.
PROC UPDATE_PLAYER_POSITION(PED_STRUCT &sMissionPed[], COUNTRY_RACER_DATA &sPlayerRacerData )
		
	INT iPos = 1
	INT i
	REPEAT COUNT_OF(sMissionPed) i
		
		IF IS_ENTITY_ALIVE(sMissionPed[i].PedIndex)
		
			IF sMissionPed[i].sRacerData.bFinished // Racer has finished.
				iPos++
			ELIF sMissionPed[i].sRacerData.iLap > sPlayerRacerData.iLap // Racer is a lap ahead.
				iPos++
			ELIF sMissionPed[i].sRacerData.iLap = sPlayerRacerData.iLap // Racer and Ped are on the same lap.
			
				IF sMissionPed[i].sRacerData.iCurrentCheckpoint > sPlayerRacerData.iCurrentCheckpoint // Racers checkpoint is ahead of the players.
					iPos++
				ELIF sMissionPed[i].sRacerData.iCurrentCheckpoint = sPlayerRacerData.iCurrentCheckpoint // Racer and ped are on the same checkpoint.
					
					FLOAT fRacerDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( sMissionPed[i].PedIndex, s_TrackData.vCheckpoint[sMissionPed[i].sRacerData.iRaceProgress] )
					FLOAT fPlayerDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(), s_TrackData.vCheckpoint[sPlayerRacerData.iRaceProgress] )
					
					IF fRacerDistance < fPlayerDistance // Racer is closer to the next checkpoint.
						iPos++
					ENDIF
					
				ENDIF
				
			ENDIF
		
		ENDIF
		
	ENDREPEAT
	
	sPlayerRacerData.iPosition = iPos
	
ENDPROC

CONST_INT LAP_TEXT_TIME BIG_MESSAGE_DISPLAY_TIME

/// PURPOSE:
///    Displays the lap number when the player starts a new lap
PROC DISPLAY_LAPS_BIG_TEXT(BIG_TEXT &sLapText, COUNTRY_RACER_DATA sPlayerRacerData)
	SWITCH sLapText.eBigTextState
		CASE BTS_LOAD
			sLapText.sfBigText = REQUEST_SCALEFORM_MOVIE("MIDSIZED_MESSAGE")
			IF HAS_SCALEFORM_MOVIE_LOADED(sLapText.sfBigText)
				sLapText.eBigTextState = BTS_INIT
			ENDIF
		BREAK
		
		CASE BTS_INIT
			BEGIN_SCALEFORM_MOVIE_METHOD(sLapText.sfBigText, "SHOW_SHARD_MIDSIZED_MESSAGE")
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BM_LAP_STR")
					SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_WHITE)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("BM_LAP")
					SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_WHITE)
					ADD_TEXT_COMPONENT_INTEGER(sPlayerRacerData.iLap + 1)
					SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_WHITE)
					ADD_TEXT_COMPONENT_INTEGER(s_TrackData.iNumLaps)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			sLapText.iBigTextTimer = GET_GAME_TIMER() + LAP_TEXT_TIME
			sLapText.eBigTextState = BTS_DISPLAY
		BREAK
		
		CASE BTS_DISPLAY
			IF GET_GAME_TIMER() < sLapText.iBigTextTimer - 500
				IF HAS_SCALEFORM_MOVIE_LOADED(sLapText.sfBigText)
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(sLapText.sfBigText, 255, 255, 255, 255)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				ENDIF
			ELSE
				BEGIN_SCALEFORM_MOVIE_METHOD(sLapText.sfBigText, "SHARD_ANIM_OUT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.33)
				END_SCALEFORM_MOVIE_METHOD()
				sLapText.eBigTextState = BTS_CLEANUP
			ENDIF
		BREAK
		
		CASE BTS_CLEANUP
			IF GET_GAME_TIMER() < sLapText.iBigTextTimer
				IF HAS_SCALEFORM_MOVIE_LOADED(sLapText.sfBigText)
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(sLapText.sfBigText, 255, 255, 255, 255)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				ENDIF
			ELSE
				IF HAS_SCALEFORM_MOVIE_LOADED(sLapText.sfBigText)
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sLapText.sfBigText)
				ENDIF
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sLapText.sfBigText)
				sLapText.eBigTextState = BTS_READY
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Works out which checkpoint type is needed based upon angle between checkpoints.
/// PARAMS:
///    iCheckNum - The checkpoint index we want to get.
/// RETURNS:
///    The appropriate checkpoint type.
FUNC CHECKPOINT_TYPE GET_RACE_CHECKPOINT_TYPE(INT iCheckNum)
	  
	VECTOR  pos, pos2, pos3
	VECTOR  vec1, vec2
	
	FLOAT  	fReturnAngle
	 
	pos = s_TrackData.vCheckpoint[iCheckNum]
		
	IF iCheckNum+1 = MAX_CHECKPOINTS //- 1
		pos2 = s_TrackData.vCheckpoint[0]
	ELSE
		pos2 = s_TrackData.vCheckpoint[iCheckNum + 1]
	ENDIF
	
	IF iCheckNum - 1 >= 0
		pos3 = s_TrackData.vCheckpoint[iCheckNum-1]
	ENDIF
	
	vec1 = pos3 - pos
	vec2 = pos2 - pos
	 
	fReturnAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vec1.x, vec1.y, vec2.x, vec2.y)
	
	IF fReturnAngle > 180
		fReturnAngle = (360.0 - fReturnAngle)
	ENDIF
	
	IF fReturnAngle < fChev3
	
		RETURN CHECKPOINT_RACE_GROUND_CHEVRON_3
		
   	ELIF fReturnAngle < fChev2
	
		RETURN CHECKPOINT_RACE_GROUND_CHEVRON_2
	
	ELIF fReturnAngle < fChev1
	
		RETURN CHECKPOINT_RACE_GROUND_CHEVRON_1
	ELSE
		RETURN CHECKPOINT_RACE_GROUND_CHEVRON_1
	ENDIF
	
	RETURN CHECKPOINT_RACE_GROUND_CHEVRON_1	 
ENDFUNC

/// PURPOSE:
///    Gets appropriate checkpoint alpha based on the player's distance from the checkpoint
FUNC INT GET_CHECKPOINT_ALPHA(INT iCheckpoint)
	FLOAT fDis
	INT iA = 240
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		fDis = GET_DISTANCE_BETWEEN_COORDS(s_TrackData.vCheckPoint[iCheckpoint], GET_ENTITY_COORDS(PLAYER_PED_ID()))
		IF fDis > 100.0
			iA = 240
		ELSE
			iA = ROUND(fDis*2.4)
		ENDIF
	ENDIF
	
	RETURN iA
ENDFUNC

/// PURPOSE:
///    Calculates the normal from an array of vertices.
/// PARAMS:
///    verts - The array of vertices we want to get the normal of.
/// RETURNS:
///    The vector of the normal.
FUNC VECTOR CALCULATE_QUAD_NORMAL(VECTOR &verts[])
      INT i
      VECTOR u, v
      VECTOR norm
      
      REPEAT COUNT_OF(verts) i
            u = verts[i]
            v = verts[(i + 1) % COUNT_OF(verts)]
            
            norm.x += (u.y - v.y) * (u.z + v.z) 
            norm.y += (u.z - v.z) * (u.x + v.x) 
            norm.z += (u.x - v.x) * (u.y + v.y) 

      ENDREPEAT
    
      RETURN NORMALISE_VECTOR(norm)
ENDFUNC

/// PURPOSE:
///    Removes the race checkpoints
/// PARAMS:
///    sPlayerRacerData - The players racer data which holds the checkpoints.
PROC CLEANUP_RACE_CHECKPOINTS(COUNTRY_RACER_DATA &sPlayerRacerData)
	IF DOES_BLIP_EXIST(sPlayerRacerData.blCurrentCheckPoint)
		REMOVE_BLIP(sPlayerRacerData.blCurrentCheckPoint)
	ENDIF
	
	IF DOES_BLIP_EXIST(sPlayerRacerData.blNextCheckPoint)
		REMOVE_BLIP(sPlayerRacerData.blNextCheckPoint)
	ENDIF
	
	// Remove checkpoint
	IF sPlayerRacerData.cpCurrentCheckPoint != NULL
		DELETE_CHECKPOINT(sPlayerRacerData.cpCurrentCheckPoint)
	ENDIF
	
	// Remove checkpoint
	IF sPlayerRacerData.cpPreviousCheckpoint != NULL
		DELETE_CHECKPOINT(sPlayerRacerData.cpPreviousCheckpoint)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets offset points around a checkpoint.
/// PARAMS:
///    i - The index of the offset point we want.
/// RETURNS:
///    A vector offset from the desired checkpoint. 
FUNC VECTOR GET_CHECKPOINT_OFFSET(INT i)
      SWITCH i
            CASE 0            RETURN <<0,0,1>>
            CASE 1            RETURN <<FMMC_CHECKPOINT_SIZE/2,0,1>>
            CASE 2            RETURN <<-FMMC_CHECKPOINT_SIZE/2,0,1>>
            CASE 3            RETURN <<0,FMMC_CHECKPOINT_SIZE/2,1>>
            CASE 4            RETURN <<0,-FMMC_CHECKPOINT_SIZE/2,1>>
            CASE 5            RETURN <<FMMC_CHECKPOINT_SIZE/2,FMMC_CHECKPOINT_SIZE/2,1>>
            CASE 6            RETURN <<-FMMC_CHECKPOINT_SIZE/2,-FMMC_CHECKPOINT_SIZE/2,1>>
            CASE 7            RETURN <<FMMC_CHECKPOINT_SIZE/2,-FMMC_CHECKPOINT_SIZE/2,1>>
            CASE 8            RETURN <<-FMMC_CHECKPOINT_SIZE/2,FMMC_CHECKPOINT_SIZE/2,1>>
      ENDSWITCH
      RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Clips the bottom off the checkpoint so it doesn't poke through roads
/// PARAMS:
///    checkP - The checpoint to clip
///    vCheckPointGroundPos - The coordinates of the checkpoint
PROC CLIP_THE_CHECKPOINT(CHECKPOINT_INDEX checkP, VECTOR vCheckPointGroundPos)
      int i
      VECTOR vecArray[8]
      FLOAT fGround
      FOR i = 0 TO 7
            vecArray[i] = vCheckPointGroundPos + GET_CHECKPOINT_OFFSET(i)          
            GET_GROUND_Z_FOR_3D_COORD(vecArray[i],fGround)
            IF fGround < vCheckPointGroundPos.z - 2
            OR fGround > vCheckPointGroundPos.z + 2
                  vecArray[i].z = vCheckPointGroundPos.z
                  PRINTINT(i)PRINTSTRING("vPos z. vecArray[i]  = ")PRINTVECTOR(vecArray[i])PRINTNL()
            ELSE
                  vecArray[i].z = fGround
                  PRINTINT(i)PRINTSTRING("offset z. vecArray[i]  = ")PRINTVECTOR(vecArray[i])PRINTNL()
            ENDIF
      ENDFOR
      
      VECTOR vFinalNormal = CALCULATE_QUAD_NORMAL(vecArray)
      PRINTNL()PRINTSTRING("vFinalNormal  = ")PRINTVECTOR(vFinalNormal)PRINTNL()PRINTNL()
      SET_CHECKPOINT_CLIPPLANE_WITH_POS_NORM(checkP, vCheckPointGroundPos - <<0,0,0.3>>, vFinalNormal)
ENDPROC

/// PURPOSE:
///    Blips and creates the current checkpoint marker.
/// PARAMS:
///    sPlayerRacerData - The player's racer data.
///    bRaceFlag - Whether we want the checkpoint to show the finish line flag.
///    bDoPulse - Whether we want to pulse checkpoint after it is hit.
PROC BLIP_CURRENT_CHECKPOINT(COUNTRY_RACER_DATA &sPlayerRacerData, BOOL bRaceFlag = FALSE, BOOL bDoPulse = TRUE)
	
	INT iR, iG, iB, iA, iR2, iG2, iB2, iA2, iPreviousCheckpoint
	
	CHECKPOINT_TYPE cpType = GET_RACE_CHECKPOINT_TYPE(sPlayerRacerData.iRaceProgress)
	
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
	GET_HUD_COLOUR(HUD_COLOUR_NORTH_BLUE, iR2, iG2, iB2, iA2)
	iA = GET_CHECKPOINT_ALPHA(sPlayerRacerData.iRaceProgress)
	
	// Is this marker already shown?
	IF DOES_BLIP_EXIST(sPlayerRacerData.blCurrentCheckpoint)
		IF sPlayerRacerData.cpCurrentCheckpoint != NULL
			SET_CHECKPOINT_RGBA(sPlayerRacerData.cpCurrentCheckpoint,iR, iG, iB, iA)
			SET_CHECKPOINT_RGBA2(sPlayerRacerData.cpCurrentCheckpoint,iR2, iG2, iB2, iA)
		ENDIF
	ELSE
		
		VECTOR 	currentCheckpoint, nextCheckpoint, prevCheckpoint
		FLOAT fCheckPointSize, fCheckPointHeight

		fCheckPointSize = (CHECKPOINT_VISUAL_SIZE * CHECKPOINT_VISUAL_SIZE_MODIFIER)
		fCheckPointHeight = 6
		
		// Next checkpoint
		IF sPlayerRacerData.iRaceProgress = MAX_CHECKPOINTS - 1
			nextCheckpoint = s_TrackData.vCheckPoint[0]
		ELSE
			nextCheckpoint = s_TrackData.vCheckPoint[sPlayerRacerData.iRaceProgress+1]
		ENDIF
		//Previous checkpoint
		IF sPlayerRacerData.iRaceProgress = 0
			iPreviousCheckpoint = MAX_CHECKPOINTS - 1
		ELSE
			iPreviousCheckpoint = sPlayerRacerData.iRaceProgress - 1
		ENDIF
		
		currentCheckpoint = s_TrackData.vCheckPoint[sPlayerRacerData.iRaceProgress]
		prevCheckpoint = s_TrackData.vCheckPoint[iPreviousCheckpoint]
		currentCheckpoint.z -= 1
		prevCheckpoint.z -= 1
		
		IF iPreviousCheckpoint = MAX_CHECKPOINTS - 1
			prevCheckpoint.z -= 2
		ENDIF
		IF sPlayerRacerData.iRaceProgress = MAX_CHECKPOINTS - 1
			currentCheckpoint.z -= 2
		ENDIF
		
		// Blip checkpoint
		sPlayerRacerData.blCurrentCheckpoint = ADD_BLIP_FOR_COORD(currentCheckpoint)
		SHOW_HEIGHT_ON_BLIP(sPlayerRacerData.blCurrentCheckpoint, FALSE)
		
		// Setup race arrow for current checkpoint
		IF NOT bRaceFlag
			SET_BLIP_COLOUR(sPlayerRacerData.blCurrentCheckpoint, BLIP_COLOUR_YELLOW)
			SET_BLIP_SCALE(sPlayerRacerData.blCurrentCheckpoint, 1.2)
			SET_BLIP_PRIORITY(sPlayerRacerData.blCurrentCheckpoint, BLIPPRIORITY_HIGHEST)
			
			sPlayerRacerData.cpCurrentCheckpoint = CREATE_CHECKPOINT(cpType,(currentCheckpoint+<<0,0,fCheckPointHeight>>), nextCheckpoint, fCheckPointSize, iR, iG, iB, iA)//150)
			SET_CHECKPOINT_RGBA2(sPlayerRacerData.cpCurrentCheckpoint,iR2, iG2, iB2, iA)
			CLIP_THE_CHECKPOINT(sPlayerRacerData.cpCurrentCheckpoint, currentCheckpoint)
		
		// Chequered flag!
		ELSE
			SET_BLIP_SPRITE(sPlayerRacerData.blCurrentCheckpoint, RADAR_TRACE_RACEFLAG)
			SET_BLIP_SCALE(sPlayerRacerData.blCurrentCheckpoint, 1.2)
			SET_BLIP_PRIORITY(sPlayerRacerData.blCurrentCheckpoint, BLIPPRIORITY_HIGHEST)
						
			sPlayerRacerData.cpCurrentCheckpoint = CREATE_CHECKPOINT(CHECKPOINT_RACE_GROUND_FLAG, (currentCheckpoint+<<0,0,fCheckPointHeight>>), nextCheckpoint, fCheckPointSize, iR, iG, iB, iA)//150)
			SET_CHECKPOINT_RGBA2(sPlayerRacerData.cpCurrentCheckpoint,iR2, iG2, iB2, iA)
			CLIP_THE_CHECKPOINT(sPlayerRacerData.cpCurrentCheckpoint, currentCheckpoint)
		ENDIF
		
		IF bDoPulse 
			cpType = GET_RACE_CHECKPOINT_TYPE(iPreviousCheckpoint)
			GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, sPlayerRacerData.iPrevCheckpointAlpha)
			sPlayerRacerData.iPrevCheckpointAlpha = 180
			IF sPlayerRacerData.cpPreviousCheckpoint != NULL
				DELETE_CHECKPOINT(sPlayerRacerData.cpPreviousCheckpoint)
			ENDIF
			prevCheckpoint = prevCheckpoint
			sPlayerRacerData.cpPreviousCheckpoint = CREATE_CHECKPOINT(cpType,(prevCheckpoint+<<0,0,fCheckPointHeight>>), currentCheckpoint, fCheckPointSize, iR, iG, iB, sPlayerRacerData.iPrevCheckpointAlpha)//150)
			CLIP_THE_CHECKPOINT(sPlayerRacerData.cpPreviousCheckpoint, prevCheckpoint)
		ENDIF

		SET_CHECKPOINT_CYLINDER_HEIGHT(sPlayerRacerData.cpCurrentCheckpoint,9.5,9.5,30)
		SET_CHECKPOINT_CYLINDER_HEIGHT(sPlayerRacerData.cpPreviousCheckpoint,9.5,9.5,30)

		SET_BLIP_NAME_FROM_TEXT_FILE(sPlayerRacerData.blCurrentCheckpoint, "BLIP_CPOINT")		
	ENDIF

	IF sPlayerRacerData.cpPreviousCheckpoint != NULL
		sPlayerRacerData.iPrevCheckpointAlpha -= 10
		IF sPlayerRacerData.iPrevCheckpointAlpha > 0
			GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
			SET_CHECKPOINT_RGBA(sPlayerRacerData.cpPreviousCheckpoint,iR, iG, iB, sPlayerRacerData.iPrevCheckpointAlpha)
		ELSE
			DELETE_CHECKPOINT(sPlayerRacerData.cpPreviousCheckpoint)
		ENDIF
	ENDIF
	

ENDPROC

/// PURPOSE:
///    Creates the blip for the next checkpoint.
/// PARAMS:
///    sPlayerRacerData - The player's racer data.
///    nextCheckpoint - The position of the next checkpoint.
///    bRaceFlag - Whether we want the blip to be the finish line flag.
PROC BLIP_NEXT_CHECKPOINT(COUNTRY_RACER_DATA &sPlayerRacerData, VECTOR nextCheckpoint, BOOL bRaceFlag = FALSE)
	
	IF NOT DOES_BLIP_EXIST(sPlayerRacerData.blNextCheckpoint)
		
		sPlayerRacerData.blNextCheckpoint = ADD_BLIP_FOR_COORD(nextCheckpoint)
		
		SHOW_HEIGHT_ON_BLIP(sPlayerRacerData.blNextCheckpoint, FALSE)
		
		IF NOT bRaceFlag
			
			// Normal checkpoint
			SET_BLIP_COLOUR(sPlayerRacerData.blNextCheckpoint, BLIP_COLOUR_YELLOW)
			SET_BLIP_SCALE(sPlayerRacerData.blNextCheckpoint, 0.7)
		ELSE
			// Finish line
			IF sPlayerRacerData.iLap = s_TrackData.iNumLaps
				SET_BLIP_SPRITE(sPlayerRacerData.blNextCheckpoint, RADAR_TRACE_RACEFLAG)
				SET_BLIP_SCALE(sPlayerRacerData.blNextCheckpoint, 1.2)
			ENDIF
		ENDIF
		
		SET_BLIP_NAME_FROM_TEXT_FILE(sPlayerRacerData.blNextCheckpoint, "BLIP_CPOINT")
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws the race checkpoints and blips.
/// PARAMS:
///    sPlayerRacerData - The player's racer data.
PROC DRAW_RACE_CHECKPOINTS(COUNTRY_RACER_DATA &sPlayerRacerData)

	// Get next checkpoint
	INT iNextCheckpoint = sPlayerRacerData.iRaceProgress + 1

	// Final checkpoint of current lap
	IF sPlayerRacerData.iRaceProgress = MAX_CHECKPOINTS - 1
		
		// Finish line
		IF sPlayerRacerData.iLap + 1 = s_TrackData.iNumLaps
			BLIP_CURRENT_CHECKPOINT(sPlayerRacerData, TRUE)
		ELSE
			// New lap
			BLIP_CURRENT_CHECKPOINT(sPlayerRacerData, FALSE, TRUE)
			BLIP_NEXT_CHECKPOINT(sPlayerRacerData, s_TrackData.vCheckPoint[0])
		ENDIF
	ELSE
		// Normal checkpoint
		IF sPlayerRacerData.iCurrentCheckpoint < 1
			BLIP_CURRENT_CHECKPOINT(sPlayerRacerData, FALSE, FALSE)
		ELSE
			BLIP_CURRENT_CHECKPOINT(sPlayerRacerData, FALSE)
		ENDIF

		// Next checkpoint is the finish/new lap
		IF iNextCheckpoint = MAX_CHECKPOINTS - 1
			BLIP_NEXT_CHECKPOINT(sPlayerRacerData, s_TrackData.vCheckPoint[iNextCheckpoint], TRUE)
		ELSE
			BLIP_NEXT_CHECKPOINT(sPlayerRacerData, s_TrackData.vCheckPoint[iNextCheckpoint])
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Sets the country race as passed
PROC SET_COUNTRY_RACE_PASSED()
	
	CPRINTLN(DEBUG_MISSION, "Setting g_bCountryRacePassed to TRUE" )
	g_bCountryRacePassed = TRUE

ENDPROC

/// PURPOSE:
///    Sets the country race as failed.
PROC SET_COUNTRY_RACE_FAILED()
	
	CPRINTLN(DEBUG_MISSION, "Setting g_bCountryRacePassed to FALSE" )
	g_bCountryRacePassed = FALSE
	
ENDPROC

/// PURPOSE:
///    Determines if the race was passed.
/// RETURNS:
///    Whether the race was passed or not.
FUNC BOOL IS_COUNTRY_RACE_PASSED()
	
	CPRINTLN(DEBUG_MISSION, "IS_COUNTRY_RACE_PASSED() - TRUE" )
	RETURN g_bCountryRacePassed
	
ENDFUNC

/// PURPOSE:
///    Checks if the player is in a suitable vehicle for the race
/// RETURNS:
///    TRUE if the player is in a suitable car
FUNC BOOL IS_PLAYER_IN_SUITABLE_RACE_CAR()
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
		//IF IS_VEHICLE_OK(viPlayer)
		IF IS_VEHICLE_OK(GET_PLAYERS_LAST_VEHICLE())
			//MODEL_NAMES mnPlayer = GET_ENTITY_MODEL(viPlayer)
			MODEL_NAMES mnPlayer = GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())
			
			// General checks
			IF IS_MODEL_POLICE_VEHICLE(mnPlayer)
				//CPRINTLN(DEBUG_MISSION, "Player in police vehicle")
				RETURN FALSE
			ENDIF
			// these vehicles aren't covered by the IS_MODEL_POLICE_VEHICLE function
			IF mnPlayer = POLICE4
			OR mnPlayer = POLICEOLD1
			OR mnPlayer = POLICEOLD2
			OR mnPlayer = FBI
			OR mnPlayer = FBI2
			OR mnPlayer = LGUARD
			OR mnPlayer = SHERIFF
			OR mnPlayer = SHERIFF2
				//CPRINTLN(DEBUG_MISSION, "Player in police vehicle")
				RETURN FALSE
			ENDIF
			
			IF IS_THIS_MODEL_A_BIKE(mnPlayer)
			OR IS_THIS_MODEL_A_BOAT(mnPlayer)
			OR IS_THIS_MODEL_A_HELI(mnPlayer)
			OR IS_THIS_MODEL_A_PLANE(mnPlayer)
				//CPRINTLN(DEBUG_MISSION, "Player not in car")
				RETURN FALSE
			ENDIF
			
			// Race blacklist
			INT         iIndex
			MODEL_NAMES mBlacklist[91]
		    
		    mBlacklist[0] =  AMBULANCE 
		    mBlacklist[1] =  BENSON 
		    mBlacklist[2] =  BIFF 
		    mBlacklist[3] =  BUS 
		    mBlacklist[4] =  FIRETRUK
		    mBlacklist[5] =  FORKLIFT 
		    mBlacklist[6] =  MULE
			mBlacklist[7] =  MULE2
		    mBlacklist[8] =  PACKER 
		    mBlacklist[9] =  PHANTOM
		    mBlacklist[10] = MOWER 
		    mBlacklist[11] = STOCKADE
		    mBlacklist[12] = SQUALO
		    mBlacklist[13] = MAVERICK 
		    mBlacklist[14] = POLMAV
		    mBlacklist[15] = AIRTUG
		    mBlacklist[16] = PRANGER
		    mBlacklist[17] = ANNIHILATOR
		    mBlacklist[18] = DINGHY
		    mBlacklist[19] = POLICE
		    mBlacklist[20] = RIPLEY
		    mBlacklist[21] = TRASH
		    mBlacklist[22] = BURRITO
		    mBlacklist[23] = PONY
		    mBlacklist[24] = SPEEDO
		    mBlacklist[25] = MARQUIS
		    mBlacklist[26] = SANCHEZ
		 	mBlacklist[27] = AIRTUG
			mBlacklist[28] = TACO
			mBlacklist[29] = BARRACKS
			mBlacklist[30] = ROMERO
			mBlacklist[31] = BLAZER	
			mBlacklist[32] = BLAZER2
			mBlacklist[33] = BODHI2
			mBlacklist[34] = BOXVILLE2
			mBlacklist[35] = BULLDOZER
			mBlacklist[36] = CADDY
			mBlacklist[37] = CADDY2
			mBlacklist[38] = CAMPER
			mBlacklist[39] = TIPTRUCK
			mBlacklist[40] = TOURBUS
			mBlacklist[41] = TOWTRUCK
			mBlacklist[42] = TOWTRUCK2
			mBlacklist[43] = TRACTOR
			mBlacklist[44] = TRACTOR2
			mBlacklist[45] = UTILLITRUCK
			mBlacklist[46] = UTILLITRUCK2
			mBlacklist[47] = UTILLITRUCK3
			mBlacklist[48] = RATLOADER
			mBlacklist[49] = DLOADER
			mBlacklist[50] = DOCKTUG
			mBlacklist[51] = DUMP
			mBlacklist[52] = GBURRITO
			mBlacklist[53] = HANDLER
			mBlacklist[54] = HAULER
			mBlacklist[55] = JOURNEY
			mBlacklist[56] = RENTALBUS
			mBlacklist[57] = MIXER
			mBlacklist[58] = RHINO
			mBlacklist[59] = CUTTER
			mBlacklist[60] = POUNDER
			mBlacklist[61] = TIPTRUCK2
			mBlacklist[62] = MIXER2
			mBlacklist[63] = RUBBLE
			mBlacklist[64] = SCRAP
			mBlacklist[65] = ARMYTANKER
			mBlacklist[66] = BARRACKS2
			mBlacklist[67] = AIRBUS
			mBlacklist[68] = COACH
			mBlacklist[69] = PBUS
			mBlacklist[70] = RIOT
			mBlacklist[71] = BOXVILLE3
			mBlacklist[72] = STOCKADE3
			mBlacklist[73] = FLATBED
			mBlacklist[74] = BOXVILLE
			mBlacklist[75] = BURRITO2
			mBlacklist[76] = BURRITO3
			mBlacklist[77] = BURRITO4
			mBlacklist[78] = RUMPO
			mBlacklist[79] = SPEEDO2
			mBlacklist[80] = BLIMP
			mBlacklist[81] = BLIMP2
			mBlacklist[82] = SUBMERSIBLE
			mBlacklist[83] = SUBMERSIBLE2
			mBlacklist[84] = BLAZER3
			mBlacklist[85] = PONY2
			mBlacklist[86] = RUMPO2
			mBlacklist[87] = TAXI
			mBlacklist[88] = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("RATLOADER2"))
			mBlacklist[89] = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("SLAMVAN"))
			mBlacklist[90] = DUMMY_MODEL_FOR_SCRIPT
		    REPEAT COUNT_OF(mBlacklist) iIndex
		        IF mnPlayer = mBlacklist[iIndex]
					RETURN FALSE
		        ENDIF
		    ENDREPEAT

			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


USING "script_usecontext.sch"

ENUM END_SCREEN_STATE
	
	END_SCREEN_INITIATE,
	END_SCREEN_RUNNING,
	END_SCREEN_EXITTING
	
ENDENUM
END_SCREEN_STATE e_EndScreenState

/// PURPOSE:
///    Initialises the context buttons for the result screen.
/// PARAMS:
///    sUseContext - The use context we want to initialise the buttons for.
///    bAddRetry - Whether we want to add a retry button.
PROC INIT_RESULT_SCREEN_BUTTONS(SIMPLE_USE_CONTEXT &sUseContext, BOOL bAddRetry)
	CPRINTLN(DEBUG_MISSION, "INIT_RESULT_SCREEN_BUTTONS ")
	INIT_SIMPLE_USE_CONTEXT(sUseContext, FALSE, FALSE, TRUE, TRUE)
	ADD_SIMPLE_USE_CONTEXT_INPUT(sUseContext, "CRACECONT", FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
	IF bAddRetry
		ADD_SIMPLE_USE_CONTEXT_INPUT(sUseContext, "CRACERET", FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
	ENDIF
	SET_SIMPLE_USE_CONTEXT_FULLSCREEN(sUseContext)
	SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(sUseContext)
ENDPROC

/// PURPOSE:
///    Handles the context controls for the results screen.
/// PARAMS:
///    sUseContext - The use context we want to update.
PROC DRAW_RESULT_SCREEN_INSTRUCTIONS(SIMPLE_USE_CONTEXT &sUseContext)
	UPDATE_SIMPLE_USE_CONTEXT(sUseContext)
ENDPROC

/// PURPOSE:
///    Updates the race result screen.
/// PARAMS:
///    sPlayerRacerData - The player's racer data.
///    sEndScreenData - The end screen data struct.
///    sUseContext - The use context controls.
///    bRetry - The flag we set if the player wants to retry.
/// RETURNS:
///    Once the results screen has finished.
FUNC BOOL DISPLAY_RACE_RESULTS(COUNTRY_RACER_DATA sPlayerRacerData, END_SCREEN_DATASET &sEndScreenData, SIMPLE_USE_CONTEXT &sUseContext, BOOL &bRetry)
	
	SWITCH e_EndScreenState
		
		CASE END_SCREEN_INITIATE
			RESET_ENDSCREEN(sEndScreenData)
//			BOOL bAddRetry
//			IF sPlayerRacerData.iFinalPosition <> 1 bAddRetry = TRUE ELSE bAddRetry = FALSE ENDIF
			INIT_RESULT_SCREEN_BUTTONS(sUseContext, TRUE)
			
			END_SCREEN_MEDAL_STATUS eMedalStatus 
			eMedalStatus = ESMS_NO_MEDAL
			
			SWITCH sPlayerRacerData.iFinalPosition
				CASE 1	eMedalStatus = ESMS_GOLD		BREAK
				CASE 2	eMedalStatus = ESMS_SILVER		BREAK
				CASE 3	eMedalStatus = ESMS_BRONZE		BREAK
				DEFAULT	eMedalStatus = ESMS_NO_MEDAL	BREAK
			ENDSWITCH
			
			TEXT_LABEL sPositionLabel
			sPositionLabel = "CRACEP"
			sPositionLabel += sPlayerRacerData.iFinalPosition
			
			TEXT_LABEL sRaceName
			sRaceName = "CRACETRACK"
			sRaceName += (g_savedGlobals.sCountryRaceData.iCurrentRace + 1)
			
			SET_ENDSCREEN_DATASET_HEADER(sEndScreenData, sPositionLabel, sRaceName)
			IF sPlayerRacerData.iTotalTime <> 0
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(sEndScreenData, ESEF_TIME_M_S_MS_WITH_PERIOD, "CRACETIME", "", sPlayerRacerData.iTotalTime, 0, ESCM_NO_MARK)
			ENDIF
			IF g_savedGlobals.sCountryRaceData.iBestTime <> 0
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(sEndScreenData, ESEF_TIME_M_S_MS_WITH_PERIOD, "CRACEBTIME", "", g_savedGlobals.sCountryRaceData.iBestTime, 0, ESCM_NO_MARK)
			ENDIF
			
			sEndScreenData.bHoldOnEnd  = TRUE
			
			
			IF sPlayerRacerData.iFinalPosition <= 3
				SET_ENDSCREEN_COMPLETION_LINE_STATE(sEndScreenData, TRUE, "CRACERES", sPlayerRacerData.iFinalPosition, MAX_RACERS, ESC_FRACTION_COMPLETION, eMedalStatus)
			ELSE
				SET_ENDSCREEN_COMPLETION_LINE_STATE(sEndScreenData, FALSE, "CRACERES", sPlayerRacerData.iFinalPosition, MAX_RACERS, ESC_FRACTION_COMPLETION, eMedalStatus)
			ENDIF

			SETTIMERB(0)
			
			IF sPlayerRacerData.iFinalPosition <= 1
				PLAY_SOUND_FRONTEND(-1, "MEDAL_UP", "HUD_MINI_GAME_SOUNDSET")
			ELSE
				PLAY_SOUND_FRONTEND(-1, "RACE_PLACED", "HUD_AWARDS")
			ENDIF
			
			e_EndScreenState = END_SCREEN_RUNNING
			
			ENDSCREEN_PREPARE(sEndScreenData, TRUE)
			
			sEndScreenData.bAlwaysShowStats = TRUE	
		BREAK
		
		CASE END_SCREEN_RUNNING
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			RENDER_ENDSCREEN(sEndScreenData)
			DRAW_RESULT_SCREEN_INSTRUCTIONS(sUseContext)
			
			IF TIMERB() > 2000
			
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					SET_MOUSE_CURSOR_THIS_FRAME()
				ENDIF
				
				IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
					FORCE_ENDSCREEN_ANIM_OUT(sEndScreenData)
					e_EndScreenState = END_SCREEN_EXITTING
					bRetry = TRUE
				
				ELIF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
					FORCE_ENDSCREEN_ANIM_OUT(sEndScreenData)
					e_EndScreenState = END_SCREEN_EXITTING
					bRetry = FALSE
				ENDIF				
				
			ENDIF
		BREAK
		
		CASE END_SCREEN_EXITTING
			IF RENDER_ENDSCREEN(sEndScreenData)
				ENDSCREEN_SHUTDOWN(sEndScreenData)
				CLEANUP_SIMPLE_USE_CONTEXT(sUseContext)
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	
	RETURN FALSE

ENDFUNC

