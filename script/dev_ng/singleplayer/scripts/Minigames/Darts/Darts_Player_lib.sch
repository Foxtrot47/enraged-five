////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Player.sch                        //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Dart Player Defs       			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "Darts_Player.sch"
USING "Darts_Dart_lib.sch"
USING "Darts_Board_lib.sch"
USING "Darts_Audio.sch"
USING "Darts_Core.sch"
USING "commands_object.sch"
USING "commands_entity.sch"


DARTS_SHOT_STATE DartShotState

INT iThrowMeter
INT iThrowTimeStart
INT iThrowTimeEnd
INT iThrowTimeDiff
INT iGoodThrowTime = 65
INT iThrowX
INT iSteadyShotLength = 660 //880 //440 //250 //500 //1000

#IF IS_DEBUG_BUILD
INT iDartStartPos
INT iDartEndPos

FLOAT fThrowSpeed
#ENDIF

FLOAT fDartSpeedRatio
FLOAT fDartSpeedOffset
FLOAT fDartCenterRatio
FLOAT fDartCenterOffset
FLOAT fDartMaxOffset = 0.27
FLOAT fDartMaxCenterOffset = 0.1
FLOAT fThrowLeftLimit = -120.0
FLOAT fThrowRightLimit = 127.0

PROC CLEANUP_RETICLE()
	IF DOES_ENTITY_EXIST(oiReticle)
		DELETE_OBJECT(oiReticle)
		//SET_OBJECT_AS_NO_LONGER_NEEDED(oiReticle)
	ENDIF
ENDPROC

PROC INIT_SPRITE_RETICLE(INT player=0)
	INT playerColor = player + 1
	// TARGETING RETICLE
	//position
	spriteReticle.x = 0.5
	spriteReticle.y = 0.5
	
	//scale
	IF GET_IS_WIDESCREEN()
		CDEBUG1LN(DEBUG_DARTS, "widescreen, adjusting reticule accordingly")
		spriteReticle.w = 0.050 //1//0.045 //final?
		spriteReticle.h = 0.095 //1//0.080
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "not widescreen, adjusting reticule accordingly")
		spriteReticle.w = 0.065
		spriteReticle.h = 0.090
	ENDIF
	
	//color - white
	spriteReticle.r = 255
	spriteReticle.g = 250
	spriteReticle.b = 255
	spriteReticle.a = 123 //255
	
	IF playerColor = 1
		//color - blue EDIT: White
		spriteReticle.r = 255
		spriteReticle.g = 255
		spriteReticle.b = 255
	ELIF playerColor = 2
		//color - red EDIT: White
		spriteReticle.r = 255
		spriteReticle.g = 255
		spriteReticle.b = 255
	ENDIF
	
	spriteReticle.fRotation = 0.0
	
	// DART LANDING
	//position
	spriteDartLanding.x = 0.0
	spriteDartLanding.y = 0.0
	
	//scale
	//spriteDartLanding.w = 0.031 //sprite ver. 1
	//spriteDartLanding.h = 0.051
	spriteDartLanding.w = 0.061 //sprite ver. 2
	spriteDartLanding.h = 0.105
	
	//color - red
	spriteDartLanding.r = 200
	spriteDartLanding.g = 45
	spriteDartLanding.b = 40
	spriteDartLanding.a = 255
	
	spriteDartLanding.fRotation = 0.0
ENDPROC

FUNC BOOL IS_RAGE_READY(INT RangeTank)
	IF RangeTank >= 50
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DART_RETICLE_GOOD_THROW()
	// green for now.  consider making this a 'fading to green' type procedure
	spriteDartLanding.r = 16
	spriteDartLanding.g = 184
	spriteDartLanding.b = 10
ENDPROC

PROC DART_RETICLE_MEDIUM_THROW()
	// yellow-ish
	spriteDartLanding.r = 183
	spriteDartLanding.g = 137
	spriteDartLanding.b = 0
ENDPROC

PROC DART_RETICLE_BAD_THROW()
	// red-ish
	spriteDartLanding.r = 210
	spriteDartLanding.g = 23
	spriteDartLanding.b = 19
ENDPROC

PROC DART_RETICLE_WHITE()
	// default
	spriteReticle.r = 255
	spriteReticle.g = 250
	spriteReticle.b = 240
ENDPROC

PROC DART_RETICLE_NORMAL_SIZE()
//	spriteReticle.w = 0.144
//	spriteReticle.h = 0.246
//	spriteReticle.w = 0.050
//	spriteReticle.h = 0.095
ENDPROC

PROC DART_RETICLE_SMALL_SIZE()
//	spriteReticle.w = 0.089
//	spriteReticle.h = 0.152
//	spriteReticle.w = 0.045
//	spriteReticle.h = 0.085
ENDPROC

PROC DART_SHOT_SPICE(DART & Darts)
	//  add a little spice to each shot
	FLOAT 	fTemp
	
	fTemp = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.3)
	fTemp /= 10
	
	CDEBUG1LN(DEBUG_DARTS, "***** AMOUNT OF SPICE: ", fTemp, " *****")
	
	INT i
	INT	iDartDirection
	
	i = GET_RANDOM_INT_IN_RANGE(0, 900)
	iDartDirection = i%9
	
	SWITCH iDartDirection
		CASE 0
			CDEBUG1LN(DEBUG_DARTS, "****** NOT SO SPICY - NO OFFSET *****")
		BREAK
		
		CASE 1
			CDEBUG1LN(DEBUG_DARTS, "****** SO SPICY - OFFSET LEFT *****")
			Darts.vTarget.x += fTemp
		BREAK
		
		CASE 2
			CDEBUG1LN(DEBUG_DARTS, "****** SO SPICY - OFFSET RIGHT *****")
			Darts.vTarget.x += fTemp
		BREAK
		
		CASE 3
			CDEBUG1LN(DEBUG_DARTS, "****** SO SPICY - OFFSET UP *****")
			Darts.vTarget.y += fTemp
		BREAK
		
		CASE 4
			CDEBUG1LN(DEBUG_DARTS, "****** SO SPICY - OFFSET DOWN *****")
			Darts.vTarget.y -= fTemp
		BREAK
		
		CASE 5
			CDEBUG1LN(DEBUG_DARTS, "****** SO SPICY - OFFSET UP LEFT *****")
			fTemp = SQRT((fTemp * fTemp)/2)
			Darts.vTarget.x -= fTemp
			Darts.vTarget.y += fTemp
		BREAK
		
		CASE 6
			CDEBUG1LN(DEBUG_DARTS, "****** SO SPICY - OFFSET UP RIGHT *****")
			fTemp = SQRT((fTemp * fTemp)/2)
			Darts.vTarget.x += fTemp
			Darts.vTarget.y += fTemp
		BREAK
		
		CASE 7
			CDEBUG1LN(DEBUG_DARTS, "****** SO SPICY - OFFSET DOWN LEFT *****")
			fTemp = SQRT((fTemp * fTemp)/2)
			Darts.vTarget.x -= fTemp
			Darts.vTarget.y -= fTemp
		BREAK
		
		CASE 8
			CDEBUG1LN(DEBUG_DARTS, "****** SO SPICY - OFFSET DOWN RIGHT *****")
			fTemp = SQRT((fTemp * fTemp)/2)
			Darts.vTarget.x += fTemp
			Darts.vTarget.y -= fTemp
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL DO_STICK_SHOT(DART & Darts)
	BOOL bThrowMade
	
	FLOAT fRightX, fRightY
	
	fRightX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
	fRightY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	
	iThrowMeter = ROUND(fRightY)
	
	SWITCH DartShotState
		CASE DARTS_SHOT_IDLE
			IF iThrowMeter > 120
				iThrowTimeStart = GET_GAME_TIMER()
				
				DartShotState = DARTS_SHOT_BACK
				CDEBUG1LN(DEBUG_DARTS, "iThrowMeter is at ", iThrowMeter, ". Going to DARTS_SHOT_BACK")				
#IF IS_DEBUG_BUILD
				iDartStartPos = iThrowMeter
#ENDIF	//	IS_DEBUG_BUILD
			ENDIF
			
			IF iThrowMeter < -100
				iThrowX = ROUND(fRightX) //iRightX
				CDEBUG1LN(DEBUG_DARTS, "iThrowX is at ", iThrowX)
			ENDIF
		BREAK
		
		CASE DARTS_SHOT_BACK
			IF iThrowMeter > 120
				// right stick is being held back
				iThrowTimeStart = GET_GAME_TIMER()
#IF IS_DEBUG_BUILD
				iDartStartPos = iThrowMeter
#ENDIF	//	IS_DEBUG_BUILD
			ELIF iThrowMeter < 120 AND iThrowMeter > -120 AND (GET_GAME_TIMER() - iThrowTimeStart) > 500
				// right stick has returned to idle position for over half a second
				DartShotState = DARTS_SHOT_IDLE
				CDEBUG1LN(DEBUG_DARTS, "Going back to DARTS_SHOT_IDLE")
			ELIF iThrowMeter < -100
				//right stick has been flicked forward
				iThrowTimeEnd = GET_GAME_TIMER()
#IF IS_DEBUG_BUILD
				iDartEndPos = iThrowMeter
#ENDIF	//	IS_DEBUG_BUILD
				IF (iThrowTimeEnd - iThrowTimeStart) < 500
					iThrowX = ROUND(fRightX) //iRightX
					CDEBUG1LN(DEBUG_DARTS, "iThrowX offset is ", iThrowX)
					iThrowTimeDiff = iThrowTimeEnd - iThrowTimeStart
#IF IS_DEBUG_BUILD
					fThrowSpeed =  (TO_FLOAT(iDartStartPos - iDartEndPos))/(TO_FLOAT(iThrowTimeDiff))
#ENDIF	//	IS_DEBUG_BUILD
					
					DartShotState = DARTS_SHOT_FORWARD
					CDEBUG1LN(DEBUG_DARTS, "Shot made, going to DARTS_SHOT_FORWARD")
				ENDIF
			ENDIF			
		BREAK
		
		CASE DARTS_SHOT_FORWARD
			
			// calculating speed offsets
			CDEBUG1LN(DEBUG_DARTS, "-----------------------------------------------------------")
			CDEBUG1LN(DEBUG_DARTS, "***************fThrowSpeed   = ", fThrowSpeed)			
			CDEBUG1LN(DEBUG_DARTS, "iDartStartPos = ", iDartStartPos, "         iDartEndPos = ", iDartEndPos)
			CDEBUG1LN(DEBUG_DARTS, "iThrowTimeStart = ", iThrowTimeStart, "  iThrowTimeEnd = ", iThrowTimeEnd)
			CDEBUG1LN(DEBUG_DARTS, "Total time = ", (iThrowTimeDiff))
			CDEBUG1LN(DEBUG_DARTS, "-----------------------------------------------------------")
			
			IF iThrowTimeDiff > iGoodThrowTime
				IF iThrowTimeDiff > 100 AND iThrowTimeDiff < 150
					// throw speeds in this range are pretty close to a good speed, and were getting penalized a bit much.
					// in a way it's faking out the drop off, but for now it will work
					fDartSpeedOffset = 0.066477
					Darts.vTarget.z -= fDartSpeedOffset
				ELSE
					fDartSpeedRatio = ((TO_FLOAT(iGoodThrowTime))/(TO_FLOAT(iThrowTimeDiff)))
					fDartSpeedOffset = ((1 - fDartSpeedRatio) * fDartMaxOffset)
					Darts.vTarget.z -= fDartSpeedOffset
				ENDIF
			ELSE
				fDartSpeedOffset = 0
			ENDIF
			
			CDEBUG1LN(DEBUG_DARTS, "Old vTarget.x = ", Darts.vTarget.x)
			// calculating off-center... offsets
			IF iThrowX > 7
				fDartCenterRatio = TO_FLOAT(iThrowX)/fThrowRightLimit
				fDartCenterOffset = fDartMaxCenterOffset * fDartCenterRatio
				Darts.vTarget.x += fDartCenterOffset
				CDEBUG1LN(DEBUG_DARTS, "New vTarget.x = ", Darts.vTarget.x)
			ELIF iThrowX < -7
				fDartCenterRatio = TO_FLOAT(iThrowX)/fThrowLeftLimit
				fDartCenterOffset = fDartMaxCenterOffset * fDartCenterRatio
				Darts.vTarget.x -= fDartCenterOffset
				CDEBUG1LN(DEBUG_DARTS, "New vTarget.x = ", Darts.vTarget.x)				
			ELSE
				fDartCenterOffset = 0
			ENDIF
			
			// Coloring the reticle
			IF fDartSpeedOffset < 0.066477 AND fDartCenterOffset = 0
				DART_RETICLE_GOOD_THROW()
			ELIF fDartSpeedOffset <= 0.066477 AND fDartCenterOffset < 0.06
				DART_RETICLE_MEDIUM_THROW()
			ELSE
				DART_RETICLE_BAD_THROW()
			ENDIF
			
			
			CDEBUG1LN(DEBUG_DARTS, "fDartSpeedOffset = ", fDartSpeedOffset, " fDartCenterOffset = ", fDartCenterOffset)
			
			//Darts.vTarget.x += fDartCenterOffset
						
			bThrowMade = TRUE
			DartShotState = DARTS_SHOT_IDLE
			
		BREAK
	ENDSWITCH
	
	RETURN bThrowMade
ENDFUNC

// PURPOSE: Capture the players stick movement
PROC DO_STICK_MOVEMENT(DART & Darts, DARTS_THROW_STYLE DartsThrowStyle)	
	//BOOL sixAxisEnabled
	FLOAT fTimeSinceLastFrame
	FLOAT fTemp1
	FLOAT fTemp2
	
	fTimeSinceLastFrame = 0.033367 //GET_FRAME_TIME() // can't use frame time since pc has super high frame rates
	
	FLOAT fLeftX, fLeftY//, fLeftX2, fLeftY2
	FLOAT fRightX, fRightY
	BOOL  bMouseActive = FALSE
	
	fLeftX = GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
	fLeftY = GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
	
	fRightX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
	fRightY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
				
	// Handle mouse differently from other controls.
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		// Use keyboard controls if active
		
		IF ABSF(fRightX) > ABSF(fLeftX)
		OR ABSF(fRightY) > ABSF(fLeftY)
			fLeftX = fRightX
			fLeftY = fRightY
			
			bMouseActive = FALSE
			
		ELSE
		
		// Otherwise use mouse
		
			IF IS_MOUSE_LOOK_INVERTED()
				fLeftY *= -1
			ENDIF
				
			IF IS_LOOK_INVERTED()
				fLeftY *= -1
			ENDIF
			
			// Fast move
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB)
				Darts.vTarget.x += fLeftX * 0.06
				Darts.vTarget.z -= fLeftY * 0.06
			ELSE
			// Normal move
				Darts.vTarget.x += fLeftX * 0.03
				Darts.vTarget.z -= fLeftY * 0.03
			ENDIF
			
			Darts.vTarget.x = CLAMP( Darts.vTarget.x, -0.33, 0.33)
			Darts.vTarget.z = CLAMP( Darts.vTarget.z, -0.29, 0.29)
			
			bMouseActive = TRUE
			
		ENDIF
		
	ENDIF
				
	// Normal controls
	IF NOT bMouseActive
	
		// Enable movement using movement keys (e.g. WASD), but only if the mouse isn't being used.
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			
			IF fRightX != 0.0
			OR fRightY != 0.0
				fLeftX = fRightX
				fLeftY = fRightY
			ENDIF
		
		ENDIF
	
		IF fLeftX > 0 //148 //20.0
			IF (Darts.vTarget.x + (fTargetMoveSpeed * fTimeSinceLastFrame)) < 0.33
				Darts.vTarget.x += (fTargetMoveSpeed * fTimeSinceLastFrame)
			ENDIF
		ELSE
			IF fLeftX < 0 //108 //-20.0 
				IF (Darts.vTarget.x - (fTargetMoveSpeed * fTimeSinceLastFrame)) > -0.33
					Darts.vTarget.x -= (fTargetMoveSpeed * fTimeSinceLastFrame)
				ENDIF
			ENDIF
		ENDIF
		
		// Adjust the Z axis based on the player's left stick vertical movement. 
		IF fLeftY > 0 //148 //20.0
			IF (Darts.vTarget.z - (fTargetMoveSpeed * fTimeSinceLastFrame)) > -0.29
				Darts.vTarget.z -= (fTargetMoveSpeed * fTimeSinceLastFrame)
			ENDIF
		ELSE
			IF fLeftY < 0 //108 //-20.0 
				IF (Darts.vTarget.z + (fTargetMoveSpeed * fTimeSinceLastFrame)) < 0.29
					Darts.vTarget.z += (fTargetMoveSpeed * fTimeSinceLastFrame)
				ENDIF
			ENDIF
		ENDIF
	
	
	
	ENDIF
		
	Darts.vRandomTarget = Darts.vTarget	
	
	// calculating the wobbles
	IF DartsThrowStyle = DARTS_STYLE_IV
		// Move the target around randomly on the x axis, the player needs to fight against this.
		fTemp2 = GET_RANDOM_FLOAT_IN_RANGE(0.0 - fRealWobFactor, 0.0 + fRealWobFactor)		
		fTemp1 = GET_RANDOM_FLOAT_IN_RANGE(0.0 - fWobbleFactor, 0.0 + fWobbleFactor)
		
		IF 	(Darts.vTarget.x + (fTemp1 * fTimeSinceLastFrame)) > -0.33 
		AND (Darts.vTarget.x + (fTemp1 * fTimeSinceLastFrame)) < 0.33			
			
			Darts.vTarget.x += (fTemp2 * fTimeSinceLastFrame)
			Darts.vRandomTarget.x = Darts.vTarget.x
			Darts.vRandomTarget.x += (fTemp1 * fTimeSinceLastFrame)
			
		ENDIF

		// Move the target around randomly on the z axis, the player needs to fight against this.
		fTemp2 = GET_RANDOM_FLOAT_IN_RANGE(0.0 - fRealWobFactor, 0.0 + fRealWobFactor)
		fTemp1 = GET_RANDOM_FLOAT_IN_RANGE(0.0 - fWobbleFactor, 0.0 + fWobbleFactor)
		
		IF 	(Darts.vTarget.z + (fTemp1 * fTimeSinceLastFrame)) > -0.29  
		AND (Darts.vTarget.z + (fTemp1 * fTimeSinceLastFrame)) < 0.29
			
			Darts.vTarget.z += (fTemp2 * fTimeSinceLastFrame)
			Darts.vRandomTarget.z = Darts.vTarget.z
			Darts.vRandomTarget.z += (fTemp1 * fTimeSinceLastFrame)
			
		ENDIF
	ENDIF
	
	IF DartsThrowStyle = DARTS_STYLE_STICK
		fTargetMoveSpeed = 0.5
		FLOAT fStickWobbleFactor // = 0.02
	 	FLOAT fStickRealWobFactor // = 0.03
		
		IF DartShotState = DARTS_SHOT_BACK
			fStickWobbleFactor = 0.06
	 		fStickRealWobFactor = 0.08
		ELSE
			fStickWobbleFactor = 0.02
	 		fStickRealWobFactor = 0.03
		ENDIF
	
		// Move the target around randomly on the x axis, the player needs to fight against this.
		fTemp2 = GET_RANDOM_FLOAT_IN_RANGE(0.0 - fStickRealWobFactor, 0.0 + fStickRealWobFactor)		
		fTemp1 = GET_RANDOM_FLOAT_IN_RANGE(0.0 - fStickWobbleFactor, 0.0 + fStickWobbleFactor)
		
		IF 	(Darts.vTarget.x + (fTemp1 * fTimeSinceLastFrame)) > -0.33 
		AND (Darts.vTarget.x + (fTemp1 * fTimeSinceLastFrame)) < 0.33
		
			Darts.vTarget.x += (fTemp2 * fTimeSinceLastFrame)
			Darts.vRandomTarget.x = Darts.vTarget.x
			Darts.vRandomTarget.x += (fTemp1 * fTimeSinceLastFrame)
			
		ENDIF

		// Move the target around randomly on the z axis, the player needs to fight against this.
		fTemp2 = GET_RANDOM_FLOAT_IN_RANGE(0.0 - fStickRealWobFactor, 0.0 + fStickRealWobFactor)
		fTemp1 = GET_RANDOM_FLOAT_IN_RANGE(0.0 - fStickWobbleFactor, 0.0 + fStickWobbleFactor)
		
		IF 	(Darts.vTarget.z + (fTemp1 * fTimeSinceLastFrame)) > -0.33  
		AND (Darts.vTarget.z + (fTemp1 * fTimeSinceLastFrame)) < 0.33
		
			Darts.vTarget.z += (fTemp2 * fTimeSinceLastFrame)
			Darts.vRandomTarget.z = Darts.vTarget.z
			Darts.vRandomTarget.z += (fTemp1 * fTimeSinceLastFrame)
		
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Handles when the target reticle should be zoomed in.  Effect lasts for one second, and only once per throw.
PROC DO_STEADY_SHOT()
	// Speed up the reticule movement
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB)
		fTargetMoveSpeed = 0.3
	ELSE
		fTargetMoveSpeed = 0.1
	ENDIF	
	
	// STEADY SHOT with the right trigger
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
		IF bSteadyShotOn
			IF TIMERA() > iSteadyShotLength
				fWobbleFactor = fScaledWobble
				fRealWobFactor = fScaledRealWobble
				fTargetMoveSpeed = 0.1
				
				//TODO: play sound?
				
				bSteadyShotOn = FALSE
			ENDIF	
		ELSE
			IF iSteadyShotsUsed < 1
				
				fWobbleFactor = 0.03 //0.045 
				fRealWobFactor = 0.05 //0.065 
				fTargetMoveSpeed = 0.1
				
				SETTIMERA(0)
				
				//PLAY_SOUND_FROM_OBJECT(soundExhale, "DARTS_EXHALE", soundObject)

				bSteadyShotOn = TRUE
				iSteadyShotsUsed++ 
			ELSE
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_FST_HLP")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_AIM_HLP")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_CLOCK")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_STD_HLP")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_W")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_B")
				AND NOT bSteadyShotHelp
					bSteadyShotHelp = TRUE
					IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gSTEADY_SHOT_HELP_SHOWN)
						PRINT_HELP("DARTS_SHT_USE")
						SET_BIT(g_iDartsTutorialFlags, gSTEADY_SHOT_HELP_SHOWN)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ELSE
		IF bSteadyShotOn 
			fWobbleFactor = fScaledWobble
			fRealWobFactor = fScaledRealWobble
			fTargetMoveSpeed = 0.1
			
			bSteadyShotOn = FALSE
		ENDIF	
	ENDIF
ENDPROC

PROC UPDATE_RETICLE_POS(DART & Darts, DARTS_BOARD & DartsBoard, BOOL bGameCamInterpolating, DARTS_THROW_STYLE thisThrowStyle = DARTS_STYLE_IV)
	VECTOR vTemp
	
	vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartsBoard.vDartBoard, DartsBoard.fBoardHeading, 
															<< Darts.vRandomTarget.x, 
															Darts.vRandomTarget.y + fdartboardOriginOffset,
										   		   			Darts.vRandomTarget.z >>)
	
	
	IF thisThrowStyle = DARTS_STYLE_STICK
		vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartsBoard.vDartBoard, DartsBoard.fBoardHeading, 
													<< Darts.vRandomTarget.x, 
													Darts.vRandomTarget.y + fdartboardOriginOffset, 
								   		   			Darts.vRandomTarget.z >>)
	ENDIF
	
	GET_SCREEN_COORD_FROM_WORLD_COORD(vTemp, spriteReticle.x, spriteReticle.y)	
	
	// Uncomment to see reticule debug
	// /*
	IF (GET_GAME_TIMER() % 1000) < 50
		CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]%%%%%%%%%%%%% RETICULE POSITION DEBUG %%%%%%%%%%%%%")
		CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]world pos of reticule    = ", vTemp)
		CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]local pos of reticule    = ", Darts.vRandomTarget)
		CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]screen pos X             = ", spriteReticle.x)
		CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]screen pos y             = ", spriteReticle.y)
		CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
	ENDIF
	// */
	
	// Commented out the script hack fix for 4:3 TV's for NG.
	
//	// sometime 4:3 screens get bad values from the above function
//	// if that happens, use these formulas
//	// X formula offset : GoodX = (BadX * A) + B | A = 1.645277, B = 0.043237
//	// Y formula offset : GoodY = (BadY * A) + B | A = 1.496613, B = 0.001075
//	
//	// left extreme = 0.421799, if it's less then that, it's definitely bad data
//	// or if vrandomTarget.x > 0.0, midpoint minimum x is 0.665102
//	
//	IF NOT GET_IS_WIDESCREEN()
//		IF spriteReticle.x < 0.410
//		OR (Darts.vRandomTarget.x > 0.0 AND spriteReticle.x < 0.650)
//			IF (GET_GAME_TIMER() % 1000) < 50
//				CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]%%%%%%%%%%%%% RETICULE POSITION Coorection %%%%%%%%%%%%%")
//				CDEBUG1LN(DEBUG_DARTS, "[DARTAIM] this is bad data!!!")
//				CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]old screen pos X             = ", spriteReticle.x)
//				CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]old screen pos y             = ", spriteReticle.y)
//			ENDIF
//			
//			spriteReticle.x = (spriteReticle.x * 1.645277) + 0.043237
//			spriteReticle.y = (spriteReticle.y * 1.496613) + 0.001075
//			
//			IF (GET_GAME_TIMER() % 1000) < 50
//				CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]new screen pos X             = ", spriteReticle.x)
//				CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]new screen pos y             = ", spriteReticle.y)
//				CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
//			ENDIF
//		ELSE
//			IF (GET_GAME_TIMER() % 1000) < 50
//				CDEBUG1LN(DEBUG_DARTS, "[DARTAIM] position looks good")
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF NOT bGameCamInterpolating
		IF bSteadyShotOn
			DRAW_2D_SPRITE("Darts", "Dart_Reticules_Zoomed", spriteReticle, TRUE, HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_AFTER_HUD,FALSE)
		ELSE
			DRAW_2D_SPRITE("Darts", "Dart_Reticules", spriteReticle, TRUE, HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_AFTER_HUD,FALSE)
		ENDIF
		
		IF NOT bReticleDrawn
			bReticleDrawn = TRUE
		ENDIF
	ENDIF
ENDPROC


// PURPOSE: Initialise the reticle and the dart
PROC DO_RETICLE_INIT(DART & Darts, DARTS_STARTING_RETICLE_POS InitRetPos)//, DARTS_BOARD & DartsBoard)
	VECTOR vTemp
	
	// create the target at a random point from the center of the board
	vTemp 	= <<0,0,0>>//<< 0.0, -0.1, 0.0 >>
	vTemp.x += GET_RANDOM_FLOAT_IN_RANGE(-0.1, 0.1)	
	vTemp.z += GET_RANDOM_FLOAT_IN_RANGE(-0.1, 0.1)
	
	// create the target object 	
	Darts.vTarget = InitRetPos.vInitReticlePosOffset + vTemp
	
	CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]Initial reticule position = ", Darts.vTarget)
	
	Darts.vPosition =  vInitDartPosOffset // keep track of the dart's offset
	
ENDPROC

FUNC BOOL RELEASE_THE_DART(DART & Darts, DARTS_BOARD & DartsBoard, BOOL bStickShot = FALSE)
	VECTOR vTemp, vTemp2, vTemp3
	
	// create the dart
	IF DOES_ENTITY_EXIST(Darts.object)
		SET_ENTITY_COORDS(Darts.object, vInitDartPos)
	ELSE
		Darts.object = CREATE_OBJECT(Darts.modelDart, vInitDartPos, FALSE, FALSE)
	ENDIF
	
	// Reset steady shot targetting variables
	bSteadyShotOn 		= FALSE
	
	//iSteadyShotsUsed 	= 0
	fTargetMoveSpeed 	= 0.1
	fWobbleFactor 		= fScaledWobble
	fRealWobFactor 		= fScaledRealWobble
				
	Darts.bStuck 		= FALSE
	Darts.bTravelling 	= TRUE
		
	SET_DART_THROW_VECTOR (Darts)	
	
	FLOAT fTemp1, fTemp2, fTemp3
	fTemp2 = GET_RANDOM_FLOAT_IN_RANGE(0.0, 90.0)
		
	vTemp2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartsBoard.vDartBoard, DartsBoard.fBoardHeading, 
															vInitDartPosOffset)
	
	vTemp3 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartsBoard.vDartBoard, DartsBoard.fBoardHeading, 
															<< Darts.vRandomTarget.x + 0.0035, 
															Darts.vRandomTarget.y + fdartboardOriginOffset,
										   		   			Darts.vRandomTarget.z >>)
	
	fTemp3 = GET_HEADING_FROM_VECTOR_2D((vTemp2.x - vTemp3.x), (vTemp2.y - vTemp3.y))
	fTemp3 -= 180
	
	fTemp1 = 90
	
	// rotation.x should be 90, but we angle it down to make it look more natural
	//Darts.vRotation = << 87.5, fTemp1, DartsBoard.fBoardHeading >>
	Darts.vRotation = << fTemp1, fTemp2, fTemp3>>
	SET_ENTITY_ROTATION(Darts.object, Darts.vRotation, EULER_XYZ)
	
	CDEBUG1LN(DEBUG_DARTS, "Initial Rotation Vector for Dart = ", Darts.vRotation)
	
	bCrossButtonReleased 	= FALSE												 
	bInitReticle 			= FALSE
	
	IF bStickShot
		DRAW_2D_SPRITE("Darts", "Dart_Reticules", spriteReticle, TRUE, HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_AFTER_HUD,FALSE)
	ENDIF
	
	vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
															<< Darts.vTarget.x, 
															Darts.vTarget.y + fdartboardOriginOffset, 
										   		   			Darts.vTarget.z >>)												
	
	GET_SCREEN_COORD_FROM_WORLD_COORD(vTemp, spriteDartLanding.x, spriteDartLanding.y)	
	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DO_AIMING(DART & Darts, DARTS_BOARD & DartsBoard, DARTS_STARTING_RETICLE_POS & InitRetPos, BOOL bGameCamInterpolating, 
					BOOL bShotClockEnded=FALSE, BOOL bThrowOff = FALSE, DARTS_THROW_STYLE DartsThrowStyle=DARTS_STYLE_IV)
	
	SWITCH DartsThrowStyle
		//Old button based throw
		CASE DARTS_STYLE_IV
			IF bWaitThrow
				IF TIMERB() > 50
					bCrossButtonReleased = FALSE
					bNoCrossConflict = FALSE
					bReticleDrawn = FALSE
					
					//transits to stage throw
					IF RELEASE_THE_DART(Darts, DartsBoard)
						PLAY_SOUND_FROM_ENTITY(-1, "DARTS_THROW_DART_MASTER", Darts.object)
						bWaitThrow = FALSE
						RETURN TRUE						
					ENDIF		
				ENDIF
			ELSE
				IF NOT bGameCamInterpolating
					IF NOT (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN) AND bReticleDrawn)
					AND NOT bShotClockEnded
				
						bCrossButtonReleased = TRUE
						bNoCrossConflict = TRUE
						
						IF NOT bInitReticle
							DO_RETICLE_INIT(Darts, InitRetPos)
							
							CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]DartsBoard.vDartBoard    = ", DartsBoard.vDartBoard)
							CDEBUG1LN(DEBUG_DARTS, "[DARTAIM]DartsBoard.fBoardHeading = ", DartsBoard.fBoardHeading)
							
							bInitReticle = TRUE	
						ELSE
							
							IF NOT bThrowOff
								DO_STEADY_SHOT()
							ENDIF
							DO_STICK_MOVEMENT(Darts, DartsThrowStyle)
							UPDATE_RETICLE_POS(Darts, DartsBoard, bGameCamInterpolating)
						ENDIF
					ELSE
						
						// offset the shot if they ran out of time
						IF bShotClockEnded
							FLOAT fTempOffset
							INT iOffsetDecider
							
							fTempOffset = GET_RANDOM_FLOAT_IN_RANGE(0.02, 0.1)
							iOffsetDecider = GET_RANDOM_INT_IN_RANGE(0, 100)
							CDEBUG1LN(DEBUG_DARTS, "1st offset.  fTempOffset = ", fTempOffset, ".  iOffsetDecider = ", iOffsetDecider)
							
							IF iOffsetDecider < 50
								IF (Darts.vTarget.x + fTempOffset) < 0.33
									Darts.vTarget.x += fTempOffset
								ELSE
									Darts.vTarget.x -= fTempOffset
								ENDIF
							ELSE
								IF (Darts.vTarget.x - fTempOffset) > -0.33
									Darts.vTarget.x -= fTempOffset
								ELSE
									Darts.vTarget.x += fTempOffset
								ENDIF
							ENDIF
							
							fTempOffset = GET_RANDOM_FLOAT_IN_RANGE(0.02, 0.1)
							iOffsetDecider = GET_RANDOM_INT_IN_RANGE(0, 100)
							CDEBUG1LN(DEBUG_DARTS, "2nd offset.  fTempOffset = ", fTempOffset, ".  iOffsetDecider = ", iOffsetDecider)
							
							IF iOffsetDecider < 50
								IF (Darts.vTarget.z + fTempOffset) < 0.29
									Darts.vTarget.z += fTempOffset
								ELSE
									Darts.vTarget.z -= fTempOffset
								ENDIF
							ELSE
								IF (Darts.vTarget.z - fTempOffset) > -0.29
									Darts.vTarget.z -= fTempOffset
								ELSE
									Darts.vTarget.z += fTempOffset
								ENDIF
							ENDIF
							
							Darts.vPosition =  vInitDartPosOffset
							
							bCrossButtonReleased = TRUE
							bNoCrossConflict = TRUE
						ENDIF
						
						IF bCrossButtonReleased AND bNoCrossConflict
							IF NOT bWaitThrow
								CLEAR_PRINTS()
								
								bWaitThrow = TRUE
								SETTIMERB(0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//Shiny new shot stick throw
		CASE DARTS_STYLE_STICK
			IF bWaitThrow
				DRAW_2D_SPRITE("Darts", "Dart_Reticules", spriteReticle, TRUE, HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_AFTER_HUD,FALSE)
				IF TIMERB() > 25
					//transits to stage throw
					
					CDEBUG1LN(DEBUG_DARTS, "At the end of DO_AIMING, Dart.vTarget.x = ", Darts.vTarget.x)
					
					IF RELEASE_THE_DART(Darts, DartsBoard, TRUE)
						PLAY_SOUND_FROM_ENTITY(-1, "DARTS_THROW_DART_MASTER", Darts.object)
						bWaitThrow = FALSE
						RETURN TRUE 
					ENDIF		
				ENDIF
			ELSE
				IF NOT DO_STICK_SHOT(Darts)					
					
					IF NOT bInitReticle
						DO_RETICLE_INIT(Darts, InitRetPos)
					ELSE
						
						DO_STEADY_SHOT()
						DO_STICK_MOVEMENT(Darts, DartsThrowStyle)
						UPDATE_RETICLE_POS(Darts, DartsBoard, bGameCamInterpolating)
						
					ENDIF
				ELSE
					IF NOT bWaitThrow					
						CDEBUG1LN(DEBUG_DARTS, "Just before the the Spice, Dart.vTarget.x = ", Darts.vTarget.x)
					//	DART_SHOT_SPICE(Darts) 
						
						bWaitThrow = TRUE
						SETTIMERB(0)
					ENDIF					
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SKIP_AI_TURN_CHECK(DARTS_PLAYER_STATE & turnStage)

	// check to see if the x button has been released
	IF NOT bNoCrossConflict
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			bNoCrossConflict = TRUE
		ENDIF
	ENDIF
			
	// check to see if the player skips
	IF iPlayr = 1 AND iTurn < 3 AND turnStage <> DPS_TURN_CHANGE
		IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)) 
		AND bNoCrossConflict
						
			// Store where the turn was at... and move us to skip state
			turnStage = DPS_SKIP_AI
		ENDIF
	ENDIF	
ENDPROC 
