USING "Darts_Stats.sch"
USING "globals.sch"

PROC DARTS_STATS_DEBUG(STRING sStatus, INT iNum)
	CDEBUG1LN(DEBUG_DARTS, "DARTS_STATS_DEBUG - ", sStatus, " is now ", iNum)
ENDPROC

PROC DARTS_STATS_UPDATE(DARTS_STATS_INFO dartStat)//, INT iNewStatValue = 0)
	SWITCH dartStat
		CASE DARTS_STAT_NUM_BULLSEYES
			g_savedGlobals.sDartsData.iNumBullseyes++
			DARTS_STATS_DEBUG("DARTS_STAT_NUM_BULLSEYES", g_savedGlobals.sDartsData.iNumBullseyes)
		BREAK
		CASE DARTS_STAT_NUM_180S
			g_savedGlobals.sDartsData.iNumOneEighties++
			DARTS_STATS_DEBUG("DARTS_STAT_NUM_180S", g_savedGlobals.sDartsData.iNumOneEighties)
		BREAK
		CASE DARTS_STAT_NUM_WINS
			g_savedGlobals.sDartsData.iDartGamesWon++
			DARTS_STATS_DEBUG("DARTS_STAT_NUM_WINS", g_savedGlobals.sDartsData.iDartGamesWon)
		BREAK
		CASE DARTS_STAT_NUM_LOSS
			g_savedGlobals.sDartsData.iDartGamesLoss++
			DARTS_STATS_DEBUG("DARTS_STAT_NUM_LOSS", g_savedGlobals.sDartsData.iDartGamesLoss)
		BREAK
		CASE DARTS_STAT_DARTS_THROWN
			g_savedGlobals.sDartsData.iDartsThrown++
			DARTS_STATS_DEBUG("DARTS_STAT_DARTS_THROWN", g_savedGlobals.sDartsData.iDartsThrown)
		BREAK
		CASE DARTS_STAT_NUM_GAMES
			g_savedGlobals.sDartsData.iTotalDartGames++
			DARTS_STATS_DEBUG("DARTS_STAT_NUM_GAMES", g_savedGlobals.sDartsData.iTotalDartGames)
		BREAK
//		CASE DARTS_STAT_TIME_PLAYED
//			g_savedGlobals.sDartsData.iDartTimePlayed += iNewStatValue
//			DARTS_STATS_DEBUG("DARTS_STAT_TIME_PLAYED", g_savedGlobals.sDartsData.iDartTimePlayed)
//		BREAK
		CASE DARTS_STAT_WIN_PCT
			g_savedGlobals.sDartsData.fWinPct = TO_FLOAT(g_savedGlobals.sDartsData.iDartGamesWon)/TO_FLOAT(g_savedGlobals.sDartsData.iTotalDartGames)
			CDEBUG1LN(DEBUG_DARTS, "DARTS_STATS_DEBUG - DARTS_STAT_WIN_PCT is now ", g_savedGlobals.sDartsData.fWinPct)
		BREAK
		CASE DARTS_STAT_AVG_DARTS_MATCH
			g_savedGlobals.sDartsData.fAvgDartsPerMatch = TO_FLOAT(g_savedGlobals.sDartsData.iDartsThrown)/TO_FLOAT(g_savedGlobals.sDartsData.iTotalDartGames)
			CDEBUG1LN(DEBUG_DARTS, "DARTS_STATS_DEBUG - DARTS_STAT_AVG_DARTS_MATCH is now ", g_savedGlobals.sDartsData.fAvgDartsPerMatch)
		BREAK
		
	ENDSWITCH
ENDPROC
