////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_UI.sch                            //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Darts UI Defs       			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "minigame_uiinputs.sch"
USING "minigame_big_message.sch"
USING "minigame_midsized_message.sch"
USING "script_usecontext.sch"
USING "mp_globals_block_SC_LB.sch" 

ENUM DARTS_UI_RETVAL
	DARTS_UIRETVAL_NONE,
	DARTS_UIRETVAL_TRUE,
	DARTS_UIRETVAL_FALSE
ENDENUM

ENUM DARTS_UI_FLAG
	DARTS_UIFLAGS_TURN_CONTINUE = 0,
	DARTS_UIFLAGS_SWITCH_PLAYER,
	DARTS_UIFLAGS_BUST_UNDER_ZERO,
	DARTS_UIFLAGS_BUST_NOT_DOUBLE,
	DARTS_UIFLAGS_BUST_ONE_POINT_LEFT,
	DARTS_UIFLAGS_GAME_WON,
	DARTS_UIFLAGS_IN_WIN_RANGE,
	DARTS_UIFLAGS_QUIT_MENU,
	DARTS_UIFLAGS_SETUP_QUITUI,
	DARTS_UIFLAGS_SETUP_ENDUI,
	DARTS_UIFLAGS_SETUP_STARTUI,
	DARTS_UIFLAGS_UPDATE_PRINTED,
	DARTS_UIFLAGS_FIRST_THROW,
	DARTS_UIFLAGS_SHOT_CLOCK_HELP,
	DARTS_UIFLAGS_SHOW_LEADERBOARD,
	DARTS_UIFLAGS_SETUP_INGAME,
	DARTS_UIFLAGS_SETUP_INGAME_OPP,
	DARTS_UIFLAGS_STEADY_USED_SETUP,
	DARTS_UIFLAGS_END_HELP_READY,
	DARTS_UIFLAGS_BUST_HELP_READY,
	DARTS_UIFLAGS_BUST_HELP,
	DARTS_UIFLAGS_WIN_HELP,
	DARTS_UIFLAGS_SHOT_CLECK_HELP,
	DARTS_UIFLAGS_ZOOM_SETUP,
	DARTS_UIFLAGS_SHOW_LEVEL_UP,
	DARTS_UIFLAGS_NUM_FLAGS		//24
ENDENUM

ENUM DARTS_SPECIAL_SPLASH
	DARTSSPLASH_180,
	DARTSSPLASH_BULLSEYE
ENDENUM

ENUM DARTS_SPLASH_STAGE
	DARTSSPLASHSTAGE_INIT,
	DARTSSPLASHSTAGE_DISPLAY,
	DARTSSPLASHSTAGE_DONE
ENDENUM

STRUCT DARTS_SCOREBOARD
	
	SCALEFORM_INDEX		siScoreBoard
	
	INT					iChalkScores [2][7]
	
	VECTOR 				vScoreBoardPos
	VECTOR 				vScoreBoardRot
	VECTOR 				vScoreBoardScale
	
	// placeholder values for 2d scaleform drawing
	FLOAT 				fScoreboardPosX
	FLOAT 				fScoreboardPosY
	FLOAT 				fScoreboardSizeX
	FLOAT 				fScoreboardSizeY
ENDSTRUCT

STRUCT DARTS_LEG_TRACKER
	INT 	iSlot 
	INT 	iSets
	INT 	iLegs
	BOOL 	bHighlight
	STRING 	sName
ENDSTRUCT

STRUCT DARTS_UI
	
	SCRIPT_SCALEFORM_UI	uiQuitControls
	SCRIPT_SCALEFORM_UI	uiEndGameControls
	
	SCRIPT_SCALEFORM_SPLASH uiSplashMessage
	SCRIPT_SCALEFORM_BIG_MESSAGE siBigMessage
	SCRIPT_SHARD_BIG_MESSAGE siMidMessage
	
	SC_LEADERBOARD_CONTROL_STRUCT dartsLB_control
	
	SIMPLE_USE_CONTEXT inGameControlContext
	
	structTimer	scrollTimerLeft
	structTimer	scrollTimerRight
	structTimer	scrollTimerUp
	structTimer	scrollTimerDown
	
	SCALEFORM_INDEX	uiLeaderboard
	BOOL 			bLBDOnlineWarningDone
	INT				iLBDWarningBitset
	
	TEXT_LABEL_23 sMyName
	TEXT_LABEL_23 sTheirName
	
	INT 	iMenuSelection
	INT		iMenuLength
	INT		iLegSelection
	INT		iLegLength
	INT		iSetSelection
	INT		iSetLength
	INT 	iFlags
	INT 	iHelpTime
	INT 	iOurScore
	INT 	iTheirScore
	INT		iDartsLeft
	INT 	iEndDelay
	
	BOOL	bInstructTextTransIn
	BOOL	bStickTiltUp
	BOOL	bStickTiltDown
	BOOL	bStickTiltLeft
	BOOL	bStickTiltRight
	BOOL	bLeaderboardUpdated
	BOOL	bEndscreenSetup
	BOOL 	bTriggerSplash
	
	BOOL	bDrawingLeaderboard = FALSE
		
	DARTS_SPLASH_STAGE eSplashStage = DARTSSPLASHSTAGE_DONE
	DARTS_SPECIAL_SPLASH eWhichSplash
ENDSTRUCT

BOOL bUITextDisplayed

FUNC STRING DARTS_GET_SPLASH_STRING(DARTS_SPECIAL_SPLASH eWhichSplash)
	SWITCH eWhichSplash
		CASE DARTSSPLASH_180		RETURN "DARTS_180_SPLSH"
		CASE DARTSSPLASH_BULLSEYE	RETURN "DARTS_BE_SPLSH"
		DEFAULT 					RETURN "DARTS_BE_SPLSH"
	ENDSWITCH
ENDFUNC

// Set a UI flag.
PROC DARTS_SET_UI_FLAG(INT & iFlags, DARTS_UI_FLAG uiFlag, BOOL bSet)
	IF bSet
		SET_BIT(iFlags, ENUM_TO_INT(uiFlag))
	ELSE
		CLEAR_BIT(iFlags, ENUM_TO_INT(uiFlag))
	ENDIF
ENDPROC

// Checks to see if a flag is set.
FUNC BOOL DARTS_GET_UI_FLAG(INT & iFlags, DARTS_UI_FLAG uiFlag)
	RETURN IS_BIT_SET(iFlags, ENUM_TO_INT(uiFlag))
ENDFUNC
