////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    MP_Darts_Client_lib.sch                 //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Dart Client Procs and Funcs		      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "MP_Darts_Client.sch"
USING "MP_Darts_Server.sch"
USING "minigame_uiinputs.sch"


// Before throwing dart, we have to clone certain characteristics, namely:
// Darts.vPosition, Darts.vThrow, Darts.vTarget, Darts.bStuck, Darts.bTravelling
// Darts.vRotation, Darts.vOffsetTarget
PROC COPY_DART_TO_CLIENT (DARTS_PLAYER_BROADCAST_DATA & thisPlayerData, DART thrownDart)
	thisPlayerData.ClientDart.vPosition 	= thrownDart.vPosition
	thisPlayerData.ClientDart.vThrow 		= thrownDart.vThrow
	thisPlayerData.ClientDart.vTarget 		= thrownDart.vTarget
	thisPlayerData.ClientDart.vRotation 	= thrownDart.vRotation
	thisPlayerData.ClientDart.vOffsetTarget = thrownDart.vOffsetTarget
	thisPlayerData.ClientDart.bStuck 		= thrownDart.bStuck
	thisPlayerData.ClientDart.bTravelling 	= thrownDart.bTravelling
	//thisPlayerData.ClientDart.modelDart 	= thrownDart.modelDart
ENDPROC

PROC RESET_CLIENT_DART (DARTS_PLAYER_BROADCAST_DATA & thisPlayerData)
	thisPlayerData.ClientDart.vPosition 	= << 0.0, -2.3685, -0.2 >>
	thisPlayerData.ClientDart.vThrow 		= << 0.0, 0.0, 0.0 >>
	thisPlayerData.ClientDart.vTarget 		= << 0.0, 0.0, 0.0 >>
	
	thisPlayerData.ClientDart.bStuck 		= FALSE
	thisPlayerData.ClientDart.bTravelling 	= FALSE
	
	thisPlayerData.ClientDart.iHitValue 	= 0
	
	IF DOES_ENTITY_EXIST(thisPlayerData.ClientDart.object)
		DELETE_OBJECT(thisPlayerData.ClientDart.object)
	ENDIF
ENDPROC

// If the server has thrown any flags, handle them.
FUNC BOOL DARTS_PLAYER_PROCESS_SERVER_FLAGS(DARTS_SERVER_BROADCAST_DATA& serverData, DARTS_PLAYER_BROADCAST_DATA& playerData, DARTS_TIMER & DartsMenuTimer)
	IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_QUIT_1CLIENT)
	AND playerData.eMPState <> DARTS_MPSTATE_TERMINATE_SPLASH
	AND playerData.eMPState <> DARTS_MPSTATE_LEAVE
	AND playerData.eMPState <> DARTS_MPSTATE_END
		PRINTLN("[MP DARTS] Client Lib saw server flag to quit")
		
		RETURN TRUE
		
		CLEAR_HELP()
		playerData.eMPState = DARTS_MPSTATE_TERMINATE_SPLASH
		RESTART_TIMER_NOW(DartsMenuTimer.stMenuClock)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_QUIT_1CLIENT)
		
		// Set the minigame instructions.
		//SETUP_MINIGAME_INSTRUCTIONS(airUI.uiControls, ICON_UP, "", 1, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "FE_HLP32")
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Get opponent's reticle info from broadcast data
PROC DARTS_UPDATE_OPPONENT_RETICLE (VECTOR vReticle, DARTS_PLAYER_BROADCAST_DATA & opponentData)
	VECTOR vOldPos = vReticle
	VECTOR vNewPos = opponentData.vReticlePos
	
	IF NOT ARE_VECTORS_EQUAL(vOldPos, vNewPos)
		vReticle = opponentData.vReticlePos
	ENDIF
ENDPROC
