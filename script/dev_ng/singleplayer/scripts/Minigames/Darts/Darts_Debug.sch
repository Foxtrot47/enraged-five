////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Debug.sch                         //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Darts Debug Funcs      			      //
//                                                           //
//////////////////////////////////////////////////////////////


#IF IS_DEBUG_BUILD
USING "shared_debug.sch"
USING "script_debug.sch"
#ENDIF


STRUCT DEBUG_POS_DATA
	
	BOOL bDebugWobbles
	FLOAT fDebugWobbleFactor
	FLOAT fDebugRealWobbleFactor
	
	BOOL bPlaySFX
	BOOL bUseSoundSet
	STRING sSFX
	STRING sSoundSet
	TEXT_WIDGET_ID twiSFX
	TEXT_WIDGET_ID twiSoundSet
	
	BOOL bPlayAudio
	BOOL bPlayerOrOpponentAudio
	STRING sAudioName
	TEXT_WIDGET_ID twiAudioName
	STRING sAudioVoice
	TEXT_WIDGET_ID twiAudioVoice
	
	// Stat Debug
	BOOL bShowStats
	
	// Camera debug
	BOOL bSwitchCam
	
	//-----Debug for ScoreBoard
	BOOL bTurnOnScoreBoardDebug
	BOOL bTurnOnScoreBoardDebugPos
	BOOL bTurnOnScoreBoardDebug2DPos
	VECTOR vScoreBoardPos
	VECTOR vScoreBoardRotation
	VECTOR vScoreBoardSize
	FLOAT fScoreBoardPosX
	FLOAT fScoreBoardPosY
	FLOAT fScoreBoardRotateX
	FLOAT fScoreBoardRotateY
	FLOAT fScoreBoardRotateZ
	FLOAT fScoreBoardSizeX
	FLOAT fScoreBoardSizeY
	
	INT iScoreBoardPoints
	BOOL bAddScoreToPlayerOne
	BOOL bAddScoreToPlayerTwo
	
	STRING scoreBoardName1
	STRING scoreBoardName2
	TEXT_WIDGET_ID twiScoreboardName1
	TEXT_WIDGET_ID twiScoreboardName2
	BOOL bAddNames
	
	BOOL bClearScores1
	BOOL bClearScores2
	BOOL bClearBoard
		
	//-----Debug for button or shot stick throws
	BOOL bDebugThrowStyle
	BOOL bThrowStyleStick
	
	//-----Debug for busts
	BOOL bDebugBusts
	
	//-----Debug for sprite reticle
	BOOL bDrawSprite
	FLOAT fSpriteX
	FLOAT fSpriteY
	FLOAT fSpriteW
	FLOAT fSpriteH
	INT iSpriteR
	INT iSpriteG
	INT iSpriteB
	
	BOOL bDrawZoomedSprite
	
	//-----Debug for Reticle Position
	BOOL bTurnOnRetDebugPos
	VECTOR vDebugRetVector	
	FLOAT fRotateRetX
	FLOAT fRotateRetY
	FLOAT fRotateRetZ
	
	//-----Debug for Dart Rotation
	BOOL bTurnOnDartThrowDebug
	BOOL bTurnOnDartRotDebug
	FLOAT fScoreValue
	FLOAT fScoreMult
	INT iDartNum
	FLOAT fBoardHeading
	FLOAT fRotateDartX
	FLOAT fRotateDartY
	FLOAT fRotateDartZ
	FLOAT fLength
	FLOAT fDartAngle
	
	//-----Debug for Dart Rotation
	BOOL bTurnOnDartLine
	
	BOOL bTurnOnReticuleDebug
	FLOAT fRetX
	FLOAT fRetZ
	
	CAMERA_INDEX camTemp
	VECTOR vRealRot
	FLOAT fDartHeading
ENDSTRUCT

PROC ADD_WIDGETS(DEBUG_POS_DATA& myData)
    UNUSED_PARAMETER(myData)
	#IF IS_DEBUG_BUILD
      	START_WIDGET_GROUP("Darts")
			
			ADD_WIDGET_BOOL("Debug Wobbles", myData.bDebugWobbles)
			ADD_WIDGET_FLOAT_SLIDER("Wobble", myData.fDebugWobbleFactor, 0.0, 0.15, 0.01)
			ADD_WIDGET_FLOAT_READ_ONLY("Real Wobble", myData.fDebugRealWobbleFactor)
			
			ADD_WIDGET_FLOAT_READ_ONLY("Actual Wobble", fScaledWobble)
			ADD_WIDGET_FLOAT_READ_ONLY("Actual Real Wobble", fScaledRealWobble)
			
			ADD_WIDGET_BOOL("Play SFX", myData.bPlaySFX)
			ADD_WIDGET_BOOL("Use Soundset", myData.bUseSoundSet)
			myData.twiSFX = ADD_TEXT_WIDGET("SFX Name")
			myData.twiSoundSet = ADD_TEXT_WIDGET("SoundSet")
			
			ADD_WIDGET_STRING("Playing ambient audio")
			ADD_WIDGET_BOOL("Play Audio", myData.bPlayAudio)
			ADD_WIDGET_BOOL("Player Voice?", myData.bPlayerOrOpponentAudio)
			myData.twiAudioVoice = ADD_TEXT_WIDGET("Darts Audio voice")
			myData.twiAudioName = ADD_TEXT_WIDGET("Darts Audio name")
			
			ADD_WIDGET_STRING("Stat Debug")
			ADD_WIDGET_BOOL("Debug Stats", myData.bShowStats)
			
			ADD_WIDGET_BOOL("Switch Cam", myData.bSwitchCam)
			
			//Score zone debug
			ADD_WIDGET_BOOL("Dart Line", myData.bTurnOnDartLine)
			ADD_WIDGET_BOOL("Dart Throw Debug", myData.bTurnOnDartThrowDebug)
			ADD_WIDGET_BOOL("Reticule Debug", myData.bTurnOnReticuleDebug)
			ADD_WIDGET_FLOAT_SLIDER("Reticule Rotate X", myData.fRetX, -0.3, 0.3, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("Reticule Rotate Z", myData.fRetZ, -0.3, 0.3, 0.0001)
			
			//Debug for Scoreboard Scaleform 
			ADD_WIDGET_BOOL("Debug ScoreBoard", myData.bTurnOnScoreBoardDebug)
			
			ADD_WIDGET_BOOL("Debug ScoreBoard 3D Pos", myData.bTurnOnScoreBoardDebugPos)
			ADD_WIDGET_VECTOR_SLIDER("Position", myData.vScoreBoardPos, -4000, 4000, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Rotate X", myData.fScoreBoardRotateX, 0, 360, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Rotate Y", myData.fScoreBoardRotateY, 0, 360, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Rotate Z", myData.fScoreBoardRotateZ, 0, 360, 0.5)
			
			ADD_WIDGET_BOOL("Debug ScoreBoard 2D Pos", myData.bTurnOnScoreBoardDebug2DPos)
			ADD_WIDGET_FLOAT_SLIDER("ScoreBoard Pos X", myData.fScoreBoardPosX, 0.0, 1.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("ScoreBoard Pos Y", myData.fScoreBoardPosY, 0.0, 1.0, 0.05)
			
			ADD_WIDGET_STRING("Scoreboard Size")
			ADD_WIDGET_FLOAT_SLIDER("Size X", myData.fScoreBoardSizeX, 0, 5, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Size Y", myData.fScoreBoardSizeY, 0, 5, 0.01)
			
			ADD_WIDGET_STRING("Scoreboard Names")
			myData.twiScoreboardName1 = ADD_TEXT_WIDGET("Player Name 1")
			myData.twiScoreboardName2 = ADD_TEXT_WIDGET("Player Name 2")
			ADD_WIDGET_BOOL("Add names", myData.bAddNames)
			
			ADD_WIDGET_STRING("Scoreboard Scores")
			ADD_WIDGET_INT_SLIDER("Score", myData.iScoreBoardPoints, 0, 301, 1)
			ADD_WIDGET_BOOL("Add score to player one", myData.bAddScoreToPlayerOne)
			ADD_WIDGET_BOOL("Add score to player two", myData.bAddScoreToPlayerTwo)
			
			ADD_WIDGET_STRING("Scoreboard Clears")
			ADD_WIDGET_BOOL("Clear Scores 1", myData.bClearScores1)
			ADD_WIDGET_BOOL("Clear Scores 2", myData.bClearScores2)
			ADD_WIDGET_BOOL("Clear All", myData.bClearBoard)
			
			//Debug for busts
			ADD_WIDGET_BOOL("Debug Busts", myData.bDebugBusts)
			
			//Debug for sprite
			ADD_WIDGET_STRING("Aiming Reticule")
			ADD_WIDGET_BOOL("Debug sprite", myData.bDrawSprite)
			ADD_WIDGET_FLOAT_SLIDER("SpritePosX", myData.fSpriteX, 0, 1, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("SpritePosY", myData.fSpriteY, 0, 1, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("SpriteW", myData.fSpriteW, 0, 1, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("SpriteH", myData.fSpriteH, 0, 1, 0.01)
			ADD_WIDGET_INT_SLIDER("SpriteR", myData.iSpriteR, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("SpriteG", myData.iSpriteG, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("SpriteB", myData.iSpriteB, 0, 255, 1)
			ADD_WIDGET_BOOL("Debug zoomed", myData.bDrawZoomedSprite)
			
			//Debug for reticle positioning
			ADD_WIDGET_BOOL("Turn On Reticle Positioning", myData.bTurnOnRetDebugPos)
			ADD_WIDGET_VECTOR_SLIDER("DebugRetPosition", myData.vDebugRetVector, -2000, 2000, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("RotateRetX", myData.fRotateRetX, 0, 360, 1)
            ADD_WIDGET_FLOAT_SLIDER("RotateRetY", myData.fRotateRetY, 0, 360, 1)
            ADD_WIDGET_FLOAT_SLIDER("RotateRetZ", myData.fRotateRetZ, 0, 360, 1)
			
			//Debug for throw styles
			ADD_WIDGET_BOOL("Debug Throw Style. Default: Stick Throw", myData.bDebugThrowStyle)			
			ADD_WIDGET_BOOL("GTA IV Style", myData.bThrowStyleStick)
			
			//Dart Debug 
			//START_WIDGET_GROUP("Dart Rotation")
				ADD_WIDGET_BOOL("Rotate Dart", myData.bTurnOnDartRotDebug)
				ADD_WIDGET_INT_SLIDER("Dart Number", myData.iDartNum, 0, 2, 1)
				ADD_WIDGET_FLOAT_SLIDER("Dart Heading", myData.fBoardHeading, -180, 180, 0.5)
				ADD_WIDGET_FLOAT_SLIDER("RotateDartX", myData.fRotateDartX, -180, 180, 0.5)
	            ADD_WIDGET_FLOAT_SLIDER("RotateDartY", myData.fRotateDartY, -180, 180, 0.5)
	            ADD_WIDGET_FLOAT_SLIDER("RotateDartZ", myData.fRotateDartZ, -180, 180, 0.5)
			//STOP_WIDGET_GROUP()
      	STOP_WIDGET_GROUP()
		
		
	#ENDIF
ENDPROC

	#IF IS_DEBUG_BUILD
PROC DO_DEBUG_PROCS(DEBUG_POS_DATA & DebugBoard, DART_GAME & DartGame, SPRITE_PLACEMENT spriteDebug, SCALEFORM_INDEX & siScoreBoardDebug, PED_INDEX & piDartPlayers[])
	
	IF DebugBoard.bDebugWobbles
		fWobbleFactor = DebugBoard.fDebugWobbleFactor
		DebugBoard.fDebugRealWobbleFactor = (DebugBoard.fDebugWobbleFactor / 3) + DebugBoard.fDebugWobbleFactor
		fRealWobFactor = DebugBoard.fDebugRealWobbleFactor
	ENDIF
	
	IF DebugBoard.bPlaySFX
		DebugBoard.sSFX = GET_CONTENTS_OF_TEXT_WIDGET(DebugBoard.twiSFX)
		DebugBoard.sSoundSet = GET_CONTENTS_OF_TEXT_WIDGET(DebugBoard.twiSoundSet)
		
		IF DebugBoard.bUseSoundSet
			PLAY_SOUND_FRONTEND(-1, DebugBoard.sSFX, DebugBoard.sSoundSet)
		ELSE
			PLAY_SOUND_FRONTEND(-1, DebugBoard.sSFX)
		ENDIF
		DebugBoard.bPlaySFX = FALSE
	ENDIF
	
	IF DebugBoard.bPlayAudio
		DebugBoard.sAudioName = GET_CONTENTS_OF_TEXT_WIDGET(DebugBoard.twiAudioName)
		DebugBoard.sAudioVoice = GET_CONTENTS_OF_TEXT_WIDGET(DebugBoard.twiAudioVoice)
		
		INT iTemp = PICK_INT(DebugBoard.bPlayerOrOpponentAudio, 0, 1)
		
		IF NOT IS_PED_INJURED(piDartPlayers[iTemp])
			
			IF IS_STRING_NULL(DebugBoard.sAudioVoice)
			OR DebugBoard.bPlayerOrOpponentAudio
				PLAY_PED_AMBIENT_SPEECH(piDartPlayers[iTemp], DebugBoard.sAudioName, SPEECH_PARAMS_FORCE_FRONTEND)
				CDEBUG1LN(DEBUG_DARTS, DebugBoard.sAudioName, " audio played with natural voice")
			ELSE
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(piDartPlayers[iTemp], DebugBoard.sAudioName, DebugBoard.sAudioVoice, SPEECH_PARAMS_FORCE_FRONTEND)
				CDEBUG1LN(DEBUG_DARTS, DebugBoard.sAudioName, " audio played with debugged voice")
			ENDIF
		ENDIF
		
		DebugBoard.bPlayAudio = FALSE
	ENDIF
	
	IF DebugBoard.bShowStats
		
		DRAW_DEBUG_TEXT_2D("DARTS_STAT_NUM_BULLSEYES ", <<0.15, 0.20, 0.0>>)
		DRAW_DEBUG_TEXT_2D("DARTS_STAT_NUM_180S ", 		<<0.15, 0.22, 0.0>>)
		DRAW_DEBUG_TEXT_2D("DARTS_STAT_NUM_WINS ", 		<<0.15, 0.24, 0.0>>)
		DRAW_DEBUG_TEXT_2D("DARTS_STAT_NUM_LOSS ", 		<<0.15, 0.26, 0.0>>)
		DRAW_DEBUG_TEXT_2D("DARTS_STAT_DARTS_THROWN ", 	<<0.15, 0.28, 0.0>>)
		DRAW_DEBUG_TEXT_2D("DARTS_STAT_NUM_GAMES ", 	<<0.15, 0.30, 0.0>>)
		DRAW_DEBUG_TEXT_2D("DARTS_STAT_TIME_PLAYED ", 	<<0.15, 0.32, 0.0>>)
		DRAW_DEBUG_TEXT_2D("DARTS_STAT_WIN_PCT ", 		<<0.15, 0.34, 0.0>>)
		DRAW_DEBUG_TEXT_2D("DARTS_STAT_AVG_DARTS_MATCH",<<0.15, 0.36, 0.0>>)
		
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(TO_FLOAT(g_savedGlobals.sDartsData.iNumBullseyes), 0),  	<<0.35, 0.20, 0.0>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(TO_FLOAT(g_savedGlobals.sDartsData.iNumOneEighties), 0), 	<<0.35, 0.22, 0.0>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(TO_FLOAT(g_savedGlobals.sDartsData.iDartGamesWon), 0), 	<<0.35, 0.24, 0.0>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(TO_FLOAT(g_savedGlobals.sDartsData.iDartGamesLoss), 0), 	<<0.35, 0.26, 0.0>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(TO_FLOAT(g_savedGlobals.sDartsData.iDartsThrown), 0), 		<<0.35, 0.28, 0.0>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(TO_FLOAT(g_savedGlobals.sDartsData.iTotalDartGames), 0), 	<<0.35, 0.30, 0.0>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(TO_FLOAT(g_savedGlobals.sDartsData.iDartTimePlayed), 0), 	<<0.35, 0.32, 0.0>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(g_savedGlobals.sDartsData.fWinPct, 2),  					<<0.35, 0.34, 0.0>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(g_savedGlobals.sDartsData.fAvgDartsPerMatch, 2),  			<<0.35, 0.36, 0.0>>)
	ENDIF
	
	IF DebugBoard.bTurnOnScoreBoardDebug
		
		IF DebugBoard.bTurnOnScoreBoardDebugPos
		
			DebugBoard.vScoreBoardRotation.x = DebugBoard.fScoreBoardRotateX
			DebugBoard.vScoreBoardRotation.y = DebugBoard.fScoreBoardRotateY
			DebugBoard.vScoreBoardRotation.z = DebugBoard.fScoreBoardRotateZ
			
			DebugBoard.vScoreBoardSize.x = DebugBoard.fScoreBoardSizeX
			DebugBoard.vScoreBoardSize.y = DebugBoard.fScoreBoardSizeY
			DebugBoard.vScoreBoardSize.z = 1.0
			
			DRAW_SCALEFORM_MOVIE_3D(siScoreBoardDebug, DebugBoard.vScoreBoardPos, 
									DebugBoard.vScoreBoardRotation, DebugBoard.vScoreBoardSize, DebugBoard.vScoreBoardSize)
									
			DebugBoard.bTurnOnScoreBoardDebug2DPos = FALSE
		ENDIF
		
		IF DebugBoard.bTurnOnScoreBoardDebug2DPos
			
			DRAW_SCALEFORM_MOVIE(siScoreBoardDebug, DebugBoard.fScoreBoardPosX, DebugBoard.fScoreBoardPosY, 
								DebugBoard.fScoreBoardSizeX, DebugBoard.fScoreBoardSizeY, 
								255, 255, 255, 0)
		
			DebugBoard.bTurnOnScoreBoardDebugPos = FALSE
		ENDIF
	

		IF DebugBoard.bAddScoreToPlayerOne
			
			BEGIN_SCALEFORM_MOVIE_METHOD(siScoreBoardDebug, "ADD_DARTS_SCORE")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // player one
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DebugBoard.iScoreBoardPoints)
			END_SCALEFORM_MOVIE_METHOD()
			
			DebugBoard.bAddScoreToPlayerOne = FALSE
		ENDIF
		
		IF DebugBoard.bAddScoreToPlayerTwo
			BEGIN_SCALEFORM_MOVIE_METHOD(siScoreBoardDebug, "ADD_DARTS_SCORE")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // player two
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DebugBoard.iScoreBoardPoints)
			END_SCALEFORM_MOVIE_METHOD()
			
			DebugBoard.bAddScoreToPlayerTwo = FALSE
		ENDIF
		
		IF DebugBoard.bAddNames
			DebugBoard.scoreBoardName1 = GET_CONTENTS_OF_TEXT_WIDGET(DebugBoard.twiScoreboardName1)
			DebugBoard.scoreBoardName2 = GET_CONTENTS_OF_TEXT_WIDGET(DebugBoard.twiScoreboardName2)
			
			BEGIN_SCALEFORM_MOVIE_METHOD(siScoreBoardDebug, "SET_DARTS_PLAYER_NAMES")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(DebugBoard.scoreBoardName1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(DebugBoard.scoreBoardName2)
			//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING()
			//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING()
			END_SCALEFORM_MOVIE_METHOD()
			
			DebugBoard.bAddNames = FALSE
		ENDIF
		
		IF DebugBoard.bClearScores1
			BEGIN_SCALEFORM_MOVIE_METHOD(siScoreBoardDebug, "CLEAR_SCORES")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			END_SCALEFORM_MOVIE_METHOD()
			
			DebugBoard.bClearScores1 = FALSE
		ENDIF
		
		IF DebugBoard.bClearScores2
			BEGIN_SCALEFORM_MOVIE_METHOD(siScoreBoardDebug, "CLEAR_SCORES")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			END_SCALEFORM_MOVIE_METHOD()
			
			DebugBoard.bClearScores2 = FALSE
		ENDIF
		
		IF DebugBoard.bClearBoard
			BEGIN_SCALEFORM_MOVIE_METHOD(siScoreBoardDebug, "CLEAR_DARTBOARD")
			//CALL_SCALEFORM_MOVIE_METHOD(siScoreBoardDebug, "CLEAR_DARTBOARD")
			END_SCALEFORM_MOVIE_METHOD()
			DebugBoard.bClearBoard = FALSE
		ENDIF
	ENDIF
	
	IF DebugBoard.bDebugBusts
		DartGame.Scores[0] = 5
		DartGame.Scores[1] = 5
	ENDIF
	
	IF DebugBoard.bDrawSprite
		spriteDebug.x = DebugBoard.fSpriteX
		spriteDebug.y = DebugBoard.fSpriteX
		spriteDebug.w = DebugBoard.fSpriteW
		spriteDebug.h = DebugBoard.fSpriteH
		spriteDebug.r = DebugBoard.iSpriteR
		spriteDebug.g = DebugBoard.iSpriteG
		spriteDebug.b = DebugBoard.iSpriteB
		spriteDebug.a = 255
		
		IF DebugBoard.bDrawZoomedSprite
			DRAW_2D_SPRITE("Darts", "Dart_Reticules_Zoomed", spriteDebug, TRUE, HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_AFTER_HUD,TRUE)
		ELSE
			DRAW_2D_SPRITE("Darts", "Dart_Reticules", spriteDebug, TRUE, HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_AFTER_HUD,TRUE)
		ENDIF
	ENDIF
	
	IF DebugBoard.bTurnOnRetDebugPos
		IF DOES_ENTITY_EXIST(oiReticle)
			//SET_ENTITY_COORDS(oiReticle, DebugBoard.vDebugRetVector)
    		//SET_ENTITY_ROTATION(oiReticle, << DebugBoard.fRotateRetX, DebugBoard.fRotateRetY, DebugBoard.fRotateRetZ >>)
		ENDIF
	ENDIF
	
	IF DebugBoard.bDebugThrowStyle
		IF DebugBoard.bThrowStyleStick
			DartGame.eThrowStyle = DARTS_STYLE_IV
		ELSE
			DartGame.eThrowStyle = DARTS_STYLE_STICK
		ENDIF
	ENDIF
	
	IF DebugBoard.bTurnOnDartLine
		VECTOR vTemp, vTemp2
		
		IF NOT DOES_CAM_EXIST(DebugBoard.camTemp)
			DebugBoard.camTemp = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>)
		ENDIF
		
		vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartGame.DartsBoard.vDartBoard, DartGame.DartsBoard.fBoardHeading, 
																<< DartGame.Darts[iPlayr][iTurn].vRandomTarget.x, 
																DartGame.Darts[iPlayr][iTurn].vRandomTarget.y + fdartboardOriginOffset,
											   		   			DartGame.Darts[iPlayr][iTurn].vRandomTarget.z >>)
		
		vTemp2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartGame.DartsBoard.vDartBoard, DartGame.DartsBoard.fBoardHeading, 
																vInitDartPosOffset)
		DRAW_DEBUG_LINE(vTemp, vTemp2)
		
		TEXT_LABEL_31 sDebugPath[4]
		
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			SET_CAM_COORD(DebugBoard.camTemp, vTemp2)
			POINT_CAM_AT_COORD(DebugBoard.camTemp, vTemp)
			DebugBoard.vRealRot = GET_CAM_ROT(DebugBoard.camTemp, EULER_XYZ)
			SET_CAM_ACTIVE(DebugBoard.camTemp, TRUE)
			
			DebugBoard.fDartHeading = GET_HEADING_FROM_VECTOR_2D((vTemp2.x - vTemp.x), (vTemp2.y - vTemp.y))
			DebugBoard.fDartHeading -= 180
		ELSE
			IF IS_CAM_ACTIVE(DebugBoard.camTemp)
				SET_CAM_ACTIVE(DebugBoard.camTemp, FALSE)
			ENDIF
		ENDIF
		
		sDebugPath[0] += GET_STRING_FROM_FLOAT(DebugBoard.vRealRot.x)
		sDebugPath[1] += GET_STRING_FROM_FLOAT(DebugBoard.vRealRot.y)
		sDebugPath[2] += GET_STRING_FROM_FLOAT(DebugBoard.vRealRot.z)
		sDebugPath[3] += GET_STRING_FROM_FLOAT(DebugBoard.fDartHeading)
		
		DRAW_DEBUG_TEXT_2D(sDebugPath[0], <<0.20, 0.40, 0>>)
		DRAW_DEBUG_TEXT_2D(sDebugPath[1], <<0.20, 0.42, 0>>)
		DRAW_DEBUG_TEXT_2D(sDebugPath[2], <<0.20, 0.44, 0>>)
		DRAW_DEBUG_TEXT_2D(sDebugPath[3], <<0.20, 0.46, 0>>)
	ENDIF
	
	IF DebugBoard.bTurnOnReticuleDebug
		DartGame.Darts[iPlayr][iTurn].vTarget.x = DebugBoard.fRetX - 0.0041
		DartGame.Darts[iPlayr][iTurn].vTarget.z = DebugBoard.fRetZ - 0.0002
	ENDIF
	
	IF DebugBoard.bTurnOnDartThrowDebug 
		fWobbleFactor = 0.0
		fRealWobFactor = 0.0
		fTargetMoveSpeed = 0.025
		
		spriteReticle.w = 0.045
		spriteReticle.h = 0.085
		
		TEXT_LABEL_31 sDebugScore[6]
		
		sDebugScore[0] = "RETICULE SCORE: "
		sDebugScore[1] = "RETICULE MULTIPLIER: "
		sDebugScore[2] = "RETICULE LENGTH: "
		sDebugScore[3] = "RETICULE ANGLE: "
		sDebugScore[4] = "RETICULE X: "
		sDebugScore[5] = "RETICULE Y: "
		
		// CHECK SCORE AND MULTIPLIER IF PLAYER PRESSES CIRCLE
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			INT iTemp
			
			FLOAT fDartAngleIncremented
			
			fDartAngleIncremented = 0	
			iTemp = 0
			
			VECTOR vTempTarget = DartGame.Darts[iPlayr][iTurn].vTarget
			vTempTarget.x += 0.0041
			vTempTarget.z += 0.0002
			
			DebugBoard.fDartAngle = GET_ANGLE_BETWEEN_2D_VECTORS(0.0, 1.0, vTempTarget.x, vTempTarget.z)
			DebugBoard.fLength = GET_VECTOR_LENGTH(vTempTarget) 
			
			IF DebugBoard.fLength > 0.1285 AND DebugBoard.fLength < 0.1405  // treble
				DebugBoard.fScoreMult = 3
			ELIF DebugBoard.fLength > 0.2132 AND DebugBoard.fLength < 0.226  // double
				DebugBoard.fScoreMult = 2
			ELSE
				DebugBoard.fScoreMult = 1
			ENDIF
			
			REPEAT 21 iTemp
				IF (DebugBoard.fDartAngle >= (fDartAngleIncremented - 9.0)) AND (DebugBoard.fDartAngle < (fDartAngleIncremented + 9.0))
					DebugBoard.fScoreValue = TO_FLOAT(iScoreForSection[iTemp])
				ENDIF
				fDartAngleIncremented += 18.0
			ENDREPEAT
			
			IF DebugBoard.fLength < 0.009 // bull
				DebugBoard.fScoreValue = 50
			ELIF DebugBoard.fLength < 0.021 // outer bull
				DebugBoard.fScoreValue = 25
			ELIF DebugBoard.fLength >= 0.226
				DebugBoard.fScoreValue = 0
			ENDIF
		ENDIF
		
		sDebugScore[0] += GET_STRING_FROM_FLOAT(DebugBoard.fScoreValue)
		sDebugScore[1] += GET_STRING_FROM_FLOAT(DebugBoard.fScoreMult)
		sDebugScore[2] += GET_STRING_FROM_FLOAT(DebugBoard.fLength)
		sDebugScore[3] += GET_STRING_FROM_FLOAT(DebugBoard.fDartAngle)
		sDebugScore[4] += GET_STRING_FROM_FLOAT(DartGame.Darts[iPlayr][iTurn].vTarget.x)
		sDebugScore[5] += GET_STRING_FROM_FLOAT(DartGame.Darts[iPlayr][iTurn].vTarget.z)
		
		DRAW_DEBUG_TEXT_2D(sDebugScore[0], <<0.20, 0.25, 0>>)
		DRAW_DEBUG_TEXT_2D(sDebugScore[1], <<0.20, 0.27, 0>>)
		DRAW_DEBUG_TEXT_2D(sDebugScore[2], <<0.20, 0.29, 0>>)
		DRAW_DEBUG_TEXT_2D(sDebugScore[3], <<0.20, 0.31, 0>>)
		DRAW_DEBUG_TEXT_2D(sDebugScore[4], <<0.20, 0.33, 0>>)
		DRAW_DEBUG_TEXT_2D(sDebugScore[5], <<0.20, 0.35, 0>>)
		
		IF DebugBoard.bTurnOnDartRotDebug
			IF DOES_ENTITY_EXIST(DartGame.Darts[0][DebugBoard.iDartNum].object)
				//SET_ENTITY_HEADING(DartGame.Darts[0][DebugBoard.iDartNum].object, DebugBoard.fBoardHeading)
				SET_ENTITY_ROTATION(DartGame.Darts[0][DebugBoard.iDartNum].object, << DebugBoard.fRotateDartX, DebugBoard.fRotateDartY, DebugBoard.fRotateDartZ >>, EULER_XYZ)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
	#ENDIF
