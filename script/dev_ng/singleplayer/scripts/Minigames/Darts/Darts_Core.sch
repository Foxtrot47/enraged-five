////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Core.sch                          //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Mostly Camera Work and tutorials      //
//                                                           //
//////////////////////////////////////////////////////////////

//USING "types.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USINg "commands_hud.sch"
USING "minigame_uiinputs.sch"

ENUM DARTS_TUTORIAL_STAGE
	TS_THROWING,
	TS_DART_LANDING,
	TS_DOUBLE,
	TS_TRIPLE,
	TS_BULLSEYE,
	TS_DBL_TO_WIN,
	TS_ENDING,
	TS_ENDING2,
	TS_SKIP
ENDENUM

CAMERA_INDEX	camDartGame
CAMERA_INDEX	camDartGame2
CAMERA_INDEX	camAnotherGame
CAMERA_INDEX	camAnotherGameInterp
CAMERA_INDEX	camAnotherGameMP
CAMERA_INDEX	camAnotherGameSetsLegs
CAMERA_INDEX	camAnotherGameBackSide
CAMERA_INDEX	camTutorial
CAMERA_INDEX	camTutInterp

CAMERA_INDEX	camZoomBoard
CAMERA_INDEX	camZoomDart
CAMERA_INDEX	camZoomDartPrev
CAMERA_INDEX	camDoubleToWin

BOOL bDartZoomFlag
FLOAT fOldAspectRatio

PROC CLEANUP_HUD()	
	CLEAR_HELP()
	
	//RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DESTROY_ALL_CAMS()
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	DISPLAY_RADAR(TRUE)
ENDPROC

PROC START_DARTS_GAME_FADE_OUT()	
	DO_SCREEN_FADE_OUT(500)
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
ENDPROC

PROC ANOTHER_GAME_CAM()	
	
	SET_CAM_ACTIVE_WITH_INTERP(camAnotherGame, camDartGame, 1500)
	
	SET_CAM_ACTIVE(camAnotherGameInterp, TRUE)
	SET_CAM_ACTIVE_WITH_INTERP(camAnotherGame, camAnotherGameInterp, 8000)
ENDPROC

PROC ANOTHER_GAME_CAM_START()
	SET_CAM_ACTIVE(camAnotherGame, TRUE)
ENDPROC

PROC ANOTHER_GAME_CAM_MP(PED_INDEX piPointTo)
	
	ATTACH_CAM_TO_ENTITY(camAnotherGameMP, piPointTo, <<0.0166, 1.6423, 0.4373>>)
	POINT_CAM_AT_ENTITY(camAnotherGameMP, piPointTo, <<-0.0432, -1.3557, 0.3447>>)
	SET_CAM_FOV(camAnotherGameMP, 35.0)
	
	SET_CAM_ACTIVE(camAnotherGameMP, TRUE)
ENDPROC

PROC ANOTHER_GAME_CAM_SETS_LEGS(PED_INDEX piPointTo)
	
	SET_CAM_ACTIVE_WITH_INTERP(camAnotherGame, camDartGame, 1500)
	
	SET_CAM_ACTIVE(camAnotherGameInterp, TRUE)
	SET_CAM_ACTIVE_WITH_INTERP(camAnotherGame, camAnotherGameInterp, 8000)
	
	ATTACH_CAM_TO_ENTITY(camAnotherGameSetsLegs, piPointTo, <<-0.0301, 1.4980, 0.7435>>)
	POINT_CAM_AT_ENTITY(camAnotherGameSetsLegs, piPointTo, <<0.0557, -1.4905, 0.4958>>)
	SET_CAM_FOV(camAnotherGameSetsLegs, 35.0)
//	
//	SET_CAM_ACTIVE(camAnotherGameSetsLegs, TRUE)
ENDPROC

PROC ANOTHER_GAME_BACKSIDE()
	SET_CAM_ACTIVE(camAnotherGameBackSide, TRUE)
	
ENDPROC

FUNC FLOAT GET_CAM_FOV_AR_BASED(FLOAT fAspectRatio)
	IF  fAspectRatio < 1.3
		RETURN 35.0
	ELIF fAspectRatio < 1.35
		RETURN 33.0
	ENDIF
	RETURN 30.0	//Default
ENDFUNC

PROC INIT_CAM(VECTOR vDartBoardPos, FLOAt fDartBoardHeading)	

	VECTOR	vDartCamPosition, vDartCamPosition2
	VECTOR 	vDartCamRot, vDartCamRot2
	VECTOR	vCamAnotherGame
	VECTOR	vCamAnotherGameRot
	VECTOR 	vCamAnotherIntrpRot
	
//	VECTOR 	vDartGameCamOffset 		= << -0.0470, -2.1560, 0.0 >>
//	FLOAT 	fDartGameCamOffsetRot 	= -2.3332
//	FLOAT 	fDartCamFOV 			= 23.2811
	
	//Initialise aspect ratio
	fOldAspectRatio = GET_ASPECT_RATIO(FALSE)
	
	FLOAT 	fDartCamFOV 			= 30.0
	
	//B* 2234016: Increase FOV on very low aspect ratios
	fDartCamFOV = GET_CAM_FOV_AR_BASED(fOldAspectRatio)
	
	//SIDE
	VECTOR 	vDartGameCamOffset2 	= << -0.192784, -1.73287, 0.0262985 >>
	FLOAT 	fDartGameCamOffsetRotz2 = 1.7939
	FLOAT 	fDartGameCamOffsetRotx2	= -1.3346
	
	VECTOR 	vDartGameCamOffset
	FLOAT 	fDartGameCamOffsetRotz
	FLOAT 	fDartGameCamOffsetRotx
	
	//TOP
	IF GET_IS_WIDESCREEN()
		CDEBUG1LN(DEBUG_DARTS, "this is widescreen")
		vDartGameCamOffset 		= << -0.0974819, -1.65968, 0.335098 >> //<< -0.0975875, -1.65007, 0.378502 >>
		fDartGameCamOffsetRotz 	= -1.3023
		fDartGameCamOffsetRotx 	= -12.4139 //-14.4576
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "this is NOT widescreen")
		vDartGameCamOffset 		= << -0.112635, -1.86963, 0.45417 >> //<< -0.156828, -1.87872, 0.45417 >>
		fDartGameCamOffsetRotz 	= -3.7542 //-0.6023
		fDartGameCamOffsetRotx 	= -14.4576
	ENDIF
	
	VECTOR 	vAnotherGameCamOffset 	= << 0.261599, -0.750354, -0.392929 >> //<< 0.256954, -3.13459, 0.0454788 >>
	//VECTOR 	vAnotherGameRotOffset	= << 3.0539, 0.0, -4.2507 >>
	
	VECTOR	vCamAnotherIntrp
	VECTOR 	vAnotherIntrpCamOffset 	= << 0.281314, -0.573735, -0.39603 >> //<< 0.256954, -3.13459, 0.0454788 >>
	//VECTOR 	vAnotherIntrpRotOffset	= << 3.0539, 0.0, -4.2507 >>
	
	vDartCamPosition2	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDartBoardPos, fDartBoardHeading, vDartGameCamOffset2)
	vDartCamRot2		= << fDartGameCamOffsetRotx2, 0, (fDartBoardHeading - fDartGameCamOffsetRotz2) >>
	
	vDartCamPosition 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDartBoardPos, fDartBoardHeading, vDartGameCamOffset)
	vDartCamRot			= << fDartGameCamOffsetRotx, 0, (fDartBoardHeading - fDartGameCamOffsetRotz) >>
	
	vCamAnotherGame	 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDartBoardPos, fDartBoardHeading, vAnotherGameCamOffset)
	vCamAnotherGameRot 	= <<0.8351, -0.0048, fDartBoardHeading - 186.507346>> //<< 0, 0, fDartBoardHeading >> - vAnotherGameRotOffset
	
	vCamAnotherIntrp	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDartBoardPos, fDartBoardHeading, vAnotherIntrpCamOffset)
	vCamAnotherIntrpRot = <<0.8351, -0.0048, fDartBoardHeading - 186.507346>>
	//<<0.8351, -0.0048, -128.7242>>
	//57.783146
	
	camDartGame 			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vDartCamPosition, vDartCamRot, fDartCamFOV)
	camDartGame2 			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vDartCamPosition2, vDartCamRot2, fDartCamFOV)
	camAnotherGame 			= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamAnotherGame, vCamAnotherGameRot)
	camAnotherGameInterp	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamAnotherIntrp, vCamAnotherIntrpRot) // <<1.0038, -0.0147, -128.7269>>)
	camAnotherGameMP		= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0, 0, 0>>, <<0, 0, 0>>)
	camAnotherGameSetsLegs 	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0, 0, 0>>, <<0, 0, 0>>)
	
	camTutorial 	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>)
	camTutInterp	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>)
	
	camAnotherGameBackSide = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1996.2350, 3048.4558, 48.0237>>, <<-3.4831, 0.0223, 60.6925>>, 38.1)
	
//	POINT_CAM_AT_ENTITY(camAnotherGame, PLAYER_PED_ID(), <<0.2910, -1.3950, 0.4251>>)
//	POINT_CAM_AT_ENTITY(camAnotherGameInterp, PLAYER_PED_ID(), <<0.2910, -1.3950, 0.4251>>)
	SET_CAM_FOV(camAnotherGame, 43.35)
	SET_CAM_FOV(camAnotherGameInterp, 42.35)
	SHAKE_CAM(camAnotherGame, "HAND_SHAKE", 0.1)
	SHAKE_CAM(camAnotherGameInterp, "HAND_SHAKE", 0.1)
	
ENDPROC

PROC HANDLE_SWITCH_CAM(BOOL& bSwitchCam)
	IF NOT bSwitchCam
		IF NOT IS_CAM_ACTIVE(camDartGame)
			SET_CAM_ACTIVE(camDartGame, TRUE)
			SET_CAM_ACTIVE(camDartGame2, FALSE)
		ENDIF
	ELSE
		IF NOT IS_CAM_ACTIVE(camDartGame2)
			SET_CAM_ACTIVE(camDartGame, FALSE)
			SET_CAM_ACTIVE(camDartGame2, TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_TUTORIAL_CAM_COORD_OFFSET(DARTS_TUTORIAL_STAGE thisTutorialStage)
	SWITCH thisTutorialStage
		CASE TS_THROWING 	RETURN <<-0.0588, -1.6075, 0.1336>> //<<-0.0557, -1.4371, 0.1306>>	//<< -0.0454239, -1.0904, 0.0918808 >>
		CASE TS_DOUBLE		RETURN <<-0.0944, -1.2308, 0.3468>> // <<-0.0419, -0.9557, 0.3361>>	//<< -0.0273121, -0.547176, 0.257683 >>
		CASE TS_TRIPLE 		RETURN <<-0.0948, -1.2430, 0.2067>> // <<-0.0381, -0.9370, 0.2529>>	//<< -0.0276095, -0.555066, 0.174179 >>
		CASE TS_BULLSEYE	RETURN <<-0.0955, -1.2524, 0.1006>> // <<-0.0516, -1.2965, 0.1172>>	//<< -0.0326267, -0.700141, 0.0550842 >>
		CASE TS_DBL_TO_WIN	RETURN <<-0.0951, -1.2526, 0.0895>> // <<-0.2495, -1.3864, 0.1264>>	//<< -0.229153, -0.69313, 0.0550842 >>
		DEFAULT 	RETURN <<0,0,0>>
	ENDSWITCH
ENDFUNC

FUNC VECTOR GET_TUTORIAL_CAM_ROT_OFFSET(BOOL bWinRot = FALSE)
	IF bWinRot
		RETURN << -4.7411, 0.0, -15.32 >>
	ELSE
		RETURN << 4.9918, 0.0, 4.3916 >> //<< 5.3775, 0.0000, 2.0157 >>
	ENDIF
ENDFUNC

PROC DARTS_INTRO_SPLASH_CAM_NO_TUTORIAL()
	
	SET_CAM_ACTIVE_WITH_INTERP(camDartGame, camTutorial, 2000)
	//RENDER_SCRIPT_CAMS(TRUE, TRUE)
	
ENDPROC

PROC DARTS_INTRO_SPLASH_CAM(VECTOR vDartBoardPos, FLOAt fDartBoardHeading)
	
	SET_CAM_COORD(camTutorial, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDartBoardPos, fDartBoardHeading, GET_TUTORIAL_CAM_COORD_OFFSET(TS_THROWING)))
	//SET_CAM_FOV(camTutorial, 35.0)
	SET_CAM_FOV(camTutorial, 35.0)
	SET_CAM_ROT(camTutorial, (<< 0, 0, fDartBoardHeading >> - GET_TUTORIAL_CAM_ROT_OFFSET()))
	
	SET_CAM_ACTIVE(camTutorial, TRUE)
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
ENDPROC

PROC DARTS_INTRO_SPLASH_INTERP(VECTOR vDartBoardPos, FLOAt fDartBoardHeading)
	
	SET_CAM_COORD(camTutInterp, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDartBoardPos, fDartBoardHeading, <<0.0, -1.5, 0.0>>))
	SET_CAM_FOV(camTutInterp, 35.0)
	POINT_CAM_AT_COORD(camTutInterp, vDartBoardPos)
	
	SET_CAM_ACTIVE_WITH_INTERP(camTutInterp, camTutorial, 3000)
	
ENDPROC

PROC DARTS_TUTORIAL_CAM(VECTOR vDartBoardPos, FLOAt fDartBoardHeading, VECTOR vCamTutorial, VECTOR vCamtutorialRot, 
						INT interpTime = 0, BOOL bStart = FALSE)
	
	STOP_CAM_POINTING(camTutorial)
	SET_CAM_FOV(camTutorial, 35.0)
	SET_CAM_COORD(camTutorial, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDartBoardPos, fDartBoardHeading, vCamTutorial))
	SET_CAM_ROT(camTutorial, (<< 0, 0, fDartBoardHeading >> - vCamTutorialRot))
	IF bStart
		SET_CAM_ACTIVE(camTutorial, TRUE)
	ELSE
		SET_CAM_ACTIVE_WITH_INTERP(camTutorial, camTutInterp, interpTime)
	ENDIF
ENDPROC

PROC DARTS_TUTORIAL_CAM_INTERP(VECTOR vDartBoardPos, FLOAt fDartBoardHeading, VECTOR vCamTutorial, VECTOR vCamtutorialRot, INT interpTime = 0)
	
	STOP_CAM_POINTING(camTutInterp)
	SET_CAM_FOV(camTutInterp, 35.0)
	
	SET_CAM_COORD(camTutInterp, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDartBoardPos, fDartBoardHeading, vCamTutorial))
	SET_CAM_ROT(camTutInterp, (<< 0, 0, fDartBoardHeading >> - vCamTutorialRot))
	SET_CAM_ACTIVE_WITH_INTERP(camTutInterp, camTutorial, interpTime)
ENDPROC

PROC DARTS_START_GAME_CAM(BOOL bFromAnotherCam=TRUE)
	
	IF bFromAnotherCam
		//SET_CAM_ACTIVE_WITH_INTERP(camDartGame, camAnotherGame, 1500)
		SET_CAM_ACTIVE(camDartGame, TRUE)
	ELSE
		/*
		IF IS_CAM_ACTIVE(camZoomDart)
			SET_CAM_ACTIVE_WITH_INTERP(camDartGame, camZoomDart, 1000)
		ELIF IS_CAM_ACTIVE(camZoomDartPrev)
			SET_CAM_ACTIVE_WITH_INTERP(camDartGame, camZoomDartPrev, 1000)
		ENDIF
		// */
	ENDIF
	
ENDPROC

PROC DARTS_GAME_CAM()
	SET_CAM_ACTIVE(camDartGame, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	IF IS_CAM_ACTIVE(camTutorial)
		SET_CAM_ACTIVE(camTutorial, FALSE)
	ENDIF
	
	IF IS_CAM_ACTIVE(camAnotherGame)
		SET_CAM_ACTIVE(camAnotherGame, FALSE)
	ENDIF
	
	IF IS_CAM_ACTIVE(camAnotherGameInterp)
		SET_CAM_ACTIVE(camAnotherGameInterp, FALSE)
	ENDIF
	
	IF IS_CAM_ACTIVE(camAnotherGameMP)
		SET_CAM_ACTIVE(camAnotherGameMP, FALSE)
	ENDIF
	
	IF IS_CAM_ACTIVE(camAnotherGameSetsLegs)
		SET_CAM_ACTIVE(camAnotherGameSetsLegs, FALSE)
	ENDIF
	
	IF IS_CAM_ACTIVE(camAnotherGameBackSide)
		SET_CAM_ACTIVE(camAnotherGameBackSide, FALSE)
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "Game cam set")
ENDPROC

PROC START_DARTS_GAME_FADE_IN(BOOL bFromAnotherCam)
	// kill the huds
	DISPLAY_RADAR(FALSE)
	
	IF IS_CAM_ACTIVE(camAnotherGame)
		SET_CAM_ACTIVE(camAnotherGame, FALSE)
	ENDIF
	
	IF IS_CAM_ACTIVE(camAnotherGameBackSide)
		SET_CAM_ACTIVE(camAnotherGameBackSide, FALSE)
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		ANOTHER_GAME_CAM_START()
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		DO_SCREEN_FADE_IN(500)		
	ENDIF
	
	DARTS_START_GAME_CAM(bFromAnotherCam)
	//DO_SCREEN_FADE_IN(500)
ENDPROC

PROC DARTS_DOUBLE_CAM_START(VECTOR vDoubleWinner)
	VECTOR vDartCamRot = << -5.5606, -0.0106, -131.6781 >>
	
	camDoubleToWin = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vDoubleWinner, vDartCamRot)
	
	SET_CAM_ACTIVE_WITH_INTERP(camDoubleToWin, camDartGame, 2000)
ENDPROC

PROC DARTS_DOUBLE_CAM_END()
	SET_CAM_ACTIVE_WITH_INTERP(camDartGame, camDoubleToWin, 2000)
	DESTROY_CAM(camDoubleToWin)
ENDPROC

FUNC VECTOR GET_DART_OFFSET(VECTOR vDartPos)
	VECTOR vTemp
	VECTOR vDartCamOffset = << 0.3495, -0.0276, -0.0114 >>
	
	vTemp = vDartPos - vDartCamOffset
	
	RETURN vTemp
ENDFUNC

PROC DARTS_ZOOM_DART_CAM(VECTOR vDartPos)
	
	VECTOR vCamTemp
	
	vCamTemp = GET_DART_OFFSET(vDartPos)
	
	IF bDartZoomFlag
		SET_CAM_COORD(camZoomDartPrev, vCamTemp)	
		SET_CAM_ACTIVE_WITH_INTERP(camZoomDartPrev, camZoomDart, 500)
		
		bDartZoomFlag = FALSE
		
	ELSE
	
		SET_CAM_COORD(camZoomDart, vCamTemp)	
		SET_CAM_ACTIVE_WITH_INTERP(camZoomDart, camZoomDartPrev, 500)
		
		bDartZoomFlag = TRUE
		
	ENDIF
ENDPROC

PROC DARTS_ZOOM_DART_CAM_TRANS(VECTOR vDartPos, BOOL bIsStart=TRUE, BOOL bGameEnd=FALSE)
	VECTOR vCamTemp
	
	vCamTemp = GET_DART_OFFSET(vDartPos)
	
	IF bIsStart
		SET_CAM_COORD(camZoomDart, vCamTemp)	
		SET_CAM_ACTIVE_WITH_INTERP(camZoomDart, camZoomBoard, 1500)
		SET_CAM_ACTIVE(camZoomBoard, FALSE)
		
		SET_CAM_COORD(camZoomDartPrev, vCamTemp)
		
		bDartZoomFlag = TRUE
	
	ELIF bGameEnd	
		SET_CAM_COORD(camZoomDart, vCamTemp)	
		SET_CAM_ACTIVE_WITH_INTERP(camZoomDart, camAnotherGame, 2000)
		
		bDartZoomFlag = TRUE
	ELSE
		IF bDartZoomFlag
			SET_CAM_ACTIVE_WITH_INTERP(camZoomBoard, camZoomDart, 1500)
		ELSE
			SET_CAM_ACTIVE_WITH_INTERP(camZoomBoard, camZoomDartPrev, 1500)
		ENDIF
		SET_CAM_ACTIVE(camZoomDart, FALSE)
	ENDIF
ENDPROC

PROC DARTS_ZOOM_LAST_TURN(BOOL bZoomBoard=TRUE)	
	IF bZoomBoard
		SET_CAM_ACTIVE_WITH_INTERP(camZoomBoard, camDartGame, 1000)
	ELSE
		SET_CAM_ACTIVE_WITH_INTERP(camDartGame, camZoomBoard, 1000)
	ENDIF
ENDPROC

PROC BOARD_ZOOM_CAM()
	SET_CAM_ACTIVE(camZoomBoard, TRUE)
	SET_CAM_ACTIVE(camDartGame, FALSE)
ENDPROC
// */
