////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    MP_Darts_Server.sch                     //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Dart Server Defs       			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "Darts_Timer.sch"

ENUM DARTS_SERVER_FLAGS
	// Terminate flags
	DARTSERVERFLAG_QUIT_1CLIENT		= 1,
	
	// Winner flags
	DARTSERVERFLAG_AWAYWIN   		= 10,
	DARTSERVERFLAG_HOMEWIN   		= 11,
	DARTSERVERFLAG_AWAYTURNEND		= 12,
	DARTSERVERFLAG_HOMETURNEND		= 13,
	
	// Player state movement flags
	DARTSERVERFLAG_MOVETOGAME		= 20,
	DARTSERVERFLAG_MOVETOMENU		= 21,
	DARTSERVERFLAG_DO_THROWOFF		= 22,
	DARTSERVERFLAG_DO_TRANSITION	= 23,
	DARTSERVERFLAG_SET_JUST_WON		= 24,
	DARTSERVERFLAG_SPECS_SCORED		= 25,
	DARTSERVERFLAG_THROW_OFF_DONE	= 26,
	DARTSERVERFLAG_PLAYERS_INTRO_READY = 27
ENDENUM

ENUM DARTS_MP_TURN_STATUS
	DARTS_MPTURN_THROWING,
	DARTS_MPTURN_WAITING,
	DARTS_MPTURN_SCORE_CHECK,
	DARTS_MPTURN_WINNING
ENDENUM

INT DART_PLAYER_AWAY_SERVER
INT DART_PLAYER_HOME_SERVER

STRUCT DARTS_SERVER_BROADCAST_DATA
	DARTS_MP_STATE 			mpState
	DARTS_MP_TURN_STATUS 	mpTurnStatus[DARTSMP_IDS]
	DARTS_TIMER				DartsTimer
	
	//INT iBoardIndex
	INT iScores[DARTSMP_IDS]
	INT iLastScore[DARTSMP_IDS]
	INT iLegs[DARTSMP_IDS]
	INT iSets[DARTSMP_IDS]
	INT iNumLegs
	INT iNumSets
	INT iServerTurn
	INT iTurnscore	
	INT iWinner
	INT iCoinFlip
	INT iThrower
	INT iWaiter
	INT iFlags
	INT iUIFlags
	INT iMatchType
	INT iMatchHistoryID
	
	FLOAT fLength[DARTSMP_IDS]
	
	BOOL bDangerZoneOccupied
	BOOL bFinishedScoreTracking
	BOOL bTurnContinue
	BOOL bTurnEnd
	BOOL bTurnBust
	BOOL bDoSwitchPlayer
	BOOL bDoWinning
	
	SERVER_EOM_VARS  	sServerFMMC_EOM
	SCRIPT_TIMER		timeLeaderboardTimeOut
ENDSTRUCT

// Mutator for multiplayer state
PROC DARTS_SET_SERVER_MP_STATE(DARTS_SERVER_BROADCAST_DATA & serverData, DARTS_MP_STATE mpState)
	serverData.mpState = mpState
ENDPROC

FUNC DARTS_MP_STATE DARTS_GET_SERVER_MP_STATE(DARTS_SERVER_BROADCAST_DATA & serverData)
	RETURN serverData.mpState
ENDFUNC


PROC DARTS_SERVER_SET_FLAG (DARTS_SERVER_BROADCAST_DATA & serverData, DARTS_SERVER_FLAGS eFlagToSet)
	SET_BIT(serverData.iFlags, ENUM_TO_INT(eFlagToSet))
ENDPROC

PROC DARTS_SERVER_CLEAR_FLAG (DARTS_SERVER_BROADCAST_DATA & serverData, DARTS_SERVER_FLAGS eFlagToClear)
	CLEAR_BIT(serverData.iFlags, ENUM_TO_INT(eFlagToClear))
ENDPROC

FUNC BOOL DARTS_SERVER_CHECK_FLAG (DARTS_SERVER_BROADCAST_DATA & serverData, DARTS_SERVER_FLAGS eFlagToCheck)
	RETURN IS_BIT_SET(serverData.iFlags, ENUM_TO_INT(eFlagToCheck))
ENDFUNC

// Erases all goal and current scores from the server BD
PROC DARTS_SERVER_RESET_SCORES(DARTS_SERVER_BROADCAST_DATA & serverData)	
	serverData.iScores[DART_PLAYER_AWAY_SERVER] = 301
	serverData.iScores[DART_PLAYER_HOME_SERVER] = 301
ENDPROC


