////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Player.sch                        //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Dart Player Defs       			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "globals.sch"
USING "shared_hud_displays.sch"

ENUM DARTS_PLAYER_STATE
	DPS_ONE_DART_HELPER,
	DPS_ZOOM,
	DPS_AIM,
	DPS_THROW,
	DPS_SCORING,
	DPS_TURN_CHANGE,
	DPS_SKIP_AI
ENDENUM

ENUM DARTS_PLAYER
	DART_PLAYER_AWAY 	= 0, 
	DART_PLAYER_HOME 	= 1,
	DART_PLAYER_IDS 	= 2
ENDENUM

ENUM DARTS_THROW_STYLE
	DARTS_STYLE_IV,
	DARTS_STYLE_STICK,
	DARTS_STYLE_DUAL_RET
ENDENUM

ENUM DARTS_SHOT_STATE
	DARTS_SHOT_IDLE,
	DARTS_SHOT_BACK,
	DARTS_SHOT_FORWARD
ENDENUM

OBJECT_INDEX oiReticle

MODEL_NAMES modelReticleRegular = PROP_TARGET_BULL

SPRITE_PLACEMENT spriteReticle
SPRITE_PLACEMENT spriteDartLanding

STRUCT DARTS_STARTING_RETICLE_POS
	VECTOR 	vInitReticlePos
	VECTOR 	vInitReticlePosOffset
ENDSTRUCT

VECTOR 			vInitDartPos
VECTOR 			vInitDartPosOffset
VECTOR 			vInitLostHideoutPos = << 987.8541, -98.4173, 73.8599 >>

CONST_FLOAT		MAX_WOBBLE_FACTOR 		0.080
CONST_FLOAT		MAX_REAL_WOBBLE_FACTOR 	0.107
CONST_FLOAT		MIN_WOBBLE_FACTOR 		0.040
CONST_FLOAT		MIN_REAL_WOBBLE_FACTOR 	0.053

//0.09 //0.105 //0.12 //0.06
//0.12 //0.14 //0.16 //0.08

//TWEAK_FLOAT 		fWobbleFactor 			MAX_WOBBLE_FACTOR 		
//TWEAK_FLOAT		fRealWobFactor 			MAX_REAL_WOBBLE_FACTOR 	
//TWEAK_FLOAT		fScaledWobble 			MAX_WOBBLE_FACTOR
//TWEAK_FLOAT		fScaledRealWobble 		MAX_REAL_WOBBLE_FACTOR

FLOAT 			fWobbleFactor			
FLOAT			fRealWobFactor
FLOAT			fScaledWobble	
FLOAT			fScaledRealWobble

FLOAT 			fTargetMoveSpeed 		= 0.1

INT 			iSteadyShotsUsed

BOOL 			bSteadyShotOn = FALSE
BOOL			bSteadyShotHelp
BOOL 			bFastMoveHelp
BOOL 			bWaitThrow
BOOL 			bCrossButtonReleased
BOOL 			bNoCrossConflict
BOOL 			bInitReticle = FALSE
BOOL 			bReticleDrawn

// different wobble levels
// 10 wins: 0.072 25: 0.064 50: 0.056 70: 0.048

// different real levels
// 10 wins: 0.096 25: 0.085 50: 0.075 70: 0.064

FUNC FLOAT GET_WOBBLE_LEVEL(INT iNumWins)
	IF iNumWins < 10
		RETURN 0.080
	ELIF iNumWins < 25
		RETURN 0.072
	ELIF iNumWins < 50
		RETURN 0.064
	ELIF iNumWins < 70
		RETURN 0.056
	ELSE 
		RETURN 0.048
	ENDIF
ENDFUNC

FUNC FLOAT GET_REAL_WOBBLE_LEVEL(INT iNumWins)
	IF iNumWins < 10
		RETURN 0.107
	ELIF iNumWins < 25
		RETURN 0.096
	ELIF iNumWins < 50
		RETURN 0.085
	ELIF iNumWins < 70
		RETURN 0.075
	ELSE
		RETURN 0.064
	ENDIF
ENDFUNC

FUNC FLOAT GET_MP_WOBBLE_LEVEL(INT iNumWins)
	IF iNumWins < 10
		RETURN 0.080
	ELIF iNumWins < 25
		RETURN 0.0768
	ELIF iNumWins < 50
		RETURN 0.0736
	ELIF iNumWins < 70
		RETURN 0.0704
	ELSE
		RETURN 0.0672
	ENDIF
ENDFUNC

FUNC FLOAT GET_MP_REAL_WOBBLE_LEVEL(INT iNumWins)
	IF iNumWins < 10
		RETURN 0.107
	ELIF iNumWins < 25
		RETURN 0.10272
	ELIF iNumWins < 50
		RETURN 0.09844
	ELIF iNumWins < 70
		RETURN 0.09416
	ELSE
		RETURN 0.08988
	ENDIF
ENDFUNC
