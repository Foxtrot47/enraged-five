////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    MP_Darts.sch 	 	                    //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    MP Dart Defs        			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "Darts_Player.sch"
USING "Darts_Dart.sch"
USING "Darts_Board.sch"

CONST_INT NUM_MP_DART_PLAYERS 2
CONST_INT NUM_MP_DART_TURNS 3

// Multiplayer (network) state.
ENUM DARTS_MP_STATE
	DARTS_MPSTATE_INVALID,
	DARTS_MPSTATE_WAIT_PRE_INIT,
	DARTS_MPSTATE_PRE_INIT,
	DARTS_MPSTATE_PRE_INIT_WAIT,
	DARTS_MPSTATE_INIT,
	DARTS_MPSTATE_STREAMING,
	DARTS_MPSTATE_SETUP,
	DARTS_MPSTATE_READY_UP,
	DARTS_MPSTATE_TUTORIAL,
	DARTS_MPSTATE_INTRO_SYNC_SETUP,
	DARTS_MPSTATE_INTRO_SYNC_PLAY,
	DARTS_MPSTATE_THROW_OFF_SETUP,	// USED FOR TODO B* 805137
	DARTS_MPSTATE_THROW_OFF, 		// USED FOR TODO B* 805137
	DARTS_MPSTATE_SYNC_OBJECTS,
	DARTS_MPSTATE_SETUP_POST,
	DARTS_MPSTATE_WAIT_POST_MENU,
	DARTS_MPSTATE_IN_PROGRESS,
	DARTS_MPSTATE_GAME_TRANSITION,
	DARTS_MPSTATE_GAME_END,
	DARTS_MPSTATE_WAIT_POST_GAME,	
	DARTS_MPSTATE_TERMINATE_SPLASH,
	DARTS_MPSTATE_LEAVE,
	DARTS_MPSTATE_FAILED,
	DARTS_MPSTATE_END
ENDENUM

// Reason we are leaving the script.
ENUM DARTS_MP_TERMINATE_REASON
	DARTS_MPTERMINATEREASON_INVALID,
	DARTS_MPTERMINATEREASON_PLAYERLEFT,
	DARTS_MPTERMINATEREASON_PLAYER_NO_RESPONSE,
	DARTS_MPTERMINATEREASON_ENDGAME
ENDENUM

ENUM DARTS_MP_LEAVE_STATE
	DARTS_MPLEAVESTATE_BIG_MESSAGE,
	DARTS_MPLEAVESTATE_SWOOP_UP,
	DARTS_MPLEAVESTATE_LEADERBOARD,
	DARTS_MPLEAVESTATE_OUT_FADE,
	DARTS_MPLEAVESTATE_OUT_ANIM
ENDENUM

STRUCT MP_DARTS_GAME	
	// structs
	// since both players has an mp darts game, only keep track of turns
	DART				Darts[NUM_MP_DART_TURNS] //[NUM_MP_DART_PLAYERS][NUM_MP_DART_TURNS] 
	//DART				TutorialDarts[NUM_DART_TURNS]  // worry about the tutorial on the second pass
	DARTS_BOARD			dBoard
	//DARTS_THROW_STYLE	eThrowStyle = DARTS_STYLE_IV //DARTS_STYLE_STICK	// if new controls get approved, bring back in
	
	// var's	
	INT		iThrowsTaken[NUM_MP_DART_PLAYERS]
	INT		iBullsEyesHit[NUM_MP_DART_PLAYERS]
	INT 	iGamesWon
	INT 	iGamesLost
	
	//BOOL 	bHasQuitTextPrinted
	BOOL 	bEndGameMessageDisplayed
	BOOL 	bWinNoise
	BOOL	bDartWinner
	//BOOL	bDartsClash // TODO:  implement so that clash audio will get played
	//BOOL	bOneDartAway // TODO:  implement so that the one dart away prompt will play
	BOOL 	bAnotherCamActive
	
	structPedsForConversation DialogueStruct
ENDSTRUCT
