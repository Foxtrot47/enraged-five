////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    MP_Darts_Broadcast_lib.sch              //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Dart Broadcast Procs and Funcs		  //
//                                                           //
//////////////////////////////////////////////////////////////

//USING "MP_Darts_Broadcast.sch"
//USING "MP_Darts_Client.sch"
//USING "Darts_Dart.sch"

// Send a message to all clients
//PROC DARTS_SEND_BROADCAST_EVENT(DARTS_BROADCAST_EVENT & sBroadcast, INT iPlayerFlags)
//	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sBroadcast, SIZE_OF(sBroadcast), iPlayerFlags)
//ENDPROC

// Currently just using concurrent client data to update reticule info, so not this at the moment
//// Broadcast an update whenever a dart is thrown
//PROC DARTS_BROADCAST_RETICLE_UPDATE_TO_OTHER_PLAYER(vector vLandingPos, INT iSelfID, DARTS_PLAYER_BROADCAST_DATA & playerData[])
//	DARTS_BROADCAST_EVENT eventToSend
//	
//	// Filling up the broadcast event	
//	eventToSend.eEventType = DARTSBROADCASTEVENTTYPE_DARTSTHROW_UPDATE
//	eventToSend.vDartLandingPos = vLandingPos
//	
//	// Sending out the broadcast event
//	DARTS_SEND_BROADCAST_EVENT(eventToSend, SPECIFIC_PLAYER(playerData[1-iSelfID].piCurPlayerID))
//ENDPROC

// Sets dart landing position when other player makes a throw
// Call this every frame when player is in wait state
//PROC DARTS_BROADCAST_PROCESS_EVENTS (DART & Darts)
//	DARTS_BROADCAST_EVENT eventDetails
//	INT iCount
//	VECTOR vDartLandingPos
//	
//	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
//		// Got an event we care about
//		IF (GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount) = EVENT_NETWORK_SCRIPT_EVENT)
//			GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, eventDetails, SIZE_OF(eventDetails))
//						
//			//  Place the condition here that will tell us what action to take
//			IF eventDetails.eEventType = DARTSBROADCASTEVENTTYPE_DARTSTHROW_UPDATE
//				vDartLandingPos = eventDetails.vDartLandingPos
//				Darts.vTarget = vDartLandingPos
//			ENDIF
//			
//		ENDIF
//	ENDREPEAT
//ENDPROC
