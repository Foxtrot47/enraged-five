////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    MP_Darts_Client.sch                     //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Dart Client Defs       			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "MP_Darts.sch"

// TODO: These are all placeholder, fill out with Darts specific values
ENUM DARTS_CLIENT_FLAGS
	DARTS_CLIENTFLAG_SYNC_COMPLETE 				=	 1,
	DARTS_CLIENTFLAG_SETUP_SCORE_CONTROLS 		=	 2,
	DARTS_CLIENTFLAG_DONE_WITH_DART_THROWS 		=	 3,
	DARTS_CLIENTFLAG_WIN_LOSS_REACT				=	 4,
	DARTS_CLIENTFLAG_MATCH_MADE 		 		=	 5,
	DARTS_CLIENTFLAG_RETHROW_NEEDED		 		=	 6,
	DARTS_CLIENTFLAG_QUIT_ATTEMPTED 	 		=	 7,
	DARTS_CLIENTFLAG_IN_POSITION	 	 		=	 8,
	DARTS_CLIENTFLAG_DANGER_DART	 	 		=	 9,
	DARTS_CLIENTFLAG_TUTORIAL_SKIP				=	10,
	DARTS_CLIENTFLAG_TUTORIAL_DONE				=	11,
	DARTS_CLIENTFLAG_AT_TUTORIAL				=	12,
	DARTS_CLIENTFLAG_WINNERD					=	13,
	DARTS_CLIENTFLAG_LOSERD						=	14,
	DARTS_CLIENTFLAG_AT_THROWOFF				=	15,
	DARTS_CLIENTFLAG_SINGLE_PLAYER				=	16,
	DARTS_CLIENTFLAG_NEW_CHALLENGER				=	17,
	DARTS_CLIENTFLAG_I_AM_SPECTATOR				=	18,
	DARTS_CLIENTFLAG_I_AM_SCTV					=	19,
	DARTS_CLIENTFLAG_PARTICIPANT_SET			=	20,
	DARTS_CLIENTFLAG_SCORING_DONE				=	21,
	DARTS_CLIENTFLAG_QUIT_CONFIRMED				=	22
ENDENUM


ENUM DARTS_MP_THROW_STAGE
	DARTS_MPTHROW_AIM,
	DARTS_MPTHROW_THROW,
	DARTS_MPTHROW_SCORE,
	DARTS_MPTHROW_TURN_CHANGE
ENDENUM

// might not need these... could probably just go off the throw stages
ENUM DARTS_MP_WAIT_STAGE
	DARTS_MPWAIT_AIM,
	DARTS_MPWAIT_THROW,
	DARTS_MPWAIT_SCORE,
	DARTS_MPWAIT_TURN_CHANGE
ENDENUM

ENUM DARTS_MP_SPEC_STAGE
	DARTS_MPSPEC_AIM,
	DARTS_MPSPEC_THROW,
	DARTS_MPSPEC_TURN_CHANGE
ENDENUM

ENUM DARTS_MP_PLAYER_IDS
	DARTSMP_AWAY 	= 0, 
	DARTSMP_HOME 	= 1,
	DARTSMP_SPEC1	= 2,
	DARTSMP_SPEC2	= 3,
	DARTSMP_IDS 	= 4
ENDENUM

// Broadcast Data. Client can write SELF, and read all others.
STRUCT DARTS_PLAYER_BROADCAST_DATA
	DARTS_MP_STATE 			eMPState
	
	DARTS_MP_THROW_STAGE 	eMPThrowStage
	DARTS_MP_WAIT_STAGE 	eMPWaitStage
	//PLAYER_INDEX			piCurPlayerID
	PED_INDEX				piOther
	//TEXT_LABEL_63 			sName
	
	DART 					Darts[NUM_MP_DART_TURNS]
	DART 					ClientDart	
	//VECTOR				vDartLandingPos
	VECTOR					vReticlePos	
	BOOL					bDartThrown
	BOOL					bDartAim
	BOOL					bDoneScoring
	BOOL					bDoubleThrow
	BOOL					bResetDone
	BOOL					bSwitchPlayer
	BOOL					bDoRematch
	BOOL					bDoQuit
	INT 					iClientFlags
	
	INT						iParticipantID
	INT						iOffsetPlayerID
ENDSTRUCT

// Mutator for the player's MP state
PROC DARTS_SET_PLAYER_MP_STATE(DARTS_PLAYER_BROADCAST_DATA & playerData, DARTS_MP_STATE eMPState)
	playerData.eMPState = eMPState
ENDPROC

FUNC DARTS_MP_STATE DARTS_GET_PLAYER_MP_STATE(DARTS_PLAYER_BROADCAST_DATA & playerData)
	RETURN playerData.eMPState
ENDFUNC

PROC DARTS_SET_PLAYER_MP_THROW_STAGE(DARTS_PLAYER_BROADCAST_DATA & playerData, DARTS_MP_THROW_STAGE eMPThrowStage)
	playerData.eMPThrowStage = eMPThrowStage
ENDPROC

FUNC DARTS_MP_THROW_STAGE DARTS_GET_PLAYER_MP_THROW_STAGE(DARTS_PLAYER_BROADCAST_DATA & playerData)
	RETURN playerData.eMPThrowStage
ENDFUNC

PROC DARTS_SET_PLAYER_MP_WAIT_STAGE(DARTS_PLAYER_BROADCAST_DATA & playerData, DARTS_MP_WAIT_STAGE eMPWaitStage)
	playerData.eMPWaitStage = eMPWaitStage
ENDPROC

FUNC DARTS_MP_WAIT_STAGE DARTS_GET_PLAYER_MP_WAIT_STAGE(DARTS_PLAYER_BROADCAST_DATA & playerData)
	RETURN playerData.eMPWaitStage
ENDFUNC

//// Get the player's network name (i.e. gamertag).
//FUNC TEXT_LABEL_63 DARTS_GET_PLAYER_NAME(DARTS_PLAYER_BROADCAST_DATA& playerData)
//	RETURN playerData.sName
//ENDFUNC

//// Set the player's network name (i.e. gamertag).
//PROC DARTS_SET_PLAYER_NAME(DARTS_PLAYER_BROADCAST_DATA& playerData, STRING sName)
//	playerData.sName = sName
//ENDPROC

// Sets a flag in the client BD
PROC DARTS_SET_CLIENT_FLAG(DARTS_PLAYER_BROADCAST_DATA & playerData, DARTS_CLIENT_FLAGS eFlag)
	SET_BIT(playerData.iClientFlags, ENUM_TO_INT(eFlag))
ENDPROC

// Clears a flag in the client BD
PROC DARTS_CLEAR_CLIENT_FLAG(DARTS_PLAYER_BROADCAST_DATA & playerData, DARTS_CLIENT_FLAGS eFlag)
	CLEAR_BIT(playerData.iClientFlags, ENUM_TO_INT(eFlag))
ENDPROC

// Checks a flag in the client BD
FUNC BOOL DARTS_CHECK_CLIENT_FLAG(DARTS_PLAYER_BROADCAST_DATA & playerData, DARTS_CLIENT_FLAGS eFlag)
	RETURN IS_BIT_SET(playerData.iClientFlags, ENUM_TO_INT(eFlag))
ENDFUNC
