////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts.sc                                //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Darts Minigame for V     			  //
//                                                           //
//////////////////////////////////////////////////////////////

USING "commands_player.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_brains.sch"
USING "friendactivity_public.sch"
USING "net_fps_cam.sch"

USING "Darts.sch"
USING "Darts_Core.sch"
USING "Darts_Player_lib.sch"
USING "Darts_UI_lib.sch"
USING "Darts_Audio.sch"
USING "Darts_Args.sch"
USING "Darts_Stats_lib.sch"
USING "Darts_Leaderboard_lib.sch"
USING "script_oddjob_funcs.sch"

#IF IS_DEBUG_BUILD
USING "Darts_Debug.sch"
#ENDIF

PED_INDEX piDartPlayers[NUM_DART_PLAYERS]

BOOL bZoomCamSkip
BOOL bEndGameDartZoomTrans
BOOL bEndGameMessageDisplayed
BOOL bBoredAudioPlayedThisTurn
BOOL bOneDartAudioPlayed
BOOL bAnyAudioPlayedThisTurn
BOOL bOneDartHelpShown
BOOL bVehicleSaved
BOOL bHasQuitTextPrinted

// per B* 843808 "Can we skip opponent’s turn instantly", turn this on or off for instant skips
BOOL bInstantSkip = TRUE
BOOL bAISkipped
BOOL bDartsWon
//BOOL bSubtitlesMoved
BOOL bNewOpponent
BOOL bGameOverSpeechPlayed
BOOL bLogCompletion = FALSE
BOOL bInSyncCam
BOOL bFranklin
BOOL bOutroRequested
BOOL bOutroStreamed
BOOL bActivateScoreBoard

BOOL bDartsLBPredictDone
BOOL bDartsLBWrite
BOOL bDartsProfilesReady

VEHICLE_SAVE_ARGS VehicleSaveArgs

//INT iDartStartTime
INT iDartZoom
INT iAllowSkipCutsceneTime
INT iSkipAILoopCatch
INT iStaminaXP
INT iLevelBit
INT iSceneID
INT iAnimLimit

enumActivityLocation DartsLoc
enumActivityResult DartsResult

MODEL_NAMES modelOpponent

SEQUENCE_INDEX siTemp

CAMERA_INDEX camDartsSync

DARTS_LOCATION DartsLocation

FIRST_PERSON_CAM_STRUCT fpDartsCam
VECTOR vFPDartsPos, vFPDartsRot

VECTOR vRockClubSpawnPos 	= <<-573.1373, 294.0269, 78.1765>>
VECTOR vRockClubSpawnPos2 	= <<-574.1169, 292.7964, 78.1766>>
VECTOR vHickBarSpawnPos		= <<1995.29517, 3050.08447, 46.91535>>//<<1995.6138, 3049.8774, 46.2153>>
VECTOR vHickBarSpawnPos2	= <<1995.4880, 3047.3833, 46.91535>> //<<1996.0013, 3048.4270, 46.2153>>
FLOAT fRockClubHeading		= 172.6813
FLOAT fRockClubHeading2		= 274.5094
FLOAT fHickBarHeading		= 142.6717
FLOAT fHickBarHeading2		= 44.8785

enumCharacterList eDartsPlayerOne, eDartsPlayerTwo
STRING sDartsPlayerOne, sDartsPlayerTwo
INT iP2NameHash

STRING sFacialDictPed
STRING sFacialDictPlayer
STRING sFacialClip[3]
STRING sOutroWin[3]
STRING sOutroLoss[3]
STRING sOutroCam[3]
								
#IF IS_DEBUG_BUILD
//BOOL bDartsNoSocialClub
DEBUG_POS_DATA DebugBoard
SPRITE_PLACEMENT spriteDebug
SCALEFORM_INDEX siScoreBoardDebug
#ENDIF

/// PURPOSE:
///    Sets peds as no longer needed
/// PARAMS:
///    piDartOpp - The ped opponent to be cleaned up
PROC CLEANUP_PEDS(PED_INDEX & piDartOpp)
//	VECTOR vRockScenPos1, vRockScenPos2, vHickScenPos1, vHickScenPos2
//	vRockScenPos1 = <<-555.543640,284.138580,81.176231>>
//	vRockScenPos2 = <<-572.095398,286.635406,78.176636>>
//	vHickScenPos1 = <<1990.397827,3045.063965,46.215023>> 
//	vHickScenPos2 = <<1985.908203,3054.239014,46.215214>>
	
	IF DOES_ENTITY_EXIST(piDartOpp)
	AND NOT IS_PED_INJURED(piDartOpp)
		
		CLEAR_SEQUENCE_TASK(siTemp)
		OPEN_SEQUENCE_TASK(siTemp)
			IF DOES_ENTITY_EXIST(piDartPlayers[0])			
				TASK_LOOK_AT_ENTITY(NULL, piDartPlayers[0], 8000, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
			ENDIF
			TASK_STAND_STILL(NULL, 1000)
			IF bNewOpponent
				IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_ALT_START_POS)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, PICK_VECTOR((DartsLocation = DARTSLOCATION_ROCK_CLUB), vRockClubSpawnPos2, vHickBarSpawnPos2),
										PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 
										PICK_FLOAT((DartsLocation = DARTSLOCATION_ROCK_CLUB), fRockClubHeading2, fHickBarHeading2))
//					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, PICK_VECTOR((DartsLocation = DARTSLOCATION_ROCK_CLUB), vRockScenPos1, vHickScenPos1), PEDMOVEBLENDRATIO_WALK,
//												DEFAULT_TIME_BEFORE_WARP, 1.0)
//					IF DOES_SCENARIO_EXIST_IN_AREA(PICK_VECTOR((DartsLocation = DARTSLOCATION_ROCK_CLUB), vRockScenPos1, vHickScenPos1), 2.0, TRUE)
//						TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, PICK_VECTOR((DartsLocation = DARTSLOCATION_ROCK_CLUB), vRockScenPos1, vHickScenPos1), 2.00)
//					ELSE
//						TASK_WANDER_STANDARD(NULL)
//					ENDIF
				ELSE
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, PICK_VECTOR((DartsLocation = DARTSLOCATION_ROCK_CLUB), vRockClubSpawnPos, vHickBarSpawnPos),
										PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 
										PICK_FLOAT((DartsLocation = DARTSLOCATION_ROCK_CLUB), fRockClubHeading, fHickBarHeading))
				ENDIF
				TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
			ELSE
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, PICK_VECTOR((DartsLocation = DARTSLOCATION_ROCK_CLUB), vRockClubSpawnPos, vHickBarSpawnPos),
										PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 
										PICK_FLOAT((DartsLocation = DARTSLOCATION_ROCK_CLUB), 172.6813, 142.6717))
				TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_IMPATIENT", 0, TRUE) //"WORLD_HUMAN_DRINKING")
			ENDIF
		CLOSE_SEQUENCE_TASK(siTemp)
		TASK_PERFORM_SEQUENCE(piDartOpp, siTemp)
		
		SET_PED_KEEP_TASK(piDartOpp, TRUE)
		
		//SET_PED_AS_NO_LONGER_NEEDED(piDartOpp)
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(modelOpponent)
ENDPROC

/// PURPOSE:
///    Ends the dart game script
/// PARAMS:
///    DartGame - All the DART_GAME parts are cleaned out
PROC CLEANUP_DARTS_GAME(DART_GAME & DartGame, DARTS_UI & DartsUI, END_SCREEN_DATASET & DartsEndScreen)
	DISPLAY_AREA_NAME(TRUE)
	
	DARTS_STATS_UPDATE(DARTS_STAT_AVG_DARTS_MATCH)
//	DARTS_STATS_UPDATE(DARTS_STAT_TIME_PLAYED, GET_GAME_TIMER() - iDartStartTime)
	
	CLEANUP_DARTS_PC_CONTROLS()
	
	//cleanups
	IF bVehicleSaved
		ODDJOB_RESTORE_SAVED_VEHICLE(VehicleSaveArgs)
	ENDIF
	
	
	INT iTemp1, iTemp2
	REPEAT NUM_DART_PLAYERS iTemp1
		REPEAT NUM_DART_TURNS iTemp2
			CLEANUP_DART(DartGame.Darts[iTemp1][iTemp2])
		ENDREPEAT
	ENDREPEAT
	
	SET_MODEL_AS_NO_LONGER_NEEDED(modelDart1)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelDart2)
	
	IF NOT IS_PED_INJURED(piDartPlayers[0]) AND NOT IS_PED_INJURED(piDartPlayers[1])
		TASK_CLEAR_LOOK_AT(piDartPlayers[0])
		TASK_CLEAR_LOOK_AT(piDartPlayers[1])
	ENDIF
	
	CLEANUP_RETICLE()
	SET_MODEL_AS_NO_LONGER_NEEDED(modelReticleRegular)
	
	CLEANUP_HUD()
	
	CLEANUP_SC_LEADERBOARD_UI(DartsUI.uiLeaderboard)
	
	CLEANUP_MG_BIG_MESSAGE(DartsUI.siBigMessage)
//	CLEANUP_MG_MIDSIZED_MESSAGE(DartsUI.siMidMessage)
	
	IF NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())	
		SET_PED_AS_GROUP_MEMBER(FRIEND_A_PED_ID(), PLAYER_GROUP_ID())
		finishActivity(DartsLoc, DartsResult)
	ELSE
		CLEANUP_PEDS(piDartPlayers[1])
	ENDIF
	
	DISABLE_CELLPHONE(FALSE)
	
	ENDSCREEN_SHUTDOWN(DartsEndScreen)
	
	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	
	RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
	
	//CLEANUP_MENU_ASSETS()			
	
	IF IS_PLAYER_ONLINE()
		
		IF DartGame.bWriteToLeaderboard
		AND NOT bDartsLBWrite
			CDEBUG1LN(DEBUG_DARTS, "online, emergency writing to social club leaderboard")
			WRITE_DARTS_SCLB_DATA(LEADERBOARD_MINI_GAMES_DARTS,FALSE)
		ENDIF
		
	ENDIF
	
	CLEAR_RANK_REDICTION_DETAILS()
	SC_LEADERBOARD_CACHE_CLEAR_ALL()
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(DartsUI.dartsLB_control)
	
	CDEBUG1LN(DEBUG_DARTS, "All Cleaned Up")
	
	// Put a small wait so that player doesn't throw a punch if he's exiting
	WAIT(200)
	
	// Log completion if need be.
	IF bLogCompletion
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_DARTS)
		ODDJOB_AUTO_SAVE()
	ENDIF
	
	DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Grab the necessary assets for Darts
PROC REQUEST_STREAMS(DARTS_SCOREBOARD & DartsSB, DARTS_UI & DartsUI)
	REQUEST_MODEL(modelDart1)
	REQUEST_MODEL(modelDart2)
	REQUEST_MODEL(modelReticleRegular)
	REQUEST_MODEL(modelOpponent)
	
	REQUEST_STREAMED_TEXTURE_DICT("Darts")
	REQUEST_STREAMED_TEXTURE_DICT("ShopUI_Title_Darts")
	
//	REQUEST_ANIM_DICT("mini@dartsintro")
//	REQUEST_ANIM_DICT("mini@dartsintro_alt1")
	REQUEST_ANIM_DICT(sFacialDictPed)
	REQUEST_ANIM_DICT(sFacialDictPlayer)
	
	DartsSB.siScoreBoard			= REQUEST_SCALEFORM_MOVIE("darts_scoreboard")
	DartsUI.uiLeaderboard			= REQUEST_SC_LEADERBOARD_UI()
	DartsUI.siBigMessage.siMovie	= REQUEST_MG_BIG_MESSAGE()
	DartsUI.siMidMessage.siMovie	= REQUEST_MG_MIDSIZED_MESSAGE()
	
	#IF IS_DEBUG_BUILD
	siScoreBoardDebug		= REQUEST_SCALEFORM_MOVIE("darts_scoreboard")
	#ENDIF
	
	REQUEST_ADDITIONAL_TEXT("DARTS", MINIGAME_TEXT_SLOT)
ENDPROC

/// PURPOSE:
///    Asset loading checker
/// RETURNS:
///    TRUE if all the needed assets have loaded
FUNC BOOL HAVE_STREAMS_LOADED(DARTS_SCOREBOARD & DartsSB, DARTS_UI & DartsUI)
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\DARTS")	
		CDEBUG1LN(DEBUG_DARTS, "Darts Audio loading")
		RETURN FALSE
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FAMILY1_2")	
		CDEBUG1LN(DEBUG_DARTS, "Family Audio loading")
		RETURN FALSE
	ENDIF
			
	IF NOT HAS_MODEL_LOADED(modelDart1)
	OR NOT HAS_MODEL_LOADED(modelDart2)
	OR NOT HAS_MODEL_LOADED(modelReticleRegular)
	OR NOT HAS_MODEL_LOADED(modelOpponent)
	
	OR NOT HAS_SCALEFORM_MOVIE_LOADED(DartsSB.siScoreBoard)
	OR NOT HAS_SCALEFORM_MOVIE_LOADED(DartsUI.uiLeaderboard)
	OR NOT HAS_SCALEFORM_MOVIE_LOADED(DartsUI.siBigMessage.siMovie)
	OR NOT HAS_SCALEFORM_MOVIE_LOADED(DartsUI.siMidMessage.siMovie)
	
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
	
	OR NOT HAS_STREAMED_TEXTURE_DICT_LOADED("Darts")
	
//	OR NOT HAS_ANIM_DICT_LOADED("mini@dartsintro")
//	OR NOT HAS_ANIM_DICT_LOADED("mini@dartsintro_alt1")
	OR NOT HAS_ANIM_DICT_LOADED(sFacialDictPed)
	OR NOT HAS_ANIM_DICT_LOADED(sFacialDictPlayer)
	
	OR NOT LOAD_MENU_ASSETS()
		CDEBUG1LN(DEBUG_DARTS, "Streams loading")
		RETURN FALSE
	ENDIF	
	
	#IF IS_DEBUG_BUILD
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(siScoreBoardDebug)
		CDEBUG1LN(DEBUG_DARTS, "darts scoreboard loading")
		RETURN FALSE
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "darts scoreboard finished")
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC STRING GET_DARTS_RAVE_SCENE(DARTS_RAVE_SCENES DartsRaveScene)

	IF DartsLocation = DARTSLOCATION_HICK_BAR
		SWITCH DartsRaveScene
			CASE DARTSRAVE_GREETING	RETURN "DARTS_MEET_OPPONENT_BAR"
			CASE DARTSRAVE_MENU		RETURN "DARTS_MENU_BAR"
			CASE DARTSRAVE_MATCH	RETURN "DARTS_START_MATCH_BAR"
			CASE DARTSRAVE_COMPLETE	RETURN "DARTS_MATCH_COMPLETE_BAR"
		ENDSWITCH
	ELIF DartsLocation = DARTSLOCATION_ROCK_CLUB
		SWITCH DartsRaveScene
			CASE DARTSRAVE_GREETING	RETURN "DARTS_MEET_OPPONENT_ROCK"
			CASE DARTSRAVE_MENU		RETURN "DARTS_MENU_ROCK"
			CASE DARTSRAVE_MATCH	RETURN "DARTS_START_MATCH_ROCK"
			CASE DARTSRAVE_COMPLETE	RETURN "DARTS_MATCH_COMPLETE_ROCK"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC INT GET_WHICH_DARTS_OUTRO()
	INT iWhichOutro
	
	IF NOT bFranklin
		iWhichOutro = (GET_RANDOM_INT_IN_RANGE() % 3)
	ELSE
		iAnimLimit++
		iWhichOutro = iAnimLimit % 2
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "outro chosen is ", iWhichOutro)
	RETURN iWhichOutro
ENDFUNC

/// PURPOSE:
///    Manages the process in which players quit Darts
/// PARAMS:
///    DartGame - 
PROC PLAYER_QUIT_CONTROL(DART_GAME & DartGame)
	
	BOOL bWaitToQuit = FALSE
	//BOOL bHasQuitTextPrinted = FALSE
	
	// Check to see if the player has released CROSS (just do CIRCLE too)
	IF NOT bNoCrossConflict
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) 
		AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			bNoCrossConflict = TRUE
		ELSE
			bNoCrossConflict = FALSE
		ENDIF
	ENDIF		

	IF DartGame.Stage <> STAGE_FINISHED
		IF bWaitToQuit
			IF bNoCrossConflict AND
				bCrossButtonReleased = FALSE
				bNoCrossConflict	 = FALSE
				bWaitToQuit			 = FALSE
				bHasQuitTextPrinted	 = FALSE
								
				CLEAR_HELP()	
			ENDIF
		ENDIF
		
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
		OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
			CDEBUG1LN(DEBUG_DARTS, "quit just attempted")
			IF NOT bHasQuitTextPrinted
				DartGame.Stage = STAGE_PLAYER_QUIT
				bHasQuitTextPrinted = TRUE
				CLEAR_HELP()
			ENDIF			

		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_QUIT")
				bWaitToQuit = TRUE
			ELSE
				bWaitToQuit = FALSE
				bHasQuitTextPrinted = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Manages if the player wants to skip some of the in-game text transitions
PROC WAIT_FOR_TEXT_OR_SKIP()
	
	SETTIMERA(0)
	bNoCrossConflict = FALSE
	
	CLEAR_SMALL_PRINTS()
ENDPROC

/// PURPOSE:
///    Initiate the game, scores, darts
/// PARAMS:
///    DartGame - 
FUNC BOOL DO_START_GAME(DART_GAME & DartGame, DARTS_SCOREBOARD & DartsSB, DARTS_UI & DartsUI)
	INT i, j
	
	// Initialise the scoreboard
	// Reset the player scores to 301
	// Reset the number of throws and bullseyes
	REPEAT NUM_DART_PLAYERS i
		DartGame.Scores[i] 			= INIT_DART_SCORE
		DartGame.LastScore[i] 		= INIT_DART_SCORE
		//DartGame.iThrowsTaken[i] 	= 0
		//DartGame.iBullsEyesHit[i]	= 0
	ENDREPEAT
	CDEBUG1LN(DEBUG_DARTS, "[DO_START_GAME] Scores reset")
	
	DartGame.bDartWinner = FALSE
	
	REPEAT NUM_DART_PLAYERS i
		REPEAT  NUM_DART_TURNS j
			RESET_DART(DartGame.Darts[i][j])
		ENDREPEAT
		
		DARTS_CLEAR_SCORES(DartsSB, i)
		DARTS_ADD_SCORE(DartsSB, i, 301)
	ENDREPEAT
	CDEBUG1LN(DEBUG_DARTS, "[DO_START_GAME] Scoreboard reset")
	
	// reset the player
	iPlayr = 0
		
	// reset the turn
	iTurn = 0
	DartGame.iTurnScore = 0
	
	// reset the AI brain
	DartGame.AIBrain.iThrowTotal = 0
	
	// reset Zoom
	iDartZoom = 0
	DartGame.iNumThrowsTurn = 0
	iSteadyShotsUsed = 0
	
	// reset the end game dart zoom
	bEndGameDartZoomTrans = TRUE
	
	//reset the end game message
	bEndGameMessageDisplayed = FALSE

	// AUDIO CONTEXT CHECK
	bOneDartAudioPlayed = FALSE
	
	bSteadyShotHelp = FALSE
	
	// Win audio reset
	DartGame.bWinNoise = FALSE
	
	DartGame.bOneDartAway = FALSE
	
	bAISkipped = FALSE
	
	DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_IN_WIN_RANGE, FALSE)
	
	// set minigame control scaleforms
	//DARTS_ACTIVATE_QUIT_UI(DartsUI.uiQuitControls)
	//DARTS_ACTIVATE_ENDGAME_UI(DartsUI.uiEndGameControls)
	//DARTS_ACTIVATE_CONTROLS_UI(DartsUI.uiInGameControls)
	
	WAIT_FOR_TEXT_OR_SKIP() 
	
	DARTS_INIT_UI(DartsUI)
	
	IF DartGame.bAnotherCamActive
		START_DARTS_GAME_FADE_IN(DartGame.bAnotherCamActive)
		DartGame.bAnotherCamActive = FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks where the player is in the game, transition the number of legs and sets
/// RETURNS:
///    TRUE if all the sets have been completed, FALSE if there's more to go
FUNC BOOL EVALUATE_DART_GAME_PROGRESS(DART_GAME & DartGame)
	
	INT iEvaluator = PICK_INT(DartGame.bDartWinner, 0, 1)
	INT iWinsNeeded
	
	SWITCH DartGame.iNumDartLegs
		CASE 1	iWinsNeeded = 1 BREAK
		CASE 3	iWinsNeeded = 2 BREAK
		CASE 5	iWinsNeeded = 3 BREAK
	ENDSWITCH
	
	IF (DartGame.iDartGamesWon[iEvaluator] = iWinsNeeded)
		DartGame.iDartSetsWon[iEvaluator]++
		DartGame.iDartSBSetsWon[iEvaluator]++
		DartGame.iDartGamesWon[0] = 0
		DartGame.iDartGamesWon[1] = 0
		DartGame.bSetJustWon = TRUE
		
		IF iEvaluator = 0
			DartsStats[DARTS_SCLB_STAT_NUM_SETS_WON]++
		ENDIF
		
		CDEBUG1LN(DEBUG_DARTS, "EVALUATE_DART_GAME_PROGRESS - Set just won by ", iEvaluator, ", number of sets won is ", DartGame.iDartSetsWon[iEvaluator])
		
		IF (DartGame.iDartSetsWon[iEvaluator] = DartGame.iNumDartSets)
			DartGame.bSetJustWon = FALSE
			RETURN TRUE
		ENDIF
	ELSE
		//continue on until the set is completed
		CDEBUG1LN(DEBUG_DARTS, "EVALUATE_DART_GAME_PROGRESS - leg just won by ", iEvaluator, ", number of legs won is ", DartGame.iDartGamesWon[iEvaluator])
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Processes a players win (or loss), gets game ready for a replay or a quit
/// PARAMS:
///    DartGame - 
PROC GAME_WON(DART_GAME & DartGame)
	//GET_GAME_TIMER(i)
	//INCREMENT_INT_STAT_NO_MESSAGE(STAT_SCORE_DARTS_TOTAL_TIME_PLAYED, (i - gameStartTime))
	
	IF NOT DartGame.bWriteToLeaderboard
		DartGame.bWriteToLeaderboard = TRUE
	ENDIF
	
	DARTS_STATS_UPDATE(DARTS_STAT_NUM_GAMES)
	
	CLEAR_SMALL_PRINTS()
	CLEAR_HELP()
	
	IF IS_AUDIO_SCENE_ACTIVE(GET_DARTS_RAVE_SCENE(DARTSRAVE_MATCH))
		STOP_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_MATCH))
	ENDIF
	
	//WIN_GAME_CAM()
	IF DartGame.eThrowStyle = DARTS_STYLE_STICK
		BOARD_ZOOM_CAM()
	ENDIF

	IF iPlayr = 0
		//PRINT_NOW("DARTS_WON", 5000, 1)
		//PRINT_WITH_NUMBER_NOW("DARTS_REMAINL", 0, 5000, 1)
		DartGame.bDartWinner = TRUE
		bLogCompletion = TRUE
		
		DartGame.iDartGamesWon[iPlayr]++
		DartGame.iDartSBGamesWon[iPlayr]++
		// updating stats
		
		DARTS_STATS_UPDATE(DARTS_STAT_NUM_WINS)
		DartsStats[DARTS_SCLB_STAT_NUM_LEGS_WON]++
		//PLAY_WIN_AUDIO(piDartPlayers, bFirstGame)
		
		CDEBUG1LN(DEBUG_DARTS, "updating sc stats: DartsStats[DARTS_SCLB_STAT_NUM_LEGS_WON] = ", DartsStats[DARTS_SCLB_STAT_NUM_LEGS_WON])
		
		// TODO: implement ambient speech and overall game stat
		//IF isFriendActivity
		//	SAY_AMBIENT_SPEECH(opponent, "Darts_Player_Wins")
		//ENDIF
		//Implement when it's ready in V
		/*INCREMENT_INT_STAT_NO_MESSAGE(STAT_SCORE_DARTS_NUMBER_OF_WINS, 1)
		REGISTER_BEST_POSITION(STAT_SCORE_DARTS_SHORTEST_CHECK_OUT, throwsTaken)
		Minigames_Store_Result(MGR_PLAYER_WON)
		Flow_Achievements_Games_Complete(GAME_COMPLETION_DARTS)
		IncreaseOpponentSkill(dartsGameGlobal)*/
	ELSE
		//PRINT_NOW("DARTS_LOSE", 5000, 1)
		//PRINT_WITH_NUMBER_NOW("DARTS_REMAINL", 0, 5000, 1)
		
		DartGame.bDartWinner = FALSE
		CDEBUG1LN(DEBUG_DARTS, "******************** AI Throws: ", DartGame.AIBrain.iThrowTotal)
		
		DartGame.iDartGamesWon[iPlayr]++
		DartGame.iDartSBGamesWon[iPlayr]++
		
		DARTS_STATS_UPDATE(DARTS_STAT_NUM_LOSS)
		DartsStats[DARTS_SCLB_STAT_NUM_LEGS_LOST]++
		//PLAY_LOSS_AUDIO(piDartPlayers)
		
		CDEBUG1LN(DEBUG_DARTS, "updating sc stats: DartsStats[DARTS_SCLB_STAT_NUM_LEGS_LOST] = ", DartsStats[DARTS_SCLB_STAT_NUM_LEGS_LOST])
		
		// TODO: implement ambient speech and overall game stat
		//IF isFriendActivity
		//	SAY_AMBIENT_SPEECH(opponent, "Darts_Player_Loses")
		//ENDIF
		//Implement when it's ready in V
		/*INCREMENT_INT_STAT_NO_MESSAGE(STAT_SCORE_DARTS_NUMBER_OF_DEFEATS, 1)
		Minigames_Store_Result(MGR_PLAYER_LOST)
		DecreaseOpponentSkill(dartsGameGlobal)*/
	ENDIF
	
	DARTS_STATS_UPDATE(DARTS_STAT_WIN_PCT)
	
//	IF NOT IS_PED_INJURED(piDartPlayers[0]) AND NOT IS_PED_INJURED(piDartPlayers[1])
//		TASK_LOOK_AT_ENTITY(piDartPlayers[0], piDartPlayers[1], 10000, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
//		TASK_LOOK_AT_ENTITY(piDartPlayers[1], piDartPlayers[0], 11500, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
//	ENDIF
	
	RESTART_TIMER_NOW(DartGame.miscTimer) 
	
	IF EVALUATE_DART_GAME_PROGRESS(DartGame)
		CDEBUG1LN(DEBUG_DARTS, "darts game is won, resetting sets and legs")
		DartGame.iDartSetsWon[0] = 0
		DartGame.iDartSetsWon[1] = 0
		DartGame.iDartGamesWon[0] = 0
		DartGame.iDartGamesWon[1] = 0
		
		DartGame.iDartSBFullGamesWon[iPlayr]++
		
		IF NOT DartGame.bWinNoise
			IF DartGame.bDartWinner
				MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
			ELSE
				// lose sound gets played later
			ENDIF
			DartGame.bWinNoise = TRUE
		ENDIF
		
		IF DartGame.bDartWinner // player won
			DartsStats[DARTS_SCLB_STAT_NUM_WINS]++
			DartsResult = AR_playerWon
			CDEBUG1LN(DEBUG_DARTS, "updating sc stats: DartsStats[DARTS_SCLB_STAT_NUM_WINS] = ", DartsStats[DARTS_SCLB_STAT_NUM_WINS])
		ELSE
			DartsStats[DARTS_SCLB_STAT_NUM_LOSSES]++
			DartsResult = AR_buddyA_won
			CDEBUG1LN(DEBUG_DARTS, "updating sc stats: DartsStats[DARTS_SCLB_STAT_NUM_LOSSES] = ", DartsStats[DARTS_SCLB_STAT_NUM_LOSSES])
		ENDIF
		
		// Write leaderboard stats here.
		//WRITE_DARTS_SCLB_DATA(LEADERBOARD_MINI_GAMES_DARTS, FALSE)
		
		DartGame.Stage = STAGE_GAME_END
	ELSE
		DartGame.Stage = STAGE_GAME_TRANSITION
	ENDIF
	
	DartGame.TurnStage = DPS_AIM
	
	START_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_COMPLETE))
			
	CDEBUG1LN(DEBUG_DARTS, "darts won being set to true right now")
	bDartsWon = TRUE
	
	// reset the turn
//	iTurn = 0
//	iPlayr = 0
	DartGame.iTurnScore = 0
	
	bNoCrossConflict = FALSE	
	
	DartGame.LastScore[0] = 301
	DartGame.LastScore[1] = 301
	
	CDEBUG1LN(DEBUG_DARTS, "Stage is now ", DartGame.Stage)

ENDPROC


/// PURPOSE:
///    Call after every throw, checks for wins, busts, or player transition
/// PARAMS:
///    DartGame - 
PROC TURN_INCREMENT(DART_GAME & DartGame, DARTS_UI & DartsUI)

#IF IS_DEBUG_BUILD
	INT iDebugScores
#ENDIF
	
	DartGame.bDartsClash = FALSE
	
	IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_GAME_WON)
		// Update the player's score
		DartGame.Scores[iPlayr] -= DartGame.Darts[iPlayr][iTurn].iHitValue
		
		#IF IS_DEBUG_BUILD
			iDebugScores = DartGame.Scores[iPlayr]
			CDEBUG1LN(DEBUG_DARTS, "Player ", iPlayr, "'s Total score: ", iDebugScores)
		#ENDIF
		
		// Update the player's turn score
		DartGame.iTurnScore += DartGame.Darts[iPlayr][iTurn].iHitValue
		
		#IF IS_DEBUG_BUILD
			iDebugScores = DartGame.iTurnScore
			CDEBUG1LN(DEBUG_DARTS, "Player ", iPlayr, "'s Turn score: ", iDebugScores)
		#ENDIF
	
	ENDIF
	
	// if the game has been won and the initial score has been shown
	IF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_GAME_WON)
		CDEBUG1LN(DEBUG_DARTS, "**********GAME HAS BEEN WON**********")
		CDEBUG1LN(DEBUG_DARTS, "**********Stage is ", DartGame.TurnStage, "**********")
		
		GAME_WON(DartGame)
		
		CDEBUG1LN(DEBUG_DARTS, "Un-setting uiFlag for DARTS_UIFLAGS_GAME_WON in line 455")
		DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_GAME_WON, FALSE)
		
	ELSE
		DartGame.iThrowsTaken[iPlayr]++
		IF iPlayr = 0
			DARTS_STATS_UPDATE(DARTS_STAT_DARTS_THROWN)
			DartsStats[DARTS_SCLB_STAT_DARTS_THROWN]++
		ENDIF
		
		// if the game has been won
		IF (DartGame.Darts[iPlayr][iTurn].iHitMultiplier = 2 OR DartGame.Darts[iPlayr][iTurn].iHitValue = 50) 
		AND DartGame.Scores[iPlayr] = 0
			IF DartGame.Darts[iPlayr][iTurn].iHitValue = 50
				DartGame.iBullsEyesHit[iPlayr]++
				IF iPlayr = 0
					DARTS_STATS_UPDATE(DARTS_STAT_NUM_BULLSEYES)
					DartsStats[DARTS_SCLB_STAT_BULLS_EYES]++
				ENDIF
			ENDIF
			
			CDEBUG1LN(DEBUG_DARTS, "Setting uiFlag for DARTS_UIFLAGS_GAME_WON in line 467")
			DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, FALSE)
			DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, FALSE)
			DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, FALSE)
			DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_GAME_WON, TRUE)
			
			GAME_WON(DartGame)
			
			CDEBUG1LN(DEBUG_DARTS, "Un-setting uiFlag for DARTS_UIFLAGS_GAME_WON in line 481")
			DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_GAME_WON, FALSE)
			
			SETTIMERB(0)

		// The player has bust
		ELIF (DartGame.Scores[iPlayr] = 1 OR DartGame.Scores[iPlayr] < 0) 
		OR (DartGame.Scores[iPlayr] = 0 AND DartGame.Darts[iPlayr][iTurn].iHitMultiplier <> 2)
		
			CDEBUG1LN(DEBUG_DARTS, "**********Playr has bust**********")
			CDEBUG1LN(DEBUG_DARTS, "**********Stage is ", DartGame.TurnStage, "**********")
		
			IF DartGame.Scores[iPlayr] = 0 AND DartGame.Darts[iPlayr][iTurn].iHitMultiplier <> 2
				// The player hit the right number, but didn't double out.
				PRINT_WITH_NUMBER_NOW("DARTS_PLR_DUB", DartGame.Darts[iPlayr][iTurn].iHitValue, 5000, 0)
				DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, TRUE)
				
			ELIF DartGame.Scores[iPlayr] = 1
				DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, TRUE)
				
			ELSE	
				// output "you're bust text"
				IF iPlayr = 1
					PRINT_WITH_NUMBER_NOW("DARTS_OPP_BUST", DartGame.Darts[iPlayr][iTurn].iHitValue, 5000, 0)
				ELSE
					PRINT_WITH_NUMBER_NOW("DARTS_PLR_BUST", DartGame.Darts[iPlayr][iTurn].iHitValue, 5000, 0)
				ENDIF
			ENDIF		
			
			IF NOT bAnyAudioPlayedThisTurn
				IF PLAY_BUST_AUDIO(piDartPlayers[1 - iPlayr])
					bAnyAudioPlayedThisTurn = TRUE
					CDEBUG1LN(DEBUG_DARTS, "***** Bust Audio played by player ", 1 - iPlayr)
				ENDIF
			ENDIF
			
			// rollback the player's score at start of turn
			DartGame.Scores[iPlayr] = DartGame.LastScore[iPlayr]
			
			IF DartGame.TurnStage = DPS_SKIP_AI
				CDEBUG1LN(DEBUG_DARTS, "***** SKIP_AI MADE IT HERE! *****")
			ENDIF			
		
			SETTIMERA(0)
			DartGame.TurnStage = DPS_TURN_CHANGE
			
			IF iPlayr = 0
				DartGame.iBustStreak++
			ENDIF
			
			DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, TRUE)
			
		// The game continues
		ELSE
			
			// Increment stats for the results UI
			// Do bullseye audio						
			IF DartGame.Darts[iPlayr][iTurn].iHitValue = 50
				DartGame.iBullsEyesHit[iPlayr]++
				IF iPlayr = 0
					DARTS_TRIGGER_SPLASH(DartsUI, DARTSSPLASH_BULLSEYE)
					DARTS_STATS_UPDATE(DARTS_STAT_NUM_BULLSEYES)
					DartsStats[DARTS_SCLB_STAT_BULLS_EYES]++
				ENDIF
				// TODO: bullseye audio not recorded for player yet, make the ped say it for now
				IF NOT bAnyAudioPlayedThisTurn
				AND NOT bAISkipped
					IF PLAY_BULLSEYE_AUDIO(piDartPlayers[iPlayr])
						bAnyAudioPlayedThisTurn = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			
			IF DartGame.Darts[iPlayr][iTurn].iHitValue = 0
				// TODO: dart audio not recorded for player yet, make the ped say it for now
				IF NOT bAnyAudioPlayedThisTurn
				AND NOT bAISkipped
					PLAY_MISS_AUDIO(piDartPlayers[1 - iPlayr])
					bAnyAudioPlayedThisTurn = TRUE
				ENDIF
			ENDIF
			
			IF (DartGame.iTurnScore = 180) 
				IF NOT bAnyAudioPlayedThisTurn
				AND NOT bAISkipped
					IF PLAY_180_AUDIO(piDartPlayers[1], (iPlayr = 0))
						bAnyAudioPlayedThisTurn = TRUE
						CDEBUG1LN(DEBUG_DARTS, "***** 180 Audio Played *****")
					ENDIF
				ENDIF
				
				IF (iPlayr = 0)
					DARTS_TRIGGER_SPLASH(DartsUI, DARTSSPLASH_180)
					DARTS_STATS_UPDATE(DARTS_STAT_NUM_180S)
					DartsStats[DARTS_SCLB_STAT_NUM_180S]++
					// INCREMENT_INT_STAT_NO_MESSAGE(STAT_SCORE_DARTS_NUMBER_OF_180_HITS, 1)
					// AWARD_ACHIEVEMENT(ENUM_TO_INT(SCOREDARTGAME))	
				ENDIF
			ENDIF
			
			IF (DartGame.iTurnScore = 140) 
				// TODO: dart audio not recorded for player yet, make the ped say it for now
				IF NOT bAnyAudioPlayedThisTurn
				AND NOT bAISkipped
					IF PLAY_140_AUDIO(piDartPlayers[iPlayr], (iPlayr = 0))
						bAnyAudioPlayedThisTurn = TRUE
						CDEBUG1LN(DEBUG_DARTS, "***** 140 Audio Played *****")
					ENDIF
				ENDIF
			ENDIF
			
			// Do one dart away message			
			IF iPlayr = 0
				IF iTurn < 2
					
					IF DartGame.Scores[iPlayr] <= 170
					AND NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_END_HELP_READY)
						DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_END_HELP_READY, TRUE)
					ENDIF
					
					IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP_READY)
					AND DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_WIN_HELP)
						DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP_READY, TRUE)
					ENDIF
					
					IF ((DartGame.Scores[iPlayr] < 41 AND DartGame.Scores[iPlayr] > 0) AND (DartGame.Scores[iPlayr] % 2 = 0)) 
					OR DartGame.Scores[iPlayr] = 50
						
						IF DartGame.Scores[iPlayr] = 50
						//	CDEBUG1LN(DEBUG_DARTS, "-----One Dart Away Bullseye Should Print-----")
						ELSE
						//	CDEBUG1LN(DEBUG_DARTS, "-----One Dart Away Text Should Print-----")
						ENDIF
						
						// TODO: only recorded for peds for now, so just force it
						IF NOT bOneDartAudioPlayed
							PLAY_ONE_DART_AWAY_AUDIO(piDartPlayers[1])
							bOneDartAudioPlayed = TRUE	
						ENDIF
						
						IF NOT DartGame.bOneDartAway
							DartGame.bOneDartAway = TRUE
						ENDIF
						
						IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_IN_WIN_RANGE)
							DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_IN_WIN_RANGE, TRUE)
						ENDIF
						
					ELIF DartGame.bOneDartAway
						DartGame.bOneDartAway = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF iPlayr = 0 AND iTurn < 2 //AND ((DartGame.Scores[iPlayr] % 2 = 1) OR NOT DartGame.bOneDartAway)
				CDEBUG1LN(DEBUG_DARTS, "-----Score Text Should Have Printed-----")
				//PRINT_WITH_NUMBER_NOW("DARTS_REMAINL", DartGame.Scores[iPlayr], DEFAULT_GOD_TEXT_TIME, 0)
				PRINT_WITH_2_NUMBERS_NOW("DARTS_REMAIN", DartGame.LastScore[iPlayr] - DartGame.Scores[iPlayr], DartGame.Scores[iPlayr], DEFAULT_GOD_TEXT_TIME, 0)
			ELIF (iPlayr = 1) AND (iTurn = 2)			
				//PRINT_WITH_2_NUMBERS_NOW("DARTS_REMAIN2", DartGame.Darts[iPlayr][iTurn].iHitValue, DartGame.Scores[iPlayr], DEFAULT_GOD_TEXT_TIME, 0)
			ELSE
				// do a little pause after each dart if we're not sped up
				IF DartGame.TurnStage <> DPS_SKIP_AI					
					SKIP_AI_TURN_CHECK(DartGame.TurnStage)				
				ENDIF
			ENDIF
			
			DartGame.Darts[iPlayr][iTurn].bDoneScoring = TRUE
			// increment dart...
			iTurn++
							
			SETTIMERA(0)

			IF DartGame.TurnStage <> DPS_SKIP_AI
				IF DartGame.bOneDartAway AND DartGame.eThrowStyle = DARTS_STYLE_STICK
					DartGame.TurnStage = DPS_ONE_DART_HELPER
				ELSE
					DartGame.TurnStage = DPS_AIM
				ENDIF
			ENDIF		
		
			// If the player has had 3 throws, switch
			IF (iTurn = 3)			
				CDEBUG1LN(DEBUG_DARTS, "Player ", iPlayr, " finished, now we're switching")
				iTurn = 2
				DartGame.TurnStage = DPS_TURN_CHANGE				
			ENDIF
			
			IF iPlayr = 0
				DartGame.iBustStreak = 0
			ENDIF
			
			DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, FALSE)
			DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, FALSE)
			DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, FALSE)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Simulates a full or partial AI turn if player chooses to skip it
/// PARAMS:
///    DartGame - 
PROC DO_SKIP_AI_TURN(DART_GAME & DartGame, DARTS_UI & DartsUI)
	VECTOR vTemp

	iSkipAILoopCatch++  //this is reset at beginning of each turn
	CDEBUG1LN(DEBUG_DARTS, "iSkipAILoopCatch = ", iSkipAILoopCatch)

	iPlayr = 1
	iTurn = 0
 	
	bAISkipped = TRUE
	
	// loop through each of the 3 darts
	WHILE (iTurn < 3) AND DartGame.TurnStage = DPS_SKIP_AI
		CDEBUG1LN(DEBUG_DARTS, "-------------")
		CDEBUG1LN(DEBUG_DARTS, "DOING SKIP AI TURN - PLAYER ", iPlayr, "   - TURN ", iTurn)

		// DART HAS FINISHED
		IF DartGame.Darts[iPlayr][iTurn].bStuck

			CDEBUG1LN(DEBUG_DARTS, "######### DART HAS FINISHED")
			CDEBUG1LN(DEBUG_DARTS, "dart score ", iTurn, " = ", DartGame.Darts[iPlayr][iTurn].iHitValue)
			CDEBUG1LN(DEBUG_DARTS, "---------------------------")
		
			
			IF DartGame.Darts[iPlayr][iTurn].bDoneScoring
				// don't do anything... it's already there
				iTurn++
			
			ELSE
				// store the dart score
				CDEBUG1LN(DEBUG_DARTS, "Scoring Dart Turn ", iTurn, ", Player ", iPlayr)
				SCORE_DART(DartGame.Darts[iPlayr][iTurn], DartGame.DartsBoard)
				CDEBUG1LN(DEBUG_DARTS, "---------------------------")
				
				// Update the player's score
				TURN_INCREMENT(DartGame, DartsUI)
			ENDIF
			
		// DART IS IN FLIGHT
		ELIF DartGame.Darts[iPlayr][iTurn].bTravelling

			CDEBUG1LN(DEBUG_DARTS, "######### DART IS IN FLIGHT")
			IF bInstantSkip
				IF DOES_ENTITY_EXIST(DartGame.Darts[iPlayr][iTurn].object)
					DELETE_OBJECT(DartGame.Darts[iPlayr][iTurn].object)
					CDEBUG1LN(DEBUG_DARTS, "okay this turn's object was deleted:  ", iTurn)
				ENDIF
			
			ELSE
				DartGame.Darts[iPlayr][iTurn].bStuck = TRUE
				DartGame.Darts[iPlayr][iTurn].bTravelling = FALSE
				CDEBUG1LN(DEBUG_DARTS, "darts[playr][turn].offsetTarget = ", vTemp)
				
				// snap it to coords						   
				vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartGame.DartsBoard.vDartBoard, DartGame.DartsBoard.fBoardHeading,
																		<<DartGame.Darts[iPlayr][iTurn].vOffsetTarget.x, 
															   			DartGame.Darts[iPlayr][iTurn].vOffsetTarget.y + fDartBoardOriginOffset, 
															   			DartGame.Darts[iPlayr][iTurn].vOffsetTarget.z>>)
				
				IF DOES_ENTITY_EXIST(DartGame.Darts[iPlayr][iTurn].object)
					SET_ENTITY_COORDS(DartGame.Darts[iPlayr][iTurn].object, vTemp)
				ENDIF	
			ENDIF
			
			// store the dart score
			CDEBUG1LN(DEBUG_DARTS, "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ STATS BEFORE SCORING - iPlayr: ", iPlayr, ", iTurn:", iTurn)
			CDEBUG1LN(DEBUG_DARTS, "Scoring Dart Turn ", iTurn, ", Player ", iPlayr)
			SCORE_DART(DartGame.Darts[iPlayr][iTurn], DartGame.DartsBoard)
			CDEBUG1LN(DEBUG_DARTS, " $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ")
			
			// Update the player's score
			TURN_INCREMENT(DartGame, DartsUI)
			
			IF bInstantSkip
				IF bDartsWon
					CDEBUG1LN(DEBUG_DARTS, "creating win in-flight-dart number ", iTurn)
					
					DartGame.Darts[iPlayr][iTurn].object = CREATE_OBJECT(DartGame.Darts[iPlayr][iTurn].modelDart, vInitDartPos)
					
					//CDEBUG1LN(DEBUG_DARTS, "Darts[iPlayr][iturn].vOffsetTarget coords ", DartGame.Darts[iPlayr][iTurn].vOffsetTarget)
					// create it snapped to board
					vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartGame.DartsBoard.vDartBoard, DartGame.DartsBoard.fBoardHeading,
																			<<DartGame.Darts[iPlayr][iTurn].vOffsetTarget.x, 
																   			DartGame.Darts[iPlayr][iTurn].vOffsetTarget.y + fDartBoardOriginOffset, 
																   			DartGame.Darts[iPlayr][iTurn].vOffsetTarget.z>>)
					
					SET_ENTITY_COORDS(DartGame.Darts[iPlayr][iTurn].object, vTemp)
					
					// put some random spin on it
					FLOAT fTemp1
					fTemp1 = GET_RANDOM_FLOAT_IN_RANGE(0.0, 90.0)
					DartGame.Darts[iPlayr][iTurn].vRotation =  <<  90.0, fTemp1, DartGame.DartsBoard.fBoardHeading>>
					SET_ENTITY_ROTATION(DartGame.Darts[iPlayr][iTurn].object, DartGame.Darts[iPlayr][iTurn].vRotation, EULER_XYZ)
					
					DartGame.Darts[iPlayr][iTurn].bStuck = TRUE
					DartGame.Darts[iPlayr][iTurn].bTravelling = FALSE
				ENDIF
			ENDIF
			
		// DART HASN'T YET BEEN THROWN
		ELSE

			CDEBUG1LN(DEBUG_DARTS, "@@@@@@@@@@@@@ DART HASN'T YET BEEN THROWN")
			 
			// calculate it's coords
			GET_NEXT_AI_THROW(DartGame.Scores, DartGame.AIBrain)
			GET_AI_THROW_VECTOR(DartGame.Darts[iPlayr][iTurn], DartGame.DartsBoard, DartGame.TurnStage, DartGame.AIBrain)
			
			DartGame.iNumThrowsTurn++					
			
			// store the dart score
			CDEBUG1LN(DEBUG_DARTS, "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ STATS BEFORE SCORING - iPlayr: ", iPlayr, ", iTurn:", iTurn)
			CDEBUG1LN(DEBUG_DARTS, "Scoring Dart Turn ", iTurn, ", Player ", iPlayr)
			SCORE_DART(DartGame.Darts[iPlayr][iTurn], DartGame.DartsBoard)
			CDEBUG1LN(DEBUG_DARTS, " $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ")
			
			// we have to check for wins early if it's skip AI, otherwise the win may not register
			IF (DartGame.Darts[iPlayr][iTurn].iHitMultiplier = 2 OR DartGame.Darts[iPlayr][iTurn].iHitValue = 50) 
			AND DartGame.Scores[iPlayr] = 0
				IF DartGame.Darts[iPlayr][iTurn].iHitValue = 50
					DartGame.iBullsEyesHit[iPlayr]++
				ENDIF
				
				CDEBUG1LN(DEBUG_DARTS, "Setting uiFlag for DARTS_UIFLAGS_GAME_WON in line 758")
				DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, FALSE)
				DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, FALSE)
				DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, FALSE)
				DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_GAME_WON, TRUE)
				
				//PRINT_WITH_2_NUMBERS_NOW("DARTS_REMAIN2", DartGame.iTurnScore, DartGame.Scores[iPlayr], DEFAULT_GOD_TEXT_TIME, 0)
				
				SETTIMERB(0)
			ENDIF
			
			// Update the player's score
			TURN_INCREMENT(DartGame, DartsUI)
			
			IF bInstantSkip
				IF bDartsWon
					INT idx
					
					REPEAT (iTurn + 1) idx
						IF NOT DartGame.Darts[iPlayr][idx].bStuck
							CDEBUG1LN(DEBUG_DARTS, "creating win dart number ", idx)
							
							DartGame.Darts[iPlayr][idx].object = CREATE_OBJECT(DartGame.Darts[iPlayr][idx].modelDart, vInitDartPos)
							
							//CDEBUG1LN(DEBUG_DARTS, "Darts[iPlayr][iturn].vOffsetTarget coords ", DartGame.Darts[iPlayr][iTurn].vOffsetTarget)
							// create it snapped to board
							vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartGame.DartsBoard.vDartBoard, DartGame.DartsBoard.fBoardHeading,
																					<<DartGame.Darts[iPlayr][idx].vOffsetTarget.x, 
																		   			DartGame.Darts[iPlayr][idx].vOffsetTarget.y + fDartBoardOriginOffset, 
																		   			DartGame.Darts[iPlayr][idx].vOffsetTarget.z>>)
							
							IF DOES_ENTITY_EXIST(DartGame.Darts[iPlayr][idx].object)
								SET_ENTITY_COORDS(DartGame.Darts[iPlayr][idx].object, vTemp)
							ENDIF
							
							// put some random spin on it
							FLOAT fTemp1
							fTemp1 = GET_RANDOM_FLOAT_IN_RANGE(0.0, 90.0)
							DartGame.Darts[iPlayr][idx].vRotation =  <<  90.0, fTemp1, DartGame.DartsBoard.fBoardHeading>>
							SET_ENTITY_ROTATION(DartGame.Darts[iPlayr][idx].object, DartGame.Darts[iPlayr][idx].vRotation, EULER_XYZ)
							
							DartGame.Darts[iPlayr][idx].bStuck = TRUE
							DartGame.Darts[iPlayr][idx].bTravelling = FALSE
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF

		CDEBUG1LN(DEBUG_DARTS, "TURN AT END OF THROW- ", iTurn)
		CDEBUG1LN(DEBUG_DARTS, "@@@@@@@@@@@@@@@@@@@@@@@@@@@")
	ENDWHILE

	//iTurn = 0
	CDEBUG1LN(DEBUG_DARTS, "TURN AFTER AI SKIP THROWS HAVE COMPLETED- ", iTurn)
	
	DartGame.TurnStage = DPS_TURN_CHANGE // this is redundant since TURN_INCREMENT should already do this, but at least it will prevent inf loops
	
	IF iSkipAILoopCatch > 2
		SCRIPT_ASSERT("SKIP_AI was caught looping!  Please copy game log and attach to B* 248553")
		DartGame.TurnStage = DPS_TURN_CHANGE
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Handles whether another darts game will be started or if the darts session will end.
/// PARAMS:
///    DartGame - 
PROC REPLAY_GAME_LOOP(DART_GAME & DartGame, DARTS_UI & DartsUI, DARTS_SCOREBOARD & DartsSB, END_SCREEN_DATASET & DartsEndScreen, BOOL bUsedSetsLegs)
	
	DARTS_UI_RETVAL DartsUIRetVal
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		DISABLE_CONTROL_ACTION( FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	ENDIF
			
	IF IS_PLAYER_ONLINE()
		IF NOT bDartsLBPredictDone
			IF DARTS_RANK_PREDICTIONS(DartsUI, FALSE, bDartsLBWrite)
				DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SETUP_ENDUI, FALSE)
				bDartsLBPredictDone = TRUE
				CDEBUG1LN(DEBUG_DARTS, "rank predictions finished")
			ENDIF
		ENDIF
	ENDIF
	
	// wait for the player to pick an option
	IF bEndGameMessageDisplayed
	
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			SET_MOUSE_CURSOR_THIS_FRAME()
		ENDIF
	
		
		BOOL bProcessMenuInput = TRUE
		IF DartsUI.bDrawingLeaderboard
			IF NOT DartsUI.bLBDOnlineWarningDone AND NOT IS_PLAYER_ONLINE()
				DartsUI.bLBDOnlineWarningDone = DO_SIGNED_OUT_WARNING(DartsUI.iLBDWarningBitset)
				bProcessMenuInput = FALSE
				
				// After the warning, don't then transition to the leaderboards...
				IF DartsUI.bLBDOnlineWarningDone
					DartsUI.bDrawingLeaderboard = FALSE
					DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SETUP_ENDUI, FALSE)
					
					// Reset the warning display.
					DartsUI.bLBDOnlineWarningDone = FALSE
				ENDIF
			ELSE
				DARTS_DISPLAY_SOCIAL_CLUB_LEADERBOARD(DartsUI.dartsLB_control, DartsUI.uiLeaderboard) //, FALSE)
			ENDIF
			
			// Process input here.
			IF bProcessMenuInput
				UPDATE_SIMPLE_USE_CONTEXT(DartsUI.inGameControlContext)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					// No longer drawing the leaderboard, and re-do the input.
					DartsUI.bDrawingLeaderboard = FALSE
					DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SETUP_ENDUI, FALSE)
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
					// Continue.
					DartsUIRetVal = DARTS_UIRETVAL_FALSE
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
					// Rematch.
					DartsUIRetVal = DARTS_UIRETVAL_TRUE
				ENDIF
			ENDIF
			
			IF SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(DartsUI.dartsLB_control)
				IF NOT bDartsProfilesReady
					
					bDartsProfilesReady = TRUE
					
					// just do override here
					INIT_SIMPLE_USE_CONTEXT(DartsUI.inGameControlContext, FALSE, FALSE, TRUE, TRUE)
					ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "DARTS_CONT",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
					ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "DARTS_REMATCH",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
					ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "IB_BACK",		FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "SCLB_PROFILE",	FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
					SET_SIMPLE_USE_CONTEXT_FULLSCREEN(DartsUI.inGameControlContext)
					
					
					CDEBUG1LN(DEBUG_DARTS, "profile is ready, button should show")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_DARTS, "profiles not ready yet")
			ENDIF
		ELSE
			DartsUIRetVal = DARTS_UPDATE_END_MENU(DartsUI, FALSE, FALSE, bDartsLBPredictDone)
			
			RENDER_ENDSCREEN(DartsEndScreen)
		ENDIF
		
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) >= 0.995)
		OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID)
			bInSyncCam = TRUE
			
			IF DOES_CAM_EXIST(camDartsSync)
				DESTROY_CAM(camDartsSync)
				
				IF NOT IS_PED_INJURED(piDartPlayers[0])
				AND NOT IS_PED_INJURED(piDartPlayers[1])
					CLEAR_PED_TASKS_IMMEDIATELY(piDartPlayers[0])
					CLEAR_PED_TASKS_IMMEDIATELY(piDartPlayers[1])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(piDartPlayers[0])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(piDartPlayers[1])
				ENDIF
			ENDIF
			
			ANOTHER_GAME_BACKSIDE()
		ENDIF
		
		IF GET_TIMER_IN_SECONDS_SAFE(DartGame.miscTimer) > 2.0
			IF NOT bGameOverSpeechPlayed
				IF DartGame.bDartWinner
					BOOL bFirstGame
					IF (DartGame.iDartSBGamesWon[0] + DartGame.iDartSBGamesWon[1]) = 1
						bFirstGame = TRUE
					ELSE
						bFirstGame = FALSE
					ENDIF
					
					PLAY_WIN_AUDIO(piDartPlayers, bFirstGame)
				ELSE
					PLAY_LOSS_AUDIO(piDartPlayers)
				ENDIF
				bGameOverSpeechPlayed = TRUE
			ENDIF
		ENDIF
		
		// player wants another game
		IF DartsUIRetVal = DARTS_UIRETVAL_TRUE
			
			CLEAR_HELP()
			
			STOP_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_COMPLETE))
			
			IF NOT IS_PED_INJURED(piDartPlayers[0]) AND NOT IS_PED_INJURED(piDartPlayers[1])
				TASK_CLEAR_LOOK_AT(piDartPlayers[0])
				TASK_CLEAR_LOOK_AT(piDartPlayers[1])
			ENDIF
			
			//CLEANUP_MINIGAME_INSTRUCTIONS(DartsUI.uiQuitControls)
			//CLEANUP_MINIGAME_INSTRUCTIONS(DartsUI.uiEndGameControls)
			
			RESET_ENDSCREEN(DartsEndScreen)
			
			IF bUsedSetsLegs
				UPDATE_SB_SETS_LEGS(DartsSB, 0, 0, 0, 0)
			ENDIF
			
			DartGame.iThrowsTaken[0] 	= 0
			DartGame.iThrowsTaken[1] 	= 0
			DartGame.iBullsEyesHit[0]	= 0
			DartGame.iBullsEyesHit[1]	= 0
			
			iPlayr = 1
			CANCEL_TIMER(DartGame.miscTimer)
			DartGame.Stage = STAGE_STARTING
			
			bGameOverSpeechPlayed = FALSE
			bDartsWon = FALSE
			
			bInSyncCam = FALSE
			
			CDEBUG1LN(DEBUG_DARTS, "rematch happening, resetting rank prediction stuff")
			CLEAR_RANK_REDICTION_DETAILS()
			bDartsLBPredictDone = FALSE
			bDartsLBRead = FALSE
			DartsUI.bDrawingLeaderboard = FALSE
			bDartsLBWrite = FALSE
			bDartsProfilesReady = FALSE
			
			CLEANUP_SOCIAL_CLUB_LEADERBOARD(DartsUI.dartsLB_control)
			SC_LEADERBOARD_CACHE_CLEAR_ALL()
			
		// player wants to quit 
		ELIF DartsUIRetVal = DARTS_UIRETVAL_FALSE
			//Set endscreen to transition-out
			ENDSCREEN_START_TRANSITION_OUT(DartsendScreen)
			
			STOP_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_COMPLETE))
			
			// give player his money
			IF (DartGame.iDartGamesWon[0] * DART_MONEY_WON) > 0
				CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_TAXI, (DartGame.iDartGamesWon[0] * DART_MONEY_WON))
			ENDIF
			
			CLEAR_PRINTS()
			DartGame.Stage = STAGE_FINISHED
			//CLEANUP_DARTS_GAME(DartGame, DartsUI)
			
		ELIF (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT) OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT))
		AND DartGame.eThrowStyle = DARTS_STYLE_STICK
		AND bProcessMenuInput
			
			VECTOR vTemp
			DartGame.bAnotherCamActive = FALSE
		
			IF bEndGameDartZoomTrans
				vTemp = GET_ENTITY_COORDS(DartGame.Darts[iPlayr][iDartZoom].object)
				DARTS_ZOOM_DART_CAM_TRANS(vTemp, FALSE, bEndGameDartZoomTrans)
				
				bEndGameDartZoomTrans = FALSE
			
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				iDartZoom++				
				IF iDartZoom >= DartGame.iNumThrowsTurn
					iDartZoom = 0
				ENDIF
				
				vTemp = GET_ENTITY_COORDS(DartGame.Darts[iPlayr][iDartZoom].object)
				DARTS_ZOOM_DART_CAM(vTemp)				
				CDEBUG1LN(DEBUG_DARTS, "_____ Throw ", iDartZoom+1, ": ", DartGame.Darts[iPlayr][iDartZoom].iHitValue, " points _____")
				
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
				iDartZoom--
				IF iDartZoom < 0
					iDartZoom = DartGame.iNumThrowsTurn - 1
				ENDIF
				
				vTemp = GET_ENTITY_COORDS(DartGame.Darts[iPlayr][iDartZoom].object)
				DARTS_ZOOM_DART_CAM(vTemp)				
				CDEBUG1LN(DEBUG_DARTS, "_____ Throw ", iDartZoom+1, ": ", DartGame.Darts[iPlayr][iDartZoom].iHitValue, " points _____")
			ENDIF
			
		ELIF (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_LEADERBOARD)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_LEADERBOARD))
		AND bProcessMenuInput
		AND (bDartsLBPredictDone OR NOT IS_PLAYER_ONLINE())
			// Trying to go to the leaderboard.
			DartsUI.bDrawingLeaderboard = TRUE
			
//			STRING eOpenGamerCardButton = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
						
			// Setup new prompts.
			INIT_SIMPLE_USE_CONTEXT(DartsUI.inGameControlContext, FALSE, FALSE, TRUE, TRUE)
			ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "DARTS_CONT",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
			ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "DARTS_REMATCH",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
			ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "IB_BACK",		FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			IF bDartsProfilesReady
				ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "SCLB_PROFILE",	FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)//eOpenGamerCardButton
			ENDIF
			SET_SIMPLE_USE_CONTEXT_FULLSCREEN(DartsUI.inGameControlContext)	
		ENDIF
		
	ELSE
		// wait 'til we've finished rendering the player win/lost text
		IF NOT DartGame.bAnotherCamActive //IS_CAM_ACTIVE(camAnotherGame) 
			CLEAR_HELP()
			CLEAR_PRINTS()
			
			DARTS_SETUP_END_MENU(DartsEndScreen, DartGame.bDartWinner, DartGame.iThrowsTaken[iPlayr], DartGame.iBullsEyesHit[iPlayr], DartsStats[DARTS_SCLB_STAT_NUM_180S],
								DartGame.iDartSBGamesWon[0], DartGame.iDartSBGamesWon[1], 
								DartGame.iDartSBSetsWon[0], DartGame.iDartSBSetsWon[1], 
								DartGame.iDartSBFullGamesWon[0], DartGame.iDartSBFullGamesWon[1], 
								bUsedSetsLegs)
			
//			SET_SCALEFORM_BIG_MESSAGE(DartsUI.siBigMessage, PICK_STRING(DartGame.bDartWinner, "DARTS_WINNER", "DARTS_LOSER"), 
//									PICK_STRING(DartGame.bDartWinner, "DARTS_WON", "DARTS_LOSE"), 6000)
			
			FLOAT fResolution
			fResolution = GET_ASPECT_RATIO(TRUE)
			CDEBUG1LN(DEBUG_DARTS, " fResolution = ", fResolution)
			
			VECTOR scenePosition
			VECTOR sceneRotation
			
			IF fResolution > 2.0
				CDEBUG1LN(DEBUG_DARTS, " resolution is eyefinity")
				scenePosition = << 1992.29407, 3047.57690, 46.21517 >>
				sceneRotation = << 0.000, 0.000, 138.740 >>
			ELSE
				CDEBUG1LN(DEBUG_DARTS, "resolution is not eyefinity")
				scenePosition = << 1992.33569, 3047.92432, 46.21517 >>
				sceneRotation = << 0.000, 0.000, 136.740 >>
			ENDIF
			
			INT iWhichOutro
			
			iWhichOutro = GET_WHICH_DARTS_OUTRO() //GET_RANDOM_INT_IN_RANGE(0, iAnimLimit)
			iSceneID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			camDartsSync = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
			PLAY_SYNCHRONIZED_CAM_ANIM(camDartsSync, iSceneID, sOutroCam[iWhichOutro], "mini@dartsoutro")
			SET_CAM_ACTIVE(camDartsSync, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			IF NOT IS_PED_INJURED(piDartPlayers[0])
			AND NOT IS_PED_INJURED(piDartPlayers[1])
				IF DartGame.bDartWinner
					TASK_SYNCHRONIZED_SCENE (piDartPlayers[0], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (piDartPlayers[1], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
					TASK_PLAY_ANIM(PLAYER_PED_ID(), sFacialDictPlayer, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY)
				ELSE
					TASK_SYNCHRONIZED_SCENE (piDartPlayers[1], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (piDartPlayers[0], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
					TASK_PLAY_ANIM(piDartPlayers[1], sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY)
				ENDIF
			ENDIF
			
			bInSyncCam = FALSE
			
			//ANOTHER_GAME_CAM()
			DartGame.bAnotherCamActive = TRUE
			DartsUI.iEndDelay = GET_GAME_TIMER()
			
			INT idx, idy
			REPEAT NUM_DART_PLAYERS idx
				REPEAT  NUM_DART_TURNS idy
					RESET_DART(DartGame.Darts[idx][idy])
				ENDREPEAT
				
				DARTS_CLEAR_SCORES(DartsSB, idx)
			ENDREPEAT
			
		//ELIF NOT IS_CAM_INTERPOLATING(camAnotherGame)
		ELIF GET_GAME_TIMER() - DartsUI.iEndDelay > 400
		AND ENDSCREEN_PREPARE(DartsEndScreen, FALSE)
			
			IF NOT DartGame.bDartWinner
				PLAY_SOUND_FRONTEND(-1, "LOOSE_MATCH", "HUD_MINI_GAME_SOUNDSET")
			ENDIF
			
			bEndGameMessageDisplayed = TRUE
			
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SPECIFIC_DARTS_OPPONENT_NEARBY(PED_INDEX & piNearbyPeds[], PED_INDEX & piOpponent, MODEL_NAMES eSpecificModel)
	INT iPed
	
	REPEAT GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), piNearbyPeds) iPed
		IF NOT IS_PED_INJURED(piNearbyPeds[iPed])
			IF GET_ENTITY_MODEL(piNearbyPeds[iPed]) = eSpecificModel
				SET_ENTITY_AS_MISSION_ENTITY(piNearbyPeds[iPed], TRUE, TRUE)
				piOpponent = piNearbyPeds[iPed]
				CDEBUG1LN(DEBUG_DARTS, "Ped model ", iPed, " got what we need")
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_DARTS, "Ped model ", iPed, " is not the one we want")
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DARTS_OPPONENT_NEARBY(PED_INDEX & piNearbyPeds[])
	INT iPed
	
	REPEAT GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), piNearbyPeds) iPed
		IF NOT IS_PED_INJURED(piNearbyPeds[iPed])
			IF GET_ENTITY_MODEL(piNearbyPeds[iPed]) = A_F_M_SALTON_01
			OR GET_ENTITY_MODEL(piNearbyPeds[iPed]) = A_F_O_SALTON_01
			OR GET_ENTITY_MODEL(piNearbyPeds[iPed]) = A_M_Y_VINEWOOD_01
			OR GET_ENTITY_MODEL(piNearbyPeds[iPed]) = A_M_Y_STLAT_01
			OR GET_ENTITY_MODEL(piNearbyPeds[iPed]) = A_M_Y_VINEWOOD_04
			OR GET_ENTITY_MODEL(piNearbyPeds[iPed]) = A_M_Y_STWHI_02
			OR GET_ENTITY_MODEL(piNearbyPeds[iPed]) = A_M_Y_VINEWOOD_03
				SET_ENTITY_AS_MISSION_ENTITY(piNearbyPeds[iPed], TRUE, TRUE)
				piDartPlayers[1] = piNearbyPeds[iPed]
				CDEBUG1LN(DEBUG_DARTS, "Ped model ", iPed, " got what we need")
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_DARTS, "Ped model ", iPed, " is not the one we want")
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_NEXT_OPPONENT(MODEL_NAMES eDartOpponent, MODEL_NAMES & eNextOpponent)
	SWITCH eDartOpponent
		CASE A_F_M_SALTON_01 	eNextOpponent = A_F_O_SALTON_01		BREAK
		CASE A_F_O_SALTON_01	eNextOpponent = A_F_M_SALTON_01 	BREAK
		CASE A_M_Y_VINEWOOD_01	eNextOpponent = A_M_Y_STWHI_02		BREAK
		CASE A_M_Y_VINEWOOD_03	eNextOpponent = A_M_Y_VINEWOOD_01	BREAK
		CASE A_M_Y_VINEWOOD_04	eNextOpponent = A_M_Y_VINEWOOD_03	BREAK
		CASE A_M_Y_STLAT_01		eNextOpponent = A_M_Y_VINEWOOD_04	BREAK
		CASE A_M_Y_STWHI_02		eNextOpponent = A_M_Y_STLAT_01		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    used for russian name translation
/// PARAMS:
///    sName - original name
/// RETURNS:
///    russian translation
FUNC STRING GET_DART_LOCAL_PLAYER_NAME(STRING sName)
	
	IF GET_CURRENT_LANGUAGE() = LANGUAGE_RUSSIAN
		IF   ARE_STRINGS_EQUAL(sName, "MICHAEL") 
			RETURN "МАЙКЛ"
		ELIF ARE_STRINGS_EQUAL(sName, "FRANKLIN") 
			RETURN "ФРАНКЛИН"
		ELIF ARE_STRINGS_EQUAL(sName, "TREVOR") 
			RETURN "ТРЕВОР"
		ELIF ARE_STRINGS_EQUAL(sName, "LAMAR") 
			RETURN "ЛАМАР"
		ELIF ARE_STRINGS_EQUAL(sName, "JIMMY") 
			RETURN "ДЖИММИ"
		ELIF ARE_STRINGS_EQUAL(sName, "KRISTY") 
			RETURN "КРИСТИ"
		ELIF ARE_STRINGS_EQUAL(sName, "MARLENE") 
			RETURN "МАРЛЕН"
		ELIF ARE_STRINGS_EQUAL(sName, "LORIE") 
			RETURN "ЛОРИ"
		ELIF ARE_STRINGS_EQUAL(sName, "SHELLEY") 
			RETURN "ШЕЛЛИ"
		ELIF ARE_STRINGS_EQUAL(sName, "RAYMOND") 
			RETURN "РЕЙМОНД"
		ELIF ARE_STRINGS_EQUAL(sName, "JOHAN") 
			RETURN "ЙОХАН"
		ELIF ARE_STRINGS_EQUAL(sName, "STAN") 
			RETURN "СТЭН"
		ELIF ARE_STRINGS_EQUAL(sName, "VINCE") 
			RETURN "ВИНС"
		ENDIF
	ENDIF
	
	RETURN sName
ENDFUNC

/// PURPOSE:
///    Creates opponent ped, sets player and opponent at the proper position
PROC INIT_PEDS(FLOAT fBoardHeading, DARTS_PLAYER_POS PlayerPositions, PED_INDEX & piNearbyPeds[], DARTS_ARGS & DartsArgs)
	CDEBUG1LN(DEBUG_DARTS, "PlayerPositions.vDartPlayrPos = ", PlayerPositions.vDartPlayrPos, ".  PlayerPositions.vOpponentPos = ", PlayerPositions.vOpponentPos)
	
	//SET_ENTITY_COORDS(piDartPlayers[0], PlayerPositions.vDartPlayrPos)
	//SET_ENTITY_HEADING(piDartPlayers[0], fBoardHeading)
	
	
	IF DOES_ENTITY_EXIST(FRIEND_A_PED_ID())// if this is a friend activity, this should already be set
	AND NOT IS_PED_INJURED(FRIEND_A_PED_ID())
		REMOVE_PED_FROM_GROUP(FRIEND_A_PED_ID())
		//SET_ENTITY_COORDS(FRIEND_A_PED_ID(), PlayerPositions.vOpponentPos)
		//SET_ENTITY_HEADING(FRIEND_A_PED_ID(), fBoardHeading)
		CDEBUG1LN(DEBUG_DARTS, "Friend activity, familiar ped is set")
		
	ELSE
		IF DOES_ENTITY_EXIST(DartsArgs.piDartOpponent)
		AND NOT IS_PED_INJURED(DartsArgs.piDartOpponent)
			CDEBUG1LN(DEBUG_DARTS, "Ped found from DartsArgs")
			SET_ENTITY_AS_MISSION_ENTITY(DartsArgs.piDartOpponent, TRUE, TRUE)
			piDartPlayers[1] = DartsArgs.piDartOpponent
			CLEAR_PED_TASKS(piDartPlayers[1])
		ELSE
			IF IS_DARTS_OPPONENT_NEARBY(piNearbyPeds)
				CDEBUG1LN(DEBUG_DARTS, "Non friend activity, ped grabbed from setting")
				CLEAR_PED_TASKS(piDartPlayers[1])
			ELSE
				piDartPlayers[1] = CREATE_PED(PEDTYPE_MISSION, modelOpponent, PlayerPositions.vOpponentPos, fBoardHeading)
				CDEBUG1LN(DEBUG_DARTS, "Non friend activity, random ped generated")
			ENDIF
		ENDIF
		
		IF DECOR_EXIST_ON(piDartPlayers[1], "Darts_name")
			// Awesome, get that name.
			INT iHashName = DECOR_GET_INT(piDartPlayers[1], "Darts_name")
			IF (HASH("RAYMOND") = iHashName)			
				sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("RAYMOND")
				iP2NameHash = HASH("RAYMOND")
			ELIF (HASH("JOHAN") = iHashName)			
				sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("JOHAN")
				iP2NameHash = HASH("JOHAN")
			ELIF (HASH("STAN") = iHashName)				
				sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("STAN")
				iP2NameHash = HASH("STAN")
			ELIF (HASH("VINCE") = iHashName)			
				sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("VINCE")
				iP2NameHash = HASH("VINCE")
			ELIF (HASH("KRISTY") = iHashName)			
				sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("KRISTY")
				iP2NameHash = HASH("KRISTY")
			ELIF (HASH("MARLENE") = iHashName)			
				sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("MARLENE")
				iP2NameHash = HASH("MARLENE")
			ELIF (HASH("LORIE") = iHashName)			
				sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("LORIE")
				iP2NameHash = HASH("LORIE")
			ELIF (HASH("SHELLEY") = iHashName)			
				sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("SHELLEY")
				iP2NameHash = HASH("SHELLEY")
			ENDIF
		ENDIF
		DECOR_SET_INT(piDartPlayers[1], "Darts_name", iP2NameHash)
	ENDIF
	
	MODEL_NAMES mDartOpponentModel
	mDartOpponentModel = GET_ENTITY_MODEL(piDartPlayers[1])
	
	SET_DART_OPPONENT_VOICE(mDartOpponentModel)
	
	TASK_TURN_PED_TO_FACE_ENTITY(piDartPlayers[1], piDartPlayers[0])
	TASK_TURN_PED_TO_FACE_ENTITY(piDartPlayers[0], piDartPlayers[1])
	
#IF IS_DEBUG_BUILD
	VECTOR vTemp1, vTemp2
	vTemp1 = GET_ENTITY_COORDS(piDartPlayers[0])
	vTemp2 = GET_ENTITY_COORDS(piDartPlayers[1])
	
	IF NOT IS_PED_INJURED(piDartPlayers[0])
	AND NOT IS_PED_INJURED(piDartPlayers[1])
		CDEBUG1LN(DEBUG_DARTS, "Player is at ", vTemp1, ". Opponent is at ", vTemp2)
		CDEBUG1LN(DEBUG_DARTS, "Player heading is ", GET_ENTITY_HEADING(piDartPlayers[0]), ". Opponent heading is ", GET_ENTITY_HEADING(piDartPlayers[1]))
	ENDIF
#ENDIF	//	IS_DEBUG_BUILD
	
ENDPROC

/// PURPOSE:
///    An end of turn transtion showing the players score and a closeup of the Darts Board
/// PARAMS:
///    DartGame - 
PROC DO_TURN_CHANGE (DART_GAME & DartGame, DARTS_SCOREBOARD & DartsSB, DARTS_UI & DartsUI)//, DARTS_LOCATION DartsLocation = DARTSLOCATION_ROCK_CLUB)
	VECTOR vTemp

	SWITCH DartGame.TurnChange
		CASE DTC_BOARD_ZOOM			
			IF DartGame.eThrowStyle = DARTS_STYLE_STICK
				BOARD_ZOOM_CAM()
			ENDIF
			
			IF iPlayr = 0
				IF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE)
					PRINT_WITH_NUMBER_NOW("DARTS_PLR_DUB", DartGame.Darts[iPlayr][iTurn].iHitValue, 5000, 0)
					
				ELIF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT)
					PRINT_NOW("DARTS_ONE_PT", 5000, 0)
					
				ELIF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO)
					PRINT_WITH_NUMBER_NOW("DARTS_PLR_BUST", DartGame.Darts[iPlayr][iTurn].iHitValue, 5000, 0)
					
				ELSE
					PRINT_WITH_NUMBER_NOW("DARTS_SWITCH_C", DartGame.Scores[iPlayr], 5000, 0)
					
				ENDIF
			ELSE
				IF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE)
					PRINT_WITH_NUMBER_NOW("DARTS_OPP_DUB", DartGame.Darts[iPlayr][iTurn].iHitValue, 5000, 0)
					
				ELIF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT)
					PRINT_NOW("DARTS_ONE_PT", 5000, 0)
					
				ELIF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO)
					PRINT_WITH_NUMBER_NOW("DARTS_OPP_BUST", DartGame.Darts[iPlayr][iTurn].iHitValue, 5000, 0)
					
				ELSE
					PRINT_WITH_NUMBER_NOW("DARTS_SWITCH_B", DartGame.Scores[iPlayr], 5000, 0)
					
				ENDIF
			ENDIF				
					
			SETTIMERB(0)			
						
			DartGame.TurnChange = DTC_NEXT_TURN
		BREAK
			
		CASE DTC_DART_ZOOM			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				iDartZoom++
				IF iDartZoom >= DartGame.iNumThrowsTurn
					iDartZoom = 0
				ENDIF
				vTemp = GET_ENTITY_COORDS(DartGame.Darts[iPlayr][iDartZoom].object)
				DARTS_ZOOM_DART_CAM(vTemp)
				CDEBUG1LN(DEBUG_DARTS, "********** iDartZoom is ", iDartZoom)
				CDEBUG1LN(DEBUG_DARTS, "********** vPosition is: ", DartGame.Darts[iPlayr][iDartZoom].vPosition)
				CDEBUG1LN(DEBUG_DARTS, "********** Entity coords are: ",vTemp)
				CDEBUG1LN(DEBUG_DARTS, "********************")
				CDEBUG1LN(DEBUG_DARTS, "_____ Throw ", iDartZoom+1, ": ", DartGame.Darts[iPlayr][iDartZoom].iHitValue, " points _____")
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
				iDartZoom--
				IF iDartZoom < 0
					iDartZoom = DartGame.iNumThrowsTurn - 1
				ENDIF
				vTemp = GET_ENTITY_COORDS(DartGame.Darts[iPlayr][iDartZoom].object)
				DARTS_ZOOM_DART_CAM(vTemp)
				CDEBUG1LN(DEBUG_DARTS, "********** iDartZoom is ", iDartZoom)
				CDEBUG1LN(DEBUG_DARTS, "********** vPosition is: ", DartGame.Darts[iPlayr][iDartZoom].vPosition)
				CDEBUG1LN(DEBUG_DARTS, "********** Entity coords are: ", vTemp)
				CDEBUG1LN(DEBUG_DARTS, "********************")
				CDEBUG1LN(DEBUG_DARTS, "_____ Throw ", iDartZoom+1, ": ", DartGame.Darts[iPlayr][iDartZoom].iHitValue, " points _____")
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				vTemp = GET_ENTITY_COORDS(DartGame.Darts[iPlayr][iDartZoom].object)
				DARTS_ZOOM_DART_CAM_TRANS(vTemp, FALSE)
				DartGame.TurnChange = DTC_NEXT_TURN
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				bZoomCamSkip = TRUE
				DartGame.TurnChange = DTC_NEXT_TURN
			ENDIF
		BREAK
			
		CASE DTC_NEXT_TURN
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			OR (bAISkipped AND bInstantSkip)
				bZoomCamSkip = TRUE
			ENDIF
			
			IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT))
			AND DartGame.eThrowStyle = DARTS_STYLE_STICK
				vTemp = GET_ENTITY_COORDS(DartGame.Darts[iPlayr][iDartZoom].object)
				DARTS_ZOOM_DART_CAM_TRANS(vTemp)
				//iDartZoom = iTurn
				DartGame.TurnChange = DTC_DART_ZOOM
				
				CDEBUG1LN(DEBUG_DARTS, "_____ Throw ", iDartZoom+1, ": ", DartGame.Darts[iPlayr][iDartZoom].iHitValue, " points _____")
			ENDIF
			
			IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_IN_WIN_RANGE)
				IF NOT bAnyAudioPlayedThisTurn
					IF PLAY_DARTS_TRASH_TALK((iPlayr = 0), piDartPlayers[1], (DartGame.LastScore[iPlayr] - DartGame.Scores[iPlayr]), DartGame.Scores[0], DartGame.Scores[1])
						CDEBUG1LN(DEBUG_DARTS, "TRASH TALK PLAYED")
						bAnyAudioPlayedThisTurn = TRUE
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "TRASH TALK DID NOT PLAY")
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() % 500) < 50
						CDEBUG1LN(DEBUG_DARTS, "Some audio already played this turn")
					ENDIF
				ENDIF
			ELSE
				IF NOT bAnyAudioPlayedThisTurn
					IF PLAY_DARTS_TRASH_TALK_CLUTCH((iPlayr = 0), piDartPlayers[1], (DartGame.LastScore[iPlayr] - DartGame.Scores[iPlayr]), DartGame.Scores[0], DartGame.Scores[1])
						CDEBUG1LN(DEBUG_DARTS, "TRASH TALK CLUTCH PLAYED")
						bAnyAudioPlayedThisTurn = TRUE
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "TRASH TALK CLUTCH DID NOT PLAY")
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() % 500) < 50
						CDEBUG1LN(DEBUG_DARTS, "Some audio already played this turn")
					ENDIF
				ENDIF
			ENDIF
			
			IF bZoomCamSkip OR TIMERB() > 2000				
				bAISkipped = FALSE
				
				INT i
				REPEAT NUM_DART_TURNS i
					RESET_DART(DartGame.Darts[iPlayr][i])
				ENDREPEAT
				
				IF DartGame.LastScore[iPlayr] <> DartGame.Scores[iPlayr]
					DARTS_ADD_SCORE(DartsSB, iPlayr, DartGame.Scores[iPlayr])
					PLAY_SOUND_FROM_COORD(-1, "DARTS_SCOREBOARD_MASTER", Dartgame.DartsBoard.vDartBoard)
				ENDIF
				
				// store the previous score in case we need to roll back to it for bustouts
				DartGame.LastScore[iPlayr] = DartGame.Scores[iPlayr]
				
				// reset the turn
				iTurn = 0
				DartGame.iTurnScore = 0
				
				// switch the player
				IF iPlayr = 0
					iPlayr = 1
				ELSE
					iPlayr = 0
				ENDIF
				
				UPDATE_HIGHLIGHT(DartsSB, iPlayr)
				
				bNoCrossConflict = FALSE
				CDEBUG1LN(DEBUG_DARTS, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%     SWITCHING PLAYER     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
				
				DARTS_GAME_CAM()
				IF NOT bInstantSkip
					CLEAR_SMALL_PRINTS()
				ENDIF
				
//				BOOL bIsPlayer
//				
//				IF iPlayr = 0
//					bIsPlayer = TRUE
//				ELSE
//					bIsPlayer = FALSE
//				ENDIF
				
				iSteadyShotsUsed = 0
				
				DartsUI.iDartsLeft = 3
				
				bOneDartHelpShown = FALSE
				bBoredAudioPlayedThisTurn = FALSE
				bAnyAudioPlayedThisTurn = FALSE
				bZoomCamSkip = FALSE
				iDartZoom = 0
				DartGame.iNumThrowsTurn = 0
				DartGame.TurnStage = DPS_AIM
				DartGame.TurnChange = DTC_BOARD_ZOOM
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

FUNC BOOL DID_DARTS_CLASH(DART_GAME & DartGame)
	INT i
	
	REPEAT iTurn i		
		IF 	DartGame.Darts[iPlayr][iTurn].vTarget.x > DartGame.Darts[iPlayr][i].vTarget.x - 0.008
		AND DartGame.Darts[iPlayr][iTurn].vTarget.x < DartGame.Darts[iPlayr][i].vTarget.x + 0.008
		AND DartGame.Darts[iPlayr][iTurn].vTarget.z > DartGame.Darts[iPlayr][i].vTarget.z - 0.008
		AND DartGame.Darts[iPlayr][iTurn].vTarget.z < DartGame.Darts[iPlayr][i].vTarget.z + 0.008		
			RETURN TRUE
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

FUNC BOOL DO_TURN_ZOOM(DART_ZOOM_TURN & DartZoomStage)
	SWITCH DartZoomStage
		CASE DZT_ZOOM_IN
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT)
				DARTS_ZOOM_LAST_TURN()
				DartZoomStage = DZT_ZOOM_INTERP
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE DZT_ZOOM_INTERP
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT)
				IF NOT IS_CAM_INTERPOLATING(camZoomBoard)
					SETUP_DARTS_PC_ZOOM_CONTROLS()
					INIT_FIRST_PERSON_CAMERA(fpDartsCam, vFPDartsPos, vFPDartsRot, 30, 10, 10, 3, 15, FALSE, 0, -1, TRUE)
					DartZoomStage = DZT_ZOOM_FP
				ENDIF
			ELSE
				DartZoomStage = DZT_ZOOM_OUT
			ENDIF
		BREAK
		
		CASE DZT_ZOOM_FP
			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT)
				DartZoomStage = DZT_ZOOM_OUT
			ELSE
				UPDATE_FIRST_PERSON_CAMERA(fpDartsCam)
			ENDIF
		BREAK
		
		CASE DZT_ZOOM_OUT
			
			//CLEAR_FIRST_PERSON_CAMERA(fpDartsCam)
			DARTS_ZOOM_LAST_TURN(FALSE)
			
			SETUP_DARTS_PC_CONTROLS()
			
			IF DOES_CAM_EXIST(fpDartsCam.theCam)
				IF IS_CAM_ACTIVE(fpDartsCam.theCam)
					SET_CAM_ACTIVE(fpDartsCam.theCam,FALSE)
				ENDIF
				DESTROY_CAM(fpDartsCam.theCam)
			ENDIF
			
			DartZoomStage = DZT_ZOOM_IN
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DO_ONE_DART_HELPER (INT iTotalScore, DARTS_BOARD & DartsBoard, ONE_DART_STAGE & OneDartStage, VECTOR & vOneDartZoomCoord)
	SWITCH OneDartStage
		CASE ODS_ZOOM_IN
			VECTOR vLocalDoubleCoord
			
			vLocalDoubleCoord = GET_DOUBLE_VECTOR(iTotalScore)			
			vOneDartZoomCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(DartsBoard.oiDartBoard, vLocalDoubleCoord)			
			CDEBUG1LN(DEBUG_DARTS, "--------------- vLocalDoubleCoord = ", vLocalDoubleCoord, ", vOneDartZoomCoord = ", vOneDartZoomCoord)
			
			DARTS_DOUBLE_CAM_START(vOneDartZoomCoord)			
			SETTIMERB(0)			
			OneDartStage = ODS_ZOOM_OUT
		BREAK
		
		CASE ODS_ZOOM_OUT
			IF TIMERB() > 3000 OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				DARTS_DOUBLE_CAM_END()
				OneDartStage = ODS_ZOOM_IN
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    The main in game progress loop.
/// PARAMS:
///    DartGame - 
PROC DO_IN_PROGRESS (DART_GAME & DartGame, DARTS_STARTING_RETICLE_POS & InitRetPos, DARTS_SCOREBOARD & DartsSB, DARTS_UI & DartsUI, structTimer & ThrowTimer)//, DARTS_LOCATION DartsLocation)
	
	#IF IS_DEBUG_BUILD
	IF iPlayr < 2 AND iTurn < 3
		IF DartGame.Darts[iPlayr][iTurn].bTravelling
			DRAW_DEBUG_TEXT_2D("bTravelling is TRUE", << 0.7, 0.3, 0.0 >>)
		ENDIF
	ENDIF
	HANDLE_SWITCH_CAM(DebugBoard.bSwitchCam)
	#ENDIF
	
	BOOL bGameCamInterpolating
	
	//If cam is moving, don't show HUD
	IF NOT IS_CAM_INTERPOLATING(camDartGame)
	
		bGameCamInterpolating = FALSE
		PLAYER_QUIT_CONTROL(DartGame)
		
		IF NOT bHasQuitTextPrinted 
			// Instructions during end of turn board and dart zooms
			IF DartGame.TurnStage = DPS_TURN_CHANGE				
				IF DartsUI.bInstructTextTransIn
					CLEAR_HELP()
					DartsUI.bInstructTextTransIn = FALSE
				ENDIF
			// Instructions during regular gameplay
			ELIF NOT bUITextDisplayed				
				
				
				IF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_END_HELP_READY)
					IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_WIN_HELP)
						IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gWIN_HELP_SHOWN)
							PRINT_HELP("DARTS_INSTR_W")
							SET_BIT(g_iDartsTutorialFlags, gWIN_HELP_SHOWN)
						ENDIF
						DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_WIN_HELP, TRUE)
					ELIF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP)
					AND DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP_READY)
						IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gBUST_HELP_SHOWN)
							PRINT_HELP("DARTS_INSTR_B")
							SET_BIT(g_iDartsTutorialFlags, gBUST_HELP_SHOWN)
						ENDIF
						DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP, TRUE)
					ENDIF
				ENDIF
				
//				IF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_IN_WIN_RANGE)
//				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_SHT_USE")
//				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_FST_HLP")
//				AND iPlayr = 0
//					IF (DartGame.Scores[iPlayr] % 2) = 0
//						IF DartGame.Scores[iPlayr] = 50
//						ELSE
//							CLEAR_HELP()
//						ENDIF
//					
//					ELSE
//						//PRINT_HELP_FOREVER_WITH_NUMBER("DARTS_CURRENT", DartGame.Scores[iPlayr])
//					ENDIF
//				ELSE
//					//DARTS_HELP(iPlayr, DartsUI)
//					
//				ENDIF
				DartsUI.bInstructTextTransIn = TRUE
			ELSE
				IF DartsUI.bInstructTextTransIn
					CLEAR_HELP()
					DartsUI.bInstructTextTransIn = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF DartGame.TurnStage = DPS_ZOOM
			IF NOT IS_CAM_INTERPOLATING(camZoomBoard)
			AND DartGame.ZoomTurn = DZT_ZOOM_FP
				DARTS_ZOOM_CONTROLS_UI(DartsUI)
			ENDIF
		ELSE
			DARTS_CONTROLS_UI(DartsUI, iPlayr)
		ENDIF
		
//		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_BOTTOM)
//		SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
//		
//		IF NOT bSubtitlesMoved
//			FLOAT fnewx, fnewy
//			GET_SCRIPT_GFX_ALIGN_POSITION(0.0, 0.0, fnewx, fnewy)
//			SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, fnewx, fnewy-0.21)
//			bSubtitlesMoved = TRUE
//		ENDIF
//		
//		RESET_SCRIPT_GFX_ALIGN()
		
//		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
//		SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
//		
//		RESET_SCRIPT_GFX_ALIGN()
		
		//DRAW_DARTS_HUD(DartsUI)
		
		SKIP_AI_TURN_CHECK(DartGame.TurnStage)
		
	ELSE
		bGameCamInterpolating = TRUE
	ENDIF
	
	DARTS_DISPLAY_SPECIAL_SPLASH(DartsUI)
	
	SWITCH DartGame.TurnStage
		CASE DPS_ZOOM
			IF NOT DO_TURN_ZOOM(DartGame.ZoomTurn)			
				DartGame.TurnStage = DPS_AIM
			ENDIF
		BREAK
		
		CASE DPS_ONE_DART_HELPER
			IF DO_ONE_DART_HELPER(DartGame.Scores[iPlayr], DartGame.DartsBoard, DartGame.OneDartStage, DartGame.vOneDartZoomCoord)
				DartGame.TurnStage = DPS_AIM
			ENDIF
		BREAK
		
		CASE DPS_AIM
			//prepping reticle and reticle shake
			//doing reticle move speed			
			
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT) AND iPlayr = 0 //AND DartGame.eThrowStyle = DARTS_STYLE_STICK
				DartGame.TurnStage = DPS_ZOOM	
			ENDIF
			
			IF iPlayr = 0
			//	IF DartGame.iBustStreak > 3 AND  (DartGame.Scores[iPlayr] % 2 = 0) AND DartGame.eThrowStyle = DARTS_STYLE_STICK
			//		DartGame.TurnStage = DPS_ONE_DART_HELPER
			//		DartGame.iBustStreak = 0
				
				DARTS_GAME_HELP(iTurn)
				
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_SHT_USE")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_W")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_B")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_AIM_HLP")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_STD_HLP")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_FST_HLP")
					IF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOW_LEVEL_UP)
						// set the global bit here
						PRINT_HELP("DARTS_LEVEL")
						SET_BIT(g_savedGlobals.sDartsData.iDartTimePlayed, iLevelBit)
						DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOW_LEVEL_UP, FALSE)
					ENDIF
				ENDIF
				
				IF (DartGame.Scores[iPlayr] % 2 = 0) AND DartGame.Scores[iPlayr] < 41 AND iTurn = 0 AND NOT bOneDartHelpShown
					IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_IN_WIN_RANGE)
						DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_IN_WIN_RANGE, TRUE)
					ENDIF
					bOneDartHelpShown = TRUE
					
				ELIF DartGame.Scores[iPlayr] = 50 AND NOT bOneDartHelpShown
					IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_IN_WIN_RANGE)
						DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_IN_WIN_RANGE, TRUE)
					ENDIF
					bOneDartHelpShown = TRUE
					
				ELSE
					IF DO_AIMING(DartGame.Darts[iPlayr][iTurn], DartGame.DartsBoard, InitRetPos, 
								bGameCamInterpolating, FALSE, FALSE, DartGame.eThrowStyle)
						
						CLEAR_PRINTS()
						DartsUI.iDartsLeft--
						CDEBUG1LN(DEBUG_DARTS, "[DO_IN_PROGRESS->DPS_AIM] Aiming finished")
						DartGame.TurnStage = DPS_THROW
					ELSE
						IF NOT bBoredAudioPlayedThisTurn
							IF NOT IS_TIMER_STARTED(ThrowTimer)
								RESTART_COUNTDOWNTIMER_NOW(ThrowTimer)
							
							ELIF GET_TIMER_IN_SECONDS(ThrowTimer) > 20.0
								PLAY_HURRY_AUDIO(piDartPlayers[1-iPlayr])
								CANCEL_TIMER(ThrowTimer)
								bBoredAudioPlayedThisTurn = TRUE
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			
			ELSE
				IF iTurn = 0
					iSkipAILoopCatch = 0 // resetting for loop catch, take out when skipAIloop bug is resolved
					GET_NEXT_AI_THROW(DartGame.Scores, DartGame.AIBrain)
					GET_AI_THROW_VECTOR(DartGame.Darts[iPlayr][iTurn], DartGame.DartsBoard, DartGame.TurnStage, DartGame.AIBrain)
				ELIF TIMERA() > GET_RANDOM_INT_IN_RANGE(1250, 2500)  //750
					GET_NEXT_AI_THROW(DartGame.Scores, DartGame.AIBrain)
					GET_AI_THROW_VECTOR(DartGame.Darts[iPlayr][iTurn], DartGame.DartsBoard, DartGame.TurnStage, DartGame.AIBrain)
				ENDIF
				
				// AUDIO PROC FLAG
				//bDartMissAudioPlayed = FALSE
			ENDIF			
		BREAK
		
		CASE DPS_THROW
			//displaying the dart flying through the air and hitting the board
			//playing the correct sound effects
			IF THROW_DART(DartGame.Darts[iPlayr][iTurn], DartGame.DartsBoard, DartGame.eThrowStyle)
				DartGame.iNumThrowsTurn++
				DartGame.bDartsClash = DID_DARTS_CLASH(DartGame)
				DartGame.TurnStage = DPS_SCORING
				CDEBUG1LN(DEBUG_DARTS, "[DO_IN_PROGRESS->DPS_THROW] Throw finished")
			ENDIF
		BREAK
	
		CASE DPS_SCORING
			//calculating the score
			//keeping track of bust and win possibilities
			//keeping track of turn cycles
			//displaying to the UI
			IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_GAME_WON)
				CDEBUG1LN(DEBUG_DARTS, "[DO_IN_PROGRESS->DPS_SCORING] Scoring Dart Turn ", iTurn, ", Player ", iPlayr)
				SCORE_DART(DartGame.Darts[iPlayr][iTurn], DartGame.DartsBoard, DartGame.bDartsClash)
			ENDIF
			
			TURN_INCREMENT(DartGame, DartsUI)
			
		BREAK
	
		CASE DPS_TURN_CHANGE
			DO_TURN_CHANGE(DartGame, DartsSB, DartsUI)//, DartsLocation)
			IF IS_TIMER_STARTED(ThrowTimer)
				CANCEL_TIMER(ThrowTimer)
			ENDIF
		BREAK
	
		CASE DPS_SKIP_AI		
			//simulate AI player until a skip is called
			DO_SKIP_AI_TURN(DartGame, DartsUI)
		BREAK	
	ENDSWITCH
ENDPROC

FUNC BOOL DARTS_TUTORIAL(DARTS_TUTORIAL_STAGE & DartsTutorialStage, DARTS_BOARD & DartsBoard, DARTS_AI_BRAIN & AIBrain, 
						DARTS_PLAYER_STATE & TurnStage, DART & TutorialDarts[], structTimer & TutorialTimer)
	
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
	AND GET_GAME_TIMER() >= iAllowSkipCutsceneTime + 1000
	AND DartsTutorialStage != TS_SKIP
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
		ENDIF
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
	AND DartsTutorialStage != TS_SKIP
		//HANG_UP_AND_PUT_AWAY_PHONE()
		//KILL_ANY_CONVERSATION()
		DartsTutorialStage = TS_SKIP
	ENDIF
	
	SWITCH DartsTutorialStage
		CASE TS_DOUBLE
			SWITCH iTurn
				CASE 0
					IF TurnStage = DPS_AIM 
						iAllowSkipCutsceneTime = GET_GAME_TIMER()
						DARTS_TUTORIAL_CAM(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
											GET_TUTORIAL_CAM_COORD_OFFSET(TS_DOUBLE), GET_TUTORIAL_CAM_ROT_OFFSET(), 0, TRUE)
						
						AIBrain.iThrowValue = 20
						AIBrain.iThrowMultiplier = 2
						GET_AI_THROW_VECTOR(TutorialDarts[0], DartsBoard, TurnStage, AIBrain, TRUE)
					ENDIF
					
					IF TurnStage = DPS_THROW
						IF THROW_DART(TutorialDarts[0], DartsBoard)
							PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER", DartsBoard.vDartBoard)
							iTurn++
							RESTART_TIMER_NOW(TutorialTimer)
							TurnStage = DPS_AIM
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 1.0
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_DOUBLE_T")
						PRINT_HELP("DARTS_DOUBLE_T")	
					ENDIF
					
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 5.0
						RESTART_TIMER_NOW(TutorialTimer)
						CLEAR_HELP()
						DARTS_TUTORIAL_CAM_INTERP(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
													GET_TUTORIAL_CAM_COORD_OFFSET(TS_TRIPLE), GET_TUTORIAL_CAM_ROT_OFFSET(), 1000)
						iTurn=0
						DartsTutorialStage = TS_TRIPLE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE TS_TRIPLE
			SWITCH iTurn
				CASE 0
					IF NOT IS_CAM_INTERPOLATING(camTutInterp)
						IF TurnStage = DPS_AIM 
							
							AIBrain.iThrowValue = 20
							AIBrain.iThrowMultiplier = 3
							GET_AI_THROW_VECTOR(TutorialDarts[1], DartsBoard, TurnStage, AIBrain, TRUE)
						ENDIF
						
						IF TurnStage = DPS_THROW
							IF THROW_DART(TutorialDarts[1], DartsBoard)
								PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER", DartsBoard.vDartBoard)
								iTurn++
								RESTART_TIMER_NOW(TutorialTimer)
								TurnStage = DPS_AIM
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 1.0
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_TRIPLE_T")
						PRINT_HELP("DARTS_TRIPLE_T")	
					ENDIF
					
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 5.0
						RESTART_TIMER_NOW(TutorialTimer)
						CLEAR_HELP()
						DARTS_TUTORIAL_CAM(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
													GET_TUTORIAL_CAM_COORD_OFFSET(TS_BULLSEYE), GET_TUTORIAL_CAM_ROT_OFFSET(), 1000)
						iTurn=0
						DartsTutorialStage = TS_BULLSEYE
					ENDIF
				BREAK
			ENDSWITCH
			
			
			
//			IF NOT IS_CAM_INTERPOLATING(camTutInterp)
//			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_DOUBLE_T")
//				PRINT_HELP("DARTS_DOUBLE_T")
//				
//			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_DOUBLE_T")
//			//	HELP_AT_ENTITY_OFFSET("DARTS_DOUBLE_T", DartGame.TutorialDarts[0].object, << 0, 0, -0.85 >>)
//			ENDIF
//			
//			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 6.0
//				RESTART_TIMER_NOW(TutorialTimer)
//				DARTS_TUTORIAL_CAM(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
//									GET_TUTORIAL_CAM_COORD_OFFSET(TS_TRIPLE), GET_TUTORIAL_CAM_ROT_OFFSET(), 1000)
//				//HELP_AT_ENTITY("DARTS_TRIPLE_T", DartGame.TutorialDarts[0].object)
//				//CLEAR_THIS_FLOATING_HELP("DARTS_DOUBLE_T")
//				DartsTutorialStage = TS_BULLSEYE
//			ENDIF
		BREAK
		
		CASE TS_BULLSEYE
			
			SWITCH iTurn
				CASE 0
					IF NOT IS_CAM_INTERPOLATING(camTutorial)
						IF TurnStage = DPS_AIM 
							
							AIBrain.iThrowValue = 50
							AIBrain.iThrowMultiplier = 1
							GET_AI_THROW_VECTOR(TutorialDarts[2], DartsBoard, TurnStage, AIBrain, TRUE)
						ENDIF
						
						IF TurnStage = DPS_THROW
							IF THROW_DART(TutorialDarts[2], DartsBoard)
								PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER", DartsBoard.vDartBoard)
								iTurn++
								RESTART_TIMER_NOW(TutorialTimer)
								TurnStage = DPS_AIM
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 1.0
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_BULL_T")
						PRINT_HELP("DARTS_BULL_T")	
					ENDIF
					
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 5.0
						RESTART_TIMER_NOW(TutorialTimer)
						CLEAR_HELP()
						
						PLAY_SOUND_FRONTEND(-1, "CAM_PAN_DARTS", "HUD_MINI_GAME_SOUNDSET")
						CPRINTLN(DEBUG_DARTS, "Playing first pan whoosh sound")
						DARTS_TUTORIAL_CAM_INTERP(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
													GET_TUTORIAL_CAM_COORD_OFFSET(TS_DBL_TO_WIN), GET_TUTORIAL_CAM_ROT_OFFSET(TRUE), 1500)
						iTurn=0
						DartsTutorialStage = TS_DBL_TO_WIN
					ENDIF
				BREAK
			ENDSWITCH
			
//			IF NOT IS_CAM_INTERPOLATING(camTutorial)
//			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_TRIPLE_T")
//				PRINT_HELP("DARTS_TRIPLE_T")
//				
//			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_TRIPLE_T")
//			//	HELP_AT_ENTITY("DARTS_TRIPLE_T", DartGame.TutorialDarts[0].object)
//			ENDIF
//			
//			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 6.0
//				RESTART_TIMER_NOW(TutorialTimer)
//				DARTS_TUTORIAL_CAM_INTERP(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
//											GET_TUTORIAL_CAM_COORD_OFFSET(TS_BULLSEYE), GET_TUTORIAL_CAM_ROT_OFFSET(), 1000)
//				//HELP_AT_ENTITY("DARTS_BULL_T", DartGame.TutorialDarts[1].object)
//				//CLEAR_THIS_FLOATING_HELP("DARTS_TRIPLE_T")
//				DartsTutorialStage = TS_DBL_TO_WIN
//			ENDIF
		BREAK
		
		CASE TS_DBL_TO_WIN
			IF NOT IS_CAM_INTERPOLATING(camTutInterp)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_DBL_WIN")
				PRINT_HELP("DARTS_DBL_WIN")
			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_BULL_T")
			//	HELP_AT_ENTITY("DARTS_BULL_T", DartGame.TutorialDarts[1].object)
			ENDIF
			
			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 6.0
				RESTART_TIMER_NOW(TutorialTimer)
//				DARTS_TUTORIAL_CAM(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
//									GET_TUTORIAL_CAM_COORD_OFFSET(TS_DBL_TO_WIN), GET_TUTORIAL_CAM_ROT_OFFSET(), 1500)
				//HELP_AT_ENTITY("DARTS_DBL_WIN", DartGame.TutorialDarts[2].object)
				//CLEAR_THIS_FLOATING_HELP("DARTS_BULL_T")
				DartsTutorialStage = TS_ENDING				
			ENDIF
		BREAK
		
		CASE TS_ENDING
//			IF NOT IS_CAM_INTERPOLATING(camTutorial)
//			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_DBL_WIN")
//				PRINT_HELP("DARTS_DBL_WIN")
//				
//			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_DBL_WIN")
//			//	HELP_AT_ENTITY("DARTS_DBL_WIN", DartGame.TutorialDarts[2].object)
//			ENDIF
			
			//IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 5.0 //TIMERA() > 6000
				PLAY_SOUND_FRONTEND(-1, "CAM_PAN_DARTS", "HUD_MINI_GAME_SOUNDSET")
				CPRINTLN(DEBUG_DARTS, "Playing second whoosh sound")
				SET_CAM_ACTIVE_WITH_INTERP(camDartGame, camTutInterp, 1500)
				//CLEAR_THIS_FLOATING_HELP("DARTS_DBL_WIN")
				DartsTutorialStage = TS_ENDING2
			//ENDIF
		BREAK
		
		CASE TS_ENDING2
			IF NOT IS_CAM_INTERPOLATING(camDartGame)
				iTurn = 0
				CLEAR_HELP()
				CLEAR_PRINTS()
				//CLEAR_ALL_FLOATING_HELP()
				INT i
				REPEAT NUM_DART_TURNS i
					CLEANUP_DART(TutorialDarts[i])
				ENDREPEAT
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TS_SKIP
			IF IS_SCREEN_FADED_OUT()
				iTurn = 0
				CLEAR_HELP()
				CLEAR_PRINTS()
				//CLEAR_ALL_FLOATING_HELP()
				INT i
				REPEAT NUM_DART_TURNS i
					CLEANUP_DART(TutorialDarts[i])
				ENDREPEAT
				
				SET_CAM_ACTIVE(camDartGame, TRUE)
				
				DO_SCREEN_FADE_IN(500)
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DARTS_LEVEL_HELP_SHOW(INT iNumGames, INT & iBitToSet)
	
//	IF iNumGames >= 100
//		IF NOT IS_BIT_SET(g_savedGlobals.sDartsData.iDartTimePlayed, gDARTS_LVL_UP5_SHWN)
//			iBitToSet = gDARTS_LVL_UP5_SHWN
//			RETURN TRUE
//		ENDIF
//	ELIF iNumGames >= 70
//		IF NOT IS_BIT_SET(g_savedGlobals.sDartsData.iDartTimePlayed, gDARTS_LVL_UP4_SHWN)
//			iBitToSet = gDARTS_LVL_UP4_SHWN
//			RETURN TRUE
//		ENDIF
//	ELIF iNumGames >= 50
//		IF NOT IS_BIT_SET(g_savedGlobals.sDartsData.iDartTimePlayed, gDARTS_LVL_UP3_SHWN)
//			iBitToSet = gDARTS_LVL_UP3_SHWN
//			RETURN TRUE
//		ENDIF
//	ELIF iNumGames >= 30
//		IF NOT IS_BIT_SET(g_savedGlobals.sDartsData.iDartTimePlayed, gDARTS_LVL_UP2_SHWN)
//			iBitToSet = gDARTS_LVL_UP2_SHWN
//			RETURN TRUE
//		ENDIF
//	ELIF iNumGames >= 10
	IF iNumGames >= 10
		IF NOT IS_BIT_SET(g_savedGlobals.sDartsData.iDartTimePlayed, gDARTS_LVL_UP1_SHWN)
			iBitToSet = gDARTS_LVL_UP1_SHWN
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

SCRIPT (DARTS_ARGS DartsArgs)
	
	//Necessary variables
	DART_GAME DartGame
	DARTS_TUTORIAL_STAGE DartsTutorialStage
	DARTS_PLAYER_POS DartsPlayerPositions
	DARTS_STARTING_RETICLE_POS DartsInitReticlePos
	DARTS_SCOREBOARD DartsSB
	DARTS_UI DartsUI
	
	END_SCREEN_DATASET DartsEndScreen
	
	BOOL bPlayerQuitEarly
	BOOL bUsedSetsLegs
	BOOL bRequestGameSaid
	BOOL bAcceptGameSaid
	BOOL bCanGoToCleanup
	
	INT iNameTemp
	INT iSplashStage = 0
	INT idz
	
	FLOAT fStaminaPercentage
	FLOAT fSteadyShotLength
	//	FLOAT fRotTemp
	
	VECTOR vHickBar, vRockClub //, vLostHideout
	VECTOR vTemp
//	VECTOR vCamTemp
	VECTOR scenePosition
	VECTOR sceneRotation
	
	STRING sPlayerFacialAnimClip
	
	structTimer ThrowTimer
	structTimer TutorialTimer
	structTimer DialogTimer
	
	PED_INDEX piNearbyPeds[20]
	
	OBJECT_INDEX oiDartChallenge
	
	sFacialDictPed = "facials@gen_female@variations@happy"
	
	SWITCH GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
		CASE CHAR_MICHAEL
			sFacialDictPlayer = "facials@p_m_zero@variations@happy"
		BREAK
		CASE CHAR_FRANKLIN
			sFacialDictPlayer = "facials@p_m_one@variations@happy"
		BREAK
		CASE CHAR_TREVOR
			sFacialDictPlayer = "facials@p_m_two@variations@happy"
		BREAK
	ENDSWITCH
	
	sFacialClip[0] = "mood_happy_1"
	sFacialClip[1] = "mood_happy_2"
	sFacialClip[2] = "mood_happy_3"
	
	sOutroWin[0] = "darts_outro_01_guy1"
	sOutroWin[1] = "darts_outro_02_guy2"
	sOutroWin[2] = "darts_outro_03_guy2"
	
	sOutroLoss[0] = "darts_outro_01_guy2"
	sOutroLoss[1] = "darts_outro_02_guy1"
	sOutroLoss[2] = "darts_outro_03_guy1"
	
	sOutroCam[0] = "darts_outro_01_cam"
	sOutroCam[1] = "darts_outro_02_cam"
	sOutroCam[2] = "darts_outro_03_cam"
	
	piDartPlayers[0] = PLAYER_PED_ID()
	
	bNewOpponent = TRUE
	
	vHickBar = <<1992.293213,3050.582764,47.989731>>
	vRockClub = <<-572.040649,294.195770,79.937401>>
	
	SETUP_DARTS_PC_CONTROLS()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vRockClub) < GET_PLAYER_DISTANCE_FROM_LOCATION(vHickBar)
//			DartsLocation = DARTSLOCATION_ROCK_CLUB
//			SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
//				CASE 0 	
//					modelOpponent = A_M_Y_VINEWOOD_01
//					CDEBUG1LN(DEBUG_DARTS, "modelOpponent = A_M_Y_VINEWOOD_01")
//				BREAK
//				CASE 1 	
//					modelOpponent = A_M_Y_STLAT_01
//					CDEBUG1LN(DEBUG_DARTS, "modelOpponent = A_M_Y_VINEWOOD_01")
//				BREAK
//				CASE 2 	
//					modelOpponent = A_M_Y_VINEWOOD_04
//					CDEBUG1LN(DEBUG_DARTS, "modelOpponent = A_M_Y_VINEWOOD_01")
//				BREAK
//				CASE 3 	
//					modelOpponent = A_M_Y_STWHI_02	
//					CDEBUG1LN(DEBUG_DARTS, "modelOpponent = A_M_Y_VINEWOOD_01")
//				BREAK
//				CASE 4 	
//					modelOpponent = A_M_Y_VINEWOOD_03
//					CDEBUG1LN(DEBUG_DARTS, "modelOpponent = A_M_Y_VINEWOOD_01")
//				BREAK
//			ENDSWITCH			
//			DartsLoc = ALOC_darts_rockClub
//			
//			CDEBUG1LN(DEBUG_DARTS, "Darts game started at Rock Club")
//		ELSE
			DartsLocation 			= DARTSLOCATION_HICK_BAR
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0 	
					modelOpponent = A_F_M_SALTON_01
					CDEBUG1LN(DEBUG_DARTS, "modelOpponent = A_F_M_SALTON_01")
				BREAK
				CASE 1 	
					modelOpponent = A_F_O_SALTON_01
					CDEBUG1LN(DEBUG_DARTS, "modelOpponent = A_F_O_SALTON_01")
				BREAK
			ENDSWITCH
			DartsLoc = ALOC_darts_hickBar
			
			CDEBUG1LN(DEBUG_DARTS, "Darts game started at Hick Bar")
//		ENDIF
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		//TASK_SWAP_WEAPON(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(DartsArgs.oiDartBoard)
		CDEBUG1LN(DEBUG_DARTS, "DartsArgs.oiDartBoard DOES NOT exist.")
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			IF DartsLocation = DARTSLOCATION_LOST_HIDEOUT
//				DartsArgs.vDartBoard = vLostHideout
//				DartsArgs.fBoardHeading =  -137.348129
			IF DartsLocation = DARTSLOCATION_ROCK_CLUB
				DartsArgs.vDartBoard = vRockClub
				DartsArgs.fBoardHeading =  -0.09
			ELSE
				DartsArgs.vDartBoard = vHickBar
				DartsArgs.fBoardHeading =  57.783146
			ENDIF
			
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(DartsArgs.vDartBoard, 5.0, PROP_DART_BD_CAB_01)
				DartsArgs.oiDartBoard = GET_CLOSEST_OBJECT_OF_TYPE(DartsArgs.vDartBoard, 5.0, PROP_DART_BD_CAB_01)
				GET_COORDS_AND_ROTATION_OF_CLOSEST_OBJECT_OF_TYPE(DartsArgs.vDartBoard, 5.0, PROP_DART_BD_CAB_01, DartsArgs.vDartBoard, vTemp, EULER_XYZ)
				DartsArgs.fBoardHeading = vTemp.z
				CDEBUG1LN(DEBUG_DARTS, "PROP_DART_BD_CAB_01 found in area.")
			ELSE
				CDEBUG1LN(DEBUG_DARTS, "PROP_DART_BD_CAB_01 not found in area of Darts Args.")
			ENDIF
		ENDIF
		
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "DartsArgs.oiDartBoard DOES exist.")
		vTemp = GET_ENTITY_COORDS(DartsArgs.oiDartBoard)
		CDEBUG1LN(DEBUG_DARTS, "DartsArgs.oiDartBoard is located in ", vTemp)
		CDEBUG1LN(DEBUG_DARTS, "DartsArgs.oiDartBoard is heading ", GET_ENTITY_HEADING(DartsArgs.oiDartBoard))
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "********** DartsArgs.vDartBoard", DartsArgs.vDartBoard, "**************")	
	CDEBUG1LN(DEBUG_DARTS, "********** DartsArgs.fBoardHeading", DartsArgs.fBoardHeading, "*************")	
	
	//Gets the friend for a friend activity
	IF NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())
		startActivity(DartsLoc) 
		IF IS_PED_IN_ANY_VEHICLE(FRIEND_A_PED_ID())		
			//SPECIAL_FUNCTION_DO_NOT_USE(FRIEND_A_PED_ID(), TRUE)
			TASK_LEAVE_ANY_VEHICLE(FRIEND_A_PED_ID())
		ENDIF
		
		piDartPlayers[1] = FRIEND_A_PED_ID()
		
	    //tasking the friend to do nothing is sufficient for now.
	    //TASK_PAUSE(FRIEND_A_PED_ID(), -1)             	  
		CDEBUG1LN(DEBUG_DARTS, "[DARTS] starting friend activity")
	ELSE
	 	CDEBUG1LN(DEBUG_DARTS, "[DARTS] friend not valid, using normal opponent")
		SET_MISSION_FLAG(TRUE)
	ENDIF
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		//Cleanups
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		CLEANUP_DARTS_GAME(DartGame, DartsUI, DartsEndScreen)
	ENDIF
		
	//FORCE_CLEANUP_FLAG_SP_TO_MP
	#IF IS_DEBUG_BUILD
		ADD_WIDGETS(DebugBoard)
	#ENDIF
	
	//SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE)
	DISPLAY_AREA_NAME(FALSE)
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	DISABLE_CELLPHONE(TRUE)
	
	// max steady shot length = 1320
	iStaminaXP = GET_SP_PLAYER_PED_STAT_VALUE(GET_PLAYER_PED_ENUM(piDartPlayers[0]), PS_STAMINA)
	CDEBUG1LN(DEBUG_DARTS, "stamina stat = ", iStaminaXP)
	
	fStaminaPercentage = TO_FLOAT(iStaminaXP)/100
	fSteadyShotLength = fStaminaPercentage * 1200
	iSteadyShotLength = ROUND(fSteadyShotLength)
	
	CDEBUG1LN(DEBUG_DARTS, "steady shot = ", iSteadyShotLength)
	IF iSteadyShotLength < 660
		iSteadyShotLength = 660
		CDEBUG1LN(DEBUG_DARTS, "steady shot readjusted to 660")
	ENDIF
	
	DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
	
    WHILE (TRUE)
		// main loop
       	WAIT(0)
		
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0.0, -0.0375)
		
		// debug builds only
		#IF IS_DEBUG_BUILD
			DO_DEBUG_PROCS(DebugBoard, DartGame, spriteDebug, siScoreBoardDebug, piDartPlayers)
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				CDEBUG1LN(DEBUG_DARTS, "Darts Script Debug Quit")
				//bDartsNoSocialClub = TRUE
				CLEANUP_DARTS_GAME(DartGame, DartsUI, DartsEndScreen)
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				CDEBUG1LN(DEBUG_DARTS, "Darts Script Debug Won")
				//bDartsNoSocialClub = TRUE
				GAME_WON(DartGame)
			ENDIF
		#ENDIF
		
		// draw the scoreboard scaleform
		//IF DartGame.Stage > STAGE_CHALLENGE AND DartGame.Stage < STAGE_FINISHED
		IF bActivateScoreBoard AND DartGame.Stage < STAGE_FINISHED
			//DRAW_SCALEFORM_MOVIE(DartsSB.siScoreBoard, 0.275, 0.3, 0.24, 0.5, 255, 255, 255, 0) //placeholder 2D scoreboard
			DRAW_SCALEFORM_MOVIE_3D(DartsSB.siScoreBoard, DartsSB.vScoreBoardPos, DartsSB.vScoreBoardRot, DartsSB.vScoreBoardScale, DartsSB.vScoreBoardScale)
		ENDIF
		
		IF (IS_PED_INJURED(DartsArgs.piDartOpponent)
			AND NOT DOES_ENTITY_EXIST(FRIEND_A_PED_ID()))
		OR (DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
			AND IS_PED_INJURED(FRIEND_A_PED_ID()))
			CDEBUG1LN(DEBUG_DARTS, "opponent died, cleaning up.")
			CLEANUP_DARTS_GAME(DartGame, DartsUI, DartsEndScreen)
		ENDIF
		
		IF NOT IS_PED_INJURED(piDartPlayers[0])
			
			SET_PED_RESET_FLAG(piDartPlayers[0], PRF_DisableAgitation, TRUE)
			SET_PED_RESET_FLAG(piDartPlayers[0], PRF_CannotBeTargetedByAI, TRUE)
			DISABLE_SELECTOR_THIS_FRAME()
			
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
			
			REPEAT GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), piNearbyPeds) idz
				IF piNearbyPeds[idz] != piDartPlayers[1]
					IF NOT IS_PED_INJURED(piNearbyPeds[idz])
						SET_PED_RESET_FLAG(piNearbyPeds[idz], PRF_DisableTalk, TRUE)
						//SET_PED_CONFIG_FLAG(piNearbyPeds[idz], PCF_CanBeAgitated, FALSE)
					ENDIF
				ENDIF
			ENDREPEAT
			
			// run appropriate stage
			SWITCH DartGame.Stage
				CASE STAGE_PRE_INIT
					CDEBUG1LN(DEBUG_DARTS, "Reached STAGE_PRE_INIT")
					
					CLEAR_HELP()
					
					INIT_BOARD(DartGame.DartsBoard, DartsArgs)
					INIT_CAM(DartGame.DartsBoard.vDartBoard, DartGame.DartsBoard.fBoardHeading)
					
					IF DOES_ENTITY_EXIST(DartGame.DartsBoard.oiDartBoard)
						//ATTACH_CAM_TO_ENTITY(camZoomBoard, DartGame.DartsBoard.oiDartBoard, <<0.7792, -1.1380, 0.1814>>)
						//POINT_CAM_AT_ENTITY(camZoomBoard, DartGame.DartsBoard.oiDartBoard, <<-0.9485, 1.2798, -0.2296>>)
						
						vFPDartsPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(DartGame.DartsBoard.oiDartBoard, <<0.7792, -1.1380, 0.1814>>)
						vFPDartsRot = << -7.9947, 0.0, DartGame.DartsBoard.fBoardHeading + 36.191757 >> 
						
						camZoomBoard = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vFPDartsPos, vFPDartsRot)
						SET_CAM_FOV(camZoomBoard, 30.0)
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "dartboard object does not exist!!")
					ENDIF
					
					SC_LEADERBOARD_CACHE_CLEAR_ALL()
					CDEBUG1LN(DEBUG_DARTS, "cleared leaderboard cache")
					
					SETTIMERA(0)
					DartGame.Stage = STAGE_PRE_INIT_WAIT
					
				BREAK
				
				CASE STAGE_PRE_INIT_WAIT
					IF TIMERA() > 500
					AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
						CDEBUG1LN(DEBUG_DARTS, "Reached STAGE_PRE_INIT_WAIT")
						
						CLEAR_AREA(DartsArgs.vDartBoard, 0.5, TRUE)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
							CLEAR_SEQUENCE_TASK(siTemp)
							OPEN_SEQUENCE_TASK(siTemp)
								//failsafe if player gets on a bike before the control is turned off
								TASK_LEAVE_ANY_VEHICLE(NULL)
								//TASK_GO_STRAIGHT_TO_COORD(NULL, vInitLostHideoutPos, PEDMOVEBLENDRATIO_WALK)
							CLOSE_SEQUENCE_TASK(siTemp)
							TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siTemp)
							
						ENDIF
						
						vInitLostHideoutPos = vInitLostHideoutPos
						
						DartGame.Stage = STAGE_INIT
					
					ENDIF
					
				BREAK
				
				CASE STAGE_INIT
					CDEBUG1LN(DEBUG_DARTS, "Reached STAGE_INIT")
					
					//init variables
					
					// CAM POS Offset
//					vCamTemp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(DartsArgs.oiDartBoard, <<1992.679, 3050.739, 47.660>>)
//					CDEBUG1LN(DEBUG_DARTS, "TUTORIAL CAM OFFSET!!: ", vCamTemp)
//					fRotTemp = GET_ENTITY_HEADING(DartsArgs.oiDartBoard) - 54.000
//					CDEBUG1LN(DEBUG_DARTS, "rotation : ", fRotTemp)
					
					INT i, j
					REPEAT NUM_DART_PLAYERS i
						REPEAT NUM_DART_TURNS j 
							INIT_DARTS(DartGame.Darts[i][j], i)
						ENDREPEAT
					ENDREPEAT
					
					INT k
					REPEAT NUM_DART_TURNS k
						INIT_DARTS(DartGame.TutorialDarts[k], 0)
					ENDREPEAT
					
					DartsTutorialStage = TS_DOUBLE //TS_THROWING
					
					INIT_SPRITE_RETICLE()
					DARTS_INIT_SCOREBOARD(DartsSB, ENUM_TO_INT(DartsLocation), DartGame.DartsBoard.vDartBoard, DartGame.DartsBoard.fBoardHeading)
					
					IF DOES_ENTITY_EXIST(DartGame.DartsBoard.oiDartBoard)
						vTemp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(DartGame.DartsBoard.oiDartBoard, DartsSB.vScoreBoardPos)
						CDEBUG1LN(DEBUG_DARTS, "offset for scoreboard = ", vTemp)
					ENDIF
					
					//request streams
					REQUEST_STREAMS(DartsSB, DartsUI)
					REGISTER_SCRIPT_WITH_AUDIO()
					
					CDEBUG1LN(DEBUG_DARTS, "Finished Stage_init")
					
					DartGame.Stage = STAGE_STREAMING
					
				BREAK
			
				CASE STAGE_STREAMING
					//wait for all streams to load up before proceeding
					IF HAVE_STREAMS_LOADED(DartsSB, DartsUI)
						
						//create all necessary components
						
						GET_WORLD_OFFSETS(DartGame.DartsBoard, DartsPlayerPositions, DartsInitReticlePos)
						
						//writing the names on the chalkboard
						
						eDartsPlayerOne = GET_PLAYER_PED_ENUM(piDartPlayers[0])
						SWITCH eDartsPlayerOne
							CASE CHAR_MICHAEL
								sDartsPlayerOne = GET_DART_LOCAL_PLAYER_NAME("MICHAEL")
								
								IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_TUTORIAL_SHOWN)
									sPlayerFacialAnimClip = "darts_ig_intro_player_0_face"
								ELSE
									sPlayerFacialAnimClip = "darts_ig_intro_alt1_player_0_face"
								ENDIF
							BREAK
							CASE CHAR_FRANKLIN
								sDartsPlayerOne = GET_DART_LOCAL_PLAYER_NAME("FRANKLIN")
								bFranklin = TRUE
								
								IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_TUTORIAL_SHOWN)
									sPlayerFacialAnimClip = "darts_ig_intro_player_1_face"
								ELSE
									sPlayerFacialAnimClip = "darts_ig_intro_alt1_player_1_face"
								ENDIF
							BREAK
							CASE CHAR_TREVOR
								sDartsPlayerOne = GET_DART_LOCAL_PLAYER_NAME("TREVOR")
								
								IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_TUTORIAL_SHOWN)
									sPlayerFacialAnimClip = "darts_ig_intro_player_2_face"
								ELSE
									sPlayerFacialAnimClip = "darts_ig_intro_alt1_player_2_face"
								ENDIF
							BREAK
						ENDSWITCH
						
						IF NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())
							// setting name for friend activity
							eDartsPlayerTwo = GET_PLAYER_PED_ENUM(FRIEND_A_PED_ID())
							IF eDartsPlayerTwo = NO_CHARACTER
								eDartsPlayerTwo = GET_NPC_PED_ENUM(FRIEND_A_PED_ID())
							ENDIF
							SWITCH eDartsPlayerTwo
								CASE CHAR_MICHAEL
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("MICHAEL")
								BREAK
								CASE CHAR_FRANKLIN
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("FRANKLIN")
								BREAK
								CASE CHAR_TREVOR
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("TREVOR")
								BREAK
								CASE CHAR_LAMAR
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("LAMAR")
									SET_PED_COMPONENT_VARIATION(FRIEND_A_PED_ID(), PED_COMP_HAND, 2, 0)
								BREAK
								CASE CHAR_JIMMY
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("JIMMY")
								BREAK
								DEFAULT
									sDartsPlayerTwo = "NEW_GUY"
								BREAK
							ENDSWITCH
							CDEBUG1LN(DEBUG_DARTS, "Setting name for friend activity to ", sDartsPlayerTwo)
						ELSE
							// Setting random name for ped
							iNameTemp = GET_RANDOM_INT_IN_RANGE(0, 200)
							IF DartsLocation = DARTSLOCATION_ROCK_CLUB
								IF iNameTemp < 51
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("RAYMOND")
									iP2NameHash = HASH("RAYMOND")
								ELIF iNameTemp < 101
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("JOHAN")
									iP2NameHash = HASH("JOHAN")
								ELIF iNameTemp < 151
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("STAN")
									iP2NameHash = HASH("STAN")
								ELSE
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("VINCE")
									iP2NameHash = HASH("VINCE")
								ENDIF
							ELSE
								IF iNameTemp < 51
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("KRISTY")
									iP2NameHash = HASH("KRISTY")
								ELIF iNameTemp < 101
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("MARLENE")
									iP2NameHash = HASH("MARLENE")
								ELIF iNameTemp < 151
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("LORIE")
									iP2NameHash = HASH("LORIE")
								ELSE
									sDartsPlayerTwo = GET_DART_LOCAL_PLAYER_NAME("SHELLEY")
									iP2NameHash = HASH("SHELLEY")
								ENDIF
							ENDIF
							CDEBUG1LN(DEBUG_DARTS, "Setting name for non friend activity to ", sDartsPlayerTwo)
						ENDIF
						
						oiReticle = CREATE_OBJECT(modelReticleRegular, DartsInitReticlePos.vInitReticlePos)
						
						DartGame.iDartGamesWon[0] = 0
						DartGame.iDartGamesWon[1] = 0
						
						//iDartStartTime = GET_GAME_TIMER()
						
						DartGame.Stage = STAGE_SPAWNING
						CDEBUG1LN(DEBUG_DARTS, "Finished Stage_streaming")
					ENDIF					
				BREAK
				
				CASE STAGE_SPAWNING
					
					IF NOT IS_INTERPOLATING_TO_SCRIPT_CAMS()
						
						oiDartChallenge = CREATE_OBJECT(PROP_DART_1, DartsInitReticlePos.vInitReticlePos)
						
						INIT_PEDS(DartGame.DartsBoard.fBoardHeading, DartsPlayerPositions, piNearbyPeds, DartsArgs)
						DARTS_ADD_NAMES(DartsSB, sDartsPlayerOne, sDartsPlayerTwo)
						
						CLEAR_PED_TASKS(piDartPlayers[1])
						
						camDartsSync = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE)
							
							CDEBUG1LN(DEBUG_DARTS, "Set vehicle save position, set player to walking")
						ENDIF
						
						//bVehicleSaved = ODDJOB_SAVE_VEHICLE_AND_WARP(VehicleSaveArgs, PICK_VECTOR((DartsLocation = DARTSLOCATION_ROCK_CLUB), << -572.0901, 268.8933, 81.7819 >>, << 1995.0244, 3055.6152, 46.0753 >>),
						//											TRUE,PICK_FLOAT((DartsLocation = DARTSLOCATION_ROCK_CLUB), 85.7488, 241.7907))
						
						
						ADD_PED_FOR_DIALOGUE(DartGame.DartsConvo, 0, PLAYER_PED_ID(), sDartsPlayerOne)
						
						IF IS_ENTITY_DEAD(FRIEND_A_PED_ID())	
							CDEBUG1LN(DEBUG_DARTS, "Adding dialogue for a non-friend activity")
							
							//TODO: fix dialogue
							IF DartsLocation = DARTSLOCATION_ROCK_CLUB
								ADD_PED_FOR_DIALOGUE(DartGame.DartsConvo, 3, piDartPlayers[1], "DartsBillLost1")
							ELSE
								ADD_PED_FOR_DIALOGUE(DartGame.DartsConvo, 3, piDartPlayers[1], "DartsMelHick1")
							ENDIF
							
							iSplashStage = -1
						ELSE
							
							ADD_PED_FOR_DIALOGUE(DartGame.DartsConvo, 3, FRIEND_A_PED_ID(), sDartsPlayerTwo)
							CDEBUG1LN(DEBUG_DARTS, "This is a friend activity, dialogue to be added later")
							CDEBUG1LN(DEBUG_DARTS, "Also setting the intro scene to be skipped")
							RESTART_TIMER_NOW(DialogTimer)
							iSplashStage = 6
						ENDIF
						
						IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_TUTORIAL_SHOWN)
							iSplashStage = -1
						ELSE
							iSplashStage = 3
						ENDIF
						
						UPDATE_HIGHLIGHT(DartsSB, 0)
						
						CDEBUG1LN(DEBUG_DARTS, "Finished STAGE_SPAWNING")
						DartGame.Stage = STAGE_CHALLENGE
					ENDIF
				BREAK
				
				CASE STAGE_CHALLENGE
					
					IF NOT IS_PED_INJURED(piDartPlayers[1])
					AND IS_PED_ON_FOOT(piDartPlayers[0])
					AND NOT IS_PED_IN_ANY_VEHICLE(piDartPlayers[0], TRUE)
					AND IS_PED_ON_FOOT(piDartPlayers[1])
					AND NOT IS_PED_IN_ANY_VEHICLE(piDartPlayers[1], TRUE)
						
						bVehicleSaved = ODDJOB_SAVE_VEHICLE(VehicleSaveArgs)
						
						IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
						AND GET_GAME_TIMER() >= iAllowSkipCutsceneTime + 1000
						AND iSplashStage != 9
							IF NOT IS_SCREEN_FADED_OUT()
							AND NOT IS_SCREEN_FADING_OUT()
								DO_SCREEN_FADE_OUT(500)
							ENDIF
						ENDIF
						
						IF IS_SCREEN_FADED_OUT()
						AND iSplashStage != 9
							HANG_UP_AND_PUT_AWAY_PHONE()
							KILL_ANY_CONVERSATION()
							iSplashStage = 9
						ENDIF
						
						SWITCH iSplashStage
							// -------------- First game in a session -------------------------
							CASE -1
								IF NOT IS_PED_INJURED(piDartPlayers[1])
									iSceneId = CREATE_SYNCHRONIZED_SCENE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(DartsArgs.oiDartBoard, << 0.337842, -0.243051, -0.329731 >>), 
																		<< 0, 0, GET_ENTITY_HEADING(DartsArgs.oiDartBoard) - 3.783146>>)
									camDartsSync = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
									PLAY_SYNCHRONIZED_CAM_ANIM(camDartsSync, iSceneId, "darts_ig_intro_cam", "mini@dartsintro")
									SET_CAM_ACTIVE(camDartsSync, TRUE)
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
									TASK_CLEAR_LOOK_AT(piDartPlayers[0])
									TASK_CLEAR_LOOK_AT(piDartPlayers[1])
									CLEAR_PED_TASKS_IMMEDIATELY(piDartPlayers[0])
									CLEAR_PED_TASKS_IMMEDIATELY(piDartPlayers[1])
									TASK_SYNCHRONIZED_SCENE (piDartPlayers[1], iSceneId, "mini@dartsintro", "darts_ig_intro_guy1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
									TASK_SYNCHRONIZED_SCENE (piDartPlayers[0], iSceneId, "mini@dartsintro", "darts_ig_intro_guy2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
									TASK_PLAY_ANIM(piDartPlayers[1], "mini@dartsintro", "darts_ig_intro_guy1_face", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
									TASK_PLAY_ANIM(piDartPlayers[0], "mini@dartsintro", sPlayerFacialAnimClip, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
									PLAY_SYNCHRONIZED_ENTITY_ANIM( oiDartChallenge, iSceneId, "darts_ig_intro_dart","mini@dartsintro" , INSTANT_BLEND_IN )
									//PLAY_PED_AMBIENT_SPEECH(piDartPlayers[0], "DARTS_REQUEST_GAME")
									START_TIMER_NOW_SAFE(DialogTimer)
									iSplashStage++
								ENDIF
							BREAK
							
							CASE 0
								IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
									
									IF NOT bRequestGameSaid
										IF GET_TIMER_IN_SECONDS_SAFE(DialogTimer) > 1.5
											PLAY_PED_AMBIENT_SPEECH(piDartPlayers[0], "DARTS_REQUEST_GAME", SPEECH_PARAMS_FORCE_NORMAL)
											bRequestGameSaid = TRUE
											CANCEL_TIMER(DialogTimer)
										ENDIF
									ELSE
										IF NOT IS_AMBIENT_SPEECH_PLAYING(piDartPlayers[0])
											IF NOT bAcceptGameSaid
												IF NOT IS_TIMER_STARTED(DialogTimer)
													RESTART_TIMER_NOW(DialogTimer)
												ELIF GET_TIMER_IN_SECONDS_SAFE(DialogTimer) > 0.5
													PLAY_AGREE_AUDIO(piDartPlayers[1])
													CANCEL_TIMER(DialogTimer)
													bAcceptGameSaid = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									SWITCH iTurn
										CASE 0
											START_TIMER_NOW_SAFE(DialogTimer)
											
											PRINT_HELP("DARTS_HOW_TO_2")
											iTurn++
										BREAK
										
										CASE 1
//											IF GET_TIMER_IN_SECONDS_SAFE(DialogTimer) > 1.25
//												CANCEL_TIMER(DialogTimer)
//												PLAY_PED_AMBIENT_SPEECH(piDartPlayers[0], "DARTS_REQUEST_GAME")
//											ENDIF
											
											IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.25
												START_TIMER_NOW_SAFE(DialogTimer)
												PRINT_HELP("DARTS_HOW_TO_3")
												iTurn++
											ENDIF
										BREAK
										
										CASE 2
//											IF GET_TIMER_IN_SECONDS_SAFE(DialogTimer) > 1.25
//												CANCEL_TIMER(DialogTimer)
//												PLAY_AGREE_AUDIO(piDartPlayers[1])
//											ENDIF
											
											IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.55
												PRINT_HELP("DARTS_HOW_3A")
												iTurn++
											ENDIF
										BREAK
										
										CASE 3
											IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.85
												PRINT_HELP("DARTS_HOW_3A")
												iTurn++
											ENDIF
										BREAK
									ENDSWITCH
									
									IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.95
										CDEBUG1LN(DEBUG_DARTS, "sync scene almost done, going to splash stage 1")
										bActivateScoreBoard = TRUE
										iSplashStage++
									ELSE
										IF (GET_GAME_TIMER() % 1000) < 50
											CDEBUG1LN(DEBUG_DARTS, "sync scene phase = ", GET_SYNCHRONIZED_SCENE_PHASE(iSceneId))
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE 1
								IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.99
									
									IF NOT IS_PED_INJURED(piDartPlayers[1])
										DARTS_TUTORIAL_CAM(DartGame.DartsBoard.vDartBoard, DartGame.DartsBoard.fBoardHeading,
														GET_TUTORIAL_CAM_COORD_OFFSET(TS_DOUBLE), GET_TUTORIAL_CAM_ROT_OFFSET(), 0, TRUE)
										
										iTurn = 0
										CLEAR_HELP()
										
										DESTROY_CAM(camDartsSync)
										CLEAR_PED_TASKS (piDartPlayers[0])
										CLEAR_PED_TASKS (piDartPlayers[1])
										STOP_SYNCHRONIZED_ENTITY_ANIM( oiDartChallenge, INSTANT_BLEND_OUT, FALSE)
										
										DELETE_OBJECT(oiDartChallenge)
										
										STOP_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_GREETING))
										
										CANCEL_TIMER(DialogTimer)
										DartGame.Stage = STAGE_SPLASH
										iSplashStage = 0
									ENDIF
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "sync scene is still running")
								ENDIF
							BREAK
							
							// -------------- Succesive games in a session -------------------------
							CASE 3
								IF NOT IS_PED_INJURED(piDartPlayers[1])
									iSceneId = CREATE_SYNCHRONIZED_SCENE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(DartsArgs.oiDartBoard, << 0.337842, -0.243051, -0.329731 >>), 
																		<< 0, 0, GET_ENTITY_HEADING(DartsArgs.oiDartBoard) - 3.783146>>)
									camDartsSync = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
									PLAY_SYNCHRONIZED_CAM_ANIM(camDartsSync, iSceneId, "darts_ig_intro_alt1_cam", "mini@dartsintro_alt1")
									SET_CAM_ACTIVE(camDartsSync, TRUE)
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
									TASK_CLEAR_LOOK_AT(piDartPlayers[0])
									TASK_CLEAR_LOOK_AT(piDartPlayers[1])
									CLEAR_PED_TASKS_IMMEDIATELY(piDartPlayers[0])
									CLEAR_PED_TASKS_IMMEDIATELY(piDartPlayers[1])
									TASK_SYNCHRONIZED_SCENE (piDartPlayers[1], iSceneId, "mini@dartsintro_alt1", "darts_ig_intro_alt1_guy1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
									TASK_SYNCHRONIZED_SCENE (piDartPlayers[0], iSceneId, "mini@dartsintro_alt1", "darts_ig_intro_alt1_guy2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
									TASK_PLAY_ANIM(piDartPlayers[1], "mini@dartsintro_alt1", "darts_ig_intro_alt1_guy1_face", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
									TASK_PLAY_ANIM(piDartPlayers[0], "mini@dartsintro_alt1", sPlayerFacialAnimClip, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
									PLAY_SYNCHRONIZED_ENTITY_ANIM( oiDartChallenge, iSceneId, "darts_ig_intro_alt1_dart","mini@dartsintro_alt1" , INSTANT_BLEND_IN )
									START_TIMER_NOW_SAFE(DialogTimer)
									iSplashStage++
								ENDIF
							BREAK
							
							CASE 4
								IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
									
									IF NOT bRequestGameSaid
										IF GET_TIMER_IN_SECONDS_SAFE(DialogTimer) > 0.5
											PLAY_PED_AMBIENT_SPEECH(piDartPlayers[0], "DARTS_REQUEST_GAME", SPEECH_PARAMS_FORCE_NORMAL)
											bRequestGameSaid = TRUE
											CANCEL_TIMER(DialogTimer)
										ENDIF
									ELSE
										IF NOT IS_AMBIENT_SPEECH_PLAYING(piDartPlayers[0])
											IF NOT bAcceptGameSaid
												IF NOT IS_TIMER_STARTED(DialogTimer)
													RESTART_TIMER_NOW(DialogTimer)
												ELIF GET_TIMER_IN_SECONDS_SAFE(DialogTimer) > 0.25
													PLAY_AGREE_AUDIO(piDartPlayers[1])
													CANCEL_TIMER(DialogTimer)
													bAcceptGameSaid = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									
									IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.95
										CDEBUG1LN(DEBUG_DARTS, "sync scene almost done, going to splash stage 1")
										bActivateScoreBoard = TRUE
										iSplashStage++
									ELSE
										IF (GET_GAME_TIMER() % 1000) < 50
											CDEBUG1LN(DEBUG_DARTS, "sync scene phase = ", GET_SYNCHRONIZED_SCENE_PHASE(iSceneId))
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE 5
								IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.99
									
									IF NOT IS_PED_INJURED(piDartPlayers[1])
										
										DARTS_GAME_CAM()
										
										DESTROY_CAM(camDartsSync)
										CLEAR_PED_TASKS (piDartPlayers[0])
										CLEAR_PED_TASKS (piDartPlayers[1])
										STOP_SYNCHRONIZED_ENTITY_ANIM( oiDartChallenge, INSTANT_BLEND_OUT, FALSE)
										
										DELETE_OBJECT(oiDartChallenge)
										
										STOP_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_GREETING))
										
										CANCEL_TIMER(DialogTimer)
										DartGame.Stage = STAGE_SPLASH
										iSplashStage = 0
									ENDIF
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "sync scene is still running")
								ENDIF
							BREAK
							
							// -------------- skipping the intro -------------------------
							CASE 9
								IF IS_SCREEN_FADED_OUT()
									
									bActivateScoreBoard = TRUE
									
									iTurn = 0
									CLEAR_HELP()
									
									IF NOT IS_PED_INJURED(piDartPlayers[1])
										SET_ENTITY_COORDS(piDartPlayers[0], DartsPlayerPositions.vDartPlayrPos)
										SET_ENTITY_HEADING(piDartPlayers[0], DartGame.DartsBoard.fBoardHeading)
										SET_ENTITY_COORDS(piDartPlayers[1], DartsPlayerPositions.vOpponentPos)
										SET_ENTITY_HEADING(piDartPlayers[1], DartGame.DartsBoard.fBoardHeading)
										CLEAR_PED_TASKS (piDartPlayers[0])
										CLEAR_PED_TASKS (piDartPlayers[1])
									ENDIF
									
									DESTROY_CAM(camDartsSync)
									STOP_SYNCHRONIZED_ENTITY_ANIM( oiDartChallenge, INSTANT_BLEND_OUT, FALSE)
									DELETE_OBJECT(oiDartChallenge)
									
									
									IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_TUTORIAL_SHOWN)
										DARTS_TUTORIAL_CAM(DartGame.DartsBoard.vDartBoard, DartGame.DartsBoard.fBoardHeading,
														GET_TUTORIAL_CAM_COORD_OFFSET(TS_DOUBLE), GET_TUTORIAL_CAM_ROT_OFFSET(), 0, TRUE)
									ELSE
										DARTS_GAME_CAM()
									ENDIF
									
									STOP_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_GREETING))
									
									CANCEL_TIMER(DialogTimer)
									
									DO_SCREEN_FADE_IN(500)
									
									DartGame.Stage = STAGE_SPLASH
									iSplashStage = 0
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE STAGE_SPLASH
					
					SWITCH iSplashStage
						CASE 0
							CDEBUG1LN(DEBUG_DARTS, "STAGE_SPLASH, setting up cam iSplashStage = ", iSplashStage)
							
							iAllowSkipCutsceneTime = GET_GAME_TIMER()
							
							IF NOT IS_PED_INJURED(piDartPlayers[1])
								SET_ENTITY_COORDS(piDartPlayers[0], DartsPlayerPositions.vDartPlayrPos)
								SET_ENTITY_HEADING(piDartPlayers[0], DartGame.DartsBoard.fBoardHeading)
								SET_ENTITY_COORDS(piDartPlayers[1], DartsPlayerPositions.vOpponentPos)
								SET_ENTITY_HEADING(piDartPlayers[1], DartGame.DartsBoard.fBoardHeading)
							ENDIF
							
							//iSplashStage++ // skipping splash screen
							START_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_MENU))
							
							iSplashStage = 0
							CDEBUG1LN(DEBUG_DARTS, "Going to STAGE_TUTORIAL")
							DartGame.Stage = STAGE_TUTORIAL
							
						BREAK
						CASE 1
							// print splash
							IF (GET_GAME_TIMER() - iAllowSkipCutsceneTime) > 1500
								CDEBUG1LN(DEBUG_DARTS, "STAGE_SPLASH, printing splash iSplashStage = ", iSplashStage)
								
								SET_SCALEFORM_BIG_MESSAGE(DartsUI.siBigMessage, "DARTS_TITLE")
								//SET_SCALEFORM_BIG_MESSAGE(DartsUI.siBigMessage, "DARTS_TITLE", "", 4000, HUD_COLOUR_BLUELIGHT)
								//SET_SCALEFORM_BIG_MESSAGE_WITH_NUMBER(DartsUI.siBigMessage, "DARTS_SETS", 301, "DARTS_SETD")
								//SET_SCALEFORM_BIG_MESSAGE_WITH_NUMBER_IN_STRAPLINE(DartsUI.siBigMessage, "DARTS_TITLE", 301, "DARTS_CURRENT")
								//SET_SCALEFORM_BIG_MESSAGE_WITH_STRING_IN_STRAPLINE(DartsUI.siBigMessage, "DARTS_TITLE", "DARTS_WOPP", "DARTS_RANK")
								//SET_SCALEFORM_BIG_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(DartsUI.siBigMessage, "DARTS_TITLE", "DARTS_FIRST", GET_PLAYER_NAME(PLAYER_ID()))
								CDEBUG1LN(DEBUG_DARTS, "STAGE_SPLASH, player name is  = ", GET_PLAYER_NAME(PLAYER_ID()))
								
								iSplashStage++
								//ENDIF
							ENDIF
						BREAK
						CASE 2
							
							IF NOT UPDATE_SCALEFORM_BIG_MESSAGE(DartsUI.siBigMessage, TRUE)
								CDEBUG1LN(DEBUG_DARTS, "STAGE_SPLASH, proceeding iSplashStage = ", iSplashStage)
								
								START_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_MENU))
								
								iSplashStage = 0
								CDEBUG1LN(DEBUG_DARTS, "Finished STAGE_TUTORIAL")
								DartGame.Stage = STAGE_TUTORIAL
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE STAGE_TUTORIAL
					
					IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_TUTORIAL_SHOWN) //g_bDartsTutorialShown
						IF DARTS_TUTORIAL(DartsTutorialStage, DartGame.DartsBoard, DartGame.AIBrain, 
										DartGame.TurnStage, DartGame.TutorialDarts, TutorialTimer)//DartGame)
						
							CDEBUG1LN(DEBUG_DARTS, "FINISHED STAGE_TUTORIAL")
							
							SET_BIT(g_iDartsTutorialFlags, gDARTS_TUTORIAL_SHOWN)
							
							DartGame.Stage = STAGE_INIT_MENU
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "TUTORIAL ALREADY SHOWN, SKIPPING")
						//SET_CAM_ACTIVE_WITH_INTERP(camDartGame, camTutorial, 1500)
						SET_CAM_ACTIVE(camDartGame, TRUE)
						
						DartGame.Stage = STAGE_INIT_MENU
					ENDIF
				
				BREAK
				
				CASE STAGE_INIT_MENU
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					SWITCH iSplashStage
						CASE 0
							IF NOT DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
								IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_ALT_START_POS)
									SET_BIT(g_iDartsTutorialFlags, gDARTS_ALT_START_POS)
								ELSE
									CLEAR_BIT(g_iDartsTutorialFlags, gDARTS_ALT_START_POS)
								ENDIF
								
								DARTS_SETUP_START_MENU(DartsUI)//, TRUE)
								
								iSplashStage++
							ELSE
								DARTS_SETUP_START_MENU(DartsUI)//, TRUE)
								iSplashStage++
							ENDIF
						BREAK
						CASE 1
						
							//Check if the aspect ratio has changed during the minigame
							IF IS_PC_VERSION()
								IF fOldAspectRatio <> GET_ASPECT_RATIO(FALSE)
									fOldAspectRatio = GET_ASPECT_RATIO(FALSE)
									SET_CAM_FOV(camDartGame,GET_CAM_FOV_AR_BASED(fOldAspectRatio))
									SET_CAM_FOV(camDartGame2,GET_CAM_FOV_AR_BASED(fOldAspectRatio))
								ENDIF
							ENDIF
							
							IF DARTS_UPDATE_START_MENU(DartsUI, DartGame.iNumDartLegs, DartGame.iNumDartSets)
								
								STOP_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_MENU))
								
								IF DartGame.iNumDartLegs > 0
									CDEBUG1LN(DEBUG_DARTS, "INIT_MENU - legs = ", DartGame.iNumDartLegs)
									CDEBUG1LN(DEBUG_DARTS, "INIT_MENU - sets = ", DartGame.iNumDartSets)
									iSplashStage = 0
									
									IF DartGame.iNumDartLegs > 1
									OR DartGame.iNumDartSets > 1
										UPDATE_SB_SETS_LEGS(DartsSB, 0,0,0,0)
										bUsedSetsLegs = TRUE
									ENDIF
									
									CLEANUP_MENU_ASSETS()
									
									DartGame.Stage = STAGE_STARTING
									
								ELIF DartGame.iNumDartLegs < 0
									DartGame.Stage = STAGE_CLEANUP
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE STAGE_STARTING
					CDEBUG1LN(DEBUG_DARTS, "Got to STAGE_START")
					
					IF DO_START_GAME(DartGame, DartsSB, DartsUI)
						
						CDEBUG1LN(DEBUG_DARTS, "Re - Calculating difficulty level")
						
						DartGame.AIBrain.eDifficulty = DARTSDIFF_HARD
						
						IF g_savedGlobals.sDartsData.iTotalDartGames >= 5
						AND g_savedGlobals.sDartsData.fWinPct >= 0.80
							DartGame.AIBrain.eDifficulty = DARTSDIFF_VERY_HARD
							CDEBUG1LN(DEBUG_DARTS, "setting difficulty to VERY HARD")
						ELIF g_savedGlobals.sDartsData.iTotalDartGames >= 3
						AND g_savedGlobals.sDartsData.fWinPct >= 0.66
							DartGame.AIBrain.eDifficulty = DARTSDIFF_HARD
							CDEBUG1LN(DEBUG_DARTS, "setting difficulty to HARD")
						ELSE
							DartGame.AIBrain.eDifficulty = DARTSDIFF_NORMAL
							CDEBUG1LN(DEBUG_DARTS, "setting difficulty to MEDIUM")
						ENDIF
						
						CDEBUG1LN(DEBUG_DARTS, "Re - Calculating wobbles")
						
						IF SHOULD_DARTS_LEVEL_HELP_SHOW(g_savedGlobals.sDartsData.iTotalDartGames, iLevelBit)
							CDEBUG1LN(DEBUG_DARTS, "show the level up prompt")
							DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOW_LEVEL_UP, TRUE)
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "no level up prompt to show")
						ENDIF
						
						fScaledWobble = GET_WOBBLE_LEVEL(g_savedGlobals.sDartsData.iTotalDartGames)
						fScaledRealWobble = GET_REAL_WOBBLE_LEVEL(g_savedGlobals.sDartsData.iTotalDartGames)
						
						CDEBUG1LN(DEBUG_DARTS, "total darts games  = ", g_savedGlobals.sDartsData.iTotalDartGames)
						CDEBUG1LN(DEBUG_DARTS, "scaled wobble      = ", fScaledWobble)
						CDEBUG1LN(DEBUG_DARTS, "scaled real wobble = ", fScaledRealWobble)
						
						fWobbleFactor = fScaledWobble
						fRealWobFactor = fScaledRealWobble
						
						CDEBUG1LN(DEBUG_DARTS, "Going to STAGE_IN_PROGRESS")
						
						DartsStats[DARTS_SCLB_STAT_NUM_MATCHES]++
						
						IF NOT IS_PED_INJURED(piDartPlayers[1])
							CLEAR_PED_TASKS_IMMEDIATELY(piDartPlayers[1])
							SET_ENTITY_COORDS(piDartPlayers[1], DartsPlayerPositions.vOpponentPos)
							SET_ENTITY_HEADING(piDartPlayers[1], DartGame.DartsBoard.fBoardHeading)
						ENDIF
						
						IF IS_TIMER_STARTED(ThrowTimer)
							CANCEL_TIMER(ThrowTimer)
						ENDIF
						
						START_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_MATCH))
						
						PLAY_BOAST_AUDIO(piDartPlayers[1])
						
						DartGame.Stage = STAGE_IN_PROGRESS
						DartGame.TurnStage = DPS_AIM
					ENDIF
				BREAK
	
				CASE STAGE_IN_PROGRESS
					//Check if the aspect ratio has changed during the minigame
					IF IS_PC_VERSION()
						IF fOldAspectRatio <> GET_ASPECT_RATIO(FALSE)
							fOldAspectRatio = GET_ASPECT_RATIO(FALSE)
							SET_CAM_FOV(camDartGame,GET_CAM_FOV_AR_BASED(fOldAspectRatio))
							SET_CAM_FOV(camDartGame2,GET_CAM_FOV_AR_BASED(fOldAspectRatio))
						ENDIF
					ENDIF
					
					// doing outro streams during gameplay to speed up start time
					IF NOT bOutroRequested
						REQUEST_ANIM_DICT("mini@dartsoutro")
						bOutroRequested = TRUE
					ENDIF
					IF NOT bOutroStreamed
						IF bOutroRequested
							IF NOT HAS_ANIM_DICT_LOADED("mini@dartsoutro")
								CDEBUG1LN(DEBUG_DARTS, "streaming outro anims")
							ELSE
								bOutroStreamed = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					//game loop until a winner is decided
					DO_IN_PROGRESS(DartGame, DartsInitReticlePos, DartsSB, DartsUI, ThrowTimer)//, DartsLocation)
				BREAK
				
				CASE STAGE_GAME_TRANSITION
					
					SWITCH iSplashStage
						CASE 0
							IF GET_TIMER_IN_SECONDS_SAFE(DartGame.miscTimer) > 0.5
								CLEAR_HELP()
								CLEAR_PRINTS()
								
								//ANOTHER_GAME_CAM_MP(PLAYER_PED_ID())
								ANOTHER_GAME_CAM_SETS_LEGS(PLAYER_PED_ID())
								
								UPDATE_SB_SETS_LEGS(DartsSB, DartGame.iDartSetsWon[0], DartGame.iDartSetsWon[1], DartGame.iDartGamesWon[0], DartGame.iDartGamesWon[1])
								
								INT idx, idy
								REPEAT NUM_DART_PLAYERS idx
									REPEAT  NUM_DART_TURNS idy
										RESET_DART(DartGame.Darts[idx][idy])
									ENDREPEAT
									
									DARTS_CLEAR_SCORES(DartsSB, idx)
								ENDREPEAT
								
								//big message here
								IF DartGame.bSetJustWon
									IF DartGame.bDartWinner
										SET_SHARD_BIG_MESSAGE(DartsUI.siMidMessage, "DARTS_WINNER", "DARTS_SETW", DEFAULT, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_YELLOW)
									ELSE
										SET_SHARD_BIG_MESSAGE(DartsUI.siMidMessage, "DARTS_LOSER", "DARTS_SETL", DEFAULT, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_RED)
									ENDIF
									DartGame.bSetJustWon = FALSE
								ELSE
									IF DartGame.bDartWinner
										SET_SHARD_BIG_MESSAGE(DartsUI.siMidMessage, "DARTS_WINNER", "DARTS_LEGW", DEFAULT, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_YELLOW)
									ELSE
										SET_SHARD_BIG_MESSAGE(DartsUI.siMidMessage, "DARTS_LOSER", "DARTS_LEGL", DEFAULT, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_RED)
									ENDIF
								ENDIF
								
								INT iWhichOutro
								
								FLOAT fResolution
								fResolution = GET_ASPECT_RATIO(TRUE)
								CDEBUG1LN(DEBUG_DARTS, " fResolution = ", fResolution)
								
								IF fResolution > 2.0
									CDEBUG1LN(DEBUG_DARTS, " resolution is eyefinity")
									scenePosition = << 1992.29407, 3047.57690, 46.21517 >>
									sceneRotation = << 0.000, 0.000, 138.740 >>
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "resolution is not eyefinity")
									scenePosition = << 1992.33569, 3047.92432, 46.21517 >>
									sceneRotation = << 0.000, 0.000, 136.740 >>
								ENDIF
								
								iWhichOutro = GET_WHICH_DARTS_OUTRO() //GET_RANDOM_INT_IN_RANGE(0, iAnimLimit)
								
								CDEBUG1LN(DEBUG_DARTS, " iWhichOutro = ", iWhichOutro)
								
								iSceneID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
								camDartsSync = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
								PLAY_SYNCHRONIZED_CAM_ANIM(camDartsSync, iSceneID, sOutroCam[iWhichOutro], "mini@dartsoutro")
								SET_CAM_ACTIVE(camDartsSync, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
								IF NOT IS_PED_INJURED(piDartPlayers[0])
								AND NOT IS_PED_INJURED(piDartPlayers[1])
									IF DartGame.bDartWinner
										BOOL bFirstGame
										IF (DartGame.iDartSBGamesWon[0] + DartGame.iDartSBGamesWon[1]) = 1
											bFirstGame = TRUE
										ELSE
											bFirstGame = FALSE
										ENDIF
										
										PLAY_SOUND_FRONTEND (-1, "TENNIS_POINT_WON", "HUD_AWARDS")
										
										PLAY_WIN_AUDIO(piDartPlayers, bFirstGame)
										
										TASK_SYNCHRONIZED_SCENE (piDartPlayers[0], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
										TASK_SYNCHRONIZED_SCENE (piDartPlayers[1], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
										TASK_PLAY_ANIM(PLAYER_PED_ID(), sFacialDictPlayer, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY)
									ELSE
										PLAY_SOUND_FRONTEND (-1, "OTHER_TEXT", "HUD_AWARDS")
										
										PLAY_LOSS_AUDIO(piDartPlayers)
										
										TASK_SYNCHRONIZED_SCENE (piDartPlayers[1], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
										TASK_SYNCHRONIZED_SCENE (piDartPlayers[0], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
										TASK_PLAY_ANIM(piDartPlayers[1], sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY)
									ENDIF
								ENDIF
								iSplashStage++
							ENDIF
						BREAK
						
						CASE 1
							IF NOT UPDATE_SHARD_BIG_MESSAGE(DartsUI.siMidMessage, TRUE)
							
								iSplashStage++
							ENDIF
						BREAK
						
						CASE 2
							CANCEL_TIMER(DartGame.miscTimer)
							bGameOverSpeechPlayed = FALSE
							
							STOP_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_COMPLETE))
							
							DARTS_GAME_CAM()
							
							IF DOES_CAM_EXIST(camDartsSync)
								DESTROY_CAM(camDartsSync)
							ENDIF
							
							iSplashStage = 0
							DartGame.Stage = STAGE_STARTING
						BREAK
						
					ENDSWITCH
				BREAK
				
				CASE STAGE_GAME_END
					
					IF GET_TIMER_IN_SECONDS_SAFE(DartGame.miscTimer) > 0.92 //0.75
						REPLAY_GAME_LOOP(DartGame, DartsUI, DartsSB, DartsEndScreen, bUsedSetsLegs)
					ENDIF
				BREAK

				CASE STAGE_FINISHED //game has ended
					
					//Render the pass screen, Check to see if we can cleanup when done
					bCanGoToCleanup = RENDER_ENDSCREEN(dartsEndScreen)
					
					IF NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())
						CDEBUG1LN(DEBUG_DARTS, "on friend activity, going straight to cleanup")
						CLEAR_PED_TASKS(piDartPlayers[1])
						DartGame.Stage = STAGE_CLEANUP
					ENDIF
					
					SWITCH iSplashStage
						CASE 0
							CLEAR_PED_TASKS(piDartPlayers[0])
							CDEBUG1LN(DEBUG_DARTS, "Got to STAGE_FINISHED, getting ready to up")
							
							IF NOT (bInSyncCam)
								CDEBUG1LN(DEBUG_DARTS, "just killing all cams")
								RENDER_SCRIPT_CAMS(FALSE, bInSyncCam)
							ELSE
								CDEBUG1LN(DEBUG_DARTS, "using catch up cam")
								STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
							ENDIF
							
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							
							CLEAR_PRINTS()
							
							TASK_LOOK_AT_ENTITY(piDartPlayers[0], piDartPlayers[1], 5000, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
							
							RESTART_TIMER_NOW(DialogTimer)
							iSplashStage++
						BREAK
						CASE 1
							IF GET_TIMER_IN_SECONDS(DialogTimer) > 0.1 //1.0
								IF NOT bPlayerQuitEarly
									//PLAY_PED_AMBIENT_SPEECH(piDartPlayers[0], "GAME_GOOD_OTHER")  // add this ambient speech when it's ready
								ENDIF
								
								RESTART_TIMER_NOW(DialogTimer)
								iSplashStage++
							ENDIF
						BREAK
						CASE 2
							IF GET_TIMER_IN_SECONDS(DialogTimer) > 0.3
								
								IF NOT IS_PED_INJURED(piDartPlayers[1])
									IF NOT bPlayerQuitEarly
										PLAY_DARTS_SPEECH(piDartPlayers[0], "GENERIC_THANKS", TRUE, SPEECH_PARAMS_FORCE_FRONTEND)
										//PLAY_DARTS_SPEECH(piDartPlayers[1], "GENERIC_THANKS", FALSE, SPEECH_PARAMS_FORCE_FRONTEND)
									ENDIF
								ENDIF
								
								RESTART_TIMER_NOW(DialogTimer)
								
								iSplashStage++
							ENDIF
						BREAK
						CASE 3
							IF bCanGoToCleanup OR bPlayerQuitEarly
								DartGame.Stage = STAGE_CLEANUP
								iSplashStage++
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE STAGE_PLAYER_QUIT
					DARTS_QUIT_UI(DartsUI)
					
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					//OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						PLAY_EARLY_QUIT_AUDIO(piDartPlayers[1])
						CDEBUG1LN(DEBUG_DARTS, "Player chose to Quit")
						DartsResult = AR_playerQuit
						
						bPlayerQuitEarly = TRUE
						
						IF IS_AUDIO_SCENE_ACTIVE(GET_DARTS_RAVE_SCENE(DARTSRAVE_MATCH))
							STOP_AUDIO_SCENE(GET_DARTS_RAVE_SCENE(DARTSRAVE_MATCH))
						ENDIF
						
						DartGame.Stage = STAGE_FINISHED
					ENDIF
					
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					//OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
						CDEBUG1LN(DEBUG_DARTS, "Player chose to Resume")
						IF DartGame.TurnStage = DPS_AIM
							CDEBUG1LN(DEBUG_DARTS, "Re randomizing reticle")
							bInitReticle = FALSE
						ENDIF
						
						DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SETUP_QUITUI, FALSE)
						DartGame.Stage = STAGE_IN_PROGRESS
					ENDIF
				BREAK
				
				CASE STAGE_CLEANUP
					CDEBUG1LN(DEBUG_DARTS, "Got to STAGE_CLEANUP, getting ready to up")
					CLEANUP_DARTS_GAME(DartGame, DartsUI, DartsEndScreen)
				BREAK
			ENDSWITCH
		ENDIF
	ENDWHILE
ENDSCRIPT

