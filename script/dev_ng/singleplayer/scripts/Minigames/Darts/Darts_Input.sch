////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Input.sch                         //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Darts Input Defs       			      //
//                                                           //
//////////////////////////////////////////////////////////////


USING "minigame_uiinputs.sch"

ENUM DARTS_BUTTON
	DBUTTON_AIM,
	DBUTTON_THROW,
	DBUTTON_TURBO,
	DBUTTON_STEADY,
	DBUTTON_QUIT
ENDENUM

STRUCT  DARTS_INPUT
	FLOAT fLeftStickX
	FLOAT fLeftStickY
	INT iButtons
	INT iLastButtons
ENDSTRUCT

// Accessor for left stick x position.
FUNC FLOAT DARTS_GET_LEFT_STICK_X(DARTS_INPUT& input)
	RETURN input.fLeftStickX
ENDFUNC

// Mutator for left stick x position.
PROC DARTS_SET_LEFT_STICK_X(DARTS_INPUT& input, FLOAT fLeftStickX)
	//input.fLeftStickX = CLAMP(fLeftStickX, -1.0, 1.0)
ENDPROC

// Accessor for left stick y position.
FUNC FLOAT DARTS_GET_LEFT_STICK_Y(DARTS_INPUT& input)
	RETURN input.fLeftStickY
ENDFUNC

// Mutator for left stick y position.
PROC DARTS_SET_LEFT_STICK_Y(DARTS_INPUT& input, FLOAT fLeftStickY)
	//input.fLeftStickY = CLAMP(fLeftStickY, -1.0, 1.0)
ENDPROC

// Mutator for left stick x & y positions.
PROC DARTS_SET_LEFT_STICK_XY(DARTS_INPUT& input, FLOAT fLeftStickX, FLOAT fLeftStickY)
	DARTS_SET_LEFT_STICK_X(input, fLeftStickX)
	DARTS_SET_LEFT_STICK_Y(input, fLeftStickY)
ENDPROC

// Accessor for left stick x & y positions (as 3D vector).
FUNC VECTOR DARTS_GET_LEFT_STICK_AS_VECTOR(DARTS_INPUT& input)
	RETURN <<input.fLeftStickX, input.fLeftStickY, 0.0>>
ENDFUNC

// See if a button is currently down.
FUNC BOOL DARTS_IS_CONTROL_PRESSED(DARTS_INPUT& input, DARTS_BUTTON button)
	RETURN IS_BIT_SET(input.iButtons, ENUM_TO_INT(button))
ENDFUNC

// See if a button was just pressed this frame.
FUNC BOOL DARTS_IS_CONTROL_JUST_PRESSED(DARTS_INPUT& input, DARTS_BUTTON button)
	RETURN NOT IS_BIT_SET(input.iLastButtons, ENUM_TO_INT(button)) AND IS_BIT_SET(input.iButtons, ENUM_TO_INT(button))
ENDFUNC

// See if a button was just released this frame.
FUNC BOOL DARTS_IS_CONTROL_JUST_RELEASED(DARTS_INPUT& input, DARTS_BUTTON button)
	RETURN IS_BIT_SET(input.iLastButtons, ENUM_TO_INT(button)) AND NOT IS_BIT_SET(input.iButtons, ENUM_TO_INT(button))
ENDFUNC

// Mutator for this frame's button input.
PROC DARTS_SET_CONTROL_PRESSED(DARTS_INPUT& input, DARTS_BUTTON button, BOOL bPressed)
	IF bPressed
		SET_BIT(input.iButtons, ENUM_TO_INT(button))
	ELSE
		CLEAR_BIT(input.iButtons, ENUM_TO_INT(button))
	ENDIF
ENDPROC

// Mutator for last frame's button input.
PROC DARTS_SET_CONTROL_PRESSED_LAST_FRAME(DARTS_INPUT& input, DARTS_BUTTON button, BOOL bPressed)
	IF bPressed
		SET_BIT(input.iLastButtons, ENUM_TO_INT(button))
	ELSE
		CLEAR_BIT(input.iLastButtons, ENUM_TO_INT(button))
	ENDIF
ENDPROC

// Gives one frame of no input.
PROC DARTS_SIMULATE_DEAD_INPUT(DARTS_INPUT& input)
	input.fLeftStickX = 0.0
	input.fLeftStickY = 0.0
	input.iLastButtons = input.iButtons
	input.iButtons = 0
ENDPROC


//// Collect input from a local human pad.
//PROC DARTS_GET_LOCAL_INPUT(DARTS_INPUT& input, PAD_NUMBER padNumber)
//	VECTOR vCamRotation, vLeftStick, vRightStick
//	FLOAT fSin, fCos
//	INT iLeftX, iLeftY, iRightX, iRightY
//	
//	// Get analogue stick data.
//	iLeftX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
//	iLeftY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
//	iRightX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
//	iRightY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
//	
//	vLeftStick.x = TO_FLOAT(iLeftX) / 128.0
//	vLeftStick.y = TO_FLOAT(iLeftY) / -128.0
//	vRightStick.x = TO_FLOAT(iRightX) / 128.0
//	vRightStick.y = TO_FLOAT(iRightY) / -128.0
//	
//	// Rotate the inputs into world space (use camera heading).
//	vCamRotation = GET_FINAL_RENDERED_CAM_ROT()
//	fSin = SIN(vCamRotation.z)
//	fCos = COS(vCamRotation.z)
//	vLeftStick  = ROTATE_VECTOR_ABOUT_Z_PRECOMPUTE(vLeftStick,  fSin, fCos)
//	vRightStick = ROTATE_VECTOR_ABOUT_Z_PRECOMPUTE(vRightStick, fSin, fCos)
//	
//	// Store the stick values.
//	DARTS_SET_LEFT_STICK_XY(input, vLeftStick.x, vLeftStick.y)
//	//DARTS_SET_RIGHT_STICK_XY(input, vRightStick.x, vRightStick.y)
//	
//	// Store last frame's button data.
//	//DARTS_STORE_LAST_BUTTONS(input)
//	
//	// Check for steady trigger.
//	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
//		DARTS_SET_BUTTON_PRESSED(input, DBUTTON_STEADY, TRUE)
//	ENDIF
//	
//	// Check for turbo trigger.
//	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
//		DARTS_SET_BUTTON_PRESSED(input, DBUTTON_TURBO, TRUE)
//	ENDIF
//	
//	// Check for shooting button.
//	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//		DARTS_SET_BUTTON_PRESSED(input, DBUTTON_THROW, TRUE)
//	ENDIF	
//ENDPROC
