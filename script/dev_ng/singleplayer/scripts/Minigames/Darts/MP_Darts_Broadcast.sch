////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    MP_Darts_Broadcast.sch                  //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Dart Broadcast Defs				      //
//                                                           //
//////////////////////////////////////////////////////////////


//ENUM DARTS_BROADCAST_EVENT_TYPE
//	DARTSBROADCASTEVENTTYPE_DARTSTHROW_UPDATE = 111
//ENDENUM

//STRUCT DARTS_BROADCAST_EVENT
//	DARTS_BROADCAST_EVENT_TYPE eEventType
//	
//	// Used for the reticle update event
//	VECTOR vDartLandingPos	
//ENDSTRUCT

