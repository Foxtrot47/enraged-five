////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Timer.sch                         //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Timer element for Darts			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "shared_hud_displays.sch"
USING "minigame_uiinputs.sch"
USING "UIUtil.sch"

STRUCT DARTS_TIMER
	structTimer stShotClock
	structTimer stMenuClock
	
	INT  iCountDown
	
	FLOAT fShotClockLength
	FLOAT fMenuClockLength
	FLOAT fThrowClockLength
	
	//BOOL bShotClockStarted
	//BOOL bMenuClockStarted
	BOOL bNewShot
	BOOL bTenSecondMark
	BOOL bTimeRunOut
ENDSTRUCT

PROC DARTS_INIT_TIMERS(DARTS_TIMER & DartsTimer)
	DartsTimer.fShotClockLength = 19.0
	DartsTimer.fMenuClockLength = 20.0
	DartsTimer.fThrowClockLength = 10.0
	
	DartsTimer.bTenSecondMark = FALSE
	DartsTimer.bTimeRunOut = FALSE
	
	IF IS_TIMER_STARTED(DartsTimer.stShotClock)
		CANCEL_TIMER(DartsTimer.stShotClock)
	ENDIF
	
	IF IS_TIMER_STARTED(DartsTimer.stMenuClock)
		CANCEL_TIMER(DartsTimer.stMenuClock)
	ENDIF
	
ENDPROC

PROC DARTS_UPDATE_MENU_CLOCK(DARTS_TIMER & DartsTimer)
	IF NOT IS_TIMER_STARTED(DartsTimer.stMenuClock)
		START_TIMER_NOW(DartsTimer.stMenuClock)
		DartsTimer.bTimeRunOut = FALSE
		DartsTimer.bTenSecondMark = FALSE
	ELSE	
		IF (DartsTimer.fMenuClockLength - GET_TIMER_IN_SECONDS(DartsTimer.stMenuClock)) < 10.5
			DartsTimer.bTenSecondMark = TRUE
		ENDIF
		
		IF GET_TIMER_IN_SECONDS(DartsTimer.stMenuClock) >= DartsTimer.fMenuClockLength
			DartsTimer.bTimeRunOut = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC DARTS_DRAW_MENU_CLOCK(DARTS_TIMER & DartsTimer)
	INT iTimer = ROUND((DartsTimer.fMenuClockLength - GET_TIMER_IN_SECONDS_SAFE(DartsTimer.stMenuClock))* 1000.0)
	
	IF iTimer < 0
	OR DartsTimer.bTimeRunOut
		iTimer = 0
	ENDIF
	
	IF DartsTimer.bTenSecondMark
		DRAW_GENERIC_TIMER(iTimer, "TIM_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_BOTTOM)
	ENDIF
	
ENDPROC

PROC DARTS_UPDATE_SHOT_CLOCK(DARTS_TIMER & DartsTimer)
	IF DartsTimer.bNewShot // OR NOT IS_TIMER_STARTED(DartsTimer.stShotClock) 
		RESTART_TIMER_NOW(DartsTimer.stShotClock)
		DartsTimer.bTimeRunOut = FALSE
		DartsTimer.bTenSecondMark = FALSE
		DartsTimer.bNewShot = FALSE
	ENDIF
	
	IF IS_TIMER_STARTED(DartsTimer.stShotClock)
		IF (DartsTimer.fShotClockLength - GET_TIMER_IN_SECONDS(DartsTimer.stShotClock)) < 10.5
			DartsTimer.bTenSecondMark = TRUE
		ENDIF
		
		IF GET_TIMER_IN_SECONDS(DartsTimer.stShotClock) >= DartsTimer.fShotClockLength
			CANCEL_TIMER(DartsTimer.stShotClock)
			DartsTimer.bTimeRunOut = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC DARTS_DRAW_SHOT_CLOCK(DARTS_TIMER & DartsTimer, BOOL bThrowing)
	INT iTimer = ROUND((DartsTimer.fShotClockLength - GET_TIMER_IN_SECONDS_SAFE(DartsTimer.stShotClock)) * 1000.0)
	
	IF iTimer < 0
	OR DartsTimer.bTimeRunOut
		iTimer = 0
	ENDIF
	
	IF NOT DartsTimer.bNewShot
		IF bThrowing
			IF iTimer < 6000
				DRAW_GENERIC_TIMER(iTimer, "DARTS_SHT_CLCK", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1,
									PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
			ELSE
				DRAW_GENERIC_TIMER(iTimer, "DARTS_SHT_CLCK", 0, TIMER_STYLE_DONTUSEMILLISECONDS)
			ENDIF
		ELSE
			DRAW_GENERIC_TIMER(iTimer, "DARTS_SHT_CLCK") //, 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM)
		ENDIF
	ENDIF
	
ENDPROC

PROC DARTS_UPDATE_THROW_OFF_CLOCK(DARTS_TIMER & DartsTimer)
	IF DartsTimer.bNewShot // OR NOT IS_TIMER_STARTED(DartsTimer.stShotClock) 
		RESTART_TIMER_NOW(DartsTimer.stShotClock)
		DartsTimer.bTimeRunOut = FALSE
		DartsTimer.bTenSecondMark = FALSE
		DartsTimer.bNewShot = FALSE
	ENDIF
	
	IF IS_TIMER_STARTED(DartsTimer.stShotClock)
		IF (DartsTimer.fThrowClockLength - GET_TIMER_IN_SECONDS(DartsTimer.stShotClock)) < 10.5
			DartsTimer.bTenSecondMark = TRUE
		ENDIF
		
		IF GET_TIMER_IN_SECONDS(DartsTimer.stShotClock) >= DartsTimer.fThrowClockLength
			CANCEL_TIMER(DartsTimer.stShotClock)
			DartsTimer.bTimeRunOut = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC DARTS_DRAW_THROW_OFF_CLOCK(DARTS_TIMER & DartsTimer, BOOL bThrowing)
	INT iTimer = ROUND((DartsTimer.fThrowClockLength - GET_TIMER_IN_SECONDS_SAFE(DartsTimer.stShotClock)) * 1000.0)
	
	IF iTimer < 0
	OR DartsTimer.bTimeRunOut
		iTimer = 0
	ENDIF
	
	IF NOT DartsTimer.bNewShot
		IF bThrowing
			IF iTimer < 6000
				DRAW_GENERIC_TIMER(iTimer, "DARTS_SHT_CLCK", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1,
									PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
			ELSE
				DRAW_GENERIC_TIMER(iTimer, "DARTS_SHT_CLCK", 0, TIMER_STYLE_DONTUSEMILLISECONDS)
			ENDIF
		ELSE
			DRAW_GENERIC_TIMER(iTimer, "DARTS_SHT_CLCK")
		ENDIF
	ENDIF
	
ENDPROC
