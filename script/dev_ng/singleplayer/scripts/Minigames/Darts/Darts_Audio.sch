////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Audio.sch                         //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Darts Dialogue and maybe sfx	      //
//                                                           //
//////////////////////////////////////////////////////////////


USING "dialogue_public.sch"
USING "friendactivity_public.sch"

//STRING sDartsBlockText = "DARTSAU"

CONST_INT THIRD_CHANCE 33
CONST_INT HALF_CHANCE 50
CONST_INT TWOTHIRD_CHANCE 67
CONST_INT THREEFOUR_CHANCE 75

STRING sDartPedVoice

/// PURPOSE:Wrapper for darts speech procs
///    
PROC PLAY_DARTS_SPEECH(PED_INDEX piDartPlayer, STRING sSpeech, BOOL bIsPlayer = FALSE, SPEECH_PARAMS eSpeechParams = SPEECH_PARAMS_FORCE_FRONTEND)
	IF NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())
	OR bIsPlayer
		CDEBUG1LN(DEBUG_DARTS, "Playing Darts buddy or player speech: ", sSpeech)
		PLAY_PED_AMBIENT_SPEECH(piDartPlayer, sSpeech, eSpeechParams)
	ELIF NOT IS_PED_INJURED(piDartPlayer)
		CDEBUG1LN(DEBUG_DARTS, "Playing Darts ambient ped speech: ", sSpeech)
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(piDartPlayer, sSpeech, sDartPedVoice, eSpeechParams)
	ENDIF
ENDPROC

PROC SET_DART_OPPONENT_VOICE(MODEL_NAMES eDartOpponent)
	INT iSaltonVoice
	iSaltonVoice = GET_RANDOM_INT_IN_RANGE(0, 2)
	
	SWITCH eDartOpponent
		CASE A_F_M_SALTON_01
			SWITCH iSaltonVoice
				CASE 0		 	sDartPedVoice = "A_F_M_SALTON_01_WHITE_FULL_03"	BREAK
				CASE 1 			sDartPedVoice = "A_F_M_SALTON_01_WHITE_FULL_02"	BREAK
			ENDSWITCH
		BREAK
		CASE A_F_O_SALTON_01	sDartPedVoice = "A_F_M_SALTON_01_WHITE_FULL_01"	BREAK
		CASE A_M_Y_VINEWOOD_01	sDartPedVoice = "G_M_Y_LOST_01_BLACK_FULL_01"	BREAK
		CASE A_M_Y_VINEWOOD_03	sDartPedVoice = "G_M_Y_LOST_02_LATINO_FULL_01"	BREAK
		CASE A_M_Y_VINEWOOD_04	sDartPedVoice = "G_M_Y_LOST_01_BLACK_FULL_02"	BREAK
		CASE A_M_Y_STLAT_01		sDartPedVoice = "G_M_Y_LOST_02_LATINO_FULL_02"	BREAK
		CASE A_M_Y_STWHI_02		sDartPedVoice = "G_M_Y_LOST_01_WHITE_FULL_01"	BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL PLAY_AGREE_AUDIO(PED_INDEX piDartPlayers)
	
	IF NOT IS_ENTITY_DEAD(FRIEND_A_PED_ID())
		PLAY_DARTS_SPEECH(FRIEND_A_PED_ID(), "GENERIC_YES", FALSE, SPEECH_PARAMS_FORCE_NORMAL)
	ELSE
		PLAY_DARTS_SPEECH(piDartPlayers, "GENERIC_AGREE", FALSE, SPEECH_PARAMS_FORCE_NORMAL)
	ENDIF
	RETURN TRUE
	
ENDFUNC


FUNC BOOL PLAY_BULLSEYE_AUDIO(PED_INDEX piDartPlayers)
	INT i
	i = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	//only play a bullseye line 1/3 times
	IF i < 100 //THREEFOUR_CHANCE
		PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_BULLSEYE")
		RETURN TRUE
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "DARTS_BULLSEYE audio NOT played")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PLAY_HURRY_AUDIO(PED_INDEX piDartPlayers)
	INT i
	i = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	//only play the hurry line 3/4 times
	IF i < THIRD_CHANCE
		PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_BORED")
		RETURN TRUE
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "DARTS_BORED audio NOT played")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PLAY_ONE_DART_AWAY_AUDIO(PED_INDEX piDartPlayers)
	
	IF NOT IS_PED_INJURED(piDartPlayers)
		PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_1_DART_AWAY")
		
	ENDIF
	
ENDPROC

FUNC BOOL PLAY_BUST_AUDIO(PED_INDEX piDartPlayers)
//	INT i
//	i = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	//only play a bust line 1/4 times
//	IF i < HALF_CHANCE
		
		BOOL bPlayer
		IF piDartPlayers = PLAYER_PED_ID()
			bPlayer = TRUE
		ENDIF
		
		IF NOT IS_PED_INJURED(piDartPlayers)
			PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_BUST", bPlayer)
			CDEBUG1LN(DEBUG_DARTS, "DARTS_BUST audio played")
		ENDIF
		
		RETURN TRUE
//	ELSE
//		CDEBUG1LN(DEBUG_DARTS, "DARTS_BUST audio NOT played")
//	ENDIF
//	
//	RETURN FALSE
ENDFUNC

FUNC BOOL PLAY_180_AUDIO(PED_INDEX piDartPlayers, BOOL bPlayer)
	INT i
	i = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	//only play a 180 line 4/5 times (180's should not be happening that often)
	IF i < THREEFOUR_CHANCE
		
		IF DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
			IF bPlayer
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "DARTS_180", TRUE)
			ELSE
				PLAY_DARTS_SPEECH(FRIEND_A_PED_ID(), "DARTS_180")
			ENDIF
			
			RETURN TRUE
		ELSE
			// if this isn't a friend match, only say something if it's the player that did it
			// ai opponents say this context in reference to the player
			IF bPlayer
				IF GET_RANDOM_BOOL()
					PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "DARTS_180", TRUE)
				ELSE
					PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_180")
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
		
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "DARTS_180 audio NOT played")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PLAY_140_AUDIO(PED_INDEX piDartPlayers, BOOL bPlayer)
	INT i
	i = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	//only play a 180 line 4/5 times (140's should not be happening that often)
	IF i < THREEFOUR_CHANCE
		
		IF DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
			IF bPlayer
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "DARTS_140", TRUE)
			ELSE
				PLAY_DARTS_SPEECH(FRIEND_A_PED_ID(), "DARTS_140")
			ENDIF
			
			RETURN TRUE
		ELSE
			// if this isn't a friend match, only say something if it's the player that did it
			// ai opponents say this context in reference to the player
			IF bPlayer
				IF GET_RANDOM_BOOL()
					PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "DARTS_140", TRUE)
				ELSE
					PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_140")
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
		
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "DARTS_140 audio NOT played")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PLAY_MISS_AUDIO(PED_INDEX piDartPlayers)
	INT i
	i = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	//only play a miss line 4/5 times (misses are not that common)
	IF i < THREEFOUR_CHANCE
		BOOL bPlayer
		IF piDartPlayers = PLAYER_PED_ID()
			bPlayer = TRUE
		ENDIF
		
		IF NOT IS_PED_INJURED(piDartPlayers)
			PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_MISS_BOARD", bPlayer)
			CDEBUG1LN(DEBUG_DARTS, "DARTS_MISS_BOARD audio played")
		ENDIF
		
		RETURN TRUE
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "DARTS_MISS_BOARD audio NOT played")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PLAY_WIN_AUDIO(PED_INDEX & piDartPlayers[], BOOL bFirstGame)
	
	//"I love me some me."
	
	INT i
	i = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	IF i < 30
		IF NOT IS_PED_INJURED(piDartPlayers[0])
			PLAY_DARTS_SPEECH(piDartPlayers[0], "DARTS_LOSE", TRUE)
			CDEBUG1LN(DEBUG_DARTS, "DARTS_LOSE audio said by player")
		ENDIF
	
	ELIF i < 50
		IF NOT IS_PED_INJURED(piDartPlayers[1])
			PLAY_DARTS_SPEECH(piDartPlayers[1], "DARTS_WIN")
			CDEBUG1LN(DEBUG_DARTS, "DARTS_LOSE audio said by ped")
		ENDIF
	ELIF i < 75 AND bFirstGame
		IF NOT IS_PED_INJURED(piDartPlayers[1])
			PLAY_DARTS_SPEECH(piDartPlayers[1], "DARTS_LOSING_BADLY")
			CDEBUG1LN(DEBUG_DARTS, "DARTS_LOSING_BADLY audio said by ped")
		ENDIF
	ELIF i < 88
		IF NOT IS_PED_INJURED(piDartPlayers[0])
			PLAY_DARTS_SPEECH(piDartPlayers[0], "GAME_WIN_SELF", TRUE)
			CDEBUG1LN(DEBUG_DARTS, "GAME_WIN_SELF audio said by player")
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(piDartPlayers[1])
			PLAY_DARTS_SPEECH(piDartPlayers[1], "GAME_LOSE_SELF")
			CDEBUG1LN(DEBUG_DARTS, "GAME_LOSE_SELF audio said by ped")
		ENDIF
	ENDIF
	
ENDPROC

PROC PLAY_LOSS_AUDIO(PED_INDEX & piDartPlayers[])
	
	//"Every dog has their day."
	
	INT i
	i = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	IF i < 40
		IF NOT IS_PED_INJURED(piDartPlayers[1])
			PLAY_DARTS_SPEECH(piDartPlayers[1], "DARTS_LOSE")
			CDEBUG1LN(DEBUG_DARTS, "DARTS_WIN audio played by ped")
		ENDIF
	
	ELIF i < 70
		IF NOT IS_PED_INJURED(piDartPlayers[0])
			PLAY_DARTS_SPEECH(piDartPlayers[0], "GAME_LOSE_SELF", TRUE)
			CDEBUG1LN(DEBUG_DARTS, "GAME_LOSE_SELF audio played by player")
		ENDIF
	
	ELSE
		IF NOT IS_PED_INJURED(piDartPlayers[1])
			PLAY_DARTS_SPEECH(piDartPlayers[1], "GAME_WIN_SELF")
			CDEBUG1LN(DEBUG_DARTS, "GAME_WIN_SELF audio played by ped")
		ENDIF
	ENDIF
	
ENDPROC

// we'll play this at the start
PROC PLAY_BOAST_AUDIO(PED_INDEX piDartPlayers)
	
	IF NOT IS_PED_INJURED(piDartPlayers)
		PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_BOAST")
		CDEBUG1LN(DEBUG_DARTS, "DARTS_BOAST audio played")
	ENDIF
		
ENDPROC

PROC PLAY_EARLY_QUIT_AUDIO(PED_INDEX piDartPlayers)
	
	IF NOT IS_PED_INJURED(piDartPlayers)
		PLAY_DARTS_SPEECH(piDartPlayers, "GAME_QUIT_EARLY", FALSE, SPEECH_PARAMS_FORCE_NORMAL)
		CDEBUG1LN(DEBUG_DARTS, "GAME_QUIT_EARLY audio played")
	ENDIF
		
ENDPROC

FUNC BOOL PLAY_DARTS_TRASH_TALK(BOOL bIsPlayer, PED_INDEX & piDartPlayers, INT iLastTurnScore, INT iPlayerScore, INT iOpponentScore)
	
	CONST_INT K_PLAYER_PLAYING_WELL		0
	CONST_INT K_OPPONENT_PLAYING_WELL	1
	CONST_INT K_PLAYER_PLAYING_BAD		2
	CONST_INT K_OPPONENT_PLAYING_BAD	3
	
	INT iPlayFlags
	
	// Checking how the player is playing
	IF bIsPlayer
		IF (iLastTurnScore > 100 AND bIsPlayer)
		OR (iOpponentScore - iPlayerScore) > 75
			SET_BIT(iPlayFlags, K_PLAYER_PLAYING_WELL)
			CDEBUG1LN(DEBUG_DARTS, "K_PLAYER_PLAYING_WELL bit set")
		ENDIF
		
		IF (iLastTurnScore < 70 AND bIsPlayer)
		OR (iPlayerScore - iOpponentScore) < 50
			SET_BIT(iPlayFlags, K_PLAYER_PLAYING_BAD)
			CDEBUG1LN(DEBUG_DARTS, "K_PLAYER_PLAYING_BAD bit set")
		ENDIF
	ELSE
		// Checking how the opponent is playing - they're on an easier scale
		IF (iLastTurnScore > 75 AND NOT bIsPlayer)
		OR (iPlayerScore - iOpponentScore) > 50
			SET_BIT(iPlayFlags, K_OPPONENT_PLAYING_WELL)
			CDEBUG1LN(DEBUG_DARTS, "K_PLAYER_PLAYING_WELL bit set")
		ENDIF
		
		IF (iLastTurnScore < 40 AND NOT bIsPlayer)
		OR (iOpponentScore - iPlayerScore) < 30
			SET_BIT(iPlayFlags, K_OPPONENT_PLAYING_BAD)
			CDEBUG1LN(DEBUG_DARTS, "K_OPPONENT_PLAYING_BAD bit set")
		ENDIF
	ENDIF
	
	//only play a trash talk line 1/2 times
	INT i, j
	i = GET_RANDOM_INT_IN_RANGE(0, 100)
	j = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	IF j < 75
		IF IS_BIT_SET(iPlayFlags, K_PLAYER_PLAYING_WELL)
			IF i < 35
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "DARTS_HAPPY", TRUE)
			ELIF i < 70
				PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_PLAYING_WELL")
			ELIF i < 85
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "GAME_GOOD_SELF", TRUE)
			ELSE
				PLAY_DARTS_SPEECH(piDartPlayers, "GAME_GOOD_OTHER")
			ENDIF
			RETURN TRUE
		ELIF IS_BIT_SET(iPlayFlags, K_PLAYER_PLAYING_BAD)
			IF i < 50
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "GAME_BAD_SELF", TRUE)
			ELIF i < 68 AND NOT DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
				PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_PLAYING_POORLY")
			ELIF i < 86
				PLAY_DARTS_SPEECH(piDartPlayers, "GAME_BAD_OTHER")
			ELIF i < 100
				PLAY_DARTS_SPEECH(piDartPlayers, "GAME_HECKLE")
			ENDIF
			RETURN TRUE
		ELIF IS_BIT_SET(iPlayFlags, K_OPPONENT_PLAYING_WELL)
			IF i < 40
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "DARTS_PLAYING_WELL", TRUE)
			ELIF i < 60
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "GAME_GOOD_OTHER", TRUE)
			ELIF i < 80
				PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_HAPPY")
			ELIF i < 100
				PLAY_DARTS_SPEECH(piDartPlayers, "GAME_GOOD_SELF")
			ENDIF
			RETURN TRUE
		ELIF IS_BIT_SET(iPlayFlags, K_OPPONENT_PLAYING_BAD)
			IF i < 33
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "GAME_BAD_OTHER", TRUE)
			ELIF i < 66
				PLAY_DARTS_SPEECH(piDartPlayers, "GAME_BAD_SELF")
			ELIF i < 100
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "GAME_HECKLE", TRUE)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PLAY_DARTS_TRASH_TALK_CLUTCH(BOOL bIsPlayer, PED_INDEX & piDartPlayers, INT iLastTurnScore, INT iPlayerScore, INT iOpponentScore)
	
	CONST_INT K_PLAYER_PLAYING_WELL		0
	CONST_INT K_OPPONENT_PLAYING_WELL	1
	CONST_INT K_PLAYER_PLAYING_BAD		2
	CONST_INT K_OPPONENT_PLAYING_BAD	3
	
	INT iPlayFlags
	
	// Checking how the player is playing
	IF bIsPlayer
		IF (iLastTurnScore > 30 AND bIsPlayer)
		OR iPlayerScore < 20
			SET_BIT(iPlayFlags, K_PLAYER_PLAYING_WELL)
			CDEBUG1LN(DEBUG_DARTS, "K_PLAYER_PLAYING_WELL bit set")
		ENDIF
		
		IF (iLastTurnScore <= 30 AND bIsPlayer)
		OR iPlayerScore >= 20
			SET_BIT(iPlayFlags, K_PLAYER_PLAYING_BAD)
			CDEBUG1LN(DEBUG_DARTS, "K_PLAYER_PLAYING_BAD bit set")
		ENDIF
	ELSE
		// Checking how the opponent is playing - they're on an easier scale
		IF (iLastTurnScore > 20 AND NOT bIsPlayer)
		OR (iOpponentScore) < 30
			SET_BIT(iPlayFlags, K_OPPONENT_PLAYING_WELL)
			CDEBUG1LN(DEBUG_DARTS, "K_PLAYER_PLAYING_WELL bit set")
		ENDIF
		
		IF (iLastTurnScore <= 20 AND NOT bIsPlayer)
		OR (iOpponentScore) >= 30
			SET_BIT(iPlayFlags, K_OPPONENT_PLAYING_BAD)
			CDEBUG1LN(DEBUG_DARTS, "K_OPPONENT_PLAYING_BAD bit set")
		ENDIF
	ENDIF
	
	//only play a trash talk clutch line 2/3 times
	INT i, j
	i = GET_RANDOM_INT_IN_RANGE(0, 100)
	j = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	IF j < 85
		IF IS_BIT_SET(iPlayFlags, K_PLAYER_PLAYING_WELL)
			IF i < 50
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "GAME_GOOD_SELF", TRUE)
			ELIF i < 75
				PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_PLAYING_WELL")
			ELIF i < 100
				PLAY_DARTS_SPEECH(piDartPlayers, "GAME_GOOD_OTHER")
			ENDIF
			RETURN TRUE
		ELIF IS_BIT_SET(iPlayFlags, K_PLAYER_PLAYING_BAD)
			IF i < 50
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "GAME_BAD_SELF", TRUE)
			ELIF i < 68
				PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_PLAYING_POORLY")
			ELIF i < 86
				PLAY_DARTS_SPEECH(piDartPlayers, "GAME_BAD_OTHER")
			ELIF i < 100
				PLAY_DARTS_SPEECH(piDartPlayers, "GAME_HECKLE")
			ENDIF
			RETURN TRUE
		ELIF IS_BIT_SET(iPlayFlags, K_OPPONENT_PLAYING_WELL)
			IF i < 50
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "GAME_GOOD_OTHER", TRUE)
			ELIF i < 75
				PLAY_DARTS_SPEECH(piDartPlayers, "DARTS_HAPPY")
			ELIF i < 100
				PLAY_DARTS_SPEECH(piDartPlayers, "GAME_GOOD_SELF")
			ENDIF
			RETURN TRUE
		ELIF IS_BIT_SET(iPlayFlags, K_OPPONENT_PLAYING_BAD)
			IF i < 50
				PLAY_DARTS_SPEECH(PLAYER_PED_ID(), "GAME_BAD_OTHER", TRUE)
			ELIF i < 100
				PLAY_DARTS_SPEECH(piDartPlayers, "GAME_BAD_SELF")
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
