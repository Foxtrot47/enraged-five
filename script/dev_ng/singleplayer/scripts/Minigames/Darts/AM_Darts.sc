////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    MP_Darts.sc                             //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Multiplayer Darts      			      //
//                                                           //
//////////////////////////////////////////////////////////////

// GUIDE
// search for these and add bookmarks to make your life easier
// 
// #SPECTATE
// #NORMAL
// #THROWOFF
// #REALGAME
//

USING "net_mission.sch"
USING "net_hud_activating.sch"
USING "fmmc_restart_header.sch"
USING "net_celebration_screen.sch"

USING "Darts.sch"
USING "Darts_Core.sch"
USING "Darts_UI_lib.sch"

USING "MP_Darts.sch"
USING "MP_Darts_Client_lib.sch"
USING "MP_Darts_Server_lib.sch"
USING "MP_Darts_Broadcast_lib.sch"
USING "Darts_Leaderboard_lib.sch"
USING "fmmc_mp_setup_mission.sch"
USING "achievement_public.sch"

CONST_INT BASE_BULLSEYE_XP 	10 		// award for succesful bullseyes
CONST_INT BASE_LEG_WIN_XP 	50		// award for each leg win
CONST_INT BASE_MATCH_WIN_XP 250		// award for a total match win
CONST_INT BASE_DART_PLAY_XP 100		// award for participating

INT iSelfID, iOtherID
//INT iAllowSkipCutsceneTime
BOOL bSoloMP
BOOL bPlayerQuit

STRING sFacialDictPed
STRING sFacialClip[3]
STRING sFacialDictPed2
STRING sFacialClip2[3]

INT iNumberOfPlayers

CELEBRATION_SCREEN_DATA sCelebrationData
CAMERA_INDEX camEndScreen
CAMERA_INDEX renderingCam
CAMERA_INDEX ciDartsQuit
CAMERA_INDEX camDartsSync

SCRIPT_SHARD_BIG_MESSAGE shardBigMessage
SCRIPT_SHARD_BIG_MESSAGE shardMidSizeMessage



ENUM DART_PLAYER_POSITION
	DART_PLAYER_IN_POSITION,
	DART_PLAYER_OUT_POSITION
ENDENUM

#IF IS_DEBUG_BUILD
BOOL bDartsNoSocialClub
#ENDIF

PROC PRINT_WITH_NAME_AND_TWO_NUMBERS(STRING pTextLabel, STRING PlayerName, INT FirstNumberToInsert, INT SecondNumberToInsert, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
//		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
//		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
		ADD_TEXT_COMPONENT_INTEGER(FirstNumberToInsert)
		ADD_TEXT_COMPONENT_INTEGER(SecondNumberToInsert)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

PROC PRINT_WITH_NAME(STRING pTextLabel, STRING PlayerName, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
//		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
//		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

PROC PRINT_WITH_NAME_AND_NUMBER(STRING pTextLabel, STRING PlayerName, INT NumberToInsert, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
//		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
//		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
		ADD_TEXT_COMPONENT_INTEGER(NumberToInsert)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

PROC PRINT_WITH_TWO_NAMES_AND_NUMBER(STRING pTextLabel, STRING FirstPlayerName, STRING SecondPlayerName, INT NumberToInsert, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
//		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
//		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(FirstPlayerName)
		ADD_TEXT_COMPONENT_INTEGER(NumberToInsert)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(SecondPlayerName)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

PROC PRINT_WITH_TWO_NAMES_AND_TWO_NUMBERS(STRING pTextLabel, STRING FirstPlayerName, STRING SecondPlayerName, INT FirstNumberToInsert, INT SecondNumberToInsert, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
//		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
//		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(FirstPlayerName)
		ADD_TEXT_COMPONENT_INTEGER(FirstNumberToInsert)
		ADD_TEXT_COMPONENT_INTEGER(SecondNumberToInsert)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(SecondPlayerName)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

/// PURPOSE:
///    how many participants are actually players?  rather then spectators or sctv
/// PARAMS:
///    bPrintDebugInfo - TRUE to show info about who's who, should not be set if this is getting called every frame
/// RETURNS:
///    
FUNC INT NETWORK_GET_NUM_PLAYERS(DARTS_PLAYER_BROADCAST_DATA & DartsPlayerData[DARTSMP_IDS], BOOL bPrintDebugInfo = FALSE) 
	INT iNumPlayers
	INT idx, iNumParticipants
	INT partID
	
	PARTICIPANT_INDEX participantID
	
	iNumParticipants = ENUM_TO_INT(DARTSMP_IDS)
	
	IF bPrintDebugInfo
	CDEBUG1LN(DEBUG_DARTS, "checking players, current participants = ", iNumParticipants)
	ENDIF
	
	// can't run a repeat method if there's only one participant, because the first participant gets id 1
	// this would work if it started at 0
	
	REPEAT iNumParticipants idx
		participantID = INT_TO_PARTICIPANTINDEX( idx )
		IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
			IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
			OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(participantID))
				IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
					IF bPrintDebugInfo
						partID = DartsPlayerData[idx].iParticipantID
						CDEBUG1LN(DEBUG_DARTS, "this participant is sctv: ", DartsPlayerData[idx].iParticipantID)
						IF partID = idx
							CDEBUG1LN(DEBUG_DARTS, "partID = idx")
						ENDIF
					ENDIF
				ENDIF
				IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(participantID))
					IF bPrintDebugInfo
						CDEBUG1LN(DEBUG_DARTS, "this participant is spectator: ", DartsPlayerData[idx].iParticipantID)
					ENDIF
				ENDIF
			ELSE
				iNumPlayers++
				IF bPrintDebugInfo
					CDEBUG1LN(DEBUG_DARTS, "this participant is a player: ", DartsPlayerData[idx].iParticipantID)
					CDEBUG1LN(DEBUG_DARTS, "number of players: ", iNumPlayers)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iNumPlayers
ENDFUNC

/// PURPOSE:
///    how many participants are spectators?
/// PARAMS:
///    bPrintDebugInfo - TRUE to show info about who's who, should not be set if this is getting called every frame
/// RETURNS:
///    
FUNC INT NETWORK_GET_NUM_SPECTATORS(DARTS_PLAYER_BROADCAST_DATA & DartsPlayerData[DARTSMP_IDS], BOOL bPrintDebugInfo = FALSE) 
	INT iNumPlayers
	INT idx, iNumParticipants
	
	iNumParticipants = ENUM_TO_INT(DARTSMP_IDS)
	
	IF bPrintDebugInfo
		CDEBUG1LN(DEBUG_DARTS, "checking spectators, current participants = ", iNumParticipants)
	ENDIF
	
	REPEAT iNumParticipants idx
		IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_PARTICIPANT_SET)
			IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(DartsPlayerData[idx].iParticipantID)))
			OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(DartsPlayerData[idx].iParticipantID)))
				IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(DartsPlayerData[idx].iParticipantID)))
					IF bPrintDebugInfo
						CDEBUG1LN(DEBUG_DARTS, "this participant is spectator: ", DartsPlayerData[idx].iParticipantID)
					ENDIF
					iNumPlayers++
				ENDIF
				IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(DartsPlayerData[idx].iParticipantID)))
					IF bPrintDebugInfo
						CDEBUG1LN(DEBUG_DARTS, "this participant is sctv: ", DartsPlayerData[idx].iParticipantID)
					ENDIF
					iNumPlayers--
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iNumPlayers
ENDFUNC

/// PURPOSE:
///    how many participants are SCTV? not as important as players or spectators, but putting in place anyway
/// PARAMS:
///    bPrintDebugInfo - TRUE to show info about who's who, should not be set if this is getting called every frame
/// RETURNS:
///    
FUNC INT NETWORK_GET_NUM_SCTV(DARTS_PLAYER_BROADCAST_DATA & DartsPlayerData[DARTSMP_IDS], BOOL bPrintDebugInfo = FALSE) 
	INT iNumPlayers
	INT idx, iNumParticipants
	
	iNumParticipants = ENUM_TO_INT(DARTSMP_IDS)
	
	IF bPrintDebugInfo
		CDEBUG1LN(DEBUG_DARTS, "checking SCTV, current participants = ", iNumParticipants)
	ENDIF
	
	REPEAT iNumParticipants idx
		IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_PARTICIPANT_SET)
			IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(idx)))
				IF bPrintDebugInfo
					CDEBUG1LN(DEBUG_DARTS, "this participant is sctv: ", idx)
				ENDIF
				iNumPlayers++
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iNumPlayers
ENDFUNC


FUNC INT DART_GET_OTHER_PLAYERS_ID(DARTS_PLAYER_BROADCAST_DATA & DartsPlayerData[DARTSMP_IDS], INT & returnID)
	INT myPartID
	INT idx, iNumParticipants, iMaxParticipants
	
	PARTICIPANT_INDEX participantID
	
	myPartID = PARTICIPANT_ID_TO_INT()
	
	iMaxParticipants = ENUM_TO_INT(DARTSMP_IDS)
	
	REPEAT iMaxParticipants idx
		participantID = INT_TO_PARTICIPANTINDEX( idx )
		IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
		AND DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_PARTICIPANT_SET)
			IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(participantID))
				IF myPartID <> DartsPlayerData[idx].iParticipantID
				
					returnID = DartsPlayerData[idx].iParticipantID
					CDEBUG1LN(DEBUG_DARTS, "the other participant is: ", returnID)
				ENDIF
				
				iNumParticipants++
			ENDIF
		ENDIF
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_DARTS, "num players seen: ", iNumParticipants)
	RETURN iNumParticipants
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    DartsPlayerData - 
///    iReturnPlayers - 
/// RETURNS:
///    
FUNC BOOL GET_BOTH_PLAYERS_ID(DARTS_PLAYER_BROADCAST_DATA & DartsPlayerData[DARTSMP_IDS], INT & iReturnPlayers[DART_PLAYER_IDS])
	
	INT idx, iMaxParticipants, iNumParticipants
	
	PARTICIPANT_INDEX participantID
	
	iMaxParticipants = ENUM_TO_INT(DARTSMP_IDS)
	
	REPEAT iMaxParticipants idx
		participantID = INT_TO_PARTICIPANTINDEX( idx )
		IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
		AND DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_PARTICIPANT_SET)
			IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(participantID))
				
				iReturnPlayers[DartsPlayerData[idx].iOffsetPlayerID] = DartsPlayerData[idx].iParticipantID
				iNumParticipants++
				
				CDEBUG1LN(DEBUG_DARTS, "found a participant")
				CDEBUG1LN(DEBUG_DARTS, "which one : ", DartsPlayerData[idx].iOffsetPlayerID)
				CDEBUG1LN(DEBUG_DARTS, "what ID: ", DartsPlayerData[idx].iParticipantID)
				CDEBUG1LN(DEBUG_DARTS, "num participants: ", iNumParticipants)
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iNumParticipants = 2
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC CLEANUP_MP_DARTS_GAME(MP_DARTS_GAME & MPDartGame, DARTS_UI & DartsUI, BOOL bRestoreUI = TRUE, BOOL bLeaveArea = FALSE, BOOL bQuitter = FALSE)
	
	CDEBUG1LN(DEBUG_DARTS, "Starting Darts cleanup procedure")
	
	IF NOT bPlayerQuit
		SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
		CDEBUG1LN(DEBUG_DARTS, "dart mission ended at leaderboard or timed out ")
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
		//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		TASK_SWAP_WEAPON(PLAYER_PED_ID(), TRUE)
		IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
		AND NOT IS_PLAYER_SCTV(PLAYER_ID()) // B*2339942
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		ENDIF
	ENDIF
	
	DISPLAY_AREA_NAME(TRUE)
	
	IF bRestoreUI
	AND NETWORK_IS_SIGNED_ONLINE()
		CDEBUG1LN(DEBUG_DARTS, "Restoring UI in Darts cleanup")
		
		DISABLE_CELLPHONE(FALSE)
//		SET_MP_OVERHEAD_STATS_ACTIVE(TRUE)
//		DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, FALSE)
//		ENABLE_RANK_AND_XP_BAR()
//		DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
		ENABLE_ALL_MP_HUD()
		
		DISPLAY_RADAR(TRUE)
		
		PLAYER_INDEX broadcastIndex
		broadcastIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(PARTICIPANT_ID_TO_INT()))
		CDEBUG1LN(DEBUG_DARTS, "Broadcasting darts clear to all players on team ", GET_PLAYER_TEAM(broadcastIndex))
		BROADCAST_DARTS_CLEAR(broadcastIndex, GET_PLAYER_TEAM(broadcastIndex))
	ENDIF
	
	IF bQuitter
		CLEAR_PRINTS()
	ENDIF
	
	// cleanup big splash text
	//CLEANUP_BIG_MESSAGE()
	CLEAR_ALL_BIG_MESSAGES()
	
	SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_DARTS)
						
	// cleanup dem darts here
	INT iTemp1
	REPEAT NUM_DART_TURNS iTemp1
		CLEANUP_DART(MPDartGame.Darts[iTemp1])
	ENDREPEAT
	
	CLEANUP_BOARD(MPDartGame.dBoard)
	CLEANUP_RETICLE()
	
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(DartsUI.dartsLB_control)
	
	IF NOT bLeaveArea
		CLEANUP_HUD()
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(modelDart1)
//	SET_MODEL_AS_NO_LONGER_NEEDED(modelDart2)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelReticleRegular)
	
	IF (MPDartGame.iGamesWon = MPDartGame.iGamesLost)
	AND NOT bQuitter
		iTemp1 = ciFMMC_END_OF_MISSION_STATUS_CANCELLED
	ELIF (MPDartGame.iGamesWon > MPDartGame.iGamesLost)
		iTemp1 = ciFMMC_END_OF_MISSION_STATUS_PASSED
	ELSE
		iTemp1 = ciFMMC_END_OF_MISSION_STATUS_FAILED
	ENDIF
	
	//Clean up freemode variables
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		RESET_FMMC_MISSION_VARIABLES(TRUE, FALSE, iTemp1)
	ENDIF
	
	IF NETWORK_IS_SIGNED_ONLINE()
	AND NOT bSoloMP
		#IF IS_DEBUG_BUILD
		IF NOT bDartsNoSocialClub
		#ENDIF
		WRITE_DARTS_SCLB_DATA(LEADERBOARD_MINI_GAMES_DARTS,TRUE)
		#IF IS_DEBUG_BUILD
		ELSE
		CDEBUG1LN(DEBUG_DARTS, "Debug used, not uploading stats to social club")
		ENDIF
		#ENDIF
	ELIF bSoloMP
		CDEBUG1LN(DEBUG_DARTS, "soloMP, no leaderboard write")
	ENDIF
	
	IF MPGlobals.DartsData.bResultsDisplayed
		MPGlobals.DartsData.bResultsDisplayed = FALSE
	ENDIF
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
	
	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
	CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
	
	CDEBUG1LN(DEBUG_DARTS, "All Cleaned Up")
	
//	IF IS_SCREEN_FADED_OUT()
//		DO_SCREEN_FADE_IN(2000)
//	ENDIF

	CLEANUP_DARTS_PC_CONTROLS()
	
	//gdisablerankupmessage = FALSE
	SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	
	SET_ON_JOB_INTRO(FALSE)
	
	IF DOES_CAM_EXIST(renderingCam)
		IF IS_CAM_ACTIVE(renderingCam)
			SET_CAM_ACTIVE(renderingCam, FALSE)
		ENDIF
		DESTROY_CAM(renderingCam)
	ENDIF
	
	IF DOES_CAM_EXIST(ciDartsQuit)
		IF IS_CAM_ACTIVE(ciDartsQuit)
			SET_CAM_ACTIVE(ciDartsQuit, FALSE)
		ENDIF
		DESTROY_CAM(ciDartsQuit)
	ENDIF
	
	IF DOES_CAM_EXIST(camDartsSync)
		IF IS_CAM_ACTIVE(camDartsSync)
			SET_CAM_ACTIVE(camDartsSync, FALSE)
		ENDIF
		DESTROY_CAM(camDartsSync)
	ENDIF
	
	IF DOES_CAM_EXIST(camEndScreen)
		IF IS_CAM_ACTIVE(camEndScreen)
			SET_CAM_ACTIVE(camEndScreen, FALSE)
		ENDIF
		DESTROY_CAM(camEndScreen)
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()	
ENDPROC

/// PURPOSE:
///    Grab the necessary assets for Darts
PROC REQUEST_STREAMS(DARTS_SCOREBOARD & DartsSB)
	REQUEST_MODEL(modelDart1)
//	REQUEST_MODEL(modelDart2)
	REQUEST_MODEL(modelReticleRegular)	
		
	REQUEST_STREAMED_TEXTURE_DICT("Darts")
	
	REQUEST_ANIM_DICT("mini@darts")
	REQUEST_ANIM_DICT(sFacialDictPed)
	REQUEST_ANIM_DICT(sFacialDictPed2)
	REQUEST_ANIM_DICT("mini@dartsintro_alt1")
	REQUEST_ANIM_DICT("mini@dartsoutro")
	
	DartsSB.siScoreBoard			= REQUEST_SCALEFORM_MOVIE("darts_scoreboard")
	shardBigMessage.siMovie			= REQUEST_MG_BIG_MESSAGE()
	shardMidSizeMessage.siMovie		= REQUEST_MG_MIDSIZED_MESSAGE()
	
	
//	REQUEST_CELEBRATION_SCREEN(sCelebrationData)
	
	REQUEST_ADDITIONAL_TEXT("DARTS", MINIGAME_TEXT_SLOT)
ENDPROC

/// PURPOSE:
///    Asset loading checker
/// RETURNS:
///    TRUE if all the needed assets have loaded
FUNC BOOL HAVE_STREAMS_LOADED(DARTS_SCOREBOARD & DartsSB)
//	IF NOT LOAD_MENU_ASSETS()
//		CDEBUG1LN(DEBUG_DARTS, "Menu assets loading")
//		RETURN FALSE
//	ENDIF
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\DARTS")	
		CDEBUG1LN(DEBUG_DARTS, "Darts Audio loading")
		RETURN FALSE
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FAMILY1_2")	
		CDEBUG1LN(DEBUG_DARTS, "Family Audio loading")
		RETURN FALSE
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("HUD_AWARDS")	
		CDEBUG1LN(DEBUG_DARTS, "HUD_AWARDS Audio loading")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(modelDart1)
		CDEBUG1LN(DEBUG_DARTS, "modelDart1 loading")
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(modelReticleRegular)
		CDEBUG1LN(DEBUG_DARTS, "modelReticleRegular loading")
		RETURN FALSE
	ENDIF
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(DartsSB.siScoreBoard)
		CDEBUG1LN(DEBUG_DARTS, "DartsSB.siScoreBoard loading")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
		CDEBUG1LN(DEBUG_DARTS, "MINIGAME_TEXT_SLOT loading")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("mini@darts")
		CDEBUG1LN(DEBUG_DARTS, "mini@darts loading")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED(sFacialDictPed)
		CDEBUG1LN(DEBUG_DARTS, "sFacialDictPed loading")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED(sFacialDictPed2)
		CDEBUG1LN(DEBUG_DARTS, "sFacialDictPed2 loading")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("mini@dartsintro_alt1")
		CDEBUG1LN(DEBUG_DARTS, "mini@dartsintro_alt1 loading")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("mini@dartsoutro")
		CDEBUG1LN(DEBUG_DARTS, "mini@dartsoutro loading")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("Darts")
		CDEBUG1LN(DEBUG_DARTS, "Darts streamed texture dict loading")
		RETURN FALSE
	ENDIF
	
//	IF NOT HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
//		CDEBUG1LN(DEBUG_DARTS, "Celebration data loading")
//		RETURN FALSE
//	ENDIF
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Do necessary pre game start ini.
/// RETURNS:
///    FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL DARTS_NETWORK_INIT(MP_MISSION_DATA & missionScriptArgs, DARTS_SERVER_BROADCAST_DATA & DartsServerData, DARTS_PLAYER_BROADCAST_DATA & DartsPlayerData[DARTSMP_IDS])
	
	CLEAR_HELP()
	
	// This marks the script as a net script, and handles any instancing setup. 
	IF missionScriptArgs.mdID.idMission <> eAM_DARTS
		CDEBUG1LN(DEBUG_DARTS, "Changing missionScriptArgs.mdID.idMission to eAM_DARTS...")
		missionScriptArgs.mdID.idMission = eAM_DARTS
	ENDIF
	
	//If we are in freemode launch diffrently
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		
		CDEBUG1LN(DEBUG_DARTS, "Set this script as a network script with 2 players.  FM VERSION")
		
		//Freemode mission start details
		INT iPlayerMissionToLoad = FMMC_MINI_GAME_CREATOR_ID
		INT iMissionVariation 	 = 0
		INT iNumberPlayers 	 = ENUM_TO_INT(DARTSMP_IDS)//2
		//Call the function that controls the setting up of this script
		FMMC_PROCESS_PRE_GAME_COMMON(missionScriptArgs, iPlayerMissionToLoad, iMissionVariation, iNumberPlayers)
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_DARTS
		
		//Script has started set the global broadcast data up so every body knows
		SET_FM_MISSION_LAUNCHED_SUCESS(missionScriptArgs.mdID.idCreator, missionScriptArgs.mdID.idVariation, missionScriptArgs.iInstanceId)
	
	//If we are not in freemode deal with it normally
	ELSE
	
		CDEBUG1LN(DEBUG_DARTS, "Set this script as a network script with 2 players.  CNC VERSION")
		SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
		//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(2, FALSE)
		
		// This makes sure the net script is active, waits untull it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		RESERVE_NETWORK_MISSION_OBJECTS(5)//8) // TODO: just the dart board?
		
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
	ENDIF
	
	//These are common to all modes
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(DartsServerData, SIZE_OF(DartsServerData))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(DartsPlayerData, SIZE_OF(DartsPlayerData))

	iSelfID = PARTICIPANT_ID_TO_INT()
	
	CDEBUG1LN(DEBUG_DARTS, "*********ID Check********************")
	CDEBUG1LN(DEBUG_DARTS, "* iSelfID  = ", iSelfID)
	CDEBUG1LN(DEBUG_DARTS, "*************************************")
	
	IF DID_I_JOIN_MISSION_AS_SPECTATOR()
	OR IS_PLAYER_SCTV(PLAYER_ID())
		
		iSelfID = PARTICIPANT_ID_TO_INT()
		CDEBUG1LN(DEBUG_DARTS, "*********ID Check********************")
		CDEBUG1LN(DEBUG_DARTS, "* iSelfID  = ", iSelfID)
		CDEBUG1LN(DEBUG_DARTS, "*************************************")
		
		
		IF DID_I_JOIN_MISSION_AS_SPECTATOR()
			CDEBUG1LN(DEBUG_DARTS, "FTR THIS IS STARTING AS SPECTATOR")
			DARTS_SET_CLIENT_FLAG(DartsPlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
		ENDIF
		
		IF IS_PLAYER_SCTV(PLAYER_ID())
			CDEBUG1LN(DEBUG_DARTS, "FTR THIS IS STARTING AS SCTV")
			DARTS_SET_CLIENT_FLAG(DartsPlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SCTV)
			DARTS_CLEAR_CLIENT_FLAG(DartsPlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
		ENDIF
		
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "FTR THIS IS NOT SPECTATING")
	ENDIF
	
	DARTS_SET_PLAYER_MP_STATE(DartsPlayerData[iSelfID], DARTS_MPSTATE_WAIT_PRE_INIT)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		DARTS_SET_SERVER_MP_STATE(DartsServerData, DARTS_MPSTATE_INIT)
		//DartsServerData.iMatchHistoryID = PLAYSTATS_CREATE_MATCH_HISTORY_ID()
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(DartsServerData.iMatchType, DartsServerData.iMatchHistoryID)
	ENDIF
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DARTS_LEVEL_HELP_SHOW(INT iNumGames, INT & iBitToSet)
	IF iNumGames >= 10
		IF NOT IS_BIT_SET(g_savedGlobals.sDartsData.iDartTimePlayed, gDARTS_MP_LVL_UP_SHWN)
			iBitToSet = gDARTS_MP_LVL_UP_SHWN
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//  take in a vector from the other player, and your own darts
// Draw the sprite reticle at that point
// Sprite reticle has to be different color, so make sure a function is ready for that
PROC DARTS_DRAW_PLAYERS_RETICLE(VECTOR vReticlePos, INT iDartPlayerID, BOOL bWatching = FALSE)
	spriteReticle.x = vReticlePos.x
	spriteReticle.y = vReticlePos.y
	IF iDartPlayerID = 0
		// color - blue
		spriteReticle.r = 255
		spriteReticle.g = 255
		spriteReticle.b = 255
	ELIF iDartPlayerID = 1
		// color - red
		spriteReticle.r = 255
		spriteReticle.g = 255
		spriteReticle.b = 255
	ELSE
		// color - white
		spriteReticle.r = 255
		spriteReticle.g = 250
		spriteReticle.b = 255
	ENDIF
	
	IF bWatching
		//DRAW_2D_SPRITE("MPCharCard", "MP_CharCard_Stats_Icons5", spriteReticle)
	ENDIF
ENDPROC

PROC COPY_DART_TO_LOCAL(DART & localDart, DARTS_PLAYER_BROADCAST_DATA & thatPlayerData, INT iOtherPlayerID = 0)
	localDart.vPosition 	= thatPlayerData.ClientDart.vPosition
	localDart.vThrow 		= thatPlayerData.ClientDart.vThrow
	localDart.vTarget 		= thatPlayerData.ClientDart.vTarget	
	localDart.vRotation 	= thatPlayerData.ClientDart.vRotation
	localDart.vOffsetTarget = thatPlayerData.ClientDart.vOffsetTarget
	localDart.bStuck 		= thatPlayerData.ClientDart.bStuck
	localDart.bTravelling 	= thatPlayerData.ClientDart.bTravelling
	
	MODEL_NAMES otherDart
	
	otherDart = modelDart1
	
	iOtherPlayerID = iOtherPlayerID
//	
//	IF iOtherPlayerID = 0
//		otherDart = modelDart1
//		CDEBUG1LN(DEBUG_DARTS, "**********PROP_DART_1 registered**********")
//	ELSE
//		otherDart = modelDart2
//		CDEBUG1LN(DEBUG_DARTS, "**********PROP_DART_2 registered**********")
//	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "**********localDart.vTarget = ", localDart.vTarget)
	CDEBUG1LN(DEBUG_DARTS, "**********localDart.vOffsetTarget = ", localDart.vOffsetTarget)
	CDEBUG1LN(DEBUG_DARTS, "**********localDart.vThrow = ", localDart.vThrow)
	
	localDart.object = CREATE_OBJECT(otherDart, vInitDartPos, FALSE, FALSE)
ENDPROC

FUNC BOOL DARTS_PLAYER_QUIT_CONTROL(DARTS_PLAYER_BROADCAST_DATA & playerData, DARTS_UI & DartsUI, INT & iDebugThrottle)
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
	AND NOT IS_PAUSE_MENU_ACTIVE()
		IF NOT DARTS_CHECK_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
			DARTS_SET_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	IF DARTS_CHECK_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
		DEBUG_MESSAGE_PERIODIC("CLIENT DARTS_MPSTATE_LEAVE", iDebugThrottle)
		DARTS_QUIT_UI(DartsUI)
		
		IF NOT IS_PAUSE_MENU_ACTIVE()
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				CLEAR_PRINTS()
				DARTS_SET_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_CONFIRMED)
				bPlayerQuit = TRUE
				RETURN TRUE
			ENDIF
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SETUP_QUITUI, FALSE)
				DARTS_CLEAR_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DARTS_PLAYER_QUIT_CONTROL_SPECTATOR(DARTS_PLAYER_BROADCAST_DATA & playerData, DARTS_UI & DartsUI, INT & iDebugThrottle)
	
	IF DARTS_CHECK_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
		DEBUG_MESSAGE_PERIODIC("CLIENT DARTS_MPSTATE_LEAVE", iDebugThrottle)
		DARTS_QUIT_UI(DartsUI)
		
		IF NOT IS_PAUSE_MENU_ACTIVE()
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				CLEAR_PRINTS()
				bPlayerQuit = TRUE
				RETURN TRUE
			ENDIF
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SETUP_QUITUI, FALSE)
				DARTS_CLEAR_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
			ENDIF
		ENDIF
	ELSE
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		AND NOT IS_PAUSE_MENU_ACTIVE()
			IF NOT DARTS_CHECK_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
				DARTS_SET_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

CONST_FLOAT fBoard_offsetZ -0.7744

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"

	BOOL bDangerZoneOccupied
	BOOL bWinDartsGame
	BOOL bTurnOffShakes

	PROC ADD_AM_DART_WIDGETS()
		START_WIDGET_GROUP("AM_DARTS")
			ADD_WIDGET_BOOL("Occupy Danger Zone", bDangerZoneOccupied)
			ADD_WIDGET_BOOL("Win Game", bWinDartsGame)
			ADD_WIDGET_BOOL("Turn off shake", bTurnOffShakes)
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC PROCESS_WIDGETS(DARTS_SERVER_BROADCAST_DATA & ServerData)
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF bDangerZoneOccupied
			
				ServerData.bDangerZoneOccupied = TRUE
			ELSE
				ServerData.bDangerZoneOccupied = FALSE
			ENDIF
			
			IF bWinDartsGame
				ServerData.bDoWinning = TRUE
				bWinDartsGame = FALSE
			ENDIF
		ENDIF
		
		IF bTurnOffShakes
			fWobbleFactor = 0.0
			fRealWobFactor = 0.0
		ENDIF
		
	ENDPROC
	
#ENDIF

FUNC BOOL DARTS_TUTORIAL(DARTS_TUTORIAL_STAGE & DartsTutorialStage, DARTS_BOARD & DartsBoard, DARTS_AI_BRAIN & AIBrain, 
						DARTS_PLAYER_STATE & TurnStage, DART & TutorialDarts[], structTimer & TutorialTimer, DARTS_PLAYER_BROADCAST_DATA & playerData[])
	
//	IF HANDLE_SKIP_CUTSCENE(iAllowSkipCutsceneTime)
//		DartsTutorialStage = TS_SKIP
//	ENDIF
	
//	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
//		IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_SKIP)
//			DARTS_SET_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_SKIP)
//		ELSE
//			DARTS_CLEAR_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_SKIP)
//		ENDIF
//	ENDIF
	
	IF DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_SKIP)
	AND DARTS_CHECK_CLIENT_FLAG(playerData[iOtherID], DARTS_CLIENTFLAG_TUTORIAL_SKIP)
		DO_SCREEN_FADE_OUT(500)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
		
		DartsTutorialStage = TS_SKIP
	ENDIF
	
	SWITCH DartsTutorialStage
		CASE TS_THROWING
			//iAllowSkipCutsceneTime = GET_GAME_TIMER()
			
			RESTART_TIMER_NOW(TutorialTimer)
			
			DISPLAY_RADAR(FALSE)	
			
			DARTS_TUTORIAL_CAM(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
								GET_TUTORIAL_CAM_COORD_OFFSET(TS_THROWING), GET_TUTORIAL_CAM_ROT_OFFSET(), 0, TRUE)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
				CDEBUG1LN(DEBUG_DARTS, "[DARTS_MPSTATE_TUTORIAL] fade in called for")
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			CDEBUG1LN(DEBUG_DARTS, "Finished TS_THROWING")
			DartsTutorialStage = TS_DART_LANDING
		BREAK
		
		CASE TS_DART_LANDING			
			
			SWITCH iTurn
				CASE 0
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 1.0
						
						PRINT_HELP("DARTS_HOW_TO_2")
						RESTART_TIMER_NOW(TutorialTimer)
						iTurn++
						
					ENDIF
				BREAK
				
				CASE 1
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 2.0
						IF TurnStage = DPS_AIM 
							AIBrain.iThrowValue = 20
							AIBrain.iThrowMultiplier = 3
							GET_AI_THROW_VECTOR(TutorialDarts[0], DartsBoard, TurnStage, AIBrain, TRUE)
						ENDIF
			
						IF TurnStage = DPS_THROW
							IF THROW_DART(TutorialDarts[0], DartsBoard)
								PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER", DartsBoard.vDartBoard)
								iTurn++
								RESTART_TIMER_NOW(TutorialTimer)
								TurnStage = DPS_AIM
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 5.0
						
						PRINT_HELP("DARTS_HOW_TO_3")
						RESTART_TIMER_NOW(TutorialTimer)
						iTurn++
						
					ENDIF
				BREAK
				
				CASE 3
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 2.0
						IF TurnStage = DPS_AIM 
							AIBrain.iThrowValue = 50
							AIBrain.iThrowMultiplier = 1
							GET_AI_THROW_VECTOR(TutorialDarts[1], DartsBoard, TurnStage, AIBrain, TRUE)
						ENDIF
			
						IF TurnStage = DPS_THROW
							IF THROW_DART(TutorialDarts[1], DartsBoard)
								PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER", DartsBoard.vDartBoard)
								RESTART_TIMER_NOW(TutorialTimer)
								TurnStage = DPS_AIM
								iTurn++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 4
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 5.0
						
						PRINT_HELP("DARTS_HOW_3A")
						RESTART_TIMER_NOW(TutorialTimer)
						iTurn++
						
					ENDIF
				BREAK
				
				CASE 5
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 2.0
						
						IF TurnStage = DPS_AIM
							AIBrain.iThrowValue = 20
							AIBrain.iThrowMultiplier = 2
							GET_AI_THROW_VECTOR(TutorialDarts[2], DartsBoard, 
												TurnStage, AIBrain, TRUE)
							CDEBUG1LN(DEBUG_DARTS, "*****************Got the last Throw Vector")
						ENDIF
			
						IF TurnStage = DPS_THROW
							IF THROW_DART(TutorialDarts[2], DartsBoard)
								RESTART_TIMER_NOW(TutorialTimer)
								PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER", DartsBoard.vDartBoard)
								CDEBUG1LN(DEBUG_DARTS, "*****************Threw the last Dart")
								TurnStage = DPS_AIM					
								DartsTutorialStage = TS_DOUBLE
								
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE TS_DOUBLE
			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 5.0
				RESTART_TIMER_NOW(TutorialTimer)
				CLEAR_HELP()
				DARTS_TUTORIAL_CAM_INTERP(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
											GET_TUTORIAL_CAM_COORD_OFFSET(TS_DOUBLE), GET_TUTORIAL_CAM_ROT_OFFSET(), 1500)
				//HELP_AT_ENTITY_OFFSET("DARTS_DOUBLE_T", DartGame.TutorialDarts[0].object, << 0, 0, -0.85 >>)
				DartsTutorialStage = TS_TRIPLE
				CDEBUG1LN(DEBUG_DARTS, "Leaving TS_DOUBLE")
			ENDIF
		BREAK
		
		CASE TS_TRIPLE
			IF NOT IS_CAM_INTERPOLATING(camTutInterp)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_DOUBLE_T")
				PRINT_HELP("DARTS_DOUBLE_T")
				
			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_DOUBLE_T")
			//	HELP_AT_ENTITY_OFFSET("DARTS_DOUBLE_T", DartGame.TutorialDarts[0].object, << 0, 0, -0.85 >>)
			ENDIF
			
			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 6.0
				RESTART_TIMER_NOW(TutorialTimer)
				DARTS_TUTORIAL_CAM(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
									GET_TUTORIAL_CAM_COORD_OFFSET(TS_TRIPLE), GET_TUTORIAL_CAM_ROT_OFFSET(), 1000)
				//HELP_AT_ENTITY("DARTS_TRIPLE_T", DartGame.TutorialDarts[0].object)
				//CLEAR_THIS_FLOATING_HELP("DARTS_DOUBLE_T")
				DartsTutorialStage = TS_BULLSEYE
				CDEBUG1LN(DEBUG_DARTS, "Leaving TS_TRIPLE")
			ENDIF
		BREAK
		
		CASE TS_BULLSEYE
			IF NOT IS_CAM_INTERPOLATING(camTutorial)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_TRIPLE_T")
				PRINT_HELP("DARTS_TRIPLE_T")
				
			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_TRIPLE_T")
			//	HELP_AT_ENTITY("DARTS_TRIPLE_T", DartGame.TutorialDarts[0].object)
			ENDIF
			
			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 6.0
				RESTART_TIMER_NOW(TutorialTimer)
				DARTS_TUTORIAL_CAM_INTERP(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
											GET_TUTORIAL_CAM_COORD_OFFSET(TS_BULLSEYE), GET_TUTORIAL_CAM_ROT_OFFSET(), 1000)
				//HELP_AT_ENTITY("DARTS_BULL_T", DartGame.TutorialDarts[1].object)
				//CLEAR_THIS_FLOATING_HELP("DARTS_TRIPLE_T")
				DartsTutorialStage = TS_DBL_TO_WIN
				CDEBUG1LN(DEBUG_DARTS, "Leaving TS_BULLSEYE")
			ENDIF
		BREAK
		
		CASE TS_DBL_TO_WIN
			IF NOT IS_CAM_INTERPOLATING(camTutInterp)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_BULL_T")
				PRINT_HELP("DARTS_BULL_T")
				
			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_BULL_T")
			//	HELP_AT_ENTITY("DARTS_BULL_T", DartGame.TutorialDarts[1].object)
			ENDIF
			
			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 6.0
				RESTART_TIMER_NOW(TutorialTimer)
				DARTS_TUTORIAL_CAM(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
									GET_TUTORIAL_CAM_COORD_OFFSET(TS_DBL_TO_WIN), GET_TUTORIAL_CAM_ROT_OFFSET(), 1500)
				//HELP_AT_ENTITY("DARTS_DBL_WIN", DartGame.TutorialDarts[2].object)
				//CLEAR_THIS_FLOATING_HELP("DARTS_BULL_T")
				DartsTutorialStage = TS_ENDING
				CDEBUG1LN(DEBUG_DARTS, "Leaving TS_DBL_TO_WIN")
			ENDIF
		BREAK
		
		CASE TS_ENDING
			IF NOT IS_CAM_INTERPOLATING(camTutorial)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_DBL_WIN")
				PRINT_HELP("DARTS_DBL_WIN")
				
			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_DBL_WIN")
			//	HELP_AT_ENTITY("DARTS_DBL_WIN", DartGame.TutorialDarts[2].object)
			ENDIF
			
			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 6.0 //TIMERA() > 6000
				
				SET_CAM_ACTIVE_WITH_INTERP(camDartGame, camTutorial, 1500)
				//CLEAR_THIS_FLOATING_HELP("DARTS_DBL_WIN")
				DartsTutorialStage = TS_ENDING2
				CDEBUG1LN(DEBUG_DARTS, "Leaving TS_ENDING")
			ENDIF
		BREAK
		
		CASE TS_ENDING2
			IF NOT IS_CAM_INTERPOLATING(camDartGame)
				iTurn = 0
				CLEAR_HELP()
				CLEAR_PRINTS()
				CLEAR_ALL_FLOATING_HELP()
				INT i
				REPEAT NUM_DART_TURNS i
					CLEANUP_DART(TutorialDarts[i])
				ENDREPEAT
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TS_SKIP
			IF IS_SCREEN_FADED_OUT()
				iTurn = 0
				CLEAR_HELP()
				CLEAR_PRINTS()
				CLEAR_ALL_FLOATING_HELP()
				INT i
				REPEAT NUM_DART_TURNS i
					CLEANUP_DART(TutorialDarts[i])
				ENDREPEAT
				
				SET_CAM_ACTIVE(camDartGame, TRUE)
				
				//DO_SCREEN_FADE_IN(500)
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC DARTS_SETUP_MP_QUIT_CAM()
	
	IF NOT DOES_CAM_EXIST(ciDartsQuit)
		ciDartsQuit = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>)
	ENDIF
	
	SET_CAM_COORD(ciDartsQuit, <<1993.8000, 3049.7000, 47.8000>>)
	SET_CAM_ROT(ciDartsQuit, <<5.8000, -0.0000, 55.6000>>)
	SET_CAM_FOV(ciDartsQuit, 50.0)
	SET_CAM_ACTIVE(ciDartsQuit, TRUE)
	
ENDPROC

/// PURPOSE:
///    takes off high heels, ONLY USE FOR CLONED PEDS
/// PARAMS:
///    dartPed - one of the players
PROC DARTS_TAKE_OFF_HIGH_HEELS(PED_INDEX & dartPed)
	SET_PED_COMPONENT_VARIATION(dartPed, PED_COMP_FEET, 1, 0)
ENDPROC


FUNC BOOL IS_DARTS_GAME_ALMOST_DONE(DARTS_SERVER_BROADCAST_DATA & ServerData, DARTS_PLAYER_BROADCAST_DATA & PlayerData[])
	
	BOOL bGameAlmostDone
	
	// sets and legs are both 1, score is at least < 100
	// sets > 1, set amount is greater then set max * 66%
	// sets = 1, leg amount is greater then leg max * 66%
	
	IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
		IF ((serverData.iNumLegs = 1 AND serverData.iNumSets = 1) AND (ServerData.iScores[iSelfID] < 101 OR ServerData.iScores[iOtherID] < 101))
		OR ( serverData.iNumSets > 1 AND (serverData.iSets[iSelfID] >= FLOOR(serverData.iNumSets * 0.66) OR serverData.iSets[iOtherID] >= FLOOR(serverData.iNumSets * 0.66)) )
		OR ( serverData.iNumLegs > 1 AND (serverData.iLegs[iSelfID] >= FLOOR(serverData.iNumLegs * 0.33) OR serverData.iLegs[iOtherID] >= FLOOR(serverData.iNumLegs * 0.33)) )
			CDEBUG1LN(DEBUG_DARTS, "dart game is almost done")
			bGameAlmostDone = TRUE
		ELSE
			CDEBUG1LN(DEBUG_DARTS, "dart game is NOT almost done")
		ENDIF
		
		CDEBUG1LN(DEBUG_DARTS, "serverData.iNumLegs = ", serverData.iNumLegs)
		CDEBUG1LN(DEBUG_DARTS, "serverData.iNumSets = ", serverData.iNumSets)
		CDEBUG1LN(DEBUG_DARTS, "serverData.iSets[iSelfID] = ", serverData.iSets[iSelfID])
		CDEBUG1LN(DEBUG_DARTS, "serverData.iSets[iOtherID] = ", serverData.iSets[iOtherID])
		CDEBUG1LN(DEBUG_DARTS, "serverData.iLegs[iSelfID] = ", serverData.iLegs[iSelfID])
		CDEBUG1LN(DEBUG_DARTS, "serverData.iLegs[iOtherID] = ", serverData.iLegs[iOtherID])
		CDEBUG1LN(DEBUG_DARTS, "serverData.iScores[iSelfID] = ", serverData.iScores[iSelfID])
		CDEBUG1LN(DEBUG_DARTS, "serverData.iScores[iOtherID] = ", serverData.iScores[iOtherID])
		
	ENDIF
	
	RETURN bGameAlmostDone
	
ENDFUNC

PROC DARTS_DISPLAY_SPECIAL_SPLASH_MP_SHARD(DARTS_UI & DartsUI)
	IF DartsUI.bTriggerSplash
		IF DartsUI.eSplashStage = DARTSSPLASHSTAGE_DONE
			DartsUI.eSplashStage = DARTSSPLASHSTAGE_INIT
		ENDIF
		DartsUI.bTriggerSplash = FALSE
	ENDIF
	
	SWITCH DartsUI.eSplashStage
		CASE DARTSSPLASHSTAGE_INIT
			SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, DARTS_GET_SPLASH_STRING(DartsUI.eWhichSplash), "", 2000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
			DartsUI.eSplashStage = DARTSSPLASHSTAGE_DISPLAY
		BREAK
		CASE DARTSSPLASHSTAGE_DISPLAY
			IF NOT UPDATE_SHARD_BIG_MESSAGE(shardMidSizeMessage, FALSE)
				RESET_SHARD_BIG_MESSAGE(shardMidSizeMessage)
				DartsUI.eSplashStage = DARTSSPLASHSTAGE_DONE
			ENDIF
		BREAK
		CASE DARTSSPLASHSTAGE_DONE
			// nada
		BREAK
	ENDSWITCH
ENDPROC

PROC DARTS_MAINTAIN_REALTIME_MP(DARTS_MP_STATE eMPState)
	IF eMPState < DARTS_MPSTATE_IN_PROGRESS
		NETWORK_DISABLE_REALTIME_MULTIPLAYER()
	ENDIF
ENDPROC

SCRIPT (MP_MISSION_DATA missionScriptArgs)
	
	MP_DARTS_GAME MPDartGame 
	DARTS_ARGS Args
	DARTS_STARTING_RETICLE_POS DartsInitReticlePos
	DARTS_PLAYER_POS DartsPlayerPos
	DARTS_MP_TERMINATE_REASON terminateReason
	DARTS_MP_LEAVE_STATE leaveState
	DARTS_SCOREBOARD DartsSB
	DARTS_UI DartsUI
	DARTS_TIMER localDartsTimer //for use with the rematch/quit clock
	DART_PLAYER_POSITION DartPlayerPosition
	DARTS_LOCATION DartsLocation
	
	DARTS_TUTORIAL_STAGE DartsTutorialStage
	DARTS_AI_BRAIN AIBrain
	DARTS_PLAYER_STATE TurnStage = DPS_AIM
	//DART TutorialDarts[3]
	
	DARTS_MP_STATE eSpecState = DARTS_MPSTATE_INIT
	DARTS_MP_WAIT_STAGE eSpecWaitStage
	DARTS_MP_TURN_STATUS eSpecTurnStatus
	
	//END_SCREEN_DATASET_TITLE_UPDATE DartsEndScreen
	
	GAMER_HANDLE DartsGamerHandle[NUM_DART_PLAYERS]
	
	SCRIPT_TIMER timeDartsAwardFailSafe
	
	structTimer controlTimer
	structTimer ReThrowTimer
	structTimer TutorialTimer
	structTimer SpectateGateTimer
	structTimer endDelay
	
//	FMMC_EOM_DETAILS sEndOfMission
//	PLAYER_CURRENT_STATUS_COUNTS sPlayerCounts
	
	GENERIC_MINIGAME_STAGE eGenericMGStage = GENERIC_CELEBRATION_WINNER
	PLAYER_INDEX winningPlayer
	
	PLAYER_INDEX broadcastIndex

	SEQUENCE_INDEX siTemp
	
	PED_INDEX piOtherPlayer
	//PED_INDEX piOpponentClone
	PED_INDEX piPlayerClones[2]
	
	OBJECT_INDEX oiDartChallenge
	
	VECTOR vPlayerCoord
	VECTOR vCamCoord
	VECTOR vPlayerCurrentPos
	VECTOR vHickBar //, vRockClub //, vLostHideout
	//VECTOR vTemp
	VECTOR vSpectatePos = <<1993.2574, 3044.2151, 46.2151>>//<<1995.5557, 3047.5154, 46.2153>>
	
	VECTOR scenePosition
	VECTOR sceneRotation
	
	INT i, j
	INT iSplashStage
	INT iDebugThrottle
	INT iStaminaXP
	INT iLevelBit
	
	INT iBullseyeXP
	INT iXPGained
	INT iXPToGive
	
	INT iBetWinnings
	
	INT iNumParticipants
	INT iLastNumParticipants
	INT iNumPlayers
	INT iLastNumPlayers
	
	//for use by spectators / sctv
	INT iSpectatePlayerID[DART_PLAYER_IDS]
	INT iSpecTurn
	INT iSpecThower
	INT iSpecLastScore[DART_PLAYER_IDS]
	
	INT iSceneId
	
	FLOAT fStaminaPercentage
	FLOAT fSteadyShotLength
	FLOAT fSpectateHeading = 42.3856
	
	BOOL bFoundPlayer
	BOOL bAlreadyInGangHouse
	BOOL bWaitUIShown // TODO:  CONVERT TO FLAG SYSTEM
	BOOL bBoardTakenUIShown // TODO:  CONVERT TO FLAG SYSTEM
	BOOL bInWinRange
	BOOL bWaitUIShow
	BOOL bShowSplash
	BOOL bShowShotClockThrowHelp
	BOOL bSpecResetDone
	BOOL bCheckPlayers
	BOOL bPlaySplashFX
	BOOL bPhaseGrabbed
	
	TEXT_LABEL_7 sCrewTags[NUM_DART_PLAYERS]
	
	TEXT_LABEL_63 sShardPlayerName
	TEXT_LABEL_23 contentID	
	
	STRING sDartsPlayerNames[NUM_DART_PLAYERS]
	
	STRING sOutroWin[3]
	STRING sOutroLoss[3]
	STRING sOutroCam[3]
	
	sOutroWin[0] = "darts_outro_01_guy1"
	sOutroWin[1] = "darts_outro_02_guy2"
	sOutroWin[2] = "darts_outro_03_guy2"
	
	sOutroLoss[0] = "darts_outro_01_guy2"
	sOutroLoss[1] = "darts_outro_02_guy1"
	sOutroLoss[2] = "darts_outro_03_guy1"
	
	sOutroCam[0] = "darts_outro_01_cam"
	sOutroCam[1] = "darts_outro_02_cam"
	sOutroCam[2] = "darts_outro_03_cam"
	
	sFacialDictPed 	= "facials@gen_male@variations@happy"
	sFacialDictPed2 = "facials@gen_male@variations@angry"
	
	sFacialClip[0] = "mood_happy_1"
	sFacialClip[1] = "mood_happy_2"
	sFacialClip[2] = "mood_happy_3"
	
	sFacialClip2[0] = "mood_angry_1"
	sFacialClip2[1] = "mood_angry_2"
	sFacialClip2[2] = "mood_angry_3"
	
	vHickBar = <<1992.293213,3050.582764,47.989731>>
	//vRockClub = <<-572.040649,294.195770,79.937401>>
	
	// uncomment this to reset flags
	//g_iDartsTutorialFlags = 0
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vRockClub) < GET_PLAYER_DISTANCE_FROM_LOCATION(vHickBar)
//			DartsLocation = DARTSLOCATION_ROCK_CLUB
//			Args.vDartBoard = vRockClub
//			Args.fBoardHeading = -4.999956
//			CDEBUG1LN(DEBUG_DARTS, "Darts game started at Rock Club")
//		ELSE
			DartsLocation = DARTSLOCATION_HICK_BAR
			Args.vDartBoard = vHickBar
			Args.fBoardHeading = 57.783146
			CDEBUG1LN(DEBUG_DARTS, "Darts game started at Hick Bar")
//		ENDIF
	ENDIF
	
//	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(Args.vDartBoard, 5.0, PROP_DART_BD_CAB_01)
//		Args.oiDartBoard = GET_CLOSEST_OBJECT_OF_TYPE(Args.vDartBoard, 5.0, PROP_DART_BD_CAB_01)
//		GET_COORDS_AND_ROTATION_OF_CLOSEST_OBJECT_OF_TYPE(Args.vDartBoard, 5.0, PROP_DART_BD_CAB_01, Args.vDartBoard, vTemp, EULER_XYZ)
//		Args.fBoardHeading = vTemp.z
//		CDEBUG1LN(DEBUG_DARTS, "PROP_DART_BD_CAB_01 found in area.")
//		CDEBUG1LN(DEBUG_DARTS, "Args.vDartBoard is now ", Args.vDartBoard)
//		CDEBUG1LN(DEBUG_DARTS, "Args.fBoardHeading is now ", Args.fBoardHeading)
//	ELSE
//		CDEBUG1LN(DEBUG_DARTS, "PROP_DART_BD_CAB_01 not found in area of Darts Args.")
//	ENDIF

	SETUP_DARTS_PC_CONTROLS()
	
	MPGlobals.DartsData.bDartsPlayBusy = FALSE
	
	bShowSplash = FALSE
	
	DARTS_SERVER_BROADCAST_DATA ServerData
	DARTS_PLAYER_BROADCAST_DATA PlayerData[DARTSMP_IDS]//[DART_PLAYER_IDS]
	
	IF NOT DARTS_NETWORK_INIT(missionScriptArgs, ServerData, PlayerData)
		CDEBUG1LN(DEBUG_DARTS, "Failed to receive an initial network broadcast. Cleaning up.")
		CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI, FALSE)
	ENDIF
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
	ENDIF
	
	//DARTS_SET_PLAYER_NAME(PlayerData[iSelfID], GET_PLAYER_NAME(PLAYER_ID()))
	
	fScaledWobble = 0.06
	fScaledRealWobble = 0.08
	fWobbleFactor = fScaledWobble
	fRealWobFactor = fScaledRealWobble
	
	PlayerData[iSelfID].iParticipantID = iSelfID
	DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_PARTICIPANT_SET)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PlayerData[iSelfID].iOffsetPlayerID = ENUM_TO_INT(DART_PLAYER_HOME)
	ELSE
		PlayerData[iSelfID].iOffsetPlayerID = ENUM_TO_INT(DART_PLAYER_AWAY)
	ENDIF
	
	CLIENT_SET_UP_PROXIMITY_CHAT(CHAT_PROXIMITY_MAX)
	
	REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_DARTS()
	
	WHILE NOT bFoundPlayer
		WAIT(0)
		
		MAINTAIN_CELEBRATION_PRE_LOAD(sCelebrationData)
		
		iNumberOfPlayers = NETWORK_GET_NUM_PLAYERS(PlayerData)
		
		IF NOT NETWORK_IS_IN_SESSION()
			CDEBUG1LN(DEBUG_DARTS, "NETWORK_IS_IN_SESSION() is FALSE, cleaning up")
			CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI, FALSE)
		
		ELIF DID_I_JOIN_MISSION_AS_SPECTATOR()
		OR IS_PLAYER_SCTV(PLAYER_ID())
			
			IF iNumberOfPlayers >= 2
				
				IF GET_BOTH_PLAYERS_ID(PlayerData, iSpectatePlayerID)
					bFoundPlayer = TRUE
					CDEBUG1LN(DEBUG_DARTS, "am_darts: joining as spectator for 2 player game")
				ENDIF
				
			ELIF iNumberOfPlayers >= 1
			
				IF NOT IS_TIMER_STARTED(controlTimer)
					CDEBUG1LN(DEBUG_DARTS, "looking for more players ")
					START_TIMER_NOW(controlTimer)
				ELIF GET_TIMER_IN_SECONDS(controlTimer) > 4.0
					GET_BOTH_PLAYERS_ID(PlayerData, iSpectatePlayerID)
					
					bFoundPlayer = TRUE
					CDEBUG1LN(DEBUG_DARTS, "am_darts: joining as spectator for one player game")
				ENDIF
			
			ELSE
			
				IF NOT IS_TIMER_STARTED(controlTimer)
					CDEBUG1LN(DEBUG_DARTS, "no players, wait 10 seconds for one to appear ")
					START_TIMER_NOW(controlTimer)
				ELIF GET_TIMER_IN_SECONDS(controlTimer) > 10.0
					
					CDEBUG1LN(DEBUG_DARTS, "no players, clean up")
					CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI, FALSE)
					
				ENDIF
			
			ENDIF
		
		ELIF iNumberOfPlayers >= NUM_DART_PLAYERS
			IF DART_GET_OTHER_PLAYERS_ID(PlayerData, iOtherID) = NUM_DART_PLAYERS
				bFoundPlayer = TRUE
				IF IS_TIMER_STARTED(controlTimer)
					CANCEL_TIMER(controlTimer)
				ENDIF
				
				CDEBUG1LN(DEBUG_DARTS, "2 player game started")
				
				//Stop people joining my mission // this will be changed for SCTV
				SET_FM_MISSION_AS_NOT_JOINABLE()
			ENDIF
			
		ELIF (MPGlobals.DartsData.bDartsPlayBusy = TRUE)
			CLEAR_HELP()
			
			BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE)
			CDEBUG1LN(DEBUG_DARTS, "am_darts: player started sitting, cleaning up")
			CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI, FALSE)
		
		ELIF IS_BIT_SET(MPGlobals.DartsData.iDisplayState, 5)
			PRINT_NOW("DARTS_ENEMIES", DEFAULT_GOD_TEXT_TIME, 0)
			BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE)
			CDEBUG1LN(DEBUG_DARTS, "ENEMIES IN THE HOUSE, CLEANING UP")
			CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
		
		ELSE
			
			IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
				IF NOT IS_TIMER_STARTED(controlTimer)
					CDEBUG1LN(DEBUG_DARTS, "bFoundPlayer = False, setting controlTimer ")
					START_TIMER_NOW(controlTimer)
				ELIF GET_TIMER_IN_SECONDS(controlTimer) > 4.0
					CDEBUG1LN(DEBUG_DARTS, "controlTimer > 4.0, must be a single player darts game")
					bFoundPlayer = TRUE
					
					DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
					bSoloMP = TRUE
					
					PlayerData[iSelfID].iOffsetPlayerID = ENUM_TO_INT(DART_PLAYER_HOME)
					
					IF IS_TIMER_STARTED(controlTimer)
						CANCEL_TIMER(controlTimer)
					ENDIF
					
					//Stop people joining my mission // this will be changed for SCTV
					SET_FM_MISSION_AS_NOT_JOINABLE()
				ENDIF
			ENDIF
			
			renderingCam = GET_RENDERING_CAM()
			IF DOES_CAM_EXIST(renderingCam)
				vCamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
				ENDIF
			ELSE
				vCamCoord = vPlayerCoord
			ENDIF
			
			// See if we are changing players.
			IF vCamCoord.z > vPlayerCoord.z + 50.0
				BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE)
				CDEBUG1LN(DEBUG_DARTS, "Player initiated MP menu.  Cleaning up AM_DARTS")
				CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI, FALSE)
			
				
			// Show help text.
			ELIF NOT bWaitUIShown
				
				bWaitUIShown = TRUE
			ENDIF
		ENDIF
	ENDWHILE
	
	CDEBUG1LN(DEBUG_DARTS, "*********ID Check********************")
	CDEBUG1LN(DEBUG_DARTS, "* iOtherID  = ", iOtherID)
	CDEBUG1LN(DEBUG_DARTS, "*************************************")
	
	// Wait for server and client broadcast data to sync up.
	IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
		WAIT(500)
	ENDIF
	
	// Make sure we aren't trying to join a game in progress.
	// this check was mainly for CnC
	IF (DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_MATCH_MADE) AND GET_CURRENT_GAMEMODE() != GAMEMODE_FM)
	AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		WHILE TRUE
			// Print a message saying we can't join this game.
			IF NOT bBoardTakenUIShown
				START_TIMER_NOW(controlTimer)
				
				bBoardTakenUIShown = TRUE
				
			// Wait for confirmation from player or timeout.
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR GET_TIMER_IN_SECONDS(controlTimer) >= 8.0
				CLEAR_HELP()
				CDEBUG1LN(DEBUG_DARTS, "Darts game was already in progress, cleaning up AM_DARTS")
				CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI, FALSE)
			ENDIF
			WAIT(0)
		ENDWHILE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		ADD_AM_DART_WIDGETS()
	#ENDIF
	
	DISPLAY_AREA_NAME(FALSE)
	
	IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
	AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		PlayerData[iSelfID].piOther = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))  
		CDEBUG1LN(DEBUG_DARTS, "piOther is set")
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "piOther is not set, must be a single player game, or spectator")
	ENDIF
	
	iStaminaXP = GET_MP_INT_CHARACTER_STAT(MP_STAT_STAMINA) //GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MULTIPLAYER, PS_STAMINA)
	CDEBUG1LN(DEBUG_DARTS, "stamina stat = ", iStaminaXP)
	
	fStaminaPercentage = TO_FLOAT(iStaminaXP)/100
	fSteadyShotLength = fStaminaPercentage * 540
	iSteadyShotLength = ROUND(fSteadyShotLength) + 660
	
	CDEBUG1LN(DEBUG_DARTS, "steady shot = ", iSteadyShotLength)
	IF iSteadyShotLength < 660
		iSteadyShotLength = 660
		CDEBUG1LN(DEBUG_DARTS, "steady shot readjusted to 660")
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] has reached the while loop")
	CPRINTLN(DEBUG_DARTS, "TYPING CHANGES ARE IN")
	
	IF bMinigamePlayerQuitFlag
		bMinigamePlayerQuitFlag = FALSE
	ENDIF
	
	STORE_PLAYER_INTERACTION_ANIM_VALUES_FOR_CELEBRATION_SCREEN()
	
	IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		ANIMPOSTFX_STOP_ALL()
	ENDIF
	
	//gdisablerankupmessage = TRUE
	SET_DISABLE_RANK_UP_MESSAGE(TRUE)
	
	WHILE TRUE
		WAIT(0)
		
		iNumberOfPlayers = NETWORK_GET_NUM_PLAYERS(PlayerData)
		
		//Check at the start of the frame if the script should die. 		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
		ENDIF
		
		SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0.0, -0.0375)
		
		NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
		
		// _______________________________DEBUG START______________________________________________
		#IF IS_DEBUG_BUILD
		
		// Debug Key: win game
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			CDEBUG1LN(DEBUG_DARTS, "Darts debug succeed!  setting win flag")
			bDartsNoSocialClub = TRUE
			RESTART_TIMER_NOW(controlTimer)
			PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END
			DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_WINNERD)
		ENDIF
		
		// Debug Key: lose game
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			CDEBUG1LN(DEBUG_DARTS, "Darts debug failed!  setting lose flag")
			bDartsNoSocialClub = TRUE
			RESTART_TIMER_NOW(controlTimer)
			PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END
			DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_LOSERD)
		ENDIF
		
		IF (PlayerData[iOtherID].eMPState = DARTS_MPSTATE_GAME_END)
		AND NOT (PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END)
		AND ( DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_WINNERD)
			OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_LOSERD) )
			CDEBUG1LN(DEBUG_DARTS, "DARTS_MPSTATE_GAME_END getting set because opponent debugged")
			bDartsNoSocialClub = TRUE
			RESTART_TIMER_NOW(controlTimer)
			PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END
		ENDIF
		
		PROCESS_WIDGETS(ServerData)
		
		#ENDIF
		// _______________________________DEBUG END________________________________________________
		
		//TODO: add an every frame check here to see who are players and who are sctv's
		INT idx
		INT IDcheck
		PARTICIPANT_INDEX participantID
		
		iNumPlayers = 0
		iNumParticipants = NETWORK_GET_NUM_PARTICIPANTS()
		
		IDcheck = PARTICIPANT_ID_TO_INT()
		
		IF IDcheck <> iSelfID
			CDEBUG1LN(DEBUG_DARTS, "ID has changed! WHAT?!!")
			iSelfID = IDcheck
		ENDIF
		
		IF iLastNumParticipants <> iNumParticipants
			CDEBUG1LN(DEBUG_DARTS, "last num participants has changed!")
			CDEBUG1LN(DEBUG_DARTS, "last num participants = ", iLastNumParticipants)
			CDEBUG1LN(DEBUG_DARTS, "current participants = ", iNumParticipants)
			bCheckPlayers = TRUE
			
			iLastNumParticipants = iNumParticipants
		ENDIF
		
		
		REPEAT iNumParticipants idx
			participantID = INT_TO_PARTICIPANTINDEX( idx )
			IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
				IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
				OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(participantID))
					IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
						IF bCheckPlayers
							CDEBUG1LN(DEBUG_DARTS, "this participant is sctv: ", idx)
						ENDIF
					ENDIF
					IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(participantID))
						IF bCheckPlayers
							CDEBUG1LN(DEBUG_DARTS, "this participant is spectator: ", idx)
						ENDIF
					ENDIF
				ELSE
					IF bCheckPlayers
						CDEBUG1LN(DEBUG_DARTS, "this participant is a player: ", idx)
						iNumPlayers++
						CDEBUG1LN(DEBUG_DARTS, "number of players: ", iNumPlayers)
						IF iNumPlayers <> iLastNumPlayers
							CDEBUG1LN(DEBUG_DARTS, "number of players: ", iNumPlayers)
							CDEBUG1LN(DEBUG_DARTS, "number of last players: ", iLastNumPlayers)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bCheckPlayers
					CDEBUG1LN(DEBUG_DARTS, "this participant is NOT active! : ", idx)
				ENDIF
			ENDIF
		ENDREPEAT
		
		iLastNumPlayers = iNumPlayers
		
		bCheckPlayers = FALSE
		
		// doing this to hide location text
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		DISABLE_SELECTOR_THIS_FRAME()
		SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
		
		ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
		ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
		
		DARTS_MAINTAIN_REALTIME_MP(PlayerData[iSelfID].eMPState)
		
		// #SPECTATE GAMEPLAY HERE VVVVV
		
		//IF IS_PLAYER_SCTV(PLAYER_ID())
		IF DID_I_JOIN_MISSION_AS_SPECTATOR()
		OR IS_PLAYER_SCTV(PLAYER_ID())
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			OR PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]].eMPState > DARTS_MPSTATE_WAIT_POST_GAME
			OR PlayerData[iSpectatePlayerID[DART_PLAYER_AWAY]].eMPState > DARTS_MPSTATE_TERMINATE_SPLASH
			OR iNumberOfPlayers < 1
				CDEBUG1LN(DEBUG_DARTS, "looks like game players are quitting, CLEANING UP DARTS")
				CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
			ENDIF
			
			IF PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]].eMPState > DARTS_MPSTATE_SETUP
			AND eSpecState > DARTS_MPSTATE_SETUP
				DRAW_SCALEFORM_MOVIE_3D(DartsSB.siScoreBoard, DartsSB.vScoreBoardPos, DartsSB.vScoreBoardRot, DartsSB.vScoreBoardScale, DartsSB.vScoreBoardScale)
			ENDIF
			
			// checking the current sync'd data from the spectators view point
//			
//			IF (GET_GAME_TIMER() % 1500) < 50
//				CDEBUG1LN(DEBUG_DARTS, "********** ************ ***********")
//				CDEBUG1LN(DEBUG_DARTS, "PlayerData[DART_PLAYER_HOME_SERVER].eMPState = ", PlayerData[DART_PLAYER_HOME_SERVER].eMPState)
//				CDEBUG1LN(DEBUG_DARTS, "PlayerData[DART_PLAYER_HOME_SERVER].eMPThrowStage = ", PlayerData[DART_PLAYER_HOME_SERVER].eMPThrowStage)
//				CDEBUG1LN(DEBUG_DARTS, "PlayerData[DART_PLAYER_HOME_SERVER].eMPWaitStage = ", PlayerData[DART_PLAYER_HOME_SERVER].eMPWaitStage)
//				CDEBUG1LN(DEBUG_DARTS, "PlayerData[DART_PLAYER_HOME_SERVER].sName = ", PlayerData[DART_PLAYER_HOME_SERVER].sName)
//				CDEBUG1LN(DEBUG_DARTS, "********** ************ ***********")
//				CDEBUG1LN(DEBUG_DARTS, "PlayerData[DART_PLAYER_AWAY_SERVER].eMPState = ", PlayerData[DART_PLAYER_AWAY_SERVER].eMPState)
//				CDEBUG1LN(DEBUG_DARTS, "PlayerData[DART_PLAYER_AWAY_SERVER].eMPThrowStage = ", PlayerData[DART_PLAYER_AWAY_SERVER].eMPThrowStage)
//				CDEBUG1LN(DEBUG_DARTS, "PlayerData[DART_PLAYER_AWAY_SERVER].eMPWaitStage = ", PlayerData[DART_PLAYER_AWAY_SERVER].eMPWaitStage)
//				CDEBUG1LN(DEBUG_DARTS, "PlayerData[DART_PLAYER_AWAY_SERVER].sName = ", PlayerData[DART_PLAYER_AWAY_SERVER].sName)
//				CDEBUG1LN(DEBUG_DARTS, "********** ************ ***********")
//				CDEBUG1LN(DEBUG_DARTS, "serverData.mpState = ", serverData.mpState)
//				CDEBUG1LN(DEBUG_DARTS, "serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = ", serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER])
//				CDEBUG1LN(DEBUG_DARTS, "serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = ", serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER])
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iScores[DART_PLAYER_HOME_SERVER] = ", serverData.iScores[DART_PLAYER_HOME_SERVER])
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iScores[DART_PLAYER_AWAY_SERVER] = ", serverData.iScores[DART_PLAYER_AWAY_SERVER])
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iLastScore[DART_PLAYER_HOME_SERVER].iLastScore = ", serverData.iLastScore[DART_PLAYER_HOME_SERVER])
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iLastScore[DART_PLAYER_AWAY_SERVER].iLastScore = ", serverData.iLastScore[DART_PLAYER_AWAY_SERVER])
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iLegs[DART_PLAYER_HOME_SERVER] = ", serverData.iLegs[DART_PLAYER_HOME_SERVER])
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iLegs[DART_PLAYER_AWAY_SERVER] = ", serverData.iLegs[DART_PLAYER_AWAY_SERVER])
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iSets[DART_PLAYER_HOME_SERVER] = ", serverData.iSets[DART_PLAYER_HOME_SERVER])
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iSets[DART_PLAYER_AWAY_SERVER] = ", serverData.iSets[DART_PLAYER_AWAY_SERVER])
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iNumLegs = ", serverData.iNumLegs)
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iNumSets = ", serverData.iNumSets)
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iServerTurn = ", serverData.iServerTurn)
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iTurnscore = ", serverData.iTurnscore)
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iWinner = ", serverData.iWinner)
//				CDEBUG1LN(DEBUG_DARTS, "serverData.iThrower = ", serverData.iThrower)
//				CDEBUG1LN(DEBUG_DARTS, "********** ************ ***********")
//			ENDIF
			
			IF eSpecState >= DARTS_MPSTATE_THROW_OFF
			AND eSpecState < DARTS_MPSTATE_GAME_END
				
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
					
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						
						IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SETUP_INGAME)
							INIT_SIMPLE_USE_CONTEXT(DartsUI.inGameControlContext, FALSE, FALSE, FALSE, TRUE)
							ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "DARTS_END_QT", FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
							DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SETUP_INGAME, TRUE)
						ENDIF
						
						IF NOT NETWORK_TEXT_CHAT_IS_TYPING()
							UPDATE_SIMPLE_USE_CONTEXT(DartsUI.inGameControlContext, 200, default, default, FALSE)
						ENDIF
						
						IF DARTS_PLAYER_QUIT_CONTROL(PlayerData[iSelfID], DartsUI, iDebugThrottle)
							eSpecState = DARTS_MPSTATE_END
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						IF DARTS_PLAYER_QUIT_CONTROL_SPECTATOR(PlayerData[iSelfID], DartsUI, iDebugThrottle)
							eSpecState = DARTS_MPSTATE_END
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			SWITCH eSpecState
				
				CASE DARTS_MPSTATE_INIT	
					
					CDEBUG1LN(DEBUG_DARTS, "Spectator has reached Client side INIT")
					INIT_BOARD(MPDartGame.dBoard, Args)
					
					INIT_CAM(MPDartGame.dBoard.vDartBoard, MPDartGame.dBoard.fBoardHeading)
					
					INT k
					REPEAT NUM_DART_TURNS k
						INIT_DARTS(MPDartGame.Darts[k], 0)//iSelfID)
					ENDREPEAT
					
					DARTS_INIT_SCOREBOARD(DartsSB, ENUM_TO_INT(DartsLocation), MPDartGame.dBoard.vDartBoard, MPDartGame.dBoard.fBoardHeading)
					REQUEST_STREAMS(DartsSB)
					REGISTER_SCRIPT_WITH_AUDIO()
					
					//Disabling UI
					DISABLE_ALL_MP_HUD()
					
					DISPLAY_RADAR(FALSE)
					DISABLE_CELLPHONE(TRUE)
					
					GET_WORLD_OFFSETS(MPDartGame.dBoard, DartsPlayerPos, DartsInitReticlePos, TRUE)
					CDEBUG1LN(DEBUG_DARTS, "GET_WORLD_OFFSETS obtained in DARTS_MPSTATE_SETUP")
					
					CDEBUG1LN(DEBUG_DARTS, "GET_WORLD_OFFSETS obtained in DARTS_MPSTATE_SETUP")
					
					DartsPlayerPos.vDartPlayrPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( MPDartGame.dBoard.vDartBoard, 
																									MPDartGame.dBoard.fBoardHeading,  
																									<< -0.4, -2.4 + fDartBoardOriginOffset, fBoard_offsetZ-1 >>)	
					DartsPlayerPos.vOpponentPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( MPDartGame.dBoard.vDartBoard, 
																									MPDartGame.dBoard.fBoardHeading, 
																									<< 0.5, -2.4 + fDartBoardOriginOffset, fBoard_offsetZ >>)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vSpectatePos)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), fSpectateHeading)
						IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
						ENDIF
						
						IF GET_PLAYER_CURRENT_HELD_WEAPON() <> WEAPONTYPE_UNARMED
							//TASK_SWAP_WEAPON(PLAYER_PED_ID(), FALSE)
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
							CDEBUG1LN(DEBUG_DARTS, "Had to holster weapon again in spectate")
						ENDIF
						
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							IF NOT IS_PLAYER_SCTV(PLAYER_ID())
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								CDEBUG1LN(DEBUG_DARTS, "Had to turn player control off again in spectate")
							ENDIF
						ENDIF
					ENDIF
					
					//NEW_LOAD_SCENE_START(vHickBar, <<-3.4831, 0.0223, 60.6925>>, 20)
					
					CDEBUG1LN(DEBUG_DARTS, "Spectator Finished Stage_init")
					eSpecState = DARTS_MPSTATE_STREAMING
				BREAK
				
				CASE DARTS_MPSTATE_STREAMING
					IF HAVE_STREAMS_LOADED(DartsSB)
					//AND ( IS_NEW_LOAD_SCENE_LOADED() )
						CDEBUG1LN(DEBUG_DARTS, "Spectator Finished DARTS_MPSTATE_STREAMING")
						eSpecState = DARTS_MPSTATE_SETUP
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "Spectator Loading scene")
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_SETUP
					CDEBUG1LN(DEBUG_DARTS, "Reached Spectator side SETUP")
					
					DARTS_ACTIVATE_QUIT_UI(DartsUI.uiQuitControls)
					DARTS_ACTIVATE_ENDGAME_UI(DartsUI.uiEndGameControls)
						
					
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						sDartsPlayerNames[DART_PLAYER_AWAY] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_AWAY])))
						DartsGamerHandle[DART_PLAYER_AWAY] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_AWAY])))
						sCrewTags[DART_PLAYER_AWAY] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[DART_PLAYER_AWAY])
						
						IF NOT IS_PED_INJURED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_AWAY]))))
							piPlayerClones[DART_PLAYER_AWAY] = CLONE_PED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_AWAY]))), FALSE, FALSE)
						ENDIF
						
						CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iSpectatePlayerID[DART_PLAYER_AWAY], " = ", sCrewTags[DART_PLAYER_AWAY])
						IF NOT IS_STRING_EMPTY(sCrewTags[DART_PLAYER_AWAY])
							DARTS_ADD_SB_CREW_TAG(DartsSB, ENUM_TO_INT(DART_PLAYER_AWAY), sCrewTags[DART_PLAYER_AWAY])
						ENDIF
						DARTS_CLEAR_SCORES(DartsSB, ENUM_TO_INT(DART_PLAYER_AWAY))
						
						IF IS_PED_WEARING_HIGH_HEELS(piPlayerClones[DART_PLAYER_AWAY])
							CDEBUG1LN(DEBUG_DARTS, "clone away ped is wearing high heels, taking them off")
							DARTS_TAKE_OFF_HIGH_HEELS(piPlayerClones[DART_PLAYER_AWAY])
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "clone away ped is not wearing high heels")
						ENDIF
					ENDIF
					
					sDartsPlayerNames[DART_PLAYER_HOME] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_HOME])))
					DartsGamerHandle[DART_PLAYER_HOME] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_HOME])))
					sCrewTags[DART_PLAYER_HOME] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[DART_PLAYER_HOME])
					
					piOtherPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_HOME])))
					
					IF NOT IS_PED_INJURED(piOtherPlayer)
						piPlayerClones[DART_PLAYER_HOME] = CLONE_PED(piOtherPlayer, FALSE, FALSE)
					ENDIF
					
					IF IS_PED_WEARING_HIGH_HEELS(piPlayerClones[DART_PLAYER_HOME])
						CDEBUG1LN(DEBUG_DARTS, "clone home ped is wearing high heels, taking them off")
						DARTS_TAKE_OFF_HIGH_HEELS(piPlayerClones[DART_PLAYER_HOME])
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "clone home ped is not wearing high heels")
					ENDIF
					
					CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iSpectatePlayerID[DART_PLAYER_HOME], " = ", sCrewTags[DART_PLAYER_HOME])
					IF NOT IS_STRING_EMPTY(sCrewTags[DART_PLAYER_HOME])
						DARTS_ADD_SB_CREW_TAG(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME), sCrewTags[DART_PLAYER_HOME])
					ENDIF
					DARTS_CLEAR_SCORES(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
					
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						DARTS_ADD_NAMES(DartsSB, sDartsPlayerNames[DART_PLAYER_AWAY], sDartsPlayerNames[DART_PLAYER_HOME])
					ELSE
						DARTS_ADD_NAMES(DartsSB, "", sDartsPlayerNames[DART_PLAYER_HOME])
					ENDIF
					
					// maybe add already listed scores here
					
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts.sc: g_FMMC_STRUCT.iNumberOfLegs = ", g_FMMC_STRUCT.iNumberOfLegs)
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts.sc: g_FMMC_STRUCT.iNumberOfSets = ", g_FMMC_STRUCT.iNumberOfSets)
					
					IF g_FMMC_STRUCT.iNumberOfLegs > 0
					OR g_FMMC_STRUCT.iNumberOfSets > 0
						UPDATE_SB_SETS_LEGS(DartsSB, 0, 0, 0, 0)
					ENDIF
					
					CLEAR_AREA(Args.vDartBoard, 0.5, TRUE)
					
					DARTS_GAME_CAM()
					
					CDEBUG1LN(DEBUG_DARTS, "spectator Going from DARTS_MPSTATE_SETUP to DARTS_MPSTATE_TUTORIAL")
					eSpecState = DARTS_MPSTATE_TUTORIAL
				BREAK
				
				CASE DARTS_MPSTATE_TUTORIAL
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
					AND ServerData.mpState < DARTS_MPSTATE_SETUP
						CDEBUG1LN(DEBUG_DARTS, "spectator Going to DARTS_MPSTATE_THROW_OFF_SETUP")
						eSpecState = DARTS_MPSTATE_THROW_OFF_SETUP
					ELSE
						DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_DARTS, ServerData.iMatchHistoryID, ServerData.iMatchType)
						CDEBUG1LN(DEBUG_DARTS, "spectator Going to DARTS_MPSTATE_SYNC_OBJECTS")
						CDEBUG1LN(DEBUG_DARTS, "ServerData.mpState = ", ServerData.mpState)
						CDEBUG1LN(DEBUG_DARTS, "single player flag = ", DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER))
						eSpecState = DARTS_MPSTATE_SYNC_OBJECTS
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_THROW_OFF_SETUP
					IF NOT IS_TIMER_STARTED(controlTimer)
						//SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_CUSTOM, "DARTS_THRW_OFF", "DARTS_THRW_STR")
						//SET_SCALEFORM_BIG_MESSAGE(DartsUI.siBigMessage, "DARTS_THRW_OFF", "DARTS_THRW_STR", default, default, default, TRUE, MG_BIG_MESSAGE_ANIM_TIME)
						
						START_TIMER_NOW(controlTimer)
						
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ENDIF
					ELIF GET_TIMER_IN_SECONDS_SAFE(controlTimer) >= 5.0
					//OR NOT UPDATE_SCALEFORM_BIG_MESSAGE(DartsUI.siBigMessage)
					OR PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]].eMPState = DARTS_MPSTATE_THROW_OFF
					OR PlayerData[iSpectatePlayerID[DART_PLAYER_AWAY]].eMPState = DARTS_MPSTATE_THROW_OFF
						CLEANUP_BIG_MESSAGE()
						CANCEL_TIMER(controlTimer)
						
						UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
						
						DARTS_INIT_TIMERS(localDartsTimer)
						
						DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_DARTS, ServerData.iMatchHistoryID, ServerData.iMatchType)
						
						eSpecState = DARTS_MPSTATE_THROW_OFF
						
						eSpecTurnStatus = DARTS_MPTURN_WAITING
						eSpecWaitStage = DARTS_MPWAIT_AIM
						iSpecThower = ENUM_TO_INT(DART_PLAYER_HOME)
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_THROW_OFF
					IF DARTS_SERVER_CHECK_FLAG(ServerData, DARTSERVERFLAG_DO_THROWOFF)
						IF serverData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_HOME]] = DARTS_MPTURN_WINNING
						OR serverData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_AWAY]] = DARTS_MPTURN_WINNING
							eSpecTurnStatus = DARTS_MPTURN_WINNING
						ENDIF
						
						// Do everyting playing related right chea
						SWITCH eSpecTurnStatus 
							CASE DARTS_MPTURN_WAITING
								SWITCH eSpecWaitStage
									CASE DARTS_MPWAIT_AIM
										
										
										
										IF playerData[iSpectatePlayerID[iSpecThower]].bDartThrown
											CDEBUG1LN(DEBUG_DARTS, "Current client finished WAIT - AIMING")
											
											DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
											
											// grab other players dart throw info so we can throw that thang
											iSpecTurn = ServerData.iServerTurn
											COPY_DART_TO_LOCAL(MPDartGame.Darts[iSpecTurn], playerData[iSpectatePlayerID[iSpecThower]])
											
											eSpecWaitStage = DARTS_MPWAIT_THROW
										ELSE
											IF (GET_GAME_TIMER() % 1500) < 50
												CDEBUG1LN(DEBUG_DARTS, "spectator is waiting for other client to throw")
												CDEBUG1LN(DEBUG_DARTS, "iSpecThower = ", iSpecThower)
												CDEBUG1LN(DEBUG_DARTS, "iSpectatePlayerID[iSpecThower] = ", iSpectatePlayerID[iSpecThower])
												CDEBUG1LN(DEBUG_DARTS, "serverdata.iThrower = ", serverdata.iThrower)
											ENDIF
										ENDIF
									BREAK
									
									CASE DARTS_MPWAIT_THROW		
										CDEBUG1LN(DEBUG_DARTS, "[spec] current client is WAITING - Throwing")
										IF THROW_DART(MPDartGame.Darts[iSpecTurn], MPDartGame.dBoard)
											CDEBUG1LN(DEBUG_DARTS, "Regular Throw Completed - spec side")
											
											bSpecResetDone = FALSE
											
											eSpecWaitStage = DARTS_MPWAIT_SCORE
											CDEBUG1LN(DEBUG_DARTS, "[spec] going to spec side score check")
											eSpecTurnStatus = DARTS_MPTURN_SCORE_CHECK
										ENDIF
									BREAK
									
									CASE DARTS_MPWAIT_SCORE
										IF (GET_GAME_TIMER() % 2000) < 50
											CDEBUG1LN(DEBUG_DARTS, "[spec] going to spec side score check")
										ENDIF
									BREAK
								ENDSWITCH
							BREAK
							
							CASE DARTS_MPTURN_SCORE_CHECK
								
								// spectate is only for sp darts, so tuning this just for that
								IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED)
									// if prints are needed 
									DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, TRUE)
								ENDIF
								
								IF NOT bSpecResetDone
								//AND DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE)
									IF (playerData[iSpectatePlayerID[DART_PLAYER_HOME]].bResetDone OR playerData[iSpectatePlayerID[DART_PLAYER_AWAY]].bResetDone)
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec is in SCORE CHECK")
										
										eSpecWaitStage = DARTS_MPWAIT_AIM
										
										RESTART_TIMER_NOW(SpectateGateTimer)
										
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec finished reset in SCORE CHECK")
										bSpecResetDone = TRUE
									ENDIF
								ELSE
									IF (GET_GAME_TIMER() % 2000) < 50
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec waiting for client flag bResetDone")
									ENDIF
								ENDIF
								
								IF bSpecResetDone
								AND ( ServerData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_HOME]] = DARTS_MPTURN_THROWING
									OR ServerData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_AWAY]] = DARTS_MPTURN_THROWING )
								AND ( playerData[iSpectatePlayerID[DART_PLAYER_HOME]].eMPThrowStage = DARTS_MPTHROW_AIM
									OR playerData[iSpectatePlayerID[DART_PLAYER_AWAY]].eMPThrowStage = DARTS_MPTHROW_AIM )
								AND GET_TIMER_IN_SECONDS_SAFE(SpectateGateTimer) > 1.0
//									IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER) //serverData.bDoSwitchPlayer
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec did see server flag for switch_player")
										
										iSpecThower = playerData[iSpectatePlayerID[DART_PLAYER_AWAY]].iOffsetPlayerID
										
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											UPDATE_HIGHLIGHT(DartsSB, playerData[serverData.iWaiter].iOffsetPlayerID)
										ENDIF
//									ELSE
//										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec did not see server flag for switch_player")
//									ENDIF
									
									CANCEL_TIMER(SpectateGateTimer)
									
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec going back to waiting")
									eSpecTurnStatus = DARTS_MPTURN_WAITING
								ENDIF
							BREAK
							
							CASE DARTS_MPTURN_WINNING
								INT y
								REPEAT NUM_MP_DART_TURNS y
									RESET_DART(MPDartGame.Darts[y])
									MPDartGame.Darts[y].bDoneScoring = FALSE
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", y, " was reset")
								ENDREPEAT
								
								CANCEL_TIMER(controlTimer)
								
								eSpecState = DARTS_MPSTATE_SYNC_OBJECTS
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_SYNC_OBJECTS
					CDEBUG1LN(DEBUG_DARTS, "spectator DARTS_MPSTATE_SYNC_OBJECTS")
					i = 0
					j = 0
					
					// TODO this will have to be adjusted to account for mid game joins
					// looks like other player will have to place this info in the broadcast data
					REPEAT NUM_MP_DART_PLAYERS i
						MPDartGame.iThrowsTaken[i] 	= 0
						MPDartGame.iBullsEyesHit[i]	= 0
						DARTS_CLEAR_SCORES(DartsSB, i)
						iSpecLastScore[i] = 301
					ENDREPEAT
					
					DARTS_ADD_SCORE(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME), 301)
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						DARTS_ADD_SCORE(DartsSB, ENUM_TO_INT(DART_PLAYER_AWAY), 301)
					ENDIF
					
					REPEAT  NUM_MP_DART_TURNS j
						RESET_DART(MPDartGame.Darts[j])
						MPDartGame.Darts[j].bDoneScoring = FALSE
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", j, " was reset")
					ENDREPEAT
					
					//RESET_CLIENT_DART(PlayerData[DART_PLAYER_HOME_SERVER])
					
					DARTS_INIT_TIMERS(localDartsTimer)
					
					eSpecWaitStage = DARTS_MPWAIT_AIM
					
					MPDartGame.bEndGameMessageDisplayed = FALSE
					MPDartGame.bWinNoise = FALSE
					
					bInWinRange = FALSE
					
					CLEAR_AREA(Args.vDartBoard, 0.5, TRUE)
					REMOVE_DECALS_IN_RANGE(Args.vDartBoard, 0.5)
					
					eSpecState = DARTS_MPSTATE_SETUP_POST
				BREAK
				
				CASE DARTS_MPSTATE_SETUP_POST
					IF ServerData.mpState = DARTS_MPSTATE_IN_PROGRESS
						
						DARTS_GAME_CAM()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						
						eSpecState = DARTS_MPSTATE_IN_PROGRESS
						eSpecTurnStatus = DARTS_MPTURN_WAITING
						eSpecWaitStage = DARTS_MPWAIT_AIM
						CDEBUG1LN(DEBUG_DARTS, "spectator is proceeding to IN PROGRESS")
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							iSpecThower = PICK_INT((serverData.iCoinFlip = 0), ENUM_TO_INT(DART_PLAYER_AWAY), ENUM_TO_INT(DART_PLAYER_HOME))
						ELSE
							iSpecThower = ENUM_TO_INT(DART_PLAYER_HOME)
						ENDIF
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							UPDATE_HIGHLIGHT(DartsSB, PICK_INT((serverData.iCoinFlip = 0), ENUM_TO_INT(DART_PLAYER_AWAY), ENUM_TO_INT(DART_PLAYER_HOME)))
						ELSE
							UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
						ENDIF
						bWaitUIShow = FALSE
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_WAIT_POST_MENU
					CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] has reached Client side WAIT POST MENU")
					// Sitting in a waiting state
					
					IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_MOVETOGAME)
						PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]].eMPState = DARTS_MPSTATE_IN_PROGRESS
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_IN_PROGRESS
					
					// failsafe for player not being there
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())	//Not needed as the Target is the Focus - Bug 1673437
					AND GET_PLAYER_DISTANCE_FROM_LOCATION(vSpectatePos) > 2.0
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vSpectatePos)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), fSpectateHeading)
						IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
						ENDIF
						CDEBUG1LN(DEBUG_DARTS, " DARTS - SET PLAYER COORDS (2) ", vSpectatePos)
					ELSE
						// failsafe for screen not fading back in
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
							IF IS_SCREEN_FADED_OUT()
								DO_SCREEN_FADE_IN(500)
							ENDIF
							
							IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								CDEBUG1LN(DEBUG_DARTS, "Had to turn player control off again in in progress")
							ENDIF
						ENDIF
						
					ENDIF
					
					IF serverData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_HOME]] = DARTS_MPTURN_WINNING
					OR serverData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_AWAY]] = DARTS_MPTURN_WINNING
						eSpecTurnStatus = DARTS_MPTURN_WINNING
					ENDIF
					
//					IF (GET_GAME_TIMER() % 1000) < 50
//						CDEBUG1LN(DEBUG_DARTS, "serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = ", serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER])
//					ENDIF
					
						// Do everyting playing related right chea
					SWITCH eSpecTurnStatus 
						CASE DARTS_MPTURN_WAITING
							SWITCH eSpecWaitStage
								CASE DARTS_MPWAIT_AIM
									
									IF playerData[iSpectatePlayerID[iSpecThower]].bDartThrown
										CDEBUG1LN(DEBUG_DARTS, "Current client finished WAIT - AIMING")
										
										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
										
										// grab other players dart throw info so we can throw that thang
										iSpecTurn = ServerData.iServerTurn
										COPY_DART_TO_LOCAL(MPDartGame.Darts[iSpecTurn], playerData[iSpectatePlayerID[iSpecThower]])
										
										eSpecWaitStage = DARTS_MPWAIT_THROW
									ELSE
										IF (GET_GAME_TIMER() % 2000) < 50
											CDEBUG1LN(DEBUG_DARTS, "Spectator is waiting for client to throw")
											CDEBUG1LN(DEBUG_DARTS, "iSpecThower = ", iSpecThower)
											CDEBUG1LN(DEBUG_DARTS, "iSpectatePlayerID[iSpecThower] = ", iSpectatePlayerID[iSpecThower])
											CDEBUG1LN(DEBUG_DARTS, "playerData[iSpectatePlayerID[iSpecThower]].bDartThrown = ", playerData[iSpectatePlayerID[iSpecThower]].bDartThrown)
										ENDIF
									ENDIF
								BREAK
								
								CASE DARTS_MPWAIT_THROW		
									CDEBUG1LN(DEBUG_DARTS, "[spec] current client is WAITING - Throwing")
									IF THROW_DART(MPDartGame.Darts[iSpecTurn], MPDartGame.dBoard)
										CDEBUG1LN(DEBUG_DARTS, "Regular Throw Completed - spec side")
										
										bSpecResetDone = FALSE
										
										eSpecWaitStage = DARTS_MPWAIT_SCORE
										CDEBUG1LN(DEBUG_DARTS, "[spec] going to spec side score check")
										eSpecTurnStatus = DARTS_MPTURN_SCORE_CHECK
									ENDIF
								BREAK
								
								CASE DARTS_MPWAIT_SCORE
									IF (GET_GAME_TIMER() % 2000) < 50
										CDEBUG1LN(DEBUG_DARTS, "[spec] going to spec side score check")
									ENDIF
								BREAK
							ENDSWITCH
						BREAK
						
						CASE DARTS_MPTURN_SCORE_CHECK
							
							// spectate is only for sp darts, so tuning this just for that
							IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED)
								// if prints are needed 
								DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, TRUE)
							ENDIF
							
							IF NOT bSpecResetDone
							//AND DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE)
								IF (playerData[iSpectatePlayerID[DART_PLAYER_HOME]].bResetDone OR playerData[iSpectatePlayerID[DART_PLAYER_AWAY]].bResetDone)
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec is in SCORE CHECK")
									
									eSpecWaitStage = DARTS_MPWAIT_AIM
									
									RESTART_TIMER_NOW(SpectateGateTimer)
									
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec finished reset in SCORE CHECK")
									bSpecResetDone = TRUE
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 2000) < 50
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec waiting for client flag bResetDone")
								ENDIF
							ENDIF
							
							IF bSpecResetDone
							AND ( ServerData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_HOME]] = DARTS_MPTURN_THROWING
								OR (ServerData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_AWAY]] = DARTS_MPTURN_THROWING 
									AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)) )
							AND ( playerData[iSpectatePlayerID[DART_PLAYER_HOME]].eMPThrowStage = DARTS_MPTHROW_AIM
								OR (playerData[iSpectatePlayerID[DART_PLAYER_AWAY]].eMPThrowStage = DARTS_MPTHROW_AIM 
									AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)))
							AND GET_TIMER_IN_SECONDS_SAFE(SpectateGateTimer) > 1.0
								IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER) //serverData.bDoSwitchPlayer
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec did see server flag for switch_player")
									// sending the darts back to the player, resetting them
									// serves the same functioon as RESET_DART without deleting the object
									INT y
									REPEAT NUM_MP_DART_TURNS y
										RESET_DART(MPDartGame.Darts[y])
										MPDartGame.Darts[y].bDoneScoring = FALSE
										CDEBUG1LN(DEBUG_DARTS, "[spec] MPDartGame.Darts ", y, " was reset")
									ENDREPEAT
									
									IF iSpecLastScore[playerData[ServerData.iThrower].iOffsetPlayerID] <> ServerData.iScores[ServerData.iThrower]
										DARTS_ADD_SCORE(DartsSB, playerData[ServerData.iThrower].iOffsetPlayerID, ServerData.iScores[ServerData.iThrower])
									ENDIF
									
									CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] ServerData.iThrower = ", ServerData.iThrower)
									CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] ServerData.iLastScore = ", ServerData.iLastScore[ServerData.iThrower])
									CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] ServerData.iScores = ", ServerData.iScores[ServerData.iThrower])
									CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] iSpecLastScore = ", iSpecLastScore[playerData[ServerData.iThrower].iOffsetPlayerID])
									
									CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] ServerData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_HOME]] = ", ServerData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_HOME]])
									CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] ServerData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_AWAY]] = ", ServerData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_AWAY]])
									CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] playerData[iSpectatePlayerID[DART_PLAYER_HOME]].eMPThrowStage = ", playerData[iSpectatePlayerID[DART_PLAYER_HOME]].eMPThrowStage)
									CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] playerData[iSpectatePlayerID[DART_PLAYER_AWAY]].eMPThrowStage = ", playerData[iSpectatePlayerID[DART_PLAYER_AWAY]].eMPThrowStage)
									
									IF ServerData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_HOME]] = DARTS_MPTURN_THROWING
										iSpecThower = playerData[iSpectatePlayerID[DART_PLAYER_HOME]].iOffsetPlayerID
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] iSpecThower is going home")
									ELSE
										iSpecThower = playerData[iSpectatePlayerID[DART_PLAYER_AWAY]].iOffsetPlayerID
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] iSpecThower is going away")
									ENDIF
									
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] iSpecThower = ", iSpecThower)
									
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
										UPDATE_HIGHLIGHT(DartsSB, playerData[serverData.iWaiter].iOffsetPlayerID)
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] changed highlight to player ", playerData[serverData.iWaiter].iOffsetPlayerID)
									ENDIF
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec did not see server flag for switch_player")
								ENDIF
								
								CANCEL_TIMER(SpectateGateTimer)
								
								
								
								
								
								CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec going back to waiting")
								eSpecTurnStatus = DARTS_MPTURN_WAITING
							ENDIF
						BREAK
						
						CASE DARTS_MPTURN_WINNING
							CDEBUG1LN(DEBUG_DARTS, "********** serverData.iWinner = ", serverData.iWinner)
							
							CLEAR_PRINTS()
							
							IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
								CDEBUG1LN(DEBUG_DARTS, "[spec] server said to go to transition")
								iSplashStage = 0
								RESTART_TIMER_NOW(controlTimer)
								eSpecState = DARTS_MPSTATE_GAME_TRANSITION
							ELSE
								RESTART_TIMER_NOW(controlTimer)
								CDEBUG1LN(DEBUG_DARTS, "[spec] server said to go to game end")
								eSpecState = DARTS_MPSTATE_GAME_END
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DARTS_MPSTATE_GAME_TRANSITION
					
					SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_HOME])))
					
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_AWAY])))
					ENDIF
					
					SWITCH iSplashStage
						CASE 0
							IF GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 1.5
								CLEAR_HELP()
								
								//ANOTHER_GAME_CAM_MP(PLAYER_PED_ID())
								ANOTHER_GAME_CAM()
								
								SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "DARTS_WINP", "", 2500, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
								
								IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									UPDATE_SB_SETS_LEGS(DartsSB, 0, serverData.iSets[iSpectatePlayerID[DART_PLAYER_HOME]], 0, serverData.iLegs[iSpectatePlayerID[DART_PLAYER_HOME]])
								ELSE
									UPDATE_SB_SETS_LEGS(DartsSB, serverData.iSets[iSpectatePlayerID[DART_PLAYER_AWAY]], serverData.iSets[iSpectatePlayerID[DART_PLAYER_HOME]], 
														serverData.iLegs[iSpectatePlayerID[DART_PLAYER_AWAY]], serverData.iLegs[iSpectatePlayerID[DART_PLAYER_HOME]])
								ENDIF
								
								INT iWhichOutro
									
								FLOAT fResolution
								fResolution = GET_ASPECT_RATIO(TRUE)
								CDEBUG1LN(DEBUG_DARTS, " fResolution = ", fResolution)
								
								IF fResolution > 2.0
									CDEBUG1LN(DEBUG_DARTS, " resolution is eyefinity")
									scenePosition = << 1992.29407, 3047.57690, 46.21517 >>
									sceneRotation = << 0.000, 0.000, 138.740 >>
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "resolution is not eyefinity")
									scenePosition = << 1992.33569, 3047.92432, 46.21517 >>
									sceneRotation = << 0.000, 0.000, 136.740 >>
								ENDIF
								
								iWhichOutro = (GET_RANDOM_INT_IN_RANGE() % 3)
								
								CDEBUG1LN(DEBUG_DARTS, "iWhichOutro = ", iWhichOutro)
								
								iSceneID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
								camDartsSync = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
								PLAY_SYNCHRONIZED_CAM_ANIM(camDartsSync, iSceneID, sOutroCam[iWhichOutro], "mini@dartsoutro")
								SET_CAM_ACTIVE(camDartsSync, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									IF NOT IS_PED_INJURED(piPlayerClones[0])
									AND NOT IS_PED_INJURED(piPlayerClones[1])
										
										IF serverData.iWinner = iSpectatePlayerID[DART_PLAYER_HOME]
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_HOME], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_AWAY], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_PLAY_ANIM(piPlayerClones[DART_PLAYER_HOME], sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
										ELSE
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_AWAY], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_HOME], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_PLAY_ANIM(piPlayerClones[DART_PLAYER_AWAY], sFacialDictPed2, sFacialClip2[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_HOME])
										TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_HOME], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
										TASK_PLAY_ANIM(piPlayerClones[DART_PLAYER_HOME], sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
									ENDIF
								ENDIF
								
								iSplashStage++
							ENDIF
						BREAK
						
						CASE 1
							IF NOT UPDATE_SHARD_BIG_MESSAGE(shardMidSizeMessage, TRUE)
								
								RESET_SHARD_BIG_MESSAGE(shardMidSizeMessage)
								
								REPEAT NUM_MP_DART_PLAYERS i
									DARTS_CLEAR_SCORES(DartsSB, i)
								ENDREPEAT
								
								REPEAT NUM_MP_DART_TURNS j
									RESET_DART(MPDartGame.Darts[j])
								ENDREPEAT
								
								iSplashStage++
							ENDIF
						BREAK
						
						CASE 2
							CDEBUG1LN(DEBUG_DARTS, "spec going to wait post game Starting another set/leg")
							
							DARTS_GAME_CAM()
							
							IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
								IF NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_AWAY])
								AND NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_HOME])
									DESTROY_CAM(camDartsSync)
									CLEAR_PED_TASKS (piPlayerClones[DART_PLAYER_AWAY])
									CLEAR_PED_TASKS (piPlayerClones[DART_PLAYER_HOME])
								ENDIF
							ELSE
								IF NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_HOME])
									DESTROY_CAM(camDartsSync)
									CLEAR_PED_TASKS (piPlayerClones[DART_PLAYER_HOME])
								ENDIF
							ENDIF
							
							
							
							iSplashStage = 0
							eSpecState = DARTS_MPSTATE_WAIT_POST_GAME
						BREAK
						
					ENDSWITCH
				BREAK
				
				CASE DARTS_MPSTATE_GAME_END
					
					IF (GET_GAME_TIMER() % 2500) < 50
						CDEBUG1LN(DEBUG_DARTS, "Reached spec side GAME END")
					ENDIF
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_HOME]))
						SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_HOME])))
						IF (GET_GAME_TIMER() % 2500) < 50
							CDEBUG1LN(DEBUG_DARTS, "home player succesfully set invisible")
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "home player not set invisible, participant is not active")
					ENDIF
					
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_AWAY]))
							SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatePlayerID[DART_PLAYER_AWAY])))
							CDEBUG1LN(DEBUG_DARTS, "away player succesfully set invisible")
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "away player not set invisible, participant is not active")
						ENDIF
					ENDIF
					
					IF GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 0.75
						// wait for the player to pick an option
						IF MPDartGame.bEndGameMessageDisplayed 
							
							UPDATE_SHARD_BIG_MESSAGE(shardBigMessage)
							
							IF DID_I_JOIN_MISSION_AS_SPECTATOR()
							AND NOT IS_PLAYER_SCTV(PLAYER_ID())
								IF HAS_NET_TIMER_STARTED(serverData.timeLeaderboardTimeOut)
								OR (IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) >= 0.995)
								OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID)
									
//									IF MAINTAIN_END_OF_MISSION_SCREEN(sEndOfMission, serverData.sServerFMMC_EOM,
//																GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),serverData.timeLeaderboardTimeOut.timer) >= VIEW_LEADERBOARD_TIME,
//																FALSE, FALSE, FALSE, FALSE, FALSE, serverData.timeLeaderboardTimeOut.timer, VIEW_LEADERBOARD_TIME, "LBD_TIMEOUT", TRUE)
										//Go to cleanup state
										CDEBUG1LN(DEBUG_DARTS, "spectate voted")
										eSpecState = DARTS_MPSTATE_END
//									ELSE
//										//DISPLAY_DARTS_LEADERBOARD(DartsUI, DartsEndScreen, iXPGained, DartsUI.iOurScore, DartsUI.iTheirScore, iBetWinnings)
//									ENDIF
								ENDIF
							ENDIF
							
							// player wants another game
							IF PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]].bDoRematch 
							OR PlayerData[iSpectatePlayerID[DART_PLAYER_AWAY]].bDoRematch
								IF serverData.mpState >= DARTS_MPSTATE_SETUP
									
									INT iTemp1, iTemp2
									iTemp1 = ENUM_TO_INT(DART_PLAYER_AWAY)
									iTemp2 = ENUM_TO_INT(DART_PLAYER_HOME)
									
									CDEBUG1LN(DEBUG_DARTS, "iTemp1 = ", iTemp1, " iTemp2 = ", iTemp2)
									DARTS_CLEAR_SCORES(DartsSB, iTemp1)
									DARTS_CLEAR_SCORES(DartsSB, iTemp2)
									
									IF DID_I_JOIN_MISSION_AS_SPECTATOR()
									AND NOT IS_PLAYER_SCTV(PLAYER_ID())
										CDEBUG1LN(DEBUG_DARTS, "transitioning into actual game")
										
									ELSE
										CDEBUG1LN(DEBUG_DARTS, "spec state going to wait post game")
										eSpecState = DARTS_MPSTATE_WAIT_POST_GAME
									ENDIF
									
								ENDIF
							ENDIF
							
						ELSE
							
							// wait 'til we've finished rendering the player win/lost text
							IF NOT MPDartGame.bAnotherCamActive
								
								CLEAR_HELP()
								CLEAR_PRINTS()
								
								DartsUI.iOurScore += serverData.iSets[iSelfID]
								DartsUI.iTheirScore += serverData.iSets[iOtherID]
								DartsUI.sMyName = sDartsPlayerNames[PlayerData[iSelfID].iOffsetPlayerID]
								DartsUI.sTheirName = sDartsPlayerNames[PlayerData[iOtherID].iOffsetPlayerID]
								
								SET_SHARD_BIG_MESSAGE(shardBigMessage, "DARTS_WINP", "DARTS_WINSP")
								
								INT iWhichOutro
									
								FLOAT fResolution
								fResolution = GET_ASPECT_RATIO(TRUE)
								CDEBUG1LN(DEBUG_DARTS, " fResolution = ", fResolution)
								
								IF fResolution > 2.0
									CDEBUG1LN(DEBUG_DARTS, " resolution is eyefinity")
									scenePosition = << 1992.29407, 3047.57690, 46.21517 >>
									sceneRotation = << 0.000, 0.000, 138.740 >>
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "resolution is not eyefinity")
									scenePosition = << 1992.33569, 3047.92432, 46.21517 >>
									sceneRotation = << 0.000, 0.000, 136.740 >>
								ENDIF
								
								iWhichOutro = (GET_RANDOM_INT_IN_RANGE() % 3)
								
								CDEBUG1LN(DEBUG_DARTS, "iWhichOutro = ", iWhichOutro)
								
								iSceneID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
								camDartsSync = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
								PLAY_SYNCHRONIZED_CAM_ANIM(camDartsSync, iSceneID, sOutroCam[iWhichOutro], "mini@dartsoutro")
								SET_CAM_ACTIVE(camDartsSync, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									IF NOT IS_PED_INJURED(piPlayerClones[0])
									AND NOT IS_PED_INJURED(piPlayerClones[1])
										
										IF serverData.iWinner = iSpectatePlayerID[DART_PLAYER_HOME]
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_HOME], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_AWAY], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_PLAY_ANIM(piPlayerClones[DART_PLAYER_HOME], sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
										ELSE
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_AWAY], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_HOME], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_PLAY_ANIM(piPlayerClones[DART_PLAYER_AWAY], sFacialDictPed2, sFacialClip2[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_HOME])
										TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_HOME], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
										TASK_PLAY_ANIM(piPlayerClones[DART_PLAYER_HOME], sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
									ENDIF
								ENDIF
								
								//ANOTHER_GAME_CAM()
								DartsUI.iEndDelay = GET_GAME_TIMER()
								
								PRINT_HELP("DARTS_SPECAD")
								
								MPDartGame.bAnotherCamActive = TRUE
							
							//ELIF NOT IS_CAM_INTERPOLATING(camAnotherGameMP)
							ELIF GET_GAME_TIMER() - DartsUI.iEndDelay > 400
								
								MPDartGame.bEndGameMessageDisplayed = TRUE
								
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE DARTS_MPSTATE_WAIT_POST_GAME
					DEBUG_MESSAGE_PERIODIC("spectate DARTS_MPSTATE_WAIT_POST_GAME", iDebugThrottle)
					
					IF (PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]].bDoRematch AND PlayerData[iSpectatePlayerID[DART_PLAYER_AWAY]].bDoRematch)
					OR (PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]].bDoRematch AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]], DARTS_CLIENTFLAG_SINGLE_PLAYER))
					OR PlayerData[iSpectatePlayerID[DART_PLAYER_HOME]].eMPState = DARTS_MPSTATE_IN_PROGRESS
						//IF serverData.mpState = DARTS_MPSTATE_SETUP
							CDEBUG1LN(DEBUG_DARTS, "spectate going to sync objects")
							eSpecState = DARTS_MPSTATE_SYNC_OBJECTS
						//ENDIF
					ENDIF
					
				BREAK
				
				CASE DARTS_MPSTATE_TERMINATE_SPLASH
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_DARTS, "spec is chilling in DARTS_MPSTATE_TERMINATE_SPLASH")
						IF DID_I_JOIN_MISSION_AS_SPECTATOR()
							CDEBUG1LN(DEBUG_DARTS, "still being seen as a spectator")
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "no longer a spectator - this is physically impossible")
						ENDIF
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_END
					IF HAS_NET_TIMER_STARTED(timeDartsAwardFailSafe)
					
						//RENDER_SCRIPT_CAMS(FALSE, FALSE)
						DARTS_SETUP_MP_QUIT_CAM()
						
						CLEANUP_BIG_MESSAGE()
						
						IF IS_SCREEN_FADED_OUT()
						OR IS_SCREEN_FADING_OUT()
							DO_SCREEN_FADE_IN(500)
						ENDIF
						
						CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
						CDEBUG1LN(DEBUG_DARTS, "spec cleanup called in DARTS_MPSTATE_END")
						
						IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
							REQUEST_LEADERBOARD_CAM()
						ELIF IS_LEADERBOARD_CAM_READY(TRUE)
							REINIT_NET_TIMER(timeDartsAwardFailSafe)
							SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
							
							CDEBUG1LN(DEBUG_DARTS, "spec going to DARTS_MPSTATE_LEAVE")
							PlayerData[iSelfID].eMPState = DARTS_MPSTATE_LEAVE
						ENDIF
					ELSE
						START_NET_TIMER(timeDartsAwardFailSafe)
						CDEBUG1LN(DEBUG_DARTS, "started failsafe net timer")
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_LEAVE
					
					DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MG_DARTS, ServerData.iMatchHistoryID, missionScriptArgs.mdID.idVariation, 
											PICK_INT(MPDartGame.bDartWinner, 1, 0), PICK_INT(MPDartGame.bDartWinner, 1, 2))
					SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
					CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
					CDEBUG1LN(DEBUG_DARTS, "spec cleanup called in DARTS_MPSTATE_LEAVE")
				BREAK
				
			ENDSWITCH
		
//  ---------------------------------------------------------------------------------------------------------------------------------			

		ELSE
			// #NORMAL GAMEPLAY HERE VVVVVVV
			
			// If we have a match end event, bail
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			OR SHOULD_PLAYER_LEAVE_MP_MISSION(TRUE)
				
				IF PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END
				AND serverData.mpTurnStatus[iSelfID]  < DARTS_MPTURN_WINNING
				AND (MPDartGame.iGamesWon = 0)
				AND (MPDartGame.iGamesLost = 0)
					CDEBUG1LN(DEBUG_DARTS, "Also giving money back to the player in line 970")
					IF IS_DARTS_GAME_ALMOST_DONE(ServerData, PlayerData)
						INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
						iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
					ELSE
						iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED_TIED()
					ENDIF
					CDEBUG1LN(DEBUG_DARTS, "iBetWinnings = ", iBetWinnings)
				
					IF RECEIVED_EVENT(EVENT_NETWORK_END_MATCH)
						bMinigamePlayerQuitFlag = TRUE
					ENDIF
				
				ENDIF
				
				CDEBUG1LN(DEBUG_DARTS, "SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE returned true, CLEANING UP DARTS")
				CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
			ENDIF
			
			//If the mission is being told to die
			IF SHOULD_PLAYER_LEAVE_MP_MISSION(TRUE)
				IF PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END
				AND serverData.mpTurnStatus[iSelfID]  < DARTS_MPTURN_WINNING
				AND (MPDartGame.iGamesWon = 0)
				AND (MPDartGame.iGamesLost = 0)
					CDEBUG1LN(DEBUG_DARTS, "Also giving money back to the player in line 970")
					
					IF IS_DARTS_GAME_ALMOST_DONE(ServerData, PlayerData)
						INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
						iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
					ELSE
						iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED_TIED()
					ENDIF
					CDEBUG1LN(DEBUG_DARTS, "iBetWinnings = ", iBetWinnings)
				ENDIF
				
				CDEBUG1LN(DEBUG_DARTS, "SHOULD_PLAYER_LEAVE_MP_MISSION returned true, CLEANING UP DARTS")
				CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
			ENDIF
			
			
			IF IS_PED_INJURED(PLAYER_PED_ID())
				CDEBUG1LN(DEBUG_DARTS, "IS_PED_INJURED(PLAYER_PED_ID()) returned true, CLEANING UP DARTS")
				CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
			ENDIF
			
			IF (	(iNumberOfPlayers < 2
					AND PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END)
				OR (PlayerData[iOtherID].eMPState > DARTS_MPSTATE_WAIT_POST_GAME
					AND PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END)	)
			AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
				
				IF PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END
				OR PlayerData[iSelfID].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
					CDEBUG1LN(DEBUG_DARTS, "game had already ended, setting terminate reason to ENDGAME")
					terminateReason = DARTS_MPTERMINATEREASON_ENDGAME
				ELSE
					CDEBUG1LN(DEBUG_DARTS, "game was still in play, setting terminate reason to PLAYERLEFT")
					terminateReason = DARTS_MPTERMINATEREASON_PLAYERLEFT
				
					IF IS_DARTS_GAME_ALMOST_DONE(ServerData, PlayerData)
						INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
						iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
					ELSE
						iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED_TIED()
					ENDIF
					CDEBUG1LN(DEBUG_DARTS, "iBetWinnings in 1812 is ", iBetWinnings)
				ENDIF
				
				BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE)
				CDEBUG1LN(DEBUG_DARTS, "NETWORK_GET_NUM_PLAYERS() < 2 returned true, CLEANING UP DARTS")
				CDEBUG1LN(DEBUG_DARTS, "NETWORK_GET_NUM_PLAYERS() has returned: ", iNumberOfPlayers)
				CDEBUG1LN(DEBUG_DARTS, "AM_Darts: PlayerData[iOtherID].eMPState at this moment = ", PlayerData[iOtherID].eMPState)
				CDEBUG1LN(DEBUG_DARTS, "AM_Darts: PlayerData[iSelfID].eMPState at this moment = ", PlayerData[iSelfID].eMPState)
				PlayerData[iSelfID].eMPState = DARTS_MPSTATE_TERMINATE_SPLASH
			ELSE
				IF (GET_GAME_TIMER() % 10000) < 50
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts: NETWORK_GET_NUM_PLAYERS() = ", NETWORK_GET_NUM_PLAYERS(PlayerData, TRUE))
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts: PlayerData[iOtherID].eMPState  = ", PlayerData[iOtherID].eMPState)
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts: PlayerData[iSelfID].eMPState   = ", PlayerData[iSelfID].eMPState)
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts: host status   = ", NETWORK_IS_HOST_OF_THIS_SCRIPT())
				ENDIF
			ENDIF
			
			// checking for new spectators
			IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
			AND NETWORK_GET_NUM_PARTICIPANTS() = 2
				// listen for spectator flag
				IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
						CDEBUG1LN(DEBUG_DARTS, "A new challenger has been sensed")
						DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
					ENDIF
				ENDIF
			ELIF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
			AND NETWORK_GET_NUM_PARTICIPANTS() < 2
				CDEBUG1LN(DEBUG_DARTS, "New challenger left")
				DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sTest = "NETWORK_GET_NUM_PARTICIPANTS Num Players "
			sTest += CONVERT_INT_TO_STRING(NETWORK_GET_NUM_PARTICIPANTS())
			
			TEXT_LABEL_63 sTest2 = "NETWORK_GET_NUM_SCRIPT_PARTICIPANTS Num Players "
			sTest2 += CONVERT_INT_TO_STRING(NETWORK_GET_NUM_SCRIPT_PARTICIPANTS("AM_Darts", NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT()))
			
			DRAW_DEBUG_TEXT_2D(sTest, <<0.6, 0.25, 0>>)
			DRAW_DEBUG_TEXT_2D(sTest2, <<0.6, 0.28, 0>>)
			
			#ENDIF
			
	//		IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
	//			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	//				IF IS_PED_RAGDOLL(PLAYER_PED_ID())
	//					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	//						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	//					ENDIF
	//					BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE)
	//					CDEBUG1LN(DEBUG_DARTS, "ONE OF THE PEDS IS RAGDOLLING")
	//					CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
	//				ENDIF
	//			ENDIF
	//			
	//			IF DOES_ENTITY_EXIST(PlayerData[iSelfID].piOther)
	//				IF IS_PED_RAGDOLL(PlayerData[iSelfID].piOther)
	//					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	//						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	//					ENDIF
	//					
	//					BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE)
	//					CDEBUG1LN(DEBUG_DARTS, "OTHER PLAYER IS RAGDOLLING")
	//					CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
	//				ENDIF
	//			ELSE
	//				DEBUG_MESSAGE_PERIODIC("PlayerData[iSelfID].piOther does NOT exist.  what up with that?", iDebugThrottle)
	//			ENDIF
	//				
	//		ENDIF
			
			IF IS_BIT_SET(MPGlobals.DartsData.iDisplayState, 5)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
				PRINT_NOW("DARTS_ENEMIES", DEFAULT_GOD_TEXT_TIME, 0)
				BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE)
				CDEBUG1LN(DEBUG_DARTS, "ENEMIES IN THE HOUSE, CLEANING UP")
				CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
			ENDIF
			
			// Detecting quit flags thrown by server
			IF DARTS_PLAYER_PROCESS_SERVER_FLAGS(ServerData, PlayerData[iSelfID], localDartsTimer)
				
				IF IS_DARTS_GAME_ALMOST_DONE(ServerData, PlayerData)
					INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
					iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
				ELSE
					iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED_TIED()
				ENDIF
				CDEBUG1LN(DEBUG_DARTS, "iBetWinnings = ", iBetWinnings)
			ENDIF
			
			// Process Events thrown from other clients
			//DARTS_BROADCAST_PROCESS_EVENTS(Player[iSelfID])  // TODO: this isn't currently being used all that much... not much at all.  take out?
			
			// chalk scoreboard
			IF PlayerData[iSelfID].eMPState > DARTS_MPSTATE_SETUP
				DRAW_SCALEFORM_MOVIE_3D(DartsSB.siScoreBoard, DartsSB.vScoreBoardPos, DartsSB.vScoreBoardRot, DartsSB.vScoreBoardScale, DartsSB.vScoreBoardScale)
			ENDIF
			
			
			IF PlayerData[iSelfID].eMPState = DARTS_MPSTATE_READY_UP
				IF NOT IS_PED_INJURED(piOtherPlayer)
//					NETWORK_OVERRIDE_COORDS_AND_HEADING(piOtherPlayer, DartsPlayerPos.vOpponentPos, MPDartGame.dBoard.fBoardHeading)
				ENDIF
			ENDIF
			
			IF PlayerData[iSelfID].eMPState >= DARTS_MPSTATE_THROW_OFF
			AND PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END //DARTS_MPSTATE_TERMINATE_SPLASH
				IF DARTS_PLAYER_QUIT_CONTROL(PlayerData[iSelfID], DartsUI, iDebugThrottle)
					bMinigamePlayerQuitFlag = TRUE
					
					IF IS_DARTS_GAME_ALMOST_DONE(ServerData, PlayerData)
						INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
						iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
					ELSE
						iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED_TIED()
					ENDIF
					
					SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_QUIT, <<0.0, 0.0, 0.0>>, contentID)
					
					DartsUI.iTheirScore++
					PlayerData[iSelfID].eMPState = DARTS_MPSTATE_END
				ENDIF
			ENDIF
			
			SWITCH PlayerData[iSelfID].eMPState
				CASE DARTS_MPSTATE_WAIT_PRE_INIT
					IF ServerData.mpState > DARTS_MPSTATE_INIT
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							//TASK_SWAP_WEAPON(PLAYER_PED_ID(), FALSE)
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
							IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM 
								CDEBUG1LN(DEBUG_DARTS, "Darts Faded out at DARTS_MPSTATE_WAIT_PRE_INIT")
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								START_DARTS_GAME_FADE_OUT()
							ELSE
								CDEBUG1LN(DEBUG_DARTS, "Darts DID NOT Fade out at DARTS_MPSTATE_WAIT_PRE_INIT")
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
								bAlreadyInGangHouse = TRUE
							ENDIF
							
							CDEBUG1LN(DEBUG_DARTS, "Weapon should be holstered and control should be off")
							
							IF NOT IS_PLAYER_SCTV(PLAYER_ID())
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS  | NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)
							ENDIF
							
							PlayerData[iSelfID].eMPState = DARTS_MPSTATE_PRE_INIT
						ENDIF
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_PRE_INIT
					CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] has reached Client side PRE INIT")
					IF IS_SCREEN_FADED_OUT()
						
						IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM 
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_ENTITY_COORDS(PLAYER_PED_ID(), vInitLostHideoutPos)
								NEW_LOAD_SCENE_START(vInitLostHideoutPos, <<-3.4831, 0.0223, 60.6925>>, 20)
							ENDIF
							
							CDEBUG1LN(DEBUG_DARTS, "SET_ENTITY_COORDS and NETWORK_START_LOAD_SCENE done")
						ENDIF
						
						CDEBUG1LN(DEBUG_DARTS, "going to _pre_init_wait")
						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_PRE_INIT_WAIT
					
					ELSE
						
						INIT_BOARD(MPDartGame.dBoard, Args)
						GET_WORLD_OFFSETS(MPDartGame.dBoard, DartsPlayerPos, DartsInitReticlePos, TRUE)
						
						IF iSelfID = DART_PLAYER_AWAY_SERVER
							DartsPlayerPos.vDartPlayrPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(MPDartGame.dBoard.vDartBoard, 
																											MPDartGame.dBoard.fBoardHeading, 
																											<< -0.3, -2.6 + fDartBoardOriginOffset, -1.7272 >>)	
						ELSE
							DartsPlayerPos.vDartPlayrPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(MPDartGame.dBoard.vDartBoard, 
																											MPDartGame.dBoard.fBoardHeading, 
																											<< 0.3, -2.6 + fDartBoardOriginOffset, -1.7272 >>)
						ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							vPlayerCurrentPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
							
							CLEAR_SEQUENCE_TASK(siTemp)
							OPEN_SEQUENCE_TASK(siTemp)
								
								IF iSelfID = DART_PLAYER_AWAY_SERVER
									TASK_STAND_STILL(NULL, 500)
								ENDIF
								IF vPlayerCurrentPos.z > 75.2
									TASK_JUMP(NULL, FALSE)
								ENDIF
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, DartsPlayerPos.vDartPlayrPos, PEDMOVEBLENDRATIO_WALK, 5000, 0.3)
							//	TASK_GO_STRAIGHT_TO_COORD(NULL, DartsPlayerPos.vDartPlayrPos, PEDMOVEBLENDRATIO_WALK)
								TASK_ACHIEVE_HEADING(NULL, MPDartGame.dBoard.fBoardHeading)
								
							CLOSE_SEQUENCE_TASK(siTemp)
							TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siTemp)
						ENDIF
						
						CDEBUG1LN(DEBUG_DARTS, "Players tasked to get into position")
						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_PRE_INIT_WAIT
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_PRE_INIT_WAIT
					IF NOT bAlreadyInGangHouse
						IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM 
						AND NETWORK_UPDATE_LOAD_SCENE()
							CDEBUG1LN(DEBUG_DARTS, "NETWORK_UPDATE_LOAD_SCENE finished")
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							PlayerData[iSelfID].eMPState = DARTS_MPSTATE_INIT
							CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] has reached Client side PRE INIT WAIT")
						ELSE
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							PlayerData[iSelfID].eMPState = DARTS_MPSTATE_INIT
							CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] has reached Client side INIT")
						ENDIF
					
					ELIF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK //GET_PLAYER_DISTANCE_FROM_LOCATION(DartsPlayerPos.vDartPlayrPos) < 1.5
					AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_IN_POSITION)
						CDEBUG1LN(DEBUG_DARTS, "Darts player in place")
						DartPlayerPosition = DART_PLAYER_IN_POSITION
						DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_IN_POSITION)
					
					ELIF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_IN_POSITION)
					AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_IN_POSITION)
						CDEBUG1LN(DEBUG_DARTS, "Both Darts players in place, fading out")
					//	DO_SCREEN_FADE_OUT(500)
						
						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_INIT
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_INIT	
				//	IF (bAlreadyInGangHouse AND IS_SCREEN_FADED_OUT())
				//	OR NOT bAlreadyInGangHouse
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] has reached Client side INIT")
						INIT_BOARD(MPDartGame.dBoard, Args)
						
						INIT_CAM(MPDartGame.dBoard.vDartBoard, MPDartGame.dBoard.fBoardHeading)
						
						INT k
						REPEAT NUM_DART_TURNS k
							INIT_DARTS(MPDartGame.Darts[k], 0)//iSelfID)
							IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
								//INIT_DARTS(TutorialDarts[k], 0)
							ENDIF
						ENDREPEAT
						
						INIT_SPRITE_RETICLE()
						DARTS_INIT_SCOREBOARD(DartsSB, ENUM_TO_INT(DartsLocation), MPDartGame.dBoard.vDartBoard, MPDartGame.dBoard.fBoardHeading)
						REQUEST_STREAMS(DartsSB)
						REGISTER_SCRIPT_WITH_AUDIO()
						
						//Disabling UI
					//	SET_MP_OVERHEAD_STATS_ACTIVE(FALSE)
					//	DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, TRUE)
					//	DISABLE_RANK_AND_XP_BAR()
					//	DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, TRUE)
						DISABLE_ALL_MP_HUD()
						
						DISPLAY_RADAR(FALSE)
						DISABLE_CELLPHONE(TRUE)
						
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						
						iSteadyShotLength = 770 
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
								IF NOT IS_PLAYER_SCTV(PLAYER_ID())
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
									CDEBUG1LN(DEBUG_DARTS, "Had to turn player control off again in  DARTS_MPSTATE_INIT")
								ENDIF
							ENDIF
						ENDIF
						
						CDEBUG1LN(DEBUG_DARTS, "Finished Stage_init")
						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_STREAMING
				//	ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_STREAMING
					IF HAVE_STREAMS_LOADED(DartsSB)
						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_SETUP
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] Finished STREAMING")
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_SETUP
					CDEBUG1LN(DEBUG_DARTS, "Reached Client side SETUP")
					
					DARTS_SET_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_MATCH_MADE)
					
					// placing the players in their prospective positions
					IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM 
					OR NOT bAlreadyInGangHouse
						
						GET_WORLD_OFFSETS(MPDartGame.dBoard, DartsPlayerPos, DartsInitReticlePos, TRUE)
						CDEBUG1LN(DEBUG_DARTS, "GET_WORLD_OFFSETS obtained in DARTS_MPSTATE_SETUP")
						
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							DartsPlayerPos.vDartPlayrPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( MPDartGame.dBoard.vDartBoard, 
																											MPDartGame.dBoard.fBoardHeading, 
																											<< 0.5, -2.4 + fDartBoardOriginOffset, fBoard_offsetZ-1 >>)
							DartsPlayerPos.vOpponentPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( MPDartGame.dBoard.vDartBoard, 
																											MPDartGame.dBoard.fBoardHeading, 
																											<< -0.4, -2.4 + fDartBoardOriginOffset, fBoard_offsetZ >>)
						ELSE
							DartsPlayerPos.vDartPlayrPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( MPDartGame.dBoard.vDartBoard, 
																											MPDartGame.dBoard.fBoardHeading,
																											<< -0.4, -2.4 + fDartBoardOriginOffset, fBoard_offsetZ-1 >>)	
							DartsPlayerPos.vOpponentPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( MPDartGame.dBoard.vDartBoard, 
																											MPDartGame.dBoard.fBoardHeading,
																											<< 0.5, -2.4 + fDartBoardOriginOffset, fBoard_offsetZ >>)
						ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							
							CDEBUG1LN(DEBUG_DARTS, "my player pos = ", DartsPlayerPos.vDartPlayrPos)
							CDEBUG1LN(DEBUG_DARTS, "oponent   pos = ", DartsPlayerPos.vOpponentPos)
							
							SET_ENTITY_COORDS(PLAYER_PED_ID(), DartsPlayerPos.vDartPlayrPos)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), MPDartGame.dBoard.fBoardHeading)
							IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
								ENDIF
							ENDIF
							
							IF GET_PLAYER_CURRENT_HELD_WEAPON() <> WEAPONTYPE_UNARMED
								//TASK_SWAP_WEAPON(PLAYER_PED_ID(), FALSE)
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
								CDEBUG1LN(DEBUG_DARTS, "Had to holster weapon again in  DARTS_MPSTATE_SETUP")
							ENDIF
							
							IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
								IF NOT IS_PLAYER_SCTV(PLAYER_ID())
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
									CDEBUG1LN(DEBUG_DARTS, "Had to turn player control off again in  DARTS_MPSTATE_SETUP")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					oiReticle = CREATE_OBJECT(modelReticleRegular, DartsInitReticlePos.vInitReticlePos, FALSE, FALSE)
					
					oiDartChallenge = CREATE_OBJECT(PROP_DART_1, DartsInitReticlePos.vInitReticlePos, FALSE, FALSE)
					
					// set minigame control scaleforms
					DARTS_ACTIVATE_QUIT_UI(DartsUI.uiQuitControls)
					DARTS_ACTIVATE_ENDGAME_UI(DartsUI.uiEndGameControls)
					
					sDartsPlayerNames[PlayerData[iSelfID].iOffsetPlayerID] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSelfID)))
					DartsGamerHandle[PlayerData[iSelfID].iOffsetPlayerID] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSelfID)))
					sCrewTags[PlayerData[iSelfID].iOffsetPlayerID] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[PlayerData[iSelfID].iOffsetPlayerID])
					
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						
						sDartsPlayerNames[PlayerData[iOtherID].iOffsetPlayerID] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
						DartsGamerHandle[PlayerData[iOtherID].iOffsetPlayerID] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
						sCrewTags[PlayerData[iOtherID].iOffsetPlayerID] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[PlayerData[iOtherID].iOffsetPlayerID])
						
						piOtherPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
						
						IF NOT IS_PED_INJURED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID))))
							piPlayerClones[PlayerData[iOtherID].iOffsetPlayerID] = CLONE_PED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID))), FALSE, FALSE)
						ENDIF
						
						IF NOT IS_PED_INJURED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSelfID))))
							piPlayerClones[PlayerData[iSelfID].iOffsetPlayerID] = CLONE_PED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSelfID))), FALSE, FALSE)
						ENDIF
						
						IF IS_PED_WEARING_HIGH_HEELS(piPlayerClones[PlayerData[iSelfID].iOffsetPlayerID])
							CDEBUG1LN(DEBUG_DARTS, "clone self ped is wearing high heels, taking them off")
							DARTS_TAKE_OFF_HIGH_HEELS(piPlayerClones[PlayerData[iSelfID].iOffsetPlayerID])
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "clone self ped is not wearing high heels")
						ENDIF
						
						IF IS_PED_WEARING_HIGH_HEELS(piPlayerClones[PlayerData[iOtherID].iOffsetPlayerID])
							CDEBUG1LN(DEBUG_DARTS, "clone other ped is wearing high heels, taking them off")
							DARTS_TAKE_OFF_HIGH_HEELS(piPlayerClones[PlayerData[iOtherID].iOffsetPlayerID])
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "clone other ped is not wearing high heels")
						ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1991.1589, 3045.9758, 46.2151>>) // coord out of camera view, since clones are now being used
							IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
								SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
							ENDIF
						ENDIF
						
						INT iTemp1, iTemp2
						iTemp1 = ENUM_TO_INT(DART_PLAYER_AWAY)
						iTemp2 = ENUM_TO_INT(DART_PLAYER_HOME)
						
						CDEBUG1LN(DEBUG_DARTS, "iTemp1 = ", iTemp1, " iTemp2 = ", iTemp2)
						CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iTemp1, " = ", sCrewTags[iTemp1])
						CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iTemp2, " = ", sCrewTags[iTemp2])
						
						DARTS_ADD_NAMES(DartsSB, sDartsPlayerNames[iTemp1], sDartsPlayerNames[iTemp2])
						IF NOT IS_STRING_EMPTY(sCrewTags[iTemp1])
							DARTS_ADD_SB_CREW_TAG(DartsSB, iTemp1, sCrewTags[iTemp1])
						ENDIF
						IF NOT IS_STRING_EMPTY(sCrewTags[iTemp2])
							DARTS_ADD_SB_CREW_TAG(DartsSB, iTemp2, sCrewTags[iTemp2])
						ENDIF
						DARTS_CLEAR_SCORES(DartsSB, iTemp1)
						DARTS_CLEAR_SCORES(DartsSB, iTemp2)
						
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", PlayerData[iSelfID].iOffsetPlayerID, " = ", sCrewTags[PlayerData[iSelfID].iOffsetPlayerID])
						DARTS_ADD_NAMES(DartsSB, "", sDartsPlayerNames[PlayerData[iSelfID].iOffsetPlayerID])
						IF NOT IS_STRING_EMPTY(sCrewTags[PlayerData[iSelfID].iOffsetPlayerID])
							DARTS_ADD_SB_CREW_TAG(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME), sCrewTags[PlayerData[iSelfID].iOffsetPlayerID])
						ENDIF
						DARTS_CLEAR_SCORES(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
					ENDIF
					
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts.sc: g_FMMC_STRUCT.iNumberOfLegs = ", g_FMMC_STRUCT.iNumberOfLegs)
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts.sc: g_FMMC_STRUCT.iNumberOfSets = ", g_FMMC_STRUCT.iNumberOfSets)
					
					IF g_FMMC_STRUCT.iNumberOfLegs > 0
					OR g_FMMC_STRUCT.iNumberOfSets > 0
						UPDATE_SB_SETS_LEGS(DartsSB, 0, 0, 0, 0)
					ENDIF
					
					IF bShowSplash
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_CUSTOM, "MN_DART")
						CDEBUG1LN(DEBUG_DARTS, "AM_Darts.sc: Big 'Darts' Message displayed")
						
						START_TIMER_NOW(controlTimer)
						
						ANOTHER_GAME_CAM_MP(PLAYER_PED_ID())
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
					
					CLEAR_AREA(Args.vDartBoard, 0.5, TRUE)
					REMOVE_DECALS_IN_RANGE(Args.vDartBoard, 0.5)
					
					SET_ON_JOB_INTRO(TRUE)
					
					CDEBUG1LN(DEBUG_DARTS, "Going from DARTS_MPSTATE_SETUP to DARTS_MPSTATE_READY_UP")
					PlayerData[iSelfID].eMPState = DARTS_MPSTATE_READY_UP
				BREAK
				
				CASE DARTS_MPSTATE_READY_UP
					IF (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT())
					AND GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 1.0
					AND bShowSplash
						CDEBUG1LN(DEBUG_DARTS, "[DARTS_MPSTATE_READY_UP] fade in called for")
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					IF ( NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_TUTORIAL)
						AND ((IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) AND GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 1.6)
							OR GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 20.0) )
					OR NOT bShowSplash
						
						IF bShowSplash
							CLEANUP_BIG_MESSAGE()
							CANCEL_TIMER(controlTimer)
						ENDIF
						
						//IF IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_MPTUTORIAL_SHOWN)
						//IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_DARTS_SEEN_TUTORIAL)
							DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_DONE)
							CDEBUG1LN(DEBUG_DARTS, "Tutorial has been seen, skipping it")
							CDEBUG1LN(DEBUG_DARTS, "tbh the tutorial will always get skipped")
						//ELSE
						//	CDEBUG1LN(DEBUG_DARTS, "Tutorial has not been seen yet")
						//ENDIF
						
						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_TUTORIAL
						DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_TUTORIAL)
					ELSE
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_TUTORIAL)
						AND bShowSplash
							UPDATE_CUSTOM_BIG_MESSAGE("MN_DART")
						ENDIF
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_TUTORIAL
					IF (DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_TUTORIAL) AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_AT_TUTORIAL))
					OR (DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_TUTORIAL) AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER))
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
						AND (NOT (DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_DONE) AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_TUTORIAL_DONE))
							AND NOT (DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_DONE) AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)))
							IF DARTS_TUTORIAL(DartsTutorialStage, MPDartGame.dBoard, AIBrain, 
											TurnStage, MPDartGame.Darts, TutorialTimer, PlayerData)
											//TurnStage, TutorialDarts, TutorialTimer, PlayerData)
							
								CDEBUG1LN(DEBUG_DARTS, "FINISHED TUTORIAL")
								//SET_BIT(g_iDartsTutorialFlags, gDARTS_MPTUTORIAL_SHOWN)
								SET_MP_BOOL_CHARACTER_STAT(MP_STAT_DARTS_SEEN_TUTORIAL, TRUE)
								CDEBUG1LN(DEBUG_DARTS, "Setting MP_STAT_DARTS_SEEN_TUTORIAL to true")
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_THROW_OFF_SETUP
									PlayerData[iSelfID].eMPState = DARTS_MPSTATE_INTRO_SYNC_SETUP
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]  setting FM Match Start just before the game starts")
									DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_DARTS, ServerData.iMatchHistoryID, ServerData.iMatchType)
									
									PlayerData[iSelfID].eMPState = DARTS_MPSTATE_SYNC_OBJECTS
								ENDIF
							ENDIF
							
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "TUTORIAL ALREADY SHOWN, SKIPPING")
							CLEAR_HELP()
							DARTS_GAME_CAM()
							IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
								//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_THROW_OFF_SETUP
								PlayerData[iSelfID].eMPState = DARTS_MPSTATE_INTRO_SYNC_SETUP
							ELSE
								CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]  setting FM Match Start just before the game starts")
								DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_DARTS, ServerData.iMatchHistoryID, ServerData.iMatchType)
								
								PlayerData[iSelfID].eMPState = DARTS_MPSTATE_SYNC_OBJECTS
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_INTRO_SYNC_SETUP
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
						CDEBUG1LN(DEBUG_DARTS, "doing fade in in intro sync setup")
					ELSE
						
						//piOpponentClone = CLONE_PED(piOtherPlayer, FALSE, FALSE)
						SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
						SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
						CDEBUG1LN(DEBUG_DARTS, "created clone")
						
						IF NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_AWAY])
						AND NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_HOME])
	//						iSceneId = CREATE_SYNCHRONIZED_SCENE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(DartsArgs.oiDartBoard, << 0.337842, -0.243051, -0.329731 >>), 
	//																		<< 0, 0, GET_ENTITY_HEADING(DartsArgs.oiDartBoard) - 3.783146>>)
							CDEBUG1LN(DEBUG_DARTS, "tasking sync scene")
							
							iSceneId = CREATE_SYNCHRONIZED_SCENE(<<1992.679, 3050.739, 47.660>>, << 0, 0, 54.000>>)
							camDartsSync = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
							PLAY_SYNCHRONIZED_CAM_ANIM(camDartsSync, iSceneId, "darts_ig_intro_alt1_cam", "mini@dartsintro_alt1")
							SET_CAM_ACTIVE(camDartsSync, TRUE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_AWAY], iSceneId, "mini@dartsintro_alt1", "darts_ig_intro_alt1_guy1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
							TASK_SYNCHRONIZED_SCENE (piPlayerClones[DART_PLAYER_HOME], iSceneId, "mini@dartsintro_alt1", "darts_ig_intro_alt1_guy2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
							TASK_PLAY_ANIM(piPlayerClones[DART_PLAYER_AWAY], "mini@dartsintro_alt1", "darts_ig_intro_alt1_guy1_face", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
							TASK_PLAY_ANIM(piPlayerClones[DART_PLAYER_HOME], "mini@dartsintro_alt1", "darts_ig_intro_alt1_guy2_face", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
							PLAY_SYNCHRONIZED_ENTITY_ANIM( oiDartChallenge, iSceneId, "darts_ig_intro_alt1_dart","mini@dartsintro_alt1" , INSTANT_BLEND_IN )
							
							CDEBUG1LN(DEBUG_DARTS, "going to intro sync play")
							PlayerData[iSelfID].eMPState = DARTS_MPSTATE_INTRO_SYNC_PLAY
						ENDIF
					ENDIF
					
				BREAK
				
				CASE DARTS_MPSTATE_INTRO_SYNC_PLAY
					
					SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
					SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) = 1.0
						
						CDEBUG1LN(DEBUG_DARTS, "sync scene is done")
						
						IF NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_AWAY])
						AND NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_HOME])
						
							DARTS_GAME_CAM()
							
							DESTROY_CAM(camDartsSync)
							CLEAR_PED_TASKS (piPlayerClones[DART_PLAYER_AWAY])
							CLEAR_PED_TASKS (piPlayerClones[DART_PLAYER_HOME])
							
							DELETE_OBJECT(oiDartChallenge)
							
							PlayerData[iSelfID].eMPState = DARTS_MPSTATE_THROW_OFF_SETUP
							iSplashStage = 0
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "sync scene is still running")
					ENDIF
					
				BREAK
				
				CASE DARTS_MPSTATE_THROW_OFF_SETUP
					IF NOT IS_TIMER_STARTED(controlTimer)
						
						SET_SHARD_BIG_MESSAGE(shardBigMessage, "DARTS_THRW_OFF", "DARTS_THRW_STR", 3000)
						
						START_TIMER_NOW(controlTimer)
						
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ENDIF
					ELIF GET_TIMER_IN_SECONDS_SAFE(controlTimer) >= 3.5
					//OR (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) 
					//	AND GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 1.6)
					//OR NOT UPDATE_SCALEFORM_BIG_MESSAGE(DartsUI.siBigMessage, TRUE)
					OR NOT UPDATE_SHARD_BIG_MESSAGE(shardBigMessage)
						
						CLEANUP_BIG_MESSAGE()
						CANCEL_TIMER(controlTimer)
						
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]  setting FM Match Start just before the game starts")
						DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_DARTS, ServerData.iMatchHistoryID, ServerData.iMatchType)
						
						UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
						
						DARTS_INIT_TIMERS(localDartsTimer)
						
						RESET_SHARD_BIG_MESSAGE(shardBigMessage)
						
						DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_THROWOFF)
						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_THROW_OFF
					ENDIF
				BREAK
				
				// #THROWOFF GAMEPLAY HERE VVVVVVV
				
				CASE DARTS_MPSTATE_THROW_OFF
					
					IF DARTS_SERVER_CHECK_FLAG(ServerData, DARTSERVERFLAG_DO_THROWOFF)
						
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ENDIF
						
	//					IF DARTS_PLAYER_QUIT_CONTROL(PlayerData[iSelfID], DartsUI, iDebugThrottle)
	//						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_END
	//					ENDIF
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
							IF NOT (serverData.mpTurnStatus[iSelfID] = DARTS_MPTURN_WINNING)
							AND ( NOT playerData[iSelfID].bDartThrown = TRUE
								OR NOT playerData[iOtherID].bDartThrown = TRUE )
							AND NOT DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
								IF NOT bUITextDisplayed
									IF NOT (serverData.mpTurnStatus[iSelfID] = DARTS_MPTURN_SCORE_CHECK)
										IF bShowShotClockThrowHelp
										AND NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLOCK_HELP)
											IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gSHOT_CLOCK_HELP_SHOWN)
												PRINT_HELP("DARTS_TIOT")
												SET_BIT(g_iDartsTutorialFlags, gSHOT_CLOCK_HELP_SHOWN)
											ENDIF
											DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLOCK_HELP, TRUE)
										ENDIF
									ENDIF
									DartsUI.bInstructTextTransIn = TRUE
								ELSE
									IF DartsUI.bInstructTextTransIn
										CLEAR_HELP()
										DartsUI.bInstructTextTransIn = FALSE
									ENDIF
								ENDIF
							ENDIF
							
							// Drawing the instuctional button UI
							IF ((playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_AIM) 
									AND (serverData.mpTurnStatus[iSelfID] = DARTS_MPTURN_THROWING))
							OR bWaitUIShow
								DARTS_MP_THROWOFF_CONTROLS_UI(DartsUI, (serverData.mpTurnStatus[iSelfID] = DARTS_MPTURN_THROWING))
							ENDIF
						ENDIF
						
						// Do everyting playing related right chea
						SWITCH serverData.mpTurnStatus[iSelfID] 
							CASE DARTS_MPTURN_THROWING
								SWITCH playerData[iSelfID].eMPThrowStage
									CASE DARTS_MPTHROW_AIM
										
										//DARTS_GAME_HELP(ServerData.iServerTurn, TRUE)
										
	//									IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLECK_HELP)
	//										IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_CLOCK_HELP_SHOWN)
	//											PRINT_HELP("DARTS_CLOCK")
	//											SET_BIT(g_iDartsTutorialFlags, gDARTS_CLOCK_HELP_SHOWN)
	//										ENDIF
	//										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLECK_HELP, TRUE)
	//									ENDIF
										
										IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_AIM_TUT_SHOWN)
											PRINT_HELP("DARTS_AIM_HLP")
											SET_BIT(g_iDartsTutorialFlags, gDARTS_AIM_TUT_SHOWN)
										ELIF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_AIM_HLP")
										AND NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_CLOCK_HELP_SHOWN)
											PRINT_HELP("DARTS_CLOCK")
											SET_BIT(g_iDartsTutorialFlags, gDARTS_CLOCK_HELP_SHOWN)
										ENDIF
										
										IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
											DARTS_DRAW_THROW_OFF_CLOCK(ServerData.DartsTimer, TRUE)
										ENDIF
										
										IF ServerData.DartsTimer.bNewShot
											CDEBUG1LN(DEBUG_DARTS, "Server timer has sensed a new shot!")
										ENDIF
										
										IF ServerData.DartsTimer.bTimeRunOut
											CDEBUG1LN(DEBUG_DARTS, "Server timer has sensed that it has run out!")
										ENDIF
										
										IF (NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
											AND NOT IS_PAUSE_MENU_ACTIVE())
										OR ServerData.DartsTimer.bTimeRunOut
											IF DO_AIMING(MPDartGame.Darts[serverData.iServerTurn], MPDartGame.dBoard,
														DartsInitReticlePos, FALSE, ServerData.DartsTimer.bTimeRunOut, TRUE)
												CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] current client finished AIMING")
												
												IF ServerData.DartsTimer.bTimeRunOut
												AND NOT bShowShotClockThrowHelp
													bShowShotClockThrowHelp = TRUE
												ENDIF
												
												DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
												
												// copy dart's throw info to the client so that other player can grab it
												CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] Copy the throwers data for dart ", 0)
												COPY_DART_TO_CLIENT(playerData[iSelfID], MPDartGame.Darts[serverData.iServerTurn])
												
												IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
													DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												ENDIF
												
												IF MPGlobals.DartsData.iDangerPedCount <= 0
												OR NOT IS_BIT_SET(MPGlobals.DartsData.iDisplayState, 2)
													DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
													CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] no peds in danger zone, clearing danger flag")
												ELSE
													DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
													CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] setting danger flag, peds in danger zone: ", MPGlobals.DartsData.iDangerPedCount)
												ENDIF
												
												playerData[iSelfID].bDartThrown = TRUE
												playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_THROW
											ENDIF
										ENDIF
									BREAK
									
									CASE DARTS_MPTHROW_THROW
										DEBUG_MESSAGE_PERIODIC("[AM_DARTS] current client is THROWING", iDebugThrottle)
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
											IF THROW_DART(MPDartGame.Darts[serverData.iServerTurn], MPDartGame.dBoard)
												CDEBUG1LN(DEBUG_DARTS, "Regular Throw completed")
												playerData[iSelfID].bResetDone = FALSE
												playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_SCORE
											ENDIF
										ELSE
											IF NOT IS_TIMER_STARTED(ReThrowTimer)
												IF THROW_DART_AT_ENTITY(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard, << 988.1219, -99.3023, 73.8456 >>)
													BROADCAST_DANGER_ZONE_UPDATE(PLAYER_ID(), DARTS_DANGERZONE_DART_THROW)
													CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed")
													
													playerData[iSelfID].bDartThrown = FALSE
													playerData[iSelfID].bResetDone = FALSE
													
													RESTART_TIMER_NOW(ReThrowTimer)
												ENDIF
											
											ELIF GET_TIMER_IN_SECONDS(ReThrowTimer) > 1.0
											OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												
												IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
													DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												ENDIF
												
												CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed, going back to DARTS_MPTHROW_AIM - THROW SIDE")
												CANCEL_TIMER(ReThrowTimer)
												playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_AIM
											ENDIF
										ENDIF
									BREAK
									
									CASE DARTS_MPTHROW_SCORE
										//IF PlayerData[iOtherID].eMPWaitStage >= DARTS_MPWAIT_SCORE
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is SCORING")
											
											SCORE_DART(MPDartGame.Darts[serverData.iServerTurn], MPDartGame.dBoard)
											playerData[iSelfID].ClientDart.fLength = MPDartGame.Darts[serverData.iServerTurn].fLength
											
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] THROW - Client Dart ", ServerData.iServerTurn, " fLength = ", playerData[iSelfID].ClientDart.fLength)
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] THROW - Local Dart ", ServerData.iServerTurn, " fLength = ", MPDartGame.Darts[ServerData.iServerTurn].fLength)
											playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_TURN_CHANGE
										//ENDIF
										DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to finish throwing", iDebugThrottle)
									BREAK
									
									CASE DARTS_MPTHROW_TURN_CHANGE	
										IF PlayerData[iOtherID].eMPWaitStage >= DARTS_MPWAIT_TURN_CHANGE
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is THROWING - TURN CHANGE")
											
											playerData[iSelfID].bDartThrown = FALSE
											
											DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
											
											IF NOT MPDartGame.Darts[serverData.iServerTurn].bDoneScoring
												MPDartGame.Darts[serverData.iServerTurn].bDoneScoring = TRUE
											ENDIF
											SETTIMERA(0)
										ENDIF
										DEBUG_MESSAGE_PERIODIC("Waiting for other client to get to DARTS_MPWAIT_TURN_CHANGE", iDebugThrottle)
									BREAK
								ENDSWITCH
							BREAK
							
							CASE DARTS_MPTURN_WAITING
								SWITCH playerData[iSelfID].eMPWaitStage
									CASE DARTS_MPWAIT_AIM
										
										IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
											DARTS_DRAW_THROW_OFF_CLOCK(ServerData.DartsTimer, FALSE)
										ENDIF
										
										IF NOT bWaitUIShow
											bWaitUIShow = TRUE
										ENDIF
										
										IF playerData[iOtherID].bDartThrown
											CDEBUG1LN(DEBUG_DARTS, "Current client finished WAIT - AIMING")
											
											DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
											
											// grab other players dart throw info so we can throw that thang
											COPY_DART_TO_LOCAL(MPDartGame.Darts[serverData.iServerTurn], playerData[iOtherID], iOtherID)
											
											IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											ENDIF
											
											playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_THROW
										ELSE
											DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to throw", iDebugThrottle)
										ENDIF
									BREAK
									
									CASE DARTS_MPWAIT_THROW		
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - Throwing")
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_DANGER_DART)
											IF THROW_DART(MPDartGame.Darts[serverData.iServerTurn], MPDartGame.dBoard)
												CDEBUG1LN(DEBUG_DARTS, "Regular Throw Completed - Wait side")
												playerData[iSelfID].bResetDone = FALSE
												playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_SCORE
											ENDIF
										ELSE
											IF NOT IS_TIMER_STARTED(ReThrowTimer)
												IF THROW_DART_AT_ENTITY(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard, << 988.1219, -99.3023, 73.8456 >>)
													CDEBUG1LN(DEBUG_DARTS, "Danger Dart Completed - WAIT SIDE")
													RESTART_TIMER_NOW(ReThrowTimer)
												ENDIF
											
											ELIF GET_TIMER_IN_SECONDS(ReThrowTimer) > 1.0
											OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												
												IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
													DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												ENDIF
												
												CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed, going back to DARTS_MPTHROW_AIM - WAIT SIDE")
												CANCEL_TIMER(RethrowTimer)
												playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_AIM
											ENDIF
										ENDIF
									BREAK
									
									CASE DARTS_MPWAIT_SCORE
										IF PlayerData[iOtherID].eMPThrowStage >= DARTS_MPTHROW_SCORE
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - SCORING")
											SCORE_DART(MPDartGame.Darts[serverData.iServerTurn], MPDartGame.dBoard)
											playerData[iSelfID].ClientDart.fLength = MPDartGame.Darts[serverData.iServerTurn].fLength
											
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] WAIT - Client Dart ", ServerData.iServerTurn, " fLength = ", playerData[iSelfID].ClientDart.fLength)
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] WAIT - Local Dart ", ServerData.iServerTurn, " fLength = ", MPDartGame.Darts[serverData.iServerTurn].fLength)
											playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_TURN_CHANGE
										ENDIF
										DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to finish throwing", iDebugThrottle)
									BREAK
									
									CASE DARTS_MPWAIT_TURN_CHANGE
										IF PlayerData[iOtherID].eMPThrowStage >= DARTS_MPTHROW_TURN_CHANGE
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - TURN CHANGE")
											
											DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
											
											IF NOT MPDartGame.Darts[serverData.iServerTurn].bDoneScoring
												MPDartGame.Darts[serverData.iServerTurn].bDoneScoring = TRUE
											ENDIF
											SETTIMERA(0)
										ENDIF
										DEBUG_MESSAGE_PERIODIC("Waiting for other client to get to DARTS_MPTHROW_TURN_CHANGE", iDebugThrottle)
									BREAK
								ENDSWITCH
							BREAK
							
							CASE DARTS_MPTURN_SCORE_CHECK
								
								IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED)
									IF iSelfID = serverData.iThrower
										// print something?
									ELSE
										// print something else?
									ENDIF
									DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, TRUE)
								ENDIF
								
								IF NOT playerData[iSelfID].bResetDone 
								AND ( TIMERA() > 1000 )
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is in SCORE CHECK")
									playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_AIM
									playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_AIM
									MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring = FALSE
									DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
									
									CLEAR_HELP()
									
									IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
										
										// sending the darts back to the player, resetting them
										// serves the same functioon as RESET_DART without deleting the object
										INT y
										REPEAT NUM_MP_DART_TURNS y
											//RESET_DART(MPDartGame.Darts[y])
											MPDartGame.Darts[y].bDoneScoring = FALSE
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", y, " was reset")
										ENDREPEAT
										
										IF iSelfID != serverData.iThrower
											bWaitUIShow = FALSE
										ENDIF
										
										UPDATE_HIGHLIGHT(DartsSB, playerData[serverData.iWaiter].iOffsetPlayerID)
										
										iSteadyShotsUsed = 0
									ENDIF
									
									playerData[iSelfID].bResetDone = TRUE
								ENDIF
							BREAK
							
							CASE DARTS_MPTURN_WINNING
								
								IF NOT IS_TIMER_STARTED(controlTimer)
									
									CDEBUG1LN(DEBUG_DARTS, "********** serverData.iWinner = ", serverData.iWinner)
									CLEAR_PRINTS()
									CLEAR_HELP()
									
									IF (serverData.iWinner = PlayerData[iSelfID].iOffsetPlayerID)
										
										sShardPlayerName = "<C>"
										sShardPlayerName += "~HUD_COLOUR_BLUE~"
										sShardPlayerName += sDartsPlayerNames[PlayerData[iSelfID].iOffsetPlayerID]
										sShardPlayerName += "</C>"
										sShardPlayerName += "~s~"
										
										SET_SHARD_BIG_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(shardBigMessage, "DARTS_THRW_WIN", "DARTS_FIRST", sShardPlayerName, 3000, SHARD_MESSAGE_CENTERED, HUD_COLOUR_WHITE)
										
										//SET_SCALEFORM_BIG_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(DartsUI.siBigMessage, "DARTS_THRW_WIN", "DARTS_FIRST", sDartsPlayerNames[PlayerData[iSelfID].iOffsetPlayerID], 5000, 
										//														default, default, TRUE, MG_BIG_MESSAGE_ANIM_TIME_SLOW)
										PLAY_SOUND_FRONTEND(-1, "WIN", "HUD_AWARDS")
									ELSE
										
										sShardPlayerName = "<C>"
										sShardPlayerName += "~HUD_COLOUR_RED~"
										sShardPlayerName += sDartsPlayerNames[PlayerData[iOtherID].iOffsetPlayerID]
										sShardPlayerName += "</C>"
										sShardPlayerName += "~s~"
										
										SET_SHARD_BIG_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(shardBigMessage, "DARTS_THRW_LOS", "DARTS_FIRST", sShardPlayerName, 3000, SHARD_MESSAGE_CENTERED, HUD_COLOUR_RED)
										
										//SET_SCALEFORM_BIG_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(DartsUI.siBigMessage, "DARTS_THRW_LOS", "DARTS_FIRST", sDartsPlayerNames[PlayerData[iOtherID].iOffsetPlayerID], 5000, 
										//														HUD_COLOUR_RED, default, TRUE, MG_BIG_MESSAGE_ANIM_TIME_SLOW)
										PLAY_SOUND_FRONTEND(-1, "LOSER", "HUD_AWARDS")
									ENDIF
									//SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_CUSTOM, "DARTS_THRW_OFF", "DARTS_THRW_STR")
									SET_CAM_ACTIVE(camDartGame, TRUE)
									START_TIMER_NOW(controlTimer)
									
								ELIF GET_TIMER_IN_SECONDS_SAFE(controlTimer) >= 5.0
								//OR NOT UPDATE_SCALEFORM_BIG_MESSAGE(DartsUI.siBigMessage, TRUE)
								OR NOT UPDATE_SHARD_BIG_MESSAGE(shardBigMessage, TRUE)
									
									INT y
									REPEAT NUM_MP_DART_TURNS y
										RESET_DART(MPDartGame.Darts[y])
										MPDartGame.Darts[y].bDoneScoring = FALSE
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", y, " was reset")
									ENDREPEAT
									
									CANCEL_TIMER(controlTimer)
									
									RESET_SHARD_BIG_MESSAGE(shardBigMessage)
									
									DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_THROWOFF)
									PlayerData[iSelfID].eMPState = DARTS_MPSTATE_SYNC_OBJECTS
								ENDIF
								
	//							IF NOT MPGlobals.DartsData.bResultsDisplayed
	//								MPGlobals.DartsData.bResultsDisplayed = TRUE
	//							ENDIF
	//							
	//							MPDartGame.bDartWinner = (serverData.iWinner = iSelfID)
								
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_SYNC_OBJECTS
					CDEBUG1LN(DEBUG_DARTS, "Client DARTS_MPSTATE_SYNC_OBJECTS")
					i = 0
					j = 0
					REPEAT NUM_MP_DART_PLAYERS i
						MPDartGame.iThrowsTaken[i] 	= 0
						MPDartGame.iBullsEyesHit[i]	= 0
						DARTS_CLEAR_SCORES(DartsSB, i)
					ENDREPEAT
					
					DARTS_ADD_SCORE(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME), 301)
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						DARTS_ADD_SCORE(DartsSB, ENUM_TO_INT(DART_PLAYER_AWAY), 301)
					ENDIF
					
					REPEAT  NUM_MP_DART_TURNS j
						RESET_DART(MPDartGame.Darts[j])
						MPDartGame.Darts[j].bDoneScoring = FALSE
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", j, " was reset")
					ENDREPEAT
					
					RESET_CLIENT_DART(PlayerData[iSelfID])
					
					DARTS_INIT_TIMERS(localDartsTimer)
					
					playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_AIM
					playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_AIM
					playerData[iSelfID].bDartThrown = FALSE
					
					MPDartGame.bEndGameMessageDisplayed = FALSE
					MPDartGame.bWinNoise = FALSE
					
					bInWinRange = FALSE
					
//					CLEAR_AREA(Args.vDartBoard, 0.5, TRUE)
//					REMOVE_DECALS_IN_RANGE(Args.vDartBoard, 0.5)
					
					iSteadyShotsUsed = 0
					
					bSteadyShotHelp = FALSE
					
					DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_FIRST_THROW, FALSE)
					
					SET_ON_JOB_INTRO(FALSE)
					
					DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SYNC_COMPLETE)
					PlayerData[iSelfID].eMPState = DARTS_MPSTATE_SETUP_POST
				BREAK
				
				CASE DARTS_MPSTATE_SETUP_POST
					DEBUG_MESSAGE_PERIODIC("Client DARTS_MPSTATE_SETUP_POST", iDebugThrottle)
					IF ServerData.mpState = DARTS_MPSTATE_IN_PROGRESS
						PlayerData[iSelfID].bDoRematch = FALSE
						
						// TODO: implement the cam interpolation
	//					IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
	//						DO_SCREEN_FADE_IN(500)
	//					ENDIF
						
						DARTS_GAME_CAM()
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						
						IF SHOULD_DARTS_LEVEL_HELP_SHOW(GET_MP_INT_CHARACTER_STAT(MP_STAT_CRDARTS), iLevelBit)
							CDEBUG1LN(DEBUG_DARTS, "show the level up prompt")
							DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOW_LEVEL_UP, TRUE)
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "no level up prompt to show")
						ENDIF
						
						fScaledWobble = GET_MP_WOBBLE_LEVEL(GET_MP_INT_CHARACTER_STAT(MP_STAT_CRDARTS)) //0.076
						fScaledRealWobble = GET_MP_REAL_WOBBLE_LEVEL(GET_MP_INT_CHARACTER_STAT(MP_STAT_CRDARTS)) //0.1015
						
						CDEBUG1LN(DEBUG_DARTS, "total darts games  = ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CRDARTS))
						CDEBUG1LN(DEBUG_DARTS, "scaled wobble      = ", fScaledWobble)
						CDEBUG1LN(DEBUG_DARTS, "scaled real wobble = ", fScaledRealWobble)
						
						fWobbleFactor = fScaledWobble
						fRealWobFactor = fScaledRealWobble
						
						DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SYNC_COMPLETE)
						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_IN_PROGRESS
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] Client is proceeding to IN PROGRESS")
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							DartsStats[DARTS_SCLB_STAT_NUM_MATCHES]++
						ENDIF
						
						DARTS_INIT_UI(DartsUI)
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							UPDATE_HIGHLIGHT(DartsSB, PICK_INT((serverData.iCoinFlip = 0), ENUM_TO_INT(DART_PLAYER_AWAY), ENUM_TO_INT(DART_PLAYER_HOME)))
						ELSE
							UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
						ENDIF
						bWaitUIShow = FALSE
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_WAIT_POST_MENU
					CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] has reached Client side WAIT POST MENU")
					// Sitting in a waiting state
					
					IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_MOVETOGAME)
						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_IN_PROGRESS
					ENDIF
				BREAK
				
				// #REALGAME HERE VVVVVVV				
				
				CASE DARTS_MPSTATE_IN_PROGRESS
					
					// failsafe for screen not fading back in
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
	//				IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_FIRST_THROW)
	//					PRINT_WITH_NAME("DARTS_FIRST", sDartsPlayerNames[PICK_INT((serverData.mpTurnStatus[iSelfID]=DARTS_MPTURN_THROWING), iSelfID, iOtherID)], DEFAULT_GOD_TEXT_TIME, 0)
	//					DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_FIRST_THROW, TRUE)
	//				ENDIF
					
	//				HANDLE_SWITCH_CAM()
					
					// making sure the dart players stay in position
					SWITCH DartPlayerPosition
						CASE DART_PLAYER_IN_POSITION
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							AND GET_PLAYER_DISTANCE_FROM_LOCATION(DartsPlayerPos.vDartPlayrPos) > 1.5
								
//							#IF IS_DEBUG_BUILD
//								CDEBUG1LN(DEBUG_DARTS, "GET_PLAYER_DISTANCE_FROM_LOCATION(DartsPlayerPos.vDartPlayrPos) = ", GET_PLAYER_DISTANCE_FROM_LOCATION(DartsPlayerPos.vDartPlayrPos))
//							#ENDIF
//								
//								CLEAR_SEQUENCE_TASK(siTemp)
//								OPEN_SEQUENCE_TASK(siTemp)
//									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, DartsPlayerPos.vDartPlayrPos, PEDMOVEBLENDRATIO_WALK, 5000, 0.3)
//									TASK_ACHIEVE_HEADING(NULL, MPDartGame.dBoard.fBoardHeading)
//								CLOSE_SEQUENCE_TASK(siTemp)
//								TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siTemp)
//								
//								CDEBUG1LN(DEBUG_DARTS, "Dart player out of position, tasked to move back")
//								
//								DartPlayerPosition = DART_PLAYER_OUT_POSITION
							ENDIF
						BREAK
						
						CASE DART_PLAYER_OUT_POSITION
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
								
//								CDEBUG1LN(DEBUG_DARTS, "Dart player is now back in position")
								
								DartPlayerPosition = DART_PLAYER_IN_POSITION
							ENDIF
						BREAK
					ENDSWITCH
					
	//				IF DARTS_PLAYER_QUIT_CONTROL(PlayerData[iSelfID], DartsUI, iDebugThrottle)
	//					PlayerData[iSelfID].eMPState = DARTS_MPSTATE_END
	//				ENDIF
					
					// Drawing the hud and the control scheme
					IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
						
						// help text drawing
						IF NOT (serverData.mpTurnStatus[iSelfID] = DARTS_MPTURN_WINNING)
						AND ( NOT playerData[iSelfID].bDartThrown = TRUE
							OR NOT playerData[iOtherID].bDartThrown = TRUE )
						AND NOT DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
									
							IF NOT bUITextDisplayed
								
								IF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_END_HELP_READY)
								AND serverData.mpTurnStatus[iSelfID] = DARTS_MPTURN_THROWING
									IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_WIN_HELP)
										IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gWIN_HELP_SHOWN)
											PRINT_HELP("DARTS_INSTR_W")
											SET_BIT(g_iDartsTutorialFlags, gWIN_HELP_SHOWN)
										ENDIF
										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_WIN_HELP, TRUE)
									ELIF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP)
									AND DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP_READY)
										IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gBUST_HELP_SHOWN)
											PRINT_HELP("DARTS_INSTR_B")
											SET_BIT(g_iDartsTutorialFlags, gBUST_HELP_SHOWN)
										ENDIF
										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP, TRUE)
									ENDIF
								ENDIF
								
								IF bInWinRange
								AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_SHT_USE")
								AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_FST_HLP")
								AND serverData.mpTurnStatus[iSelfID] = DARTS_MPTURN_THROWING
									
	//								IF (serverData.iScores[iSelfID] % 2) = 0
	//								AND (serverData.iScores[iSelfID] = 50 OR serverData.iScores[iSelfID] < 41)
	//								
	//									IF serverData.iScores[iSelfID] = 50
	//									ELSE
	//										CLEAR_HELP()
	//									ENDIF
	//								ENDIF
								
								ELIF NOT (serverData.mpTurnStatus[iSelfID] = DARTS_MPTURN_SCORE_CHECK)
									//DEBUG_MESSAGE_PERIODIC("DARTS UI IS SHOWING REGULAR HELP", iDebugThrottle)
									IF bShowShotClockThrowHelp //ServerData.DartsTimer.bTimeRunOut
									AND NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLOCK_HELP)
									AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_W")
									AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_B")
									AND (IS_TIMER_STARTED(ServerData.DartsTimer.stShotClock) AND GET_TIMER_IN_SECONDS_SAFE(ServerData.DartsTimer.stShotClock) < 5)
										IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gSHOT_CLOCK_HELP_SHOWN)
											PRINT_HELP("DARTS_TIOT")
											SET_BIT(g_iDartsTutorialFlags, gSHOT_CLOCK_HELP_SHOWN)
											CDEBUG1LN(DEBUG_DARTS, "shot clock help bit set here")
										ELSE
											CDEBUG1LN(DEBUG_DARTS, "shot clock help bit already set")
										ENDIF
										CDEBUG1LN(DEBUG_DARTS, "shot clock ui flag set")
										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLOCK_HELP, TRUE)
									ENDIF
								ENDIF
								DartsUI.bInstructTextTransIn = TRUE
							ELSE
								IF DartsUI.bInstructTextTransIn
									CLEAR_HELP()
									DartsUI.bInstructTextTransIn = FALSE
								ENDIF
							ENDIF
						ENDIF
						
						// Drawing the Darts Left UI element and the instructional buttons
						IF ((playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_AIM) 
							AND (serverData.mpTurnStatus[iSelfID] = DARTS_MPTURN_THROWING))
						OR bWaitUIShow
							IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
							AND NOT IS_TRANSITION_ACTIVE()
								DARTS_MP_CONTROLS_UI(DartsUI, (serverData.mpTurnStatus[iSelfID] = DARTS_MPTURN_THROWING))
							ENDIF
						ENDIF
						
						IF NOT IS_PAUSE_MENU_ACTIVE()
							DARTS_DISPLAY_SPECIAL_SPLASH_MP_SHARD(DartsUI)
							IF bPlaySplashFX
								PLAY_SOUND_FRONTEND ( -1, "GOLF_NEW_RECORD", "HUD_AWARDS")
								bPlaySplashFX = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					// Do everyting playing related right chea
					SWITCH serverData.mpTurnStatus[iSelfID] 
						CASE DARTS_MPTURN_THROWING
							SWITCH playerData[iSelfID].eMPThrowStage
								CASE DARTS_MPTHROW_AIM
									playerData[iSelfID].bDartAim = TRUE
	//								playerData[iSelfID].vReticlePos.x = spriteReticle.x
	//								playerData[iSelfID].vReticlePos.y = spriteReticle.y
									
									IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
										//DARTS_DRAW_PLAYERS_RETICLE(playerData[iSelfID].vReticlePos, iSelfID)
										DARTS_DRAW_SHOT_CLOCK(ServerData.DartsTimer, TRUE)
										//DRAW_DARTS_RETICLE_GUIDE(iSelfID, iSelfID)
									ENDIF
									
									DARTS_GAME_HELP(ServerData.iServerTurn, TRUE)
									
									IF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOW_LEVEL_UP)
										IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_SHT_USE")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_CLOCK")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_W")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_B")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_AIM_HLP")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_STD_HLP")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_FST_HLP")
											PRINT_HELP("DARTS_LEVEL")
											SET_BIT(g_savedGlobals.sDartsData.iDartTimePlayed, iLevelBit)
											DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOW_LEVEL_UP, FALSE)
										ENDIF
									ENDIF
									
									IF ServerData.DartsTimer.bNewShot
										CDEBUG1LN(DEBUG_DARTS, "Server timer has sensed a new shot!")
									ENDIF
									
									IF ServerData.DartsTimer.bTimeRunOut
										CDEBUG1LN(DEBUG_DARTS, "Server timer has sensed that it has run out!")
									ENDIF
									
									IF (NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
										AND NOT IS_PAUSE_MENU_ACTIVE()
										AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
										AND NOT IS_TRANSITION_ACTIVE())
									OR ServerData.DartsTimer.bTimeRunOut
										IF DO_AIMING(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard,
													DartsInitReticlePos, FALSE, ServerData.DartsTimer.bTimeRunOut)
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] current client finished AIMING")
											
											IF ServerData.DartsTimer.bTimeRunOut
											AND NOT bShowShotClockThrowHelp
												bShowShotClockThrowHelp = TRUE
											ENDIF
											
											DartsUI.iDartsLeft--
											
											DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
											
											// copy dart's throw info to the client so that other player can grab it
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] Copy the throwers data for dart ", ServerData.iServerTurn)
											COPY_DART_TO_CLIENT(playerData[iSelfID], MPDartGame.Darts[ServerData.iServerTurn])
											
											IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											ENDIF
											
											IF MPGlobals.DartsData.iDangerPedCount <= 0
											OR NOT IS_BIT_SET(MPGlobals.DartsData.iDisplayState, 2)
												DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
												CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] no peds in danger zone, clearing danger flag")
											ELSE
												DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
												CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] setting danger flag, peds in danger zone: ", MPGlobals.DartsData.iDangerPedCount)
											ENDIF
											
											IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
												DartsStats[DARTS_SCLB_STAT_DARTS_THROWN]++
											ENDIF
											
											playerData[iSelfID].bDartThrown = TRUE
											playerData[iSelfID].bDartAim = FALSE
											playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_THROW
										ENDIF
									ENDIF
								BREAK
								
								CASE DARTS_MPTHROW_THROW
									DEBUG_MESSAGE_PERIODIC("[AM_DARTS] current client is THROWING", iDebugThrottle)
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
										IF THROW_DART(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard)
											CDEBUG1LN(DEBUG_DARTS, "Regular Throw completed")
											MPDartGame.iThrowsTaken[PlayerData[iSelfID].iOffsetPlayerID]++
											playerData[iSelfID].bResetDone = FALSE
											playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_SCORE
										ENDIF
									ELSE
										IF NOT IS_TIMER_STARTED(ReThrowTimer)
											IF THROW_DART_AT_ENTITY(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard, << 988.1219, -99.3023, 73.8456 >>)
												BROADCAST_DANGER_ZONE_UPDATE(PLAYER_ID(), DARTS_DANGERZONE_DART_THROW)
												CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed")
												
												playerData[iSelfID].bDartThrown = FALSE
												playerData[iSelfID].bDartAim = TRUE
												playerData[iSelfID].bResetDone = FALSE
												
												RESTART_TIMER_NOW(ReThrowTimer)
											ENDIF
										
										ELIF GET_TIMER_IN_SECONDS(ReThrowTimer) > 1.0
										OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											
											IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											ENDIF
											
											CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed, going back to DARTS_MPTHROW_AIM - THROW SIDE")
											CANCEL_TIMER(ReThrowTimer)
											playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_AIM
										ENDIF
									ENDIF
								BREAK
								
								CASE DARTS_MPTHROW_SCORE
//									IF PlayerData[iOtherID].eMPWaitStage >= DARTS_MPWAIT_SCORE
//									OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is SCORING")
										
										//playerData[iSelfID].bDartThrown = FALSE
										
										SCORE_DART(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard)
										playerData[iSelfID].ClientDart.iHitValue = MPDartGame.Darts[ServerData.iServerTurn].iHitValue
										
										IF playerData[iSelfID].ClientDart.iHitValue = 50
											MPDartGame.iBullsEyesHit[PlayerData[iSelfID].iOffsetPlayerID]++
										ENDIF
										
										IF playerData[iSelfID].ClientDart.iHitMultiplier = 2 OR playerData[iSelfID].ClientDart.iHitValue = 50
											playerData[iSelfID].bDoubleThrow = TRUE
										ELSE
											playerData[iSelfID].bDoubleThrow = FALSE
										ENDIF
										
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] THROW - Client Dart ", ServerData.iServerTurn, " hit value = ", playerData[iSelfID].ClientDart.iHitValue)
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] THROW - Local Dart ", ServerData.iServerTurn, " hit value = ", MPDartGame.Darts[ServerData.iServerTurn].iHitValue)
										playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_TURN_CHANGE
//									ENDIF
									DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to finish throwing", iDebugThrottle)
								BREAK
								
								CASE DARTS_MPTHROW_TURN_CHANGE	
									IF PlayerData[iOtherID].eMPWaitStage >= DARTS_MPWAIT_TURN_CHANGE
									OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
										
										playerData[iSelfID].bDartThrown = FALSE
										
										DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
										
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is THROWING - TURN CHANGE")
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] ServerData.iServerTurn = ", ServerData.iServerTurn)
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] iServerTurn].bDoneScoring = ", MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring)
										
										
										IF NOT MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring
											MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring = TRUE
										ENDIF
										SETTIMERA(0)
									ENDIF
									DEBUG_MESSAGE_PERIODIC("Waiting for other client to get to DARTS_MPWAIT_TURN_CHANGE", iDebugThrottle)
								BREAK
							ENDSWITCH
						BREAK
						
						CASE DARTS_MPTURN_WAITING
							SWITCH playerData[iSelfID].eMPWaitStage
								CASE DARTS_MPWAIT_AIM
									
									// uncomment if you want to draw the reticle on both machines 
									/*
									IF playerData[iOtherID].bDartAim
										DARTS_DRAW_PLAYERS_RETICLE(playerData[iOtherID].vReticlePos, iOtherID, TRUE)
										DRAW_DARTS_RETICLE_GUIDE(iSelfID, iOtherID)
									ENDIF
									*/
									
									IF NOT bWaitUIShow
										bWaitUIShow = TRUE
									ENDIF
									
									IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
										DARTS_DRAW_SHOT_CLOCK(ServerData.DartsTimer, FALSE)
									ENDIF
									
									IF playerData[iOtherID].bDartThrown
										CDEBUG1LN(DEBUG_DARTS, "Current client finished WAIT - AIMING")
										
										DartsUI.iDartsLeft--
										
										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
										
										// grab other players dart throw info so we can throw that thang
										COPY_DART_TO_LOCAL(MPDartGame.Darts[ServerData.iServerTurn], playerData[iOtherID], iOtherID)
										
										IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
										ENDIF
										
										playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_THROW
									ELSE
										DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to throw", iDebugThrottle)
									ENDIF
								BREAK
								
								CASE DARTS_MPWAIT_THROW		
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - Throwing")
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_DANGER_DART)
										IF THROW_DART(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard)
											CDEBUG1LN(DEBUG_DARTS, "Regular Throw Completed - Wait side")
											MPDartGame.iThrowsTaken[PlayerData[iOtherID].iOffsetPlayerID]++
											playerData[iSelfID].bResetDone = FALSE
											playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_SCORE
										ENDIF
									ELSE
										IF NOT IS_TIMER_STARTED(ReThrowTimer)
											IF THROW_DART_AT_ENTITY(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard, << 988.1219, -99.3023, 73.8456 >>)
												CDEBUG1LN(DEBUG_DARTS, "Danger Dart Completed - WAIT SIDE")
												RESTART_TIMER_NOW(ReThrowTimer)
											ENDIF
										
										ELIF GET_TIMER_IN_SECONDS(ReThrowTimer) > 1.0
										OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											
											IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											ENDIF
											
											CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed, going back to DARTS_MPTHROW_AIM - WAIT SIDE")
											CANCEL_TIMER(RethrowTimer)
											playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_AIM
										ENDIF
									ENDIF
								BREAK
								
								CASE DARTS_MPWAIT_SCORE
									IF PlayerData[iOtherID].eMPThrowStage >= DARTS_MPTHROW_SCORE
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - SCORING")
										SCORE_DART(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard)
										playerData[iSelfID].ClientDart.iHitValue = MPDartGame.Darts[ServerData.iServerTurn].iHitValue
										
										IF playerData[iSelfID].ClientDart.iHitValue = 50
											MPDartGame.iBullsEyesHit[PlayerData[iOtherID].iOffsetPlayerID]++
										ENDIF
										
										IF playerData[iSelfID].ClientDart.iHitMultiplier = 2 OR playerData[iSelfID].ClientDart.iHitValue = 50
											playerData[iSelfID].bDoubleThrow = TRUE
										ELSE
											playerData[iSelfID].bDoubleThrow = FALSE
										ENDIF
										
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] WAIT - Client Dart ", ServerData.iServerTurn, " hit value = ", playerData[iSelfID].ClientDart.iHitValue)
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] WAIT - Local Dart ", ServerData.iServerTurn, " hit value = ", MPDartGame.Darts[ServerData.iServerTurn].iHitValue)
										playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_TURN_CHANGE
									ENDIF
									DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to finish throwing", iDebugThrottle)
								BREAK
								
								CASE DARTS_MPWAIT_TURN_CHANGE
									IF PlayerData[iOtherID].eMPThrowStage >= DARTS_MPTHROW_TURN_CHANGE
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - TURN CHANGE")
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] ServerData.iServerTurn = ", ServerData.iServerTurn)
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] iServerTurn].bDoneScoring = ", MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring)
										
										DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
										
										IF NOT MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring
											MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring = TRUE
										ENDIF
										SETTIMERA(0)
									ENDIF
									DEBUG_MESSAGE_PERIODIC("Waiting for other client to get to DARTS_MPTHROW_TURN_CHANGE", iDebugThrottle)
								BREAK
							ENDSWITCH
						BREAK
						
						CASE DARTS_MPTURN_SCORE_CHECK
							IF ((serverData.iScores[iSelfID] < 41 AND serverData.iScores[iSelfID] > 0) AND (serverData.iScores[iSelfID] % 2 = 0)) 
							OR serverData.iScores[iSelfID] = 50
								IF NOT bInWinRange //DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_IN_WIN_RANGE)
									CDEBUG1LN(DEBUG_DARTS, "DARTS UIFLAG IS SETTING DARTS_UIFLAGS_IN_WIN_RANGE")
									//DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_IN_WIN_RANGE, TRUE)
									bInWinRange = TRUE
								ELSE
									DEBUG_MESSAGE_PERIODIC("DARTS UIFLAG IS DARTS_UIFLAGS_IN_WIN_RANGE IS ALREADY SET", iDebugThrottle)
								ENDIF
							ENDIF
							
							IF serverData.iScores[iSelfID] <= 170
							AND NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_END_HELP_READY)
								DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_END_HELP_READY, TRUE)
							ENDIF
							
							IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP_READY)
							AND DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_WIN_HELP)
								DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP_READY, TRUE)
							ENDIF
							
							IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED)
								CLEAR_PRINTS()
								CLEAR_HELP()
								IF iSelfID = serverData.iThrower
									IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE)
										CDEBUG1LN(DEBUG_DARTS, "My server score is ", ServerData.iScores[iSelfID])
										CDEBUG1LN(DEBUG_DARTS, "Other server score is ", ServerData.iScores[iOtherID])
										PRINT_WITH_2_NUMBERS_NOW("DARTS_REMAIN", ServerData.iLastScore[iSelfID] - ServerData.iScores[iSelfID], ServerData.iScores[iSelfID], 20000, 0)
										//PRINT_WITH_NUMBER("DARTS_REMAINL", ServerData.iScores[iSelfID], 20000, 0)
										
										IF playerData[iSelfID].ClientDart.iHitValue = 50
											DARTS_TRIGGER_SPLASH(DartsUI, DARTSSPLASH_BULLSEYE)
											bPlaySplashFX = TRUE
										ENDIF
										
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO)
										PRINT_WITH_NUMBER("DARTS_BUST_L", ServerData.iTurnscore, 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE)
										PRINT_WITH_NUMBER("DARTS_DOUBLE_L", playerData[iSelfID].ClientDart.iHitValue, 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT)
										PRINT_NOW("DARTS_ONE_PT", 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
										IF playerData[iSelfID].ClientDart.iHitValue = 50
											DARTS_TRIGGER_SPLASH(DartsUI, DARTSSPLASH_BULLSEYE)
											bPlaySplashFX = TRUE
										ELIF ServerData.iTurnscore = 180
											DARTS_TRIGGER_SPLASH(DartsUI, DARTSSPLASH_180)
											bPlaySplashFX = TRUE
										ENDIF
										
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											PRINT_WITH_NAME_AND_NUMBER("DARTS_SWITCH_L", sDartsPlayerNames[playerData[iOtherID].iOffsetPlayerID], ServerData.iScores[iSelfID], 5000, 0)
										ELSE
											PRINT_WITH_2_NUMBERS_NOW("DARTS_REMAIN", ServerData.iLastScore[iSelfID] - ServerData.iScores[iSelfID], ServerData.iScores[iSelfID], 5000, 0)
											//PRINT_WITH_NUMBER("DARTS_REMAINL", ServerData.iScores[iSelfID], 5000, 0)
										ENDIF
										
									ENDIF
								ELSE
									IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE)
										CDEBUG1LN(DEBUG_DARTS, "My server score is ", ServerData.iScores[iSelfID])
										CDEBUG1LN(DEBUG_DARTS, "Other server score is ", ServerData.iScores[iOtherID])
										PRINT_WITH_2_NUMBERS_NOW("DARTS_REMAIN", ServerData.iLastScore[iOtherID] - ServerData.iScores[iOtherID], ServerData.iScores[iOtherID], 20000, 0)
										//PRINT_WITH_NUMBER("DARTS_REMAINL", ServerData.iScores[iOtherID], 20000, 0)
									
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO)
										PRINT_WITH_NUMBER("DARTS_BUST_L", ServerData.iTurnscore, 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE)
										PRINT_WITH_NUMBER("DARTS_DOUBLE_L", playerData[iSelfID].ClientDart.iHitValue, 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT)
										PRINT_NOW("DARTS_ONE_PT", 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
										PRINT_WITH_NAME_AND_NUMBER("DARTS_SWITCH_L", GET_PLAYER_NAME(PLAYER_ID()), ServerData.iScores[iOtherID], 5000, 0)
										
									ENDIF
								ENDIF
								DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, TRUE)
							ENDIF
							
							IF NOT playerData[iSelfID].bResetDone 
							AND ( (DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE) AND TIMERA() > 1000)
								OR TIMERA() > 2000 )
								CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is in SCORE CHECK")
								playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_AIM
								playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_AIM
								MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring = FALSE
								DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
								
								IF iSelfID = serverData.iThrower
									IF playerData[iSelfID].ClientDart.iHitValue = 50
										DartsStats[DARTS_SCLB_STAT_BULLS_EYES]++
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] xp given for bullseye")
										iXPToGive = ROUND(BASE_BULLSEYE_XP * g_sMPTunables.fxp_tunable_darts_bullseye)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] after tuning, xp given = ", iXPToGive)
										iBullseyeXP += GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_SKILL, XPCATEGORY_SKILL_DARTS_BULLSEYE, iXPToGive)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iBullseyeXP is now ", iBullseyeXP)
									ENDIF
								ENDIF
								
								IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER) //serverData.bDoSwitchPlayer
									
									DartsUI.iDartsLeft = 3
									
									// sending the darts back to the player, resetting them
									// serves the same functioon as RESET_DART without deleting the object
									INT y
									REPEAT NUM_MP_DART_TURNS y
										RESET_DART(MPDartGame.Darts[y])
										MPDartGame.Darts[y].bDoneScoring = FALSE
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", y, " was reset")
									ENDREPEAT
									
									IF ServerData.iLastScore[ServerData.iThrower] <> ServerData.iScores[ServerData.iThrower]
										DARTS_ADD_SCORE(DartsSB, playerData[ServerData.iThrower].iOffsetPlayerID, ServerData.iScores[ServerData.iThrower])
									ENDIF
									
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]iSelfID = ", iSelfID)
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]serverData.iThrower = ", serverData.iThrower)
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]ServerData.iTurnscore = ", ServerData.iTurnscore)
									
									IF iSelfID = serverData.iThrower
										IF ServerData.iTurnscore = 180
										AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											DartsStats[DARTS_SCLB_STAT_NUM_180S]++
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]Added 180 stat")
										ENDIF
									ELSE
										bWaitUIShow = FALSE
									ENDIF
									
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
										UPDATE_HIGHLIGHT(DartsSB, playerData[serverData.iWaiter].iOffsetPlayerID)
									ENDIF
									
									iSteadyShotsUsed = 0
								ENDIF
								CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client finished reset in SCORE CHECK")
								playerData[iSelfID].bResetDone = TRUE
							ENDIF
						BREAK
						
						CASE DARTS_MPTURN_WINNING
							CDEBUG1LN(DEBUG_DARTS, "********** serverData.iWinner = ", serverData.iWinner)
							
							CLEAR_PRINTS()
							
							IF NOT MPGlobals.DartsData.bResultsDisplayed
								MPGlobals.DartsData.bResultsDisplayed = TRUE
							ENDIF
							
							// if you done won
							IF serverData.iWinner = iSelfID
								//PRINT_WITH_NUMBER_NOW("DARTS_REMAINL", 0, 5000, 1)
								MPDartGame.bDartWinner = TRUE
								
								MPDartGame.iGamesWon++
								
								IF iSelfID = serverData.iThrower
									IF playerData[iSelfID].ClientDart.iHitValue = 50
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] experience given for bullseye after a win")
										DartsStats[DARTS_SCLB_STAT_BULLS_EYES]++
										iXPToGive = ROUND(BASE_BULLSEYE_XP * g_sMPTunables.fxp_tunable_darts_bullseye)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] after tuning, xp given = ", iXPToGive)
										iBullseyeXP += GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_SKILL, XPCATEGORY_SKILL_DARTS_BULLSEYE, iXPToGive)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iBullseyeXP is now ", iBullseyeXP)
									ENDIF
								ENDIF
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] experience given for leg win")
									DartsStats[DARTS_SCLB_STAT_NUM_LEGS_WON]++
									iXPGained += ROUND(BASE_LEG_WIN_XP * g_sMPTunables.fxp_tunable_darts_legwon)
									CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_legwon = ", g_sMPTunables.fxp_tunable_darts_legwon)
									//GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DARTS_WIN, 50)
									CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iXPGained is now ", iXPGained)
								ENDIF
								
								IF MPDartGame.iThrowsTaken[PlayerData[iSelfID].iOffsetPlayerID] <= 6
									SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FM6DARTCHKOUT, TRUE)
								ENDIF
								
							// if you lost... loser
							ELSE
								//PRINT_WITH_NUMBER_NOW("DARTS_REMAINL", 0, 5000, 1)
								MPDartGame.bDartWinner = FALSE
								
								MPDartGame.iGamesLost++
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									DartsStats[DARTS_SCLB_STAT_NUM_LEGS_LOST]++
								ENDIF
								
							ENDIF
							
							RESTART_TIMER_NOW(controlTimer) 
							
							IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
								PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_TRANSITION
							ELSE
								
								CDEBUG1LN(DEBUG_DARTS, "calling trigger pre load for DARTS")
								TRIGGER_CELEBRATION_PRE_LOAD(sCelebrationData, TRUE)
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									IF MPDartGame.bDartWinner
										
										IF serverData.iNumLegs < 5
											INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STRENGTH, 1)
											CPRINTLN(DEBUG_DARTS, "STRENGTH STAT INCREASED by 1 FROM DARTS")
										ELSE
											INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STRENGTH, 2)
											CPRINTLN(DEBUG_DARTS, "STRENGTH STAT INCREASED by 2 FROM DARTS")
										ENDIF
										
										INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
										INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_DARTS_TOTAL_WINS, 1)
										DartsStats[DARTS_SCLB_STAT_NUM_WINS]++
										DartsStats[DARTS_SCLB_STAT_NUM_SETS_WON]++
									ELSE
										DartsStats[DARTS_SCLB_STAT_NUM_LOSSES]++
										DartsStats[DARTS_SCLB_STAT_NUM_SETS_LOST]++
									ENDIF
									
									INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_DARTS_TOTAL_MATCHES, 1)
								ENDIF
								
								DartsUI.iOurScore += serverData.iSets[iSelfID]
								DartsUI.iTheirScore += serverData.iSets[iOtherID]
								DartsUI.sMyName = sDartsPlayerNames[playerData[iSelfID].iOffsetPlayerID]
								DartsUI.sTheirName = sDartsPlayerNames[playerData[iOtherID].iOffsetPlayerID]
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									IF MPDartGame.bDartWinner
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] giving darts won XP")
										iXPGained += ROUND(BASE_MATCH_WIN_XP * g_sMPTunables.fxp_tunable_darts_match_win)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_match_win is  ", g_sMPTunables.fxp_tunable_darts_match_win)
										//GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DARTS_WIN, 250)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iXPGained is now ", iXPGained)
										iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
										CDEBUG1LN(DEBUG_DARTS, "money given to me the winner")
									ELSE
										iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
										CDEBUG1LN(DEBUG_DARTS, "money given to the other guy, cuz i lost")
									ENDIF
								ENDIF
								
								IF MPDartGame.bDartWinner
									
								ENDIF
								
								IF iBetWinnings > 0
									CDEBUG1LN(DEBUG_DARTS, "iBetWinnings = ", iBetWinnings)
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "no bets were won")
								ENDIF
								
								CDEBUG1LN(DEBUG_DARTS, "g_iMyBetWinnings = ", g_iMyBetWinnings)
								
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] giving darts match finish bonus")
								iXPGained += ROUND(BASE_DART_PLAY_XP * g_sMPTunables.fxp_tunable_darts_taking_part)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_taking_part = ", g_sMPTunables.fxp_tunable_darts_taking_part)
								//GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DARTS_WIN, 100)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iXPGained is now ", iXPGained)
								
								SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PLAY_DARTS)
								CPRINTLN(DEBUG_DARTS, "DAILY OBJECTIVE COMPLETE FOR DARTS")
								
								PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END
							ENDIF
						BREAK
					ENDSWITCH			
				BREAK
				
				CASE DARTS_MPSTATE_GAME_TRANSITION
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
						SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
					ENDIF
					
					SWITCH iSplashStage
						CASE 0
							IF GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 0.75
								CLEAR_HELP()
								CLEAR_PRINTS()
								
								
								
								IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									UPDATE_SB_SETS_LEGS(DartsSB, 0, serverData.iSets[iSelfID], 0, serverData.iLegs[iSelfID])
								ELSE
									IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
										UPDATE_SB_SETS_LEGS(DartsSB, serverData.iSets[iOtherID], serverData.iSets[iSelfID], serverData.iLegs[iOtherID], serverData.iLegs[iSelfID])
									ELSE
										UPDATE_SB_SETS_LEGS(DartsSB, serverData.iSets[iSelfID], serverData.iSets[iOtherID], serverData.iLegs[iSelfID], serverData.iLegs[iOtherID])
									ENDIF
								ENDIF
								
								//big message here
								IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_SET_JUST_WON)
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
										IF MPDartGame.bDartWinner
											DartsStats[DARTS_SCLB_STAT_NUM_SETS_WON]++
										ELSE
											DartsStats[DARTS_SCLB_STAT_NUM_SETS_LOST]++
										ENDIF
									ENDIF
									
									IF MPDartGame.bDartWinner
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "DARTS_WINNER", "DARTS_SETW", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
											//SET_SCALEFORM_MIDSIZED_MESSAGE(DartsUI.siMidMessage, "DARTS_WINNER", "DARTS_SETW")
										ELSE
											SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "DARTS_WINP", "DARTS_SETW", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
											//SET_SCALEFORM_MIDSIZED_MESSAGE(DartsUI.siMidMessage, "DARTS_WINP", "DARTS_SETW")
										ENDIF
									ELSE
										SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "BM_R2P_LOSS", "DARTS_SETL", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_RED)
										//SET_SCALEFORM_MIDSIZED_MESSAGE(DartsUI.siMidMessage, "DARTS_LOSER", "DARTS_SETL",4000, HUD_COLOUR_RED)
									ENDIF
								ELSE
									IF MPDartGame.bDartWinner
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "DARTS_WINNER", "DARTS_LEGW", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
											//SET_SCALEFORM_MIDSIZED_MESSAGE(DartsUI.siMidMessage, "DARTS_WINNER", "DARTS_LEGW")
										ELSE
											SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "DARTS_WINP", "DARTS_LEGW", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
											//SET_SCALEFORM_MIDSIZED_MESSAGE(DartsUI.siMidMessage, "DARTS_WINP", "DARTS_LEGW")
										ENDIF
									ELSE
										SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "BM_R2P_LOSS", "DARTS_LEGL", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_RED)
										//SET_SCALEFORM_MIDSIZED_MESSAGE(DartsUI.siMidMessage, "DARTS_LOSER", "DARTS_LEGL", 4000, HUD_COLOUR_RED)
									ENDIF
								ENDIF
								
								IF MPDartGame.bDartWinner
									PLAY_SOUND_FRONTEND ( -1, "WIN", "HUD_AWARDS")
								ELSE
									PLAY_SOUND_FRONTEND ( -1, "LOSER", "HUD_AWARDS")
								ENDIF
								
								INT iWhichOutro
									
								FLOAT fResolution
								fResolution = GET_ASPECT_RATIO(TRUE)
								CDEBUG1LN(DEBUG_DARTS, " fResolution = ", fResolution)
								
								IF fResolution > 2.0
									CDEBUG1LN(DEBUG_DARTS, " resolution is eyefinity")
									scenePosition = << 1992.29407, 3047.57690, 46.21517 >>
									sceneRotation = << 0.000, 0.000, 138.740 >>
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "resolution is not eyefinity")
									scenePosition = << 1992.33569, 3047.92432, 46.21517 >>
									sceneRotation = << 0.000, 0.000, 136.740 >>
								ENDIF
								
								iWhichOutro = (GET_RANDOM_INT_IN_RANGE() % 3)
								
								CDEBUG1LN(DEBUG_DARTS, "iWhichOutro = ", iWhichOutro)
								
								iSceneID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
								camDartsSync = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
								PLAY_SYNCHRONIZED_CAM_ANIM(camDartsSync, iSceneID, sOutroCam[iWhichOutro], "mini@dartsoutro")
								SET_CAM_ACTIVE(camDartsSync, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									IF NOT IS_PED_INJURED(piPlayerClones[0])
									AND NOT IS_PED_INJURED(piPlayerClones[1])
										IF MPDartGame.bDartWinner
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iSelfID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iOtherID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_PLAY_ANIM(piPlayerClones[playerData[iSelfID].iOffsetPlayerID], sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
										ELSE
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iOtherID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iSelfID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_PLAY_ANIM(piPlayerClones[playerData[iSelfID].iOffsetPlayerID], sFacialDictPed2, sFacialClip2[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
										TASK_SYNCHRONIZED_SCENE (PLAYER_PED_ID(), iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
										TASK_PLAY_ANIM(PLAYER_PED_ID(), sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
									ENDIF
								ENDIF
								
								iSplashStage++
							ENDIF
						BREAK
						
						CASE 1
							//IF NOT UPDATE_SCALEFORM_MIDSIZED_MESSAGE(DartsUI.siMidMessage, TRUE)
							IF NOT UPDATE_SHARD_BIG_MESSAGE(shardMidSizeMessage, TRUE)
								
								RESET_SHARD_BIG_MESSAGE(shardMidSizeMessage)
								
								REPEAT NUM_MP_DART_PLAYERS i
									DARTS_CLEAR_SCORES(DartsSB, i)
								ENDREPEAT
								
								REPEAT NUM_MP_DART_TURNS j
									RESET_DART(MPDartGame.Darts[j])
								ENDREPEAT
								
								iSplashStage++
							ENDIF
						BREAK
						
						CASE 2
							IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
								IF NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_AWAY])
								AND NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_HOME])
									DARTS_GAME_CAM()
									DESTROY_CAM(camDartsSync)
									CLEAR_PED_TASKS (piPlayerClones[DART_PLAYER_AWAY])
									CLEAR_PED_TASKS (piPlayerClones[DART_PLAYER_HOME])
								ENDIF
							ELSE
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									DARTS_GAME_CAM()
									DESTROY_CAM(camDartsSync)
									CLEAR_PED_TASKS (PLAYER_PED_ID())
								ENDIF
							ENDIF
							
							CDEBUG1LN(DEBUG_DARTS, "Starting another set/leg")
							PlayerData[iSelfID].bDoRematch = TRUE
							KILL_MINIGAME_MISSION_TRACKER()
							iSplashStage = 0
							PlayerData[iSelfID].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
						BREAK
						
					ENDSWITCH
				BREAK
				
				CASE DARTS_MPSTATE_GAME_END
					//DARTS_UI_RETVAL DartsUIRetVal
					JOB_WIN_STATUS eJobWinStatus
					
					DEBUG_MESSAGE_PERIODIC("Reached Client side GAME END", iDebugThrottle)
					
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX(iOtherID) )
							SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
						ENDIF
						SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
					ENDIF
					
					IF GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 0.75
						// wait for the player to pick an option
						
						IF (PlayerData[iOtherID].eMPState >= DARTS_MPSTATE_GAME_END)
						OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							
							// wait 'til we've finished rendering the player win/lost text
							IF NOT MPDartGame.bAnotherCamActive
								
								CLEAR_HELP()
								CLEAR_PRINTS()
								
								#IF IS_DEBUG_BUILD
									IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_WINNERD)
									OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_LOSERD)
										MPDartGame.bDartWinner = TRUE
									ELIF DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_WINNERD)
									OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_LOSERD)
										MPDartGame.bDartWinner = FALSE
									ENDIF
								#ENDIF
								
								INT iWhichOutro
								
								FLOAT fResolution
								fResolution = GET_ASPECT_RATIO(TRUE)
								CDEBUG1LN(DEBUG_DARTS, " fResolution = ", fResolution)
								
								IF fResolution > 2.0
									CDEBUG1LN(DEBUG_DARTS, " resolution is eyefinity")
									scenePosition = << 1992.29407, 3047.57690, 46.21517 >>
									sceneRotation = << 0.000, 0.000, 138.740 >>
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "resolution is not eyefinity")
									scenePosition = << 1992.33569, 3047.92432, 46.21517 >>
									sceneRotation = << 0.000, 0.000, 136.740 >>
								ENDIF
								
								iWhichOutro = 2 //(GET_RANDOM_INT_IN_RANGE() % 3) DO NOT CHECK THIS IN
								
								CDEBUG1LN(DEBUG_DARTS, "iWhichOutro = ", iWhichOutro)
								
								iSceneID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
								camDartsSync = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
								PLAY_SYNCHRONIZED_CAM_ANIM(camDartsSync, iSceneID, sOutroCam[iWhichOutro], "mini@dartsoutro")
								SET_CAM_ACTIVE(camDartsSync, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									IF NOT IS_PED_INJURED(piPlayerClones[0])
									AND NOT IS_PED_INJURED(piPlayerClones[1])
										IF MPDartGame.bDartWinner
											PLAY_CELEB_WIN_POST_FX()
										
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iSelfID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iOtherID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_PLAY_ANIM(piPlayerClones[playerData[iSelfID].iOffsetPlayerID], sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
										ELSE
											PLAY_CELEB_LOSE_POST_FX()
											
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iOtherID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iSelfID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
											TASK_PLAY_ANIM(piPlayerClones[playerData[iSelfID].iOffsetPlayerID], sFacialDictPed2, sFacialClip2[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
										PLAY_CELEB_WIN_POST_FX()
										
										TASK_SYNCHRONIZED_SCENE (PLAYER_PED_ID(), iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
										TASK_PLAY_ANIM(PLAYER_PED_ID(), sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
									ENDIF
								ENDIF
								
								MPDartGame.bAnotherCamActive = TRUE
							ELSE
								IF (IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) >= 0.995)
								OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID)
									
//									IF DOES_CAM_EXIST(camDartsSync)
//										DESTROY_CAM(camDartsSync)
//										
//										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
//											IF NOT IS_PED_INJURED(piPlayerClones[0])
//											AND NOT IS_PED_INJURED(piPlayerClones[1])
//												CLEAR_PED_TASKS_IMMEDIATELY(piPlayerClones[0])
//												CLEAR_PED_TASKS_IMMEDIATELY(piPlayerClones[1])
//												FORCE_PED_AI_AND_ANIMATION_UPDATE(piPlayerClones[0])
//												FORCE_PED_AI_AND_ANIMATION_UPDATE(piPlayerClones[1])
//											ENDIF
//										ELSE
//											IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//												CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//												FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//											ENDIF
//										ENDIF
//									ENDIF
//									
//									DARTS_SETUP_MP_QUIT_CAM()
//									//ANOTHER_GAME_BACKSIDE()
								ENDIF
							ENDIF
						ENDIF
						
						IF iBullseyeXP > 0
							CDEBUG1LN(DEBUG_DARTS, "iBullseyeXP = ", iBullseyeXP)
						ENDIF
						
						IF MPDartGame.bDartWinner
							eJobWinStatus = JOB_STATUS_WIN
							winningPlayer = PLAYER_ID()
						ELSE
							eJobWinStatus = JOB_STATUS_LOSE
							winningPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID))
						ENDIF
						
						IF GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 2.15
							IF HAS_GENERIC_CELEBRATION_FINISHED(sCelebrationData, camEndScreen, 
																"XPT_DARTS", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DARTS_WIN, iXPGained, 
																eJobWinStatus, g_iMyBetWinnings, eGenericMGStage, winningPlayer, TRUE, 
																0, 0, TRUE, FALSE)
																//DartsStats[DARTS_SCLB_STAT_NUM_LEGS_WON], DartsStats[DARTS_SCLB_STAT_NUM_LEGS_WON] + DartsStats[DARTS_SCLB_STAT_NUM_LEGS_LOST], TRUE)
							
	//							IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
	//								REQUEST_LEADERBOARD_CAM()
	//								//RENDER_SCRIPT_CAMS(FALSE, FALSE)
	//								
	//							ELIF IS_LEADERBOARD_CAM_READY(TRUE)
									
								IF NOT IS_TIMER_STARTED(endDelay)
									RESTART_TIMER_NOW(endDelay)
//									IF DOES_CAM_EXIST(camDartsSync)
//										DESTROY_CAM(camDartsSync)
//										
//										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
//											IF NOT IS_PED_INJURED(piPlayerClones[0])
//											AND NOT IS_PED_INJURED(piPlayerClones[1])
//												CLEAR_PED_TASKS_IMMEDIATELY(piPlayerClones[0])
//												CLEAR_PED_TASKS_IMMEDIATELY(piPlayerClones[1])
//												FORCE_PED_AI_AND_ANIMATION_UPDATE(piPlayerClones[0])
//												FORCE_PED_AI_AND_ANIMATION_UPDATE(piPlayerClones[1])
//											ENDIF
//										ELSE
//											IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//												CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//												FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//											ENDIF
//										ENDIF
//									ENDIF
//									
//									DARTS_SETUP_MP_QUIT_CAM()
									IF MPDartGame.bDartWinner
										ANIMPOSTFX_STOP("MP_Celeb_Win")
										//ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, TRUE)
									ELSE
										ANIMPOSTFX_STOP("MP_Celeb_Lose")
										//ANIMPOSTFX_PLAY("MP_Celeb_Lose_Out", 0, TRUE)
									ENDIF
									
								ELIF GET_TIMER_IN_SECONDS(endDelay) > 0.1
								
								ENDIF
								
								IF SET_SKYSWOOP_UP(TRUE, TRUE, FALSE)
								
									IF MPDartGame.bEndGameMessageDisplayed 
										
										#IF IS_DEBUG_BUILD
										IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_WINNERD)
										OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_LOSERD)
											CDEBUG1LN(DEBUG_DARTS, "Win / Lose debug flags cleared up")
											DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_WINNERD)
											DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_LOSERD)
										ENDIF
										#ENDIF
										
//										IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//											IF NOT HAS_NET_TIMER_STARTED(serverData.timeLeaderboardTimeOut)
//												START_NET_TIMER(serverData.timeLeaderboardTimeOut)
//											ENDIF
//										
//											FMMC_EOM_SERVER_RESET_PLAYER_COUNTS(sPlayerCounts)
//											INT iRematchLoop
//											REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iRematchLoop
//												IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iRematchLoop))
//													FMMC_EOM_SERVER_PLAYER_PROCESSING(sPlayerCounts, iRematchLoop)
//												ENDIF
//											ENDREPEAT
//											
//											FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING(sPlayerCounts, serverData.sServerFMMC_EOM, FMMC_TYPE_MG_DARTS, 0)
//										ENDIF
										
										
//										IF HAS_NET_TIMER_STARTED(serverData.timeLeaderboardTimeOut)
//											IF MAINTAIN_END_OF_MISSION_SCREEN(sEndOfMission, serverData.sServerFMMC_EOM,
//																		GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),serverData.timeLeaderboardTimeOut.timer) >= VIEW_LEADERBOARD_TIME,
//																		FALSE, FALSE, FALSE, FALSE, FALSE, serverData.timeLeaderboardTimeOut.timer, VIEW_LEADERBOARD_TIME, "LBD_TIMEOUT", TRUE)
												//Go to cleanup state
												CDEBUG1LN(DEBUG_DARTS, "Client chose to quit")
												CDEBUG1LN(DEBUG_DARTS, "my betting Wins = ", iBetWinnings)
												
												SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, contentID)
												
												CLEAR_PRINTS()
												PlayerData[iSelfID].bDoQuit = TRUE
												PlayerData[iSelfID].eMPState = DARTS_MPSTATE_END
//											ELSE
//												CDEBUG1LN(DEBUG_DARTS, "still voting")
//												//DISPLAY_DARTS_LEADERBOARD(DartsUI, DartsEndScreen, iXPGained, DartsUI.iOurScore, DartsUI.iTheirScore, iBetWinnings)
//											ENDIF
//										ENDIF
										
									ELSE
										IF (PlayerData[iOtherID].eMPState >= DARTS_MPSTATE_GAME_END)
										OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											
											#IF IS_DEBUG_BUILD
												IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_WINNERD)
												OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_LOSERD)
													MPDartGame.bDartWinner = TRUE
												ELIF DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_WINNERD)
												OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_LOSERD)
													MPDartGame.bDartWinner = FALSE
												ENDIF
											#ENDIF
											
											MPDartGame.bEndGameMessageDisplayed = TRUE
											
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
							
							IF sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING 
							AND HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, (CELEBRATION_SCREEN_STAT_DISPLAY_TIME/2) - 750)
								IF NOT bPhaseGrabbed
									
									//SET_SKYFREEZE_FROZEN()
									
									CDEBUG1LN(DEBUG_DARTS, "g_sMPTunables.xpMultiplier = ", g_sMPTunables.xpMultiplier)
									CDEBUG1LN(DEBUG_DARTS, "LOBBY_AUTO_MULTIPLAYER_FREEMODE() = ", LOBBY_AUTO_MULTIPLAYER_FREEMODE())
									CDEBUG1LN(DEBUG_DARTS, "GET_SKYFREEZE_STAGE() = ", GET_SKYFREEZE_STAGE())
									CDEBUG1LN(DEBUG_DARTS, "IS_PAUSE_MENU_ACTIVE() = ", IS_PAUSE_MENU_ACTIVE())
									
									IF GET_SKYFREEZE_STAGE() = SKYFREEZE_NONE
										SET_SKYFREEZE_STAGE(SKYFREEZE_FROZEN)
									ENDIF
									
									TOGGLE_RENDERPHASES(FALSE)
									
									CDEBUG1LN(DEBUG_DARTS, "rendering toggled")
									bPhaseGrabbed = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_WAIT_POST_GAME
					DEBUG_MESSAGE_PERIODIC("CLIENT DARTS_MPSTATE_WAIT_POST_GAME", iDebugThrottle)
					
					IF (PlayerData[iSelfID].bDoRematch AND PlayerData[iOtherID].bDoRematch)
					OR (PlayerData[iSelfID].bDoRematch AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER))
										
						IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PlayerData[iSelfID].piOther)
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())							
						ENDIF
						// TODO:  add audio for playing again
						
						IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
							sDartsPlayerNames[PlayerData[iSelfID].iOffsetPlayerID] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSelfID)))
							DartsGamerHandle[PlayerData[iSelfID].iOffsetPlayerID] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSelfID)))
							sCrewTags[PlayerData[iSelfID].iOffsetPlayerID] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[PlayerData[iSelfID].iOffsetPlayerID])
								
							sDartsPlayerNames[PlayerData[iOtherID].iOffsetPlayerID] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
							DartsGamerHandle[PlayerData[iOtherID].iOffsetPlayerID] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
							sCrewTags[PlayerData[iOtherID].iOffsetPlayerID] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[PlayerData[iOtherID].iOffsetPlayerID])
							
							piOtherPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
							
							INT iTemp1, iTemp2
							iTemp1 = ENUM_TO_INT(DART_PLAYER_AWAY)
							iTemp2 = ENUM_TO_INT(DART_PLAYER_HOME)
							
							CDEBUG1LN(DEBUG_DARTS, "iTemp1 = ", iTemp1, " iTemp2 = ", iTemp2)
							CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iTemp1, " = ", sCrewTags[iTemp1])
							CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iTemp2, " = ", sCrewTags[iTemp2])
							
							DARTS_ADD_NAMES(DartsSB, sDartsPlayerNames[iTemp1], sDartsPlayerNames[iTemp2])
							IF NOT IS_STRING_EMPTY(sCrewTags[iTemp1])
								DARTS_ADD_SB_CREW_TAG(DartsSB, iTemp1, sCrewTags[iTemp1])
							ENDIF
							IF NOT IS_STRING_EMPTY(sCrewTags[iTemp2])
								DARTS_ADD_SB_CREW_TAG(DartsSB, iTemp2, sCrewTags[iTemp2])
							ENDIF
							DARTS_CLEAR_SCORES(DartsSB, iTemp1)
							DARTS_CLEAR_SCORES(DartsSB, iTemp2)
							
							DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
						ENDIF
						
						CLEAR_HELP()
						CLEANUP_BIG_MESSAGE()
						
						IF (serverData.mpState = DARTS_MPSTATE_SETUP)
							PlayerData[iSelfID].eMPState = DARTS_MPSTATE_SYNC_OBJECTS
						ENDIF
					ENDIF
					
					// other player wants to quit 
					IF PlayerData[iOtherID].bDoQuit
						KILL_MINIGAME_MISSION_TRACKER()
						CLEAR_PRINTS()
						CLEANUP_BIG_MESSAGE()
						
						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_TERMINATE_SPLASH
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_TERMINATE_SPLASH
					DEBUG_MESSAGE_PERIODIC("Reached Client side TERMINATE SPLASH", iDebugThrottle)
					
					IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
						IF HAS_NET_TIMER_STARTED(timeDartsAwardFailSafe)
							//RENDER_SCRIPT_CAMS(FALSE, FALSE)
							DARTS_SETUP_MP_QUIT_CAM()
							
							
							IF IS_SCREEN_FADED_OUT()
							OR IS_SCREEN_FADING_OUT()
								DO_SCREEN_FADE_IN(500)
							ENDIF
							
							IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
								REQUEST_LEADERBOARD_CAM()
							ELIF IS_LEADERBOARD_CAM_READY(TRUE)
								REINIT_NET_TIMER(timeDartsAwardFailSafe)
								CLEAR_PRINTS()
								CLEAR_HELP()
								SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
								
								IF MPGlobals.DartsData.bResultsDisplayed
									MPGlobals.DartsData.bResultsDisplayed = FALSE
								ENDIF
								
								PlayerData[iSelfID].eMPState = DARTS_MPSTATE_LEAVE
								
								IF (terminateReason = DARTS_MPTERMINATEREASON_PLAYERLEFT)
									SET_SHARD_BIG_MESSAGE(shardBigMessage, "DARTS_WINNER", "DARTS_OPP_LFT", (AWARD_FAILSAFE/2), SHARD_MESSAGE_CENTERED, HUD_COLOUR_WHITE)
									INIT_SIMPLE_USE_CONTEXT(DartsUI.inGameControlContext, FALSE, FALSE, FALSE, TRUE)
									ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "CMRC_CONT", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
									leaveState = DARTS_MPLEAVESTATE_BIG_MESSAGE
									
									SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, contentID)
								ELSE
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[DART_PLAYER_HOME_SERVER], DARTS_CLIENTFLAG_SINGLE_PLAYER)
										IF IS_STRING_NULL(sDartsPlayerNames[playerData[iOtherID].iOffsetPlayerID])
											PRINT_NOW("DARTS_OPP_LFT", DEFAULT_GOD_TEXT_TIME, 0)
										ELSE
											CDEBUG1LN(DEBUG_DARTS, "Other player name (ln 2950) = ", sDartsPlayerNames[playerData[iOtherID].iOffsetPlayerID])
											PRINT_WITH_NAME("DARTS_PLYR_LFT", sDartsPlayerNames[playerData[iOtherID].iOffsetPlayerID], DEFAULT_GOD_TEXT_TIME, 0)
										ENDIF
									ENDIF
									leaveState = DARTS_MPLEAVESTATE_LEADERBOARD
								ENDIF
							
							ENDIF
						ELSE
							START_NET_TIMER(timeDartsAwardFailSafe)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "Other player name (ln 2962) = ", sDartsPlayerNames[playerData[iOtherID].iOffsetPlayerID])
						PRINT_WITH_NAME("DARTS_PLYR_LFT", sDartsPlayerNames[playerData[iOtherID].iOffsetPlayerID], DEFAULT_GOD_TEXT_TIME, 0)
						CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_END
					IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
						
						IF HAS_NET_TIMER_STARTED(timeDartsAwardFailSafe)
						
							//RENDER_SCRIPT_CAMS(FALSE, FALSE)
							DARTS_SETUP_MP_QUIT_CAM()
							
							CLEANUP_BIG_MESSAGE()
							
							IF IS_SCREEN_FADED_OUT()
							OR IS_SCREEN_FADING_OUT()
								DO_SCREEN_FADE_IN(500)
							ENDIF
							
//							IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
//								REQUEST_LEADERBOARD_CAM()
//							ELIF IS_LEADERBOARD_CAM_READY(TRUE)
							IF SET_SKYSWOOP_UP(TRUE)
								REINIT_NET_TIMER(timeDartsAwardFailSafe)
								SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
								
								IF MPGlobals.DartsData.bResultsDisplayed
									MPGlobals.DartsData.bResultsDisplayed = FALSE
								ENDIF
								PlayerData[iSelfID].eMPState = DARTS_MPSTATE_LEAVE
//							ENDIF
							
								leaveState = DARTS_MPLEAVESTATE_LEADERBOARD
							ENDIF
						ELSE
							START_NET_TIMER(timeDartsAwardFailSafe)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "CLIENT STATE: DARTS_MPSTATE_END")
						CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI, TRUE, FALSE, TRUE)
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_LEAVE
					SWITCH leaveState
						CASE DARTS_MPLEAVESTATE_BIG_MESSAGE
							IF NOT UPDATE_SHARD_BIG_MESSAGE(shardBigMessage, TRUE)
							OR HAS_NET_TIMER_EXPIRED(timeDartsAwardFailSafe, AWARD_FAILSAFE/2)
								CLEANUP_BIG_MESSAGE()
								
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] timeDartsAwardFailSafe = ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeDartsAwardFailSafe))
								
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] giving participation XP after an early quit")
								iXPToGive = ROUND(BASE_DART_PLAY_XP * g_sMPTunables.fxp_tunable_darts_taking_part)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_bullseye = ", g_sMPTunables.fxp_tunable_darts_bullseye)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_legwon = ", g_sMPTunables.fxp_tunable_darts_legwon)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_match_win = ", g_sMPTunables.fxp_tunable_darts_match_win)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_taking_part = ", g_sMPTunables.fxp_tunable_darts_taking_part)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iXPToGive = ", iXPToGive)
								iXPGained += GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DARTS_WIN, iXPToGive)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iXPGained is now ", iXPGained)
								DartsUI.iOurScore++
								leaveState = DARTS_MPLEAVESTATE_SWOOP_UP
							ELSE
								IF NOT NETWORK_TEXT_CHAT_IS_TYPING()
									UPDATE_SIMPLE_USE_CONTEXT(DartsUI.inGameControlContext)
								ENDIF
							ENDIF
						BREAK
						
						CASE DARTS_MPLEAVESTATE_SWOOP_UP
							IF SET_SKYSWOOP_UP(TRUE)
								leaveState = DARTS_MPLEAVESTATE_LEADERBOARD
							ENDIF
						BREAK
						
						CASE DARTS_MPLEAVESTATE_LEADERBOARD
//							IF DISPLAY_DARTS_LEADERBOARD(DartsUI, DartsEndScreen, iXPGained, DartsUI.iOurScore, DartsUI.iTheirScore, iBetWinnings)
//							OR HAS_NET_TIMER_EXPIRED(timeDartsAwardFailSafe, AWARD_FAILSAFE)
								DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MG_DARTS, ServerData.iMatchHistoryID, missionScriptArgs.mdID.idVariation, 
														PICK_INT(MPDartGame.bDartWinner, 1, 0), PICK_INT(MPDartGame.bDartWinner, 1, 2))
								
								SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
								
								CLEANUP_MP_DARTS_GAME(MPDartGame, DartsUI)
//							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
			ENDSWITCH
		ENDIF
		
		// Server state machine
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			// General Update
			SWITCH ServerData.mpState
				CASE DARTS_MPSTATE_INIT
					CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] has reached Server side INIT")
					// wait for another participant
					IF iNumberOfPlayers >= 2
						// server side init stuff HERE
						
						IF DART_GET_OTHER_PLAYERS_ID(PlayerData, DART_PLAYER_AWAY_SERVER) = 2
						
							DART_PLAYER_HOME_SERVER = PARTICIPANT_ID_TO_INT()
							
							CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] Server says:")
							CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] DART_PLAYER_HOME_SERVER = ", DART_PLAYER_HOME_SERVER)
							CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] DART_PLAYER_AWAY_SERVER = ", DART_PLAYER_AWAY_SERVER)
							
							ServerData.mpState = DARTS_MPSTATE_THROW_OFF_SETUP
						ENDIF
						
					ELIF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						
						DART_PLAYER_HOME_SERVER = PARTICIPANT_ID_TO_INT()
						
						ServerData.mpState = DARTS_MPSTATE_SETUP
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_THROW_OFF_SETUP
					DEBUG_MESSAGE_PERIODIC("Reached Server side THROW_OFF_SETUP", iDebugThrottle)
					IF IS_BIT_SET(PlayerData[iOtherID].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_AT_THROWOFF))
					AND IS_BIT_SET(PlayerData[iSelfID].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_AT_THROWOFF))
						DARTS_SERVER_THROW_OFF_SETUP(ServerData)
						ServerData.mpState = DARTS_MPSTATE_THROW_OFF
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_THROW_OFF
					//DARTS_SERVER_UPDATE_RUN_CONDITIONS(ServerData, terminateReason, PlayerData)
					DARTS_SERVER_THROW_OFF_UPDATE(ServerData, PlayerData, MPDartGame.Darts[ServerData.iServerTurn])
				BREAK
				
				CASE DARTS_MPSTATE_SETUP
					DEBUG_MESSAGE_PERIODIC("Reached Server side SETUP", iDebugThrottle)
					IF (IS_BIT_SET(PlayerData[iOtherID].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_SYNC_COMPLETE))
						AND IS_BIT_SET(PlayerData[iSelfID].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_SYNC_COMPLETE)))
					OR (IS_BIT_SET(PlayerData[iSelfID].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_SYNC_COMPLETE))
						AND IS_BIT_SET(PlayerData[iSelfID].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_SINGLE_PLAYER)))
						
						DARTS_SERVER_GAME_SETUP(ServerData, DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER), iSelfID)
						
						// deactivate floating board help
						BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE)
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							// broadcast match info to all players
							broadcastIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSelfID))
							BROADCAST_DARTS_UPDATE(broadcastIndex, NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)), 
													ServerData.iScores[DART_PLAYER_AWAY_SERVER], ServerData.iScores[DART_PLAYER_HOME_SERVER], 
													GET_PLAYER_TEAM(broadcastIndex), TRUE)
						ENDIF
						
						ServerData.mpState = DARTS_MPSTATE_IN_PROGRESS
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]Server is proceeding to IN PROGRESS")
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_IN_PROGRESS
					//DARTS_SERVER_UPDATE_RUN_CONDITIONS(ServerData, terminateReason, PlayerData)
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						IF (GET_GAME_TIMER() % 5000) < 50
							CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] ServerData.iServerTurn = ", ServerData.iServerTurn)
							CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] iServerTurn.bDoneScoring = ", MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring)
						ENDIF
						
						DARTS_SERVER_UPDATE_GAME(ServerData, PlayerData, MPDartGame.Darts[ServerData.iServerTurn])
					ELSE
						DARTS_SERVER_UPDATE_SINGLE_PLAYER_GAME(ServerData, PlayerData, MPDartGame.Darts[ServerData.iServerTurn])
					ENDIF
				BREAK				
			ENDSWITCH		
		ENDIF		
	ENDWHILE
ENDSCRIPT
