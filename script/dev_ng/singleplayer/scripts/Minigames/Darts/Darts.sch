////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts.sch                               //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    High Level Defs        			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "globals.sch"
USING "Darts_Dart.sch"
USING "Darts_Board.sch"
USING "Darts_Player_lib.sch"
USING "Darts_AI.sch"

// Enums
ENUM DART_STAGE
	STAGE_PRE_INIT,
	STAGE_PRE_INIT_WAIT,
	STAGE_INIT,
	STAGE_STREAMING,
	STAGE_SPAWNING,
	STAGE_CHALLENGE,
	STAGE_SPLASH,
	STAGE_TUTORIAL,
	STAGE_INIT_MENU,
	STAGE_STARTING,
	STAGE_IN_PROGRESS,
	STAGE_GAME_TRANSITION,
	STAGE_GAME_END,
	STAGE_FINISHED,
	STAGE_PLAYER_QUIT,
	STAGE_CLEANUP
ENDENUM

ENUM DARTS_LOCATION
	DARTSLOCATION_LOST_HIDEOUT,
	DARTSLOCATION_HICK_BAR,
	DARTSLOCATION_ROCK_CLUB,
	DARTSLOCATION_NUM
ENDENUM

ENUM DART_TURN_CHANGE
	DTC_BOARD_ZOOM,
	DTC_DART_ZOOM,
	DTC_NEXT_TURN
ENDENUM

ENUM DART_ZOOM_TURN
	DZT_ZOOM_IN,
	DZT_ZOOM_INTERP,
	DZT_ZOOM_FP,
	DZT_ZOOM_OUT
ENDENUM

ENUM ONE_DART_STAGE
	ODS_ZOOM_IN,
	ODS_ZOOM_OUT
ENDENUM

ENUM DARTS_RAVE_SCENES
	DARTSRAVE_GREETING,
	DARTSRAVE_MENU,
	DARTSRAVE_MATCH,
	DARTSRAVE_COMPLETE
ENDENUM

ENUM DARTS_SCLB_STAT_KEEPER
	DARTS_SCLB_STAT_SCORE,
	DARTS_SCLB_STAT_BULLS_EYES,
	DARTS_SCLB_STAT_NUM_180S,
	DARTS_SCLB_STAT_NUM_WINS,
	DARTS_SCLB_STAT_NUM_MATCHES,
	DARTS_SCLB_STAT_NUM_LOSSES,
	DARTS_SCLB_STAT_NUM_SETS_WON,
	DARTS_SCLB_STAT_NUM_SETS_LOST,
	DARTS_SCLB_STAT_NUM_LEGS_WON,
	DARTS_SCLB_STAT_NUM_LEGS_LOST,
	DARTS_SCLB_STAT_DARTS_THROWN,
	DARTS_SCLB_STAT_TOTAL
ENDENUM
INT DartsStats[DARTS_SCLB_STAT_TOTAL]

CONST_INT NUM_DART_PLAYERS 2
CONST_INT NUM_DART_TURNS 3
CONST_INT INIT_DART_SCORE 301
CONST_INT DART_MONEY_WON 10

// Structs
STRUCT DART_GAME
	// enums
	DART_STAGE 			Stage 			= STAGE_PRE_INIT
	DARTS_PLAYER_STATE 	TurnStage 		= DPS_AIM
	DART_TURN_CHANGE	TurnChange 		= DTC_BOARD_ZOOM
	DART_ZOOM_TURN		ZoomTurn 		= DZT_ZOOM_IN
	ONE_DART_STAGE		OneDartStage 	= ODS_ZOOM_IN
	
	// structs
	DART				Darts[NUM_DART_PLAYERS][NUM_DART_TURNS]
	DART				TutorialDarts[NUM_DART_TURNS]
	DARTS_BOARD			DartsBoard
	DARTS_THROW_STYLE	eThrowStyle = DARTS_STYLE_IV //DARTS_STYLE_STICK
	DARTS_AI_BRAIN		AIBrain
	structTimer 		miscTimer
	structPedsForConversation DartsConvo
	
	// var's
	INT 	Scores[NUM_DART_PLAYERS]
	INT		LastScore[NUM_DART_PLAYERS]
	INT		iTurnScore
	INT		iThrowsTaken[NUM_DART_PLAYERS]
	INT		iBullsEyesHit[NUM_DART_PLAYERS]
	INT		iBustStreak
	INT		iNumThrowsTurn
	INT 	iDartSBGamesWon[NUM_DART_PLAYERS]
	INT 	iDartSBSetsWon[NUM_DART_PLAYERS]
	INT 	iDartSBFullGamesWon[NUM_DART_PLAYERS]
	INT 	iDartGamesWon[NUM_DART_PLAYERS]
	INT		iDartSetsWon[NUM_DART_PLAYERS]
	INT		iNumDartLegs
	INT		iNumDartSets
		
	BOOL	bDartWinner
	BOOL	bDartsClash
	BOOL	bOneDartAway
	BOOL 	bAnotherCamActive//=TRUE
	BOOL 	bWinNoise
	BOOL	bSetJustWon
	BOOL	bWriteToLeaderboard
	
	VECTOR 	vOneDartZoomCoord
ENDSTRUCT

STRUCT DARTS_PLAYER_POS
	VECTOR 	vDartPlayrPos
	VECTOR 	vOpponentPos
ENDSTRUCT

// Funcs and Procs

PROC SET_PLAYER_POSITIONS(DARTS_PLAYER_POS & PlayerPositions, DARTS_BOARD & DartsBoard)
	PlayerPositions.vDartPlayrPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
																					<< -0.4, -2.4 + fDartBoardOriginOffset, -1.7272 >>)	
	PlayerPositions.vOpponentPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartsBoard.vDartBoard, DartsBoard.fBoardHeading, 
																					<< 0.5, -2.4 + fDartBoardOriginOffset, -1.7272 >>)
	
	//non playing group members?  have to check if that will come in to play
	// get standing positions for non-playing group members
	//VECTOR nonPlayingGroupMembers[2]
	//nonPlayingGroupMembers[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(DartsBoard.oiDartBoard, << 0.25, -4.5, -1.6 >>)
	//nonPlayingGroupMembers[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(DartsBoard.oiDartBoard, << -0.5, -4.5, -1.6 >>) 
ENDPROC

PROC INIT_RETICLE_OFFSETS(DARTS_STARTING_RETICLE_POS & InitRetPos, DARTS_BOARD & DartsBoard)
// Get the initial dart position	
	
	// Get the initial target position
	InitRetPos.vInitReticlePosOffset = << 0.0, -0.15, 0.0 >>
	InitRetPos.vInitReticlePosOffset += exactCentreOfBoardXZOffset
	CDEBUG1LN(DEBUG_DARTS, "[INIT_RETICLE_OFFSETS] vInitReticlePosOffset = ", InitRetPos.vInitReticlePosOffset)
	
	InitRetPos.vInitReticlePos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartsBoard.vDartBoard, DartsBoard.fBoardHeading, << 0.0, 0.0, 0.0 >>)
	CDEBUG1LN(DEBUG_DARTS, "[INIT_RETICLE_OFFSETS] vInitReticlePos = ", InitRetPos.vInitReticlePos)
ENDPROC

/// PURPOSE:
///    Sets offsets for objects in relation to the Dart board
/// PARAMS:
///    DartsBoard - provides the vector and heading for the necessary offsets
PROC GET_WORLD_OFFSETS(DARTS_BOARD & DartsBoard, DARTS_PLAYER_POS & PlayerPositions, DARTS_STARTING_RETICLE_POS & InitRetPos, BOOL bMPDarts=FALSE)
	
	// Get the dart board's heading
	IF DOES_ENTITY_EXIST(DartsBoard.oiDartBoard)
		DartsBoard.fBoardHeading = GET_ENTITY_HEADING(DartsBoard.oiDartBoard)
		CDEBUG1LN(DEBUG_DARTS, "[GET_WORLD_OFFSETS] DartsBoard.fBoardHeading = ", DartsBoard.fBoardHeading)
	ENDIF
	
	// Get the player throw position.  Values came from old script
	IF NOT bMPDarts
		SET_PLAYER_POSITIONS(PlayerPositions, DartsBoard)
		CDEBUG1LN(DEBUG_DARTS, "[GET_WORLD_OFFSETS] Player positions set ")
	ENDIF
	
	// Get the initial dart position
	vInitDartPosOffset 	= << 0.0, -2.3685 + fDartboardOriginOffset, 0.1 >> 
	vInitDartPosOffset 	+= exactCentreOfBoardXZOffset
	CDEBUG1LN(DEBUG_DARTS, "[GET_WORLD_OFFSETS] vInitDartPosOffset = ", vInitDartPosOffset)
	vInitDartPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartsBoard.vDartBoard, DartsBoard.fBoardHeading, vInitDartPosOffset)
	
	INIT_RETICLE_OFFSETS(InitRetPos, DartsBoard)
ENDPROC

/// PURPOSE:
///    Accesor for Dart_stage
/// PARAMS:
///    DartGame - by reference
/// RETURNS:
///    Current Dart_Stage
FUNC DART_STAGE GET_DARTS_STAGE(DART_GAME& DartGame)
	RETURN DartGame.Stage
ENDFUNC

/// PURPOSE:
///    Sets Dart Stage
/// PARAMS:
///    DartGame - Current Game being used
///    DartStage - Desired stage to be set
PROC SET_DARTS_STAGE(DART_GAME& DartGame, DART_STAGE DartStage)
	DartGame.Stage = DartStage
ENDPROC


/// PURPOSE:
///    Accesor for Turn_stage
/// PARAMS:
///    DartGame - by reference
/// RETURNS:
///    Current Turn_Stage
FUNC DARTS_PLAYER_STATE GET_DARTS_PLAYER_STATE(DART_GAME& DartGame)
	RETURN DartGame.TurnStage
ENDFUNC

/// PURPOSE:
///    Sets Dart Turn
/// PARAMS:
///    DartGame - Current Game being used
///    Turn - Desired Darts_Player_State to be set
PROC SET_DARTS_TURN(DART_GAME& DartGame, DARTS_PLAYER_STATE TurnStage)
	DartGame.TurnStage = TurnStage
ENDPROC
