////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Args.sch							//
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Arg Defs 							  //
//                                                           //
//////////////////////////////////////////////////////////////

//ENUM DARTS_GAME_TYPE
//	DARTSGAMETYPE_SINGLEPLAYER,
//	DARTSGAMETYPE_MULTIPLAYER
//ENDENUM

STRUCT DARTS_ARGS
//	DARTS_GAME_TYPE gameType

	VECTOR 			vDartBoard
	FLOAT			fBoardHeading
	OBJECT_INDEX	oiDartBoard
	PED_INDEX		piDartOpponent
	MODEL_NAMES		eNextOpponentModel
	
//	INT				iBoardIndex
ENDSTRUCT

// Mutator for game type
//PROC DARTS_SET_ARG_BOARD_INDEX(DARTS_ARGS & theseArgs, INT iBoardIndex)
//	IF iBoardIndex < 1
//		SCRIPT_ASSERT("Passing in a zero or less value for board index! Are you sure?")
//		EXIT
//	ENDIF
//	
//	theseArgs.iBoardIndex = iBoardIndex
//ENDPROC
