////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    MP_Darts.sc                             //
// AUTHOR          :    Lino Manansala
// EDITED FOR APARTMENTS: Aleksej Leskin
// DESCRIPTION     :    Multiplayer Darts      			      //
//                                                           //
//////////////////////////////////////////////////////////////

// GUIDE
// search for these and add bookmarks to make your life easier
// 
// #NORMAL
// #THROWOFF
// #REALGAME
//

USING "net_mission.sch"
USING "net_hud_activating.sch"
USING "fmmc_restart_header.sch"
USING "net_celebration_screen.sch"

USING "Darts.sch"
USING "Darts_Core.sch"
USING "Darts_UI_lib.sch"

USING "MP_Darts.sch"
USING "MP_Darts_Apartment_Client_lib.sch"
//USING "MP_Darts_Server_lib.sch"
USING "MP_Darts_Broadcast_lib.sch"
USING "Darts_Leaderboard_lib.sch"
USING "fmmc_mp_setup_mission.sch"
USING "achievement_public.sch"

USING "AM_Darts_Apartment.sch"
USING "MP_Darts_Apartment_Server_lib.sch"
CONST_INT BASE_BULLSEYE_XP 	10 		// award for succesful bullseyes
CONST_INT BASE_LEG_WIN_XP 	50		// award for each leg win
CONST_INT BASE_MATCH_WIN_XP 250		// award for a total match win
CONST_INT BASE_DART_PLAY_XP 100		// award for participating

INT iSelfID, iOtherID
//INT iAllowSkipCutsceneTime
BOOL bSoloMP
BOOL bPlayerQuit

//STRING sFacialDictPed
//STRING sFacialDictPed2
//structTimer stCleanupTimer
INT iNumberOfPlayers

CELEBRATION_SCREEN_DATA sCelebrationData
CAMERA_INDEX camEndScreen
CAMERA_INDEX renderingCam
CAMERA_INDEX ciDartsQuit
CAMERA_INDEX camDartsSync

SCRIPT_SHARD_BIG_MESSAGE shardBigMessage
SCRIPT_SHARD_BIG_MESSAGE shardMidSizeMessage



ENUM DART_PLAYER_POSITION
	DART_PLAYER_IN_POSITION,
	DART_PLAYER_OUT_POSITION
ENDENUM

#IF IS_DEBUG_BUILD
BOOL bDartsNoSocialClub
#ENDIF

PROC PRINT_WITH_NAME_AND_TWO_NUMBERS(STRING pTextLabel, STRING PlayerName, INT FirstNumberToInsert, INT SecondNumberToInsert, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
//		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
//		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
		ADD_TEXT_COMPONENT_INTEGER(FirstNumberToInsert)
		ADD_TEXT_COMPONENT_INTEGER(SecondNumberToInsert)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

PROC PRINT_WITH_NAME(STRING pTextLabel, STRING PlayerName, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
//		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
//		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

PROC PRINT_WITH_NAME_AND_NUMBER(STRING pTextLabel, STRING PlayerName, INT NumberToInsert, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
//		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
//		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
		ADD_TEXT_COMPONENT_INTEGER(NumberToInsert)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

PROC PRINT_WITH_TWO_NAMES_AND_NUMBER(STRING pTextLabel, STRING FirstPlayerName, STRING SecondPlayerName, INT NumberToInsert, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
//		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
//		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(FirstPlayerName)
		ADD_TEXT_COMPONENT_INTEGER(NumberToInsert)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(SecondPlayerName)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

PROC PRINT_WITH_TWO_NAMES_AND_TWO_NUMBERS(STRING pTextLabel, STRING FirstPlayerName, STRING SecondPlayerName, INT FirstNumberToInsert, INT SecondNumberToInsert, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
//		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
//		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(FirstPlayerName)
		ADD_TEXT_COMPONENT_INTEGER(FirstNumberToInsert)
		ADD_TEXT_COMPONENT_INTEGER(SecondNumberToInsert)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(SecondPlayerName)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

/// PURPOSE:
///    how many participants are actually players?  rather then spectators or sctv
/// PARAMS:
///    bPrintDebugInfo - TRUE to show info about who's who, should not be set if this is getting called every frame
/// RETURNS:
///    
FUNC INT NETWORK_GET_NUM_PLAYERS(DARTS_AP_PLAYER_BROADCAST_DATA & DartsPlayerData[MAXPLAYERS], BOOL bPrintDebugInfo = FALSE) 
	INT iNumPlayers
	INT idx, iNumParticipants
	INT partID
	
	PARTICIPANT_INDEX participantID
	
	iNumParticipants = ENUM_TO_INT(MAXPLAYERS)
	
	IF bPrintDebugInfo
	CDEBUG1LN(DEBUG_DARTS, "checking players, current participants = ", iNumParticipants)
	ENDIF
	
	// can't run a repeat method if there's only one participant, because the first participant gets id 1
	// this would work if it started at 0
	
	REPEAT iNumParticipants idx
		participantID = INT_TO_PARTICIPANTINDEX( idx )
		IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
			IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
			OR DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
			//OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(participantID))
				IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
					IF bPrintDebugInfo
						partID = DartsPlayerData[idx].iParticipantID
						CDEBUG1LN(DEBUG_DARTS, "this participant is sctv: ", DartsPlayerData[idx].iParticipantID)
						IF partID = idx
							CDEBUG1LN(DEBUG_DARTS, "partID = idx")
						ENDIF
					ENDIF
				ENDIF
				IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
					IF bPrintDebugInfo
						CDEBUG1LN(DEBUG_DARTS, "this participant is spectator: ", DartsPlayerData[idx].iParticipantID)
					ENDIF
				ENDIF
			ELSE
				iNumPlayers++
				IF bPrintDebugInfo
					CDEBUG1LN(DEBUG_DARTS, "this participant is a player: ", DartsPlayerData[idx].iParticipantID)
					CDEBUG1LN(DEBUG_DARTS, "number of players: ", iNumPlayers)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iNumPlayers
ENDFUNC

/// PURPOSE:
///    how many participants are spectators?
/// PARAMS:
///    bPrintDebugInfo - TRUE to show info about who's who, should not be set if this is getting called every frame
/// RETURNS:
///  
FUNC INT NETWORK_GET_NUM_SPECTATORS(DARTS_AP_PLAYER_BROADCAST_DATA & DartsPlayerData[MAXPLAYERS], BOOL bPrintDebugInfo = FALSE) 
	INT iNumPlayers
	INT idx, iNumParticipants
	
	iNumParticipants = ENUM_TO_INT(MAXPLAYERS)
	
	IF bPrintDebugInfo
		CDEBUG1LN(DEBUG_DARTS, "checking spectators, current participants = ", iNumParticipants)
	ENDIF
	
	REPEAT iNumParticipants idx
		IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_PARTICIPANT_SET)
			IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(DartsPlayerData[idx].iParticipantID)))
			OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(DartsPlayerData[idx].iParticipantID)))
				IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(DartsPlayerData[idx].iParticipantID)))
					IF bPrintDebugInfo
						CDEBUG1LN(DEBUG_DARTS, "this participant is spectator: ", DartsPlayerData[idx].iParticipantID)
					ENDIF
					iNumPlayers++
				ENDIF
				IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(DartsPlayerData[idx].iParticipantID)))
					IF bPrintDebugInfo
						CDEBUG1LN(DEBUG_DARTS, "this participant is sctv: ", DartsPlayerData[idx].iParticipantID)
					ENDIF
					iNumPlayers--
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iNumPlayers
ENDFUNC

FUNC BOOL NETWORK_CHECK_ALL_SPEC_SCORED(DARTS_AP_PLAYER_BROADCAST_DATA & DartsPlayerData[MAXPLAYERS], BOOL bPrintDebugInfo = FALSE) 
	#IF IS_DEBUG_BUILD
		INT iNumPlayers
	#ENDIF
	INT idx, iNumParticipants
	
	iNumParticipants = ENUM_TO_INT(MAXPLAYERS)
	
	INT iNumSpecs
	iNumSpecs = 0

	INT iNumSpecScored
	iNumSpecScored = 0
	
	BOOL bReturn
	bReturn = FALSE
	
	IF bPrintDebugInfo
		CDEBUG1LN(DEBUG_DARTS, "")
		CDEBUG1LN(DEBUG_DARTS, "checking spectators, have scored = ")
	ENDIF
	
	REPEAT iNumParticipants idx
		IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_PARTICIPANT_SET)
			#IF IS_DEBUG_BUILD
				iNumPlayers++
			#ENDIF
			IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
				iNumSpecs++
				IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_SCORING_DONE)
					iNumSpecScored++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iNumSpecScored >= iNumSpecs
		bReturn = TRUE
	ENDIF
	
//		REPEAT iNumParticipants idx
//		IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_PARTICIPANT_SET)
//			IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
//				IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_SCORING_DONE)
//					DARTS_CLEAR_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_SCORING_DONE)
//				ENDIF
//			ENDIF
//		ENDIF
	//ENDREPEAT
	#IF IS_DEBUG_BUILD
	IF bPrintDebugInfo
		CDEBUG1LN(DEBUG_DARTS, "NumPlayers:", iNumPlayers, " NumSpecs:", iNumSpecs, " NumSpecScored:", iNumSpecScored)
	ENDIF
	#ENDIF
	RETURN bReturn
ENDFUNC

/// PURPOSE:
///    how many participants are SCTV? not as important as players or spectators, but putting in place anyway
/// PARAMS:
///    bPrintDebugInfo - TRUE to show info about who's who, should not be set if this is getting called every frame
/// RETURNS:
///    
FUNC INT NETWORK_GET_NUM_SCTV(DARTS_AP_PLAYER_BROADCAST_DATA & DartsPlayerData[MAXPLAYERS], BOOL bPrintDebugInfo = FALSE) 
	INT iNumPlayers
	INT idx, iNumParticipants
	
	iNumParticipants = ENUM_TO_INT(MAXPLAYERS)
	
	IF bPrintDebugInfo
		CDEBUG1LN(DEBUG_DARTS, "checking SCTV, current participants = ", iNumParticipants)
	ENDIF
	
	REPEAT iNumParticipants idx
		IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_PARTICIPANT_SET)
			IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(idx)))
				IF bPrintDebugInfo
					CDEBUG1LN(DEBUG_DARTS, "this participant is sctv: ", idx)
				ENDIF
				iNumPlayers++
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iNumPlayers
ENDFUNC


PROC DART_GET_DARTS_PLAYERS_ID(DARTS_AP_PLAYER_BROADCAST_DATA & DartsPlayerData[MAXPLAYERS],  INT & iReturnPlayers[DART_PLAYER_IDS], INT iPlayerNum, INT selfID, INT otherID, BOOL bSpec)
	
	INT iCurrentPlayer = 0
	INT idx, iMaxParticipants
	
	PARTICIPANT_INDEX participantID
	iMaxParticipants = ENUM_TO_INT(MAXPLAYERS)
	
	REPEAT iMaxParticipants idx
		participantID = INT_TO_PARTICIPANTINDEX( idx )
		IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
		AND DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_PARTICIPANT_SET)
			IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
			AND NOT DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
				IF iCurrentPlayer < iPlayerNum
					IF bSpec
						
						IF DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_I_AM_SETUP_PLAYER)
							iReturnPlayers[0] = DartsPlayerData[idx].iParticipantID
						ELSE
							iReturnPlayers[1] = DartsPlayerData[idx].iParticipantID
						ENDIF
					
					
						//iReturnPlayers[iCurrentPlayer] = DartsPlayerData[idx].iParticipantID
					ELSE
						IF PARTICIPANT_ID() = participantID
							iReturnPlayers[selfID] = DartsPlayerData[idx].iParticipantID
						ELSE
							iReturnPlayers[otherID] = DartsPlayerData[idx].iParticipantID
						ENDIF
					ENDIF	
					iCurrentPlayer++
					CDEBUG1LN(DEBUG_DARTS, "FOUND DARTS PLAYER, Participant_Index: ", DartsPlayerData[idx].iParticipantID)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


FUNC INT DART_GET_OTHER_PLAYERS_ID(DARTS_AP_PLAYER_BROADCAST_DATA & DartsPlayerData[MAXPLAYERS], INT & returnID)
	INT myPartID
	INT idx, iNumParticipants, iMaxParticipants
	
	PARTICIPANT_INDEX participantID
	
	myPartID = PARTICIPANT_ID_TO_INT()
	
	iMaxParticipants = ENUM_TO_INT(MAXPLAYERS)
	
	REPEAT iMaxParticipants idx
		participantID = INT_TO_PARTICIPANTINDEX( idx )
		IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
		AND DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_PARTICIPANT_SET)
			IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
			AND NOT DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
				IF myPartID <> DartsPlayerData[idx].iParticipantID
				
					returnID = DartsPlayerData[idx].iParticipantID
					CDEBUG1LN(DEBUG_DARTS, "the other participant is: ", returnID)
				ENDIF
				
				iNumParticipants++
			ENDIF
		ENDIF
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_DARTS, "num players seen: ", iNumParticipants)
	RETURN iNumParticipants
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    DartsPlayerData - 
///    iReturnPlayers - 
/// RETURNS:
///    
FUNC BOOL GET_BOTH_PLAYERS_ID(DARTS_AP_PLAYER_BROADCAST_DATA & DartsPlayerData[MAXPLAYERS], INT & iReturnPlayers[DART_PLAYER_IDS])
	
	INT idx, iMaxParticipants, iNumParticipants
	
	PARTICIPANT_INDEX participantID
	iMaxParticipants = ENUM_TO_INT(MAXPLAYERS)
	
	REPEAT iMaxParticipants idx
		participantID = INT_TO_PARTICIPANTINDEX( idx )
		IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
		AND DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_PARTICIPANT_SET)
			IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
			AND NOT DARTS_CHECK_CLIENT_FLAG(DartsPlayerData[idx], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
				
				iReturnPlayers[DartsPlayerData[idx].iOffsetPlayerID] = DartsPlayerData[idx].iParticipantID
				iNumParticipants++
				
				CDEBUG1LN(DEBUG_DARTS, "found a participant")
				CDEBUG1LN(DEBUG_DARTS, "which one : ", DartsPlayerData[idx].iOffsetPlayerID)
				CDEBUG1LN(DEBUG_DARTS, "what ID: ", DartsPlayerData[idx].iParticipantID)
				CDEBUG1LN(DEBUG_DARTS, "num participants: ", iNumParticipants)
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iNumParticipants = 2
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


/// PURPOSE:
///    Grab the necessary assets for Darts
PROC REQUEST_STREAMS(DARTS_SCOREBOARD & DartsSB, BOOL bSpectator = FALSE)

	//REQUEST_ANIM_DICT(sFacialDictPed)
	//REQUEST_ANIM_DICT(sFacialDictPed2)
	REQUEST_MODEL(modelDart1)
	REQUEST_MODEL(prop_phonebox_03)
	DartsSB.siScoreBoard = REQUEST_SCALEFORM_MOVIE("DARTS_SCOREBOARD_BIKER")
	REQUEST_ANIM_DICT("mini@darts")
	REQUEST_ANIM_DICT("mini@dartsintro_alt1")
	REQUEST_ANIM_DICT("mini@dartsoutro")
	REQUEST_ANIM_DICT("anim@amb@clubhouse@mini@darts@")
		
	IF NOT bSpectator
		REQUEST_MODEL(modelReticleRegular)	
		REQUEST_STREAMED_TEXTURE_DICT("Darts")

		shardBigMessage.siMovie			= REQUEST_MG_BIG_MESSAGE()
		shardMidSizeMessage.siMovie		= REQUEST_MG_MIDSIZED_MESSAGE()
		REQUEST_ADDITIONAL_TEXT("DARTS", PROPERTY_TEXT_SLOT)
	ENDIF
	
ENDPROC

PROC RELEASE_ASSETS(BOOL bSpectator)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelDart1)
	SET_MODEL_AS_NO_LONGER_NEEDED(prop_phonebox_03)
	
	IF NOT bSpectator
		RELEASE_SCRIPT_AUDIO_BANK()
		SET_MODEL_AS_NO_LONGER_NEEDED(modelReticleRegular)
	ENDIF
ENDPROC

/// PURPOSE:
///    Asset loading checker
/// RETURNS:
///    TRUE if all the needed assets have loaded
FUNC BOOL HAVE_STREAMS_LOADED(DARTS_SCOREBOARD & DartsSB, BOOL bSpectator)

	IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\DARTS")	
		CDEBUG1LN(DEBUG_DARTS, "Darts Audio loading")
		RETURN FALSE
	ENDIF
		
	IF NOT HAS_MODEL_LOADED(prop_phonebox_03)
		CDEBUG1LN(DEBUG_DARTS, "prop_phonebox_03 loading")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(modelDart1)
		CDEBUG1LN(DEBUG_DARTS, "modelDart1 loading")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(DartsSB.siScoreBoard)
		CDEBUG1LN(DEBUG_DARTS, "DartsSB.siScoreBoard loading")
		RETURN FALSE
	ENDIF
	
//	IF NOT HAS_ANIM_DICT_LOADED(sFacialDictPed)
//		CDEBUG1LN(DEBUG_DARTS, "sFacialDictPed loading")
//		RETURN FALSE
//	ENDIF
//	IF NOT HAS_ANIM_DICT_LOADED(sFacialDictPed2)
//		CDEBUG1LN(DEBUG_DARTS, "sFacialDictPed2 loading")
//		RETURN FALSE
//	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("mini@darts")
		CDEBUG1LN(DEBUG_DARTS, "mini@darts loading")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("mini@dartsintro_alt1")
		CDEBUG1LN(DEBUG_DARTS, "mini@darts_alt1 loading")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("mini@dartsoutro")
		CDEBUG1LN(DEBUG_DARTS, "mini@dartsoutro loading")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("anim@amb@clubhouse@mini@darts@")
		CDEBUG1LN(DEBUG_DARTS, "anim@amb@clubhouse@mini@darts@ loading")
		RETURN FALSE
	ENDIF
	
	IF NOT bSpectator
		
		IF NOT HAS_MODEL_LOADED(modelReticleRegular)
			CDEBUG1LN(DEBUG_DARTS, "modelReticleRegular loading")
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_ADDITIONAL_TEXT_LOADED(PROPERTY_TEXT_SLOT)
			CDEBUG1LN(DEBUG_DARTS, "PROPERTY_TEXT_SLOT loading")
			RETURN FALSE
		ENDIF

		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("Darts")
			CDEBUG1LN(DEBUG_DARTS, "Darts streamed texture dict loading")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CLEANUP_MP_DARTS_GAME(
BOOL bSpectator,
STRUCT_INTRO_SCENE	&sLocalIntroScene,
MP_MISSION_DATA &missionScriptArgs, 
DARTS_SERVER_BROADCAST_DATA &ServerData, 
DARTS_SCOREBOARD & DartsSB,   
DARTS_AP_PLAYER_BROADCAST_DATA & PlayerData[MAXPLAYERS], 
STRUCT_PLAYER_INFO &PlayerDarts[DART_PLAYER_IDS],
MP_DARTS_GAME & MPDartGame, 
DARTS_UI & DartsUI,
BOOL bRestoreUI = TRUE, 
BOOL bLeaveArea = FALSE, 
BOOL bQuitter = FALSE)
	
	CDEBUG1LN(DEBUG_DARTS, "Starting Darts cleanup procedure")
	CDEBUG1LN(DEBUG_DARTS, "CLEANUP_MP_DARTS_GAME - SCALEFORM ID: = ", NATIVE_TO_INT(DartsSB.siScoreBoard))
	CDEBUG1LN(DEBUG_DARTS, "CLEANUP_MP_DARTS_GAME - My Participant ID: = ", PARTICIPANT_ID_TO_INT())
	CDEBUG1LN(DEBUG_DARTS, "CLEANUP_MP_DARTS_GAME - Is Signed online: = ", NETWORK_IS_SIGNED_ONLINE())
	
	INT iPlayerID
	REPEAT DART_PLAYER_IDS iPlayerID
		DART_CLEANUP_DETACH(PlayerDarts[iPlayerID].PlayerDarts)
	ENDREPEAT
		
	DART_CLEANUP_DETACH(MPDartGame.Darts)
	DESTORY_HAND_BLOCKER(PlayerDarts)
	
	IF NOT bSpectator
			
		IF BUSYSPINNER_IS_ON()
			CDEBUG1LN(DEBUG_DARTS, "CLEANUP_MP_DARTS_GAME - Spinner OFF")
			BUSYSPINNER_OFF()
		ENDIF
			
		//### delete clone peds from intro
		INTRO_SCENE_CLEANUP(sLocalIntroScene)
					
		//##### TELEMETRY FOR GAME WIN, END
		INT iPointsEarned = MPDartGame.iGamesWon * CONST_MC_POINTS_FOR_WIN
		//Only Add points if won a game.
		IF iPointsEarned > 0
			CDEBUG1LN(DEBUG_DARTS, "DEBUG_DARTS TELEM: Minigame was WON, times: ",MPDartGame.iGamesWon, " MC points Awarded: ", iPointsEarned)
			PLAYSTATS_EARNED_MC_POINTS(
				GB_GET_GANG_BOSS_UUID_INT_A(GB_GET_LOCAL_PLAYER_GANG_BOSS()), 
				GB_GET_GANG_BOSS_UUID_INT_B(GB_GET_LOCAL_PLAYER_GANG_BOSS()),
				GB_GET_MATCH_ID_1_FOR_TELEMETRY(), 
				GB_GET_MATCH_ID_2_FOR_TELEMETRY(), 
				BIK_ADD_POINTS_MINIGAME,
				iPointsEarned)
		ENDIF
		
		//Telemetry minigame ended
		PLAYSTATS_MC_CLUBHOUSE_ACTIVITY(
			GB_GET_GANG_BOSS_UUID_INT_A(GB_GET_LOCAL_PLAYER_GANG_BOSS()), 
			GB_GET_GANG_BOSS_UUID_INT_B(GB_GET_LOCAL_PLAYER_GANG_BOSS()),
			GB_GET_MATCH_ID_1_FOR_TELEMETRY(), 
			GB_GET_MATCH_ID_2_FOR_TELEMETRY(), 
			ENUM_TO_INT(eMGID_DARTS), 
			-1, 
			0)
		CDEBUG1LN(DEBUG_DARTS, "DEBUG_DARTS TELEM: Minigame Ended")
		
		IF NETWORK_IS_SIGNED_ONLINE()
		AND (PARTICIPANT_ID_TO_INT() >= 0 AND PARTICIPANT_ID_TO_INT() <= NETWORK_GET_MAX_NUM_PARTICIPANTS())
			//FM MATCH END
			IF DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_FM_MATCH_STARTED)
				DEAL_WITH_FM_MATCH_END(
				FMMC_TYPE_MG_DARTS, 
				ServerData.iMatchHistoryID, 
				missionScriptArgs.mdID.idVariation, 
				PICK_INT(MPDartGame.bDartWinner, 1, 0), 
				PICK_INT(MPDartGame.bDartWinner, 1, 2))
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_DARTS, "DEBUG_DARTS CLEANUP: Cant deal with fm end, not signed online or participantId is Invalid.")
		ENDIF
		
		//No minigame is active 
		MGID_CLUBHOUSE_RESET()
		SHOW_SPEC_PLAYERS()
		CLEANUP_LEADERBOARD_CAM(TRUE, TRUE, TRUE, TRUE)
			
		IF NOT bPlayerQuit
			SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
			CDEBUG1LN(DEBUG_DARTS, "dart mission ended at leaderboard or timed out ")
		ENDIF
			
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)

			IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
			AND NOT IS_PLAYER_SCTV(PLAYER_ID()) // B*2339942
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			ENDIF
		ENDIF
			
		DISPLAY_AREA_NAME(TRUE)
			
		IF bRestoreUI
		AND NETWORK_IS_SIGNED_ONLINE()
			CDEBUG1LN(DEBUG_DARTS, "Restoring UI in Darts cleanup")
			
			DISABLE_CELLPHONE(FALSE)
			ENABLE_ALL_MP_HUD()
			
			DISPLAY_RADAR(TRUE)
			
			PLAYER_INDEX broadcastIndex
			broadcastIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(PARTICIPANT_ID_TO_INT()))
			CDEBUG1LN(DEBUG_DARTS, "Broadcasting darts clear to all players on team ", GET_PLAYER_TEAM(broadcastIndex))
			BROADCAST_DARTS_CLEAR(broadcastIndex, GET_PLAYER_TEAM(broadcastIndex), TRUE)
		ENDIF
	
		IF bQuitter
			CLEAR_PRINTS()
		ENDIF
			
		CLEAR_ALL_BIG_MESSAGES()
		SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_DARTS)
		
		INT iTemp1
		REPEAT NUM_DART_TURNS iTemp1
			CLEANUP_DART(MPDartGame.Darts[iTemp1])
		ENDREPEAT
		
		CLEANUP_BOARD(MPDartGame.dBoard)
		CLEANUP_RETICLE()
		
		CLEANUP_SOCIAL_CLUB_LEADERBOARD(DartsUI.dartsLB_control)
		
		IF NOT bLeaveArea
			CLEANUP_HUD()
		ENDIF
			
		IF (MPDartGame.iGamesWon = MPDartGame.iGamesLost)
		AND NOT bQuitter
			iTemp1 = ciFMMC_END_OF_MISSION_STATUS_CANCELLED
		ELIF (MPDartGame.iGamesWon > MPDartGame.iGamesLost)
			iTemp1 = ciFMMC_END_OF_MISSION_STATUS_PASSED
		ELSE
			iTemp1 = ciFMMC_END_OF_MISSION_STATUS_FAILED
		ENDIF
			
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		CLEAR_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL)
			
		IF NETWORK_IS_SIGNED_ONLINE()
		AND NOT bSoloMP
			#IF IS_DEBUG_BUILD
			IF NOT bDartsNoSocialClub
			#ENDIF
			WRITE_DARTS_SCLB_DATA(LEADERBOARD_MINI_GAMES_DARTS,TRUE)
			#IF IS_DEBUG_BUILD
			ELSE
			CDEBUG1LN(DEBUG_DARTS, "Debug used, not uploading stats to social club")
			ENDIF
			#ENDIF
		ELIF bSoloMP
			CDEBUG1LN(DEBUG_DARTS, "soloMP, no leaderboard write")
		ENDIF
			
		IF MPGlobals.DartsData.bResultsDisplayed
			MPGlobals.DartsData.bResultsDisplayed = FALSE
		ENDIF
			
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
		RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
			
		SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
			
		SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
		CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
			
		CLEANUP_DARTS_PC_CONTROLS()
			
		//gdisablerankupmessage = FALSE
		SET_DISABLE_RANK_UP_MESSAGE(FALSE)
		
		SET_ON_JOB_INTRO(FALSE)
			
		//@@@ Cameras
		IF DOES_CAM_EXIST(renderingCam)
			IF IS_CAM_ACTIVE(renderingCam)
				SET_CAM_ACTIVE(renderingCam, FALSE)
			ENDIF
			DESTROY_CAM(renderingCam)
		ENDIF
		
		IF DOES_CAM_EXIST(ciDartsQuit)
			IF IS_CAM_ACTIVE(ciDartsQuit)
				SET_CAM_ACTIVE(ciDartsQuit, FALSE)
			ENDIF
			DESTROY_CAM(ciDartsQuit)
		ENDIF
		
		IF DOES_CAM_EXIST(camDartsSync)
			IF IS_CAM_ACTIVE(camDartsSync)
				SET_CAM_ACTIVE(camDartsSync, FALSE)
			ENDIF
			DESTROY_CAM(camDartsSync)
		ENDIF
		
		IF DOES_CAM_EXIST(camEndScreen)
			IF IS_CAM_ACTIVE(camEndScreen)
				SET_CAM_ACTIVE(camEndScreen, FALSE)
			ENDIF
			DESTROY_CAM(camEndScreen)
		ENDIF
		RENDER_SCRIPT_CAMS(FALSE, FALSE) 
			
	ENDIF
		
	//##General
	RELEASE_ASSETS(bSpectator)
	
	CDEBUG1LN(DEBUG_DARTS, "SF STATE", DartsUI.inGameControlContext.sfMov <> NULL)	
	IF DartsUI.inGameControlContext.sfMov != NULL
		BEGIN_SCALEFORM_MOVIE_METHOD( DartsUI.inGameControlContext.sfMov, "CLEAR_ALL")
		END_SCALEFORM_MOVIE_METHOD()
		
		CLEANUP_SIMPLE_USE_CONTEXT(DartsUI.inGameControlContext)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(DartsSB.siScoreBoard)
	ENDIF
	CDEBUG1LN(DEBUG_DARTS, "SF STATE", DartsUI.inGameControlContext.sfMov <> NULL)

	SET_DARTS_PLAYER_COUNT(0)
	SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
	CDEBUG1LN(DEBUG_DARTS, "All Cleaned Up")
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()	
ENDPROC	

// BIKER 
//Determine which darts playboard vCoords are closest to, used to determine which board palyer needs to paly on , since boards are not linked to coronas.
FUNC DARTS_APARTMENT_TYPE GET_CLOSEST_DARTS_FROM_COORDS(VECTOR vCoords)
	//CDEBUG1LN(DEBUG_DARTS, "FIND CLOSEST DARTS LOCATION:")
	
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		RETURN APT_TYPE_ARCADE
	#IF FEATURE_DLC_2_2022
	ELIF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
		RETURN APT_TYPE_JUGGALO_HIDEOUT
	#ENDIF
	ENDIF
	
	DARTS_APARTMENT_TYPE closestLocationOut = BIKER_VARIANT_A
	//check if player is in apartment
	INT iCurrentApartment
	GET_APARTMENT_INTERIOR_I_AM_IN_BASED_ON_COORDS(vCoords, iCurrentApartment)
	
	IF iCurrentApartment > 0
		//STRING sPropName
		//sPropName = GET_PROPERTY_INTERIOR_NAME(0, iCurrentApartment)
		//CDEBUG1LN(DEBUG_DARTS, "Player Requested darts in apartment: ", iCurrentApartment, " Proeprty Name: ", sPropName)
		//Quick return if apartment determined.
		
		////////SWITCH iCurrentApartment
			///////////////////////////CASE 3 closestLocationOut = DARTSLOCATION_ROCK_CLUB
		////////ENDSWITCH
	ELSE
		//check by distance
		DARTS_LOCATION_INFO infoOne, infoTwo // store info about locations
		INT iCurrentClosestID = 1 // pick any by default
		FLOAT fCurrentDistance = 0
		//Current closest picked random for compare
		DARTS_APARTMENT_TYPE dlClosestLocation
		dlClosestLocation = BIKER_VARIANT_A
		GET_DARTS_LOCATION_INFO(infoOne,dlClosestLocation) // no need to always grab it
		fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(infoOne.vBoardPos,infoTwo.vBoardPos,TRUE)
		//CDEBUG1LN(DEBUG_DARTS, "CurrentDistance: ", fCurrentDistance)
		//loop all locations
		INT iCurrentID
		FOR iCurrentID = 0 TO COUNT_OF(DARTS_LOCATION) - 2 // turn size into index, num dont count
		//CDEBUG1LN(DEBUG_DARTS, "Current ID: ", iCurrentID)
			IF iCurrentID <> iCurrentClosestID
			//CDEBUG1LN(DEBUG_DARTS, "Valid for Id Check: ", iCurrentID, " Current: ", iCurrentClosestID)
				GET_DARTS_LOCATION_INFO(infoTwo, INT_TO_ENUM(DARTS_APARTMENT_TYPE,iCurrentID))
				//CDEBUG1LN(DEBUG_DARTS, "Darts Coord: ", infoTwo.vBoardPos)
				//CDEBUG1LN(DEBUG_DARTS, "Player Coord: ", vCoords)
				FLOAT newDistance = GET_DISTANCE_BETWEEN_COORDS(infoTwo.vBoardPos, vCoords, TRUE)
				//CDEBUG1LN(DEBUG_DARTS, "NewDistance: ", newDistance)
				IF newDistance < fCurrentDistance
				//CDEBUG1LN(DEBUG_DARTS, "New closer Location Selected: ")
					//Closer Location found.
					fCurrentDistance = newDistance
					iCurrentClosestID = iCurrentID
					dlClosestLocation = INT_TO_ENUM(DARTS_APARTMENT_TYPE,iCurrentID)
					infoOne = infoTwo
				ENDIF
			ENDIF
		ENDFOR
		closestLocationOut =  dlClosestLocation
	ENDIF
	//CDEBUG1LN(DEBUG_DARTS, "Closest Darts Location is :", closestLocationOut)
	//return smth by default in case fails to pick proper
	RETURN closestLocationOut
ENDFUNC

/// PURPOSE:
///    Do necessary pre game start ini.
/// RETURNS:
///    FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL DARTS_NETWORK_INIT(MP_MISSION_DATA & missionScriptArgs, DARTS_SERVER_BROADCAST_DATA & DartsServerData, DARTS_AP_PLAYER_BROADCAST_DATA & DartsPlayerData[MAXPLAYERS], BOOL bSpectator = FALSE)
	
	CLEAR_HELP()
	
	//If we are in freemode launch diffrently

		CDEBUG1LN(DEBUG_DARTS, "Set this script as a network script with 32 players.  FM VERSION")

		missionScriptArgs.mdID.idMission = eAM_APARTMENT_DARTS
		missionScriptArgs.mdID.idCreator = FMMC_TYPE_MG_DARTS
		missionScriptArgs.iInstanceId = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
		
		#IF FEATURE_CASINO_HEIST
		IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
			missionScriptArgs.iInstanceId = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance + 32
		ENDIF
		#ENDIF
		
	//	SETUP_MP_MISSION_FOR_NETWORK(MAXPLAYERS, missionScriptArgs)
	//	HANDLE_NET_SCRIPT_INITIALISATION()

		//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_DARTS
		//SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_APARTMENT_DARTS), FALSE, missionScriptArgs.iInstanceId) 
	HANDLE_NET_SCRIPT_INITIALISATION()
	SET_THIS_SCRIPT_CAN_BE_PAUSED(TRUE)
	REGISTER_SCRIPT_WITH_AUDIO(TRUE)
	SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_MINIGAME_STARTED)
	
	#IF FEATURE_CASINO_HEIST
	IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
		SET_FM_MISSION_LAUNCHED_SUCESS(missionScriptArgs.mdID.idCreator, missionScriptArgs.mdID.idVariation, missionScriptArgs.iInstanceId)
	ENDIF
	#ENDIF
	
	//These are common to all modes
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(DartsServerData, SIZE_OF(DartsServerData))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(DartsPlayerData, SIZE_OF(DartsPlayerData))

	iSelfID = PARTICIPANT_ID_TO_INT()
	
	CDEBUG1LN(DEBUG_DARTS, "*********ID Check********************")
	CDEBUG1LN(DEBUG_DARTS, "* iSelfID  = ", iSelfID)
	CDEBUG1LN(DEBUG_DARTS, "*************************************")
	
	IF bSpectator
	OR IS_PLAYER_SCTV(PLAYER_ID())
			
		IF bSpectator
			CDEBUG1LN(DEBUG_DARTS, "FTR THIS IS STARTING AS SPECTATOR")
			DARTS_SET_CLIENT_FLAG(DartsPlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
		ENDIF
		
		IF IS_PLAYER_SCTV(PLAYER_ID())
			CDEBUG1LN(DEBUG_DARTS, "FTR THIS IS STARTING AS SCTV")
			DARTS_SET_CLIENT_FLAG(DartsPlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SCTV)
			DARTS_CLEAR_CLIENT_FLAG(DartsPlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
		ENDIF
		
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "FTR THIS IS NOT SPECTATING")
	ENDIF
	
	//DARTS_SET_PLAYER_MP_STATE(DartsPlayerData[iSelfID], DARTS_MPSTATE_WAIT_PRE_INIT)
	SWITCH_MPSTATE(DartsPlayerData[iSelfID].eMPState, DARTS_MPSTATE_WAIT_PRE_INIT)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		DARTS_SET_SERVER_MP_STATE(DartsServerData, DARTS_MPSTATE_INIT)
		//DartsServerData.iMatchHistoryID = PLAYSTATS_CREATE_MATCH_HISTORY_ID()
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(DartsServerData.iMatchType, DartsServerData.iMatchHistoryID)
	
	ENDIF
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DARTS_LEVEL_HELP_SHOW(INT iNumGames, INT & iBitToSet)
	IF iNumGames >= 10
		IF NOT IS_BIT_SET(g_savedGlobals.sDartsData.iDartTimePlayed, gDARTS_MP_LVL_UP_SHWN)
			iBitToSet = gDARTS_MP_LVL_UP_SHWN
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//  take in a vector from the other player, and your own darts
// Draw the sprite reticle at that point
// Sprite reticle has to be different color, so make sure a function is ready for that
PROC DARTS_DRAW_PLAYERS_RETICLE(VECTOR vReticlePos, INT iDartPlayerID, BOOL bWatching = FALSE)
	spriteReticle.x = vReticlePos.x
	spriteReticle.y = vReticlePos.y
	IF iDartPlayerID = 0
		// color - blue
		spriteReticle.r = 255
		spriteReticle.g = 255
		spriteReticle.b = 255
	ELIF iDartPlayerID = 1
		// color - red
		spriteReticle.r = 255
		spriteReticle.g = 255
		spriteReticle.b = 255
	ELSE
		// color - white
		spriteReticle.r = 255
		spriteReticle.g = 250
		spriteReticle.b = 255
	ENDIF
	
	IF bWatching
		//DRAW_2D_SPRITE("MPCharCard", "MP_CharCard_Stats_Icons5", spriteReticle)
	ENDIF
ENDPROC

PROC COPY_DART_TO_LOCAL_INFO(DART & localDart, DARTS_AP_PLAYER_BROADCAST_DATA & thatPlayerData)
	localDart.vPosition 	= thatPlayerData.ClientDart.vPosition
	localDart.vThrow 		= thatPlayerData.ClientDart.vThrow
	localDart.vTarget 		= thatPlayerData.ClientDart.vTarget	
	localDart.vRotation 	= thatPlayerData.ClientDart.vRotation
	localDart.vOffsetTarget = thatPlayerData.ClientDart.vOffsetTarget
	localDart.bStuck 		= thatPlayerData.ClientDart.bStuck
	localDart.bTravelling 	= thatPlayerData.ClientDart.bTravelling
	
ENDPROC

PROC COPY_DART_TO_LOCAL(DART & localDart, DARTS_AP_PLAYER_BROADCAST_DATA & thatPlayerData, INT iOtherPlayerID = 0)
	localDart.vPosition 	= thatPlayerData.ClientDart.vPosition
	localDart.vThrow 		= thatPlayerData.ClientDart.vThrow
	localDart.vTarget 		= thatPlayerData.ClientDart.vTarget	
	localDart.vRotation 	= thatPlayerData.ClientDart.vRotation
	localDart.vOffsetTarget = thatPlayerData.ClientDart.vOffsetTarget
	localDart.bStuck 		= thatPlayerData.ClientDart.bStuck
	localDart.bTravelling 	= thatPlayerData.ClientDart.bTravelling
	
	MODEL_NAMES otherDart
	
	otherDart = modelDart1
	
	iOtherPlayerID = iOtherPlayerID
//	
//	IF iOtherPlayerID = 0
//		otherDart = modelDart1
//		CDEBUG1LN(DEBUG_DARTS, "**********PROP_DART_1 registered**********")
//	ELSE
//		otherDart = modelDart2
//		CDEBUG1LN(DEBUG_DARTS, "**********PROP_DART_2 registered**********")
//	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "**********localDart.vTarget = ", localDart.vTarget)
	CDEBUG1LN(DEBUG_DARTS, "**********localDart.vOffsetTarget = ", localDart.vOffsetTarget)
	CDEBUG1LN(DEBUG_DARTS, "**********localDart.vThrow = ", localDart.vThrow)
	
	localDart.object = CREATE_OBJECT(otherDart, vInitDartPos, FALSE, FALSE)
ENDPROC

FUNC BOOL DARTS_PLAYER_QUIT_CONTROL(DARTS_AP_PLAYER_BROADCAST_DATA & playerData, DARTS_UI & DartsUI, INT & iDebugThrottle)
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
	AND NOT IS_PAUSE_MENU_ACTIVE()
		IF NOT DARTS_CHECK_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
			DARTS_SET_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	IF DARTS_CHECK_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
		DEBUG_MESSAGE_PERIODIC("CLIENT DARTS_MPSTATE_LEAVE", iDebugThrottle)
		DARTS_QUIT_UI(DartsUI)
		
		IF NOT IS_PAUSE_MENU_ACTIVE()
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				CLEAR_PRINTS()
				DARTS_SET_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_CONFIRMED)
				bPlayerQuit = TRUE
				RETURN TRUE
			ENDIF
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SETUP_QUITUI, FALSE)
				DARTS_CLEAR_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DARTS_PLAYER_QUIT_CONTROL_SPECTATOR(DARTS_AP_PLAYER_BROADCAST_DATA & playerData, DARTS_UI & DartsUI, INT & iDebugThrottle)
	
	IF DARTS_CHECK_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
		DEBUG_MESSAGE_PERIODIC("CLIENT DARTS_MPSTATE_LEAVE", iDebugThrottle)
		DARTS_QUIT_UI(DartsUI)
		
		IF NOT IS_PAUSE_MENU_ACTIVE()
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				CLEAR_PRINTS()
				bPlayerQuit = TRUE
				RETURN TRUE
			ENDIF
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SETUP_QUITUI, FALSE)
				DARTS_CLEAR_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
			ENDIF
		ENDIF
	ELSE
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		AND NOT IS_PAUSE_MENU_ACTIVE()
			IF NOT DARTS_CHECK_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
				DARTS_SET_CLIENT_FLAG(playerData, DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

CONST_FLOAT fBoard_offsetZ -0.7744


			
#IF IS_DEBUG_BUILD
	INT fOneD, fTwoD
	USING "shared_debug.sch"

	BOOL bDangerZoneOccupied
	BOOL bWinDartsGame
	BOOL bTurnOffShakes

	PROC ADD_AM_DART_WIDGETS()
		START_WIDGET_GROUP("AM_DARTS")
			ADD_WIDGET_BOOL("Occupy Danger Zone", bDangerZoneOccupied)
			ADD_WIDGET_BOOL("Win Game", bWinDartsGame)
			ADD_WIDGET_BOOL("Turn off shake", bTurnOffShakes)
			
			ADD_WIDGET_INT_READ_ONLY("ONE", fOneD)
			ADD_WIDGET_INT_READ_ONLY("TWO", fTwoD)
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC PROCESS_WIDGETS(DARTS_SERVER_BROADCAST_DATA & ServerData)
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF bDangerZoneOccupied
			
				ServerData.bDangerZoneOccupied = TRUE
			ELSE
				ServerData.bDangerZoneOccupied = FALSE
			ENDIF
			
			IF bWinDartsGame
				ServerData.bDoWinning = TRUE
				bWinDartsGame = FALSE
			ENDIF
		ENDIF
		
		IF bTurnOffShakes
			fWobbleFactor = 0.0
			fRealWobFactor = 0.0
		ENDIF
		
	ENDPROC
	
#ENDIF

FUNC BOOL DARTS_TUTORIAL(DARTS_TUTORIAL_STAGE & DartsTutorialStage, DARTS_BOARD & DartsBoard, DARTS_AI_BRAIN & AIBrain, 
						DARTS_PLAYER_STATE & TurnStage, DART & TutorialDarts[], structTimer & TutorialTimer, DARTS_AP_PLAYER_BROADCAST_DATA & playerData[])
	
//	IF HANDLE_SKIP_CUTSCENE(iAllowSkipCutsceneTime)
//		DartsTutorialStage = TS_SKIP
//	ENDIF
	
//	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
//		IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_SKIP)
//			DARTS_SET_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_SKIP)
//		ELSE
//			DARTS_CLEAR_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_SKIP)
//		ENDIF
//	ENDIF
	
	IF DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_SKIP)
	AND DARTS_CHECK_CLIENT_FLAG(playerData[iOtherID], DARTS_CLIENTFLAG_TUTORIAL_SKIP)
		//DO_SCREEN_FADE_OUT(500)
//		WHILE NOT IS_SCREEN_FADED_OUT()
//			WAIT(0)
//		ENDWHILE
		
		DartsTutorialStage = TS_SKIP
	ENDIF
	
	SWITCH DartsTutorialStage
		CASE TS_THROWING
			//iAllowSkipCutsceneTime = GET_GAME_TIMER()
			
			RESTART_TIMER_NOW(TutorialTimer)
			
			DISPLAY_RADAR(FALSE)	
			
			DARTS_TUTORIAL_CAM(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
								GET_TUTORIAL_CAM_COORD_OFFSET(TS_THROWING), GET_TUTORIAL_CAM_ROT_OFFSET(), 0, TRUE)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			IF (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT())
			AND NOT IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				CDEBUG1LN(DEBUG_DARTS, "[DARTS_MPSTATE_TUTORIAL] fade in called for")
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			CDEBUG1LN(DEBUG_DARTS, "Finished TS_THROWING")
			DartsTutorialStage = TS_DART_LANDING
		BREAK
		
		CASE TS_DART_LANDING			
			
			SWITCH iTurn
				CASE 0
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 1.0
						
						PRINT_HELP("DARTS_HOW_TO_2")
						RESTART_TIMER_NOW(TutorialTimer)
						iTurn++
						
					ENDIF
				BREAK
				
				CASE 1
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 2.0
						IF TurnStage = DPS_AIM 
							AIBrain.iThrowValue = 20
							AIBrain.iThrowMultiplier = 3
							GET_AI_THROW_VECTOR(TutorialDarts[0], DartsBoard, TurnStage, AIBrain, TRUE)
						ENDIF
			
						IF TurnStage = DPS_THROW
							IF THROW_DART(TutorialDarts[0], DartsBoard)
								PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER", DartsBoard.vDartBoard)
								iTurn++
								RESTART_TIMER_NOW(TutorialTimer)
								TurnStage = DPS_AIM
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 5.0
						
						PRINT_HELP("DARTS_HOW_TO_3")
						RESTART_TIMER_NOW(TutorialTimer)
						iTurn++
						
					ENDIF
				BREAK
				
				CASE 3
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 2.0
						IF TurnStage = DPS_AIM 
							AIBrain.iThrowValue = 50
							AIBrain.iThrowMultiplier = 1
							GET_AI_THROW_VECTOR(TutorialDarts[1], DartsBoard, TurnStage, AIBrain, TRUE)
						ENDIF
			
						IF TurnStage = DPS_THROW
							IF THROW_DART(TutorialDarts[1], DartsBoard)
								PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER", DartsBoard.vDartBoard)
								RESTART_TIMER_NOW(TutorialTimer)
								TurnStage = DPS_AIM
								iTurn++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 4
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 5.0
						
						PRINT_HELP("DARTS_HOW_3A")
						RESTART_TIMER_NOW(TutorialTimer)
						iTurn++
						
					ENDIF
				BREAK
				
				CASE 5
					IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 2.0
						
						IF TurnStage = DPS_AIM
							AIBrain.iThrowValue = 20
							AIBrain.iThrowMultiplier = 2
							GET_AI_THROW_VECTOR(TutorialDarts[2], DartsBoard, 
												TurnStage, AIBrain, TRUE)
							CDEBUG1LN(DEBUG_DARTS, "*****************Got the last Throw Vector")
						ENDIF
			
						IF TurnStage = DPS_THROW
							IF THROW_DART(TutorialDarts[2], DartsBoard)
								RESTART_TIMER_NOW(TutorialTimer)
								PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER", DartsBoard.vDartBoard)
								CDEBUG1LN(DEBUG_DARTS, "*****************Threw the last Dart")
								TurnStage = DPS_AIM					
								DartsTutorialStage = TS_DOUBLE
								
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE TS_DOUBLE
			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 5.0
				RESTART_TIMER_NOW(TutorialTimer)
				CLEAR_HELP()
				DARTS_TUTORIAL_CAM_INTERP(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
											GET_TUTORIAL_CAM_COORD_OFFSET(TS_DOUBLE), GET_TUTORIAL_CAM_ROT_OFFSET(), 1500)
				//HELP_AT_ENTITY_OFFSET("DARTS_DOUBLE_T", DartGame.TutorialDarts[0].object, << 0, 0, -0.85 >>)
				DartsTutorialStage = TS_TRIPLE
				CDEBUG1LN(DEBUG_DARTS, "Leaving TS_DOUBLE")
			ENDIF
		BREAK
		
		CASE TS_TRIPLE
			IF NOT IS_CAM_INTERPOLATING(camTutInterp)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_DOUBLE_T")
				PRINT_HELP("DARTS_DOUBLE_T")
				
			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_DOUBLE_T")
			//	HELP_AT_ENTITY_OFFSET("DARTS_DOUBLE_T", DartGame.TutorialDarts[0].object, << 0, 0, -0.85 >>)
			ENDIF
			
			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 6.0
				RESTART_TIMER_NOW(TutorialTimer)
				DARTS_TUTORIAL_CAM(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
									GET_TUTORIAL_CAM_COORD_OFFSET(TS_TRIPLE), GET_TUTORIAL_CAM_ROT_OFFSET(), 1000)
				//HELP_AT_ENTITY("DARTS_TRIPLE_T", DartGame.TutorialDarts[0].object)
				//CLEAR_THIS_FLOATING_HELP("DARTS_DOUBLE_T")
				DartsTutorialStage = TS_BULLSEYE
				CDEBUG1LN(DEBUG_DARTS, "Leaving TS_TRIPLE")
			ENDIF
		BREAK
		
		CASE TS_BULLSEYE
			IF NOT IS_CAM_INTERPOLATING(camTutorial)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_TRIPLE_T")
				PRINT_HELP("DARTS_TRIPLE_T")
				
			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_TRIPLE_T")
			//	HELP_AT_ENTITY("DARTS_TRIPLE_T", DartGame.TutorialDarts[0].object)
			ENDIF
			
			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 6.0
				RESTART_TIMER_NOW(TutorialTimer)
				DARTS_TUTORIAL_CAM_INTERP(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
											GET_TUTORIAL_CAM_COORD_OFFSET(TS_BULLSEYE), GET_TUTORIAL_CAM_ROT_OFFSET(), 1000)
				//HELP_AT_ENTITY("DARTS_BULL_T", DartGame.TutorialDarts[1].object)
				//CLEAR_THIS_FLOATING_HELP("DARTS_TRIPLE_T")
				DartsTutorialStage = TS_DBL_TO_WIN
				CDEBUG1LN(DEBUG_DARTS, "Leaving TS_BULLSEYE")
			ENDIF
		BREAK
		
		CASE TS_DBL_TO_WIN
			IF NOT IS_CAM_INTERPOLATING(camTutInterp)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_BULL_T")
				PRINT_HELP("DARTS_BULL_T")
				
			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_BULL_T")
			//	HELP_AT_ENTITY("DARTS_BULL_T", DartGame.TutorialDarts[1].object)
			ENDIF
			
			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 6.0
				RESTART_TIMER_NOW(TutorialTimer)
				DARTS_TUTORIAL_CAM(DartsBoard.vDartBoard, DartsBoard.fBoardHeading,
									GET_TUTORIAL_CAM_COORD_OFFSET(TS_DBL_TO_WIN), GET_TUTORIAL_CAM_ROT_OFFSET(), 1500)
				//HELP_AT_ENTITY("DARTS_DBL_WIN", DartGame.TutorialDarts[2].object)
				//CLEAR_THIS_FLOATING_HELP("DARTS_BULL_T")
				DartsTutorialStage = TS_ENDING
				CDEBUG1LN(DEBUG_DARTS, "Leaving TS_DBL_TO_WIN")
			ENDIF
		BREAK
		
		CASE TS_ENDING
			IF NOT IS_CAM_INTERPOLATING(camTutorial)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_DBL_WIN")
				PRINT_HELP("DARTS_DBL_WIN")
				
			//AND NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("DARTS_DBL_WIN")
			//	HELP_AT_ENTITY("DARTS_DBL_WIN", DartGame.TutorialDarts[2].object)
			ENDIF
			
			IF GET_TIMER_IN_SECONDS_SAFE(TutorialTimer) > 6.0 //TIMERA() > 6000
				
				SET_CAM_ACTIVE_WITH_INTERP(camDartGame, camTutorial, 1500)
				//CLEAR_THIS_FLOATING_HELP("DARTS_DBL_WIN")
				DartsTutorialStage = TS_ENDING2
				CDEBUG1LN(DEBUG_DARTS, "Leaving TS_ENDING")
			ENDIF
		BREAK
		
		CASE TS_ENDING2
			IF NOT IS_CAM_INTERPOLATING(camDartGame)
				iTurn = 0
				CLEAR_HELP()
				CLEAR_PRINTS()
				CLEAR_ALL_FLOATING_HELP()
				INT i
				REPEAT NUM_DART_TURNS i
					CLEANUP_DART(TutorialDarts[i])
				ENDREPEAT
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TS_SKIP
			//IF IS_SCREEN_FADED_OUT()
				iTurn = 0
				CLEAR_HELP()
				CLEAR_PRINTS()
				CLEAR_ALL_FLOATING_HELP()
				INT i
				REPEAT NUM_DART_TURNS i
					CLEANUP_DART(TutorialDarts[i])
				ENDREPEAT
				
				SET_CAM_ACTIVE(camDartGame, TRUE)
				
				//DO_SCREEN_FADE_IN(500)
				
				RETURN TRUE
			//ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC DARTS_SETUP_MP_QUIT_CAM(CAMERA_SETUP camSetup)
	CDEBUG1LN(DEBUG_DARTS, "QUIT CAM SETUP")
	IF NOT DOES_CAM_EXIST(ciDartsQuit)
		ciDartsQuit = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>)
	ENDIF
	
	SET_CAM_COORD(ciDartsQuit, camSetup.vPosition)
	SET_CAM_ROT(ciDartsQuit, camSetup.vRotation)
	SET_CAM_FOV(ciDartsQuit,camSetup.fFOV)
	SET_CAM_ACTIVE(ciDartsQuit, TRUE)
	
ENDPROC

/// PURPOSE:
///    takes off high heels, ONLY USE FOR CLONED PEDS
/// PARAMS:
///    dartPed - one of the players
PROC DARTS_TAKE_OFF_HIGH_HEELS(PED_INDEX & dartPed)
	SET_PED_COMPONENT_VARIATION(dartPed, PED_COMP_FEET, 1, 0)
ENDPROC


FUNC BOOL IS_DARTS_GAME_ALMOST_DONE(DARTS_SERVER_BROADCAST_DATA & ServerData, DARTS_AP_PLAYER_BROADCAST_DATA & PlayerData[], INT &iDartPlayerID[DART_PLAYER_IDS])
	
	BOOL bGameAlmostDone
	
	// sets and legs are both 1, score is at least < 100
	// sets > 1, set amount is greater then set max * 66%
	// sets = 1, leg amount is greater then leg max * 66%
	
	IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
		IF ((serverData.iNumLegs = 1 AND serverData.iNumSets = 1) AND (ServerData.iScores[DARTS_PLAYER_ONE] < 101 OR ServerData.iScores[DARTS_PLAYER_TWO] < 101))
		OR ( serverData.iNumSets > 1 AND (serverData.iSets[DARTS_PLAYER_ONE] >= FLOOR(serverData.iNumSets * 0.66) OR serverData.iSets[DARTS_PLAYER_TWO] >= FLOOR(serverData.iNumSets * 0.66)) )
		OR ( serverData.iNumLegs > 1 AND (serverData.iLegs[DARTS_PLAYER_ONE] >= FLOOR(serverData.iNumLegs * 0.33) OR serverData.iLegs[DARTS_PLAYER_TWO] >= FLOOR(serverData.iNumLegs * 0.33)) )
			CDEBUG1LN(DEBUG_DARTS, "dart game is almost done")
			bGameAlmostDone = TRUE
		ELSE
			CDEBUG1LN(DEBUG_DARTS, "dart game is NOT almost done")
		ENDIF
		
		CDEBUG1LN(DEBUG_DARTS, "serverData.iNumLegs = ", serverData.iNumLegs)
		CDEBUG1LN(DEBUG_DARTS, "serverData.iNumSets = ", serverData.iNumSets)
		CDEBUG1LN(DEBUG_DARTS, "serverData.iSets[iSelfID] = ", serverData.iSets[DARTS_PLAYER_ONE])
		CDEBUG1LN(DEBUG_DARTS, "serverData.iSets[iOtherID] = ", serverData.iSets[DARTS_PLAYER_TWO])
		CDEBUG1LN(DEBUG_DARTS, "serverData.iLegs[iSelfID] = ", serverData.iLegs[DARTS_PLAYER_ONE])
		CDEBUG1LN(DEBUG_DARTS, "serverData.iLegs[iOtherID] = ", serverData.iLegs[DARTS_PLAYER_TWO])
		CDEBUG1LN(DEBUG_DARTS, "serverData.iScores[iSelfID] = ", serverData.iScores[DARTS_PLAYER_ONE])
		CDEBUG1LN(DEBUG_DARTS, "serverData.iScores[iOtherID] = ", serverData.iScores[DARTS_PLAYER_TWO])
		
	ENDIF
	
	RETURN bGameAlmostDone
	
ENDFUNC

PROC DARTS_DISPLAY_SPECIAL_SPLASH_MP_SHARD(DARTS_UI & DartsUI)
	IF DartsUI.bTriggerSplash
		IF DartsUI.eSplashStage = DARTSSPLASHSTAGE_DONE
			DartsUI.eSplashStage = DARTSSPLASHSTAGE_INIT
		ENDIF
		DartsUI.bTriggerSplash = FALSE
	ENDIF
	
	SWITCH DartsUI.eSplashStage
		CASE DARTSSPLASHSTAGE_INIT
			SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, DARTS_GET_SPLASH_STRING(DartsUI.eWhichSplash), "", 2000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
			DartsUI.eSplashStage = DARTSSPLASHSTAGE_DISPLAY
		BREAK
		CASE DARTSSPLASHSTAGE_DISPLAY
			IF NOT UPDATE_SHARD_BIG_MESSAGE(shardMidSizeMessage, FALSE)
				RESET_SHARD_BIG_MESSAGE(shardMidSizeMessage)
				DartsUI.eSplashStage = DARTSSPLASHSTAGE_DONE
			ENDIF
		BREAK
		CASE DARTSSPLASHSTAGE_DONE
			// nada
		BREAK
	ENDSWITCH
ENDPROC



SCRIPT (MP_MISSION_DATA missionScriptArgs)
		
	//SPEC
	STRUCT_INTRO_SCENE		sLocalIntroScene
	vInitLostHideoutPos = 	vInitLostHideoutPos
	BOOL					bSpectator = FALSE
	BOOL					bSetupPlayer = FALSE
	STRUCT_DART_RECEIVE 	SpecDartReceiveInfo
	BOOL					bSinglePlayerGame = TRUE
	BOOL					bIsHost = FALSE
	CAMERA_INDEX 			camDartPreGame
	ENUM_INTRO_SEQUENCE		eIntroSequence = eIT_INIT
	BOOL					bPlayerSideA = FALSE
	
	INT	iLocalStateFlags
	INT iCleanupFlags
	 
	vInitDartPos = <<1,1,1>>
	//BOOL 			bControl = FALSE
	//spec stuff
	//BOOL			bPlayerTwoDartsCreated
	DARTS_MP_WAIT_STAGE eSpecWaitStage
	DARTS_MP_TURN_STATUS eSpecTurnStatus
	structTimer SpectateGateTimer
	//for use by spectators / sctv
	INT iDartPlayerID[DART_PLAYER_IDS]
	STRUCT_PLAYER_INFO PlayerDarts[DART_PLAYER_IDS]
	//set invalid ids
	iDartPlayerID[DARTS_PLAYER_ONE] = -1
	iDartPlayerID[DARTS_PLAYER_TWO] = -1
	
	INT iOpponentDartID = 0
	INT iSelfDartID = 0
	
	#IF IS_DEBUG_BUILD
	INT iSpecTurn
	#ENDIF
	INT iSpecThower
	BOOL bSpecResetDone
	PED_INDEX piOtherPlayer
	
	MP_DARTS_GAME MPDartGame 
	DARTS_ARGS Args
	DARTS_STARTING_RETICLE_POS DartsInitReticlePos
	DARTS_PLAYER_POS DartsPlayerPos
	DARTS_MP_TERMINATE_REASON terminateReason
	DARTS_MP_LEAVE_STATE leaveState
	DARTS_SCOREBOARD DartsSB
	DARTS_UI DartsUI
	DARTS_TIMER localDartsTimer //for use with the rematch/quit clock
	DART_PLAYER_POSITION DartPlayerPosition
	DARTS_APARTMENT_TYPE DartsLocation
	
	DARTS_TUTORIAL_STAGE DartsTutorialStage
	DARTS_AI_BRAIN AIBrain
	DARTS_PLAYER_STATE TurnStage = DPS_AIM
	
	GAMER_HANDLE DartsGamerHandle[NUM_DART_PLAYERS]
	
	SCRIPT_TIMER timeDartsAwardFailSafe
	
	structTimer controlTimer
	structTimer stFlowCheck
	structTimer TimerInPosition
	structTimer ReThrowTimer
	structTimer TutorialTimer
	structTimer endDelay
	structTimer	stGameViewDelay
	structTimer stDesync
	BOOL bSyncComplete
	
	GENERIC_MINIGAME_STAGE eGenericMGStage = GENERIC_CELEBRATION_WINNER
	PLAYER_INDEX winningPlayer
	
	PLAYER_INDEX broadcastIndex

	SEQUENCE_INDEX siTemp
	
	PED_INDEX piPlayerClones[2]
	
	INT i, j
	INT iSplashStage
	INT iDebugThrottle
	INT iStaminaXP
	INT iLevelBit
	
	INT iBullseyeXP
	INT iXPGained
	INT iXPToGive
	
	INT iBetWinnings
	
	INT iNumParticipants
	INT iLastNumParticipants
	INT iNumPlayers
	INT iLastNumPlayers
		
	FLOAT fStaminaPercentage
	FLOAT fSteadyShotLength
	//FLOAT fSpectateHeading = 42.3856
	
	BOOL bFoundPlayer
	//BOOL bAlreadyInGangHouse
	BOOL bWaitUIShown // TODO:  CONVERT TO FLAG SYSTEM
	BOOL bBoardTakenUIShown // TODO:  CONVERT TO FLAG SYSTEM
	BOOL bInWinRange
	BOOL bWaitUIShow
	BOOL bShowSplash
	BOOL bShowShotClockThrowHelp
	//BOOL bSpecResetDone
	BOOL bCheckPlayers
	BOOL bPlaySplashFX
	BOOL bPhaseGrabbed
	
	TEXT_LABEL_7 sCrewTags[NUM_DART_PLAYERS]
	
	TEXT_LABEL_63 sShardPlayerName
	TEXT_LABEL_23 contentID	
	
	STRING sDartsPlayerNames[NUM_DART_PLAYERS]
	
		////////////////////////////TEST SETUP
	DARTS_APARTMENT_TYPE dsCurrent
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		dsCurrent = GET_CLOSEST_DARTS_FROM_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	
	DARTS_LOCATION_INFO infoOut
	GET_DARTS_LOCATION_INFO(infoOut, dsCurrent)
	CDEBUG1LN(DEBUG_DARTS, "Darts game started at location: ", dsCurrent)
	//basic
	Args.vDartBoard = infoOut.vBoardPos
	Args.fBoardHeading = infoOut.vBoardHead
	DartsLocation = dsCurrent
	
	fScaledWobble = 0.06
	fScaledRealWobble = 0.08
	fWobbleFactor = fScaledWobble
	fRealWobFactor = fScaledRealWobble
	
	MPGlobals.DartsData.bDartsPlayBusy = FALSE
	bShowSplash = FALSE

	IF IS_BIT_SET(missionScriptArgs.iBitSet, ENUM_TO_INT(EMGPT_JOINED_AS_SPECTATOR))
		bSpectator = TRUE
	ENDIF
	CDEBUG1LN(DEBUG_DARTS, "START INFO - IS PLAYER SPECTATOR?: ", bSpectator)
	
	IF IS_BIT_SET(missionScriptArgs.iBitSet, ENUM_TO_INT(EMGPT_JOINED_AS_SETUP))
		bSetupPlayer = TRUE
	ENDIF
	CDEBUG1LN(DEBUG_DARTS, "START INFO - IS PLAYER SETUP?: ", bSetupPlayer)
	
	IF IS_BIT_SET(missionScriptArgs.iBitSet, ENUM_TO_INT(eMGPT_CLOSEST_TO_A_SIDE))
		bPlayerSideA = TRUE
	ENDIF
	CDEBUG1LN(DEBUG_DARTS, "START INFO - IS PLAYER ON SIDE A?: ", bPlayerSideA)
	

	DARTS_SERVER_BROADCAST_DATA ServerData
	DARTS_AP_PLAYER_BROADCAST_DATA PlayerData[MAXPLAYERS]
	//WAIT(20000) //test
	IF NOT DARTS_NETWORK_INIT(missionScriptArgs, ServerData, PlayerData, bSpectator)
		CDEBUG1LN(DEBUG_DARTS, "Failed to receive an initial network broadcast. Cleaning up.")
		CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB, PlayerData, PlayerDarts, MPDartGame, DartsUI, FALSE)
	ENDIF
	
	IF bSpectator
		DARTS_SET_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
	ENDIF
	
	IF bPlayerSideA
		DARTS_SET_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLEINTFLAG_A_SIDE)
	ENDIF
	
	//##### Set global to see if minigame is still active.
	IF NOT bSpectator
		BROADCAST_MG_APARTMENTS_LAUNCHED(eMGID_DARTS, missionScriptArgs.iInstanceId)
		MGID_CLUBHOUSE_SET(eMGID_DARTS)
		SETUP_DARTS_PC_CONTROLS()
	ENDIF
	
	IF bSetupPlayer
		DARTS_SET_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_I_AM_SETUP_PLAYER)
		serverData.iNumLegs = g_FMMC_STRUCT.iNumberOfLegs
		serverData.iNumSets = g_FMMC_STRUCT.iNumberOfSets
	ENDIF
		
	CDEBUG1LN(DEBUG_DARTS, "Participant ID: ", PARTICIPANT_ID_TO_INT())

	PlayerData[iSelfID].iParticipantID = iSelfID
	DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_PARTICIPANT_SET)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PlayerData[iSelfID].iOffsetPlayerID = ENUM_TO_INT(DART_PLAYER_HOME)
	ELSE
		PlayerData[iSelfID].iOffsetPlayerID = ENUM_TO_INT(DART_PLAYER_AWAY)
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "JOB ENTRY TYPE START: ", GET_FM_JOB_ENTERY_TYPE())
	CDEBUG1LN(DEBUG_DARTS, "Number Of Players: ", NETWORK_GET_NUM_PLAYERS(PlayerData))
	
	//######### INIT BOARD , start moving palyer to palying position. 
	IF NOT bSpectator
		CLIENT_SET_UP_PROXIMITY_CHAT(CHAT_PROXIMITY_MAX)
		REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_DARTS()
		SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_CLUB_HOUSE)
		SET_AUDIO_FLAG("AllowRadioOverScreenFade", TRUE)
		INIT_BOARD(MPDartGame.dBoard, Args)
		GET_WORLD_OFFSETS(MPDartGame.dBoard, DartsPlayerPos, DartsInitReticlePos, TRUE)	
		INIT_PLAYER_POSITIONS(bPlayerSideA, DartsPlayerPos, MPDartGame.dBoard) 
		CDEBUG1LN(DEBUG_DARTS, "££££££££££££££££")
		CDEBUG1LN(DEBUG_DARTS, "Player Side A?", bPlayerSideA, " Pos:", DartsPlayerPos.vDartPlayrPos)
		CDEBUG1LN(DEBUG_DARTS, "Opponent Pos:", DartsPlayerPos.vOpponentPos)
		CDEBUG1LN(DEBUG_DARTS, "Dart Board Position: ", MPDartGame.dBoard.vDartBoard)
		CDEBUG1LN(DEBUG_DARTS, "Dart Board Heading: ", MPDartGame.dBoard.fBoardHeading)
		CDEBUG1LN(DEBUG_DARTS, "££££££££££££££££")	
	ENDIF
	
	//################ TRY TO FIND OPPONENTS		
	WHILE NOT bFoundPlayer
		NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
		//check if not inside the apartment, terminate.
		IF NOT IS_PLAYER_ENTERING_PROPERTY(GET_PLAYER_INDEX())
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			//check if script sohuld terminate - if outside an apartment
			IF NOT IS_PLAYER_IN_MP_PROPERTY(GET_PLAYER_INDEX(), FALSE)
			AND NOT IS_PLAYER_IN_CLUBHOUSE(GET_PLAYER_INDEX())
			AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			#IF FEATURE_DLC_2_2022
			AND NOT IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
			#ENDIF
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "DARTS APARTMENT - TERMINATE NOT INSIDE THE BUILDING")
							IF PlayerData[iSelfID].eMPState < DARTS_MPSTATE_LEAVE
				SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_LEAVE)
			ENDIF
			ENDIF
		ENDIF
		
		#IF FEATURE_CASINO
		IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
		AND IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "DARTS APARTMENT - TERMINATE NOT INSIDE THE BUILDING")
			IF PlayerData[iSelfID].eMPState < DARTS_MPSTATE_LEAVE
				SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_LEAVE)
			ENDIF
		ENDIF
		#ENDIF
		
		MAINTAIN_CELEBRATION_PRE_LOAD(sCelebrationData)
		
		iNumberOfPlayers = NETWORK_GET_NUM_PLAYERS(PlayerData)
		
		IF NOT NETWORK_IS_IN_SESSION()
			CDEBUG1LN(DEBUG_DARTS, "NETWORK_IS_IN_SESSION() is FALSE, cleaning up")
			CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB, PlayerData, PlayerDarts, MPDartGame, DartsUI, FALSE)
		
		ELIF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
		OR IS_PLAYER_SCTV(PLAYER_ID())
			IF (GET_GAME_TIMER() % 30000) < 50
				CDEBUG1LN(DEBUG_DARTS, "SPEC AND SCTV")
			ENDIF
			
			IF iNumberOfPlayers >= 2
				IF (GET_GAME_TIMER() % 1000) < 50
					CDEBUG1LN(DEBUG_DARTS, "2 player <- delay for initialize of other real players.")
				ENDIF
			
				IF NOT IS_TIMER_STARTED(controlTimer)
					START_TIMER_NOW(controlTimer)
				ELIF GET_TIMER_IN_SECONDS(controlTimer) > 4.0
						bFoundPlayer = TRUE
						CDEBUG1LN(DEBUG_DARTS, "am_darts: joining as spectator for two player game")
				ENDIF

				
			ELIF iNumberOfPlayers >= 1
				
				IF NOT IS_TIMER_STARTED(controlTimer)
					CDEBUG1LN(DEBUG_DARTS, "looking for more players ")
					START_TIMER_NOW(controlTimer)
				ELIF GET_TIMER_IN_SECONDS(controlTimer) > 4.0
					bFoundPlayer = TRUE
					DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
					CDEBUG1LN(DEBUG_DARTS, "am_darts: joining as spectator for one player game")
				ENDIF
			
			
			ELIF iNumberOfPlayers < 1
			
				IF NOT IS_TIMER_STARTED(controlTimer)
					CDEBUG1LN(DEBUG_DARTS, "no players, wait 3 seconds for one to appear ")
					START_TIMER_NOW(controlTimer)
				ELIF GET_TIMER_IN_SECONDS(controlTimer) > 3.0
					
					CDEBUG1LN(DEBUG_DARTS, "no players, clean up")
					CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB, PlayerData, PlayerDarts, MPDartGame, DartsUI, FALSE)
				ENDIF
			
			ENDIF
		
		ELIF iNumberOfPlayers >= NUM_DART_PLAYERS
			IF NOT IS_TIMER_STARTED(controlTimer)
				START_TIMER_NOW(controlTimer)
			ELIF GET_TIMER_IN_SECONDS(controlTimer) > 0
				
				INT NumFound
				NumFound = DART_GET_OTHER_PLAYERS_ID(PlayerData, iOtherID)
				CDEBUG1LN(DEBUG_DARTS, "= num of dart players found", NumFound)
				IF NumFound  = NUM_DART_PLAYERS
					bFoundPlayer = TRUE
					IF IS_TIMER_STARTED(controlTimer)
						IF GET_TIMER_IN_SECONDS(controlTimer) > 10
							CDEBUG1LN(DEBUG_DARTS, "Failed to find 2 palyers in 2 player game")
							CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB, PlayerData, PlayerDarts, MPDartGame, DartsUI, FALSE)
						ENDIF
					ENDIF
						
					CDEBUG1LN(DEBUG_DARTS, "2 player game started")
						
					//Stop people joining my mission // this will be changed for SCTV
					//SET_FM_MISSION_AS_NOT_JOINABLE()
				ENDIF
			ELSE
				IF NOT bFoundPlayer
					//IF GET_GAME_TIMER() % 1000 > 50
						IF IS_TIMER_STARTED(controlTimer)
							CDEBUG1LN(DEBUG_DARTS, "Waiting For Second Player for: ", 3, " Seconds # Current Time:", GET_TIMER_IN_SECONDS(controlTimer))
						ENDIF
					//ENDIF
				ENDIF
			ENDIF
			
		ELIF (MPGlobals.DartsData.bDartsPlayBusy = TRUE)
			CDEBUG1LN(DEBUG_DARTS, "Darts Play Busy?")
			CLEAR_HELP()
			
			BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE, TRUE)
			CDEBUG1LN(DEBUG_DARTS, "am_darts: player started sitting, cleaning up")
			CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB, PlayerData, PlayerDarts, MPDartGame, DartsUI, FALSE, FALSE, FALSE)
		
		ELIF IS_BIT_SET(MPGlobals.DartsData.iDisplayState, 5)
			CDEBUG1LN(DEBUG_DARTS, "Dispaly state 5")
			PRINT_NOW("DARTS_ENEMIES", DEFAULT_GOD_TEXT_TIME, 0)
			BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE, TRUE)
			CDEBUG1LN(DEBUG_DARTS, "ENEMIES IN THE HOUSE, CLEANING UP")
			CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB, PlayerData, PlayerDarts, MPDartGame, DartsUI, TRUE, FALSE, FALSE)
		
		ELSE
			IF NOT bFoundPlayer
					IF NOT IS_TIMER_STARTED(controlTimer)
						CDEBUG1LN(DEBUG_DARTS, "bFoundPlayer = False, setting controlTimer ")
						START_TIMER_NOW(controlTimer)
						PLAYER_TOGGLE_CONTROLS(FALSE)
						
					ELIF GET_TIMER_IN_SECONDS(controlTimer) > 3.0 //
						CDEBUG1LN(DEBUG_DARTS, "controlTimer > 3.0, must be a single player darts game")
						bFoundPlayer = TRUE
						
						DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						bSoloMP = TRUE
						
						PlayerData[iSelfID].iOffsetPlayerID = ENUM_TO_INT(DART_PLAYER_HOME)
						
						IF IS_TIMER_STARTED(controlTimer)
							CANCEL_TIMER(controlTimer)
						ENDIF
						
						//Stop people joining my mission // this will be changed for SCTV
						//SET_FM_MISSION_AS_NOT_JOINABLE()
					ENDIF
			ENDIF
			
//			renderingCam = GET_RENDERING_CAM()
//			IF DOES_CAM_EXIST(renderingCam)
//				vCamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
//				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//					vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
//				ENDIF
//			ELSE
//				vCamCoord = vPlayerCoord
//			ENDIF
//			
//			// See if we are changing players.
//			IF vCamCoord.z > vPlayerCoord.z + 50.0
//				BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE)
//				CDEBUG1LN(DEBUG_DARTS, "Player initiated MP menu.  Cleaning up AM_DARTS")
//				CLEANUP_MP_DARTS_GAME(sLocalIntroScene, missionScriptArgs, ServerData, iLocalStateFlags, DartsSB, iCleanupFlags,  PlayerData, PlayerDarts,MPDartGame, DartsUI, FALSE, FALSE, FALSE, bSpectator)
//			
				
			// Show help text.
			IF NOT bWaitUIShown
				
				bWaitUIShown = TRUE
			ENDIF
		ENDIF
	WAIT(0)
	ENDWHILE
	
	iSelfDartID = 0
	iOpponentDartID = 1
	
	IF NOT bSetupPlayer
	AND NOT bSpectator
		CDEBUG1LN(DEBUG_DARTS, "[DARTS_INITIALIZE] - Swaped Self and opponent")
		iSelfDartID = 1
		iOpponentDartID = 0
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "")
	CDEBUG1LN(DEBUG_DARTS, "[DARTS_INITIALIZE] - #######################################################")
	CDEBUG1LN(DEBUG_DARTS, "[DARTS_INITIALIZE] - ################ PLAYER SEARCH FINISHED ###############")
	CDEBUG1LN(DEBUG_DARTS, "")
	
	CDEBUG1LN(DEBUG_DARTS, "[DARTS_INITIALIZE] - Is This Host?: ",  NETWORK_IS_HOST_OF_THIS_SCRIPT())
	CDEBUG1LN(DEBUG_DARTS, "[DARTS_INITIALIZE] - Is This SetupPlayer?: ", bSetupPlayer)
	CDEBUG1LN(DEBUG_DARTS, "[DARTS_INITIALIZE] - Is This Spectator?: ",  bSpectator)
	
	//This is due to the checks in the minigame are being made for both players even if single player..
	IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
		DARTS_PLAYER_TWO = DARTS_PLAYER_ONE
		iOpponentDartID = iSelfDartID
		CDEBUG1LN(DEBUG_DARTS, "[DARTS_INITIALIZE] - Single player Game, player TWO ID is set to Player ONE ID: ", DARTS_PLAYER_TWO )
	ELSE
		//iOpponentDartID = GET_OPPONENT_ID(iDartPlayerID, PARTICIPANT_ID_TO_INT())
		//CDEBUG1LN(DEBUG_DARTS, "[DARTS_INITIALIZE] - Your iOpponentDartID is: ", iOpponentDartID)
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "[DARTS_INITIALIZE] - SinglePlayer Game?: ", DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER))

	DART_GET_DARTS_PLAYERS_ID(PlayerData, iDartPlayerID, iNumberOfPlayers, iSelfDartID, iOpponentDartID, bSpectator)
	
	IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
	AND iDartPlayerID[DARTS_PLAYER_TWO] < 0
		DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
		
		DARTS_PLAYER_TWO = DARTS_PLAYER_ONE
		iOpponentDartID = iSelfDartID
		
		DART_GET_DARTS_PLAYERS_ID(PlayerData, iDartPlayerID, iNumberOfPlayers, iSelfDartID, iOpponentDartID, bSpectator)
		
		CDEBUG1LN(DEBUG_DARTS, "[DARTS_INITIALIZE] - Something is wrong with second player, defaulting to single player")
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_DARTS,
	"[DARTS_INITIALIZE] - DARTS_PLAYER_ONE(0) Participant ID is", iDartPlayerID[DARTS_PLAYER_ONE])
	
	CDEBUG1LN(DEBUG_DARTS, 
	"[DARTS_INITIALIZE] - DARTS_PLAYER_TWO(1) Participant ID is", iDartPlayerID[DARTS_PLAYER_TWO])
	
	PLAYER_INDEX piSelf, piOther
	piSelf = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iSelfDartID]))
	piOther	 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iOpponentDartID]))
	
	PlayerDarts[iOpponentDartID].vHandBlockerLocation = GET_PLAYER_POSITION(dsCurrent , !bPlayerSideA)
	PlayerDarts[iSelfDartID].vHandBlockerLocation = GET_PLAYER_POSITION(dsCurrent , bPlayerSideA)
	PlayerDarts[iSelfDartID].playerID = piSelf
	PlayerDarts[iSelfDartID].pedID = GET_PLAYER_PED(piSelf)
	PlayerDarts[iOpponentDartID].playerID = piOther
	PlayerDarts[iOpponentDartID].pedID = GET_PLAYER_PED(piOther)
	CDEBUG1LN(DEBUG_DARTS, 
	" Self Name:", GET_PLAYER_NAME(piSelf), 
	" ParticipandID:", iDartPlayerID[iSelfDartID],
	" iSelfID(partID?):", iSelfID,
	" iSelfDartID(ArrayPos)", iSelfDartID)
	#ENDIF	
	
	SET_DARTS_PLAYER_COUNT(iNumberOfPlayers)
	iOtherID = iDartPlayerID[iOpponentDartID]

	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_DARTS, 
	" Opponent Name:", GET_PLAYER_NAME(piOther), 
	" ParticipandID:", iDartPlayerID[iOpponentDartID],
	" iOppenentID(partID?):", iOtherID,
	" iOpponentDartID(ArrayPos):", iOpponentDartID)
	
	CDEBUG1LN(DEBUG_DARTS, "Updating Sets Legs")
	CDEBUG1LN(DEBUG_DARTS, "Curent Set:", serverData.iNumSets, " Current Legs:", serverData.iNumLegs)
	CDEBUG1LN(DEBUG_DARTS, "Server Set:", serverData.iNumSets, " Server Legs:", serverData.iNumLegs)

	CDEBUG1LN(DEBUG_DARTS, "")
	CDEBUG1LN(DEBUG_DARTS, "[DARTS_INITIALIZE] - #######################################################")
	CDEBUG1LN(DEBUG_DARTS, "")
	#ENDIF
	
	//Init IDS
	PlayerDarts[DARTS_PLAYER_ONE].iParticipantID = iDartPlayerID[DARTS_PLAYER_ONE]
	PlayerDarts[DARTS_PLAYER_TWO].iParticipantID = iDartPlayerID[DARTS_PLAYER_TWO]
	CDEBUG1LN(DEBUG_DARTS, "ONEID", PlayerDarts[DARTS_PLAYER_ONE].iParticipantID)
	CDEBUG1LN(DEBUG_DARTS, "TWOID", PlayerDarts[DARTS_PLAYER_TWO].iParticipantID)
	
	
	IF bSpectator
		BOOL bIsHostSideA =  DARTS_CHECK_CLIENT_FLAG(PlayerData[PlayerDarts[DARTS_PLAYER_ONE].iParticipantID], DARTS_CLEINTFLAG_A_SIDE)
		PlayerDarts[DARTS_PLAYER_TWO].vHandBlockerLocation = GET_HAND_BLOCKER_POSITION(GET_PLAYER_POSITION(dsCurrent , !bIsHostSideA), Args.vDartBoard)
		PlayerDarts[DARTS_PLAYER_ONE].vHandBlockerLocation = GET_HAND_BLOCKER_POSITION(GET_PLAYER_POSITION(dsCurrent , bIsHostSideA), Args.vDartBoard)
	ENDIF
	
	//############### Check Id integrity
	IF NOT bSpectator
		IF iSelfID <> iDartPlayerID[iSelfDartID]
			ASSERTLN("Self:", iSelfID, " <> StoredDartID", iDartPlayerID[iSelfDartID], " , Please Tell - Aleksej Leskin")
		ENDIF
		
		IF (iSelfDartID < 0 OR iSelfDartID > 1)
			ASSERTLN("Self:", iSelfID, " Out of bounds(selfdartID", iSelfDartID, " , Please Tell - Aleksej Leskin")
		ENDIF
	ENDIF
	
	//############### Check Setup is Host
	IF bSetupPlayer
	AND NOT  NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ASSERTLN("Setup player is not the HOST, Please Tell - Aleksej Leskin")
	ENDIF
	
		//fixup ID if singlepalyer
	IF DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_SINGLE_PLAYER)
	AND NOT bSpectator
		CDEBUG1LN(DEBUG_DARTS, "ID fixup for singlepalyer iOtherID:", iOtherID, " iSelfID", iSelfID)
		iOtherID = iSelfID
	ENDIF
		
	DART_PLAYER_AWAY_SERVER = DARTS_PLAYER_TWO//<-
	
	// Wait for server and client broadcast data to sync up.
	CDEBUG1LN(DEBUG_DARTS, "GameMode: ")
	IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
		WAIT(500)
	ENDIF
	
	// Make sure we aren't trying to join a game in progress.
	// this check was mainly for CnC
	IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_SINGLE_PLAYER)
	AND iOtherID >= 0
		IF (DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_MATCH_MADE) AND GET_CURRENT_GAMEMODE() != GAMEMODE_FM)
		AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
		AND NOT IS_PLAYER_SCTV(PLAYER_ID())
			WHILE TRUE
				// Print a message saying we can't join this game.
				IF NOT bBoardTakenUIShown
					START_TIMER_NOW(controlTimer)
					
					bBoardTakenUIShown = TRUE
					
				// Wait for confirmation from player or timeout. 
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR GET_TIMER_IN_SECONDS(controlTimer) >= 8.0
					CLEAR_HELP()
					CDEBUG1LN(DEBUG_DARTS, "Darts game was already in progress, cleaning up AM_DARTS")
					CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB,  PlayerData, PlayerDarts,MPDartGame, DartsUI, FALSE, FALSE, FALSE)
				ENDIF
				WAIT(0)
			ENDWHILE
		ENDIF
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		ADD_AM_DART_WIDGETS()
	#ENDIF
	
	IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
		DISPLAY_AREA_NAME(FALSE)
	ENDIF
	
	IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
	AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		PlayerData[iSelfID].piOther = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))  
		CDEBUG1LN(DEBUG_DARTS, "piOther is set")
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "piOther is not set, must be a single player game, or spectator")
	ENDIF


	iStaminaXP = GET_MP_INT_CHARACTER_STAT(MP_STAT_STAMINA) //GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MULTIPLAYER, PS_STAMINA)
	CDEBUG1LN(DEBUG_DARTS, "stamina stat = ", iStaminaXP)
	
	fStaminaPercentage = TO_FLOAT(iStaminaXP)/100
	fSteadyShotLength = fStaminaPercentage * 540
	iSteadyShotLength = ROUND(fSteadyShotLength) + 660
	
	CDEBUG1LN(DEBUG_DARTS, "steady shot = ", iSteadyShotLength)
	IF iSteadyShotLength < 660
		iSteadyShotLength = 660
		CDEBUG1LN(DEBUG_DARTS, "steady shot readjusted to 660")
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] has reached the while loop")
	CPRINTLN(DEBUG_DARTS, "TYPING CHANGES ARE IN")
	
	IF bMinigamePlayerQuitFlag
		bMinigamePlayerQuitFlag = FALSE
	ENDIF
	
	STORE_PLAYER_INTERACTION_ANIM_VALUES_FOR_CELEBRATION_SCREEN()
	
	bIsHost = TRUE
	bSinglePlayerGame = DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
	
	IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
		ANIMPOSTFX_STOP_ALL()
		//gdisablerankupmessage = TRUE
		SET_DISABLE_RANK_UP_MESSAGE(TRUE)
	ENDIF
	
	WHILE TRUE
		NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
		WAIT(0)
		iNumberOfPlayers = NETWORK_GET_NUM_PLAYERS(PlayerData)
		//Check at the start of the frame if the script should die. 		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] THIS SHOULD TERMINE SO RUN IT")
			CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB,  PlayerData, PlayerDarts,MPDartGame, DartsUI, TRUE, FALSE, FALSE)
		ENDIF
		
		SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0.0, -0.0375)
		
		
		
		// _______________________________DEBUG START______________________________________________
		#IF IS_DEBUG_BUILD
		
		// Debug Key: win game
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			CDEBUG1LN(DEBUG_DARTS, "Darts debug succeed!  setting win flag")
			bDartsNoSocialClub = TRUE
			RESTART_TIMER_NOW(controlTimer)
			//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END
			SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_GAME_END)
			DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_WINNERD)
		ENDIF
		
		// Debug Key: lose game
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			CDEBUG1LN(DEBUG_DARTS, "Darts debug failed!  setting lose flag")
			bDartsNoSocialClub = TRUE
			RESTART_TIMER_NOW(controlTimer)
			//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END
			SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_GAME_END)
			DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_LOSERD)
		ENDIF
		
		IF (PlayerData[iOtherID].eMPState = DARTS_MPSTATE_GAME_END)
		AND NOT (PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END)
		AND ( DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_WINNERD)
			OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_LOSERD) )
			CDEBUG1LN(DEBUG_DARTS, "DARTS_MPSTATE_GAME_END getting set because opponent debugged")
			bDartsNoSocialClub = TRUE
			RESTART_TIMER_NOW(controlTimer)
			//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END
			SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_GAME_END)
		ENDIF
		
		PROCESS_WIDGETS(ServerData)
		
		#ENDIF
		// _______________________________DEBUG END________________________________________________
		
		//TODO: add an every frame check here to see who are players and who are sctv's
		INT idx
		INT IDcheck
		PARTICIPANT_INDEX participantID
		
		iNumPlayers = 0
		iNumParticipants = NETWORK_GET_NUM_PARTICIPANTS()
		
		IDcheck = PARTICIPANT_ID_TO_INT()
		
		IF IDcheck <> iSelfID
			CDEBUG1LN(DEBUG_DARTS, "ID has changed! WHAT?!!")
			iSelfID = IDcheck
		ENDIF
		
		IF iLastNumParticipants <> iNumParticipants
			CDEBUG1LN(DEBUG_DARTS, "last num participants has changed!")
			CDEBUG1LN(DEBUG_DARTS, "last num participants = ", iLastNumParticipants)
			CDEBUG1LN(DEBUG_DARTS, "current participants = ", iNumParticipants)
			bCheckPlayers = TRUE
			
			iLastNumParticipants = iNumParticipants
		ENDIF
		
		IF bCheckPlayers
	//	PARTICIPANT_INDEX piLocalPlayer = INT_TO_PARTICIPANTINDEX( PARTICIPANT_ID_TO_INT() )
			REPEAT iNumParticipants idx
				participantID = INT_TO_PARTICIPANTINDEX( idx )
				IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )

					IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
					OR DARTS_CHECK_CLIENT_FLAG(PlayerData[idx], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
						IF IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(participantID))
							CDEBUG1LN(DEBUG_DARTS, "this participant is sctv: ", idx)
						ENDIF
						
						IF DARTS_CHECK_CLIENT_FLAG(PlayerData[idx], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
							CDEBUG1LN(DEBUG_DARTS, "this participant is spectator: ", idx)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "this participant is a player: ", idx)
						iNumPlayers++
						CDEBUG1LN(DEBUG_DARTS, "number of players: ", iNumPlayers)
						IF iNumPlayers <> iLastNumPlayers
							CDEBUG1LN(DEBUG_DARTS, "number of players: ", iNumPlayers)
							CDEBUG1LN(DEBUG_DARTS, "number of last players: ", iLastNumPlayers)
						ENDIF
					ENDIF
				ELSE
				//	CDEBUG1LN(DEBUG_DARTS, "this participant is NOT active! : ", idx)
				ENDIF
			ENDREPEAT
		ENDIF
		
		iLastNumPlayers = iNumPlayers
		bCheckPlayers = FALSE
		
		//CDEBUG1LN(DEBUG_DARTS, "PLAYER STATE IS NORMAL PLAYER")
		// #NORMAL GAMEPLAY HERE VVVVVVV
			
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_PLAYER_LEAVE_MP_MISSION(TRUE)
			CDEBUG1LN(DEBUG_DARTS, "MATCH END THREAD TERMINATE OR PLAYER SHOULD LEAVE")
			IF PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END
			AND serverData.mpTurnStatus[NETID_TO_DARTID(iDartPlayerID, iSelfID)]  < DARTS_MPTURN_WINNING
			AND (MPDartGame.iGamesWon = 0)
			AND (MPDartGame.iGamesLost = 0)
				CDEBUG1LN(DEBUG_DARTS, "Also giving money back to the player in line 970")
				IF IS_DARTS_GAME_ALMOST_DONE(ServerData, PlayerData, iDartPlayerID)
					INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
					iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
				ELSE
					iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED_TIED()
				ENDIF
				CDEBUG1LN(DEBUG_DARTS, "iBetWinnings = ", iBetWinnings)
				
				IF RECEIVED_EVENT(EVENT_NETWORK_END_MATCH)
					bMinigamePlayerQuitFlag = TRUE
				ENDIF
			ENDIF
				
		CDEBUG1LN(DEBUG_DARTS, "SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE returned true, CLEANING UP DARTS")
			CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB, PlayerData, PlayerDarts,MPDartGame, DartsUI, TRUE, FALSE, FALSE)
		ENDIF
			
		//If the mission is being told to die
		IF SHOULD_PLAYER_LEAVE_MP_MISSION(TRUE)
			CDEBUG1LN(DEBUG_DARTS, "MATCH END  PLAYER SHOULD LEAVE")
			IF PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END
			AND serverData.mpTurnStatus[NETID_TO_DARTID(iDartPlayerID, iSelfID)]  < DARTS_MPTURN_WINNING
			AND (MPDartGame.iGamesWon = 0)
			AND (MPDartGame.iGamesLost = 0)
				CDEBUG1LN(DEBUG_DARTS, "Also giving money back to the player in line 970")
					
			IF IS_DARTS_GAME_ALMOST_DONE(ServerData, PlayerData, iDartPlayerID)
				INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
				iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
			ELSE
				iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED_TIED()
			ENDIF
				CDEBUG1LN(DEBUG_DARTS, "iBetWinnings = ", iBetWinnings)
			ENDIF
			
			CDEBUG1LN(DEBUG_DARTS, "SHOULD_PLAYER_LEAVE_MP_MISSION returned true, CLEANING UP DARTS")
			CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB, PlayerData, PlayerDarts,MPDartGame, DartsUI, TRUE, FALSE, FALSE)
		ENDIF
		

		//############### SPECTATOR HIDE CHECK, call each frame
		IF PlayerData[iSelfID].eMPState > DARTS_MPSTATE_SETUP
		AND PlayerData[iSelfID].eMPState < DARTS_MPSTATE_LEAVE
		AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
		AND NOT IS_GAMEPLAY_CAM_RENDERING()
			SAFE_BIT_TOGGLE(iLocalStateFlags, ENUM_TO_INT(eLP_HIDE_PARTICIPANTS)) 
		ENDIF
		
		//############### BLOCK SPECTATE(BETA) MENU IF IS A PLAYER
		IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
			IF IS_PAUSE_MENU_ACTIVE()
	    		IF GET_PAUSE_MENU_STATE() = PM_READY
					IF NOT PAUSE_MENU_IS_CONTEXT_ACTIVE(HASH("DisableSpectateScript"))
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "DARTS APARTMENT - DISABLE SPECTATE(BETA) BUTTON")
						PAUSE_MENU_ACTIVATE_CONTEXT(HASH("DisableSpectateScript"))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_CLEANUP_STARTED)
			//############### TERMINATE GAME IF PED GOT DAMAGED.
			IF IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
				CDEBUG1LN(DEBUG_DARTS, "IS_PED_INJURED(PLAYER_PED_ID()) returned true, CLEANING UP DARTS")
				CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB,  PlayerData, PlayerDarts,MPDartGame, DartsUI, TRUE, FALSE, FALSE)
			ENDIF
			
			//############### SPECTATOR LEAVE CHECK
			IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
				IF 	iNumberOfPlayers < 1 
					CDEBUG1LN(DEBUG_DARTS, "Cleanup Trigger - I am a spectator, num of players < 1")
					CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB,  PlayerData, PlayerDarts,MPDartGame, DartsUI, TRUE, FALSE, FALSE)
				ENDIF
			ENDIF
			
			//############## IS EXITING WITH WARP TO VEHICLES
			IF IS_LOCAL_PLAYER_DOING_A_GROUP_WARP()
			
			//OR g_bPlayerAllExitApartment
				SET_BIT(iCleanupFlags, ENUM_TO_INT(eCLEANUP_FLAG_WAIT_FOR_FADED_OUT))
				SET_BIT(iCleanupFlags, ENUM_TO_INT(eCLEANUP_FLAG_DONT_FADE_IN))
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "DARTS APARTMENT - TERMINATE KICK OUT ON BIKES")
				CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB,  PlayerData, PlayerDarts,MPDartGame, DartsUI, TRUE, FALSE, FALSE)
			ENDIF
			
			IF g_bPlayerAllExitApartment
				//SET_BIT(iCleanupFlags, ENUM_TO_INT(eCLEANUP_FLAG_WAIT_FOR_FADED_OUT))
				SET_BIT(iCleanupFlags, ENUM_TO_INT(eCLEANUP_FLAG_DONT_FADE_IN))
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "DARTS APARTMENT - TERMINATE KICK OUT ON BIKES")
				CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB,  PlayerData, PlayerDarts,MPDartGame, DartsUI, TRUE, FALSE, FALSE)
			ENDIF
			
			//############### NOT INSIDE BUILDING LEAVE CHECK
			IF NOT IS_PLAYER_ENTERING_PROPERTY(GET_PLAYER_INDEX())
				//check if script sohuld terminate - if outside an apartment
				IF (NOT IS_PLAYER_IN_MP_PROPERTY(GET_PLAYER_INDEX(), FALSE)
				AND NOT IS_PLAYER_IN_CLUBHOUSE(GET_PLAYER_INDEX())
				AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				#IF FEATURE_DLC_2_2022
				AND NOT IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
				#ENDIF )
				OR IS_PLAYER_EXITING_PROPERTY(GET_PLAYER_INDEX())
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "DARTS APARTMENT - TERMINATE NOT INSIDE THE PROPERTY")
					IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
						CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB,  PlayerData, PlayerDarts ,MPDartGame, DartsUI, TRUE, FALSE, FALSE)
					ELSE
						IF PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END
							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_END)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//############### INTERIOR RENOVATION CHECK
			IF GET_UPDATE_CUSTOM_APT()
			OR g_bPlayerShouldBeKickedFromApartment
				CDEBUG1LN(DEBUG_DARTS, "DARTS APARTMENT - TERMINATE APARTMENT UPDATE")
				IF PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END
					SET_BIT(iCleanupFlags, ENUM_TO_INT(eCLEANUP_FLAG_DONT_FADE_IN))
					SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_END)
				ENDIF
			ENDIF
	
			//### IF 2 Player but one left.
			IF ((iNumberOfPlayers < 2 AND PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END)
			OR (PlayerData[iOtherID].eMPState > DARTS_MPSTATE_WAIT_POST_GAME AND PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END))
			AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
			AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
			
				CDEBUG1LN(DEBUG_DARTS, "Is single palyer?", DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER))
				CDEBUG1LN(DEBUG_DARTS, "MATCH END NOT SINGLE PLAYER AND LESS THAN 2 PLAYERS")

				IF PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END
				OR PlayerData[iSelfID].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
					CDEBUG1LN(DEBUG_DARTS, "game had already ended, setting terminate reason to ENDGAME")
					terminateReason = DARTS_MPTERMINATEREASON_ENDGAME
				ELSE
					CDEBUG1LN(DEBUG_DARTS, "game was still in play, setting terminate reason to PLAYERLEFT")
					terminateReason = DARTS_MPTERMINATEREASON_PLAYERLEFT
					
					IF IS_DARTS_GAME_ALMOST_DONE(ServerData, PlayerData, iDartPlayerID)
						INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
						iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
					ELSE
						iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED_TIED()
					ENDIF
						CDEBUG1LN(DEBUG_DARTS, "iBetWinnings in 1812 is ", iBetWinnings)
				ENDIF
					
				BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE, TRUE)
				CDEBUG1LN(DEBUG_DARTS, "NETWORK_GET_NUM_PLAYERS() < 2 returned true, CLEANING UP DARTS")
				CDEBUG1LN(DEBUG_DARTS, "NETWORK_GET_NUM_PLAYERS() has returned: ", iNumberOfPlayers)
				CDEBUG1LN(DEBUG_DARTS, "AM_Darts: PlayerData[iOtherID].eMPState at this moment = ", PlayerData[iDartPlayerID[iOpponentDartID]].eMPState)
				CDEBUG1LN(DEBUG_DARTS, "AM_Darts: PlayerData[iSelfID].eMPState at this moment = ", PlayerData[iDartPlayerID[iSelfDartID]].eMPState)
				PlayerData[iSelfID].eMPState = DARTS_MPSTATE_TERMINATE_SPLASH
			ELSE
				IF (GET_GAME_TIMER() % 30000) < 50
					CDEBUG1LN(DEBUG_DARTS, "")
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts: NETWORK_GET_NUM_PLAYERS() = ", NETWORK_GET_NUM_PLAYERS(PlayerData, TRUE))
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts: PlayerData[iOtherID].eMPState  = ", PlayerData[iDartPlayerID[iOpponentDartID]].eMPState)
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts: PlayerData[iSelfID].eMPState   = ", PlayerData[iDartPlayerID[iSelfDartID]].eMPState)
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts: host status   = ", NETWORK_IS_HOST_OF_THIS_SCRIPT())
					CDEBUG1LN(DEBUG_DARTS, "")
				ENDIF
			ENDIF
		ENDIF
		
		// checking for new spectators
		IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
		AND NETWORK_GET_NUM_PARTICIPANTS() = 2
			// listen for spectator flag
			IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
				IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
					CDEBUG1LN(DEBUG_DARTS, "A new challenger has been sensed")
					DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
				ENDIF
			ENDIF
		ELIF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
		AND NETWORK_GET_NUM_PARTICIPANTS() < 2
			CDEBUG1LN(DEBUG_DARTS, "New challenger left")
			DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
		ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sTest = "NETWORK_GET_NUM_PARTICIPANTS Num Players "
			sTest += CONVERT_INT_TO_STRING(NETWORK_GET_NUM_PARTICIPANTS())
			
			TEXT_LABEL_63 sTest2 = "NETWORK_GET_NUM_SCRIPT_PARTICIPANTS Num Players "
			sTest2 += CONVERT_INT_TO_STRING(NETWORK_GET_NUM_SCRIPT_PARTICIPANTS("AM_Darts_Apartment", NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT()))
			
			DRAW_DEBUG_TEXT_2D(sTest, <<0.6, 0.25, 0>>)
			DRAW_DEBUG_TEXT_2D(sTest2, <<0.6, 0.28, 0>>)
			
			#ENDIF
			

			IF IS_BIT_SET(MPGlobals.DartsData.iDisplayState, 5)
				PRINT_NOW("DARTS_ENEMIES", DEFAULT_GOD_TEXT_TIME, 0)
				BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE, TRUE)
				CDEBUG1LN(DEBUG_DARTS, "ENEMIES IN THE HOUSE, CLEANING UP")
				CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB, PlayerData, PlayerDarts,MPDartGame, DartsUI, TRUE, FALSE, FALSE)
			ENDIF
			
			// Detecting quit flags thrown by server
			IF DARTS_PLAYER_PROCESS_SERVER_FLAGS(ServerData, PlayerData[iSelfID], localDartsTimer)
				
				IF IS_DARTS_GAME_ALMOST_DONE(ServerData, PlayerData, iDartPlayerID)
					INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
					iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
				ELSE
					iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED_TIED()
				ENDIF
				CDEBUG1LN(DEBUG_DARTS, "iBetWinnings = ", iBetWinnings)
			ENDIF
			
			// Process Events thrown from other clients
			//DARTS_BROADCAST_PROCESS_EVENTS(Player[iSelfID])  // TODO: this isn't currently being used all that much... not much at all.  take out?
				
			//Quit minigame controlls?
			IF PlayerData[iSelfID].eMPState >= DARTS_MPSTATE_THROW_OFF
			AND PlayerData[iSelfID].eMPState < DARTS_MPSTATE_GAME_END //DARTS_MPSTATE_TERMINATE_SPLASH
				IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_I_AM_SPECTATOR)					
					IF DARTS_PLAYER_QUIT_CONTROL(PlayerData[iSelfID], DartsUI, iDebugThrottle)
						CLEAR_BIT(iLocalStateFlags, ENUM_TO_INT(eLP_DRAW_SCORE_BOARD))
						CLEAR_BIT(iLocalStateFlags, ENUM_TO_INT(eLP_HIDE_PLAYER_HUD)) 
						CLEAR_BIT(iLocalStateFlags, ENUM_TO_INT(eLP_HIDE_PARTICIPANTS)) 
						bMinigamePlayerQuitFlag = TRUE
						
						IF IS_DARTS_GAME_ALMOST_DONE(ServerData, PlayerData, iDartPlayerID)
							INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
							iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
						ELSE
							iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED_TIED()
						ENDIF
						
						INTRO_SCENE_CLEANUP(sLocalIntroScene)
						
						//### intro is played locally, fix if player quits at throw off.
						IF DARTS_CHECK_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_QUIT_CONFIRMED)
						AND NOT DARTS_CHECK_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							PED_INDEX pedOpponent
							pedOpponent = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iOpponentDartID])))
							IF IS_ENTITY_ALIVE(pedOpponent)
								IF NOT IS_ENTITY_PLAYING_ANIM(pedOpponent, "anim@amb@clubhouse@mini@darts@", "throw_idle_a_down")
									TASK_PLAY_ANIM(pedOpponent, "anim@amb@clubhouse@mini@darts@", "outro", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT, 0, FALSE, AIK_NONE, TRUE)
									CDEBUG1LN(DEBUG_DARTS, "START PLAYING OPPONENT OUTRO")
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "THROW IDLE IS LOOPING")
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_DARTS, "DEAD DO NOT PLAY OPPONENT OUTRO")
							ENDIF
						ENDIF
						//#### HIDE DARTS
						DARTS_HIDE(PlayerDarts[DARTS_PLAYER_TWO])
						DARTS_HIDE(PlayerDarts[DARTS_PLAYER_ONE])
						DartsUI.iTheirScore++
						CDEBUG1LN(DEBUG_DARTS, "Player chose to quit manually")
						RENDER_SCRIPT_CAMS(FALSE, FALSE) 
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_END)
					ENDIF
				ENDIF
			ENDIF			

			//###########################################################################
			//########################### SPECTATOR GAMEPALY ############################
			IF bSpectator

				//CATCH EVENTS FROM PLAYERS.
				INT iCount
				iCount = 0
				REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
					SWITCH GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)		
						CASE EVENT_NETWORK_SCRIPT_EVENT
							IF EVENT_CATCH_DART_INFO(iCount, SpecDartReceiveInfo)
								IF SpecDartReceiveInfo.bThrow
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS EVENT SPECTATOR] RECEIVED DART THROW")
								ELIF  SpecDartReceiveInfo.bResetDone
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS EVENT SPECTATOR] RECEIVED DART ResetDone")
								ENDIF
							ENDIF	
						BREAK
					ENDSWITCH
				ENDREPEAT
				
				//UPDATE DARTS, SWITCHING, THROWING
				INT iDartPlayer
				REPEAT 2 iDartPlayer
					IF IS_NET_PARTICIPANT_INT_ID_VALID(PlayerDarts[iDartPlayer].iParticipantID)
						DART_THROW_PROCESS(PlayerDarts[iDartPlayer],  MPDartGame.dBoard, SpecDartReceiveInfo.vOffsetTarget)
					ENDIF
				ENDREPEAT
				
				//UPDATE SCORE 
				IF SpecDartReceiveInfo.bNextPlayer
				AND HAS_SCALEFORM_MOVIE_LOADED(DartsSB.siScoreBoard)
					SpecDartReceiveInfo.bNextPlayer = FALSE	
					//Invalid score - throw off
					IF SpecDartReceiveInfo.iPlayerScore <> -1
						DARTS_ADD_SCORE(DartsSB, PLAYERID_TO_SCOREID(SpecDartReceiveInfo.iThrower), SpecDartReceiveInfo.iPlayerScore)
					ENDIF
					CDEBUG1LN(DEBUG_DARTS, "!THROWER: ", SpecDartReceiveInfo.iThrower)
					CDEBUG1LN(DEBUG_DARTS, "!THROWER PICK: ", PICK_INT( SpecDartReceiveInfo.iThrower = DARTS_PLAYER_ONE, DARTS_PLAYER_TWO, DARTS_PLAYER_ONE))
					UPDATE_HIGHLIGHT(DartsSB, PICK_INT( SpecDartReceiveInfo.iThrower = ENUM_TO_INT(DART_PLAYER_HOME), ENUM_TO_INT(DART_PLAYER_AWAY), ENUM_TO_INT(DART_PLAYER_HOME)))
					
					//Initialize darts for players.
					PED_INDEX piPlayerOne
					piPlayerOne = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_ONE])))
					DARTS_RESET(PlayerDarts[DARTS_PLAYER_ONE], piPlayerOne)
					DARTS_SELECT(PlayerDarts[DARTS_PLAYER_ONE], piPlayerOne, 0)
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						PED_INDEX piPlayerTwo
						piPlayerTwo = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_TWO])))
						DARTS_RESET(PlayerDarts[DARTS_PLAYER_TWO], piPlayerTwo)
						DARTS_SELECT(PlayerDarts[DARTS_PLAYER_TWO], piPlayerTwo, 0)	
					ENDIF
				ELSE
					//CDEBUG1LN(DEBUG_DARTS, "NOT LOADEDD")
				ENDIF
				
				SWITCH PlayerData[iSelfID].eMPState
				
					CASE DARTS_MPSTATE_WAIT_PRE_INIT
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_INIT, SPEC)
					BREAK
				
					CASE DARTS_MPSTATE_INIT	
						INIT_BOARD(MPDartGame.dBoard, Args)
						MPDartGame.dBoard.fBoardHeading += 0.01 // V function check if heading is 0 and then doesnt audo adjust score board......
						DARTS_INIT_SCOREBOARD(DartsSB, ENUM_TO_INT(DartsLocation), MPDartGame.dBoard.vDartBoard, MPDartGame.dBoard.fBoardHeading)
						SET_BIT(iLocalStateFlags, ENUM_TO_INT(eLP_DRAW_SCORE_BOARD))
						REQUEST_STREAMS(DartsSB, bSpectator)
						REGISTER_SCRIPT_WITH_AUDIO()

						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_STREAMING, SPEC)
					BREAK
				
					CASE DARTS_MPSTATE_STREAMING

						IF HAVE_STREAMS_LOADED(DartsSB, bSpectator)
							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SETUP, SPEC)
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "Spectator Loading Streams")
						ENDIF

					BREAK
				
					CASE DARTS_MPSTATE_SETUP					
						
						//Initialize darts for players.
						PED_INDEX piPlayerOne
						piPlayerOne = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_ONE])))
						DARTS_CREATE(PlayerDarts[DARTS_PLAYER_ONE].PlayerDarts, modelDart1)
						DARTS_RESET(PlayerDarts[DARTS_PLAYER_ONE], piPlayerOne)
						DARTS_SELECT(PlayerDarts[DARTS_PLAYER_ONE], piPlayerOne, 0)
						
						SPAWN_HAND_BLOCKER(PlayerDarts[DARTS_PLAYER_ONE], MPDartGame.dBoard.vDartBoard)
						CDEBUG1LN(DEBUG_DARTS, "SPEC - CREATE DARTS FOR SPEC PLAYER 1")
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							PED_INDEX piPlayerTwo
							piPlayerTwo = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_TWO])))
							DARTS_CREATE(PlayerDarts[DARTS_PLAYER_TWO].PlayerDarts, modelDart1)
							DARTS_RESET(PlayerDarts[DARTS_PLAYER_TWO], piPlayerTwo)
							DARTS_SELECT(PlayerDarts[DARTS_PLAYER_TWO], piPlayerTwo, 0)	
							CDEBUG1LN(DEBUG_DARTS, "SPEC - CREATE DARTS FOR SPEC PLAYER 2")
							
							SPAWN_HAND_BLOCKER(PlayerDarts[DARTS_PLAYER_TWO], MPDartGame.dBoard.vDartBoard)
						ENDIF
						
						SET_BIT(iLocalStateFlags, ENUM_TO_INT(eLP_DISABLE_CAM_COL_HAND_BLOCK)) 
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							sDartsPlayerNames[DARTS_PLAYER_TWO] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_TWO])))
							DartsGamerHandle[DARTS_PLAYER_TWO] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_TWO])))
							sCrewTags[DARTS_PLAYER_TWO] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[DARTS_PLAYER_TWO])
							
							IF NOT IS_PED_INJURED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_TWO]))))
								piPlayerClones[DARTS_PLAYER_TWO] = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_TWO])))
							ENDIF
							
							CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iDartPlayerID[DARTS_PLAYER_TWO], " = ", sCrewTags[DARTS_PLAYER_TWO])
							IF NOT IS_STRING_EMPTY(sCrewTags[DARTS_PLAYER_TWO])
								DARTS_ADD_SB_CREW_TAG_UGC_CHECK(DartsSB, PLAYERID_TO_SCOREID(DARTS_PLAYER_TWO), sCrewTags[DARTS_PLAYER_TWO])
							ENDIF
							DARTS_CLEAR_SCORES(DartsSB, PLAYERID_TO_SCOREID(DARTS_PLAYER_TWO))
						ENDIF

						sDartsPlayerNames[DARTS_PLAYER_ONE] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_ONE])))
						DartsGamerHandle[DARTS_PLAYER_ONE] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_ONE])))
						sCrewTags[DARTS_PLAYER_ONE] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[DARTS_PLAYER_ONE])
						
						piOtherPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_ONE])))
						IF NOT IS_PED_INJURED(piOtherPlayer)
							piPlayerClones[DARTS_PLAYER_ONE] = piOtherPlayer
						ENDIF
						
						//PED_INDEX piOpponenet
						//piOpponenet =  GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_TWO])))
						
						CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iDartPlayerID[DARTS_PLAYER_ONE], " = ", sCrewTags[DARTS_PLAYER_ONE])
						IF NOT IS_STRING_EMPTY(sCrewTags[DARTS_PLAYER_ONE])
							DARTS_ADD_SB_CREW_TAG_UGC_CHECK(DartsSB, PLAYERID_TO_SCOREID(DARTS_PLAYER_ONE), sCrewTags[DARTS_PLAYER_ONE])
						ENDIF
						DARTS_CLEAR_SCORES(DartsSB, PLAYERID_TO_SCOREID(DARTS_PLAYER_ONE))
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							DARTS_ADD_NAMES(DartsSB, sDartsPlayerNames[DARTS_PLAYER_TWO], sDartsPlayerNames[DARTS_PLAYER_ONE])
						ELSE
							DARTS_ADD_NAMES(DartsSB, "", sDartsPlayerNames[DARTS_PLAYER_ONE])
						ENDIF
						
						CDEBUG1LN(DEBUG_DARTS, "AM_Darts.sc: serverData.iNumberOfLegs = ", serverData.iNumLegs)
						CDEBUG1LN(DEBUG_DARTS, "AM_Darts.sc: serverData.iNumberOfSets = ", serverData.iNumSets)
						UPDATE_SB_GAMES(DartsSB.siScoreBoard, bSinglePlayerGame, TRUE, serverData.iSets[DARTS_PLAYER_TWO], serverData.iSets[DARTS_PLAYER_ONE])
						
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_TUTORIAL, SPEC)
					BREAK
				
				CASE DARTS_MPSTATE_TUTORIAL					
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						IF NOT DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_THROW_OFF_DONE)
							CDEBUG1LN(DEBUG_DARTS, "DARTS THROWW OFF IN PROGGRESS")
							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_THROW_OFF_SETUP, SPEC)
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "DARTS THROW OFF DONE< DO NORMAL UPDATE")
							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SYNC_OBJECTS, SPEC)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "DARTS DO NORMAL UPDATE")
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SYNC_OBJECTS, SPEC)
					ENDIF
				BREAK	
					
				CASE DARTS_MPSTATE_THROW_OFF_SETUP
					IF NOT IS_TIMER_STARTED(controlTimer)

						START_TIMER_NOW(controlTimer)
						
					ELIF GET_TIMER_IN_SECONDS_SAFE(controlTimer) >= 5.0
					//OR NOT UPDATE_SCALEFORM_BIG_MESSAGE(DartsUI.siBigMessage)
					OR PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState = DARTS_MPSTATE_THROW_OFF
					OR PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState = DARTS_MPSTATE_THROW_OFF
						//CLEANUP_BIG_MESSAGE()
						CANCEL_TIMER(controlTimer)
						
						IF serverData.iWaiter = iSelfDartID
							UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
						ELSE
							UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_AWAY))
						ENDIF
						
						DARTS_INIT_TIMERS(localDartsTimer)
						
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_THROW_OFF, SPEC)
						
						eSpecTurnStatus = DARTS_MPTURN_WAITING
						eSpecWaitStage = DARTS_MPWAIT_AIM
						iSpecThower = serverData.iThrower
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_THROW_OFF		
					
					IF ((GET_GAME_TIMER() % 1000) < 50)
						CDEBUG1LN(DEBUG_DARTS, "IN THROW OFF- eSpecTurn:", eSpecTurnStatus,
						" eSpecWait:", eSpecWaitStage,
						" ")
					ENDIF
			
					IF DARTS_SERVER_CHECK_FLAG(ServerData, DARTSERVERFLAG_DO_THROWOFF)
						IF serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_WINNING
						OR serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_WINNING
							eSpecTurnStatus = DARTS_MPTURN_WINNING
						ENDIF
						
						// Do everyting playing related right chea
						SWITCH eSpecTurnStatus 
							CASE DARTS_MPTURN_WAITING
								SWITCH eSpecWaitStage
									CASE DARTS_MPWAIT_AIM
										IF SpecDartReceiveInfo.bThrow
										//OR playerData[iDartPlayerID[iSpecThower]].bDartThrown
											CDEBUG1LN(DEBUG_DARTS, "Current client finished WAIT - AIMING")
											SpecDartReceiveInfo.bThrow = FALSE
											
											DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
											
											// grab other players dart throw info so we can throw that thang
											#IF IS_DEBUG_BUILD
											iSpecTurn = ServerData.iServerTurn
											#ENDIF
											//DART_RELEASE_HAND(PlayerDarts[serverData.iThrower], SpecDartReceiveInfo.vOffsetTarget)
											//PlayerDarts[serverData.iThrower].PlayerDarts[iSpecTurn].vOffsetTarget = SpecDartReceiveInfo.vOffsetTarget
											
											eSpecWaitStage = DARTS_MPWAIT_THROW
										ELSE
											IF (GET_GAME_TIMER() % 1500) < 50
												CDEBUG1LN(DEBUG_DARTS, "spectator is waiting for other client to throw")
												CDEBUG1LN(DEBUG_DARTS, "iSpecThower = ", iSpecThower)
												CDEBUG1LN(DEBUG_DARTS, "iSpectatePlayerID[iSpecThower] = ", iDartPlayerID[iSpecThower])
												CDEBUG1LN(DEBUG_DARTS, "serverdata.iThrower = ", serverdata.iThrower)
											ENDIF
										ENDIF
									BREAK
								
									CASE DARTS_MPWAIT_THROW		
										CDEBUG1LN(DEBUG_DARTS, "[spec] current client is WAITING - Throwing")
										//IF THROW_DART_SPEC(MPDartGame.Darts[iSpecTurn], MPDartGame.dBoard)
											CDEBUG1LN(DEBUG_DARTS, "Regular Throw Received: ", MPDartGame.Darts[iSpecTurn].vOffsetTarget, " ThrowNum:",  iSpecTurn)
											CDEBUG1LN(DEBUG_DARTS, "Regular Throw Completed - spec side")
											
											bSpecResetDone = FALSE
											
											eSpecWaitStage = DARTS_MPWAIT_SCORE
											CDEBUG1LN(DEBUG_DARTS, "[spec] going to spec side score check")
											eSpecTurnStatus = DARTS_MPTURN_SCORE_CHECK
										//ENDIF
									BREAK
									
									CASE DARTS_MPWAIT_SCORE
										IF (GET_GAME_TIMER() % 2000) < 50
											CDEBUG1LN(DEBUG_DARTS, "[spec] going to spec side score check")
										ENDIF
									BREAK
								ENDSWITCH
							BREAK
							
							CASE DARTS_MPTURN_SCORE_CHECK
								
								// spectate is only for sp darts, so tuning this just for that
								IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED)
									// if prints are needed 
									DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, TRUE)
								ENDIF
								
								IF NOT bSpecResetDone
								//AND DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE)
									//IF (playerData[iDartPlayerID[DARTS_PLAYER_ONE]].bResetDone OR playerData[iDartPlayerID[DARTS_PLAYER_TWO]].bResetDone)
									IF (GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
									OR GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE))
									OR SpecDartReceiveInfo.bResetDone
										SpecDartReceiveInfo.bResetDone = FALSE
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec is in SCORE CHECK")
										
										eSpecWaitStage = DARTS_MPWAIT_AIM
										
										RESTART_TIMER_NOW(SpectateGateTimer)
										
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec finished reset in SCORE CHECK")
										bSpecResetDone = TRUE
									ENDIF
								ELSE
									IF (GET_GAME_TIMER() % 2000) < 50
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec waiting for client flag bResetDone")
									ENDIF
								ENDIF
								
								IF bSpecResetDone
								AND ( ServerData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
									OR ServerData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_THROWING )
								AND ( playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage = DARTS_MPTHROW_AIM
									OR playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage = DARTS_MPTHROW_AIM )
								AND GET_TIMER_IN_SECONDS_SAFE(SpectateGateTimer) > 1.0
//									IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER) //serverData.bDoSwitchPlayer
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec did see server flag for switch_player")
										
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											IF iSpecThower = 1
												iSpecThower = 0
											ELIF iSpecThower = 0
												iSpecThower = 1
											ENDIF
										ENDIF
										iSpecThower = serverData.iThrower
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] New iSpecThrowerID: ", iSpecThower)
										//iSpecThower = playerData[iDartPlayerID[DARTS_PLAYER_TWO]].iOffsetPlayerID //<-
										
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
												UPDATE_HIGHLIGHT(DartsSB, PLAYERID_TO_SCOREID(serverData.iWaiter))
											 //<-
										ENDIF
//									ELSE
//										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec did not see server flag for switch_player")
//									ENDIF
									
									CANCEL_TIMER(SpectateGateTimer)
									
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec going back to waiting")
									SpecDartReceiveInfo.bThrow = FALSE
									SpecDartReceiveInfo.bResetDone = FALSE
									eSpecTurnStatus = DARTS_MPTURN_WAITING
								ENDIF
							BREAK
							
							CASE DARTS_MPTURN_WINNING
								
//								INT iDartPlayerOffReset
//								FOR iDartPlayerOffReset = 0 To 1
//									PED_INDEX piPReset
//									piPReset = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iDartPlayerOffReset])))
//									DARTS_RESET(PlayerDarts[iDartPlayerOffReset], piPReset)
//									DARTS_SELECT(playerDarts[iDartPlayerOffReset], piPReset, 0)
//								ENDFOR
//									PED_INDEX piPOne
//										piPOne = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_ONE])))
//										PED_INDEX piPTwo
//										piPTwo = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_TWO])))
//										
//										DARTS_RESET(PlayerDarts[DARTS_PLAYER_ONE], piPOne)
//										
										//DARTS_SELECT(playerDarts[DARTS_PLAYER_TWO], piPTwo, 0)
								
//								INT y
//								REPEAT NUM_MP_DART_TURNS y
//									///~RESET_DART(MPDartGame.Darts[y])
//									PlayerDarts[serverData.iThrower].PlayerDarts[y].bDoneScoring = FALSE
//									//MPDartGame.Darts[y].bDoneScoring = FALSE
//									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", y, " was reset")
//								ENDREPEAT
								PED_INDEX piPlayerOneReset
								piPlayerOneReset = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_ONE])))
			
								PED_INDEX piPlayerTwoReset
								piPlayerTwoReset = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_TWO])))		
								
								DARTS_RESET(PlayerDarts[DARTS_PLAYER_ONE], piPlayerOneReset)
								DARTS_RESET(PlayerDarts[DARTS_PLAYER_TWO], piPlayerTwoReset)
								
								DARTS_SELECT(PlayerDarts[DARTS_PLAYER_ONE],  piPlayerOneReset, 0)  
								DARTS_SELECT(PlayerDarts[DARTS_PLAYER_TWO],  piPlayerTwoReset, 0)  
								
								CANCEL_TIMER(controlTimer)
								
								SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SYNC_OBJECTS, SPEC)
							BREAK
						ENDSWITCH
						
					ELSE
						IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_THROW_OFF_DONE)
							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SYNC_OBJECTS, SPEC)
						ENDIF
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_SYNC_OBJECTS
					CDEBUG1LN(DEBUG_DARTS, "spectator DARTS_MPSTATE_SYNC_OBJECTS")
					i = 0
					j = 0
					
					// TODO this will have to be adjusted to account for mid game joins
					// looks like other player will have to place this info in the broadcast data
					REPEAT NUM_MP_DART_PLAYERS i
						MPDartGame.iThrowsTaken[i] 	= 0
						MPDartGame.iBullsEyesHit[i]	= 0
						DARTS_CLEAR_SCORES(DartsSB, i)
					//	iSpecLastScore[i] = 301
					ENDREPEAT
					
					//If joined mid game check current score on server and disaply that as latest.
					INT iScore
					iScore = ServerData.iScores[PLAYERID_TO_SCOREID(DARTS_PLAYER_ONE)]
					IF iScore = 0
						iScore = 301
					ENDIF
					DARTS_ADD_SCORE(DartsSB, PLAYERID_TO_SCOREID(DARTS_PLAYER_ONE), iScore)
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						iScore = ServerData.iScores[PLAYERID_TO_SCOREID(DARTS_PLAYER_TWO)]
						IF iScore = 0
							iScore = 301
						ENDIF
						DARTS_ADD_SCORE(DartsSB, PLAYERID_TO_SCOREID(DARTS_PLAYER_TWO), iScore)
					ENDIF
					
					REPEAT  NUM_MP_DART_TURNS j
						MPDartGame.Darts[j].bDoneScoring = FALSE
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", j, " was reset")
					ENDREPEAT
					
					DARTS_INIT_TIMERS(localDartsTimer)
					eSpecWaitStage = DARTS_MPWAIT_AIM
					MPDartGame.bEndGameMessageDisplayed = FALSE
					MPDartGame.bWinNoise = FALSE
					bInWinRange = FALSE
	
					
					SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SETUP_POST, SPEC)
				BREAK
				
				CASE DARTS_MPSTATE_SETUP_POST
					IF ServerData.mpState = DARTS_MPSTATE_IN_PROGRESS
							//disablecol?
						
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_IN_PROGRESS, SPEC)
						eSpecTurnStatus = DARTS_MPTURN_WAITING
						eSpecWaitStage = DARTS_MPWAIT_AIM
						CDEBUG1LN(DEBUG_DARTS, "spectator is proceeding to IN PROGRESS")
						//<-
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							iSpecThower = PICK_INT((serverData.iCoinFlip = 1), DARTS_PLAYER_TWO, DARTS_PLAYER_ONE)
						ELSE
							iSpecThower = DARTS_PLAYER_ONE
						ENDIF
						iSpecThower = serverData.iThrower
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							UPDATE_HIGHLIGHT(DartsSB, PICK_INT((serverData.iCoinFlip = 1), ENUM_TO_INT(DART_PLAYER_AWAY), ENUM_TO_INT(DART_PLAYER_HOME)))
							
						ELSE
							UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
						ENDIF
						bWaitUIShow = FALSE
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_IN_PROGRESS
//
//					IF serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_WINNING
//					OR serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_WINNING
//						eSpecTurnStatus = DARTS_MPTURN_WINNING
//					ENDIF
//					
//					// Do everyting playing related right chea
//					SWITCH eSpecTurnStatus 
//						CASE DARTS_MPTURN_WAITING
//							SWITCH eSpecWaitStage
//								CASE DARTS_MPWAIT_AIM
//									
//									IF SpecDartReceiveInfo.bThrow
//									//OR playerData[iDartPlayerID[iSpecThower]].bDartThrown //normal check
//										DARTS_CLEAR_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_SCORING_DONE)
//										CDEBUG1LN(DEBUG_DARTS, "Current client finished WAIT - AIMING")
//										SpecDartReceiveInfo.bThrow = FALSE
//										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
//										
//										// grab other players dart throw info so we can throw that thang
//										#IF IS_DEBUG_BUILD
//										iSpecTurn = ServerData.iServerTurn
//										#ENDIF
//
//										eSpecWaitStage = DARTS_MPWAIT_THROW
//									ELSE
//										IF (GET_GAME_TIMER() % 1000) < 50
//											IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX(iDartPlayerID[iSpecThower]) )
//												CDEBUG1LN(DEBUG_DARTS, "Spectator is waiting for client to throw PROG ->", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iSpecThower]))))
//											ENDIF
//										ENDIF
//									ENDIF
//								BREAK
//			
//								CASE DARTS_MPWAIT_THROW		
//									CDEBUG1LN(DEBUG_DARTS, "[spec prog] current client is WAITING - Throwing")
//									//IF THROW_DART_SPEC(MPDartGame.Darts[iSpecTurn], MPDartGame.dBoard)
//									//CDEBUG1LN(DEBUG_DARTS, "Regular Throw Received: ", MPDartGame.Darts[iSpecTurn].vOffsetTarget, " ThrowNum:",  iSpecTurn)
//										CDEBUG1LN(DEBUG_DARTS, "Regular Throw Completed - spec side")
//
//										bSpecResetDone = FALSE
//										eSpecWaitStage = DARTS_MPWAIT_SCORE
//										CDEBUG1LN(DEBUG_DARTS, "[spec] going to spec side score check")
//										eSpecTurnStatus = DARTS_MPTURN_SCORE_CHECK
//										
//									//ENDIF
//								BREAK
//								
//								CASE DARTS_MPWAIT_SCORE
//									IF (GET_GAME_TIMER() % 2000) < 50
//										CDEBUG1LN(DEBUG_DARTS, "[spec] going to spec side score check")
//									ENDIF
//								BREAK
//							ENDSWITCH
//						BREAK
//						
//						CASE DARTS_MPTURN_SCORE_CHECK
//							
//							// spectate is only for sp darts, so tuning this just for that
//							IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED)
//								// if prints are needed 
//								DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, TRUE)
//							ENDIF
//							DARTS_SET_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_SCORING_DONE)
//							IF NOT bSpecResetDone
//							//AND DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE)
//								//IF (playerData[iDartPlayerID[DARTS_PLAYER_ONE]].bResetDone OR playerData[iDartPlayerID[DARTS_PLAYER_TWO]].bResetDone)
//								IF (GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
//								OR GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE))
//								
//								OR SpecDartReceiveInfo.bResetDone
//								SpecDartReceiveInfo.bResetDone = FALSE
//								
//									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec is in SCORE CHECK")
//									
//									eSpecWaitStage = DARTS_MPWAIT_AIM
//									
//									RESTART_TIMER_NOW(SpectateGateTimer)
//									
//									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec finished reset in SCORE CHECK")
//									bSpecResetDone = TRUE
//								ENDIF
//							ELSE
//								IF (GET_GAME_TIMER() % 2000) < 50
//									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec waiting for client flag bResetDone, prog")
//								ENDIF
//							ENDIF
//														
//							IF bSpecResetDone
//							AND ( ServerData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
//								OR (ServerData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_THROWING 
//								AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)) )
//							AND ( playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage = DARTS_MPTHROW_AIM
//								OR (playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage = DARTS_MPTHROW_AIM 
//								AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)))
//							AND GET_TIMER_IN_SECONDS_SAFE(SpectateGateTimer) > 1.0
//									IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER) //serverData.bDoSwitchPlayer
//										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec did see server flag for switch_player")
//										// sending the darts back to the player, resetting them
//										// serves the same functioon as RESET_DART without deleting the object
//										
//										IF iSpecLastScore[ServerData.iThrower] <> ServerData.iScores[ServerData.iThrower] //<-
//											CDEBUG1LN(DEBUG_DARTS, "[spec] SCore UPDATED last score:", iSpecLastScore[ServerData.iThrower],
//											" serverscore:", ServerData.iScores[ServerData.iThrower])
//											
//											DARTS_ADD_SCORE(DartsSB, PLAYERID_TO_SCOREID(ServerData.iThrower), ServerData.iScores[ServerData.iThrower])
//										ENDIF
//										CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] ServerData.iThrower = ", ServerData.iThrower)
//										CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] ServerData.iLastScore = ", ServerData.iLastScore[ServerData.iThrower])
//										CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] ServerData.iScores = ", ServerData.iScores[ServerData.iThrower])
//										CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] iSpecLastScore = ", iSpecLastScore[playerData[ServerData.iThrower].iOffsetPlayerID])
//										
//										CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] ServerData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_HOME]] = ", ServerData.mpTurnStatus[iSelfDartID])
//										CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] ServerData.mpTurnStatus[iSpectatePlayerID[DART_PLAYER_AWAY]] = ", ServerData.mpTurnStatus[iOpponentDartID])
//										CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] playerData[iSpectatePlayerID[DART_PLAYER_HOME]].eMPThrowStage = ", playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage)
//										CDEBUG2LN(DEBUG_DARTS, "[AM_DARTS] playerData[iSpectatePlayerID[DART_PLAYER_AWAY]].eMPThrowStage = ", playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage)
//										
//										IF ServerData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
//											iSpecThower = DARTS_PLAYER_ONE
//											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] iSpecThower is going home")
//										ELSE
//											iSpecThower = DARTS_PLAYER_TWO
//											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] iSpecThower is going away")
//										ENDIF
//										
//										iSpecThower = serverData.iThrower
//										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] iSpecThower = ", iSpecThower)
//										
//										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
//
//										UPDATE_HIGHLIGHT(DartsSB,PLAYERID_TO_SCOREID(serverData.iWaiter))
//
//										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] changed highlight to player ", serverData.iWaiter)
//										ENDIF
//									ELSE
//										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec did not see server flag for switch_player")
//									ENDIF
//									
//									CANCEL_TIMER(SpectateGateTimer)
//																										
//									
//									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] spec going back to waiting")
//									SpecDartReceiveInfo.bThrow = FALSE
//									SpecDartReceiveInfo.bResetDone = FALSE
//									eSpecTurnStatus = DARTS_MPTURN_WAITING
//								ENDIF
//						BREAK
//						
//						CASE DARTS_MPTURN_WINNING
//							CDEBUG1LN(DEBUG_DARTS, "********** serverData.iWinner = ", serverData.iWinner)
//							
//							CLEAR_PRINTS()
//							
//							IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
//								CDEBUG1LN(DEBUG_DARTS, "[spec] server said to go to transition")
//								iSplashStage = 0
//								RESTART_TIMER_NOW(controlTimer)
//								SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_GAME_TRANSITION, SPEC)
//							ELSE
//								RESTART_TIMER_NOW(controlTimer)
//								CDEBUG1LN(DEBUG_DARTS, "[spec] server said to go to game end")
//								SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_GAME_END, SPEC)
//							ENDIF
//						BREAK
//					ENDSWITCH
				BREAK
				
				CASE DARTS_MPSTATE_GAME_TRANSITION				
					SWITCH iSplashStage
						CASE 0
							IF GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 1.5
								//IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
								//	CDEBUG1LN(DEBUG_DARTS, "[spec] trans sb")
									UPDATE_SB_GAMES(DartsSB.siScoreBoard, bSinglePlayerGame, TRUE, serverData.iSets[DARTS_PLAYER_TWO], serverData.iSets[DARTS_PLAYER_ONE])
//								ELSE
//									CDEBUG1LN(DEBUG_DARTS, "[spec] trans s2b")
//									UPDATE_SB_GAMES(DartsSB.siScoreBoard, serverData.iSets[DARTS_PLAYER_TWO], serverData.iSets[DARTS_PLAYER_ONE])
//								ENDIF
								iSplashStage++
							ENDIF
						BREAK
						
						CASE 1
								REPEAT NUM_MP_DART_PLAYERS i
									DARTS_CLEAR_SCORES(DartsSB, i)
								ENDREPEAT
//								PED_INDEX piPOne
//								piPOne = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_ONE])))
//								PED_INDEX piPTwo
//								piPTwo = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_TWO])))
//								
//								DARTS_RESET(PlayerDarts[DARTS_PLAYER_ONE], piPOne)
//								DARTS_RESET(PlayerDarts[DARTS_PLAYER_TWO], piPTwo)
//								REPEAT NUM_MP_DART_TURNS j
//									RESET_DART(MPDartGame.Darts[j])
//								ENDREPEAT
								
								iSplashStage++
						BREAK
						
						CASE 2
							CDEBUG1LN(DEBUG_DARTS, "spec going to wait post game Starting another set/leg")							
							iSplashStage = 0
							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_WAIT_POST_GAME, SPEC)
						BREAK
						
					ENDSWITCH
				BREAK
				
				CASE DARTS_MPSTATE_GAME_END
					
					IF (GET_GAME_TIMER() % 2500) < 50
					AND IS_TIMER_STARTED(controlTimer)
						CDEBUG1LN(DEBUG_DARTS, "Reached spec side GAME END timer: ", GET_TIMER_IN_SECONDS_SAFE(controlTimer) )
					ENDIF
					
					IF NOT IS_TIMER_STARTED(controlTimer)
						RESTART_TIMER_NOW(controlTimer)
						
					ELIF GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 0.75
						// wait for the player to pick an option
						//IF MPDartGame.bEndGameMessageDisplayed 
							
							//UPDATE_SHARD_BIG_MESSAGE(shardBigMessage)
							
							IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
							AND NOT IS_PLAYER_SCTV(PLAYER_ID())
							//	IF HAS_NET_TIMER_STARTED(serverData.timeLeaderboardTimeOut)
									
//									IF MAINTAIN_END_OF_MISSION_SCREEN(sEndOfMission, serverData.sServerFMMC_EOM,
//																GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),serverData.timeLeaderboardTimeOut.timer) >= VIEW_LEADERBOARD_TIME,
//																FALSE, FALSE, FALSE, FALSE, FALSE, serverData.timeLeaderboardTimeOut.timer, VIEW_LEADERBOARD_TIME, "LBD_TIMEOUT", TRUE)
										//Go to cleanup state
										CDEBUG1LN(DEBUG_DARTS, "spectate voted")
										SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_END, SPEC)
//									ELSE
//										//DISPLAY_DARTS_LEADERBOARD(DartsUI, DartsEndScreen, iXPGained, DartsUI.iOurScore, DartsUI.iTheirScore, iBetWinnings)
//									ENDIF
							//	ENDIF
							ENDIF
							
							// player wants another game //<-
							IF (DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							AND GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH))
							//AND PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].bDoRematch )
							
							OR NOT (DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							//AND PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].bDoRematch 
							//AND PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].bDoRematch)
							AND GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH)
							AND GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH))
								IF serverData.mpState >= DARTS_MPSTATE_SETUP
									
									INT iTemp1, iTemp2
									iTemp1 = ENUM_TO_INT(DART_PLAYER_AWAY)
									iTemp2 = ENUM_TO_INT(DART_PLAYER_HOME)
									
									CDEBUG1LN(DEBUG_DARTS, "iTemp1 = ", iTemp1, " iTemp2 = ", iTemp2)
									DARTS_CLEAR_SCORES(DartsSB, iTemp1)
									DARTS_CLEAR_SCORES(DartsSB, iTemp2)
								
									SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_WAIT_POST_GAME, SPEC)
									
								ENDIF
							ENDIF
							
								DartsUI.iOurScore += serverData.iSets[DARTS_PLAYER_ONE]
								DartsUI.iTheirScore += serverData.iSets[DARTS_PLAYER_TWO]
								DartsUI.sMyName = sDartsPlayerNames[DARTS_PLAYER_ONE]
								DartsUI.sTheirName = sDartsPlayerNames[DARTS_PLAYER_TWO]
								
								DartsUI.iEndDelay = GET_GAME_TIMER()

					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_WAIT_POST_GAME
					DEBUG_MESSAGE_PERIODIC("spectate DARTS_MPSTATE_WAIT_POST_GAME", iDebugThrottle)
					
					//IF (PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].bDoRematch AND PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].bDoRematch)
					IF (GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH)
					AND GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH))
					
					OR (GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH)
					AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER))
					
					//OR (PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].bDoRematch AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER))
					
					OR PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState = DARTS_MPSTATE_IN_PROGRESS
						//IF serverData.mpState = DARTS_MPSTATE_SETUP
							CDEBUG1LN(DEBUG_DARTS, "spectate going to sync objects")
							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SYNC_OBJECTS, SPEC)
						//ENDIF
					ENDIF
					
				BREAK
				
				CASE DARTS_MPSTATE_TERMINATE_SPLASH
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_DARTS, "spec is chilling in DARTS_MPSTATE_TERMINATE_SPLASH")
						IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
							CDEBUG1LN(DEBUG_DARTS, "still being seen as a spectator")
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "no longer a spectator - this is physically impossible")
						ENDIF
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_END
					CDEBUG1LN(DEBUG_DARTS, "spec going to STARTING END")
					IF HAS_NET_TIMER_STARTED(timeDartsAwardFailSafe)
						CDEBUG1LN(DEBUG_DARTS, "spec going to DARTS_MPSTATE_LEAVE")
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_LEAVE, SPEC)
					ELSE
						START_NET_TIMER(timeDartsAwardFailSafe)
						CDEBUG1LN(DEBUG_DARTS, "started failsafe net timer")
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_LEAVE
					SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
					CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB,  PlayerData, PlayerDarts,MPDartGame, DartsUI, TRUE, FALSE, FALSE)
					CDEBUG1LN(DEBUG_DARTS, "spec cleanup called in DARTS_MPSTATE_LEAVE")
				BREAK
			ENDSWITCH
			
			//########################## END OF SPEC STATE #############################	
				
			ELSE

				//UPDATE DARTS, SWITCHING, THROWING
				INT iDartPlayer
				REPEAT 2 iDartPlayer
					//CDEBUG1LN(DEBUG_DARTS, "LOOP DART INFO", iDartPlayer)
					IF IS_NET_PARTICIPANT_INT_ID_VALID(PlayerDarts[iDartPlayer].iParticipantID)
						//CDEBUG1LN(DEBUG_DARTS, "VALID PARTICIPANT")
						DART_GET_INTRO(PlayerDarts[iDartPlayer])
					ENDIF
				ENDREPEAT
			
			//###########################################################################
			//########################### NORMAL GAMEPALY ###############################
			SWITCH PlayerData[iSelfID].eMPState
				//#Wait until server has loaded
				//#Sum remove player gun, toggle off controlls, fade out screen 
				CASE DARTS_MPSTATE_WAIT_PRE_INIT
					IF ServerData.mpState > DARTS_MPSTATE_INIT
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)

							IF NOT IS_PLAYER_SCTV(PLAYER_ID())
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE,  NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)
								//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
							ENDIF
						
							REQUEST_ANIM_DICT("mini@dartsintro_alt1")
							REQUEST_ANIM_DICT("mini@darts")
							REQUEST_ANIM_DICT("anim@amb@clubhouse@mini@darts@")
							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_PRE_INIT)
						ENDIF
					ENDIF
				BREAK
				
				//Set the player to be in the correct position to play the darts game
				CASE DARTS_MPSTATE_PRE_INIT
					SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_PRE_INIT_WAIT)
				BREAK
				
				//######################################################################
				//### If Single player task ped to go to location and play loop anim ###
				CASE DARTS_MPSTATE_PRE_INIT_WAIT
			//	vInitLostHideoutPos = vInitLostHideoutPos
					//CDEBUG1LN(DEBUG_DARTS, "Is already in Gang House?: ",bAlreadyInGangHouse)
					//GANG HOUSE?
//					IF NOT bAlreadyInGangHouse
//						IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM 
//						AND NETWORK_UPDATE_LOAD_SCENE()
//							CDEBUG1LN(DEBUG_DARTS, "NETWORK_UPDATE_LOAD_SCENE finished")
//							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
//							//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_INIT
//							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_INIT)
//							//CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] has reached Client side PRE INIT WAIT")
//						ELSE
//							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
//							
//							
//							
//							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_INIT)
//						ENDIF
//					BOOL bTaskFinishedManual
//					bTaskFinishedManual = FALSE
//					
//					IF NOT IS_TIMER_STARTED(TimerInPosition)
//						CDEBUG1LN(DEBUG_DARTS, "Started timer fail safe player game location")
//						START_TIMER_NOW(TimerInPosition)
//					ELIF GET_TIMER_IN_SECONDS(TimerInPosition) > COSNT_TIME_BEFORE_GAME_TRANSITION
//						CDEBUG1LN(DEBUG_DARTS, "FAILSAFE game position skip timer reached")
//						//if didnt get into game position in X time , start the game anyway.
//						DartPlayerPosition = DART_PLAYER_IN_POSITION
//						DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_IN_POSITION)
//						bTaskFinishedManual = TRUE
//					ENDIF
//
//					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
//					OR bTaskFinishedManual
						
						//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_IN_POSITION)
							CDEBUG1LN(DEBUG_DARTS, "Darts player in place")
							DartPlayerPosition = DART_PLAYER_IN_POSITION
							DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_IN_POSITION)
						
						//IF IN POSITION
						ELSE DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_IN_POSITION)
							//CDEBUG1LN(DEBUG_DARTS, "IN POSITION TASK FINISHED")
							//IF SINGLE PLAYER
							//DO_SCREEN_FADE_OUT(500)
//							IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)

							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_INIT)

						ENDIF
					//ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_INIT	
				
					IF NOT IS_TIMER_STARTED(stGameViewDelay)
						START_TIMER_NOW(stGameViewDelay)
					ENDIF
					
					//IF IS_SCREEN_FADED_OUT()
					//AND GET_TIMER_IN_SECONDS_SAFE(stGameViewDelay) >= CONST_DELAY_GAME_VIEW
						CDEBUG1LN(DEBUG_DARTS, "Fade delay complete", GET_TIMER_IN_SECONDS_SAFE(stGameViewDelay))
							INIT_BOARD(MPDartGame.dBoard, Args)
							INIT_CAM(MPDartGame.dBoard.vDartBoard, MPDartGame.dBoard.fBoardHeading)
							
							INTRO_DART_CREATE(sLocalIntroScene.objDart, MPDartGame.dBoard.vDartBoard)
							//INTRO_DART_VISIBILITY(sLocalIntroScene.objDart, FALSE)
							INT k
							REPEAT NUM_DART_TURNS k
								//INIT_DARTS(MPDartGame.Darts[k], 0)
								INIT_DARTS(MPDartGame.Darts[k], 0)
							ENDREPEAT
							
							INIT_SPRITE_RETICLE()
							MPDartGame.dBoard.fBoardHeading += 0.01 // V function check if heading is 0 and then doesnt audo adjust score board......
							DARTS_INIT_SCOREBOARD(DartsSB, ENUM_TO_INT(DartsLocation), MPDartGame.dBoard.vDartBoard, MPDartGame.dBoard.fBoardHeading)
							REQUEST_STREAMS(DartsSB, bSpectator)
							REGISTER_SCRIPT_WITH_AUDIO()
							DISABLE_CELLPHONE(TRUE)
							
							
							iSteadyShotLength = 770 
							
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
									IF NOT IS_PLAYER_SCTV(PLAYER_ID())
										//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
										CDEBUG1LN(DEBUG_DARTS, "Had to turn player control off again in  DARTS_MPSTATE_INIT")
									ENDIF
								ENDIF
							ENDIF
							
							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_STREAMING)
					//ENDIF
				BREAK
				
				//#Sum check if game essential assets are loaded
				CASE DARTS_MPSTATE_STREAMING
					IF HAVE_STREAMS_LOADED(DartsSB, bSpectator)
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SETUP)
					ENDIF
				BREAK
				
				//set positions for sync scene, clothes check, 
				CASE DARTS_MPSTATE_SETUP
					DARTS_SET_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_MATCH_MADE)
					// placing the players in their prospective positions
					IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM 
					//OR NOT bAlreadyInGangHouse
						
						//GET_WORLD_OFFSETS(MPDartGame.dBoard, DartsPlayerPos, DartsInitReticlePos, TRUE)
						CDEBUG1LN(DEBUG_DARTS, "GET_WORLD_OFFSETS obtained in DARTS_MPSTATE_SETUP")
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							
							CDEBUG1LN(DEBUG_DARTS, "my player pos = ", DartsPlayerPos.vDartPlayrPos)
							CDEBUG1LN(DEBUG_DARTS, "oponent   pos = ", DartsPlayerPos.vOpponentPos)
							//tell ped to move to game start position
							//TASK_PED_SLIDE_TO_COORD(PLAYER_PED_ID(), DartsPlayerPos.vDartPlayrPos, MPDartGame.dBoard.fBoardHeading)
							//SET_ENTITY_COORDS(PLAYER_PED_ID(), DartsPlayerPos.vDartPlayrPos)
							//SET_ENTITY_HEADING(PLAYER_PED_ID(), MPDartGame.dBoard.fBoardHeading)
							PLAYER_HOLSTER_WEAPON()
							PLAYER_TOGGLE_CONTROLS(FALSE)
						ENDIF
					ENDIF
					
					oiReticle = CREATE_OBJECT(modelReticleRegular, DartsInitReticlePos.vInitReticlePos, FALSE, FALSE)
					
				//	oiDartChallenge = CREATE_OBJECT(PROP_DART_1, DartsInitReticlePos.vInitReticlePos, FALSE, FALSE)
					
					// set minigame control scaleforms
					DARTS_ACTIVATE_QUIT_UI(DartsUI.uiQuitControls)
					DARTS_ACTIVATE_ENDGAME_UI(DartsUI.uiEndGameControls)
					
					sDartsPlayerNames[iSelfDartID] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iSelfDartID])))
					DartsGamerHandle[iSelfDartID] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iSelfDartID])))
					sCrewTags[iSelfDartID] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[iSelfDartID])
					
					///////////////////////////////
					//INT iScoreOpponentID, iScoreSelfID
					//iScoreOpponentID = PLAYERID_TO_SCOREID(iOpponentDartID)//ENUM_TO_INT(DART_PLAYER_AWAY)
					//iScoreSelfID = PLAYERID_TO_SCOREID(iSelfDartID)//ENUM_TO_INT(DART_PLAYER_HOME)
						
						
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						
						sDartsPlayerNames[iOpponentDartID] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iOpponentDartID])))
						DartsGamerHandle[iOpponentDartID] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iOpponentDartID])))
						sCrewTags[iOpponentDartID] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[iOpponentDartID])
						
						//piOtherPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
							CDEBUG1LN(DEBUG_DARTS, "CHECK ID iDartIndx:", iDartPlayerID[iOpponentDartID], " Saved: iOther:", iOtherID)
						piPlayerClones[0] = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
						piPlayerClones[1] = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSelfID)))


						//CDEBUG1LN(DEBUG_DARTS, "iTemp1 = ", iScoreOpponentID, " iTemp2 = ", iScoreSelfID)
						//CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iScoreOpponentID, " = ", sCrewTags[iOpponentDartID])
						//CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iScoreSelfID, " = ", sCrewTags[iSelfDartID])
						
						
						
						DARTS_ADD_NAMES(DartsSB, sDartsPlayerNames[iOpponentDartID], sDartsPlayerNames[iSelfDartID])
						IF NOT IS_STRING_EMPTY(sCrewTags[iOpponentDartID])
							DARTS_ADD_SB_CREW_TAG_UGC_CHECK(DartsSB,  ENUM_TO_INT(DART_PLAYER_AWAY), sCrewTags[iOpponentDartID])
						ENDIF
						IF NOT IS_STRING_EMPTY(sCrewTags[iSelfDartID])
							DARTS_ADD_SB_CREW_TAG_UGC_CHECK(DartsSB,  ENUM_TO_INT(DART_PLAYER_HOME), sCrewTags[iSelfDartID])
						ENDIF
						DARTS_CLEAR_SCORES(DartsSB,  ENUM_TO_INT(DART_PLAYER_AWAY))
						DARTS_CLEAR_SCORES(DartsSB,  ENUM_TO_INT(DART_PLAYER_HOME))
						
					ELSE
						//CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iScoreSelfID, " = ", sCrewTags[iSelfDartID])
						DARTS_ADD_NAMES(DartsSB, "", sDartsPlayerNames[iSelfDartID])
						IF NOT IS_STRING_EMPTY(sCrewTags[iSelfDartID])
							DARTS_ADD_SB_CREW_TAG_UGC_CHECK(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME), sCrewTags[iSelfDartID])
						ENDIF
						DARTS_CLEAR_SCORES(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
					ENDIF
					
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts.sc: serverData.iNumLegs = ", serverData.iNumLegs)
					CDEBUG1LN(DEBUG_DARTS, "AM_Darts.sc: serverData.iNumSets = ", serverData.iNumSets)
					
					IF serverData.iNumLegs > 0
					OR serverData.iNumSets > 0
						UPDATE_SB_GAMES(DartsSB.siScoreBoard, bSinglePlayerGame, bIsHost, 0, 0)
					ENDIF
					
					IF bShowSplash
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_CUSTOM, "MN_DART")
						CDEBUG1LN(DEBUG_DARTS, "AM_Darts.sc: Big 'Darts' Message displayed")
						
						START_TIMER_NOW(controlTimer)
						
						ANOTHER_GAME_CAM_MP(PLAYER_PED_ID())
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
					
					//CLEAR_AREA(Args.vDartBoard, 0.5, TRUE)
					//REMOVE_DECALS_IN_RANGE(Args.vDartBoard, 0.5)
					
					SET_ON_JOB_INTRO(TRUE)
					SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_READY_UP)
				BREAK
				
				CASE DARTS_MPSTATE_READY_UP
					IF (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT())
					AND GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 1.0
					AND bShowSplash
					AND NOT IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
						CDEBUG1LN(DEBUG_DARTS, "[DARTS_MPSTATE_READY_UP] fade in called for")
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					IF ( NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_TUTORIAL)
						AND ((IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) AND GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 1.6)
							OR GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 20.0) )
					OR NOT bShowSplash
						
						IF bShowSplash
							CLEANUP_BIG_MESSAGE()
							CANCEL_TIMER(controlTimer)
						ENDIF
						
						//IF IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_MPTUTORIAL_SHOWN)
						//IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_DARTS_SEEN_TUTORIAL)
							DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_DONE)
							CDEBUG1LN(DEBUG_DARTS, "Tutorial has been seen, skipping it")
							CDEBUG1LN(DEBUG_DARTS, "tbh the tutorial will always get skipped")
						//ELSE
						//	CDEBUG1LN(DEBUG_DARTS, "Tutorial has not been seen yet")
						//ENDIF
						
						//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_TUTORIAL
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_INTRO_SYNC_SETUP)
						DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_TUTORIAL)
					ELSE
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_TUTORIAL)
						AND bShowSplash
							UPDATE_CUSTOM_BIG_MESSAGE("MN_DART")
						ENDIF
					ENDIF
				BREAK
				
				//##############################################################################################
				//############################# CLIENT - DARTS_MPSTATE_TUTORIAL ################################
				CASE DARTS_MPSTATE_TUTORIAL
					IF (DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_TUTORIAL) AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_AT_TUTORIAL))
					OR (DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_TUTORIAL) AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER))
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
						AND (NOT (DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_DONE) AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_TUTORIAL_DONE))
							AND NOT (DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_TUTORIAL_DONE) AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)))
							IF DARTS_TUTORIAL(DartsTutorialStage, MPDartGame.dBoard, AIBrain, 
											TurnStage, MPDartGame.Darts, TutorialTimer, PlayerData)
							
								CDEBUG1LN(DEBUG_DARTS, "FINISHED TUTORIAL")
								SET_MP_BOOL_CHARACTER_STAT(MP_STAT_DARTS_SEEN_TUTORIAL, TRUE)
								CDEBUG1LN(DEBUG_DARTS, "Setting MP_STAT_DARTS_SEEN_TUTORIAL to true")
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_INTRO_SYNC_SETUP)
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]  setting FM Match Start just before the game starts")
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_FM_MATCH_STARTED)
										DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_DARTS, ServerData.iMatchHistoryID, ServerData.iMatchType)
										CDEBUG1LN(DEBUG_DARTS, "JOB ENTRY TYPE: ", GET_FM_JOB_ENTERY_TYPE())
										DARTS_SET_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_FM_MATCH_STARTED)
									ENDIF
									SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SYNC_OBJECTS)
								ENDIF
							ENDIF
							
						ELSE
						//#####################################################################
						//########################### SWITCH BETWEEN MP AND SP... #############
							CDEBUG1LN(DEBUG_DARTS, "TUTORIAL ALREADY SHOWN, SKIPPING")
							CLEAR_HELP()
							DARTS_GAME_CAM()
							SAFE_BIT_TOGGLE(iLocalStateFlags, ENUM_TO_INT(eLP_DRAW_SCORE_BOARD))
							
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							
							IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
								SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_THROW_OFF_SETUP)
							ELSE
								CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]  setting FM Match Start just before the game starts")
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_FM_MATCH_STARTED)
									DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_DARTS, ServerData.iMatchHistoryID, ServerData.iMatchType)
									CDEBUG1LN(DEBUG_DARTS, "JOB ENTRY TYPE: ", GET_FM_JOB_ENTERY_TYPE())
									DARTS_SET_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_FM_MATCH_STARTED)
								ENDIF
								SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SYNC_OBJECTS)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				
				//### REUSE STATES FOR INTRO CAM INSTEAD
				CASE DARTS_MPSTATE_INTRO_SYNC_SETUP
					INIT_PRE_GAME_CAM(dsCurrent, camDartPreGame, DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER))

						
					SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_INTRO_SYNC_PLAY)
				BREAK
				
				CASE DARTS_MPSTATE_INTRO_SYNC_PLAY
					//CHECK IF BOTH REACHED POSITION AND PLAYED ANIM FOR x time?
					SWITCH(eIntroSequence)
						
						CASE eIT_INIT
//							IF NOT IS_TIMER_STARTED(stFlowCheck)
//								START_TIMER_NOW(stFlowCheck)
//							ENDIF
//							
//							IF SPAWN_HAND_BLOCKER(PlayerDarts[iSelfDartID])
//							OR GET_TIMER_IN_SECONDS(stFlowCheck) >= 2.5
//								IF DOES_ENTITY_EXIST(PlayerDarts[iSelfDartID].oHandBlocker)
//									//disable collision between players.
//									TOGGLE_COLLISION_BETWEEN_ENTITIES(PlayerDarts[iSelfDartID].oHandBlocker, PlayerDarts[iSelfDartID].pedID, FALSE)
//									TOGGLE_COLLISION_BETWEEN_ENTITIES(PlayerDarts[iSelfDartID].oHandBlocker, PlayerDarts[iOpponentDartID].pedID, FALSE)
//									CDEBUG1LN(DEBUG_DARTS, "Hand Blocker Spawned")
//								ELSE
//									CDEBUG1LN(DEBUG_DARTS, "Hand Blocker Did not Spawn")
//								ENDIF
								eIntroSequence = eIT_MOVE_TO_START
								CANCEL_TIMER(stFlowCheck)
//							ENDIF					
						BREAK
						
						CASE eIT_MOVE_TO_START					
						
							VECTOR 	vPlayerPosition
							FLOAT 	fPlayerHeading
							vPlayerPosition = GET_PLAYER_POSITION(dsCurrent , bPlayerSideA)
							fPlayerHeading = GET_HEADING_BETWEEN_VECTORS_2D(vPlayerPosition, MPDartGame.dBoard.vDartBoard)
								
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())						
								CLEAR_SEQUENCE_TASK(siTemp)
								OPEN_SEQUENCE_TASK(siTemp)
								TASK_GO_STRAIGHT_TO_COORD(NULL, vPlayerPosition, PEDMOVEBLENDRATIO_WALK, 6000, fPlayerHeading, 0.020)
								TASK_PLAY_ANIM(NULL, "anim@amb@clubhouse@mini@darts@", PICK_INTRO_ANIM(bSetupPlayer), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME,0,FALSE, AIK_NONE, FALSE)
								TASK_PLAY_ANIM(NULL, "anim@amb@clubhouse@mini@darts@", "throw_idle_a_down", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0 , FALSE, AIK_NONE, FALSE)
								CLOSE_SEQUENCE_TASK(siTemp)
								TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siTemp)
								CDEBUG1LN(DEBUG_DARTS, "TASKED TO GO TO LOCATION: ", vPlayerPosition)
							ENDIF
							
							eIntroSequence = eIT_INTRO_CAM
						BREAK						
													
						CASE eIT_INTRO_CAM
							
							INTRO_SCENE_CREATE(PlayerData, PlayerDarts, sLocalIntroScene, dsCurrent)
							
							IF DOES_CAM_EXIST(camDartPreGame)
								SET_CAM_ACTIVE(camDartPreGame, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								SHAKE_CAM(camDartPreGame, "HAND_SHAKE", 0.3)
								CDEBUG1LN(DEBUG_DARTS, "Intro - cam created, shake added")
							ENDIF
							
							SAFE_BIT_TOGGLE(iLocalStateFlags, ENUM_TO_INT(eLP_HIDE_PLAYER_OVERHEAD)) 
							SAFE_BIT_TOGGLE(iLocalStateFlags, ENUM_TO_INT(eLP_HIDE_PLAYER_HUD)) 
							SAFE_BIT_TOGGLE(iLocalStateFlags, ENUM_TO_INT(eLP_HIDE_PARTICIPANTS))
							SAFE_BIT_TOGGLE(iLocalStateFlags, ENUM_TO_INT(eLP_HIDE_PLAYERS))
							CDEBUG1LN(DEBUG_DARTS, "Intro - Update Opponenet Transform Flag Toggled ON")

							eIntroSequence = eIT_WAIT_FOR_ANIM
						BREAK
						
						CASE eIT_WAIT_FOR_ANIM
							
							IF NOT IS_TIMER_STARTED(TimerInPosition)
								CDEBUG1LN(DEBUG_DARTS, "STARTED TIMER FOR INTRO CAM SWITCH")
								START_TIMER_NOW(TimerInPosition)
							ELIF GET_TIMER_IN_SECONDS(TimerInPosition) > 5.5
							OR  GET_SYNCHRONIZED_SCENE_PHASE(sLocalIntroScene.iSceneID) >= 0.95
								CANCEL_TIMER(TimerInPosition)
								SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_TUTORIAL)
							ENDIF
						BREAK
					ENDSWITCH
				BREAK		
				
				
				//##############################################################################################
				//############################# DARTS_MPSTATE_THROW_OFF_SETUP ##################################
				CASE DARTS_MPSTATE_THROW_OFF_SETUP
					IF NOT IS_TIMER_STARTED(controlTimer)
						
						SET_SHARD_BIG_MESSAGE(shardBigMessage, "DARTS_THRW_OFF", "DARTS_THRW_STR", 3000)
						
						START_TIMER_NOW(controlTimer)
						
						IF IS_SCREEN_FADED_OUT()
						AND NOT IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
							DO_SCREEN_FADE_IN(500)
						ENDIF
					ELIF GET_TIMER_IN_SECONDS_SAFE(controlTimer) >= 3.5
					//OR (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) 
					//	AND GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 1.6)
					//OR NOT UPDATE_SCALEFORM_BIG_MESSAGE(DartsUI.siBigMessage, TRUE)
					OR NOT UPDATE_SHARD_BIG_MESSAGE(shardBigMessage)
						
						CLEANUP_BIG_MESSAGE()
						CANCEL_TIMER(controlTimer)
						
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]  setting FM Match Start just before the game starts")
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_FM_MATCH_STARTED)
							DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_DARTS, ServerData.iMatchHistoryID, ServerData.iMatchType)
							CDEBUG1LN(DEBUG_DARTS, "JOB ENTRY TYPE: ", GET_FM_JOB_ENTERY_TYPE())
							DARTS_SET_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_FM_MATCH_STARTED)
						ENDIF
						
						IF serverData.iWaiter = iSelfDartID
							UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
						ELSE
							UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_AWAY))
						ENDIF
						
						DARTS_INIT_TIMERS(localDartsTimer)
						
						RESET_SHARD_BIG_MESSAGE(shardBigMessage)
						
						DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_THROWOFF)
						//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_THROW_OFF
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_THROW_OFF)
					ENDIF
				BREAK
				
				// #THROWOFF GAMEPLAY HERE VVVVVVV
				//##############################################################################################
				//############################# DARTS_MPSTATE_THROW_OFF - MULTIPLAYER.... ####################
				CASE DARTS_MPSTATE_THROW_OFF
					
					IF DARTS_SERVER_CHECK_FLAG(ServerData, DARTSERVERFLAG_DO_THROWOFF)
						
						IF IS_SCREEN_FADED_OUT()
						AND NOT IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
							DO_SCREEN_FADE_IN(500)
						ENDIF
						
	//					IF DARTS_PLAYER_QUIT_CONTROL(PlayerData[iSelfID], DartsUI, iDebugThrottle)
	//						PlayerData[iSelfID].eMPState = DARTS_MPSTATE_END
	//					ENDIF
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
							IF NOT (serverData.mpTurnStatus[iSelfDartID] = DARTS_MPTURN_WINNING)
							
							//AND ( NOT playerData[iSelfID].bDartThrown = TRUE
							//	OR NOT playerData[iOtherID].bDartThrown = TRUE ) //<-----
							
							AND ( NOT GET_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN) = TRUE
								OR NOT GET_PLAYER_STATE(playerData[iDartPlayerID[iOpponentDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN) = TRUE)
								
							AND NOT DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
								IF NOT bUITextDisplayed
									IF NOT (serverData.mpTurnStatus[iSelfDartID] = DARTS_MPTURN_SCORE_CHECK)
										IF bShowShotClockThrowHelp
										AND NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLOCK_HELP)
											IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gSHOT_CLOCK_HELP_SHOWN)
												PRINT_HELP("DARTS_TIOT")
												SET_BIT(g_iDartsTutorialFlags, gSHOT_CLOCK_HELP_SHOWN)
											ENDIF
											DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLOCK_HELP, TRUE)
										ENDIF
									ENDIF
									DartsUI.bInstructTextTransIn = TRUE
								ELSE
									IF DartsUI.bInstructTextTransIn
										CLEAR_HELP()
										DartsUI.bInstructTextTransIn = FALSE
									ENDIF
								ENDIF
							ENDIF
							
							// Drawing the instuctional button UI
							IF ((playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_AIM) 
									AND (serverData.mpTurnStatus[iSelfDartID] = DARTS_MPTURN_THROWING))
							OR bWaitUIShow
								DARTS_MP_THROWOFF_CONTROLS_UI(DartsUI, (serverData.mpTurnStatus[iSelfDartID] = DARTS_MPTURN_THROWING))
							ENDIF
						ENDIF
						
						IF(GET_GAME_TIMER() % 2000) < 50
							CDEBUG1LN(DEBUG_DARTS, "THROW serverMyTurn:", serverData.mpTurnStatus[iSelfDartID],
							" throwStage:", playerData[iSelfID].eMPThrowStage)
						ENDIF
						
						// Do everyting playing related right chea
						//##############################################################################################
						//wait for server to catch up and switch gameaply states.
						SWITCH serverData.mpTurnStatus[iSelfDartID] 
							CASE DARTS_MPTURN_THROWING
								SWITCH playerData[iSelfID].eMPThrowStage
									CASE DARTS_MPTHROW_AIM
										
										//DARTS_GAME_HELP(ServerData.iServerTurn, TRUE)
										
	//									IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLECK_HELP)
	//										IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_CLOCK_HELP_SHOWN)
	//											PRINT_HELP("DARTS_CLOCK")
	//											SET_BIT(g_iDartsTutorialFlags, gDARTS_CLOCK_HELP_SHOWN)
	//										ENDIF
	//										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLECK_HELP, TRUE)
	//									ENDIF
										
										IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_AIM_TUT_SHOWN)
											PRINT_HELP("DARTS_AIM_HLP")
											SET_BIT(g_iDartsTutorialFlags, gDARTS_AIM_TUT_SHOWN)
										ELIF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_AIM_HLP")
										AND NOT IS_BIT_SET(g_iDartsTutorialFlags, gDARTS_CLOCK_HELP_SHOWN)
											PRINT_HELP("DARTS_CLOCK")
											SET_BIT(g_iDartsTutorialFlags, gDARTS_CLOCK_HELP_SHOWN)
										ENDIF
										
										IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
											DARTS_DRAW_THROW_OFF_CLOCK(ServerData.DartsTimer, TRUE)
										ENDIF
										
										IF ServerData.DartsTimer.bNewShot
											CDEBUG1LN(DEBUG_DARTS, "Server timer has sensed a new shot!")
										ENDIF
										
										IF ServerData.DartsTimer.bTimeRunOut
											CDEBUG1LN(DEBUG_DARTS, "Server timer has sensed that it has run out!")
										ENDIF
										
										IF (NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
											AND NOT IS_PAUSE_MENU_ACTIVE())
										OR ServerData.DartsTimer.bTimeRunOut
											IF DO_AIMING(MPDartGame.Darts[serverData.iServerTurn], MPDartGame.dBoard,
														DartsInitReticlePos, FALSE, ServerData.DartsTimer.bTimeRunOut, TRUE)
												CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] current client finished AIMING")
												
												//Send event for spectators.
//												STRUCT_DART_SEND sDartEventInfo
//												sDartEventInfo.Details.Type = SCRIPT_EVENT_MG_DART_THROW
//												sDartEventInfo.vOffsetTarget = MPDartGame.darts[serverData.iServerTurn].vOffsetTarget
//												EVENT_SEND_DART_INFO(sDartEventInfo)
												
												EVENT_SEND_DART_INFO_THROW(MPDartGame.darts[serverData.iServerTurn].vOffsetTarget)
												
												IF ServerData.DartsTimer.bTimeRunOut
												AND NOT bShowShotClockThrowHelp
													bShowShotClockThrowHelp = TRUE
												ENDIF
												
												DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
												
												// copy dart's throw info to the client so that other player can grab it
												CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] Copy the throwers data for dart ", 0)
												COPY_DART_TO_CLIENT(playerData[iSelfID], MPDartGame.Darts[serverData.iServerTurn])
												
												IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
													DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												ENDIF
												
												IF MPGlobals.DartsData.iDangerPedCount <= 0
												OR NOT IS_BIT_SET(MPGlobals.DartsData.iDisplayState, 2)
													DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
													CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] no peds in danger zone, clearing danger flag")
												ELSE
													DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
													CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] setting danger flag, peds in danger zone: ", MPGlobals.DartsData.iDangerPedCount)
												ENDIF
												
												//playerData[iSelfID].bDartThrown = TRUE
												TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN, TRUE)
													//sequence palyer to throw a dart and then after loop the standing pose animation.	
													CLEAR_SEQUENCE_TASK(siTemp)
													OPEN_SEQUENCE_TASK(siTemp)
													TASK_PLAY_ANIM(NULL,"anim@amb@clubhouse@mini@darts@", "throw_overlay", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
													TASK_PLAY_ANIM(NULL, "anim@amb@clubhouse@mini@darts@", "throw_idle_a_down", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
													CLOSE_SEQUENCE_TASK(siTemp)
													TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siTemp)

												SWITCH_MPTHROW_STATE(playerData[iSelfID].eMPThrowStage, DARTS_MPTHROW_THROW)
											ENDIF
										ENDIF
									BREAK
									
									CASE DARTS_MPTHROW_THROW
										DEBUG_MESSAGE_PERIODIC("[AM_DARTS] current client is THROWING", iDebugThrottle)
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
											IF THROW_DART(MPDartGame.Darts[serverData.iServerTurn], MPDartGame.dBoard)
											
												
												CDEBUG1LN(DEBUG_DARTS, "Regular Throw completed, Event Sent")
												//playerData[iSelfID].bResetDone = FALSE
												TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE, FALSE)
												SWITCH_MPTHROW_STATE(playerData[iSelfID].eMPThrowStage, DARTS_MPTHROW_SCORE)
											ENDIF
										ELSE
											IF NOT IS_TIMER_STARTED(ReThrowTimer)
												IF THROW_DART_AT_ENTITY(MPDartGame.darts[ServerData.iServerTurn], MPDartGame.dBoard, << 988.1219, -99.3023, 73.8456 >>)
													BROADCAST_DANGER_ZONE_UPDATE(PLAYER_ID(), DARTS_DANGERZONE_DART_THROW)
													CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed")
													
													//playerData[iSelfID].bDartThrown = FALSE
													//playerData[iSelfID].bResetDone = FALSE
													TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN, FALSE)
													TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE, FALSE)
													
													RESTART_TIMER_NOW(ReThrowTimer)
												ENDIF
											
											ELIF GET_TIMER_IN_SECONDS_SAFE(ReThrowTimer) > 1.0
											OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												
												IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
													DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												ENDIF
												
												CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed, going back to DARTS_MPTHROW_AIM - THROW SIDE")
												CANCEL_TIMER(ReThrowTimer)
												SWITCH_MPTHROW_STATE(playerData[iSelfID].eMPThrowStage, DARTS_MPTHROW_AIM)
											ENDIF
										ENDIF
									BREAK
									
									CASE DARTS_MPTHROW_SCORE
										//IF PlayerData[iOtherID].eMPWaitStage >= DARTS_MPWAIT_SCORE
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is SCORING")
											
											SCORE_DART(MPDartGame.darts[serverData.iServerTurn], MPDartGame.dBoard)
											playerData[iSelfID].ClientDart.iHitValue = MPDartGame.Darts[ServerData.iServerTurn].iHitValue
										playerData[iSelfID].ClientDart.iHitMultiplier = MPDartGame.Darts[ServerData.iServerTurn].iHitMultiplier
											playerData[iSelfID].ClientDart.fLength = MPDartGame.darts[serverData.iServerTurn].fLength
											
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] THROW - Client Dart ", ServerData.iServerTurn, " fLength = ", playerData[iSelfID].ClientDart.fLength)
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] THROW - Local Dart ", ServerData.iServerTurn, " fLength = ", MPDartGame.darts[ServerData.iServerTurn].fLength)
											SWITCH_MPTHROW_STATE(playerData[iSelfID].eMPThrowStage, DARTS_MPTHROW_TURN_CHANGE)
										//ENDIF
										DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to finish throwing", iDebugThrottle)
									BREAK
									
									CASE DARTS_MPTHROW_TURN_CHANGE	
										IF PlayerData[iOtherID].eMPWaitStage >= DARTS_MPWAIT_TURN_CHANGE
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is THROWING - TURN CHANGE throwoff", playerData[iDartPlayerID[iSelfDartID]].ClientDart.bDoneScoring)
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] self:", iSelfID, " mem", iDartPlayerID[iSelfDartID], "scoring Done")
											//playerData[iSelfID].bDartThrown = FALSE
											TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN, FALSE)
											DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
											IF NOT MPDartGame.darts[serverData.iServerTurn].bDoneScoring
												MPDartGame.darts[serverData.iServerTurn].bDoneScoring = TRUE
												playerData[iSelfID].ClientDart.bDoneScoring = TRUE
												
//												STRUCT_DART_SEND sDartEventInfo
//												sDartEventInfo.Details.Type = SCRIPT_EVENT_MG_DART_RESET
//												EVENT_SEND_DART_INFO(sDartEventInfo)
												
												EVENT_SEND_DART_INFO_RESET()
												
												CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] Client score copied in progress = ", playerData[iSelfID].ClientDart.bDoneScoring)
											ENDIF
											SETTIMERA(0)
										ENDIF
										DEBUG_MESSAGE_PERIODIC("Waiting for other client to get to DARTS_MPWAIT_TURN_CHANGE", iDebugThrottle)
									BREAK
								ENDSWITCH
							BREAK
							
							CASE DARTS_MPTURN_WAITING
								SWITCH playerData[iSelfID].eMPWaitStage
									CASE DARTS_MPWAIT_AIM
										#IF IS_DEBUG_BUILD
											PLAYER_INDEX piOpponent
											piOpponent = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iOpponentDartID]))
												
											//IF playerData[iOtherID].bDartThrown
											IF GET_GAME_TIMER() % 1000 > 50
												CDEBUG1LN(DEBUG_DARTS, "Waiting For opponent to throw participantID:", iDartPlayerID[iOpponentDartID], 
												" OpponentId",iOpponentDartID,
												" Name:", GET_PLAYER_NAME(piOpponent) )	
											ENDIF
										#ENDIF
										
										IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
											DARTS_DRAW_THROW_OFF_CLOCK(ServerData.DartsTimer, FALSE)
										ENDIF
										
										IF NOT bWaitUIShow
											bWaitUIShow = TRUE
										ENDIF
										
										IF GET_PLAYER_STATE(playerData[iDartPlayerID[iOpponentDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN)
											CDEBUG1LN(DEBUG_DARTS, "Current client finished WAIT - AIMING")
											
											DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
											
											// grab other players dart throw info so we can throw that thang
											COPY_DART_TO_LOCAL(MPDartGame.darts[serverData.iServerTurn], playerData[iOtherID], iOtherID)
											
											IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											ENDIF
											
											playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_THROW
										ELSE
											DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to throw", iDebugThrottle)
										ENDIF
									BREAK
									
									CASE DARTS_MPWAIT_THROW		
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - Throwing ", iOtherID)
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_DANGER_DART)
											IF THROW_DART(MPDartGame.darts[serverData.iServerTurn], MPDartGame.dBoard)
												//BROADCAST_DART_THROW()
												CDEBUG1LN(DEBUG_DARTS, "Regular Throw Completed - Wait side")
												//playerData[iSelfID].bResetDone = FALSE
												TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE, FALSE)
												playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_SCORE
											ENDIF
										ELSE
											IF NOT IS_TIMER_STARTED(ReThrowTimer)
												IF THROW_DART_AT_ENTITY(MPDartGame.darts[ServerData.iServerTurn], MPDartGame.dBoard, << 988.1219, -99.3023, 73.8456 >>)
													CDEBUG1LN(DEBUG_DARTS, "Danger Dart Completed - WAIT SIDE")
													RESTART_TIMER_NOW(ReThrowTimer)
												ENDIF
											
											ELIF GET_TIMER_IN_SECONDS(ReThrowTimer) > 1.0
											OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												
												IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
													DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												ENDIF
												
												CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed, going back to DARTS_MPTHROW_AIM - WAIT SIDE")
												CANCEL_TIMER(RethrowTimer)
												playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_AIM
											ENDIF
										ENDIF
									BREAK
									
									CASE DARTS_MPWAIT_SCORE
										IF PlayerData[iOtherID].eMPThrowStage >= DARTS_MPTHROW_SCORE
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - SCORING")
											SCORE_DART(MPDartGame.darts[serverData.iServerTurn], MPDartGame.dBoard)
											playerData[iSelfID].ClientDart.fLength = MPDartGame.darts[serverData.iServerTurn].fLength
											playerData[iSelfID].ClientDart.iHitValue = MPDartGame.Darts[ServerData.iServerTurn].iHitValue
											playerData[iSelfID].ClientDart.iHitMultiplier = MPDartGame.Darts[ServerData.iServerTurn].iHitMultiplier
										
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] WAIT - Client Dart ", ServerData.iServerTurn, " fLength = ", playerData[iSelfID].ClientDart.fLength)
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] WAIT - Local Dart ", ServerData.iServerTurn, " fLength = ", MPDartGame.Darts[serverData.iServerTurn].fLength)
											playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_TURN_CHANGE
										ENDIF
										DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to finish throwing", iDebugThrottle)
									BREAK
									
									CASE DARTS_MPWAIT_TURN_CHANGE
										IF PlayerData[iOtherID].eMPThrowStage >= DARTS_MPTHROW_TURN_CHANGE
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - TURN CHANGE scoring Done")
											
											DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
											
											IF NOT MPDartGame.Darts[serverData.iServerTurn].bDoneScoring
												MPDartGame.Darts[serverData.iServerTurn].bDoneScoring = TRUE
											ENDIF
											SETTIMERA(0)
										ENDIF
										DEBUG_MESSAGE_PERIODIC("Waiting for other client to get to DARTS_MPTHROW_TURN_CHANGE", iDebugThrottle)
									BREAK
								ENDSWITCH
							BREAK
							
							CASE DARTS_MPTURN_SCORE_CHECK
								
								IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED)
									IF iSelfID = serverData.iThrower
										// print something?
									ELSE
										// print something else?
									ENDIF
									DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, TRUE)
								ENDIF
								
								//IF NOT playerData[iSelfID].bResetDone 
								IF NOT GET_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
								AND ( TIMERA() > 1000 )
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is in SCORE CHECK")
									playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_AIM
									playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_AIM
									MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring = FALSE
									DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
									
									CLEAR_HELP()
									
									IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
										
										// sending the darts back to the player, resetting them
										// serves the same functioon as RESET_DART without deleting the object
										INT y
										REPEAT NUM_MP_DART_TURNS y
											//RESET_DART(MPDartGame.Darts[y])
											MPDartGame.Darts[y].bDoneScoring = FALSE
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", y, " was reset")
										ENDREPEAT
										
										IF iSelfDartID != serverData.iThrower
											bWaitUIShow = FALSE
										ENDIF
										
										//SEND EVENT TO SPECATTORS TO UPDATE SCOREBOARD, only thrower should send the info.
										IF ServerData.iThrower = iSelfDartID
											EVENT_SEND_DART_SET_FINISHED(ServerData.iThrower, -1)
										ENDIF
										
										IF serverData.iWaiter = iSelfDartID
											UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
										ELSE
											UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_AWAY))
										ENDIF
										
										iSteadyShotsUsed = 0
									ENDIF
									
									//playerData[iSelfID].bResetDone = TRUE
									TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE, TRUE)
								ENDIF
							BREAK
							
							CASE DARTS_MPTURN_WINNING
								
								IF NOT IS_TIMER_STARTED(controlTimer)
									
									CDEBUG1LN(DEBUG_DARTS, "********** serverData.iWinner = ", serverData.iWinner)
									CLEAR_PRINTS()
									CLEAR_HELP()
									
									IF (serverData.iWinner = iSelfDartID)
										
										sShardPlayerName = "<C>"
										sShardPlayerName += "~HUD_COLOUR_BLUE~"
										sShardPlayerName += sDartsPlayerNames[iSelfDartID]
										sShardPlayerName += "</C>"
										sShardPlayerName += "~s~"
										
										SET_SHARD_BIG_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(shardBigMessage, "DARTS_THRW_WIN", "DARTS_FIRST_DT", sShardPlayerName, 3000, SHARD_MESSAGE_CENTERED, HUD_COLOUR_WHITE)
										
										//SET_SCALEFORM_BIG_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(DartsUI.siBigMessage, "DARTS_THRW_WIN", "DARTS_FIRST_DT", sDartsPlayerNames[PlayerData[iSelfID].iOffsetPlayerID], 5000, 
										//														default, default, TRUE, MG_BIG_MESSAGE_ANIM_TIME_SLOW)
										PLAY_SOUND_FRONTEND(-1, "WINNER", "CELEBRATION_SOUNDSET")
									ELSE
										
										sShardPlayerName = "<C>"
										sShardPlayerName += "~HUD_COLOUR_RED~"
										sShardPlayerName += sDartsPlayerNames[iOpponentDartID]
										sShardPlayerName += "</C>"
										sShardPlayerName += "~s~"
										
										SET_SHARD_BIG_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(shardBigMessage, "DARTS_THRW_LOS", "DARTS_FIRST_DT", sShardPlayerName, 3000, SHARD_MESSAGE_CENTERED, HUD_COLOUR_RED)
										
										//SET_SCALEFORM_BIG_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(DartsUI.siBigMessage, "DARTS_THRW_LOS", "DARTS_FIRST_DT", sDartsPlayerNames[PlayerData[iOtherID].iOffsetPlayerID], 5000, 
										//														HUD_COLOUR_RED, default, TRUE, MG_BIG_MESSAGE_ANIM_TIME_SLOW)
										PLAY_SOUND_FRONTEND(-1, "LOSER", "CELEBRATION_SOUNDSET")
									ENDIF
									
									
									
									SET_CAM_ACTIVE(camDartGame, TRUE)
									START_TIMER_NOW(controlTimer)
									
								ELIF GET_TIMER_IN_SECONDS_SAFE(controlTimer) >= 5.0
								//OR NOT UPDATE_SCALEFORM_BIG_MESSAGE(DartsUI.siBigMessage, TRUE)
								OR NOT UPDATE_SHARD_BIG_MESSAGE(shardBigMessage, TRUE)
									
									INT y
									REPEAT NUM_MP_DART_TURNS y
										RESET_DART(MPDartGame.Darts[y])
										MPDartGame.Darts[y].bDoneScoring = FALSE
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", y, " was reset")
									ENDREPEAT
									
									CANCEL_TIMER(controlTimer)
									
									RESET_SHARD_BIG_MESSAGE(shardBigMessage)
									
									DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_AT_THROWOFF)
									//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_SYNC_OBJECTS
									SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SYNC_OBJECTS)
								ENDIF
								
	//							IF NOT MPGlobals.DartsData.bResultsDisplayed
	//								MPGlobals.DartsData.bResultsDisplayed = TRUE
	//							ENDIF
	//							
	//							MPDartGame.bDartWinner = (serverData.iWinner = iSelfID)
								
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_SYNC_OBJECTS
					//CDEBUG1LN(DEBUG_DARTS, "Client DARTS_MPSTATE_SYNC_OBJECTS")
					i = 0
					j = 0
					REPEAT NUM_MP_DART_PLAYERS i
						MPDartGame.iThrowsTaken[i] 	= 0
						MPDartGame.iBullsEyesHit[i]	= 0
						DARTS_CLEAR_SCORES(DartsSB, i)
					ENDREPEAT
					
					DARTS_ADD_SCORE(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME), 301)
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						DARTS_ADD_SCORE(DartsSB, ENUM_TO_INT(DART_PLAYER_AWAY), 301)
					ENDIF
					
					REPEAT  NUM_MP_DART_TURNS j
						RESET_DART(MPDartGame.Darts[j])
						MPDartGame.Darts[j].bDoneScoring = FALSE
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", j, " was reset")
					ENDREPEAT
					
					RESET_CLIENT_DART(PlayerData[iSelfID])
					
					DARTS_INIT_TIMERS(localDartsTimer)
					
					SWITCH_MPTHROW_STATE(playerData[iSelfID].eMPThrowStage, DARTS_MPTHROW_AIM)
					playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_AIM
					//playerData[iSelfID].bDartThrown = FALSE
					CDEBUG1LN(DEBUG_DARTS, "selfID", iSelfID)
					CDEBUG1LN(DEBUG_DARTS, "ParticipantID", iDartPlayerID[iSelfDartID])
					TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN, FALSE)
					
					MPDartGame.bEndGameMessageDisplayed = FALSE
					MPDartGame.bWinNoise = FALSE
					
					bInWinRange = FALSE
					
//					CLEAR_AREA(Args.vDartBoard, 0.5, TRUE)
//					REMOVE_DECALS_IN_RANGE(Args.vDartBoard, 0.5)
					iSteadyShotsUsed = 0
					
					bSteadyShotHelp = FALSE
					
					DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_FIRST_THROW, FALSE)
					
					SET_ON_JOB_INTRO(FALSE)
					
					DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SYNC_COMPLETE)
					SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SETUP_POST)
				BREAK
				
				CASE DARTS_MPSTATE_SETUP_POST //14
					
					//@@@ force display quit controlls.
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					AND NOT IS_TRANSITION_ACTIVE()
						DARTS_MP_CONTROLS_UI(DartsUI, FALSE)
					ENDIF
							
					IF ServerData.mpState = DARTS_MPSTATE_IN_PROGRESS
						TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH, FALSE)

						INTRO_SCENE_CLEANUP(sLocalIntroScene)
						DISABLE_ALL_MP_HUD()
						DISPLAY_RADAR(FALSE)
						DARTS_GAME_CAM()
						SAFE_BIT_TOGGLE(iLocalStateFlags, ENUM_TO_INT(eLP_DRAW_SCORE_BOARD))
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						
						IF SHOULD_DARTS_LEVEL_HELP_SHOW(GET_MP_INT_CHARACTER_STAT(MP_STAT_CRDARTS), iLevelBit)
							CDEBUG1LN(DEBUG_DARTS, "show the level up prompt")
							DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOW_LEVEL_UP, TRUE)
						ELSE
							CDEBUG1LN(DEBUG_DARTS, "no level up prompt to show")
						ENDIF
						
						fScaledWobble = GET_MP_WOBBLE_LEVEL(GET_MP_INT_CHARACTER_STAT(MP_STAT_CRDARTS)) //0.076
						fScaledRealWobble = GET_MP_REAL_WOBBLE_LEVEL(GET_MP_INT_CHARACTER_STAT(MP_STAT_CRDARTS)) //0.1015
						
						CDEBUG1LN(DEBUG_DARTS, "total darts games  = ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CRDARTS))
						CDEBUG1LN(DEBUG_DARTS, "scaled wobble      = ", fScaledWobble)
						CDEBUG1LN(DEBUG_DARTS, "scaled real wobble = ", fScaledRealWobble)
						
						fWobbleFactor = fScaledWobble
						fRealWobFactor = fScaledRealWobble
						
						DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SYNC_COMPLETE)

						
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							DartsStats[DARTS_SCLB_STAT_NUM_MATCHES]++
						ENDIF
						
						DARTS_INIT_UI(DartsUI)
	
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							UPDATE_HIGHLIGHT(DartsSB, PICK_INT((serverData.iCoinFlip = iSelfDartID), ENUM_TO_INT(DART_PLAYER_HOME), ENUM_TO_INT(DART_PLAYER_AWAY)))
						ELSE
							UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
						ENDIF
						
						bWaitUIShow = FALSE
						
						CLEAR_BIT_ENUM(iLocalStateFlags, eLP_SHOW_BUSYSPINNER)
						CANCEL_TIMER(stDesync)
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_IN_PROGRESS)
					ELSE
					
						IF NOT IS_TIMER_STARTED(stDesync)
							START_TIMER_NOW(stDesync)	
							CDEBUG1LN(DEBUG_DARTS,"DARTS_MPSTATE_SETUP_POST - Started Desync Timer")
						ELIF GET_TIMER_IN_SECONDS(stDesync) >= 25.0
							//player left.
							CDEBUG1LN(DEBUG_DARTS,"DARTS_MPSTATE_SETUP_POST - waited for 25, sync did not complete")
							CLEAR_BIT_ENUM(iLocalStateFlags, eLP_SHOW_BUSYSPINNER)
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB,  PlayerData, PlayerDarts,MPDartGame, DartsUI)
							
						ELIF (GET_TIMER_IN_SECONDS(stDesync) >= 12.0 AND GET_TIMER_IN_SECONDS(stDesync) < 25.0)//start spinner
							IF NOT IS_BIT_SET_ENUM(iLocalStateFlags, eLP_SHOW_BUSYSPINNER)
								CDEBUG1LN(DEBUG_DARTS,"DARTS_MPSTATE_SETUP_POST - Started busy spinner")
								SET_BIT_ENUM(iLocalStateFlags, eLP_SHOW_BUSYSPINNER)
							ENDIF
						ENDIF
						
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_WAIT_POST_MENU
					// Sitting in a waiting state
					IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_MOVETOGAME)
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_IN_PROGRESS)
					ENDIF
				BREAK
				
				// #REALGAME HERE VVVVVVV
				//######################################################################################
				//#################################### GAME LOOP - SinglePlayer .... ##################
				CASE DARTS_MPSTATE_IN_PROGRESS
					
					// failsafe for screen not fading back in
					IF IS_SCREEN_FADED_OUT()
					AND NOT IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
	//				IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_FIRST_THROW)
	//					PRINT_WITH_NAME("DARTS_FIRST_DT", sDartsPlayerNames[PICK_INT((serverData.mpTurnStatus[iSelfID]=DARTS_MPTURN_THROWING), iSelfID, iOtherID)], DEFAULT_GOD_TEXT_TIME, 0)
	//					DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_FIRST_THROW, TRUE)
	//				ENDIF
					
	//				HANDLE_SWITCH_CAM()
					
					// making sure the dart players stay in position
					SWITCH DartPlayerPosition
						CASE DART_PLAYER_IN_POSITION
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							AND GET_PLAYER_DISTANCE_FROM_LOCATION(DartsPlayerPos.vDartPlayrPos) > 1.5
								
//							#IF IS_DEBUG_BUILD
//								CDEBUG1LN(DEBUG_DARTS, "GET_PLAYER_DISTANCE_FROM_LOCATION(DartsPlayerPos.vDartPlayrPos) = ", GET_PLAYER_DISTANCE_FROM_LOCATION(DartsPlayerPos.vDartPlayrPos))
//							#ENDIF
//								
//								CLEAR_SEQUENCE_TASK(siTemp)
//								OPEN_SEQUENCE_TASK(siTemp)
//									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, DartsPlayerPos.vDartPlayrPos, PEDMOVEBLENDRATIO_WALK, 5000, 0.3)
//									TASK_ACHIEVE_HEADING(NULL, MPDartGame.dBoard.fBoardHeading)
//								CLOSE_SEQUENCE_TASK(siTemp)
//								TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siTemp)
//								
//								CDEBUG1LN(DEBUG_DARTS, "Dart player out of position, tasked to move back")
//								
//								DartPlayerPosition = DART_PLAYER_OUT_POSITION
							ENDIF
						BREAK
						
						CASE DART_PLAYER_OUT_POSITION
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
								
//								CDEBUG1LN(DEBUG_DARTS, "Dart player is now back in position")
								
								DartPlayerPosition = DART_PLAYER_IN_POSITION
							ENDIF
						BREAK
					ENDSWITCH
					
	//				IF DARTS_PLAYER_QUIT_CONTROL(PlayerData[iSelfID], DartsUI, iDebugThrottle)
	//					PlayerData[iSelfID].eMPState = DARTS_MPSTATE_END
	//				ENDIF
					
					// Drawing the hud and the control scheme
					IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
						
						// help text drawing
						IF NOT (serverData.mpTurnStatus[iSelfDartID] = DARTS_MPTURN_WINNING)
						
						//AND ( NOT playerData[iSelfID].bDartThrown = TRUE
						//	OR NOT playerData[iOtherID].bDartThrown = TRUE )
							
						AND ( NOT GET_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN)
							OR NOT GET_PLAYER_STATE(playerData[iDartPlayerID[iOpponentDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN))
							
						AND NOT DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
									
							IF NOT bUITextDisplayed
								
								IF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_END_HELP_READY)
								AND serverData.mpTurnStatus[iSelfDartID] = DARTS_MPTURN_THROWING
									IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_WIN_HELP)
										IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gWIN_HELP_SHOWN)
											PRINT_HELP("DARTS_INSTR_W")
											SET_BIT(g_iDartsTutorialFlags, gWIN_HELP_SHOWN)
										ENDIF
										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_WIN_HELP, TRUE)
									ELIF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP)
									AND DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP_READY)
										IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gBUST_HELP_SHOWN)
											PRINT_HELP("DARTS_INSTR_B")
											SET_BIT(g_iDartsTutorialFlags, gBUST_HELP_SHOWN)
										ENDIF
										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP, TRUE)
									ENDIF
								ENDIF
								
								IF bInWinRange
								AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_SHT_USE")
								AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_FST_HLP")
								AND serverData.mpTurnStatus[iSelfDartID] = DARTS_MPTURN_THROWING
									
	//								IF (serverData.iScores[iSelfID] % 2) = 0
	//								AND (serverData.iScores[iSelfID] = 50 OR serverData.iScores[iSelfID] < 41)
	//								
	//									IF serverData.iScores[iSelfID] = 50
	//									ELSE
	//										CLEAR_HELP()
	//									ENDIF
	//								ENDIF
								
								ELIF NOT (serverData.mpTurnStatus[iSelfDartID] = DARTS_MPTURN_SCORE_CHECK)
									//DEBUG_MESSAGE_PERIODIC("DARTS UI IS SHOWING REGULAR HELP", iDebugThrottle)
									IF bShowShotClockThrowHelp //ServerData.DartsTimer.bTimeRunOut
									AND NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLOCK_HELP)
									AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_W")
									AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_B")
									AND (IS_TIMER_STARTED(ServerData.DartsTimer.stShotClock) AND GET_TIMER_IN_SECONDS_SAFE(ServerData.DartsTimer.stShotClock) < 5)
										IF NOT IS_BIT_SET(g_iDartsTutorialFlags, gSHOT_CLOCK_HELP_SHOWN)
											PRINT_HELP("DARTS_TIOT")
											SET_BIT(g_iDartsTutorialFlags, gSHOT_CLOCK_HELP_SHOWN)
											CDEBUG1LN(DEBUG_DARTS, "shot clock help bit set here")
										ELSE
											CDEBUG1LN(DEBUG_DARTS, "shot clock help bit already set")
										ENDIF
										CDEBUG1LN(DEBUG_DARTS, "shot clock ui flag set")
										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOT_CLOCK_HELP, TRUE)
									ENDIF
								ENDIF
								DartsUI.bInstructTextTransIn = TRUE
							ELSE
								IF DartsUI.bInstructTextTransIn
									CLEAR_HELP()
									DartsUI.bInstructTextTransIn = FALSE
								ENDIF
							ENDIF
						ENDIF
						
						// Drawing the Darts Left UI element and the instructional buttons
						IF ((playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_AIM) 
							AND (serverData.mpTurnStatus[iSelfDartID] = DARTS_MPTURN_THROWING))
						OR bWaitUIShow
							IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
							AND NOT IS_TRANSITION_ACTIVE()
								DARTS_MP_CONTROLS_UI(DartsUI, (serverData.mpTurnStatus[iSelfDartID] = DARTS_MPTURN_THROWING))
							ENDIF
						ENDIF
						
						IF NOT IS_PAUSE_MENU_ACTIVE()
							DARTS_DISPLAY_SPECIAL_SPLASH_MP_SHARD(DartsUI)
							IF bPlaySplashFX
								PLAY_SOUND_FRONTEND ( -1, "GOLF_NEW_RECORD", "HUD_AWARDS")
								bPlaySplashFX = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					// Do everyting playing related right chea
					SWITCH serverData.mpTurnStatus[iSelfDartID] 
						CASE DARTS_MPTURN_THROWING
							SWITCH playerData[iSelfID].eMPThrowStage
								CASE DARTS_MPTHROW_AIM
									//playerData[iSelfID].bDartAim = TRUE
									TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_AIM, TRUE)
	//								playerData[iSelfID].vReticlePos.x = spriteReticle.x
	//								playerData[iSelfID].vReticlePos.y = spriteReticle.y
									
									IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
										//DARTS_DRAW_PLAYERS_RETICLE(playerData[iSelfID].vReticlePos, iSelfID)
										DARTS_DRAW_SHOT_CLOCK(ServerData.DartsTimer, TRUE)
										//DRAW_DARTS_RETICLE_GUIDE(iSelfID, iSelfID)
									ENDIF
									
									DARTS_GAME_HELP(ServerData.iServerTurn, TRUE)
									
									IF DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOW_LEVEL_UP)
										IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_SHT_USE")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_CLOCK")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_W")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_INSTR_B")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_AIM_HLP")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_STD_HLP")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DARTS_FST_HLP")
											PRINT_HELP("DARTS_LEVEL")
											SET_BIT(g_savedGlobals.sDartsData.iDartTimePlayed, iLevelBit)
											DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_SHOW_LEVEL_UP, FALSE)
										ENDIF
									ENDIF
									
									IF ServerData.DartsTimer.bNewShot
										CDEBUG1LN(DEBUG_DARTS, "Server timer has sensed a new shot!")
									ENDIF
									
									IF ServerData.DartsTimer.bTimeRunOut
										CDEBUG1LN(DEBUG_DARTS, "Server timer has sensed that it has run out!")
									ENDIF
									
									IF (NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
										AND NOT IS_PAUSE_MENU_ACTIVE()
										AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
										AND NOT IS_TRANSITION_ACTIVE())
									OR ServerData.DartsTimer.bTimeRunOut
										IF DO_AIMING(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard,
													DartsInitReticlePos, FALSE, ServerData.DartsTimer.bTimeRunOut)
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] current client finished AIMING")
											
//											STRUCT_DART_SEND sDartEventInfo
//											sDartEventInfo.Details.Type = SCRIPT_EVENT_MG_DART_THROW
//											sDartEventInfo.vOffsetTarget = MPDartGame.Darts[serverData.iServerTurn].vOffsetTarget
//											EVENT_SEND_DART_INFO(sDartEventInfo)
											
											EVENT_SEND_DART_INFO_THROW(MPDartGame.darts[serverData.iServerTurn].vOffsetTarget)
											
											IF ServerData.DartsTimer.bTimeRunOut
											AND NOT bShowShotClockThrowHelp
												bShowShotClockThrowHelp = TRUE
											ENDIF
											
											DartsUI.iDartsLeft--
											
											DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
											
											// copy dart's throw info to the client so that other player can grab it
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] Copy the throwers data for dart ", ServerData.iServerTurn)
											COPY_DART_TO_CLIENT(playerData[iSelfID], MPDartGame.Darts[ServerData.iServerTurn])
											
											IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											ENDIF
											
											IF MPGlobals.DartsData.iDangerPedCount <= 0
											OR NOT IS_BIT_SET(MPGlobals.DartsData.iDisplayState, 2)
												DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
												CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] no peds in danger zone, clearing danger flag")
											ELSE
												DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
												CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS->DARTS_MPTHROW_AIM] setting danger flag, peds in danger zone: ", MPGlobals.DartsData.iDangerPedCount)
											ENDIF
											
											IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
												DartsStats[DARTS_SCLB_STAT_DARTS_THROWN]++
											ENDIF
											
											//playerData[iSelfID].bDartThrown = TRUE
										//	playerData[iSelfID].bDartAim = FALSE
											TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN, TRUE)
											TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_AIM, FALSE)
												//sequence palyer to throw a dart and then after loop the standing pose animation.	
												CLEAR_SEQUENCE_TASK(siTemp)
												OPEN_SEQUENCE_TASK(siTemp)
												TASK_PLAY_ANIM(NULL,"anim@amb@clubhouse@mini@darts@", "throw_overlay", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
												TASK_PLAY_ANIM(NULL, "anim@amb@clubhouse@mini@darts@", "throw_idle_a_down", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
												CLOSE_SEQUENCE_TASK(siTemp)
												TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siTemp)
													
											SWITCH_MPTHROW_STATE(playerData[iSelfID].eMPThrowStage, DARTS_MPTHROW_THROW)
										ENDIF
									ENDIF
								BREAK
								
								CASE DARTS_MPTHROW_THROW
									DEBUG_MESSAGE_PERIODIC("[AM_DARTS] current client is THROWING", iDebugThrottle)
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_DANGER_DART)
										IF THROW_DART(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard)
											
											CDEBUG1LN(DEBUG_DARTS, "Regular Throw completed, event sent")
											MPDartGame.iThrowsTaken[iSelfDartID]++ //<-
											//playerData[iSelfID].bResetDone = FALSE
											TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE, FALSE)
											SWITCH_MPTHROW_STATE(playerData[iSelfID].eMPThrowStage, DARTS_MPTHROW_SCORE)
										ENDIF
									ELSE
										IF NOT IS_TIMER_STARTED(ReThrowTimer)
											IF THROW_DART_AT_ENTITY(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard, << 988.1219, -99.3023, 73.8456 >>)
												BROADCAST_DANGER_ZONE_UPDATE(PLAYER_ID(), DARTS_DANGERZONE_DART_THROW)
												CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed")
												
												//playerData[iSelfID].bDartThrown = FALSE
												//playerData[iSelfID].bDartAim = TRUE
												//playerData[iSelfID].bResetDone = FALSE
												
												TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN, FALSE)
												TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_AIM, FALSE)
												TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE, FALSE)
												
												RESTART_TIMER_NOW(ReThrowTimer)
											ENDIF
										
										ELIF GET_TIMER_IN_SECONDS(ReThrowTimer) > 1.0
										OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											
											IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											ENDIF
											
											CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed, going back to DARTS_MPTHROW_AIM - THROW SIDE")
											CANCEL_TIMER(ReThrowTimer)
											SWITCH_MPTHROW_STATE(playerData[iSelfID].eMPThrowStage, DARTS_MPTHROW_AIM)
										ENDIF
									ENDIF
								BREAK
								
								CASE DARTS_MPTHROW_SCORE
//									IF PlayerData[iOtherID].eMPWaitStage >= DARTS_MPWAIT_SCORE
//									OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is SCORING")
										
										//playerData[iSelfID].bDartThrown = FALSE
										
										SCORE_DART(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard)
										playerData[iSelfID].ClientDart.iHitValue = MPDartGame.Darts[ServerData.iServerTurn].iHitValue
										playerData[iSelfID].ClientDart.iHitMultiplier = MPDartGame.Darts[ServerData.iServerTurn].iHitMultiplier
										
										IF playerData[iSelfID].ClientDart.iHitValue = 50
											MPDartGame.iBullsEyesHit[iSelfDartID]++
										ENDIF
										
										IF playerData[iSelfID].ClientDart.iHitMultiplier = 2 OR playerData[iSelfID].ClientDart.iHitValue = 50
											//playerData[iSelfID].bDoubleThrow = TRUE
											TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW, TRUE)
										ELSE
											//playerData[iSelfID].bDoubleThrow = FALSE
											TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW, FALSE)
										ENDIF
										
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] THROW - Client Dart ", ServerData.iServerTurn, " hit value = ", playerData[iSelfID].ClientDart.iHitValue)
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] THROW - Local Dart ", ServerData.iServerTurn, " hit value = ", MPDartGame.Darts[ServerData.iServerTurn].iHitValue)
										SWITCH_MPTHROW_STATE(playerData[iSelfID].eMPThrowStage, DARTS_MPTHROW_TURN_CHANGE)
//									ENDIF
									DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to finish throwing", iDebugThrottle)
								BREAK
								
								CASE DARTS_MPTHROW_TURN_CHANGE	
									IF PlayerData[iOtherID].eMPWaitStage >= DARTS_MPWAIT_TURN_CHANGE
									OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
										
										//IF 	DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_SPECS_SCORED)
											//playerData[iSelfID].bDartThrown = FALSE
											TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN, FALSE)
											DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
											
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is THROWING - TURN CHANGE prog", DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE))
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] self:", iSelfID, " mem", iDartPlayerID[iSelfDartID])
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] ServerData.iServerTurn = ", ServerData.iServerTurn)
											#IF IS_DEBUG_BUILD
											PLAYER_INDEX piOne
											piOne = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iSelfDartID]))

				 							CDEBUG1LN(DEBUG_DARTS, 
											" NAME ONE:", GET_PLAYER_NAME(piOne), " SCORING", DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_SCORING_DONE))
											#ENDIF

											
											IF NOT MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring
												MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring = TRUE
												playerData[iSelfID].ClientDart.bDoneScoring = TRUE
												
//												STRUCT_DART_SEND sDartEventInfo
//												sDartEventInfo.Details.Type = SCRIPT_EVENT_MG_DART_RESET
//												EVENT_SEND_DART_INFO(sDartEventInfo)
												
												EVENT_SEND_DART_INFO_RESET()
												
												CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] Client score copied in progress = ", playerData[iSelfID].ClientDart.bDoneScoring)
											ENDIF
											SETTIMERA(0)
										//ENDIF
									ENDIF
									DEBUG_MESSAGE_PERIODIC("Waiting for other client to get to DARTS_MPWAIT_TURN_CHANGE", iDebugThrottle)
								BREAK
							ENDSWITCH
						BREAK
						
						CASE DARTS_MPTURN_WAITING
							SWITCH playerData[iSelfID].eMPWaitStage
								CASE DARTS_MPWAIT_AIM
									
									PRINT_WITH_NAME("DART_A_P_WAIT", sDartsPlayerNames[iOpponentDartID], 20000, 0)
									
									IF NOT bWaitUIShow
										bWaitUIShow = TRUE
									ENDIF
									
									IF NOT DARTS_CHECK_CLIENT_FLAG(playerData[iSelfID], DARTS_CLIENTFLAG_QUIT_ATTEMPTED)
										DARTS_DRAW_SHOT_CLOCK(ServerData.DartsTimer, FALSE)
									ENDIF
									
									//IF playerData[iOtherID].bDartThrown
									IF GET_PLAYER_STATE(playerData[iDartPlayerID[iOpponentDartID]], DARTS_CLIENTFLAG_STATE_DART_THROWN)
										CDEBUG1LN(DEBUG_DARTS, "Current client finished WAIT - AIMING")
										
										DartsUI.iDartsLeft--
										
										DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, FALSE)
										
										// grab other players dart throw info so we can throw that thang
										COPY_DART_TO_LOCAL(MPDartGame.Darts[ServerData.iServerTurn], playerData[iOtherID], iOtherID)
										
										IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
										ENDIF
										
										playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_THROW
									ELSE
										DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to throw", iDebugThrottle)
									ENDIF
								BREAK
								
								CASE DARTS_MPWAIT_THROW		
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - Throwing", iOtherID)
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_DANGER_DART)
										IF THROW_DART(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard)
											//BROADCAST_DART_THROW()
											CDEBUG1LN(DEBUG_DARTS, "Regular Throw Completed - Wait side")
											//MPDartGame.iThrowsTaken[PlayerData[iOtherID].iOffsetPlayerID]++
											MPDartGame.iThrowsTaken[iOpponentDartID]++
											//playerData[iSelfID].bResetDone = FALSE
											TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE, FALSE)
											playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_SCORE
										ENDIF
									ELSE
										IF NOT IS_TIMER_STARTED(ReThrowTimer)
											IF THROW_DART_AT_ENTITY(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard, << 988.1219, -99.3023, 73.8456 >>)
												CDEBUG1LN(DEBUG_DARTS, "Danger Dart Completed - WAIT SIDE")
												RESTART_TIMER_NOW(ReThrowTimer)
											ENDIF
										
										ELIF GET_TIMER_IN_SECONDS(ReThrowTimer) > 1.0
										OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											
											IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
												DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_RETHROW_NEEDED)
											ENDIF
											
											CDEBUG1LN(DEBUG_DARTS, "Danger dart throw completed, going back to DARTS_MPTHROW_AIM - WAIT SIDE")
											CANCEL_TIMER(RethrowTimer)
											playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_AIM
										ENDIF
									ENDIF
								BREAK
								
								CASE DARTS_MPWAIT_SCORE
									IF PlayerData[iOtherID].eMPThrowStage >= DARTS_MPTHROW_SCORE
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - SCORING")
										SCORE_DART(MPDartGame.Darts[ServerData.iServerTurn], MPDartGame.dBoard)
										playerData[iSelfID].ClientDart.iHitValue = MPDartGame.Darts[ServerData.iServerTurn].iHitValue
										playerData[iSelfID].ClientDart.iHitMultiplier = MPDartGame.Darts[ServerData.iServerTurn].iHitMultiplier
										
										IF playerData[iSelfID].ClientDart.iHitValue = 50
											MPDartGame.iBullsEyesHit[iOpponentDartID]++
										ENDIF
										
										IF playerData[iSelfID].ClientDart.iHitMultiplier = 2 OR playerData[iSelfID].ClientDart.iHitValue = 50
											//playerData[iSelfID].bDoubleThrow = TRUE
											TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW, TRUE)
										ELSE
											//playerData[iSelfID].bDoubleThrow = FALSE
											TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW, FALSE)
										ENDIF
										
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] WAIT - Client Dart ", ServerData.iServerTurn, " hit value = ", playerData[iSelfID].ClientDart.iHitValue)
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] WAIT - Local Dart ", ServerData.iServerTurn, " hit value = ", MPDartGame.Darts[ServerData.iServerTurn].iHitValue)
										playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_TURN_CHANGE
									ENDIF
									DEBUG_MESSAGE_PERIODIC("Current client is waiting for other client to finish throwing", iDebugThrottle)
								BREAK
								
								CASE DARTS_MPWAIT_TURN_CHANGE
									IF PlayerData[iOtherID].eMPThrowStage >= DARTS_MPTHROW_TURN_CHANGE
									//AND DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_SPECS_SCORED)
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is WAITING - TURN CHANGE")
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] ServerData.iServerTurn = ", ServerData.iServerTurn)
										CDEBUG1LN(DEBUG_DARTS, 
										"[AM_DARTS] iServerTurn].bDoneScoring = ", MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring,
										" FLAG?", DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE))
										
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] iSelfID ParticipandID = ", iSelfID)
										
										#IF IS_DEBUG_BUILD
										PLAYER_INDEX piOne
											piOne = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iSelfDartID]))
											
										CDEBUG1LN(DEBUG_DARTS, 
		 " NAME ONE:", GET_PLAYER_NAME(piOne), " SCORING", DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_SCORING_DONE))
											#ENDIF
											
										DARTS_SET_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
										
										IF NOT MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring
											MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring = TRUE
										ENDIF
										SETTIMERA(0)
									ENDIF
									DEBUG_MESSAGE_PERIODIC("Waiting for other client to get to DARTS_MPTHROW_TURN_CHANGE", iDebugThrottle)
								BREAK
							ENDSWITCH
						BREAK
						
						CASE DARTS_MPTURN_SCORE_CHECK

							IF ((serverData.iScores[iSelfDartID ] < 41 AND serverData.iScores[iSelfDartID] > 0) AND (serverData.iScores[iSelfDartID] % 2 = 0)) 
							OR serverData.iScores[iSelfDartID] = 50
								IF NOT bInWinRange //DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_IN_WIN_RANGE)
									CDEBUG1LN(DEBUG_DARTS, "DARTS UIFLAG IS SETTING DARTS_UIFLAGS_IN_WIN_RANGE")
									//DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_IN_WIN_RANGE, TRUE)
									bInWinRange = TRUE
								ELSE
									DEBUG_MESSAGE_PERIODIC("DARTS UIFLAG IS DARTS_UIFLAGS_IN_WIN_RANGE IS ALREADY SET", iDebugThrottle)
								ENDIF
							ENDIF
							
							IF serverData.iScores[iSelfDartID] <= 170
							AND NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_END_HELP_READY)
								DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_END_HELP_READY, TRUE)
							ENDIF
							
							IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP_READY)
							AND DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_WIN_HELP)
								DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_BUST_HELP_READY, TRUE)
							ENDIF
							
							IF NOT DARTS_GET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED)
								CLEAR_PRINTS()
								CLEAR_HELP()
								IF iSelfDartID = serverData.iThrower
									IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE)
										CDEBUG1LN(DEBUG_DARTS, "My server score is ", ServerData.iScores[iSelfDartID])
										CDEBUG1LN(DEBUG_DARTS, "Other server score is ", ServerData.iScores[iOpponentDartID])
										PRINT_WITH_2_NUMBERS_NOW("DARTS_REMAIN", 
										ServerData.iLastScore[iSelfDartID] - ServerData.iScores[iSelfDartID], ServerData.iScores[iSelfDartID], 20000, 0)
										//PRINT_WITH_NUMBER("DARTS_REMAINL", ServerData.iScores[iSelfID], 20000, 0)
										
										IF playerData[iSelfID].ClientDart.iHitValue = 50
											DARTS_TRIGGER_SPLASH(DartsUI, DARTSSPLASH_BULLSEYE)
											bPlaySplashFX = TRUE
										ENDIF
										
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO)
										PRINT_WITH_NUMBER("DARTS_BUST_L", ServerData.iTurnscore, 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE)
										PRINT_WITH_NUMBER("DARTS_DOUBLE_L", playerData[iSelfID].ClientDart.iHitValue, 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT)
										PRINT_NOW("DARTS_ONE_PT", 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
										IF playerData[iSelfID].ClientDart.iHitValue = 50
											DARTS_TRIGGER_SPLASH(DartsUI, DARTSSPLASH_BULLSEYE)
											bPlaySplashFX = TRUE
										ELIF ServerData.iTurnscore = 180
											DARTS_TRIGGER_SPLASH(DartsUI, DARTSSPLASH_180)
											bPlaySplashFX = TRUE
										ENDIF
										
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											PRINT_WITH_NAME_AND_NUMBER("DARTS_SWITCH_L", sDartsPlayerNames[iOpponentDartID], ServerData.iScores[iSelfDartID], 5000, 0)
										ELSE
											PRINT_WITH_2_NUMBERS_NOW("DARTS_REMAIN", ServerData.iLastScore[iSelfDartID] - ServerData.iScores[iSelfDartID], ServerData.iScores[iSelfDartID], 5000, 0)
											//PRINT_WITH_NUMBER("DARTS_REMAINL", ServerData.iScores[iSelfID], 5000, 0)
										ENDIF
										
									ENDIF
								ELSE
									IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE)
										CDEBUG1LN(DEBUG_DARTS, "My server score is ", ServerData.iScores[iSelfDartID])
										CDEBUG1LN(DEBUG_DARTS, "Other server score is ", ServerData.iScores[iOpponentDartID])
										PRINT_WITH_2_NUMBERS_NOW("DARTS_REMAIN", ServerData.iLastScore[iOpponentDartID] - ServerData.iScores[iOpponentDartID], ServerData.iScores[iOpponentDartID], 20000, 0)
										//PRINT_WITH_NUMBER("DARTS_REMAINL", ServerData.iScores[iOtherID], 20000, 0)
									
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO)
										PRINT_WITH_NUMBER("DARTS_BUST_L", ServerData.iTurnscore, 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE)
										PRINT_WITH_NUMBER("DARTS_DOUBLE_L", playerData[iSelfID].ClientDart.iHitValue, 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT)
										PRINT_NOW("DARTS_ONE_PT", 5000, 0)
									ELIF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
										PRINT_WITH_NAME_AND_NUMBER("DARTS_SWITCH_L", GET_PLAYER_NAME(PLAYER_ID()), ServerData.iScores[iOpponentDartID], 5000, 0)
										
									ENDIF
								ENDIF
								DARTS_SET_UI_FLAG(DartsUI.iFlags, DARTS_UIFLAGS_UPDATE_PRINTED, TRUE)
							ENDIF
							
							//IF NOT playerData[iSelfID].bResetDone 
							IF NOT GET_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
							AND ( (DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE) AND TIMERA() > 1000)
								OR TIMERA() > 2000 )
								CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client is in SCORE CHECK")
								
								//IF  DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_SPECS_SCORED)
								playerData[iSelfID].eMPWaitStage = DARTS_MPWAIT_AIM
								playerData[iSelfID].eMPThrowStage = DARTS_MPTHROW_AIM
								MPDartGame.Darts[ServerData.iServerTurn].bDoneScoring = FALSE
								DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SCORING_DONE)
								
								IF iSelfDartID = iDartPlayerID[serverData.iThrower]
									IF playerData[iSelfID].ClientDart.iHitValue = 50
										DartsStats[DARTS_SCLB_STAT_BULLS_EYES]++
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] xp given for bullseye")
										iXPToGive = ROUND(BASE_BULLSEYE_XP * g_sMPTunables.fxp_tunable_darts_bullseye)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] after tuning, xp given = ", iXPToGive)
										iBullseyeXP += GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_SKILL, XPCATEGORY_SKILL_DARTS_BULLSEYE, iXPToGive)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iBullseyeXP is now ", iBullseyeXP)
									ENDIF
								ENDIF
								
								IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER) //serverData.bDoSwitchPlayer
									
									DartsUI.iDartsLeft = 3
									
									// sending the darts back to the player, resetting them
									// serves the same functioon as RESET_DART without deleting the object
									INT y
									REPEAT NUM_MP_DART_TURNS y
										RESET_DART(MPDartGame.Darts[y])
										MPDartGame.Darts[y].bDoneScoring = FALSE
										CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]MPDartGame.Darts ", y, " was reset")
									ENDREPEAT
									
									//SEND EVENT TO SPECATTORS TO UPDATE SCOREBOARD, only thrower should send the info.
									IF ServerData.iThrower = iSelfDartID
										EVENT_SEND_DART_SET_FINISHED(ServerData.iThrower, ServerData.iScores[ServerData.iThrower])
									ENDIF
									
									IF ServerData.iLastScore[ServerData.iThrower] <> ServerData.iScores[ServerData.iThrower]
										IF ServerData.iThrower = iSelfDartID
											DARTS_ADD_SCORE(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME), ServerData.iScores[ServerData.iThrower]) //<-
										ELSE
											DARTS_ADD_SCORE(DartsSB, ENUM_TO_INT(DART_PLAYER_AWAY), ServerData.iScores[ServerData.iThrower]) //<-
										ENDIF
										
									ENDIF
									
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]iSelfID = ", iSelfID)
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]serverData.iThrower = ", serverData.iThrower)
									CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]ServerData.iTurnscore = ", ServerData.iTurnscore)
									
									IF iSelfDartID = serverData.iThrower
										IF ServerData.iTurnscore = 180
										AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											DartsStats[DARTS_SCLB_STAT_NUM_180S]++
											CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]Added 180 stat")
										ENDIF
									ELSE
										bWaitUIShow = FALSE
									ENDIF
									
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
										IF serverData.iWaiter = iSelfDartID
											UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_HOME))
										ELSE
											UPDATE_HIGHLIGHT(DartsSB, ENUM_TO_INT(DART_PLAYER_AWAY))
										ENDIF
									ENDIF
									
									iSteadyShotsUsed = 0
								ENDIF
								CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] current client finished reset in SCORE CHECK")
								//playerData[iSelfID].bResetDone = TRUE
								TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE, TRUE)
							//ENDIF
							ENDIF
						BREAK
						
						CASE DARTS_MPTURN_WINNING
							CDEBUG1LN(DEBUG_DARTS, "********** serverData.iWinner = ", serverData.iWinner)
							
							CLEAR_PRINTS()
							
							IF NOT MPGlobals.DartsData.bResultsDisplayed
								MPGlobals.DartsData.bResultsDisplayed = TRUE
							ENDIF
							
							// if you done won
							IF serverData.iWinner = iSelfDartID
								//PRINT_WITH_NUMBER_NOW("DARTS_REMAINL", 0, 5000, 1)
								
								MPDartGame.bDartWinner = TRUE
								
								MPDartGame.iGamesWon++
								
								IF iSelfDartID = serverData.iThrower
									IF playerData[iSelfID].ClientDart.iHitValue = 50
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] experience given for bullseye after a win")
										DartsStats[DARTS_SCLB_STAT_BULLS_EYES]++
										iXPToGive = ROUND(BASE_BULLSEYE_XP * g_sMPTunables.fxp_tunable_darts_bullseye)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] after tuning, xp given = ", iXPToGive)
										iBullseyeXP += GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_SKILL, XPCATEGORY_SKILL_DARTS_BULLSEYE, iXPToGive)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iBullseyeXP is now ", iBullseyeXP)
									ENDIF
								ENDIF
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] experience given for leg win")
									DartsStats[DARTS_SCLB_STAT_NUM_LEGS_WON]++
									iXPGained += ROUND(BASE_LEG_WIN_XP * g_sMPTunables.fxp_tunable_darts_legwon)
									CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_legwon = ", g_sMPTunables.fxp_tunable_darts_legwon)
									//GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DARTS_WIN, 50)
									CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iXPGained is now ", iXPGained)
								ENDIF
								
								IF MPDartGame.iThrowsTaken[iSelfDartID] <= 6
									SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FM6DARTCHKOUT, TRUE)
								ENDIF
								
							// if you lost... loser
							ELSE
								//PRINT_WITH_NUMBER_NOW("DARTS_REMAINL", 0, 5000, 1)
								MPDartGame.bDartWinner = FALSE
								
								MPDartGame.iGamesLost++
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									DartsStats[DARTS_SCLB_STAT_NUM_LEGS_LOST]++
								ENDIF
								
							ENDIF
							
							RESTART_TIMER_NOW(controlTimer) 
							
							IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
								//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_TRANSITION
								SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_GAME_TRANSITION)
							ELSE
								
								CDEBUG1LN(DEBUG_DARTS, "calling trigger pre load for DARTS")
								TRIGGER_CELEBRATION_PRE_LOAD(sCelebrationData, TRUE)
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									IF MPDartGame.bDartWinner
										
										IF serverData.iNumLegs < 5
											INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STRENGTH, 1)
											CPRINTLN(DEBUG_DARTS, "STRENGTH STAT INCREASED by 1 FROM DARTS")
										ELSE
											INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STRENGTH, 2)
											CPRINTLN(DEBUG_DARTS, "STRENGTH STAT INCREASED by 2 FROM DARTS")
										ENDIF
										
										INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS , 1)
										INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_DARTS_TOTAL_WINS, 1)
										DartsStats[DARTS_SCLB_STAT_NUM_WINS]++
										DartsStats[DARTS_SCLB_STAT_NUM_SETS_WON]++
									ELSE
										DartsStats[DARTS_SCLB_STAT_NUM_LOSSES]++
										DartsStats[DARTS_SCLB_STAT_NUM_SETS_LOST]++
									ENDIF
									
									INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_DARTS_TOTAL_MATCHES, 1)
								ENDIF
								
								DartsUI.iOurScore += serverData.iSets[iSelfDartID] // <-
								DartsUI.iTheirScore += serverData.iSets[iOpponentDartID]
								DartsUI.sMyName = sDartsPlayerNames[iSelfDartID]
								DartsUI.sTheirName = sDartsPlayerNames[iOpponentDartID]
								
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									IF MPDartGame.bDartWinner
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] giving darts won XP")
										iXPGained += ROUND(BASE_MATCH_WIN_XP * g_sMPTunables.fxp_tunable_darts_match_win)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_match_win is  ", g_sMPTunables.fxp_tunable_darts_match_win)
										//GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DARTS_WIN, 250)
										CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iXPGained is now ", iXPGained)
										iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
										CDEBUG1LN(DEBUG_DARTS, "money given to me the winner")
									ELSE
										iBetWinnings = BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
										CDEBUG1LN(DEBUG_DARTS, "money given to the other guy, cuz i lost")
									ENDIF
								ENDIF
								
								IF MPDartGame.bDartWinner
									
								ENDIF
								
								IF iBetWinnings > 0
									CDEBUG1LN(DEBUG_DARTS, "iBetWinnings = ", iBetWinnings)
								ELSE
									CDEBUG1LN(DEBUG_DARTS, "no bets were won")
								ENDIF
								
								CDEBUG1LN(DEBUG_DARTS, "g_iMyBetWinnings = ", g_iMyBetWinnings)
								
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] giving darts match finish bonus")
								iXPGained += ROUND(BASE_DART_PLAY_XP * g_sMPTunables.fxp_tunable_darts_taking_part)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_taking_part = ", g_sMPTunables.fxp_tunable_darts_taking_part)
								//GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DARTS_WIN, 100)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iXPGained is now ", iXPGained)
								
								SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PLAY_DARTS)
								CPRINTLN(DEBUG_DARTS, "DAILY OBJECTIVE COMPLETE FOR DARTS")
								
								//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_GAME_END
								SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_GAME_END)
							ENDIF
						BREAK
					ENDSWITCH			
				BREAK
				
				CASE DARTS_MPSTATE_GAME_TRANSITION
				
					SWITCH iSplashStage
						CASE 0
							IF GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 0.75
								CLEAR_HELP()
								CLEAR_PRINTS()
								
								//IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
									UPDATE_SB_GAMES(DartsSB.siScoreBoard, bSinglePlayerGame, bIsHost, serverData.iSets[iOpponentDartID], serverData.iSets[iSelfDartID])
								//ELSE
								//	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
								//		UPDATE_SB_GAMES(DartsSB.siScoreBoard, bSinglePlayerGame, serverData.iSets[iOpponentDartID], serverData.iSets[iSelfDartID])
								//	ELSE
								//		UPDATE_SB_GAMES(DartsSB.siScoreBoard, bSinglePlayerGame, serverData.iSets[iSelfDartID], serverData.iSets[iOpponentDartID])
								//	ENDIF
							//	ENDIF
								
								//big message here
								IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_SET_JUST_WON)
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
										IF MPDartGame.bDartWinner
											DartsStats[DARTS_SCLB_STAT_NUM_SETS_WON]++
										ELSE
											DartsStats[DARTS_SCLB_STAT_NUM_SETS_LOST]++
										ENDIF
									ENDIF
									
									IF MPDartGame.bDartWinner
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "DARTS_WINNER", "DARTS_GAMEW", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
										ELSE
											SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "DARTS_WINP", "DARTS_GAMEW", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
										ENDIF
									ELSE
										SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "BM_R2P_LOSS", "DARTS_GAMEL", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_RED)
									ENDIF
								ELSE
									IF MPDartGame.bDartWinner
										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "DARTS_WINNER", "DARTS_LEGW", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
											//SET_SCALEFORM_MIDSIZED_MESSAGE(DartsUI.siMidMessage, "DARTS_WINNER", "DARTS_LEGW")
										ELSE
											SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "DARTS_WINP", "DARTS_LEGW", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
											//SET_SCALEFORM_MIDSIZED_MESSAGE(DartsUI.siMidMessage, "DARTS_WINP", "DARTS_LEGW")
										ENDIF
									ELSE
										SET_SHARD_BIG_MESSAGE(shardMidSizeMessage, "BM_R2P_LOSS", "DARTS_LEGL", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_RED)
										//SET_SCALEFORM_MIDSIZED_MESSAGE(DartsUI.siMidMessage, "DARTS_LOSER", "DARTS_LEGL", 4000, HUD_COLOUR_RED)
									ENDIF
								ENDIF
								
								IF MPDartGame.bDartWinner
									PLAY_SOUND_FRONTEND ( -1, "WINNER", "CELEBRATION_SOUNDSET")
								ELSE
									PLAY_SOUND_FRONTEND ( -1, "LOSER", "CELEBRATION_SOUNDSET")
								ENDIF
								
//								INT iWhichOutro
//									
//								FLOAT fResolution
//								fResolution = GET_ASPECT_RATIO(TRUE)
//								CDEBUG1LN(DEBUG_DARTS, " fResolution = ", fResolution)
//								
//								IF fResolution > 2.0
//									CDEBUG1LN(DEBUG_DARTS, " resolution is eyefinity")
//									scenePosition = infoOut.ssTransition.tMPSceneEyefinity.Position
//									sceneRotation = infoOut.ssTransition.tMPSceneEyefinity.Rotation
//									//scenePosition = << 1992.29407, 3047.57690, 46.21517 >>
//									//sceneRotation = << 0.000, 0.000, 138.740 >>
//								ELSE
//									CDEBUG1LN(DEBUG_DARTS, "resolution is not eyefinity")
//									scenePosition = infoOut.ssTransition.tMPScene.Position
//									sceneRotation = infoOut.ssTransition.tMPScene.Rotation
//								//	scenePosition = << 1992.33569, 3047.92432, 46.21517 >>
//								//	sceneRotation = << 0.000, 0.000, 136.740 >>
//								ENDIF
								
//								iWhichOutro = (GET_RANDOM_INT_IN_RANGE() % 3)
//								
//								CDEBUG1LN(DEBUG_DARTS, "iWhichOutro = ", iWhichOutro)
//								
//								iSceneID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//								camDartsSync = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//								PLAY_SYNCHRONIZED_CAM_ANIM(camDartsSync, iSceneID, sOutroCam[iWhichOutro], "mini@dartsoutro")
//								SET_CAM_ACTIVE(camDartsSync, TRUE)
//								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
//								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
//									IF NOT IS_PED_INJURED(piPlayerClones[0])
//									AND NOT IS_PED_INJURED(piPlayerClones[1])
//										IF MPDartGame.bDartWinner
//											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iSelfID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
//											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iOtherID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
//											TASK_PLAY_ANIM(piPlayerClones[playerData[iSelfID].iOffsetPlayerID], sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
//										ELSE
//											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iOtherID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
//											TASK_SYNCHRONIZED_SCENE (piPlayerClones[playerData[iSelfID].iOffsetPlayerID], iSceneID, "mini@dartsoutro", sOutroLoss[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
//											TASK_PLAY_ANIM(piPlayerClones[playerData[iSelfID].iOffsetPlayerID], sFacialDictPed2, sFacialClip2[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
//										ENDIF
//									ENDIF
//								ELSE
//									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//										TASK_SYNCHRONIZED_SCENE (PLAYER_PED_ID(), iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
//										TASK_PLAY_ANIM(PLAYER_PED_ID(), sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
//									ENDIF
//								ENDIF
								
								
									//IF NOT IS_PED_INJURED(PLAYER_PED_ID())						
									//	TASK_PLAY_ANIM(NULL, "anim@amb@clubhouse@mini@darts@", "outro", WALK_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
									//ENDIF
										
								iSplashStage++
							ENDIF
						BREAK
						
						CASE 1
							//IF NOT UPDATE_SCALEFORM_MIDSIZED_MESSAGE(DartsUI.siMidMessage, TRUE)
							IF NOT UPDATE_SHARD_BIG_MESSAGE(shardMidSizeMessage, TRUE)
								
								RESET_SHARD_BIG_MESSAGE(shardMidSizeMessage)
								
								REPEAT NUM_MP_DART_PLAYERS i
									DARTS_CLEAR_SCORES(DartsSB, i)
								ENDREPEAT
								
								REPEAT NUM_MP_DART_TURNS j
									RESET_DART(MPDartGame.Darts[j])
								ENDREPEAT
								
								iSplashStage++
							ENDIF
						BREAK
						
						CASE 2
							IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
								IF NOT IS_PED_INJURED(piPlayerClones[DARTS_PLAYER_TWO])
								AND NOT IS_PED_INJURED(piPlayerClones[DARTS_PLAYER_ONE])
									DARTS_GAME_CAM()
									DESTROY_CAM(camDartsSync)
									//CLEAR_PED_TASKS (piPlayerClones[DARTS_PLAYER_TWO])
									//CLEAR_PED_TASKS (piPlayerClones[DARTS_PLAYER_ONE])
								ENDIF
							ELSE
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									DARTS_GAME_CAM()
									DESTROY_CAM(camDartsSync)
									//CLEAR_PED_TASKS (PLAYER_PED_ID())
								ENDIF
							ENDIF
							
							CDEBUG1LN(DEBUG_DARTS, "Starting another set/leg")
							//PlayerData[iSelfID].bDoRematch = TRUE
							TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH, TRUE)
							KILL_MINIGAME_MISSION_TRACKER()
							iSplashStage = 0
							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_WAIT_POST_GAME)
						BREAK
						
					ENDSWITCH
				BREAK
				
				CASE DARTS_MPSTATE_GAME_END
					//DARTS_UI_RETVAL DartsUIRetVal
					JOB_WIN_STATUS eJobWinStatus
					
					DEBUG_MESSAGE_PERIODIC("Reached Client side GAME END", iDebugThrottle)
					
					//IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
						//IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX(iOtherID) )
							//SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
						//ENDIF
						//SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
					//ENDIF
					
					IF GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 0.75
						// wait for the player to pick an option
						
						IF (PlayerData[iOtherID].eMPState >= DARTS_MPSTATE_GAME_END)
						OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							
							// wait 'til we've finished rendering the player win/lost text
							IF NOT MPDartGame.bAnotherCamActive
								
								CLEAR_HELP()
								CLEAR_PRINTS()
								
								#IF IS_DEBUG_BUILD
									IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_WINNERD)
									OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_LOSERD)
										MPDartGame.bDartWinner = TRUE
									ELIF DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_WINNERD)
									OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_LOSERD)
										MPDartGame.bDartWinner = FALSE
									ENDIF
								#ENDIF
								
								
								IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
									IF MPDartGame.bDartWinner
										BIK_ADD_POINTS_FOR_THIS(BIK_ADD_POINTS_MINIGAME)
									ENDIF
								ENDIF
								
								START_PLAYING_OUTRO_ANIM(PlayerData[PARTICIPANT_ID_TO_INT()])
									
								IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)									
									IF MPDartGame.bDartWinner
										PLAY_CELEB_WIN_POST_FX()
									ELSE
										PLAY_CELEB_LOSE_POST_FX()
									ENDIF
								ELSE
									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									//	PLAY_CELEB_WIN_POST_FX()
										
									//	TASK_SYNCHRONIZED_SCENE (PLAYER_PED_ID(), iSceneID, "mini@dartsoutro", sOutroWin[iWhichOutro], INSTANT_BLEND_IN, WALK_BLEND_OUT )
									//	TASK_PLAY_ANIM(PLAYER_PED_ID(), sFacialDictPed, sFacialClip[GET_RANDOM_INT_IN_RANGE(0, 3)], WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
									ENDIF
								ENDIF
								
								MPDartGame.bAnotherCamActive = TRUE
//							ELSE
//								IF (IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) >= 0.995)
//								OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID)
//									
////									IF DOES_CAM_EXIST(camDartsSync)
////										DESTROY_CAM(camDartsSync)
////										
////										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
////											IF NOT IS_PED_INJURED(piPlayerClones[0])
////											AND NOT IS_PED_INJURED(piPlayerClones[1])
////												CLEAR_PED_TASKS_IMMEDIATELY(piPlayerClones[0])
////												CLEAR_PED_TASKS_IMMEDIATELY(piPlayerClones[1])
////												FORCE_PED_AI_AND_ANIMATION_UPDATE(piPlayerClones[0])
////												FORCE_PED_AI_AND_ANIMATION_UPDATE(piPlayerClones[1])
////											ENDIF
////										ELSE
////											IF NOT IS_PED_INJURED(PLAYER_PED_ID())
////												CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
////												FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
////											ENDIF
////										ENDIF
////									ENDIF
////									
////									DARTS_SETUP_MP_QUIT_CAM()
////									//ANOTHER_GAME_BACKSIDE()
//								ENDIF
							ENDIF
						ENDIF
						
						IF iBullseyeXP > 0
							CDEBUG1LN(DEBUG_DARTS, "iBullseyeXP = ", iBullseyeXP)
						ENDIF
						
						IF MPDartGame.bDartWinner
							eJobWinStatus = JOB_STATUS_WIN
							winningPlayer = PLAYER_ID()
						ELSE
							eJobWinStatus = JOB_STATUS_LOSE
							winningPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID))
						ENDIF
						
						IF GET_TIMER_IN_SECONDS_SAFE(controlTimer) > 2.15
							IF HAS_GENERIC_CELEBRATION_FINISHED(sCelebrationData, camEndScreen, 
																"XPT_DARTS", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DARTS_WIN, iXPGained, 
																eJobWinStatus, g_iMyBetWinnings, eGenericMGStage, winningPlayer, TRUE, 
																0, 0, TRUE, FALSE)
																//DartsStats[DARTS_SCLB_STAT_NUM_LEGS_WON], DartsStats[DARTS_SCLB_STAT_NUM_LEGS_WON] + DartsStats[DARTS_SCLB_STAT_NUM_LEGS_LOST], TRUE)
							
	//							IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
	//								REQUEST_LEADERBOARD_CAM()
	//								//RENDER_SCRIPT_CAMS(FALSE, FALSE)
	//								
	//							ELIF IS_LEADERBOARD_CAM_READY(TRUE)
									
								IF NOT IS_TIMER_STARTED(endDelay)
									RESTART_TIMER_NOW(endDelay)
//									IF DOES_CAM_EXIST(camDartsSync)
//										DESTROY_CAM(camDartsSync)
//										
//										IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
//											IF NOT IS_PED_INJURED(piPlayerClones[0])
//											AND NOT IS_PED_INJURED(piPlayerClones[1])
//												CLEAR_PED_TASKS_IMMEDIATELY(piPlayerClones[0])
//												CLEAR_PED_TASKS_IMMEDIATELY(piPlayerClones[1])
//												FORCE_PED_AI_AND_ANIMATION_UPDATE(piPlayerClones[0])
//												FORCE_PED_AI_AND_ANIMATION_UPDATE(piPlayerClones[1])
//											ENDIF
//										ELSE
//											IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//												CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//												FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//											ENDIF
//										ENDIF
//									ENDIF
//									
//									DARTS_SETUP_MP_QUIT_CAM()
									IF MPDartGame.bDartWinner
										ANIMPOSTFX_STOP("MP_Celeb_Win")
										//ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, TRUE)
									ELSE
										ANIMPOSTFX_STOP("MP_Celeb_Lose")
										//ANIMPOSTFX_PLAY("MP_Celeb_Lose_Out", 0, TRUE)
									ENDIF
									
								ELIF GET_TIMER_IN_SECONDS(endDelay) > 0.1
								
								ENDIF
								
								//IF SET_SKYSWOOP_UP(TRUE, TRUE, FALSE)
								//CDEBUG1LN(DEBUG_DARTS, "THIS SKY SWOOP1")
									IF MPDartGame.bEndGameMessageDisplayed 
										
										#IF IS_DEBUG_BUILD
										IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_WINNERD)
										OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_LOSERD)
											CDEBUG1LN(DEBUG_DARTS, "Win / Lose debug flags cleared up")
											DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_WINNERD)
											DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_LOSERD)
										ENDIF
										#ENDIF
//										IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//											IF NOT HAS_NET_TIMER_STARTED(serverData.timeLeaderboardTimeOut)
//												START_NET_TIMER(serverData.timeLeaderboardTimeOut)
//											ENDIF
//										
//											FMMC_EOM_SERVER_RESET_PLAYER_COUNTS(sPlayerCounts)
//											INT iRematchLoop
//											REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iRematchLoop
//												IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iRematchLoop))
//													FMMC_EOM_SERVER_PLAYER_PROCESSING(sPlayerCounts, iRematchLoop)
//												ENDIF
//											ENDREPEAT
//											
//											FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING(sPlayerCounts, serverData.sServerFMMC_EOM, FMMC_TYPE_MG_DARTS, 0)
//										ENDIF
										
										
//										IF HAS_NET_TIMER_STARTED(serverData.timeLeaderboardTimeOut)
//											IF MAINTAIN_END_OF_MISSION_SCREEN(sEndOfMission, serverData.sServerFMMC_EOM,
//																		GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),serverData.timeLeaderboardTimeOut.timer) >= VIEW_LEADERBOARD_TIME,
//																		FALSE, FALSE, FALSE, FALSE, FALSE, serverData.timeLeaderboardTimeOut.timer, VIEW_LEADERBOARD_TIME, "LBD_TIMEOUT", TRUE)
												//Go to cleanup state
												CDEBUG1LN(DEBUG_DARTS, "Client chose to quit")
												CDEBUG1LN(DEBUG_DARTS, "my betting Wins = ", iBetWinnings)
												
												SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, contentID)
												
												CLEAR_PRINTS()
												//PlayerData[iSelfID].bDoQuit = TRUE
												TOGGLE_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_DO_QUIT, TRUE)
												//PlayerData[iSelfID].eMPState = DARTS_MPSTATE_END
												SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_END)
//											ELSE
//												CDEBUG1LN(DEBUG_DARTS, "still voting")
//												//DISPLAY_DARTS_LEADERBOARD(DartsUI, DartsEndScreen, iXPGained, DartsUI.iOurScore, DartsUI.iTheirScore, iBetWinnings)
//											ENDIF
//										ENDIF
										
									ELSE
										IF (PlayerData[iOtherID].eMPState >= DARTS_MPSTATE_GAME_END)
										OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
											
											#IF IS_DEBUG_BUILD
												IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_WINNERD)
												OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_LOSERD)
													MPDartGame.bDartWinner = TRUE
												ELIF DARTS_CHECK_CLIENT_FLAG(PlayerData[iOtherID], DARTS_CLIENTFLAG_WINNERD)
												OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_LOSERD)
													MPDartGame.bDartWinner = FALSE
												ENDIF
											#ENDIF
											
											MPDartGame.bEndGameMessageDisplayed = TRUE
											
										ENDIF
									ENDIF
								//ENDIF
								
							ENDIF
							
							IF sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING 
							AND HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, (CELEBRATION_SCREEN_STAT_DISPLAY_TIME/2) - 750)
								IF NOT bPhaseGrabbed
									
									
									
									
									CDEBUG1LN(DEBUG_DARTS, "g_sMPTunables.xpMultiplier = ", g_sMPTunables.xpMultiplier)
									CDEBUG1LN(DEBUG_DARTS, "LOBBY_AUTO_MULTIPLAYER_FREEMODE() = ", LOBBY_AUTO_MULTIPLAYER_FREEMODE())
									CDEBUG1LN(DEBUG_DARTS, "GET_SKYFREEZE_STAGE() = ", GET_SKYFREEZE_STAGE())
									CDEBUG1LN(DEBUG_DARTS, "IS_PAUSE_MENU_ACTIVE() = ", IS_PAUSE_MENU_ACTIVE())
									
									IF GET_SKYFREEZE_STAGE() = SKYFREEZE_NONE
										SET_SKYFREEZE_STAGE(SKYFREEZE_FROZEN)
									ENDIF
									
									TOGGLE_RENDERPHASES(FALSE)
									
									CDEBUG1LN(DEBUG_DARTS, "rendering toggled")
									bPhaseGrabbed = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_WAIT_POST_GAME
					DEBUG_MESSAGE_PERIODIC("CLIENT DARTS_MPSTATE_WAIT_POST_GAME", iDebugThrottle)
					
					//IF (PlayerData[iSelfID].bDoRematch AND PlayerData[iOtherID].bDoRematch)
					
					IF (GET_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH)
						AND GET_PLAYER_STATE(playerData[iDartPlayerID[iOpponentDartID]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH))
					
					OR (GET_PLAYER_STATE(playerData[iDartPlayerID[iSelfDartID]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH)
						AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER))
					
					//OR (PlayerData[iSelfID].bDoRematch AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER))
										
						IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PlayerData[iSelfID].piOther)
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())		//<-					
						ENDIF
						// TODO:  add audio for playing again
						
						IF DARTS_CHECK_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
							sDartsPlayerNames[iSelfDartID] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSelfID)))
							DartsGamerHandle[iSelfDartID] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSelfID)))
							sCrewTags[iSelfDartID] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[iSelfDartID])
								
							sDartsPlayerNames[iOpponentDartID] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
							DartsGamerHandle[iOpponentDartID] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
							sCrewTags[iOpponentDartID] = GET_PLAYER_CLAN_TAG(DartsGamerHandle[iOpponentDartID])
							
							//piOtherPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherID)))
							
							INT iTemp1, iTemp2
							iTemp1 = ENUM_TO_INT(DART_PLAYER_AWAY)
							iTemp2 = ENUM_TO_INT(DART_PLAYER_HOME)
							
							CDEBUG1LN(DEBUG_DARTS, "iTemp1 = ", iTemp1, " iTemp2 = ", iTemp2)
							CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iTemp1, " = ", sCrewTags[iTemp1])
							CDEBUG1LN(DEBUG_DARTS, "sCrewTags ", iTemp2, " = ", sCrewTags[iTemp2])
							
							DARTS_ADD_NAMES(DartsSB, sDartsPlayerNames[iOpponentDartID], sDartsPlayerNames[iSelfDartID])
							IF NOT IS_STRING_EMPTY(sCrewTags[iOpponentDartID])
								DARTS_ADD_SB_CREW_TAG_UGC_CHECK(DartsSB, iTemp1, sCrewTags[iOpponentDartID])
							ENDIF
							IF NOT IS_STRING_EMPTY(sCrewTags[iSelfDartID])
								DARTS_ADD_SB_CREW_TAG_UGC_CHECK(DartsSB, iTemp2, sCrewTags[iSelfDartID])
							ENDIF
							DARTS_CLEAR_SCORES(DartsSB, iTemp1)
							DARTS_CLEAR_SCORES(DartsSB, iTemp2)
							
							DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							DARTS_CLEAR_CLIENT_FLAG(PlayerData[iSelfID], DARTS_CLIENTFLAG_NEW_CHALLENGER)
						ENDIF
						
						CLEAR_HELP()
						CLEANUP_BIG_MESSAGE()
						
						IF (serverData.mpState = DARTS_MPSTATE_SETUP)
							SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_SYNC_OBJECTS)
						ENDIF
					ENDIF
					
					// other player wants to quit 
					//IF PlayerData[iOtherID].bDoQuit
					IF GET_PLAYER_STATE(playerData[iDartPlayerID[iOpponentDartID]], DARTS_CLIENTFLAG_STATE_DART_DO_QUIT)
						KILL_MINIGAME_MISSION_TRACKER()
						CLEAR_PRINTS()
						CLEANUP_BIG_MESSAGE()
						
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_TERMINATE_SPLASH)
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_TERMINATE_SPLASH //20
					DEBUG_MESSAGE_PERIODIC("Reached Client side TERMINATE SPLASH", iDebugThrottle)
					
					IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
						IF HAS_NET_TIMER_STARTED(timeDartsAwardFailSafe)
							//RENDER_SCRIPT_CAMS(FALSE, FALSE)
							//DARTS_SETUP_MP_QUIT_CAM(infoOut.csQuitCam)
							
							
							IF (IS_SCREEN_FADED_OUT()
							OR IS_SCREEN_FADING_OUT())
							AND NOT IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
								DO_SCREEN_FADE_IN(500)
							ENDIF
							
							IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
								CDEBUG1LN(DEBUG_DARTS, "REQUEST LEADERBOARD CAMERA")
							//	REQUEST_LEADERBOARD_CAM()
							//ELIF IS_LEADERBOARD_CAM_READY(TRUE)
							//IF 1 > 0
								REINIT_NET_TIMER(timeDartsAwardFailSafe)
								CLEAR_PRINTS()
								CLEAR_HELP()
								SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
								
								IF MPGlobals.DartsData.bResultsDisplayed
									MPGlobals.DartsData.bResultsDisplayed = FALSE
								ENDIF
								
								SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_LEAVE)
								
								IF (terminateReason = DARTS_MPTERMINATEREASON_PLAYERLEFT)
									START_PLAYING_OUTRO_ANIM(PlayerData[PARTICIPANT_ID_TO_INT()])
									SET_SHARD_BIG_MESSAGE(shardBigMessage, "DARTS_WINNER", "DARTS_OPP_LFT", (AWARD_FAILSAFE/2), SHARD_MESSAGE_CENTERED, HUD_COLOUR_WHITE)
									INIT_SIMPLE_USE_CONTEXT(DartsUI.inGameControlContext, FALSE, FALSE, FALSE, TRUE)
									ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "CMRC_CONT", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
									SWITCH_MPLEAVE_STATE(leaveState, DARTS_MPLEAVESTATE_BIG_MESSAGE)
									
									SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, contentID)
							
								ELIF terminateReason = DARTS_MPTERMINATEREASON_PLAYER_NO_RESPONSE
									START_PLAYING_OUTRO_ANIM(PlayerData[PARTICIPANT_ID_TO_INT()])
									SET_SHARD_BIG_MESSAGE(shardBigMessage, "DARTS_WINNER", "", (AWARD_FAILSAFE/2), SHARD_MESSAGE_CENTERED, HUD_COLOUR_WHITE)
									INIT_SIMPLE_USE_CONTEXT(DartsUI.inGameControlContext, FALSE, FALSE, FALSE, TRUE)
									ADD_SIMPLE_USE_CONTEXT_INPUT(DartsUI.inGameControlContext, "CMRC_CONT", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
									SWITCH_MPLEAVE_STATE(leaveState, DARTS_MPLEAVESTATE_BIG_MESSAGE)
									
									SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, contentID)
								ELSE
									IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
										IF IS_STRING_NULL(sDartsPlayerNames[playerData[iOtherID].iOffsetPlayerID])
											PRINT_NOW("DARTS_OPP_LFT", DEFAULT_GOD_TEXT_TIME, 0)
										ELSE
											CDEBUG1LN(DEBUG_DARTS, "Other player name (ln 2950) = ", sDartsPlayerNames[playerData[iOtherID].iOffsetPlayerID])
											PRINT_WITH_NAME("DARTS_PLYR_LFT", sDartsPlayerNames[playerData[iOtherID].iOffsetPlayerID], DEFAULT_GOD_TEXT_TIME, 0)
										ENDIF
									ENDIF
									SWITCH_MPLEAVE_STATE(leaveState, DARTS_MPLEAVESTATE_LEADERBOARD)
								ENDIF
							
							ENDIF
						ELSE
							START_NET_TIMER(timeDartsAwardFailSafe)
						ENDIF
					ELSE
						//CDEBUG1LN(DEBUG_DARTS, "Other player name (ln 2962) = ", sDartsPlayerNames[iOpponentDartID])
						//PRINT_WITH_NAME("DARTS_PLYR_LFT", sDartsPlayerNames[playerData[iOtherID].iOffsetPlayerID], DEFAULT_GOD_TEXT_TIME, 0)
						CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB,  PlayerData, PlayerDarts,MPDartGame, DartsUI)
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_END //23
					
					IF HAS_NET_TIMER_STARTED(timeDartsAwardFailSafe)
					
						CLEANUP_BIG_MESSAGE()
						START_PLAYING_OUTRO_ANIM(PlayerData[PARTICIPANT_ID_TO_INT()])
						
						REINIT_NET_TIMER(timeDartsAwardFailSafe)
						SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
						
						IF MPGlobals.DartsData.bResultsDisplayed
							MPGlobals.DartsData.bResultsDisplayed = FALSE
						ENDIF
						
						CDEBUG1LN(DEBUG_DARTS, "DARTS MPSTATE END CALL LEADER")
						SWITCH_MPSTATE(PlayerData[iSelfID].eMPState, DARTS_MPSTATE_LEAVE)
						SWITCH_MPLEAVE_STATE(leaveState, DARTS_MPLEAVESTATE_OUT_FADE)
					ELSE
						START_NET_TIMER(timeDartsAwardFailSafe)
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_LEAVE //21
					SWITCH leaveState
						CASE DARTS_MPLEAVESTATE_BIG_MESSAGE
							IF NOT UPDATE_SHARD_BIG_MESSAGE(shardBigMessage, TRUE)
							OR HAS_NET_TIMER_EXPIRED(timeDartsAwardFailSafe, 3000) // wait 5 seconds and quit instead AWARD_FAILSAFE/2)
								CLEANUP_BIG_MESSAGE()
								
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] timeDartsAwardFailSafe = ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeDartsAwardFailSafe))
								
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] giving participation XP after an early quit")
								iXPToGive = ROUND(BASE_DART_PLAY_XP * g_sMPTunables.fxp_tunable_darts_taking_part)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_bullseye = ", g_sMPTunables.fxp_tunable_darts_bullseye)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_legwon = ", g_sMPTunables.fxp_tunable_darts_legwon)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_match_win = ", g_sMPTunables.fxp_tunable_darts_match_win)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] g_sMPTunables.fxp_tunable_darts_taking_part = ", g_sMPTunables.fxp_tunable_darts_taking_part)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iXPToGive = ", iXPToGive)
								iXPGained += GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_DARTS", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DARTS_WIN, iXPToGive)
								CDEBUG1LN(DEBUG_DARTS, "[DARTSXP] iXPGained is now ", iXPGained)
								DartsUI.iOurScore++

								SWITCH_MPLEAVE_STATE(leaveState, DARTS_MPLEAVESTATE_OUT_FADE)
//							ELSE
//								IF NOT NETWORK_TEXT_CHAT_IS_TYPING()
//									UPDATE_SIMPLE_USE_CONTEXT(DartsUI.inGameControlContext)
//								ENDIF
							ENDIF
						BREAK
						
						CASE DARTS_MPLEAVESTATE_SWOOP_UP
							SWITCH_MPLEAVE_STATE(leaveState, DARTS_MPLEAVESTATE_LEADERBOARD)
						BREAK
						
						CASE DARTS_MPLEAVESTATE_LEADERBOARD
//							IF DISPLAY_DARTS_LEADERBOARD(DartsUI, DartsEndScreen, iXPGained, DartsUI.iOurScore, DartsUI.iTheirScore, iBetWinnings)
//							OR HAS_NET_TIMER_EXPIRED(timeDartsAwardFailSafe, AWARD_FAILSAFE)
								CDEBUG1LN(DEBUG_DARTS, "WINNER ID", serverData.iWinner)
								SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
								CDEBUG1LN(DEBUG_DARTS, "TERMINATE LEAVE LEADERBOARD")
								CLEANUP_MP_DARTS_GAME(bSpectator, sLocalIntroScene, missionScriptArgs, ServerData, DartsSB,  PlayerData, PlayerDarts,MPDartGame, DartsUI)
//							ENDIF
						BREAK
						
						CASE DARTS_MPLEAVESTATE_OUT_FADE
//							IF IS_SCREEN_FADED_IN()
//							AND NOT IS_SCREEN_FADING_OUT()
//								DO_SCREEN_FADE_OUT(500)
//							ENDIF
							
							SWITCH_MPLEAVE_STATE(leaveState, DARTS_MPLEAVESTATE_OUT_ANIM)
						BREAK
						
						CASE DARTS_MPLEAVESTATE_OUT_ANIM
							//IF NOT IS_TIMER_STARTED(stFailSafe)
							//	START_TIMER_NOW(stFailSafe)
							//ENDIF
							
							//IF (NOT IS_SCREEN_FADING_OUT() AND IS_SCREEN_FADED_OUT())
							//OR GET_TIMER_IN_SECONDS(stFailSafe) >= 5.0
//								IF IS_SCREEN_FADED_OUT()
//								OR IS_SCREEN_FADING_OUT()
//									IF NOT IS_BIT_SET(iCleanupFlags, ENUM_TO_INT(eCLEANUP_FLAG_DONT_FADE_IN))
//										DO_SCREEN_FADE_IN(500)
//									ENDIF
//								ENDIF
											
								SWITCH_MPLEAVE_STATE(leaveState, DARTS_MPLEAVESTATE_LEADERBOARD)
							//ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
			ENDSWITCH
		ENDIF
		
		//### CHECK CLIENT FLAGS
		//### SCOREBAORD
		IF IS_BIT_SET(iLocalStateFlags, ENUM_TO_INT(eLP_DRAW_SCORE_BOARD))
			IF HAS_SCALEFORM_MOVIE_LOADED(DartsSB.siScoreBoard)
				DRAW_SCALEFORM_MOVIE_3D(DartsSB.siScoreBoard, DartsSB.vScoreBoardPos, DartsSB.vScoreBoardRot, DartsSB.vScoreBoardScale, DartsSB.vScoreBoardScale)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalStateFlags, ENUM_TO_INT(eLP_HIDE_PLAYER_HUD)) 
			HIDE_PLAYER_HUD()
			
		ENDIF
		
		IF IS_BIT_SET(iLocalStateFlags, ENUM_TO_INT(eLP_HIDE_PLAYER_OVERHEAD)) 
			DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
		ENDIF
		
		IF IS_BIT_SET(iLocalStateFlags, ENUM_TO_INT(eLP_HIDE_PARTICIPANTS)) 
		AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_I_AM_SPECTATOR)
			HIDE_SPEC_PLAYERS()
		ENDIF
		
		IF IS_BIT_SET(iLocalStateFlags, ENUM_TO_INT(eLP_UPDATE_OPPONENT_TRANSFORM))
			IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX(iDartPlayerID[iOpponentDartID]) )
				PED_INDEX pedOpponent 
				pedOpponent =  GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iOpponentDartID])))
				IF IS_ENTITY_ALIVE(pedOpponent)
					FLOAT fHeadingToPlayBoard
					fHeadingToPlayBoard = GET_HEADING_BETWEEN_VECTORS_2D(DartsPlayerPos.vOpponentPos, MPDartGame.dBoard.vDartBoard)
					VECTOR posAdjust
					posAdjust = GET_ENTITY_COORDS(pedOpponent)
					DartsPlayerPos.vOpponentPos.z = posAdjust.z
					NETWORK_OVERRIDE_COORDS_AND_HEADING(pedOpponent, posAdjust, fHeadingToPlayBoard)
					//CDEBUG1LN(DEBUG_DARTS, " Intro - Adjust opponent Position:", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[iOpponentDartID]))))
				ENDIF
			ENDIF
		ENDIF
		
		//for spec only
		IF IS_BIT_SET(iLocalStateFlags, ENUM_TO_INT(eLP_DISABLE_CAM_COL_HAND_BLOCK)) 
			IF DOES_ENTITY_EXIST(PlayerDarts[DARTS_PLAYER_ONE].oHandBlocker)
				SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(PlayerDarts[DARTS_PLAYER_ONE].oHandBlocker)
				IF (GET_GAME_TIMER() % 1000) > 100
					//CDEBUG1LN(DEBUG_DARTS, " Disable Col player ONE")
				ENDIF
			ENDIF
			IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
				IF DOES_ENTITY_EXIST(PlayerDarts[DARTS_PLAYER_TWO].oHandBlocker)
					SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(PlayerDarts[DARTS_PLAYER_TWO].oHandBlocker)
					IF (GET_GAME_TIMER() % 1000) > 100
						//CDEBUG1LN(DEBUG_DARTS, " Disable Col player TWO")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET_ENUM(iLocalStateFlags, eLP_SHOW_BUSYSPINNER) 
			IF NOT BUSYSPINNER_IS_ON()
				CDEBUG1LN(DEBUG_DARTS,"Server - Start busy spinner for local")
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
				END_TEXT_COMMAND_BUSYSPINNER_ON(0)
			ENDIF
		ELSE
			IF BUSYSPINNER_IS_ON()
				BUSYSPINNER_OFF()
			ENDIF
		ENDIF
		//######################################## HOST UPDATE ###################################################
		//#######################################################################################################
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			// General Update
			SWITCH ServerData.mpState
				CASE DARTS_MPSTATE_INIT
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS] has reached Server side INIT")
						// wait for another participant
						IF iNumberOfPlayers >= 2
							// server side init stuff HERE
							
							//IF DART_GET_OTHER_PLAYERS_ID(PlayerData, DART_PLAYER_AWAY_SERVER) = 2
							
								//DART_PLAYER_HOME_SERVER = PARTICIPANT_ID_TO_INT()
								
								CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS SERVER] Server detected a 2 player game:")
								CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS SERVER] DART_PLAYER_ONE_ID = ", iDartPlayerID[DARTS_PLAYER_ONE])
								CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS SERVER] DART_PLAYER_TWO_ID = ", iDartPlayerID[DARTS_PLAYER_TWO])
								
								//ServerData.mpState = DARTS_MPSTATE_INTRO_SYNC_SETUP
								//SWITCH_MPSTATE(ServerData.mpState, DARTS_MPSTATE_INTRO_SYNC_SETUP, TRUE)
								SWITCH_MPSTATE(ServerData.mpState, DARTS_MPSTATE_THROW_OFF_SETUP, HOST)
							//ENDIF
							
						ELIF DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							SWITCH_MPSTATE(ServerData.mpState, DARTS_MPSTATE_SETUP, HOST)
							CDEBUG1LN(DEBUG_DARTS, "Single palyer")
						ENDIF
				BREAK
				
				
//				CASE DARTS_MPSTATE_INTRO_SYNC_SETUP
//					IF IS_BIT_SET(PlayerData[iOtherID].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_SYNC_SCENE_READY))
//					AND IS_BIT_SET(PlayerData[iSelfID].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_SYNC_SCENE_READY))
//						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS SERVER] ALL PLAYERS READY, CREATING NETWORK SYNC SCENE")
//						IF NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_AWAY])
//						AND NOT IS_PED_INJURED(piPlayerClones[DART_PLAYER_HOME])
//						//Camera/Scene Setup
//						iSceneId = NETWORK_CREATE_SYNCHRONISED_SCENE(infoOut.tIntroScene.Position, infoOut.tIntroScene.Rotation)
//						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(piPlayerClones[DART_PLAYER_AWAY], iSceneId, "mini@dartsintro_alt1", "darts_ig_intro_alt1_guy1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(piPlayerClones[DART_PLAYER_HOME], iSceneId, "mini@dartsintro_alt1", "darts_ig_intro_alt1_guy2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//						NETWORK_START_SYNCHRONISED_SCENE(iSceneId)
//
//						SWITCH_MPSTATE(ServerData.mpState, DARTS_MPSTATE_INTRO_SYNC_PLAY, TRUE)
//						ENDIF
//					ENDIF
//				BREAK
//				
//				CASE DARTS_MPSTATE_INTRO_SYNC_PLAY
//					IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) = 1.0
//						CDEBUG1LN(DEBUG_DARTS, "SERVER - sync scene is done")
//						SWITCH_MPSTATE(ServerData.mpState, DARTS_MPSTATE_THROW_OFF_SETUP, TRUE)
//					ELSE
//						CDEBUG1LN(DEBUG_DARTS, "SERVER - sync scene is still running")
//					ENDIF
//				BREAK
				
				CASE DARTS_MPSTATE_THROW_OFF_SETUP
					
					IF IS_BIT_SET(PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_INTRO_READY))
					AND IS_BIT_SET(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_INTRO_READY))
						IF NOT DARTS_SERVER_CHECK_FLAG(ServerData, DARTSERVERFLAG_PLAYERS_INTRO_READY)
							DARTS_SERVER_SET_FLAG(ServerData, DARTSERVERFLAG_PLAYERS_INTRO_READY)
							CDEBUG1LN(DEBUG_DARTS, "SERVER - both palyers are in the position for intro.")
						ENDIF
					ELSE
						IF GET_GAME_TIMER() % 1000 > 50
							CDEBUG1LN(DEBUG_DARTS, "SERVER - waiting for players to get into position.")
						ENDIF
					ENDIF

				
					DEBUG_MESSAGE_PERIODIC("Reached Server side THROW_OFF_SETUP", iDebugThrottle)
					IF IS_BIT_SET(PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_AT_THROWOFF))
					AND IS_BIT_SET(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_AT_THROWOFF))
						DARTS_SERVER_THROW_OFF_SETUP(ServerData)
						//ServerData.mpState = DARTS_MPSTATE_THROW_OFF
						SWITCH_MPSTATE(ServerData.mpState, DARTS_MPSTATE_THROW_OFF, HOST)
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_THROW_OFF
					
						DART DartInfoOff
						DartInfoOff = MPDartGame.darts[ServerData.iServerTurn]
						IF bSpectator
						AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
								
								IF PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage < DARTS_MPTHROW_TURN_CHANGE
								AND PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage < DARTS_MPTHROW_TURN_CHANGE
									DartInfoOff.bDoneScoring = FALSE
								ENDIF
								
								CDEBUG1LN(DEBUG_DARTS,"ONE:", PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage, " TWO",PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage )
								IF PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage >= DARTS_MPTHROW_TURN_CHANGE
								OR PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage >= DARTS_MPTHROW_TURN_CHANGE
									DartInfoOff.bDoneScoring = TRUE
									DartInfoOff.iHitValue = PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].ClientDart.iHitValue
									DartInfoOff.iHitMultiplier = PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].ClientDart.iHitMultiplier
								ENDIF
								
							//COPY_DART_TO_LOCAL(MPDartGame.Darts[ServerData.iServerTurn], playerData[iDartPlayerID[DARTS_PLAYER_ONE]])
						ENDIF
						
					DARTS_SERVER_THROW_OFF_UPDATE(ServerData, PlayerData, DartInfoOff, iDartPlayerID)
				BREAK
				
				CASE DARTS_MPSTATE_SETUP
															
					IF NOT DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_THROW_OFF_DONE)
						DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_THROW_OFF_DONE)
					ENDIF

					DEBUG_MESSAGE_PERIODIC("Reached Server side SETUP", iDebugThrottle)
								
					bSyncComplete = FALSE
						IF IS_BIT_SET(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_SINGLE_PLAYER))
							IF IS_BIT_SET(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_SYNC_COMPLETE))
								bSyncComplete = TRUE
								CDEBUG1LN(DEBUG_DARTS,"Single sync complete")
							ENDIF
						ELSE
							IF iDartPlayerID[DARTS_PLAYER_TWO] >= 0
								IF (IS_BIT_SET(PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_SYNC_COMPLETE))
								AND IS_BIT_SET(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].iClientFlags, ENUM_TO_INT(DARTS_CLIENTFLAG_SYNC_COMPLETE)))
									bSyncComplete = TRUE
									CDEBUG1LN(DEBUG_DARTS,"Multi sync complete")
								ENDIF
							ENDIF
						ENDIF

					IF bSyncComplete
						DARTS_SERVER_GAME_SETUP(ServerData, DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER), 0)
						
						// deactivate floating board help
						BROADCAST_DARTS_BOARD_ACTIVATE(PLAYER_ID(), FALSE, TRUE)
						
						IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)
							// broadcast match info to all players
							broadcastIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSelfID))
							BROADCAST_DARTS_UPDATE(broadcastIndex, NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_ONE])), 
													ServerData.iScores[DART_PLAYER_AWAY_SERVER], ServerData.iScores[DART_PLAYER_HOME_SERVER], 
													GET_PLAYER_TEAM(broadcastIndex), TRUE, TRUE)
						ENDIF
						
						//ServerData.mpState = DARTS_MPSTATE_IN_PROGRESS
						SWITCH_MPSTATE(ServerData.mpState, DARTS_MPSTATE_IN_PROGRESS, HOST)
						CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS]Server is proceeding to IN PROGRESS")
					ENDIF
				BREAK
				
				CASE DARTS_MPSTATE_IN_PROGRESS
					
					IF NETWORK_CHECK_ALL_SPEC_SCORED(PlayerData, FALSE)
						DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_SPECS_SCORED)
					ELSE
						DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_SPECS_SCORED)
					ENDIF
					
					IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SINGLE_PLAYER)	
					//If spectator is host copy current hit info from main client.
						DART DartInfoMulti
						DartInfoMulti = MPDartGame.darts[ServerData.iServerTurn]
						IF bSpectator
						AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
								
								IF PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage < DARTS_MPTHROW_TURN_CHANGE
								AND PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage < DARTS_MPTHROW_TURN_CHANGE
									DartInfoMulti.bDoneScoring = FALSE
								ENDIF
								
								IF PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage >= DARTS_MPTHROW_TURN_CHANGE
								OR PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage >= DARTS_MPTHROW_TURN_CHANGE
									DartInfoMulti.bDoneScoring = TRUE
									DartInfoMulti.iHitValue = PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].ClientDart.iHitValue
									DartInfoMulti.iHitMultiplier = PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].ClientDart.iHitMultiplier
								ENDIF

						ENDIF
						
						DARTS_SERVER_UPDATE_GAME(ServerData, PlayerData, DartInfoMulti, iDartPlayerID)
					ELSE
						//If spectator is host copy current hit info from main client.
						DART DartInfo
						DartInfo = MPDartGame.darts[ServerData.iServerTurn]
						
						IF bSpectator
						AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
								
							IF PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage < DARTS_MPTHROW_TURN_CHANGE
							AND PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage < DARTS_MPTHROW_TURN_CHANGE
								DartInfo.bDoneScoring = FALSE
							ENDIF
							
							IF PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage >= DARTS_MPTHROW_TURN_CHANGE
							AND PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage >= DARTS_MPTHROW_TURN_CHANGE
								DartInfo.bDoneScoring = TRUE
								DartInfo.iHitValue = PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].ClientDart.iHitValue
								DartInfo.iHitMultiplier = PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].ClientDart.iHitMultiplier
							ENDIF
						ENDIF
						
						DARTS_SERVER_UPDATE_SINGLE_PLAYER_GAME(ServerData, PlayerData, DartInfo, iDartPlayerID)
					ENDIF
				BREAK				
			ENDSWITCH		
		ENDIF		
	ENDWHILE
ENDSCRIPT


