////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Dart.sch                          //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Dart Defs           			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "globals.sch"

MODEL_NAMES modelDart1 = PROP_DART_1
MODEL_NAMES modelDart2 = PROP_DART_2

INT iPlayr
INT iTurn

// From IV
STRUCT DART
   	OBJECT_INDEX object
	MODEL_NAMES modelDart 
	VECTOR 	vPosition 		//current position in the air
	VECTOR 	vThrow			//the path the dart is traveling
	VECTOR 	vTarget 		//local point on the dartboard
	VECTOR 	vOffsetTarget	//real world point ont he dartboard
	VECTOR 	vRandomTarget	//initial random point at the start of the turn
    VECTOR 	vRotation		//spin of the dart
	INT 	iHitValue		//raw score
	INT		iHitMultiplier	//applicable multiplier
	FLOAT	fLength			//distance from bullseye
	BOOL 	bStuck			//has dart hit the board?
	BOOL 	bTravelling		//is dart travelling through the air?
	BOOL 	bDoneScoring	//are we finished scoring the dart?
ENDSTRUCT

FUNC VECTOR GET_DART_POSITION(DART Darts)
	RETURN Darts.vPosition
ENDFUNC

PROC SET_DART_POSITION(DART& Darts, VECTOR vPosition)
	Darts.vPosition = vPosition
ENDPROC

FUNC VECTOR GET_DART_THROW(DART& Darts)
	RETURN Darts.vThrow
ENDFUNC

PROC SET_DART_THROW(DART& Darts, VECTOR vThrow)
	Darts.vThrow = vThrow
ENDPROC

FUNC VECTOR GET_DART_TARGET(DART& Darts)
	RETURN Darts.vTarget
ENDFUNC

PROC SET_DART_TARGET(DART& Darts, VECTOR vTarget)
	Darts.vTarget = vTarget
ENDPROC

FUNC VECTOR GET_DART_OFFSETTARGET(DART& Darts)
	RETURN Darts.vOffsetTarget
ENDFUNC

PROC SET_DART_OFFSETTARGET(DART& Darts, VECTOR vOffsetTarget)
	Darts.vOffsetTarget = vOffsetTarget
ENDPROC

FUNC VECTOR GET_DART_RANDOMTARGET(DART& Darts)
	RETURN Darts.vRandomTarget
ENDFUNC

PROC SET_DART_RANDOMTARGET(DART& Darts, VECTOR vRandomTarget)
	Darts.vRandomTarget = vRandomTarget
ENDPROC

FUNC VECTOR GET_DART_ROTATION(DART& Darts)
	RETURN Darts.vRotation
ENDFUNC

PROC SET_DART_ROTATION(DART& Darts, VECTOR vRotation)
	Darts.vRotation = vRotation
ENDPROC
