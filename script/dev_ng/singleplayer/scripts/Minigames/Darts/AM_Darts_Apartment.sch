USING "commands_network.sch"
USING "Darts_Dart.sch"
USING "rc_helper_functions.sch"
USING "darts_board_lib.sch"
USING "Apartment_Minigame_General.sch"

	CONST_INT CONST_MC_POINTS_FOR_WIN 5
	CONST_FLOAT CONST_ANIM_PLAY_DELAY 0.7
  	CONST_INT END_FADE_IN_TIME 500
 	CONST_INT MAXPLAYERS 16
	CONST_FLOAT CONST_DELAY_GAME_VIEW 3.0
	CONST_INT CONST_SPEC_DART_NUM 3
	CONST_FLOAT COSNT_TIME_BEFORE_GAME_TRANSITION 15.0
	  ENUM PLAYER_TYPE
	  		NORMAL, 
			HOST, 
			SPEC
	  ENDENUM
  
  STRUCT STRUCT_ANIM_INFO
  		FLOAT 	fCurrentPhase
		FLOAT	fPrevPhase
		BOOL	bAllowThrow 	= TRUE
		BOOL	bAllowChoose	= TRUE
  ENDSTRUCT
  
  FUNC PED_BONETAG GET_BONETAG_DART_HAND(BOOL bRightHand = TRUE)
  	IF bRightHand
		RETURN BONETAG_PH_R_HAND
	ENDIF
	RETURN BONETAG_PH_L_HAND 
  ENDFUNC
  
STRUCT STRUCT_PLAYER_INFO
	DART			PlayerDarts[CONST_SPEC_DART_NUM]
	INT 			iParticipantID
	STRING 			sPlayerName
	PED_INDEX 		pedID
	PLAYER_INDEX 	playerID
	INT 			iDartPlayerID
	BOOL			DartsSelected[CONST_SPEC_DART_NUM]
	INT				iCurrentDart = 0
	OBJECT_INDEX	oHandBlocker
	STRUCT_ANIM_INFO	sThrowInfo
	VECTOR			vHandBlockerLocation
ENDSTRUCT
  
  
FUNC STRING PLAYER_TYPE_ENUM_STRING(PLAYER_TYPE ePlayerType)
  	SWITCH (ePlayerType)
		CASE NORMAL RETURN "NORMAL"
		CASE HOST 	RETURN "HOST"
		CASE SPEC	RETURN "SPECTATOR"
	ENDSWITCH
	  RETURN "INVALID"
ENDFUNC
  
FUNC STRING GET_MPSTATE_ENUM_STRING(DARTS_MP_STATE eCurrentState)
	SWITCH eCurrentState
		CASE DARTS_MPSTATE_INVALID RETURN " DARTS_MPSTATE_INVALID "
		CASE DARTS_MPSTATE_WAIT_PRE_INIT RETURN " DARTS_MPSTATE_WAIT_PRE_INIT "
		CASE DARTS_MPSTATE_PRE_INIT RETURN " DARTS_MPSTATE_PRE_INIT "
		CASE DARTS_MPSTATE_PRE_INIT_WAIT RETURN " DARTS_MPSTATE_PRE_INIT_WAIT "
		CASE DARTS_MPSTATE_INIT RETURN " DARTS_MPSTATE_INIT "
		CASE DARTS_MPSTATE_STREAMING RETURN " DARTS_MPSTATE_STREAMING "
		CASE DARTS_MPSTATE_SETUP RETURN " DARTS_MPSTATE_SETUP " 
		CASE DARTS_MPSTATE_READY_UP RETURN " DARTS_MPSTATE_READY_UP "
		CASE DARTS_MPSTATE_TUTORIAL RETURN " DARTS_MPSTATE_TUTORIAL " 
		CASE DARTS_MPSTATE_INTRO_SYNC_SETUP RETURN " DARTS_MPSTATE_INTRO_SYNC_SETUP "
		CASE DARTS_MPSTATE_INTRO_SYNC_PLAY RETURN " DARTS_MPSTATE_INTRO_SYNC_PLAY "
		CASE DARTS_MPSTATE_THROW_OFF_SETUP RETURN " DARTS_MPSTATE_THROW_OFF_SETUP "
		CASE DARTS_MPSTATE_THROW_OFF RETURN " DARTS_MPSTATE_THROW_OFF "
		CASE DARTS_MPSTATE_SYNC_OBJECTS RETURN " DARTS_MPSTATE_SYNC_OBJECTS "
		CASE DARTS_MPSTATE_SETUP_POST RETURN " DARTS_MPSTATE_SETUP_POST "
		CASE DARTS_MPSTATE_WAIT_POST_MENU RETURN " DARTS_MPSTATE_WAIT_POST_MENU "
		CASE DARTS_MPSTATE_IN_PROGRESS RETURN " DARTS_MPSTATE_IN_PROGRESS "	 	
		CASE DARTS_MPSTATE_GAME_TRANSITION RETURN " DARTS_MPSTATE_GAME_TRANSITION "
		CASE DARTS_MPSTATE_GAME_END	RETURN " DARTS_MPSTATE_GAME_END "	
		CASE DARTS_MPSTATE_WAIT_POST_GAME RETURN " DARTS_MPSTATE_WAIT_POST_GAME "	
		CASE DARTS_MPSTATE_TERMINATE_SPLASH RETURN " DARTS_MPSTATE_TERMINATE_SPLASH "
		CASE DARTS_MPSTATE_LEAVE RETURN " DARTS_MPSTATE_LEAVE "
		CASE DARTS_MPSTATE_FAILED RETURN " DARTS_MPSTATE_FAILED "
		CASE DARTS_MPSTATE_END RETURN " DARTS_MPSTATE_END " 
	  ENDSWITCH
	RETURN " ~~~INVALID STATE~~~ "
ENDFUNC

FUNC STRING GET_DARTS_MP_TURN_ENUM_STRING(DARTS_MP_TURN_STATUS eCurrentState)
	SWITCH eCurrentState
		CASE DARTS_MPTURN_THROWING RETURN " DARTS_MPTURN_THROWING "
		CASE DARTS_MPTURN_WAITING RETURN " DARTS_MPTURN_WAITING "
		CASE DARTS_MPTURN_SCORE_CHECK RETURN " DARTS_MPTURN_SCORE_CHECK "
		CASE DARTS_MPTURN_WINNING RETURN " DARTS_MPTURN_WINNING "
	  ENDSWITCH
	RETURN " ~~~INVALID STATE~~~ "
ENDFUNC
  
FUNC STRING GET_DARTS_MP_THROW_ENUM_STRING(DARTS_MP_THROW_STAGE eCurrentState)
	SWITCH eCurrentState
		CASE DARTS_MPTHROW_AIM RETURN " DARTS_MPTHROW_AIM "
		CASE DARTS_MPTHROW_THROW RETURN " DARTS_MPTHROW_THROW "
		CASE DARTS_MPTHROW_SCORE RETURN " DARTS_MPTHROW_SCORE "
		CASE DARTS_MPTHROW_TURN_CHANGE RETURN " DARTS_MPTHROW_TURN_CHANGE "
	  ENDSWITCH
	RETURN " ~~~INVALID STATE~~~ "
ENDFUNC

PROC SWITCH_MPSTATE(DARTS_MP_STATE &StateVariable, DARTS_MP_STATE newState, PLAYER_TYPE	pType = NORMAL)
  	IF StateVariable <> newState
		CDEBUG1LN(DEBUG_DARTS, " ")
		CDEBUG1LN(DEBUG_DARTS, " ")
	  	CDEBUG1LN(DEBUG_DARTS, " ######################### ")
		STRING sPlayerType = PLAYER_TYPE_ENUM_STRING(pType)
		CDEBUG1LN(DEBUG_DARTS, "SWITCHING ",sPlayerType , " MPSTATE FROM:", GET_MPSTATE_ENUM_STRING(StateVariable), "TO:", GET_MPSTATE_ENUM_STRING(newState))
		StateVariable = newState
		CDEBUG1LN(DEBUG_DARTS, "CURRENT ", sPlayerType, " MPSTATE:",GET_MPSTATE_ENUM_STRING(newState))

		CDEBUG1LN(DEBUG_DARTS, " VVVVVVVVVVVVVVVVVVVVVVVVV ")
	ENDIF
ENDPROC

PROC SWITCH_MPTURN_STATE(DARTS_MP_TURN_STATUS &StateVariable, DARTS_MP_TURN_STATUS newState, BOOL bIsServer = FALSE)
	  	CDEBUG1LN(DEBUG_DARTS, " ")
	CDEBUG1LN(DEBUG_DARTS, " ")
  	CDEBUG1LN(DEBUG_DARTS, " &&&&&&&&&&&&& ")
	IF bIsServer
		CDEBUG1LN(DEBUG_DARTS, "SWITCHING SERVER MP TURN STATE FROM:", GET_DARTS_MP_TURN_ENUM_STRING(StateVariable), "TO:", GET_DARTS_MP_TURN_ENUM_STRING(newState))
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "SWITCHING CLIENT MP TURN STATE FROM:", GET_DARTS_MP_TURN_ENUM_STRING(StateVariable), "TO:", GET_DARTS_MP_TURN_ENUM_STRING(newState))
	ENDIF
	StateVariable = newState
	IF bIsServer
		CDEBUG1LN(DEBUG_DARTS, "CURRENT SERVER MP TURN STATE:",GET_DARTS_MP_TURN_ENUM_STRING(newState))
	ELSE
	CDEBUG1LN(DEBUG_DARTS, "CURRENT CLIENT MP TURN STATE:",GET_DARTS_MP_TURN_ENUM_STRING(newState))
		ENDIF
	CDEBUG1LN(DEBUG_DARTS, " VVVVVVVVVVVVVV ")
ENDPROC

PROC SWITCH_MPTHROW_STATE(DARTS_MP_THROW_STAGE &StateVariable, DARTS_MP_THROW_STAGE newState, BOOL bIsServer = FALSE)
	  	CDEBUG1LN(DEBUG_DARTS, " ")
	CDEBUG1LN(DEBUG_DARTS, " ")
  	CDEBUG1LN(DEBUG_DARTS, " ******* ")
	IF bIsServer
		CDEBUG1LN(DEBUG_DARTS, "SWITCHING SERVER MP TURN STATE FROM:", GET_DARTS_MP_THROW_ENUM_STRING(StateVariable), "TO:", GET_DARTS_MP_THROW_ENUM_STRING(newState))
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "SWITCHING CLIENT MP TURN STATE FROM:", GET_DARTS_MP_THROW_ENUM_STRING(StateVariable), "TO:", GET_DARTS_MP_THROW_ENUM_STRING(newState))
	ENDIF
	StateVariable = newState
	IF bIsServer
		CDEBUG1LN(DEBUG_DARTS, "CURRENT SERVER MP TURN STATE:",GET_DARTS_MP_THROW_ENUM_STRING(newState))
	ELSE
	CDEBUG1LN(DEBUG_DARTS, "CURRENT CLIENT MP TURN STATE:",GET_DARTS_MP_THROW_ENUM_STRING(newState))
		ENDIF
	CDEBUG1LN(DEBUG_DARTS, " VVVVVVVVV ")
ENDPROC


FUNC STRING GET_DARTS_MP_LEAVE_ENUM_STRING(DARTS_MP_LEAVE_STATE eCurrentState)
	SWITCH eCurrentState
		CASE DARTS_MPLEAVESTATE_BIG_MESSAGE RETURN " DARTS_MPLEAVESTATE_BIG_MESSAGE "
		CASE DARTS_MPLEAVESTATE_SWOOP_UP RETURN " DARTS_MPLEAVESTATE_SWOOP_UP "
		CASE DARTS_MPLEAVESTATE_LEADERBOARD RETURN " DARTS_MPLEAVESTATE_LEADERBOARD "
		CASE DARTS_MPLEAVESTATE_OUT_ANIM RETURN " DARTS_MPLEAVESTATE_OUT_ANIM "
	  ENDSWITCH
	RETURN " ~~~INVALID STATE~~~ "
ENDFUNC

PROC SWITCH_MPLEAVE_STATE(DARTS_MP_LEAVE_STATE &StateVariable, DARTS_MP_LEAVE_STATE newState, BOOL bIsServer = FALSE)
	IF StateVariable <> newState
		CDEBUG1LN(DEBUG_DARTS, " ")
		CDEBUG1LN(DEBUG_DARTS, " ")
	  	CDEBUG1LN(DEBUG_DARTS, "LLLLLLLLLLLLLLLLLLLLLL")
		IF bIsServer
			CDEBUG1LN(DEBUG_DARTS, "SWITCHING SERVER MP LEAVE STATE FROM:", GET_DARTS_MP_LEAVE_ENUM_STRING(StateVariable), "TO:", GET_DARTS_MP_LEAVE_ENUM_STRING(newState))
		ELSE
			CDEBUG1LN(DEBUG_DARTS, "SWITCHING CLIENT MP LEAVE STATE FROM:", GET_DARTS_MP_LEAVE_ENUM_STRING(StateVariable), "TO:", GET_DARTS_MP_LEAVE_ENUM_STRING(newState))
		ENDIF
		StateVariable = newState
		IF bIsServer
			CDEBUG1LN(DEBUG_DARTS, "CURRENT SERVER MP LEAVE STATE:", GET_DARTS_MP_LEAVE_ENUM_STRING(newState))
		ELSE
		CDEBUG1LN(DEBUG_DARTS, "CURRENT CLIENT MP LEAVE STATE:", GET_DARTS_MP_LEAVE_ENUM_STRING(newState))
			ENDIF
		CDEBUG1LN(DEBUG_DARTS, "VVVVVVVVVVVVVVVVVVVVVV")
	ENDIF
ENDPROC

STRUCT TRANSFORM 
	VECTOR Position 
	VECTOR Rotation
	VECTOR Scale
ENDSTRUCT

STRUCT SYNCSCENE_SETUP
	TRANSFORM tMPScene
	TRANSFORM tMPSceneEyefinity
ENDSTRUCT	

STRUCT CAMERA_SETUP
	VECTOR 	vPosition
	VECTOR	vRotation
	FLOAT	fFOV
ENDSTRUCT


FUNC BOOL CREATE_PERSONAL_CLONE(PLAYER_INDEX piToClone, PED_INDEX &pedCloneRef)
	
	PED_INDEX pedToClone = GET_PLAYER_PED(piToClone)
	IF NOT DOES_ENTITY_EXIST(pedCloneRef)
		pedCloneRef = CLONE_PED(pedToClone, FALSE, FALSE)
	ENDIF	
	
	IF DOES_ENTITY_EXIST(pedCloneRef)
		//MP_OUTFITS_APPLY_DATA 	sApplyData
		//sApplyData.pedID = pedCloneRef
		//sApplyData.eApplyStage = AOS_SET  

		SET_ENTITY_VISIBLE(pedCloneRef, FALSE)
		
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(piToClone)
			CDEBUG1LN(DEBUG_DARTS, "CREATE_PERSONAL_CLONE - Player is a member of a biker gang")
			IF GB_OUTFITS_GD_GET_BIKER_EMBLEM_TO_DISPLAY(piToClone) = 2
				CDEBUG1LN(DEBUG_DARTS, "CREATE_PERSONAL_CLONE - Biker emblem to display is 2")
				IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(piToClone)
					CDEBUG1LN(DEBUG_DARTS, "CREATE_PERSONAL_CLONE - Boss of a biker gang")
				//	sApplyData.eOutfit = GB_OUTFITS_GET_BOSS_OUTFIT_FROM_STYLE(GB_OUTFITS_GD_GET_BOSS_STYLE_FROM_GOON(piToClone))
				ELSE
					CDEBUG1LN(DEBUG_DARTS, "CREATE_PERSONAL_CLONE - Member of a biker gang")
				//	sApplyData.eOutfit = GB_OUTFITS_BD_GET_THIS_GOON_OUTFIT(piToClone)
				ENDIF
				
			//	SET_PED_MP_OUTFIT(sApplyData, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, GB_OUTFITS_GD_GET_BIKER_EMBLEM_TO_DISPLAY(piToClone), OUTFIT_ROLE_GB_PRESIDENT)
				//texture apply based on the clone, ovveride ped based.
				REQUEST_STREAMED_TEXTURE_DICT(GET_CUSTOM_CREW_LOGO_TXD_NAME_FOR_PLAYER(piToClone))
				IF OVERRIDE_PED_CREW_LOGO_TEXTURE(pedCloneRef, GET_CUSTOM_CREW_LOGO_TXD_NAME_FOR_PLAYER(piToClone), GET_CUSTOM_CREW_LOGO_TXD_TEXTURE_FOR_PLAYER(piToClone, CUSTOM_CREW_LOGO_PRESET))
					CPRINTLN(DEBUG_MISSION,"CREATE_PERSONAL_CLONE - PRESET OVERRIDED: ", GET_CUSTOM_CREW_LOGO_TXD_TEXTURE_FOR_PLAYER(piToClone, CUSTOM_CREW_LOGO_PRESET))
				ELSE
					CPRINTLN(DEBUG_MISSION,"CREATE_PERSONAL_CLONE - PRESET FAILED texture Name: ", GET_CUSTOM_CREW_LOGO_TXD_TEXTURE_FOR_PLAYER(piToClone, CUSTOM_CREW_LOGO_PRESET))
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_DARTS, "CREATE_PERSONAL_CLONE - Biker emblem to display is NOT 2")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_DARTS, "CREATE_PERSONAL_CLONE - Player is not a member of a biker gang")
		ENDIF	
		
		IF NOT IS_PED_INJURED(pedCloneRef)
			IF IS_PED_WEARING_A_HELMET(pedToClone)
				SET_PED_HELMET(pedCloneRef, TRUE)
			ELSE
				SET_PED_HELMET(pedCloneRef, FALSE)
				REMOVE_PED_HELMET(pedCloneRef, TRUE)
				IF  GET_PED_PROP_INDEX(pedCloneRef, ANCHOR_EYES) = 26	// biker pack MC style Razer Punk style's glasses 
				OR  GET_PED_PROP_INDEX(pedToClone, ANCHOR_EYES) = 26
					CLEAR_PED_PROP(pedCloneRef, ANCHOR_EYES)
				ENDIF	
			ENDIF	
		ENDIF	
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

//Toggle for when all players in the session shoudl be hidden and would not interfiere with the gameplay , walking in fron of tha game board.
PROC TOGGLE_SESSION_PLAYERS_VISIBILITY(BOOL bAllPlayersVisible)

	INT iMaxParticipants
	iMaxParticipants = NETWORK_GET_MAX_NUM_PARTICIPANTS()
	INT iParticipant
	FOR iParticipant = 0 TO iMaxParticipants
		//Make sure player is active before updating server info
		PARTICIPANT_INDEX piCurrent = INT_TO_PARTICIPANTINDEX(iParticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piCurrent)
			IF bAllPlayersVisible
				SET_PLAYER_VISIBLE_LOCALLY(piCurrent)
			ELSE
				SET_PLAYER_INVISIBLE_LOCALLY(piCurrent)
			ENDIF
		ENDIF
	ENDFOR
  ENDPROC

PROC PLAYER_HOLSTER_WEAPON()
	IF GET_PLAYER_CURRENT_HELD_WEAPON() <> WEAPONTYPE_UNARMED
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		CDEBUG1LN(DEBUG_DARTS, "Weapon Holstered")
	ENDIF
ENDPROC

PROC PLAYER_TOGGLE_CONTROLS(BOOL bTurnOn)
	IF bTurnOn
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CDEBUG1LN(DEBUG_DARTS, "Player controlls turned on")
	ELSE
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			CDEBUG1LN(DEBUG_DARTS, "Player controlls turned off")
		ENDIF
	ENDIF
ENDPROC

STRUCT DARTS_LOCATION_INFO
	VECTOR	vBoardPos
	FLOAT	vBoardHead
	TRANSFORM 	tScoreBoard
	TRANSFORM	tIntroScene
	CAMERA_SETUP	csQuitCam
	
	SYNCSCENE_SETUP ssTransition
	SYNCSCENE_SETUP ssOutro
  ENDSTRUCT 
  
ENUM DARTS_APARTMENT_TYPE
	BIKER_VARIANT_A,
	BIKER_VARIANT_B
	, APT_TYPE_ARCADE
	#IF FEATURE_DLC_2_2022
	, APT_TYPE_JUGGALO_HIDEOUT
	#ENDIF
ENDENUM

/// DARTS APARTMENT MOD
//Stores all possible MP darts playing lcoation setups.
PROC GET_DARTS_LOCATION_INFO(DARTS_LOCATION_INFO& locationInfoOut, DARTS_APARTMENT_TYPE location)
	SWITCH location
	
		CASE BIKER_VARIANT_A 
			locationInfoOut.vBoardPos = <<1119.8805,-3157.0261,-36.3397>>
			locationInfoOut.vBoardHead = -90
			//scoreboard
			locationInfoOut.tScoreBoard.Position = <<1119.811,-3157.026,-36.340>>
			locationInfoOut.tScoreBoard.Rotation = <<9.1023, -0.0000, -90.7620>>
			locationInfoOut.tScoreBoard.Scale =	<< 1.820, 1.328, 1.0 >>
			//MP INtro sync scene setup
			locationInfoOut.tIntroScene.Position =  <<1119.024, -3157.138, -36.62417>>
			locationInfoOut.tIntroScene.Rotation =	<< 0, 0, -63.360>>
			//??? Spectator Stuff
			locationInfoOut.ssTransition.tMPScene.Position =  <<1119.7642, -3155.5112, -36.1562>>
			locationInfoOut.ssTransition.tMPScene.Rotation = <<-9.4800, -0.0000, 121.5339>>
			locationInfoOut.ssTransition.tMPSceneEyefinity.Position = <<1105.4803, -3155.6680, -38.0624>>
			locationInfoOut.ssTransition.tMPSceneEyefinity.Rotation = <<9.1023, -0.0000, -90.7620>>
			//Leave Mp Cam Setup
			locationInfoOut.csQuitCam.vPosition = <<1107.1398, -3155.7478, -36.2049>> 
			locationInfoOut.csQuitCam.vRotation = <<-4.3824, -0.0000, 88.2424>>
			locationInfoOut.csQuitCam.fFOV = 50.0
		BREAK
		
		CASE BIKER_VARIANT_B
			locationInfoOut.vBoardPos = <<1000.9350,-3162.8154,-33.4827>>
			locationInfoOut.vBoardHead = 0
			//scoreboard
			locationInfoOut.tScoreBoard.Position = <<1003.9058, -3162.8079, -33.4827>>
			locationInfoOut.tScoreBoard.Rotation = << 9.1023, 0.0, 0 >>
			locationInfoOut.tScoreBoard.Scale =	<< 1.820, 1.328, 1.0 >>
			//MP INtro sync scene setup
			locationInfoOut.tIntroScene.Position =  <<1003.857, -3163.348, -33.66635>>
			locationInfoOut.tIntroScene.Rotation =	<< 0, 0, 4.320>>
			//??? Spectator Stuff
			locationInfoOut.ssTransition.tMPScene.Position =  <<1003.6043, -3166.1809, -35.0826>>
			locationInfoOut.ssTransition.tMPScene.Rotation = <<9.1023, -0.0000, -90.7620>>
			locationInfoOut.ssTransition.tMPSceneEyefinity.Position = <<1003.6043, -3166.1809, -35.0826>>
			locationInfoOut.ssTransition.tMPSceneEyefinity.Rotation = <<9.1023, -0.0000, -90.7620>>
			//Leave Mp Cam Setup
			locationInfoOut.csQuitCam.vPosition = <<1003.8449, -3165.8247, -33.4828>>
			locationInfoOut.csQuitCam.vRotation = <<-0.0504, 0.0000, -0.6806>>
			locationInfoOut.csQuitCam.fFOV = 50.0
			
		BREAK
		
		CASE APT_TYPE_ARCADE
			locationInfoOut.vBoardPos = <<2708.5547, -357.6672, -54.4704>>
			locationInfoOut.vBoardHead = 0.0
			//scoreboard
			locationInfoOut.tScoreBoard.Position = <<2711.5255, -357.6597, -54.4704>>
			locationInfoOut.tScoreBoard.Rotation = <<9.1023, 0.0, 0.0>>
			locationInfoOut.tScoreBoard.Scale =	<<1.820, 1.328, 1.0>>
			//MP INtro sync scene setup
			locationInfoOut.tIntroScene.Position =  <<2711.4767, -358.1998, -54.65405>>
			locationInfoOut.tIntroScene.Rotation =	<<0.0, 0.0, 4.320>>
			//??? Spectator Stuff
			locationInfoOut.ssTransition.tMPScene.Position =  <<2711.224, -361.0327, -56.0703>>
			locationInfoOut.ssTransition.tMPScene.Rotation = <<9.1023, 0.0, -90.7620>>
			locationInfoOut.ssTransition.tMPSceneEyefinity.Position = <<2711.224, -361.0327, -56.0703>>
			locationInfoOut.ssTransition.tMPSceneEyefinity.Rotation = <<9.1023, 0.0, -90.7620>>
			//Leave Mp Cam Setup
			locationInfoOut.csQuitCam.vPosition = <<2711.4646, -360.6765, -54.4705>>
			locationInfoOut.csQuitCam.vRotation = <<-0.0504, 0.0, -0.6806>>
			locationInfoOut.csQuitCam.fFOV = 50.0
		BREAK
		
		#IF FEATURE_DLC_2_2022
		CASE APT_TYPE_JUGGALO_HIDEOUT
			locationInfoOut.vBoardPos = <<556.5533, -421.0700, -68.9403>>
			locationInfoOut.vBoardHead = 180.0
			//scoreboard
			locationInfoOut.tScoreBoard.Position = <<556.5533, -421.0700, -68.9403>>
			locationInfoOut.tScoreBoard.Rotation = <<0.0, 0.0, 90.0>>
			locationInfoOut.tScoreBoard.Scale =	<<1.820, 1.328, 1.0>>
			//MP INtro sync scene setup
			locationInfoOut.tIntroScene.Position =  <<556.5750, -420.1195, -70.6348>>
			locationInfoOut.tIntroScene.Rotation =	<<0.0, 0.0, 180.0>>
			//??? Spectator Stuff
			locationInfoOut.ssTransition.tMPScene.Position =  <<556.5750, -420.1195, -70.6348>>
			locationInfoOut.ssTransition.tMPScene.Rotation = <<0.0, 0.0, 180.0>>
			locationInfoOut.ssTransition.tMPSceneEyefinity.Position = <<560.8927, -419.1003, -67.8403>>
			locationInfoOut.ssTransition.tMPSceneEyefinity.Rotation = <<0.0, 0.0, 180.0>>
			//Leave Mp Cam Setup
			locationInfoOut.csQuitCam.vPosition = <<560.8927, -419.1003, -67.8403>>
			locationInfoOut.csQuitCam.vRotation = <<0.0, 0.0, 180.0>>
			locationInfoOut.csQuitCam.fFOV = 50.0
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC


CONST_INT	DARTS_PLAYER_ONE 0
//due to how script is written , and make less chcanges, player two id will be chcanged to player 1 (0) when in singlepalyer..
INT	DARTS_PLAYER_TWO = 1

FUNC INT NETID_TO_DARTID(INT &iDartPlayers[2], INT iNetworkInt)
	INT iDartID
	INT iReturnID
	iReturnID = 0
	BOOL bIDFound
	bIDFound = FALSE
	
	FOR iDartID = 0 TO 1
		IF iDartPlayers[iDartID] >= 0
			IF iDartPlayers[iDartID] = iNetworkInt
				IF bIDFound
					CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS PLAYER ID] - TRYING TO SET ID AGAIN")
				ENDIF
				iReturnID = iDartID
				bIDFound = TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT bIDFound
		CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS PLAYER ID] - ID WAS NOT FOUND!")
	ENDIF
	RETURN iReturnID
ENDFUNC

//Returns the oponent player index [0-1 , DARTS_PLAYER_ONE, DARTS_PLAYER_TWO]
FUNC INT GET_OPPONENT_ID(INT &iDartPlayers[2], INT iNetworkInt)
	INT iCurrentPlayerID = NETID_TO_DARTID(iDartPlayers, iNetworkInt)
	INT iOponentID
	iOponentID = 0
		SWITCH iCurrentPlayerID
			CASE 0 iOponentID =  1 BREAK
			CASE 1 iOponentID =  0 BREAK
		ENDSWITCH
	RETURN iOponentID
ENDFUNC

//Returns the oponent player index [0-1 , DARTS_PLAYER_ONE, DARTS_PLAYER_TWO]
FUNC INT GET_DARTID_TO_NETID(INT &iDartPlayers[2], INT iDartPlayerID)
	RETURN iDartPlayers[iDartPlayerID]
ENDFUNC

//FUNC INT DARTID_TO_NETID(INT &iDartPlayers[2], INT iDartPlayerID)
//
//
//ENDFUNC


FUNC INT PLAYERID_TO_SCOREID(INT iPlayerID)
	//if palyer DARTS_PLAYER_ONE(0) -> then score id is 1.
	SWITCH iPlayerID
		CASE 0 RETURN 1
		CASE 1 RETURN 0
	ENDSWITCH
	RETURN 1
ENDFUNC

//ENUM EVENT_TYPE_ENUM
// 	SETUP_INFO,
//	DART_THROWN,
//	DART_THROW_RESET
// ENDENUM
 
//STRUCT STRUCT_CLIENT_SETUP
//	EVENT_TYPE_ENUM eInfo
//	INT	iLegs
//	INT	iSets
//ENDSTRUCT

/// PURPOSE: DART EVENTS RECEIVE / SEND
STRUCT STRUCT_DART_RECEIVE
	BOOL	bThrow
	VECTOR  vOffsetTarget
	BOOL	bResetDone
	BOOL	bIsHost
	BOOL	bNextPlayer
	INT		iThrower
	INT		iPlayerScore
ENDSTRUCT

STRUCT STRUCT_DART_SEND
	STRUCT_EVENT_COMMON_DETAILS Details
	VECTOR  vOffsetTarget
	BOOL	bIsHost
	INT		iThrower
	INT		iPlayerScore
ENDSTRUCT

//PROC EVENT_SEND_DART_INFO(STRUCT_DART_SEND &sDartEventInfo)
//	IF sDartEventInfo.Details.Type = SCRIPT_EVENT_MG_DART_THROW
//		CDEBUG1LN(DEBUG_DARTS, "Throw Sent")
//	ENDIF
//	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sDartEventInfo, SIZE_OF(STRUCT_DART_SEND), ALL_PLAYERS())
//ENDPROC

PROC EVENT_SEND_DART_INFO_THROW(VECTOR vOffsetTarget)
	STRUCT_DART_SEND sDartEventInfo
	sDartEventInfo.Details.Type = SCRIPT_EVENT_MG_DART_THROW
	sDartEventInfo.Details.FromPlayerIndex = PLAYER_ID()
	sDartEventInfo.vOffsetTarget = vOffsetTarget
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sDartEventInfo, SIZE_OF(STRUCT_DART_SEND), ALL_PLAYERS())
	CDEBUG1LN(DEBUG_DARTS, "Throw Sent")
ENDPROC

PROC EVENT_SEND_DART_INFO_RESET()
	STRUCT_DART_SEND sDartEventInfo
	sDartEventInfo.Details.Type = SCRIPT_EVENT_MG_DART_RESET
	sDartEventInfo.Details.FromPlayerIndex = PLAYER_ID()
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sDartEventInfo, SIZE_OF(STRUCT_DART_SEND), ALL_PLAYERS())
	CDEBUG1LN(DEBUG_DARTS, "Throw Sent")
ENDPROC

PROC EVENT_SEND_DART_SET_FINISHED(INT iThrower, INT iCurrPlayerScore)
	CDEBUG1LN(DEBUG_DARTS, "EVENT SWITCH TO NEXT PLAYER.")
	STRUCT_DART_SEND sDartEventInfo
	sDartEventInfo.Details.Type = SCRIPT_EVENT_MG_DART_NEXT_PLAYER
	sDartEventInfo.Details.FromPlayerIndex = PLAYER_ID()
	sDartEventInfo.iThrower = iThrower
	sDartEventInfo.iPlayerScore = iCurrPlayerScore

	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sDartEventInfo, SIZE_OF(STRUCT_DART_SEND), ALL_PLAYERS())
ENDPROC

FUNC BOOL EVENT_CATCH_DART_INFO(INT eventID, STRUCT_DART_RECEIVE &DartReceiveInfo)
	BOOL bReturn
	bReturn = FALSE
	STRUCT_DART_SEND sCatchEventInfo
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, eventID, sCatchEventInfo, SIZE_OF(STRUCT_DART_SEND))
		SWITCH sCatchEventInfo.Details.Type
			CASE SCRIPT_EVENT_MG_DART_THROW
				CDEBUG1LN(DEBUG_DARTS, "RECEIVED DART THROW Internal ", DartReceiveInfo.vOffsetTarget)
				DartReceiveInfo.bThrow = TRUE
				DartReceiveInfo.vOffsetTarget = sCatchEventInfo.vOffsetTarget
				DartReceiveInfo.bIsHost = sCatchEventInfo.bIsHost
				bReturn = TRUE
			BREAK
			
			CASE SCRIPT_EVENT_MG_DART_RESET
				CDEBUG1LN(DEBUG_DARTS, "RECEIVED DART ResetDone Internal")
				DartReceiveInfo.bResetDone = TRUE
				bReturn = TRUE
			BREAK
			
			CASE SCRIPT_EVENT_MG_DART_NEXT_PLAYER
				CDEBUG1LN(DEBUG_DARTS, "RECEIVED DART NEXT PLAYER TURN")
				DartReceiveInfo.bNextPlayer = TRUE
				DartReceiveInfo.iPlayerScore = sCatchEventInfo.iPlayerScore
				DartReceiveInfo.iThrower = sCatchEventInfo.iThrower
				bReturn = TRUE
			BREAK
		ENDSWITCH
	ENDIF
	RETURN bReturn
ENDFUNC

//Hide spectator players for minigame players.
//Call each frame
PROC HIDE_SPEC_PLAYERS()
	INT idx
	PLAYER_INDEX		piCurrent
	REPEAT 32 idx
			piCurrent = INT_TO_NATIVE(PLAYER_INDEX, idx)
			IF NETWORK_IS_PLAYER_ACTIVE(piCurrent)			
				SET_PLAYER_INVISIBLE_LOCALLY(piCurrent)
			ENDIF
	ENDREPEAT			
ENDPROC

PROC SHOW_SPEC_PLAYERS()
	INT idx
	PARTICIPANT_INDEX 	participantID
	PLAYER_INDEX		piCurrent
	INT iNumParticipants = NETWORK_GET_MAX_NUM_PARTICIPANTS()
	REPEAT iNumParticipants idx
			participantID = INT_TO_PARTICIPANTINDEX( idx )
			IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
			piCurrent = NETWORK_GET_PLAYER_INDEX(participantID)
				SET_PLAYER_VISIBLE_LOCALLY(piCurrent)
			ENDIF
	ENDREPEAT			
ENDPROC

ENUM ENUM_PLAYER_STATE_FLAGS
	DARTS_CLIENTFLAG_STATE_DART_THROWN			= 1,
	DARTS_CLIENTFLAG_STATE_DART_AIM 			= 2,
	DARTS_CLIENTFLAG_STATE_DART_DONE_SCORING 	= 3,
	DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW	= 4,
	DARTS_CLIENTFLAG_STATE_DART_RESET_DONE		= 5,
	DARTS_CLIENTFLAG_STATE_DART_SWITCH_PLAYER	= 6,
	DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH		= 7,
	DARTS_CLIENTFLAG_STATE_DART_DO_QUIT			= 8
ENDENUM

FUNC BOOL GET_PLAYER_STATE(DARTS_AP_PLAYER_BROADCAST_DATA &playerData, ENUM_PLAYER_STATE_FLAGS eFlag)
	RETURN IS_BIT_SET(playerData.iPlayerStateFlags, ENUM_TO_INT(eFlag))
ENDFUNC

PROC TOGGLE_PLAYER_STATE(DARTS_AP_PLAYER_BROADCAST_DATA &playerData, ENUM_PLAYER_STATE_FLAGS eFlag, BOOL bActive)
	
	BOOL isBitSet
	isBitSet = IS_BIT_SET(playerData.iPlayerStateFlags, ENUM_TO_INT(eFlag))
	IF bActive
		IF NOT isBitSet
			SET_BIT(playerData.iPlayerStateFlags, ENUM_TO_INT(eFlag))
		ENDIF
	ELSE	
		IF isBitSet
			CLEAR_BIT(playerData.iPlayerStateFlags, ENUM_TO_INT(eFlag))
		ENDIF
	ENDIF
ENDPROC

PROC TOGGLE_ATTACH_DART_TO_HAND(OBJECT_INDEX &obj, PED_INDEX player, PED_BONETAG boneTag , BOOL bAttach = TRUE)
	IF bAttach
		IF IS_ENTITY_ALIVE(obj)
		AND NOT IS_PED_DEAD_OR_DYING(player)	
		
			INT iBone
			iBone = GET_PED_BONE_INDEX(player, boneTag)
			
			ATTACH_ENTITY_TO_ENTITY(obj, player, iBone, NULL_VECTOR(), NULL_VECTOR())									
		ENDIF
	ELSE
		DETACH_ENTITY(obj)
	ENDIF
ENDPROC

PROC INIT_PLAYER_POSITIONS(BOOL bPlayerSideA, DARTS_PLAYER_POS &DartsPlayerPos, DARTS_BOARD dBoard)
	IF bPlayerSideA
		CDEBUG1LN(DEBUG_DARTS, "PlayerPos AWAY: ", DartsPlayerPos.vDartPlayrPos)
		DartsPlayerPos.vDartPlayrPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(
		dBoard.vDartBoard, dBoard.fBoardHeading, << -0.35, -2.6 + fDartBoardOriginOffset, -1.7272 >>)
		
		CDEBUG1LN(DEBUG_DARTS, "PlayerPos 2: ", DartsPlayerPos.vOpponentPos)
		DartsPlayerPos.vOpponentPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(
		dBoard.vDartBoard, dBoard.fBoardHeading, << 0.35, -2.6 + fDartBoardOriginOffset, -1.7272 >>)
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "PlayerPos HOME: ", DartsPlayerPos.vDartPlayrPos)
		DartsPlayerPos.vDartPlayrPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(
		dBoard.vDartBoard, dBoard.fBoardHeading, << 0.35, -2.6 + fDartBoardOriginOffset, -1.7272 >>)
		
		CDEBUG1LN(DEBUG_DARTS, "PlayerPos 2: ", DartsPlayerPos.vOpponentPos)
		DartsPlayerPos.vOpponentPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(
		dBoard.vDartBoard, dBoard.fBoardHeading, << -0.35, -2.6 + fDartBoardOriginOffset, -1.7272 >>)
	ENDIF
ENDPROC

PROC TASK_MOVE_PLAYER_TO_PLAY_POSITION(SEQUENCE_INDEX &siTemp, PED_INDEX Ped, VECTOR DartsPlayerPos, VECTOR vBoardPos)
	
	FLOAT fHeadingToPlayBoard 
		fHeadingToPlayBoard = GET_HEADING_BETWEEN_VECTORS_2D(DartsPlayerPos,  vBoardPos)
		//BETTER USE SLIDE TO COORD INSIDE APARTMENT? apartments not always has navmesh?
		IF NOT IS_PED_INJURED(Ped)
			CLEAR_SEQUENCE_TASK(siTemp)
			OPEN_SEQUENCE_TASK(siTemp)
			TASK_GO_STRAIGHT_TO_COORD(NULL, DartsPlayerPos, PEDMOVEBLENDRATIO_WALK, 3000, DEFAULT_NAVMESH_FINAL_HEADING, 0.5)
			TASK_ACHIEVE_HEADING(NULL, fHeadingToPlayBoard)
			CLOSE_SEQUENCE_TASK(siTemp)
			TASK_PERFORM_SEQUENCE_LOCALLY(Ped, siTemp)
			CDEBUG1LN(DEBUG_DARTS, "Players tasked to get into position: ", DartsPlayerPos, " With heading: ", fHeadingToPlayBoard)
		ENDIF

		//POINT_CAM_AT_COORD(GET_RENDERING_CAM(), vBoardPos)
		//SET_GAMEPLAY_COORD_HINT(vBoardPos, 10000)
ENDPROC

PROC INITIALIZE_PLAYER_INFO(STRUCT_PLAYER_INFO &sPlayerInfo[DART_PLAYER_IDS])
	INT iPID
	INT iPlayerID
	INT iSize = ENUM_TO_INT(DART_PLAYER_IDS) - 1
	FOR iPlayerID = 0 TO iSize
		iPID = sPlayerInfo[iPlayerID].iParticipantID
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPID))
			sPlayerInfo[iPlayerID].iDartPlayerID = iPlayerID
			sPlayerInfo[iPlayerID].pedID = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPID))	)
			sPlayerInfo[iPlayerID].playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPID))
			sPlayerInfo[iPlayerID].sPlayerName = GET_PLAYER_NAME(sPlayerInfo[iPlayerID].playerID)
		ENDIF
	ENDFOR
ENDPROC

PROC PRINT_PLAYER_INFO(STRUCT_PLAYER_INFO sPlayerInfo)

	STRING sDartPlayerID = ""
	IF sPlayerInfo.iDartPlayerID = 0
		sDartPlayerID = "DART_PLAYER_ONE"
	ELIF sPlayerInfo.iDartPlayerID = 1
		sDartPlayerID = "DART_PLAYER_TWO"
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, 
	" playerName:", sPlayerInfo.sPlayerName, 
	" ParticipandID:", sPlayerInfo.iParticipantID,
	" playerDartID(ArrayPos):", sDartPlayerID)
ENDPROC



//### create darts once , reuse.
PROC DARTS_CREATE(DART &darts[CONST_SPEC_DART_NUM], MODEL_NAMES dartModel)
	VECTOR vDartPos
	INT iDartID
	FOR iDartID = 0 TO CONST_SPEC_DART_NUM - 1
		//offset darts so assert doesnt fire about obj in same place.
		vDartPos = <<1,1,1>>
		vDartPos.x += TO_FLOAT((iDartID * 10))
		darts[iDartID].object = CREATE_OBJECT(dartModel, vDartPos, FALSE, FALSE)
		SET_ENTITY_COLLISION(darts[iDartID].object, FALSE, FALSE)

		CDEBUG1LN(DEBUG_DARTS, "[DART SPEC CREATE- dart created Pos:", vDartPos)
	ENDFOR
ENDPROC

//### Position darts back into left hand.
PROC DARTS_HIDE(STRUCT_PLAYER_INFO &playerInfo)
	
	playerInfo.iCurrentDart = 0
	INT iDartID
	FOR iDartID = 0 TO CONST_SPEC_DART_NUM - 1
		IF DOES_ENTITY_EXIST(playerInfo.PlayerDarts[iDartID].object)
			SET_ENTITY_VISIBLE(playerInfo.PlayerDarts[iDartID].object, FALSE)
			CDEBUG1LN(DEBUG_DARTS, "[DART SPEC HIDE- dart hidden")
		ELSE
			CDEBUG1LN(DEBUG_DARTS, "[DART SPEC HIDE- dart does not exist!")
		ENDIF
	ENDFOR
ENDPROC

//### Position darts back into left hand.
PROC DARTS_RESET(STRUCT_PLAYER_INFO &playerInfo, PED_INDEX &player)
	
	playerInfo.iCurrentDart = 0
	INT iDartID
	FOR iDartID = 0 TO CONST_SPEC_DART_NUM - 1
		IF DOES_ENTITY_EXIST(playerInfo.PlayerDarts[iDartID].object)
			playerInfo.DartsSelected[iDartID] = FALSE
			playerInfo.PlayerDarts[iDartID].bStuck = FALSE
			playerInfo.PlayerDarts[iDartID].bTravelling = FALSE
			playerInfo.PlayerDarts[iDartID].bDoneScoring = FALSE
			TOGGLE_ATTACH_DART_TO_HAND(playerInfo.PlayerDarts[iDartID].object, player, GET_BONETAG_DART_HAND(FALSE), TRUE)
			CDEBUG1LN(DEBUG_DARTS, "[DART SPEC RESET- dart position was reset")
		ELSE
			CDEBUG1LN(DEBUG_DARTS, "[DART SPEC RESET- dart does not exist!")
		ENDIF
	ENDFOR
ENDPROC

//### attach dart to the other hand.
PROC DARTS_SELECT(STRUCT_PLAYER_INFO &playerInfo, PED_INDEX &player, INT iDartID)
	playerInfo.DartsSelected[iDartID] = TRUE
	IF DOES_ENTITY_EXIST(playerInfo.PlayerDarts[iDartID].object)
		TOGGLE_ATTACH_DART_TO_HAND(playerInfo.PlayerDarts[iDartID].object, 
		player, 
		GET_BONETAG_DART_HAND(), 
		TRUE)
	ENDIF
ENDPROC

PROC DART_RELEASE_HAND(STRUCT_PLAYER_INFO &playerInfo, VECTOR vTarget)// DART &RefDart)
	INT iCurrentDart = playerInfo.iCurrentDart
	IF DOES_ENTITY_EXIST(playerInfo.PlayerDarts[iCurrentDart].object)
		DETACH_ENTITY(playerInfo.PlayerDarts[iCurrentDart].object)
		playerInfo.PlayerDarts[iCurrentDart].bTravelling = TRUE
		playerInfo.PlayerDarts[iCurrentDart].vOffsetTarget = vTarget
	ENDIF
	playerInfo.iCurrentDart++
ENDPROC

FUNC BOOL DART_CLEANUP_DETACH(DART &darts[CONST_SPEC_DART_NUM])
	
	INT iNumDetached = 0
	INT iDartID
	FOR iDartID = 0 TO CONST_SPEC_DART_NUM - 1
		IF DOES_ENTITY_EXIST(darts[iDartID].object)
			IF IS_ENTITY_ATTACHED_TO_ANY_OBJECT(darts[iDartID].object)
				CDEBUG1LN(DEBUG_DARTS, "ATTACHED", iDartID)
				DETACH_ENTITY(darts[iDartID].object)
			ELSE
				DELETE_OBJECT(darts[iDartID].object)
				CDEBUG1LN(DEBUG_DARTS, "DETACHED", iDartID)
				iNumDetached++
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iNumDetached >= CONST_SPEC_DART_NUM
ENDFUNC


PROC DART_CLEANUP_DELETE(DART &darts[CONST_SPEC_DART_NUM])
	INT iDartID
	FOR iDartID = 0 TO CONST_SPEC_DART_NUM - 1
		IF DOES_ENTITY_EXIST(darts[iDartID].object)
			IF NOT IS_ENTITY_ATTACHED_TO_ANY_OBJECT(darts[iDartID].object)
				DELETE_OBJECT(darts[iDartID].object)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC


ENUM ENUM_CLEANUP_FLAGS
	eCLEANUP_FLAG_DONT_FADE_IN,
	eCLEANUP_FLAG_WAIT_FOR_FADED_OUT
ENDENUM

FUNC BOOL DARTS_ANIM_PHASE(PARTICIPANT_INDEX partID, STRING sAnimDict, STRING sAnimName, FLOAT &fPhase)

	PLAYER_INDEX piPLayer = NETWORK_GET_PLAYER_INDEX(partID)
	PED_INDEX pedPlayer = GET_PLAYER_PED(piPLayer)
//	
//	CDEBUG1LN(DEBUG_DARTS, 
//	"Anim:", sAnimName, 
//	" By?", GET_PLAYER_NAME(piPLayer),
//	" playing?", IS_ENTITY_PLAYING_ANIM(pedPlayer, sAnimDict, sAnimName)
//	)
	
	
	IF IS_ENTITY_ALIVE(pedPlayer)
		IF IS_ENTITY_PLAYING_ANIM(pedPlayer, sAnimDict, sAnimName)
			fPhase = GET_ENTITY_ANIM_CURRENT_TIME(pedPlayer, sAnimDict, sAnimName)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL THROW_DART_SPEC(DART & Darts, DARTS_BOARD & DartBoard)
	
	//CDEBUG1LN(DEBUG_DARTS, "DART THROWN SPECTATOR")
	IF NOT Darts.bStuck
	Darts.bStuck = TRUE
	CDEBUG1LN(DEBUG_DARTS, "DART THROWN INITIALIZED")
	
	VECTOR loc = GET_ENTITY_COORDS(Darts.object)
	CDEBUG1LN(DEBUG_DARTS, "DART THROWN INITIALIZED Dart Location:", loc)
		Darts.vTarget = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartBoard.vDartBoard, DartBoard.fBoardHeading,
																<<Darts.vOffsetTarget.x, 
																Darts.vOffsetTarget.y + fDartBoardOriginOffset, 
																Darts.vOffsetTarget.z>>)	
		CDEBUG1LN(DEBUG_DARTS, "DART THROWN Target Loc:", Darts.vTarget)
		
		//dsitance to target
		Darts.vRandomTarget = GET_ENTITY_COORDS(Darts.object) - Darts.vTarget
		Darts.vRandomTarget.x = GET_VECTOR_LENGTH(Darts.vRandomTarget)
		CDEBUG1LN(DEBUG_DARTS, "DART THROWN Distance:", Darts.vRandomTarget.x)
		
		//direction
		Darts.vThrow = Darts.vTarget - GET_ENTITY_COORDS(Darts.object)
		Darts.vThrow = NORMALISE_VECTOR(Darts.vThrow)
		CDEBUG1LN(DEBUG_DARTS, "DART THROWN Direction:", Darts.vThrow )
		
		//Angle between dart and end location
		//FLOAT AngleToTarget = GET_ANGLE_BETWEEN_2D_VECTORS(loc.x, loc.y, Darts.vTarget.x, Darts.vTarget.y)
		//CDEBUG1LN(DEBUG_DARTS, "DART THROWN ANGLE:", AngleToTarget )
		
		VECTOR vNewRotation
		vNewRotation =  
		<<  
		90.0 + GET_RANDOM_FLOAT_IN_RANGE(-9.0, 9.0), 
		GET_RANDOM_FLOAT_IN_RANGE(0.0, 90.0), 
		DartBoard.fBoardHeading + GET_RANDOM_FLOAT_IN_RANGE(-9.0, 9.0)
		>>
		SET_ENTITY_ROTATION(Darts.object, vNewRotation, EULER_XYZ)

	ENDIF
	
	IF Darts.bStuck
		IF Darts.vRandomTarget.x > 0
		
			VECTOR vNewOffset
			vNewOffset = Darts.vThrow * GET_FRAME_TIME() * DART_THROW_SPEED
			vNewOffset = GET_ENTITY_COORDS(Darts.object) + vNewOffset
			Darts.vRandomTarget.x -= GET_VECTOR_LENGTH(GET_ENTITY_COORDS(Darts.object) - vNewOffset)
			
			IF Darts.vRandomTarget.x <= 0
				//vNewOffset = Darts.vThrow * 0.02//GET_RANDOM_FLOAT_IN_RANGE(0.05, 0.15)
				//vNewOffset = GET_ENTITY_COORDS(Darts.object) + vNewOffset
				vNewOffset = Darts.vTarget + Darts.vThrow * GET_RANDOM_FLOAT_IN_RANGE(0, 0.005)
			ENDIF
			//CDEBUG1LN(DEBUG_DARTS, "DART THROWN Distance New:", Darts.vRandomTarget.x)
			SET_ENTITY_COORDS(Darts.object, vNewOffset)
		ELSE
			PLAY_SOUND_FROM_COORD(-1, "DLC_Biker_Darts_Hit_Board_Remote_Master", DartBoard.vDartBoard)
			RETURN TRUE
		ENDIF
	
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DART_GET_INTRO(STRUCT_PLAYER_INFO &DartPlayer)

	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(DartPlayer.iParticipantID))
		INT iCurrentDart = DartPlayer.iCurrentDart
		PED_INDEX piPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(DartPlayer.iParticipantID)))
		PARTICIPANT_INDEX partPlayer = INT_TO_PARTICIPANTINDEX(DartPlayer.iParticipantID)
		
		FLOAT fPhase = 0
		DARTS_ANIM_PHASE(partPlayer, "anim@amb@clubhouse@mini@darts@", "intro", fPhase)
		DARTS_ANIM_PHASE(partPlayer, "anim@amb@clubhouse@mini@darts@", "Intro_B", fPhase)
		DartPlayer.sThrowInfo.fCurrentPhase = fPhase
		
		//###Check if anim cycle finished.
		IF DartPlayer.sThrowInfo.fCurrentPhase < DartPlayer.sThrowInfo.fPrevPhase
			DartPlayer.sThrowInfo.bAllowThrow = TRUE
			DartPlayer.sThrowInfo.bAllowChoose = TRUE
			CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - ALLOW TO THROW AGAIN!", iCurrentDart)
		ENDIF
		
		IF iCurrentDart < CONST_SPEC_DART_NUM
			IF DartPlayer.sThrowInfo.bAllowChoose
				//### Check if Dart select Anim Is playing
				IF fPhase >= 0.17
				AND fPhase < 0.5

					IF NOT DartPlayer.DartsSelected[iCurrentDart]
						CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - NEW DART SELECTED!", iCurrentDart)
						//DartPlayer.DartsSelected[iCurrentDart] = TRUE
						DartPlayer.sThrowInfo.bAllowChoose = FALSE
						DARTS_SELECT(DartPlayer,  piPlayer, iCurrentDart)  
					ELSE
						CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - DART IS ALREADY SELECTED!", iCurrentDart)
					ENDIF
				ENDIF
			ELSE
				//CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - DART DONT ALLOW", iCurrentDart)
			ENDIF
			
		ENDIF		
		DartPlayer.sThrowInfo.fPrevPhase = fPhase
	ENDIF
ENDPROC

PROC DART_THROW_PROCESS(STRUCT_PLAYER_INFO &DartPlayer, DARTS_BOARD &dBoard, VECTOR vTarget)
	
	
	//Darts.vOffsetTarget = <<0.083, -0.23, -0.035>>
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(DartPlayer.iParticipantID))
		INT iCurrentDart = DartPlayer.iCurrentDart
		PED_INDEX piPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(DartPlayer.iParticipantID)))
		PARTICIPANT_INDEX partPlayer = INT_TO_PARTICIPANTINDEX(DartPlayer.iParticipantID)
		
		FLOAT fPhase = 0
		DARTS_ANIM_PHASE(partPlayer, "anim@amb@clubhouse@mini@darts@", "throw_overlay", fPhase)
		DartPlayer.sThrowInfo.fCurrentPhase = fPhase
		
		//###Check if anim cycle finished.
		IF DartPlayer.sThrowInfo.fCurrentPhase < DartPlayer.sThrowInfo.fPrevPhase
			DartPlayer.sThrowInfo.bAllowThrow = TRUE
			DartPlayer.sThrowInfo.bAllowChoose = TRUE
			CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - ALLOW TO THROW AGAIN!", iCurrentDart)
		ENDIF
		
		
		IF iCurrentDart >= CONST_SPEC_DART_NUM
			IF fPhase >= 0.6
			AND fPhase < 0.9
				//### make sure not out of bounds
				DARTS_RESET(DartPlayer, piPlayer)
				DartPlayer.sThrowInfo.bAllowThrow = TRUE
				DartPlayer.sThrowInfo.bAllowChoose = TRUE
				CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - RESET DARTS!", iCurrentDart)
			ENDIF
		ENDIF

		IF iCurrentDart < CONST_SPEC_DART_NUM
			IF DartPlayer.sThrowInfo.bAllowChoose
				//### Check if Dart select Anim Is playing
				IF fPhase >= 0.6
				AND fPhase < 0.9

					IF NOT DartPlayer.DartsSelected[iCurrentDart]
						CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - NEW DART SELECTED!", iCurrentDart)
						//DartPlayer.DartsSelected[iCurrentDart] = TRUE
						DartPlayer.sThrowInfo.bAllowChoose = FALSE
						DARTS_SELECT(DartPlayer,  piPlayer, iCurrentDart)  
					ELSE
						CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - DART IS ALREADY SELECTED!", iCurrentDart)
					ENDIF
				ENDIF
			ENDIF
			
			IF DartPlayer.sThrowInfo.bAllowThrow
				//### Check if Dart was thrown
				IF fPhase >= 0.04
				AND fPhase < 0.5
				//	IF DartPlayer.DartsSelected[iCurrentDart]
						IF NOT DartPlayer.PlayerDarts[iCurrentDart].bTravelling
							CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - DART RELEASED, BLOCK NEW THROW!", iCurrentDart)
							CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - DART THROW TARGET:", vTarget)
							DART_RELEASE_HAND(DartPlayer, vTarget)
							DartPlayer.sThrowInfo.bAllowThrow = FALSE
						ELSE
							CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - DART IS ALREADY TRAVELLING FOR THROW!", iCurrentDart)
						ENDIF
					//ELSE
						//CDEBUG3LN(DEBUG_DARTS, "ANIM PHASE DETECTED - DART IS NOT SELECTED FOR THROW!", iCurrentDart)
				//	ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//### Update released darts.
		INT iDart = 0
		REPEAT CONST_SPEC_DART_NUM iDart
			IF DOES_ENTITY_EXIST(DartPlayer.PlayerDarts[iDart].object)
				IF DartPlayer.PlayerDarts[iDart].bTravelling
					IF NOT DartPlayer.PlayerDarts[iDart].bDoneScoring
						IF THROW_DART_SPEC(DartPlayer.PlayerDarts[iDart], dBoard)
							DartPlayer.PlayerDarts[iDart].bDoneScoring = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		DartPlayer.sThrowInfo.fPrevPhase = fPhase
	ENDIF
ENDPROC


PROC UPDATE_SB_GAMES(SCALEFORM_INDEX &siScoreBoard, BOOL bIsSinglePlayer, BOOL bIsHost, INT iGamesWonP2, INT iGamesWonP1)

	INT iP2 = iGamesWonP2
	INT iP1 = iGamesWonP1
	
	IF NOT bIsHost
	iP1 = iGamesWonP2
	iP2 = iGamesWonP1
	ENDIF
	CDEBUG1LN(DEBUG_DARTS, "*[spec] UPDATE GAMES")
	BEGIN_SCALEFORM_MOVIE_METHOD(siScoreBoard, "SET_PLAYER_SETS_AND_LEGS")
		IF bIsSinglePlayer
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iP2)
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iP1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC START_PLAYING_OUTRO_ANIM(DARTS_AP_PLAYER_BROADCAST_DATA &PlayerData)
	
	REQUEST_ANIM_DICT("anim@amb@clubhouse@mini@darts@")
	IF HAS_ANIM_DICT_LOADED("anim@amb@clubhouse@mini@darts@")
		IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData, DARTS_CLIENTFLAG_OUTRO_STARTED)	
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CDEBUG1LN(DEBUG_DARTS, "STARTED PLAYING OUTRO ANIM")
				TASK_PLAY_ANIM(PLAYER_PED_ID(), "anim@amb@clubhouse@mini@darts@", "outro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)	
				DARTS_SET_CLIENT_FLAG(PlayerData, DARTS_CLIENTFLAG_OUTRO_STARTED)
			ENDIF	
		ENDIF
	ENDIF
ENDPROC


PROC INIT_PRE_GAME_CAM(DARTS_APARTMENT_TYPE eDAT, CAMERA_INDEX &cam, BOOL bIsSinglePlayer)
	SWITCH eDAT
		CASE BIKER_VARIANT_A
			IF bIsSinglePlayer
				//cam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1118.9298, -3158.2610, -36.3659>>, <<-3.3597, -0.0000, 40.3392>>, 40.9065)//old
				cam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1118.0883, -3159.1189, -36.3843>>, <<-3.1143, 0.0000, 45.6699>>,  47.0142)
			ELSE
				cam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1118.0822, -3158.7229, -36.3498>>, <<-5.6008, -0.0000, 45.8057>>, 47.0142)
			ENDIF
		BREAK
		
		CASE BIKER_VARIANT_B
			IF bIsSinglePlayer
				cam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1002.8469, -3164.9526, -33.4244>>, <<-3.5670, -0.0000, 112.2157>>, 40.3693)
			ELSE
				cam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1002.3334, -3164.7175, -33.4356>>, <<-1.6441, 0.0000, 122.4329>>, 40.3693)
			ENDIF
		BREAK
		
		CASE APT_TYPE_ARCADE
			IF bIsSinglePlayer
				cam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2710.4666, -359.8044, -54.4121>>, <<-3.5670, 0.0, 112.2157>>, 40.3693)
			ELSE
				cam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2709.9531, -359.5693, -54.4233>>, <<-1.6441, 0.0, 122.4329>>, 40.3693)
			ENDIF
		BREAK
		
		#IF FEATURE_DLC_2_2022
		CASE APT_TYPE_JUGGALO_HIDEOUT
			IF bIsSinglePlayer
				cam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<560.8927, -419.1003, -67.8403>>, <<-18.4027, -0.0000, 90.0239>>, 40.3693)
			ELSE
				cam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<560.8927, -419.1003, -67.8403>>, <<-18.4027, -0.0000, 90.0239>>, 40.3693)
			ENDIF
		BREAK
		#ENDIF
	ENDSWITCH
	
ENDPROC

PROC HIDE_PLAYER_HUD()
	// doing this to hide location text
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()
	SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
ENDPROC

ENUM ENUM_LOCAL_PLAYER
	eLP_DRAW_SCORE_BOARD,
	eLP_HIDE_PLAYER_HUD,
	eLP_HIDE_PARTICIPANTS,
	eLP_HIDE_PLAYERS,
	eLP_UPDATE_OPPONENT_TRANSFORM, 
	eLP_HIDE_PLAYER_OVERHEAD,
	eLP_DISABLE_CAM_COL_HAND_BLOCK, // spec
	eLP_SHOW_BUSYSPINNER
ENDENUM

FUNC BOOL SAFE_BIT_TOGGLE(INT &iBitFlags, INT iFlag, BOOL bState = TRUE)

	BOOL bCurrentState = IS_BIT_SET(iBitFlags, iFlag)
	
	IF bState
		IF NOT bCurrentState
			SET_BIT(iBitFlags, iFlag)
			RETURN TRUE
		ENDIF
	ELSE
		IF bCurrentState
			CLEAR_BIT(iBitFlags, iFlag)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


ENUM ENUM_INTRO_SEQUENCE
	eIT_INIT,
	eIT_MOVE_TO_START,
	eIT_CHECK_REACHED_START,
	eIT_INTRO_CAM,
	eIT_SPAWN_HAND_BLOCKERS,
	eIT_CHECK_HEADING,
	eIT_WAIT_PLAYER_SYNC,
	eIT_STAT_ANIM_PLAY,
	eIT_WAIT_FOR_ANIM
ENDENUM

PROC DARTS_ADD_SB_CREW_TAG_UGC_CHECK(DARTS_SCOREBOARD & DartsSB, INT iSlot, STRING sCrewName, BOOL bPrivate = FALSE)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
		AND NOT GB_IS_THIS_PLAYERS_GANG_NAME_BLOCKED(PLAYER_ID())
		OR (IS_DURANGO_AND_CAN_VIEW_GAMER_USER_CONTENT(PLAYER_ID())
			AND NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT))
			iSlot++
			BEGIN_SCALEFORM_MOVIE_METHOD(DartsSB.siScoreBoard, "SET_CREW_TAG")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sCrewName)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bPrivate)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
	
ENDPROC

FUNC STRING PICK_INTRO_ANIM(BOOL bDefault)
	IF bDefault
		RETURN "Intro"
	ELSE
		RETURN "Intro_B"
	ENDIF
ENDFUNC

FUNC BOOL IS_PLAYING_INTRO(PED_INDEX pedPlayer, BOOL &bDefaultIntro)
	STRING sIntroCheck
	sIntroCheck = PICK_INTRO_ANIM(TRUE)
	
	IF IS_ENTITY_PLAYING_ANIM(pedPlayer, "anim@amb@clubhouse@mini@darts@", sIntroCheck)
		bDefaultIntro = TRUE
		CDEBUG1LN(DEBUG_DARTS, "DEFAULT INTRO PICKED ",sIntroCheck)
		RETURN TRUE
	ENDIF
	
	sIntroCheck = PICK_INTRO_ANIM(FALSE)
	IF IS_ENTITY_PLAYING_ANIM(pedPlayer, "anim@amb@clubhouse@mini@darts@", sIntroCheck)
		bDefaultIntro = FALSE
		CDEBUG1LN(DEBUG_DARTS, "2ND INTRO PICKED ",sIntroCheck)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC VECTOR GET_PLAYER_POSITION(DARTS_APARTMENT_TYPE dsCurrent , BOOL bAside)
	SWITCH dsCurrent
		CASE BIKER_VARIANT_A
			IF bAside
				RETURN <<1117.34, -3156.68, -37.0669 >>
			ELSE
				RETURN << 1117.34, -3157.38, -38.0669 >>
			ENDIF
		BREAK
		
		CASE BIKER_VARIANT_B
			IF bAside	
				RETURN <<1000.59, -3165.35, -35.0540>> 
			ELSE
				RETURN <<1001.28, -3165.35, -35.0540 >> 
			ENDIF
		BREAK
		
		CASE APT_TYPE_ARCADE
			IF bAside
				RETURN <<2708.2097, -360.2018, -56.0417>>
			ELSE
				RETURN <<2708.8997, -360.2018, -56.0417>>
			ENDIF
		BREAK
		
		#IF FEATURE_DLC_2_2022
		CASE APT_TYPE_JUGGALO_HIDEOUT
			IF bAside
				RETURN <<556.3322, -418.6962, -70.6348>>
			ELSE
				RETURN <<557.2029, -418.9254, -70.6348>>
			ENDIF
		BREAK
		#ENDIF
	ENDSWITCH
	CDEBUG1LN(DEBUG_DARTS, "INVALID LOCATION ")
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Toggle collision between 2 players
/// PARAMS:
///    piOne - 
///    piTwo - 
///    bCollisionON - off - collision is always off, on - collision will reenable when not colliding.
/// RETURNS: return true if succeeded
///    
FUNC BOOL TOGGLE_COLLISION_BETWEEN_ENTITIES(ENTITY_INDEX eiOne, ENTITY_INDEX eiTwo, BOOL bEnableIfNotColiding)
	IF IS_ENTITY_ALIVE(eiOne)
	AND IS_ENTITY_ALIVE(eiTwo)
		SET_ENTITY_NO_COLLISION_ENTITY(eiOne, eiTwo, bEnableIfNotColiding)
		SET_ENTITY_NO_COLLISION_ENTITY(eiTwo, eiOne, bEnableIfNotColiding)
		CDEBUG1LN(DEBUG_DARTS,"TOGGLE_COLLISION_BETWEEN_ENTITIES - TOGGLE COLLISION BETWEEN ", bEnableIfNotColiding)
		RETURN TRUE
	ENDIF
	CDEBUG1LN(DEBUG_DARTS,"TOGGLE_COLLISION_BETWEEN_ENTITIES - Could not toggle collision ", bEnableIfNotColiding)
	RETURN FALSE
ENDFUNC

STRUCT STRUCT_INTRO_SCENE
	INT 			iSceneID
	PED_INDEX 		pedClones[2]
	OBJECT_INDEX 	objDart
ENDSTRUCT

FUNC INT CREATE_INTRO_SCENE(DARTS_APARTMENT_TYPE eApartmentType)
	
	SWITCH eApartmentType
		CASE BIKER_VARIANT_A
			RETURN CREATE_SYNCHRONIZED_SCENE(<<1119.07, -3157.373, -36.63106>>, << 0.0, 0.0, -72.00>>)
		BREAK
		
		CASE BIKER_VARIANT_B
			RETURN CREATE_SYNCHRONIZED_SCENE(<<1000.93, -3163.369, -33.6125>>, << 0.0, 0.0, 7.560>>)
		BREAK
		
		CASE APT_TYPE_ARCADE
			RETURN CREATE_SYNCHRONIZED_SCENE(<<2708.5497, -358.2208, -54.6002>>, <<0.0, 0.0, 7.560>>)
		BREAK
		
		#IF FEATURE_DLC_2_2022
		CASE APT_TYPE_JUGGALO_HIDEOUT
			RETURN CREATE_SYNCHRONIZED_SCENE(<<556.0620, -421.2944, -69.1398>>, <<0.0, 0.0, 180.0>>)
		BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN -1
ENDFUNC

PROC INTRO_DART_VISIBILITY(OBJECT_INDEX &objDart, BOOL bVisible)
	IF DOES_ENTITY_EXIST(objDart)
		SET_ENTITY_VISIBLE(objDart, bVisible)
		CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - TOGGLE VISIBILITY: ", bVisible)
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - TOGGLE VISIBILITY, DART DOESNT EXIST")
	ENDIF
ENDPROC

PROC INTRO_DART_CREATE(OBJECT_INDEX &objDart, VECTOR vLocation)
	//offset darts so assert doesnt fire about obj in same place.
	objDart = CREATE_OBJECT(modelDart1, vLocation, FALSE, FALSE)
	//SET_ENTITY_COLLISION(objDart, FALSE, FALSE)
	CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - CREATING DART")
ENDPROC

FUNC BOOL INTRO_SCENE_CREATE(DARTS_AP_PLAYER_BROADCAST_DATA &PlayerData[MAXPLAYERS], STRUCT_PLAYER_INFO &PlayerDarts[DART_PLAYER_IDS], STRUCT_INTRO_SCENE &SceneInfo, DARTS_APARTMENT_TYPE eApartmentType)
	CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - CREATING INTRO")
		
	SceneInfo.iSceneID = CREATE_INTRO_SCENE(eApartmentType)
				
	IF CREATE_PERSONAL_CLONE(PlayerDarts[DARTS_PLAYER_ONE].playerID, SceneInfo.pedClones[DARTS_PLAYER_ONE])
		CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - DARTS_PLAYER_ONE - CLONE CREATED For ", GET_PLAYER_NAME(PlayerDarts[DARTS_PLAYER_ONE].playerID))
	ELSE
		RETURN FALSE
		CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - DARTS_PLAYER_ONE - Was not able to create a personal clone")
	ENDIF
	
	IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_SINGLE_PLAYER)
		IF CREATE_PERSONAL_CLONE(PlayerDarts[DARTS_PLAYER_TWO].playerID, SceneInfo.pedClones[DARTS_PLAYER_TWO])
			CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - DARTS_PLAYER_TWO - CLONE CREATED", GET_PLAYER_NAME(PlayerDarts[DARTS_PLAYER_TWO].playerID))
		ELSE
			RETURN FALSE
			CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - DARTS_PLAYER_TWO - Was not able to create a personal clone")
		ENDIF
	ENDIF
			
	//Check if both area ready or single
	IF (DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_SINGLE_PLAYER) AND DOES_ENTITY_EXIST(SceneInfo.pedClones[DARTS_PLAYER_TWO]))
	OR (DOES_ENTITY_EXIST(SceneInfo.pedClones[DARTS_PLAYER_ONE]) AND DOES_ENTITY_EXIST(SceneInfo.pedClones[DARTS_PLAYER_TWO]))
		
		IF DOES_ENTITY_EXIST(SceneInfo.pedClones[DARTS_PLAYER_ONE])
			SET_ENTITY_VISIBLE(SceneInfo.pedClones[DARTS_PLAYER_ONE], TRUE)
			TOGGLE_COLLISION_BETWEEN_ENTITIES(PLAYER_PED_ID(), SceneInfo.pedClones[DARTS_PLAYER_ONE], FALSE)
			TASK_SYNCHRONIZED_SCENE(SceneInfo.pedClones[DARTS_PLAYER_ONE], SceneInfo.iSceneID, "mini@dartsintro_alt1", "darts_ig_intro_alt1_guy2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - PLAYING GUY1")
		ELSE
			RETURN FALSE
			CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - PED DOES NOT EXIST")
		ENDIF
		
		IF NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[PARTICIPANT_ID_TO_INT()], DARTS_CLIENTFLAG_SINGLE_PLAYER)
			IF DOES_ENTITY_EXIST(SceneInfo.pedClones[DARTS_PLAYER_TWO])
				SET_ENTITY_VISIBLE(SceneInfo.pedClones[DARTS_PLAYER_TWO], TRUE)
				TOGGLE_COLLISION_BETWEEN_ENTITIES(PLAYER_PED_ID(), SceneInfo.pedClones[DARTS_PLAYER_TWO], FALSE)
				TASK_SYNCHRONIZED_SCENE(SceneInfo.pedClones[DARTS_PLAYER_TWO], SceneInfo.iSceneID, "mini@dartsintro_alt1", "darts_ig_intro_alt1_guy1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
				CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - PLAYING GUY2")
			ELSE
				RETURN FALSE
				CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - PED DOES NOT EXIST")
			ENDIF
		ENDIF
		
		
		IF DOES_ENTITY_EXIST(SceneInfo.objDart)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(SceneInfo.objDart, SceneInfo.iSceneID, "darts_ig_intro_alt1_dart", "mini@dartsintro_alt1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(AF_FORCE_START))
			CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - PLAY DART INTO ANIM", SceneInfo.iSceneID)
		ENDIF
		
		SET_SYNCHRONIZED_SCENE_PHASE(SceneInfo.iSceneID, 0.45)
		
		CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - FINISHED")
		RETURN TRUE
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "INTRO SCENE - SCENE PEDS NOT READY YET.")
		RETURN FALSE
	ENDIF
ENDFUNC


PROC INTRO_SCENE_CLEANUP(STRUCT_INTRO_SCENE &SceneInfo)
	INT iPlayerID
	REPEAT 2 iPlayerID
		IF DOES_ENTITY_EXIST(SceneInfo.pedClones[iPlayerID])
			DELETE_PED(SceneInfo.pedClones[iPlayerID])
			CDEBUG1LN(DEBUG_DARTS, "INTRO_SCENE_CLEANUP - PED DELETED: ", iPlayerID)
		ELSE
			CDEBUG1LN(DEBUG_DARTS, "INTRO_SCENE_CLEANUP - CANT DELETE DOESNT EXIST: ", iPlayerID)
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(SceneInfo.objDart)
		DELETE_OBJECT(SceneInfo.objDart)
		CDEBUG1LN(DEBUG_DARTS, "INTRO_SCENE_CLEANUP - DART DELETED: ")
	ELSE
		CDEBUG1LN(DEBUG_DARTS, "INTRO_SCENE_CLEANUP - CANT DELETE DART: ")
	ENDIF
ENDPROC

FUNC BOOL SPAWN_HAND_BLOCKER(STRUCT_PLAYER_INFO &sPlayerInfo, VECTOR vBoardLocation)
	
	REQUEST_MODEL(prop_phonebox_03)
	
	IF HAS_MODEL_LOADED(prop_phonebox_03)
		IF NOT DOES_ENTITY_EXIST(sPlayerInfo.oHandBlocker)			
		
			sPlayerInfo.oHandBlocker = CREATE_OBJECT_NO_OFFSET(prop_phonebox_03, sPlayerInfo.vHandBlockerLocation, FALSE, FALSE)
			SET_CAN_CLIMB_ON_ENTITY(sPlayerInfo.oHandBlocker, FALSE)
			FREEZE_ENTITY_POSITION(sPlayerInfo.oHandBlocker, TRUE)
			TOGGLE_COLLISION_BETWEEN_ENTITIES(sPlayerInfo.pedID, sPlayerInfo.oHandBlocker, FALSE)
			SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), sPlayerInfo.oHandBlocker, TRUE)
			SET_ENTITY_VISIBLE(sPlayerInfo.oHandBlocker, FALSE)
			//90 is default rot for correct orientation with the player. 
			SET_ENTITY_HEADING(sPlayerInfo.oHandBlocker, GET_HEADING_BETWEEN_VECTORS_2D(sPlayerInfo.vHandBlockerLocation, vBoardLocation) - 90)
			CDEBUG1LN(DEBUG_DARTS, "SPAWN_HAND_BLOCKER - Final Heading FROM PLAYER TO BOARD : ", GET_HEADING_BETWEEN_VECTORS_2D(sPlayerInfo.vHandBlockerLocation, vBoardLocation))
			CDEBUG1LN(DEBUG_DARTS, "SPAWN_HAND_BLOCKER - Final Heading : ", GET_ENTITY_HEADING(sPlayerInfo.oHandBlocker))
			SET_ENTITY_COORDS(sPlayerInfo.oHandBlocker, sPlayerInfo.vHandBlockerLocation)
			CDEBUG1LN(DEBUG_DARTS, "SPAWN_HAND_BLOCKER - created blocked at position: ", sPlayerInfo.vHandBlockerLocation)
		ENDIF	
		RETURN TRUE	
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "SPAWN_HAND_BLOCKER - Unable to spawn hand blocker: ", sPlayerInfo.vHandBlockerLocation)
	RETURN FALSE
ENDFUNC

PROC DESTORY_HAND_BLOCKER(STRUCT_PLAYER_INFO &PlayerDarts[DART_PLAYER_IDS])
	IF DOES_ENTITY_EXIST(PlayerDarts[DARTS_PLAYER_ONE].oHandBlocker)
		DELETE_OBJECT(PlayerDarts[DARTS_PLAYER_ONE].oHandBlocker)
		CDEBUG1LN(DEBUG_DARTS, "DESTORY_HAND_BLOCKER - Blocker was destroyed.1")
	ENDIF
	
	IF DOES_ENTITY_EXIST(PlayerDarts[DARTS_PLAYER_TWO].oHandBlocker)
		DELETE_OBJECT(PlayerDarts[DARTS_PLAYER_TWO].oHandBlocker)
		CDEBUG1LN(DEBUG_DARTS, "DESTORY_HAND_BLOCKER - Blocker was destroyed.2")
	ENDIF
ENDPROC

FUNC VECTOR GET_HAND_BLOCKER_POSITION(VECTOR vPlayerPos, VECTOR vBoardPos)
	CDEBUG1LN(DEBUG_DARTS, "GET_HAND_BLOCKER_POSITION - vPlayerPos: ", vPlayerPos, " vBoardPos", vBoardPos)
	
	VECTOR vOut = <<0, 0, 0>>
	VECTOR vDir = NORMALISE_VECTOR(vBoardPos - vPlayerPos)
	VECTOR vLeft = CROSS_PRODUCT(vDir, <<0,0,1>>)
	//manual blocker offsets
	vOut += vDir * 0.4 // forward
	vOut -= <<0, 0, 1.2>> //down
	vOut += vLeft * 0.1 //left
	RETURN vPlayerPos + vOut
ENDFUNC
