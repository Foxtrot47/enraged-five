////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Dart_lib.sch                      //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Dart Funcs           			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "Darts_Dart.sch"
USING "Darts_Board.sch"
USING "Darts_Player.sch"
USING "commands_entity.sch"

CONST_FLOAT DART_RADIUS 0.04 //0.2
CONST_FLOAT DART_THROW_SPEED 8.5

PROC CLEANUP_DART(DART & Darts)
	VECTOR vCleanupDart = << -1668.0444, -1056.4501, 13.1063 >>

	IF DOES_ENTITY_EXIST(Darts.object)
		SET_ENTITY_COORDS(Darts.object, vCleanupDart)
		SET_OBJECT_AS_NO_LONGER_NEEDED(Darts.object)
		DELETE_OBJECT(Darts.object)
	ENDIF
ENDPROC

/// PURPOSE:
///    Renders Dart throw traveling through the air and hitting the board
/// PARAMS:
///    Darts - a dart object that will be traveling
///    DartBoard - the destination of the dart object
/// RETURNS:
///    TRUE when the dart hits the board, false otherwise
FUNC BOOL THROW_DART(DART & Darts, DARTS_BOARD & DartBoard, DARTS_THROW_STYLE ThisThrowStyle = DARTS_STYLE_IV, BOOL bDebugDartInfo = FALSE)
	
	VECTOR vTemp, vTemp2
	
	// Drawing the actual dart landing	
	IF iPlayr = 0 AND ThisThrowStyle = DARTS_STYLE_STICK
		DRAW_2D_SPRITE("Darts", "Dart_Reticules", spriteDartLanding, TRUE, HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_AFTER_HUD,FALSE)
	ENDIF
	
	FLOAT fTimeSinceLastFrame = GET_FRAME_TIME()
	
	vTemp2 = Darts.vThrow * fTimeSinceLastFrame * DART_THROW_SPEED
	Darts.vPosition -=  vTemp2
	
	//If dart has hit the board
	IF (Darts.vPosition.y > Darts.vTarget.y - DART_RADIUS)
		
//		VECTOR vTemp3, vDir
//		FLOAT fTemp1, fTemp3
		
		Darts.bStuck = TRUE
		Darts.bTravelling = FALSE
														
		vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartBoard.vDartBoard, DartBoard.fBoardHeading,
																<<Darts.vOffsetTarget.x, 
																Darts.vOffsetTarget.y + fDartBoardOriginOffset, 
																Darts.vOffsetTarget.z>>)	
		SET_ENTITY_COORDS(Darts.object, vTemp)
		
		CDEBUG1LN(DEBUG_DARTS, "Dart landed on board at world coords: ", vTemp, ", local coords: ", Darts.vOffsetTarget)
		
//		vTemp3 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartBoard.vDartBoard, DartBoard.fBoardHeading, 
//																vInitDartPosOffset)
//		
//		vDir = vTemp - vTemp3
//		vDir = NORMALISE_VECTOR(vDir)
//		CDEBUG1LN(DEBUG_DARTS, "vDir = ", vDir)
//		fTemp1 = ASIN(vDir.z)
//		CDEBUG1LN(DEBUG_DARTS, "fTemp1 = ", fTemp1)
//		fTemp1 += 90
//		CDEBUG1LN(DEBUG_DARTS, "fTemp1 = ", fTemp1)
//		fTemp3 = ATAN((vDir.y/vDir.x))
//		CDEBUG1LN(DEBUG_DARTS, "fTemp3 = ", fTemp3)
//		fTemp3 -= 90
//		CDEBUG1LN(DEBUG_DARTS, "fTemp3 = ", fTemp3)
//		
//		SET_ENTITY_ROTATION(Darts.object, <<fTemp1, Darts.vRotation.y, fTemp3>>, EULER_XYZ)
		
		RETURN TRUE
		
	ELSE	
		//Get offset, Set position		
														
		vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartBoard.vDartBoard, DartBoard.fBoardHeading,
																<<Darts.vPosition.x, 
																Darts.vPosition.y + fDartBoardOriginOffset, 
																Darts.vPosition.z>>)
		SET_ENTITY_COORDS(Darts.object, vTemp)
		
		//Set object's rotation
		Darts.vRotation.y += 1000 * fTimeSinceLastFrame
		SET_ENTITY_ROTATION(Darts.object, Darts.vRotation, EULER_XYZ)
		
		IF DOES_ENTITY_EXIST(Darts.object)
			IF bDebugDartInfo
				CDEBUG1LN(DEBUG_DARTS, "Darts.vThrow * fTimeSinceLastFrame * DART_THROW_SPEED = ", vTemp2)
				CDEBUG1LN(DEBUG_DARTS, "Dart flying through the air at world coords: ", vTemp, ", local coords: ", Darts.vPosition)
				CDEBUG1LN(DEBUG_DARTS, "Dart flying through the air with rotation: ", Darts.vRotation)
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_DARTS, "*************Dart object was lost!")
			CDEBUG1LN(DEBUG_DARTS, "*************Last known whereabouts:")
			CDEBUG1LN(DEBUG_DARTS, "*************world coords: ", vTemp, ", local coords: ", Darts.vPosition)
			CDEBUG1LN(DEBUG_DARTS, "*************rotation: ", Darts.vRotation)
			CDEBUG1LN(DEBUG_DARTS, "*************Let's see what happens if we just return true")
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Throws dart at entity standing in front of a darts_board
/// PARAMS:
///    Darts - Object to be thrown
///    DartBoard - Intended destination for dart
///    EntityIndex - Actual destination of dart
/// RETURNS: TRUE when dart hits entity
///    
FUNC BOOL THROW_DART_AT_ENTITY(DART & Darts, DARTS_BOARD & DartBoard, VECTOR vEntityCoord)// ENTITY_INDEX & EntityIndex)
	
	VECTOR vTemp, vTemp2
	VECTOR vEntityTarget
	
	vEntityTarget = vEntityCoord - DartBoard.vDartBoard //GET_ENTITY_COORDS(EntityIndex) - DartBoard.vDartBoard
	
	FLOAT fTimeSinceLastFrame = GET_FRAME_TIME()
	
	vTemp2 = Darts.vThrow * fTimeSinceLastFrame * DART_THROW_SPEED
	Darts.vPosition -=  vTemp2
	
	//If dart has hit the entity
	IF (Darts.vPosition.y > vEntityTarget.y - DART_RADIUS)
		
		Darts.bStuck = TRUE
		Darts.bTravelling = FALSE
		
		SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(Darts.object, TRUE)
		
		RETURN TRUE
		
	ELSE	
		//Get offset, Set position		
														
		vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DartBoard.vDartBoard, DartBoard.fBoardHeading,
																<<Darts.vPosition.x, 
																Darts.vPosition.y + fDartBoardOriginOffset, 
																Darts.vPosition.z>>)
		SET_ENTITY_COORDS(Darts.object, vTemp)
		
		//Set object's rotation
		Darts.vRotation.y += 1000 * fTimeSinceLastFrame
		SET_ENTITY_ROTATION(Darts.object, Darts.vRotation, EULER_XYZ)
		
		IF DOES_ENTITY_EXIST(Darts.object)
			
			CDEBUG1LN(DEBUG_DARTS, "Darts.vThrow * fTimeSinceLastFrame * DART_THROW_SPEED = ", vTemp2)
			CDEBUG1LN(DEBUG_DARTS, "Dart flying through the air at world coords: ", vTemp, ", local coords: ", Darts.vPosition)
			CDEBUG1LN(DEBUG_DARTS, "Dart flying through the air with rotation: ", Darts.vRotation)
		ELSE
			CDEBUG1LN(DEBUG_DARTS, "*************Dart object was lost!")
			CDEBUG1LN(DEBUG_DARTS, "*************Last known whereabouts:")
			CDEBUG1LN(DEBUG_DARTS, "*************world coords: ", vTemp, ", local coords: ", Darts.vPosition)
			CDEBUG1LN(DEBUG_DARTS, "*************rotation: ", Darts.vRotation)
			CDEBUG1LN(DEBUG_DARTS, "*************Let's see what happens if we just return true")
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
ENDFUNC

//MAKE SENSE OF WHAT'S GOING ON HERE
PROC SET_DART_THROW_VECTOR (DART & Darts)
	CDEBUG1LN(DEBUG_DARTS, "*******************SET DART THROW VECTOR VALUES*****************")
	VECTOR vThrowVectorBeforeNormalize 
	FLOAT fThrowVectorLength
	FLOAT fTemp1
	FLOAT fXOffset
	FLOAT fDartDistance = 0.08
	
	// THIS VALUE DECIDES HOW DEEP THE DART STICKS IN.  PAUSE!
	Darts.vTarget.y = -0.213 //-0.23
	
	vThrowVectorBeforeNormalize = vInitDartPosOffset - Darts.vTarget
	//CDEBUG1LN(DEBUG_DARTS, "*******************vInitDartPosOffset = ",  vInitDartPosOffset)
	//CDEBUG1LN(DEBUG_DARTS, "*******************vThrowVectorBeforeNormalize = ", vThrowVectorBeforeNormalize)
	
	// Normalize it
	fThrowVectorLength = VMAG(vThrowVectorBeforeNormalize)
	Darts.vThrow = (vThrowVectorBeforeNormalize / fThrowVectorLength)
	//CDEBUG1LN(DEBUG_DARTS, "*******************After Normalize, Darts.vThrow = ", Darts.vThrow)

	fTemp1 = GET_HEADING_FROM_VECTOR_2D(Darts.vThrow.x, Darts.vThrow.y)
	//CDEBUG1LN(DEBUG_DARTS, "*******************Heading from Vector 2D = ", ftemp1)
	
	Darts.vRotation.z = ftemp1 - 180.0
	//CDEBUG1LN(DEBUG_DARTS, "*******************Darts.vRotation.z = ", Darts.vRotation.z)
	
	// offset the dart after we've angled it	
	fXOffset = TAN(Darts.vRotation.z) *  fDartDistance //* 0.03
	//CDEBUG1LN(DEBUG_DARTS, "*******************fXOffset = ",fXOffset)
	
	Darts.vOffsetTarget = Darts.vTarget
	Darts.vOffsetTarget.x += fXOffset //-= fXOffset
	CDEBUG1LN(DEBUG_DARTS, "*******************Darts.vTarget       = ",Darts.vTarget)
	//CDEBUG1LN(DEBUG_DARTS, "*******************Darts.vOffsetTarget = ",Darts.vOffsetTarget)
	
	// now recalculate the throw vector with the new offset target
	vThrowVectorBeforeNormalize = vInitDartPosOffset - Darts.vOffsetTarget
	fThrowVectorLength = VMAG(vThrowVectorBeforeNormalize)
	Darts.vThrow = (vThrowVectorBeforeNormalize / fThrowVectorLength)
	//CDEBUG1LN(DEBUG_DARTS, "*******************Darts.vThrow = ",Darts.vThrow)
	
	//Darts.vOffsetTarget.x += fXOffset

ENDPROC

PROC INIT_DARTS(DART & Darts, INT i)
		
	IF i=0
		Darts.modelDart = modelDart1
		CDEBUG1LN(DEBUG_DARTS, "**********PROP_DART_1 registered**********")
	ELSE
		Darts.modelDart = modelDart2
		CDEBUG1LN(DEBUG_DARTS, "**********PROP_DART_2 registered**********")
	ENDIF
	
	Darts.bTravelling = FALSE
	Darts.bStuck = FALSE
	
ENDPROC

FUNC BOOL RESET_DART(DART & oldDart)
	
	oldDart.vPosition =  << 0.0, -2.3685, -0.2 >>
	oldDart.vThrow =  << 0.0, 0.0, 0.0 >>
	oldDart.vTarget =  << 0.0, 0.0, 0.0 >>
	
	oldDart.iHitValue = 0
	
	oldDart.bStuck = FALSE
	oldDart.bTravelling = FALSE
	oldDart.bDoneScoring = FALSE
	
	IF DOES_ENTITY_EXIST(oldDart.object)
		DELETE_OBJECT(oldDart.object)
	ENDIF
	
	RETURN TRUE
ENDFUNC
