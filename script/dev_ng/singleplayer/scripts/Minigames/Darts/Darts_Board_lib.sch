////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Board.sch                         //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Board Library          			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "Darts_Board.sch"
USING "Darts_Dart_lib.sch"
USING "Darts_Player.sch"

FUNC FLOAT GET_VECTOR_LENGTH(VECTOR A)
	RETURN SQRT(A.x*A.x + A.z*A.z)
ENDFUNC

PROC CLEANUP_BOARD(DARTS_BOARD & DartsBoard)
	IF DOES_ENTITY_EXIST(DartsBoard.oiDartBoard)
		SET_OBJECT_AS_NO_LONGER_NEEDED(DartsBoard.oiDartBoard)
	ENDIF
ENDPROC

PROC CREATE_BOARD(DARTS_BOARD & DartsBoard)
	IF NOT DOES_ENTITY_EXIST(DartsBoard.oiDartBoard)
		DartsBoard.oiDartBoard = CREATE_OBJECT(modelDartBoard, DartsBoard.vDartBoard)
		SET_ENTITY_ROTATION(DartsBoard.oiDartBoard, << 0, 0, DartsBoard.fBoardHeading >>)
	ENDIF
ENDPROC

FUNC FLOAT GET_DOUBLE_ANGLE(INT iTotalScore)
	INT i
	INT iTotalHalved
	FLOAT fDartAngleIncremented
	FLOAT fDartAngle
	
	iTotalHalved = iTotalScore/2  // iTotalScore will always be sent an even number

	REPEAT 21 i
		IF iTotalHalved = iScoreForSection[i]
			fDartAngle = fDartAngleIncremented			
		ENDIF		
		fDartAngleIncremented += 18
	ENDREPEAT
	
	
	CDEBUG1LN(DEBUG_DARTS, "-------------- FROM GET_DOUBLE_ANGLE: fDartAngle = ", fDartAngle, ", iTotalHalved = ", iTotalHalved)
	
	
	RETURN fDartAngle
ENDFUNC

FUNC FLOAT GET_DOUBLE_X (FLOAT fDartAngle)
	FLOAT fLength = 0.218
	FLOAT fDoubleX
	
	fDoubleX = (SIN(fDartAngle)) * fLength
	
	RETURN fDoubleX
ENDFUNC

FUNC FLOAT GET_DOUBLE_Z (FLOAT fDartAngle)
	FLOAT fLength = 0.218
	FLOAT fDoubleZ
	
	fDoubleZ = (COS(fDartAngle)) * fLength
	
	RETURN fDoubleZ
ENDFUNC

FUNC VECTOR GET_DOUBLE_VECTOR(INT iTotalScore)
	FLOAT fDartAngle
	VECTOR vDoubleWinner
	
	IF iTotalScore = 50 // win by bullseye
		vDoubleWinner = << 0, -0.5, 0 >>
	ELSE
		fDartAngle = GET_DOUBLE_ANGLE(iTotalScore)
	
		vDoubleWinner.y = -0.5 //this will be the offset from the board
		vDoubleWinner.x = GET_DOUBLE_X(fDartAngle)
		vDoubleWinner.z = GET_DOUBLE_Z(fDartAngle)
	ENDIF
	
	RETURN vDoubleWinner
ENDFUNC

PROC SCORE_DART(DART & Darts, DARTS_BOARD & DartsBoard, BOOL bDartClash=FALSE)
	
	FLOAT fLength
	FLOAT fDartAngle
	FLOAT fDartAngleIncremented
	
	VECTOR vTempTarget = Darts.vTarget
	vTempTarget.x += 0.0041
	vTempTarget.z += 0.0002
	
	// Get the angle and fLength from the centre of the board to determine the dart's score
	//get angle of dart in relation to the origin of the board
	fDartAngle = GET_ANGLE_BETWEEN_2D_VECTORS(0.0, 1.0, vTempTarget.x, vTempTarget.z)
	fLength = GET_VECTOR_LENGTH(vTempTarget) //coords are dart vector, and origin of dart board
	
	Darts.fLength = fLength
	
	CDEBUG1LN(DEBUG_DARTS, "**********Dart angle = ", fDartAngle, ", Dart Length from center = ", fLength)
	
	// Do Dart Clash sound function
	IF bDartClash
		PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_DART_MASTER", DartsBoard.vDartBoard)
	ENDIF

	// Find out if the dart has landed in the bull, outer bull or out of the score zone
	IF fLength < 0.009 // bull
		PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BULLSEYE_MASTER", DartsBoard.vDartBoard)
		Darts.iHitValue = 50
		
	ELIF fLength < 0.021 // outer bull
		PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER", DartsBoard.vDartBoard)
		Darts.iHitValue = 25
		
	ELIF fLength > 0.300 // no score	(miss the board)
		PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_WALL_MASTER", DartsBoard.vDartBoard)
		// TODO: DO SOME MISSBOARD CONTEXTS HERE
		Darts.iHitValue = 0
		
	ELIF fLength >= 0.226 // no score	(but hit the board)	
	//ELIF fLength > 0.215 // no score	(but hit the board)	
		PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER",DartsBoard.vDartBoard)
		Darts.iHitValue = 0
	ELSE

		// Find out if the dart has landed in a treble or double zone
		IF fLength > 0.1285 AND fLength < 0.1405  // treble
		//IF fLength > 0.123 AND fLength < 0.138  // treble
			Darts.iHitMultiplier = 3
			PLAY_SOUND_FROM_COORD(-1, "DARTS_SCORE_TRIPLE_MASTER", DartsBoard.vDartBoard)			
		ELIF fLength > 0.2132 AND fLength < 0.226  // double
		//ELIF fLength > 0.201 AND fLength < 0.216  // double
			Darts.iHitMultiplier = 2
			PLAY_SOUND_FROM_COORD(-1, "DARTS_SCORE_DOUBLE_MASTER", DartsBoard.vDartBoard)		
		ELSE
			Darts.iHitMultiplier = 1
			PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_BOARD_MASTER", DartsBoard.vDartBoard)
		ENDIF

		// If the dart is on the left hand side of the board, add 180 degrees to it
		IF Darts.vTarget.x < 0.0 
			fDartAngle = 360.0 - fDartAngle			
		ENDIF

		INT iTemp, iTemp2
		
		// The dart has landed in the score zone, find out which section, 
		// and multiply it by the scoreMultiplier 
		fDartAngleIncremented=0	iTemp=0
		REPEAT 21 iTemp
			IF (fDartAngle >= (fDartAngleIncremented - 9.0)) AND (fDartAngle < (fDartAngleIncremented + 9.0))
				// checking if dart hit the wire
				IF ((fDartAngle <= (fDartAngleIncremented - 8.1)) AND (fDartAngle >= (fDartAngleIncremented - 9.9)))
				OR ((fDartAngle >= (fDartAngleIncremented + 8.1)) AND (fDartAngle <= (fDartAngleIncremented + 9.9)))
					CDEBUG1LN(DEBUG_DARTS, "hit wire master audio played")
					PLAY_SOUND_FROM_COORD(-1, "DARTS_HIT_WIRE_MASTER", DartsBoard.vDartBoard)
				ELSE
					CDEBUG1LN(DEBUG_DARTS, "angle incremented = ", fDartAngleIncremented)
					CDEBUG1LN(DEBUG_DARTS, "angle = ", fDartAngle)
					CDEBUG1LN(DEBUG_DARTS, "angle incremented - 8.1 = ", fDartAngleIncremented - 8.1)
					CDEBUG1LN(DEBUG_DARTS, "angle incremented - 9.9 = ", fDartAngleIncremented - 9.9)
					CDEBUG1LN(DEBUG_DARTS, "angle incremented + 8.1 = ", fDartAngleIncremented + 8.1)
					CDEBUG1LN(DEBUG_DARTS, "angle incremented + 9.9 = ", fDartAngleIncremented + 9.9)
				ENDIF
				
				iTemp2 = iScoreForSection[iTemp] * Darts.iHitMultiplier
				Darts.iHitValue = iTemp2
			ENDIF

			fDartAngleIncremented += 18.0
		ENDREPEAT
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "********* SCORE = ", Darts.iHitValue)
	CDEBUG1LN(DEBUG_DARTS, "********* MULTIPLIER = ", Darts.iHitMultiplier)
	
	// TODO: LOG BULLSEYES HERE FOR GAME STATS

ENDPROC

