////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    MP_Darts_Server_lib.sch                 //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Dart Server Procs and Funcs		      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "MP_Darts_Server.sch"
USING "MP_Darts_client.sch"
USING "MP_Darts.sch"
USING "Darts_Player.sch"
USING "Darts_UI.sch"

INT iDartsServerThrottle

// Look for reasons to terminate the minigame session
PROC DARTS_SERVER_UPDATE_RUN_CONDITIONS(DARTS_SERVER_BROADCAST_DATA & serverData, DARTS_MP_TERMINATE_REASON & terminateReason, DARTS_PLAYER_BROADCAST_DATA & playerData[])
	IF NETWORK_GET_NUM_PARTICIPANTS() < 2 //ENUM_TO_INT(DART_PLAYER_IDS)
	AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[DART_PLAYER_HOME_SERVER], DARTS_CLIENTFLAG_SINGLE_PLAYER)
	AND NOT DARTS_CHECK_CLIENT_FLAG(PlayerData[DART_PLAYER_AWAY_SERVER], DARTS_CLIENTFLAG_SINGLE_PLAYER)
		IF NOT DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_QUIT_1CLIENT)
			CDEBUG1LN(DEBUG_DARTS, "[MP_Darts_Server_lib -> DARTS_SERVER_UPDATE_RUN_CONDITIONS] There is only one player, setting a quit flag.")
			DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_QUIT_1CLIENT)
			IF PlayerData[DART_PLAYER_HOME_SERVER].eMPState = DARTS_MPSTATE_GAME_END
			OR PlayerData[DART_PLAYER_HOME_SERVER].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
			OR PlayerData[DART_PLAYER_AWAY_SERVER].eMPState = DARTS_MPSTATE_GAME_END
			OR PlayerData[DART_PLAYER_AWAY_SERVER].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
				CDEBUG1LN(DEBUG_DARTS, "game had already ended, setting terminate reason to ENDGAME")
				terminateReason = DARTS_MPTERMINATEREASON_ENDGAME
			ELSE
				CDEBUG1LN(DEBUG_DARTS, "game was still in play, setting terminate reason to PLAYERLEFT")
				terminateReason = DARTS_MPTERMINATEREASON_PLAYERLEFT
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DARTS_SERVER_THROW_OFF_SETUP(DARTS_SERVER_BROADCAST_DATA & serverData)
	
	serverData.iCoinFlip = GET_RANDOM_INT_IN_RANGE(0, 1000)	
	serverData.iCoinFlip = serverData.iCoinFlip % 2
	
	//IF serverData.iCoinFlip = 0
		serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_THROWING
		serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_WAITING
	//ELSE
		serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_THROWING
		serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_WAITING
	//ENDIF
	
	DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_DO_THROWOFF)
	serverData.DartsTimer.bNewShot = TRUE
	
	DARTS_INIT_TIMERS(serverData.DartsTimer)
ENDPROC

PROC DARTS_SERVER_GAME_SETUP(DARTS_SERVER_BROADCAST_DATA & serverData, BOOL bSinglePlayer, INT iSinglePlayerID)
	// Initialise the scoreboard
	// Reset the player scores to 301
	INT i
	REPEAT DARTSMP_IDS i
	//	DartGame.ScoreBoard[i] 		= INIT_DART_SCORE
		serverData.iScores[i] 		= 301
		serverData.iLastScore[i] 	= 301
	ENDREPEAT
	
	IF NOT DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
		serverData.iLegs[DART_PLAYER_HOME_SERVER] = 0
		serverData.iLegs[DART_PLAYER_AWAY_SERVER] = 0
		serverData.iSets[DART_PLAYER_HOME_SERVER] = 0
		serverData.iSets[DART_PLAYER_AWAY_SERVER] = 0
	ENDIF
	
	INT iLegs = g_FMMC_STRUCT.iNumberOfLegs
	INT iSets = g_FMMC_STRUCT.iNumberOfSets
	
	CPRINTLN(DEBUG_DARTS, "[MP DARTS] setup iLegs = ", iLegs)
	CPRINTLN(DEBUG_DARTS, "[MP DARTS] setup iSets = ", iSets)
	
	SWITCH iLegs
		CASE 0
			serverData.iNumLegs = 1
		BREAK
		CASE 1
			serverData.iNumLegs = 3
		BREAK
		CASE 2
			serverData.iNumLegs = 5
		BREAK
	ENDSWITCH
	
	SWITCH iSets
		CASE 0
			serverData.iNumSets = 1
		BREAK
		CASE 1
			serverData.iNumSets = 3
		BREAK
		CASE 2
			serverData.iNumSets = 6
		BREAK
		CASE 3
			serverData.iNumSets = 9
		BREAK
		CASE 4
			serverData.iNumSets = 11
		BREAK
		CASE 5
			serverData.iNumSets = 13
		BREAK
		CASE 6
			serverData.iNumSets = 15
		BREAK
	ENDSWITCH
	
	IF bSinglePlayer
		serverData.mpTurnStatus[iSinglePlayerID] = DARTS_MPTURN_THROWING
		
	ELSE
		// pick the first thrower at random for initial game,
		// for ever rematch after, alternate the first throwers
		IF NOT serverData.bDoWinning
			//serverData.iCoinFlip = GET_RANDOM_INT_IN_RANGE(0, 1000)	
			//serverData.iCoinFlip = serverData.iCoinFlip % 2
			CPRINTLN(DEBUG_DARTS, "[MP DARTS] serverData.iWinner = ", serverData.iWinner)
			serverData.iCoinFlip = serverData.iWinner
			CPRINTLN(DEBUG_DARTS, "[MP DARTS] serverData.iCoinFlip = ", serverData.iCoinFlip)
		ELSE
			serverData.iCoinFlip = 1 - serverData.iCoinFlip
		ENDIF
		
		IF serverData.iCoinFlip = 0
			serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_THROWING
			serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_WAITING
		ELSE
			serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_THROWING
			serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_WAITING
		ENDIF
	ENDIF
	
	serverData.iServerTurn = 0
	serverData.iTurnscore = 0
	serverData.iUIFlags = 0
	
	serverData.bDoWinning = FALSE
	serverData.bTurnContinue = FALSE
	serverData.bTurnEnd = FALSE
	serverData.bTurnBust = FALSE
	serverData.bDoSwitchPlayer = FALSE
	serverData.DartsTimer.bNewShot = TRUE
	
	DARTS_INIT_TIMERS(serverData.DartsTimer)
ENDPROC

// keep track of score server side, this should happen only at the end of turns
FUNC BOOL DARTS_SERVER_UPDATE_SCORING(DART & Darts)
	//serverData.iScores[DartsPlayer] -= iTurnScore
	IF Darts.bDoneScoring
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT DARTS_SERVER_GET_LEGS_NEEDED(DARTS_SERVER_BROADCAST_DATA & serverData)
	SWITCH serverData.iNumLegs
		CASE 1	RETURN 1
		CASE 3	RETURN 2
		CASE 5	RETURN 3
	ENDSWITCH
	
	RETURN 1
ENDFUNC

FUNC BOOL EVALUATE_DART_GAME_PROGRESS(DARTS_SERVER_BROADCAST_DATA & serverData)
	
	INT iEvaluator = serverData.iWinner
	INT iWinsNeeded
	
	iWinsNeeded = DARTS_SERVER_GET_LEGS_NEEDED(serverData)
	
	IF (serverData.iLegs[iEvaluator] = iWinsNeeded)
		serverData.iSets[iEvaluator]++
		serverData.iLegs[DART_PLAYER_AWAY_SERVER] = 0
		serverData.iLegs[DART_PLAYER_HOME_SERVER] = 0
		DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_SET_JUST_WON)
		
		CDEBUG1LN(DEBUG_DARTS, "EVALUATE_DART_GAME_PROGRESS - Set just won by ", iEvaluator, ", number of sets won is ", serverData.iSets[iEvaluator])
		
		IF (serverData.iSets[iEvaluator] = serverData.iNumSets)
			RETURN TRUE
		ENDIF
	ELSE
		IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_SET_JUST_WON)
			DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_SET_JUST_WON)
		ENDIF
		
		//continue on until the set is completed
		CDEBUG1LN(DEBUG_DARTS, "EVALUATE_DART_GAME_PROGRESS - leg just won by ", iEvaluator, ", number of legs won is ", serverData.iLegs[iEvaluator])
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DARTS_SERVER_THROW_OFF_UPDATE(DARTS_SERVER_BROADCAST_DATA & serverData, DARTS_PLAYER_BROADCAST_DATA & playerData[], DART & Darts)
	IF playerData[DART_PLAYER_AWAY_SERVER].eMPState = DARTS_MPSTATE_THROW_OFF
	AND playerData[DART_PLAYER_HOME_SERVER].eMPState = DARTS_MPSTATE_THROW_OFF
		IF (playerData[DART_PLAYER_AWAY_SERVER].eMPThrowStage = DARTS_MPTHROW_AIM OR playerData[DART_PLAYER_AWAY_SERVER].eMPWaitStage = DARTS_MPWAIT_AIM)
		AND (playerData[DART_PLAYER_HOME_SERVER].eMPThrowStage = DARTS_MPTHROW_AIM OR playerData[DART_PLAYER_HOME_SERVER].eMPWaitStage = DARTS_MPWAIT_AIM)
			
			DARTS_UPDATE_THROW_OFF_CLOCK(serverData.DartsTimer)
			
		ENDIF
		
		IF (serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_THROWING 
			OR serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_THROWING)
		AND NOT serverData.bFinishedScoreTracking
			// handing the score recording after throws
			IF DARTS_SERVER_UPDATE_SCORING(Darts)
			AND DARTS_CHECK_CLIENT_FLAG(PlayerData[DART_PLAYER_HOME_SERVER], DARTS_CLIENTFLAG_SCORING_DONE)
			AND DARTS_CHECK_CLIENT_FLAG(PlayerData[DART_PLAYER_AWAY_SERVER], DARTS_CLIENTFLAG_SCORING_DONE)
			
				IF serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_THROWING
					serverData.iThrower = DART_PLAYER_HOME_SERVER
					serverData.iWaiter = DART_PLAYER_AWAY_SERVER
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the thrower is DART_PLAYER_HOME_SERVER")
				ELSE
					serverData.iThrower = DART_PLAYER_AWAY_SERVER
					serverData.iWaiter = DART_PLAYER_HOME_SERVER
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the thrower is DART_PLAYER_AWAY_SERVER")
				ENDIF
				
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that playerData[DART_PLAYER_HOME_SERVER].ClientDart.fLength = ", playerData[DART_PLAYER_HOME_SERVER].ClientDart.fLength)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that playerData[DART_PLAYER_AWAY_SERVER].ClientDart.fLength = ", playerData[DART_PLAYER_AWAY_SERVER].ClientDart.fLength)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that Darts.fLength = ", Darts.fLength)
				
				serverData.fLength[serverData.iThrower] = Darts.fLength
				
				IF (serverData.iThrower = DART_PLAYER_HOME_SERVER)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, TRUE)
				ELSE
					serverData.bDoWinning = TRUE
				ENDIF
				
				serverData.bFinishedScoreTracking = TRUE
				
				serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_SCORE_CHECK
				serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_SCORE_CHECK
			ENDIF
		
		ELIF serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_SCORE_CHECK
		OR serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_SCORE_CHECK
			
			serverData.DartsTimer.bTimeRunOut = FALSE
			
			// handle switching turns
			IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
				// set this client flag so that both clients know to clear the board
				// maybe do some printing here
				
				IF playerData[DART_PLAYER_HOME_SERVER].bResetDone AND playerData[DART_PLAYER_AWAY_SERVER].bResetDone
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is switching turns")
					// keep track of the last scores in case the current score has to be reset from a bust
					serverData.iLastScore[DART_PLAYER_HOME_SERVER] = serverData.iScores[DART_PLAYER_HOME_SERVER]
					serverData.iLastScore[DART_PLAYER_AWAY_SERVER] = serverData.iScores[DART_PLAYER_AWAY_SERVER]
					serverData.iTurnscore = 0
					
					// switch the player throwing and waiting
					IF serverData.iThrower = DART_PLAYER_HOME_SERVER //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
						serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_WAITING
						serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_THROWING
					ELSE
						serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_THROWING
						serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_WAITING
					ENDIF
					
					serverData.iServerTurn++
					serverData.bFinishedScoreTracking = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, FALSE)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, FALSE)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, FALSE)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, FALSE)
					serverData.DartsTimer.bNewShot = TRUE
				ENDIF
			ENDIF
			
			// don't really do winning here but have it be the way out of the throw off
			IF serverData.bDoWinning
				IF playerData[DART_PLAYER_HOME_SERVER].bResetDone AND playerData[DART_PLAYER_AWAY_SERVER].bResetDone
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is processing the winner")
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] serverData.fLength[DART_PLAYER_HOME_SERVER] = ", serverData.fLength[DART_PLAYER_HOME_SERVER])
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] serverData.fLength[DART_PLAYER_AWAY_SERVER] = ", serverData.fLength[DART_PLAYER_AWAY_SERVER])
					IF serverData.fLength[DART_PLAYER_HOME_SERVER] < serverData.fLength[DART_PLAYER_AWAY_SERVER]
						serverData.iWinner = ENUM_TO_INT(DART_PLAYER_HOME) //DART_PLAYER_HOME_SERVER 
					ELSE
						serverData.iWinner = ENUM_TO_INT(DART_PLAYER_AWAY) //DART_PLAYER_AWAY_SERVER 
					ENDIF
					
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] serverData.iWinner = ", serverData.iWinner)
					
					serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_WINNING
					serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_WINNING
					
					// this might have to be moved to the game start stage
					serverData.bFinishedScoreTracking = FALSE
					serverData.bDoWinning = FALSE
					serverData.mpState = DARTS_MPSTATE_SETUP
				ENDIF
			ENDIF
		
		ENDIF
	ENDIF
	
	// If both clients are in the wait for server state, kick them into the game. This is only possible if a player debugs
	IF playerData[DART_PLAYER_AWAY_SERVER].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
	AND playerData[DART_PLAYER_HOME_SERVER].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
		CPRINTLN(DEBUG_DARTS, "Server resetting because both clients are in DARTS_MPSTATE_WAIT_POST_GAME")
		
		// when both clients are waiting after the game, server tells them to go to menu, set's itself on the menu.
		DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_MOVETOMENU)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_MOVETOGAME)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_AWAYWIN)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_HOMEWIN)
		
		DARTS_SERVER_RESET_SCORES(serverData)
		
		// TODO: resolve this, it's out of date
		serverData.mpState = DARTS_MPSTATE_SETUP
	ENDIF
ENDPROC

PROC DARTS_SERVER_UPDATE_GAME(DARTS_SERVER_BROADCAST_DATA & serverData, DARTS_PLAYER_BROADCAST_DATA & playerData[], DART & Darts) //fill out the parameters later
	// dart game is in progress
	IF (playerData[DART_PLAYER_AWAY_SERVER].eMPState = DARTS_MPSTATE_IN_PROGRESS AND playerData[DART_PLAYER_HOME_SERVER].eMPState = DARTS_MPSTATE_IN_PROGRESS)
		
		IF (playerData[DART_PLAYER_AWAY_SERVER].eMPThrowStage = DARTS_MPTHROW_AIM OR playerData[DART_PLAYER_AWAY_SERVER].eMPWaitStage = DARTS_MPWAIT_AIM)
		AND (playerData[DART_PLAYER_HOME_SERVER].eMPThrowStage = DARTS_MPTHROW_AIM OR playerData[DART_PLAYER_HOME_SERVER].eMPWaitStage = DARTS_MPWAIT_AIM)
			
			DARTS_UPDATE_SHOT_CLOCK(serverData.DartsTimer)
			
		ENDIF
		
		IF (serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_THROWING OR serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_THROWING)
		AND NOT serverData.bFinishedScoreTracking
			
			IF (DARTS_CHECK_CLIENT_FLAG(PlayerData[DART_PLAYER_AWAY_SERVER], DARTS_CLIENTFLAG_DANGER_DART) OR DARTS_CHECK_CLIENT_FLAG(PlayerData[DART_PLAYER_HOME_SERVER], DARTS_CLIENTFLAG_DANGER_DART))
			AND (playerData[DART_PLAYER_AWAY_SERVER].eMPThrowStage = DARTS_MPTHROW_THROW OR playerData[DART_PLAYER_AWAY_SERVER].eMPWaitStage = DARTS_MPWAIT_THROW)
			AND (playerData[DART_PLAYER_HOME_SERVER].eMPThrowStage = DARTS_MPTHROW_THROW OR playerData[DART_PLAYER_HOME_SERVER].eMPWaitStage = DARTS_MPWAIT_THROW)
				ServerData.DartsTimer.bNewShot = TRUE
			ENDIF
			
			// handing the score recording after throws
			IF DARTS_SERVER_UPDATE_SCORING(Darts)
			AND DARTS_CHECK_CLIENT_FLAG(PlayerData[DART_PLAYER_HOME_SERVER], DARTS_CLIENTFLAG_SCORING_DONE)
			AND DARTS_CHECK_CLIENT_FLAG(PlayerData[DART_PLAYER_AWAY_SERVER], DARTS_CLIENTFLAG_SCORING_DONE)
				
				DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, FALSE)
				
				INT iCurrentScore
				IF serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_THROWING
					serverData.iThrower = DART_PLAYER_HOME_SERVER //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
					serverData.iWaiter = DART_PLAYER_AWAY_SERVER
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the thrower is DART_PLAYER_HOME_SERVER")
				ELSE
					serverData.iThrower = DART_PLAYER_AWAY_SERVER //ENUM_TO_INT(DART_PLAYER_AWAY_SERVER)
					serverData.iWaiter = DART_PLAYER_HOME_SERVER
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the thrower is DART_PLAYER_AWAY_SERVER")
				ENDIF
				
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that playerData[DART_PLAYER_HOME_SERVER].ClientDart.iHitValue = ", playerData[DART_PLAYER_HOME_SERVER].ClientDart.iHitValue)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that playerData[DART_PLAYER_AWAY_SERVER].ClientDart.iHitValue = ", playerData[DART_PLAYER_AWAY_SERVER].ClientDart.iHitValue)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that Darts.iHitValue = ", Darts.iHitValue)
				
				serverData.iTurnscore += Darts.iHitValue
				iCurrentScore = serverData.iScores[serverData.iThrower] - Darts.iHitValue
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] ServerData.iTurnscore = ", serverData.iTurnscore)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server iCurrentScore = ", iCurrentScore)
				
				IF playerData[DART_PLAYER_HOME_SERVER].bDoubleThrow
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_HOME_SERVER].bDoubleThrow = TRUE")
				ELSE
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_HOME_SERVER].bDoubleThrow = FALSE")
				ENDIF
				IF playerData[DART_PLAYER_AWAY_SERVER].bDoubleThrow
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_AWAY_SERVER].bDoubleThrow = TRUE")
				ELSE
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_AWAY_SERVER].bDoubleThrow = FALSE")
				ENDIF
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Darts.iHitMultiplier = ", Darts.iHitMultiplier) 
				
				// if game has been won
				IF iCurrentScore = 0 AND ((Darts.iHitMultiplier = 2) OR (Darts.iHitValue = 50))
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found a winner")
					serverData.iScores[serverData.iThrower] = iCurrentScore
					serverData.bDoWinning = TRUE
				
				// if player has bust
				ELIF iCurrentScore = 1 OR iCurrentScore < 0 OR (iCurrentScore = 0 AND playerData[serverData.iThrower].bDoubleThrow = FALSE)
					CPRINTLN(DEBUG_DARTS, "Current throwing player has bust, changing turns")
					serverData.iScores[serverData.iThrower] = serverData.iLastScore[serverData.iThrower]
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, TRUE) //serverData.bDoSwitchPlayer = TRUE
					IF iCurrentScore < 0
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, TRUE) //serverData.bTurnBust = TRUE
					ELIF (iCurrentScore = 1)
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, TRUE)
					ELIF ((iCurrentScore = 0) AND (playerData[serverData.iThrower].bDoubleThrow = FALSE))
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, TRUE)
					ENDIF
					
				// normal scoring throw
				ELSE
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is processing the score")
					//serverData.iScores[serverData.iThrower] = iCurrentScore
					serverData.iServerTurn++
					serverData.iScores[serverData.iThrower] = iCurrentScore
					
					IF serverData.iServerTurn >= 3
						CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the turn shall end")
						serverData.iServerTurn--
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, TRUE) //serverData.bDoSwitchPlayer = TRUE
					ELSE
						CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the turn shall continue")
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE, TRUE) //serverData.bTurnContinue = TRUE
					ENDIF				
				ENDIF
			
				serverData.bFinishedScoreTracking = TRUE
				
				serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_SCORE_CHECK
				serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_SCORE_CHECK
			ENDIF
		
		ELIF serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_SCORE_CHECK
		OR serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_SCORE_CHECK
			
			serverData.DartsTimer.bTimeRunOut = FALSE
			
			//CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is in SCORE CHECK")
			// handle setting the winner
			IF serverData.bDoWinning
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is processing the winner")
				IF serverData.iThrower = DART_PLAYER_HOME_SERVER //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
					serverData.iWinner = DART_PLAYER_HOME_SERVER //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
				ELSE
					serverData.iWinner = DART_PLAYER_AWAY_SERVER //ENUM_TO_INT(DART_PLAYER_AWAY_SERVER)
				ENDIF
				
				serverData.iLegs[serverData.iWinner]++
				
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server [", serverData.iWinner, "] has won this many legs: ", serverData.iLegs[serverData.iWinner])
				
				IF EVALUATE_DART_GAME_PROGRESS(serverData)
//					serverData.iLegs[DART_PLAYER_HOME_SERVER] = 0
//					serverData.iLegs[DART_PLAYER_AWAY_SERVER] = 0
//					serverData.iSets[DART_PLAYER_HOME_SERVER] = 0
//					serverData.iSets[DART_PLAYER_AWAY_SERVER] = 0
					
					DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
				ELSE
					DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
				ENDIF
				
				serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_WINNING
				serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_WINNING
				
				// this might have to be moved to the game start stage
				serverData.bFinishedScoreTracking = FALSE
				serverData.mpState = DARTS_MPSTATE_SETUP
			ENDIF
			
			// handle turn continuing
			IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE) //serverData.bTurnContinue
				DEBUG_MESSAGE_PERIODIC("[MP DARTS] Server is continuing turn", iDartsServerThrottle)
				IF playerData[DART_PLAYER_HOME_SERVER].bResetDone AND playerData[DART_PLAYER_AWAY_SERVER].bResetDone
					serverData.mpTurnStatus[serverData.iThrower] = DARTS_MPTURN_THROWING
					serverData.mpTurnStatus[serverData.iWaiter] =  DARTS_MPTURN_WAITING
					serverData.bFinishedScoreTracking = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE, FALSE) //serverData.bTurnContinue = FALSE
					serverData.DartsTimer.bNewShot = TRUE
				ENDIF
			ENDIF
			
			// handle switching turns
			IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER) //serverData.bDoSwitchPlayer
				// set this client flag so that both clients know to clear the board
				// maybe do some printing here
				
				IF playerData[DART_PLAYER_HOME_SERVER].bResetDone AND playerData[DART_PLAYER_AWAY_SERVER].bResetDone
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is switching turns")
					// keep track of the last scores in case the current score has to be reset from a bust
					serverData.iLastScore[DART_PLAYER_HOME_SERVER] = serverData.iScores[DART_PLAYER_HOME_SERVER]
					serverData.iLastScore[DART_PLAYER_AWAY_SERVER] = serverData.iScores[DART_PLAYER_AWAY_SERVER]
					serverData.iTurnscore = 0
					
					// switch the player throwing and waiting
					IF serverData.iThrower = DART_PLAYER_HOME_SERVER //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
						serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_WAITING
						serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_THROWING
					ELSE
						serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_THROWING
						serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_WAITING
					ENDIF
					
					serverData.iServerTurn = 0
					serverData.bFinishedScoreTracking = FALSE
					//DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, FALSE) //serverData.bDoSwitchPlayer = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, FALSE) //serverData.bTurnBust = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, FALSE)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, FALSE)
					serverData.DartsTimer.bNewShot = TRUE
				ENDIF
			ENDIF
			
		ELIF serverData.mpTurnStatus[DART_PLAYER_AWAY_SERVER] = DARTS_MPTURN_WINNING
		OR serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_WINNING
			IF PlayerData[DART_PLAYER_AWAY_SERVER].bDoRematch AND PlayerData[DART_PLAYER_HOME_SERVER].bDoRematch
				serverData.mpState = DARTS_MPSTATE_SETUP
			ENDIF
		ENDIF
		
	ENDIF
	
	// If both clients are in the wait for server state, kick them into the game.
	IF playerData[DART_PLAYER_AWAY_SERVER].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
	AND playerData[DART_PLAYER_HOME_SERVER].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
		DEBUG_MESSAGE("Server resetting because both clients are in DARTS_MPSTATE_WAIT_POST_GAME")
		
		// when both clients are waiting after the game, server tells them to go to menu, set's itself on the menu.
		DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_MOVETOMENU)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_MOVETOGAME)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_AWAYWIN)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_HOMEWIN)
		
		DARTS_SERVER_RESET_SCORES(serverData)
		
		// TODO: resolve this, it's out of date
		serverData.mpState = DARTS_MPSTATE_SETUP
	ENDIF
	
ENDPROC

PROC DARTS_SERVER_UPDATE_SINGLE_PLAYER_GAME(DARTS_SERVER_BROADCAST_DATA & serverData, DARTS_PLAYER_BROADCAST_DATA & playerData[], DART & Darts) //fill out the parameters later
	// dart game is in progress
	IF playerData[DART_PLAYER_HOME_SERVER].eMPState = DARTS_MPSTATE_IN_PROGRESS
		
		IF playerData[DART_PLAYER_HOME_SERVER].eMPThrowStage = DARTS_MPTHROW_AIM
			DARTS_UPDATE_SHOT_CLOCK(serverData.DartsTimer)
		ENDIF
		
		IF serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_THROWING
		AND NOT serverData.bFinishedScoreTracking
			
			IF (DARTS_CHECK_CLIENT_FLAG(PlayerData[DART_PLAYER_HOME_SERVER], DARTS_CLIENTFLAG_DANGER_DART))
			AND (playerData[DART_PLAYER_HOME_SERVER].eMPThrowStage = DARTS_MPTHROW_THROW)
				ServerData.DartsTimer.bNewShot = TRUE
			ENDIF
			
			// handing the score recording after throws
			IF DARTS_SERVER_UPDATE_SCORING(Darts)
				
				DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, FALSE)
				
				INT iCurrentScore
				serverData.iThrower = DART_PLAYER_HOME_SERVER //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the thrower is DART_PLAYER_HOME_SERVER")
				
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that playerData[DART_PLAYER_HOME_SERVER].ClientDart.iHitValue = ", playerData[DART_PLAYER_HOME_SERVER].ClientDart.iHitValue)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that Darts.iHitValue = ", Darts.iHitValue)
				
				serverData.iTurnscore += Darts.iHitValue//playerData[serverData.iThrower].ClientDart.iHitValue
				iCurrentScore = serverData.iScores[serverData.iThrower] - Darts.iHitValue//playerData[serverData.iThrower].ClientDart.iHitValue//serverData.iTurnscore
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] ServerData.iTurnscore = ", serverData.iTurnscore)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server iCurrentScore = ", iCurrentScore)
				
				IF playerData[DART_PLAYER_HOME_SERVER].bDoubleThrow
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_HOME_SERVER].bDoubleThrow = TRUE")
				ELSE
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_HOME_SERVER].bDoubleThrow = FALSE")
				ENDIF
				
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Darts.iHitMultiplier = ", Darts.iHitMultiplier) 
				
				// if game has been won
				IF iCurrentScore = 0 AND ((Darts.iHitMultiplier = 2) OR (Darts.iHitValue = 50)) //playerData[serverData.iThrower].bDoubleThrow
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found a winner")
					serverData.iScores[serverData.iThrower] = iCurrentScore
					serverData.bDoWinning = TRUE
				
				// if player has bust
				ELIF iCurrentScore = 1 OR iCurrentScore < 0 OR (iCurrentScore = 0 AND playerData[serverData.iThrower].bDoubleThrow = FALSE)
					CPRINTLN(DEBUG_DARTS, "Current throwing player has bust, changing turns")
					serverData.iScores[serverData.iThrower] = serverData.iLastScore[serverData.iThrower]
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, TRUE) //serverData.bDoSwitchPlayer = TRUE
					IF iCurrentScore < 0
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, TRUE) //serverData.bTurnBust = TRUE
					ELIF (iCurrentScore = 1)
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, TRUE)
					ELIF ((iCurrentScore = 0) AND (playerData[serverData.iThrower].bDoubleThrow = FALSE))
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, TRUE)
					ENDIF
					
				// normal scoring throw
				ELSE
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is processing the score")
					//serverData.iScores[serverData.iThrower] = iCurrentScore
					serverData.iServerTurn++
					serverData.iScores[serverData.iThrower] = iCurrentScore
					
					IF serverData.iServerTurn >= 3
						CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the turn shall end")
						serverData.iServerTurn--
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, TRUE) //serverData.bDoSwitchPlayer = TRUE
					ELSE
						CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the turn shall continue")
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE, TRUE) //serverData.bTurnContinue = TRUE
					ENDIF				
				ENDIF
			
				serverData.bFinishedScoreTracking = TRUE
				
				serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_SCORE_CHECK
			ENDIF
		
		ELIF serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_SCORE_CHECK
			
			serverData.DartsTimer.bTimeRunOut = FALSE
			
			//CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is in SCORE CHECK")
			// handle setting the winner
			IF serverData.bDoWinning
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is processing the winner")
				serverData.iWinner = DART_PLAYER_HOME_SERVER //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
				
				serverData.iLegs[serverData.iWinner]++
				
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server [", serverData.iWinner, "] has won this many legs: ", serverData.iLegs[serverData.iWinner])
				
				IF EVALUATE_DART_GAME_PROGRESS(serverData)
//					serverData.iLegs[DART_PLAYER_HOME_SERVER] = 0
//					serverData.iLegs[DART_PLAYER_AWAY_SERVER] = 0
//					serverData.iSets[DART_PLAYER_HOME_SERVER] = 0
//					serverData.iSets[DART_PLAYER_AWAY_SERVER] = 0
					
					DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
				ELSE
					DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
				ENDIF
				
				serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_WINNING
				
				// this might have to be moved to the game start stage
				serverData.bFinishedScoreTracking = FALSE
				serverData.mpState = DARTS_MPSTATE_SETUP
			ENDIF
			
			// handle turn continuing
			IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE) //serverData.bTurnContinue
				DEBUG_MESSAGE_PERIODIC("[MP DARTS] Server is continuing turn", iDartsServerThrottle)
				IF playerData[DART_PLAYER_HOME_SERVER].bResetDone
					serverData.mpTurnStatus[serverData.iThrower] = DARTS_MPTURN_THROWING
					serverData.bFinishedScoreTracking = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE, FALSE) //serverData.bTurnContinue = FALSE
					serverData.DartsTimer.bNewShot = TRUE
				ENDIF
			ENDIF
			
			// handle switching turns
			IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER) //serverData.bDoSwitchPlayer
				// set this client flag so that both clients know to clear the board
				// maybe do some printing here
				
				IF playerData[DART_PLAYER_HOME_SERVER].bResetDone
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is switching turns")
					// keep track of the last scores in case the current score has to be reset from a bust
					serverData.iLastScore[DART_PLAYER_HOME_SERVER] = serverData.iScores[DART_PLAYER_HOME_SERVER]
					serverData.iTurnscore = 0
					
					// single player never waits, so just re-inforce throwing
					serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_THROWING
					
					serverData.iServerTurn = 0
					serverData.bFinishedScoreTracking = FALSE
					//DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, FALSE) //serverData.bDoSwitchPlayer = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, FALSE) //serverData.bTurnBust = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, FALSE)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, FALSE)
					serverData.DartsTimer.bNewShot = TRUE
				ENDIF
			ENDIF
			
		ELIF serverData.mpTurnStatus[DART_PLAYER_HOME_SERVER] = DARTS_MPTURN_WINNING
			IF PlayerData[DART_PLAYER_HOME_SERVER].bDoRematch
				serverData.mpState = DARTS_MPSTATE_SETUP
			ENDIF
		ENDIF
		
	ENDIF
	
	// If both clients are in the wait for server state, kick them into the game.
	IF playerData[DART_PLAYER_HOME_SERVER].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
		CPRINTLN(DEBUG_DARTS, "Server resetting because both clients are in DARTS_MPSTATE_WAIT_POST_GAME")
		
		// when both clients are waiting after the game, server tells them to go to menu, set's itself on the menu.
		DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_MOVETOMENU)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_MOVETOGAME)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_AWAYWIN)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_HOMEWIN)
		
		DARTS_SERVER_RESET_SCORES(serverData)
		
		// TODO: resolve this, it's out of date
		serverData.mpState = DARTS_MPSTATE_SETUP
	ENDIF
	
ENDPROC

