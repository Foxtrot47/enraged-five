////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    Darts_Board.sch                         //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Board Defs           			      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_entity.sch"
USING "commands_streaming.sch"
USING "Darts_Args.sch"

//MODEL_NAMES modelDartBoard = PROP_DART_BD_01
MODEL_NAMES modelDartBoard = PROP_DART_BD_CAB_01 

//Darts Board
STRUCT DARTS_BOARD
	OBJECT_INDEX 	oiDartBoard
	VECTOR 			vDartBoard
	FLOAT 			fBoardHeading	
ENDSTRUCT

//VECTOR 	vLostHideoutDartBoard = << 989.097, -100.356, 75.579 >>
//FLOAT		fLostHideoutDartBoardHeading = 222.400

INT iScoreForSection[21]

//offset of the reticule from the object, to get as close to the board as possible
FLOAT fDartBoardOriginOffset = 0.063 //0.08 //0.18

//offset of the center of the object to the center of the board
VECTOR exactCentreOfBoardXZOffset = << -0.0035, 0.0, -0.001 >>

PROC INIT_BOARD(DARTS_BOARD & DartsBoard, DARTS_ARGS & DartsArgs)
	
	DartsBoard.vDartBoard 	 = DartsArgs.vDartBoard 
	DartsBoard.fBoardHeading = DartsArgs.fBoardHeading 
	
	IF DOES_ENTITY_EXIST(DartsArgs.oiDartBoard)
		DartsBoard.oiDartBoard = DartsArgs.oiDartBoard
	ENDIF
	
	// The scores on the dartboard, running clockwise from the 20
	iScoreForSection[0] = 20	iScoreForSection[10] = 3
	iScoreForSection[1] = 1		iScoreForSection[11] = 19
	iScoreForSection[2] = 18  	iScoreForSection[12] = 7
	iScoreForSection[3] = 4		iScoreForSection[13] = 16
	iScoreForSection[4] = 13  	iScoreForSection[14] = 8
	iScoreForSection[5] = 6	    iScoreForSection[15] = 11
	iScoreForSection[6] = 10  	iScoreForSection[16] = 14
	iScoreForSection[7] = 15  	iScoreForSection[17] = 9
	iScoreForSection[8] = 2		iScoreForSection[18] = 12
	iScoreForSection[9] = 17  	iScoreForSection[19] = 5
								iScoreForSection[20] = 20
ENDPROC

FUNC VECTOR GET_DB_VECTOR(DARTS_BOARD& DB)
	RETURN DB.vDartBoard
ENDFUNC

PROC SET_DB_VECTOR(DARTS_BOARD& DB, VECTOR vDartBoard)
	DB.vDartBoard = vDartboard
ENDPROC

FUNC FLOAT GET_DB_HEADING(DARTS_BOARD& DB)
	RETURN DB.fBoardHeading
ENDFUNC

PROC SET_DB_HEADING(DARTS_BOARD& DB, FLOAT fHeading)
	DB.fBoardHeading = fHeading
ENDPROC

