////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    MP_Darts_Server_lib.sch                 //
// AUTHOR          :    Lino Manansala                         //
// DESCRIPTION     :    Dart Server Procs and Funcs		      //
//                                                           //
//////////////////////////////////////////////////////////////

USING "MP_Darts_Server.sch"
USING "MP_Darts_apartment_client.sch"
USING "MP_Darts.sch"
USING "Darts_Player.sch"
USING "Darts_UI.sch"

INT iDartsServerThrottle

PROC DARTS_SERVER_THROW_OFF_SETUP(DARTS_SERVER_BROADCAST_DATA & serverData)
	
	serverData.iCoinFlip = GET_RANDOM_INT_IN_RANGE(0, 1000)	
	serverData.iCoinFlip = serverData.iCoinFlip % 2
	
	//IF serverData.iCoinFlip = 0
		serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_THROWING
		serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_WAITING
	//ELSE
		serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
		serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_WAITING
	//ENDIF
	
	DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_DO_THROWOFF)
	serverData.DartsTimer.bNewShot = TRUE
	
	DARTS_INIT_TIMERS(serverData.DartsTimer)
ENDPROC

PROC DARTS_SERVER_GAME_SETUP(DARTS_SERVER_BROADCAST_DATA & serverData, BOOL bSinglePlayer, INT iSinglePlayerID)
	iSinglePlayerID = iSinglePlayerID 
	// Initialise the scoreboard
	// Reset the player scores to 301
	INT i
	REPEAT DARTSMP_IDS i
	//	DartGame.ScoreBoard[i] 		= INIT_DART_SCORE
		serverData.iScores[i] 		= 301
		serverData.iLastScore[i] 	= 301
	ENDREPEAT
	
	IF NOT DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
		serverData.iLegs[DARTS_PLAYER_ONE] = 0
		serverData.iLegs[DARTS_PLAYER_TWO] = 0
		serverData.iSets[DARTS_PLAYER_ONE] = 0
		serverData.iSets[DARTS_PLAYER_TWO] = 0
	ENDIF
	
	INT iLegs = g_FMMC_STRUCT.iNumberOfLegs
	INT iSets = g_FMMC_STRUCT.iNumberOfSets
	
	CPRINTLN(DEBUG_DARTS, "[MP DARTS] setup iLegs = ", iLegs)
	CPRINTLN(DEBUG_DARTS, "[MP DARTS] setup iSets = ", iSets)
	
	SWITCH iLegs
		CASE 0
			serverData.iNumLegs = 0
		BREAK
		CASE 1
			serverData.iNumLegs = 3
		BREAK
		CASE 2
			serverData.iNumLegs = 5
		BREAK
	ENDSWITCH
	
	SWITCH iSets
		CASE 0
			serverData.iNumSets = 0
		BREAK
		CASE 1
			serverData.iNumSets = 1
		BREAK
		CASE 2
			serverData.iNumSets = 2
		BREAK
		CASE 3
			serverData.iNumSets = 3
		BREAK
		CASE 4
			serverData.iNumSets = 4
		BREAK
		CASE 5
			serverData.iNumSets = 5 
		BREAK
		CASE 6
			serverData.iNumSets = 15  // not used for clubhouse
		BREAK
	ENDSWITCH
	
	IF bSinglePlayer
		serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
		
	ELSE
		// pick the first thrower at random for initial game,
		// for ever rematch after, alternate the first throwers
		IF NOT serverData.bDoWinning
			//serverData.iCoinFlip = GET_RANDOM_INT_IN_RANGE(0, 1000)	
			//serverData.iCoinFlip = serverData.iCoinFlip % 2
			CPRINTLN(DEBUG_DARTS, "[MP DARTS] serverData.iWinner = ", serverData.iWinner)
			serverData.iCoinFlip = serverData.iWinner
			CPRINTLN(DEBUG_DARTS, "[MP DARTS] serverData.iCoinFlip = ", serverData.iCoinFlip)
		ELSE
			serverData.iCoinFlip = 1 - serverData.iCoinFlip
		ENDIF
		
		IF serverData.iCoinFlip = 1
			serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_THROWING
			CPRINTLN(DEBUG_DARTS, "[MP DARTS] TWO IS TROWING")
			serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_WAITING
			CPRINTLN(DEBUG_DARTS, "[MP DARTS] ONE IS WAITING")
		ELSE
			serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
			CPRINTLN(DEBUG_DARTS, "[MP DARTS] ONE IS TROWING")
			serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_WAITING
			CPRINTLN(DEBUG_DARTS, "[MP DARTS] TWO IS WAITING")
		ENDIF
	ENDIF
	
	serverData.iServerTurn = 0
	serverData.iTurnscore = 0
	serverData.iUIFlags = 0
	
	serverData.bDoWinning = FALSE
	serverData.bTurnContinue = FALSE
	serverData.bTurnEnd = FALSE
	serverData.bTurnBust = FALSE
	serverData.bDoSwitchPlayer = FALSE
	serverData.DartsTimer.bNewShot = TRUE
	
	DARTS_INIT_TIMERS(serverData.DartsTimer)
ENDPROC

// keep track of score server side, this should happen only at the end of turns
FUNC BOOL DARTS_SERVER_UPDATE_SCORING(DART & Darts)
	//RETURN TRUE
	//serverData.iScores[DartsPlayer] -= iTurnScore
	//RETURN TRUE
	IF Darts.bDoneScoring
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT DARTS_SERVER_GET_LEGS_NEEDED(DARTS_SERVER_BROADCAST_DATA & serverData)
	SWITCH serverData.iNumLegs
		CASE 1	RETURN 1
		CASE 3	RETURN 1
		CASE 5	RETURN 1
	ENDSWITCH
	
	RETURN 1
ENDFUNC

FUNC BOOL EVALUATE_DART_GAME_PROGRESS(DARTS_SERVER_BROADCAST_DATA & serverData)
	CDEBUG3LN(DEBUG_DARTS, "EVALUATE_DART_GAME_PROGRESS - GAME CHECKED")
	INT iEvaluator = serverData.iWinner
	INT iWinsNeeded
	
	iWinsNeeded = DARTS_SERVER_GET_LEGS_NEEDED(serverData)
	CDEBUG3LN(DEBUG_DARTS, "EVALUATE_DART_GAME_PROGRESS - Info iLegs: ", serverData.iLegs[iEvaluator], " Needed: ", iWinsNeeded)
	IF (serverData.iLegs[iEvaluator] = iWinsNeeded)
		serverData.iSets[iEvaluator]++
		serverData.iLegs[DARTS_PLAYER_ONE] = 0
		serverData.iLegs[DARTS_PLAYER_TWO] = 0
		DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_SET_JUST_WON)
		
		CDEBUG3LN(DEBUG_DARTS, "EVALUATE_DART_GAME_PROGRESS - Set just won by ", iEvaluator, ", number of sets won is ", serverData.iSets[iEvaluator])
		
		CDEBUG3LN(DEBUG_DARTS, "EVALUATE_DART_GAME_PROGRESS - To WIN Need Games: ", serverData.iSets[iEvaluator], ", number of curent games won is ", serverData.iNumSets)
		IF (serverData.iSets[iEvaluator] = serverData.iNumSets)
			RETURN TRUE
		ENDIF
	ELSE
		IF DARTS_SERVER_CHECK_FLAG(serverData, DARTSERVERFLAG_SET_JUST_WON)
			DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_SET_JUST_WON)
		ENDIF
		
		//continue on until the set is completed
		CDEBUG3LN(DEBUG_DARTS, "EVALUATE_DART_GAME_PROGRESS - leg just won by ", iEvaluator, ", number of legs won is ", serverData.iLegs[iEvaluator])
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DARTS_SERVER_THROW_OFF_UPDATE(DARTS_SERVER_BROADCAST_DATA & serverData, DARTS_AP_PLAYER_BROADCAST_DATA & playerData[], DART & Darts, INT & iDartPlayerID[2])

	IF playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPState = DARTS_MPSTATE_THROW_OFF
	AND playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState = DARTS_MPSTATE_THROW_OFF
		IF (playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage = DARTS_MPTHROW_AIM OR playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPWaitStage = DARTS_MPWAIT_AIM)
		AND (playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage = DARTS_MPTHROW_AIM OR playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPWaitStage = DARTS_MPWAIT_AIM)
			
			DARTS_UPDATE_THROW_OFF_CLOCK(serverData.DartsTimer)
			
		ENDIF
		
	CDEBUG1LN(DEBUG_DARTS, "IN THROW OFF")	
		IF (serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_THROWING 
			OR serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING)
		AND NOT serverData.bFinishedScoreTracking
			// handing the score recording after throws
			CDEBUG1LN(DEBUG_DARTS, 
			"DSC:", DARTS_SERVER_UPDATE_SCORING(Darts),
			"FLAG ONE:", DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SCORING_DONE),
			"FLAG TWO:", DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_SCORING_DONE)
			)	
			
			IF DARTS_SERVER_UPDATE_SCORING(Darts)
			AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SCORING_DONE)
			AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_SCORING_DONE)
			CDEBUG1LN(DEBUG_DARTS, "IN THROW OFF3")	
				IF serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
					serverData.iThrower = DARTS_PLAYER_ONE
					serverData.iWaiter = DARTS_PLAYER_TWO
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the thrower is DARTS_PLAYER_ONE")
				ELSE
					serverData.iThrower = DARTS_PLAYER_TWO
					serverData.iWaiter = DARTS_PLAYER_ONE
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the thrower is DARTS_PLAYER_TWO")
				ENDIF
				
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that playerData[DART_PLAYER_HOME_SERVER].ClientDart.fLength = ", playerData[iDartPlayerID[DARTS_PLAYER_ONE]].ClientDart.fLength)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that playerData[DART_PLAYER_AWAY_SERVER].ClientDart.fLength = ", playerData[iDartPlayerID[DARTS_PLAYER_TWO]].ClientDart.fLength)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that Darts.fLength = ", Darts.fLength)
				
				serverData.fLength[serverData.iThrower] = Darts.fLength
				
				IF (serverData.iThrower = DARTS_PLAYER_ONE)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, TRUE)
				ELSE
					serverData.bDoWinning = TRUE
				ENDIF
				
				serverData.bFinishedScoreTracking = TRUE
				
				serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_SCORE_CHECK
				serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_SCORE_CHECK
			ENDIF
		
		ELIF serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_SCORE_CHECK
		OR serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_SCORE_CHECK
			
			serverData.DartsTimer.bTimeRunOut = FALSE
			
			// handle switching turns
			IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER)
				// set this client flag so that both clients know to clear the board
				// maybe do some printing here
				
				//IF playerData[iDartPlayerID[DARTS_PLAYER_ONE]].bResetDone AND playerData[iDartPlayerID[DARTS_PLAYER_TWO]].bResetDone
				IF 	GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
				AND GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is switching turns")
					// keep track of the last scores in case the current score has to be reset from a bust
					serverData.iLastScore[DARTS_PLAYER_ONE] = serverData.iScores[DARTS_PLAYER_ONE]
					serverData.iLastScore[DARTS_PLAYER_TWO] = serverData.iScores[DARTS_PLAYER_TWO]
					serverData.iTurnscore = 0
					
					// switch the player throwing and waiting
					IF serverData.iThrower = DARTS_PLAYER_ONE 
						serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_WAITING
						serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_THROWING
					ELSE
						serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
						serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_WAITING
					ENDIF
					
					serverData.iServerTurn++
					serverData.bFinishedScoreTracking = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, FALSE)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, FALSE)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, FALSE)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, FALSE)
					serverData.DartsTimer.bNewShot = TRUE
				ENDIF
			ENDIF
			
			// don't really do winning here but have it be the way out of the throw off
			IF serverData.bDoWinning
				//IF playerData[iDartPlayerID[DARTS_PLAYER_ONE]].bResetDone AND playerData[iDartPlayerID[DARTS_PLAYER_TWO]].bResetDone
				IF 	GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
				AND GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)	
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is processing the winner")
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] serverData.fLength[DARTS_PLAYER_ONE] = ", serverData.fLength[DARTS_PLAYER_ONE])
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] serverData.fLength[DARTS_PLAYER_TWO] = ", serverData.fLength[DARTS_PLAYER_TWO])
					IF serverData.fLength[DARTS_PLAYER_ONE] < serverData.fLength[DARTS_PLAYER_TWO]
						serverData.iWinner = DARTS_PLAYER_ONE //DART_PLAYER_HOME_SERVER 
					ELSE
						serverData.iWinner = DARTS_PLAYER_TWO //DART_PLAYER_AWAY_SERVER 
					ENDIF
					
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] serverData.iWinner = ", serverData.iWinner)
					
					serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_WINNING
					serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_WINNING
					
					// this might have to be moved to the game start stage
					serverData.bFinishedScoreTracking = FALSE
					serverData.bDoWinning = FALSE
					serverData.mpState = DARTS_MPSTATE_SETUP
				ENDIF
			ENDIF
		
		ENDIF
	ENDIF
	
	// If both clients are in the wait for server state, kick them into the game. This is only possible if a player debugs
	IF playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
	AND playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
		CPRINTLN(DEBUG_DARTS, "Server resetting because both clients are in DARTS_MPSTATE_WAIT_POST_GAME")
		
		// when both clients are waiting after the game, server tells them to go to menu, set's itself on the menu.
		DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_MOVETOMENU)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_MOVETOGAME)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_AWAYWIN)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_HOMEWIN)
		
		DARTS_SERVER_RESET_SCORES(serverData)
		
		// TODO: resolve this, it's out of date
		serverData.mpState = DARTS_MPSTATE_SETUP
	ENDIF
ENDPROC

PROC DARTS_SERVER_UPDATE_GAME(DARTS_SERVER_BROADCAST_DATA & serverData, DARTS_AP_PLAYER_BROADCAST_DATA & playerData[], DART & Darts, INT & iDartPlayerID[2]) //fill out the parameters later

	
	// dart game is in progress
	IF (playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPState = DARTS_MPSTATE_IN_PROGRESS AND playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState = DARTS_MPSTATE_IN_PROGRESS)
			IF GET_GAME_TIMER() % 1000 > 50
				//CDEBUG1LN(DEBUG_DARTS, "in prog")
			ENDIF
		IF (playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage = DARTS_MPTHROW_AIM OR playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPWaitStage = DARTS_MPWAIT_AIM)
		AND (playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage = DARTS_MPTHROW_AIM OR playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPWaitStage = DARTS_MPWAIT_AIM)
			
			DARTS_UPDATE_SHOT_CLOCK(serverData.DartsTimer)
			
		ENDIF
		
//		IF GET_GAME_TIMER() % 1000 > 50
//				CDEBUG1LN(DEBUG_DARTS, 
//				"opnethrowing:", serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING,
//				"twothrowing:", serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_THROWING)
//			ENDIF
			
		IF (serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_THROWING OR serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING)
		AND NOT serverData.bFinishedScoreTracking
			IF (DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_DANGER_DART)
			OR DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_DANGER_DART))
			AND (playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPThrowStage = DARTS_MPTHROW_THROW OR playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPWaitStage = DARTS_MPWAIT_THROW)
			AND (playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage = DARTS_MPTHROW_THROW OR playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPWaitStage = DARTS_MPWAIT_THROW)
				ServerData.DartsTimer.bNewShot = TRUE
			ENDIF
			
//						CDEBUG1LN(DEBUG_DARTS, 
//			"DSC:", DARTS_SERVER_UPDATE_SCORING(Darts),
//			"FLAG ONE:", DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SCORING_DONE),
//			"FLAG TWO:", DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_SCORING_DONE)
//			)	
						
			// handing the score recording after throws
			IF DARTS_SERVER_UPDATE_SCORING(Darts)
			AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SCORING_DONE)
			AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_SCORING_DONE)
				
				DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, FALSE)
				
				INT iCurrentScore
				IF serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
					serverData.iThrower = DARTS_PLAYER_ONE //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
					serverData.iWaiter = DARTS_PLAYER_TWO
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the thrower is DARTS_PLAYER_ONE")
				ELSE
					serverData.iThrower = DARTS_PLAYER_TWO //ENUM_TO_INT(DART_PLAYER_AWAY_SERVER)
					serverData.iWaiter = DARTS_PLAYER_ONE
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the thrower is DARTS_PLAYER_TWO")
				ENDIF
				
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that playerData[DARTS_PLAYER_ONE].ClientDart.iHitValue = ", playerData[iDartPlayerID[DARTS_PLAYER_ONE]].ClientDart.iHitValue)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that playerData[DARTS_PLAYER_TWO].ClientDart.iHitValue = ", playerData[iDartPlayerID[DARTS_PLAYER_TWO]].ClientDart.iHitValue)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server sees that Darts.iHitValue = ", Darts.iHitValue)
				
				serverData.iTurnscore += Darts.iHitValue
				iCurrentScore = serverData.iScores[serverData.iThrower] - Darts.iHitValue
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] ServerData.iTurnscore = ", serverData.iTurnscore)
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server iCurrentScore = ", iCurrentScore)
				
				//IF playerData[iDartPlayerID[DARTS_PLAYER_ONE]].bDoubleThrow
				IF 	GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW)
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_HOME_SERVER].bDoubleThrow = TRUE")
				ELSE
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_HOME_SERVER].bDoubleThrow = FALSE")
				ENDIF
				//IF playerData[iDartPlayerID[DARTS_PLAYER_TWO]].bDoubleThrow
				IF 	GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW)
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_AWAY_SERVER].bDoubleThrow = TRUE")
				ELSE
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_AWAY_SERVER].bDoubleThrow = FALSE")
				ENDIF
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Darts.iHitMultiplier = ", Darts.iHitMultiplier) 
				
				// if game has been won
				IF iCurrentScore = 0 AND ((Darts.iHitMultiplier = 2) OR (Darts.iHitValue = 50))
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found a winner")
					serverData.iScores[serverData.iThrower] = iCurrentScore
					serverData.bDoWinning = TRUE
				
				// if player has bust
				ELIF iCurrentScore = 1 OR iCurrentScore < 0 
				OR (iCurrentScore = 0 AND GET_PLAYER_STATE(playerData[iDartPlayerID[serverData.iThrower]], DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW) = FALSE)
					CPRINTLN(DEBUG_DARTS, "Current throwing player has bust, changing turns")
					serverData.iScores[serverData.iThrower] = serverData.iLastScore[serverData.iThrower]
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, TRUE) //serverData.bDoSwitchPlayer = TRUE
					IF iCurrentScore < 0
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, TRUE) //serverData.bTurnBust = TRUE
					ELIF (iCurrentScore = 1)
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, TRUE)
					ELIF ((iCurrentScore = 0) AND (GET_PLAYER_STATE(playerData[iDartPlayerID[serverData.iThrower]], DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW) = FALSE))
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, TRUE)
					ENDIF
					
				// normal scoring throw
				ELSE
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is processing the score")
					//serverData.iScores[serverData.iThrower] = iCurrentScore
					serverData.iServerTurn++
					serverData.iScores[serverData.iThrower] = iCurrentScore
					
					IF serverData.iServerTurn >= 3
						CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the turn shall end")
						serverData.iServerTurn--
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, TRUE) //serverData.bDoSwitchPlayer = TRUE
					ELSE
						CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server has found that the turn shall continue")
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE, TRUE) //serverData.bTurnContinue = TRUE
					ENDIF				
				ENDIF
			
				serverData.bFinishedScoreTracking = TRUE
				
				serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_SCORE_CHECK
				serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_SCORE_CHECK
			ENDIF
		
		ELIF serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_SCORE_CHECK
		OR serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_SCORE_CHECK	
			serverData.DartsTimer.bTimeRunOut = FALSE
			
			//CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is in SCORE CHECK")
			// handle setting the winner
			IF serverData.bDoWinning
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is processing the winner")
				IF serverData.iThrower = DARTS_PLAYER_ONE //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
					serverData.iWinner = DARTS_PLAYER_ONE //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
				ELSE
					serverData.iWinner = DARTS_PLAYER_TWO //ENUM_TO_INT(DART_PLAYER_AWAY_SERVER)
				ENDIF
				
				serverData.iLegs[serverData.iWinner]++
				
				CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server [", serverData.iWinner, "] has won this many legs: ", serverData.iLegs[serverData.iWinner])
				
				IF EVALUATE_DART_GAME_PROGRESS(serverData)
//					serverData.iLegs[DART_PLAYER_HOME_SERVER] = 0
//					serverData.iLegs[DART_PLAYER_AWAY_SERVER] = 0
//					serverData.iSets[DART_PLAYER_HOME_SERVER] = 0
//					serverData.iSets[DART_PLAYER_AWAY_SERVER] = 0
					
					DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
				ELSE
					DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
				ENDIF
				
				serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_WINNING
				serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_WINNING
				
				// this might have to be moved to the game start stage
				serverData.bFinishedScoreTracking = FALSE
				serverData.mpState = DARTS_MPSTATE_SETUP
			ENDIF
			
			// handle turn continuing
			IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE) //serverData.bTurnContinue
				DEBUG_MESSAGE_PERIODIC("[MP DARTS] Server is continuing turn", iDartsServerThrottle)
				//IF playerData[iDartPlayerID[DARTS_PLAYER_ONE]].bResetDone 
				//AND playerData[iDartPlayerID[DARTS_PLAYER_TWO]].bResetDone
				IF 	GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
				AND GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
					serverData.mpTurnStatus[serverData.iThrower] = DARTS_MPTURN_THROWING
					serverData.mpTurnStatus[serverData.iWaiter] =  DARTS_MPTURN_WAITING
					serverData.bFinishedScoreTracking = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE, FALSE) //serverData.bTurnContinue = FALSE
					serverData.DartsTimer.bNewShot = TRUE
				ENDIF
			ENDIF
			
			// handle switching turns
			IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER) //serverData.bDoSwitchPlayer
				// set this client flag so that both clients know to clear the board
				// maybe do some printing here
				
				//IF playerData[iDartPlayerID[DARTS_PLAYER_ONE]].bResetDone 
				//AND playerData[iDartPlayerID[DARTS_PLAYER_TWO]].bResetDone
				IF 	GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
				AND GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
					CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is switching turns")
					// keep track of the last scores in case the current score has to be reset from a bust
					serverData.iLastScore[DARTS_PLAYER_ONE] = serverData.iScores[DARTS_PLAYER_ONE]
					serverData.iLastScore[DARTS_PLAYER_TWO] = serverData.iScores[DARTS_PLAYER_TWO]
					serverData.iTurnscore = 0
					
					// switch the player throwing and waiting
					IF serverData.iThrower = DARTS_PLAYER_ONE //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
						serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_WAITING
						serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_THROWING
					ELSE
						serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
						serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_WAITING
					ENDIF
					
					serverData.iServerTurn = 0
					serverData.bFinishedScoreTracking = FALSE
					//DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, FALSE) //serverData.bDoSwitchPlayer = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, FALSE) //serverData.bTurnBust = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, FALSE)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, FALSE)
					serverData.DartsTimer.bNewShot = TRUE
				ENDIF
			ENDIF
			
		ELIF serverData.mpTurnStatus[DARTS_PLAYER_TWO] = DARTS_MPTURN_WINNING
		OR serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_WINNING
			//IF PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]].bDoRematch AND PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].bDoRematch
			IF 	GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH)
			AND	GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH)
				serverData.mpState = DARTS_MPSTATE_SETUP
			ENDIF
		ENDIF
		
	ENDIF
	
	// If both clients are in the wait for server state, kick them into the game.
	IF playerData[iDartPlayerID[DARTS_PLAYER_TWO]].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
	AND playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
		DEBUG_MESSAGE("Server resetting because both clients are in DARTS_MPSTATE_WAIT_POST_GAME")
		
		// when both clients are waiting after the game, server tells them to go to menu, set's itself on the menu.
		DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_MOVETOMENU)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_MOVETOGAME)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_AWAYWIN)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_HOMEWIN)
		
		DARTS_SERVER_RESET_SCORES(serverData)
		
		// TODO: resolve this, it's out of date
		serverData.mpState = DARTS_MPSTATE_SETUP
	ENDIF
	
ENDPROC

PROC DARTS_SERVER_UPDATE_SINGLE_PLAYER_GAME(DARTS_SERVER_BROADCAST_DATA & serverData, DARTS_AP_PLAYER_BROADCAST_DATA & playerData[], DART & Darts, INT & iDartPlayerID[2]) //fill out the parameters later
	//CDEBUG1LN(DEBUG_DARTS, "[AM_DARTS PLAYER ID] - CUSTOM SINGLEPLAYER APARTMENT UPDATE ONE:", iDartPlayerID[DARTS_PLAYER_ONE], " TWO:", iDartPlayerID[DARTS_PLAYER_TWO])
	// dart game is in progress
	
	IF playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState = DARTS_MPSTATE_IN_PROGRESS
		//CDEBUG1LN(DEBUG_DARTS, "S1:", playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState)
		IF playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage = DARTS_MPTHROW_AIM
			DARTS_UPDATE_SHOT_CLOCK(serverData.DartsTimer)
		ENDIF
		
		IF serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
		AND NOT serverData.bFinishedScoreTracking
			//CDEBUG1LN(DEBUG_DARTS, "S2:", playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState)
			IF (DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_DANGER_DART))
			AND (playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPThrowStage = DARTS_MPTHROW_THROW)
				ServerData.DartsTimer.bNewShot = TRUE
			ENDIF
			//CDEBUG1LN(DEBUG_DARTS, "S2.5: scoringDone?", DARTS_SERVER_UPDATE_SCORING(Darts), " SRV TURN:", serverData.iServerTurn)
			
//				PLAYER_INDEX piSelf, piOther
//				piSelf = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_ONE]))
//				piOther	 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDartPlayerID[DARTS_PLAYER_TWO]))
//	
	
	
//			CDEBUG1LN(DEBUG_DARTS, 
//			"DSC:", DARTS_SERVER_UPDATE_SCORING(Darts),
//			" P1", GET_PLAYER_NAME(piSelf),
//			"FLAG ONE:", DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SCORING_DONE),
//			" P2", GET_PLAYER_NAME(piOther),
//			"FLAG TWO:", DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_TWO]], DARTS_CLIENTFLAG_SCORING_DONE)
//			)	
			
			
			// handing the score recording after throws
			IF DARTS_SERVER_UPDATE_SCORING(Darts)
			AND DARTS_CHECK_CLIENT_FLAG(PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_SCORING_DONE)
				//CDEBUG1LN(DEBUG_DARTS, "S3:", playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState)
				DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, FALSE)
				
				INT iCurrentScore
				serverData.iThrower = DARTS_PLAYER_ONE //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
				CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Server has found that the thrower is DART_PLAYER_HOME_SERVER")
				
				CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Server sees that playerData[DART_PLAYER_HOME_SERVER].ClientDart.iHitValue = ", playerData[iDartPlayerID[DARTS_PLAYER_ONE]].ClientDart.iHitValue)
				CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Server sees that Darts.iHitValue = ", Darts.iHitValue)
				
				serverData.iTurnscore += Darts.iHitValue//playerData[serverData.iThrower].ClientDart.iHitValue
				iCurrentScore = serverData.iScores[serverData.iThrower] - Darts.iHitValue//playerData[serverData.iThrower].ClientDart.iHitValue//serverData.iTurnscore
				CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] ServerData.iTurnscore = ", serverData.iTurnscore)
				CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Server iCurrentScore = ", iCurrentScore)
				
				//IF playerData[iDartPlayerID[DARTS_PLAYER_ONE]].bDoubleThrow
				IF 	GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW)
					CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_HOME_SERVER].bDoubleThrow = TRUE")
				ELSE
					CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] playerData[DART_PLAYER_HOME_SERVER].bDoubleThrow = FALSE")
				ENDIF
				
				CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Darts.iHitMultiplier = ", Darts.iHitMultiplier) 
				
				// if game has been won
				IF iCurrentScore = 0 AND ((Darts.iHitMultiplier = 2) OR (Darts.iHitValue = 50)) //playerData[serverData.iThrower].bDoubleThrow
					CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Server has found a winner")
					serverData.iScores[serverData.iThrower] = iCurrentScore
					serverData.bDoWinning = TRUE
				
				// if player has bust
				ELIF iCurrentScore = 1 OR iCurrentScore < 0 
				OR (iCurrentScore = 0 AND GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW) = FALSE)
					CDEBUG1LN(DEBUG_DARTS, "Current throwing player has bust, changing turns")
					serverData.iScores[serverData.iThrower] = serverData.iLastScore[serverData.iThrower]
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, TRUE) //serverData.bDoSwitchPlayer = TRUE
					IF iCurrentScore < 0
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, TRUE) //serverData.bTurnBust = TRUE
					ELIF (iCurrentScore = 1)
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, TRUE)
					ELIF ((iCurrentScore = 0) AND (GET_PLAYER_STATE(playerData[iDartPlayerID[serverData.iThrower]], DARTS_CLIENTFLAG_STATE_DART_DOUBLE_THROW) = FALSE))
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, TRUE)
					ENDIF
					
				// normal scoring throw
				ELSE
					CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Server is processing the score")
					//serverData.iScores[serverData.iThrower] = iCurrentScore
					serverData.iServerTurn++
					serverData.iScores[serverData.iThrower] = iCurrentScore
					
					IF serverData.iServerTurn >= 3
						CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Server has found that the turn shall end")
						serverData.iServerTurn--
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, TRUE) //serverData.bDoSwitchPlayer = TRUE
					ELSE
						CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Server has found that the turn shall continue")
						DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE, TRUE) //serverData.bTurnContinue = TRUE
					ENDIF				
				ENDIF
			
				serverData.bFinishedScoreTracking = TRUE
				
				serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_SCORE_CHECK
			ENDIF
		
		ELIF serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_SCORE_CHECK
			
			serverData.DartsTimer.bTimeRunOut = FALSE
			
			//CPRINTLN(DEBUG_DARTS, "[MP DARTS] Server is in SCORE CHECK")
			// handle setting the winner
			IF serverData.bDoWinning
				CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Server is processing the winner")
				serverData.iWinner = DARTS_PLAYER_ONE //ENUM_TO_INT(DART_PLAYER_HOME_SERVER)
				
				serverData.iLegs[serverData.iWinner]++
				
				CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Server [", serverData.iWinner, "] has won this many legs: ", serverData.iLegs[serverData.iWinner])
				
				IF EVALUATE_DART_GAME_PROGRESS(serverData)
//					serverData.iLegs[DART_PLAYER_HOME_SERVER] = 0
//					serverData.iLegs[DART_PLAYER_AWAY_SERVER] = 0
//					serverData.iSets[DART_PLAYER_HOME_SERVER] = 0
//					serverData.iSets[DART_PLAYER_AWAY_SERVER] = 0
					
					DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
				ELSE
					DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_DO_TRANSITION)
				ENDIF
				
				serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_WINNING
				
				// this might have to be moved to the game start stage
				serverData.bFinishedScoreTracking = FALSE
				serverData.mpState = DARTS_MPSTATE_SETUP
			ENDIF
			
			// handle turn continuing
			IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE) //serverData.bTurnContinue
				DEBUG_MESSAGE_PERIODIC("[MP DARTS] Server is continuing turn", iDartsServerThrottle)
				//IF playerData[iDartPlayerID[DARTS_PLAYER_ONE]].bResetDone
				IF GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
					serverData.mpTurnStatus[serverData.iThrower] = DARTS_MPTURN_THROWING
					serverData.bFinishedScoreTracking = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_TURN_CONTINUE, FALSE) //serverData.bTurnContinue = FALSE
					serverData.DartsTimer.bNewShot = TRUE
				ENDIF
			ENDIF
			
			// handle switching turns
			IF DARTS_GET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER) //serverData.bDoSwitchPlayer
				// set this client flag so that both clients know to clear the board
				// maybe do some printing here
				
				//IF playerData[iDartPlayerID[DARTS_PLAYER_ONE]].bResetDone
				IF GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_RESET_DONE)
					CDEBUG1LN(DEBUG_DARTS, "[MP DARTS] Server is switching turns")
					// keep track of the last scores in case the current score has to be reset from a bust
					serverData.iLastScore[DARTS_PLAYER_ONE] = serverData.iScores[DARTS_PLAYER_ONE]
					serverData.iTurnscore = 0
					
					// single player never waits, so just re-inforce throwing
					serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_THROWING
					
					serverData.iServerTurn = 0
					serverData.bFinishedScoreTracking = FALSE
					//DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_SWITCH_PLAYER, FALSE) //serverData.bDoSwitchPlayer = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_NOT_DOUBLE, FALSE) //serverData.bTurnBust = FALSE
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_ONE_POINT_LEFT, FALSE)
					DARTS_SET_UI_FLAG(serverData.iUIFlags, DARTS_UIFLAGS_BUST_UNDER_ZERO, FALSE)
					serverData.DartsTimer.bNewShot = TRUE
				ENDIF
			ENDIF
			
		ELIF serverData.mpTurnStatus[DARTS_PLAYER_ONE] = DARTS_MPTURN_WINNING
			//IF PlayerData[iDartPlayerID[DARTS_PLAYER_ONE]].bDoRematch
			IF GET_PLAYER_STATE(playerData[iDartPlayerID[DARTS_PLAYER_ONE]], DARTS_CLIENTFLAG_STATE_DART_DO_REMATCH)
				serverData.mpState = DARTS_MPSTATE_SETUP
			ENDIF
		ENDIF
		
	ENDIF
	
	// If both clients are in the wait for server state, kick them into the game.
	IF playerData[iDartPlayerID[DARTS_PLAYER_ONE]].eMPState = DARTS_MPSTATE_WAIT_POST_GAME
		CDEBUG1LN(DEBUG_DARTS, "Server resetting because both clients are in DARTS_MPSTATE_WAIT_POST_GAME")
		
		// when both clients are waiting after the game, server tells them to go to menu, set's itself on the menu.
		DARTS_SERVER_SET_FLAG(serverData, DARTSERVERFLAG_MOVETOMENU)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_MOVETOGAME)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_AWAYWIN)
		DARTS_SERVER_CLEAR_FLAG(serverData, DARTSERVERFLAG_HOMEWIN)
		
		DARTS_SERVER_RESET_SCORES(serverData)
		
		// TODO: resolve this, it's out of date
		serverData.mpState = DARTS_MPSTATE_SETUP
	ENDIF
	
ENDPROC

