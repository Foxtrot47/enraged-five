

USING "drug_trafficking_data.sch"

USING "minigames_helpers.sch"

CONST_INT MAX_REPEAT_LOCATIONS 4

EDrugLocation repeatLocations[18]
INT chosenIndex[MAX_REPEAT_LOCATIONS]

PROC CONFIGURE_TRAIN_SPAWNS(ARGS& myArgs, BOOL bRandom = FALSE)
	INT iRand

	IF NOT bRandom
		myArgs.genVectors[0] = <<1230.9960, 3262.4490, 38.5671>>
		myArgs.bGenBools[0] = FALSE 
		
		myArgs.genVectors[1] = << -429.9039, 5771.2168, 57.5407 >>
		myArgs.bGenBools[1] =  TRUE
	ELSE
		iRand = GET_RANDOM_INT_IN_RANGE(0,3999)/1000 //0,1,2,3
		IF iRand = 0
			//TrainWindMill
			myArgs.genVectors[0] = << 2443.9504, 2530.4590, 41.6537 >>
			myArgs.bGenBools[0] = FALSE 
			//TrainUnderPass
			myArgs.genVectors[1] = << 2863.8638, 3783.8127, 43.8137 >>
			myArgs.bGenBools[1] = TRUE
		ELIF iRand = 1
			//TrainFarRight
			myArgs.genVectors[0] = << 696.5959, 3176.0833, 41.7577 >>
			//TrainUnderPass
			myArgs.genVectors[1] = << 2863.8638, 3783.8127, 43.8137 >>
		ELIF iRand = 2
			//airfield
			myArgs.genVectors[0] = << 1918.4714, 3571.9446, 38.2471 >>
			//TrainFarRight
			myArgs.genVectors[1] = << 696.5959, 3176.0833, 41.7577 >>
			myArgs.bGenBools[1] =  TRUE
		ELIF iRand = 3
			//TrainUnderPass
			myArgs.genVectors[0] = << 2863.8638, 3783.8127, 43.8137 >>
			//ocean
			myArgs.genVectors[1] = << -429.9039, 5771.2168, 57.5407 >>
		ENDIF
	ENDIF

//	EDrugLocation_TrainUnderPass
//	<< 2863.8638, 3783.8127, 43.8137 >>
//	EDrugLocation_TrainWindMill
//	<< 2443.9504, 2530.4590, 41.6537 >>
//	EDrugLocation_TrainFarRight
//	<< 696.5959, 3176.0833, 41.7577 >>

ENDPROC

FUNC BOOL POPULATE_RANDOM_AIR_LOCATIONS(ARGS& myArgs, BOOL bIsReplay)
	repeatLocations[0] = EDrugLocation_SmugglersDropoff_01 // << 2463.5637, 3422.8289, 49.0819 >>
	repeatLocations[1] = EDrugLocation_WindField // << 1559.3218, 2204.1628, 77.9422 >>
	repeatLocations[2] = EDrugLocation_AutoService // << 274.1707, 2608.0642, 43.6995 >>
	repeatLocations[3] = EDrugLocation_MegaMall // << 2669.8481, 3546.6760, 50.5799 >>
	repeatLocations[4] = EDrugLocation_TrailerPark // << 53.8249, 3733.3455, 38.6775 >>
	repeatLocations[5] = EDrugLocation_CountryHouse // << 817.3708, 2197.8608, 51.0174 >>
	repeatLocations[6] = EDrugLocation_OldLiquorStore // << 944.6802, 3614.7341, 31.6225 >>
	repeatLocations[7] = EDrugLocation_DeadZone_01 // << 1178.1967, 3271.2627, 38.2326 >>
	repeatLocations[8] = EDrugLocation_TempDiner // << 2683.9819, 4335.4253, 44.8820 >>
	repeatLocations[9] = EDrugLocation_YellowJackInn // << 2006.8500, 3069.6411, 46.0500 >>
	repeatLocations[10] = EDrugLocation_WreckedAirfield // << 2346.9280, 3095.9612, 47.0212 >>
	repeatLocations[11] = EDrugLocation_FarEastDock01 // << 3811.8823, 4462.6157, 3.1544 >>
	repeatLocations[12] = EDrugLocation_DirtPit // << 2953.7483, 2789.8262, 41.0514 >>
	repeatLocations[13] = EDrugLocation_LightHouse // << 3284.1011, 5153.7642, 17.5866 >>
	repeatLocations[14] = EDrugLocation_Clearing // <<-312.8494, 3796.1387, 66.9603>>
	repeatLocations[15] = EDrugLocation_NorthBeach // << 11.4536, 6856.3940, 11.9117 >>
	repeatLocations[16] = EDrugLocation_MountainCuldesac // << -1632.0560, 4738.6890, 52.1812 >>
	repeatLocations[17] = EDrugLocation_WaterTreatmentPlant // << 1885.5386, 432.2420, 163.2558 >>
	
	INT count = 0
	INT index = 0
	
	IF bIsReplay
		REPEAT myArgs.numDropLocations index
			myArgs.dropLocations[index] = repeatLocations[g_savedGlobals.sTraffickingData.iDropLocations[index]]
			PRINTLN("myArgs.dropLocations[ ", index, " ] = ", myArgs.dropLocations[index])
		ENDREPEAT
	ELSE
		WHILE count < myArgs.numDropLocations
			index = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(chosenIndex, myArgs.numDropLocations, 0, 13)
			
			IF index <> -1
				chosenIndex[count] = index
				myArgs.dropLocations[count] = repeatLocations[index]
				PRINTLN("myArgs.dropLocations[ ", count, " ] = ", myArgs.dropLocations[count])
				count++
			ELSE
				SCRIPT_ASSERT("GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY returned -1.  Can't continue")
				RETURN FALSE
			ENDIF
		ENDWHILE
	ENDIF
	RETURN TRUE

ENDFUNC

PROC POPULATE_UBER_REPEATABLE(ARGS& myArgs)
	myArgs.mnFinalTargets[0] = BARRACKS
	myArgs.mnFinalTargets[1] = BARRACKS
	myArgs.mnFinalTargets[2] = BARRACKS
	myArgs.mnFinalTargets[3] = BARRACKS
	myArgs.mnFinalTargets[4] = BARRACKS
	myArgs.mnFinalTargets[5] = BARRACKS
	myArgs.mnFinalTargets[6] = BARRACKS
	myArgs.mnFinalTargets[7] = BARRACKS
	myArgs.mnFinalTargets[8] = BARRACKS
//	myArgs.mnFinalTargets[9] = BARRACKS
//	myArgs.mnFinalTargets[10] = BARRACKS
//	myArgs.mnFinalTargets[11] = BARRACKS
ENDPROC

PROC SETUP_REPEATABLE_CLUSTER_BOMB_MODE(ARGS& myArgs)
	myArgs.bDoStationary = TRUE
	myArgs.bDoLowAlt = FALSE
	myArgs.bDoBomb = TRUE
	myArgs.bDoUberBombs = TRUE
	myArgs.bDoTrain = FALSE
	myArgs.bDoTimed = FALSE
	POPULATE_UBER_REPEATABLE(myArgs)
	myArgs.numDropLocations = 3
	myArgs.numCargoBombs = 4
	myArgs.sConversation = "ARMS_CB_REP"
	PRINTLN("myArgs.bDoUberBombs = TRUE")
ENDPROC

PROC PRINT_ARGS(ARGS& myArgs)
	
	PRINTLN("********************************************************************************************************")
	
	PRINTLN("myArgs.bDoStationary: ", myArgs.bDoStationary)
	PRINTLN("myArgs.bDoLowAlt: ", myArgs.bDoLowAlt)
	PRINTLN("myArgs.bDoBomb: ", myArgs.bDoBomb)
	PRINTLN("myArgs.bDoUberBombs: ", myArgs.bDoUberBombs)
	PRINTLN("myArgs.bDoTrain: ", myArgs.bDoTrain)
	PRINTLN("myArgs.bDoTimed: ", myArgs.bDoTimed)
	
	PRINTLN("myArgs.numDropLocations = ", myArgs.numDropLocations)
	PRINTLN("myArgs.numCargoBombs = ", myArgs.numCargoBombs)
	
	PRINTLN("********************************************************************************************************")
	
ENDPROC

PROC REPLAY_SAVE_DATA(ARGS& myArgs)
	g_savedGlobals.sTraffickingData.bBomb = myArgs.bDoBomb 
	g_savedGlobals.sTraffickingData.bTrain = myArgs.bDoTrain
	//g_savedGlobals.sTraffickingData.bConvoy
	g_savedGlobals.sTraffickingData.bTimed = myArgs.bDoTimed
	g_savedGlobals.sTraffickingData.bLowAlt = myArgs.bDoLowAlt
	g_savedGlobals.sTraffickingData.iNumDropLocations = myArgs.numDropLocations
	g_savedGlobals.sTraffickingData.iDropLocations[0] = chosenIndex[0]
	g_savedGlobals.sTraffickingData.iDropLocations[1] = chosenIndex[1]
	g_savedGlobals.sTraffickingData.iDropLocations[2] = chosenIndex[2]
	g_savedGlobals.sTraffickingData.iDropLocations[3] = chosenIndex[3]
	PRINTLN("REPLAY_SAVE_DATA - LOCATION 1: ", g_savedGlobals.sTraffickingData.iDropLocations[0])
	PRINTLN("REPLAY_SAVE_DATA - LOCATION 2: ", g_savedGlobals.sTraffickingData.iDropLocations[1])
	PRINTLN("REPLAY_SAVE_DATA - LOCATION 3: ", g_savedGlobals.sTraffickingData.iDropLocations[2])
	PRINTLN("REPLAY_SAVE_DATA - LOCATION 4: ", g_savedGlobals.sTraffickingData.iDropLocations[3])
	
	g_savedGlobals.sTraffickingData.gangTypes = myArgs.gangType
	PRINTLN("SAVE DATA: GANG TYPE = ", ENUM_TO_INT(myArgs.gangType))
ENDPROC

PROC REPLAY_LOAD_DATA(ARGS& myArgs)
	myArgs.gangType = g_savedGlobals.sTraffickingData.gangTypes
	PRINTLN("REPLAY LOAD DATA: GANG TYPE = ",ENUM_TO_INT(myArgs.gangType))
	
	myArgs.bDoBomb = g_savedGlobals.sTraffickingData.bBomb
	myArgs.bDoTrain = g_savedGlobals.sTraffickingData.bTrain
	//g_savedGlobals.sTraffickingData.bConvoy
	myArgs.bDoTimed = g_savedGlobals.sTraffickingData.bTimed
	myArgs.bDoLowAlt = g_savedGlobals.sTraffickingData.bLowAlt
	myArgs.numDropLocations = g_savedGlobals.sTraffickingData.iNumDropLocations
	
	INT count = 0
	WHILE count < myArgs.numDropLocations
		PRINTLN("REPLAY_LOAD_DATA IDX: ", count, " :: repeatLocation IDX: ", g_savedGlobals.sTraffickingData.iDropLocations[count])
		myArgs.dropLocations[count] = repeatLocations[g_savedGlobals.sTraffickingData.iDropLocations[count]]
		chosenIndex[count] = g_savedGlobals.sTraffickingData.iDropLocations[count]
		count++
	ENDWHILE
ENDPROC

PROC POPULATE_RANDOM_AIR_VARIATION(ARGS& myArgs, BOOL bIsReplay)
	INT iRandGang

	//default params for air
	myArgs.bDoCutscene = FALSE
	myArgs.myLocation = ELocation_AirLocation
	myArgs.difficultyLevel = 0		
	myArgs.bDoPlane = TRUE
	myArgs.numDropLocations = GET_RANDOM_INT_IN_RANGE(2,MAX_REPEAT_LOCATIONS+1)

	PRINTLN("Air rank: ", g_savedGlobals.sTraffickingData.iAirRank)
	
	IF bIsReplay
		PRINTLN("IS REPLAY")
		REPLAY_LOAD_DATA(myArgs)
	ENDIF
	PRINT_ARGS(myArgs)
	//Drop or Bomb?
	IF g_savedGlobals.sTraffickingData.iAirRank % 2 = 0
		myArgs.bDoBomb = TRUE
		PRINTLN("myArgs.bDoBomb = TRUE")
	ELSE
		myArgs.bDoBomb = FALSE
		PRINTLN("myArgs.bDoBomb = FALSE")
	ENDIF
	
	IF myArgs.bDoBomb
		IF myArgs.bDoTrain 
		OR (NOT bIsReplay AND GET_RANDOM_BOOL())
			// Commenting out for play through.
//			IF GET_RANDOM_BOOL()
				//todo add to save data
//				SETUP_REPEATABLE_CLUSTER_BOMB_MODE(myArgs)
//			ELSE
				myArgs.bDoTrain = TRUE
				myArgs.sConversation = "ARMS_AIRT"
				CONFIGURE_TRAIN_SPAWNS(myArgs, TRUE)
				myArgs.bDoStationary = TRUE
				PRINTLN("myArgs.bDoStationary = TRUE VIA POPULATE_RANDOM_AIR_VARIATION - 01")
				myArgs.numDropLocations = 2
//			ENDIF
		ELSE
			IF GET_RANDOM_BOOL()
				myArgs.sConversation = "ARMS_AIRB"
			ELSE
				myArgs.sConversation = "ARMS_AIRB2"
			ENDIF
			myArgs.bDoStationary = TRUE
			PRINTLN("myArgs.bDoStationary = TRUE VIA POPULATE_RANDOM_AIR_VARIATION - 02")
		ENDIF
	ELSE
		IF GET_RANDOM_BOOL()
			myArgs.sConversation = "ARMS_AIRP"
		ELSE
			IF GET_RANDOM_BOOL()
				myArgs.sConversation = "ARMS_AIRP2"
			ELSE
				myArgs.sConversation = "ARMS_AIRP3"
			ENDIF
		ENDIF
		IF GET_RANDOM_BOOL()
			myArgs.bDoLowAlt = TRUE
		ENDIF
	ENDIF
	
	//
	//Optional stuff to flip on
	//
	//Timed
	IF GET_RANDOM_BOOL() OR myArgs.bDoTimed
		//TODO base this on number of targets.
		myArgs.fTimeLimit = TO_FLOAT(myArgs.numDropLocations * 85000)
		myArgs.bDoTimed = TRUE
	ENDIF
	
	myArgs.fBonusTime = TO_FLOAT(myArgs.numDropLocations * 80000) 
	myArgs.numCargoBombs = myArgs.numDropLocations + 1
	myArgs.iMoneyToPass = REWARD_MEDIUM * myArgs.numDropLocations
	
	IF NOT myArgs.bDoTrain
		PRINTLN("NOT TRAIN = POPULATE_RANDOM_AIR_LOCATIONS")
		POPULATE_RANDOM_AIR_LOCATIONS(myArgs, bIsReplay)
	ENDIF
	
	iRandGang = GET_RANDOM_INT_IN_RANGE() % 3
	IF iRandGang = 0
		myArgs.gangType = GANG_HILLBILLIES
		PRINTLN("ARGS: GANG TYPE = GANG_HILLBILLIES")
	ELIF iRandGang = 1
		myArgs.gangType = GANG_MEXICANS
		PRINTLN("ARGS: GANG TYPE = GANG_MEXICANS")
	ELIF iRandGang = 2
		myArgs.gangType = GANG_MARABUNTA
		PRINTLN("ARGS: GANG TYPE = GANG_MARABUNTA")
	ENDIF
	
	IF bIsReplay
		//have to run this again to ensure nothing was stomped
		REPLAY_LOAD_DATA(myArgs)
	ENDIF
	REPLAY_SAVE_DATA(myArgs)
	PRINT_ARGS(myArgs)
ENDPROC


ENUM AIR_PROGRESSION
	AIR_PROGRESSION_INTRO = 0,				//1
//	AIR_PROGRESSION_THREE_DROP,				//2
	AIR_PROGRESSION_ALT,					//3
//	AIR_PROGRESSION_ALT_TIMED,				//4
//	AIR_PROGRESSION_MOBILE_DROP_01,			//5
//	AIR_PROGRESSION_MOBILE_DROP_02,			//6
//	AIR_PROGRESSION_BOMB_STATIONARY,		//7
	AIR_PROGRESSION_BOMB_STATIONARY_A,		//8
	AIR_PROGRESSION_BOMB_TRAIN,				//9
//	AIR_PROGRESSION_BOMB_MOBILE,	        //10
//	AIR_PROGRESSION_BOMB_TIMED,				//11
	AIR_PROGRESSION_BOMB_MULTI			    //12
ENDENUM

ENUM GROUND_PROGRESSION
	GROUND_PROGRESSION_TYPE1_01 = 0, 		
	GROUND_PROGRESSION_TYPE2_01,				
	GROUND_PROGRESSION_TYPE1_02,				
	GROUND_PROGRESSION_TYPE2_02,					
	GROUND_PROGRESSION_TYPE1_03,					
	GROUND_PROGRESSION_REPEAT  						// Cycle Complete
	
//	GROUND_PROGRESSION_TYPE1_04,					
//	GROUND_PROGRESSION_TYPE2_04,
//	GROUND_PROGRESSION_TYPE1_05,					
//	GROUND_PROGRESSION_TYPE2_05,
//	GROUND_PROGRESSION_TYPE1_06,					
//	GROUND_PROGRESSION_TYPE2_06,
//	GROUND_PROGRESSION_TYPE1_07,					
//	GROUND_PROGRESSION_TYPE2_07,
//	GROUND_PROGRESSION_TYPE1_08,					
//	GROUND_PROGRESSION_TYPE2_08,
//	GROUND_PROGRESSION_TYPE1_09,					
//	GROUND_PROGRESSION_TYPE2_09,
//	GROUND_PROGRESSION_TYPE1_10,
//	GROUND_PROGRESSION_TYPE2_10,
//	GROUND_PROGRESSION_REPEAT
ENDENUM

INT iNumPairingToUse

FUNC INT TYPE1_PICK_RANDOM_PAIRINGS(ARGS& myArgs)

	myArgs.endLocation[0] =  EDrugLocation_MainAirport
	
	iNumPairingToUse = GET_RANDOM_INT_IN_RANGE(0, 65000) % MAX_NUMBER_RANDOM_TYPE_1_PAIRINGS
	PRINTLN("TYPE1: iLastPairing = ", g_savedGlobals.sTraffickingData.iLastPairing)
	PRINTLN("TYPE1: iNumPairingToUse = ", iNumPairingToUse)
	
	IF IS_REPLAY_IN_PROGRESS()
		iNumPairingToUse = g_savedGlobals.sTraffickingData.iLastPairing
		PRINTLN("TYPE 1 - REPLAY IN PROGRESS: PAIRING TO USE = ", iNumPairingToUse)
	ELSE
		WHILE iNumPairingToUse = g_savedGlobals.sTraffickingData.iLastPairing
			iNumPairingToUse = GET_RANDOM_INT_IN_RANGE(0, 65000) % MAX_NUMBER_RANDOM_TYPE_1_PAIRINGS
		ENDWHILE
	ENDIF
	
	SWITCH iNumPairingToUse
//		CASE 0
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_09
//			myArgs.fTimeLimit = 90000
//			myArgs.fSTime = 35000
//			myArgs.fTNowTime = 22000
//			PRINTLN("USING TYPE 1: REPEAT - 0")
//			RETURN 0
//		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
//		CASE 1
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_01
//			myArgs.fTimeLimit = 120000
//			myArgs.fSTime = 35000
//			myArgs.fTNowTime = 20000
//			PRINTLN("USING TYPE 1: REPEAT - 1")
//			RETURN 1
//		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
//		CASE 2
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_05
//			myArgs.fTimeLimit = 100000
//			myArgs.fSTime = 30000
//			myArgs.fTNowTime = 21000
//			PRINTLN("USING TYPE 1: REPEAT - 2")
//			RETURN 2
//		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 0
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_07 // Using MEXICANS
			myArgs.fTimeLimit = 100000
			myArgs.fSTime = 35000
			myArgs.fTNowTime = 30000
			PRINTLN("USING TYPE 1: REPEAT - 0")
			RETURN 0
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 1
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_04 // Using MARABUNTA
			myArgs.fTimeLimit = 160000
			myArgs.fSTime = 35000
			myArgs.fTNowTime = 250000
			PRINTLN("USING TYPE 1: REPEAT - 1")
			RETURN 1
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 2
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_03 // Using HILLBILLIES
			myArgs.fTimeLimit = 120000
			myArgs.fSTime = 32000
			myArgs.fTNowTime = 20000
			PRINTLN("USING TYPE 1: REPEAT - 2")
			RETURN 2
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 3
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_02 // Using MEXICANS
			myArgs.fTimeLimit = 100000
			myArgs.fSTime = 32000
			myArgs.fTNowTime = 20000
			PRINTLN("USING TYPE 1: REPEAT - 3")
			RETURN 3
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 4
			myArgs.dropLocations[0] = EDrugLocation_WreckedAirfield // Using MARABUNTA
			myArgs.fTimeLimit = 120000
			myArgs.fSTime = 30000
			myArgs.fTNowTime = 21000
			PRINTLN("USING TYPE 1: REPEAT - 4")
			RETURN 4
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 5
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_08 // Using HILLBILLIES
			myArgs.fTimeLimit = 120000
			myArgs.fSTime = 35000
			myArgs.fTNowTime = 20000
			PRINTLN("USING TYPE 1: REPEAT - 5")
			RETURN 5
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 6
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_06 // Using MEXICANS
			myArgs.fTimeLimit = 160000
			myArgs.fSTime = 45000
			myArgs.fTNowTime = 30000
			PRINTLN("USING TYPE 1: REPEAT - 6")
			RETURN 6
		BREAK
	ENDSWITCH
	
	RETURN -1
	
ENDFUNC

FUNC INT TYPE2_PICK_RANDOM_PAIRINGS(ARGS& myArgs)

	myArgs.endLocation[0] =  EDrugLocation_MainAirport
	
	iNumPairingToUse = GET_RANDOM_INT_IN_RANGE(0, 65000) % MAX_NUMBER_RANDOM_TYPE_2_PAIRINGS
	PRINTLN("iLastPairing = ", g_savedGlobals.sTraffickingData.iLastPairing)
	PRINTLN("iNumPairingToUse = ", iNumPairingToUse)
	
	IF IS_REPLAY_IN_PROGRESS()
		iNumPairingToUse = g_savedGlobals.sTraffickingData.iLastPairing
		PRINTLN("TYPE 2 - REPLAY IN PROGRESS: PAIRING TO USE = ", iNumPairingToUse)
	ELSE
		WHILE iNumPairingToUse = g_savedGlobals.sTraffickingData.iLastPairing
			iNumPairingToUse = GET_RANDOM_INT_IN_RANGE(0, 65000) % MAX_NUMBER_RANDOM_TYPE_2_PAIRINGS
		ENDWHILE
	ENDIF
	
	SWITCH iNumPairingToUse
//		CASE 0
//			myArgs.bDoSmugglers = TRUE
//			myArgs.dropLocations[0] = EDrugLocation_BoatDepot_Smugglers01
//			myArgs.endLocation[1] =  EDrugLocation_CornField
//			PRINTLN("USING RANDOM SMUGGLERS 0")
//			RETURN 0
//		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
//		CASE 1
//			myArgs.bDoSmugglers = TRUE
//			myArgs.dropLocations[0] = EDrugLocation_RailHouse
//			myArgs.endLocation[1] =  EDrugLocation_TrailersAndBoat
//			PRINTLN("USING RANDOM SMUGGLERS 1")
//			RETURN 1
//		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 0
			myArgs.bDoSmugglers = TRUE
			myArgs.dropLocations[0] = EDrugLocation_DeadZone_02 // Using MARABUNTA
			myArgs.endLocation[1] =  EDrugLocation_OldHouseSmugglers
			PRINTLN("USING RANDOM SMUGGLERS 0")
			RETURN 0
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 1
			myArgs.bDoSmugglers = TRUE
			myArgs.dropLocations[0] = EDrugLocation_BoatDepot_Smugglers // Using HILLBILLIES - No Convo setup?
			myArgs.endLocation[1] =  EDrugLocation_CornField
			PRINTLN("USING RANDOM SMUGGLERS 1")
			RETURN 1
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 2
			myArgs.bDoSmugglers = TRUE
			myArgs.dropLocations[0] = EDrugLocation_RecycleCenter // Using MEXICANS
			myArgs.endLocation[1] =  EDrugLocation_TrailerParkOnHill
			PRINTLN("USING RANDOM SMUGGLERS 2")
			RETURN 2
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 3
			myArgs.bDoSmugglers = TRUE
			myArgs.dropLocations[0] = EDrugLocation_DeadZone_01 // Using HILLBILLIES
			myArgs.endLocation[1] =  EDrugLocation_TrailersAndBoat
			PRINTLN("USING RANDOM SMUGGLERS 3")
			RETURN 3
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 4
			myArgs.bDoSmugglers = TRUE
			myArgs.dropLocations[0] = EDrugLocation_MegaMall // Using MARABUNTA
			myArgs.endLocation[1] =  EDrugLocation_CornField
			PRINTLN("USING RANDOM SMUGGLERS 4")
			RETURN 4
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 5
			myArgs.bDoSmugglers = TRUE
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE2_01 // Using MEXICANS
			myArgs.endLocation[1] =  EDrugLocation_TrailerParkOnHill
			PRINTLN("USING RANDOM SMUGGLERS 5")
			RETURN 5
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 6
			myArgs.bDoSmugglers = TRUE
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE2_02 // Using HILLBILLIES
			myArgs.endLocation[1] =  EDrugLocation_TrailerParkOnHill
			PRINTLN("USING RANDOM SMUGGLERS 6")
			RETURN 6
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE 7
			myArgs.bDoSmugglers = TRUE
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE2_03 // Using MARABUNTA
			myArgs.endLocation[1] =  EDrugLocation_OldHouseSmugglers
			PRINTLN("USING RANDOM SMUGGLERS 7")
			RETURN 7
		BREAK
	ENDSWITCH
	
	RETURN -1
	
ENDFUNC

PROC CONFIGURE_GROUND_PROGRESSION(INT progression, ARGS& myArgs)
	
	UNUSED_PARAMETER(myArgs)
	SWITCH INT_TO_ENUM(GROUND_PROGRESSION, progression)
		// ==========================================================LEVEL 1==========================================================
		CASE GROUND_PROGRESSION_TYPE1_01
			myArgs.completionEntry = CP_OJ_DTG1
			myArgs.myLocation = ELocation_GroundLocation
			myArgs.difficultyLevel = 0
			myArgs.bDoTimed = TRUE
			myArgs.bDoChase = FALSE
			myArgs.bDoAmbush = TRUE
			
			// Setting as an override
			myArgs.numDropLocations = 1
			
			// Near temp diner
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_09	// closest site, straight ahead 
			myArgs.endLocation[0] =  EDrugLocation_MainAirportGround
			myArgs.fTimeLimit = 90000
			myArgs.fSTime = 35000
			myArgs.fTNowTime = 22000
			
			SET_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
			CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type2)	
						
			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 1")
			PRINTNL()
		BREAK
		// ==========================================================LEVEL 2==========================================================
		CASE GROUND_PROGRESSION_TYPE2_01
			myArgs.completionEntry = CP_OJ_DTG2
			myArgs.myLocation = ELocation_GroundLocation
			myArgs.difficultyLevel = 0
			myArgs.bDoSmugglers = TRUE
			
			// Setting as an override
			myArgs.numDropLocations = 1
			
			myArgs.dropLocations[0] = EDrugLocation_BoatDepot_Smugglers01 	// shortest site, one of two to go right from start
			myArgs.endLocation[0] =  EDrugLocation_MainAirportGround
			myArgs.endLocation[1] =  EDrugLocation_CornField
			myArgs.fTimeLimit = 45000
			
			SET_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type2)
			CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
			
			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 2")
			PRINTNL()
		BREAK
		// ==========================================================LEVEL 3==========================================================
		CASE GROUND_PROGRESSION_TYPE1_02
			
			myArgs.completionEntry = CP_OJ_DTG3
			myArgs.myLocation = ELocation_GroundLocation
			myArgs.difficultyLevel = 0
			myArgs.bDoTimed = TRUE
			myArgs.bDoChase = FALSE
			myArgs.bDoAmbush = TRUE
			
			// Setting as an override
			myArgs.numDropLocations = 1
			
			// Directly behind, northern most site
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_01	// site furthest north
			myArgs.endLocation[0] =  EDrugLocation_MainAirportGround
			myArgs.fTimeLimit = 100000
			myArgs.fSTime = 35000
			myArgs.fTNowTime = 25000
			
			SET_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
			CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type2)
			
			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 3")
			PRINTNL()
			
		BREAK
		// ==========================================================LEVEL 4==========================================================
		CASE GROUND_PROGRESSION_TYPE2_02
			
			myArgs.completionEntry = CP_OJ_DTG4
			myArgs.myLocation = ELocation_GroundLocation
			myArgs.difficultyLevel = 0
			myArgs.bDoSmugglers = TRUE
			
			// Setting as an override
			myArgs.numDropLocations = 1
			
			myArgs.dropLocations[0] = EDrugLocation_RailHouse 		// site near the east, by train tracks
			myArgs.endLocation[0] =  EDrugLocation_MainAirportGround
			myArgs.endLocation[1] =  EDrugLocation_TrailersAndBoat
			myArgs.fTimeLimit = 45000
			
			SET_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type2)
			CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
			
			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 4")
			PRINTNL()
		BREAK
		// ==========================================================LEVEL 5==========================================================
		CASE GROUND_PROGRESSION_TYPE1_03
			
			myArgs.completionEntry = CP_OJ_DTG5
			myArgs.myLocation = ELocation_GroundLocation
			myArgs.difficultyLevel = 0
			myArgs.bDoTimed = TRUE
			myArgs.bDoChase = FALSE
			myArgs.bDoAmbush = TRUE
			
			// Setting as an override
			myArgs.numDropLocations = 1
			
			// In the middle near the rock
			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_05	// site in the middle of rock formation
			myArgs.endLocation[0] =  EDrugLocation_MainAirportGround
			myArgs.fTimeLimit = 100000
			myArgs.fSTime = 30000
			myArgs.fTNowTime = 21000
			
			SET_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
			CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type2)
			
			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 5")
			PRINTNL()
		BREAK
		// ==========================================================LEVEL 6==========================================================
		CASE GROUND_PROGRESSION_REPEAT
		
			myArgs.completionEntry = CP_OJ_DTG20
			myArgs.myLocation = ELocation_GroundLocation
			myArgs.difficultyLevel = 0
			
			IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
				IF IS_REPLAY_IN_PROGRESS()
					myArgs.bDoTimed = TRUE
					myArgs.bDoChase = FALSE
					myArgs.bDoAmbush = TRUE
					myArgs.bDoSmugglers = FALSE
					PRINTLN("REPLAY - REPEAT: TYPE 1")
				ELSE
					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
					myArgs.bDoSmugglers = TRUE
					PRINTLN("REPEAT: TYPE 2")
				ENDIF
			ELSE
				IF IS_REPLAY_IN_PROGRESS()
					myArgs.bDoSmugglers = TRUE
					PRINTLN("REPLAY - REPEAT: TYPE 2")
				ELSE
					myArgs.bDoTimed = TRUE
					myArgs.bDoChase = FALSE
					myArgs.bDoAmbush = TRUE
					myArgs.bDoSmugglers = FALSE
					PRINTLN("REPEAT: TYPE 1")
				ENDIF
			ENDIF
			
			// Setting as an override
			myArgs.numDropLocations = 1
			
			IF myArgs.bDoSmugglers
				SET_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type2)
				g_savedGlobals.sTraffickingData.iLastPairing = TYPE2_PICK_RANDOM_PAIRINGS(myArgs)
				PRINTLN("TYPE2: iLastPairing = ", g_savedGlobals.sTraffickingData.iLastPairing)
				myArgs.fTimeLimit = 45000
			ELSE
				SET_BITMASK_ENUM_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
				g_savedGlobals.sTraffickingData.iLastPairing = TYPE1_PICK_RANDOM_PAIRINGS(myArgs)
				PRINTLN("TYPE1: iLastPairing = ", g_savedGlobals.sTraffickingData.iLastPairing)
			ENDIF
			
			myArgs.endLocation[0] =  EDrugLocation_MainAirportGround
			
			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL REPEAT")
			PRINTNL()
		BREAK
//		// ==========================================================LEVEL 7==========================================================
//		CASE GROUND_PROGRESSION_TYPE1_04
//			myArgs.completionEntry = CP_OJ_DTG7
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoTimed = TRUE
//			myArgs.bDoChase = FALSE
//			myArgs.bDoAmbush = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			// Near railhouse - good early site
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_07	// site near railhouse
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.fTimeLimit = 90000
//			myArgs.fSTime = 45000
//			myArgs.fTNowTime = 30000
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 7")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 8==========================================================
//		CASE GROUND_PROGRESSION_TYPE2_04
//		
//			myArgs.completionEntry = CP_OJ_DTG8
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoSmugglers = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			myArgs.dropLocations[0] = EDrugLocation_BoatDepot_Smugglers		// old second site
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.endLocation[1] =  EDrugLocation_CornField
//			myArgs.fTimeLimit = 45000
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 8")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 9==========================================================
//		CASE GROUND_PROGRESSION_TYPE1_05
//			myArgs.completionEntry = CP_OJ_DTG9
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoTimed = TRUE
//			myArgs.bDoChase = FALSE
//			myArgs.bDoAmbush = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			// Site on the hill - good shot of airplane
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_04  	// Site on the hill, near airport - good site
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.fTimeLimit = 160000
//			myArgs.fSTime = 35000
//			myArgs.fTNowTime = 250000
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 9")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 10==========================================================
//		CASE GROUND_PROGRESSION_TYPE2_05
//		
//			myArgs.completionEntry = CP_OJ_DTG10
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoSmugglers = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			myArgs.dropLocations[0] = EDrugLocation_RecycleCenter 	// just east of the wrecked airfield
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.endLocation[1] =  EDrugLocation_TrailerParkOnHill
//			myArgs.fTimeLimit = 45000
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 10")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 11==========================================================
//		CASE GROUND_PROGRESSION_TYPE1_06
//			myArgs.completionEntry = CP_OJ_DTG11
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoTimed = TRUE
//			myArgs.bDoChase = FALSE
//			myArgs.bDoAmbush = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			// Site near past 1st site
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_03	// Near highway, between houses
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.fTimeLimit = 120000
//			myArgs.fSTime = 32000
//			myArgs.fTNowTime = 20000
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 11")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 12==========================================================
//		CASE GROUND_PROGRESSION_TYPE2_06
//		
//			myArgs.completionEntry = CP_OJ_DTG12
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoSmugglers = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			myArgs.dropLocations[0] = EDrugLocation_DeadZone_01 	// old first site
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.endLocation[1] =  EDrugLocation_TrailersAndBoat
//			myArgs.fTimeLimit = 45000
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 6")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 13==========================================================
//		CASE GROUND_PROGRESSION_TYPE1_07
//			myArgs.completionEntry = CP_OJ_DTG13
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoTimed = TRUE
//			myArgs.bDoChase = FALSE
//			myArgs.bDoAmbush = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			// Far east side, also used in air
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_02 	// Far east site, near the water
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.fTimeLimit = 100000
//			myArgs.fSTime = 32000
//			myArgs.fTNowTime = 20000
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 13")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 14==========================================================
//		CASE GROUND_PROGRESSION_TYPE2_07
//		
//			myArgs.completionEntry = CP_OJ_DTG14
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoSmugglers = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			myArgs.dropLocations[0] = EDrugLocation_MegaMall 		// just beyond mountain range
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.endLocation[1] =  EDrugLocation_CornField
//			myArgs.fTimeLimit = 45000
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 14")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 15==========================================================
//		CASE GROUND_PROGRESSION_TYPE1_08
//			myArgs.completionEntry = CP_OJ_DTG15
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoTimed = TRUE
//			myArgs.bDoChase = FALSE
//			myArgs.bDoAmbush = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			// First location in the past
//			myArgs.dropLocations[0] = EDrugLocation_WreckedAirfield		// old first site
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.fTimeLimit = 120000
//			myArgs.fSTime = 35000
//			myArgs.fTNowTime = 25000
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 15")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 16==========================================================
//		CASE GROUND_PROGRESSION_TYPE2_08
//			
//			myArgs.completionEntry = CP_OJ_DTG16
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoSmugglers = TRUE
//
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE2_01	// closes site to the main road, just pass town
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.endLocation[1] =  EDrugLocation_TrailerParkOnHill
//			
//			myArgs.fTimeLimit = 200000
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 16")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 17==========================================================
//		CASE GROUND_PROGRESSION_TYPE1_09
//			myArgs.completionEntry = CP_OJ_DTG17
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoTimed = TRUE
//			myArgs.bDoChase = FALSE
//			myArgs.bDoAmbush = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			// Along road
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_08	// along the road, near airport
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.fTimeLimit = 120000
//			myArgs.fSTime = 35000
//			myArgs.fTNowTime = 20000
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 17")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 18==========================================================
//		CASE GROUND_PROGRESSION_TYPE2_09
//		
//			myArgs.completionEntry = CP_OJ_DTG18
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoSmugglers = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE2_02	// culdesac before wrecked airfield
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.endLocation[1] =  EDrugLocation_TrailerParkOnHill
//			myArgs.fTimeLimit = 45000
//
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 18")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 19==========================================================
//		CASE GROUND_PROGRESSION_TYPE1_10
//			myArgs.completionEntry = CP_OJ_DTG19
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoTimed = TRUE
//			myArgs.bDoChase = FALSE
//			myArgs.bDoAmbush = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			// Furthest site - near mining area
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE1_06	// mining area, toughest location
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.fTimeLimit = 160000
//			myArgs.fSTime = 45000
//			myArgs.fTNowTime = 30000
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 19")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL 20==========================================================
//		CASE GROUND_PROGRESSION_TYPE2_10
//		
//			myArgs.completionEntry = CP_OJ_DTG20
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			myArgs.bDoSmugglers = TRUE
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			myArgs.dropLocations[0] = EDrugLocation_NEW_TYPE2_03 	// site on hill near airfield
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			myArgs.endLocation[1] =  EDrugLocation_OldHouseSmugglers
//			myArgs.fTimeLimit = 45000
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL 20")
//			PRINTNL()
//		BREAK
//		// ==========================================================LEVEL REPEAT==========================================================
//		CASE GROUND_PROGRESSION_REPEAT
//		
//			myArgs.completionEntry = CP_OJ_DTG20
//			myArgs.myLocation = ELocation_GroundLocation
//			myArgs.difficultyLevel = 0
//			
//			IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
//				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
//				myArgs.bDoSmugglers = TRUE
//				PRINTLN("REPEAT: TYPE 2")
//			ELSE
//				myArgs.bDoTimed = TRUE
//				myArgs.bDoChase = FALSE
//				myArgs.bDoAmbush = TRUE
//				myArgs.bDoSmugglers = FALSE
//				PRINTLN("REPEAT: TYPE 1")
//			ENDIF
//			
//			// Setting as an override
//			myArgs.numDropLocations = 1
//			
//			IF myArgs.bDoSmugglers
//				g_savedGlobals.sTraffickingData.iLastPairing = TYPE2_PICK_RANDOM_PAIRINGS(myArgs)
//				PRINTLN("TYPE2: iLastPairing = ", g_savedGlobals.sTraffickingData.iLastPairing)
//				myArgs.fTimeLimit = 45000
//			ELSE
//				SET_BITMASK_ENUM_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
//				g_savedGlobals.sTraffickingData.iLastPairing = TYPE1_PICK_RANDOM_PAIRINGS(myArgs)
//				PRINTLN("TYPE1: iLastPairing = ", g_savedGlobals.sTraffickingData.iLastPairing)
//			ENDIF
//			
//			myArgs.endLocation[0] =  EDrugLocation_MainAirport
//			
//			PRINTSTRING("SETTING GROUND PROGRESSION - LEVEL REPEAT")
//			PRINTNL()
//		BREAK
	ENDSWITCH

ENDPROC

PROC CONFIGURE_AIR_PROGRESSION(INT progression, ARGS& myArgs, BOOL bIsReplay)
	PRINTLN("CONFIGURE_AIR_PROGRESSION")
	//For now setting money required to pass to be every drop but 1 except for the last two
	
	
	SWITCH INT_TO_ENUM(AIR_PROGRESSION, progression)
		
		// ==========================================================LEVEL 1==========================================================
		CASE AIR_PROGRESSION_INTRO
			//REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_DTA1 )
			myArgs.completionEntry = CP_OJ_DTA1 
			myArgs.bDoCutscene = TRUE
			//myArgs.bDebugLaunch = TRUE
			myArgs.myLocation = ELocation_AirLocation
			//myArgs.myVariation = EVariation_Air
			myArgs.difficultyLevel = 0
			myArgs.bDoPlane = TRUE
			myArgs.bDoTutorial = TRUE
			myArgs.fBonusTime = 250
			
			myArgs.sConversation = "ARMS_AIR1"
			
			myArgs.numDropLocations = 2
			myArgs.numCargoBombs = 4
			myArgs.dropLocations[0] = EDrugLocation_LittleHouseOnTheHill //EDrugLocation_TrailerPark
			myArgs.dropLocations[1] = EDrugLocation_MountainCommune

			myArgs.iMoneyToPass = REWARD_MEDIUM * (myArgs.numDropLocations)
				
		BREAK
		// ==========================================================LEVEL 2==========================================================
//		CASE AIR_PROGRESSION_THREE_DROP
//			myArgs.bDoCutscene = FALSE
//			myArgs.bDebugLaunch = TRUE
//			myArgs.myLocation = ELocation_AirLocation
//			myArgs.myVariation = EVariation_Air
//			myArgs.difficultyLevel = 0
//			myArgs.bDoPlane = TRUE
//			myArgs.fBonusTime = 160
//			
//			myArgs.sConversation = "ARMS_AIR2"
//			myArgs.numDropLocations = 3
//			myArgs.numCargoBombs = 4
//			myArgs.dropLocations[0] = EDrugLocation_SmugglersDropoff_01 //EDrugLocation_SmallTrailer
//			myArgs.dropLocations[1] = EDrugLocation_WindField
//			myArgs.dropLocations[2] = EDrugLocation_AutoService
//			
//			myArgs.iMoneyToPass = REWARD_MEDIUM * (myArgs.numDropLocations)
//		BREAK
		// ==========================================================LEVEL 3==========================================================
		CASE AIR_PROGRESSION_ALT
			//REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_DTA2 )
			myArgs.completionEntry = CP_OJ_DTA2
			myArgs.bDoCutscene = FALSE
			//myArgs.bDebugLaunch = TRUE
			myArgs.myLocation = ELocation_AirLocation
			//myArgs.myVariation = EVariation_Air
			myArgs.difficultyLevel = 0
			myArgs.bDoPlane = TRUE
			myArgs.bDoBomb = FALSE
			myArgs.bDoLowAlt = TRUE
			myArgs.fBonusTime = 300
			
			myArgs.sConversation = "ARMS_AIR3"
			myArgs.numDropLocations = 3
			myArgs.numCargoBombs = 4
			myArgs.dropLocations[0] = EDrugLocation_Clearing //EDrugLocation_SeaSide
			myArgs.dropLocations[1] = EDrugLocation_FarmOnHill //EDrugLocation_WindField
			myArgs.dropLocations[2] = EDrugLocation_WaterTreatmentPlant //EDrugLocation_LittleHouseOnTheHill
			
			myArgs.iMoneyToPass = REWARD_MEDIUM * (myArgs.numDropLocations)
			
		BREAK
		// ==========================================================LEVEL 4==========================================================
//		CASE AIR_PROGRESSION_ALT_TIMED
//			myArgs.bDoCutscene = FALSE
//			myArgs.bDebugLaunch = TRUE
//			myArgs.myLocation = ELocation_AirLocation
//			myArgs.myVariation = EVariation_Air
//			myArgs.difficultyLevel = 0
//			myArgs.bDoTimed = TRUE
//			myArgs.bDoPlane = TRUE
//			myArgs.bDoLowAlt = TRUE
//			myArgs.fBonusTime = 170
//			
//			myArgs.fTimeLimit = 150000
//			
//			myArgs.sConversation = "ARMS_AIR4"
//			myArgs.numDropLocations = 4
//			myArgs.numCargoBombs = 5
//			
//			myArgs.dropLocations[0] = EDrugLocation_SkethcyHouse
//			myArgs.dropLocations[1] = EDrugLocation_TrailerPark
//			myArgs.dropLocations[2] = EDrugLocation_SmugglersDropoff_01 //EDrugLocation_SmallTrailer
//			myArgs.dropLocations[3] = EDrugLocation_CountryHouse
//			
//			myArgs.iMoneyToPass = REWARD_MEDIUM * (myArgs.numDropLocations+1)
//			
//		BREAK
		// ==========================================================LEVEL 5==========================================================
//		CASE AIR_PROGRESSION_MOBILE_DROP_01
//			myArgs.bDoCutscene = FALSE
//			myArgs.bDebugLaunch = TRUE
//			myArgs.myLocation = ELocation_AirLocation
//			myArgs.myVariation = EVariation_Air
//			myArgs.difficultyLevel = 0
//			myArgs.bDoLockOn = TRUE
//			myArgs.bDoPlane = TRUE
//			myArgs.bDoBomb = FALSE
//			myArgs.bDoLowAlt = TRUE
//			myArgs.bDoMobile = TRUE
//			myArgs.fBonusTime = 170
//		
//			myArgs.sConversation = "ARMS_AIR5"
//			myArgs.numDropLocations = 3
//			myArgs.numCargoBombs = 4
//			
//			//ok straight path to start
//			
//			myArgs.dropLocations[0] = EDrugLocation_TempDiner
//			myArgs.dropLocations[1] = EDrugLocation_DeadZone_01
//			myArgs.dropLocations[2] = EDrugLocation_TrailerPark
//			//myArgs.dropLocations[0] = EDrugLocation_LiquorJrMarket
//
//			myArgs.genVectors[0] = << 556.4238, 3508.5186, 33.1729 >>
//			myArgs.genVectors[1] = << 1097.2122, 4429.3359, 62.1544 >>
//			myArgs.genVectors[2] = << 2295.8408, 3855.1628, 33.6880 >>
//			
//			//myArgs.dropLocations[1] = EDrugLocation_LiquorJrMarket
//			//myArgs.dropLocations[2] = EDrugLocation_OldLiquorStore
//
//			myArgs.iMoneyToPass = REWARD_MEDIUM * 4
//		BREAK
//		// ==========================================================LEVEL 6==========================================================
//		CASE AIR_PROGRESSION_MOBILE_DROP_02
//			myArgs.bDoCutscene = FALSE
//			myArgs.bDebugLaunch = TRUE
//			myArgs.myLocation = ELocation_AirLocation
//			myArgs.myVariation = EVariation_Air
//			myArgs.difficultyLevel = 0
//			myArgs.bDoLockOn = TRUE
//			myArgs.bDoPlane = TRUE
//			myArgs.bDoBomb = FALSE
//			myArgs.bDoLowAlt = TRUE
//			myArgs.bDoMobile = TRUE
//			myArgs.fBonusTime = 180
//						
//			myArgs.mnTargetCars[0] = MARQUIS
//			myArgs.mnTargetCars[1] = MARQUIS
//			myArgs.mnTargetCars[2] = SQUALO
//			
//			//mnTargetCars
//			myArgs.sConversation = "ARMS_AIR6"
//			myArgs.numDropLocations = 3
//			myArgs.numCargoBombs = 5
//			
//			myArgs.dropLocations[0] = EDrugLocation_WaterMiddle
//			myArgs.dropLocations[1] = EDrugLocation_WaterFar
//			myArgs.dropLocations[2] = EDrugLocation_WaterRiver
//
//			myArgs.iMoneyToPass = REWARD_MEDIUM * 4
//		BREAK
		// ==========================================================LEVEL 7==========================================================
//		CASE AIR_PROGRESSION_BOMB_STATIONARY
//			myArgs.bDoStationary = TRUE
//			myArgs.bDoCutscene = FALSE
//			myArgs.bDebugLaunch = TRUE
//			myArgs.myLocation = ELocation_AirLocation
//			myArgs.myVariation = EVariation_Air
//			myArgs.difficultyLevel = 0
//			myArgs.bDoPlane = TRUE
//			myArgs.bDoBomb = TRUE
//			myArgs.fBonusTime = 160
//			
//			myArgs.sConversation = "ARMS_AIR7"
//			myArgs.numDropLocations = 3
//			myArgs.numCargoBombs = 6
//			
//			myArgs.dropLocations[0] = EDrugLocation_OldLiquorStore
//			myArgs.dropLocations[1] = EDrugLocation_SmugglersDropoff_01
//			myArgs.dropLocations[2] = EDrugLocation_DeadZone_01
//
//			myArgs.iMoneyToPass = REWARD_MEDIUM * 4
//		BREAK
		// ==========================================================LEVEL 8==========================================================
		CASE AIR_PROGRESSION_BOMB_STATIONARY_A
			//REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_DTA3 )
			myArgs.completionEntry = CP_OJ_DTA3
			myArgs.bDoStationary = TRUE
			myArgs.bDoCutscene = FALSE
			//myArgs.bDebugLaunch = TRUE
			myArgs.myLocation = ELocation_AirLocation
			//myArgs.myVariation = EVariation_Air
			myArgs.difficultyLevel = 0
			myArgs.bDoPlane = TRUE
			myArgs.bDoBomb = TRUE
			myArgs.fBonusTime = 360
			
			myArgs.sConversation = "ARMS_AIR7"
			myArgs.numDropLocations = 4
			myArgs.numCargoBombs = 6
			myArgs.gangType = GANG_MEXICANS
			
			myArgs.dropLocations[0] = EDrugLocation_NorthBeach
			myArgs.dropLocations[1] = EDrugLocation_MountainCuldesac
			myArgs.dropLocations[2] = EDrugLocation_BoatDepot_Smugglers
			myArgs.dropLocations[3] = EDrugLocation_FarEastDock01
			
			myArgs.iMoneyToPass = REWARD_MEDIUM * (myArgs.numDropLocations)
			PRINTLN("SETTING LEVEL 8 - AIR_PROGRESSION_BOMB_STATIONARY_A")
		BREAK
		// ==========================================================LEVEL 9==========================================================
		CASE AIR_PROGRESSION_BOMB_TRAIN
			//REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_DTA4 )
			myArgs.completionEntry = CP_OJ_DTA4
			myArgs.bDoStationary = TRUE
			//myArgs.bDebugLaunch = TRUE
			myArgs.myLocation = ELocation_AirLocation
			//myArgs.myVariation = EVariation_Air
			myArgs.difficultyLevel = 0
			myArgs.bDoPlane = TRUE
			myArgs.bDoBomb = TRUE
			myArgs.bDoTrain = TRUE
			myArgs.fBonusTime = 270
			
			myArgs.sConversation = "ARMS_AIR9"
			myArgs.numDropLocations = 2
			myArgs.numCargoBombs = 4
			myArgs.gangType = GANG_MARABUNTA
			
			CONFIGURE_TRAIN_SPAWNS(myArgs)
			
			//work in train location here somehow
			myArgs.dropLocations[0] = EDrugLocation_OldHouse

			myArgs.iMoneyToPass = REWARD_MEDIUM * myArgs.numDropLocations
		BREAK
		// ==========================================================LEVEL 10==========================================================
//		CASE AIR_PROGRESSION_BOMB_MOBILE
//			myArgs.bDoCutscene = FALSE
//			myArgs.bDebugLaunch = TRUE
//			myArgs.myLocation = ELocation_AirLocation
//			myArgs.myVariation = EVariation_Air
//			myArgs.difficultyLevel = 0
//			myArgs.bDoPlane = TRUE
//			myArgs.bDoBomb = TRUE
//			myArgs.fBonusTime = 180
//			
//			myArgs.sConversation = "ARMS_AIR10"
//			myArgs.numDropLocations = 3
//			myArgs.numCargoBombs = 5
//			
//			myArgs.dropLocations[0] = EDrugLocation_TempDiner
//			myArgs.dropLocations[1] = EDrugLocation_OldLiquorStore
//			myArgs.dropLocations[2] = EDrugLocation_YellowJackInn
//			
//			
//			myArgs.iMoneyToPass = REWARD_MEDIUM * 3
//		
//		BREAK
//		// ==========================================================LEVEL 11==========================================================
//		CASE AIR_PROGRESSION_BOMB_TIMED
//			myArgs.bDoCutscene = FALSE
//			myArgs.bDebugLaunch = TRUE
//			myArgs.myLocation = ELocation_AirLocation
//			myArgs.myVariation = EVariation_Air
//			myArgs.difficultyLevel = 0
//			myArgs.bDoTimed = TRUE
//			myArgs.bDoPlane = TRUE
//			myArgs.bDoBomb = TRUE
//			myArgs.fBonusTime = 150
//			
//			myArgs.sConversation = "ARMS_AIR11"
//			myArgs.fTimeLimit = 300000
//			myArgs.numDropLocations = 4
//			myArgs.numCargoBombs = 4
//			
//			myArgs.dropLocations[0] = EDrugLocation_YellowJackInn
//			myArgs.dropLocations[1] = EDrugLocation_WreckedAirfield
//			myArgs.dropLocations[2] = EDrugLocation_LiquorJrMarket
//			myArgs.dropLocations[3] = EDrugLocation_TrailerPark
//			
//			myArgs.iMoneyToPass = REWARD_MEDIUM * 4
//		BREAK
		// ==========================================================LEVEL 12==========================================================
		CASE AIR_PROGRESSION_BOMB_MULTI
			//REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_DTA5 )
			myArgs.completionEntry = CP_OJ_DTA5
			myArgs.bDoStationary = TRUE
			myArgs.bDoCutscene = FALSE
			//myArgs.bDebugLaunch = TRUE
			myArgs.myLocation = ELocation_AirLocation
			//myArgs.myVariation = EVariation_Air
			myArgs.difficultyLevel = 0
			//myArgs.bDoTimed = TRUE
			myArgs.bDoPlane = TRUE
			myArgs.bDoBomb = TRUE
			
			myArgs.bDoUberBombs = TRUE

			myArgs.fBonusTime = 200
			
			myArgs.sConversation = "ARMS_AIR12"
			//myArgs.fTimeLimit = 300000
			myArgs.numDropLocations = 3
			myArgs.numCargoBombs = 4
			
			myArgs.gangType = GANG_HILLBILLIES
			
//			myArgs.mnFinalTargets[0] = CUBAN800
//			myArgs.mnFinalTargets[1] = CUBAN800
//			myArgs.mnFinalTargets[2] = CUBAN800
//			myArgs.mnFinalTargets[3] = CUBAN800
//			
//			myArgs.mnFinalTargets[4] = BARRACKS
//			myArgs.mnFinalTargets[5] = BARRACKS
//			myArgs.mnFinalTargets[6] = BARRACKS
//			myArgs.mnFinalTargets[7] = BARRACKS
//			
//			myArgs.mnFinalTargets[8] = CUBAN800
//			myArgs.mnFinalTargets[9] = CUBAN800
//			myArgs.mnFinalTargets[10] = CUBAN800
//			myArgs.mnFinalTargets[11] = CUBAN800

			myArgs.mnFinalTargets[0] = CUBAN800
			myArgs.mnFinalTargets[1] = CUBAN800
			myArgs.mnFinalTargets[2] = CUBAN800
			
			myArgs.mnFinalTargets[3] = BARRACKS
			myArgs.mnFinalTargets[4] = BARRACKS
			myArgs.mnFinalTargets[5] = BARRACKS
			
			myArgs.mnFinalTargets[6] = CUBAN800
			myArgs.mnFinalTargets[7] = CUBAN800
			myArgs.mnFinalTargets[8] = CUBAN800
			
			myArgs.dropLocations[0] = EDrugLocation_YellowJackInn
			myArgs.dropLocations[1] = EDrugLocation_WreckedAirfield
			myArgs.dropLocations[2] = EDrugLocation_LiquorJrMarket
			//myArgs.dropLocations[3] = EDrugLocation_TrailerPark
			
			myArgs.iMoneyToPass = REWARD_MEDIUM * myArgs.numDropLocations
		BREAK
		
		DEFAULT
			POPULATE_RANDOM_AIR_VARIATION(myArgs, bIsReplay)
			
//			TEXT_LABEL DisplayText
//			DisplayText = "Air Trafficking variant " 
//			DisplayText += progression
//			DisplayText += " doesn't exist!"
//			SCRIPT_ASSERT(DisplayText)
		BREAK
	ENDSWITCH

ENDPROC
