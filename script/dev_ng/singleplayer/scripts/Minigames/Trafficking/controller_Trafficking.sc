

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// controller_Trafficking.sc
USING "controller_Trafficking.sch"
USING "controller_Trafficking_Air_lib.sch"
USING "controller_Trafficking_Ground_lib.sch"
USING "controller_Trafficking_Weapons_lib.sch"

#IF IS_DEBUG_BUILD
	USING "debug_channels_structs.sch"
#ENDIF

SCENARIO_BLOCKING_INDEX	scenarioBlock = NULL
BOOL bAddScenarioBlocking = FALSE

INT	iVehicleSnapshotLastStored = 0 // helps correctly restore the last vehicle player used before the mission

PROC CONTROLLER_TRAFFICKING_THREAD_CLEANUP()
	CPRINTLN(DEBUG_MINIGAME, "==== TRAFFICKING CONTROLLER TERMINATING ====")
	
	IF (scenarioBlock != NULL)
		REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlock)
	ENDIF
	
	TRAFFICKING_AIR_CLEANUP(TRUE)
	TRAFFICKING_GROUND_CLEANUP(TRUE)
	TRAFFICKING_WEAPONS_CLEANUP()
	
	Set_Leave_Area_Flag_For_All_Blipped_Missions()
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC CONTROLLER_TRAFFICKING_INIT()
	IF NOT bAddScenarioBlocking
		scenarioBlock = ADD_SCENARIO_BLOCKING_AREA((<<2157.1089, 4788.5586, 40.0730>> - <<25,25,25>>), (<<2157.1089, 4788.5586, 40.0730>> + <<25,25,25>>))
		PRINTLN("TRAFFICKING - ADDING SCENARIO BLOCKING AREA")
		bAddScenarioBlocking = TRUE
	ENDIF
ENDPROC

SCRIPT
	PRINTLN("==== TRAFFICKING CONTROLLER STARTING ====")
	
	// Do we need to cleanup?
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_DEBUG_MENU | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		CONTROLLER_TRAFFICKING_THREAD_CLEANUP()
	ENDIF
	
	// Set us to relaunch when a save is loaded.
	Register_Script_To_Relaunch_List(LAUNCH_BIT_MG_CTRL_TRAF)
	
	CONTROLLER_TRAFFICKING_INIT()
	
	// Local variable init.
	INT iWaitTime = 0
	BOOL bIsReplayLaunch_Air = FALSE
	BOOL bIsReplayLaunch_Ground = FALSE
	
	WHILE TRUE
	
		IF Get_Player_Timetable_Scene_In_Progress() = PR_SCENE_T6_TRAF_AIR
			bSwitchInProgress = TRUE
//			PRINTLN("bSwitchInProgress = TRUE")
		ELSE
			bSwitchInProgress = FALSE
//			PRINTLN("NO SWITCH IS IN PROGRESS - 01")
		ENDIF
	
		IF TRAFFICKING_CONTROLLER_SHOULD_UPDATE()			
			// Going to need the distance for multiple things.
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				fDist2ToLauncher = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vLaunchCoords)
			ENDIF
			
			// --------------------
			// Air trafficking -- Wait afterwards so we're not updating both each frame.
			TRAFFICKING_AIR_UPDATE(bIsReplayLaunch_Air)
			
			// -- Put a wait in here, so that we're not updating both trafficking launchers every frame.
//			WAIT(100) // Removing wait time to fix Bug # 1175414
			
			// Air trafficking -- Wait afterwards so we're not updating both each frame.
			TRAFFICKING_GROUND_UPDATE(bIsReplayLaunch_Ground)
			
//			WAIT(100) // Removing wait time to fix Bug # 1175414
						
			// check if we should update the vehicle snapshot
			IF eAirState > TKS_STREAM_WAIT 
				// wait until plane has been created so we don't get the plane from Trevor2 in our snapshot
				IF fDist2ToLauncher < 600
					IF IS_CURRENTLY_ON_MISSION_TO_TYPE() = FALSE
					AND GET_GAME_TIMER() - iVehicleSnapshotLastStored > 2000
						iVehicleSnapshotLastStored = GET_GAME_TIMER()
						STORE_START_PLAYER_VEHICLE_SNAPSHOT()
					ENDIF
				ENDIF
			ENDIF
			
			// Shorter wait time before we update again.
//			iWaitTime = 100 // Removing wait time to fix Bug # 1175414
			iWaitTime = 0
		ELSE
//			PRINTLN("TRAFFICKING Controller feels no need to update.") //removed by AndyMinghella for bug 568589

//			CONTROLLER_TRAFFICKING_THREAD_CLEANUP()
			
			IF NOT bOkayToKeepBlipsActive
				IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_TRAF_AIR)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRAF_AIR, FALSE)
					g_savedGlobals.sTraffickingData.bAirBlipActive = FALSE
//					PRINTLN("SETTING - g_savedGlobals.sTraffickingData.bAirBlipActive = FALSE")
				ENDIF
			ELSE
				g_savedGlobals.sTraffickingData.bAirBlipActive = FALSE
//				PRINTLN("SETTING - g_savedGlobals.sTraffickingData.bAirBlipActive = FALSE")
			ENDIF
			
			IF NOT bOkayToKeepBlipsActive
				IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MINIGAME_TRAF_GND)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRAF_GND, FALSE)
					g_savedGlobals.sTraffickingData.bGndBlipActive = FALSE
//					PRINTLN("SETTING - g_savedGlobals.sTraffickingData.bGndBlipActive = FALSE")
				ENDIF
			ELSE
				g_savedGlobals.sTraffickingData.bGndBlipActive = FALSE
//				PRINTLN("SETTING - g_savedGlobals.sTraffickingData.bGndBlipActive = FALSE")
			ENDIF
			
			iWaitTime = 1000
		ENDIF
		
		WAIT(iWaitTime)
	ENDWHILE
ENDSCRIPT
