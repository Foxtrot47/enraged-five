
USING "Drug_Trafficking_Data.sch"
USING "Drug_Trafficking_core.sch"
USING "commands_water.sch"
USING "oddjob_aggro.sch"

ENUM SMUGGLER_PLANE_STATE
	SMUGGLER_PLANE_STATE_INIT = 0,
	SMUGGLER_PLANE_STATE_01,
	SMUGGLER_PLANE_STATE_02,
	SMUGGLER_PLANE_STATE_03,
	SMUGGLER_PLANE_STATE_04,
	SMUGGLER_PLANE_STATE_05,
	SMUGGLER_PLANE_STATE_06,
	SMUGGLER_PLANE_STATE_07
ENDENUM


// Smugglers constants
CONST_INT DISTANCE_TO_LOSE_SMUGGLERS 300
CONST_INT CLOSE_TO_BASE_DISTANCE 200
CONST_FLOAT SMUGGLER_AFTER_PICKUP_TOO_FAR_DISTANCE 75.0

VECTOR vPlayerTooClose = <<50,50,50>>
VECTOR vPlayerTooFar = <<150,150,150>>
VECTOR vPickupDimension = <<1,1,1>>

FLOAT fDistToObjPlayer
FLOAT fDistToObjAI, fDistBetweenPlayerAndAI

BOOL bSmugglerUpsideDown = FALSE
BOOL bGrabbedHealth = FALSE
BOOL bTaskedGuyToLeave = FALSE
BOOL bOkayToBlipEnemies = FALSE
BOOL bDoneBlipingEnemies = FALSE
BOOL bCreateFlare = FALSE
BOOL bSmugglersAggroed = FALSE
BOOL bTriggeredMusicEvent01 = FALSE
BOOL bPlayedOscarLine = FALSE
BOOL bSmugglersOutVehicle = FALSE

BOOL bSmuggglersDead = FALSE
BOOL bPlayedKilledSmugglersLine = FALSE
BOOL bPrintReturnToPackageWarning = FALSE
BOOL bPlayedSightsConvo = FALSE

BOOL bSmugglersLost = FALSE
BOOL bPlayedLostSmugglersLine = FALSE

INT iSmugglerUpsideDownVehicleTime
INT iDialogueWarning
INT iStages
INT iStartingHealth

AGGRO_ARGS smugglersAggroArgs
EAggro eAggroReasons

VECTOR vRangeToUse = <<6.5,6.5,6.5>>

// Timers
structTimer updateTimer

// Objects
OBJECT_INDEX oFlareProp

BLIP_INDEX vehBlip

PED_INDEX pedTempPilot

PROC CREATE_SMUGGLER_PED_IN_VEHICLE(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, INT idx)
	INT idx2, iStartIndex
	
	IF IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[idx])
		SET_VEHICLE_ON_GROUND_PROPERLY(smugArgs.smugglersVehicles[idx])
		SET_VEHICLE_ENGINE_ON(smugArgs.smugglersVehicles[idx], TRUE, TRUE)
		SET_ENTITY_PROOFS(smugArgs.smugglersVehicles[idx], FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_VEHICLE_CAN_BE_TARGETTED(smugArgs.smugglersVehicles[idx], TRUE)
		SET_VEHICLE_CAN_LEAK_OIL(smugArgs.smugglersVehicles[idx], FALSE)
		SET_VEHICLE_CAN_LEAK_PETROL(smugArgs.smugglersVehicles[idx], FALSE)
		SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(smugArgs.smugglersVehicles[idx], TRUE)
		
		REPEAT myLocData.iNumSmugPedsPerVehicle idx2
			iStartIndex = idx*myLocData.iNumSmugPedsPerVehicle
			smugArgs.smugglersPeds[idx2 + iStartIndex] = CREATE_PED_INSIDE_VEHICLE(smugArgs.smugglersVehicles[idx], PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy, myLocData.PedSeat[idx2])
			
			// Setup ped attributes
			IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[idx2 + iStartIndex])
				SET_PED_ACCURACY(smugArgs.smugglersPeds[idx2 + iStartIndex], 15)
				SET_PED_MAX_HEALTH(smugArgs.smugglersPeds[idx2 + iStartIndex], 100)
				SET_PED_COMBAT_ABILITY(smugArgs.smugglersPeds[idx2 + iStartIndex], CAL_AVERAGE)
				SET_PED_COMBAT_RANGE(smugArgs.smugglersPeds[idx2 + iStartIndex], CR_FAR)
				SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(smugArgs.smugglersPeds[idx2 + iStartIndex], TRUE)
				SET_PED_KEEP_TASK(smugArgs.smugglersPeds[idx2 + iStartIndex], TRUE)
				SET_PED_COMBAT_ATTRIBUTES(smugArgs.smugglersPeds[idx2 + iStartIndex], CA_LEAVE_VEHICLES, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(smugArgs.smugglersPeds[idx2 + iStartIndex], TRUE)
				GIVE_WEAPON_TO_PED(smugArgs.smugglersPeds[idx2 + iStartIndex], WEAPONTYPE_MICROSMG, 1000, TRUE)
				SET_PED_DIES_IN_WATER(smugArgs.smugglersPeds[idx2 + iStartIndex], TRUE)
				SET_PED_STAY_IN_VEHICLE_WHEN_JACKED(smugArgs.smugglersPeds[idx2 + iStartIndex], TRUE)
				SET_PED_CONFIG_FLAG(smugArgs.smugglersPeds[idx2 + iStartIndex], PCF_GetOutUndriveableVehicle, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(myLocData.mnGenericBadGuy)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL UPDATE_SMUGGLER_PLANE_RECORDING(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, SMUGGLER_PLANE_STATE& smugglerPlaneRecordingStages, BLIP_INDEX& myLocationBlip[])
	
	FLOAT fTempPlanePos
	
	SWITCH smugglerPlaneRecordingStages
		CASE SMUGGLER_PLANE_STATE_INIT
		
			IF myLocData.dropData[0].smugglersRecordings[1].bConfigured
				IF myLocData.dropData[0].smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
					REQUEST_MODEL(myLocData.dropData[0].smugglersRecordings[1].carrecVehicle)
					REQUEST_MODEL(mnBombProp)
					REQUEST_MODEL(mnParachuteProp)
					REQUEST_ANIM_DICT("p_cargo_chute_s")					
					DEBUG_PRINT("REQUESTING CARGO AND PARACHUTE AND ANIMATION")
					REQUEST_VEHICLE_RECORDING(myLocData.dropData[0].smugglersRecordings[1].carrecNum, myLocData.dropData[0].smugglersRecordings[1].carrecName)
					PRINTLN("PLANE RECORDING NAME: ", myLocData.dropData[0].smugglersRecordings[1].carrecName)
					PRINTLN("PLANE RECORDING NUMBER: ", myLocData.dropData[0].smugglersRecordings[1].carrecNum)
				ENDIF
			ENDIF
			
			IF myLocData.dropData[0].smugglersRecordings[1].bConfigured
				IF myLocData.dropData[0].smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(myLocData.dropData[0].smugglersRecordings[1].carrecNum, myLocData.dropData[0].smugglersRecordings[1].carrecName)
					AND HAS_MODEL_LOADED(myLocData.dropData[0].smugglersRecordings[1].carrecVehicle)
					AND HAS_MODEL_LOADED(mnParachuteProp)
					AND HAS_MODEL_LOADED(mnBombProp)
					AND HAS_ANIM_DICT_LOADED("p_cargo_chute_s")
						DEBUG_PRINT("GOING TO STATE - SMUGGLER_PLANE_STATE_01")
						smugglerPlaneRecordingStages = SMUGGLER_PLANE_STATE_01
					ELSE
						IF HAS_VEHICLE_RECORDING_BEEN_LOADED(myLocData.dropData[0].smugglersRecordings[1].carrecNum, myLocData.dropData[0].smugglersRecordings[1].carrecName)
							PRINTLN("RECORDING HAS BEEN LOADED")
						ENDIF
						IF HAS_MODEL_LOADED(myLocData.dropData[0].smugglersRecordings[1].carrecVehicle)
							PRINTLN("VEHICLE HAS BEEN LOADED")
						ENDIF
						IF HAS_MODEL_LOADED(mnParachuteProp)
							PRINTLN("mnParachuteProp HAS BEEN LOADED")
						ENDIF
						IF HAS_MODEL_LOADED(mnBombProp)
							PRINTLN("mnBombProp HAS BEEN LOADED")
						ENDIF
						IF HAS_ANIM_DICT_LOADED("p_cargo_chute_s")
							PRINTLN("ANIM DICT HAS BEEN LOADED")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//======================================================================================================================================
		CASE SMUGGLER_PLANE_STATE_01
			smugArgs.vehSmugglerPlane = CREATE_VEHICLE(myLocData.dropData[0].smugglersRecordings[1].carrecVehicle, myLocData.dropData[0].smugglersRecordings[1].carCoords,  0.0)
			pedTempPilot = CREATE_PED_INSIDE_VEHICLE(smugArgs.vehSmugglerPlane, PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTempPilot, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(myLocData.mnGenericBadGuy)
			SET_ENTITY_RECORDS_COLLISIONS(smugArgs.vehSmugglerPlane, TRUE)
			SET_ENTITY_INVINCIBLE(smugArgs.vehSmugglerPlane, TRUE)
			
			FREEZE_ENTITY_POSITION( smugArgs.vehSmugglerPlane, TRUE )
			
			SET_VEHICLE_ENGINE_ON(smugArgs.vehSmugglerPlane, TRUE, TRUE)
			SET_VEHICLE_ENGINE_CAN_DEGRADE(smugArgs.vehSmugglerPlane, FALSE)
			SET_ENTITY_LOD_DIST(smugArgs.vehSmugglerPlane, 1000)
			CONTROL_LANDING_GEAR(smugArgs.vehSmugglerPlane, LGC_RETRACT_INSTANT)
			OPEN_BOMB_BAY_DOORS(smugArgs.vehSmugglerPlane)
			
			// Create the package to be dropped
			smugArgs.oArmsPackage = CREATE_OBJECT(mnBombProp, myLocData.dropData[0].smugglersRecordings[1].carCoords)
			SET_ENTITY_RECORDS_COLLISIONS(smugArgs.oArmsPackage, TRUE)
			SET_ENTITY_LOD_DIST(smugArgs.oArmsPackage, 1000)
			SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(smugArgs.oArmsPackage, TRUE)
			SET_ENTITY_VISIBLE(smugArgs.oArmsPackage, FALSE)
			
			// Create the parachute and attach to crate
			IF NOT IS_ENTITY_DEAD(smugArgs.oArmsPackage)
				smugArgs.oParachute = CREATE_OBJECT(mnParachuteProp, myLocData.dropData[0].smugglersRecordings[1].carCoords)
				ATTACH_ENTITY_TO_ENTITY(smugArgs.oParachute, smugArgs.oArmsPackage, 0, vParchuteOffset, <<0,0,0>>)
				SET_ENTITY_RECORDS_COLLISIONS(smugArgs.oParachute, TRUE)
				SET_ENTITY_LOD_DIST(smugArgs.oParachute, 1000)
				SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(smugArgs.oParachute, TRUE)
				SET_ENTITY_VISIBLE(smugArgs.oParachute, FALSE)
				PRINTLN("SMUGGLERS - CREATING PARACHUTE")
			ENDIF
			
			// Create the guy inside the plane and attach the cargo
			IF NOT IS_ENTITY_DEAD(smugArgs.vehSmugglerPlane)
				ATTACH_ENTITY_TO_ENTITY(smugArgs.oArmsPackage, smugArgs.vehSmugglerPlane, 0, << 0, -1.640, -0.480 >>, <<0,0,0>>)
			ENDIF
			
			DEBUG_PRINT("INSIDE STATE - SMUGGLER_PLANE_STATE_01")
			smugglerPlaneRecordingStages = SMUGGLER_PLANE_STATE_02
		BREAK
		//======================================================================================================================================
		CASE SMUGGLER_PLANE_STATE_02
			// TODO: update if we start using more than one vehicle
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[0])
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vPlaneRecTriggerPos, myLocData.dropData[0].smugglersRecordings[1].vTriggerDistance) 
				OR IS_ENTITY_AT_COORD(smugArgs.smugglersVehicles[0], myLocData.dropData[0].vPlaneRecTriggerPos, myLocData.dropData[0].smugglersRecordings[1].vTriggerDistance)
					IF DOES_ENTITY_EXIST(smugArgs.vehSmugglerPlane) AND IS_VEHICLE_DRIVEABLE(smugArgs.vehSmugglerPlane)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(smugArgs.vehSmugglerPlane)
							IF myLocData.dropData[0].smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
								FREEZE_ENTITY_POSITION( smugArgs.vehSmugglerPlane, FALSE )
								SET_ENTITY_COORDS(smugArgs.vehSmugglerPlane, myLocData.dropData[0].smugglersRecordings[1].carCoords)
								SET_ENTITY_QUATERNION(smugArgs.vehSmugglerPlane, myLocData.dropData[0].smugglersRecordings[1].x,  myLocData.dropData[0].smugglersRecordings[1].y,  myLocData.dropData[0].smugglersRecordings[1].j,  myLocData.dropData[0].smugglersRecordings[1].k)

								START_PLAYBACK_RECORDED_VEHICLE(smugArgs.vehSmugglerPlane, myLocData.dropData[0].smugglersRecordings[1].carrecNum, myLocData.dropData[0].smugglersRecordings[1].carrecName)
								SET_PLAYBACK_SPEED(smugArgs.vehSmugglerPlane, 1.2)
								DEBUG_PRINT("STARTING PLAYBACK ON SMUGGLERS PLANE")
								
								START_AUDIO_SCENE("PLANE_FLY_OVER_SCENE")
								PRINTLN("SMUGGLERS: STARTING AUDIO SCENE - PLANE_FLY_OVER_SCENE")
								
								smugglerPlaneRecordingStages = SMUGGLER_PLANE_STATE_03
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vPlaneRecTriggerPos, myLocData.dropData[0].smugglersRecordings[1].vTriggerDistance)
						IF DOES_ENTITY_EXIST(smugArgs.vehSmugglerPlane) AND IS_VEHICLE_DRIVEABLE(smugArgs.vehSmugglerPlane)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(smugArgs.vehSmugglerPlane)
								IF myLocData.dropData[0].smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
									FREEZE_ENTITY_POSITION( smugArgs.vehSmugglerPlane, FALSE )
									SET_ENTITY_COORDS(smugArgs.vehSmugglerPlane, myLocData.dropData[0].smugglersRecordings[1].carCoords)
									SET_ENTITY_QUATERNION(smugArgs.vehSmugglerPlane, myLocData.dropData[0].smugglersRecordings[1].x,  myLocData.dropData[0].smugglersRecordings[1].y,  myLocData.dropData[0].smugglersRecordings[1].j,  myLocData.dropData[0].smugglersRecordings[1].k)

									START_PLAYBACK_RECORDED_VEHICLE(smugArgs.vehSmugglerPlane, myLocData.dropData[0].smugglersRecordings[1].carrecNum, myLocData.dropData[0].smugglersRecordings[1].carrecName)
									SET_PLAYBACK_SPEED(smugArgs.vehSmugglerPlane, 1.2)
									DEBUG_PRINT("STARTING PLAYBACK ON SMUGGLERS PLANE")
									smugglerPlaneRecordingStages = SMUGGLER_PLANE_STATE_03
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//======================================================================================================================================
		CASE SMUGGLER_PLANE_STATE_03
			IF IS_VEHICLE_DRIVEABLE(smugArgs.vehSmugglerPlane) 
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(smugArgs.vehSmugglerPlane)
								
					fTempPlanePos = GET_POSITION_IN_RECORDING(smugArgs.vehSmugglerPlane)
					PRINTLN("SMUGGLER PLANE POSITION: ", fTempPlanePos)
					
					IF fTempPlanePos > myLocData.dropData[0].smugglersRecordings[1].fDropTime
						// DROP PACKAGE
						DEBUG_PRINT("DROPPING PACKAGE")
						DETACH_ENTITY(smugArgs.oArmsPackage)
						SET_ENTITY_LOAD_COLLISION_FLAG(smugArgs.oArmsPackage, TRUE)
						
						IF NOT DOES_BLIP_EXIST(smugArgs.blipPackage)
							STOP_PARTICLE_FX_SAFE(PTFXsmugglersFlare)
							smugArgs.blipPackage = ADD_BLIP_FOR_ENTITY(smugArgs.oArmsPackage)
							SET_BLIP_COLOUR(smugArgs.blipPackage, BLIP_COLOUR_GREEN)
							//SET_BLIP_SCALE(smugArgs.blipPackage, 0.5)
							PRINTLN("ADDING BLIP - smugArgs.blipPackage")
							PRINT_NOW("DTRFKGR_06",DEFAULT_GOD_TEXT_TIME,1)
							IF DOES_BLIP_EXIST(myLocationBlip[0])
								REMOVE_BLIP(myLocationBlip[0])
							ENDIF
						ENDIF
						
						SET_ENTITY_VISIBLE(smugArgs.oArmsPackage, TRUE)
						SET_ENTITY_VISIBLE(smugArgs.oParachute, TRUE)
						
						IF NOT IS_ENTITY_DEAD(smugArgs.oParachute)
							PLAY_ENTITY_ANIM(smugArgs.oParachute, "p_cargo_chute_s_deploy", "p_cargo_chute_s", 1.0, FALSE, TRUE)
							PRINTLN("SMUGGLERS - TASKING TO PLAY DEPLOY ANIM")
						ENDIF
						
						DEBUG_PRINT("STARTING PLAYBACK ON SMUGGLERS PLANE")
						smugglerPlaneRecordingStages = SMUGGLER_PLANE_STATE_04
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//======================================================================================================================================
		CASE SMUGGLER_PLANE_STATE_04
			IF DOES_ENTITY_EXIST(smugArgs.oArmsPackage)
			
				SET_ENTITY_LOAD_COLLISION_FLAG(smugArgs.oArmsPackage, TRUE)
			
				IF NOT IS_ENTITY_ATTACHED(smugArgs.oArmsPackage)
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(smugArgs.oArmsPackage)
					
						FREEZE_ENTITY_POSITION(smugArgs.oArmsPackage, TRUE)
						
						smugArgs.bPackageLanded = TRUE
						PRINTLN("SMUGGLERS - smugArgs.bPackageLanded = TRUE")
					
						PLAY_ENTITY_ANIM(smugArgs.oParachute, "p_cargo_chute_s_crumple", "p_cargo_chute_s", 0.5, FALSE, TRUE)
						PRINTLN("SMUGGLERS - TASKING TO PLAY CRUMPLE ANIM ON PARACHUTE")
						
						IF NOT IS_TIMER_STARTED(smugArgs.tParachuteTimer)
							START_TIMER_NOW(smugArgs.tParachuteTimer)
							PRINTLN("STARTING smugArgs.tParachuteTimer")
						ENDIF
						
						smugglerPlaneRecordingStages = SMUGGLER_PLANE_STATE_05
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//======================================================================================================================================
		CASE SMUGGLER_PLANE_STATE_05
			
			IF NOT smugArgs.bUnfreezeProp
				IF DOES_ENTITY_EXIST(smugArgs.oArmsPackage)
					FREEZE_ENTITY_POSITION(smugArgs.oArmsPackage, FALSE)
					APPLY_FORCE_TO_ENTITY(smugArgs.oArmsPackage, APPLY_TYPE_IMPULSE, <<0,0,0>>, <<0,0,0.2>>, 0, TRUE, TRUE, TRUE)
					smugArgs.bUnfreezeProp = TRUE
				ENDIF
			ENDIF
		
			IF DOES_ENTITY_EXIST(smugArgs.oParachute)
				IF IS_TIMER_STARTED(smugArgs.tParachuteTimer) 
					IF GET_TIMER_IN_SECONDS(smugArgs.tParachuteTimer) > 1.0
						IF DOES_ENTITY_EXIST(smugArgs.oArmsPackage)
							FREEZE_ENTITY_POSITION(smugArgs.oArmsPackage, TRUE)
							PRINTLN("FREEZING SMUGGLER'S PACKAGE")
						ENDIF
					
						DELETE_OBJECT(smugArgs.oParachute)
						PRINTLN("SMUGGLERS - DELETING PARACHUTE")
						smugglerPlaneRecordingStages = SMUGGLER_PLANE_STATE_06
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//======================================================================================================================================
		CASE SMUGGLER_PLANE_STATE_06
		
			SET_MODEL_AS_NO_LONGER_NEEDED(P_CARGO_CHUTE_S)
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_FLARE_01)
			REMOVE_ANIM_DICT("p_cargo_chute_s")
			REMOVE_VEHICLE_RECORDING(myLocData.dropData[0].smugglersRecordings[1].carrecNum, myLocData.dropData[0].smugglersRecordings[1].carrecName)
			
			IF DOES_ENTITY_EXIST(smugArgs.vehSmugglerPlane)
				IF IS_VEHICLE_DRIVEABLE(smugArgs.vehSmugglerPlane)
			
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(smugArgs.vehSmugglerPlane)
					
						STOP_AUDIO_SCENE("PLANE_FLY_OVER_SCENE")
						PRINTLN("SMUGGLERS: STOPPING AUDIO SCENE - PLANE_FLY_OVER_SCENE")
					
						SET_VEHICLE_AS_NO_LONGER_NEEDED(smugArgs.vehSmugglerPlane)
						SET_MODEL_AS_NO_LONGER_NEEDED(CUBAN800)
						smugglerPlaneRecordingStages = SMUGGLER_PLANE_STATE_07
					ELSE
						fTempPlanePos = GET_POSITION_IN_RECORDING(smugArgs.vehSmugglerPlane)
						
						IF NOT IS_ENTITY_ON_SCREEN(smugArgs.vehSmugglerPlane)
						
							STOP_AUDIO_SCENE("PLANE_FLY_OVER_SCENE")
							PRINTLN("SMUGGLERS: STOPPING AUDIO SCENE - PLANE_FLY_OVER_SCENE")
						
							DELETE_PED(pedTempPilot)
							PRINTLN("DELETING PED - pedTempPilot")
							DELETE_VEHICLE(smugArgs.vehSmugglerPlane)
							PRINTLN("DELETING VEHICLE - smugArgs.vehSmugglerPlan")
							
							SET_MODEL_AS_NO_LONGER_NEEDED(CUBAN800)
							smugglerPlaneRecordingStages = SMUGGLER_PLANE_STATE_07
						ENDIF
	//					PRINTLN("SMUGGLER PLANE POSITION: ", fTempPlanePos)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//======================================================================================================================================
		CASE SMUGGLER_PLANE_STATE_07
		
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SMUGGLERS_LOCK_VEHICLE_DOORS(BOOL bDrugsLoaded, VEHICLE_INDEX& myVehicle) 
	IF bDrugsLoaded
		SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_LOCKED_NO_PASSENGERS)
		PRINTLN("LOCKING PASSENGER DOOR")
	ELSE
		PRINTLN("bDrugsLoaded = FALSE")
	ENDIF
ENDPROC

PROC CREATE_SMUGGLERS_MODE(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, BOOL& bSmugglersCreated)
	INT idx
#IF IS_DEBUG_BUILD
	FLOAT fSmugglersTopSpeed, fSmugglersTopSpeedNew
#ENDIF	//	IS_DEBUG_BUILD
	
	IF NOT bSmugglersCreated
	
		ADD_RELATIONSHIP_GROUP("SmugglerEnemies", smugArgs.relSmugglersEnemies)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, smugArgs.relSmugglersEnemies)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, smugArgs.relSmugglersEnemies, RELGROUPHASH_PLAYER)
		DEBUG_PRINT("SETTING SMUGGLERS RELATIONSHIP GROUPS")
	
		REPEAT myLocData.iNumSmugVehicles idx
			smugArgs.smugglersVehicles[idx] = CREATE_VEHICLE(myLocData.mnSmugglersVehicles[idx], myLocData.dropData[0].vSmugVehPos[idx], myLocData.dropData[0].fSmugVehRot[idx])
			CREATE_SMUGGLER_PED_IN_VEHICLE(myLocData, smugArgs, idx)
			SET_ENTITY_LOAD_COLLISION_FLAG(smugArgs.smugglersVehicles[idx], TRUE)
			SET_VEHICLE_EXTRA(smugArgs.smugglersVehicles[idx], 1, TRUE)
//			SET_VEHICLE_DOORS_LOCKED(smugArgs.smugglersVehicles[idx], VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_MODEL_AS_NO_LONGER_NEEDED(myLocData.mnSmugglersVehicles[idx])
			// Get and modify the top speed for the smuggler's vehicle...
#IF IS_DEBUG_BUILD
			fSmugglersTopSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED(smugArgs.smugglersVehicles[idx])
			PRINTLN("fSmugglersTopSpeed = ", fSmugglersTopSpeed)
#ENDIF	//	IS_DEBUG_BUILD
			MODIFY_VEHICLE_TOP_SPEED(smugArgs.smugglersVehicles[idx], 100.0)
#IF IS_DEBUG_BUILD
			fSmugglersTopSpeedNew = GET_VEHICLE_ESTIMATED_MAX_SPEED(smugArgs.smugglersVehicles[idx])
			PRINTLN("fSmugglersTopSpeedNew = ", fSmugglersTopSpeedNew)
#ENDIF	//	IS_DEBUG_BUILD
		ENDREPEAT
		
		DEBUG_PRINT("CREATING SMUGGLERS")
		bSmugglersCreated = TRUE
		
		DEBUG_PRINT("SET_WANTED_LEVEL_MULTIPLIER TO ZERO VIA SMUGGLERS MODE")
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
	ENDIF
	
ENDPROC

PROC UPDATE_HELI_PED(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, SMUGGLERS_CREATE_HELI_VEHICLE& smugglersCreateHeliPedStages)
	VECTOR vHeliPedPostion
	
	SWITCH smugglersCreateHeliPedStages
		CASE SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_01 
			REQUEST_MODEL(myLocData.mnSmugglersHeli)
			DEBUG_PRINT("REQUESTING SMUGGLERS HELI")
			
			IF HAS_MODEL_LOADED(myLocData.mnSmugglersHeli)
				smugglersCreateHeliPedStages = SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_02
			ELSE
				PRINTLN("WAITING TO LOAD myLocData.mnSmugglersHeli")
			ENDIF
		BREAK
		//===========================================================================================================================
		CASE SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_02
			// Creating Helicopter for ending
			smugArgs.smugglersHeliVeh = CREATE_VEHICLE(myLocData.mnSmugglersHeli, myLocData.endData[1].vSmugHeliPos, myLocData.endData[1].fSmugHeliRot)
			SET_ENTITY_LOAD_COLLISION_FLAG(smugArgs.smugglersHeliVeh, TRUE)
			FREEZE_ENTITY_POSITION(smugArgs.smugglersHeliVeh, TRUE)
			smugArgs.smugglersHeliPed = CREATE_PED(PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(smugArgs.smugglersHeliVeh, <<0, 4, 0>>), 0)
			GIVE_WEAPON_TO_PED(smugArgs.smugglersHeliPed, WEAPONTYPE_SMG, 1000, TRUE)
			SET_ENTITY_LOAD_COLLISION_FLAG(smugArgs.smugglersHeliPed, TRUE)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(smugArgs.smugglersHeliPed, TRUE)
			SET_PED_KEEP_TASK(smugArgs.smugglersHeliPed, TRUE)
			SET_VEHICLE_ENGINE_ON(smugArgs.smugglersHeliVeh, FALSE, TRUE)
			SET_VEHICLE_ON_GROUND_PROPERLY(smugArgs.smugglersHeliVeh)
			SET_VEHICLE_DOORS_LOCKED(smugArgs.smugglersHeliVeh, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_PED_RELATIONSHIP_GROUP_HASH(smugArgs.smugglersHeliPed, smugArgs.relSmugglersEnemies)
//			SET_MODEL_AS_NO_LONGER_NEEDED(myLocData.mnSmugglersHeli)
			
			smugglersCreateHeliPedStages = SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_03
		BREAK
		//===========================================================================================================================
		CASE SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_03
			IF NOT IS_ENTITY_DEAD(smugArgs.smugglersHeliPed)
				vHeliPedPostion = GET_ENTITY_COORDS(smugArgs.smugglersHeliPed)
//				PRINTLN("vHeliPedPostion = ", vHeliPedPostion)
			ELSE
				smugArgs.bHeliPedDead = TRUE
				PRINTLN("smugArgs.bHeliPedDead = TRUE")
				
				smugglersCreateHeliPedStages = SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_04
				BREAK
			ENDIF
				
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) 
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vHeliPedPostion) < 50
					TASK_LOOK_AT_ENTITY(smugArgs.smugglersHeliPed, PLAYER_PED_ID(), -1)
					TASK_COMBAT_PED(smugArgs.smugglersHeliPed, PLAYER_PED_ID())
					
					PRINTLN("PLAYER CLOSE TO HELI PED, TASKING TO ATTACK")
					smugglersCreateHeliPedStages = SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_04
				ENDIF
			ENDIF
		BREAK
		//===========================================================================================================================
		CASE SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_04
		
		BREAK
	ENDSWITCH
ENDPROC

PROC DELETE_SMUGGLER_HELI_PED(SMUGGLERS_MODE_ARGS& smugArgs)
	IF DOES_ENTITY_EXIST(smugArgs.smugglersHeliVeh)
		DELETE_VEHICLE(smugArgs.smugglersHeliVeh)
		PRINTLN("DELETING smugArgs.smugglersHeliVeh")
	ENDIF
	IF DOES_ENTITY_EXIST(smugArgs.smugglersHeliPed)
		DELETE_PED(smugArgs.smugglersHeliPed)
		PRINTLN("DELETING smugArgs.smugglersHeliPed")
	ENDIF 
ENDPROC

PROC PAUSE_OR_STOP_RECORDING(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, INT& idx)
	IF myLocData.dropData[0].smugglersRecordings[idx].bWaypointConfigured
		IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx])
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(smugArgs.smugglersVehicles[idx])
				VEHICLE_WAYPOINT_PLAYBACK_PAUSE(smugArgs.smugglersVehicles[idx])
				PRINTLN("PAUSING WAYPOINT RECORDING")
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(smugArgs.smugglersVehicles[idx])
				STOP_PLAYBACK_RECORDED_VEHICLE(smugArgs.smugglersVehicles[idx])
				DEBUG_PRINT("STOPPING PLAYBACK RECORDING")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GRAB_SMUGGLERS_COORDINATES(SMUGGLERS_MODE_ARGS& smugArgs, INT idx)
	INT iReturnLanes
	VECTOR vOffset = <<0, -5, 0>>
	VECTOR vCoordToUseToGrabNodePos
	
	// Grab positions for dynamic cutscene
	smugArgs.vDefeatedSmugglerVehCoor = GET_ENTITY_COORDS(smugArgs.smugglersVehicles[idx])
	smugArgs.fDefeatedSmugglerVehHeading = GET_ENTITY_HEADING(smugArgs.smugglersVehicles[idx])
	smugArgs.vDefeatedSmugglerVehRotation = GET_ENTITY_ROTATION(smugArgs.smugglersVehicles[idx])
	
	vCoordToUseToGrabNodePos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(smugArgs.vDefeatedSmugglerVehCoor, smugArgs.fDefeatedSmugglerVehHeading, vOffset)
	
	// Grab vehicle node for the Smuggler's car
	GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vCoordToUseToGrabNodePos, 1, smugArgs.vSafeCarPos, smugArgs.fVehicleNodeHeading01, iReturnLanes)
	
	// Grab nearest vehicle node for the player
	 GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vCoordToUseToGrabNodePos, 3, smugArgs.vVehicleNodePos, smugArgs.fVehicleNodeHeading, iReturnLanes)
	 smugArgs.fVehicleNodeHeading = smugArgs.fVehicleNodeHeading
	 
	 IF VDIST(smugArgs.vSafeCarPos, smugArgs.vVehicleNodePos) < 3
	 	// Grab vehicle node for the Smuggler's car
		GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vCoordToUseToGrabNodePos, 4, smugArgs.vVehicleNodePos, smugArgs.fVehicleNodeHeading, iReturnLanes)
	 	PRINTLN("TOO CLOSE, RE-GRABBING NODE")
	 ENDIF
	 
	 iReturnLanes = iReturnLanes
ENDPROC

FUNC BOOL IS_DEAD_VEHICLE_ALMOST_STOPPED(VEHICLE_INDEX invehicle)
    FLOAT fTemp
    IF IS_ENTITY_DEAD(invehicle)
        fTemp = GET_ENTITY_SPEED(invehicle)
        IF (fTemp > -0.5)
        AND (fTemp < 0.5)
            RETURN(TRUE)
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

FUNC BOOL KILL_PEDS_RETRIEVE_DRUGS(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, SMUGGLERS_RETRIEVE_DRUGS& smugglerRetrieveState, 
TEXT_LABEL_31& sLastObjective, VEHICLE_INDEX& myVehicle, CUTSCENE_ARGS& cutArgs, DRUG_CARGO_ARGS& drugCargoArgs, BOOL& bOutOfVehicle, BOOL& bDrugsLoaded)
	
	INT idx, idx2
	BOOL bPedsDead = TRUE
//	VECTOR vOffsetPoint02 = <<0,10,1>>		// Right behind car
	VECTOR vPlayerLocation
	
	SWITCH smugglerRetrieveState
	
		// First check if the peds inside vehicle with drugs are dead...
		CASE SMUGGLERS_RETRIEVE_DRUGS_CHECK_PEDS_DEAD
			REPEAT (myLocData.iNumSmugPedsPerVehicle * myLocData.iNumSmugVehicles) idx
				IF DOES_ENTITY_EXIST(smugArgs.smugglersPeds[idx])
					IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[idx])
						bPedsDead = FALSE
//						IF NOT DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[idx]) 
//							IF NOT IS_PED_IN_ANY_VEHICLE(smugArgs.smugglersPeds[idx])
//								IF NOT DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[idx])
//									smugArgs.blipSmugglersPeds[idx] = ADD_BLIP_FOR_ENTITY(smugArgs.smugglersPeds[idx])
//									SET_BLIP_COLOUR(smugArgs.blipSmugglersPeds[idx], BLIP_COLOUR_RED)
//									PRINTLN("ADDING BLIPS FOR SMUGGLER PEDS BECAUSE THEY ARE NOT IN THEIR VEHICLE IDX = ", idx)
//								ENDIF
//							ENDIF
//						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF bPedsDead
				
				IF DOES_ENTITY_EXIST(smugArgs.smugglersHeliPed)
					IF IS_ENTITY_OCCLUDED(smugArgs.smugglersHeliPed)
						DELETE_SMUGGLER_HELI_PED(smugArgs)
						PRINTLN("DELETING HELI PED AND VEHICLE")
					ENDIF
				ENDIF
			
				// Remove blips on peds if they exist
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[0])
					REMOVE_BLIP(smugArgs.blipSmugglersPeds[0])
					PRINTLN("REMOVING BLIP ON FIRST SMUGGLER PED")
				ENDIF
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[1])
					REMOVE_BLIP(smugArgs.blipSmugglersPeds[1])
					PRINTLN("REMOVING BLIP ON SECOND SMUGGLER PED")
				ELSE
					PRINTLN("BLIP DOES NOT EXIST")
				ENDIF
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[0])
					REMOVE_BLIP(smugArgs.blipSmugglersCars[0])
					PRINTLN("REMOVING BLIP - smugArgs.blipSmugglersCars[0]")
				ENDIF
				
				SET_VEHICLE_DOOR_OPEN(smugArgs.smugglersVehicles[0], SC_DOOR_BOOT)
				
				IF NOT bOutOfVehicle
					IF smugArgs.bPickupAIFirst
						smugArgs.bSmugglerPedsDead = TRUE
						PRINTLN("GOING TO STATE - SMUGGLERS_RETRIEVE_DRUGS_GOTO_VEHICLE")
						smugglerRetrieveState = SMUGGLERS_RETRIEVE_DRUGS_GOTO_VEHICLE
					ELSE
						DEBUG_PRINT("smugArgs.bPickupAIFirst IS FALSE: GOING TO STATE - SMUGGLERS_RETRIEVE_DRUGS_COMPLETE NEW")
						smugglerRetrieveState = SMUGGLERS_RETRIEVE_DRUGS_COMPLETE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//===========================================================================================================================
		CASE SMUGGLERS_RETRIEVE_DRUGS_GOTO_VEHICLE
			REPEAT myLocData.iNumSmugVehicles idx
				// Remove blips on cars 
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[idx])
					REMOVE_BLIP(smugArgs.blipSmugglersCars[idx])
					DEBUG_PRINT("REMOVING blipSmugglersCars 01")
				ENDIF
				
				PAUSE_OR_STOP_RECORDING(myLocData, smugArgs, idx)
				
				// Loop on vehicles, check only the one that had the drugs
//				IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx]) AND smugArgs.bCarHasDrugs[idx]
					// Check that the passengers are dead... to retrieve the drug package
					REPEAT (myLocData.iNumSmugPedsPerVehicle * myLocData.iNumSmugVehicles) idx2
						IF IS_ENTITY_DEAD(smugArgs.smugglersPeds[idx2])
							
							// Add blip to pickup the package
							IF NOT DOES_BLIP_EXIST(smugArgs.blipSmugglersRetrieve)
													
								smugArgs.blipSmugglersRetrieve = ADD_BLIP_FOR_ENTITY(smugArgs.smugglersVehicles[idx])
								DEBUG_PRINT("ADDING BLIP TO PICK UP THE PACKAGE")
								SET_BLIP_COLOUR(smugArgs.blipSmugglersRetrieve, BLIP_COLOUR_GREEN)
								
								// Print objective
								PRINT_NOW("DTRFKGR_06", DEFAULT_GOD_TEXT_TIME, 1)
								sLastObjective = "DTRFKGR_06"
							ENDIF
							
							// Wait until the vehicle is stopped to grab it's coordinates.
							IF IS_VEHICLE_ALMOST_STOPPED_GROUND(smugArgs.smugglersVehicles[idx])
								GRAB_SMUGGLERS_COORDINATES(smugArgs, idx)
								smugglerRetrieveState = SMUGGLERS_RETRIEVE_DRUGS_CUTSCENE
							ELSE
								IF IS_DEAD_VEHICLE_ALMOST_STOPPED(smugArgs.smugglersVehicles[idx])
									GRAB_SMUGGLERS_COORDINATES(smugArgs, idx)
									smugglerRetrieveState = SMUGGLERS_RETRIEVE_DRUGS_CUTSCENE
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
//				ENDIF 
			ENDREPEAT
		BREAK
		//===========================================================================================================================
		CASE SMUGGLERS_RETRIEVE_DRUGS_CUTSCENE
		
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), smugArgs.vDefeatedSmugglerVehCoor, <<5,5,4>>)
				IF IS_TIMER_STARTED(smugArgs.tBufferTimer)
					IF GET_TIMER_IN_SECONDS(smugArgs.tBufferTimer) > 1.0
						IF IS_VEHICLE_DRIVEABLE(myVehicle)
							
							// Remove blue package blip
							IF DOES_BLIP_EXIST(smugArgs.blipSmugglersRetrieve)
								REMOVE_BLIP(smugArgs.blipSmugglersRetrieve)
							ENDIF
						
							// Remove from AI 
							REMOVE_DRUG_CARGO(drugCargoArgs)
							// Add to player's vehicle
							ADD_DRUGS_TO_VEHICLE(drugCargoArgs, myLocData, myVehicle)
							smugArgs.bCarHasDrugs[0] = FALSE
							
							bDrugsLoaded = TRUE
							SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_LOCKED_NO_PASSENGERS)
				
							DEBUG_PRINT("GOING TO STATE - SMUGGLERS_RETRIEVE_DRUGS_COMPLETE NEW")
							smugglerRetrieveState = SMUGGLERS_RETRIEVE_DRUGS_COMPLETE
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_TIMER_STARTED(smugArgs.tBufferTimer)
						START_TIMER_NOW(smugArgs.tBufferTimer)
						PRINTLN("STARTING BUFFER TIMER - 01")
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					vPlayerLocation =  GET_ENTITY_COORDS(PLAYER_PED_ID())
				ENDIF
			
				IF VDIST2(vPlayerLocation, smugArgs.vDefeatedSmugglerVehCoor) > 122500 // 350m
					PRINTLN("smugArgs.bAbandonedPackage = TRUE")
					smugArgs.bAbandonedPackage = TRUE
				ELIF VDIST2(vPlayerLocation, smugArgs.vDefeatedSmugglerVehCoor) > 10000 // 100m
					IF NOT bPrintReturnToPackageWarning
						PRINT_NOW("DTRFKGR_06a",DEFAULT_GOD_TEXT_TIME,1)
						PRINTLN("bPrintReturnToPackageWarning = TRUE")
						bPrintReturnToPackageWarning = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//===========================================================================================================================
		CASE SMUGGLERS_RETRIEVE_DRUGS_COMPLETE
			
			cutArgs.cutsceneState = CUTSCENE_STATE_START
			
			DEBUG_PRINT("SET_WANTED_LEVEL_MULTIPLIER TO 1.0 VIA SMUGGLERS MODE")
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			
			// Remove blips on peds if they exist
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[0])
					REMOVE_BLIP(smugArgs.blipSmugglersPeds[0])
					PRINTLN("REMOVING BLIP ON FIRST SMUGGLER PED")
				ENDIF
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[1])
					REMOVE_BLIP(smugArgs.blipSmugglersPeds[1])
					PRINTLN("REMOVING BLIP ON SECOND SMUGGLER PED")
				ENDIF
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[0])
					REMOVE_BLIP(smugArgs.blipSmugglersCars[0])
					PRINTLN("REMOVING BLIP - smugArgs.blipSmugglersCars[0]")
				ENDIF
			
			DEBUG_PRINT("RETRIEVE DRUG CHECK COMPLETE - RETURNING TRUE")
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Checks if the smuggler peds are dead prior to reaching the pickup location

FUNC BOOL SMUGGLER_PED_DEAD_CHECK(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, structPedsForConversation& MyLocalPedStruct)
	
	INT idx, idx2, iStartIndex
	SEQUENCE_INDEX iSeqTask
	BOOL bPedsDead = TRUE
	PED_INDEX pedSmugglerDriver
//	SEQUENCE_INDEX iSeq
	INT iRandConvo
	
	REPEAT myLocData.iNumSmugVehicles idx
		IF IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[idx]) AND NOT IS_VEHICLE_STUCK_TIMER_UP(smugArgs.smugglersVehicles[idx], VEH_STUCK_ON_ROOF, 3000)
		
			REPEAT myLocData.iNumSmugPedsPerVehicle idx2
				iStartIndex = idx*myLocData.iNumSmugPedsPerVehicle
				IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[idx2 + iStartIndex])
					bPedsDead = FALSE
				ELSE
					IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[idx2 + iStartIndex])
						REMOVE_BLIP(smugArgs.blipSmugglersPeds[idx2 + iStartIndex])
						DEBUG_PRINT("REMOVING BLIPS ON SMUGGLER PEDS")
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx])
				pedSmugglerDriver = GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx])
			ENDIF
			
			IF IS_PED_INJURED(pedSmugglerDriver)
				
				PAUSE_OR_STOP_RECORDING(myLocData, smugArgs, idx)
				
				// If the driver is dead, task the the other guy to get out and keep shooting.
				IF NOT bTaskedGuyToLeave
					IF IS_VEHICLE_ALMOST_STOPPED_GROUND(smugArgs.smugglersVehicles[idx])
						REPEAT myLocData.iNumSmugPedsPerVehicle idx2
							iStartIndex = idx*myLocData.iNumSmugPedsPerVehicle
	
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(smugArgs.smugglersPeds[1], TRUE)
							
							IF DOES_ENTITY_EXIST(smugArgs.smugglersPeds[idx2 + iStartIndex]) AND NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx])
								IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx])) 
									IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx]) AND NOT IS_ENTITY_DEAD(GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx], VS_FRONT_RIGHT))
										IF GET_SCRIPT_TASK_STATUS(GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx], VS_FRONT_RIGHT), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) <> PERFORMING_TASK
											TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx], VS_FRONT_RIGHT), smugArgs.smugglersVehicles[idx])
											PRINTLN("TASKING GUY TO SWITCH SEATS")
											bTaskedGuyToLeave = TRUE
										ENDIF
									ELSE
//										PRINTLN("FAILED CHECK HERE")
									ENDIF
								ENDIF
							ENDIF									
						ENDREPEAT
					ENDIF
				ENDIF
			ELSE
				IF bTaskedGuyToLeave
					
					pedSmugglerDriver = GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx])
					
					IF pedSmugglerDriver != PLAYER_PED_ID()
						IF GET_SCRIPT_TASK_STATUS(pedSmugglerDriver, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) <> PERFORMING_TASK
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedSmugglerDriver, smugArgs.smugglersVehicles[idx],
							myLocData.dropData[0].smugglersRecordings[idx].carrecWaypointName, DRIVINGMODE_PLOUGHTHROUGH, 0, 
							EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN | EWAYPOINT_START_FROM_CLOSEST_POINT, -1, -1)
							PRINTLN("TASKING PASSENGER TO USE WAYPOINT RECORDING")
						ENDIF
					ELSE
						PRINTLN("THE DRIVER IS THE PLAYER!!!")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// PEDS GOT OUT OF THE CAR
			REPEAT myLocData.iNumSmugPedsPerVehicle idx2
				iStartIndex = idx*myLocData.iNumSmugPedsPerVehicle
				// Check if the peds out of their cars are dead/injured... if so remove their blips
				
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[0])
					REMOVE_BLIP(smugArgs.blipSmugglersCars[0])
					PRINTLN("REMOVING BLIP ON SMUGGLERS CAR, SINCE IT'S UNDRIVABLE")
				ENDIF
				IF NOT DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[idx2 + iStartIndex])
					smugArgs.blipSmugglersPeds[idx2 + iStartIndex] = ADD_BLIP_FOR_ENTITY(smugArgs.smugglersPeds[idx2 + iStartIndex])
					SET_BLIP_SCALE(smugArgs.blipSmugglersPeds[idx2 + iStartIndex], BLIP_SIZE_PED)
					SET_BLIP_COLOUR(smugArgs.blipSmugglersPeds[idx2 + iStartIndex], BLIP_COLOUR_RED)
					PRINTLN("ADDING BLIP TO SMUGGLER PED: ", idx2 + iStartIndex)
				ENDIF
				
				IF IS_ENTITY_DEAD(smugArgs.smugglersPeds[idx2 + iStartIndex])
					IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[idx2 + iStartIndex])
						REMOVE_BLIP(smugArgs.blipSmugglersPeds[idx2 + iStartIndex])
						DEBUG_PRINT("REMOVING BLIPS ON SMUGGLER PEDS")
					ENDIF
				ELSE
					IF NOT smugArgs.bTaskedToCombat[idx2 + iStartIndex]
						CLEAR_SEQUENCE_TASK(iSeqTask)
						OPEN_SEQUENCE_TASK(iSeqTask)
							TASK_LEAVE_ANY_VEHICLE(NULL)
							TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(iSeqTask)
						TASK_PERFORM_SEQUENCE(smugArgs.smugglersPeds[idx2 + iStartIndex], iSeqTask)
						CLEAR_SEQUENCE_TASK(iSeqTask)
						PRINTLN("TASKING GUYS TO LEAVE VEHICLE AND COMBAT, BECAUSE IT HAS BECOME UNDRIVEABLE")
						
						smugArgs.bTaskedToCombat[idx2 + iStartIndex] = TRUE
					ENDIF
					
					bPedsDead = FALSE
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
	// PEDS ARE DEAD AND THEY NEVER GOT OUT OF THE CAR
	IF bPedsDead
		REPEAT myLocData.iNumSmugVehicles idx
			// Remove blips on cars 
			IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[idx])
				REMOVE_BLIP(smugArgs.blipSmugglersCars[idx])
				DEBUG_PRINT("REMOVING blipSmugglersCars 02")
			ENDIF
			
			PAUSE_OR_STOP_RECORDING(myLocData, smugArgs, idx)
		ENDREPEAT
		
		bSmuggglersDead = TRUE
		bSmuggglersDead = bSmuggglersDead
		
		IF NOT bSmugglersLost 
			IF NOT bPlayedKilledSmugglersLine
				iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
				
				IF iRandConvo = 0
					IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL01", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("iRandConvo = ", iRandConvo)
						PRINTLN("bPlayedKilledSmugglersLine = TRUE")
						bPlayedKilledSmugglersLine = TRUE
					ENDIF
				ELIF iRandConvo = 1
					IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL02", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("iRandConvo = ", iRandConvo)
						PRINTLN("bPlayedKilledSmugglersLine = TRUE")
						bPlayedKilledSmugglersLine = TRUE
					ENDIF
				ELIF iRandConvo = 2
					IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL03", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("iRandConvo = ", iRandConvo)
						PRINTLN("bPlayedKilledSmugglersLine = TRUE")
						bPlayedKilledSmugglersLine = TRUE
					ENDIF
				ELIF iRandConvo = 3
					IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL04", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("iRandConvo = ", iRandConvo)
						PRINTLN("bPlayedKilledSmugglersLine = TRUE")
						bPlayedKilledSmugglersLine = TRUE
					ENDIF
				ELIF iRandConvo = 4
					IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL05", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("iRandConvo = ", iRandConvo)
						PRINTLN("bPlayedKilledSmugglersLine = TRUE")
						bPlayedKilledSmugglersLine = TRUE
					ENDIF
				ELIF iRandConvo = 5
					IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL06", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						PRINTLN("iRandConvo = ", iRandConvo)
						PRINTLN("bPlayedKilledSmugglersLine = TRUE")
						bPlayedKilledSmugglersLine = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		DEBUG_PRINT("RETURING TRUE ON SMUGGLER_PED_DEAD_CHECK")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DISABLE_TIRES_DUE_TO_DAMAGE(INT index, SMUGGLERS_MODE_ARGS& smugArgs)
	INT iCurrentHealth
	INT iHealthDelta
	
	IF NOT bGrabbedHealth
		iStartingHealth = GET_ENTITY_HEALTH(smugArgs.smugglersVehicles[index])
		PRINTLN("iStartingHealth = ", iStartingHealth)
		bGrabbedHealth = TRUE
	ENDIF
	
	iCurrentHealth = GET_ENTITY_HEALTH(smugArgs.smugglersVehicles[index])
	iHealthDelta = iStartingHealth - iCurrentHealth
//	PRINTLN("iHealthDelta = ", iHealthDelta)
	
	SWITCH iStages
		CASE 0
			IF iHealthDelta > 150
				IF NOT IS_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_REAR_LEFT)
					SET_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_REAR_LEFT)
					PRINTLN("INSIDE DAMAGE lEVEL 1")
					iStages ++
				ELSE
					PRINTLN("INSIDE DAMAGE lEVEL 1a")
					iStages ++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF iHealthDelta > 250
				IF NOT IS_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_REAR_RIGHT)
					SET_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_REAR_RIGHT)
					PRINTLN("INSIDE DAMAGE lEVEL 2")
					iStages ++
				ELSE
					PRINTLN("INSIDE DAMAGE lEVEL 2a")
					iStages ++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF iHealthDelta > 300
				IF NOT IS_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_FRONT_LEFT)
					SET_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_FRONT_LEFT)
					PRINTLN("INSIDE DAMAGE lEVEL 3")
					iStages ++
				ELSE
					PRINTLN("INSIDE DAMAGE lEVEL 3a")
					iStages ++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF iHealthDelta > 400
				IF NOT IS_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_FRONT_RIGHT)
					SET_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_FRONT_RIGHT)
					PRINTLN("INSIDE DAMAGE lEVEL 4")
					iStages ++
				ELSE
					PRINTLN("INSIDE DAMAGE lEVEL 4a")
					iStages ++
				ENDIF
			ENDIF
		BREAK
		CASE 4
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_SMUGGLER_VEHICLES_TIRES_DAMAGED(INT& index, SMUGGLERS_MODE_ARGS& smugArgs)
	
	IF IS_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_FRONT_LEFT)
	OR IS_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_FRONT_RIGHT)
	OR IS_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_REAR_LEFT)
	OR IS_VEHICLE_TYRE_BURST(smugArgs.smugglersVehicles[index], SC_WHEEL_CAR_REAR_RIGHT)
//		DEBUG_PRINT("A TIRE HAS BEEN BURST ON SMUGGLER VEHICLE, RETURNING TRUE")
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC AFTER_PICKUP_SPEED_ADJUSTMENT(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs,INT& idx)

	// If the player has a greater distance to the objective(most likely behind the AI), increase the playback speed of the smuggler vehicle.
	IF fDistToObjPlayer > fDistToObjAI 
		// If the disance between the player and AI is NOT too great, make their speed normal... relative to the default speed.
		IF NOT (fDistBetweenPlayerAndAI > SMUGGLER_AFTER_PICKUP_TOO_FAR_DISTANCE)
			SET_PLAYBACK_SPEED(smugArgs.smugglersVehicles[idx], (myLocData.dropData[0].smugglersRecordings[0].fDefaultPlaybackSpeed * myLocData.dropData[0].smugglersRecordings[0].fMultiplier))
//			PRINTSTRING("PLAYBACK SPEED POST A = ")
//			PRINTFLOAT(myLocData.dropData[0].smugglersRecordings[0].fDefaultPlaybackSpeed * myLocData.dropData[0].smugglersRecordings[0].fMultiplier)
//			PRINTNL()
		// If the distnace is too great, slow them down... less than their default speed
		ELSE
			IF NOT IS_ENTITY_ON_SCREEN(smugArgs.smugglersVehicles[idx]) OR IS_ENTITY_OCCLUDED(smugArgs.smugglersVehicles[idx])
				SET_PLAYBACK_SPEED(smugArgs.smugglersVehicles[idx], (myLocData.dropData[0].smugglersRecordings[0].fDefaultPlaybackSpeed - 0.2))
	//			PRINTSTRING("PLAYBACK SPEED POST B = ")
	//			PRINTFLOAT((myLocData.dropData[0].smugglersRecordings[0].fDefaultPlaybackSpeed - 0.2))
	//			PRINTNL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TO_PICKUP_SPEED_ADJUSTMENT(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, INT& idx)
	IF fDistToObjPlayer > fDistToObjAI 
		// If the player has a greater distance to the objective, slow down the playback speed.
		SET_PLAYBACK_SPEED(smugArgs.smugglersVehicles[idx], myLocData.dropData[0].smugglersRecordings[0].fDefaultPlaybackSpeed)
//		PRINTSTRING("PLAYBACK SPEED = ")
//		PRINTFLOAT(myLocData.dropData[0].smugglersRecordings[0].fDefaultPlaybackSpeed)
//		PRINTNL()
	ELSE
		/* If the AI distance to the objective is greater than that of the player plus 50m, and they're not on screen, 
		...greatly incrase playback speed.*/
		IF (fDistToObjAI > fDistToObjPlayer + 50.0) AND NOT IS_ENTITY_ON_SCREEN(smugArgs.smugglersVehicles[idx])
			SET_PLAYBACK_SPEED(smugArgs.smugglersVehicles[idx], (myLocData.dropData[0].smugglersRecordings[0].fDefaultPlaybackSpeed + 0.6))
//			DEBUG_PRINT("PLAYBACK SPEED = 1.5")
		// Otherwise maintain default speed.
		ELSE
			SET_PLAYBACK_SPEED(smugArgs.smugglersVehicles[idx], (myLocData.dropData[0].smugglersRecordings[0].fDefaultPlaybackSpeed + 0.4))
//			DEBUG_PRINT("PLAYBACK SPEED = 0.8")
		ENDIF
	ENDIF
ENDPROC

PROC SMUGGLERS_WAYPOINT_RUBBERBAND(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, VEHICLE_INDEX& myVehicle, VECTOR& vVectorToUse, BOOL bToPickup = TRUE)
	FLOAT fCurrentSpeed, fVehicleMaxSpeed
	VECTOR vPlayerCoords, vAIPos
	
	PED_INDEX pedDriver
	IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0])
		pedDriver = GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[0])
	ENDIF
	
	pedDriver = pedDriver
	myLocData.dropData[0].smugglersRecordings[0].carrecWaypointName = myLocData.dropData[0].smugglersRecordings[0].carrecWaypointName
	
	IF NOT IS_ENTITY_DEAD(myVehicle) AND IS_VEHICLE_DRIVEABLE(myVehicle)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
				fVehicleMaxSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED(myVehicle)
				fVehicleMaxSpeed = fVehicleMaxSpeed
//				PRINTLN("MAX SPEED = ", fVehicleMaxSpeed)
			ENDIF
		ENDIF
	ENDIF
	
	// Grab the player's coordinates
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	// Grab distance of the player's distance to pickup site.
	fDistToObjPlayer = VDIST(vPlayerCoords, vVectorToUse)
//	PRINTLN("fDistToObjPlayer = ", fDistToObjPlayer)
	
	// Grab the position of the Smuggler vehicle
	vAIPos = GET_ENTITY_COORDS(smugArgs.smugglersVehicles[0])
	
	// Get distance to objective
	fDistToObjAI = VDIST(vAIPos, vVectorToUse)
//	PRINTLN("fDistToObjAI = ", fDistToObjAI)
	
	fDistBetweenPlayerAndAI = VDIST(vAIPos, vPlayerCoords)
//	PRINTLN("fDistBetweenPlayerAndAI = ", fDistBetweenPlayerAndAI)
	
	IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0]) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[0]) 
		fCurrentSpeed = GET_ENTITY_SPEED(smugArgs.smugglersVehicles[0])
		fCurrentSpeed = fCurrentSpeed
//		PRINTLN("fCurrentSpeed = ", fCurrentSpeed)
	ENDIF
	
	IF NOT smugArgs.bPickupAIFirst
		IF fDistToObjAI < 200
			IF NOT IS_PED_INJURED(smugArgs.smugglersPeds[0])
				IF IS_PED_IN_VEHICLE(smugArgs.smugglersPeds[0], smugArgs.smugglersVehicles[0])
					SET_DRIVE_TASK_CRUISE_SPEED(smugArgs.smugglersPeds[0], 50.0)
					smugArgs.bCloseToDestination = TRUE
	//				PRINTLN("AI CLOSE TO DESTINATION - INCREASE SPEED!!!!")
				ENDIF
			ENDIF
		ELSE
			smugArgs.bCloseToDestination = FALSE
		ENDIF
	ENDIF
	
	IF bToPickup
		// If we're not too close to the pickup, rubberband AI
		IF NOT smugArgs.bCloseToDestination
			IF fDistToObjPlayer > fDistToObjAI 
				// If the player has a greater distance to the objective, make AI go slower
				IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0]) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[0])
				AND NOT IS_PED_INJURED(smugArgs.smugglersPeds[0])
					IF IS_PED_IN_VEHICLE(smugArgs.smugglersPeds[0], smugArgs.smugglersVehicles[0])
						SET_DRIVE_TASK_CRUISE_SPEED(smugArgs.smugglersPeds[0], 30.0)
//						PRINTLN("GOING SLOWER")
					ENDIF
				ENDIF
			ELSE
				// If the AI has a greater distance to the objective than the player, make AI go faster.
				IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0]) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[0])
				AND NOT IS_PED_INJURED(smugArgs.smugglersPeds[0])
					IF IS_PED_IN_VEHICLE(smugArgs.smugglersPeds[0], smugArgs.smugglersVehicles[0])
						SET_DRIVE_TASK_CRUISE_SPEED(smugArgs.smugglersPeds[0], 50.0)
//						PRINTLN("GOING FASTER")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF fDistToObjPlayer > fDistToObjAI 
			// If the player has a greater distance to the objective, make AI go slower
			IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0]) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[0])
			AND NOT IS_PED_INJURED(smugArgs.smugglersPeds[0])
				IF IS_PED_IN_VEHICLE(smugArgs.smugglersPeds[0], smugArgs.smugglersVehicles[0])
					SET_DRIVE_TASK_CRUISE_SPEED(smugArgs.smugglersPeds[0], 25.0)
//					PRINTLN("GOING SLOWER - AFTER PICKUP")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC ADJUST_PLAYBACK_SPEED(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, VECTOR& vVectorToUse)
	
	INT idx
	VECTOR vPlayerPos 
	VECTOR vAIPos[MAX_NUM_SMUGGLERS_CARS]
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	IF NOT IS_TIMER_STARTED(updateTimer)
		START_TIMER_NOW(updateTimer)
	ENDIF
	
	// ONLY UPDATE EVERY OTHER SECOND
	IF GET_TIMER_IN_SECONDS(updateTimer) > 1
		fDistToObjPlayer = VDIST(vPlayerPos, vVectorToUse)
	
		REPEAT myLocData.iNumSmugVehicles idx
			IF DOES_ENTITY_EXIST(smugArgs.smugglersVehicles[idx]) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[idx])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(smugArgs.smugglersVehicles[idx])
				
					// Grab the position of the Smuggler vehicle
					vAIPos[idx] = GET_ENTITY_COORDS(smugArgs.smugglersVehicles[idx])
					// Get distance to objective
					fDistToObjAI = VDIST(vAIPos[idx], vVectorToUse)
					fDistBetweenPlayerAndAI = VDIST(vAIPos[idx], vPlayerPos)
					
					// ******************************IF THIS RETURNS TRUE, NO LONGER ADJUST SPEED******************************
					IF IS_SMUGGLER_VEHICLES_TIRES_DAMAGED(idx, smugArgs)
						SET_PLAYBACK_SPEED(smugArgs.smugglersVehicles[idx], 0.65)
						DEBUG_PRINT("SETTING PLAYBACK SPEED = 0.65 DUE TO DAMAGE TO TIRES")
						smugArgs.bRunSpeedAdjustment = FALSE
						EXIT
					ENDIF
			
					// Check if the player and smuggler vehicle are close
					IF fDistBetweenPlayerAndAI < 10
						// Check if they are in front of the player
						IF IS_ENTITY_ON_SCREEN(smugArgs.smugglersVehicles[idx])
							// Bool used to check if we're too close... 
							// ...to fix the stuttering of the playback when trying to increase playback speed.
							smugArgs.TooCloseToIncreaseSpeed = TRUE
//							DEBUG_PRINT("smugArgs.TooCloseToIncreaseSpeed = TRUE")
						ELSE
							smugArgs.TooCloseToIncreaseSpeed = FALSE
//							DEBUG_PRINT("smugArgs.TooCloseToIncreaseSpeed = FALSE")
						ENDIF
					ELSE
						smugArgs.TooCloseToIncreaseSpeed = FALSE
//						DEBUG_PRINT("smugArgs.TooCloseToIncreaseSpeed = FALSE")
					ENDIF
					
					// Check if we're close to the destination... if so, speed up the playback.
					IF fDistToObjAI < 150
						SET_PLAYBACK_SPEED(smugArgs.smugglersVehicles[idx], (myLocData.dropData[0].smugglersRecordings[0].fDefaultPlaybackSpeed + 0.5))
//						DEBUG_PRINT("CLOSE TO TARGET, INCREASE SPEED")
						smugArgs.bCloseToDestination = TRUE
					ELSE
						smugArgs.bCloseToDestination = FALSE
					ENDIF
						
					// Do not run check if the player is too close to the smuggler vehicle.
					// Do not run check if the smuggler vehicle is too close to the destination.
					IF NOT smugArgs.TooCloseToIncreaseSpeed AND NOT smugArgs.bCloseToDestination
						// AI has the package
						IF smugArgs.bPickupAIFirst
							AFTER_PICKUP_SPEED_ADJUSTMENT(myLocData, smugArgs, idx)
						ELSE
							// To pickup, no one has the package
							TO_PICKUP_SPEED_ADJUSTMENT(myLocData, smugArgs, idx)
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDREPEAT
	ELIF GET_TIMER_IN_SECONDS(updateTimer) >= 2.0
		RESTART_TIMER_NOW(updateTimer)
	ENDIF
	
ENDPROC

PROC RUN_DIALOGUE_WARNINGS(FLOAT& fDistToDisplay, structPedsForConversation& MyLocalPedStruct)

	SWITCH iDialogueWarning
		CASE 0
			IF NOT IS_MESSAGE_BEING_DISPLAYED() AND NOT IS_HELP_MESSAGE_ON_SCREEN()
				IF fDistToDisplay < 1500
					IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GR10", CONV_PRIORITY_VERY_HIGH)
						PRINTLN("PLAYING CONVERSATION - ARMS_GR10")
						iDialogueWarning++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF fDistToDisplay < 250
				IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GR11", CONV_PRIORITY_VERY_HIGH)
					iDialogueWarning++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			
		BREAK
	ENDSWITCH
ENDPROC

PROC PRINT_RIVAL_DISTANCE_TO_DROP(SMUGGLERS_MODE_ARGS& smugArgs, VECTOR& vVectorToUse, structPedsForConversation& MyLocalPedStruct)
	FLOAT fDistToDisplay
	VECTOR vAIPos

	// PRINT DISTANCE TO DROP SITE
	IF smugArgs.bPickupAIFirst
		
		IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0]) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[0])
			vAIPos = GET_ENTITY_COORDS(smugArgs.smugglersVehicles[0])
			fDistToDisplay = VDIST(vAIPos, vVectorToUse)
		ENDIF

		RUN_DIALOGUE_WARNINGS(fDistToDisplay, MyLocalPedStruct)
	ENDIF

ENDPROC

PROC RETASK_SMUGGLERS(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs)
	INT idx, idx2, iStartIndex
	
	// Loop on number of vehicles
	REPEAT myLocData.iNumSmugVehicles idx
		IF DOES_ENTITY_EXIST(smugArgs.smugglersVehicles[idx]) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[idx])
			// Loop on peds in each vehicle
			REPEAT myLocData.iNumSmugPedsPerVehicle idx2
				iStartIndex = idx*myLocData.iNumSmugPedsPerVehicle
				IF NOT IS_PED_INJURED(smugArgs.smugglersPeds[idx2 + iStartIndex])
					REMOVE_ALL_PED_WEAPONS(smugArgs.smugglersPeds[idx2 + iStartIndex])
					CLEAR_PED_TASKS(smugArgs.smugglersPeds[idx2 + iStartIndex])
					TASK_VEHICLE_DRIVE_WANDER(smugArgs.smugglersPeds[idx2 + iStartIndex], smugArgs.smugglersVehicles[idx], 30.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
					SET_PED_AS_NO_LONGER_NEEDED(smugArgs.smugglersPeds[idx2 + iStartIndex])
					DEBUG_PRINT("TASKING SMUGGLERS TO WANDER")
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
	CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, smugArgs.relSmugglersEnemies)
	CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, smugArgs.relSmugglersEnemies, RELGROUPHASH_PLAYER)	
ENDPROC

PROC SMUGGLERS_HANDLE_PLAYER_LEAVING_VEHICLE(SMUGGLERS_MODE_ARGS& smugArgs, BOOL& bOutOfVehicle, BOOL& bWanted, BOOL& bDrugsLoaded)
	
	UNUSED_PARAMETER(bOutOfVehicle)
	IF bOkayToBlipEnemies
		IF bDrugsLoaded
//			IF bOutOfVehicle
//				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[0])
//					REMOVE_BLIP(smugArgs.blipSmugglersCars[0])
//					DEBUG_PRINT("REMOVING SMUGGLER'S BLIP DUE TO PLAYER BEING OUT OF VEHICLE")
//				ENDIF
//			ELSE
				IF NOT bWanted
					IF DOES_ENTITY_EXIST(smugArgs.smugglersVehicles[0])
						IF NOT DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[0])
							IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[0]) OR NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[1])
								IF IS_PED_IN_VEHICLE(smugArgs.smugglersPeds[0], smugArgs.smugglersVehicles[0]) 
								OR IS_PED_IN_VEHICLE(smugArgs.smugglersPeds[1], smugArgs.smugglersVehicles[0]) 
									smugArgs.blipSmugglersCars[0] = ADD_BLIP_FOR_ENTITY(smugArgs.smugglersVehicles[0])
									SET_BLIP_COLOUR(smugArgs.blipSmugglersCars[0], BLIP_COLOUR_RED)
									SET_BLIP_NAME_FROM_TEXT_FILE(smugArgs.blipSmugglersCars[0], "DTRFKGR_BLIP03")
		//							DEBUG_PRINT("ADDING BACK IN SMUGGLER'S BLIP DUE TO PLAYER BEING BACK IN THEIR VEHICLE")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
//			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SMUGGLERS_HANDLE_PLAYER_GOING_WANTED(SMUGGLERS_MODE_ARGS& smugArgs, SMUGGLERS_STATE& smugglersStage, BOOL& bOutOfVehicle, BOOL& bWanted)
	IF bOkayToBlipEnemies
		IF smugglersStage > SMUGGLERS_STATE_TO_PICKUP AND smugglersStage < SMUGGLERS_STATE_DROPOFF_FIGHT
			IF bWanted
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[0])
					REMOVE_BLIP(smugArgs.blipSmugglersCars[0])
					DEBUG_PRINT("REMOVING SMUGGLER'S BLIP DUE TO BEING WANTED")
				ENDIF
			ELSE
				IF NOT bOutOfVehicle
					IF IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[0])
						IF DOES_ENTITY_EXIST(smugArgs.smugglersVehicles[0])
							IF NOT DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[0])
								IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[0]) AND NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[1])
									smugArgs.blipSmugglersCars[0] = ADD_BLIP_FOR_ENTITY(smugArgs.smugglersVehicles[0])
									SET_BLIP_COLOUR(smugArgs.blipSmugglersCars[0], BLIP_COLOUR_RED)
									SET_BLIP_NAME_FROM_TEXT_FILE(smugArgs.blipSmugglersCars[0], "DTRFKGR_BLIP03")
									DEBUG_PRINT("ADDING BACK IN SMUGGLER'S BLIP DUE TO PLAYER LOSING WANTED LEVEL")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_POST_PICKUP_OBJECTIVE_PRINT(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, VEHICLE_INDEX& myVehicle, 
BLIP_INDEX& myLocationBlip[], TEXT_LABEL_31& sLastObjective, VECTOR& vCurDestObjective[], structPedsForConversation& MyLocalPedStruct)

	// Handles printing of objective, are we in/out of our car, and do we have smuggler's still around.
	IF NOT smugArgs.bPrintObj
		IF IS_TIMER_STARTED(smugArgs.tPrintObjTimer)
			IF GET_TIMER_IN_SECONDS(smugArgs.tPrintObjTimer) > 3.0
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
					IF smugArgs.bSmugglersTooFarAfterPickup OR smugArgs.bSmugglerPedsDeadPriorToPickup
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
							// If we're out of the vehicle, print to go to the airfield
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
								// Print new objective
								PRINT_NOW("DTRFKGR_03",DEFAULT_GOD_TEXT_TIME,1) 
								sLastObjective = "DTRFKGR_03"
								myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.endData[0].vDrugPickup)
								PRINTLN("ADDING IN myLocationBlip[0] VIA SMUGGLERS")
								
								IF DOES_BLIP_EXIST(myLocationBlip[0])
		//								SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
									SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
								ENDIF
								vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
							ELSE
		//						// If we're not, tell the player to go back to their vehicle first.
		//						PRINT_NOW("DTRSHRD_03", DEFAULT_GOD_TEXT_TIME, 1)
		//						sLastObjective = "DTRSHRD_03"
		//						vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
		//						PRINTLN("SMUGGLERS: PLAYER OUT OF THEIR VEHICLE", vCurDestObjective[0])
		//						
		//						IF NOT DOES_BLIP_EXIST(vehBlip)
		//							vehBlip = ADD_BLIP_FOR_ENTITY(myVehicle)
		//							SET_BLIP_COLOUR(vehBlip, BLIP_COLOUR_BLUE)
		//							PRINTLN("SMUGGLERS: ADDING IN VEHICLE BLIP POST PICKUP")
		//						ENDIF
		//						
		//						bSmugglersOutVehicle = TRUE
							ENDIF
						ENDIF
					ELSE
						// Print new objective
						PRINT_NOW("DTRFKGR_04",DEFAULT_GOD_TEXT_TIME,1) 
						sLastObjective = "DTRFKGR_04"
						
						PRINTLN("PRINTING OBJECTIVE TO LOSE THE RIVAL RUNNERS, BECAUSE THEY'RE STILL ACTIVE")
					ENDIF
					
					smugArgs.bPrintObj = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bSmugglersOutVehicle
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, NULL, "TREVOR")
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_PACK", CONV_PRIORITY_VERY_HIGH)
					
					IF NOT IS_TIMER_STARTED(smugArgs.tPrintObjTimer)
						START_TIMER_NOW(smugArgs.tPrintObjTimer)
						PRINTLN("STARTING TIMER - smugArgs.tPrintObjTimer")
					ENDIF
					
					PRINTLN("PLAYING GOT IT LINE - 1")
					bSmugglersOutVehicle = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

//	IF smugArgs.bPrintObj AND bSmugglersOutVehicle
//		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
//			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
//				// Print new objective
//				PRINT_NOW("DTRFKGR_03",DEFAULT_GOD_TEXT_TIME,1) 
//				sLastObjective = "DTRFKGR_03"
//				myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.endData[0].vDrugPickup)
//				PRINTLN("ADDING IN myLocationBlip[0] VIA SMUGGLERS - bSmugglersOutVehicle")
//				
//				IF DOES_BLIP_EXIST(myLocationBlip[0])
////								SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
//					SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
//				ENDIF
//				vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
//				
//				bSmugglersOutVehicle = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC

PROC CREATE_FLARE_PROP(LOCATION_DATA& myLocData)
	IF g_savedGlobals.sTraffickingData.iGroundRank != 1
		SNAP_3D_COORD_TO_GROUND(myLocData.dropData[0].vCenterPos)
		PRINTLN("SNAPPING PROP FLARE TO GROUND")
	ENDIF
	
	oFlareProp = CREATE_OBJECT(Prop_Flare_01b, myLocData.dropData[0].vCenterPos)
ENDPROC

FUNC BOOL CHECK_FOR_PLAYER_AGGRO(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs)
	INT idx, idx2, iStartIndex
	PED_INDEX pedToTask
	
	IF bSmugglersAggroed
//		PRINTLN("WE'VE AGGROED, EXITING EARLY")
		RETURN FALSE 
	ENDIF
	
	IF DO_AGGRO_CHECK(smugArgs.smugglersPeds[0], smugArgs.smugglersVehicles[0], smugglersAggroArgs, eAggroReasons) 
	OR DO_AGGRO_CHECK(smugArgs.smugglersPeds[1], smugArgs.smugglersVehicles[0], smugglersAggroArgs, eAggroReasons)
	OR smugArgs.bPickupPlayerFirst
	OR smugArgs.bShootout
	OR smugArgs.bPickupAIFirst
	
		REPEAT myLocData.iNumSmugPedsPerVehicle idx2
			iStartIndex = idx*myLocData.iNumSmugPedsPerVehicle
			
			IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[idx2 + iStartIndex])
				SET_PED_RELATIONSHIP_GROUP_HASH(smugArgs.smugglersPeds[idx2 + iStartIndex], smugArgs.relSmugglersEnemies)
				
				IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0])
					pedToTask = GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[0], VS_FRONT_RIGHT)
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedToTask) AND NOT IS_ENTITY_DEAD(pedToTask)
					TASK_COMBAT_PED(pedToTask, PLAYER_PED_ID())
					PRINTLN("TASKING SMUGGLER PED TO COMBAT")
				ENDIF
			ENDIF
		ENDREPEAT
		
		bSmugglersAggroed = TRUE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE 
ENDFUNC

PROC SMUGGLERS_HANDLE_PLAYER_FIRST(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, DRUG_CARGO_ARGS& drugCargoArgs, 
VEHICLE_INDEX& myVehicle, BLIP_INDEX& myLocationBlip[], INT idx, BOOL& bDrugsLoaded)
					
	TRIGGER_MUSIC_EVENT("OJDG2_TREV_FIRST")
	PRINTLN("TRIGGERING MUSIC - OJDG2_TREV_FIRST - 01")
	
	CLEAR_THIS_PRINT("DTRFKGR_06")
	
	ADD_DRUGS_TO_VEHICLE(drugCargoArgs, myLocData, myVehicle)
	SMUGGLERS_LOCK_VEHICLE_DOORS(bDrugsLoaded, myVehicle)
	smugArgs.bCarHasDrugs[0] = FALSE
	
	IF DOES_ENTITY_EXIST(smugArgs.oParachute)
		DELETE_OBJECT(smugArgs.oParachute)
		PRINTLN("DELETING PARACHUTE")
	ENDIF
	
	IF DOES_ENTITY_EXIST(smugArgs.oArmsPackage)
		DELETE_OBJECT(smugArgs.oArmsPackage)
		DEBUG_PRINT("DELETING smugArgs.oArmsPackage")
	ENDIF
	
	PAUSE_OR_STOP_RECORDING(myLocData, smugArgs, idx)
	
	IF DOES_ENTITY_EXIST(oFlareProp)
		DELETE_OBJECT(oFlareProp)
		DEBUG_PRINT("DELETING oFlareProp VIA SMUGGLERS PLAYER FIRST")
	ENDIF
	
	IF DOES_BLIP_EXIST(myLocationBlip[0])
		REMOVE_BLIP(myLocationBlip[0])
		DEBUG_PRINT("REMOVING myLocationBlip[0] VIA PLAYER FIRST")
	ENDIF
	bDrugsLoaded = TRUE
	
	STOP_PARTICLE_FX_SAFE(PTFXsmugglersFlare)
	PRINTLN("STOPPING PARTICLE EFFECT - PLAYER FIRST")
	
	IF g_savedGlobals.sTraffickingData.iGroundRank = 1
		CLEAR_GPS_CUSTOM_ROUTE()
	ENDIF
	
	DELETE_SMUGGLER_HELI_PED(smugArgs)
ENDPROC

FUNC BOOL HAS_PLAYER_COLLIDED_WITH_SMUGGLERS_PACKAGE(SMUGGLERS_MODE_ARGS& smugArgs, VEHICLE_INDEX& myVehicle)
	IF DOES_ENTITY_EXIST(smugArgs.oArmsPackage) AND NOT IS_ENTITY_DEAD(smugArgs.oArmsPackage)
		IF NOT IS_ENTITY_DEAD(myVehicle)
			IF IS_ENTITY_TOUCHING_ENTITY(smugArgs.oArmsPackage, myVehicle)
				PRINTLN("SMUGGLERS - PLAYER VEHICLE HAS COLLIDED WITH PACKAGE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_INSIGHT_CONVO(SMUGGLERS_MODE_ARGS& smugArgs, structPedsForConversation& MyLocalPedStruct)
	VECTOR vPlayerPosition, vSmuggleVehiclePosition
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0])
		vSmuggleVehiclePosition = GET_ENTITY_COORDS(smugArgs.smugglersVehicles[0])
	ENDIF
	
	IF NOT bPlayedSightsConvo
		IF VDIST2(vPlayerPosition, vSmuggleVehiclePosition) < 1225 // 35m
		AND IS_ENTITY_ON_SCREEN(smugArgs.smugglersVehicles[0])
			IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_SIGHT", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
				PRINTLN("bPlayedSightsConvo = TRUE")
				bPlayedSightsConvo = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL UPDATE_SMUGGLERS_MODE(LOCATION_DATA& myLocData, SMUGGLERS_MODE_ARGS& smugArgs, SMUGGLERS_STATE& smugglersStage, 
VECTOR& vCurDestObjective[], BLIP_INDEX& myLocationBlip[], TEXT_LABEL_31& sLastObjective, BOOL& bDrugsLoaded, BOOL& bIsSmugglersActive,
SMUGGLERS_RETRIEVE_DRUGS& smugglerRetrieveState, VEHICLE_INDEX& myVehicle, CustomCutsceneCaller fpSmugglersEnding, 
CUTSCENE_ARGS& cutArgs, CAMERA_INDEX& myCamera, DRUG_CARGO_ARGS& drugCargoArgs, 
SMUGGLER_PLANE_STATE& smugglerPlaneRecordingStages, structPedsForConversation& MyLocalPedStruct, CustomCutsceneCaller fpSmugglersPickupCutscene, BOOL& bOkayToRunTrap, BOOL& bOutOfVehicle,
BOOL& bWanted, SMUGGLERS_CREATE_HELI_VEHICLE& smugglersCreateHeliPedStages, INT& outOfVehicleTime)//, BOOL& bDoSmugglers)
	
	SEQUENCE_INDEX iSeq, iSeqTask
	PED_INDEX pedDriver, pedTemp, tempDriver
	INT idx, idx2, iStartIndex, iRandInt, iRandConvo
	VECTOR vClosestNode
	VECTOR vPlayerLocation
	fpSmugglersPickupCutscene = fpSmugglersPickupCutscene
	
	// Run stuck check on all smuggler vehicles
//	IF smugglersStage < SMUGGLERS_STATE_DROPOFF_DELIVERY AND NOT smugArgs.bSmugglerPedsDead 
//		REPEAT myLocData.iNumSmugVehicles idx
//			IF DOES_ENTITY_EXIST(smugArgs.smugglersVehicles[idx])
//				TELEPORT_STUCK_AI(smugArgs.smugglersVehicles[idx])
//				ADJUST_ENEMY_VEHICLE_SPEED(smugArgs.smugglersVehicles[idx], 50.0)
//			ENDIF
//		ENDREPEAT
//	ENDIF
	
	SMUGGLERS_HANDLE_PLAYER_LEAVING_VEHICLE(smugArgs, bOutOfVehicle, bWanted, bDrugsLoaded)
	
	// To Fix Bug #294784
	IF smugArgs.bNeedToPrintObj
		IF smugArgs.bPickupAIFirst
			IF NOT bOutOfVehicle
				PRINT_NOW("DTRFKGR_09", 3000, 1)
				smugArgs.bNeedToPrintObj = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	SMUGGLERS_HANDLE_PLAYER_GOING_WANTED(smugArgs, smugglersStage, bOutOfVehicle, bWanted)
	
	CHECK_FOR_PLAYER_AGGRO(myLocData, smugArgs)
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.05)
	
	SWITCH smugglersStage
		CASE  SMUGGLERS_STATE_TO_PICKUP
			REPEAT myLocData.iNumSmugVehicles idx
				IF IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[idx])
					// Add blip on car
//					smugArgs.blipSmugglersCars[idx] = ADD_BLIP_FOR_ENTITY(smugArgs.smugglersVehicles[idx])
//					SET_BLIP_COLOUR(smugArgs.blipSmugglersCars[idx], BLIP_COLOUR_RED)
//					SET_BLIP_NAME_FROM_TEXT_FILE(smugArgs.blipSmugglersCars[idx], "DTRFKGR_BLIP03")
//					DEBUG_PRINT("ADDING IN SMUGGLERS BLIPS")
				
					IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx])
						pedDriver = GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx])
						pedTemp = GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx], VS_FRONT_RIGHT)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTemp, FALSE)
					ENDIF
					
//					FLOAT fMaxSpeedToUse
//					IF g_savedGlobals.sTraffickingData.iGroundRank = 1
//						fMaxSpeedToUse = -1
//						PRINTLN("fMaxSpeedToUse = ", fMaxSpeedToUse)
//					ELSE
//						fMaxSpeedToUse = -1
//						PRINTLN("fMaxSpeedToUse = ", fMaxSpeedToUse)
//					ENDIF
					
					IF myLocData.dropData[0].smugglersRecordings[idx].bConfigured
						SET_ENTITY_COORDS(smugArgs.smugglersVehicles[idx], myLocData.dropData[0].smugglersRecordings[idx].carCoords)
						SET_ENTITY_QUATERNION(smugArgs.smugglersVehicles[idx], myLocData.dropData[0].smugglersRecordings[idx].x,  myLocData.dropData[0].smugglersRecordings[idx].y,  myLocData.dropData[0].smugglersRecordings[idx].j,  myLocData.dropData[0].smugglersRecordings[idx].k)
					
						START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(smugArgs.smugglersVehicles[idx], myLocData.dropData[0].smugglersRecordings[idx].carrecNum, myLocData.dropData[0].smugglersRecordings[idx].carrecName, ENUM_TO_INT(SWITCH_ON_PLAYER_VEHICLE_IMPACT), 1000)
						DEBUG_PRINT("STARTING PLAYBACK ON SMUGGLERS VEHICLE")
						
					// If we're using WAYPOINT recording
					ELIF myLocData.dropData[0].smugglersRecordings[idx].bWaypointConfigured
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedDriver, smugArgs.smugglersVehicles[idx],
						myLocData.dropData[0].smugglersRecordings[idx].carrecWaypointName, DRIVINGMODE_AVOIDCARS, 0, 
						EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, -1)
						PRINTLN("USING WAYPOINT RECORDING")
					// Task vehicle
					ELSE
						OPEN_SEQUENCE_TASK(iSeq)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, smugArgs.smugglersVehicles[idx], 
							vCurDestObjective[0], 40.0, DRIVINGSTYLE_RACING, myLocData.mnSmugglersVehicles[idx], 
							DRIVINGMODE_AVOIDCARS, 2.0, -1)
						CLOSE_SEQUENCE_TASK(iSeq)
						IF NOT IS_PED_INJURED(smugArgs.smugglersPeds[idx])
							TASK_PERFORM_SEQUENCE(smugArgs.smugglersPeds[idx], iSeq)
						ENDIF
						CLEAR_SEQUENCE_TASK(iSeq)
					ENDIF
				ENDIF
			ENDREPEAT
			
			vCurDestObjective[0] = myLocData.dropData[0].vCenterPos
			
			// Create particle effect right at center position
			PTFXsmugglersFlare = START_PARTICLE_FX_LOOPED_AT_COORD("scr_drug_traffic_flare_L", vCurDestObjective[0], <<0,0,0>>, 1.0)
			SET_PARTICLE_FX_LOOPED_COLOUR(PTFXsmugglersFlare, 1.0, 0.84, 0.0)
			PRINTLN("STARTING PARTICLE EFFECT")
			
			SNAP_3D_COORD_TO_GROUND(vCurDestObjective[0])
			
			IF g_savedGlobals.sTraffickingData.iGroundRank = 3
				vRangeToUse = <<3.5,3.5,3.5>>
				PRINTLN("vRangeToUse = ", vRangeToUse)
			ELSE
				vRangeToUse = <<6.5,6.5,6.5>>
				PRINTLN("vRangeToUse = ", vRangeToUse)
			ENDIF
				
			// Set bool to run speed adjustment
			smugArgs.bRunSpeedAdjustment = TRUE
			
			DEBUG_PRINT("INSIDE STATE - SMUGGLERS_STATE_TO_PICKUP")
			smugglersStage = SMUGGLERS_STATE_PICKUP_CHECK
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_STATE_PICKUP_CHECK
			
			// Check for both peds dead in a vehicle
			IF SMUGGLER_PED_DEAD_CHECK(myLocData, smugArgs, MyLocalPedStruct)
				smugArgs.bSmugglerPedsDeadPriorToPickup = TRUE
				
				IF NOT bTriggeredMusicEvent01
					TRIGGER_MUSIC_EVENT("OJDG2_FIRST_ENEMIES_DEAD")
					PRINTLN("TRIGGERING MUSIC - OJDG2_FIRST_ENEMIES_DEAD - 01")
					
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					
					bTriggeredMusicEvent01 = TRUE
				ENDIF
				
//				PRINTLN("smugArgs.bSmugglerPedsDeadPriorToPickup = TRUE")
			ENDIF
			
			// Adjust the recording playback speed
			IF myLocData.dropData[0].smugglersRecordings[idx].bWaypointConfigured
				// Run speed adjustment on waypoint recordings
				SMUGGLERS_WAYPOINT_RUBBERBAND(myLocData, smugArgs, myVehicle, vCurDestObjective[0])
			ELSE
				IF smugArgs.bRunSpeedAdjustment
					ADJUST_PLAYBACK_SPEED(myLocData, smugArgs, vCurDestObjective[0])
				ENDIF
			ENDIF
			
			IF NOT bCreateFlare
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCurDestObjective[0], <<100,100,100>>)
					CREATE_FLARE_PROP(myLocData)
					PRINTLN("CREATING FLARE")
					bCreateFlare = TRUE
				ENDIF
			ENDIF
			
			HANDLE_INSIGHT_CONVO(smugArgs, MyLocalPedStruct)
			
			// Update plane recording
			UPDATE_SMUGGLER_PLANE_RECORDING(myLocData, smugArgs, smugglerPlaneRecordingStages, myLocationBlip)
						
			REPEAT myLocData.iNumSmugVehicles idx
				
				// This is set in the main update loop... after initial conversation from Oscar is done.
				IF bOkayToBlipEnemies AND NOT bDoneBlipingEnemies
					IF NOT DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[idx])
						smugArgs.blipSmugglersCars[idx] = ADD_BLIP_FOR_ENTITY(smugArgs.smugglersVehicles[idx])
						SET_BLIP_COLOUR(smugArgs.blipSmugglersCars[idx], BLIP_COLOUR_RED)
						SET_BLIP_NAME_FROM_TEXT_FILE(smugArgs.blipSmugglersCars[idx], "DTRFKGR_BLIP03")
						DEBUG_PRINT("ADDING IN SMUGGLERS BLIPS NEW")
						bDoneBlipingEnemies = TRUE
					ENDIF
				ENDIF
			
				IF IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[idx]) OR smugArgs.bSmugglerPedsDeadPriorToPickup
					
					IF IS_ENTITY_AT_COORD(smugArgs.smugglersVehicles[idx], vCurDestObjective[0], <<50,50,50>>)
						IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx])
							tempDriver = GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx])
							
							IF tempDriver != PLAYER_PED_ID()
								IF DOES_ENTITY_EXIST(tempDriver) AND NOT IS_ENTITY_DEAD(tempDriver)
									SET_DRIVE_TASK_DRIVING_STYLE(tempDriver, ENUM_TO_INT(DRIVINGMODE_PLOUGHTHROUGH))
	//								PRINTLN("SMUGGLER DRIVER SHOULD BE USING DRIVINGMODE_PLOUGHTHROUGH")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx])
							tempDriver = GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx])
							
							IF tempDriver != PLAYER_PED_ID()
								IF DOES_ENTITY_EXIST(tempDriver) AND NOT IS_ENTITY_DEAD(tempDriver)
									SET_DRIVE_TASK_DRIVING_STYLE(tempDriver, ENUM_TO_INT(DRIVINGMODE_AVOIDCARS))
	//								PRINTLN("SMUGGLER DRIVER SHOULD BE USING DRIVINGMODE_AVOIDCARS")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					// ----------------------------------------AI FIRST----------------------------------------
					IF IS_ENTITY_AT_COORD(smugArgs.smugglersVehicles[idx], vCurDestObjective[0], vRangeToUse)
					AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCurDestObjective[0], vPickupDimension)
					AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), smugArgs.smugglersVehicles[idx])
						DEBUG_PRINT("SETTING smugArgs.bPickupAIFirst TO TRUE")
						smugArgs.bPickupAIFirst = TRUE
						
						TRIGGER_MUSIC_EVENT("OJDG2_PACKAGE_STOLEN")
						PRINTLN("TRIGGERING MUSIC - OJDG2_PACKAGE_STOLEN")
						
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(smugArgs.smugglersVehicles[idx])
//							SET_PLAYBACK_SPEED(smugArgs.smugglersVehicles[idx], 0.90)
//							DEBUG_PRINT("SETTING PLAYBACK SPEED TO 0.90")
//						ENDIF
						
						ADD_DRUGS_TO_VEHICLE(drugCargoArgs, myLocData, smugArgs.smugglersVehicles[idx], FALSE)
						
						IF DOES_ENTITY_EXIST(smugArgs.oArmsPackage)
							DELETE_OBJECT(smugArgs.oArmsPackage)
							DEBUG_PRINT("DELETING smugArgs.oArmsPackage")
						ENDIF
						
						IF DOES_ENTITY_EXIST(oFlareProp)
							DELETE_OBJECT(oFlareProp)
							DEBUG_PRINT("DELETING oFlareProp VIA SMUGGLERS AI FIRST")
						ENDIF
						
						// Setting bIsSmugglersActive to TRUE, so the ABANDON check will not run.
						bIsSmugglersActive = TRUE
						
						// If the AI makes it there first, remove the pickup blip
						IF DOES_BLIP_EXIST(myLocationBlip[0])
							REMOVE_BLIP(myLocationBlip[0])
							DEBUG_PRINT("REMOVING myLocationBlip[0] VIA AI FIRST")
						ENDIF
						
						// Print the message, "~s~Retrieve the drugs by taking out the competing ~r~drug runners.~s~"
						// To Fix Bug #294784
						IF bOutOfVehicle
							smugArgs.bNeedToPrintObj = TRUE
							PRINTLN("SETTING smugArgs.bNeedToPrintObj = TRUE")
						ELSE
							PRINT_NOW("DTRFKGR_09", 3000, 1)
						ENDIF
						
						smugArgs.bCarHasDrugs[idx] = TRUE
						
						// Store off ped and vehicle that got the drugs.
						IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx])
							smugArgs.pedSmugglerWithDrugs = GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx])
						ENDIF
						IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(smugArgs.pedSmugglerWithDrugs))
							smugArgs.vehSmugglerCarWithDrugs = GET_VEHICLE_PED_IS_IN(smugArgs.pedSmugglerWithDrugs)
						ENDIF
						
//						// Set new destination objective if the player exits their vehicle.
						vCurDestObjective[0] = <<0,0,0>>
						
						IF smugArgs.bCarHasDrugs[0]
							DEBUG_PRINT("smugArgs.bCarHasDrugs[0] IS TRUE")
						ENDIF
						
						SET_MAX_WANTED_LEVEL(0)
						DEBUG_PRINT("SETTING MAX WANTED LEVEL BACK TO ZERO")
						
						STOP_PARTICLE_FX_SAFE(PTFXsmugglersFlare)
						PRINTLN("STOPPING PARTICLE EFFECT - AI FIRST")
						
						tempDriver = GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[idx])
							
						IF DOES_ENTITY_EXIST(tempDriver) AND NOT IS_ENTITY_DEAD(tempDriver)
							SET_DRIVE_TASK_DRIVING_STYLE(tempDriver, ENUM_TO_INT(DRIVINGMODE_AVOIDCARS))
							PRINTLN("SMUGGLER DRIVER SHOULD BE USING DRIVINGMODE_AVOIDCARS")
						ENDIF
						
						DEBUG_PRINT("GOING TO SMUGGLERS_STATE_PICKUP_COMPLETE VIA AI GETTING TO PICKUP FIRST.")
						smugglersStage = SMUGGLERS_STATE_PICKUP_COMPLETE
					ENDIF
					// ----------------------------------------PLAYER FIRST----------------------------------------
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCurDestObjective[0], vPickupDimension)
					AND NOT IS_ENTITY_AT_COORD(smugArgs.smugglersVehicles[idx], vCurDestObjective[0], vPickupDimension)
					OR (DOES_ENTITY_EXIST(smugArgs.oArmsPackage) AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(smugArgs.oArmsPackage), vPickupDimension, FALSE))
					OR HAS_PLAYER_COLLIDED_WITH_SMUGGLERS_PACKAGE(smugArgs, myVehicle)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_PLAYER_IN_VALID_VEHICLE(myVehicle)
								DEBUG_PRINT("SETTING smugArgs.bPickupPlayerFirst TO TRUE")
								smugArgs.bPickupPlayerFirst = TRUE
								
								SMUGGLERS_HANDLE_PLAYER_FIRST(myLocData, smugArgs, drugCargoArgs, myVehicle, myLocationBlip, idx, bDrugsLoaded)
								
								DEBUG_PRINT("GOING TO SMUGGLERS_STATE_PICKUP_CUTSCENE VIA PLAYER GETTING TO PICKUP FIRST, IN VEHICLE.")
								smugglersStage = SMUGGLERS_STATE_PICKUP_CUTSCENE
							ENDIF
						ELSE
							DEBUG_PRINT("SETTING smugArgs.bPickupPlayerFirst TO TRUE")
							smugArgs.bPickupPlayerFirst = TRUE
							
							SMUGGLERS_HANDLE_PLAYER_FIRST(myLocData, smugArgs, drugCargoArgs, myVehicle, myLocationBlip, idx, bDrugsLoaded)
							
							DEBUG_PRINT("GOING TO SMUGGLERS_STATE_PICKUP_CUTSCENE VIA PLAYER GETTING TO PICKUP FIRST, ON FOOT.")
							smugglersStage = SMUGGLERS_STATE_PICKUP_CUTSCENE
						ENDIF
					ENDIF
					// ----------------------------------------TIE----------------------------------------
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCurDestObjective[0], vPickupDimension)
					AND IS_ENTITY_AT_COORD(smugArgs.smugglersVehicles[idx], vCurDestObjective[0], vPickupDimension)
						DEBUG_PRINT("SETTING smugArgs.bPickupTie TO TRUE")
						smugArgs.bPickupTie = TRUE
						
						TRIGGER_MUSIC_EVENT("OJDG2_TREV_FIRST")
						PRINTLN("TRIGGERING MUSIC - OJDG2_TREV_FIRST - 02")
							
						PAUSE_OR_STOP_RECORDING(myLocData, smugArgs, idx)
						
						ADD_DRUGS_TO_VEHICLE(drugCargoArgs, myLocData, myVehicle)
						smugArgs.bCarHasDrugs[0] = FALSE
						
						SMUGGLERS_LOCK_VEHICLE_DOORS(bDrugsLoaded, myVehicle)
						
						IF DOES_ENTITY_EXIST(smugArgs.oArmsPackage)
							DELETE_OBJECT(smugArgs.oArmsPackage)
							DEBUG_PRINT("DELETING smugArgs.oArmsPackage")
						ENDIF
						
						IF DOES_ENTITY_EXIST(oFlareProp)
							DELETE_OBJECT(oFlareProp)
							DEBUG_PRINT("DELETING oFlareProp VIA SMUGGLERS TIE")
						ENDIF
						
						IF DOES_BLIP_EXIST(myLocationBlip[0])
							REMOVE_BLIP(myLocationBlip[0])
							DEBUG_PRINT("REMOVING myLocationBlip[0] VIA TIE")
						ENDIF
						bDrugsLoaded = TRUE
						
						STOP_PARTICLE_FX_SAFE(PTFXsmugglersFlare)
						PRINTLN("STOPPING PARTICLE EFFECT - TIE")
						
						IF g_savedGlobals.sTraffickingData.iGroundRank = 1
							CLEAR_GPS_CUSTOM_ROUTE()
						ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = GET_PLAYER_PED_MODEL(CHAR_TREVOR)
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, NULL, "TREVOR")
								CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_PACK", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING GOT IT LINE - 2")
								IF NOT IS_TIMER_STARTED(smugArgs.tPrintObjTimer)
									START_TIMER_NOW(smugArgs.tPrintObjTimer)
									PRINTLN("STARTING tPrintObjTimer - 2")
								ENDIF
							ENDIF
						ENDIF
						
						DELETE_SMUGGLER_HELI_PED(smugArgs)
						
						DEBUG_PRINT("GOING TO SMUGGLERS_STATE_PICKUP_CUTSCENE VIA TIE.")
						smugglersStage = SMUGGLERS_STATE_PICKUP_CUTSCENE
					ENDIF
				ELSE
					// If the smugglers vehicle becomes undriveable remove their blip.
					IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[idx])
						REMOVE_BLIP(smugArgs.blipSmugglersCars[idx])
						DEBUG_PRINT("REMOVING BLIPS BECAUSE THE SMUGGLER VEHICLE IS UNDRIVEABLE")
					ENDIF
					
					// If the smugglers vehicle becomes undriveable, stop the recording and blow them up!
					IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx])
						IF myLocData.dropData[0].smugglersRecordings[idx].bWaypointConfigured
							IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(smugArgs.smugglersVehicles[idx])
								VEHICLE_WAYPOINT_PLAYBACK_PAUSE(smugArgs.smugglersVehicles[idx])
								PRINTLN("PAUSING WAYPOINT RECORDING")
								EXPLODE_VEHICLE(smugArgs.smugglersVehicles[idx])
								DEBUG_PRINT("STOPPING PLAYBACK RECORDING DUE TO VEHICLE BEING UNDRIVEABLE 01")
							ENDIF
						ELSE
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(smugArgs.smugglersVehicles[idx])
								STOP_PLAYBACK_RECORDED_VEHICLE(smugArgs.smugglersVehicles[idx])
								EXPLODE_VEHICLE(smugArgs.smugglersVehicles[idx])
								DEBUG_PRINT("STOPPING PLAYBACK RECORDING DUE TO VEHICLE BEING UNDRIVEABLE 02")
							ENDIF
						ENDIF
					ENDIF
					
					// Still need to check if the player reached the coordinate.
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCurDestObjective[0], g_vAnyMeansLocate)
						DEBUG_PRINT("SETTING smugArgs.bPickupPlayerFirst TO TRUE DUE TO SMUGGLER VEHICLE BEING UNDRIVEABLE")
						smugArgs.bPickupPlayerFirst = TRUE
						
						TRIGGER_MUSIC_EVENT("OJDG2_PACKAGE_OBTAINED")
						PRINTLN("TRIGGERING MUSIC - OJDG2_PACKAGE_OBTAINED")
						
						IF DOES_BLIP_EXIST(myLocationBlip[0])
							REMOVE_BLIP(myLocationBlip[0])
							DEBUG_PRINT("REMOVING myLocationBlip[0] VIA PLAYER FIRST DUE TO SMUGGLERS VEHICLE BEING UNDRIVEABLE")
						ENDIF
						bDrugsLoaded = TRUE
						
						STOP_PARTICLE_FX_SAFE(PTFXsmugglersFlare)
						PRINTLN("STOPPING PARTICLE EFFECT")
						
						IF g_savedGlobals.sTraffickingData.iGroundRank = 1
							CLEAR_GPS_CUSTOM_ROUTE()
						ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = GET_PLAYER_PED_MODEL(CHAR_TREVOR)
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, NULL, "TREVOR")
								CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_PACK", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING GOT IT LINE - 3")
								IF NOT IS_TIMER_STARTED(smugArgs.tPrintObjTimer)
									START_TIMER_NOW(smugArgs.tPrintObjTimer)
									PRINTLN("STARTING tPrintObjTimer - 3")
								ENDIF
							ENDIF
						ENDIF
						
						DEBUG_PRINT("GOING TO SMUGGLERS_STATE_PICKUP_CUTSCENE VIA PLAYER GETTING TO PICKUP FIRST - SMUGGLERS BEING UNDRIVEABLE.")
						smugglersStage = SMUGGLERS_STATE_PICKUP_CUTSCENE
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_STATE_PICKUP_CUTSCENE
//			IF DO_CUTSCENE_CUSTOM(cutArgs.cutsceneState, cutArgs.cutSceneTimer, myLocData.dropData[0].vCutscenePos4, myLocData.dropData[0].vCutsceneOrien4, myCamera, "", fpSmugglersPickupCutscene, FALSE, FALSE, FALSE)
			
				REPEAT (myLocData.iNumSmugPedsPerVehicle * myLocData.iNumSmugVehicles) idx
					IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[idx]) AND NOT IS_PED_INJURED(smugArgs.smugglersPeds[idx])  
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), smugArgs.smugglersPeds[idx]) > DISTANCE_TO_LOSE_SMUGGLERS 
							smugArgs.bSmugglersTooFarAfterPickup = TRUE
							bSmugglersLost = TRUE
							
							IF NOT bPlayedLostSmugglersLine
								iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
								
								IF iRandConvo = 0
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP02", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ELIF iRandConvo = 1
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP03", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ELIF iRandConvo = 2
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP04", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ELIF iRandConvo = 3
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP05", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ELIF iRandConvo = 4
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP06", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ELIF iRandConvo = 5
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP07", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				// Add drop off blip
				IF smugArgs.bSmugglersTooFarAfterPickup OR smugArgs.bSmugglerPedsDeadPriorToPickup
					IF smugArgs.bSmugglersTooFarAfterPickup
						DEBUG_PRINT("SMUGGLERS ARE TOO FAR AFTER PICKUP - BLIPPING AIRFIELD")
					ELIF smugArgs.bSmugglerPedsDeadPriorToPickup
						DEBUG_PRINT("SMUGGLERS ARE DEAD PRIOR TO PICKUP - BLIPPING AIRFIELD")
					ENDIF
				ELSE
					IF NOT IS_TIMER_STARTED(smugArgs.tTrapStartTimer)
						START_TIMER_NOW(smugArgs.tTrapStartTimer)
						DEBUG_PRINT("STARTING smugArgs.tTrapStartTimer")
					ENDIF
					
					bOkayToRunTrap = TRUE
					
					vCurDestObjective[0] = <<0,0,0>>
					
					bIsSmugglersActive = TRUE
					PRINTLN("bIsSmugglersActive = TRUE BECAUSE THE SMUGGLER VEHICLE IS STILL AROUND")
			
					// TELL THEM TO LOSE THE SMUGGLERS CAR
				ENDIF
				
				// Reseting cutscene state
				cutArgs.cutsceneState = CUTSCENE_STATE_START
				
				SMUGGLERS_LOCK_VEHICLE_DOORS(bDrugsLoaded, myVehicle)
				
				DEBUG_PRINT("INSIDE STATE - SMUGGLERS_STATE_PICKUP_CUTSCENE")
				smugglersStage = SMUGGLERS_STATE_PICKUP_COMPLETE
//			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_STATE_PICKUP_COMPLETE
			IF NOT smugArgs.bSmugglerPedsDeadPriorToPickup AND NOT smugArgs.bSmugglersTooFarAfterPickup
				REPEAT myLocData.iNumSmugVehicles idx
					IF IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[idx])
					
						// If the AI made it to the pickup first, task them to immediately go to the dropoff location
						IF smugArgs.bPickupAIFirst
//							IF NOT myLocData.dropData[0].smugglersRecordings[idx].bConfigured
//								// Task vehicle
//								OPEN_SEQUENCE_TASK(iSeq)
//									TASK_VEHICLE_DRIVE_TO_COORD(NULL, smugArgs.smugglersVehicles[idx], 
//									myLocData.endData[1].vCenterPos, 40.0, DRIVINGSTYLE_RACING, myLocData.mnSmugglersVehicles[idx], 
//									DRIVINGMODE_AVOIDCARS, 1.0, -1)
//								CLOSE_SEQUENCE_TASK(iSeq)
//								IF NOT IS_PED_INJURED(smugArgs.smugglersPeds[idx])
//									TASK_PERFORM_SEQUENCE(smugArgs.smugglersPeds[idx], iSeq)
//								ENDIF
//								CLEAR_SEQUENCE_TASK(iSeq)
//							ENDIF
							
							DEBUG_PRINT("bPickupAIFirst IS TRUE - ATTEMPTING TO RE-TASK")
							smugglersStage = SMUGGLERS_STATE_TO_DROPOFF
							
						// If the player made it to the pickup first, immediately task the AI to attack the player... to get the drugs.
						ELIF smugArgs.bPickupPlayerFirst OR smugArgs.bPickupTie
						
							REPEAT myLocData.iNumSmugPedsPerVehicle idx2
								iStartIndex = idx*myLocData.iNumSmugPedsPerVehicle
								IF NOT IS_PED_INJURED(smugArgs.smugglersPeds[idx2 + iStartIndex])
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(smugArgs.smugglersPeds[idx2 + iStartIndex], TRUE)
									SET_PED_COMBAT_ABILITY(smugArgs.smugglersPeds[idx2 + iStartIndex], CAL_AVERAGE)
									SET_PED_COMBAT_RANGE(smugArgs.smugglersPeds[idx2 + iStartIndex], CR_NEAR)
									SET_PED_COMBAT_ATTRIBUTES(smugArgs.smugglersPeds[idx2 + iStartIndex], CA_DO_DRIVEBYS, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(smugArgs.smugglersPeds[idx2 + iStartIndex], CA_CAN_CHARGE, TRUE)
									
									// New flags to address Bug # 1404711
									SET_PED_COMBAT_ATTRIBUTES(smugArgs.smugglersPeds[idx2 + iStartIndex], CA_PREFER_NAVMESH_DURING_VEHICLE_CHASE, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(smugArgs.smugglersPeds[idx2 + iStartIndex], CA_ALLOWED_TO_AVOID_OFFROAD_DURING_VEHICLE_CHASE, TRUE)
									
									SET_PED_COMBAT_MOVEMENT(smugArgs.smugglersPeds[idx2 + iStartIndex], CM_WILLADVANCE)
									SET_PED_COMBAT_ATTRIBUTES(smugArgs.smugglersPeds[idx2 + iStartIndex], CA_USE_VEHICLE, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(smugArgs.smugglersPeds[idx2 + iStartIndex], CA_LEAVE_VEHICLES, FALSE)
									SET_PED_CONFIG_FLAG(smugArgs.smugglersPeds[idx2 + iStartIndex], PCF_AllowToBeTargetedInAVehicle, TRUE)
									TASK_COMBAT_PED(smugArgs.smugglersPeds[idx2 + iStartIndex], PLAYER_PED_ID())
									
									PRINTLN("TASKING SMUGGLER PED TO COMBAT: ", idx2 + iStartIndex)
								ENDIF
							ENDREPEAT
							
							DEBUG_PRINT("bPickupPlayerFirst IS TRUE - ATTEMPTING TO RE-TASK")
							DEBUG_PRINT("GOING TO STATE - SMUGGLERS_STATE_TO_DROPOFF")
							smugglersStage = SMUGGLERS_STATE_TO_DROPOFF
							BREAK
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				DEBUG_PRINT("smugArgs.bGoToDestination = TRUE BECAUSE smugArgs.bSmugglerPedsDeadPriorToPickup IS TRUE")
				smugArgs.bGoToDestination = TRUE
				DEBUG_PRINT("smugArgs.bSmugglerPedsDeadPriorToPickup IS TRUE, GOING TO STATE - SMUGGLERS_STATE_COMPLETE")
				smugglersStage = SMUGGLERS_STATE_COMPLETE
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_STATE_TO_DROPOFF
			
			IF smugArgs.bPickupAIFirst
				UPDATE_HELI_PED(myLocData, smugArgs, smugglersCreateHeliPedStages)
				
				// Oscar gets on Trevor for letting them get the package first.
				IF NOT bPlayedOscarLine
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						iRandInt = GET_RANDOM_INT_IN_RANGE() % 6
						IF iRandInt = 0
							IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_EN_GOT", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING OSCAR LINE: ", iRandInt)
								bPlayedOscarLine = TRUE
							ENDIF
						ELIF iRandInt = 1
							IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_EN_GOT2", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING OSCAR LINE: ", iRandInt)
								bPlayedOscarLine = TRUE
							ENDIF
						ELIF iRandInt = 2
							IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_EN_GOT3", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING OSCAR LINE: ", iRandInt)
								bPlayedOscarLine = TRUE
							ENDIF
						ELIF iRandInt = 3
							IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_EN_GOT4", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING OSCAR LINE: ", iRandInt)
								bPlayedOscarLine = TRUE
							ENDIF
						ELIF iRandInt = 4
							IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_EN_GOT5", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING OSCAR LINE: ", iRandInt)
								bPlayedOscarLine = TRUE
							ENDIF
						ELIF iRandInt = 5
							IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_EN_GOT6", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING OSCAR LINE: ", iRandInt)
								bPlayedOscarLine = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF smugArgs.bPickupPlayerFirst
				HANDLE_POST_PICKUP_OBJECTIVE_PRINT(myLocData, smugArgs, myVehicle, myLocationBlip, sLastObjective, vCurDestObjective, MyLocalPedStruct)
			ENDIF
			
			// If the player got to the drugs first... remove the AI if they become too far away during the chase.
			IF smugArgs.bPickupPlayerFirst OR smugArgs.bPickupTie
				REPEAT (myLocData.iNumSmugPedsPerVehicle * myLocData.iNumSmugVehicles) idx
					IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[idx]) AND NOT IS_PED_INJURED(smugArgs.smugglersPeds[idx])  
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), smugArgs.smugglersPeds[idx]) > DISTANCE_TO_LOSE_SMUGGLERS 
							
							IF NOT bTriggeredMusicEvent01
								TRIGGER_MUSIC_EVENT("OJDG2_FIRST_ENEMIES_DEAD")
								PRINTLN("TRIGGERING MUSIC - OJDG2_FIRST_ENEMIES_DEAD - 02")
								bTriggeredMusicEvent01 = TRUE
							ENDIF
							
							bSmugglersLost = TRUE
								
							IF NOT bPlayedLostSmugglersLine
								iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
								
								IF iRandConvo = 0
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP02", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ELIF iRandConvo = 1
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP03", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ELIF iRandConvo = 2
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP04", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ELIF iRandConvo = 3
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP05", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ELIF iRandConvo = 4
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP06", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ELIF iRandConvo = 5
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP07", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bSmugglersLost = TRUE")
										bPlayedLostSmugglersLine = TRUE
									ENDIF
								ENDIF	
							ENDIF
							
							RETASK_SMUGGLERS(myLocData, smugArgs)
							DEBUG_PRINT("PLAYER GOT THE DRUGS FIRST AND THE SMUGGLERS ARE TOO FAR AWAY")
							smugglersStage = SMUGGLERS_STATE_COMPLETE
							BREAK
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		
			// Print Help - telling the player about retrieving the drugs before they reach the dropoff spot.
			IF NOT smugArgs.bPrintHelpRetrieve 
				IF smugArgs.bPickupAIFirst AND NOT IS_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("DTRFKGR_HELP_04")
					PRINTLN("RESETING VEHICLE TOP SPEED")
					// ************************Reseting speed modifier************************
					IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0])
						MODIFY_VEHICLE_TOP_SPEED(smugArgs.smugglersVehicles[0], 1.0)
					ENDIF
					smugArgs.bPrintHelpRetrieve = TRUE
				ENDIF
			ENDIF
		
			IF smugArgs.bPickupAIFirst AND NOT smugArgs.bSmugglerPedsDead
				PRINT_RIVAL_DISTANCE_TO_DROP(smugArgs, myLocData.endData[1].vCenterPos, MyLocalPedStruct)
			ENDIF
			
			// Adjust the recording playback speed
			IF myLocData.dropData[0].smugglersRecordings[0].bWaypointConfigured
				IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0])
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(smugArgs.smugglersVehicles[0])
						// Run speed adjustment on waypoint recordings
						SMUGGLERS_WAYPOINT_RUBBERBAND(myLocData, smugArgs, myVehicle, myLocData.endData[1].vCenterPos, FALSE)
					
						// ADJUST SPEED HERE
//						DISABLE_TIRES_DUE_TO_DAMAGE(0, smugArgs)
					ENDIF
				ENDIF
			ELSE
				IF smugArgs.bRunSpeedAdjustment
					ADJUST_PLAYBACK_SPEED(myLocData, smugArgs, myLocData.endData[1].vCenterPos)
				ENDIF
			ENDIF
			
			// -------------------------------------------------------KILL VEHICLE CHECK-------------------------------------------------------
			REPEAT myLocData.iNumSmugVehicles idx
				IF DOES_ENTITY_EXIST(smugArgs.smugglersVehicles[idx]) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersVehicles[idx])
						
						// --------------------------------------------DROPOFF CHECK---------------------------------------------
						
						IF IS_ENTITY_AT_COORD(smugArgs.smugglersVehicles[idx], myLocData.endData[1].vCenterPos, <<12, 12, 3>>) AND smugArgs.bCarHasDrugs[idx]
						AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.endData[1].vCenterPos, vPlayerTooFar)
						AND NOT smugArgs.bHeliPedDead
						OR (IS_ENTITY_AT_COORD(smugArgs.smugglersVehicles[idx], myLocData.endData[1].vCenterPos, <<12, 12, 3>>) AND smugArgs.bCarHasDrugs[idx] 
						AND IS_ENTITY_OCCLUDED(smugArgs.smugglersVehicles[idx]) AND NOT smugArgs.bHeliPedDead)
							
							FREEZE_ENTITY_POSITION(smugArgs.smugglersHeliVeh, FALSE)
							
							IF NOT IS_ENTITY_OCCLUDED(smugArgs.smugglersHeliVeh)
								PRINTLN("HELICOPTER IS VISIBLE")
								IF NOT IS_PED_INJURED(smugArgs.smugglersHeliPed) AND NOT IS_ENTITY_DEAD(smugArgs.smugglersHeliVeh)
									CLEAR_SEQUENCE_TASK(iSeq)
									OPEN_SEQUENCE_TASK(iSeq)
										TASK_ENTER_VEHICLE(NULL, smugArgs.smugglersHeliVeh)
										TASK_HELI_MISSION(NULL, smugArgs.smugglersHeliVeh, NULL, NULL, <<myLocData.endData[1].vSmugHeliPos.x, myLocData.endData[1].vSmugHeliPos.y, (myLocData.endData[1].vSmugHeliPos.z + 50.0)>>, MISSION_GOTO, 40.0, -1, 0, 60, 30)
										TASK_HELI_MISSION(NULL, smugArgs.smugglersHeliVeh, NULL, NULL, <<(myLocData.endData[1].vSmugHeliPos.x + 500), (myLocData.endData[1].vSmugHeliPos.y + 500), (myLocData.endData[1].vSmugHeliPos.z + 10.0)>>, MISSION_GOTO, 40.0, -1, 0, 60, 30)
									CLOSE_SEQUENCE_TASK(iSeq)
									IF NOT IS_PED_INJURED(smugArgs.smugglersHeliPed)
										TASK_PERFORM_SEQUENCE(smugArgs.smugglersHeliPed, iSeq)
										PRINTLN("TASKING HELI PED TO FLY AWAY")
									ENDIF
									CLEAR_SEQUENCE_TASK(iSeq)
								ENDIF
							ELSE
								IF DOES_ENTITY_EXIST(smugArgs.smugglersHeliVeh)
									DELETE_VEHICLE(smugArgs.smugglersHeliVeh)
									PRINTLN("DELETING smugArgs.smugglersHeliVeh")
								ENDIF
								IF DOES_ENTITY_EXIST(smugArgs.smugglersHeliPed)
									DELETE_PED(smugArgs.smugglersHeliPed)
									PRINTLN("DELETING smugArgs.smugglersHeliPed")
								ENDIF
							ENDIF
							
							IF NOT IS_ENTITY_OCCLUDED(smugArgs.vehSmugglerCarWithDrugs)
								IF IS_VEHICLE_DRIVEABLE(smugArgs.vehSmugglerCarWithDrugs) AND NOT IS_PED_INJURED(smugArgs.pedSmugglerWithDrugs)
									GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(smugArgs.vehSmugglerCarWithDrugs), vClosestNode)
									SET_ENTITY_COORDS(smugArgs.vehSmugglerCarWithDrugs, vClosestNode)
									PRINTLN("MOVING SMUGGLER'S VEHICLE TO NEAREST NODE")
								ENDIF
							ELSE
								IF DOES_ENTITY_EXIST(smugArgs.vehSmugglerCarWithDrugs)
									DELETE_VEHICLE(smugArgs.vehSmugglerCarWithDrugs)
									PRINTLN("DELETING smugArgs.vehSmugglerCarWithDrugs")
								ENDIF
								IF DOES_ENTITY_EXIST(smugArgs.smugglersPeds[0])
									DELETE_PED(smugArgs.smugglersPeds[0])
									PRINTLN("DELETING smugArgs.smugglersPeds[0]")
								ENDIF
								IF DOES_ENTITY_EXIST(smugArgs.smugglersPeds[1])
									DELETE_PED(smugArgs.smugglersPeds[1])
									PRINTLN("DELETING smugArgs.smugglersPeds[1]")
								ENDIF
							ENDIF
							
							smugArgs.bSmugglerReachedDest = TRUE
							DEBUG_PRINT("smugArgs.bSmugglerReachedDest = TRUE VIA PLAYER BEING TOO FAR AWAY TO TRIGGER CUTSCENE")
							
							smugglersStage = SMUGGLERS_STATE_COMPLETE
							BREAK
						ENDIF
						
						//******************************************************************************************************************
						// If the smuggler AI are at the dropoff and the player is not close enough, go ahead and run ending sequence.
						IF IS_ENTITY_AT_COORD(smugArgs.smugglersVehicles[idx], myLocData.endData[1].vCenterPos, <<12, 12, 3>>) AND smugArgs.bCarHasDrugs[idx]
						AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.endData[1].vCenterPos, vPlayerTooClose)
						AND NOT smugArgs.bHeliPedDead
							DEBUG_PRINT("SMUGGLER REACHED OBJECTIVE")
							
							IF myLocData.dropData[0].smugglersRecordings[idx].bWaypointConfigured
								IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx])
									BRING_VEHICLE_TO_HALT(smugArgs.smugglersVehicles[idx], 5.0, 2)
								ENDIF
							ENDIF
						
							PAUSE_OR_STOP_RECORDING(myLocData, smugArgs, idx)
							
							// Task smuggler to meet guy at the helicopter
							IF NOT IS_PED_INJURED(smugArgs.pedSmugglerWithDrugs) AND NOT IS_ENTITY_DEAD(smugArgs.smugglersHeliVeh) AND NOT IS_PED_INJURED(smugArgs.smugglersHeliPed)
								CLEAR_SEQUENCE_TASK(iSeq)
								OPEN_SEQUENCE_TASK(iSeq)
									TASK_LEAVE_VEHICLE(NULL, smugArgs.vehSmugglerCarWithDrugs, ECF_DONT_CLOSE_DOOR)
//									TASK_GO_TO_ENTITY(NULL, smugArgs.smugglersHeliPed)
								CLOSE_SEQUENCE_TASK(iSeq)
								IF NOT IS_PED_INJURED(smugArgs.pedSmugglerWithDrugs)
									TASK_PERFORM_SEQUENCE(smugArgs.pedSmugglerWithDrugs, iSeq)
								ENDIF
								CLEAR_SEQUENCE_TASK(iSeq)
							ELSE
//								IF NOT smugArgs.bHeliPedDead
//									IF IS_PED_INJURED(smugArgs.smugglersHeliPed) OR NOT IS_VEHICLE_DRIVEABLE(smugArgs.smugglersHeliVeh)
//										IF NOT IS_PED_INJURED(smugArgs.pedSmugglerWithDrugs)
//											TASK_COMBAT_PED(smugArgs.pedSmugglerWithDrugs, PLAYER_PED_ID())
//										ENDIF
//										smugArgs.bHeliPedDead = TRUE
//									ENDIF
//								ENDIF
							ENDIF
							
							IF NOT smugArgs.bHeliPedDead
								IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.endData[1].vCenterPos, <<15,15,15>>)
										SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<myLocData.endData[1].vCenterPos.x + 20, myLocData.endData[1].vCenterPos.y + 20, myLocData.endData[1].vCenterPos.z>>)
									ENDIF
								ENDIF
								
								smugglersStage = SMUGGLERS_STATE_DROPOFF_DELIVERY
								BREAK
							ENDIF
						//******************************************************************************************************************
						// If the smuggler AI are at the dropoff site and the player is nearby, make them fight it out.
						ELIF (IS_ENTITY_AT_COORD(smugArgs.smugglersVehicles[idx], myLocData.endData[1].vCenterPos, g_vAnyMeansLocate) AND smugArgs.bCarHasDrugs[idx]
						AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.endData[1].vCenterPos, vPlayerTooClose))
						OR (IS_ENTITY_AT_COORD(smugArgs.smugglersVehicles[idx], myLocData.endData[1].vCenterPos, g_vAnyMeansLocate) AND smugArgs.bCarHasDrugs[idx]
							AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.endData[1].vCenterPos, vPlayerTooClose) AND smugArgs.bHeliPedDead AND NOT smugArgs.bSmugglerPedsDead)
							// Task the driver to fight
							
							SET_VEHICLE_DOORS_LOCKED(smugArgs.smugglersVehicles[0], VEHICLELOCK_UNLOCKED)
							
							IF NOT IS_PED_INJURED(smugArgs.smugglersPeds[0])
								SET_PED_COMBAT_MOVEMENT(smugArgs.smugglersPeds[0], CM_DEFENSIVE)
								
								CLEAR_SEQUENCE_TASK(iSeq)
								OPEN_SEQUENCE_TASK(iSeq)
									TASK_LEAVE_ANY_VEHICLE(NULL, 500, ECF_DONT_CLOSE_DOOR)
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(iSeq)
								IF NOT IS_PED_INJURED(smugArgs.smugglersPeds[0])
									TASK_PERFORM_SEQUENCE(smugArgs.smugglersPeds[0], iSeq)
									DEBUG_PRINT("GIVING TASK TO SMUGGLER PED 0")
									SET_PED_COMBAT_MOVEMENT(smugArgs.smugglersPeds[0], CM_DEFENSIVE)
								ENDIF
								CLEAR_SEQUENCE_TASK(iSeq)
							ENDIF
							IF NOT IS_PED_INJURED(smugArgs.smugglersPeds[1])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(smugArgs.smugglersPeds[1], TRUE)
								SET_PED_COMBAT_MOVEMENT(smugArgs.smugglersPeds[1], CM_DEFENSIVE)
								
								CLEAR_SEQUENCE_TASK(iSeq)
								OPEN_SEQUENCE_TASK(iSeq)
									TASK_LEAVE_ANY_VEHICLE(NULL, 1000, ECF_DONT_CLOSE_DOOR)
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(iSeq)
								IF NOT IS_PED_INJURED(smugArgs.smugglersPeds[1])
									TASK_PERFORM_SEQUENCE(smugArgs.smugglersPeds[1], iSeq)
									DEBUG_PRINT("GIVING TASK TO SMUGGLER PED 1")
									SET_PED_COMBAT_MOVEMENT(smugArgs.smugglersPeds[1], CM_DEFENSIVE)
								ENDIF
								CLEAR_SEQUENCE_TASK(iSeq)
							ENDIF
							// Task the heli ped to fight
							IF NOT IS_PED_INJURED(smugArgs.smugglersHeliPed)
								TASK_COMBAT_PED(smugArgs.smugglersHeliPed, PLAYER_PED_ID())
								SET_PED_COMBAT_MOVEMENT(smugArgs.smugglersHeliPed, CM_DEFENSIVE)
							ENDIF
							
							PRINT_NOW("DTRFKGR_KILL", DEFAULT_GOD_TEXT_TIME, 1)
							
							// Remove blip on car
							IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[0])
								REMOVE_BLIP(smugArgs.blipSmugglersCars[0])
								DEBUG_PRINT("REMOVING BLIP ON CAR BECAUSE GUYS SHOULD BE OUTSIDE OF CAR NOW")
							ENDIF
							
							// Add blips for guys
							IF NOT DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[0])
								smugArgs.blipSmugglersPeds[0] = ADD_BLIP_FOR_ENTITY(smugArgs.smugglersPeds[0])
								DEBUG_PRINT("ADDING BLIP TO smugArgs.smugglersPeds[0]")
								SET_BLIP_SCALE(smugArgs.blipSmugglersPeds[0], 0.5)
							ENDIF
							
							IF NOT DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[1])
								smugArgs.blipSmugglersPeds[1] = ADD_BLIP_FOR_ENTITY(smugArgs.smugglersPeds[1])
								DEBUG_PRINT("ADDING BLIP TO smugArgs.smugglersPeds[1]")
								SET_BLIP_SCALE(smugArgs.blipSmugglersPeds[1], 0.5)
							ENDIF
							
							IF NOT DOES_BLIP_EXIST(smugArgs.blipHeliPed)
								smugArgs.blipHeliPed = ADD_BLIP_FOR_ENTITY(smugArgs.smugglersHeliPed)
								DEBUG_PRINT("ADDING BLIP TO smugArgs.smugglersHeliPed")
								SET_BLIP_SCALE(smugArgs.blipHeliPed, 0.5)
							ENDIF
							
							smugArgs.bShootout = TRUE
							PRINTLN("smugArgs.bShootout = TRUE")
							
							smugglersStage = SMUGGLERS_STATE_DROPOFF_FIGHT
							BREAK
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[idx])
							REMOVE_BLIP(smugArgs.blipSmugglersCars[idx])
							DEBUG_PRINT("REMOVING BLIPS BECAUSE THE CAR IS DEAD")
						ENDIF
						
//						IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[0])
//							REMOVE_BLIP(smugArgs.blipSmugglersPeds[0])
//							DEBUG_PRINT("REMOVING smugArgs.blipSmugglersPeds[0] BECAUSE THE VEHICLE IS DEAD")
//						ENDIF
//						IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[1])
//							REMOVE_BLIP(smugArgs.blipSmugglersPeds[1])
//							DEBUG_PRINT("REMOVING smugArgs.blipSmugglersPeds[1] BECAUSE THE VEHICLE IS DEAD")
//						ENDIF
						
						IF NOT smugArgs.bTaskedToCombat[0] AND NOT smugArgs.bTaskedToCombat[1]
							IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[0])
								CLEAR_SEQUENCE_TASK(iSeqTask)
								OPEN_SEQUENCE_TASK(iSeqTask)
									TASK_LEAVE_ANY_VEHICLE(NULL)
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(iSeqTask)
									TASK_PERFORM_SEQUENCE(smugArgs.smugglersPeds[0], iSeqTask)
									PRINTLN("TASKING - smugArgs.smugglersPeds[0] TO EXIT AND COMBAT")
								CLEAR_SEQUENCE_TASK(iSeqTask)
							ENDIF
							
							IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[1])
								CLEAR_SEQUENCE_TASK(iSeqTask)
								OPEN_SEQUENCE_TASK(iSeqTask)
									TASK_LEAVE_ANY_VEHICLE(NULL)
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(iSeqTask)
									TASK_PERFORM_SEQUENCE(smugArgs.smugglersPeds[1], iSeqTask)
									PRINTLN("TASKING - smugArgs.smugglersPeds[1] TO EXIT AND COMBAT")
								CLEAR_SEQUENCE_TASK(iSeqTask)
							ENDIF
							
							smugArgs.bTaskedToCombat[0] = TRUE
							smugArgs.bTaskedToCombat[1] = TRUE
						ENDIF
					
						//  -------------------------------FAIL MISSION IF THE VEHICLE DIES-------------------------------
						// If the vehicle has the drugs and is destroyed, set flag to fail the mission.
//						IF smugArgs.bPickupAIFirst
//							EXPLODE_VEHICLE(smugArgs.smugglersVehicles[0])
//							DEBUG_PRINT("SETTING smugArgs.bDestroyedVehicleWithDrugs TO TRUE")
//							smugArgs.bDestroyedVehicleWithDrugs = TRUE
//						ENDIF
						
						PAUSE_OR_STOP_RECORDING(myLocData, smugArgs, idx)
						
						IF smugArgs.bPickupPlayerFirst
							EXPLODE_VEHICLE(smugArgs.smugglersVehicles[0])
							smugArgs.bGoToDestination = TRUE
							DEBUG_PRINT("SETTING smugArgs.bGoToDestination = TRUE BECAUSE THE PLAYER GOT THE PACKAGE AND THE SMUGGLERS VEHICLE DIED")
							
							DEBUG_PRINT("GOING TO STATE SMUGGLERS_STATE_COMPLETE")
							smugglersStage = SMUGGLERS_STATE_COMPLETE
							BREAK
						ENDIF
					ENDIF
				
				
				// Perform upside down check... if the AI is upsidedown for 4 seconds, blow them up.
				IF DOES_ENTITY_EXIST(smugArgs.smugglersVehicles[idx])
					IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[idx])
						IF IS_ENTITY_UPSIDEDOWN(smugArgs.smugglersVehicles[idx]) 
							IF NOT bSmugglerUpsideDown
								bSmugglerUpsideDown = TRUE
								iSmugglerUpsideDownVehicleTime = GET_GAME_TIMER()
								
								IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[0])
									IF IS_PED_IN_ANY_VEHICLE(smugArgs.smugglersPeds[0])
										CLEAR_SEQUENCE_TASK(iSeqTask)
										OPEN_SEQUENCE_TASK(iSeqTask)
											TASK_LEAVE_ANY_VEHICLE(NULL)
											TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
										CLOSE_SEQUENCE_TASK(iSeqTask)
										TASK_PERFORM_SEQUENCE(smugArgs.smugglersPeds[0], iSeqTask)
										PRINTLN("TASKING GUY TO LEAVE VEHICLE AND ATTACH, BECAUSE SMUGGLER VEHICLE IS UPSIDEDOWN - 01")
										CLEAR_SEQUENCE_TASK(iSeqTask)
									ENDIF
								ENDIF
								IF NOT IS_ENTITY_DEAD(smugArgs.smugglersPeds[1])
									IF IS_PED_IN_ANY_VEHICLE(smugArgs.smugglersPeds[1])
										CLEAR_SEQUENCE_TASK(iSeqTask)
										OPEN_SEQUENCE_TASK(iSeqTask)
											TASK_LEAVE_ANY_VEHICLE(NULL)
											TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
										CLOSE_SEQUENCE_TASK(iSeqTask)
										TASK_PERFORM_SEQUENCE(smugArgs.smugglersPeds[1], iSeqTask)
										PRINTLN("TASKING GUY TO LEAVE VEHICLE AND ATTACH, BECAUSE SMUGGLER VEHICLE IS UPSIDEDOWN - 02")
										CLEAR_SEQUENCE_TASK(iSeqTask)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							bSmugglerUpsideDown = FALSE
							iSmugglerUpsideDownVehicleTime = 0
						ENDIF
						
						IF bSmugglerUpsideDown
							IF (GET_GAME_TIMER() - iSmugglerUpsideDownVehicleTime) > BLOW_UP_TIME
								DEBUG_PRINT("EXPLODING SMUGGLER VEHICLE")
								EXPLODE_VEHICLE(smugArgs.smugglersVehicles[idx])
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			// Check for killed peds... if all dead, continue with retrieve check.
			IF KILL_PEDS_RETRIEVE_DRUGS(myLocData, smugArgs, smugglerRetrieveState, sLastObjective, myVehicle, cutArgs, 
			drugCargoArgs, bOutOfVehicle, bDrugsLoaded)
				
				TRIGGER_MUSIC_EVENT("OJDG2_1ST_SET_DEAD")
				PRINTLN("TRIGGERING MUSIC - OJDG2_1ST_SET_DEAD")
				
				// Reseting cutscene state
				cutArgs.cutsceneState = CUTSCENE_STATE_START
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle) 
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
					
						// If the AI got to the pickup first, re-blip destination objective when the smugglers chase is over.
						IF smugArgs.bPickupAIFirst
						
							// Print new objective
							CLEAR_PRINTS()
							PRINT_NOW("DTRFKGR_03",DEFAULT_GOD_TEXT_TIME,1) 
							sLastObjective = "DTRFKGR_03"
							
							// Setting to turn back on ABANDON check
							bIsSmugglersActive = FALSE
							
							// Add drop off blip
							IF NOT DOES_BLIP_EXIST(myLocationBlip[0])
								myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.endData[0].vDrugPickup)
								IF DOES_BLIP_EXIST(myLocationBlip[0])
			//						SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
									SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
								ENDIF
								vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
							ELSE
								DEBUG_PRINT("myLocationBlip[0] DOES EXIST")
							ENDIF
						ENDIF
					
						PRINTLN("PLAYER IS IN THEIR VEHICLE GOING TO STATE - SMUGGLERS_STATE_COMPLETE")
						bDrugsLoaded = TRUE
						smugglersStage = SMUGGLERS_STATE_COMPLETE
					ELSE
//						PRINT_NOW("DTRSHRD_03",DEFAULT_GOD_TEXT_TIME,1)
//						vehBlip = ADD_BLIP_FOR_ENTITY(myVehicle)
//						SET_BLIP_COLOUR(vehBlip, BLIP_COLOUR_BLUE)
					
						PRINTLN("PLAYER IS NOT IN THEIR VEHICLE GOING TO STATE - SMUGGLERS_STATE_CHECK_FOR_IN_VEHICLE")
						smugglersStage = SMUGGLERS_STATE_CHECK_FOR_IN_VEHICLE
					ENDIF
				ENDIF
			ENDIF
			
			IF SMUGGLER_PED_DEAD_CHECK(myLocData, smugArgs, MyLocalPedStruct)
				IF NOT bTriggeredMusicEvent01
					TRIGGER_MUSIC_EVENT("OJDG2_FIRST_ENEMIES_DEAD")
					PRINTLN("TRIGGERING MUSIC - OJDG2_FIRST_ENEMIES_DEAD - 03")
					bTriggeredMusicEvent01 = TRUE
				ENDIF
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_STATE_CHECK_FOR_IN_VEHICLE
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle) 
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
					IF DOES_BLIP_EXIST(vehBlip)
						REMOVE_BLIP(vehBlip)
					ENDIF
		
						// If the AI got to the pickup first, re-blip destination objective when the smugglers chase is over.
					IF smugArgs.bPickupAIFirst
					
						// Print new objective
						CLEAR_PRINTS()
						PRINT_NOW("DTRFKGR_03",DEFAULT_GOD_TEXT_TIME,1) 
						sLastObjective = "DTRFKGR_03"
						
						// Setting to turn back on ABANDON check
						bIsSmugglersActive = FALSE
						
						// Add drop off blip
						IF NOT DOES_BLIP_EXIST(myLocationBlip[0])
							myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.endData[0].vDrugPickup)
							IF DOES_BLIP_EXIST(myLocationBlip[0])
		//						SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
								SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
							ENDIF
							vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
						ELSE
							DEBUG_PRINT("myLocationBlip[0] DOES EXIST")
						ENDIF
					ENDIF
					
					PRINTLN("PLAYER IS IN THEIR VEHICLE GOING TO STATE - SMUGGLERS_STATE_COMPLETE VIA SMUGGLERS_STATE_CHECK_FOR_IN_VEHICLE")
					bDrugsLoaded = TRUE
					smugglersStage = SMUGGLERS_STATE_COMPLETE
				ELSE
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						vPlayerLocation =  GET_ENTITY_COORDS(PLAYER_PED_ID())
					ENDIF
				
					IF VDIST2(vPlayerLocation, smugArgs.vDefeatedSmugglerVehCoor) > 122500 // 350m
						PRINTLN("smugArgs.bAbandonedPackage = TRUE - SPECIAL CASE")
						smugArgs.bAbandonedPackage = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_STATE_DROPOFF_FIGHT
		
			// Remove blip on car
			IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[0])
				REMOVE_BLIP(smugArgs.blipSmugglersCars[0])
				DEBUG_PRINT("REMOVING BLIP ON CAR BECAUSE GUYS SHOULD BE OUTSIDE OF CAR NOW")
			ENDIF
		
			// Check if all are dead, run cutscene
			IF IS_PED_INJURED(smugArgs.smugglersPeds[0])
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[0])
					REMOVE_BLIP(smugArgs.blipSmugglersPeds[0])
					PRINTLN("REMOVING smugArgs.blipSmugglersPeds[0]")
				ENDIF
			ENDIF
			IF IS_PED_INJURED(smugArgs.smugglersPeds[1])
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[1])
					REMOVE_BLIP(smugArgs.blipSmugglersPeds[1])
					PRINTLN("REMOVING smugArgs.blipSmugglersPeds[1]")
				ENDIF
			ENDIF
			IF IS_PED_INJURED(smugArgs.smugglersHeliPed)
				IF DOES_BLIP_EXIST(smugArgs.blipHeliPed)
					REMOVE_BLIP(smugArgs.blipHeliPed)
					PRINTLN("REMOVING smugArgs.blipHeliPed")
				ENDIF
			ENDIF
		
			IF IS_PED_INJURED(smugArgs.smugglersHeliPed) AND IS_PED_INJURED(smugArgs.smugglersPeds[0]) AND IS_PED_INJURED(smugArgs.smugglersPeds[1])
				SETTIMERA(0)
				DEBUG_PRINT("ALL GUYS ARE KILLED, MOVE ON TO STATE - SMUGGLERS_STATE_DROPOFF_FIGHT_A")
				smugglersStage = SMUGGLERS_STATE_DROPOFF_FIGHT_A
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_STATE_DROPOFF_FIGHT_A
			IF TIMERA() > 1000
			
				// Add blip to pickup the package
				IF NOT DOES_BLIP_EXIST(smugArgs.blipSmugglersRetrieve)
					
					IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0])
						SET_VEHICLE_DOOR_OPEN(smugArgs.smugglersVehicles[0], SC_DOOR_BOOT)
					ENDIF
				
					smugArgs.blipSmugglersRetrieve = ADD_BLIP_FOR_ENTITY(smugArgs.smugglersVehicles[idx])
					DEBUG_PRINT("ADDING BLIP TO PICK UP THE PACKAGE")
					SET_BLIP_COLOUR(smugArgs.blipSmugglersRetrieve, BLIP_COLOUR_GREEN)
					
					// Print objective
					PRINT_NOW("DTRFKGR_06", DEFAULT_GOD_TEXT_TIME, 1)
					sLastObjective = "DTRFKGR_06"
				ENDIF
				
				// Wait until the vehicle is stopped to grab it's coordinates.
				IF IS_VEHICLE_ALMOST_STOPPED_GROUND(smugArgs.smugglersVehicles[idx])
					GRAB_SMUGGLERS_COORDINATES(smugArgs, idx)
					
					DEBUG_PRINT("BUFFER TIME IS UP, MOVE ON TO CUTSCENE")
					smugglersStage = SMUGGLERS_STATE_DROPOFF_FIGHT_CUTSCENE
				ELSE
					IF IS_DEAD_VEHICLE_ALMOST_STOPPED(smugArgs.smugglersVehicles[idx])
						GRAB_SMUGGLERS_COORDINATES(smugArgs, idx)
						
						DEBUG_PRINT("BUFFER TIME IS UP, MOVE ON TO CUTSCENE")
						smugglersStage = SMUGGLERS_STATE_DROPOFF_FIGHT_CUTSCENE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_STATE_DROPOFF_FIGHT_CUTSCENE
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), smugArgs.vDefeatedSmugglerVehCoor, <<5,5,4>>)
				IF IS_TIMER_STARTED(smugArgs.tBufferTimer)
					IF GET_TIMER_IN_SECONDS(smugArgs.tBufferTimer) > 2.0
						IF IS_VEHICLE_DRIVEABLE(myVehicle)
							
							// Remove blue package blip
							IF DOES_BLIP_EXIST(smugArgs.blipSmugglersRetrieve)
								REMOVE_BLIP(smugArgs.blipSmugglersRetrieve)
								
								// Remove from AI 
								REMOVE_DRUG_CARGO(drugCargoArgs)
								// Add to player's vehicle
								ADD_DRUGS_TO_VEHICLE(drugCargoArgs, myLocData, myVehicle)
								smugArgs.bCarHasDrugs[0] = FALSE
							ENDIF
						
							bDrugsLoaded = TRUE
							IF NOT IS_ENTITY_DEAD(myVehicle)
								SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_LOCKED_NO_PASSENGERS)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_TIMER_STARTED(smugArgs.tBufferTimer)
						START_TIMER_NOW(smugArgs.tBufferTimer)
						PRINTLN("STARTING BUFFER TIMER - 02")
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					vPlayerLocation =  GET_ENTITY_COORDS(PLAYER_PED_ID())
				ENDIF
			
				IF VDIST2(vPlayerLocation, smugArgs.vDefeatedSmugglerVehCoor) > 122500 // 350m
					PRINTLN("smugArgs.bAbandonedPackage = TRUE")
					smugArgs.bAbandonedPackage = TRUE
				ELIF VDIST2(vPlayerLocation, smugArgs.vDefeatedSmugglerVehCoor) > 10000 // 100m
					IF NOT bPrintReturnToPackageWarning
						PRINT_NOW("DTRFKGR_06a",DEFAULT_GOD_TEXT_TIME,1)
						PRINTLN("bPrintReturnToPackageWarning = TRUE")
						bPrintReturnToPackageWarning = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bDrugsLoaded
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
					
						// Setting to turn back on ABANDON check
						bIsSmugglersActive = FALSE
						
						// Reseting cutscene state
						cutArgs.cutsceneState = CUTSCENE_STATE_START
					
						// Print new objective
						PRINT_NOW("DTRFKGR_03",DEFAULT_GOD_TEXT_TIME,1) 
						sLastObjective = "DTRFKGR_03"
						
						// Add drop off blip
						myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.endData[0].vDrugPickup)
						IF DOES_BLIP_EXIST(myLocationBlip[0])
		//					SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
							SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
						ENDIF
						vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
						
						PRINTLN("GOING TO STATE - SMUGGLERS_STATE_COMPLETE VIA SMUGGLERS_STATE_DROPOFF_FIGHT_CUTSCENE")
						smugglersStage = SMUGGLERS_STATE_COMPLETE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_STATE_DROPOFF_DELIVERY
			// Run ending cutscene
			IF DO_CUTSCENE_CUSTOM(cutArgs.cutsceneState, cutArgs.cutSceneTimer, myLocData.endData[1].vCutscenePos1, 
			myLocData.endData[1].vCutsceneOrien1, myCamera, "", fpSmugglersEnding, FALSE, FALSE, FALSE, 35.0, 3000, 
			DEFAULT_INTERP_TO_FROM_GAME, FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				smugglersStage = SMUGGLERS_STATE_COMPLETE
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_STATE_COMPLETE
			// Remove blips
			REPEAT COUNT_OF(smugArgs.blipSmugglersPeds) idx
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[idx])
					REMOVE_BLIP(smugArgs.blipSmugglersPeds[idx])
					DEBUG_PRINT("REMOVING BLIP blipSmugglersPeds")
				ENDIF
			ENDREPEAT
			REPEAT COUNT_OF(smugArgs.blipSmugglersCars) idx 
				IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[idx])
					REMOVE_BLIP(smugArgs.blipSmugglersCars[idx])
					DEBUG_PRINT("REMOVING blipSmugglersCars")
				ENDIF
			ENDREPEAT
			
			REPEAT (myLocData.iNumSmugPedsPerVehicle * myLocData.iNumSmugVehicles) idx
				IF DOES_ENTITY_EXIST(smugArgs.smugglersPeds[idx]) 
					IF IS_ENTITY_OCCLUDED(smugArgs.smugglersPeds[idx])
						DELETE_PED(smugArgs.smugglersPeds[idx])
						PRINTLN("DELETE PED - smugArgs.smugglersPeds[idx]")
					ELSE
						SET_PED_AS_NO_LONGER_NEEDED(smugArgs.smugglersPeds[idx])
						PRINTLN("SETTING smugArgs.smugglersPeds[idx] AS NO LONGER NEEDED")
					ENDIF
				ENDIF
			ENDREPEAT
			
			REPEAT myLocData.iNumSmugVehicles idx
				IF DOES_ENTITY_EXIST(smugArgs.smugglersVehicles[idx])
					IF IS_ENTITY_OCCLUDED(smugArgs.smugglersVehicles[idx])
						DELETE_VEHICLE(smugArgs.smugglersVehicles[idx])
						PRINTLN("DELETING VEHICLE - smugArgs.smugglersVehicles[idx]")
					ELSE
						SET_VEHICLE_AS_NO_LONGER_NEEDED(smugArgs.smugglersVehicles[idx])
						PRINTLN("SETTING smugArgs.smugglersVehicles[idx] AS NO LONGER NEEDED")
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF DOES_ENTITY_EXIST(smugArgs.smugglersHeliVeh)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(smugArgs.smugglersHeliVeh)
				PRINTLN("SETTING smugArgs.smugglersHeliVeh AS NO LONGER NEEDED")
			ELSE
				SET_MODEL_AS_NO_LONGER_NEEDED(myLocData.mnSmugglersHeli)
				SET_MODEL_AS_NO_LONGER_NEEDED(CUBAN800)
				PRINTLN("SETTING myLocData.mnSmugglersHeli MODEL AS NO LONGER NEEDED")
			ENDIF
			
			IF DOES_ENTITY_EXIST(smugArgs.smugglersHeliPed)
				SET_PED_AS_NO_LONGER_NEEDED(smugArgs.smugglersHeliPed)
				PRINTLN("SETTING smugArgs.smugglersHeliPed AS NO LONGER NEEDED")
			ENDIF
			
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT DOES_BLIP_EXIST(myLocationBlip[0])
					PRINT_NOW("DTRFKGR_03",DEFAULT_GOD_TEXT_TIME,1) 
					sLastObjective = "DTRFKGR_03"
					myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.endData[0].vDrugPickup)
					IF DOES_BLIP_EXIST(myLocationBlip[0])
						SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
					ENDIF
					vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
				ELSE
					DEBUG_PRINT("myLocationBlip[0] DOES EXIST")
					sLastObjective = "DTRFKGR_03"
					vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
					PRINTLN("vCurDestObjective[0] = ", vCurDestObjective[0])
				ENDIF
				
				// Setting to turn back on ABANDON check
				bIsSmugglersActive = FALSE
				
				// Re-setting to fix bug where the player fails when being out of their vehicle for too long.
				outOfVehicleTime = GET_GAME_TIMER()
				
				DEBUG_PRINT("smugArgs.bGoToDestination = TRUE IN STATE SMUGGLERS_STATE_COMPLETE")
				smugArgs.bGoToDestination = TRUE
				
				SET_MAX_WANTED_LEVEL(5)
				DEBUG_PRINT("SETTING MAX WANTED LEVEL BACK TO SIX")
			
				DEBUG_PRINT("RETURNING TRUE ON UPDATE_SMUGGLERS_MODE")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

