//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     : Transport                                               		//
//      AUTHOR          : Steven Messinger                                              //
//      DESCRIPTION     :         														//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// including the most common script headers
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_vehicle.sch"
USING "commands_player.sch" 
USING "commands_path.sch"
USING "finance_control_public.sch"
USING "player_ped_public.sch"
USING "model_enums.sch"
USING "timer_public.sch"
USING "lineactivation.sch"
USING "completionpercentage_public.sch"
USING "flow_public_game.sch"
USING "Drug_Trafficking_core.sch"
USING "Traffick_CarChase_lib.sch"
USING "Traffick_Ground_Smugglers_lib.sch"
USING "Traffick_Ground_Hijack_lib.sch"
USING "replay_public.sch"
USING "RC_Helper_Functions.sch"
USING "end_screen.sch"
USING "achievement_public.sch"
USING "rich_presence_public.sch"

//USING "z_volumes.sch"

// Enums
ENUM MISSION_STAGE_ENUM // distinct stages of the mission
	STAGE_START = 0,
	STAGE_GET_CAR,
	STAGE_GET_CAR_B,
	STAGE_DO_TAXI_CUTSCENE,
	STAGE_GET_CAR_C,
	STAGE_DRIVE_HERE,
	STAGE_PICKUP,
	STAGE_PICKUP_NEED_CAR,
	STAGE_PICKUP_HAVE_CAR,
	STAGE_FIGHT_AMBUSH,
	STAGE_DELIVER,
	STAGE_PICKUP_B,
	STAGE_PICKUP_C,
	STAGE_DESTINATION,
	STAGE_ENDING_CUTSCENE,
	STAGE_ENDING_CUTSCENE_RUN,
	STAGE_DISPLAY_UI_01,
	STAGE_DISPLAY_UI_02,
	STAGE_DISPLAY_UI_03,
	STAGE_DRIVE_OFF,
	STAGE_ESCAPE
ENDENUM

ENUM ENDING_CUTSCENE_STATE
	ENDING_CUTSCENE_STATE_01 = 0,
	ENDING_CUTSCENE_STATE_02,
	ENDING_CUTSCENE_STATE_03,
	ENDING_CUTSCENE_STATE_04,
	ENDING_CUTSCENE_SKIP_01,
	ENDING_CUTSCENE_SKIP_02
ENDENUM

ENUM SMUGGLERS_ENDING_STATE
	SMUGGLERS_ENDING_STATE_01 = 0,
	SMUGGLERS_ENDING_STATE_02,
	SMUGGLERS_ENDING_STATE_03,
	SMUGGLERS_ENDING_STATE_04,
	SMUGGLERS_ENDING_STATE_05,
	SMUGGLERS_ENDING_SKIP_01,
	SMUGGLERS_ENDING_SKIP_02
ENDENUM

ENUM SMUGGLERS_PICKUP_STATE
	SMUGGLERS_PICKUP_STATE_01,
	SMUGGLERS_PICKUP_STATE_02,
	SMUGGLERS_PICKUP_STATE_03,
	SMUGGLERS_PICKUP_STATE_04,
	SMUGGLERS_PICKUP_STATE_05,
	SMUGGLERS_PICKUP_SKIP_01,
	SMUGGLERS_PICKUP_SKIP_02
ENDENUM

ENUM SMUGGLER_ENDING_SHOOTOUT_STATE
	SMUGGLER_ENDING_SHOOTOUT_STATE_01 = 0, 
	SMUGGLER_ENDING_SHOOTOUT_STATE_02,
	SMUGGLER_ENDING_SHOOTOUT_STATE_03,
	SMUGGLER_ENDING_SHOOTOUT_SKIP_01,
	SMUGGLER_ENDING_SHOOTOUT_SKIP_02
ENDENUM

ENUM SMUGGLERS_RETRIEVE_STATE
	SMUGGLERS_RETRIEVE_STATE_01 = 0,
	SMUGGLERS_RETRIEVE_STATE_02,
	SMUGGLERS_RETRIEVE_STATE_03,
	SMUGGLERS_RETRIEVE_STATE_04,
	SMUGGLERS_RETRIEVE_SKIP_01,
	SMUGGLERS_RETRIEVE_SKIP_02
ENDENUM

ENUM FLARE_PICKUP_STATE
	FLARE_PICKUP_STATE_01 = 0,
	FLARE_PICKUP_STATE_02,
	FLARE_PICKUP_STATE_03,
	FLARE_PICKUP_STATE_04,
	FLARE_PICKUP_STATE_05,
	FLARE_PICKUP_STATE_06,
	FLARE_PICKUP_STATE_07,
	FLARE_PICKUP_STATE_08,
	FLARE_PICKUP_SKIP_01,
	FLARE_PICKUP_SKIP_02
ENDENUM

ENUM CONTACT_CUTSCENE_STATE
	CONTACT_CUTSCENE_STATE_01 = 0,
	CONTACT_CUTSCENE_STATE_02,
	CONTACT_CUTSCENE_STATE_03,
	CONTACT_CUTSCENE_STATE_04
ENDENUM

ENUM HIJACK_RETRIEVE_STATE
	HIJACK_RETRIEVE_STATE_01 = 0,
	HIJACK_RETRIEVE_STATE_01a,
	HIJACK_RETRIEVE_STATE_02, 
	HIJACK_RETRIEVE_STATE_03,
	HIJACK_RETRIEVE_STATE_04,
	HIJACK_RETRIEVE_STATE_SKIPPED_01,
	HIJACK_RETRIEVE_STATE_SKIPPED_02
ENDENUM

ENUM STARTING_CUTSCENE_STATE
	STARTING_CUTSCENE_STATE_01 = 0,
	STARTING_CUTSCENE_STATE_02,
	STARTING_CUTSCENE_STATE_03,
	STARTING_CUTSCENE_SKIP_01,
	STARTING_CUTSCENE_SKIP_02
ENDENUM

ENUM OSCAR_STATES
	OSCAR_CREATE = 0,
	OSCAR_UPDATE,
	OSCAR_COMPLETE
ENDENUM

ENUM QUICK_CAM
	QUICK_CAM_STAGE_01 = 0,
	QUICK_CAM_STAGE_02,
	QUICK_CAM_STAGE_03,
	QUICK_CAM_STAGE_04
ENDENUM

ENUM VARIATION
	TYPE_1 = 0,
	TYPE_2
ENDENUM

// Enum inits
SMUGGLERS_ENDING_STATE smugglerEndingStage = SMUGGLERS_ENDING_STATE_01
SMUGGLERS_RETRIEVE_STATE smugglerRetrieveCutState = SMUGGLERS_RETRIEVE_STATE_01
CAR_CHASE carChaseProgress = CAR_CHASE_INIT
AMBUSH ambushState = AMBUSH_INIT
PLANE_DROP planeDropState = PLANE_DROP_INIT
SMUGGLERS_STATE smugglersState = SMUGGLERS_STATE_TO_PICKUP
SMUGGLERS_RETRIEVE_DRUGS smugglerRetrieveState = SMUGGLERS_RETRIEVE_DRUGS_CHECK_PEDS_DEAD
FAIL_REASON_ENUM failReason
ENDING_CUTSCENE_STATE endingCutsceneStages = ENDING_CUTSCENE_STATE_01
SMUGGLER_PLANE_STATE smugglerPlaneRecordingStages = SMUGGLER_PLANE_STATE_INIT
//TRAP_STATE trapStages = TRAP_INIT
SMUGGLERS_CREATE_HELI_VEHICLE smugglersCreateHeliPedStages = SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_01
QUICK_CAM quickCamStages = QUICK_CAM_STAGE_01
MISSION_STAGE_ENUM currentMissionStage = STAGE_START

// All data needed by the UI.
STRUCT GROUND_TRAFFICKING_UI
	SCRIPT_SCALEFORM_UI uiControls
	END_SCREEN_DATASET groundTraffickingEndScreen
	INT uiFlags
ENDSTRUCT

GROUND_TRAFFICKING_UI groundEndScreen

// Function pointers
CustomCutsceneCaller fpSmugglersEnding
//CustomCutsceneCaller fpEndingCutscene01
CustomCutsceneCaller fpSmugglersPickupCutscene

// Structs
CUTSCENE_ARGS cutArgs
ARGS myArgsCopy
LOCATION_DATA myLocData
CAR_AMBUSH_ARGS ambushArgs
PLANE_DROP_ARGS planeDropArgs
DRUG_CARGO_ARGS drugCargoArgs
SMUGGLERS_MODE_ARGS smugArgs
CAR_CHASER_ARGS carChaserArgs
STATIC_CHASE_ARGS staticChaseArgs
HIJACK_ARGS hijackArgs
EXPLOSIVE_PACKAGE_ARGS expPackageArgs
//TRAP_ARGS trapArgs
structPedsForConversation MyLocalPedStruct

CONST_INT NUMBER_BAD_GUY_VEHICLES 1
CONST_INT HIJACK_DELAY_TIME 20  // using struct timer, so time is in seconds
CONST_INT DELAY_TIME 15000
CONST_INT DELAY_TIME_AMBUSH 25000
CONST_INT DISTANCE_TO_WARN_OF_AMBUSHERS 300

CONST_FLOAT MAX_TIME_ALLOWED_ON_ROADS 32.0

TEXT_LABEL_31 sLastObjective

BOOL bInAmbush = FALSE
BOOL bIsChaseActive = FALSE
BOOL bInActiveFight = FALSE
BOOL bDrugsLoaded = FALSE
BOOL bIsSmugglersActive = FALSE
BOOL bIsCutsceneActive = FALSE
BOOL bDoPlane = FALSE
BOOL bDoAmbush = TRUE
BOOL bDoCarChase = TRUE
BOOL bDoPoliceChase = FALSE
BOOL bDoDelicate = TRUE
BOOL bDoSmugglers = FALSE
BOOL bSmugglersCreated = FALSE
BOOL bComplete = FALSE
BOOL bDoFlareCutscene = FALSE
BOOL bDoContactCutscene = FALSE
BOOL bDoHijack = FALSE
BOOL bOkayToRunTrap = FALSE
BOOL bSkippedMission = FALSE
BOOL bWanted = FALSE
BOOL bOutOfVehicle = FALSE
BOOL bGenPrinted = FALSE
BOOL bTimedPickup = TRUE
BOOL bDisplayTimer = FALSE
BOOL bCarChaseComplete = FALSE
BOOL bPrintObjAgain = TRUE
BOOL bSkippedCutscene = FALSE
BOOL bSmugglersRetrieveCutscene = FALSE
BOOL bSmugglersEndingCutscene = FALSE
BOOL bEndingCutscene = FALSE
BOOL bRunOnce = FALSE
BOOL bTimerStarted = FALSE
BOOL bGiveTimeWarning = FALSE
BOOL bTriggerAmbushNow = FALSE
BOOL bTriggerAmbushLate = FALSE
BOOL bTimeExpired = FALSE
BOOL bPlayMusic = FALSE
BOOL bPrintObjectivesAndHelp = FALSE
BOOL bGrabbedTimeForCutscene = FALSE
BOOL bStreamed = FALSE
BOOL bInRepeatableMode = FALSE
BOOL bRemoveFlare = FALSE
BOOL bGiveAmbushWarning = FALSE
BOOL bPassed = FALSE
BOOL bPlayerVisible = FALSE
BOOL bOnRoads = FALSE
BOOL bGiveCopWarning = FALSE
BOOL bAmbushActive = FALSE
BOOL bPlayerWantedAtDropOff = FALSE
BOOL bToldToGetBackInVehicle = FALSE
BOOL bSetTimer = FALSE
BOOL bOutOfVehicleAtPickup = FALSE
BOOL bPrintLateObjective = FALSE
BOOL bUseInterp = FALSE
BOOL bTriggeredWanted = FALSE
BOOL bNoAmbush = FALSE
BOOL bRunHeliChase = FALSE
BOOL bPlayedNoAmbushDialogue = FALSE
BOOL bLedPoliceToSite = FALSE
BOOL bIdleAmbusherTrigger = FALSE
BOOL bRunAmbushChase = FALSE
BOOL bRunSmugglerChase = FALSE
BOOL bDrawCorona = FALSE
BOOL bGaveDelayWarning = FALSE
BOOL bPassedOnce = FALSE
BOOL bPrintHelpForOtherVehicle = FALSE
BOOL bInterrupt = FALSE
BOOL bPrintedObjectiveToLoseRunners = FALSE
BOOL bNoAmbushPrintObjective = FALSE
BOOL bPrintedLoseCopsAtEnd = FALSE
BOOL bPrintedLoseAmbushersAtEnd = FALSE
BOOL bPlayedWantedDialogue = FALSE
BOOL bOkayToBlipObjective = FALSE
BOOL bSkippedEndingScreen = FALSE
BOOL bDoneRenderingEndScreen = FALSE

VECTOR vCurDestObjective[MAX_NUMBER_DROPS]
VECTOR vQuickCamPosOffset = <<3, -10.0, 1.5>> 
VECTOR vQuickCamRotOffset = <<0, 0, 0>>
VECTOR vCamPosition, vCamRotation
VECTOR vGroundEndingPos = <<2134.7585, 4789.8989, 39.9702>> 
VECTOR vSpawnPosition = <<2149.7371, 4798.4717, 40.1071>>

FLOAT fHeadingDelta
FLOAT fGroundEndingHeading = 24.9014
FLOAT fMissionTime
FLOAT fPlayerSpeed
FLOAT fHealth = 100
FLOAT fCurDist = -1
FLOAT fTimeToBlip
FLOAT fTimeToBlipRivals
FLOAT fSpawnHeading = 55.4847

INT startTime
INT outOfVehicleTime = 0
INT iAllowSkipCutsceneTime = 0
INT iRandInt
INT iStageToUse
INT iSmugglersCinematicStages = 0
INT iTypeOneCinmaticStages = 0

STRING sWeapon

// Peds
PED_INDEX badGuyPed[maxNumBadGuys]

// Vehicles
VEHICLE_INDEX myVehicle
VEHICLE_INDEX myEntities[MAX_NUMBER_DROPS]

// Blips
BLIP_INDEX myVehicleBlip
BLIP_INDEX myLocationBlip[MAX_NUMBER_DROPS]
//BLIP_INDEX badGuyBlips[maxNumBadGuys]
//BLIP_INDEX badGuyVehicleBlips[4]

// Cameras
CAMERA_INDEX camQuickPlaneCam
CAMERA_INDEX myCamera
CAMERA_INDEX transitionCam

SCENARIO_BLOCKING_INDEX sbi01, sbi02, sbi03, sbi04

TEXT_LABEL_23 TestResumptionLabel

// Timers
structTimer stMissionTimer
structTimer tQuickCam
structTimer	myTimer
structTimer stLoadSceneTimer
structTimer tOnRoadTimer
structTimer tNoAmbushTimer
structTimer tIdleTimer
structTimer tDelayReturnTimer
structTimer tAbandonPackageTimer
structTimer tEndChaseTimer

// Model Names
MODEL_NAMES mnVehicleToUse
MODEL_NAMES mnPedToUse

//CustomCutsceneCaller fpEndingCutscene01

// Misc
WEAPON_TYPE weaponInHand

SIMPLE_USE_CONTEXT cucEndScreen

#IF IS_DEBUG_BUILD
	BOOL pSkipping = FALSE
#ENDIF

#IF IS_DEBUG_BUILD

      //Necessary data for adjusting objects via RAG widgets
      STRUCT DEBUG_POSITION_DATA
            BOOL bTurnOnPositioning
            BOOL bUseAttachWidgets
            VECTOR vDebugVector
            FLOAT fRotateX
            FLOAT fRotateY
            FLOAT fRotateZ
            FLOAT fOffsetX
            FLOAT fOffsetY
            FLOAT fOffsetZ
      ENDSTRUCT
      DEBUG_POSITION_DATA myDebugData
      
      //need to call this once at the start of the script to add the
      PROC BEAT_ADD_WIDGETS()
            START_WIDGET_GROUP("BEAT WIDGETS")
                  ADD_WIDGET_BOOL("Turn On Positioning Widgets", myDebugData.bTurnOnPositioning)
                  ADD_WIDGET_BOOL("Use Attach", myDebugData.bUseAttachWidgets)
                  ADD_WIDGET_FLOAT_SLIDER("RotateX", myDebugData.fRotateX, -359, 359, 2.5)
                  ADD_WIDGET_FLOAT_SLIDER("RotateY", myDebugData.fRotateY, -359, 359, 2.5)
                  ADD_WIDGET_FLOAT_SLIDER("RotateZ", myDebugData.fRotateZ, -359, 359, 2.5)
                  ADD_WIDGET_FLOAT_SLIDER("OffsetX", myDebugData.fOffsetX, -100, 100, 0.05)
                  ADD_WIDGET_FLOAT_SLIDER("OffsetY", myDebugData.fOffsetY, -100, 100, 0.05)
                  ADD_WIDGET_FLOAT_SLIDER("OffsetZ", myDebugData.fOffsetZ, -100, 100, 0.05)
            STOP_WIDGET_GROUP()
      ENDPROC
	  
	  PROC BEAT_UPDATE_WIDGETS_FOR_OBJECT(OBJECT_INDEX entityFirst, VECTOR vCenterPoint, FLOAT fCenterHeading, ENTITY_INDEX entitySecond = NULL, INT iBoneIndexSecond = 0)
      
	    IF myDebugData.bTurnOnPositioning
	      IF NOT myDebugData.bUseAttachWidgets
	        IF DOES_ENTITY_EXIST(entityFirst)
	         	VECTOR vOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCenterPoint, fCenterHeading, << myDebugData.fOffsetX, myDebugData.fOffsetY, myDebugData.fOffsetZ >>)
	         	SET_ENTITY_COORDS(entityFirst, vOffset)
	         	SET_ENTITY_ROTATION(entityFirst, << myDebugData.fRotateX, myDebugData.fRotateY, myDebugData.fRotateZ >>)
	        ENDIF
			
	      ELSE
	        IF DOES_ENTITY_EXIST(entityFirst) AND DOES_ENTITY_EXIST(entitySecond)
	        	ATTACH_ENTITY_TO_ENTITY(entityFirst, entitySecond, iBoneIndexSecond, << myDebugData.fOffsetX, myDebugData.fOffsetY, myDebugData.fOffsetZ >>, << myDebugData.fRotateX, myDebugData.fRotateY, myDebugData.fRotateZ >> )
	        ENDIF
	      ENDIF
	    ENDIF
  	ENDPROC
#ENDIF

// cleanup the mission
PROC MISSION_CLEANUP()
	INT iStatsTime

	// cleanup anything you need to. You don't need to dereference script-created entities like peds and vehicles,
	// as code will do this automatically when the script terminates.
	
	IF DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
		SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_UNLOCKED)
		PRINTLN("MAKING SURE DOORS ARE UNLOCKED ON - myVehicle")
	ENDIF
	
	SET_SCENARIO_GROUP_ENABLED("PRISON_TOWERS", FALSE) 
	PRINTLN("DISABLING PRISON TOWERS")
	
	CLEAR_AREA_OF_PROJECTILES(vGroundEndingPos, 150.0)
	
	REMOVE_NAVMESH_REQUIRED_REGIONS()
	
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	iStatsTime = FLOOR(fMissionTime)
	PRINTLN("iStatsTime = ", iStatsTime)
	
	//set rich presence back to default.
	SET_DEFAULT_RICH_PRESENCE_FOR_SP()
	
	PLAYSTATS_ODDJOB_DONE(iStatsTime, ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND))
	PRINTLN("CALLING PLAYSTATS_ODDJOB_DONE - MINIGAME_TRAFFICKING_GROUND")
	
	SET_ROADS_BACK_TO_ORIGINAL(<< 1651.576, 4939.412, 41.818 >>, << 1721.456, 4540.486, 41.818 >>)
	SET_ROADS_BACK_TO_ORIGINAL(<< 2499.300, 3456.401, -49.795>>, << 2424.300, 3456.401, 49.795>>)
	SET_ROADS_BACK_TO_ORIGINAL(<< 1751.693, 4562.989, -57.869 >>, << 769.406, 4375.608, 57.869 >>)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<2164.037842,4825.626953,35.613750>>, <<1899.277832,4697.752441,49.086414>>, 100.000000)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<1757.214600,4570.540039,30.477287>>, <<1385.558228,4495.363770,62.740479>>, 100.000000)
	
	// Re-setting wanted multiplier back to 1.0
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_MAX_WANTED_LEVEL(5)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
	
	SET_TIME_SCALE(1.0)
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	
	IF NOT bPassed
		IF DOES_ENTITY_EXIST(myVehicle)
			SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
			
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(myVehicle, vSpawnPosition) < 50
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vGroundEndingPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fGroundEndingHeading)
					DELETE_VEHICLE(myVehicle)
					PRINTLN("DELETING GROUND VEHICLE - TOO CLOSE TO SPAWN POSITION")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	IF NOT IS_ENTITY_DEAD(myVehicle)
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(myVehicle)
//		PRINTLN("SETTING myVehicle AS NO LONGER NEEDED")
//	ENDIF
	
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		// only delete the vehicle if the player isn't in it
//		SAFE_DELETE_VEHICLE(myVehicle)
//	ELSE
//		PRINTLN("PLAYER INJURED IN CLEANUP")
//	ENDIF

	IF bPassed = TRUE
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
		ENDIF
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		DISPLAY_RADAR(TRUE)
	ENDIF
	
	DISABLE_CELLPHONE(FALSE)
	
	REMOVE_VEHICLE_RECORDING(101, "GroundTaxi")
	
	CLEAR_GPS_CUSTOM_ROUTE()
	
	REMOVE_ROAD_NODE_SPEED_ZONE(hijackArgs.iSpeedNode)
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbi01)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi02)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi03)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi04)
	
	PRINTLN("TERMINATING Traffic_Ground.sc")
	TERMINATE_THIS_THREAD() // important, kills the script
ENDPROC

FUNC VARIATION GET_VARIATION_TYPE()
	VARIATION enumType
	
//	PRINTLN("g_savedGlobals.sTraffickingData.iGroundRank = ", g_savedGlobals.sTraffickingData.iGroundRank)
	
	IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type1)
		enumType = TYPE_1
//		PRINTLN("RETURNING TYPE 1")
	ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_Type2)
		enumType = TYPE_2
//		PRINTLN("RETURNING TYPE 2")
	ENDIF
	
	RETURN enumType
ENDFUNC

PROC DELETE_CUTSCENE_CARGO()
	INT idx 
	
	REPEAT COUNT_OF(drugCargoArgs.oCutsceneCargo) idx
		IF DOES_ENTITY_EXIST(drugCargoArgs.oCutsceneCargo[idx])
			DELETE_OBJECT(drugCargoArgs.oCutsceneCargo[idx])
			DEBUG_PRINT("DELETING CUTSCENE CARGO")
		ENDIF
	ENDREPEAT
ENDPROC

PROC HANDLE_TAXI_DIALOGUE()

	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
	CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", myLocData.dropData[0].sConvo, CONV_PRIORITY_VERY_HIGH)
	
ENDPROC

PROC HANDLE_PRINTS(TEXT_LABEL_63 sObjective, STRING sHelp)
	IF IS_STRING_NULL_OR_EMPTY(sObjective)
		PRINT("DTRFKGR_02", DEFAULT_GOD_TEXT_TIME, 1)
		PRINTLN("STRING IS NULL - USING DEFAULT")
	ELSE
		PRINT(sObjective, DEFAULT_GOD_TEXT_TIME, 1)
		PRINTLN("STRING IS GOOD - USING SPECIFIC STRING")
	ENDIF
	sLastObjective = sObjective
	REGISTER_PRINT_DELAY(printTimer, sHelp)
ENDPROC

PROC PRINT_FIRST_OBJECTIVES_AND_HELP()
		
	IF GET_VARIATION_TYPE() = TYPE_1
		HANDLE_PRINTS(myLocData.dropData[0].sObjective, "DTRFKGR_HELP_02")
		DEBUG_PRINT("PRINTING OBJECTIVE - LEVEL 1")
	ELSE
		HANDLE_PRINTS(myLocData.dropData[0].sObjective, "DTRFKGR_HELP_03")
		DEBUG_PRINT("PRINTING OBJECTIVE - LEVEL 2")
	ENDIF
	
ENDPROC

FUNC STRING PRINT_FIRST_HELP()

	STRING sHelpPrint

	IF GET_VARIATION_TYPE() = TYPE_1
		sHelpPrint = "DTRFKGR_HELP_07"
	ELSE
		sHelpPrint = "DTRFKGR_HELP_03"
	ENDIF
	
	RETURN sHelpPrint
ENDFUNC

// common stuff for cutscene skip
PROC HANDLE_SKIP_CUTSCENE_GROUND()
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
	AND GET_GAME_TIMER() >= iAllowSkipCutsceneTime + 1000
		DEBUG_PRINT("SKIPPED CUTSCENE")
		DO_SCREEN_FADE_OUT(500)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
		
		CLEAR_PRINTS()
		HANG_UP_AND_PUT_AWAY_PHONE()
		KILL_ANY_CONVERSATION()
		bSkippedCutscene = TRUE
		
		// Ending cargo unloading cutscene
		IF bEndingCutscene
			PRINTLN("SETTING STATE - ENDING_CUTSCENE_SKIP_01")
			endingCutsceneStages = ENDING_CUTSCENE_SKIP_01
		ENDIF
		
		// Smuggler's cutscenes
		IF bSmugglersRetrieveCutscene
			smugglerRetrieveCutState = SMUGGLERS_RETRIEVE_SKIP_01
		ENDIF
		IF bSmugglersEndingCutscene
			smugglerEndingStage = SMUGGLERS_ENDING_SKIP_01
		ENDIF
	ENDIF
ENDPROC

PROC GIVE_PLAYER_WEAPONS()
	INT iStickyBombAmmo, iPistolAmmo, iSMGAmmo

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB)
			PRINTLN("PLAYER HAS STICKYBOMB")
			iStickyBombAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB)
			PRINTLN("NUMBER OF STICKYBOMBS = ", iStickyBombAmmo)
			IF iStickyBombAmmo < 5
				SET_PED_AMMO(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 5)
				PRINTLN("GIVING PLAYER STICKYBOMB 01")
			ENDIF
		ELSE
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 5)
			PRINTLN("GIVING PLAYER STICKYBOMB 02")
		ENDIF
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
			PRINTLN("PLAYER HAS WEAPONTYPE_PISTOL")
			iPistolAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
			PRINTLN("NUMBER OF PISTOL BULLETS = ", iPistolAmmo)
			IF iPistolAmmo < 100
				SET_PED_AMMO(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 100)
				PRINTLN("GIVING PLAYER WEAPONTYPE_PISTOL 01")
			ENDIF
		ELSE
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 100)
			PRINTLN("GIVING PLAYER WEAPONTYPE_PISTOL 02")
		ENDIF
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG)
			PRINTLN("PLAYER HAS WEAPONTYPE_MICROSMG")
			iSMGAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG)
			PRINTLN("NUMBER OF MICROSMG BULLETS = ", iSMGAmmo)
			IF iSMGAmmo < 100
				SET_PED_AMMO(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG, 100)
				PRINTLN("GIVING PLAYER WEAPONTYPE_MICROSMG 01")
			ENDIF
		ELSE
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG, 100)
			PRINTLN("GIVING PLAYER WEAPONTYPE_SMG 02")
		ENDIF
	ENDIF
ENDPROC

PROC AWARD_PLAYER_WEAPONS(WEAPON_TYPE wtWeapon, INT iDesiredAmmoCount)
	INT iAmmoCount

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), wtWeapon)
			PRINTLN("PLAYER HAS WEAPON ALREADY")
			iAmmoCount = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtWeapon)
			PRINTLN("NUMBER OF AMMO 01 = ", iAmmoCount)
//			IF iAmmoCount < iDesiredAmmoCount
				SET_PED_AMMO(PLAYER_PED_ID(), wtWeapon, (iDesiredAmmoCount + iAmmoCount))
				PRINTLN("GIVING PLAYER WEAPON WITH AMMO")
//			ENDIF
		ELSE
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), wtWeapon, 0)
		
			iAmmoCount = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtWeapon)
			PRINTLN("NUMBER OF AMMO 02 = ", iAmmoCount)
			
			IF iAmmoCount < iDesiredAmmoCount
				SET_PED_AMMO(PLAYER_PED_ID(), wtWeapon, iDesiredAmmoCount)
			ENDIF
			
			PRINTLN("GIVING PLAYER WEAPON")
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_WEAPON_AWARDED()
	IF g_savedGlobals.sTraffickingData.iGroundRank = 0
		sWeapon = "DTRSHRD_WEAP01"
		AWARD_PLAYER_WEAPONS(WEAPONTYPE_PISTOL, 100)
	ELIF g_savedGlobals.sTraffickingData.iGroundRank = 1
		sWeapon = "DTRSHRD_WEAP02"
		AWARD_PLAYER_WEAPONS(WEAPONTYPE_MICROSMG, 100)
	ELIF g_savedGlobals.sTraffickingData.iGroundRank = 2
		sWeapon = "DTRSHRD_WEAP03"
		AWARD_PLAYER_WEAPONS(WEAPONTYPE_GRENADE, 5)
	ELIF g_savedGlobals.sTraffickingData.iGroundRank = 3
		sWeapon = "DTRSHRD_WEAP04"
		AWARD_PLAYER_WEAPONS(WEAPONTYPE_PUMPSHOTGUN, 40)
	ELIF g_savedGlobals.sTraffickingData.iGroundRank = 4
		sWeapon = "DTRSHRD_WEAP05"
		AWARD_PLAYER_WEAPONS(WEAPONTYPE_ASSAULTRIFLE, 5)
	ELIF g_savedGlobals.sTraffickingData.iGroundRank = 5
		
		SWITCH g_savedGlobals.sTraffickingData.iNumRepeatComplete
			CASE 0
				sWeapon = "DTRSHRD_WEAP01"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_PISTOL, 100)
			BREAK
			CASE 1
				sWeapon = "DTRSHRD_WEAP02"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_MICROSMG, 100)
			BREAK
			CASE 2
				sWeapon = "DTRSHRD_WEAP03"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_GRENADE, 5)
			BREAK
			CASE 3
				sWeapon = "DTRSHRD_WEAP04"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_PUMPSHOTGUN, 40)
			BREAK
			CASE 4
				sWeapon = "DTRSHRD_WEAP05"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_ASSAULTRIFLE, 5)
			BREAK
			CASE 5
				sWeapon = "DTRSHRD_WEAP01"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_PISTOL, 100)
			BREAK
			CASE 6
				sWeapon = "DTRSHRD_WEAP02"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_MICROSMG, 100)
			BREAK
			CASE 7
				sWeapon = "DTRSHRD_WEAP03"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_GRENADE, 5)
			BREAK
			CASE 8
				sWeapon = "DTRSHRD_WEAP04"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_PUMPSHOTGUN, 40)
			BREAK
			CASE 9
				sWeapon = "DTRSHRD_WEAP05"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_ASSAULTRIFLE, 5)
			BREAK
			CASE 10
				sWeapon = "DTRSHRD_WEAP01"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_PISTOL, 100)
			BREAK
			CASE 11
				sWeapon = "DTRSHRD_WEAP02"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_MICROSMG, 100)
			BREAK
			CASE 12
				sWeapon = "DTRSHRD_WEAP03"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_GRENADE, 5)
			BREAK
			CASE 13
				sWeapon = "DTRSHRD_WEAP04"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_PUMPSHOTGUN, 40)
			BREAK
			CASE 14
				sWeapon = "DTRSHRD_WEAP05"
				AWARD_PLAYER_WEAPONS(WEAPONTYPE_ASSAULTRIFLE, 5)
			BREAK
		ENDSWITCH
	ENDIF
		
	RETURN sWeapon
ENDFUNC

// mission passed
PROC MISSION_PASSED()
	
	IF NOT bPassedOnce 
		// To Fix Bug # 504501, for some reason -1 was being passed into myArgsCopy.completionEntry.
		myArgsCopy.completionEntry = myArgsCopy.completionEntry
		
		SWITCH g_savedGlobals.sTraffickingData.iGroundRank
			CASE 0
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_OJ_DTG1)
				PRINTLN("REGISTERING COMPLETION PERCENTAGE FOR COMPLETING GROUND TRAFFICKING 1")
			BREAK
			CASE 1
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_OJ_DTG2)
				PRINTLN("REGISTERING COMPLETION PERCENTAGE FOR COMPLETING GROUND TRAFFICKING 2")
			BREAK
			CASE 2
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_OJ_DTG3)
				PRINTLN("REGISTERING COMPLETION PERCENTAGE FOR COMPLETING GROUND TRAFFICKING 3")
			BREAK
			CASE 3
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_OJ_DTG4)
				PRINTLN("REGISTERING COMPLETION PERCENTAGE FOR COMPLETING GROUND TRAFFICKING 4")
			BREAK
			CASE 4
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_OJ_DTG5)
				PRINTLN("REGISTERING COMPLETION PERCENTAGE FOR COMPLETING GROUND TRAFFICKING 5")
			BREAK
		ENDSWITCH
		
		IF g_savedGlobals.sTraffickingData.iGroundRank < 5
			g_savedGlobals.sTraffickingData.iGroundRank++
			PRINTLN("GROUND RANK NOW = ", g_savedGlobals.sTraffickingData.iGroundRank)
		ELIF g_savedGlobals.sTraffickingData.iGroundRank >= 5
			g_savedGlobals.sTraffickingData.iGroundRank = 5
			PRINTLN("COMPLETED CYCLE, GROUND RANK = ", g_savedGlobals.sTraffickingData.iGroundRank)
			
			g_savedGlobals.sTraffickingData.iNumRepeatComplete++
			PRINTLN("NUMBER OF REPEATS COMPLETED = ", g_savedGlobals.sTraffickingData.iNumRepeatComplete)
			

		ENDIF
		
		//Check if the trafficking total stat has changed
		int iCurrStatVal
		int iNewStatVal = g_savedGlobals.sTraffickingData.iAirRank + g_savedGlobals.sTraffickingData.iGroundRank
		STAT_GET_INT(NUM_TRAFFICKING_COMPLETED, iCurrStatVal)
//		CPRINTLN(debug_dan,"Current/new trafficking values: ",iCurrStatVal,"/",iNewStatVal)
		IF icurrStatVal < iNewstatVal
			//Update total trafficking count stat
			STAT_SET_INT(NUM_TRAFFICKING_COMPLETED, iNewStatVal)
			//Update achievement progress
			SET_ACHIEVEMENT_PROGRESS_SAFE(ENUM_TO_INT(ACH13), iNewStatVal)
//			CPRINTLN(debug_dan,"Set tp industries achievement to ",iNewStatVal)
		ENDIF
	
		CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]: ACH13 - TP Industries Arms Race - ", g_savedGlobals.sTraffickingData.iAirRank + g_savedGlobals.sTraffickingData.iGroundRank, " of ", ACH13_AIR_TOTAL + ACH13_GND_TOTAL)
		IF (g_savedGlobals.sTraffickingData.iAirRank >= ACH13_AIR_TOTAL) AND (g_savedGlobals.sTraffickingData.iGroundRank >= ACH13_GND_TOTAL)
			AWARD_ACHIEVEMENT(ACH13) // TP Industries Arms Race
		ENDIF
			
		IF NOT bRunOnce
			IF bSkippedMission

				// Using myArgs.fTimeLimit as a way to determine if we're in repeatable mode.
				fMissionTime = GET_TIMER_IN_SECONDS(stMissionTimer)
				PRINTSTRING("TOTAL MISSION TIME = ")
				PRINTFLOAT(fMissionTime)
				PRINTNL()
				
				IF bInRepeatableMode
					PRINTLN("S-SKIP: IN REPEATABLE MODE")
					
					SET_ENDSCREEN_DATASET_HEADER(groundEndScreen.groundTraffickingEndScreen, "DTRSHRD_MPASS", "DTRSHRD_GRT")
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(groundEndScreen.groundTraffickingEndScreen, ESEF_TIME_M_S, "DTRSHRD_TIME", "", FLOOR(fMissionTime), 0, ESCM_NO_MARK)
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(groundEndScreen.groundTraffickingEndScreen, ESEF_DOLLAR_VALUE, "DTRSHRD_MONEY", "", 5000, 0, ESCM_NO_MARK)
				ELSE
					PRINTLN("IN REGULAR MODE")
					
					sWeapon = GET_WEAPON_AWARDED()
					
					SET_ENDSCREEN_DATASET_HEADER(groundEndScreen.groundTraffickingEndScreen, "DTRSHRD_MPASS", "DTRSHRD_GRT")
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(groundEndScreen.groundTraffickingEndScreen, ESEF_TIME_M_S, "DTRSHRD_TIME", "", FLOOR(fMissionTime), 0, ESCM_NO_MARK)
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(groundEndScreen.groundTraffickingEndScreen, ESEF_DOLLAR_VALUE, "DTRSHRD_MONEY", "", 5000, 0, ESCM_NO_MARK)
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(groundEndScreen.groundTraffickingEndScreen, ESEF_RAW_STRING, "DTRSHRD_WEAP", sWeapon, 0, 0, ESCM_NO_MARK)
				ENDIF
				
				currentMissionStage = STAGE_ENDING_CUTSCENE
				bRunOnce = TRUE
			ENDIF
		ENDIF
		
		PROPERTY_PAY_INCOME(PROPERTY_ARMS_TRAFFICKING, 5000)
		
		SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)
		ODDJOB_AUTO_SAVE()
		PRINTLN("CALLING AUTOSAVE")
		
		bPassedOnce = TRUE
		PRINTLN("bPassedOnce = TRUE") // To Fix Bug # 1327402
	ENDIF
ENDPROC

PROC REPOSITION_PLAYER_AND_REMOVE_ITEMS()
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vGroundEndingPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fGroundEndingHeading)
		
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(myVehicle)
		SET_ENTITY_AS_MISSION_ENTITY(myVehicle)
	ENDIF
	
	IF bDoHijack
		DELETE_CANISTERS(expPackageArgs)
		IF DOES_ENTITY_EXIST(hijackArgs.oWeaponsCargo)
			DELETE_OBJECT(hijackArgs.oWeaponsCargo)
			DEBUG_PRINT("DELETING hijackArgs.oWeaponsCargo")
		ENDIF
		IF DOES_ENTITY_EXIST(expPackageArgs.oRamp)
			DELETE_OBJECT(expPackageArgs.oRamp)
			PRINTLN("DELETING RAMP")
		ENDIF
		IF DOES_ENTITY_EXIST(expPackageArgs.oRampDoor)
			DELETE_OBJECT(expPackageArgs.oRampDoor)
			PRINTLN("DELETING RAMP DOOR")
		ENDIF
	ENDIF

	DEBUG_PRINT("MISSION ENDING")
	REMOVE_DRUG_CARGO(drugCargoArgs)
	
	IF DOES_ENTITY_EXIST(myVehicle)
		DELETE_VEHICLE(myVehicle)
		DEBUG_PRINT("DELETING myVehicle")
	ENDIF
ENDPROC

FUNC BOOL ENDING_CUTSCENE_NEW()

	FLOAT fTempHeading = 0.0
	FLOAT fPlayerHeading
	VECTOR vPlayerPos

	IF NOT bGrabbedTimeForCutscene
		// Set for skip
		bEndingCutscene = TRUE
		iAllowSkipCutsceneTime = GET_GAME_TIMER()
		bGrabbedTimeForCutscene = TRUE
	ENDIF
	
	//------------------------------------------SKIP CUTSCENE------------------------------------------
	IF bSetTimer AND TIMERA() > 3000
		IF NOT bSkippedCutscene
			HANDLE_SKIP_CUTSCENE_GROUND()
		ENDIF
	ENDIF
	
	SWITCH endingCutsceneStages
		CASE ENDING_CUTSCENE_STATE_01
		
			ODDJOB_ENTER_CUTSCENE(SPC_REMOVE_EXPLOSIONS | SPC_REMOVE_FIRES | SPC_REMOVE_PROJECTILES)
			CLEAR_PRINTS()
			KILL_ANY_CONVERSATION()
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			REMOVE_BLIP(myLocationBlip[0])
			
			TRIGGER_MUSIC_EVENT("OJDG_COMPLETE")
			PRINTLN("STARTING MUSIC - OJDG_COMPLETE")
			
			IF NOT IS_TIMER_STARTED(cutArgs.cutSceneTimer)
				START_TIMER_NOW(cutArgs.cutSceneTimer)
			ELSE
				RESTART_TIMER_NOW(cutArgs.cutSceneTimer)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				PRINTLN("vPlayerPos = ",vPlayerPos)
			ENDIF
			
			fTempHeading = GET_HEADING_BETWEEN_VECTORS(vPlayerPos, vGroundEndingPos)
			PRINTLN("fTempHeading = ", fTempHeading)
			
			fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
			PRINTLN("fPlayerHeading = ", fPlayerHeading)
			
			fHeadingDelta = fPlayerHeading - fTempHeading 
			PRINTLN("BEFORE: fHeadingDelta = ", fHeadingDelta)
	
			IF fHeadingDelta > 180
				fHeadingDelta -= 360
			ELIF fHeadingDelta < -180
				fHeadingDelta += 360
			ENDIF
			
			PRINTLN("AFTER: fHeadingDelta = ", fHeadingDelta)
		
			bIsCutsceneActive = TRUE
			DEBUG_PRINT("bIsCutsceneActive = TRUE")
			
			vCamPosition = GET_GAMEPLAY_CAM_COORD()
			PRINTLN("BEFORE: vCamPosition = ", vCamPosition)
			
			vCamRotation = GET_GAMEPLAY_CAM_ROT()
			PRINTLN("vCamRotation = ", vCamRotation)
			
			vCamPosition.z += 20.0 //25m above the player.
			PRINTLN("AFTER: vCamPosition = ", vCamPosition)
			
			IF fHeadingDelta < 0
				vCamRotation.z -= 40
			ELSE
				vCamRotation.z += 40
			ENDIF
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 2146.438, 4785.841, -40.970 >>, << 2123.817, 4775.197, 50.970 >>, 23.000)
				PRINTLN("PLAYER IS IN ANGLED AREA")
				bUseInterp = FALSE
			ELSE
				PRINTLN("PLAYER IS NOT IN ANGLED AREA")
				bUseInterp = TRUE
			ENDIF
			
			IF NOT DOES_CAM_EXIST(myCamera)
				myCamera = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vCamPosition, vCamRotation, 45, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, bUseInterp, 6500)
				SHAKE_CAM(myCamera, "HAND_SHAKE", 0.1)
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), myVehicle)
				ENDIF
								
				CLEAR_HELP()
				
				IF bUseInterp
					DEBUG_PRINT("GOING TO STATE - ENDING_CUTSCENE_STATE_02")
					endingCutsceneStages = ENDING_CUTSCENE_STATE_02
				ELSE
					DEBUG_PRINT("bUseInterp IS TRUE: GOING TO STATE - ENDING_CUTSCENE_STATE_03")
					endingCutsceneStages = ENDING_CUTSCENE_STATE_03
				ENDIF
			ENDIF
		BREAK
		//======================================================================================================================================
		CASE ENDING_CUTSCENE_STATE_02
			IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 5)
				IF fHeadingDelta < 0
					vCamRotation.z -= 20
				ELSE
					vCamRotation.z += 20
				ENDIF
				transitionCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vCamPosition, vCamRotation, 45, TRUE)
				SET_CAM_ACTIVE_WITH_INTERP(transitionCam, myCamera, 15000, GRAPH_TYPE_SIN_ACCEL_DECEL)
				SHAKE_CAM(myCamera, "HAND_SHAKE", 0.1)
				RESTART_TIMER_NOW(cutArgs.cutSceneTimer)
				
				// Setting player coordinates following cutscene
				IF NOT IS_ENTITY_ON_SCREEN(PLAYER_PED_ID()) AND IS_ENTITY_OCCLUDED(PLAYER_PED_ID())
					REPOSITION_PLAYER_AND_REMOVE_ITEMS()
					PRINTLN("REPOSITIONING PLAYER EARLY")
				ELSE
					bPlayerVisible = TRUE
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vGroundEndingPos, PEDMOVE_WALK)
					PRINTLN("PLAYER VISIBLE, TASKING GO TO COORDS")
				ENDIF
			
				DEBUG_PRINT("GOING TO STATE - ENDING_CUTSCENE_STATE_03")
				endingCutsceneStages = ENDING_CUTSCENE_STATE_03
			ENDIF
		BREAK
		//======================================================================================================================================
		CASE ENDING_CUTSCENE_STATE_03
		
			IF ENDSCREEN_PREPARE(groundEndScreen.groundTraffickingEndScreen)
			
				IF NOT bPlayMusic
					MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
					bPlayMusic = TRUE
				ENDIF
			
				DEBUG_PRINT("GOING TO STATE - ENDING_CUTSCENE_STATE_04")
				endingCutsceneStages = ENDING_CUTSCENE_STATE_04
			ENDIF
		BREAK
		//======================================================================================================================================
		CASE ENDING_CUTSCENE_STATE_04
		
			IF NOT bSetTimer
				SETTIMERA(0)
				bSetTimer = TRUE
			ENDIF
		
			IF RENDER_ENDSCREEN(groundEndScreen.groundTraffickingEndScreen)
				PRINTLN("RENDERING ENDSCREEN")
			ENDIF
			
			IF TIMERA() > 5000
			
				ENDSCREEN_SHUTDOWN(groundEndScreen.groundTraffickingEndScreen)
			
				IF IS_CAM_SHAKING(myCamera)
					STOP_CAM_SHAKING(myCamera)
					PRINTLN("STOP CAM, myCamera, FROM SHAKING")
				ENDIF
				
				IF bPlayerVisible
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					PRINTLN("REPOSITIONING PLAYER LATE")
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK)
				ENDIF
				
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				DESTROY_CAM(transitionCam)
				ODDJOB_EXIT_CUTSCENE()
				bIsCutsceneActive = FALSE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				RETURN TRUE
			ENDIF
		BREAK
		//======================================================================================================================================
		CASE ENDING_CUTSCENE_SKIP_01
			DESTROY_CAM(myCamera)
			DESTROY_CAM(transitionCam)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			// Setting player coordinates following cutscene
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vGroundEndingPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fGroundEndingHeading)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK)
			ENDIF
			
			IF bDoHijack
				DELETE_CANISTERS(expPackageArgs)
			ENDIF
			
			IF DOES_ENTITY_EXIST(hijackArgs.oWeaponsCargo)
				DELETE_OBJECT(hijackArgs.oWeaponsCargo)
				DEBUG_PRINT("DELETING hijackArgs.oWeaponsCargo")
			ENDIF
			
			DELETE_CUTSCENE_CARGO()
			
			DEBUG_PRINT("MISSION ENDING VIA SKIP")
			REMOVE_DRUG_CARGO(drugCargoArgs)
			
			IF DOES_ENTITY_EXIST(expPackageArgs.oRamp)
				DELETE_OBJECT(expPackageArgs.oRamp)
				PRINTLN("DELETING RAMP")
			ENDIF
			IF DOES_ENTITY_EXIST(expPackageArgs.oRampDoor)
				DELETE_OBJECT(expPackageArgs.oRampDoor)
				PRINTLN("DELETING RAMP DOOR")
			ENDIF
			
			IF DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
				DELETE_VEHICLE(myVehicle)
				DEBUG_PRINT("DELETING myVehicle VIA SKIP")
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			
			endingCutsceneStages = ENDING_CUTSCENE_SKIP_02
		BREAK
		//======================================================================================================================================
		CASE ENDING_CUTSCENE_SKIP_02
			DO_SCREEN_FADE_IN(1000)
			WHILE NOT IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
			
			DEBUG_PRINT("RETURNING TRUE IN STATE- ENDING_CUTSCENE_SKIP_02")
			RETURN TRUE
			
//			IF NOT bLaunchedMissionEndingScreen
//				IF LAUNCH_MINIGAME_MISSION_TRACKER()
//					MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
//					DEBUG_PRINT("RETURNING TRUE IN STATE- ENDING_CUTSCENE_SKIP_02 WITHOUT ENDING SCREEN")
//					RETURN TRUE
//				ENDIF
//			ELSE 
//				DEBUG_PRINT("RETURNING TRUE IN STATE- ENDING_CUTSCENE_SKIP_02")
//				RETURN TRUE
//			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SMUGGLERS_RETRIEVE_CUTSCENE_NEW()

	INT iInterpTime = 3000
	VECTOR vPlayerVehiclePos
	FLOAT fPlayerVehicleHeading
	VECTOR vPlayerPositionNearCar
	FLOAT fGroundZ
	VECTOR vCamPos01 = <<1.9821, -3.7360, 0.8606>>
	VECTOR vCamRot01 = <<0.7863, -1.0294, 0.3664>>
	VECTOR vCamPos02 = <<1.8626, -2.6789, 0.6921>>
	VECTOR vCamRot02 = <<0.6378, 0.0387, 0.3551>>
	FLOAT fSmugglersHeading, fHeadingToUse
	VECTOR vFront, vSide, vUp, vPosition
	VECTOR vPlayerCoord, vSmugglerRealCoord, vSmugglerCoord
	
	IF NOT bGrabbedTimeForCutscene
		bSmugglersRetrieveCutscene = TRUE
		iAllowSkipCutsceneTime = GET_GAME_TIMER()
		bGrabbedTimeForCutscene = TRUE
	ENDIF
	
	IF NOT IS_TIMER_STARTED(cutArgs.cutSceneTimer)
		START_TIMER_NOW(cutArgs.cutSceneTimer)
	ENDIF
	
	//------------------------------------------SKIP CUTSCENE------------------------------------------
	IF NOT bSkippedCutscene
		HANDLE_SKIP_CUTSCENE_GROUND()
	ENDIF
	
	// ------------------------------------------FAIL SAFE------------------------------------------
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
		IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 10.0)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
			DEBUG_PRINT("TRIGGERING FAIL SAFE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	SWITCH smugglerRetrieveCutState
		CASE SMUGGLERS_RETRIEVE_STATE_01
		
			SET_TIME_SCALE(0.8)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weaponInHand)
			ENDIF
		
			// Remove from AI 
			REMOVE_DRUG_CARGO(drugCargoArgs)
			// Add to player's vehicle
			ADD_DRUGS_TO_VEHICLE(drugCargoArgs, myLocData, myVehicle)
			
			CLEAR_PRINTS()
			CLEAR_HELP()
			ODDJOB_ENTER_CUTSCENE()
			CLEAR_AREA(smugArgs.vVehicleNodePos, 10.0, TRUE)
			
			IF IS_VEHICLE_DRIVEABLE(myVehicle)
				IF IS_ENTITY_AT_ENTITY(smugArgs.smugglersVehicles[0], myVehicle, <<1.5,1.5,1.5>>)
					SET_ENTITY_VISIBLE(smugArgs.smugglersVehicles[0], FALSE)
					SET_ENTITY_COLLISION(smugArgs.smugglersVehicles[0], FALSE)
					PRINTLN("TURNING OFF VISIBILITY AND COLLISION ON SMUGGLER VEHICLE")
				ELSE
					SET_ENTITY_COORDS(smugArgs.smugglersVehicles[0], smugArgs.vSafeCarPos)
					SET_ENTITY_HEADING(smugArgs.smugglersVehicles[0], smugArgs.fDefeatedSmugglerVehHeading)
					SET_VEHICLE_ON_GROUND_PROPERLY(smugArgs.smugglersVehicles[0])
				ENDIF
			ENDIF
			
			// Move Car
			IF IS_VEHICLE_DRIVEABLE(myVehicle)
				// Set positions on player's vehicle and smugglers vehicle
				SET_ENTITY_COORDS(myVehicle, smugArgs.vVehicleNodePos)
				// Set player vehicle settings
				
				IF DT_IS_CAR_IN_FRONT_OF_CAR(myVehicle, smugArgs.smugglersVehicles[0])
					PRINTLN("PLAYER'S CAR IS IN FRONT OF SMUGGLER'S CAR")
					fSmugglersHeading = GET_ENTITY_HEADING(smugArgs.smugglersVehicles[0])
					PRINTLN("FRONT: fSmugglersHeading = ", fSmugglersHeading)
					fHeadingToUse = fSmugglersHeading - 180
					PRINTLN("FRONT: fHeadingToUse = ", fHeadingToUse)
//					IF fHeadingToUse < 0
//						fHeadingToUse += 360
//						PRINTLN("FRONT: fHeadingToUse NEW = ", fHeadingToUse)
//					ENDIF
				ELSE
					fSmugglersHeading = GET_ENTITY_HEADING(smugArgs.smugglersVehicles[0])
					PRINTLN("BEHIND: fSmugglersHeading = ", fSmugglersHeading)
					fHeadingToUse = fSmugglersHeading
					PRINTLN("BEHIND: fHeadingToUse = ", fHeadingToUse)
				ENDIF
				
				SET_ENTITY_HEADING(myVehicle, fHeadingToUse)
				SET_VEHICLE_ON_GROUND_PROPERLY(myVehicle)
			ENDIF
			
			// Need to test if the smuggler's vehicle is offscreen, if so we'll need to point at it.
			IF IS_VEHICLE_DRIVEABLE(myVehicle)
				GET_ENTITY_MATRIX(myVehicle, vFront, vSide, vUp, vPosition)
				vPlayerCoord = GET_ENTITY_COORDS(myVehicle)
			ENDIF
			
			vSmugglerRealCoord = GET_ENTITY_COORDS(smugArgs.smugglersVehicles[0])
			vSmugglerCoord =  vSmugglerRealCoord - vPlayerCoord
			
			IF NOT IS_VECTOR_ZERO(vSide)
				IF DOT_PRODUCT(vSide, vSmugglerCoord) < 0
					smugArgs.bNeedToPointAtSmugglerVehicle = TRUE
					PRINTLN("smugArgs.bNeedToPointAtSmugglerVehicle = TRUE")
				ELSE
					smugArgs.bNeedToPointAtSmugglerVehicle = FALSE
					PRINTLN("smugArgs.bNeedToPointAtSmugglerVehicle = FALSE")
				ENDIF
			ENDIF
			
			// Move Player
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
				vPlayerVehiclePos = GET_ENTITY_COORDS(myVehicle)
				PRINTLN("vPlayerVehiclePos = ", vPlayerVehiclePos)
				fPlayerVehicleHeading = GET_ENTITY_HEADING(myVehicle)
				PRINTLN("fPlayerVehicleHeading = ", fPlayerVehicleHeading)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					vPlayerPositionNearCar = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerVehiclePos, fPlayerVehicleHeading, <<-1.5,-1.25,0>>)
					
					GET_GROUND_Z_FOR_3D_COORD(vPlayerPositionNearCar, fGroundZ)
					PRINTLN("fGroundZ = ", fGroundZ)
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					ENDIF
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << vPlayerPositionNearCar.x, vPlayerPositionNearCar.y, fGroundZ >>, TRUE, TRUE)
					fPlayerVehicleHeading = GET_ENTITY_HEADING(myVehicle)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerVehicleHeading - 90)
				ENDIF
			ENDIF
			
			// Attach 1st camera to player's vehicle
			myCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED)
			SET_CAM_FOV(myCamera, 35.0)
			SET_CAM_NEAR_DOF(myCamera, 0.5)
			SET_CAM_FAR_DOF(myCamera, 4.0)
			SET_CAM_DOF_STRENGTH(myCamera, 0.5)
//			SET_CAM_USE_SHALLOW_DOF_MODE(myCamera, TRUE)
			SHAKE_CAM(myCamera, "HAND_SHAKE", 0.2)
			ATTACH_CAM_TO_ENTITY(myCamera, myVehicle, vCamPos01)
			
			IF smugArgs.bNeedToPointAtSmugglerVehicle
				POINT_CAM_AT_ENTITY(myCamera, smugArgs.smugglersVehicles[0], vCamRot01)
				PRINTLN("POINTING AT SMUGGLER'S VEHICLE 01")
			ELSE
				POINT_CAM_AT_ENTITY(myCamera, myVehicle, vCamRot01)
			ENDIF
			
			SET_CAM_ACTIVE(myCamera, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			// Attach 2nd camera to player's vehicle
			transitionCam = CREATE_CAMERA(CAMTYPE_SCRIPTED)
			SET_CAM_FOV(transitionCam, 35.0)
			SET_CAM_NEAR_DOF(transitionCam, 0.5)
			SET_CAM_FAR_DOF(transitionCam, 4.0)
			SET_CAM_DOF_STRENGTH(transitionCam, 0.5)
//			SET_CAM_USE_SHALLOW_DOF_MODE(transitionCam, TRUE)
			SHAKE_CAM(transitionCam, "HAND_SHAKE", 0.2)
			
			ATTACH_CAM_TO_ENTITY(transitionCam, myVehicle, vCamPos02)
			IF smugArgs.bNeedToPointAtSmugglerVehicle
				POINT_CAM_AT_ENTITY(transitionCam, smugArgs.smugglersVehicles[0], vCamRot02)
				PRINTLN("POINTING AT SMUGGLER'S VEHICLE 02")
			ELSE
				POINT_CAM_AT_ENTITY(transitionCam, myVehicle, vCamRot02)
			ENDIF
			SET_CAM_ACTIVE_WITH_INTERP(transitionCam, myCamera, iInterpTime, GRAPH_TYPE_SIN_ACCEL_DECEL)
			
			PRINTLN("GOING TO STATE - SMUGGLERS_RETRIEVE_STATE_02")
			smugglerRetrieveCutState = SMUGGLERS_RETRIEVE_STATE_02
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_RETRIEVE_STATE_02
			SET_USE_HI_DOF()
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
				TASK_ENTER_VEHICLE(PLAYER_PED_ID(), myVehicle, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_ENTRY_POINT)
				
				PRINTLN("GOING TO STATE - SMUGGLERS_RETRIEVE_STATE_03")
				smugglerRetrieveCutState = SMUGGLERS_RETRIEVE_STATE_03
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_RETRIEVE_STATE_03
			SET_USE_HI_DOF()
		
			IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 2.5)
				IF IS_CAM_SHAKING(myCamera)
					STOP_CAM_SHAKING(myCamera)
					PRINTLN("STOP CAM, myCamera, FROM SHAKING")
				ENDIF
				IF IS_CAM_SHAKING(transitionCam)
					STOP_CAM_SHAKING(transitionCam)
					PRINTLN("STOP CAM, transitionCam, FROM SHAKING")
				ENDIF
			
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				IF DOES_CAM_EXIST(myCamera)
					DESTROY_CAM(myCamera)
					PRINTLN("DESTROYING CAM - myCamera")
				ENDIF
				
				IF DOES_CAM_EXIST(transitionCam)
					DESTROY_CAM(transitionCam)
					PRINTLN("DESTROYING CAM - transitionCam")
				ENDIF
				
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_TIME_SCALE(1.0)
				
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
					PRINTLN("FAIL SAFE - SETTING PLAYER IN VEHICLE")
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weaponInHand, TRUE)
				ENDIF
				
				DEBUG_PRINT("RETURNING TRUE IN STATE- SMUGGLERS_RETRIEVE_STATE_03 ")
				ODDJOB_EXIT_CUTSCENE()
				RETURN TRUE
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_RETRIEVE_SKIP_01
			DESTROY_CAM(myCamera)
			DESTROY_CAM(transitionCam)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
				ENDIF
			ENDIF
			
			SET_TIME_SCALE(1.0)
			
			DEBUG_PRINT("INSIDE STATE - SMUGGLERS_RETRIEVE_SKIP_01")
			smugglerRetrieveCutState = SMUGGLERS_RETRIEVE_SKIP_02
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_RETRIEVE_SKIP_02
			DO_SCREEN_FADE_IN(1000)
			WHILE NOT IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				
			// Reseting skipped cutscene bool, so you can skip ending cutscenes.
			bSkippedCutscene = FALSE
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weaponInHand, TRUE)
			ENDIF
			
			ODDJOB_EXIT_CUTSCENE()
			DEBUG_PRINT("RETURNING TRUE IN STATE- SMUGGLERS_RETRIEVE_SKIP_02 ")
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SMUGGLERS_ENDING_CUTSCENE()
	
	SEQUENCE_INDEX iSeq
	VECTOR vClosestNode
	
	IF NOT bGrabbedTimeForCutscene
		// Set for skip
		bSmugglersEndingCutscene = TRUE
		iAllowSkipCutsceneTime = GET_GAME_TIMER()
		bGrabbedTimeForCutscene = TRUE
	ENDIF
	
	//------------------------------------------SKIP CUTSCENE------------------------------------------
	IF NOT bSkippedCutscene
		HANDLE_SKIP_CUTSCENE_GROUND()
	ENDIF
	
	//-----------------------------------------------FAIL SAFE-----------------------------------------------
	IF NOT IS_TIMER_STARTED(cutArgs.cutSceneTimer)
		START_TIMER_NOW(cutArgs.cutSceneTimer)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
		IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 12.0)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
			DEBUG_PRINT("TRIGGERING FAIL SAFE")
			DEBUG_PRINT("FAIL SAFE - smugArgs.bSmugglerReachedDest = TRUE")
			smugArgs.bSmugglerReachedDest = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	
	SWITCH smugglerEndingStage
		CASE SMUGGLERS_ENDING_STATE_01
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			POINT_CAM_AT_ENTITY(myCamera, smugArgs.smugglersVehicles[0], <<0,0,0>>)
			
			IF NOT IS_PED_INJURED(smugArgs.smugglersHeliPed) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersHeliVeh)
				SET_PED_INTO_VEHICLE(smugArgs.smugglersHeliPed, smugArgs.smugglersHeliVeh)
			ENDIF
		
			smugglerEndingStage = SMUGGLERS_ENDING_STATE_02
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_ENDING_STATE_02
			// After two seconds cut to the heli
			IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 1.0)
				
				// Point cam at helicopter
				SET_CAM_COORD(myCamera, myLocData.endData[1].vCutscenePos2)
				SET_CAM_ROT(myCamera, myLocData.endData[1].vCutsceneOrien2)
				SET_CAM_FOV(myCamera, 35.0)
				SHAKE_CAM(myCamera,  "ROAD_VIBRATION_SHAKE", 2.0)
				POINT_CAM_AT_ENTITY(myCamera, smugArgs.smugglersHeliVeh, <<0,0,0>>)
				
				smugglerEndingStage = SMUGGLERS_ENDING_STATE_03
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_ENDING_STATE_03
		
			IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 2.0)
				FREEZE_ENTITY_POSITION(smugArgs.smugglersHeliVeh, FALSE)
				
				IF NOT IS_PED_INJURED(smugArgs.smugglersHeliPed) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersHeliVeh)
					CLEAR_SEQUENCE_TASK(iSeq)
					OPEN_SEQUENCE_TASK(iSeq)
						TASK_HELI_MISSION(NULL, smugArgs.smugglersHeliVeh, NULL, NULL, <<myLocData.endData[1].vSmugHeliPos.x, myLocData.endData[1].vSmugHeliPos.y, (myLocData.endData[1].vSmugHeliPos.z + 50.0)>>, MISSION_GOTO, 40.0, -1, 0, 60, 30)
						TASK_HELI_MISSION(NULL, smugArgs.smugglersHeliVeh, NULL, NULL, <<(myLocData.endData[1].vSmugHeliPos.x + 500), (myLocData.endData[1].vSmugHeliPos.y + 500), (myLocData.endData[1].vSmugHeliPos.z + 10.0)>>, MISSION_GOTO, 40.0, -1, 0, 60, 30)
					CLOSE_SEQUENCE_TASK(iSeq)
					IF NOT IS_PED_INJURED(smugArgs.smugglersHeliPed)
						TASK_PERFORM_SEQUENCE(smugArgs.smugglersHeliPed, iSeq)
					ENDIF
					CLEAR_SEQUENCE_TASK(iSeq)
			
					smugglerEndingStage = SMUGGLERS_ENDING_STATE_04
				ENDIF
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_ENDING_STATE_04
			IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 5.0)
				// Put smuggler drive back into the car
				IF IS_VEHICLE_DRIVEABLE(smugArgs.vehSmugglerCarWithDrugs) AND NOT IS_PED_INJURED(smugArgs.pedSmugglerWithDrugs)
					SET_PED_INTO_VEHICLE(smugArgs.pedSmugglerWithDrugs, smugArgs.vehSmugglerCarWithDrugs)
				ENDIF
			
				IF IS_VEHICLE_DRIVEABLE(smugArgs.vehSmugglerCarWithDrugs) AND NOT IS_PED_INJURED(smugArgs.pedSmugglerWithDrugs)
					GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(smugArgs.vehSmugglerCarWithDrugs), vClosestNode)
					SET_ENTITY_COORDS(smugArgs.vehSmugglerCarWithDrugs, vClosestNode)
					smugglerEndingStage = SMUGGLERS_ENDING_STATE_05
				ENDIF
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_ENDING_STATE_05
			IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 7.0)
				
				IF IS_VEHICLE_DRIVEABLE(smugArgs.vehSmugglerCarWithDrugs) AND NOT IS_PED_INJURED(smugArgs.pedSmugglerWithDrugs)
					TASK_VEHICLE_DRIVE_WANDER(smugArgs.pedSmugglerWithDrugs, smugArgs.vehSmugglerCarWithDrugs, 20.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
				ENDIF
			
				DEBUG_PRINT("smugArgs.bSmugglerReachedDest = TRUE")
				smugArgs.bSmugglerReachedDest = TRUE
				
				DEBUG_PRINT("RETURNING TRUE ON SMUGGLERS_ENDING_CUTSCENE")
				RETURN TRUE	
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_ENDING_SKIP_01
			DESTROY_CAM(myCamera)
			DESTROY_CAM(transitionCam)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			
			// Put the driver into his vehicle and task him to wander.
			IF IS_VEHICLE_DRIVEABLE(smugArgs.vehSmugglerCarWithDrugs) AND NOT IS_PED_INJURED(smugArgs.pedSmugglerWithDrugs)
				SET_PED_INTO_VEHICLE(smugArgs.pedSmugglerWithDrugs, smugArgs.vehSmugglerCarWithDrugs)
				TASK_VEHICLE_DRIVE_WANDER(smugArgs.pedSmugglerWithDrugs, smugArgs.vehSmugglerCarWithDrugs, 20.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
			ENDIF
			
			// Put the helicopter driver in the heli and task him to fly off.
			FREEZE_ENTITY_POSITION(smugArgs.smugglersHeliVeh, FALSE)
			
			IF NOT IS_PED_INJURED(smugArgs.smugglersHeliPed) AND IS_VEHICLE_DRIVEABLE(smugArgs.smugglersHeliVeh)
				CLEAR_SEQUENCE_TASK(iSeq)
				OPEN_SEQUENCE_TASK(iSeq)
					TASK_HELI_MISSION(NULL, smugArgs.smugglersHeliVeh, NULL, NULL, <<myLocData.endData[1].vSmugHeliPos.x, myLocData.endData[1].vSmugHeliPos.y, (myLocData.endData[1].vSmugHeliPos.z + 50.0)>>, MISSION_GOTO, 40.0, -1, 0, 60, 30)
					TASK_HELI_MISSION(NULL, smugArgs.smugglersHeliVeh, NULL, NULL, <<(myLocData.endData[1].vSmugHeliPos.x + 500), (myLocData.endData[1].vSmugHeliPos.y + 500), (myLocData.endData[1].vSmugHeliPos.z + 10.0)>>, MISSION_GOTO, 40.0, -1, 0, 60, 30)
				CLOSE_SEQUENCE_TASK(iSeq)
				IF NOT IS_PED_INJURED(smugArgs.smugglersHeliPed)
					TASK_PERFORM_SEQUENCE(smugArgs.smugglersHeliPed, iSeq)
				ENDIF
				CLEAR_SEQUENCE_TASK(iSeq)
			ENDIF
			
			DEBUG_PRINT("INSIDE STATE - SMUGGLERS_ENDING_SKIP_01")
			smugglerEndingStage = SMUGGLERS_ENDING_SKIP_02
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_ENDING_SKIP_02
			DO_SCREEN_FADE_IN(1000)
			WHILE NOT IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
			
			DEBUG_PRINT("smugArgs.bSmugglerReachedDest = TRUE VIA SKIP")
			smugArgs.bSmugglerReachedDest = TRUE
			
			// Reseting skipped cutscene bool, so you can skip ending cutscenes.
			bSkippedCutscene = FALSE
			
			DEBUG_PRINT("RETURNING TRUE IN STATE- SMUGGLERS_ENDING_SKIP_02 ")
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SMUGGLER_ENDING_SHOOTOUT_CUTSCENE_NEW()

	INT iInterpTime = 4000
	VECTOR vPlayerVehiclePos
	FLOAT fPlayerVehicleHeading
	VECTOR vPlayerPositionNearCar
	FLOAT fGroundZ
	VECTOR vCamPos01 = <<1.9821, -3.7360, 0.8606>>
	VECTOR vCamRot01 = <<0.7863, -1.0294, 0.3664>>
	VECTOR vCamPos02 = <<1.8626, -2.6789, 0.6921>>
	VECTOR vCamRot02 = <<0.6378, 0.0387, 0.3551>>

	IF NOT bGrabbedTimeForCutscene
		bSmugglersRetrieveCutscene = TRUE
		iAllowSkipCutsceneTime = GET_GAME_TIMER()
		bGrabbedTimeForCutscene = TRUE
	ENDIF
	
	IF NOT IS_TIMER_STARTED(cutArgs.cutSceneTimer)
		START_TIMER_NOW(cutArgs.cutSceneTimer)
	ENDIF
	
	//------------------------------------------SKIP CUTSCENE------------------------------------------
	IF NOT bSkippedCutscene
		HANDLE_SKIP_CUTSCENE_GROUND()
	ENDIF
	
	// ------------------------------------------FAIL SAFE------------------------------------------
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
		IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 10.0)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
			DEBUG_PRINT("TRIGGERING FAIL SAFE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	SWITCH smugglerRetrieveCutState
		CASE SMUGGLERS_RETRIEVE_STATE_01
		
			// Remove from AI 
			REMOVE_DRUG_CARGO(drugCargoArgs)
			// Add to player's vehicle
			ADD_DRUGS_TO_VEHICLE(drugCargoArgs, myLocData, myVehicle)
			
			CLEAR_PRINTS()
			ODDJOB_ENTER_CUTSCENE()
			CLEAR_AREA(smugArgs.vVehicleNodePos, 10.0, TRUE)
			
			// Move Car
			IF IS_VEHICLE_DRIVEABLE(myVehicle)
				// Set positions on player's vehicle and smugglers vehicle
				SET_ENTITY_COORDS(myVehicle, myLocData.endData[1].vCarPlacementShootEnd)
				SET_ENTITY_HEADING(myVehicle, myLocData.endData[1].fCarPlacementShootEnd)
				SET_VEHICLE_ON_GROUND_PROPERLY(myVehicle)
			ENDIF
			
			// Move Player
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
				vPlayerVehiclePos = GET_ENTITY_COORDS(myVehicle)
				PRINTLN("vPlayerVehiclePos = ", vPlayerVehiclePos)
				fPlayerVehicleHeading = GET_ENTITY_HEADING(myVehicle)
				PRINTLN("fPlayerVehicleHeading = ", fPlayerVehicleHeading)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					vPlayerPositionNearCar = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerVehiclePos, fPlayerVehicleHeading, <<-1.5,0.25,0>>)
					
					GET_GROUND_Z_FOR_3D_COORD(vPlayerPositionNearCar, fGroundZ)
					PRINTLN("fGroundZ = ", fGroundZ)
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					ENDIF
					
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << vPlayerPositionNearCar.x, vPlayerPositionNearCar.y, fGroundZ >>, TRUE, TRUE)
					fPlayerVehicleHeading = GET_ENTITY_HEADING(myVehicle)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerVehicleHeading - 90)
				ENDIF
			ENDIF
			
			// Attach 1st camera to player's vehicle
			myCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED)
			SET_CAM_FOV(myCamera, 35.0)
			SET_CAM_NEAR_DOF(myCamera, 0.5)
			SET_CAM_FAR_DOF(myCamera, 4.0)
			SET_CAM_DOF_STRENGTH(myCamera, 0.5)
//			SET_CAM_USE_SHALLOW_DOF_MODE(myCamera, TRUE)
			SHAKE_CAM(myCamera, "HAND_SHAKE", 0.2)
			ATTACH_CAM_TO_ENTITY(myCamera, myVehicle, vCamPos01)
			POINT_CAM_AT_ENTITY(myCamera, myVehicle, vCamRot01)
			SET_CAM_ACTIVE(myCamera, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			// Attach 2nd camera to player's vehicle
			transitionCam = CREATE_CAMERA(CAMTYPE_SCRIPTED)
			SET_CAM_FOV(transitionCam, 35.0)
			SET_CAM_NEAR_DOF(transitionCam, 0.5)
			SET_CAM_FAR_DOF(transitionCam, 4.0)
			SET_CAM_DOF_STRENGTH(transitionCam, 0.5)
//			SET_CAM_USE_SHALLOW_DOF_MODE(transitionCam, TRUE)
			SHAKE_CAM(transitionCam, "HAND_SHAKE", 0.2)
			ATTACH_CAM_TO_ENTITY(transitionCam, myVehicle, vCamPos02)
			POINT_CAM_AT_ENTITY(transitionCam, myVehicle, vCamRot02)
			
			SET_CAM_ACTIVE_WITH_INTERP(transitionCam, myCamera, iInterpTime, GRAPH_TYPE_SIN_ACCEL_DECEL)
			
			PRINTLN("GOING TO STATE - SMUGGLERS_RETRIEVE_STATE_02")
			smugglerRetrieveCutState = SMUGGLERS_RETRIEVE_STATE_02
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_RETRIEVE_STATE_02
			SET_USE_HI_DOF()
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
				TASK_ENTER_VEHICLE(PLAYER_PED_ID(), myVehicle)
				
				PRINTLN("GOING TO STATE - SMUGGLERS_RETRIEVE_STATE_03")
				smugglerRetrieveCutState = SMUGGLERS_RETRIEVE_STATE_03
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_RETRIEVE_STATE_03
			SET_USE_HI_DOF()
		
			IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 4.0)
				IF IS_CAM_SHAKING(myCamera)
					STOP_CAM_SHAKING(myCamera)
					PRINTLN("STOP CAM, myCamera, FROM SHAKING")
				ENDIF
				IF IS_CAM_SHAKING(transitionCam)
					STOP_CAM_SHAKING(transitionCam)
					PRINTLN("STOP CAM, transitionCam, FROM SHAKING")
				ENDIF
			
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				IF DOES_CAM_EXIST(myCamera)
					DESTROY_CAM(myCamera)
					PRINTLN("DESTROYING CAM - myCamera, SHOOTOUT CUTSCENE")
				ENDIF
				
				IF DOES_CAM_EXIST(transitionCam)
					DESTROY_CAM(transitionCam)
					PRINTLN("DESTROYING CAM - transitionCam, SHOOTOUT CUTSCENE")
				ENDIF
				
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				
				DEBUG_PRINT("RETURNING TRUE IN STATE- SMUGGLERS_RETRIEVE_STATE_03 ")
				ODDJOB_EXIT_CUTSCENE()
				RETURN TRUE
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_RETRIEVE_SKIP_01
			DESTROY_CAM(myCamera)
			DESTROY_CAM(transitionCam)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
				ENDIF
			ENDIF
			
			DEBUG_PRINT("INSIDE STATE - SMUGGLERS_RETRIEVE_SKIP_01")
			smugglerRetrieveCutState = SMUGGLERS_RETRIEVE_SKIP_02
		BREAK
		//==============================================================================================================================
		CASE SMUGGLERS_RETRIEVE_SKIP_02
			DO_SCREEN_FADE_IN(1000)
			WHILE NOT IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				
			// Reseting skipped cutscene bool, so you can skip ending cutscenes.
			bSkippedCutscene = FALSE
			
			ODDJOB_EXIT_CUTSCENE()
			DEBUG_PRINT("RETURNING TRUE IN STATE- SMUGGLERS_RETRIEVE_SKIP_02 ")
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_DESTROYED_PLANE()
	INT idx
	
//	PRINTLN("RUNNING - HAS_PLAYER_DESTROYED_PLANE")

	REPEAT MAX_NUM_RECORDS idx
		IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE)
			IF DOES_ENTITY_EXIST(planeDropArgs.planeDropVehicles[idx])
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(planeDropArgs.planeDropVehicles[idx], PLAYER_PED_ID())
					PRINTLN("PLANE HAS BEEN DAMAGED BY PLAYER")
					IF IS_ENTITY_DEAD(planeDropArgs.planeDropVehicles[idx])
						PRINTLN("PLANE IS DEAD, RETURNING TRUE")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_K, KEYBOARD_MODIFIER_ALT, "")
			REPEAT MAX_NUM_RECORDS idx
				IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE)
					IF DOES_ENTITY_EXIST(planeDropArgs.planeDropVehicles[idx])
						IF NOT IS_ENTITY_DEAD(planeDropArgs.planeDropVehicles[idx])
							EXPLODE_VEHICLE(planeDropArgs.planeDropVehicles[idx])
							PRINTLN("DEBUG, EXPLODING PLANE")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_ABANDONED_PACKAGE()
	VECTOR vPlayerPos
	
	IF NOT bDrugsLoaded
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
	
		IF planeDropArgs.bCargoHasLanded
			IF NOT IS_TIMER_STARTED(tAbandonPackageTimer)
				START_TIMER_NOW(tAbandonPackageTimer)
				PRINTLN("STARTING TIMER - tAbandonPackageTimer")
			ELSE
				IF VDIST2(vPlayerPos, myLocData.dropData[0].vCenterPos) > 10000 //100m
//					PRINTLN("GREATER THAN 100m")
					IF GET_TIMER_IN_SECONDS(tAbandonPackageTimer) > 60
						RETURN TRUE
					ELSE
//						PRINTLN("TIMER CHECK STILL GOING")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_FAIL_CONDITIONS()
//	FLOAT fTempDistance

	IF HAS_PLAYER_ABANDONED_PACKAGE()
		PRINTLN("PLAYER HAS ABANDONED JOB")
		failReason = FAIL_ABANDONED_JOB
		RETURN TRUE
	ENDIF

	IF DOES_ENTITY_EXIST(planeDropArgs.oLandedPackage) AND NOT IS_ENTITY_DEAD(planeDropArgs.oLandedPackage)
		IF HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(planeDropArgs.oLandedPackage), 0.5)
			ADD_EXPLOSION(GET_ENTITY_COORDS(planeDropArgs.oLandedPackage), EXP_TAG_TRAIN)
			DELETE_OBJECT(planeDropArgs.oLandedPackage)
			PRINTLN("PLAYER HAS DESTROYED PACKAGE - TYPE 1")
			failReason = FAIL_PACKAGE_DESTROYED
			RETURN TRUE
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(smugArgs.oArmsPackage) AND NOT IS_ENTITY_DEAD(smugArgs.oArmsPackage)
		IF HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(smugArgs.oArmsPackage), 0.5)
			ADD_EXPLOSION(GET_ENTITY_COORDS(smugArgs.oArmsPackage), EXP_TAG_TRAIN)
			DELETE_OBJECT(smugArgs.oArmsPackage)
			PRINTLN("PLAYER HAS DESTROYED PACKAGE - TYPE 2")
			failReason = FAIL_PACKAGE_DESTROYED
			RETURN TRUE
		ENDIF
	ENDIF

	IF IS_PLAYER_IN_ANY_SHOP()
		PRINTLN("PLAYER HAS ABANDONED JOB")
		failReason = FAIL_ABANDONED_JOB
		RETURN TRUE
	ENDIF

	IF bPlayerWantedAtDropOff
		PRINTLN("FAILING, BECAUSE PLAYER WANTED AT DROPOFF SITE")
		failReason = FAIL_PLAYER_WANTED
		RETURN TRUE
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) OR NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT bInActiveFight AND NOT bWanted AND NOT bIsCutsceneActive
			IF bOutOfVehicle
				IF HANDLE_OUT_OF_VEHICLE_WARNING_AND_FAIL("DTRSHRD_03", myVehicle, outOfVehicleTime)
					PRINTLN("FAILING, BECAUSE OUT OF CAR!!!!!!!!!!!!!!!!!")
					failReason = FAIL_ABANDONED_CAR
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF planeDropArgs.bCargoHasLanded 
		IF bTimeExpired 
			IF bWanted
				PRINTLN("failReason = FAIL_TIME_EXPIRED_WANTED")
				failReason = FAIL_TIME_EXPIRED_WANTED
				RETURN TRUE
			ELSE
				PRINTLN("failReason = FAIL_TIME_EXPIRED")
				failReason = FAIL_TIME_EXPIRED
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bLedPoliceToSite
		PRINTLN("failReason = FAIL_WANTED_AT_SITE")
		failReason = FAIL_WANTED_AT_SITE
		RETURN TRUE
	ENDIF
	
	IF bDoDelicate
		IF fHealth <= 0
			IF bDoHijack
				IF DOES_ENTITY_EXIST(hijackArgs.oWeaponsCargo)
					ADD_EXPLOSION(GET_ENTITY_COORDS(hijackArgs.oWeaponsCargo), EXP_TAG_MOLOTOV)
					failReason = FAIL_WEAPONS_DESTROYED
				ENDIF
			ELSE
				EXPLODE_VEHICLE(myVehicle)
				PRINTLN("FAIL REASON - FAIL_TRANSPORT_DESTROYED - 01")
				failReason = FAIL_TRANSPORT_DESTROYED
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT bDoSmugglers AND NOT bDoHijack
		IF HAS_PLAYER_DESTROYED_PLANE()
			PRINTLN("FAILING, THE PLAYER DESTROYED THE PLANE")
			failReason = FAIL_PLANE_DESTROYED
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF bDoSmugglers
		IF DOES_ENTITY_EXIST(smugArgs.smugglersVehicles[0]) AND NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0])
			IF smugArgs.bCarHasDrugs[0]
				IF IS_ENTITY_IN_WATER(smugArgs.smugglersVehicles[0])
					PRINTLN("FAIL REASON - FAIL_TRANSPORT_DESTROYED - 02")
					failReason = FAIL_TRANSPORT_DESTROYED
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF smugArgs.bSmugglerReachedDest
			PRINTLN("FAILING, BECAUSE SMUGGLERS REACHED THEIR DESTINATION")
			failReason = FAIL_SMUGGLER_REACHED_DEST
			RETURN TRUE
		ENDIF
		IF smugArgs.bDestroyedVehicleWithDrugs
			ADD_EXPLOSION(GET_ENTITY_COORDS(drugCargoArgs.oLoadedCargo), EXP_TAG_TRAIN)
			DELETE_OBJECT(drugCargoArgs.oLoadedCargo)
			PRINTLN("FAILING, BECAUSE SMUGGLERS VEHICLE IS DESTROYED AND THEY WERE CARRYING THE PACKAGE")
			failReason = FAIL_WEAPONS_DESTROYED
			RETURN TRUE
		ENDIF
		IF smugArgs.bAbandonedPackage
			PRINTLN("PLAYER HAS ABANDONED JOB - ABANDONED SMUGGLER'S PACKAGE")
			failReason = FAIL_ABANDONED_JOB
			RETURN TRUE
		ENDIF	
	ENDIF
	
	IF bDoHijack
		IF hijackArgs.bTransportReachedDest
			failReason = FAIL_SMUGGLER_REACHED_DEST
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF currentMissionStage < STAGE_ENDING_CUTSCENE
			IF NOT IS_VEHICLE_DRIVEABLE(myVehicle) //AND bDrugsLoaded
				DEBUG_PRINT("myVehicle IS NOT DRIVEABLE, FAILED MISSION")
				failReason = FAIL_TRANSPORT_DESTROYED
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bRunAbandonedCheck = TRUE
	
	// If the player has gone too far away from their current objective, assume they have abandoned the mission and fail them.
	// TODO: setup Hijack for abandon check
	IF bWanted OR bIsChaseActive OR (bIsSmugglersActive AND NOT bDrugsLoaded) OR bDoHijack OR bInActiveFight
		fDistanceToObjective = GET_DISTANCE_TO_NEXT_OBJECTIVE(vCurDestObjective)
//		PRINTLN("fDistanceToObjective = ", fDistanceToObjective)
		bRunAbandonedCheck = FALSE
	ENDIF	
	
	IF bRunAbandonedCheck
		IF PLAYER_HAS_ABANDONED_JOB(vCurDestObjective, MyLocalPedStruct)//, myLocData)
			PRINTLN("PLAYER HAS ABANDONED JOB")
			failReason = FAIL_ABANDONED_JOB
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(myVehicle)
		IF IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_ON_ROOF, ROOF_TIME) 
		OR IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_ON_SIDE, SIDE_TIME) 
		OR IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_HUNG_UP, HUNG_UP_TIME) 
		OR IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_JAMMED, JAMMED_TIME) 
			DEBUG_PRINT("FAILING - VEHICLE HAS BECOME UNDRIVEABLE/STUCK")
			failReason = FAIL_VEHICLE_DISABLED
			RETURN TRUE
		ENDIF
	ENDIF

//	RETURN UPSIDE_DOWN_CHECK(myVehicle, bIsUpsideDown, upsideDownVehicleTime, failReason)

	RETURN FALSE
ENDFUNC

// mission failed
PROC MISSION_FAILED()

	STRING strFailReason

	CLEAR_PRINTS()
	STOP_SCRIPTED_CONVERSATION(TRUE)
	
	TRIGGER_MUSIC_EVENT("OJDG_STOP")
	PRINTLN("STARTING MUSIC - OJDG_STOP - FAIL")
	
	// This sets us up for a replay through the launcher.
	SET_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_GroundTraffickingFailed)
	
	// will usually print a line of god text on fail describing the reason
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SWITCH failReason
			CASE FAIL_LOST_BAD_GUY
				strFailReason = "SAN_FAIL1"
				PRINTLN("FAIL REASON - SAN_FAIL1")
			BREAK
			CASE FAIL_TRANSPORT_DESTROYED
				strFailReason = "DTRSHRD_FAIL_09"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_09")
			BREAK
			CASE FAIL_PLANE_DESTROYED
				strFailReason = "DTRSHRD_FAIL_11"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_11")
			BREAK
			CASE FAIL_ABANDONED_CAR
				strFailReason = "DTRSHRD_FAIL_02"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_02")
			BREAK
			CASE FAIL_PLAYER_WANTED
				strFailReason = "DTRSHRD_FAIL_14"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_14")
			BREAK
			CASE FAIL_TIME_EXPIRED
				strFailReason = "DTRSHRD_FAIL_03"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_03")
			BREAK
			CASE FAIL_TIME_EXPIRED_WANTED
				strFailReason = "DTRSHRD_FAIL_15"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_15")
			BREAK
			CASE FAIL_WANTED_AT_SITE
				strFailReason = "DTRSHRD_FAIL_15"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_15")
			BREAK
			CASE FAIL_DISTANCE_FAIL
				strFailReason = "DTRSHRD_FAIL_05"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_05")
			BREAK
			CASE FAIL_ABANDONED_JOB
				strFailReason = "DTRSHRD_FAIL_06"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_06")
			BREAK
			CASE FAIL_SMUGGLER_REACHED_DEST
				strFailReason = "DTRSHRD_FAIL_07"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_07")
			BREAK
			CASE FAIL_VEHICLE_DISABLED
				strFailReason = "DTRSHRD_FAIL_08"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_08")
			BREAK
			CASE FAIL_WEAPONS_DESTROYED
				strFailReason = "DTRSHRD_FAIL_01"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_01")
			BREAK
			CASE FAIL_PLAYER_DIED
				// this gets handled by SET_FORCE_CLEANUP_FAIL_REASON()
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_10")
			BREAK
			CASE FAIL_PACKAGE_DESTROYED
				strFailReason = "DTRSHRD_FAIL_16"
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_16")
			BREAK
		ENDSWITCH
	ELSE
		// this gets handled by SET_FORCE_CLEANUP_FAIL_REASON()
		PRINTLN("FAIL REASON - DTRSHRD_FAIL_10 - A")
	ENDIF
	
	// Do a check here to see if we need to warp the player at all
	// (only set the fail warp locations if we can't leave the player where he was)
	//SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(vGroundEndingPos, fGroundEndingHeading)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
				MISSION_FLOW_SET_FAIL_WARP_LOCATION(vGroundEndingPos, fGroundEndingHeading)
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_STRING_NULL_OR_EMPTY(strFailReason)
		MISSION_FLOW_SET_FAIL_REASON(strFailReason)
	ENDIF
	SET_FORCE_CLEANUP_FAIL_REASON()
	
	PRINTLN("Setting us up for a replay!")
	Setup_Minigame_Replay(GET_THIS_SCRIPT_NAME())
	
	// Wait for replay controller to fade the screen out
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		WAIT(0)
	ENDWHILE
	
	// cleanup and terminate the thread (must be done in 1 frame)
	MISSION_CLEANUP()
ENDPROC

FUNC BOOL VALIDATE_CAR()
	VEHICLE_INDEX tempCar
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			tempCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			RETURN VALIDATE_OFF_ROAD_CAR(GET_ENTITY_MODEL(tempCar))
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL CAR_CHASE_READY_FIRST_TIME()
	VECTOR vPlayerPosition
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	IF NOT bIsChaseActive
		IF VDIST2(vPlayerPosition, myLocData.endData[0].vDrugPickup) < 40000 //200m
//			PRINTLN("EXITING EARLY ON CAR CHASE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF bDoCarChase
		IF bRunAmbushChase
			IF TIMERA() > DELAY_TIME
				RETURN TRUE
			ENDIF
		ELIF bRunSmugglerChase
			IF TIMERB() > DELAY_TIME
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC TYPE_2_CINEMATIC_CAM()
	PED_INDEX pedSmugglerDriver

	SWITCH iSmugglersCinematicStages
		CASE 0
			CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, myLocData.dropData[0].vCenterPos)
//			PRINTLN("SHOULD BE LOOKING AT DROP")
			
			IF DOES_ENTITY_EXIST(smugArgs.vehSmugglerPlane) 
			AND NOT IS_ENTITY_DEAD(smugArgs.vehSmugglerPlane) 
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(smugArgs.vehSmugglerPlane, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 250
					PRINTLN("iSmugglersCinematicStages = 1")
					iSmugglersCinematicStages = 1
				ENDIF
			ENDIF
		BREAK
		CASE 1
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			PRINTLN("iSmugglersCinematicStages = 2")
			iSmugglersCinematicStages = 2
		BREAK
		CASE 2
			CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, smugArgs.oArmsPackage)
//			PRINTLN("SHOULD BE LOOKING AT PACKAGE")
			
			IF smugArgs.bPickupAIFirst
				PRINTLN("iSmugglersCinematicStages = 3")
				iSmugglersCinematicStages = 3
			ENDIF
		BREAK
		CASE 3
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			PRINTLN("iSmugglersCinematicStages = 4")
			iSmugglersCinematicStages = 4
		BREAK
		CASE 4
			IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0]) 
			
				pedSmugglerDriver = GET_PED_IN_VEHICLE_SEAT(smugArgs.smugglersVehicles[0])
				
				IF DOES_ENTITY_EXIST(pedSmugglerDriver) AND NOT IS_ENTITY_DEAD(pedSmugglerDriver)
					CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, smugArgs.smugglersVehicles[0])
//					PRINTLN("SHOULD BE LOOKING AT ENEMIES")
				ELSE
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					PRINTLN("iSmugglersCinematicStages = 7")
					iSmugglersCinematicStages = 7
				ENDIF
			ENDIF
		BREAK
		CASE 7
			CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, myLocData.endData[0].vDrugPickup)
//			PRINTLN("SHOULD BE LOOKING AT ENEMIES")
		BREAK
	ENDSWITCH
ENDPROC

PROC TYPE_1_CINEMATIC_CAM()

	SWITCH iTypeOneCinmaticStages
		CASE 0
			CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, myLocData.dropData[0].vCenterPos)
			PRINTLN("TYPE 1: SHOULD BE LOOKING AT DROP")
			
			IF DOES_ENTITY_EXIST(planeDropArgs.planeVehicle) 
			AND NOT IS_ENTITY_DEAD(planeDropArgs.planeVehicle) 
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(planeDropArgs.planeVehicle, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 250
					PRINTLN("iTypeOneCinmaticStages = 1")
					iTypeOneCinmaticStages = 1
				ENDIF
			ENDIF
		BREAK
		CASE 1
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			PRINTLN("iTypeOneCinmaticStages = 2")
			iTypeOneCinmaticStages = 2
		BREAK
		CASE 2
			IF DOES_ENTITY_EXIST(planeDropArgs.planeVehicle)
				CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, planeDropArgs.planeVehicle)
				PRINTLN("TYPE 1: SHOULD BE LOOKING AT PLANE")
			ENDIF
			
			IF DOES_BLIP_EXIST(planeDropArgs.blipPackage)
				PRINTLN("iTypeOneCinmaticStages = 3")
				iTypeOneCinmaticStages = 3
			ENDIF
		BREAK
		CASE 3
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			PRINTLN("iTypeOneCinmaticStages = 4")
			iTypeOneCinmaticStages = 4
		BREAK
		CASE 4
			IF DOES_ENTITY_EXIST(planeDropArgs.oPackage)
				CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, planeDropArgs.oPackage)
				PRINTLN("TYPE 1: SHOULD BE LOOKING AT PACKAGE")
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC HANDLE_HINT_CAM()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
		
			IF GET_VARIATION_TYPE() = TYPE_1
				TYPE_1_CINEMATIC_CAM()
			ELSE
				TYPE_2_CINEMATIC_CAM()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC RUN_QUICK_PLANE_CAM()

	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
	
		IF IS_VEHICLE_DRIVEABLE(myVehicle)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(myVehicle, FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_VEHICLE_FORWARD_SPEED(myVehicle, fPlayerSpeed)
				PRINTLN("RETURNING PLAYER CONTROL AND SETTING SPEED BACK TO = ", fPlayerSpeed)
			ENDIF
		ENDIF
	
		IF DOES_CAM_EXIST(camQuickPlaneCam)
			IF IS_CAM_SHAKING(camQuickPlaneCam)
				STOP_CAM_SHAKING(camQuickPlaneCam, TRUE)
			ENDIF
		
			SET_CAM_ACTIVE(camQuickPlaneCam, FALSE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(camQuickPlaneCam)
		ENDIF
		
		PRINTLN("GOING TO STATE - QUICK_CAM_STAGE_04")
		quickCamStages = QUICK_CAM_STAGE_04
	ENDIF
	
	SWITCH quickCamStages
		CASE QUICK_CAM_STAGE_01
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(myVehicle) AND IS_VEHICLE_DRIVEABLE(myVehicle)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
						fPlayerSpeed = GET_ENTITY_SPEED(myVehicle)
						FREEZE_ENTITY_POSITION(myVehicle, TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						PRINTLN("FREEZING PLAYER VEHICLE, REMOVING CONTROL, AND STORING SPEED = ", fPlayerSpeed)
					ENDIF
				ENDIF
			ENDIF
			
			camQuickPlaneCam = CREATE_CAMERA(CAMTYPE_SCRIPTED)
			
			IF DOES_CAM_EXIST(camQuickPlaneCam)
				IF DOES_ENTITY_EXIST(planeDropArgs.planeVehicle) AND IS_VEHICLE_DRIVEABLE(planeDropArgs.planeVehicle)
					ATTACH_CAM_TO_ENTITY(camQuickPlaneCam, planeDropArgs.planeVehicle, vQuickCamPosOffset)
					POINT_CAM_AT_ENTITY(camQuickPlaneCam, planeDropArgs.planeVehicle, vQuickCamRotOffset)
					SET_CAM_FOV(camQuickPlaneCam, 50.0000)
				ENDIF
			ENDIF
			
			SET_CAM_ACTIVE(camQuickPlaneCam, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			IF NOT IS_TIMER_STARTED(tQuickCam)
				START_TIMER_NOW(tQuickCam)
				PRINTLN("STARTING tQuickCam TIMER")
			ENDIF
			
			IF DOES_CAM_EXIST(camQuickPlaneCam) AND IS_CAM_ACTIVE(camQuickPlaneCam)
				SHAKE_CAM(camQuickPlaneCam, "ROAD_VIBRATION_SHAKE", 0.5)
			ENDIF
			
			quickCamStages = QUICK_CAM_STAGE_02
		BREAK
		//==============================================================================================================================
		CASE QUICK_CAM_STAGE_02
		
//			OUTPUT_DEBUG_CAM_RELATIVE_TO_ENTITY(planeDropArgs.planeVehicle)
		
			IF IS_TIMER_STARTED(tQuickCam)
				IF GET_TIMER_IN_SECONDS(tQuickCam) > 2.0
					IF DOES_CAM_EXIST(camQuickPlaneCam) AND IS_CAM_ACTIVE(camQuickPlaneCam)
						IF IS_CAM_SHAKING(camQuickPlaneCam)
							STOP_CAM_SHAKING(camQuickPlaneCam, TRUE)
						ENDIF
					ENDIF
					PRINTLN("GOING TO STATE - QUICK_CAM_STAGE_02")
					quickCamStages = QUICK_CAM_STAGE_03
				ENDIF
			ENDIF
		BREAK
		//==============================================================================================================================
		CASE QUICK_CAM_STAGE_03
			
			IF IS_VEHICLE_DRIVEABLE(myVehicle)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(myVehicle, FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_VEHICLE_FORWARD_SPEED(myVehicle, fPlayerSpeed)
					PRINTLN("RETURNING PLAYER CONTROL AND SETTING SPEED BACK TO = ", fPlayerSpeed)
				ENDIF
			ENDIF
			
			SET_CAM_ACTIVE(camQuickPlaneCam, FALSE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			IF DOES_CAM_EXIST(camQuickPlaneCam)
				DESTROY_CAM(camQuickPlaneCam)
			ENDIF
			
			PRINTLN("GOING TO STATE - QUICK_CAM_STAGE_04")
			quickCamStages = QUICK_CAM_STAGE_04
		BREAK
		//==============================================================================================================================
		CASE QUICK_CAM_STAGE_04
			
		BREAK
	ENDSWITCH
ENDPROC

PROC SAFELY_REMOVE_PLANE()
	IF DOES_ENTITY_EXIST(planeDropArgs.planeVehicle) AND IS_VEHICLE_DRIVEABLE(planeDropArgs.planeVehicle)
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(planeDropArgs.planeVehicle)
        	IF planeDropArgs.bCargoHasLanded          
                SET_PED_AS_NO_LONGER_NEEDED(tempPed)
                PRINTLN("SETTING PED AS NO LONGER NEEDED - tempPed - 01")
                SET_VEHICLE_AS_NO_LONGER_NEEDED(planeDropArgs.planeVehicle)
                PRINTLN("SETTING PLANE AS NO LONGER NEEDED - 01")
        	ENDIF
    	ENDIF
  	ENDIF
ENDPROC

PROC RUN_PLANE_DROP()
	VECTOR vPlanePosition, vPlayerPosition
	FLOAT fPlayerDistanceToPoint
	FLOAT fPlaneDistanceToPoint

	// Start plane when you're close to the area...
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vCenterPos, <<myLocData.dropData[0].fTriggerPlaneDistance, myLocData.dropData[0].fTriggerPlaneDistance, 250>>, TRUE)
	OR bGiveTimeWarning
		UPDATE_AIR_DROP(myLocData, planeDropArgs, planeDropState, myLocationBlip)
	ENDIF
	
	IF DOES_ENTITY_EXIST(planeDropArgs.planeVehicle) AND IS_VEHICLE_DRIVEABLE(planeDropArgs.planeVehicle)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(planeDropArgs.planeVehicle)
		
			vPlanePosition = GET_ENTITY_COORDS(planeDropArgs.planeVehicle)
			IF NOT IS_VECTOR_ZERO(vPlanePosition)
				fPlaneDistanceToPoint = VDIST2(vPlanePosition, myLocData.dropData[0].vCenterPos)
			ENDIF
			
			vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IF NOT IS_VECTOR_ZERO(vPlayerPosition)
				fPlayerDistanceToPoint = VDIST2(vPlayerPosition, myLocData.dropData[0].vCenterPos)
			ENDIF
			
//			PRINTLN("fPlayerDistanceToPoint = ", fPlayerDistanceToPoint)
//			PRINTLN("fPlaneDistanceToPoint = ", fPlaneDistanceToPoint)
			
			IF fPlayerDistanceToPoint < fPlaneDistanceToPoint
				SET_PLAYBACK_SPEED(planeDropArgs.planeVehicle, 1.3)
				PRINTLN("INCREASING PLANE PLAYBACK SPEED")
			ELSE
				SET_PLAYBACK_SPEED(planeDropArgs.planeVehicle, 1.0)
			ENDIF
		ELSE
			IF planeDropArgs.bCargoHasLanded
				SET_PED_AS_NO_LONGER_NEEDED(tempPed)
				PRINTLN("SETTING PED AS NO LONGER NEEDED - tempPed - 01")
				SET_VEHICLE_AS_NO_LONGER_NEEDED(planeDropArgs.planeVehicle)
				PRINTLN("SETTING PLANE AS NO LONGER NEEDED - 01")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC HANDLE_AMBUSHER_BLIPPING()
	
	// We need to first check if we're running ambush at all, if we are, then wait until one of them is blipped to print objective - to fix Bug # 1371514
	IF bTriggerAmbushNow OR bTriggerAmbushLate
		IF bOkayToPrintAmbusherObjective
			IF NOT bPrintedObjectiveToLoseRunners
				PRINT_NOW("DTRFKGR_04", DEFAULT_GOD_TEXT_TIME, -1)
				PRINTLN("PRINTING OBJECTIVE TO LOSE THE RIVAL RUNNERS")
				bPrintedObjectiveToLoseRunners = TRUE
				
//				vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
//				PRINTLN("HANDLE_AMBUSHER_BLIPPING: vCurDestObjective[0] = ", vCurDestObjective[0])
			ENDIF
		ENDIF
	ELSE
		IF bNoAmbush
			IF NOT bNoAmbushPrintObjective
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
					
						IF NOT DOES_BLIP_EXIST(myLocationBlip[0])
							myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.endData[0].vDrugPickup)
					//		SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
							SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
							PRINTLN("ADDING IN BLIP VIA HANDLE_PLAYER_REACHING_PACKAGE")
						ELSE
							PRINTLN("BLIP ALREADY EXIST, NOT ADDING - 01")
						ENDIF
						
						PRINTLN("THE PLAYER IS NOT OUT OF THEIR VEHICLE")
						
						PRINT_NOW("DTRFKGR_03a", DEFAULT_GOD_TEXT_TIME, 1)
						sLastObjective = "DTRFKGR_03a"
						
						vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
						PRINTLN("vCurDestObjective[0] = ", vCurDestObjective[0])
						
						bNoAmbushPrintObjective = TRUE
						PRINTLN("bNoAmbushPrintObjective = TRUE - 01")
					ELSE
						bNoAmbushPrintObjective = TRUE
						PRINTLN("bNoAmbushPrintObjective = TRUE - 02")
					
						bOutOfVehicleAtPickup = TRUE
						PRINTLN("bOutOfVehicleAtPickup = TRUE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_PLAYER_REACHING_PACKAGE()
	INT idx
	
	CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_PACK", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
	
	bDisplayTimer = FALSE
	
	CLEAR_THIS_PRINT("DTRFKGR_06")
	
	IF DOES_ENTITY_EXIST(oFlareProp)
		DELETE_OBJECT(oFlareProp)
		DEBUG_PRINT("DELETING oFlareProp")
	ENDIF
	
	IF DOES_BLIP_EXIST(myLocationBlip[0])
		REMOVE_BLIP(myLocationBlip[0])
		DEBUG_PRINT("REMOVING myLocationBlip[0] VIA PLAYER REACHING PICKUP LOCATION")
	ENDIF
			
	// Turn off the flare
	IF DOES_PARTICLE_FX_LOOPED_EXIST(PTFXsmugglersFlare)
		STOP_PARTICLE_FX_LOOPED(PTFXsmugglersFlare)
	ENDIF
	
	// Delete package
	REPEAT MAX_NUM_RECORDS idx
		IF DOES_ENTITY_EXIST(planeDropArgs.oPackage)
			FREEZE_ENTITY_POSITION(planeDropArgs.oPackage, FALSE)
			DELETE_OBJECT(planeDropArgs.oPackage)
			DEBUG_PRINT("DELETING planeDropArgs.oPackage")
		ENDIF
	ENDREPEAT
	
	IF bTriggerAmbushNow
		PRINTLN("bTriggerAmbushNow IS TRUE")
	ELSE
		PRINTLN("bTriggerAmbushNow IS FALSE")
	ENDIF
	IF bOutOfVehicle
		PRINTLN("bOutOfVehicle IS TRUE")
	ELSE
		PRINTLN("bOutOfVehicle IS FALSE")
	ENDIF
	
	IF bTriggerAmbushNow AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
		vCurDestObjective[0] = <<0,0,0>>
		PRINTLN("SETTING vCurDestObjective[0] = <<0,0,0>>")
	ELSE
		vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
		PRINTLN("SETTING vCurDestObjective[0] = myLocData.endData[0].vDrugPickup")
	ENDIF
	
	IF DOES_BLIP_EXIST(smugArgs.blipPackage)
		REMOVE_BLIP(smugArgs.blipPackage)
		PRINTLN("REMOVING BLIP - smugArgs.blipPackage")
	ENDIF
	IF DOES_BLIP_EXIST(planeDropArgs.blipPackage)
		REMOVE_BLIP(planeDropArgs.blipPackage)
		PRINTLN("REMOVING BLIP - planeDropArgs.blipPackage")
	ENDIF 
	
	IF DOES_ENTITY_EXIST(planeDropArgs.oParachute)
		DELETE_OBJECT(planeDropArgs.oParachute)
		PRINTLN("DELETING PARACHUTE - VIA HANDLE_PLAYER_REACHING_PACKAGE")
	ENDIF
	
	ADD_DRUGS_TO_VEHICLE(drugCargoArgs, myLocData, myVehicle)
	bDrugsLoaded = TRUE
	
	SET_VEHICLE_DOORS_LOCKED(myVehicle, VEHICLELOCK_LOCKED_NO_PASSENGERS)
	
ENDPROC

PROC GIVE_AMBUSH_WARNING()
	// Warn the player of the ambushers.
	IF NOT bGiveAmbushWarning
		IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vCenterPos, <<250,250,250>>)
			AND g_savedGlobals.sTraffickingData.iGroundRank = 0
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AMBH", CONV_PRIORITY_VERY_HIGH)
				bGiveAmbushWarning = TRUE
			ELSE
				IF GET_RANDOM_BOOL()
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vCenterPos, <<150,150,150>>)
					AND g_savedGlobals.sTraffickingData.iGroundRank <> 0
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
						CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AMBH", CONV_PRIORITY_VERY_HIGH)
						bGiveAmbushWarning = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_FOR_STAYING_ON_MAIN_ROADS()

	VECTOR vPlayerCoordinates, vClosetNodePosition
	FLOAT fTempDistance //, fTempDistance01
	
	IF bTriggeredWanted
		IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
			TRIGGER_MUSIC_EVENT("OJDG1_GOING_LOST")
			PRINTLN("TRIGGERING MUSIC - OJDG1_GOING_LOST")
			bTriggeredWanted = FALSE
		ENDIF
	ENDIF
		
//	fTempDistance01 = GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), <<2058.6912, 4761.4756, 40.2296>>)
//	PRINTLN("fTempDistance = ", fTempDistance01)

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF (GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), myLocData.dropData[0].vCenterPos) < 50)
		OR (GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), myLocData.endData[0].vDrugPickup) < 50)
		OR (GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), <<2058.6912, 4761.4756, 40.2296>>) < 150)
		OR bAmbushActive
		OR bIsChaseActive
		OR bIsSmugglersActive
//		OR planeDropArgs.bPlaneActive
			RESTART_TIMER_NOW(tOnRoadTimer)
//			PRINTLN("CHECK_FOR_STAYING_ON_MAIN_ROADS - EXITING EARLY")
			EXIT
		ENDIF
	ENDIF
	
//	IF iRoadCounter > 20
//		PRINTLN("CHECKING FOR PROXIMITY TO ROAD NODES")
	
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			vPlayerCoordinates = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(vPlayerCoordinates)
			GET_CLOSEST_VEHICLE_NODE(vPlayerCoordinates, vClosetNodePosition, NF_NONE)
			
			IF NOT IS_VECTOR_ZERO(vClosetNodePosition)
				
				fTempDistance = VDIST2(vPlayerCoordinates, vClosetNodePosition)
				fTempDistance = SQRT(fTempDistance)
//				PRINTLN("fTempDistance = ", fTempDistance)
				
				IF VDIST2(vPlayerCoordinates, vClosetNodePosition) < 400 // 20m
					IF NOT IS_TIMER_STARTED(tOnRoadTimer)
						START_TIMER_NOW(tOnRoadTimer)
						bOnRoads = TRUE
//						PRINTLN("bOnRoads = TRUE 01")
					ELSE
						bOnRoads = TRUE
//						PRINTLN("bOnRoads = TRUE 02")
					ENDIF
				ELSE
					bOnRoads = FALSE
//					PRINTLN("bOnRoads = FALSE")
					IF IS_TIMER_STARTED(tOnRoadTimer)
						RESTART_TIMER_NOW(tOnRoadTimer)
//						PRINTLN("RESTARTING TIMER - tOnRoadTimer")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bOnRoads
			IF IS_TIMER_STARTED(tOnRoadTimer)
				FLOAT fTempTime = GET_TIMER_IN_SECONDS(tOnRoadTimer)
				fTempTime = fTempTime
//				PRINTLN("fTempTime = ", fTempTime)
				
				IF myLocData.dropData[0].fTimeAllowedToStayOnRoads = 0
					myLocData.dropData[0].fTimeAllowedToStayOnRoads = MAX_TIME_ALLOWED_ON_ROADS
					PRINTLN("TIME ALLOWED EQUALS ZERO, SETTING TO DEFAULT VALUE OF MAX_TIME_ALLOWED_ON_ROADS")
				ENDIF
				
				IF GET_TIMER_IN_SECONDS(tOnRoadTimer) > myLocData.dropData[0].fTimeAllowedToStayOnRoads
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						PRINTLN("SETTING PLAYER WANTED LEVEL - BEING ON ROADS TOO LONG")
					ENDIF
					
					IF NOT bTriggeredWanted
						TRIGGER_MUSIC_EVENT("OJDG1_GOING_WANTED")
						PRINTLN("TRIGGERING MUSIC - OJDG1_GOING_WANTED")
						bTriggeredWanted = TRUE
					ENDIF
				ELIF GET_TIMER_IN_SECONDS(tOnRoadTimer) > (myLocData.dropData[0].fTimeAllowedToStayOnRoads - 11.0)
					IF NOT bGiveCopWarning
						IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_ROADS", CONV_PRIORITY_VERY_HIGH)
							DEBUG_PRINT("OSCAR - POLICE WARNING DIALOGUE")
							bGiveCopWarning = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
//		iRoadCounter = 0
//	ENDIF
//	
//	iRoadCounter++
ENDPROC

PROC HANDLE_OBJECTIVE_PRINT_LOSE_RIVALS()
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.endData[0].vDrugPickup, <<GATEWAY_SIZE,GATEWAY_SIZE,GATEWAY_SIZEZ>>, TRUE)
	AND (bInActiveFight OR bIsChaseActive OR bIsSmugglersActive)
	AND NOT bWanted
		PRINT_NOW("DTRSHRD_06", DEFAULT_GOD_TEXT_TIME, 1)
		PRINTLN("PLAYER AT PICKUP SPOT - PRINTING OBJECTIVE TO LOSE THE RIVAL RUNNERS")
	ELSE
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.endData[0].vDrugPickup, <<GATEWAY_SIZE,GATEWAY_SIZE,GATEWAY_SIZEZ>>, TRUE)
		AND NOT (bInActiveFight AND bIsChaseActive AND bIsSmugglersActive)
			IF IS_MESSAGE_BEING_DISPLAYED()
				CLEAR_THIS_PRINT("DTRSHRD_06")
			ENDIF
			IF IS_HELP_MESSAGE_ON_SCREEN()
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL OKAY_TO_RUN_TRAP()
	IF g_savedGlobals.sTraffickingData.iGroundRank <> 20
	AND g_savedGlobals.sTraffickingData.iGroundRank > 4
		IF GET_VARIATION_TYPE() = TYPE_2
			IF iRandInt = 0
				IF bOkayToRunTrap
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_FLARE()
	// Remove the flare if you the package has landed, is visible, and you're close.
	IF NOT bRemoveFlare
		IF planeDropArgs.bCargoHasLanded 
			IF DOES_ENTITY_EXIST(planeDropArgs.oLandedPackage)
			AND NOT IS_ENTITY_OCCLUDED(planeDropArgs.oLandedPackage)
			AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vCenterPos, <<25,25,25>>)			
				// Turn off the flare
				IF DOES_PARTICLE_FX_LOOPED_EXIST(PTFXsmugglersFlare)
					STOP_PARTICLE_FX_LOOPED(PTFXsmugglersFlare)
				ENDIF
				bRemoveFlare = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_OSCAR_POST_AMBUSH_DIALOGUE()
	IF NOT bPlayedNoAmbushDialogue 
		IF NOT bIdleAmbusherTrigger AND NOT bTriggerAmbushLate
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
					IF IS_TIMER_STARTED(tNoAmbushTimer)
						IF GET_TIMER_IN_SECONDS(tNoAmbushTimer) > 7.5
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GOOD", CONV_PRIORITY_VERY_HIGH)							
									bPlayedNoAmbushDialogue = TRUE
									PRINTLN("PLAYING OSCAR CONVO - ARMS_GOOD")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_TREVOR_DELAYING_RETURN()

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vGroundEndingPos) < 200
//			PRINTLN("EXITING EARLY, BECAUSE WE'RE CLOSE")
			EXIT
		ENDIF
	ENDIF
	
	IF NOT bGaveDelayWarning
		IF bDrugsLoaded AND NOT bInActiveFight
			IF NOT IS_TIMER_STARTED(tDelayReturnTimer)
				START_TIMER_NOW(tDelayReturnTimer)
				PRINTLN("STARTING TIMER - tDelayReturnTimer")
			ELSE
				IF GET_TIMER_IN_SECONDS(tDelayReturnTimer) > 180
					IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "OSCAR_HURRY", CONV_PRIORITY_VERY_HIGH) 
						PRINTLN("bGaveDelayWarning = TRUE")
						bGaveDelayWarning = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_COLLIDED_WITH_PACKAGE()
	IF DOES_ENTITY_EXIST(planeDropArgs.oLandedPackage) AND NOT IS_ENTITY_DEAD(planeDropArgs.oLandedPackage)
		IF NOT IS_ENTITY_DEAD(myVehicle)
			IF IS_ENTITY_TOUCHING_ENTITY(myVehicle, planeDropArgs.oLandedPackage)
				PRINTLN("PLAYER'S VEHICLE HAS COLLIDED WITH PACKAGE")
				
				planeDropArgs.bCargoHasLanded = TRUE
				PRINTLN("SETTING planeDropArgs.bCargoHasLanded = TRUE SINCE THE PLAYER HAS TOUCHED THE PACKAGE")
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_AT_PACKAGE_ON_FOOT()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), planeDropArgs.oLandedPackage)
				PRINTLN("PLAYER IS ON FOOT AND CAN PICK UP THE PACKAGE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CREATE_AMBUSH_ATTACKER_VEHICLES()
	INT idx
	
	REPEAT 2 idx
		IF NOT DOES_ENTITY_EXIST(ambushArgs.vehAmbushAttackers[idx])
			IF NOT IS_VECTOR_ZERO(myLocData.dropData[0].vAmbushVehicleAttackerPositions[idx])
				ambushArgs.vehAmbushAttackers[idx] = CREATE_VEHICLE(REBEL, myLocData.dropData[0].vAmbushVehicleAttackerPositions[idx])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_VEHICLE_ON_FIRE_OR_UPSIDEDOWN(VEHICLE_INDEX TheVehicle)

	IF IS_ENTITY_UPSIDEDOWN(TheVehicle)
	OR IS_ENTITY_ON_FIRE(TheVehicle)
	OR GET_NUMBER_OF_FIRES_IN_RANGE(GET_ENTITY_COORDS(TheVehicle, FALSE), 5) > 0
	OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(TheVehicle, WEAPONTYPE_MOLOTOV)
	OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(TheVehicle, FALSE) - << 5, 5, 5 >>, GET_ENTITY_COORDS(TheVehicle, FALSE) + << 5, 5, 5 >>, WEAPONTYPE_MOLOTOV)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC HANDLE_PLAYER_LEAVING_VEHICLE_DURING_CONVO()
	IF NOT IS_PLAYER_IN_VALID_VEHICLE(myVehicle)
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			TestResumptionLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
			PRINTLN("KILLING ANY CONVERSATION THAT MIGHT BE ONGOING")
			KILL_FACE_TO_FACE_CONVERSATION()
			
			bInterrupt = TRUE
			PRINTLN("bInterrupt = TRUE")
		ENDIF
	ELSE
		IF bInterrupt
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, "ARMSAUD", myLocData.dropData[0].sConvo, TestResumptionLabel, CONV_PRIORITY_HIGH)
				PRINTLN("TRYING TO RESUME INTERRUPTED LINE")
				bInterrupt = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_DUNE_BUGGY()
	IF NOT DOES_ENTITY_EXIST(myVehicle)
		myVehicle = CREATE_VEHICLE(DUNE, vSpawnPosition, fSpawnHeading)
		PRINTLN("VEHICLE DOES NOT EXIST - CREATING NEW ONE")
		
		SET_VEHICLE_RADIO_ENABLED(myVehicle, FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(DUNE)
		
		IF DOES_ENTITY_EXIST(myVehicle)
			IF DOES_EXTRA_EXIST(myVehicle, 1)
				SET_VEHICLE_EXTRA(myVehicle, 1, TRUE)
				CPRINTLN(DEBUG_MINIGAME, "SETTING myVehicle VEHICLE EXTRA 1 TO TRUE")
			ENDIF
			IF DOES_EXTRA_EXIST(myVehicle, 2)
				SET_VEHICLE_EXTRA(myVehicle, 2, FALSE)
				CPRINTLN(DEBUG_MINIGAME, "SETTING myVehicle VEHICLE EXTRA 2 TO FALSE")
			ENDIF
			IF DOES_EXTRA_EXIST(myVehicle, 3)
				SET_VEHICLE_EXTRA(myVehicle, 3, FALSE)
				CPRINTLN(DEBUG_MINIGAME, "SETTING myVehicle VEHICLE EXTRA 3 TO FALSE")
			ENDIF
			SET_VEHICLE_COLOURS(myVehicle, 0, 0)
		ENDIF
	ELSE
		PRINTLN("VEHICLE ALREADY EXISTS - MOVING ALONG")
	ENDIF
ENDPROC

FUNC BOOL UPDATE()

	SWITCH currentMissionStage
			CASE STAGE_START
				IF IS_SCREEN_FADED_OUT()
				
					CREATE_DUNE_BUGGY()

					IF NOT IS_TIMER_STARTED(stLoadSceneTimer)
						START_TIMER_NOW(stLoadSceneTimer)
						PRINTLN("STARTING TIMER - stLoadSceneTimer - 01")
					ENDIF
					currentMissionStage = STAGE_GET_CAR
				ELSE
					PRINTLN("GOING TO STATE - STAGE_GET_CAR_B 01")
					currentMissionStage = STAGE_GET_CAR_B
				ENDIF
			BREAK
			CASE STAGE_GET_CAR
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				END_REPLAY_SETUP(myVehicle)
				
				PRINTLN("GOING TO STATE - STAGE_GET_CAR_B 02")
				currentMissionStage = STAGE_GET_CAR_B
			BREAK
			CASE STAGE_GET_CAR_B
			
				#IF IS_DEBUG_BUILD
					IF pSkipping
						DO_SCREEN_FADE_IN(500)
					ENDIF
				#ENDIF
			
				// Fade in if we need to - RParadis (for replays)
			    IF IS_SCREEN_FADED_OUT()
					CLEAR_AREA_OF_VEHICLES(<< 2149.3284, 4805.9575, 40.2094 >>, 100, FALSE, TRUE)
			    	DO_SCREEN_FADE_IN(500)
					PRINTLN("CALLING SCREEN FADE IN")
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ELSE
					PRINTLN("SCREEN IS FADED OUT")
			    ENDIF
			
				IF NOT bGenPrinted AND NOT DOES_ENTITY_EXIST(myVehicle)  // AND TIMER_DO_WHEN_READY(myTimer, 8) 
					//Aqcuire an off-road vehicle
					//TODO make this a phone call // A known location for off road vehicles has been added to your map.  You must aquire a proper vehicle to handle the rugged terrain of this drug drop.
					//PRINT_HELP("DTRFKGR_HELP_01")
					//Acquire an off-road vehicle.
					PRINT_NOW("DTRFKGR_01",DEFAULT_GOD_TEXT_TIME,1) //PRINT("S3_PASS", DEFAULT_GOD_TEXT_TIME, 1)
					sLastObjective = "DTRFKGR_01"
					myVehicle = CREATE_VEHICLE(myLocData.mnCarChasers[0], myLocData.vKnownVehicleLoc, myLocData.vKnownVehicleHead)	
					myVehicleBlip = ADD_BLIP_FOR_ENTITY(myVehicle)
					SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
					//RADIUS
					bGenPrinted = TRUE
				ENDIF
				IF VALIDATE_CAR()
					IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							myVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
							SET_VEHICLE_CAN_LEAK_OIL(myVehicle, FALSE)
							SET_VEHICLE_CAN_LEAK_PETROL(myVehicle, FALSE)
							SET_VEHICLE_RADIO_ENABLED(myVehicle, FALSE)
							SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(myVehicle, TRUE)
							SET_ENTITY_HEALTH(myVehicle, 2000)
							SET_VEHICLE_STRONG(myVehicle, TRUE)
							SET_VEHICLE_TYRES_CAN_BURST(myVehicle, FALSE)
							SET_VEHICLE_HAS_STRONG_AXLES(myVehicle, TRUE)
							SUPPRESS_PLAYERS_CAR()
							PRINTLN("SETTING PETROL/OIL TO NOT LEAK")
						ENDIF
						
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
					ENDIF
							
					REMOVE_BLIP(myVehicleBlip)
					vCurDestObjective[0] = myLocData.dropData[0].vCenterPos
					
					//set up the relationship groups.
					ADD_RELATIONSHIP_GROUP("policeChasers", staticChaseArgs.relStaticPoliceChasers)
					ADD_RELATIONSHIP_GROUP("carChaserEnemies", carChaserArgs.relCarChaserEnemies)

					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, RELGROUPHASH_PLAYER, staticChaseArgs.relStaticPoliceChasers)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, staticChaseArgs.relStaticPoliceChasers, RELGROUPHASH_PLAYER)
					
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, carChaserArgs.relCarChaserEnemies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, carChaserArgs.relCarChaserEnemies, RELGROUPHASH_PLAYER)
					
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, carChaserArgs.relCarChaserEnemies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, carChaserArgs.relCarChaserEnemies, RELGROUPHASH_COP)
				
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, staticChaseArgs.relStaticPoliceChasers, carChaserArgs.relCarChaserEnemies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, carChaserArgs.relCarChaserEnemies, staticChaseArgs.relStaticPoliceChasers)
					DEBUG_PRINT("SETTING RELATIONSHIP GROUPS")
					
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), "TREVOR")
					
					IF NOT IS_TIMER_STARTED(stMissionTimer)
						START_TIMER_NOW(stMissionTimer)
						PRINTSTRING("STARTING MISSION TIMER AT = ")
						PRINTFLOAT(GET_TIMER_IN_SECONDS(stMissionTimer))
						PRINTNL()
					ENDIF
					
					GIVE_PLAYER_WEAPONS()
					HANDLE_TAXI_DIALOGUE()
					
					SETTIMERA(0)
					
					// Basically reseting cutscene, first bool is to grab the time at the very start and the second is to reset to the first state
					bGrabbedTimeForCutscene = FALSE
					cutArgs.cutsceneState = CUTSCENE_STATE_START
					
					IF GET_VARIATION_TYPE() = TYPE_1
						// Create particle effect right at center position
						PTFXsmugglersFlare = START_PARTICLE_FX_LOOPED_AT_COORD("scr_drug_traffic_flare_L", myLocData.dropData[0].vCenterPos, <<0,0,0>>, 0.5)
						SET_PARTICLE_FX_LOOPED_COLOUR(PTFXsmugglersFlare, 1.0, 0.84, 0.0)
						
						TRIGGER_MUSIC_EVENT("OJDG1_START")
						PRINTLN("STARTING MUSIC - OJDG1_START")
						
						SNAP_3D_COORD_TO_GROUND(myLocData.dropData[0].vCenterPos)
						oFlareProp = CREATE_OBJECT(Prop_Flare_01b, myLocData.dropData[0].vCenterPos)
					ELSE
						TRIGGER_MUSIC_EVENT("OJDG2_START")
						PRINTLN("STARTING MUSIC - OJDG2_START")
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DEBUG_PRINT("GIVING BACK PLAYER CONTROL")
					ENDIF
					
					currentMissionStage = STAGE_PICKUP
				ENDIF
			BREAK
			//===================================================================================================================================
			CASE STAGE_PICKUP
				IF NOT bOutOfVehicle
				
					IF g_savedGlobals.sTraffickingData.iGroundRank = 0
						fTimeToBlip = 15000
//						PRINTLN("fTimeToBlip = ", fTimeToBlip)
					ELIF g_savedGlobals.sTraffickingData.iGroundRank = 1
						fTimeToBlip = 9000
//						PRINTLN("fTimeToBlip = ", fTimeToBlip)
						fTimeToBlipRivals = 7500
					ELIF g_savedGlobals.sTraffickingData.iGroundRank = 2
						fTimeToBlip = 6000
//						PRINTLN("fTimeToBlip = ", fTimeToBlip)
					ELIF g_savedGlobals.sTraffickingData.iGroundRank = 3
						fTimeToBlip = 6000
//						PRINTLN("fTimeToBlip = ", fTimeToBlip)
						fTimeToBlipRivals = 9500
					ELIF g_savedGlobals.sTraffickingData.iGroundRank = 4
						fTimeToBlip = 7000
//						PRINTLN("fTimeToBlip = ", fTimeToBlip)
					ELSE
						fTimeToBlip = 5000
						fTimeToBlipRivals = 5000
//						PRINTLN("fTimeToBlip = ", fTimeToBlip)
					ENDIF
					
					IF TIMERA() > fTimeToBlipRivals
						bOkayToBlipEnemies = TRUE
					ENDIF
				
					IF NOT bOkayToBlipObjective
						IF TIMERA() > fTimeToBlip
							myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.dropData[0].vCenterPos)
							IF DOES_BLIP_EXIST(myLocationBlip[0])
//								SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
								SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP02")
							ENDIF
							vCurDestObjective[0] = myLocData.dropData[0].vCenterPos
							
							PRINTLN("BLIPPING LOCATION")
							
							IF NOT IS_ENTITY_DEAD(myVehicle)
								SET_VEHICLE_RADIO_ENABLED(myVehicle, TRUE)
							ENDIF
							
							bOkayToBlipObjective = TRUE
						ENDIF
					ENDIF
					IF TIMERA() > fTimeToBlip
						// This handles the objective prints and help
						IF NOT bPrintObjectivesAndHelp
//							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF bTimedPickup
									bDisplayTimer = TRUE
								ENDIF
								
								PRINT_FIRST_OBJECTIVES_AND_HELP()
								bPrintObjectivesAndHelp = TRUE
//							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				HANDLE_PLAYER_LEAVING_VEHICLE_DURING_CONVO()
				
				HANDLE_HINT_CAM()
				
				// ------------------------------------------------TYPE 1 UPDATE------------------------------------------------
				IF GET_VARIATION_TYPE() = TYPE_1
					// Check to see if the player is staying on the main roads too long
					CHECK_FOR_STAYING_ON_MAIN_ROADS()
					// Handles the plane dropping the package
					RUN_PLANE_DROP()
					// Creates some vehicles that are to be used by the attackers on foot.
					CREATE_AMBUSH_ATTACKER_VEHICLES()
				
//					IF NOT bWanted
						
						REMOVE_FLARE()
						
						// Point at which we judge the player's time.
						IF NOT bTriggerAmbushNow AND NOT bTriggerAmbushLate
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vCenterPos, <<50,50,50>>, FALSE) AND IS_PLAYER_IN_VALID_VEHICLE(myVehicle) 
//								PRINTLN("UPDATE: fTimeLeft = ", fTimeLeft)
								IF fTimeLeft < myLocData.fTriggerNowTime
									bTriggerAmbushNow = TRUE
									PRINTLN("fTimeLeft = ", fTimeLeft)
									PRINTLN("!!!!!!!!!!!TRIGGERING AMBUSH EARLY!!!!!!!!!!!")
									
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GR09", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
										PRINTLN("TRYING TO PLAY OSCAR AMBUSH LINE - 01")
									ENDIF
								ELIF fTimeLeft < myLocData.fSlowTime AND fTimeLeft > myLocData.fTriggerNowTime
									bTriggerAmbushLate = TRUE
									PRINTLN("fTimeLeft = ", fTimeLeft)
									PRINTLN("!!!!!!!!!!!TRIGGERING AMBUSH LATE!!!!!!!!!!!")
									
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GR09", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
										PRINTLN("TRYING TO PLAY OSCAR AMBUSH LINE - 02")
									ENDIF
									
									bAmbushActive = TRUE
								ELSE
									bNoAmbush = TRUE
//									PRINTLN("bNoAmbush = TRUE")
								ENDIF
							ENDIF
						ENDIF
						
						IF planeDropArgs.bCargoHasLanded
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vCenterPos, <<35,35,35>>, FALSE) AND IS_PLAYER_IN_VALID_VEHICLE(myVehicle) 
								bDisplayTimer = FALSE
//								PRINTLN("bDisplayTimer = FALSE - PLAYER CLOSE ENOUGH TO DROPSITE")
								IF NOT IS_TIMER_STARTED(tIdleTimer)
									START_TIMER_NOW(tIdleTimer)
									PRINTLN("STARTING TIMER - tIdleTimer")
									
									PRINT_HELP("DTRFKGR_HELP_05")
								ELSE
									IF GET_TIMER_IN_SECONDS(tIdleTimer) > 20
										bIdleAmbusherTrigger = TRUE
										bTriggerAmbushNow = TRUE
										PRINTLN("WE'RE SITTING IDLE FOR TOO LONG, TRIGGER AMBUSH")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF bTriggerAmbushNow
							UPDATE_AMBUSH(myLocData, ambushState, badGuyPed, bInAmbush, ambushArgs, bDoAmbush, vCurDestObjective, 
							bWanted, myVehicle, outOfVehicleTime, bAmbushActive, MyLocalPedStruct, bDrugsLoaded, bOutOfVehicle,
							sLastObjective, myLocationBlip)
						ENDIF
						
						// Once your there, add the package
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vCenterPos, vPickupDimension, FALSE)
						OR HAS_PLAYER_COLLIDED_WITH_PACKAGE()
						OR (NOT IS_ENTITY_DEAD(planeDropArgs.oLandedPackage) AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(planeDropArgs.oLandedPackage), vPickupDimension, FALSE))
							
							IF planeDropArgs.bCargoHasLanded
							AND IS_PLAYER_IN_VALID_VEHICLE(myVehicle)
							OR (planeDropArgs.bCargoHasLanded AND IS_PLAYER_AT_PACKAGE_ON_FOOT())
						
								HANDLE_PLAYER_REACHING_PACKAGE()
								
								KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
								
								IF bNoAmbush
									IF NOT IS_TIMER_STARTED(tNoAmbushTimer)
										START_TIMER_NOW(tNoAmbushTimer)
										PRINTLN("STARTING TIMER - tNoAmbushTimer")
									ENDIF
								
									TRIGGER_MUSIC_EVENT("OJDG1_SAFE_PACKAGE")
									PRINTLN("TRIGGERING MUSIC - OJDG1_SAFE_PACKAGE")
								ENDIF
								
								RESTART_TIMER_NOW(tOnRoadTimer)
								DEBUG_PRINT("GO TO STAGE_DESTINATION VIA PICKUP UPDATE")
								currentMissionStage = STAGE_DESTINATION
							ELSE
//								PRINTLN("PACKAGE HAS NOT TECHNICALLY LANDED YET")
							ENDIF
						ENDIF
//					ELSE
//						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vCenterPos, vPickupDimension, FALSE)
//							bLedPoliceToSite = TRUE
//							DEBUG_PRINT("bLedPoliceToSite = TRUE")
//						ENDIF
//					ENDIF
				ENDIF
				
				// ------------------------------------------------TYPE 2 UPDATE------------------------------------------------
				IF bDoSmugglers AND bOkayToBlipEnemies
					CREATE_SMUGGLERS_MODE(myLocData, smugArgs, bSmugglersCreated)

					// If the smugglers have been created, go ahead and run update loop.
					IF bSmugglersCreated
						UPDATE_SMUGGLERS_MODE(myLocData, smugArgs, smugglersState, vCurDestObjective, myLocationBlip, sLastObjective, 
						bDrugsLoaded, bIsSmugglersActive, smugglerRetrieveState, myVehicle, fpSmugglersEnding, cutArgs, myCamera, 
						drugCargoArgs, smugglerPlaneRecordingStages, MyLocalPedStruct, 
						fpSmugglersPickupCutscene, bOkayToRunTrap, bOutOfVehicle, bWanted, smugglersCreateHeliPedStages, outOfVehicleTime)//, bDoSmugglers)
					
//						HANDLE_OBJECTIVE_PRINT_LOSE_RIVALS()
					ENDIF
					// If the flag has been set, i.e. the mode is effectively over... go to STAGE_DESTINATION to read when the player has reached the drop-off location.
					IF smugArgs.bGoToDestination
						DEBUG_PRINT("EARLY BREAK")
						
						// Setting to false in an effort to fix Bug #201467
						bIsSmugglersActive = FALSE
						
						bGrabbedTimeForCutscene = FALSE
						currentMissionStage = STAGE_DESTINATION
						BREAK
					ENDIF
					
//					IF OKAY_TO_RUN_TRAP()
//						UPDATE_TRAP(myLocData, trapArgs, carChaserArgs, smugArgs, trapStages, bIsChaseActive, MyLocalPedStruct, myVehicle, 
//						bDoCarChase, bDoSmugglers, myLocationBlip, sLastObjective)
//					ENDIF
					
				ENDIF
			BREAK
			//----------------------------------------------------------------------------------------------------------------------------------
			CASE STAGE_DESTINATION
			
				SAFELY_REMOVE_PLANE()
				
				IF g_savedGlobals.sTraffickingData.iGroundRank = 0 // Handles Ground 1
					CHECK_FOR_STAYING_ON_MAIN_ROADS()
				ELSE
					IF bCarChaseComplete // should handle all the other variations... but we want a 5 second buffer
						IF IS_TIMER_STARTED(tEndChaseTimer)
							IF GET_TIMER_IN_SECONDS(tEndChaseTimer) > 5
								CHECK_FOR_STAYING_ON_MAIN_ROADS()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// If the player gets to the location without triggering an ambush, Oscar say's "Good Job!"
				HANDLE_OSCAR_POST_AMBUSH_DIALOGUE()
				
				HANDLE_TREVOR_DELAYING_RETURN()
				
				HANDLE_AMBUSHER_BLIPPING()
				
				IF NOT bInActiveFight AND NOT bIsChaseActive
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
							CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, myLocData.endData[0].vDrugPickup)
						ELSE
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						ENDIF
					ELSE
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					ENDIF
				ENDIF
				
				// ------------------------------------------------TYPE 1 UPDATE------------------------------------------------
				
				// If the player got the warning and we didn't trigger the ambush early, trigger now.
				IF bTriggerAmbushLate OR bTriggerAmbushNow
					IF NOT bToldToGetBackInVehicle
						IF UPDATE_AMBUSH(myLocData, ambushState, badGuyPed, bInAmbush, ambushArgs, bDoAmbush, vCurDestObjective, 
						bWanted, myVehicle, outOfVehicleTime, bAmbushActive, MyLocalPedStruct, bDrugsLoaded, bOutOfVehicle,
						sLastObjective, myLocationBlip)
							IF bOutOfVehicle
								PRINT_NOW("DTRSHRD_03", DEFAULT_GOD_TEXT_TIME, 1)
								sLastObjective = "DTRSHRD_03"
								vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
								PRINTLN("OUT OF VEHICLE IS TRUE - vCurDestObjective[0] = ", vCurDestObjective[0])
								IF NOT DOES_BLIP_EXIST(myVehicleBlip)
									myVehicleBlip = ADD_BLIP_FOR_ENTITY(myVehicle)
									SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
									PRINTLN("ADDING IN myVehicleBlip IN AMBUSH")
								ENDIF
							ENDIF
							
							SETTIMERA(0)
						
							DEBUG_PRINT("***********************STARTING CAR CHASE TIMER VIA AMBUSH - 01***********************")
							bDoCarChase = TRUE
							bRunAmbushChase = TRUE
							
							bToldToGetBackInVehicle = TRUE
						ENDIF
					ENDIF
				ELSE
					IF g_savedGlobals.sTraffickingData.iGroundRank > 0
						IF NOT bDoCarChase
							SETTIMERA(0)
								
							DEBUG_PRINT("***********************STARTING CAR CHASE TIMER VIA AMBUSH - 02***********************")
							bDoCarChase = TRUE
							bRunAmbushChase = TRUE
						ENDIF
					ENDIF
				ENDIF
				IF NOT bPrintLateObjective
					IF bOutOfVehicleAtPickup AND NOT bTriggerAmbushNow
						IF GET_VARIATION_TYPE() = TYPE_1
							IF NOT bOutOfVehicle
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									PRINT_NOW("DTRFKGR_03a", DEFAULT_GOD_TEXT_TIME, 1)
									PRINTLN("PRINTING OBJECTIVE NOW THAT THE PLAYER IS BACK IN THEIR VEHICLE")
								
									bPrintLateObjective = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				// ------------------------------------------------TYPE 2 UPDATE------------------------------------------------
				IF bDoSmugglers AND bSmugglersCreated
					IF UPDATE_SMUGGLERS_MODE(myLocData, smugArgs, smugglersState, vCurDestObjective, myLocationBlip, sLastObjective, 
					bDrugsLoaded, bIsSmugglersActive, smugglerRetrieveState, myVehicle, fpSmugglersEnding, cutArgs, myCamera, 
					drugCargoArgs, smugglerPlaneRecordingStages, MyLocalPedStruct, 
					fpSmugglersPickupCutscene, bOkayToRunTrap, bOutOfVehicle, bWanted, smugglersCreateHeliPedStages, outOfVehicleTime)//, bDoSmugglers)
						
						// Setting to false in an effort to fix Bug #201467
						bIsSmugglersActive = FALSE
						
						DEBUG_PRINT("SETTING WANTED LEVEL MULTIPLIER TO ZERO IN SMUGGLERS MODE AGAIN!!!")
						SET_WANTED_LEVEL_MULTIPLIER(0.0)
						
						SETTIMERB(0)
						
						DEBUG_PRINT("***********************STARTING CAR CHASE TIMER VIA SMUGGLERS***********************")
						bDoCarChase = TRUE
						bRunSmugglerChase = TRUE
						
						bDoSmugglers = FALSE
					ENDIF
				ENDIF
				
//				IF OKAY_TO_RUN_TRAP()
//					UPDATE_TRAP(myLocData, trapArgs, carChaserArgs, smugArgs, trapStages, bIsChaseActive, MyLocalPedStruct, myVehicle, 
//					bDoCarChase, bDoSmugglers, myLocationBlip, sLastObjective)
//				ENDIF
					
				//------------------------------------------------CAR CHASE------------------------------------------------
				IF CAR_CHASE_READY_FIRST_TIME()
					IF UPDATE_CAR_CHASE(carChaseProgress, myTimer, myLocationBlip, myLocData, carChaserArgs, sLastObjective, 
						vCurDestObjective, bCarChaseComplete, bIsChaseActive, bInActiveFight, bWanted, bOutOfVehicle, myVehicleBlip,
						bDoPoliceChase, mnVehicleToUse, mnPedToUse, bOkayToRunTrap, MyLocalPedStruct, outOfVehicleTime, myVehicle, 
						bRunHeliChase)
						
						IF NOT IS_TIMER_STARTED(tEndChaseTimer)
							START_TIMER_NOW(tEndChaseTimer)
							PRINTLN("STARTING TIMER - tEndChaseTimer")
							
							RESTART_TIMER_NOW(tOnRoadTimer)
							PRINTLN("RESTARTING TIMER - tOnRoadTimer")
							
							bGiveCopWarning = FALSE
							PRINTLN("bGiveCopWarning = FALSE")
						ENDIF
						
						/* Hack: to fix Bug # 488111, we need to check if the player is back in their vehicle.  We're turning off bool 
						so UPDATE_MISSION_CAR does not run until we're given our next objective... was killing print immediately after 
						it was printed.*/
						IF NOT DOES_BLIP_EXIST(myLocationBlip[0])
							IF NOT bOutOfVehicle
								myLocationBlip[0] = ADD_BLIP_FOR_COORD(vCurDestObjective[0])
								IF DOES_BLIP_EXIST(myLocationBlip[0])
//									SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
									SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
									PRINTLN("ADDING IN myLocationBlip[0] AFTER CAR CHASE")
								ENDIF
								PRINT_NOW("DTRFKGR_03a", DEFAULT_GOD_TEXT_TIME, 1)
								sLastObjective = "DTRFKGR_03a"
							ELSE
								PRINTLN("STILL OUT OF VEHICLE")
							ENDIF
						ELSE
							IF bOutOfVehicle
								IF DOES_BLIP_EXIST(myLocationBlip[0])
									REMOVE_BLIP(myLocationBlip[0])
									PRINTLN("REMOVING myLocationBlip[0] POST UPDATE_CAR_CHASE")
								ENDIF
							ENDIF
							PRINTLN("myLocationBlip[0] DOES EXIST")
						ENDIF
					ENDIF
				ENDIF
				
				HANDLE_CHASE_ENDING_CONVO(MyLocalPedStruct, bRunHeliChase, myVehicle, bCarChaseComplete)
				
//				HANDLE_OBJECTIVE_PRINT_LOSE_RIVALS()
				
				bComplete = TRUE
				IF bInActiveFight
					bComplete = FALSE
				ENDIF
				
				IF bInActiveFight OR bWanted
					bDrawCorona = FALSE
//					PRINTLN("bDrawCorona = FALSE")
				ELSE
					bDrawCorona = TRUE
//					PRINTLN("bDrawCorona = TRUE")
				ENDIF
				
				IF NOT IS_ENTITY_AT_COORD(myVehicle, myLocData.endData[0].vDrugPickup, <<5,5,LOCATE_SIZE_HEIGHT>>, bDrawCorona) 
				OR NOT IS_PLAYER_IN_VALID_VEHICLE(myVehicle)
				AND NOT IS_VEHICLE_ON_FIRE_OR_UPSIDEDOWN(myVehicle)
					bComplete = FALSE
				ELIF NOT bInActiveFight
					bComplete = TRUE
				ENDIF
				
				IF bComplete
					
//					TRIGGER_MUSIC_EVENT("OJDG1_ALL_SAFE")
//					PRINTLN("TRIGGERING MUSIC - OJDG1_ALL_SAFE")
					
					TRIGGER_MUSIC_EVENT("OJDG_COMPLETE")
					PRINTLN("STARTING MUSIC - OJDG_COMPLETE")
					
					BRING_VEHICLE_TO_HALT(myVehicle, 5.0, 2)
					
					REMOVE_DRUG_CARGO(drugCargoArgs)
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
						TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), myVehicle)
						PRINTLN("TASKING THE PLAYER TO LEAVE VEHICLE")
					ENDIF
				
					// Using myArgs.fTimeLimit as a way to determine if we're in repeatable mode.
					fMissionTime = GET_TIMER_IN_SECONDS(stMissionTimer)
					PRINTLN("BEFORE CONVERSION: fMissionTime = ", fMissionTime)
					fMissionTime = (fMissionTime*1000)
					PRINTLN("AFTER CONVERSION: fMissionTime = ", fMissionTime)
					
					IF g_savedGlobals.sTraffickingData.iGroundRank >=20
						PRINTLN("IN REPEATABLE MODE")
						
						SET_ENDSCREEN_DATASET_HEADER(groundEndScreen.groundTraffickingEndScreen, "DTRSHRD_MPASS", "DTRSHRD_GRT")
						ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(groundEndScreen.groundTraffickingEndScreen, ESEF_TIME_M_S, "DTRSHRD_TIME1", "", FLOOR(fMissionTime), 0, ESCM_NO_MARK, FALSE)
						ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(groundEndScreen.groundTraffickingEndScreen, ESEF_DOLLAR_VALUE, "DTRSHRD_MONEY", "", 5000, 0, ESCM_NO_MARK, FALSE)
					ELSE
						PRINTLN("IN REGULAR MODE")
					
						sWeapon = GET_WEAPON_AWARDED()
						
						SET_ENDSCREEN_DATASET_HEADER(groundEndScreen.groundTraffickingEndScreen, "DTRSHRD_MPASS", "DTRSHRD_GRT")
						ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(groundEndScreen.groundTraffickingEndScreen, ESEF_TIME_M_S, "DTRSHRD_TIME1", "", FLOOR(fMissionTime), 0, ESCM_NO_MARK, FALSE)
						ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(groundEndScreen.groundTraffickingEndScreen, ESEF_DOLLAR_VALUE, "DTRSHRD_MONEY", "", 5000, 0, ESCM_NO_MARK, FALSE)
						ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(groundEndScreen.groundTraffickingEndScreen, ESEF_RAW_STRING, "DTRSHRD_WEAP", sWeapon, 0, 0, ESCM_NO_MARK, FALSE)
					ENDIF
				
//					currentMissionStage = STAGE_ENDING_CUTSCENE
					
					TRIGGER_MUSIC_EVENT("OJDG_COMPLETE")
					PRINTLN("STARTING MUSIC - OJDG_COMPLETE")

					DEBUG_PRINT("GOING TO STATE - STAGE_ENDING_CUTSCENE_RUN")
					currentMissionStage = STAGE_ENDING_CUTSCENE_RUN
				ELSE
					IF IS_ENTITY_AT_COORD(myVehicle, myLocData.endData[0].vDrugPickup, <<5,5,LOCATE_SIZE_HEIGHT>>, bDrawCorona)
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0 
								IF NOT bPrintedLoseCopsAtEnd
									PRINT_NOW("DTRSHRD_01", DEFAULT_GOD_TEXT_TIME, -1)
									bPrintedLoseCopsAtEnd = TRUE
								ENDIF
							ELSE
								IF bInActiveFight
								AND NOT bPrintedLoseAmbushersAtEnd
									PRINT_NOW("DTRSHRD_06", DEFAULT_GOD_TEXT_TIME, -1)
									bPrintedLoseAmbushersAtEnd = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE STAGE_ENDING_CUTSCENE_RUN
				IF DOES_BLIP_EXIST(myLocationBlip[0])
					REMOVE_BLIP(myLocationBlip[0])
					PRINTLN("REMOVING myLocationBlip[0] - STAGE_ENDING_CUTSCENE_RUN")
				ENDIF
				
				IF NOT bPlayMusic
					PLAY_MISSION_COMPLETE_AUDIO("TREVOR_SMALL_01")
					bPlayMusic = TRUE
				ENDIF
			
				IF ENDSCREEN_PREPARE(groundEndScreen.groundTraffickingEndScreen)
				AND IS_MISSION_COMPLETE_READY_FOR_UI()
								
					INIT_SIMPLE_USE_CONTEXT(cucEndScreen, TRUE, FALSE, FALSE, TRUE)
					ADD_SIMPLE_USE_CONTEXT_INPUT(cucEndScreen, "DTRSHRD_CONT", FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
					
					SETTIMERA(0)
					
					CLEAR_PRINTS()
					CLEAR_HELP()
					KILL_ANY_CONVERSATION()
					
					DEBUG_PRINT("GOING TO STATE - STAGE_ENDING_CUTSCENE")
					currentMissionStage = STAGE_ENDING_CUTSCENE
				ENDIF
			BREAK
			CASE STAGE_ENDING_CUTSCENE
				
//				IF DO_CUTSCENE_CUSTOM_GROUND(fpEndingCutscene01)

				IF TIMERA() > 7000 AND NOT bSkippedEndingScreen
					UPDATE_SIMPLE_USE_CONTEXT(cucEndScreen)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,			INPUT_FRONTEND_ENDSCREEN_ACCEPT)
					OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,	INPUT_FRONTEND_PAUSE_ALTERNATE)		// B*2276517 - Player can now press esc to close endscreen, if player control is on during endscreen
						PRINTLN("bSkippedEndingScreen = TRUE")
						bSkippedEndingScreen = TRUE
					ENDIF
				ENDIF

				bDoneRenderingEndScreen = RENDER_ENDSCREEN(groundEndScreen.groundTraffickingEndScreen, FALSE)
				IF bSkippedEndingScreen OR TIMERA() > 12000
					//Call transition out
					ENDSCREEN_START_TRANSITION_OUT(groundEndScreen.groundTraffickingEndScreen)
					//Wait until endscreen is done rendering
					IF bDoneRenderingEndScreen
						ENDSCREEN_SHUTDOWN(groundEndScreen.groundTraffickingEndScreen)
						
						MISSION_PASSED()
						
						bPassed = TRUE
							
						//Mission Complete
						DELETE_CUTSCENE_CARGO()
						
						PRINTLN("GROUND RANK = ", g_savedGlobals.sTraffickingData.iGroundRank)
						
						DEBUG_PRINT("GOING TO STATE - STAGE_DRIVE_OFF")
						currentMissionStage = STAGE_DRIVE_OFF
					ENDIF
				ENDIF
			BREAK
			CASE STAGE_DRIVE_OFF
				ENDSCREEN_SHUTDOWN(groundEndScreen.groundTraffickingEndScreen)
				
				DEBUG_PRINT("INSIDE STATE - STAGE_DRIVE_OFF")
				RETURN TRUE
			BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC TELEPORT_PLAYER()
	WHILE NOT FADE_DOWN()
		WAIT(0)
	ENDWHILE
	CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	LOAD_SCENE(myLocData.vPlayerStart)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), myLocData.vPlayerStart)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), myLocData.fPlayerRot)
	ENDIF
	
	WAIT(500)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	WHILE NOT FADE_UP()
		WAIT(0)
	ENDWHILE
ENDPROC

FUNC BOOL LOAD_SMUGGLERS_MODELS() 
	INT idx
	
	PRINTLN("INSIDE - LOAD_SMUGGLERS_MODELS")

	REPEAT myLocData.iNumSmugVehicles idx
		REQUEST_MODEL(myLocData.mnSmugglersVehicles[idx])
		DEBUG_PRINT("REQUESTING SMUGGLERS VEHICLES")
	ENDREPEAT
	
	REPEAT MAX_NUM_SMUGGLERS_RECORDINGS idx
		IF myLocData.dropData[0].smugglersRecordings[idx].recordingType = RECORDING_TYPE_CAR
			IF myLocData.dropData[0].smugglersRecordings[idx].bConfigured
				REQUEST_VEHICLE_RECORDING(myLocData.dropData[0].smugglersRecordings[idx].carrecNum, myLocData.dropData[0].smugglersRecordings[idx].carrecName)
				DEBUG_PRINT("REQUESTING VEHICLE RECORDING FOR SMUGGLERS")
				DEBUG_PRINT("CarRecInfo:")
				DEBUG_PRINT(myLocData.dropData[0].smugglersRecordings[idx].carrecName)
				PRINTLN("P-SKIP: REQUEST")
				PRINTSTRING(" num: ")
				PRINTINT(myLocData.dropData[0].smugglersRecordings[idx].carrecNum)
				PRINTNL()
			ELIF myLocData.dropData[0].smugglersRecordings[idx].bWaypointConfigured
				REQUEST_WAYPOINT_RECORDING(myLocData.dropData[0].smugglersRecordings[idx].carrecWaypointName)
				PRINTSTRING("REQUESTING WAYPOINT RECORDING: ")
				PRINTSTRING(myLocData.dropData[0].smugglersRecordings[idx].carrecWaypointName)
				PRINTNL()
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUM_SMUGGLERS_RECORDINGS idx
		IF myLocData.dropData[0].smugglersRecordings[idx].recordingType = RECORDING_TYPE_CAR
			IF myLocData.dropData[0].smugglersRecordings[idx].bWaypointConfigured
				IF NOT GET_IS_WAYPOINT_RECORDING_LOADED(myLocData.dropData[0].smugglersRecordings[idx].carrecWaypointName)
					DEBUG_PRINT("DRUG TRAFFICKING GROUND: trying to load Smugglers WAYPOINT recordings")
					PRINTLN("P-SKIP: WAITING ON SMUGGLERS")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC PRINT_GANG_TYPES()
	IF bUsingMexicans
		PRINTLN("bUsingMexicans IS TRUE")
	ELSE
		PRINTLN("bUsingMexicans IS FALSE")
	ENDIF
	IF bUsingMarabunta
		PRINTLN("bUsingMarabunta IS TRUE")
	ELSE
		PRINTLN("bUsingMarabunta IS FALSE")
	ENDIF
	IF bUsingHillbillies
		PRINTLN("bUsingHillbillies IS TRUE")
	ELSE
		PRINTLN("bUsingHillbillies IS FALSE")
	ENDIF
ENDPROC

// initialise mission
PROC INIT_MISSION()
	
	INT idx
	
	IF myLocData.dropData[0].gangType = GANG_MEXICANS
		bUsingMexicans = TRUE
		bUsingMarabunta = FALSE
		bUsingHillbillies = FALSE
	ELIF myLocData.dropData[0].gangType = GANG_MARABUNTA
		bUsingMexicans = FALSE
		bUsingMarabunta = TRUE
		bUsingHillbillies = FALSE
	ELIF myLocData.dropData[0].gangType = GANG_HILLBILLIES
		bUsingMexicans = FALSE
		bUsingMarabunta = FALSE
		bUsingHillbillies = TRUE
	ENDIF
	
	PRINT_GANG_TYPES()

	iRandInt = GET_RANDOM_INT_IN_RANGE() % 3
	PRINTLN("iRandInt = ", iRandInt)
	
//	INIT_ZVOLUME_WIDGETS()

	// -----------------------------------------------Used in all missions-----------------------------------------------
	REQUEST_MODEL(myLocData.mnLoadedCargo)
	DEBUG_PRINT("REQUESTING LOADED CARGO")

	REQUEST_MODEL(REBEL)
	DEBUG_PRINT("REQUESTING REBEL")
	REQUEST_MODEL(DUNE)
	DEBUG_PRINT("REQUESTING DUNE")
	
	IF NOT bDoHijack
		REQUEST_MODEL(Prop_Flare_01b)
		DEBUG_PRINT("REQUESTING - Prop_Flare_01b")
	ENDIF
	
	REQUEST_PTFX_ASSET()
	
	IF GET_VARIATION_TYPE() = TYPE_2
		IF bUsingMexicans
			myLocData.mnGenericBadGuy = G_M_M_MEXBOSS_01
			REQUEST_MODEL(myLocData.mnGenericBadGuy)
			DEBUG_PRINT("REQUESTING MEXICAN BAD GUY - G_M_M_MEXBOSS_01")
		ELIF bUsingMarabunta
			myLocData.mnGenericBadGuy = G_M_Y_SalvaGoon_03
			REQUEST_MODEL(myLocData.mnGenericBadGuy)
			DEBUG_PRINT("REQUESTING MARABUNTA BAD GUY - G_M_Y_SalvaGoon_03")
		ELIF bUsingHillbillies
			myLocData.mnGenericBadGuy = A_M_M_HillBilly_01
			REQUEST_MODEL(myLocData.mnGenericBadGuy)
			DEBUG_PRINT("REQUESTING HILLBILLY BAD GUY - A_M_M_HillBilly_01")
		ENDIF
	ENDIF
	
	REQUEST_ADDITIONAL_TEXT("DTRFKGR", ODDJOB_TEXT_SLOT) // load text for the mission
	REQUEST_ADDITIONAL_TEXT("DTRSHRD", MINIGAME_TEXT_SLOT) // load text for the mission
	// ------------------------------------------------------------------------------------------------------------------
	
	IF bDoSmugglers
		REPEAT myLocData.iNumSmugVehicles idx
			REQUEST_MODEL(myLocData.mnSmugglersVehicles[idx])
			DEBUG_PRINT("REQUESTING SMUGGLERS VEHICLES")
		ENDREPEAT
		
		REPEAT MAX_NUM_SMUGGLERS_RECORDINGS idx
			IF myLocData.dropData[0].smugglersRecordings[idx].recordingType = RECORDING_TYPE_CAR
				IF myLocData.dropData[0].smugglersRecordings[idx].bConfigured
					REQUEST_VEHICLE_RECORDING(myLocData.dropData[0].smugglersRecordings[idx].carrecNum, myLocData.dropData[0].smugglersRecordings[idx].carrecName)
					DEBUG_PRINT("REQUESTING VEHICLE RECORDING FOR SMUGGLERS")
					DEBUG_PRINT("CarRecInfo:")
					DEBUG_PRINT(myLocData.dropData[0].smugglersRecordings[idx].carrecName)
					PRINTSTRING(" num: ")
					PRINTINT(myLocData.dropData[0].smugglersRecordings[idx].carrecNum)
					PRINTNL()
				ELIF myLocData.dropData[0].smugglersRecordings[idx].bWaypointConfigured
					REQUEST_WAYPOINT_RECORDING(myLocData.dropData[0].smugglersRecordings[idx].carrecWaypointName)
					PRINTSTRING("REQUESTING WAYPOINT RECORDING: ")
					PRINTSTRING(myLocData.dropData[0].smugglersRecordings[idx].carrecWaypointName)
					PRINTNL()
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF bDoAmbush
		REPEAT MAX_NUM_RECORDS idx
			IF myLocData.dropData[0].carRecData[idx].bConfigured
				IF myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR
					REQUEST_MODEL(myLocData.dropData[0].carRecData[idx].carrecVehicle)
					DEBUG_PRINT("REQUESTING CAR RECORDING VEHICLE")
					REQUEST_VEHICLE_RECORDING(myLocData.dropData[0].carRecData[idx].carrecNum, myLocData.dropData[0].carRecData[idx].carrecName)
					PRINTLN("CAR RECORDING NAME: ", myLocData.dropData[0].carRecData[idx].carrecName)
					PRINTLN("CAR RECORDING NUMBER: ", myLocData.dropData[0].carRecData[idx].carrecNum)
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT MAX_NUM_RECORDS idx
			IF myLocData.dropData[0].carRecData[idx].bConfigured
				IF myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE
					REQUEST_MODEL(myLocData.dropData[0].carRecData[idx].carrecVehicle)
					REQUEST_VEHICLE_RECORDING(myLocData.dropData[0].carRecData[idx].carrecNum, myLocData.dropData[0].carRecData[idx].carrecName)
					PRINTLN("CAR RECORDING NAME: ", myLocData.dropData[0].carRecData[idx].carrecName)
					PRINTLN("CAR RECORDING NUMBER: ", myLocData.dropData[0].carRecData[idx].carrecNum)
				ENDIF
			ENDIF	
		ENDREPEAT
	ENDIF
	
	//------------------------------------------------CHECK LOADING STATUS------------------------------------------------
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
		WAIT(0)
		DEBUG_PRINT("WAITING ON TEXT SLOTS")
	ENDWHILE 

	WHILE NOT HAS_MODEL_LOADED(Prop_Flare_01b)
		WAIT(0)
		DEBUG_PRINT("WAITING ON Prop_Flare_01b")
	ENDWHILE
		
	WHILE NOT HAS_MODEL_LOADED(REBEL)
	OR NOT HAS_MODEL_LOADED(DUNE)
		WAIT(0)
		DEBUG_PRINT("WAITING ON REBEL AND DUNE")
	ENDWHILE
		
	IF bDoSmugglers
		REPEAT MAX_NUM_SMUGGLERS_RECORDINGS idx
			IF myLocData.dropData[0].smugglersRecordings[idx].recordingType = RECORDING_TYPE_CAR
				IF myLocData.dropData[0].smugglersRecordings[idx].bConfigured
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(myLocData.dropData[0].smugglersRecordings[idx].carrecNum, myLocData.dropData[0].smugglersRecordings[idx].carrecName)
						DEBUG_PRINT("DRUG TRAFFICKING GROUND: trying to load Smugglers recordings")
						WAIT(0)
					ENDWHILE
				ELIF myLocData.dropData[0].smugglersRecordings[idx].bWaypointConfigured
					WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED(myLocData.dropData[0].smugglersRecordings[idx].carrecWaypointName)
						DEBUG_PRINT("DRUG TRAFFICKING GROUND: trying to load Smugglers WAYPOINT recordings")
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
	OR NOT HAS_MODEL_LOADED(myLocData.mnLoadedCargo)
		DEBUG_PRINT("DRUG TRAFFICKING GROUND:  trying to load string table, and cargo model")
		WAIT(0)
	ENDWHILE
	
	bStreamed = TRUE
	
	IF bDoAmbush
		REPEAT MAX_NUM_RECORDS idx
			IF myLocData.dropData[0].carRecData[idx].bConfigured
				IF myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(myLocData.dropData[0].carRecData[idx].carrecNum, myLocData.dropData[0].carRecData[idx].carrecName)
					OR NOT HAS_MODEL_LOADED(myLocData.dropData[0].carRecData[idx].carrecVehicle)
						DEBUG_PRINT("DRUG TRAFFICKING GROUND:  trying to load ambush models")
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	WHILE NOT HAS_PTFX_ASSET_LOADED()
		WAIT(0)
		DEBUG_PRINT("WAITING ON PARTICLE EFFECTS")
	ENDWHILE

	IF NOT bStreamed
		DEBUG_PRINT("Waiting for vehicle recording to be loaded")
		WAIT(0)
	ENDIF
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(DLOADER, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(DUNE, TRUE)
	
ENDPROC

// debug builds only
#IF IS_DEBUG_BUILD
	BLIP_INDEX blipLocations[20]
	VECTOR vCenterPositions[20]
	
	PROC INIT_BLIP_LOCATIONS()
		vCenterPositions[0] = 	<< 2709.4263, 4148.5249, 42.7539 >>
		vCenterPositions[1] = 	<< 1352.0400, 4370.2515, 43.3564 >>
		vCenterPositions[2] = 	<< 2221.6963, 5579.6660, 52.8354 >>
		vCenterPositions[3] = 	<< 2924.0862, 4638.2456, 47.5449 >>
		vCenterPositions[4] = 	<< 1954.2291, 3440.9077, 40.6718 >>
		vCenterPositions[5] = 	<< 1428.30, 3143.96, 40.387 >>
		vCenterPositions[6] = 	<< 2856.1038, 4488.6426, 47.2848 >>
		vCenterPositions[7] = 	<< 717.6153, 4176.4287, 39.7190 >>
		vCenterPositions[8] = 	<< 1761.8362, 3050.5618, 60.8981 >>
		vCenterPositions[9] = 	<< 2038.5756, 3224.4949, 43.8766 >>
		vCenterPositions[10] = 	<< 2623.6450, 3287.7129, 54.3001 >>
		vCenterPositions[11] = 	<< 1178.1967, 3271.2627, 38.2326 >>
		vCenterPositions[12] = 	<< 3702.8269, 4511.5928, 20.1186 >>
		vCenterPositions[13] = 	<< 2669.8481, 3546.6760, 50.5799 >>
		vCenterPositions[14] = 	<< 2346.9280, 3095.9612, 47.0212 >>
		vCenterPositions[15] = 	<< 1206.8820, 3599.0930, 33.0185 >>
		vCenterPositions[16] = 	<< 1178.1967, 3271.2627, 38.2326 >>
		vCenterPositions[17] = 	<< 2181.7673, 3367.6118, 44.4324 >>
		vCenterPositions[18] = 	<< 2685.8694, 2844.3059, 38.8406 >>
		vCenterPositions[19] = 	<< 1690.2025, 3166.5276, 44.8952 >>
	ENDPROC
	
	PROC DEBUG_BLIP_LOCATIONS
		INT idx
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_B, KEYBOARD_MODIFIER_ALT, "")
			INIT_BLIP_LOCATIONS()
		
			REPEAT 20 idx
				IF NOT DOES_BLIP_EXIST(blipLocations[idx])
					blipLocations[idx] = ADD_BLIP_FOR_COORD(vCenterPositions[idx])
					PRINTLN("ADDING BLIP FOR LOCATION INDEX: ", idx)
					IF idx % 2 = 0
						SET_BLIP_COLOUR(blipLocations[idx], BLIP_COLOUR_GREEN)
						PRINTLN("SETTING INDEX: ", idx, " TO GREEN")
					ENDIF
				ELSE
					REMOVE_BLIP(blipLocations[idx])
					PRINTLN("REMOVING BLIP FOR LOCATION INDEX: ", idx)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDPROC
	
PROC REMOVE_AMBUSHERS()
	INT idx

	REPEAT MAX_NUM_RECORDS idx
		IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
			IF DOES_ENTITY_EXIST(ambushArgs.ambushVehicles[idx])
				DELETE_VEHICLE(ambushArgs.ambushVehicles[idx])
				PRINTLN("DELETING AMBUSH VEHICLE INDEX: ", idx)
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT myLocData.iNumBadGuys idx
		IF DOES_ENTITY_EXIST(badGuyPed[idx])
			DELETE_PED(badGuyPed[idx])
			PRINTLN("DELETING AMBUSH PED INDEX: ", idx)
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUMBER_AMBUSH_ATTACKERS idx
		IF DOES_ENTITY_EXIST(ambushArgs.pedAmbushAttackers[idx])
			DELETE_PED(ambushArgs.pedAmbushAttackers[idx])
			PRINTLN("DELETING AMBUSH ATTACKER PED INDEX: ", idx)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC REMOVE_SMUGGLERS()
	INT idx

	// Remove blips
	REPEAT COUNT_OF(smugArgs.blipSmugglersPeds) idx
		IF DOES_BLIP_EXIST(smugArgs.blipSmugglersPeds[idx])
			REMOVE_BLIP(smugArgs.blipSmugglersPeds[idx])
			DEBUG_PRINT("REMOVING BLIP blipSmugglersPeds")
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(smugArgs.blipSmugglersCars) idx 
		IF DOES_BLIP_EXIST(smugArgs.blipSmugglersCars[idx])
			REMOVE_BLIP(smugArgs.blipSmugglersCars[idx])
			DEBUG_PRINT("REMOVING blipSmugglersCars")
		ENDIF
	ENDREPEAT
	
	REPEAT myLocData.iNumSmugVehicles idx
		IF DOES_ENTITY_EXIST(smugArgs.smugglersVehicles[idx])
			DELETE_VEHICLE(smugArgs.smugglersVehicles[idx])
			PRINTLN("DELETING SMUGGLER VEHICLE INDEX: ", idx)
		ENDIF
	ENDREPEAT
	
	REPEAT (myLocData.iNumSmugPedsPerVehicle * myLocData.iNumSmugVehicles) idx
		IF DOES_ENTITY_EXIST(smugArgs.smugglersPeds[idx]) 
			DELETE_PED(smugArgs.smugglersPeds[idx])
			PRINTLN("DELETING SMUGGLER PED INDEX: ", idx)
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(smugArgs.smugglersHeliVeh)
		DELETE_VEHICLE(smugArgs.smugglersHeliVeh)
		PRINTLN("DELETING SMUGGLER VEHICLE - HELICOPTER")
	ENDIF
	
	IF DOES_ENTITY_EXIST(smugArgs.smugglersHeliPed)
		DELETE_PED(smugArgs.smugglersHeliPed)
		PRINTLN("DELETING SMUGGLER PED - HELICOPTER PILOT")
	ENDIF
	
ENDPROC

PROC REMOVE_CAR_CHASERS()
	INT idx

	REPEAT COUNT_OF(carChaserArgs.badGuyBlips) idx
		IF DOES_BLIP_EXIST(carChaserArgs.badGuyBlips[idx])
			REMOVE_BLIP(carChaserArgs.badGuyBlips[idx])
			DEBUG_PRINT("REMOVING BLIP badGuyBlips")
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(carChaserArgs.blipCarChasers) idx 
		IF DOES_BLIP_EXIST(carChaserArgs.blipCarChasers[idx])
			REMOVE_BLIP(carChaserArgs.blipCarChasers[idx])
			DEBUG_PRINT("REMOVING blipCarChasers")
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(carChaserArgs.vehCarChasers[0])
		DELETE_VEHICLE(carChaserArgs.vehCarChasers[0])
		PRINTLN("REMOVE_CAR_CHASERS - DELETING VEHICLE - carChaserArgs.vehCarChasers[0]")
	ENDIF
	
	IF DOES_ENTITY_EXIST(carChaserArgs.pedCarChasers[0])
		DELETE_PED(carChaserArgs.pedCarChasers[0])
		PRINTLN("DELETING CAR CHASER PED - carChaserArgs.pedCarChasers[0]")
	ENDIF
	
	IF DOES_ENTITY_EXIST(carChaserArgs.pedCarChasers[1])
		DELETE_PED(carChaserArgs.pedCarChasers[1])
		PRINTLN("DELETING CAR CHASER PED - carChaserArgs.pedCarChasers[1]")
	ENDIF
	
ENDPROC

PROC REMOVE_PLANE()

	// Type 1 plane
	IF DOES_ENTITY_EXIST(planeDropArgs.planeDropVehicles[0])
		DELETE_VEHICLE(planeDropArgs.planeDropVehicles[0])
		PRINTLN("DELETING PLANE - planeDropArgs.planeDropVehicles[0]")
	ENDIF
	IF DOES_ENTITY_EXIST(tempPed)
		DELETE_PED(tempPed)
		PRINTLN("DELETING PED - tempPed]")
	ENDIF
	
	// Type 2 plane
	IF DOES_ENTITY_EXIST(smugArgs.vehSmugglerPlane)
		DELETE_VEHICLE(smugArgs.vehSmugglerPlane)
		PRINTLN("DELETING PLANE - smugArgs.vehSmugglerPlane")
	ENDIF
	IF DOES_ENTITY_EXIST(pedTempPilot)
		DELETE_PED(pedTempPilot)
		PRINTLN("DELETING PED - pedTempPilot")
	ENDIF

ENDPROC
	// debug skips
	PROC DO_DEBUG_SKIPS()
		// mission pass S
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		
			TRIGGER_MUSIC_EVENT("OJDG_STOP")
			PRINTLN("STARTING MUSIC - OJDG_STOP - SKIP")
		
			bSkippedMission = TRUE
			MISSION_PASSED()
		ENDIF
		
		// mission failed F
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			failReason = FAIL_TRANSPORT_DESTROYED
			MISSION_FAILED()
		ENDIF
		
		// skip stage J
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			
			planeDropArgs.bCargoHasLanded = TRUE
			
			// skip forward a stage
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vCurDestObjective[0])
			ENDIF
		ENDIF
		
		// previous stage P
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			// skip back a stage
			PRINTLN("P-SKIP ACTIVATED")
			
			IF currentMissionStage = STAGE_DESTINATION
				DO_SCREEN_FADE_OUT(500)
				
				CLEAR_PRINTS()
				KILL_ANY_CONVERSATION()
				pSkipping = TRUE
			
				IF DOES_BLIP_EXIST(myLocationBlip[0])
					REMOVE_BLIP(myLocationBlip[0])
					PRINTLN("REMOVING BLIP - myLocationBlip[0] VIA P-SKIP")
				ENDIF
				
				REMOVE_RELATIONSHIP_GROUP(staticChaseArgs.relStaticPoliceChasers)
				REMOVE_RELATIONSHIP_GROUP(carChaserArgs.relCarChaserEnemies)
				REMOVE_RELATIONSHIP_GROUP(smugArgs.relSmugglersEnemies)
				
				REMOVE_AMBUSHERS()
				REMOVE_SMUGGLERS()
				REMOVE_CAR_CHASERS()
				REMOVE_PLANE()
				
				IF GET_VARIATION_TYPE() = TYPE_2
					PRINTLN("P-SKIP, ATTEMPTING TO LOAD MODELS")
					bDoSmugglers = TRUE
				
					WHILE NOT LOAD_SMUGGLERS_MODELS()
						PRINTLN("WAITING ON SMUGGLERS MODELS")
						WAIT(0)
					ENDWHILE
				ELSE
					PRINTLN("VARIATION NOT TYPE_2")
				ENDIF
				
				bGenPrinted = FALSE
				bOkayToBlipEnemies = FALSE
				bPrintObjectivesAndHelp = FALSE
				bRemoveFlare = FALSE
				bTriggerAmbushNow = FALSE
				bTriggerAmbushLate = FALSE
				planeDropArgs.bCargoHasLanded = FALSE
				bSmugglersCreated = FALSE
				smugArgs.bGoToDestination = FALSE
				bTimerStarted = FALSE
				
				planeDropState = PLANE_DROP_INIT
				smugglerPlaneRecordingStages = SMUGGLER_PLANE_STATE_INIT
				smugglersState = SMUGGLERS_STATE_TO_PICKUP
				ambushState = AMBUSH_INIT
				carChaseProgress = CAR_CHASE_INIT
				
				IF IS_TIMER_STARTED(stMissionTimer)
					RESTART_TIMER_NOW(stMissionTimer)
				ENDIF
				
				SETTIMERA(0)
				
				SET_ENTITY_COORDS(myVehicle, <<2006.7253, 4738.1470, 40.2239>>)
				SET_ENTITY_HEADING(myVehicle, 298.2369)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
				PRINTLN("GOING TO STATE - STAGE_GET_CAR_B")
				currentMissionStage = STAGE_GET_CAR_B
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

PROC CONFIGURE_OVERRIDE_DATA(ARGS myArgs, BOOL bSetEnd = TRUE)
	INT idx

	POPULATE_DRUG_LOCATION(myLocData.dropData[0], myArgs.dropLocations[0])
	IF bSetEnd
		POPULATE_DRUG_LOCATION(myLocData.endData[0], myArgs.endLocation[0])
		POPULATE_DRUG_LOCATION(myLocData.endData[1], myArgs.endLocation[1])
	ENDIF
	myLocData.fPickupTime = myArgs.fTimeLimit
	myLocData.fSlowTime = myArgs.fSTime
	myLocData.fTriggerNowTime = myArgs.fTNowTime
	
	IF myArgs.iNumStaticCars > 0
		myLocData.iNumCarChaserCars = myArgs.iNumStaticCars
		myLocData.iNumPedsPerCar = myArgs.iNumStaticPedsPerCar
		DEBUG_PRINT("OVERRIDING CAR CHASER DATA")
	ENDIF
	
	IF myArgs.mnPedChasers[0] <> DUMMY_MODEL_FOR_SCRIPT
		REPEAT myLocData.iNumPedsPerCar idx
			myLocData.mnPedChasers[idx] = myArgs.mnPedChasers[idx]
			DEBUG_PRINT("REPLACING PED MODELS")
		ENDREPEAT
	ENDIF
	IF myArgs.mnCarChasers[0] <> DUMMY_MODEL_FOR_SCRIPT
		REPEAT myLocData.iNumCarChaserCars idx
			myLocData.mnCarChasers[idx] = myArgs.mnCarChasers[idx]
			DEBUG_PRINT("REPLACING VEHICLE MODELS")
		ENDREPEAT
	ENDIF
	
	VALIDATE_VARIATIONS(myLocData, bDoDelicate, bTimedPickup, bDoAmbush, bDoCarChase, bDoPlane, bDoSmugglers,bDoFlareCutscene, bDoPoliceChase, bDoContactCutscene, bDoHijack, myArgs)
	CONFIGURE_DIFFICULTY_SETTINGS(myLocData, myArgs)
ENDPROC

PROC HANDLE_LAW_DIALOGUE()
	IF NOT bPlayedWantedDialogue
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, NULL, "TREVOR")
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_WANTED", CONV_PRIORITY_VERY_HIGH)
					
					PRINTLN("bPlayedWantedDialogue = TRUE")
					bPlayedWantedDialogue = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// main script
SCRIPT(ARGS myArgs)
	DEBUG_PRINT("Traffick Ground Start")
	SET_MISSION_FLAG(TRUE)
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
	
		TRIGGER_MUSIC_EVENT("OJDG_STOP")
		PRINTLN("STARTING MUSIC - OJDG_STOP - FAIL")
	
		DEBUG_PRINT("FORCE CLEANUP HAS OCCURRED: setting us up for a replay!!!")
		SET_FORCE_CLEANUP_FAIL_REASON()
		IF NOT IS_REPLAY_BEING_PROCESSED()
			SET_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_GroundTraffickingFailed)
			Setup_Minigame_Replay(GET_THIS_SCRIPT_NAME())
		ENDIF
		MISSION_CLEANUP() // cleanup and terminate thread (must be done in same frame)
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		AddWidgetsNew(myData)
//	#ENDIF
	
	//LAUNCHER Start
	IF NOT myArgs.bSpawnVehicle
		//Fill in myArgs 
		CONFIGURE_GROUND_PROGRESSION(g_savedGlobals.sTraffickingData.iGroundRank, myArgs)
			
		//Fill in Data
		POPULATE_DATA(myLocData, ENUM_TO_INT(myArgs.myLocation))
		
		//Transfer values from myArgs into myData
		CONFIGURE_OVERRIDE_DATA(myArgs, TRUE)
		
		DEBUG_PRINT("LAUNCHING VIA PROGRESSION")
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				myVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
		ENDIF
	ELSE
		//Fill in Data
		POPULATE_DATA(myLocData, ENUM_TO_INT(myArgs.myLocation))
		
		DEBUG_PRINT("LAUNCHING VIA DEBUG SCRIPT")
		
		//Transfer values from myArgs into myData
		CONFIGURE_OVERRIDE_DATA(myArgs, FALSE)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(myVehicle) 
		TELEPORT_PLAYER()
		PRINTSTRING("Teleport Player")
	ENDIF
	
//	CREATE_OFFSET_WIDGETS_NEW()
	
	myArgsCopy = myArgs
	INIT_MISSION()
	
	// General modifications
	CLEAR_AREA_OF_VEHICLES(<< 2149.3284, 4805.9575, 40.2094 >>, 100, FALSE, TRUE)
	CLEAR_AREA(<<2158.5933, 4787.7354, 40.0958>>, 5.0, TRUE) // Trying to remove guy sitting on chair behind start position.
	
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	SET_CREATE_RANDOM_COPS(FALSE)
	sbi01 = ADD_SCENARIO_BLOCKING_AREA(<< 2784.2268, 4507.9756, 45.9679 >> - <<10,10,10>>, << 2784.2268, 4507.9756, 45.9679 >> + <<10,10,10>>)
	sbi02 = ADD_SCENARIO_BLOCKING_AREA(<< 2890.9604, 4198.8589, 49.1813 >> - <<10,10,10>>, << 2890.9604, 4198.8589, 49.1813 >> + <<10,10,10>>)
	sbi01 = ADD_SCENARIO_BLOCKING_AREA(<<2030.6633, 4760.4531, 40.0605>> - <<800,800,800>>, <<2030.6633, 4760.4531, 40.0605>> + <<800,800,800>>) // Area around airfield
	sbi04 = ADD_SCENARIO_BLOCKING_AREA(myLocData.dropData[0].vCenterPos - <<50,50,50>>, myLocData.dropData[0].vCenterPos + <<50,50,50>>) // Area around drop sites

	DISABLE_CELLPHONE(TRUE)
	SET_CELLPHONE_PROFILE_TO_NORMAL()
	
//	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//		SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID()))
//		PRINTLN("SETTING PLAYER TO HAVE MAX HEALTH")
//	ENDIF
	
	// Cutscene
	cutArgs.cutsceneState = CUTSCENE_STATE_START
//	fpEndingCutscene01 = &ENDING_CUTSCENE_NEW

	IF bDoSmugglers
		fpSmugglersEnding = &SMUGGLERS_ENDING_CUTSCENE
		DEBUG_PRINT("SETTING WANTED LEVEL MULTIPLIER TO ZERO IN SMUGGLERS MODE")
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_ROADS_IN_ANGLED_AREA(<<2164.037842,4825.626953,35.613750>>, <<1899.277832,4697.752441,49.086414>>, 100.000000, FALSE, FALSE)
//		SET_ROADS_IN_ANGLED_AREA(<< 2499.300, 3456.401, -49.795>>, << 2424.300, 3456.401, 49.795>>, 125, FALSE, FALSE)
//		SET_ROADS_IN_ANGLED_AREA(<< 1751.693, 4562.989, -57.869 >>, << 769.406, 4375.608, 57.869 >>, 385, FALSE, FALSE)
		SET_ROADS_IN_ANGLED_AREA(<<1757.214600,4570.540039,30.477287>>, <<1385.558228,4495.363770,62.740479>>, 100.000000, FALSE, FALSE) // Dirt road leading to location in ground 2
	ENDIF
	
	// Way to check if we're in repeatable mode.
	IF myArgs.fTimeLimit = 1
		bInRepeatableMode = TRUE
		DEBUG_MESSAGE("bInRepeatableMode = TRUE")
	ENDIF
	
	DEBUG_PRINT_MODES(bTimedPickup, bDoCarChase, bDoDelicate, bDoAmbush, bDoPlane, bDoSmugglers, bDoFlareCutscene, 
	bDoPoliceChase, bDoContactCutscene, bDoHijack)
	
	DISPLAY_RADAR(TRUE)
	
//	IF GET_RANDOM_BOOL()
//		ambushArgs.bCreateGuysOnFoot = TRUE
//		PRINTLN("ambushArgs.bCreateGuysOnFoot = TRUE")
//	ELSE
//		ambushArgs.bCreateGuysOnFoot = FALSE
//		PRINTLN("ambushArgs.bCreateGuysOnFoot = FALSE")
//	ENDIF
	
	ambushArgs.bCreateGuysOnFoot = TRUE
	
	// Tests to run heli chase
	IF g_savedGlobals.sTraffickingData.iGroundRank >= 5
		IF GET_RANDOM_BOOL()
			bRunHeliChase = TRUE
			PRINTLN("bRunHeliChase = TRUE")
		ELSE
			bRunHeliChase = FALSE
			PRINTLN("bRunHeliChase = FALSE")
		ENDIF
	ENDIF
	
	IF g_savedGlobals.sTraffickingData.iGroundRank = 4
		bRunHeliChase = TRUE
		PRINTLN("OVERRIDE bRunHeliChase = TRUE")
	ENDIF
	
	// Cutscene
	cutArgs.cutsceneState = CUTSCENE_STATE_START
//	fpEndingCutscene01 = &ENDING_CUTSCENE_NEW
	
	PRINTLN("GROUND TRAFFICKING LEVEL = ", g_savedGlobals.sTraffickingData.iGroundRank)
	
	#IF IS_DEBUG_BUILD
		BEAT_ADD_WIDGETS()
	#ENDIF
	
	IF IS_REPLAY_IN_PROGRESS()
		g_replay.iReplayInt[0] ++		//used to strictly to track number of retries specific to trafficking
		
		iStageToUse = Get_Replay_Mid_Mission_Stage()
		PRINTLN("START - REPLAY IS IN PROGRESS, USING STAGE = ", iStageToUse)
	
		IF iStageToUse = 0
			START_REPLAY_SETUP(vSpawnPosition, fSpawnHeading)
			PRINTLN("CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ENDIF
	ELSE
		g_replay.iReplayInt[0] = 0		//reset this int since we are not replaying the mission
	ENDIF
	
	SET_SCENARIO_GROUP_ENABLED("PRISON_TOWERS", TRUE) 
	PRINTLN("ENABLING PRISON TOWERS")
	
	vCamTransitionOffsetPosition = vCamTransitionOffsetPosition
	vCamTransitionOffsetRotation = vCamTransitionOffsetRotation
	
	REMOVE_HELP_FROM_FLOW_QUEUE("MG_TRAF_AVAIL")
	
    WHILE TRUE
		// main loop	
       	WAIT(0)
			
//		UPDATE_ZVOLUME_WIDGETS()

		IF bInAmbush OR bWanted OR bIsChaseActive OR bIsSmugglersActive OR hijackArgs.bHijackActive
			bInActiveFight = TRUE
		ELSE
			bInActiveFight = FALSE
		ENDIF
		
		IF bTimedPickup AND bDisplayTimer
			UPDATE_TIMER_NEW(startTime, myLocData.fPickupTime, myLocData.fSlowTime, bTimerStarted, bTimeExpired, 
			bGiveTimeWarning, "ARMS_GR08", MyLocalPedStruct)
		ENDIF
		
		IF currentMissionStage > STAGE_DO_TAXI_CUTSCENE AND currentMissionStage < STAGE_ENDING_CUTSCENE
			IF bDrugsLoaded
				UPDATE_MISSION_CAR(myVehicle, myVehicleBlip, myEntities, myLocationBlip, vCurDestObjective, outOfVehicleTime, 
				bOutOfVehicle, bInActiveFight, bIsCutsceneActive, bDoPlane, bPrintObjAgain, FALSE, TRUE)
			ENDIF
		ENDIF
		
		IF NOT bDrugsLoaded
			IF IS_PLAYER_IN_OTHER_VEHICLE(myVehicle)
				IF NOT bPrintHelpForOtherVehicle
					CLEAR_HELP(TRUE)
				
					PRINT_HELP("DTRFKGR_HELP_17")
					bPrintHelpForOtherVehicle = TRUE
					PRINTLN("bPrintHelpForOtherVehicle = TRUE")
				ENDIF
			ENDIF
		ENDIF

		UPDATE_PRINT_DELAY(printTimer, 7, TRUE)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			// run appropriate stage
			SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_3, FALSE)
			IF UPDATE()
				MISSION_CLEANUP()
			ENDIF
		ENDIF
		
		IF currentMissionStage > STAGE_PICKUP
		AND NOT bIsSmugglersActive 
		AND NOT bIsChaseActive
		AND NOT bAmbushActive
			UPDATE_LAW(myVehicle, bWanted, myEntities, myLocationBlip, vCurDestObjective, sLastObjective, fCurDist, 
			outOfVehicleTime, myVehicleBlip, bOutOfVehicle, TRUE, FALSE, TRUE, TRUE)	
		ENDIF
		
		HANDLE_LAW_DIALOGUE()
		
		IF currentMissionStage > STAGE_GET_CAR_B
			IF UPDATE_FAIL_CONDITIONS()
				MISSION_FAILED()
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			DO_DEBUG_SKIPS()
			DEBUG_BLIP_LOCATIONS()
			IF DOES_ENTITY_EXIST(drugCargoArgs.oLoadedCargo) AND NOT IS_ENTITY_DEAD(drugCargoArgs.oLoadedCargo)
				IF NOT IS_ENTITY_DEAD(myVehicle)
					BEAT_UPDATE_WIDGETS_FOR_OBJECT(drugCargoArgs.oLoadedCargo, <<0,0,0>>, 0, myVehicle)
				ENDIF
			ENDIF
		#ENDIF
    ENDWHILE	
ENDSCRIPT
