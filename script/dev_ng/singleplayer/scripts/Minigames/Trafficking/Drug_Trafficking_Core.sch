//Steven Messinger
//Purpose: A group of functions to set up drug trafficking missions
USING "Traffick_Ground.sch"
USING "Drug_Trafficking_Data.sch"
USING "Trafficking_Progression.sch"
USING "script_oddjob_funcs.sch"
USING "minigame_uiinputs.sch"
USING "chase_hint_cam.sch"
USING "minigame_stats_tracker_helpers.sch"
USING "shared_hud_displays.sch"
USING "script_oddjob_queues.sch"
USING "script_usecontext.sch"

PROC STOP_PARTICLE_FX_SAFE(PTFX_ID myPtfx)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(myPtfx)
		STOP_PARTICLE_FX_LOOPED(myPtfx)
	ENDIF
ENDPROC

ENUM AMBUSH
	AMBUSH_INIT = 0,
	AMBUSH_CREATION,
	AMBUSH_SETUP,
	AMBUSH_REPLAY,
	AMBUSH_CREATION_BUFFER,
	AMBUSH_FIGHT,
	AMBUSH_COMPLETE
ENDENUM

ENUM PLANE_PRINTS
	PLANE_PRINTS_CAM = 0,
	PLANE_PRINTS_STEAR,
	PLANE_PRINTS_DROP,
	PLANE_PRINTS_COMPLETE
ENDENUM

ENUM PLANE_DROP
	PLANE_DROP_INIT = 0,
	PLANE_DROP_CREATE,
	PLANE_DROP_START,
	PLANE_DROP_UPDATE,
	PLANE_DROP_DROP_CARGO,
	PLANE_DROP_DELETE_PARACHUTE,
	PLANE_DROP_DELETE,
	PLANE_DROP_COMPLETE
ENDENUM

ENUM PLANE_STATE
	PLANE_FADE_INTRO = 0,
	PLANE_FADE_WAIT,
	PLANE_STATE_TAXI,
	PLANE_STATE_PRINTS,
	PLANE_STATE_DROPOFF,
	PLANE_STATE_LAND,
	PLANE_STATE_CUTSCENE_PREPARE,
	PLANE_STATE_CUTSCENE,
	PLANE_STATE_DONE,
	PLANE_STATE_FAILED,
	PLANE_STATE_FINAL
ENDENUM

ENUM STATIC_CHASE_STATE
	STATIC_CHASE_STATE_01 = 0,
	STATIC_CHASE_STATE_02,
	STATIC_CHASE_STATE_03
ENDENUM

ENUM HELI_STATE
	HELI_STATE_01 = 0,
	HELI_STATE_02
ENDENUM

ENUM TRAP_STATE
	TRAP_INIT = 0,
	TRAP_STATE_01,
	TRAP_STATE_02,
	TRAP_STATE_03,
	TRAP_STATE_04,
	TRAP_STATE_05,
	TRAP_STATE_06,
	TRAP_STATE_07,
	TRAP_STATE_08,
	TRAP_STATE_09,
	TRAP_STATE_10
ENDENUM

ENUM CREATE_STATIC_CHASE_STATE
	CREATE_STATIC_CHASE_STATE_01 = 0,
	CREATE_STATIC_CHASE_STATE_02
ENDENUM

// Particle effects
PTFX_ID PTFXsmugglersFlare

CONST_FLOAT TRAF_BLIP_SIZE_LARGE 1.35
CONST_FLOAT TRAF_BLIP_SIZE_SMALL 0.9
CONST_INT maxDistance 100000 //for now no max distance
CONST_INT minDistance 50
CONST_INT BLOW_UP_TIME 12000 // 12 seconds
CONST_INT iAmbushAbandonDistance 300
CONST_INT iAbandonDistanceAddOn 800
CONST_INT DISTANCE_TO_LOSE_AMBUSHERS 300
CONST_INT DROP_RADIUS 60
CONST_INT PERFECT_DROP_RADIUS 15
CONST_INT TRAP_NOT_TRIGGERED_TIME 45000// 45 seconds, may need to be set per location?
CONST_INT STUCK_TIME 6
CONST_INT FAIL_TIME 20000 // 60 seconds

VECTOR vParchuteOffset = <<0,0,0.18>>
VECTOR vTempCurDestObjective

FLOAT fDistanceToObjective
FLOAT fAbandonedDistance
FLOAT fWarningDistance
FLOAT fTimeLeft

INT iWarningStages = 0

BOOL bBelowLowSpeed = FALSE
BOOL bCanTeleport = FALSE
BOOL bGrabbedDistance = FALSE
BOOL bVectorsExist = FALSE
BOOL bRanTrapOnce = FALSE
BOOL bPrintReturnWarning = FALSE
BOOL bGaveAbandondedWarning = FALSE

// GANG TYPES
BOOL bUsingMexicans = FALSE
BOOL bUsingMarabunta = FALSE
BOOL bUsingHillbillies = FALSE

BOOL bOkayToPrintAmbusherObjective = FALSE

MODEL_NAMES mnBombProp = Prop_Drop_ArmsCrate_01b
MODEL_NAMES mnParachuteProp = P_CARGO_CHUTE_S

structTimer TeleportTimer
structTimer printTimer
structTimer tBeepTimer
structTimer tWarpTimer

AI_BLIP_STRUCT blipAmbushers[10]
AI_BLIP_STRUCT blipAmbushAttackers[MAX_NUMBER_AMBUSH_ATTACKERS]

VECTOR vCenterTrapPos = << 1670.6827, 4839.6680, 41.0695 >>

VECTOR vCamTransitionOffsetPosition = <<0.6120, -30.7126, 6.6732>> //<<2.6242, -15.5869, -0.0995>>
VECTOR vCamTransitionOffsetRotation = <<0.6626, -27.7333, 6.3245>> //<<2.3207, -12.6258, 0.2739>>
									

VEHICLE_INDEX vehEnemyVehicle

CAMERA_INDEX camGroundOneHintCam

PED_INDEX tempPed


// Widgets
//FLOAT fOffsetX, fOffsetY, fOffsetZ
//FLOAT fHeadingX, fHeadingY, fHeadingZ

FUNC BOOL IS_VEHICLE_ALMOST_STOPPED_GROUND(VEHICLE_INDEX invehicle, FLOAT fGoodSpeed = 1.5)
    FLOAT fTemp
    IF NOT IS_ENTITY_DEAD(invehicle)
        fTemp = GET_ENTITY_SPEED(invehicle)
		PRINTLN("fTemp = ", fTemp)
        IF (fTemp > -1.5)
        AND (fTemp < fGoodSpeed)
            RETURN(TRUE)
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

FUNC BOOL DO_CUTSCENE_CUSTOM_GROUND(CustomCutsceneCaller fpCutsceneCustom)
	IF CALL fpCutsceneCustom()
		PRINTLN("RETURNING TRUE ON DO_CUTSCENE_CUSTOM_GROUND")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_MODELS_LOADED(MODEL_NAMES& modelArray[], INT size = 0)
	INT idx
	IF size = 0
		size = COUNT_OF(modelArray)
	ENDIF
	REPEAT size idx
		IF NOT HAS_MODEL_LOADED(modelArray[idx])
			RETURN FALSE
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_VALID_VEHICLE(VEHICLE_INDEX& myVehicle)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) <> myVehicle
//				DEBUG_MESSAGE("IS_PLAYER_IN_VALID_VEHICLE returning FALSE")
				RETURN FALSE
			ENDIF
		ELSE
//			DEBUG_MESSAGE("IS_PLAYER_IN_VALID_VEHICLE returning FALSE because not in ANY vehicle")
			RETURN FALSE
		ENDIF
	ENDIF
//	DEBUG_PRINT("True")
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_OTHER_VEHICLE(VEHICLE_INDEX& myVehicle)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) <> myVehicle
				DEBUG_MESSAGE("IS_PLAYER_IN_OTHER_VEHICLE returning TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_DISTANCE_TO_NEXT_OBJECTIVE(VECTOR& vCurDestObjective[])
	
	VECTOR vPlayerPos
	
	// Grab player's position.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	fDistanceToObjective = VDIST(vPlayerPos, vCurDestObjective[0])
	
	RETURN fDistanceToObjective
ENDFUNC

FUNC BOOL PLAYER_HAS_ABANDONED_JOB(VECTOR& vCurDestObjective[], structPedsForConversation& MyLocalPedStruct)//, LOCATION_DATA& myLocData)
	
//	VECTOR vPlayerPos
	
//	// Grab player's position.
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
//	ENDIF

	// Need to check an updated vCurDestObjective... if it changes, go ahead and allow a new fAbandonedDistance to be grabbed.
	IF NOT ARE_VECTORS_EQUAL(vTempCurDestObjective, vCurDestObjective[0])
		bGrabbedDistance = FALSE
		DEBUG_PRINT("SETTING bGrabbedDistance TO FALSE")
	ENDIF
	
	// If vCurDestObjective has been set, check the distance between the player and objective.
	IF NOT ARE_VECTORS_EQUAL(vCurDestObjective[0], <<0,0,0>>)
		// Grab distance to next objective
		fDistanceToObjective = GET_DISTANCE_TO_NEXT_OBJECTIVE(vCurDestObjective)
	
		IF NOT bGrabbedDistance
			// Add on abandon distance
			fAbandonedDistance = fDistanceToObjective + TO_FLOAT(iAbandonDistanceAddOn)
			fWarningDistance = fDistanceToObjective + 300
			PRINTLN("fWarningDistance = ", fWarningDistance)
			
			PRINTSTRING("fDistanceToObjective = ")
			PRINTFLOAT(fDistanceToObjective)
			PRINTNL()
			
			PRINTSTRING("fAbandonedDistance = ")
			PRINTFLOAT(fAbandonedDistance)
			PRINTNL()
			
			// Store current destination object to check if it changes
			vTempCurDestObjective = vCurDestObjective[0]
		
			bGrabbedDistance = TRUE
		ENDIF
	ENDIF
	
	IF fDistanceToObjective > fWarningDistance
		IF NOT bGaveAbandondedWarning
			CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AB_WRN", CONV_PRIORITY_VERY_HIGH)
			PRINTLN("bGaveAbandondedWarning = TRUE")
			bGaveAbandondedWarning = TRUE
		ENDIF
	ENDIF
	
	IF fDistanceToObjective > fAbandonedDistance
		PRINTLN("fDistanceToObjective = ", fDistanceToObjective)
		PRINTLN("fAbandonedDistance = ", fAbandonedDistance)
		DEBUG_PRINT("RETURNING TRUE ON ABANDON DISTANCE - NORMAL")
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
//	ENDIF

ENDFUNC

PROC GET_VALID_VEHICLE_OFFSETS(VEHICLE_INDEX vehMyVehicle, DRUG_CARGO_ARGS& drugCargoArgs)
	
	MODEL_NAMES vehModelName
	vehModelName = GET_ENTITY_MODEL(vehMyVehicle)

	// TODO: create valid prop offsets for each vehicle, once a proper prop/vehicle list is defined.
	SWITCH vehModelName
		CASE BLAZER
			drugCargoArgs.vLoadedCargoOffset = <<0, -1.560, 0.900>>
			drugCargoArgs.vLoadedCargoRot = <<0, 0, 0>>
		BREAK
		CASE SADLER
			drugCargoArgs.vLoadedCargoOffset = <<0, -1.560, 0.900>>
			drugCargoArgs.vLoadedCargoRot = <<0, 0, 0>>
		BREAK
		CASE MESA
			drugCargoArgs.vLoadedCargoOffset = <<0, -1.560, 0.400>>
			drugCargoArgs.vLoadedCargoRot = <<0, 0, 90>>
		BREAK
		CASE DUBSTA
			drugCargoArgs.vLoadedCargoOffset = <<0, -1.560, 0.900>>
			drugCargoArgs.vLoadedCargoRot = <<0, 0, 0>>
		BREAK
		CASE BISON
			drugCargoArgs.vLoadedCargoOffset = <<0, -1.560, 0.900>>
			drugCargoArgs.vLoadedCargoRot = <<0, 0, 0>>
		BREAK
		CASE DLOADER
			drugCargoArgs.vLoadedCargoOffset = <<0, -1.560, 0.590>>
			drugCargoArgs.vLoadedCargoRot = <<0, 0, 0>>
		BREAK
		CASE DUNE
			drugCargoArgs.vLoadedCargoOffset = <<0.300, 0.350, 0.040>>
			drugCargoArgs.vLoadedCargoRot = <<0, 0, 90>>
		BREAK
		CASE REBEL
			drugCargoArgs.vLoadedCargoOffset = <<0, -2.0, 0.500>>
			drugCargoArgs.vLoadedCargoRot = <<0, 0, 90>>
			DEBUG_PRINT("USING REBEL OFFSETS")
		BREAK
	ENDSWITCH
	
	DEBUG_PRINT("GRABBED OFFSETS FOR VEHICLE MODEL")
	
ENDPROC

PROC ADD_DRUGS_TO_VEHICLE(DRUG_CARGO_ARGS& drugCargoArgs, LOCATION_DATA& myLocData, VEHICLE_INDEX vehMyVehicle, BOOL bPlaySound = TRUE)

	MODEL_NAMES vehModelName
	vehModelName = GET_ENTITY_MODEL(vehMyVehicle)
	vehModelName = vehModelName

	GET_VALID_VEHICLE_OFFSETS(vehMyVehicle, drugCargoArgs)
	
	// Create drug packaga
	IF DOES_ENTITY_EXIST(vehMyVehicle) AND IS_VEHICLE_DRIVEABLE(vehMyVehicle)
		IF NOT DOES_ENTITY_EXIST(drugCargoArgs.oLoadedCargo)
			drugCargoArgs.oLoadedCargo = CREATE_OBJECT(myLocData.mnLoadedCargo, GET_ENTITY_COORDS(vehMyVehicle))
			SET_ENTITY_RECORDS_COLLISIONS(drugCargoArgs.oLoadedCargo, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(drugCargoArgs.oLoadedCargo) AND IS_VEHICLE_DRIVEABLE(vehMyVehicle)
		ATTACH_ENTITY_TO_ENTITY(drugCargoArgs.oLoadedCargo, vehMyVehicle, 0, drugCargoArgs.vLoadedCargoOffset, drugCargoArgs.vLoadedCargoRot)
		PRINTLN("USING REGULAR OFFSETS AND ROTATIONS")
	ENDIF
	
	IF bPlaySound
		PLAY_SOUND_FRONTEND(-1, "PICK_UP_WEAPON", "HUD_FRONTEND_CUSTOM_SOUNDSET")
		PRINTLN("PLAYING SOUND")
	ELSE
//		PLAY_SOUND_FRONTEND(-1, "PICK_UP_WEAPON", "HUD_FRONTEND_CUSTOM_SOUNDSET")
//		PRINTLN("PLAYING SOUND")
		PLAY_SOUND_FROM_ENTITY(-1, "TRAFFIC_GROUND_ENEMY_PICK_UP_WEAPON_MASTER", vehMyVehicle)
		PRINTLN("PLAYING SOUND")
	ENDIF
	
ENDPROC

PROC REMOVE_DRUG_CARGO(DRUG_CARGO_ARGS& drugCargoArgs)
	
	IF DOES_ENTITY_EXIST(drugCargoArgs.oLoadedCargo)
		DELETE_OBJECT(drugCargoArgs.oLoadedCargo)
	ENDIF
	
ENDPROC

//Function to check if the bad guys are out of range of their original location so the player doesn't have to go hunt them
FUNC BOOL IS_BADGUY_OOR(PED_INDEX myPed, VECTOR vDropLoc)
	VECTOR temp
	
	IF NOT IS_PED_INJURED(myPed)
		temp = GET_ENTITY_COORDS(myPed)
		//the bad guy somehow got 25m below or above the original location.. this is a check that should handle him falling down a cliff
		IF ABSF(temp.z - vDropLoc.z) > 25
			TASK_SMART_FLEE_PED(myPed, PLAYER_PED_ID(), 250, 20000)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC UPDATE_TIMER_NEW(INT& startTime, FLOAT& fPickupTime, FLOAT& fSlowTime, BOOL& bTimerStarted, 
BOOL& bTimeExpired, BOOL& bGiveTimeWarning, STRING dialougeLine, structPedsForConversation& MyLocalPedStruct)
	
	// Grab starting game time
	IF NOT bTimerStarted
		startTime = GET_GAME_TIMER()
		PRINTLN("fPickupTime = ", fPickupTime)
		PRINTLN("fSlowTime = ", fSlowTime)
		bTimerStarted = TRUE
	ENDIF
	
	// Variable time 
	INT timePassed = GET_GAME_TIMER() - startTime
	
	IF timePassed > fPickupTime
		bTimeExpired = TRUE
	ENDIF
	
//	PRINTSTRING("TIME LEFT = ")
//	PRINTINT((ROUND(fPickupTime) - timePassed))
//	PRINTNL()
	
	IF fSlowTime = -1
		PRINTLN("fSlowTime NOT DEFINED SETTING TO DEFAULT VALUE")
		fSlowTime = 30000
		PRINTLN("fSlowTime = ", fSlowTime)
	ENDIF
	
	fTimeLeft = (fPickupTime - TO_FLOAT(timePassed))
	fTimeLeft = fTimeLeft
//	PRINTLN("fTimeLeft = ", fTimeLeft)
		
	IF NOT bGiveTimeWarning
		IF (ROUND(fPickupTime) - timePassed) <= fSlowTime 
			CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", dialougeLine, CONV_PRIORITY_VERY_HIGH)
			DEBUG_PRINT("GIVING PLAYER TIME WARNING")
			bGiveTimeWarning = TRUE
		ENDIF
	ENDIF

	INT iTimer = (ROUND(fPickupTime) - timePassed)
//	PRINTLN("iTimer = ", iTimer)
	
	IF iTimer <= 11000 AND iTimer >= 5000
		IF IS_TIMER_STARTED(tBeepTimer)
			IF GET_TIMER_IN_SECONDS(tBeepTimer) > 1
				PLAY_SOUND_FRONTEND(-1, "TIMER", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				RESTART_TIMER_NOW(tBeepTimer)
				PRINTLN("PLAYING BEEP")
			ENDIF
		ELSE
			START_TIMER_NOW(tBeepTimer)
			PRINTLN("STARTING TIMER - tBeepTimer - 01")
		ENDIF
	ELIF iTimer <= 5000 AND iTimer > 0
		IF IS_TIMER_STARTED(tBeepTimer)
			IF GET_TIMER_IN_SECONDS(tBeepTimer) > 0.5
				PLAY_SOUND_FRONTEND(-1, "TIMER", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				RESTART_TIMER_NOW(tBeepTimer)
				PRINTLN("PLAYING BEEP")
			ENDIF
		ELSE
			START_TIMER_NOW(tBeepTimer)
			PRINTLN("STARTING TIMER - tBeepTimer - 02")
		ENDIF
	ENDIF
		
	DRAW_GENERIC_TIMER(iTimer, "DTRFKGR_TIME")
	
//	DRAW_TIMER_HUD((ROUND(fPickupTime) - timePassed), ROUND(fPickupTime), "DTRSHRD_TIMEG")
	
ENDPROC

PROC UPDATE_TIMER_NEW_PLANE(INT& startTime, FLOAT& fPickupTime, BOOL& bTimerStarted, 
BOOL& bTimeExpired, BOOL& bGiveTimeWarning, STRING dialougeLine, structPedsForConversation& MyLocalPedStruct)
	
	// Grab starting game time
	IF NOT bTimerStarted
		startTime = GET_GAME_TIMER()
		PRINTLN("fPickupTime = ", fPickupTime)
		bTimerStarted = TRUE
	ENDIF
	
	// Variable time 
	INT timePassed = GET_GAME_TIMER() - startTime
	
	IF timePassed > fPickupTime
		bTimeExpired = TRUE
	ENDIF
	
//	PRINTSTRING("TIME LEFT = ")
//	PRINTINT((ROUND(fPickupTime) - timePassed))
//	PRINTNL()
	
	IF NOT bGiveTimeWarning
		IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
			IF (ROUND(fPickupTime) - timePassed) <= 30000
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", dialougeLine, CONV_PRIORITY_VERY_HIGH)
				DEBUG_PRINT("GIVING PLAYER TIME WARNING")
				bGiveTimeWarning = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	INT iTimer = (ROUND(fPickupTime) - timePassed)
	
	DRAW_GENERIC_TIMER(iTimer, "DTRSHRD_TIME")
ENDPROC

PROC UPDATE_TIMER(INT& startTime, FLOAT& fPickupTime, BOOL& bTimerStarted, BOOL& bTimeExpired)
	
	FLOAT timeLeft
	
	INT minutes
	INT seconds
	
	IF NOT bTimerStarted
		startTime = GET_GAME_TIMER()
		bTimerStarted = TRUE
	ENDIF
	
	INT timePassed = GET_GAME_TIMER() - startTime
	IF timePassed > fPickupTime
		SET_TEXT_SCALE(0.5, 0.5)      
		DISPLAY_TEXT_WITH_3_NUMBERS(0.15, 0.46, "DTRSHRD_TIMER", 0, 0, 0)
		bTimeExpired = TRUE
	ELSE
		timeLeft = fPickupTime - timePassed
		
		minutes = FLOOR(timeLeft / 60000)
		seconds = FLOOR(timeLeft - (minutes * 60000) )
		seconds = seconds / 1000
		
		SET_TEXT_SCALE(0.5, 0.5)      
		
		INT iZeroPlace = 0
		
		// If we're less than 10 seconds, we need a place holder zero.
		IF seconds < 10
			IF timeLeft < 25000
				SET_TEXT_COLOUR(255, 0, 0, 255)
				DISPLAY_TEXT_WITH_3_NUMBERS(0.092, 0.6920, "DTRSHRD_TIMERA", minutes, iZeroPlace, seconds)
			ELSE
				SET_TEXT_COLOUR(255, 255, 255, 255)
				DISPLAY_TEXT_WITH_3_NUMBERS(0.092, 0.6920, "DTRSHRD_TIMERA", minutes, iZeroPlace, seconds)
			ENDIF
		ELSE
			IF timeLeft < 25000
				SET_TEXT_COLOUR(255, 0, 0, 255)
				DISPLAY_TEXT_WITH_2_NUMBERS(0.092, 0.6920, "DTRSHRD_TIMER", minutes, seconds)
			ELSE
				SET_TEXT_COLOUR(255, 255, 255, 255)
				DISPLAY_TEXT_WITH_2_NUMBERS(0.092, 0.6920, "DTRSHRD_TIMER", minutes, seconds)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC AddWidgets(DEBUG_POS_DATA& myData)
	UNUSED_PARAMETER(myData)
#IF IS_DEBUG_BUILD
	START_WIDGET_GROUP("Ground Transport")
	ADD_WIDGET_BOOL("turn on positioning", myData.bTurnOnDebugPos)
	ADD_WIDGET_VECTOR_SLIDER("DebugPosition", myData.vDebugVector, -15, 15, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("DebugRotation", myData.fDebugFloat, -360, 360, 1.0)
	
	ADD_WIDGET_FLOAT_SLIDER("DebugX", myData.fDebugX, 0, 1, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("DebugY", myData.fDebugY, 0, 1, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("DebugWidth", myData.fdebugWidth, 0, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("DebugHeight", myData.fdebugHeight, 0, 2, 0.01)
	
	ADD_WIDGET_FLOAT_SLIDER("DebugX2", myData.fDebugX2, 0, 1, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("DebugY2", myData.fDebugY2, 0, 1, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("DebugWidth2", myData.fdebugWidth2, 0, 2, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("DebugHeight2", myData.fdebugHeight2, 0, 2, 0.01)
	
	ADD_WIDGET_FLOAT_SLIDER("RotateX", myData.fRotateX, -360, 360, 1)
	ADD_WIDGET_FLOAT_SLIDER("RotateY", myData.fRotateY, -360, 360, 1)
	ADD_WIDGET_FLOAT_SLIDER("RotateZ", myData.fRotateZ, -360, 360, 1)
	ADD_WIDGET_FLOAT_SLIDER("OffsetX", myData.fOffsetX, -10, 10, 0.1)
	
	ADD_WIDGET_FLOAT_SLIDER("OffsetZ", myData.fOffsetZ, -10, 10, 0.1)
	
	ADD_WIDGET_FLOAT_SLIDER("FOV", myData.fFOV, 0, 125, 0.1)
	
	STOP_WIDGET_GROUP()
	
//	START_WIDGET_GROUP("Carpet Bomb Camera")
//		ADD_WIDGET_BOOL("Turn on Carpet Bomb Update", myData.bTurnOnCameraPos)
//		
//		ADD_WIDGET_FLOAT_SLIDER("Offset Position X", myData.fDebugX, -360, 360, 0.5)
//		ADD_WIDGET_FLOAT_SLIDER("Offset Position Y", myData.fDebugY, -360, 360, 0.5)
//		ADD_WIDGET_FLOAT_SLIDER("Offset Position Z", myData.fDebugZ, -360, 360, 0.5)
//		
//		ADD_WIDGET_FLOAT_SLIDER("Offset Rotation X", myData.fRotateX, -360, 360, 0.5)
//		ADD_WIDGET_FLOAT_SLIDER("Offset Rotation Y", myData.fRotateY, -360, 360, 0.5)
//		ADD_WIDGET_FLOAT_SLIDER("Offset Rotation Z", myData.fRotateZ, -360, 360, 0.5)
//	STOP_WIDGET_GROUP()
#ENDIF
ENDPROC

PROC RunWidgets(DEBUG_POS_DATA myData)
	VECTOR temp 
	IF myData.bTurnOnDebugPos
		temp = myData.myObjectPos + myData.vDebugVector
		SET_ENTITY_COORDS(myData.myObject, temp)
	ENDIF
//	IF myData.bTurnOnCameraPos
//		vCamTransitionOffsetPosition.x = myData.fDebugX
//		vCamTransitionOffsetPosition.y = myData.fDebugY
//		vCamTransitionOffsetPosition.z = myData.fDebugZ
//		
//		vCamTransitionOffsetRotation.x = myData.fRotateX
//		vCamTransitionOffsetRotation.y = myData.fRotateY
//		vCamTransitionOffsetRotation.z = myData.fRotateZ
//	ENDIF
ENDPROC

PROC UPDATE_HEALTH_BAR(FLOAT healthPoints, STRING sLabel)
	SET_TEXT_SCALE(0.4, 0.4)
	DISPLAY_TEXT_WITH_STRING(HEALTH_TEXT_X,HEALTH_TEXT_Y, sLabel, sLabel)
	IF healthPoints < 0
		healthPoints = 0
	ENDIF
	//BackGround
	DRAW_RECT_FROM_CORNER(HEALTH_BAR_X, HEALTH_BAR_Y, HEALTH_BAR_W, HEALTH_BAR_H, 1,0,0,255)
	FLOAT fCurHealth = (HEALTH_BAR_W * healthPoints) / 100
	//health
	DRAW_RECT_FROM_CORNER(HEALTH_BAR_X, HEALTH_BAR_Y, fCurHealth, HEALTH_BAR_H, 100,255,100,255)
ENDPROC

PROC UPDATE_HEALTH(BOOL bMonitor, FLOAT& curHealth, INT& lastHealth)
	VEHICLE_INDEX myVehicle
	FLOAT totalDamage = 0.0
	FLOAT fEngineHealth
	FLOAT vehSpeed
	VECTOR vPlayerVehiclePos
	VECTOR vNodePos
	
	IF bMonitor
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			myVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(myVehicle)
				vehSpeed = GET_ENTITY_SPEED(myVehicle)
								
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(myVehicle) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
						vPlayerVehiclePos = GET_ENTITY_COORDS(myVehicle)
						GET_CLOSEST_VEHICLE_NODE(vPlayerVehiclePos, vNodePos)
					ENDIF
				ENDIF
				
				// If the player is far from a vehicle node and going at a decent speed, give a little damage.
				IF VDIST(vPlayerVehiclePos, vNodePos) > 20.0
					IF vehSpeed > 12.0
						totalDamage += 0.05
					ENDIF
				ENDIF
				
				INT fHealthDelta
				fHealthDelta = lastHealth - GET_ENTITY_HEALTH(myVehicle)
				
				// Total hack... need a metric that can be decremented for chemical stability meter.
				IF GET_ENTITY_HEALTH(myVehicle) = 0
					SET_ENTITY_HEALTH(myVehicle, 100)
				ENDIF
				
				// Another hack... if the engine health is almost done... add massive damage to the meter, so you don't have green left in the meter with a blown up car.
				fEngineHealth = GET_VEHICLE_ENGINE_HEALTH(myVehicle)
				IF fEngineHealth < 10
					totalDamage += 10
				ENDIF
				
				// This doesn't seem to work???
//				IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 15.0)
//					DEBUG_PRINT("BULLETS ARE IMPACTED, INFLICTING DAMAGE")
//					totalDamage += 0.75
//				ENDIF
				
				//there has been some damage.. apply it
				IF fHealthDelta > 0
					lastHealth = GET_ENTITY_HEALTH(myVehicle)
					DEBUG_PRINT("Vehicle has been damaged.. Applying:")
					PRINTINT(fHealthDelta/16)
					PRINTNL()
					totalDamage += fHealthDelta/16
				ENDIF
				
				//if the car is in the airr.. apply damage
				IF NOT IS_VEHICLE_ON_ALL_WHEELS(myVehicle)
					DEBUG_PRINT("Vehicle is in the air.. Applying:")
					PRINTFLOAT(1.0)
					PRINTNL()
					IF vehSpeed > 30
						totalDamage += 0.25
					ELIF vehSpeed > 15
						totalDamage += 0.15
					ELSE
						totalDamage += 0.05
					ENDIF
				ENDIF
				
				IF IS_ENTITY_UPSIDEDOWN(myVehicle)
					totalDamage += 0.45
				ENDIF
				
//				PRINTSTRING("VEHICLE SPEED = ")
//				PRINTFLOAT(vehSpeed)
//				PRINTNL()
				
				//if the car is going above a high speed apply damage
				IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
						IF vehSpeed > 35
							DEBUG_PRINT("Vehicle is going too fast.. Applying:")
							PRINTFLOAT(2.0)
							PRINTNL()
							totalDamage += 0.25
						
							PRINT_HELP("DTRFKGR_HELP_12")
						ENDIF
					ENDIF
				ENDIF
				
				curHealth -= totalDamage
		
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL UPSIDE_DOWN_CHECK(VEHICLE_INDEX myVehicle, BOOL& bIsUpsideDown, INT& upsideDownVehicleTime, FAIL_REASON_ENUM& failReason)
	IF NOT IS_ENTITY_DEAD(myVehicle)
		IF IS_ENTITY_UPSIDEDOWN(myVehicle) AND GET_ENTITY_HEIGHT_ABOVE_GROUND(myVehicle) < 2.0
			IF NOT bIsUpsideDown
				bIsUpsideDown = TRUE
				upsideDownVehicleTime = GET_GAME_TIMER()
			ENDIF
		ELSE
			bIsUpsideDown = FALSE
			upsideDownVehicleTime = 0
		ENDIF
		
		IF bIsUpsideDown
			IF (GET_GAME_TIMER() - upsideDownVehicleTime) > BLOW_UP_TIME
				//EXPLODE_VEHICLE(myVehicle)
				failReason = FAIL_VEHICLE_DISABLED
				RETURN TRUE
			ENDIF	
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//TODO>  This function has become a mess because it has so much custom shit for specific variations.  The idial solution is having a shared "update_mission_car" that's called by a 
//Variations specific update_mission_car_air.  update_mission_car_air would have anything custom to air and keep the generic update_mission_car simple and pure.

//TODO do a better champ with passing in blips/bools (bInRangeForPerfect) based on variation.
PROC UPDATE_MISSION_CAR(VEHICLE_INDEX& myVehicle, BLIP_INDEX& myVehicleBlip, VEHICLE_INDEX& myEntities[], BLIP_INDEX& myLocationBlip[], 
VECTOR& vCurDestObjective[], INT& outOfVehicleTime, BOOL& bOutOfVehicle, BOOL& bInActiveFight, BOOL& bIsCutsceneActive, 
BOOL& bPlaneMission, BOOL& bPrintObjAgain, BOOL bRed = FALSE, BOOL bPrintWarning = TRUE, BOOL bReBlip = TRUE, BOOL bReBlipRoute = TRUE)
	
	INT idx, closestIndex
	VECTOR vPlayerPos
	
	UNUSED_PARAMETER(bInActiveFight)

	// If we're doing a plane mission, grab the closest location to reduce the size of the blip.
	IF bPlaneMission
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		closestIndex = FIND_CLOSEST_LOCATION_INDEX(vCurDestObjective, vPlayerPos)
	ENDIF
	
	IF bIsCutsceneActive OR (NOT IS_ENTITY_DEAD(myVehicle) AND IS_ENTITY_IN_AIR(myVehicle))
	OR (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
		IF DOES_BLIP_EXIST(myVehicleBlip) AND IS_PLAYER_IN_VALID_VEHICLE(myVehicle)
			REMOVE_BLIP(myVehicleBlip)
		ENDIF
		//at least update the in / out status of the player
		bOutOfVehicle = NOT IS_PLAYER_IN_VALID_VEHICLE(myVehicle)
		EXIT
	ENDIF
	
	IF NOT bOutOfVehicle
		IF NOT IS_PLAYER_IN_VALID_VEHICLE(myVehicle)
			
			CLEAR_PRINT_DELAY(printTimer)
			IF IS_HELP_MESSAGE_ON_SCREEN()
				CLEAR_HELP()
				DEBUG_PRINT("CLEARING HELP VIA UPDATE_MISSION_CAR")
			ENDIF
			CLEAR_PRINTS()
			
			// Record time for abandoned car check
			outOfVehicleTime = GET_GAME_TIMER()
			PRINTLN("outOfVehicleTime = ", outOfVehicleTime)
			
			IF bPrintWarning
				IF bPrintObjAgain
					IF bPlaneMission
						PRINT_NOW("DTRSHRD_03",DEFAULT_GOD_TEXT_TIME,1)
						bPrintObjAgain = FALSE
						PRINTLN("PRINTING OBJECTIVE TO RETURN TO PLANE VIA UPDATE MISSION CAR")
					ELSE
						PRINT_NOW("DTRSHRD_03",DEFAULT_GOD_TEXT_TIME,1)
						bPrintObjAgain = FALSE
						PRINTLN("PRINTING OBJECTIVE TO RETURN TO VEHICLE VIA UPDATE MISSION CAR")
					ENDIF
				ENDIF
				myVehicleBlip = ADD_BLIP_FOR_ENTITY(myVehicle)
				SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
			ENDIF
			
			REPEAT COUNT_OF(myLocationBlip) idx 
				IF DOES_BLIP_EXIST(myLocationBlip[idx])
//					SET_BLIP_ROUTE(myLocationBlip[idx], FALSE)
					REMOVE_BLIP(myLocationBlip[idx])
					DEBUG_PRINT("REMOVING myLocationBlip")
				ENDIF
			ENDREPEAT
				
			DEBUG_PRINT("bOutOfVehicle IS TRUE")
			bOutOfVehicle = TRUE
		ENDIF
	ELSE
		IF IS_PLAYER_IN_VALID_VEHICLE(myVehicle)
			//bDisplayTimer = FALSE
			REMOVE_BLIP(myVehicleBlip)
			PRINTLN("REMOVING BLIP - myVehicleBlip VIA UPDATE MISSION CAR")
			
			CLEAR_PRINTS()
						
			IF bReBlip
				REPEAT COUNT_OF(myLocationBlip) idx 
					IF NOT DOES_BLIP_EXIST(myLocationBlip[idx])
					//AND NOT DOES_BLIP_EXIST(blipRadiusPerfect[idx])
					AND NOT ARE_VECTORS_EQUAL(vCurDestObjective[idx], <<0,0,0>>)
				
						bVectorsExist = TRUE
						
						IF NOT bPlaneMission AND bReBlipRoute
							myLocationBlip[idx] = ADD_BLIP_FOR_COORD(vCurDestObjective[idx])
//							SET_BLIP_ROUTE(myLocationBlip[idx], TRUE)
							DEBUG_PRINT("ADDING BACK IN myLocationBlip")
						ENDIF
						
						// If we're doing a plane mission... 
						IF bPlaneMission
							DEBUG_PRINT("bPlaneMission IS TRUE")
							myLocationBlip[idx] = ADD_BLIP_FOR_COORD(vCurDestObjective[idx])
							
							//colour red if bombinb and we're not meant to land.
							IF bRed
								SET_BLIP_COLOUR(myLocationBlip[idx], BLIP_COLOUR_RED)
							ENDIF
							
							IF idx = closestIndex
								SET_BLIP_SCALE(myLocationBlip[idx], 1.0)
							ELSE
								SET_BLIP_SCALE(myLocationBlip[idx], 0.75)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF NOT bVectorsExist and bReBlip
					REPEAT COUNT_OF(myEntities) idx 
						IF DOES_ENTITY_EXIST(myEntities[idx]) AND NOT IS_ENTITY_DEAD(myEntities[idx])
							myLocationBlip[idx] = ADD_BLIP_FOR_ENTITY(myEntities[idx])
							DEBUG_PRINT("ADDING IN BLIPS FOR ENTITIES")
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
			
			outOfVehicleTime = 0
			
			DEBUG_PRINT("bOutOfVehicle IS FALSE")
			bOutOfVehicle = FALSE
		ELSE
			//still not in vehicle
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HANDLE_OUT_OF_VEHICLE_WARNING_AND_FAIL(STRING sFailText, VEHICLE_INDEX myVehicle, INT outOfVehicleTime)
	VECTOR vPlayerPosition, vCarPosition
	#IF IS_DEBUG_BUILD
		FLOAT fTempDistance
	#ENDIF
	vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
				
	IF NOT IS_ENTITY_DEAD(myVehicle)
		vCarPosition = GET_ENTITY_COORDS(myVehicle)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		fTempDistance = VDIST(vPlayerPosition, vCarPosition)
		fTempDistance = fTempDistance
//		PRINTLN("DISTANCE FROM CAR = ", fTempDistance)
	#ENDIF
	
	//if the player has been out of the vehicle for more than 60 seconds fail him
	SWITCH iWarningStages 
		CASE 0
			IF VDIST2(vPlayerPosition, vCarPosition) > 625 // 25m
				IF NOT bPrintReturnWarning
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						
						PRINTLN("PRINTING OBJECTIVE TO RETURN TO VEHICLE VIA HANDLE_OUT_OF_VEHICLE_WARNING_AND_FAIL")
						PRINT_NOW(sFailText, DEFAULT_GOD_TEXT_TIME, 1)
						bPrintReturnWarning = TRUE
						
						iWarningStages = 1
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//-----------------------------------------------------------------------------
		CASE 1
			IF VDIST2(vPlayerPosition, vCarPosition) > 2500 // 50m
				IF (GET_GAME_TIMER() - outOfVehicleTime) > FAIL_TIME
					PRINTLN("RETURNING TRUE - HANDLE_OUT_OF_VEHICLE_WARNING_AND_FAIL")
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_LAW(VEHICLE_INDEX& myVehicle, BOOL& bWanted, VEHICLE_INDEX& myEntities[], BLIP_INDEX& myLocationBlip[], VECTOR& vCurDestObjective[], 
TEXT_LABEL_31& sLastObjective, FLOAT& fCurDist, INT& outOfVehicleTime, BLIP_INDEX& myVehicleBlip, BOOL& bOutOfVehicle, 
BOOL bSetRoute = FALSE, BOOL bRed = FALSE, BOOL bReBlip = TRUE, BOOL bPrintObjective = FALSE)	

	VECTOR vTemp
	INT idx
	
	IF NOT bWanted
		IF IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
		
			PRINT_NOW("DTRSHRD_01", DEFAULT_GOD_TEXT_TIME,1)
			
			IF DOES_BLIP_EXIST(myLocationBlip[0])
				SET_BLIP_ROUTE(myLocationBlip[0], FALSE)
			ENDIF
			
			REPEAT COUNT_OF(myLocationBlip) idx
				IF DOES_BLIP_EXIST(myLocationBlip[idx])
					REMOVE_BLIP(myLocationBlip[idx])
					DEBUG_MESSAGE("Removing blip because I went wanted.")
				ENDIF
			ENDREPEAT
			
			IF DOES_BLIP_EXIST(myVehicleBlip)
				REMOVE_BLIP(myVehicleBlip)
				DEBUG_PRINT("REMOVING myVehicleBlip VIA UPDATE_LAW")
			ENDIF

			bWanted = TRUE
		ENDIF
	ELSE
		IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0) AND bWanted
	
			CLEAR_PRINTS()
			
			bWanted = FALSE
			
			vTemp = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			fCurDist = VDIST(vCurDestObjective[0], vTemp)
			
			//bOutOfVehicle = NOT IS_PLAYER_IN_VALID_VEHICLE(myVehicle)
			
//			IF NOT bOutOfVehicle
//				PRINT_NOW(sLastObjective,DEFAULT_GOD_TEXT_TIME,1)
//			ENDIF

			IF bPrintObjective
				PRINT_NOW(sLastObjective,DEFAULT_GOD_TEXT_TIME,1)
			ENDIF

			UNUSED_PARAMETER(sLastObjective)
			
			IF NOT bOutOfVehicle
				REPEAT COUNT_OF(myLocationBlip) idx
					IF NOT ARE_VECTORS_EQUAL(vCurDestObjective[idx], <<0,0,0>>)
						bVectorsExist = TRUE
						myLocationBlip[idx] = ADD_BLIP_FOR_COORD(vCurDestObjective[idx])
						PRINTLN("ADDING BACK IN myLocationBlip VIA UPDATE LAW")
						IF bRed
							SET_BLIP_COLOUR(myLocationBlip[idx], BLIP_COLOUR_RED)
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF NOT bVectorsExist and bReBlip
					REPEAT COUNT_OF(myEntities) idx 
						IF DOES_ENTITY_EXIST(myEntities[idx]) AND NOT IS_ENTITY_DEAD(myEntities[idx])
							myLocationBlip[idx] = ADD_BLIP_FOR_ENTITY(myEntities[idx])
							DEBUG_PRINT("ADDING IN BLIPS FOR ENTITIES")
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
			
			IF bOutOfVehicle
				IF NOT DOES_BLIP_EXIST(myVehicleBlip)
					myVehicleBlip = ADD_BLIP_FOR_ENTITY(myVehicle)
					SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
					DEBUG_MESSAGE("Get back in print 2")
					DEBUG_PRINT("ADDING IN BLIP FOR myVehicle VIA UPDATE_LAW")
					PRINT_NOW("DTRSHRD_03", DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
			ENDIF
			
			outOfVehicleTime = GET_GAME_TIMER()
			DEBUG_PRINT("RESETING outOfVehicleTime VIA UPDATE_LAW")
			
			bGrabbedDistance = FALSE
			PRINTLN("SETTING bGrabbedDistance = FALSE - VIA UPDATE LAW")

			IF bSetRoute
				IF DOES_BLIP_EXIST(myLocationBlip[0])
//					SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_CUTSCENE(VECTOR camVec, VECTOR camOrien, CAMERA_INDEX& myCamera, BOOL skipCutscene = FALSE)
	IF skipCutscene 
		RETURN TRUE
	ENDIF
	
	DEBUG_PRINT("INSIDE DO_CUTSCENE")
	
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	myCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, camVec, camOrien, 65, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, TRUE)
	SET_CAM_ACTIVE(myCamera, TRUE)
	DISPLAY_RADAR(FALSE)
	DISPLAY_HUD(FALSE)
	CLEAR_PRINTS()
	WAIT(6000)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DESTROY_CAM(myCamera)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	RETURN TRUE
ENDFUNC

PROC SETUP_PED_FOR_COMBAT(PED_INDEX myPed, REL_GROUP_HASH myHash, BOOL bKeepTask = TRUE)
	SET_PED_COMBAT_ABILITY(myPed, CAL_AVERAGE)
	SET_PED_COMBAT_RANGE(myPed, CR_FAR)
	SET_PED_COMBAT_ATTRIBUTES(myPed, CA_DO_DRIVEBYS, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(myPed, CA_CAN_CHARGE, TRUE)
	SET_PED_COMBAT_MOVEMENT(myPed, CM_WILLADVANCE)
	GIVE_WEAPON_TO_PED(myPed, WEAPONTYPE_MICROSMG, -1, TRUE)
	SET_PED_ACCURACY(myPed, 20)
	
	// Adding to address Bug #73392 - trying to keep the ambushers from losing their tasks and from shooting at one another... when leaving and coming back.
	SET_PED_KEEP_TASK(myPed, bKeepTask)
	SET_PED_RELATIONSHIP_GROUP_HASH(myPed, myHash)
	SET_PED_SEEING_RANGE(myPed, 1000.0)
ENDPROC

PROC SETUP_PED_FOR_CHASE(PED_INDEX myPed, REL_GROUP_HASH myHash, BOOL bAffectWantedLevel = TRUE)
	
	SET_PED_COMBAT_ABILITY(myPed, CAL_PROFESSIONAL)
	SET_PED_COMBAT_RANGE(myPed, CR_FAR)
	SET_PED_COMBAT_MOVEMENT(myPed, CM_DEFENSIVE)
	GIVE_WEAPON_TO_PED(myPed, WEAPONTYPE_SMG, -1, TRUE)
	
	SET_PED_COMBAT_ATTRIBUTES(myPed, CA_LEAVE_VEHICLES, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(myPed, CA_DO_DRIVEBYS, TRUE)																		
	SET_PED_CONFIG_FLAG(myPed, PCF_DontInfluenceWantedLevel, bAffectWantedLevel)
	SET_PED_RELATIONSHIP_GROUP_HASH(myPed, myHash)
	
	// Task everyone with combat
	TASK_COMBAT_PED(myPed, PLAYER_PED_ID())								
	DEBUG_PRINT("TASKING GUYS TO COMBAT")
//	SET_PED_ACCURACY(myPed, 15)
ENDPROC

PROC SETUP_PED_FOR_FLEEING(PED_INDEX myPed)
	SET_PED_COMBAT_ABILITY(myPed, CAL_POOR)
	//SET_PED_COMBAT_RANGE(myPed, CR_NEAR)
	//SET_PED_COMBAT_ATTRIBUTES(myPed, CA_, TRUE)
	
	SET_PED_COMBAT_MOVEMENT(myPed, CM_WILLRETREAT)
	//SET_CURRENT_PED_WEAPON(badGuyPed[idx], WEAPONTYPE_ASSAULTRIFLE, TRUE)
	//SET_PED_WEAPON_SKILL(badGuyPed[idx], WEAPONSKILL_STD)
	//GIVE_WEAPON_TO_PED(myPed, WEAPONTYPE_PISTOL, 25, TRUE)
ENDPROC

//FUNC BOOL CREATE_STATIC_CHASE(LOCATION_DATA& myLocData, STATIC_CHASE_ARGS& staticChaseArgs, CREATE_STATIC_CHASE_STATE & createStaticChaseStages)
//	INT idx//, idx2, iStartIndex
//	
//	SWITCH createStaticChaseStages
//		CASE CREATE_STATIC_CHASE_STATE_01
//			
//			bStreamedPolice = TRUE
//			
//			REQUEST_MODEL(myLocData.mnPoliceCar)
//			REQUEST_MODEL(myLocData.mnPolicePed)
//			DEBUG_PRINT("REQUESTING POLICE CAR AND PEDS")
//			
//			IF NOT HAS_MODEL_LOADED(myLocData.mnPoliceCar)
//			OR NOT HAS_MODEL_LOADED(myLocData.mnPolicePed)
//				DEBUG_PRINT("MODELS HAVEN'T LOADED IN YET")
//				bStreamedPolice = FALSE	
//			ENDIF
//			
//			IF bStreamedPolice
//				DEBUG_PRINT("INSIDE STATE - CREATE_STATIC_CHASE_STATE_01")
//				createStaticChaseStages = CREATE_STATIC_CHASE_STATE_02
//			ENDIF
//		BREAK
//		//============================================================================================================================
//		CASE CREATE_STATIC_CHASE_STATE_02
//			// Create vehicles
//			REPEAT myLocData.iNumCarChaserCars idx
//				staticChaseArgs.vehStaticAmbush[idx] = CREATE_VEHICLE(myLocData.mnPoliceCar, myLocData.dropData[0].vStaticPos[idx], myLocData.dropData[0].fStaticRot[idx])
//				SET_VEHICLE_ON_GROUND_PROPERLY(staticChaseArgs.vehStaticAmbush[idx])
//				SET_VEHICLE_ENGINE_ON(staticChaseArgs.vehStaticAmbush[idx], TRUE, TRUE)
//				SET_ENTITY_PROOFS(staticChaseArgs.vehStaticAmbush[idx], FALSE, FALSE, FALSE, FALSE, FALSE)
//				SET_VEHICLE_CAN_BE_TARGETTED(staticChaseArgs.vehStaticAmbush[idx], TRUE)
//				SET_VEHICLE_PROVIDES_COVER(staticChaseArgs.vehStaticAmbush[idx], TRUE)
//				SET_VEHICLE_DOOR_OPEN(staticChaseArgs.vehStaticAmbush[idx], SC_DOOR_FRONT_LEFT)
//				SET_VEHICLE_DOOR_OPEN(staticChaseArgs.vehStaticAmbush[idx], SC_DOOR_FRONT_RIGHT)
//				DEBUG_PRINT("CREATING STATIC CHASE VEHICLE")
//			ENDREPEAT
//
//			staticChaseArgs.pedStaticAmbushOutside[0] = CREATE_PED(PEDTYPE_CRIMINAL,  myLocData.mnPolicePed, myLocData.dropData[0].vStaticPedPos[0], 204.0455)
//			staticChaseArgs.pedStaticAmbushOutside[1] = CREATE_PED(PEDTYPE_CRIMINAL, myLocData.mnPolicePed, myLocData.dropData[0].vStaticPedPos[1], 204.0455)
//			SETUP_PED_FOR_COMBAT(staticChaseArgs.pedStaticAmbushOutside[0], staticChaseArgs.relStaticPoliceChasers, FALSE)
//			SETUP_PED_FOR_COMBAT(staticChaseArgs.pedStaticAmbushOutside[1], staticChaseArgs.relStaticPoliceChasers, FALSE)
//			
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	RETURN FALSE
//ENDFUNC
//
//FUNC BOOL UPDATE_STATIC_CHASE(LOCATION_DATA& myLocData, STATIC_CHASE_ARGS& staticChaseArgs, STATIC_CHASE_STATE& staticChaseStage, BOOL& bDoPoliceChase)
//	
//	INT idx, idx2, iStartIndex
//	
//	
//	SWITCH staticChaseStage
//		CASE STATIC_CHASE_STATE_01
//			
//			// Start timer once update is running
//			IF NOT staticChaseArgs.bTimerStarted
//				staticChaseArgs.bTimerStarted = TRUE
//				SETTIMERA(0)
//			ENDIF
//			// If the player has not triggered the trap and gone wanted after 30 seconds, kick off dynamic police chase.
//			IF TIMERA() > TRAP_NOT_TRIGGERED_TIME
//			AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
////				DEBUG_PRINT("PLAYER HAS NOT TRIGGERED TRAP IN SET TIME, SETTING bDoPoliceChase TO TRUE")
//				bDoPoliceChase = TRUE
////				RETURN TRUE
//			ENDIF
//		
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				REPEAT myLocData.iNumCarChaserCars idx
//					IF DOES_ENTITY_EXIST(staticChaseArgs.vehStaticAmbush[idx]) AND NOT IS_ENTITY_DEAD(staticChaseArgs.vehStaticAmbush[idx])
//						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vStaticPos[idx], <<40,40,40>>) 
////						OR NOT IS_ENTITY_OCCLUDED(staticChaseArgs.vehStaticAmbush[idx])
//							
////							REPEAT myLocData.iNumPedsPerCar idx2
////							iStartIndex = idx*myLocData.iNumPedsPerCar
////								IF NOT IS_PED_INJURED(staticChaseArgs.pedStaticAmbush[idx2 + iStartIndex])
////									TASK_COMBAT_PED(staticChaseArgs.pedStaticAmbush[idx2 + iStartIndex], PLAYER_PED_ID())
////								ENDIF
////							ENDREPEAT
//							
//							IF NOT IS_PED_INJURED(staticChaseArgs.pedStaticAmbushOutside[0])
//								TASK_COMBAT_PED(staticChaseArgs.pedStaticAmbushOutside[0], PLAYER_PED_ID())
//							ENDIF
//							IF NOT IS_PED_INJURED(staticChaseArgs.pedStaticAmbushOutside[1])
//								TASK_COMBAT_PED(staticChaseArgs.pedStaticAmbushOutside[1], PLAYER_PED_ID())
//							ENDIF
//							
//							SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(), 2)
//							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//							DEBUG_PRINT("INSIDE STATE- STATIC_CHASE_STATE_01")
//							staticChaseStage = STATIC_CHASE_STATE_02
//						ENDIF
//					ENDIF
//				ENDREPEAT
//			ENDIF
//		BREAK
//		//==================================================================================================================================
//		CASE STATIC_CHASE_STATE_02
//			// AI not needed?
//			staticChaseStage = STATIC_CHASE_STATE_03
//		BREAK
//		//==================================================================================================================================
//		CASE STATIC_CHASE_STATE_03
//			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
//				REPEAT myLocData.iNumCarChaserCars idx
//					IF DOES_ENTITY_EXIST(staticChaseArgs.vehStaticAmbush[idx]) AND IS_VEHICLE_DRIVEABLE(staticChaseArgs.vehStaticAmbush[idx])
//						REPEAT myLocData.iNumPedsPerCar idx2
//							iStartIndex = idx*myLocData.iNumPedsPerCar
//							IF NOT IS_PED_INJURED(staticChaseArgs.pedStaticAmbush[idx2 + iStartIndex])
//								CLEAR_PED_TASKS(staticChaseArgs.pedStaticAmbush[idx2 + iStartIndex])
//								TASK_VEHICLE_DRIVE_WANDER(staticChaseArgs.pedStaticAmbush[idx2 + iStartIndex], staticChaseArgs.vehStaticAmbush[idx], 20.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
//								DEBUG_PRINT("TASKING AMBUSH VEHICLES TO WANDER VIA UPDATE_STATIC_CHASE")
//							ENDIF
//						ENDREPEAT
//					ENDIF
//				ENDREPEAT
//				
//				RETURN TRUE
//				
//				DEBUG_PRINT("RETURNING TRUE ON UPDATE_STATIC_CHASE")
//			ENDIF
//		BREAK
//	ENDSWITCH
//	
//	RETURN FALSE
//
//ENDFUNC

PROC SETUP_AMBUSH_PED_VARIATIONS(PED_INDEX& badGuyPed[], INT iVariation)
	
	PRINTLN("iVariation = ", iVariation)

	SWITCH iVariation
		CASE 0
			IF DOES_ENTITY_EXIST(badGuyPed[0]) AND NOT IS_PED_INJURED(badGuyPed[0])
				SET_PED_COMPONENT_VARIATION(badGuyPed[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(badGuyPed[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(badGuyPed[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			ENDIF
		BREAK
		CASE 1
			IF DOES_ENTITY_EXIST(badGuyPed[1]) AND NOT IS_PED_INJURED(badGuyPed[1])
				SET_PED_COMPONENT_VARIATION(badGuyPed[1], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(badGuyPed[1], INT_TO_ENUM(PED_COMPONENT,3), 1, 3, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(badGuyPed[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			ENDIF
		BREAK
		CASE 2
			IF DOES_ENTITY_EXIST(badGuyPed[2]) AND NOT IS_PED_INJURED(badGuyPed[2])
				SET_PED_COMPONENT_VARIATION(badGuyPed[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(badGuyPed[2], INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(badGuyPed[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			ENDIF
		BREAK
		CASE 3
			IF DOES_ENTITY_EXIST(badGuyPed[3]) AND NOT IS_PED_INJURED(badGuyPed[3])
				SET_PED_COMPONENT_VARIATION(badGuyPed[3], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(badGuyPed[3], INT_TO_ENUM(PED_COMPONENT,3), 0, 3, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(badGuyPed[3], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			ENDIF
		BREAK
		CASE 4
			IF DOES_ENTITY_EXIST(badGuyPed[4]) AND NOT IS_PED_INJURED(badGuyPed[4])
				SET_PED_COMPONENT_VARIATION(badGuyPed[4], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(badGuyPed[4], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(badGuyPed[4], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			ENDIF
		BREAK
		CASE 5
			IF DOES_ENTITY_EXIST(badGuyPed[5]) AND NOT IS_PED_INJURED(badGuyPed[5])
				SET_PED_COMPONENT_VARIATION(badGuyPed[5], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(badGuyPed[5], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(badGuyPed[5], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			ENDIF
		BREAK
		CASE 6
			IF DOES_ENTITY_EXIST(badGuyPed[6]) AND NOT IS_PED_INJURED(badGuyPed[6])
				SET_PED_COMPONENT_VARIATION(badGuyPed[6], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(badGuyPed[6], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(badGuyPed[6], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			ENDIF
		BREAK
		CASE 7
			IF DOES_ENTITY_EXIST(badGuyPed[7]) AND NOT IS_PED_INJURED(badGuyPed[7])
				SET_PED_COMPONENT_VARIATION(badGuyPed[7], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(badGuyPed[7], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(badGuyPed[7], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			ENDIF
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("INVALID VALUE PASSED INTO - SETUP_AMBUSH_PED_VARIATIONS")
		BREAK
	ENDSWITCH
	
ENDPROC

PROC CREATE_AMBUSH(LOCATION_DATA& myLocData, PED_INDEX& badGuyPed[], CAR_AMBUSH_ARGS& ambushArgs)

	// Create ambush relationship group
	ADD_RELATIONSHIP_GROUP("ambushGroup", ambushArgs.relAmbushEnemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, ambushArgs.relAmbushEnemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, ambushArgs.relAmbushEnemies, RELGROUPHASH_PLAYER)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, ambushArgs.relAmbushEnemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, ambushArgs.relAmbushEnemies, RELGROUPHASH_COP)
	
	ADD_NAVMESH_REQUIRED_REGION(2346.9280, 3095.9612, 250)

	INT idx, idx2, startIndex

	//number of cars must be less than number of peds
	REPEAT MAX_NUM_RECORDS idx
		IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
			ambushArgs.ambushVehicles[idx] = CREATE_VEHICLE(myLocData.dropData[0].carRecData[idx].carrecVehicle,  myLocData.dropData[0].carRecData[idx].carCoords,  0.0)
			SET_ENTITY_COORDS(ambushArgs.ambushVehicles[idx], myLocData.dropData[0].carRecData[idx].carCoords)
			SET_ENTITY_QUATERNION(ambushArgs.ambushVehicles[idx], myLocData.dropData[0].carRecData[idx].x, myLocData.dropData[0].carRecData[idx].y, myLocData.dropData[0].carRecData[idx].j, myLocData.dropData[0].carRecData[idx].k)
			SET_ENTITY_LOAD_COLLISION_FLAG(ambushArgs.ambushVehicles[idx], TRUE)
			SET_ENTITY_HEALTH(ambushArgs.ambushVehicles[idx], 2000)
			MODIFY_VEHICLE_TOP_SPEED(ambushArgs.ambushVehicles[idx], 100.0)
			ROLL_DOWN_WINDOWS(ambushArgs.ambushVehicles[idx])
			
			REPEAT myLocData.iNumPedsPerCar idx2
				startIndex = idx * myLocData.iNumBadGuysPerCar
				
				badGuyPed[idx2 + startIndex] = CREATE_PED_INSIDE_VEHICLE(ambushArgs.ambushVehicles[idx], PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy, myLocData.PedSeat[idx2])
				SETUP_PED_FOR_COMBAT(badGuyPed[idx2 + startIndex], ambushArgs.relAmbushEnemies)
//				SETUP_AMBUSH_PED_VARIATIONS(badGuyPed, idx2 + startIndex)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(badGuyPed[idx2 + startIndex], TRUE)
				SET_PED_COMBAT_ATTRIBUTES(badGuyPed[idx2 + startIndex], CA_REQUIRES_LOS_TO_SHOOT, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(badGuyPed[idx2 + startIndex], ambushArgs.relAmbushEnemies)
				SET_PED_KEEP_TASK(badGuyPed[idx2 + startIndex], TRUE)
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
	DEBUG_PRINT("SETTING MAX WANTERD LEVEL TO ZERO VIA CREATE AMBUSH")
ENDPROC

PROC AMBUSHERS_GET_OUT_CAR_AND_ATTACK(LOCATION_DATA& myLocData, CAR_AMBUSH_ARGS& ambushArgs, PED_INDEX& badGuyPed[], BOOL bLeaveVehicle = TRUE)
	INT idx, idx2, iStartIndex
	SEQUENCE_INDEX taskSequence
	VEHICLE_SEAT pedVehicleSeat

	REPEAT MAX_NUM_RECORDS idx
		IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
			IF IS_VEHICLE_DRIVEABLE(ambushArgs.ambushVehicles[idx])
				
				// If we're told to leave the vehicle, remove blips on the vehicles
				IF bLeaveVehicle
//					IF DOES_BLIP_EXIST(badGuyVehicleBlips[idx])
//						REMOVE_BLIP(badGuyVehicleBlips[idx])
//						PRINTLN("REMOVING VEHICLE BLIP VIA AMBUSHERS_GET_OUT_CAR_AND_ATTACK - IDX: ", idx)
//					ENDIF
				ENDIF
			
				REPEAT myLocData.iNumPedsPerCar idx2
					iStartIndex = idx * myLocData.iNumBadGuysPerCar
					
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(ambushArgs.ambushVehicles[idx])
						OPEN_SEQUENCE_TASK(taskSequence)
							IF bLeaveVehicle
								TASK_LEAVE_VEHICLE(NULL, ambushArgs.ambushVehicles[idx], ECF_DONT_CLOSE_DOOR)
								
//								// Add blips for individual guys
//								IF NOT DOES_BLIP_EXIST(badGuyBlips[idx2 + iStartIndex])
//									IF DOES_ENTITY_EXIST(badGuyPed[idx2 + iStartIndex]) AND NOT IS_ENTITY_DEAD(badGuyPed[idx2 + iStartIndex])
//										badGuyBlips[idx2 + iStartIndex] = ADD_BLIP_FOR_ENTITY(badGuyPed[idx2 + iStartIndex])
//										SET_BLIP_SCALE(badGuyBlips[idx2 + iStartIndex], 0.5)
//										SET_BLIP_COLOUR(badGuyBlips[idx2 + iStartIndex], BLIP_COLOUR_RED)
//										PRINTLN("ADD BLIP TO GUYS: ", idx2 + iStartIndex)
//									ENDIF
//								ENDIF
								
							ELSE
								IF (idx2 + iStartIndex = 0) OR (idx2 + iStartIndex = 2)
								OR (idx2 + iStartIndex = 4) OR (idx2 + iStartIndex = 6)
									pedVehicleSeat = VS_DRIVER
									IF IS_VEHICLE_SEAT_FREE(ambushArgs.ambushVehicles[idx])
										PRINTLN("pedVehicleSeat = VS_DRIVER FOR INDEX = ", idx2 + iStartIndex)
										TASK_ENTER_VEHICLE(NULL, ambushArgs.ambushVehicles[idx], DEFAULT_TIME_BEFORE_WARP, pedVehicleSeat)
									
//										// Remove on guys
//										IF DOES_BLIP_EXIST(badGuyBlips[idx2 + iStartIndex])
//											REMOVE_BLIP(badGuyBlips[idx2 + iStartIndex])
//											PRINTLN("REMOVING BLIPS ON GUYS 01: ", idx2 + iStartIndex)
//										ENDIF
//										// Add on cars
//										IF NOT DOES_BLIP_EXIST(badGuyVehicleBlips[idx])
//											IF DOES_ENTITY_EXIST(ambushArgs.ambushVehicles[idx]) AND NOT IS_ENTITY_DEAD(ambushArgs.ambushVehicles[idx])
//												badGuyVehicleBlips[idx] = ADD_BLIP_FOR_ENTITY(ambushArgs.ambushVehicles[idx])
//												SET_BLIP_COLOUR(badGuyVehicleBlips[idx], BLIP_COLOUR_RED)
//												PRINTLN("ADD BLIP TO VEHICLES 01: ", idx)
//											ENDIF
//										ENDIF
									ENDIF
								ELSE
									pedVehicleSeat = VS_FRONT_RIGHT
									PRINTLN("pedVehicleSeat = VS_FRONT_RIGHT FOR INDEX = ", idx2 + iStartIndex)
									TASK_ENTER_VEHICLE(NULL, ambushArgs.ambushVehicles[idx], DEFAULT_TIME_BEFORE_WARP, pedVehicleSeat)
									
									// Remove on guys
//									IF DOES_BLIP_EXIST(badGuyBlips[idx2 + iStartIndex])
//										REMOVE_BLIP(badGuyBlips[idx2 + iStartIndex])
//										PRINTLN("REMOVING BLIPS ON GUYS 01: ", idx2 + iStartIndex)
//									ENDIF
									// Add on cars
//									IF NOT DOES_BLIP_EXIST(badGuyVehicleBlips[idx])
//										badGuyVehicleBlips[idx] = ADD_BLIP_FOR_ENTITY(ambushArgs.ambushVehicles[idx])
//										SET_BLIP_COLOUR(badGuyVehicleBlips[idx], BLIP_COLOUR_RED)
//										PRINTLN("ADD BLIP TO VEHICLES 01: ", idx)
//									ENDIF
								ENDIF
							ENDIF
							TASK_COMBAT_PED(NULL, GET_PLAYER_PED(GET_PLAYER_INDEX()))
						CLOSE_SEQUENCE_TASK(taskSequence)
						IF NOT IS_PED_INJURED(badGuyPed[idx2 + iStartIndex])
							TASK_PERFORM_SEQUENCE(badGuyPed[idx2 + iStartIndex], taskSequence)
							PRINTLN("TASKING TO LEAVE VEHICLE, GUY = ", idx2 + iStartIndex)
						ENDIF
						CLEAR_SEQUENCE_TASK(taskSequence)
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC AMBUSHERS_GET_IN_CAR_AND_ATTACK(LOCATION_DATA& myLocData, CAR_AMBUSH_ARGS& ambushArgs, PED_INDEX& badGuyPed[], VEHICLE_INDEX& myVehicle)
	INT idx, idx2, iStartIndex
	SEQUENCE_INDEX taskSequence
	VEHICLE_SEAT pedVehicleSeat
	VEHICLE_MISSION vehicleMissionToUse

	REPEAT MAX_NUM_RECORDS idx
		IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)				
			REPEAT myLocData.iNumPedsPerCar idx2
				iStartIndex = idx * myLocData.iNumBadGuysPerCar
				
				IF (idx2 + iStartIndex = 0) OR (idx2 + iStartIndex = 2)
				OR (idx2 + iStartIndex = 4) OR (idx2 + iStartIndex = 6)
					pedVehicleSeat = VS_DRIVER
					PRINTLN("pedVehicleSeat = VS_DRIVER FOR INDEX = ", idx2 + iStartIndex)
					
					IF idx2 + iStartIndex = 0
						vehicleMissionToUse = MISSION_ESCORT_LEFT
					ELIF idx2 + iStartIndex = 2
						vehicleMissionToUse = MISSION_ESCORT_RIGHT
					ELIF idx2 + iStartIndex = 4
						vehicleMissionToUse = MISSION_ESCORT_REAR
					ELSE
						vehicleMissionToUse = MISSION_ESCORT_REAR
					ENDIF
						
					IF NOT IS_PED_INJURED(badGuyPed[idx2 + iStartIndex])
						SET_PED_COMBAT_ATTRIBUTES(badGuyPed[idx2 + iStartIndex], CA_USE_VEHICLE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(badGuyPed[idx2 + iStartIndex], CA_JUST_FOLLOW_VEHICLE, TRUE)
						DEBUG_MESSAGE("SETTING COMBAT ATTRIBUTES")
					ENDIF
					
					// Remove on guys
//					IF DOES_BLIP_EXIST(badGuyBlips[idx2 + iStartIndex])
//						REMOVE_BLIP(badGuyBlips[idx2 + iStartIndex])
//						PRINTLN("REMOVING BLIPS ON GUYS 01: ", idx2 + iStartIndex)
//					ENDIF
					// Add on cars
//					IF NOT IS_PED_INJURED(badGuyPed[idx2 + iStartIndex])
//						IF NOT DOES_BLIP_EXIST(badGuyVehicleBlips[idx])
//							badGuyVehicleBlips[idx] = ADD_BLIP_FOR_ENTITY(ambushArgs.ambushVehicles[idx])
//							SET_BLIP_COLOUR(badGuyVehicleBlips[idx], BLIP_COLOUR_RED)
//							PRINTLN("ADD BLIP TO VEHICLES 01: ", idx)
//						ENDIF
//					ENDIF
					
					IF NOT IS_PED_INJURED(badGuyPed[idx2 + iStartIndex]) AND IS_VEHICLE_DRIVEABLE(ambushArgs.ambushVehicles[idx])
						OPEN_SEQUENCE_TASK(taskSequence)
							IF IS_VEHICLE_SEAT_FREE(ambushArgs.ambushVehicles[idx])
								TASK_ENTER_VEHICLE(NULL, ambushArgs.ambushVehicles[idx], DEFAULT_TIME_BEFORE_WARP, pedVehicleSeat)
								DEBUG_MESSAGE("TASK_ENTER_VEHICLE")
							ENDIF
							TASK_VEHICLE_MISSION(NULL, ambushArgs.ambushVehicles[idx], myVehicle, vehicleMissionToUse, 70, DF_PreferNavmeshRoute, 10, -1)
							ADD_VEHICLE_SUBTASK_ATTACK_PED(NULL, PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(taskSequence)
						IF NOT IS_PED_INJURED(badGuyPed[idx2 + iStartIndex])
							TASK_PERFORM_SEQUENCE(badGuyPed[idx2 + iStartIndex], taskSequence)
							PRINTLN("TASKING VEHICLE MISSION, GUY = ", idx2 + iStartIndex)
						ENDIF
						CLEAR_SEQUENCE_TASK(taskSequence)
					ENDIF
				ELSE
					IF IS_PED_INJURED(badGuyPed[(idx2 + iStartIndex) - 1])
						pedVehicleSeat = VS_DRIVER
						PRINTLN("SPECIAL CASE: pedVehicleSeat = VS_DRIVER FOR INDEX = ", idx2 + iStartIndex)
					ELSE
						pedVehicleSeat = VS_FRONT_RIGHT
						PRINTLN("pedVehicleSeat = VS_FRONT_RIGHT FOR INDEX = ", idx2 + iStartIndex)
					ENDIF
					
//					// Remove on guys
//					IF DOES_BLIP_EXIST(badGuyBlips[idx2 + iStartIndex])
//						REMOVE_BLIP(badGuyBlips[idx2 + iStartIndex])
//						PRINTLN("REMOVING BLIPS ON GUYS 01: ", idx2 + iStartIndex)
//					ENDIF
					// Add on cars
//					IF NOT IS_PED_INJURED(badGuyPed[idx2 + iStartIndex])
//						IF NOT DOES_BLIP_EXIST(badGuyVehicleBlips[idx])
//							badGuyVehicleBlips[idx] = ADD_BLIP_FOR_ENTITY(ambushArgs.ambushVehicles[idx])
//							SET_BLIP_COLOUR(badGuyVehicleBlips[idx], BLIP_COLOUR_RED)
//							PRINTLN("ADD BLIP TO VEHICLES 01: ", idx)
//						ENDIF
//					ENDIF
					
					IF NOT IS_PED_INJURED(badGuyPed[idx2 + iStartIndex]) AND IS_VEHICLE_DRIVEABLE(ambushArgs.ambushVehicles[idx])
						OPEN_SEQUENCE_TASK(taskSequence)
							TASK_ENTER_VEHICLE(NULL, ambushArgs.ambushVehicles[idx], DEFAULT_TIME_BEFORE_WARP, pedVehicleSeat)
							TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(taskSequence)
						IF NOT IS_PED_INJURED(badGuyPed[idx2 + iStartIndex])
							TASK_PERFORM_SEQUENCE(badGuyPed[idx2 + iStartIndex], taskSequence)
							PRINTLN("TASKING TO COMBAT PLAYER, GUY = ", idx2 + iStartIndex)
						ENDIF
						CLEAR_SEQUENCE_TASK(taskSequence)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
ENDPROC

PROC AMBUSH_CHECK_FOR_IN_OUT_VEHICLE(LOCATION_DATA& myLocData, CAR_AMBUSH_ARGS& ambushArgs, VEHICLE_INDEX& myVehicle, PED_INDEX& badGuyPed[])

	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle) 
			IF NOT ambushArgs.bPlayerLeavingAreaInCar
				ambushArgs.bPlayerLeavingAreaInCar = TRUE
				ambushArgs.bReTasked = FALSE
				PRINTLN("SETTING ambushArgs.bPlayerLeavingAreaInCar = TRUE")
			ENDIF
			
			IF ambushArgs.bReTaskedSpecialCase
				PRINTLN("SPECIAL CASE: RESETING ambushArgs.bReTaskedSpecialCase TO FALSE")
				ambushArgs.bReTaskedSpecialCase = FALSE
			ENDIF
			
			IF NOT ambushArgs.bReTasked
				AMBUSHERS_GET_IN_CAR_AND_ATTACK(myLocData, ambushArgs, badGuyPed, myVehicle)
				ambushArgs.bReTasked = TRUE
			ENDIF
		ELSE
			// SPECIAL CASE - if the player gets out of their vehicle and then get into another vehicle.
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				IF NOT ambushArgs.bReTaskedSpecialCase
					AMBUSHERS_GET_OUT_CAR_AND_ATTACK(myLocData, ambushArgs, badGuyPed, FALSE)
					PRINTLN("SPECIAL CASE: TASKING TO ATTACK THE PLAYER INSIDE THEIR VEHICLE")
					ambushArgs.bReTaskedSpecialCase = TRUE
				ENDIF
			ELSE
				IF ambushArgs.bPlayerLeavingAreaInCar
					ambushArgs.bPlayerLeavingAreaInCar = FALSE
					ambushArgs.bReTasked = FALSE
					PRINTLN("SETTING ambushArgs.bPlayerLeavingAreaInCar = FALSE")
				ENDIF
				
				IF NOT ambushArgs.bReTasked
					AMBUSHERS_GET_OUT_CAR_AND_ATTACK(myLocData, ambushArgs, badGuyPed)
					ambushArgs.bReTasked = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_PED_DEATH_TO_REMOVE_VEHICLE_BLIP(LOCATION_DATA& myLocData, CAR_AMBUSH_ARGS& ambushArgs, PED_INDEX& badGuyPed[], BLIP_INDEX& badGuyVehicleBlips[])
	INT idx
	
	IF DOES_ENTITY_EXIST(badGuyPed[0]) AND DOES_ENTITY_EXIST(badGuyPed[1])
		IF IS_PED_INJURED(badGuyPed[0]) AND IS_PED_INJURED(badGuyPed[1])
			IF DOES_BLIP_EXIST(badGuyVehicleBlips[0])
				REMOVE_BLIP(badGuyVehicleBlips[0])
				PRINTLN("REMOVING VEHICLE BLIP 0 - VIA HH CHECK")
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(badGuyPed[2]) AND DOES_ENTITY_EXIST(badGuyPed[3])
		IF IS_PED_INJURED(badGuyPed[2]) AND IS_PED_INJURED(badGuyPed[3])
			IF DOES_BLIP_EXIST(badGuyVehicleBlips[1])
				REMOVE_BLIP(badGuyVehicleBlips[1])
				PRINTLN("REMOVING VEHICLE BLIP 1 - VIA HH CHECK")
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(badGuyPed[4]) AND DOES_ENTITY_EXIST(badGuyPed[5])
		IF IS_PED_INJURED(badGuyPed[4]) AND IS_PED_INJURED(badGuyPed[4])
			IF DOES_BLIP_EXIST(badGuyVehicleBlips[2])
				REMOVE_BLIP(badGuyVehicleBlips[2])
				PRINTLN("REMOVING VEHICLE BLIP 2 - VIA HH CHECK")
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(badGuyPed[6]) AND DOES_ENTITY_EXIST(badGuyPed[7])
		IF IS_PED_INJURED(badGuyPed[6]) AND IS_PED_INJURED(badGuyPed[7])
			IF DOES_BLIP_EXIST(badGuyVehicleBlips[3])
				REMOVE_BLIP(badGuyVehicleBlips[3])
				PRINTLN("REMOVING VEHICLE BLIP 3 - VIA HH CHECK")
			ENDIF
		ENDIF
	ENDIF
	
	REPEAT MAX_NUM_RECORDS idx
		IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
			IF IS_VEHICLE_DRIVEABLE(ambushArgs.ambushVehicles[idx])
				IF IS_VEHICLE_SEAT_FREE(ambushArgs.ambushVehicles[idx])
					IF DOES_BLIP_EXIST(badGuyVehicleBlips[idx])
						REMOVE_BLIP(badGuyVehicleBlips[idx])
						PRINTLN("CHECK_PED_DEATH_TO_REMOVE_VEHICLE_BLIP - SEAT IS FREE: REMOVING VEHICLE BLIP INDEX = ", idx)
					ENDIF
				ELSE
					PRINTLN("VEHICLE SEAT IS NOT FREE FOR INDEX = ", idx)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(badGuyVehicleBlips[idx])
					REMOVE_BLIP(badGuyVehicleBlips[idx])
					PRINTLN("CHECK_PED_DEATH_TO_REMOVE_VEHICLE_BLIP: REMOVING VEHICLE BLIP INDEX = ", idx)
				ELSE
					PRINTLN("CHECK_PED_DEATH_TO_REMOVE_VEHICLE_BLIP: BLIP DOES NOT EXIST, INDEX = ", idx)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC HANDLE_AMBUSHER_BLIPS(LOCATION_DATA& myLocData, CAR_AMBUSH_ARGS& ambushArgs, PED_INDEX& badGuyPed[], BOOL bDrawBlip)
	INT idx, idx2, iStartIndex
	
	REPEAT MAX_NUM_RECORDS idx
		IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
			REPEAT myLocData.iNumPedsPerCar idx2
				iStartIndex = idx * myLocData.iNumBadGuysPerCar
				
				IF DOES_ENTITY_EXIST(badGuyPed[idx2 + iStartIndex])
					
					//TO FIX BUG 1404970 about peds not getting deblipped/cleaned up at a distance 
					IF NOT IS_PED_INJURED(badGuyPed[idx2 + iStartIndex])
					AND GET_PLAYER_DISTANCE_FROM_ENTITY(badGuyPed[idx2 + iStartIndex]) > 450
						SET_ENTITY_HEALTH(badGuyPed[idx2 + iStartIndex], 0)
						PRINTLN("SET_ENTITY_HEALTH to ZERO due to Player escaping them on badGuyPed = ", idx2 + iStartIndex)
					ENDIF
					
					UPDATE_AI_PED_BLIP(badGuyPed[idx2 + iStartIndex], blipAmbushers[idx2 + iStartIndex], -1, NULL, bDrawBlip)
//					PRINTLN("UPDATING BLIP ON PED: ", idx2 + iStartIndex
					IF NOT bOkayToPrintAmbusherObjective
						PRINTLN("bOkayToPrintAmbusherObjective = TRUE")
						bOkayToPrintAmbusherObjective = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
	IF ambushArgs.bCreateGuysOnFoot
		REPEAT MAX_NUMBER_AMBUSH_ATTACKERS idx
			
			//TO FIX BUG 1404970 about peds not getting deblipped/cleaned up at a distance 
			IF NOT IS_PED_INJURED(ambushArgs.pedAmbushAttackers[idx])
			AND GET_PLAYER_DISTANCE_FROM_ENTITY(ambushArgs.pedAmbushAttackers[idx]) > 450
				SET_ENTITY_HEALTH(ambushArgs.pedAmbushAttackers[idx], 0)
				PRINTLN("SET_ENTITY_HEALTH to ZERO due to Player escaping them on ambushArgs.pedAmbushAttackers = ", idx)
			ENDIF
			
			UPDATE_AI_PED_BLIP(ambushArgs.pedAmbushAttackers[idx], blipAmbushAttackers[idx], -1, NULL, bDrawBlip)
		ENDREPEAT
	ENDIF
ENDPROC

PROC HANDLE_SEAT_SWAP(LOCATION_DATA& myLocData, CAR_AMBUSH_ARGS& ambushArgs, PED_INDEX& badGuyPed[])
	INT idx, idx2, iStartIndex
	
	REPEAT MAX_NUM_RECORDS idx
		IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
			REPEAT myLocData.iNumPedsPerCar idx2
				iStartIndex = idx * myLocData.iNumBadGuysPerCar
				
				IF DOES_ENTITY_EXIST(badGuyPed[idx2 + iStartIndex]) AND NOT IS_ENTITY_DEAD(ambushArgs.ambushVehicles[idx])
					IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(ambushArgs.ambushVehicles[idx])) 
						IF NOT IS_ENTITY_DEAD(ambushArgs.ambushVehicles[idx]) AND NOT IS_ENTITY_DEAD(GET_PED_IN_VEHICLE_SEAT(ambushArgs.ambushVehicles[idx], VS_FRONT_RIGHT))
							IF GET_SCRIPT_TASK_STATUS(GET_PED_IN_VEHICLE_SEAT(ambushArgs.ambushVehicles[idx], VS_FRONT_RIGHT), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) <> PERFORMING_TASK
								TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(GET_PED_IN_VEHICLE_SEAT(ambushArgs.ambushVehicles[idx], VS_FRONT_RIGHT), ambushArgs.ambushVehicles[idx])
								PRINTLN("TASKING GUY TO SWITCH SEATS")
							ENDIF
						ELSE
////							PRINTLN("FAILED CHECK HERE")
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(GET_PED_IN_VEHICLE_SEAT(ambushArgs.ambushVehicles[idx]), SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
							TASK_COMBAT_PED(GET_PED_IN_VEHICLE_SEAT(ambushArgs.ambushVehicles[idx]), PLAYER_PED_ID())
//							PRINTLN("TASKING DRIVER TO COMBAT")
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC CREATE_AMBUSH_ATTACKERS(CAR_AMBUSH_ARGS& ambushArgs, LOCATION_DATA& myLocData)
	INT idx

	REPEAT MAX_NUMBER_AMBUSH_ATTACKERS idx
		IF NOT IS_VECTOR_ZERO(myLocData.dropData[0].vAmbushAttackerPositions[idx])
			IF NOT IS_SPHERE_VISIBLE(myLocData.dropData[0].carRecData[idx].carCoords, 10.0)
				IF NOT ambushArgs.bCreatedFootAttacker[idx]
					ambushArgs.pedAmbushAttackers[idx] = CREATE_PED(PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy, myLocData.dropData[0].vAmbushAttackerPositions[idx])
					PRINTLN("CREATING AMBUSHER ATTACK ON FOOT: ", idx)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ambushArgs.pedAmbushAttackers[idx], TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(ambushArgs.pedAmbushAttackers[idx], ambushArgs.relAmbushEnemies)
					SET_PED_COMBAT_ATTRIBUTES(ambushArgs.pedAmbushAttackers[idx], CA_USE_VEHICLE, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(ambushArgs.pedAmbushAttackers[idx], CA_CAN_CHARGE, TRUE)
					SET_PED_COMBAT_MOVEMENT(ambushArgs.pedAmbushAttackers[idx], CM_WILLADVANCE)
					SET_PED_KEEP_TASK(ambushArgs.pedAmbushAttackers[idx], TRUE)
					
					IF GET_RANDOM_INT_IN_RANGE() % 2 = 0
						GIVE_WEAPON_TO_PED(ambushArgs.pedAmbushAttackers[idx], WEAPONTYPE_ASSAULTSHOTGUN, -1, TRUE)
						PRINTLN("GIVING AMBUSHER ATTACK: ", idx, " AN RPG")
					ELSE
						GIVE_WEAPON_TO_PED(ambushArgs.pedAmbushAttackers[idx], WEAPONTYPE_ASSAULTSHOTGUN, -1, TRUE)
						PRINTLN("GIVING AMBUSHER ATTACK: ", idx, " AN ASSAULT RIFLE")
					ENDIF
					
					TASK_COMBAT_PED(ambushArgs.pedAmbushAttackers[idx], PLAYER_PED_ID())
					
					ambushArgs.bCreatedFootAttacker[idx] = TRUE
					PRINTLN("CREATED AMBUSH ATTACKER WITH INDEX: ", idx)
					
					WAIT(1000)
				ELSE
					PRINTLN("WE HAVE NOT CREATED AMBUSH ATTACKER WITH INDEX: ", idx)
				ENDIF
			ELSE
				PRINTLN("AMBUSH SPAWN AREA IS VISIBLE FOR INDEX: ", idx)
			ENDIF
		ELSE
			PRINTLN("SPAWN VECTORS ARE ZERO")
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC HANDLE_AMBUSH_ONE_LINER(CAR_AMBUSH_ARGS& ambushArgs, structPedsForConversation& MyLocalPedStruct, BOOL& bDrugsLoaded)
	IF NOT ambushArgs.bPlayerSingleLine
		IF bDrugsLoaded
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
					IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GR13C", CONV_PRIORITY_VERY_HIGH)
						DEBUG_PRINT("PLAYING CONVO - ARMS_GR13C")
						ambushArgs.bPlayerSingleLine = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_AMBUSH_VEHICLE_ATTACKERS(LOCATION_DATA& myLocData, CAR_AMBUSH_ARGS& ambushArgs,PED_INDEX& badGuyPed[])
	INT idx, idx2, iStartIndex
	
	IF NOT IS_TIMER_STARTED(ambushArgs.tAmbushCreationTimer)
		START_TIMER_NOW(ambushArgs.tAmbushCreationTimer)
		PRINTLN("STARTING TIMER - ambushArgs.tAmbushCreationTimer")
	ENDIF

	//number of cars must be less than number of peds
		REPEAT MAX_NUM_RECORDS idx
			IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
				IF NOT IS_SPHERE_VISIBLE(myLocData.dropData[0].carRecData[idx].carCoords, 10.0)
					IF NOT ambushArgs.bCreatedVehicleAttacker[idx]
						IF GET_TIMER_IN_SECONDS(ambushArgs.tAmbushCreationTimer) > 1.0
							ambushArgs.ambushVehicles[idx] = CREATE_VEHICLE(myLocData.dropData[0].carRecData[idx].carrecVehicle,  myLocData.dropData[0].carRecData[idx].carCoords,  0.0)
							SET_ENTITY_COORDS(ambushArgs.ambushVehicles[idx], myLocData.dropData[0].carRecData[idx].carCoords)
							SET_ENTITY_QUATERNION(ambushArgs.ambushVehicles[idx], myLocData.dropData[0].carRecData[idx].x, myLocData.dropData[0].carRecData[idx].y, myLocData.dropData[0].carRecData[idx].j, myLocData.dropData[0].carRecData[idx].k)
							SET_ENTITY_LOAD_COLLISION_FLAG(ambushArgs.ambushVehicles[idx], TRUE)
							SET_ENTITY_HEALTH(ambushArgs.ambushVehicles[idx], 2000)
							MODIFY_VEHICLE_TOP_SPEED(ambushArgs.ambushVehicles[idx], 30.0)
							PRINTLN("MADE AMBUSH CAR: ", idx)
							
							REPEAT myLocData.iNumPedsPerCar idx2
								iStartIndex = idx * myLocData.iNumBadGuysPerCar
								
								badGuyPed[idx2 + iStartIndex] = CREATE_PED_INSIDE_VEHICLE(ambushArgs.ambushVehicles[idx], PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy, myLocData.PedSeat[idx2])
								SET_MODEL_AS_NO_LONGER_NEEDED(myLocData.mnGenericBadGuy)
								SETUP_PED_FOR_COMBAT(badGuyPed[idx2 + iStartIndex], ambushArgs.relAmbushEnemies)
				//				SETUP_AMBUSH_PED_VARIATIONS(badGuyPed, idx2 + startIndex)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(badGuyPed[idx2 + iStartIndex], TRUE)
								
								// New flags to address Bug # 1404711
								SET_PED_COMBAT_ATTRIBUTES(badGuyPed[idx2 + iStartIndex], CA_PREFER_NAVMESH_DURING_VEHICLE_CHASE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(badGuyPed[idx2 + iStartIndex], CA_ALLOWED_TO_AVOID_OFFROAD_DURING_VEHICLE_CHASE, TRUE)
								
								SET_PED_RELATIONSHIP_GROUP_HASH(badGuyPed[idx2 + iStartIndex], ambushArgs.relAmbushEnemies)
								SET_PED_COMBAT_ATTRIBUTES(badGuyPed[idx2 + iStartIndex], CA_USE_VEHICLE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(badGuyPed[idx2 + iStartIndex], CA_LEAVE_VEHICLES, FALSE)
								SET_PED_CONFIG_FLAG(badGuyPed[idx2 + iStartIndex], PCF_AllowToBeTargetedInAVehicle, TRUE)
							ENDREPEAT
							
							RESTART_TIMER_NOW(ambushArgs.tAmbushCreationTimer)
							
							ambushArgs.bCreatedVehicleAttacker[idx] = TRUE
							PRINTLN("CREATED VEHICLE ATTACKER FOR INDEX: ", idx)
						ENDIF
					ELSE
						PRINTLN("WE HAVE NOT CREATED VEHICLE ATTACKER FOR INDEX: ", idx)
					ENDIF
				ELSE
					PRINTLN("NOT MAKING AMBUSH CAR: ", idx)
				ENDIF
			ENDIF
		ENDREPEAT
ENDPROC

PROC HANDLE_AMBUSH_ATTACKERS_ENTERING_VEHICLES(LOCATION_DATA& myLocData, CAR_AMBUSH_ARGS& ambushArgs) 
	INT idx
	VECTOR vPlayerPosition
	FLOAT fDistanceFromObjective
	SEQUENCE_INDEX iSeq
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		fDistanceFromObjective = VDIST2(vPlayerPosition, myLocData.dropData[0].vCenterPos)
//		PRINTLN("fDistanceFromObjective = ", fDistanceFromObjective)
	ENDIF
	
	REPEAT MAX_NUMBER_AMBUSH_ATTACKERS idx
		IF DOES_ENTITY_EXIST(ambushArgs.pedAmbushAttackers[idx])
			IF NOT IS_ENTITY_DEAD(ambushArgs.pedAmbushAttackers[idx])
				IF fDistanceFromObjective > (75*75)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						SET_PED_COMBAT_ATTRIBUTES(ambushArgs.pedAmbushAttackers[idx], CA_USE_VEHICLE, TRUE)
//						PRINTLN("SETTING AMBUSH ATTACKER TO USE VECHICLES: ", idx)
						
						IF idx = 0 
							IF NOT ambushArgs.bTaskedFootAttacker[idx]
								OPEN_SEQUENCE_TASK(iSeq)
									IF DOES_ENTITY_EXIST(ambushArgs.vehAmbushAttackers[0]) AND NOT IS_ENTITY_DEAD(ambushArgs.vehAmbushAttackers[0])
										TASK_ENTER_VEHICLE(NULL, ambushArgs.vehAmbushAttackers[0], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
										PRINTLN("TASKING AMBUSH ATTACKER TO ENTER VEHICLE 0")
									ENDIF
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(iSeq)
								IF NOT IS_ENTITY_DEAD(ambushArgs.pedAmbushAttackers[idx])
									TASK_PERFORM_SEQUENCE(ambushArgs.pedAmbushAttackers[idx], iSeq)
									PRINTLN("TASKING AMBUSH ATTACKER ENTER AND ATTACK: ", idx)
								ENDIF
								CLEAR_SEQUENCE_TASK(iSeq)
								
								ambushArgs.bTaskedFootAttacker[idx] = TRUE
							ENDIF
						ELIF idx = 1
							IF NOT ambushArgs.bTaskedFootAttacker[idx]
								OPEN_SEQUENCE_TASK(iSeq)
									IF DOES_ENTITY_EXIST(ambushArgs.vehAmbushAttackers[1]) AND NOT IS_ENTITY_DEAD(ambushArgs.vehAmbushAttackers[1])
										TASK_ENTER_VEHICLE(NULL, ambushArgs.vehAmbushAttackers[1], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
									ENDIF
									PRINTLN("TASKING AMBUSH ATTACKER TO ENTER VEHICLE 1")
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(iSeq)
								IF NOT IS_ENTITY_DEAD(ambushArgs.pedAmbushAttackers[idx])
									TASK_PERFORM_SEQUENCE(ambushArgs.pedAmbushAttackers[idx], iSeq)
									PRINTLN("TASKING AMBUSH ATTACKER ENTER AND ATTACK: ", idx)
								ENDIF
								CLEAR_SEQUENCE_TASK(iSeq)
								
								ambushArgs.bTaskedFootAttacker[idx] = TRUE
							ENDIF
						ENDIF
					ELSE
//						PRINTLN("PLAYER IS NOT IN ANY CAR")
					ENDIF
				ENDIF
			ELSE
//				PRINTLN("TOO CLOSE TO CARE")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//Returns a bool letting us know if we have finished the ambush or not
FUNC BOOL UPDATE_AMBUSH(LOCATION_DATA& myLocData, AMBUSH& ambushState, PED_INDEX& badGuyPed[], BOOL &bInActiveFight, 
CAR_AMBUSH_ARGS& ambushArgs, BOOL& bDoAmbush, VECTOR& vCurDestObjective[], BOOL& bWanted, VEHICLE_INDEX& myVehicle, 
INT& outOfVehicleTime, BOOL& bAmbushActive, structPedsForConversation& MyLocalPedStruct, 
BOOL& bDrugsLoaded, BOOL& bOutOfVehicle, TEXT_LABEL_31& sLastObjective, BLIP_INDEX& myLocationBlip[])

	INT idx, idx2, iStartIndex
	BOOL bAllDead, bReplay, bExceededDistanceCheck
	SEQUENCE_INDEX taskSequence
	FLOAT fAmbushersDistanceToPlayer
	VECTOR vNodeToReturn
	FLOAT fTempSpeed
	
	//  Handle going wanted
	IF ambushState > AMBUSH_SETUP AND ambushState < AMBUSH_COMPLETE
		IF ambushArgs.bPrintAmbushObjective
			IF bWanted
				vCurDestObjective[0] = <<0,0,0>>
				REPEAT myLocData.iNumBadGuys idx
//					IF DOES_BLIP_EXIST(badGuyBlips[idx])
//						REMOVE_BLIP(badGuyBlips[idx])
//						DEBUG_PRINT("REMOVOING AMBUSHER BLIPS DUE TO GOING WANTED")
//					ENDIF
				ENDREPEAT
				REPEAT 4 idx
//					IF DOES_BLIP_EXIST(badGuyVehicleBlips[idx])
//						REMOVE_BLIP(badGuyVehicleBlips[idx])
//						DEBUG_PRINT("REMOVOING AMBUSHER VEHICLE BLIPS DUE TO GOING WANTED")
//					ENDIF
				ENDREPEAT
			ELSE
				IF bInActiveFight
					vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bOutOfVehicle
	
		REPEAT MAX_NUM_RECORDS idx
			IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
				REPEAT myLocData.iNumPedsPerCar idx2
					iStartIndex = idx * myLocData.iNumBadGuysPerCar	
					CLEANUP_AI_PED_BLIP(blipAmbushers[iStartIndex])
				ENDREPEAT
			ENDIF
		ENDREPEAT
		
		IF ambushArgs.bCreateGuysOnFoot
			REPEAT MAX_NUMBER_AMBUSH_ATTACKERS idx
				CLEANUP_AI_PED_BLIP(blipAmbushAttackers[idx])
			ENDREPEAT
		ENDIF
	ENDIF

	SWITCH ambushState
		CASE AMBUSH_INIT
			IF bUsingMexicans
				myLocData.mnGenericBadGuy = G_M_M_MEXBOSS_01
				REQUEST_MODEL(myLocData.mnGenericBadGuy)
				DEBUG_PRINT("REQUESTING MEXICAN BAD GUY - G_M_M_MEXBOSS_01")
			ELIF bUsingMarabunta
				myLocData.mnGenericBadGuy = G_M_Y_SalvaGoon_03
				REQUEST_MODEL(myLocData.mnGenericBadGuy)
				DEBUG_PRINT("REQUESTING MARABUNTA BAD GUY - G_M_Y_SalvaGoon_03")
			ELIF bUsingHillbillies
				myLocData.mnGenericBadGuy = A_M_M_HillBilly_01
				REQUEST_MODEL(myLocData.mnGenericBadGuy)
				DEBUG_PRINT("REQUESTING HILLBILLY BAD GUY - A_M_M_HillBilly_01")
			ENDIF
			
			IF HAS_MODEL_LOADED(myLocData.mnGenericBadGuy)
				PRINTLN("GOING TO STATE - AMBUSH_CREATION")
				ambushState = AMBUSH_CREATION
			ENDIF
		BREAK
		CASE AMBUSH_CREATION
			IF NOT ambushArgs.bCreateAmbush
				// Create ambush relationship group
				ADD_RELATIONSHIP_GROUP("ambushGroup", ambushArgs.relAmbushEnemies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, ambushArgs.relAmbushEnemies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, ambushArgs.relAmbushEnemies, RELGROUPHASH_PLAYER)
				
				CREATE_AMBUSH_VEHICLE_ATTACKERS(myLocData, ambushArgs, badGuyPed)

				UNUSED_PARAMETER(myVehicle)
				
				TRIGGER_MUSIC_EVENT("OJDG1_PACKAGE")
				PRINTLN("TRIGGERING MUSIC - OJDG1_PACKAGE")
				
				DEBUG_PRINT("SETTING MAX WANTERD LEVEL TO ZERO VIA CREATE AMBUSH")
				ambushArgs.bCreateAmbush = TRUE
			ENDIF
			
			PRINTLN("GOING TO STATE - AMBUSH_SETUP")
			ambushState = AMBUSH_SETUP
		BREAK
		//==========================================================================================================================
		CASE AMBUSH_SETUP
			
			CREATE_AMBUSH_VEHICLE_ATTACKERS(myLocData, ambushArgs, badGuyPed)
			
			REPEAT MAX_NUM_RECORDS idx
				IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
					IF NOT IS_SPHERE_VISIBLE(myLocData.dropData[0].carRecData[idx].carCoords, 10.0)
						IF NOT IS_ENTITY_DEAD(ambushArgs.ambushVehicles[idx])
							bReplay = TRUE
							
							START_PLAYBACK_RECORDED_VEHICLE(ambushArgs.ambushVehicles[idx], myLocData.dropData[0].carRecData[idx].carrecNum, myLocData.dropData[0].carRecData[idx].carrecName)
							SET_PLAYBACK_SPEED(ambushArgs.ambushVehicles[idx], 1.50)
						ELSE
							PRINTLN("NOT PLAYING RECORDING ON AMBUSH CAR: ", idx)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		
			bInActiveFight = TRUE
			bAmbushActive = TRUE
			vCurDestObjective[0] = <<0,0,0>>
			PRINTLN("AMBUSH: SETTING vCurDestObjective[0] = <<0,0,0>>")
			
			
			// Set law multiplier low, so the player does not go wanted when attempting to kill the ambushers.
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			DEBUG_PRINT("SETTING WANTED LEVEL MULTIPLIER TO ZERO")
			
			IF bReplay
				DEBUG_PRINT("REPLAY IS TRUE GOING TO STATE - AMBUSH_REPLAY")
				ambushState = AMBUSH_REPLAY
//			ELSE
//				ambushState = AMBUSH_FIGHT
			ENDIF
		BREAK
		//=============================================================================================================================
		CASE AMBUSH_REPLAY
			
			HANDLE_AMBUSHER_BLIPS(myLocData, ambushArgs, badGuyPed, TRUE)
			
			CREATE_AMBUSH_VEHICLE_ATTACKERS(myLocData, ambushArgs, badGuyPed)
			
			bReplay = FALSE
			REPEAT MAX_NUM_RECORDS idx
				IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
					IF IS_VEHICLE_DRIVEABLE(ambushArgs.ambushVehicles[idx])
						REPEAT myLocData.iNumPedsPerCar idx2
							iStartIndex = idx * myLocData.iNumBadGuysPerCar
							
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(ambushArgs.ambushVehicles[idx])
								OPEN_SEQUENCE_TASK(taskSequence)
									IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(GET_PLAYER_INDEX()))
										TASK_LEAVE_VEHICLE(NULL, ambushArgs.ambushVehicles[idx], ECF_DONT_CLOSE_DOOR)
									ENDIF
									TASK_COMBAT_PED(NULL, GET_PLAYER_PED(GET_PLAYER_INDEX()))
								CLOSE_SEQUENCE_TASK(taskSequence)
								IF NOT IS_PED_INJURED(badGuyPed[idx2 + iStartIndex])
									TASK_PERFORM_SEQUENCE(badGuyPed[idx2 + iStartIndex], taskSequence)
									
									HANDLE_AMBUSH_ONE_LINER(ambushArgs, MyLocalPedStruct, bDrugsLoaded)
								ENDIF
								CLEAR_SEQUENCE_TASK(taskSequence)
							ELSE
								bReplay = TRUE
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
			ENDREPEAT
				
			IF NOT bReplay
				IF NOT IS_TIMER_STARTED(ambushArgs.tAmbushTimer)
					START_TIMER_NOW(ambushArgs.tAmbushTimer)
					PRINTLN("STARTING TIMER - ambushArgs.tAmbushTimer")
				ENDIF
			
				DEBUG_PRINT("bReplay IS FALSE GOING TO STATE - AMBUSH_CREATION_BUFFER")
				ambushState = AMBUSH_CREATION_BUFFER
			ENDIF
		BREAK
		//=============================================================================================================================
		CASE AMBUSH_CREATION_BUFFER
		
			HANDLE_AMBUSHER_BLIPS(myLocData, ambushArgs, badGuyPed, TRUE)
		
			HANDLE_AMBUSH_ONE_LINER(ambushArgs, MyLocalPedStruct, bDrugsLoaded)
			
			HANDLE_SEAT_SWAP(myLocData, ambushArgs, badGuyPed)
			
			CREATE_AMBUSH_VEHICLE_ATTACKERS(myLocData, ambushArgs, badGuyPed)
		
			IF IS_TIMER_STARTED(ambushArgs.tAmbushTimer)
				IF GET_TIMER_IN_SECONDS(ambushArgs.tAmbushTimer) > 5.0
					IF ambushArgs.bCreateGuysOnFoot
						CREATE_AMBUSH_ATTACKERS(ambushArgs, myLocData)
						
						DEBUG_PRINT("bReplay IS FALSE GOING TO STATE - AMBUSH_FIGHT")
						ambushState = AMBUSH_FIGHT
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//=============================================================================================================================
		CASE AMBUSH_FIGHT
			bAllDead = TRUE
			bExceededDistanceCheck = TRUE
			
			HANDLE_AMBUSHER_BLIPS(myLocData, ambushArgs, badGuyPed, TRUE)
			
			HANDLE_AMBUSH_ONE_LINER(ambushArgs, MyLocalPedStruct, bDrugsLoaded)
			
			HANDLE_SEAT_SWAP(myLocData, ambushArgs, badGuyPed)
			
			HANDLE_AMBUSH_ATTACKERS_ENTERING_VEHICLES(myLocData, ambushArgs)
		
			// Dead check
			REPEAT myLocData.iNumBadGuys idx
				IF IS_ENTITY_DEAD(badGuyPed[idx]) //OR IS_BADGUY_OOR(badGuyPed[idx], myLocData.dropData[0].vDrugPickup)					
					// We used to set peds as no longer needed here but they were not being deleted in complete because we had not handle to them.
				ELSE
					bAllDead = FALSE
				ENDIF
			ENDREPEAT
			
			// Distance check
			REPEAT myLocData.iNumBadGuys idx
				IF NOT IS_ENTITY_DEAD(badGuyPed[idx]) AND NOT IS_PED_INJURED(badGuyPed[idx])
					fAmbushersDistanceToPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), badGuyPed[idx])
//					PRINTLN("fAmbushersDistanceToPlayer = ", fAmbushersDistanceToPlayer)
					
					IF fAmbushersDistanceToPlayer < DISTANCE_TO_LOSE_AMBUSHERS
						bExceededDistanceCheck = FALSE
					ENDIF
					IF fAmbushersDistanceToPlayer > 150
						IF IS_PED_IN_ANY_VEHICLE(badGuyPed[idx])
							
							fTempSpeed = GET_ENTITY_SPEED(badGuyPed[idx])
//							PRINTLN("fTempSpeed = ", fTempSpeed)
							
							IF fTempSpeed < 2.0
								IF NOT IS_TIMER_STARTED(tWarpTimer)
									START_TIMER_NOW(tWarpTimer)
								ENDIF
								
								IF GET_TIMER_IN_SECONDS(tWarpTimer) > 5.0
									GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(badGuyPed[idx]), vNodeToReturn, NF_INCLUDE_SWITCHED_OFF_NODES)
//									PRINTLN("vNodeToReturn = ", vNodeToReturn)
								
									IF NOT IS_VECTOR_ZERO(vNodeToReturn)
										IF IS_ENTITY_OCCLUDED(badGuyPed[idx])
											SET_PED_COORDS_KEEP_VEHICLE(badGuyPed[idx], vNodeToReturn)
											PRINTLN("SETTING BAD GUYS ON ROAD NODE: ", vNodeToReturn)
											RESTART_TIMER_NOW(tWarpTimer)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF ambushArgs.bCreateGuysOnFoot
				REPEAT MAX_NUMBER_AMBUSH_ATTACKERS idx
				  	IF IS_ENTITY_DEAD(ambushArgs.pedAmbushAttackers[idx])
						// We used to set peds as no longer needed here but they were not being deleted in complete because we had not handle to them.
					ELSE
						bAllDead = FALSE
					ENDIF
				ENDREPEAT
				REPEAT MAX_NUMBER_AMBUSH_ATTACKERS idx
					IF NOT IS_ENTITY_DEAD(ambushArgs.pedAmbushAttackers[idx])
						fAmbushersDistanceToPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), ambushArgs.pedAmbushAttackers[idx])
						IF fAmbushersDistanceToPlayer < DISTANCE_TO_LOSE_AMBUSHERS
							bExceededDistanceCheck = FALSE
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
						
			IF bAllDead OR bExceededDistanceCheck//NOT IS_ANY_ENTITY_ALIVE(badGuyPed)
				IF bAllDead
					PRINTLN("bAllDead IS TRUE")
				ENDIF
				IF bExceededDistanceCheck 
					PRINTLN("bExceededDistanceCheck IS TRUE")
				ENDIF
				
				REPEAT MAX_NUM_RECORDS idx
					IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(ambushArgs.ambushVehicles[idx])
						DEBUG_PRINT("SETTING AMBUSH VEHICLE AS NO LONGER NEEDED")
					ENDIF
				ENDREPEAT
				
				bInActiveFight = FALSE

				// Set Wanted multiplier back to it's default value.
				SET_WANTED_LEVEL_MULTIPLIER(1.0)
								
				ambushState = AMBUSH_COMPLETE
				SETTIMERA(0)
			ENDIF
		BREAK
		CASE AMBUSH_COMPLETE

			bDoAmbush = FALSE
			bAmbushActive = FALSE
			
			REPEAT MAX_NUM_RECORDS idx
				IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
					IF DOES_ENTITY_EXIST(ambushArgs.ambushVehicles[idx])
						DELETE_VEHICLE(ambushArgs.ambushVehicles[idx])
						PRINTLN("DELETING ambushArgs.ambushVehicles: ", idx)
					ENDIF
					REMOVE_VEHICLE_RECORDING(myLocData.dropData[0].carRecData[idx].carrecNum, myLocData.dropData[0].carRecData[idx].carrecName)
					PRINTLN("REMOVING VEHICLE RECORDINGS: ", idx)
				ENDIF
			ENDREPEAT
			
			REPEAT myLocData.iNumBadGuys idx
				IF DOES_ENTITY_EXIST(badGuyPed[idx])
					IF IS_ENTITY_OCCLUDED(badGuyPed[idx])
						DELETE_PED(badGuyPed[idx])
						PRINTLN("DELETING badGuyPed: ", idx)
					ELSE						
						SET_PED_AS_NO_LONGER_NEEDED(badGuyPed[idx])
						PRINTLN("SETING PED AS NO LONGER NEEDED - badGuyPed: ", idx)
					ENDIF
				ENDIF
			ENDREPEAT
			REPEAT MAX_NUMBER_AMBUSH_ATTACKERS idx
				IF DOES_ENTITY_EXIST(ambushArgs.pedAmbushAttackers[idx])
					IF IS_ENTITY_OCCLUDED(ambushArgs.pedAmbushAttackers[idx])
						DELETE_PED(ambushArgs.pedAmbushAttackers[idx])
						PRINTLN("DELETING ambushArgs.pedAmbushAttackers: ", idx)
					ELSE
						SET_PED_AS_NO_LONGER_NEEDED(ambushArgs.pedAmbushAttackers[idx])
						PRINTLN("SETTING PED AS NO LONGER NEEDED - ambushArgs.pedAmbushAttackers: ", idx)
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT bOutOfVehicle
				PRINT_NOW("DTRFKGR_03a", DEFAULT_GOD_TEXT_TIME, 1)
				sLastObjective = "DTRFKGR_03a"
				
				IF NOT DOES_BLIP_EXIST(myLocationBlip[0])
					myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.endData[0].vDrugPickup)
					SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
					PRINTLN("ADDING BACK IN LOCATION BLIP")
				ELSE
					PRINTLN("BLIP ALREADY EXIST, NOT ADDING - 02")
				ENDIF
			ENDIF
			
			vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
			
			REPEAT MAX_NUM_RECORDS idx
				IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_CAR)
					REPEAT myLocData.iNumPedsPerCar idx2
						iStartIndex = idx * myLocData.iNumBadGuysPerCar	
						CLEANUP_AI_PED_BLIP(blipAmbushers[iStartIndex])
						PRINTLN("REMOVING BLIPS VIA AMBUSH_COMPLETE - ON BAD GUYS")
					ENDREPEAT
				ENDIF
			ENDREPEAT
			
			IF ambushArgs.bCreateGuysOnFoot
				REPEAT MAX_NUMBER_AMBUSH_ATTACKERS idx
					CLEANUP_AI_PED_BLIP(blipAmbushAttackers[idx])
					PRINTLN("REMOVING BLIPS VIA AMBUSH_COMPLETE - ON PED ATTACKERS")
				ENDREPEAT
			ENDIF
		
			// To Fix Bug #371162 - need to re-grab distance after ambush is over, because we set vCurDestObjective[0]... 
			// ... above to fix problems with going wanted during ambush fight.
			bGrabbedDistance = FALSE
			
			// Re-setting to fix bug where the player fails when being out of their vehicle for too long.
			outOfVehicleTime = GET_GAME_TIMER()
			
			TRIGGER_MUSIC_EVENT("OJDG1_ENEMIES_DEAD")
			PRINTLN("TRIGGERING MUSIC - OJDG1_ENEMIES_DEAD")
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DT_IS_CAR_IN_FRONT_OF_CAR(VEHICLE_INDEX inCar1, VEHICLE_INDEX inCar2, FLOAT fDistanceInFront = 5.0)

	VECTOR posA		// car1 position
	VECTOR posB 	// car2 position
	VECTOR posC
	VECTOR vecAB 	// vector from car1 to car2
	VECTOR vecCar2	// unit vector pointing in the direction the car2 is facing
	FLOAT fTemp
	
	// get positions
	IF IS_VEHICLE_DRIVEABLE(inCar1)
		posA = GET_ENTITY_COORDS(inCar1)
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(inCar2)
		posB = GET_ENTITY_COORDS(inCar2)
	ENDIF

	// get vec from car1 to car2
	vecAB = posB - posA
	
	
	// get unit vector pointing forwards from car2
	IF IS_VEHICLE_DRIVEABLE(inCar2)
		posC = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(inCar2, <<0.0, fDistanceInFront, 0.0>>)
		vecCar2 = posC - posB
	ENDIF

	// take the z out
	vecAB.z = 0.0
	vecCar2.z = 0.0

	// calculate dot product of vecAB and vecCar2
	fTemp = DOT_PRODUCT(vecAB,vecCar2)
	IF (fTemp < 0.0)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)

ENDFUNC	 

FUNC BOOL DT_ARE_CARS_TRAVELLING_IN_SAME_DIRECTION(VEHICLE_INDEX vehPlayer, VEHICLE_INDEX vehEnemy)
	
	FLOAT tempPlayerRot, tempEnemyRot
	
	// Grab rotation values for player and enemy vehicle
	tempPlayerRot = GET_ENTITY_HEADING(vehPlayer)
	tempEnemyRot = GET_ENTITY_HEADING(vehEnemy)
	
	// 0.867 equates to ~30 degree window... 
	IF COS(ABSF(tempEnemyRot - tempPlayerRot)) <= 0.867
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF

ENDFUNC

PROC ADJUST_ENEMY_VEHICLE_SPEED(VEHICLE_INDEX& viEnemyVehicle, FLOAT fDistanceToCheck)
	
	FLOAT fDistanceBetweenPlayerAndEnemies
	VEHICLE_INDEX playerVehicle
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		playerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		// Check if the player's car is in front of the car chaser
		IF IS_VEHICLE_DRIVEABLE(viEnemyVehicle) AND IS_VEHICLE_DRIVEABLE(playerVehicle)
			IF DT_IS_CAR_IN_FRONT_OF_CAR(playerVehicle, viEnemyVehicle)
//				DEBUG_PRINT("CAR IS IN FRONT OF CAR")
				// Check if they are traveling in the same direction
				IF DT_ARE_CARS_TRAVELLING_IN_SAME_DIRECTION(playerVehicle, viEnemyVehicle)
//					DEBUG_PRINT("CARS ARE TRAVELING IN THE SAME DIRECTION")
					// Make sure they're not upside down
					IF IS_VEHICLE_ON_ALL_WHEELS(viEnemyVehicle)
						fDistanceBetweenPlayerAndEnemies = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), viEnemyVehicle)
						// Make sure they are at least moving before increasing their speed
						IF GET_ENTITY_SPEED(viEnemyVehicle) > 5
							IF fDistanceBetweenPlayerAndEnemies > fDistanceToCheck
								SET_VEHICLE_FORWARD_SPEED(viEnemyVehicle, (GET_ENTITY_SPEED(playerVehicle) + 10))
								DEBUG_PRINT("INCREASING CAR CHASER SPEED")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC TELEPORT_STUCK_AI(VEHICLE_INDEX& vehToCheck)
	
	FLOAT fVehSpeed
	VECTOR vVehStuckPos
	VECTOR vClosestVehNode
	
	IF DOES_ENTITY_EXIST(vehToCheck)
		IF IS_VEHICLE_DRIVEABLE(vehToCheck)
			// Grab vehicle's speed to check
			fVehSpeed = GET_ENTITY_SPEED(vehToCheck)
			
			// If the vehicle's speed is less than 2.0mph, start a timer to see how long they are stuck.
			IF fVehSpeed < 2.0
				IF NOT bBelowLowSpeed
					bBelowLowSpeed = TRUE
					bCanTeleport = TRUE
					START_TIMER_NOW(TeleportTimer)
					DEBUG_PRINT("TeleportTimer STARTED")
				ENDIF
			ELSE
				bBelowLowSpeed = FALSE
				bCanTeleport = FALSE
				RESTART_TIMER_NOW(TeleportTimer)
			ENDIF
		ENDIF
	ENDIF
	
	// If the vehicle to check is traveling less than 2.0mph for more than 6 seconds... consider the vehicle stuck and warp them to closest vehicle node.
	IF bCanTeleport
		IF GET_TIMER_IN_SECONDS(TeleportTimer) > STUCK_TIME
			DEBUG_PRINT("TeleportTimer > STUCK_TIME")
			IF IS_VEHICLE_DRIVEABLE(vehToCheck)
				IF NOT IS_ENTITY_ON_SCREEN(vehToCheck) OR IS_ENTITY_OCCLUDED(vehToCheck)
					vVehStuckPos = GET_ENTITY_COORDS(vehToCheck)
					GET_CLOSEST_VEHICLE_NODE(vVehStuckPos, vClosestVehNode)
					SET_ENTITY_COORDS(vehToCheck, vClosestVehNode)
					bCanTeleport = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//FUNC BOOL TRANSITION_TO_PLANE(PLANE_DATA& myData, CAMERA_INDEX& myCamera, CUTSCENE_STATE& myState, structTimer& myTimer, CustomCutsceneCaller fpCutsceneCustom)
//	PRINT_HELP("DTRSHRD_TEMP_03")
//	
//	STRING temp = "Place Holder Cutscene: Cutescene of the drugs being loaded into the plane"
//	IF DO_CUTSCENE_CUSTOM(myState, myTimer, myData.vCutscenePosStart, myData.vCutsceneOrienStart, myCamera, temp, fpCutsceneCustom)
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

//LEGACY FUNCTION
//TODO REMOVE THIS and pass down myArgs
PROC VALIDATE_VARIATIONS_AIR(PLANE_DATA& myLocData, BOOL& bDoDelicate, BOOL& bTimedPickup, BOOL& bDoAmbush, BOOL& bDoCarChase, BOOL& bDoPlane, BOOL& bDoBomb, BOOL& bDoLowAlt, BOOL& bDoCutScene, BOOL& bDoStationary, ARGS myArgs)
	
	UNUSED_PARAMETER(myLocData)
	bDoStationary = myArgs.bDoStationary
	bTimedPickup = myArgs.bDoTimed
 	bDoCarChase = myArgs.bDoChase
	bDoAmbush = myArgs.bDoAmbush 
	bDoDelicate = myArgs.bDoFragile
	bDoPlane = myArgs.bDoPlane
	bDoBomb = myArgs.bDoBomb
	bDoLowAlt = myArgs.bDoLowAlt
	
	bDoCutScene = myArgs.bDoCutscene
	
//	IF NOT myLocData.bSupportsBombing
//		bDoBomb = FALSE
//	ENDIF
	
	//TODO add any validation
ENDPROC


PROC VALIDATE_VARIATIONS(LOCATION_DATA& myLocData, BOOL& bDoDelicate, BOOL& bTimedPickup, BOOL& bDoAmbush, BOOL& bDoCarChase, 
BOOL& bDoPlane, BOOL& bDoSmugglers, BOOL& bDoFlareCutscene, BOOL& bDoPoliceChase, 
BOOL& bDoContactCutscene, BOOL& bDoHijack, ARGS myArgs)

	bTimedPickup = myArgs.bDoTimed
 	bDoCarChase = myArgs.bDoChase
	bDoAmbush = myArgs.bDoAmbush 
	bDoDelicate = myArgs.bDoFragile
	bDoPlane = myArgs.bDoPlane
	// Smugglers Mode
	bDoSmugglers = myArgs.bDoSmugglers
	bDoFlareCutscene = myArgs.bDoFlareCutscene
	bDoPoliceChase = myArgs.bDoPoliceChase
	bDoContactCutscene = myArgs.bDoContactCutscene
	bDoHijack = myArgs.bDoHijack
	
//	IF NOT myLocData.bSupportsFragile
//		bDoDelicate = FALSE
//	ENDIF
	
	//IF NOT myLocData.bSupportsPlane
		UNUSED_PARAMETER(bTimedPickup)
	//ENDIF
	
	IF NOT myLocData.bSupportsAmbush
		bDoAmbush = FALSE
	ENDIF
	
	IF NOT myLocData.bSupportsChase
		bDoCarChase = FALSE
	ENDIF
	
	IF NOT myLocData.bSupportsPlane
		bDoPlane = FALSE
	ENDIF
	
	// IF WERE DOING SMUGGLERS TURN OFF AMBUSH, LATE AMBUSH, TIMED PICK UP, AND HELI
	IF bDoSmugglers
		bDoAmbush = FALSE
		bTimedPickup = FALSE
		bDoCarChase = FALSE
		bDoPoliceChase = FALSE
	ENDIF
	
ENDPROC

FUNC VECTOR UPDATE_BLIP_PROXIMITY(BLIP_INDEX& myLocationBlip[], VECTOR& vCurDestObjective[], VEHICLE_INDEX& targetCars[], BOOL bValidEntities = TRUE)
	INT closestIndex, idx
	VECTOR vPlayerPos, vClosePos
	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	vCurDestObjective[0] = vCurDestObjective[0]
	vClosePos = <<0,0,0>>
	
	IF bValidEntities
		closestIndex = FIND_CLOSEST_ENTITY_INDEX(targetCars, vPlayerPos)
		IF closestIndex <> -1
			vClosePos = GET_ENTITY_COORDS(targetCars[closestIndex])
		ENDIF
	ELSE
		closestIndex = FIND_CLOSEST_LOCATION_INDEX(vCurDestObjective, vPlayerPos)
		IF closestIndex <> -1
			vClosePos = vCurDestObjective[closestIndex]
		ENDIF
	ENDIF
	
	IF closestIndex <> -1
		REPEAT COUNT_OF(myLocationBlip) idx
			IF DOES_BLIP_EXIST(myLocationBlip[idx])
				IF idx = closestIndex
					SET_BLIP_SCALE(myLocationBlip[idx], 1.0)
				ELSE
					SET_BLIP_SCALE(myLocationBlip[idx], 0.75)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN vClosePos
	
ENDFUNC

PROC CONFIGURE_DIFFICULTY_SETTINGS(LOCATION_DATA& myLocationData, ARGS myArgs)

	SWITCH myArgs.difficultyLevel 
	
		CASE 0// EASY
			myLocationData.iNumWaves = 1
			myLocationData.iNumCarChaserCarsPerWave = 1
			myLocationData.iNumBadGuys = 8
//			myLocationData.difficultySetting = DIFFICULTY_EASY
		BREAK
		CASE 1// MEDIUM
			myLocationData.iNumWaves = 2
			myLocationData.iNumCarChaserCarsPerWave = 1
			myLocationData.iNumBadGuys = 4
//			myLocationData.difficultySetting = DIFFICULTY_MED
		BREAK
		CASE 2 //HARD
			myLocationData.iNumWaves = 2
			myLocationData.iNumCarChaserCarsPerWave = 1
			myLocationData.iNumBadGuys = 4
//			myLocationData.difficultySetting = DIFFICULTY_HARD
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("INVALID DIFFICULTY")
		BREAK
	
	ENDSWITCH

ENDPROC

PROC UPDATE_AIR_DROP(LOCATION_DATA& myLocData, PLANE_DROP_ARGS& planeDropArgs, PLANE_DROP& planeDropState, BLIP_INDEX& myLocationBlip[])

	INT idx
	FLOAT fTempPlanePos
	BOOL bLoaded = FALSE
	VECTOR vPlanePositionAtDrop
//	VECTOR vPackagePosition, vPackageRotation
	
	SWITCH planeDropState
		CASE PLANE_DROP_INIT
			
			PRINTLN("STARTING AIR DROP")
			
			REPEAT MAX_NUM_RECORDS idx
				IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE)
					REQUEST_MODEL(myLocData.dropData[0].carRecData[idx].carrecVehicle)
					REQUEST_VEHICLE_RECORDING(myLocData.dropData[0].carRecData[idx].carrecNum, myLocData.dropData[0].carRecData[idx].carrecName)
				ENDIF	
			ENDREPEAT
		
			REQUEST_MODEL(mnBombProp)
			REQUEST_MODEL(mnParachuteProp)
			REQUEST_ANIM_DICT("p_cargo_chute_s")
			
			IF bUsingMexicans
				myLocData.mnGenericBadGuy = G_M_M_MEXBOSS_01
				REQUEST_MODEL(myLocData.mnGenericBadGuy)
				DEBUG_PRINT("UPDATE PLANE DROP: REQUESTING MEXICAN BAD GUY - G_M_M_MEXBOSS_01")
			ELIF bUsingMarabunta
				myLocData.mnGenericBadGuy = G_M_Y_SalvaGoon_03
				REQUEST_MODEL(myLocData.mnGenericBadGuy)
				DEBUG_PRINT("UPDATE PLANE DROP: REQUESTING MARABUNTA BAD GUY - G_M_Y_SalvaGoon_03")
			ELIF bUsingHillbillies
				myLocData.mnGenericBadGuy = A_M_M_HillBilly_01
				REQUEST_MODEL(myLocData.mnGenericBadGuy)
				DEBUG_PRINT("UPDATE PLANE DROP: REQUESTING HILLBILLY BAD GUY - A_M_M_HillBilly_01")
			ENDIF
			
			REPEAT MAX_NUM_RECORDS idx
				IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE)
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(myLocData.dropData[0].carRecData[idx].carrecNum, myLocData.dropData[0].carRecData[idx].carrecName)
					AND HAS_MODEL_LOADED(myLocData.dropData[0].carRecData[idx].carrecVehicle)
					AND HAS_MODEL_LOADED(myLocData.mnGenericBadGuy)
						bLoaded = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF HAS_MODEL_LOADED(mnBombProp)
			AND HAS_MODEL_LOADED(mnParachuteProp)
			AND HAS_ANIM_DICT_LOADED("p_cargo_chute_s")
			AND bLoaded
				DEBUG_PRINT("GOING TO STATE - PLANE_DROP_CREATE")
				planeDropState = PLANE_DROP_CREATE
			ELSE
				PRINTLN("WAITING ON PLANE MODELS TO LOAD")
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------------------
		CASE PLANE_DROP_CREATE
			// If we're doing a flare cutscene, start plane drop in cutscene; otherwise wait until the player is close.
//			IF bDoFlareCutscene OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vDrugPickup, <<200,200,200>>)
				REPEAT MAX_NUM_RECORDS idx
					IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE)
						
						// Create the plane
						planeDropArgs.planeDropVehicles[idx] = CREATE_VEHICLE(myLocData.dropData[0].carRecData[idx].carrecVehicle, myLocData.dropData[0].carRecData[idx].carCoords,  0.0)
						// Store off indexed plane to use in flare cutscene
						planeDropArgs.planeVehicle = planeDropArgs.planeDropVehicles[idx]
						
						tempPed = CREATE_PED_INSIDE_VEHICLE(planeDropArgs.planeVehicle, PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy)
						
						SET_VEHICLE_ENGINE_ON(planeDropArgs.planeVehicle, TRUE, TRUE)
						SET_ENTITY_LOD_DIST(planeDropArgs.planeVehicle, 1500)
						CONTROL_LANDING_GEAR(planeDropArgs.planeVehicle, LGC_RETRACT_INSTANT)
						OPEN_BOMB_BAY_DOORS(planeDropArgs.planeVehicle)
						
						// Create the package to be dropped
						planeDropArgs.oPackage = CREATE_OBJECT(mnBombProp, myLocData.dropData[0].carRecData[idx].carCoords)
						SET_ENTITY_RECORDS_COLLISIONS(planeDropArgs.oPackage, TRUE)
						SET_ENTITY_LOAD_COLLISION_FLAG(planeDropArgs.oPackage, TRUE)
						SET_ENTITY_LOD_DIST(planeDropArgs.oPackage, 1500)
						SET_ENTITY_VISIBLE(planeDropArgs.oPackage, FALSE)
						SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(planeDropArgs.oPackage, TRUE)
//						// Store off indexed package to be used in flare cutscene
//						planeDropArgs.planeCargo = planeDropArgs.oPackage[idx]
						
						// Create the guy inside the plane and attach the cargo
						IF NOT IS_ENTITY_DEAD(planeDropArgs.planeDropVehicles[idx])
//							planeDropArgs.planeDropPeds[idx] = CREATE_PED_INSIDE_VEHICLE(planeDropArgs.planeDropVehicles[idx], PEDTYPE_CRIMINAL, myLocData.dropData[0].carRecData[idx].carrecActor)
							ATTACH_ENTITY_TO_ENTITY(planeDropArgs.oPackage, planeDropArgs.planeDropVehicles[idx], 0, << 0, -1.640, -0.480 >>, <<0,0,0>>)
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(planeDropArgs.oPackage)
							planeDropArgs.oParachute = CREATE_OBJECT(mnParachuteProp, myLocData.dropData[0].carRecData[idx].carCoords)
							SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(planeDropArgs.oParachute, TRUE)
							ATTACH_ENTITY_TO_ENTITY(planeDropArgs.oParachute, planeDropArgs.oPackage, 0, vParchuteOffset, <<0,0,0>>)
							SET_ENTITY_VISIBLE(planeDropArgs.oParachute, FALSE)
						ENDIF
						
						SET_ENTITY_COORDS(planeDropArgs.planeDropVehicles[idx], myLocData.dropData[0].carRecData[idx].carCoords)
						SET_ENTITY_QUATERNION(planeDropArgs.planeDropVehicles[idx], myLocData.dropData[0].carRecData[idx].x, myLocData.dropData[0].carRecData[idx].y, myLocData.dropData[0].carRecData[idx].j, myLocData.dropData[0].carRecData[idx].k)
						
						DEBUG_PRINT("INSIDE STATE - PLANE_DROP_CREATE")
						planeDropState = PLANE_DROP_START
					ENDIF
				ENDREPEAT
//			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------------------
		CASE PLANE_DROP_START
			REPEAT MAX_NUM_RECORDS idx
				IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE)
					IF NOT IS_ENTITY_DEAD(planeDropArgs.planeDropVehicles[idx])

						START_PLAYBACK_RECORDED_VEHICLE(planeDropArgs.planeDropVehicles[idx], myLocData.dropData[0].carRecData[idx].carrecNum, myLocData.dropData[0].carRecData[idx].carrecName)
						SET_PLAYBACK_SPEED(planeDropArgs.planeDropVehicles[idx], 1.1)
						
						START_AUDIO_SCENE("PLANE_FLY_OVER_SCENE")
						PRINTLN("STARTING AUDIO SCENE - PLANE_FLY_OVER_SCENE")

						planeDropArgs.bPlaneActive = TRUE
						DEBUG_PRINT("STARTING PLAYBACK")
						planeDropState = PLANE_DROP_UPDATE
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------------------
		CASE PLANE_DROP_UPDATE
			REPEAT MAX_NUM_RECORDS idx
				IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE)
					IF NOT IS_ENTITY_DEAD(planeDropArgs.planeDropVehicles[idx])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(planeDropArgs.planeDropVehicles[idx])
							
							fTempPlanePos = GET_POSITION_IN_RECORDING(planeDropArgs.planeDropVehicles[idx])
							PRINTSTRING("PLANE POSITION: ")
							PRINTFLOAT(fTempPlanePos)
							PRINTNL()
							
							vPlanePositionAtDrop = GET_ENTITY_COORDS(planeDropArgs.planeDropVehicles[idx])
							vPlanePositionAtDrop = vPlanePositionAtDrop
							
							IF fTempPlanePos > myLocData.dropData[0].carRecData[idx].fDropTime
								// DROP PACKAGE
								DEBUG_PRINT("DROPPING PACKAGE")
								
								PRINTLN("PLANE POSITION WHEN DROPPING = ", vPlanePositionAtDrop)
								
								DETACH_ENTITY(planeDropArgs.oPackage)
								SET_ENTITY_VISIBLE(planeDropArgs.oPackage, TRUE)
								SET_ENTITY_VISIBLE(planeDropArgs.oParachute, TRUE)
								
								IF NOT DOES_BLIP_EXIST(planeDropArgs.blipPackage)
									STOP_PARTICLE_FX_SAFE(PTFXsmugglersFlare)
									planeDropArgs.blipPackage = ADD_BLIP_FOR_ENTITY(planeDropArgs.oPackage)
									SET_BLIP_COLOUR(planeDropArgs.blipPackage, BLIP_COLOUR_GREEN)
									//SET_BLIP_SCALE(planeDropArgs.blipPackage, 0.5)
									IF DOES_BLIP_EXIST(myLocationBlip[0])
										REMOVE_BLIP(myLocationBlip[0])
									ENDIF
									PRINT_NOW("DTRFKGR_06",DEFAULT_GOD_TEXT_TIME,1)
									PRINTLN("ADDING BLIP - planeDropArgs.blipPackage")
								ENDIF
								
								APPLY_FORCE_TO_ENTITY(planeDropArgs.oPackage, APPLY_TYPE_IMPULSE, <<0,0,0>>, <<0,0,0.5>>, 0, TRUE, TRUE, TRUE)
								SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(planeDropArgs.oPackage, TRUE)
								
								IF NOT IS_ENTITY_DEAD(planeDropArgs.oParachute)
									PLAY_ENTITY_ANIM(planeDropArgs.oParachute, "p_cargo_chute_s_deploy", "p_cargo_chute_s", 1.0, FALSE, TRUE)
									PRINTLN("TASKING TO PLAY DEPLOY ANIM")
								ENDIF
								
								SET_ENTITY_LOD_DIST(planeDropArgs.planeVehicle, 500)
								
								planeDropState = PLANE_DROP_DROP_CARGO
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------------------
		CASE PLANE_DROP_DROP_CARGO
		
			REPEAT MAX_NUM_RECORDS idx
				
				IF NOT IS_ENTITY_DEAD(planeDropArgs.planeDropVehicles[idx])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(planeDropArgs.planeDropVehicles[idx])
						fTempPlanePos = GET_POSITION_IN_RECORDING(planeDropArgs.planeDropVehicles[idx])
						PRINTSTRING("PLANE POSITION 02: ")
						PRINTFLOAT(fTempPlanePos)
						PRINTNL()
					ENDIF
				ENDIF
				
				IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE)
					IF DOES_ENTITY_EXIST(planeDropArgs.oPackage)
					
						SET_ENTITY_LOAD_COLLISION_FLAG(planeDropArgs.oPackage, TRUE)
					
						IF NOT IS_ENTITY_ATTACHED(planeDropArgs.oPackage)
							IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(planeDropArgs.oPackage)
								planeDropArgs.bCargoHasLanded = TRUE
								planeDropArgs.oLandedPackage = planeDropArgs.oPackage
								
//								FREEZE_ENTITY_POSITION(planeDropArgs.oLandedPackage, TRUE)
								
								DETACH_ENTITY(planeDropArgs.oParachute)
								PLAY_ENTITY_ANIM(planeDropArgs.oParachute, "p_cargo_chute_s_crumple", "p_cargo_chute_s", 0.5, FALSE, TRUE, FALSE, 0, ENUM_TO_INT(AF_TURN_OFF_COLLISION))
								PRINTLN("TASKING TO PLAY CRUMPLE ANIM ON PARACHUTE")
								
								SET_ENTITY_COLLISION(planeDropArgs.oParachute, FALSE)
								PRINTLN("TURNING OFF COLLISION ON THE PARACHUTE")
								
								IF NOT IS_TIMER_STARTED(planeDropArgs.tParachuteTimer)
									START_TIMER_NOW(planeDropArgs.tParachuteTimer)
									PRINTLN("STARTING planeDropArgs.tParachuteTimer")
								ENDIF
								
								IF planeDropArgs.bCargoHasLanded
									DEBUG_PRINT("planeDropArgs.bCargoHasLanded = TRUE")
								ENDIF
								
//								vPackagePosition = GET_ENTITY_COORDS(planeDropArgs.oPackage[idx])
//								PRINTLN("PACKAGE POSITION = ", vPackagePosition)
//								vPackageRotation = GET_ENTITY_ROTATION(planeDropArgs.oPackage[idx])
//								PRINTLN("PACKAGE ROTATION = ", vPackageRotation)
//								
//								DELETE_OBJECT(planeDropArgs.oPackage[idx])
//								PRINTLN("DELETING PACKAGE")
//								
//								planeDropArgs.oPackage[idx] = CREATE_OBJECT(myLocData.mnLoadedCargo, vPackagePosition)
//								SET_ENTITY_ROTATION(planeDropArgs.oPackage[idx], vPackageRotation)
								
								DEBUG_PRINT("ENTITY HAS COLLIDED")
	//							ADD_EXPLOSION(GET_ENTITY_COORDS(planeDropArgs.oPackage[idx]), EXP_TAG_HI_OCTANE)
								planeDropState = PLANE_DROP_DELETE_PARACHUTE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------------------
		CASE PLANE_DROP_DELETE_PARACHUTE
			
//			IF NOT planeDropArgs.bUnfreezeProp
//				IF DOES_ENTITY_EXIST(planeDropArgs.oLandedPackage)
//					FREEZE_ENTITY_POSITION(planeDropArgs.oLandedPackage, FALSE)
//					APPLY_FORCE_TO_ENTITY(planeDropArgs.oLandedPackage, APPLY_TYPE_IMPULSE, <<0,0,0>>, <<0,0,0.2>>, 0, TRUE, TRUE, TRUE)
//					planeDropArgs.bUnfreezeProp = TRUE
//				ENDIF
//			ENDIF
		
			// Delete plane if it's not on screen
			IF DOES_ENTITY_EXIST(planeDropArgs.oParachute)
				IF IS_TIMER_STARTED(planeDropArgs.tParachuteTimer) 
					IF GET_TIMER_IN_SECONDS(planeDropArgs.tParachuteTimer) > 1.0
						
						REPEAT MAX_NUM_RECORDS idx
							IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE)
								IF DOES_ENTITY_EXIST(planeDropArgs.oPackage)
									IF DOES_ENTITY_EXIST(planeDropArgs.oPackage)
										FREEZE_ENTITY_POSITION(planeDropArgs.oPackage, TRUE)
										PRINTLN("FREEZING PACKAGE POSITION IDX = ")
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
					
						DELETE_OBJECT(planeDropArgs.oParachute)
						PRINTLN("DELETING PARACHUTE")
						planeDropState = PLANE_DROP_DELETE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------------------
		CASE PLANE_DROP_DELETE
			REPEAT MAX_NUM_RECORDS idx
				IF myLocData.dropData[0].carRecData[idx].bConfigured AND (myLocData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE)
					IF NOT IS_ENTITY_DEAD(planeDropArgs.planeDropVehicles[idx])
//						IF NOT IS_ENTITY_ON_SCREEN(planeDropArgs.planeDropVehicles[idx])

						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(planeDropArgs.planeDropVehicles[idx])
							STOP_AUDIO_SCENE("PLANE_FLY_OVER_SCENE")
							PRINTLN("STOPPING AUDIO SCENE - PLANE_FLY_OVER_SCENE")
							
							SET_PED_AS_NO_LONGER_NEEDED(tempPed)
							PRINTLN("SETTING PED AS NO LONGER NEEDED - tempPed")
							SET_VEHICLE_AS_NO_LONGER_NEEDED(planeDropArgs.planeDropVehicles[idx])
							PRINTLN("SETTING PLANE AS NO LONGER NEEDED")
							
							DELETE_VEHICLE(planeDropArgs.planeDropVehicles[idx])
							DELETE_PED(tempPed)
							PRINTLN("DELETING PLANE")
							planeDropArgs.bPlaneActive = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------------------
		CASE PLANE_DROP_COMPLETE
		
		BREAK	
	ENDSWITCH
		
ENDPROC

PROC PLANE_HINT_CAM()
	
	IF NOT DOES_CAM_EXIST(camGroundOneHintCam)
		camGroundOneHintCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 2307.6038, 3084.8286, 59.0214 >>, << -11.3797, -0.0000, -48.7896 >>)
		SET_CAM_ACTIVE(camGroundOneHintCam, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		SET_CAM_FOV(camGroundOneHintCam, 60.0)
	ENDIF

ENDPROC

PROC PRINT_TRAP_OJBECTIVE(TRAP_ARGS& trapArgs, BLIP_INDEX& myLocationBlip[])
	IF NOT trapArgs.bPrintTrapObjective
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		
			IF trapArgs.bRunningForSmugglers AND NOT trapArgs.bReset
				PRINT_NOW("DTRFKGR_LEAD_1", DEFAULT_GOD_TEXT_TIME, 1)
			ELSE
				PRINT_NOW("DTRFKGR_LEAD", DEFAULT_GOD_TEXT_TIME, 1)
			ENDIF
			
			// Show a blip for the trap... not specific to side yet.
			IF NOT DOES_BLIP_EXIST(trapArgs.blipTrap)
				trapArgs.blipTrap = ADD_BLIP_FOR_COORD(trapArgs.vTrapPos)
				
				IF DOES_BLIP_EXIST(trapArgs.blipTrap)
					SET_BLIP_COLOUR(trapArgs.blipTrap, BLIP_COLOUR_YELLOW)
					SET_BLIP_ROUTE(trapArgs.blipTrap, TRUE)
					SET_BLIP_NAME_FROM_TEXT_FILE(trapArgs.blipTrap, "DTRFKGR_BLIP08")
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(myLocationBlip[0])
				REMOVE_BLIP(myLocationBlip[0])
				PRINTLN("REMOVING myLocationBlip[0] VIA PRINT_TRAP_OJBECTIVE")
			ELSE	
				PRINTLN("myLocationBlip[0] DOES NOT EXIST")
			ENDIF
			
			trapArgs.bPrintTrapObjective = TRUE
			DEBUG_PRINT("trapArgs.bPrintTrapObjective = TRUE")
		ENDIF
	ENDIF
ENDPROC

PROC ADD_TRAP_BLIP(TRAP_ARGS& trapArgs)

	// If the generic blip exists, remove it and add specific blip
	IF DOES_BLIP_EXIST(trapArgs.blipTrap)
		REMOVE_BLIP(trapArgs.blipTrap)
		trapArgs.blipTrap = ADD_BLIP_FOR_COORD(trapArgs.vTrapPos)
		SET_BLIP_COLOUR(trapArgs.blipTrap, BLIP_COLOUR_YELLOW)
		SET_BLIP_ROUTE(trapArgs.blipTrap, TRUE)
		SET_BLIP_NAME_FROM_TEXT_FILE(trapArgs.blipTrap, "DTRFKGR_BLIP08")
	ELSE
		trapArgs.blipTrap = ADD_BLIP_FOR_COORD(trapArgs.vTrapPos)
		SET_BLIP_COLOUR(trapArgs.blipTrap, BLIP_COLOUR_YELLOW)
		SET_BLIP_ROUTE(trapArgs.blipTrap, TRUE)
		SET_BLIP_NAME_FROM_TEXT_FILE(trapArgs.blipTrap, "DTRFKGR_BLIP08")
	ENDIF
	
ENDPROC

PROC ENDING_TRAP(TRAP_ARGS& trapArgs, VEHICLE_INDEX& myVehicle)
	IF IS_VEHICLE_DRIVEABLE(myVehicle)
		SET_ENTITY_PROOFS(myVehicle, FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_VEHICLE_FORWARD_SPEED(myVehicle, 10.0)
	ENDIF
	
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	
	SET_CAM_ACTIVE(trapArgs.camTrap, FALSE)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DESTROY_CAM(trapArgs.camTrap)
	
	IF NOT IS_PED_INJURED(trapArgs.pedOscar)
		CLEAR_PED_TASKS(trapArgs.pedOscar)
		TASK_PLAY_ANIM(trapArgs.pedOscar, "amb@world_human_smoking@male@male_a@base", "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
	ENDIF
	
	IF DOES_ENTITY_EXIST(trapArgs.oTrapProp)
		DELETE_OBJECT(trapArgs.oTrapProp)
	ENDIF
	
//	PRINT_NOW("DTRFKGR_03", DEFAULT_GOD_TEXT_TIME, 1)
	
	SET_TIME_SCALE(1.0)
ENDPROC

FUNC BOOL IS_TRAP_IN_FRONT_OF_THE_PLAYER(VEHICLE_INDEX& myVehicle)
	VECTOR vPlayerPos
	VECTOR vPlayerDir, vTrapDir
	
	IF NOT IS_ENTITY_DEAD(myVehicle)
		vPlayerPos = GET_ENTITY_COORDS(myVehicle)
		vPlayerDir = GET_ENTITY_FORWARD_VECTOR(myVehicle)
	ENDIF
	
	vTrapDir = (vCenterTrapPos - vPlayerPos)
	
	IF DOT_PRODUCT(vPlayerDir, vTrapDir) > 0
		DEBUG_PRINT("RETURNING TRUE ON IS_TRAP_IN_FRONT_OF_THE_PLAYER")
		RETURN TRUE
	ELSE
		DEBUG_PRINT("RETURNING FALSE ON IS_TRAP_IN_FRONT_OF_THE_PLAYER")
		RETURN FALSE
	ENDIF
	
ENDFUNC

PROC HANDLE_TRAP_RESET(TRAP_ARGS& trapArgs, SMUGGLERS_MODE_ARGS& smugArgs, TRAP_STATE& trapStages)
	IF NOT trapArgs.bReset
		// We need to allow a reset if you started, but we need to wait if you started and haven't fully finished.
		IF trapStages > TRAP_STATE_01 AND NOT (trapStages = TRAP_STATE_07)
		AND NOT (trapStages = TRAP_STATE_08)
			// If the trap has been brought up once, but then never reached on smugglers... reset for car chase
			// Trap has been set for smugglers
			IF trapArgs.bRunningForSmugglers 
				// We're doing smugglers and we've completed it, but we have not ran the trap... so reset
				IF smugArgs.bGoToDestination AND NOT bRanTrapOnce
					DEBUG_PRINT("RESETTING FOR CAR CHASE!!!")
					IF DOES_ENTITY_EXIST(trapArgs.oTrapProp)
						DELETE_OBJECT(trapArgs.oTrapProp)
						DEBUG_PRINT("DELETING TRAP ON RESET")
					ENDIF
					trapStages = TRAP_STATE_01
					trapArgs.bReset = TRUE
					trapArgs.bPrintTrapObjective = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_TRAP(TRAP_ARGS& trapArgs, structPedsForConversation& MyLocalPedStruct)
	
	// Dispatch Dialogue
	IF trapArgs.bReset
		CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP01", CONV_PRIORITY_VERY_HIGH)
	ELSE
		CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP", CONV_PRIORITY_VERY_HIGH)
	ENDIF
	
	SET_ROADS_IN_ANGLED_AREA(<< 1651.576, 4939.412, -410.818 >>, << 1721.456, 4540.486, 410.818 >>, 145.000, FALSE, FALSE)
	
	// Give a default value to the blip
	trapArgs.vTrapPos = vCenterTrapPos
	
	trapArgs.oTrapProp = CREATE_OBJECT(trapArgs.mnTrapProp, trapArgs.vTrapPos)
	SET_ENTITY_ROTATION(trapArgs.oTrapProp, <<-90,0,0>>)
	SET_ENTITY_LOD_DIST(trapArgs.oTrapProp, 500)
ENDPROC

FUNC BOOL UPDATE_TRAP(LOCATION_DATA &myLocData, TRAP_ARGS& trapArgs, CAR_CHASER_ARGS& carChaserArgs, SMUGGLERS_MODE_ARGS& smugArgs, TRAP_STATE& trapStages, 
BOOL& bIsChaseActive, structPedsForConversation& MyLocalPedStruct, VEHICLE_INDEX& myVehicle, BOOL& bDoCarChase, BOOL& bDoSmugglers,
BLIP_INDEX& myLocationBlip[], TEXT_LABEL_31& sLastObjective)
	
	VECTOR vCamVector, vCamVector01
	VECTOR vCamRotation, vCamRotation01
	VECTOR vAngledAreaVec01, vAngledAreaVec02
	VECTOR vPlayerPos
	VECTOR vChaserPos
	FLOAT fChaserHeading
	VECTOR vFrontOffset = <<-2,10,2>>
	VECTOR vChaserRot
	VECTOR vOffsetToUse
	INT iInterpTime = 3000
	INT iDistanceFromCenterPos = 250

	trapArgs.vNorthSide = << 1663.5786, 4891.7778, 41.0890 >>
	trapArgs.vSouthSide = << 1677.2208, 4765.8438, 40.9952 >>
	
	IF trapStages > TRAP_STATE_01 AND trapStages <> TRAP_STATE_07
	AND trapStages <> TRAP_STATE_08
		IF bDoCarChase
			IF NOT bIsChaseActive
				IF DOES_BLIP_EXIST(trapArgs.blipTrap)
					REMOVE_BLIP(trapArgs.blipTrap)
					DEBUG_PRINT("REMOVING trapArgs.blipTrap DUE TO CHASE NO LONGER BEING ACTIVE")
				ENDIF
				
				PRINTLN("STOPPING CONVERSATIONS - 1")
				STOP_SCRIPTED_CONVERSATION (FALSE)
				SAFE_CLEAR_THIS_PRINT("DTRFKGR_LEAD")
				
				RETURN FALSE
			ENDIF
		ENDIF
		IF bDoSmugglers
			IF smugArgs.bGoToDestination
				IF DOES_BLIP_EXIST(trapArgs.blipTrap)
					REMOVE_BLIP(trapArgs.blipTrap)
					DEBUG_PRINT("REMOVING trapArgs.blipTrap DUE TO smugArgs.bGoToDestination BEING TRUE")
				ENDIF
				
				PRINTLN("STOPPING CONVERSATIONS - 2")
				STOP_SCRIPTED_CONVERSATION (FALSE)
				SAFE_CLEAR_THIS_PRINT("DTRFKGR_LEAD")
			
			ENDIF
		ENDIF
	ENDIF
	
	HANDLE_TRAP_RESET(trapArgs, smugArgs, trapStages)
	
	IF trapStages > TRAP_STATE_01
		PRINT_TRAP_OJBECTIVE(trapArgs, myLocationBlip)
	ENDIF
	
	//----------------------------------------------------FAIL SAFE----------------------------------------------------
	IF trapStages > TRAP_STATE_03 AND trapStages < TRAP_STATE_08
		IF IS_TIMER_STARTED(trapArgs.tTrapTimer)
			IF TIMER_DO_WHEN_READY(trapArgs.tTrapTimer, 10.0)
				ENDING_TRAP(trapArgs, myVehicle)
				DEBUG_PRINT("TRIGGERING FAIL SAFE IN UPDATE_TRAP")
				trapStages = TRAP_STATE_09
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bRanTrapOnce
		SWITCH trapStages
			CASE TRAP_INIT
				REQUEST_MODEL(trapArgs.mnOscar)
				DEBUG_PRINT("REQUESTING trapArgs.mnOscar")
				REQUEST_MODEL(trapArgs.mnTrapProp)
				DEBUG_PRINT("REQUESTING trapArgs.mnTrapProp")
				
				REQUEST_ANIM_DICT("amb@world_human_smoking@male@male_a@base")
				DEBUG_PRINT("REQUESTING ANIMATION - amb@world_human_smoking@male@male_a@base")
						
				IF HAS_MODEL_LOADED(trapArgs.mnOscar) AND HAS_MODEL_LOADED(trapArgs.mnTrapProp)
				AND HAS_ANIM_DICT_LOADED("amb@world_human_smoking@male@male_a@base")
				
					PRINTLN("GOING TO STATE - TRAP_STATE_01")
					trapStages = TRAP_STATE_01
				ELSE
					PRINTLN("WAITING ON TRAP MODELS TO LOAD")
				ENDIF
			BREAK
			//================================================================================================================================
			CASE TRAP_STATE_01
			
				// Check first if a car chase is active
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF bIsChaseActive
						vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
//						PRINTSTRING("PLAYER'S DISTANCE TO CENTER POSITION = ")
//						PRINTFLOAT(VDIST(vPlayerPos, vCenterTrapPos))
//						PRINTNL()

						IF NOT IS_ENTITY_DEAD(carChaserArgs.vehCarChasers[0])
							// Check if the car chase has been going for x amount of time, or if the player is close to the airfield
							IF IS_TIMER_STARTED(carChaserArgs.tChaseActiveTimer) AND TIMER_DO_WHEN_READY(carChaserArgs.tChaseActiveTimer, trapArgs.fTriggerTrapTime)
							AND (VDIST(vPlayerPos, vCenterTrapPos) > iDistanceFromCenterPos AND VDIST(vPlayerPos, vCenterTrapPos) < 900)
							AND (VDIST(vPlayerPos, GET_ENTITY_COORDS(carChaserArgs.vehCarChasers[0])) < 150)
							AND (VDIST(vPlayerPos, myLocData.endData[0].vDrugPickup) > 200)
								vehEnemyVehicle = carChaserArgs.vehCarChasers[0]
								DEBUG_PRINT("SETTING vehEnemyVehicle = carChaserArgs.vehCarChasers[0]")
							
								SETUP_TRAP(trapArgs, MyLocalPedStruct)
								
								DEBUG_PRINT("CAR CHASE - GOING TO STATE - TRAP_STATE_02")
								trapStages = TRAP_STATE_02
							ENDIF
						ENDIF
					ELSE
						vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
//						PRINTSTRING("PLAYER'S DISTANCE TO CENTER POSITION = ")
//						PRINTFLOAT(VDIST(vPlayerPos, vCenterTrapPos))
//						PRINTNL()
						
						IF NOT IS_ENTITY_DEAD(smugArgs.smugglersVehicles[0])
							IF bDoSmugglers AND NOT smugArgs.bGoToDestination
								IF IS_TIMER_STARTED(smugArgs.tTrapStartTimer) AND TIMER_DO_WHEN_READY(smugArgs.tTrapStartTimer, trapArgs.fTriggerTrapTime)
								AND (VDIST(vPlayerPos, vCenterTrapPos) > iDistanceFromCenterPos AND VDIST(vPlayerPos, vCenterTrapPos) < 900)
								AND (VDIST(vPlayerPos, GET_ENTITY_COORDS(smugArgs.smugglersVehicles[0])) < 150)
								AND (VDIST(vPlayerPos, myLocData.endData[0].vDrugPickup) > 200)
								
									trapArgs.bRunningForSmugglers = TRUE
									vehEnemyVehicle = smugArgs.smugglersVehicles[0]
									DEBUG_PRINT("SETTING vehEnemyVehicle = smugArgs.vehSmugglerCarWithDrugs")
								
									SETUP_TRAP(trapArgs, MyLocalPedStruct)
									
									DEBUG_PRINT("SMUGGLERS - GOING TO STATE - TRAP_STATE_02")
									trapStages = TRAP_STATE_02
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			//================================================================================================================================
			CASE TRAP_STATE_02
			
				IF IS_ENTITY_AT_COORD(myVehicle, trapArgs.vNorthSide, <<30,30,30>>)
				AND IS_TRAP_IN_FRONT_OF_THE_PLAYER(myVehicle)
					trapArgs.bNorthSide = TRUE
					trapArgs.vTrapPos = << 1671.5881, 4835.1348, 41.1 >>
					DEBUG_PRINT("trapArgs.bNorthSide IS TRUE")
					trapArgs.bGotSide = TRUE
				ENDIF
				IF IS_ENTITY_AT_COORD(myVehicle, trapArgs.vSouthSide, <<30,30,30>>)
				AND IS_TRAP_IN_FRONT_OF_THE_PLAYER(myVehicle)
					trapArgs.bSouthSide = TRUE
					trapArgs.vTrapPos = << 1670.2511, 4845.7544, 41.0679 >>
					DEBUG_PRINT("trapArgs.bSouthSide IS TRUE")
					trapArgs.bGotSide = TRUE
				ENDIF
				
				IF trapArgs.bGotSide
					// If the player is coming from the north, create Oscar on the south side of the buiding
					IF trapArgs.bNorthSide
				
						ADD_TRAP_BLIP(trapArgs)
							
						SET_ENTITY_COORDS(trapArgs.oTrapProp, trapArgs.vTrapPos)
						
						// Create Oscar
						trapArgs.pedOscar = CREATE_PED(PEDTYPE_CIVMALE, trapArgs.mnOscar, << 1684.9423, 4835.0522, 41.0118 >>, 92.6607)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(trapArgs.pedOscar, TRUE)
					
						ADD_COVER_POINT(<< 1683.3269, 4834.7090, 41.0192 >>, 7.0588, COVUSE_WALLTORIGHT, COVHEIGHT_HIGH, COVARC_180)
						
						IF DOES_ENTITY_EXIST(trapArgs.pedOscar) AND NOT IS_PED_INJURED(trapArgs.pedOscar)
							// Commenting out, because this function was not working in build, v206.1
//							TASK_SEEK_COVER_TO_COVER_POINT(trapArgs.pedOscar, trapArgs.coverSouth, << 1683.3269, 4834.7090, 41.0192 >>, -1)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(trapArgs.pedOscar, << 1683.3269, 4834.7090, 41.0192 >>, -1)
							PRINTLN("TASKING OSCAR TO SEEK COVER 01")
						ENDIF
						
						DEBUG_PRINT("CREATING OSCAR ON SOUTH SIDE")
					
					// If the player is coming from the south, create Oscar on the north side of the building
					ELIF trapArgs.bSouthSide
				
						ADD_TRAP_BLIP(trapArgs)
						
						SET_ENTITY_COORDS(trapArgs.oTrapProp, trapArgs.vTrapPos)
					
						// Create Oscar
						trapArgs.pedOscar = CREATE_PED(PEDTYPE_CIVMALE, trapArgs.mnOscar, << 1682.0835, 4847.7891, 41.1619 >>, 119.3626)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(trapArgs.pedOscar, TRUE)
						
						ADD_COVER_POINT(<< 1681.7396, 4846.7974, 41.1265 >>, 186.3530, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_180)
					
						IF DOES_ENTITY_EXIST(trapArgs.pedOscar) AND NOT IS_PED_INJURED(trapArgs.pedOscar)
							// Commenting out, because this function was not working in build, v206.1
//							TASK_SEEK_COVER_TO_COVER_POINT(trapArgs.pedOscar, trapArgs.coverNorth, << 1681.7396, 4846.7974, 41.1265 >>, -1)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(trapArgs.pedOscar, << 1681.7396, 4846.7974, 41.1265 >>, -1)
							PRINTLN("TASKING OSCAR TO SEEK COVER 02")
						ENDIF
						
						DEBUG_PRINT("CREATING OSCAR ON NORTH SIDE")
					ENDIF
				
					DEBUG_PRINT("GOING TO STATE - TRAP_STATE_03")
					trapStages = TRAP_STATE_03
				ENDIF
			BREAK
			//================================================================================================================================
			CASE TRAP_STATE_03
			
//				PRINTLN("trapArgs.vTrapPos = ", trapArgs.vTrapPos)
//				PRINTSTRING("PLAYERS DISTANCE TO TRAP POSITION = ")
//				PRINTFLOAT(VDIST(vPlayerPos, trapArgs.vTrapPos))
//				PRINTNL()
			
				// Check if the car chaser is in trap area... cut camera, blow up the car
				IF IS_VEHICLE_DRIVEABLE(vehEnemyVehicle)
					IF IS_ENTITY_AT_COORD(vehEnemyVehicle, trapArgs.vTrapPos, <<20,20,20>>)
					OR IS_ENTITY_AT_COORD(myVehicle, trapArgs.vTrapPos, <<20,20,20>>)
					
						// If the player is coming from the north, that means Oscar is on the south side... Oscar/camera always opposite of player.
						IF trapArgs.bNorthSide
							// 2 cam - cam focused on Oscar
							vCamVector = << 1686.0687, 4833.3125, 41.8653 >>  
							vCamRotation =  << 1.3368, -0.0284, 76.0114 >>
							// 1 cam - cam focused on package
							vCamVector01 = << 1675.5507, 4834.6538, 43.1803 >> 
							vCamRotation01 = << -6.5055, -0.1800, 84.8734 >>
							
							IF NOT IS_ENTITY_DEAD(vehEnemyVehicle)
								SET_ENTITY_COORDS(vehEnemyVehicle, << 1662.1481, 4877.9121, 41.0532 >>)
								SET_ENTITY_HEADING(vehEnemyVehicle, 184.3198)
								FREEZE_ENTITY_POSITION(vehEnemyVehicle, TRUE)
							ENDIF
							
							IF NOT IS_ENTITY_DEAD(myVehicle)
								SET_ENTITY_COORDS(myVehicle, << 1664.3207, 4864.2969, 41.0448 >>)
								SET_ENTITY_HEADING(myVehicle, 185.9042)
								FREEZE_ENTITY_POSITION(myVehicle, TRUE)
							ENDIF
							
							DEBUG_PRINT("USING THE bNorthSide VECTORS")
						ELIF trapArgs.bSouthSide
							// 2 cam - cam focused on Oscar
							vCamVector = << 1682.7050, 4847.6104, 42.3463 >> 
							vCamRotation = << -8.8261, 0.0282, 105.6904 >> 
							// 1 cam - cam focused on package
							vCamVector01 = << 1674.7518, 4846.3091, 41.8394 >>
							vCamRotation01 = << -1.4958, 0.0000, 99.3335 >>
							
							IF NOT IS_ENTITY_DEAD(vehEnemyVehicle)
								SET_ENTITY_COORDS(vehEnemyVehicle, << 1673.9340, 4817.3232, 41.0525 >>)
								SET_ENTITY_HEADING(vehEnemyVehicle, 8.3252)
								FREEZE_ENTITY_POSITION(vehEnemyVehicle, TRUE)
							ENDIF
							
							IF NOT IS_ENTITY_DEAD(myVehicle)
								SET_ENTITY_COORDS(myVehicle, << 1677.4688, 4813.0278, 41.0247 >>)
								SET_ENTITY_HEADING(myVehicle, 4.9601)
								FREEZE_ENTITY_POSITION(myVehicle, TRUE)
							ENDIF
							
							DEBUG_PRINT("USING THE bSouthSide VECTORS")
						ELSE
							vCamVector = << 1680.5353, 4831.1812, 42.6465 >>
							vCamRotation = << -3.3921, -0.0000, -1.9242 >>
							DEBUG_PRINT("USING THE DEFAULT VECTORS")
						ENDIF
						
						// Create camera
						trapArgs.camTrap = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamVector, vCamRotation)
						SET_CAM_ACTIVE(trapArgs.camTrap, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						trapArgs.camTransition = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamVector01, vCamRotation01)
						SET_CAM_ACTIVE_WITH_INTERP(trapArgs.camTrap, trapArgs.camTransition, iInterpTime)
						POINT_CAM_AT_ENTITY(trapArgs.camTransition, trapArgs.oTrapProp, <<0,0,0>>)
							
						IF NOT IS_TIMER_STARTED(trapArgs.tTrapTimer)
							START_TIMER_NOW(trapArgs.tTrapTimer)
						ENDIF
						
						IF DOES_BLIP_EXIST(trapArgs.blipTrap)
							REMOVE_BLIP(trapArgs.blipTrap)
						ENDIF
						
						DISPLAY_HUD(FALSE)
						DISPLAY_RADAR(FALSE)
						CLEAR_PRINTS()
						STOP_SCRIPTED_CONVERSATION(TRUE)
						
						DEBUG_PRINT("GOING TO STATE - TRAP_STATE_04")
						trapStages = TRAP_STATE_04
					ENDIF
				ENDIF
			BREAK
			//================================================================================================================================
			CASE TRAP_STATE_04
				
				IF TIMER_DO_WHEN_READY(trapArgs.tTrapTimer, 1.5)
					IF NOT IS_ENTITY_DEAD(myVehicle)
						FREEZE_ENTITY_POSITION(myVehicle, FALSE)
						SET_VEHICLE_FORWARD_SPEED(myVehicle, 35.0)
					ENDIF
					
					DEBUG_PRINT("GOING TO STATE - TRAP_STATE_05")
					trapStages = TRAP_STATE_05
				ENDIF
			BREAK
			//================================================================================================================================
			CASE TRAP_STATE_05
				// Early Explosion
				IF TIMER_DO_WHEN_READY(trapArgs.tTrapTimer, 2.5)
					IF IS_VEHICLE_DRIVEABLE(vehEnemyVehicle)
						
						IF NOT IS_ENTITY_DEAD(vehEnemyVehicle)
							FREEZE_ENTITY_POSITION(vehEnemyVehicle, FALSE)
							
							TASK_VEHICLE_DRIVE_TO_COORD(GET_PED_IN_VEHICLE_SEAT(vehEnemyVehicle), vehEnemyVehicle, trapArgs.vTrapPos, 35.0, DRIVINGSTYLE_STRAIGHTLINE, COGCABRIO, DRIVINGMODE_PLOUGHTHROUGH, 1, -1)
							
							SET_VEHICLE_FORWARD_SPEED(vehEnemyVehicle, 35.0)
						ENDIF
						IF NOT IS_ENTITY_DEAD(myVehicle)
							FREEZE_ENTITY_POSITION(myVehicle, FALSE)
						ENDIF
						
						DEBUG_PRINT("GOING TO STATE - TRAP_STATE_06")
						trapStages = TRAP_STATE_06
					ENDIF
				ENDIF
			BREAK
			//================================================================================================================================
			CASE TRAP_STATE_06
				IF trapArgs.bNorthSide
					vAngledAreaVec01 = << 1671.609, 4836.023, 42.074 >>
					vAngledAreaVec02 = << 1671.985, 4833.047, 42.074 >>
				ELIF trapArgs.bSouthSide
					vAngledAreaVec01 = << 1670.522, 4845.571, 42.074 >>
					vAngledAreaVec02 = << 1670.222, 4848.053, 42.074 >>
				ENDIF
			
				IF IS_ENTITY_IN_ANGLED_AREA(vehEnemyVehicle, vAngledAreaVec01, vAngledAreaVec02, 3.0, FALSE, FALSE)
					IF IS_VEHICLE_DRIVEABLE(myVehicle)
					
						SET_TIME_SCALE(0.5)
						
						IF trapArgs.bNorthSide
							IF NOT IS_ENTITY_DEAD(myVehicle)
								SET_ENTITY_COORDS(myVehicle, << 1672.8822, 4793.7734, 40.9954 >>)
								SET_ENTITY_HEADING(myVehicle, 183.5411)
							ENDIF
						ELIF trapArgs.bSouthSide
							IF NOT IS_ENTITY_DEAD(myVehicle)
								SET_ENTITY_COORDS(myVehicle, << 1668.1105, 4884.9414, 41.0673 >>)
								SET_ENTITY_HEADING(myVehicle, 8.9855)
							ENDIF
						ENDIF
						
						SET_ENTITY_PROOFS(myVehicle, FALSE, TRUE, TRUE, TRUE, FALSE)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(vehEnemyVehicle)
					
						STOP_CAM_POINTING(trapArgs.camTrap)
						
						// Get position and heading to use in offset
						vChaserPos = GET_ENTITY_COORDS(vehEnemyVehicle)
						PRINTSTRING("vChaserPos = ")
						PRINTVECTOR(vChaserPos)
						PRINTNL()
						fChaserHeading = GET_ENTITY_HEADING(vehEnemyVehicle)
						PRINTLN("fChaserHeading = ", fChaserHeading)
						vChaserRot = GET_ENTITY_ROTATION(vehEnemyVehicle)
						PRINTLN("vChaserRot = ", vChaserRot)
						
						vOffsetToUse = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vChaserPos, fChaserHeading, vFrontOffset)
						
						SET_CAM_COORD(trapArgs.camTrap, vOffsetToUse)
						PRINTLN("vFrontOffset = ", vFrontOffset)
						SET_CAM_ROT(trapArgs.camTrap, <<vChaserRot.x - 10, vChaserRot.y, (vChaserRot.z - 180)>>)
						PRINTLN("trapArgs.camTrap ROTATION = ", <<vChaserRot.x - 10, vChaserRot.y , (vChaserRot.z - 180)>>)
						
						IF NOT IS_PED_INJURED(trapArgs.pedOscar)
							CLEAR_PED_TASKS(trapArgs.pedOscar)
							SET_ENTITY_HEADING(trapArgs.pedOscar, fChaserHeading)
							PRINTLN("CLEARING OSCAR'S TASKS")
							
//							TASK_PLAY_ANIM(trapArgs.pedOscar, "gestures@male", "anger_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						ENDIF
						
						EXPLODE_VEHICLE(vehEnemyVehicle)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(trapArgs.oTrapProp)
						ADD_EXPLOSION(GET_ENTITY_COORDS(trapArgs.oTrapProp), EXP_TAG_GRENADE)
					ENDIF
					
					// Dispatch Dialogue
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_TRAP", CONV_PRIORITY_VERY_HIGH)

					RESTART_TIMER_NOW(trapArgs.tTrapTimer)
					SETTIMERA(0)
					
					DEBUG_PRINT("GOING TO STATE - TRAP_STATE_07")
					trapStages = TRAP_STATE_07
				ENDIF
			BREAK
			//================================================================================================================================
			CASE TRAP_STATE_07
			
				FLOAT fGameSpeed
				fGameSpeed = TO_FLOAT(TIMERA() - 40) / 220.0
				If fGameSpeed > 1.0
					fGameSpeed = 1.0
				ENDIF
				IF fGameSpeed < 0.1
					fGameSpeed = 0.1
				ENDIF
				SET_TIME_SCALE(fGameSpeed)	
				
//				FLOAT fTempTime
//				fTempTime = GET_TIMER_IN_SECONDS(trapArgs.tTrapTimer)
//				PRINTLN("TIME = ", fTempTime)
	
//				IF TIMERA() > 1000
				IF IS_TIMER_STARTED(trapArgs.tTrapTimer)
					IF GET_TIMER_IN_SECONDS(trapArgs.tTrapTimer) > 1.0
						
						ENDING_TRAP(trapArgs, myVehicle)
						
						bDoCarChase = FALSE
						DEBUG_PRINT("SETTING bDoCarChase = FALSE IN UPDATE_TRAP")
						
						DEBUG_PRINT("GOING TO STATE - TRAP_STATE_08")
						trapStages = TRAP_STATE_08
					ENDIF
				ELSE
					PRINTLN("TIMER IS NOT STARTED")
				ENDIF
			BREAK
			//================================================================================================================================
			CASE TRAP_STATE_08
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_TP_F", CONV_PRIORITY_VERY_HIGH)
					
					bRanTrapOnce = TRUE
					DEBUG_PRINT("SETTING bRanTrapOnce = TRUE")
					
					DEBUG_PRINT("GOING TO STATE - TRAP_STATE_09")
					trapStages = TRAP_STATE_09
				ENDIF
			BREAK
			//================================================================================================================================
			CASE TRAP_STATE_09
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINT_NOW("DTRFKGR_03",DEFAULT_GOD_TEXT_TIME,1) 
					sLastObjective = "DTRFKGR_03"
					myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.endData[0].vDrugPickup)
					
					IF DOES_BLIP_EXIST(myLocationBlip[0])
	//					SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
						SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
					ENDIF
					
					DEBUG_PRINT("GOING TO STATE - TRAP_STATE_10")
					trapStages = TRAP_STATE_10
				ENDIF
			BREAK
			//================================================================================================================================
			CASE TRAP_STATE_10
			
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DEBUG_PRINT_MODES(BOOL& bDoTimed, BOOL& bDoChase, BOOL& bDoFragile, BOOL& bDoAmbush, BOOL& bDoPlane, BOOL& bDoSmugglers, 
BOOL& bDoFlareCutscene, BOOL& bDoPoliceChase, BOOL& bDoContactCutscene, BOOL& bDoHijack)
	
	IF bDoTimed 
		DEBUG_PRINT("bDoTimed IS TRUE")
	ENDIF
	IF bDoChase
		DEBUG_PRINT("bDoChase IS TRUE")
	ENDIF
	IF bDoFragile
		DEBUG_PRINT("bDoFragile IS TRUE")
	ENDIF
	IF bDoAmbush
		DEBUG_PRINT("bDoAmbush IS TRUE")
	ENDIF
	IF bDoPlane
		DEBUG_PRINT("bDoPlane IS TRUE")
	ENDIF
	IF bDoSmugglers
		DEBUG_PRINT("bDoSmugglers IS TRUE")
	ENDIF
	IF bDoFlareCutscene
		DEBUG_PRINT("bDoFlareCutscene IS TRUE")
	ENDIF
	IF bDoPoliceChase
		DEBUG_PRINT("bDoPoliceChase IS TRUE")
	ENDIF
	IF bDoContactCutscene
		DEBUG_PRINT("bDoContactCutscene IS TRUE")
	ENDIF
	IF bDoHijack
		DEBUG_PRINT("bDoHijack IS TRUE")
	ENDIF
	
ENDPROC

//PROC CREATE_OFFSET_WIDGETS_NEW()
//
//	START_WIDGET_GROUP("DrugDropCrateOffset") 
//		DEBUG_PRINT("INSIDE CREATE WIDGETS")
//		ADD_WIDGET_FLOAT_SLIDER("crateOffsetX", fOffsetX, -10, 10, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("crateOffsetY", fOffsetY, -10, 10, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("crateOffsetZ", fOffsetZ, -10, 10, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("crateHeadingX", fHeadingX, -180, 180, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("crateHeadingY", fHeadingY, -180, 180, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("crateHeadingZ", fHeadingZ, -180, 180, 0.01)
//	STOP_WIDGET_GROUP()
//
//ENDPROC
//
//PROC RUN_WIDGETS_NEW(VEHICLE_INDEX vehicle, OBJECT_INDEX &oCargoDrop)
//	IF DOES_ENTITY_EXIST(vehicle) 
////		DEBUG_PRINT("RUNNING WIDGETS")
//		IF NOT IS_ENTITY_DEAD(vehicle)
//			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehicle)
//				ATTACH_ENTITY_TO_ENTITY(oCargoDrop, vehicle, 0, <<fOffsetX, fOffsetY, fOffsetZ>>, <<fHeadingX, fHeadingY, fHeadingZ>> )
//				DEBUG_PRINT("INSIDE ATTACHMENT")
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

