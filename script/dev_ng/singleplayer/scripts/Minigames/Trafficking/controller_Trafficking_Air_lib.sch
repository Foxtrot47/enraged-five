// controller_Trafficking_Air_lib.sch
USING "controller_trafficking.sch"
USING "flow_help_public.sch"

TRAFFICKING_STATE 	eAirState
VEHICLE_INDEX		vehPlane
BOOL 				bPrintedInVehAir = FALSE
VECTOR 				vPlaneSpawnPosition = <<2136.1331, 4780.5635, 39.9702>>

//VECTOR vSpawnPosition = <<2136.1331, 4780.5635, 39.9702>>
//FLOAT fSpawnHeading = 25.29

#IF IS_DEBUG_BUILD
	USING "debug_channels_structs.sch"
#ENDIF

//VEHICLE_SETUP_STRUCT sVehicleSetup


/// PURPOSE:
///    Cleans all data associated with the air trafficking system.
PROC TRAFFICKING_AIR_CLEANUP(BOOL bDestroy = FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(CUBAN800)
	IF DOES_ENTITY_EXIST(vehPlane)
		IF bDestroy
			DELETE_VEHICLE(vehPlane)
			PRINTLN("AIR CLEANUP: DELETING PLANE")
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlane)
			PRINTLN("AIR CLEANUP: SETTING PLANE AS NO LONGER NEEDED")
		ENDIF
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_PlaneCreated)
		CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_PlaneCreated)
		PRINTLN("CLEARING BITMASK AS ENUM - TRAF_BOOL_PlaneCreated")
	ENDIF
	
	bPrintedInVehAir = FALSE
ENDPROC


PROC TRAFFICKING_AIR_UPDATE(BOOL & bIsAirReplay)

	// General blip update.
	BOOL bEventActive = FALSE
	IF (eAirState != TKS_LEAVE_WAIT)
		IF NOT (g_savedGlobals.sTraffickingData.bAirBlipActive)
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRAF_AIR, TRUE)
			g_savedGlobals.sTraffickingData.bAirBlipActive = TRUE
			bEventActive = TRUE
		ELSE
			bEventActive = TRUE
		ENDIF
	ELSE
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRAF_AIR, FALSE)
		g_savedGlobals.sTraffickingData.bAirBlipActive = FALSE
	ENDIF
	
	
	// See if we're being force reset.
	IF (eAirState != TKS_AREA_WAIT) AND (eAirState != TKS_WAIT_FOR_FAILSCREEN_REPLAY)
		IF TRAFFICKING_SHOULD_CLEAN_AND_WAIT() AND NOT bIsAirReplay
			TRAFFICKING_AIR_CLEANUP()
			eAirState = TKS_AREA_WAIT
		ENDIF
	ENDIF
	
	// General update.
	SWITCH (eAirState)
		CASE TKS_INIT
			eAirState = TKS_AREA_WAIT
		BREAK

		CASE TKS_AREA_WAIT
//			CPRINTLN(DEBUG_MINIGAME, "TKS_AREA_WAIT")
			
			// Make sure the player is within range of the actual vehicle. If he is, move to stream it.
			IF NOT TRAFFICKING_SHOULD_CLEAN_AND_WAIT()
				IF (fDist2ToLauncher < TRAF_UPDATE_DIST2)
					CPRINTLN(DEBUG_MINIGAME, "GOING TO STATE - TKS_STREAM")
					eAirState = TKS_STREAM
				ELSE
					IF bSwitchInProgress
						PRINTLN("WE'RE USING AIR SWITCH, GO AHEAD AND GO TO STATE - TKS_STREAM")
						eAirState = TKS_STREAM
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TKS_STREAM
//			CPRINTLN(DEBUG_MINIGAME, "TKS_STREAM")
			
			// Request all streams here. Once done, move on to waiting for them.
			REQUEST_MODEL(CUBAN800)
			
//			CPRINTLN(DEBUG_MINIGAME, "GOING TO STATE - TKS_STREAM_WAIT")
			eAirState = TKS_STREAM_WAIT
		BREAK
		
		CASE TKS_STREAM_WAIT
//			CPRINTLN(DEBUG_MINIGAME, "TKS_STREAM_WAIT")
			
			REQUEST_MODEL(CUBAN800)
			
			// When everything is done loading, move to waiting for the player to engage the launcher.
			IF HAS_MODEL_LOADED(CUBAN800)
				IF NOT DOES_ENTITY_EXIST(vehPlane)
					CLEAR_AREA_OF_VEHICLES(vPlaneSpawnPosition, 8.0)
					vehPlane = CREATE_VEHICLE(CUBAN800, vPlaneSpawnPosition, 25.29)
					SET_MODEL_AS_NO_LONGER_NEEDED(CUBAN800)
					
					SET_VEHICLE_COLOUR_COMBINATION(vehPlane,2)
					
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_PlaneCreated)
					PRINTLN("SETTING BITMASK AS ENUM - TRAF_BOOL_PlaneCreated")
				
					CPRINTLN(DEBUG_MINIGAME, "CREATING PLANE")
					CPRINTLN(DEBUG_MINIGAME, "AIR: TKS_STREAM_WAIT -> TKS_LAUNCH_WAIT")
					eAirState = TKS_LAUNCH_WAIT
				ELSE
					CPRINTLN(DEBUG_MINIGAME, "AIR: TKS_STREAM_WAIT -> PLAYER ALREADY EXISTS -> TKS_LAUNCH_WAIT 01")
					eAirState = TKS_LAUNCH_WAIT
				ENDIF
			ENDIF
		BREAK 
		
		CASE TKS_LAUNCH_WAIT
									
//			CPRINTLN(DEBUG_MINIGAME, "AIR: TKS_LAUNCH_WAIT")

			// Okay, in debug, there are some cases where we need to plant the player in the vehicle.
			//CPRINTLN(DEBUG_MINIGAME, "Checking debug.")
			#IF IS_DEBUG_BUILD
				IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_LaunchedAirViaDebug)
					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_LaunchedAirViaDebug)
					
					PRINTLN("RUNNING AIR DEBUG")
					
					IF NOT IS_ENTITY_DEAD(pedPlayer) AND NOT IS_ENTITY_DEAD(vehPlane)
                        SET_PED_INTO_VEHICLE(pedPlayer, vehPlane)
                        SET_GAMEPLAY_CAM_RELATIVE_HEADING()
                        SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	                ENDIF
	                  
	                bEventActive = TRUE

					PRINTLN("AIR: SETTING PLAYER INTO VEHICLE - THROUGH DEBUG LAUNCH")
				ENDIF
			#ENDIF
			
			// Make sure the vehicle is still alive...
			//CPRINTLN(DEBUG_MINIGAME, "Checking dead.")
			IF DOES_ENTITY_EXIST(vehPlane)
				IF IS_ENTITY_DEAD(vehPlane) OR IS_ENTITY_DEAD(PLAYER_PED_ID())
					CPRINTLN(DEBUG_MINIGAME, "AIR: Dead :: TKS_LAUNCH_WAIT -> TKS_LEAVE_WAIT")

					eAirState = TKS_LEAVE_WAIT
					EXIT
				ENDIF
			ENDIF
			
			/* Exit early from air launcher if we're running ground... to fix an issue when the player starts ground, gets out and 
			enters the Cuban 800, which was starting up air, causing issues. */
			IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
				IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Traffick_Ground")) = 1) // To Fix Bug # 1565809 - DS 
					IF NOT IS_ENTITY_DEAD(vehPlane)
						SET_VEHICLE_DOORS_LOCKED(vehPlane, VEHICLELOCK_LOCKED)
//						PRINTLN("SETTING DOORS TO BE LOCKED ON THE PLANE")
						EXIT
					ENDIF
				ENDIF
			ELSE
				IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Traffick_Ground")) = 0)
//					CPRINTLN(DEBUG_MINIGAME, "AIR CONTROLLER:  NO MISSION IS LAUNCHED AT THIS TIME")
					
					IF NOT IS_ENTITY_DEAD(vehPlane)
						SET_VEHICLE_DOORS_LOCKED(vehPlane, VEHICLELOCK_UNLOCKED)
//						PRINTLN("SETTING DOORS TO BE UNLOCKED ON THE PLANE")
					ENDIF
				ENDIF
			ENDIF
			
			// Make sure the player's still in the area, and we care.
			//CPRINTLN(DEBUG_MINIGAME, "Checking dist.")
			IF NOT bSwitchInProgress
				IF (fDist2ToLauncher >= TRAF_UPDATE_DIST2) AND NOT bIsAirReplay				
					TRAFFICKING_AIR_CLEANUP()
					CPRINTLN(DEBUG_MINIGAME, "Dead :: TKS_LAUNCH_WAIT -> TKS_AREA_WAIT")
					eAirState = TKS_AREA_WAIT
					EXIT
				ENDIF
			ENDIF
			
			// Are we even supposed to be active?
			//CPRINTLN(DEBUG_MINIGAME, "Checking activity.")
			IF NOT bEventActive AND NOT bIsAirReplay
				//CPRINTLN(DEBUG_MINIGAME, "We're an inactive event. Don't care about anything else.")
				IF NOT bPrintedInVehAir
					// If the player gets into the vehicle while we're not active... notify him.
					IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
						VEHICLE_INDEX tempVeh
						tempVeh = GET_VEHICLE_PED_IS_USING(pedPlayer)
						IF (tempVeh = vehPlane)
							//PRINT_HELP("MG_ATRAF_H_TOD")
//							ADD_HELP_TO_FLOW_QUEUE("MG_ATRAF_H_TOD", FHP_LOW)
							bPrintedInVehAir = TRUE
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlane)
							
							CPRINTLN(DEBUG_MINIGAME, "TKS_LAUNCH_WAIT -> TKS_LEAVE_WAIT")
							eAirState = TKS_LEAVE_WAIT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// If this is a replay... put the ped in the vehicle.
			//CPRINTLN(DEBUG_MINIGAME, "Checking replay.")
			IF bIsAirReplay
				CPRINTLN(DEBUG_MINIGAME, "Snapping into vehicle!")
				SET_PED_INTO_VEHICLE(pedPlayer, vehPlane)
				PRINTLN("AIR: SETTING PLAYER INTO VEHICLE - THROUGH REPLAY")
			ELSE
				//CPRINTLN(DEBUG_MINIGAME, "Definitely not a replay.")
			ENDIF
			
			// Wait for the player to enter the vehicle, and launch then.
			//CPRINTLN(DEBUG_MINIGAME, "Checking in vehicle..")
						
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(vehPlane)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlane, TRUE)
					DISABLE_SELECTOR_THIS_FRAME()
					PRINTLN("AIR: DISABLING SELECTOR")
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX tempVeh
					tempVeh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
					IF (tempVeh = vehPlane)
						REQUEST_SCRIPT("Traffick_Air")
						
						IF DOES_ENTITY_EXIST(vehToSave) AND NOT IS_ENTITY_DEAD(vehToSave)
							IF GET_ENTITY_MODEL(vehToSave) <> DUNE
							AND GET_ENTITY_MODEL(vehToSave) <> CUBAN800
								
								IF NOT IS_ENTITY_IN_ANGLED_AREA(vehToSave, <<2137.120361,4799.970215,39.678535>>, <<2116.639893,4790.416992,45.452946>>, 25.0)
									SET_MISSION_VEHICLE_GEN_VEHICLE(vehToSave, <<2142.2451, 4823.4629, 40.2769>>, 118.4428)
									PRINTLN("TRYING TO SAVE LAST VEHICLE")
								ENDIF
							ELSE
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
									SET_PLAYERS_LAST_VEHICLE(vehToSave)
									PRINTLN("LAST VEHICLE WAS THE DUNE OR CUBAN800 - NOT SAVING")
								ENDIF
							ENDIF
						ENDIF
						
						CPRINTLN(DEBUG_MINIGAME, "TKS_LAUNCH_WAIT -> TKS_LAUNCH")
						eAirState = TKS_LAUNCH
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TKS_LAUNCH
			//CPRINTLN(DEBUG_MINIGAME, "TKS_LAUNCH")
			
			// Once the script is loaded, we're free to launch.
			IF HAS_SCRIPT_LOADED("Traffick_Air")
				CPRINTLN(DEBUG_MINIGAME, "Script loaded... moving on.")
				
				m_enumMissionCandidateReturnValue eLaunchVal
				eLaunchVal = MCRET_DENIED
				
				// If we're a replay, just continue on. Short circuit all.
				IF bIsAirReplay
					CPRINTLN(DEBUG_MINIGAME, "Replay launch!!")
					eLaunchVal = MCRET_ACCEPTED
				ELSE
					eLaunchVal = Request_Mission_Launch(iMissionCandidateID, MCTID_CONTACT_POINT, MISSION_TYPE_MINIGAME)
					bRequestedLaunchedTrafficking = TRUE
					PRINTLN("AIR bRequestedLaunchedTrafficking = TRUE")
				ENDIF
				
				// Check acceptance.
				IF (eLaunchVal = MCRET_ACCEPTED)
					CPRINTLN(DEBUG_MINIGAME, "Launching Trafficking_Air.sc !")
						
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						CPRINTLN(DEBUG_MINIGAME, "TRAFFICKING AIR CONTROLLER - CLEARING WANTED LEVEL")
					ENDIF
					
					IF NOT bIsAirReplay
						Store_Minigame_Replay_Starting_Snapshot(GET_THIS_SCRIPT_NAME(), FALSE)
					ENDIF
					
					traffickingThread = START_NEW_SCRIPT("Traffick_Air", MISSION_STACK_SIZE)
					SET_RICH_PRESENCE_FOR_SP_MINIGAME(MINIGAME_TRAFFICKING_AIR)
                    SET_SCRIPT_AS_NO_LONGER_NEEDED("Traffick_Air")
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlane)
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					    // Sober the player up
					    IF g_isPlayerDrunk = TRUE
					        Make_Ped_Sober(PLAYER_PED_ID())
					    ENDIF
					ENDIF
					
					bPrintedInVehAir = FALSE
					
					CPRINTLN(DEBUG_MINIGAME, "TKS_LAUNCH_WAIT -> TKS_RUN_WAIT")
					eAirState = TKS_RUN_WAIT
					
				ELIF (eLaunchVal = MCRET_DENIED)
				
					bRequestedLaunchedTrafficking = FALSE
					PRINTLN("AIR bRequestedLaunchedTrafficking = FALSE")
				
					TRAFFICKING_AIR_CLEANUP()
					eAirState = TKS_LEAVE_WAIT
				ENDIF
			ENDIF
		BREAK
		
		CASE TKS_RUN_WAIT
			IF NOT IS_THREAD_ACTIVE(traffickingThread)
				// If we failed, we'll be doing a replay...
				IF IS_BITMASK_AS_ENUM_SET(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_AirTraffickingFailed)
					eAirState = TKS_WAIT_FOR_FAILSCREEN_REPLAY
				ELSE
					// passed 
					CPRINTLN(DEBUG_MINIGAME, "Clearing the mission candidate!!")
					Mission_Over(iMissionCandidateID)
					Reset_All_Replay_Variables()
					
					// Store next playable time of day.
					TIMEOFDAY todNext
					todNext = GET_CURRENT_TIMEOFDAY()
					ADD_TIME_TO_TIMEOFDAY(todNext, 0, 0, 3)
					g_savedGlobals.sTraffickingData.todNextPlayable_Air = todNext
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRAF_AIR, FALSE)
					g_savedGlobals.sTraffickingData.bAirBlipActive = FALSE
					
					PRINTLN("GOING TO STATE - TKS_LEAVE_WAIT")
					eAirState = TKS_LEAVE_WAIT
					
//					eAirState = TKS_AREA_WAIT
				ENDIF
				
				bIsAirReplay = FALSE
				CLEAR_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_AirTraffickingFailed)
			ENDIF
		BREAK
		
		CASE TKS_WAIT_FOR_FAILSCREEN_REPLAY
			//CPRINTLN(DEBUG_MINIGAME, "We're waiting on replay screen.")
			IF (g_replay.replayStageID = RS_ACTIVE)
				CPRINTLN(DEBUG_MINIGAME, "Got replay is active.")
				
				IF g_bLaunchMinigameReplay
					CPRINTLN(DEBUG_MINIGAME, "Flag set, replay starting. Replay script name: ", g_replay.replayScriptName)
					bIsAirReplay = TRUE
					
					// put player in suitable position for replay
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2136.4456, 4791.4829, 39.9702>>)
						ENDIF
					ENDIF
					
					REQUEST_MODEL(CUBAN800)
					REQUEST_SCRIPT("Traffick_Air")
					
					IF HAS_MODEL_LOADED(CUBAN800)
						CLEAR_AREA_OF_VEHICLES(vPlaneSpawnPosition, 8.0)
						vehPlane = CREATE_VEHICLE(CUBAN800, vPlaneSpawnPosition, 25.29)
						SET_MODEL_AS_NO_LONGER_NEEDED(CUBAN800)
						
						CPRINTLN(DEBUG_MINIGAME, "TKS_WAIT_FOR_FAILSCREEN_REPLAY -> CREATED PLANE NEW!!!")
						CPRINTLN(DEBUG_MINIGAME, "TKS_WAIT_FOR_FAILSCREEN_REPLAY -> TKS_STREAM_WAIT")
						eAirState = TKS_STREAM_WAIT
					ELSE
						CPRINTLN(DEBUG_MINIGAME, "TKS_WAIT_FOR_FAILSCREEN_REPLAY -> WAITNG FOR MODEL TO LOAD")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MINIGAME, "No minigame replay.. This is a BAD state!")
					bIsAirReplay = FALSE
					eAirState = TKS_AREA_WAIT
				ENDIF
				
			ELIF (g_replay.replayStageID = RS_NOT_REQUIRED)
				CPRINTLN(DEBUG_MINIGAME, "Replay refused...")
				CPRINTLN(DEBUG_MINIGAME, "Clearing the mission candidate!!")
				Mission_Over(iMissionCandidateID)
				Reset_All_Replay_Variables()
				bIsAirReplay = FALSE
				eAirState = TKS_AREA_WAIT
			ENDIF
		BREAK
		
		CASE TKS_LEAVE_WAIT
//			CPRINTLN(DEBUG_MINIGAME, "INSIDE STATE - TKS_LEAVE_WAIT")
			
			// Here' we're simply waiting for the player to leave the area before resetting.
			IF (fDist2ToLauncher >= TRAF_UPDATE_DIST2)
			AND IS_NOW_AFTER_TIMEOFDAY(g_savedGlobals.sTraffickingData.todNextPlayable_Air)
				TRAFFICKING_AIR_CLEANUP()
				CPRINTLN(DEBUG_MINIGAME, "GOING TO STATE: eAirState = TKS_AREA_WAIT")
				eAirState = TKS_AREA_WAIT
			ENDIF
		BREAK
		
		DEFAULT
			CPRINTLN(DEBUG_MINIGAME, "DEFAULT--Air: Bug this for David S.")
		BREAK
	ENDSWITCH
ENDPROC

