USING "globals.sch"
USING "types.sch"
USING "model_enums.sch"
USING "timer_public.sch"
USING "completionpercentage_public.sch"

ENUM FAIL_REASON_ENUM // reasons for failure
	FAIL_PLAYER_DIED = 0,
	FAIL_EARLY_TERMINATION,
	FAIL_BUYER_DEAD,
	FAIL_ABANDONED_CAR,
	FAIL_PLAYER_WANTED,
	FAIL_DISTANCE_FAIL,
	FAIL_LOST_BAD_GUY,
	FAIL_TRANSPORT_DESTROYED,
	FAIL_PLANE_DESTROYED,
	FAIL_VEHICLE_DISABLED,
	FAIL_TIME_EXPIRED,
	FAIL_TIME_EXPIRED_WANTED,
	FAIL_WANTED_AT_SITE,
	FAIL_TIME_ALTITUDE,
	FAIL_ABANDONED_JOB,
	FAIL_PACKAGE_DESTROYED, 
	FAIL_SMUGGLER_REACHED_DEST,
	FAIL_WEAPONS_DESTROYED,
	FAIL_NOT_ALL_TARGETS_DESTROYED
ENDENUM

ENUM RECORD_STATUS
	RECORD_STATUS_INIT = 0,
	RECORD_STATUS_PLAYING,
	RECORD_STATUS_END
ENDENUM

ENUM EDrugLocation
	EDrugLocation_BeerBar = 0,
	EDrugLocation_TrailerPark,
	EDrugLocation_TrailerPark_02,
	EDrugLocation_AutoService,
	EDrugLocation_CountryHouse,
	EDrugLocation_PlateauHouse,
	EDrugLocation_OceanExit,
	EDrugLocation_RiverCanyonPass,
	EDrugLocation_GraineryParkingLot,
	EDrugLocation_LargeFarm,
	EDrugLocation_SmallTrailer,
	EDrugLocation_LargeCountryHouse,
	EDrugLocation_SmallCountryHouse,
	EDrugLocation_WreckedAirfield,
	EDrugLocation_DirtCuldesac,
	EDrugLocation_WestLakeMiddle,
	EDrugLocation_WestBeach,
	EDrugLocation_NorthDock,
	EDrugLocation_SeaSide,
	EDrugLocation_SouthDock,
	EDrugLocation_BoatDepot,
	EDrugLocation_BoatDepot_Smugglers,
	EDrugLocation_BoatDepot_Smugglers01,
	EDrugLocation_CornField,
	EDrugLocation_MegaMall,
	EDrugLocation_MainAirport,
	EDrugLocation_MainAirportGround,
	EDrugLocation_LiquorJrMarket,
	EDrugLocation_JackInn,
	EDrugLocation_TempDiner,
	EDrugLocation_RecycleCenter,
	EDrugLocation_TrailerParkOnHill,
	EDrugLocation_SmugglersDropoff_01,
	EDrugLocation_TrailersAndBoat,
	EDrugLocation_RightRoadSide,
	EDrugLocation_OldLiquorStore,
	EDrugLocation_YellowJackInn, 
	EDrugLocation_DeadZone_01,
	EDrugLocation_DeadZone_02,
	EDrugLocation_DeadZone_Air,
	EDrugLocation_RailHouse,
	EDrugLocation_OldHouse,
	EDrugLocation_OldHouseSmugglers,
	EDrugLocation_WaterMiddle,
	EDrugLocation_WaterFar,
	EDrugLocation_WaterRiver,
	EDrugLocation_MilitaryBase01,
	EDrugLocation_MilitaryBase02,
	EDrugLocation_WindField,
	EDrugLocation_SkethcyHouse,
	EDrugLocation_LightHouse,
	EDrugLocation_DirtPit, 
	EDrugLocation_LittleHoleHouse,
	EDrugLocation_FarEastDock01,
	EDrugLocation_FarEastDock02,
	EDrugLocation_NorthBeach,
	EDrugLocation_NewBeach,
	EDrugLocation_MountainCommune,
	EDrugLocation_MountainCuldesac,
	EDrugLocation_HillBarn,
	EDrugLocation_LittleHouseOnTheHill,
	EDrugLocation_NEW_TYPE1_01,
	EDrugLocation_NEW_TYPE1_02,
	EDrugLocation_NEW_TYPE1_03,
	EDrugLocation_NEW_TYPE1_04,
	EDrugLocation_NEW_TYPE1_05,
	EDrugLocation_NEW_TYPE1_06,
	EDrugLocation_NEW_TYPE1_07,
	EDrugLocation_NEW_TYPE1_08,
	EDrugLocation_NEW_TYPE1_09,
	EDrugLocation_NEW_TYPE2_01,
	EDrugLocation_NEW_TYPE2_02,
	EDrugLocation_NEW_TYPE2_03,
	EDrugLocation_FarmOnHill,
	EDrugLocation_WaterTreatmentPlant,
	EDrugLocation_BeachHead,
	EDrugLocation_Clearing
ENDENUM

ENUM EWaterLocation
	EWaterLocation_West = 0
ENDENUM

ENUM ELocation	
	ELocation_GroundLocation, 
	ELocation_AirLocation,
	ELocation_Water
ENDENUM

ENUM TUTORIAL_STATE
	TUTORIAL_STATE_PENDING = 0,
	TUTORIAL_STATE_PRE,
	TUTORIAL_STATE_START,
	TUTORIAL_STATE_A,
	TUTORIAL_STATE_CUT,
	TUTORIAL_STATE_B,
	TUTORIAL_STATE_BB,
	TUTORIAL_STATE_C,
	TUTORIAL_STATE_D,
	TUTORIAL_STATE_E,
	TUTORIAL_STATE_F,
	TUTORIAL_STATE_REPEAT,
	TUTORIAL_STATE_LANDING_GEAR,
	TUTORIAL_STATE_LANDING_WAIT,
	TUTORIAL_STATE_END
ENDENUM

ENUM EVariation
	EVariation_OffRoad=0,
	EVariation_Water,
	EVariation_Dynamic,
	EVariation_Air
ENDENUM

ENUM SMUGGLERS_STATE 
	SMUGGLERS_STATE_TO_PICKUP = 0,
	SMUGGLERS_STATE_PICKUP_CHECK,
	SMUGGLERS_STATE_PICKUP_COMPLETE,
	SMUGGLERS_STATE_PICKUP_CUTSCENE,
	SMUGGLERS_STATE_TO_DROPOFF,
	SMUGGLERS_STATE_CHECK_FOR_IN_VEHICLE,
	SMUGGLERS_STATE_DROPOFF_FIGHT,
	SMUGGLERS_STATE_DROPOFF_FIGHT_A,
	SMUGGLERS_STATE_DROPOFF_FIGHT_CUTSCENE,
	SMUGGLERS_STATE_DROPOFF_DELIVERY,
	SMUGGLERS_STATE_COMPLETE
ENDENUM

ENUM SMUGGLERS_RETRIEVE_DRUGS
	SMUGGLERS_RETRIEVE_DRUGS_CHECK_PEDS_DEAD = 0,
	SMUGGLERS_RETRIEVE_DRUGS_GOTO_VEHICLE,
	SMUGGLERS_RETRIEVE_DRUGS_AT_VEHICLE,
	SMUGGLERS_RETRIEVE_DRUGS_GET_IN_CAR,
	SMUGGLERS_RETRIEVE_DRUGS_CUTSCENE,
	SMUGGLERS_RETRIEVE_DRUGS_COMPLETE
ENDENUM

ENUM SMUGGLERS_CREATE_HELI_VEHICLE
	SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_01 = 0,
	SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_02,
	SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_03,
	SMUGGLERS_CREATE_HELI_VEHICLE_STAGE_04
ENDENUM

ENUM CHASE_STATE
	CHASE_STATE_WAIT = 0,
	CHASE_STATE_CREATE,
	CHASE_STATE_CHASE,
	CHASE_STATE_OVER
ENDENUM

ENUM DROP_STATE
	DROP_STATE_HELP = 0,
	DROP_STATE_CONVERSATION,
	DROP_STATE_CHECK_DROPS,
	DROP_STATE_FINAL
ENDENUM

ENUM DROP_STATE_CHECKS
	DROP_STATE_CHECKS_INITIALIZE = 0,
	DROP_STATE_CHECKS_PRE_RUNNING,
	DROP_STATE_TUTORIAL,
	DROP_STATE_CHECKS_RUNNING,
	DROP_STATE_CHECKS_FINAL
ENDENUM

ENUM RECORDING_TYPE
	RECORDING_TYPE_CAR = 0,
	RECORDING_TYPE_PLANE,
	RECORDING_TYPE_BOAT,
	RECORDING_TYPE_PED
ENDENUM

ENUM AIR_VARIATION
	AIR_VARIATION_SIMPLE = 0,
	AIR_VARIATION_DROP,
	AIR_VARIATION_BOMB,
	AIR_VARIATION_LOW_ALT,
	AIR_VARIATION_WATER
ENDENUM

ENUM DIFFICULTY
	DIFFICULTY_EASY = 0,
	DIFFICULTY_MED,
	DIFFICULTY_HARD
ENDENUM

ENUM CLUSTER_STATE
	CLUSTER_STATE_INIT = 0,
	CLUSTER_STATE_FIRST,
	CLUSTER_STATE_DROP,
	CLUSTER_STATE_DROPB,
	CLUSTER_STATE_CAM,
	CLUSTER_STATE_WAIT,
	CLUSTER_STATE_LAST_CUT,
	CLUSTER_STATE_DONE
ENDENUM

CONST_FLOAT LOW_ALTITUDE_VAL 65.0

CONST_INT REWARD_HIGH 415
CONST_INT REWARD_MEDIUM 300
CONST_INT REWARD_LOW 150
CONST_INT BONUS_MEDIUM 125

CONST_INT TIME_BONUS_HIGH 150
CONST_INT TIME_BONUS_MEDIUM 100

CONST_INT HEIGHT_BONUS_HIGH 25
CONST_INT HEIGHT_BONUS_MEDIUM 45

CONST_INT MAX_NUMBER_FINAL_TARGETS 9

CONST_INT maxNumBadGuys 8
CONST_INT MAX_NUMBER_CLUSTER 9
CONST_INT MAX_NUMBER_DROPS 4 //6
CONST_INT MAX_EXTRA_CARS 3
CONST_INT MAX_NUMBER_PACKAGES 6
CONST_INT MAX_NUM_RECORDS 5
CONST_INT MAX_NUM_AMBUSH_CARS 4
CONST_INT MAX_NUMBER_AMBUSH_ATTACKERS 2
CONST_INT MAX_NUMBER_CAR_CHASERS 8
CONST_INT MAX_NUMBER_STATIC_CARS 2
CONST_INT MAX_NUMBER_STATIC_PEDS 4
CONST_INT MAX_NUM_DROP_PLANES 2
CONST_INT fDelayTime 10
CONST_INT MAX_NUMBER_WAVES 3
CONST_INT MAX_NUM_SMUGGLERS_CARS 1
CONST_INT MAX_NUM_SMUGGLERS_RECORDINGS 2
CONST_INT MAX_NUM_SMUGGLERS_PEDS 2
CONST_INT MAX_NUM_STATIC_POSITIONS 4
CONST_INT MAX_NUMBER_ESCORT_VEHICLES 4
CONST_INT MAX_NUMBER_ESCORT_PEDS 4
CONST_INT MAX_NUMBER_EXPLOSIVES 4
CONST_INT MAX_NUMBER_STAGED_CHASE_PEDS 2
CONST_INT MAX_NUMBER_STAGED_CHASE_VEHICLES 1
CONST_INT MAX_NUMBER_STAGED_PROPS 3

// For Random Smugglers
CONST_INT MAX_NUMBER_RANDOM_TYPE_1_PAIRINGS 7
CONST_INT MAX_NUMBER_RANDOM_TYPE_2_PAIRINGS 8

CONST_FLOAT HEALTH_TEXT_X 0.425
CONST_FLOAT HEALTH_TEXT_Y 0.032

CONST_FLOAT GATEWAY_SIZE 12.0
CONST_FLOAT GATEWAY_SIZEZ 5.0

CONST_FLOAT GATEWAY_SIZEWATERZ 25.0

CONST_FLOAT HEALTH_BAR_X 0.383
CONST_FLOAT HEALTH_BAR_Y 0.067
CONST_FLOAT HEALTH_BAR_W 0.204
CONST_FLOAT HEALTH_BAR_H 0.032

STRUCT CAR_RECORD_DATA
	RECORDING_TYPE recordingType
	MODEL_NAMES carrecVehicle
	BOOL bConfigured
	INT carrecNum 
	STRING carrecName
	VECTOR carCoords
	FLOAT x,y,j,k //quaternion values
	FLOAT fDropTime
ENDSTRUCT

STRUCT SMUGGLERS_RECORDING_DATA
	RECORDING_TYPE recordingType
	MODEL_NAMES carrecVehicle
	INT carrecNum 
	STRING carrecName
	STRING carrecWaypointName
	VECTOR carCoords
	VECTOR vTriggerDistance
	FLOAT x,y,j,k //quaternion values
	FLOAT fDefaultPlaybackSpeed
	FLOAT fMultiplier
	FLOAT fDropTime
	BOOL bConfigured
	BOOL bWaypointConfigured
ENDSTRUCT

STRUCT DRUG_LOCATION_DATA
	//Update Ambush only checks the 0 index to be a car type to decide if we're doing a ambush w/ it
	CAR_RECORD_DATA carRecData[MAX_NUM_RECORDS]
	
	// Smugglers vehicle recordings
	SMUGGLERS_RECORDING_DATA smugglersRecordings[MAX_NUM_SMUGGLERS_RECORDINGS]
	
	//BOOL bConfigured
	
	VECTOR vCenterPos
	VECTOR vDrugPickup
	
	TEXT_LABEL_63 sObjective
	TEXT_LABEL_63 sConvo
	
	GANG_TYPE gangType

	// Cutscene cameras
	VECTOR vCutscenePos1
	VECTOR vCutsceneOrien1
	VECTOR vCutscenePos2
	VECTOR vCutsceneOrien2

	//Smugglers Mode 
	VECTOR vSmugVehPos[MAX_NUM_SMUGGLERS_CARS]
	FLOAT fSmugVehRot[MAX_NUM_SMUGGLERS_CARS]
	VECTOR vSmugHeliPos
	FLOAT fSmugHeliRot
	VECTOR vPlaneRecTriggerPos
	VECTOR vCarPlacementShootEnd
	FLOAT fCarPlacementShootEnd
	
	FLOAT fTriggerPlaneDistance
	
	// Ambush Attackers
	VECTOR vAmbushAttackerPositions[MAX_NUMBER_AMBUSH_ATTACKERS]
	VECTOR vAmbushVehicleAttackerPositions[2]
	
	// Check for staying on main roads
	FLOAT fTimeAllowedToStayOnRoads
	
	//Hijack
	VECTOR vTruckPos
	FLOAT fTruckRot
	VECTOR vDestination[7]

ENDSTRUCT


STRUCT PLANE_DATA
	
	//AIR_VARIATION myVariation
	VECTOR vPlayerStart
	FLOAT fPlayerRot
	//INT iMoneyToPass
	
	FLOAT fmaxHeight
	FLOAT fAltTimeLimit
	
	//Data needed for timer
	FLOAT fPickupTime
			
	VEHICLE_INDEX myPlane
	MODEL_NAMES sPlaneName
	VECTOR vPlanePos
	FLOAT fPlaneAngle
	//VECTOR vDestination
	
	//VECTOR vCutscenePosStart
	//VECTOR vCutsceneOrienStart

	//VECTOR vCargoOffsetPos[MAX_NUMBER_DROPS]
	
	//This vector is for the final only.. re-used for trains!
	VECTOR vRunwayTargets[MAX_NUMBER_FINAL_TARGETS]
	FLOAT fRunwayTargets[MAX_NUMBER_FINAL_TARGETS]
	
	DRUG_LOCATION_DATA dropData[MAX_NUMBER_DROPS]
	DRUG_LOCATION_DATA endData
	
	//bomb data
	//VECTOR vCarSpawnPos[MAX_NUMBER_DROPS]
	MODEL_NAMES trainNames[MAX_NUMBER_DROPS]
	MODEL_NAMES boatNames[2]
	MODEL_NAMES targetCarNames[MAX_NUMBER_DROPS]
	MODEL_NAMES stationaryCarNames[MAX_NUMBER_DROPS]
	MODEL_NAMES targetCarDriverNames[MAX_NUMBER_DROPS]
	
	// Cargo Drop Destinations
	INT iNumDrops
	INT iNumTotalCargo
	
	//replaced by DRUG_LOCATION_DATA
	//VECTOR vCargoDrops[MAX_NUMBER_DROPS]
	
ENDSTRUCT

STRUCT DEBUG_POS_DATA
	BOOL bTurnOnDebugPos
	BOOL bTurnOnCameraPos
	VECTOR vDebugVector
	FLOAT fDebugFloat
	FLOAT fDebugX
	FLOAT fDebugY
	FLOAT fDebugZ
	FLOAT fdebugWidth
	FLOAT fdebugHeight
	
	FLOAT fDebugX2
	FLOAT fDebugY2
	FLOAT fdebugWidth2
	FLOAT fdebugHeight2
	
	FLOAT fRotateX
	FLOAT fRotateY
	FLOAT fRotateZ
	FLOAT fOffsetX
	FLOAT fOffsetZ
	FLOAT fFOV
	ENTITY_INDEX myObject
	VECTOR myObjectPos
ENDSTRUCT


STRUCT ARGS
	enumCompletionPercentageEntries completionEntry = -1
	BOOL bSpawnVehicle //bool to tell the ssytem we're not in the launcher and need to manually spawn the vehicle
	//BOOL bDebugLaunch
	ELocation myLocation
	//EVariation myVariation
	INT difficultyLevel
	INT numDropLocations
	//INT numEndLocations
	INT numCargoBombs
	INT iNumStaticCars
	INT iNumStaticPedsPerCar
	EDrugLocation dropLocations[MAX_NUMBER_DROPS]
	EDrugLocation endLocation[2]
	VECTOR genVectors[MAX_NUMBER_DROPS]
	BOOL bGenBools[MAX_NUMBER_DROPS]
	BOOL bDoStationary //Variables default to false so need both..
	BOOL bDoMobile
	BOOL bDoLockOn
	BOOL bDoTrain
	BOOL bDoTimed
	BOOL bDoChase
	BOOL bDoPoliceChase
	BOOL bDoCutscene
	BOOL bDoFragile
	BOOL bDoAmbush
	BOOL bDoPlane
	BOOL bDoBomb
	BOOL bDoLowAlt
	BOOL bDoSmugglers
	BOOL bDoFlareCutscene
	BOOL bDoContactCutscene
	BOOL bDoHijack
	BOOL bDoTutorial
	BOOL bDoUberBombs
	VEHICLE_INDEX trafVehicle
	INT iMoneyToPass
	FLOAT fTimeLimit
	FLOAT fSTime
	FLOAT fTNowTime
	FLOAT fBonusTime
	STRING sConversation
	GANG_TYPE gangType
	//This vector is for the final only..
	MODEL_NAMES mnFinalTargets[MAX_NUMBER_FINAL_TARGETS]
	MODEL_NAMES mnTargetCars[MAX_NUMBER_DROPS]
	MODEL_NAMES mnPedChasers[MAX_NUMBER_CAR_CHASERS]
	MODEL_NAMES mnCarChasers[MAX_NUMBER_CAR_CHASERS/2]
	ENTITY_INDEX miscObjects[6]
ENDSTRUCT

STRUCT DRUG_CARGO_ARGS
	OBJECT_INDEX oLoadedCargo
	OBJECT_INDEX oCutsceneCargo[6]
	VECTOR vLoadedCargoOffset 
	VECTOR vLoadedCargoRot
ENDSTRUCT

STRUCT EXPLOSIVE_PACKAGE_ARGS
	OBJECT_INDEX oExpPackage[MAX_NUMBER_EXPLOSIVES]
	OBJECT_INDEX oRamp
	OBJECT_INDEX oRampDoor
//	VECTOR vExpPackageOffset 
	VECTOR vExpPackageRot
	VECTOR vCanisterPos[MAX_NUMBER_EXPLOSIVES]
	structTimer tExplosivePackTimer
	INT iNumExplosivePackageDropped
	BOOL bExplodedPackage[MAX_NUMBER_EXPLOSIVES]
ENDSTRUCT

STRUCT CAR_AMBUSH_ARGS
	VEHICLE_INDEX ambushVehicles[MAX_NUM_AMBUSH_CARS]
	VEHICLE_INDEX vehAmbushAttackers[2]
	BOOL bTaskedFootAttacker[MAX_NUMBER_AMBUSH_ATTACKERS]
	BOOL bCreatedVehicleAttacker[MAX_NUM_AMBUSH_CARS]
	PED_INDEX pedAmbushAttackers[MAX_NUMBER_AMBUSH_ATTACKERS]
	BOOL bCreatedFootAttacker[MAX_NUMBER_AMBUSH_ATTACKERS]
	REL_GROUP_HASH relAmbushEnemies
	structTimer tAmbushTimer
	structTimer tAmbushCreationTimer
	BOOL bPrintAmbushObjective
	BOOL bPlayerLeavingAreaInCar
	BOOL bReTasked
	BOOL bReTaskedSpecialCase
	BOOL bLeftAreaOnce
	BOOL bPlayerSingleLine
	BOOL bCreateAmbush
	BOOL bCreateGuysOnFoot
ENDSTRUCT

STRUCT STATIC_CHASE_ARGS
//	VEHICLE_INDEX vehStaticAmbush[MAX_NUMBER_STATIC_CARS]
//	PED_INDEX pedStaticAmbush[MAX_NUMBER_STATIC_PEDS]
//	PED_INDEX pedStaticAmbushOutside[MAX_NUMBER_STATIC_PEDS]
	REL_GROUP_HASH relStaticPoliceChasers
//	BOOL bTimerStarted
	BOOL bStaticChaseDone
ENDSTRUCT

STRUCT SMUGGLERS_MODE_ARGS
	VEHICLE_INDEX smugglersVehicles[MAX_NUM_SMUGGLERS_CARS]
	VEHICLE_INDEX smugglersHeliVeh
	VEHICLE_INDEX vehSmugglerCarWithDrugs
	VEHICLE_INDEX vehSmugglerPlane
	OBJECT_INDEX oArmsPackage
	OBJECT_INDEX oParachute
	PED_INDEX smugglersPeds[MAX_NUM_SMUGGLERS_PEDS]
	PED_INDEX smugglersHeliPed
	PED_INDEX pedSmugglerWithDrugs
//	PED_INDEX pedSmugglerPassenger
	BLIP_INDEX blipSmugglersCars[MAX_NUM_SMUGGLERS_CARS]
	BLIP_INDEX blipSmugglersPeds[MAX_NUM_SMUGGLERS_PEDS]
	BLIP_INDEX blipSmugglersRetrieve
	BLIP_INDEX blipHeliPed
	BLIP_INDEX blipPackage
	BOOL bCarHasDrugs[MAX_NUM_SMUGGLERS_CARS]
	BOOL bDestroyedVehicleWithDrugs
	BOOL bPickupAIFirst
	BOOL bPickupPlayerFirst
	BOOL bPickupTie
	BOOL bGoToDestination
	BOOL bSmugglerReachedDest
	BOOL bSmugglerPedsDead
	BOOL bSmugglerPedsDeadPriorToPickup
	BOOL bPrintHelpRetrieve
	BOOL bCloseToDestination
	BOOL TooCloseToIncreaseSpeed
	BOOL bRunSpeedAdjustment
	BOOL bSmugglersTooFarAfterPickup
	BOOL bHeliPedDead
	BOOL bNeedToPrintObj
	BOOL bPrintObj
	BOOL bNeedToPointAtSmugglerVehicle
	BOOL bShootout
	BOOL bUnfreezeProp
	BOOL bAbandonedPackage
	BOOL bPackageLanded
	BOOL bTaskedToCombat[2]
	REL_GROUP_HASH relSmugglersEnemies
	VECTOR vDefeatedSmugglerVehCoor
	FLOAT fDefeatedSmugglerVehHeading
	VECTOR vDefeatedSmugglerVehRotation
	VECTOR vSafeCarPos
	VECTOR vVehicleNodePos
	FLOAT fVehicleNodeHeading
	FLOAT fVehicleNodeHeading01
	structTimer tTrapStartTimer
	structTimer tParachuteTimer
	structTimer tPrintObjTimer
	structTimer tBufferTimer
ENDSTRUCT

// For car recordings
STRUCT PLANE_DROP_ARGS
	VEHICLE_INDEX planeDropVehicles[MAX_NUM_RECORDS]
	VEHICLE_INDEX planeVehicle
	OBJECT_INDEX oPackage
	OBJECT_INDEX oParachute
	OBJECT_INDEX oLandedPackage
	BLIP_INDEX blipPackage
	structTimer tParachuteTimer
	BOOL bCargoHasLanded
	BOOL bPlaneActive
	BOOL bUnfreezeProp
ENDSTRUCT

STRUCT CAR_CHASER_ARGS
	// Car Chasers
	VEHICLE_INDEX vehCarChasers[MAX_NUMBER_CAR_CHASERS] //Vehicles 
	PED_INDEX pedCarChasers[MAX_NUMBER_CAR_CHASERS]		//Guys in cars 
	BLIP_INDEX blipCarChasers[MAX_NUMBER_CAR_CHASERS]	//Blips for cars 
	BLIP_INDEX badGuyBlips[MAX_NUMBER_CAR_CHASERS]		//Blips for guys in cars
	REL_GROUP_HASH relCarChaserEnemies
//	CHASE_STATE chaseSate
	structTimer tChaseActiveTimer
	structTimer tCarChaserStuckTimer
	BOOL bVehicleStuck
	FLOAT fStuckTime
ENDSTRUCT	

STRUCT HIJACK_ARGS
	// Truck/Trailer 
	VEHICLE_INDEX vehTruck
	VEHICLE_INDEX vehTrailer
	OBJECT_INDEX oWeaponsCargo
	OBJECT_INDEX oWeaponsCargoPreAnim
	OBJECT_INDEX oFiller02
	PED_INDEX pedTruckDriver
	BLIP_INDEX blipTruck
	BLIP_INDEX blipTrailerBack
	BLIP_INDEX blipWeapons
	// Escort vehicle
	VEHICLE_INDEX vehEscort[MAX_NUMBER_ESCORT_VEHICLES]
	VECTOR vMoveEscortPos[MAX_NUMBER_ESCORT_VEHICLES]
	BOOL bMovedBike[MAX_NUMBER_ESCORT_VEHICLES]
	BLIP_INDEX blipEscortCar[MAX_NUMBER_ESCORT_VEHICLES]
	// Escort peds
	PED_INDEX pedEscort[MAX_NUMBER_ESCORT_PEDS]
	BLIP_INDEX blipEscortPed[MAX_NUMBER_ESCORT_PEDS]
	// Kill cam
	CAMERA_INDEX camKillShot
	BOOL bTransportReachedDest
	BOOL bHijackActive
	FLOAT fPlayerVehicleSpeed
	structTimer tExplosiveCamTimer
	// To test for aggro check
	FLOAT fExplosiveDistance
	FLOAT fExplosiveAggroDistance
	FLOAT fTrailerHeading
	BOOL bStopRunningAggroCheck
	BOOL bDroppedOnePackage
	BOOL bGrabbedTime
	BOOL bRetaskedToWander[MAX_NUMBER_ESCORT_PEDS]
	BOOL bTruckDead
	BOOL bDoorsOpen
	BOOL bKilledByHeadExploding
	BOOL bOkayToUseOffset
	BOOL bTaskedToDefend
	BOOL bTaskedToCombat[MAX_NUMBER_ESCORT_PEDS]
	BOOL bFailSafeRemoved[MAX_NUMBER_ESCORT_PEDS]
	BOOL bOrientationIsCorrect
	BOOL bEscortsDone
	BOOL bOkayToRunCanisterDamageTest
	BOOL bTruckInPosition
	INT iTooCloseTime
	REL_GROUP_HASH relHijackGuys
	// Dynamic cutscene
	VECTOR vCamPos01
	VECTOR vVehicleNode
	FLOAT fVehicleNodeHeading
	VECTOR vDefensivePos
	// Chemical effects
	PTFX_ID PTFXChem01
	PTFX_ID PTFXChem02
	PTFX_ID PTFXChem03
	structTimer tKillCamTimer
	INT iSpeedNode
ENDSTRUCT

STRUCT TRAP_ARGS
	FLOAT fTriggerTrapTime = 20.0
	VECTOR vTrapPos
	VECTOR vNorthSide
	VECTOR vSouthSide
	BOOL bPrintTrapObjective = FALSE
	BOOL bNorthSide
	BOOL bSouthSide
	BOOL bGotSide
	BOOL bRunningForSmugglers
	BOOL bReset
	BLIP_INDEX blipTrap
	CAMERA_INDEX camTransition
	CAMERA_INDEX camTrap
	OBJECT_INDEX oTrapProp
	PED_INDEX pedOscar
	MODEL_NAMES mnOscar = A_M_M_HILLBILLY_01
	MODEL_NAMES mnTrapProp = PROP_LD_BOMB
	structTimer tTrapTimer
ENDSTRUCT

//STRUCT FINAL_TARGETS
//	VEHICLE_INDEX vehicles[MAX_NUMBER_FINAL_TARGETS]
//ENDSTRUCT

STRUCT PLANE_CARGO_ARGS
	DEBUG_POS_DATA debugData
	VEHICLE_INDEX myPlane
//	SCALEFORM_INDEX sf_hud
	PED_INDEX carDrivers[MAX_NUMBER_DROPS]
	OBJECT_INDEX oCargoDrop[MAX_NUMBER_PACKAGES]
	OBJECT_INDEX oClusterDrop[MAX_NUMBER_PACKAGES][MAX_NUMBER_CLUSTER]
	INT iDropTime[MAX_NUMBER_PACKAGES][MAX_NUMBER_CLUSTER]
	OBJECT_INDEX oStagingProps[MAX_NUMBER_STAGED_PROPS]
	OBJECT_INDEX oBombDummy
	BLIP_INDEX blipCrateDrop[MAX_NUMBER_PACKAGES]
	INT cargoTimes[MAX_NUMBER_PACKAGES]
	
//	BOOL bCargoDrop = TRUE

	//Data needed for chute hack
	OBJECT_INDEX oCargoChute[MAX_NUMBER_PACKAGES]
	INT			 iCargoChuteTime[MAX_NUMBER_PACKAGES]
	BOOL		 bCargoChuteVisible[MAX_NUMBER_PACKAGES]
	
	BOOL bBombCam = FALSE
	INT iNumPackageDropped = 0
	INT iNumPackagesTotal
	structTimer tCargoDropTimer
	structTimer tHelpPrintTimer
	structTimer tTotalTime
	structTimer tBombBayTimer
	BOOL bCargoHitGround[MAX_NUMBER_PACKAGES]
	BOOL bDetached[MAX_NUMBER_PACKAGES]
	DROP_STATE cargoState
//	INT iClosest
	INT iNumSuccessfulDrops = 0
	//INT iNumPerfectDrops = 0
	//INT iNumFailedDrops = 0
	INT iNumLanded = 0
	INT iNumReleased = 0
	INT iCutGameTime = 0
	INT iCutGameTimeStart = 0
	INT soundID

	//TODO make this a bit field reduce the amount of arrays.. no need for so many
	BOOL bCargoDropComplete[MAX_NUMBER_PACKAGES]
	BOOL bDropSuccessful[MAX_NUMBER_PACKAGES]
	BOOL bCargoSuccessful[MAX_NUMBER_PACKAGES]
//	BOOL bDropPerfect[MAX_NUMBER_PACKAGES]
//	BOOL bCargoPerfect[MAX_NUMBER_PACKAGES]
//	BOOL bDropFailed[MAX_NUMBER_PACKAGES]
	BOOL bCargoFailed[MAX_NUMBER_PACKAGES]
	BOOL bCargoComplete[MAX_NUMBER_PACKAGES]
	BOOL bDetectedOnRadar
	BOOL bDrugPurchaserDead[MAX_NUMBER_PACKAGES]
	BOOL bOkayToAddBackInObjBlip[MAX_NUMBER_PACKAGES]
//	BOOL bWithinPrintRange[MAX_NUMBER_PACKAGES]
	
	BOOL bCloseRange[MAX_NUMBER_PACKAGES]
	
	BOOL bInstructionsPrinted = FALSE
	BOOL bCompletedOneDrop = FALSE
	BOOL bDroppedOne = FALSE
	BOOL bDoTimed
	BOOL bPassed
	BOOL bButtonPressed = FALSE
//	BOOL bWithinRange
//	INT iSuccessBonus
//	INT iPerfectBonus
//	INT iFailedBonus
	INT iTimeBonus
	INT iHeightBonus
	INT iDistanceBonus
	INT iAllDropBonus
//	INT iTimeColorR
//	INT iTimeColorG
//	INT iTimeColorB
	FLOAT fActualMaxHeight
//	STRING sTimeBonus
	//STRING sHeightBonus
	CAMERA_INDEX CamExplosion
	CAMERA_INDEX CamTransition
	CAMERA_INDEX CamTransition01
	
	// Staging for dropoffs
	PED_INDEX pedDropoff[MAX_NUMBER_DROPS]
	VEHICLE_INDEX vehDropoff[MAX_NUMBER_DROPS]
	BOOL bVehDropoffFrozen[MAX_NUMBER_DROPS]
	
	//vehicle recordings for final mode
	VEHICLE_INDEX vehFinalExtra[6]
	BOOL bVehExtraFrozen[6]
	
	RECORD_STATUS recordingStatus[6]
	//vehicles for the final mode
	VEHICLE_INDEX vehFinal[MAX_NUMBER_FINAL_TARGETS]
	BLIP_INDEX blipFinal[MAX_NUMBER_FINAL_TARGETS]
	//OBJECT_INDEX oDropoff[MAX_NUMBER_DROPS]
	PTFX_ID PTFXflare[MAX_NUMBER_DROPS]
	REL_GROUP_HASH relRivalRunners
	
	STREAMVOL_ID streamVolID 
	BOOL bTraf4_FreezeBoats
ENDSTRUCT

//STRUCT STAGED_CAR_CHASE_ARGS
//	PED_INDEX pedStagedCarChase[MAX_NUMBER_STAGED_CHASE_PEDS]
//	VEHICLE_INDEX vehStagedCarChase[MAX_NUMBER_STAGED_CHASE_VEHICLES]
//	BLIP_INDEX blipStagedCarChasePeds[MAX_NUMBER_STAGED_CHASE_PEDS]
//	REL_GROUP_HASH relStagedCarChaserEnemies
//	BOOL bStagedCarChaseCreated 
//	BOOL bStagedCarChaseActive
////	BOOL bStagedCarChaseComplete
//ENDSTRUCT

STRUCT LOCATION_DATA

	//CAR_RECORD_DATA carRecData[MAX_NUM_RECORDS]
	DRUG_LOCATION_DATA dropData[1]
	
	//go back to 2?
	DRUG_LOCATION_DATA endData[MAX_NUMBER_DROPS]
	
//	DIFFICULTY difficultySetting
	
	// in use
//	INT locationIDX
//	BOOL bAcquireVehicle
//
//Support Info
//
	BOOL bSupportsPlane
	BOOL bSupportsChase
	BOOL bSupportsAmbush

	VECTOR vPlayerStart
	FLOAT fPlayerRot
	INT iNumBadGuys
	INT iNumBadGuysPerCar
//
//Known vehicle location Data
//
	VECTOR vKnownVehicleLoc
	FLOAT vKnownVehicleHead
	MODEL_NAMES vehicleName
	MODEL_NAMES mnGenericBadGuy
	
//Timed info
	FLOAT fPickupTime
	FLOAT fSlowTime
	FLOAT fTriggerNowTime
//	INT iNumDrops
// Car chasers info
	INT iNumWaves
	INT iNumCarChaserCars
	INT iNumCarChaserCarsPerWave
	INT iNumPedsPerCar
//	FLOAT fTimeToDelay
	MODEL_NAMES mnCarChasers[maxNumBadGuys]
	MODEL_NAMES mnPedChasers[maxNumBadGuys]
	VEHICLE_SEAT PedSeat[8]
	
//Smugglers Mode
	INT iNumSmugVehicles
	INT iNumSmugPedsPerVehicle
	MODEL_NAMES mnSmugglersVehicles[MAX_NUM_SMUGGLERS_CARS]
//	MODEL_NAMES mnSmugglersPeds[MAX_NUM_SMUGGLERS_PEDS]
	MODEL_NAMES mnSmugglersHeli
		
//Dynamic specific values
	
// Hijack Mode
//	MODEL_NAMES genericDriver
	MODEL_NAMES genericVehicle
	MODEL_NAMES mnEscortVehicle[MAX_NUMBER_ESCORT_VEHICLES]
//	MODEL_NAMES mnEscortPeds[MAX_NUMBER_ESCORT_PEDS]
//	INT iNumEscortCars
//	INT iNumEscortPeds

//Drug Loading per location
	MODEL_NAMES mnLoadedCargo
	
//Police static 
//	MODEL_NAMES mnPoliceCar
	MODEL_NAMES mnPolicePed
	
//Hijack bomb
	MODEL_NAMES mnHijackBomb
	
ENDSTRUCT

//PROC FILL_IN_LOCATIONS_AIR(VECTOR& myLocations[])
//	myLocations[0] = << 315.3289, -1157.8644, 28.2918 >>
//ENDPROC
//
//PROC FILL_IN_LOCATIONS(VECTOR& myLocations[])
//	myLocations[0] = << -2189.6768, 4374.3579, 41.3474 >>	//Off-road A
//	myLocations[1] = << 1758.9991, 3291.6294, 40.1563 >> 	//Off-road B
//	myLocations[2] = << 1758.9991, 3291.6294, 40.1563 >> //city A
//	myLocations[3] = << 1758.9991, 3291.6294, 40.1563 >> //city B
//ENDPROC

STRUCT AUTHORED_DATA
	VECTOR vCarPos[5]
	FLOAT fCarOri[5]
	VECTOR vPedPos[5]
	FLOAT fPedOri[5]
	VECTOR vPropPos[3]
	FLOAT fPropOri[3]
	VECTOR vGazeboPos
ENDSTRUCT

STRUCT AUTHORED_DATE_PROPS
	MODEL_NAMES mnPropName[MAX_NUMBER_DROPS]
	MODEL_NAMES mnGazeboProp[MAX_NUMBER_DROPS]
ENDSTRUCT

PROC INIT_STATIONARY_PROPS(AUTHORED_DATE_PROPS& authoredDataProps)
	// 3 Props at each location
	authoredDataProps.mnPropName[0] = Prop_Box_Wood01A
	authoredDataProps.mnPropName[1] = Prop_CratePile_07A
	authoredDataProps.mnPropName[2] = Prop_SackTruck_02a
	authoredDataProps.mnPropName[3] = Prop_Box_Wood01A
	
	// EDrugLocation_BoatDepot
	authoredDataProps.mnGazeboProp[0] = Prop_Gazebo_01
	// EDrugLocation_MountainCuldesac
	authoredDataProps.mnGazeboProp[1] = Prop_Gazebo_01
	// EDrugLocation_NorthBeach
	authoredDataProps.mnGazeboProp[2] = Prop_Gazebo_01
	// EDrugLocation_LightHouse
	authoredDataProps.mnGazeboProp[3] = Prop_Gazebo_01
ENDPROC

//As we move away from a data driven dynamic system and move towards 5 hardcoded variations we might need to hand authe each location.
//This function moves away from how everything was initially setup but it appears we no longer needs to support nice dynamic setup
FUNC AUTHORED_DATA GET_AUTHORED_LOCATION_DATA(EDrugLocation drugLocation, VECTOR vCenter)

	 AUTHORED_DATA returnData
	 
	//Legacy offsets from old proceduraly generated staging.
	VECTOR vOffsets[MAX_NUMBER_DROPS]
	VECTOR vOffsetPed = <<0,3,0>>
	vOffsets[0] = <<4,6,0>>
	vOffsets[1] = <<2,-3,0>>
	vOffsets[2] = <<-2,4,0>>
	vOffsets[3] = <<2,-4,0>>
	
	VECTOR vOffsetCar = <<-3,-3,0>>
	VECTOR vOffsetCar01 = <<3,-6,0>>
	VECTOR vOffsetCar02 = <<6,10,0>>
	
	SWITCH drugLocation
		CASE EDrugLocation_BoatDepot
			//lets get some props in there..
			returnData.vPedPos[0] = << 1363.8345, 4335.6045, 38.4631 >>
			returnData.vPedPos[1] = << 1354.1566, 4340.7661, 37.7030 >>
			returnData.vPedPos[2] = << 1345.7589, 4328.6436, 37.1615 >>
			returnData.vPedPos[3] = << 1347.0410, 4338.5811, 36.9735 >>
			returnData.vPedPos[4] = << 1355.1727, 4330.1406, 37.5035 >>
	
			returnData.vCarPos[0] = vCenter - vOffsetCar
			returnData.vCarPos[1] = vCenter - vOffsetCar01
			returnData.vCarPos[2] = vCenter - vOffsetCar02
			returnData.vCarPos[3] = <<0,0,0>>
	
			returnData.vPropPos[0] = << 1353.7715, 4336.5991, 37.4569 >>
			returnData.fPropOri[0] = 312.6768
			returnData.vPropPos[1] = << 1358.8907, 4336.1128, 37.9091 >>
			returnData.fPropOri[1] = 14.6000 
			returnData.vPropPos[2] = << 1356.7289, 4333.0356, 37.6442 >>
			returnData.fPropOri[2] = 200.9488
		
			returnData.vGazeboPos = << 1357.0568, 4333.5488, 37.6376 >>
			PRINTLN("INSIDE FIRST LOCATION")
		BREAK
		CASE EDrugLocation_BoatDepot_Smugglers
			//car
			returnData.vCarPos[0] = << 730.3441, 4184.7632, 39.6674 >>  
			returnData.fCarOri[0] = 239.3436 
			returnData.vCarPos[1] = << 726.7508, 4172.9619, 39.7089 >>  
			returnData.fCarOri[1] = 297.6413  
			returnData.vCarPos[2] = << 713.7870, 4170.1567, 39.7089 >>   
			returnData.fCarOri[2] = 266.5356  
			returnData.vCarPos[3] = << 707.7895, 4178.9570, 39.7089 >>  
			returnData.fCarOri[3] = 137.1188
			//ped
			returnData.vPedPos[0] = << 709.8475, 4178.1606, 39.7089 >>  
			returnData.fPedOri[0] = 269.8039
			returnData.vPedPos[1] = << 711.4312, 4171.7153, 39.7089 >>  
			returnData.fPedOri[1] = 357.7190 
			returnData.vPedPos[2] =  << 723.5290, 4173.1948, 39.7089 >> 
			returnData.fPedOri[2] = 115.2960
			returnData.vPedPos[3] = << 728.7289, 4175.9580, 39.7089 >>  
			returnData.fPedOri[3] = 48.0898
			returnData.vPedPos[4] = << 725.3412, 4183.0376, 39.7128 >>  
			returnData.fPedOri[4] = 199.2670 
			
			//lets get some props in there..
			returnData.vPropPos[0] = << 716.7869, 4173.5869, 39.7089 >>   
			returnData.fPropOri[0] = 194.6003
			returnData.vPropPos[1] = << 717.8929, 4180.6821, 39.7089 >>  
			returnData.fPropOri[1] = 197.2691
			returnData.vPropPos[2] = << 710.9958, 4174.8076, 39.7089 >>  
			returnData.fPropOri[2] = 259.3369
			
			returnData.vGazeboPos = << 717.6153, 4176.4287, 39.7190 >>
			PRINTLN("INSIDE SECOND LOCATION")
		BREAK
		CASE EDrugLocation_MountainCuldesac
			//car
			returnData.vCarPos[0] = << -1631.2777, 4725.8149, 51.7894 >> 
			returnData.fCarOri[0] = 121.2473  
			returnData.vCarPos[1] = << -1639.5769, 4730.7202, 52.5423 >> 
			returnData.fCarOri[1] = 16.1595  
			returnData.vCarPos[2] = << -1631.3024, 4745.5928, 51.7993 >>  
			returnData.fCarOri[2] = 278.0015   
			returnData.vCarPos[3] = << -1620.8112, 4743.3481, 51.9550 >> 
			returnData.fCarOri[3] = 351.9291
			//ped
			returnData.vPedPos[0] = << -1627.3552, 4739.2915, 51.8071 >> 
			returnData.fPedOri[0] = 118.1087
			returnData.vPedPos[1] = << -1631.1965, 4730.8564, 51.8749 >> 
			returnData.fPedOri[1] = 355.5203  
			returnData.vPedPos[2] =  << -1636.6216, 4731.4019, 52.4070 >>
			returnData.fPedOri[2] = 279.3583 
			returnData.vPedPos[3] = << -1625.1704, 4744.4541, 51.8431 >> 
			returnData.fPedOri[3] = 293.9838
			returnData.vPedPos[4] = << -1629.1821, 4743.2422, 51.9302 >> 
			returnData.fPedOri[4] = 85.6846  
			
			//lets get some props in there..
			returnData.vPropPos[0] = << -1634.8248, 4736.9702, 52.2205 >> 
			returnData.fPropOri[0] = 21.1632
			returnData.vPropPos[1] = << -1632.2793, 4739.0938, 52.1592 >> 
			returnData.fPropOri[1] = 120.4514 
			returnData.vPropPos[2] = << -1632.4955, 4731.7598, 51.9804 >> 
			returnData.fPropOri[2] = 52.9858
			
			returnData.vGazeboPos = << -1632.3506, 4737.4453, 52.3292 >>
			PRINTLN("INSIDE THIRD LOCATION")
		BREAK
		CASE EDrugLocation_MountainCommune
			//car
			returnData.vCarPos[0] = << -1138.6160, 4926.6880, 219.2112 >>  
			returnData.fCarOri[0] = 49.9944  
			returnData.vCarPos[1] = << -1156.5404, 4926.2515, 221.4249 >>  
			returnData.fCarOri[1] = 159.0628 
			returnData.vCarPos[2] = << -1155.9170, 4913.3149, 219.5449 >>   
			returnData.fCarOri[2] = 263.1431   
			returnData.vCarPos[3] = << -1145.1646, 4919.8740, 219.3643 >>  
			returnData.fCarOri[3] = 336.6283
			//ped
			returnData.vPedPos[0] = << -1145.1855, 4927.3394, 219.8742 >>  
			returnData.fPedOri[0] = 83.0160
			returnData.vPedPos[1] = << -1162.1506, 4924.1162, 221.7936 >>  
			returnData.fPedOri[1] = 33.9003  
			returnData.vPedPos[2] =  << -1157.7997, 4918.8857, 220.7632 >> 
			returnData.fPedOri[2] = 287.8745 
			returnData.vPedPos[3] = << -1154.9393, 4918.0649, 220.2623 >>  
			returnData.fPedOri[3] = 262.2286
			returnData.vPedPos[4] = << -1148.8778, 4919.9473, 219.7502 >>  
			returnData.fPedOri[4] = 344.0094 
			
			//lets get some props in there..
			returnData.vPropPos[0] = << -1151.8002, 4920.1021, 220.1048 >>  
			returnData.fPropOri[0] = 68.7571
			returnData.vPropPos[1] = << -1152.3186, 4924.2397, 220.6281 >>  
			returnData.fPropOri[1] = 352.4581 
			returnData.vPropPos[2] = << -1156.0400, 4922.1890, 221.1452 >>  
			returnData.fPropOri[2] = 270.6073
			
			returnData.vGazeboPos = << -1150.0455, 4923.9932, 220.3399 >>
			PRINTLN("INSIDE THIRD LOCATION")
		BREAK
		CASE EDrugLocation_NorthBeach
		
			//car
			returnData.vCarPos[0] = << 16.7885, 6863.9775, 11.8225 >>  
			returnData.fCarOri[0] = 92.5444   
			returnData.vCarPos[1] = << 6.5061, 6861.1328, 11.7246 >>   
			returnData.fCarOri[1] = 126.0629 
			returnData.vCarPos[2] = << 0.1925, 6853.1880, 11.9355 >>   
			returnData.fCarOri[2] = 215.1629 
			returnData.vCarPos[3] = << 14.7123, 6849.2192, 12.1607 >>   
			returnData.fCarOri[3] = 115.3306
			
			//ped
			returnData.vPedPos[0] = << 18.6217, 6848.2310, 12.2675 >>    
			returnData.fPedOri[0] = 22.8309
			returnData.vPedPos[1] = << 16.8315, 6855.5425, 12.0034 >>   
			returnData.fPedOri[1] = 15.3643 
			returnData.vPedPos[2] =  << 9.6123, 6863.3472, 11.6965 >>  
			returnData.fPedOri[2] = 68.9709 
			returnData.vPedPos[3] = << 7.5847, 6857.2651, 11.8480 >>   
			returnData.fPedOri[3] = 206.5964
			returnData.vPedPos[4] = << 5.8760, 6852.9209, 11.9634 >>   
			returnData.fPedOri[4] = 173.5287  
		
			returnData.vPropPos[0] = << 7.9738, 6857.0601, 11.8578 >>  
			returnData.fPropOri[0] = 240.1411
			returnData.vPropPos[1] = << 11.7550, 6852.4995, 12.0280 >> 
			returnData.fPropOri[1] = 137.0916 
			returnData.vPropPos[2] = << 20.3591, 6858.5659, 12.0046 >> 
			returnData.fPropOri[2] = 332.1983
			
			returnData.vGazeboPos = << 11.4565, 6856.5308, 11.9089 >>
			PRINTLN("INSIDE FOURTH LOCATION")
		BREAK
		CASE EDrugLocation_FarEastDock01
			//car
			returnData.vCarPos[0] = << 3814.9568, 4469.3784, 3.1262 >> 
			returnData.fCarOri[0] = 54.5640 
			returnData.vCarPos[1] = << 3803.2151, 4468.3433, 3.9447 >>  
			returnData.fCarOri[1] = 148.6869 
			returnData.vCarPos[2] = << 3800.1123, 4458.1406, 3.8260 >>  
			returnData.fCarOri[2] = 32.7177  
			returnData.vCarPos[3] = << 3817.3796, 4454.6758, 2.6161 >>  
			returnData.fCarOri[3] = 287.2097
			//ped
			returnData.vPedPos[0] = << 3821.6870, 4457.6372, 2.4513 >>  
			returnData.fPedOri[0] = 209.2304
			returnData.vPedPos[1] = << 3820.9556, 4464.0806, 2.6946 >>  
			returnData.fPedOri[1] = 242.0150
			returnData.vPedPos[2] = << 3810.6702, 4468.0996, 3.3584 >>  
			returnData.fPedOri[2] = 281.8730 
			returnData.vPedPos[3] = << 3805.0984, 4466.0293, 3.7161 >>  
			returnData.fPedOri[3] = 38.3414
			returnData.vPedPos[4] = << 3806.3831, 4459.8325, 3.4414 >> 
			returnData.fPedOri[4] = 83.1667 
			
			returnData.vPropPos[0] = << 3808.1733, 4462.0933, 3.3789 >>   
			returnData.fPropOri[0] = 280.1460
			returnData.vPropPos[1] = << 3812.6360, 4465.7993, 3.1841 >>   
			returnData.fPropOri[1] = 207.2805
			returnData.vPropPos[2] = << 3812.8054, 4458.6792, 3.0086 >>  
			returnData.fPropOri[2] = 317.0439
		
			returnData.vGazeboPos = << 3811.8823, 4462.6157, 3.1544 >>
			PRINTLN("INSIDE FIFTH LOCATION")
		BREAK
			CASE EDrugLocation_DeadZone_Air
			//car
			returnData.vCarPos[0] = << 1423.16, 3151.31, 40.73 >>
			returnData.fCarOri[0] = 355.06 
			returnData.vCarPos[1] = << 1426.03, 3141.74, 40.91 >>
			returnData.fCarOri[1] = 207.55 
			returnData.vCarPos[2] = << 1441.76, 3140.27, 40.91 >>
			returnData.fCarOri[2] = 160.77  
			returnData.vCarPos[3] = << 1442.42, 3150.59, 41.08 >>
			returnData.fCarOri[3] = 236.10
			//ped
			returnData.vPedPos[0] = << 1434.1869, 3133.8311, 40.0035 >>
			returnData.fPedOri[0] = 340.3069 
			returnData.vPedPos[1] = << 1444.3312, 3144.4058, 40.1064 >>
			returnData.fPedOri[1] = 88.7275  
			returnData.vPedPos[2] =  << 1417.9550, 3142.7859, 39.9881 >>
			returnData.fPedOri[2] = 280.2164  
			returnData.vPedPos[3] = << 1418.0444, 3135.8210, 39.9269 >>
			returnData.fPedOri[3] = 230.4450 
			returnData.vPedPos[4] = << 1424.7180, 3130.7385, 39.9528 >>
			returnData.fPedOri[4] = 242.1440  
			
			//lets get some props in there..
			returnData.vPropPos[0] = << 1430.2728, 3148.4370, 39.9986 >>
			returnData.fPropOri[0] = 312.6768
			returnData.vPropPos[1] = << 1429.0413, 3144.9070, 39.9223 >>
			returnData.fPropOri[1] = 14.6000 
			returnData.vPropPos[2] = << 1430.3026, 3140.7173, 39.9181 >>
			returnData.fPropOri[2] = 200.9488
			
			returnData.vGazeboPos = << 1436.9674, 3144.6394, 40.0988 >>
			PRINTLN("INSIDE SIXTH LOCATION")
		BREAK
		CASE EDrugLocation_DirtPit
			//lets get some props in there..
			returnData.vPedPos[0] = vCenter - vOffsetPed
			returnData.vPedPos[1] = vCenter - vOffsets[0]
			returnData.vPedPos[2] = vCenter - vOffsets[1]
			returnData.vPedPos[3] = vCenter - vOffsets[2]
			returnData.vPedPos[4] = vCenter - vOffsets[3]
	
			returnData.vCarPos[0] = << 2957.7729, 2802.5657, 40.7760 >>
			returnData.vCarPos[1] = << 2947.3162, 2798.2712, 39.9545 >>
			returnData.vCarPos[2] = << 2943.3979, 2782.4780, 38.6242 >>
			returnData.vCarPos[3] = << 2957.2480, 2780.8262, 39.7850 >>
		
			returnData.vPropPos[0] = << 2954.6069, 2796.8025, 40.0158 >>
			returnData.fPropOri[0] = 312.6768
			returnData.vPropPos[1] = << 2958.0706, 2794.7825, 39.7859 >>
			returnData.fPropOri[1] = 14.6000 
			returnData.vPropPos[2] = << 2954.1577, 2787.9978, 40.5765 >>
			returnData.fPropOri[2] = 200.9488
			
			returnData.vGazeboPos = << 2943.9609, 2790.9014, 39.3536 >>
			PRINTLN("INSIDE SEVENTH LOCATION")
		BREAK
		CASE EDrugLocation_LightHouse
			//car
			returnData.vCarPos[0] = << 3297.6489, 5145.3354, 17.3838 >> 
			returnData.fCarOri[0] = 42.8914 
			returnData.vCarPos[1] = << 3275.0557, 5148.4961, 18.3073 >> 
			returnData.fCarOri[1] = 316.9439 
			returnData.vCarPos[2] = << 3283.3335, 5165.7163, 17.7520 >> 
			returnData.fCarOri[2] = 220.1146  
			returnData.vCarPos[3] = << 3300.9229, 5157.7139, 17.3992 >> 
			returnData.fCarOri[3] = 104.3215
			//ped
			returnData.vPedPos[0] = << 3300.8369, 5148.7290, 17.2490 >> 
			returnData.fPedOri[0] = 173.5395
			returnData.vPedPos[1] = << 3285.8970, 5145.3618, 17.5925 >> 
			returnData.fPedOri[1] = 97.3138
			returnData.vPedPos[2] =  << 3273.1328, 5154.5044, 18.3428 >> 
			returnData.fPedOri[2] = 120.0509  
			returnData.vPedPos[3] = << 3288.1099, 5146.3799, 17.4096 >> 
			returnData.fPedOri[3] = 240.1863
			returnData.vPedPos[4] = << 3271.6138, 5169.0933, 18.6001 >>
			returnData.fPedOri[4] = 259.8900 
			
			returnData.vPropPos[0] = << 3284.2732, 5149.6470, 17.6151 >>  
			returnData.fPropOri[0] = 53.2345
			returnData.vPropPos[1] = << 3285.8784, 5155.1318, 17.5177 >>  
			returnData.fPropOri[1] = 305.6007
			returnData.vPropPos[2] = << 3291.3550, 5152.9663, 17.2847 >> 
			returnData.fPropOri[2] = 250.5027
		
			returnData.vGazeboPos = << 3284.2732, 5149.6470, 17.6151 >>
			PRINTLN("INSIDE EIGHTH LOCATION")
		BREAK
		CASE EDrugLocation_LittleHouseOnTheHill
			returnData.vCarPos[0] = << -285.5493, 2572.4973, 70.8495 >>
			returnData.vPedPos[0] = << -288.5072, 2566.4160, 72.7682>>
		BREAK
		
		DEFAULT
			//ped
			returnData.vPedPos[0] = vCenter - vOffsetPed
			returnData.vPedPos[1] = vCenter - vOffsets[0]
			returnData.vPedPos[2] = vCenter - vOffsets[1]
			returnData.vPedPos[3] = vCenter - vOffsets[2]
			returnData.vPedPos[4] = vCenter - vOffsets[3]
	
			returnData.vCarPos[0] = vCenter - vOffsetCar
			returnData.vCarPos[1] = vCenter - vOffsetCar01
			returnData.vCarPos[2] = vCenter - vOffsetCar02
			returnData.vCarPos[3] = <<0,0,0>>
			
		BREAK
	ENDSWITCH
	
	RETURN returnData
ENDFUNC

FUNC MODEL_NAMES GET_AMBUSHER_VEHICLE_MODEL()
	INT iRandInt
	
	iRandInt = GET_RANDOM_INT_IN_RANGE() % 3
	
	SWITCH iRandInt
		CASE 0
			PRINTLN("RETURNING - BISON")
			RETURN BISON
		BREAK
		CASE 1
			PRINTLN("RETURNING - DLOADER")
			RETURN DLOADER
		BREAK
		CASE 2
			PRINTLN("RETURNING - REBEL")
			RETURN REBEL
		BREAK
		DEFAULT
			PRINTLN("DEFAULT: RETURNING - REBEL")
			RETURN BISON
		BREAK
	ENDSWITCH
	
	PRINTLN("NO CASE: RETURNING - BISON")
	RETURN BISON
ENDFUNC

FUNC BOOL POPULATE_DRUG_LOCATION(DRUG_LOCATION_DATA& myData, EDrugLocation drugLocation)
	//myData.bConfigured = TRUE

	SWITCH drugLocation
	
		CASE EDrugLocation_TrailerPark
			//Supports ground and air
		
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "TrailerParkGround" //"DrgTrfkAmb"
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 28.7612, 3711.1597, 39.2174 >> //<< 26.7261, 3747.9270, 39.2392 >>
			myData.carRecData[0].x = 0.0079 //0.0004
			myData.carRecData[0].y = 0.0104 //0.0012
			myData.carRecData[0].j = -0.2702 //0.9432
			myData.carRecData[0].k = 0.9627 //-0.3321
			myData.carRecData[0].carrecVehicle = Bison
			
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "TrailerParkGround" //"DrgTrfkAmb"
			myData.carRecData[1].carrecNum = 104 
			myData.carRecData[1].carCoords = << 99.5151, 3714.7649, 39.2737 >> //<< 67.3368, 3765.8794, 39.2033 >>
			myData.carRecData[1].x = 0.0145 //-0.0010
			myData.carRecData[1].y = 0.0599 //0.0131
			myData.carRecData[1].j = 0.1116 //0.9966
			myData.carRecData[1].k = 0.9918 //-0.0813
			myData.carRecData[1].carrecVehicle = Bison
			
			myData.carRecData[2].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "TrailerParkAir" 
			myData.carRecData[2].carrecNum = 101
			myData.carRecData[2].carCoords = << 30.8342, 3948.3455, 83.5679 >> //<< -499.8578, 3830.9246, 177.1888 >>
			myData.carRecData[2].x = -0.0132 //-0.0132
			myData.carRecData[2].y = -0.0055 //-0.0055
			myData.carRecData[2].j = 0.9998 //0.9998
			myData.carRecData[2].k = -0.0155 //-0.0155
			myData.carRecData[2].carrecVehicle = CUBAN800
			myData.carRecData[2].fDropTime = 176.5
			
			//
			//Drug Pickup Location
			//
			
			myData.vCenterPos = << 53.8249, 3733.3455, 38.6775 >>
			myData.vDrugPickup = << 51.8722, 3725.2280, 38.6335 >>
		
			myData.vCutscenePos1 = << 49.8334, 3726.3777, 39.1895 >> 
			myData.vCutsceneOrien1 = << 12.3204, 0.0111, 37.4670 >>
			myData.vCutscenePos2 = << 55.9215, 3731.2354, 39.2212 >> 
			myData.vCutsceneOrien2 = << 12.3204, 0.0111, 37.4670 >>
			
			//myData.vBadGuysCar = << 42.0726, 3730.5212, 38.6346 >>
			//myData.fBadGuyCarRot = 215.4962
			
			myData.vSmugVehPos[0] = << 985.9750, 2152.6077, 47.8136 >>
			myData.fSmugVehRot[0] = 35.7220
		BREAK
		
		CASE EDrugLocation_TrailerPark_02
			myData.vCenterPos = << 2368.3445, 2530.2019, 45.6681 >>
			myData.vDrugPickup = << 2368.3445, 2530.2019, 45.6681 >>
		BREAK
		
		CASE EDrugLocation_BeerBar
			myData.vCenterPos = << 2533.5491, 2612.9316, 36.9446 >>
			
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "BeerBarAmbush" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 2591.0618, 2527.5847, 30.1715 >>
			myData.carRecData[0].x = 0.0635
			myData.carRecData[0].y = -0.0044
			myData.carRecData[0].j = 0.1434
			myData.carRecData[0].k = 0.9876
			myData.carRecData[0].carrecVehicle = Bison
		
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "BeerBarAmbush" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 2465.6577, 2542.4580, 43.3590 >>
			myData.carRecData[1].x = 0.0739
			myData.carRecData[1].y = -0.0406
			myData.carRecData[1].j = -0.3310
			myData.carRecData[1].k = 0.9398
			myData.carRecData[1].carrecVehicle = Bison
			
			// Plane Drop Recording Data
			myData.carRecData[2].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "beerBar" 
			myData.carRecData[2].carrecNum = 101
			myData.carRecData[2].carCoords = << 2254.2129, 2837.0562, 69.6294 >>
			myData.carRecData[2].x = -0.0099
			myData.carRecData[2].y = 0.0039
			myData.carRecData[2].j = 0.8687
			myData.carRecData[2].k = -0.4952
			myData.carRecData[2].carrecVehicle = CUBAN800
			myData.carRecData[2].fDropTime = 759.0
			
			//
			//Drug Pickup Location
			//
			myData.vDrugPickup = << 2537.0156, 2591.7642, 35.6751 >>
			
			myData.vCutscenePos1 = << 2556.1963, 2626.2241, 42.7265 >> 
			myData.vCutsceneOrien1 = << -4.2563, -0.2061, 162.6607 >>
			
		BREAK
	
		CASE EDrugLocation_AutoService
			myData.vCenterPos = << 274.1707, 2608.0642, 43.6995 >>
			myData.vDrugPickup = << 274.1707, 2608.0642, 43.6995 >>
		BREAK
		
		CASE EDrugLocation_CountryHouse
			//Hasn't been setup for ground transport yet
			myData.vCenterPos = << 817.3708, 2197.8608, 51.0174 >>
			myData.vDrugPickup = << 817.3708, 2197.8608, 51.0174 >>
		BREAK
		
		CASE EDrugLocation_PlateauHouse
			//Hasn't been setup for ground transport yet
			myData.vCenterPos = << -1445.6195, 1636.3636, 95.4142 >> 
		BREAK
		
		CASE EDrugLocation_OceanExit
			//Hasn't been setup for ground transport yet
			myData.vCenterPos = << -1847.1652, 4512.6045, 19.7672 >>
		BREAK
		
		CASE EDrugLocation_RiverCanyonPass
			//ambush
			
			// Plane Drop Recording Data
			myData.carRecData[0].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "rugged" 
			myData.carRecData[0].carrecNum = 101
			myData.carRecData[0].carCoords = << -1456.6138, 4504.2051, 112.8639 >>
			myData.carRecData[0].x = 0.0388
			myData.carRecData[0].y = -0.0834
			myData.carRecData[0].j = 0.9008
			myData.carRecData[0].k = -0.4245
			myData.carRecData[0].carrecVehicle = CUBAN800
			myData.carRecData[0].fDropTime = 310.0 

			myData.vDrugPickup = << -1185.6533, 4305.0718, 75.2981 >>
			myData.vCenterPos = << -1185.6533, 4305.0718, 75.2981 >>
			
			myData.vCutscenePos1 = << -1178.0541, 4306.7651, 84.5263 >>
			myData.vCutsceneOrien1 = << -4.4212, -0.0000, 121.8599 >>
			myData.vCutscenePos2 = << -1177.5720, 4307.0640, 77.1852 >>
			myData.vCutsceneOrien2 = << -4.4212, -0.0000, 121.8599 >>
		
		BREAK
		
		CASE EDrugLocation_GraineryParkingLot
			// Plane Drop Recording Data
			myData.carRecData[0].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "GraineryParkingLot" 
			myData.carRecData[0].carrecNum = 101
			myData.carRecData[0].carCoords = << 2829.4553, 4634.9912, 81.9373 >>
			myData.carRecData[0].x = 0.0323
			myData.carRecData[0].y = 0.0133
			myData.carRecData[0].j = 0.9882
			myData.carRecData[0].k = -0.1494
			myData.carRecData[0].carrecVehicle = CUBAN800
			myData.carRecData[0].fDropTime = 234.0
		
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = << 2911.2507, 4375.0264, 49.4025 >>
			myData.vDrugPickup = << 2910.9648, 4371.0435, 49.3964 >>
			
			// Cutscene cameras - setup for flare cutscene
			myData.vCutscenePos1 = << 2901.5540, 4384.2588, 50.4232 >>
			myData.vCutsceneOrien1 = << 21.8867, 0.0000, 156.2361 >>
			myData.vCutscenePos2 = << 2911.5889, 4379.8408, 50.4232 >>
			myData.vCutsceneOrien2 = << 18.0566, 0.0000, 156.2361 >>
	
		BREAK
		
		
		CASE EDrugLocation_LargeFarm
			//Hasn't been setup for ground transport yet
			myData.vCenterPos = << 1237.7247, 1851.8115, 78.6409 >>
			myData.vDrugPickup = << 1237.7247, 1851.8115, 78.6409 >>
			
			myData.vCutscenePos1 = << 1244.2601, 1838.8962, 90.8553 >>
			myData.vCutsceneOrien1 = << -5.0750, 0.0000, 0.2706 >>
		
		BREAK
		
		CASE EDrugLocation_SmallTrailer
			//Hasn't been setup for ground transport yet
			myData.vCenterPos = << 2670.7637, 3541.2505, 50.7144 >>
			myData.vDrugPickup = << 2670.7637, 3541.2505, 50.7144 >>
		BREAK
		
		CASE EDrugLocation_LargeCountryHouse
			//Hasn't been setup for ground transport yet
			myData.vCenterPos = << 2477.0017, 4988.3696, 45.2699 >>
			myData.vDrugPickup = << 2477.0017, 4988.3696, 45.2699 >>
		BREAK
		
		CASE EDrugLocation_SmallCountryHouse
			//Hasn't been setup for ground transport yet
			myData.vCenterPos = << 1949.2999, 4637.1104, 39.5446 >>
			myData.vDrugPickup = << 1949.2999, 4637.1104, 39.5446 >>
		BREAK
		
		CASE EDrugLocation_WreckedAirfield
		
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "PlaneGraveyardGround" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 2442.7661, 3105.6719, 47.0850 >>
			myData.carRecData[0].x = -0.0150
			myData.carRecData[0].y = 0.0685
			myData.carRecData[0].j = 0.6052
			myData.carRecData[0].k = 0.7930
			myData.carRecData[0].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "PlaneGraveyardGround" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 2305.8843, 3053.9641, 45.6252 >>
			myData.carRecData[1].x = 0.0386
			myData.carRecData[1].y = 0.0166
			myData.carRecData[1].j = -0.2598
			myData.carRecData[1].k = 0.9647
			myData.carRecData[1].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
		
			// Extra ambushers
			//TODO figure out how to handle this aray
			myData.carRecData[2].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "PlaneGraveyardGround" 
			myData.carRecData[2].carrecNum = 103 
			myData.carRecData[2].carCoords = << 2416.5168, 3173.6790, 48.2513 >>
			myData.carRecData[2].x = 0.0018
			myData.carRecData[2].y = 0.0105
			myData.carRecData[2].j = 0.8773
			myData.carRecData[2].k = 0.4797
			myData.carRecData[2].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[3].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[3].bConfigured = TRUE
			myData.carRecData[3].carrecName = "PlaneGraveyardGround" 
			myData.carRecData[3].carrecNum = 104 
			myData.carRecData[3].carCoords = << 2306.6968, 3186.0728, 46.2549 >>
			myData.carRecData[3].x = 0.0211
			myData.carRecData[3].y = 0.0036
			myData.carRecData[3].j = 0.9995
			myData.carRecData[3].k = -0.0235
			myData.carRecData[3].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			// Plane Drop Recording Data
			myData.carRecData[4].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[4].bConfigured = TRUE
			myData.carRecData[4].carrecName = "WreckedAirfield" 
			myData.carRecData[4].carrecNum = 101
			myData.carRecData[4].carCoords = << 2659.0615, 3921.6250, 96.9736 >>
			myData.carRecData[4].x = -0.2848
			myData.carRecData[4].y = 0.0749
			myData.carRecData[4].j = 0.9529
			myData.carRecData[4].k = 0.0727
			myData.carRecData[4].carrecVehicle = CUBAN800
			myData.carRecData[4].fDropTime = 755
			
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = <<2340.5076, 3094.9241, 47.0909>> //<< 2345.8074, 3098.5378, 47.0438 >>
			myData.vDrugPickup = <<2340.5076, 3094.9241, 47.0909>>
			myData.fTriggerPlaneDistance = 225
			myData.sObjective = "DTRFKGR_15"
			myData.sConvo = "ARMS_TYPE108"
			myData.gangType = GANG_MARABUNTA		
			myData.fTimeAllowedToStayOnRoads = 32			
			
			// Cutscene cameras - setup for flare cutscene
			myData.vCutscenePos1 = << 2350.1982, 3107.4761, 54.1539 >> //<< 2320.7612, 3120.3679, 55.2518 >>
			myData.vCutsceneOrien1 = << 11.3665, 0.0000, 143.9136 >> //<< -5.7185, 0.0000, -65.8899 >>
			myData.vCutscenePos2 = << 2349.4607, 3106.4644, 47.9250 >>
			myData.vCutsceneOrien2 = << 11.3665, -0.0000, 143.9136 >> 
		
		BREAK
		
		CASE EDrugLocation_DirtCuldesac
		
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "DirtCuldesac" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 359.8208, 2933.4028, 40.1980 >>
			myData.carRecData[0].x = 0.0444
			myData.carRecData[0].y = 0.0183
			myData.carRecData[0].j = -0.4302
			myData.carRecData[0].k = 0.9015
			myData.carRecData[0].carrecVehicle = Bison
			
			//TODO figure out how to handle this aray
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "DirtCuldesac" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 342.1675, 3008.6018, 39.3665 >>
			myData.carRecData[1].x = 0.0187
			myData.carRecData[1].y = -0.0117
			myData.carRecData[1].j = 0.7739
			myData.carRecData[1].k = -0.6329
			myData.carRecData[1].carrecVehicle = Bison
		
			// Plane Drop Recording Data
			myData.carRecData[2].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "DirtCuldesacAir" 
			myData.carRecData[2].carrecNum = 101
			myData.carRecData[2].carCoords = << 580.9810, 2946.8357, 66.1960 >>
			myData.carRecData[2].x = -0.1316
			myData.carRecData[2].y = 0.0963
			myData.carRecData[2].j = 0.6449
			myData.carRecData[2].k = 0.7466
			myData.carRecData[2].carrecVehicle = CUBAN800
			myData.carRecData[2].fDropTime = 154
		
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = << 409.2248, 2991.8599, 39.4351 >>
			myData.vDrugPickup = << 406.0919, 2994.9817, 39.3263 >>
			
			// Cutscene cameras - setup for flare cutscene
			myData.vCutscenePos1 = << 413.8964, 2984.8691, 40.6165 >>
			myData.vCutsceneOrien1 = << -0.3626, -0.0000, -40.2616 >>
			myData.vCutscenePos2 = << 407.7802, 2988.2852, 40.2685 >>
			myData.vCutsceneOrien2 = << -1.6860, -0.0000, -31.8646 >>
			
		BREAK
		
		CASE EDrugLocation_WestLakeMiddle
			myData.vCenterPos = << 1755.0707, 4239.8984, 17.2274 >>
			
			// Plane Drop Recording Data
			myData.carRecData[0].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "WaterDrop" 
			myData.carRecData[0].carrecNum = 101
			myData.carRecData[0].carCoords = << -3393.0884, 3841.7505, 59.0627 >>
			myData.carRecData[0].x = 0.1246
			myData.carRecData[0].y = 0.0559
			myData.carRecData[0].j = 0.8677
			myData.carRecData[0].k = -0.4780
			myData.carRecData[0].carrecVehicle = CUBAN800
			myData.carRecData[0].fDropTime = 759.0
			
			myData.vDrugPickup = << 1755.0707, 4239.8984, 17.2274 >>
		
			myData.vCutscenePos1 = << 1756.2843, 4219.0542, 34.1280 >>
			myData.vCutsceneOrien1 = << 0.6506, 0.0000, 34.3944 >>
			
			RETURN TRUE
		BREAK
		
		CASE EDrugLocation_WestBeach
			myData.vCenterPos = << 2107.1292, 4794.2754, 39.8284 >>
		BREAK
		
		CASE EDrugLocation_NorthDock
			myData.vCenterPos = << 733.6285, 4129.9336, 23.8211 >>
			myData.vDrugPickup = << 725.9564, 4215.8081, 49.7332 >>
		BREAK
		CASE EDrugLocation_SeaSide // Used in Air, same as Ground 3
			myData.vCenterPos = << 715.0595, 4175.0371, 39.7089 >>
			myData.vDrugPickup = << 715.0595, 4175.0371, 39.7089 >>
		BREAK
		CASE EDrugLocation_SouthDock
			myData.vCenterPos = << 1487.1635, 3807.3096, 27.8818 >> 
		BREAK
		CASE EDrugLocation_BoatDepot
		
			myData.smugglersRecordings[0].bConfigured = TRUE
			myData.smugglersRecordings[0].carrecName = "BoatDepotFarms"
			myData.smugglersRecordings[0].carrecNum = 102
			myData.smugglersRecordings[0].carCoords = << 1656.1627, 4828.5933, 41.4706 >>
			myData.smugglersRecordings[0].x = -0.0164
			myData.smugglersRecordings[0].y = -0.0289
			myData.smugglersRecordings[0].j = -0.6341
			myData.smugglersRecordings[0].k = 0.7726
			myData.smugglersRecordings[0].fDefaultPlaybackSpeed = 0.70
			myData.smugglersRecordings[0].fMultiplier = 1.75
			
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "BoatDepot"
			myData.smugglersRecordings[1].carrecNum = 101
			myData.smugglersRecordings[1].carCoords = << 859.8226, 4281.0049, 95.4879 >>
			myData.smugglersRecordings[1].x = -0.0822
			myData.smugglersRecordings[1].y = -0.0467
			myData.smugglersRecordings[1].j = -0.6692
			myData.smugglersRecordings[1].k = 0.7370
			myData.smugglersRecordings[1].vTriggerDistance = <<100,100,10>>
			
			myData.vCenterPos = << 1362.0553, 4337.4702, 38.3688 >>
			myData.vDrugPickup =  << 1362.0553, 4337.4702, 38.3688 >>
			myData.vPlaneRecTriggerPos = << 1545.5469, 4561.8384, 49.8170 >>
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 1658.0387, 4828.5361, 40.9956 >> 
			myData.fSmugVehRot[0] = 275.6415
		BREAK
		
		CASE EDrugLocation_BoatDepot_Smugglers
		
			myData.smugglersRecordings[0].bWaypointConfigured = TRUE
			myData.smugglersRecordings[0].recordingType = RECORDING_TYPE_CAR
			myData.smugglersRecordings[0].carrecWaypointName = "BDFWaypoint"
			myData.smugglersRecordings[0].carCoords = << 1653.9952, 4826.3555, 41.0051 >>
			
//			myData.smugglersRecordings[0].bConfigured = FALSE
//			myData.smugglersRecordings[0].carrecName = "BoatDepotFarms"
//			myData.smugglersRecordings[0].carrecNum = 103
//			myData.smugglersRecordings[0].carCoords = << 1656.1627, 4828.5933, 41.4706 >>
//			myData.smugglersRecordings[0].x = -0.0164
//			myData.smugglersRecordings[0].y = -0.0289
//			myData.smugglersRecordings[0].j = -0.6341
//			myData.smugglersRecordings[0].k = 0.7726
//			myData.smugglersRecordings[0].fDefaultPlaybackSpeed = 0.70
//			myData.smugglersRecordings[0].fMultiplier = 1.54
			
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "BoatDepot"
			myData.smugglersRecordings[1].carrecNum = 102
			myData.smugglersRecordings[1].carCoords = << 1327.6686, 3345.0820, 99.6745 >>
			myData.smugglersRecordings[1].x = 0.0279
			myData.smugglersRecordings[1].y = 0.0197
			myData.smugglersRecordings[1].j = 0.3136
			myData.smugglersRecordings[1].k = 0.9489
			myData.smugglersRecordings[1].vTriggerDistance = <<50,50,10>>
			myData.smugglersRecordings[1].fDropTime = 990.0
			
			myData.vCenterPos = << 717.6153, 4176.4287, 39.7190 >>
			myData.vDrugPickup =  << 717.6153, 4176.4287, 39.7190 >>
			myData.vPlaneRecTriggerPos = << 831.2018, 4407.9390, 51.2971 >>
			myData.sObjective = "DTRFKGR_8"
			myData.sConvo = "ARMS_REPT1"			
			myData.gangType = GANG_HILLBILLIES			
			myData.fTimeAllowedToStayOnRoads = 32			
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 1658.0387, 4828.5361, 40.9956 >> 
			myData.fSmugVehRot[0] = 275.6415
			
			// SITE CAN ONLY BE USED AS A PICKUP NOW!!!
			myData.vSmugHeliPos = << 777.5092, 4227.9326, 50.2396 >>
			myData.fSmugHeliRot = 282.9541
	
		BREAK
		
		CASE EDrugLocation_BoatDepot_Smugglers01
		
			myData.smugglersRecordings[0].bWaypointConfigured = TRUE
			myData.smugglersRecordings[0].recordingType = RECORDING_TYPE_CAR
			myData.smugglersRecordings[0].carrecWaypointName = "BoatDepotFarms"
			myData.smugglersRecordings[0].carCoords = << 1656.1627, 4828.5933, 41.4706 >>
			
//			myData.smugglersRecordings[0].bConfigured = TRUE
//			myData.smugglersRecordings[0].carrecName = "BoatDepotFarms"
//			myData.smugglersRecordings[0].carrecNum = 102
//			myData.smugglersRecordings[0].carCoords = << 1656.1627, 4828.5933, 41.4706 >>
//			myData.smugglersRecordings[0].x = -0.0164
//			myData.smugglersRecordings[0].y = -0.0289
//			myData.smugglersRecordings[0].j = -0.6341
//			myData.smugglersRecordings[0].k = 0.7726
//			myData.smugglersRecordings[0].fDefaultPlaybackSpeed = 0.70
//			myData.smugglersRecordings[0].fMultiplier = 1.75
			
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "BoatDepot"
			myData.smugglersRecordings[1].carrecNum = 101
			myData.smugglersRecordings[1].carCoords = << 859.8226, 4281.0049, 52.4879 >>
			myData.smugglersRecordings[1].x = -0.0822
			myData.smugglersRecordings[1].y = -0.0467
			myData.smugglersRecordings[1].j = -0.6692
			myData.smugglersRecordings[1].k = 0.7370
			myData.smugglersRecordings[1].vTriggerDistance = <<150,150,10>>
			myData.smugglersRecordings[1].fDropTime = 455.0
			myData.vPlaneRecTriggerPos = << 1352.0400, 4370.2515, 43.3564 >>
			
			myData.vCenterPos = << 1352.0400, 4370.2515, 43.3564 >>
			myData.vDrugPickup =  << 1352.0400, 4370.2515, 43.3564 >>
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 1658.0387, 4828.5361, 40.9956 >> 
			myData.fSmugVehRot[0] = 275.6415
			
			// SITE CAN ONLY BE USED AS A PICKUP NOW!!!
			myData.vSmugHeliPos = << 1386.1794, 4347.2559, 42.0701 >> 
			myData.fSmugHeliRot = 321.2393
			myData.sObjective = "DTRFKGR_2"
			myData.sConvo = "ARMS_GR03"
			myData.fTimeAllowedToStayOnRoads = 32
			
			myData.gangType = GANG_MARABUNTA		
		BREAK
		
		CASE EDrugLocation_CornField
			myData.vCenterPos = << 3280.2900, 5159.4180, 17.9351 >>
			myData.vDrugPickup = << 3280.2900, 5159.4180, 17.9351 >>
			
			myData.vCutscenePos1 = << 3274.4675, 5158.0952, 19.1077 >>  
			myData.vCutsceneOrien1 = << 0.2953, -0.0000, -85.7135 >>
			myData.vCutscenePos2 = << 3286.7043, 5151.1255, 18.4534 >>      
			myData.vCutsceneOrien2 = << 7.5371, 0.0056, -79.4105 >>
		
			myData.vSmugHeliPos = << 3292.4490, 5150.6470, 17.7515 >>   
			myData.fSmugHeliRot = 302.2079
			
			myData.vCarPlacementShootEnd = << 3287.8691, 5163.0586, 17.4489 >>  
			myData.fCarPlacementShootEnd = 131.4570
		BREAK
		
		CASE EDrugLocation_MegaMall
		
			myData.smugglersRecordings[0].bWaypointConfigured = TRUE
			myData.smugglersRecordings[0].recordingType = RECORDING_TYPE_CAR
			myData.smugglersRecordings[0].carrecWaypointName = "MegaMallWayPt"
			myData.smugglersRecordings[0].carCoords = << 2869.1921, 4514.6646, 46.2980 >>
			
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "MegaMall"
			myData.smugglersRecordings[1].carrecNum = 101
			myData.smugglersRecordings[1].carCoords = << 2426.6526, 3348.9714, 72.6849 >>
			myData.smugglersRecordings[1].x = 0.0557
			myData.smugglersRecordings[1].y = -0.0115
			myData.smugglersRecordings[1].j = -0.4094
			myData.smugglersRecordings[1].k = 0.9106
			
			// Plane dropping info
			myData.vPlaneRecTriggerPos = << 2669.8481, 3546.6760, 50.5799 >>
			myData.smugglersRecordings[1].vTriggerDistance = <<175,175,10>>
			myData.smugglersRecordings[1].fDropTime = 268.0
			
			myData.vCenterPos = << 2669.8481, 3546.6760, 50.5799 >>
			myData.vDrugPickup =  << 2669.8481, 3546.6760, 50.5799 >>
			myData.sObjective = "DTRFKGR_14"
			myData.sConvo = "ARMS_TYP2_14"
			myData.gangType = GANG_MARABUNTA			
			myData.fTimeAllowedToStayOnRoads = 32			
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 2869.1921, 4514.6646, 46.2980 >>   
			myData.fSmugVehRot[0] = 174.1073
		BREAK
			
		CASE EDrugLocation_MainAirport
			myData.vCenterPos = << 2076.7112, 4782.2544, 40.0605 >> //<< 2129.5649, 4802.7271, 40.0246 >> //<< 2107.1292, 4794.2754, 39.8284 >>
			myData.vDrugPickup = << 2129.5649, 4802.7271, 40.0246 >>
		BREAK
		
		CASE EDrugLocation_MainAirportGround
			myData.vCenterPos = <<2149.7371, 4798.4717, 40.1071>>
			myData.vDrugPickup = <<2149.7371, 4798.4717, 40.1071>>
		BREAK
		
		CASE EDrugLocation_LiquorJrMarket
			myData.vCenterPos = << 532.8872, 2670.8606, 41.3513 >>
			myData.vDrugPickup = << 532.8872, 2670.8606, 41.3513 >>

			myData.vCutscenePos1 = << 535.0646, 2669.3086, 42.8091 >> 
			myData.vCutsceneOrien1 = << -3.1569, -0.1465, -11.3918 >> 
			myData.vCutscenePos2 = << 525.2866, 2665.8369, 42.9374 >> 
			myData.vCutsceneOrien2 = << 4.9124, -0.0076, -96.9372 >> 
		
			myData.vSmugHeliPos = << 532.8944, 2658.2800, 41.65 >> 
			myData.fSmugHeliRot = 329.0565
			
			myData.vCarPlacementShootEnd = << 521.9125, 2666.1011, 41.4715 >>
			myData.fCarPlacementShootEnd = 3.0063
		BREAK
		
		CASE EDrugLocation_JackInn
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "JackInn" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 1892.3666, 3830.5952, 31.9409 >>
			myData.carRecData[0].x = 0.0109
			myData.carRecData[0].y = -0.0022
			myData.carRecData[0].j = -0.4457
			myData.carRecData[0].k = 0.8951
			myData.carRecData[0].carrecVehicle = POLICE
			
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "JackInn" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 1964.5411, 3775.3755, 31.6535 >>
			myData.carRecData[1].x = 0.0124
			myData.carRecData[1].y = 0.0048
			myData.carRecData[1].j = 0.1899
			myData.carRecData[1].k = 0.9817
			myData.carRecData[1].carrecVehicle = POLICE
			
			//
			//Drug Pickup Location
			//
			myData.vCenterPos = << 1963.9569, 3838.8428, 31.2091 >>
			myData.vDrugPickup = << 1963.9569, 3838.8428, 31.2091 >>
			
			// Camera Setup - Contact Cutscene
			myData.vCutscenePos1 = << 1974.4448, 3839.4541, 35.3948 >> //<< 1975.4607, 3858.1719, 37.7924 >> 
			myData.vCutsceneOrien1 = << 12.1583, -0.0096, 166.4116 >> //<< -1.2582, 0.0000, 84.6681 >>
			myData.vCutscenePos2 = << 1974.4095, 3836.8657, 31.9998 >>
			myData.vCutsceneOrien2 = << 16.4125, 0.0000, 143.1034 >>
		BREAK
		
		CASE EDrugLocation_TempDiner
			myData.vCenterPos = << 2683.9819, 4335.4253, 44.8820 >>
			myData.vDrugPickup = << 2683.9819, 4335.4253, 44.8820 >>

			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 2857.7578, 4436.2798, 47.7903 >>
			myData.fSmugVehRot[0] = 96.8728
		
			// For Hijack
			myData.vTruckPos = << 2715.1477, 4386.8677, 46.7436 >> //<< 2324.0315, 4709.0962, 35.4122 >>  
			myData.fTruckRot = 115.0899 //247.8917
		BREAK
		
		CASE EDrugLocation_RecycleCenter
			myData.vCenterPos = <<2038.9847, 3222.6926, 43.8001>>
			myData.vDrugPickup = <<2038.9847, 3222.6926, 43.8001>>
			myData.sObjective = "DTRFKGR_10"
			myData.sConvo = "ARMS_REPT2"
			myData.gangType = GANG_MEXICANS			
			myData.fTimeAllowedToStayOnRoads = 32			
			
			myData.vPlaneRecTriggerPos = << 2039.0469, 3220.2834, 43.7959 >>
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 2678.2783, 3963.5103, 43.4808 >>    
			myData.fSmugVehRot[0] = 213.4533
			
			myData.smugglersRecordings[0].bWaypointConfigured = TRUE
			myData.smugglersRecordings[0].recordingType = RECORDING_TYPE_CAR
			myData.smugglersRecordings[0].carrecWaypointName = "NewTrailerParkWayPt"
			myData.smugglersRecordings[0].carCoords = << 2678.2783, 3963.5103, 43.4808 >> 
			
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "RecycleCenterPlane"
			myData.smugglersRecordings[1].carrecNum = 101
			myData.smugglersRecordings[1].carCoords = << 1674.2310, 2611.9253, 113.5059 >>
			myData.smugglersRecordings[1].x = -0.0331
			myData.smugglersRecordings[1].y = 0.0094
			myData.smugglersRecordings[1].j = -0.2760
			myData.smugglersRecordings[1].k = 0.9606
			myData.smugglersRecordings[1].vTriggerDistance = <<350,350,10>>
			myData.smugglersRecordings[1].fDropTime = 665.0
			myData.vPlaneRecTriggerPos = << 2038.5756, 3224.4949, 43.8766 >>
		BREAK
		
		CASE EDrugLocation_TrailerParkOnHill
			myData.vCenterPos = << 873.6542, 2849.9204, 55.9454 >>
			myData.vDrugPickup = << 873.6542, 2849.9204, 55.9454 >>
			
			// TODO: need to update
			myData.vCutscenePos1 = << 871.9132, 2845.7163, 57.1240 >>   
			myData.vCutsceneOrien1 = << -0.9276, -0.0000, 5.2516 >>
			myData.vCutscenePos2 = << 866.4645, 2861.7485, 57.0600 >>        
			myData.vCutsceneOrien2 = << 5.3438, 0.0060, -75.1635 >>
		
			myData.vSmugHeliPos = << 872.2538, 2865.4375, 56.2123 >>    
			myData.fSmugHeliRot = 152.2005
			
			myData.vCarPlacementShootEnd = << 878.3112, 2841.1265, 55.8722 >>   
			myData.fCarPlacementShootEnd = 216.5979
		BREAK
		
		CASE EDrugLocation_SmugglersDropoff_01
			myData.vCenterPos = << 2463.5637, 3422.8289, 49.0819 >>
			myData.vDrugPickup = << 2463.5637, 3422.8289, 49.0819 >>
			myData.vPlaneRecTriggerPos = << 2473.7744, 3640.9138, 44.2181 >>
			
			myData.vCutscenePos1 = << 2438.7786, 3467.4934, 64.7144 >>
			myData.vCutsceneOrien1 = << 2.0660, 0.0000, -93.2163 >>
			myData.vCutscenePos2 = << 2439.3264, 3467.4629, 49.5392 >>
			myData.vCutsceneOrien2 = << 2.0660, -0.0000, -93.2163 >>
		
			// SITE CAN ONLY BE USED AS A PICKUP NOW!!!
			myData.vSmugHeliPos = << 2408.0703, 3477.8757, 48.1443 >> 
			myData.fSmugHeliRot = 221.7315
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 2857.7578, 4436.2798, 47.7903 >>
			myData.fSmugVehRot[0] = 96.8728
			
			myData.smugglersRecordings[0].bWaypointConfigured = TRUE
			myData.smugglersRecordings[0].recordingType = RECORDING_TYPE_CAR
			myData.smugglersRecordings[0].carrecWaypointName = "SmugglersDropOff_01"
			myData.smugglersRecordings[0].carCoords = << 2894.7368, 4450.1147, 47.7495 >>
			
//			myData.smugglersRecordings[0].bConfigured = FALSE
//			myData.smugglersRecordings[0].carrecName = "Smugglers04_01"
//			myData.smugglersRecordings[0].carrecNum = 101
//			myData.smugglersRecordings[0].carCoords = << 2894.7368, 4450.1147, 47.7495 >>
//			myData.smugglersRecordings[0].x =0.0019
//			myData.smugglersRecordings[0].y = 0.0097
//			myData.smugglersRecordings[0].j = 0.9159
//			myData.smugglersRecordings[0].k = 0.4012
//			myData.smugglersRecordings[0].fDefaultPlaybackSpeed = 0.50
//			myData.smugglersRecordings[0].fMultiplier = 1.75
			
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "SmugglersPlane"
			myData.smugglersRecordings[1].carrecNum = 102
			myData.smugglersRecordings[1].carCoords = << 2376.4980, 3162.7400, 73.3197 >>
			myData.smugglersRecordings[1].x = 0.0002
			myData.smugglersRecordings[1].y = 0.0434
			myData.smugglersRecordings[1].j = -0.0808
			myData.smugglersRecordings[1].k = 0.9958
			myData.smugglersRecordings[1].vTriggerDistance = <<200,200,10>>
			myData.smugglersRecordings[1].fDropTime = 260.0
		BREAK
		
		CASE EDrugLocation_TrailersAndBoat
			myData.vCenterPos = << 526.8232, 3123.0251, 39.5975 >>
			myData.vDrugPickup = << 526.8232, 3123.0251, 39.5975 >>
		
			myData.vCutscenePos1 = << 527.3740, 3118.4248, 40.8740 >>    
			myData.vCutsceneOrien1 = << -3.6327, 0.0000, -65.3691 >>  
			myData.vCutscenePos2 = << 530.2779, 3116.6016, 40.8915 >>   
			myData.vCutsceneOrien2 = << 2.2863, 0.0424, 81.3362 >>  

			myData.vSmugHeliPos = << 524.4958, 3115.1194, 40.000 >>    
			myData.fSmugHeliRot = 41.0465
			
			myData.vCarPlacementShootEnd = <<511.3625, 3120.4875, 39.7792>>    
			myData.fCarPlacementShootEnd = 248.7609 
		BREAK
		
		CASE EDrugLocation_RightRoadSide
			myData.vCenterPos = << 972.52, 4461.05, 50.81 >>
			myData.vDrugPickup = << 972.52, 4461.05, 50.81 >>
	
			// Cutscene cameras - setup for flare cutscene
			myData.vCutscenePos1 = << 975.6762, 4455.7803, 56.5023 >>
			myData.vCutsceneOrien1 = << -4.9392, 0.0000, -78.2685 >>
			myData.vCutscenePos2 = << 978.1075, 4464.2959, 55.4540 >>
			myData.vCutsceneOrien2 = << -23.0999, 0.0000, 101.9763 >>
		BREAK
		
		// Used in Hijack 6
		CASE EDrugLocation_OldLiquorStore
			myData.vCenterPos = << 944.6802, 3614.7341, 31.6225 >>
			myData.vDrugPickup = << 944.6802, 3614.7341, 31.6225 >>
			
			// For Hijack
			myData.vTruckPos = << 2921.1941, 3724.1877, 51.6753 >>
			myData.fTruckRot = 344.2805  
		BREAK
		
		CASE EDrugLocation_YellowJackInn
			myData.vCenterPos = << 2006.8500, 3069.6411, 46.0500 >>
			myData.vDrugPickup = << 2006.8500, 3069.6411, 46.0500 >>
		BREAK
		
		CASE EDrugLocation_DeadZone_01
	
			// SITE CAN ONLY BE USED AS A PICKUP NOW!!!
			myData.vSmugHeliPos = << 1463.1201, 3099.2747, 39.7173 >> 
			myData.fSmugHeliRot = 343.2928 
		
			myData.vCenterPos = << 1178.1967, 3271.2627, 38.2326 >>
			myData.vDrugPickup = << 1178.1967, 3271.2627, 38.2326 >>
			myData.sObjective = "DTRFKGR_12"
			myData.sConvo = "ARMS_REPT3"
			myData.gangType = GANG_HILLBILLIES			
			myData.fTimeAllowedToStayOnRoads = 32			
			
			myData.vPlaneRecTriggerPos = << 1433.0398, 3146.1035, 40.0637 >>
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 2592.2725, 4648.5601, 33.1437 >>    
			myData.fSmugVehRot[0] =  276.7005 
			
			myData.smugglersRecordings[0].bWaypointConfigured = TRUE
			myData.smugglersRecordings[0].recordingType = RECORDING_TYPE_CAR
			myData.smugglersRecordings[0].carrecWaypointName = "OldBoatWayPt"
			myData.smugglersRecordings[0].carCoords = << 2592.2725, 4648.5601, 33.1437 >>
		
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "NEW_TYPE1_08_PLANE"
			myData.smugglersRecordings[1].carrecNum = 101
			myData.smugglersRecordings[1].carCoords = << 462.8008, 3198.3857, 76.7479 >>
			myData.smugglersRecordings[1].x = 0.0056
			myData.smugglersRecordings[1].y = 0.0335
			myData.smugglersRecordings[1].j = -0.7024
			myData.smugglersRecordings[1].k = 0.7110
			myData.smugglersRecordings[1].vTriggerDistance = <<350,350,10>>
			myData.smugglersRecordings[1].fDropTime = 665
			
			myData.vPlaneRecTriggerPos = << 1178.1967, 3271.2627, 38.2326 >>
		BREAK
		
		CASE EDrugLocation_DeadZone_02
			
			// SITE CAN ONLY BE USED AS A PICKUP NOW!!!
			myData.vSmugHeliPos = << 1463.1201, 3099.2747, 40.2 >> 
			myData.fSmugHeliRot = 343.2928 
		
			myData.vCenterPos = <<1421.5698, 3139.9407, 39.9915>>
			myData.vDrugPickup = <<1421.5698, 3139.9407, 39.9915>>
			myData.sObjective = "DTRFKGR_6"
			myData.sConvo = "ARMS_REPT0"
			myData.gangType = GANG_MARABUNTA			
			myData.fTimeAllowedToStayOnRoads = 32			
			
			myData.vPlaneRecTriggerPos = << 1433.0398, 3146.1035, 40.0637 >>
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 2533.4993, 4590.1362, 32.3838 >>    
			myData.fSmugVehRot[0] =  137.0484
			
			myData.smugglersRecordings[0].bWaypointConfigured = TRUE
			myData.smugglersRecordings[0].recordingType = RECORDING_TYPE_CAR
			myData.smugglersRecordings[0].carrecWaypointName = "TrailersAndBoatWayPt"
			myData.smugglersRecordings[0].carCoords = << 2533.4993, 4590.1362, 32.3838 >>
			
//			myData.smugglersRecordings[0].bConfigured = TRUE
//			myData.smugglersRecordings[0].carrecName = "TrailersAndBoat"
//			myData.smugglersRecordings[0].carrecNum = 102
//			myData.smugglersRecordings[0].carCoords = << 2592.2725, 4648.5601, 33.1437 >>
//			myData.smugglersRecordings[0].x = 0.0115
//			myData.smugglersRecordings[0].y = -0.0020
//			myData.smugglersRecordings[0].j = 0.9322
//			myData.smugglersRecordings[0].k = 0.3617
//			myData.smugglersRecordings[0].fDefaultPlaybackSpeed = 0.65
//			myData.smugglersRecordings[0].fMultiplier = 1.5
			
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "SmugglersPlane"
			myData.smugglersRecordings[1].carrecNum = 101
			myData.smugglersRecordings[1].carCoords = << 675.2627, 2968.7585, 71.1935 >>
			myData.smugglersRecordings[1].x = 0.0182
			myData.smugglersRecordings[1].y = 0.0018
			myData.smugglersRecordings[1].j = -0.6328
			myData.smugglersRecordings[1].k = 0.7741
			myData.smugglersRecordings[1].vTriggerDistance = <<500,500,10>>
			myData.smugglersRecordings[1].fDropTime = 726.0
			
			myData.vPlaneRecTriggerPos = << 1428.30, 3143.96, 40.387 >>
		BREAK
		
		CASE EDrugLocation_RailHouse
			myData.vCenterPos = <<2920.0676, 4635.7495, 47.5452>>
			myData.vDrugPickup = <<2920.0676, 4635.7495, 47.5452>>
			myData.sObjective = "DTRFKGR_4"
			myData.sConvo = "ARMS_GR04a"
			myData.fTimeAllowedToStayOnRoads = 32
			
			myData.gangType = GANG_MARABUNTA		
			
			myData.vPlaneRecTriggerPos = << 1433.0398, 3146.1035, 40.0637 >>
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = <<3265.9482, 5004.2964, 21.2842>>   
			myData.fSmugVehRot[0] = 71.4713
			
			myData.smugglersRecordings[0].bWaypointConfigured = TRUE
			myData.smugglersRecordings[0].recordingType = RECORDING_TYPE_CAR
			myData.smugglersRecordings[0].carrecWaypointName = "RailHouse01WayPt"
			myData.smugglersRecordings[0].carCoords = <<3265.9482, 5004.2964, 21.2842>>
			
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "RailHousePlane"
			myData.smugglersRecordings[1].carrecNum = 101
			myData.smugglersRecordings[1].carCoords = << 3056.8899, 4057.3364, 103.7062 >>
			myData.smugglersRecordings[1].x = 0.0016
			myData.smugglersRecordings[1].y = -0.0023
			myData.smugglersRecordings[1].j = 0.1340
			myData.smugglersRecordings[1].k = 0.9910
			myData.smugglersRecordings[1].vTriggerDistance = <<250,250,10>>
			myData.smugglersRecordings[1].fDropTime = 548.0
			myData.vPlaneRecTriggerPos = << 2924.0862, 4638.2456, 47.5449 >>
		BREAK
		
		CASE EDrugLocation_OldHouseSmugglers
			myData.vCenterPos = << 269.1340, 3150.7397, 41.1195 >>
			myData.vDrugPickup = << 269.1340, 3150.7397, 41.1195 >>
		
			myData.vCutscenePos1 = << 283.6494, 3136.6440, 42.3597 >> 
			myData.vCutsceneOrien1 = << -3.8976, -0.0000, 45.0614 >>    
			myData.vCutscenePos2 = << 273.0124, 3173.1838, 42.1140 >> 
			myData.vCutsceneOrien2 = << 11.1353, -0.0101, 123.4599 >> 

			myData.vSmugHeliPos = << 268.3168, 3168.1250, 41.7703 >>
			myData.fSmugHeliRot = 329.5460
			
			myData.vCarPlacementShootEnd = << 282.4717, 3140.1780, 40.7880 >>   
			myData.fCarPlacementShootEnd = 93.0356
		BREAK
		
		CASE EDrugLocation_OldHouse
			myData.vCenterPos = << 279.3029, 3184.3894, 41.1172 >>
			myData.vDrugPickup = << 279.3029, 3184.3894, 41.1172 >>
		BREAK
		
		CASE EDrugLocation_WaterMiddle
			myData.vCenterPos = << 1350.97, 4064.97, 30.15 >>
			myData.vDrugPickup = << 1350.97, 4064.97, 30.15 >>
		BREAK
		
		CASE EDrugLocation_WaterFar
			myData.vCenterPos = << 533.2363, 3972.0984, 30.9251 >> 
			myData.vDrugPickup = << 533.2363, 3972.0984, 30.9251 >> 
		BREAK
		
		CASE EDrugLocation_WaterRiver
			myData.vCenterPos = << -429.9030, 4436.6499, 27.1854 >>
			myData.vDrugPickup = << -429.9030, 4436.6499, 27.1854 >>
		BREAK
		
		CASE EDrugLocation_MilitaryBase01
			myData.vCenterPos = << -1249.9149, 2557.7070, 16.3597 >>
			myData.vDrugPickup = << -1249.9149, 2557.7070, 16.3597 >>
			
			myData.vDestination[0] = << 2434.2896, 3982.0024, 35.7981 >> //<< 2501.3059, 4118.3350, 37.4626 >>
			myData.vDestination[1] = << 1633.5798, 3486.5481, 35.6313 >>
			myData.vDestination[2] = << 936.2628, 3538.1904, 33.0475 >>
			myData.vDestination[3] = << 225.5858, 3007.9485, 41.4752 >>
			myData.vDestination[4] = << 77.1040, 2856.5022, 51.9140 >>
			myData.vDestination[5] = << -374.4270, 2878.7983, 41.0882 >>
			myData.vDestination[6] = << -1441.4967, 2661.5413, 16.6505 >>
		BREAK
		
		CASE EDrugLocation_MilitaryBase02
			myData.vCenterPos = << 180.5068, 6577.1890, 30.8447 >>
			myData.vDrugPickup = << 180.5068, 6577.1890, 30.8447 >>
//			myData.vCenterPos = << 1566.2058, 6453.3848, 23.1387 >>  // For use if using waypoint recording
//			myData.vDrugPickup = << 1566.2058, 6453.3848, 23.1387 >>
			
			myData.vDestination[0] = << 2945.6475, 3901.0779, 51.2096 >>
			myData.vDestination[1] = << 2725.5100, 4750.3115, 43.4419 >>
			myData.vDestination[2] = << 2446.7400, 5704.0234, 44.2439 >>
			myData.vDestination[3] = << 2158.8369, 6068.1714, 50.8745 >>
			myData.vDestination[4] = << 1929.8635, 6332.4829, 42.1894 >>
			myData.vDestination[5] = << 1119.3063, 6494.8145, 20.0857 >>
			myData.vDestination[6] = << 180.5068, 6577.1890, 30.8447 >>
		BREAK
		
		CASE EDrugLocation_WindField
			//Moving windfarm..
//			myData.vCenterPos = << 2265.1848, 2041.2842, 127.8743 >>
//			myData.vDrugPickup =  << 2265.1848, 2041.2842, 127.8743 >>
			myData.vCenterPos = << 1559.3218, 2204.1628, 77.9422 >>
			myData.vDrugPickup =  << 1559.3218, 2204.1628, 77.9422 >>
		BREAK
		
		CASE EDrugLocation_SkethcyHouse
			myData.vCenterPos = << 2711.4468, 4145.7173, 42.8083 >>
			myData.vDrugPickup =  << 2711.4468, 4145.7173, 42.8083 >>
		BREAK
		CASE EDrugLocation_DeadZone_Air
			myData.vCenterPos = << 1436.9674, 3144.6394, 40.0988 >>
			myData.vDrugPickup =  << 1436.9674, 3144.6394, 40.0988 >>
		BREAK
		
		//far west light house.. cool locaiton  
		//3430.14, 5167.48, 21.50
		
		//house near light house
		//<< 3299.0474, 5165.3711, 17.2092 >>
		
		CASE EDrugLocation_LightHouse
			myData.vCenterPos = << 3284.1011, 5153.7642, 17.5866 >>
			myData.vDrugPickup =  << 3284.1011, 5153.7642, 17.5866 >>
		BREAK
		
		CASE EDrugLocation_DirtPit
			myData.vCenterPos = << 2953.7483, 2789.8262, 41.0514 >>
			myData.vDrugPickup =  << 2953.7483, 2789.8262, 41.0514 >>
		BREAK
		
		CASE EDrugLocation_LittleHoleHouse
			myData.vCenterPos = << 2211.1553, 5599.2720, 52.8715 >>
			myData.vDrugPickup =  << 2211.1553, 5599.2720, 52.8715 >>
		BREAK
		
		CASE EDrugLocation_FarEastDock01
			myData.vCenterPos = << 3811.8823, 4462.6157, 3.1544 >>
			myData.vDrugPickup =  << 3811.8823, 4462.6157, 3.1544 >>
		BREAK
		
		CASE EDrugLocation_FarEastDock02
			myData.vCenterPos = << 3927.0530, 4391.2734, 15.6169 >>
			myData.vDrugPickup =  << 3927.0530, 4391.2734, 15.6169 >>
		BREAK
		
		CASE EDrugLocation_NorthBeach // Bomb
			myData.vCenterPos = << 11.4536, 6856.3940, 11.9117 >>
			myData.vDrugPickup = << 11.4536, 6856.3940, 11.9117 >>
		BREAK
		
		CASE EDrugLocation_NewBeach // Bomb
			myData.vCenterPos = << -943.9194, 6176.1606, 3.0489 >>
			myData.vDrugPickup = << -943.9194, 6176.1606, 3.0489 >>
		BREAK
		
		CASE EDrugLocation_MountainCommune
			myData.vCenterPos = << -1150.0455, 4923.9932, 220.3399 >>
			myData.vDrugPickup = << -1150.0455, 4923.9932, 220.3399 >>
		BREAK
		
		CASE EDrugLocation_MountainCuldesac
			myData.vCenterPos = << -1632.0560, 4738.6890, 52.1812 >>
			myData.vDrugPickup = << -1632.0560, 4738.6890, 52.1812 >>
		BREAK
		
		CASE EDrugLocation_HillBarn
			myData.vCenterPos = << 166.3960, 2281.3225, 92.2100 >>
			myData.vDrugPickup = << 166.3960, 2281.3225, 92.2100 >>
		BREAK
		
		CASE EDrugLocation_LittleHouseOnTheHill
			myData.vCenterPos = << -288.5126, 2569.3750, 71.5438 >>
			myData.vDrugPickup = << -288.5126, 2569.3750, 71.5438 >>
		BREAK
		
		CASE EDrugLocation_FarmOnHill
			myData.vCenterPos = <<-68.9043, 1895.9185, 195.3272>>
			myData.vDrugPickup = <<-68.9043, 1895.9185, 195.3272>>
		BREAK
		
		CASE EDrugLocation_WaterTreatmentPlant
			myData.vCenterPos = <<1885.5386, 432.2420, 163.2558>>
			myData.vDrugPickup = <<1885.5386, 432.2420, 163.2558>>
		BREAK
		
		CASE EDrugLocation_BeachHead
			myData.vCenterPos = <<-2130.0000, 4574.2959, 1.6719>>
			myData.vDrugPickup = <<-2130.0000, 4574.2959, 1.6719>>
		BREAK
		
		CASE EDrugLocation_Clearing
			myData.vCenterPos = <<-312.8494, 3796.1387, 66.9603>>
			myData.vDrugPickup = <<-312.8494, 3796.1387, 66.9603>>
		BREAK
		//===========================================NEW SITES===========================================
		
		CASE EDrugLocation_NEW_TYPE1_01
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "NEW_TYPE1_01" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 2347.1802, 5510.7676, 51.2754 >>
			myData.carRecData[0].x = 0.0058
			myData.carRecData[0].y = -0.0200
			myData.carRecData[0].j = 0.4672
			myData.carRecData[0].k = 0.8839
			myData.carRecData[0].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "NEW_TYPE1_01" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 2366.1467, 5640.4375, 88.7714 >>
			myData.carRecData[1].x = 0.0794
			myData.carRecData[1].y = -0.0272
			myData.carRecData[1].j = 0.7464
			myData.carRecData[1].k = 0.6602
			myData.carRecData[1].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
		
			// Plane Drop Recording Data
			myData.carRecData[2].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "NEW_TYPE1_01_PLANE" 
			myData.carRecData[2].carrecNum = 101
			myData.carRecData[2].carCoords = << 3367.5864, 5277.9883, 130.1408 >>
			myData.carRecData[2].x = 0.0266
			myData.carRecData[2].y = 0.0840
			myData.carRecData[2].j = 0.6817
			myData.carRecData[2].k = 0.7263
			myData.carRecData[2].carrecVehicle = CUBAN800
			myData.carRecData[2].fDropTime = 1135
			
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = <<2225.5156, 5582.6646, 52.8192>>
			myData.vDrugPickup = <<2225.5156, 5582.6646, 52.8192>>
			myData.fTriggerPlaneDistance = 150
			myData.sObjective = "DTRFKGR_3"
			myData.sConvo = "ARMS_TYPE102"
			myData.fTimeAllowedToStayOnRoads = 22
			
			myData.gangType = GANG_MEXICANS
			
			myData.vAmbushAttackerPositions[0] = <<2216.7715, 5564.9932, 52.7060>>
			myData.vAmbushAttackerPositions[1] = <<2217.3667, 5595.7373, 53.0513>>
//			myData.vAmbushAttackerPositions[2] = <<2241.8323, 5638.9834, 57.4035>>
//			myData.vAmbushAttackerPositions[3] = <<2253.3931, 5551.3149, 46.5916>>			
			
			myData.vAmbushVehicleAttackerPositions[0] = <<2204.3311, 5565.9868, 52.8359>>
			myData.vAmbushVehicleAttackerPositions[1] = <<2216.0701, 5601.5278, 53.2750>>
		BREAK
		
		CASE EDrugLocation_NEW_TYPE1_02
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "NEW_TYPE1_02" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 3689.6467, 4616.0991, 18.2380 >>
			myData.carRecData[0].x = 0.0438
			myData.carRecData[0].y = 0.0453
			myData.carRecData[0].j = 0.9958
			myData.carRecData[0].k = -0.0665
			myData.carRecData[0].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "NEW_TYPE1_02" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 3767.3599, 4528.4971, 5.2465 >>
			myData.carRecData[1].x = 0.0233
			myData.carRecData[1].y = 0.0886
			myData.carRecData[1].j = 0.9359
			myData.carRecData[1].k = 0.3400
			myData.carRecData[1].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "NEW_TYPE1_02" 
			myData.carRecData[1].carrecNum = 103 
			myData.carRecData[1].carCoords = << 3589.6929, 4594.0610, 26.9929 >>
			myData.carRecData[1].x = 0.1094
			myData.carRecData[1].y = 0.1563
			myData.carRecData[1].j = 0.9815
			myData.carRecData[1].k = 0.0156
			myData.carRecData[1].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
		
			// Plane Drop Recording Data
			myData.carRecData[2].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "NEW_TYPE1_02_PLANE" 
			myData.carRecData[2].carrecNum = 101
			myData.carRecData[2].carCoords = << 3933.1338, 3757.1287, 105.0956 >>
			myData.carRecData[2].x = 0.0209
			myData.carRecData[2].y = 0.0207
			myData.carRecData[2].j = 0.1520
			myData.carRecData[2].k = 0.9879
			myData.carRecData[2].carrecVehicle = CUBAN800
			myData.carRecData[2].fDropTime = 745
			
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = << 3702.8269, 4511.5928, 20.1186 >>
			myData.vDrugPickup = << 3702.8269, 4511.5928, 20.1186 >>
			myData.fTriggerPlaneDistance = 175
			myData.sObjective = "DTRFKGR_13"
			myData.sConvo = "ARMS_TYPE107"
			myData.gangType = GANG_MEXICANS			
			myData.fTimeAllowedToStayOnRoads = 32
			
		BREAK
		
		CASE EDrugLocation_NEW_TYPE1_03
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "NEW_TYPE1_03" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 2639.0210, 3208.7131, 53.3696 >>
			myData.carRecData[0].x = 0.0760
			myData.carRecData[0].y = 0.0220
			myData.carRecData[0].j = -0.0104
			myData.carRecData[0].k = 0.9968
			myData.carRecData[0].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "NEW_TYPE1_03" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 2697.8630, 3346.0085, 56.7158 >>
			myData.carRecData[1].x = 0.0581
			myData.carRecData[1].y = -0.0074
			myData.carRecData[1].j = 0.9615
			myData.carRecData[1].k = 0.2686
			myData.carRecData[1].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[2].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "NEW_TYPE1_03" 
			myData.carRecData[2].carrecNum = 103 
			myData.carRecData[2].carCoords = << 2534.3384, 3230.4761, 51.7537 >>
			myData.carRecData[2].x = 0.0483
			myData.carRecData[2].y = -0.0277
			myData.carRecData[2].j = -0.4655
			myData.carRecData[2].k = 0.8833
			myData.carRecData[2].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
		
			// Plane Drop Recording Data
			myData.carRecData[3].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[3].bConfigured = TRUE
			myData.carRecData[3].carrecName = "NEW_TYPE1_03_PLANE" 
			myData.carRecData[3].carrecNum = 101
			myData.carRecData[3].carCoords = << 2343.5032, 2962.5986, 92.1772 >>
			myData.carRecData[3].x = 0.1689
			myData.carRecData[3].y = 0.4059
			myData.carRecData[3].j = -0.2324
			myData.carRecData[3].k = 0.8676
			myData.carRecData[3].carrecVehicle = CUBAN800
			myData.carRecData[3].fDropTime = 391
			
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = << 2623.6450, 3287.7129, 54.3001 >>
			myData.vDrugPickup = << 2623.6450, 3287.7129, 54.3001 >>
			myData.fTriggerPlaneDistance = 160
			myData.sObjective = "DTRFKGR_11"
			myData.sConvo = "ARMS_TYPE106"
			myData.gangType = GANG_HILLBILLIES			
			myData.fTimeAllowedToStayOnRoads = 32
		
		BREAK
		
		CASE EDrugLocation_NEW_TYPE1_04
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "NEW_TYPE1_04" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 1687.7415, 2958.5908, 57.2880 >>
			myData.carRecData[0].x = 0.0874
			myData.carRecData[0].y = -0.0283
			myData.carRecData[0].j = -0.4800
			myData.carRecData[0].k = 0.8724
			myData.carRecData[0].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "NEW_TYPE1_04" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 1735.8540, 3167.7427, 42.7488 >>
			myData.carRecData[1].x = 0.0047
			myData.carRecData[1].y = 0.0033
			myData.carRecData[1].j = 0.9995
			myData.carRecData[1].k = 0.0313
			myData.carRecData[1].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
		
			// Plane Drop Recording Data
			myData.carRecData[2].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "NEW_TYPE1_04_PLANE" 
			myData.carRecData[2].carrecNum = 101
			myData.carRecData[2].carCoords = << 873.8751, 3879.5564, 272.0572 >>
			myData.carRecData[2].x = -0.0166
			myData.carRecData[2].y = -0.1132
			myData.carRecData[2].j = 0.9184
			myData.carRecData[2].k = -0.3788
			myData.carRecData[2].carrecVehicle = CUBAN800
			myData.carRecData[2].fDropTime = 1178
			
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = << 1761.8362, 3050.5618, 60.8981 >>
			myData.vDrugPickup = << 1761.8362, 3050.5618, 60.8981 >>
			myData.fTriggerPlaneDistance = 190
			myData.sObjective = "DTRFKGR_9"
			myData.sConvo = "ARMS_TYPE105"
			myData.gangType = GANG_MARABUNTA			
			myData.fTimeAllowedToStayOnRoads = 15
			
			myData.vAmbushAttackerPositions[0] = <<1736.8912, 3019.6133, 61.4096>>
			myData.vAmbushAttackerPositions[1] = <<1790.9928, 3030.5703, 59.2148>>
//			myData.vAmbushAttackerPositions[2] = <<1719.7460, 3018.0637, 60.7347>>
//			myData.vAmbushAttackerPositions[3] = <<1757.9978, 3090.4695, 54.9940>>	

			myData.vAmbushVehicleAttackerPositions[0] = <<1724.0321, 3014.9343, 61.2488>>
			myData.vAmbushVehicleAttackerPositions[1] = <<1777.9575, 3023.6682, 61.7380>>
		BREAK
		
		CASE EDrugLocation_NEW_TYPE1_05
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "NEW_TYPE1_05" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 1968.2506, 3563.2922, 37.8475 >>
			myData.carRecData[0].x = -0.0183
			myData.carRecData[0].y = 0.0422
			myData.carRecData[0].j = 0.9225
			myData.carRecData[0].k = 0.3832
			myData.carRecData[0].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "NEW_TYPE1_05" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 1867.2853, 3437.4766, 39.7184 >>
			myData.carRecData[1].x = 0.0253
			myData.carRecData[1].y = 0.0153
			myData.carRecData[1].j = 0.7140
			myData.carRecData[1].k = -0.6995
			myData.carRecData[1].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[2].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "NEW_TYPE1_05" 
			myData.carRecData[2].carrecNum = 103 
			myData.carRecData[2].carCoords = << 1890.2610, 3391.3069, 40.7963 >>
			myData.carRecData[2].x = 0.0134
			myData.carRecData[2].y = 0.0213
			myData.carRecData[2].j = -0.4276
			myData.carRecData[2].k = 0.9036
			myData.carRecData[2].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[3].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[3].bConfigured = TRUE
			myData.carRecData[3].carrecName = "NEW_TYPE1_05" 
			myData.carRecData[3].carrecNum = 104
			myData.carRecData[3].carCoords = << 2067.0317, 3330.4736, 45.1739 >>
			myData.carRecData[3].x = 0.0121
			myData.carRecData[3].y = 0.0396
			myData.carRecData[3].j = 0.3730
			myData.carRecData[3].k = 0.9269
			myData.carRecData[3].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
		
			// Plane Drop Recording Data
			myData.carRecData[4].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[4].bConfigured = TRUE
			myData.carRecData[4].carrecName = "NEW_TYPE1_05_PLANE" 
			myData.carRecData[4].carrecNum = 101
			myData.carRecData[4].carCoords = << 1730.7928, 4305.9399, 149.1613 >>
			myData.carRecData[4].x = 0.0174
			myData.carRecData[4].y = -0.0833
			myData.carRecData[4].j = 0.9814
			myData.carRecData[4].k = -0.1720
			myData.carRecData[4].carrecVehicle = CUBAN800
			myData.carRecData[4].fDropTime = 853
			
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = <<1956.4425, 3438.9238, 40.7919>>
			myData.vDrugPickup = <<1956.4425, 3438.9238, 40.7919>>
			myData.fTriggerPlaneDistance = 150
			myData.sObjective = "DTRFKGR_5"
			myData.sConvo = "ARMS_TYPE103"
			myData.fTimeAllowedToStayOnRoads = 32
			
			myData.gangType = GANG_HILLBILLIES			
			
			myData.vAmbushAttackerPositions[0] = <<1911.5790, 3434.2429, 44.4280>>
			myData.vAmbushAttackerPositions[1] = <<1958.6998, 3486.6560, 40.0350>>
//			myData.vAmbushAttackerPositions[2] = <<1926.6465, 3482.9321, 45.7372>>
//			myData.vAmbushAttackerPositions[3] = <<1911.3834, 3420.5879, 42.1507>>

			myData.vAmbushVehicleAttackerPositions[0] = <<1906.1078, 3434.0496, 43.1727>>
			myData.vAmbushVehicleAttackerPositions[1] = <<1931.4058, 3503.6531, 40.4347>>
		BREAK
		
		CASE EDrugLocation_NEW_TYPE1_06
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "NEW_TYPE1_06" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 2747.7649, 2902.5305, 35.8669 >>
			myData.carRecData[0].x = -0.0031
			myData.carRecData[0].y = -0.0001
			myData.carRecData[0].j = 0.9773
			myData.carRecData[0].k = -0.2119
			myData.carRecData[0].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "NEW_TYPE1_06" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 2625.5078, 2814.4434, 33.1277 >>
			myData.carRecData[1].x = 0.0499
			myData.carRecData[1].y = -0.0287
			myData.carRecData[1].j = -0.3652
			myData.carRecData[1].k = 0.9292
			myData.carRecData[1].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
		
			//TODO figure out how to handle this aray
			myData.carRecData[2].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "NEW_TYPE1_06" 
			myData.carRecData[2].carrecNum = 103
			myData.carRecData[2].carCoords = << 2694.7964, 2796.2488, 34.4047 >>
			myData.carRecData[2].x = 0.1772
			myData.carRecData[2].y = -0.0431
			myData.carRecData[2].j = -0.1088
			myData.carRecData[2].k = 0.9772
			myData.carRecData[2].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			// Plane Drop Recording Data
			myData.carRecData[3].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[3].bConfigured = TRUE
			myData.carRecData[3].carrecName = "NEW_TYPE1_06_PLANE" 
			myData.carRecData[3].carrecNum = 101
			myData.carRecData[3].carCoords = << 1902.6300, 3026.4875, 286.5499 >>
			myData.carRecData[3].x = 0.0504
			myData.carRecData[3].y = -0.1138
			myData.carRecData[3].j = 0.8059
			myData.carRecData[3].k = -0.5788
			myData.carRecData[3].carrecVehicle = CUBAN800
			myData.carRecData[3].fDropTime = 753
			
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = << 2685.8694, 2844.3059, 38.8406 >>
			myData.vDrugPickup = << 2685.8694, 2844.3059, 38.8406 >>
			myData.fTriggerPlaneDistance = 125
			myData.sObjective = "DTRFKGR_19"
			myData.sConvo = "ARMS_TYPE110"
			myData.gangType = GANG_MEXICANS			
			myData.fTimeAllowedToStayOnRoads = 32
		
		BREAK
		
		CASE EDrugLocation_NEW_TYPE1_07
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "NEW_TYPE1_07" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 2935.6072, 4592.2144, 48.7006 >>
			myData.carRecData[0].x = -0.0031
			myData.carRecData[0].y = -0.0130
			myData.carRecData[0].j = 0.9318
			myData.carRecData[0].k = 0.3628
			myData.carRecData[0].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "NEW_TYPE1_07" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 2941.7087, 4464.8892, 46.4858 >>
			myData.carRecData[1].x = 0.0103
			myData.carRecData[1].y = 0.0254
			myData.carRecData[1].j = 0.8549
			myData.carRecData[1].k = 0.5181
			myData.carRecData[1].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
		
			//TODO figure out how to handle this aray
			myData.carRecData[2].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "NEW_TYPE1_07" 
			myData.carRecData[2].carrecNum = 103
			myData.carRecData[2].carCoords = << 2774.9207, 4468.4482, 47.5966 >>
			myData.carRecData[2].x = 0.0053
			myData.carRecData[2].y = -0.0205
			myData.carRecData[2].j = -0.3996
			myData.carRecData[2].k = 0.9164
			myData.carRecData[2].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			// Plane Drop Recording Data
			myData.carRecData[3].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[3].bConfigured = TRUE
			myData.carRecData[3].carrecName = "NEW_TYPE1_07_PLANE" 
			myData.carRecData[3].carrecNum = 101
			myData.carRecData[3].carCoords = << 3495.6008, 3895.2461, 284.7452 >>
			myData.carRecData[3].x = -0.0611
			myData.carRecData[3].y = 0.0094
			myData.carRecData[3].j = 0.4051
			myData.carRecData[3].k = 0.9122
			myData.carRecData[3].carrecVehicle = CUBAN800
			myData.carRecData[3].fDropTime = 825
			
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = <<2856.5022, 4485.0645, 47.3373>>
			myData.vDrugPickup = <<2856.5022, 4485.0645, 47.3373>>
			myData.fTriggerPlaneDistance = 140
			myData.sObjective = "DTRFKGR_7"
			myData.sConvo = "ARMS_TYPE104"
			myData.gangType = GANG_MEXICANS
			myData.fTimeAllowedToStayOnRoads = 32
			
			myData.vAmbushAttackerPositions[0] = <<2862.3186, 4465.2915, 47.4456>>
			myData.vAmbushAttackerPositions[1] = <<2889.9612, 4494.5981, 46.9656>>
//			myData.vAmbushAttackerPositions[2] = <<2884.3701, 4523.4536, 46.6400>>
//			myData.vAmbushAttackerPositions[3] = <<2845.1882, 4451.1724, 47.5126>>

			myData.vAmbushVehicleAttackerPositions[0] = <<2868.7737, 4467.2563, 47.3611>>
			myData.vAmbushVehicleAttackerPositions[1] = <<2883.8303, 4484.4087, 47.3394>>
		BREAK
		
		CASE EDrugLocation_NEW_TYPE1_08
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "NEW_TYPE1_08" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 1174.9436, 3191.4382, 38.8565 >>
			myData.carRecData[0].x = 0.0672
			myData.carRecData[0].y = 0.0398
			myData.carRecData[0].j = 0.0199
			myData.carRecData[0].k = 0.9967
			myData.carRecData[0].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "NEW_TYPE1_08" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 1117.5906, 3328.3115, 34.7940 >>
			myData.carRecData[1].x = 0.0001
			myData.carRecData[1].y = 0.0037
			myData.carRecData[1].j = 0.9944
			myData.carRecData[1].k = 0.1059
			myData.carRecData[1].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
		
			//TODO figure out how to handle this aray
			myData.carRecData[2].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "NEW_TYPE1_08" 
			myData.carRecData[2].carrecNum = 103
			myData.carRecData[2].carCoords = << 1357.3586, 3275.7349, 37.4399 >>
			myData.carRecData[2].x = -0.0329
			myData.carRecData[2].y = 0.0150
			myData.carRecData[2].j = 0.6826
			myData.carRecData[2].k = 0.7299
			myData.carRecData[2].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			// Plane Drop Recording Data
			myData.carRecData[3].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[3].bConfigured = TRUE
			myData.carRecData[3].carrecName = "NEW_TYPE1_08_PLANE" 
			myData.carRecData[3].carrecNum = 101
			myData.carRecData[3].carCoords = << 462.8008, 3198.3857, 76.7479 >>
			myData.carRecData[3].x = 0.0056
			myData.carRecData[3].y = 0.0335
			myData.carRecData[3].j = -0.7024
			myData.carRecData[3].k = 0.7110
			myData.carRecData[3].carrecVehicle = CUBAN800
			myData.carRecData[3].fDropTime = 665
			
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = << 1178.1967, 3271.2627, 38.2326 >>
			myData.vDrugPickup = << 1178.1967, 3271.2627, 38.2326 >>
			myData.fTriggerPlaneDistance = 175
			myData.sObjective = "DTRFKGR_17"
			myData.sConvo = "ARMS_TYPE109"
			myData.gangType = GANG_HILLBILLIES			
			myData.fTimeAllowedToStayOnRoads = 32
		
		BREAK
		
		CASE EDrugLocation_NEW_TYPE1_09
			//TODO figure out how to handle this aray
			myData.carRecData[0].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[0].bConfigured = TRUE
			myData.carRecData[0].carrecName = "NEW_TYPE1_09" 
			myData.carRecData[0].carrecNum = 101 
			myData.carRecData[0].carCoords = << 2626.5120, 4173.1689, 42.5261 >>
			myData.carRecData[0].x = -0.0250
			myData.carRecData[0].y = -0.0094
			myData.carRecData[0].j = -0.6106
			myData.carRecData[0].k = 0.7915
			myData.carRecData[0].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			//TODO figure out how to handle this aray
			myData.carRecData[1].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[1].bConfigured = TRUE
			myData.carRecData[1].carrecName = "NEW_TYPE1_09" 
			myData.carRecData[1].carrecNum = 102 
			myData.carRecData[1].carCoords = << 2749.9749, 4049.2852, 60.0000 >>
			myData.carRecData[1].x = -0.0153
			myData.carRecData[1].y = 0.0378
			myData.carRecData[1].j = 0.3854
			myData.carRecData[1].k = 0.9219
			myData.carRecData[1].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
		
			//TODO figure out how to handle this aray
			myData.carRecData[2].recordingType = RECORDING_TYPE_CAR
			myData.carRecData[2].bConfigured = TRUE
			myData.carRecData[2].carrecName = "NEW_TYPE1_09" 
			myData.carRecData[2].carrecNum = 103
			myData.carRecData[2].carCoords = << 2770.1384, 4202.6680, 46.3271 >>
			myData.carRecData[2].x = -0.0508
			myData.carRecData[2].y = -0.0362
			myData.carRecData[2].j = 0.8874
			myData.carRecData[2].k = 0.4568
			myData.carRecData[2].carrecVehicle = GET_AMBUSHER_VEHICLE_MODEL()
			
			// Plane Drop Recording Data
			myData.carRecData[3].recordingType = RECORDING_TYPE_PLANE
			myData.carRecData[3].bConfigured = TRUE
			myData.carRecData[3].carrecName = "NEW_TYPE1_09_PLANE" 
			myData.carRecData[3].carrecNum = 101
			myData.carRecData[3].carCoords = << 462.8008, 3198.3857, 76.7479 >>
			myData.carRecData[3].x = 0.0056
			myData.carRecData[3].y = 0.0335
			myData.carRecData[3].j = -0.7024
			myData.carRecData[3].k = 0.7110
			myData.carRecData[3].carrecVehicle = CUBAN800
			myData.carRecData[3].fDropTime = 860
			
			// Also used for flare cutscene... location to teleport the player
			myData.vCenterPos = << 2709.13037, 4146.81982, 42.7539 >>
			myData.vDrugPickup = << 2709.13037, 4146.81982, 42.7539 >>
			myData.fTriggerPlaneDistance = 350
			myData.sObjective = "DTRFKGR_1"
			myData.sConvo = "ARMS_GR01"
			myData.fTimeAllowedToStayOnRoads = 27
			myData.gangType = GANG_HILLBILLIES
			
			myData.vAmbushAttackerPositions[0] = <<2702.3206, 4120.5615, 42.8035>>
			myData.vAmbushAttackerPositions[1] = <<2719.2542, 4122.1514, 42.7810>>
//			myData.vAmbushAttackerPositions[2] = <<2739.7319, 4153.2905, 43.2742>>
//			myData.vAmbushAttackerPositions[3] = <<2728.6091, 4164.6045, 42.8043>>
			
			myData.vAmbushVehicleAttackerPositions[0] = <<2696.8213, 4123.5444, 42.7634>>
			myData.vAmbushVehicleAttackerPositions[1] = <<2745.1328, 4144.9722, 43.1873>>
		BREAK
		
		CASE EDrugLocation_NEW_TYPE2_01
			myData.vCenterPos = << 1206.8820, 3599.0930, 33.0185 >>
			myData.vDrugPickup = << 1206.8820, 3599.0930, 33.0185 >>
			myData.sObjective = "DTRFKGR_16"
			myData.sConvo = "ARMS_TYP2_16"
			myData.gangType = GANG_MEXICANS			
			myData.fTimeAllowedToStayOnRoads = 32
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 2639.7034, 3993.1482, 41.6388 >>   
			myData.fSmugVehRot[0] = 80.0101 
			
			myData.smugglersRecordings[0].bWaypointConfigured = TRUE
			myData.smugglersRecordings[0].recordingType = RECORDING_TYPE_CAR
			myData.smugglersRecordings[0].carrecWaypointName = "NEW_TYPE2_01"
			myData.smugglersRecordings[0].carCoords = << 2639.7034, 3993.1482, 41.6388 >>
			
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "NEW_TYPE2_01_PLANE"
			myData.smugglersRecordings[1].carrecNum = 101
			myData.smugglersRecordings[1].carCoords = << 28.4625, 3715.9260, 230.1408 >>
			myData.smugglersRecordings[1].x = 0.0491
			myData.smugglersRecordings[1].y = -0.1637
			myData.smugglersRecordings[1].j = 0.7293
			myData.smugglersRecordings[1].k = -0.6625
			myData.smugglersRecordings[1].vTriggerDistance = <<700,700,10>>
			myData.smugglersRecordings[1].fDropTime = 1135
			myData.vPlaneRecTriggerPos = << 1206.8820, 3599.0930, 33.0185 >>
		BREAK
		
		CASE EDrugLocation_NEW_TYPE2_02
			myData.vCenterPos = << 2181.7673, 3367.6118, 44.4324 >>
			myData.vDrugPickup = << 2181.7673, 3367.6118, 44.4324 >>
			myData.sObjective = "DTRFKGR_18"
			myData.sConvo = "ARMS_TYP2_18"
			myData.gangType = GANG_HILLBILLIES			
			myData.fTimeAllowedToStayOnRoads = 32
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 2849.4541, 3579.7410, 52.3279 >>    
			myData.fSmugVehRot[0] = 155.8420
			
			myData.smugglersRecordings[0].bWaypointConfigured = TRUE
			myData.smugglersRecordings[0].recordingType = RECORDING_TYPE_CAR
			myData.smugglersRecordings[0].carrecWaypointName = "NEW_TYPE2_02"
			myData.smugglersRecordings[0].carCoords = << 2849.4541, 3579.7410, 52.3279 >>
			
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "NEW_TYPE2_02_PLANE"
			myData.smugglersRecordings[1].carrecNum = 101
			myData.smugglersRecordings[1].carCoords = << 1693.5651, 3099.6008, 85.4015 >>
			myData.smugglersRecordings[1].x = 0.0143
			myData.smugglersRecordings[1].y =  0.0283
			myData.smugglersRecordings[1].j = -0.4536
			myData.smugglersRecordings[1].k = 0.8906
			myData.smugglersRecordings[1].vTriggerDistance = <<175,175,10>>
			myData.smugglersRecordings[1].fDropTime = 510
			myData.vPlaneRecTriggerPos = << 2181.7673, 3367.6118, 44.4324 >>
		BREAK
		
		CASE EDrugLocation_NEW_TYPE2_03
			myData.vCenterPos = << 1690.2025, 3166.5276, 44.8952 >>
			myData.vDrugPickup = << 1690.2025, 3166.5276, 44.8952 >>
			myData.sObjective = "DTRFKGR_20"
			myData.sConvo = "ARMS_TYP2_20"
			myData.gangType = GANG_MARABUNTA			
			myData.fTimeAllowedToStayOnRoads = 32
			
			// For Smuggler's - offset from drug pickup location
			myData.vSmugVehPos[0] = << 2820.6567, 3520.2715, 53.4036 >>      
			myData.fSmugVehRot[0] = 159.6483
			
			myData.smugglersRecordings[0].bWaypointConfigured = TRUE
			myData.smugglersRecordings[0].recordingType = RECORDING_TYPE_CAR
			myData.smugglersRecordings[0].carrecWaypointName = "NEW_TYPE2_03"
			myData.smugglersRecordings[0].carCoords = << 2673.6875, 3429.5681, 54.8667 >>
			
			myData.smugglersRecordings[1].bConfigured = TRUE
			myData.smugglersRecordings[1].recordingType = RECORDING_TYPE_PLANE
			myData.smugglersRecordings[1].carrecVehicle = CUBAN800
			myData.smugglersRecordings[1].carrecName = "NEW_TYPE2_03_PLANE"
			myData.smugglersRecordings[1].carrecNum = 101
			myData.smugglersRecordings[1].carCoords = << 684.4690, 2863.9463, 141.8009 >>
			myData.smugglersRecordings[1].x = -0.1047
			myData.smugglersRecordings[1].y =  0.0170
			myData.smugglersRecordings[1].j = -0.6939
			myData.smugglersRecordings[1].k = 0.7122
			myData.smugglersRecordings[1].vTriggerDistance = <<350,350,10>>
			myData.smugglersRecordings[1].fDropTime = 1010
			myData.vPlaneRecTriggerPos = << 1690.2025, 3166.5276, 44.8952 >>
		BREAK
		
		//cool dirt location
		//Camera Position = << 2945.7732, 2777.7791, 39.5467 >>
		
	ENDSWITCH
	RETURN TRUE
ENDFUNC

//
//Ground Transport - Data needed for Air Drop
//
//myData.vFlarePos //The players position for this cutscene
//myData.vDrugPickup // The car goes here

//myData.vCutscenePos1  //the posiiton the camera is looking at while we set up the positions
//myData.vCutsceneOrien1 
//myData.vCutscenePos2  //looking at the player place the flare
//myData.vCutsceneOrien2
//myData.vCutscenePos3  //over head posiiton watching the plane drop the package
//myData.vCutsceneOrien3 
			

FUNC BOOL POPULATE_DATA(LOCATION_DATA& myLocationData, INT closeestIdx)
	//defualt to the airport for return
	POPULATE_DRUG_LOCATION(myLocationData.endData[0], EDrugLocation_MainAirport)
	
	SWITCH INT_TO_ENUM(ELocation, closeestIdx)   
		CASE ELocation_GroundLocation
		
			myLocationData.mnGenericBadGuy = A_M_Y_GENSTREET_01
		
//			myLocationData.bAcquireVehicle = TRUE

			POPULATE_DRUG_LOCATION(myLocationData.dropData[0], EDrugLocation_TrailerPark)
			POPULATE_DRUG_LOCATION(myLocationData.endData[0], EDrugLocation_MainAirportGround)
			POPULATE_DRUG_LOCATION(myLocationData.endData[1], EDrugLocation_LiquorJrMarket)
			
			myLocationData.bSupportsPlane = TRUE
			myLocationData.bSupportsAmbush = TRUE
			myLocationDAta.bSupportsChase = TRUE
			
			myLocationData.vPlayerStart = << 1758.9991, 3291.6294, 40.1563 >> //<< -866.7942, 5455.2827, 33.5698 >>
			myLocationData.fPlayerRot = 290.3184
			myLocationData.iNumBadGuys = 8
			myLocationData.iNumBadGuysPerCar = 2
			
			//
			//Known vehicle location Data
			//  
			myLocationData.vKnownVehicleLoc = << 1767.5173, 3291.9792, 40.2355 >>
			myLocationData.vKnownVehicleHead = 259.6187
			
			// Needed for Hijack Mode
			myLocationData.vehicleName = HAULER 
			myLocationData.genericVehicle = TRAILERS2
			myLocationData.mnEscortVehicle[0] = BATI
			myLocationData.mnEscortVehicle[1] = BATI
			myLocationData.mnEscortVehicle[2] = BATI
			myLocationData.mnEscortVehicle[3] = BATI
			
			// Static Chase
//			myLocationData.mnPoliceCar = POLICE
			myLocationData.mnPolicePed = S_F_Y_COP_01

			//myLocationData.vHelperCarPos = << 1767.5173, 3291.9792, 40.2355 >>
			//myLocationData.fHelperCarHeading = 86.55
			myLocationData.fPickupTime = 120000 
			
			// Car chasers info
			myLocationData.iNumWaves = 1
			myLocationData.iNumCarChaserCarsPerWave = 1
			myLocationData.iNumCarChaserCars = 2
			myLocationData.iNumPedsPerCar = 2
			myLocationData.PedSeat[0] = VS_DRIVER
			myLocationData.PedSeat[1] = VS_FRONT_RIGHT
			myLocationData.PedSeat[2] = VS_DRIVER
			myLocationData.PedSeat[3] = VS_FRONT_RIGHT
			myLocationData.PedSeat[4] = VS_DRIVER
			myLocationData.PedSeat[5] = VS_FRONT_RIGHT
			myLocationData.PedSeat[6] = VS_DRIVER
			myLocationData.PedSeat[7] = VS_FRONT_RIGHT
			myLocationData.mnCarChasers[0] = GET_AMBUSHER_VEHICLE_MODEL()
			myLocationData.mnCarChasers[1] = GET_AMBUSHER_VEHICLE_MODEL()
			myLocationData.mnCarChasers[2] = GET_AMBUSHER_VEHICLE_MODEL()
			myLocationData.mnCarChasers[3] = GET_AMBUSHER_VEHICLE_MODEL()
			myLocationData.mnPedChasers[0] = A_M_Y_GENSTREET_01
			myLocationData.mnPedChasers[1] = G_M_Y_StrPunk_01
			myLocationData.mnPedChasers[2] = A_M_Y_GENSTREET_01
			myLocationData.mnPedChasers[3] = G_M_Y_StrPunk_01
//			myLocationData.fTimeToDelay = fDelayTime
			
			// Smuggler's Mode Info
			myLocationData.iNumSmugVehicles = 1
			myLocationData.iNumSmugPedsPerVehicle = 2
			myLocationData.mnSmugglersVehicles[0] = REBEL
			myLocationData.mnSmugglersHeli = BUZZARD
			
			myLocationData.mnLoadedCargo = Prop_Drop_ArmsCrate_01b
			
			myLocationData.mnHijackBomb = PROP_GASCYL_01A
			
			RETURN TRUE
		BREAK	
		
		CASE ELocation_Water
		
			
			POPULATE_DRUG_LOCATION(myLocationData.dropData[0], 	EDrugLocation_WestLakeMiddle)
			//POPULATE_DRUG_LOCATION(myLocationData.endData, 	EDrugLocation_WestWater)
			
			myLocationData.bSupportsPlane = TRUE
			myLocationData.bSupportsAmbush = TRUE
			myLocationDAta.bSupportsChase = TRUE 
			
			myLocationData.vPlayerStart = << 1847.2817, 4550.0474, 28.8452 >>
			myLocationData.fPlayerRot = 185.4917
			myLocationData.iNumBadGuys = 4
			//
			//Known vehicle location Data
			//  
			myLocationData.vKnownVehicleLoc = << 1847.2817, 4550.0474, 28.8452 >>
			myLocationData.vKnownVehicleHead = 185.4917
			myLocationData.vehicleName = squalo //PATRIOT //MESA
			
			//
			//Drop Off Data
			//
//			myLocationData.iNumDrops = 1
			myLocationData.fPickupTime = 120000 
			
			// Car chasers info
			myLocationData.iNumWaves = 2
			myLocationData.iNumCarChaserCarsPerWave = 1
			myLocationData.iNumCarChaserCars = 2
			myLocationData.iNumPedsPerCar = 2
			myLocationData.PedSeat[0] = VS_DRIVER
			myLocationData.PedSeat[1] = VS_FRONT_RIGHT
			myLocationData.PedSeat[2] = VS_BACK_RIGHT
			myLocationData.mnCarChasers[0] = DINGHY
			myLocationData.mnCarChasers[1] = SEASHARK
			myLocationData.mnPedChasers[0] = A_M_Y_GENSTREET_01
			myLocationData.mnPedChasers[1] = G_M_Y_StrPunk_01
			myLocationData.mnPedChasers[2] = G_M_Y_StrPunk_01
//			myLocationData.fTimeToDelay = fDelayTime
			
			// Smuggler's Mode Info --Used here for more chaser data
			myLocationData.iNumSmugVehicles = 2
			myLocationData.iNumSmugPedsPerVehicle = 1
			myLocationData.mnSmugglersVehicles[0] = SEASHARK
//			myLocationData.mnSmugglersPeds[0] = A_M_Y_GENSTREET_01
//			myLocationData.mnSmugglersPeds[1] = G_M_Y_StrPunk_01
			myLocationData.mnSmugglersHeli = POLMAV
			
			RETURN TRUE
		BREAK	
		
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//This vector is for the final only..
//PROC POPULATE_FINALE(PLANE_DATA& myData)     
//
//	myData.vRunwayTargets[0] = << 1568.2661, 3226.8794, 39.4840 >>
//	myData.fRunwayTargets[0] = 82.9402
//	myData.vRunwayTargets[1] = << 1544.9666, 3220.6628, 39.4859 >>
//	myData.fRunwayTargets[1] = 85.9402
//	myData.vRunwayTargets[2] = << 1518.8503, 3215.1252, 39.5010 >>
//	myData.fRunwayTargets[2] = 130.2702
//	myData.vRunwayTargets[3] = << 1491.3268, 3206.3945, 39.4858 >>
//	myData.fRunwayTargets[3] = 129.9732
//	
//	myData.vRunwayTargets[4] = << 1829.2856, 3233.4890, 43.0507 >>
//	myData.fRunwayTargets[4] = 358.4576 
//	myData.vRunwayTargets[5] = << 1806.7382, 3235.8108, 42.1702 >>
//	myData.fRunwayTargets[5] = 6.5631
//	myData.vRunwayTargets[6] = << 1787.3132, 3235.4229, 41.5092 >>
//	myData.fRunwayTargets[6] = 0.4536
//	myData.vRunwayTargets[7] = << 1757.9932, 3237.2512, 41.0381 >>
//	myData.fRunwayTargets[7] = 338.9639
//
//	
//	myData.vRunwayTargets[8] = << 1487.2351, 3072.2769, 39.5341 >>
//	myData.fRunwayTargets[8] = 165.1521
//	myData.vRunwayTargets[9] = << 1470.8726, 3054.8704, 39.5341 >>
//	myData.fRunwayTargets[9] =  163.1142 
//	myData.vRunwayTargets[10] = << 1453.5428, 3038.1406, 39.5341 >>
//	myData.fRunwayTargets[10] =  167.1521 
//	myData.vRunwayTargets[11] = << 1435.8273, 3019.2769, 39.5341 >>
//	myData.fRunwayTargets[11] = 164.4727
//
//ENDPROC

//This vector is for the final only..
PROC POPULATE_FINALE(PLANE_DATA& myData)     

	myData.vRunwayTargets[0] = << 1580.01807, 3229.95532, 39.48187 >>
	myData.fRunwayTargets[0] = 82.95
	myData.vRunwayTargets[1] = << 1546.08691, 3220.24463, 39.47596 >>
	myData.fRunwayTargets[1] = 85.9402
	myData.vRunwayTargets[2] = << 1510.83594, 3210.79614, 39.47606 >>
	myData.fRunwayTargets[2] = 80.15
	
	myData.vRunwayTargets[3] = << 1811.73596, 3232.81519, 42.38194 >>
	myData.fRunwayTargets[3] = 358.4576 
	myData.vRunwayTargets[4] = << 1785.83765, 3233.18237, 41.53580 >>
	myData.fRunwayTargets[4] = 6.5631
	myData.vRunwayTargets[5] = << 1756.59998, 3233.45361, 41.13475 >>
	myData.fRunwayTargets[5] = 0.4536
	
	myData.vRunwayTargets[6] = << 1499.49829, 3083.61621, 39.53300 >>
	myData.fRunwayTargets[6] = 165.15
	myData.vRunwayTargets[7] = << 1470.8726, 3054.8704, 39.5341 >>
	myData.fRunwayTargets[7] =  163.1142 
	myData.vRunwayTargets[8] = << 1442.11755, 3025.79370, 39.53325 >>
	myData.fRunwayTargets[8] =  167.15
	
ENDPROC

//This vector is for the final only..
PROC POPULATE_FINALE_REPEATABLE(PLANE_DATA& myData)     
	
	myData.vRunwayTargets[0] = << 811.4817, 3535.0896, 33.2207 >>
	myData.fRunwayTargets[0] = 92.2979 
	myData.vRunwayTargets[1] = << 830.1038, 3535.8198, 33.2073 >>  
	myData.fRunwayTargets[1] = 92.8474
	myData.vRunwayTargets[2] = << 846.6023, 3536.9529, 33.1838 >>
	myData.fRunwayTargets[2] = 94.2316 
	myData.vRunwayTargets[3] = << 860.8500, 3538.0269, 33.1552 >> 
	myData.fRunwayTargets[3] = 94.3862
	
	myData.vRunwayTargets[4] = << 1785.5818, 3555.7080, 34.7017 >> 
	myData.fRunwayTargets[4] = 298.9814
	myData.vRunwayTargets[5] = << 1765.0183, 3544.6660, 34.9090 >> 
	myData.fRunwayTargets[5] = 303.9579
	myData.vRunwayTargets[6] = << 1747.5751, 3534.3225, 35.0587 >> 
	myData.fRunwayTargets[6] = 300.1915
	myData.vRunwayTargets[7] = << 1728.2397, 3523.5674, 35.2582 >> 
	myData.fRunwayTargets[7] = 299.9568 
	
	myData.vRunwayTargets[8] = << 987.4857, 2689.4407, 38.8479 >> 
	myData.fRunwayTargets[8] = 266.2740
//	myData.vRunwayTargets[9] = << 959.5693, 2691.0061, 39.2238 >> 
//	myData.fRunwayTargets[9] = 266.1181
//	myData.vRunwayTargets[10] = << 934.5667, 2692.6765, 39.5701 >> 
//	myData.fRunwayTargets[10] = 266.4420
//	myData.vRunwayTargets[11] = << 910.7165, 2694.1450, 39.7999 >> 
//	myData.fRunwayTargets[11] = 266.7428

ENDPROC


PROC POPULATE_PLANE_DATA(PLANE_DATA& myData, INT closeestIdx)
	//defualt to the airport for return
	POPULATE_DRUG_LOCATION(myData.endData, EDrugLocation_MainAirport)
	
	SWITCH INT_TO_ENUM(ELocation, closeestIdx)   
	 
		CASE ELocation_AirLocation
			
			myData.fmaxHeight = LOW_ALTITUDE_VAL
			myData.fAltTimeLimit = 14
			
			//myData.bSupportsPlane = TRUE
			myData.vPlayerStart = << 2136.6531, 4817.5376, 40.1959 >> 
			myData.fPlayerRot = 208.41
		
			myData.sPlaneName = CUBAN800
			myData.vPlanePos = << 2142.5640, 4810.4976, 40.1925 >>
			myData.fPlaneAngle = 111.27

			//This is data that should be set variation by variation
			//myData.iNumDrops = 4
			//myData.iNumTotalCargo = 4
			
			POPULATE_DRUG_LOCATION(myData.dropData[0], EDrugLocation_TrailerPark)
			POPULATE_DRUG_LOCATION(myData.dropData[1], EDrugLocation_CountryHouse)
			POPULATE_DRUG_LOCATION(myData.dropData[2], EDrugLocation_PlateauHouse)
			POPULATE_DRUG_LOCATION(myData.dropData[3], EDrugLocation_OceanExit)
			
//			//Bomb target cars
//			myData.vCarSpawnPos[0] = << 1605.3579, 2822.5396, 38.0192 >>
//			myData.vCarSpawnPos[1] = << 1498.2870, 2081.6301, 93.8740 >>
//			myData.vCarSpawnPos[2] = << 1382.2533, 4497.6558, 53.2176 >>
//			myData.vCarSpawnPos[3] = << 815.3937, 4263.1548, 54.4693 >>
			
			myData.targetCarNames[0] = BARRACKS
			myData.targetCarNames[1] = BARRACKS
			myData.targetCarNames[2] = BARRACKS
			myData.targetCarNames[3] = BARRACKS
			
			myData.stationaryCarNames[0] = BALLER
			myData.stationaryCarNames[1] = BALLER
			myData.stationaryCarNames[2] = BALLER
			myData.stationaryCarNames[3] = BALLER
			
			myData.targetCarDriverNames[0] = A_M_Y_GENSTREET_01
			myData.targetCarDriverNames[1] = A_M_Y_GENSTREET_01
			myData.targetCarDriverNames[2] = A_M_Y_GENSTREET_01
			myData.targetCarDriverNames[3] = A_M_Y_GENSTREET_01
			
		BREAK
		
		DEFAULT
			myData.sPlaneName = CUBAN800
			myData.vPlanePos = << 1698.3767, 3251.0305, 39.9022 >>
			myData.fPlaneAngle = 113.0270 
		BREAK
	ENDSWITCH
ENDPROC
