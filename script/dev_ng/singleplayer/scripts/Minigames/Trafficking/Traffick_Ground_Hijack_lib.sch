USING "Drug_Trafficking_Data.sch"
USING "Drug_Trafficking_core.sch"
USING "commands_water.sch"

ENUM HIJACK_STATE
	HIJACK_STATE_01 = 0, 
	HIJACK_STATE_02,
	HIJACK_STATE_03,
	HIJACK_STATE_04,
	HIJACK_STATE_05,
	HIJACK_STATE_06,
	HIJACK_STATE_07
ENDENUM

ENUM DROP_EXPLOSIVE_STATE
	DROP_EXPLOSIVE_STATE_01 = 0,
	DROP_EXPLOSIVE_STATE_02,
	DROP_EXPLOSIVE_STATE_03,
	DROP_EXPLOSIVE_STATE_04
ENDENUM

ENUM HIJACK_PRINTS
	HIJACK_PRINTS_01 = 0,
	HIJACK_PRINTS_02,
	HIJACK_PRINTS_03,
	HIJACK_PRINTS_04
ENDENUM

ENUM HIJACK_KILL_CAM
	HIJACK_KILL_CAM_01 = 0,
	HIJACK_KILL_CAM_02
ENDENUM

// Hijack
CONST_INT EXPLOSIVE_TIMER 3
CONST_INT HIJACK_TOO_CLOSE_TIME 3000
CONST_INT MIN_DISTANCE_TO_ESCORT_PEDS 100
CONST_FLOAT DISTANCE_TO_LOSE_HIJACK_ESCORTS 350.0
CONST_FLOAT HIJACK_SUCCESSFUL_DROP_DIST 10.0
CONST_FLOAT HIJACK_SUCCESSFUL_DROP_DIST_ESCORTS 10.0
CONST_FLOAT EXP_HELP_PRINT_DISTANCE 100.0
CONST_FLOAT HIJACK_AGGRO_DISTANCE_EXPLOSIVE 50.0
CONST_FLOAT HIJACK_AGGRO_DISTANCE 10.0
CONST_FLOAT MINIMUM_SPEED_FOR_DROPPING_CANISTER 5.0
CONST_FLOAT DISTANCE_ALLOWED_FROM_ROAD_NODE 6.0

BOOL bPrintObjectiveBlowOpenDoors = FALSE
BOOL bOscarTellAboutC4 = FALSE
BOOL bHelpTellAboutC4 = FALSE
BOOL bPrintExtraHelp = FALSE
BOOL bGoodOri = FALSE
BOOL bResetRotation = FALSE
BOOL bRunExplosionCam = FALSE
BOOL bGrabbedStartIndex = FALSE
BOOL bTaskedTransportAI = FALSE
BOOL bFreezedTruck = FALSE
BOOL bToggleCam = FALSE
BOOL bFrozeCanister = FALSE

FLOAT fTransportHeading

INT hijackEffectStages = 0
INT iCanisterToUnfreeze = 0 

OBJECT_INDEX tempCanister[MAX_NUMBER_EXPLOSIVES]

structTimer tEffectsTimer

MODEL_NAMES mnScriptRamp = Prop_GasCyl_Ramp_01
MODEL_NAMES mnScriptRampDoor = Prop_GasCyl_Ramp_Door_01

FUNC BOOL UPDATE_HIJACK_PRINTS(HIJACK_ARGS& hijackArgs, HIJACK_PRINTS& hijackPrintStates, VEHICLE_INDEX& myVehicle, EXPLOSIVE_PACKAGE_ARGS& expPackageArgs)
	VECTOR vPlayerPos
	VECTOR vTransportPos
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
	
			IF hijackPrintStates < HIJACK_PRINTS_02
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(hijackArgs.vehTruck)
					vTransportPos = GET_ENTITY_COORDS(hijackArgs.vehTruck)
				ENDIF
			ENDIF
			
				
			SWITCH hijackPrintStates
				CASE HIJACK_PRINTS_01
					IF expPackageArgs.iNumExplosivePackageDropped < MAX_NUMBER_EXPLOSIVES
						IF VDIST(vPlayerPos, vTransportPos) < EXP_HELP_PRINT_DISTANCE
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_NOW("DTRFKGR_18", 3500, 1)
								hijackPrintStates = HIJACK_PRINTS_02
							ENDIF
						ENDIF
					ENDIF
				BREAK
				//--------------------------------------------------------------------------------------------------------------------------------------
				CASE HIJACK_PRINTS_02
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						IF DT_IS_CAR_IN_FRONT_OF_CAR(myVehicle, hijackArgs.vehTruck)
							IF expPackageArgs.iNumExplosivePackageDropped < MAX_NUMBER_EXPLOSIVES
								PRINT_HELP("DTRFKGR_HELP_11")
								hijackPrintStates = HIJACK_PRINTS_03
							ENDIF
						ENDIF
					ENDIF
				BREAK
				//--------------------------------------------------------------------------------------------------------------------------------------
				CASE HIJACK_PRINTS_03
					// If the player hasn't dropped one explosive canister, remind them how to do it.
					IF hijackArgs.bDroppedOnePackage
						RETURN TRUE
					ELSE
		//				DEBUG_PRINT("GOING BACK TO HIJACK_PRINTS_02 BECAUSE hijackArgs.bDroppedOnePackage IS FALSE")
						hijackPrintStates = HIJACK_PRINTS_02
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ADD_EXPLOSIVE_PACKAGE_TO_VEHICLE(LOCATION_DATA& myLocData, EXPLOSIVE_PACKAGE_ARGS& expPackageArgs, VEHICLE_INDEX vehMyVehicle, ARGS & launchArgs)
	
	INT idx
	FLOAT offsetStart = -2.500
	FLOAT offsetHeightStart = 0.65
	
	expPackageArgs.vExpPackageRot = <<0, -90, 0>>
	
	REPEAT MAX_NUMBER_EXPLOSIVES idx
		IF DOES_ENTITY_EXIST(vehMyVehicle) AND IS_VEHICLE_DRIVEABLE(vehMyVehicle)
			IF DOES_ENTITY_EXIST(launchArgs.miscObjects[idx])
				expPackageArgs.oExpPackage[idx] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(launchArgs.miscObjects[idx])
				SET_ENTITY_AS_MISSION_ENTITY(expPackageArgs.oExpPackage[idx])
				SET_ENTITY_LOD_DIST(expPackageArgs.oExpPackage[idx], 500)
				PRINTLN("GRABBING CANISTERS FROM LAUNCHER")
			ELSE
				expPackageArgs.oExpPackage[idx] = CREATE_OBJECT(myLocData.mnHijackBomb, GET_ENTITY_COORDS(vehMyVehicle))
				SET_ENTITY_RECORDS_COLLISIONS(expPackageArgs.oExpPackage[idx], TRUE)
				ATTACH_ENTITY_TO_ENTITY(expPackageArgs.oExpPackage[idx], vehMyVehicle, 0, <<0, (offsetStart + (idx*0.410)), (offsetHeightStart + (idx*0.120))>>, expPackageArgs.vExpPackageRot, TRUE)
				SET_ENTITY_LOD_DIST(expPackageArgs.oExpPackage[idx], 500)
				PRINTLN("CREATING CANISTERS IN GROUND SCRIPT")
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(vehMyVehicle) AND IS_VEHICLE_DRIVEABLE(vehMyVehicle)
		IF DOES_ENTITY_EXIST(launchArgs.miscObjects[4])
			expPackageArgs.oRamp = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(launchArgs.miscObjects[4])
			SET_ENTITY_AS_MISSION_ENTITY(expPackageArgs.oRamp)
			SET_ENTITY_RECORDS_COLLISIONS(expPackageArgs.oRamp, FALSE)
			PRINTLN("GRABBING RAMP FROM LAUNCHER SCRIPT")
		ELSE
			expPackageArgs.oRamp = CREATE_OBJECT(mnScriptRamp, GET_ENTITY_COORDS(vehMyVehicle))
			ATTACH_ENTITY_TO_ENTITY(expPackageArgs.oRamp, vehMyVehicle, 0, <<-0.450, -1.650, 0.700>>, <<0,0,0>>, TRUE)
			SET_ENTITY_RECORDS_COLLISIONS(expPackageArgs.oRamp, FALSE)
			PRINTLN("CREATING RAMP IN GROUND SCRIPT")
		ENDIF
		IF DOES_ENTITY_EXIST(launchArgs.miscObjects[5])
			expPackageArgs.oRampDoor = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(launchArgs.miscObjects[5])
			SET_ENTITY_AS_MISSION_ENTITY(expPackageArgs.oRampDoor)
			SET_ENTITY_RECORDS_COLLISIONS(expPackageArgs.oRampDoor, FALSE)
		ELSE
			expPackageArgs.oRampDoor = CREATE_OBJECT(mnScriptRampDoor, GET_ENTITY_COORDS(vehMyVehicle))
			ATTACH_ENTITY_TO_ENTITY(expPackageArgs.oRampDoor, vehMyVehicle, 0, <<0,-2.745, 0.450>>, <<-9,0,0>>, TRUE)
			SET_ENTITY_RECORDS_COLLISIONS(expPackageArgs.oRampDoor, FALSE)
			PRINTLN("CREATING RAMP DOOR IN GROUND SCRIPT")
		ENDIF
	ENDIF
ENDPROC

PROC RUN_EXPLOSIVE_CAM(VEHICLE_INDEX& myVehicle, BOOL& bRunCam, HIJACK_ARGS& hijackArgs)

	VECTOR vDriveToCoord
	VECTOR vPlayerPos
	VECTOR vReturnVector
	VECTOR vCamPosition
	VECTOR vCamRotation
	
	FLOAT vPlayerHeading

	FLOAT fReturnHeading
	
	INT iReturnLanes
	CAMERA_INDEX camLookback
	
	IF bRunCam
		// Grab the player's position, heading, and speed
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			vPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
			vCamRotation = GET_ENTITY_ROTATION(PLAYER_PED_ID())
		ENDIF
		
		// Grab a position in front of the player.
		vDriveToCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerPos, vPlayerHeading, <<0,50,0>>)
		vCamPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerPos, vPlayerHeading, <<0,10,5>>)
		
		camLookback = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPosition, <<(vCamRotation.x - 20), vCamRotation.y, (vCamRotation.z - 180)>>)
		ATTACH_CAM_TO_ENTITY(camLookback, myVehicle, <<0,10,5>>)
		POINT_CAM_AT_ENTITY(camLookback, myVehicle, <<0,0,0>>)
		SET_CAM_ROT(camLookback, <<(vCamRotation.x - 20), vCamRotation.y, (vCamRotation.z - 180)>>)
		IF DOES_CAM_EXIST(camLookback)
			SET_CAM_ACTIVE(camLookback, TRUE)
		ENDIF
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
		IF NOT IS_TIMER_STARTED(hijackArgs.tExplosiveCamTimer)
			START_TIMER_NOW(hijackArgs.tExplosiveCamTimer)
		ELSE
			RESTART_TIMER_NOW(hijackArgs.tExplosiveCamTimer)
		ENDIF
		
		// Grab a vehicle node that's in front of the player based on position in front of the player.
		GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vDriveToCoord, 1, vReturnVector, fReturnHeading, iReturnLanes)
	ELSE
		IF DOES_CAM_EXIST(hijackArgs.camKillShot) 
			IF NOT IS_CAM_ACTIVE(hijackArgs.camKillShot)
				IF DOES_CAM_EXIST(camLookback)
					SET_CAM_ACTIVE(camLookback, FALSE)
				ENDIF
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				DEBUG_PRINT("SETTING camLookback TO FALSE")
			ENDIF
		ELSE
			IF IS_TIMER_STARTED(hijackArgs.tExplosiveCamTimer)
				RESTART_TIMER_NOW(hijackArgs.tExplosiveCamTimer)
			ENDIF
		
			IF DOES_CAM_EXIST(camLookback)
				SET_CAM_ACTIVE(camLookback, FALSE)
			ENDIF
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DEBUG_PRINT("SETTING camLookback TO FALSE")
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_PACKAGE_CONTACTED_ANY_ENEMY(INT& index, HIJACK_ARGS& hijackArgs, EXPLOSIVE_PACKAGE_ARGS& expPackageArgs)
	INT idx
	
	IF IS_VEHICLE_DRIVEABLE(hijackArgs.vehTruck)
		IF NOT IS_ENTITY_DEAD(expPackageArgs.oExpPackage[index])
			IF IS_ENTITY_TOUCHING_ENTITY(expPackageArgs.oExpPackage[index], hijackArgs.vehTruck)
				DEBUG_PRINT("PACKAGE CONTACTED WITH hijackArgs.vehTruck")
		 		RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
		IF NOT IS_ENTITY_DEAD(expPackageArgs.oExpPackage[index])
			IF IS_ENTITY_TOUCHING_ENTITY(expPackageArgs.oExpPackage[index], hijackArgs.vehTrailer)
				DEBUG_PRINT("PACKAGE CONTACTED WITH hijackArgs.vehTrailer")
		 		RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	REPEAT MAX_NUMBER_ESCORT_VEHICLES idx
		IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx]) AND IS_VEHICLE_DRIVEABLE(hijackArgs.vehEscort[idx])
			IF NOT IS_ENTITY_DEAD(expPackageArgs.oExpPackage[index])
				IF IS_ENTITY_TOUCHING_ENTITY(expPackageArgs.oExpPackage[index], hijackArgs.vehEscort[idx])
					PRINTLN("PACKAGE HAS CONTACTED ESCORT = ", idx)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

PROC DROP_EXPLOSIVE_PACKAGE(EXPLOSIVE_PACKAGE_ARGS& expPackageArgs, VEHICLE_INDEX& myVehicle, HIJACK_ARGS& hijackArgs)
	
	INT idx, iIteration, iStartIndex
//	FLOAT fPlayerSpeed
	FLOAT offsetStart = -2.500
	FLOAT offsetHeightStart = 0.65
	VECTOR vForwardVector
	
	FLOAT fReturnZ
	VECTOR vCanisterPosition

//	IF IS_VEHICLE_DRIVEABLE(myVehicle)
//		fPlayerSpeed = GET_ENTITY_SPEED(myVehicle)
//	ENDIF

	IF bRunExplosionCam
		IF IS_TIMER_STARTED(hijackArgs.tExplosiveCamTimer) AND TIMER_DO_WHEN_READY(hijackArgs.tExplosiveCamTimer, 0.5)
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
				bRunExplosionCam = FALSE
				RUN_EXPLOSIVE_CAM(myVehicle, bRunExplosionCam, hijackArgs)
				DEBUG_PRINT("EXITING EXPLOSIVE CAM EARLY")
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bProcede = FALSE
	
	IF NOT IS_TIMER_STARTED(expPackageArgs.tExplosivePackTimer)
		bProcede = TRUE
	ELSE
		IF GET_TIMER_IN_SECONDS(expPackageArgs.tExplosivePackTimer) > EXPLOSIVE_TIMER
			bProcede = TRUE
		ENDIF
	ENDIF
	
	IF bProcede
		// Check if the player is in their vehicle
		IF IS_VEHICLE_DRIVEABLE(myVehicle) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)//AND fPlayerSpeed > MINIMUM_SPEED_FOR_DROPPING_CANISTER
			// Check to make sure we haven't exceeded the number of packages.
			IF expPackageArgs.iNumExplosivePackageDropped < MAX_NUMBER_EXPLOSIVES
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
					IF DOES_ENTITY_EXIST(expPackageArgs.oExpPackage[expPackageArgs.iNumExplosivePackageDropped])
						IF IS_ENTITY_ATTACHED(expPackageArgs.oExpPackage[expPackageArgs.iNumExplosivePackageDropped])
						
							hijackArgs.bOkayToRunCanisterDamageTest = FALSE
						
							DETACH_ENTITY(expPackageArgs.oExpPackage[expPackageArgs.iNumExplosivePackageDropped])
							
							IF NOT hijackArgs.bTruckDead
								vForwardVector = GET_ENTITY_FORWARD_VECTOR(myVehicle)
								APPLY_FORCE_TO_ENTITY(expPackageArgs.oExpPackage[expPackageArgs.iNumExplosivePackageDropped], APPLY_TYPE_IMPULSE, <<vForwardVector.x, vForwardVector.y - 10, vForwardVector.z>>, <<0,1,0>>, 0, TRUE, TRUE, TRUE)
								PRINTLN("TRUCK IS NOT DEAD - APPLYING FORCE")
							ELSE
								IF hijackArgs.bTruckDead AND NOT hijackArgs.bEscortsDone
									vForwardVector = GET_ENTITY_FORWARD_VECTOR(myVehicle)
									APPLY_FORCE_TO_ENTITY(expPackageArgs.oExpPackage[expPackageArgs.iNumExplosivePackageDropped], APPLY_TYPE_IMPULSE, <<vForwardVector.x, vForwardVector.y - 10, vForwardVector.z>>, <<0,1,0>>, 0, TRUE, TRUE, TRUE)
									PRINTLN("TRUCK IS DEAD BUT NOT ESCORTS - APPLYING FORCE")
								ELSE
									vForwardVector = GET_ENTITY_FORWARD_VECTOR(myVehicle)
									APPLY_FORCE_TO_ENTITY(expPackageArgs.oExpPackage[expPackageArgs.iNumExplosivePackageDropped], APPLY_TYPE_IMPULSE, <<0, vForwardVector.y - 2, 0>>, <<0,1,0>>, 0, TRUE, TRUE, TRUE)
									PRINTLN("TRUCK IS DEAD BUT NOT ESCORTS - APPLYING FORCE")
									PRINTLN("NO FORCE BEING APPLIED")
								ENDIF
							ENDIF
							
							DEBUG_PRINT("DETACHING PACKAGE")
							
							IF NOT IS_TIMER_STARTED(expPackageArgs.tExplosivePackTimer)
								START_TIMER_NOW(expPackageArgs.tExplosivePackTimer)
							ELSE
								RESTART_TIMER_NOW(expPackageArgs.tExplosivePackTimer)
							ENDIF
							
							bRunExplosionCam = TRUE
							RUN_EXPLOSIVE_CAM(myVehicle, bRunExplosionCam, hijackArgs)
							
							PRINTSTRING("DROPPING NUMBER: ")
							PRINTINT(expPackageArgs.iNumExplosivePackageDropped)
							PRINTNL()
							
							SETTIMERA(0)
							
							// Move canisters down
							REPEAT MAX_NUMBER_EXPLOSIVES idx
								IF IS_ENTITY_ATTACHED(expPackageArgs.oExpPackage[idx])
									// Store off start index
									IF NOT bGrabbedStartIndex
										iStartIndex = idx
										bGrabbedStartIndex = TRUE
									ENDIF
									IF iStartIndex = 1
										iIteration = idx - 1
										ATTACH_ENTITY_TO_ENTITY(expPackageArgs.oExpPackage[idx], myVehicle, 0, <<0, (offsetStart + (0.42*iIteration)), (offsetHeightStart + (iIteration*0.100))>>, expPackageArgs.vExpPackageRot)
									ELIF iStartIndex = 2
										iIteration = idx - 2
										ATTACH_ENTITY_TO_ENTITY(expPackageArgs.oExpPackage[idx], myVehicle, 0, <<0, (offsetStart+ (0.42*iIteration)), (offsetHeightStart + (iIteration*0.100))>>, expPackageArgs.vExpPackageRot)
									ELIF iStartIndex = 3
										iIteration = idx - 3
										ATTACH_ENTITY_TO_ENTITY(expPackageArgs.oExpPackage[idx], myVehicle, 0, <<0, (offsetStart+ (0.42*iIteration)), (offsetHeightStart + (iIteration*0.100))>>, expPackageArgs.vExpPackageRot)
									ENDIF
								ENDIF
							ENDREPEAT
							
							bGrabbedStartIndex = FALSE
							
							hijackArgs.bDroppedOnePackage = TRUE
							expPackageArgs.iNumExplosivePackageDropped++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	REPEAT MAX_NUMBER_EXPLOSIVES idx
		IF NOT expPackageArgs.bExplodedPackage[idx]
			IF TIMERA() > 1000
			OR HAS_PACKAGE_CONTACTED_ANY_ENEMY(idx, hijackArgs, expPackageArgs)
				
//				IF TIMERA() > 1000
//					PRINTLN("TIMER CHECK PASSED ON DROP: ", idx)
//				ENDIF
				IF HAS_PACKAGE_CONTACTED_ANY_ENEMY(idx, hijackArgs, expPackageArgs)
					PRINTLN("CONTACT WITH ENEMY CHECK PASSED ON DROP: ", idx)
				ENDIF
			
				IF NOT IS_ENTITY_ATTACHED(expPackageArgs.oExpPackage[idx])
//					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(expPackageArgs.oExpPackage[idx])
						IF NOT IS_ENTITY_DEAD(expPackageArgs.oExpPackage[idx])
							IF NOT hijackArgs.bTruckDead
								ADD_EXPLOSION(GET_ENTITY_COORDS(expPackageArgs.oExpPackage[idx]), EXP_TAG_TRUCK, 1.0)
								PRINTLN("TRUCK NOT DEAD: ADDING EXPLOSION ON CANISTER: ", idx)
							ELSE
								IF hijackArgs.bTruckDead AND NOT hijackArgs.bEscortsDone
									ADD_EXPLOSION(GET_ENTITY_COORDS(expPackageArgs.oExpPackage[idx]), EXP_TAG_TRUCK, 1.0)
									PRINTLN("ESCORTS NOT DEAD: ADDING EXPLOSION ON CANISTER: ", idx)
								ELSE
									IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(expPackageArgs.oExpPackage[idx])
										FREEZE_ENTITY_POSITION(expPackageArgs.oExpPackage[idx], TRUE)
										bFrozeCanister = TRUE
										iCanisterToUnfreeze = idx
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTLN("CANISTER IS DEAD: ", idx)
						ENDIF
						expPackageArgs.bExplodedPackage[idx] = TRUE
						SETTIMERB(0)
						DEBUG_PRINT("bToggleCam = TRUE")
						bToggleCam = TRUE
//					ENDIF
				ENDIF
			ELSE
				PRINTLN("WAITING FOR EXPLOSION ON DROP: ", idx)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bToggleCam
		IF TIMERB() > 500
			IF bFrozeCanister
				IF NOT IS_ENTITY_DEAD(expPackageArgs.oExpPackage[iCanisterToUnfreeze])
					vCanisterPosition = GET_ENTITY_COORDS(expPackageArgs.oExpPackage[iCanisterToUnfreeze])
					GET_GROUND_Z_FOR_3D_COORD(vCanisterPosition, fReturnZ)
					fReturnZ = fReturnZ + 0.2
					SET_ENTITY_COORDS_NO_OFFSET(expPackageArgs.oExpPackage[iCanisterToUnfreeze], << vCanisterPosition.x, vCanisterPosition.y, fReturnZ >>)
					FREEZE_ENTITY_POSITION(expPackageArgs.oExpPackage[iCanisterToUnfreeze], FALSE)
				ENDIF
			ENDIF
			
			DEBUG_PRINT("bRunExplosionCam = FALSE")
			bRunExplosionCam = FALSE
			RUN_EXPLOSIVE_CAM(myVehicle, bRunExplosionCam, hijackArgs)
			bToggleCam = FALSE
			hijackArgs.bOkayToRunCanisterDamageTest = TRUE
		ENDIF
	ENDIF

ENDPROC

PROC SETUP_ESCORTS(VEHICLE_INDEX& Escort)
	SET_ENTITY_PROOFS(Escort, FALSE, TRUE, FALSE, FALSE, FALSE)
	SET_VEHICLE_ON_GROUND_PROPERLY(Escort)
	SET_VEHICLE_ENGINE_ON(Escort, TRUE, TRUE)
	SET_ENTITY_LOAD_COLLISION_FLAG(Escort, TRUE)
ENDPROC

FUNC BOOL CREATE_HIJACK(LOCATION_DATA& myLocData, HIJACK_ARGS& hijackArgs)//, VECTOR& RESETING outOfVehicleTime VIA UPDATE_LAW[])
	INT idx
//	VECTOR vOffsetTrailerPos

	CLEAR_AREA_OF_VEHICLES(myLocData.dropData[0].vTruckPos, 10.0)
	hijackArgs.bOkayToRunCanisterDamageTest = TRUE
	
	// Create Truck and Trailer
	hijackArgs.vehTruck = CREATE_VEHICLE(myLocData.vehicleName, myLocData.dropData[0].vTruckPos, myLocData.dropData[0].fTruckRot)
	hijackArgs.vehTrailer = CREATE_VEHICLE(myLocData.genericVehicle, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(myLocData.dropData[0].vTruckPos, myLocData.dropData[0].fTruckRot, <<0,-10,0>>), GET_ENTITY_HEADING(hijackArgs.vehTruck))
	
	SET_VEHICLE_LIVERY(hijackArgs.vehTrailer, 1)
	
	// Create Escorts
	// If we're doing the first hijack, only create 2 escorts
	IF g_savedGlobals.sTraffickingData.iGroundRank = 3
		hijackArgs.vehEscort[0] = CREATE_VEHICLE(myLocData.mnEscortVehicle[0], GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(myLocData.dropData[0].vTruckPos, myLocData.dropData[0].fTruckRot, <<0,-20,0>>), myLocData.dropData[0].fTruckRot)
		hijackArgs.vehEscort[1] = CREATE_VEHICLE(myLocData.mnEscortVehicle[0], GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(myLocData.dropData[0].vTruckPos, myLocData.dropData[0].fTruckRot, <<5,-20,0>>), myLocData.dropData[0].fTruckRot)
		SETUP_ESCORTS(hijackArgs.vehEscort[0])
		SETUP_ESCORTS(hijackArgs.vehEscort[1])
	ELSE
		hijackArgs.vehEscort[0] = CREATE_VEHICLE(myLocData.mnEscortVehicle[0], GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(myLocData.dropData[0].vTruckPos, myLocData.dropData[0].fTruckRot, <<0,-20,0>>),myLocData.dropData[0].fTruckRot)
		hijackArgs.vehEscort[1] = CREATE_VEHICLE(myLocData.mnEscortVehicle[0], GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(myLocData.dropData[0].vTruckPos, myLocData.dropData[0].fTruckRot, <<5,-20,0>>), myLocData.dropData[0].fTruckRot)
		hijackArgs.vehEscort[2] = CREATE_VEHICLE(myLocData.mnEscortVehicle[0], GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(myLocData.dropData[0].vTruckPos, myLocData.dropData[0].fTruckRot, <<-2.5,0,0>>), myLocData.dropData[0].fTruckRot)
		hijackArgs.vehEscort[3] = CREATE_VEHICLE(myLocData.mnEscortVehicle[0], GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(myLocData.dropData[0].vTruckPos, myLocData.dropData[0].fTruckRot, <<2.5,0,0>>), myLocData.dropData[0].fTruckRot)
		SETUP_ESCORTS(hijackArgs.vehEscort[0])
		SETUP_ESCORTS(hijackArgs.vehEscort[1])
		SETUP_ESCORTS(hijackArgs.vehEscort[2])
		SETUP_ESCORTS(hijackArgs.vehEscort[3])
	ENDIF
	
	// Truck
	SET_VEHICLE_ON_GROUND_PROPERLY(hijackArgs.vehTruck)
	SET_VEHICLE_ENGINE_ON(hijackArgs.vehTruck, TRUE, TRUE)
	SET_ENTITY_PROOFS(hijackArgs.vehTruck, FALSE, TRUE, FALSE, FALSE, FALSE)
	SET_VEHICLE_STRONG(hijackArgs.vehTruck, TRUE)
	
	// Trailer
	SET_VEHICLE_ON_GROUND_PROPERLY(hijackArgs.vehTrailer)
	SET_ENTITY_PROOFS(hijackArgs.vehTrailer, FALSE, TRUE, TRUE, FALSE, FALSE)
	SET_VEHICLE_DOOR_SHUT(hijackArgs.vehTrailer, SC_DOOR_REAR_LEFT)
	SET_VEHICLE_DOOR_SHUT(hijackArgs.vehTrailer, SC_DOOR_REAR_RIGHT)
	SET_VEHICLE_DOOR_LATCHED(hijackArgs.vehTrailer, SC_DOOR_REAR_LEFT, TRUE, TRUE)
	SET_VEHICLE_DOOR_LATCHED(hijackArgs.vehTrailer, SC_DOOR_REAR_RIGHT, TRUE, TRUE)
	SET_VEHICLE_DOORS_LOCKED(hijackArgs.vehTrailer, VEHICLELOCK_LOCKED)
	
//	// Create weapon cargo in trailer
//	IF DOES_ENTITY_EXIST(hijackArgs.vehTrailer)
//		vOffsetTrailerPos = GET_ENTITY_COORDS(hijackArgs.vehTrailer)
//		
//		OBJECT_INDEX oFiller01
//		
//		IF g_savedGlobals.sTraffickingData.iGroundRank = 4
//			hijackArgs.oWeaponsCargo = CREATE_OBJECT(myLocData.mnLoadedCargo, vOffsetTrailerPos)
////			oFiller01 = CREATE_OBJECT(Prop_Box_Wood08A, vOffsetTrailerPos)
//			hijackArgs.oFiller02 = CREATE_OBJECT(PROP_BARREL_01B, <<vOffsetTrailerPos.x+1,vOffsetTrailerPos.y, vOffsetTrailerPos.z>>)
//			ATTACH_ENTITY_TO_ENTITY(oFiller01, hijackArgs.vehTrailer, 0, <<0,0,-1.25>>, <<0,0,0>>)
//			ATTACH_ENTITY_TO_ENTITY(hijackArgs.oWeaponsCargo, hijackArgs.vehTrailer, 0, <<0,-4.640,-0.820>>, <<0,0,-90>>)
//			ATTACH_ENTITY_TO_ENTITY(hijackArgs.oFiller02, hijackArgs.vehTrailer, 0, <<-0.7,-4.640,-0.820>>, <<0,0,-90>>)
//		ELSE
//			hijackArgs.oWeaponsCargo = CREATE_OBJECT(myLocData.mnLoadedCargo, vOffsetTrailerPos)
////			oFiller01 = CREATE_OBJECT(Prop_Box_Wood08A, vOffsetTrailerPos)
//			ATTACH_ENTITY_TO_ENTITY(oFiller01, hijackArgs.vehTrailer, 0, <<0,0,-1.25>>, <<0,0,0>>)
//			ATTACH_ENTITY_TO_ENTITY(hijackArgs.oWeaponsCargo, hijackArgs.vehTrailer, 0, <<0,-4.640,-1.120>>, <<0,0,-90>>)
//		ENDIF
//		
//		DEBUG_PRINT("CREATING AND ATTACHING WEAPONS BOX")
//	ENDIF
	
	PRINTSTRING("CARGO'S STARTING HEALTH = ")
	PRINTINT(GET_ENTITY_HEALTH(hijackArgs.oWeaponsCargo))
	PRINTNL()

	// Attach trailer to truck
	ATTACH_VEHICLE_TO_TRAILER(hijackArgs.vehTruck, hijackArgs.vehTrailer)
	IF IS_VEHICLE_ATTACHED_TO_TRAILER(hijackArgs.vehTruck)
		DEBUG_PRINT("TRANSPORT TRUCK IS ATTACHED TO TRAILER")
	ENDIF
	
	// Need to load collision early, so they will move at the start.
	SET_ENTITY_LOAD_COLLISION_FLAG(hijackArgs.vehTruck, TRUE)
	SET_ENTITY_LOAD_COLLISION_FLAG(hijackArgs.vehTrailer, TRUE)
	
	IF DOES_ENTITY_EXIST(hijackArgs.vehTruck) AND DOES_ENTITY_EXIST(hijackArgs.vehTrailer) AND DOES_ENTITY_EXIST(hijackArgs.vehEscort[0])
	AND DOES_ENTITY_EXIST(hijackArgs.vehEscort[1]) AND IS_VEHICLE_DRIVEABLE(hijackArgs.vehTruck) //AND IS_VEHICLE_DRIVEABLE(hijackArgs.vehTrailer)
		
		// Create hijack relationship group
		ADD_RELATIONSHIP_GROUP("hijackGroup", hijackArgs.relHijackGuys)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, hijackArgs.relHijackGuys)
		
		// Create truck driver ped
		hijackArgs.pedTruckDriver = CREATE_PED_INSIDE_VEHICLE(hijackArgs.vehTruck, PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy)
		DEBUG_PRINT("CREATING TRUCK DRIVER")
		
		IF g_savedGlobals.sTraffickingData.iGroundRank = 3
			hijackArgs.pedEscort[0] = CREATE_PED_INSIDE_VEHICLE(hijackArgs.vehEscort[0], PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy)
			hijackArgs.pedEscort[1] = CREATE_PED_INSIDE_VEHICLE(hijackArgs.vehEscort[1], PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy)

			DEBUG_PRINT("CREATING HIJACK ESCORTS - LEVEL 4")
		ELSE
			hijackArgs.pedEscort[0] = CREATE_PED_INSIDE_VEHICLE(hijackArgs.vehEscort[0], PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy)
			hijackArgs.pedEscort[1] = CREATE_PED_INSIDE_VEHICLE(hijackArgs.vehEscort[1], PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy)
			hijackArgs.pedEscort[2] = CREATE_PED_INSIDE_VEHICLE(hijackArgs.vehEscort[2], PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy)
			hijackArgs.pedEscort[3] = CREATE_PED_INSIDE_VEHICLE(hijackArgs.vehEscort[3], PEDTYPE_CRIMINAL, myLocData.mnGenericBadGuy)

			DEBUG_PRINT("CREATING HIJACK ESCORTS - LEVEL 5")
		ENDIF
		
		// Set some ped attributes
		// Truck driver
//		SET_PED_COMBAT_ATTRIBUTES(hijackArgs.pedTruckDriver, CA_ALWAYS_FLEE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(hijackArgs.pedTruckDriver, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_FLEE_ATTRIBUTES(hijackArgs.pedTruckDriver, FA_USE_VEHICLE, TRUE)
		SET_PED_KEEP_TASK(hijackArgs.pedTruckDriver, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hijackArgs.pedTruckDriver, TRUE)
//		SET_ENTITY_PROOFS(hijackArgs.pedTruckDriver, FALSE, TRUE, TRUE, FALSE, FALSE)
		
		// Escort peds
		REPEAT MAX_NUMBER_ESCORT_PEDS idx
			IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
				SET_PED_KEEP_TASK(hijackArgs.pedEscort[idx], TRUE)
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(hijackArgs.pedEscort[idx], KNOCKOFFVEHICLE_HARD)
				SET_PED_HELMET(hijackArgs.pedEscort[idx], TRUE)
				GIVE_PED_HELMET(hijackArgs.pedEscort[idx], TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hijackArgs.pedEscort[idx], TRUE)
				SET_PED_CONFIG_FLAG(hijackArgs.pedEscort[idx], PCF_RunFromFiresAndExplosions, FALSE)
				SET_ENTITY_PROOFS(hijackArgs.pedEscort[idx], FALSE, TRUE, FALSE, FALSE, FALSE)
				SET_PED_COMBAT_RANGE(hijackArgs.pedEscort[idx], CR_FAR)
				SET_PED_COMBAT_ATTRIBUTES(hijackArgs.pedEscort[idx], CA_REQUIRES_LOS_TO_SHOOT, FALSE)
				SET_PED_FIRING_PATTERN(hijackArgs.pedEscort[idx], FIRING_PATTERN_FULL_AUTO)
				SET_PED_TARGET_LOSS_RESPONSE(hijackArgs.pedEscort[idx], TLR_NEVER_LOSE_TARGET)
			ENDIF
		ENDREPEAT
		
		// Disable going wanted
		SET_WANTED_LEVEL_MULTIPLIER(0)
		DEBUG_PRINT("SETTING WANTED MULTIPLIER IN HIJACK MODE")
		
		RETURN TRUE
	ELSE
		DEBUG_PRINT("TRUCK AND TRAILER DO NOT EXIST IN HIJACK MODE")
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL HIJACK_TASK_TRANSPORT_AND_ESCORTS(LOCATION_DATA& myLocData, HIJACK_ARGS& hijackArgs)
	
	FLOAT fTruckSpeed
	SEQUENCE_INDEX iSeq
//	FLOAT fNodeRange = 500
	
	IF ARE_NODES_LOADED_FOR_AREA(myLocData.dropData[0].vTruckPos.x, myLocData.endData[1].vCenterPos.y, myLocData.endData[1].vCenterPos.x, myLocData.dropData[0].vTruckPos.y)
	
		IF g_savedGlobals.sTraffickingData.iGroundRank = 3
			fTruckSpeed = 16.0
			DEBUG_PRINT("fTruckSpeed = 16.0")
		ELSE
			fTruckSpeed = 18.0
			DEBUG_PRINT("fTruckSpeed = 18.0")
		ENDIF
	
		// ----------------------------------------------------Setup AI ----------------------------------------------------
		IF g_savedGlobals.sTraffickingData.iGroundRank = 3
			// Escort vehicles
			TASK_VEHICLE_ESCORT(hijackArgs.pedEscort[0], hijackArgs.vehEscort[0], hijackArgs.vehTrailer, VEHICLE_ESCORT_REAR, fTruckSpeed + 10, DRIVINGMODE_AVOIDCARS, 8)
			TASK_VEHICLE_ESCORT(hijackArgs.pedEscort[1], hijackArgs.vehEscort[1], hijackArgs.vehTrailer, VEHICLE_ESCORT_REAR, fTruckSpeed + 10, DRIVINGMODE_AVOIDCARS, 18)
		ELSE
			// Escort vehicles
			TASK_VEHICLE_ESCORT(hijackArgs.pedEscort[0], hijackArgs.vehEscort[0], hijackArgs.vehTrailer, VEHICLE_ESCORT_REAR, fTruckSpeed + 10, DRIVINGMODE_AVOIDCARS, 8)
			TASK_VEHICLE_ESCORT(hijackArgs.pedEscort[1], hijackArgs.vehEscort[1], hijackArgs.vehTrailer, VEHICLE_ESCORT_REAR, fTruckSpeed + 10, DRIVINGMODE_AVOIDCARS, 18)
			TASK_VEHICLE_ESCORT(hijackArgs.pedEscort[2], hijackArgs.vehEscort[2], hijackArgs.vehTrailer, VEHICLE_ESCORT_LEFT, fTruckSpeed + 10, DRIVINGMODE_AVOIDCARS, 5)
			TASK_VEHICLE_ESCORT(hijackArgs.pedEscort[3], hijackArgs.vehEscort[3], hijackArgs.vehTrailer, VEHICLE_ESCORT_RIGHT, fTruckSpeed + 10, DRIVINGMODE_AVOIDCARS, 5)
		ENDIF
		
		IF g_savedGlobals.sTraffickingData.iGroundRank = 4
	//			TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(hijackArgs.pedTruckDriver, hijackArgs.vehTruck,
	//			"Ground5_Hijack", DRIVINGMODE_STOPFORCARS, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, -1)
	//			PRINTLN("USING HIJACK WAYPOINT RECORDING")
			
			// Main truck and trailer
			OPEN_SEQUENCE_TASK(iSeq)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[0], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[1], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[2], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[3], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[4], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[5], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[6], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
			CLOSE_SEQUENCE_TASK(iSeq)
			IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
				TASK_PERFORM_SEQUENCE(hijackArgs.pedTruckDriver, iSeq)
			ENDIF
			CLEAR_SEQUENCE_TASK(iSeq)
			PRINTLN("USING DRIVING MODE = DRIVINGMODE_STOPFORCARS 01")
		ELSE
			// Main truck and trailer
			OPEN_SEQUENCE_TASK(iSeq)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[0], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[1], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[2], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[3], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[4], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[5], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[6], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_STOPFORCARS, 10, -1)
			CLOSE_SEQUENCE_TASK(iSeq)
			IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
				TASK_PERFORM_SEQUENCE(hijackArgs.pedTruckDriver, iSeq)
			ENDIF
			CLEAR_SEQUENCE_TASK(iSeq)
			PRINTLN("USING DRIVING MODE = DRIVINGMODE_STOPFORCARS 02")
		ENDIF
		
		RETURN TRUE
	ELSE
		PRINTLN("WAITING ON ROAD NODES TO LOAD")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MOVE_ESCORT_BIKES(HIJACK_ARGS& hijackArgs)
	INT idx
	VECTOR vTrailerPos
	FLOAT fTrailerHeading

	vTrailerPos = GET_ENTITY_COORDS(hijackArgs.vehTrailer)
//	PRINTLN("vTrailerPos = ", vTrailerPos)
	fTrailerHeading = GET_ENTITY_HEADING(hijackArgs.vehTrailer)
	
	REPEAT MAX_NUMBER_ESCORT_VEHICLES idx
		IF NOT hijackArgs.bMovedBike[idx]
			IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
				IF IS_VEHICLE_DRIVEABLE(hijackArgs.vehEscort[idx])
//					IF NOT IS_ENTITY_ON_SCREEN(hijackArgs.vehEscort[idx]) OR IS_ENTITY_OCCLUDED(hijackArgs.vehEscort[idx])
						hijackArgs.vMoveEscortPos[idx] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, <<GET_RANDOM_FLOAT_IN_RANGE(5,10), GET_RANDOM_FLOAT_IN_RANGE(-15,-30), 0>>)
						PRINTLN("hijackArgs.vMoveEscortPos idx = ", idx)
						
						IF NOT IS_SPHERE_VISIBLE(hijackArgs.vMoveEscortPos[idx], 5.0)
							IF NOT IS_VECTOR_ZERO(hijackArgs.vMoveEscortPos[idx])
								SET_ENTITY_COORDS(hijackArgs.vehEscort[idx], hijackArgs.vMoveEscortPos[idx])
								BRING_VEHICLE_TO_HALT(hijackArgs.vehEscort[idx], 1.0, -1)
								SPECIAL_FUNCTION_DO_NOT_USE(hijackArgs.pedEscort[idx])
//								DELETE_VEHICLE(hijackArgs.vehEscort[idx])
								PRINTLN("MOVING BIKE TO: ", hijackArgs.vMoveEscortPos[idx])
								hijackArgs.bMovedBike[idx] = TRUE
							ENDIF
						ELSE
							DELETE_VEHICLE(hijackArgs.vehEscort[idx])
							PRINTLN("DELETING ESCORT BIKES 01 IDX: ", idx)
							hijackArgs.bMovedBike[idx] = TRUE
						ENDIF
					
//					ENDIF
				ELSE
//					IF NOT IS_ENTITY_DEAD(hijackArgs.vehEscort[idx])
//						IF NOT IS_ENTITY_ON_SCREEN(hijackArgs.vehEscort[idx]) OR IS_ENTITY_OCCLUDED(hijackArgs.vehEscort[idx])
							DELETE_VEHICLE(hijackArgs.vehEscort[idx])
							PRINTLN("DELETING ESCORT BIKES 02 IDX: ", idx)
							hijackArgs.bMovedBike[idx] = TRUE
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SET_TRAILER_ON_GROUND(HIJACK_ARGS& hijackArgs)
	VECTOR vTruckRot, vTrailerPosition, vNodePosition
	FLOAT fDeltaX

	
	IF IS_ENTITY_DEAD(hijackArgs.vehTruck) AND NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
		DETACH_VEHICLE_FROM_TRAILER(hijackArgs.vehTruck)
		PRINTLN("***********************************DETACHING TRUCK FROM TRAILER*****************************************")
	ELSE
		IF NOT IS_ENTITY_DEAD(hijackArgs.vehTruck) AND NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
			DETACH_VEHICLE_FROM_TRAILER(hijackArgs.vehTruck)
			PRINTLN("***********************************DETACHING TRUCK FROM TRAILER*****************************************")
		ENDIF
	ENDIF
	
	// If the trailer has an unusual rotation, i.e. on it's side... try and flip it so it's on all 4 wheels.
	IF NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
		vTruckRot = GET_ENTITY_ROTATION(hijackArgs.vehTrailer)
		PRINTLN("vTruckRot = ", vTruckRot)
	ENDIF
	
	IF vTruckRot.y > 10.0 AND vTruckRot.y < 100.0
	OR vTruckRot.y < -10.0 AND vTruckRot.y > -100.0
		vTruckRot.y = 0.0
		PRINTLN("TRAILER MUST BE FLIPPED, CLAPPING Y VALUE TO 0.0", vTruckRot.y)
	ENDIF
	
	IF NOT bResetRotation
		SET_ENTITY_ROTATION(hijackArgs.vehTrailer, vTruckRot)
		PRINTLN("RESETING TRAILER'S ROATION: ", vTruckRot)
		bResetRotation  = TRUE
	ENDIF
	
	// Attempting to put truck/trailer on a road node.
	IF NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
		vTrailerPosition = GET_ENTITY_COORDS(hijackArgs.vehTrailer)
		PRINTLN("SET_TRAILER_ON_GROUND: vTrailerPosition = ", vTrailerPosition)
	ENDIF
	
	// Grab a vector that's 5m behind the trailer
	VECTOR vOffsetPositionToUse
	vOffsetPositionToUse = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPosition, hijackArgs.fTrailerHeading, <<0,-5,0>>)
	
	// Grab the closest vehicle node based on vector behind the trailer
	GET_NTH_CLOSEST_VEHICLE_NODE(vOffsetPositionToUse, 1, vNodePosition)
	PRINTLN("SET_TRAILER_ON_GROUND: vNodePosition = ", vNodePosition)
	
	IF NOT ARE_VECTORS_EQUAL(vNodePosition, <<0,0,0>>)
		fDeltaX = ABSF(vTrailerPosition.x - vNodePosition.x)
		PRINTLN("SET_TRAILER_ON_GROUND: fDeltaX = ", fDeltaX)
		
		IF fDeltaX > DISTANCE_ALLOWED_FROM_ROAD_NODE
			SET_ENTITY_COORDS(hijackArgs.vehTrailer, vNodePosition)
			SET_ENTITY_HEADING(hijackArgs.vehTrailer, fTransportHeading)
			PRINTLN("********SET_TRAILER_ON_GROUND BASED ON NODE POSITION AND HEADING********")
		ENDIF
	ELSE
		PRINTLN("NODE POSITION = ", vNodePosition)
	ENDIF
	
	SET_ENTITY_HEADING(hijackArgs.vehTrailer, fTransportHeading)
	PRINTLN("IN KILL CAM - fTransportHeading = ", fTransportHeading)
	
	SET_VEHICLE_ON_GROUND_PROPERLY(hijackArgs.vehTrailer)
	FREEZE_ENTITY_POSITION(hijackArgs.vehTrailer, TRUE)
ENDPROC

FUNC BOOL RUN_HIJACK_KILL_CAM(HIJACK_ARGS& hijackArgs, HIJACK_KILL_CAM& runHijackKillCamStages, VEHICLE_INDEX& myVehicle)
	VECTOR vTruckPos
	FLOAT vTruckHeading 
	VECTOR vKillCamOffset = <<0,30,3>>
	VECTOR vTruckRot
	VECTOR vKillCamPos
	VECTOR vKillCamRot
//	INT idx
	
	IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		SET_TIME_SCALE(1.0)
		
		SET_TRAILER_ON_GROUND(hijackArgs)
		MOVE_ESCORT_BIKES(hijackArgs)
		
		IF DOES_CAM_EXIST(hijackArgs.camKillShot)
			IF IS_CAM_ACTIVE(hijackArgs.camKillShot)
				SET_CAM_ACTIVE(hijackArgs.camKillShot, FALSE)
				DESTROY_CAM(hijackArgs.camKillShot)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(myVehicle)
			FREEZE_ENTITY_POSITION(myVehicle, FALSE)
		ENDIF
		
		hijackArgs.bOkayToRunCanisterDamageTest = TRUE
		
		DEBUG_PRINT("SKIP CUTSCENE - RETURNING TRUE EARLY ON KILL CAM")
		RETURN TRUE
	ENDIF
	
	SWITCH runHijackKillCamStages
		CASE HIJACK_KILL_CAM_01
			hijackArgs.bOkayToRunCanisterDamageTest = FALSE
		
			vTruckPos = GET_ENTITY_COORDS(hijackArgs.vehTruck)
			vTruckHeading = GET_ENTITY_HEADING(hijackArgs.vehTruck)
			vTruckRot = GET_ENTITY_ROTATION(hijackArgs.vehTruck)
			
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
			
			CLEAR_PRINTS()
			CLEAR_HELP()
				
			// Grab camera positions
			vKillCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTruckPos, vTruckHeading, vKillCamOffset)
			vKillCamRot = <<vTruckRot.x - 10, vTruckRot.y, (vTruckRot.z - 180)>>
			
			// Create kill cam positions
			hijackArgs.camKillShot = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vKillCamPos, vKillCamRot)
			IF DOES_CAM_EXIST(hijackArgs.camKillShot)
				SET_CAM_DOF_STRENGTH(hijackArgs.camKillShot, 0.0)
				SET_CAM_ACTIVE(hijackArgs.camKillShot, TRUE)
				SET_CAM_FOV(hijackArgs.camKillShot, 35.0)
			ENDIF
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				hijackArgs.fPlayerVehicleSpeed = GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				PRINTSTRING("PLAYER SPEED = ")
				PRINTFLOAT(hijackArgs.fPlayerVehicleSpeed)
				PRINTNL()
			ENDIF
			
			IF DOES_CAM_EXIST(hijackArgs.camKillShot)
				SET_TIME_SCALE(0.1)
				IF NOT IS_TIMER_STARTED(hijackArgs.tKillCamTimer)
					START_TIMER_NOW(hijackArgs.tKillCamTimer)
					PRINTLN("STARTING TIMER - hijackArgs.tKillCamTimer")
				ENDIF
				DEBUG_PRINT("GOING TO STATE - HIJACK_KILL_CAM_02")
				runHijackKillCamStages = HIJACK_KILL_CAM_02
			ENDIF
		BREAK
		//=================================================================================================================================
		CASE HIJACK_KILL_CAM_02
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
				IF IS_ENTITY_OCCLUDED(myVehicle) AND NOT IS_ENTITY_ON_SCREEN(myVehicle)
					CLEAR_AREA(GET_ENTITY_COORDS(myVehicle), 10.0, TRUE)
					FREEZE_ENTITY_POSITION(myVehicle, TRUE)
				ENDIF
			ENDIF
			
			FLOAT fGameSpeed
//			fGameSpeed = TO_FLOAT(TIMERA() - 40) / 220.0
			fGameSpeed =  GET_TIMER_IN_SECONDS(hijackArgs.tKillCamTimer)
			fGameSpeed = ((fGameSpeed * 1000) - 40) / 220.0
			
			If fGameSpeed > 1.0
				fGameSpeed = 1.0
			ENDIF
			IF fGameSpeed < 0.1
				fGameSpeed = 0.1
			ENDIF
			
//			PRINTLN("fGameSpeed = ", fGameSpeed)
			SET_TIME_SCALE(fGameSpeed)
			
			IF IS_TIMER_STARTED(hijackArgs.tKillCamTimer)
				IF GET_TIMER_IN_SECONDS(hijackArgs.tKillCamTimer) > 1.5
					FREEZE_ENTITY_POSITION(myVehicle, FALSE)
					SET_VEHICLE_FORWARD_SPEED(myVehicle, hijackArgs.fPlayerVehicleSpeed)
					DEBUG_PRINT("RETURNING TRUE ON RUN_HIJACK_KILL_CAM")
					SET_CAM_ACTIVE(hijackArgs.camKillShot, FALSE)
					DESTROY_CAM(hijackArgs.camKillShot)
					
					SET_TRAILER_ON_GROUND(hijackArgs)
					MOVE_ESCORT_BIKES(hijackArgs)
					
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					
					SET_TIME_SCALE(1.0)
					
					hijackArgs.bOkayToRunCanisterDamageTest = TRUE
					
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("TIMER IS NOT STARTED")
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_HIJACK_STOP_CONDITIONS(LOCATION_DATA& myLocData, HIJACK_ARGS& hijackArgs, EXPLOSIVE_PACKAGE_ARGS& expPackageArgs, 
HIJACK_KILL_CAM& runHijackKillCamStages, VEHICLE_INDEX& myVehicle)
	VECTOR vTransportPos
	VECTOR vExplosiveCanisterPos[MAX_NUMBER_EXPLOSIVES]
	INT idx
	VECTOR vFront, vSide, vUp, vPosition
	
	// PASS CONDITIONS
	IF IS_VEHICLE_DRIVEABLE(hijackArgs.vehTruck) AND NOT IS_ENTITY_DEAD(hijackArgs.vehTruck)
		vTransportPos = GET_ENTITY_COORDS(hijackArgs.vehTruck)
	ENDIF
	
	// CONDITION 1 - EXPLOSIVE CANISTER DROP 
	REPEAT MAX_NUMBER_EXPLOSIVES idx
		IF expPackageArgs.bExplodedPackage[idx]
			IF NOT IS_ENTITY_DEAD(expPackageArgs.oExpPackage[idx])
//				PRINTLN("CANISTER IS NOT DEAD, ", idx)
				vExplosiveCanisterPos[idx] = GET_ENTITY_COORDS(expPackageArgs.oExpPackage[idx])
				// store off distance to check 
				hijackArgs.fExplosiveDistance = VDIST(vExplosiveCanisterPos[idx], vTransportPos)
			
				IF hijackArgs.fExplosiveDistance < HIJACK_SUCCESSFUL_DROP_DIST
					
					IF IS_VEHICLE_DRIVEABLE(hijackArgs.vehTruck) AND NOT IS_ENTITY_DEAD(hijackArgs.vehTruck)
						GET_ENTITY_MATRIX(hijackArgs.vehTruck, vFront, vSide, vUp, vPosition)
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(hijackArgs.vehTrailer) AND NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
						hijackArgs.fTrailerHeading = GET_ENTITY_HEADING(hijackArgs.vehTrailer)
					ENDIF
					
					APPLY_FORCE_TO_ENTITY(hijackArgs.vehTruck, APPLY_TYPE_IMPULSE, vUp, <<0,6,2>>, 0, TRUE, TRUE, TRUE)
					ADD_EXPLOSION(vPosition, EXP_TAG_GRENADE)
				
					PRINTLN("CANISTER DISTANCE CHECK HAS PASSED")
					// TODO: find a better way to kill this guy then to explode his head.
					IF NOT IS_ENTITY_DEAD(hijackArgs.pedTruckDriver)
						IF NOT IS_ENTITY_DEAD(hijackArgs.vehTruck)
							EXPLODE_VEHICLE(hijackArgs.vehTruck)
							PRINTLN("EXPLODING VEHICLE")
						ENDIF
						DEBUG_PRINT("EXPLODE DRIVER'S HEAD")
						EXPLODE_PED_HEAD(hijackArgs.pedTruckDriver)
						hijackArgs.bKilledByHeadExploding = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF hijackArgs.bKilledByHeadExploding
		IF RUN_HIJACK_KILL_CAM(hijackArgs, runHijackKillCamStages, myVehicle)
			DEBUG_PRINT("SUCCESSFUL CANISTER DROP, RETURNING TRUE")
			
			// Remove blip on truck
			IF DOES_BLIP_EXIST(hijackArgs.blipTruck)
				REMOVE_BLIP(hijackArgs.blipTruck)
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// CONDITION 2 - TRANSPORT REACHED ENDING POSITION, AIRPORT
	IF IS_ENTITY_AT_COORD(hijackArgs.vehTruck, myLocData.endData[1].vCenterPos, g_vAnyMeansLocate)
		IF DOES_BLIP_EXIST(hijackArgs.blipTruck)
			REMOVE_BLIP(hijackArgs.blipTruck)
		ENDIF
		
		IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver) AND IS_VEHICLE_DRIVEABLE(hijackArgs.vehTruck)
			CLEAR_PED_TASKS(hijackArgs.pedTruckDriver)
			TASK_VEHICLE_DRIVE_WANDER(hijackArgs.pedTruckDriver, hijackArgs.vehTruck, 15.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
			PRINTLN("TASKING HIJACK DRIVER TO WANDER")
		ENDIF
		
		hijackArgs.bTransportReachedDest = TRUE
		DEBUG_PRINT("hijackArgs.bTransportReachedDest = TRUE")
	ENDIF
	
	// CONDITION 3 - DRIVE IS KILLED
	IF NOT hijackArgs.bKilledByHeadExploding
		IF IS_ENTITY_DEAD(hijackArgs.pedTruckDriver)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(hijackArgs.pedTruckDriver, PLAYER_PED_ID())
					DEBUG_PRINT("TRANSPORT DRIVE IS KILLED 01, RETURNING TRUE")
					// Remove blip on truck
					IF DOES_BLIP_EXIST(hijackArgs.blipTruck)
						REMOVE_BLIP(hijackArgs.blipTruck)
					ENDIF
					
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					SET_TIME_SCALE(1.0)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					RETURN TRUE
				ELSE
					IF RUN_HIJACK_KILL_CAM(hijackArgs, runHijackKillCamStages, myVehicle)
						DEBUG_PRINT("TRANSPORT DRIVE IS KILLED 02, RETURNING TRUE")
						// Remove blip on truck
						IF DOES_BLIP_EXIST(hijackArgs.blipTruck)
							REMOVE_BLIP(hijackArgs.blipTruck)
						ENDIF
					
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_DEAD(hijackArgs.vehTruck)
//		PRINTLN("TRUCK IS DEAD")
		IF RUN_HIJACK_KILL_CAM(hijackArgs, runHijackKillCamStages, myVehicle)
			// Remove blip on truck
			IF DOES_BLIP_EXIST(hijackArgs.blipTruck)
				REMOVE_BLIP(hijackArgs.blipTruck)
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC HIJACK_AGGRO_CHECK(LOCATION_DATA& myLocData, HIJACK_ARGS& hijackArgs, EXPLOSIVE_PACKAGE_ARGS& expPackageArgs)
	VECTOR vPlayerPos, vTruckPos
	VECTOR vEscortPedPos[MAX_NUMBER_ESCORT_PEDS]
	BOOL bAggroed = FALSE
	FLOAT fTruckSpeed = 25.0
	FLOAT fTarget = 10.0
	INT iSequenceProgress, idx
	SEQUENCE_INDEX iSeq
	VEHICLE_INDEX vehPlayerVehicle
		
	IF NOT hijackArgs.bStopRunningAggroCheck
		// Grab player's position and truck's position
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehPlayerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(hijackArgs.vehTruck)
			vTruckPos = GET_ENTITY_COORDS(hijackArgs.vehTruck)
		ENDIF
		
		
		REPEAT MAX_NUMBER_EXPLOSIVES idx
			IF expPackageArgs.bExplodedPackage[idx]
				expPackageArgs.vCanisterPos[idx] = GET_ENTITY_COORDS(expPackageArgs.oExpPackage[idx])
				// store off distance to check 
				hijackArgs.fExplosiveAggroDistance = VDIST(expPackageArgs.vCanisterPos[idx], vTruckPos)
			ENDIF
		ENDREPEAT
		
		// Check if the player is close to the truck or if they have released a canister near the truck.
		IF hijackArgs.bDroppedOnePackage
			IF hijackArgs.fExplosiveAggroDistance > 0
				IF hijackArgs.fExplosiveAggroDistance < HIJACK_AGGRO_DISTANCE_EXPLOSIVE
					DEBUG_PRINT("PLAYER AGGROED DUE TO EXPLOSIVE DISTANCE")
					bAggroed = TRUE
				ENDIF
			ENDIF
		ENDIF
		IF VDIST(vPlayerPos, vTruckPos) < HIJACK_AGGRO_DISTANCE
			IF NOT hijackArgs.bGrabbedTime
				hijackArgs.iTooCloseTime = GET_GAME_TIMER()
				PRINTSTRING("GRABBED TIME = ")
				PRINTINT(hijackArgs.iTooCloseTime)
				PRINTNL()
				hijackArgs.bGrabbedTime = TRUE
			ENDIF
			IF GET_GAME_TIMER() - hijackArgs.iTooCloseTime > HIJACK_TOO_CLOSE_TIME
				DEBUG_PRINT("PLAYER AGGROED DUE TO BEING TOO CLOSE TO TRUCK")
				bAggroed = TRUE
			ENDIF
		ELSE
			hijackArgs.bGrabbedTime = FALSE
			hijackArgs.iTooCloseTime = 0
		ENDIF
		
		REPEAT MAX_NUMBER_ESCORT_PEDS idx
			IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
				IF NOT IS_PED_INJURED(hijackArgs.pedEscort[idx])
					vEscortPedPos[idx] = GET_ENTITY_COORDS(hijackArgs.pedEscort[idx])
					vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					IF IS_PED_SHOOTING(PLAYER_PED_ID()) AND (VDIST(vPlayerPos, vEscortPedPos[idx]) < MIN_DISTANCE_TO_ESCORT_PEDS)
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(hijackArgs.pedEscort[idx], PLAYER_PED_ID())
						bAggroed = TRUE
						DEBUG_PRINT("PLAYER AGGROED DUE TO PLAYER FIRING ON ESCORT PEDS")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF DOES_ENTITY_EXIST(hijackArgs.vehTruck) AND DOES_ENTITY_EXIST(vehPlayerVehicle)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(hijackArgs.vehTruck, vehPlayerVehicle)
				bAggroed = TRUE
				DEBUG_PRINT("PLAYER AGGROED DUE TO TRUCK TAKING DAMAGE FROM THE PLAYER'S VEHICLE")
			ENDIF
		ENDIF
		
		// Touching entity checks
		// TRUCK
		IF NOT IS_ENTITY_DEAD(hijackArgs.vehTruck) AND NOT IS_ENTITY_DEAD(vehPlayerVehicle)
			IF IS_ENTITY_TOUCHING_ENTITY(hijackArgs.vehTruck, vehPlayerVehicle)
				bAggroed = TRUE
				DEBUG_PRINT("PLAYER AGGROED DUE TO COLLISION WITH MAIN TRUCK")
			ENDIF
		ENDIF
		// TRAILER
		IF NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer) AND NOT IS_ENTITY_DEAD(vehPlayerVehicle)
			IF IS_ENTITY_TOUCHING_ENTITY(hijackArgs.vehTrailer, vehPlayerVehicle)
				bAggroed = TRUE
				DEBUG_PRINT("PLAYER AGGROED DUE TO COLLISION WITH TRAILER")
			ENDIF
		ENDIF
		
		REPEAT MAX_NUMBER_ESCORT_VEHICLES idx
			IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
				IF NOT IS_ENTITY_DEAD(hijackArgs.vehEscort[idx]) AND NOT IS_ENTITY_DEAD(vehPlayerVehicle)
					IF IS_ENTITY_TOUCHING_ENTITY(hijackArgs.vehEscort[idx], vehPlayerVehicle)
						bAggroed = TRUE
						DEBUG_PRINT("PLAYER AGGROED DUE TO COLLISION WITH ESCORT VEHICLE")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bAggroed AND NOT hijackArgs.bTruckDead
			// Change AI
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, hijackArgs.relHijackGuys)
			
			REPEAT MAX_NUMBER_ESCORT_VEHICLES idx
				IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx]) 
					IF IS_VEHICLE_DRIVEABLE(hijackArgs.vehEscort[idx])
						IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx]) 
							IF NOT IS_PED_INJURED(hijackArgs.pedEscort[idx])
								CLEAR_PED_TASKS(hijackArgs.pedEscort[idx])
//								SETUP_PED_FOR_CHASE(hijackArgs.pedEscort[idx], hijackArgs.relHijackGuys)
								
								SET_PED_COMBAT_ABILITY(hijackArgs.pedEscort[idx], CAL_PROFESSIONAL)
								SET_PED_COMBAT_RANGE(hijackArgs.pedEscort[idx], CR_MEDIUM)
								SET_PED_COMBAT_MOVEMENT(hijackArgs.pedEscort[idx], CM_WILLADVANCE)
								GIVE_WEAPON_TO_PED(hijackArgs.pedEscort[idx], WEAPONTYPE_MICROSMG, -1, TRUE)																	
								SET_PED_CONFIG_FLAG(hijackArgs.pedEscort[idx], PCF_DontInfluenceWantedLevel, FALSE)
								SET_PED_RELATIONSHIP_GROUP_HASH(hijackArgs.pedEscort[idx], hijackArgs.relHijackGuys)
								SET_PED_KEEP_TASK(hijackArgs.pedEscort[idx], TRUE)
								SET_PED_ACCURACY(hijackArgs.pedEscort[idx], 95)
								SET_PED_COMBAT_ATTRIBUTES(hijackArgs.pedEscort[idx], CA_LEAVE_VEHICLES, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(hijackArgs.pedEscort[idx], CA_DO_DRIVEBYS, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(hijackArgs.pedEscort[idx], CA_JUST_FOLLOW_VEHICLE, TRUE)
								TASK_VEHICLE_MISSION(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx], hijackArgs.vehTruck, MISSION_ESCORT_REAR, fTruckSpeed, DRIVINGMODE_AVOIDCARS, TO_FLOAT(idx*5 + 5), -1)
								ADD_VEHICLE_SUBTASK_ATTACK_PED(hijackArgs.pedEscort[idx], PLAYER_PED_ID())
								PRINTLN("TASKING ESCORT PEDS TO ATTACK AND FOLLOW TRUCK: ", idx)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
				iSequenceProgress = GET_SEQUENCE_PROGRESS(hijackArgs.pedTruckDriver)
			ENDIF
			PRINTSTRING("iSequenceProgress = ")
			PRINTINT(iSequenceProgress)
			PRINTNL()
			
//			IF g_savedGlobals.sTraffickingData.iGroundRank = 4
//				TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(hijackArgs.pedTruckDriver, hijackArgs.vehTruck,
//				"Ground5_Hijack", DRIVINGMODE_AVOIDCARS, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, -1)
//				PRINTLN("USING HIJACK WAYPOINT RECORDING")
//			ELSE
				SWITCH iSequenceProgress
					CASE 0
						IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
							CLEAR_PED_TASKS(hijackArgs.pedTruckDriver)
							OPEN_SEQUENCE_TASK(iSeq)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[0], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[1], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[2], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[3], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[4], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[5], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[6], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
							CLOSE_SEQUENCE_TASK(iSeq)
							IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
								TASK_PERFORM_SEQUENCE(hijackArgs.pedTruckDriver, iSeq)
								PRINTLN("TASKED hijackArgs.pedTruckDriver: ", iSequenceProgress)
							ENDIF
							CLEAR_SEQUENCE_TASK(iSeq)
						ENDIF
					BREAK
					//============================================================================================================================================
					CASE 1
						IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
							CLEAR_PED_TASKS(hijackArgs.pedTruckDriver)
							OPEN_SEQUENCE_TASK(iSeq)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[1], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[2], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[3], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[4], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[5], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[6], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
							CLOSE_SEQUENCE_TASK(iSeq)
							IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
								TASK_PERFORM_SEQUENCE(hijackArgs.pedTruckDriver, iSeq)
								PRINTLN("TASKED hijackArgs.pedTruckDriver: ", iSequenceProgress)
							ENDIF
							CLEAR_SEQUENCE_TASK(iSeq)
						ENDIF
					BREAK
					//============================================================================================================================================
					CASE 2
						IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
							CLEAR_PED_TASKS(hijackArgs.pedTruckDriver)
							OPEN_SEQUENCE_TASK(iSeq)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[2], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[3], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[4], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[5], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[6], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
							CLOSE_SEQUENCE_TASK(iSeq)
							IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
								TASK_PERFORM_SEQUENCE(hijackArgs.pedTruckDriver, iSeq)
								PRINTLN("TASKED hijackArgs.pedTruckDriver: ", iSequenceProgress)
							ENDIF
							CLEAR_SEQUENCE_TASK(iSeq)
						ENDIF
					BREAK
					//============================================================================================================================================
					CASE 3
						IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
							CLEAR_PED_TASKS(hijackArgs.pedTruckDriver)
							OPEN_SEQUENCE_TASK(iSeq)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[3], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[4], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[5], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[6], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
							CLOSE_SEQUENCE_TASK(iSeq)
							IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
								TASK_PERFORM_SEQUENCE(hijackArgs.pedTruckDriver, iSeq)
								PRINTLN("TASKED hijackArgs.pedTruckDriver: ", iSequenceProgress)
							ENDIF
							CLEAR_SEQUENCE_TASK(iSeq)
						ENDIF
					BREAK
					//============================================================================================================================================
					CASE 4
						IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
							CLEAR_PED_TASKS(hijackArgs.pedTruckDriver)
							OPEN_SEQUENCE_TASK(iSeq)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[4], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[5], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[6], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
							CLOSE_SEQUENCE_TASK(iSeq)
							IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
								TASK_PERFORM_SEQUENCE(hijackArgs.pedTruckDriver, iSeq)
								PRINTLN("TASKED hijackArgs.pedTruckDriver: ", iSequenceProgress)
							ENDIF
							CLEAR_SEQUENCE_TASK(iSeq)
						ENDIF
					BREAK
					//============================================================================================================================================
					CASE 5
						IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
							CLEAR_PED_TASKS(hijackArgs.pedTruckDriver)
							OPEN_SEQUENCE_TASK(iSeq)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[5], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[6], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
							CLOSE_SEQUENCE_TASK(iSeq)
							IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
								TASK_PERFORM_SEQUENCE(hijackArgs.pedTruckDriver, iSeq)
								PRINTLN("TASKED hijackArgs.pedTruckDriver: ", iSequenceProgress)
							ENDIF
							CLEAR_SEQUENCE_TASK(iSeq)
						ENDIF
					BREAK
					//============================================================================================================================================
					CASE 6
						IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
							CLEAR_PED_TASKS(hijackArgs.pedTruckDriver)
							OPEN_SEQUENCE_TASK(iSeq)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, hijackArgs.vehTruck, myLocData.endData[1].vDestination[6], fTruckSpeed, DRIVINGSTYLE_NORMAL, HAULER, DRIVINGMODE_AVOIDCARS, fTarget, -1)
							CLOSE_SEQUENCE_TASK(iSeq)
							IF NOT IS_PED_INJURED(hijackArgs.pedTruckDriver)
								TASK_PERFORM_SEQUENCE(hijackArgs.pedTruckDriver, iSeq)
								PRINTLN("TASKED hijackArgs.pedTruckDriver: ", iSequenceProgress)
							ENDIF
							CLEAR_SEQUENCE_TASK(iSeq)
						ENDIF
					BREAK
				ENDSWITCH
//			ENDIF
			
			hijackArgs.bHijackActive = TRUE
			hijackArgs.bStopRunningAggroCheck = TRUE
			DEBUG_PRINT("bAggroed IS TRUE, STOP RUNNING AGGRO CHECK 01")
		ENDIF
	ENDIF
	
ENDPROC

PROC CHECK_DROP_DISTANCE_TO_BLOWUP_ESCORTS(HIJACK_ARGS& hijackArgs, EXPLOSIVE_PACKAGE_ARGS& expPackageArgs)
	INT idx, idx2
	VECTOR vEscortPos[MAX_NUMBER_ESCORT_VEHICLES]
	VECTOR vExplosiveCanisterPos[MAX_NUMBER_EXPLOSIVES]
	
	REPEAT MAX_NUMBER_ESCORT_PEDS idx
		IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
			IF NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx])
				vEscortPos[idx] = GET_ENTITY_COORDS(hijackArgs.pedEscort[idx])
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUMBER_EXPLOSIVES idx
		IF expPackageArgs.bExplodedPackage[idx]
			// Grab coordinates of explosive packages
			IF NOT IS_ENTITY_DEAD(expPackageArgs.oExpPackage[idx])
				vExplosiveCanisterPos[idx] = GET_ENTITY_COORDS(expPackageArgs.oExpPackage[idx])
				
				// Check if any are close to the escort vehicles, and if they are... force an explosion, similar to the truck.
				REPEAT MAX_NUMBER_ESCORT_PEDS idx2
					IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx2])
						IF NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx2])
							hijackArgs.fExplosiveDistance = VDIST(vExplosiveCanisterPos[idx], vEscortPos[idx2])
							
							IF hijackArgs.fExplosiveDistance < HIJACK_SUCCESSFUL_DROP_DIST_ESCORTS
								IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx2])
									IF NOT IS_ENTITY_DEAD(hijackArgs.vehEscort[idx2])
										EXPLODE_VEHICLE(hijackArgs.vehEscort[idx2])
									ENDIF
								ENDIF
								IF NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx2])
									EXPLODE_PED_HEAD(hijackArgs.pedEscort[idx2])
									PRINTLN("EXPLODING ESCORT PED'S HEAD: ", idx2)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC HIJACK_DISTANCE_FAIL_SAFE(HIJACK_ARGS& hijackArgs) 
	INT idx

	REPEAT MAX_NUMBER_ESCORT_PEDS idx
		IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
			IF GET_DISTANCE_BETWEEN_ENTITIES(hijackArgs.vehTrailer, hijackArgs.pedEscort[idx]) > 250.0
				
				hijackArgs.bFailSafeRemoved[idx] = TRUE
//				PRINTLN("FAIL SAFE REMOVED ON INDEX = ", idx)
				
				IF DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx])
					REMOVE_BLIP(hijackArgs.blipEscortPed[idx])
//					DEBUG_PRINT("REMOVING BLIP ON PED - FAIL SAFE")
				ENDIF
//				IF DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx])
//					REMOVE_BLIP(hijackArgs.blipEscortCar[idx])
////					DEBUG_PRINT("REMOVING BLIP ON CAR - FAIL SAFE")
//				ENDIF
			
				IF NOT IS_PED_INJURED(hijackArgs.pedEscort[idx]) AND IS_VEHICLE_DRIVEABLE(hijackArgs.vehEscort[idx])
					IF IS_PED_IN_VEHICLE(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx])
						IF IS_ENTITY_OCCLUDED(hijackArgs.pedEscort[idx])
							DELETE_PED(hijackArgs.pedEscort[idx])
							DELETE_VEHICLE(hijackArgs.vehEscort[idx])
							PRINTLN("DELETING PED AND BIKE SINCE THEY ARE NOT ON SCREEN - FAIL SAFE: ", idx)
						ELSE
							CLEAR_PED_TASKS(hijackArgs.pedEscort[idx])
							TASK_VEHICLE_DRIVE_WANDER(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx], 30.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
							DEBUG_PRINT("CLEARING ESCORT'S TASKS BECAUSE HE IS TOO FAR AWAY FROM THE TRANSPORT - FAIL SAFE")
							hijackArgs.bRetaskedToWander[idx] = TRUE
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(hijackArgs.pedEscort[idx])
						IF IS_ENTITY_OCCLUDED(hijackArgs.pedEscort[idx])
							DELETE_PED(hijackArgs.pedEscort[idx])
							PRINTLN("DELETING PED SINCE HE IS NOT ON SCREEN - FAIL SAFE: ", idx)
						ELSE
							CLEAR_PED_TASKS(hijackArgs.pedEscort[idx])
							TASK_SMART_FLEE_PED(hijackArgs.pedEscort[idx], PLAYER_PED_ID(), -1, -1)
							PRINTLN("TASKING ESCORT PEDS TO FLEE - FAIL SAFE: ", idx)
							hijackArgs.bRetaskedToWander[idx] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL UPDATE_HIJACK_ESCORT(LOCATION_DATA& myLocData, HIJACK_ARGS& hijackArgs, EXPLOSIVE_PACKAGE_ARGS& expPackageArgs)

	INT idx, idx2
	BOOL bAllDead = TRUE
	BOOL bPedsOutOfRange
	SEQUENCE_INDEX iSeq
	
	IF hijackArgs.bStopRunningAggroCheck
		bPedsOutOfRange = TRUE
	ELSE
		bPedsOutOfRange = FALSE
	ENDIF
	
	HIJACK_AGGRO_CHECK(myLocData, hijackArgs, expPackageArgs)
	
	CHECK_DROP_DISTANCE_TO_BLOWUP_ESCORTS(hijackArgs, expPackageArgs)
	
	HIJACK_DISTANCE_FAIL_SAFE(hijackArgs)
	
	REPEAT MAX_NUMBER_ESCORT_PEDS idx
		IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
			IF NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx]) 
			
				bAllDead = FALSE
				
				/* If the doors on the trailer are blown open, tasked the escorts to flee... since they are no longer essesntial for 
				 mission completeion, we need to get rid of them at some point. */
				IF hijackArgs.bDoorsOpen
					IF IS_PED_IN_ANY_VEHICLE(hijackArgs.pedEscort[idx])
						CLEAR_PED_TASKS(hijackArgs.pedEscort[idx])
						TASK_VEHICLE_DRIVE_WANDER(hijackArgs.pedEscort[idx], GET_VEHICLE_PED_IS_IN(hijackArgs.pedEscort[idx]), 30.0, DRIVINGMODE_AVOIDCARS)
//						DEBUG_PRINT("CLEARING TASKS ON ESCORTS DUE TO THE PLAYER OPENING TRUCK'S DOORS")
					ELSE
						IF NOT IS_PED_INJURED(hijackArgs.pedEscort[idx]) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_PED_TASKS(hijackArgs.pedEscort[idx])
							TASK_SMART_FLEE_PED(hijackArgs.pedEscort[idx], PLAYER_PED_ID(), 10000, -1)
						ENDIF
					ENDIF
				ENDIF
				
				// If the escorts are not dead but off their vehicle, add blips for the peds.
				IF NOT hijackArgs.bFailSafeRemoved[idx]
					IF NOT DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx])
						IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx]) AND DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
							IF NOT IS_PED_IN_VEHICLE(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx])
							
//								IF DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx])
//									REMOVE_BLIP(hijackArgs.blipEscortCar[idx])
//									DEBUG_PRINT("REMOVING BLIP ON CAR")
//								ENDIF
								
								IF NOT hijackArgs.bRetaskedToWander[idx]
									hijackArgs.blipEscortPed[idx] = ADD_BLIP_FOR_ENTITY(hijackArgs.pedEscort[idx])
									SET_BLIP_COLOUR(hijackArgs.blipEscortPed[idx], BLIP_COLOUR_RED)
									SET_BLIP_SCALE(hijackArgs.blipEscortPed[idx], 0.5)
									DEBUG_PRINT("ADDING BLIP FOR PED - NOT IN VEHICLE")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF hijackArgs.bStopRunningAggroCheck
					// The aggro check has been met, now check if they get too far away from the main truck.
					
					IF NOT hijackArgs.bTaskedToDefend 
						IF hijackArgs.bTruckDead
							IF NOT IS_VECTOR_ZERO(hijackArgs.vDefensivePos)
								REPEAT MAX_NUMBER_ESCORT_PEDS idx2 
									IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx2]) AND NOT IS_PED_INJURED(hijackArgs.pedEscort[idx2])
									AND NOT IS_PED_INJURED(PLAYER_PED_ID()) 
										
										CLEAR_PED_TASKS(hijackArgs.pedEscort[idx2])
										SET_PED_COMBAT_MOVEMENT(hijackArgs.pedEscort[idx], CM_DEFENSIVE)
										SET_PED_COMBAT_ATTRIBUTES(hijackArgs.pedEscort[idx2], CA_LEAVE_VEHICLES, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(hijackArgs.pedEscort[idx2], CA_DO_DRIVEBYS, FALSE)
										SET_PED_SPHERE_DEFENSIVE_AREA(hijackArgs.pedEscort[idx2], hijackArgs.vDefensivePos, 20.0)
										SET_PED_KEEP_TASK(hijackArgs.pedEscort[idx2], TRUE)
										
										CLEAR_SEQUENCE_TASK(iSeq)
										OPEN_SEQUENCE_TASK(iSeq)
											TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR)
											TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
										CLOSE_SEQUENCE_TASK(iSeq)
										IF NOT IS_PED_INJURED(hijackArgs.pedEscort[idx2])
											TASK_PERFORM_SEQUENCE(hijackArgs.pedEscort[idx2], iSeq)
											PRINTLN("TASKING ESCORT PED TO COMBAT PLAYER; idx = ", idx2)
										ENDIF
										CLEAR_SEQUENCE_TASK(iSeq)
									ENDIF
								ENDREPEAT
								
								hijackArgs.bTaskedToDefend = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF hijackArgs.bTaskedToDefend
						REPEAT MAX_NUMBER_ESCORT_PEDS idx2 
							
							IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx2]) AND NOT IS_PED_INJURED(hijackArgs.pedEscort[idx2])
							AND NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF NOT IS_PED_IN_ANY_VEHICLE(hijackArgs.pedEscort[idx2])
									IF NOT hijackArgs.bTaskedToCombat[idx2]
										TASK_COMBAT_PED(hijackArgs.pedEscort[idx2], PLAYER_PED_ID())
										PRINTLN("TASKING ESCORT PED TO DEFEND AREA: ", idx2)
										hijackArgs.bTaskedToCombat[idx2] = TRUE
									ENDIF
									
//									IF DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx])
//										REMOVE_BLIP(hijackArgs.blipEscortCar[idx])
//										DEBUG_PRINT("REMOVING BLIP ON CAR")
//									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
					
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hijackArgs.pedEscort[idx]) < DISTANCE_TO_LOSE_HIJACK_ESCORTS
						bPedsOutOfRange = FALSE
					ELSE
						IF NOT hijackArgs.bRetaskedToWander[idx]
							IF DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx])
								REMOVE_BLIP(hijackArgs.blipEscortPed[idx])
								DEBUG_PRINT("REMOVING BLIP ON PED")
							ENDIF
//							IF DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx])
//								REMOVE_BLIP(hijackArgs.blipEscortCar[idx])
//								DEBUG_PRINT("REMOVING BLIP ON CAR")
//							ENDIF
					
							IF NOT IS_PED_INJURED(hijackArgs.pedEscort[idx]) AND IS_VEHICLE_DRIVEABLE(hijackArgs.vehEscort[idx])
								IF IS_PED_IN_VEHICLE(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx])
									IF IS_ENTITY_OCCLUDED(hijackArgs.pedEscort[idx])
										DELETE_PED(hijackArgs.pedEscort[idx])
										DELETE_VEHICLE(hijackArgs.vehEscort[idx])
										PRINTLN("DELETING PED AND BIKE SINCE THEY ARE NOT ON SCREEN 01: ", idx)
									ELSE
										CLEAR_PED_TASKS(hijackArgs.pedEscort[idx])
										TASK_VEHICLE_DRIVE_WANDER(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx], 30.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
										DEBUG_PRINT("CLEARING ESCORT'S TASKS BECAUSE HE IS TOO FAR AWAY FROM THE TRANSPORT")
										hijackArgs.bRetaskedToWander[idx] = TRUE
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_PED_INJURED(hijackArgs.pedEscort[idx])
									IF IS_ENTITY_OCCLUDED(hijackArgs.pedEscort[idx])
										DELETE_PED(hijackArgs.pedEscort[idx])
										PRINTLN("DELETING PED SINCE HE IS NOT ON SCREEN 02: ", idx)
									ELSE
										CLEAR_PED_TASKS(hijackArgs.pedEscort[idx])
										TASK_SMART_FLEE_PED(hijackArgs.pedEscort[idx], PLAYER_PED_ID(), -1, -1)
										PRINTLN("TASKING ESCORT PEDS TO FLEE: ", idx)
										hijackArgs.bRetaskedToWander[idx] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// If a ped is dead, remove blip on ped or vehicle.
				IF DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx])
					REMOVE_BLIP(hijackArgs.blipEscortPed[idx])
					DEBUG_PRINT("REMOVING BLIP ON PED")
				ENDIF
//				IF DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx])
//					REMOVE_BLIP(hijackArgs.blipEscortCar[idx])
//					DEBUG_PRINT("REMOVING BLIP ON CAR")
//				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUMBER_ESCORT_VEHICLES idx
		IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
			IF IS_ENTITY_DEAD(hijackArgs.vehEscort[idx])
//				IF DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx])
//					REMOVE_BLIP(hijackArgs.blipEscortCar[idx])
//					DEBUG_PRINT("REMOVING BLIP ON CAR")
//				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	IF bAllDead OR bPedsOutOfRange OR hijackArgs.bDoorsOpen
	
//		IF bAllDead
//			DEBUG_PRINT("bAllDead IS TRUE IN HIJACK MODE")
//		ENDIF
//		IF bPedsOutOfRange
//			DEBUG_PRINT("bPedsOutOfRange IS TRUE IN HIJACK MODE")
//		ENDIF
	
		// Remove blips on cars
		REPEAT COUNT_OF(hijackArgs.vehEscort) idx
//			IF DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx])
//				REMOVE_BLIP(hijackArgs.blipEscortCar[idx])
//				DEBUG_PRINT("REMOVING ESCORT VEHICLE BLIPS")
//			ENDIF
			
			IF NOT IS_PED_INJURED(hijackArgs.pedEscort[idx]) AND IS_VEHICLE_DRIVEABLE(hijackArgs.vehEscort[idx])
				IF IS_PED_IN_VEHICLE(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx])
					TASK_VEHICLE_DRIVE_WANDER(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx], 30.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
				ENDIF
			ENDIF
		ENDREPEAT
		// Remove blips on peds
		REPEAT COUNT_OF(hijackArgs.pedEscort) idx
			IF DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx])
				REMOVE_BLIP(hijackArgs.blipEscortPed[idx])
				DEBUG_PRINT("REMOVING ESCORT PED BLIPS")
			ENDIF
		ENDREPEAT
		
		hijackArgs.bEscortsDone = TRUE
//		DEBUG_PRINT("hijackArgs.bEscortsDone = TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DEBUG_DRAW_VECTORS(LOCATION_DATA& myLocData)
	DRAW_DEBUG_SPHERE(myLocData.endData[1].vDestination[0], 10.0)
	DRAW_DEBUG_SPHERE(myLocData.endData[1].vDestination[1], 10.0)
	DRAW_DEBUG_SPHERE(myLocData.endData[1].vDestination[2], 10.0)
	DRAW_DEBUG_SPHERE(myLocData.endData[1].vDestination[3], 10.0)
	DRAW_DEBUG_SPHERE(myLocData.endData[1].vDestination[4], 10.0)
	DRAW_DEBUG_SPHERE(myLocData.endData[1].vDestination[5], 10.0)
	DRAW_DEBUG_SPHERE(myLocData.endData[1].vDestination[6], 10.0)
ENDPROC

PROC DELETE_CANISTERS(EXPLOSIVE_PACKAGE_ARGS& expPackageArgs)
	INT idx
	REPEAT MAX_NUMBER_EXPLOSIVES idx
		IF DOES_ENTITY_EXIST(expPackageArgs.oExpPackage[idx])
			IF IS_ENTITY_ATTACHED(expPackageArgs.oExpPackage[idx]) 
				DELETE_OBJECT(expPackageArgs.oExpPackage[idx])
				DEBUG_PRINT("DELETING CANNISTERS")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MOVE_CANISTERS(LOCATION_DATA& myLocData, EXPLOSIVE_PACKAGE_ARGS& expPackageArgs, VEHICLE_INDEX& myVehicle)
	INT iCanistersRemaining, idx
	VECTOR vPlayerVehiclePosition, vCanisterPosition[MAX_NUMBER_EXPLOSIVES]
	FLOAT fPlayerVehicleHeading
	FLOAT fGroundZ[MAX_NUMBER_EXPLOSIVES]
	
	iCanistersRemaining = (MAX_NUMBER_EXPLOSIVES - expPackageArgs.iNumExplosivePackageDropped)
	PRINTLN("iCanistersRemaining = ", iCanistersRemaining)
	
	IF IS_VEHICLE_DRIVEABLE(myVehicle)
		vPlayerVehiclePosition = GET_ENTITY_COORDS(myVehicle)
		fPlayerVehicleHeading = GET_ENTITY_HEADING(myVehicle)
	ENDIF
	
	vCanisterPosition[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerVehiclePosition, fPlayerVehicleHeading, <<2,0,0>>)
	GET_GROUND_Z_FOR_3D_COORD(vCanisterPosition[0], fGroundZ[0])
	vCanisterPosition[0] = <<vCanisterPosition[0].x, vCanisterPosition[0].y, fGroundZ[0]>>
	PRINTLN("vCanisterPosition[0] = ", vCanisterPosition[0])
	
	vCanisterPosition[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerVehiclePosition, fPlayerVehicleHeading, <<GET_RANDOM_FLOAT_IN_RANGE(2.2, 2.5),GET_RANDOM_FLOAT_IN_RANGE(-0.5,-0.9),0>>)
	GET_GROUND_Z_FOR_3D_COORD(vCanisterPosition[1], fGroundZ[1])
	vCanisterPosition[1] = <<vCanisterPosition[1].x, vCanisterPosition[1].y, fGroundZ[1]>>
	PRINTLN("vCanisterPosition[1] = ", vCanisterPosition[1])
	
	vCanisterPosition[2] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerVehiclePosition, fPlayerVehicleHeading, <<GET_RANDOM_FLOAT_IN_RANGE(2.0,2.9),GET_RANDOM_FLOAT_IN_RANGE(-1.2,-1.5),0>>)
	GET_GROUND_Z_FOR_3D_COORD(vCanisterPosition[2], fGroundZ[2])
	vCanisterPosition[2] = <<vCanisterPosition[2].x, vCanisterPosition[2].y, fGroundZ[2]>>
	PRINTLN("vCanisterPosition[2] = ", vCanisterPosition[2])
	
	vCanisterPosition[3] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerVehiclePosition, fPlayerVehicleHeading, <<GET_RANDOM_FLOAT_IN_RANGE(2.0,2.5),GET_RANDOM_FLOAT_IN_RANGE(-1.65,-2.0),0>>)
	GET_GROUND_Z_FOR_3D_COORD(vCanisterPosition[3], fGroundZ[3])
	vCanisterPosition[3] = <<vCanisterPosition[3].x, vCanisterPosition[3].y, fGroundZ[3]>>
	PRINTLN("vCanisterPosition[3] = ", vCanisterPosition[3])
	
	REPEAT iCanistersRemaining idx
		IF NOT DOES_ENTITY_EXIST(tempCanister[idx])
			tempCanister[idx] = CREATE_OBJECT(myLocData.mnHijackBomb, vCanisterPosition[idx])
			SET_ENTITY_ROTATION(tempCanister[idx], <<0,0,0>>)
			PRINTLN("CREATING CANISTER = ", idx)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC RUN_HIJACK_WEAPONS_EFFECTS_BARRELS(HIJACK_ARGS& hijackArgs, FLOAT fHealth, VEHICLE_INDEX& myVehicle, 
structPedsForConversation& MyLocalPedStruct)

//	PRINTSTRING("fHealth = ")
//	PRINTFLOAT(fHealth)
//	PRINTNL()
	
	IF fHealth <= 0.5
		EXPLODE_VEHICLE(myVehicle)
		DEBUG_PRINT("EXPLODING VEHICLE VIA HIJACK")
	ENDIF

	SWITCH hijackEffectStages 
		CASE 0
			IF fHealth < 90.0
				hijackArgs.PTFXChem03 = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojdg5_barrels_smoke", hijackArgs.oWeaponsCargo, <<0,0,0.4>>, <<0,0,0>>, 0.75)
				IF DOES_PARTICLE_FX_LOOPED_EXIST(hijackArgs.PTFXChem03)
					SET_PARTICLE_FX_LOOPED_COLOUR(hijackArgs.PTFXChem03, 0.8, 0.18, 0.19)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(hijackArgs.PTFXChem03, "damage", 0.5)
				ENDIF
				
				RESTART_TIMER_NOW(tEffectsTimer)
				
				DEBUG_PRINT("STARTING SMOKE EFFECT")
				hijackEffectStages++ 
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------------------
		CASE 1
			IF fHealth <50.0
				IF IS_TIMER_STARTED(tEffectsTimer)
					IF TIMER_DO_WHEN_READY(tEffectsTimer, 5.0)
						IF DOES_PARTICLE_FX_LOOPED_EXIST(hijackArgs.PTFXChem03)
							SET_PARTICLE_FX_LOOPED_EVOLUTION(hijackArgs.PTFXChem03, "damage", 0.65)
							RESTART_TIMER_NOW(tEffectsTimer)
							DEBUG_PRINT("INCREASING SMOKE 1")
							hijackEffectStages++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------------------
		CASE 2
			IF fHealth <30.0
				IF IS_TIMER_STARTED(tEffectsTimer)
					IF TIMER_DO_WHEN_READY(tEffectsTimer, 2.0)
						IF DOES_PARTICLE_FX_LOOPED_EXIST(hijackArgs.PTFXChem03)
							SET_PARTICLE_FX_LOOPED_EVOLUTION(hijackArgs.PTFXChem03, "damage", 0.8)
							DEBUG_PRINT("INCREASING SMOKE 2")
						ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = GET_PLAYER_PED_MODEL(CHAR_TREVOR)
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, NULL, "TREVOR")
								CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_EXP", CONV_PRIORITY_VERY_HIGH)
								PRINTLN("PLAYING EXPLODE LINE")
							ENDIF
						ENDIF
						
						hijackEffectStages++ 
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------------------
		CASE 3
			IF fHealth < 10.0
				// Start on barrel 1 - bubbles
				hijackArgs.PTFXChem01 = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojdg5_barrels", hijackArgs.oWeaponsCargo, <<0,0,0.4>>, <<0,0,0>>, 0.75)
				IF DOES_PARTICLE_FX_LOOPED_EXIST(hijackArgs.PTFXChem01)
					SET_PARTICLE_FX_LOOPED_COLOUR(hijackArgs.PTFXChem01, 0.8, 0.18, 0.19)
				ENDIF
				
				RESTART_TIMER_NOW(tEffectsTimer)
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST(hijackArgs.PTFXChem03)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(hijackArgs.PTFXChem03, "damage", 1.0)
				ENDIF
			
				DEBUG_PRINT("INCREASING SMOKE 3")
				
				hijackEffectStages++ 
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------------------
		CASE 4
			IF IS_TIMER_STARTED(tEffectsTimer)
				IF TIMER_DO_WHEN_READY(tEffectsTimer, 5.0)
					IF DOES_PARTICLE_FX_LOOPED_EXIST(hijackArgs.PTFXChem01)
						STOP_PARTICLE_FX_LOOPED(hijackArgs.PTFXChem01)
//						DEBUG_PRINT("STOPPING BUBBLES")
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
		
ENDPROC

PROC RUN_HIJACK_WEAPONS_EFFECTS_CRATE(HIJACK_ARGS& hijackArgs, FLOAT fHealth, VEHICLE_INDEX& myVehicle)

//	PRINTSTRING("fHealth = ")
//	PRINTFLOAT(fHealth)
//	PRINTNL()

	SWITCH hijackEffectStages 
		CASE 0
			IF fHealth < 70.0 AND fHealth > 30.0
				hijackArgs.PTFXChem01 = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_smoke_grenade", hijackArgs.oWeaponsCargo, <<0,0,0>>, <<0,0,0>>, 1.0)
				SET_PARTICLE_FX_LOOPED_COLOUR(hijackArgs.PTFXChem01, 0.8, 0.18, 0.19)
				DEBUG_PRINT("STARTED FLARE01")
				PRINTSTRING("WEAPONS COORDINATES = ")
				PRINTVECTOR(GET_ENTITY_COORDS(hijackArgs.oWeaponsCargo))
				PRINTNL()
				hijackEffectStages++ 
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------------------
		CASE 1
			IF fHealth < 30.0 AND fHealth > 0
				hijackArgs.PTFXChem02 = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_smoke_grenade", hijackArgs.oWeaponsCargo, <<0,0,0>>, <<0,0,0>>, 1.5)
				SET_PARTICLE_FX_LOOPED_COLOUR(hijackArgs.PTFXChem02, 0, 255, 0)
				DEBUG_PRINT("STARTED FLARE02")
				PRINTSTRING("WEAPONS COORDINATES = ")
				PRINTVECTOR(GET_ENTITY_COORDS(hijackArgs.oWeaponsCargo))
				PRINTNL()
				hijackEffectStages++ 
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------------------
		CASE 2
			IF fHealth <= 0
				hijackArgs.PTFXChem01 = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fbi_ext_blaze", hijackArgs.oWeaponsCargo, <<0,0,0>>, <<0,0,0>>, 2.0)
				hijackArgs.PTFXChem02 = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_drug_traffic_flare_L", hijackArgs.oWeaponsCargo, <<0,0,0>>, <<0,0,0>>, 1.0)
				SET_PARTICLE_FX_LOOPED_COLOUR(hijackArgs.PTFXChem01, 0, 255, 0)
				SET_PARTICLE_FX_LOOPED_COLOUR(hijackArgs.PTFXChem02, 0.8, 0.18, 0.19)
				
				EXPLODE_VEHICLE(myVehicle)
				
				DEBUG_PRINT("STARTED FIRE")
				PRINTSTRING("WEAPONS COORDINATES = ")
				PRINTVECTOR(GET_ENTITY_COORDS(hijackArgs.oWeaponsCargo))
				PRINTNL()
				
				hijackEffectStages++
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------------------
		CASE 3
		BREAK
	ENDSWITCH
		
ENDPROC

PROC HIJACK_HANDLE_PLAYER_LEAVING_VEHICLE(HIJACK_ARGS& hijackArgs, BOOL& bOutOfVehicle)
	INT idx
	
	IF bOkayToBlipEnemies
		IF bOutOfVehicle
			IF NOT hijackArgs.bTruckDead
				// Truck
				IF DOES_BLIP_EXIST(hijackArgs.blipTruck)
					REMOVE_BLIP(hijackArgs.blipTruck)
					PRINTLN("REMOVING BLIP hijackArgs.blipTruck")
				ELSE
//					PRINTLN("hijackArgs.blipTruck DOES NOT EXIST")
				ENDIF
				// Escort peds/vehicles
				IF g_savedGlobals.sTraffickingData.iGroundRank = 3
					REPEAT 2 idx
//						IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
//							IF DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx])
//								REMOVE_BLIP(hijackArgs.blipEscortCar[idx])
//							ENDIF
//						ENDIF
						IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
							IF DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx])
								REMOVE_BLIP(hijackArgs.blipEscortPed[idx])
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					REPEAT 4 idx
						IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
//							IF DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx])
//								REMOVE_BLIP(hijackArgs.blipEscortCar[idx])
//							ENDIF
							IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
								IF DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx])
									REMOVE_BLIP(hijackArgs.blipEscortPed[idx])
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ELSE
			IF NOT hijackArgs.bTruckDead
				// Truck
				IF NOT DOES_BLIP_EXIST(hijackArgs.blipTruck)
					hijackArgs.blipTruck = ADD_BLIP_FOR_ENTITY(hijackArgs.vehTruck)
					SET_BLIP_COLOUR(hijackArgs.blipTruck, BLIP_COLOUR_RED)
				ENDIF
				// Escort peds/vehicles
				IF g_savedGlobals.sTraffickingData.iGroundRank = 3
					REPEAT 2 idx
						// Vehicles
//						IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
//							IF NOT DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx]) AND NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx])
//								hijackArgs.blipEscortCar[idx] = ADD_BLIP_FOR_ENTITY(hijackArgs.vehEscort[idx])
//								SET_BLIP_COLOUR(hijackArgs.blipEscortCar[idx], BLIP_COLOUR_RED)
//								SET_BLIP_SCALE(hijackArgs.blipEscortCar[idx], 0.5)
//							ENDIF
//						ENDIF
						// Peds
						IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
							IF NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx])
								IF NOT DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx]) AND IS_PED_IN_VEHICLE(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx])
									hijackArgs.blipEscortPed[idx] = ADD_BLIP_FOR_ENTITY(hijackArgs.pedEscort[idx])
									SET_BLIP_COLOUR(hijackArgs.blipEscortPed[idx], BLIP_COLOUR_RED)
									SET_BLIP_SCALE(hijackArgs.blipEscortPed[idx], 0.5)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					REPEAT 4 idx
						// Vehicles
//						IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
//							IF NOT DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx]) AND NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx])
//								hijackArgs.blipEscortCar[idx] = ADD_BLIP_FOR_ENTITY(hijackArgs.vehEscort[idx])
//								SET_BLIP_COLOUR(hijackArgs.blipEscortCar[idx], BLIP_COLOUR_RED)
//								SET_BLIP_SCALE(hijackArgs.blipEscortCar[idx], 0.5)
//							ENDIF
//						ENDIF
						// Peds
						IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
							IF NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx])
								IF NOT DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx]) AND IS_PED_IN_VEHICLE(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx])
									hijackArgs.blipEscortPed[idx] = ADD_BLIP_FOR_ENTITY(hijackArgs.pedEscort[idx])
									SET_BLIP_COLOUR(hijackArgs.blipEscortPed[idx], BLIP_COLOUR_RED)
									SET_BLIP_SCALE(hijackArgs.blipEscortPed[idx], 0.5)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HIJACK_HANDLE_PLAYER_GOING_WANTED(HIJACK_ARGS& hijackArgs, BOOL& bOutOfVehicle, BOOL& bWanted)
	INT idx
	
	IF bOkayToBlipEnemies
		IF bWanted
			IF DOES_BLIP_EXIST(hijackArgs.blipTruck)
				REMOVE_BLIP(hijackArgs.blipTruck)
			ENDIF
			// Escort peds/vehicles
			IF g_savedGlobals.sTraffickingData.iGroundRank = 3
				REPEAT 2 idx
//					IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
//						IF DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx])
//							REMOVE_BLIP(hijackArgs.blipEscortCar[idx])
//						ENDIF
//					ENDIF
					IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
						IF DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx])
							REMOVE_BLIP(hijackArgs.blipEscortPed[idx])
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				REPEAT 4 idx
					IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
//						IF DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx])
//							REMOVE_BLIP(hijackArgs.blipEscortCar[idx])
//						ENDIF
						IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
							IF DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx])
								REMOVE_BLIP(hijackArgs.blipEscortPed[idx])
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ELSE
			IF NOT hijackArgs.bTruckDead AND NOT bOutOfVehicle
				IF NOT DOES_BLIP_EXIST(hijackArgs.blipTruck)
					hijackArgs.blipTruck = ADD_BLIP_FOR_ENTITY(hijackArgs.vehTruck)
					SET_BLIP_COLOUR(hijackArgs.blipTruck, BLIP_COLOUR_RED)
				ENDIF
				// Escort peds/vehicles
				IF g_savedGlobals.sTraffickingData.iGroundRank = 3
					REPEAT 2 idx
						// Vehicles
//						IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
//							IF NOT DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx]) AND NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx])
//								hijackArgs.blipEscortCar[idx] = ADD_BLIP_FOR_ENTITY(hijackArgs.vehEscort[idx])
//								SET_BLIP_COLOUR(hijackArgs.blipEscortCar[idx], BLIP_COLOUR_RED)
//								SET_BLIP_SCALE(hijackArgs.blipEscortCar[idx], 0.5)
//							ENDIF
//						ENDIF
						// Peds
						IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
							IF NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx])
								IF NOT DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx]) AND IS_PED_IN_VEHICLE(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx])
									hijackArgs.blipEscortPed[idx] = ADD_BLIP_FOR_ENTITY(hijackArgs.pedEscort[idx])
									SET_BLIP_COLOUR(hijackArgs.blipEscortPed[idx], BLIP_COLOUR_RED)
									SET_BLIP_SCALE(hijackArgs.blipEscortPed[idx], 0.5)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					REPEAT 4 idx
						// Vehicles
//						IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
//							IF NOT DOES_BLIP_EXIST(hijackArgs.blipEscortCar[idx]) AND NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx])
//								hijackArgs.blipEscortCar[idx] = ADD_BLIP_FOR_ENTITY(hijackArgs.vehEscort[idx])
//								SET_BLIP_COLOUR(hijackArgs.blipEscortCar[idx], BLIP_COLOUR_RED)
//								SET_BLIP_SCALE(hijackArgs.blipEscortCar[idx], 0.5)
//							ENDIF
//						ENDIF
						// Peds
						IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
							IF NOT IS_ENTITY_DEAD(hijackArgs.pedEscort[idx])
								IF NOT DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx]) AND IS_PED_IN_VEHICLE(hijackArgs.pedEscort[idx], hijackArgs.vehEscort[idx])
									hijackArgs.blipEscortPed[idx] = ADD_BLIP_FOR_ENTITY(hijackArgs.pedEscort[idx])
									SET_BLIP_COLOUR(hijackArgs.blipEscortPed[idx], BLIP_COLOUR_RED)
									SET_BLIP_SCALE(hijackArgs.blipEscortPed[idx], 0.5)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_EXPLOSIVE_HELP(VEHICLE_INDEX& myVehicle, structPedsForConversation& MyLocalPedStruct)
	IF g_savedGlobals.sTraffickingData.iGroundRank = 3
		IF NOT bOscarTellAboutC4
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GR12", CONV_PRIORITY_VERY_HIGH)
						DEBUG_PRINT("OSCAR TELLING ABOUT C4")
						bOscarTellAboutC4 = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bHelpTellAboutC4
			IF NOT bOscarTellAboutC4 AND bPrintObjectiveBlowOpenDoors
				IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(myVehicle)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							PRINT_HELP("DTRFKGR_HELP_13")
							DEBUG_PRINT("PRINTING HELP ON C4")
							bHelpTellAboutC4 = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ESCORTS_DAMAGED_CANISTERS(HIJACK_ARGS& hijackArgs, EXPLOSIVE_PACKAGE_ARGS& expPackageArgs)
	INT idx, idx2

	REPEAT MAX_NUMBER_ESCORT_PEDS idx
		IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
			REPEAT MAX_NUMBER_EXPLOSIVES idx2
				IF DOES_ENTITY_EXIST(expPackageArgs.oExpPackage[idx2])
					IF IS_ENTITY_ATTACHED(expPackageArgs.oExpPackage[idx2])
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(expPackageArgs.oExpPackage[idx2], hijackArgs.pedEscort[idx])
							IF hijackArgs.bOkayToRunCanisterDamageTest
								PRINTLN("hijackArgs.bOkayToRunCanisterDamageTest = TRUE")
							ENDIF	
							PRINTSTRING("ESCORT PED: ")
							PRINTINT(idx)
							PRINTSTRING(" - HAS DAMAGED CANISTER: ")
							PRINTINT(idx2)
							PRINTNL()
							ADD_EXPLOSION(GET_ENTITY_COORDS(expPackageArgs.oExpPackage[idx2]), EXP_TAG_GAS_CANISTER)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

PROC CLEAR_ESCORTS(HIJACK_ARGS& hijackArgs)
	INT idx

	REPEAT MAX_NUMBER_ESCORT_PEDS idx
		IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx]) 
			DELETE_PED(hijackArgs.pedEscort[idx])
			PRINTLN("DELETING ESCORT PEDS: ", idx)
		ENDIF
		IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
			DELETE_VEHICLE(hijackArgs.vehEscort[idx])
			PRINTLN("DELETING ESCORT BIKES: ", idx)
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEAR_ESCORTS_BIKES(HIJACK_ARGS& hijackArgs)
	INT idx

	REPEAT MAX_NUMBER_ESCORT_PEDS idx
		IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx])
//			IF NOT IS_ENTITY_ON_SCREEN(hijackArgs.vehEscort[idx]) 
				DELETE_VEHICLE(hijackArgs.vehEscort[idx])
				PRINTLN("DELETING ESCORT BIKES: ", idx)
//			ELSE
//				SET_VEHICLE_AS_NO_LONGER_NEEDED(hijackArgs.vehEscort[idx])
//				PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED: ", idx)
//			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC BLIP_ALL_HIJACK_ENEMIES(HIJACK_ARGS& hijackArgs)
	INT idx
	
	IF NOT bDoneBlipingEnemies
		REPEAT MAX_NUMBER_ESCORT_VEHICLES idx
			IF DOES_ENTITY_EXIST(hijackArgs.pedEscort[idx])
				IF NOT DOES_BLIP_EXIST(hijackArgs.blipEscortPed[idx])
					hijackArgs.blipEscortPed[idx] = ADD_BLIP_FOR_ENTITY(hijackArgs.pedEscort[idx])
					SET_BLIP_COLOUR(hijackArgs.blipEscortPed[idx], BLIP_COLOUR_RED)
					SET_BLIP_SCALE(hijackArgs.blipEscortPed[idx], 0.5)
					SET_BLIP_NAME_FROM_TEXT_FILE(hijackArgs.blipEscortPed[idx], "DTRFKGR_BLIP06")
					PRINTLN("ADDING ESCORT BLIPS: ", idx)
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF DOES_ENTITY_EXIST(hijackArgs.vehTruck)
			IF NOT DOES_BLIP_EXIST(hijackArgs.blipTruck)
				hijackArgs.blipTruck = ADD_BLIP_FOR_ENTITY(hijackArgs.vehTruck)
				SET_BLIP_COLOUR(hijackArgs.blipTruck, BLIP_COLOUR_RED)
				SET_BLIP_NAME_FROM_TEXT_FILE(hijackArgs.blipTruck, "DTRFKGR_BLIP05")
				PRINTLN("ADDING BLIP FOR TRANSPORT TRUCK")
			ENDIF
		ENDIF
		
		bDoneBlipingEnemies = TRUE
	ENDIF
ENDPROC


FUNC BOOL UPDATE_HIJACK(LOCATION_DATA& myLocData, HIJACK_ARGS& hijackArgs, HIJACK_STATE& hijackStages, VECTOR& vCurDestObjective[], 
BLIP_INDEX& myLocationBlip[], TEXT_LABEL_31& sLastObjective, VEHICLE_INDEX& myVehicle, BOOL& bDrugsLoaded, 
EXPLOSIVE_PACKAGE_ARGS& expPackageArgs, HIJACK_PRINTS& hijackPrintStates, HIJACK_KILL_CAM& runHijackKillCamStages, 
CUTSCENE_ARGS& cutArgs, BOOL& bDoDelicate, CustomCutsceneCaller& fpHijackCutscenePush, 
BLIP_INDEX& myVehicleBlip, BOOL& bOutOfVehicle, BOOL& bWanted, structPedsForConversation& MyLocalPedStruct)
	
	// Trailer
	VECTOR vTrailerPos //, vTrailerForwardVector
//	VECTOR vTrailerRot
	FLOAT fTrailerHeading
	VECTOR vTrailerPosOffset = <<0,0,5.0>>
	VECTOR vTrailerPosOffset01 = <<0,-22,3.5>>
	VECTOR vTrailerVehicleNodeOffset = <<0,-18,0>>
	VECTOR vForwardVec
	VECTOR vTriggerCutscenePos
//	VECTOR vMinExp, vMaxExp
//#IF IS_DEBUG_BUILD
//	VECTOR vNewRotation
//#ENDIF	//	IS_DEBUG_BUILD
	VECTOR vClosestNodePos
	VECTOR vExp01, vExp02
	VECTOR vSpeedNodeOffset
	FLOAT fPlayerVehicleHeading, fOrientationValue
	INT idx
	INT iNumLanes
	FLOAT fRadiusCheck = 2.7
	

	// Check for player's position to display the appropriate help prints.
	UPDATE_HIJACK_PRINTS(hijackArgs, hijackPrintStates, myVehicle, expPackageArgs)
	
	// Rear weapon mechanic update.
	IF hijackStages < HIJACK_STATE_05 OR bToggleCam
		DROP_EXPLOSIVE_PACKAGE(expPackageArgs, myVehicle, hijackArgs)
	ENDIF
	
	// Run update on the enemy escort peds
	UPDATE_HIJACK_ESCORT(myLocData, hijackArgs, expPackageArgs)
	
	HIJACK_HANDLE_PLAYER_LEAVING_VEHICLE(hijackArgs, bOutOfVehicle)
	HIJACK_HANDLE_PLAYER_GOING_WANTED(hijackArgs, bOutOfVehicle, bWanted)
	
	// Adding in block to tell the player to enter their vehicle if they have killed the escorts 
	// but haven't finished off the truck... trying to avoid the player getting in another vehicle and chasing the truck.
	IF hijackStages < HIJACK_STATE_06
		// If we've aggroed and the truck isn't dead yet, set HijackActive to False to tell the player to get back in their car.
		IF hijackArgs.bStopRunningAggroCheck AND NOT hijackArgs.bTruckDead
			hijackArgs.bHijackActive = FALSE
		// If we've aggroed and the truck is dead, set HijackActive to True so we do not run UPDATE_MISSION_CAR.
		ELIF hijackArgs.bStopRunningAggroCheck AND hijackArgs.bTruckDead
			IF DOES_BLIP_EXIST(myVehicleBlip)
				REMOVE_BLIP(myVehicleBlip)
				DEBUG_PRINT("REMOVING myVehicleBlip VIA HIJACK")
			ENDIF
			hijackArgs.bHijackActive = TRUE
		ENDIF
	ENDIF
	
//	INT iTruckHealth
//	iTruckHealth = GET_ENTITY_HEALTH(hijackArgs.vehTruck)
//	PRINTLN("iTruckHealth = ", iTruckHealth)
	
//	IF IS_DEBUG_KEY_PRESSED(KEY_K, KEYBOARD_MODIFIER_NONE, "")
//		IF NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer) 
//			VECTOR vTruckRot
//			vTruckRot = GET_ENTITY_ROTATION(hijackArgs.vehTruck)
//			vTrailerRot = GET_ENTITY_ROTATION(hijackArgs.vehTrailer)
//			SET_ENTITY_ROTATION(hijackArgs.vehTruck, <<vTruckRot.x, vTruckRot.y - 90, vTruckRot.z>>)
//			SET_ENTITY_ROTATION(hijackArgs.vehTrailer, <<vTrailerRot.x, vTrailerRot.y - 90, vTrailerRot.z>>)
//			PRINTLN("SETTING NEW ROTATION = ", <<vTrailerRot.x, vTrailerRot.y - 90, vTrailerRot.z>>)
//		ENDIF
//	ENDIF
	
//	IF NOT hijackArgs.bTruckDead
//		IF NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
//			vTrailerRot = GET_ENTITY_ROTATION(hijackArgs.vehTrailer)
//			PRINTLN("NORMAL ROTATION = ", vTrailerRot)
//		ENDIF
//	ENDIF
	
	// Check to see if canisters are dead and attached, if so blow up the truck.
	IF hijackArgs.bOkayToRunCanisterDamageTest
		REPEAT MAX_NUMBER_EXPLOSIVES idx
			IF DOES_ENTITY_EXIST(expPackageArgs.oExpPackage[idx])
				IF IS_ENTITY_ATTACHED(expPackageArgs.oExpPackage[idx]) 
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(expPackageArgs.oExpPackage[idx], PLAYER_PED_ID())
					OR HAS_ESCORTS_DAMAGED_CANISTERS(hijackArgs, expPackageArgs)
						EXPLODE_VEHICLE(myVehicle)
//						PRINTLN("PLAYER HAS DAMAGED CANISTER, EXPLODING VEHICLE")
					ENDIF
					IF IS_ENTITY_DEAD(expPackageArgs.oExpPackage[idx])
						EXPLODE_VEHICLE(myVehicle)
						PRINTLN("EXPLODING VEHICLE DUE TO DEAD CANISTERS")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
		
//	DEBUG_DRAW_VECTORS(myLocData)
	
	SWITCH hijackStages
		CASE HIJACK_STATE_01
		
			IF NOT bTaskedTransportAI
				IF HIJACK_TASK_TRANSPORT_AND_ESCORTS(myLocData, hijackArgs)
					bTaskedTransportAI = TRUE
					PRINTLN("bTaskedTransportAI = TRUE")
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
				fTransportHeading = GET_ENTITY_HEADING(hijackArgs.vehTrailer)
				PRINTLN("fTransportHeading = ", fTransportHeading)
			ENDIF
			
			// MAIN CHECK FOR HIJACK
			IF UPDATE_HIJACK_STOP_CONDITIONS(myLocData, hijackArgs, expPackageArgs, runHijackKillCamStages, myVehicle)
			
				hijackArgs.bTruckDead = TRUE
				DEBUG_PRINT("hijackArgs.bTruckDead = TRUE")
	
				IF NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
					vTrailerPos = GET_ENTITY_COORDS(hijackArgs.vehTrailer)
					fTrailerHeading = GET_ENTITY_HEADING(hijackArgs.vehTrailer)
					vSpeedNodeOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, <<0,-20,0>>)
					hijackArgs.iSpeedNode = ADD_ROAD_NODE_SPEED_ZONE(vSpeedNodeOffset, 40.0, 0)
					SET_ROADS_IN_AREA(<<vTrailerPos.x - 100, vTrailerPos.y -100, vTrailerPos.z - 100>>,<<vTrailerPos.x + 100, vTrailerPos.y + 100, vTrailerPos.z + 100>>, FALSE)
				ENDIF
				
				DEBUG_PRINT("GOING TO STATE - HIJACK_STATE_02")
				hijackStages = HIJACK_STATE_02
			ENDIF
		BREAK
		//=======================================================================================================================================
		CASE HIJACK_STATE_02
//			IF UPDATE_HIJACK_ESCORT(myLocData, hijackArgs, expPackageArgs)
				DEBUG_PRINT("INSIDE STATE - HIJACK_STATE_02")
				
				SET_MAX_WANTED_LEVEL(0)
				DEBUG_PRINT("SETTING MAX WANTED LEVEL TO ZERO IN HIJACK MODE")
				
				// Add blip for back of the trailer
				hijackArgs.blipTrailerBack = ADD_BLIP_FOR_ENTITY(hijackArgs.vehTrailer)
				SET_BLIP_COLOUR(hijackArgs.blipTrailerBack, BLIP_COLOUR_GREEN)
				SET_BLIP_NAME_FROM_TEXT_FILE(hijackArgs.blipTrailerBack, "DTRFKGR_BLIP10")
				
				// Tell player to open the back doors of the trailer.
				CLEAR_PRINTS()
				
				IF g_savedGlobals.sTraffickingData.iGroundRank = 3
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_H_EXP", CONV_PRIORITY_VERY_HIGH)
				ENDIF
				
				SETTIMERA(0)
				
				DEBUG_PRINT("GOING TO STATE - HIJACK_STATE_03")
				hijackStages = HIJACK_STATE_03
//			ENDIF
		BREAK
		//=======================================================================================================================================
		CASE HIJACK_STATE_03
		
			IF NOT bFreezedTruck
				IF IS_VEHICLE_ON_ALL_WHEELS(hijackArgs.vehTrailer)
					IF IS_ENTITY_DEAD(hijackArgs.vehTruck)
						DETACH_VEHICLE_FROM_TRAILER(hijackArgs.vehTruck)
						FREEZE_ENTITY_POSITION(hijackArgs.vehTrailer, TRUE)
						PRINTLN("FREEZING TRUCK'S POSITION")
						bFreezedTruck = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR TIMERA() > 7000
				PRINT_NOW("DTRFKGR_14", DEFAULT_GOD_TEXT_TIME, 1)
				
				SETTIMERA(0)
				
				DEBUG_PRINT("INSIDE STATE - HIJACK_STATE_04")
				hijackStages = HIJACK_STATE_04
			ENDIF
		BREAK
		//=======================================================================================================================================
		CASE HIJACK_STATE_04
		
			IF NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
				IF IS_VEHICLE_ALMOST_STOPPED_GROUND(hijackArgs.vehTrailer)
					vTrailerPos = GET_ENTITY_COORDS(hijackArgs.vehTrailer)
					fTrailerHeading = GET_ENTITY_HEADING(hijackArgs.vehTrailer)
					PRINTLN("HEADING AFTER STOPPING = ", fTrailerHeading)
					
//					vExp01 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, <<-1.8,-6,-1.5>>)
					vExp01 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, <<0,-6,-0.25>>)
					hijackArgs.vDefensivePos = vExp01
					
					vExp02 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, <<1.8,-7,1.5>>)
					vExp02 = vExp02
//					PRINTLN("GRABBING OFFSETS FOR EXPLOSION DETECTION")
				ENDIF
			ENDIF
			
			// Reprint objective to blow open trailer doors.
			IF NOT bPrintObjectiveBlowOpenDoors
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT ARE_VECTORS_EQUAL(vTrailerPos, <<0,0,0>>)
						IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, <<0,-7,0>>)) < 5.0
							CLEAR_PRINTS()
							CLEAR_HELP()
							PRINT_NOW("DTRFKGR_14a", DEFAULT_GOD_TEXT_TIME, 1)
							
							bPrintObjectiveBlowOpenDoors = TRUE
						ELSE
							IF NOT bPrintExtraHelp
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
									CLEAR_HELP()
									PRINT_HELP("DTRFKGR_HELP_15")
									bPrintExtraHelp = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bPrintObjectiveBlowOpenDoors
				HANDLE_EXPLOSIVE_HELP(myVehicle, MyLocalPedStruct)
			ENDIF
			
//			DRAW_DEBUG_BOX(vExp01, vExp02, 0, 0, 255, 126)
//			DRAW_DEBUG_SPHERE(vExp01, 2, 0, 0, 255, 126)
			
			// Test if an explosion is in the area of the back of the trailer
			// Add in a timer, so the player doesn't complete two goals simultaneously.
			IF (IS_EXPLOSION_IN_SPHERE(EXP_TAG_STICKYBOMB, vExp01, fRadiusCheck) AND TIMERA() > 2000)
			OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_TRUCK, vExp01, fRadiusCheck) AND TIMERA() > 2000)
			OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_GAS_CANISTER, vExp01, fRadiusCheck) AND TIMERA() > 2000)
			OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, vExp01, fRadiusCheck) AND TIMERA() > 2000)
			OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADELAUNCHER, vExp01, fRadiusCheck) AND TIMERA() > 2000)
			OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_ROCKET, vExp01, fRadiusCheck) AND TIMERA() > 2000)
				
				ADD_EXPLOSION(vExp01, EXP_TAG_PLANE, 0.5)
				ADD_EXPLOSION(vExp02, EXP_TAG_PLANE, 0.5)
				
				DEBUG_PRINT("EXPLOSION DETECTED IN BACK OF TRAILER")
				
				STOP_SCRIPTED_CONVERSATION(FALSE)
				CLEAR_HELP()
				
				// Remove back of the trailer blip
				IF DOES_BLIP_EXIST(hijackArgs.blipTrailerBack)
					REMOVE_BLIP(hijackArgs.blipTrailerBack)
				ENDIF
			
				vForwardVec = <<1.2,1.2,0.35>>
				PRINTLN("vForwardVec = ", vForwardVec)
				
				SET_VEHICLE_DOOR_LATCHED(hijackArgs.vehTrailer, SC_DOOR_REAR_LEFT, FALSE, FALSE)
				SET_VEHICLE_DOOR_LATCHED(hijackArgs.vehTrailer, SC_DOOR_REAR_RIGHT, FALSE, FALSE)
							
				SET_VEHICLE_DOOR_CONTROL(hijackArgs.vehTrailer, SC_DOOR_REAR_LEFT, DT_DOOR_SWINGING_FREE, 1.0)
				SET_VEHICLE_DOOR_CONTROL(hijackArgs.vehTrailer, SC_DOOR_REAR_RIGHT, DT_DOOR_SWINGING_FREE, 1.0)
				APPLY_FORCE_TO_ENTITY(hijackArgs.vehTrailer, APPLY_TYPE_IMPULSE, vForwardVec, <<0,-10,0>>, 0, TRUE, TRUE, TRUE)
				SET_VEHICLE_DOOR_OPEN(hijackArgs.vehTrailer, SC_DOOR_REAR_LEFT)
				SET_VEHICLE_DOOR_OPEN(hijackArgs.vehTrailer, SC_DOOR_REAR_RIGHT)
				
				// Add blip for weapons
				hijackArgs.blipWeapons = ADD_BLIP_FOR_ENTITY(hijackArgs.vehTrailer)
				SET_BLIP_COLOUR(hijackArgs.blipWeapons, BLIP_COLOUR_GREEN)
				SET_BLIP_NAME_FROM_TEXT_FILE(hijackArgs.blipWeapons, "DTRFKGR_BLIP09")
				
				CLEAR_PRINTS()
				PRINT_NOW("DTRFKGR_16a", DEFAULT_GOD_TEXT_TIME, 1)
				
				SETTIMERA(0)
				
				DEBUG_PRINT("GOING TO STATE - HIJACK_STATE_05")
				hijackStages = HIJACK_STATE_05
			ENDIF
		BREAK
		//=======================================================================================================================================
		CASE HIJACK_STATE_05
		
			IF NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
				vTrailerPos = GET_ENTITY_COORDS(hijackArgs.vehTrailer)
				fTrailerHeading = GET_ENTITY_HEADING(hijackArgs.vehTrailer)
//				PRINTLN("fTrailerHeading = ", fTrailerHeading)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(myVehicle)
				fPlayerVehicleHeading = GET_ENTITY_HEADING(myVehicle)
//				PRINTLN("fVehicleHeading = ", fPlayerVehicleHeading)
			ENDIF
			
			fOrientationValue = COS(ABSF(fTrailerHeading - fPlayerVehicleHeading))
//			PRINTLN("ORIENTATION VALUE = ", fOrientationValue)
			
			IF fOrientationValue > -0.86
				bGoodOri = FALSE
//				PRINTLN("bGoodOri = FALSE")
			ELSE
				bGoodOri = TRUE
//				PRINTLN("bGoodOri = TRUE")
			ENDIF
			
			IF fOrientationValue >= -1.00 AND bGoodOri
				hijackArgs.bOrientationIsCorrect = TRUE
				CLEAR_THIS_PRINT("DTRFKGR_16b")
//				PRINTLN("bOrientationIsCorrect = TRUE")
			ELSE
				hijackArgs.bOrientationIsCorrect = FALSE
//				PRINTLN("bOrientationIsCorrect IS NOT CORRECT")
			ENDIF

			vTriggerCutscenePos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, <<0,-12,0>>)
			
			// Normal case - if the player is in there vehicle, close enough, and the orientation is correct -> Play cutscene
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle) AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vTriggerCutscenePos, <<5,5,10>>)
			AND hijackArgs.bOrientationIsCorrect
				hijackArgs.bTruckInPosition = TRUE
			ELSE
				// Exceptions:
				
				// Car facing wrong direction - Player is in their vehicle, close enough, but facing the wrong direction.
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle) AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vTriggerCutscenePos, <<5,5,10>>)
				AND NOT hijackArgs.bOrientationIsCorrect
					PRINT_NOW("DTRFKGR_16b", DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
//				// Player is out of their vehicle, but everything else is good.
//				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle) AND hijackArgs.bOrientationIsCorrect AND IS_ENTITY_AT_COORD(myVehicle, vTriggerCutscenePos, <<5,5,10>>)
//					CLEAR_PRINTS()
//					PRINT_NOW("DTRFKGR_16", DEFAULT_GOD_TEXT_TIME, 1)
//				ENDIF
			ENDIF
			
			// If the player is close to the weapons, run cutscene.
			IF (hijackArgs.bTruckInPosition AND TIMERA() > 2000) //OR (hijackArgs.bOrientationIsCorrect AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vTriggerCutscenePos, <<2,2,10>>)) AND TIMERA() > 2000
				// Grab trailer info
				IF NOT IS_ENTITY_DEAD(hijackArgs.vehTrailer)
					vTrailerPos = GET_ENTITY_COORDS(hijackArgs.vehTrailer)
					PRINTLN("vTrailerPos = ", vTrailerPos)
					fTrailerHeading = GET_ENTITY_HEADING(hijackArgs.vehTrailer)
				ENDIF
				
				GET_CLOSEST_VEHICLE_NODE(vTrailerPos, vClosestNodePos)
				PRINTLN("vClosestNodePos = ", vClosestNodePos)
				
				IF NOT ARE_VECTORS_EQUAL(vClosestNodePos, <<0,0,0>>)
					FLOAT vDistToNode
					vDistToNode = VDIST(vTrailerPos, vClosestNodePos)
					vDistToNode = vDistToNode
					PRINTLN("DISTANCE TO NODE = ", vDistToNode)
					
					FLOAT fDeltaX
					fDeltaX =  ABSF(vClosestNodePos.x - vTrailerPos.x)
					PRINTLN("fDeltaX = ", fDeltaX)
					
					IF fDeltaX < DISTANCE_ALLOWED_FROM_ROAD_NODE
						hijackArgs.bOkayToUseOffset = TRUE
						PRINTLN("hijackArgs.bOkayToUseOffset = TRUE")
					ENDIF
				ENDIF
	
				IF DT_IS_CAR_IN_FRONT_OF_CAR(myVehicle, hijackArgs.vehTrailer)
					// Base first cam off trailer position
					hijackArgs.vCamPos01 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, vTrailerPosOffset01)
					PRINTLN("PLAYER CAR IN FRONT: hijackArgs.vCamPos01 = ", hijackArgs.vCamPos01)
				
					// Move player vehicle
					IF hijackArgs.bOkayToUseOffset
						hijackArgs.vVehicleNode = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, vTrailerVehicleNodeOffset)
						PRINTLN("*************USING OFFSET 01*************")
					ELSE
						GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, vTrailerVehicleNodeOffset), 1, hijackArgs.vVehicleNode, hijackArgs.fVehicleNodeHeading, iNumLanes)
						PRINTLN("*************USING NODE 01*************")
					ENDIF
					
					PRINTLN("PLAYER CAR IN FRONT: hijackArgs.vVehicleNode = ", hijackArgs.vVehicleNode)
				ELSE
					// Base first cam off trailer position
					hijackArgs.vCamPos01 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, vTrailerPosOffset)
					PRINTLN("PLAYER CAR BEHIND: hijackArgs.vCamPos01 = ", hijackArgs.vCamPos01)
					
					// Move player vehicle
					IF hijackArgs.bOkayToUseOffset
						hijackArgs.vVehicleNode = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, vTrailerVehicleNodeOffset)
						PRINTLN("*************USING OFFSET 02*************")
					ELSE
						GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTrailerPos, fTrailerHeading, vTrailerVehicleNodeOffset), 1, hijackArgs.vVehicleNode, hijackArgs.fVehicleNodeHeading, iNumLanes)
						PRINTLN("*************USING NODE 02*************")
					ENDIF
					
					PRINTLN("PLAYER CAR BEHIND: hijackArgs.vVehicleNode = ", hijackArgs.vVehicleNode)
				ENDIF
			
				IF DOES_BLIP_EXIST(hijackArgs.blipWeapons)
					REMOVE_BLIP(hijackArgs.blipWeapons)
					DEBUG_PRINT("REMOVING WEAPONS BLIP")
				ENDIF
				
				// Set bool to make escorts leave
				hijackArgs.bDoorsOpen = TRUE
				
				DEBUG_PRINT("INSIDE STATE - HIJACK_STATE_05")
				hijackStages = HIJACK_STATE_06
			ENDIF
		BREAK
		//=======================================================================================================================================
		CASE HIJACK_STATE_06
			// Trying to minimize rotation during cutscene
//			#IF IS_DEBUG_BUILD
//				vNewRotation = <<vTrailerRot.x, vTrailerRot.y, vTrailerRot.z + 90>>
//			#ENDIF
		
			IF DO_CUTSCENE_CUSTOM_GROUND(fpHijackCutscenePush)
				hijackArgs.bHijackActive = FALSE
				DEBUG_PRINT("GOING TO STATE - HIJACK_STATE_07")
				hijackStages = HIJACK_STATE_07
			ENDIF
		BREAK
		//=======================================================================================================================================
		CASE HIJACK_STATE_07
			// The player is back in their car, with the weapons... return to the safehouse.
			IF IS_VEHICLE_DRIVEABLE(myVehicle) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
				
					SET_MAX_WANTED_LEVEL(6)
					DEBUG_PRINT("SETTING MAX WANTED LEVEL TO SIX IN HIJACK MODE")
				
					bDrugsLoaded = TRUE
					
					IF NOT bDoDelicate
						// Set returning vector
						PRINT_NOW("DTRFKGR_03", DEFAULT_GOD_TEXT_TIME, 1)
						sLastObjective = "DTRFKGR_03"
						myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.endData[0].vDrugPickup)
						IF DOES_BLIP_EXIST(myLocationBlip[0])
//							SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
							SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
						ENDIF
						vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
					ENDIF
					
//					REPEAT MAX_NUMBER_EXPLOSIVES idx
//						IF DOES_ENTITY_EXIST(tempCanister[idx])
//							SET_OBJECT_AS_NO_LONGER_NEEDED(tempCanister[idx])
//							PRINTLN("SETTING CANISTER AS NO LONGER NEEDED: ", idx)
//						ENDIF
//					ENDREPEAT
					
					IF DOES_ENTITY_EXIST(hijackArgs.vehTruck)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(hijackArgs.vehTruck)
						PRINTLN("SETTING VEHICLE - hijackArgs.vehTruck AS NO LONGER NEEDED")
					ENDIF
					IF DOES_ENTITY_EXIST(hijackArgs.vehTrailer)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(hijackArgs.vehTrailer)
						PRINTLN("SETTING VEHICLE - hijackArgs.vehTrailer AS NO LONGER NEEDED")
					ENDIF
					REPEAT MAX_NUMBER_ESCORT_VEHICLES idx
						IF DOES_ENTITY_EXIST(hijackArgs.vehEscort[idx]) 
							SET_VEHICLE_AS_NO_LONGER_NEEDED(hijackArgs.vehEscort[idx])
							PRINTLN("SETTING VEHICLE - hijackArgs.vehEscort AS NO LONGER NEEDED", idx)
						ENDIF
					ENDREPEAT
					
					SET_MODEL_AS_NO_LONGER_NEEDED(BATI)
					SET_MODEL_AS_NO_LONGER_NEEDED(HAULER)
					SET_MODEL_AS_NO_LONGER_NEEDED(TRAILERS2)
					
					cutArgs.cutsceneState = CUTSCENE_STATE_START
					
					SET_WANTED_LEVEL_MULTIPLIER(1)
					DEBUG_PRINT("SETTING WANTED MULTIPLIER BACK TO ONE IN HIJACK MODE")
					
					SET_VEHICLE_STRONG(myVehicle, FALSE)
					PRINTLN("MAKING VEHICLE NO LONGER STRONG")
					
					DEBUG_PRINT("RETURING TRUE - UPDATE_HIJACK")
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
		
	RETURN FALSE
ENDFUNC

