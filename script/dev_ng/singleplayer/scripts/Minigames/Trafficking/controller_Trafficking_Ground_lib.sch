// controller Trafficking_Ground_lib.sch
USING "drug_trafficking_data.sch"
USING "controller_trafficking.sch"
USING "flow_help_public.sch"

TRAFFICKING_STATE 	eGndState
VEHICLE_INDEX		vehCar

#IF IS_DEBUG_BUILD
	USING "debug_channels_structs.sch"
#ENDIF

VECTOR 				vGroundSpawnPosition = <<2149.7371, 4798.4717, 40.1071>>
FLOAT 				fGroundSpawnHeading = 55.4847

BOOL 				bPrintedInVeh = FALSE

FUNC MODEL_NAMES GET_VEHICLE_MODEL_FOR_RANK()
	RETURN DUNE	
ENDFUNC

FUNC MODEL_NAMES GET_ALTERNATE_VEHICLE_MODEL_FOR_RANK()	
	RETURN DUNE	
ENDFUNC

PROC TRAFFICKING_GROUND_CLEANUP(BOOL bDestroy = FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(DUNE)
	
	IF DOES_ENTITY_EXIST(vehCar)
		IF bDestroy
			DELETE_VEHICLE(vehCar)
			PRINTLN("GROUND CLEANUP: DELETING CAR")
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCar)
			PRINTLN("GROUND CLEANUP: SETTING CAR AS NO LONGER NEEDED")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CHECK_FOR_TRAFFIC_VEHICLE_IN_AREA()
	VECTOR vTraffickVehiclePos
//	FLOAT fDistanceTemp
	
	IF eGndState = TKS_LAUNCH_WAIT
		IF DOES_ENTITY_EXIST(vehCar) AND IS_VEHICLE_DRIVEABLE(vehCar)
			vTraffickVehiclePos = GET_ENTITY_COORDS(vehCar)
//			CPRINTLN(DEBUG_MINIGAME, "vTraffickVehiclePos = ", vTraffickVehiclePos)
			
//			fDistanceTemp = VDIST2(vTraffickVehiclePos, vGroundSpawnPosition)
//			fDistanceTemp = SQRT(fDistanceTemp)
//			CPRINTLN(DEBUG_MINIGAME, "fDistanceTemp = ", fDistanceTemp)
			
			IF VDIST2(vTraffickVehiclePos, vGroundSpawnPosition) < 4 // 2m
//				CPRINTLN(DEBUG_MINIGAME, "GROUND: CAR IS CLOSE ENOUGH TO CREATE BLIP")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
//		CPRINTLN(DEBUG_MINIGAME, "WE'RE NOT IN LAUNCH WAIT, SO RETURN TRUE ON CHECK_FOR_TRAFFIC_VEHICLE_IN_AREA")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC TRAFFICKING_GROUND_UPDATE(BOOL & bIsGroundReplay)

	// General blip update.
	BOOL bEventActive = FALSE
	IF (eGndState != TKS_LEAVE_WAIT)
		IF NOT (g_savedGlobals.sTraffickingData.bGndBlipActive)
			IF (g_savedGlobals.sTraffickingData.iGroundRank = 0) AND (g_savedGlobals.sTraffickingData.iAirRank = 0)
				IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_GaveHelpPrint)
					//Displaying help for both ground and air trafficking here. It is assumed they
					//become available at the same time. TODO #900555. -BenR
					
					ADD_HELP_TO_FLOW_QUEUE("MG_TRAF_AVAIL", FHP_LOW) // Adding back in to fix Bug # 1267071
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_GaveHelpPrint)
				ENDIF
			ENDIF
		
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRAF_GND, TRUE)
			g_savedGlobals.sTraffickingData.bGndBlipActive = TRUE
			bEventActive = TRUE
		ELSE
			bEventActive = TRUE
		ENDIF
	ELSE
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRAF_GND, FALSE)
		g_savedGlobals.sTraffickingData.bGndBlipActive = FALSE
	ENDIF
	
	
	// See if we're being force reset.
	IF (eGndState != TKS_AREA_WAIT) AND (eGndState != TKS_WAIT_FOR_FAILSCREEN_REPLAY)
		IF TRAFFICKING_SHOULD_CLEAN_AND_WAIT() AND NOT bIsGroundReplay
			CPRINTLN(DEBUG_MINIGAME, "Sending ground controller to area wait BECAUSE WE SHOULD.")
			TRAFFICKING_GROUND_CLEANUP()
			eGndState = TKS_AREA_WAIT
		ENDIF
	ENDIF
	
	
	// General state update.
	SWITCH (eGndState)
		CASE TKS_INIT
//			CPRINTLN(DEBUG_MINIGAME, "GROUND: GOING TO STATE - TKS_AREA_WAIT FROM - TKS_INIT")
			eGndState = TKS_AREA_WAIT
		BREAK

		CASE TKS_AREA_WAIT
//			CPRINTLN(DEBUG_MINIGAME, "Ground -- TKS_AREA_WAIT")
			
			// Make sure the player is within range of the actual vehicle. If he is, move to stream it.
			IF NOT TRAFFICKING_SHOULD_CLEAN_AND_WAIT()
				IF (fDist2ToLauncher < TRAF_UPDATE_DIST2)
					CPRINTLN(DEBUG_MINIGAME, "GROUND: GOING TO STATE - TKS_STREAM")
					eGndState = TKS_STREAM
				ELSE
					IF bSwitchInProgress
						PRINTLN("WE'RE USING AIR SWITCH, GO AHEAD AND GO TO STATE - TKS_STREAM")
						eGndState = TKS_STREAM
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TKS_STREAM
			CPRINTLN(DEBUG_MINIGAME, "Ground -- TKS_STREAM")
			
			// Request all streams here. Once done, move on to waiting for them.
			REQUEST_MODEL(DUNE)
			
//			CPRINTLN(DEBUG_MINIGAME, "GROUND: GOING TO STATE - TKS_STREAM_WAIT")
			eGndState = TKS_STREAM_WAIT
		BREAK
		
		CASE TKS_STREAM_WAIT
//			CPRINTLN(DEBUG_MINIGAME, "Ground -- TKS_STREAM_WAIT")
			
			// When everything is done loading, move to waiting for the player to engage the launcher.
			IF HAS_MODEL_LOADED(DUNE) 
				// Create the launch vehicle.
				CLEAR_AREA_OF_VEHICLES(vGroundSpawnPosition, 4.0)
				IF DOES_ENTITY_EXIST(vehCar)
					DELETE_VEHICLE(vehCar)
					PRINTLN("DELETING ANY EXTRA - vehCar")
				ENDIF
				vehCar = CREATE_VEHICLE(GET_VEHICLE_MODEL_FOR_RANK(), vGroundSpawnPosition, fGroundSpawnHeading)
				SET_VEHICLE_RADIO_ENABLED(vehCar, FALSE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_VEHICLE_MODEL_FOR_RANK())
				
				IF DOES_ENTITY_EXIST(vehCar)
					IF DOES_EXTRA_EXIST(vehCar, 1)
						SET_VEHICLE_EXTRA(vehCar, 1, TRUE)
						CPRINTLN(DEBUG_MINIGAME, "SETTING vehCar VEHICLE EXTRA 1 TO TRUE")
					ENDIF
					IF DOES_EXTRA_EXIST(vehCar, 2)
						SET_VEHICLE_EXTRA(vehCar, 2, FALSE)
						CPRINTLN(DEBUG_MINIGAME, "SETTING vehCar VEHICLE EXTRA 2 TO FALSE")
					ENDIF
					IF DOES_EXTRA_EXIST(vehCar, 3)
						SET_VEHICLE_EXTRA(vehCar, 3, FALSE)
						CPRINTLN(DEBUG_MINIGAME, "SETTING vehCar VEHICLE EXTRA 3 TO FALSE")
					ENDIF
					SET_VEHICLE_COLOURS(vehCar, 0, 0)
				ENDIF
				
				CPRINTLN(DEBUG_MINIGAME, "GROUND: GOING TO STATE - TKS_LAUNCH_WAIT")
				eGndState = TKS_LAUNCH_WAIT
			ENDIF
		BREAK
		
		CASE TKS_CUTSCENE
			// Make sure the player's still in the area, and we care.
			IF (fDist2ToLauncher >= TRAF_UPDATE_DIST2) AND NOT bIsGroundReplay
				CPRINTLN(DEBUG_MINIGAME, "GROUND: GOING TO STATE - TKS_AREA_WAIT - FROM TKS_CUTSCENE")
				TRAFFICKING_GROUND_CLEANUP()
				eGndState = TKS_AREA_WAIT
				EXIT
			ENDIF
		BREAK
		
		CASE TKS_LAUNCH_WAIT
//			CPRINTLN(DEBUG_MINIGAME, "GROUND: INSIDE STATE - TKS_LAUNCH_WAIT")
			
			// Okay, in debug, there are some cases where we need to plant the player in the vehicle.
			#IF IS_DEBUG_BUILD
				IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_LaunchedGndViaDebug)
					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_LaunchedGndViaDebug)
					bEventActive = TRUE
					
					IF NOT IS_ENTITY_DEAD(pedPlayer) AND NOT IS_ENTITY_DEAD(vehCar)
						SET_PED_INTO_VEHICLE(pedPlayer, vehCar)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						
						PRINTLN("GROUND: SETTING PLAYER INTO VEHICLE - THROUGH DEBUG LAUNCH")
					ENDIF
				ENDIF
			#ENDIF
						
//			// Make sure the vehicle is still alive...
//			IF DOES_ENTITY_EXIST(vehPlane)
//				IF IS_ENTITY_DEAD(vehPlane) OR IS_ENTITY_DEAD(PLAYER_PED_ID())
//					CPRINTLN(DEBUG_MINIGAME, "Leaving due to invalidity, but then we'd leave?")
//					eGndState = TKS_LEAVE_WAIT
//					EXIT
//				ENDIF
//			ENDIF
			
			HANDLE_VEHICLE_STORING()
			
			// Make sure the player's still in the area, and we care.
			IF NOT bSwitchInProgress
				IF (fDist2ToLauncher >= TRAF_UPDATE_DIST2) AND NOT bIsGroundReplay
					CPRINTLN(DEBUG_MINIGAME, "GROUND: GOING TO STATE - TKS_AREA_WAIT - FROM TKS_LAUNCH_WAIT")
					TRAFFICKING_GROUND_CLEANUP()
					eGndState = TKS_AREA_WAIT
					EXIT
				ENDIF
			ENDIF
			
			// Are we even supposed to be active?
			IF NOT bEventActive AND NOT bIsGroundReplay
			
				IF NOT bEventActive
//					CPRINTLN(DEBUG_MINIGAME, "GROUND: WE'RE NOT ACTIVE")
				ENDIF
				IF NOT bIsGroundReplay
//					CPRINTLN(DEBUG_MINIGAME, "GROUND: WE'RE NOT IN A REPLAY")
				ENDIF
			
				IF NOT bPrintedInVeh
					// If the player gets into the vehicle while we're not active... notify him.
					IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
						VEHICLE_INDEX tempVeh
						tempVeh = GET_VEHICLE_PED_IS_USING(pedPlayer)
						IF (tempVeh = vehCar)
							//PRINT_HELP("MG_GTRAF_H_TOD")
//							ADD_HELP_TO_FLOW_QUEUE("MG_GTRAF_H_TOD", FHP_LOW)

							bPrintedInVeh = TRUE
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCar)
							
							CPRINTLN(DEBUG_MINIGAME, "GROUND: GOING TO STATE - TKS_LEAVE_WAIT - FROM TKS_LAUNCH_WAIT")
							eGndState = TKS_LEAVE_WAIT
						ENDIF
					ENDIF
				ENDIF
				
				// Event not active. Leave us.
//				CPRINTLN(DEBUG_MINIGAME, "GROUND: EXITING bEventActive AND bIsGroundReplay IS FALSE")
				EXIT
			ENDIF
			
			// If this is a replay... put the ped in the vehicle.
			IF bIsGroundReplay
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(vehCar)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
					PRINTLN("GROUND: SETTING PLAYER INTO VEHICLE - THROUGH REPLAY")
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(vehCar)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar, TRUE)
					DISABLE_SELECTOR_THIS_FRAME()
					PRINTLN("GROUND: DISABLING SELECTOR")
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(vehCar) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				// Wait for the player to enter the vehicle, and launch then.
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX tempVeh
					tempVeh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
					IF (tempVeh = vehCar)
						IF VDIST2(vGroundSpawnPosition, GET_ENTITY_COORDS(pedPlayer)) < 100.00
							REQUEST_SCRIPT("Traffick_Ground")
							
							IF DOES_ENTITY_EXIST(vehToSave) AND NOT IS_ENTITY_DEAD(vehToSave)
								IF GET_ENTITY_MODEL(vehToSave) <> DUNE
								AND GET_ENTITY_MODEL(vehToSave) <> CUBAN800
									SET_MISSION_VEHICLE_GEN_VEHICLE(vehToSave, <<2142.2451, 4823.4629, 40.2769>>, 118.4428)
									PRINTLN("GROUND: TRYING TO SAVE LAST VEHICLE")
								ELSE
									IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
										SET_PLAYERS_LAST_VEHICLE(vehToSave)
										PRINTLN("GROUND: LAST VEHICLE WAS THE DUNE OR CUBAN800 - NOT SAVING")
									ENDIF
								ENDIF
							ENDIF
							
							eGndState = TKS_LAUNCH
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TKS_LAUNCH
//			CPRINTLN(DEBUG_MINIGAME, "Ground -- TKS_LAUNCH")
			// Once the script is loaded, we're free to launch.
			
			IF HAS_SCRIPT_LOADED("Traffick_Ground")
				CPRINTLN(DEBUG_MINIGAME, "Script loaded... moving on.")
				
				m_enumMissionCandidateReturnValue eLaunchVal
				eLaunchVal = MCRET_DENIED
				
				// If we're a replay, just continue on. Short circuit all.
				IF bIsGroundReplay
					CPRINTLN(DEBUG_MINIGAME, "Replay launch!!")
					eLaunchVal = MCRET_ACCEPTED
				ELSE
					eLaunchVal = Request_Mission_Launch(iMissionCandidateID, MCTID_CONTACT_POINT, MISSION_TYPE_MINIGAME)
					bRequestedLaunchedTrafficking = TRUE
				ENDIF
				
				// Check acceptance.
				IF (eLaunchVal = MCRET_ACCEPTED)
				
					CPRINTLN(DEBUG_MINIGAME, "Launching Traffick_Ground.sc !")
					
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						CPRINTLN(DEBUG_MINIGAME, "TRAFFICKING GROUND CONTROLLER - CLEARING WANTED LEVEL")
					ENDIF
					
					IF NOT bIsGroundReplay
						Store_Minigame_Replay_Starting_Snapshot("Traffick_Ground", FALSE)
					ENDIF
					
					ARGS launchArgs

					// Nothing custom has been done, just run default.
				    traffickingThread = START_NEW_SCRIPT_WITH_ARGS("Traffick_Ground", launchArgs, SIZE_OF(launchArgs), MISSION_STACK_SIZE)
				    SET_RICH_PRESENCE_FOR_SP_MINIGAME(MINIGAME_TRAFFICKING_GROUND)
					SET_SCRIPT_AS_NO_LONGER_NEEDED("Traffick_Ground")
					SET_ENTITY_AS_MISSION_ENTITY(vehCar)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCar)
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					    // Sober the player up
					    IF g_isPlayerDrunk = TRUE
					        Make_Ped_Sober(PLAYER_PED_ID())
					    ENDIF
					ENDIF
					
					bPrintedInVeh = FALSE
					
					eGndState = TKS_RUN_WAIT
					
				ELIF (eLaunchVal = MCRET_DENIED)
				
					bRequestedLaunchedTrafficking = FALSE
					TRAFFICKING_GROUND_CLEANUP()
					CPRINTLN(DEBUG_MINIGAME, "GROUND: GOING TO STATE - TKS_LEAVE_WAIT")
					eGndState = TKS_LEAVE_WAIT
				ENDIF
			ENDIF
		BREAK
		
		CASE TKS_RUN_WAIT
//			CPRINTLN(DEBUG_MINIGAME, "Ground -- TKS_RUN_WAIT")
			
			IF NOT IS_THREAD_ACTIVE(traffickingThread)
				
////				// By deafult, we'd go back to streaming.
//				CPRINTLN(DEBUG_MINIGAME, "GROUND: GOING TO STATE - TKS_AREA_WAIT, FROM TKS_RUN_WAIT")
//				eGndState = TKS_AREA_WAIT
				
				// By deafult, we'd go back to streaming.
				CPRINTLN(DEBUG_MINIGAME, "GROUND: GOING TO STATE - TKS_LEAVE_WAIT, FROM TKS_RUN_WAIT")
				eGndState = TKS_LEAVE_WAIT
				
				// If we failed, we'll be doing a replay...
				IF IS_BITMASK_AS_ENUM_SET(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_GroundTraffickingFailed)
					eGndState = TKS_WAIT_FOR_FAILSCREEN_REPLAY
				ELSE
					// passed, cleanup
					CPRINTLN(DEBUG_MINIGAME, "Clearing the mission candidate!!")
					Mission_Over(iMissionCandidateID)
					Reset_All_Replay_Variables()
					
					// Make the weapons respawn themselves.
					bTriggerWeaponsReset = TRUE
					
					// Store next playable time of day.
					TIMEOFDAY todNext
					todNext = GET_CURRENT_TIMEOFDAY()
					ADD_TIME_TO_TIMEOFDAY(todNext, 0, 0, 3)
					g_savedGlobals.sTraffickingData.todNextPlayable_Gnd = todNext

					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRAF_GND, FALSE)
					g_savedGlobals.sTraffickingData.bGndBlipActive = FALSE
				ENDIF
				
				bIsGroundReplay = FALSE
				CLEAR_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_GroundTraffickingFailed)
			ENDIF
		BREAK
		
		CASE TKS_WAIT_FOR_FAILSCREEN_REPLAY
			CPRINTLN(DEBUG_MINIGAME, "Ground -- TKS_WAIT_FOR_FAILSCREEN_REPLAY")
			
			IF (g_replay.replayStageID = RS_ACTIVE)
				IF g_bLaunchMinigameReplay
					CPRINTLN(DEBUG_MINIGAME, "Ground -- Flag set, replay starting. Replay script name: ", g_replay.replayScriptName)
					bIsGroundReplay = TRUE
					
					//REQUEST_MODEL(GET_VEHICLE_MODEL_FOR_RANK())
					
					REQUEST_SCRIPT("Traffick_Ground")
					eGndState = TKS_STREAM
				ELSE
					CPRINTLN(DEBUG_MINIGAME, "Ground -- No minigame replay...")

					bIsGroundReplay = FALSE
					eGndState = TKS_STREAM
				ENDIF
				
			ELIF (g_replay.replayStageID = RS_NOT_REQUIRED)
				// replay rejected, cleanup
				CPRINTLN(DEBUG_MINIGAME, "Ground -- Replay refused...")
				CPRINTLN(DEBUG_MINIGAME, "Clearing the mission candidate!!")
				Mission_Over(iMissionCandidateID)
				Reset_All_Replay_Variables()
				bIsGroundReplay = FALSE
				eGndState = TKS_STREAM
			ENDIF
		BREAK
		
		CASE TKS_LEAVE_WAIT
//			CPRINTLN(DEBUG_MINIGAME, "Ground -- TKS_LEAVE_WAIT")
			
			// Here' we're simply waiting for the player to leave the area before resetting.
			IF (fDist2ToLauncher >= TRAF_UPDATE_DIST2)
			AND IS_NOW_AFTER_TIMEOFDAY(g_savedGlobals.sTraffickingData.todNextPlayable_Gnd)
				CPRINTLN(DEBUG_MINIGAME, "GROUND: GOING TO STATE - TKS_AREA_WAIT - FROM TKS_LEAVE_WAIT")
				
				TRAFFICKING_GROUND_CLEANUP()
				eGndState = TKS_AREA_WAIT
			ENDIF
		BREAK
		
		DEFAULT
			CPRINTLN(DEBUG_MINIGAME, "DEFAULT--Ground: Bug this for David S.")
		BREAK
	ENDSWITCH
ENDPROC
