//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     : Traffick Air                                        		    //
//      AUTHOR          : Steven Messinger                                              //
//      DESCRIPTION     :         														//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// including the most common script headers
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_vehicle.sch"
USING "commands_player.sch" 
USING "commands_path.sch"
USING "finance_control_public.sch"
USING "player_ped_public.sch"
USING "model_enums.sch"
USING "timer_public.sch"
USING "Traffick_air.sch"
USING "replay_public.sch"
USING "flow_public_game.sch"
USING "shared_hud_displays.sch"
USING "replay_public.sch"
USING "RC_Helper_Functions.sch"
USING "achievement_public.sch"
USING "rich_presence_public.sch"

//temp remove me
//INT frameCount = 0


//CustomCutsceneCaller fpCutsceneCustomIntro
CustomCutsceneCaller fpAirTaxiCutscene
//CustomCutsceneCaller fpEndingCutscene

//DEBUG_POS_DATA myData
//DEBUG_POS_DATA debugData
//structTimer	myTimer
//structTimer	cutSceneTimer
//CUTSCENE_STATE cutsceneState
BOOL bIsReplay

ARGS myArgsCopy
CUTSCENE_ARGS cutArgs
INT iAllowSkipCutsceneTime = 0

ENUM MISSION_STAGE_ENUM
	STAGE_START = 0,
	STAGE_MAIN,
	STAGE_RESULTS_SCREEN,
	STAGE_END
ENDENUM

ENUM INTRO_CUTSCENE_STATE
	INTRO_CUTSCENE_STATE_STAGE1 = 0,
	INTRO_CUTSCENE_STATE_STAGE2,
	INTRO_CUTSCENE_STATE_STAGE3,
	INTRO_CUTSCENE_STATE_STAGE4,
	INTRO_CUTSCENE_STATE_STAGE5, 
	INTRO_CUTSCENE_STATE_STAGE6
ENDENUM

ENUM AIR_TAXI_STATE
	AIR_TAXI_STATE_01 = 0,
	AIR_TAXI_STATE_02,
	AIR_TAXI_STATE_03,
	AIR_TAXI_STATE_CLEANUP,
	AIR_TAXI_STATE_FADEIN
ENDENUM

ENUM ENDING_CUTSCENE_AIR_STATE
	ENDING_CUTSCENE_AIR_STATE_CAMERAS = 0,
	ENDING_CUTSCENE_AIR_STATE_01,
	ENDING_CUTSCENE_AIR_STATE_02,
	ENDING_CUTSCENE_AIR_STATE_03,
	ENDING_CUTSCENE_AIR_STATE_CLEANUP,
	ENDING_CUTSCENE_AIR_STATE_UI,
	ENDING_CUTSCENE_AIR_STATE_FADEIN
ENDENUM

//INTRO_CUTSCENE_STATE introCutsceneStates = INTRO_CUTSCENE_STATE_STAGE1
AIR_VARIATION airVariation = AIR_VARIATION_BOMB //AIR_VARIATION_SIMPLE
CARGO_DROP_UI_STATES cargoUI = CARGO_DROP_UI_STATES_INIT
DROP_STATE_CHECKS cargoDropStates = DROP_STATE_CHECKS_INITIALIZE
PLANE_STATE planeState = PLANE_FADE_INTRO
AIR_TAXI_STATE airTaxiStages = AIR_TAXI_STATE_01
ENDING_CUTSCENE_AIR_STATE endingCutsceneAirStages = ENDING_CUTSCENE_AIR_STATE_CAMERAS

//PLANE_DROP_ARGS planeDropArgs
SCRIPT_SCALEFORM_UI helpScaleform
//STRING sLConversation

//CAR_CHASE_UPDATE carChaseProgress = TRIGGER

//AMBUSH ambushState = AMBUSH_SETUP
FAIL_REASON_ENUM failReason

CONST_INT NUMBER_BAD_GUY_VEHICLES 1

VECTOR vCurDestObjective[MAX_NUMBER_DROPS]

VECTOR vTaxiRecordingStartPos = << 2134.06, 4780.69, 41.6644 >> //<< 2134.1416, 4780.6128, 41.6598 >>
VECTOR vTaxiRecordingEndPos = << 2042.0498, 4772.5923, 40.0958 >> 
FLOAT fTaxingRecordingEndAngle = 114.4402
TEXT_LABEL_31 sLastObjective

FLOAT fCurDist = -1
FLOAT fHeight = 0
FLOAT fLastHeight = 0
FLOAt fBonusT = 120000
FLOAT fHeadingDelta

//Plane Data
PLANE_DATA planeData
PLANE_CARGO_ARGS cargoArgs

VECTOR vCamPos, vCamRot
CAMERA_INDEX myCamera
CAMERA_INDEX transitionCam

BOOL bHeliActive = FALSE
BOOL bInAmbush = FALSE
BOOL bIsChaseActive = FALSE
BOOL bIsCutsceneActive = FALSE
BOOL bSetTimer = FALSE
BOOL bSkipEndingCutscene = FALSE
//BOOL bDrugsLoaded = TRUE

//
//Options
//
BOOL bDoLowAlt = TRUE
BOOL bDoPlane = TRUE
BOOL bDoBomb = FALSE
BOOL bDoAmbush = FALSE
BOOL bDoCarChase = FALSE
BOOL bDoDelicate = TRUE
BOOL bDoCutScene = TRUE
BOOl bDoStationary = FALSE

//BOOL bComplete = FALSE
BOOL bGiveTimeWarning = FALSE
BOOL bLanding = FALSE
BOOL bIsHighAlt = FALSE
//BOOL bIsXtraLowAlt = FALSE
//INT planeRecordingIndex

VECTOR vAirEndingPos = <<2134.7585, 4789.8989, 39.9702>> 
//VECTOR vAirEndingWalkToPos = <<2132.7969, 4794.2075, 40.1159>> 
FLOAT fAirEndingHeading = 24.9014 
BOOL bGrabbedTimeForCutscene = FALSE

structTimer tHighAltTimer

INT iStageToUse

SCENARIO_BLOCKING_INDEX sbi01, sbi02
	
//
//Drop Off Data
//
//VEHICLE_INDEX ambushArgs.ambushVehicle

// Peds
//PED_INDEX badGuyPed[maxNumBadGuys]
//BLIP_INDEX badGuyBlips[maxNumBadGuys]

// Vehicles
VEHICLE_INDEX myVehicle//[NUMBER_BAD_GUY_VEHICLES] 

// Blips
BLIP_INDEX myVehicleBlip
BLIP_INDEX myLocationBlip[MAX_NUMBER_DROPS]
VEHICLE_INDEX myEntities[MAX_NUMBER_DROPS]

// Cameras
//CAMERA_INDEX cutsceneCam

// Bools
BOOL bWanted = FALSE
//BOOL bTasked = FALSE
//BOOL bGenPrinted = FALSE
BOOL bTimedPickup = FALSE
BOOL bPrintObjAgain = TRUE
//BOOL bDisplayTimer = FALSE

//BOOL bCarChaseComplete = FALSE
//BOOL beTimedDelivery = FALSE

//Timers
INT startTime
BOOL bTimerStarted = FALSE
BOOL bTimeExpired = FALSE

// Ints
INT outOfVehicleTime = 0

//PED_INDEX pedCarChasers[MAX_NUMBER_CAR_CHASERS]
//BLIP_INDEX blipCarChasers[MAX_NUMBER_CAR_CHASERS]
//VEHICLE_INDEX vehCarChasers[MAX_NUMBER_CAR_CHASERS]

MISSION_STAGE_ENUM curStage = STAGE_START

PROC HANDLE_ENDING_WALK_OUT()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vAirEndingPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fAirEndingHeading)
	
//		TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vAirEndingWalkToPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP)
		
		FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
		SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK)
	ENDIF
ENDPROC

PROC ENABLE_CONTROLS_ON_PLANE()
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
ENDPROC

// cleanup the mission
PROC MISSION_CLEANUP()
	CLEAR_FOCUS()
	
	ENABLE_CONTROLS_ON_PLANE()
	
	// cleanup anything you need to. You don't need to dereference script-created entities like peds and vehicles,
	// as code will do this automatically when the script terminates.
	IF bClearPrints
		IF IS_HELP_MESSAGE_ON_SCREEN()
			CLEAR_HELP()
			PRINTLN("CH - 08")
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	STOP_SCRIPTED_CONVERSATION(FALSE)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	//set rich presence back to default.
	SET_DEFAULT_RICH_PRESENCE_FOR_SP()
	
//	PLAYSTATS_ODDJOB_DONE(ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR))
//	PRINTLN("CALLING PLAYSTATS_ODDJOB_DONE - MINIGAME_TRAFFICKING_AIR")
	
	IF NOT IS_ENTITY_DEAD(myVehicle)
		SET_VEHICLE_ENGINE_CAN_DEGRADE(myVehicle, TRUE)
	ENDIF
	
//	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vCarGenPlane - <<7,7,7>>, vCarGenPlane + <<7,7,7>>, TRUE)
//	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vCarGenTruck - <<7,7,7>>, vCarGenTruck + <<7,7,7>>, TRUE)
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbi01)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi02)
	
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<2164.037842,4825.626953,35.613750>>, <<1899.277832,4697.752441,49.086414>>, 100.000000)
	
	IF NOT cargoArgs.bPassed
		PRINTLN("WE DIDN'T PASS - GO AHEAD AND CLEAR VEHICLE")
	
		IF DOES_ENTITY_EXIST(myVehicle)
			SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
			
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(myVehicle, <<2134.06, 4780.69, 41.664>>) < 50
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vAirEndingPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fAirEndingHeading)
					DELETE_VEHICLE(myVehicle)
					PRINTLN("DELETING AIR VEHICLE - TOO CLOSE TO SPAWN POSITION")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("WE PASSED - LEAVE VEHICLE")
	ENDIF
	
	IF bPlayerAttached
		DETACH_PLAYER_FROM_BOMB(cargoArgs.myPlane)
	ENDIF
	ODDJOB_STOP_SOUND(iSirenSoundID)
	ODDJOB_STOP_SOUND(iSirenSoundID2)
	ODDJOB_STOP_SOUND(iSirenSoundID3)
	ODDJOB_STOP_SOUND(cargoArgs.soundID)
	ODDJOB_STOP_SOUND(soundIdAlarm)
	TOGGLE_VISIBILITY(cargoARgs, NULL, TRUE, TRUE)
	STOP_ALL_PARTICLES(cargoArgs, planeData)
	SET_RANDOM_TRAINS(TRUE) 

	RELEASE_SCRIPT_AUDIO_BANK()
	PRINTLN("RELEASING SCRIPT AUDIO BANKS")
	
	IF DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
		SET_VEHICLE_ENGINE_ON(myVehicle, TRUE, FALSE)
		DEBUG_PRINT("CLEANUP - TURNING OFF PLANE ENGINE")
	ENDIF
	
	IF DOES_ENTITY_EXIST(myVehicle)
		SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(myVehicle)
		PRINTLN("CLEANUP - myVehicle EXISTS, SETTING AS NO LONGER NEEDED")
	ELSE
		PRINTLN("CLEANUP - myVehicle DOES NOT EXIST")
	ENDIF
	
	DELETE_PROPS(cargoArgs, myArgsCopy)
	CLEAR_TIMECYCLE_MODIFIER()
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	//OVERLAY_SET_SECURITY_CAM(FALSE,FALSE)
	//SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(cargoArgs.sf_hud)
	
	// Commenting out to fix Bug #264303
//	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_2, TRUE)
//	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_3, TRUE)
	
	//CLEANUP_MINIGAME_INSTRUCTIONS(helpScaleform)
	IF bPassed = TRUE
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
	ENDIF
	
	DISABLE_CELLPHONE(FALSE)
	
	DISPLAY_RADAR(TRUE)
	
//	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	TERMINATE_THIS_THREAD() // important, kills the script
ENDPROC

// mission passed
PROC MISSION_PASSED()
	PRINTLN("BEFORE: AIR RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
	g_savedGlobals.sTraffickingData.iAirRank++
	IF g_savedGlobals.sTraffickingData.iAirRank >ACH13_AIR_TOTAL
		g_savedGlobals.sTraffickingData.iAirRank = ACH13_AIR_TOTAL
	ENDIF
	PRINTLN("AFTER: AIR RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
	
	//Check if the trafficking total stat has changed
	int iCurrStatVal
	int iNewStatVal = g_savedGlobals.sTraffickingData.iAirRank + g_savedGlobals.sTraffickingData.iGroundRank
	STAT_GET_INT(NUM_TRAFFICKING_COMPLETED, iCurrStatVal)
//	CPRINTLN(debug_dan,"Current/new trafficking values: ",iCurrStatVal,"/",iNewStatVal)
	IF icurrStatVal < iNewstatVal
		//Update total trafficking count stat
		STAT_SET_INT(NUM_TRAFFICKING_COMPLETED, iNewStatVal)
		//Update achievement progress
		SET_ACHIEVEMENT_PROGRESS_SAFE(ENUM_TO_INT(ACH13), iNewStatVal)
//		CPRINTLN(debug_dan,"Set tp industries achievement to ",iNewStatVal)
	ENDIF
	
	IF g_savedGlobals.sTraffickingData.iAirRank = 3
		UNLOCK_MISSION_NEWS_STORY(ENUM_TO_INT(NEWS_MISC_TRAFF))		
		PRINTLN("CALLING - UNLOCK_MISSION_NEWS_STORY - NEWS_MISC_TRAFF")
		g_savedGlobals.sFinanceData.eCurrentEyeFindNewsStoryState = EYEFIND_NEWS_STORY_STATE_O_ATA3
		CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " UPDATE_EYEFIND_CURRENT_NEWS_STORY_STATE() : updated EYEFIND_NEWS_STORY_STATE_O_ATA3")
	ENDIF
	
	// To fix Bug #404728, since we increment before, need to register stat increase here.
	IF g_savedGlobals.sTraffickingData.iAirRank = 5
		IF ENUM_TO_INT(myArgsCopy.completionEntry) >= 0
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(myArgsCopy.completionEntry)
		ENDIF
		ODDJOB_AUTO_SAVE()
		
		CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]: ACH13 - TP Industries Arms Race - ", g_savedGlobals.sTraffickingData.iAirRank + g_savedGlobals.sTraffickingData.iGroundRank, " of ", ACH13_AIR_TOTAL + ACH13_GND_TOTAL)
		IF (g_savedGlobals.sTraffickingData.iAirRank >= ACH13_AIR_TOTAL) AND (g_savedGlobals.sTraffickingData.iGroundRank >= ACH13_GND_TOTAL)
			AWARD_ACHIEVEMENT(ACH13) // TP Industries Arms Race
		ENDIF
		
		PRINTLN("SPECIAL CASE COMPLETION PERCENTAGE INCREASE")
	ENDIF
	
	IF g_savedGlobals.sTraffickingData.iAirRank <= 4
		// Pegging air rank to be 5 and under, so we know when we're in repeatable mode.
		IF ENUM_TO_INT(myArgsCopy.completionEntry) >= 0
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(myArgsCopy.completionEntry)
		ENDIF
		PRINTLN("AIR TRAFFICKING RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
	ENDIF
	
	PROPERTY_PAY_INCOME(PROPERTY_ARMS_TRAFFICKING, iPayment)
	PRINTLN("FINAL PAYMENT = ", iPayment)
	
	ODDJOB_AUTO_SAVE()
	
	// anything that should happen when mission passes
	MISSION_CLEANUP()
ENDPROC

FUNC BOOL UPDATE_FAIL_CONDITIONS(ARGS& myArgs)
	
	IF bWanted
		failReason = FAIL_PLAYER_WANTED
		PRINTLN("FAILING, FAIL_PLAYER_WANTED")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID()) OR IS_PED_INJURED(PLAYER_PED_ID())
		failReason = FAIL_PLAYER_DIED
		PRINTLN("FAILING, TREVOR DIED")
		RETURN TRUE
	ENDIF
	
	IF bKilledBuyer
		failReason = FAIL_BUYER_DEAD
		PRINTLN("FAILING, FAIL_BUYER_DEAD")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(myVehicle)
		IF NOT ARE_PLANE_PROPELLERS_INTACT(myVehicle)
			DEBUG_PRINT("TRAFFICKING AIR: FAILING - PROPELLERS ARE DAMAGED")
			failReason = FAIL_VEHICLE_DISABLED
		ENDIF
	ENDIF

	// If you don't have enough bombs or packages to complete mission, fail early.
	IF bEarlyFail
		failReason = FAIL_EARLY_TERMINATION
		PRINTLN("FAILING, EARLY TERMINATION")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF bOutOfVehicle AND NOT bInActiveFight AND NOT bPlayerAttached
		
			PRINTLN("FAILING, BECAUSE OUT OF CAR!")
			failReason = FAIL_ABANDONED_CAR
			RETURN TRUE
		
			IF HANDLE_OUT_OF_VEHICLE_WARNING_AND_FAIL("DTRSHRD_03P", myVehicle, outOfVehicleTime)
				PRINTLN("FAILING, BECAUSE OUT OF CAR!!!!!!!!!!!!!!!!!")
				failReason = FAIL_ABANDONED_CAR
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF planeState = PLANE_STATE_FAILED
		PRINTLN("FAILED - DID NOT DESTROY ENOUGH TARGETS")
		failReason = FAIL_NOT_ALL_TARGETS_DESTROYED
		RETURN TRUE
	ENDIF

	IF bTimeExpired
		PRINTLN("failReason = FAIL_TIME_EXPIRED")
		failReason = FAIL_TIME_EXPIRED
		RETURN TRUE
	ENDIF

	IF bDoLowAlt AND planeState > PLANE_STATE_PRINTS AND cargoDropStates < DROP_STATE_CHECKS_FINAL
		IF UPDATE_HEIGHT_HUD(bIsHighAlt, cargoArgs, planeData, fHeight, fLastHeight, tHighAltTimer, failReason, vPlayerPos, MyLocalPedStruct)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Run abandoned check
	IF NOT myArgs.bDoTrain
		IF NOT bPlayerAttached
			IF PLAYER_HAS_ABANDONED_JOB_PLANE(planeData, vCurDestObjective)
				PRINTLN("failReason = PLAYER_HAS_ABANDONED_JOB_PLANE")
				failReason = FAIL_ABANDONED_JOB
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	fCurDist = fCurDist
//	IF fCurDist <> -1 AND NOT bWanted
//		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vCurDestObjective) > (fCurDist + fFailDist)
//			failReason = FAIL_DISTANCE_FAIL
//			RETURN TRUE
//		ENDIF
//	ENDIF

	//this is only a fail if the drugs are loaded into the vehicle.. Otherwise give objective to aquire new vehicle.

	IF planeState < PLANE_STATE_CUTSCENE
		IF IS_ENTITY_DEAD(myVehicle) //AND bDrugsLoaded//IS_VEHICLE_DRIVEABLE(myVehicle)
			PRINTLN("failReason = FAIL_TRANSPORT_DESTROYED, ENTITY IS DEAD")
			failReason = FAIL_TRANSPORT_DESTROYED
			RETURN TRUE
		ELIF NOT IS_VEHICLE_DRIVEABLE(myVehicle)
		OR NOT IS_PLANE_LANDING_GEAR_INTACT(myVehicle)
		OR NOT ARE_PLANE_PROPELLERS_INTACT(myVehicle)
			PRINTLN("failReason = FAIL_TRANSPORT_DESTROYED")
			failReason = FAIL_TRANSPORT_DESTROYED
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(myVehicle)
		IF IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_ON_ROOF, ROOF_TIME) 
		OR IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_ON_SIDE, SIDE_TIME) 
		OR IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_HUNG_UP, 20000) 
		OR IS_VEHICLE_STUCK_TIMER_UP(myVehicle, VEH_STUCK_JAMMED, JAMMED_TIME) 
			DEBUG_PRINT("TRAFFICKING AIR: FAILING - VEHICLE HAS BECOME UNDRIVEABLE/STUCK")
			failReason = FAIL_VEHICLE_DISABLED
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// mission failed
PROC MISSION_FAILED(ARGS myArgs)
	// anything that should happen when mission fails
	
	STRING strFailReason
	
	CLEAR_PRINTS()
	STOP_SCRIPTED_CONVERSATION(TRUE)
	
	IF bBombBayDoorsOpen
		CLOSE_BOMB_BAY_DOORS(planeData.myPlane)
		ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BAY_DOOR_OPEN_MASTER", cargoArgs.soundID, FALSE, myVehicle)
		PRINTLN("CLOSING BOMB BAY DOORS - 01")
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		CLEANUP_CAM(cargoArgs, bClearPrints)
		PRINTLN("MISSION_FAILED: Setting player visible after fail")
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\DRUG_TRAFFIC_AIR")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\TARGET_PRACTICE")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\DRUG_TRAFFIC_AIR_PANIC")
	PRINTLN("RELEASING SCRIPT AUDIO BANKS BY NAME")
	
	TRIGGER_MUSIC_EVENT("OJDA_STOP")
	PRINTLN("STARTING MUSIC - OJDA_STOP")
	
	// This sets us up for a replay through the launcher.
	SET_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_AirTraffickingFailed)

	// will usually print a line of god text on fail describing the reason
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SWITCH failReason
			CASE FAIL_LOST_BAD_GUY
				strFailReason = "SAN_FAIL1"
			BREAK
			CASE FAIL_TRANSPORT_DESTROYED
				strFailReason = "DTRFAIR_FAIL01"
			BREAK
			CASE FAIL_ABANDONED_CAR
				strFailReason = "DTRFAIR_FAIL_02"
			BREAK
			CASE FAIL_TIME_EXPIRED
				IF bDoBomb
					strFailReason = "DTRFAIR_FAIL_05"
					PRINTLN("TIMER EXPIRED BUT WE'RE DOING BOMB, USE FAIL MESSAGE: DTRFAIR_FAIL_05")
				ELSE
					strFailReason = "DTRFAIR_FAIL_03"
					PRINTLN("TIMER EXPIRED, USE FAIL MESSAGE: DTRFAIR_FAIL_03")
				ENDIF
			BREAK
			CASE FAIL_DISTANCE_FAIL
				strFailReason = "DTRSHRD_FAIL_05"
			BREAK
			CASE FAIL_TIME_ALTITUDE
				strFailReason = "DTRFAIR_FAIL_01"
			BREAK
			CASE FAIL_ABANDONED_JOB
				strFailReason = "DTRSHRD_FAIL_06"
			BREAK
			CASE FAIL_VEHICLE_DISABLED
				strFailReason = "DTRFAIR_FAIL_04"
			BREAK
			CASE FAIL_PLAYER_WANTED
				strFailReason = "DTRFAIR_FAIL_08"
			BREAK
			CASE FAIL_PLAYER_DIED
				// this gets handled by SET_FORCE_CLEANUP_FAIL_REASON()
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_10")
			BREAK
			CASE FAIL_EARLY_TERMINATION
				IF bDoBomb
					strFailReason = "DTRSHRD_FAIL_12" // Not enough bombs left to win.
					PRINTLN("FAIL REASON - DTRSHRD_FAIL_12")
				ELSE
					strFailReason = "DTRSHRD_FAIL_13" // Not enough packages left to win.
					PRINTLN("FAIL REASON - DTRSHRD_FAIL_13")
				ENDIF
			BREAK
			CASE FAIL_BUYER_DEAD
				strFailReason = "DTRSHRD_FAIL_17" // Killed a buyer
				PRINTLN("FAIL REASON - DTRSHRD_FAIL_17")
			BREAK
			CASE FAIL_NOT_ALL_TARGETS_DESTROYED
				IF bDoBomb AND NOT myArgs.bDoUberBombs
					strFailReason = "DTRFAIR_FAIL_05"
				ELIF bDoBomb AND myArgs.bDoUberBombs
					strFailReason = "DTRFAIR_FAIL_06"
				ELSE
					strFailReason = "DTRFAIR_FAIL_07"
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		// this gets handled by SET_FORCE_CLEANUP_FAIL_REASON()
		PRINTLN("FAIL REASON - DTRSHRD_FAIL_10 - A")
	ENDIF

	IF NOT IS_STRING_NULL_OR_EMPTY(strFailReason)
		MISSION_FLOW_SET_FAIL_REASON(strFailReason)
	ENDIF
	SET_FORCE_CLEANUP_FAIL_REASON()
	
	PRINTLN("Setting us up for a replay!")
	Setup_Minigame_Replay(GET_THIS_SCRIPT_NAME())
	
	// Wait for replay controller to fade the screen out
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		WAIT(0)
	ENDWHILE
	
	// check if we need to warp the player at all
	// (only set the fail warp locations if we can't leave the player where he was)
	IF NOT IS_PLAYER_IN_PLANE(planeData)
		PRINTLN("PLAYER IS NOT IN THE PLANE")
		IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(vAirEndingPos, fAirEndingHeading)
			PRINTLN("TrafficAir: Player is in the air. SETTING PLAYER TO DEFAULT WARP LOCATION")
		ELSE
			PRINTLN("TrafficAir: Player is not in the air.")
		ENDIF
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(myVehicle, TRUE) OR IS_ENTITY_DEAD(myVehicle)
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(vAirEndingPos, fAirEndingHeading)
		PRINTLN("TrafficAir: Plane is not driveable. SETTING PLAYER TO DEFAULT WARP LOCATION")
	ENDIF

	// cleanup and terminate the thread (must be done in 1 frame)
	MISSION_CLEANUP()
ENDPROC

FUNC INT FIND_FIRST_PLANE_RECODRING()
	INT idx
	
	REPEAT MAX_NUM_RECORDS idx
		IF planeData.dropData[0].carRecData[idx].bConfigured AND ( planeData.dropData[0].carRecData[idx].recordingType = RECORDING_TYPE_PLANE)
			RETURN idx
		ENDIF
	ENDREPEAT
	
	RETURN -1

ENDFUNC

FUNC BOOL ENDING_CUTSCENE_AIR()
	//INT iInterpTime = 4000
	FLOAT fTempHeading = 0.0
	FLOAT fPlayerHeading
	
	IF NOT bGrabbedTimeForCutscene
		iAllowSkipCutsceneTime = GET_GAME_TIMER()
		bGrabbedTimeForCutscene = TRUE
	ENDIF
	
	IF endingCutsceneAirStages < ENDING_CUTSCENE_AIR_STATE_CLEANUP
		IF bSetTimer AND TIMERA() > 3000
			IF HANDLE_SKIP_CUTSCENE(iAllowSkipCutsceneTime)
				bSkipEndingCutscene = TRUE
				PRINTLN("bSkipEndingCutscene = TRUE")
				endingCutsceneAirStages = ENDING_CUTSCENE_AIR_STATE_CLEANUP
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH endingCutsceneAirStages
		CASE ENDING_CUTSCENE_AIR_STATE_CAMERAS
		
			CLEAR_PRINTS()
			CLEAR_HELP()
			STOP_SCRIPTED_CONVERSATION(FALSE)
		
			ODDJOB_ENTER_CUTSCENE()
			
			IF NOT IS_TIMER_STARTED(cutArgs.cutSceneTimer)
				START_TIMER_NOW(cutArgs.cutSceneTimer)
			ELSE
				RESTART_TIMER_NOW(cutArgs.cutSceneTimer)
			ENDIF
			
			fTempHeading = GET_HEADING_BETWEEN_VECTORS(vPlayerPos, <<2153.1975, 4788.7695, 39.9549>> )
			fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
			fHeadingDelta = fPlayerHeading - fTempHeading 
	
			IF fHeadingDelta > 180
				fHeadingDelta -= 360
			ELIF fHeadingDelta < -180
				fHeadingDelta += 360
			ENDIF
			
			bIsCutsceneActive = TRUE
			DEBUG_PRINT("bIsCutsceneActive = TRUE")
			vCamPos = GET_GAMEPLAY_CAM_COORD()
			vCamRot = GET_GAMEPLAY_CAM_ROT()
			vCamPos.z += 20.0 //25m above the player.
			
			IF fHeadingDelta < 0
				vCamRot.z -= 40
			ELSE
				vCamRot.z += 40
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
				TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), myVehicle)
			ENDIF
			
			WAIT(0)
			
			IF NOT IS_ENTITY_DEAD(myVehicle)
				SET_VEHICLE_ENGINE_ON(myVehicle, FALSE, FALSE)
			ENDIF
			
			myCamera = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vCamPos, vCamRot, 45, TRUE)
			
			IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT) = CAM_VIEW_MODE_FIRST_PERSON)
				PRINTLN("WE'RE IN BONNET CAM, NOT DOING INTERP")
				RENDER_SCRIPT_CAMS(TRUE, FALSE, 6500)
			ELSE
				PRINTLN("WE'RE NOT IN BONNET CAM, DOING INTERP")
				RENDER_SCRIPT_CAMS(TRUE, TRUE, 6500)
			ENDIF
			
			SHAKE_CAM(myCamera, "HAND_SHAKE", 0.1)
			endingCutsceneAirStages = ENDING_CUTSCENE_AIR_STATE_01
		BREAK
		CASE ENDING_CUTSCENE_AIR_STATE_01
		
			IF NOT IS_ENTITY_DEAD(myVehicle)
				SET_VEHICLE_ENGINE_ON(myVehicle, FALSE, TRUE)
			ENDIF
		
			IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 3)
			
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vAirEndingPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fAirEndingHeading)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
				DEBUG_PRINT("INSIDE STATE - ENDING_CUTSCENE_AIR_STATE_01")
				endingCutsceneAirStages = ENDING_CUTSCENE_AIR_STATE_02
			ENDIF
		BREAK
		//======================================================================================================================================
		CASE ENDING_CUTSCENE_AIR_STATE_02
	
			IF fHeadingDelta < 0
				vCamRot.z -= 20
			ELSE
				vCamRot.z += 20
			ENDIF
			transitionCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vCamPos, vCamRot, 45, TRUE)
			SET_CAM_ACTIVE_WITH_INTERP(transitionCam, myCamera, 15000, GRAPH_TYPE_SIN_ACCEL_DECEL)
			SHAKE_CAM(myCamera, "HAND_SHAKE", 0.1)
			RESTART_TIMER_NOW(cutArgs.cutSceneTimer)
			//IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 4)
				IF NOT IS_ENTITY_DEAD(myVehicle)
					SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
				ENDIF
			
				IF DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
					DELETE_VEHICLE(myVehicle)
					DEBUG_PRINT("DELETING myVehicle")
				ENDIF
				endingCutsceneAirStages = ENDING_CUTSCENE_AIR_STATE_03
			//ENDIF
		BREAK
		//======================================================================================================================================
		CASE ENDING_CUTSCENE_AIR_STATE_03
		PRINTLN("ENDING_CUTSCENE_AIR_STATE_03")
		
			IF NOT bSetTimer
				SETTIMERA(0)
				bSetTimer = TRUE
			ENDIF
		
			IF DISPLAY_ENDING_UI(cargoArgs, planeData, fBonusT, cargoUI, myArgsCopy, bSkipEndingCutscene, FALSE)
			
				IF cargoArgs.bPassed
					MISSION_PASSED()
				ENDIF
				
				IF NOT bSkipEndingCutscene
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DESTROY_CAM(transitionCam)
					ODDJOB_EXIT_CUTSCENE()
					bIsCutsceneActive = FALSE
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE ENDING_CUTSCENE_AIR_STATE_CLEANUP
			
			// Setting player coordinates following cutscene
			
			HANDLE_ENDING_WALK_OUT()
			
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			
			DESTROY_CAM(myCamera)
			DESTROY_CAM(transitionCam)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			IF NOT IS_ENTITY_DEAD(myVehicle)
				SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
			ENDIF
		
			IF DOES_ENTITY_EXIST(myVehicle) AND NOT IS_ENTITY_DEAD(myVehicle)
				DELETE_VEHICLE(myVehicle)
				DEBUG_PRINT("DELETING myVehicle")
			ENDIF
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			endingCutsceneAirStages = ENDING_CUTSCENE_AIR_STATE_FADEIN
		BREAK
		CASE ENDING_CUTSCENE_AIR_STATE_FADEIN
			DO_SCREEN_FADE_IN(1000)
			WHILE NOT IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
			endingCutsceneAirStages = ENDING_CUTSCENE_AIR_STATE_UI
		BREAK
		CASE ENDING_CUTSCENE_AIR_STATE_UI
//			IF DISPLAY_ENDING_UI(cargoArgs, planeData, fBonusT, cargoUI, myArgsCopy, TRUE)
				IF cargoArgs.bPassed
					MISSION_PASSED()
				ENDIF
				
				bIsCutsceneActive = FALSE
				RETURN TRUE
//			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AIR_TAXI_CUTSCENE()
	
//	INT iInterpTime = 2500
//	FLOAT fPositionInRecording
	FLOAT fCutsceneTime

	IF airTaxiStages < AIR_TAXI_STATE_CLEANUP
		IF HANDLE_SKIP_CUTSCENE(iAllowSkipCutsceneTime)
			airTaxiStages = AIR_TAXI_STATE_CLEANUP
		ENDIF
	ENDIF
	
	IF NOT IS_TIMER_STARTED(cutArgs.cutSceneTimer)
		START_TIMER_NOW(cutArgs.cutSceneTimer)
		PRINTLN("STARTING CUTSCENE TIMER")
	ELSE
		fCutsceneTime = GET_TIMER_IN_SECONDS(cutArgs.cutSceneTimer)
		PRINTLN("fCutsceneTime = ", fCutsceneTime)
	ENDIF
	
	SWITCH airTaxiStages
		CASE AIR_TAXI_STATE_01
			IF NOT IS_ENTITY_DEAD(myVehicle) AND IS_VEHICLE_DRIVEABLE(myVehicle)
			
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
				iAllowSkipCutsceneTime = GET_GAME_TIMER()
				SET_ENTITY_COORDS(myVehicle,  vTaxiRecordingStartPos)
				SET_ENTITY_QUATERNION(myVehicle, -0.0030, -0.0000, 0.2176, 0.9760)
				// Start playback
				START_PLAYBACK_RECORDED_VEHICLE(myVehicle, 103, "AirTaxiCut")
				SET_PLAYBACK_SPEED(myVehicle, 0.8)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(myVehicle, 4007.000000)
				
				POINT_CAM_AT_ENTITY(myCamera, myVehicle, <<0,0,0>>)
				SET_CAM_NEAR_DOF(myCamera, 0.5)
				SET_CAM_FAR_DOF(myCamera, 15.0)
				SET_CAM_DOF_STRENGTH(myCamera, 0.7)
				SHAKE_CAM(myCamera, "HAND_SHAKE", 0.2)
				
//				IF NOT IS_STRING_EMPTY(sLConversation)
//					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
//					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", sLConversation, CONV_PRIORITY_VERY_HIGH)
//				ENDIF
				
				// Fade in if we need to - RParadis (for replays)
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)				
				ENDIF
				
				airTaxiStages = AIR_TAXI_STATE_02
			ENDIF
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------------
		CASE AIR_TAXI_STATE_02
		
			SET_USE_HI_DOF()

			IF fCutsceneTime > 2.0
				SET_PLAYBACK_SPEED(myVehicle, 1.3)
			ENDIF
			
			IF fCutsceneTime > 5.5
				IF NOT IS_ENTITY_DEAD(myVehicle)
					STOP_PLAYBACK_RECORDED_VEHICLE(myVehicle)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------------
		CASE AIR_TAXI_STATE_CLEANUP
			DESTROY_CAM(transitionCam)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			STOP_SCRIPTED_CONVERSATION(FALSE)
			//KILL_ANY_CONVERSATION()
			CLEAR_PRINTS()

			IF NOT IS_ENTITY_DEAD(myVehicle)
				STOP_PLAYBACK_RECORDED_VEHICLE(myVehicle)
			ENDIF

			SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vTaxiRecordingEndPos)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fTaxingRecordingEndAngle)
			airTaxiStages = AIR_TAXI_STATE_FADEIN
			//warp car to starting location
		BREAK
		CASE AIR_TAXI_STATE_FADEIN
			DO_SCREEN_FADE_IN(1000)
			DEBUG_MESSAGE("Setting speed to 10")

			WHILE NOT IS_SCREEN_FADED_IN()
				IF NOT IS_ENTITY_DEAD(myVehicle)
					SET_VEHICLE_FORWARD_SPEED(myVehicle, 70.0)
				ENDIF
				WAIT(0)
			ENDWHILE
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AIR_CUSTOM_CUTSCENE()
	IF NOT IS_TIMER_STARTED(cutArgs.cutSceneTimer)
		START_TIMER_NOW(cutArgs.cutSceneTimer)
	ENDIF
	
	IF TIMER_DO_WHEN_READY(cutArgs.cutSceneTimer, 5) 
		IF DISPLAY_ENDING_UI(cargoArgs, planeData, fBonusT, cargoUI)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE(ARGS& myArgs) // VEHICLE_INDEX & planeIndex)
	SWITCH curStage
		CASE STAGE_START
			myVehicle = myArgs.trafVehicle
			IF NOT IS_ENTITY_DEAD(myVehicle)
				SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
				SET_VEHICLE_ENGINE_CAN_DEGRADE(myVehicle, FALSE)
				SET_VEHICLE_COLOUR_COMBINATION(myVehicle,2)
				SET_HELI_BLADES_SPEED(myVehicle, 1.0)
			ENDIF
			
			START_PLANE(myEntities, myVehicle, planeData, cargoArgs, bInActiveFight, myArgs, FALSE, airVariation)
			
			cutArgs.cutsceneState = CUTSCENE_STATE_START
			//fpCutsceneCustomIntro = &AIR_INTRO_CUSTOM_CUTSCENE
			fpAirTaxiCutscene = &AIR_TAXI_CUTSCENE
//			fpEndingCutscene = &ENDING_CUTSCENE_AIR
			DEBUG_MESSAGE("finished start")
			//SETUP_MINIGAME_INSTRUCTIONS(helpScaleform, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X), "DTRFAIR_CTRL_01", 1, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LB), "DTRFAIR_CTRL_02", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB), "DTRFAIR_CTRL_03", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y), "DTRFAIR_CTRL_06", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "DTRFAIR_CTRL_05", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "DTRFAIR_CTRL_04")
			curStage = STAGE_MAIN
		BREAK
		CASE STAGE_MAIN
			IF UPDATE_PLANE(myArgs, planeState, myVehicleBlip, planeData, sLastObjective, vCurDestObjective, myLocationBlip,
			cargoArgs, cargoDropStates, bTimeExpired, bIsHighAlt, bDoBomb, helpScaleform, fpAirTaxiCutscene, cutArgs, 
			myCamera, airVariation)
				PRINTLN("GOING TO STATE - STAGE_RESULTS_SCREEN")
				curStage = STAGE_RESULTS_SCREEN
			ENDIF
		BREAK
		CASE STAGE_RESULTS_SCREEN
			IF DISPLAY_ENDING_UI(cargoArgs, planeData, fBonusT, cargoUI, myArgs, bSkipEndingCutscene, FALSE)
				IF cargoArgs.bPassed
					MISSION_PASSED()
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC TELEPORT_PLAYER()
	WHILE NOT FADE_DOWN()
		WAIT(0)
	ENDWHILE
	CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	LOAD_SCENE(planeData.vPlayerStart)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), planeData.vPlayerStart)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), planeData.fPlayerRot)
	ENDIF
	
	WAIT(500)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	WHILE NOT FADE_UP()
		WAIT(0)
	ENDWHILE
ENDPROC

PROC PRINT_GANG_TYPES()
	IF bUsingMexicans
		PRINTLN("bUsingMexicans IS TRUE")
	ELSE
		PRINTLN("bUsingMexicans IS FALSE")
	ENDIF
	IF bUsingMarabunta
		PRINTLN("bUsingMarabunta IS TRUE")
	ELSE
		PRINTLN("bUsingMarabunta IS FALSE")
	ENDIF
	IF bUsingHillbillies
		PRINTLN("bUsingHillbillies IS TRUE")
	ELSE
		PRINTLN("bUsingHillbillies IS FALSE")
	ENDIF
ENDPROC

//
//STRUCT myAnim
//	STRING dict
//	STRING anim
//ENDSTRUCT

// initialise mission
PROC INIT_MISSION(ARGS& myArgs)
	PRINTLN("INIT_MISSION")
	
	IF g_savedGlobals.sTraffickingData.iAirRank >= 5
	
		IF g_savedGlobals.sTraffickingData.gangTypes = GANG_MEXICANS
			bUsingMexicans = TRUE
			bUsingMarabunta = FALSE
			bUsingHillbillies = FALSE
			PRINTLN("REPEATABLE: USING MEXICANS")
		ELIF g_savedGlobals.sTraffickingData.gangTypes = GANG_MARABUNTA
			bUsingMexicans = FALSE
			bUsingMarabunta = TRUE
			bUsingHillbillies = FALSE
			PRINTLN("REPEATABLE: USING MARABUNTA")
		ELIF g_savedGlobals.sTraffickingData.gangTypes = GANG_HILLBILLIES
			bUsingMexicans = FALSE
			bUsingMarabunta = FALSE
			bUsingHillbillies = TRUE
			PRINTLN("REPEATABLE: USING HILLBILLIES")
		ENDIF
	ELSE
		IF myArgs.gangType = GANG_MEXICANS
			bUsingMexicans = TRUE
			bUsingMarabunta = FALSE
			bUsingHillbillies = FALSE
			PRINTLN("REGULAR: USING MEXICANS")
		ELIF myArgs.gangType = GANG_MARABUNTA
			bUsingMexicans = FALSE
			bUsingMarabunta = TRUE
			bUsingHillbillies = FALSE
			PRINTLN("REGULAR: USING MARABUNTA")
		ELIF myArgs.gangType = GANG_HILLBILLIES
			bUsingMexicans = FALSE
			bUsingMarabunta = FALSE
			bUsingHillbillies = TRUE
			PRINTLN("REGULAR: USING HILLBILLIES")
		ENDIF
	ENDIF
	
	PRINT_GANG_TYPES()
	
	INIT_PROPER_PLANE_CARGO(myArgs)

	INT idx
	
	IF bDoPlane
		REQUEST_MODEL(planeData.sPlaneName)
		REQUEST_PTFX_ASSET()
		REQUEST_VEHICLE_RECORDING(103, "AirTaxiCut")
		//REQUEST_VEHICLE_RECORDING(101, "fAirEnding")
		DEBUG_PRINT("REQUESTING TAXI RECORDING")
	ENDIF
	
	
	

	IF myArgs.bDoTrain
		planeData.trainNames[0] = FREIGHTCONT1
		planeData.trainNames[1] = TANKERCAR
		planeData.trainNames[2] = FREIGHT
		planeData.trainNames[3] = FREIGHTCAR
		REQUEST_MODEL(planeData.trainNames[0])
		REQUEST_MODEL(planeData.trainNames[1])
		REQUEST_MODEL(planeData.trainNames[2])
		REQUEST_MODEL(planeData.trainNames[3])
		DEBUG_PRINT("REQUESTING TRAIN MODELS")
		planeData.boatNames[0] = SQUALO
		planeData.boatNames[1] = SQUALO
		REQUEST_MODEL(planeData.boatNames[0])
		REQUEST_MODEL(planeData.boatNames[1])
		//we want some boats sucka
	ENDIF
	
	IF myArgs.bDoUberBombs
		
		REQUEST_VEHICLE_RECORDING(101, "carpetbombtruck")
		REQUEST_VEHICLE_RECORDING(101, "carpetbombplane")
		REQUEST_VEHICLE_RECORDING(101, "CarpetBombNew")
		REQUEST_VEHICLE_RECORDING(102, "CarpetBombNew")
		REQUEST_VEHICLE_RECORDING(103, "CarpetBombNew")
		REQUEST_VEHICLE_RECORDING(104, "CarpetBombNew")
		
		REQUEST_ANIM_DICT("oddjobs@arms_traffic@")
		
		REQUEST_MODEL(myArgs.mnFinalTargets[0])
		REQUEST_MODEL(myArgs.mnFinalTargets[3])
		REQUEST_MODEL(myArgs.mnFinalTargets[6])
		DEBUG_PRINT("REQUESTING UBER BOMBS")
	ENDIF

	//cargoArgs.sf_hud = REQUEST_SCALEFORM_MOVIE("remote_sniper_hud")
	//REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
	//FOR NOW ONLY LOAD THE FIRST PROP NAME.. they have the same prop.. fix this later
	
	IF bDoStationary
		REPEAT planeData.iNumDrops idx
			REQUEST_MODEL(authoredDataProps.mnPropName[idx])
			PRINTLN("REQUESTING mnPropName idx = ", idx)
			REQUEST_MODEL(authoredDataProps.mnGazeboProp[idx])
			PRINTLN("REQUESTING mnGazeboProp idx = ", idx)
		ENDREPEAT
	ENDIF
	
	IF bUsingMexicans
		REPEAT planeData.iNumDrops idx
			planeData.targetCarDriverNames[idx] = G_M_M_MEXBOSS_01
			PRINTLN("USING MEXICAN, INDEX: ", idx)
		ENDREPEAT
	ELIF bUsingMarabunta
		REPEAT planeData.iNumDrops idx
			planeData.targetCarDriverNames[idx] = G_M_Y_SalvaGoon_03
			PRINTLN("USING MARABUNTA, INDEX: ", idx)
		ENDREPEAT
	ELIF bUsingHillbillies
		REPEAT planeData.iNumDrops idx
			planeData.targetCarDriverNames[idx] = A_M_M_HillBilly_01
			PRINTLN("USING HILLBILLIES, INDEX: ", idx)
		ENDREPEAT
	ENDIF
	
	IF bDoBomb 
		REPEAT planeData.iNumDrops idx
			REQUEST_MODEL(planeData.targetCarDriverNames[idx])
			PRINTLN("REQUESTING MODELS - targetCarDriverNames, INDEX: ", idx)
			REQUEST_MODEL(planeData.stationaryCarNames[idx])
			PRINTLN("REQUESTING MODELS - stationaryCarNames, INDEX: ", idx)
		ENDREPEAT
	ENDIF
	
	IF myArgs.bDoUberBombs
	OR (airVariation = AIR_VARIATION_DROP)
	OR (airVariation = AIR_VARIATION_LOW_ALT)
		DEBUG_PRINT("REQUESTING MODELS")
		REPEAT planeData.iNumDrops idx
			REQUEST_MODEL(planeData.targetCarNames[idx])
			REQUEST_MODEL(planeData.targetCarDriverNames[idx])
			REQUEST_MODEL(planeData.stationaryCarNames[idx])
		ENDREPEAT
				
		// Request animation for guys waving at drop off locations
//		REQUEST_ANIM_DICT("amb@celebration")
//		DEBUG_PRINT("REQUESTING ANIMATIONS")
	ENDIF
	
	//REQUEST_MODEL(mnBombProp)
	REQUEST_MODEL(mnBombProp01)
	IF NOT bDoBomb
		REQUEST_ANIM_DICT("p_cargo_chute_s")
		REQUEST_MODEL(mnChuteName)
	ENDIF
	
	REQUEST_ADDITIONAL_TEXT("DTRFAIR", ODDJOB_TEXT_SLOT) // load text for the mission
	REQUEST_ADDITIONAL_TEXT("DTRSHRD", MINIGAME_TEXT_SLOT) // load text for the mission
	REQUEST_ANIM_DICT("oddjobs@towingcome_here")
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
	OR NOT HAS_ANIM_DICT_LOADED("oddjobs@towingcome_here")
		WAIT(0)
		DEBUG_PRINT("WAITING ON TEXT SLOTS")
	ENDWHILE 
	
	IF myArgs.bDoUberBombs
		IF NOT ARE_MODELS_LOADED(myArgs.mnFinalTargets, MAX_NUMBER_FINAL_TARGETS)
		OR NOT HAS_ANIM_DICT_LOADED("oddjobs@arms_traffic@") 
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "carpetbombplane")
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "carpetbombtruck")
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "CarpetBombNew")
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(102, "CarpetBombNew")
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(103, "CarpetBombNew")
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(104, "CarpetBombNew")
			PRINTSTRING("Waiting for uber cars to load")
			PRINTNL()
			WAIT(0)
		ENDIF
	ENDIF
	
	IF bDoBomb 
		WHILE NOT ARE_MODELS_LOADED(planeData.stationaryCarNames, planeData.iNumDrops)
		OR NOT ARE_MODELS_LOADED(planeData.targetCarDriverNames, planeData.iNumDrops)
		OR NOT HAS_MODEL_LOADED(mnBombProp01)
			PRINTLN("trying target cars and drivers, and animations")
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF myArgs.bDoUberBombs
	OR (airVariation = AIR_VARIATION_DROP)
	OR (airVariation = AIR_VARIATION_LOW_ALT)
		WHILE NOT ARE_MODELS_LOADED(planeData.targetCarNames, planeData.iNumDrops) 
		OR NOT ARE_MODELS_LOADED(planeData.targetCarDriverNames, planeData.iNumDrops)
		OR NOT ARE_MODELS_LOADED(planeData.stationaryCarNames, planeData.iNumDrops)
		OR NOT HAS_MODEL_LOADED(mnBombProp01)
			PRINTSTRING("trying target cars and drivers, and animations")
			PRINTNL()
			WAIT(0)
		ENDWHILE
	ENDIF

	IF NOT bDoBomb
		WHILE NOT HAS_MODEL_LOADED(mnChuteName)
		OR NOT HAS_ANIM_DICT_LOADED("p_cargo_chute_s")
			REQUEST_MODEL(mnChuteName)
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF myArgs.bDoTrain
		WHILE NOT ARE_MODELS_LOADED(planeData.trainNames, 4) 
		OR NOT ARE_MODELS_LOADED(planeData.boatNames, 2)
			PRINTSTRING("waiting for the train")
			PRINTNL()
			WAIT(0)
		ENDWHILE
	ENDIF
	
	WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\DRUG_TRAFFIC_AIR")	
	OR NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\TARGET_PRACTICE")
	OR NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\DRUG_TRAFFIC_AIR_PANIC")
		PRINTSTRING("waiting for audio")
		PRINTNL()
		WAIT(0)
	ENDWHILE
	
	IF bDoStationary
		WHILE NOT ARE_MODELS_LOADED(authoredDataProps.mnPropName, planeData.iNumDrops)
		OR NOT ARE_MODELS_LOADED(authoredDataProps.mnGazeboProp, planeData.iNumDrops)
			DEBUG_PRINT("WAITING ON AUTHORED DATA")
			WAIT(0)
		ENDWHILE
	ENDIF
	
	WHILE NOT HAS_PTFX_ASSET_LOADED()
		PRINTLN("WAITING ON PARTICLE EFFECT TO LOAD")
		WAIT(0)
	ENDWHILE

	WHILE (NOT HAS_MODEL_LOADED(planeData.sPlaneName) OR NOT bDoPlane) 
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
	OR NOT HAS_PTFX_ASSET_LOADED()
	OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(103, "AirTaxiCut")
		PRINTSTRING("trying to load string table")
		PRINTNL()
		WAIT(0)
	ENDWHILE

ENDPROC


PROC DO_STAGE_DRIVE_START()

ENDPROC

PROC DO_STAGE_GET_CAR()

ENDPROC

// example mission stages
PROC DO_STAGE_DRIVE_HERE()

	// detect if player had got to a coord, then advance currentMissionStage to STAGE_KILL_GUY
ENDPROC

PROC DO_STAGE_KILL_GUY()

	// check if player has killed the target, if so call MISSION_PASSED()
ENDPROC


// debug builds only
#IF IS_DEBUG_BUILD
	// debug skips
	PROC DO_DEBUG_SKIPS(ARGS myArgs)
		INT idx
		
		// mission pass S
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			CLEAR_HELP()
			CLEAR_PRINTS()
			
			RELEASE_SCRIPT_AUDIO_BANK()
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<2133.2668, 4782.7607, 39.9702>>)
				SET_ENTITY_HEADING(myVehicle, 206.8989)
			ENDIF
			cargoArgs.bPassed = TRUE
			planeState =  PLANE_STATE_CUTSCENE
		ENDIF
		
		// mission failed F
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			failReason = FAIL_TRANSPORT_DESTROYED
			MISSION_FAILED(myArgs)
		ENDIF
		
		// skip stage J
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		
//			IF planeState < PLANE_STATE_LAND
//				planeState = PLANE_STATE_LAND
//			ENDIF
			
			CLEAR_PRINTS()
			cargoArgs.bPassed = TRUE
			
			planeState = PLANE_STATE_DROPOFF
			cargoDropStates = DROP_STATE_CHECKS_RUNNING
			
			REPEAT planeData.iNumTotalCargo idx	
				IF NOT cargoArgs.bCargoComplete[idx]
					cargoArgs.bCargoComplete[idx] = TRUE
					cargoArgs.bCargoSuccessful[idx] = TRUE
				ENDIF
			ENDREPEAT
			REPEAT planeData.iNumDrops idx
				IF NOT cargoArgs.bDropSuccessful[idx]
					cargoArgs.bCargoDropComplete[idx] = TRUE
					cargoArgs.bDropSuccessful[idx] = TRUE
				ENDIF
			ENDREPEAT
			
			bJSkipped = TRUE
				
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 2121.2393, 4809.6924, 40.1959 >>)
				SET_ENTITY_HEADING(myVehicle, 204.2772)
			ENDIF
			
		ENDIF
		
		// previous stage P
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			DO_SCREEN_FADE_OUT(500)
				
			CLEAR_PRINTS()
			KILL_ANY_CONVERSATION()
			pSkipping = TRUE
			
			REMOVE_ALL_BLIPS(cargoArgs, myLocationBlip, vCurDestObjective)
			
			REMOVE_RELATIONSHIP_GROUP(cargoArgs.relRivalRunners)
			
			cargoArgs.bPassed = FALSE
			
			REMOVE_DROPOFF_STAGING(myArgs, planeData, cargoArgs)
			
			REPEAT planeData.iNumTotalCargo idx	
				IF NOT cargoArgs.bCargoComplete[idx]
					cargoArgs.bCargoComplete[idx] = FALSE
					cargoArgs.bCargoSuccessful[idx] = FALSE
				ENDIF
			ENDREPEAT
			REPEAT planeData.iNumDrops idx
				IF NOT cargoArgs.bDropSuccessful[idx]
					cargoArgs.bCargoDropComplete[idx] = FALSE
					cargoArgs.bDropSuccessful[idx] = FALSE
				ENDIF
			ENDREPEAT
			
			curStage = STAGE_START
			planeState = PLANE_FADE_WAIT
			
			IF DOES_ENTITY_EXIST(myVehicle)
				SET_ENTITY_AS_MISSION_ENTITY(myVehicle, TRUE, TRUE)
				
//				SPECIAL_FUNCTION_DO_NOT_USE(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2134.06, 4780.69, 41.664>>)
				
				DELETE_VEHICLE(myVehicle)
				PRINTLN("DELETING VEHICLE")
			ENDIF
						
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		ENDIF
	ENDPROC
#ENDIF


//Since we currently pass PLANE_DATA down into the core logic and not ARGS we need to transfer the proper override data
//TODO move this stuff out of PLANE_DATA and just pass in ARGS
PROC CONFIGURE_OVERRIDE_DATA(PLANE_DATA& myPlaneData, ARGS myArgs, BOOL bSetEnd = TRUE)
	INT idx
	fBonusT = myArgs.fBonusTime
	myPlaneData.iNumTotalCargo = myArgs.numCargoBombs
	myPlaneData.iNumDrops = myArgs.numDropLocations
	REPEAT myArgs.numDropLocations idx
		POPULATE_DRUG_LOCATION(myPlaneData.dropData[idx], myArgs.dropLocations[idx])
	ENDREPEAT
	IF bSetEnd
		POPULATE_DRUG_LOCATION(myPlaneData.endData, myArgs.endLocation[0])
	ENDIF
	
	IF myArgs.mnTargetCars[0] <> DUMMY_MODEL_FOR_SCRIPT
		REPEAT MAX_NUMBER_DROPS idx
			myPlaneData.targetCarNames[idx] = myArgs.mnTargetCars[idx]
		ENDREPEAT
	ENDIF
	
	//myPlaneData.iMoneyToPass = myArgs.iMoneyToPass
	myPlaneData.fPickupTime = myArgs.fTimeLimit
	VALIDATE_VARIATIONS_AIR(myPlaneData, bDoDelicate, bTimedPickup, bDoAmbush, bDoCarChase, bDoPlane, bDoBomb, bDoLowAlt, bDoCutScene, bDoStationary, myArgs)
ENDPROC

// main script
SCRIPT(ARGS myArgs)
	INT idx
	DEBUG_PRINT("Traffick Air Start")
	SET_MISSION_FLAG(TRUE)
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
	
		TRIGGER_MUSIC_EVENT("OJDA_STOP")
		PRINTLN("TRIGGERING MUSIC - OJDA_STOP - FAIL")
	
		DEBUG_PRINT("FORCE CLEANUP HAS OCCURRED: setting us up for a replay!!!")
		SET_FORCE_CLEANUP_FAIL_REASON()
		IF NOT IS_REPLAY_BEING_PROCESSED()
			SET_BITMASK_AS_ENUM(sLauncherData.iLauncherFlags, LAUNCHED_SCRIPT_AirTraffickingFailed)
			Setup_Minigame_Replay(GET_THIS_SCRIPT_NAME())
		ENDIF
		MISSION_CLEANUP() // cleanup and terminate thread (must be done in same frame)
	ENDIF
	
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//		DEBUG_PRINT("SETTING PLAYER CONTROL TO FALSE - TO PREVENT EARLY EXIT OF VEHICLE")
//	ENDIF

	cargoArgs.soundID = -1
//	cargoArgs.iNumLanded = -1 
	
	bIsReplay = IS_REPLAY_IN_PROGRESS()
	//Two ways to start.  Getting in the plane created from the launcer.  Or a debug launch and creating our own plane

	//LAUNCHER Start
	IF NOT myArgs.bSpawnVehicle 
		//Fill in myArgs 
		PRINTLN("AIR RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		CONFIGURE_AIR_PROGRESSION(g_savedGlobals.sTraffickingData.iAirRank, myArgs, bIsReplay)
		
		//Fill in planeData...Data assocated with the AIR start
		POPULATE_PLANE_DATA(planeData, ENUM_TO_INT(myArgs.myLocation))
		
		//Since we currently pass PLANE_DATA down into the core logic and not ARGS we need to transfer the proper override data
		CONFIGURE_OVERRIDE_DATA(planeData, myArgs, FALSE)
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				myArgs.trafVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				//myArgs.bDebugLaunch = FALSE
			ENDIF
		ENDIF
	ELSE
		//Fill in planeData...Data assocated with the AIR start
		POPULATE_PLANE_DATA(planeData, ENUM_TO_INT(myArgs.myLocation))
		
		//Since we currently pass PLANE_DATA down into the core logic and not ARGS we need to transfer the proper override data
		CONFIGURE_OVERRIDE_DATA(planeData, myArgs)
	ENDIF
	
	myArgsCopy = myArgs
	
	IF myArgs.bDoUberBombs
		IF g_savedGlobals.sTraffickingData.iAirRank >= 5 
			POPULATE_FINALE_REPEATABLE(planeData)
			airVariation = AIR_VARIATION_BOMB
			PRINTLN("IN REPEATABLE MODE - DOING UBER BOMB")
		ELSE
			POPULATE_FINALE(planeData)
			PRINTLN("IN REGULAR MODE - DOING UBER BOMB")
		ENDIF
	ENDIF
	
	//TODO set this up better! if low alt can't be bomb..
	//TODO remove airVariaton and only use the planeData one
	IF bDoLowAlt
		//SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(planeData.fmaxHeight)
		airVariation = AIR_VARIATION_LOW_ALT
		//planeData.myVariation = AIR_VARIATION_LOW_ALT
		DEBUG_PRINT("planeData.myVariation = AIR_VARIATION_LOW_ALT")
	ELIF bDoBomb
		airVariation = AIR_VARIATION_BOMB
		//planeData.myVariation = AIR_VARIATION_BOMB
		DEBUG_PRINT("planeData.myVariation = AIR_VARIATION_BOMB")
	ELSE
		airVariation = AIR_VARIATION_DROP
		//planeData.myVariation = AIR_VARIATION_DROP
		DEBUG_PRINT("planeData.myVariation = AIR_VARIATION_DROP")
	ENDIF

	PRINTSTRING("Teleport Player")
	
	IF NOT DOES_ENTITY_EXIST(myArgs.trafVehicle) 
		TELEPORT_PLAYER()
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF
	
	IF bDoStationary
		INIT_STATIONARY_PROPS(authoredDataProps)
		DEBUG_PRINT("bDoStationary IS TRUE")
	ENDIF
	
	INIT_MISSION(myArgs)
	//CLEAR_AREA_OF_VEHICLES(<< 2099.1582, 4790.9097, 43.7838 >>, 50)
	CLEAR_AREA_OF_OBJECTS(<< 2035.9138, 4765.7661, 40.0606 >>, 200)
	AddWidgets(cargoArgs.debugData)
	
//	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	IF NOT IS_ENTITY_DEAD(myArgs.trafVehicle)
		SET_VEHICLE_ENGINE_ON(myArgs.trafVehicle, TRUE, TRUE)
	ENDIF
	
	DISABLE_CELLPHONE(TRUE)
	SET_CELLPHONE_PROFILE_TO_NORMAL()
	
	START_AUDIO_SCENE("BAY_DOOR_SCENE")
	PRINTLN("STARTING AUDIO SCENE - BAY_DOOR_SCENE VIA SCRIPT START")
	
	sbi01 = ADD_SCENARIO_BLOCKING_AREA(<<2030.6633, 4760.4531, 40.0605>> - <<800,800,800>>, <<2030.6633, 4760.4531, 40.0605>> + <<800,800,800>>) // Area around airfield
	PRINTLN("ADDING SCENARIO BLOCKING AREA - sbi01")
	sbi02 = ADD_SCENARIO_BLOCKING_AREA(planeData.dropData[0].vCenterPos - <<50,50,50>>, planeData.dropData[0].vCenterPos + <<50,50,50>>) // Area around drop sites
	PRINTLN("ADDING SCENARIO BLOCKING AREA - sbi02")
	SET_ROADS_IN_ANGLED_AREA(<<2164.037842,4825.626953,35.613750>>, <<1899.277832,4697.752441,49.086414>>, 100.000000, FALSE, FALSE)
	
	PRINTLN("myPlaneData.iNumDrops = ", planeData.iNumDrops)
	
	PRINTLN("AIR RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
	
	//This is a bit of a hack.. doing this to prevent having to pass myData struct
	cargoArgs.iNumPackagesTotal = planeData.iNumTotalCargo
	SET_WANTED_LEVEL_MULTIPLIER(0.1)
	
	IF IS_REPLAY_IN_PROGRESS()
		g_replay.iReplayInt[0] ++		//used to strictly to track number of retries specific to trafficking
		
		bReplaying = TRUE
		PRINTLN("bReplaying = TRUE")
		
		iStageToUse = Get_Replay_Mid_Mission_Stage()
		PRINTLN("AIR START - REPLAY IS IN PROGRESS, USING STAGE = ", iStageToUse)
	
		IF iStageToUse = 0
			START_REPLAY_SETUP(vSpawnPosition, fSpawnHeading)
			PRINTLN("AIR: CALLING START REPLAY SETUP FOR CHECKPOINT = ", iStageToUse)
		ENDIF
	ELSE
		g_replay.iReplayInt[0] = 0		//reset this int since replay is not in progress
	ENDIF
	
	REMOVE_HELP_FROM_FLOW_QUEUE("MG_TRAF_AVAIL")
	
    WHILE TRUE
		// main loop	
       	WAIT(0)
		
//		FLOAT fRoll, fPitch
//		
//		IF NOT IS_ENTITY_DEAD(cargoArgs.myPlane)
//			fRoll = GET_ENTITY_ROLL(cargoArgs.myPlane)
//			PRINTLN("PLANE'S ROLL = ", fRoll)
//			fPitch = GET_ENTITY_PITCH(cargoArgs.myPlane)
//			PRINTLN("PLANE'S PITCH = ", fPitch)
//		ENDIF
		
		//DRAW_RACE_TIMER_HUD(25, 2, 0, -1, 0, 0, 0, -1, 0, "STUFF" )
		
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		//PRINT_HELP("TEST")
		
		//DEBUG_UPDATE_BOMB_CAM(planeData, cargoArgs)
		RunWidgets(cargoArgs.debugData)
		
		IF bInAmbush OR bHeliActive OR IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0) OR bIsChaseActive
			bInActiveFight = TRUE
		ELSE
			bInActiveFight = FALSE
		ENDIF
			
		IF bTimedPickup AND (planeState = PLANE_STATE_DROPOFF) AND NOT bRemoveHUD
			//UPDATE_TIMER(startTime, planeData.fPickupTime, bTimerStarted, bTimeExpired)
			UPDATE_TIMER_NEW_PLANE(startTime, planeData.fPickupTime, bTimerStarted, bTimeExpired, bGiveTimeWarning, "ARMS_AIR08", MyLocalPedStruct)
		ENDIF
		
		IF planeState >= PLANE_STATE_LAND
			bLanding = TRUE
		ENDIF
		
		IF curStage > STAGE_START AND (planeState > PLANE_STATE_TAXI AND planeState < PLANE_STATE_CUTSCENE)
			UPDATE_MISSION_CAR(myVehicle, myVehicleBlip, myEntities, myLocationBlip,vCurDestObjective, 
			outOfVehicleTime, bOutOfVehicle, bInActiveFight, bIsCutsceneActive, bDoPlane, bPrintObjAgain,  
			(airVariation = AIR_VARIATION_BOMB AND planeState < PLANE_STATE_LAND), TRUE, NOT bLanding AND NOT myArgs.bDoUberBombs)
			
			IF myArgs.bDoUberBombs AND bOutOfVehicle
				REPEAT MAX_NUMBER_FINAL_TARGETS idx
					IF DOES_BLIP_EXIST(cargoArgs.blipFinal[idx])
						REMOVE_BLIP(cargoArgs.blipFinal[idx])
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
		
		IF planeState <= PLANE_STATE_DROPOFF
			SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_3, FALSE)
			SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_2, FALSE)
		ENDIF
			
//		IF curStage > STAGE_START AND (planeState > PLANE_STATE_TAXI AND planeState < PLANE_STATE_LAND)
//			DRAW_MINIGAME_INSTRUCTIONS(helpScaleform)
//		ENDIF
		
		//IF currentMissionStage > STAGE_PICKUP_C
			UPDATE_LAW(myVehicle, bWanted, myEntities, myLocationBlip, vCurDestObjective, sLastObjective, fCurDist, outOfVehicleTime, myVehicleBlip, bOutOfVehicle, FALSE, (airVariation = AIR_VARIATION_BOMB AND planeState < PLANE_STATE_LAND), NOT myArgs.bDoUberBombs)	
		//ENDIF
	
		//TODO improve this sytem.. very basic and lame atm
		UPDATE_PRINT_DELAY(cargoArgs.tHelpPrintTimer, 7, TRUE)
		//hack..
		UPDATE_QUICK_DELAY()
		UPDATE_QUICK_CONV_QUEUE()
		UPDATE_BLIPPED_ENTITIES()
		
		IF planeState = PLANE_STATE_DROPOFF
		AND NOT bRemoveHUD
			UPDATE_UI(cargoArgs, planeData, myArgs.bDoBomb)
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			// run appropriate stage
			IF UPDATE(myArgs)
				MISSION_CLEANUP()
			ENDIF
		ENDIF
		
//		frameCount++
//		IF frameCount > 30
//			OUTPUT_DEBUG_CAM_RELATIVE_TO_ENTITY(cargoArgs.myPlane)
//			frameCount = 0
//		ENDIF
		
		IF UPDATE_FAIL_CONDITIONS(myArgs)
			MISSION_FAILED(myArgs)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			DO_DEBUG_SKIPS(myArgs)
		#ENDIF
    ENDWHILE	
ENDSCRIPT
