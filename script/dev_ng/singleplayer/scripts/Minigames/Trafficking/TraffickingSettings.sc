

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// TraffickingSettings.sc
USING "drug_trafficking_data.sch"
USING "trafficking_progression.sch"
USING "commands_script.sch"

#IF IS_DEBUG_BUILD
// ************ DEBUG VARIABLES AND FUNCS************
WIDGET_GROUP_ID	TrafWidgets
INT				iLevelDisplay = -1
INT				iLastLevelDisplay = -1

TEXT_WIDGET_ID	airLoc
TEXT_WIDGET_ID	gndLoc
TEXT_LABEL_15	sLocation 	= "Location"

TEXT_WIDGET_ID	airTim
TEXT_WIDGET_ID	gndTim
TEXT_LABEL_15	sTimed 		= "Timed"

TEXT_WIDGET_ID	airCha
TEXT_WIDGET_ID	gndCha
TEXT_LABEL_15	sChase 		= "Chase"

TEXT_WIDGET_ID	airFra
TEXT_WIDGET_ID	gndFra
TEXT_LABEL_15	sFragile 	= "Fragile"

TEXT_WIDGET_ID	airAmb
TEXT_WIDGET_ID	gndAmb
TEXT_LABEL_15	sAmbush 	= "Ambush"

TEXT_WIDGET_ID	airBom
TEXT_WIDGET_ID	gndBom
TEXT_LABEL_15	sBomb 		= "Bomb"

TEXT_WIDGET_ID	airLow
TEXT_WIDGET_ID	gndLow
TEXT_LABEL_15	sLowAlt 	= "LowAlt"


PROC TRAFFICKING_FillOutDebugInfo(ARGS trafStr, BOOL bAir = TRUE)		
	IF bAir
		// Location
		IF (trafStr.myLocation = ELocation_GroundLocation)
			SET_CONTENTS_OF_TEXT_WIDGET(airLoc, "B")
		ELIF (trafStr.myLocation = ELocation_AirLocation)
			SET_CONTENTS_OF_TEXT_WIDGET(airLoc, "C")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(gndLoc, "NO DATA!!!")
		ENDIF
		
		// Timed
		IF (trafStr.bDoTimed)
			SET_CONTENTS_OF_TEXT_WIDGET(airTim, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(airTim, "NO")
		ENDIF
		
		// Chase
		IF (trafStr.bDoChase)
			SET_CONTENTS_OF_TEXT_WIDGET(airCha, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(airCha, "NO")
		ENDIF
		
		// Frag
		IF (trafStr.bDoFragile)
			SET_CONTENTS_OF_TEXT_WIDGET(airFra, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(airFra, "NO")
		ENDIF
		
		// Ambush
		IF (trafStr.bDoAmbush)
			SET_CONTENTS_OF_TEXT_WIDGET(airAmb, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(airAmb, "NO")
		ENDIF
		
		// Bomb
		IF (trafStr.bDoBomb)
			SET_CONTENTS_OF_TEXT_WIDGET(airBom, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(airBom, "NO")
		ENDIF
		
		// Alt
		IF (trafStr.bDoLowAlt)
			SET_CONTENTS_OF_TEXT_WIDGET(airLow, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(airLow, "NO")
		ENDIF
	ELSE
		// Location
		IF (trafStr.myLocation = ELocation_GroundLocation)
			SET_CONTENTS_OF_TEXT_WIDGET(gndLoc, "B")
		ELIF (trafStr.myLocation = ELocation_AirLocation)
			SET_CONTENTS_OF_TEXT_WIDGET(gndLoc, "C")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(gndLoc, "NO DATA!!!")
		ENDIF
		
		// Timed
		IF (trafStr.bDoTimed)
			SET_CONTENTS_OF_TEXT_WIDGET(gndTim, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(gndTim, "NO")
		ENDIF
		
		// Chase
		IF (trafStr.bDoChase)
			SET_CONTENTS_OF_TEXT_WIDGET(gndCha, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(gndCha, "NO")
		ENDIF
		
		// Frag
		IF (trafStr.bDoFragile)
			SET_CONTENTS_OF_TEXT_WIDGET(gndFra, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(gndFra, "NO")
		ENDIF
		
		// Ambush
		IF (trafStr.bDoAmbush)
			SET_CONTENTS_OF_TEXT_WIDGET(gndAmb, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(gndAmb, "NO")
		ENDIF
		
		// Bomb
		IF (trafStr.bDoBomb)
			SET_CONTENTS_OF_TEXT_WIDGET(gndBom, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(gndBom, "NO")
		ENDIF
		
		// Alt
		IF (trafStr.bDoLowAlt)
			SET_CONTENTS_OF_TEXT_WIDGET(gndLow, "YES")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(gndLow, "NO")
		ENDIF
	ENDIF
ENDPROC
// *****************************************

PROC SCRIPT_CLEANUP()
	IF DOES_WIDGET_GROUP_EXIST(TrafWidgets)
		DELETE_WIDGET_GROUP(TrafWidgets)
	ENDIF
	TERMINATE_THIS_THREAD()
ENDPROC
#ENDIF

SCRIPT
	#IF IS_DEBUG_BUILD
		// Do we need to cleanup?
		IF (HAS_FORCE_CLEANUP_OCCURRED())
			//DEBUG_MESSAGE("...launcher_Trafficking.sc has been asked to clean up")
			SCRIPT_CLEANUP()
		ENDIF
	
		ARGS structBlank
		TrafWidgets = START_WIDGET_GROUP("Trafficking")
			ADD_WIDGET_INT_SLIDER("Level Settings", iLevelDisplay, -1, 10, 1)

			ADD_WIDGET_STRING("AIR DATA:")
			airLoc = ADD_TEXT_WIDGET(sLocation)
			airTim = ADD_TEXT_WIDGET(sTimed)
			airCha = ADD_TEXT_WIDGET(sChase)
			airFra = ADD_TEXT_WIDGET(sFragile)
			airAmb = ADD_TEXT_WIDGET(sAmbush)
			airBom = ADD_TEXT_WIDGET(sBomb)
			airLow = ADD_TEXT_WIDGET(sLowAlt)
			
			ADD_WIDGET_STRING("GROUND DATA:")
			gndLoc = ADD_TEXT_WIDGET(sLocation)
			gndTim = ADD_TEXT_WIDGET(sTimed)
			gndCha = ADD_TEXT_WIDGET(sChase)
			gndFra = ADD_TEXT_WIDGET(sFragile)
			gndAmb = ADD_TEXT_WIDGET(sAmbush)
			gndBom = ADD_TEXT_WIDGET(sBomb)
			gndLow = ADD_TEXT_WIDGET(sLowAlt)
			
			// These sliders affect the actual global values!!!
			ADD_WIDGET_STRING("BELOW VALUES AFFECT GLOBAL DATA. NEXT TIME YOU LAUNCH, IT WILL BE WITH THESE LEVELS:")
			ADD_WIDGET_INT_SLIDER("Air Level to Launch Next", g_savedGlobals.sTraffickingData.iAirRank, 0, 10, 1)
			ADD_WIDGET_INT_SLIDER("Ground Level to Launch Next", g_savedGlobals.sTraffickingData.iGroundRank, 0, 10, 1)
		STOP_WIDGET_GROUP()
		
		WHILE TRUE
			IF (iLevelDisplay <> -1) AND (iLevelDisplay > -1)
				IF (iLevelDisplay <> iLastLevelDisplay)									
					// We've altered the level. Fill us back in.
					ARGS airStruct = structBlank
					CONFIGURE_AIR_PROGRESSION(iLevelDisplay, airStruct, FALSE)
					TRAFFICKING_FillOutDebugInfo(airStruct)
					
					ARGS gndStruct = structBlank
					CONFIGURE_GROUND_PROGRESSION(iLevelDisplay, gndStruct)
					TRAFFICKING_FillOutDebugInfo(gndStruct, FALSE)
					
					iLastLevelDisplay = iLevelDisplay
				ENDIF
			ENDIF
			
			WAIT(100)
		ENDWHILE
	#ENDIF
ENDSCRIPT
