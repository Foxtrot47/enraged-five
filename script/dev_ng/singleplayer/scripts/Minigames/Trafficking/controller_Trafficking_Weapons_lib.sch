// controller_Trafficking_Weapons_lib.sch
USING "controller_trafficking.sch"
CONST_INT 			MAX_NUMBER_CRATES 5
CONST_INT 			MILLISECONDS_IN_TWO_HOURS 7200000

TRAFFICKING_STATE 	eWeaponState
OBJECT_INDEX 		oWeaponsCrate[MAX_NUMBER_CRATES]
PICKUP_INDEX 		pickupWeaponCrate[MAX_NUMBER_CRATES]
BOOL 				bHasWeaponBeenCollected[MAX_NUMBER_CRATES]

PROC CREATE_WEAPONS_PICKUPS(VECTOR & vCratePos[], INT idx)
	PICKUP_TYPE ptWeaponToGive = PICKUP_WEAPON_COMBATPISTOL
	VECTOR vOffset[5]
	
	SWITCH (idx)
		CASE 0
			ptWeaponToGive = PICKUP_WEAPON_COMBATPISTOL
	    	vOffset[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCratePos[0], GET_ENTITY_HEADING(oWeaponsCrate[0]), << 0, 0.0, 0.55>>)
		BREAK
		CASE 1
			ptWeaponToGive = PICKUP_WEAPON_SMG
	    	vOffset[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCratePos[1], GET_ENTITY_HEADING(oWeaponsCrate[1]), << 0, 0.0, 0.55>>)
		BREAK
		CASE 2
			ptWeaponToGive = PICKUP_WEAPON_STICKYBOMB
	    	vOffset[2] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCratePos[2], GET_ENTITY_HEADING(oWeaponsCrate[2]), << 0, 0.0, 0.55>>)
		BREAK
		CASE 3
			ptWeaponToGive = PICKUP_WEAPON_ASSAULTSHOTGUN
	    	vOffset[3] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCratePos[3], GET_ENTITY_HEADING(oWeaponsCrate[3]), << 0, -0.11, 0.55>>)
		BREAK
		CASE 4
			ptWeaponToGive = PICKUP_WEAPON_RPG
	   		vOffset[4] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCratePos[4], GET_ENTITY_HEADING(oWeaponsCrate[4]), << 0, 0.25, 0.58>>)
		BREAK
	ENDSWITCH
      
    pickupWeaponCrate[idx] = CREATE_PICKUP_ROTATE(ptWeaponToGive, vOffset[idx], <<90,0,0>>)
    PRINTLN("CREATING PICKUP")
ENDPROC

PROC TRAFFICKING_CREATE_WEAPONS_CRATES()
	VECTOR vCratePos[MAX_NUMBER_CRATES]
	vCratePos[0] = << 2141.207, 4789.468, 39.970 >>
    vCratePos[1] = << 2141.747, 4788.218, 39.970 >>
    vCratePos[2] = << 2142.307, 4786.938, 39.970 >>
    vCratePos[3] = << 2142.827, 4785.678, 39.970 >>
    vCratePos[4] = << 2143.387, 4784.448, 39.970 >>
	
	INT idx
	FOR idx = 0 TO (g_savedGlobals.sTraffickingData.iGroundRank - 1)
        IF (g_savedGlobals.sTraffickingData.iGroundRank <= 5)
            // Check if it's okay to create the pickup.
            IF NOT DOES_ENTITY_EXIST(oWeaponsCrate[idx])
                // Always create the crate
//                oWeaponsCrate[idx] = CREATE_OBJECT(PROP_DROP_CRATE_01, vCratePos[idx])
                SET_ENTITY_ROTATION(oWeaponsCrate[idx], << 0, 0, -67.0 >>)
                FREEZE_ENTITY_POSITION(oWeaponsCrate[idx], TRUE)
                PRINTLN("CREATING WEAPON CRATE = ", idx)
          
                // If no time has been set... it's the first time, so go ahead and create the pickup.
                IF g_savedGlobals.sTraffickingData.iTimePackageCollected[idx] = 0
                      CREATE_WEAPONS_PICKUPS(vCratePos, idx)
                ELSE
					// If a time has been set, then check against the current time to see if it's been long enough to create.
                    INT iCurrentTime = GET_GAME_TIMER()
                    PRINTLN("iCurrentTime = ", iCurrentTime)
                    PRINTLN("iTimePackageCollected = ", g_savedGlobals.sTraffickingData.iTimePackageCollected[idx])
                      
                    IF (iCurrentTime >= (g_savedGlobals.sTraffickingData.iTimePackageCollected[idx] + MILLISECONDS_IN_TWO_HOURS))          
                        CREATE_WEAPONS_PICKUPS(vCratePos, idx)
                        PRINTLN("TIME IS OKAY TO CREATE WEAPON PICKUP")
                    ENDIF
                ENDIF
          	ENDIF
        ENDIF
    ENDFOR
	
	PRINTLN("Ground rank: ", g_savedGlobals.sTraffickingData.iGroundRank, " and num made is: ", idx)
ENDPROC

PROC REQUEST_WEAPONS_MODELS()
	REQUEST_WEAPON_ASSET(WEAPONTYPE_COMBATPISTOL)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_SMG)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_STICKYBOMB)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTSHOTGUN)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_RPG)
	
	PRINTLN("REQUESTING WEAPON MODELS")
ENDPROC

FUNC BOOL HAVE_WEAPONS_STREAMED()
	RETURN HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_COMBATPISTOL)
		AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SMG)
		AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_STICKYBOMB)
		AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTSHOTGUN)
		AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_RPG)
ENDFUNC

/// PURPOSE:
///    Cleans all data associated with the weapons part of trafficking.
PROC TRAFFICKING_WEAPONS_CLEANUP()
	bTriggerWeaponsReset = FALSE
	
//	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_DROP_CRATE_01)

	REMOVE_WEAPON_ASSET(WEAPONTYPE_COMBATPISTOL)
	REMOVE_WEAPON_ASSET(WEAPONTYPE_SMG)
	REMOVE_WEAPON_ASSET(WEAPONTYPE_STICKYBOMB)
	REMOVE_WEAPON_ASSET(WEAPONTYPE_ASSAULTSHOTGUN)
	REMOVE_WEAPON_ASSET(WEAPONTYPE_RPG)
	
	INT idx
	REPEAT MAX_NUMBER_CRATES idx
		IF DOES_PICKUP_EXIST(pickupWeaponCrate[idx])
			REMOVE_PICKUP(pickupWeaponCrate[idx])
		ENDIF
		
		IF DOES_ENTITY_EXIST(oWeaponsCrate[idx])
			DELETE_OBJECT(oWeaponsCrate[idx])
		ENDIF
	ENDREPEAT
ENDPROC

PROC TRAFFICKING_WEAPONS_UPDATE(BOOL bIsAirReplay, BOOL bIsGroundReplay)
	// See if we're being force reset.
	IF (eWeaponState != TKS_AREA_WAIT)
		IF TRAFFICKING_SHOULD_CLEAN_AND_WAIT()
			TRAFFICKING_WEAPONS_CLEANUP()
			eWeaponState = TKS_AREA_WAIT
		ENDIF
	ENDIF
	
	
	// General update
	SWITCH (eWeaponState)
		CASE TKS_INIT
			//PRINTLN("Weapons -- TKS_INIT")
			eWeaponState = TKS_AREA_WAIT
		BREAK

		CASE TKS_AREA_WAIT	
			//PRINTLN("TKS_AREA_WAIT")
			// Make sure the player is within range of the actual vehicle. If he is, move to stream it.
			IF NOT TRAFFICKING_SHOULD_CLEAN_AND_WAIT()
				IF (fDist2ToLauncher < TRAF_UPDATE_DIST2) AND (g_savedGlobals.sTraffickingData.iGroundRank > 0)
					eWeaponState = TKS_STREAM
				ENDIF
			ENDIF
		BREAK
		
		CASE TKS_STREAM
			//PRINTLN("Weapons -- TKS_STREAM")
			// Request all streams here. Once done, move on to waiting for them.
//			REQUEST_MODEL(PROP_DROP_CRATE_01)
			REQUEST_WEAPONS_MODELS()
			
			eWeaponState = TKS_STREAM_WAIT
		BREAK
		
		CASE TKS_STREAM_WAIT
			//PRINTLN("Weapons -- TKS_STREAM_WAIT")
			// When everything is done loading, move to waiting for the player to engage the launcher.
//			IF HAS_MODEL_LOADED(PROP_DROP_CRATE_01)
//			AND HAVE_WEAPONS_STREAMED()
				TRAFFICKING_CREATE_WEAPONS_CRATES()
			
				eWeaponState = TKS_RUN_WAIT
//			ENDIF
		BREAK
		
		CASE TKS_RUN_WAIT
			//PRINTLN("Weapons -- TKS_RUN_WAIT")
			// If we can picklup weapons, watch for it.
			INT idx
			REPEAT MAX_NUMBER_CRATES idx
			    IF NOT bHasWeaponBeenCollected[idx]
		            IF DOES_PICKUP_EXIST(pickupWeaponCrate[idx])
			            IF HAS_PICKUP_BEEN_COLLECTED(pickupWeaponCrate[idx])
		                    g_savedGlobals.sTraffickingData.iTimePackageCollected[idx] = GET_GAME_TIMER()
		                    PRINTLN("HAS_PICKUP_BEEN_COLLECTED IS TRUE FOR = ", idx)
		                    PRINTLN("TIME COLLECTED = ", g_savedGlobals.sTraffickingData.iTimePackageCollected[idx])
		                    bHasWeaponBeenCollected[idx] = TRUE
			            ENDIF
			        ENDIF
			    ENDIF
			ENDREPEAT
	  
			// Make sure the player's still in the area, and we care.
			IF ((fDist2ToLauncher >= TRAF_UPDATE_DIST2) OR bTriggerWeaponsReset) AND NOT (bIsAirReplay OR bIsGroundReplay)
				TRAFFICKING_WEAPONS_CLEANUP()
				eWeaponState = TKS_AREA_WAIT
			ENDIF			
		BREAK
		
		DEFAULT
			PRINTLN("DEFAULT--Weapons: Bug this for David S.")
		BREAK
	ENDSWITCH
ENDPROC
