
USING "script_oddjob_funcs.sch"
USING "Overlay_Effects.sch"


CONST_FLOAT EXPLOSION_CAM_BOMB_PITCH		79.1

BOOL bPlayerAttached = FALSE
BOOL bPrintHeightWarning = FALSE
BOOL bStartSceneAgain = FALSE
BOOL bPrintHeighWarningHelp = FALSE

FLOAT fCamFov = 65.0

BOOL bCreatedThisFrame = FALSE

COLLISION_ARGS collisionArgs
FLOAT fHeightAtRelease = -1.0
FLOAT fLerpCount = 1.0

BOOL bPlaneCollidable = TRUE

VECTOR vOrigOrientation

//Hack wrapper functions to attach / detach the player to the dropped bomb for bounds loading
PROC ATTACH_PLAYER_TO_BOMB(ENTITY_INDEX bombProp)
	//COMMENT this back in when this is working again
	SPECIAL_FUNCTION_DO_NOT_USE(PLAYER_PED_ID())
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
	
	PRINTLN("ATTACH_PLAYER_FROM_BOMB: Attaching player")
	ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), bombProp, 0, <<0,0,0>>, <<0,0,0>>)
	SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
	bPlayerAttached = TRUE
ENDPROC

PROC DETACH_PLAYER_FROM_BOMB(VEHICLE_INDEX myPlane)
	//Comment back in when we can attach player to bomb.
	PRINTLN("DETACH_PLAYER_FROM_BOMB: Detaching player")
	DETACH_ENTITY(PLAYER_PED_ID())
	SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),myPlane)
	bPlayerAttached = FALSE
ENDPROC

FUNC BOOL EVALUATE_DROP_CONDITIONS(ARGS& myArgs, PLANE_CARGO_ARGS& cargoArgs, structPedsForConversation MyLocalPedStruct, BOOL& bPlayConvo, AIR_VARIATION myVariation = AIR_VARIATION_BOMB, BOOL bInCam = FALSE)
	
	VECTOR playerPos
	playerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	BOOL bCanDrop = TRUE
	
	FLOAT fWaterHeight = 0
	IF GET_WATER_HEIGHT_NO_WAVES(GET_ENTITY_COORDS(PLAYER_PED_ID()), fWaterHeight)
		//PRINTLN("water height:", fWaterHeight, "Playerz: ", playerPos.z )
		IF (playerPos.z - fWaterHeight) < 16.5
			bCanDrop = FALSE
			
			IF NOT bPrintHeighWarningHelp
				PRINT_HELP("DTRFAIR_HEIGHT")
				PRINTLN("bPrintHeighWarningHelp = TRUE")
				bPrintHeighWarningHelp = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_ENTITY_HEIGHT_ABOVE_GROUND(cargoArgs.myPlane) < 6.0 OR GET_ENTITY_SPEED(PLAYER_PED_ID()) < 10	
	OR IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		IF NOT bPrintHeighWarningHelp
			PRINT_HELP("DTRFAIR_HEIGHT")
			PRINTLN("bPrintHeighWarningHelp = TRUE")
			bPrintHeighWarningHelp = TRUE
		ENDIF
	
		bCanDrop = FALSE
	ENDIF
	
	
	//TODO this is currenlty breaking the abort drop cam
	#IF IS_DEBUG_BUILD
		FLOAT fHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(cargoArgs.myPlane)
		PRINTLN("Height from ground: ", fHeight)
	#ENDIF	//	IS_DEBUG_BUILD
	
	IF GET_ENTITY_HEIGHT_ABOVE_GROUND(cargoArgs.myPlane) > 230 AND NOT bInCam
		IF myVariation = AIR_VARIATION_BOMB AND myArgs.bDoBomb
			IF bPlayConvo
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_ALTP", CONV_PRIORITY_VERY_HIGH)
				bPlayConvo = FALSE
			ENDIF
			IF NOT bPrintHeightWarning 
				PRINT_HELP("DTRFAIR_ERR1")
				PRINTLN("BOMB - HEIGHT WARNING")
				bPrintHeightWarning = TRUE
			ENDIF
		ELSE
			IF bPlayConvo
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_ALTP", CONV_PRIORITY_VERY_HIGH)
				bPlayConvo = FALSE
			ENDIF
			IF NOT bPrintHeightWarning 
				PRINT_HELP("DTRFAIR_ERR2")
				PRINTLN("PACKAGE - HEIGHT WARNING")
				bPrintHeightWarning = TRUE
			ENDIF
		ENDIF
		bCanDrop = FALSE
	ENDIF

	//If we're too high don't drop.. need a print as well
	
	IF bCanDrop = FALSE //AND IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
		ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BOMB_DROP_ERROR_MASTER", cargoArgs.soundID)
	ENDIF

	RETURN bCanDrop
ENDFUNC

BOOL bInCam = FALSE

PROC TOGGLE_VISIBILITY(PLANE_CARGO_ARGS& cargoArgs, OBJECT_INDEX myEntity, BOOL bUberBombs = FALSE, BOOL bVisible = FALSE)
	INT idx, idx2
	PRINTLN("Calling Toggle Visibility uber, visible", bUberBombs, bVisible)
	IF DOES_ENTITY_EXIST(cargoArgs.myPlane)
		PRINTLN("Setting plane invisible: ", bVisible)
		SET_ENTITY_VISIBLE(cargoArgs.myPlane, bVisible)
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		PRINTLN("Setting player invisible: ", bVisible)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), bVisible)
	ENDIF
	
	REPEAT cargoArgs.iNumPackagesTotal idx
		IF DOES_ENTITY_EXIST(cargoArgs.oCargoDrop[idx]) AND myEntity <> cargoArgs.oCargoDrop[idx]
			PRINTLN("Setting package invisible: ", "num: ", idx, bVisible)
			SET_ENTITY_VISIBLE(cargoArgs.oCargoDrop[idx], bVisible)
			IF bUberBombs
				REPEAT MAX_NUMBER_CLUSTER idx2
					IF DOES_ENTITY_EXIST(cargoArgs.oClusterDrop[idx][idx2])
						IF IS_ENTITY_ATTACHED(cargoArgs.oClusterDrop[idx][idx2])
							SET_ENTITY_VISIBLE(cargoArgs.oClusterDrop[idx][idx2], bVisible)
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


FUNC BOOL IS_COLLISION_IMMINENT(ENTITY_INDEX myPlane, VECTOR vPlayerPos, FLOAT yDist = 230.0)
	VECTOR planeCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myPlane, <<0,yDist,0>>)
	DEBUG_MESSAGE(" yDist: ")
	PRINTFLOAT(yDist)
	PRINTNL()
	PRINTVECTOR(planeCoord)
	DEBUG_MESSAGE(" playerPos: ")
	PRINTVECTOR(vPlayerPos)
	PRINTNL()
	
	//If we are really low to the ground lets bail out as well
	IF GET_ENTITY_HEIGHT_ABOVE_GROUND(myPlane) < 20.0
		RETURN TRUE
	ENDIF
	
	FLOAT zGround
	BOOL bHasBounds = FALSE
	planeCoord.z += 240.0
	IF GET_GROUND_Z_FOR_3D_COORD(planeCoord, zGround)
		bHasBounds = TRUE
	//ELIF GET_WATER_HEIGHT_NO_WAVES(planeCoord, zGround)
		//bHasBounds = TRUE
	ENDIF
	
	IF bHasBounds
	
		DEBUG_MESSAGE("Has Bounds")
		PRINTFLOAT(zGround)
		PRINTNL()
		PRINTFLOAT(vPlayerPos.z)
		IF (zGround+15.0) > vPlayerPos.z
			RETURN TRUE
		ELSE 
			RETURN FALSE
		ENDIF
	ENDIF
	
	DEBUG_MESSAGE("NO BOUNDS")
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_COLLISION_IMMINENT_WRAPPER(ENTITY_INDEX myPlane)
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	IF IS_COLLISION_IMMINENT(myPlane, vPlayerPos, 180) OR IS_COLLISION_IMMINENT(myPlane, vPlayerPos, 130)
	OR IS_COLLISION_IMMINENT(myPlane, vPlayerPos, 80)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



INT testSoundID = -1
FLOAT fBlurStrength = 0.01
//This is the camera that attaches to the package and follows the bomb down
FUNC BOOL DO_EXPLOSION_CAM(PLANE_CARGO_ARGS& cargoArgs, OBJECT_INDEX& myEntity)
	CLEAR_TIMECYCLE_MODIFIER()
	OVERLAY_SET_SECURITY_CAM(FALSE,FALSE)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	SET_VEHICLE_USED_FOR_PILOT_SCHOOL(cargoArgs.myPlane, TRUE)
	PRINTLN("DISABLING FLIGHT SKILL - 01")
	
	bCreatedThisFrame = TRUE
	SET_ENTITY_COLLISION(myEntity, FALSE)
	
	VECTOR vRotTemp = GET_ENTITY_ROTATION(myEntity)
	vRotTemp.x = EXPLOSION_CAM_BOMB_PITCH
	
	SET_ENTITY_ROTATION(myEntity, vRotTemp)
	SET_ENTITY_ROTATION(cargoArgs.oBombDummy, vRotTemp)
	
	SET_ENTITY_COLLISION(cargoArgs.oBombDummy, FALSE)
	
	bInCam = TRUE
	TOGGLE_VISIBILITY(cargoArgs, myEntity, FALSE)
	DESTROY_CAM(cargoArgs.CamExplosion)
	//exit the targeting cam
	cargoArgs.bBombCam = FALSE

	IF NOT DOES_CAM_EXIST(cargoArgs.CamExplosion)
		cargoArgs.CamExplosion = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, 65.00, FALSE)
		PRINTLN("CREATING EXPLOSION CAM - 01")
	ENDIF
	
	//SET_CAM_MOTION_BLUR_STRENGTH(cargoArgs.CamExplosion, fBlurStrength)

	ATTACH_CAM_TO_ENTITY(cargoArgs.CamExplosion, cargoArgs.oBombDummy, <<0.2402, 1.3324, -0.1095>>, TRUE)
	POINT_CAM_AT_ENTITY(cargoArgs.CamExplosion, cargoArgs.oBombDummy, <<0.3169, -1.8554, 0.1502>>)

	ATTACH_PLAYER_TO_BOMB(myEntity)

	SHAKE_CAM(cargoArgs.CamExplosion, "ROAD_VIBRATION_SHAKE", 1.0)
		
	SET_CAM_FOV(cargoArgs.CamExplosion, 65.0)
	SET_CAM_ACTIVE(cargoArgs.CamExplosion, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	DISPLAY_RADAR(FALSE)
	DISPLAY_HUD(FALSE)
	fCamFov = 65.0
		
	ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BOMB_CAM_MASTER", testSoundID, TRUE)
	
	VECTOR vProbeStart = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(cargoArgs.myPlane, <<0.0, 20.0, 0.0>>)
	
	GET_GROUND_Z_FOR_3D_COORD(vProbeStart, fHeightAtRelease)
	
	SET_ENTITY_COLLISION(cargoArgs.myPlane, FALSE)
	bPlaneCollidable = FALSE
	//SET_FOCUS_POS_AND_VEL(GET_ENTITY_COORDS(cargoArgs.myPlane), <<-60.8813, 5.0506, 122.7079>>)
	
	RETURN TRUE
ENDFUNC

//Old targeting cam
//FUNC BOOL SETUP_BOMB_CAM(PLANE_CARGO_ARGS& cargoArgs)
//
//	DEBUG_PRINT("INSIDE SETUP_BOMB_CAM")
//	DESTROY_CAM(cargoArgs.CamExplosion)
//	RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			
//	//TODO if the vehicle isn't flying.. don't do bomb cam.
//	IF EVALUATE_DROP_CONDITIONS(cargoArgs)
//		IF NOT DOES_CAM_EXIST(cargoArgs.CamExplosion)
//			cargoArgs.CamExplosion = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, 95, FALSE)
//		ENDIF
//		
//		ATTACH_CAM_TO_ENTITY(cargoArgs.CamExplosion, cargoArgs.myPlane, <<0,-0.65,-0.60>>, TRUE)
//		SET_CAM_ACTIVE(cargoArgs.CamExplosion, TRUE)
//		RENDER_SCRIPT_CAMS(TRUE, FALSE)
//		DISPLAY_RADAR(FALSE)
//		DISPLAY_HUD(FALSE)
//		DEBUG_PRINT("RETURNING TRUE ON SETUP_BOMB_CAM")
//		RETURN TRUE
//	ELSE
//		RETURN FALSE
//	ENDIF
//	
//ENDFUNC

//Attach the camera to the bomb bay door view
//bCut is used to only call LAST_BOMB_CUT_SINGLE once.. hack..
FUNC BOOL SETUP_CARPET_BOMB_CAM(ARGS& myArgs, PLANE_CARGO_ARGS& cargoArgs)
	DEBUG_PRINT("INSIDE SETUP_BOMB_CAM")
	DESTROY_CAM(cargoArgs.CamExplosion)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	FLOAT fHeading
	fHeading = fHeading
	//TODO if the vehicle isn't flying.. don't do bomb cam.
	structPedsForConversation temp
	BOOL bPlayCon = FALSE
	
	PRINTLN("EVALUATE_DROP_CONDITIONS VIA SETUP_CARPET_BOMB_CAM")
	IF EVALUATE_DROP_CONDITIONS(myArgs, cargoArgs, temp, bPlayCon)
		
		IF NOT DOES_CAM_EXIST(cargoArgs.CamExplosion)
			fHeading = GET_ENTITY_HEADING(cargoArgs.myPlane)
			cargoArgs.CamExplosion = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0, 0, 0>>, 65, FALSE)
			SHAKE_CAM(cargoArgs.CamExplosion, "ROAD_VIBRATION_SHAKE", 0.5)
			PRINTLN("CREATING EXPLOSION CAM - 02")
		ENDIF
		cargoArgs.iCutGameTimeStart = GET_GAME_TIMER()
		ATTACH_CAM_TO_ENTITY(cargoArgs.CamExplosion, cargoArgs.myPlane, <<0, 1.44, -0.9565>>)
		POINT_CAM_AT_ENTITY(cargoArgs.CamExplosion, cargoArgs.myPlane, <<-0.1281, -1.5596, -1.4592>>)
		
		SET_VEHICLE_USED_FOR_PILOT_SCHOOL(cargoArgs.myPlane, TRUE)
		PRINTLN("DISABLING FLIGHT SKILL - 02")
		
		SET_CAM_ACTIVE(cargoArgs.CamExplosion, TRUE)
		DISPLAY_RADAR(FALSE)
		DISPLAY_HUD(FALSE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		DEBUG_PRINT("RETURNING TRUE ON SETUP_BOMB_CAM")
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC BOOL SETUP_OPEN_BOMB_CAM(ARGS& myArgs, PLANE_CARGO_ARGS& cargoArgs)
	DEBUG_PRINT("INSIDE SETUP_BOMB_CAM")
	DESTROY_CAM(cargoArgs.CamExplosion)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	FLOAT fHeading
	fHeading = fHeading
	structPedsForConversation temp
	BOOL bPlayCon = FALSE
	//TODO if the vehicle isn't flying.. don't do bomb cam.
	
	PRINTLN("EVALUATE_DROP_CONDITIONS VIA SETARGS& myArgsUP_OPEN_BOMB_CAM")
	IF EVALUATE_DROP_CONDITIONS(myArgs, cargoArgs, temp, bPlayCon)
		IF NOT DOES_CAM_EXIST(cargoArgs.CamExplosion)
			fHeading = GET_ENTITY_HEADING(cargoArgs.myPlane)
			cargoArgs.CamExplosion = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0, 0, 0>>, 65, FALSE)
			PRINTLN("CREATING EXPLOSION CAM - 03")
		ENDIF
		
		ATTACH_CAM_TO_ENTITY(cargoArgs.CamExplosion, cargoArgs.myPlane, <<0.0018, -1.0467, -0.9686>>)
		POINT_CAM_AT_ENTITY(cargoArgs.CamExplosion, cargoArgs.myPlane, <<0.0586, 1.9170, -1.2309>>)
		SET_CAM_FOV(cargoArgs.CamExplosion, 50.0000)
		
		SET_VEHICLE_USED_FOR_PILOT_SCHOOL(cargoArgs.myPlane, TRUE)
		PRINTLN("DISABLING FLIGHT SKILL - 03")
		
		SET_CAM_ACTIVE(cargoArgs.CamExplosion, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		DISPLAY_RADAR(FALSE)
		DISPLAY_HUD(FALSE)
		DEBUG_PRINT("RETURNING TRUE ON SETUP_BOMB_CAM")
		
		STOP_AUDIO_SCENE("BAY_DOOR_SCENE")
		PRINTLN("STOP AUDIO SCENE - BAY_DOOR_SCENE VIA SETUP_OPEN_BOMB_CAM")
		
		TRIGGER_MUSIC_EVENT("OJDA1_HATCH_OPEN")
		PRINTLN("TRIGGERING MUSIC - OJDA1_HATCH_OPEN")
		
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

//After all of the bombs have dropped from the plan we attach the cam to the final bomb and follow it down
PROC LAST_BOMB_CUT(PLANE_CARGO_ARGS& cargoArgs, ENTITY_INDEX myEntity)
	VECTOR rotVec
	rotVec = GET_CAM_ROT(cargoArgs.CamExplosion)
	cargoArgs.CamTransition = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, rotVec, 65.00, TRUE)

	INT iInterpTime
	FLOAT fHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(cargoArgs.myPlane)
	

	IF fHeight < 35
		PRINTLN("DIST: ", GET_ENTITY_HEIGHT_ABOVE_GROUND(cargoArgs.myPlane))
		iInterpTime = 3000
		ATTACH_CAM_TO_ENTITY(cargoArgs.CamTransition, myEntity, <<1.0262, -9.9728, 1.8926>>, TRUE)
		POINT_CAM_AT_ENTITY(cargoArgs.CamTransition, myEntity, <<0.8841, -7.0213, 2.4110>>)
	ELIF fHeight < 70
		PRINTLN("DIST: ", GET_ENTITY_HEIGHT_ABOVE_GROUND(cargoArgs.myPlane))
		iInterpTime = 3500
		ATTACH_CAM_TO_ENTITY(cargoArgs.CamTransition, myEntity, <<1.1287, -8.3258, 0.7281>>, TRUE)
		POINT_CAM_AT_ENTITY(cargoArgs.CamTransition, myEntity, <<0.9513, -5.4114, 1.4169>>)
	ELSE
		PRINTLN("DIST: ", GET_ENTITY_HEIGHT_ABOVE_GROUND(cargoArgs.myPlane))
		iInterpTime = 5000
		ATTACH_CAM_TO_ENTITY(cargoArgs.CamTransition, myEntity,  <<1.4631, -3.0219, -3.0115>>, TRUE)
		POINT_CAM_AT_ENTITY(cargoArgs.CamTransition, myEntity, <<1.1996, -0.3242, -1.7257>>)
	ENDIF

	// 2nd Shot

	TOGGLE_VISIBILITY(cargoArgs, NULL, TRUE, FALSE)
	PRINTLN("[LAST_BOMB_CUT] Hiding plane, and bomb if they are attached.")
	
	IF NOT IS_VECTOR_ZERO(vCamTransitionOffsetPosition) AND NOT IS_VECTOR_ZERO(vCamTransitionOffsetRotation)
		cargoArgs.CamTransition01 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, rotVec, 65.00, FALSE)
		ATTACH_CAM_TO_ENTITY(cargoArgs.CamTransition01, myEntity,  vCamTransitionOffsetPosition, TRUE)
		POINT_CAM_AT_ENTITY(cargoArgs.CamTransition01, myEntity, vCamTransitionOffsetRotation)
	ENDIF
	
	PRINTLN("vCamTransitionOffsetPosition = ", vCamTransitionOffsetPosition)
	PRINTLN("vCamTransitionOffsetRotation = ", vCamTransitionOffsetRotation)

	PRINTLN("CREATING NEW CAM")
	
	SHAKE_CAM(cargoArgs.CamTransition, "HAND_SHAKE", 2.0)
	SHAKE_CAM(cargoArgs.CamTransition01, "HAND_SHAKE", 2.0)
	
//	SET_CAM_MOTION_BLUR_STRENGTH(cargoArgs.CamTransition, 0.25)
//	SET_CAM_MOTION_BLUR_STRENGTH(cargoArgs.CamTransition01, 0.25)
	
	SET_CAM_ACTIVE_WITH_INTERP(cargoArgs.CamTransition01, cargoArgs.CamTransition, iInterpTime)
	
	ATTACH_PLAYER_TO_BOMB(myEntity)
	
	ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BOMB_CAM_MASTER", cargoArgs.soundID, TRUE)

	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	DISPLAY_RADAR(FALSE)
	DISPLAY_HUD(FALSE)
ENDPROC

PROC CLEANUP_CLUSTER_CAM(PLANE_CARGO_ARGS& cargoArgs, ENTITY_INDEX myEntity, INT delay = 0)
	DESTROY_CAM(cargoArgs.CamTransition)
	DESTROY_CAM(cargoArgs.CamTransition01)
	DESTROY_CAM(cargoArgs.CamExplosion)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	TOGGLE_VISIBILITY(cargoARgs, NULL, TRUE, TRUE)
	
	IF bPlayerAttached
		DETACH_PLAYER_FROM_BOMB(cargoArgs.myPlane)
	ENDIF

	cargoArgs.CamExplosion = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1,1,1>>, <<0,0,0>>, 60.00, FALSE)
	PRINTLN("CREATING EXPLOSION CAM - 04")

	ODDJOB_STOP_SOUND(cargoArgs.soundID)

	ATTACH_CAM_TO_ENTITY(cargoArgs.CamExplosion, cargoArgs.myPlane, <<0,-0.65,-1.60>>, TRUE)
	SHAKE_CAM(cargoArgs.CamExplosion, "HAND_SHAKE", 1.5)
	POINT_CAM_AT_ENTITY(cargoArgs.CamExplosion, myEntity, <<0,0,0>>)

	SET_VEHICLE_USED_FOR_PILOT_SCHOOL(cargoArgs.myPlane, FALSE)
	PRINTLN("ENABLING FLIGHT SKILL - 01")
	
	SET_CAM_ACTIVE(cargoArgs.CamExplosion, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	cargoArgs.iCutGameTimeStart = 0
	cargoArgs.iCutGameTime = GET_GAME_TIMER() + delay
ENDPROC

PROC CLEANUP_EXPLOSION_CAM(PLANE_CARGO_ARGS& cargoArgs, ENTITY_INDEX myEntity, INT iCargo, INT delay = 0, BOOL bDoLookBack = TRUE)	
	iCargo = iCargo
	CLEAR_FOCUS()
	
	// Clear the values for lerping from large to small FOV for bomb drop in the explosion cam
//	fLerpCount = 0.0
	
	IF cargoArgs.iNumReleased-1 = iCargo //OR NOT bDoLookBack
		
		TOGGLE_VISIBILITY(cargoARgs, NULL, FALSE, TRUE)
			
		IF bPlayerAttached
			DETACH_PLAYER_FROM_BOMB(cargoArgs.myPlane)
		ENDIF
		
		SET_ENTITY_COLLISION(cargoArgs.myPlane, TRUE)
		bPlaneCollidable = TRUE
		DESTROY_CAM(cargoArgs.CamExplosion)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ODDJOB_STOP_SOUND(cargoArgs.soundID)
		ODDJOB_STOP_SOUND(testSoundID)
		cargoArgs.iCutGameTimeStart = 0
		bInCam = FALSE
		DEBUG_PRINT("CALLING CLEANUP_EXPLOSION_CAM")
		
		IF bDoLookBack AND NOT UPDATE_COLLISION_TESTS_NOW(collisionArgs, cargoArgs.myPlane)
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			SET_VEHICLE_USED_FOR_PILOT_SCHOOL(cargoArgs.myPlane, FALSE)
			PRINTLN("ENABLING FLIGHT SKILL - 02")
			
			cargoArgs.CamExplosion = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1,1,1>>, <<0,0,0>>, 60.00, FALSE)
			PRINTLN("CREATING EXPLOSION CAM - 05")
				
			ATTACH_CAM_TO_ENTITY(cargoArgs.CamExplosion, cargoArgs.myPlane, <<0,-0.65,-1.60>>, TRUE)
			SHAKE_CAM(cargoArgs.CamExplosion, "HAND_SHAKE", 1.5)
			POINT_CAM_AT_ENTITY(cargoArgs.CamExplosion, myEntity, <<0,0,0>>)
			SET_FOCUS_POS_AND_VEL(GET_ENTITY_COORDS(myEntity), <<0,0,0>>)
			SET_CAM_ACTIVE(cargoArgs.CamExplosion, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			cargoArgs.iCutGameTimeStart = 0
			cargoArgs.iCutGameTime = GET_GAME_TIMER() + delay
		ELSE
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
		ENDIF
	
	ENDIF
ENDPROC

PROC CLEANUP_CAM(PLANE_CARGO_ARGS &cargoArgs, BOOL& bClearPrints)
	IF bClearPrints
		CLEAR_HELP()
		bClearPrints = FALSE
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	SET_VEHICLE_USED_FOR_PILOT_SCHOOL(cargoArgs.myPlane, FALSE)
	PRINTLN("ENABLING FLIGHT SKILL - 03")
	
	IF NOT bStartSceneAgain
		START_AUDIO_SCENE("BAY_DOOR_SCENE")
		PRINTLN("STARTING AUDIO SCENE - BAY_DOOR_SCENE VIA CLEANUP_CAM")
		bStartSceneAgain = TRUE
	ENDIF
	
	TOGGLE_VISIBILITY(cargoArgs, NULL, FALSE, TRUE)
	
	
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	CLEAR_TIMECYCLE_MODIFIER()
	OVERLAY_SET_SECURITY_CAM(FALSE,FALSE)
	DESTROY_CAM(cargoArgs.CamExplosion)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
ENDPROC

PROC CLEANUP_QUICK_CAM(PLANE_CARGO_ARGS &cargoArgs, BOOL& bLocked, INT& iLockedIdx)
	bLocked = FALSE
	iLockedIdx = -1
	cargoArgs.iCutGameTimeStart = 0
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	//cargoArgs.iCutGameTime = GET_GAME_TIMER()
	DESTROY_CAM(cargoArgs.CamExplosion)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	SET_VEHICLE_USED_FOR_PILOT_SCHOOL(cargoArgs.myPlane, FALSE)
	PRINTLN("ENABLING FLIGHT SKILL - 04")
ENDPROC
