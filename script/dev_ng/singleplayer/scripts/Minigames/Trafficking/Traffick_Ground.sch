USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_vehicle.sch"
USING "commands_player.sch" 
USING "finance_control_public.sch"
USING "player_ped_public.sch"
USING "model_enums.sch"
USING "minigames_helpers.sch"
USING "traffic_default_values.sch" // this must be included before traffic.sch 
USING "traffic.sch"
USING "script_oddjob_funcs.sch"
USING "flow_public_game.sch"
USING "flow_public_game.sch"

CONST_INT numValidOffRoadCars 8

FUNC BOOL VALIDATE_OFF_ROAD_CAR(MODEL_NAMES myCar)
	INT idx
	
	MODEL_NAMES validCars[numValidOffRoadCars]
	validCars[0] = BLAZER
	validCars[1] = SADLER
	validCars[2] = MESA
	validCars[3] = DUBSTA
	validCars[4] = BISON
	validCars[5] = DLOADER
	validCars[6] = PCJ
	validCars[6] = DUNE
	
	REPEAT numValidOffRoadCars idx
		IF myCar = validCars[idx]
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC DEBUG_PRINT(STRING message)
	PRINTSTRING(message)
	PRINTNL()
ENDPROC
