USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "script_drawing.sch"
USING "commands_audio.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_vehicle.sch"
USING "commands_player.sch" 
USING "commands_water.sch"
USING "finance_control_public.sch"
USING "player_ped_public.sch"
USING "model_enums.sch"
USING "minigames_helpers.sch"
USING "Drug_Trafficking_core.sch"
USING "Traffick_Cams.sch"
USING "Traffick_UI.sch"
USING "Commands_pad.sch"
USING "chase_hint_cam.sch"
USING "script_oddjob_queues.sch"
USING "flow_public_game.sch"


//remove me
BOOL bLandingReminder = TRUE
BOOL bSkipped = FALSE
BOOL bHitATarget = FALSE
BOOL bPlayConvo = TRUE
BOOL bHitWater = FALSE
BOOL bJSkipped = FALSE
BOOL bFlareHelpPrint = FALSE
BOOL bRemoveHUD = FALSE
BOOL bTriggerUberMusic = FALSE
BOOL bUberReminder = TRUE
BOOL bSwitchedBombs = FALSE

structTimer tBombTimer

//VECTOR direction
REL_GROUP_HASH grpEnemies

//USING "Drug_Trafficking_Data.sch"
VECTOR vPlayerPos//, vTempFocus
AUTHORED_DATA authoredData
AUTHORED_DATE_PROPS authoredDataProps

// Widgets
FLOAT fCameraOffsetX, fCameraOffsetY, fCameraOffsetZ
FLOAT fCameraHeadingX, fCameraHeadingY, fCameraHeadingZ

CONST_FLOAT CARGO_DROP_TIME_RESET 1.7
CONST_FLOAT CARGO_DROP_SKIP_DELAY 1.0

CONST_INT LOCK_ON_BUFFER 550

CONST_INT FAIL_SAFE_TIME 30000 //30 seconds.. for uber

CONST_INT CAM_TIME 2500 //Lookback cam time
CONST_INT CONST_DROP_PRINT 200 //150
CONST_INT CONST_FIRST_PRINT_DIST_TUT 500
CONST_INT CONST_FIRST_PRINT_DIST 500
CONST_INT CONST_HELP_PRINT_DIST 1000
CONST_INT HOLDING_TIME 2400 //when we start the holding pattern
CONST_INT dropTimeout 28000
CONST_FLOAT MAX_SPEED_AUTO 35.0
CONST_FLOAT MIN_SPEED_AUTO 13.0
CONST_FLOAT CONST_TOO_CLOSE_GROUND 23.0
CONST_INT 	LIGHT_ON_HOUR				20
CONST_INT 	LIGHT_OFF_HOUR				6

structPedsForConversation MyLocalPedStruct
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

FLOAT minTime = 0.55
FLOAT maxTime = 0.6


CONST_FLOAT AUTO_Y_ROT 5.0

CONST_FLOAT CONST_LOCK_SOUND_MAX 200.0
			
//INT iLockOnTimeBufferStart
BOOL bTriggerAlarms = TRUE
INT iSirenSoundID = -1
INT iSirenSoundID2 = -1
INT iSirenSoundID3 = -1
INT iClosestTrainCarriage = -1
INT iIntroCutStages = 0
SEQUENCE_INDEX taskSequence

//Todo pass this in instead of being a header global
TUTORIAL_STATE tutorialState = TUTORIAL_STATE_PRE
STRING sTaxiHelp
BOOL bOutOfVehicle = FALSE
BOOL bCloseRange = FALSE
BOOL bDropPhaseComplete = FALSE
//BOOL bStartCutScene = FALSE
//BOOL bInRangeForPerfect = FALSE
BOOL bSwitchedTasks[MAX_NUMBER_DROPS]
BOOL bDisableHolding = FALSE
BOOL bClearPrints = FALSE
BOOL bGenericPrint = FALSE
BOOL bDistWarned = FALSE
BOOL bInActiveFight = FALSE
BOOL bCollisionDetected = FALSE
BOOL bPassed = FALSE
BOOL bEarlyFail = FALSE
BOOL bPrintDropZoneHelp = FALSE
BOOL bTriggerInAirMusic = FALSE
BOOL bRanLastConvo = FALSE
BOOL bKilledBuyer = FALSE
BOOL bClearedLandingHelp = FALSE
BOOL bCreateUberBomb[6]
BOOL bReplaying = FALSE
BOOL bPlaneTooLow = FALSE

// FLASH BOOLS
BOOL bIntroFlash = FALSE
BOOL bDoneFlash01 = FALSE
BOOL bDoneFlash02 = FALSE

//BOOL bCollisionImminent

//BOOL bCut = TRUE

//BOOL bPlayingLockDist = FALSE
//BOOL bPlayingLooping = FALSE

BOOL bUberActive = FALSE
BOOL bDoUberCam = FALSE
BOOL bUberSucess = FALSE
//BOOL bOneHit = FALSE
INT iUberSuccess = -1

//BOOL bWithinPrintRange = FALSE
//BOOL bToldAboutTargetCam[MAX_NUMBER_DROPS] 
//BOOL bToldAboutCargoDrop[MAX_NUMBER_DROPS] 
PLANE_PRINTS dropInstructState = PLANE_PRINTS_CAM

BOOL bTaskedPedToFlee[MAX_NUMBER_DROPS]

//Define reason for variables to be in struct vs global to the fial
BOOL bLocked = FALSE
INT iLockedIdx = -1
// For Abandon Check
FLOAT fGreatestDistanceToObjective, fAbandonDistance, fWarnDistance, fDistanceFromStart
BOOL bSetDistance = FALSE
CONST_FLOAT fAbandonAddOn 1400.0
CONST_FLOAT fWarnAddOn 800.0
//hack..
structTimer clusterTimer
structTimer tutorialTimer
//structTimer stLoadSceneTimer
structTimer tLastConvoTimer
//structTimer tEndingTimer
INT count = 0

CLUSTER_STATE clusterState = CLUSTER_STATE_INIT

//VECTOR vCargoOffset = <<-0.1776, 0.3160, -0.325>> //-0.2125>>
//VECTOR vCargoOffset2 = <<0.2445, 0.2969, -0.325>> //-0.1761>>

VECTOR vCargoOffset = <<-0.1776, 0.17, -0.325>> //-0.2125>>
VECTOR vCargoOffset2 = <<0.2445, 0.17, -0.325>> //-0.1761>>

VECTOR vReturn
VECTOR vNewEndingBlip = <<2022.3451, 4756.5522, 40.0605>>

//VECTOR vCarGenPlane = <<1470.90088, 3203.77368, 39.51754>>
//VECTOR vCarGenTruck = <<1775.92261, 3235.84937, 41.31895>>
	
VECTOR vDummyBombPostion

FLOAT LeftX, LeftY //, RightX, RightY
VECTOR vBombMoveOffset

// For Bomb Stationary Staging
VEHICLE_INDEX vehStationaryCars[MAX_NUMBER_DROPS][MAX_EXTRA_CARS]
PED_INDEX pedStationaryPeds[MAX_NUMBER_DROPS][MAX_NUMBER_DROPS]
OBJECT_INDEX oGazebo[MAX_NUMBER_DROPS]
CAMERA_INDEX camIntro
//INT iClosestDrop

PED_INDEX pedPilot[2]

FLOAT fMinGroundZ

MODEL_NAMES mnBombProp01
MODEL_NAMES mnChuteName = P_CARGO_CHUTE_S

VECTOR vChuteOffset = <<0,0,0.18>>
INT iFrameCounter
INT iFrameCounter01

VECTOR vSpawnPosition = <<2136.1331, 4780.5635, 39.9702>>
FLOAT fSpawnHeading = 25.29

VECTOR vBombVelocity

INT iFrameCounterCam = 5
FLOAT fBombHeading

// Debug
#IF IS_DEBUG_BUILD
	BOOL pSkipping = FALSE
#ENDIF

PROC RESET_BOOLS(BOOL &bTemp[])
	INT idx
	REPEAT MAX_NUMBER_DROPS idx
		bTemp[idx] = FALSE
	ENDREPEAT
ENDPROC


FUNC BOOL SHOULD_CHECK_CARGO(PLANE_CARGO_ARGS& cargoArgs, INT idx)
	IF IS_ENTITY_DEAD(cargoArgs.oCargoDrop[idx])
		RETURN FALSE
	ELIF IS_ENTITY_ATTACHED(cargoArgs.oCargoDrop[idx]) OR cargoArgs.bCargoComplete[idx]
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC REMOVE_DISTANT_CONVOY_VEHICLES(PLANE_CARGO_ARGS& cargoArgs)
	INT idx
	VECTOR vGroupOneLeader
	VECTOR vGroupTwoLeader
	VECTOR vGroupThreeLeader
	VECTOR vTempPos, vTempPos01, vTempPos02
	
	IF iFrameCounter = 0
		IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[0])
			vGroupOneLeader = GET_ENTITY_COORDS(cargoArgs.vehFinal[0])
		ELSE
			vGroupOneLeader = <<0,0,0>>
		ENDIF
		IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[4])
			vGroupTwoLeader = GET_ENTITY_COORDS(cargoArgs.vehFinal[4])
		ELSE
			vGroupTwoLeader = <<0,0,0>>
		ENDIF
		IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[8])
			vGroupThreeLeader = GET_ENTITY_COORDS(cargoArgs.vehFinal[8])
		ELSE
			vGroupThreeLeader = <<0,0,0>>
		ENDIF
		
		FOR idx = 1 TO 3 
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx])
				vTempPos = GET_ENTITY_COORDS(cargoArgs.vehFinal[idx])
				IF NOT ARE_VECTORS_EQUAL(vGroupOneLeader, <<0,0,0>>)
					IF VDIST(vTempPos, vGroupOneLeader) > 150.0
						PRINTLN("DISTANCE HAS BEEN EXCEEDED: ", idx)
						IF IS_ENTITY_OCCLUDED(cargoArgs.vehFinal[idx])
							DELETE_VEHICLE(cargoArgs.vehFinal[idx])
							PRINTLN("EXPLODING VEHICLE: ", idx)
							IF DOES_BLIP_EXIST(cargoArgs.blipFinal[idx])
								REMOVE_BLIP(cargoArgs.blipFinal[idx])
								PRINTLN("REMOVING BLIP: ", idx)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		FOR idx = 5 TO 7 
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx])
				vTempPos01 = GET_ENTITY_COORDS(cargoArgs.vehFinal[idx])
				IF NOT ARE_VECTORS_EQUAL(vGroupTwoLeader, <<0,0,0>>)
					IF VDIST(vTempPos01, vGroupTwoLeader) > 150.0
						PRINTLN("DISTANCE HAS BEEN EXCEEDED: ", idx)
						IF IS_ENTITY_OCCLUDED(cargoArgs.vehFinal[idx])
							DELETE_VEHICLE(cargoArgs.vehFinal[idx])
							PRINTLN("EXPLODING VEHICLE: ", idx)
							IF DOES_BLIP_EXIST(cargoArgs.blipFinal[idx])
								REMOVE_BLIP(cargoArgs.blipFinal[idx])
								PRINTLN("REMOVING BLIP: ", idx)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		FOR idx = 9 TO 11 
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx])
				vTempPos02 = GET_ENTITY_COORDS(cargoArgs.vehFinal[idx])
				IF NOT ARE_VECTORS_EQUAL(vGroupThreeLeader, <<0,0,0>>)
					IF VDIST(vTempPos02, vGroupThreeLeader) > 150.0
						PRINTLN("DISTANCE HAS BEEN EXCEEDED: ", idx)
						IF IS_ENTITY_OCCLUDED(cargoArgs.vehFinal[idx])
							DELETE_VEHICLE(cargoArgs.vehFinal[idx])
							PRINTLN("EXPLODING VEHICLE: ", idx)
							IF DOES_BLIP_EXIST(cargoArgs.blipFinal[idx])
								REMOVE_BLIP(cargoArgs.blipFinal[idx])
								PRINTLN("REMOVING BLIP: ", idx)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF iFrameCounter > 10
		iFrameCounter = 0
	ELSE
		iFrameCounter++
	ENDIF
ENDPROC

PROC INIT_PROPER_PLANE_CARGO(ARGS& myArgs)
	
	IF myArgs.bDoBomb
		mnBombProp01 = PROP_LD_BOMB_ANIM
	ELSE
		mnBombProp01 = Prop_Drop_ArmsCrate_01b
	ENDIF
	
	IF myArgs.bDoUberBombs
		mnBombProp01 = PROP_LD_BOMB_ANIM
	ENDIF
	
ENDPROC

//important... funcitons that take basic types when possible
PROC RELEASE_CARGO(OBJECT_INDEX& myEntity, ENTITY_INDEX myPlane, INT& soundID, BOOL bPlaySound = TRUE, FLOAT fScale = -10.0)
	fLerpCount = 0.0
	bSwitchedBombs = FALSE
	DEBUG_MESSAGE("Detaching entity")
	DETACH_ENTITY(myEntity, FALSE)
	
	VECTOR front, side, up, pos
	
	IF bPlaySound
		IF NOT IS_ENTITY_DEAD(myPlane)
			ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BOMB_DROP_MASTER", soundID, FALSE, myPlane)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(myPlane)
		vBombVelocity = GET_ENTITY_VELOCITY(myPlane)
		GET_ENTITY_MATRIX(myPlane, front, side, up, pos)
	ENDIF

	up = NORMALISE_VECTOR(up)
	vBombVelocity = vBombVelocity + <<up.x* fScale, up.y* fScale, up.z * fScale>>
	IF vBombVelocity.z <= -70
		DEBUG_MESSAGE("overriding z to -70")
		vBombVelocity.z = -70
	ENDIF
			
	vOrigOrientation = <<80, 0, GET_ENTITY_HEADING(myPlane)+180>> //GET_ENTITY_ROTATION(myEntity)

	SET_ENTITY_MAX_SPEED(myEntity, 90)
	SET_ENTITY_VELOCITY(myEntity, <<vBombVelocity.x, vBombVelocity.y, vBombVelocity.z>>)
	PRINTLN("vBombVelocity = ", vBombVelocity)

ENDPROC

PROC UPDATE_VISIBLILITY_AND_COLLISION(OBJECT_INDEX& myBomb, PLANE_CARGO_ARGS &cargoArgs)
	IF DOES_ENTITY_EXIST(myBomb)
		SET_ENTITY_VISIBLE(myBomb, TRUE) //todo.. why the hell do we have to call this
		SET_ENTITY_COLLISION(myBomb, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(cargoArgs.oBombDummy)
		SET_ENTITY_VISIBLE(cargoArgs.oBombDummy, FALSE)
		SET_ENTITY_COLLISION(cargoArgs.oBombDummy,FALSE)
	ENDIF
	
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
	
	// Cannot set collision on a ped if that ped is in a vehicle
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
	ENDIF
ENDPROC

// Called once original bomb orientation is reset. This ensures that the velocity remains
// unchanged. It is a pared down version of RELEASE_CARGO logic
PROC RESET_BOMB_VELOCITY(OBJECT_INDEX& myEntity, ENTITY_INDEX myPlane, FLOAT fScale = -5.0)

	FLOAT iMaxVeloc = -60
	
	//If we're higher up we can have it go faster.
	IF GET_ENTITY_HEIGHT_ABOVE_GROUND(myPlane) > 150
		fScale = -10.0
		iMaxVeloc = -70
	ENDIF
	//Calculate distance from ground
 
	VECTOR front, side, up, pos
		
	IF NOT IS_ENTITY_DEAD(myPlane)
		vBombVelocity = GET_ENTITY_VELOCITY(myPlane)
		GET_ENTITY_MATRIX(myPlane, front, side, up, pos)
	ENDIF

	up = NORMALISE_VECTOR(up)
	vBombVelocity = vBombVelocity + <<up.x* fScale, up.y* fScale, up.z * fScale>>
	
	IF vBombVelocity.z <= iMaxVeloc
		DEBUG_MESSAGE("RESET_BOMB_VELOCITY: overriding z to -70")
		vBombVelocity.z = iMaxVeloc
	ENDIF
	
	vOrigOrientation = <<80, 0, GET_ENTITY_HEADING(myPlane)+180>>
	
	IF DOES_ENTITY_HAVE_PHYSICS(myEntity)
		SET_ENTITY_MAX_SPEED(myEntity, 90)
	ENDIF
	SET_ENTITY_VELOCITY(myEntity, <<vBombVelocity.x, vBombVelocity.y, vBombVelocity.z>>)
ENDPROC

PROC SIMULATE_NOSE_TIP_DOWN(OBJECT_INDEX& myBomb, PLANE_CARGO_ARGS &cargoArgs)
	VECTOR vCurRot//, vCurRotDummy
	vCurRot = GET_ENTITY_ROTATION(myBomb)
	
	//PRINTLN("SIMULATE_NOSE_TIP_DOWN: The current bomb rotation is ", GET_STRING_FROM_VECTOR(vCurRot))
	IF NOT bSwitchedBombs
		IF vCurRot.x < EXPLOSION_CAM_BOMB_PITCH
			vCurRot.x += 4.0
			IF vCurRot.x > EXPLOSION_CAM_BOMB_PITCH
				vCurRot.x = EXPLOSION_CAM_BOMB_PITCH
			ENDIF
			SET_ENTITY_ROTATION(myBomb, vCurRot)
		ENDIF
		
		IF DOES_ENTITY_EXIST(cargoArgs.oBombDummy)
			SET_ENTITY_VISIBLE(cargoArgs.oBombDummy, FALSE)
		ENDIF
		
		IF IS_STREAMVOL_ACTIVE()
			IF STREAMVOL_IS_VALID(cargoArgs.streamVolID)
				IF NOT DOES_ENTITY_EXIST(cargoArgs.oBombDummy)
					cargoArgs.oBombDummy = CREATE_OBJECT(Prop_LD_Bomb_01, GET_ENTITY_COORDS(myBomb))
					SET_ENTITY_COLLISION(cargoArgs.oBombDummy, FALSE)
				ELSE
					SET_ENTITY_COORDS(cargoArgs.oBombDummy, GET_ENTITY_COORDS(myBomb))
					SET_ENTITY_ROTATION(cargoArgs.oBombDummy, << EXPLOSION_CAM_BOMB_PITCH, vCurRot.y, vCurRot.z>>)
					SET_ENTITY_VELOCITY(cargoArgs.oBombDummy, GET_ENTITY_VELOCITY(myBomb))
				ENDIF
				
				IF TIMER_DO_ONCE_WHEN_READY(tBombTimer, 0.50)
					fBombHeading = GET_ENTITY_HEADING(cargoArgs.myPlane) - 180
					IF fBombHeading < 0
						fBombHeading += 360.0
					ENDIF
					
					cargoArgs.iCutGameTimeStart = GET_GAME_TIMER()
					DO_EXPLOSION_CAM(cargoArgs, myBomb)
					bSwitchedBombs = TRUE

					UPDATE_VISIBLILITY_AND_COLLISION(myBomb, cargoArgs)
					
					RESET_BOMB_VELOCITY(cargoArgs.oBombDummy, cargoArgs.myPlane)	
					RESET_BOMB_VELOCITY(myBomb, cargoArgs.myPlane)	
					
					vCurRot.x = EXPLOSION_CAM_BOMB_PITCH
					SET_ENTITY_ROTATION(myBomb, vCurRot)
				ENDIF
			ENDIF
		ENDIF
		
		vCurRot.x = EXPLOSION_CAM_BOMB_PITCH
		IF  DOES_ENTITY_EXIST(cargoArgs.oBombDummy)
			SET_ENTITY_COORDS(cargoArgs.oBombDummy, GET_ENTITY_COORDS(myBomb))
			SET_ENTITY_ROTATION(cargoArgs.oBombDummy, vCurRot)
		ENDIF
	ELSE
		IF DOES_CAM_EXIST(cargoArgs.CamExplosion)
			IF IS_CAM_ACTIVE(cargoArgs.CamExplosion)
				IF fBlurStrength <= 0.20
					fBlurStrength += 0.01
					//SET_CAM_MOTION_BLUR_STRENGTH(cargoArgs.CamExplosion, fBlurStrength)
				ENDIF
			ENDIF
		ENDIF
	
		vCurRot.x = EXPLOSION_CAM_BOMB_PITCH
		vCurRot.y += GET_RANDOM_FLOAT_IN_RANGE(1.5.3.0)
		
		IF vCurRot.y > 360.0
			vCurRot.y -= 360.0
		ENDIF
		
		vBombVelocity = GET_ENTITY_VELOCITY(myBomb)
		
		IF vBombVelocity.z > -70
			vBombVelocity.z -= 0.25
			SET_ENTITY_VELOCITY(myBomb, vBombVelocity)
			SET_ENTITY_VELOCITY(cargoArgs.oBombDummy, vBombVelocity)
		ENDIF
		
		vOrigOrientation = vOrigOrientation
		
		SET_ENTITY_ROTATION(myBomb, vCurRot)
		
		IF DOES_ENTITY_EXIST(cargoArgs.oBombDummy)
			SET_ENTITY_COORDS(cargoArgs.oBombDummy, GET_ENTITY_COORDS(myBomb))
		ENDIF
	ENDIF
ENDPROC

PROC SIMULATE_NOSE_TIP_DOWN_UBER(OBJECT_INDEX& myBomb)

	VECTOR vCurRot//, vCurRotDummy
	vCurRot = GET_ENTITY_ROTATION(myBomb)
	
	FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE(1,2)
	
	IF bSwitchedBombs
		vCurRot.x = 80.0
	ELSE
		IF vCurRot.x < 80.0
			vCurRot.x += fRand
		ENDIF
	ENDIF
	
	fRand = GET_RANDOM_FLOAT_IN_RANGE(0.4,1.0)

	vCurRot.y += fRand
	IF vCurRot.y > 360.0
		vCurRot.y -= 360.0
	ENDIF
	
	SET_ENTITY_ROTATION(myBomb, vCurRot)
//	PRINTLN("SIMULATE_NOSE_TIP_DOWN UBER: The rotation of bomb is ", GET_STRING_FROM_VECTOR(GET_ENTITY_ROTATION(myBomb)))
ENDPROC

//Function used to apply a chute to the prop and animate it.  Because the chute shows up for a half a second fully deployed we have to hack this to be invisible for a few frames
//INT& hideTime to track when we should make it visible
//BOOL& vVisible to track if it's visible or not
//OBJECT_INDEX myBox - The the box we're going to attache the chute too
//OBJECT_INDEX& myChute - The chute we're going to create and pass back by reference
PROC APPLY_CHUTE(OBJECT_INDEX& myChute, OBJECT_INDEX myBox, INT& hideTime, BOOL& bVisible)
	VECTOR temp
	temp = GET_ENTITY_COORDS(myBox)
	PRINTLN("APPLY_CHUTE..new")
	myChute = CREATE_OBJECT(mnChuteName, temp)
	SET_ENTITY_VISIBLE(myChute, FALSE)
	hideTime= GET_GAME_TIMER()
	bVisible = FALSE
	ATTACH_ENTITY_TO_ENTITY(myChute, myBox, 0, vChuteOffset, <<0,0,0>>)
	//Moved this to the script that turns on visibility
	//PLAY_ENTITY_ANIM(myChute, "p_cargo_chute_s_deploy", "p_cargo_chute_s", 1.0, FALSE, TRUE)
ENDPROC

//This function is now mainly just hardcoded since we only have one variation and no longer need to keep things data driven / madular
PROC CREATE_FINAL_STAGING(ARGS& myArgs, PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& myData)
	INT idx, idx2
//	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vCarGenPlane - <<7,7,7>>, vCarGenPlane + <<7,7,7>>, FALSE)
//	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vCarGenTruck - <<7,7,7>>, vCarGenTruck + <<7,7,7>>, FALSE)

	REPEAT MAX_NUMBER_FINAL_TARGETS idx
		cargoArgs.vehFinal[idx] = CREATE_VEHICLE(myArgs.mnFinalTargets[idx], myData.vRunwayTargets[idx], myData.fRunwayTargets[idx])
		SET_VEHICLE_ON_GROUND_PROPERLY(cargoArgs.vehFinal[idx])
		//cargoArgs.blipFinal[idx] = ADD_BLIP_FOR_ENTITY(cargoArgs.vehFinal[idx])
		SET_ENTITY_LOD_DIST(cargoArgs.vehFinal[idx], 1000)
		SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehFinal[idx], TRUE)
	ENDREPEAT
	
	cargoArgs.vehDropoff[0] = cargoArgs.vehFinal[0]
	cargoArgs.vehDropoff[1] = cargoArgs.vehFinal[3]
	cargoArgs.vehDropoff[2] = cargoArgs.vehFinal[6]
	
	cargoArgs.vehFinalExtra[0] =  CREATE_VEHICLE(myArgs.mnFinalTargets[0], << 1730.0139, 3314.9282, 41.7539 >>, 0)
	SET_ENTITY_QUATERNION(cargoArgs.vehFinalExtra[0], -0.0017, 0.0226, 0.9922, -0.1222)
	pedPilot[0] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinalExtra[0], PEDTYPE_CIVMALE, myData.targetCarDriverNames[0])
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPilot[0], TRUE)
	cargoArgs.recordingStatus[0] = RECORD_STATUS_INIT
	
	cargoArgs.vehFinalExtra[1] =  CREATE_VEHICLE(myArgs.mnFinalTargets[4], << 1774.5613, 3235.6404, 41.9390 >>, 0)
	SET_ENTITY_QUATERNION(cargoArgs.vehFinalExtra[1], -0.0128, -0.0072, 0.0939, 0.9955)		
	cargoArgs.recordingStatus[1] = RECORD_STATUS_INIT
	
	cargoArgs.vehFinalExtra[2] =  CREATE_VEHICLE(myArgs.mnFinalTargets[4], << 1646.4313, 3258.0586, 40.4998 >>, 0)
	SET_ENTITY_QUATERNION(cargoArgs.vehFinalExtra[2], 0.0267, -0.0274, 0.8044, 0.5929)		
	cargoArgs.recordingStatus[2] = RECORD_STATUS_INIT
	
	cargoArgs.vehFinalExtra[3] =  CREATE_VEHICLE(myArgs.mnFinalTargets[1], <<1349.9443, 3155.6465, 41.0453>>, 0)
	SET_ENTITY_QUATERNION(cargoArgs.vehFinalExtra[3], 0.0024, 0.0030, 0.7994, 0.6008)	
	pedPilot[1] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinalExtra[3], PEDTYPE_CIVMALE, myData.targetCarDriverNames[0])
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPilot[1], TRUE)
	cargoArgs.recordingStatus[3] = RECORD_STATUS_INIT
	
	cargoArgs.vehFinalExtra[4] =  CREATE_VEHICLE(myArgs.mnFinalTargets[4], << 1120.4285, 3016.5684, 40.1836 >>, 0)
	SET_ENTITY_QUATERNION(cargoArgs.vehFinalExtra[4], 0.0011, -0.0010, -0.5982, 0.8013)		
	cargoArgs.recordingStatus[4] = RECORD_STATUS_INIT
	
	cargoArgs.vehFinalExtra[5] =  CREATE_VEHICLE(myArgs.mnFinalTargets[4], << 1468.7660, 3013.5898, 40.3801 >>, 0)
	SET_ENTITY_QUATERNION(cargoArgs.vehFinalExtra[5], -0.0091, -0.0121, -0.3688, 0.9294)		
	cargoArgs.recordingStatus[5] = RECORD_STATUS_INIT

	pedStationaryPeds[0][0] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1587.4852, 3225.5764, 39.4216 >>)
	pedStationaryPeds[0][1] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[1], << 1521.7310, 3206.7068, 39.4141 >>)
	pedStationaryPeds[0][2] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1707.4034, 3258.0220, 40.0469 >>)
	pedStationaryPeds[0][3] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[1], << 1716.1447, 3265.4775, 40.1539 >>)
	
	pedStationaryPeds[1][0] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1513.6698, 3109.8762, 39.5341 >>)
	pedStationaryPeds[1][1] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1514.7271, 3097.7983, 39.5341 >>)
	pedStationaryPeds[1][2] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1589.6923, 3206.3936, 39.4834 >>)
	pedStationaryPeds[1][3] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1573.4342, 3201.3796, 39.4925 >>)
	
	pedStationaryPeds[2][0] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1773.3756, 3245.5645, 41.1507 >>)
	pedStationaryPeds[2][1] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1796.5787, 3246.5784, 41.6352 >>)
	pedStationaryPeds[2][2] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1460.6106, 3047.9028, 39.5341 >>)
	pedStationaryPeds[2][3] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1408.7350, 3005.2290, 39.5435 >>)
	
	pedStationaryPeds[3][0] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1658.3489, 3256.0186, 39.5491 >>)
	pedStationaryPeds[3][1] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1619.2214, 3243.9282, 39.5161 >>)
	pedStationaryPeds[3][2] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1700.0881, 3287.2615, 47.9222 >>)
	pedStationaryPeds[3][3] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << 1785.4822, 3293.3582, 40.7863 >>)
	
	ADD_RELATIONSHIP_GROUP("ENEMIES", grpEnemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, grpEnemies, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, grpEnemies)
	
	//two cars by stunt planes
	//Player Position = << 1525.3549, 3107.5337, 39.5341 >>, 128.4660   room: NONE
	//Player Position = << 1519.9740, 3112.0833, 39.5341 >>, 131.7277   room: NONE
	
	REPEAT 4 idx
		REPEAT 4 idx2
			IF idx = 1 OR idx = 3
				GIVE_WEAPON_TO_PED(pedStationaryPeds[idx][idx2], WEAPONTYPE_ASSAULTRIFLE, 100, TRUE)
				
				//SET_CURRENT_PED_WEAPON(pedStationaryPeds[idx][idx2], WEAPONTYPE_ASSAULTRIFLE, TRUE)
			ELSE
				GIVE_WEAPON_TO_PED(pedStationaryPeds[idx][idx2], WEAPONTYPE_ASSAULTRIFLE, 100, TRUE)
				//SET_CURRENT_PED_WEAPON(pedStationaryPeds[idx][idx2], WEAPONTYPE_ASSAULTRIFLE, TRUE)
			ENDIF
			
			SET_PED_TO_INFORM_RESPECTED_FRIENDS(pedStationaryPeds[idx][idx2], 300, 10)
			SET_PED_SEEING_RANGE(pedStationaryPeds[idx][idx2], 500.0)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedStationaryPeds[idx][idx2], grpEnemies)
			SET_ENTITY_LOD_DIST(pedStationaryPeds[idx][idx2], 500)
			SET_PED_ACCURACY(pedStationaryPeds[idx][idx2], 5)
			TASK_COMBAT_PED(pedStationaryPeds[idx][idx2], PLAYER_PED_ID())
		ENDREPEAT
	ENDREPEAT

ENDPROC

PROC CREATE_FINAL_STAGING_REPEATABLE(ARGS& myArgs, PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& myData)
	INT idx, idx2

	REPEAT MAX_NUMBER_FINAL_TARGETS idx
		cargoArgs.vehFinal[idx] = CREATE_VEHICLE(myArgs.mnFinalTargets[idx], myData.vRunwayTargets[idx], myData.fRunwayTargets[idx])
		//cargoArgs.blipFinal[idx] = ADD_BLIP_FOR_ENTITY(cargoArgs.vehFinal[idx])
		SET_ENTITY_LOD_DIST(cargoArgs.vehFinal[idx], 500)
		SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehFinal[idx], TRUE)
	ENDREPEAT
	
	cargoArgs.vehDropoff[0] = cargoArgs.vehFinal[0]
	cargoArgs.vehDropoff[1] = cargoArgs.vehFinal[4]
	cargoArgs.vehDropoff[2] = cargoArgs.vehFinal[8]
	
	// Group 1
	pedStationaryPeds[0][0] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[0], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
	IF NOT IS_PED_INJURED(pedStationaryPeds[0][0])
		TASK_VEHICLE_DRIVE_WANDER(pedStationaryPeds[0][0], cargoArgs.vehFinal[0], 8.0, DRIVINGMODE_STOPFORCARS)
	ENDIF
	pedStationaryPeds[0][1] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[1], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
	IF NOT IS_PED_INJURED(pedStationaryPeds[0][1])
		TASK_VEHICLE_MISSION(pedStationaryPeds[0][1], cargoArgs.vehFinal[1], cargoArgs.vehFinal[0], MISSION_ESCORT_REAR, 12.0, DRIVINGMODE_AVOIDCARS, -1, -1)
	ENDIF
	pedStationaryPeds[0][2] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[2], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
	IF NOT IS_PED_INJURED(pedStationaryPeds[0][2])
		TASK_VEHICLE_MISSION(pedStationaryPeds[0][2], cargoArgs.vehFinal[2], cargoArgs.vehFinal[1], MISSION_ESCORT_REAR, 12.0, DRIVINGMODE_AVOIDCARS, -1, -1)
	ENDIF
	pedStationaryPeds[0][3] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[3], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
	IF NOT IS_PED_INJURED(pedStationaryPeds[0][3])
		TASK_VEHICLE_MISSION(pedStationaryPeds[0][3], cargoArgs.vehFinal[3], cargoArgs.vehFinal[2], MISSION_ESCORT_REAR, 12.0, DRIVINGMODE_AVOIDCARS, -1, -1)
	ENDIF
	
	// Group 2
	pedStationaryPeds[1][0] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[4], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
	IF NOT IS_PED_INJURED(pedStationaryPeds[1][0])
		TASK_VEHICLE_DRIVE_WANDER(pedStationaryPeds[1][0], cargoArgs.vehFinal[4], 8.0, DRIVINGMODE_STOPFORCARS)
	ENDIF
	pedStationaryPeds[1][1] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[5], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
	IF NOT IS_PED_INJURED(pedStationaryPeds[1][1])
		TASK_VEHICLE_MISSION(pedStationaryPeds[1][1], cargoArgs.vehFinal[5], cargoArgs.vehFinal[4], MISSION_ESCORT_REAR, 12.0, DRIVINGMODE_AVOIDCARS, -1, -1)
	ENDIF
	pedStationaryPeds[1][2] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[6], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
	IF NOT IS_PED_INJURED(pedStationaryPeds[1][2])
		TASK_VEHICLE_MISSION(pedStationaryPeds[1][2], cargoArgs.vehFinal[6], cargoArgs.vehFinal[5], MISSION_ESCORT_REAR, 12.0, DRIVINGMODE_AVOIDCARS, -1, -1)
	ENDIF
	pedStationaryPeds[1][3] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[7], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
	IF NOT IS_PED_INJURED(pedStationaryPeds[1][3])
		TASK_VEHICLE_MISSION(pedStationaryPeds[1][3], cargoArgs.vehFinal[7], cargoArgs.vehFinal[6], MISSION_ESCORT_REAR, 12.0, DRIVINGMODE_AVOIDCARS, -1, -1)
	ENDIF
	
	// Group 3
	pedStationaryPeds[2][0] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[8], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
	IF NOT IS_PED_INJURED(pedStationaryPeds[2][0])
		TASK_VEHICLE_DRIVE_WANDER(pedStationaryPeds[2][0], cargoArgs.vehFinal[8], 8.0, DRIVINGMODE_STOPFORCARS)
	ENDIF
//	pedStationaryPeds[2][1] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[9], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
//	IF NOT IS_PED_INJURED(pedStationaryPeds[2][1])
//		TASK_VEHICLE_MISSION(pedStationaryPeds[2][1], cargoArgs.vehFinal[9], cargoArgs.vehFinal[8], MISSION_ESCORT_REAR, 12.0, DRIVINGMODE_AVOIDCARS, -1, -1)
//	ENDIF
//	pedStationaryPeds[2][2] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[10], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
//	IF NOT IS_PED_INJURED(pedStationaryPeds[2][2])
//		TASK_VEHICLE_MISSION(pedStationaryPeds[2][2], cargoArgs.vehFinal[10], cargoArgs.vehFinal[9], MISSION_ESCORT_REAR, 12.0, DRIVINGMODE_AVOIDCARS, -1, -1)
//	ENDIF
//	pedStationaryPeds[2][3] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinal[11], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
//	IF NOT IS_PED_INJURED(pedStationaryPeds[2][3])
//		TASK_VEHICLE_MISSION(pedStationaryPeds[2][3], cargoArgs.vehFinal[11], cargoArgs.vehFinal[10], MISSION_ESCORT_REAR, 12.0, DRIVINGMODE_AVOIDCARS, -1, -1)
//	ENDIF
	
	ADD_RELATIONSHIP_GROUP("ENEMIES", grpEnemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, grpEnemies, RELGROUPHASH_PLAYER)
	
	//two cars by stunt planes
	//Player Position = << 1525.3549, 3107.5337, 39.5341 >>, 128.4660   room: NONE
	//Player Position = << 1519.9740, 3112.0833, 39.5341 >>, 131.7277   room: NONE
	
	REPEAT 3 idx
		REPEAT 4 idx2
			GIVE_WEAPON_TO_PED(pedStationaryPeds[idx][idx2], WEAPONTYPE_SMG, 100, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedStationaryPeds[idx][idx2], TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedStationaryPeds[idx][idx2], grpEnemies)
			SET_ENTITY_LOD_DIST(pedStationaryPeds[idx][idx2], 500)
		ENDREPEAT
	ENDREPEAT

ENDPROC

PROC REMOVE_DROPOFF_STAGING(ARGS& myArgs, PLANE_DATA& myData, PLANE_CARGO_ARGS& cargoArgs)
	INT idx, idx2
	
	REPEAT myData.iNumDrops idx
		IF DOES_ENTITY_EXIST(cargoArgs.pedDropoff[idx])
			DELETE_PED(cargoArgs.pedDropoff[idx])
			PRINTLN("DELETING PED - cargoArgs.pedDropoff INDEX: ", idx)
		ENDIF
		IF DOES_ENTITY_EXIST(cargoArgs.vehDropoff[idx])
			DELETE_VEHICLE(cargoArgs.vehDropoff[idx])
			PRINTLN("DELETING VEHICLE - cargoArgs.vehDropoff INDEX: ", idx)
		ENDIF
		
		REPEAT 3 idx2
			IF DOES_ENTITY_EXIST(vehStationaryCars[idx][idx2])
				DELETE_VEHICLE(vehStationaryCars[idx][idx2])
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	REPEAT MAX_NUMBER_FINAL_TARGETS idx
		IF DOES_ENTITY_EXIST(cargoArgs.vehFinal[idx])
			DELETE_VEHICLE(cargoArgs.vehFinal[idx])
			PRINTLN("DELETING VEHICLE - cargoArgs.vehFinal INDEX: ", idx)
		ENDIF
	ENDREPEAT
	
	IF myArgs.bDoStationary
		REPEAT MAX_NUMBER_STAGED_PROPS idx2
			IF DOES_ENTITY_EXIST(cargoArgs.oStagingProps[idx2])
				DELETE_OBJECT(cargoArgs.oStagingProps[idx2])
				PRINTLN("DELETING OBJECT - cargoArgs.oStagingProps INDEX: ", idx2)
			ENDIF	
		ENDREPEAT
	ENDIF
	
	IF DOES_ENTITY_EXIST(cargoArgs.vehFinalExtra[0])
		DELETE_VEHICLE(cargoArgs.vehFinalExtra[0])
		PRINTLN("DELETING VEHICLE - cargoArgs.vehFinalExtra[0]")
	ENDIF
	IF DOES_ENTITY_EXIST(cargoArgs.vehFinalExtra[1])
		DELETE_VEHICLE(cargoArgs.vehFinalExtra[1])
		PRINTLN("DELETING VEHICLE - cargoArgs.vehFinalExtra[1]")
	ENDIF
	IF DOES_ENTITY_EXIST(cargoArgs.vehFinalExtra[2])
		DELETE_VEHICLE(cargoArgs.vehFinalExtra[2])
		PRINTLN("DELETING VEHICLE - cargoArgs.vehFinalExtra[2]")
	ENDIF
	IF DOES_ENTITY_EXIST(cargoArgs.vehFinalExtra[3])
		DELETE_VEHICLE(cargoArgs.vehFinalExtra[3])
		PRINTLN("DELETING VEHICLE - cargoArgs.vehFinalExtra[3]")
	ENDIF
	
	REPEAT 3 idx
		REPEAT 4 idx2
			IF DOES_ENTITY_EXIST(pedStationaryPeds[idx][idx2])
				DELETE_PED(pedStationaryPeds[idx][idx2])
				PRINTLN("DELETING STATIONARY PED INDEX: ", idx, " INDEX 2: ", idx2)
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_LIGHTS_BE_ON()
	IF (GET_CLOCK_HOURS() >= LIGHT_ON_HOUR) OR (GET_CLOCK_HOURS() < LIGHT_OFF_HOUR)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CREATE_DROPOFF_STAGING(ARGS& myArgs, PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& myData, AIR_VARIATION myVariation)

	INT idx, idx2
	myVariation = myVariation
	UNUSED_PARAMETER(myArgs)
	//TODO write custom setupt for the bomb drop
	//IF myVariation <> AIR_VARIATION_BOMB
		REPEAT myData.iNumDrops idx	
			VECTOR vTemp
		
			//TODO create a interested drug scene instead of just one car
			IF ARE_VECTORS_EQUAL(myData.dropData[idx].vDrugPickup, <<0,0,0>>)
				vTemp = myData.dropData[idx].vCenterPos
			ELSE
				vTemp = myData.dropData[idx].vDrugPickup
			ENDIF
			
//			OBJECT_INDEX temp
			
			authoredData = GET_AUTHORED_LOCATION_DATA(myArgs.dropLocations[idx], vTemp)
			//
			// Ceate Props
			//
			IF myArgs.bDoStationary
				REPEAT MAX_NUMBER_STAGED_PROPS idx2
					IF NOT IS_VECTOR_ZERO(authoredData.vPropPos[idx2])
						// Crates
						cargoArgs.oStagingProps[idx2] = CREATE_OBJECT(authoredDataProps.mnPropName[idx2], authoredData.vPropPos[idx2])
						SET_ENTITY_HEADING(cargoArgs.oStagingProps[idx2], authoredData.fPropOri[idx2])
						SET_ENTITY_LOD_DIST(cargoArgs.oStagingProps[idx2], 1000)
					ENDIF
				ENDREPEAT
			ENDIF
			
			//
			// Create peds
			//
			cargoArgs.pedDropoff[idx] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[idx], authoredData.vPedPos[0], authoredData.fPedOri[0])
			// Play animations on guy at drop location.
			IF NOT IS_ENTITY_DEAD(cargoArgs.pedDropoff[idx]) AND NOT IS_PED_INJURED(cargoArgs.pedDropoff[idx])
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				GIVE_WEAPON_TO_PED(cargoArgs.pedDropoff[idx], WEAPONTYPE_ASSAULTRIFLE, 100, TRUE)
				TASK_LOOK_AT_ENTITY(cargoArgs.pedDropoff[idx], PLAYER_PED_ID(), -1)
				
				IF myArgs.bDoBomb
					SET_PED_RELATIONSHIP_GROUP_HASH(cargoArgs.pedDropoff[idx], cargoArgs.relRivalRunners)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(cargoArgs.pedDropoff[idx], TRUE)
					SET_PED_ACCURACY(cargoArgs.pedDropoff[idx], 0)
					SET_COMBAT_FLOAT(cargoArgs.pedDropoff[idx], CCF_MAX_SHOOTING_DISTANCE, 200.0)
					TASK_SHOOT_AT_ENTITY(cargoArgs.pedDropoff[idx], PLAYER_PED_ID(), -1, FIRING_TYPE_1_BURST)
				ENDIF
			ENDIF
			
			IF myArgs.bDoStationary
				count = 4 //GET_RANDOM_INT_IN_RANGE(1,4)
				REPEAT count idx2
					IF myData.targetCarDriverNames[idx2] != DUMMY_MODEL_FOR_SCRIPT
						pedStationaryPeds[idx][idx2] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[idx2], authoredData.vPedPos[idx2+1], authoredData.fPedOri[idx2+1])
						IF DOES_ENTITY_EXIST(pedStationaryPeds[idx][idx2])
							GIVE_WEAPON_TO_PED(pedStationaryPeds[idx][idx2], WEAPONTYPE_ASSAULTRIFLE, 100, TRUE)
							SET_PED_RELATIONSHIP_GROUP_HASH(pedStationaryPeds[idx][idx2], cargoArgs.relRivalRunners)
							SET_CURRENT_PED_WEAPON(pedStationaryPeds[idx][idx2], WEAPONTYPE_ASSAULTRIFLE, TRUE)
							SET_PED_SPHERE_DEFENSIVE_AREA(pedStationaryPeds[idx][idx2], authoredData.vPedPos[idx2], 20.0)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedStationaryPeds[idx][idx2], TRUE)
							SET_PED_ACCURACY(pedStationaryPeds[idx][idx2], 0)
							SET_COMBAT_FLOAT(pedStationaryPeds[idx][idx2], CCF_MAX_SHOOTING_DISTANCE, 200.0)
							TASK_SHOOT_AT_ENTITY(pedStationaryPeds[idx][idx2], PLAYER_PED_ID(), -1, FIRING_TYPE_1_BURST)
							PRINTLN("TASKING PED TO SHOOT AT ENTITY WITH INDEX: ", idx, " NUMBER: ", idx2)
						ENDIF
					ENDIF
				ENDREPEAT
				
				// Gazebo or large prop
				IF NOT IS_VECTOR_ZERO(authoredData.vGazeboPos)
					oGazebo[idx] = CREATE_OBJECT(authoredDataProps.mnGazeboProp[idx], authoredData.vGazeboPos)
					IF DOES_ENTITY_EXIST(oGazebo[idx])
						SET_ENTITY_LOD_DIST(oGazebo[idx], 1000)
					ENDIF
				ENDIF
			ENDIF
			
			//
			// Create vehicles
			//
			IF myArgs.bDoStationary
				cargoArgs.vehDropoff[idx] = CREATE_VEHICLE(myData.stationaryCarNames[idx],  authoredData.vCarPos[0], authoredData.fCarOri[0])
				SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehDropoff[idx], TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(cargoArgs.vehDropoff[idx])
				SET_ENTITY_LOD_DIST(cargoArgs.vehDropoff[idx], 1000)
				
				IF SHOULD_LIGHTS_BE_ON()
					SET_VEHICLE_LIGHTS(cargoArgs.vehDropoff[idx], FORCE_VEHICLE_LIGHTS_ON)
					PRINTLN("TURNING LIGHTS ON VEHICLE")
				ENDIF
//				FREEZE_ENTITY_POSITION(cargoArgs.vehDropoff[idx], TRUE)
				
				PRINTLN("STATIONARY - CREATING VEHICLE DROPOFF: ", idx, " AT LOCATION: ", authoredData.vCarPos[0])
//				SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehDropoff[idx], TRUE)
				
				// Additional vehicles
				count = 3 //GET_RANDOM_INT_IN_RANGE(1,4)
				REPEAT count idx2
					IF NOT IS_VECTOR_ZERO(authoredData.vCarPos[idx2+1]) 
						PRINTLN("DropLocation: ", idx, " Car: ", idx2, " Posiiton: ", authoredData.vCarPos[idx2+1])
						vehStationaryCars[idx][idx2] = CREATE_VEHICLE(myData.stationaryCarNames[idx2], authoredData.vCarPos[idx2+1], authoredData.fCarOri[idx2+1])
						SET_VEHICLE_ON_GROUND_PROPERLY(vehStationaryCars[idx][idx2])
						SET_ENTITY_LOD_DIST(vehStationaryCars[idx][idx2], 1000)
//						FREEZE_ENTITY_POSITION(vehStationaryCars[idx][idx2], TRUE)

						IF SHOULD_LIGHTS_BE_ON()
							SET_VEHICLE_LIGHTS(vehStationaryCars[idx][idx2], FORCE_VEHICLE_LIGHTS_ON)
							PRINTLN("TURNING LIGHTS ON VEHICLE")
						ENDIF
						
						IF authoredData.fCarOri[idx2+1] = 0
							SET_ENTITY_HEADING(vehStationaryCars[idx][idx2], GET_RANDOM_FLOAT_IN_RANGE(0, 360))
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				cargoArgs.vehDropoff[idx] = CREATE_VEHICLE(myData.targetCarNames[idx], authoredData.vCarPos[0])
				PRINTLN("CREATING VEHICLE DROPOFF: ", idx, " AT LOCATION: ", authoredData.vCarPos[0])
				SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehDropoff[idx], TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(cargoArgs.vehDropoff[idx])
				PRINTLN("AFTER: CREATING VEHICLE DROPOFF: ", idx, " AT LOCATION: ", authoredData.vCarPos[0])
			ENDIF
			
			IF cargoArgs.PTFXflare[idx] = NULL
				DEBUG_MESSAGE("Particle effect is null!!")
			ENDIF
			
		ENDREPEAT
	//ENDIF

ENDPROC

PROC CREATE_FLARES_AT_DROP_SITES(PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData, ARGS& myArgs)
	INT idx	

	REPEAT myData.iNumDrops idx
		VECTOR vTemp, vColour
		
		IF ARE_VECTORS_EQUAL(myData.dropData[idx].vDrugPickup, <<0,0,0>>)
			vTemp = myData.dropData[idx].vCenterPos
		ELSE
			vTemp = myData.dropData[idx].vDrugPickup
		ENDIF
		
		IF NOT myArgs.bDoTrain AND NOT myArgs.bDoUberBombs
			IF NOT myArgs.bDoBomb
				vColour = <<1.0, 0.84, 0.0>>
			ELSE
				vColour = <<0.8, 0.18, 0.19>>
			ENDIF
			
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(cargoArgs.PTFXflare[idx])
				cargoArgs.PTFXflare[idx] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_drug_traffic_flare_L", vTemp, <<0,0,0>>)
				SET_PARTICLE_FX_LOOPED_COLOUR(cargoArgs.PTFXflare[idx], vColour.x, vColour.y, vColour.z)
				PRINTLN("NEW: STARTING FLARE PARTICLE EFFECT")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CREATE_DROPOFF_STAGING_TRAIN(ARGS& myArgs, PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& myData, AIR_VARIATION myVariation)
	myVariation = myVariation
	
	INT idx, idx2
	VECTOR vSmallBoatOffset =  <<0, -3.0, 0.8>>
	
	ADD_RELATIONSHIP_GROUP("ENEMIES", grpEnemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, grpEnemies, RELGROUPHASH_PLAYER)
	
	cargoArgs.vehDropoff[0] = CREATE_MISSION_TRAIN(6, myArgs.genVectors[0], myArgs.bGenBools[0])
	IF NOT IS_ENTITY_DEAD(cargoArgs.vehDropoff[0])
		SET_ENTITY_LOD_DIST(cargoArgs.vehDropoff[0], 1000)
		SET_TRAIN_CRUISE_SPEED(cargoArgs.vehDropoff[0], 18.0)
		SET_TRAIN_SPEED(cargoArgs.vehDropoff[0], 18.0)
	ENDIF
	
	REPEAT 4 idx
		SET_ENTITY_LOD_DIST(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[0], idx), 1000)
	ENDREPEAT
	
	// Yacht
	cargoArgs.vehDropoff[1] = CREATE_VEHICLE(myData.boatNames[0], << -2020.55, 4735.80, 0.39 >>, 134.50)
	SET_ENTITY_LOD_DIST(cargoArgs.vehDropoff[1], 1000)
	pedStationaryPeds[0][0] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehDropoff[1], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
	pedStationaryPeds[0][1] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0], << -1985.6525, 4769.1455, -3.6671 >>)
	ATTACH_ENTITY_TO_ENTITY(pedStationaryPeds[0][1], cargoArgs.vehDropoff[1], 0, <<-0.16, -4.56, 1.45>>, <<0, 0, 90.72>>, TRUE, TRUE)
	TASK_SHOOT_AT_ENTITY(pedStationaryPeds[0][1], PLAYER_PED_ID(), -1, FIRING_TYPE_DEFAULT)
	//SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehDropoff[1], TRUE)
	
	// Small Boat
	cargoArgs.vehFinalExtra[0] = CREATE_VEHICLE(myData.boatNames[1], << -2029.95, 4744.09, 0.39 >>, 77.31)
	SET_ENTITY_LOD_DIST(cargoArgs.vehFinalExtra[0], 1000)
	pedStationaryPeds[0][2] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinalExtra[0], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[1])
	pedStationaryPeds[0][3] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[1], << -1976.4246, 4748.5498, -2.7524 >>)
	ATTACH_ENTITY_TO_ENTITY(pedStationaryPeds[0][3], cargoArgs.vehFinalExtra[0], 0, vSmallBoatOffset, <<0, 0, 90.72>>, TRUE, TRUE)
	TASK_SHOOT_AT_ENTITY(pedStationaryPeds[0][3], PLAYER_PED_ID(), -1, FIRING_TYPE_DEFAULT)
	//SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehFinalExtra[0], TRUE)
	
	// Small Boat
	cargoArgs.vehFinalExtra[1] = CREATE_VEHICLE(myData.boatNames[1], << -2028.16, 4739.94, 0.39 >>, 89.54)
	SET_ENTITY_LOD_DIST(cargoArgs.vehFinalExtra[1], 1000)
	pedStationaryPeds[1][0] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinalExtra[1], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[0])
	pedStationaryPeds[1][1] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[1], << -1968.9366, 4758.7271, -2.8807 >>)
	ATTACH_ENTITY_TO_ENTITY(pedStationaryPeds[1][1], cargoArgs.vehFinalExtra[1], 0, vSmallBoatOffset, <<0, 0, 90.72>>, TRUE, TRUE)
	TASK_SHOOT_AT_ENTITY(pedStationaryPeds[1][1], PLAYER_PED_ID(), -1, FIRING_TYPE_DEFAULT)
	//SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehFinalExtra[1], TRUE)
	
	// Yacht
	cargoArgs.vehFinalExtra[2] = CREATE_VEHICLE(myData.boatNames[0], << -2035.27, 4735.40, -0.16 >>, 103.55)
	SET_ENTITY_LOD_DIST(cargoArgs.vehFinalExtra[2], 1000)
	pedStationaryPeds[1][2] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinalExtra[2], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[1])
	pedStationaryPeds[1][3] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[1], << -1986.3005, 4768.8428, -3.6552 >>)
	ATTACH_ENTITY_TO_ENTITY(pedStationaryPeds[1][3], cargoArgs.vehFinalExtra[2], 0, <<-0.16, -4.56, 1.45>>, <<0, 0, 90.72>>, TRUE, TRUE)
	TASK_SHOOT_AT_ENTITY(pedStationaryPeds[1][3], PLAYER_PED_ID(), -1, FIRING_TYPE_DEFAULT)
	//SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehFinalExtra[2], TRUE)
	
	// Small Boat
	cargoArgs.vehFinalExtra[3] = CREATE_VEHICLE(myData.boatNames[1], << -2030.35, 4731.77, 0.39 >>, 127.40)
	SET_ENTITY_LOD_DIST(cargoArgs.vehFinalExtra[3], 1000)
	pedStationaryPeds[2][0] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehFinalExtra[3], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[1])
	pedStationaryPeds[2][1] = CREATE_PED(PEDTYPE_CRIMINAL, myData.targetCarDriverNames[1], << -1975.3752, 4766.6040, -3.4422 >>)
	ATTACH_ENTITY_TO_ENTITY(pedStationaryPeds[2][1], cargoArgs.vehFinalExtra[3], 0, vSmallBoatOffset, <<0, 0, 90.72>>, TRUE, TRUE)
	TASK_SHOOT_AT_ENTITY(pedStationaryPeds[2][1], PLAYER_PED_ID(), -1, FIRING_TYPE_DEFAULT)
	//SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehFinalExtra[3], TRUE)
	
	// Can't seemt o keep collision loaded, and boats are sinking underground...
	cargoArgs.bTraf4_FreezeBoats = TRUE
	
	REPEAT 3 idx
		REPEAT 4 idx2
			IF DOES_ENTITY_EXIST(pedStationaryPeds[idx][idx2])
				GIVE_WEAPON_TO_PED(pedStationaryPeds[idx][idx2], WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedStationaryPeds[idx][idx2], TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedStationaryPeds[idx][idx2], grpEnemies)
				SET_ENTITY_LOD_DIST(pedStationaryPeds[idx][idx2], 1000)
				SET_PED_ACCURACY(pedStationaryPeds[idx][idx2], 0)
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	SET_RANDOM_TRAINS(FALSE) 
ENDPROC

PROC STOP_ALL_PARTICLES(PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& myData)
	DEBUG_MESSAGE("STOP_ALL_PARTICLES")
	INT idx
	REPEAT myData.iNumDrops idx
//		IF cargoArgs.PTFXflare[idx] <> NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST(cargoArgs.PTFXflare[idx])
			DEBUG_MESSAGE("Stopping a particle effect")
			STOP_PARTICLE_FX_LOOPED(cargoArgs.PTFXflare[idx])
//			cargoArgs.PTFXflare[idx] = NULL
		ENDIF
	ENDREPEAT
ENDPROC

PROC HANDLE_RECORDING_START(VEHICLE_INDEX myVehicle, STRING recordingName, INT num, VECTOR checkLoc, RECORD_STATUS& myStatus, INT iTriggerDistance = 260, BOOL bExplodeEarly = FALSE)
	FLOAT fTimePosition
	
	SWITCH  myStatus
		CASE RECORD_STATUS_INIT
			IF NOT IS_ENTITY_DEAD(myVehicle)
				IF GET_DISTANCE_BETWEEN_COORDS(checkLoc, vPlayerPos, FALSE) < iTriggerDistance
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(myVehicle)
						START_PLAYBACK_RECORDED_VEHICLE(myVehicle, num, recordingName)
						SET_VEHICLE_ENGINE_ON(myVehicle, TRUE, TRUE)
						myStatus = RECORD_STATUS_PLAYING
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE RECORD_STATUS_PLAYING
			IF NOT IS_ENTITY_DEAD(myVehicle)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(myVehicle)
					EXPLODE_VEHICLE(myVehicle)
					
					SET_VEHICLE_AS_NO_LONGER_NEEDED(myVehicle)
					PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED - 1")
					
					myStatus = RECORD_STATUS_END
				ELSE
					IF bExplodeEarly
						fTimePosition = GET_TIME_POSITION_IN_RECORDING(myVehicle)
//						PRINTLN("fTimePosition = ", fTimePosition)

						IF fTimePosition > 35500
							EXPLODE_VEHICLE(myVehicle)
							PRINTLN("EXPLODING PLANE EARLY")
							
							SET_VEHICLE_AS_NO_LONGER_NEEDED(myVehicle)
							PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED - 2")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED(myVehicle)
				PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED - 3")
				myStatus = RECORD_STATUS_END
			ENDIF
		BREAK
		CASE RECORD_STATUS_END
		
		BREAK
	ENDSWITCH


ENDPROC

FUNC INT GET_NUMBER_OF_REMAINING_CARGO(INT& iTotalCargo, INT& iNumLanded)
	RETURN (iTotalCargo - iNumLanded)
ENDFUNC

FUNC BOOL ARE_DROPS_COMPLETE(PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData)
	
	INT idx
	BOOL bAllDropsComplete = TRUE
	
	REPEAT myData.iNumDrops idx
		IF NOT cargoArgs.bCargoDropComplete[idx]
			bAllDropsComplete = FALSE
		ENDIF
	ENDREPEAT
	
	IF bAllDropsComplete
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC BOOL HAS_PLAYER_PASSED(ARGS& myArgs, PLANE_CARGO_ARGS &cargoArgs)
	//Used to have a "money to pass" system.. myArgs.iMoneyToPass
	INT fMoneyEarned = cargoArgs.iNumSuccessfulDrops * REWARD_MEDIUM 
	
	IF fMoneyEarned >= myArgs.iMoneyToPass
		cargoArgs.bPassed = TRUE
		RETURN TRUE
	ELSE
		cargoArgs.bPassed = FALSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC STRING GET_FINAL_DIALOUGE_STRING(ARGS& myArgs, PLANE_DATA& myData, PLANE_CARGO_ARGS &cargoArgs, BOOL bSucess)

	STRING returnString
	INT iRandConvo
	
	IF myArgs.bDoBomb
		//If we're an overall fail
		
		IF NOT HAS_PLAYER_PASSED(myArgs, cargoArgs)
			//If we're an overall fail
			returnString = "ARMS_AIR_BF"
		ELSE
			IF NOT bSucess
				returnString = "ARMS_A_LBFB" //Oscar telling you your last bomb drop was a FAIL, you're out of bombs, and you now need to land
			ELSE
				IF GET_NUMBER_OF_REMAINING_CARGO(myData.iNumTotalCargo, cargoArgs.iNumLanded) = 0
					returnString = "ARMS_A_LBPB" //Oscar telling you your last bomb drop was a sucess, you're out of bombs, and you now need to land
					iRandConvo = GET_RANDOM_INT_IN_RANGE() % 4
					IF iRandConvo = 0
						returnString = "ARMS_A_LBPB" //Oscar telling you your last bomb drop was a sucess, you're out of targets, and you now need to land LBPT = "Land, bomb, pass, targets"
					ELIF iRandConvo = 1
						returnString = "ARMS_A_LBPB2"
					ELIF iRandConvo = 2
						returnString = "ARMS_A_LBPB3"
					ELIF iRandConvo = 3
						returnString = "ARMS_A_LBPB4"
					ENDIF
				ELSE
					iRandConvo = GET_RANDOM_INT_IN_RANGE() % 4
					IF iRandConvo = 0
						returnString = "ARMS_A_LPPT" //Oscar telling you your last bomb drop was a sucess, you're out of targets, and you now need to land LBPT = "Land, bomb, pass, targets"
					ELIF iRandConvo = 1
						returnString = "ARMS_A_LPPT2"
					ELIF iRandConvo = 2
						returnString = "ARMS_A_LPPT3"
					ELIF iRandConvo = 3
						returnString = "ARMS_A_LPPT4"
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ELSE
		
		//ARMS_AIR_PF Oscar telling you that you've failed to complete enough drops with pacakges
		IF NOT HAS_PLAYER_PASSED(myArgs, cargoArgs)
			//If we're an overall fail
			returnString = "ARMS_AIR_PF"
		ELSE
			IF NOT bSucess
				returnString = "ARMS_A_LPFP" //Oscar telling you your last package drop was a FAIL, you're out of packages, and you now need to land.
			ELSE
				IF GET_NUMBER_OF_REMAINING_CARGO(myData.iNumTotalCargo, cargoArgs.iNumLanded) = 0
					iRandConvo = GET_RANDOM_INT_IN_RANGE() % 3
					IF iRandConvo = 0
						returnString = "ARMS_A_LPPP" // Oscar telling you your last package drop was a sucess, you're out of packages, and you now need to land.	
					ELIF iRandConvo = 1
						returnString = "ARMS_A_LPPP2"
					ELIF iRandConvo = 2
						returnString = "ARMS_A_LPPP3"
					ELIF iRandConvo = 3
						returnString = "ARMS_A_LPPP4"
					ENDIF
				ELSE
					iRandConvo = GET_RANDOM_INT_IN_RANGE() % 4
					IF iRandConvo = 0
						returnString = "ARMS_A_LBPT" // Oscar telling you your last package drop was a sucess, you're out of targerts, and you now need to land.
					ELIF iRandConvo = 1
						returnString = "ARMS_A_LBPT2"
					ELIF iRandConvo = 2
						returnString = "ARMS_A_LBPT3"
					ELIF iRandConvo = 3
						returnString = "ARMS_A_LBPT4"
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN returnString
ENDFUNC

PROC HANDLE_MUSIC_ON_SUCCESSFUL_DROPS(PLANE_CARGO_ARGS &cargoArgs, BOOL& bDoBomb, ARGS& myArgs)
	
	IF g_savedGlobals.sTraffickingData.iAirRank >= 5
		IF bDoBomb
			IF myArgs.bDoTrain
				TRIGGER_MUSIC_EVENT("OJDA4_TRAIN_HIT")
				PRINTLN("REPEATABLE TRAIN: HANDLE_MUSIC_ON_SUCCESSFUL_DROPS MUSIC - OJDA4_TRAIN_HIT")
				
				PRINTLN("REPEATABLE TRAIN: HANDLE_MUSIC_ON_SUCCESSFUL_DROPS: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
			ELSE
				TRIGGER_MUSIC_EVENT("OJDA3_BOMB_HIT")
				PRINTLN("REPEATABLE BOMB: HANDLE_MUSIC_ON_SUCCESSFUL_DROPS - OJDA3_BOMB_HIT")
				
				PRINTLN("REPEATABLE BOMB: HANDLE_MUSIC_ON_SUCCESSFUL_DROPS: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
			ENDIF
		ELSE
			TRIGGER_MUSIC_EVENT("OJDA2_1ST_DROPPED")
			PRINTLN("REPEATABLE DROP: HANDLE_MUSIC_ON_SUCCESSFUL_DROPS - OJDA2_1ST_DROPPED")
			
			PRINTLN("REPEATABLE DROP: HANDLE_MUSIC_ON_SUCCESSFUL_DROPS: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		ENDIF
	ENDIF
	
	SWITCH g_savedGlobals.sTraffickingData.iAirRank
		CASE 0
			IF cargoArgs.iNumSuccessfulDrops = 0
				TRIGGER_MUSIC_EVENT("OJDA1_1ST_DROPPED")
				PRINTLN("TRIGGERING MUSIC - OJDA1_1ST_DROPPED")
			ELIF cargoArgs.iNumSuccessfulDrops = 1
				TRIGGER_MUSIC_EVENT("OJDA1_2ND_DROPPED")
				PRINTLN("TRIGGERING MUSIC - OJDA1_2ND_DROPPED")
			ENDIF
			
			PRINTLN("g_savedGlobals.sTraffickingData.iAirRank = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
		//---------------------------------------------------------------------------------------------------------------------------
		CASE 1
			TRIGGER_MUSIC_EVENT("OJDA2_1ST_DROPPED")
			PRINTLN("TRIGGERING MUSIC - OJDA2_1ST_DROPPED")
		BREAK
		//---------------------------------------------------------------------------------------------------------------------------
		CASE 2
			TRIGGER_MUSIC_EVENT("OJDA3_BOMB_HIT")
			PRINTLN("TRIGGERING MUSIC - OJDA3_BOMB_HIT")
		BREAK
		//---------------------------------------------------------------------------------------------------------------------------
		CASE 3
			TRIGGER_MUSIC_EVENT("OJDA4_TRAIN_HIT")
			PRINTLN("TRIGGERING MUSIC - OJDA4_TRAIN_HIT")
			
			IF cargoArgs.iNumSuccessfulDrops = 0
				TRIGGER_MUSIC_EVENT("OJDA4_1_LEFT")
				PRINTLN("TRIGGERING MUSIC - OJDA4_1_LEFT")
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------------------
		CASE 4
		
		BREAK
	ENDSWITCH
	
ENDPROC

PROC HANDLE_DROP_SUCESS(PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData, AIR_VARIATION myVariation, 
ARGS& myArgs, INT iCargo, INT iDrop, VECTOR& vCurDestObjective[], BOOL& bDoBomb, BOOL bUsedBomb = TRUE)

	bUsedBomb = bUsedBomb
	BOOL bDone = FALSE
	myVariation = myVariation
	STRING tempLine
	INT iRandConvo
	
	//on every successful drop we can do another warning.
	bDistWarned = FALSE
	
	HANDLE_MUSIC_ON_SUCCESSFUL_DROPS(cargoArgs, bDoBomb, myArgs)
	
	cargoArgs.iNumSuccessfulDrops++
	cargoArgs.bCargoSuccessful[iCargo] = TRUE
	PRINTLN("CARGO SUCCESSFUL = ", iCargo)
	cargoArgs.bDropSuccessful[iDrop] = TRUE
	PRINTLN("DROP SUCCESSFUL = ", iDrop)
	cargoArgs.bCargoDropComplete[iDrop] = TRUE
	PRINTLN("DROP COMPLETE = ", iDrop)
	cargoArgs.bCargoComplete[iCargo] = TRUE
	PRINTLN("CARGO COMPLETE = ", iCargo)
	
	cargoArgs.bCompletedOneDrop = TRUE
	
	DEBUG_MESSAGE("cargoArgs.bCompletedOneDrop = TRUE")
	PRINTNL()

//	IF cargoArgs.fActualMaxHeight < myData.fmaxHeight
//		DEBUG_MESSAGE("setting height bonus to true..")
//		cargoArgs.bCargoHeightBonus[iDrop] = TRUE
//	ELSE
//		DEBUG_MESSAGE("setting height bonus to false..")
//		cargoArgs.bCargoHeightBonus[iDrop] = FALSE
//	ENDIF
	
	cargoArgs.fActualMaxHeight = 0
			
	IF cargoArgs.PTFXflare[iDrop] <> NULL
		STOP_PARTICLE_FX_LOOPED(cargoArgs.PTFXflare[iDrop])
		cargoArgs.PTFXflare[iDrop] = NULL
	ENDIF
	
	//If we're done we want to play specific dialouge
	IF GET_NUMBER_OF_REMAINING_CARGO(myData.iNumTotalCargo, cargoArgs.iNumLanded) = 0
	OR ARE_DROPS_COMPLETE(cargoArgs, myData)
		bDone = TRUE
		CLEAR_HELP()
		bRemoveHUD = TRUE
		ODDJOB_STOP_SOUND(iSirenSoundID)
		ODDJOB_STOP_SOUND(iSirenSoundID2)
		ODDJOB_STOP_SOUND(iSirenSoundID3)
		STOP_SCRIPTED_CONVERSATION(FALSE)
		
		IF IS_AUDIO_SCENE_ACTIVE("SIREN_SCENE")
			STOP_AUDIO_SCENE("SIREN_SCENE")
			PRINTLN("STOPPING AUDIO SCENE - SIREN_SCENE VIA HANDLE_DROP_SUCESS")
		ENDIF
		
		tempLine = GET_FINAL_DIALOUGE_STRING(myArgs, myData, cargoArgs, TRUE)
	ENDIF
	
	vCurDestObjective[iDrop] = <<0,0,0>>
	
	IF myArgs.bDoBomb
		IF NOT myArgs.bDoUberBombs
			CLEANUP_EXPLOSION_CAM(cargoArgs, cargoARgs.oCargoDrop[iCargo], iCargo, 0, NOT bCollisionDetected)
		ENDIF
		DEBUG_MESSAGE("Handle_Drop_sucess clear prints")
		CLEAR_PRINTS()
		
		IF bDone
			//final line is extra import.. add to queued system if conversation is currently going
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				REGISTER_QUICK_CONV_QUEUE(MyLocalPedStruct, "ARMSAUD", tempLine)
				//Need to do our own conversation queue
			ELSE
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", tempLine, CONV_PRIORITY_VERY_HIGH)
			ENDIF
		ELSE
			IF myArgs.bDoUberBombs
				iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
				IF iRandConvo = 0
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SMB", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("UBER BOMB SUCCESS: iRandConvo = ", iRandConvo)
				ELIF iRandConvo = 1
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SM2", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("UBER BOMB SUCCESS: iRandConvo = ", iRandConvo)
				ELIF iRandConvo = 2
					IF cargoArgs.iNumSuccessfulDrops = 2
						CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SM3", CONV_PRIORITY_VERY_HIGH)
						PRINTLN("SPECIAL CASE: UBER BOMB SUCCESS: iRandConvo = ", iRandConvo)
					ELSE
						CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SM4", CONV_PRIORITY_VERY_HIGH)
						PRINTLN("SPECIAL CASE: UBER BOMB SUCCESS: iRandConvo = ", iRandConvo)
					ENDIF
				ELIF iRandConvo = 3
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SM4", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("UBER BOMB SUCCESS: iRandConvo = ", iRandConvo)
				ELIF iRandConvo = 4
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SM5", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("UBER BOMB SUCCESS: iRandConvo = ", iRandConvo)
				ELIF iRandConvo = 5
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SM6", CONV_PRIORITY_VERY_HIGH)
					PRINTLN("UBER BOMB SUCCESS: iRandConvo = ", iRandConvo)
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					REGISTER_QUICK_CONV_QUEUE(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SB", TRUE)
				ELSE
					iRandConvo = GET_RANDOM_INT_IN_RANGE() % 3
					IF iRandConvo = 0
						PRINTLN("YEEHAH - PACKAGE SUCCESS: iRandConvo = ", iRandConvo)
						CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "OSCAR_YEEHA", CONV_PRIORITY_VERY_HIGH)
					ELSE
						iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
						IF iRandConvo = 0
							PRINTLN("BOMB SUCCESS: iRandConvo = ", iRandConvo)
							CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SB", CONV_PRIORITY_VERY_HIGH)
						ELIF iRandConvo = 1
							PRINTLN("BOMB SUCCESS: iRandConvo = ", iRandConvo)
							CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SB2", CONV_PRIORITY_VERY_HIGH)
						ELIF iRandConvo = 2
							PRINTLN("BOMB SUCCESS: iRandConvo = ", iRandConvo)
							CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SB3", CONV_PRIORITY_VERY_HIGH)
						ELIF iRandConvo = 3
							PRINTLN("BOMB SUCCESS: iRandConvo = ", iRandConvo)
							CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SB4", CONV_PRIORITY_VERY_HIGH)
						ELIF iRandConvo = 4
							PRINTLN("BOMB SUCCESS: iRandConvo = ", iRandConvo)
							CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SB5", CONV_PRIORITY_VERY_HIGH)
						ELIF iRandConvo = 5
							PRINTLN("BOMB SUCCESS: iRandConvo = ", iRandConvo)
							CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_SB6", CONV_PRIORITY_VERY_HIGH)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF bUsedBomb
				IF myArgs.bDoUberBombs
					REGISTER_QUICK_DELAY("DTRFAIR_PASSC")
				ELSE
					REGISTER_QUICK_DELAY("DTRFAIR_PASSB")
				ENDIF
			ENDIF
		ENDIF
	ELSE

		DEBUG_MESSAGE("Handle_Drop_sucess clear prints")
		CLEAR_PRINTS()
		
		PLAY_SOUND_FRONTEND(-1, "CONFIRM_BEEP", "HUD_MINI_GAME_SOUNDSET")
		PRINTLN("PLAYING BEEP SOUND FROM FRONTEND")

		IF bDone
			//final line is extra import.. add to queued system if conversation is currently going
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				REGISTER_QUICK_CONV_QUEUE(MyLocalPedStruct, "ARMSAUD", tempLine)
				//Need to do our own conversation queue
			ELSE
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", tempLine, CONV_PRIORITY_VERY_HIGH)
			ENDIF
		ELSE
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				REGISTER_QUICK_CONV_QUEUE(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_S", TRUE)
			ELSE
				iRandConvo = GET_RANDOM_INT_IN_RANGE() % 3
				IF iRandConvo = 0
					PRINTLN("YEEHAH - PACKAGE SUCCESS: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "OSCAR_YEEHA", CONV_PRIORITY_VERY_HIGH)
				ELSE
					iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
					IF iRandConvo = 0
						PRINTLN("PACKAGE SUCCESS: iRandConvo = ", iRandConvo)
						CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_S", CONV_PRIORITY_VERY_HIGH)
					ELIF iRandConvo = 1
						PRINTLN("PACKAGE SUCCESS: iRandConvo = ", iRandConvo)
						CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_S2", CONV_PRIORITY_VERY_HIGH)
					ELIF iRandConvo = 2
						PRINTLN("PACKAGE SUCCESS: iRandConvo = ", iRandConvo)
						CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_S3", CONV_PRIORITY_VERY_HIGH)
					ELIF iRandConvo = 3
						PRINTLN("PACKAGE SUCCESS: iRandConvo = ", iRandConvo)
						CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_S4", CONV_PRIORITY_VERY_HIGH)
					ELIF iRandConvo = 4
						PRINTLN("PACKAGE SUCCESS: iRandConvo = ", iRandConvo)
						CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_S5", CONV_PRIORITY_VERY_HIGH)
					ELIF iRandConvo = 5
						PRINTLN("PACKAGE SUCCESS: iRandConvo = ", iRandConvo)
						CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_S6", CONV_PRIORITY_VERY_HIGH)
					ENDIF
				ENDIF
			ENDIF
			IF bUsedBomb
				REGISTER_QUICK_DELAY("DTRFAIR_PASS")
			ENDIF
		ENDIF

	ENDIF
	
	//DISPLAY_PACKAGE_PRINT(myARgs, cargoArgs, GET_NUMBER_OF_REMAINING_CARGO(myData.iNumTotalCargo, cargoArgs.iNumLanded), TRUE)
	
ENDPROC

PROC HANDLE_DROP_FAIL(PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData, AIR_VARIATION myVariation, ARGS& myArgs, INT iCargo)
	BOOL bDone = FALSE
	STRING tempLine
	INT iRandConvo
	
	IF cargoArgs.bCargoHitGround[iCargo] AND NOT cargoArgs.bCargoComplete[iCargo] AND NOT cargoArgs.bCargoSuccessful[iCargo]
		//If we're done we want to play specific dialouge
		IF GET_NUMBER_OF_REMAINING_CARGO(myData.iNumTotalCargo, cargoArgs.iNumLanded) = 0
		OR ARE_DROPS_COMPLETE(cargoArgs, myData)
			bDone = TRUE
			STOP_SCRIPTED_CONVERSATION(FALSE)
			tempLine = GET_FINAL_DIALOUGE_STRING(myArgs, myData, cargoArgs, FALSE)
		ENDIF
		
		IF myVariation = AIR_VARIATION_BOMB AND cargoArgs.iCutGameTimeStart <> 0
			CLEANUP_EXPLOSION_CAM(cargoArgs, cargoARgs.oCargoDrop[iCargo], iCargo, 0, NOT bCollisionDetected)
		ENDIF
	
		IF myARgs.bDoLockOn
			CLEANUP_QUICK_CAM(cargoArgs, bLocked, iLockedIdx)
		ENDIF
		
		//cargoArgs.iNumFailedDrops ++
		cargoArgs.bCargoFailed[iCargo] = TRUE
		cargoArgs.bCargoComplete[iCargo] = TRUE
		
		SCRIPT_REMOVE_BLIP_SAFE(cargoArgs.blipCrateDrop[iCargo])
		
		DEBUG_MESSAGE("HANDLE_DROP_FAIL")
		CLEAR_PRINTS()

		IF bDone
			//final line is extra import.. add to queued system if conversation is currently going
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				REGISTER_QUICK_CONV_QUEUE(MyLocalPedStruct, "ARMSAUD", tempLine)
				//Need to do our own conversation queue
			ELSE
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", tempLine, CONV_PRIORITY_VERY_HIGH)
			ENDIF
		ELSE
			IF myArgs.bDoUberBombs AND bHitATarget
				PRINTLN("bHitATarget SHOULD BE TRUE")
				
				IF bUberReminder
					PRINT_HELP("DTRFAIR_HELPL")
					bUberReminder = FALSE
				ENDIF
				
				iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
				IF iRandConvo = 0
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FMB", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_AIR_FMB, RANDOM: ", iRandConvo)
				ELIF iRandConvo = 1
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FM2", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_AIR_FM2, RANDOM: ", iRandConvo)
				ELIF iRandConvo = 2
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FM3", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_AIR_FM3, RANDOM: ", iRandConvo)
				ELIF iRandConvo = 3
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FM4", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_AIR_FM4, RANDOM: ", iRandConvo)
				ELIF iRandConvo = 4
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FM5", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_AIR_FM5, RANDOM: ", iRandConvo)
				ELIF iRandConvo = 5
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FM6", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_AIR_FM6, RANDOM: ", iRandConvo)
				ENDIF
			ELIF myArgs.bDoUberBombs AND NOT bHitATarget
				
				PRINTLN("bHitATarget SHOULD BE FALSE - NO TARGETS HIT")
				
				iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
				IF iRandConvo = 0
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_FMBN", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_A_FMBN, RANDOM: ", iRandConvo)
				ELIF iRandConvo = 1
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_FMB2", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_A_FMB2, RANDOM: ", iRandConvo)
				ELIF iRandConvo = 2
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_FMB3", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_A_FMB3, RANDOM: ", iRandConvo)
				ELIF iRandConvo = 3
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_FMB4", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_A_FMB4, RANDOM: ", iRandConvo)
				ELIF iRandConvo = 4
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_FMB5", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_A_FMB5, RANDOM: ", iRandConvo)
				ELIF iRandConvo = 5
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_FMB6", CONV_PRIORITY_VERY_HIGH)
					REGISTER_QUICK_DELAY("DTRFAIR_OBJCF")
					PRINTLN("PLAYING CONVO - ARMS_A_FMB6, RANDOM: ", iRandConvo)
				ENDIF
			ELIF myArgs.bDoBomb
				iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
				IF iRandConvo = 0
					PRINTLN("BOMB FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FB", CONV_PRIORITY_VERY_HIGH)
				ELIF iRandConvo = 1
					PRINTLN("BOMB FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FB2", CONV_PRIORITY_VERY_HIGH)
				ELIF iRandConvo = 2
					PRINTLN("BOMB FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FB3", CONV_PRIORITY_VERY_HIGH)
				ELIF iRandConvo = 3
					PRINTLN("BOMB FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FB4", CONV_PRIORITY_VERY_HIGH)
				ELIF iRandConvo = 4
					PRINTLN("BOMB FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FB5", CONV_PRIORITY_VERY_HIGH)
				ELIF iRandConvo = 5
					PRINTLN("BOMB FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_FB6", CONV_PRIORITY_VERY_HIGH)
				ENDIF
				REGISTER_QUICK_DELAY("DTRFAIR_OBJB")	
			ELSE
				iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
				IF iRandConvo = 0
					PRINTLN("PACKAGE FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_F", CONV_PRIORITY_VERY_HIGH)
				ELIF iRandConvo = 1
					PRINTLN("PACKAGE FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_F2", CONV_PRIORITY_VERY_HIGH)
				ELIF iRandConvo = 2
					PRINTLN("PACKAGE FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_F3", CONV_PRIORITY_VERY_HIGH)
				ELIF iRandConvo = 3
					PRINTLN("PACKAGE FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_F4", CONV_PRIORITY_VERY_HIGH)
				ELIF iRandConvo = 4
					PRINTLN("PACKAGE FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_F5", CONV_PRIORITY_VERY_HIGH)
				ELIF iRandConvo = 5
					PRINTLN("PACKAGE FAIL: iRandConvo = ", iRandConvo)
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_F6", CONV_PRIORITY_VERY_HIGH)
				ENDIF
				REGISTER_QUICK_DELAY("DTRFAIR_OBJ")	
			ENDIF
		ENDIF

		//DISPLAY_PACKAGE_PRINT(myARgs, cargoArgs, GET_NUMBER_OF_REMAINING_CARGO(myData.iNumTotalCargo, cargoArgs.iNumLanded), TRUE)
		DEBUG_PRINT("PRINTING UNSUCCESSFUL DROP")
		DEBUG_PRINT("DROP FAILED")
	ENDIF
ENDPROC

PROC HANDLE_SIREN_AUDIO_SCENE()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF (GET_PLAYER_DISTANCE_FROM_LOCATION(<<1701.1, 3292.2, 52.6>>) < 100 )
		OR (GET_PLAYER_DISTANCE_FROM_LOCATION(<<1461.3,3091.2,45.8>>) < 100 )
			IF NOT IS_AUDIO_SCENE_ACTIVE("SIREN_SCENE")
				START_AUDIO_SCENE("SIREN_SCENE")
				PRINTLN("STARTING AUDIO SCENE - SIREN_SCENE")
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("SIREN_SCENE")
				STOP_AUDIO_SCENE("SIREN_SCENE")
				PRINTLN("STOPPING AUDIO SCENE - SIREN_SCENE")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_DROPOFF_STAGING(ARGS& myArgs, PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& myData, BLIP_INDEX& myLocationBlip[], 
VECTOR& vCurDestObjective[], AIR_VARIATION myVariation, BOOL& bDoBomb)
	UNUSED_PARAMETER(myArgs)
	VECTOR myPos
	INT idx, idx2, stationIdx
	FLOAT fReturnz
	
	//DEBUG_MESSAGE("UPDATE_DROPOFF_STAGING")
	REPEAT myData.iNumDrops idx
	
		
		// If the drop is complete, go ahead and stop the flare particle effect... TODO: make the guy at position do something too.
		IF cargoArgs.bCargoDropComplete[idx]
			IF cargoArgs.PTFXflare[idx] <> NULL
				STOP_PARTICLE_FX_LOOPED(cargoArgs.PTFXflare[idx])
				cargoArgs.PTFXflare[idx] = NULL
			ENDIF
			
			IF NOT bSwitchedTasks[idx]
				IF NOT IS_ENTITY_DEAD(cargoArgs.pedDropoff[idx]) AND NOT IS_PED_INJURED(cargoArgs.pedDropoff[idx])
					IF IS_VEHICLE_DRIVEABLE(cargoArgs.vehDropoff[idx])
					
						CLEAR_PED_TASKS(cargoArgs.pedDropoff[idx])
						SET_PED_INTO_VEHICLE(cargoArgs.pedDropoff[idx], cargoArgs.vehDropoff[idx])
						
						TASK_VEHICLE_DRIVE_WANDER(cargoArgs.pedDropoff[idx], cargoArgs.vehDropoff[idx], 15.0, DRIVINGMODE_PLOUGHTHROUGH)
						PRINTLN("TASKING VEHICLES TO WANDER ON DROP: ", idx)
						
						SET_PED_KEEP_TASK(cargoArgs.pedDropoff[idx], TRUE)
					
						bSwitchedTasks[idx] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF myArgs.bDoStationary or myArgs.bDoUberBombs
			
			IF myArgs.bDoUberBombs
				// Only do sirens on air trafficking 5
				IF g_savedGlobals.sTraffickingData.iAirRank = 4 
//					HANDLE_RECORDING_START(cargoArgs.vehFinalExtra[0], "carpetbombplane", 101, << 1730.0139, 3314.9282, 41.7539 >>, cargoArgs.recordingStatus[0], 260, TRUE)
					HANDLE_RECORDING_START(cargoArgs.vehFinalExtra[1], "carpetbombtruck", 101, << 1376.1750, 2968.6406, 41.7020 >>, cargoArgs.recordingStatus[1], 850)
					HANDLE_RECORDING_START(cargoArgs.vehFinalExtra[2], "CarpetBombNew", 101, << 1498.5651, 3185.2371, 40.1012 >>, cargoArgs.recordingStatus[2])
					HANDLE_RECORDING_START(cargoArgs.vehFinalExtra[3], "CarpetBombNew", 102, << 1469.9932, 3203.6406, 41.1834 >>, cargoArgs.recordingStatus[3])
					HANDLE_RECORDING_START(cargoArgs.vehFinalExtra[4], "CarpetBombNew", 103, << 1120.4285, 3016.5684, 40.1836 >>, cargoArgs.recordingStatus[4])
					HANDLE_RECORDING_START(cargoArgs.vehFinalExtra[5], "CarpetBombNew", 104, << 1468.7660, 3013.5898, 40.3801 >>, cargoArgs.recordingStatus[5])
					
					//IF NOT IS_MESSAGE_BEING_DISPLAYED()
						IF bTriggerAlarms AND ( GET_PLAYER_DISTANCE_FROM_LOCATION(<<1701.1, 3292.2, 52.6>>) < 75 OR GET_PLAYER_DISTANCE_FROM_LOCATION(<<1461.3,3091.2,45.8>>) < 75 )
							ODDJOB_PLAY_SOUND("Generic_Alarms_Siren", iSirenSoundID, TRUE, NULL, 1701.1, 3292.2, 52.6)
							ODDJOB_PLAY_SOUND("Generic_Alarms_Siren", iSirenSoundID2, TRUE, NULL, 1461.3,3091.2,45.8)
							ODDJOB_PLAY_SOUND("Generic_Alarms_Siren", iSirenSoundID3, TRUE, NULL, 1261.3,3043.8,43.5)
							CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_CRPA", CONV_PRIORITY_VERY_HIGH)
							bTriggerAlarms = FALSE
						ENDIF
						
						HANDLE_SIREN_AUDIO_SCENE()
					//ENDIF
				ENDIF
			ELSE
				//Only for bomb mode.. Lets try to detect if the player killed all the targets.
				BOOL bExtraCarsDead = TRUE
				
				IF NOT cargoArgs.bCargoDropComplete[idx]
				//If this drop site hasn't been destroyed lets check to see if the player somehow killd all the extra cars.
				
					REPEAT MAX_EXTRA_CARS idx2
						IF NOT IS_ENTITY_DEAD(vehStationaryCars[idx][idx2])
							bExtraCarsDead = FALSE
						ENDIF
					ENDREPEAT
					
					//we manually destroyed a target site
					IF bExtraCarsDead
						SCRIPT_ASSERT("Triggering manual detection")
						PRINTLN("SOMETHING FUCKED UP")
						//How do we deal with iCargo.
						//cargoArgs.bCargoSuccessful[iCargo] = TRUE
						//cargoArgs.bCargoComplete[iCargo] = TRUE
						cargoArgs.bDropSuccessful[idx] = TRUE
						cargoArgs.bCargoDropComplete[idx] = TRUE
						HANDLE_DROP_SUCESS(cargoARgs, myData, myVariation, myArgs, 0, idx, vCurDestObjective, bDoBomb, FALSE)
					ENDIF

				ENDIF
				
			ENDIF

			//Optimize.. why do we loop on this
			REPEAT myData.iNumTotalCargo idx2
				IF NOT bTaskedPedToFlee[idx]
					IF SHOULD_CHECK_CARGO(cargoArgs, idx2)
						IF NOT IS_PED_INJURED(cargoArgs.pedDropoff[idx])
							IF GET_DISTANCE_BETWEEN_ENTITIES(cargoArgs.oCargoDrop[idx2], cargoArgs.pedDropoff[idx]) < 100
								myPos = GET_ENTITY_COORDS(cargoArgs.oCargoDrop[idx2])
								IF GET_GROUND_Z_FOR_3D_COORD(myPos, fReturnz)
									myPos.z = fReturnz
								ENDIF
								CLEAR_PED_TASKS(cargoArgs.pedDropoff[idx])
								TASK_SMART_FLEE_COORD(cargoArgs.pedDropoff[idx], myPos, 1000, -1)
								
								PLAY_SOUND_FRONTEND(-1, "DRUG_TRAFFIC_AIR_SCREAMS")
								PRINTLN("PLAYING PED AMBIENT SPEECH - GENERIC_SHOCKED_HIGH")
								
								REPEAT MAX_NUMBER_DROPS stationIdx
									IF NOT IS_ENTITY_DEAD(pedStationaryPeds[idx][stationIdx])
										CLEAR_PED_TASKS(pedStationaryPeds[idx][stationIdx])
										TASK_SMART_FLEE_COORD(pedStationaryPeds[idx][stationIdx], myPos, 1000, -1)
										PRINTLN("PLAYING PED AMBIENT SPEECH - GENERIC_FRIGHTENED_HIGH")
									ENDIF
								ENDREPEAT

								bTaskedPedToFlee[idx] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			//ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(cargoArgs.pedDropoff[idx])
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), cargoArgs.pedDropoff[idx]) < 200
					IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
						IF IS_PED_SHOOTING(PLAYER_PED_ID()) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), cargoArgs.pedDropoff[idx])
							IF NOT bSwitchedTasks[idx]
								CLEAR_PED_TASKS_IMMEDIATELY(cargoArgs.pedDropoff[idx])
								GIVE_WEAPON_TO_PED(cargoArgs.pedDropoff[idx], WEAPONTYPE_PISTOL, 20)
								TASK_COMBAT_PED(cargoArgs.pedDropoff[idx], PLAYER_PED_ID())
								SET_PED_ACCURACY(cargoArgs.pedDropoff[idx], 0)
								DEBUG_PRINT("bPlayerAggroing = TRUE")
								bSwitchedTasks[idx] = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					// TODO: switch to "Come Here" anims once available... use same as intro in towing.
					
					IF NOT bSwitchedTasks[idx]
						IF NOT IS_ENTITY_DEAD(cargoArgs.pedDropoff[idx]) AND NOT IS_PED_INJURED(cargoArgs.pedDropoff[idx])
							IF (GET_SCRIPT_TASK_STATUS(cargoArgs.pedDropoff[idx], SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK)
							AND NOT IS_ENTITY_PLAYING_ANIM(cargoArgs.pedDropoff[idx], "oddjobs@towingcome_here", "come_here_idle_a")
								TASK_PLAY_ANIM(cargoArgs.pedDropoff[idx], "oddjobs@towingcome_here", "come_here_idle_a")
								DEBUG_PRINT("TASKING AGAIN TO PLAY ANIMATION")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// Since the guy at the drop location is dead, make the drop a fail and remove blips.
				cargoArgs.bDrugPurchaserDead[idx] = TRUE
				
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(cargoArgs.pedDropoff[idx], PLAYER_PED_ID())
					bKilledBuyer = TRUE
					PRINTLN("bKilledBuyer = TRUE")
				ENDIF
				
				// Zeroing out vector, so it does not get re-added when getting back in the plane.
				vCurDestObjective[idx] = <<0,0,0>>
				
				IF NOT cargoArgs.bCargoDropComplete[idx]
					PRINT_HELP("DTRFAIR_HELPD")
					PRINTLN("ATTEMPTING TO PRINT HELP - DTRFAIR_HELPD")
					cargoArgs.bCargoDropComplete[idx] = TRUE
				ENDIF
				
				IF DOES_BLIP_EXIST(myLocationBlip[idx])
					SCRIPT_REMOVE_BLIP_SAFE(myLocationBlip[idx])					
				ENDIF
//				IF DOES_BLIP_EXIST(blipRadiusPerfect[idx])
//					SCRIPT_REMOVE_BLIP_SAFE(blipRadiusPerfect[idx])
//				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//Cleanup function to delete our bombs / packages
PROC DELETE_PROPS(PLANE_CARGO_ARGS &cargoArgs, ARGS& myArgs)
	INT idx, idx2
	REPEAT cargoArgs.iNumPackagesTotal idx
		IF DOES_ENTITY_EXIST(cargoArgs.oCargoDrop[idx]) 
			DELETE_OBJECT(cargoArgs.oCargoDrop[idx])
			IF myArgs.bDoUberBombs 
				REPEAT MAX_NUMBER_CLUSTER idx2
					IF DOES_ENTITY_EXIST(cargoArgs.oClusterDrop[idx][idx2]) 
						DELETE_OBJECT(cargoArgs.oClusterDrop[idx][idx2])
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CREATE_UBER_BOMB(PLANE_CARGO_ARGS &cargoArgs, INT iBomb, VEHICLE_INDEX & myVehicle)
	INT idx2
	
	REPEAT MAX_NUMBER_CLUSTER idx2
		IF NOT IS_ENTITY_DEAD(myVehicle)
			cargoArgs.oClusterDrop[iBomb][idx2] = CREATE_OBJECT(mnBombProp01, GET_ENTITY_COORDS(myVehicle))
			SET_ENTITY_RECORDS_COLLISIONS(cargoArgs.oClusterDrop[iBomb][idx2], TRUE)
			SET_ENTITY_LOD_DIST(cargoArgs.oClusterDrop[iBomb][idx2], 1000)
			PRINTLN("CREATING CLUSTER BOMB: ", idx2, " FOR BOMB: ", iBomb)

			IF idx2%2 = 0
				vCargoOffset2.z -= 0.001
				vCargoOffset2.y -= 0.001
				ATTACH_ENTITY_TO_ENTITY(cargoArgs.oClusterDrop[iBomb][idx2], myVehicle, 0, vCargoOffset2, <<0,0,180>>)
			ELSE
				vCargoOffset.z -= 0.001
				vCargoOffset.y -= 0.001
				ATTACH_ENTITY_TO_ENTITY(cargoArgs.oClusterDrop[iBomb][idx2], myVehicle, 0, vCargoOffset, <<0,0,180>>)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CREATE_UBER_BOMBS_IN_PLANE_AND_ATTACH(PLANE_CARGO_ARGS &cargoArgs, INT iBomb, VEHICLE_INDEX & myVehicle, ARGS& myArgs)
	IF myArgs.bDoUberBombs 
		IF iBomb = 0
			IF NOT bCreateUberBomb[0]
				CREATE_UBER_BOMB(cargoArgs, iBomb, myVehicle)
				PRINTLN("iBomb = ", iBomb)
				bCreateUberBomb[0] = TRUE
			ENDIF
		ELIF iBomb  = 1
			IF NOT bCreateUberBomb[1]
				CREATE_UBER_BOMB(cargoArgs, iBomb, myVehicle)
				PRINTLN("iBomb = ", iBomb)
				bCreateUberBomb[1] = TRUE
			ENDIF
		ELIF iBomb  = 2
			IF NOT bCreateUberBomb[2]
				CREATE_UBER_BOMB(cargoArgs, iBomb, myVehicle)
				PRINTLN("iBomb = ", iBomb)
				bCreateUberBomb[2] = TRUE
			ENDIF
		ELIF iBomb  = 3
			IF NOT bCreateUberBomb[3]
				CREATE_UBER_BOMB(cargoArgs, iBomb, myVehicle)
				PRINTLN("iBomb = ", iBomb)
				bCreateUberBomb[3] = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL START_PLANE(VEHICLE_INDEX& myEntities[], VEHICLE_INDEX & myVehicle, PLANE_DATA& myData, PLANE_CARGO_ARGS &cargoArgs, BOOL& bInTransition, ARGS& myArgs, BOOL bTakeControl = TRUE, AIR_VARIATION myVariation = AIR_VARIATION_SIMPLE)
	DEBUG_MESSAGE("START_PLANE")
	INT idx
	bInTransition = TRUE
//	VECTOR vClosestNode[MAX_NUMBER_DROPS]

	IF bTakeControl
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	
	// Possible that the plane was passed in to us, if not, then create it. If so, then copy that.
	IF IS_ENTITY_DEAD(myVehicle)
		myData.myPlane = CREATE_VEHICLE(myData.sPlaneName, vSpawnPosition, fSpawnHeading)
		myVehicle = myData.myPlane
		
		SET_VEHICLE_COLOUR_COMBINATION(myVehicle, 2)
		SET_VEHICLE_ENGINE_CAN_DEGRADE(myVehicle, FALSE)
		
		SET_HELI_BLADES_SPEED(myVehicle, 1.0)
		
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), myVehicle)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		
		PRINTLN("CREATING NEW PLANE ON RETRY")
	ELSE
		myData.myPlane = myVehicle
	ENDIF
	
	// Wipe the area we need collision for. We'll set this later.
	cargoArgs.bTraf4_FreezeBoats = FALSE

	SWITCH myVariation
		CASE AIR_VARIATION_SIMPLE
			
		BREAK
		CASE AIR_VARIATION_LOW_ALT
		CASE AIR_VARIATION_BOMB
		CASE AIR_VARIATION_DROP
		
			IF myArgs.bDoTrain
				CREATE_DROPOFF_STAGING_TRAIN(myArgs, cargoArgs, myData, myVariation)
			ELIF myArgs.bDoUberBombs
				IF g_savedGlobals.sTraffickingData.iAirRank >= 5 
					CREATE_FINAL_STAGING_REPEATABLE(myArgs, cargoArgs, myData)
					PRINTLN("CREATE FINAL STAGING - REPEATABLE MODE")
				ELSE
					CREATE_FINAL_STAGING(myArgs, cargoArgs, myData)
					PRINTLN("CREATE FINAL STAGING - REGULAR MODE")
				ENDIF
			ELIF NOT myArgs.bDoMobile AND NOT(myVariation = AIR_VARIATION_BOMB AND NOT myArgs.bDoStationary)
				// Create dropoff staging.. unless we're the mobile version
				ADD_RELATIONSHIP_GROUP("relRivalRunners", cargoArgs.relRivalRunners)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, cargoArgs.relRivalRunners, RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, cargoArgs.relRivalRunners)
			
				CREATE_DROPOFF_STAGING(myArgs, cargoArgs, myData, myVariation)
			ENDIF
			
			REPEAT myData.iNumTotalCargo idx
				IF DOES_ENTITY_EXIST(myVehicle) AND IS_VEHICLE_DRIVEABLE(myVehicle)
					cargoArgs.oCargoDrop[idx] = CREATE_OBJECT(mnBombProp01, myData.vPlanePos)
					SET_ENTITY_VISIBLE(cargoArgs.oCargoDrop[idx], TRUE)
					
					SET_ENTITY_RECORDS_COLLISIONS(cargoArgs.oCargoDrop[idx], TRUE)
					vCargoOffset.x += 0.01
					//vCargoOffset2.x += 0.01
					ATTACH_ENTITY_TO_ENTITY(cargoArgs.oCargoDrop[idx], myVehicle, 0, vCargoOffset, <<0,0,180>>)
					RESET_BOOLS(cargoArgs.bCargoHitGround)
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH

	IF myVariation = AIR_VARIATION_BOMB OR myArgs.bDoMobile
	
		//Hack? //TODO why was this needed!?
		//myData.endData.vCenterPos = myData.vBomReturnPos
		REPEAT myData.iNumDrops idx
		
			//If we're doing the stationary variation we use the drug locaiton data instead of vectors
			IF NOT myArgs.bDoStationary
				cargoArgs.vehDropoff[idx] = CREATE_VEHICLE(myData.targetCarNames[idx], myData.dropData[idx].vDrugPickup) //myData.vCarSpawnPos[idx])
				PRINTLN("CREATING DROPOFF VEHICLE AT DROP: ", idx, " AT LOCATION: ", myData.dropData[idx].vDrugPickup)
				
				SET_VEHICLE_EXTRA(cargoArgs.vehDropoff[idx], 1, TRUE)
				cargoArgs.carDrivers[idx] = CREATE_PED_INSIDE_VEHICLE(cargoArgs.vehDropoff[idx], PEDTYPE_CRIMINAL, myData.targetCarDriverNames[idx])
				SET_PED_RELATIONSHIP_GROUP_HASH(cargoArgs.carDrivers[idx], cargoArgs.relRivalRunners)
				
				SET_VEHICLE_ON_GROUND_PROPERLY(cargoArgs.vehDropoff[idx])
				
				IF NOT ARE_VECTORS_EQUAL(myArgs.genVectors[idx], <<0,0,0>>)
					OPEN_SEQUENCE_TASK(taskSequence)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, cargoArgs.vehDropoff[idx], myArgs.genVectors[idx], 10.0, DRIVINGSTYLE_NORMAL, PHANTOM, DRIVINGMODE_AVOIDCARS, 10, 10)
						TASK_VEHICLE_DRIVE_WANDER(NULL, cargoArgs.vehDropoff[idx], 1.0, DRIVINGMODE_AVOIDCARS)
					CLOSE_SEQUENCE_TASK(taskSequence)
					TASK_PERFORM_SEQUENCE(cargoArgs.carDrivers[idx], taskSequence)
					CLEAR_SEQUENCE_TASK(taskSequence)
					SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehDropoff[idx], TRUE)
				ELSE
					TASK_VEHICLE_DRIVE_WANDER(cargoArgs.carDrivers[idx], cargoArgs.vehDropoff[idx], 10.0, DRIVINGMODE_AVOIDCARS) //DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
					SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.vehDropoff[idx], TRUE)
				ENDIF
			ENDIF
	
			// Storing off entities for blip tracking
			myEntities[idx] = cargoArgs.vehDropoff[idx] 
		ENDREPEAT

	ENDIF

	IF NOT IS_ENTITY_DEAD(myData.myPlane)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_PLANE(PLANE_DATA& myData)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = myData.myPlane
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PLAYER_HAS_ABANDONED_JOB_PLANE(PLANE_DATA& myData, VECTOR& vCurDestObjective[])

	INT iDrop
	
	FLOAT fCurrDistObj[MAX_NUMBER_DROPS]
	
	// If we have set an abandon distance, get player's distance from start position.
	IF bSetDistance
		fDistanceFromStart = VDIST(vPlayerPos, myData.vPlanePos)
	ENDIF
	
	// Loop through all drops
	REPEAT myData.iNumDrops iDrop
		IF NOT ARE_VECTORS_EQUAL(vCurDestObjective[iDrop], <<0,0,0>>)
			
			// Grab the distance between the player and drops
			fCurrDistObj[iDrop] = VDIST(vPlayerPos, vCurDestObjective[iDrop])
						
			IF fCurrDistObj[iDrop] > fGreatestDistanceToObjective
				fGreatestDistanceToObjective = fCurrDistObj[iDrop] 	
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	// Once the greatest distance has been found, add on the fAbandonAddOn and set as the fAbandonDistance.
	IF NOT bSetDistance
		IF fGreatestDistanceToObjective > 0
			fAbandonDistance = fGreatestDistanceToObjective + fAbandonAddOn
			fWarnDistance =  fGreatestDistanceToObjective + fWarnAddOn
			bSetDistance = TRUE
		ENDIF
	ENDIF
	
	IF fDistanceFromStart > fWarnDistance AND NOT bDistWarned
		bDistWarned = TRUE
		CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIR_D", CONV_PRIORITY_VERY_HIGH)
	ELIF fDistanceFromStart > fAbandonDistance
		DEBUG_PRINT("RETURNING TRUE ON PLAYER_HAS_ABANDONED_JOB_PLANE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC HOLDING_PATTERN(VEHICLE_INDEX myVehicle, VECTOR& myPos, FLOAT& myHeading, INT timer)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	
	//DEBUG_MESSAGE("HOLDING_PATTERN")
	timer = timer
	SET_VEHICLE_ENGINE_ON(myVehicle, TRUE, TRUE)
	
	SET_ENTITY_COORDS(myVehicle, myPos)
	SET_ENTITY_HEADING(myVehicle, myHeading)
	//60 top speed
	//HACK FOR NOW override top speed
	//SET_ENTITY_MAX_SPEED(myVehicle, 50)
	SET_VEHICLE_FORWARD_SPEED(myVehicle, 40.0)
	
	
ENDPROC

FUNC BOOL HAS_PLANE_LANDED(VECTOR myLoc, VEHICLE_INDEX myPlane, VECTOR vLandPos)
	myLoc = myLoc
	
	IF NOT IS_ENTITY_DEAD(myPlane) AND IS_VEHICLE_DRIVEABLE(myPlane)
		//IF fSpeed < 5.0 //AND IS_ENTITY_IN_ANGLED_AREA(myPlane, <<2153.2, 4817.9, 31.06>>, <<1958.6, 4726.4, 51.06>>, 25.0)
		IF GET_ENTITY_DISTANCE_FROM_LOCATION(myPlane, <<2135.9922, 4776.9351, 39.9702>>) < LAND_RADIUS
			IF GET_ENTITY_HEIGHT_ABOVE_GROUND(myPlane) < 2.0  AND vPlayerPos.z - vLandPos.z < 5.0
				BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(myPlane, 6.0, 15)
				PRINTLN("BRINGING PLANE TO A HALT")
				
				TRIGGER_MUSIC_EVENT("OJDA_COMPLETE")
				PRINTLN("STARTING MUSIC - OJDA_COMPLETE")
				
				CLEAR_PRINTS()
				CLEAR_HELP()
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//FUNC BOOL IS_CROSSHAIR_OVER_TARGET(PLANE_CARGO_ARGS &cargoArgs, VECTOR vCrossHair, FLOAT fRadius, BOOL& bIsHighAlt, VECTOR vTarget, INT closestIndex)
//	//INT idx
//	
//	IF bIsHighAlt
//		IF bLocked
//			bLocked = FALSE
//			ODDJOB_STOP_SOUND(cargoArgs.soundID)
//		ENDIF
////		DEBUG_PRINT("PLAYER IS TOO HIGH, RETURNING FALSE ON LOCK ON MODE")
//		RETURN FALSE
//	ENDIF
//	
//	FLOAT fDist = VDIST(vTarget, vCrossHair)
//
//	IF fDist < fRadius
//		IF NOT bLocked
//			ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_LOCK_ON_MASTER", cargoArgs.soundID, TRUE)
//
//		ENDIF
//		bLocked = TRUE
//		iLockedIdx = closestIndex
//		DEBUG_PRINT("bLocked = TRUE")
//		RETURN TRUE
//	ELSE
//		IF (iLockedIdx = closestIndex) AND (iLockOnTimeBufferStart = 0)
//			iLockOnTimeBufferStart = GET_GAME_TIMER()
//		ENDIF
//	ENDIF
//
//	IF NOT bLocked and NOT bPlayingLockDist
//		IF fDist < CONST_LOCK_SOUND_MAX
//			//We want lock on master to play
//			IF NOT bPlayingLockDist
//				bPlayingLockDist = TRUE
//				ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_PRE_LOCK_ON_MASTER", cargoArgs.soundID, TRUE)
//			ENDIF
//		ENDIF
//	ELIF bPlayingLockDist AND NOT bLocked
//		fDist = fDist / CONST_LOCK_SOUND_MAX
//		DEBUG_MESSAGE("Passing in normalized distance:")
//		PRINTFLOAT(fDist)
//		PRINTNL()
////		AUDIO_MG_SET_DRUG_TRAFFIC_TARGET_DIST(fDist)
//	ENDIF
//	
//	IF iLockOnTimeBufferStart <> 0
//		IF GET_GAME_TIMER() - iLockOnTimeBufferStart > LOCK_ON_BUFFER
//			iLockedIdx = -1
//			bLocked = FALSE
//			ODDJOB_STOP_SOUND(cargoArgs.soundID)
//			iLockOnTimeBufferStart = 0
//			DEBUG_PRINT("bLocked = FALSE DUE TO BUFFER RUNNING OUT")
//		ENDIF
//	ENDIF
//
//	RETURN FALSE
//ENDFUNC

////TODO optimize this function!!
////Special flying mode where the player can only turn the plane left and right
//PROC PLANE_BOMB_MODE(PLANE_CARGO_ARGS &cargoArgs, ARGS& myArgs, BOOL& bIsHighAlt, VECTOR& vCurDestObjective[])
//	VECTOR tempRot, vTarget
//	FLOAT xRot, zGround, zWater, fHeight, fRadius, forwardOffset, fPercentage, camRot, xpos, ypos
//	FLOAt fSpeed = 45
//	FLOAT fMaxRadius = 22
//	FLOAT fMinRadius = 3.0
//	INT LeftX, LeftY, RightX, RightY, delta, closestIndex
//	INT idx
//	//BOOL bVisible = FALSE
//	
//	//TODO use this to simulate LB and RB stearing
//	//SET_VEHICLE_STEER_BIAS in the next build to adjust yaw, with values from -1 -> 1
//	
//	//for bomb mode we have to do this differently
//	IF myArgs.bDoBomb OR myArgs.bDoLockOn
//		//We are in bomb mode... 
//		closestIndex = FIND_CLOSEST_ENTITY_INDEX(cargoArgs.vehDropoff, vPlayerPos)
//		IF closestIndex <> -1
//			IF DOES_ENTITY_EXIST(vehStationaryCars[closestIndex])
//				GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(vehStationaryCars[closestIndex]),xpos,ypos)		
//				DRAW_SPRITE("helicopterhud", "hud_outline",xpos,ypos,0.1,0.1,0.1,255,0,0, 200)	
//			ENDIF
//			vTarget = GET_ENTITY_COORDS(cargoArgs.vehDropoff[closestIndex])
//		ENDIF
//	ELSE
//		closestIndex = FIND_CLOSEST_LOCATION_INDEX(vCurDestObjective, vPlayerPos)
//		IF closestIndex <> -1
//			vTarget = vCurDestObjective[closestIndex]
//		ENDIF
//	ENDIF
//	
//	DRAW_OFFSCREEN_ARROW_FOR_COORD(vTarget)
//	
//	GET_SCREEN_COORD_FROM_WORLD_COORD(vTarget,xpos,ypos)	
//	
//	//support lock on
//	IF myArgs.bDoLockOn AND bLocked 
//		DRAW_SPRITE("helicopterhud", "hud_outline",xpos,ypos,0.07,0.07,0.07,0,255,0, 200)	
//	ELSE
//		IF NOT ARE_VECTORS_EQUAL(vTarget, <<0,0,0>>)
//			DRAW_SPRITE("helicopterhud", "hud_outline",xpos,ypos,0.08,0.08,0.08,255,0,0, 200)	
//		ENDIF
//	ENDIF
//
//	IF myArgs.bDoUberBombs
//		//TODO.. only draw the closest group of sprites
//		
//		//TODO clean this up.. this was done ultra quick to demo needs to be better engineered
//		INT startIdx
//		IF closestIndex <> -1
//			
//			startIdx = 1+closestIndex*4
//			//0 means draw 1,2,3
//			//1 means draw 5,6,7
//			//2 means draw 9,10,11
//		ENDIF
//		REPEAT 3 idx
//			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[startIdx+idx])
//				vTarget = GET_ENTITY_COORDS(cargoArgs.vehFinal[startIdx+idx]) 
//				GET_SCREEN_COORD_FROM_WORLD_COORD(vTarget,xpos,ypos)
//				DRAW_SPRITE("helicopterhud", "hud_outline",xpos,ypos,0.08,0.08,0.08,255,0,0, 200)	
//			ENDIF
//		ENDREPEAT
//	ENDIF
//	
//	GET_POSITION_OF_ANALOGUE_STICKS(PAD1, LeftX, LeftY, RightX, RightY)
//	//tilt the camera from 65 to 35
//	IF NOT IS_LOOK_INVERTED()
//		RightY = -RightY
//	ENDIF
//	camRot = 65
//	//180 to -180
//	IF RightY > 0
//		delta = RightY / 6
//		camRot -= delta
//	ELIF RightY < 0
//		camRot = 65 //25 degrees rotated up from looking stright down
//	ENDIF
//	
//	tempRot = GET_ENTITY_ROTATION(cargoArgs.myPlane)
//	xRot = tempRot.x - camRot
//	SET_CAM_ROT(cargoArgs.CamExplosion, <<xRot, tempRot.y, tempRot.z>>)
//	
//	IF myArgs.bDoLockOn
//		fSpeed = 35
//		fMaxRadius = fMinRadius
//	ENDIF
//	
//	SET_TIMECYCLE_MODIFIER("blackNwhite")
//	OVERLAY_SET_SECURITY_CAM(TRUE,FALSE)
//	
//	DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, RIGHTSHOULDER2)
//	DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, LEFTSHOULDER2)
//	DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, LEFTSTICKX)
//	DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, LEFTSTICKY)
//	
//	SET_ENTITY_MAX_SPEED(cargoArgs.myPlane, fSpeed)
//	SET_VEHICLE_FORWARD_SPEED(cargoArgs.myPlane, fSpeed)
//	PLANE_AUTOPILOT(cargoArgs.myPlane, fSpeed, FALSE, 0.5)
//	
//	VECTOR planeCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(cargoArgs.myPlane, <<0,15.0,0>>)
//	
//	BOOL bHasBounds = FALSE
//	IF GET_GROUND_Z_FOR_3D_COORD(planeCoord, zGround)
//		bHasBounds = TRUE
//	ELIF GET_WATER_HEIGHT_NO_WAVES(planeCoord, zGround)
//		bHasBounds = TRUE
//	ENDIF
//	
//	IF bHasBounds
//		//If the water is closer use that
//		IF GET_WATER_HEIGHT_NO_WAVES(planeCoord, zWater)
//			IF zWater > zGround
//				zGround = zWater
//			ENDIF
//		ENDIF
//	
//		fHeight = planeCoord.z - zGround
//		
//		//90m is the max forward offset
//		//15m is the min forward offset
//		fPercentage = fHeight / 100
//		forwardOffset = fPercentage * 25
//		IF forwardOffset > 25
//			forwardOffset = 25.0
//		ELIF forwardOffset < 6.0
//			forwardOffset = 6.0
//		ENDIF
//		
//		//grab height again but off a point further in front of the player
//		planeCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(cargoArgs.myPlane, <<0,forwardOffset,0>>)
//		
//		//Use height to set the radius
//		planeCoord.z = zGround
//		fPercentage = fHeight / 100
//		fRadius = fPercentage * fMaxRadius
//		
//		IF fRadius > fMaxRadius
//			fRadius = fMaxRadius
//		ELIF fRadius < fMinRadius
//			fRadius = fMinRadius
//		ENDIF
//		
//		IF myArgs.bDoLockOn
//			IF IS_CROSSHAIR_OVER_TARGET(cargoArgs, planeCoord, 6.0, bIsHighAlt, vTarget, closestIndex)
//				IF bLocked
//				
//					GET_SCREEN_COORD_FROM_WORLD_COORD(planeCoord, xpos, ypos)
//					//DRAW_SCALEFORM_MOVIE(cargoArgs.sf_hud, 0.5, 0.5, 1.0, 1.0, 255, 0, 0, 255) //(cargoArgs.sf_hud, xpos, ypos, 0.218, 0.515, 255, 0, 0, 255)
//					DRAW_SCALEFORM_MOVIE(cargoArgs.sf_hud, 0.5, 0.5, 1.0, 1.0, 255, 0, 0, 255)//(cargoArgs.sf_hud, xpos, ypos, 1.0, 1.0, 255, 0, 0, 255)
//					//DRAW_CHECKPOINT(planeCoord, fRadius, 0, 255, 0)
//					DEBUG_PRINT("DRAWING GREEN")
//				ENDIF
//			ELSE
//				IF NOT bLocked
//					GET_SCREEN_COORD_FROM_WORLD_COORD(planeCoord, xpos, ypos)
//					DRAW_SCALEFORM_MOVIE(cargoArgs.sf_hud, 0.5, 0.5, 1.0, 1.0, 255, 0, 0, 255)//(cargoArgs.sf_hud, xpos, ypos, 1.0, 1.0, 255, 0, 0, 255)
//					//DRAW_CHECKPOINT(planeCoord, fRadius, 255, 0, 0)
//				ENDIF
//			ENDIF
//		ELSE
//			GET_SCREEN_COORD_FROM_WORLD_COORD(planeCoord, xpos, ypos)
//			DRAW_SCALEFORM_MOVIE(cargoArgs.sf_hud, 0.5, 0.5, 1.0, 1.0, 255, 0, 0, 255)				//(cargoArgs.sf_hud, xpos, ypos, 0.218, 0.515, 255, 0, 0, 255)
//			//DRAW_CHECKPOINT(planeCoord, fRadius, 255, 0, 0)
//		ENDIF
//	ELSE	
//		DRAW_SCALEFORM_MOVIE(cargoArgs.sf_hud, 0.5, 0.5, 1.0, 1.0, 255, 0, 0, 255) //(cargoArgs.sf_hud, 0.5, 0.5, 0.218, 0.515, 255, 0, 0, 255)
//	ENDIF
//ENDPROC

FUNC BOOL IS_PLANE_RIGHTSIDEUP(PLANE_CARGO_ARGS& cargoArgs)
//	FLOAT fRoll, fPitch
	
	IF NOT IS_ENTITY_DEAD(cargoArgs.myPlane)
//		fRoll = GET_ENTITY_ROLL(cargoArgs.myPlane)
//		PRINTLN("PLANE'S ROLL = ", fRoll)
//		fPitch = GET_ENTITY_PITCH(cargoArgs.myPlane)
//		PRINTLN("PLANE'S PITCH = ", fPitch)
		
		IF GET_ENTITY_ROLL(cargoArgs.myPlane) < -150 OR GET_ENTITY_ROLL(cargoArgs.myPlane) > 150
		OR (GET_ENTITY_PITCH(cargoArgs.myPlane) < -150 OR GET_ENTITY_PITCH(cargoArgs.myPlane) > 150)
			PRINTLN("PLANE IS UPDSIDEDOWN")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLANE_TOO_CLOSE_TO_GROUND(PLANE_CARGO_ARGS& cargoArgs)
	VECTOR vPlanePosition
	FLOAT fGroundZ
	
	IF NOT IS_ENTITY_DEAD(cargoArgs.myPlane)
		vPlanePosition = GET_ENTITY_COORDS(cargoArgs.myPlane)
//		PRINTLN("PLANE POSITION = ", vPlanePosition)
	ENDIF
	
	GET_GROUND_Z_FOR_3D_COORD(vPlanePosition, fGroundZ)
//	PRINTLN("GROUND Z FOR PLANE POSITION = ", fGroundZ)
	
	IF ABSF(vPlanePosition.z - fGroundZ) < CONST_TOO_CLOSE_GROUND
		bPlaneTooLow = TRUE
		PRINTLN("WE'RE TOO CLOSE TO THE GROUND")
		RETURN TRUE
	ENDIF
	
	bPlaneTooLow = FALSE
	RETURN FALSE
	
ENDFUNC


PROC GET_OBJ_POSITION_AND_DIRECTION(OBJECT_INDEX objIndex, VECTOR& vMyPos, VECTOR& vDir)
	IF NOT DOES_ENTITY_EXIST(objIndex)
		EXIT
	ENDIF
	
	vMyPos = GET_ENTITY_COORDS(objIndex) + 0.5*vBombVelocity
	SET_FOCUS_POS_AND_VEL(vMyPos, vBombVelocity)
	vDir = <<-80, 0, GET_ENTITY_HEADING(objIndex)+180>>
//	// Get the rotation of the object in world space
//	vDir = GET_ENTITY_ROTATION(objIndex)
//	vDir.x += 180 // Flip it around town
//	vDir.x += 60 // Flip that thang down
	// IF it is single bomb (probably will have to differentiate this on cargo type) then we want to flip
	
	// Generate the direction from the entity's orientation			
	vDir = <<-SIN(vDir.z) * COS(vDir.x), COS(vDir.z) * COS(vDir.x), SIN(vDir.x)>>
ENDPROC

PROC HANDLE_BUTTON_PRESS_SUPPORT(ARGS& myArgs, PLANE_CARGO_ARGS& cargoArgs,  AIR_VARIATION myVariation, FLOAT& fScale, INT idx)
	cargoArgs.bButtonPressed = TRUE

	IF NOT myArgs.bDoUberBombs
		IF myVariation = AIR_VARIATION_BOMB
			fScale = -16.0
			IF NOT UPDATE_COLLISION_TESTS_NOW(collisionArgs, cargoArgs.myPlane)
			AND NOT IS_PLANE_TOO_CLOSE_TO_GROUND(cargoArgs)
				bCollisionDetected = FALSE
				START_TIMER_NOW(tBombTimer)
			ELSE
				bCollisionDetected = TRUE
				PRINTLN("Collision detected")
			ENDIF
			
		ELIF cargoArgs.bBombCam //once we've dropped a package exit the targeting cam
			cargoArgs.bBombCam = FALSE
			CLEANUP_CAM(cargoArgs, bClearPrints)
		ENDIF
	ELSE
		IF bUberActive
			EXIT
		ENDIF
		
		bDoUberCam = FALSE
		IF NOT UPDATE_COLLISION_TESTS_NOW(collisionArgs, cargoArgs.myPlane)
		AND NOT IS_PLANE_TOO_CLOSE_TO_GROUND(cargoArgs)
			bDoUberCam = TRUE
		ENDIF
		
		bUberActive = TRUE
		clusterState = CLUSTER_STATE_INIT
	ENDIF
	
	IF myArgs.bDoUberBombs
		//RELEASE_CARGO(myArgs, cargoArgs.oCargoDrop[cargoArgs.iNumPackageDropped], cargoArgs.myPlane, cargoArgs.soundID, FALSE, -7.0)
		//SET_ENTITY_VISIBLE(cargoArgs.oCargoDrop[cargoArgs.iNumPackageDropped], FALSE)		
	ELSE
		idx = cargoArgs.iNumPackageDropped
		//bCut = TRUE
		IF DOES_ENTITY_EXIST(cargoArgs.oCargoDrop[idx]) 
			SET_ENTITY_VISIBLE(cargoArgs.oCargoDrop[idx], TRUE) //todo.. why the hell do we have to call this
			SET_ENTITY_COLLISION(cargoArgs.oCargoDrop[idx], TRUE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(cargoArgs.oBombDummy)
			SET_ENTITY_VISIBLE(cargoArgs.oBombDummy, FALSE)
			SET_ENTITY_COLLISION(cargoArgs.oBombDummy,FALSE)
		ENDIF
		
		RELEASE_CARGO(cargoArgs.oCargoDrop[idx], cargoArgs.myPlane, cargoArgs.soundID, TRUE, fScale)
		IF NOT myArgs.bDoBomb
			PRINTLN("Applying chute")
			APPLY_CHUTE(cargoArgs.oCargoChute[idx], cargoArgs.oCargoDrop[idx], cargoArgs.iCargoChuteTime[idx], cargoArgs.bCargoChuteVisible[idx])
		ENDIF
		
		IF myArgs.bDoBomb AND NOT myArgs.bDoUberBombs
			IF NOT STREAMVOL_IS_VALID(cargoArgs.streamVolID)
				VECTOR vStreamPos, vStreamDir
				FLOAT fAngleHeight
				fAngleHeight = 200 //Find suitable far clip based on height and direction of drop
				GET_OBJ_POSITION_AND_DIRECTION(cargoArgs.oCargoDrop[idx], vStreamPos, vStreamDir)
			
				PRINTLN("HANDLE_BUTTON_PRESS_SUPPORT: Created stream vol at: ", GET_STRING_FROM_VECTOR(vStreamPos))
				cargoArgs.streamVolID = STREAMVOL_CREATE_FRUSTUM(vStreamPos, vStreamDir, fAngleHeight, FLAG_MAPDATA, STREAMVOL_LOD_FLAG_ALL)
			ENDIF
		ENDIF
			
		IF NOT DOES_BLIP_EXIST(cargoArgs.blipCrateDrop[idx]) AND NOT myArgs.bDoUberBombs
			cargoArgs.blipCrateDrop[idx] = ADD_BLIP_FOR_ENTITY(cargoArgs.oCargoDrop[idx])
			SET_BLIP_COLOUR(cargoArgs.blipCrateDrop[idx], BLIP_COLOUR_GREEN)
			SET_BLIP_SCALE(cargoArgs.blipCrateDrop[idx], 0.75)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(cargoArgs.oBombDummy)
		PRINTLN("OUTSIDE Entity2 exists. Rotation: ", GET_STRING_FROM_VECTOR(GET_ENTITY_ROTATION(cargoArgs.oBombDummy)))
		IF DOES_ENTITY_EXIST(cargoArgs.oCargoDrop[0])
			PRINTLN("OUTSIDE Entity exists. Rotation: ", GET_STRING_FROM_VECTOR(GET_ENTITY_ROTATION(cargoArgs.oCargoDrop[0])))
		ENDIF
	ENDIF
	
	
	
	cargoArgs.iNumReleased++
	
	cargoArgs.bDetached[cargoArgs.iNumPackageDropped] = TRUE
	cargoArgs.cargoTimes[cargoArgs.iNumPackageDropped] = GET_GAME_TIMER()
	
	IF NOT IS_TIMER_STARTED(cargoArgs.tCargoDropTimer)
		START_TIMER_NOW(cargoArgs.tCargoDropTimer)
	ELSE
		RESTART_TIMER_NOW(cargoArgs.tCargoDropTimer)
	ENDIF
	
	IF IS_TIMER_STARTED(cargoArgs.tBombBayTimer)
		RESTART_TIMER_NOW(cargoArgs.tBombBayTimer)
		PRINTLN("RESTARTING TIMER - cargoArgs.tBombBayTimer")
	ENDIF
	
	DEBUG_PRINT("DROPPED PACKAGE")
	cargoArgs.iNumPackageDropped++
ENDPROC

PROC CHECK_FOR_BUTTON_PRESS(ARGS& myArgs, PLANE_CARGO_ARGS& cargoArgs, AIR_VARIATION myVariation)
	
	INT idx
	// If the player has completed the drop phase, do not allow them to drop anymore cargo/bombs.
	IF bDropPhaseComplete
		EXIT
	ENDIF
	IF NOT IS_PLANE_RIGHTSIDEUP(cargoArgs)
		EXIT
	ENDIF
	
	//If we're not currently in a cam lets ensure we don't drop more bombs than we have
	IF NOT bInCam AND NOT bDoUberCam
		IF cargoArgs.iNumReleased = cargoArgs.iNumPackagesTotal
			EXIT
		ENDIF
	ENDIF
	
	FLOAT fScale = -4.0
	BOOL bProcede = FALSE
	
	IF NOT IS_TIMER_STARTED(cargoArgs.tCargoDropTimer)
		bProcede = TRUE
	ELSE
		IF GET_TIMER_IN_SECONDS(cargoArgs.tCargoDropTimer) > CARGO_DROP_TIME_RESET 
			bProcede = TRUE
		ENDIF
		
		IF GET_TIMER_IN_SECONDS(cargoArgs.tCargoDropTimer) > CARGO_DROP_SKIP_DELAY AND (bInCam OR bDoUberCam)
			bProcede = TRUE
		ENDIF
	ENDIF
	
	IF cargoArgs.iCutGameTime <> 0
		bProcede = FALSE
	ENDIF
	
	// If we're in either the uber or bomb cam and detect a collision, go ahead and cleanup cam sequence
	IF iFrameCounter01 = 0
		IF bInCam OR bDoUberCam

			IF UPDATE_COLLISION_TESTS_NOW(collisionArgs, cargoArgs.myPlane)
			OR IS_PLANE_TOO_CLOSE_TO_GROUND(cargoArgs)
			
				PLAY_SOUND_FRONTEND(-1, "DRUG_TRAFFIC_AIR_BOMB_DROP_ERROR_MASTER")
				
				CLEAR_HELP()
				
//				IF myArgs.bDoUberBombs
//					PRINT_HELP_WITH_NUMBER("DTRFAIR_BLFT1", cargoArgs.iNumPackagesTotal - cargoArgs.iNumReleased)
//				ELSE
//					PRINT_HELP_WITH_NUMBER("DTRFAIR_BLFT", cargoArgs.iNumPackagesTotal - cargoArgs.iNumReleased)
//				ENDIF
				
				VECTOR vPlaneRotation
				IF NOT IS_ENTITY_DEAD(cargoArgs.myPlane)
					vPlaneRotation = GET_ENTITY_ROTATION(cargoArgs.myPlane)
					PRINTLN("BEFORE: PLANE'S ROTATION = ", vPlaneRotation)
					SET_ENTITY_ROTATION(cargoArgs.myPlane, <<0, 0, vPlaneRotation.z>>)
					PRINTLN("AFTER: PLANE'S ROTATION = ", vPlaneRotation)
				ENDIF
				IF STREAMVOL_IS_VALID(cargoArgs.streamVolID)
					STREAMVOL_DELETE(cargoArgs.streamVolID)
					CLEAR_FOCUS()
				ENDIF	
				
				IF bDoUberCam
					PRINTLN("CHECK_FOR_BUTTON_PRESS: Cleanup cluster cam")
					CLEANUP_CLUSTER_CAM(cargoArgs, cargoArgs.oCargoDrop[cargoArgs.iNumPackageDropped-1])				
				ENDIF
				
				bSkipped = TRUE
				bDoUberCam = FALSE
				CLEAR_FOCUS()
				
				IF bInCam
					PRINTLN("CHECK_FOR_BUTTON_PRESS: Cleanup explosion cam")
					CLEANUP_EXPLOSION_CAM(cargoArgs, cargoARgs.oCargoDrop[cargoArgs.iNumReleased-1], cargoArgs.iNumReleased-1, 0, FALSE)
				ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF

	IF bProcede
		IF IS_VEHICLE_DRIVEABLE(cargoArgs.myPlane) AND NOT IS_ENTITY_DEAD(cargoArgs.myPlane)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), cargoArgs.myPlane)

				//don't let the player press the button if he's on the ground.
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK) OR bCheckTime
				
					CREATE_UBER_BOMBS_IN_PLANE_AND_ATTACH(cargoArgs, cargoArgs.iNumPackageDropped, cargoArgs.myPlane, myArgs)
				
					bSkipped = FALSE
					IF cargoArgs.iNumReleased <> cargoArgs.iNumLanded AND (bInCam OR bDoUberCam)
						IF STREAMVOL_IS_VALID(cargoArgs.streamVolID)
							STREAMVOL_DELETE(cargoArgs.streamVolID)
							//CLEAR_FOCUS()
						ENDIF
						bSkipped = TRUE
						bDoUberCam = FALSE		
						PRINTLN("CHECK_FOR_BUTTON_PRESS bProcede: CLEANUP_EXPLOSION_CAM")
						CLEANUP_EXPLOSION_CAM(cargoArgs, cargoARgs.oCargoDrop[cargoArgs.iNumReleased-1], cargoArgs.iNumReleased-1, 0, FALSE)
						EXIT
					ENDIF
					
					PRINTLN("EVALUATE_DROP_CONDITIONS VIA CHECK_FOR_BUTTON_PRESS")
					IF EVALUATE_DROP_CONDITIONS(myArgs, cargoArgs, MyLocalPedStruct, bPlayConvo, myVariation, bInCam)
						IF DOES_ENTITY_EXIST(cargoArgs.oCargoDrop[cargoArgs.iNumPackageDropped])
							IF IS_ENTITY_ATTACHED(cargoArgs.oCargoDrop[cargoArgs.iNumPackageDropped]) 
								IF NOT bSkipped
									IF IS_HELP_MESSAGE_ON_SCREEN()
										CLEAR_HELP()
										PRINTLN("CH - 01")
									ENDIF
									
									IF NOT bBombBayDoorsOpen
										IF NOT IS_ENTITY_DEAD(cargoArgs.myPlane)
											OPEN_BOMB_BAY_DOORS(cargoArgs.myPlane)
											PRINTLN("bBombBayDoorsOpen = TRUE - 01")
											bBombBayDoorsOpen = TRUE
											ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BAY_DOOR_OPEN_MASTER", cargoArgs.soundID, FALSE, cargoArgs.myPlane)
											
//											WAIT(1000)
											SETTIMERA(0)
											bCheckTime = TRUE
										ENDIF
									ENDIF
									
									IF bCheckTime 
										IF TIMERA() > 500
											bCheckTime = FALSE
											HANDLE_BUTTON_PRESS_SUPPORT(myArgs, cargoArgs, myVariation, fScale, idx)
										ENDIF
									ELSE
										HANDLE_BUTTON_PRESS_SUPPORT(myArgs, cargoArgs, myVariation, fScale, idx)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iFrameCounter01 > 10
		iFrameCounter01 = 0
	ELSE
		iFrameCounter01++
	ENDIF
ENDPROC

PROC HANDLE_DROP_CLEANUP(BLIP_INDEX cargoBlip, BLIP_INDEX locationBlip, VEHICLE_INDEX& vehicle)
    SCRIPT_REMOVE_BLIP_SAFE(cargoBlip)
	SCRIPT_REMOVE_BLIP_SAFE(locationBlip)
	vehicle = NULL
ENDPROC

FUNC BOOL DID_BOMB_HIT_ANY_TARGET(PLANE_DATA& myData, ARGS& myArgs, VECTOR& vCargoPos[], PLANE_CARGO_ARGS& cargoArgs, INT iDrop, INT iCargo)
//	PRINTLN("DID_BOMB_HIT_ANY_TARGET: ", " IDrop: ", iDrop, " ICargo: ", iCargo)
	
	PRINTLN("DID_BOMB_HIT_ANY_TARGET - CHECKING DROP = ", iDrop)
	PRINTLN("DID_BOMB_HIT_ANY_TARGET - CHECKING CARGO = ", iCargo)
	
	INT idx
	IF myArgs.bDoTrain
		//4 train cars
		//Only the first entity is a train now
		IF iDrop = 0
			REPEAT 4 idx
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], idx), vCargoPos[iCargo]) < 25 / 1.5
					iClosestTrainCarriage = idx
					PRINTLN("iClosestTrainCarriage = ", iClosestTrainCarriage)
					RETURN TRUE
				ENDIF
			ENDREPEAT
//		ELIF iDrop = 1
//			IF GET_ENTITY_DISTANCE_FROM_LOCATION(cargoArgs.vehDropoff[iDrop], vCargoPos[iCargo]) < EXPLODE_RADIUS /// 1.5
//				RETURN TRUE
//			ENDIF
		ENDIF
		
		//don't think engine is counted as cariage?
		IF GET_ENTITY_DISTANCE_FROM_LOCATION(cargoArgs.vehDropoff[iDrop], vCargoPos[iCargo]) < 20 
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(cargoArgs.vehDropoff[iDrop])
			FLOAT fTemp
			fTemp = VDIST(myData.dropData[iDrop].vCenterPos, vCargoPos[iCargo])
			PRINTLN("myData.dropData[iDrop].vCenterPos = ", myData.dropData[iDrop].vCenterPos)
			PRINTLN("vCargoPos[iCargo] = ", vCargoPos[iCargo])
			PRINTLN("DISTANCE = ", fTemp)
			fTemp = fTemp
			IF VDIST(myData.dropData[iDrop].vCenterPos, vCargoPos[iCargo]) < EXPLODE_RADIUS OR (bUberSucess AND iUberSuccess = iDrop)
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("cargoArgs.vehDropoff[iDrop] IS DEAD, ", iDrop)
			//entity dead... return true
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC
	
//This function is an ugly hack to check for uber drop being a success.  Ideally this data would be stored better.. maybe in a 2d array or a struct.  
//In the interest of quicly getting this done and in consideration that this is no just 1 static variation.. going with this ugly hack. StM 
FUNC BOOL CHECK_UBER_DROP_RESULTS(INT checkIDX, PLANE_CARGO_ARGS& cargoArgs)
	//in carpet bombing we just have 1 big array with all targets but they really should be stored in groups of four.
	PRINTLN("CheckIDX = ", checkIDX)
	PRINTLN("INSIDE CHECKING SUCCESS FUNCTION")
	BOOL bSucess = TRUE
	INT idx, idx2
	IF checkIDX = 0 //0,1,2,3
		FOR idx = 0 TO 2
			PRINTLN("Checking idx: ", idx)
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx])
				bSucess = FALSE
			ELSE
				REPEAT 4 idx2
					IF NOT IS_ENTITY_DEAD(pedStationaryPeds[0][idx2]) AND NOT IS_PED_INJURED(pedStationaryPeds[0][idx2])
//						EXPLODE_PED_HEAD(pedStationaryPeds[0][idx2])
						SET_ENTITY_HEALTH(pedStationaryPeds[0][idx2], 0)
						PRINTLN("SETTING PED'S HEALTH TO ZERO, GROUP 0: PED = ", idx2)
					ENDIF
				ENDREPEAT
				PRINTLN("Entity is DEAD: ", idx)
			ENDIF
		ENDFOR
	ELIF checkIDX = 1 //4,5,6,7
		FOR idx = 3 TO 5
			PRINTLN("Checking idx: ", idx)
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx])
				bSucess = FALSE
			ELSE
				REPEAT 4 idx2
					IF NOT IS_ENTITY_DEAD(pedStationaryPeds[1][idx2]) AND NOT IS_PED_INJURED(pedStationaryPeds[1][idx2])
//						EXPLODE_PED_HEAD(pedStationaryPeds[1][idx2])
						SET_ENTITY_HEALTH(pedStationaryPeds[1][idx2], 0)
						PRINTLN("SETTING PED'S HEALTH TO ZERO, GROUP 1: PED = ", idx2)
					ENDIF
				ENDREPEAT
				// There are four groups, and the extra group seems to be closest to Group 1
				REPEAT 4 idx2
					IF NOT IS_ENTITY_DEAD(pedStationaryPeds[3][idx2]) AND NOT IS_PED_INJURED(pedStationaryPeds[3][idx2])
						IF idx2 = 0 OR  idx2 = 2
//							EXPLODE_PED_HEAD(pedStationaryPeds[3][idx2])
							SET_ENTITY_HEALTH(pedStationaryPeds[3][idx2], 0)
							PRINTLN("SETTING PED'S HEALTH TO ZERO, GROUP 3: PED = ", idx2)
						ELSE
							TASK_SMART_FLEE_PED(pedStationaryPeds[3][idx2], PLAYER_PED_ID(), 1000, -1)
						ENDIF
					ENDIF
				ENDREPEAT
				PRINTLN("Entity is DEAD: ", idx)
			ENDIF
		ENDFOR
	ELIF checkIDX = 2//8,9,10,11
		FOR idx = 6 TO 8
			PRINTLN("Checking idx: ", idx)
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx])
				 bSucess = FALSE
			ELSE
				REPEAT 4 idx2
					IF NOT IS_ENTITY_DEAD(pedStationaryPeds[2][idx2]) AND NOT IS_PED_INJURED(pedStationaryPeds[2][idx2])
//						EXPLODE_PED_HEAD(pedStationaryPeds[2][idx2])
						SET_ENTITY_HEALTH(pedStationaryPeds[2][idx2], 0)
						PRINTLN("SETTING PED'S HEALTH TO ZERO, GROUP 2: PED = ", idx2)
					ENDIF
				ENDREPEAT
				PRINTLN("Entity is DEAD: ", idx)
			ENDIF
		ENDFOR
	ENDIF
	PRINTLN("bSucess: ", bSucess)
	RETURN bSucess
ENDFUNC

FUNC BOOL IS_DROP_SUCCESS(ARGS& myArgs, AIR_VARIATION myVariation, VECTOR& vCargoPos[], PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& myData, INT iDrop, INT iCargo, BLIP_INDEX &myLocationBlip[], VECTOR& vCurDestObjective[])
	INT idx
	VECTOR vOffset01 = <<3,0,0>>
	VECTOR vOffset02 = <<-3,0,0>>
	VECTOR vOffset03 = <<0,3,0>>
	VECTOR vOffset04 = <<0,-3,0>>
	VECTOR vTemp,vTemp01, vTemp02, vTemp03
	DEBUG_MESSAGE("IS_DROP_SUCCESS")
	
	PRINTLN("IS_DROP_SUCCESS - CHECKING DROP: ", iDrop)
	
	IF NOT IS_ENTITY_DEAD(cargoArgs.oCargoDrop[iCargo]) AND NOT myArgs.bDoTrain //so train drops can now be in the water..
		IF IS_ENTITY_IN_WATER(cargoArgs.oCargoDrop[iCargo])
			DEBUG_PRINT("Package in the water")
			RETURN FALSE
		ENDIF
	ENDIF
	
	SWITCH myVariation
		CASE AIR_VARIATION_BOMB
			//REPEAT myData.iNumDrops idx
				IF NOT IS_ENTITY_DEAD(cargoArgs.vehDropoff[iDrop])
			
					IF DID_BOMB_HIT_ANY_TARGET(myData, myArgs, vCargoPos, cargoArgs, iDrop, iCargo)
					//IF GET_ENTITY_DISTANCE_FROM_LOCATION(cargoArgs.vehDropoff[iDrop], vCargoPos[iCargo]) < EXPLODE_RADIUS OR (bUberSucess AND iUberSuccess = iDrop)
						bUberSucess = FALSE
						clusterState = CLUSTER_STATE_INIT
						
						IF myArgs.bDoTrain
							IF iDrop = 0
								SET_ENTITY_RENDER_SCORCHED(cargoArgs.vehDropoff[iDrop], TRUE)
								SET_RENDER_TRAIN_AS_DERAILED(cargoArgs.vehDropoff[iDrop], TRUE)
								START_PARTICLE_FX_LOOPED_AT_COORD("scr_ojdg4_train_fire", GET_ENTITY_COORDS(cargoArgs.vehDropoff[iDrop]), <<0,0,0>>, 3.0)
								
								SET_ENTITY_RENDER_SCORCHED(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 1), TRUE)
								SET_RENDER_TRAIN_AS_DERAILED(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 1), TRUE)
								
								SET_ENTITY_RENDER_SCORCHED(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 2), TRUE)
								SET_RENDER_TRAIN_AS_DERAILED(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 2), TRUE)
								
								SET_ENTITY_RENDER_SCORCHED(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 3), TRUE)
								SET_RENDER_TRAIN_AS_DERAILED(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 3), TRUE)
								
								SET_ENTITY_RENDER_SCORCHED(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 4), TRUE)
								SET_RENDER_TRAIN_AS_DERAILED(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 4), TRUE)
								
								IF iClosestTrainCarriage = 0
									vTemp = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 0))
									ADD_EXPLOSION(vTemp + vOffset01, EXP_TAG_TRAIN)
									ADD_EXPLOSION(vTemp + vOffset02, EXP_TAG_TRAIN)
									ADD_EXPLOSION(vTemp + vOffset03, EXP_TAG_TRAIN)
									START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", vTemp, <<0,0,0>>, 3.0)
									START_PARTICLE_FX_LOOPED_AT_COORD("scr_ojdg4_train_fire", vTemp, <<0,0,0>>, 3.0)
									PRINTLN("ADDING EXPLOSIONS - CARRIAGE = 0")
								ELIF iClosestTrainCarriage = 1
									vTemp = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 1))
									ADD_EXPLOSION(vTemp + vOffset01, EXP_TAG_TRAIN)
									ADD_EXPLOSION(vTemp + vOffset02, EXP_TAG_TRAIN)
									ADD_EXPLOSION(vTemp + vOffset03, EXP_TAG_TRAIN)
									START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", vTemp, <<0,0,0>>, 3.0)
									START_PARTICLE_FX_LOOPED_AT_COORD("scr_ojdg4_train_fire", vTemp, <<0,0,0>>, 3.0)
									PRINTLN("ADDING EXPLOSIONS - CARRIAGE = 1")
								ELIF iClosestTrainCarriage = 2
									vTemp = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 2))
									ADD_EXPLOSION(vTemp + vOffset01, EXP_TAG_TRAIN)
									ADD_EXPLOSION(vTemp + vOffset02, EXP_TAG_TRAIN)
									ADD_EXPLOSION(vTemp + vOffset03, EXP_TAG_TRAIN)
									START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", vTemp, <<0,0,0>>, 3.0)
									START_PARTICLE_FX_LOOPED_AT_COORD("scr_ojdg4_train_fire", vTemp, <<0,0,0>>, 3.0)
									PRINTLN("ADDING EXPLOSIONS - CARRIAGE = 2")
								ELIF iClosestTrainCarriage = 3
									vTemp = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 3))
									ADD_EXPLOSION(vTemp + vOffset01, EXP_TAG_TRAIN)
									ADD_EXPLOSION(vTemp + vOffset02, EXP_TAG_TRAIN)
									ADD_EXPLOSION(vTemp + vOffset03, EXP_TAG_TRAIN)
									START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", vTemp, <<0,0,0>>, 3.0)
									START_PARTICLE_FX_LOOPED_AT_COORD("scr_ojdg4_train_fire", vTemp, <<0,0,0>>, 3.0)
									PRINTLN("ADDING EXPLOSIONS - CARRIAGE = 3")
								ELIF iClosestTrainCarriage = 4
									vTemp = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 4))
									ADD_EXPLOSION(vTemp + vOffset01, EXP_TAG_TRAIN)
									ADD_EXPLOSION(vTemp + vOffset02, EXP_TAG_TRAIN)
									ADD_EXPLOSION(vTemp + vOffset03, EXP_TAG_TRAIN)
									START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", vTemp, <<0,0,0>>, 3.0)
									START_PARTICLE_FX_LOOPED_AT_COORD("scr_ojdg4_train_fire", vTemp, <<0,0,0>>, 3.0)
									PRINTLN("ADDING EXPLOSIONS - CARRIAGE = 4")
								ENDIF	
								
								vTemp01 = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(cargoArgs.vehDropoff[iDrop], 4))
								ADD_EXPLOSION(vTemp01 + vOffset03, EXP_TAG_TRAIN)
								
								PRINTLN("ADDING EXPLOSION - DEFAULT")
								
								SET_TRAIN_CRUISE_SPEED(cargoArgs.vehDropoff[iDrop], 0.0)
								EXPLODE_VEHICLE(cargoArgs.vehDropoff[iDrop])
								PRINTLN("TRAIN 0 - ADDING EXPLOSION TO VEHICLE DROPOFF AT DROP: ", iDrop)
							ELIF iDrop = 1
								//The extra boats
							
								EXPLODE_VEHICLE(cargoArgs.vehDropoff[iDrop])
								PRINTLN("TRAIN 1 - ADDING EXPLOSION TO VEHICLE DROPOFF AT DROP: ", iDrop)
								
								REPEAT 4 idx
									IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinalExtra[idx])
										START_PARTICLE_FX_LOOPED_AT_COORD("scr_ojdg4_boat_wreck_fire", GET_ENTITY_COORDS(cargoArgs.vehFinalExtra[idx]), <<0,0,0>>, 3.0)
										EXPLODE_VEHICLE(cargoArgs.vehFinalExtra[idx])
										SET_BOAT_SINKS_WHEN_WRECKED(cargoArgs.vehFinalExtra[idx], TRUE)
									ENDIF
								ENDREPEAT
								
								vTemp01 = GET_ENTITY_COORDS(cargoArgs.vehFinalExtra[2])
								ADD_EXPLOSION(vTemp01 + vOffset03, EXP_TAG_TRAIN)
							
							ENDIF
						ELSE
//							vTemp01 = GET_ENTITY_COORDS(cargoArgs.vehDropoff[iDrop])
							vTemp01 =  myData.dropData[iDrop].vCenterPos
							PRINTLN("ADDING EXPLOSION TO VEHICLE DROPOFF AT DROP: ", iDrop)
							START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", vTemp01, <<0,0,0>>, 3.0)
							
							IF NOT myArgs.bDoUberBombs
//								ADD_EXPLOSION(vTemp01 + vOffset01, EXP_TAG_TRAIN, 0.5, TRUE, TRUE)
								PRINTLN("ADDING EXPLOSION TO VECTOR: ", vTemp01 + vOffset01)
								START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", vTemp01 + vOffset01, <<0,0,0>>, 3.0)
								START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", vTemp01 + vOffset02, <<0,0,0>>, 3.0)
								START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", vTemp01 + vOffset03, <<0,0,0>>, 3.0)
								START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", vTemp01 + vOffset04, <<0,0,0>>, 3.0)
							ENDIF
							
							IF myArgs.bDoStationary
								REPEAT MAX_EXTRA_CARS idx
									IF DOES_ENTITY_EXIST(vehStationaryCars[iDrop][idx]) AND NOT IS_ENTITY_DEAD(vehStationaryCars[iDrop][idx])
										vTemp02 = GET_ENTITY_COORDS(vehStationaryCars[iDrop][idx])
										ADD_EXPLOSION(vTemp02 + vOffset02, EXP_TAG_TRAIN, 0.5, TRUE, TRUE)
										EXPLODE_VEHICLE(vehStationaryCars[iDrop][idx])
										PRINTLN("ADDING EXPLOSION TO STATIONARY CAR AT DROP: ", iDrop, " INDEX = ", idx)
										
									ENDIF
								ENDREPEAT
								REPEAT MAX_NUMBER_STAGED_PROPS idx
									IF DOES_ENTITY_EXIST(cargoArgs.oStagingProps[idx])
										vTemp03 = GET_ENTITY_COORDS(cargoArgs.oStagingProps[idx])
										ADD_EXPLOSION(vTemp03, EXP_TAG_TRAIN, 0.5, TRUE, TRUE)
										PRINTLN("ADDING EXPLOSION PROP AT DROP: ", iDrop, " INDEX = ", idx)
									ENDIF
								ENDREPEAT
								REPEAT MAX_NUMBER_DROPS idx
									IF NOT IS_ENTITY_DEAD(pedStationaryPeds[iDrop][idx])
										SET_ENTITY_HEALTH(pedStationaryPeds[iDrop][idx], 0)
										PRINTLN("SETTING PED'S HEALTH TO ZERO, DROP: ", iDrop, " PED: ", idx)
									ENDIF
								ENDREPEAT
							ENDIF
						ENDIF
				
						HANDLE_DROP_CLEANUP(cargoArgs.blipCrateDrop[iCargo], myLocationBlip[iDrop], cargoArgs.vehDropoff[iDrop])
						
						RETURN TRUE
					ENDIF
				ELSE
				
					IF bUberSucess AND iUberSuccess = iDrop
						bUberSucess = FALSE
						clusterState = CLUSTER_STATE_INIT
					ENDIF
					
					DEBUG_MESSAGE("VEHICLE DROPOFF GUY IS DEAD")
					HANDLE_DROP_CLEANUP(cargoArgs.blipCrateDrop[iCargo], myLocationBlip[iDrop], cargoArgs.vehDropoff[iDrop])
					
					//uber bomb needs a special check
					IF myArgs.bDoUberBombs
						IF NOT CHECK_UBER_DROP_RESULTS(iDrop, cargoArgs)
							RETURN FALSE
						ENDIF
					ENDIF
				
					RETURN TRUE
				ENDIF
			//ENDREPEAT
		BREAK
		DEFAULT
			IF myArgs.bDoLockOn 
				DEBUG_MESSAGE("myArgs.bDoLockOn ")
				IF iLockedIdx = iDrop
					DEBUG_MESSAGE("iLockedIdx = iDrop")
					IF GET_ENTITY_DISTANCE_FROM_LOCATION(cargoArgs.vehDropoff[iDrop], vCargoPos[iCargo]) < 5
						cargoArgs.vehDropoff[iDrop] = NULL
						CLEANUP_QUICK_CAM(cargoArgs, bLocked, iLockedIdx)
						SCRIPT_REMOVE_BLIP_SAFE(cargoArgs.blipCrateDrop[iCargo])
						SCRIPT_REMOVE_BLIP_SAFE(myLocationBlip[iDrop])
						RETURN TRUE
					ENDIF
				ENDIF
			ELIF VDIST(myData.dropData[iDrop].vCenterPos, vCargoPos[iCargo]) < DROP_RADIUS 
				SCRIPT_REMOVE_BLIP_SAFE(cargoArgs.blipCrateDrop[iCargo])
				SCRIPT_REMOVE_BLIP_SAFE(myLocationBlip[iDrop])
				vCurDestObjective[iDrop] = <<0,0,0>>
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	SCRIPT_REMOVE_BLIP_SAFE(cargoArgs.blipCrateDrop[iCargo])
	//SCRIPT_REMOVE_BLIP_SAFE(cargoArgs.blipCargoDrop[iDrop])
	RETURN FALSE
ENDFUNC


PROC CHECK_DROP_RESULTS(ARGS& myArgs, INT iCargo, INT iDrop, PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& myData, 
VECTOR& vCargoPos[], AIR_VARIATION myVariation, BLIP_INDEX &myLocationBlip[], VECTOR& vCurDestObjective[], BOOL& bDoBomb)

	vCargoPos[iCargo] = GET_ENTITY_COORDS(cargoArgs.oCargoDrop[iCargo])
	DEBUG_MESSAGE(" CHECK_DROP_RESULTS")		
	// Loop through number of drops and check the distance from the drop off spots
	REPEAT myData.iNumDrops iDrop
	
		PRINTLN("CHECK_DROP_RESULTS - CHECKING DROP: ", iDrop)
	
		IF IS_ENTITY_DEAD(cargoArgs.oCargoDrop[iCargo])
			DEBUG_MESSAGE("IS_ENTITY_DEAD(cargoArgs.oCargoDrop[iCargo] = TRUE")	
		ENDIF
	
		IF cargoArgs.bDrugPurchaserDead[iDrop]
			DEBUG_MESSAGE("cargoArgs.bDrugPurchaserDead[iDrop] = TRUE")	
		ENDIF
		
		//IF NOT cargoArgs.bCargoComplete[iCargo]
		IF NOT cargoArgs.bCargoDropComplete[iDrop] //AND NOT IS_ENTITY_DEAD(cargoArgs.oCargoDrop[iCargo])
			// If the cargo has hit the ground, check the distance
			IF NOT cargoArgs.bDrugPurchaserDead[iDrop]
				IF IS_DROP_SUCCESS(myArgs, myVariation, vCargoPos, cargoArgs, myData, iDrop, iCargo, myLocationBlip, vCurDestObjective)
					
					PRINTLN("DROP IS A SUCCESS - GOING TO HANDLE DROP SUCCESS")
					
					HANDLE_DROP_SUCESS(cargoARgs, myData, myVariation, myArgs, iCargo, iDrop, vCurDestObjective, bDoBomb)

				ELSE
					//DEBUG_MESSAGE("Failed drop")
					//CargoArgs.bCargoDropComplete[iDrop] = TRUE
				ENDIF
			ELSE
				//argoArgs.bCargoDropComplete[iDrop]
				DEBUG_MESSAGE("bDrugPurchaserDead = TRUE")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//hack function for now... need a vehicle where the entity actually stays in but for now do this
FUNC BOOL HAS_VEHICLE_LANDED_IN_CAR(OBJECT_INDEX myEntity, PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& myData)
	INT idx
		
	REPEAT myData.iNumDrops idx
		IF DOES_ENTITY_EXIST(cargoArgs.vehDropoff[idx]) AND NOT IS_ENTITY_DEAD(cargoArgs.vehDropoff[idx])
			IF IS_ENTITY_TOUCHING_ENTITY(cargoArgs.vehDropoff[idx], myEntity)
				DEBUG_MESSAGE("HAS_VEHICLE_LANDED_IN_CAR true")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_DISTANCE_AND_PRINTS(AIR_VARIATION myVariation, PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& myData, ARGS& myArgs)
	INT closestIndex 
	FLOAT fDist
	STRING sCamDrop
	UNUSED_PARAMETER(myData)
	IF NOT cargoArgs.bCompletedOneDrop
		
		IF g_savedGlobals.sTraffickingData.iAirRank >= 5 AND myArgs.bDoUberBombs
			closestIndex = FIND_CLOSEST_ENTITY_INDEX(cargoArgs.vehFinal, vPlayerPos)
		ELSE
			closestIndex = FIND_CLOSEST_ENTITY_INDEX(cargoArgs.vehDropoff, vPlayerPos)
		ENDIF
		
		IF closestIndex <> -1
			
			IF g_savedGlobals.sTraffickingData.iAirRank >= 5 AND myArgs.bDoUberBombs
				fDist = GET_DISTANCE_BETWEEN_ENTITIES(cargoArgs.vehFinal[closestIndex], PLAYER_PED_ID(), FALSE) 
			ELSE
				fDist = GET_DISTANCE_BETWEEN_ENTITIES(cargoArgs.vehDropoff[closestIndex], PLAYER_PED_ID(), FALSE) 
			ENDIF
			
			SWITCH myVariation
				CASE AIR_VARIATION_BOMB
					//bombs
					sCamDrop = "DTRFAIR_HELPB"
				BREAK
				CASE AIR_VARIATION_DROP
				CASE AIR_VARIATION_LOW_ALT
					//cargo
					sCamDrop = "DTRFAIR_DPCG"
				BREAK
			ENDSWITCH
			
			IF myArgs.bDoMobile
				sCamDrop = "DTRFAIR_LOCK"
			ENDIF
				
			SWITCH dropInstructState
				CASE PLANE_PRINTS_CAM
					IF fDist < CONST_FIRST_PRINT_DIST
						// To enter bomb cam press ~PAD_B~.
						//PRINT_HELP("DTRFAIR_HELPE")
						
						IF myArgs.bDoUberBombs
							OPEN_BOMB_BAY_DOORS(myData.myPlane)
							bBombBayDoorsOpen = TRUE
							PRINTLN("OPENING BOMB BAY DOORS - UBER")
							ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BAY_DOOR_OPEN_MASTER", cargoArgs.soundID, FALSE, myData.myPlane)
						ENDIF
						
						IF NOT myArgs.bDoTutorial
							cargoArgs.bInstructionsPrinted = TRUE
						ENDIF
						
						IF g_savedGlobals.sTraffickingData.iAirRank = 2
							TRIGGER_MUSIC_EVENT("OJDA3_HATCH")
							PRINTLN("TRIGGERING MUSIC - OJDA3_HATCH")
						ENDIF
						
						dropInstructState = PLANE_PRINTS_STEAR
					ENDIF
				BREAK
				CASE PLANE_PRINTS_STEAR
					//Distance check.. if we're too close skip stearing controls
					IF fDist < CONST_DROP_PRINT
						dropInstructState = PLANE_PRINTS_DROP
					ELSE
						IF cargoArgs.bBombCam
						// To steer press
							IF IS_HELP_MESSAGE_ON_SCREEN()
								CLEAR_HELP()
								PRINTLN("CH - 02")
							ENDIF
							PRINT_HELP("DTRFAIR_TGTS")
							dropInstructState = PLANE_PRINTS_DROP
						ENDIF
					ENDIF
				BREAK
				CASE PLANE_PRINTS_DROP
					//to drop the bomb press
					IF fDist < CONST_DROP_PRINT
						IF cargoArgs.iNumReleased = 0
							IF IS_HELP_MESSAGE_ON_SCREEN()
								CLEAR_HELP()
								PRINTLN("CH - 03")
							ENDIF
							PRINT_HELP(sCamDrop)
						ENDIF
						dropInstructState = PLANE_PRINTS_COMPLETE
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
			
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_TUTORIAL(PLANE_CARGO_ARGS& cargoArgs, VECTOR& vCurDestObjective[], PLANE_STATE& myState)
//	DEBUG_MESSAGE("UPDATE_TUTORIAL")

	INT closestIndex
	//VECTOR temp
	closestIndex = FIND_CLOSEST_LOCATION_INDEX(vCurDestObjective, vPlayerPos)
//	iClosestDrop = closestIndex
//	
//	IF iClosestDrop <> -1
//		PRINTLN("iClosestDrop = ", iClosestDrop)
//	ENDIF

	//skip if we're told to land
	IF myState = PLANE_STATE_LAND AND tutorialState  < TUTORIAL_STATE_LANDING_GEAR
		tutorialState = TUTORIAL_STATE_LANDING_GEAR
	ENDIF
	
	//If you get out of the vehicle reset and to go a pending state?
	IF bOutOfVehicle
		IF IS_HELP_MESSAGE_ON_SCREEN()
			CLEAR_HELP()
			PRINTLN("CH - 04")
		ENDIF
		tutorialState = TUTORIAL_STATE_PENDING
	ENDIF
	
	SWITCH tutorialState
		CASE TUTORIAL_STATE_PENDING
			IF NOT bOutOfVehicle
				tutorialState = TUTORIAL_STATE_PRE
			ENDIF
		BREAK
		CASE TUTORIAL_STATE_PRE	
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				//Check to see if we're high enough in the air
				IF GET_LANDING_GEAR_STATE(cargoArgs.myPlane) = LGS_LOCKED_UP OR GET_LANDING_GEAR_STATE(cargoArgs.myPlane) = LGS_RETRACTING 
					tutorialState = TUTORIAL_STATE_START
				ELIF GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID()) > 20
					SETTIMERA(0)
//					CLEAR_HELP()
					
					IF g_replay.iReplayInt[0] < 3  	//int used to track number of retries for the current mission. Only show tutorial messages for the first 3 retries
						PRINT_HELP("DTRFAIR_TUTO3")
					ENDIF
				
					tutorialState = TUTORIAL_STATE_START
				ENDIF
			ENDIF
		BREAK
		CASE TUTORIAL_STATE_START
			IF NOT cargoArgs.bCompletedOneDrop
				IF NOT bClearedLandingHelp
					IF GET_LANDING_GEAR_STATE(cargoArgs.myPlane) = LGS_LOCKED_UP
					OR GET_LANDING_GEAR_STATE(cargoArgs.myPlane) = LGS_RETRACTING
						CLEAR_HELP()
						bClearedLandingHelp = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF g_replay.iReplayInt[0] < 3 					//int used to track number of retries for the current mission. Only show tutorial messages for the first 3 retries
				IF closestIndex <> -1
					IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vCurDestObjective[closestIndex])  < 2300
						IF NOT bFlareHelpPrint
							PRINT_HELP("DTRFAIR_TUTO1")
							bFlareHelpPrint = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF closestIndex <> -1
				IF NOT (cargoArgs.bDropSuccessful[closestIndex]) 
					//todo
					IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vCurDestObjective[closestIndex])  < 1500
						SETTIMERA(0)
//						CLEAR_HELP()
					
						DEBUG_MESSAGE("Setting print position 1")
						
						IF g_replay.iReplayInt[0] < 3		//int used to track number of retries for the current mission. Only show tutorial messages for the first 3 retries
							IF NOT bPrintDropZoneHelp
								IF NOT IS_HELP_MESSAGE_ON_SCREEN()
									PRINT_HELP("DTRFAIR_HELPJ")
									bPrintDropZoneHelp = TRUE
									PRINTLN("PRINTING DROP ZONE HELP")
								ENDIF
							ENDIF
						ENDIF
						
						tutorialState = TUTORIAL_STATE_A
					ENDIF
				ENDIF
			ENDIF
			//ENDIF
		BREAK
		CASE TUTORIAL_STATE_A
			closestIndex = FIND_CLOSEST_LOCATION_INDEX(vCurDestObjective, vPlayerPos)
			//SET_HELP_MESSAGE_STYLE(HELP_MESSAGE_STYLE_TAGGABLE, HUD_COLOUR_NET_PLAYER15, 255, HELP_TEXT_SOUTH)
			//DEBUG_MESSAGE("Setting print position 2")
			//SET_HELP_MESSAGE_WORLD_POSITION(<<vCurDestObjective[closestIndex].x, vCurDestObjective[closestIndex].y, vCurDestObjective[closestIndex].z+25>> )
			
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vCurDestObjective[closestIndex])  < CONST_FIRST_PRINT_DIST_TUT - 50
				PREPARE_MUSIC_EVENT("OJDA1_HATCH_OPEN")
				PRINTLN("PREPARING MUSIC - OJDA1_HATCH_OPEN")
			ENDIF
			
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vCurDestObjective[closestIndex])  < CONST_FIRST_PRINT_DIST_TUT
				SETTIMERA(0)
//				CLEAR_HELP()
				
				IF NOT cargoArgs.bInstructionsPrinted
					//Camera cut here!
					OPEN_BOMB_BAY_DOORS(cargoArgs.myPlane)
					bBombBayDoorsOpen = TRUE
					PRINTLN("bBombBayDoorsOpen = TRUE - 03")
					ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BAY_DOOR_OPEN_MASTER", cargoArgs.soundID, FALSE, cargoArgs.myPlane)
					PRINTLN("PLAYING SOUND")
					tutorialState = TUTORIAL_STATE_CUT
				ELSE
					SETTIMERA(1000)
					tutorialState = TUTORIAL_STATE_B
				ENDIF
			ENDIF
		BREAK
		CASE TUTORIAL_STATE_CUT
//			SETUP_OPEN_BOMB_CAM(myArgs, cargoArgs)
			tutorialState = TUTORIAL_STATE_B
		BREAK	
		CASE TUTORIAL_STATE_B
//			IF TIMERA() > 1500
//				cargoArgs.bInstructionsPrinted = TRUE
//				CLEANUP_CAM(cargoArgs, bClearPrints)
//				IF IS_HELP_MESSAGE_ON_SCREEN()
//					CLEAR_HELP()
//					PRINTLN("CH - 05")
//				ENDIF
//				tutorialState = TUTORIAL_STATE_E
//
//			ENDIF
			
			cargoArgs.bInstructionsPrinted = TRUE
			tutorialState = TUTORIAL_STATE_E
		BREAK
//		CASE TUTORIAL_STATE_C
//			SET_HELP_MESSAGE_STYLE(HELP_MESSAGE_STYLE_TAGGABLE, HUD_COLOUR_NET_PLAYER15, -1, HELP_TEXT_SOUTH)
//			temp = vPlayerPos
//			DEBUG_MESSAGE("Setting print position 4")
//			SET_HELP_MESSAGE_WORLD_POSITION(temp)
//			IF TIMERA() > 6000 OR cargoArgs.bBombCam
//				CLEAR_HELP()
//				tutorialState = TUTORIAL_STATE_D 
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE TUTORIAL_STATE_D
//			IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vCurDestObjective[closestIndex], FALSE) < CONST_DROP_PRINT
//				//we're too closej ust print how to drop the bomb
//				tutorialState = TUTORIAL_STATE_E
//			ELIF cargoArgs.bBombCam
//				//~s~To steer while in target cam press ~PAD_LB~. and ~PAD_RB~~s~
//				PRINT_HELP("DTRFAIR_TGTS")
//				tutorialState = TUTORIAL_STATE_E
//			ENDIF
//		BREAK
		CASE TUTORIAL_STATE_E
			//IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vCurDestObjective[closestIndex], FALSE) < CONST_DROP_PRINT
//				CLEAR_HELP()
				
				PRINT_HELP("DTRFAIR_DPCG")
							
				SETTIMERB(0)
				//SET_HELP_MESSAGE_WORLD_POSITION(<<vCurDestObjective[closestIndex].x,vCurDestObjective[closestIndex].y,vCurDestObjective[closestIndex].z+2.0>>)
				tutorialState = TUTORIAL_STATE_F
			//ENDIF
		BREAK
		CASE TUTORIAL_STATE_F
			IF cargoArgs.bCompletedOneDrop OR cargoArgs.bDroppedOne
				//rely on clear help called in other scripts
				tutorialState = TUTORIAL_STATE_REPEAT
				BREAK
			ELIF TIMERB() > 8000
				IF IS_HELP_MESSAGE_ON_SCREEN()
					CLEAR_HELP()
					PRINTLN("CH - 06")
				ENDIF
				tutorialState = TUTORIAL_STATE_REPEAT
			ENDIF
			//SET_HELP_MESSAGE_STYLE(HELP_MESSAGE_STYLE_TAGGABLE, HUD_COLOUR_NET_PLAYER15, -1, HELP_TEXT_SOUTH)
			//DEBUG_MESSAGE("Setting print position 6")
			//SET_HELP_MESSAGE_WORLD_POSITION(vPlayerPos)
			
		BREAK
		CASE TUTORIAL_STATE_REPEAT
			IF cargoArgs.bCompletedOneDrop
				cargoArgs.bCompletedOneDrop = FALSE
				tutorialState = TUTORIAL_STATE_START
			ENDIF
		BREAK
		CASE TUTORIAL_STATE_LANDING_GEAR
			//PRINTLN("In landing gear state destionation is: ", vCurDestObjective[0])
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vCurDestObjective[0], FALSE) < 800 //CONST_FIRST_PRINT_DIST_TUT
				IF GET_LANDING_GEAR_STATE(cargoArgs.myPlane) = LGS_LOCKED_UP OR GET_LANDING_GEAR_STATE(cargoArgs.myPlane) = LGS_RETRACTING
					START_TIMER_NOW(tutorialTimer)
					CLEAR_HELP()
					PRINT_HELP("DTRFAIR_TUTO4")

					tutorialState = TUTORIAL_STATE_LANDING_WAIT
				ELSE
					tutorialState = TUTORIAL_STATE_END
				ENDIF
			ENDIF
		BREAK
		CASE TUTORIAL_STATE_LANDING_WAIT
			DEBUG_MESSAGE("TUTORIAL_STATE_LANDING_WAIT")
			
			IF GET_LANDING_GEAR_STATE(cargoArgs.myPlane) = LGS_LOCKED_DOWN OR TIMER_DO_WHEN_READY(tutorialTimer, 8)
				DEBUG_MESSAGE("TUTORIAL_STATE_END")
				tutorialState = TUTORIAL_STATE_END
			ENDIF
		BREAK
		CASE TUTORIAL_STATE_END
			//empty
		BREAK
			
	ENDSWITCH
					
	RETURN FALSE
ENDFUNC

PROC UPDATE_HACK_HEAT_SINK(PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData, ENTITY_INDEX myPackage, ARGS& myArgs)
	iFrameCounterCam++
	
	IF iFrameCounterCam > 1
		iFrameCounterCam = 0
		
		LeftX = (0.5) * GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) 
		LeftY = (0.5) * GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
		vBombMoveOffset = ROTATE_VECTOR_ABOUT_Z(<<-LeftX, LeftY, 0>>, fBombHeading)
	ENDIF
	
	IF myArgs.bDoBomb AND NOT bSkipped AND NOT bPlaneTooLow
		PRINT_HELP("DTRFAIR_HELPK")
	ENDIF
	
	IF DOES_ENTITY_EXIST(myPackage)
	AND DOES_ENTITY_EXIST(cargoArgs.oBombDummy)
		IF NOT IS_ENTITY_DEAD(myPackage)
		AND NOT IS_ENTITY_DEAD(cargoArgs.oBombDummy)
			APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(myPackage, APPLY_TYPE_IMPULSE, vBombMoveOffset, 0, FALSE, TRUE)
			APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(cargoArgs.oBombDummy, APPLY_TYPE_IMPULSE, vBombMoveOffset, 0, FALSE, TRUE)
		ENDIF
	ENDIF
	
	myData.fPlayerRot = myData.fPlayerRot
ENDPROC

//broke this out for readbility.  Checks to see if our bomb / package have hit the ground or water
FUNC BOOL HAS_DROP_HIT(PLANE_CARGO_ARGS &cargoArgs, INT iCargo)
	//PRINTLN("HAS_DROP_HIT: ", " ICargo: ", iCargo)
	BOOL bHit = FALSE

	VECTOR vBombCoord = GET_ENTITY_COORDS(cargoArgs.oCargoDrop[iCargo])
	FLOAT fBombHeight = vBombCoord.z
	
	VECTOR vPlaneCoord = GET_ENTITY_COORDS(cargoArgs.myPlane)
	FLOAT fPlaneHeight = vPlaneCoord.z

	IF NOT bPlaneCollidable
		IF VDIST2(GET_ENTITY_COORDS(cargoArgs.oCargoDrop[iCargo]), GET_ENTITY_COORDS(cargoArgs.myPlane)) > 16 // 4*4
			SET_ENTITY_COLLISION(cargoArgs.myPlane, TRUE)
			bPlaneCollidable = TRUE
		ENDIF
	ENDIF

	// Has entity collided with anything other than plane (makes sure it has escaped the plane in the z)
	IF (HAS_ENTITY_COLLIDED_WITH_ANYTHING(cargoArgs.oCargoDrop[iCargo]) AND NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(cargoArgs.oCargoDrop[iCargo]))
	AND fBombHeight < (fPlaneHeight - 3)
		PRINTLN("HAS_DROP_HIT: Entity collided with something")
		bHit = TRUE
	ENDIF
	
	IF NOT bCreatedThisFrame
		IF GET_ENTITY_SPEED(cargoArgs.oCargoDrop[iCargo]) = 0 AND NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(cargoArgs.oCargoDrop[iCargo])
			PRINTLN("HAS_DROP_HIT: Entity speed is too low")
			bHit = TRUE
		ENDIF
	ELSE
		bCreatedThisFrame = FALSE
	ENDIF
			
	IF IS_ENTITY_IN_WATER(cargoArgs.oCargoDrop[iCargo])
		PRINTLN("HAS_DROP_HIT: Entity has hit the water")
		//need to track this for what explosion to trigger.
		bHit = TRUE
		bHitWater = TRUE
	ELSE
		bHitWater = FALSE
	ENDIF

	RETURN bHit
ENDFUNC


PROC UPDATE_DROP_STATUS(ARGS& myArgs, PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData, INT& iCargo, INT& iDrop, 
VECTOR &vCurDestObjective[], VECTOR& vCargoPos[], BLIP_INDEX &myLocationBlip[], AIR_VARIATION &myVariation, BOOL& bDoBomb)
	VECTOR vTemp, vOffset01, vOffset02, vOffset03, vOffset04

	vOffset01 = <<7,0,0>>
	vOffset02 = <<-7,0,0>>
	vOffset03 = <<0,7,0>>
	vOffset04 = <<0,-7,0>>

	IF cargoArgs.iNumPackageDropped > 0 AND myArgs.bDoBomb
		IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(cargoArgs.oCargoDrop[cargoArgs.iNumPackageDropped-1]) AND NOT cargoArgs.bCargoComplete[cargoArgs.iNumPackageDropped-1]
			//UPDATE_VISIBLILITY_AND_COLLISION(cargoArgs.oCargoDrop[cargoArgs.iNumPackageDropped-1], cargoArgs)
			SIMULATE_NOSE_TIP_DOWN(cargoArgs.oCargoDrop[cargoArgs.iNumPackageDropped-1], cargoArgs)

			IF bSwitchedBombs
				fCamFov = LERP_FLOAT(65.0, 25.0, fLerpCount/360)
				fLerpCount += 1.0
					
				IF fCamFov >= 25.0
					IF DOES_CAM_EXIST(cargoArgs.CamExplosion)
						IF IS_CAM_ACTIVE(cargoArgs.CamExplosion)
							SET_CAM_FOV(cargoArgs.CamExplosion, fCamFov)
							PRINTLN("CAM FOV = ", GET_CAM_FOV(cargoArgs.CamExplosion))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	// Loop on number of cargo packages  //
	REPEAT myData.iNumTotalCargo iCargo
		IF DOES_ENTITY_EXIST(cargoArgs.oCargoDrop[iCargo]) AND NOT cargoArgs.bCargoComplete[iCargo] AND NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(cargoArgs.oCargoDrop[iCargo]) 
		
			//We're falling down... let's try to hack check early sucess and if too far away push the package towards the target
			IF NOT myArgs.bDoUberBombs
				UPDATE_HACK_HEAT_SINK(cargoArgs, myData, cargoArgs.oCargoDrop[iCargo], myArgs)
			ENDIF
			
			vBombVelocity = GET_ENTITY_VELOCITY(cargoArgs.oCargoDrop[iCargo])
			IF vBombVelocity.z > -70
				vBombVelocity.z -= 0.25
				
				SET_ENTITY_VELOCITY(cargoArgs.oCargoDrop[iCargo], vBombVelocity)
				
				IF DOES_ENTITY_EXIST(cargoArgs.oBombDummy)
					SET_ENTITY_VELOCITY(cargoArgs.oBombDummy, vBombVelocity)
					//VECTOR vTempRot = GET_ENTITY_ROTATION(cargoArgs.oBombDummy)
					//VECTOR vTempRot2 = GET_ENTITY_ROTATION(cargoARgs.oCargoDrop[iCargo])
					//vTempRot.x = vTempRot2.x
					//SET_ENTITY_ROTATION(cargoArgs.oBombDummy, vTempRot)
					//SET_ENTITY_COORDS(cargoArgs.oBombDummy, GET_ENTITY_COORDS(cargoArgs.oCargoDrop[iCargo]))
				ENDIF
			ENDIF
			
			IF myVariation = AIR_VARIATION_DROP OR myVariation = AIR_VARIATION_LOW_ALT
				IF NOT cargoArgs.bCargoChuteVisible[iCargo] AND DOES_ENTITY_EXIST(cargoArgs.oCargoChute[iCargo])
				PRINTLN("Checking if we should turn on chute..")
					IF GET_GAME_TIMER() - cargoArgs.iCargoChuteTime[iCargo] > 150
						PRINTLN("Turning on the chute drawing")
						PLAY_ENTITY_ANIM(cargoArgs.oCargoChute[iCargo], "p_cargo_chute_s_deploy", "p_cargo_chute_s", INSTANT_BLEND_IN, FALSE, FALSE)
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(cargoArgs.oCargoChute[iCargo])
						SET_ENTITY_VISIBLE(cargoArgs.oCargoChute[iCargo], TRUE)
						cargoArgs.bCargoChuteVisible[iCargo] = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_DROP_HIT(cargoArgs, iCargo)
				IF NOT cargoArgs.bCargoHitGround[iCargo]
					cargoArgs.iNumLanded ++
					DEBUG_PRINT("INCREASING NUM LANDED")
					cargoArgs.bCargoHitGround[iCargo] = TRUE
					
					IF STREAMVOL_IS_VALID(cargoArgs.streamVolID)
						STREAMVOL_DELETE(cargoArgs.streamVolID)
						CLEAR_FOCUS()
					ENDIF
					//bSwitchedBombs = FALSE // Lets us drop bombs, son
					
					IF myVariation = AIR_VARIATION_BOMB AND NOT myArgs.bDoUberBombs
						vTemp = GET_ENTITY_COORDS(cargoArgs.oCargoDrop[iCargo])
						
						IF bHitWater
							START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_ojdg4_water_exp", vTemp, <<0,0,0>>, 1.0)
							ODDJOB_PLAY_SOUND("WATER_EXPLOSION", cargoArgs.soundID)
						ELSE
							START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_ojdg4_boat_exp", vTemp, <<0,0,0>>, 3.0)
							ADD_EXPLOSION(vTemp + vOffset01, EXP_TAG_TRUCK, 1.0,FALSE)
							ADD_EXPLOSION(vTemp + vOffset02, EXP_TAG_TRAIN, 1.0,FALSE)
							ADD_EXPLOSION(vTemp + vOffset03, EXP_TAG_TRUCK, 1.0,FALSE)
							ADD_EXPLOSION(vTemp + vOffset04, EXP_TAG_TRAIN, 1.0,FALSE)
							ADD_EXPLOSION(vTemp, EXP_TAG_TRAIN, 1.0,FALSE)
							
							START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", vTemp, <<0,0,0>>, 3.0)
							ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_EXPLOSION_MASTER", cargoArgs.soundID)
						ENDIF
					ENDIF
				
					CHECK_DROP_RESULTS(myArgs, iCargo, iDrop, cargoArgs, myData, vCargoPos, myVariation, myLocationBlip, 
					vCurDestObjective, bDoBomb)
					//checks to see if it's a fail and handles it
					HANDLE_DROP_FAIL(cargoArgs, myData, myVariation, myArgs, iCargo)
					HANDLE_SIDE_UI_EXPLANATION(myArgs)
					
					IF DOES_ENTITY_EXIST(cargoArgs.oCargoDrop[iCargo])
						SET_ENTITY_VISIBLE(cargoArgs.oCargoDrop[iCargo], FALSE)
					ENDIF
				ENDIF
				
				// ------------------------------------MAIN CHECK------------------------------------
			ELIF cargoArgs.bDetached[iCargo]
			
				//TODO take into account player height
				IF (GET_GAME_TIMER() - cargoArgs.cargoTimes[iCargo]) > dropTimeout
												
					// Adding block to prevent cargo drop from never ending
					IF NOT cargoArgs.bCargoHitGround[iCargo]
						cargoArgs.bBombCam = FALSE
						CLEANUP_CAM(cargoArgs, bClearPrints)
						
						//TODO CLEANUP_EXPLOSION_CAM should go here too?
					
						DEBUG_PRINT("TIMED OUT PACKAGE!!!")
						cargoArgs.iNumLanded ++
						DEBUG_PRINT("INCREASING NUM LANDED VIA TIME OUT CHECK")
						cargoArgs.bCargoHitGround[iCargo] = TRUE
						
						cargoArgs.bCargoSuccessful[iCargo] = FALSE
						//cargoArgs.bCargoPerfect[iCargo] = FALSE
						
						//checks to see if it's a fail based on
						HANDLE_DROP_FAIL(cargoArgs, myData, myVariation, myArgs, iCargo)
						
						HANDLE_SIDE_UI_EXPLANATION(myArgs)
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC INIT_BLIPPING(ARGS& myArgs, PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData, BLIP_INDEX &myLocationBlip[], VECTOR &vCurDestObjective[])
	INT idx
	IF myArgs.bDoUberBombs
		IF g_savedGlobals.sTraffickingData.iAirRank >= 5 
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[0])
				myLocationBlip[0] = ADD_BLIP_FOR_ENTITY(cargoArgs.vehFinal[0])
				SET_BLIP_COLOUR(myLocationBlip[0], BLIP_COLOUR_RED)
			ENDIF
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[4])
				myLocationBlip[1] = ADD_BLIP_FOR_ENTITY(cargoArgs.vehFinal[4])
				SET_BLIP_COLOUR(myLocationBlip[1], BLIP_COLOUR_RED)
			ENDIF
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[8])
				myLocationBlip[2] = ADD_BLIP_FOR_ENTITY(cargoArgs.vehFinal[8])
				SET_BLIP_COLOUR(myLocationBlip[2], BLIP_COLOUR_RED)
			ENDIF
		ELSE
			myLocationBlip[0] = ADD_BLIP_FOR_COORD(<<1796.6689, 3287.4692, 41.4058>>)
			SET_BLIP_COLOUR(myLocationBlip[0], BLIP_COLOUR_YELLOW)
			SET_BLIP_SCALE(myLocationBlip[0], 1.0)
		ENDIF
	ENDIF
	IF myArgs.bDoTrain
		REPEAT myData.iNumDrops idx
			myLocationBlip[idx] = ADD_BLIP_FOR_ENTITY(cargoArgs.vehDropoff[idx])
			SET_BLIP_COLOUR(myLocationBlip[idx], BLIP_COLOUR_RED)
			SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[idx], "DTRFAIR_TGT1")
		ENDREPEAT
	ELIF NOT myArgs.bDoUberBombs
		REPEAT myData.iNumDrops idx
			IF NOT DOES_BLIP_EXIST(myLocationBlip[idx])
				myLocationBlip[idx] = ADD_BLIP_FOR_COORD(myData.dropData[idx].vCenterPos)
				IF myArgs.bDoBomb
					SET_BLIP_COLOUR(myLocationBlip[idx], BLIP_COLOUR_RED)
					SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[idx], "DTRFAIR_TGT")
				ELSE
					SET_BLIP_COLOUR(myLocationBlip[idx], BLIP_COLOUR_YELLOW)
					SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[idx], "DTRFAIR_DRP")
				ENDIF
				vCurDestObjective[idx] = myData.dropData[idx].vCenterPos
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL CHECK_FOR_EARLY_TERMINATION(PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData)
	INT iRemainingCargo, iRemainingDrops
	
//	PRINTLN("myData.iNumTotalCargo = ", myData.iNumTotalCargo)
//	PRINTLN("cargoArgs.iNumLanded = ", cargoArgs.iNumLanded)
	
	iRemainingCargo = GET_NUMBER_OF_REMAINING_CARGO(myData.iNumTotalCargo, cargoArgs.iNumLanded)
//	PRINTLN("iRemainingCargo = ", iRemainingCargo)
//	
//	PRINTLN("planeData.iNumDrops = ", myData.iNumDrops)
//	PRINTLN("cargoArgs.iNumSuccessfulDrops = ", cargoArgs.iNumSuccessfulDrops)
	
	iRemainingDrops = (myData.iNumDrops - cargoArgs.iNumSuccessfulDrops)
//	PRINTLN("iRemainingDrops = ", iRemainingDrops)
	
	/* If the number of remaining cargo is less than the total needed to pass - the number successful drops, fail early... 
	because the player can't win at this point*/
	
	IF iRemainingCargo < iRemainingDrops
		PRINTLN("CHECK_FOR_EARLY_TERMINATION RETURNING - TRUE")
		bEarlyFail = TRUE
		RETURN TRUE
	ELSE
//		PRINTLN("CHECK_FOR_EARLY_TERMINATION RETURNING - FALSE")
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC BOOL CARGO_DROPS_COMPLETE(ARGS& myArgs, PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData, DROP_STATE_CHECKS &dropStates, structTimer &tPrintHelp, 
TEXT_LABEL_31& sLastObjective, VECTOR &vCurDestObjective[], BLIP_INDEX &myLocationBlip[], AIR_VARIATION &myVariation, 
BOOL& bTimeExpired, BOOL& bIsHighAlt, BOOL& bDoBomb, PLANE_STATE planeState)
	UNUSED_PARAMETER(bIsHighAlt)
	VECTOR vCargoPos[MAX_NUMBER_PACKAGES]
	INT iCargo, iDrop
	
	SWITCH dropStates
	
		CASE DROP_STATE_CHECKS_INITIALIZE
			DEBUG_PRINT("INSIDE CASE DROP_STATE_CHECKS_INITIALIZE")
			START_TIMER_NOW(tPrintHelp)
			
			//INIT_BILPPING(myArgs, cargoArgs, myData, myLocationBlip, vCurDestObjective)
			
			dropStates = DROP_STATE_CHECKS_RUNNING //DROP_STATE_CHECKS_PRE_RUNNING

		BREAK
		CASE DROP_STATE_CHECKS_RUNNING
			//DEBUG_PRINT("DROP_STATE_CHECKS_RUNNING")
			//UPDATE_UI(cargoArgs, myData, cargoArgs.bDetached, TRUE, myArgs.bDoBomb)
			IF myArgs.bDoTutorial
				UPDATE_TUTORIAL(cargoArgs, vCurDestObjective, planeState)
			ENDIF
			
			//We need to skip over if another bomb dropped..
			//THIS cleans up the bomb follow cam and look back cam
			IF myVariation = AIR_VARIATION_BOMB AND cargoArgs.iCutGameTime <> 0 AND cargoArgs.iCutGameTime <> 1
				PLANE_AUTOPILOT(cargoArgs.myPlane, 20.0, FALSE, 10)
				DEBUG_MESSAGE("PLANE_AUTOPILOT 1")
				
				IF NOT bDoneFlash01
					IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
						IF GET_GAME_TIMER() - cargoArgs.iCutGameTime > 2200 
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
								
							DEBUG_MESSAGE("bDoneFlash01 = TRUE")
							bDoneFlash01 = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_GAME_TIMER() - cargoArgs.iCutGameTime > CAM_TIME 
				OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
//					IF myArgs.bDoUberBombs
//						PRINT_HELP_WITH_NUMBER("DTRFAIR_BLFT1", cargoArgs.iNumPackagesTotal - cargoArgs.iNumReleased)
//					ELSE
//						PRINT_HELP_WITH_NUMBER("DTRFAIR_BLFT", cargoArgs.iNumPackagesTotal - cargoArgs.iNumReleased)
//					ENDIF
					
					DEBUG_MESSAGE("RESETTING: bDoneFlash01 = FALSE")
					bDoneFlash01 = FALSE

					//auto piolot here too? or hold pattern?
					CLEAR_FOCUS()
					CLEANUP_CAM(cargoArgs, bClearPrints)
					DEBUG_MESSAGE("Setting forward speed to 40")
					SET_VEHICLE_FORWARD_SPEED(cargoArgs.myPlane, 40.0)
					cargoArgs.iCutGameTime = 0
				ELIF cargoArgs.iCutGameTimeStart <> 0
				
				ENDIF
			ENDIF

			IF cargoArgs.bBombCam
				//PLANE_BOMB_MODE(cargoArgs, myArgs, bIsHighAlt, vCurDestObjective)
			ENDIF
			
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220418
			
			IF NOT myArgs.bDoTutorial
				UPDATE_DISTANCE_AND_PRINTS(myVariation, cargoArgs, myData, myArgs)
			ENDIF
			IF myArgs.bDoUberBombs AND NOT bUberActive 
				UPDATE_DROP_STATUS(myArgs, cargoArgs, myData, iCargo, iDrop, vCurDestObjective, vCargoPos, myLocationBlip, 
				myVariation, bDoBomb)
				IF g_savedGlobals.sTraffickingData.iAirRank >= 5 
					REMOVE_DISTANT_CONVOY_VEHICLES(cargoArgs)
					ALTERNATE_FINAL_BLIP_REPEATABLE(cargoArgs, myLocationBlip, sLastObjective, bGenericPrint, bOutOfVehicle)
				ELSE
					ALTERNATE_FINAL_BLIP(cargoArgs, myLocationBlip, sLastObjective, bGenericPrint, bOutOfVehicle)
				ENDIF
			ELIF NOT myArgs.bDoUberBombs
				UPDATE_DROP_STATUS(myArgs, cargoArgs, myData, iCargo, iDrop, vCurDestObjective, vCargoPos, myLocationBlip, 
				myVariation, bDoBomb)
			ENDIF
			
//			IF NOT myArgs.bDoTrain AND 
			IF NOT myArgs.bDoMobile AND NOT bOutOfVehicle AND NOT bInActiveFight AND NOT myArgs.bDoUberBombs
				// Change blips from normal to radius
				ALTERNATE_BLIP_STYLES(myArgs, myLocationBlip, vCurDestObjective, myData, cargoArgs, vPlayerPos, bDoBomb)
			ELSE
				IF NOT myArgs.bDoUberBombs
					vReturn = UPDATE_BLIP_PROXIMITY(myLocationBlip, vCurDestObjective, cargoArgs.vehDropoff, FALSE)
					IF NOT ARE_VECTORS_EQUAL(vReturn, <<0,0,0>>)
						IF NOT myArgs.bDoStationary
							vReturn.z += 3.0
							//DRAW_CHECKPOINT(vReturn, 1.5, 0, 255, 0)
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF GET_NUMBER_OF_REMAINING_CARGO(myData.iNumTotalCargo, cargoArgs.iNumLanded) = 0
			OR ARE_DROPS_COMPLETE(cargoArgs, myData)
			OR CHECK_FOR_EARLY_TERMINATION(cargoArgs, myData)
			OR bTimeExpired
			OR bJSkipped
				REMOVE_ALL_BLIPS(cargoArgs, myLocationBlip, vCurDestObjective)
				//CLEANUP_CAM(cargoArgs, bClearPrints)
				DEBUG_PRINT("RETURNING TRUE ON CARGO CHECK")
				
				// to fix bug 1546889 - 7/15/13 - MB
				IF ( DOES_CAM_EXIST(cargoArgs.CamExplosion)	AND IS_CAM_ACTIVE(cargoArgs.CamExplosion) )
				OR ( DOES_CAM_EXIST(cargoArgs.CamTransition)	AND IS_CAM_ACTIVE(cargoArgs.CamTransition) )
					SET_VEHICLE_FORWARD_SPEED(cargoArgs.myPlane, 40.0)
					DEBUG_PRINT("SETTING FORWARD SPEED TO 40")
				ENDIF
				
				bDropPhaseComplete = TRUE
				dropStates = DROP_STATE_CHECKS_FINAL
				SETTIMERA(0)
			ENDIF

		BREAK
		CASE DROP_STATE_CHECKS_FINAL
		
			IF myVariation = AIR_VARIATION_BOMB AND cargoArgs.iCutGameTime <> 0
				
				IF NOT bDoneFlash02
					IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
						IF GET_GAME_TIMER() - cargoArgs.iCutGameTime > 2200 
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
								
							DEBUG_MESSAGE("bDoneFlash02 = TRUE")
							bDoneFlash02 = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_GAME_TIMER() - cargoArgs.iCutGameTime > CAM_TIME //OR IS_COLLISION_IMMINENT_WRAPPER(cargoArgs.myPlane)
					DEBUG_PRINT("SECOND CHECK PASS")
					IF DOES_CAM_EXIST(cargoArgs.CamTransition)
						SET_CAM_ACTIVE(cargoArgs.CamTransition, FALSE)
					ENDIF
					CLEAR_FOCUS()
					CLEANUP_CAM(cargoArgs, bClearPrints)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					cargoArgs.iCutGameTime = 0
				ENDIF
			ENDIF
			
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
			//UPDATE_UI(cargoArgs, myData, cargoArgs.bDetached, TRUE, myArgs.bDoBomb)
			//todo add delay so we can see results
			//This was fucking up my life and cutting the conversation short
			IF TIMERA() > 3000 AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			OR bJSkipped
				IF GET_GAME_TIMER() - cargoArgs.iCutGameTime > 4500 
					DEBUG_MESSAGE("Clear prints asd")
					CLEAR_PRINTS()
					CLEAR_FOCUS()
					cargoArgs.bBombCam = FALSE
					CLEANUP_CAM(cargoArgs, bClearPrints)

					vCurDestObjective[0] = myData.endData.vCenterPos
					RETURN TRUE
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_OFFSET_WIDGETS()

	START_WIDGET_GROUP("DrugDropCrateOffset") 
		DEBUG_PRINT("INSIDE CREATE WIDGETS")
		ADD_WIDGET_FLOAT_SLIDER("crateOffsetX",fCameraOffsetX, -10, 10, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("crateOffsetY",fCameraOffsetY, -10, 10, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("crateOffsetZ",fCameraOffsetZ, -10, 10, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("crateHeadingX",fCameraHeadingX, -10, 10, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("crateHeadingY",fCameraHeadingY, -10, 10, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("crateHeadingZ",fCameraHeadingZ, -10, 10, 0.01)
	STOP_WIDGET_GROUP()

ENDPROC
#ENDIF	//	IS_DEBUG_BUILD

PROC RUN_WIDGETS(VEHICLE_INDEX vehicle, OBJECT_INDEX &oCargoDrop[])
	IF DOES_ENTITY_EXIST(vehicle) 
		DEBUG_PRINT("RUNNING WIDGETS")
		IF NOT IS_ENTITY_DEAD(vehicle)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehicle)
				ATTACH_ENTITY_TO_ENTITY(oCargoDrop[0], vehicle, 0, <<fCameraOffsetX, fCameraOffsetY, fCameraOffsetZ>>, <<fCameraHeadingX, fCameraHeadingY, fCameraHeadingZ>> )
				DEBUG_PRINT("INSIDE ATTACHMENT")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Wraper for all of the checks that tell us a bomb or package has hit something
FUNC BOOL RUN_HAS_HIT_LOGIC(PLANE_CARGO_ARGS &cargoArgs, INT iNum, INT idx)
	VECTOR vTemp

	IF DOES_ENTITY_EXIST(cargoArgs.oClusterDrop[iNum][idx]) AND NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(cargoArgs.oClusterDrop[iNum][idx])
		IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(cargoArgs.oClusterDrop[iNum][idx]) OR GET_ENTITY_SPEED(cargoArgs.oClusterDrop[iNum][idx]) = 0.0 
		OR IS_ENTITY_IN_WATER(cargoArgs.oClusterDrop[iNum][idx]) //OR (TIMERB() > FAIL_SAFE_TIME)
			
			//fix for a single bomb falling through the world and holding up this from completing
			vTemp = GET_ENTITY_COORDS(cargoArgs.oClusterDrop[iNum][idx])
			IF fMinGroundZ > vTemp.z
				//PRINTLN("Setting fMinGroundZ = ", vTemp.z)
				fMinGroundZ = vTemp.z
			ENDIF
			
			//If at least one bomb in the cluster has hit 
			IF ABSF(vTemp.z - fMinGroundZ) > 20.0 AND vTemp.z < fMinGroundZ
				DEBUG_MESSAGE("Fail safe hit")
				ADD_EXPLOSION(vTemp, EXP_TAG_TRAIN, 1.0)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CAMERA_TOO_CLOSE_TO_GROUND(PLANE_CARGO_ARGS &cargoArgs)
	VECTOR vCamPosition
	FLOAT fCameraGroundZ
	
	IF DOES_CAM_EXIST(cargoArgs.CamTransition01) AND IS_CAM_ACTIVE(cargoArgs.CamTransition01)
		vCamPosition = GET_CAM_COORD(cargoArgs.CamTransition01)
//		PRINTLN("vCamPosition = ", vCamPosition)
		
		IF GET_GROUND_Z_FOR_3D_COORD(vCamPosition, fCameraGroundZ)
	//		PRINTLN("fCameraGroundZ = ", fCameraGroundZ)
			
			IF ABSF(vCamPosition.z - fCameraGroundZ) < 5.0
				PRINTLN("WE'RE TOO CLOSE TO THE GROUND, CUT OUT NOW. vCamPosition = ", GET_STRING_FROM_VECTOR(vCamPosition), " and fCameraGroundZ = ", fCameraGroundZ)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CHECK_FOR_UBER_COLLISIONS(PLANE_CARGO_ARGS &cargoArgs, INT iNum, BLIP_INDEX& myLocationBlip[])
	BOOL bAllDropped = TRUE
	BOOL bAllInvalid = TRUE
	INT idx, iTarget
	FLOAT fCheckingDistance
	
	
	REPEAT MAX_NUMBER_CLUSTER idx
		IF DOES_ENTITY_EXIST(cargoArgs.oClusterDrop[iNum][idx])
			bAllInvalid = FALSE
//			DEBUG_MESSAGE("bAllInvalid = FALSE")
			IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(cargoArgs.oClusterDrop[iNum][idx])
//				PRINTLN("Valid bomb that is detached and we are waiting for it to hit something")
		
				SIMULATE_NOSE_TIP_DOWN_UBER(cargoArgs.oClusterDrop[iNum][idx])
			ENDIF
			
			IF (HAS_ENTITY_COLLIDED_WITH_ANYTHING(cargoArgs.oClusterDrop[iNum][idx]))
//				SCRIPT_ASSERT("Prop hit something")
				PRINTLN("Prop hit something")
			ENDIF
			
			IF GET_ENTITY_SPEED(cargoArgs.oClusterDrop[iNum][idx]) = 0 
//				PRINTLN("Prop speed is 0")
			ENDIF
			
			IF TIMERB() > FAIL_SAFE_TIME
				PRINTLN("TIMERB() > FAIL_SAFE_TIME")
			ENDIF
			
			IF RUN_HAS_HIT_LOGIC(cargoArgs, iNum, idx)
//				bOneHit = TRUE
			
				IF NOT bUberSucess
					
					REPEAT 4 iTarget
						IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinalExtra[iTarget])
							FLOAT fTempDist
							fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(cargoArgs.vehFinalExtra[iTarget], cargoArgs.oClusterDrop[iNum][idx])
							IF fTempDist < EXPLODE_RADIUS_CARPET
								EXPLODE_VEHICLE(cargoArgs.vehFinalExtra[iTarget])
							ENDIF
						ENDIF
					ENDREPEAT
				
					//check for drop sucess
					REPEAT MAX_NUMBER_FINAL_TARGETS iTarget
					//checks car for now
						IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[iTarget])
							fCheckingDistance = GET_DISTANCE_BETWEEN_ENTITIES(cargoArgs.vehFinal[iTarget], cargoArgs.oClusterDrop[iNum][idx])
							PRINTLN("fCheckingDistance = ", fCheckingDistance)
							
							IF fCheckingDistance < EXPLODE_RADIUS_CARPET
								bHitATarget = TRUE
								PRINTLN("SETTING: bHitATarget = TRUE")
								
								EXPLODE_VEHICLE(cargoArgs.vehFinal[iTarget])
								myLocationBlip[0] = myLocationBlip[0]
							ENDIF
						ENDIF
						
						IF IS_ENTITY_DEAD(cargoArgs.vehFinal[iTarget]) AND DOES_BLIP_EXIST(cargoArgs.blipFinal[iTarget])
							SCRIPT_REMOVE_BLIP_SAFE(cargoArgs.blipFinal[iTarget])
							PRINTLN("REMOVING BLIP IN UPDATE_CLUSTER_BOMB: ", iTarget)
						ENDIF
					ENDREPEAT
				ENDIF
				
//				IF idx%2 = 0
//					ADD_EXPLOSION(GET_ENTITY_COORDS(cargoArgs.oClusterDrop[iNum][idx]), EXP_TAG_TRAIN, 1.0)
//					PRINTLN("DOING AN EXPLOSION")
//				ELSE
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_train_exp", GET_ENTITY_COORDS(cargoArgs.oClusterDrop[iNum][idx]), <<0,0,0>>, 3.0)
					PRINTLN("DOING PARTICLE EFFECT")
//				ENDIF
				
				IF NOT bTriggerUberMusic
					TRIGGER_MUSIC_EVENT("OJDA5_FIRST_BOMBS")
					PRINTLN("TRIGGERING MUSIC - OJDA5_FIRST_BOMBS")
				
					bTriggerUberMusic = TRUE
				ENDIF
				
				//ADD_EXPLOSION(GET_ENTITY_COORDS(cargoArgs.oClusterDrop[iNum][idx]), EXP_TAG_GAS_CANISTER, 0.5)
				DELETE_OBJECT(cargoArgs.oClusterDrop[iNum][idx])
			ELSE
				bAllDropped = FALSE
//				DEBUG_MESSAGE("bAllDropped = FALSE")
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bAllDropped OR bAllInvalid OR IS_CAMERA_TOO_CLOSE_TO_GROUND(cargoArgs)
		DEBUG_MESSAGE("bAllDropped OR bAllInvalid")
		SETTIMERB(0)
		SCRIPT_REMOVE_BLIP_SAFE(cargoArgs.blipCrateDrop[iNum])
		ODDJOB_STOP_SOUND(cargoArgs.soundID)
		IF bDoUberCam
//			SETTIMERB(0)
			clusterState = CLUSTER_STATE_LAST_CUT
		ELSE
			bUberActive = FALSE
			clusterState = CLUSTER_STATE_DONE
		ENDIF
		
	ENDIF
ENDPROC

PROC CREATE_DUMMY_BOMB(PLANE_CARGO_ARGS &cargoArgs, INT iNum)
	IF DOES_ENTITY_EXIST(cargoArgs.oBombDummy)
		DELETE_OBJECT(cargoArgs.oBombDummy)
		PRINTLN("DELETING DUMMY BOMB")
	ENDIF

	cargoArgs.oBombDummy = CREATE_OBJECT(mnBombProp01, GET_ENTITY_COORDS(cargoArgs.oClusterDrop[iNum][count-1]))
	
	SET_ENTITY_ROTATION(cargoArgs.oBombDummy, <<0, 0, GET_ENTITY_HEADING(cargoArgs.myPlane)+180>>)
	PRINTLN("UBER: The rotation of dummy is ", GET_STRING_FROM_VECTOR(GET_ENTITY_ROTATION(cargoArgs.oBombDummy)))

	PRINTLN("CREATING DUMMY BOMB")
	SET_ENTITY_VISIBLE(cargoArgs.oBombDummy, FALSE)
	SET_ENTITY_COLLISION(cargoArgs.oBombDummy, FALSE)
ENDPROC

PROC HACK_UPDATE_UBER_ANIMS(PLANE_CARGO_ARGS &cargoArgs)
	INT idx, idx2
	REPEAT cargoArgs.iNumPackagesTotal idx
		REPEAT MAX_NUMBER_CLUSTER idx2
			IF DOES_ENTITY_EXIST(cargoArgs.oClusterDrop[idx][idx2]) AND NOT IS_ENTITY_ATTACHED(cargoArgs.oClusterDrop[idx][idx2])
				IF cargoArgs.iDropTime[idx][idx2] <> 0 AND (GET_GAME_TIMER() - cargoArgs.iDropTime[idx][idx2]) > 125
					cargoArgs.iDropTime[idx][idx2] = 0
					PLAY_ENTITY_ANIM(cargoArgs.oClusterDrop[idx][idx2], "BOMB_TAIL_OPEN_UP", "oddjobs@arms_traffic@", SLOW_BLEND_IN, FALSE, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

FUNC BOOL UPDATE_CLUSTER_BOMB(ARGS& myArgs, PLANE_CARGO_ARGS &cargoArgs, INT iNum, PLANE_DATA& myData, BLIP_INDEX& myLocationBlip[])
	//VECTOR velocity
	UNUSED_PARAMETER(myData)
	//FLOAT fTemp
//	VECTOR vTemp
	INT idx
	
	// To Fix Bug # 1558096 - DS
	IF clusterState > CLUSTER_STATE_INIT AND clusterState < CLUSTER_STATE_DONE
		REPEAT MAX_NUMBER_FINAL_TARGETS idx
			IF DOES_ENTITY_EXIST(cargoArgs.vehFinal[idx])
				IF IS_ENTITY_DEAD(cargoArgs.vehFinal[idx])
					SCRIPT_REMOVE_BLIP_SAFE(cargoArgs.blipFinal[idx])
					PRINTLN("FAIL SAFE: REMOVING BLIP IN UPDATE_CLUSTER_BOMB: ", idx)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	SWITCH clusterState
		CASE CLUSTER_STATE_INIT
			DEBUG_MESSAGE("CLUSTER_STATE_INIT")
			//set to large number
			fMinGroundZ = 100000
			cargoArgs.bBombCam = FALSE
			CLEANUP_CAM(cargoArgs, bClearPrints)
			count = 0
			bUberSucess = FALSE
//			bOneHit = FALSE
			DEBUG_MESSAGE("CLUSTER_STATE_INIT")
			RESTART_TIMER_NOW(clusterTimer)
			
			IF bDoUberCam
				PLANE_AUTOPILOT(cargoArgs.myPlane, MIN_SPEED_AUTO-1, FALSE, 0.5)
				DEBUG_MESSAGE("PLANE_AUTOPILOT 2")
				SETUP_CARPET_BOMB_CAM(myArgs, cargoArgs)
				SET_ENTITY_INVINCIBLE(cargoArgs.myPlane, TRUE)
			ENDIF
			
			bHitATarget = FALSE
			PRINTLN("SETTING: bHitATarget = FALSE")
			
			minTime = 0.0
			maxTime = 0.0
		
			clusterState = CLUSTER_STATE_FIRST
		BREAK
		CASE CLUSTER_STATE_FIRST
			RELEASE_CARGO(cargoArgs.oCargoDrop[cargoArgs.iNumPackageDropped-1], cargoArgs.myPlane, cargoArgs.soundID, TRUE, -7.0)
			SET_ENTITY_VISIBLE(cargoArgs.oCargoDrop[cargoArgs.iNumPackageDropped-1], FALSE)	
			HACK_UPDATE_UBER_ANIMS(cargoArgs)
			clusterState = CLUSTER_STATE_DROP
		BREAK
		CASE CLUSTER_STATE_DROP
			DEBUG_MESSAGE("CLUSTER_STATE_DROP")
			IF TIMER_DO_WHEN_READY(clusterTimer, GET_RANDOM_FLOAT_IN_RANGE(minTime, maxTime)) and count <> -1
				RELEASE_CARGO(cargoArgs.oClusterDrop[iNum][count], cargoARgs.myPlane, cargoArgs.soundID, TRUE, -7)
				cargoArgs.iDropTime[iNum][count] = GET_GAME_TIMER()
				HACK_UPDATE_UBER_ANIMS(cargoArgs)
				SETTIMERA(0)
				count++
				RESTART_TIMER_NOW(clusterTimer)
			
				minTime = 0.55
				maxTime = 0.6
			
				IF count >= MAX_NUMBER_CLUSTER
					CREATE_DUMMY_BOMB(cargoArgs, iNum)					
				
					//count = -1
					SETTIMERB(0)
					clusterState = CLUSTER_STATE_CAM
				ELSE
					clusterState = CLUSTER_STATE_DROPB
				ENDIF
			
			ENDIF
		BREAK
		//DROPB state is here in order to release bombs in a pair with a very slight delay
		CASE CLUSTER_STATE_DROPB
			CHECK_FOR_UBER_COLLISIONS(cargoArgs, iNum, myLocationBlip)
			IF TIMERA() > 400 //150
				DEBUG_MESSAGE("RELEASE CARGO OFFSET")
				RELEASE_CARGO(cargoArgs.oClusterDrop[iNum][count], cargoARgs.myPlane, cargoArgs.soundID, TRUE, -7)
				cargoArgs.iDropTime[iNum][count] = GET_GAME_TIMER()
				HACK_UPDATE_UBER_ANIMS(cargoArgs)
				SETTIMERA(0)
				//SET_ENTITY_LOAD_COLLISION_FLAG(cargoArgs.oClusterDrop[iNum][count], TRUE)
				count++
				
				IF count >= MAX_NUMBER_CLUSTER
					CREATE_DUMMY_BOMB(cargoArgs, iNum)					
				
					SETTIMERB(0)
					clusterState = CLUSTER_STATE_CAM
				ELSE
					clusterState = CLUSTER_STATE_DROP
				ENDIF
				
			ENDIF

		BREAK
		CASE CLUSTER_STATE_CAM
			CHECK_FOR_UBER_COLLISIONS(cargoArgs, iNum, myLocationBlip)
			HACK_UPDATE_UBER_ANIMS(cargoArgs)
			
			IF TIMERB() > 800 //200
				//This is needed for the holding pattern stuff
				SETTIMERB(0)
				myData.vPlayerStart = GET_ENTITY_COORDS(cargoArgs.myPlane)
				myData.fPlayerRot = GET_ENTITY_HEADING(cargoArgs.myPlane)
				IF bDoUberCam
					vDummyBombPostion = GET_ENTITY_COORDS(cargoArgs.oClusterDrop[iNum][count-1])
					PRINTLN("REGULAR BOMB POSITION - FIRST FRAME = ", vDummyBombPostion)
					
					SET_ENTITY_COORDS(cargoArgs.oBombDummy, vDummyBombPostion)
					LAST_BOMB_CUT(cargoArgs, cargoArgs.oBombDummy)
				ENDIF
				
//				vOrigOrientation = <<80, 0, GET_ENTITY_HEADING(cargoArgs.myPlane)+180>>
				
				clusterState = CLUSTER_STATE_WAIT
			ENDIF
		BREAK
		CASE CLUSTER_STATE_WAIT
			DEBUG_MESSAGE("CLUSTER_STATE_WAIT")
			HACK_UPDATE_UBER_ANIMS(cargoArgs)
//			fCamFov = (fCamFov + 0.4)
//			PRINTLN("fCamFov = ", fCamFov)
			
//			IF fCamFov > 35.0
//				IF IS_CAM_ACTIVE(cargoArgs.CamTransition)
//					SET_CAM_FOV(cargoArgs.CamTransition, fCamFov)
//				ENDIF
//			ENDIF
			
			IF DOES_ENTITY_EXIST(cargoArgs.oClusterDrop[iNum][count-1])
				vDummyBombPostion = GET_ENTITY_COORDS(cargoArgs.oClusterDrop[iNum][count-1])
				PRINTLN("REGULAR BOMB POSITION = ", vDummyBombPostion)
			ELSE
				PRINTLN("cargoArgs.oClusterDrop DOES NOT EXIST")
			ENDIF
			
			IF NOT IS_VECTOR_ZERO(vDummyBombPostion)
				SET_ENTITY_COORDS(cargoArgs.oBombDummy, vDummyBombPostion)
			ELSE
				PRINTLN("vDummyBombPostion EQUALS ZERO")
			ENDIF
						
			//SIMULATE_NOSE_TIP_DOWN_UBER(cargoArgs.oClusterDrop[iNum][count-1])
			
			//Holding pattern hack... Can't update position once we're in holding pattern
			IF TIMERB() < 1500
				DEBUG_MESSAGE("Storing off posiitons 2")
				myData.vPlayerStart = GET_ENTITY_COORDS(cargoArgs.myPlane)
				myData.fPlayerRot = GET_ENTITY_HEADING(cargoArgs.myPlane)
			ENDIF
			
			CHECK_FOR_UBER_COLLISIONS(cargoArgs, iNum, myLocationBlip)
			
		BREAK
		CASE CLUSTER_STATE_LAST_CUT
//			IF TIMERB() > 150
//				SETTIMERB(0)
				
				fCamFov = 65.0
				
				IF DOES_ENTITY_EXIST(cargoArgs.oClusterDrop[iNum][count-1])
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_drug_grd_plane_exp", GET_ENTITY_COORDS(cargoArgs.oClusterDrop[iNum][count-1]), <<0,0,0>>, 3.0)
					DELETE_OBJECT(cargoArgs.oClusterDrop[iNum][count-1])
				ENDIF
				
				PRINTLN("UPDATE_CLUSTER_BOMB: Cleaning up cluster cam")
				CLEANUP_CLUSTER_CAM(cargoArgs, cargoArgs.oCargoDrop[iNum])
				clusterState = CLUSTER_STATE_DONE
//			ENDIF
		BREAK
		CASE CLUSTER_STATE_DONE
			IF TIMERB() > 1000
				DEBUG_MESSAGE("CLUSTER_STATE_DONE")
				
				SET_ENTITY_INVINCIBLE(cargoArgs.myPlane, FALSE)
				bDoUberCam = FALSE
				bUberActive = FALSE
			ENDIF
		BREAK
	ENDSWITCH
		
	IF bDoUberCam	
		IF clusterState <> CLUSTER_STATE_DONE
			IF clusterState > CLUSTER_STATE_CAM AND clusterState < CLUSTER_STATE_LAST_CUT AND TIMERB() > 2500
				DEBUG_MESSAGE("holding pattern cluster")
				HOLDING_PATTERN(cargoArgs.myPlane, myData.vPlayerStart, myData.fPlayerRot, 3)
			ELSE
				//SET_ENTITY_MAX_SPEED(cargoArgs.myPlane, MIN_SPEED_AUTO)
				PLANE_AUTOPILOT(cargoArgs.myPlane, MIN_SPEED_AUTO-1, FALSE, 2.0)
//				DEBUG_MESSAGE("PLANE_AUTOPILOT 3")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_DROPS_COMPLETE_MUSIC(ARGS& myArgs, BOOL& bDoBomb)
	
	IF g_savedGlobals.sTraffickingData.iAirRank >= 5
		IF bDoBomb
			IF myArgs.bDoTrain
				TRIGGER_MUSIC_EVENT("OJDA4_RETURN")
				PRINTLN("REPEATABLE TRAIN: TRIGGERING MUSIC - OJDA4_RETURN")
				
				PRINTLN("REPEATABLE TRAIN: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
			ELSE
				TRIGGER_MUSIC_EVENT("OJDA3_LAST_ONE")
				PRINTLN("REPEATABLE BOMB: TRIGGERING MUSIC - OJDA3_LAST_ONE")
				
				PRINTLN("REPEATABLE BOMB: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
			ENDIF
		ELSE
			TRIGGER_MUSIC_EVENT("OJDA2_HEAD_BACK")
			PRINTLN("REPEATABLE DROP: TRIGGERING MUSIC - OJDA2_HEAD_BACK")
			
			PRINTLN("REPEATABLE DROP: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		ENDIF		
	ENDIF

	SWITCH g_savedGlobals.sTraffickingData.iAirRank
		CASE 0
			// No music cues
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------
		CASE 1
			TRIGGER_MUSIC_EVENT("OJDA2_HEAD_BACK")
			PRINTLN("TRIGGERING MUSIC - OJDA2_HEAD_BACK")
			
			PRINTLN("HANDLE_DROPS_COMPLETE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------
		CASE 2
			TRIGGER_MUSIC_EVENT("OJDA3_LAST_ONE")
			PRINTLN("TRIGGERING MUSIC - OJDA3_LAST_ONE")
			
			PRINTLN("HANDLE_DROPS_COMPLETE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------
		CASE 3
			TRIGGER_MUSIC_EVENT("OJDA4_RETURN")
			PRINTLN("TRIGGERING MUSIC - OJDA4_RETURN")
			
			PRINTLN("HANDLE_DROPS_COMPLETE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------
		CASE 4
			TRIGGER_MUSIC_EVENT("OJDA5_BASE_DESTROYED")
			PRINTLN("TRIGGERING MUSIC - OJDA5_BASE_DESTROYED")
			
			PRINTLN("HANDLE_DROPS_COMPLETE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL UPDATE_CARGO_DROP(ARGS& myArgs, PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData, DROP_STATE_CHECKS &dropStates, TEXT_LABEL_31& sLastObjective, 
VECTOR &vCurDestObjective[], BLIP_INDEX &myLocationBlip[], AIR_VARIATION myVariation, BOOL& bTimeExpired, BOOL& bIsHighAlt, BOOL& bDoBomb,
SCRIPT_SCALEFORM_UI& helpScaleform, PLANE_STATE planeSate)

	SWITCH cargoArgs.cargoState
		CASE DROP_STATE_HELP
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF myVariation = AIR_VARIATION_LOW_ALT
					PRINT_HELP_WITH_NUMBER("DTRFAIR_HELP_01", CEIL(myData.fmaxHeight))
				ELIF NOT IS_STRING_NULL(sTaxiHelp) AND NOT myArgs.bDoUberBombs
					PRINT_HELP(sTaxiHelp)
				ENDIF
				
				IF myArgs.bDoUberBombs
					SETTIMERB(0)
					cargoArgs.cargoState = DROP_STATE_CONVERSATION
				ELSE
					cargoArgs.cargoState = DROP_STATE_CHECK_DROPS
				ENDIF
			ENDIF
		BREAK
		CASE DROP_STATE_CONVERSATION
			IF g_savedGlobals.sTraffickingData.iAirRank >= 5
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED() OR TIMERB() > 3000
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_CARPA", CONV_PRIORITY_VERY_HIGH)
					cargoArgs.cargoState = DROP_STATE_CHECK_DROPS
				ENDIF
			ELSE
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED() OR TIMERB() > 3000
//					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_CARP", CONV_PRIORITY_VERY_HIGH)
					cargoArgs.cargoState = DROP_STATE_CHECK_DROPS
				ENDIF
			ENDIF
		BREAK
		CASE DROP_STATE_CHECK_DROPS
			//don't let the player drop cargo until he's told how
			IF cargoArgs.bInstructionsPrinted  = TRUE
				UNUSED_PARAMETER(helpScaleform)
				//DRAW_MINIGAME_INSTRUCTIONS(helpScaleform)
				
				//SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
				CHECK_FOR_BUTTON_PRESS(myArgs, cargoArgs, myVariation)
			ENDIF
			IF CARGO_DROPS_COMPLETE(myArgs, cargoArgs, myData, dropStates, cargoArgs.tHelpPrintTimer, sLastObjective, 
			vCurDestObjective, myLocationBlip, myVariation, bTimeExpired, bIsHighAlt, bDoBomb, planeSate)
				
				HANDLE_DROPS_COMPLETE_MUSIC(myArgs, bDoBomb)
			
				cargoArgs.cargoState = DROP_STATE_FINAL
			ENDIF
			
			IF myArgs.bDoUberBombs AND bUberActive
				UPDATE_CLUSTER_BOMB(myArgs, cargoArgs, cargoArgs.iNumPackageDropped-1, myData, myLocationBlip)
			ENDIF
			
			//if we're in lock on or we're in mobile bomb.
			IF NOT myARgs.bDoMobile AND NOT (myVariation = AIR_VARIATION_BOMB AND NOT myArgs.bDoStationary) AND NOT myArgs.bDoTrain //AND NOT myArgs.bDoUberBombs
				// Run updates on the guys at the dropoff locations
				//DEBUG_MESSAGE("Running UPDATE_DROPOFF_STAGING")
				UPDATE_DROPOFF_STAGING(myArgs, cargoArgs, myData, myLocationBlip, vCurDestObjective, myVariation, bDoBomb)
			ENDIF
			
			//cluster bombs have their own drop logic and camera stuff..
			IF NOT myArgs.bDoUberBombs
				//TODO re-use variables
				IF cargoArgs.iCutGameTimeStart <> 0
					IF (GET_GAME_TIMER() - cargoArgs.iCutGameTimeStart) > HOLDING_TIME and NOT bDisableHolding
						//DEBUG_MESSAGE("Other Holding")
						HOLDING_PATTERN(cargoArgs.myPlane, myData.vPlayerStart, myData.fPlayerRot, 3)
					ELSE
						
						//this is the actual auto pilot for the bomb drop
						//SET_ENTITY_MAX_SPEED(cargoArgs.myPlane,15.0)
						
						
						PLANE_AUTOPILOT(cargoArgs.myPlane, 9, FALSE, AUTO_Y_ROT)
						DEBUG_MESSAGE("PLANE_AUTOPILOT 4")
						//save off the positions
						//SET_VEHICLE_FORWARD_SPEED(cargoArgs.myPlane, 40.0)
						IF NOT IS_ENTITY_DEAD(cargoArgs.myPlane) //AND NOT IS_ENTITY_DEAD()
							DEBUG_MESSAGE("Storing off posiitons 1")
							myData.vPlayerStart = GET_ENTITY_COORDS(cargoArgs.myPlane)
							myData.fPlayerRot = GET_ENTITY_HEADING(cargoArgs.myPlane)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE DROP_STATE_FINAL	
			SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC HANDLE_MUSIC_START(BOOL& bDoBomb, ARGS& myArgs)
	
	IF g_savedGlobals.sTraffickingData.iAirRank >= 5
		IF bDoBomb
			IF myArgs.bDoTrain
				TRIGGER_MUSIC_EVENT("OJDA4_START")
				PRINTLN("REPEATABLE TRAIN: TRIGGERING MUSIC - OJDA4_START")
				
				PRINTLN("REPEATABLE TRAIN: HANDLE_MUSIC_START: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
			ELSE
				TRIGGER_MUSIC_EVENT("OJDA3_START")
				PRINTLN("REPEATABLE BOMB: TRIGGERING MUSIC - OJDA3_START")
				
				PRINTLN("REPEATABLE BOMB: HANDLE_MUSIC_START: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
			ENDIF
		ELSE
			TRIGGER_MUSIC_EVENT("OJDA2_START")
			PRINTLN("REPEATABLE DROP: TRIGGERING MUSIC - OJDA2_START")
			
			PRINTLN("REPEATABLE DROP: HANDLE_MUSIC_START: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		ENDIF
	ENDIF
	
	SWITCH g_savedGlobals.sTraffickingData.iAirRank
		CASE 0
			TRIGGER_MUSIC_EVENT("OJDA1_START")
			PRINTLN("TRIGGERING MUSIC - OJDA1_START")
			
			PRINTLN("HANDLE_MUSIC_START: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
		//-----------------------------------------------------------------------------------------------------------------------------
		CASE 1
			TRIGGER_MUSIC_EVENT("OJDA2_START")
			PRINTLN("TRIGGERING MUSIC - OJDA2_START")
			
			PRINTLN("HANDLE_MUSIC_START: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
		//-----------------------------------------------------------------------------------------------------------------------------
		CASE 2
			TRIGGER_MUSIC_EVENT("OJDA3_START")
			PRINTLN("TRIGGERING MUSIC - OJDA3_START")
			
			PRINTLN("HANDLE_MUSIC_START: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
		//-----------------------------------------------------------------------------------------------------------------------------
		CASE 3
			TRIGGER_MUSIC_EVENT("OJDA4_START")
			PRINTLN("TRIGGERING MUSIC - OJDA4_START")
			
			PRINTLN("HANDLE_MUSIC_START: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
		//-----------------------------------------------------------------------------------------------------------------------------
		CASE 4
			TRIGGER_MUSIC_EVENT("OJDA5_START")
			PRINTLN("TRIGGERING MUSIC - OJDA5_START")
			
			PRINTLN("HANDLE_MUSIC_START: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
	ENDSWITCH

ENDPROC

PROC HANDLE_AIRBORNE_MUSIC(PLANE_DATA& myData, ARGS& myArgs, BOOL& bDoBomb)

	IF NOT bTriggerInAirMusic
		IF NOT IS_ENTITY_DEAD(myData.myPlane)
			IF IS_ENTITY_IN_AIR(myData.myPlane)
				
				IF g_savedGlobals.sTraffickingData.iAirRank >= 5
					IF bDoBomb
						IF myArgs.bDoTrain
							TRIGGER_MUSIC_EVENT("OJDA4_AIRBORNE")
							PRINTLN("REPEATABLE TRAIN: HANDLE_AIRBORNE_MUSIC - OJDA4_AIRBORNE")
							
							PRINTLN("REPEATABLE TRAIN: HANDLE_AIRBORNE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
							bTriggerInAirMusic = TRUE
						ELSE
							TRIGGER_MUSIC_EVENT("OJDA3_AIRBORNE")
							PRINTLN("REPEATABLE BOMB: HANDLE_AIRBORNE_MUSIC - OJDA3_AIRBORNE")
							
							PRINTLN("REPEATABLE BOMB: HANDLE_AIRBORNE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
							bTriggerInAirMusic = TRUE
						ENDIF
					ELSE
						TRIGGER_MUSIC_EVENT("OJDA2_AIRBORNE")
						PRINTLN("REPEATABLE DROP: HANDLE_AIRBORNE_MUSIC - OJDA2_AIRBORNE")
						
						PRINTLN("REPEATABLE DROP: HANDLE_AIRBORNE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
						bTriggerInAirMusic = TRUE
					ENDIF
				ENDIF
				
				SWITCH g_savedGlobals.sTraffickingData.iAirRank
					CASE 0
						TRIGGER_MUSIC_EVENT("OJDA1_AIRBORNE")
						PRINTLN("TRIGGERING MUSIC - OJDA1_AIRBORNE")
						
						PRINTLN("HANDLE_AIRBORNE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
						bTriggerInAirMusic = TRUE
					BREAK
					//----------------------------------------------------------------------------------------------------------------------------
					CASE 1
						TRIGGER_MUSIC_EVENT("OJDA2_AIRBORNE")
						PRINTLN("TRIGGERING MUSIC - OJDA2_AIRBORNE")
						
						PRINTLN("HANDLE_AIRBORNE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
						bTriggerInAirMusic = TRUE
					BREAK
					//----------------------------------------------------------------------------------------------------------------------------
					CASE 2
						TRIGGER_MUSIC_EVENT("OJDA3_AIRBORNE")
						PRINTLN("TRIGGERING MUSIC - OJDA3_AIRBORNE")
						
						PRINTLN("HANDLE_AIRBORNE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
						bTriggerInAirMusic = TRUE
					BREAK
					//----------------------------------------------------------------------------------------------------------------------------
					CASE 3
						TRIGGER_MUSIC_EVENT("OJDA4_AIRBORNE")
						PRINTLN("TRIGGERING MUSIC - OJDA4_AIRBORNE")
						
						PRINTLN("HANDLE_AIRBORNE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
						bTriggerInAirMusic = TRUE
					BREAK
					//----------------------------------------------------------------------------------------------------------------------------
					CASE 4
						TRIGGER_MUSIC_EVENT("OJDA5_AIRBORNE")
						PRINTLN("TRIGGERING MUSIC - OJDA5_AIRBORNE")
						
						PRINTLN("HANDLE_AIRBORNE_MUSIC: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
						bTriggerInAirMusic = TRUE
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_INTRO_CAMERA(PLANE_CARGO_ARGS &cargoArgs)
	
	IF NOT bIntroFlash
		IF DOES_CAM_EXIST(camIntro)
			IF NOT IS_ENTITY_DEAD(cargoArgs.myPlane)
				IF VDIST(<<2131.70752, 4790.60596, 39.97022>>, GET_ENTITY_COORDS(cargoArgs.myPlane)) > 20
				
					SETTIMERA(0)
					
					IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						
						PRINTLN("bIntroFlash = TRUE")
						bIntroFlash = TRUE
					ELSE
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						DESTROY_CAM(camIntro)
						PRINTLN("NORMAL: DESTROYING - camIntro")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bIntroFlash
		IF TIMERA() > 300
			IF DOES_CAM_EXIST(camIntro)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				DESTROY_CAM(camIntro)
				PRINTLN("FIRST PERSON: DESTROYING - camIntro")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL FINISH_INTRO_CAMERA_CUT(PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData)
	
	SWITCH iIntroCutStages
		CASE 0
			IF NOT DOES_CAM_EXIST(camIntro)
				
				IF bReplaying
					END_REPLAY_SETUP()
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(cargoArgs.myPlane)
					SET_VEHICLE_FIXED(cargoArgs.myPlane)
				ENDIF
				
				DO_SCREEN_FADE_IN(500)
	
				camIntro = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2128.3669, 4834.3740, 43.0187>>, <<0.4794, 0.0000, -174.1694>>, 36.5100, TRUE)
				POINT_CAM_AT_ENTITY(camIntro, myData.myPlane, <<0,0,0>>)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_CAM_CONTROLS_MINI_MAP_HEADING(camIntro, TRUE)
				
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<2137.120361,4799.970215,39.678535>>, <<2116.639893,4790.416992,45.452946>>, 25.0, <<2140.62866, 4816.91357, 40.22365>>, 193.1398)
				CLEAR_AREA_OF_VEHICLES(<< 2035.9138, 4765.7661, 40.0606 >>, 200, TRUE, FALSE)
				
				PRINTLN("iIntroCutStages = 1")
				iIntroCutStages = 1
			ENDIF
		BREAK
		CASE 1
			REMOVE_INTRO_CAMERA(cargoArgs)
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_LAST_CONVO()

	SWITCH g_savedGlobals.sTraffickingData.iAirRank
		CASE 0
			IF IS_TIMER_STARTED(tLastConvoTimer)
				IF GET_TIMER_IN_SECONDS(tLastConvoTimer) > 5.0
					IF NOT bRanLastConvo
						IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_CHAT1", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("RANK: ", g_savedGlobals.sTraffickingData.iAirRank)
							PRINTLN("bRanLastConvo = TRUE")
							bRanLastConvo = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_TIMER_STARTED(tLastConvoTimer)
				IF GET_TIMER_IN_SECONDS(tLastConvoTimer) > 15.0
					IF NOT bRanLastConvo
						IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_CHAT2", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("RANK: ", g_savedGlobals.sTraffickingData.iAirRank)
							PRINTLN("bRanLastConvo = TRUE")
							bRanLastConvo = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_TIMER_STARTED(tLastConvoTimer)
				IF GET_TIMER_IN_SECONDS(tLastConvoTimer) > 3.0
					IF NOT bRanLastConvo
						IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_CHAT3", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("RANK: ", g_savedGlobals.sTraffickingData.iAirRank)
							PRINTLN("bRanLastConvo = TRUE")
							bRanLastConvo = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF IS_TIMER_STARTED(tLastConvoTimer)
				IF GET_TIMER_IN_SECONDS(tLastConvoTimer) > 15.0
					IF NOT bRanLastConvo
						IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_CHAT4", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("RANK: ", g_savedGlobals.sTraffickingData.iAirRank)
							PRINTLN("bRanLastConvo = TRUE")
							bRanLastConvo = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF IS_TIMER_STARTED(tLastConvoTimer)
				IF GET_TIMER_IN_SECONDS(tLastConvoTimer) > 3.0
					IF NOT bRanLastConvo
						IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_CHAT5", CONV_PRIORITY_VERY_HIGH)
							PRINTLN("RANK: ", g_savedGlobals.sTraffickingData.iAirRank)
							PRINTLN("bRanLastConvo = TRUE")
							bRanLastConvo = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC HANDLE_REPEATABLE_INTRO_DIALOGUE(ARGS& myArgs)
	INT iRandConvo	

	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), "TREVOR")
	
	IF myArgs.bDoBomb
		IF bUsingMexicans
			PRINTLN("REPEATABLE MEXICANS")
			
			iRandConvo = GET_RANDOM_INT_IN_RANGE() % 3
			IF iRandConvo = 0
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIRB", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE BOMB: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 1
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIRB2", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE BOMB: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 2
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIRB3", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE BOMB: iRandConvo = ", iRandConvo)
			ENDIF	
		ELIF bUsingMarabunta
			PRINTLN("REPEATABLE MARABUNTA")
		
			iRandConvo = GET_RANDOM_INT_IN_RANGE() % 3
			IF iRandConvo = 0
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_ELSAL1", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE BOMB: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 1
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_ELSAL2", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE BOMB: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 2
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_ELSAL3", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE BOMB: iRandConvo = ", iRandConvo)
			ENDIF	
		ELIF bUsingHillbillies
			PRINTLN("REPEATABLE HILLBILLIES")
		
			iRandConvo = GET_RANDOM_INT_IN_RANGE() % 3
			IF iRandConvo = 0
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_HILLB1", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE BOMB: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 1
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_HILLB2", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE BOMB: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 2
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_HILLB3", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE BOMB: iRandConvo = ", iRandConvo)
			ENDIF	
		ENDIF
	ELIF myArgs.bDoTrain
		IF bUsingMexicans
			PRINTLN("REPEATABLE MEXICANS")
			
			iRandConvo = GET_RANDOM_INT_IN_RANGE() % 3
			IF iRandConvo = 0
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIRT", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE TRAIN: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 1
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIRT2", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE TRAIN: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 2
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_AIRT3", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE TRAIN: iRandConvo = ", iRandConvo)
			ENDIF	
		ELIF bUsingMarabunta
			PRINTLN("REPEATABLE MARABUNTA")
		
			iRandConvo = GET_RANDOM_INT_IN_RANGE() % 3
			IF iRandConvo = 0
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_ELSALT1", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE TRAIN: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 1
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_ELSALT2", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE TRAIN: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 2
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_ELSALT3", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE TRAIN: iRandConvo = ", iRandConvo)
			ENDIF	
		ELIF bUsingHillbillies
			PRINTLN("REPEATABLE HILLBILLIES")
		
			iRandConvo = GET_RANDOM_INT_IN_RANGE() % 3
			IF iRandConvo = 0
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_HILLBT1", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE TRAIN: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 1
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_HILLBT2", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE TRAIN: iRandConvo = ", iRandConvo)
			ELIF iRandConvo = 2
				CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_HILLBT3", CONV_PRIORITY_VERY_HIGH)
				PRINTLN("REPEATABLE TRAIN: iRandConvo = ", iRandConvo)
			ENDIF	
		ENDIF
	ELSE
		IF NOT IS_STRING_EMPTY(myArgs.sConversation)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), "TREVOR")
			CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", myArgs.sConversation, CONV_PRIORITY_VERY_HIGH)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    There are certain updates we need to do for targets, so that they don't do things like fall through the world.
PROC HANDLE_AIR_TARGETS(PLANE_CARGO_ARGS &cargoArgs)
	IF cargoArgs.bTraf4_FreezeBoats
		INT idx
		REPEAT 4 idx
			IF NOT cargoArgs.bVehExtraFrozen[idx] 
			AND	DOES_ENTITY_EXIST(cargoArgs.vehFinalExtra[idx]) AND IS_ENTITY_DEAD(cargoArgs.vehFinalExtra[idx])
				FREEZE_ENTITY_POSITION(cargoArgs.vehFinalExtra[idx], TRUE)
				cargoArgs.bVehExtraFrozen[idx] = TRUE
			ENDIF
		ENDREPEAT
		
		// This freezes the boat, and not the train.
		IF NOT cargoArgs.bVehDropoffFrozen[1] 
		AND DOES_ENTITY_EXIST(cargoArgs.vehDropoff[1]) AND IS_ENTITY_DEAD(cargoArgs.vehDropoff[1])
			FREEZE_ENTITY_POSITION(cargoArgs.vehDropoff[1], TRUE)
			cargoArgs.bVehDropoffFrozen[1] = TRUE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL UPDATE_PLANE(ARGS& myArgs, PLANE_STATE& myState, BLIP_INDEX& myVehicleBlip, PLANE_DATA& myData, TEXT_LABEL_31& sLastObjective, 
VECTOR& vCurDestObjective[], BLIP_INDEX& myLocationBlip[], PLANE_CARGO_ARGS &cargoArgs, DROP_STATE_CHECKS &dropStates, 
BOOL& bTimeExpired, BOOL& bIsHighAlt, BOOL& bDoBomb, SCRIPT_SCALEFORM_UI& helpScaleform, CustomCutsceneCaller fpAirTaxiCutscene, 
CUTSCENE_ARGS& cutArgs, CAMERA_INDEX& myCamera,
AIR_VARIATION myVariation = AIR_VARIATION_SIMPLE)

	BOOL bDropComplete
//	FLOAT fPositionInRecording
	//INT iTotalCash
	
//	IF NOT IS_ENTITY_DEAD(myVehicle) AND IS_VEHICLE_DRIVEABLE(myVehicle)
//		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(myVehicle)
//			fPositionInRecording = GET_POSITION_IN_RECORDING(myVehicle)
//			PRINTLN("fPositionInRecording = ", fPositionInRecording)
//			IF fPositionInRecording > 75.0
//				STOP_PLAYBACK_RECORDED_VEHICLE(myVehicle)
//			ENDIF
//		ENDIF
//	ENDIF

	SWITCH myState
		CASE PLANE_FADE_INTRO
			IF FINISH_INTRO_CAMERA_CUT(cargoArgs, myData)
				SETTIMERA(0)
				DEBUG_MESSAGE("PLANE_STATE_PRINTS")
				myState = PLANE_STATE_PRINTS
			ENDIF
		
			fpAirTaxiCutscene = fpAirTaxiCutscene
			UNUSED_PARAMETER(cutArgs)
			UNUSED_PARAMETER(myCamera)	
			
			myState = PLANE_FADE_WAIT
		BREAK
		CASE PLANE_FADE_WAIT
			IF NOT IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
				myState = PLANE_STATE_TAXI
			ENDIF
		BREAK
		//=====================================================================================================================================
		CASE PLANE_STATE_TAXI
//			IF IS_REPLAY_BEING_SET_UP()
//				BREAK
//			ENDIF
//			
			cargoArgs.myPlane = myData.myPlane
			DEBUG_MESSAGE("PLANE_STATE_TAXI")
			RESTART_TIMER_NOW(cargoArgs.tTotalTime)
						
			#IF IS_DEBUG_BUILD
				IF pSkipping
					DO_SCREEN_FADE_IN(500)
				ENDIF
			#ENDIF
				
			//carpet bombs
			IF myArgs.bDoTrain
				sTaxiHelp = "DTRFAIR_TRAIN"
			ELIF myARgs.bDoUberBombs
				sTaxiHelp = "DTRFAIR_FINAL"
			// If we're doing mobile lock on, print help explaining mode.
			ELIF myArgs.bDoMobile
				sTaxiHelp = "DTRFAIR_HELPG"
			// If we're doing bomb stationary, print help explaining mode.
			ELIF myArgs.bDoStationary
				sTaxiHelp = "DTRFAIR_HELPH"
			// If we're doing bomb that's not stationary, print help explaining mode.
			ELIF myArgs.bDoBomb AND NOT myArgs.bDoStationary
				sTaxiHelp = "DTRFAIR_HELPI"
			// Otherwise, print nothing... levels 1-4.
			ELSE
				sTaxiHelp = ""
			ENDIF
			
			HANDLE_MUSIC_START(bDoBomb, myArgs)
			
			IF g_savedGlobals.sTraffickingData.iAirRank >= 5
				KILL_ANY_CONVERSATION()
				HANDLE_REPEATABLE_INTRO_DIALOGUE(myArgs)
			ELSE
				IF NOT IS_STRING_EMPTY(myArgs.sConversation)
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), "TREVOR")
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", myArgs.sConversation, CONV_PRIORITY_VERY_HIGH)
				ENDIF
			ENDIF
			
			DEBUG_MESSAGE("PLANE_STATE_INTRO")
			myState = PLANE_STATE_PRINTS
		BREAK
		CASE PLANE_STATE_PRINTS
//			DEBUG_MESSAGE("INSIDE - PLANE_STATE_PRINTS")
			
			CREATE_FLARES_AT_DROP_SITES(cargoArgs, myData, myArgs)
			
			REMOVE_INTRO_CAMERA(cargoArgs)
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR TIMERA() > 25000
				SWITCH myVariation
					CASE AIR_VARIATION_SIMPLE
						PRINT("DTRSHRD_PLN_01",DEFAULT_GOD_TEXT_TIME,1)
						//REGISTER_PRINT_DELAY()
						SCRIPT_REMOVE_BLIP_SAFE(myVehicleBlip)
						UNUSED_PARAMETER(sLastObjective)
						sLastObjective = "DTRSHRD_PLN_01"
						vCurDestObjective[0] = myData.endData.vCenterPos
						myLocationBlip[0] = ADD_BLIP_FOR_COORD(myData.endData.vCenterPos)
						SET_BLIP_SCALE(myLocationBlip[0], 1.0)
					BREAK
					CASE AIR_VARIATION_DROP
					CASE AIR_VARIATION_LOW_ALT
						SCRIPT_REMOVE_BLIP_SAFE(myVehicleBlip)
							
						IF cargoArgs.bDoTimed
							PRINT("DTRFAIR_OBJ_T", DEFAULT_GOD_TEXT_TIME, 1)
							sLastObjective = "DTRFAIR_OBJ_T"
						ELIF myArgs.bDoMobile
							PRINT("DTRFAIR_OBJ_M", DEFAULT_GOD_TEXT_TIME, 1)
							sLastObjective = "DTRFAIR_OBJ_M"
						ELSE
							PRINT("DTRFAIR_OBJ", DEFAULT_GOD_TEXT_TIME, 1)
							sLastObjective = "DTRFAIR_OBJ"
						ENDIF
					BREAK
					CASE AIR_VARIATION_BOMB
						SCRIPT_REMOVE_BLIP_SAFE(myVehicleBlip)
						
						IF myArgs.bDoUberBombs
							IF g_savedGlobals.sTraffickingData.iAirRank >= 5
								PRINT("DTRFAIR_OBJCF", DEFAULT_GOD_TEXT_TIME, 1)
								sLastObjective = "DTRFAIR_OBJCF"
							ELSE
								PRINT("DTRFAIR_OBJBF", DEFAULT_GOD_TEXT_TIME, 1)
								sLastObjective = "DTRFAIR_OBJBF"
							ENDIF
						ELIF myArgs.bDoTrain
							PRINT("DTRFAIR_OBJT", DEFAULT_GOD_TEXT_TIME, 1)
							sLastObjective = "DTRFAIR_OBJT"
						ELSE
							PRINT("DTRFAIR_OBJB", DEFAULT_GOD_TEXT_TIME, 1)
							sLastObjective = "DTRFAIR_OBJB"
						ENDIF
						
					BREAK
				ENDSWITCH	
				
				INIT_BLIPPING(myArgs, cargoArgs, myData, myLocationBlip, vCurDestObjective)
				UPDATE_UI(cargoArgs, myData, myArgs.bDoBomb)
				IF myVariation <> AIR_VARIATION_BOMB AND NOT myArgs.bDoMobile AND NOT bOutOfVehicle AND NOT bInActiveFight AND NOT myArgs.bDoUberBombs
					// Change blips from normal to radius
					ALTERNATE_BLIP_STYLES(myArgs, myLocationBlip, vCurDestObjective, myData, cargoArgs, vPlayerPos, bDoBomb)
					PRINTLN("USING ALTERNATE_BLIP_STYLES")
				ELSE
					IF NOT myArgs.bDoUberBombs
						vReturn = UPDATE_BLIP_PROXIMITY(myLocationBlip, vCurDestObjective, cargoArgs.vehDropoff)
						IF NOT ARE_VECTORS_EQUAL(vReturn, <<0,0,0>>)
							IF NOT myArgs.bDoStationary
								vReturn.z += 3.0
								//DRAW_CHECKPOINT(vReturn, 1.5, 0, 255, 0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				myState = PLANE_STATE_DROPOFF
			ELSE
//				DEBUG_MESSAGE("INSIDE - PLANE_STATE_PRINTS - Still running a convo.")
			ENDIF
		BREAK
		CASE PLANE_STATE_DROPOFF
		
			REMOVE_INTRO_CAMERA(cargoArgs)
		
//			DEBUG_MESSAGE("INSIDE - PLANE_STATE_DROPOFF")
			
//			STRING tempLine
			bDropComplete = TRUE
			IF bInActiveFight
				bDropComplete = FALSE
			ENDIF
			
			// Update our targets.
			IF myArgs.bDoTrain
				HANDLE_AIR_TARGETS(cargoArgs)
			ENDIF
			
			// Airborne music cues
			HANDLE_AIRBORNE_MUSIC(myData, myArgs, bDoBomb)
						
			SWITCH myVariation
				CASE AIR_VARIATION_SIMPLE
					IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myData.endData.vCenterPos, <<15,15,15>>, TRUE) //OR bOutOfVehicle
						bDropComplete = FALSE
					ENDIF
				BREAK
				CASE AIR_VARIATION_DROP
				CASE AIR_VARIATION_LOW_ALT
				CASE AIR_VARIATION_BOMB
					
					IF NOT UPDATE_CARGO_DROP(MyArgs, cargoArgs, myData, dropStates, sLastObjective, vCurDestObjective, 
					myLocationBlip, myVariation, bTimeExpired, bIsHighAlt, bDoBomb, helpScaleform, myState)
						bDropComplete = FALSE
					ENDIF
				BREAK
			ENDSWITCH
			
			IF bDropComplete 
				IF NOT IS_ENTITY_DEAD(myData.myPlane)
					IF bBombBayDoorsOpen
						CLOSE_BOMB_BAY_DOORS(myData.myPlane)
						bBombBayDoorsOpen = FALSE
						PRINTLN("bBombBayDoorsOpen = FALSE - 03")
						ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BAY_DOOR_OPEN_MASTER", cargoArgs.soundID, FALSE, myData.myPlane)
						PRINTLN("CLOSING BOMB BAY DOORS - 03")
					ENDIF
				ENDIF
				IF IS_HELP_MESSAGE_ON_SCREEN()
					CLEAR_HELP(TRUE)
					PRINTLN("CH - 07")
				ENDIF
//				ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BAY_DOOR_OPEN_MASTER", cargoArgs.soundID)
				
				STOP_ALL_PARTICLES(cargoArgs, myData)
				ODDJOB_STOP_SOUND(cargoArgs.soundID)
				ODDJOB_STOP_SOUND(soundIdAlarm)
				ODDJOB_STOP_SOUND(testSoundID)
				CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, <<2134.0632, 4780.6826, 39.9702>>)
				
				IF IS_AUDIO_SCENE_ACTIVE("SIREN_SCENE")
					STOP_AUDIO_SCENE("SIREN_SCENE")
					PRINTLN("STOPPING AUDIO SCENE - SIREN_SCENE VIA bDropComplete")
				ENDIF				
				
				//Do we need to land or have we failed?
				
				IF HAS_PLAYER_PASSED(myArgs, cargoArgs)
				OR bJSkipped
					REGISTER_QUICK_DELAY("DTRFAIR_LAND")
					sLastObjective = "DTRFAIR_LAND"
					vCurDestObjective[0] = myData.endData.vCenterPos
					//Need some dialouge
					SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_3, TRUE)
					SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
					IF NOT IS_TIMER_STARTED(tLastConvoTimer)
						START_TIMER_NOW(tLastConvoTimer)
						PRINTLN("STARTING TIMER - tLastConvoTimer")
					ENDIF
					myState = PLANE_STATE_LAND
				ELSE
					myState = PLANE_STATE_FAILED
				ENDIF
			ENDIF
		BREAK
		CASE PLANE_STATE_LAND
//			DEBUG_MESSAGE("INSIDE - PLANE_STATE_LAND")
			
			CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, <<2134.0632, 4780.6826, 39.9702>>)
			ALTERNATE_LANDING_BLIP(myLocationBlip, cargoArgs.myPlane, vNewEndingBlip, bOutOfVehicle, vPlayerPos, bCloseRange)
			
			HANDLE_LAST_CONVO()
			
			IF myArgs.bDoTutorial
				UPDATE_TUTORIAL(cargoArgs, vCurDestObjective, myState)
			ELIF bLandingReminder
				IF DOES_ENTITY_EXIST(cargoArgs.myPlane) AND NOT IS_ENTITY_DEAD(cargoArgs.myPlane)
					IF GET_LANDING_GEAR_STATE(cargoArgs.myPlane) = LGS_LOCKED_UP AND GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vCurDestObjective[0], FALSE) < CONST_FIRST_PRINT_DIST_TUT
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							bLandingReminder = FALSE
							PRINT_HELP("DTRFAIR_TUTO4")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Don't show elimination hud for now
			//UPDATE_UI(cargoArgs, myData, cargoArgs.bDetached, FALSE, myArgs.bDoBomb)
			
			IF HAS_PLANE_LANDED(myData.endData.vCenterPos, cargoArgs.myPlane, myData.endData.vCenterPos)
			
				RELEASE_SCRIPT_AUDIO_BANK()
				
				SET_VEHICLE_ENGINE_ON(cargoArgs.myPlane, FALSE, TRUE)
				DEBUG_PRINT("TURNING OFF PLANE ENGINE")
				SET_HELI_BLADES_SPEED(cargoArgs.myPlane, 0.2)
				DEBUG_PRINT("SETTING BLADE SPEED = 0.2")
			
				DEBUG_PRINT("PLANE HAS LANDED")
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				
				PRINTLN("GOING TO STATE - PLANE_STATE_CUTSCENE")
				myState =  PLANE_STATE_CUTSCENE
			ENDIF
		BREAK
		
//		CASE PLANE_STATE_CUTSCENE_PREPARE
//			
//			PRINTLN("GOING TO STATE - PLANE_STATE_CUTSCENE")
//			myState =  PLANE_STATE_CUTSCENE
//		BREAK

		CASE PLANE_STATE_CUTSCENE
//			IF CALL fpEndingCutscene()

//			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(cargoArgs.myPlane)
//				TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), cargoArgs.myPlane)
//				PRINTLN("TASKING PLAYER TO LEAVE VEHICLE")
//			ENDIF

			CLEAR_PRINTS()
			CLEAR_HELP()
			KILL_ANY_CONVERSATION()
		
			bPassed = TRUE
			DEBUG_PRINT("CUTSCENE COMPLETE")
			SCRIPT_REMOVE_BLIP_SAFE(myLocationBlip[0])
			SCRIPT_REMOVE_BLIP_SAFE(myLocationBlip[1])
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				PRINTLN("TURNING BACK ON VISIBILTY ON THE PLAYER")
			ENDIF
			
			myState = PLANE_STATE_FINAL //DONE
		BREAK
		CASE PLANE_STATE_FAILED
			// No longer displaying "Mission Failed" screen... just saying why you failed in the retry screen.
		
//			IF DISPLAY_ENDING_UI(cargoArgs, myData, fBonusT, cargoUI, myArgs, FALSE, FALSE)
//				myState = PLANE_STATE_FINAL
//			ENDIF
		BREAK
		CASE PLANE_STATE_FINAL
			RETURN TRUE
		BREAK

		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC
