
USING "Drug_Trafficking_Data.sch"
USING "Drug_Trafficking_core.sch"
USING "commands_water.sch"

CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

ENUM CAR_CHASE
	CAR_CHASE_INIT = 0,
	CAR_CHASE_TRIGGER,
	CAR_CHASE_SPAWN_CARS, 
	CAR_CHASE_CHASE,
	CAR_CHASE_FINAL,
	CAR_CHASE_COMPLETE
ENDENUM

ENUM CC_SPAWN_UPDATE
	CC_SPAWN_UPDATE_FIRST_WAVE = 0,
	CC_SPAWN_UPDATE_SECOND_WAVE,
	CC_SPAWN_UPDATE_FINAL
ENDENUM

ENUM STAGED_CAR_CHASE
	STAGED_CAR_CHASE_STAGE_01 = 0,
	STAGED_CAR_CHASE_STAGE_02,
	STAGED_CAR_CHASE_STAGE_03,
	STAGED_CAR_CHASE_STAGE_04
ENDENUM

CONST_INT DIST_TO_LOSE_CAR_CHASERS 500
CONST_INT DIST_TO_LOSE_CAR_CHASERS_WHEN_STUCK 150
CONST_INT DISTANCE_TO_LOSE_STAGED_CHASERS 350

CONST_INT iWaveDelay 10
CONST_INT iTimeDelayForIncrease 10


// Car Chase variables
BOOL bWavesDone 
BOOL bWaveActive[MAX_NUMBER_WAVES] 
//BOOL bAllowSpeedIncrease = TRUE
BOOL bCarChaserBehind = FALSE
BOOL bCarChaserUpsideDown = FALSE
BOOL bAIOnLeftSide, bAIOnRightSide
BOOL bPlayAudioLine = FALSE
BOOL bPrintObjective = FALSE
BOOL bHeliTimeExpired = FALSE
BOOL bCloseEnough = FALSE
BOOL bPlayedChaseEndingConvo = FALSE
BOOL bCarChasersLost = FALSE
BOOL bChasersDead = FALSE
BOOL bPlayedChaseDialogue = FALSE
BOOL bTaskedToWander = FALSE

INT iCarChaseUpsideDownVehicleTime

structTimer tCarChaseTimer
structTimer tHeliTimer

// For extra dynamic police
VEHICLE_INDEX vehExtraCop[2]
PED_INDEX pedExtraCop[2]
FLOAT fHeading01, fHeading02, fTimeToLoseHeli
VECTOR vExtraOffset01, vExtraOffset02
BOOL bCopChaseActive = FALSE
structTimer tExtraCopChaseTimer

FUNC BOOL CAR_CHASER_VEHICLE_STUCK(LOCATION_DATA &myLocData, CAR_CHASER_ARGS& carChaserArgs)
	INT idx

	REPEAT myLocData.iNumCarChaserCarsPerWave idx
		IF DOES_ENTITY_EXIST(carChaserArgs.vehCarChasers[idx])
			IF NOT IS_ENTITY_DEAD(carChaserArgs.vehCarChasers[idx])
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), carChaserArgs.vehCarChasers[idx]) > DIST_TO_LOSE_CAR_CHASERS_WHEN_STUCK 
					FLOAT fVehicleChasersSpeed = GET_ENTITY_SPEED(carChaserArgs.vehCarChasers[idx])
//					PRINTLN("fVehicleChasersSpeed = ", fVehicleChasersSpeed)
					
					IF fVehicleChasersSpeed < 5.0
						IF NOT IS_TIMER_STARTED(carChaserArgs.tCarChaserStuckTimer)
							START_TIMER_NOW(carChaserArgs.tCarChaserStuckTimer)
						ELSE
							carChaserArgs.fStuckTime = GET_TIMER_IN_SECONDS(carChaserArgs.tCarChaserStuckTimer)
							IF carChaserArgs.fStuckTime > 10.0
								RETURN TRUE
								PRINTLN("RETURNING TRUE ON CAR_CHASER_VEHICLE_STUCK")
							ENDIF
						ENDIF
					ELSE
						carChaserArgs.fStuckTime = 0
//						PRINTLN("SETTING STUCK TIME TO ZERO - 01")
					ENDIF
				ELSE
					carChaserArgs.fStuckTime = 0
//					PRINTLN("SETTING STUCK TIME TO ZERO - 02")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC	

FUNC BOOL SHOULD_LIGHTS_BE_ON()
	IF (GET_CLOCK_HOURS() >= 20) OR (GET_CLOCK_HOURS() < 6)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_CHASE_ENDING_CONVO(structPedsForConversation& MyLocalPedStruct, BOOL& bRunHeliChase, VEHICLE_INDEX& myVehicle, BOOL bRunConvo)
	INT iRandConvo

	IF bRunConvo
		IF NOT IS_MESSAGE_BEING_DISPLAYED()
			IF NOT bPlayedChaseEndingConvo
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(myVehicle)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), myVehicle)
						IF bRunHeliChase AND bHeliTimeExpired
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
							IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_LOSTH", CONV_PRIORITY_VERY_HIGH)//, DO_NOT_DISPLAY_SUBTITLES)
								PRINTLN("PLAYING HELI CHASER LOST CONVO")
								bPlayedChaseEndingConvo = TRUE
							ENDIF
						ELSE
							IF bCarChasersLost
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
								iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
									
								IF iRandConvo = 0
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP02", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bPlayedChaseEndingConvo = TRUE")
										bPlayedChaseEndingConvo = TRUE
									ENDIF
								ELIF iRandConvo = 1
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP03", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bPlayedChaseEndingConvo = TRUE")
										bPlayedChaseEndingConvo = TRUE
									ENDIF
								ELIF iRandConvo = 2
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP04", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bPlayedChaseEndingConvo = TRUE")
										bPlayedChaseEndingConvo = TRUE
									ENDIF
								ELIF iRandConvo = 3
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP05", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bPlayedChaseEndingConvo = TRUE")
										bPlayedChaseEndingConvo = TRUE
									ENDIF
								ELIF iRandConvo = 4
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP06", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bPlayedChaseEndingConvo = TRUE")
										bPlayedChaseEndingConvo = TRUE
									ENDIF
								ELIF iRandConvo = 5
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_GRTP07", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("iRandConvo = ", iRandConvo)
										PRINTLN("bPlayedChaseEndingConvo = TRUE")
										bPlayedChaseEndingConvo = TRUE
									ENDIF
								ENDIF
							ELSE
								IF bChasersDead
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
									iRandConvo = GET_RANDOM_INT_IN_RANGE() % 6
										
									IF iRandConvo = 0
										IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL01", CONV_PRIORITY_VERY_HIGH)
											PRINTLN("iRandConvo = ", iRandConvo)
											PRINTLN("bPlayedChaseEndingConvo = TRUE")
											bPlayedChaseEndingConvo = TRUE
										ENDIF
									ELIF iRandConvo = 1
										IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL02", CONV_PRIORITY_VERY_HIGH)
											PRINTLN("iRandConvo = ", iRandConvo)
											PRINTLN("bPlayedChaseEndingConvo = TRUE")
											bPlayedChaseEndingConvo = TRUE
										ENDIF
									ELIF iRandConvo = 2
										IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL03", CONV_PRIORITY_VERY_HIGH)
											PRINTLN("iRandConvo = ", iRandConvo)
											PRINTLN("bPlayedChaseEndingConvo = TRUE")
											bPlayedChaseEndingConvo = TRUE
										ENDIF
									ELIF iRandConvo = 3
										IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL04", CONV_PRIORITY_VERY_HIGH)
											PRINTLN("iRandConvo = ", iRandConvo)
											PRINTLN("bPlayedChaseEndingConvo = TRUE")
											bPlayedChaseEndingConvo = TRUE
										ENDIF
									ELIF iRandConvo = 4
										IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL05", CONV_PRIORITY_VERY_HIGH)
											PRINTLN("iRandConvo = ", iRandConvo)
											PRINTLN("bPlayedChaseEndingConvo = TRUE")
											bPlayedChaseEndingConvo = TRUE
										ENDIF
									ELIF iRandConvo = 5
										IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_KILL06", CONV_PRIORITY_VERY_HIGH)
											PRINTLN("iRandConvo = ", iRandConvo)
											PRINTLN("bPlayedChaseEndingConvo = TRUE")
											bPlayedChaseEndingConvo = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
//			PRINTLN("UNABLE TO GIVE CAR CHASER LOST CONVO - MESSAGE BEING DISPLAYED")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL REGULAR_CHASE_PASS_CONDITIONS(LOCATION_DATA &myLocData, CAR_CHASER_ARGS &carChaserArgs, BOOL& bWanted, 
BOOL& bDoPoliceChase, BOOL& bRunHeliChase)

	INT idx, iWaveNum, i, j, iStartIndex
	BOOL bPedsOutOfRange = TRUE
	BOOL bPedsDead = TRUE
	BOOL bCarsDead = TRUE
	VEHICLE_INDEX vehToUse
	
//	FLOAT fTempTime, fTempDistance

	REPEAT myLocData.iNumWaves iWaveNum
		
		IF bWaveActive[iWaveNum]
				
			idx += (iWaveNum*myLocData.iNumCarChaserCarsPerWave)
		
			// Remove blips on cars if they are dead or if the peds leave the car
			REPEAT myLocData.iNumCarChaserCarsPerWave idx
				IF DOES_ENTITY_EXIST(carChaserArgs.vehCarChasers[idx])
					IF NOT IS_ENTITY_DEAD(carChaserArgs.vehCarChasers[idx])
					
						IF bRunHeliChase
						
//							fTempDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), carChaserArgs.vehCarChasers[idx])
//							PRINTLN("fTempDistance = ", fTempDistance)
							
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), carChaserArgs.vehCarChasers[idx]) < 90
							OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND 
								HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), carChaserArgs.vehCarChasers[idx]))
//								PRINTLN("CLOSE ENOUGH, START TIMER")
								bCloseEnough = TRUE
							ENDIF
							
							IF bCloseEnough
								IF NOT IS_TIMER_STARTED(tHeliTimer) 
									START_TIMER_NOW(tHeliTimer)
									PRINTLN("STARTING TIMER - tHeliTimer")
								ELSE
									
//									fTempTime = GET_TIMER_IN_SECONDS(tHeliTimer)
//									PRINTLN("fTempTime = ", fTempTime)
//									PRINTLN("fTimeToLoseHeli = ", fTimeToLoseHeli)
									
									IF GET_TIMER_IN_SECONDS(tHeliTimer) > (fTimeToLoseHeli - 15.0)
										IF NOT bTaskedToWander
											IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
												vehToUse = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
											ELSE
												vehToUse = NULL
											ENDIF
											TASK_HELI_MISSION(GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[i]), carChaserArgs.vehCarChasers[i], vehToUse, PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 100, -1, GET_ENTITY_HEADING(PLAYER_PED_ID()), 100, 50)								
											
											SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, carChaserArgs.relCarChaserEnemies)
											SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, carChaserArgs.relCarChaserEnemies, RELGROUPHASH_PLAYER)
											
											DEBUG_PRINT("bTaskedToWander = TRUE")
											bTaskedToWander = TRUE
										ENDIF
										
										IF bTaskedToWander
											IF GET_TIMER_IN_SECONDS(tHeliTimer) > fTimeToLoseHeli
												bHeliTimeExpired = TRUE
												PRINTLN("bHeliTimeExpired = TRUE")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), carChaserArgs.vehCarChasers[idx]) < DIST_TO_LOSE_CAR_CHASERS 
							bPedsOutOfRange = FALSE
						ENDIF
						
						IF CAR_CHASER_VEHICLE_STUCK(myLocData, carChaserArgs)
							carChaserArgs.bVehicleStuck = TRUE
							PRINTLN("FIRST CHECK: carChaserArgs.bVehicleStuck = TRUE")
						ENDIF
												
						IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[0]))
							PRINTLN("VEHICLE SEAT IS FREE")
							
							IF DOES_BLIP_EXIST(carChaserArgs.blipCarChasers[idx])
								REMOVE_BLIP(carChaserArgs.blipCarChasers[idx])
							ENDIF
							
							IF NOT IS_ENTITY_DEAD(GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[0], VS_FRONT_RIGHT))
								IF GET_SCRIPT_TASK_STATUS(GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[0], VS_FRONT_RIGHT), SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) <> PERFORMING_TASK
									TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[0], VS_FRONT_RIGHT), carChaserArgs.vehCarChasers[0])
									PRINTLN("CAR CHASE: TASKING GUY TO SWITCH SEATS")
								ENDIF
							ENDIF
						ELSE
							IF bRunHeliChase
								IF NOT bTaskedToWander
									IF NOT IS_ENTITY_DEAD(GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[0]))
										IF GET_SCRIPT_TASK_STATUS(GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[0]), SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
											TASK_COMBAT_PED(GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[0]), PLAYER_PED_ID())
											PRINTLN("CAR CHASE: TASKING PASSENGER TO USE COMBAT")
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_ENTITY_DEAD(GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[0]))
									IF GET_SCRIPT_TASK_STATUS(GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[0]), SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
										TASK_COMBAT_PED(GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[0]), PLAYER_PED_ID())
										PRINTLN("CAR CHASE: TASKING PASSENGER TO USE COMBAT")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(carChaserArgs.blipCarChasers[idx])
							REMOVE_BLIP(carChaserArgs.blipCarChasers[idx])
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(carChaserArgs.vehCarChasers[idx])
					IF NOT IS_ENTITY_DEAD(carChaserArgs.vehCarChasers[idx])
						IF IS_ENTITY_UPSIDEDOWN(carChaserArgs.vehCarChasers[idx]) //AND GET_ENTITY_HEIGHT_ABOVE_GROUND(carChaserArgs.vehCarChasers[idx]) < 2.0
							IF NOT bCarChaserUpsideDown
								bCarChaserUpsideDown = TRUE
								iCarChaseUpsideDownVehicleTime = GET_GAME_TIMER()
							ENDIF
						ELSE
							bCarChaserUpsideDown = FALSE
							iCarChaseUpsideDownVehicleTime = 0
						ENDIF
						
						IF bCarChaserUpsideDown
							IF (GET_GAME_TIMER() - iCarChaseUpsideDownVehicleTime) > BLOW_UP_TIME
								DEBUG_PRINT("EXPLODING VEHICLE")
								EXPLODE_VEHICLE(carChaserArgs.vehCarChasers[idx])
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
				
			ENDREPEAT
			
			// Check for distance between car chaser and the player
			REPEAT (myLocData.iNumPedsPerCar * myLocData.iNumCarChaserCarsPerWave) idx
				IF DOES_ENTITY_EXIST(carChaserArgs.pedCarChasers[idx])
					IF NOT IS_ENTITY_DEAD(carChaserArgs.pedCarChasers[idx])
					
						bPedsDead = FALSE
//						DEBUG_PRINT("bPedsDead = FALSE")
					
						IF NOT DOES_BLIP_EXIST(carChaserArgs.badGuyBlips[idx]) 
							IF NOT IS_PED_IN_ANY_VEHICLE(carChaserArgs.pedCarChasers[idx])
								carChaserArgs.badGuyBlips[idx] = ADD_BLIP_FOR_ENTITY(carChaserArgs.pedCarChasers[idx])
								SET_BLIP_COLOUR(carChaserArgs.badGuyBlips[idx], BLIP_COLOUR_RED)
							ENDIF
						ENDIF
						
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), carChaserArgs.pedCarChasers[idx]) < DIST_TO_LOSE_CAR_CHASERS 
							bPedsOutOfRange = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		
			REPEAT myLocData.iNumCarChaserCarsPerWave idx
				IF DOES_ENTITY_EXIST(carChaserArgs.vehCarChasers[idx])
					IF NOT IS_ENTITY_DEAD(carChaserArgs.vehCarChasers[idx])
						bCarsDead = FALSE
//						DEBUG_PRINT("bCarsDead = FALSE")
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT bDoPoliceChase
				IF bPedsOutOfRange OR bCarsDead OR bWanted OR bPedsDead OR carChaserArgs.bVehicleStuck 
				OR (bRunHeliChase AND bHeliTimeExpired)
					bWavesDone = TRUE
					
					IF carChaserArgs.bVehicleStuck
						DEBUG_PRINT("SECOND CHECK: carChaserArgs.bVehicleStuck = TRUE")
					ENDIF
					IF bPedsOutOfRange
						DEBUG_PRINT("bPedsOutOfRange IS TRUE")
						bCarChasersLost = TRUE
						DEBUG_PRINT("bCarChasersLost = TRUE")
					ENDIF
					IF bCarsDead
						DEBUG_PRINT("bCarsDead IS TRUE")
						bChasersDead = TRUE
						DEBUG_PRINT("bChasersDead = TRUE")
					ENDIF
					IF bPedsDead
						DEBUG_PRINT("bPedsDead IS TRUE")
						bChasersDead = TRUE
						DEBUG_PRINT("bChasersDead = TRUE")
					ENDIF
					IF bWanted
						DEBUG_PRINT("bWanted IS TRUE")
					ENDIF
					
				ELSE 
					bWavesDone = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bWavesDone
		DEBUG_PRINT("bWavesDone IS TRUE")
		
		IF bPedsOutOfRange OR bWanted OR carChaserArgs.bVehicleStuck 
		OR (bRunHeliChase AND bHeliTimeExpired)
			REPEAT COUNT_OF(carChaserArgs.vehCarChasers) i
				IF IS_VEHICLE_DRIVEABLE(carChaserArgs.vehCarChasers[i])
					REPEAT myLocData.iNumPedsPerCar j
						iStartIndex = i*myLocData.iNumPedsPerCar
						IF NOT IS_PED_INJURED(carChaserArgs.pedCarChasers[j + iStartIndex])
							IF bRunHeliChase
								
							ELSE
								TASK_VEHICLE_DRIVE_WANDER(carChaserArgs.pedCarChasers[j + iStartIndex], carChaserArgs.vehCarChasers[i], 20.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
								DEBUG_PRINT("TASKING VEHICLES TO WANDER")
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDREPEAT
		ENDIF
		
		RETURN TRUE
	ELSE	
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL COP_CHASE_CONDITIONS_MET(LOCATION_DATA &myLocData, CAR_CHASER_ARGS &carChaserArgs, BOOL& bDoPoliceChase)
	INT i, j, iStartIndex
	
	IF IS_TIMER_STARTED(tExtraCopChaseTimer)
		IF GET_TIMER_IN_SECONDS(tExtraCopChaseTimer) > 30 AND NOT bCopChaseActive
		OR (VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), myLocData.endData[0].vDrugPickup) < 100 AND NOT bCopChaseActive)
			DEBUG_PRINT("TIME UP ON STAGED POLICE CHASE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	// If we're doing a police chase, and the player is no longer wanted... task the cars to wander
	IF bDoPoliceChase 
		IF bCopChaseActive AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
		
			REPEAT COUNT_OF(carChaserArgs.vehCarChasers) i
				IF IS_VEHICLE_DRIVEABLE(carChaserArgs.vehCarChasers[i])
					REPEAT myLocData.iNumPedsPerCar j
						iStartIndex = i*myLocData.iNumPedsPerCar
						IF NOT IS_PED_INJURED(carChaserArgs.pedCarChasers[j + iStartIndex])
							CLEAR_PED_TASKS(carChaserArgs.pedCarChasers[j + iStartIndex])
							TASK_VEHICLE_DRIVE_WANDER(carChaserArgs.pedCarChasers[j + iStartIndex], carChaserArgs.vehCarChasers[i], 20.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
							DEBUG_PRINT("TASKING CAR TO WANDER VIA POLICE CHASE")
						ENDIF
						IF NOT IS_ENTITY_ON_SCREEN(carChaserArgs.pedCarChasers[j + iStartIndex])
							DELETE_PED(carChaserArgs.pedCarChasers[j + iStartIndex])
							DEBUG_PRINT("DELETING PEDS IN POLICE CAR CHASE")
						ENDIF
					ENDREPEAT
					IF NOT IS_ENTITY_ON_SCREEN(carChaserArgs.vehCarChasers[i])
						DELETE_VEHICLE(carChaserArgs.vehCarChasers[i])
						DEBUG_PRINT("DELETING CARS IN POLICE CAR CHASE")
					ENDIF
				ENDIF
			ENDREPEAT
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAR_CHASER_PASS_CONDITIONS_MET(LOCATION_DATA &myLocData, CAR_CHASER_ARGS &carChaserArgs, BOOL& bWanted, 
BOOL& bDoPoliceChase, BOOL& bRunHeliChase)
	
	IF DOES_ENTITY_EXIST(carChaserArgs.vehCarChasers[0]) AND NOT IS_ENTITY_DEAD(carChaserArgs.vehCarChasers[0])
		CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, carChaserArgs.vehCarChasers[0])
	ELSE
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	ENDIF
	
	IF REGULAR_CHASE_PASS_CONDITIONS(myLocData, carChaserArgs, bWanted, bDoPoliceChase, bRunHeliChase)
	OR COP_CHASE_CONDITIONS_MET(myLocData, carChaserArgs, bDoPoliceChase)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DECREASE_CAR_CHASER_SPEED(LOCATION_DATA &myLocData, VEHICLE_INDEX& vehCarChasers[], BOOL &AllowSpeedInc)
	
	VEHICLE_INDEX playerVehicle
	INT iWaveNum, idx
	
	INT iThrottleBackTime
	INT iDistanceCheck
	INT iThrottleBackSpeed
	
	iThrottleBackTime = 30		// Time allowed behind the player, i.e. once the car is within range
	iDistanceCheck = 50			// How close the car chaser can get before the timer begins
	iThrottleBackSpeed = 10		// How much speed is subtracted from the car chaser forward speed

//	 -------------------Difficulty tuning for car chasers -------------------
//	SWITCH myLocData.difficultySetting
//		CASE DIFFICULTY_EASY
//			iThrottleBackTime = 30		// Time allowed behind the player, i.e. once the car is within range
//			iDistanceCheck = 50			// How close the car chaser can get before the timer begins
//			iThrottleBackSpeed = 10		// How much speed is subtracted from the car chaser forward speed
//		BREAK
//		CASE DIFFICULTY_MED
//			iThrottleBackTime = 30		// Time allowed behind the player, i.e. once the car is within range
//			iDistanceCheck = 30			// How close the car chaser can get before the timer begins
//			iThrottleBackSpeed = 5		// How much speed is subtracted from the car chaser forward speed
//		BREAK
//		CASE DIFFICULTY_HARD
//			iThrottleBackTime = 30		// Time allowed behind the player, i.e. once the car is within range
//			iDistanceCheck = 20			// How close the car chaser can get before the timer begins
//			iThrottleBackSpeed = 2		// How much speed is subtracted from the car chaser forward speed
//		BREAK
//	ENDSWITCH
	
	REPEAT myLocData.iNumWaves iWaveNum
		IF bWaveActive[iWaveNum]
			REPEAT myLocData.iNumCarChaserCarsPerWave idx
				IF AllowSpeedInc
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
						playerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						
						IF IS_VEHICLE_DRIVEABLE(vehCarChasers[idx])
							IF GET_DISTANCE_BETWEEN_ENTITIES(playerVehicle, vehCarChasers[idx]) < iDistanceCheck
						
								IF NOT IS_TIMER_STARTED(tCarChaseTimer)
									START_TIMER_NOW(tCarChaseTimer)
									DEBUG_PRINT("tCarChaseTimer HAS STARTED")
								ENDIF
								
								IF GET_TIMER_IN_SECONDS(tCarChaseTimer) > iThrottleBackTime
									
									IF IS_VEHICLE_DRIVEABLE(playerVehicle) AND NOT IS_ENTITY_DEAD(playerVehicle)
										IF GET_ENTITY_SPEED(playerVehicle) > iThrottleBackSpeed
											SET_VEHICLE_FORWARD_SPEED(vehCarChasers[idx], (GET_ENTITY_SPEED(playerVehicle) - iThrottleBackSpeed))
											DEBUG_PRINT("SETTING CAR CHASER TO A LOWER SPEED")
										ENDIF
									ENDIF
								
									RESTART_TIMER_NOW(tCarChaseTimer)
									DEBUG_PRINT("RESTARTING tCarChaseTimer TO ALLOW FOR A SPEED INCREASE x SECONDS")
								
									AllowSpeedInc = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT

ENDPROC

PROC KEEP_CHASERS_IN_RANGE(LOCATION_DATA &myLocData, CAR_CHASER_ARGS &carChaserArgs)
	
	SEQUENCE_INDEX seq
	VEHICLE_INDEX vehPlayer
	
	INT iWaveNum, idx, idx2, iStartIndex
	VECTOR vDesiredRange 
	VECTOR vOffsetLeft = <<-5, 15, 0>>
	VECTOR vOffsetRight = <<5, 15, 0>>
	VECTOR vPlayer, vCarChaser, vCarChaserRealCoor
	FLOAT fPlayerOri
	FLOAT fPlayerSpeed
	
	// For Entity Matrix
	VECTOR vFront, vSide, vUp, vPos
	
	// Get the player's front, side, up and position vectors
	GET_ENTITY_MATRIX(PLAYER_PED_ID(), vFront, vSide, vUp, vPos)
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ENDIF
	
	// Get the player's position
	VECTOR vPlayerCoor
	vPlayerCoor = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	// Get the player's heading
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		fPlayerOri = GET_ENTITY_HEADING(PLAYER_PED_ID())
	ENDIF
	
	// Get the Car Chaser's position
	REPEAT myLocData.iNumWaves iWaveNum
		IF bWaveActive[iWaveNum]
			REPEAT myLocData.iNumCarChaserCarsPerWave idx
				IF NOT IS_PED_INJURED(carChaserArgs.pedCarChasers[idx])
					vCarChaserRealCoor = GET_ENTITY_COORDS(carChaserArgs.pedCarChasers[idx])
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
	// Construct the forward vector for the player
	vPlayer = <<-SIN(fPlayerOri), COS(fPlayerOri), 0>>
	
	// Construct the car chaser's vector relative to the player
	vCarChaser = vCarChaserRealCoor - vPlayerCoor
	
	//---------------------------------LEFT/RIGHT CHECK---------------------------------
	
	// Use the player's side vector to test if the car chaser is to the left or right of the player
	IF DOT_PRODUCT(vSide, vCarChaser) < 0
		bAIOnLeftSide = TRUE
		bAIOnRightSide = FALSE
//		DEBUG_PRINT("bAIOnLeftSide = TRUE")
	ELSE
		bAIOnRightSide = TRUE
		bAIOnLeftSide = FALSE
//		DEBUG_PRINT("bAIOnRightSide = TRUE")
	ENDIF
	
	// Determine the desired range based on the car chaser's left/right position relative to the player.
	IF IS_VEHICLE_DRIVEABLE(vehPlayer)
		IF bAIOnLeftSide
			vDesiredRange = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayer, vOffsetLeft)
//			DRAW_SPHERE(vDesiredRange, 2)
		ELIF bAIOnRightSide
			vDesiredRange = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayer, vOffsetRight)
//			DRAW_SPHERE(vDesiredRange, 2)
		ENDIF
	ENDIF
	
	//---------------------------------FRONT/BACK CHECK----------------------------------
	
	IF DOT_PRODUCT(vPlayer, vCarChaser) < 0
		bCarChaserBehind = TRUE
//		DEBUG_PRINT("bCarChaserBehind = TRUE")
	ELSE	
		bCarChaserBehind = FALSE
//		DEBUG_PRINT("bCarChaserBehind = FALSE")
	ENDIF
	
	REPEAT myLocData.iNumWaves iWaveNum
		IF bWaveActive[iWaveNum]
			REPEAT myLocData.iNumCarChaserCarsPerWave idx
				REPEAT myLocData.iNumPedsPerCar idx2
					iStartIndex = idx*myLocData.iNumPedsPerCar
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_PED_INJURED(carChaserArgs.pedCarChasers[idx2 + iStartIndex])
							IF bCarChaserBehind
								// Hack ?
								SET_PED_AMMO(carChaserArgs.pedCarChasers[idx2 + iStartIndex], WEAPONTYPE_PISTOL, 15)
								SET_PED_SHOOT_RATE(carChaserArgs.pedCarChasers[idx2 + iStartIndex], 15)
								SET_PED_ACCURACY(carChaserArgs.pedCarChasers[idx2 + iStartIndex], 15)
							ELSE
								// Hack ?
								SET_PED_AMMO(carChaserArgs.pedCarChasers[idx2 + iStartIndex], WEAPONTYPE_PISTOL, 35)
								SET_PED_SHOOT_RATE(carChaserArgs.pedCarChasers[idx2 + iStartIndex], 100)
								SET_PED_ACCURACY(carChaserArgs.pedCarChasers[idx2 + iStartIndex], 100)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
	IF bCarChaserBehind
		REPEAT myLocData.iNumWaves iWaveNum
			IF bWaveActive[iWaveNum]
				REPEAT myLocData.iNumCarChaserCarsPerWave idx
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(carChaserArgs.vehCarChasers[idx])
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), carChaserArgs.vehCarChasers[idx]) < 50
								IF IS_VEHICLE_DRIVEABLE(carChaserArgs.vehCarChasers[idx])
									fPlayerSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
									OPEN_SEQUENCE_TASK(seq)
										IF fPlayerSpeed < 10
											TASK_VEHICLE_DRIVE_TO_COORD(NULL, carChaserArgs.vehCarChasers[idx], vDesiredRange, 30.0, DRIVINGSTYLE_NORMAL, myLocData.mnCarChasers[idx], DRIVINGMODE_AVOIDCARS, 2, -1)
										ELSE
											TASK_VEHICLE_DRIVE_TO_COORD(NULL, carChaserArgs.vehCarChasers[idx], vDesiredRange, (fPlayerSpeed + 20), DRIVINGSTYLE_NORMAL, myLocData.mnCarChasers[idx], DRIVINGMODE_AVOIDCARS, 2, -1)
										ENDIF
//										DEBUG_PRINT("TASKING VEHICLE DRIVE TO COORD")
									CLOSE_SEQUENCE_TASK(seq)
									IF NOT IS_PED_INJURED(carChaserArgs.pedCarChasers[idx])
										TASK_PERFORM_SEQUENCE(carChaserArgs.pedCarChasers[idx], seq)
									ENDIF
									CLEAR_SEQUENCE_TASK(seq)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC ADJUST_CAR_CHASERS_SPEED(LOCATION_DATA &myLocData, VEHICLE_INDEX& vehCarChasers[], FLOAT fDistanceToCheck)
	
	INT idx, iWaveNum
	FLOAT fDistanceBetweenPlayerAndChasers
	FLOAT fPlayerSpeed
	VEHICLE_INDEX playerVehicle
	
	// Monitor if the car chaser is within range for a certain period of time and then throttle back their speed.
//	DECREASE_CAR_CHASER_SPEED(myLocData, vehCarChasers, bAllowSpeedIncrease)
	
//	// If car chaser's speed has been decreased, wait ten seconds and allow for an increase again.
//	IF NOT bAllowSpeedIncrease
//		IF IS_TIMER_STARTED(tCarChaseTimer)
//			IF GET_TIMER_IN_SECONDS(tCarChaseTimer) > iTimeDelayForIncrease
//				bAllowSpeedIncrease = TRUE
//				DEBUG_PRINT("SETTING bAllowSpeedIncrease BACK TO TRUE")
//			ENDIF
//		ENDIF
//	ENDIF
	
	// Give them an increase in speed if it's okay to do so, i.e. the throttle back bool hasn't been set.
//	IF bAllowSpeedIncrease
		REPEAT myLocData.iNumWaves iWaveNum
			IF bWaveActive[iWaveNum]
				REPEAT myLocData.iNumCarChaserCarsPerWave idx
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						playerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						// Check if the player's car is in front of the car chaser
						IF IS_VEHICLE_DRIVEABLE(vehCarChasers[idx])
							IF DT_IS_CAR_IN_FRONT_OF_CAR(playerVehicle, vehCarChasers[idx])
								// Check if they are traveling in the same direction
								IF DT_ARE_CARS_TRAVELLING_IN_SAME_DIRECTION(playerVehicle, vehCarChasers[idx])
									// Make sure they're not upside down
									IF IS_VEHICLE_ON_ALL_WHEELS(vehCarChasers[idx])
										fDistanceBetweenPlayerAndChasers = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehCarChasers[idx])
										// Make sure they are at least moving before increasing their speed
										IF GET_ENTITY_SPEED(vehCarChasers[idx]) > 5
											IF fDistanceBetweenPlayerAndChasers > fDistanceToCheck
												fPlayerSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
												IF fPlayerSpeed < 10
													SET_VEHICLE_FORWARD_SPEED(vehCarChasers[idx], 30.0)
//													DEBUG_PRINT("INCREASING CAR CHASER SPEED 01")
												ELSE
													SET_VEHICLE_FORWARD_SPEED(vehCarChasers[idx], fPlayerSpeed + 10)
//													DEBUG_PRINT("INCREASING CAR CHASER SPEED 02")
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDREPEAT
//	ENDIF

ENDPROC

FUNC BOOL GET_FORWARD_POINT(VECTOR &vSpawnPos, FLOAT &fSpawnOri, FLOAT iNodeOffsetToCheck = 200.0)

	// If the player is not in a vehicle, exit early.
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	// Players Initial Position/Orientation - WITHOUT DIRECTION
	VECTOR tempPlayerPos
	FLOAT tempPlayerRot
	
	// Vehicle Node Position/Orientation - WITHOUT DIRECTION
	VECTOR vNodePos
	FLOAT fNodeOri
	
	// Constructed vectors - WITH DIRECTION
	VECTOR vPlayerDir 
//	VECTOR vNodeDir
	VECTOR vNodeDirAlt
	
	// Get num of lanes, required with using NATIVE function below
	INT iNumLanes
	
	// Get the player position and heading
	tempPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	tempPlayerRot = GET_ENTITY_HEADING(PLAYER_PED_ID())
	
	// Grab vehicle note position/orientation based on an offset from the direction the player is facing
	GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(tempPlayerPos, tempPlayerRot, <<0, iNodeOffsetToCheck, 0>>), 1, vNodePos, fNodeOri, iNumLanes, NF_NONE)
	
	// ====================================== CONSTRUCT VECTORS FROM HEADINGS ======================================
	
	// Formulate vector based on player's heading
	vPlayerDir = <<-SIN(tempPlayerRot), COS(tempPlayerRot), 0>>
	// Formulate vector of node based on node's heading
//	vNodeDir = <<-SIN(fNodeOri), COS(fNodeOri), 0>>
	// Formulate vector based on spawn coordinates given from vehicle node functions
	vNodeDirAlt = vNodePos - tempPlayerPos
	

//	// If the node is almost 180 degrees, just turn it around... useful for going against one-way streets.
	IF COS(ABSF(fNodeOri - tempPlayerRot)) <= -0.98
		fNodeOri += 180
	ENDIF
	
	// ============================================ RUN TESTS ======================================================
	
	BOOL bPointIsGood
	bPointIsGood = TRUE
	
	// Check the orientations of the points used are roughly pointing in the same direction... 0.867 equates roughly to 30 degrees.
	IF COS(ABSF(fNodeOri - tempPlayerRot)) <= 0.867
		DEBUG_PRINT("ORIENTATION BETWEEN HEADINGS ARE TOO GREAT, RETURNING FALSE!!!")
		bPointIsGood = FALSE
	ENDIF
	
	//  Check the z values to make sure the points are not too high, e.g. on an overpass, etc.
	IF ABSF(vNodePos.z - tempPlayerPos.z) > 3.0
		DEBUG_PRINT("GET_FORWARD_POINT: Z VALUE IS TOO HIGH, RETURNING FALSE!!!")
		bPointIsGood = FALSE
	ENDIF
	
	// Check against a zero vector, i.e. empty node
	IF ARE_VECTORS_EQUAL(vNodePos, <<0,0,0>>)
		DEBUG_PRINT("GET_FORWARD_POINT: VECTOR IS ZERO, RETURNING FALSE!!!")
		bPointIsGood = FALSE
	ENDIF
	
	// Check to make sure point is in front of the player.
	IF DOT_PRODUCT(vPlayerDir, vNodeDirAlt) <= 0
		bPointIsGood = FALSE
	ENDIF
	
	IF bPointIsGood 
		vSpawnPos = vNodePos
		fSpawnOri = fNodeOri
		DEBUG_PRINT("GET_FORWARD_POINT: GOT A VALID POINT!!!")
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC BOOL GET_BACKWARD_POINT(VECTOR &vSpawnPos, FLOAT &fSpawnOri, FLOAT iNodeOffsetToCheck = -100.0)

	// If the player is not in a vehicle, exit early.
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	// Players Initial Position/Orientation - WITHOUT DIRECTION
	VECTOR tempPlayerPos
	FLOAT tempPlayerRot
	
	// Vehicle Node Position/Orientation - WITHOUT DIRECTION
	VECTOR vNodePos
	FLOAT fNodeOri
	
	// Constructed vectors - WITH DIRECTION
	VECTOR vPlayerDir 
//	VECTOR vNodeDir
	VECTOR vNodeDirAlt
	
	// Get num of lanes, required with using NATIVE function below
	INT iNumLanes
	
	// Get the player position and heading
	tempPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	tempPlayerRot = GET_ENTITY_HEADING(PLAYER_PED_ID())
	
	// Grab vehicle note position/orientation based on an offset from the direction the player is facing
	GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0, iNodeOffsetToCheck, 0>>), 1, vNodePos, fNodeOri, iNumLanes, NF_NONE)
	
	
	// ====================================== CONSTRUCT VECTORS FROM HEADINGS ======================================
	
	// Formulate vector based on player's heading
	vPlayerDir = <<-SIN(tempPlayerRot), COS(tempPlayerRot), 0>>
	// Formulate vector of node based on node's heading
//	vNodeDir = <<-SIN(fNodeOri), COS(fNodeOri), 0>>
	// Formulate vector based on spawn coordinates given from vehicle node functions
	vNodeDirAlt = vNodePos - tempPlayerPos
	

	// If the node is almost 180 degrees, just turn it around... useful for going against one-way streets.
	IF COS(ABSF(fNodeOri - tempPlayerRot)) <= -0.98
		fNodeOri += 180
	ENDIF
	
	// ============================================ RUN TESTS ======================================================
	
	BOOL bPointIsGood
	bPointIsGood = TRUE
	
	// Check the orientations of the points used are roughly pointing in the same direction... 0.867 equates roughly to 30 degrees.
	IF COS(ABSF(fNodeOri - tempPlayerRot)) <= 0.867
		DEBUG_PRINT("ORIENTATION BETWEEN HEADINGS ARE TOO GREAT, RETURNING FALSE!!!")
		bPointIsGood = FALSE
	ENDIF
	
	//  Check the z values to make sure the points are not too high, e.g. on an overpass, etc.
	IF ABSF(vNodePos.z - tempPlayerPos.z) > 3.0
		DEBUG_PRINT("Z VALUE IS TOO HIGH, RETURNING FALSE!!!")
		bPointIsGood = FALSE
	ENDIF
	
	// Check against a zero vector, i.e. empty node
	IF ARE_VECTORS_EQUAL(vNodePos, <<0,0,0>>)
		DEBUG_PRINT("VECTOR IS ZERO, RETURNING FALSE!!!")
		bPointIsGood = FALSE
	ENDIF
	
	IF IS_SPHERE_VISIBLE(vNodePos, 5.0)
		DEBUG_PRINT("NODE VECTOR IS VISIBLE, RETURNING FALSE")
	ENDIF
	
	// Check point to see if it's sufficiently behind the player.
	IF DOT_PRODUCT(vPlayerDir, vNodeDirAlt) >= 0
		bPointIsGood = FALSE
	ENDIF
	
	IF bPointIsGood 
		vSpawnPos = vNodePos
		fSpawnOri = fNodeOri
		DEBUG_PRINT("GOT A VALID POINT!!!")
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

////Sorta hacky function to tell if a point is above the water
FUNC BOOL IS_POINT_OVER_WATER(VECTOR vCoord)

	vCoord.z += 10
	FLOAT fGroundDist
	FLOAT fWaterDist
	//BOOL bValidGround = FALSE
	//If we can't get a z collision for the ground use the water height instead
	IF NOT GET_GROUND_Z_FOR_3D_COORD(vCoord, fGroundDist)
		//bValidGround  = TRUE
	ENDIF
	
	PRINTSTRING("groundDist:")
	PRINTFLOAT(fGroundDist)
	PRINTNL()

	IF NOT GET_WATER_HEIGHT(vCoord, fWaterDist)
		RETURN FALSE
	ENDIF

	PRINTSTRING("fWaterDist:")
	PRINTFLOAT(fWaterDist)
	PRINTNL()
	
//	IF bValidGround 
//		//If we found a ground collision see if the water is closer than the ground
//		IF fGroundDist < fWaterDist
//			RETURN FALSE
//		ENDIF
//	ENDIF
	
	//IF fWaterDist < 14.0
		DEBUG_PRINT("IS_POINT_OVER_WATER Failed to find a point")
		RETURN TRUE
	//ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_WATER_CHASE_OVER(LOCATION_DATA &myLocData, CAR_CHASER_ARGS &carChaserArgs)
	//BOOL bCompelte
	INT idx, idx2, iStartIndex
	
	BOOL bAllVehiclesDead = TRUE
	BOOL bAllPedsDead = TRUE
	BOOL bAllOutOfRange = TRUE
	
	//check all of the chaser cars
	REPEAT myLocData.iNumCarChaserCarsPerWave idx
		IF NOT IS_ENTITY_DEAD(carChaserArgs.vehCarChasers[idx])
			bAllVehiclesDead = FALSE
			
			IF GET_PLAYER_DISTANCE_FROM_ENTITY(carChaserArgs.vehCarChasers[idx]) < 150
				bAllOutOfRange = FALSE
			ENDIF
		ENDIF
		
		REPEAT myLocData.iNumPedsPerCar idx2
			iStartIndex = idx*myLocData.iNumPedsPerCar
			IF NOT IS_ENTITY_DEAD(carChaserArgs.pedCarChasers[idx2 + iStartIndex])
				bAllPedsDead = FALSE
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	IF bAllVehiclesDead OR bAllPedsDead OR bAllOutOfRange
		REPEAT myLocData.iNumCarChaserCarsPerWave idx
			REMOVE_BLIP(carChaserArgs.blipCarChasers[idx]) 
		ENDREPEAT
		
		SET_VEHICLE_AS_NO_LONGER_NEEDED(carChaserArgs.vehCarChasers[idx])
	
	
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//FUNC BOOL UPDATE_WATER_ATTACKERS(LOCATION_DATA &myLocData, structTimer& myTimer, CAR_CHASER_ARGS &carChaserArgs, BLIP_INDEX &myLocationBlip[],
//BOOL &bIsChaseActive, BOOL bWanted, TEXT_LABEL_31& sLastObjective)
//	bWanted = bWanted
//	INT idx, idx2
//	BOOL nodeFound = FALSE
//	INT rand, iTemp
//	INT iStartIndex 
//	VECTOR vOffset
//	VECTOR vSpawnCenter[MAX_NUMBER_CAR_CHASERS]
//	VECTOR vCarChaserSpawn[MAX_NUMBER_CAR_CHASERS]
//	SWITCH carChaserArgs.chaseSate
//		CASE CHASE_STATE_WAIT
//			IF TIMER_DO_WHEN_READY(myTimer, 10)
//				carChaserArgs.chaseSate = CHASE_STATE_CREATE
//			ENDIF
//		BREAK
//		
//		CASE CHASE_STATE_CREATE
//			DEBUG_PRINT("CHASE_STATE_CREATE")
//			//We're in the water so randomly spawn behind or beside.
//			rand = GET_RANDOM_INT_IN_RANGE(0,3)
//
//			IF rand = 0
//				//Behind
//				vOffset = <<0, -65, 0>>
//			ELIF rand = 1
//				//Left
//				vOffset = <<-65, -8, 0>>
//			ELIF rand = 2
//				//Right
//				vOffset = <<65, -8, 0>>
//			ENDIF
//			
//			REPEAT myLocData.iNumCarChaserCarsPerWave idx
//				vSpawnCenter[idx] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vOffset)
//			ENDREPEAT
//			
//			REPEAT myLocData.iNumCarChaserCarsPerWave idx
//				IF GET_RANDOM_WATER_NODE(vSpawnCenter[idx], 10.0, 0, TRUE, TRUE, vCarChaserSpawn[idx], iTemp )
//					DEBUG_PRINT("Node found")
//					nodeFound = TRUE
//				ELSE
//					DEBUG_PRINT("GET_RANDOM_WATER_NODE failed")
//					IF IS_POINT_OVER_WATER(vSpawnCenter[idx])
//						DEBUG_PRINT("IS_POINT_OVER_WATER fallback passed")
//						vCarChaserSpawn[idx] = vSpawnCenter[idx]
//						nodeFound = TRUE
//					ELSE
//						DEBUG_PRINT("IS_POINT_OVER_WATER fallback failed")
//					ENDIF
//				ENDIF
//			ENDREPEAT
//			
//			IF nodeFound
//				REPEAT myLocData.iNumCarChaserCarsPerWave idx
//					CLEAR_AREA_OF_VEHICLES(vCarChaserSpawn[idx], 10.0)
//					carChaserArgs.vehCarChasers[idx] = CREATE_VEHICLE(myLocData.mnCarChasers[idx], vCarChaserSpawn[idx], 90.0)
//					SET_VEHICLE_ON_GROUND_PROPERLY(carChaserArgs.vehCarChasers[idx])
//					SET_VEHICLE_ENGINE_ON(carChaserArgs.vehCarChasers[idx], TRUE, TRUE)
//					SET_ENTITY_PROOFS(carChaserArgs.vehCarChasers[idx], FALSE, FALSE, FALSE, FALSE, FALSE)
//					SET_VEHICLE_CAN_BE_TARGETTED(carChaserArgs.vehCarChasers[idx], TRUE)
//					
//					REPEAT myLocData.iNumPedsPerCar idx2
//												
//						DEBUG_PRINT("CREATING PEDS IN CAR - WAVE ONE")
//						
//						iStartIndex = idx*myLocData.iNumPedsPerCar
//						carChaserArgs.pedCarChasers[idx2 + iStartIndex] = CREATE_PED_INSIDE_VEHICLE(carChaserArgs.vehCarChasers[idx], PEDTYPE_CRIMINAL, myLocData.mnPedChasers[idx2], myLocData.PedSeat[idx2])
//						
//						IF NOT IS_ENTITY_DEAD(carChaserArgs.pedCarChasers[idx2 + iStartIndex])
//							SETUP_PED_FOR_CHASE(carChaserArgs.pedCarChasers[idx2 + iStartIndex], carChaserArgs.relCarChaserEnemies)
//						ENDIF
//
//					ENDREPEAT
//
//					carChaserArgs.blipCarChasers[idx] = ADD_BLIP_FOR_ENTITY(carChaserArgs.vehCarChasers[idx])
//					SET_BLIP_COLOUR(carChaserArgs.blipCarChasers[idx], BLIP_COLOUR_RED)
//					
//					bIsChaseActive = TRUE
//				ENDREPEAT
//				
//				// Print objective to lose the car chasers.
//				PRINT("DTRSHRD_02",DEFAULT_GOD_TEXT_TIME,1)
//				
//				REPEAT COUNT_OF(myLocationBlip) idx
//					IF DOES_BLIP_EXIST(myLocationBlip[idx])
//						REMOVE_BLIP(myLocationBlip[idx])
//					ENDIF
//				ENDREPEAT
//				
//				myLocData.iNumWaves = myLocData.iNumWaves - 1
//				bWaveActive[0] = TRUE
//				carChaserArgs.chaseSate = CHASE_STATE_CHASE
//			ENDIF
//		BREAK
//		
//		CASE CHASE_STATE_CHASE
//			KEEP_CHASERS_IN_RANGE(myLocData, carChaserArgs)
//			
//			ADJUST_CAR_CHASERS_SPEED(myLocData, carChaserArgs.vehCarChasers, 40)
//			
//			IF IS_WATER_CHASE_OVER(myLocData, carChaserArgs)
//				DEBUG_PRINT("GOING TO CAR_CHASE_FINAL")
//				PRINT_NOW(sLastObjective,DEFAULT_GOD_TEXT_TIME,1)
//				//IF myLocData.iNumWaves > 0
//					REPEAT myLocData.iNumDrops idx
//						IF myLocData.endData[idx].bConfigured
//							myLocationBlip[idx] = ADD_BLIP_FOR_COORD(myLocData.endData[idx].vCenterPos)
//						ENDIF
//					ENDREPEAT
//					RESTART_TIMER_NOW(myTimer)
//					bIsChaseActive = FALSE
//					carChaserArgs.chaseSate = CHASE_STATE_OVER //CHASE_STATE_WAIT
//				//ELSE
//					//carChaserArgs.chaseSate = CHASE_STATE_OVER
//				//ENDIF
//				
//			ENDIF		
//			DEBUG_PRINT("CHASE_STATE_CHASE")
//		BREAK
//		CASE CHASE_STATE_OVER
//		
//		BREAK
//	
//	ENDSWITCH
//
//	RETURN FALSE
//ENDFUNC

FUNC BOOL GET_POINT_FOR_SPAWN(BOOL& bDoPoliceChase, INT& idx, VECTOR& vCarChaserSpawn[], FLOAT& fCarChaserRot[])
	IF bDoPoliceChase
		GET_FORWARD_POINT(vCarChaserSpawn[idx], fCarChaserRot[idx])
		DEBUG_PRINT("CHECKING FOR FORWARD POINT")
	ELSE
		GET_BACKWARD_POINT(vCarChaserSpawn[idx], fCarChaserRot[idx])
		DEBUG_PRINT("CHECKING FOR BACKWARD POINT")
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vCarChaserSpawn[idx])
		PRINTLN("VECTOR SPAWN = ", vCarChaserSpawn[idx])
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC UPDATE_CAR_CHASE_SPAWNS(LOCATION_DATA &myLocData, structTimer &myTimer, CAR_CHASER_ARGS &carChaserArgs, BLIP_INDEX &myLocationBlip[],
BOOL &bIsChaseActive, BOOL& bDoPoliceChase, MODEL_NAMES& mnVehicleToUse, MODEL_NAMES& mnPedToUse, BOOL& bRunHeliChase)

	INT idx, idx2, iStartIndex, iWaveNum
	SEQUENCE_INDEX iSeq
	VECTOR vCarChaserSpawn[MAX_NUMBER_CAR_CHASERS]
	VECTOR vOffset = <<-5, -35, 0>>
	FLOAT fCarChaserRot[MAX_NUMBER_CAR_CHASERS]
	BOOL bProcede 
	
	IF bDoPoliceChase
		IF DOES_ENTITY_EXIST(vehExtraCop[0]) AND DOES_ENTITY_EXIST(vehExtraCop[1])
			EXIT
		ENDIF
	ENDIF
	
	REPEAT myLocData.iNumWaves iWaveNum
//		DEBUG_PRINT("STOPPING HERE")
		IF NOT bWaveActive[iWaveNum]
		
			IF NOT IS_TIMER_STARTED(myTimer)
				DEBUG_PRINT("SETTING bProcede 01")
				bProcede = TRUE
			ELSE
				IF GET_TIMER_IN_SECONDS(myTimer) > iWaveDelay
					DEBUG_PRINT("SETTING bProcede 02")
					bProcede = TRUE
				ENDIF
			ENDIF
		
			IF bProcede
				DEBUG_PRINT("INSIDE bProcede")
				REPEAT myLocData.iNumCarChaserCarsPerWave idx
				
					// If iWaveNum = 0, we add nothing... this is to fix stomping on blip indices
					idx += (iWaveNum*myLocData.iNumCarChaserCarsPerWave)
					
					DEBUG_PRINT("CAR CHASERS PER WAVE 01")

					IF iWaveNum = 0 AND GET_POINT_FOR_SPAWN(bDoPoliceChase, idx, vCarChaserSpawn, fCarChaserRot)
					
						IF myLocData.iNumCarChaserCarsPerWave > 1
							IF idx > 0
							
								PRINTSTRING("vCarChaserSpawn[idx] = ")
								PRINTVECTOR(vCarChaserSpawn[idx])
								PRINTNL()
								
								IF DOES_ENTITY_EXIST(carChaserArgs.vehCarChasers[0])
									IF NOT IS_ENTITY_DEAD(carChaserArgs.vehCarChasers[0])
										vCarChaserSpawn[idx] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carChaserArgs.vehCarChasers[0], vOffset)
									ELSE
										vCarChaserSpawn[idx] = vCarChaserSpawn[idx]
									ENDIF
								ENDIF
								
//								vCarChaserSpawn[idx] = vCarChaserSpawn[idx] - vOffset
								
								PRINTSTRING("vCarChaserSpawn[idx] = ")
								PRINTVECTOR(vCarChaserSpawn[idx])
								PRINTNL()
							ENDIF
						ENDIF
						
						DEBUG_PRINT("GOT BACKWARD POINT")
						
						// Check again against a zero vector
						IF NOT ARE_VECTORS_EQUAL(vCarChaserSpawn[idx], <<0,0,0>>)
							CLEAR_AREA_OF_VEHICLES(vCarChaserSpawn[idx], 10.0)
							// If we're doing a police chase, then use a police model vehicle.
							IF NOT bDoPoliceChase
								IF bRunHeliChase
									carChaserArgs.vehCarChasers[idx] = CREATE_VEHICLE(BUZZARD, << vCarChaserSpawn[idx].x, vCarChaserSpawn[idx].y, vCarChaserSpawn[idx].z + 100 >>, fCarChaserRot[idx])
									
									fTimeToLoseHeli = 65.0
									PRINTLN("fTimeToLoseHeli = ", fTimeToLoseHeli)
								ELSE
									carChaserArgs.vehCarChasers[idx] = CREATE_VEHICLE(mnVehicleToUse, vCarChaserSpawn[idx], fCarChaserRot[idx])
								ENDIF
								IF bRunHeliChase
									SET_VEHICLE_ENGINE_ON(carChaserArgs.vehCarChasers[idx], TRUE, TRUE)
									SET_ENTITY_PROOFS(carChaserArgs.vehCarChasers[idx], FALSE, FALSE, FALSE, FALSE, FALSE)
									SET_VEHICLE_CAN_BE_TARGETTED(carChaserArgs.vehCarChasers[idx], TRUE)
									IF SHOULD_LIGHTS_BE_ON()
										SET_VEHICLE_SEARCHLIGHT(carChaserArgs.vehCarChasers[idx], TRUE, TRUE)
										PRINTLN("---- CAR CHASE: TURNING HELI LIGHTS ON! ----")
									ELSE
										PRINTLN("---- CAR CHASE: NO HELI LIGHTS ----")
									ENDIF
								ELSE
									SET_VEHICLE_ON_GROUND_PROPERLY(carChaserArgs.vehCarChasers[idx])
									SET_VEHICLE_ENGINE_ON(carChaserArgs.vehCarChasers[idx], TRUE, TRUE)
									SET_ENTITY_PROOFS(carChaserArgs.vehCarChasers[idx], FALSE, FALSE, FALSE, FALSE, FALSE)
//									SET_VEHICLE_CAN_BE_TARGETTED(carChaserArgs.vehCarChasers[idx], TRUE)
									MODIFY_VEHICLE_TOP_SPEED(carChaserArgs.vehCarChasers[idx], 100.0)
									ROLL_DOWN_WINDOWS(carChaserArgs.vehCarChasers[idx])
								ENDIF
								
							// If we're doing a police chase, don't spawn a pursuing cop
							ELSE
								vExtraOffset01 = <<vCarChaserSpawn[idx].x - 3, vCarChaserSpawn[idx].y - 3, vCarChaserSpawn[idx].z>>
								vExtraOffset02 = <<vCarChaserSpawn[idx].x + 3, vCarChaserSpawn[idx].y + 3, vCarChaserSpawn[idx].z>>
								fHeading01 = fCarChaserRot[idx] - 45
								fHeading02 = fCarChaserRot[idx] + 45
							
								CLEAR_AREA_OF_VEHICLES(vCarChaserSpawn[idx], 50.0)
							
								// Extra Cop 1
								vehExtraCop[0] = CREATE_VEHICLE(mnVehicleToUse, vExtraOffset01, fHeading01)
								SET_VEHICLE_ON_GROUND_PROPERLY(vehExtraCop[0])
								SET_VEHICLE_DOOR_OPEN(vehExtraCop[0], SC_DOOR_FRONT_RIGHT)
								SET_VEHICLE_DOOR_OPEN(vehExtraCop[0], SC_DOOR_FRONT_LEFT)
								IF DOES_ENTITY_EXIST(vehExtraCop[0]) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
									pedExtraCop[0] = CREATE_PED_INSIDE_VEHICLE(vehExtraCop[0], PEDTYPE_COP,  myLocData.mnPolicePed)
									GIVE_WEAPON_TO_PED(pedExtraCop[0], WEAPONTYPE_PISTOL, 100, TRUE)
									
									OPEN_SEQUENCE_TASK(iSeq)
										TASK_LEAVE_VEHICLE(NULL, vehExtraCop[0], ECF_DONT_CLOSE_DOOR)
										TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
									CLOSE_SEQUENCE_TASK(iSeq)
									TASK_PERFORM_SEQUENCE(pedExtraCop[0], iSeq)
									CLEAR_SEQUENCE_TASK(iSeq)
									
									SET_PED_COMBAT_MOVEMENT(pedExtraCop[0], CM_DEFENSIVE)
								ENDIF
								
								// Extra Cop 2
								vehExtraCop[1] = CREATE_VEHICLE(mnVehicleToUse, vExtraOffset02, fHeading02)
								SET_VEHICLE_ON_GROUND_PROPERLY(vehExtraCop[1])
								SET_VEHICLE_DOOR_OPEN(vehExtraCop[1], SC_DOOR_FRONT_RIGHT)
								SET_VEHICLE_DOOR_OPEN(vehExtraCop[1], SC_DOOR_FRONT_LEFT)
								IF DOES_ENTITY_EXIST(vehExtraCop[1]) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
									pedExtraCop[1] = CREATE_PED_INSIDE_VEHICLE(vehExtraCop[1], PEDTYPE_COP,  myLocData.mnPolicePed)
									GIVE_WEAPON_TO_PED(pedExtraCop[1], WEAPONTYPE_PISTOL, 100, TRUE)
									
									OPEN_SEQUENCE_TASK(iSeq)
										TASK_LEAVE_VEHICLE(NULL, vehExtraCop[1], ECF_DONT_CLOSE_DOOR)
										TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
									CLOSE_SEQUENCE_TASK(iSeq)
									TASK_PERFORM_SEQUENCE(pedExtraCop[1], iSeq)
									CLEAR_SEQUENCE_TASK(iSeq)
									
									SET_PED_COMBAT_MOVEMENT(pedExtraCop[1], CM_DEFENSIVE)
								ENDIF
								
								DEBUG_PRINT("CREATING EXTRA COPS")
								
								IF NOT IS_TIMER_STARTED(tExtraCopChaseTimer)
									START_TIMER_NOW(tExtraCopChaseTimer)
								ENDIF
								
								IF DOES_ENTITY_EXIST(vehExtraCop[0]) AND DOES_ENTITY_EXIST(vehExtraCop[1])
									bIsChaseActive = TRUE
								ENDIF
							ENDIF
							
							DEBUG_PRINT("CREATING VEHICLE(s) - WAVE ONE")
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(carChaserArgs.vehCarChasers[idx])
							MODIFY_VEHICLE_TOP_SPEED(carChaserArgs.vehCarChasers[idx], 50.0)
							
							DEBUG_PRINT("VEHICLE IS DRIVABLE")
							REPEAT myLocData.iNumPedsPerCar idx2
								
								DEBUG_PRINT("CREATING PEDS IN CAR - WAVE ONE")
								
								iStartIndex = idx*myLocData.iNumPedsPerCar
								
								IF bRunHeliChase
									IF (idx2 + iStartIndex) = 0
										carChaserArgs.pedCarChasers[idx2 + iStartIndex] = CREATE_PED_INSIDE_VEHICLE(carChaserArgs.vehCarChasers[idx], PEDTYPE_CRIMINAL, mnPedToUse)
										PRINTLN("PUTTING PASSENGER IN THE BACK")
									ELIF (idx2 + iStartIndex) = 1
										carChaserArgs.pedCarChasers[idx2 + iStartIndex] = CREATE_PED_INSIDE_VEHICLE(carChaserArgs.vehCarChasers[idx], PEDTYPE_CRIMINAL, mnPedToUse, VS_BACK_LEFT)
										PRINTLN("PUTTING PASSENGER IN THE BACK")
									ENDIF
								ELSE
									carChaserArgs.pedCarChasers[idx2 + iStartIndex] = CREATE_PED_INSIDE_VEHICLE(carChaserArgs.vehCarChasers[idx], PEDTYPE_CRIMINAL, mnPedToUse, myLocData.PedSeat[idx2])
									PRINTLN("NORMAL SEAT CONDITIONS")
								ENDIF
					
								IF NOT IS_ENTITY_DEAD(carChaserArgs.pedCarChasers[idx2 + iStartIndex])
//									SETUP_PED_FOR_CHASE(carChaserArgs.pedCarChasers[idx2 + iStartIndex], carChaserArgs.relCarChaserEnemies)
									
									SET_MODEL_AS_NO_LONGER_NEEDED(mnPedToUse)
									SET_PED_COMBAT_ABILITY(carChaserArgs.pedCarChasers[idx2 + iStartIndex], CAL_PROFESSIONAL)
									SET_PED_COMBAT_ATTRIBUTES(carChaserArgs.pedCarChasers[idx2 + iStartIndex], CA_LEAVE_VEHICLES, FALSE)															
									SET_PED_CONFIG_FLAG(carChaserArgs.pedCarChasers[idx2 + iStartIndex], PCF_DontInfluenceWantedLevel, TRUE)
									SET_PED_RELATIONSHIP_GROUP_HASH(carChaserArgs.pedCarChasers[idx2 + iStartIndex], carChaserArgs.relCarChaserEnemies)
									SET_PED_ACCURACY(carChaserArgs.pedCarChasers[idx2 + iStartIndex], 35)
									IF bRunHeliChase
										GIVE_WEAPON_TO_PED(carChaserArgs.pedCarChasers[idx2 + iStartIndex], WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
										PRINTLN("GIVING PED ASSAULTRIFLE - bRunHeliChase")
									ELSE
										GIVE_WEAPON_TO_PED(carChaserArgs.pedCarChasers[idx2 + iStartIndex], WEAPONTYPE_MICROSMG, -1, TRUE)
									ENDIF
									SET_PED_KEEP_TASK(carChaserArgs.pedCarChasers[idx2 + iStartIndex], TRUE)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(carChaserArgs.pedCarChasers[idx2 + iStartIndex], TRUE)
									TASK_COMBAT_PED(carChaserArgs.pedCarChasers[idx2 + iStartIndex], PLAYER_PED_ID())
									
									// Driver
									PED_INDEX piDriver, piPassenger
									IF NOT IS_VEHICLE_SEAT_FREE(carChaserArgs.vehCarChasers[idx], VS_DRIVER)
										piDriver = GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[idx])
										piPassenger = GET_PED_IN_VEHICLE_SEAT(carChaserArgs.vehCarChasers[idx], VS_BACK_LEFT)
								
										SET_PED_COMBAT_ATTRIBUTES(piDriver, CA_JUST_FOLLOW_VEHICLE, FALSE)
										PRINTLN("SETTING CA_JUST_FOLLOW_VEHICLE TO FALSE")
											
										IF bRunHeliChase
											TASK_HELI_MISSION(piDriver, carChaserArgs.vehCarChasers[idx], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_ATTACK, 100, -1, GET_ENTITY_HEADING(PLAYER_PED_ID()), 50, 25)
											IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
												SET_PED_ACCURACY(piDriver, 5)
												SET_VEHICLE_SHOOT_AT_TARGET(piDriver, PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
												DEBUG_PRINT("CALLING SET_VEHICLE_SHOOT_AT_TARGET")
											ENDIF
//											IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(carChaserArgs.pedCarChasers[0])
//												IF NOT IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(carChaserArgs.pedCarChasers[0]) 
//								                	CONTROL_MOUNTED_WEAPON(carChaserArgs.pedCarChasers[0]) 
//								                ENDIF
//												SET_MOUNTED_WEAPON_TARGET(PLAYER_PED_ID(), NULL, NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()))
//											ENDIF
											IF DOES_ENTITY_EXIST(piPassenger) AND NOT IS_ENTITY_DEAD(piPassenger)
												TASK_COMBAT_PED(piPassenger, PLAYER_PED_ID())
												SET_PED_ACCURACY(piDriver, 35)
												DEBUG_PRINT("TASKING PASSENGER TO COMBAT")
											ENDIF
										ELSE
											SET_PED_COMBAT_ATTRIBUTES(piDriver, CA_JUST_FOLLOW_VEHICLE, TRUE)
											PRINTLN("SETTING CA_JUST_FOLLOW_VEHICLE TO TRUE")
//											TASK_VEHICLE_MISSION(piDriver, carChaserArgs.vehCarChasers[idx], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_ESCORT_LEFT, 70, DRIVINGMODE_AVOIDCARS, -1, -1)
//											ADD_VEHICLE_SUBTASK_ATTACK_PED(piDriver, PLAYER_PED_ID())
											
											TASK_COMBAT_PED(piDriver, PLAYER_PED_ID())
											DEBUG_PRINT("TASKING DRIVER - ESCORT LEFT")
										ENDIF
									ENDIF
								ENDIF
						
							ENDREPEAT
							
							IF NOT bDoPoliceChase
								carChaserArgs.blipCarChasers[idx] = ADD_BLIP_FOR_ENTITY(carChaserArgs.vehCarChasers[idx])
								SET_BLIP_COLOUR(carChaserArgs.blipCarChasers[idx], BLIP_COLOUR_RED)
								SET_BLIP_NAME_FROM_TEXT_FILE(carChaserArgs.blipCarChasers[idx], "DTRFKGR_BLIP04")
							ENDIF
							
							bIsChaseActive = TRUE
							bWaveActive[iWaveNum] = TRUE
							
							TRIGGER_MUSIC_EVENT("OJDG2_MORE_ENEMIES")
							PRINTLN("TRIGGERING MUSIC - OJDG2_MORE_ENEMIES")
							
							PRINTSTRING("iWaveNum = ")
							PRINTINT(iWaveNum)
							PRINTNL()
							
							PRINTSTRING("carChaserArgs.vehCarChasers[0] POSITION = ")
							PRINTVECTOR(GET_ENTITY_COORDS(carChaserArgs.vehCarChasers[0]))
							PRINTNL()
							
							IF DOES_ENTITY_EXIST(carChaserArgs.vehCarChasers[1])
								PRINTSTRING("carChaserArgs.vehCarChasers[1] POSITION = ")
								PRINTVECTOR(GET_ENTITY_COORDS(carChaserArgs.vehCarChasers[1]))
								PRINTNL()
							ENDIF
							
							DEBUG_PRINT("bIsChaseActive AND bWaveActive = TRUE")
							
							// Remove drop off blip until car chase is complete	
							IF DOES_BLIP_EXIST(myLocationBlip[0])
								REMOVE_BLIP(myLocationBlip[0])
							ENDIF

							bProcede = FALSE
							
							IF NOT IS_TIMER_STARTED(myTimer)
								START_TIMER_NOW(myTimer)
								DEBUG_PRINT("STARTING TIMER")
							ELSE
								RESTART_TIMER_NOW(myTimer)
								DEBUG_PRINT("RESTARTING TIMER")
							ENDIF
						ENDIF
						
					ELIF iWaveNum = 1 AND GET_FORWARD_POINT(vCarChaserSpawn[idx], fCarChaserRot[idx])
						DEBUG_PRINT("GOT FORWARD POINT")
						
						// Check again against a zero vector
						IF NOT ARE_VECTORS_EQUAL(vCarChaserSpawn[idx], <<0,0,0>>)
							CLEAR_AREA_OF_VEHICLES(vCarChaserSpawn[idx], 10.0)
							carChaserArgs.vehCarChasers[idx] = CREATE_VEHICLE(myLocData.mnCarChasers[idx], vCarChaserSpawn[idx], fCarChaserRot[idx])
							SET_VEHICLE_ON_GROUND_PROPERLY(carChaserArgs.vehCarChasers[idx])
							SET_VEHICLE_ENGINE_ON(carChaserArgs.vehCarChasers[idx], TRUE, TRUE)
							SET_ENTITY_PROOFS(carChaserArgs.vehCarChasers[idx], FALSE, FALSE, FALSE, FALSE, FALSE)
							SET_VEHICLE_CAN_BE_TARGETTED(carChaserArgs.vehCarChasers[idx], TRUE)
							
							DEBUG_PRINT("CREATING VEHICLE(s) - WAVE TWO")
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(carChaserArgs.vehCarChasers[idx])
							DEBUG_PRINT("VEHICLE IS DRIVABLE")
							REPEAT myLocData.iNumPedsPerCar idx2
								
								DEBUG_PRINT("CREATING PEDS IN CAR - WAVE TWO")
								
								iStartIndex = idx*myLocData.iNumPedsPerCar
								carChaserArgs.pedCarChasers[idx2 + iStartIndex] = CREATE_PED_INSIDE_VEHICLE(carChaserArgs.vehCarChasers[idx], PEDTYPE_CRIMINAL, myLocData.mnPedChasers[idx2], myLocData.PedSeat[idx2])
								
								IF NOT IS_ENTITY_DEAD(carChaserArgs.pedCarChasers[idx2 + iStartIndex])
									SETUP_PED_FOR_CHASE(carChaserArgs.pedCarChasers[idx2 + iStartIndex], carChaserArgs.relCarChaserEnemies)						
									DEBUG_PRINT("TASKING GUYS TO COMBAT")
								ENDIF
					
								// TODO:  Make it so it depends on which seat the ped is in to task differently
								// If they're in the driver's seat, task them with vehicle mission.
		//						TASK_VEHICLE_MISSION(GET_PED_IN_VEHICLE_SEAT(vehCarChasers[0]), vehCarChasers[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_BLOCK, 40, DRIVINGMODE_PLOUGHTHROUGH, -1, 1)
							ENDREPEAT
							
							carChaserArgs.blipCarChasers[idx] = ADD_BLIP_FOR_ENTITY(carChaserArgs.vehCarChasers[idx])
							SET_BLIP_COLOUR(carChaserArgs.blipCarChasers[idx], BLIP_COLOUR_RED)
							
							bIsChaseActive = TRUE
							bWaveActive[iWaveNum] = TRUE
							
							PRINTSTRING("iWaveNum = ")
							PRINTINT(iWaveNum)
							PRINTNL()
							
							DEBUG_PRINT("bIsChaseActive AND bWaveActive = TRUE")
						
//							// Remove drop off blip until car chase is complete	
//							IF DOES_BLIP_EXIST(myLocationBlip[0])
//								REMOVE_BLIP(myLocationBlip[0])
//							ENDIF
							
							bProcede = FALSE
							
							IF NOT IS_TIMER_STARTED(myTimer)
								START_TIMER_NOW(myTimer)
								DEBUG_PRINT("STARTING TIMER")
							ELSE
								RESTART_TIMER_NOW(myTimer)
								DEBUG_PRINT("RESTARTING TIMER")
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC HANDLE_AUDIO_AND_OBJECTIVE_PRINT(structPedsForConversation& MyLocalPedStruct)

	IF NOT bPlayAudioLine
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "OSCAR")
		CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_CC", CONV_PRIORITY_VERY_HIGH)
		bPlayAudioLine = TRUE
	ENDIF
	
	IF bPlayAudioLine AND NOT bPrintObjective
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			// Print objective to lose the car chasers.
			PRINT("DTRSHRD_06", DEFAULT_GOD_TEXT_TIME, 1)
			bPrintObjective = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_CHASE_DIALOGUE(structPedsForConversation& MyLocalPedStruct)
	IF NOT bPlayedChaseDialogue
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, NULL, "TREVOR")
					CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_CCT", CONV_PRIORITY_VERY_HIGH)
					
					PRINTLN("bPlayedChaseDialogue = TRUE")
					bPlayedChaseDialogue = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL UPDATE_CAR_CHASE(CAR_CHASE &carChaseProgress, structTimer &myTimer, BLIP_INDEX &myLocationBlip[], LOCATION_DATA &myLocData, 
CAR_CHASER_ARGS &carChaserArgs, TEXT_LABEL_31& sLastObjective, VECTOR &vCurDestObjective[], BOOL &bCarChaseComplete, BOOL &bIsChaseActive, 
BOOL bInActiveFight, BOOL& bWanted, BOOL& bOutOfVehicle, BLIP_INDEX& myVehicleBlip, BOOL& bDoPoliceChase, MODEL_NAMES& mnVehicleToUse, MODEL_NAMES& mnPedToUse, BOOL& bOkayToRunTrap,
structPedsForConversation& MyLocalPedStruct, INT& outOfVehicleTime, VEHICLE_INDEX& myVehicle, BOOL& bRunHeliChase)

	INT idx
	
	VECTOR vRoadNode
	VECTOR vPlayerPos
	VECTOR vPolicePos
	
	IF bIsChaseActive
//		IF bOutOfVehicle
//			// Remove blips
//			REPEAT COUNT_OF(carChaserArgs.badGuyBlips) idx
//				IF DOES_BLIP_EXIST(carChaserArgs.badGuyBlips[idx])
//					REMOVE_BLIP(carChaserArgs.badGuyBlips[idx])
//					DEBUG_PRINT("REMOVING BLIP badGuyBlips")
//				ENDIF
//			ENDREPEAT
//			
//			REPEAT COUNT_OF(carChaserArgs.blipCarChasers) idx 
//				IF DOES_BLIP_EXIST(carChaserArgs.blipCarChasers[idx])
//					REMOVE_BLIP(carChaserArgs.blipCarChasers[idx])
//					DEBUG_PRINT("REMOVING blipCarChasers")
//				ENDIF
//			ENDREPEAT
//		ELSE
			REPEAT COUNT_OF(carChaserArgs.blipCarChasers) idx
				IF NOT DOES_BLIP_EXIST(carChaserArgs.blipCarChasers[idx])
					IF DOES_ENTITY_EXIST(carChaserArgs.vehCarChasers[idx])
						carChaserArgs.blipCarChasers[idx] = ADD_BLIP_FOR_ENTITY(carChaserArgs.vehCarChasers[idx])
						SET_BLIP_COLOUR(carChaserArgs.blipCarChasers[idx], BLIP_COLOUR_RED)
						SET_BLIP_NAME_FROM_TEXT_FILE(carChaserArgs.blipCarChasers[idx], "DTRFKGR_BLIP04")
					ENDIF
				ENDIF
			ENDREPEAT
//		ENDIF
	ENDIF
	
	SWITCH carChaseProgress
	
		CASE CAR_CHASE_INIT
		
			IF bRunHeliChase
				REQUEST_MODEL(BUZZARD)
				DEBUG_PRINT("REQUESTING BUZZARD")
			ELSE
				REQUEST_MODEL(myLocData.mnCarChasers[0])
				DEBUG_PRINT("REQUESTING CAR CHASERS")
			ENDIF
		
			IF bDoPoliceChase
				mnVehicleToUse = POLICE
				mnPedToUse = S_F_Y_COP_01
			ELSE
				IF bRunHeliChase
					mnVehicleToUse = BUZZARD
					IF bUsingMexicans
						mnPedToUse = G_M_M_MEXBOSS_01
						PRINTLN("mnPedToUse = G_M_M_MEXBOSS_01")
					ELIF bUsingMarabunta
						mnPedToUse = G_M_Y_SalvaGoon_03
						PRINTLN("mnPedToUse = G_M_Y_SalvaGoon_03")
					ELIF bUsingHillbillies
						mnPedToUse = A_M_M_HillBilly_01
						PRINTLN("mnPedToUse = A_M_M_HillBilly_01")
					ENDIF
				ELSE
					mnVehicleToUse = myLocData.mnCarChasers[0]
					IF bUsingMexicans
						mnPedToUse = G_M_M_MEXBOSS_01
						PRINTLN("mnPedToUse = G_M_M_MEXBOSS_01")
					ELIF bUsingMarabunta
						mnPedToUse = G_M_Y_SalvaGoon_03
						PRINTLN("mnPedToUse = G_M_Y_SalvaGoon_03")
					ELIF bUsingHillbillies
						mnPedToUse = A_M_M_HillBilly_01
						PRINTLN("mnPedToUse = A_M_M_HillBilly_01")
					ENDIF
				ENDIF
			ENDIF
			
			REQUEST_MODEL(mnPedToUse)
		
			REPEAT myLocData.iNumWaves idx
				IF bWaveActive[idx]
					bWaveActive[idx] = FALSE
				ENDIF
			ENDREPEAT
			
			IF bRunHeliChase
				IF HAS_MODEL_LOADED(BUZZARD)
				AND HAS_MODEL_LOADED(mnPedToUse)
					carChaseProgress = CAR_CHASE_TRIGGER
				ELSE
					PRINTLN("WAITING FOR BUZZARD TO LOAD")
				ENDIF
			ELSE
				IF HAS_MODEL_LOADED(mnVehicleToUse)
				AND HAS_MODEL_LOADED(mnPedToUse)
					carChaseProgress = CAR_CHASE_TRIGGER
				ELSE
					PRINTLN("WAITING FOR mnVehicleToUse TO LOAD")
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------------------
		CASE CAR_CHASE_TRIGGER
			IF NOT bInActiveFight
				vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				
				// Check if the player is on a valid road... to begin car chase.
				IF GET_CLOSEST_VEHICLE_NODE(vPlayerPos, vRoadNode) 
					IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vRoadNode) < 5
			
						DEBUG_PRINT("PLAYER IS IN DEFINED AREA FOR CAR CHASE")
						RESTART_TIMER_NOW(myTimer)
						
						DEBUG_PRINT("SETTING RELATIONSHIP GROUPS")
					
						carChaseProgress = CAR_CHASE_SPAWN_CARS 
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//----------------------------------------------------------------------------------------------------------------------------------
		CASE CAR_CHASE_SPAWN_CARS
			UPDATE_CAR_CHASE_SPAWNS(myLocData, myTimer, carChaserArgs, myLocationBlip, bIsChaseActive, bDoPoliceChase, 
			mnVehicleToUse, mnPedToUse, bRunHeliChase)
			
			IF bIsChaseActive
				IF NOT IS_TIMER_STARTED(carChaserArgs.tChaseActiveTimer)
					START_TIMER_NOW(carChaserArgs.tChaseActiveTimer)	
				ENDIF
				
				bOkayToRunTrap = TRUE
				DEBUG_PRINT("SETTING bOkayToRunTrap = TRUE VIA CAR CAR CHASE")
				
				vCurDestObjective[0] = <<0,0,0>>
				PRINTLN("CAR CHASE: SETTING vCurDestObjective[0] = <<0,0,0>>")
			
				IF NOT bDoPoliceChase
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					DEBUG_PRINT("SETTING WANTED MULTIPLIER TO 0.0 VIA CAR CHASE")
				ENDIF
				
				DEBUG_PRINT("GOING TO STATE - CAR_CHASE_CHASE")
				carChaseProgress = CAR_CHASE_CHASE 
			ENDIF
		BREAK
		
		//----------------------------------------------------------------------------------------------------------------------------------
		
		CASE CAR_CHASE_CHASE
			
			IF NOT bCopChaseActive
				IF bDoPoliceChase
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID()) 
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehExtraCop[0])
						vPolicePos = GET_ENTITY_COORDS(vehExtraCop[0])
					ENDIF
				
					IF IS_VEHICLE_DRIVEABLE(vehExtraCop[0])
						IF IS_ENTITY_ON_SCREEN(vehExtraCop[0]) AND NOT IS_ENTITY_OCCLUDED(vehExtraCop[0])
						AND (VDIST(vPlayerPos, vPolicePos) < 100)
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							bCopChaseActive = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			HANDLE_AUDIO_AND_OBJECTIVE_PRINT(MyLocalPedStruct)
			
			UPDATE_CAR_CHASE_SPAWNS(myLocData, myTimer, carChaserArgs, myLocationBlip, bIsChaseActive, bDoPoliceChase, 
			mnVehicleToUse, mnPedToUse, bRunHeliChase)
			
			// AI functions - could be handled with Native functions
//			KEEP_CHASERS_IN_RANGE(myLocData, carChaserArgs)
//			ADJUST_CAR_CHASERS_SPEED(myLocData, carChaserArgs.vehCarChasers, 40)

			HANDLE_CHASE_DIALOGUE(MyLocalPedStruct)
			
			IF CAR_CHASER_PASS_CONDITIONS_MET(myLocData, carChaserArgs, bWanted, bDoPoliceChase, bRunHeliChase)
				DEBUG_PRINT("GOING TO CAR_CHASE_FINAL")
				carChaseProgress = CAR_CHASE_FINAL
			ENDIF		
		BREAK
		
		//----------------------------------------------------------------------------------------------------------------------------------
		
		CASE CAR_CHASE_FINAL
		
			// Remove blips
			REPEAT COUNT_OF(carChaserArgs.badGuyBlips) idx
				IF DOES_BLIP_EXIST(carChaserArgs.badGuyBlips[idx])
					REMOVE_BLIP(carChaserArgs.badGuyBlips[idx])
					DEBUG_PRINT("REMOVING BLIP badGuyBlips")
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(carChaserArgs.blipCarChasers) idx 
				IF DOES_BLIP_EXIST(carChaserArgs.blipCarChasers[idx])
					REMOVE_BLIP(carChaserArgs.blipCarChasers[idx])
					DEBUG_PRINT("REMOVING blipCarChasers")
				ENDIF
			ENDREPEAT
			
			IF DOES_ENTITY_EXIST(carChaserArgs.vehCarChasers[0])
				IF IS_ENTITY_OCCLUDED(carChaserArgs.vehCarChasers[0])
					DELETE_VEHICLE(carChaserArgs.vehCarChasers[0])
					PRINTLN("DELETING VEHICLE - carChaserArgs.vehCarChasers[0]")
					
					DELETE_PED(carChaserArgs.pedCarChasers[0])
					PRINTLN("DELETING PED - carChaserArgs.pedCarChasers[0]")
					
					DELETE_PED(carChaserArgs.pedCarChasers[1])
					PRINTLN("DELETING PED - carChaserArgs.pedCarChasers[1]")
				ELSE
					SET_VEHICLE_AS_NO_LONGER_NEEDED(carChaserArgs.vehCarChasers[0])
					PRINTLN("SETTING VEHICLE - carChaserArgs.vehCarChasers[0] AS NO LONGER NEEDED")
					
					SET_PED_AS_NO_LONGER_NEEDED(carChaserArgs.pedCarChasers[0])
					PRINTLN("SETTING PED - carChaserArgs.pedCarChasers[0] AS NO LONGER NEEDED")
					
					SET_PED_AS_NO_LONGER_NEEDED(carChaserArgs.pedCarChasers[1])
					PRINTLN("SETTING PED - carChaserArgs.pedCarChasers[1] AS NO LONGER NEEDED")
				ENDIF
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
			
			// Add back in drop off blip
			IF NOT bWanted AND NOT bOutOfVehicle
				IF NOT DOES_BLIP_EXIST(myLocationBlip[0])
					myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLocData.endData[0].vDrugPickup)
					IF DOES_BLIP_EXIST(myLocationBlip[0])
//						SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
						SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[0], "DTRFKGR_BLIP07")
						PRINTLN("ADDING IN myLocationBlip[0] VIA CAR CHASE")
					ENDIF
					// Print the last objective
					PRINT("DTRFKGR_03a", 3500,1)
				ENDIF
			ELSE
				IF bOutOfVehicle
					IF NOT DOES_BLIP_EXIST(myVehicleBlip)
						myVehicleBlip = ADD_BLIP_FOR_ENTITY(myVehicle)
						SET_BLIP_COLOUR(myVehicleBlip, BLIP_COLOUR_BLUE)
						PRINTLN("ADDING IN myVehicleBlip VIA CAR CHASE")
						PRINT_NOW("DTRSHRD_03",3500,1)
						sLastObjective = "DTRSHRD_03"
					ENDIF
				ENDIF
			ENDIF
			
			vCurDestObjective[0] = myLocData.endData[0].vDrugPickup
			
			TRIGGER_MUSIC_EVENT("OJDG2_MORE_DEAD")
			PRINTLN("TRIGGERING MUSIC - OJDG2_MORE_DEAD")
			
			bGrabbedDistance = FALSE
			
			// Re-setting to fix bug where the player fails when being out of their vehicle for too long.
			outOfVehicleTime = GET_GAME_TIMER()
			
			DEBUG_PRINT("COMPLETED CAR CHASE")
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			DEBUG_PRINT("SETTING WANTED MULTIPLIER TO 1.0 VIA CAR CHASE")
			carChaseProgress = CAR_CHASE_COMPLETE
			bCarChaseComplete = TRUE
			bIsChaseActive = FALSE
			RETURN TRUE
		BREAK
		
		CASE CAR_CHASE_COMPLETE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_GENERIC_CHASE(CAR_CHASE &carChaseProgress, structTimer &myTimer, BLIP_INDEX &myLocationBlip[], LOCATION_DATA &myLocData, 
CAR_CHASER_ARGS &carChaserArgs, TEXT_LABEL_31& sLastObjective, VECTOR &vCurDestObjective[], BOOL &bCarChaseComplete, BOOL &bIsChaseActive, 
BOOL bInActiveFight, BOOL& bWanted)

	INT idx
	
	VECTOR vRoadNode
	VECTOR vPlayerPos
	
	SWITCH carChaseProgress

		CASE CAR_CHASE_TRIGGER
		
			IF NOT bInActiveFight
				vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				
				FIND_CLOSEST_LOCATION_INDEX(myLocData.endData)
				
				// Check if the player is on a valid road... to begin car chase.
				IF GET_CLOSEST_VEHICLE_NODE(vPlayerPos, vRoadNode) 
					IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vRoadNode) < 5
			
						DEBUG_PRINT("PLAYER IS IN DEFINED AREA FOR CAR CHASE")
						RESTART_TIMER_NOW(myTimer)
						
						carChaseProgress = CAR_CHASE_SPAWN_CARS 
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//----------------------------------------------------------------------------------------------------------------------------------
		
		CASE CAR_CHASE_SPAWN_CARS
			UPDATE_CAR_CHASE_SPAWNS(myLocData, myTimer, carChaserArgs, myLocationBlip, bIsChaseActive)
			IF bIsChaseActive
				
				IF NOT IS_TIMER_STARTED(carChaserArgs.tChaseActiveTimer)
					START_TIMER_NOW(carChaserArgs.tChaseActiveTimer)
					DEBUG_PRINT("STARTING tChaseActiveTimer TIMER FOR TRAP")
				ENDIF
				
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				DEBUG_PRINT("SETTING WANTED MULTIPLIER TO 0.0 VIA CAR CHASE")
				DEBUG_PRINT("GOING TO STATE - CAR_CHASE_CHASE")
				carChaseProgress = CAR_CHASE_CHASE 
			ENDIF
		BREAK
		
		//----------------------------------------------------------------------------------------------------------------------------------
		
		CASE CAR_CHASE_CHASE
			
			UPDATE_CAR_CHASE_SPAWNS(myLocData, myTimer, carChaserArgs, myLocationBlip, bIsChaseActive)
			
			KEEP_CHASERS_IN_RANGE(myLocData, carChaserArgs)
			
			ADJUST_CAR_CHASERS_SPEED(myLocData, carChaserArgs.vehCarChasers, 40)
			
			IF CAR_CHASER_PASS_CONDITIONS_MET(myLocData, carChaserArgs, bWanted)
				DEBUG_PRINT("GOING TO CAR_CHASE_FINAL")
				carChaseProgress = CAR_CHASE_FINAL
			ENDIF		
		BREAK
		
		//----------------------------------------------------------------------------------------------------------------------------------
		
		CASE CAR_CHASE_FINAL
			
			// Remove blips
			REPEAT COUNT_OF(carChaserArgs.badGuyBlips) idx
				IF DOES_BLIP_EXIST(carChaserArgs.badGuyBlips[idx])
					REMOVE_BLIP(carChaserArgs.badGuyBlips[idx])
					DEBUG_PRINT("REMOVING BLIP badGuyBlips")
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(carChaserArgs.blipCarChasers) idx 
				IF DOES_BLIP_EXIST(carChaserArgs.blipCarChasers[idx])
					REMOVE_BLIP(carChaserArgs.blipCarChasers[idx])
					DEBUG_PRINT("REMOVING blipCarChasers")
				ENDIF
			ENDREPEAT
			
			// Print the last objective
			PRINT(sLastObjective, DEFAULT_GOD_TEXT_TIME,1)
			
			// Add back in drop off blip
			IF NOT bWanted
				IF NOT DOES_BLIP_EXIST(myLocationBlip[0])
					myLocationBlip[0] = ADD_BLIP_FOR_COORD(vCurDestObjective[0])
//					SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
				ENDIF
			ENDIF
			
			DEBUG_PRINT("COMPLETED CAR CHASE")
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			DEBUG_PRINT("SETTING WANTED MULTIPLIER TO 1.0 VIA CAR CHASE")
			carChaseProgress = CAR_CHASE_COMPLETE
			bCarChaseComplete = TRUE
			bIsChaseActive = FALSE
			RETURN TRUE
		BREAK
		
		CASE CAR_CHASE_COMPLETE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//FUNC BOOL CREATE_STAGED_CAR_CHASE(LOCATION_DATA& myLocData, STAGED_CAR_CHASE_ARGS& stagedCarChaseArgs)
//
//	INT idx
//	
//	REPEAT MAX_NUMBER_STAGED_CHASE_VEHICLES idx
//		stagedCarChaseArgs.vehStagedCarChase[idx] = CREATE_VEHICLE(myLocData.mnStagedCarChase[idx], myLocData.dropData[0].vStagedPos[idx], myLocData.dropData[0].fStagedHeading[idx])
//		SET_VEHICLE_ENGINE_ON(stagedCarChaseArgs.vehStagedCarChase[idx], TRUE, TRUE)
//		SET_VEHICLE_ON_GROUND_PROPERLY(stagedCarChaseArgs.vehStagedCarChase[idx])
//		SET_VEHICLE_CAN_BE_TARGETTED(stagedCarChaseArgs.vehStagedCarChase[idx], TRUE)
//		SET_VEHICLE_CAN_LEAK_OIL(stagedCarChaseArgs.vehStagedCarChase[idx], TRUE)
//		SET_VEHICLE_CAN_LEAK_PETROL(stagedCarChaseArgs.vehStagedCarChase[idx], TRUE)
//		DEBUG_PRINT("CREATING STAGED VEHICLES")
//		
//		IF IS_VEHICLE_DRIVEABLE(stagedCarChaseArgs.vehStagedCarChase[idx])
//			stagedCarChaseArgs.pedStagedCarChase[idx] = CREATE_PED_INSIDE_VEHICLE(stagedCarChaseArgs.vehStagedCarChase[idx], PEDTYPE_CRIMINAL, myLocData.actorModelArray[idx])
//			SET_PED_COMBAT_ABILITY(stagedCarChaseArgs.pedStagedCarChase[idx], CAL_AVERAGE)
//			SET_PED_COMBAT_RANGE(stagedCarChaseArgs.pedStagedCarChase[idx], CR_FAR)
//			SET_PED_COMBAT_ATTRIBUTES(stagedCarChaseArgs.pedStagedCarChase[idx], CA_FLEE_WHILST_IN_VEHICLE, TRUE)
//			SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(stagedCarChaseArgs.pedStagedCarChase[idx], TRUE)
//			SET_PED_KEEP_TASK(stagedCarChaseArgs.pedStagedCarChase[idx], TRUE)
//			SET_PED_COMBAT_ATTRIBUTES(stagedCarChaseArgs.pedStagedCarChase[idx], CA_LEAVE_VEHICLES, FALSE)
//			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(stagedCarChaseArgs.pedStagedCarChase[idx], TRUE)
//			SET_PED_RELATIONSHIP_GROUP_HASH(stagedCarChaseArgs.pedStagedCarChase[idx], stagedCarChaseArgs.relStagedCarChaserEnemies)
//			GIVE_WEAPON_TO_PED(stagedCarChaseArgs.pedStagedCarChase[idx], WEAPONTYPE_SMG, 1000, TRUE)
//		ENDIF
//		
//	ENDREPEAT
//	
//	IF DOES_ENTITY_EXIST(stagedCarChaseArgs.pedStagedCarChase[0])
//
//		stagedCarChaseArgs.bStagedCarChaseCreated = TRUE
//		DEBUG_PRINT("stagedCarChaseArgs.bStagedCarChaseCreated = TRUE")
//	
//		RETURN TRUE
//	ELSE
//		RETURN FALSE
//	ENDIF
//ENDFUNC
//
//FUNC BOOL UPDATE_STAGED_CAR_CHASE(LOCATION_DATA& myLocData, STAGED_CAR_CHASE_ARGS& stagedCarChaseArgs, BLIP_INDEX &myLocationBlip[], 
//TEXT_LABEL_31& sLastObjective, VECTOR &vCurDestObjective[], STAGED_CAR_CHASE& stagedCarChaseStages)
//
//	BOOL bAllDead = TRUE
//	BOOL bOutofRange = TRUE
//	VECTOR vPlayerPos
//	VECTOR vStagedTriggerSize = <<30,30,30>>
//	INT idx
//	
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
//	ENDIF
//	
//	SWITCH stagedCarChaseStages
//		CASE STAGED_CAR_CHASE_STAGE_01
//		
//			IF NOT stagedCarChaseArgs.bStagedCarChaseActive
//				REPEAT MAX_NUMBER_STAGED_CHASE_VEHICLES idx
//					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), myLocData.dropData[0].vStagedTriggerPos, vStagedTriggerSize) 
////					OR (IS_ENTITY_ON_SCREEN(stagedCarChaseArgs.vehStagedCarChase[idx]) AND NOT IS_ENTITY_OCCLUDED(stagedCarChaseArgs.vehStagedCarChase[idx]))
//					
//						// Remove drop off blip until car chase is complete	
//						IF DOES_BLIP_EXIST(myLocationBlip[0])
//							REMOVE_BLIP(myLocationBlip[0])
//							DEBUG_PRINT("REMOVING myLocationBlip[0] IN STAGED CAR CHASE CREATION")
//						ENDIF
//						
//						PRINT_NOW("DTRSHRD_02", DEFAULT_GOD_TEXT_TIME, 1)
//											
//						stagedCarChaseArgs.bStagedCarChaseActive = TRUE
//						DEBUG_PRINT("stagedCarChaseArgs.bStagedCarChaseActive = TRUE")
//						
//						DEBUG_PRINT("INSIDE STATE - STAGED_CAR_CHASE_STAGE_01")
//						stagedCarChaseStages = STAGED_CAR_CHASE_STAGE_02
//					ENDIF
//				ENDREPEAT
//			ENDIF
//		BREAK
//		//================================================================================================================================
//		CASE STAGED_CAR_CHASE_STAGE_02
//			REPEAT MAX_NUMBER_STAGED_CHASE_VEHICLES idx
//				// Task the peds
//				IF IS_VEHICLE_DRIVEABLE(stagedCarChaseArgs.vehStagedCarChase[idx])
//					IF NOT IS_PED_INJURED(stagedCarChaseArgs.pedStagedCarChase[idx])
//						TASK_VEHICLE_MISSION(stagedCarChaseArgs.pedStagedCarChase[idx], stagedCarChaseArgs.vehStagedCarChase[idx], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_ESCORT_LEFT, 70, DRIVINGMODE_AVOIDCARS, -1, -1)
//						ADD_VEHICLE_SUBTASK_ATTACK_PED(stagedCarChaseArgs.pedStagedCarChase[idx], PLAYER_PED_ID())
//					ENDIF
//					// Add blips to the peds
//					IF NOT DOES_BLIP_EXIST(stagedCarChaseArgs.blipStagedCarChasePeds[idx])
//						stagedCarChaseArgs.blipStagedCarChasePeds[idx] = ADD_BLIP_FOR_ENTITY(stagedCarChaseArgs.pedStagedCarChase[idx])
//						DEBUG_PRINT("ADDING BLIPS ON STAGED CAR CHASER PEDS")
//					ENDIF
//				ENDIF
//			ENDREPEAT
//			
//			DEBUG_PRINT("INSIDE STATE - STAGED_CAR_CHASE_STAGE_02")
//			stagedCarChaseStages = STAGED_CAR_CHASE_STAGE_03
//		BREAK
//		//================================================================================================================================
//		CASE STAGED_CAR_CHASE_STAGE_03
//			IF stagedCarChaseArgs.bStagedCarChaseActive
//				REPEAT MAX_NUMBER_STAGED_CHASE_VEHICLES idx
//					IF IS_VEHICLE_DRIVEABLE(stagedCarChaseArgs.vehStagedCarChase[idx])
//						IF NOT IS_PED_INJURED(stagedCarChaseArgs.pedStagedCarChase[idx])
//							bAllDead = FALSE
//						ELSE
//							IF DOES_BLIP_EXIST(stagedCarChaseArgs.blipStagedCarChasePeds[idx])
//								REMOVE_BLIP(stagedCarChaseArgs.blipStagedCarChasePeds[idx])
//								DEBUG_PRINT("REMOVING BLIPS ON STAGED PEDS BECAUSE INJURED")
//							ENDIF
//						ENDIF
//						
//						IF VDIST(vPlayerPos, GET_ENTITY_COORDS(stagedCarChaseArgs.vehStagedCarChase[idx])) < DISTANCE_TO_LOSE_STAGED_CHASERS
//							bOutofRange = FALSE
//						ENDIF
//					ENDIF
//				ENDREPEAT
//				
//				IF bAllDead OR bOutofRange
//					IF NOT DOES_BLIP_EXIST(myLocationBlip[0])
//						myLocationBlip[0] = ADD_BLIP_FOR_COORD(vCurDestObjective[0])
//						SET_BLIP_ROUTE(myLocationBlip[0], TRUE)
//						DEBUG_PRINT("ADDING BACK IN myLocationBlip[0] IN UPDATE_STAGED_CAR_CHASE")
//					ENDIF
//					
//					REPEAT COUNT_OF(stagedCarChaseArgs.blipStagedCarChasePeds) idx
//						IF DOES_BLIP_EXIST(stagedCarChaseArgs.blipStagedCarChasePeds[idx])
//							REMOVE_BLIP(stagedCarChaseArgs.blipStagedCarChasePeds[idx])
//							DEBUG_PRINT("REMOVING BLIP blipStagedCarChaserPeds")
//						ENDIF
//					ENDREPEAT
//					
//					// Print the last objective
//					PRINT(sLastObjective, DEFAULT_GOD_TEXT_TIME, 1)
//				
//					stagedCarChaseArgs.bStagedCarChaseActive = FALSE
//					
//					DEBUG_PRINT("INSIDE STATE - STAGED_CAR_CHASE_STAGE_03")
//					stagedCarChaseStages = STAGED_CAR_CHASE_STAGE_04
//				ENDIF
//			ENDIF
//		BREAK
//		//================================================================================================================================
//		CASE STAGED_CAR_CHASE_STAGE_04
//			DEBUG_PRINT("RETURNING TRUE ON UPDATE_STAGED_CAR_CHASE")
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//
//	RETURN FALSE
//ENDFUNC


