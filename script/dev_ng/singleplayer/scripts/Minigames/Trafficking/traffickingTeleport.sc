

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// traffickingTeleport.sc
USING "minigames_helpers.sch"
USING "code_control_public.sch"

SCRIPT
	PED_INDEX playerEntity = GET_PLAYER_PED(GET_PLAYER_INDEX())
	IF NOT IS_ENTITY_DEAD(playerEntity)
		SET_ENTITY_COORDS(playerEntity, << 2119.1587, 4813.5225, 40.2223 >>)
		SET_ENTITY_HEADING(playerEntity, 225.2837)
	ENDIF
	
	Execute_Code_ID(CID_ACTIVATE_MINIGAME_TRAFFICKING)
ENDSCRIPT
