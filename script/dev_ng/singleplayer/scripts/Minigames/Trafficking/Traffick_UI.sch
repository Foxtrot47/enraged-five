//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     : Traffick UI                                       		    //
//      AUTHOR          : Steven Messinger                                              //
//      DESCRIPTION     : Contains all things HUD, UI, BLIP in AIR trafficking       	//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "minigame_stats_tracker_helpers.sch"
USING "Drug_trafficking_Data.sch"
USING "script_oddjob_funcs.sch"
USING "script_oddjob_queues.sch"
USING "shared_hud_displays.sch"
USING "end_screen.sch"

CONST_INT PENATLY_NORMAL -50
CONST_INT LAND_RADIUS 11
CONST_INT EXPLODE_RADIUS 22
CONST_INT EXPLODE_RADIUS_CARPET 17

END_SCREEN_DATASET airTraffickingEndScreen

COMPLEX_USE_CONTEXT cucEndScreenAir

// UI Update positions
//FLOAT foffSet = 0.030
//FLOAT fBoxPos = 0.194
//FLOAT fLittleBoxPos = 0.200
//FLOAT fBackground = 0.038
//FLOAT fBackOffset = 0.040


// Define colors
//INT iWhite = INT_COLOUR(255, 255, 255, 255)
//INT iRed = INT_COLOUR(255, 0, 0, 255)
//INT iGreen = INT_COLOUR(0, 255, 0, 255)
//INT iYellow = INT_COLOUR(255, 255, 0, 255)
//INT iBlack = INT_COLOUR(0, 0, 0, 255)
//INT iGold = INT_COLOUR(218, 165, 32, 255)
//INT curColor[MAX_NUMBER_PACKAGES]

INT soundIdAlarm = -1

//HUD_COLOURS hcPass[5]

//INT currentReward

//INT flashCount

//BLIP_INDEX blipRadiusPerfect[MAX_NUMBER_DROPS]

//BOOL bProximity = FALSE
BOOL bSideUIExplained = FALSE
BOOL bPrintRudderHelp = FALSE
BOOL bPlayerUsedRudders = FALSE
BOOL bBombBayDoorsOpen = FALSE
BOOL bTriggerNearTrainMusic = FALSE
BOOL bTriggerNearBoatMusic = FALSE
BOOL bAddAirportBlip = FALSE
BOOL bCheckTime = FALSE
BOOL bSkippedEndingScreen = FALSE
BOOL bPlayMusic = FALSE

FLOAT fBombBayTime
FLOAT fBlipChangingDistance = 200

INT iPayment

structTimer tRudderHelpTimer

ENUM CARGO_DROP_UI_STATES
	CARGO_DROP_UI_STATES_INIT = 0,
	CARGO_DROP_UI_STATES_PREPARE,
	CARGO_DROP_UI_STATES_WAITING,
	CARGO_DROP_UI_STATES_RUNNING
ENDENUM


PROC HANDLE_SIDE_UI_EXPLANATION(ARGS& myArgs)
	UNUSED_PARAMETER(myArgs)
	DEBUG_MESSAGE("HANDLE_SIDE_UI_EXPLANATION")
	// Only display help text if you're in the right mode
	IF NOT bSideUIExplained 
//		IF myArgs.bDoStationary OR myArgs.bDoBomb OR myArgs.bDoMobile
		//CLEAR_HELP(TRUE)
		//PRINT_HELP("DTRFAIR_HELPJ")
		bSideUIExplained = TRUE
			//ENDIF
//		ELSE
//			CLEAR_HELP(TRUE)
//			//IF NOT IS_HELP_MESSAGE_ON_SCREEN()
//				DEBUG_MESSAGE("PRINT HELP: DTRFAIR_HELPC")
//				PRINT_HELP("DTRFAIR_HELPC")
//				bSideUIExplained = TRUE
//			//ENDIF
//		ENDIF
	ELSE
		DEBUG_MESSAGE("bSideUIExplained = TRUE")
	ENDIF
ENDPROC

PROC CALCULATE_CARGO_BONUS(ARGS& myArgs, PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData, FLOAT fBonusT)
	//BOOL bSucess = TRUE
	BOOL bAwardDistanceBonus = TRUE
	BOOL bAwardAllDropsBonus = TRUE
	
	PRINTLN("myData.iNumDrops: ", myData.iNumDrops, "cargoArgs.iNumReleased: ", cargoArgs.iNumReleased)
	
	IF myData.iNumDrops <> cargoArgs.iNumSuccessfulDrops
		PRINTLN("iNumDrops != iNumSuccessfulDrops ", "Not giving an all drop bonus")
		bAwardAllDropsBonus = FALSE
	ENDIF
	
	PRINTLN("NUMBER DROPS = ", myData.iNumDrops)
	PRINTLN("NUMBER RELEASED = ", cargoArgs.iNumReleased)
	
	IF myData.iNumDrops <> cargoArgs.iNumReleased
		bAwardDistanceBonus = FALSE
	ENDIF
	
//	REPEAT cargoArgs.iNumReleased idx	
//		IF NOT cargoArgs.bCargoHeightBonus[idx] AND cargoArgs.bCargoSuccessful[idx]
//			PRINTLN("One of our drops didn't have bCargoHeightBonus = TRUE.. at index: ", idx, "Not giving heright bonus", " NumReleased: ", cargoArgs.iNumReleased)
//			bAwardHeightBonus = FALSE
//		ENDIF	
//	ENDREPEAT
	
	IF myArgs.bDoLowAlt
		IF NOT cargoArgs.bDetectedOnRadar 
			cargoArgs.iHeightBonus = BONUS_MEDIUM
		ENDIF
	ENDIF
	
	IF bAwardDistanceBonus 
		PRINTLN("Giving a distance bonus")
		cargoArgs.iDistanceBonus = BONUS_MEDIUM
	ENDIF
	
	IF bAwardAllDropsBonus
		cargoArgs.iAllDropBonus = BONUS_MEDIUM
	ENDIF
	
	IF GET_TIMER_IN_SECONDS(cargoArgs.tTotalTime) < fBonusT AND cargoArgs.bPassed
		cargoArgs.iTimeBonus = BONUS_MEDIUM
	ELSE
		PRINTLN("We didn't hit our bonus time of: ", fBonusT, " with a time of: ", GET_TIMER_IN_SECONDS(cargoArgs.tTotalTime))
		cargoArgs.iTimeBonus = 0
	ENDIF
	
ENDPROC

//PROC DISPLAY_TIME_INFO(PLANE_CARGO_ARGS& cargoArgs, FLOAT& fDropNum, INT& idx)
//
//	SET_TEXT_SCALE(0.75, 0.75)
//	DISPLAY_TEXT(0.175, fDropNum, "DTRFAIR_UI03")
//	
//	fDropNum = fRowPos + (idx*fRowOffset)
//	idx += 1
//	SET_TEXT_SCALE(0.5, 0.5) 
//	SET_TEXT_COLOUR(cargoArgs.iTimeColorR, cargoArgs.iTimeColorG, cargoArgs.iTimeColorB, 255)
//	DISPLAY_TEXT(0.175, fDropNum, "DTRFAIR_UI06")
//	SET_TEXT_SCALE(0.5, 0.5) 
//	SET_TEXT_COLOUR(cargoArgs.iTimeColorR, cargoArgs.iTimeColorG, cargoArgs.iTimeColorB, 255)
//	DISPLAY_TEXT_WITH_3_NUMBERS(0.279, fDropNum, "DTRSHRD_TIMER", 0, iMinutes, iSeconds)
//	
//	
//	SET_TEXT_SCALE(0.5, 0.5) 
//	SET_TEXT_COLOUR(cargoArgs.iTimeColorR, cargoArgs.iTimeColorG, cargoArgs.iTimeColorB, 255)
//	DISPLAY_TEXT_WITH_NUMBER(0.583, fDropNum, "DTRFAIR_PMONEY", cargoArgs.iTimeBonus)
//	
//	SET_TEXT_SCALE(0.5, 0.5) 
//	SET_TEXT_COLOUR(cargoArgs.iTimeColorR, cargoArgs.iTimeColorG, cargoArgs.iTimeColorB, 255)
//	DISPLAY_TEXT(0.409, fDropNum, cargoArgs.sTimeBonus)	
//ENDPROC

//PROC DISPLAY_PROGRESSION_INFO(PLANE_DATA& myData, PLANE_CARGO_ARGS& cargoArgs, FLOAT& fDropNum, INT& idx)
//			
//	//Display pass or fail info
//	SET_TEXT_SCALE(0.5, 0.5) 
//	DISPLAY_TEXT(0.405, fDropNum, "DTRFAIR_UI09")
//	SET_TEXT_SCALE(0.5, 0.5) 
//	DISPLAY_TEXT_WITH_NUMBER(0.583, fDropNum, "DTRFAIR_PMONEY", myData.iMoneyToPass)
//	
//
//	SET_TEXT_SCALE(0.9, 0.9) 
//	IF totalREward >= myData.iMoneyToPass
//		cargoArgs.bPassed = TRUE
//		DISPLAY_TEXT(0.663, fRowPos + ((idx)*fRowOffset) - fRowOffset/2, "DTRSHRD_PASS")
//	ELSE
//		DISPLAY_TEXT(0.663, fRowPos + ((idx)*fRowOffset) - fRowOffset/2, "DTRSHRD_FAIL")
//	ENDIF
//	
//	fDropNum = fRowPos + (idx*fRowOffset)
//	idx += 1
//ENDPROC

PROC DISPLAY_PACKAGE_PRINT(ARGS& myArgs, PLANE_CARGO_ARGS& cargoArgs, INT num, BOOL bDelay = FALSE)
	//Added these prints now that we don't have UI to represent it
	cargoArgs.bDroppedOne = TRUE
	//if no more packages override bDelay to false
	//IF num = 0
		bDelay = FALSE
	//ENDIF
	IF myArgs.bDoBomb
		//bombs left
		IF bDelay
			REGISTER_PRINT_DELAY(cargoArgs.tHelpPrintTimer, "DTRFAIR_BLFT")
		ELSE
			CLEAR_HELP()
			PRINT_HELP_WITH_NUMBER("DTRFAIR_BLFT", num)
		ENDIF
	ELSE
		//packages left
		IF bDelay
			REGISTER_PRINT_DELAY(cargoArgs.tHelpPrintTimer, "DTRFAIR_PLFT")
		ELSE
			CLEAR_HELP()
			PRINT_HELP_WITH_NUMBER("DTRFAIR_PLFT", num)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DISPLAY_ENDING_UI(PLANE_CARGO_ARGS &cargoArgs, PLANE_DATA& myData, FLOAT fBonusT, CARGO_DROP_UI_STATES &cargoDropUIStates, ARGS& myArgs, BOOL bSkip = FALSE, BOOL bHideRadar = TRUE)
	//TODO... all these variables should be consts..
	//TODO!!! All the math should only be done once not every frame to display the menu

	//INT idx
	INT iStatsTime

	FLOAT fTotalPayment
	FLOAT fTime
	BOOL bDoneRenderingEndScreen
	//FLOAT fBonusPayment

	SWITCH cargoDropUIStates
	
		CASE CARGO_DROP_UI_STATES_INIT
			CLEAR_HELP()
			
			IF bHideRadar 
				DISPLAY_RADAR(FALSE)
			ENDIF
			
			IF cargoArgs.bPassed AND NOT bSkip
				CLEAR_PRINTS()
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			CALCULATE_CARGO_BONUS(myArgs, cargoArgs, myData, fBonusT)
			
//			fTotalPayment += (cargoArgs.iNumSuccessfulDrops * REWARD_MEDIUM)
			fTotalPayment += (7000)
			
			PRINTLN("cargoArgs.iHeightBonus = ", cargoArgs.iHeightBonus)
			PRINTLN("cargoArgs.iDistanceBonus = ", cargoArgs.iDistanceBonus)
			PRINTLN("cargoArgs.iAllDropBonus = ", cargoArgs.iAllDropBonus)
			PRINTLN("cargoArgs.iTimeBonus = ", cargoArgs.iTimeBonus)
			
			fTotalPayment += cargoArgs.iHeightBonus + cargoArgs.iDistanceBonus + cargoArgs.iTimeBonus
			
			IF cargoArgs.bPassed
				//Award money on pass.. we used to deduct for missing arms packages.. look at PLANE_STATE_DONE
				iPayment = ROUND(fTotalPayment)
				PRINTLN("PAYMENT = ", ROUND(fTotalPayment))
				SET_ENDSCREEN_DATASET_HEADER(airTraffickingEndScreen, "DTRSHRD_MPASS", "DTRSHRD_ART")
			ELSE
				cargoArgs.iHeightBonus = 0
				cargoArgs.iDistanceBonus = 0
				cargoArgs.iAllDropBonus = 0
				cargoArgs.iTimeBonus = 0
				fTotalPayment = 0
				SET_ENDSCREEN_DATASET_HEADER(airTraffickingEndScreen, "DTRFAIR_UIF", "DTRSHRD_ART")
			ENDIF
				
			IF myArgs.bDoLowAlt
				IF cargoArgs.iHeightBonus > 0
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(airTraffickingEndScreen, ESEF_DOLLAR_VALUE, "DTRFAIR_UI05D", "", cargoArgs.iHeightBonus, 0, ESCM_CHECKED, FALSE)
				ELSE
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(airTraffickingEndScreen, ESEF_DOLLAR_VALUE, "DTRFAIR_UI05D", "", cargoArgs.iHeightBonus, 0, ESCM_NO_MARK, FALSE)
				ENDIF
			ENDIF

			IF cargoArgs.iDistanceBonus > 0
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(airTraffickingEndScreen, ESEF_DOLLAR_VALUE, "DTRFAIR_UI05C", "", cargoArgs.iDistanceBonus, 0, ESCM_CHECKED, FALSE)
			ELSE
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(airTraffickingEndScreen, ESEF_DOLLAR_VALUE, "DTRFAIR_UI05C", "", cargoArgs.iDistanceBonus, 0, ESCM_NO_MARK, FALSE)
			ENDIF
	
			IF cargoArgs.iTimeBonus > 0
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(airTraffickingEndScreen, ESEF_DOLLAR_VALUE, "DTRFAIR_UI05A", "", cargoArgs.iTimeBonus, 0, ESCM_CHECKED, FALSE)
			ELSE
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(airTraffickingEndScreen, ESEF_DOLLAR_VALUE, "DTRFAIR_UI05A", "", cargoArgs.iTimeBonus, 0, ESCM_NO_MARK, FALSE)
			ENDIF
			
			fTime = GET_TIMER_IN_SECONDS(cargoArgs.tTotalTime)
			PRINTLN("cargoArgs.tTotalTime = ", fTime)
			fTime = (fTime*1000)
			
			// Grab time for Stats Tracking
			iStatsTime = ROUND(fTime)
			PRINTLN("iStatsTime = ", iStatsTime)
			
			PLAYSTATS_ODDJOB_DONE(iStatsTime, ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR))
			PRINTLN("CALLING PLAYSTATS_ODDJOB_DONE - MINIGAME_TRAFFICKING_AIR")
			
			ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(airTraffickingEndScreen, ESEF_TIME_M_S, "DTRFAIR_UI03", "", ROUND(fTime), 0, ESCM_NO_MARK, FALSE)
			CANCEL_TIMER(cargoArgs.tTotalTime)
			
			ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(airTraffickingEndScreen, ESEF_DOLLAR_VALUE, "DTRFAIR_UI06", "", CEIL(fTotalPayment), 0, ESCM_NO_MARK, FALSE)
			
			SET_ENDSCREEN_COMPLETION_LINE_STATE(airTraffickingEndScreen, FALSE, "", 0,0)

			PRINTLN("GOING TO STATE - CARGO_DROP_UI_STATES_PREPARE")
			cargoDropUIStates = CARGO_DROP_UI_STATES_PREPARE
		BREAK
		CASE CARGO_DROP_UI_STATES_PREPARE
			IF NOT bPlayMusic
				PLAY_MISSION_COMPLETE_AUDIO("TREVOR_SMALL_01")
				bPlayMusic = TRUE
			ENDIF
		
			IF ENDSCREEN_PREPARE(airTraffickingEndScreen)
			AND IS_MISSION_COMPLETE_READY_FOR_UI()
			
				INIT_COMPLEX_USE_CONTEXT(cucEndScreenAir, "DTRSHRD_CONT", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT))
				
				SETTIMERB(0)
			
				PRINTLN("GOING TO STATE - CARGO_DROP_UI_STATES_WAITING")
				cargoDropUIStates = CARGO_DROP_UI_STATES_WAITING
			ENDIF
		BREAK
		CASE CARGO_DROP_UI_STATES_WAITING
			
			IF TIMERB() > 7000 AND NOT bSkippedEndingScreen
				UPDATE_COMPLEX_USE_CONTEXT(cucEndScreenAir)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,			INPUT_FRONTEND_ENDSCREEN_ACCEPT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,	INPUT_FRONTEND_PAUSE_ALTERNATE)		// B*2276517 - Player can now press esc to close endscreen, if player control is on during endscreen
					PRINTLN("bSkippedEndingScreen = TRUE")
					bSkippedEndingScreen = TRUE
				ENDIF
			ENDIF
			
			bDoneRenderingEndScreen = RENDER_ENDSCREEN(airTraffickingEndScreen, FALSE)
			
			IF bSkippedEndingScreen OR TIMERB() > 12000
				//Call endscreen transition out
				ENDSCREEN_START_TRANSITION_OUT(airTraffickingEndScreen)
				//Wait until transition out completed
				IF bDoneRenderingEndScreen
					ENDSCREEN_SHUTDOWN(airTraffickingEndScreen)
				
					PRINTLN("cargoDropUIStates = CARGO_DROP_UI_STATES_RUNNING")
			        cargoDropUIStates = CARGO_DROP_UI_STATES_RUNNING
				ENDIF
			ELSE
				PRINTLN("TIMER FAILING")
		    ENDIF
		BREAK
		CASE CARGO_DROP_UI_STATES_RUNNING
			IF bHideRadar 
				ODDJOB_EXIT_CUTSCENE()
			ENDIF
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_HEADING_FROM_COORDS(vector oldCoords,vector newCoords, bool invert=FALSE)
	float heading
	float dX = newCoords.x - oldCoords.x
	float dY = newCoords.y - oldCoords.y
	if dY != 0
		heading = ATAN2(dX,dY)
	ELSE
		if dX < 0
			heading = -90
		ELSE
			heading = 90
		ENDIF
	ENDIF
	
	IF invert = TRUE 	
		heading *= -1.0
		//below not necessary but helps for debugging
		IF heading < 0
			heading += 360.0
		ENDIF
	ENDIF
	
	RETURN heading
ENDFUNC

PROC SET_TEXT_FOR_HUD(int r,int g,int b, float fScale, bool restrictScale=false)
	float fFontScale=0.319
	if restrictScale = TRUE
		if fScale < 0.7
			fScale = 0.7
		ENDIF
		if fScale > 1.5	
			fScale = 1.5
		ENDIF
	ENDIF
	SET_TEXT_SCALE(fFontScale*fScale,fFontScale*fScale)
	SET_TEXT_COLOUR(r,g,b,200)
	SET_TEXT_DROPSHADOW(0,0,0,0,0)
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC

PROC DRAW_RANGE_TEXT(float scale,vector v1, vector v2, float screenX, float screenY, int r, int g, int b)							
	float fRange = GET_DISTANCE_BETWEEN_COORDS(v1,v2,FALSE)
	int iRange = floor(fRange)


	SET_TEXT_FOR_HUD(r,g,b,scale,true)
	
	if iRange < 10
		DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.005,"DTRFAIR_DIST",iRange)
	else 
		if iRange < 100
			DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.005,"DTRFAIR_DIST",iRange)
		ELSE	
			if iRange < 1000								
				DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.005,"DTRFAIR_DIST",iRange)
			ELSE
				DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.005,"DTRFAIR_DIST",iRange)
			endif
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_OFFSCREEN_ARROW_FOR_COORD(vector vDestCoord, int r=255, int g=255, int b=0)
	float fpos
	if DOES_CAM_EXIST(GET_RENDERING_CAM())	
		if not GET_SCREEN_COORD_FROM_WORLD_COORD(vDestCoord,fpos,fpos)
			vector camCoord = GET_CAM_COORD(GET_RENDERING_CAM())
			vector camRot = GET_CAM_ROT(GET_RENDERING_CAM())
			float dist = GET_DISTANCE_BETWEEN_COORDS(<<camCoord.x,camCoord.y,0>>,<<vDestCoord.x,vDestCoord.y,0>>)
			float height = vDestCoord.z - camCoord.z
			float fPitchToPed
			if dist > 0
				fPitchToPed = ATAN(height/dist)
			ELSE
				fPitchToPed = 0
			ENDIF
			
			float fHeadingToPed = GET_HEADING_FROM_COORDS(camCoord,vDestCoord)
			
			//float angleToPoint=ATAN2(COS(lat1)*SIN(lat2)-SIN(lat1)*COS(lat2)*COS(lon2-lon1),SIN(lon2-lon1)*COS(lat2)) 
			float angleToPoint=ATAN2(COS(camRot.x)*SIN(fPitchToPed)-SIN(camRot.x)*COS(fPitchToPed)*COS((fHeadingToPed*-1)-camRot.z),SIN((fHeadingToPed*-1)-camRot.z)*COS(fPitchToPed))
		
			float sX = 0.5 - (cos(angleToPoint)*0.3)
			float sY = 0.5 - (sin(angleToPoint)*0.3)
			
			float stx = 0.5 - (cos(angleToPoint)*0.25)
			float sty = 0.5 - (sin(angleToPoint)*0.25) - 0.02
				
			DRAW_SPRITE("helicopterhud", "hudArrow", sX,sY , 0.02, 0.06, angleToPoint-90, r,g,b, 255)						
			DRAW_RANGE_TEXT(1.0,camCoord,vDestCoord,stx,sty,r,g,b)
		ENDIF
	ENDIF	
ENDPROC

PROC REMOVE_ALL_BLIPS(PLANE_CARGO_ARGS &cargoArgs, BLIP_INDEX& myLocationBlip[], VECTOR& vCurDestObjective[])
	DEBUG_MESSAGE("REMOVE_ALL_BLIPS")
	INT idx
	REPEAT MAX_NUMBER_DROPS idx
		IF DOES_BLIP_EXIST(myLocationBlip[idx])
			REMOVE_BLIP(myLocationBlip[idx])
			vCurDestObjective[idx] = <<0,0,0>>
		ENDIF
		IF DOES_BLIP_EXIST(cargoArgs.blipCrateDrop[idx])
			REMOVE_BLIP(cargoArgs.blipCrateDrop[idx])
		ENDIF
//		IF DOES_BLIP_EXIST(blipRadiusPerfect[idx])
//			REMOVE_BLIP(blipRadiusPerfect[idx])
//		ENDIF
	ENDREPEAT
	REPEAT MAX_NUMBER_FINAL_TARGETS idx
		IF DOES_BLIP_EXIST(cargoArgs.blipFinal[idx])
			REMOVE_BLIP(cargoArgs.blipFinal[idx])
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVE_SPECIFIC_BLIPS(BLIP_INDEX& coorBlip[], BLIP_INDEX& radiusBlip[], BLIP_INDEX& crateBlip[], INT iDrop, INT iCargo)
	DEBUG_MESSAGE("REMOVE_SPECIFIC_BLIPS")
	// Remove Single Dot Dropoff Blip
	IF DOES_BLIP_EXIST(coorBlip[iDrop])
		REMOVE_BLIP(coorBlip[iDrop])
	ENDIF
	// Remove Dropoff radius blip
	IF DOES_BLIP_EXIST(radiusBlip[iDrop])
		REMOVE_BLIP(radiusBlip[iDrop])
	ENDIF
	// Remove package blip
	IF DOES_BLIP_EXIST(crateBlip[iCargo])
		REMOVE_BLIP(crateBlip[iCargo])
	ENDIF
ENDPROC

PROC CLEAR_BLIP_ARRAY(BLIP_INDEX& myLocationBlip[])
	DEBUG_MESSAGE("CLEAR_BLIP_ARRAY")
	INT idx
	REPEAT COUNT_OF(myLocationBlip) idx
		IF DOES_BLIP_EXIST(myLocationBlip[idx])
			REMOVE_BLIP(myLocationBlip[idx])
		ENDIF
	ENDREPEAT
ENDPROC

PROC ALTERNATE_LANDING_BLIP(BLIP_INDEX& myLocationBlip[], ENTITY_INDEX myPlane, VECTOR& myLoc, BOOL& bOutOfVehicle, VECTOR& vPlayerPos, BOOL& bCloseRange)
	// Do not run check if you're out of your vehicle
	IF bOutOfVehicle
		EXIT
	ENDIF

	IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, myLoc) < 200 AND IS_POINT_IN_ANGLED_AREA(vPlayerPos, <<2153.2, 4817.9, 31.06>>, <<1958.6, 4726.4, 51.06>>, 25.0)
	AND GET_ENTITY_HEIGHT_ABOVE_GROUND(myPlane) < 2.0
		IF NOT bCloseRange OR NOT DOES_BLIP_EXIST(myLocationBlip[0])
			IF DOES_BLIP_EXIST(myLocationBlip[0])
				DEBUG_PRINT("REMOVING LOCATION BLIP VIA ALTERNATE_LANDING_BLIP 01")
				REMOVE_BLIP(myLocationBlip[0])
			ENDIF
			
			TRIGGER_MUSIC_EVENT("OJDA1_TAXI")
			PRINTLN("TRIGGERING MUSIC - OJDA1_TAXI")
			
			//add landing bilp
			//Print park plane in the hanger.
			PRINT("DTRFAIR_OBJTX", DEFAULT_GOD_TEXT_TIME, 1)
			myLocationBlip[0] = ADD_BLIP_FOR_COORD(<< 2134.7576, 4779.1909, 40.0159 >>)//, LAND_RADIUS)
			bCloseRange = TRUE
		ENDIF
		
		IF NOT bPlayerUsedRudders
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_RIGHT)
				PRINTLN("PLAYER HAS USED RUDDERS")
				bPlayerUsedRudders = TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_MESSAGE_BEING_DISPLAYED()
			IF NOT bPrintRudderHelp
				IF NOT IS_TIMER_STARTED(tRudderHelpTimer)
					START_TIMER_NOW(tRudderHelpTimer)
					PRINTLN("STARTING TIMER - tRudderHelpTimer")
				ELSE
					IF GET_TIMER_IN_SECONDS(tRudderHelpTimer) > 1
					AND NOT bPlayerUsedRudders
						PRINT_HELP("DTRFAIR_RUDD")
						bPrintRudderHelp = TRUE
						PRINTLN("PRINTING RUDDER HELP")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT bAddAirportBlip
			myLocationBlip[0] = ADD_BLIP_FOR_COORD(myLoc)
			SET_BLIP_SCALE(myLocationBlip[0], 1.0)
			
//			SET_BLIP_SPRITE(myLocationBlip[0], RADAR_TRACE_AIRPORT)
//			SET_BLIP_COLOUR(myLocationBlip[0], BLIP_COLOUR_YELLOW)
//			SET_BLIP_ROTATION(myLocationBlip[0], 120)
//			SET_BLIP_PRIORITY(myLocationBlip[0], BLIPPRIORITY_HIGHEST)
			DEBUG_PRINT("ADDING LOCATION BLIP VIA ALTERNATE_LANDING_BLIP")
			bAddAirportBlip = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC ALTERNATE_FINAL_BLIP(PLANE_CARGO_ARGS& cargoArgs, BLIP_INDEX& myLocationBlip[], STRING sLastObjective, BOOL& bGenericPrint, BOOL bOutOfVehicle)
	INT idx
	//HACK airpot vector hardcoded..
	
	IF NOT bOutOfVehicle //might also need a wanted check here
		IF GET_PLAYER_DISTANCE_FROM_LOCATION(<< 1487.5842, 3161.5088, 39.9267 >>, FALSE) < 800
			IF NOT bGenericPrint
				sLastObjective = sLastObjective
				bGenericPrint = TRUE
				PRINT("DTRFAIR_OBJCF", DEFAULT_GOD_TEXT_TIME, 1)
				sLastObjective = "DTRFAIR_OBJCF"
				
				TRIGGER_MUSIC_EVENT("OJDA5_AT_BASE")
				PRINTLN("TRIGGERING MUSIC - OJDA5_AT_BASE")
			ENDIF
			
			IF DOES_BLIP_EXIST(myLocationBlip[0])
				REMOVE_BLIP(myLocationBlip[0])
			ENDIF
		
			REPEAT MAX_NUMBER_FINAL_TARGETS idx
				IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx]) AND NOT DOES_BLIP_EXIST(cargoArgs.blipFinal[idx])
					cargoArgs.blipFinal[idx] = ADD_BLIP_FOR_ENTITY(cargoArgs.vehFinal[idx])
					SET_BLIP_SCALE(cargoArgs.blipFinal[idx], 0.80)
					SET_BLIP_NAME_FROM_TEXT_FILE(cargoArgs.blipFinal[idx], "DTRFAIR_TGT1")					
					PRINTLN("Calling Register Blipped Entity on index: ", idx, " and on blip id: ")
				ENDIF
			ENDREPEAT
		ELSE
			//PER TODO 426973 we don't ever go back to single blip
//			IF NOT DOES_BLIP_EXIST(myLocationBlip[0])
//				//CLEAR_BLIPPED_ENTITIES()
//				REPEAT MAX_NUMBER_FINAL_TARGETS idx
//					IF DOES_BLIP_EXIST(cargoArgs.blipFinal[idx])
//						REMOVE_BLIP(cargoArgs.blipFinal[idx])
//					ENDIF
//				ENDREPEAT
//				myLocationBlip[0] = ADD_BLIP_FOR_COORD(<< 1487.5842, 3161.5088, 39.9267 >>)
//				SET_BLIP_SCALE(myLocationBlip[0], 1.5)
//				SET_BLIP_COLOUR(myLocationBlip[0], BLIP_COLOUR_YELLOW)
//			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_ENTITY_INDEX(INT& iGroupToUse, PLANE_CARGO_ARGS &cargoArgs)
	INT idx, iIndexToUse
	
	SWITCH iGroupToUse
		CASE 0
			FOR idx = 0 TO 3
				IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx])
					iIndexToUse = idx
					PRINTLN("iIndexToUse = ", idx)
					BREAK
				ENDIF
			ENDFOR
		BREAK
		CASE 1
			FOR idx = 4 TO 7
				IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx])
					iIndexToUse = idx
					PRINTLN("iIndexToUse = ", idx)
					BREAK
				ENDIF
			ENDFOR
		BREAK
		CASE 2
			FOR idx = 8 TO 11
				IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx])
					iIndexToUse = idx
					PRINTLN("iIndexToUse = ", idx)
					BREAK
				ENDIF
			ENDFOR
		BREAK
		DEFAULT 
	ENDSWITCH
	
	PRINTLN("iIndexToUse = ", iIndexToUse)
	RETURN iIndexToUse
ENDFUNC

PROC REMOVE_MULTI_BLIP_BEFORE_ADDING_MAIN_BLIP(PLANE_CARGO_ARGS& cargoArgs)
	INT closestIndex, idx2
	VECTOR vPlayerPosition
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	closestIndex = FIND_CLOSEST_ENTITY_INDEX(cargoArgs.vehDropoff, vPlayerPosition)
	PRINTLN("FROM REMOVE_MULTI_BLIP, closestIndex = ", closestIndex)

	IF closestIndex = 0
		FOR idx2 = 0 TO 3
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx2]) AND DOES_BLIP_EXIST(cargoArgs.blipFinal[idx2])
				REMOVE_BLIP(cargoArgs.blipFinal[idx2])  
				PRINTLN("REMOVING BLIP: ", idx2)
			ENDIF
		ENDFOR
	ELIF closestIndex = 1
		FOR idx2 = 4 TO 7
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx2]) AND DOES_BLIP_EXIST(cargoArgs.blipFinal[idx2])
				REMOVE_BLIP(cargoArgs.blipFinal[idx2])  
				PRINTLN("REMOVING BLIP: ", idx2)
				cargoArgs.bOkayToAddBackInObjBlip[closestIndex] = FALSE
			ENDIF
		ENDFOR
	ELIF closestIndex = 2
		FOR idx2 = 8 TO 11
			IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx2]) AND DOES_BLIP_EXIST(cargoArgs.blipFinal[idx2])
				REMOVE_BLIP(cargoArgs.blipFinal[idx2])  
				PRINTLN("REMOVING BLIP: ", idx2)
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC ALTERNATE_FINAL_BLIP_REPEATABLE(PLANE_CARGO_ARGS& cargoArgs, BLIP_INDEX& myLocationBlip[], STRING sLastObjective, BOOL& bGenericPrint, BOOL bOutOfVehicle)
	INT idx2
	INT closestIndex
	VECTOR vPlayerPosition
	FLOAT fDist
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	/*EXPLANATION:  First we check what is the closest group index, then we remove all the multi blips from the non-closest index, i.e
	other mini-group blips.  This is to ensure we always have IF the player is close enough, 1) one group of mini-blips and 2) 0-2 other 
	group blips.  Once the player is within range, we remove the group blip and blip the mini-blips.  If they go out of range, the
	group blip should return.
	*/
	
	IF NOT bOutOfVehicle //might also need a wanted check here
	
		closestIndex = FIND_CLOSEST_ENTITY_INDEX(cargoArgs.vehDropoff, vPlayerPosition)
//		PRINTLN("closestIndex = ", closestIndex)
		
		IF closestIndex <> -1
			cargoArgs.bOkayToAddBackInObjBlip[closestIndex] = TRUE
		ENDIF
		
		// If closest index is 0, remove groups 1 and 2, i.e. 4-7 and 8-11
		IF closestIndex = 0
			FOR idx2 = 4 TO 11
				IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx2]) AND DOES_BLIP_EXIST(cargoArgs.blipFinal[idx2])
					REMOVE_BLIP(cargoArgs.blipFinal[idx2])  
					PRINTLN("REMOVING BLIP: ", idx2)
					cargoArgs.bOkayToAddBackInObjBlip[closestIndex] = FALSE
				ENDIF
			ENDFOR
		// If the closest index is 1, remove groups 0 and 2, i.e. 0-3 and 8-11
		ELIF closestIndex = 1
			FOR idx2 = 0 TO 3
				IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx2]) AND DOES_BLIP_EXIST(cargoArgs.blipFinal[idx2])
					REMOVE_BLIP(cargoArgs.blipFinal[idx2])  
					PRINTLN("REMOVING BLIP: ", idx2)
					cargoArgs.bOkayToAddBackInObjBlip[closestIndex] = FALSE
				ENDIF
			ENDFOR
			FOR idx2 = 8 TO 11
				IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx2]) AND DOES_BLIP_EXIST(cargoArgs.blipFinal[idx2])
					REMOVE_BLIP(cargoArgs.blipFinal[idx2])  
					PRINTLN("REMOVING BLIP: ", idx2)
					cargoArgs.bOkayToAddBackInObjBlip[closestIndex] = FALSE
				ENDIF
			ENDFOR
		// If the closest index is 2, remove groups 0 and 1, i.e. 0-3 and 4-7
		ELIF closestIndex = 2
			FOR idx2 = 0 TO 7
				IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx2]) AND DOES_BLIP_EXIST(cargoArgs.blipFinal[idx2])
					REMOVE_BLIP(cargoArgs.blipFinal[idx2])  
					PRINTLN("REMOVING BLIP: ", idx2)
					cargoArgs.bOkayToAddBackInObjBlip[closestIndex] = FALSE
				ENDIF
			ENDFOR
		ENDIF
					
		IF closestIndex <> -1
			fDist = GET_DISTANCE_BETWEEN_ENTITIES(cargoArgs.vehDropoff[closestIndex], PLAYER_PED_ID(), FALSE) 
			
			IF fDist < 600
				IF NOT bGenericPrint
					sLastObjective = sLastObjective
					bGenericPrint = TRUE
					sLastObjective = "DTRFAIR_OBJCF"
				ENDIF
				
				IF DOES_BLIP_EXIST(myLocationBlip[closestIndex])
					REMOVE_BLIP(myLocationBlip[closestIndex])
					PRINTLN("REMOVING myLocationBlip: ", closestIndex)
				ENDIF
			
				IF closestIndex = 0
					FOR idx2 = 0 TO 3
						IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx2]) AND NOT DOES_BLIP_EXIST(cargoArgs.blipFinal[idx2])
							cargoArgs.blipFinal[idx2] = ADD_BLIP_FOR_ENTITY(cargoArgs.vehFinal[idx2])
							PRINTLN("ADDING BLIP INDEX: ", idx2)
						ENDIF
					ENDFOR
				ELIF closestIndex = 1
					FOR idx2 = 4 TO 7
						IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx2]) AND NOT DOES_BLIP_EXIST(cargoArgs.blipFinal[idx2])
							cargoArgs.blipFinal[idx2] = ADD_BLIP_FOR_ENTITY(cargoArgs.vehFinal[idx2])
							PRINTLN("ADDING BLIP INDEX: ", idx2)
						ENDIF
					ENDFOR
				ELIF closestIndex = 2
					FOR idx2 = 8 TO 11
						IF NOT IS_ENTITY_DEAD(cargoArgs.vehFinal[idx2]) AND NOT DOES_BLIP_EXIST(cargoArgs.blipFinal[idx2])
							cargoArgs.blipFinal[idx2] = ADD_BLIP_FOR_ENTITY(cargoArgs.vehFinal[idx2])
							PRINTLN("ADDING BLIP INDEX: ", idx2)
						ENDIF
					ENDFOR
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(myLocationBlip[closestIndex])
//					PRINTLN("closestIndex = ", closestIndex)
					
					IF cargoArgs.bOkayToAddBackInObjBlip[closestIndex]
						INT iIndexToUse 
						iIndexToUse = GET_ENTITY_INDEX(closestIndex, cargoArgs)
						
						IF iIndexToUse <> -1
							REMOVE_MULTI_BLIP_BEFORE_ADDING_MAIN_BLIP(cargoArgs)
							myLocationBlip[closestIndex] = ADD_BLIP_FOR_ENTITY(cargoArgs.vehFinal[iIndexToUse])
							SET_BLIP_SCALE(myLocationBlip[closestIndex], 1.0)
							SET_BLIP_COLOUR(myLocationBlip[closestIndex], BLIP_COLOUR_RED)
							PRINTLN("ADDING BACK IN myLocationBlip closestIndex = ", closestIndex)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_MUSIC_NEAR_DROP(PLANE_CARGO_ARGS &cargoArgs, BOOL& bDoBomb)
	
	IF g_savedGlobals.sTraffickingData.iAirRank >= 5
		IF bDoBomb
			TRIGGER_MUSIC_EVENT("OJDA3_HATCH")
			PRINTLN("REPEATABLE BOMB: HANDLE_MUSIC_NEAR_DROP - OJDA3_HATCH")
			
			PRINTLN("REPEATABLE BOMB: HANDLE_MUSIC_NEAR_DROP: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		ELSE
			TRIGGER_MUSIC_EVENT("OJDA2_READY_1ST")
			PRINTLN("REPEATABLE DROP: HANDLE_MUSIC_NEAR_DROP - OJDA2_READY_1ST")
			
			PRINTLN("REPEATABLE DROP: HANDLE_MUSIC_NEAR_DROP: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		ENDIF
	ENDIF
	
	SWITCH g_savedGlobals.sTraffickingData.iAirRank
		CASE 0
			IF cargoArgs.iNumSuccessfulDrops = 1
				TRIGGER_MUSIC_EVENT("OJDA1_READY_2ND")
				PRINTLN("TRIGGERING MUSIC - OJDA1_READY_2ND")
				
				PREPARE_MUSIC_EVENT("OJDA1_2ND_DROPPED")
				PRINTLN("PREPARING MUSIC - OJDA1_2ND_DROPPED")
			ENDIF
			
			PRINTLN("HANDLE_MUSIC_NEAR_DROP: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------
		CASE 1
			IF cargoArgs.iNumSuccessfulDrops = 0
				TRIGGER_MUSIC_EVENT("OJDA2_READY_1ST")
				PRINTLN("TRIGGERING MUSIC - OJDA2_READY_1ST")
			ENDIF
			
			PRINTLN("HANDLE_MUSIC_NEAR_DROP: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------
		CASE 2
		
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------
		CASE 3
			PRINTLN("HANDLE_MUSIC_NEAR_DROP: RANK = ", g_savedGlobals.sTraffickingData.iAirRank)
		BREAK
		//--------------------------------------------------------------------------------------------------------------------------
		CASE 4
		
		BREAK
	ENDSWITCH
	
ENDPROC

//TODO.. shouldn't be doing new blips every frame..
PROC ALTERNATE_BLIP_STYLES(ARGS& myArgs, BLIP_INDEX& myLocationBlip[], VECTOR& vCurDestObjective[], PLANE_DATA& myData, 
PLANE_CARGO_ARGS& cargoArgs, VECTOR& vPlayerPos, BOOL& bDoBomb)
	
	INT idx, closestIndex
	
	closestIndex = FIND_CLOSEST_LOCATION_INDEX(vCurDestObjective, vPlayerPos)
	
	IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCurDestObjective[closestIndex]) < 500
		IF NOT bBombBayDoorsOpen
			IF NOT IS_ENTITY_DEAD(myData.myPlane)
				OPEN_BOMB_BAY_DOORS(myData.myPlane)
				bBombBayDoorsOpen = TRUE
				ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BAY_DOOR_OPEN_MASTER", cargoArgs.soundID, FALSE, myData.myPlane)
			ENDIF
		ENDIF
	ELSE
		IF cargoArgs.bButtonPressed
			IF NOT IS_TIMER_STARTED(cargoArgs.tBombBayTimer)
				START_TIMER_NOW(cargoArgs.tBombBayTimer)
				PRINTLN("STARTING TIMER - cargoArgs.tBombBayTimer")
			ELSE IS_TIMER_STARTED(cargoArgs.tBombBayTimer)
				fBombBayTime = GET_TIMER_IN_SECONDS(cargoArgs.tBombBayTimer)
				
				IF fBombBayTime > 3.0
					IF bBombBayDoorsOpen
						IF NOT IS_ENTITY_DEAD(myData.myPlane)
							CLOSE_BOMB_BAY_DOORS(myData.myPlane)
							PRINTLN("bBombBayDoorsOpen = FALSE - SPECIAL")
							
							ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BAY_DOOR_OPEN_MASTER", cargoArgs.soundID, FALSE, myData.myPlane)
							PRINTLN("CLOSING BOMB BAY DOORS - 04")
							
							bBombBayDoorsOpen = FALSE
							cargoArgs.bButtonPressed = FALSE
							fBombBayTime = 0
							RESTART_TIMER_NOW(cargoArgs.tBombBayTimer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF bBombBayDoorsOpen AND NOT bCheckTime
				IF NOT IS_ENTITY_DEAD(myData.myPlane)
					CLOSE_BOMB_BAY_DOORS(myData.myPlane)
					
					ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_BAY_DOOR_OPEN_MASTER", cargoArgs.soundID, FALSE, myData.myPlane)
					PRINTLN("CLOSING BOMB BAY DOORS - 05")
					
					bBombBayDoorsOpen = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	REPEAT myData.iNumDrops idx
		IF NOT cargoArgs.bCargoDropComplete[idx]
		
			IF myArgs.bDoTrain
				IF idx = 0
					IF NOT IS_ENTITY_DEAD(cargoArgs.vehDropoff[0])
						vCurDestObjective[0] = GET_ENTITY_COORDS(cargoArgs.vehDropoff[0])
					ENDIF
				ELIF idx = 1
					vCurDestObjective[1] = << -1992.4556, 4764.2944, 0 >>
				ENDIF
			ENDIF
		
			IF NOT cargoArgs.bCloseRange[idx]
				IF idx = closestIndex
					IF DOES_BLIP_EXIST(myLocationBlip[idx])
						SET_BLIP_SCALE(myLocationBlip[idx], 1.0)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(myLocationBlip[idx])
						SET_BLIP_SCALE(myLocationBlip[idx], 0.75)
					ENDIF
				ENDIF
			ENDIF
			
//			FLOAT fTempFloat
//			fTempFloat = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCurDestObjective[idx])
//			PRINTLN("fTempFloat = ", fTempFloat)
			
			// Don't run this check on train.
			IF NOT myArgs.bDoTrain
				// Draw radius blip
				IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCurDestObjective[idx], FALSE) < fBlipChangingDistance
				
					//cargoArgs.bWithinRange = TRUE
					IF NOT cargoArgs.bCloseRange[idx] OR NOT DOES_BLIP_EXIST(myLocationBlip[idx])
					
						HANDLE_MUSIC_NEAR_DROP(cargoArgs, bDoBomb)
						
						cargoArgs.bCloseRange[idx] = TRUE
						// Removing coordinate blip and adding in radius blips
						REMOVE_BLIP(myLocationBlip[idx])
						
						IF myArgs.bDoBomb
							myLocationBlip[idx] = ADD_BLIP_FOR_RADIUS(vCurDestObjective[idx], EXPLODE_RADIUS+5)
							SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[idx], "DTRFAIR_TGT")
							SET_BLIP_COLOUR(myLocationBlip[idx], BLIP_COLOUR_RED)
							PRINTLN("ADDED RADIUS BLIP FOR BLIP INDEX: ", idx)
						ELSE
							myLocationBlip[idx] = ADD_BLIP_FOR_RADIUS(vCurDestObjective[idx], DROP_RADIUS)
							SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[idx], "DTRFAIR_DRP")
							SET_BLIP_COLOUR(myLocationBlip[idx], BLIP_COLOUR_YELLOW)
							PRINTLN("ADDED RADIUS BLIP FOR BLIP INDEX: ", idx)
						ENDIF
						
						SET_BLIP_ALPHA(myLocationBlip[idx], 255)
						DEBUG_PRINT("ADDING IN RADIUS BLIP")
					ENDIF
				// Draw smaller blip
				ELSE 
					//cargoArgs.bWithinRange = FALSE
					IF cargoArgs.bCloseRange[idx] OR NOT DOES_BLIP_EXIST(myLocationBlip[idx])
						cargoArgs.bCloseRange[idx] = FALSE
						REMOVE_BLIP(myLocationBlip[idx])
						myLocationBlip[idx] = ADD_BLIP_FOR_COORD(vCurDestObjective[idx])
						IF myArgs.bDoBomb
							SET_BLIP_COLOUR(myLocationBlip[idx], BLIP_COLOUR_RED)
							SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[idx], "DTRFAIR_TGT")
						ELSE
							SET_BLIP_COLOUR(myLocationBlip[idx], BLIP_COLOUR_YELLOW)
							SET_BLIP_NAME_FROM_TEXT_FILE(myLocationBlip[idx], "DTRFAIR_DRP")
						ENDIF
						DEBUG_MESSAGE("Adding normal blip")
					ENDIF
				ENDIF
			ELSE
				// SPECIAL CASE FOR TRAIN
				IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCurDestObjective[0]) < fBlipChangingDistance
					
					IF NOT bTriggerNearTrainMusic
						TRIGGER_MUSIC_EVENT("OJDA4_TRAIN")
						PRINTLN("TRIGGERING MUSIC - OJDA4_TRAIN")
						
						PREPARE_MUSIC_EVENT("OJDA4_TRAIN_HIT")
						PRINTLN("PREPARING MUSIC - OJDA4_TRAIN_HIT")
						
						bTriggerNearTrainMusic = TRUE
					ENDIF
		
				ELIF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCurDestObjective[1]) < fBlipChangingDistance
					
					IF NOT bTriggerNearBoatMusic
						TRIGGER_MUSIC_EVENT("OJDA4_BOATS")
						PRINTLN("TRIGGERING MUSIC - OJDA4_BOATS")
						
						bTriggerNearBoatMusic = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC	

FUNC BOOL UPDATE_HEIGHT_HUD(BOOL& bIsHighAlt, PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& planeData, FLOAT& fHeight, FLOAT& fLastHeight, structTimer& tHighAltTimer, FAIL_REASON_ENUM& failReason, VECTOR& vPlayerPos, structPedsForConversation MyLocalPedStruct)

	VECTOR vPlayerVec
	FLOAT fWaterHeight = 0.0
	FLOAT fIntensity
	
	vPlayerVec = vPlayerPos
	
	//If we can't get a z collision for the ground use the water height instead
	IF NOT GET_GROUND_Z_FOR_3D_COORD(vPlayerVec, fHeight)
		IF NOT GET_WATER_HEIGHT_NO_WAVES(vPlayerVec, fHeight)
			fHeight = (cargoArgs.fActualMaxHeight-1)
		ENDIF
	ENDIF
	
	IF GET_WATER_HEIGHT_NO_WAVES(vPlayerVec, fWaterHeight)
		IF fWaterHeight > fHeight
			fHeight = fWaterHeight
		ENDIF
	ENDIF
	
	SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(fHeight+planeData.fmaxHeight)
	
	fLastHeight = fHeight
	IF (vPlayerVec.z - fHeight) > planeData.fmaxHeight
		IF bIsHighAlt
			IF TIMER_DO_WHEN_READY(tHighAltTimer, planeData.fAltTimeLimit)
				failReason = FAIL_TIME_ALTITUDE
				DEBUG_PRINT("FAILED ALTITUDE CHECK, RETURNING TRUE")
				RETURN TRUE
			ENDIF
		ELSE
			bIsHighAlt = TRUE
			RESTART_TIMER_NOW(tHighAltTimer) 
		ENDIF
	ELSE
		bIsHighAlt = FALSE
	ENDIF
		
	SET_TEXT_SCALE(0.4, 0.4)
	
	IF (vPlayerVec.z - fLastHeight) > cargoArgs.fActualMaxHeight
		cargoArgs.fActualMaxHeight = vPlayerVec.z - fLastHeight
	ENDIF
	
	IF bIsHighAlt AND TIMER_DO_WHEN_READY(tHighAltTimer, 1.0)		
		IF soundIdAlarm = -1
			soundIdAlarm = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(soundIdAlarm, "Altitude_Warning", "EXILE_1")
			CREATE_CONVERSATION(MyLocalPedStruct, "ARMSAUD", "ARMS_A_HALT", CONV_PRIORITY_VERY_HIGH)
			cargoArgs.bDetectedOnRadar = TRUE
		ENDIF
		
		FLOAT fHeightAboveAllowable = (vPlayerVec.z - fHeight) - planeData.fmaxHeight
		
//		PRINTLN("MAX HEIGHT = ", planeData.fmaxHeight)
//		PRINTLN("ACTUAL HEIGHT ABOVE GROUND = ", (vPlayerVec.z - fHeight))
//		PRINTLN("fHeightAboveAllowable = ", fHeightAboveAllowable)
		
		IF fHeightAboveAllowable >= 100
			fIntensity = 1.0
			PRINTLN("fIntensity = 1.0")
		ELIF fHeightAboveAllowable >= 50
			fIntensity = 0.5
			PRINTLN("fIntensity = 0.5")
		ELSE
			fIntensity = 0.0
			PRINTLN("fIntensity = 0.0")
		ENDIF
		SET_VARIABLE_ON_SOUND(soundIdAlarm, "Intensity", fIntensity)
	ELSE
		STOP_SOUND(soundIdAlarm)
		RELEASE_SOUND_ID(soundIdAlarm)
		soundIdAlarm = -1
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_UI(PLANE_CARGO_ARGS& cargoArgs, PLANE_DATA& myData, BOOL bDoBomb)	
	INT iNumCargoLeft
	
	IF bDoBomb
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(cargoArgs.iNumSuccessfulDrops, myData.iNumDrops, "DTRFAIR_NEW2", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE)
	ELSE
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(cargoArgs.iNumSuccessfulDrops, myData.iNumDrops, "DTRFAIR_NEW1", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE)
	ENDIF
	
	iNumCargoLeft = (myData.iNumTotalCargo - cargoArgs.iNumReleased)
	
	IF bDoBomb
		DRAW_GENERIC_BIG_NUMBER(iNumCargoLeft, "DTRFAIR_NEW4")
	ELSE
		DRAW_GENERIC_BIG_NUMBER(iNumCargoLeft, "DTRFAIR_NEW3")
	ENDIF

	RETURN FALSE
	
ENDFUNC
