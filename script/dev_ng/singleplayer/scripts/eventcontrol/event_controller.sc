//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	event_controller.sc											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Processes and maintains event information such as 			//
//							the number of vehicles destroyed by the player.			 	//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_script.sch"
USING "commands_entity.sch"

USING "event_public.sch"
USING "ply_to_ply_calls.sch"
USING "fmmc_request_to_play_mission.sch"


BOOL bResetEventData = FALSE


#IF IS_DEBUG_BUILD

	BOOL bPrintProcessedEvents, bPrintEventInfo
	
	PROC SETUP_EVENT_CONTROL_WIDGETS()
		START_WIDGET_GROUP("Event Controller")
			ADD_WIDGET_BOOL("Print processed events", bPrintProcessedEvents)
			ADD_WIDGET_BOOL("Print event info", bPrintEventInfo)
		STOP_WIDGET_GROUP()
	ENDPROC

	PROC MAINTAIN_EVENT_CONTROL_WIDGETS()
		IF bPrintEventInfo
			IF Has_Vehicle_Been_Damaged()
				PRINTSTRING("\n Vehicle damaged list")PRINTNL()
				INT iEvent
				ENTITY_INDEX eID
				REPEAT Get_Number_Of_Vehicle_Damaged_Events() iEvent
					eID = Get_Index_Of_Damaged_Vehicle(iEvent)
					IF DOES_ENTITY_EXIST(eID)
						PRINTSTRING("...slot[")PRINTINT(iEvent)PRINTSTRING("] = ")PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(eID)))PRINTNL()
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF Has_Vehicle_Been_Destroyed()
				PRINTSTRING("\n Vehicle destroyed list")PRINTNL()
				INT iEvent
				ENTITY_INDEX eID
				REPEAT Get_Number_Of_Vehicle_Destroyed_Events() iEvent
					eID = Get_Index_Of_Destroyed_Vehicle(iEvent)
					IF DOES_ENTITY_EXIST(eID)
						PRINTSTRING("...slot[")PRINTINT(iEvent)PRINTSTRING("] = ")PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(eID)))PRINTNL()
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF Has_Ped_Been_Injured()
				PRINTSTRING("\n Ped injured list")PRINTNL()
				INT iEvent
				ENTITY_INDEX eID
				REPEAT Get_Number_Of_Ped_Injured_Events() iEvent
					eID = Get_Index_Of_Injured_Ped(iEvent)
					IF DOES_ENTITY_EXIST(eID)
						PRINTSTRING("...slot[")PRINTINT(iEvent)PRINTSTRING("] = ")PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(eID)))PRINTNL()
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF Has_Ped_Been_Killed()
				PRINTSTRING("\n Ped killed list")PRINTNL()
				INT iEvent
				ENTITY_INDEX eID
				REPEAT Get_Number_Of_Ped_Killed_Events() iEvent
					eID = Get_Index_Of_Killed_Ped(iEvent)
					IF DOES_ENTITY_EXIST(eID)
						PRINTSTRING("...slot[")PRINTINT(iEvent)PRINTSTRING("] = ")PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(eID)))PRINTNL()
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

/// PURPOSE: Resets any data that should only persist for a single frame.
PROC RESET_EVENT_DATA()

	// Note, the bResetEventData flag is set when an event occurs.
	IF bResetEventData

		g_sEventData.iVehicleDamagedEvents = 0
		g_sEventData.iVehicleDestroyedEvents = 0
		g_sEventData.iPedInjuredEvents = 0
		g_sEventData.iPedKilledEvents = 0

		INT iEvent
		REPEAT MAX_EVENTS_STORED_PER_TYPE iEvent
			g_sEventData.eidVehicleDamaged[iEvent] = NULL
			g_sEventData.eidVehicleDestroyed[iEvent] = NULL
			g_sEventData.eidPedInjured[iEvent] = NULL
			g_sEventData.eidPedKilled[iEvent] = NULL
		ENDREPEAT

		// Clear the reset flag so we only reset when required
		bResetEventData = FALSE
	ENDIF
ENDPROC

/// PURPOSE: Returns TRUE if the specified event count is less than max
FUNC BOOL EVENT_COUNT_LESS_THAN_MAX(INT iEventCount)
	RETURN (iEventCount < MAX_EVENTS_STORED_PER_TYPE)
ENDFUNC

/// PURPOSE: Process any events that are in the event queue.
PROC PROCESS_EVENT_QUEUE()

    EVENT_NAMES eEventType
	STRUCT_ENTITY_ID sEntityID
    INT iCount = 0
	
	INT iVehicleDamagedEvent = 0
	INT iVehicleDestroyedEvent = 0
	INT iPedInjuredEvent = 0
	INT iPedKilledEvent = 0
	
	IF IS_PLAYER_ONLINE()
		EVENT_NAMES ThisScriptEvent
	
		//process the events
		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		
			ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
			//PRINTLN("PROCESS_NET_EVENTS = recieved network event # ", ENUM_TO_INT(ThisScriptEvent)) 
//			#IF IS_DEBUG_BUILD
//				
//				IF ThisScriptEvent = EVENT_NETWORK_SCRIPT_EVENT
//					GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
//					//NET_PRINT("PROCESS_NET_EVENTS = ThisScriptEvent = ") NET_PRINT_INT(ENUM_TO_INT(ThisScriptEvent)) NET_NL()
//				ENDIF
//			#ENDIF
			
			SWITCH ThisScriptEvent
				//EVENTS FOR VOICE CHAT
				CASE EVENT_VOICE_SESSION_STARTED
					PROCESS_VOICE_SESSION_STARTED_EVENT()
				BREAK
				
				CASE EVENT_VOICE_SESSION_ENDED
					PROCESS_VOICE_SESSION_ENDED_EVENT()
				BREAK
				
				CASE EVENT_VOICE_CONNECTION_REQUESTED
					PROCESS_VOICE_CONNECTION_REQUESTED_EVENT(iCount)
				BREAK
				
				CASE EVENT_VOICE_CONNECTION_RESPONSE
					PROCESS_VOICE_CONNECTION_RESPONSE_EVENT(iCount)
				BREAK
				
				CASE EVENT_VOICE_CONNECTION_TERMINATED
					PROCESS_EVENT_VOICE_CONNECTION_TERMINATED_EVENT(iCount)
				BREAK
				
				CASE EVENT_NETWORK_PRESENCE_INVITE
					PROCESS_EVENT_NETWORK_PRESENCE_INVITE(iCount)
				BREAK
			ENDSWITCH
		ENDREPEAT
	ENDIF
    
    // Go through the event queue getting the head of the queue
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iCount
		eEventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iCount)
		
		// Event occured so reset our data next frame
		bResetEventData = TRUE
		
        SWITCH (eEventType)
            CASE EVENT_ENTITY_DAMAGED
				#IF IS_DEBUG_BUILD
					IF bPrintProcessedEvents
						PRINTSTRING("...Processing AI Event - EVENT_ENTITY_DAMAGED")PRINTNL()
					ENDIF
				#ENDIF
				
				// Grab the event data and process
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iCount, sEntityID, SIZE_OF(STRUCT_ENTITY_ID))
				
				IF DOES_ENTITY_EXIST(sEntityID.EntityId)
					IF IS_ENTITY_A_VEHICLE(sEntityID.EntityId)
						IF EVENT_COUNT_LESS_THAN_MAX(iVehicleDamagedEvent)
							g_sEventData.eidVehicleDamaged[iVehicleDamagedEvent] = sEntityID.EntityId
							iVehicleDamagedEvent++
						ENDIF
					ELIF IS_ENTITY_A_PED(sEntityID.EntityId)
						IF EVENT_COUNT_LESS_THAN_MAX(iPedInjuredEvent)
							g_sEventData.eidPedInjured[iPedInjuredEvent] = sEntityID.EntityId
							iPedInjuredEvent++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE EVENT_ENTITY_DESTROYED
				#IF IS_DEBUG_BUILD
					IF bPrintProcessedEvents
						PRINTSTRING("...Processing AI Event - EVENT_ENTITY_DESTROYED")PRINTNL()
					ENDIF
				#ENDIF
				
				// Grab the event data and process
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iCount, sEntityID, SIZE_OF(STRUCT_ENTITY_ID))
				
				IF DOES_ENTITY_EXIST(sEntityID.EntityId)
					IF IS_ENTITY_A_VEHICLE(sEntityID.EntityId)
						IF EVENT_COUNT_LESS_THAN_MAX(iVehicleDestroyedEvent)
							g_sEventData.eidVehicleDestroyed[iVehicleDestroyedEvent] = sEntityID.EntityId
							iVehicleDestroyedEvent++
						ENDIF
					ELIF IS_ENTITY_A_PED(sEntityID.EntityId)
						IF EVENT_COUNT_LESS_THAN_MAX(iPedKilledEvent)
							g_sEventData.eidPedKilled[iPedKilledEvent] = sEntityID.EntityId
							iPedKilledEvent++
						ENDIF
					ENDIF
				ENDIF
			BREAK
        ENDSWITCH
    ENDREPEAT
	
	// Update the total event counts
	g_sEventData.iVehicleDamagedEvents = iVehicleDamagedEvent
	g_sEventData.iVehicleDestroyedEvents = iVehicleDestroyedEvent
	g_sEventData.iPedInjuredEvents = iPedInjuredEvent
	g_sEventData.iPedKilledEvents = iPedKilledEvent
	
ENDPROC


SCRIPT

	PRINTSTRING("\nStarting event controller")PRINTNL()
	
	// This script needs to cleanup only when the game runs the magdemo
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO))
		PRINTSTRING("...event_controller.sc has been forced to cleanup")
		PRINTNL()
		
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// Setup some debug widgets
	#IF IS_DEBUG_BUILD
		SETUP_EVENT_CONTROL_WIDGETS()
	#ENDIF
	
	// Main loop
	WHILE (TRUE)
	
		WAIT(0)
		
		// Reset any event data that should only persist for a single frame
		RESET_EVENT_DATA()
		
		// We must process the event queue each frame, otherwise we may miss an event
		PROCESS_EVENT_QUEUE()
		
		
		// Maintain the debug widgets
		#IF IS_DEBUG_BUILD
			MAINTAIN_EVENT_CONTROL_WIDGETS()
		#ENDIF
	ENDWHILE
	
ENDSCRIPT
