

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  David Roberts					Date: 16/11/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  			Pickup Controller								│
//│																				│
//│		DESCRIPTION: A basic controller script used to create, manage and		│
//│		remove armour, health and weapon pickups found in the world.			│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_public_core.sch"
USING "pickups_armour.sch"
USING "pickups_gas.sch"
USING "pickups_health.sch"
USING "pickups_weapon.sch"

// Common pickups
PICKUP_INDEX piArmourPickup[NUMBER_OF_ARMOUR_PICKUPS]
PICKUP_INDEX piGasPickup[NUMBER_OF_GAS_PICKUPS]
PICKUP_INDEX piHealthPickup[NUMBER_OF_HEALTH_PICKUPS]
PICKUP_INDEX piWeaponPickup[NUMBER_OF_WEAPON_PICKUPS]

// Unlocked pickups
PICKUP_INDEX piExilePickup[NUMBER_OF_EXILE1_PICKUPS]
BOOL 		 bUnlockedExilePickups = FALSE
BOOL		 bPickupsCreated = FALSE

// Rob B - add CGtoNG pickups 2070966, 2071227
PICKUP_INDEX piExileCGtoNGPickup[NUMBER_OF_EXILE1_CGTONG_PICKUPS]
BOOL 		 bUnlockedExileCGtoNGPickups = FALSE
PICKUP_INDEX piTrevorCGtoNGPickup[NUMBER_OF_TREVOR1_CGTONG_PICKUPS]
BOOL 		 bUnlockedTrevorCGtoNGPickups = FALSE


#IF IS_DEBUG_BUILD

	// Basic widget struct
	STRUCT sPickupWidget
		WIDGET_GROUP_ID groupID
		BOOL			bToggle
		BOOL			bBlip
	ENDSTRUCT
	
	// Common
	BLIP_INDEX 		armourBlips[NUMBER_OF_ARMOUR_PICKUPS]
	BLIP_INDEX 		gasBlips[NUMBER_OF_GAS_PICKUPS]
	BLIP_INDEX 		healthBlips[NUMBER_OF_HEALTH_PICKUPS]
	BLIP_INDEX 		weaponBlips[NUMBER_OF_WEAPON_PICKUPS]
	
	WIDGET_GROUP_ID mainWidget
	sPickupWidget   armourWidget
	sPickupWidget   gasWidget
	sPickupWidget   healthWidget
	sPickupWidget   weaponWidget
	BOOL			bWarpToArmour[NUMBER_OF_ARMOUR_PICKUPS]
	BOOL			bWarpToGas[NUMBER_OF_GAS_PICKUPS]
	BOOL			bWarpToHealth[NUMBER_OF_HEALTH_PICKUPS]
	BOOL			bWarpToWeapon[NUMBER_OF_WEAPON_PICKUPS]
	
	// Unlocks
	BLIP_INDEX 		exileBlips[NUMBER_OF_EXILE1_PICKUPS]
	sPickupWidget   exileWidget
	BOOL			bSetupExileIPL
	BOOL			bWarpToExile[NUMBER_OF_EXILE1_PICKUPS]
	
	/// PURPOSE: 
	///     Create main widget for armour, health and weapons
	PROC SETUP_MAIN_WIDGET()
		
		mainWidget = START_WIDGET_GROUP("Pickup Controller")
		ADD_WIDGET_BOOL("Enable Armour Debug", armourWidget.bToggle)
		ADD_WIDGET_BOOL("Enable Exile Debug",  exileWidget.bToggle)
		ADD_WIDGET_BOOL("Enable Gas Debug",    gasWidget.bToggle)
		ADD_WIDGET_BOOL("Enable Health Debug", healthWidget.bToggle)
		ADD_WIDGET_BOOL("Enable Weapon Debug", weaponWidget.bToggle)
		STOP_WIDGET_GROUP()
	ENDPROC
	
	/// PURPOSE: 
	///     Remove blips of specified pickups if there are any
	/// RETURNS:
	///     TRUE if blips needed removing, FALSE if blips did not exist (if the first blip does not exist, neither should the others)
	FUNC BOOL REMOVE_BLIPS(INT max_pickups, BLIP_INDEX &blips[])
		INT i
		REPEAT max_pickups i
			IF DOES_BLIP_EXIST(blips[i])
				REMOVE_BLIP(blips[i])
			ELSE
				RETURN FALSE//do not have to remove blips
			ENDIF
		ENDREPEAT
		
		RETURN TRUE
	ENDFUNC

	/// PURPOSE: 
	///     Setup armour widget
	PROC SETUP_ARMOUR_WIDGET()
		
		INT 		  i
		TEXT_LABEL_63 tlTemp

		SET_CURRENT_WIDGET_GROUP(mainWidget)
		armourWidget.groupID = START_WIDGET_GROUP("Armour Pickups")
			ADD_WIDGET_BOOL("Blip armour", armourWidget.bBlip)
			ADD_WIDGET_STRING("WARP TO ARMOUR")
			REPEAT COUNT_OF(piArmourPickup) i
				tlTemp = GET_ARMOUR_LOCATION_NAME(i)
				ADD_WIDGET_BOOL(tlTemp, bWarpToArmour[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(mainWidget)
	ENDPROC
	
	/// PURPOSE: 
	///     Setup Exile 1 weapon widget
	PROC SETUP_EXILE1_WIDGET()
		
		INT 		  i
		TEXT_LABEL_63 tlTemp

		SET_CURRENT_WIDGET_GROUP(mainWidget)
		exileWidget.groupID = START_WIDGET_GROUP("Exile 1 Pickups")
			ADD_WIDGET_BOOL("Setup Crash IPL", bSetupExileIPL)
			ADD_WIDGET_BOOL("Blip pickup", exileWidget.bBlip)
			ADD_WIDGET_STRING("WARP TO EXILE PICKUP")
			REPEAT COUNT_OF(piExilePickup) i
				tlTemp = i + 1
				tlTemp += ": " 
				tlTemp += GET_EXILE1_PICKUP_NAME(i)
				ADD_WIDGET_BOOL(tlTemp, bWarpToExile[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(mainWidget)
	ENDPROC

	/// PURPOSE: Setup gas widget
	PROC SETUP_GAS_WIDGET()
		
		INT 		  i
		TEXT_LABEL_63 tlTemp

		SET_CURRENT_WIDGET_GROUP(mainWidget)
		gasWidget.groupID = START_WIDGET_GROUP("Gas Pickups")
		ADD_WIDGET_BOOL("Blip gas", gasWidget.bBlip)
			ADD_WIDGET_STRING("WARP TO GAS")
			REPEAT COUNT_OF(piGasPickup) i
				tlTemp = GET_GAS_LOCATION_NAME(i)
				ADD_WIDGET_BOOL(tlTemp, bWarpToGas[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(mainWidget)
	ENDPROC
	
	/// PURPOSE: Setup health widget
	PROC SETUP_HEALTH_WIDGET()
		
		INT 		  i
		TEXT_LABEL_63 tlTemp

		SET_CURRENT_WIDGET_GROUP(mainWidget)
		healthWidget.groupID = START_WIDGET_GROUP("Health Pickups")
		ADD_WIDGET_BOOL("Blip health", healthWidget.bBlip)
			ADD_WIDGET_STRING("WARP TO HEALTH")
			REPEAT COUNT_OF(piHealthPickup) i
				tlTemp = GET_HEALTH_LOCATION_NAME(i)
				ADD_WIDGET_BOOL(tlTemp, bWarpToHealth[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(mainWidget)
	ENDPROC
	
	/// PURPOSE: Setup weapon widget
	PROC SETUP_WEAPON_WIDGET()
		
		INT 		  i
		TEXT_LABEL_63 tlTemp

		SET_CURRENT_WIDGET_GROUP(mainWidget)
		weaponWidget.groupID = START_WIDGET_GROUP("Weapon Pickups")
			ADD_WIDGET_BOOL("Blip weapon", weaponWidget.bBlip)
			ADD_WIDGET_STRING("WARP TO WEAPON")
			REPEAT COUNT_OF(piWeaponPickup) i
				tlTemp = GET_WEAPON_LOCATION_NAME(i)
				ADD_WIDGET_BOOL(tlTemp, bWarpToWeapon[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(mainWidget)
	ENDPROC
	
	/// PURPOSE:
	//		Loads any interior around the coord specified
	/// PARAMS:
	///    VECTOR coords = the coord of the pickup or scrap
	/// RETURNS:
	///    N/A
	PROC LOAD_INTERIOR_AROUND_COORDS(VECTOR coords)
		INTERIOR_INSTANCE_INDEX	intIndex = GET_INTERIOR_AT_COORDS(coords)
		
		IF IS_VALID_INTERIOR(intIndex)
			PIN_INTERIOR_IN_MEMORY(intIndex)

			WHILE NOT IS_INTERIOR_READY(intIndex)
				WAIT(0)
			ENDWHILE
			
			WAIT(0)
			UNPIN_INTERIOR(intIndex)
		ENDIF
	ENDPROC

	/// PURPOSE: Warp player to specified pickup coords
	PROC WARP_TO_PICKUP(VECTOR vPickupPos)

		IF NOT IS_VECTOR_ZERO(vPickupPos)
			LOAD_INTERIOR_AROUND_COORDS(vPickupPos)
			LOAD_SCENE(vPickupPos)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPickupPos + <<0.0, -1.5, 0.0>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 0.0)
			ENDIF
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Unable to warp to zero vector")
		ENDIF
	ENDPROC
	
	/// PURPOSE: Check for allowing player to warp to pickups, and blip pickups
	PROC UPDATE_WIDGETS()
		
		INT i

		// Armour pickups
		IF armourWidget.bToggle
			IF NOT DOES_WIDGET_GROUP_EXIST(armourWidget.groupID)
				SETUP_ARMOUR_WIDGET()
			ELSE
				REPEAT COUNT_OF(piArmourPickup) i
					IF (bWarpToArmour[i])
						CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Warping to armour pickup ", i)
						WARP_TO_PICKUP(GET_ARMOUR_PICKUP_COORDS(i))
						IF (i = 28) //to stop player faling to their death on a thin bridge
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_ENTITY_COORDS(PLAYER_PED_ID(), << 446.8033, -986.9347, 29.6895 >>)
								SET_ENTITY_HEADING(PLAYER_PED_ID(), 167.3769)
							ENDIF
						ELIF (i = 26)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1843.2908, 3696.4006, 33.2672>>)
								SET_ENTITY_HEADING(PLAYER_PED_ID(), 129.40)
							ENDIF
						ELIF (i = 36)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1389.7263, 3618.4751, 37.9259>>)
								SET_ENTITY_HEADING(PLAYER_PED_ID(), 230.5053)
							ENDIF
						ELIF (i = 40)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_ENTITY_COORDS(PLAYER_PED_ID(), <<156.9659, -1192.8693, 28.3474>>)
								SET_ENTITY_HEADING(PLAYER_PED_ID(), 282.9118)
							ENDIF
						ENDIF
						bWarpToArmour[i] = FALSE
					ENDIF	
				ENDREPEAT
				
				i=0
				
				IF armourWidget.bBlip
					REPEAT COUNT_OF(piArmourPickup) i
						IF NOT DOES_BLIP_EXIST(armourBlips[i])
							armourBlips[i] = ADD_BLIP_FOR_COORD(GET_ARMOUR_PICKUP_COORDS(i))
						ENDIF
					ENDREPEAT
					armourWidget.bBlip = FALSE
				ENDIF
			ENDIF
		ELSE
			IF DOES_WIDGET_GROUP_EXIST(armourWidget.groupID)
				REMOVE_BLIPS(COUNT_OF(piArmourPickup), armourBlips)
				DELETE_WIDGET_GROUP(armourWidget.groupID)
			ENDIF
		ENDIF
		
		// Exile pickups
		IF exileWidget.bToggle
			IF NOT DOES_WIDGET_GROUP_EXIST(exileWidget.groupID)
				SETUP_EXILE1_WIDGET()
			ELSE
				IF bSetupExileIPL
					REQUEST_IPL("crashed_cargoplane")
					bSetupExileIPL = FALSE
				ENDIF
				
				REPEAT COUNT_OF(piExilePickup) i
					IF (bWarpToExile[i])
						CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Warping to Exile pickup ", i)
						WARP_TO_PICKUP(GET_EXILE1_PICKUP_COORDS(i))
						bWarpToExile[i] = FALSE
					ENDIF	
				ENDREPEAT
				
				i=0
				
				IF exileWidget.bBlip
					REPEAT COUNT_OF(piExilePickup) i
						IF NOT DOES_BLIP_EXIST(exileBlips[i])
							exileBlips[i] = ADD_BLIP_FOR_COORD(GET_EXILE1_PICKUP_COORDS(i))
						ENDIF
					ENDREPEAT
					exileWidget.bBlip = FALSE
				ENDIF
			ENDIF
		ELSE
			IF DOES_WIDGET_GROUP_EXIST(exileWidget.groupID)
				REMOVE_BLIPS(COUNT_OF(piExilePickup), exileBlips)
				DELETE_WIDGET_GROUP(exileWidget.groupID)
			ENDIF
		ENDIF
		
		// Gas pickups
		IF gasWidget.bToggle
			IF NOT DOES_WIDGET_GROUP_EXIST(gasWidget.groupID)
				SETUP_GAS_WIDGET()
			ELSE
				REPEAT COUNT_OF(piGasPickup) i
					IF (bWarpToGas[i])
						CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Warping to gas pickup ", i)
						WARP_TO_PICKUP(GET_GAS_PICKUP_COORDS(i))
						bWarpToGas[i] = FALSE
					ENDIF	
				ENDREPEAT
				
				i=0
				
				IF gasWidget.bBlip
					REPEAT COUNT_OF(piGasPickup) i
						IF NOT DOES_BLIP_EXIST(gasBlips[i])
							gasBlips[i] = ADD_BLIP_FOR_COORD(GET_GAS_PICKUP_COORDS(i))
						ENDIF
					ENDREPEAT
					gasWidget.bBlip = FALSE
				ENDIF
			ENDIF
		ELSE
			IF DOES_WIDGET_GROUP_EXIST(gasWidget.groupID)
				REMOVE_BLIPS(COUNT_OF(piGasPickup), gasBlips)
				DELETE_WIDGET_GROUP(gasWidget.groupID)
			ENDIF
		ENDIF
		
		// Health pickups
		IF healthWidget.bToggle
			IF NOT DOES_WIDGET_GROUP_EXIST(healthWidget.groupID)
				SETUP_HEALTH_WIDGET()
			ELSE
				REPEAT COUNT_OF(piHealthPickup) i
					IF (bWarpToHealth[i])
						CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Warping to health pickup ", i)
						IF (i = 3)
							WARP_TO_PICKUP(<<-246.1343, 6331.7368, 31.4262>>)
						ELIF (i = 4)
							WARP_TO_PICKUP(<<-139.6319, -2497.1509, 38.6612>>)
						ELIF (i = 16)
							WARP_TO_PICKUP(<<1644.2069, 4868.0811, 41.0292>>)
						ELIF(i = 24)
							WARP_TO_PICKUP(<<1737.8108, 3322.5554, 40.2235>>)
						ELIF (i = 33)
							WARP_TO_PICKUP(<<-1505.8, 862.3, 181.6>>)
						ELIF (i = 39) // Tent village
							WARP_TO_PICKUP(<<1442.7953, 6334.1733, 22.9003>>)
						ELIF  (i = 60)
							WARP_TO_PICKUP(<<-552.6899, 5348.7275, 73.7478>>)
						ELIF  (i = 63)
							WARP_TO_PICKUP(<<154.2276, -744.3879, 257.1519>>)
						ELIF  (i = 65)
							WARP_TO_PICKUP(<<25.9113, -627.0163, 13.9622>>)
						ELSE
							WARP_TO_PICKUP(GET_HEALTH_PICKUP_COORDS(i))
						ENDIF
						bWarpToHealth[i] = FALSE
					ENDIF	
				ENDREPEAT
				
				i=0
				
				IF healthWidget.bBlip
					REPEAT COUNT_OF(piHealthPickup) i
						IF NOT DOES_BLIP_EXIST(healthBlips[i])
							healthBlips[i] = ADD_BLIP_FOR_COORD(GET_HEALTH_PICKUP_COORDS(i))
						ENDIF
					ENDREPEAT
					healthWidget.bBlip = FALSE
				ENDIF
			ENDIF
		ELSE
			IF DOES_WIDGET_GROUP_EXIST(healthWidget.groupID)
				REMOVE_BLIPS(COUNT_OF(piHealthPickup), healthBlips)
				DELETE_WIDGET_GROUP(healthWidget.groupID)
			ENDIF
		ENDIF
		
		// Weapon pickups
		IF weaponWidget.bToggle
			IF NOT DOES_WIDGET_GROUP_EXIST(weaponWidget.groupID)
				SETUP_WEAPON_WIDGET()
			ELSE
				REPEAT COUNT_OF(piWeaponPickup) i
					IF (bWarpToWeapon[i])
						CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Warping to weapon pickup ", i)
						
						// Handle special case warps
						VECTOR vPos = <<0,0,0>>
						IF (i = 12)
							vPos = << 557.5304, -837.8345, 55.1235 >>
						ELIF (i = 50)
							vPos = << -1086.5460, -2430.1213, 12.9449 >>
						ELIF (i = 100)
							vPos = <<-106.8103, -968.0106, 295.2905>>
						ELSE
							vPos = GET_WEAPON_PICKUP_COORDS(i)
						ENDIF
						
						WARP_TO_PICKUP(vPos)
						bWarpToWeapon[i] = FALSE
					ENDIF	
				ENDREPEAT
				
				i=0
				
				IF weaponWidget.bBlip
					REPEAT COUNT_OF(piWeaponPickup) i
						IF NOT DOES_BLIP_EXIST(weaponBlips[i])
							weaponBlips[i] = ADD_BLIP_FOR_COORD(GET_WEAPON_PICKUP_COORDS(i))
						ENDIF
					ENDREPEAT
					weaponWidget.bBlip = FALSE
				ENDIF
			ENDIF
		ELSE
			IF DOES_WIDGET_GROUP_EXIST(weaponWidget.groupID)
				REMOVE_BLIPS(COUNT_OF(piWeaponPickup), weaponBlips)
				DELETE_WIDGET_GROUP(weaponWidget.groupID)
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

//**********************************************************************************
//                                   ARMOUR PICKUPS
//**********************************************************************************
/// PURPOSE: 
///    Create armour pickup instance
PROC CREATE_ARMOUR_PICKUP(INT iIndex, VECTOR vRot, BOOL bSnapToGround=TRUE, BOOL bIsInInterior=FALSE, BOOL bSpawnUpright=FALSE)

	INT iPlacementFlags = 0
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	IF bSpawnUpright
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
	ENDIF
	
	IF bSnapToGround
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	ENDIF

	// Create pickup
	piArmourPickup[iIndex] = CREATE_PICKUP_ROTATE(PICKUP_ARMOUR_STANDARD, GET_ARMOUR_PICKUP_COORDS(iIndex), vRot, iPlacementFlags)
	SET_PICKUP_REGENERATION_TIME(piArmourPickup[iIndex], 600000)
	
	//if its in an interior, place it correctly
	IF bIsInInterior
		ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piArmourPickup[iIndex], GET_ARMOUR_PICKUP_ROOM_NAME(iIndex))
	ENDIF
ENDPROC

/// PURPOSE: 
///    Setup armour pickup spawners
PROC SETUP_WORLD_ARMOUR_PICKUPS()

	// DEFINE PICKUPS
	CREATE_ARMOUR_PICKUP(0, << 0,0,6 >>)
	CREATE_ARMOUR_PICKUP(1, << 0,0,78 >>)
	CREATE_ARMOUR_PICKUP(2, << 90,0,2 >>)
	CREATE_ARMOUR_PICKUP(3, << 0,0,330 >>)
	CREATE_ARMOUR_PICKUP(4, << 0,0,0 >>)
	CREATE_ARMOUR_PICKUP(5, << 0,0,0 >>)
	CREATE_ARMOUR_PICKUP(6, << 0,0,6 >>)
	CREATE_ARMOUR_PICKUP(7, << 0,0,0 >>)
	CREATE_ARMOUR_PICKUP(8, << 90,0,0 >>, FALSE)
	CREATE_ARMOUR_PICKUP(9, << 0,0,46 >>)
	
	CREATE_ARMOUR_PICKUP(10, << 90,0,9 >>)
	CREATE_ARMOUR_PICKUP(11, << -83.56,0,0 >>, FALSE)
	CREATE_ARMOUR_PICKUP(12, << -102.96,0,0 >>, FALSE)							// Under rock face
	CREATE_ARMOUR_PICKUP(13, << 0,0,0 >>)
	CREATE_ARMOUR_PICKUP(14, << 0,0,0 >>)
	CREATE_ARMOUR_PICKUP(15, << 0,0,78 >>)
	CREATE_ARMOUR_PICKUP(16, << -90,0,145.4 >>, FALSE)
	CREATE_ARMOUR_PICKUP(17, << 0,0,0 >>)
	CREATE_ARMOUR_PICKUP(18, << 0,0,42 >>)
	CREATE_ARMOUR_PICKUP(19, << 0,0,0 >>)
	
	CREATE_ARMOUR_PICKUP(20, << 0,0,0 >>)
	CREATE_ARMOUR_PICKUP(21, << 0,0,0 >>)
	CREATE_ARMOUR_PICKUP(22, << 0,0,3 >>)
	CREATE_ARMOUR_PICKUP(23, << 0,0,0 >>)
	CREATE_ARMOUR_PICKUP(24, << 0,0,0 >>)
	//police stations
	CREATE_ARMOUR_PICKUP(25, << 5,0,-146 >>)						// Paleto Bay
	CREATE_ARMOUR_PICKUP(26, << 0,0,108 >>)							// Sandy Shores
	CREATE_ARMOUR_PICKUP(27, << 0,0,130 >>)
	CREATE_ARMOUR_PICKUP(28, << -90,0,-180 >>, FALSE, TRUE)
	CREATE_ARMOUR_PICKUP(29, << 0,0,14 >>)
	
	CREATE_ARMOUR_PICKUP(30, << 0,0,0 >>)
	CREATE_ARMOUR_PICKUP(31, << 0,0,0 >>)
	CREATE_ARMOUR_PICKUP(32, << 0,0,5 >>)
	CREATE_ARMOUR_PICKUP(33, << 0,0,72 >>)
	CREATE_ARMOUR_PICKUP(34, << 0,0,27 >>)
	CREATE_ARMOUR_PICKUP(35, << 0,0,7 >>)
	CREATE_ARMOUR_PICKUP(36, << 0,0,7 >>)
	CREATE_ARMOUR_PICKUP(37, << 0,0,7 >>)
	CREATE_ARMOUR_PICKUP(38, << 0,0,7 >>)
	CREATE_ARMOUR_PICKUP(39, << -90,0,-20 >>, FALSE, TRUE)

	CREATE_ARMOUR_PICKUP(40, << -90,0,0 >>, FALSE)
	CREATE_ARMOUR_PICKUP(41, << 0,0,7 >>, TRUE, TRUE)
	CREATE_ARMOUR_PICKUP(42, << 0,0,7 >>)
	CREATE_ARMOUR_PICKUP(43, << -92,0,-18 >>, FALSE)
	CREATE_ARMOUR_PICKUP(44, << 90,0,-108 >>, FALSE)				// Altruist cache
ENDPROC

/// PURPOSE: 
///    Remove all armour pickup spawners
PROC REMOVE_WORLD_ARMOUR_PICKUPS()
	
	INT i
	REPEAT NUMBER_OF_ARMOUR_PICKUPS i
		IF DOES_PICKUP_EXIST(piArmourPickup[i])
			REMOVE_PICKUP(piArmourPickup[i])
		ENDIF
	ENDREPEAT
ENDPROC

//**********************************************************************************
//                                   EXILE PICKUPS
//**********************************************************************************
/// PURPOSE:
///    Setup weapon or armour pickup instance
/// PARAMS:
///    iIndex - array position
///    eType - Weapon type, or armour
///    vPos - location coords
///    vRot - YXZ ordered angles - use the pickup placement helper, not script debug tools!
///    bSnapToGround - snap to terrain surface. Can make items drop through props that haven't streamed in in time.
///    bOrient - Orient to ground. Armour always does.
PROC CREATE_EXILE1_UNLOCK_PICKUP(INT iIndex, PICKUP_TYPE eType, VECTOR vRot, BOOL bSnapToGround=TRUE, BOOL bOrient=TRUE)
	
	INT iPlacementFlags = 0
	
	IF eType = PICKUP_ARMOUR_STANDARD
	
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
//		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
		
		IF bSnapToGround
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		ENDIF

	ELSE
	
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
		
		IF bOrient
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		ENDIF
		
		IF bSnapToGround
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		ENDIF

	ENDIF
	
	// Create pickup
	piExilePickup[iIndex] = CREATE_PICKUP_ROTATE(eType, GET_EXILE1_PICKUP_COORDS(iIndex), vRot, iPlacementFlags)
	SET_PICKUP_REGENERATION_TIME(piExilePickup[iIndex], 600000)
	
ENDPROC

/// PURPOSE:
///    Setup weapon or armour pickup instance
/// PARAMS:
///    iIndex - array position
///    eType - Weapon type, or armour
///    vPos - location coords
///    vRot - YXZ ordered angles - use the pickup placement helper, not script debug tools!
///    bSnapToGround - snap to terrain surface. Can make items drop through props that haven't streamed in in time.
///    bOrient - Orient to ground. Armour always does.
PROC CREATE_EXILE1_CGTONG_UNLOCK_PICKUP(INT iIndex, PICKUP_TYPE eType, VECTOR vRot, BOOL bSnapToGround=TRUE, BOOL bOrient=TRUE)
	
	INT iPlacementFlags = 0
	
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	
	IF bOrient
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	ENDIF
	
	IF bSnapToGround
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	ENDIF
	
	// Create pickup
	piExileCGtoNGPickup[iIndex] = CREATE_PICKUP_ROTATE(eType, GET_EXILE1_CGTONG_PICKUP_COORDS(iIndex), vRot, iPlacementFlags)
	SET_PICKUP_REGENERATION_TIME(piExileCGtoNGPickup[iIndex], 600000)
	
ENDPROC

/// PURPOSE:
///    Setup weapon or armour pickup instance
/// PARAMS:
///    iIndex - array position
///    eType - Weapon type, or armour
///    vPos - location coords
///    vRot - YXZ ordered angles - use the pickup placement helper, not script debug tools!
///    bSnapToGround - snap to terrain surface. Can make items drop through props that haven't streamed in in time.
///    bOrient - Orient to ground. Armour always does.
PROC CREATE_TREVOR1_CGTONG_UNLOCK_PICKUP(INT iIndex, PICKUP_TYPE eType, VECTOR vRot, BOOL bSnapToGround=TRUE, BOOL bOrient=TRUE)
	
	INT iPlacementFlags = 0
	
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	
	IF bOrient
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	ENDIF
	
	IF bSnapToGround
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	ENDIF
	
	// Create pickup
	piTrevorCGtoNGPickup[iIndex] = CREATE_PICKUP_ROTATE(eType, GET_TREVOR1_CGTONG_PICKUP_COORDS(iIndex), vRot, iPlacementFlags)
	SET_PICKUP_REGENERATION_TIME(piTrevorCGtoNGPickup[iIndex], 600000)
	
ENDPROC

/// PURPOSE: 
///    Setup Mission pickup spawners
PROC SETUP_EXILE1_UNLOCK_PICKUPS()
	
	// DEFINE PICKUPS
	CREATE_EXILE1_UNLOCK_PICKUP(0, PICKUP_WEAPON_GRENADELAUNCHER,	<< 156.24, 0, 25.2 >>)
	CREATE_EXILE1_UNLOCK_PICKUP(1, PICKUP_WEAPON_RPG,				<< -92.68, 62.64, -264.24 >>)
	CREATE_EXILE1_UNLOCK_PICKUP(2, PICKUP_WEAPON_COMBATMG,			<< -70, 0, -40 >>)
	CREATE_EXILE1_UNLOCK_PICKUP(3, PICKUP_ARMOUR_STANDARD,			<< -71, 0, 168.48 >>)
	CREATE_EXILE1_UNLOCK_PICKUP(4, PICKUP_WEAPON_APPISTOL,			<< -100.8, 92.8, 0 >>)
	CREATE_EXILE1_UNLOCK_PICKUP(5, PICKUP_WEAPON_PUMPSHOTGUN,		<< 77.76, 10.08, -17.28 >>)
	CREATE_EXILE1_UNLOCK_PICKUP(6, PICKUP_WEAPON_SNIPERRIFLE,		<< 97.92, 0, 100.8 >>)
	CREATE_EXILE1_UNLOCK_PICKUP(7, PICKUP_WEAPON_STICKYBOMB,		<< 0, 0, 0 >>) //<< -7, 15, 0 >>
	CREATE_EXILE1_UNLOCK_PICKUP(8, PICKUP_WEAPON_STICKYBOMB,		<< 0, 0, 0 >>) //<< -9.0, -2.0, 0 >>
ENDPROC

/// PURPOSE: 
///    Setup Exile CGtoNG pickup spawners
PROC SETUP_EXILE1_CGTONG_UNLOCK_PICKUPS()
	IF IS_LAST_GEN_PLAYER()
		// DEFINE PICKUPS
		CREATE_EXILE1_CGTONG_UNLOCK_PICKUP(0, PICKUP_WEAPON_DLC_RAILGUN,	<< 156.24, 0, 166.2 >>)
	ENDIF
ENDPROC

/// PURPOSE: 
///    Setup Trevor CGtoNG pickup spawners
PROC SETUP_TREVOR1_CGTONG_UNLOCK_PICKUPS()
	IF IS_LAST_GEN_PLAYER()
		// DEFINE PICKUPS
		CREATE_TREVOR1_CGTONG_UNLOCK_PICKUP(0, PICKUP_WEAPON_DLC_HATCHET,	<< 156.24, 0, 71.2 >>)
	ENDIF
ENDPROC

/// PURPOSE: 
///    Remove all Exile pickup spawners
PROC REMOVE_EXILE1_UNLOCK_PICKUPS()
	INT i
	REPEAT NUMBER_OF_EXILE1_PICKUPS i
		IF DOES_PICKUP_EXIST(piExilePickup[i])
			REMOVE_PICKUP(piExilePickup[i])
		ENDIF
	ENDREPEAT
	
	bUnlockedExilePickups = FALSE
ENDPROC

/// PURPOSE: 
///    Remove all Exile CGtoNG pickup spawners
PROC REMOVE_EXILE1_CGTONG_UNLOCK_PICKUPS()
	INT i
	REPEAT NUMBER_OF_EXILE1_CGTONG_PICKUPS i
		IF DOES_PICKUP_EXIST(piExileCGtoNGPickup[i])
			REMOVE_PICKUP(piExileCGtoNGPickup[i])
		ENDIF
	ENDREPEAT
	
	bUnlockedExileCGtoNGPickups = FALSE
ENDPROC

/// PURPOSE: 
///    Remove all Trevot CGtoNG pickup spawners
PROC REMOVE_TREVOR1_CGTONG_UNLOCK_PICKUPS()
	INT i
	REPEAT NUMBER_OF_TREVOR1_CGTONG_PICKUPS i
		IF DOES_PICKUP_EXIST(piTrevorCGtoNGPickup[i])
			REMOVE_PICKUP(piTrevorCGtoNGPickup[i])
		ENDIF
	ENDREPEAT
	
	bUnlockedTrevorCGtoNGPickups = FALSE
ENDPROC

//**********************************************************************************
//                                   GAS PICKUPS
//**********************************************************************************
/// PURPOSE: 
///    Create jerry can pickup instance
PROC CREATE_GAS_PICKUP(INT iIndex, VECTOR vRot, BOOL bSnapToGround=TRUE)

	INT iPlacementFlags = 0
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
	
	IF bSnapToGround
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	ENDIF

	// Create pickup
	piGasPickup[iIndex] = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_PETROLCAN, GET_GAS_PICKUP_COORDS(iIndex), vRot, iPlacementFlags)
	SET_PICKUP_REGENERATION_TIME(piGasPickup[iIndex], 600000)
ENDPROC

/// PURPOSE: 
///    Setup armour pickup spawners
PROC SETUP_WORLD_GAS_PICKUPS()

	// DEFINE PICKUPS
	CREATE_GAS_PICKUP(0, << 0,0,0 >>)
	CREATE_GAS_PICKUP(1, << 0,0,0 >>)
	CREATE_GAS_PICKUP(2, << 0,0,0 >>)
	CREATE_GAS_PICKUP(3, << 0,0,0 >>)
	CREATE_GAS_PICKUP(4, << 0,0,0 >>)
	CREATE_GAS_PICKUP(5, << 0,0,0 >>)
	CREATE_GAS_PICKUP(6, << 0,0,0 >>)
	CREATE_GAS_PICKUP(7, << 0,0,0 >>)
	CREATE_GAS_PICKUP(8, << 0,0,0 >>)
	CREATE_GAS_PICKUP(9, << 0,0,0 >>)
	CREATE_GAS_PICKUP(10, << 0,0,0 >>)
	CREATE_GAS_PICKUP(11, << 0,0,0 >>)
	CREATE_GAS_PICKUP(12, << 0,0,0 >>)
	CREATE_GAS_PICKUP(13, << 0,0,0 >>)
	CREATE_GAS_PICKUP(14, << 0,0,0 >>)
	CREATE_GAS_PICKUP(15, << 0,0,0 >>)
	CREATE_GAS_PICKUP(16, << 0,0,0 >>)
	CREATE_GAS_PICKUP(17, << 0,0,0 >>)
	CREATE_GAS_PICKUP(18, << 0,0,0 >>)
ENDPROC

/// PURPOSE: 
///    Remove all gas pickup spawners
PROC REMOVE_WORLD_GAS_PICKUPS()
	
	INT i
	REPEAT NUMBER_OF_GAS_PICKUPS i
		IF DOES_PICKUP_EXIST(piGasPickup[i])
			REMOVE_PICKUP(piGasPickup[i])
		ENDIF
	ENDREPEAT
ENDPROC

//**********************************************************************************
//                                   HEALTH PICKUPS
//**********************************************************************************
/// PURPOSE:
///    Create health pickup instance
/// PARAMS:
///    iIndex - Array position
///    vRot - Rotation Euler angles
///    bDontSnapToGround - Set to TRUE to disable snap to ground
///    bIsInInterior - Set to TRUE if in an interior and add the interior in GET_PICKUP_ROOM_NAME
PROC CREATE_HEALTH_PICKUP(INT iIndex, VECTOR vRot, BOOL bDontSnapToGround=FALSE, BOOL bIsInInterior=FALSE)
	
	INT iPlacementFlags = 0
	
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	
	IF NOT bDontSnapToGround
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	ENDIF

	// Create pickup
	piHealthPickup[iIndex] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, GET_HEALTH_PICKUP_COORDS(iIndex), vRot, iPlacementFlags)
	SET_PICKUP_REGENERATION_TIME(piHealthPickup[iIndex], 600000)
	
	//if its in an interior, place it correctly
	IF bIsInInterior
		ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piHealthPickup[iIndex], GET_PICKUP_ROOM_NAME(iIndex))
	ENDIF
ENDPROC

/// PURPOSE: 
///    Setup health pickup data
PROC SETUP_WORLD_HEALTH_PICKUPS()

	// DEFINE PICKUPS
	CREATE_HEALTH_PICKUP(0, << 0,0,0 >>) 					// Adventure Trail
	CREATE_HEALTH_PICKUP(1, << 0,0,24 >>) 					// Altruist Camp Guard Tower
	CREATE_HEALTH_PICKUP(2, << 0,0,60 >>, TRUE, TRUE) 		// Army Control Tower (corner top floor)
	CREATE_HEALTH_PICKUP(3, << 0,0,45 >>) 					// Paleto Bay Care Centre
	CREATE_HEALTH_PICKUP(4, << 0,0,55 >>, TRUE) 					// Bridge Service Access
	CREATE_HEALTH_PICKUP(5, << 0,0,0 >>) 					// Chumash Tennis Courts
	CREATE_HEALTH_PICKUP(6, << 0,0,0 >>) 					// Church Rear Exit in Little Seoul
	CREATE_HEALTH_PICKUP(7, << 0,0,0 >>) 					// Courtyard of IAA and FIB
	CREATE_HEALTH_PICKUP(8, << 0,0,90 >>, TRUE, TRUE) 		// East LS Fire Station
	CREATE_HEALTH_PICKUP(9, << 0,0,0 >>) 					// Farm House Garden

	CREATE_HEALTH_PICKUP(10, << 0,0,-112 >>) 				// Fishing Shack at Alamo Sea
	CREATE_HEALTH_PICKUP(11, << 0,0,0 >>) 					// Fruit market
	CREATE_HEALTH_PICKUP(12, << 0,0,0 >>) 					// Harmony Motel
	CREATE_HEALTH_PICKUP(13, << 0,0,0 >>) 					// Hotel Courtyard
	CREATE_HEALTH_PICKUP(14, << 0,0,0 >>)					// House Foundations
	CREATE_HEALTH_PICKUP(15, << 0,0,35 >>) 					// Inside Dam
	CREATE_HEALTH_PICKUP(16, << 0,0,13 >>)				 	// Alley near closed Fleeca Bank in Grapeseed
	CREATE_HEALTH_PICKUP(17, << 0,0,0 >>, FALSE, TRUE)		// Inside Los Santos Customs
	CREATE_HEALTH_PICKUP(18, << 0,0,-64 >>, TRUE) 			// Inside Movie Trailer at Movie Studio // Repositioned - B*1401263
	CREATE_HEALTH_PICKUP(19, << 0,0,0 >>) 					// Lombank

	CREATE_HEALTH_PICKUP(20, << 0,0,-62.4 >>, TRUE)			// Lost MC Clubhouse exterior
	CREATE_HEALTH_PICKUP(21, << 0,0,0 >>) 					// Mirror Park Wash Station
	CREATE_HEALTH_PICKUP(22, << 0,0,0 >>) 					// Bell Building at Kortz Center
	CREATE_HEALTH_PICKUP(23, << 0,0,0 >>) 					// Mission Route - Lost Trailer park Burned out house
	CREATE_HEALTH_PICKUP(24, << 0,0,95 >>)					// Senora Airfield Hangar
	CREATE_HEALTH_PICKUP(25, << 0,0,0 >>) 					// Mount Chilead Viewing Platform
	CREATE_HEALTH_PICKUP(26, << 0,0,-15 >>, TRUE)			// Observatory
	CREATE_HEALTH_PICKUP(27, << 0,0,0 >>) 					// Outside Barn in Grapeseed
	CREATE_HEALTH_PICKUP(28, << 0,0,-5 >>) 					// Outside Yellow House on Coast
	CREATE_HEALTH_PICKUP(29, << 5,0,-45>>)					// Behind Paleto Police Station

	CREATE_HEALTH_PICKUP(30, << 0,0,0 >>) 					// Pumpkin Patch Garden
	CREATE_HEALTH_PICKUP(31, << 6,0,51 >>) 					// LS Central Medical Center rear	// Repositioned - B*1339106
	CREATE_HEALTH_PICKUP(32, << 0,0,-28 >>) 				// Rear of Pill Pharm Clinic		// Repositioned - B*1336955
	CREATE_HEALTH_PICKUP(33, << 0,0,33 >>) 					// Rehab Center in Vinewood			// Repositioned - B*1355129
	CREATE_HEALTH_PICKUP(34, << 0,0,0 >>)					// Rock Arch
	CREATE_HEALTH_PICKUP(35, << 0,0,0 >>) 					// Round the back of Odea's pharmacy
	CREATE_HEALTH_PICKUP(36, << 5,0,30.2 >>, TRUE) 			// Sandy Shores Medical Center
	CREATE_HEALTH_PICKUP(37, << 0,0,0 >>, TRUE) 			// Security Booth at Bollingbroke Peniteniary
	CREATE_HEALTH_PICKUP(38, << 0,0,70.5 >>, TRUE, TRUE) 	// Service area inside Simeon's Dealership
	CREATE_HEALTH_PICKUP(39, << 0,0,70 >>, TRUE) 			// Tent Village

	CREATE_HEALTH_PICKUP(40, << 0,0,45 >>) 					// Upstairs at marina yacht Club
	CREATE_HEALTH_PICKUP(41, << 0,0,-7 >>, TRUE) 			// Vagos Ganghouse
	CREATE_HEALTH_PICKUP(42, << 0,0,-13 >>, TRUE)			// Vespucci Beach Toilets
	CREATE_HEALTH_PICKUP(43, << 0,0,44.4 >>, TRUE)			// Vinewood Hills Construction site
	CREATE_HEALTH_PICKUP(44, << 0,0,35 >>) 					// Vinewood Organic Health Center
	CREATE_HEALTH_PICKUP(45, << 0,0,89.8 >>, TRUE)			// Vinewood Sign Power Station
	CREATE_HEALTH_PICKUP(46, << 0,0,-166 >>) 				// Vinewood trash pile
	CREATE_HEALTH_PICKUP(47, << 0,0,0 >>) 					// Vineyard House Rear Garden
	CREATE_HEALTH_PICKUP(48, << 0,0,0 >>, FALSE, TRUE) 		// Warehouse at Docks
	CREATE_HEALTH_PICKUP(49, << 0,2,90 >>, TRUE) 			// Workman's Shack Under Freeway

	CREATE_HEALTH_PICKUP(50, << 0,0,34 >>)					// Lifeguard Hut 1
	CREATE_HEALTH_PICKUP(51, << 0,0,59 >>, TRUE, TRUE)		// Interior
	CREATE_HEALTH_PICKUP(52, << 0,0,5 >>)					// Lifeguard Hut 2
	CREATE_HEALTH_PICKUP(53, << 0,0,70 >>)					// Lifeguard Hut 3
	CREATE_HEALTH_PICKUP(54, << 0,0,104 >>)					// Lifeguard Hut 4
	CREATE_HEALTH_PICKUP(55, << 0,0,-85 >>)					// Val-De-Grace roof
	CREATE_HEALTH_PICKUP(56, << 0,0,32 >>)					// Vinewood Hills - Garden Bushes
	CREATE_HEALTH_PICKUP(57, << 0,0,38 >>, TRUE) 			// O'Neill's farm stables
	CREATE_HEALTH_PICKUP(58, << 0,0,100 >>, FALSE, TRUE)	// cash register
	CREATE_HEALTH_PICKUP(59, << 0,0,70 >>, TRUE)  			// power box

	CREATE_HEALTH_PICKUP(60, << 0,0,69.7 >>, TRUE) 			// alarms 
	CREATE_HEALTH_PICKUP(61, << 0,0,100 >>)  				// dumpsters
	CREATE_HEALTH_PICKUP(62, << 0,20.1,52.5 >>, TRUE)		// outdoor toilet
	CREATE_HEALTH_PICKUP(63, << 0,0,339 >>, TRUE, TRUE)  	// back of office
	CREATE_HEALTH_PICKUP(64, << 0,0,61 >>)   				// hobo shack
	CREATE_HEALTH_PICKUP(65, << 0,0,284 >>, TRUE, TRUE)  	// work station container
	CREATE_HEALTH_PICKUP(66, << 0,0,-20 >>, TRUE)  			// fire hose
	CREATE_HEALTH_PICKUP(67, << 6,0,0 >>, TRUE)				// St. Fiacre rear garages - B*1339106
	CREATE_HEALTH_PICKUP(68, << 6,0,-215 >>, TRUE)			// St. Fiacre front door - B*1339106
	CREATE_HEALTH_PICKUP(69, << -6,0,-40 >>, TRUE)			// LS Central Medical Center Emergency - B*1339106

	CREATE_HEALTH_PICKUP(70, << 6,0,140 >>, TRUE)			// LS Central Medical Center main entrance - B*1339106
	CREATE_HEALTH_PICKUP(71, << 6,0,-110 >>, TRUE)			// Pillbox Hill Medical Center upper entrance - B*1339106
	CREATE_HEALTH_PICKUP(72, << -6,0,-110 >>, TRUE)			// Pillbox Hill Medical Center lower entrance - B*1339106
	CREATE_HEALTH_PICKUP(73, << 6,0,-7 >>, TRUE)			// Mount Zonah Medical Center Emergency - B*1339106
	CREATE_HEALTH_PICKUP(74, << 6,0,173 >>, TRUE)			// Mount Zonah Medical Center Emergency - B*1339106
	CREATE_HEALTH_PICKUP(75, << 0,0,23.3 >>, TRUE)			// Kortz Center on pillar - B*1526799

ENDPROC

/// PURPOSE: Remove all health pickup spawners
PROC REMOVE_WORLD_HEALTH_PICKUPS()
	INT i
	REPEAT NUMBER_OF_HEALTH_PICKUPS i
		IF DOES_PICKUP_EXIST(piHealthPickup[i])
			REMOVE_PICKUP(piHealthPickup[i])
		ENDIF
	ENDREPEAT
ENDPROC

//**********************************************************************************
//                                   WEAPON PICKUPS
//**********************************************************************************
/// PURPOSE: 
///    Setup weapon pickup instance
/// PARAMS:
///    bSnapToGround	- Snaps to ground (this can cause it to drop through props)
///    bIsInInterior	- MUST be true for interiors
///    bOrient			- Orient to ground
PROC CREATE_WEAPON_PICKUP(INT iIndex, PICKUP_TYPE eType, VECTOR vRot, BOOL bSnapToGround=TRUE, BOOL bIsInInterior=FALSE, BOOL bOrient=TRUE)
	
	INT iPlacementFlags = 0
	
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	
	IF bOrient
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	ENDIF
	
	IF bSnapToGround
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	ENDIF

	// Create pickup
	piWeaponPickup[iIndex] = CREATE_PICKUP_ROTATE(eType, GET_WEAPON_PICKUP_COORDS(iIndex), vRot, iPlacementFlags)
	SET_PICKUP_REGENERATION_TIME(piWeaponPickup[iIndex], 600000)

	// Add pickup to interior if required
	IF bIsInInterior
		ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piWeaponPickup[iIndex], GET_WEAPON_PICKUP_ROOM_NAME(iIndex))
	ENDIF
ENDPROC


/// PURPOSE: 
///    Setup armour pickup spawners
PROC SETUP_WORLD_WEAPON_PICKUPS()
	
	// DEFINE PICKUPS
	CREATE_WEAPON_PICKUP(0, PICKUP_WEAPON_ASSAULTRIFLE, << 90,0,70 >>, FALSE)
	CREATE_WEAPON_PICKUP(1, PICKUP_WEAPON_ASSAULTRIFLE, << 90,0,0 >>, FALSE)
	CREATE_WEAPON_PICKUP(2, PICKUP_WEAPON_ASSAULTRIFLE, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(3, PICKUP_WEAPON_ASSAULTRIFLE, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(4, PICKUP_WEAPON_ASSAULTRIFLE, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(5, PICKUP_WEAPON_ASSAULTRIFLE, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(6, PICKUP_WEAPON_GRENADE, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(7, PICKUP_WEAPON_GRENADE, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(8, PICKUP_WEAPON_GRENADE, << 95,0,0 >>, FALSE)
	CREATE_WEAPON_PICKUP(9, PICKUP_WEAPON_GRENADE, << 0,0,0 >>)
	
	CREATE_WEAPON_PICKUP(10, PICKUP_WEAPON_GRENADELAUNCHER, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(11, PICKUP_WEAPON_GRENADELAUNCHER, << 118,0,92 >>, FALSE, FALSE, FALSE)
	CREATE_WEAPON_PICKUP(12, PICKUP_WEAPON_GRENADELAUNCHER, << 82,-60,0 >>, FALSE, FALSE, FALSE)
	CREATE_WEAPON_PICKUP(13, PICKUP_WEAPON_MINIGUN, << 0,0,0 >>, TRUE, TRUE)
	CREATE_WEAPON_PICKUP(14, PICKUP_WEAPON_MG, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(15, PICKUP_WEAPON_MG, << 97.92,60.48,0 >>, FALSE)
	CREATE_WEAPON_PICKUP(16, PICKUP_WEAPON_MG, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(17, PICKUP_WEAPON_MG, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(18, PICKUP_WEAPON_PISTOL, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(19, PICKUP_WEAPON_PISTOL, << 0,0,0 >>)

	CREATE_WEAPON_PICKUP(20, PICKUP_WEAPON_PISTOL, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(21, PICKUP_WEAPON_PISTOL, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(22, PICKUP_WEAPON_PISTOL, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(23, PICKUP_WEAPON_PISTOL, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(24, PICKUP_WEAPON_PISTOL, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(25, PICKUP_WEAPON_PISTOL, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(26, PICKUP_WEAPON_PISTOL, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(27, PICKUP_WEAPON_PISTOL, << 0,0,36 >>)
	CREATE_WEAPON_PICKUP(28, PICKUP_WEAPON_RPG, << 86,0,0 >>)
	CREATE_WEAPON_PICKUP(29, PICKUP_WEAPON_RPG, << 0,0,0 >>)

	CREATE_WEAPON_PICKUP(30, PICKUP_WEAPON_RPG, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(31, PICKUP_WEAPON_SAWNOFFSHOTGUN, << -82,0,0 >>, FALSE)
	CREATE_WEAPON_PICKUP(32, PICKUP_WEAPON_SAWNOFFSHOTGUN, << 0,0,24 >>)
	CREATE_WEAPON_PICKUP(33, PICKUP_WEAPON_SAWNOFFSHOTGUN, << 0,0,12 >>)
	CREATE_WEAPON_PICKUP(34, PICKUP_WEAPON_SAWNOFFSHOTGUN, << 0,0,98 >>)
	CREATE_WEAPON_PICKUP(35, PICKUP_WEAPON_SAWNOFFSHOTGUN, << 90,0,140 >>)
	CREATE_WEAPON_PICKUP(36, PICKUP_WEAPON_SAWNOFFSHOTGUN, << 90,0,120 >>, FALSE)
	CREATE_WEAPON_PICKUP(37, PICKUP_WEAPON_SMG, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(38, PICKUP_WEAPON_SMG, << 85.68,-92.88,0 >>, FALSE)
	CREATE_WEAPON_PICKUP(39, PICKUP_WEAPON_SMG, << 0,0,0 >>)

	CREATE_WEAPON_PICKUP(40, PICKUP_WEAPON_SMG, << 0,0,20 >>)
	CREATE_WEAPON_PICKUP(41, PICKUP_WEAPON_SMG, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(42, PICKUP_WEAPON_SMG, << 0,0,0 >>, TRUE, TRUE)
	CREATE_WEAPON_PICKUP(43, PICKUP_WEAPON_SNIPERRIFLE, << 0,0,42 >>)
	CREATE_WEAPON_PICKUP(44, PICKUP_WEAPON_SNIPERRIFLE, << 0,0,52 >>)
	CREATE_WEAPON_PICKUP(45, PICKUP_WEAPON_SNIPERRIFLE, << 90,0,87 >>)
	CREATE_WEAPON_PICKUP(46, PICKUP_WEAPON_SNIPERRIFLE, << 0,0,98 >>)
	CREATE_WEAPON_PICKUP(47, PICKUP_WEAPON_SNIPERRIFLE, << 0,0,20 >>)
	CREATE_WEAPON_PICKUP(48, PICKUP_WEAPON_STICKYBOMB, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(49, PICKUP_WEAPON_STICKYBOMB, << 0,0,0 >>)
	
	CREATE_WEAPON_PICKUP(50, PICKUP_WEAPON_RPG, << 63,0,0 >>, FALSE)
	CREATE_WEAPON_PICKUP(51, PICKUP_WEAPON_PISTOL, << 0,0,4 >>)
	CREATE_WEAPON_PICKUP(52, PICKUP_WEAPON_PISTOL, << 90,90,90 >>, FALSE, FALSE, FALSE)
	CREATE_WEAPON_PICKUP(53, PICKUP_WEAPON_SNIPERRIFLE, << 0,0,2 >>)
	CREATE_WEAPON_PICKUP(54, PICKUP_WEAPON_SAWNOFFSHOTGUN, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(55, PICKUP_WEAPON_BAT, << 90,0,4 >>)
	CREATE_WEAPON_PICKUP(56, PICKUP_WEAPON_BAT, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(57, PICKUP_WEAPON_BAT, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(58, PICKUP_WEAPON_BAT, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(59, PICKUP_WEAPON_BAT, << 0,0,0 >>)

	CREATE_WEAPON_PICKUP(60, PICKUP_WEAPON_BAT, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(61, PICKUP_WEAPON_BAT, << 0,0,0 >>, TRUE, TRUE)
	CREATE_WEAPON_PICKUP(62, PICKUP_WEAPON_BAT, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(63, PICKUP_WEAPON_BAT, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(64, PICKUP_WEAPON_BAT, << 0,0,0 >>, TRUE, TRUE)
	CREATE_WEAPON_PICKUP(65, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(66, PICKUP_WEAPON_CROWBAR, << 90,0,45 >>, FALSE)	// Parking lot booth
	CREATE_WEAPON_PICKUP(67, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(68, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(69, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>)

	CREATE_WEAPON_PICKUP(70, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(71, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(72, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(73, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(74, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(75, PICKUP_WEAPON_MOLOTOV, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(76, PICKUP_WEAPON_MOLOTOV, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(77, PICKUP_WEAPON_MOLOTOV, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(78, PICKUP_WEAPON_MOLOTOV, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(79, PICKUP_WEAPON_MOLOTOV, << 0,0,0 >>)

	CREATE_WEAPON_PICKUP(80, PICKUP_WEAPON_MOLOTOV, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(81, PICKUP_WEAPON_MOLOTOV, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(82, PICKUP_WEAPON_MOLOTOV, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(83, PICKUP_WEAPON_MOLOTOV, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(84, PICKUP_WEAPON_MOLOTOV, << 0,0,0 >>)
	CREATE_WEAPON_PICKUP(85, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>) // Was hammer, removed as DLC
	CREATE_WEAPON_PICKUP(86, PICKUP_WEAPON_BAT, 	<< 0,0,0 >>) // Was hammer, removed as DLC
	CREATE_WEAPON_PICKUP(87, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>) // Was hammer, removed as DLC
	CREATE_WEAPON_PICKUP(88, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>) // Was hammer, removed as DLC
	CREATE_WEAPON_PICKUP(89, PICKUP_WEAPON_BAT,		<< 0,0,0 >>) // Was hammer, removed as DLC

	CREATE_WEAPON_PICKUP(90, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>) // Was hammer, removed as DLC
	CREATE_WEAPON_PICKUP(91, PICKUP_WEAPON_BAT,		<< 0,0,0 >>) // Was hammer, removed as DLC
	CREATE_WEAPON_PICKUP(92, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>) // Was hammer, removed as DLC
	CREATE_WEAPON_PICKUP(93, PICKUP_WEAPON_BAT, 	<< 0,0,0 >>) // Was hammer, removed as DLC
	CREATE_WEAPON_PICKUP(94, PICKUP_WEAPON_CROWBAR, << 0,0,0 >>) // Was hammer, removed as DLC
	CREATE_WEAPON_PICKUP(95, PICKUP_WEAPON_CARBINERIFLE,	<< 0,0,87 >>)
	CREATE_WEAPON_PICKUP(96, PICKUP_WEAPON_ASSAULTSHOTGUN,	<< 0,0,0 >>, TRUE, TRUE)
	CREATE_WEAPON_PICKUP(97, PICKUP_WEAPON_ASSAULTSHOTGUN,	<< 85.7,0,136 >>, FALSE, FALSE, FALSE)
	CREATE_WEAPON_PICKUP(98, PICKUP_PARACHUTE, << 10,0,-90 >>, FALSE, FALSE, FALSE) // Parachute
	CREATE_WEAPON_PICKUP(99, PICKUP_WEAPON_PUMPSHOTGUN, << -88,0,0>>, FALSE, FALSE, FALSE)

	CREATE_WEAPON_PICKUP(100, PICKUP_PARACHUTE, << 11,0,28.16 >>, FALSE, FALSE, FALSE) // Parachute
	CREATE_WEAPON_PICKUP(101, PICKUP_WEAPON_SAWNOFFSHOTGUN, << 92.8,0,50.4 >>, FALSE, FALSE, FALSE)
ENDPROC

/// PURPOSE: 
///    Remove all weapon pickup spawners
PROC REMOVE_WORLD_WEAPON_PICKUPS()
	INT i
	REPEAT NUMBER_OF_WEAPON_PICKUPS i
		IF DOES_PICKUP_EXIST(piWeaponPickup[i])
			REMOVE_PICKUP(piWeaponPickup[i])
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE: 
///    Create armour, health and weapon pickups
PROC CREATE_PICKUPS()
	CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Creating all pickups")
	SETUP_WORLD_ARMOUR_PICKUPS()
	SETUP_WORLD_GAS_PICKUPS()
	SETUP_WORLD_HEALTH_PICKUPS()
	SETUP_WORLD_WEAPON_PICKUPS()
	
	bPickupsCreated = TRUE
ENDPROC

PROC DESTROY_PICKUPS()
	CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Removing all world pickups...")
	REMOVE_WORLD_ARMOUR_PICKUPS()
	REMOVE_WORLD_GAS_PICKUPS()
	REMOVE_WORLD_HEALTH_PICKUPS()
	REMOVE_WORLD_WEAPON_PICKUPS()
	
	// Remove all mission unlock pickups
	IF bUnlockedExilePickups
		CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Removing Exile 1 unlock pickups...")
		REMOVE_EXILE1_UNLOCK_PICKUPS()
	ENDIF
	
	IF bUnlockedExileCGtoNGPickups
		CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Removing Exile 1 CGtoNG unlock pickups...")
		REMOVE_EXILE1_CGTONG_UNLOCK_PICKUPS()
	ENDIF
	
	IF bUnlockedTrevorCGtoNGPickups
		CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Removing Trevor 1 CGtoNG unlock pickups...")
		REMOVE_TREVOR1_CGTONG_UNLOCK_PICKUPS()
	ENDIF
	
	bPickupsCreated = FALSE
ENDPROC

/// PURPOSE: 
///    Listens for requests from other scripts to see if a pickup has been collected
PROC CHECK_QUERY_REQUESTS()
	
	SWITCH g_ePickupQuery 
	
		CASE PC_QUERY_INACTIVE
			// Waiting on request...
			// Need to call INIT_PICKUP_QUERY_REQUEST()
		BREAK
		
		CASE PC_QUERY_REQUEST
			
			SWITCH g_eQueryType
			
				CASE PC_QUERY_TYPE_ARMOUR
					IF DOES_PICKUP_EXIST(piArmourPickup[g_iPickupIndex])
					AND DOES_PICKUP_OBJECT_EXIST(piArmourPickup[g_iPickupIndex])
						g_bPickupCollected = FALSE
					ELSE
						g_bPickupCollected = TRUE
					ENDIF
				BREAK
				
				CASE PC_QUERY_TYPE_HEALTH
					IF DOES_PICKUP_EXIST(piHealthPickup[g_iPickupIndex])
					AND DOES_PICKUP_OBJECT_EXIST(piHealthPickup[g_iPickupIndex])
						g_bPickupCollected = FALSE
					ELSE
						g_bPickupCollected = TRUE
					ENDIF
				BREAK
				
				CASE PC_QUERY_TYPE_WEAPON
					IF DOES_PICKUP_EXIST(piWeaponPickup[g_iPickupIndex])
					AND DOES_PICKUP_OBJECT_EXIST(piWeaponPickup[g_iPickupIndex])
						g_bPickupCollected = FALSE
					ELSE
						g_bPickupCollected = TRUE
					ENDIF
				BREAK
				
				CASE PC_QUERY_TYPE_INVALID
					g_ePickupQuery = PC_QUERY_RESET
				BREAK
			ENDSWITCH

			g_ePickupQuery = PC_QUERY_RESULT
		BREAK
		
		CASE PC_QUERY_RESULT
			// Waiting on ambient script to confirm they have received the required
			// information -  need to call END_PICKUP_QUERY_REQUEST()
		BREAK
		
		CASE PC_QUERY_RESET
			g_iPickupIndex = 0
			g_bPickupCollected = FALSE
			g_ePickupQuery = PC_QUERY_INACTIVE
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: 
///    Check for completed missions unlocking new pickups
PROC CHECK_FOR_MISSION_UNLOCK_PICKUPS()
	IF NOT bUnlockedExilePickups
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EXILE1_PICKUPS_UNLOCKED)
			CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Unlocked pickups for Exile 1")
			SETUP_EXILE1_UNLOCK_PICKUPS()
			bUnlockedExilePickups = TRUE
		ENDIF
	ENDIF
	
	IF NOT bUnlockedExileCGtoNGPickups
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EXILE1_PICKUPS_UNLOCKED)
			IF IS_LAST_GEN_PLAYER()
				CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Unlocked CGtoNG pickups for Exile 1")
				SETUP_EXILE1_CGTONG_UNLOCK_PICKUPS()
				bUnlockedExileCGtoNGPickups = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bUnlockedTrevorCGtoNGPickups
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_1)
			IF IS_LAST_GEN_PLAYER()
				CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Unlocked CGtoNG pickups for Trevor 1")
				SETUP_TREVOR1_CGTONG_UNLOCK_PICKUPS()
				bUnlockedTrevorCGtoNGPickups = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///    Remove pickups and terminate thread
PROC CLEANUP_SCRIPT()
		
	#IF IS_DEBUG_BUILD
		// Cleanup widgets
		CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Deleting debug widgets...")
		IF DOES_WIDGET_GROUP_EXIST(mainWidget)
			DELETE_WIDGET_GROUP(mainWidget)
		ENDIF
		
		CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Removing any pickup blips")
		REMOVE_BLIPS(COUNT_OF(piArmourPickup), armourBlips)
		REMOVE_BLIPS(COUNT_OF(piGasPickup), gasBlips)
		REMOVE_BLIPS(COUNT_OF(piHealthPickup), healthBlips)
		REMOVE_BLIPS(COUNT_OF(piWeaponPickup), weaponBlips)
	#ENDIF
		
	// Remove all pickup instances
	CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Removing all world pickups...")
	REMOVE_WORLD_ARMOUR_PICKUPS()
	REMOVE_WORLD_GAS_PICKUPS()
	REMOVE_WORLD_HEALTH_PICKUPS()
	REMOVE_WORLD_WEAPON_PICKUPS()
	
	// Remove all mission unlock pickups
	IF bUnlockedExilePickups
		CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Removing Exile 1 unlock pickups...")
		REMOVE_EXILE1_UNLOCK_PICKUPS()
	ENDIF

	IF bUnlockedExileCGtoNGPickups
		CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Removing Exile 1 CGtoNG unlock pickups...")
		REMOVE_EXILE1_CGTONG_UNLOCK_PICKUPS()
	ENDIF

	IF bUnlockedTrevorCGtoNGPickups
		CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Removing Trevor 1 CGtoNG unlock pickups...")
		REMOVE_TREVOR1_CGTONG_UNLOCK_PICKUPS()
	ENDIF
	
	// End script
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT
	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO))
		CPRINTLN(DEBUG_AMBIENT, "Pickup Controller: Forced to cleanup (SP to MP)")
		CLEANUP_SCRIPT()
	ENDIF

	// Setup all pickup locates in the world
	CREATE_PICKUPS()
	
	// Setup basic debug widget - user needs to enable warp options 
	// to keep processing low when running normally
	#IF IS_DEBUG_BUILD
		SETUP_MAIN_WIDGET()
	#ENDIF

	// Main loop
	WHILE TRUE
	
		// #2007591 - Destroy ambient pickups while in animal form.
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			IF bPickupsCreated
				CPRINTLN(DEBUG_AMBIENT, "Destroying pickups due to being on mission as an animal/director.")
				DESTROY_PICKUPS()
			ENDIF
		ELSE
			IF NOT bPickupsCreated
				CPRINTLN(DEBUG_AMBIENT, "Coming of mission as an animal/director. Re-creating pickups.")
				CREATE_PICKUPS()
			ENDIF
		ENDIF

		// Check for pickup collected requests
		// This will typically be the Chop ambient script
		CHECK_QUERY_REQUESTS()
		
		// Check for pickups unlocked after completing flow missions
		CHECK_FOR_MISSION_UNLOCK_PICKUPS()

		// Check for warping to pickups
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
		#ENDIF
	
		WAIT(0)
	ENDWHILE

ENDSCRIPT
