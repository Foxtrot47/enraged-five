//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  David Roberts					Date: 23/05/14			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  			underWaterPickups								│
//│																				│
//│		DESCRIPTION: Basic world point script that streams and allocates		│
//│					 underwater pickups only when needed						│
//╘═════════════════════════════════════════════════════════════════════════════╛
// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "commands_object.sch"
USING "commands_misc.sch"
USING "script_debug.sch"
USING "script_clock.sch"
USING "title_update_globals.sch"

// Enums
ENUM UNDERWATER_WRECKSITE
	UWS_NONE = -1,
	
	UWS_PLANE_CRASH_01 = 0,
	UWS_CROP_DUSTER,
	UWS_LOST_CARGO,
	UWS_AGED_CARGOPLANE,
	UWS_SUNKEN_TUGBOAT,
	UWS_SUGGESTIVE_REMAINS,
	UWS_CONTAINER_SHIPWRECK,
	UWS_PADDLE_STEAMER,
	UWS_SUNKEN_BARGE,
	UWS_PLANE_CRASH_02,
	UWS_SHIP_WRECK,
	UWS_SECOND_TUGBOAT,

	MAX_WRECK_LOCATIONS
ENDENUM
UNDERWATER_WRECKSITE eUnderwaterWreck = UWS_NONE

// Constants
CONST_INT           MAX_WRECK_PICKUPS 20	// Max number of pickups per site
CONST_INT			ARMOUR_PICKUP_VAL -1	// Amount of points per armour - -1 is default (100)

// Variables
PICKUP_INDEX        piWreckPickup[MAX_WRECK_PICKUPS]
BOOL				bCashShouldSpawn
VECTOR				vecPlayer

// Debug
#IF IS_DEBUG_BUILD
	FUNC STRING GET_DEBUG_WRECK_NAME(INT iIndex)
		SWITCH iIndex
			CASE 0  RETURN "UWS_PLANE_CRASH_01" BREAK
			CASE 1  RETURN "UWS_CROP_DUSTER" BREAK
			CASE 2  RETURN "UWS_LOST_CARGO" BREAK
			CASE 3  RETURN "UWS_AGED_CARGOPLANE" BREAK
			CASE 4  RETURN "UWS_SUNKEN_TUGBOAT" BREAK
			CASE 5  RETURN "UWS_SUGGESTIVE_REMAINS" BREAK
			CASE 6  RETURN "UWS_CONTAINER_SHIPWRECK" BREAK
			CASE 7  RETURN "UWS_PADDLE_STEAMER" BREAK
			CASE 8  RETURN "UWS_SUNKEN_BARGE" BREAK
			CASE 9  RETURN "UWS_PLANE_CRASH_02" BREAK
			CASE 10 RETURN "UWS_SHIP_WRECK" BREAK
			CASE 11 RETURN "UWS_SECOND_TUGBOAT" BREAK
		ENDSWITCH		
		RETURN "UWS_NONE"
	ENDFUNC
#ENDIF

/// PURPOSE: 
///    Work out the current wreck site from the supplied coords
PROC GET_WRECK_LOCATION(VECTOR vCurrentPos)

	VECTOR vWreckPos[MAX_WRECK_LOCATIONS]

	// Wreck site coords
	vWreckPos[0]  = << -1029, 6517, -21 >>
	vWreckPos[1]  = << 3265, 6407, -47 >>
	vWreckPos[2]  = << 4215, 3603, -48 >>
	vWreckPos[3]  = << -3247, 3669, -28 >>
	vWreckPos[4]  = << -3192, 3029, -35 >>
	vWreckPos[5]  = << -2838, -446, -38 >>
	vWreckPos[6]  = << 3156, -307, -17 >>
	vWreckPos[7]  = << 2695, -1334, -26 >>
	vWreckPos[8]  = << 264, -2280, -8 >>
	vWreckPos[9]  = << 1804, -2993, -46 >>
	vWreckPos[10] = << -131, -2867, -10 >>
	vWreckPos[11] = << 3595, 3041, -15 >>

	INT i
	FOR i=0 TO ENUM_TO_INT(MAX_WRECK_LOCATIONS)-1
		IF ARE_VECTORS_ALMOST_EQUAL(vCurrentPos, vWreckPos[i])
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "Underwater Pickups: Found location ", GET_DEBUG_WRECK_NAME(i)) #ENDIF
			eUnderwaterWreck = INT_TO_ENUM(UNDERWATER_WRECKSITE, i)
			EXIT
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    Get the coordinates and types for pickups at the current location
/// PARAMS:
///    iIndex - The pickup index you're querying
///    pType - Variable to set to pickup type
///    vCoords - Variable to set to pickup coordinates
///    bAngles - Variable to set to euler angles. If non-zero pickup will not snap to ground.
/// RETURNS:
///    TRUE if data exists for requested index
FUNC BOOL GET_WRECK_PICKUP_DATA(INT iIndex, PICKUP_TYPE &pType, VECTOR &vCoords, VECTOR &vAngles, INT &iAmount)

	vAngles = <<0,0,0>> // Set to zero - pickups that need it can change it afterwards
	iAmount = -1 // Default amount
	
	SWITCH eUnderwaterWreck

		CASE UWS_PLANE_CRASH_01
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << -998.18927, 6538.74756, -31.81680 >>
					iAmount = 12000
				BREAK
				CASE 1
					pType = PICKUP_WEAPON_SMG
					vCoords = << -992.1827, 6568.4663, -25.4098 >>
				BREAK
				CASE 2
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << -980.2034, 6595.5835, -27.4281 >>
					iAmount = ARMOUR_PICKUP_VAL
				BREAK
				CASE 3
					pType = PICKUP_HEALTH_STANDARD
					vCoords = << -914.1810, 6644.1392, -33.8392 >>
				BREAK
				CASE 4
					pType = PICKUP_WEAPON_SMG
					vCoords = << -988.0040, 6524.1733, -37.6496 >>
				BREAK
				CASE 5
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << -975.5475, 6604.4834, -27.6453 >>
				BREAK
				CASE 6
					pType = PICKUP_WEAPON_ASSAULTRIFLE
					vCoords = << -947.2839, 6601.1406, -29.9503 >>
				BREAK
				CASE 7
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << -921.4030, 6613.2822, -31.4210 >>
				BREAK
				CASE 8
					pType = PICKUP_WEAPON_RPG
					vCoords = <<-1032.6805, 6543.8403, -29.6800>>
				BREAK
				CASE 9
					pType = PICKUP_WEAPON_MG
					vCoords = << -942.8458, 6547.7505, -35.4082 >>
				BREAK
				CASE 10
					pType = PICKUP_WEAPON_SMG
					vCoords = << -985.56, 6495.06, -36.07 >>
				BREAK
				CASE 11
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << -1006.82, 6491.08, -36.31 >>
				BREAK
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			RETURN TRUE
		BREAK
		CASE UWS_CROP_DUSTER
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << 3267.37158, 6408.69141, -49.36513 >>
					iAmount = 8000
				BREAK
				CASE 1
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << 3270.7068, 6417.4600, -50.2241 >>
				BREAK
				CASE 2
					pType = PICKUP_WEAPON_STICKYBOMB
					vCoords = << 3254.63, 6414.13, -47.78 >>
				BREAK
				CASE 3
					pType = PICKUP_WEAPON_PISTOL
					vCoords = << 3270.39, 6425.12, -51.36 >>
				BREAK
				CASE 4
					pType = PICKUP_WEAPON_PISTOL
					vCoords = << 3283.49, 6418.16, -51.91 >>
				BREAK
				CASE 5
					pType = PICKUP_WEAPON_STICKYBOMB
					vCoords = << 3259.42, 6404.40, -48.03 >>
				BREAK
				CASE 6
					pType = PICKUP_WEAPON_SMG
					vCoords = << 3230.95, 6402.33, -38.66 >>
				BREAK
				CASE 7
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 3246.36, 6390.96, -44.68 >>
				BREAK
				CASE 8
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 3256.19, 6386.68, -46.12 >>
				BREAK
				CASE 9
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << 3285.74, 6436.81, -53.08 >>
				BREAK
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			RETURN TRUE
		BREAK
		CASE UWS_LOST_CARGO
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << 4241.07129, 3600.49170, -46.81019 >>
					iAmount = 10000
				BREAK
				CASE 1
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << 4245.2549, 3595.4717, -48.4023 >>
				BREAK
				CASE 2
					pType = PICKUP_WEAPON_STICKYBOMB
					vCoords = << 4234.14, 3598.21, -48.61 >>
				BREAK
				CASE 3
					pType = PICKUP_WEAPON_STICKYBOMB
					vCoords = << 4233.57, 3593.39, -48.97 >>
				BREAK
				CASE 4
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 4239.69, 3598.28, -47.06 >>
				BREAK
				CASE 5
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 4238.71, 3600.99, -46.41 >>
				BREAK
				CASE 6
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 4195.88, 3587.34, -47.35 >>
				BREAK
				CASE 7
					pType = PICKUP_WEAPON_ASSAULTRIFLE
					vCoords = << 4170.99, 3666.27, -40.33 >>
				BREAK
				CASE 8
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << 4159.58, 3664.66, -35.82 >>
				BREAK
				CASE 9
					pType = PICKUP_WEAPON_GRENADELAUNCHER
					vCoords = << 4202.88, 3643.18, -44.81 >>
				BREAK
				CASE 10
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 4213.56, 3647.85, -43.00 >>
				BREAK
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			RETURN TRUE
		BREAK
		CASE UWS_AGED_CARGOPLANE
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << -3242.5000, 3667.48633, -30.78001 >>
					iAmount = 12500
				BREAK
				CASE 1
					pType = PICKUP_WEAPON_MG
					vCoords = << -3207.6277, 3668.3774, -37.7120 >>
				BREAK
				CASE 2
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << -3263.5366, 3673.9275, -36.4283 >>
					iAmount = ARMOUR_PICKUP_VAL
				BREAK
				CASE 3
					pType = PICKUP_WEAPON_PISTOL
					vCoords = << -3232.39, 3626.27, -36.50 >>
				BREAK
				CASE 4
					pType = PICKUP_WEAPON_GRENADELAUNCHER
					vCoords = << -3345.58, 3707.10, -98.22 >>
				BREAK
				CASE 5
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << -3312.91, 3673.69, -80.69 >>
				BREAK
				CASE 6
					pType = PICKUP_WEAPON_ASSAULTRIFLE
					vCoords = << -3328.56, 3723.76, -106.53 >>
				BREAK
				CASE 7
					pType = PICKUP_HEALTH_STANDARD
					vCoords = << -3351.79, 3705.02, -96.25 >>
				BREAK
				CASE 8
					pType = PICKUP_WEAPON_SMG
					vCoords = << -3377.97, 3717.68, -91.79 >>
				BREAK
				CASE 9
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << -3205.89, 3644.45, -39.76 >>
				BREAK
				CASE 10
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << -3330.38, 3695.19, -98.64 >>
				BREAK
				CASE 11
					pType = PICKUP_WEAPON_SMG
					vCoords = <<-3285.3323, 3677.1887, -80.9180>>
				BREAK
				CASE 12
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << -3208.50, 3649.73, -39.33 >>
				BREAK
				CASE 13
					pType = PICKUP_WEAPON_PISTOL
					vCoords = << -3231.45, 3645.32, -36.42 >>
				BREAK
				CASE 14
					pType = PICKUP_WEAPON_SMG
					vCoords = << -3251.42, 3629.54, -37.31 >>
				BREAK
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			RETURN TRUE
		BREAK
		CASE UWS_SUNKEN_TUGBOAT
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << -3178.46191, 3038.09448, -35.22701 >>
					iAmount = 9000
				BREAK
				CASE 1
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << -3168.2446, 3023.4277, -38.9854 >>
					iAmount = ARMOUR_PICKUP_VAL
				BREAK
				CASE 2
					pType = PICKUP_WEAPON_SNIPERRIFLE
					vCoords = << -3185.3225, 3055.6277, -40.6014 >>
				BREAK
				CASE 3
					pType = PICKUP_WEAPON_STICKYBOMB
					vCoords = << -3184.8499, 3042.1804, -40.2300 >>
				BREAK
				CASE 4
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << -3171.4509, 3025.4661, -37.8630 >>
					vAngles = << 287.2800, 5.0400, 136.2800 >>
				BREAK
				CASE 5
					pType = PICKUP_WEAPON_SMG
					vCoords = << -3174.09, 2993.02, -40.26 >>
				BREAK
				CASE 6
					pType = PICKUP_WEAPON_ASSAULTRIFLE
					vCoords = << -3198.3933, 3015.7593, -41.2039 >>
				BREAK
				CASE 7
					pType = PICKUP_WEAPON_SMG
					vCoords = << -3184.3508, 3021.4722, -40.0128 >>
				BREAK
				CASE 8
					pType = PICKUP_WEAPON_STICKYBOMB
					vCoords = << -3195.0173, 3061.2754, -41.5999 >>
				BREAK
				CASE 9
					pType = PICKUP_HEALTH_STANDARD
					vCoords = << -3173.9595, 3069.5830, -37.0880 >>
				BREAK
				CASE 10
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << -3193.34, 3023.59, -41.20 >>
				BREAK
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			RETURN TRUE
		BREAK
		CASE UWS_SUGGESTIVE_REMAINS
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << -2833.38354, -468.01550, -20.40657 >>
					iAmount = 25000
				BREAK
				CASE 1
					pType = PICKUP_HEALTH_STANDARD
					vCoords = << -2825.3301, -475.7387, -26.0530 >>
					vAngles = << -94.0000, 0.0000, 34.0000 >>
				BREAK
				CASE 2
					pType = PICKUP_WEAPON_RPG
					vCoords = << -2820.9639, -441.4406, -40.6061 >>
				BREAK
				CASE 3
					pType = PICKUP_WEAPON_MG
					vCoords = << -2841.8770, -512.4902, -67.7683 >>
					vAngles = << -95.7600, 0.0000, 20.1600 >>
				BREAK
				CASE 4
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << -2806.0251, -404.1602, -39.3099 >>
				BREAK
				CASE 5
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << -2822.3020, -398.0594, -40.3273 >>
				BREAK
				CASE 6
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << -2865.8323, -399.6541, -40.8540 >>
				BREAK
				CASE 7
					pType = PICKUP_WEAPON_GRENADELAUNCHER
					vCoords = << -2892.7722, -404.4103, -41.0115 >>
				BREAK
				CASE 8
					pType = PICKUP_WEAPON_PISTOL
					vCoords = << -2863.0625, -498.6346, -64.4100 >>
					vAngles = << 102.8800, 0.0000, 47.5200 >>
				BREAK
				CASE 9
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << -2782.8784, -477.8723, -51.8290 >>
				BREAK
				CASE 10
					pType = PICKUP_WEAPON_ASSAULTRIFLE
					vCoords = << -2822.5317, -479.8832, -61.0600 >>
					vAngles = << 87.8400, 0.0000, 120.9600 >>

				BREAK
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			RETURN TRUE
		BREAK
		CASE UWS_CONTAINER_SHIPWRECK
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << 3166.30273, -309.24265, -13.46703 >>
					iAmount = 20000
				BREAK
				CASE 1
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << 3181.0818, -333.6050, -30.3985 >>
					iAmount = ARMOUR_PICKUP_VAL
				BREAK
				CASE 2
					pType = PICKUP_WEAPON_ASSAULTRIFLE
					vCoords = << 3148.4033, -324.3696, -27.4506 >>
				BREAK
				CASE 3
					pType = PICKUP_WEAPON_STICKYBOMB
					vCoords = << 3153.7173, -291.6851, -27.2959 >>
				BREAK
				CASE 4
					pType = PICKUP_WEAPON_SNIPERRIFLE
					vCoords = << 3152.9617, -251.0106, -28.5123 >>
				BREAK
				CASE 5
					pType = PICKUP_HEALTH_STANDARD
					vCoords = << 3190.7268, -374.6689, -33.3551 >>
				BREAK
				CASE 6
					pType = PICKUP_WEAPON_MICROSMG
					vCoords = << 3188.7417, -363.4831, -31.9785 >>
				BREAK
				CASE 7
					pType = PICKUP_WEAPON_PISTOL
					vCoords = << 3175.0176, -368.1337, -30.7548 >>
				BREAK
				CASE 8
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 3166.6445, -356.8430, -29.1230 >>
				BREAK
				CASE 9
					pType = PICKUP_WEAPON_STICKYBOMB
					vCoords = << 3171.9861, -339.4889, -30.0216 >>
				BREAK
				CASE 10
					pType = PICKUP_WEAPON_SMG
					vCoords = << 3184.3066, -336.6477, -30.7653 >>
				BREAK
				CASE 11
					pType = PICKUP_WEAPON_GRENADELAUNCHER
					vCoords = << 3155.87, -314.77, -28.94 >>
				BREAK
				CASE 12
					pType = PICKUP_WEAPON_SMG
					vCoords = << 3151.5913, -293.2682, -27.3977 >>
				BREAK
				CASE 13
					pType = PICKUP_WEAPON_ASSAULTRIFLE
					vCoords = << 3175.2981, -318.2845, -27.8797 >>
				BREAK
				CASE 14
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << 3133.6492, -340.5377, -24.0701 >>
				BREAK
				CASE 15
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 3171.96, -306.42, -26.44 >>
				BREAK
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			RETURN TRUE
		BREAK
		CASE UWS_PADDLE_STEAMER
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << 2690.0488, -1407.1602, -23.1385 >>
					iAmount = 12000
				BREAK
				CASE 1
					pType = PICKUP_WEAPON_CROWBAR
					vCoords = << 2678.1948, -1387.2520, -12.5066 >>
				BREAK
				CASE 2
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << 2670.2378, -1415.6990, -24.4404 >>
				BREAK
				CASE 3
					pType = PICKUP_WEAPON_ASSAULTRIFLE
					vCoords = << 2692.9316, -1390.9557, -21.7692 >>
				BREAK
				CASE 4
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << 2660.8867, -1391.9243, -21.4938 >>
				BREAK
				CASE 5
					pType = PICKUP_WEAPON_PISTOL
					vCoords = << 2694.8530, -1381.5310, -13.7913 >>
					vAngles = << 95.0000, 0.0000, -32.3600 >>
				BREAK
				CASE 6
					pType = PICKUP_WEAPON_RPG
					vCoords = << 2702.0129, -1355.4290, -18.3013 >>
					vAngles = << 94.0000, 0.0000, -61.5200 >> 
				BREAK
				CASE 7
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 2691.1367, -1358.3881, -23.3247 >>
				BREAK
				CASE 8
					pType = PICKUP_HEALTH_STANDARD
					vCoords = << 2659.5532, -1363.3910, -21.2188 >>
				BREAK
				CASE 9
					pType = PICKUP_WEAPON_SNIPERRIFLE
					vCoords = << 2647.7019, -1399.8690, -18.9282 >>
					vAngles = << 85.6800, 0.0000, 70.5600 >>
				BREAK
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			RETURN TRUE
		BREAK
		CASE UWS_SUNKEN_BARGE
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << 263.03833, -2278.80835, -5.90399 >>
					iAmount = 7500
				BREAK
				CASE 1
					pType = PICKUP_WEAPON_CROWBAR
					vCoords = << 270.1463, -2268.9683, -4.8704 >>
				BREAK
				CASE 2
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << 262.2520, -2296.5461, -12.7082 >>
					iAmount = ARMOUR_PICKUP_VAL
				BREAK
				CASE 3
					pType = PICKUP_WEAPON_SMG
					vCoords = << 250.2981, -2291.3555, -6.0576 >>
				BREAK
				CASE 4
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 284.8206, -2270.1646, -9.7622 >>
				BREAK
				CASE 5
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 291.5531, -2262.1348, -8.4540 >>
				BREAK
				CASE 6
					pType = PICKUP_WEAPON_SAWNOFFSHOTGUN
					vCoords = << 282.7375, -2299.4905, -15.0066 >>
				BREAK
				CASE 7
					pType = PICKUP_HEALTH_STANDARD
					vCoords = << 262.5209, -2305.6453, -14.8879 >>
				BREAK
				CASE 8
					pType = PICKUP_WEAPON_STICKYBOMB
					vCoords = << 227.4087, -2284.6018, -3.8739 >>
				BREAK
				CASE 9
					pType = PICKUP_WEAPON_PISTOL
					vCoords = << 264.1946, -2260.7024, -7.1215 >>
				BREAK
				CASE 10
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << 247.1884, -2279.8521, -6.9333 >>
				BREAK
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			RETURN TRUE
		BREAK
		CASE UWS_PLANE_CRASH_02
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << 1779.4543, -2984.5491, -47.8651 >>
					iAmount = 16500
				BREAK
				CASE 1
					pType = PICKUP_HEALTH_STANDARD
					vCoords = << 1791.1947, -2962.9128, -44.5483 >>
				BREAK
				CASE 2
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << 1771.2632, -2966.9006, -47.0437 >>
				BREAK
				CASE 3
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << 1853.2838, -2939.2280, -49.1402 >>
				BREAK
				CASE 4
					pType = PICKUP_WEAPON_SMG
					vCoords = << 1803.0144, -2981.6716, -46.2222 >>
				BREAK
				CASE 5
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 1846.3389, -2969.9009, -54.1489 >>
				BREAK
				CASE 6
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << 1852.2867, -2967.1116, -54.1508 >>
				BREAK
				CASE 7
					pType = PICKUP_WEAPON_SMG
					vCoords = << 1848.7842, -2987.8860, -54.4582 >>
				BREAK
				CASE 8
					pType = PICKUP_WEAPON_GRENADELAUNCHER
					vCoords = << 1865.3719, -2938.2920, -47.5032 >>
				BREAK
				CASE 9
					pType = PICKUP_WEAPON_ASSAULTRIFLE
					vCoords = << 1829.6630, -2916.4241, -37.2224 >>
					vAngles = << 83.7600, 0.0000, 32.0000 >>
				BREAK
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			RETURN TRUE
		BREAK
		CASE UWS_SHIP_WRECK
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << -134.8904, -2870.5483, -21.0327 >>
					iAmount = 11000
				BREAK
				CASE 1
					pType = PICKUP_WEAPON_RPG
					vCoords = << -171.6402, -2873.3403, -21.9516 >>
				BREAK
				CASE 2
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << -173.2814, -2841.6643, -16.9981 >>
					iAmount = ARMOUR_PICKUP_VAL
				BREAK
				CASE 3
					pType = PICKUP_WEAPON_SAWNOFFSHOTGUN
					vCoords = << -106.2988, -2879.6846, -25.3423 >>
				BREAK
				CASE 4
					pType = PICKUP_HEALTH_STANDARD
					vCoords = << -276.8266, -2896.9128, -19.5037 >>
				BREAK
				CASE 5
					pType = PICKUP_WEAPON_ASSAULTRIFLE
					vCoords = << -282.5338, -2890.5554, -18.2755 >>
				BREAK
				CASE 6
					pType = PICKUP_WEAPON_SMG
					vCoords = << -261.7152, -2878.2239, -16.9878 >>
				BREAK
				CASE 7
					pType = PICKUP_WEAPON_PISTOL
					vCoords = << -239.3330, -2843.8862, -21.3757 >>
				BREAK
				CASE 8
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << -238.3619, -2870.6753, -24.0740 >>
				BREAK
				CASE 9
					pType = PICKUP_WEAPON_PISTOL
					vCoords = << -169.2835, -2834.5674, -16.0327 >>
				BREAK
				CASE 10
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << -184.2335, -2828.0972, -17.2720 >>
				BREAK
				CASE 11
					pType = PICKUP_WEAPON_GRENADE
					vCoords = << -211.7357, -2842.1680, -19.8885 >>
				BREAK
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			RETURN TRUE
		BREAK
		CASE UWS_SECOND_TUGBOAT
			SWITCH iIndex
				CASE 0
					pType = PICKUP_MONEY_CASE
					vCoords = << 3881.5168, 3042.3633, -24.8541 >>
					iAmount = 3000
				BREAK
				CASE 1
					pType = PICKUP_ARMOUR_STANDARD
					vCoords = << 3893.8899, 3026.4863, -30.7098 >>
				BREAK
				CASE 2
					pType = PICKUP_WEAPON_SAWNOFFSHOTGUN
					vCoords = << 3893.2207, 3032.4836, -30.1293 >>
					iAmount = ARMOUR_PICKUP_VAL
				BREAK
				CASE 3
					pType = PICKUP_WEAPON_PUMPSHOTGUN
					vCoords = << 3906.8992, 3034.0024, -33.4517 >>
				BREAK
				CASE 4
					pType = PICKUP_HEALTH_STANDARD
					vCoords = << 3923.1182, 3065.8325, -26.6635 >>
				BREAK
				CASE 5
					pType = PICKUP_WEAPON_ASSAULTRIFLE
					vCoords = << 3889.0266, 3062.0205, -27.3278 >>
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Create wreck pickups
PROC CREATE_WRECK_PICKUPS()

	INT iCount = 0
	PICKUP_TYPE puType
	VECTOR vecCoord, vecAngle
	INT iPlacementFlags, iAmount
	
	// Create pickups
	REPEAT MAX_WRECK_PICKUPS iCount 
		IF GET_WRECK_PICKUP_DATA(iCount, puType, vecCoord, vecAngle, iAmount)
			IF ARE_VECTORS_EQUAL(vecAngle, <<0,0,0>>)
				// Non-angled pickup
				iPlacementFlags = 0
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
				IF puType = PICKUP_MONEY_CASE
					// Only spawn cash and high value items if enough time has passed
					IF bCashShouldSpawn
						piWreckPickup[iCount] = CREATE_PICKUP(puType, vecCoord, iPlacementFlags, iAmount)
					ENDIF
				ELSE
					IF VDIST2(vecCoord, vecPlayer) > 400.0  // Prevent low-value item ammo exploit by ensuring player is >20m away
						IF puType = PICKUP_ARMOUR_STANDARD // Make a separate case in case someone wants these less than 100%
							piWreckPickup[iCount] = CREATE_PICKUP(puType, vecCoord, iPlacementFlags, iAmount)
						ELIF puType = PICKUP_WEAPON_GRENADELAUNCHER
						OR puType = PICKUP_WEAPON_RPG
						OR puType = PICKUP_WEAPON_MG
							// Only spawn cash and high value items if enough time has passed
							IF bCashShouldSpawn
								piWreckPickup[iCount] = CREATE_PICKUP(puType, vecCoord, iPlacementFlags)
							ENDIF
						ELSE
							// Other pickups spawn regardless
							piWreckPickup[iCount] = CREATE_PICKUP(puType, vecCoord, iPlacementFlags)
						ENDIF
					ELSE
						CPRINTLN(DEBUG_AMBIENT, "Pickup ", iCount, " at current location spawn blocked, player too close")
					ENDIF
				ENDIF
			ELSE
				// Angled pickup
				iPlacementFlags = 0
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
				IF puType = PICKUP_MONEY_CASE
					IF bCashShouldSpawn
						piWreckPickup[iCount] = CREATE_PICKUP_ROTATE(puType, vecCoord, vecAngle, iPlacementFlags, 10000)
					ENDIF
				ELSE
					IF VDIST2(vecCoord, vecPlayer) > 400.0  // Prevent low-value item ammo exploit by ensuring player is >20m away
						IF puType = PICKUP_ARMOUR_STANDARD // Make a separate case in case someone wants these less than 100%
							piWreckPickup[iCount] = CREATE_PICKUP_ROTATE(puType, vecCoord, vecAngle, iPlacementFlags)
						ELIF puType = PICKUP_WEAPON_GRENADELAUNCHER
						OR puType = PICKUP_WEAPON_RPG
						OR puType = PICKUP_WEAPON_MG
							// Only spawn cash and high value items if enough time has passed
							IF bCashShouldSpawn
								piWreckPickup[iCount] = CREATE_PICKUP_ROTATE(puType, vecCoord, vecAngle, iPlacementFlags)
							ENDIF
						ELSE
							// Other pickups spawn regardless
							piWreckPickup[iCount] = CREATE_PICKUP_ROTATE(puType, vecCoord, vecAngle, iPlacementFlags)
						ENDIF
					ELSE
						CPRINTLN(DEBUG_AMBIENT, "Pickup ", iCount, " at current location spawn blocked, player too close")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Check whether we have recently spawned a cash pickup
/// RETURNS:
///    TRUE if cash should spawn
FUNC BOOL GET_SHOULD_CASH_SPAWN()

	// See if we've spawned our first briefcase yet
	IF g_todNextBriefcaseSpawn = INVALID_TIMEOFDAY
		// Not spawned a case before - set time
		g_todNextBriefcaseSpawn = GET_CURRENT_TIMEOFDAY()
		ADD_TIME_TO_TIMEOFDAY(g_todNextBriefcaseSpawn, 0, 0, 0, 1, 0) // Once per day
		RETURN TRUE
	ENDIF

	// We've spawned a case before, see if enough time has passed
	IF IS_TIMEOFDAY_AFTER_TIMEOFDAY(GET_CURRENT_TIMEOFDAY(), g_todNextBriefcaseSpawn)
		// Enough time has passed - spawn permitted, reset timer
		g_todNextBriefcaseSpawn = GET_CURRENT_TIMEOFDAY()
		ADD_TIME_TO_TIMEOFDAY(g_todNextBriefcaseSpawn, 0, 0, 0, 1, 0) // Once per day
		RETURN TRUE
	ENDIF
	
	// Not enough time has passed
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Remove all pickup spawners
PROC REMOVE_WRECK_PICKUPS()
	
	INT i
	REPEAT MAX_WRECK_PICKUPS i
		IF DOES_PICKUP_EXIST(piWreckPickup[i])
			REMOVE_PICKUP(piWreckPickup[i])
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE: 
///    Remove pickups and terminate thread
PROC CLEANUP_SCRIPT()

	// Remove all pickup instances
	CPRINTLN(DEBUG_AMBIENT, "Underwater Pickups: Removing pickups...")
	REMOVE_WRECK_PICKUPS()

	// End script
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Main script loop
SCRIPT(coords_struct in_coords)

	// Obtain world point location
	VECTOR vInCoords = in_coords.vec_coord[0]

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		CPRINTLN(DEBUG_AMBIENT, "Underwater Pickups: Forced cleanup")
		CLEANUP_SCRIPT()
	ENDIF

	// Get current wreck location
	GET_WRECK_LOCATION(vInCoords)

	// Are we spawning cash pickups
	bCashShouldSpawn = GET_SHOULD_CASH_SPAWN()
	
	// Stop ammo exploit
	vecPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	
	// Create underwater pickup
	CREATE_WRECK_PICKUPS()

	// Main loop
	WHILE TRUE

		WAIT(0)
		
		// Check for leaving vicinity of the site
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			CPRINTLN(DEBUG_AMBIENT, "Underwater Pickups: Out of range")
			CLEANUP_SCRIPT()
		ENDIF
	ENDWHILE

ENDSCRIPT
