// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	mission_stat_watcher.sc
//		AUTHOR			:	Ak
//		DESCRIPTION		:	Collects the mission metrics, and handles rendering the end of
//							mission stat box
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "commands_cutscene.sch"
USING "commands_hud.sch"
USING "commands_cutscene.sch"
USING "mission_stat_public.sch"
USING "player_ped_public.sch"
USING "flow_public_core_override.sch"
USING "flow_help_public.sch"
USING "commands_stats.sch"
USING "help_at_location.sch"
USING "mission_stat_generated_lb_binds.sch"
USING "end_screen.sch"
USING "heist_end_screen.sch"
USING "minigame_midsized_message.sch"
USING "script_misc.sch"
  
//Set this to 1 to enable detailed debugging.
CONST_INT 	ENABLE_STAT_WATCHER_DEBUG 	0


//Mission passed screen timing.
CONST_INT	DELAY_BEFORE_DISPLAYING		1500
CONST_INT	DISPLAY_DURATION			15000


//SCALEFORM_INDEX mov//the stat box // gone
//SCALEFORM_INDEX splash//the splash text //gone
END_SCREEN_DATASET esd


INT iTimeDeltaThisUpdate = 0
//INT iTimeToDisplayScreen = -1

INT visiblesucceeded = 0
INT visibleattempted = visiblesucceeded

INT iHealthThisTick = visibleattempted
INT iHealthLastTick = 0
INT iHealthDelta = 0

//FLOAT fUploadPercentage = 0.0
//INT succeeded = 0
//INT attempted = 0

MISSION_STAT_SYSTEM_INVALIDATION_REASON eLastInvalidationReasonShown = MSSIR_VALID

PED_BONETAG pbt

FLOAT overallAccuracyMetric = 100.0
INT overallBulletsMetric = 0

//BOOL bUSE_ASSASSIN_OR_CLF_DLC = FALSE


#IF IS_DEBUG_BUILD
BOOL bDrawWatchlist = FALSE

SCRIPT_SHARD_BIG_MESSAGE shardBigMessage
SCALEFORM_INDEX buttons = NULL

BOOL bTestShardMessage = FALSE
INT iTWEAK_endFlash = ENUM_TO_INT(HUD_COLOUR_BLACK)
#ENDIF

//INT iSplashCount = 0
//BOOL bSplashLock = FALSE


    /*
     * 
    // diff: [REQUIRED] Is this stat used as a differentiator //(do we add on the score from this stat to allow for differentiation between 2 people who both met the objective)? For example if 2 people got > -10 car damage, we can add on the actual damage they took which may place one player above another. (bool)
	BOOL lb_include_in_calc
	// weighting: [REQUIRED] The amount of points this value is worth. For example each 1% of car damage is worth 1 point, but we don't want each millisecond of action cam use to be worth 1 point as it isn't fairly weighted. Therefore each 1 millisecond of action cam use is worth 0.001 points - this is variable and TBD per mission (float)
	FLOAT lb_weight_PPC
	// min: [REQUIRED] The lowest/worst score the player can legally get, or the cut off point where we don't care anymore if they're lower - may be negated if required (int)
	INT lb_min_legal
	// max: [REQUIRED] The highest/best score the player can legally get, or the cut off point where we don't care anymore if they#re higher - may be negated if required. Cut off point is particularly useful for unbounded values like time, bullets fired, damage etc. We can record scores up to 30 minutes, and if they go above this, we only add 30 mins worth of bonus score to their time etc. (int)
	INT lb_max_legal
	// precedence: [REQUIRED for mission objectives] The order which the objectives take precedence. 1 is worth the most. Objectives can have the same precedence level if there is no way to rank one higher than another. (int)
	INT lb_precedence
     * 
     * 
    */



PROC DO_LEADERBOARD_SCORE_CALCULATION_AND_SET()
	//LEADERBOARDS_ENUM g_eTargetMissionScoreLeaderboardBestRun
	//LEADERBOARDS_ENUM g_eTargetMissionScoreLeaderboardIndivRec


	IF g_iMissionStatsBeingTracked = 0
		CPRINTLN(DEBUG_MISSION_STATS, "DO_LEADEARBOARD_SCORE_CALCULATION_AND_SET no stats")
		EXIT
	ENDIF
	
	IF g_LeaderboardLastMissionID = -1
		CPRINTLN(DEBUG_MISSION_STATS, "DO_LEADEARBOARD_SCORE_CALCULATION_AND_SET invalid mission LB index")
		EXIT
	ENDIF
	
	
	INT i= 0
	REPEAT g_iMissionStatsBeingTracked i
		IF g_MissionStatTrackingArray[i].invalidationReason != MSSIR_VALID
			CPRINTLN(DEBUG_MISSION_STATS, "DO_LEADEARBOARD_SCORE_CALCULATION_AND_SET invalidated stat found, bailing out")
			EXIT 
		ENDIF
	ENDREPEAT

	CPRINTLN(DEBUG_MISSION_STATS, "DO_LEADEARBOARD_SCORE_CALCULATION_AND_SET Attempting leaderboard calculation for mission")
	
	INT score = 0
	/*FLOAT magnitude = 100000000
	
	//g_iMissionStatsBeingTracked
	//g_MissionStatTrackingArray
	
	
	
	
	    var score = 0;

        var magnitude = 1000000000; // this starts off HUGE. Will be modified by the precedence for each objective we loop through. The only way anyone could ever get a score greater than this would be if they completed the primary objective.

	
	
	    for(var i = 0; i < cols2.length; i++)
        {
            var col = cols2[i];
    
            // If the current objective is a target (ie a mission objective)...
            if(col.target)
            {
                // If the objective has been met/passed
                if(col.val >= col.targetval)
                {
                    // The divisor calculates the score to add based on the objective's precedence. 
                    // The lower the precedence, the more score it's worth.
                    var divisor = Math.pow(10,col.precedence)/10;

                    // Add on their bonus score for completing this objective.
                    score += magnitude/divisor;
                }
            }
    
            // If this objective is a differentiator...
            if(col.diff)
            {
                // If their score is greater than the max allowed score set it to the max value
                var val = (col.val > col.max) ? col.max : col.val;

                // If their score is less than the min allowed value set it to the min value
                val = (col.val < col.min) ? col.min : col.val;
        
                // Add on their differentiation score
                score += val * col.weighting;
            }
    
            console.log(col.name + " : " +score);
        }
	*/
	
	CPRINTLN(DEBUG_MISSION_STATS, "-----------------------------------\nStarting mission score calculation")
	i= 0
	INT iDiffSum = 0
	CPRINTLN(DEBUG_MISSION_STATS, "Gathering contributions from ", g_iMissionStatsBeingTracked)// " stats, magnitude is ",magnitude)
	REPEAT g_iMissionStatsBeingTracked i
				
//		#IF USE_CLF_DLC
//			DLC_ASSASSINATION_STATS m = INT_TO_ENUM(DLC_ASSASSINATION_STATS, ENUM_TO_INT(g_MissionStatTrackingArray[i].target))
//			BOOL target = !g_DLCMissionStatTrackingPrototypes[m].bHidden
//			BOOL diff = g_MissionStatTrackingPrototypes[m].lb_differentiator
//			INT precedence = g_MissionStatTrackingPrototypes[m].lb_precedence
//			FLOAT weighting = g_MissionStatTrackingPrototypes[m].lb_weight_PPC
//		#ENDIF
//		#IF USE_NRM_DLC
//			DLC_ASSASSINATION_STATS m = INT_TO_ENUM(DLC_ASSASSINATION_STATS, ENUM_TO_INT(g_MissionStatTrackingArray[i].target))
//			BOOL target = !g_DLCMissionStatTrackingPrototypes[m].bHidden
//			BOOL diff = g_MissionStatTrackingPrototypes[m].lb_differentiator
//			INT precedence = g_MissionStatTrackingPrototypes[m].lb_precedence
//			FLOAT weighting = g_MissionStatTrackingPrototypes[m].lb_weight_PPC
//		#ENDIF
		
//		#IF NOT USE_CLF_DLC
//		#if not USE_NRM_DLC
			ENUM_MISSION_STATS m = g_MissionStatTrackingArray[i].target
			BOOL target = !g_MissionStatTrackingPrototypes[m].bHidden
			BOOL diff = g_MissionStatTrackingPrototypes[m].lb_differentiator
			INT precedence = g_MissionStatTrackingPrototypes[m].lb_precedence
			FLOAT weighting = g_MissionStatTrackingPrototypes[m].lb_weight_PPC
//		#ENDIF
//		#endif
		
		
		BOOL bPassed = FALSE
		
		
		CPRINTLN(DEBUG_MISSION_STATS, "Target : ", target)
		CPRINTLN(DEBUG_MISSION_STATS, "Diff : ", diff)
		CPRINTLN(DEBUG_MISSION_STATS, "Precedence : ", precedence)
		CPRINTLN(DEBUG_MISSION_STATS, "Weighting : ", weighting)
		
		CPRINTLN(DEBUG_MISSION_STATS, "Raw value : ", g_MissionStatTrackingArray[i].ivalue )
		bPassed = FALSE
		
		
//		#IF USE_CLF_DLC
//			IF GET_DLC_MISSION_STAT_PASS_STATUS_FOR_VALUE(m, g_MissionStatTrackingArray[i].ivalue)
//				bPassed = TRUE
//			ENDIF
//		#ENDIF
		
//		#IF NOT USE_CLF_DLC
//		#if not USE_NRM_DLC
			IF GET_MISSION_STAT_PASS_STATUS_FOR_VALUE(m, g_MissionStatTrackingArray[i].ivalue)
				bPassed = TRUE
			ENDIF
//		#ENDIF
//		#endif
		
		CPRINTLN(DEBUG_MISSION_STATS, "Passed : ", bPassed )
		
		IF target//is a target
			IF bPassed
				/*
				FLOAT Divisor = POW(10.0,TO_FLOAT(precedence))/10.0
				CPRINTLN(DEBUG_MISSION_STATS, "This target stat's contribution is ",magnitude/Divisor)
				score += FLOOR(magnitude/Divisor)
				*/
				score += 100000000
				
				
				
				#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN(DEBUG_MISSION_STATS, "This target stat failed, no contrib")
				#ENDIF
			ENDIF
		ENDIF
		IF(diff)//is a differentiator
			//cap between the min and the max
			//INT maxv = g_MissionStatTrackingPrototypes[m].lb_max_legal
			//INT minv = g_MissionStatTrackingPrototypes[m].lb_max_legal
			INT cval = g_MissionStatTrackingArray[i].ivalue
			/*
			IF cval > maxv
				cval = maxv
			ENDIF
			IF cval < minv
				cval = minv
			ENDIF
			*/
			CPRINTLN(DEBUG_MISSION_STATS, "This differentiatior contribution is  : ", cval * weighting, " this value is from cval * weighting (", cval," * ",weighting ,")" )
			INT difso = FLOOR(cval * weighting)
			iDiffSum += difso
			score += difso
		ENDIF
		
		CPRINTLN(DEBUG_MISSION_STATS, "Score cumulative value is ",score)
	ENDREPEAT
	CPRINTLN(DEBUG_MISSION_STATS, "Mission score calculation complete\n-----------------------------------")
	
	//score = score//for now to prevent the compiler freaking out before the next section gets put in place and fixed
	g_LeaderboardLastMissionScore = score
	g_LeaderboardLastMissionScore_Derivs = iDiffSum
	CPRINTLN(DEBUG_MISSION_STATS, "Mission leaderboard score was: ", g_LeaderboardLastMissionScore," divsum ", g_LeaderboardLastMissionScore_Derivs)
	
	IF NETWORK_IS_SIGNED_ONLINE() //AND (NOT NETWORK_PLAYER_IS_CHEATER())
		IF g_LeaderboardLastMissionRCType = FALSE
			SP_MISSIONS mi = INT_TO_ENUM(SP_MISSIONS, g_LeaderboardLastMissionID)
			
			#IF USE_CLF_DLC
				CERRORLN(DEBUG_MISSION_STATS, "DO_LEADERBOARD_SCORE_CALCULATION_AND_SET - ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(mi), " needs binding up for the leaderboard.")
				EXIT
			#ENDIF
			#IF USE_NRM_DLC
				CERRORLN(DEBUG_MISSION_STATS, "DO_LEADERBOARD_SCORE_CALCULATION_AND_SET - ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(mi), " needs binding up for the leaderboard.")
				EXIT
			#ENDIF
			
			IF mi = SP_HEIST_JEWELRY_2
				//717//best
				//718//agg
				
				IF g_savedGlobals.sFlow.controls.intIDs[Get_Heist_Choice_FlowInt_ID(HEIST_JEWEL)] = HEIST_CHOICE_JEWEL_HIGH_IMPACT
				CPRINTLN(DEBUG_MISSION_STATS, "Alternate JH2 leaderboard upload")
						LeaderboardUpdateData writeData
						writeData.m_LeaderboardId = 716
						LEADERBOARDS2_WRITE_DATA(writeData)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, g_LeaderboardLastMissionScore,0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_MISSION_SCORE, g_LeaderboardLastMissionScore_Derivs,0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_PASSES, 1,0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_OBJECTIVES_PASSED, g_LeaderboardLastObjectivesPassed,0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_3, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_ACTUAL_TAKE), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_4, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_PERSONAL_TAKE), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_ABILITY_TIME, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_SPECIAL_ABILITY_TIME), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BEST_TIME, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_TIME), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_TIME_4, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_CASE_GRAB_TIME), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_PLAYER_DAMAGE, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_FRANKLIN_BIKE_DAMAGE), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_5, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_CASES_SMASHED), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CAR_DAMAGE, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_CAR_DAMAGE), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_1, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_MAX_SPEED), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_INNOCENTS_KILLED, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_INNOCENTS_KILLED), 0.0)
						writeData.m_LeaderboardId = 717
						LEADERBOARDS2_WRITE_DATA(writeData)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, g_LeaderboardLastMissionScore,0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_MISSION_SCORE, g_LeaderboardLastMissionScore_Derivs,0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_PASSES, 1,0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_OBJECTIVES_PASSED, g_LeaderboardLastObjectivesPassed,0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_3, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_ACTUAL_TAKE), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_4, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_PERSONAL_TAKE), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_ABILITY_TIME, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_SPECIAL_ABILITY_TIME), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BEST_TIME, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_TIME), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_TIME_4, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_CASE_GRAB_TIME), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_PLAYER_DAMAGE, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_FRANKLIN_BIKE_DAMAGE), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_5, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_CASES_SMASHED), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CAR_DAMAGE, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_CAR_DAMAGE), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_1, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_MAX_SPEED), 0.0)
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_INNOCENTS_KILLED, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(JH2A_INNOCENTS_KILLED), 0.0)
					
					
					
					
				ELSE
					GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARD(mi)
				ENDIF	
				
				
			ELIF mi = SP_MISSION_MICHAEL_2
			CPRINTLN(DEBUG_MISSION_STATS, "Alternate Michael2 leaderboard upload")
					LeaderboardUpdateData writeData
					writeData.m_LeaderboardId = 673
					LEADERBOARDS2_WRITE_DATA(writeData)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, g_LeaderboardLastMissionScore,0.0)
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_MISSION_SCORE, g_LeaderboardLastMissionScore_Derivs,0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_PASSES, 1,0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_OBJECTIVES_PASSED, g_LeaderboardLastObjectivesPassed,0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SWITCHES, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_TIMES_SWITCHED), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_ACCURACY, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_ACCURACY), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_HEADSHOTS, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_HEADSHOTS), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_TIME_1, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_MIKE_RESCUE_TIMER), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_1, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_WAYPOINT_USED), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CAR_DAMAGE, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_CAR_DAMAGE), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_INNOCENTS_KILLED, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_INNOCENTS_KILLED), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_KILLS), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BULLETS_FIRED, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_BULLETS_FIRED), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_PLAYER_DAMAGE, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_DAMAGE), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_TIME_2, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_TIME_TO_LOSE_TRIADS), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_2, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_MAX_SPEED), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_TIME_4, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_TIME_TO_SAVE_FRANKLIN), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_5, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_UNIQUE_TRIAD_DEATHS), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_ABILITY_TIME, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_SPECIAL_ABILITY_TIME), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BEST_TIME, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_TIME), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_TIME_3, 0, 0.0)
					writeData.m_LeaderboardId = 672
					LEADERBOARDS2_WRITE_DATA(writeData)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, g_LeaderboardLastMissionScore,0.0)
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_MISSION_SCORE, g_LeaderboardLastMissionScore_Derivs,0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_PASSES, 1,0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_OBJECTIVES_PASSED, g_LeaderboardLastObjectivesPassed,0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SWITCHES, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_TIMES_SWITCHED), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_ACCURACY, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_ACCURACY), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_HEADSHOTS, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_HEADSHOTS), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_TIME_1, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_MIKE_RESCUE_TIMER), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_1, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_WAYPOINT_USED), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CAR_DAMAGE, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_CAR_DAMAGE), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_INNOCENTS_KILLED, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_INNOCENTS_KILLED), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_KILLS), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BULLETS_FIRED, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_BULLETS_FIRED), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_PLAYER_DAMAGE, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_DAMAGE), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_TIME_2, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_TIME_TO_LOSE_TRIADS), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_2, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_MAX_SPEED), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_TIME_4, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_TIME_TO_SAVE_FRANKLIN), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_SCORE_3, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_UNIQUE_TRIAD_DEATHS), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_ABILITY_TIME, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_SPECIAL_ABILITY_TIME), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BEST_TIME, GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(MIC2_TIME), 0.0)
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SPECIAL_TIME_3, 0, 0.0)
			ELSE
				GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARD(mi)
			ENDIF
			
			g_savedGlobals.sFlow.missionSavedData[mi].iScore = g_LeaderboardLastMissionScore
			
		ELSE
			GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARDRC( INT_TO_ENUM(g_eRC_MissionIDs, g_LeaderboardLastMissionID))
		ENDIF
	ENDIF
	
	

ENDPROC




PROC TALLY_ACCURACY_METRIC()

	IF g_bMissionStatAccuracyTallied
		CPRINTLN(DEBUG_MISSION_STATS, "DON'T Tallying accuracy metrics!!!!????!!!!")
		EXIT
	ENDIF
	g_bMissionStatAccuracyTallied = TRUE
	CPRINTLN(DEBUG_MISSION_STATS, "Tallying accuracy metrics:")
	
	INT totalShots = 0
	INT totalHits = 0

	//INT g_iMissionStatsSP0Hits = 0
	//INT g_iMissionStatsSP0Shots = 0
	//INT g_iMissionStatsSP1Hits = 0
	//INT g_iMissionStatsSP1Shots = 0
	//INT g_iMissionStatsSP2Hits = 0
	//INT g_iMissionStatsSP2Shots = 0
	
	totalShots = (RETRIEVE_INT_STAT(SP0_SHOTS) - g_iMissionStatsSP0Shots)
	totalHits = (RETRIEVE_INT_STAT(SP0_HITS) - g_iMissionStatsSP0Hits)
	CPRINTLN(DEBUG_MISSION_STATS, "Sp 0 hits/shots : ", totalHits, "/", totalShots)
	
	
	totalShots += (RETRIEVE_INT_STAT(SP1_SHOTS) -g_iMissionStatsSP1Shots)
	totalHits += (RETRIEVE_INT_STAT(SP1_HITS) - g_iMissionStatsSP1Hits)
	CPRINTLN(DEBUG_MISSION_STATS, "+Sp 1 hits/shots : ", totalHits, "/", totalShots)
	
	totalShots += (RETRIEVE_INT_STAT(SP2_SHOTS) -g_iMissionStatsSP2Shots)
	totalHits += (RETRIEVE_INT_STAT(SP2_HITS) - g_iMissionStatsSP2Hits)
	CPRINTLN(DEBUG_MISSION_STATS, "+Sp 2 hits/shots : ", totalHits, "/", totalShots)
	
	//SCRIPT_ASSERT("Accuracy metrics end")
	
	//now use totalShots and totalHits to work out the mission accuracy
	WAIT(0)
	FLOAT fPercentage = 100.0
	IF NOT (totalShots = 0)
	
		CPRINTLN(DEBUG_MISSION_STATS, "totalHits/totalShots : ", totalHits, "/", totalShots)
		
		FLOAT ftotal = TO_FLOAT(totalShots)
		FLOAT fhits = TO_FLOAT(totalHits)
		
		
		fPercentage = TO_FLOAT(FLOOR((fhits/ftotal)*100.0))
		
		IF fPercentage > 100.0
			fPercentage = 100.0
		ENDIF
		
		CPRINTLN(DEBUG_MISSION_STATS, " : ", fPercentage, " \% \n\n")

	ENDIF
	
	overallAccuracyMetric = fPercentage
	overallBulletsMetric = totalShots
	
ENDPROC


PROC COMMUTE_FLOAT_STAT_REGISTERS_TO_INTS()
	
	IF g_iMissionStatsBeingTracked > MAX_TRACKED_MISSION_STATS
		CERRORLN(DEBUG_MISSION_STATS, "COMMUTE_FLOAT_STAT_REGISTERS_TO_INTS - g_iMissionStatsBeingTracked[", g_iMissionStatsBeingTracked, "] >= MAX_TRACKED_MISSION_STATS")
		SCRIPT_ASSERT("COMMUTE_FLOAT_STAT_REGISTERS_TO_INTS - g_iMissionStatsBeingTracked >= MAX_TRACKED_MISSION_STATS")
		EXIT
	ENDIF
	
	INT i = 0
	//INT t = g_MissionStatTrackingArray[i].target
	REPEAT g_iMissionStatsBeingTracked i
		IF g_MissionStatTrackingArray[i].ivalue = 0
			IF g_MissionStatTrackingArray[i].fvalue != 0.0
				g_MissionStatTrackingArray[i].ivalue = FLOOR(g_MissionStatTrackingArray[i].fvalue)
			ENDIF
		ENDIF
		
		ENUM_MISSION_STATS m = g_MissionStatTrackingArray[i].target
		IF ENUM_TO_INT(m) > 0
//#IF NOT USE_CLF_DLC
			IF g_MissionStatTrackingPrototypes[m].type = MISSION_STAT_TYPE_ACCURACY
				g_MissionStatTrackingArray[i].ivalue = ROUND(overallAccuracyMetric)
			ENDIF
			IF g_MissionStatTrackingPrototypes[m].type = MISSION_STAT_TYPE_BULLETS_FIRED
				g_MissionStatTrackingArray[i].ivalue = overallBulletsMetric
			ENDIF
//#ENDIF
//#IF USE_CLF_DLC
//			IF g_DLCMissionStatTrackingPrototypes[m].type = MISSION_STAT_TYPE_ACCURACY
//				g_MissionStatTrackingArray[i].ivalue = ROUND(overallAccuracyMetric)
//				CPRINTLN(DEBUG_MISSION_STATS, "	incrament accuracy value ", i, " to ", g_MissionStatTrackingArray[i].ivalue, " [", m, "]")
//			ENDIF
//			IF g_DLCMissionStatTrackingPrototypes[m].type = MISSION_STAT_TYPE_BULLETS_FIRED
//				g_MissionStatTrackingArray[i].ivalue = overallBulletsMetric
//				CPRINTLN(DEBUG_MISSION_STATS, "	incrament bullets fired value ", i, " to ", g_MissionStatTrackingArray[i].ivalue, " [", m, "]")
//			ENDIF
//#ENDIF
		ENDIF
	ENDREPEAT
	
//	IF bUSE_ASSASSIN_OR_CLF_DLC
//		BOOL bFoundAccuracy = FALSE
//		IF NOT bFoundAccuracy
//			i= 0
//			
//			REPEAT g_iMissionStatsBeingTracked i
//				ENUM_MISSION_STATS m
//				REPEAT MAX_TRACKED_MISSION_STATS m
//#IF NOT USE_CLF_DLC
//					IF g_MissionStatTrackingPrototypes[m].type = MISSION_STAT_TYPE_ACCURACY
//#ENDIF
//#IF USE_CLF_DLC
//					IF g_DLCMissionStatTrackingPrototypes[m].type = MISSION_STAT_TYPE_ACCURACY
//#ENDIF
//						g_MissionStatTrackingArray[i].ivalue = ROUND(overallAccuracyMetric)
//						CPRINTLN(DEBUG_MISSION_STATS, "	DLC incrament accuracy value ", i, " to ", g_MissionStatTrackingArray[i].ivalue, " [", m, "]")
//						
//						bFoundAccuracy = TRUE
//					ENDIF
//				ENDREPEAT
//			ENDREPEAT
//		ENDIF
//	ENDIF
ENDPROC


FUNC INT IS_ENTITY_IN_WATCH_LIST(ENTITY_INDEX ei)

	IF g_iEntityWatchListLoggedCount = 0
		RETURN -1
	ENDIF

	INT i = 0
	
	REPEAT MAX_ENTITY_WATCH_LIST_ENTRIES i 
		IF g_EntityWatchList[i].index = ei 
			RETURN i
		ENDIF
	ENDREPEAT

	RETURN -1
ENDFUNC


PROC UPDATE_DAMAGE_TABLE_STATS(INT fiscalDamage)

	INT i= 0
	REPEAT g_iMissionStatsBeingTracked i
		ENUM_MISSION_STATS m = g_MissionStatTrackingArray[i].target
		
//#IF NOT USE_CLF_DLC
		IF g_MissionStatTrackingPrototypes[m].type = MISSION_STAT_TYPE_FINANCE_TABLE
//#ENDIF
//#IF USE_CLF_DLC
//		IF g_DLCMissionStatTrackingPrototypes[m].type = MISSION_STAT_TYPE_FINANCE_TABLE
//#ENDIF
			g_MissionStatTrackingArray[i].ivalue += fiscalDamage
			
			CPRINTLN(DEBUG_MISSION_STATS, "	incrament finance table value ", i, " to ", g_MissionStatTrackingArray[i].ivalue, " [", m, "]")
		ENDIF
		
	ENDREPEAT

ENDPROC

PROC HEADSHOT_KILL_DETECTED()

	IF g_iEntityWatchListLoggedCount = 0
		SCRIPT_ASSERT("HEADSHOT_KILL_DETECTED called with g_iEntityWatchListLoggedCount = 0")
		EXIT
	ENDIF
	
	BOOL bFoundAHeadshot = FALSE
	
	INT i
	REPEAT g_iMissionStatsBeingTracked i
		ENUM_MISSION_STATS m = g_MissionStatTrackingArray[i].target
		
		IF g_MissionStatTrackingPrototypes[m].type = MISSION_STAT_TYPE_HEADSHOTS
			g_MissionStatTrackingArray[i].ivalue += 1
			bFoundAHeadshot = TRUE
			
			CPRINTLN(DEBUG_MISSION_STATS, "Incrementing headshot value ", i, " to ", g_MissionStatTrackingArray[i].ivalue, " [", m, "]")
		ENDIF
		
	ENDREPEAT

	IF NOT bFoundAHeadshot
		SCRIPT_ASSERT("HEADSHOT_KILL_DETECTED called without finding valid stat")
	ENDIF
	
ENDPROC

PROC INNOCENT_PED_KILLED()
	INT i= 0
	REPEAT g_iMissionStatsBeingTracked i
		ENUM_MISSION_STATS m = g_MissionStatTrackingArray[i].target
		
//#IF NOT USE_CLF_DLC
		IF g_MissionStatTrackingPrototypes[m].type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//#ENDIF
//#IF  USE_CLF_DLC
//		IF g_DLCMissionStatTrackingPrototypes[m].type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//#ENDIF
			g_MissionStatTrackingArray[i].ivalue += 1
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC UPDATE_DEATH_TRIGGERS()

	//nothing to update
	IF g_StatSystemWeponpoolEntries = 0 
	AND g_StatSystemDeadpoolEntries = 0
	AND g_iFinanceLookupLogged = 0
	AND g_iEntityWatchListLoggedCount = 0
	AND !g_bTrackingInnocentsLogged
		
		#IF IS_DEBUG_BUILD
		bDrawWatchlist = FALSE
		#ENDIF
		
		EXIT
	ENDIF
	
	INT i,j,found
	EVENT_NAMES eventType
	STRUCT_ENTITY_ID sei
	ENTITY_INDEX soi
	WEAPON_TYPE wep 
	MODEL_NAMES mn
	BOOL done
	RELATIONSHIP_TYPE relgo
	
	INT fiscalDamage = 0
	INT ewlindex //ent watch list index for headshots
	
	

	
	IF g_iEntityWatchListLoggedCount > 0
		i = 0
		REPEAT MAX_ENTITY_WATCH_LIST_ENTRIES i
			IF g_EntityWatchList[i].index != NULL
				IF NOT DOES_ENTITY_EXIST(g_EntityWatchList[i].index)
					g_EntityWatchList[i].index = NULL
					--g_iEntityWatchListLoggedCount
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	PED_INDEX inped
	PED_TYPE pedTypeValue
	i = 0
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) i
        eventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, i)
        SWITCH (eventType)
			
			CASE EVENT_ENTITY_DESTROYED
			  	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI,i, sei,SIZE_OF(STRUCT_ENTITY_ID))
                IF DOES_ENTITY_EXIST(sei.EntityId)
					IS_ENTITY_DEAD(sei.EntityId) //it is dead or it wouldn't be on the destroyed list
					//but we have to check anyway to prevent assert
					
					CPRINTLN(DEBUG_MISSION_STATS, "")
					CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - Dead entity on watch list")
					IF g_iFinanceLookupLogged > 0
						mn = GET_ENTITY_MODEL(sei.EntityId)
						j = 0
						REPEAT g_iFinanceLookupLogged j
							IF g_FinanaceLookup[j].target = mn
								fiscalDamage += g_FinanaceLookup[j].value
							ENDIF
						ENDREPEAT
					ENDIF
					
					IF IS_ENTITY_A_PED(sei.EntityId)//is a ped
					AND GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId) != PLAYER_PED_ID()
					  
						soi = GET_PED_SOURCE_OF_DEATH(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId))
						//GET_PED_SOURCE_OF_DEATH (killer id)
						//GET_PED_CAUSE_OF_DEATH (weapon enum)
						//GET_PED_TIME_OF_DEATH (time) 
						
						BOOL bVehKill
						bVehKill = FALSE
						
						IF DOES_ENTITY_EXIST(soi)
							IF IS_ENTITY_A_VEHICLE(soi)
								VEHICLE_INDEX vehTemp
								
								CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - vehicle death detected")
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								ELSE
									vehTemp = GET_PLAYERS_LAST_VEHICLE()
								ENDIF
								
								IF DOES_ENTITY_EXIST(vehTemp)
								AND GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(soi) = vehTemp
									CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - vehicle death with players vehicle")
									bVehKill = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF (IS_ENTITY_A_PED(soi) AND GET_PLAYER_PED(GET_PLAYER_INDEX()) = GET_PED_INDEX_FROM_ENTITY_INDEX(soi))//ped vs ped and is player
						OR bVehKill
						
							IF IS_ENTITY_A_PED(soi)
								//killed by a ped with a weapon
								wep = GET_PED_CAUSE_OF_DEATH(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId))
								
								//check for headshot
								IF g_iEntityWatchListLoggedCount > 0
								
									ewlindex = IS_ENTITY_IN_WATCH_LIST(sei.EntityId)
									IF ewlindex != -1
									
										GET_PED_LAST_DAMAGE_BONE(GET_PED_INDEX_FROM_ENTITY_INDEX(g_EntityWatchList[ewlindex].index), pbt)
										
										IF pbt = BONETAG_HEAD
										OR pbt = BONETAG_NECK
										
											IF wep != WEAPONTYPE_INVALID
											AND wep != WEAPONTYPE_UNARMED
												CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - headshot detected with valid weapon ", GET_THE_WEAPON_NAME(wep))
												
												HEADSHOT_KILL_DETECTED()
												g_EntityWatchList[ewlindex].index = NULL
												--g_iEntityWatchListLoggedCount
											ELSE
												CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - headshot detected, but with INVALID weapon ", GET_THE_WEAPON_NAME(wep))
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//do wepkill test
								IF g_StatSystemWeponpoolEntries > 0
									j = 0
									REPEAT g_StatSystemWeponpoolEntries j//weppool is contiguous
										IF g_StatSystemWeaponpool[j].weptar = wep
											//auto increment
											CPRINTLN(DEBUG_MISSION_STATS, "Weapon pool stat auto increment attempt. Wep: ",wep," stat: ", g_StatSystemWeaponpool[j].statar)
											INFORM_MISSION_STATS_OF_INCREMENT(g_StatSystemWeaponpool[j].statar)
										ENDIF
									ENDREPEAT
								ENDIF
								
							ELSE
								CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - soi is not a ped")
							ENDIF
							
							//check for innocent
							IF g_bTrackingInnocentsLogged
							//"We would like to track the number of Innocent 
							//Pedestrians killed in every mission. 
							//We define innocent pedestrians by peds 
							//who are not hostile towards the player. 
							//Any enemies with red blips attached to 
							//them or any cops when on a wanted level 
							//should be considered enemies, 
							//anyone else should be innocent."
								inped = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId)
								pedTypeValue = GET_PED_TYPE(inped)
								//IF GET_PED_TYPE(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId)) != PEDTYPE_MISSION//PEDTYPE_MISSION
								IF NOT IS_ENTITY_A_MISSION_ENTITY(sei.EntityId)
								/*AND (
											GET_PED_TYPE(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId)) = PEDTYPE_CIVFEMALE
										OR 	GET_PED_TYPE(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId)) = PEDTYPE_CIVMALE
										OR 	GET_PED_TYPE(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId)) = PEDTYPE_FIRE
										OR 	GET_PED_TYPE(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId)) = PEDTYPE_MEDIC
										OR 	GET_PED_TYPE(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId)) = PEDTYPE_PROSTITUTE
										
									)*/
									AND (
											(NOT (pedTypeValue = PEDTYPE_CRIMINAL))
										AND (NOT (pedTypeValue = PEDTYPE_DEALER))
										AND (NOT (pedTypeValue = PEDTYPE_GANG_CHINESE_JAPANESE))
										AND (NOT (pedTypeValue = PEDTYPE_GANG_PUERTO_RICAN))
										AND (NOT (pedTypeValue = PEDTYPE_SWAT))
										AND (NOT (pedTypeValue = PEDTYPE_ARMY))
										AND (NOT (pedTypeValue = PEDTYPE_ANIMAL))
									)	
								
									
								 	relgo = GET_RELATIONSHIP_BETWEEN_PEDS(GET_PLAYER_PED(GET_PLAYER_INDEX()), GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId))						
									SWITCH relgo
									
									    CASE ACQUAINTANCE_TYPE_PED_NONE			FALLTHRU
									    CASE ACQUAINTANCE_TYPE_PED_RESPECT      FALLTHRU        
									    CASE ACQUAINTANCE_TYPE_PED_LIKE			FALLTHRU
									    CASE ACQUAINTANCE_TYPE_PED_IGNORE
										
											IF  IS_PED_HUMAN(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.EntityId))
												//AND(NOT IS_ENTITY_A_MISSION_ENTITY(sei.EntityId))
												CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - innocent ped killed")
												INNOCENT_PED_KILLED()
											ENDIF
											
										BREAK
										
										DEFAULT 
											CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - Ped was agressive towards player, not counted towards innocents killed")
										BREAK
										
									ENDSWITCH
								#IF IS_DEBUG_BUILD
								ELSE
									CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - Ped was invalid type for innocent check")
								#ENDIF
								ENDIF
								
							ENDIF
							
							//do deadpool test
							IF g_StatSystemDeadpoolEntries > 0
								j = 0
								found = g_StatSystemDeadpoolEntries
								done = FALSE
								CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - g_StatSystemDeadpoolEntries > 0")
								WHILE !done
									IF g_StatSystemDeadpool[j].entar != null
										IF DOES_ENTITY_EXIST(g_StatSystemDeadpool[j].entar)
											//it actually exists, was it killed?
											IF IS_ENTITY_DEAD(g_StatSystemDeadpool[j].entar)
												IF g_StatSystemDeadpool[j].entar = sei.EntityId
													//it was the targeted ped! 
													//Remove from list and do increment
													CPRINTLN(DEBUG_MISSION_STATS, "Dead pool stat auto increment attempt. stat: ", g_StatSystemDeadpool[j].statar)
													INFORM_MISSION_STATS_OF_INCREMENT(g_StatSystemDeadpool[j].statar)
													g_StatSystemDeadpool[j].entar = null
												ENDIF
											ENDIF
										ELSE //doesn't exist, purge from dead list
											g_StatSystemDeadpool[j].entar = null
										ENDIF
										--found
									ENDIF
									++j
									IF j = STAT_DEADPOOL_DEPTH
									OR found = 0
										done = TRUE
									ENDIF
								ENDWHILE
								
								
								//clean up the deadpool if needed
								IF g_StatSystemDeadpoolEntries > 0
								
									WHILE ((g_StatSystemDeadpool[g_StatSystemDeadpoolEntries-1].entar = null)
											AND (g_StatSystemDeadpoolEntries > 1))
										IF g_StatSystemDeadpoolEntries > 1
											--g_StatSystemDeadpoolEntries
											CPRINTLN(DEBUG_MISSION_STATS, "stat watcher cleanup deadpool list reduced to ", g_StatSystemDeadpoolEntries)
										ENDIF
									ENDWHILE
									
								ENDIF
								
								
							ELSE
								CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - g_StatSystemDeadpoolEntries <= 0")
							ENDIF
						
						ELSE		//ELSE//hit by car
							
							#IF IS_DEBUG_BUILD
							IF IS_ENTITY_A_PED(soi)
								IF GET_PLAYER_PED(GET_PLAYER_INDEX()) = GET_PED_INDEX_FROM_ENTITY_INDEX(soi)
									CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - soi is ped, soi is player??")
								ELSE
									CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - soi is ped, soi is not player??")
								ENDIF
							ELIF IS_ENTITY_A_VEHICLE(soi)
								CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - soi is not ped [vehicle], soi cannot be player??")
							ELIF IS_ENTITY_AN_OBJECT(soi)
								CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - soi is not ped [object], soi cannot be player??")
							ELSE
								CPRINTLN(DEBUG_MISSION_STATS, "KILL STATS TRACKING - soi is not ped [other], soi cannot be player??")
							ENDIF
							#ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT


	IF fiscalDamage > 0
	
		//update damage table stats
		UPDATE_DAMAGE_TABLE_STATS(fiscalDamage)
		
	
	ENDIF



ENDPROC

//INT lam1ArmourDelta = 0

PROC UPDATE_TRACKER()


	UPDATE_DEATH_TRIGGERS()


 
	//IF (NOT IS_PLAYER_DEAD(GET_PLAYER_INDEX()))// AND (NOT )
		//IF g_bMissionStatSystemSequenceStatus AND IS_PLAYER_SCRIPT_CONTROL_ON(GET_PLAYER_INDEX()) AND (NOT IS_CUTSCENE_ACTIVE()) AND (NOT IS_PAUSE_MENU_ACTIVE())
		IF g_bMissionStatSystemSequenceStatus AND IS_PLAYER_PLAYING(GET_PLAYER_INDEX())  AND (NOT IS_CUTSCENE_ACTIVE()) AND (NOT IS_PAUSE_MENU_ACTIVE())
			CPRINTLN(DEBUG_MISSION_STATS, "Mission_stat_watcher.sc: Mission timer is currently paused by script while the player has control, this should not be the case if a sequence is not running. Please call INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END().\n")
			g_bMissionStatSystemSequenceStatus = FALSE
		ENDIF
		//ENDIF
	//ENDIF
	
	IF g_bMissionStatTimeDeltaReset
	
		g_bMissionStatTimeDeltaReset = FALSE
		//iTimeLastUpdate = GET_GAME_TIMER()
		//iTimeThisUpdate = iTimeLastUpdate
		iTimeDeltaThisUpdate = 0
	ELSE
	
		
	ENDIF
	
	BOOL bDoTimer = TRUE
	BOOL bDoSpeed = TRUE
	IF IS_PAUSE_MENU_ACTIVE()
		bDoTimer = FALSE
	ENDIF
	
	IF NOT HAS_CUTSCENE_FINISHED()
		bDoTimer = FALSE
		bDoSpeed = FALSE
	ENDIF
	
	IF g_bMissionStatSystemSequenceStatus
		bDoTimer = FALSE
		bDoSpeed = FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
		bDoTimer = FALSE
		bDoSpeed = FALSE
	ENDIF
	
	IF bDoTimer //1522664
	
		iTimeDeltaThisUpdate =  ROUND(GET_FRAME_TIME() * 1000.0)
		
		
		/*
		iTimeThisUpdate = GET_GAME_TIMER()
		IF iTimeThisUpdate > iTimeLastUpdate 
			iTimeDeltaThisUpdate = iTimeThisUpdate - iTimeLastUpdate
			CPRINTLN(DEBUG_MISSION_STATS, "WINDOWED TIMER DEBUG - iTimeDeltaThisUpdate is: ", iTimeDeltaThisUpdate)
			g_LeaderboardLastMissionBestTime += iTimeDeltaThisUpdate
		ELSE
			iTimeDeltaThisUpdate = 0
			CPRINTLN(DEBUG_MISSION_STATS, "WINDOWED TIMER DEBUG - iTimeDeltaThisUpdate set to 0 as iTimeThisUpdate <= iTimeLastUpdate")
		ENDIF
		
		iTimeLastUpdate = iTimeThisUpdate
		*/
		
	//ELSE	//stall
		//iTimeThisUpdate = GET_GAME_TIMER()
		//iTimeLastUpdate = iTimeThisUpdate
	ENDIF
	
	
	//IF NOT IS_PLAYER_DEAD(GET_PLAYER_INDEX())
	//IS_CUTSCENE_ACTIVE()
	/*
		IF ((IS_PAUSE_MENU_ACTIVE() OR (NOT HAS_CUTSCENE_FINISHED()) OR g_bMissionStatSystemSequenceStatus)) 
		//AND IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
		
			INT ithis = iTimeThisUpdate
			//GET_GAME_TIMER()
			//iTimeLastUpdate = iTimeThisUpdate
			iTimeThisUpdate = GET_GAME_TIMER()
			IF iTimeThisUpdate > iTimeLastUpdate 
				iTimeLastUpdate = ithis
				iTimeDeltaThisUpdate = iTimeThisUpdate - iTimeLastUpdate
				
				g_LeaderboardLastMissionBestTime += iTimeDeltaThisUpdate
			ELSE
				iTimeDeltaThisUpdate = 0
			ENDIF
		ELSE

			iTimeLastUpdate = GET_GAME_TIMER()
			iTimeThisUpdate = iTimeLastUpdate
			iTimeDeltaThisUpdate = 0
			 
		ENDIF
		*/
	//ENDIF
	
	//single entity health delta
	
	
	
	
	
		/*
	REPEAT g_iMissionStatsBeingTracked i
	
		IF specificTarget = UNSET_MISSION_STAT_ENUM
		OR g_MissionStatTrackingArray[i].target = specificTarget
			IF g_MissionStatTrackingArray[i].TrackingEntity != in 
				g_MissionStatTrackingArray[i].TrackingEntity = in
				g_MissionStatTrackingArray[i].bTrackingEntityChanged = TRUE
				g_MissionStatTrackingArray[index].iTrackingDelta = 0
			ENDIF
		ENDIF
	*/
/*
	IF NOT (g_MissionStatSingleDamageWatchEntity = NULL)
		IF DOES_ENTITY_EXIST(g_MissionStatSingleDamageWatchEntity)
			IF NOT (IS_ENTITY_DEAD(g_MissionStatSingleDamageWatchEntity)) 
			AND (IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY) 
				OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_RANDOM_CHAR))


				
				IF g_bMissionDamageStatEntityChanged = FALSE
					iHealthLastTick = iHealthThisTick
					
					
					IF g_MissionStatSingleDamageWatchStat = LAM1_UNMARKED
						IF IS_ENTITY_A_PED(g_MissionStatSingleDamageWatchEntity)
							INT newarm = GET_PED_ARMOUR(GET_PED_INDEX_FROM_ENTITY_INDEX(g_MissionStatSingleDamageWatchEntity))
							INT newhealth = GET_ENTITY_HEALTH(g_MissionStatSingleDamageWatchEntity)
							 
							IF newarm < lam1ArmourDelta
								//some or all of the damage was taken to armour
								
								INT armourchange = lam1ArmourDelta - newarm
								INT healthchange =iHealthLastTick - newhealth
								
								IF healthchange > armourchange
									//damage was taken to health
									iHealthThisTick = iHealthLastTick - (healthchange - armourchange)
									CPRINTLN(DEBUG_MISSION_STATS, "Lam 1 specific health delta override is : t ", iHealthThisTick, "  :  l ",  iHealthLastTick)
								ENDIF
								
							ELSE
								iHealthThisTick = GET_ENTITY_HEALTH(g_MissionStatSingleDamageWatchEntity)
							ENDIF
							
							
							
						ELSE
							iHealthThisTick = GET_ENTITY_HEALTH(g_MissionStatSingleDamageWatchEntity)	
						ENDIF
					ELSE
						iHealthThisTick = GET_ENTITY_HEALTH(g_MissionStatSingleDamageWatchEntity)	
					
					ENDIF
					
					
					iHealthDelta = iHealthThisTick - iHealthLastTick 
				
				ELSE
					iHealthThisTick = GET_ENTITY_HEALTH(g_MissionStatSingleDamageWatchEntity)	
					iHealthDelta = iHealthThisTick
					
					IF g_MissionStatSingleDamageWatchStat = LAM1_UNMARKED
						IF IS_ENTITY_A_PED(g_MissionStatSingleDamageWatchEntity)
							//PED_INDEX pedarm = GET_PED_FROM_ENTITY()
							lam1ArmourDelta = GET_PED_ARMOUR(GET_PED_INDEX_FROM_ENTITY_INDEX(g_MissionStatSingleDamageWatchEntity))
						ENDIF
					ENDIF
					
					
					g_bMissionDamageStatEntityChanged = FALSE
				
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	*/
	
	
	//for each registered stat
	INT i = 0
	FLOAT fSpeed = 0
	REPEAT g_iMissionStatsBeingTracked i
		
		IF ENUM_TO_INT(g_MissionStatTrackingArray[i].target) >= 0
			
			ENUM_MISSION_STAT_TYPES eType = g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].type
//			IF bUSE_ASSASSIN_OR_CLF_DLC		
//				#IF USE_CLF_DLC
//				eType = g_DLCMissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].type
//				#ENDIF
//				#IF NOT USE_CLF_DLC
//				#if not USE_NRM_DLC
//				eType = g_dlcAssasinationStatConfig.type[i]
//				#ENDIF
//				#endif
//			ENDIF
			
			SWITCH eType
				CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
					IF g_MissionStatTrackingArray[i].TrackingEntity != NULL
						IF g_MissionStatTrackingArray[i].bTrackingEntityChanged = TRUE
							CDEBUG3LN(DEBUG_MISSION_STATS, "track damage entity [", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target), "] - bTrackingEntityChanged is TRUE")
							
							g_MissionStatTrackingArray[i].iTrackingDelta = 0
							g_MissionStatTrackingArray[i].ivalue = 0
							g_MissionStatTrackingArray[i].bTrackingEntityChanged = FALSE
						ELSE
							IF DOES_ENTITY_EXIST(g_MissionStatTrackingArray[i].TrackingEntity)
								IF NOT IS_ENTITY_DEAD(g_MissionStatTrackingArray[i].TrackingEntity)
									iHealthLastTick = g_MissionStatTrackingArray[i].iTrackingDelta
									iHealthThisTick = GET_ENTITY_HEALTH(g_MissionStatTrackingArray[i].TrackingEntity)
									iHealthDelta =  iHealthLastTick - iHealthThisTick
									
									CDEBUG1LN(DEBUG_MISSION_STATS, "track damage ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(g_MissionStatTrackingArray[i].TrackingEntity)), " [", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target),
											"] - iTrackingDelta ", g_MissionStatTrackingArray[i].iTrackingDelta,
											", ivalue ", g_MissionStatTrackingArray[i].ivalue,
											", iHealthDelta ", iHealthDelta,
											" [iHealthLastTick:", iHealthLastTick,
											"- iHealthThisTick ", iHealthThisTick, "]")
									
									IF iHealthDelta > 0 //health lost
										g_MissionStatTrackingArray[i].ivalue += iHealthDelta
										//B* 1717661: If the value is negative, flip the sign (should never get here though)
										IF g_MissionStatTrackingArray[i].ivalue <0
											CASSERTLN(debug_mission_stats,"Tracked entity damage very big, reached negative values: ",g_MissionStatTrackingArray[i].ivalue)
											g_MissionStatTrackingArray[i].ivalue *= -1
										ENDIF
									ENDIF
									
									g_MissionStatTrackingArray[i].iTrackingDelta = iHealthThisTick
								ELSE
									CDEBUG3LN(DEBUG_MISSION_STATS, "track damage entity [", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target), "] - TrackingEntity is DEAD")
								ENDIF
							ELSE
								CDEBUG3LN(DEBUG_MISSION_STATS, "track damage entity [", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target), "] - TrackingEntity doesn't exist")
								
								g_MissionStatTrackingArray[i].TrackingEntity = NULL
							ENDIF
						ENDIF
					ELSE
						CDEBUG3LN(DEBUG_MISSION_STATS, "	track damage entity [", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target), "] - TrackingEntity is NULL")
					ENDIF
					
					/*
					//Change this 
					//if the watch entity has taken damage
					IF iHealthDelta < 0
						//for damage health delta will be negative
						IF g_MissionStatSingleDamageWatchStat = UNSET_MISSION_STAT_ENUM//general mode
							g_MissionStatTrackingArray[i].ivalue +=  -iHealthDelta
						ELIF g_MissionStatSingleDamageWatchStat = g_MissionStatTrackingArray[i].target
							//specific mode
							g_MissionStatTrackingArray[i].ivalue +=  -iHealthDelta										
						ENDIF
						CDEBUG3LN(DEBUG_MISSION_STATS, "Damage tracking delta ", g_MissionStatTrackingArray[i].ivalue, " had ", iHealthDelta, "added" )
					ENDIF
					*/	
					BREAK
			
			
			
			
				CASE MISSION_STAT_TYPE_TOTALTIME
					//update timer value if needed
					g_MissionStatTrackingArray[i].ivalue += iTimeDeltaThisUpdate
					
					/*
					CPRINTLN(DEBUG_MISSION_STATS, "Mission stat time track tick, total: ")
					PRINTINT(g_MissionStatTrackingArray[i].ivalue)
					PRINTNL()
					CPRINTLN(DEBUG_MISSION_STATS, "Delta: ")
					PRINTINT(iTimeDeltaThisUpdate)
					PRINTNL()
					*/
					BREAK
					
					
				CASE MISSION_STAT_TYPE_ACTION_CAM_USE
					//update time value if action cam is active and being watched
					IF g_bMissionStatSystemActionCamWatchStatus
						g_MissionStatTrackingArray[i].ivalue += iTimeDeltaThisUpdate
					ENDIF
					BREAK
				CASE MISSION_STAT_TYPE_UNIQUE_BOOL
					//no tracking needed for this type, mission pushed
					BREAK
					
				CASE MISSION_STAT_TYPE_WINDOWED_TIMER
					
					IF g_bMissionStatTimeWindowGate
						IF g_eCurrentTimeWindowTarget = UNSET_MISSION_STAT_ENUM
						OR g_MissionStatTrackingArray[i].target = g_eCurrentTimeWindowTarget
							IF g_MissionStatTrackingArray[i].target = FRA0_DOGCAM
							OR g_MissionStatTrackingArray[i].target = EXL2_TIME_AS_CHOP
							OR g_MissionStatTrackingArray[i].target = FRA0_DOGGY_STYLE
							OR g_MissionStatTrackingArray[i].target = FAM4_LAZLOW_CAM_USE
								g_MissionStatTrackingArray[i].ivalue += ROUND(GET_FRAME_TIME() * 1000.0)
							ELSE
								g_MissionStatTrackingArray[i].ivalue += iTimeDeltaThisUpdate
							ENDIF
						ENDIF
					ENDIF
					BREAK
					
				CASE MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
					IF bDoSpeed
					IF (g_MissionStatSpeedWatchEntitySpecific = UNSET_MISSION_STAT_ENUM)
					OR (g_MissionStatSpeedWatchEntitySpecific = g_MissionStatTrackingArray[i].target)
						IF NOT (g_MissionStatSingleSpeedWatchEntity = NULL)
						IF DOES_ENTITY_EXIST(g_MissionStatSingleSpeedWatchEntity)
							IF NOT (IS_ENTITY_DEAD(g_MissionStatSingleSpeedWatchEntity))
								IF (IS_ENTITY_A_VEHICLE(g_MissionStatSingleSpeedWatchEntity))	
									fSpeed = GET_ENTITY_SPEED(g_MissionStatSingleSpeedWatchEntity)
									IF fSpeed > g_MissionStatTrackingArray[i].fvalue
										g_MissionStatTrackingArray[i].fvalue = fSpeed
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						ENDIF
					ENDIF
					ENDIF
					BREAK
				

				CASE MISSION_STAT_TYPE_HEADSHOTS
					//parse for headshots since last time
					//this is now done in UPDATE_DEATH_TRIGGERS
					//PARSE_LISTS_FOR_HEADSHOTS(i)
					
					BREAK 
				CASE MISSION_STAT_TYPE_FRACTION
					
					//no tracking for this type, mission uploaded
					
					BREAK
					
				CASE MISSION_STAT_TYPE_ACCURACY
					//Track accuracy percentage //do nothing since this is tallied at the start and the end now
					
					//UPDATE_ACCURACY_METRIC()
					
					//g_MissionStatTrackingArray[i].fvalue = AccuracyTracker
					
					BREAK 
				CASE MISSION_STAT_TYPE_FINANCE_TABLE
				
					//this is now done in UPDATE_DEATH_TRIGGERS
					//g_MissionStatTrackingArray[i].fvalue += UPDATE_FINANCE_TABLE_METRIC()
					BREAK
				CASE MISSION_STAT_TYPE_FINANCE_DIRECT
					//no tracking for this type, mission uploaded
					BREAK
				CASE MISSION_STAT_TYPE_PURE_COUNT
				CASE MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
				CASE MISSION_STAT_TYPE_PURE_COUNT_DISTANCE
					//no tracking for this type, mission uploaded
					BREAK

				CASE MISSION_STAT_TYPE_INNOCENTS_KILLED
					//this is now done in UPDATE_DEATH_TRIGGERS
					//g_MissionStatTrackingArray[i].ivalue += GET_INNOCENTS_KILLED_SINCE_LAST_CHECK()
				
					BREAK
							
					
				CASE MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
					IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
						IF IS_SPECIAL_ABILITY_ACTIVE(GET_PLAYER_INDEX()) 
						AND IS_SPECIAL_ABILITY_ENABLED(GET_PLAYER_INDEX())
							g_MissionStatTrackingArray[i].ivalue += iTimeDeltaThisUpdate
						ENDIF
					ENDIF
					BREAK
					
				CASE MISSION_STAT_TYPE_BULLETS_FIRED
					//tracked at start and end
					BREAK
				DEFAULT
					SCRIPT_ASSERT("mission_stat_watcher->UPDATE_TRACKER attempting to update an unrecognised type")
					BREAK
			ENDSWITCH
			
		ENDIF
		
	ENDREPEAT
	
ENDPROC


/// RETURNS:
///    TRUE if stat is hidden, and false otherwise
FUNC BOOL UPLOAD_DATA_FOR_TRACKED_STAT(END_SCREEN_DATASET &esdIN, INT index)

	
	FLOAT fpassed = 0
	FLOAT successPercentage = 0
	

	IF ENUM_TO_INT(g_MissionStatTrackingArray[index].target) >= 0
		
	ELSE
		SCRIPT_ASSERT("UPLOAD_DATA_FOR_TRACKED_STAT - ENUM_TO_INT(g_MissionStatTrackingArray[index].target) < 0")
		RETURN FALSE
	ENDIF
	
	MissionStatInfo sMissionStatTrackingPrototype

	
//	#IF USE_CLF_DLC
//	sMissionStatTrackingPrototype = g_DLCMissionStatTrackingPrototypes[g_MissionStatTrackingArray[index].target]
//	#ENDIF
//	#IF USE_NRM_DLC
//	sMissionStatTrackingPrototype = g_DLCMissionStatTrackingPrototypes[g_MissionStatTrackingArray[index].target]
//	#ENDIF
//		
//	#IF NOT USE_CLF_DLC
//	#if not USE_NRM_DLC
	sMissionStatTrackingPrototype = g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[index].target]
//	#ENDIF
//	#endif
	
	IF sMissionStatTrackingPrototype.type = MISSION_STAT_TYPE_BULLETS_FIRED
		g_MissionStatTrackingArray[index].ivalue = overallBulletsMetric
		CPRINTLN(DEBUG_MISSION_STATS, "UPLOAD_DATA_FOR_TRACKED_STAT: bullet total upload: ", overallBulletsMetric)
	ENDIF
	
	//if this is an accuracy stat then override with the accuracy value
	IF sMissionStatTrackingPrototype.type = MISSION_STAT_TYPE_ACCURACY
		g_MissionStatTrackingArray[index].fvalue = overallAccuracyMetric
		CPRINTLN(DEBUG_MISSION_STATS, "UPLOAD_DATA_FOR_TRACKED_STAT: Overall accuracy upload: ", overallAccuracyMetric)
	ENDIF

	IF sMissionStatTrackingPrototype.type = MISSION_STAT_TYPE_PURE_COUNT_DISTANCE
		g_MissionStatTrackingArray[index].ivalue = FLOOR(g_MissionStatTrackingArray[index].fvalue)
	ENDIF


	//figure out if this stat passed 
	//++attempted

	
//	#IF USE_CLF_DLC
//	IF GET_DLC_MISSION_STAT_PASS_STATUS_FOR_VALUE(INT_TO_ENUM(DLC_ASSASSINATION_STATS, ENUM_TO_INT(g_MissionStatTrackingArray[index].target)), g_MissionStatTrackingArray[index].ivalue) 
//		fpassed = 1
//	ENDIF
//	#ENDIF
//	#IF USE_NRM_DLC
//	IF GET_DLC_MISSION_STAT_PASS_STATUS_FOR_VALUE(INT_TO_ENUM(DLC_ASSASSINATION_STATS, ENUM_TO_INT(g_MissionStatTrackingArray[index].target)), g_MissionStatTrackingArray[index].ivalue) 
//		fpassed = 1
//	ENDIF
//	#ENDIF
	
//	#IF NOT USE_CLF_DLC
//	#if not USE_NRM_DLC
	IF GET_MISSION_STAT_PASS_STATUS_FOR_VALUE(g_MissionStatTrackingArray[index].target, g_MissionStatTrackingArray[index].ivalue) 
		fpassed = 1
	ENDIF
//	#ENDIF
//	#endif
	
	#IF IS_DEBUG_BUILD
	IF fpassed = 1.0
		IF sMissionStatTrackingPrototype.less_than_threshold
			CPRINTLN(DEBUG_MISSION_STATS, "Stat register index ", index, "UPLOAD_DATA_FOR_TRACKED_STAT:  passed: ", g_MissionStatTrackingArray[index].ivalue, "/", sMissionStatTrackingPrototype.success_threshold, " Inverted")
		ELSE
			CPRINTLN(DEBUG_MISSION_STATS, "Stat register index ", index, "UPLOAD_DATA_FOR_TRACKED_STAT:  passed: ", g_MissionStatTrackingArray[index].ivalue, "/", sMissionStatTrackingPrototype.success_threshold, " NOT inverted")
		ENDIF
	ELSE
		IF sMissionStatTrackingPrototype.less_than_threshold
			CPRINTLN(DEBUG_MISSION_STATS, "Stat register index ", index, "UPLOAD_DATA_FOR_TRACKED_STAT:  failed: ", g_MissionStatTrackingArray[index].ivalue, "/", sMissionStatTrackingPrototype.success_threshold, " Inverted")
		ELSE
			CPRINTLN(DEBUG_MISSION_STATS, "Stat register index ", index, "UPLOAD_DATA_FOR_TRACKED_STAT:  failed: ", g_MissionStatTrackingArray[index].ivalue, "/", sMissionStatTrackingPrototype.success_threshold, " NOT inverted")
		ENDIF
		
	ENDIF
	#ENDIF
	
	//CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Index  ",index," invalidated state : ", g_MissionStatTrackingArray[index].invalidated)
	//BOOL bInvState = g_MissionStatTrackingArray[index].invalidated
	IF g_MissionStatTrackingArray[index].invalidationReason != MSSIR_VALID
		fpassed = 0
	ENDIF
	
	//work out success percentage
	IF NOT (sMissionStatTrackingPrototype.success_threshold = 0)
		successPercentage = TO_FLOAT(FLOOR((TO_FLOAT(g_MissionStatTrackingArray[index].ivalue)/TO_FLOAT(sMissionStatTrackingPrototype.success_threshold))*100.0))
		
		IF successPercentage > 100.0
			successPercentage = 100.0
		ENDIF
	ENDIF

	CPRINTLN(DEBUG_MISSION_STATS, "Stat success percentage: ", successPercentage)
	
	//if the stat is better than the one stored in the stats then upload the new value!
	INT oldvalue = GET_MISSION_STAT_STORED_VALUE(sMissionStatTrackingPrototype.statname)
	
	BOOL bDoAnyway = FALSE
	SWITCH g_MissionStatTrackingArray[index].target
		CASE ASS1_MIRROR_MEDAL								
		CASE ASS2_MIRROR_MEDAL										
		CASE ASS4_MIRROR_MEDAL
		CASE ASS3_MIRROR_MEDAL									    
		CASE ASS5_MIRROR_MEDAL	
			bDoAnyway = TRUE
			BREAK
		CASE ASS4_MIRROR_PERCENTAGE		    		    		    
		CASE ASS5_MIRROR_PERCENT	
		CASE ASS3_MIRROR_PERCENTAGE	
		CASE ASS1_MIRROR_PERCENT
		CASE ASS2_MIRROR_PERCENT
			bDoAnyway = TRUE
			BREAK
	ENDSWITCH
		
	
	
	//CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> checking invalidation status")
	IF g_MissionStatTrackingArray[index].invalidationReason = MSSIR_VALID OR bDoAnyway
		//CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> checking threshold")
		IF(sMissionStatTrackingPrototype.less_than_threshold)
			//CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> lessthan check ",oldvalue," > ",g_MissionStatTrackingArray[index].ivalue)
			//should we log the stat? is it a record, or has it never been set?
			IF oldvalue > g_MissionStatTrackingArray[index].ivalue OR oldvalue < 0 OR oldvalue = -1
				//CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> lessthan record, setting")
				SET_MISSION_STAT_STORED_VALUE(g_MissionStatTrackingArray[index].ivalue, g_MissionStatTrackingArray[index].target)
			ENDIF
		ELSE
			//CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> greaterthan check ",g_MissionStatTrackingArray[index].ivalue," > ",oldvalue)
			IF g_MissionStatTrackingArray[index].ivalue > oldvalue OR oldvalue < 0 OR oldvalue = -1
				//CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> greaterthan record, setting")
				SET_MISSION_STAT_STORED_VALUE(g_MissionStatTrackingArray[index].ivalue, g_MissionStatTrackingArray[index].target)
			ENDIF
		ENDIF
	ENDIF
	//CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> set phase complete")
	
	INT hsupload = g_MissionStatTrackingArray[index].ivalue
	

	
	IF sMissionStatTrackingPrototype.bHidden 
	#IF IS_DEBUG_BUILD
	AND (NOT g_DEBUG_show_hidden_stats_on_mission_complete)
	#ENDIF
		CPRINTLN(DEBUG_MISSION_STATS, "A stat hidden, skipped\n ")
		
		RETURN TRUE
	ENDIF
	
	END_SCREEN_CHECK_MARK_STATUS checkState = ESCM_UNCHECKED
	IF fpassed = 1.0
		++visiblesucceeded
		checkState = ESCM_CHECKED
	ENDIF
	
	STRING name
	
//	#IF USE_CLF_DLC
//	name = GET_DLC_MISSION_STAT_NAME(INT_TO_ENUM(DLC_ASSASSINATION_STATS, ENUM_TO_INT(g_MissionStatTrackingArray[index].target)))
//	#ENDIF
//	#IF USE_NRM_DLC
////	name = GET_DLC_MISSION_STAT_NAME(INT_TO_ENUM(DLC_ZOMBIE_STATS, ENUM_TO_INT(g_MissionStatTrackingArray[index].target)))
//	#ENDIF
//	
//   	#IF NOT USE_CLF_DLC
//	#if not use_NRM_DLC
	name = GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[index].target)
//	#ENDIF
//	#endif
	
	IF g_MissionStatTrackingArray[index].invalidationReason != MSSIR_VALID
		//Store mission invalidated stat, see 1762462
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			SET_MISSION_STAT_STORED_VALUE(-1, g_MissionStatTrackingArray[index].target)
			CDEBUG1LN(debug_mission_stats,"Non-replay mission stat invalidated, setting stored stat to -1")
		ENDIF
	
		STRING invalidationReason = "MTPHPERRET" 
		SWITCH g_MissionStatTrackingArray[index].invalidationReason
			
			CASE MSSIR_CHEAT_ACTIVE
				invalidationReason = "MTPHPERCHE" 
				BREAK 
			CASE MSSIR_SKIP
				invalidationReason = "MTPHPERSKI" 
				BREAK 
			CASE MSSIR_TAXI_USED
				invalidationReason = "MTPHPERTAX" 
				BREAK 
			CASE MSSIR_NOT_SET
				invalidationReason = "MTPHPERNOREC"
				BREAK
		ENDSWITCH
		IF eLastInvalidationReasonShown = g_MissionStatTrackingArray[index].invalidationReason
			invalidationReason = ""
		ENDIF
		eLastInvalidationReasonShown = g_MissionStatTrackingArray[index].invalidationReason

		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
			   ESEF_RAW_STRING,
			   name,
			   invalidationReason,
			   g_MissionStatTrackingArray[index].ivalue,
			   0,
			   ESCM_INVALIDATED)
	ELSE
		INT iTemp
		eLastInvalidationReasonShown = MSSIR_VALID
		SWITCH sMissionStatTrackingPrototype.type
			CASE MISSION_STAT_TYPE_TOTALTIME
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
				   ESEF_TIME_H_M_S, 
				   name,
				   "",
				   g_MissionStatTrackingArray[index].ivalue,
				   0,
				   checkState)
			BREAK
			
			CASE MISSION_STAT_TYPE_ACTION_CAM_USE
				//this is a time stat
				//IF NOT sMissionStatTrackingPrototype[g_MissionStatTrackingArray[index].invalidated
				//UPLOAD_TIME_STAT_FROM_MS(index,fpassed,g_MissionStatTrackingArray[index].ivalue,ACTION_CAM_STAT_NAMES(g_MissionStatTrackingArray[index].target))//g_MissionStatTrackingArray[index].target))
				//ELSE
				//Upload invalidation entry
				//ENDIF
				
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
					   ESEF_TIME_M_S, 
					   name,
					   "",
					   g_MissionStatTrackingArray[index].ivalue,
					   0,
					   checkState)
			BREAK
				
			CASE MISSION_STAT_TYPE_UNIQUE_BOOL
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
					   ESEF_NAME_ONLY, 
					   name,
					   "",
					   g_MissionStatTrackingArray[index].ivalue,
					   0,
					   checkState)
			BREAK
			
			CASE MISSION_STAT_TYPE_WINDOWED_TIMER
			CASE MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
					   ESEF_TIME_M_S, 
					   name,
					   "",
					   g_MissionStatTrackingArray[index].ivalue,
					   0,
					   checkState)
			BREAK
			
			CASE MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
				//upload as percentage for now
				//g_MissionStatTrackingArray[index].target)
				
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
					   ESEF_RAW_PERCENT, 
					   name,
					   "",
					   CEIL(TO_FLOAT(g_MissionStatTrackingArray[index].ivalue)/TO_FLOAT(sMissionStatTrackingPrototype.success_threshold)*100),
					   0,
					   checkState)
			BREAK
			CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
				//upload as a bool for now
				
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
						   ESEF_NAME_ONLY, 
						   name,
						   "",
						   g_MissionStatTrackingArray[index].ivalue,
						   0,
						   checkState)
			BREAK 
			CASE MISSION_STAT_TYPE_HEADSHOTS
				IF hsupload > sMissionStatTrackingPrototype.success_threshold
					hsupload = sMissionStatTrackingPrototype.success_threshold
				ENDIF
				
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
						   ESEF_FRACTION, 
						   name,
						   "",
						   hsupload,
						   sMissionStatTrackingPrototype.success_threshold,
						   checkState)
			BREAK
			CASE MISSION_STAT_TYPE_FRACTION
				iTemp = g_MissionStatTrackingArray[index].ivalue
				IF iTemp > sMissionStatTrackingPrototype.success_threshold
					iTemp = sMissionStatTrackingPrototype.success_threshold
				ENDIF
				
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
						   ESEF_FRACTION, 
						   name,
						   "",
						   iTemp,
						   sMissionStatTrackingPrototype.success_threshold,
						   checkState)
			BREAK
			CASE MISSION_STAT_TYPE_ACCURACY
				//g_MissionStatTrackingArray[index].target)
				
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
						   ESEF_RAW_PERCENT, 
						   name,
						   "",
						   ROUND(g_MissionStatTrackingArray[index].fvalue),
						   0,
						   checkState)
			BREAK
			CASE MISSION_STAT_TYPE_PURE_COUNT
			CASE MISSION_STAT_TYPE_BULLETS_FIRED
			CASE MISSION_STAT_TYPE_INNOCENTS_KILLED
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
						   ESEF_RAW_INTEGER, 
						   name,
						   "",
						   g_MissionStatTrackingArray[index].ivalue,
						   0,
						   checkState)
			BREAK
				
			CASE MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE 
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
						   ESEF_RAW_PERCENT, 
						   name,
						   "",
						   g_MissionStatTrackingArray[index].ivalue,
						   0,
						   checkState)
			BREAK
			
			CASE MISSION_STAT_TYPE_PURE_COUNT_DISTANCE 
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
						   ESEF_DISTANCE_VALUE_METERS, 
						   name,
						   "",
						   g_MissionStatTrackingArray[index].ivalue,
						   0,
						   checkState)
			BREAK
				
			CASE MISSION_STAT_TYPE_FINANCE_DIRECT
			CASE MISSION_STAT_TYPE_FINANCE_TABLE
					
				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdIN,
						   ESEF_DOLLAR_VALUE, 
						   name,
						   "",
						   g_MissionStatTrackingArray[index].ivalue,
						   0,
						   checkState)
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("mission_stat_watcher->UPLOAD_DATA_FOR_TRACKED_STAT attempting to update an unrecognised type")
			BREAK
		ENDSWITCH
	ENDIF
	RETURN FALSE//not hidden
ENDFUNC






PROC UPLOAD_DATA_TO_SCALEFORM(END_SCREEN_DATASET &esdIN)   

	IF g_bMissionNoStatsNeedsSplashAndSting
	
		g_bMissionStatSystemUploadPending = FALSE
		
		EXIT
	ENDIF


	visiblesucceeded = 0
	visibleattempted = 0
	
	INT i = 0
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Repeat play is active, logging results, CLEAR_REPLAY_STATS + BEGIN_REPLAY_STATS : indices ", g_iMissionStatsBeingTracked)
		CLEAR_REPLAY_STATS()
		g_bMissionStatSystemBuildingReplayStats = TRUE
		BEGIN_REPLAY_STATS(33, 33) //denotes that it is the stat system lodging values
#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> IS_REPEAT_PLAY_ACTIVE returned false, not logging for this mission")
#ENDIF
	ENDIF
	
	INT OverridePerc = -1
	
	eLastInvalidationReasonShown = MSSIR_VALID
	BOOL bSkipDetected = FALSE
	REPEAT g_iMissionStatsBeingTracked i	
	
		SWITCH g_MissionStatTrackingArray[i].target
			CASE FINB_KILLMIC // can't complete these without passing them
			CASE FINA_KILLTREV
				 g_MissionStatTrackingArray[i].ivalue = 1
				BREAK
		ENDSWITCH
	
	
	
		IF UPLOAD_DATA_FOR_TRACKED_STAT(esdIN,i)//,hshift)//increments visiblesucceeded if pass and visible
												//yeah, probably not the best plan
			//++hshift
		ENDIF
			
		IF ENUM_TO_INT(g_MissionStatTrackingArray[i].target) >= 0
//#IF NOT USE_CLF_DLC
			IF !g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].bHidden
//#ENDIF
//#IF USE_CLF_DLC
//			IF !g_DLCMissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].bHidden
//#ENDIF
				++visibleattempted
				
				IF g_MissionStatTrackingArray[i].invalidationReason = MSSIR_SKIP
					bSkipDetected = TRUE
				ENDIF
			ENDIF
		ENDIF

		//IF g_MissionStatTrackingArray[i].invalidated
			//CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Index ",i," invalidated")
			//reason = g_MissionStatTrackingArray[i].invalidationReason 
			//purerun = FALSE
		//ENDIF
		SWITCH g_MissionStatTrackingArray[i].target
			CASE ASS4_MIRROR_PERCENTAGE		    		    		    
			CASE ASS5_MIRROR_PERCENT	
			CASE ASS3_MIRROR_PERCENTAGE	
			CASE ASS1_MIRROR_PERCENT
			CASE ASS2_MIRROR_PERCENT
				OverridePerc = g_MissionStatTrackingArray[i].ivalue
				BREAK
		ENDSWITCH
		
		
	ENDREPEAT	
	
	
	g_LeaderboardLastObjectivesPassed = visiblesucceeded
	
	FLOAT fperc = SET_SCALEFORM_DATA_FOR_MISSION_STAT_TOTALS_ESD(esdIN,visiblesucceeded,visibleattempted,bSkipDetected)

	
	
	
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Repeat play is active, logged results END_REPLAY_STATS.")
					
		ADD_REPLAY_STAT_VALUE(g_LeaderboardLastMissionID)
		ADD_REPLAY_STAT_VALUE(ROUND(fperc))
		IF bSkipDetected
			ADD_REPLAY_STAT_VALUE(1)
		ELSE
			ADD_REPLAY_STAT_VALUE(0)
		ENDIF
		IF g_LeaderboardLastMissionRCType
			ADD_REPLAY_STAT_VALUE(1)
		ELSE
			ADD_REPLAY_STAT_VALUE(0)
		ENDIF
		END_REPLAY_STATS()
	
		g_bMissionStatSystemBuildingReplayStats = FALSE
	ENDIF
	
	

	g_bMissionStatSystemUploadPending = FALSE
	
	

	
	IF OverridePerc > -1
		fperc = TO_FLOAT(OverridePerc)
	ENDIF
	
	
	//lodge fprec into mission data
	//tally golds
	LODGE_PERCENTAGE_FOR_CURRENT_MISSION(fperc, bSkipDetected)

ENDPROC




BOOL bStartedTrigger = FALSE
BOOL bWhatWasTheTriggerLastCheck = FALSE
BOOL StatSystemWasPrimed = FALSE
BOOL bMovieWasLoaded = FALSE
//BOOL bFirstMissionPassedHelpDisplayed = FALSE	
	
PROC RESET_SYSTEM()
	//prevention for 1537258
	g_bResultScreenDisplaying = FALSE
	g_bResultScreenPrepared = FALSE
	g_bMissionStatSystemBlocker = FALSE
	//IF g_bMissionStatSystemResponseToReplayNeeded
	//	CPRINTLN(DEBUG_MISSION_STATS, "RESET_SYSTEM:<STAT WATCHER> preventing full reset due to replay.")
	//	EXIT
	//ENDIF
		
	CPRINTLN(DEBUG_MISSION_STATS, "RESET_SYSTEM: <STAT WATCHER> Resetting system.")
	SETTIMERA(0)
	SETTIMERB(0)
	THEFEED_RESUME()
	//iTimeLastUpdate = 0
	//iTimeThisUpdate = 0
	iTimeDeltaThisUpdate = 0
	g_bMissionStatSystemMissionStarted = FALSE
	g_eCurrentTimeWindowTarget = UNSET_MISSION_STAT_ENUM
	bStartedTrigger = FALSE
	g_bMissionOverStatTrigger = FALSE
	g_bMissionStatSystemPrimed = FALSE
	StatSystemWasPrimed = FALSE
	g_MissionStatSingleSpeedWatchEntity = NULL
	g_bMissionStatTimeWindowClosedForGood = FALSE
	g_bMissionNoStatsNeedsSplashAndSting = FALSE
	
	
	g_StatSystemWeponpoolEntries = 0
	g_StatSystemDeadpoolEntries = 0
	
	
	RESET_STAT_SYSTEM_DEADPOOL()
	RESET_STAT_SYSTEM_WEPPOOL()
	

	IF bMovieWasLoaded

		ENDSCREEN_SHUTDOWN(esd)
		bMovieWasLoaded =  FALSE
	ENDIF
	
	IF g_bMagDemoActive
		CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Terminating for magdemo.")
		TERMINATE_THIS_THREAD()
	ENDIF

	g_bMissionStatSystemUploadPending = FALSE
	g_bMissionStatSystemResetFlag = FALSE
ENDPROC

FUNC BOOL GET_UNHIDDEN_IN_REGISTERS()
	INT i = 0
	REPEAT g_iMissionStatsBeingTracked i	
		IF ENUM_TO_INT(g_MissionStatTrackingArray[i].target) > 0
//#IF NOT USE_CLF_DLC
			IF g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].bHidden = FALSE
//#ENDIF
//#IF USE_CLF_DLC
//			IF g_DLCMissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].bHidden = FALSE
//#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
	INT vehspeed
#ENDIF

SCRIPT 

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) > 1
		CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Detected multiples of itself, bailing")
		TERMINATE_THIS_THREAD()
	ENDIF
	g_bMissionOverStatTrigger = FALSE // prevent immediate trigger from 1581885
	g_bMissionStatSystemResetFlag = FALSE //prevent false positives shutdowns 1581499
	INT iModeTimerMultiplier = 1
	
//	#IF USE_CLF_DLC
//		bUSE_ASSASSIN_OR_CLF_DLC = TRUE
//	#ENDIF

	IF HAS_FORCE_CLEANUP_OCCURRED( FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_DEBUG_MENU)// | FORCE_CLEANUP_FLAG_RANDOM_EVENTS |  FORCE_CLEANUP_FLAG_MAGDEMO)	//FORCE_CLEANUP_FLAG_PLAYER_KILLED_OR_ARRESTED
		RESET_SYSTEM()	
		g_iMissionStatsBeingTracked = 0
		g_bStatsInWatchingLoop = FALSE
		g_bSuppressNextStatTrigger = FALSE
		CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Force cleanup terminated")
		TERMINATE_THIS_THREAD()
	ENDIF

	#IF IS_DEBUG_BUILD
	
		IF NOT DOES_WIDGET_GROUP_EXIST(g_missionstatwidgetgroup)
			//create the debug stat registers
			g_missionstatwidgetgroup = START_WIDGET_GROUP("Mission Stat Tracking Registers")
			
			ADD_WIDGET_INT_READ_ONLY("Current vehicle speed",vehspeed)
			
			ADD_WIDGET_INT_READ_ONLY("Total used", g_iMissionStatsBeingTracked)
			
			ADD_WIDGET_BOOL("Triggering bool, fire if primed", g_bMissionOverStatTrigger)
			
			START_WIDGET_GROUP("g_MissionStatTrackingArray[]")
				INT idbg = 0
				REPEAT MAX_TRACKED_MISSION_STATS idbg
					TEXT_LABEL dlbl = "Register_"
					IF (idbg < 10)		dlbl += "0" ENDIF
					dlbl += idbg
					
					START_WIDGET_GROUP(dlbl)
						//ADD_WIDGET_INT_READ_ONLY("Type ENUM", ENUM_TO_INT(g_MissionStatTrackingArray[idbg].target))
					
						//IF NOT bUSE_ASSASSIN_OR_CLF_DLC
							g_MissionStatTrackingArray[idbg].target_textWidget = ADD_TEXT_WIDGET("target")
						//ELSE
							//g_MissionStatTrackingArray[idbg].target_textWidget = ADD_TEXT_WIDGET("DLC target")
						//ENDIF
						
						ADD_WIDGET_INT_READ_ONLY("iValue", g_MissionStatTrackingArray[idbg].ivalue)
						ADD_WIDGET_FLOAT_READ_ONLY("fValue",g_MissionStatTrackingArray[idbg].fvalue)
						
						START_NEW_WIDGET_COMBO()
							ADD_TO_WIDGET_COMBO("MSSIR_VALID")
							ADD_TO_WIDGET_COMBO("MSSIR_CHECKPOINT")
							ADD_TO_WIDGET_COMBO("MSSIR_SKIP")
							ADD_TO_WIDGET_COMBO("MSSIR_TAXI_USED")
							ADD_TO_WIDGET_COMBO("MSSIR_CHEAT_ACTIVE")
							ADD_TO_WIDGET_COMBO("MSSIR_NOT_SET")
						STOP_WIDGET_COMBO("invalidationReason", g_MissionStatTrackingArray[idbg].invalidationReason_int)
						
						ADD_WIDGET_BOOL("successshown",g_MissionStatTrackingArray[idbg].successshown)
	//	
						ADD_WIDGET_BOOL("windowDeltaTracking",g_MissionStatTrackingArray[idbg].windowDeltaTracking)
	//	
	//	//New tracking entity
	//	ENTITY_INDEX TrackingEntity
						ADD_WIDGET_BOOL("bTrackingEntityChanged",g_MissionStatTrackingArray[idbg].bTrackingEntityChanged)
						ADD_WIDGET_INT_READ_ONLY("iTrackingDelta",g_MissionStatTrackingArray[idbg].iTrackingDelta)
						
	//					IF bUSE_ASSASSIN_OR_CLF_DLC
	//						IF idbg < DLC_ASSASSINATION_MAX_STATS
	//							SWITCH g_dlcAssasinationStatConfig.type[idbg]
	//								CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
	//									ADD_WIDGET_STRING("Single Entity Damage Threshold")
	//									ADD_WIDGET_INT_SLIDER("iHealthThisTick", iHealthThisTick, -1, 2000, 1)
	//									ADD_WIDGET_INT_SLIDER("iHealthLastTick", iHealthLastTick, -1, 2000, 1)
	//									ADD_WIDGET_INT_SLIDER("iHealthDelta", iHealthDelta, -1, 2000, 1)
	//								BREAK
	//								CASE MISSION_STAT_TYPE_ACCURACY
	//									ADD_WIDGET_STRING("Accuracy")
	//									ADD_WIDGET_FLOAT_SLIDER("overallAccuracyMetric", overallAccuracyMetric, -1, 2000, 1)
	//									ADD_WIDGET_INT_SLIDER("overallBulletsMetric", overallBulletsMetric, -1, 2000, 1)
	//								BREAK
	//							ENDSWITCH
	//						ENDIF
	//					ENDIF
						
					STOP_WIDGET_GROUP()
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
	//		IF bUSE_ASSASSIN_OR_CLF_DLC
	//			g_dlcAssasinationStatConfig.d_widget = START_WIDGET_GROUP("PREP_DLC_ASSASSINATION_STAT_CONFIG")
	//				ADD_WIDGET_INT_READ_ONLY("dlcAssassinationIndex", g_dlcAssasinationStatConfig.dlcAssassinationIndex)
	//				
	//				INT iStat
	//				REPEAT g_dlcAssasinationStatConfig.iRegisteredStats iStat
	//					ADD_WIDGET_INT_READ_ONLY(GET_DLC_MISSION_STAT_DEBUG_NAME(g_dlcAssasinationStatConfig.target[iStat]) , g_dlcAssasinationStatConfig.StatCurrentValue[iStat]) 
	//				ENDREPEAT
	//				
	//				ADD_WIDGET_FLOAT_READ_ONLY("fCompletionPercent", g_dlcAssasinationStatConfig.fCompletionPercent)
	//			STOP_WIDGET_GROUP()
	//		ENDIF
			
			ADD_WIDGET_BOOL("bDrawWatchlist", bDrawWatchlist)
			ADD_WIDGET_FLOAT_SLIDER("END_SCREEN_CURSIVE_TITLE_SCALE", END_SCREEN_CURSIVE_TITLE_SCALE, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("END_SCREEN_CURSIVE_TITLE_Y_OFFSET", END_SCREEN_CURSIVE_TITLE_Y_OFFSET, -1.0, 1.0, 0.0001)
			
			ADD_WIDGET_FLOAT_SLIDER("END_SCREEN_CURSIVE_COMP_SCALE", END_SCREEN_CURSIVE_COMP_SCALE, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("END_SCREEN_CURSIVE_COMP_X_OFFSET", END_SCREEN_CURSIVE_COMP_X_OFFSET, -1.0, 1.0, 0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("END_SCREEN_MEDAL_SCALE", END_SCREEN_MEDAL_SCALE, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("END_SCREEN_MEDAL_Y_OFFSET", END_SCREEN_MEDAL_Y_OFFSET, -20.0, 20.0, 1.0)
			
			ADD_WIDGET_BOOL("g_bEnd_Screen_Display_Finish", g_bEnd_Screen_Display_Finish)
			
			START_WIDGET_GROUP("SHARD")
				ADD_WIDGET_BOOL("bTestShardMessage", bTestShardMessage)
				
				START_NEW_WIDGET_COMBO()
					INT iRepeat
					REPEAT HUD_COLOUR_NET_PLAYER1 iRepeat
						ADD_TO_WIDGET_COMBO(HUD_COLOUR_AS_STRING(INT_TO_ENUM(HUD_COLOURS, iRepeat)))
					ENDREPEAT
				STOP_WIDGET_COMBO("iEndFlash", iTWEAK_endFlash)
			STOP_WIDGET_GROUP()
			
			STOP_WIDGET_GROUP()
		ENDIF
	#ENDIF
	


	
	BOOL bUnhiddenInRegisters = FALSE
	
	WAIT(0)
	
	CHECK_REPLAY_MISSION_STAT_STATE()
	
	

	
	
	WHILE !g_bMissionStatSystemPrimed
		CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Waiting for stat system to be primed.")
		WAIT(200)
		IF g_bMissionStatSystemResetFlag
			CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Terminating out of waiting for primed due to reset flag")
			RESET_SYSTEM()
			TERMINATE_THIS_THREAD()
		ENDIF
	ENDWHILE
	
	CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Primed for :  ", g_sMissionStatsName)
	
	CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Entering watching loop") 
	g_bMissionStatTimeWindowGate = FALSE//windowed timers can't be open while starting watching
	

	g_bStatsInWatchingLoop = TRUE
	WHILE !g_bMissionOverStatTrigger 
		//do on mission tracking.
		#IF IS_DEBUG_BUILD		//widget speed	
		IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
			PED_INDEX pii = GET_PLAYER_PED(GET_PLAYER_INDEX())
			IF NOT IS_ENTITY_DEAD(pii)
			IF IS_PED_IN_ANY_VEHICLE(pii)
				VEHICLE_INDEX vii = GET_VEHICLE_PED_IS_USING(pii)
				vehspeed = FLOOR(GET_ENTITY_SPEED(vii))
			ELSE
				vehspeed = 0
			ENDIF
			ENDIF
		ENDIF
		IF GET_GAME_TIMER()%1000 = 0
			CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Still in watching loop")
		ENDIF
		
		int idbg = 0
		REPEAT MAX_TRACKED_MISSION_STATS idbg
			TEXT_LABEL_63 NewWidgetContents
//			IF NOT bUSE_ASSASSIN_OR_AGENT_T_DLC
				IF g_MissionStatTrackingArray[idbg].target = UNSET_MISSION_STAT_ENUM 
					NewWidgetContents = "none"
#IF USE_CLF_DLC
				ELIF g_MissionStatTrackingArray[idbg].target >= MAX_MISSION_STATS_AGENT
#ENDIF
#IF NOT USE_CLF_DLC
				ELIF g_MissionStatTrackingArray[idbg].target >= MAX_MISSION_STATS
#ENDIF
					NewWidgetContents  = "unknown stat "
					NewWidgetContents += ENUM_TO_INT(g_MissionStatTrackingArray[idbg].target)
//				ELSE
//					NewWidgetContents = GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[idbg].target)
				ENDIF
//			ELSE
//				DLC_ASSASSINATION_STATS target = INT_TO_ENUM(DLC_ASSASSINATION_STATS, ENUM_TO_INT(g_MissionStatTrackingArray[idbg].target))
//				IF target <= UNSET_DLC_MISSION_STAT_ENUM
//					NewWidgetContents  = "* unset"
//				ELIF target < MAX_DLC_MISSION_STATS
//					NewWidgetContents  = "* "
//					NewWidgetContents += GET_STRING_FROM_TEXT_FILE(GET_DLC_MISSION_STAT_NAME(target))
//				ELSE
//					NewWidgetContents  = "* unknown stat "
//					NewWidgetContents += ENUM_TO_INT(target)
//					
//				ENDIF
//			ENDIF
			
			SET_CONTENTS_OF_TEXT_WIDGET(g_MissionStatTrackingArray[idbg].target_textWidget, NewWidgetContents)
			
			g_MissionStatTrackingArray[idbg].invalidationReason_int = ENUM_TO_INT(g_MissionStatTrackingArray[idbg].invalidationReason)
		ENDREPEAT
		


		IF bDrawWatchlist
			IF g_iEntityWatchListLoggedCount > 0
				INT i = 0
				REPEAT MAX_ENTITY_WATCH_LIST_ENTRIES i
					IF DOES_ENTITY_EXIST(g_EntityWatchList[i].index)
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						DRAW_DEBUG_LINE(GET_ENTITY_COORDS(g_EntityWatchList[i].index, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
					ENDIF
				ENDREPEAT
			ELSE
				bDrawWatchlist = FALSE
			ENDIF
		ENDIF
		
		IF bTestShardMessage
			shardBigMessage.siMovie = REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
			buttons = REQUEST_SCALEFORM_MOVIE_INSTANCE("INSTRUCTIONAL_BUTTONS")	//DRAW_INSTRUCTIONAL_BUTTONS
			REQUEST_ADDITIONAL_TEXT("MISHSTA", MINIGAME_TEXT_SLOT)

			CONST_INT iCONST_TEST_0	0
			CONST_INT iCONST_TEST_1	1
			CONST_INT iCONST_TEST_2	2
			CONST_INT iCONST_TEST_3	3
			CONST_INT iCONST_TEST_4	4
			CONST_INT iCONST_TEST_5	5
			INT iTestone = iCONST_TEST_0
		
			WHILE bTestShardMessage
				
				SWITCH iTestone
					CASE iCONST_TEST_0
						IF HAS_SCALEFORM_MOVIE_LOADED(shardBigMessage.siMovie)
						AND HAS_SCALEFORM_MOVIE_LOADED(buttons)
						AND HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
						AND ENDSCREEN_PREPARE(esd, FALSE, DEFAULT)
//							SET_SHARD_BIG_MESSAGE(shardBigMessage, "MISHPA", g_sMissionStatsName, -1, SHARD_MESSAGE_CENTERED, INT_TO_ENUM(HUD_COLOURS, iTWEAK_endFlash))
							START_TIMER_NOW(shardBigMessage.movieTimer)
							IF g_iMissionStatsBeingTracked > 0
								UPLOAD_DATA_TO_SCALEFORM(esd) 				// Normal upload	   
							ENDIF
							
						    BEGIN_SCALEFORM_MOVIE_METHOD(buttons, "CLEAR_ALL")
						    END_SCALEFORM_MOVIE_METHOD()
							
							iTestone = iCONST_TEST_1
						ENDIF
					BREAK
					CASE iCONST_TEST_1
//						IF NOT UPDATE_SHARD_BIG_MESSAGE(shardBigMessage, FALSE)
//							RESET_SHARD_BIG_MESSAGE(shardBigMessage)
//							iTestone = iCONST_TEST_3
//						ENDIF
						RENDER_ENDSCREEN(esd, DEFAULT, DEFAULT)
						
						IF (GET_TIMER_IN_SECONDS(shardBigMessage.movieTimer) > 2.0)
//							SET_SHARD_BIG_MESSAGE(shardBigMessage, "MISHPA", g_sMissionStatsName, -1, SHARD_MESSAGE_CENTERED_TOP, INT_TO_ENUM(HUD_COLOURS, iTWEAK_endFlash))
							
							BEGIN_SCALEFORM_MOVIE_METHOD(buttons, "SET_DATA_SLOT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("REPLAY_OK")
							END_SCALEFORM_MOVIE_METHOD()
							BEGIN_SCALEFORM_MOVIE_METHOD(buttons, "SET_DATA_SLOT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("REPLAY_NO")
							END_SCALEFORM_MOVIE_METHOD()
							BEGIN_SCALEFORM_MOVIE_METHOD(buttons, "SET_DATA_SLOT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X))
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("REPLAY_SKIP_S")
							END_SCALEFORM_MOVIE_METHOD()
							
							iTestone = iCONST_TEST_2
						ENDIF
					BREAK
					CASE iCONST_TEST_2
						
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							SET_SHARD_BIG_MESSAGE(shardBigMessage, "MISHPA", g_sMissionStatsName, -1, SHARD_MESSAGE_CENTERED_TOP, INT_TO_ENUM(HUD_COLOURS, iTWEAK_endFlash))
							iTestone = iCONST_TEST_3
						ENDIF
//						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
//							SET_SHARD_BIG_MESSAGE(shardBigMessage, "MISHPA", g_sMissionStatsName, -1, SHARD_MESSAGE_CENTERED_TOP, INT_TO_ENUM(HUD_COLOURS, iTWEAK_endFlash))
//							iTestone = iCONST_TEST_2
//						ENDIF
						
						RENDER_ENDSCREEN(esd, DEFAULT, DEFAULT)
						
						DRAW_SCALEFORM_MOVIE_FULLSCREEN(buttons, 255, 255, 255, 128)
//						IF NOT UPDATE_SHARD_BIG_MESSAGE(shardBigMessage, TRUE)
//							RESET_SHARD_BIG_MESSAGE(shardBigMessage)
//							iTestone = iCONST_TEST_3
//						ENDIF
					BREAK
					
					CASE iCONST_TEST_3
						bTestShardMessage = FALSE
					BREAK
				ENDSWITCH
				
				#IF IS_DEBUG_BUILD
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				VECTOR vTextCoord = <<0.05, 0.35, 0>>, vTextOffset = <<0.00,0.02,0.0>>
				
				
				TEXT_LABEL_63 str = "iTestone: "
				str += iTestone
				DRAW_DEBUG_TEXT_2D(str, vTextCoord+(vTextOffset*0.0),	0,0,255)
				
				str  = "iDuration: "
				str += shardBigMessage.iDuration
				DRAW_DEBUG_TEXT_2D(str, vTextCoord+(vTextOffset*1.0),	0,0,255)
				
				str  = "movieTimer: "
				IF IS_TIMER_STARTED(shardBigMessage.movieTimer)
					str += GET_STRING_FROM_FLOAT(GET_TIMER_IN_SECONDS(shardBigMessage.movieTimer))
				ELSE
					str += "NONE"
				ENDIF
				DRAW_DEBUG_TEXT_2D(str, vTextCoord+(vTextOffset*2.0),	0,0,255)
				
				str  = "animOutTimer: "
				IF IS_TIMER_STARTED(shardBigMessage.animOutTimer)
					str += GET_STRING_FROM_FLOAT(GET_TIMER_IN_SECONDS(shardBigMessage.animOutTimer))
				ELSE
					str += "NONE"
				ENDIF
				DRAW_DEBUG_TEXT_2D(str, vTextCoord+(vTextOffset*3.0),	0,0,255)
				#ENDIF
				
				
				WAIT(0)
			ENDWHILE
		ENDIF
		#ENDIF

		// Mission tracker primed, do tracking
		
		IF g_iMissionStatsBeingTracked > 0
			// Call tracker update
			UPDATE_TRACKER()
			IF g_iMissionStatsStartTime = -1
				g_iMissionStatsStartTime = GET_GAME_TIMER()
			ELIF GET_GAME_TIMER() - g_iMissionStatsStartTime > 1000
				INVALIDATE_STATS_IF_CHEATS_ACTIVE()
			ENDIF
			//g_bMissionStatSystemResetFlag = FALSE
		ENDIF	
		
		IF g_bMissionStatSystemResetFlag
			CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Terminating out of tracking due to reset flag")
			RESET_SYSTEM()
			g_bStatsInWatchingLoop = FALSE
			TERMINATE_THIS_THREAD()
		ENDIF
		

		
		//CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> In watching loop, flag is ", g_bMissionStatSystemResetFlag) 
		WAIT(0)
	ENDWHILE
	g_bStatsInWatchingLoop = FALSE
	
	CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Leaving watching loop") 
	
	
	
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS() 
		CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Switch in progress holding til complete waiting") 
		WHILE IS_PLAYER_PED_SWITCH_IN_PROGRESS() 
			
			#IF IS_DEBUG_BUILD
			IF GET_GAME_TIMER() % 1000 = 0
				CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Switch in progress holding til complete") 
			ENDIF
			#ENDIF
			
			
			
			WAIT(0)
		ENDWHILE
		
	
	ENDIF
	
	BOOL bDone = FALSE
	WHILE (!bDone)

		IF NOT  (g_bMissionStatSystemMissionStarted = bStartedTrigger)
			IF g_bMissionStatSystemMissionStarted
				SETTIMERB(0)
				//iTimeLastUpdate = 0
				//iTimeThisUpdate = 0
				iTimeDeltaThisUpdate = 0
				g_MissionStatSingleSpeedWatchEntity = NULL
			ENDIF
			bStartedTrigger = g_bMissionStatSystemMissionStarted
		ENDIF
		
		
		// If flow wants a delay then wait
		WHILE g_bMissionStatSystemBlocker
			CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Display process blocked by flow. Waiting 1 second.\n") 
			WAIT(1000)
		ENDWHILE
		

		
		IF g_bMissionOverStatTrigger  AND (NOT g_bMissionStatSystemResetFlag)
			// AND (g_bMissionStatSystemPrimed OR StatSystemWasPrimed)
			CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Overlay triggered.")
			//bWatchedAMission = TRUE
			IF (g_bMissionStatSystemPrimed OR StatSystemWasPrimed) 
				g_bMissionStatSystemResponseToShitskipNeeded = FALSE
			//OR g_bMissionStatRampageModeUpload 
			//OR g_bMissionStatBaseJumpModeUpload

				IF bWhatWasTheTriggerLastCheck
					CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> bWhatWasTheTriggerLastCheck = TRUE")
				ELSE
					CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> bWhatWasTheTriggerLastCheck = FALSE")
				ENDIF
				IF g_bMissionOverStatTrigger
					CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> g_bMissionOverStatTrigger = TRUE")
				ELSE
					CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> g_bMissionOverStatTrigger = FALSE")
				ENDIF
				
				IF NOT (bWhatWasTheTriggerLastCheck = g_bMissionOverStatTrigger)
					CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Init triggered.")
					
					RESET_ENDSCREEN(esd)
					esd.bShowSkipperPrompt = TRUE

					TALLY_ACCURACY_METRIC()
					COMMUTE_FLOAT_STAT_REGISTERS_TO_INTS()
					
					INT arrgh = 0
					REPEAT g_iMissionStatsBeingTracked arrgh
						IF ENUM_TO_INT(g_MissionStatTrackingArray[arrgh].target) > 0
							
//#IF NOT USE_CLF_DLC	
							IF g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[arrgh].target].type = MISSION_STAT_TYPE_WINDOWED_TIMER
								IF !g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[arrgh].target].bHidden
									IF g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[arrgh].target].less_than_threshold
//#ENDIF
//#IF USE_CLF_DLC	
//							IF g_DLCMissionStatTrackingPrototypes[g_MissionStatTrackingArray[arrgh].target].type = MISSION_STAT_TYPE_WINDOWED_TIMER
//								IF !g_DLCMissionStatTrackingPrototypes[g_MissionStatTrackingArray[arrgh].target].bHidden
//									IF g_DLCMissionStatTrackingPrototypes[g_MissionStatTrackingArray[arrgh].target].less_than_threshold
//#ENDIF
		
										IF g_MissionStatTrackingArray[arrgh].ivalue = 0
											//unset time windows are invalidated
											//g_MissionStatTrackingArray[arrgh].invalidated = TRUE
											g_MissionStatTrackingArray[arrgh].invalidationReason = MSSIR_NOT_SET
											CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Greater than threshold time window stat ", GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[arrgh].target), "\" auto invalidated, assuming due to checkpoint.\n") 
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT

					
					BOOL bUISuppressed = FALSE
					
					IF (g_bResultScreenDisplaying)//let heist end screen take front
					AND (NOT g_bMissionNoStatsNeedsSplashAndSting)//splash only primes can show anyway
					OR g_MissionStatSystemSuppressVisual
						
						CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> UI suppressed because, flags set to : ", g_bResultScreenDisplaying, ", ", g_bMissionNoStatsNeedsSplashAndSting, ", ", g_MissionStatSystemSuppressVisual, ".")
						
						bUISuppressed = TRUE
					ENDIF
					
					BOOL fadeSetting = FALSE
					 
					IF IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN()
						fadeSetting = TRUE
					ELSE
						IF g_sAutosaveData.bIgnoreScreenFade
							fadeSetting = TRUE
						ENDIF
						IF g_MissionStatUIIgnoreFade
							fadeSetting = TRUE
						ENDIF
					ENDIF
					
					IF IS_REPEAT_PLAY_ACTIVE() // For 1532756
						CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Stat screen about to display over fade due to IS_REPEAT_PLAY_ACTIVE")
						fadeSetting = TRUE
						g_MissionStatUIIgnoreFade = TRUE
					ENDIF
					

									
					
					
					////////////////////////////////////////////////////////////////////////////
					///////////////////////heist and mission esd upload////////////////////////////////////////////////
					////////////////////////////////////////////////////////////////////////////
					SP_MISSIONS sa = INT_TO_ENUM(SP_MISSIONS,g_LeaderboardLastMissionID)
					

					IF g_bMissionStatSystemHeistMode	
					
					
						PRIVATE_Heist_ES_CALCULATE_TOTALS_AND_UPLOAD_TO_SCALEFORM(g_iMissionStatSystemHeistIndex)
					
					
					
						iModeTimerMultiplier =2
						CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> heist mode stat render")
						SET_ENDSCREEN_DATASET_HEADER(esd,
													 "HISHPA",
													 g_sMissionStatsName)
													 
		 
						//1. Show value actually grabbed during the heist.
						IF sa = SP_HEIST_AGENCY_3A 
						OR sa = SP_HEIST_AGENCY_3B 
							//There is no take on the Agency Heist.
							//The FIB provide a budget.
							ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd,
									   ESEF_DOLLAR_VALUE, 
									   "ACTATAKE_AG",
									   "",
									   g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].iPotentialTake,
									   0,
									   ESCM_NO_MARK,
									   FALSE)
						
						ELSE
						
							ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd,
									   ESEF_DOLLAR_VALUE, 
									   "POTATAKE",
									   "",
									   g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].iPotentialTake,
									   0,
									   ESCM_NO_MARK)
									   
						ENDIF  
						
						//2. Show penalties deducted from picked up amount.
						INT i = 0
						REPEAT g_HeistNoneSavedDatas[g_iMissionStatSystemHeistIndex].iPenaltiesLogged i
							IF g_HeistNoneSavedDatas[g_iMissionStatSystemHeistIndex].iPenaltiesValues[i] > 0   	
							
								ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd,
										   ESEF_DOLLAR_VALUE, 
										   PRIVATE_GET_HEIST_PENALTY_STRING(g_HeistNoneSavedDatas[g_iMissionStatSystemHeistIndex].Penalties[i]),
										   "",
										   -1 * g_HeistNoneSavedDatas[g_iMissionStatSystemHeistIndex].iPenaltiesValues[i],
										   0,
										   ESCM_NO_MARK)	
								
							ENDIF
						ENDREPEAT
								   
						//3. Show Michael's cut of what remains.	   
						IF g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].iPlayerTake[CHAR_MICHAEL] > 0
						OR sa = SP_HEIST_DOCKS_2A OR sa = SP_HEIST_DOCKS_2B 
						OR sa = SP_HEIST_AGENCY_3A OR sa = SP_HEIST_AGENCY_3B 
						
							ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd,
									   ESEF_DOLLAR_VALUE, 
									   "ACTATAKE_M",
									   "",
									   g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].iPlayerTake[CHAR_MICHAEL],
									   0,
									   ESCM_NO_MARK)	
						ENDIF
								
						//4. Show Franklin's cut of what remains.	
						IF g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].iPlayerTake[CHAR_FRANKLIN] > 0
						OR sa = SP_HEIST_DOCKS_2A OR sa = SP_HEIST_DOCKS_2B 
						
							ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd,
									   ESEF_DOLLAR_VALUE, 
									   "ACTATAKE_F",
									   "",
									   g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].iPlayerTake[CHAR_FRANKLIN],
									   0,
									   ESCM_NO_MARK)	   		   
							
						ENDIF
							
						//5. Show Trevor's cut of what remains.	
						IF g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].iPlayerTake[CHAR_TREVOR] > 0
						OR sa = SP_HEIST_DOCKS_2A OR sa = SP_HEIST_DOCKS_2B 
						
							ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd,
									   ESEF_DOLLAR_VALUE, 
									   "ACTATAKE_T",
									   "",
									   g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].iPlayerTake[CHAR_TREVOR],
									   0,
									   ESCM_NO_MARK)	   	
						ENDIF
						
						//6. Show Lester's cut of what remains.	
						IF sEndScreen.iLesterTotal > 0
							ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd,
									   ESEF_DOLLAR_VALUE, 
									   "ACTATAKE_L",
									   "",
									   sEndScreen.iLesterTotal,
									   0,
									   ESCM_NO_MARK)	
						
						ENDIF
						
						//Calculate crew cut split. Alive take normal cuts. Dead add funeral costs.
						INT iHeistChoice = GET_MISSION_FLOW_INT_VALUE(Get_Heist_Choice_FlowInt_For_Heist(g_iMissionStatSystemHeistIndex))
						INT iCrewSum = 0
						INT iFuneralSum = 0
						REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize i
							IF NOT IS_HEIST_CREW_MEMBER_DEAD(g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][i])
								iCrewSum += g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].iCrewMemberTake[i]
							ELSE
								iFuneralSum += g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].iCrewMemberTake[i]
							ENDIF
						ENDREPEAT
						
						
						//7. Show alive crew member cuts. 
						IF sa = SP_HEIST_DOCKS_2A OR sa = SP_HEIST_DOCKS_2B 
							ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd,
									   ESEF_DOLLAR_VALUE, 
									   "LACKTAKE",
									   "",
									   iCrewSum,
									   0,
									   ESCM_NO_MARK)	
						ELSE
							ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd,
									   ESEF_DOLLAR_VALUE, 
									   "CREWTAKE",
									   "",
									   iCrewSum,
									   0,
									   ESCM_NO_MARK)	
						ENDIF
						
						
						//8. Show cut spent on funeral costs for dead crew members.
						IF iFuneralSum > 0
						
							//Update funeral bill penalty stats for social club.
							IF NOT IS_REPEAT_PLAY_ACTIVE()
								SWITCH sa
									CASE SP_HEIST_JEWELRY_2 	
										STAT_SET_INT(HCS_PENALTY_JEWEL_FUNERAL, iFuneralSum)	
									BREAK
									CASE SP_HEIST_RURAL_2
										STAT_SET_INT(HCS_PENALTY_PALETO_FUNERAL, iFuneralSum)	
									BREAK
									CASE SP_HEIST_AGENCY_3A
									CASE SP_HEIST_AGENCY_3B
										STAT_SET_INT(HCS_PENALTY_BUREAU_FUNERAL, iFuneralSum)	
									BREAK
									CASE SP_HEIST_FINALE_2A
									CASE SP_HEIST_FINALE_2B
										STAT_SET_INT(HCS_PENALTY_BIGS_FUNERAL, iFuneralSum)
									BREAK
								ENDSWITCH
							ENDIF
						
							ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd,
									   ESEF_DOLLAR_VALUE, 
									   "HSTENDPFUN",
									   "",
									   iFuneralSum,
									   0,
									   ESCM_NO_MARK)
						ENDIF
						
					ELSE
						CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> normal mission mode stat render")
						SET_ENDSCREEN_DATASET_HEADER(esd,
								"MISHPA", g_sMissionStatsName)					 
					ENDIF
					////////////////////////////////////////////////////////////////////////////
					////////////////////////////////////////////////////////////////////////////
					////////////////////////////////////////////////////////////////////////////
					
					IF g_iMissionStatsBeingTracked > 0
						UPLOAD_DATA_TO_SCALEFORM(esd) 				// Normal upload	   
					ENDIF
					
					IF (NOT g_bMissionNoStatsNeedsSplashAndSting)
						CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Doing leaderboard calculation")
						DO_LEADERBOARD_SCORE_CALCULATION_AND_SET()
					#IF IS_DEBUG_BUILD
					ELSE
						CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Skipping leaderboard calculation")
					#ENDIF
					ENDIF
					
					WHILE NOT fadeSetting
						CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Waiting for screen to be faded in or fading in")
						IF IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN()
							fadeSetting = TRUE
						ENDIF
						WAIT(0)
					ENDWHILE
					
					CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> About to load movie, fade setting ", fadeSetting, " and ",g_MissionStatUIIgnoreFade, "  : UI suppression -> ", bUISuppressed)
					IF (fadeSetting 
						OR g_MissionStatUIIgnoreFade)//skip the movie load 
						AND (NOT bUISuppressed)
						IF NOT g_MissionStatSystemSuppressVisual
						ENDSCREEN_PREPARE(esd,FALSE)
						ENDIF 
						BOOL bBlockForUISound = FALSE
						IF NOT g_LeaderboardLastMissionRCType
							SWITCH INT_TO_ENUM(SP_MISSIONS,g_LeaderboardLastMissionID)
								CASE SP_MISSION_ME_AMANDA
								CASE SP_MISSION_ME_JIMMY
								CASE SP_MISSION_ME_TRACEY
									SET_AUDIO_FLAG("HoldMissionCompleteWhenPrepared", TRUE)
									MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
									bBlockForUISound = TRUE
								BREAK
								DEFAULT
									SET_AUDIO_FLAG("HoldMissionCompleteWhenPrepared", TRUE)
									MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(FALSE)
									bBlockForUISound = TRUE
								BREAK
							ENDSWITCH
						ELSE
							SET_AUDIO_FLAG("HoldMissionCompleteWhenPrepared", TRUE)
							MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
							bBlockForUISound = TRUE
						ENDIF
						bUnhiddenInRegisters = GET_UNHIDDEN_IN_REGISTERS()

						REQUEST_ADDITIONAL_TEXT("MISHSTA", MINIGAME_TEXT_SLOT)
						WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
							CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Waiting additional text to load.")
							WAIT(0)
						ENDWHILE

						bMovieWasLoaded = TRUE
						CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Finished loading mission_complete.gfx")
						
						WHILE IS_CUTSCENE_PLAYING()
							CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Waiting for cutscene to not be playing.")
							WAIT(0)
						ENDWHILE
						
						WHILE IS_PLAYER_DEAD(GET_PLAYER_INDEX())
							CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Waiting for player to not be dead.")
							WAIT(0)
						ENDWHILE

						ENDSCREEN_PREPARE(esd,TRUE)
						SET_AUDIO_FLAG("HoldMissionCompleteWhenPrepared", FALSE)
						IF bBlockForUISound
							WHILE NOT IS_MISSION_COMPLETE_READY_FOR_UI()
								CPRINTLN(DEBUG_MISSION_STATS, "MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC: holding for IS_MISSION_COMPLETE_READY_FOR_UI due to bBlockForUI flag")
								WAIT(0)
							ENDWHILE
						ENDIF
						
						UNREGISTER_SCRIPT_WITH_AUDIO()

						//now trigger the effect
						//WAIT(500) // so that the visual synchs with the pulse
	

						//WAIT(0)
						
					ENDIF
						

					
					IF g_bMissionStatSystemPrimed 
						StatSystemWasPrimed = TRUE
						g_bMissionStatSystemPrimed = FALSE
					ENDIF
					
					//bSplashLock = FALSE
					//bSplashHidden = FALSE
					SETTIMERA(0)
					
				ENDIF
					//SCRIPT_ASSERT("BLAH2")
				// Draw the Mission Passed screen!
				CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Movie loaded : ", bMovieWasLoaded, " - reset flag : ", g_bMissionStatSystemResetFlag)
				IF bMovieWasLoaded
				AND (NOT g_bMissionStatSystemResetFlag)
				   
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))> 0
						HANG_UP_AND_PUT_AWAY_PHONE()
					ENDIF
					
					IF g_iMissionStatsBeingTracked > 0 
						OR g_bMissionNoStatsNeedsSplashAndSting
						
						IF g_MissionStatUIIgnoreFade
							SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
						ENDIF
						 
						
						//IF (NOT splash_EXCLUSION_CONDITIONS())
							//Display mission passed with no vertical offset.
							IF bUnhiddenInRegisters AND (NOT g_bMissionNoStatsNeedsSplashAndSting)
								b_SuppressContextHelpNextUpdate = TRUE
							ENDIF
							
							IF NOT g_MissionStatSystemSuppressVisual
								CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Rendering endscreen")

								IF RENDER_ENDSCREEN(esd, FALSE,TO_FLOAT(iModeTimerMultiplier))
									g_bMissionStatSystemResetFlag = TRUE
									CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> end triggered by RENDER_ENDSCREEN return")
								ENDIF
								DISABLE_SELECTOR_THIS_FRAME()
							ENDIF
							
							
						
							IF g_MissionStatUIIgnoreFade
								SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
							ENDIF		
							
						//ENDIF						
													
					ELSE
						CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> No tracked stats or no splash and sting ")
					ENDIF
					
					//Inform flow components that a mission passed box is displaying.
					SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
					IF NOT IS_SCREEN_FADED_IN()
						REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()	//Fix for bug 2159183
					ENDIF
//					THEFEED_PAUSE()
					//Hide help text while the mission passed screen is visible.
					
//						IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_FEED) 
//							HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
//						ENDIF
					
					//Display floating help text if this is the first time
					//the passed box has displayed.
					IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_FIRST_MISSION_PASS)							
						SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_PASS")
						    CASE FHS_EXPIRED
								CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Display First Mission Passed  ")
						        ADD_HELP_TO_FLOW_QUEUE("AM_H_PASS", FHP_VERY_HIGH,2000, 2500, DEFAULT_HELP_TEXT_TIME)
						    BREAK
						    CASE FHS_DISPLAYED
						        SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_MISSION_PASS)
						    BREAK
						ENDSWITCH
					ENDIF						


				ENDIF
				
				bWhatWasTheTriggerLastCheck = g_bMissionOverStatTrigger	
				
				
				
				// Reset the system if it's been visible for long enough.
				IF g_bHeistEndscreenDisplaying
					CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Cleanup delayed by g_bHeistEndscreenDisplaying")
				ELSE
				
				
					IS_PLAYER_DEAD(GET_PLAYER_INDEX())
					IF GET_ENTITY_HEALTH(GET_PLAYER_PED(GET_PLAYER_INDEX())) < 1
						CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Overlay player dead, canceling out of overlay")
						g_MissionStatSystemSuppressVisual = TRUE //not using the reset flag for the overlay cancel out
						//prevents issues
					ENDIF
				
					//IF (TIMERA() > (DISPLAY_DURATION-7500)*iModeTimerMultiplier) 
						//IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT )
						//	//g_bMissionStatSystemResetFlag = TRUE
						///	//esd.iEndScreenDisplayFinish = GET_GAME_TIMER() + 1165
						//ENDIF
					//ENDIF
				
					IF IS_PLAYER_PED_SWITCH_IN_PROGRESS() 
					OR IS_TRANSITION_ACTIVE()
					OR g_bMissionStatSystemResetFlag
					OR g_flowUnsaved.bUpdatingGameflow
					OR g_MissionStatSystemSuppressVisual
					OR !bMovieWasLoaded
						
						
						g_bMissionNoStatsNeedsSplashAndSting = FALSE
						CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Mission passed screen ended.")
						
						g_bMissionOverStatTrigger = FALSE
						StatSystemWasPrimed = FALSE

						g_bMissionStatSystemResponseToReplayNeeded = FALSE
						g_bMissionStatSystemResponseToShitskipNeeded = FALSE
						g_bMissionStatTimeWindowGate = FALSE
						g_MissionStatSingleSpeedWatchEntity = NULL

						SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
//						THEFEED_RESUME()
						//iTimeToDisplayScreen = -1

						RESET_MISSION_STATS_SYSTEM()
						g_bMissionStatSystemResetFlag = FALSE
						bDone = TRUE
					ENDIF
				ENDIF
			ENDIF

		ENDIF
		
		WAIT(0)	
	ENDWHILE
	
	ENDSCREEN_SHUTDOWN(esd)
	
	CPRINTLN(DEBUG_MISSION_STATS, "<STAT WATCHER> Terminated due to end of script")
	g_bHasAnyCheatBeenUsed = FALSE
	g_bMissionStatSystemUploadPending = FALSE
	TERMINATE_THIS_THREAD() 
ENDSCRIPT
