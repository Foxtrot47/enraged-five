

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "mission_stat_public.sch"
///    Support script that displays mid mission mission stat alerts 
//      http://rsgediwiki1/wiki/index.php/Scaleform_Mission_Target_Complete


SCALEFORM_INDEX mov = NULL

FUNC BOOL CHECK_TRACKED_STAT_FOR_SUCCESS(INT index)
	
	
	
	IF g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[index].target].less_than_threshold = true
		RETURN FALSE//no mid mission tracking for less than threshold stats, because they would trigger immediatly
	ENDIF
	
	IF g_MissionStatTrackingArray[index].successshown
		RETURN FALSE
	ENDIF 
	
	SWITCH g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[index].target].type
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
			IF FLOOR(g_MissionStatTrackingArray[index].fvalue) >= g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[index].target].success_threshold
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		CASE MISSION_STAT_TYPE_ACTION_CAM_USE
		CASE MISSION_STAT_TYPE_UNIQUE_BOOL
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD 
		CASE MISSION_STAT_TYPE_HEADSHOTS
		CASE MISSION_STAT_TYPE_FRACTION
		CASE MISSION_STAT_TYPE_PURE_COUNT
		CASE MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
		CASE MISSION_STAT_TYPE_FINANCE_TABLE
		CASE MISSION_STAT_TYPE_FINANCE_DIRECT
		CASE MISSION_STAT_TYPE_PURE_COUNT_DISTANCE
			

				IF g_MissionStatTrackingArray[index].ivalue >= g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[index].target].success_threshold
					RETURN TRUE
				ENDIF

		
			RETURN FALSE
		CASE MISSION_STAT_TYPE_WINDOWED_TIMER
		
			IF g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[index].target].less_than_threshold
				//less than success
				IF g_bMissionStatTimeWindowClosedForGood
					IF g_MissionStatTrackingArray[index].ivalue <= g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[index].target].success_threshold
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				//normal greater than success
				IF g_MissionStatTrackingArray[index].ivalue >= g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[index].target].success_threshold
					RETURN TRUE
				ENDIF
			ENDIF
			RETURN FALSE
		
		
		
		DEFAULT
			RETURN FALSE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC UPLOAD_DATA_FOR_SUCCEEDED_TRACKED_STAT(INT index)

	//upload the success string for the given entry in g_MissionStatTrackingArray
	
	//use raw current value for now
	//INT i = index
	//i = i

	SWITCH g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[index].target].type
		
		//SET_DATA_SLOT(0, 2, "Sharpshooter", "Rank");
		
		CASE MISSION_STAT_TYPE_TOTALTIME
			/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(mov,"SET_DATA_SLOT",
			1,
			2,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			"",
			MISSION_TIME_STAT_NAME(g_MissionStatTrackingArray[index].target))
			*/
			
			BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(MISSION_TIME_STAT_NAME(g_MissionStatTrackingArray[index].target))		
			END_SCALEFORM_MOVIE_METHOD()
			
			
			
			BREAK
		CASE MISSION_STAT_TYPE_ACTION_CAM_USE
		/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(mov,"SET_DATA_SLOT",
			1,
			2,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			"",
			ACTION_CAM_STAT_NAMES(g_MissionStatTrackingArray[index].target))
			*/
			BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(ACTION_CAM_STAT_NAMES(g_MissionStatTrackingArray[index].target))
			END_SCALEFORM_MOVIE_METHOD()	

			BREAK
		CASE MISSION_STAT_TYPE_UNIQUE_BOOL
		/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(mov,"SET_DATA_SLOT",
			1,
			2,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			"",
			UNIQUE_BOOLEAN_STAT_NAMES(g_MissionStatTrackingArray[index].target))
		*/
			BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(UNIQUE_BOOLEAN_STAT_NAMES(g_MissionStatTrackingArray[index].target))
			END_SCALEFORM_MOVIE_METHOD()	
			
			
			
			
			BREAK
		CASE MISSION_STAT_TYPE_WINDOWED_TIMER
		/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(mov,"SET_DATA_SLOT",
			1,
			2,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			"",
			TIME_WINDOW_STAT_NAMES(g_MissionStatTrackingArray[index].target))
		*/	
			
			
			BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(TIME_WINDOW_STAT_NAMES(g_MissionStatTrackingArray[index].target))
			END_SCALEFORM_MOVIE_METHOD()	
			
			
			BREAK
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(mov,"SET_DATA_SLOT",
			1,
			2,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			"",
			SINGLE_ENTITY_SPEED_STRING())
		*/	
			
			BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(SINGLE_ENTITY_SPEED_STRING())
			END_SCALEFORM_MOVIE_METHOD()	
			
			BREAK
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(mov,"SET_DATA_SLOT",
			1,
			2,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			INVALID_SCALEFORM_PARAM,
			"",
			SINGLE_ENTITY_DAMAGE_STAT_NAMES(g_MissionStatTrackingArray[index].target))
		*/	
			
			
			BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(SINGLE_ENTITY_DAMAGE_STAT_NAMES(g_MissionStatTrackingArray[index].target))
			END_SCALEFORM_MOVIE_METHOD()	
			
			
			
			BREAK
		CASE MISSION_STAT_TYPE_HEADSHOTS
		/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(mov,"SET_DATA_SLOT",
			1,
			2,
			INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,
			"",
			HEADSHOT_DAMAGE_STAT_NAMES(g_MissionStatTrackingArray[index].target))
		*/	
			
			
			
			BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(HEADSHOT_DAMAGE_STAT_NAMES(g_MissionStatTrackingArray[index].target))
			END_SCALEFORM_MOVIE_METHOD()	
			
			
			BREAK
		CASE MISSION_STAT_TYPE_FRACTION
		/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(mov,"SET_DATA_SLOT",
			1,
			2,
			INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,
			"",
			FRACTION_STAT_NAMES(g_MissionStatTrackingArray[index].target))
		*/	
			
			
			
			
			BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(FRACTION_STAT_NAMES(g_MissionStatTrackingArray[index].target))
			END_SCALEFORM_MOVIE_METHOD()	
			
			
			
			
			
			BREAK
		CASE MISSION_STAT_TYPE_ACCURACY
		/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(mov,"SET_DATA_SLOT",
			1,
			2,	
			INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,
			"",
			"ACCUMIS")
		*/	
			
			
			
			BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ACCUMIS")
			END_SCALEFORM_MOVIE_METHOD()	
			
			
			
			BREAK
		CASE MISSION_STAT_TYPE_PURE_COUNT
		CASE MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
		CASE MISSION_STAT_TYPE_BULLETS_FIRED
		/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(mov,"SET_DATA_SLOT",
			1,
			2,
			INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,
			"",
			PURE_COUNT_STAT_NAMES(g_MissionStatTrackingArray[index].target))
		*/	
			
			
			
			BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(PURE_COUNT_STAT_NAMES(g_MissionStatTrackingArray[index].target))
			END_SCALEFORM_MOVIE_METHOD()
			
			
			BREAK
		

		CASE MISSION_STAT_TYPE_FINANCE_DIRECT
		CASE MISSION_STAT_TYPE_FINANCE_TABLE
		/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(mov,"SET_DATA_SLOT",
			1,
			2,
			INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,
			"",
			DOLLAR_STAT_NAMES(g_MissionStatTrackingArray[index].target))
		*/	
			
			
			
			
			BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DOLLAR_STAT_NAMES(g_MissionStatTrackingArray[index].target))
			END_SCALEFORM_MOVIE_METHOD()	
			
			
			
			
			
			
			BREAK
		DEFAULT
			SCRIPT_ASSERT("mission_stat_alerter: Unimplemented stat type!")
	ENDSWITCH

	
	

ENDPROC






SCRIPT 

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO)
		TERMINATE_THIS_THREAD()
	ENDIF
	/* this feature is disabled for now as per bug 274883
	WHILE TRUE //is currently fired and forgotten
		//PRINTSTRING("Mission stat alerter running!\n")
		
		IF g_iMissionStatsBeingTracked > 0
			//check the list for succeeded metrics and display them if this is the case
						
			
			INT i = 0
			BOOL bLoaded = FALSE
			REPEAT g_iMissionStatsBeingTracked i 
				
				BOOL bSuccess = FALSE
				//g_MissionStatTrackingArray
				
				//has already been displayed?
				//check for success
				

				bSuccess = CHECK_TRACKED_STAT_FOR_SUCCESS(i)
				
				
				
				IF (NOT g_MissionStatTrackingArray[i].successshown) AND bSuccess
				
					//check for success
						//start showing this success
					
						//load the UI
						//mission_target_complete
					BOOL doLoad = FALSE
					
				
					IF mov = NULL
					 doLoad = TRUE
					ELSE
						IF(NOT HAS_SCALEFORM_MOVIE_LOADED(mov))
							doLoad = TRUE
						ENDIF
					ENDIF
					

					IF doLoad
						mov = REQUEST_SCALEFORM_MOVIE("mission_target_complete")
						
						REQUEST_ADDITIONAL_TEXT("MISHSTA", MINIGAME_TEXT_SLOT)
						
						WHILE NOT HAS_ADDITIONAL_TEXT_LOADED()
							WAIT(0)
						ENDWHILE
						
						WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(mov))
							PRINTSTRING("mission_target_complete.gfx loading\n")
							WAIT(0)
						ENDWHILE
						bLoaded = TRUE
					ENDIF
						
						
					//upload data
					UPLOAD_DATA_FOR_SUCCEEDED_TRACKED_STAT(i)
					
					
					//show metric for however long
					CALL_SCALEFORM_MOVIE_METHOD(mov,"SHOW")
					
					BOOL bDisplaying = TRUE
					
					SETTIMERA(0)
					
					INT timertracker = 0
					
					WHILE bDisplaying		
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							DRAW_SCALEFORM_MOVIE(mov, 0.155,0.115,0.1875 ,0.0569 ,255,255,255,0)
							timertracker += TIMERA()
							SETTIMERA(0)
							IF timertracker > 8000
								bDisplaying = FALSE
							ENDIF
						ELSE
							SETTIMERA(0)
						ENDIF
						WAIT(0)
					ENDWHILE

					g_MissionStatTrackingArray[i].successshown = TRUE
					
					//delay before next 
					WAIT(2000)
				ENDIF

				
			ENDREPEAT
			
			//release UI
			IF bLoaded
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov)
			ENDIF
			
		ELSE
			//no stats to track, sleeeep
			WAIT(9999)
		
		ENDIF
		
		WAIT(0)
		
		
		IF g_bInMultiplayer
			TERMINATE_THIS_THREAD()
		ENDIF
		
	ENDWHILE
	*/
ENDSCRIPT
