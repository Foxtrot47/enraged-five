USING "commands_script.sch"
USING "commands_camera.sch"
USING "script_maths.sch"
USING "rural_bank_heist_support.sch"

TRIGGER_BOX			tbExtraDownStreetCar
TRIGGER_BOX			tbExtraEnemiesDownStreet
TRIGGER_BOX			tbFarCar

TRIGGER_BOX			tbDefensiveAreaExtraSwat

INT 				iChinookState
structTimer			tmrChinookController

INT					iFarCarState = 0

PED_INDEX 			pedToShootC
PED_INDEX 			pedToShootM

COVERPOINT_INDEX 	coverPointOnRoof
SEQUENCE_INDEX 		seqInitialGetToCoverCar

SET_PIECE_COP setPieceDownStreetSwat2ndWave

PROC setupContinueDownStreet()
	
	cleanUpHeistCutsceneAndTanker()

	//REQUEST_VEHICLE_RECORDING(2, "RBHFly")
	REQUEST_PTFX_ASSET()
//	REQUEST_MODEL(POLMAV)
	
ENDPROC

PROC CREATE_CONTINUE_DOWN_STREET_TRIGGERS()
	tbFarCar = CREATE_TRIGGER_BOX(<<-164.4979, 6349.8574, 30.371054>>, <<-122.341805,6307.606934,34.301479>>, 67.500000)
	tbExtraDownStreetCar = CREATE_TRIGGER_BOX(<<-159.342911,6344.671875,30.371817>>, <<-122.341805,6307.606934,34.301479>>, 70.000000)
	tbExtraEnemiesDownStreet = CREATE_TRIGGER_BOX(<<-140.679016,6330.562500,29.538551>>, <<-159.630707,6349.070801,35.512848>>, 70.000000)
	tbDefensiveAreaExtraSwat = CREATE_TRIGGER_BOX(<<-143.508698,6327.943359,29.656431>>, <<-135.113892,6319.319336,35.555710>>, 15.000000)
ENDPROC

PROC TRIGGER_EXTRA_STREET_ENEMIES()
	INT i 
	VECTOR vSpawnPoints[NUMBER_OF_EXTRA_SWAT_IN_STREET]
	
	vSpawnPoints[0] = <<-129.94641, 6292.23486, 30.48802>>
	vSpawnPoints[1] = <<-130.98267, 6291.12305, 30.48831>>
	
	FOR i = 0 TO NUMBER_OF_EXTRA_SWAT_IN_STREET - 1
		pedsExtraSwatInStreet[i] = CREATE_PED(PEDTYPE_MISSION, mnSwatPedModel , vSpawnPoints[i], 313.4194)
		IF DOES_ENTITY_EXIST(pedsExtraSwatInStreet[i]) AND NOT IS_ENTITY_DEAD(pedsExtraSwatInStreet[i])
			SET_PED_AS_DEFENSIVE_COP(pedsExtraSwatInStreet[i], WEAPONTYPE_ASSAULTRIFLE)
			REMOVE_PED_DEFENSIVE_AREA(pedsExtraSwatInStreet[i], TRUE)
			SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(pedsExtraSwatInStreet[i], tbDefensiveAreaExtraSwat)
			SET_PED_COMBAT_ATTRIBUTES(pedsExtraSwatInStreet[i], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsExtraSwatInStreet[i], 500.0)
		ENDIF
	ENDFOR
ENDPROC


PROC UPDATE_FAR_STREET_CAR()
	SWITCH iFarCarState
		CASE 0 
			REQUEST_VEHICLE_RECORDING(10, "RBHStreet")
			iFarCarState = 1
		BREAK
		
		CASE 1
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(10, "RBHStreet")
				iFarCarState = 2
			ENDIF
		BREAK
		
		CASE 2
			IF IS_PLAYER_IN_TRIGGER_BOX(tbFarCar)
			OR (DOES_ENTITY_EXIST(setPieceDownStreetSwat2ndWave.thisCar) AND IS_PED_INJURED(setPieceDownStreetSwat2ndWave.thisDriver)
				AND IS_PED_INJURED(setPieceDownStreetSwat2ndWave.thisPassenger))
			OR (NOT IS_ENTITY_DEAD(chinnokLandingAtEndOfStreet.thisCar) AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(chinnokLandingAtEndOfStreet.thisCar)
			    AND GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinnokLandingAtEndOfStreet.thisCar, "RBHChinLand") < 40)
				INIT_SET_PIECE_COP_CAR(setPieceDownStreetInitialCop3, SHERIFF, 10, "RBHStreet", VS_BACK_LEFT, 4400, 1, FALSE, mnSwatPedModel)
				IF IS_ENTITY_OK(pedsCopsInStreet[11])
					CLEAR_PED_TASKS_IMMEDIATELY(pedsCopsInStreet[11])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsCopsInStreet[11], FALSE)
					SET_PED_SPHERE_DEFENSIVE_AREA(pedsCopsInStreet[11], (<<-132.76459, 6333.06152, 30.49623>>), 2.0, TRUE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsCopsInStreet[11], 200)
				ENDIF				
				bHaveSwatVansBeenTriggered = TRUE
				iFarCarState = 3
			ENDIF
		BREAK
		
		CASE 3
			//EXPLODE_VEHICLE_AT_HEALTH_LEVEL(setPieceDownStreetInitialCop3.thisCar, iWidgetCopCarExplosionHealth, FALSE, TRUE)
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setPieceDownStreetInitialCop3.thisCar, "RBHStreet") >= 86.0
				REMOVE_VEHICLE_RECORDING(10, "RBHStreet")
				STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(setPieceDownStreetInitialCop3)
				REMOVE_ALL_SET_PIECE_DEFENSIVE_AREAS(setPieceDownStreetInitialCop3)
				iFarCarState = 4
			ENDIF
		BREAK
		
		CASE 4
			//EXPLODE_VEHICLE_AT_HEALTH_LEVEL(setPieceDownStreetInitialCop3.thisCar, iWidgetCopCarExplosionHealth, FALSE, TRUE)
		BREAK
	ENDSWITCH
ENDPROC


PROC MANAGE_EXPLODING_CARS_DOWN_STREET()
	INT i 
	EXPLODE_VEHICLE_AT_HEALTH_LEVEL(setPieceExtraStreetCopCar.thisCar, iWidgetCopCarExplosionHealth, FALSE, TRUE)
	
	FOR i = 0 TO NUMBER_OF_COP_CARS_IN_STREET - 1
		EXPLODE_VEHICLE_AT_HEALTH_LEVEL(CopCarsParkedDownStreet[i], iWidgetCopCarExplosionHealth, FALSE, TRUE)
	ENDFOR
ENDPROC

PROC UPDATE_CHINOOK_STATE()
	
	SWITCH iChinookState
		CASE 0 
			IF (IS_ENTITY_AT_COORD(pedGunman, << -163.1532, 6353.7832, 30.3734 >>, <<2.5, 2.5, 2.5>>)
			 AND IS_ENTITY_AT_COORD(PedMichael(), << -167.3999, 6349.3896, 30.4634 >>, <<2.5, 2.5, 2.5>>))
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-166.171326,6351.199707,30.259132>>, <<-129.359970,6314.261230,33.552048>>, 77.500000)
				REQUEST_MODEL(CARGOBOB)
				REQUEST_VEHICLE_RECORDING(1, "RBHChinLand")
				REQUEST_MODEL(S_M_Y_Marine_03)
				RESTART_TIMER_NOW(tmrChinookController)
				CPRINTLN(DEBUG_MISSION, "UPDATE_CHINOOK_STATE - We've requested models, moving to State 1")
				iChinookState = 1
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_MODEL_LOADED(CARGOBOB)				
			AND HAS_MODEL_LOADED(S_M_Y_Marine_03)			
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "RBHChinLand")
				INIT_SET_PIECE_COP_CAR(chinnokLandingAtEndOfStreet, CARGOBOB, 1, "RBHChinLand", VS_FRONT_RIGHT, 14500)
					
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(chinnokLandingAtEndOfStreet.thisCar, FALSE)
				SET_PED_CAN_BE_TARGETTED(chinnokLandingAtEndOfStreet.thisDriver, FALSE)
				SET_PED_CAN_BE_TARGETTED(chinnokLandingAtEndOfStreet.thisPassenger, FALSE)
				
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(chinnokLandingAtEndOfStreet.thisDriver, TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(chinnokLandingAtEndOfStreet.thisPassenger, TRUE)
				
				CPRINTLN(DEBUG_MISSION, "PS_2A_SHOOTOUT_STREET_ARMY_HELI started")
				START_AUDIO_SCENE("PS_2A_SHOOTOUT_STREET_ARMY_HELI")
				
				SET_PLAYBACK_SPEED(chinnokLandingAtEndOfStreet.thisCar, -1.0)
				CPRINTLN(DEBUG_MISSION, "UPDATE_CHINOOK_STATE - Models streamed, and helicopter started, going to State 2")
				iChinookState = 2
			ENDIF
		BREAK
		
		CASE 2
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinnokLandingAtEndOfStreet.thisCar, "RBHChinLand") > 40.5
			OR IS_ENTITY_DEAD(chinnokLandingAtEndOfStreet.thisCar)
				eDownStreetMessageState = MISSION_MESSAGE_06
				CPRINTLN(DEBUG_MISSION, "UPDATE_CHINOOK_STATE - Chinook is > 40% through recording, play dialogue, going to State 3")
				iChinookState = 3
			ENDIF
		BREAK

		CASE 3
		
			PRINTLN("GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS:", GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinnokLandingAtEndOfStreet.thisCar, "RBHChinLand"))
		
			//IF (NOT IS_ENTITY_DEAD(chinnokLandingAtEndOfStreet.thisCar) AND IS_VEHICLE_ON_ALL_WHEELS(chinnokLandingAtEndOfStreet.thisCar))//GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinnokLandingAtEndOfStreet.thisCar, "RBHChinLand") > 46.0
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinnokLandingAtEndOfStreet.thisCar, "RBHChinLand") < 5.0 //plays in reverse!
			OR IS_ENTITY_DEAD(chinnokLandingAtEndOfStreet.thisCar)
				
				GET_COP_OUT_OF_CAR_AND_INTO_COMBAT(chinnokLandingAtEndOfStreet.thisDriver, chinnokLandingAtEndOfStreet.thisCar, WEAPONTYPE_CARBINERIFLE, 50, 3.0, TRUE)
				GET_COP_OUT_OF_CAR_AND_INTO_COMBAT(chinnokLandingAtEndOfStreet.thisPassenger, chinnokLandingAtEndOfStreet.thisCar, WEAPONTYPE_CARBINERIFLE, 500, 3.0)
				CPRINTLN(DEBUG_MISSION, "PS_2A_SHOOTOUT_STREET_ARMY_HELI stopped")
				STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_STREET_ARMY_HELI")
				CPRINTLN(DEBUG_MISSION, "UPDATE_CHINOOK_STATE - DONE!")
				iChinookState = 1000
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


PROC UPDATE_DOWN_STREET_CONVERSATIONS()
	SWITCH eDownStreetMessageState
	
		// Take cover at the car
		CASE MISSION_MESSAGE_01
			IF CREATE_CONVERSATION(myScriptedSpeech,  "RBH2AUD", "RBH2A_MCAR", CONV_PRIORITY_VERY_HIGH)
				eDownStreetMessageState = MISSION_MESSAGE_02
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_02
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_BLOCKED", CONV_PRIORITY_VERY_HIGH)
				eDownStreetMessageState = MISSION_MESSAGE_03
			ENDIF
		BREAK
				
		CASE MISSION_MESSAGE_03
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_SWAT", CONV_PRIORITY_VERY_HIGH)
				eDownStreetMessageState = MISSION_MESSAGE_11
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_11
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_GUNMAN_CONVERSATION("RBH_NOOSE_GM", "RBH_NOOSE_DJ", "RBH_NOOSE_CH", "RBH_NOOSE_NR", "RBH_NOOSE_PM")
					eDownStreetMessageState = MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		
		// Mike says to move up from the car
		CASE MISSION_MESSAGE_04
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_MFOLL1", CONV_PRIORITY_VERY_HIGH)	
				eDownStreetMessageState = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		// Mike says to move up the street
		CASE MISSION_MESSAGE_05
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_MOVEUP", CONV_PRIORITY_VERY_HIGH)	
				eDownStreetMessageState = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		// Crew reacts to the chinook helicopter landing
		CASE MISSION_MESSAGE_06
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_MARMY", CONV_PRIORITY_VERY_HIGH)
				eDownStreetMessageState = MISSION_MESSAGE_07
			ENDIF
		BREAK
			
		CASE MISSION_MESSAGE_07
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_TWIN", CONV_PRIORITY_VERY_HIGH)
				eDownStreetMessageState = MISSION_MESSAGE_09
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_09
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_ARMY", CONV_PRIORITY_VERY_HIGH)
					eDownStreetMessageState = MISSION_MESSAGE_10
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_10
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_TARMY", CONV_PRIORITY_VERY_HIGH)
					eDownStreetMessageState = MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
				
		CASE MISSION_MESSAGE_12
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND TIMERA() > 1500
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_COPARV", CONV_PRIORITY_VERY_HIGH)
					eDownStreetMessageState = MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK		
				
		
		// Sort of a generic yell
		CASE MISSION_MESSAGE_08
			//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_GOALLEY", CONV_PRIORITY_VERY_HIGH )
			IF CREATE_GUNMAN_CONVERSATION("RBH_FRONT_GM", "RBH_FRONT_DJ", "RBH_FRONT_CH","RBH_FRONT_NR", "RBH_FRONT_PM")
				eDownStreetMessageState = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC UPDATE_GUNMAN_AI_DOWN_STREET()
	//Control shooting when on waypoints
	IF NOT IS_PED_INJURED(pedGunman)
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
			
			#IF IS_DEBUG_BUILD IF bIsSuperDebugEnabled
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(pedGunman, GET_STRING_FROM_INT(iShootingStageCrewMember), -0.5, 128, 128, 0)
			ENDIF #ENDIF
			
			iShootingStageCrewMember = GET_PED_WAYPOINT_PROGRESS(pedGunman)
			
								
			SWITCH iShootingSwitchC 
				
				CASE 0
					IF iShootingStageCrewMember >= 0
						WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(pedGunman, << -159.9866, 6370.2769, 32.2477 >> , FALSE)		 
						iShootingSwitchC++
					ENDIF
				BREAK
				
				CASE 1
					IF iShootingStageCrewMember >= 6
						WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(pedGunman, << -119.2596, 6305.9980, 31.4541 >> , FALSE)
						iShootingSwitchC++
					ENDIF
				BREAK
				
				CASE 2
					IF iShootingStageCrewMember >= 21
						GET_CLOSEST_PED(<< -133.0870, 6319.7886, 31.5620 >>, 75.0, TRUE, TRUE, pedToShootC, FALSE, TRUE)
				
						IF DOES_ENTITY_EXIST(pedToShootC)
							WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(pedGunman,GET_ENTITY_COORDS(pedToShootC, FALSE) , FALSE)						
							iShootingSwitchC++
						ENDIF
					ENDIF
				BREAK
								
				CASE 3
					
					IF NOT DOES_ENTITY_EXIST(pedToShootC)
					OR IS_PED_INJURED(pedToShootC)
					OR IS_PED_HURT(pedToShootC)
						iShootingSwitchC = 2
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_MICHAEL_AI_DOWN_STREET() 
	IF NOT IS_PED_INJURED(PedMichael())
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
			
			#IF IS_DEBUG_BUILD IF bIsSuperDebugEnabled
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(PedMichael(), GET_STRING_FROM_INT(iShootingStageMichael), -0.5, 128, 128, 0)
			ENDIF #ENDIF
			
			iShootingStageMichael = GET_PED_WAYPOINT_PROGRESS(PedMichael())
			
					
			SWITCH iShootingSwitchM
				
				CASE 0
					IF iShootingStageMichael >= 0
						WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(), << -186.1119, 6340.3379, 31.8343 >> , FALSE)		 					
						iShootingSwitchM++
					ENDIF
				BREAK
				
				CASE 1
					IF iShootingStageMichael >= 6
						WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(), << -119.2596, 6305.9980, 31.4541 >> , FALSE)
						iShootingSwitchM++
					ENDIF
				BREAK
				
				CASE 2
					IF iShootingStageMichael >= 18
						GET_CLOSEST_PED(<< -133.0870, 6319.7886, 31.5620 >>, 75.0, TRUE, TRUE, pedToShootM, FALSE, TRUE)
				
						IF DOES_ENTITY_EXIST(pedToShootM)
							WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(),GET_ENTITY_COORDS(pedToShootM, FALSE) , FALSE)						
							iShootingSwitchM++
						ENDIF
					ENDIF
				BREAK
								
				CASE 3
					
					IF NOT DOES_ENTITY_EXIST(pedToShootM)
					OR IS_PED_INJURED(pedToShootM)
					OR IS_PED_HURT(pedToShootM)
						iShootingSwitchM = 2
					ENDIF
				BREAK
								
			ENDSWITCH
			
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_EXPLODE_CHOPPER()
		
	SWITCH iExplodeChopperSubstage
				
		CASE 0
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chopperFlyover.thisCar, "RBHFly") > 14.5
				
				CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_CHOP2", CONV_PRIORITY_VERY_HIGH)
				iExplodeChopperSubstage++
			ENDIF
		BREAK
		
		CASE 1
	
			PRINTSTRING("Chopper: ")PRINTFLOAT(GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chopperFlyover.thisCar, "RBHFlyover")) PRINTNL()
			IF DOES_ENTITY_EXIST(chopperFlyover.thisCar)
				PRINTSTRING("Chopper health: ")PRINTINT(GET_ENTITY_HEALTH(chopperFlyover.thisCar)) PRINTNL()
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(chopperFlyover.thisCar)
				IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chopperFlyover.thisCar, "RBHFly") > 22.0
				
					SET_ENTITY_INVINCIBLE(chopperFlyover.thisCar, FALSE)
					
					IF EXPLODE_VEHICLE_AT_HEALTH_LEVEL(chopperFlyover.thisCar, 450, TRUE, TRUE) 
						STOP_PLAYBACK_RECORDED_VEHICLE(chopperFlyover.thisCar)
						SET_VEHICLE_OUT_OF_CONTROL(chopperFlyover.thisCar, TRUE, TRUE)
						IF IS_AUDIO_SCENE_ACTIVE("PS_2A_SHOOTOUT_STREET_HELI")				
							STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_STREET_HELI")				
						ENDIF
						SET_VEHICLE_AS_NO_LONGER_NEEDED(chopperFlyover.thisCar)
						iExplodeChopperSubstage++
					ENDIF
				ENDIF
			ELSE		
				SET_VEHICLE_AS_NO_LONGER_NEEDED(chopperFlyover.thisCar)
				IF IS_AUDIO_SCENE_ACTIVE("PS_2A_SHOOTOUT_STREET_HELI")				
					STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_STREET_HELI")				
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

BOOL b2ndSwatWaveAttacking

PROC stageContinueDownStreet()

	INT i

	doBuddyFailCheck()

	//MANAGE_COP_RESPAWN_IN_STREET()
	manageTyrePopping2()
	
	MANAGE_EXTRA_TYRE_POPS()
	
	THROW_BANK_NOTES_IN_AIR_WHEN_SHOT(PedTrevor())

	KEEP_TIME_LOOKING_SWEET()
	
	MANAGE_SPAWNING_SWAT_CARS_FOR_DOWN_STREET()
	
	IF iContinueDownStreetStage <> 7
		CONTROL_WAR_CRIES_DIALOGUE()
	ENDIF
	
	DISABLE_VEHICLE_EXPLOSION_BREAK_OFF_PARTS()
	
	IF iContinueDownStreetStage > 0
		UPDATE_DOWN_STREET_CONVERSATIONS()
		UPDATE_CHINOOK_STATE()
		UPDATE_GUNMAN_AI_DOWN_STREET()
		UPDATE_MICHAEL_AI_DOWN_STREET()
//		UPDATE_EXPLODE_CHOPPER()
		UPDATE_FAR_STREET_CAR()
	ENDIF
	
	PROCESS_MICHAEL_TREVOR_SWITCH()
	MANAGE_EXPLODING_CARS_DOWN_STREET()
	//Jump over wall and some more cops turn up so you continue down south...
	
	//CPRINTLN(DEBUG_MISSION, "iContinueDownStreetStage = ", iContinueDownStreetStage)
	
	SWITCH iContinueDownStreetStage

		CASE 0
		
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("PS2A_GAS_STATION_EXPLOSION_L")
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("PS2A_GAS_STATION_EXPLOSION_R")
		
			CPRINTLN(DEBUG_MISSION, "PS_2A_SHOOTOUT_STREET_MAIN is being started")
			CREATE_CONTINUE_DOWN_STREET_TRIGGERS()
			
			START_AUDIO_SCENE("PS_2A_SHOOTOUT_STREET_MAIN")
		
			SET_PED_FIRING_PATTERN(pedMichael(), FIRING_PATTERN_BURST_FIRE)
			SET_PED_FIRING_PATTERN(pedGunman, FIRING_PATTERN_SHORT_BURSTS)
		
			RESET_INJURED_BOOLS()
			setupContinueDownStreet()
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "STAGE_CONTINUE_DOWN_STREET") 
			g_iPaletoScoreTake = iCurrentTake	//UPdate the saved take
			CLEANUP_FODDER_PEDS()
			
			REQUEST_SCRIPT_AUDIO_BANK("PS2A_Jeep_Skids")
			
			DELETE_ARRAY_OF_PEDS(pedsFodderCops)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnCopPedModel)
			
			DELETE_ARRAY_OF_PEDS(pedsCopsOutsideBank)
			DELETE_ARRAY_OF_BLIPS(blipPedsCopsOutsideBank)
			
			eDownStreetMessageState = MISSION_MESSAGE_IDLE
			
			//blipd the baddies
			FOR i = 0 TO NUMBER_OF_COPS_IN_STREET - 1
				IF DOES_ENTITY_EXIST(pedsCopsInStreet[i])
					//blipPedsCopsInStreet[i] = CREATE_BLIP_FOR_PED_WRAPPER(pedsCopsInStreet[i], TRUE)
					IF NOT IS_PED_INJURED(pedsCopsInStreet[i])
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsCopsInStreet[i], 150.00)
					ENDIF
				ENDIF
			ENDFOR
			//By Ruiner
			
			SET_PED_SPHERE_DEFENSIVE_AREA(PedMichael(), <<-183.3665, 6360.9575, 30.3013>>, 2.5)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), TRUE)
		
			IF IS_SCREEN_FADED_OUT()
				OPEN_SEQUENCE_TASK(seqInitialGetToCoverCar)
					TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHGdnMichael", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<-183.3665, 6360.9575, 30.3013>>, << -140.9123, 6352.0674, 31.4107 >>, PEDMOVE_RUN, TRUE, 0.5, 4.0, TRUE)// DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)			
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.00)
				CLOSE_SEQUENCE_TASK(seqInitialGetToCoverCar)
				TASK_PERFORM_SEQUENCE(PedMichael(), seqInitialGetToCoverCar)
				CLEAR_SEQUENCE_TASK(seqInitialGetToCoverCar)
			ELSE
				OPEN_SEQUENCE_TASK(seqInitialGetToCoverCar)
					IF IS_PED_SHOOTING(PedMichael())
						TASK_SHOOT_AT_COORD(NULL, (<<-184.75349, 6370.58984, 30.98511>>), 1500, FIRING_TYPE_CONTINUOUS)
					ENDIF
					TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHGdnMichael", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<-183.3665, 6360.9575, 30.3013>>, << -140.9123, 6352.0674, 31.4107 >>, PEDMOVE_RUN, TRUE, 0.5, 4.0, TRUE)// DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)			
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.00)
				CLOSE_SEQUENCE_TASK(seqInitialGetToCoverCar)
				TASK_PERFORM_SEQUENCE(PedMichael(), seqInitialGetToCoverCar)
				CLEAR_SEQUENCE_TASK(seqInitialGetToCoverCar)
			ENDIF
			
			SET_PED_SPHERE_DEFENSIVE_AREA(pedGunman, << -180.8451, 6364.3267, 30.3193 >>, 2.5)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
		
			OPEN_SEQUENCE_TASK(seqInitialGetToCoverCar)
				IF IS_PED_SHOOTING(pedGunman)
					TASK_SHOOT_AT_COORD(NULL, (<<-184.75349, 6370.58984, 30.98511>>), 1500, FIRING_TYPE_CONTINUOUS)
				ENDIF
				TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHGdnCrew", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
				TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, << -180.8451, 6364.3267, 30.3193 >>, << -180.5160, 6341.9678, 32.1969 >>, PEDMOVE_RUN, TRUE, 0.5, 4.0, TRUE)// DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)			
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.00)
			CLOSE_SEQUENCE_TASK(seqInitialGetToCoverCar)
			TASK_PERFORM_SEQUENCE(pedGunman, seqInitialGetToCoverCar)
			CLEAR_SEQUENCE_TASK(seqInitialGetToCoverCar)
			
			DISTANT_COP_CAR_SIRENS(TRUE)
			
			coverPointOnRoof = ADD_COVER_POINT(<<-173.1168, 6341.3677, 35.1334>>, 36.3884, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_180)
			
			SETUP_WANTED_LEVEL(5, TRUE,  150.0, 5.0, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
			
			SET_DISPATCH_TIME_BETWEEN_SPAWN_ATTEMPTS(DT_SWAT_AUTOMOBILE, 15.0)
			
			SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(dt_police_automobile, 12)
			
			REMOVE_VEHICLE_COMBAT_AVOIDANCE_AREA(avoidAreaOutsideBank)
			avoidAreaContinueDownStreet = ADD_VEHICLE_COMBAT_ANGLED_AVOIDANCE_AREA(<<16.295980,6553.105957,29.673317>>, <<-224.059769,6311.169922,42.503540>>, 25.250000)

			iShootingSwitchC = 0
			iShootingSwitchM = 0
			iChinookState = 0
			iFarCarState = 0
			bTriggeredExtraCar = FALSE
			bTriggeredExtraEnemies = FALSE
			bHaveSwatVansStopped = FALSE
			TRIGGER_MUSIC_EVENT("RH2A_FIGHT_MID")
			
			REMOVE_VEHICLE_RECORDING(2, "RBHFlyover")
			
			iInitialNumberOfCopsKilled = GET_TOTAL_NUMBER_OF_COP_CARS_EXPLODED()
			
			IF IS_SCREEN_FADED_OUT()
				DELETE_PED(pedsCopsInStreet[0])
			ENDIF
			
			FADE_IN_IF_NEEDED()
			iContinueDownStreetStage++
			
			iContinueDownStreetStage =6 //++
			SETTIMERA(0)
		BREAK
				
		CASE 6
		
			//Kill this guy
			IF IS_ENTITY_AT_COORD(PedMichael(), <<-183.3665, 6360.9575, 30.3013>>, <<4.5, 4.5, 2.5>>)
			OR IS_ENTITY_AT_COORD(pedGunman, << -180.8451, 6364.3267, 30.3193 >>, <<4.5, 4.5, 2.5>>)
				IF NOT IS_PED_INJURED(pedsCopsInStreet[0])
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PED_BONE_COORDS(pedsCopsInStreet[0], BONETAG_HEAD, <<0.0, 0.0, 0.0>>) + <<20.0, 20.0, 0.5>>, GET_PED_BONE_COORDS(pedsCopsInStreet[0], BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 25, TRUE)
					APPLY_DAMAGE_TO_PED(pedsCopsInStreet[0], 1000, TRUE)
				ENDIF
			ENDIF		
		
			IF (IS_ENTITY_AT_COORD(PedMichael(), <<-183.3665, 6360.9575, 30.3013>>, <<2.5, 2.5, 2.5>>)
			AND IS_ENTITY_AT_COORD(pedGunman, << -180.8451, 6364.3267, 30.3193 >>, <<2.5, 2.5, 2.5>>))
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-135.612366,6319.130371,30.047840>>, <<-179.073486,6361.791504,32.883209>>, 21.250000)
			
				SETTIMERA(0)
				iContinueDownStreetStage++
			ENDIF
		BREAK
				
		CASE 7
			
			
			IF DOES_ENTITY_EXIST(setPieceDownStreetInitialCop1.thisDriver)
				IF TIMERA() > 30000			
				OR (IS_PED_INJURED(setPieceDownStreetInitialCop1.thisDriver)
					AND IS_PED_INJURED(setPieceDownStreetInitialCop1.thisPassenger)
					AND IS_PED_INJURED(setPieceDownStreetInitialCop1.thisPassenger2))
				OR (GET_TOTAL_NUMBER_OF_COP_CARS_EXPLODED() - iInitialNumberOfCopsKilled) >= 4
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-135.612366,6319.130371,30.047840>>, <<-179.073486,6361.791504,32.883209>>, 21.250000)
					eDownStreetMessageState = MISSION_MESSAGE_04
					CLEAR_PED_TASKS(pedGunman)
					CLEAR_PED_TASKS(PedMichael())
							
					
					INIT_SET_PIECE_COP_CAR(setPieceDownStreetSwat2ndWave, SHERIFF, 11, "RBHStreet", VS_BACK_LEFT, 4000.0, 1.05, FALSE, mnSwatPedModel)
					b2ndSwatWaveAttacking = FALSE
					
					//START_AUDIO_SCENE("PS_2A_SHOOTOUT_STREET_HELI")
					
					SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_COORD(PedMichael(), <<-159.9952, 6358.1812, 30.4825>>, << -180.5160, 6341.9678, 32.1969 >>, FALSE, 0, PEDMOVE_RUN)	
	//				
	//				INIT_SET_PIECE_COP_CAR(chopperFlyover, POLMAV, 2, "RBHFly", VS_FRONT_RIGHT, 0000.0, 1.0, FALSE, mnSwatPedModel)
	//				DELETE_PED(chopperFlyover.thisPassenger)
	//				IF IS_ENTITY_OK(chopperFlyover.thisCar)
	//					SET_VEHICLE_LIVERY(chopperFlyover.thisCar, 0)
	//					SET_ENTITY_INVINCIBLE(chopperFlyover.thisCar, TRUE)
	//					SET_VEHICLE_SEARCHLIGHT(chopperFlyover.thisCar,true,FALSE)
	//				ENDIF
					
					//and then
					//There's a task you call if you want the pilot of the chopper to be responsible for aiming.
					//Or if it's the player,
					//TASK_VEHICLE_AIM_USING_CAMERA(driver)
					//or
	//				TASK_VEHICLE_AIM_AT_COORD(chopperFlyover.thisDriver, GET_ENTITY_COORDS(PLAYER_PED_ID()))

					
					//Clear cops by the cars in the middle
					
					IF NOT IS_PED_INJURED(pedsCopsInStreet[1])
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PED_BONE_COORDS(pedsCopsInStreet[1], BONETAG_HEAD, <<0.0, 0.0, 0.0>>) + <<20.0, 20.0, 0.5>>, GET_PED_BONE_COORDS(pedsCopsInStreet[1], BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 25, TRUE)
						APPLY_DAMAGE_TO_PED(pedsCopsInStreet[1], 1000, TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(pedsCopsInStreet[1])
					ENDIF
					
					IF NOT IS_PED_INJURED(pedsCopsInStreet[8])
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PED_BONE_COORDS(pedsCopsInStreet[8], BONETAG_HEAD, <<0.0, 0.0, 0.0>>) + <<20.0, 20.0, 0.5>>, GET_PED_BONE_COORDS(pedsCopsInStreet[8], BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 25, TRUE)
						APPLY_DAMAGE_TO_PED(pedsCopsInStreet[8], 1000, TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(setPieceRiotVan1.thisPassenger2)
						APPLY_DAMAGE_TO_PED(setPieceRiotVan1.thisPassenger2, 200, TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(setPieceRiotVan1.thisPassenger2)
					ENDIF
					
					REMOVE_WAYPOINT_RECORDING("RBHGdnCrew")
					REMOVE_WAYPOINT_RECORDING("RBHGdnMichael")
										
					SETTIMERA(0)
					iContinueDownStreetStage++
				ENDIF
			ELSE
				SETTIMERA(0)
			ENDIF
		BREAK
		
		CASE 8
			IF TIMERA() > 600			
			
				SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_COORD(pedGunman, <<-164.6292, 6356.8794, 30.3477>>, <<-149.8311, 6329.9648, 31.8097>>, FALSE, 0, PEDMOVE_RUN)
				SETTIMERA(0)
				
				IF NOT IS_PED_INJURED(pedsCopsInStreet[0])
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PED_BONE_COORDS(pedsCopsInStreet[0], BONETAG_HEAD, <<0.0, 0.0, 0.0>>) + <<20.0, 20.0, 0.5>>, GET_PED_BONE_COORDS(pedsCopsInStreet[0], BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 25, TRUE)
					APPLY_DAMAGE_TO_PED(pedsCopsInStreet[0], 1000, TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(pedsCopsInStreet[0])
				ENDIF
				IF NOT IS_PED_INJURED(pedsCopsInStreet[6])
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PED_BONE_COORDS(pedsCopsInStreet[6], BONETAG_HEAD, <<0.0, 0.0, 0.0>>) + <<20.0, 20.0, 0.5>>, GET_PED_BONE_COORDS(pedsCopsInStreet[6], BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 25, TRUE)
					APPLY_DAMAGE_TO_PED(pedsCopsInStreet[6], 1000, TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(pedsCopsInStreet[6])
				ENDIF
				
				SET_VEHICLE_AS_NO_LONGER_NEEDED(CopCarsParkedDownStreet[3])
				//SET_MODEL_AS_NO_LONGER_NEEDED(DOMINATOR)
		
				IF NOT IS_PED_INJURED(setPieceRiotVan1.thisPassenger)
					APPLY_DAMAGE_TO_PED(setPieceRiotVan1.thisPassenger, 200, TRUE)
				ENDIF			
				SETTIMERA(0)
				eDownStreetMessageState = MISSION_MESSAGE_12
				
				iContinueDownStreetStage = 10
			ENDIF
		BREAK
		
		CASE 10
			IF TIMERA() > 15000			
			OR (IS_PED_INJURED(setPieceDownStreetSwat2ndWave.thisDriver)
				AND IS_PED_INJURED(setPieceDownStreetSwat2ndWave.thisPassenger)
				AND IS_PED_INJURED(setPieceDownStreetSwat2ndWave.thisPassenger2))
						
			//OR (GET_TOTAL_NUMBER_OF_COP_CARS_EXPLODED() - iInitialNumberOfCopsKilled) >= 9
			//OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-166.171326,6351.199707,30.259132>>, <<-129.359970,6314.261230,33.552048>>, 77.500000)
				eDownStreetMessageState = MISSION_MESSAGE_05
								
				SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_HATED_ENTITIES(PedMichael(), <<-150.5553, 6337.0117, 30.6093>>, <<-180.3526, 6344.9468, 31.8729>>, FALSE, 0, PEDMOVE_RUN)
				
//				CLEAR_PED_TASKS(PedMichael())
//				IF bIsBuddyAPro
//					WHILE WAYPOINT_RECORDING_GET_COORD("RBHContSt2", iLastWaypointMichael, vLastWaypointMichael)
//						iLastWaypointMichael++
//					ENDWHILE
//				
//					IF NOT IS_ENTITY_DEAD(PedMichael())
//						SET_PED_SPHERE_DEFENSIVE_AREA(PedMichael(), <<-146.6136, 6319.2290, 30.5536>>, 2.0)
//					ENDIF
//												
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), TRUE)
//					OPEN_SEQUENCE_TASK(seqContinueDownStreet)
//						TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHContSt1", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
//						TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, <<-146.6136, 6319.2290, 30.5536>>, 2.0)
//						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
//						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//					CLOSE_SEQUENCE_TASK(seqContinueDownStreet)
//					TASK_PERFORM_SEQUENCE(PedMichael(), seqContinueDownStreet)
//					CLEAR_SEQUENCE_TASK(seqContinueDownStreet)
//				ELSE
//					SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_HATED_ENTITIES(PedMichael(), <<-150.5553, 6337.0117, 30.6093>>, << -163.6337, 6321.2432, 31.0090 >>, FALSE, 0, PEDMOVE_RUN)
//				ENDIF
				SETTIMERA(0)
				iContinueDownStreetStage++
			ENDIF
		BREAK
		
		CASE 11
			IF TIMERA() > 500			
				CLEAR_PED_TASKS(pedGunman)
				
				SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_HATED_ENTITIES(pedGunman,  <<-149.2521, 6338.2495, 30.5627>>, << -134.2528, 6331.0693, 30.5969 >>, FALSE, 0, PEDMOVE_RUN)
				
//				IF bIsBuddyAPro
					//SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_HATED_ENTITIES(pedGunman,  << -143.1167, 6328.9009, 30.6474 >>, << -140.9123, 6352.0674, 31.4107 >>, FALSE, 0, PEDMOVE_RUN)
									
//					WHILE WAYPOINT_RECORDING_GET_COORD("RBHContSt1", iLastWaypointCrew, vLastWaypointCrew)
//						iLastWaypointCrew++
//					ENDWHILE
//					
//					IF NOT IS_ENTITY_DEAD(pedGunman)
//						SET_PED_SPHERE_DEFENSIVE_AREA(pedGunman, vLastWaypointCrew, 2.0)
//					ENDIF
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
//					OPEN_SEQUENCE_TASK(seqContinueDownStreet)
//						TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHContSt2", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
//						TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, vLastWaypointMichael, 2.0)
//						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
//						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//					CLOSE_SEQUENCE_TASK(seqContinueDownStreet)
//					TASK_PERFORM_SEQUENCE(pedGunman, seqContinueDownStreet)
//					CLEAR_SEQUENCE_TASK(seqContinueDownStreet)
//					//WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(pedGunman, << -163.6337, 6321.2432, 31.0090 >>, FALSE)
//				
//				ELSE
//					SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_HATED_ENTITIES(pedGunman,  <<-149.2521, 6338.2495, 30.5627>>, << -134.2528, 6331.0693, 30.5969 >>, FALSE, 0, PEDMOVE_RUN)
//				ENDIF
				SETTIMERA(0)
				iContinueDownStreetStage++
			ENDIF
		BREAK
		
		CASE 12
			IF TIMERA() > 1000
				eDownStreetMessageState = MISSION_MESSAGE_08
				iContinueDownStreetStage = 16
			ENDIF
		BREAK
			
		CASE 16
			//IF (NOT bIsBuddyAPro
			IF (IS_ENTITY_IN_ANGLED_AREA(pedGunman, <<-133.918655,6337.026855,30.240294>>, <<-151.446930,6321.643555,35.782425>>, 13.500000)
			AND IS_ENTITY_IN_ANGLED_AREA(pedMichael(), <<-133.918655,6337.026855,30.240294>>, <<-151.446930,6321.643555,35.782425>>, 13.500000))
			OR (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-126.789757,6342.548340,30.490435>>, <<-163.214081,6318.004395,36.133335>>, 23.500000))
			OR (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-133.918655,6337.026855,30.240294>>, <<-151.446930,6321.643555,35.782425>>, 13.500000))
			
			OR (DOES_ENTITY_EXIST(setPieceExtraStreetCopCar.thisCar)			//If player takes out both far away set pieces
				AND IS_PED_INJURED(setPieceExtraStreetCopCar.thisDriver)		//stops him chilling like in: 1442312
				AND IS_PED_INJURED(setPieceExtraStreetCopCar.thisPassenger)
				AND IS_PED_INJURED(setPieceExtraStreetCopCar.thisPassenger2)
				AND DOES_ENTITY_EXIST(setPieceDownStreetInitialCop3.thisCar)
				AND IS_PED_INJURED(setPieceDownStreetInitialCop3.thisDriver)
				AND IS_PED_INJURED(setPieceDownStreetInitialCop3.thisPassenger)
				AND IS_PED_INJURED(setPieceDownStreetInitialCop3.thisPassenger2))
				
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 0.0)
				
				REMOVE_COVER_POINT(coverPointOnRoof)
				SETTIMERA(0)
				mission_stage = STAGE_PROCEED_TO_ALLEY	
			ENDIF
		BREAK
	ENDSWITCH

	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setPieceDownStreetSwat2ndWave.thisCar, "RBHSTREET") >= 95.5
	AND b2ndSwatWaveAttacking = FALSE		
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(setPieceDownStreetSwat2ndWave)
		b2ndSwatWaveAttacking = TRUE
	ENDIF			

				
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(continueDownStreetPullUpCar.thisCar, "CBSTG2") >= 95.5
	AND b2nDstageCopAttacking = FALSE		
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(continueDownStreetPullUpCar)
		b2nDstageCopAttacking = TRUE
	ENDIF
	
	IF NOT bTriggeredExtraCar
		IF IS_PLAYER_IN_TRIGGER_BOX(tbExtraDownStreetCar)
		OR (DOES_ENTITY_EXIST(setPieceDownStreetSwat2ndWave.thisCar) AND IS_PED_INJURED(setPieceDownStreetSwat2ndWave.thisDriver)
			AND IS_PED_INJURED(setPieceDownStreetSwat2ndWave.thisPassenger))
//		OR IS_ENTITY_IN_TRIGGER_BOX(tbExtraDownStreetCar, pedMichael())
//		OR IS_ENTITY_IN_TRIGGER_BOX(tbExtraDownStreetCar, pedGunman)
			INIT_SET_PIECE_COP_CAR(setPieceExtraStreetCopCar, mnCopCarModel, 7, "RBHStreet", VS_FRONT_RIGHT, 250.0, 1.00, FALSE, mnSwatPedModel)
			DELETE_PED(setPieceExtraStreetCopCar.thisPassenger2)
			bTriggeredExtraCar = TRUE
		ENDIF
	ELSE
		IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setPieceExtraStreetCopCar.thisCar, "RBHStreet") >= 95
			REMOVE_VEHICLE_RECORDING(7, "RBHStreet")
			STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(setPieceExtraStreetCopCar)
		ENDIF
	ENDIF
	
	IF NOT bTriggeredExtraEnemies
		IF IS_PLAYER_IN_TRIGGER_BOX(tbExtraEnemiesDownStreet)
		OR IS_ENTITY_IN_TRIGGER_BOX(tbExtraEnemiesDownStreet, pedMichael())
		OR IS_ENTITY_IN_TRIGGER_BOX(tbExtraEnemiesDownStreet, pedGunman)
			TRIGGER_EXTRA_STREET_ENEMIES()
			bTriggeredExtraEnemies = TRUE
		ENDIF
	ENDIF

	
ENDPROC
